--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_PR_PREPRODUCTION_LIST(
      CLIENT integer,
      MPKTM varchar(40) CHARACTER SET UTF8                           ,
      ORDTYPE varchar(3) CHARACTER SET UTF8                           ,
      ORDREG varchar(100) CHARACTER SET UTF8                           ,
      FROMDATES varchar(100) CHARACTER SET UTF8                           ,
      TODATES varchar(100) CHARACTER SET UTF8                           ,
      ORDLIST varchar(1024) CHARACTER SET UTF8                           )
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      NAZWAT varchar(255) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      PRSHMAT integer,
      PRSCHEDGUIDEPOS integer,
      PRNAGZAM integer,
      PRID varchar(40) CHARACTER SET UTF8                           ,
      PRPOZZAM integer,
      ILOSC numeric(14,4),
      ILOSCMAT numeric(14,4),
      MIARA varchar(5) CHARACTER SET UTF8                           ,
      WORKPLACE varchar(40) CHARACTER SET UTF8                           ,
      PRDEPART varchar(20) CHARACTER SET UTF8                           ,
      WH varchar(3) CHARACTER SET UTF8                           )
   as
declare variable fromdate date;
declare variable todate date;
declare variable statements varchar(1024);
begin
  if (ordtype = '' or ordtype is null) then
    exception PREPRODUCTION_ERROR 'Wymagany typ zlecenia produkcyjnego';
  if (ordreg = '' or ordreg = ';' or ordreg is null) then
    exception PREPRODUCTION_ERROR 'Wymagany rejestr zlecenia produkcyjnego';
  if (fromdates = '') then
    fromdate = null;
  else
    fromdate = cast(fromdates as date);
  if (todates = '') then
    todate = null;
  else
    todate = cast(todates as date);
  if (ordlist = '' or ordlist = ';') then ordlist = null;
  else if (ordlist is not null) then
    ordlist = replace(ordlist,';',',');
  if (client = 0) then client = null;
  if (mpktm = '') then mpktm = null;
  statements = 'select n.ref, n.id, p.ref, p.ktm, p.wersjaref ';
  statements = statements||'from nagzam n left join pozzam p on (p.zamowienie = n.ref) ';
  statements = statements||'where n.typzam = '''||:ordtype||''' and position('';''||n.rejestr||'';'' in '''||:ordreg||''') > 0 ';
  statements = statements||'and p.out = 1 and n.kpopprzam = 0 and n.anulowano = 0 and p.anulowano = 0 and (';
  if (ordlist is not null) then
    statements = statements||'n.id in ('''||:ordlist||''')) or (1 = 1 and ';
  if (client is not null) then
    statements = statements||'n.klient = '||:client||' and ';
  if (fromdate is not null) then
    statements = statements||'n.datawe >= '''||:fromdate||''' and ';
  if (todate is not null) then
    statements = statements||'n.datawe <= '''||:todate||''' and ';
  if (mpktm is not null) then
    statements = statements||'p.ktm like ''%'''||:mpktm||' and ';
  statements = statements||'1 = 1)';
  for
    execute statement statements
      into prnagzam, prid, prpozzam, ktm, wersjaref
  do begin
    for
      select s.ref, s.prshmat, s.amount - s.amountzreal,
          t.nazwa, m.grossquantity, j.jedn, o.workplace, s.warehouse, o.prdepart
        from prschedguidespos s
          left join towary t on (t.ktm = s.ktm)
          left join prshmat m on (m.ref = s.prshmat)
          left join towjedn j on (j.ref = m.jedn)
          left join prschedopers o on (o.ref = s.prschedoper)
        where s.pozzamref = :prpozzam and s.out = 1 and s.autodocout = 0
        into prschedguidepos, prshmat, ilosc,
            nazwat, iloscmat, miara, workplace, wh, prdepart
    do begin
      if (ilosc > 0) then
        suspend;
    end
  end
end^
SET TERM ; ^
