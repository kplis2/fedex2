--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_PRSCHEDORDER01(
      SCHEDULE integer)
   as
declare variable num integer;
declare variable schedzam integer;
begin
  num = 1;
  update prschedzam set PRSCHEDZAM.number = null where PRSCHEDULE = :schedule;
  for select PRSCHEDZAM.REF
    from PRSCHEDZAM join NAGZAM on (PRSCHEDZAM.zamowienie = nagzam.ref)
    where prschedzam.prschedule = :schedule
    order by nagzam.termstartdecl, nagzam.ref
  into :schedzam
  do begin
    update PRSCHEDZAM set NUMBER = :num where ref=:schedzam;
    num = num + 1;
  end
end^
SET TERM ; ^
