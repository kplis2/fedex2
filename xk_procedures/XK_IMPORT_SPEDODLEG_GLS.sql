--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_IMPORT_SPEDODLEG_GLS(
      SPOSDOST integer,
      LINIA varchar(255) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable kodp varchar(10);
declare variable kraj varchar(10);
declare variable kodrejonu varchar(10);
declare variable trasa varchar(10);
declare variable wahadlo varchar(10);
declare variable kieruneksort varchar(10);
declare variable dziesiata smallint;
declare variable sobota smallint;
declare variable rou varchar(3);
declare variable dane varchar(20);
begin
  status = 0;
  msg = '';

  rou = trim(substring(:linia from 1 for 3));
  if (:rou = 'ROU') then begin
    kraj = trim(substring(:linia from 4 for 5));
    kodp = trim(substring(:linia from 7 for 11));
    dane = trim(substring(:linia from 14 for 25));

    kodrejonu = substring(:dane from 1 for 3);
    trasa = substring(:dane from 4 for 7);
    wahadlo = substring(:dane from 8 for 8);
    kieruneksort = substring(:dane from 9 for 9);
    if (substring(:dane from 10 for 10) = 'J') then dziesiata = 1;
    else dziesiata = 0;
    if (substring(:dane from 12 for 12) = 'J') then sobota = 1;
    else sobota = 0;

    insert into spedodleg(kodp,kodrejonu,rejonsped,sposdost,krajid,wahadlo,kieruneksort,trasa,s10,ssobota)
      values(:kodp,:kodrejonu,:kodp,:sposdost,:kraj,:wahadlo,:kieruneksort,:trasa,:dziesiata,:sobota);
    msg = 'Dla kodu '||:kodp||' import poprawny';
  end
  else begin
    status = 2;
    msg = 'Linia nie zawierała specyfikacji odległości.';
  end
  suspend;
end^
SET TERM ; ^
