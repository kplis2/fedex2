--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSORD_CHECK_STOCK(
      MWSORDREF integer)
  returns (
      STATUS smallint)
   as
begin
  select status from mwsords where ref = :mwsordref
    into status;
  if (status = 5) then
    status = 1;
  else
    status = 0;
end^
SET TERM ; ^
