--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RK_GEN_ACC(
      I_DOC integer,
      I_POZOPER integer)
  returns (
      ACCOUNT varchar(20) CHARACTER SET UTF8                           )
   as
declare variable i_dictdef integer;
declare variable i_dictpos integer;
declare variable s_klient varchar(20);
declare variable s_oddzial varchar(20);
declare variable s_account varchar(80);
declare variable i integer;
declare variable ksiegowane smallint;
begin
  select slodef, slopoz from rkdoknag where ref=:i_doc
    into :i_dictdef, :i_dictpos;
  if (i_dictdef is not null) then begin
    execute procedure KODKS_FROM_DICTPOS(:i_dictdef, :i_dictpos)
      returning_values :s_klient;
  end
  select ksiegowane, konto from rkpozoper where ref=:i_pozoper
    into :ksiegowane, :s_account;

  if (ksiegowane =1) then
  begin
    if (s_oddzial is null) then
      select O.symbol
        from rkdoknag DN
        join rkrapkas RK on (DN.raport = RK.ref and DN.ref=:i_doc)
        join rkstnkas STN on (RK.stanowisko = STN.kod)
        join oddzialy O on (O.oddzial = STN.oddzial)
       into :s_oddzial;


    account = s_account;
    if (s_account is not null) then
    begin
      account = replace(account,  'PRACOWNIK',  s_klient);
      account = replace(account,  'KLIENT',  s_klient);
      account = replace(account,  'KONTRAHENT',  s_klient);
      account = replace(account,  'ODDZIAL',  s_oddzial);
    end
    if (account is null or account='') then
      account = '???';
    account = substring(account from 1 for 20);
  end
  suspend;
end^
SET TERM ; ^
