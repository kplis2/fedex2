--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GOOD_ON_MWSORD(
      MWSORDREF integer,
      TRYB smallint)
  returns (
      REF integer,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      WNAZWA varchar(255) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4),
      LEFTTOREALIZE varchar(20) CHARACTER SET UTF8                           )
   as
declare variable REALIZED numeric(14,4);
declare variable DOCID integer;
declare variable DOCTYPE varchar(1);
declare variable leftr numeric(14,4);
declare variable leftri integer;
declare variable leftrn numeric(14,2);
declare variable outcor smallint;
begin
  if (tryb = 0) then
  begin
    for
      select distinct(ma.vers), ma.good, t.nazwa||' '||coalesce(t.x_nazwa2,''), ma.docid, ma.doctype, w.nazwa
        from mwsacts ma
          join wersje w on(ma.vers = w.ref)
          left join towary t on(ma.good = t.ktm)
        where ma.mwsord = :mwsordref
        into :ref, :ktm, :nazwa, docid, doctype, :wnazwa
    do begin
      if (doctype = 'M') then
        select coalesce(sum(case when d.mwsdisposition = 1 then p.ilosc else p.iloscl end),0)
          from dokumpoz p
          left join dokumnag d on (d.ref = p.dokument)
          where p.wersjaref = :ref
            and p.dokument = :docid
          into :quantity;
      else if (doctype = 'Z') then
        select coalesce(sum(ilosc),0)
          from pozzam
          where wersjaref = :ref
            and zamowienie = :docid
          into :quantity;
      if (quantity is null) then quantity = 0;
      select coalesce(sum(ma.quantity),0)
        from mwsacts ma
        where ma.vers = :ref
          and ma.mwsord = :mwsordref and ma.status in (2,5)
        into :realized;
      leftr = quantity - realized;
      if (leftr < 0 and leftr = cast(leftr as integer)) then
        lefttorealize = '('||abs(cast(leftr as integer))||')';
      else if (leftr < 0 and leftr = cast(leftr as numeric(14,2))) then
        lefttorealize = '('||abs(cast(leftr as numeric(14,2)))||')';
      else if (leftr > 0 and leftr = cast(leftr as integer)) then
        lefttorealize = '('||cast(leftr as integer)||')';
      else if (leftr > 0 and leftr = cast(leftr as numeric(14,2))) then
        lefttorealize = '('||cast(leftr as numeric(14,2))||')';
      else
        lefttorealize = '0';
      suspend;
    end
  end
  if (tryb = 1) then
  begin
    select d.koryg
      from mwsords o
        left join dokumnag d on (d.ref = o.docid)
      where o.ref = :mwsordref
      into outcor;
    if (outcor is null) then outcor = 0;
    if (outcor = 0) then
    begin
      for
        select distinct p.vers, p.good, t.nazwa||' '||coalesce(t.x_nazwa2,''), p.docid, w.nazwa
          from mws_goods_on_mwsord(:mwsordref) p
            left join towary t on (t.ktm = p.good)
            left join wersje w on (w.ref = p.vers)
          into :ref, :ktm, :nazwa, docid, :wnazwa
      do begin
        quantity = 0;
        realized = 0;
        select coalesce(sum(case when d.mwsdisposition = 1 then p.ilosc else p.iloscl end),0)
          from dokumpoz p
          left join dokumnag d on (d.ref = p.dokument)
          where p.wersjaref = :ref
            and p.dokument = :docid
          into :quantity;
        select coalesce(sum(ma.quantity),0)
          from mwsacts ma
          where ma.vers = :ref
            and ma.mwsord = :mwsordref and ma.status in (2,5)
          into :realized;
        leftr = quantity - realized;
        leftri = abs(cast(leftr as integer));
        leftrn = abs(cast(leftr as numeric(14,2)));
        if (leftr < 0 and leftr = cast(leftr as integer)) then
          lefttorealize = '('||leftri||')';
        else if (leftr < 0 and leftr <> cast(leftr as integer)) then
          lefttorealize = '('||leftrn||')';
        else if (leftr > 0 and leftr = cast(leftr as integer)) then
          lefttorealize = leftri;
        else if (leftr > 0 and leftr = cast(leftr as integer)) then
          lefttorealize = leftrn;
        else
          lefttorealize = '0';
        if (leftr <> 0) then
          suspend;
      end
    end else if (outcor = 1) then
    begin
      for
        select a.vers, a.good, w.nazwat||' '||coalesce(t.x_nazwa2,''), a.docid, w.nazwa, sum(a.quantity)
          from mwsacts a
            left join wersje w on (w.ref = a.vers)  
            left join towary t on (t.ktm = a.good)
          where a.mwsord = :mwsordref and a.status = 0
          group by a.mwsord, a.vers, a.good, w.nazwat||' '||coalesce(t.x_nazwa2,''), a.docid, w.nazwa
          having sum(a.quantity) <> 0
          into :ref, :ktm, :nazwa, docid, :wnazwa, :quantity
      do begin
        if (quantity is null) then quantity = 0;
        realized = 0;
        select coalesce(sum(ma.quantity),0)
          from mwsacts ma
          where ma.vers = :ref
            and ma.mwsord = :mwsordref and ma.status in (2,5)
          into :realized;
        leftr = quantity - realized;
        leftri = abs(cast(leftr as integer));
        leftrn = abs(cast(leftr as numeric(14,2)));
        if (leftr < 0 and leftr = cast(leftr as integer)) then
          lefttorealize = '('||leftri||')';
        else if (leftr < 0 and leftr <> cast(leftr as integer)) then
          lefttorealize = '('||leftrn||')';
        else if (leftr > 0 and leftr = cast(leftr as integer)) then
          lefttorealize = leftri;
        else if (leftr > 0 and leftr = cast(leftr as integer)) then
          lefttorealize = leftrn;
        else
          lefttorealize = '0';
        if (leftr <> 0) then
          suspend;
      end

    end
  end
end^
SET TERM ; ^
