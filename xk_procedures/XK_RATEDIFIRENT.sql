--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RATEDIFIRENT(
      RKDOKNAG integer)
   as
declare variable stanowisko varchar(2);
declare variable data timestamp;
declare variable pozoperplus integer;
declare variable pozoperminus integer;
declare variable stanminus numeric(14,2) ;
declare variable stanwalminus integer;
declare variable kursminus numeric(14,4) ;
declare variable stanplus numeric(14,2) ;
declare variable stanwalplus integer;
declare variable kursplus numeric(14,4) ;
declare variable toinsert numeric(14,2) ;
begin
  select n.stanowisko, n.data
    from rkdoknag n
    where n.ref = :rkdoknag
    into :stanowisko, :data;

  pozoperplus = 58;
  pozoperminus = 58;

  for
    select -s.stan, s.kurs, s.ref
      from rkstanwal s
      where s.rkstnkas = :stanowisko
        and s.stan < 0
        and s.data <= :data
      order by s.data
      into :stanminus, :kursminus, :stanwalminus
  do begin
    for
      select s.stan, s.kurs, s.ref
        from rkstanwal s
        where s.rkstnkas = :stanowisko
          and s.stan > 0
          and s.data <= :data
        order by s.data
        into :stanplus, :kursplus, :stanwalplus
    do begin
      if (:stanminus > :stanplus) then
        toinsert = stanplus;
      else
        toinsert = stanminus;

      stanminus = stanminus - toinsert;

      if (toinsert > 0) then
      begin
        insert into rkdokpoz (dokument, pozoper, kwota, kurs, rkstanwal)
          values(:rkdoknag, iif (:kursplus > :kursminus, :pozoperplus, :pozoperminus), :toinsert, :kursminus, :stanwalminus);
        insert into rkdokpoz (dokument, pozoper, kwota, kurs, rkstanwal)
          values(:rkdoknag, iif (:kursplus > :kursminus, :pozoperplus, :pozoperminus), -:toinsert, :kursplus, :stanwalplus);
      end
      if (:stanminus = 0 ) then leave;
    end
  end
end^
SET TERM ; ^
