--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_IMPORT_SPEDODLEG(
      SPOSDOST varchar(255) CHARACTER SET UTF8                           ,
      COL0 varchar(255) CHARACTER SET UTF8                           ,
      COL1 varchar(255) CHARACTER SET UTF8                           ,
      COL2 varchar(255) CHARACTER SET UTF8                           ,
      COL3 varchar(255) CHARACTER SET UTF8                           ,
      COL4 varchar(255) CHARACTER SET UTF8                           ,
      COL5 varchar(255) CHARACTER SET UTF8                           ,
      COL6 varchar(255) CHARACTER SET UTF8                           ,
      COL7 varchar(255) CHARACTER SET UTF8                           ,
      COL8 varchar(255) CHARACTER SET UTF8                           ,
      COL9 varchar(255) CHARACTER SET UTF8                           )
  returns (
      STATUS integer,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
begin
/* Statusy
0 - wszystko OK
1 - blad, brak spedytora, import przerwany
2 - blad danych info i leci dalej
*/
  status = 0;
  if (coalesce(:sposdost,'') = '') then begin
    msg = 'Nie wskazano spedytora!';
    status = 1;
    suspend;
    exit;
  end
  if (:col1 is null) then col0 = '';
  if (:col1 is null) then col1 = '';
  if (:col2 is null) then col2 = '';
  if (:col3 is null) then col3 = '';
  if (:col4 is null) then col4 = '';
  if (:col5 is null) then col5 = '';
  if (:col5 is null) then col6 = '';
  if (:col5 is null) then col7 = '';
  if (:col5 is null) then col8 = '';
  if (:col5 is null) then col9 = '';

--Nalezy ustawic spedytorow
  if (:sposdost = 0) then begin
    execute procedure xk_import_spedodleg_schenker (:sposdost, :col0) returning_values :status, :msg;
  end
  else if (:sposdost = 1) then begin
--    execute procedure xk_import_spedodleg_opek (:sposdost, :col0) returning_values :status, :msg;
  end
  else if (:sposdost = 2) then begin
    execute procedure xk_import_spedodleg_gls (:sposdost, :col0) returning_values :status, :msg;
  end

  suspend;
end^
SET TERM ; ^
