--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_DOKUMNAG_GRUPASPED_LIST(
      DOCREF integer)
  returns (
      REF integer,
      GRUPASPED integer,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      SYMBOL varchar(40) CHARACTER SET UTF8                           ,
      GRUPASPEDDESC varchar(40) CHARACTER SET UTF8                           ,
      TERMDOST timestamp,
      X_HERMON_WZ STRING255)
   as
declare variable ODBIORCASPEDID integer;
declare variable SPOSZAP integer;
declare variable FLAGISPED varchar(80);
declare variable AKTUODDZIAL varchar(40);
begin
  -- wersja dla ARGO
  select odbiorcaidsped, sposzap, flagisped, oddzial
    from dokumnag d
    where d.ref = :docref
    into odbiorcaspedid, sposzap, flagisped, aktuoddzial;
  for
    select g.ref, g.grupasped, g.symbol, g.grupaspeddesc, g.termdost, g.magazyn
--<< MatJ
      , g.int_symbol
--MatJ
      from dokumnag g
        left join defdokum d on (d.symbol = g.typ)
        left join sposdost s on (g.sposdost = s.ref)
      where g.oddzial = :aktuoddzial and d.wydania = 1 and d.zewn = 1 and d.koryg = 0
        and g.transport = 1 and g.wysylkadone = 0 and g.REF <> :docref AND g.AKCEPT = 1
        and s.listywys = 1 and g.faktura is null
        and g.odbiorcaidsped = :odbiorcaspedid
        -- and coalesce( g.sposzap,0) = coalesce(:sposzap,0)
        and (g.flagisped = :flagisped or :flagisped is null or :flagisped = ''
          or g.flagisped is null or g.flagisped = '')
        and g.grupasped = g.ref
      into ref, grupasped, symbol, grupaspeddesc, termdost, magazyn, X_HERMON_WZ
  do begin
    suspend;
  end
end^
SET TERM ; ^
