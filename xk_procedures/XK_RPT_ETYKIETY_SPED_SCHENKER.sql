--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_ETYKIETY_SPED_SCHENKER(
      LISTYWYSDREF integer,
      SPOSDOSTREF integer)
  returns (
      RSYMBOLDOK varchar(255) CHARACTER SET UTF8                           ,
      RSYMBOLSPED varchar(255) CHARACTER SET UTF8                           ,
      RNAZWAODB1 varchar(255) CHARACTER SET UTF8                           ,
      RNAZWAODB2 varchar(255) CHARACTER SET UTF8                           ,
      RMIASTOODB varchar(255) CHARACTER SET UTF8                           ,
      RADRESODB varchar(255) CHARACTER SET UTF8                           ,
      RKODPODB varchar(255) CHARACTER SET UTF8                           ,
      RTELEFONODB varchar(255) CHARACTER SET UTF8                           ,
      RPOCZTAODB varchar(255) CHARACTER SET UTF8                           ,
      RDATAAKC timestamp,
      RUWAGISPED varchar(255) CHARACTER SET UTF8                           ,
      RILOSCPACZEK integer,
      RILOSCPACZEKSTR varchar(10) CHARACTER SET UTF8                           ,
      RILOSCPALET integer,
      RILOSCPALETSTR varchar(10) CHARACTER SET UTF8                           ,
      RNRPACZKI integer,
      RNRPACZKISTR varchar(10) CHARACTER SET UTF8                           ,
      RWAGA numeric(15,4),
      RPOBRANIE numeric(14,2),
      RKODREJONUODB varchar(3) CHARACTER SET UTF8                           ,
      RKODKRAJUODB varchar(20) CHARACTER SET UTF8                           ,
      RWAHADLO varchar(3) CHARACTER SET UTF8                           ,
      RKIERUNEKSORT varchar(3) CHARACTER SET UTF8                           ,
      RTRASA varchar(10) CHARACTER SET UTF8                           ,
      RS10 smallint,
      RSSOBOTA smallint,
      RSZWROTDOK smallint,
      RSYMBOLREF varchar(255) CHARACTER SET UTF8                           ,
      RSYMBOLPACZKI varchar(255) CHARACTER SET UTF8                           ,
      RWAGAPACZKI numeric(15,4),
      RKODKRESK varchar(255) CHARACTER SET UTF8                           ,
      RCYFRACTRL smallint,
      RPOBRANIEPRT smallint)
   as
declare variable slodefref integer;
declare variable slopozref integer;
declare variable flagisped varchar(255);
declare variable nazwaodb varchar(255);
declare variable miastoodb varchar(255);
declare variable wysylkapal smallint;
begin
  --sprawdzenie czy wyslka paletowa
  wysylkapal = 0;
  if (exists(select first 1 1 from listywysdroz_opk where listwysd = :listywysdref and typ = 1)) then
    wysylkapal = 1;
  else wysylkapal = 0;

  select lwd.symbol, lwd.symbolsped,
         lwd.kontrahent, lwd.miasto, lwd.adres, lwd.kodp,
         lwd.iloscpaczek, lwd.iloscpalet, lwd.waga, lwd.pobranie,
         lwd.dataakc, substring(lwd.uwagisped from 1 for 255),
         lwd.slodef, lwd.slopoz, lwd.flagisped
    from listywysd lwd
    where lwd.ref = :listywysdref
  into :rsymboldok, :rsymbolsped,
       :nazwaodb, :rmiastoodb, :radresodb, :rkodpodb,
       :riloscpaczek, :riloscpalet, :rwaga, :rpobranie,
       :rdataakc, :ruwagisped,
       :slodefref, :slopozref, :flagisped;

  if (coalesce(:rpobranie, 0) > 0) then rpobranieprt = 1;
  else rpobranieprt = 0;

  if (coalesce(:radresodb, '') = '' or coalesce(:rmiastoodb, '') = '') then begin
    if (:slodefref = 1 and :slopozref is not null) then begin
      select k.ulica, k.miasto, k.kodp, k.poczta, k.telefon
        from klienci k where ref = :slopozref
      into :radresodb, :miastoodb, :rkodpodb, :rpocztaodb, :rtelefonodb;
    end
    else if (:slodefref = 6 and :slopozref is not null) then begin
      select d.ulica, d.miasto, d.kodp, d.poczta, d.telefon
        from dostawcy d where ref = :slopozref
      into :radresodb, :miastoodb, :rkodpodb, :rpocztaodb, :rtelefonodb;
    end

    if (:miastoodb <> :rpocztaodb) then radresodb = :radresodb||', '||:miastoodb;
  end

  execute procedure xk_usun_znaki(:nazwaodb, 1) returning_values :nazwaodb;
  rnazwaodb1 = substring(:nazwaodb from 1 for 42);
  rnazwaodb2 = substring(:nazwaodb from 43 for 84);

  execute procedure xk_usun_znaki(radresodb, 1) returning_values :radresodb;
  execute procedure xk_usun_znaki(:rmiastoodb, 1) returning_values :rmiastoodb;
  execute procedure xk_usun_znaki(:rkodpodb, 0) returning_values :rkodpodb;
  execute procedure xk_usun_znaki(:ruwagisped, 1) returning_values :ruwagisped;

  select spo.kodrejonu, spo.krajid, spo.wahadlo, spo.kieruneksort, spo.trasa, spo.s10, spo.ssobota
    from spedodleg spo
    where spo.spedytor = :sposdostref
      and spo.kodp = :rkodpodb
  into :rkodrejonuodb, :rkodkrajuodb, :rwahadlo, :rkieruneksort, :rtrasa, :rs10, :rssobota;

    if (strmulticmp(:flagisped,';S;')>0 and coalesce(:rssobota, 0) = 1) then rssobota = 1;
    else rssobota = 0;
    if (strmulticmp(:flagisped,';D;')>0 and coalesce(:rs10, 0) = 1) then rs10 = 1;
    else rs10 = 0;
    if (strmulticmp(:flagisped,';Z;')>0) then rszwrotdok = 1;
    else rszwrotdok = 0;

    if (substring(:rkodrejonuodb from 1 for 1) = '0') then rkodrejonuodb = substring(:rkodrejonuodb from 2 for coalesce(char_length(:rkodrejonuodb), 0)); -- [DG] XXX ZG119346
--    rkodrejonuodb = :rtrasa;
--    rrejonspedodb = :kodrejonu;

    --dokumenty REF
    select first 1 symbol
      from nagfak where listywysd = :listywysdref
      order by ref
    into :rsymbolref;
    if (:rsymbolref is null) then begin
      select first 1 dn.symbol
        from dokumnag dn
          join listywysdpoz lwp on (dn.ref = lwp.dokref)
        where lwp.dokument = :listywysdref
        order by dn.ref
      into :rsymbolref;
    end

     for select lwo.nrpaczki, lwo.waga
          from listywysdroz_opk lwo
          where lwo.listwysd = :listywysdref
            and lwo.rodzic is null
          order by lwo.nrpaczki
        into :rsymbolpaczki, :rwagapaczki
    do begin
      rkodkresk = '';
      rnrpaczki = :rnrpaczki + 1;

      execute procedure GET_SYMBOL_FROM_NUMBER(:riloscpalet, 3) returning_values :riloscpaletstr;
      execute procedure GET_SYMBOL_FROM_NUMBER(:riloscpaczek, 3) returning_values :riloscpaczekstr;
      execute procedure GET_SYMBOL_FROM_NUMBER(:rnrpaczki, 3) returning_values :rnrpaczkistr;

      rkodkresk = :rkodkresk||:rsymbolpaczki;
      execute procedure xk_cyfractrl_schenker(:rkodkresk) returning_values :rcyfractrl;
      rkodkresk = :rkodkresk||:rcyfractrl;

      rsymbolpaczki = :rsymbolpaczki||:rcyfractrl;

      rkodkresk = :rkodkresk||:rnrpaczkistr;
      rkodkresk = :rkodkresk||:riloscpaczekstr;
      execute procedure xk_cyfractrl_schenker(:rkodkresk) returning_values :rcyfractrl;
      rkodkresk = :rkodkresk||:rcyfractrl;
      rkodkresk = 'N'||:rkodkresk;

      suspend;
      rpobranie = 0;
    end
end^
SET TERM ; ^
