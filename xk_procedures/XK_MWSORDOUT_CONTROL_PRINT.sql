--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWSORDOUT_CONTROL_PRINT(
      WTREF integer)
  returns (
      DOCGROUP integer,
      DOCSYMBS varchar(255) CHARACTER SET UTF8                           ,
      OPERATOR varchar(255) CHARACTER SET UTF8                           ,
      SPOSDOST varchar(3) CHARACTER SET UTF8                           ,
      FSKROT varchar(255) CHARACTER SET UTF8                           ,
      ODBIORCA varchar(255) CHARACTER SET UTF8                           ,
      ADRES varchar(255) CHARACTER SET UTF8                           )
   as
declare variable status smallint;
declare variable ordtype varchar(3);
declare variable flags varchar(40);
declare variable dref integer;
declare variable autocommit smallint;
declare variable endloc varchar(20);
declare variable orgwtref integer;
begin
  select m.docgroup, substring(m.docsymbs from 1 for 255), substring(s.nazwa from 1 for 3), o.login, m.slokod
    from mwsords m
      left join operator o on (o.ref = m.operator)
      left join sposdost s on (s.ref = m.shippingtype)
    where m.ref = :wtref
    into docgroup, docsymbs, sposdost, operator, fskrot;

  select first 1 o.nazwa, substring(o.dulica from 1 for 80)||', '||o.dkodp||' '||substring(o.dmiasto from 1 for 80)
    from dokumnag dn join odbiorcy o on (dn.odbiorcaidsped = o.ref)
    where dn.grupasped = :docgroup
  into :odbiorca, :adres;
  suspend;
end^
SET TERM ; ^
