--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_BARCODE_VALIDATION(
      BARCODE varchar(100) CHARACTER SET UTF8                           ,
      VERSIN INTEGER_ID = 0)
  returns (
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      QUANTITY numeric(14,4),
      STATUS smallint,
      UNIT varchar(5) CHARACTER SET UTF8                           ,
      UNITID integer)
   as
declare variable PRZELICZ numeric(14,4);
declare variable TYMPRZELICZ numeric(14,4);
declare variable TYMVERS integer;
declare variable CNT integer;
declare variable myslnik smallint_id;
begin
  if (barcode is null or barcode ='0') then begin
    status = 0;  -- XXX KBI Asysty 20190529,  bo jak tego nie byo, zwracakimy status = null a  jeli chcemy zgosić inventaryzacj przy wadawaniu i wpiszemy "0" to dostajemy komunikat ż "Wyumagany kod kreskowy"
    exit;
  end
  --  exception test_break :barcode ||' '||coalesce(versin,'0000');
  -- 133618 -- odczytywanie dziwnych kodow kreskowych
  myslnik = position('-', :BARCODE);
  if (myslnik > 0) then begin --sprawdzamy czy mamy myslnik
    barcode = right(barcode, CHAR_LENGTH(barcode) - myslnik); --obcinamy od myslnika do konca

    if (CHAR_LENGTH(barcode)=7 and left(barcode, 2) = '54') then --jesli skrocony(7) i poczatek 54
      barcode = '5400000' || right(barcode, 5); --dodaj 5400000 i 5 ostatnich znakow
  end

  -- koniec dziwnych kodow kreskowych
  -- weryfikacja czy kod kreskowy jest unikalny !!!
  select count(distinct K.WERSJAREF)
  from TOWKODKRESK K
  left join WERSJE W on (W.REF = K.WERSJAREF)
  where K.KODKRESK = :BARCODE and
        coalesce(char_length(:BARCODE), 0) > 5
  group by K.KODKRESK
  into CNT;
  if (CNT = 1) then
  begin
    select count(distinct K.TOWJEDNREF)
    from TOWKODKRESK K
    left join WERSJE W on (W.REF = K.WERSJAREF)
    where K.KODKRESK = :BARCODE and
          coalesce(char_length(:BARCODE), 0) > 5
    group by K.KODKRESK
    into CNT;
  end
  if (CNT > 1 and
      (VERSIN = 0 or VERSIN is null)) then
  begin
    --delete from towkodkresk k where k.kodkresk = :barcode and coalesce(char_length(:barcode),0) > 5; --[PM] XXX no nieee
    exception UNIVERSAL 'W systemie istnieje wiecej niz jeden towar o tym kodzie!';
    STATUS = 0;
    suspend;
    exit;
  end
  if (VERSIN is null or
      VERSIN = 0) then
  begin
    select first 1 T.KTM, O.PRZELICZ, T.WERSJAREF, O.JEDN, O.REF
    from TOWKODKRESK T
    left join TOWJEDN O on (O.REF = T.TOWJEDNREF)
    where T.KODKRESK = :BARCODE and
          coalesce(char_length(:BARCODE), 0) > 5
    into GOOD, QUANTITY, VERS, UNIT, UNITID;
    if (GOOD is not null and
        VERS is null) then
    begin
      select first 1 REF
      from WERSJE W
      where W.AKT = 1 and
            W.KTM = :GOOD and
            coalesce(char_length(:BARCODE), 0) > 5
      order by NRWERSJI
      into VERS;
    end
    if (GOOD is null and
        position('/' in :BARCODE) > 0) then
    begin
      TYMVERS = cast(substring(:BARCODE from 1 for position('/' in :BARCODE) - 1) as integer);
      TYMPRZELICZ = cast(substring(:BARCODE from position('/' in :BARCODE) + 1 for coalesce(char_length(:BARCODE), 0)) as numeric(15,4));
      select first 1 W.REF, W.KTM, O.REF, O.JEDN
      from WERSJE W
      left join TOWJEDN O on (O.KTM = W.KTM and
            O.PRZELICZ = :TYMPRZELICZ)
      where W.REF = :TYMVERS
      into VERS, GOOD, UNITID, UNIT;
      QUANTITY = :TYMPRZELICZ;
    end
  /*
   if (:GOOD is null) then
    begin
      select first 1 W.REF, W.KTM, O.REF, O.JEDN, O.PRZELICZ
      from WERSJE W
      left join TOWJEDN O on (O.KTM = W.KTM and
            O.GLOWNA = 1)
      where W.KODPROD = :BARCODE
      into VERS, GOOD, UNITID, UNIT, :QUANTITY;
    end

    -- [DG] XXX Logbox: nowe okno do pakowania: jezeli dostaje kod w postaci 'KTM:ABCD',
    -- to trzeba szukac towaru po KTMie 'ABCD'
    if (:GOOD is null and
        :BARCODE starting with 'KTM:') then
    begin
      select first 1 W.REF, W.KTM, O.REF, O.JEDN, O.PRZELICZ
      from WERSJE W
      left join TOWJEDN O on (O.KTM = W.KTM and
            O.GLOWNA = 1)
      where W.KTM = substring(:BARCODE from 5)
      into VERS, GOOD, UNITID, UNIT, :QUANTITY;
    end  */
    -- [DG] XXX end
  end
  else
  begin --dla znanej wersji
    VERS = VERSIN;
    select first 1 W.KTM, O.REF, O.JEDN, O.PRZELICZ
      from WERSJE W
      left join TOWJEDN O on (O.KTM = W.KTM)
      where w.ref = :VERS and
            o.glowna = 1
    into GOOD, UNITID, UNIT, :QUANTITY;
  end

  if (VERS is null) then
    VERS = 0;
  if (:GOOD is not null) then
    STATUS = 1;
  else
    STATUS = 0;
  suspend;
end^
SET TERM ; ^
