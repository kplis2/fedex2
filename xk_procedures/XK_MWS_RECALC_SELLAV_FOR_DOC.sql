--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_RECALC_SELLAV_FOR_DOC(
      DOCID integer,
      WH varchar(3) CHARACTER SET UTF8                           )
   as
declare variable vers integer;
declare variable quantity numeric(14,4);
begin
  for
    select p.wersjaref, dos.ildost
      from dokumpoz p
        join dokumnag d on (d.ref = p.dokument)
        join XK_STANYIL_DOSTEPNE_WMS (:docid,:wh) dos on (dos.wersjaref = p.wersjaref and dos.magazyn = d.magazyn)
      where p.dokument = :docid
      into vers, quantity
  do begin
    if (not exists (select q.ref from mwsversq q where q.wh = :wh and q.vers = :vers)) then
      insert into mwsversq (vers, wh, quantity) values(:vers,:wh,:quantity);
    else
      update mwsversq set quantity = :quantity where wh = :wh and vers = :vers and quantity <> :quantity;
  end
end^
SET TERM ; ^
