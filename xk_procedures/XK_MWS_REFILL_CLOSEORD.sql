--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_REFILL_CLOSEORD(
      MWSORD integer)
  returns (
      STATUS smallint,
      MWSCONSTLOC varchar(20) CHARACTER SET UTF8                           )
   as
declare variable mwsactref integer;
begin
  -- status = 0 mozna zamknac zlecenie
  -- status = 1 pozycje do odstawienia na lokacje poczatkowa
  -- status = 2 niepotwierdzone pozycje
  select first 1 ma.ref
    from mwsacts ma
    where ma.mwsord = :mwsord
      and (ma.mwsconstlocp <> ma.mwsconstlocl or ma.mwsconstlocl is null)
      and ma.status < 5
    into :mwsactref;
  if (mwsactref is not null) then begin
--    update mwsacts ma set ma.status = 0 where ma.mwsord = :mwsord and ma.status = 1;
--    update mwsacts ma set ma.mwsconstlocl = ma.mwsconstlocp, ma.mwspallocl = ma.mwspallocp where ma.mwsord = :mwsord and ma.status = 0;
    mwsconstloc = '';
    status = 2;
    suspend;
    exit;
  end
  select first 1 ma.ref, m.symbol
    from mwsacts ma
      left join mwsconstlocs m on(ma.mwsconstlocp = m.ref)
    where ma.mwsord = :mwsord
      and ma.mwsconstlocp = ma.mwsconstlocl
      and ma.status < 5
    into :mwsactref, :mwsconstloc;
  if (mwsactref is not null) then begin
    status = 1;
    suspend;
    exit;
  end
  status = 0;
  suspend;
end^
SET TERM ; ^
