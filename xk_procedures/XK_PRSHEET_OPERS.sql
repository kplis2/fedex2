--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_PRSHEET_OPERS(
      PRSHEET integer,
      PROPER varchar(40) CHARACTER SET UTF8                           ,
      REPORTED smallint)
  returns (
      NAMEOUT varchar(255) CHARACTER SET UTF8                           ,
      REFOUT varchar(1024) CHARACTER SET UTF8                           ,
      PROPEROUT varchar(255) CHARACTER SET UTF8                           ,
      NUMBEROUT integer)
   as
declare variable opermaster integer;
declare variable opertype integer;
declare variable defaultoper integer;
begin
  for
    select REF,OPERMASTER,NUMBER,DESCRIPT,OPER
      from prshopers
      where sheet=:prsheet and complex = 0
        and (oper = :proper or :proper is null)
        and (reported = :reported or :reported is null)
      order by opermaster,number
      into :refout,:opermaster,:numberout,:nameout,:properout
  do begin
    defaultoper = null;
    opertype = null;
    if(:opermaster is not null) then
      select opertype, operdefault
        from prshopers
        where ref = :opermaster
        into opertype, defaultoper;
    if (opermaster is null or (opermaster is not null and opertype = 1 and defaultoper = refout)
        or (opertype is not null and opertype <> 1)
    ) then
      suspend;
    opermaster = null;
  end
end^
SET TERM ; ^
