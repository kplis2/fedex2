--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GETLOTFROMPARAMS(
      WERSJAREF integer,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      TWORZDOSTAWE smallint,
      PARAMS1 varchar(40) CHARACTER SET UTF8                           ,
      PARAMS2 varchar(40) CHARACTER SET UTF8                           ,
      PARAMS3 varchar(40) CHARACTER SET UTF8                           ,
      PARAMS4 varchar(40) CHARACTER SET UTF8                           ,
      PARAMS5 varchar(40) CHARACTER SET UTF8                           ,
      PARAMS6 varchar(40) CHARACTER SET UTF8                           ,
      PARAMS7 varchar(40) CHARACTER SET UTF8                           ,
      PARAMS8 varchar(40) CHARACTER SET UTF8                           ,
      PARAMS9 varchar(40) CHARACTER SET UTF8                           ,
      PARAMS10 varchar(40) CHARACTER SET UTF8                           ,
      PARAMS11 varchar(40) CHARACTER SET UTF8                           ,
      PARAMS12 varchar(40) CHARACTER SET UTF8                           ,
      PARAMN1 numeric(14,2),
      PARAMN2 numeric(14,2),
      PARAMN3 numeric(14,2),
      PARAMN4 numeric(14,2),
      PARAMN5 numeric(14,2),
      PARAMN6 numeric(14,2),
      PARAMN7 numeric(14,2),
      PARAMN8 numeric(14,2),
      PARAMN9 numeric(14,2),
      PARAMN10 numeric(14,2),
      PARAMN11 numeric(14,2),
      PARAMN12 numeric(14,2),
      PARAMD1 varchar(40) CHARACTER SET UTF8                           ,
      PARAMD2 varchar(40) CHARACTER SET UTF8                           ,
      PARAMD3 varchar(40) CHARACTER SET UTF8                           ,
      PARAMD4 varchar(40) CHARACTER SET UTF8                           )
  returns (
      LOT integer)
   as
declare variable XPARAMN1 numeric(14,4);
declare variable XPARAMN2 numeric(14,4);
declare variable XPARAMN3 numeric(14,4);
declare variable XPARAMN4 numeric(14,4);
declare variable XPARAMN5 numeric(14,4);
declare variable XPARAMN6 numeric(14,4);
declare variable XPARAMN7 numeric(14,4);
declare variable XPARAMN8 numeric(14,4);
declare variable XPARAMN9 numeric(14,4);
declare variable XPARAMN10 numeric(14,4);
declare variable XPARAMN11 numeric(14,4);
declare variable XPARAMN12 numeric(14,4);
declare variable XPARAMS1 varchar(255);
declare variable XPARAMS2 varchar(255);
declare variable XPARAMS3 varchar(255);
declare variable XPARAMS4 varchar(255);
declare variable XPARAMS5 varchar(255);
declare variable XPARAMS6 varchar(255);
declare variable XPARAMS7 varchar(255);
declare variable XPARAMS8 varchar(255);
declare variable XPARAMS9 varchar(255);
declare variable XPARAMS10 varchar(255);
declare variable XPARAMS11 varchar(255);
declare variable XPARAMS12 varchar(255);
declare variable XPARAMD1 timestamp;
declare variable XPARAMD2 timestamp;
declare variable XPARAMD3 timestamp;
declare variable XPARAMD4 timestamp;
declare variable PARTYP integer;
declare variable PARSYMBOL varchar(40);
declare variable PARSKROT varchar(40);
declare variable SYMBKONC varchar(120);
declare variable ISPARAM integer;
declare variable PARAMVAL varchar(255);
declare variable SYMBOLRECZNY varchar(255);
declare variable SKROT varchar(60);--XXX ZG133796 MKD
declare variable DOSTAWCA integer;
declare variable DATA timestamp;
declare variable DOSTSYMBFAK varchar(20);
declare variable SYMBFAK varchar(80);
declare variable SYMBOL varchar(120);
declare variable DATAWAZN timestamp;
declare variable ISRECZNYSYMBOL integer;
begin
  isparam = 0;
  data = current_timestamp(0);
  lot = 0;
  symbkonc = '';

  if (:params1 = '') then params1 = null;
  if (:params2 = '') then params2 = null;
  if (:params3 = '') then params3 = null;
  if (:params4 = '') then params4 = null;
  if (:params5 = '') then params5 = null;
  if (:params6 = '') then params6 = null;
  if (:params7 = '') then params7 = null;
  if (:params8 = '') then params8 = null;
  if (:params9 = '') then params9 = null;
  if (:params10 = '') then params10 = null;
  if (:params11 = '') then params11 = null;
  if (:params12 = '') then params12 = null;
  if (:paramn1 = 0) then paramn1 = null;
  if (:paramn2 = 0) then paramn2 = null;
  if (:paramn3 = 0) then paramn3 = null;
  if (:paramn4 = 0) then paramn4 = null;
  if (:paramn5 = 0) then paramn5 = null;
  if (:paramn6 = 0) then paramn6 = null;
  if (:paramn7 = 0) then paramn7 = null;
  if (:paramn8 = 0) then paramn8 = null;
  if (:paramn9 = 0) then paramn9 = null;
  if (:paramn10 = 0) then paramn10 = null;
  if (:paramn11 = 0) then paramn11 = null;
  if (:paramn12 = 0) then paramn12 = null;
  if (:paramd1 = '') then paramd1 = null;
  if (:paramd2 = '') then paramd2 = null;
  if (:paramd3 = '') then paramd3 = null;
  if (:paramd4 = '') then paramd4 = null;

  select first 1 ref
    from dostawy
    where (:params1 is null or (:params1 is not null and params1 = :params1))
      and (:params2 is null or (:params2 is not null and params2 = :params2))
      and (:params3 is null or (:params3 is not null and params3 = :params3))
      and (:params4 is null or (:params4 is not null and params4 = :params4))
      and (:params5 is null or (:params5 is not null and params5 = :params5))
      and (:params6 is null or (:params6 is not null and params6 = :params6))
      and (:params7 is null or (:params7 is not null and params7 = :params7))
      and (:params8 is null or (:params8 is not null and params8 = :params8))
      and (:params9 is null or (:params9 is not null and params9 = :params9))
      and (:params10 is null or (:params10 is not null and params10 = :params10))
      and (:params11 is null or (:params11 is not null and params11 = :params11))
      and (:params12 is null or (:params12 is not null and params12 = :params12))
      and (:paramn1 is null or (:paramn1 is not null and paramn1 = :paramn1))
      and (:paramn2 is null or (:paramn2 is not null and paramn2 = :paramn2))
      and (:paramn3 is null or (:paramn3 is not null and paramn3 = :paramn3))
      and (:paramn4 is null or (:paramn4 is not null and paramn4 = :paramn4))
      and (:paramn5 is null or (:paramn5 is not null and paramn5 = :paramn5))
      and (:paramn6 is null or (:paramn6 is not null and paramn6 = :paramn6))
      and (:paramn7 is null or (:paramn7 is not null and paramn7 = :paramn7))
      and (:paramn8 is null or (:paramn8 is not null and paramn8 = :paramn8))
      and (:paramn9 is null or (:paramn9 is not null and paramn9 = :paramn9))
      and (:paramn10 is null or (:paramn10 is not null and paramn10 = :paramn10))
      and (:paramn11 is null or (:paramn11 is not null and paramn11 = :paramn11))
      and (:paramn12 is null or (:paramn12 is not null and paramn12 = :paramn12))
--      and (:paramd1 is null or (:paramd1 is not null and paramd1 = cast(:paramd1 as timestamp)))
--      and (:paramd2 is null or (:paramd2 is not null and paramd2 = cast(:paramd2 as timestamp)))
--      and (:paramd3 is null or (:paramd3 is not null and paramd3 = cast(:paramd3 as timestamp)))
--      and (:paramd4 is null or (:paramd4 is not null and paramd4 = cast(:paramd4 as timestamp)))
  into :lot;

  if (coalesce(:lot,0) = 0) then
  begin
    for
      select PARTYP,PARSYMBOL,PARSKROT
        from GET_PARAMS_FOR_KTM(:wersjaref,'M',1)
        where PARTYP>1
      into :partyp, :parsymbol, :parskrot
    do begin
      isparam = 1;
      if(:parsymbol='PARAMN1') then begin xparamn1 = :paramn1; paramval = cast(:paramn1 as varchar(40)); end
      if(:parsymbol='PARAMN2') then begin xparamn2 = :paramn2; paramval = cast(:paramn2 as varchar(40)); end
      if(:parsymbol='PARAMN3') then begin xparamn3 = :paramn3; paramval = cast(:paramn3 as varchar(40)); end
      if(:parsymbol='PARAMN4') then begin xparamn4 = :paramn4; paramval = cast(:paramn4 as varchar(40)); end
      if(:parsymbol='PARAMN5') then begin xparamn5 = :paramn5; paramval = cast(:paramn5 as varchar(40)); end
      if(:parsymbol='PARAMN6') then begin xparamn6 = :paramn6; paramval = cast(:paramn6 as varchar(40)); end
      if(:parsymbol='PARAMN7') then begin xparamn7 = :paramn7; paramval = cast(:paramn7 as varchar(40)); end
      if(:parsymbol='PARAMN8') then begin xparamn8 = :paramn8; paramval = cast(:paramn8 as varchar(40)); end
      if(:parsymbol='PARAMN9') then begin xparamn9 = :paramn9; paramval = cast(:paramn9 as varchar(40)); end
      if(:parsymbol='PARAMN10') then begin xparamn10 = :paramn10; paramval = cast(:paramn10 as varchar(40)); end
      if(:parsymbol='PARAMN11') then begin xparamn11 = :paramn11; paramval = cast(:paramn11 as varchar(40)); end
      if(:parsymbol='PARAMN12') then begin xparamn12 = :paramn12; paramval = cast(:paramn12 as varchar(40)); end

      if(:parsymbol='PARAMS1') then begin xparams1 = :params1; paramval = :params1; end
      if(:parsymbol='PARAMS2') then begin xparams2 = :params2; paramval = :params2; end
      if(:parsymbol='PARAMS3') then begin xparams3 = :params3; paramval = :params3; end
      if(:parsymbol='PARAMS4') then begin xparams4 = :params4; paramval = :params4; end
      if(:parsymbol='PARAMS5') then begin xparams5 = :params5; paramval = :params5; end
      if(:parsymbol='PARAMS6') then begin xparams6 = :params6; paramval = :params6; end
      if(:parsymbol='PARAMS7') then begin xparams7 = :params7; paramval = :params7; end
      if(:parsymbol='PARAMS8') then begin xparams8 = :params8; paramval = :params8; end
      if(:parsymbol='PARAMS9') then begin xparams9 = :params9; paramval = :params9; end
      if(:parsymbol='PARAMS10') then begin xparams10 = :params10; paramval = :params10; end
      if(:parsymbol='PARAMS11') then begin xparams11 = :params11; paramval = :params11; end
      if(:parsymbol='PARAMS12') then begin xparams12 = :params12; paramval = :params12; end

      if(:parsymbol='PARAMD1') then begin xparamd1 = :paramd1; paramval = cast(:paramd1 as varchar(40)); end
      if(:parsymbol='PARAMD2') then begin xparamd2 = :paramd2; paramval = cast(:paramd2 as varchar(40)); end
      if(:parsymbol='PARAMD3') then begin xparamd3 = :paramd3; paramval = cast(:paramd3 as varchar(40)); end
      if(:parsymbol='PARAMD4') then begin xparamd4 = :paramd4; paramval = cast(:paramd4 as varchar(40)); end
      if(:partyp=3 and :parskrot<>'') then begin
        symbkonc = substring(:symbkonc||'/'||:parskrot||:paramval from 1 for 120);
      end
    end

    if(:symbolreczny is null or (:symbolreczny='')) then begin
      skrot = null;
      select ID from DOSTAWCY where REF = :dostawca into :skrot;
      if(:skrot is null or(:skrot = '')) then skrot = :magazyn;
      if(:skrot is null or(:skrot = '')) then skrot = 'nieznany';
      if(:data is null) then data = current_date;
      symbol = '';
      if(:datawazn is not null) then symbol = substring('*'||cast(:datawazn as date)||'/'||cast(:skrot as varchar(35))||'/'||cast(:data as date)||:symbkonc from 1 for 120);
      else symbol = substring(:skrot || '/'||cast(:data as date)||:symbkonc from 1 for 120);
      isrecznysymbol = 0;
    end
    else begin
      symbol = :symbolreczny;
      isrecznysymbol = 1;
    end


    if(:tworzdostawe=1) then begin
      if(coalesce(:lot,0) = 0) then
        execute procedure GEN_REF('DOSTAWY') returning_values :lot;

      insert into DOSTAWY(REF, SYMBOL, RECZNYSYMBOL, DOSTAWCA, DATA, STATUS, MAGAZYN, DATAWAZN, REFDOKUMNAG, FROMDOKUMPOZ,
                          PARAMN1,PARAMN2,PARAMN3,PARAMN4,
                          PARAMN5,PARAMN6,PARAMN7,PARAMN8,
                          PARAMN9,PARAMN10,PARAMN11,PARAMN12,
                          PARAMS1,PARAMS2,PARAMS3,PARAMS4,
                          PARAMS5,PARAMS6,PARAMS7,PARAMS8,
                          PARAMS9,PARAMS10,PARAMS11,PARAMS12,
                          PARAMD1,PARAMD2,PARAMD3,PARAMD4,ISPARAM)
      values (:lot, :symbol, :isrecznysymbol, :dostawca, :data, 'Z', :magazyn, :datawazn, null, null,
              :xparamn1,:xparamn2,:xparamn3,:xparamn4,
              :xparamn5,:xparamn6,:xparamn7,:xparamn8,
              :xparamn9,:xparamn10,:xparamn11,:xparamn12,
              :xparams1,:xparams2,:xparams3,:xparams4,
              :xparams5,:xparams6,:xparams7,:xparams8,
              :xparams9,:xparams10,:xparams11,:xparams12,
              :xparamd1,:xparamd2,:xparamd3,:xparamd4,:isparam);
    end
  end
end^
SET TERM ; ^
