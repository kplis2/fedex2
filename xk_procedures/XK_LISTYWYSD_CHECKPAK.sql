--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSD_CHECKPAK(
      SPED_DOC integer,
      DOCLOSE smallint)
  returns (
      TOPACK integer,
      MSG STRING255,
      STATUS SMALLINT_ID)
   as
declare variable EXISTING_DEPENDS integer;
declare variable BOX_TO_REMOVE_REF integer;
declare variable REFDOK integer; /* XXX KBI ZG99955 */
declare variable GRUPASPED integer; /* XXX KBI ZG99955 */
declare variable docid integer_id;
declare variable altposmode integer_id;
declare variable nazwa string255;

begin
  topack = 0;
  existing_depends = 0;
  msg = '';
  status = 0;

  select count(*)
    from listywysdpoz lwp
    where lwp.dokument = :sped_doc
      and lwp.iloscmax <> coalesce(lwp.iloscspk,0)
  into :topack;

  if (topack is null) then topack = 0;

  -- --------------------------------------------------------------------------- zamykanie zlecenia
  if (:topack = 0 and doclose = 1) then
  begin
    status = 1;
    for select opk.ref from listywysdroz_opk opk
        where opk.listwysd = :SPED_DOC
          and opk.x_szybko <> 1 -- przy szybkim pakowaniu nie usuwamy 'pustych'
    into :box_to_remove_ref
    do begin
        -- Przy zamykaniu zlecenia usuwamy wszystkie nieużywane kartony i palety --- usuwanie pustycch opak. i palet
        select count(*) from listywysdroz_opk opk where opk.rodzic = :box_to_remove_ref
        into :existing_depends;
                
        if (existing_depends = 0) then
        Begin
            -- brak zaleznosci towaru - czy jakis jest w tym opakowaniu
            select count(*) from listywysdroz lw
            where lw.listywysd = :sped_doc and lw.opakowania LIKE '%;'||:box_to_remove_ref||';%'
            into :existing_depends;
        end
        
        if (existing_depends = 0) then
        begin
            -- brak zaleznosci pozwala usunac calkowicie opakowanie ze zlecenia
            delete from LISTYWYSDROZ_OPK where (REF = :box_to_remove_ref);
        end
    end

    -- XXX Start KBI ZG99955
    select refdok from listywysd where ref = :sped_doc into :refdok;
    if (refdok is not null) then begin
      select grupasped from dokumnag where ref = :refdok into :grupasped;
      update dokumnag set x_mws_packing = 2 where grupasped = :grupasped and x_mws_packing <> 2;
    end
    -- XXX Koniec KBI ZG99955
    update listywysd set akcept = 1 where ref = :sped_doc;
    --- XXX Ldz RW na kartony
    execute procedure x_mws_box_rw(sped_doc)
      returning_values :docid, :msg;
    --- XXX Ldz RW na kartony
  end
  if (:topack >= 1) then begin -- mamy cos nie spakowane
    select first 1 l.altposmode
      from listywysdpoz l
      where l.dokument = :sped_doc
        and coalesce(l.altposmode, 0) <> 0
      group by l.altposmode
      having sum (l.ilosc) <> sum(l.iloscspk) and sum(l.iloscspk) <> 0 -- tylko dla myjni nie spakowanych czesciowo
    into :altposmode;
    if (coalesce(altposmode,  0) <> 0) then begin
      select coalesce( t.nazwa, t.opispod)||' - '||d.ktm
        from dokumpoz d
        join towary t on (d.ktm = t.ktm)
        where d.ref = :altposmode
      into nazwa;

      msg = 'Towar '||coalesce(nazwa, '')||' nie może być dzielony.';
      status = 0;  --blokowanie zamkniecia pakowania ze wzgledu na myjnie
      suspend;
      exit;
    end else
      status = 1;
  end
  suspend;
end^
SET TERM ; ^
