--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GOODSREFILL_CLEAR(
      WHIN varchar(3) CHARACTER SET UTF8                           ,
      MWSORDTYPE integer,
      VERS integer,
      MAXLEVEL integer,
      DEEP integer)
  returns (
      STATUS integer)
   as
declare variable verstmp integer;
declare variable good varchar(40);
declare variable quantity numeric(14,4);
declare variable mwsconstlocp integer;
declare variable mwsconstlocl integer;
declare variable quantitystock numeric(14,4);
declare variable quantityrefill numeric(14,4);
declare variable quantityrefilled numeric(14,4);
declare variable qrefill numeric(14,4);
declare variable toupdate numeric(14,4);
declare variable mwsgoodrefill integer;
declare variable whareab integer;
declare variable priority smallint;
declare variable buffors varchar(100);
declare variable buffor integer;
declare variable wharealogl integer;
declare variable stop smallint;
declare variable goodpriority smallint;
declare variable mwspallocp integer;
declare variable whareap integer;
declare variable wharealogp integer;
declare variable refmwsordout integer;
declare variable mwspallocl integer;
declare variable whareal integer;
declare variable refill integer;
declare variable ordvers integer;
begin
  status = 1;
  priority = 1;
  -- czy wolno brac towar z lokacji niedostepnych dla pickerow "0"
  execute procedure get_config('MWSTAKEFROMBUFFOR',2) returning_values buffors;
  if (buffors is null or buffors = '') then
    buffor = 0;
  else
    buffor = cast(buffors as smallint);
  -- nie sciagamy towaru, jezeli mozna go brac z gory
  if (buffor > 0) then
    exit;
  refmwsordout = null;
  for
    select vers, good, quantity, priority
      from XK_GOODS_REFILL_ORDER(:whin,6)
      into verstmp, good, quantity, goodpriority
  do begin
    mwsconstlocl = null;
    mwsconstlocp = null;
    whareab = null;
    refmwsordout = null;
    -- sprawdzenie czy poprzednie zlecenia nie zestawily juz tego towaru w mixie
    select sum(m.quantity - m.ordered)
      from mwsgoodsrefill m
        left join wersje v on (v.ref = m.vers)
        left join dokumnag d on (d.ref = m.docid)
        left join sposdost s on (s.ref = d.sposdost)
        left join xk_goods_relocate x on (x.vers = m.vers and x.wh = m.wh)
      where d.ref is not null and m.preparegoods = 1 and m.vers = :verstmp and m.wh = :whin
        and exists (select s.ref from mwsstock s join mwsconstlocs l on (l.ref = s.mwsconstloc and l.act > 0)
           where s.vers = m.vers and s.blocked = 0 and (s.x_blocked<>1)) --XXX Ldz ZG 126909
        and not exists(select first 1 xk.mwsconstloc
          from xk_mws_take_full_pal_check(m.wh,d.grupasped,null,0) xk
          group by xk.mwsconstloc)
      into quantity;
    if (quantity is null) then quantity = 0;
    execute procedure XK_MWS_GET_BEST_SOURCE(:GOOD ,:VERSTMP ,null, null, :MWSORDTYPE, :WHIN, null, null, null, null, null, null, null, null, null, null, null, 1, :quantity)
        returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refill);
    if (mwsconstlocp is not null and quantity > 0) then
    begin
      quantitystock = null;
      select first 1 s.mwsconstloc, s.good, s.quantity - s.blocked
        from mwsstock s
          left join mwsconstlocs mc on (mc.ref = s.mwsconstloc)
          left join whareas w on (w.ref = mc.wharea)
          left join whsecs ws on (ws.ref = w.whsec)
          left join dostawy d on (d.ref = s.lot)
        where s.mwsconstloc = :mwsconstlocp and s.vers = :verstmp
          and mc.act > 0 and mc.goodsav = 0 and mc.ref is not null and w.ref is not null
          and :quantity > 0 and (ws.deliveryarea = 2 or ws.deliveryarea = 6)
          and (s.x_blocked<>1)  --XXX Ldz ZG 126909
        into mwsconstlocp, good, quantitystock;
      if (quantitystock is null) then quantitystock = 0;
      if (quantitystock > 0) then
      begin
        quantityrefilled = 0;
        stop = 0;
        -- okreslamy lokacje koncowa
        execute procedure XK_MWS_GET_BEST_LOCATION(:GOOD ,:verstmp ,null, null, :MWSORDTYPE, :whin, null, null, -1, 1, null, null, null, null, null, null, null, 1, :quantitystock)
            returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
        if (mwsconstlocl is not null) then
          execute procedure MWS_MWSCONSTLOC_CHANGE(:mwsordtype,null,:mwsconstlocp,null,:mwsconstlocl,0,null,0,'',:goodpriority)
              returning_values refmwsordout;
        if (refmwsordout is not null and refmwsordout > 0) then
        begin
          -- aktualizacja towarow ze zlecenia
          for
            select a.vers, a.quantity
              from mwsacts a
              where a.mwsord = :refmwsordout and a.quantity > 0
              into ordvers, quantityrefilled
          do begin
            for
              select mg.ref, mg.quantity - mg.ordered
                from mwsgoodsrefill mg
                  left join dokumnag d on (d.ref = mg.docid)
                  left join sposdost s on (s.ref = d.sposdost)
                where mg.preparegoods = 1 and mg.vers = :verstmp
                  and mg.quantity - mg.ordered > 0 and mg.wh = :whin
                into mwsgoodrefill, quantityrefill
            do begin
              if (quantityrefill >= quantityrefilled) then
                toupdate = quantityrefilled;
              else
                toupdate = quantityrefill;
              quantityrefilled = quantityrefilled - toupdate;
              update mwsgoodsrefill set ordered = ordered + :toupdate
                where ref = :mwsgoodrefill;
              if (quantityrefilled <= 0) then
                break;
            end
          end
          if (not exists (select first 1 ref from mwsacts where mwsord = :refmwsordout)) then
          begin
            update mwsords set status = 0 where ref = :refmwsordout;
            delete from mwsords where ref = :refmwsordout;
          end else
          begin
            update mwsords set status = 3 where ref = :refmwsordout;
            update mwsacts set checked = 1 where mwsord = :refmwsordout;
          end
        end
      end
    end
  end
end^
SET TERM ; ^
