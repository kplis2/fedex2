--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_SYM_DOK(
      MWSORDREF integer)
  returns (
      DOKUMENT varchar(60) CHARACTER SET UTF8                           ,
      CONSTLOC integer,
      MWSORDTYPESYMB varchar(20) CHARACTER SET UTF8                           ,
      LASTPOSQUESTION smallint)
   as
declare variable mwsconstloc varchar(40);
begin
  select case when t.mwsortypedets = 15 then mo.docsymbs else mo.symbol end, mo.mwsfirstconstlocp, t.symbol, t.lastposquestion
    from mwsords mo
      join mwsordtypes t on (mo.mwsordtype = t.ref)
    where mo.ref = :mwsordref
    into :dokument, :mwsconstloc, :mwsordtypesymb, :lastposquestion;

  select mc.ref
    from mwsconstlocs mc
    where mc.symbol = :mwsconstloc
    into :constloc;
  suspend;
end^
SET TERM ; ^
