--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_PRPRODUCT_PRICE(
      PRSCHEDGUIDE integer,
      PRSCHEDOPER integer,
      KTM varchar(60) CHARACTER SET UTF8                           ,
      AMOUNT numeric(14,4))
  returns (
      PRICE numeric(14,4))
   as
declare variable wersja integer;
declare variable sheet integer;
declare variable col integer;
BEGIN
  select g.ktm, g.wersja, g.prsheet
    from prschedguides g
    where g.ref = :prschedguide
    into ktm, wersja, sheet;
  execute procedure GET_COSTOFPROD(:ktm, :wersja, :sheet)
    returning_values (:price,:col);
END^
SET TERM ; ^
