--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSORD_OUT_DEVIDE(
      REFMWSORDIN MWSORDS_ID)
   as
declare variable aref mwsacts_id;
declare variable groups string40;
declare variable cnt integer_id;
declare variable refmwsord mwsords_id;
declare variable maxactsonmwsord smallint_id;
declare variable maxcartonmwsord smallint_id;
declare variable maxsingledocpos smallint_id;
declare variable maxweight waga_id;
declare variable maxvolume waga_id;
declare variable mwsordtype mwsordtypes_id;
declare variable multi smallint_id;
declare variable wh defmagaz_id;
declare variable weight waga_id;
declare variable volume waga_id;
declare variable mwsord mwsords_id;
declare variable status smallint_id;
begin
  cnt = 0;

  select mwsordtype, status, wh, multi from mwsords where ref = :refmwsordin
  into :mwsordtype, :status, :wh, :multi;

  select coalesce(m.maxactsonmwsord,0), coalesce(m.maxcartsonmwsord,0),
      coalesce(m.maxweight,0), coalesce(m.maxvolume,0),
      coalesce(m.maxsingledocpos,0)
    from mwsordtypes4wh m
    where m.mwsordtype = :mwsordtype
      and m.wh = :wh
  into :maxactsonmwsord, :maxcartonmwsord,
    :maxweight, :maxvolume,
    :maxsingledocpos;

  for
    select distinct g.whsecgroups
      from mwsacts a
        left join mwsconstlocs c on (c.ref = a.mwsconstlocp)
        left join whsecs s on (s.ref = c.whsec)
        left join whsecgroups g on (position(';'||s.ref||';' in g.whsecgroups) > 0)
      where a.mwsord = :refmwsordin
      order by g.whsecgroups
      into :groups
  do begin
    cnt = cnt + 1;
    if (cnt = 1) then
      update mwsords o set o.whsecgroups = :groups where o.ref = :refmwsordin;
    if (cnt > 1) then
    begin
      for
        select a.ref, a.mwsord, a.weight, a.volume
          from mwsacts a
            left join mwsconstlocs c on (c.ref = a.mwsconstlocp)
            left join whsecs s on (s.ref = c.whsec)
            left join whsecgroups g on (position(';'||s.ref||';' in g.whsecgroups) > 0)
          where a.mwsord = :refmwsordin and g.whsecgroups = :groups
          order by a.number
          into :aref, :mwsord, :weight, :volume
      do begin
        if (:weight is null) then weight = 0;
        if (:volume is null) then volume = 0;
        if (:weight is null) then weight = 0;

        select first 1 o.ref
          from mwsords o
            left join mwsacts a on (o.ref = a.mwsord)
          where o.mwsordtype = :mwsordtype
            and o.wh = :wh
            and o.status in (1,7)
            and o.whsecgroups = :groups
            and o.multi = :multi
            and o.ref <> :mwsord
          group by o.ref
          having count(a.ref) + 1 <= :maxactsonmwsord
            and sum(a.weight) + :weight <= :maxweight
            and sum(a.volume) + :volume <= :maxvolume
          order by o.ref
        into :refmwsord;

        if (:refmwsord is null) then
        begin
          execute procedure gen_ref('MWSORDS') returning_values :refmwsord;
          insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator, priority,
              description, wh,  regtime, timestartdecl, branch, period,
              flags, docsymbs, cor, rec, status, shippingarea, shippingtype, takefullpal,
              slodef, slopoz, slokod, palquantity, repal, whsecgroups, multi)
            select :refmwsord, mwsordtype, stocktaking, doctype, docgroup, docid, operator, priority,
                description, wh,  regtime, timestartdecl, branch, period,
                flags, docsymbs, cor, rec, 0, shippingarea, shippingtype, takefullpal,
                slodef, slopoz, slokod, palquantity, repal, :groups, :multi
              from mwsords where ref = :refmwsordin;
        end

        update mwsacts a set a.status = 0 where a.ref = :aref;
        update mwsacts a set a.mwsord = :refmwsord where a.ref = :aref;
        update mwsacts a set a.status = 1 where a.ref = :aref;
        update mwsords o set o.status = :status where o.ref = :refmwsord;
      end
      refmwsord = null;

      --execute procedure xk_mws_mwsord_route_opt(:refmwsord);
    end
  end

  --execute procedure xk_mws_mwsord_route_opt(:refmwsordin);
end^
SET TERM ; ^
