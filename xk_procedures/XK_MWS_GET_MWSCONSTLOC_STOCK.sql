--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_MWSCONSTLOC_STOCK(
      CONSTLOCSYMBOL varchar(40) CHARACTER SET UTF8                           )
  returns (
      REF INTEGER_ID,
      GOOD KTM_ID,
      SYMBOL STRING40,
      GOODNAME STRING255,
      VERSNAME STRING255,
      BARCODE STRING255,
      QUANTITTY numeric(14,4),
      ORDERED numeric(14,4),
      BLOCKED numeric(14,4),
      AVAILABLE numeric(14,4),
      VOLUME numeric(14,4),
      LOADPERCENT SMALLINT_ID,
      DOSTAWA STRING255,
      PARTIA STRING40,
      SERIAL STRING40)
   as
  declare variable mwsconstlock integer_id;
begin
  REF = 0;
  GOOD = 0;
  SYMBOL = '';
  GOODNAME = '';
  VERSNAME = '';
  BARCODE = '';
  QUANTITTY = 0;
  ORDERED = 0;
  BLOCKED = 0;
  AVAILABLE = 0;
  VOLUME = 0;
  LOADPERCENT = 0;
  DOSTAWA = '';
  PARTIA = '';
  SERIAL = '';

  select m.ref
    from mwsconstlocs m
    where m.symbol = :constlocsymbol
  into :mwsconstlock;
  -- szukanie kartonów na lokacji
  for
    select 'Karton: '|| l.stanowisko, 'Dokument: ', d.symbol, 1
      from listywysdroz_opk l
      join listywysd d on (l.listwysd = d.ref)
      join mwsconstlocs m on (m.symbol = l.x_mwsconstlock)
      where m.ref = :mwsconstlock
    into :good, :symbol, :goodname, :quantitty
   do begin
     suspend;
   end
-- szukanie wózków na lokacji
  for
    select first 1 'Wózek: '|| mwc.name, 'Dokument: ', mo.symbol, 1
      from mwsordcartcolours mw
      join mwsords mo on (mo.docid = mw.docid)
      join mwscartcolours mwc on (mwc.ref = mw.mwscartcolor)
      join mwsconstlocs m on (m.symbol = mw.mwsconstlocsymb)
      where m.ref = :mwsconstlock   and mw.status <2
    into :good, :symbol, :goodname, :quantitty
   do begin
     suspend;
   end


  for
    select w.REF as REF, st.good as GOOD, cl.SYMBOL as SYMBOL, w.NAZWAT as GOODNAME, w.NAZWA as VERSNAME,
        k.KODKRESK as BARCODE,  sum(st.QUANTITY) as QUANTITY,sum(st.ORDERED) as ORDERED, sum(st.BLOCKED) as BLOCKED,
        sum(st.QUANTITY-st.BLOCKED) as AVAILABLE, sum(st.VOLUME) as VOLUME, sum(st.LOADPERCENT) as LOADPERCENT, dd.symbol as DOSTAWA,
        st.X_PARTIA as PARTIA, ms.SERIALNO as SERIAL
    from MWSSTOCK st
    left join MWSCONSTLOCS cl on (st.MWSCONSTLOC = cl.ref)
    left join TOWARY tow on (tow.ktm = st.good)
    left join MWSPALLOCS cs on (st.MWSPALLOC = cs.ref)
    left join WERSJE w on (st.VERS = w.REF)
    left join TOWKODKRESK k on (k.WERSJAREF = w.REF and k.GL = 1)
    left join DOSTAWY dd on (st.lot = dd.REF)
    left join X_MWS_SERIE ms on (ms.ref = st.X_SERIAL_NO)
    where (cl.SYMBOL = :constlocsymbol or cast(cs.ref as varchar(10)) = :constlocsymbol) and st.QUANTITY <> 0
    group by w.REF, st.GOOD, cl.SYMBOL, w.NAZWAT, w.NAZWA, k.KODKRESK, Tow.MWSNAZWA, dd.symbol, st.X_PARTIA, ms.SERIALNO
  into :ref, :good, :symbol, :goodname, :versname, :barcode, quantitty, :ordered, :blocked, :available, :volume, loadpercent, :dostawa, :partia, :serial
  do begin
    if (partia is null) then
      partia = '';
    if (serial is null) then
      serial = '';
    if (dostawa is null) then
      dostawa = '';
    suspend;
  end
end^
SET TERM ; ^
