--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_SHOW_ORDS_ON_CONSTLOC(
      MWSCONSTLOC varchar(20) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint)
   as
declare variable ref integer;
begin
  select first 1 ma.ref
    from mwsconstlocs m
      left join mwsacts ma on(m.ref = ma.mwsconstlocp)
    where m.symbol = :mwsconstloc
      and ma.status > 1 and ma.status < 5
    into :ref;
    if(ref is null) then
      status = 0;
    else 
      status = 1;
  suspend;
end^
SET TERM ; ^
