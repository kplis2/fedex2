--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_FIND_MWSORDREF_VERS(
      MWSORDTYPE MWSORDTYPES_ID,
      DOCGROUP DOKUMNAG_ID,
      DOCID DOKUMNAG_ID,
      DOCPOSID DOKUMPOZ_ID,
      WH DEFMAGAZ_ID,
      WHSEC WHSECS_ID,
      MULTI SMALLINT_ID = 0)
  returns (
      REFMWSORD MWSORDS_ID)
   as
declare variable maxactsonmwsord smallint_id;
declare variable maxcartonmwsord smallint_id;
declare variable maxsingledocpos smallint_id;
declare variable weight waga_id;
declare variable maxweight waga_id;
declare variable volume waga_id;
declare variable maxvolume waga_id;
declare variable cnt smallint_id;
declare variable noconnect smallint_id;
declare variable whsecgroup string10;
begin
--JO: procedura tylko dla wydan
  refmwsord = null;
  whsecgroup = ';'||:whsec||';';

  -- sprawdzam czy istnieje zlecenie dla badanego dokumentu
  select first 1 o.ref
    from mwsords o
      left join mwsordtypes t on (t.ref = o.mwsordtype)
      left join mwsacts a on (a.mwsord = o.ref)
    where o.mwsordtype = :mwsordtype
      and o.status in (1,7)
      and a.docid = :docid
      and o.nextmwsord <> 1
      and coalesce(o.takefullpal,0) = 0
      and (coalesce(o.whsecgroups,'') = '' or coalesce(o.whsecgroups,'') containing :whsecgroup)
    order by o.ref desc
  into :refmwsord;

  -- sprawdzenie czy istnieje inne zlecenie dla grupy magazynowej
  if (:refmwsord is null) then
    select first 1 o.ref
      from mwsords o
        left join mwsacts a on (o.ref = a.mwsord)
      where o.mwsordtype = :mwsordtype
        and o.status in (1,7)
        and a.docgroup = :docgroup
        and a.docgroup <> :docid
        and o.nextmwsord <> 1
        and coalesce(o.takefullpal,0) = 0
        and (coalesce(o.whsecgroups,'') = '' or coalesce(o.whsecgroups,'') containing :whsecgroup)
      order by o.ref desc
    into :refmwsord;

  --znaleziono zlecenie wiec nie ma sensu dluzej szukac
  if (:refmwsord is not null) then
    exit;

  --multipicking
  if (:multi > 0) then
  begin
    select coalesce(m.maxactsonmwsord,0), coalesce(m.maxcartsonmwsord,0),
        coalesce(m.maxweight,0), coalesce(m.maxvolume,0),
        coalesce(m.maxsingledocpos,0)
      from mwsordtypes4wh m
      where m.mwsordtype = :mwsordtype
        and m.wh = :wh
    into :maxactsonmwsord, :maxcartonmwsord,
      :maxweight, :maxvolume,
      :maxsingledocpos;
  
    if (:maxcartonmwsord = 0) then maxcartonmwsord = 4;
    if (:maxactsonmwsord = 0) then maxactsonmwsord = 10000;
    if (:maxweight = 0) then maxweight = 10000;
    if (:maxvolume = 0) then maxvolume = 10000;
    if (:maxsingledocpos = 0) then maxsingledocpos = 10;

    select count(distinct p.wersjaref), sum(p.waga), sum(p.objetosc)
      from dokumnag n
        left join dokumpoz p on (n.ref = p.dokument)
        left join towary t on (p.ktm = t.ktm)
      where p.ref = :docposid
    into :cnt, :weight, :volume;

    if (:cnt is null) then cnt = 0;
    if (:weight is null) then weight = 0;
    if (:volume is null) then volume = 0;
    if (:weight > 0) then
      weight = :weight; --waga w CDN jest w gramach a w Sente w kg

    select first 1 o.ref
      from mwsords o
        left join mwsacts a on (o.ref = a.mwsord)
        left join dokumnag d on (o.docid = d.ref)
      where o.mwsordtype = :mwsordtype
        and o.wh = :wh
        and o.status in (1,7)
        and (select count(distinct a1.docid) from mwsacts a1 where a1.mwsord = o.ref) < :maxcartonmwsord
        and d.zewn = 1
        and d.wydania = 1
        and coalesce(o.multi,0) = :multi
        and (coalesce(o.whsecgroups,'') = '' or coalesce(o.whsecgroups,'') containing :whsecgroup)
      group by o.ref
      having count(a.ref) + :cnt <= case when :multi = 2 then :maxsingledocpos else :multi end
        and sum(a.weight) + :weight <= :maxweight
        and sum(a.volume) + :volume <= :maxvolume
      order by (select count(distinct a1.docid) from mwsacts a1 where a1.mwsord = o.ref) desc
    into :refmwsord;
  end

  suspend;
end^
SET TERM ; ^
