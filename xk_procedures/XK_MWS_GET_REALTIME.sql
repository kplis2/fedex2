--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_REALTIME(
      WHAREALOGB integer,
      WHAREALOGE integer,
      MWSORDTYPE integer,
      MWSORD integer,
      MWSACCESSORY integer,
      MWSACTNUMBER integer,
      GOOD varchar(17) CHARACTER SET UTF8                           ,
      VERS integer,
      QUANTITY numeric(14,4),
      PRIORITY smallint,
      TIMESTART timestamp,
      OPERATOR integer,
      TIMERECALC smallint,
      WH varchar(3) CHARACTER SET UTF8                           ,
      WHAREALOGP integer,
      WHAREALOGL integer,
      REC smallint,
      STOCKTAKING smallint,
      MWSPALLOCP integer,
      MWSPALLOCL integer)
  returns (
      TIMESTARTDCL timestamp,
      TIMESTOPDCL timestamp,
      REALTIME double precision,
      NEWOPERATOR integer,
      NEWMWSACCESSORY integer,
      DIST numeric(14,4))
   as
declare variable tymrealtime double precision;
declare variable pom double precision;
declare variable sec double precision;
declare variable palq numeric(14,4);
declare variable masq numeric(14,4);
declare variable boxq numeric(14,4);
declare variable pacq numeric(14,4);
declare variable pieq numeric(14,4);
declare variable qveryfy varchar(100);
declare variable piecesproc numeric(14,4);
declare variable tymtimestamp timestamp;
declare variable rate double precision;
declare variable tym integer;
begin
  -- obliczenie jednej sekundy w double precision aby mozna ja bylo dodawac do timestampow
  pom = cast(86400 as double precision);
  sec = cast(1/pom as double precision);
  realtime = 0;
  rate = 0;
  -- Pobranie każdej linii WZ do zbiórki
  realtime = realtime + 2 * sec;
  if (priority is null) then priority = 0;
  if (timestart is null) then timestart = current_timestamp(0);
  -- trzeba okreslic czas zakonczenia wszystkich zlecen danego magazyniera aby miec czas start !!!
    -- jezeli zlenie ma priorytet, to liczymy priorytety, jezeli nie to wszystkie z dnia
    select first 1 ma.timestop
      from mwsacts ma
        left join mwsords o on (o.ref = ma.mwsord)
      where ma.priority <= :priority and ma.status < 5 and ma.operator = :operator
        and ma.timestop <= :timestart and ma.timestop > current_date - 1
      order by ma.timestop desc
      into timestartdcl;
  if (timestartdcl is null) then timestartdcl = current_timestamp(0);
  -- sprawdzenie czasu pracy magazynu w danym dniu
  execute procedure MWS_CALENDAR_CHECKSTARTTIME(wh, timestartdcl)
    returning_values(timestart);
  -- obliczenie czasu dojscia do operacji
  -- lub odwiezienie wozka po ostatniej operacji
  execute procedure XK_MWS_GET_TIMETOGET(wharealogb, wharealoge, wharealogp, wharealogl,
      mwsordtype, mwsord, mwsaccessory, mwsactnumber, priority, wh, rec, stocktaking)
    returning_values(tymrealtime, dist);
  realtime = realtime + tymrealtime;
  -- trzeba policzyc ile zajmie naszykowanie jednostki towaru
  -- najpierw okreslamy ile pickow
  -- nie liczymy jezeli jest to ostania operacja
  -- jezeli paleta kocowa i poczatkowa jest ta sama  i nie jest to inwentura to nie liczymy czasów za asortyment
  if (quantity > 0 and ((((mwspallocp <> mwspallocl or (mwspallocl is null and mwspallocp is not null)
      or (mwspallocl is not null and mwspallocp is null) or (mwspallocl is null and mwspallocp is null))
      and stocktaking = 0)) or stocktaking = 1)
  ) then
  begin
    execute procedure XK_MWS_GET_PACKQUANTITIES(good, quantity)
      returning_values (palq, masq, boxq, pacq, pieq, piecesproc);
    -- czasy na poszczególne picki
    if (rec = 1 and stocktaking = 0) then
      realtime = realtime + 450 * sec * palq;
    else if (rec = 0 and stocktaking = 0) then
    begin
      if (palq > 0) then
        realtime = realtime + 5 * sec + 8 * sec * palq;
      if (masq > 0) then
        realtime = realtime + 5 * sec + 5 * sec * masq;
      if (pacq > 0) then
        realtime = realtime + 5 * sec + 5 * sec * boxq;
    end
    -- pobranie kolejnej operacji zlecenia
    execute procedure get_config('MWSORDSQVERYFY',2) returning_values qveryfy;
    -- jezeli nie ma weryfikacji to inne wspolczynniki redukcji
    if (qveryfy = '1' and stocktaking = 0 and rec = 0) then
    begin
      rate = 1;
      --od 3% do 10% -0,1
      if (piecesproc >= 0.03 and  piecesproc <= 0.1) then
        rate = 0.9;
      --od 11% do 30% -0,3
      else if (piecesproc > 0.1 and  piecesproc <= 0.3) then
        rate = 0.7;
      -- od 31% do 60% -0,55
      else if (piecesproc > 0.3 and  piecesproc <= 0.6) then
        rate = 0.45;
      -- od 61% do 86% -0,62
      else if (piecesproc > 0.6 and  piecesproc <= 0.86) then
        rate = 0.38;
      -- od 87% do 99% -0,9
      else if (piecesproc > 0.86 and  piecesproc < 1) then
        rate = 0.1;
        realtime = realtime + 3 * sec * pieq * rate;
    end else if (stocktaking = 0  and rec = 0) then
    begin
      --od 3% do 10% -0,1
      if (piecesproc >= 0.03 and  piecesproc <= 0.1) then
        rate = 0.9;
      --od 11% do 30% -0,15
      else if (piecesproc > 0.1 and  piecesproc <= 0.3) then
        rate = 0.85;
      -- od 31% do 60% -0,15
      else if (piecesproc > 0.3 and  piecesproc <= 0.6) then
        rate = 0.85;
      -- od 61% do 86% -0,2
      else if (piecesproc > 0.6 and  piecesproc <= 0.86) then
        rate = 0.8;
      -- od 87% do 99% -0,2
      else if (piecesproc > 0.86 and  piecesproc < 1) then
        rate = 0.8;
      realtime = realtime + 3 * sec * pieq * rate;
    end
    -- czasy poszczególnych pozycji zlecenia inwentaryzacyjnego
    else if (stocktaking = 1) then
    begin
      realtime = realtime + 3 * sec * pieq;
    end
  end
  -- wspólczynniki redukcji czasu naszykowania towaru
  -- jezeli pracownik pracowal juz ponad 7 godzin to wydluzamymu czasy o 10 %
  select first 1 startat
    from epresencelistnag
    where operator = :operator and cast(startat as date) = current_date
    into tymtimestamp;                                                                 -- 7 godzin --
  if (tymtimestamp is not null and tymtimestamp < timestart and timestart - tymtimestamp < 0.2916) then
    realtime = realtime * 1.1;
  timestopdcl = timestartdcl + realtime;
  newoperator = operator;
end^
SET TERM ; ^
