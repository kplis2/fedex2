--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSDPOZ_ADDING_BOX(
      BOX_CODE STRING40,
      ACTIVE_BOX_CODE STRING40,
      SPED_DOC INTEGER_ID,
      IS_PALLETE SMALLINT_ID,
      OPERATOR INTEGER_ID,
      X_BOX_KTM STRING10,
      SZEROKOSC NUMERIC_14_4 = 0,
      DLUGOSC NUMERIC_14_4 = 0,
      WYSOKOSC NUMERIC_14_4 = 0)
  returns (
      ERR_MSG varchar(255) CHARACTER SET UTF8                           ,
      INFO_MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable box_nr integer; /* Numer nowo dodawanego pudelka (ktore pudelko z kolei otwarte dla danego zlecenia) */
declare variable active_box_is_open smallint; /* Okresla czy aktywne pudelko jest otwarte - bo inaczej nie mozna do niego dodawac (1 - nie, 0 - tak) */
declare variable active_box_ref integer; /* REF aktywnego opakowania */
declare variable box_ref integer; /* REF dodwawanego opakowania */
declare variable box_is_open integer; /* Czy dodawane opakowanie jest otwarte 1 - nie, 0 - tak */
declare variable loop_danger integer; /* Czy wsytepuje proba utworzenia zapetlenia hierarchii */
begin
    ERR_MSG = '';
    INFO_MSG = '';

    if (box_code = active_box_code) then
    begin
       ERR_MSG = 'Nie możesz pakować opakowania w nie samo.';
       suspend;
       exit;
    end


-- REF nowo dodwanego pudla (lub NULL jezeli go jszcze nie ma na liscie)
    select first 1 lw.ref, lw.stan from listywysdroz_opk lw
    where lw.listwysd = :sped_doc and lw.stanowisko = :BOX_CODE
    into :box_ref, :box_is_open;

-- Zabezpieczenie przed dodawaniem do pudel otwartych opakowan (mozna dodawac tylko juz zamkniete)
    /*  --MODYFIKACJE
    if (box_is_open = 0) then
    begin
       ERR_MSG = 'Nie możesz dodawać otwartego opakowania, najpierw je zamknij.';
       suspend;
       exit;
    end   */

-- Czy aktywne pudlo jest otwarte oraz jego REF
    select first 1 lw.stan, lw.ref from listywysdroz_opk lw
    where lw.listwysd = :sped_doc and lw.stanowisko = :ACTIVE_BOX_CODE
    into :active_box_is_open, :active_box_ref;

    if (active_box_ref is null) then
    begin
       ERR_MSG = 'Nie znaleziono aktywnego opakowania - '||:active_box_code||'.';
    end
    /*  --MODYFIKACJE
    else if (active_box_is_open = 1) then   -- gdy opakowanie zamkniete
    begin
       ERR_MSG = 'Najpierw otwórz opakowanie, do którego chcesz dodawać.';
    end */
    else
    begin
        if (box_ref is null) then -- uwaga nie ma jeszcze wpisu dla nowego pudelka
        begin
            insert into listywysdroz_opk(listwysd, stanowisko, stan, rodzic, typ, x_pakowal, x_datapack)
              values(:sped_doc, :box_code, 0, :active_box_ref, :is_pallete, :operator, current_timestamp); -- nowe pudelko
            --INFO_MSG = 'Otwarto nowe opakowanie.'; -- oraz zostaniwe ono normalnie dodane
        end
        --else
        --Begin

        --  przed dodaniem sprawdzamy, czy nie wystepuje próba stworzenia PETLI
        -- (poczatek hierachi zawiera ostatnie pudlo hierarchii)

            select count(*) from xk_listywysd_packing_path(:active_box_ref) proc
            where proc.path like '%;'||:box_ref||';%'
            into: loop_danger;

            if (loop_danger = 0) then
            begin
                update listywysdroz_opk lwo set lwo.rodzic = :active_box_ref, lwo.x_mwsconstlock = :active_box_code, lwo.x_szerokosc = :szerokosc, lwo.x_dlugosc = :dlugosc,
                    lwo.x_wysokosc = :wysokosc, lwo.x_moved = 0, lwo.x_box_ktm = :x_box_ktm,
                    lwo.x_pakowal = :operator, lwo.x_datapack = current_timestamp
                where lwo.listwysd = :sped_doc and lwo.stanowisko = :box_code;
                
                -- update istniejacych danych (towarow) -> wpisy sciezek pakowania
                execute procedure xk_listywysd_packing_path_upd(:box_ref, :sped_doc);
                INFO_MSG = 'Dodano opakowanie.';
            end
            else
            begin
                ERR_MSG = 'Dodanie wskzanego opakowania nie jest możliwe przy aktualnym układzie opakowań.';
            end
      --  End
    end
    suspend;
end^
SET TERM ; ^
