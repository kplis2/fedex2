--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_SUMFAK2(
      NAGFAKREF integer)
  returns (
      WARTNET numeric(14,2),
      WARTBRU numeric(14,2),
      GR_VAT varchar(20) CHARACTER SET UTF8                           ,
      WARTVAT numeric(14,2))
   as
declare variable zaliczkowy integer;
declare variable korekta integer;
declare variable zakup integer;
declare variable walutowa integer;
begin
  select nagfak.zaliczkowy, typfak.korekta, nagfak.zakup, nagfak.walutowa
  from nagfak
  left join typfak on (typfak.symbol=nagfak.typ)
  where nagfak.ref=:nagfakref
  into :zaliczkowy, :korekta, :zakup, :walutowa;
  if(:zakup=1) then walutowa = 0;
  if(:zaliczkowy=0 and :korekta=0) then begin
    /* zwykle pozycje faktury - rozliczenie zaliczek*/
    if(exists(select refk from NAGFAK_GET_ZALICZKI(:nagfakref))) then begin
      for select
        (case when :walutowa=1 then ROZFAK.pesumwartnet else ROZFAK.pesumwartnetzl end),
        (case when :walutowa=1 then ROZFAK.pesumwartbru else ROZFAK.pesumwartbruzl end),
        ROZFAK.VAT,
        ROZFAK.pesumwartvatzl
      from ROZFAK where ROZFAK.DOKUMENT=:nagfakref
      order by ROZFAK.VAT
      into :wartnet,:wartbru,:gr_vat,:wartvat
      do begin
        suspend;
      end
    end
  end
  else if(:zaliczkowy=0 and :korekta=1) then begin
    /* pozycje korygowane */
/*  for select
      (case when :walutowa=1 then ROZFAK.sumwartnet-ROZFAK.psumwartnet else ROZFAK.sumwartnetzl-ROZFAK.psumwartnetzl end),
      (case when :walutowa=1 then ROZFAK.sumwartbru-ROZFAK.psumwartbru else ROZFAK.sumwartbruzl-ROZFAK.psumwartbruzl end),
      ROZFAK.VAT,
      (case when :walutowa=1 then ROZFAK.sumwartvat-ROZFAK.psumwartvat else ROZFAK.sumwartvatzl-ROZFAK.psumwartvatzl end)
    from ROZFAK where ROZFAK.DOKUMENT=:nagfakref
    order by ROZFAK.VAT
    into :wartnet,:wartbru,:gr_vat,:wartvat
    do begin
      suspend;
    end*/
  end
  else if(:zaliczkowy=2) then begin
    /* korekta zaliczkowa - pozycje korygowane */
    for select
      (case when :walutowa=1 then -sum(nagfakzal.wartnet) else -sum(nagfakzal.wartnetzl) end),
      (case when :walutowa=1 then -sum(nagfakzal.wartbru) else -sum(nagfakzal.wartbruzl) end),
      nagfakzal.vat,
      (-sum(nagfakzal.wartbruzl-nagfakzal.wartnetzl))
      from nagfakzal
      where nagfakzal.faktura=:nagfakref
      group by nagfakzal.vat
      into :wartnet,:wartbru,:gr_vat,:wartvat
    do begin
      suspend;
    end
  end
end^
SET TERM ; ^
