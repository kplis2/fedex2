--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_WT_MWSORD_INFO(
      MWSORD integer)
  returns (
      DOCCNT integer,
      TAKEFULLPALL smallint,
      DOCIDCNT integer,
      SHOWMWSACTS smallint,
      MSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable good varchar(40);
declare variable palsposdost smallint;
declare variable palquantity integer;
declare variable pom varchar(10);
declare variable VOLUME NUMERIC(14,2);
declare variable WEIGHT NUMERIC(14,2);
declare variable POSCNT INTEGER;
declare variable mwsordtypedest integer;
declare variable typdok varchar(3);
declare variable doctype varchar(1);
declare variable multi smallint_id;
begin
  pom = '
';
  select o.doctype
    from mwsords o
    where o.ref = :mwsord
    into doctype;
  if (doctype = 'M') then
    select mo.mwsordtypedest, mo.takefullpal, mo.volume, mo.weight, count(distinct ma.mwsconstlocp), max(d.typ),
        mo.multi
      from mwsords mo
        left join mwsacts ma on(mo.ref = ma.mwsord and ma.status < 5 and ma.status > 0)
        left join dokumnag d on (d.ref = ma.docid and ma.doctype = :doctype)
      where mo.ref = :mwsord
      group by mo.mwsordtypedest, mo.takefullpal, mo.volume, mo.weight, mo.multi
      into mwsordtypedest, takefullpall, volume, weight,  poscnt, typdok,
        :multi;
  if (doctype = 'Z') then
    SELECT mo.mwsordtypedest, mo.takefullpal, mo.volume, mo.weight, count(distinct ma.mwsconstlocp), max(d.typzam)
      FROM MWSORDS mo
        left join mwsacts ma on(mo.ref = ma.mwsord and ma.status < 5 and ma.status > 0)
        left join nagzam d on (d.ref = ma.docid and ma.doctype = :doctype)
      where mo.ref = :mwsord
      group by mo.mwsordtypedest, mo.takefullpal, mo.volume, mo.weight
      into mwsordtypedest, takefullpall, volume, weight,  POSCNT, typdok;
  if(takefullpall is null)
    then takefullpall = 0;
  SELECT first 1 ma.good
    from mwsacts ma
      join towary t on (ma.good = t.ktm)
     where ma.mwsord = :mwsord
     and t.paleta = 1
     into :good;

  select count(distinct(ma.docid))
    from mwsacts ma
    where ma.mwsord = :mwsord
    into :docidcnt;
  if (:multi in (0,2) and :docidcnt > 0) then
    docidcnt = 1;

  select count(mp.good)
    from mwspallparts mp
    where mp.good = :good
    into :doccnt;
  msg = 'Objętość: '||coalesce(volume,0)||pom||'Waga: '||coalesce(weight,0)||pom;
  msg = msg||'Pozycji: '||coalesce(poscnt,0);
  if (mwsordtypedest = 8) then
    msg = msg||pom||'Odbiór własny';
  if (typdok = 'MM-') then
    msg = msg||pom||'Dokument MM- !!!';
  msg = '';

  --Ldz, wylaczenie komunikatu, gdy zrealizowane pozycje i wracamy do wt, zeby odstawic wozek
  select count(mt.good)
    from mwsacts mt
    where mt.mwsord = :mwsord
    and mt.status < 5
    into :showmwsacts;
  if (showmwsacts is null or showmwsacts = 0) then
    showmwsacts = 0;
  else
    showmwsacts = 1;
end^
SET TERM ; ^
