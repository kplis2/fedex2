--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_GET_TABKURS(
      BANK varchar(255) CHARACTER SET UTF8                           ,
      DATA timestamp,
      REFDOK integer,
      TYP char(1) CHARACTER SET UTF8                           )
  returns (
      RREF integer)
   as
declare variable datatmp date;
begin
  if (coalesce(:bank, '') = '') then begin
    execute procedure get_config('BANK', 0)
      returning_values :bank;
  end

  if (:data is null or :data < '1900-01-01') then datatmp = current_date;
  else datatmp = cast(:data as date);
  rref = 0;
  select first 1 t.ref
    from tabkurs t
    where t.akt = 1
      and t.bank = :bank
      and cast(t.data as date) < :datatmp
    order by t.data desc
  into :rref;
  suspend;
end^
SET TERM ; ^
