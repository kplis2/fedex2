--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWSCONSTLOC_CHANGE(
      MWSORDIN integer,
      MWSORDTYPEIN integer,
      GOODIN varchar(40) CHARACTER SET UTF8                           ,
      VERSIN integer,
      QUANTITYIN numeric(14,4),
      MWSCONSTLOCPS varchar(40) CHARACTER SET UTF8                           ,
      MWSCONSTLOCP integer,
      MWSPALLOCPS varchar(40) CHARACTER SET UTF8                           ,
      MWSPALLOCP integer,
      MWSCONSTLOCLS varchar(40) CHARACTER SET UTF8                           ,
      MWSCONSTLOCL integer,
      MWSPALLOCLS varchar(40) CHARACTER SET UTF8                           ,
      MWSPALLOCL integer,
      AUTOCOMMIT smallint,
      ACTUOPERATOR integer,
      MWSSTOCKCHECK smallint,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      PRIORITY smallint)
  returns (
      MWSORDOUT integer)
   as
declare variable wh varchar(3);
declare variable operator integer;
declare variable operatordict varchar(80);
declare variable blocksymbol varchar(40);
declare variable timestart timestamp;
declare variable timestartdcl timestamp;
declare variable timestopdcl timestamp;
declare variable refmwsord integer;
declare variable mwsaccessory integer;
declare variable period varchar(6);
declare variable branch varchar(10);
declare variable mix smallint;
declare variable good varchar(40);
declare variable paltype varchar(40);
declare variable vers integer;
declare variable wharealogl integer;
declare variable wharealogp integer;
declare variable whareap integer;
declare variable whareal integer;
declare variable maxnumber integer;
declare variable quantity numeric(14,4);
declare variable realtime double precision;
declare variable mwspalloclp integer;
declare variable dist numeric(14,4);
declare variable stocktaking smallint;
declare variable rec smallint;
declare variable cor smallint;
declare variable mwsactref integer;
declare variable lot integer;
declare variable levelnump integer;
declare variable levelnuml integer;
declare variable operatortmp integer;
declare variable mixtakepal integer;
declare variable mixleavepal integer;
declare variable mixmwsconstlocl integer;
declare variable mixmwspallocl integer;
declare variable pall numeric(14,2);
declare variable palw numeric(14,2);
declare variable palh numeric(14,2);
declare variable mixrefill smallint;
declare variable actcnt integer;
declare variable MWSCONSTLOCLSYMB varchar(40);
begin
  rec = 0;
  cor = 0;
  stocktaking = 0;
  if (mwsordin is not null) then
    select mwsordtype from mwsords where ref = :mwsordin
      into mwsordtypein;
  if (versin is null) then
    select first 1 ref
      from wersje
      where ktm = :goodin and akt = 1
      order by nrwersji
      into versin;
  if (mwsstockcheck is null) then mwsstockcheck = 1;
  if (autocommit is null) then autocommit = 0;
  if (descript is null) then descript = '';
  if (mwsconstlocl is null and mwsconstlocls is not null and mwsconstlocls <> '') then
    select ref from mwsconstlocs where symbol = :mwsconstlocls
      into mwsconstlocl;
  if (mwsconstlocp is null and mwsconstlocps is not null and mwsconstlocps <> '') then
    select ref from mwsconstlocs where symbol = :mwsconstlocps
      into mwsconstlocp;
  if (mwspallocl is null and mwspallocls is not null and mwspallocls <> '') then
    select ref from mwspallocs where symbol = :mwspallocls
      into mwspallocl;
  if (mwspallocp is null and mwspallocps is not null and mwspallocps <> '') then
    select ref from mwspallocs where symbol = :mwspallocps
      into mwspallocp;
  if (mwsordtypein is null and mwsordin is null) then exception MWSORDTYPE_NOT_SET;
  if (mwsstockcheck = 1) then
  begin
    if (exists(select ref from mwsstock where mwsconstloc = :mwsconstlocl and ispal = 0)) then
      exception MWSORD_ALREADY_PLANED 'zaplanowano zlecenie dla lokacji koncowej '||mwsconstlocps||' '||mwsconstlocls;
    if (exists(select ref from mwsstock where mwsconstloc = :mwsconstlocp and ispal = 0 and blocked > 0)) then
      exception MWSORD_ALREADY_PLANED 'zaplanowano zlecenie dla lokacji poczatkowej '||mwsconstlocps||' '||mwsconstlocls;
  end
  select wh from mwsconstlocs where ref = :mwsconstlocp into wh;
  select oddzial from defmagaz where symbol = :wh into branch;
  if (mwsordin is null) then
  begin
    execute procedure gen_ref('MWSORDS') returning_values refmwsord;
    select okres from datatookres(current_date,0,0,0,0) into period;
    insert into mwsords (ref, mwsordtype, stocktaking, doctype, operator, priority, description, wh,
        regtime, timestartdecl, timestopdecl, mwsaccessory, branch, period, cor, rec,
        bwharea, ewharea, status)
      values (:refmwsord, :mwsordtypein, :stocktaking, 'P', :operator, :priority, :descript, :wh,
        current_timestamp(0), :timestart, current_timestamp(0), :mwsaccessory, :branch, :period, :cor, :rec,
        null, null, 0);
  end
  else
    refmwsord = :mwsordin;
  -- POZYCJE ZLECENIA MAGAZYNOWEGO
  if (mwsconstlocl is null) then
  begin
    select count(ref) from mwsstock where mwsconstloc = :mwsconstlocp and quantity > 0
      into :mix;
    select first 1 m.good
      from mwsstock m
        left join towary t on (t.ktm = m.good)
      where t.paleta = 1 and m.mwsconstloc = :mwsconstlocp
      into paltype;
    select first 1 m.good, m.vers, mc.l, mc.w, mc.h
      from mwsstock m
        join mwsconstlocs mc on (mc.ref = m.mwsconstloc)
        left join towary t on (t.ktm = m.good)
      where t.paleta <> 1 and m.mwsconstloc = :mwsconstlocp
      into good, vers, pall, palw, palh;
    -- sprawdzamy czy przesuwamy mixa
    if (mix > 2) then mix = 1; else mix = 0;
    select xk.mwsconstlocl, xk.wharealogl, xk.whareal
      from XK_GET_BEST_MWSCONSTLOCL(:good, :vers,null,:refmwsord,:mwsordtypein, :wh, null, null, 10, 1, 0, :mix,null,:paltype, :palw,:pall,:palh,0) xk
      into mwsconstlocl, whareal, wharealogl;
  end
  else
  begin
    select wharea, wharealogfromstartarea from mwsconstlocs where ref = :mwsconstlocl
      into whareal, wharealogl;
  end
  select wharea, wharealogfromstartarea from mwsconstlocs where ref = :mwsconstlocp
    into whareap, wharealogp;
  maxnumber = 0;
  if (mwsconstlocl is not null and mwsconstlocp is not null) then
  begin
    select symbol from mwsconstlocs where ref = :mwsconstlocl into MWSCONSTLOCLSYMB;
    operatortmp = null;
    for
      select good, vers, quantity, mwspalloc,
          mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, mixrefill
        from mwsstock
        where mwsconstloc = :mwsconstlocp and quantity > 0
          and (vers = :versin or :versin is null)
        into good, vers, quantity, mwspallocp,
          mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, mixrefill
    do begin
      if (versin is not null) then
      begin
        if (quantity >= quantityin and quantityin > 0) then
          quantity = quantityin;
      end
      select mwsstandlevelnumber
        from mwsconstlocs
        where ref = :mwsconstlocp
      into levelnump;
      select mwsstandlevelnumber
        from mwsconstlocs
        where ref = :mwsconstlocl
      into levelnuml;
      if (levelnump is null) then levelnump = 0;
      if (levelnuml is null) then levelnuml = 0;
      select ref from mwsaccessories where aktuoperator = :operator
        into :mwsaccessory;
      if (:versin is null or :quantityin > 0) then
      begin
        -- czas realizacji dla kazdej operacji zlecenia
        maxnumber = maxnumber + 1;
        execute procedure gen_ref('MWSACTS') returning_values mwsactref;
        insert into mwsacts (ref,mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
            mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
            regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, recdoc, plus,
            whareap, whareal, wharealogp, wharealogl, number, disttogo, mixedpallgroup, palgroup,
            mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, refilltry)
          values (:mwsactref,:refmwsord, 0, :stocktaking, :good, :vers, :quantity, :mwsconstlocp,  :mwspallocp,
              :mwsconstlocl, :mwspallocl, null, null, 'O', 0, 0, :wh, :whareap, null,
              current_timestamp(0), :timestartdcl, :timestopdcl, :realtime, null, null, 1, :rec, 1,
              null, :whareal, null, :wharealogl, :maxnumber, :dist, :mix, null,
              :mixtakepal, :mixleavepal, :mixmwsconstlocl, :mixmwspallocl, :mixrefill);
        update mwsacts set status = 1 where ref = :mwsactref;
        timestart = timestartdcl;
        if (:versin is not null) then
        begin
          quantityin = quantityin - quantity;
          if (quantityin <= 0) then
            break;
        end
      end
    end
    if (versin is not null and quantityin > 0) then
      exception MWSORD_ALREADY_PLANED 'Brak wystarczajacej ilosci towaru na palecie';
  end else if (mwsconstlocl is not null and mwsconstlocp is null) then
  begin
    quantity = :quantityin;
    good = goodin;
    vers = versin;
    select symbol from mwsconstlocs where ref = :mwsconstlocl into MWSCONSTLOCLSYMB;
    operatortmp = null;
    select mwsstandlevelnumber
      from mwsconstlocs
      where ref = :mwsconstlocl
    into levelnuml;
    if (levelnump is null) then levelnump = 0;
    if (levelnuml is null) then levelnuml = 0;
    if (:versin is not null and :quantityin > 0) then
    begin
      -- czas realizacji dla kazdej operacji zlecenia
      maxnumber = maxnumber + 1;
      execute procedure gen_ref('MWSACTS') returning_values mwsactref;
      insert into mwsacts (ref,mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
          mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
          regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, recdoc, plus,
          whareap, whareal, wharealogp, wharealogl, number, disttogo, mixedpallgroup, palgroup,
          mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, refilltry)
        values (:mwsactref,:refmwsord, 1, :stocktaking, :good, :vers, :quantity, :mwsconstlocp,  :mwspallocp,
            :mwsconstlocl, :mwspallocl, null, null, 'O', 0, 0, :wh, :whareap, null,
            current_timestamp(0), :timestartdcl, :timestopdcl, :realtime, null, null, 1, 1, 1,
            null, :whareal, null, :wharealogl, :maxnumber, :dist, :mix, :mwspallocl,
            :mixtakepal, :mixleavepal, :mixmwsconstlocl, :mixmwspallocl, :mixrefill);
      update mwsacts set status = 1 where ref = :mwsactref and status = 0;
    end
  end
  -- na koniec jest komunikat ze nie ma wolnego miejsca na magazynie
  else
    exception XK_NO_PLACE_IN_WHSEC 'Brak okreslonego miejsca dla palety';
  -- czas dla ostatniej operacji - dojscie do punktu pozostawienia zlecenia
  select count(ref) from mwsacts where mwsord = :refmwsord into actcnt;
  if (actcnt is null) then actcnt = 0;
  if (actcnt > 0) then
  begin
    update mwsords set status = 1, timestopdecl = :timestopdcl, operator = null, mwsaccessory = :mwsaccessory
      where ref = :refmwsord and status = 0;
    if (autocommit = 1) then
    begin
      update mwsacts ma set ma.status = 2, ma.quantityc = ma.quantity where ma.mwsord = :refmwsord and ma.status = 1;
      update mwsords mo set mo.status = 5, mo.operator = :actuoperator where mo.ref = :refmwsord and mo.status = 1;
    end
    mwsordout = :refmwsord;
  end else
    delete from mwsords where ref = :refmwsord;
end^
SET TERM ; ^
