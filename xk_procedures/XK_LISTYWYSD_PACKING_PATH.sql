--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSD_PACKING_PATH(
      BOX integer)
  returns (
      PATH varchar(255) CHARACTER SET UTF8                           )
   as
declare variable current_box integer; /* Aktualnie rozpatrywane opakowanie */
declare variable parent integer; /* Opakowanie, ktore zawiera aktualne */
begin
--if (box_code is null) then exception universal 'Nie podano kodu opakowania.';
  current_box = :box;
  path = ';'||coalesce(current_box, '');

  while (current_box is not null) do
  begin
    select first 1 lw.rodzic from listywysdroz_opk lw
    where ref = :current_box
    into :parent;

    if (parent is not null) then
    begin
       path = path||';'||parent;
    end
    current_box = parent;
  end

  path = path||';';
  suspend;
end^
SET TERM ; ^
