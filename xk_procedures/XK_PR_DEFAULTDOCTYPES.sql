--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_PR_DEFAULTDOCTYPES(
      PRPOZZAM integer)
  returns (
      DOCTYPEIN varchar(3) CHARACTER SET UTF8                           ,
      DOCTYPEOUT varchar(3) CHARACTER SET UTF8                           )
   as
declare variable wh varchar(3);
declare variable prdepart varchar(20);
declare variable out smallint;
begin
  doctypein = null;
  doctypeout = null;
  select n.prdepart, p.magazyn, p.out
    from pozzam p
      left join nagzam n on (n.ref = p.zamowienie)
    where p.ref = :prpozzam
    into prdepart, wh, out;
  if (prdepart is not null and wh is not null) then
  begin
    select w.doctypein
      from prdepartswarehouses w
      where w.prdepart = :prdepart and w.warehouse = :wh and w.out = 0
      into doctypein;
    select w.doctypeout
      from prdepartswarehouses w
      where w.prdepart = :prdepart and w.warehouse = :wh and w.out = 1
      into doctypeout;
    if (doctypein is null and out = 0) then
      exception prdepart_error 'Brak wskazania domyślnego dokumentu przychodowego dla: '||:prdepart||' '||:wh;
    if (doctypeout is null and out = 1) then
      exception prdepart_error 'Brak wskazania domyślnego dokumentu rozchodowego dla: '||:prdepart||' '||:wh;
  end
end^
SET TERM ; ^
