--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_DAY_STATS(
      OPERATORIN integer,
      WH varchar(3) CHARACTER SET UTF8                           ,
      BDATE date,
      EDATE date)
  returns (
      OPERATOR integer,
      OPERNAME varchar(80) CHARACTER SET UTF8                           ,
      DIST numeric(14,4),
      EMPTYDIST numeric(14,4),
      ACTS integer,
      OUTSHIP integer,
      OUTOWN integer,
      PALSIN integer,
      PALSMOVE integer,
      UP integer,
      STOCK integer)
   as
declare variable mwsord integer;
declare variable mwsordtypedest integer;
declare variable barea integer;
declare variable startarea integer;
declare variable earea integer;
declare variable bareatmp integer;
declare variable eareatmp integer;
declare variable disttmp numeric(14,4);
declare variable anum integer;
declare variable docid integer;
declare variable palgroup integer;
declare variable mwsconstlocl integer;
declare variable docsymbs varchar(80);
begin
  select whareastart
    from defmagaz where symbol = :wh
    into startarea;
  for
    select m.operator, o.nazwa
      from opermag m
        left join operator o on (o.ref = m.operator)
      where m.magazyn = :wh --and m.cantakemwsords = 1
        and (m.operator = :operatorin or :operatorin is null)
        and exists (select first 1 1 from mwsordtypedest4op p where p.wh = m.magazyn and m.operator = o.ref)
      into operator, opername
  do begin
    barea = null;
    dist = 0;
    emptydist = 0;
    acts = 0;
    outship = 0;
    outown = 0;
    palsin = 0;
    palsmove = 0;
    up = 0;
    stock = 0;
    for
      select o.ref, o.mwsordtypedest, o.docsymbs, o.docid
        from mwsords o
        where o.operator = :operator and o.wh = :wh and o.timestop < :edate + 1 and o.timestop >= :bdate
          and o.mwsordtypedest in (1,8,11,15,3,9)
        into mwsord, mwsordtypedest, docsymbs, docid
    do begin
      if (barea is null) then
        barea = startarea;
      if (barea is null) then exception universal 'Brak obszaru początkowego na magazynie '||:wh;
      -- szykowanie towaru
      if (mwsordtypedest in (1,8)) then
      begin
        for
          select a.number, l.wharealogfromstartarea, p.wharealogfromstartarea
            from mwsacts a
              left join mwsconstlocs p on (p.ref = a.mwsconstlocp)
              left join mwsconstlocs l on (l.ref = a.mwsconstlocl)
            where a.mwsord = :mwsord and a.status = 5
            order by a.number
            into anum, eareatmp, bareatmp
        do begin
          disttmp = null;
          select coalesce(d.distance,0)
            from whareasdists d
            where d.whareab = :barea and d.whareae = :bareatmp
            into disttmp;
          if (disttmp is null) then disttmp = 0;
          dist = dist + disttmp;
          barea = bareatmp;
          acts = acts + 1;
          if (mwsordtypedest = 1) then
            outship = outship + 1;
          else if (mwsordtypedest = 8) then
            outown = outown + 1;
        end
        -- odstawienie do strefy pakowania
        disttmp = null;
        select d.distance
          from whareasdists d
          where d.whareab = :startarea and d.whareae = :barea
          into disttmp;
        if (disttmp is null) then disttmp = 0;
        emptydist = emptydist + disttmp;
        dist = dist + disttmp;
        barea = startarea;
      end else
      -- inwentaryzacja
      if (mwsordtypedest = 15) then
      begin
        bareatmp = null;
        select wharealogfromstartarea from mwsconstlocs where symbol = :docsymbs
          into bareatmp;
        if (bareatmp is not null) then
        begin
          disttmp = null;
          select coalesce(d.distance,0)
            from whareasdists d
            where d.whareab = :barea and d.whareae = :bareatmp
            into disttmp;
          if (disttmp is null) then disttmp = 0;
          dist = dist + disttmp;
          barea = bareatmp;
          acts = acts + 1;
          stock = stock + 1;
        end
      end else
      -- zlecenia wg
      if (mwsordtypedest = 9) then
      begin
        for
          select a.number, l.wharealogfromstartarea, p.wharealogfromstartarea
            from mwsacts a
              left join mwsconstlocs p on (p.ref = a.mwsconstlocp)
              left join mwsconstlocs l on (l.ref = a.mwsconstlocl)
            where a.mwsord = :mwsord and a.status = 5
            order by a.number
            into anum, eareatmp, bareatmp
        do begin
          disttmp = null;
          select coalesce(d.distance,0)
            from whareasdists d
            where d.whareab = :barea and d.whareae = :bareatmp
            into disttmp;
          if (disttmp is null) then disttmp = 0;
          dist = dist + disttmp;
          barea = bareatmp;
          acts = acts + 1;
          up = up + 1;
        end
      end else
      -- zlecenia przesuniecia towarów
      if (mwsordtypedest = 11) then
      begin
        -- jezeli paleta z przyjecia to dodajemy droge do strefy przyjecia
        if (exists (select ref from mwsords where ref = :docid and mwsordtypedest = 2)) then
        begin
          select o.bwharea
            from mwsords o
            where o.ref = :docid
            into bareatmp;
          if (bareatmp is not null) then
          begin
            disttmp = null;
            select coalescE(d.distance,0)
              from whareasdists d
              where d.whareab = :barea and d.whareae = :bareatmp
              into disttmp;
            if (disttmp is null) then disttmp = 0;
            dist = dist + disttmp;
            barea = bareatmp;
            acts = acts + 1;
            palsmove = palsmove + 1;
          end
        end
        for
          select a.number, l.wharealogfromstartarea, p.wharealogfromstartarea
            from mwsacts a
              left join mwsconstlocs p on (p.ref = a.mwsconstlocp)
              left join mwsconstlocs l on (l.ref = a.mwsconstlocl)
            where a.mwsord = :mwsord and a.status = 5
            order by a.number
            into anum, eareatmp, bareatmp
        do begin
          disttmp = null;
          select coalesce(d.distance,0)
            from whareasdists d
            where d.whareab = :barea and d.whareae = :eareatmp
            into disttmp;
          if (disttmp is null) then disttmp = 0;
          dist = dist + disttmp;
          barea = eareatmp;
          acts = acts + 1;
          palsmove = palsmove + 1;
        end
      end else
      -- optymalizacja rozmieszczenia
      if (mwsordtypedest = 3) then
      begin
        for
          select a.number, l.wharealogfromstartarea, p.wharealogfromstartarea
            from mwsacts a
              left join mwsconstlocs p on (p.ref = a.mwsconstlocp)
              left join mwsconstlocs l on (l.ref = a.mwsconstlocl)
            where a.mwsord = :mwsord and a.status = 5
            order by a.number
            into anum, eareatmp, bareatmp
        do begin
          -- trzeba dojsc do pierwszej operacji
          if (anum = 1) then
          begin
            disttmp = null;
            select coalesce(d.distance,0)
              from whareasdists d
              where d.whareab = :barea and d.whareae = :bareatmp
              into disttmp;
            if (disttmp is null) then disttmp = 0;
            dist = dist + disttmp;
          end
          disttmp = null;
          select coalesce(d.distance,0)
            from whareasdists d
            where d.whareab = :bareatmp and d.whareae = :eareatmp
            into disttmp;
          if (disttmp is null) then disttmp = 0;
          dist = dist + disttmp;
          barea = eareatmp;
          acts = acts + 1;
          palsmove = palsmove + 1;
        end
      end
    end
    -- dodajemy pozycje rozladowane bezposrednio z przyjecia
    for
      select o.bwharea, a.palgroup, max(a.mwsconstlocl)
        from mwsacts a
          left join mwsords o on (o.ref = a.mwsord)
          left join mwsconstlocs c on (c.ref = a.mwsconstlocl)
        where a.mwsordtypedest = 2 and o.operator = :operator and o.wh = :wh
          and a.timestop < :edate + 1 and a.timestop >= :bdate
          and c.locdest <> 4
        group by o.bwharea, a.palgroup
        into barea, palgroup, mwsconstlocl
     do begin
       select c.wharealogfromstartarea from mwsconstlocs c where c.ref = :mwsconstlocl
         into eareatmp;
       if (eareatmp is not null) then
       begin
         disttmp = null;
         select coalesce(d.distance,0)
           from whareasdists d
           where d.whareab = :barea and d.whareae = :eareatmp
           into disttmp;
         if (disttmp is null) then disttmp = 0;
         dist = dist + 2 * disttmp;
         acts = acts + 2;
         palsmove = palsmove + 1;
         palsin = palsin + 1;
       end
     end
    for
      select o.bwharea, a.palgroup, max(a.mwsconstlocl)
        from mwsacts a
          left join mwsords o on (o.ref = a.mwsord)
          left join mwsconstlocs c on (c.ref = a.mwsconstlocl)
        where a.mwsordtypedest = 2 and o.operator = :operator and o.wh = :wh
          and a.timestop < :edate + 1 and a.timestop >= :bdate
          and c.locdest = 4
        group by o.bwharea, a.palgroup
        into barea, palgroup, mwsconstlocl
     do begin
       acts = acts + 1;
       palsin = palsin + 1;
     end
     if (dist >= 0) then
     begin
       dist = dist/100;
       emptydist = emptydist/100;
       suspend;
     end
  end
end^
SET TERM ; ^
