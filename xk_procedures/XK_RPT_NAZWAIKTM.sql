--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_NAZWAIKTM(
      POZFAKREF integer,
      KTM varchar(255) CHARACTER SET UTF8                           ,
      NAZWA varchar(1000) CHARACTER SET UTF8                           )
  returns (
      RKTM varchar(255) CHARACTER SET UTF8                           ,
      RNAZWA varchar(1000) CHARACTER SET UTF8                           )
   as
declare variable nagfakref integer;
DECLARE VARIABLE GRUPAKUPIECKA INTEGER;
DECLARE VARIABLE NAGFAKODBIORCAID INTEGER;
declare variable wersjaref integer;
declare variable jedn integer;
declare variable klient integer;
begin
  select pozfak.dokument,pozfak.wersjaref,pozfak.jedn
    from pozfak where ref = :pozfakref
    into :nagfakref,:wersjaref,:jedn;
  nagfakodbiorcaid = null;
  klient = null;
  grupakupiecka = null;
  select nagfak.odbiorcaid, nagfak.klient from nagfak where ref = :nagfakref into :nagfakodbiorcaid, :klient;
  if(:nagfakodbiorcaid is not null) then
    select odbiorcy.grupakup from odbiorcy where ref = :nagfakodbiorcaid into :grupakupiecka;
  if(:grupakupiecka is null and :klient is not null) then
    select klienci.grupakup from klienci where ref = :klient into :grupakupiecka;
  if(:grupakupiecka is null) then begin
    rktm = ktm;
    rnazwa = nazwa;
  end else begin
    rktm = null;
    rnazwa = null;
    select grupykupofe.symbol,grupykupofe.nazwa
    from grupykupofe
    where grupakup=:grupakupiecka and wersjaref=:wersjaref and jedn=:jedn
    into :rktm, :rnazwa;
    if(:rktm is null or :rktm='') then rktm = :ktm;
    if(:rnazwa is null or :rnazwa='') then rnazwa = :nazwa;
  end
  suspend;
end^
SET TERM ; ^
