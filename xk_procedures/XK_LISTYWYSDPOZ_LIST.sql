--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSDPOZ_LIST(
      LISTWYSD integer,
      AKTOPK integer,
      TRYB smallint)
  returns (
      WERSJAREF integer,
      ILOSCMAX numeric(14,4),
      ILOSCSPK numeric(14,4),
      ILOSCDOSPK numeric(14,4),
      KTM varchar(40) CHARACTER SET UTF8                           ,
      NAZWAT varchar(255) CHARACTER SET UTF8                           ,
      NAZWAW varchar(255) CHARACTER SET UTF8                           ,
      NRWERSJI integer,
      SUMILSPK numeric(14,4),
      SUMILDOSPK numeric(14,4),
      RLISTWYSD integer,
      X_MWS_SERIE SMALLINT_ID,
      X_MWS_PARTIE SMALLINT_ID,
      X_MWS_SLOWNIK_EHRLE SMALLINT_ID,
      PARTIA STRING40,
      SERIA STRING40,
      SLO STRING40)
   as
declare variable SUMILMAX numeric(14,4);
begin
/*Tryb:
5 - usuwanie
pozostale - dodawanie
*/
  rlistwysd = :listwysd; --XXX MSt: ZG60193

  if (:tryb = 5) then begin
    for
      select lwp.wersjaref, coalesce(lwp.iloscmax, 0), sum(coalesce(lwr.iloscmag, 0)),
          (coalesce(lwp.iloscmax, 0) - coalesce(lwp.iloscspk, 0)) as iloscdospk,
          w.ktm, coalesce(w.nazwat, lwp.opis), w.nazwa, w.nrwersji, ms.serialno
        from listywysdpoz lwp
          join listywysdroz lwr on (lwp.ref = lwr.listywysdpoz)
          left join wersje w on (lwp.wersjaref = w.ref)
          left join x_mws_serie ms on (ms.ref = w.x_mws_serie)
        where lwp.dokument = :listwysd
          and lwr.opk = :aktopk
        group by lwp.dokpoz, lwp.wersjaref, lwp.iloscmax, lwp.iloscspk, lwp.wersjaref,
          w.ktm, coalesce(w.nazwat, lwp.opis), w.nazwa, w.nrwersji, ms.serialno
        order by w.ktm
      into :wersjaref, :iloscmax, :iloscspk, :iloscdospk,
        :ktm, :nazwat, :nazwaw, :nrwersji, :seria
    do begin
      if (partia is null) then
        partia ='-';
      if (seria is null) then
        seria ='-';
      sumildospk = :iloscdospk;
      sumilspk = :iloscspk;
      suspend;
    end
  end
  else
  begin
    for
      select lwp.wersjaref, sum(coalesce(lwp.iloscmax, 0)), sum(coalesce(lwp.iloscspk, 0)),
          (sum(coalesce(lwp.iloscmax, 0)) - sum(coalesce(lwp.iloscspk, 0))) as iloscdospk,
          w.ktm, coalesce(w.nazwat, lwp.opis), w.nazwa, w.nrwersji, w.x_mws_serie, w.x_mws_partie, w.x_mws_slownik_ehrle, ms.serialno
        from listywysdpoz lwp
          left join wersje w on (lwp.wersjaref = w.ref)
          left join x_mws_serie ms on (ms.ref = w.x_mws_serie)
        where lwp.dokument = :listwysd
        group by lwp.wersjaref, w.ktm, coalesce(w.nazwat, lwp.opis), w.nazwa, w.nrwersji, w.x_mws_serie, w.x_mws_partie, w.x_mws_slownik_ehrle, ms.serialno
        having sum(coalesce(lwp.iloscmax, 0)) > sum(coalesce(lwp.iloscspk, 0))
        order by w.ktm
      into :wersjaref, :iloscmax, :iloscspk, :iloscdospk,
        :ktm, :nazwat, :nazwaw, :nrwersji, :x_mws_serie, :x_mws_partie, x_mws_slownik_ehrle, :seria
    do begin
      sumildospk = :iloscspk;
      sumilspk = :iloscdospk;
      if (partia is null) then
        partia ='-';
      if (seria is null) then
        seria ='-';
      suspend;
    end
  end

end^
SET TERM ; ^
