--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_PRSCHEDGUIDEPOS_MATERIALS(
      PRDEPART varchar(20) CHARACTER SET UTF8                           ,
      PRNAGZAM integer,
      PRPOZZAM integer,
      PRSCHEDGUIDE integer,
      PRSCHEDOPER integer,
      PRSCHEDGUIDEPOS integer,
      TIMESTAMPTO timestamp,
      QUANTITYIN numeric(14,4))
  returns (
      REF integer,
      PRNAGZAMOUT integer,
      PRPOZZAMOUT integer,
      PRSCHEDGUIDEOUT integer,
      PRSCHEDGUIDESYMOUT varchar(40) CHARACTER SET UTF8                           ,
      PRSCHEDGUIDEPOSOUT integer,
      PRSCHEDOPEROUT integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      WERSJAREFDICT varchar(50) CHARACTER SET UTF8                           ,
      NAZWAT varchar(80) CHARACTER SET UTF8                           ,
      PRSHMAT integer,
      OUT smallint,
      WH varchar(3) CHARACTER SET UTF8                           ,
      DOCTYPEOUT varchar(3) CHARACTER SET UTF8                           ,
      DOCTYPEIN varchar(3) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4),
      QUANTITYP numeric(14,4),
      QUANTITYR numeric(14,4),
      AUTODOC smallint,
      PRSHORTAGE integer,
      SHORTAGE smallint)
   as
declare variable quantityout numeric(14,4);
begin
  -- procedura daje liste produktow do wydania
  ref = 0;
  if (prdepart = '') then prdepart = null;
  if (prnagzam = 0) then prnagzam = null;
  if (prpozzam = 0) then prpozzam = null;
  if (prschedguide = 0) then prschedguide = null;
  if (prschedoper = 0) then prschedoper = null;
  if (prschedguidepos = 0) then prschedguidepos = null;
  if (prdepart is null and prnagzam is null and prpozzam is null
        and prschedguide is null and prschedoper is null and prschedguidepos is null
  ) then
    exception PRSCHEDGUIDEDETS_ERROR 'Wymagany wydział produkcyjny';
  if (prschedguidepos is not null) then
  begin
    select g.ref, g.symbol, p.ref, p.ktm, p.wersjaref, p.out, p.prshmat, p.pozzamref, g.zamowienie,
        v.nazwa, v.nazwat, case when p.out = 1 then p.amount - p.amountzreal else 0 end, p.amount, p.amountzreal,
        p.amount/g.amount*:quantityin, p.warehouse,
        p.doctypein, p.doctypeout, p.autodocout
      from prschedguidespos p
        left join prschedguides g on (g.ref = p.prschedguide)
        left join wersje v on (v.ref = p.wersjaref)
      where p.ref = :prschedguidepos and p.out in (1,2) and p.activ > 0
      into prschedguideout, prschedguidesymout, prschedguideposout, ktm, wersjaref, out, prshmat, prpozzamout, prnagzamout,
          wersjarefdict, nazwat, quantity, quantityp, quantityr,
          quantityout, wh,
          doctypein, doctypeout, autodoc;
    if (quantityout < quantity) then
      quantity = quantityout;
    if (autodoc > 0) then
      quantity = 0;
    suspend;
    ref = ref + 1;
  end
  else if (prschedoper is not null) then
  begin
    for
      select g.ref, g.symbol, p.ref, p.ktm, p.wersjaref, p.out, p.prshmat, p.pozzamref, g.zamowienie,
          v.nazwa, v.nazwat, case when p.out = 1 then p.amount - p.amountzreal else 0 end, p.amount, p.amountzreal,
          p.amount/g.amount*:quantityin, p.warehouse,
          p.doctypein, p.doctypeout, p.autodocout
        from prschedguidespos p
          left join prschedguides g on (g.ref = p.prschedguide)
          left join wersje v on (v.ref = p.wersjaref)
        where p.prschedoper = :prschedoper and p.out in (1,2) and p.activ > 0
        into prschedguideout, prschedguidesymout, prschedguideposout, ktm, wersjaref, out, prshmat, prpozzamout, prnagzamout,
            wersjarefdict, nazwat, quantity, quantityp, quantityr,
            quantityout, wh,
            doctypein, doctypeout, autodoc
    do begin
      if (quantityout < quantity) then
        quantity = quantityout;
      if (autodoc > 0) then
        quantity = 0;
      suspend;
      ref = ref + 1;
    end
  end
  else if (prschedguide is not null) then
  begin
    for
      select g.ref, g.symbol, p.ref, p.ktm, p.wersjaref, p.out, p.prshmat, p.pozzamref, g.zamowienie,
          v.nazwa, v.nazwat, case when p.out = 1 then p.amount - p.amountzreal else 0 end, p.amount, p.amountzreal,
          p.amount/g.amount*:quantityin, p.warehouse,
          p.doctypein, p.doctypeout, p.autodocout
        from prschedguidespos p
          left join prschedguides g on (g.ref = p.prschedguide)
          left join nagzam n on (n.ref = g.zamowienie)
          left join wersje v on (v.ref = p.wersjaref)
        where p.prschedguide = :prschedguide and p.out in (1,2) and (p.activ > 0 or n.prpreprod = 1)
        into prschedguideout, prschedguidesymout, prschedguideposout, ktm, wersjaref, out, prshmat, prpozzamout, prnagzamout,
            wersjarefdict, nazwat, quantity, quantityp, quantityr,
            quantityout, wh,
            doctypein, doctypeout, autodoc
    do begin
      if (quantityout < quantity) then
        quantity = quantityout;
      if (autodoc > 0) then
        quantity = 0;
      suspend;
      ref = ref + 1;
    end
  end
  else if (prpozzam is not null) then
  begin
    for
      select g.ref, g.symbol, p.ref, p.ktm, p.wersjaref, p.out, p.prshmat, p.pozzamref, g.zamowienie,
          v.nazwa, v.nazwat, case when p.out = 1 then p.amount - p.amountzreal else 0 end, p.amount, p.amountzreal,
          p.amount/g.amount*:quantityin, p.warehouse,
          p.doctypein, p.doctypeout, p.autodocout
        from prschedguides g
          left join prschedguidespos p on (p.prschedguide = g.ref)
          left join nagzam n on (n.ref = g.zamowienie)
          left join wersje v on (v.ref = p.wersjaref)
        where g.pozzam = :prpozzam and p.out in (1,2) and g.status < 2 and (p.activ > 0 or n.prpreprod = 1)
        into prschedguideout, prschedguidesymout, prschedguideposout, ktm, wersjaref, out, prshmat, prpozzamout, prnagzamout,
            wersjarefdict, nazwat, quantity, quantityp, quantityr,
            quantityout, wh,
            doctypein, doctypeout, autodoc
    do begin
      if (quantityout < quantity) then
        quantity = quantityout;
      if (autodoc > 0) then
        quantity = 0;
      suspend;
      ref = ref + 1;
    end
  end
  else if (prnagzam is not null) then
  begin
    for
      select g.ref, g.symbol, p.ref, p.ktm, p.wersjaref, p.out, p.prshmat, p.pozzamref, g.zamowienie,
          v.nazwa, v.nazwat, case when p.out = 1 then p.amount - p.amountzreal else 0 end, p.amount, p.amountzreal,
          p.amount, p.warehouse,
          p.doctypein, p.doctypeout, p.autodocout
        from prschedguides g
          left join prschedguidespos p on (p.prschedguide = g.ref)
          left join nagzam n on (n.ref = g.zamowienie)
          left join wersje v on (v.ref = p.wersjaref)
        where g.zamowienie = :prnagzam and p.out in (1,2) and g.status < 2 and (p.activ > 0 or n.prpreprod = 1)
        into prschedguideout, prschedguidesymout, prschedguideposout, ktm, wersjaref, out, prshmat, prpozzamout, prnagzamout,
            wersjarefdict, nazwat, quantity, quantityp, quantityr,
            quantityout, wh,
            doctypein, doctypeout, autodoc
    do begin
      if (quantityout < quantity) then
        quantity = quantityout;
      if (autodoc > 0) then
        quantity = 0;
      suspend;
      ref = ref + 1;
    end
  end
  else if (prdepart is not null) then
  begin
    for
      select g.ref, g.symbol, p.ref, p.ktm, p.wersjaref, p.out, p.prshmat, p.pozzamref, g.zamowienie,
          v.nazwa, v.nazwat, case when p.out = 1 then p.amount - p.amountzreal else 0 end, p.amount, p.amountzreal,
          p.amount, p.warehouse,
          p.doctypein, p.doctypeout, p.autodocout
        from prschedguides g
          left join prschedguidespos p on (p.prschedguide = g.ref)
          left join wersje v on (v.ref = p.wersjaref)
        where g.prdepart = :prdepart and p.out in (1,2) and g.status < 2 and p.activ > 0
        into prschedguideout, prschedguidesymout, prschedguideposout, ktm, wersjaref, out, prshmat, prpozzamout, prnagzamout,
            wersjarefdict, nazwat, quantity, quantityp, quantityr,
            quantityout, wh,
            doctypein, doctypeout, autodoc
    do begin
      if (quantityout < quantity) then
        quantity = quantityout;
      if (autodoc > 0) then
        quantity = 0;
      suspend;
      ref = ref + 1;
    end
  end
end^
SET TERM ; ^
