--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_PZ_MG_CONSTLOC(
      MWSACTREF integer,
      MWSORDREF integer,
      NEWCONSTLOCSYMBOL varchar(20) CHARACTER SET UTF8                           ,
      MODE smallint)
  returns (
      CONSTLOCSYMBOL varchar(20) CHARACTER SET UTF8                           ,
      ACTSNOTREALCOUNT integer,
      PLANCONSTLOCSYMBOL varchar(20) CHARACTER SET UTF8                           )
   as
declare variable mwsconstlocref integer;
declare variable mgmwsconstlocref integer;
declare variable actref integer;
declare variable ordref integer;
declare variable quan numeric(15,4);
declare variable quanc numeric(15,4);
declare variable status smallint;
declare variable planconstloc varchar(20);
declare variable mwsstandseg integer;
declare variable palref integer;
declare variable blockpalref integer;
declare variable standlevel integer;
declare variable palgroup integer;
declare variable mwsconstlocchange smallint;
begin
  mwsconstlocchange = 0;
  palgroup = mwsactref;
  update mwsacts ma set ma.closepalloc = 1 where ma.mwspallocl = :palgroup;
  if (mode = 1) then begin
    select first 1 mc.symbol, ma.planmwsconstlocl, ma.palgroup, ma.ref
      from mwsacts ma
        join mwsconstlocs mc on (mc.ref = ma.mwsconstlocl)
      where ma.palgroup = :mwsactref
        and ma.mwsordtype = 8
      into :constlocsymbol, :planconstloc, :palgroup, :actref;
    if (planconstloc is null) then
    begin
      select first 1 ma.mwsconstlocl
        from mwsords mo
          left join mwsacts ma on (mo.ref = ma.mwsord)
        where mo.palgroup = :mwsactref
        into :planconstloc;
    end
    select first 1 mc.symbol
      from mwsconstlocs mc
      where mc.ref = :planconstloc
      into :planconstlocsymbol;
  end
  else if (mode = 2) then
  begin
    select first 1 mc.ref
      from mwsconstlocs mc
      where mc.symbol = :newconstlocsymbol
      into mwsconstlocref;

    select first 1 mo.ref, ma.mwsconstlocl  --sprawdzamy czy istnieje zlecenie MG powiazane z paleta ze zlecenia PZ
      from mwsords mo
        join mwsacts ma on (ma.mwsord = mo.ref)
      where mo.status = 1 and mo.palgroup = :palgroup
        and mo.mwsordtypedest = 11
      into :ordref, mgmwsconstlocref;
    if (ordref is not null) then --jesli istnieje to usuwamy
    begin
      if (mgmwsconstlocref <> mwsconstlocref) then
        mwsconstlocchange = 1;
      update mwsacts ma set status = 1 where ma.mwsord = :ordref;
      update mwsacts ma set status = 0 where ma.mwsord = :ordref;
      delete from mwsacts ma where ma.mwsord = :ordref;
      update mwsords mo set mo.status = 1 where mo.ref = :ordref;
      update mwsords mo set mo.status = 0 where mo.ref = :ordref;
      delete from mwsords mo where mo.ref = :ordref;
    end
    --zmieniamy constlocl na pozycjach zlecenia PZ zwiazanych z przesuwana paleta
    update mwsacts ma set ma.status = 1 where ma.mwsord = :mwsordref and ma.palgroup = :palgroup;
    update mwsacts ma set ma.status = 0 where ma.mwsord = :mwsordref and ma.palgroup = :palgroup;
    update mwsacts ma set ma.mwsconstlocl = :mwsconstlocref, blockconstlocchange = 1, mwsconstlocchange = 1,
      planmwsconstlocl = :mgmwsconstlocref where ma.palgroup = :palgroup and ma.mwsord = :mwsordref;
    update mwsacts ma set ma.status = 1 where ma.palgroup = :palgroup and ma.mwsord = :mwsordref;
    for
      select ma.ref, ma.quantity
        from mwsacts ma
        where ma.mwsord = :mwsordref and ma.palgroup = :palgroup
        into actref, quan
    do begin
      if (quan < quan) then
        status = 5;
      else
        status = 2;
      -- potwierdzamy ilosci na palecie po zmianie lokacji
      update mwsacts ma set ma.quantityc = ma.quantity, ma.status = :status where ma.ref = :actref;
    end
  end
  select count(mt.good)
    from mwsacts mt
    where mt.mwsord = :mwsordref
    and mt.status < 5
    into :actsnotrealcount;
  suspend;
end^
SET TERM ; ^
