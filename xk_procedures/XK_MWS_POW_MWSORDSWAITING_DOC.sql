--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_POW_MWSORDSWAITING_DOC(
      DOCIDIN integer)
  returns (
      ACK smallint)
   as
declare variable MAXLEVEL integer;
declare variable DOCID integer;
declare variable DOCPOSID integer;
declare variable WH varchar(3);
declare variable VERS integer;
declare variable STATUS smallint;
declare variable DOCSREAL integer;
declare variable POSLEFT integer;
declare variable POSTOREAL integer;
declare variable MWSGENPARTS varchar(100);
declare variable MINPOSTOREALS varchar(100);
declare variable MINPOSTOREAL integer;
begin
  ack = 1;
  maxlevel = 5;
  execute procedure get_config('MWSMINPOSTOREAL',2) returning_values minpostoreals;
  if (minpostoreals is not null and minpostoreals <> '') then
    minpostoreal = cast(minpostoreals as integer);
  else
    minpostoreal = 15;
  execute procedure get_config('MWSGENPARTS',2) returning_values mwsgenparts;
  for
    select first 1 distinct m.docid, coalesce(d.mwsmag,d.magazyn)
      from mwsgoodsrefill m
        left join dokumnag d on (d.ref = m.docid and m.doctype = 'M' and (d.blokada = 0 or d.blokada = 4))
        left join sposdost s on (s.ref = d.sposdost)
      where d.ref is not null and m.reallater = 0 and d.ref = :docidin
      group by m.docid, coalesce(d.mwsmag,d.magazyn), d.priorytet, d.data
      order by d.priorytet
      into docid, wh
  do begin
    docsreal = docsreal + 1;
    delete from mwsgoodsrefill where docid = :docid;
    delete from docpos2real where docid = :docid;
    for
      select p.wersjaref, p.ref, coalesce(n.mwsmag,n.magazyn)
      from dokumpoz p
        join dokumnag n on (n.ref = p.dokument)
        join towary t on (p.ktm = t.ktm)
        left join dokumpoz p1 on (p1.ref = p.alttopoz and p.fake = 1)
        left join towary t1 on (t1.ktm = p1.ktm and t1.usluga <> 1)
      where n.ref = :docid 
        and ((p.havefake = 0 and p.fake = 0 and coalesce(t1.altposmode,0) = 0) or
             (p.fake = 1 and t1.usluga <> 1 and coalesce(t1.altposmode,0) = 1))
        and p.genmwsordsafter = 1
        and ((n.akcept = 1 and n.mwsdisposition = 0)
          or (n.akcept = 0 and n.mwsdisposition = 1))
        and t.usluga <> 1
      into vers, docposid, wh
    do begin
      execute procedure mws_docpos_waiting_pow (wh,docposid,vers);
    end
    status = 0;
    update mwsgoodsrefill set tried = 1 where docid = :docid;
    update dokumnag set mwsdocreal = 4, genmwsordsafter = 0 where ref = :docid;
    execute procedure MWS_CHECK_DOCPOS_REAL_POW(:wh,:docid,null,0,1,2)
      returning_values posleft, postoreal;
    delete from docpos2real where docid = :docid and posreal = 0;
    select count(docposid) from docpos2real where docid = :docid and posreal = 1
      into postoreal;
    if (postoreal is null) then postoreal = 0;
    status = 4;
    if (postoreal = posleft) then status = 1;
    if (status = 0 or status = 4) then
    begin
      execute procedure MWS_CHECK_DOCPOS_REAL_POW(:wh,:docid,null,1,maxlevel,2)
        returning_values posleft, postoreal;
      select count(docposid) from docpos2real where docid = :docid and posreal = 2
        into postoreal;
      if (postoreal is not null and postoreal > 0) then
        status = 2;
    end
    if (status <> 1) then
    begin
      -- realizujemy tylko pelne palety
      select count(docposid) from docpos2real where docid = :docid and posreal = 2
        into postoreal;
      if (postoreal is null) then postoreal = 0;
      if (postoreal > 0) then
        status = 5;
      else status = 4;
      if (mwsgenparts = '1' and status <> 1 and status <> 2 and status <> 5) then
      begin
        select count(docposid) from docpos2real where docid = :docid
          and (posreal = 1 or posreal = 5)
          into postoreal;
        if (postoreal is null) then postoreal = 0;
        if (postoreal > 0) then
          status = 5;
      end
      else if (minpostoreal > 0) then
      begin
        select count(docposid) from docpos2real where docid = :docid and posreal = 1
          into postoreal;
        if (postoreal is null) then postoreal = 0;
        if (postoreal > 0 and postoreal >= minpostoreal) then
          status = 5;
      end
    end
    -- jezeli mozna zrealzowac dokument to realizujemy zlecenie
    if (status = 1 or status = 2 or status = 5) then
    begin
      update dokumnag set mwsdocreal = :status, genmwsordsafter = 1 where ref = :docid;
    end else
      update dokumnag set mwsdocreal = :status, genmwsordsafter = 0 where ref = :docid;
  end
  ack = :status;
end^
SET TERM ; ^
