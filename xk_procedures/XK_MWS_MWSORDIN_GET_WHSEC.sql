--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSORDIN_GET_WHSEC(
      MWSORD integer,
      GOOD varchar(40) CHARACTER SET UTF8                           )
  returns (
      WHSEC integer,
      WHSECSYMB varchar(3) CHARACTER SET UTF8                           ,
      PALLOC integer)
   as
begin
  whsec = 0;
  palloc = 0;

  select whsec from towary where ktm = :good
    into :whsec;
  if (:whsec is null) then whsec = 0;

  if (:whsec > 0) then
  begin
    select symbol from whsecs where ref = :whsec
    into :whsecsymb;

    select first 1 ref from mwspallocs
      where status < 1
        and whsec = :whsec
        and fromdoctype = 'P'
    into :palloc;
    if (:palloc is null) then
      palloc = 0;
  end
  else
    whsecsymb = '';

  suspend;
end^
SET TERM ; ^
