--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSDPOZ_REMOVING_BOX(
      BOX_CODE varchar(40) CHARACTER SET UTF8                           ,
      ACTIVE_BOX_CODE varchar(40) CHARACTER SET UTF8                           ,
      SPED_DOC integer)
  returns (
      ERR_MSG varchar(255) CHARACTER SET UTF8                           ,
      INFO_MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable active_box_ref integer;
declare variable active_box_opened integer;
declare variable box_to_remove_ref integer;
begin
    err_msg = '';
    info_msg = '';

    select opk.ref, opk.stan from listywysdroz_opk opk where opk.stanowisko = :active_box_code and opk.listwysd = :sped_doc
    into :active_box_ref, :active_box_opened;

    if (active_box_opened = 1) then
    begin
        err_msg = 'Nie można usuwać z zamkniętego opakowania.';
    end
    else
    begin
       select first 1 opk.ref from listywysdroz_opk opk
       where opk.stanowisko = :box_code and opk.rodzic = :active_box_ref
       into :box_to_remove_ref;

       if (coalesce(:box_to_remove_ref, 0) = 0) then
          begin
            err_msg = 'Nie znaleziono wskazanego opakowania w aktualnym opakowaniu.';
          end
       else
       begin
            /*
            else
            begin */
            UPDATE LISTYWYSDROZ_OPK
            SET RODZIC = NULL
            WHERE (REF = :box_to_remove_ref);

            -- update istniejacych danych (towarow) -> wpisy sciezek pakowania
            execute procedure xk_listywysd_packing_path_upd(:box_to_remove_ref, :sped_doc);
            INFO_MSG = 'Usunięto opakowanie.';
            /*end */
       end
    end
    suspend;
end^
SET TERM ; ^
