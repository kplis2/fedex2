--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_PACKQUANTITIES(
      GOOD varchar(20) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4))
  returns (
      PALQ numeric(14,4),
      MASQ numeric(14,4),
      BOXQ numeric(14,4),
      PACQ numeric(14,4),
      PIEQ numeric(14,4),
      PIECESPROC numeric(14,4))
   as
declare variable qpal numeric(14,4);
declare variable qpalnum numeric(14,4);
declare variable qmast numeric(14,4);
declare variable qmastnum numeric(14,4);
declare variable qbox numeric(14,4);
declare variable qboxnum numeric(14,4);
declare variable qpac numeric(14,4);
declare variable qpacnum numeric(14,4);
declare variable bigpack numeric(14,4);
begin
  select towjedn.przelicz from towjedn where ktm = :good and towjedn.jedn = 'pal' into qpal;
  if (qpal is null) then qpal = 0;
  select towjedn.przelicz from towjedn where ktm = :good and towjedn.jedn = 'Mast' into qmast;
  if (qmast is null) then qmast = 0;
  select towjedn.przelicz from towjedn where ktm = :good and towjedn.jedn = 'kart' into qbox;
  if (qbox is null) then qbox = 0;
  select towjedn.przelicz from towjedn where ktm = :good and towjedn.jedn = 'op.' into qpac;
  if (qpac is null) then qpac = 0;
  palq = 0;
  if (quantity >= qpal and qpal > 0) then begin
    qpalnum = quantity/qpal;
    palq = cast(qpalnum as integer);
    if (palq > qpalnum) then palq = :palq - 1;
      quantity = quantity - palq * qpal;
  end
  masq = 0;
  if (quantity >= qmast and qmast > 0) then begin
    qmastnum = quantity/qmast;
    masq = cast(qmastnum as integer);
    if (masq > qmastnum) then masq = :masq - 1;
      quantity = quantity - masq * qmast;
  end
  boxq = 0;
  if (quantity >= qbox and qbox > 0) then begin
    qboxnum = quantity/qbox;
    boxq = cast(qboxnum as integer);
    if (boxq > qboxnum) then boxq = :boxq - 1;
      quantity = quantity - boxq * qbox;
  end
  pacq = 0;
  if (quantity >= qpac and qpac > 0) then begin
    qpacnum = quantity/qpac;
    pacq = cast(qpacnum as integer);
    if (pacq > qpacnum) then pacq = pacq - 1;
      quantity = quantity - pacq * qpac;
  end
  pieq = quantity;
  -- obliczanie udzialu luznych sztuk w kartonie
  bigpack = 0;
  bigpack = qbox;
  if (bigpack <= 0) then
    bigpack = qmast;
  if (bigpack > 0) then
    piecesproc = pieq/bigpack;
  else
    piecesproc = 0;
end^
SET TERM ; ^
