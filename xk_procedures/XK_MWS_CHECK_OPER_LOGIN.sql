--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_CHECK_OPER_LOGIN(
      AKTUOPERATOR integer)
  returns (
      MWSACCESORIES varchar(30) CHARACTER SET UTF8                           ,
      HISTORYREF integer)
   as
begin
  select first 1 m.ref,  ma.symbol
    from mwsaccessoryhist m
       join mwsaccessories ma on(m.mwsaccessory = ma.ref)
    where m.operator = :aktuoperator
      and m.timestartto is null
    order by m.timestartfrom
    into historyref, mwsaccesories;
  suspend;
end^
SET TERM ; ^
