--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MOVE_PZ_GEN_DOC(
      DOCID integer,
      DOCGROUP integer,
      DOCPOSID integer,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      MWSORD integer,
      MWSACT integer,
      RECALC smallint,
      MWSORDTYPE integer)
  returns (
      REFMWSORD integer)
   as
declare variable doctypeorg varchar(3);
declare variable supplier integer;
begin
  select o.docid, o.docgroup, o.slopoz
    from mwsords o
    where o.ref = :mwsord
    into docid, docgroup, supplier;
  if (:docid is null) then docid = :docgroup;
  if (not exists(select first 1 1 from dokumnag d where d.ref = :docid)) then
  begin
    docid = null;
    update mwsords o set o.docid = null where o.ref = :mwsord;
    update mwsacts a set a.docid = 0 where a.mwsord = :mwsord;
  end
  if (docid is null) then
  begin
    select d.typdokmag
      from dostawcy d
      where d.ref = :supplier
      into doctypeorg;
    if (coalesce(doctypeorg,'')='') then doctypeorg = 'PZ';
    execute procedure mws_gen_doc_from_mwsord(:mwsord,:doctypeorg) returning_values :docid;
  end
  suspend;
end^
SET TERM ; ^
