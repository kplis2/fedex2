--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_TOWARY(
      NAGFAKREF integer)
  returns (
      OPIS varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable eol varchar(10);
declare variable pozref integer;
declare variable ktm varchar(255);
declare variable nazwa varchar(1024);
declare variable pkwiu varchar(40);
declare variable ilosc numeric(14,4);
declare variable miara varchar(20);
declare variable rabat numeric(14,2);
declare variable cenanet numeric(14,2);
declare variable wartnet numeric(14,2);
declare variable cenabru numeric(14,2);
declare variable wartbru numeric(14,2);
declare variable gr_vat varchar(10);
declare variable wartvat numeric(14,2);
declare variable walcen varchar(3);
begin
  eol='
';
  opis = '';
  for select POZFAK.REF,POZFAK.KTM,TOWARY.NAZWA,TOWARY.PKWIU,POZFAK.ILOSC,POZFAK.WALCEN,
    TOWJEDN.JEDN, POZFAK.RABAT+NAGFAK.RABAT, POZFAK.CENANETZL, POZFAK.WARTNETZL, POZFAK.CENABRUZL,POZFAK.WARTBRUZL,
    POZFAK.GR_VAT,POZFAK.WARTBRUZL-POZFAK.WARTNETZL
    from POZFAK
      join NAGFAK on (NAGFAK.REF=POZFAK.DOKUMENT)
      left join TOWARY on (TOWARY.KTM = POZFAK.KTM)
      left join TOWJEDN on (TOWJEDN.REF = POZFAK.JEDN)
    where POZFAK.DOKUMENT=:nagfakref and POZFAK.OPK = 0
    order by POZFAK.NUMER
  into :pozref,:ktm,:nazwa,:pkwiu,:ilosc,:walcen,:miara,:rabat,:cenanet,:wartnet,:cenabru,:wartbru,:gr_vat,:wartvat
  do begin
    if(:opis<>'') then opis = :opis||:eol;
    opis = :opis||:nazwa||' - o cenie netto '||cast(:cenanet as varchar(10))||' '||:walcen;
  end
  if(:opis is null) then opis = '';
  suspend;
end^
SET TERM ; ^
