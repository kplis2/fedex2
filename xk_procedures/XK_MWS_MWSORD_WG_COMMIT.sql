--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSORD_WG_COMMIT(
      MWSORD integer)
  returns (
      REFMWSORD integer,
      PALGROUP integer,
      PRINTER varchar(40) CHARACTER SET UTF8                           )
   as
declare variable PERIOD varchar(6);
declare variable WH varchar(3);
declare variable NEWMWSORD integer;
declare variable MWSORDTYPE integer;
declare variable STOCKTAKING integer;
declare variable MWSCONSTLOC integer;
declare variable NEWMWSACT integer;
declare variable MWSACT integer;
declare variable OPERSETTLMODE smallint;
declare variable AUTOLOCCREATE smallint;
declare variable TOINSERT numeric(14,4);
declare variable QUANTITYC numeric(14,4);
declare variable MWSORDTYPES varchar(10);
declare variable x_partiastr string20;
declare variable x_partia date_id;              -- XXX KBI
declare variable x_serial_no integer_id;        -- XXX KBI
declare variable x_slownik integer_id;          -- XXX KBI
declare variable good ktm_id;
declare variable vers integer_id;
declare variable mwsconstlocl integer_id;
declare variable mwspallocl integer_id;
declare variable planmwsconstlocl integer_id;
declare variable ref integer_id;
declare variable wharea integer_id;
declare variable whsec integer_id;
declare variable timestartdecl timestamp_id;
declare variable timestopdecl timestamp_id;
declare variable realtimedecl timestamp_id;
declare variable flags string40;
declare variable descript string;
declare variable priority integer_id;
declare variable lot integer_id;
declare variable recdoc integer_id;
declare variable whareal integer_id;
declare variable wharealogl integer_id;
declare variable disttogo numeric(14,4);
declare variable cnt integer_id;
begin
  select o.mwsordtypes
    from mwsords o
    where o.ref = :mwsord
    into mwsordtypes;
  if (exists (select a.ref from mwsacts a where a.status = 2 and a.quantityc > 0 and a.mwsord = :mwsord)) then
  begin
    for
      select a.ref, a.quantity - a.quantityc, a.quantityc
        from mwsacts a
        where a.mwsord = :mwsord and a.status = 2 and a.quantityc > 0
        into mwsact, toinsert, quantityc
    do begin
      update mwsacts set status = 0 where ref = :mwsact;
      update mwsacts set status = 1, quantity = :quantityc where ref = :mwsact;
      update mwsacts set status = 2, quantityc = quantity where ref = :mwsact;
      execute procedure gen_ref('MWSACTS') returning_values newmwsact;
      insert into mwsacts (ref, frommwsact, clonefrommwsact, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
          planmwsconstlocl, mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
          regtime, timestartdecl, timestopdecl, flags, descript, priority, lot, recdoc,
          cortomwsact, plus, number, accepttime, mwsconstloc,
            x_partia, x_serial_no, x_slownik) -- XXX KBI)
        select :newmwsact, frommwsact, ref, mwsord, 1, stocktaking, good, vers, :toinsert, mwsconstlocp, mwspallocp,
            planmwsconstlocl, mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
            regtime, timestartdecl, timestopdecl, flags, descript, priority, lot, recdoc,
            cortomwsact, plus, number + 1, accepttime, mwsconstloc,
            x_partia, x_serial_no, x_slownik -- XXX KBI)
          from mwsacts where ref = :mwsact;
    end
  end
  if (exists (select a.ref from mwsacts a where a.mwsord = :mwsord and a.status < 5)) then  --
  begin
    execute procedure gen_ref('MWSORDS') returning_values newmwsord;
    select okres from datatookres(current_date,0,0,0,0) into period;
    insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator, priority,
        description, wh,  regtime, timestartdecl, branch, period,
        flags, docsymbs, cor, rec, status, shippingarea, SHIPPINGTYPE, takefullpal,
        slodef, slopoz, slokod, palquantity, repal)
      select :newmwsord, mwsordtype, 0, 'U', null, null, null, 1,
          '', wh, current_timestamp(0), null, branch, :period,
          '', '', 0, 0, 1, null, null, 0,
          null, null, 'MWS_MWSORD_WG_COMMIT', null, 0
        from mwsords
        where ref = :mwsord;
    update mwsacts a set a.mwsord = :newmwsord where a.status < 5 and a.mwsord = :mwsord;
    update mwsacts a set a.status = 1, a.timestart = null where a.mwsord = :newmwsord and a.status = 2;
    update mwsords o set o.status = 5 where o.ref = :mwsord;
    if (not exists (select a.ref from mwsacts a where a.mwsord = :newmwsord)) then
    begin
      update mwsords o set o.status = 0 where o.ref = :newmwsord;
      delete from mwsords o where o.ref = :newmwsord;
    end
  end else
    update mwsords o set o.status = 5 where o.ref = :mwsord;
  if (exists (select a.ref from mwsacts a where a.mwsord = :mwsord and a.status = 5)) then  --
  begin
    select wh from mwsords where ref = :mwsord
      into wh;
    select first 1 ref, stocktaking, opersettlmode, autoloccreate
      from mwsordtypes p
      where position(';'||:mwsordtypes||';' in p.doctypes) > 0
      into mwsordtype, stocktaking, opersettlmode, autoloccreate;
    if (mwsordtype is null) then exception mwsord_except 'Nie ustalono typy zlecenia';
    execute procedure gen_ref('MWSPALLOCS') returning_values palgroup;
    insert into mwspallocs (ref, symbol, mwsconstloc)
      values (:palgroup, :palgroup, :mwsconstloc);
    execute procedure gen_ref('MWSORDS') returning_values refmwsord;
    select okres from datatookres(current_date,0,0,0,0) into period;
    insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator,
        priority, description, wh, difficulty, regtime, timestartdecl, timestopdecl,
        mwsaccessory, branch, period, flags, docsymbs, cor, rec, bwharea, ewharea,
        status, shippingarea, slodef, slopoz, slokod, manmwsacts, palgroup, frommwsord)
      select :refmwsord, :mwsordtype, :stocktaking, 'D', null, null, null,
          1, 'Rozwiezienie w półki', wh, null, current_timestamp(0), current_timestamp(0), current_timestamp(0),
          null, branch, :period, flags, symbol, cor, rec, null, null,
          0, shippingarea, slodef, slopoz, slokod, 1, :palgroup, :mwsord
        from mwsords
        where ref = :mwsord;
    /*
    insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
        mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
        regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc,
        whareap, whareal, wharealogp, wharealogl, number, disttogo, palgroup, refilltry)
      select :refmwsord, 1, stocktaking, good, vers, quantityc, mwsconstlocl,  mwspallocl,
          planmwsconstlocl, mwspallocl, :mwsord, ref, 'O', :opersettlmode, :autoloccreate, wh, wharea, whsec,
          current_timestamp(0), timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc,
          null, whareal, null, wharealogl, null, disttogo, null, 1
        from mwsacts
        where mwsord = :mwsord and status = 5;
    */

    for
      select stocktaking, good, vers, quantityc, mwsconstlocl,  mwspallocl,
          ref, wh, wharea, whsec,
          timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc,
          whareal, wharealogl, disttogo, x_partia, x_serial_no, x_slownik
        from mwsacts a
        where mwsord = :mwsord and status = 5
    into  :stocktaking, :good, :vers, :quantityc,:mwsconstlocl, :mwspallocl,
          :ref, :wh, :wharea, :whsec,
          :timestartdecl, :timestopdecl, :realtimedecl, :flags, :descript, :priority, :lot, :recdoc,
          :whareal, :wharealogl, :disttogo, :x_partiastr, :x_serial_no, :x_slownik
    do begin
      if (coalesce(x_partiastr,'') <> '') then
        x_partia = cast(x_partiastr as date_id);

      execute procedure XK_MWS_GET_BEST_LOCATION_3(:good, :vers, :ref, null, :mwsordtype, :wh, null, null, null,
          null, null, null, null, null, null,  null, null,null, :quantityc, :x_partia )
        returning_values :planmwsconstlocl, :cnt, :cnt, :cnt, :cnt;

      insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
          mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
          regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc,
          whareap, whareal, wharealogp, wharealogl, number, disttogo, palgroup, refilltry,
          x_partia, x_serial_no, x_slownik)
        values (:refmwsord, 1, :stocktaking, :good, :vers, :quantityc, :mwsconstlocl,  :mwspallocl,
            :planmwsconstlocl, :mwspallocl, :mwsord, :ref, 'O', :opersettlmode, :autoloccreate, :wh, :wharea, :whsec,
            current_timestamp(0), :timestartdecl, :timestopdecl, :realtimedecl, :flags, :descript, :priority, :lot, :recdoc,
            null, :whareal, null, :wharealogl, null, :disttogo, null, 1,
            :x_partia, :x_serial_no, :x_slownik);
      planmwsconstlocl = null;
      x_partia = null;
    end
    update mwsords set status = 1 where ref = :refmwsord;
  end
  printer = '';
end^
SET TERM ; ^
