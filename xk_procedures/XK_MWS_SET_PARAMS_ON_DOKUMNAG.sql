--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_SET_PARAMS_ON_DOKUMNAG(
      WHIN varchar(3) CHARACTER SET UTF8                           )
  returns (
      WHOUT varchar(3) CHARACTER SET UTF8                           )
   as
begin
  select magmaster from defmagaz where symbol = :whin
  into :whout;
  if (:whout is not null and not exists(select symbol from defmagaz where symbol = :whout)) then
    whout = null;
  suspend;
END^
SET TERM ; ^
