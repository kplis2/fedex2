--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_CHECK_OPER_RIGHTS(
      OPERATOR integer,
      MWSORD integer,
      GRANTTO varchar(100) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint)
   as
declare variable mwsordtypedest integer;
declare variable tmp varchar(1024);
declare variable wh varchar(3);
begin
  status = 0;
  tmp = null;
  select mwsordtypedest, wh
    from mwsords
    where ref = :mwsord
    into mwsordtypedest, wh;
  -- sprawdzamy czy dany operator ma prawo na rodzaju zlecenia
  if (grantto is not null and grantto <> '') then
  begin
    select coalesce(p.grants,'')
      from mwsordtypedest4op p
      where p.operator = :operator and p.wh = :wh and p.mwsordtypedest = :mwsordtypedest
      into tmp;
    if (tmp = '') then
      tmp = null;
    if (tmp is not null and position(';'||grantto||';' in tmp) > 0) then
      status = 1;
    if (tmp is null and status = 0) then
    begin
      -- jak nie bylo rodzaju zlecenia na operatorze a bylo dowolne to sprawdzamy na dowolnym
      select coalesce(p.grants,'')
        from mwsordtypedest4op p
        where p.operator = :operator and p.wh = :wh and p.mwsordtypedest = 0
        into tmp;
      if (tmp = '') then
        tmp = null;
      if (tmp is not null and position(';'||grantto||';' in tmp) > 0) then
        status = 1;
    end
  end
end^
SET TERM ; ^
