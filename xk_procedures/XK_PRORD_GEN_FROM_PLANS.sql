--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_PRORD_GEN_FROM_PLANS(
      PRDEPART varchar(20) CHARACTER SET UTF8                           ,
      PRORDSREGS varchar(80) CHARACTER SET UTF8                           ,
      FROMDATE timestamp,
      TODATE timestamp)
  returns (
      LP integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      GRUPA varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      WERSJAREF integer,
      NAZWAT varchar(100) CHARACTER SET UTF8                           ,
      MIARA varchar(5) CHARACTER SET UTF8                           ,
      ILOSCWOPAK varchar(20) CHARACTER SET UTF8                           ,
      STAN numeric(14,4),
      CENA numeric(14,4),
      WARTOSC numeric(14,2),
      PLANQ numeric(14,4),
      REALQ numeric(14,4),
      DISPQ numeric(14,4),
      ZAREZERW numeric(14,4),
      ZABLOKOW numeric(14,4),
      ZAMOWIONO numeric(14,4),
      ZAMOWIC numeric(14,4),
      WOLNE numeric(14,4),
      ILOSCNAZAM numeric(14,4),
      STANMIN numeric(14,4),
      STANMAX numeric(14,4),
      MINMAXSTAN smallint,
      PLANBDATE date,
      PLANEDATE date)
   as
begin
  lp = 0;
  execute procedure xk_prplan_real_calc(:prdepart, :prordsregs,:fromdate);
  for
    select d.shortname, coalesce(c.planamount,0), coalesce(c.realamount,0), coalesce(c.val1,0), dt.bdate, dt.edate
     from prdeparts dp
       join frdpersdimhier ph on (dp.frdpersdimhier = ph.ref)
       join frdpersdimvals v on (v.frdpersdimshier = ph.ref)
       join frdperspects p on (ph.frdperspect = p.ref)
       join frhdrs f on (p.frhdr = f.symbol)
       join frdhdrs fh on (fh.frhdr = f.symbol)
       join frdhdrsvers fv on (fv.frdhdr = fh.ref and fv.main = 1)
       join frpsns fp on (fp.ref = v.frpsn)
       join frdimhier dh on (ph.frdimshier = dh.ref)
       join frdimelems fd on (dh.ref = fd.frdimhier)
       join dimelemdef d on (fd.dimelem = d.ref)
       join frdimhier dht on (dht.ref = p.timefrdimhier)
       join frdimelems fdt on (dht.ref = fdt.frdimhier)
       join dimelemdef dt on (fdt.dimelem = dt.ref)
       left join frdcells c on (c.frdhdrver = fv.ref and c.timelem = fdt.dimelem and c.frdpersdimval = v.ref and c.dimelem = d.ref)
     where dp.symbol = :prdepart and dt.bdate is not null and dt.edate is not null
       and dt.bdate <= :todate and dt.edate >= :fromdate
     into ktm, planq, realq, dispq, planbdate, planedate
  do begin
    lp = lp + 1;
    wersjaref = null;
    wersja = null;
    nazwat = '';
    miara = '';
    grupa = '';
    iloscwopak = 0;
    stan = 0;
    cena = 0;
    wartosc = 0;
    zarezerw = 0;
    zablokow = 0;
    zamowiono = 0;
    wolne = 0;
    iloscnazam = 0;
    stanmin = 0;
    stanmax = 0;
    zamowic = 0;
    minmaxstan = 0;
    select first 1 w.ref, w.nrwersji, w.nazwat, t.miara, t.grupa
      from wersje w
        left join towary t on (t.ktm = w.ktm)
      where w.ktm = :ktm
      order by w.nrwersji
      into wersjaref, wersja, nazwat, miara, grupa;
    select sum(s.ilosc), sum(s.wartosc), sum(s.zamowiono), sum(s.zarezerw), sum(s.zablokow), sum(s.stanmin), sum(s.stanmax)
      from stanyil s
      where s.wersjaref = :wersjaref
      into stan, wartosc, zamowiono, zarezerw, zablokow, stanmin, stanmax;
    if (stan is null) then stan = 0;
    if (wartosc is null) then wartosc = 0;
    if (zamowiono is null) then zamowiono = 0;
    if (zarezerw is null) then zarezerw = 0;
    if (zablokow is null) then zablokow = 0;
    if (stanmin is null) then stanmin = 0;
    if (stanmax is null) then stanmax = 0;
    zamowic = planq - dispq;
    if (zamowic is null) then zamowic = 0;
    suspend;
  end
end^
SET TERM ; ^
