--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_CTI_SHOWCONNECT(
      MPHONE varchar(40) CHARACTER SET UTF8                           ,
      PREVREF integer,
      SINGLELINE smallint,
      GROUPNUMBERS smallint)
  returns (
      INFO varchar(4098) CHARACTER SET UTF8                           ,
      SLODEF integer,
      SLOPOZ integer,
      BNTIDENT varchar(80) CHARACTER SET UTF8                           ,
      LINES integer,
      GRIDCOLOUR varchar(40) CHARACTER SET UTF8                           ,
      LASTUSERTEXT varchar(255) CHARACTER SET UTF8                           ,
      STABLE varchar(31) CHARACTER SET UTF8                           ,
      CPODMIOT integer,
      INFOSMALL varchar(2048) CHARACTER SET UTF8                           ,
      LINESSMALL integer,
      PHONENUM varchar(40) CHARACTER SET UTF8                           )
   as
declare variable pom varchar(10);
declare variable fskrot shortname_id; --XXX ZG133796 MKD
declare variable nip varchar(20);
declare variable nazwa varchar(80);
declare variable dulica varchar(255);
declare variable dnrdomu nrdomu_id;
declare variable dnrlokalu nrdomu_id;
declare variable dkodp varchar(10);
declare variable dmiasto varchar(255);
declare variable telefon varchar(255);
declare variable komorka varchar(255);
declare variable email varchar(255);
declare variable operopieknazwa varchar(60);
declare variable maxref integer;
declare variable kontaktdata date;
declare variable kontakttyp varchar(40);
declare variable pkosoba varchar(60);
declare variable nagfakdata date;
declare variable nagfakwartosc numeric(14,2);
declare variable notatkatresc varchar(1000);
declare variable cnt integer;
declare variable toshow smallint;
declare variable tmpmphone varchar(30);
declare variable proc varchar(1024);
--declare variable grupakli varchar(40);
declare variable oper varchar(40);
begin
/*878398306*/
  tmpmphone = mphone||'%';
  INFO = 'Kontrahent nierozpoznany';
  execute procedure string_reverse(:mphone) returning_values(:phonenum);
  INFO = phonenum || ' ' || :info;
  if(groupnumbers = 1) then
    info = 'O:'||info;
  infosmall = substring(:info from 1 for 1000);
  SLODEF = null;
  SLOPOZ = null;
  BNTIDENT = '';
  pom = '
  ';
  GRIDCOLOUR = 'clWindow';
--  priority = 1;
  LASTUSERTEXT = '';
--  STABLE = 'KLIENCI';
  LINES = 1;
  linessmall = 1;
  toshow = 1;
  if (:mphone is null or :mphone = '' or :mphone = ' ' or :mphone = '1') then
  begin
    if (:mphone = '1') then
      INFO = 'Brak aktualnie prowadzonej rozmowy';
    infosmall = substring(:info from 1 for 1000);
    suspend;
    if (:toshow = 1) then
      toshow = 0;
  end else
  begin
 --   proc = 'select count(ref) from cphones where cphones.mirrornumber like '''||
   --   :tmpmphone||''' and (cphones.sfield = ''KOMORKA'' or cphones.sfield =''TELEFON'')';
    select count(ref)
      from cphones
      where cphones.mirrornumber like :tmpmphone
        and (cphones.sfield = 'KOMORKA' or cphones.sfield ='TELEFON')
      into :cnt;
    if (:cnt > 1) then
      GRIDCOLOUR = 'clRed';
    else
      GRIDCOLOUR = 'clWindow';
--    proc = 'select cphones.stable, cphones.skey from cphones where cphones.mirrornumber like '''||
  --    :tmpmphone||''' and (cphones.sfield = ''KOMORKA'' or cphones.sfield =''TELEFON'')';
    for
      select cphones.stable, cphones.skey
        from cphones
        where cphones.mirrornumber like :tmpmphone
         and (cphones.sfield = 'KOMORKA' or cphones.sfield ='TELEFON')
      into :stable, :slopoz
    do begin
      cpodmiot = null;
      lines = 0;
      if (:stable = 'KLIENCI') then slodef = 1;
      else if (:stable = 'DOSTAWCY') then slodef = 6;
      else if (:stable = 'PKLIENCI') then slodef = 7;
      else if (:stable = 'SPRZEDAWCY') then slodef = 2;

      -- wyświetlenie informacji odnosnie podmiotu z ktorym rozmawiamy
      -- klienci
      if (:slodef = 1) then begin
        bntident = 'KLIENCI';
        select klienci.fskrot, klienci.nip, klienci.nazwa, klienci.dulica, klienci.dnrdomu, klienci.dnrlokalu,
            KLIENCI.DKODP, KLIENCI.DMIASTO, KLIENCI.TELEFON, KLIENCI.KOMORKA,
            KLIENCI.EMAIL, CPODMIOTY.OPEROPIEKNAZWA, cpodmioty.ref
          from klienci
            left join CPODMIOTY on (cpodmioty.slopoz = klienci.ref)
          where KLIENCI.REF = :slopoz and CPODMIOTY.slodef = 1
          into :fskrot, :nip, :nazwa, :dulica, :dnrdomu, :dnrlokalu,
            :dkodp, :dmiasto , :telefon, :komorka,
            :email, :operopieknazwa, :cpodmiot; --, :priority;    --, :grupakli;
          if (:operopieknazwa is null) then operopieknazwa = '';
          info = :fskrot||' - '||:nip||:pom||
            :nazwa||:pom||
            :dulica||' '||iif(coalesce(:dnrdomu,'')<>'',:dnrdomu,'')||iif(coalesce(:dnrlokalu,'')<>'','/'||:dnrlokalu,'')||' '||
            :dkodp||' '||:dmiasto||:pom||
            :telefon||' '||:komorka||' '||:email||:pom||
            :operopieknazwa||:pom;
          lines = :lines + 5;
          if(groupnumbers = 1) then
            infosmall = 'O:';
          else
            infosmall = '';
          infosmall=:infosmall || :phonenum|| :pom||
            :fskrot||'  ';
--            :pom||
--            'NIP: '||:nip||:pom||
--            :dulica||pom||
--            :dmiasto;
          linessmall=:linessmall+1;
      end

      -- dostawcy
      if (:slodef = 6) then begin
        bntident = 'DOSTAWCY';
        select dostawcy.id, dostawcy.nip, dostawcy.nazwa, dostawcy.ulica, dostawcy.nrdomu, dostawcy.nrlokalu,
             dostawcy.kodp,dostawcy.miasto, dostawcy.telefon,
             dostawcy.email, CPODMIOTY.OPEROPIEKNAZWA, cpodmioty.ref
          from dostawcy
            left join CPODMIOTY on (cpodmioty.slopoz = dostawcy.ref)
          where dostawcy.REF = :slopoz and CPODMIOTY.slodef = 6
          into :fskrot, :nip, :nazwa, :dulica, :dnrdomu, :dnrlokalu,
            :dkodp, :dmiasto , :telefon,
            :email, :operopieknazwa, :cpodmiot;
          if (:operopieknazwa is null) then operopieknazwa = '';
          info = :fskrot||' - '||:nip||:pom||
            :nazwa||:pom||
            :dulica||' '||iif(coalesce(:dnrdomu,'')<>'',:dnrdomu,'')||iif(coalesce(:dnrlokalu,'')<>'','/'||:dnrlokalu,'')||' '||
            :dkodp||' '||:dmiasto||:pom||
            :telefon||' '||:email||:pom||
            :operopieknazwa||:pom;
          lines = :lines + 5;
          if(groupnumbers = 1) then
            infosmall = 'O:';
          else
            infosmall = '';
          infosmall=:infosmall || :phonenum|| :pom||
            :fskrot||'  ';
          linessmall=:linessmall+1;
      end

      -- sprzedawcy
      if (:slodef = 2) then begin
        bntident = 'SPRZEDAWCY';
        select sprzedawcy.skrot, sprzedawcy.nip, sprzedawcy.nazwa, sprzedawcy.adres, sprzedawcy.nrdomu, sprzedawcy.nrlokalu,
             sprzedawcy.kodpoczt, sprzedawcy.miasto, sprzedawcy.telefon,
             sprzedawcy.email, CPODMIOTY.OPEROPIEKNAZWA, cpodmioty.ref
          from sprzedawcy
            left join CPODMIOTY on (cpodmioty.slopoz = sprzedawcy.ref)
          where sprzedawcy.REF = :slopoz and CPODMIOTY.slodef = 2
          into :fskrot, :nip, :nazwa, :dulica, :dnrdomu, :dnrlokalu,
            :dkodp, :dmiasto , :telefon,
            :email, :operopieknazwa, :cpodmiot;
          if (:operopieknazwa is null) then operopieknazwa = '';
          info = :fskrot||' - '||:nip||:pom||
            :nazwa||:pom||
            :dulica||' '||iif(coalesce(:dnrdomu,'')<>'',:dnrdomu,'')||iif(coalesce(:dnrlokalu,'')<>'','/'||:dnrlokalu,'')||' '||
            :dkodp||' '||:dmiasto||:pom||
            :telefon||' '||:email||:pom||
            :operopieknazwa||:pom;
          lines = :lines + 5;
          if(groupnumbers = 1) then
            infosmall = 'O:';
          else
            infosmall = '';
          infosmall=:infosmall || :phonenum|| :pom||
            :fskrot||'  ';
          linessmall=:linessmall+1;
      end

      -- pklienci
      if (:slodef = 7) then begin
        bntident = 'PKLIENCI';
        select pklienci.skrot, '',pklienci.nazwa, pklienci.ulica, pklienci.nrdomu, pklienci.nrlokalu,
             pklienci.kodp, pklienci.miasto, pklienci.telefon,
             pklienci.email, CPODMIOTY.OPEROPIEKNAZWA, cpodmioty.ref
          from pklienci
            left join CPODMIOTY on (cpodmioty.slopoz = pklienci.ref)
          where pklienci.REF = :slopoz and CPODMIOTY.slodef = 7
          into :fskrot, :nip, :nazwa, :dulica, :dnrdomu, :dnrlokalu,
            :dkodp, :dmiasto , :telefon,
            :email, :operopieknazwa, :cpodmiot;
          if (:operopieknazwa is null) then operopieknazwa = '';
          info = :fskrot||' - '||:nip||:pom||
            :nazwa||:pom||
            :dulica||' '||iif(coalesce(:dnrdomu,'')<>'',:dnrdomu,'')||iif(coalesce(:dnrlokalu,'')<>'','/'||:dnrlokalu,'')||' '||
            :dkodp||' '||:dmiasto||:pom||
            :telefon||' '||:email||:pom||
            :operopieknazwa||:pom;
          lines = :lines + 5;
          if(groupnumbers = 1) then
            infosmall = 'O:';
          else
            infosmall = '';
          infosmall=:infosmall || :phonenum|| :pom||
            :fskrot||'  ';
          linessmall=:linessmall+1;
      end

      maxref = null;
      if (:slodef = 1) then
      begin
        select max(nagfak.ref) from nagfak where nagfak.klient = :slopoz
          into :maxref;
        select nagfak.data, (nagfak.sumwartnet - nagfak.psumwartnet)
          from nagfak where nagfak.ref = :maxref
          into :nagfakdata, :nagfakwartosc;
        if (:maxref is not null) then begin
          info = :info||'Ostatnia faktura VAT: '||:nagfakdata||' o wartości netto: '||:nagfakwartosc||:pom;
          lines = :lines + 1;
        end
      end
      -- wyświetlenie informacji przetrzymywanych w CRM
      if (:cpodmiot is not null) then
      begin
        bntident = :bntident||'CPODMIOTY';
        -- wyświetlenie informacji odnosnie ostatniego kontaktu: data kontktu, medium, z kim
        maxref = null;
        select max(kontakty.ref) from kontakty where kontakty.cpodmiot = :cpodmiot
          into :maxref;
        select kontakty.data, ctypykon.nazwa, pkosoby.nazwa, operator.operskrot
          from kontakty
            left join ctypykon on (kontakty.rodzaj = ctypykon.ref)
            left join pkosoby on (pkosoby.ref = kontakty.pkosoba)
            left join operator on (kontakty.operator = operator.ref)
          where kontakty.ref = :maxref
          into :kontaktdata, :kontakttyp, :pkosoba, :oper;
        if (:maxref is not null) then
        begin
          info = :info||'Ostatni kontakt: '||:kontaktdata||' typu: '||:kontakttyp;
          if (:oper is not null and :oper <> '') then
          begin
            info = :info || ' op: '||:oper;
            infosmall = :infosmall || '('||:oper||')';
          end
          info = :info ||:pom;
          lines = :lines + 1;
          if (:pkosoba is not null and :pkosoba <> '') then
          begin
            info = :info||'z osobą: '||:pkosoba||:pom;
            lines = :lines + 1;
          end
        end
        -- wyświetlenie notatek dla podmiotu
        for
          select substring(cnotatki.tresc from 1 for 1000)
            from cnotatki
            where cnotatki.cpodmiot = :cpodmiot and cnotatki.powiadom = 1
              and (cnotatki.powiadomtime is null or cnotatki.POWIADOMTIME <= current_date)
            into :notatkatresc
        do begin
          info = :info||'----------------'||:pom||
            :notatkatresc||:pom;
          lines = :lines + 2;
        end
        info = :info||'----------------'||:pom;
        lines = :lines + 1;
      end
      if (:slodef = 1 and :fskrot <> '' and :fskrot is not null) then begin
        if (:operopieknazwa is null) then
          operopieknazwa = '';
        LASTUSERTEXT = :fskrot||' '||:operopieknazwa;
      end
      suspend;
      if (:toshow = 1) then
        toshow = 0;
    end
  end
  if (:toshow = 1) then
  begin
    toshow = 0;
    suspend;
  end
END^
SET TERM ; ^
