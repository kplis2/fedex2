--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_SUPP_DIFF_COMP(
      MWSORD integer,
      ILDNI integer,
      OPERATOR integer,
      DATADOK date)
  returns (
      STATUS smallint)
   as
declare variable DOCPOSIDPLUS integer;
declare variable DOCPOSIDMINUS integer;
declare variable CENAMINUS numeric(14,2);
declare variable MWSACTDETPLUS integer;
declare variable MWSACTDETMINUS integer;
declare variable ILOSCPLUS numeric(14,4);
declare variable ILOSCMINUS numeric(14,4);
declare variable WHPLUS varchar(3);
declare variable WHMINUS varchar(3);
declare variable VERSPLUS integer;
declare variable VERSMINUS integer;
declare variable VERSNUMPLUS integer;
declare variable VERSNUMMINUS integer;
declare variable GOODPLUS varchar(40);
declare variable GOODMINUS varchar(40);
declare variable DOCTYPEPLUS varchar(3);
declare variable DOCTYPEMINUS varchar(3);
declare variable DOSTAWAMINUS integer;
declare variable I numeric(14,4);
declare variable DOCMINUS integer;
declare variable DOCPLUS integer;
declare variable ROZREFPLUS integer;
declare variable ROZREFMINUS integer;
declare variable ROZREFPLUSNEW integer;
declare variable ROZREFMINUSNEW integer;
declare variable DOCID integer;
declare variable GRUPACENPLUS integer;
declare variable DOCGROUP integer;
declare variable MWSDISPOSITION smallint;
begin
  status = 1;
  select coalesce(n.mwsdisposition,0),
        case when o.docid is null then o.docgroup else o.docid end, o.docgroup
    from mwsords o
      left join dokumnag n on (n.ref = o.docid)
    where o.ref = :mwsord and o.doctype = 'M'
    into mwsdisposition, docid, :docgroup;
  if (docid is null) then
    exception universal 'Brak wskazania do dokumentu magazynowego na zleceniu';
  -- najpierw rozliczamy pozycje na PZ+, które da sie rozliczyc z PZ-
  -- kojarzymy je po KTM, bo z reguly jest to zamiana wersji
  if (mwsdisposition = 0) then
  begin
    for
      select r.ref, r.ktm, r.wersjaref, r.ilosc, r.dokument, r.pozycja,
          r.magazyn, r.frommwsactdet, r.doctype, r.wersja
        from MWSORDDOCDIFF r
        where r.mwsord = :mwsord and r.wydania = 0 and r.rozl = 0 and r.ilosc > 0
        into rozrefplus, goodplus, versplus, iloscplus, docplus, docposidplus,
          whplus, mwsactdetplus, doctypeplus, versnumplus
    do begin
      for
        select r.ref, r.ktm, r.wersjaref, r.ilosc, r.dokument, r.pozycja,
            r.magazyn, r.frommwsactdet, r.doctype, r.dostawa, r.cena, r.wersja
          from MWSORDDOCDIFF r
          where r.mwsord = :mwsord and r.ktm = :goodplus and r.wydania = 1 and r.rozl = 0 and r.ilosc > 0
          into rozrefminus, goodminus, versminus, iloscminus, docminus, docposidminus,
            whminus, mwsactdetminus, doctypeminus, dostawaminus, cenaminus, versnumminus
      do begin
        if (iloscplus > 0 and iloscminus > 0) then
        begin
          if (iloscplus > iloscminus) then
            i = iloscminus;
          else
            i = iloscplus;
          execute procedure gen_ref('DOKUMROZ') returning_values rozrefplusnew;
          insert into MWSORDDOCDIFF(REF,MAGAZYN,DOKUMENT,POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,FROMMWSACTDET,
              mwsord, doctype, wydania,wersja,wersjaref,ktm,rozl,cenafromroz)
            values (:rozrefplusnew,:whplus, :docplus,:docposidplus,0,:i,:cenaminus,null,:mwsactdetplus,
                :mwsord,:doctypeplus,0,:versnumplus,:versplus,:goodplus,1,:rozrefminusnew);
          execute procedure gen_ref('DOKUMROZ') returning_values rozrefminusnew;
          insert into MWSORDDOCDIFF(REF,MAGAZYN,DOKUMENT,POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,FROMMWSACTDET,
              mwsord, doctype, wydania,wersja,wersjaref,ktm,rozl,cenatoroz)
            values (:rozrefminusnew,:whminus, :docminus,:docposidminus,0,:i,:cenaminus,:dostawaminus,:mwsactdetminus,
                :mwsord,:doctypeminus,1,:versnumminus,:versminus,:goodminus,1,:rozrefplusnew);
          update MWSORDDOCDIFF x set ilosc = ilosc - :i where ref = :rozrefminus;
          update MWSORDDOCDIFF x set ilosc = ilosc - :i where ref = :rozrefplus;
          iloscplus = iloscplus - i;
        end
      end
    end
    -- najpierw rozliczamy pozycje na PZ+, które da sie rozliczyc z PZ-
    -- kojarzymy je po GRUPIE cenowej, bo moze byc to zamiana rozmiarów na przyklad
    for
      select r.ref, r.ktm, r.wersjaref, r.ilosc, r.dokument, r.pozycja,
          r.magazyn, r.frommwsactdet, r.doctype, r.wersja, r.grupacen
        from MWSORDDOCDIFF r
        where r.mwsord = :mwsord and r.wydania = 0 and r.rozl = 0 and r.ilosc > 0
          and r.grupacen is not null
        into rozrefplus, goodplus, versplus, iloscplus, docplus, docposidplus,
          whplus, mwsactdetplus, doctypeplus, versnumplus, :grupacenplus
    do begin
      for
        select r.ref, r.ktm, r.wersjaref, r.ilosc, r.dokument, r.pozycja,
            r.magazyn, r.frommwsactdet, r.doctype, r.dostawa, r.cena, r.wersja
          from MWSORDDOCDIFF r
          where r.mwsord = :mwsord and r.ktm = :goodplus and r.grupacen = :grupacenplus
            and r.wydania = 1 and r.rozl = 0 and r.ilosc > 0
          into rozrefminus, goodminus, versminus, iloscminus, docminus, docposidminus,
            whminus, mwsactdetminus, doctypeminus, dostawaminus, cenaminus, versnumminus
      do begin
        if (iloscplus > 0 and iloscminus > 0) then
        begin
          if (iloscplus > iloscminus) then
            i = iloscminus;
          else
            i = iloscplus;
          execute procedure gen_ref('DOKUMROZ') returning_values rozrefplusnew;
          execute procedure gen_ref('DOKUMROZ') returning_values rozrefminusnew;
          insert into MWSORDDOCDIFF(REF,MAGAZYN,DOKUMENT,POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,FROMMWSACTDET,
              mwsord, doctype, wydania,wersja,wersjaref,ktm,rozl,cenafromroz)
            values (:rozrefplusnew,:whplus, :docplus,:docposidplus,0,:i,:cenaminus,null,:mwsactdetplus,
                :mwsord,:doctypeplus,0,:versnumplus,:versplus,:goodplus,1,:rozrefminusnew);
          insert into MWSORDDOCDIFF(REF,MAGAZYN,DOKUMENT,POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,FROMMWSACTDET,
              mwsord, doctype, wydania,wersja,wersjaref,ktm,rozl,cenatoroz)
            values (:rozrefminusnew,:whminus, :docminus,:docposidminus,0,:i,:cenaminus,:dostawaminus,:mwsactdetminus,
                :mwsord,:doctypeminus,1,:versnumminus,:versminus,:goodminus,1,:rozrefplusnew);
          update MWSORDDOCDIFF x set ilosc = ilosc - :i where ref = :rozrefminus;
          update MWSORDDOCDIFF x set ilosc = ilosc - :i where ref = :rozrefplus;
          iloscplus = iloscplus - i;
        end
      end
    end
    delete from MWSORDDOCDIFF x where x.mwsord = :mwsord and x.ilosc = 0;
    execute procedure xk_mws_supp_diff_docs(:mwsord,:ildni,:operator,:datadok);
    status = 1;
  end else
  begin
    for select ref from dokumnag where grupasped = :docgroup
        into :docid
    do
      execute procedure mws_close_disposition(:docid, 0, 1, 0);
    delete from MWSORDDOCDIFF r where r.mwsord = :mwsord;
    status = 0;
  end
end^
SET TERM ; ^
