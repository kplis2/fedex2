--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_HH_INWENTA_BROWSE(
      OPERATOR integer,
      WH varchar(3) CHARACTER SET UTF8                           )
  returns (
      REF integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      DATA timestamp)
   as
begin
  for
    select i.ref, i.symbol, i.opis, i.data
      from inwenta i
      where i.magazyn = :wh
      into :ref, :symbol, :descript, :data
  do begin
    suspend;
  end
end^
SET TERM ; ^
