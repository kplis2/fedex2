--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_NALICZ_RAPORT_PLODOWROLNYCH(
      REF integer,
      MAGAZYN char(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      ODDATY timestamp,
      DODATY timestamp,
      TYP smallint,
      STAWKAPALIWA numeric(14,2),
      KOSZTENPOZ numeric(14,2),
      KOSZTROB numeric(14,2))
   as
declare variable zanieczyszczenia numeric(14,2);
declare variable zanieczyszczeniatech numeric(14,2);
declare variable zanieczyszczeniatechnieuz numeric(14,2);
declare variable zanieczyszczeniawe numeric(14,2);
declare variable zanieczyszczeniawy numeric(14,2);
declare variable zanieczyszczeniawenieuz numeric(14,2);
declare variable zanieczyszczeniawynieuz numeric(14,2);
declare variable zanieczyszczenianieuz numeric(14,2);
declare variable zanwe numeric(14,2);
declare variable zannieuzwe numeric(14,2);
declare variable zanwy numeric(14,2);
declare variable zannieuzwy numeric(14,2);
declare variable sumailzanwe numeric(14,2);
declare variable sumailzanwy numeric(14,2);
declare variable sumailzannieuz numeric(14,2);
declare variable sumailzannieuzwe numeric(14,2);
declare variable sumailzannieuzwy numeric(14,2);
declare variable wilgotnosc numeric(14,2);
declare variable wilgotnosctech numeric(14,2);
declare variable ilosc numeric(14,2);
declare variable wydania smallint;
declare variable koryg smallint;
declare variable sumailwilwe numeric(14,2);
declare variable sumailwe numeric(14,2);
declare variable sumailwilwy numeric(14,2);
declare variable sumailwy numeric(14,2);
declare variable wilwe numeric(14,2);
declare variable wilwy numeric(14,2);
declare variable osuszkawe numeric(14,2);
declare variable osuszkawy numeric(14,2);
declare variable wartosc numeric(14,2);
declare variable sumwartoscwe numeric(14,2);
declare variable sumwartoscwy numeric(14,2);
declare variable wartosuszkiwe numeric(14,2);
declare variable wartosuszkiwy numeric(14,2);
declare variable wartzanwe numeric(14,2);
declare variable wartzannieuzwe numeric(14,2);
declare variable wartzannieuzwy numeric(14,2);
declare variable wartzanwy numeric(14,2);
declare variable kosztpaliwa numeric(14,2);
declare variable kosztcalk numeric(14,2);
declare variable swilgotnosctech varchar(255);
declare variable szanieczyszczeniatech varchar(255);
declare variable szanieczyszczeniatechnieuz varchar(255);
begin

sumailwe = 0;
sumailwilwe = 0;
sumailzanwe = 0;
sumailzannieuzwe = 0;
sumailzannieuzwy = 0;
sumwartoscwe = 0;
osuszkawe = 0;
osuszkawy = 0;
ZANIECZYSZCZENIAWY = 0;
sumailwy = 0;
sumailwilwy = 0;
sumailzanwy = 0;
sumwartoscwy = 0;
zanwy = 0;
wilwy = 0;
wartzanwy = 0;
wartzannieuzwy = 0;
wartzannieuzwe = 0;
wartosuszkiwy = 0;
kosztpaliwa = 0;
kosztcalk = 0;
zanieczyszczenianieuz = 0;
zanieczyszczeniawenieuz = 0;
zanieczyszczeniawynieuz = 0;
sumailzannieuz = 0;
wilgotnosctech = 0;
zanieczyszczeniatech = 0;
zanieczyszczeniatechnieuz = 0;
select replace(A1.wartosc,',','.'),
 replace(A2.wartosc,',','.'),   --pobranie wilgotnosci technologicznej i zanieczyszczeń
 replace(A3.wartosc,',','.')
  from atrybuty A1
  join atrybuty A2 on (A1.ktm = A2.ktm)
  join atrybuty A3 on (A1.ktm = A3.ktm)
  join defcechy D1 on (A1.cecha = D1.symbol)
  join defcechy D2 on (A2.cecha = D2.symbol)
  join defcechy D3 on (A3.cecha = D3.symbol)
  where D1.parsymbol = 'PARAMN2' and a1.ktm = :ktm
  and D2.parsymbol ='PARAMN1' and D3.parsymbol = 'PARAMN3'
  into :swilgotnosctech, :szanieczyszczeniatech, :szanieczyszczeniatechnieuz;
if(:swilgotnosctech<>'') then wilgotnosctech = cast(:swilgotnosctech as numeric(14,2));
if(:szanieczyszczeniatech<>'') then zanieczyszczeniatech = cast(:szanieczyszczeniatech as numeric(14,2));
if(:szanieczyszczeniatechnieuz<>'') then zanieczyszczeniatechnieuz = cast(:szanieczyszczeniatechnieuz as numeric(14,2));

for select dokumpoz.ilosc, dokumpoz.paramn2,
defdokum.wydania, defdokum.koryg, dokumpoz.paramn1, dokumpoz.paramn3, dokumpoz.wartosc
from dokumnag
    join defdokum on (dokumnag.typ = defdokum.symbol)
    join dokumpoz on (dokumnag.ref = dokumpoz.dokument)
  where dokumnag.akcept = 1
    and dokumnag.magazyn = :magazyn
    and dokumnag.data >= :oddaty
    and dokumnag.data <= :dodaty
    and dokumpoz.ktm = :ktm
    and (dokumpoz.flagi not like '%;K;%')
    and ((:typ = 0 and  (dokumpoz.flagi not like '%;Z;%' or dokumpoz.flagi is null))
    or (:typ =1 and dokumpoz.flagi like '%;Z;%')
    or (:typ =2))
  into :ilosc, wilgotnosc, :wydania,
    :koryg,:zanieczyszczenia,:zanieczyszczenianieuz,:wartosc
do begin
  if(:wilgotnosc is null) then wilgotnosc = 0;
  if(:zanieczyszczenia is null) then zanieczyszczenia = 0;
  if(:zanieczyszczenianieuz is null) then zanieczyszczenianieuz = 0;
  -- wyjscie
  if (typ = 0) then
  begin
    if (:wydania = 1 and :koryg = 0) then
      begin
        sumailwilwy = :sumailwilwy + :ilosc * :wilgotnosc;
        sumailzanwy = :sumailzanwy + :ilosc * :zanieczyszczenia;
        sumailzannieuzwy = :sumailzannieuzwy + :ilosc * :zanieczyszczenianieuz;
        sumwartoscwy = sumwartoscwy + :wartosc;
        sumailwy = :sumailwy + :ilosc;
      end
    else if (:wydania = 0 and :koryg = 1) then
      begin
        sumailwilwy = :sumailwilwy - :ilosc * :wilgotnosc;
        sumailzanwy = :sumailzanwy - :ilosc * :zanieczyszczenia;
        sumailzannieuzwy = :sumailzannieuzwy + :ilosc * :zanieczyszczenianieuz;
        sumwartoscwy = sumwartoscwy - :wartosc;
        sumailwy = :sumailwy - :ilosc;
      end
  end
  -- wejscie
  if (:wydania = 0 and :koryg = 0) then
    begin
      sumailwilwe = :sumailwilwe + :ilosc * :wilgotnosc;
      sumailzanwe = :sumailzanwe + :ilosc * :zanieczyszczenia;
      sumailzannieuzwe = :sumailzannieuzwe + :ilosc * :zanieczyszczenianieuz;
      sumwartoscwe = sumwartoscwe + :wartosc;
      sumailwe = :sumailwe + :ilosc;
    end
  else if (:wydania = 1 and :koryg = 1) then
    begin
      sumailwilwe = :sumailwilwe - :ilosc * :wilgotnosc;
      sumailzanwe = :sumailzanwe - :ilosc * :zanieczyszczenia;
      sumailzannieuzwe = :sumailzannieuzwe + :ilosc * :zanieczyszczenianieuz;
      sumwartoscwe = sumwartoscwe - :wartosc;
      sumailwe = :sumailwe - :ilosc;
    end
  end
  if(sumailwe > 0 ) then  begin
    wilwe = sumailwilwe / sumailwe;
    zanwe = sumailzanwe / sumailwe;
    zannieuzwe = sumailzannieuzwe / sumailwe;
    osuszkawe = sumailwe * (wilwe - wilgotnosctech) / 100;
    wartosuszkiwe = sumwartoscwe / sumailwe *  osuszkawe;
    zanieczyszczeniawe = sumailwe * (zanwe - zanieczyszczeniatech) / 100;
    zanieczyszczeniawenieuz = sumailwe * (zannieuzwe - zanieczyszczeniatechnieuz) /100;
    wartzanwe = sumwartoscwe / sumailwe * zanieczyszczeniawe;
    wartzannieuzwe = sumwartoscwe / sumailwe * zanieczyszczeniawenieuz;
    if(typ = 1)  --tylko dla suszarni sa koszty pozostale,paliwa i robocizny
      then begin
      kosztpaliwa = stawkapaliwa * (wilwe - wilgotnosctech) * sumailwe;
      kosztcalk = :kosztenpoz + :kosztpaliwa + :kosztrob;
      end
--    kosztcalk = kosztcalk + :wartzanwe + :wartosuszkiwe ;
  end
  if(typ = 0 and sumailwy > 0) then begin  --tylko dla suszenia samoistnego
    wilwy = sumailwilwy / sumailwy;
    zanwy = sumailzanwy / sumailwy;
    zannieuzwy = sumailzannieuzwy / sumailwy;
    osuszkawy =  sumailwy * (wilgotnosctech - wilwy) / 100;
    wartosuszkiwy = sumwartoscwy / sumailwy *  osuszkawy;
    zanieczyszczeniawy = sumailwy * (zanieczyszczeniatech - zanwy) / 100;
    zanieczyszczeniawynieuz = sumailwy * (zanieczyszczeniatechnieuz - zannieuzwy)/100;
    wartzanwy = sumwartoscwy / sumailwy * zanieczyszczeniawy;
    wartzannieuzwy = sumwartoscwy / sumailwy * zanieczyszczeniawynieuz;
--    kosztcalk = kosztcalk + :wartzanwy + :wartosuszkiwy ;
  end
  if(:typ=0 or :typ=1) then
    update raporty set
      iloscwe = :sumailwe,
      wilgotnoscwe = :wilwe,
      wilgotnosctech = :wilgotnosctech,
      iloscoswe = :osuszkawe,
--      zanieczwe = :zanwe,
--      zaniecztech = :zanieczyszczeniatech,
--      ilosczanwe = :zanieczyszczeniawe,
      wartoscwe = :wartosuszkiwe,
--      wartzanwe = :wartzanwe,
      iloscwy = :sumailwy,
      wilgotnoscwy = :wilwy,
      iloscoswy = :osuszkawy,
--      zanieczwy = :zanwy,
--      ilosczanwy = :zanieczyszczeniawy,
      wartoscwy = :wartosuszkiwy,
--      wartzanwy = :wartzanwy,
      kosztpaliwa = :kosztpaliwa,
      kosztcalk = :kosztcalk + :wartosuszkiwe + :wartosuszkiwy
    where raporty.ref = :ref;
  else if(:typ=2) then
    update raporty set
      iloscwe = :sumailwe,
--      wilgotnoscwe = :wilwe,
--      wilgotnosctech = :wilgotnosctech,
--      iloscoswe = :osuszkawe,
      zanieczwe = :zanwe,
      zaniecztech = :zanieczyszczeniatech,
      zaniecznieuz = :zannieuzwe,
      ilosczanwe = :zanieczyszczeniawe,
      ilosczannieuzwe = :zanieczyszczeniawenieuz,
      ilosczannieuzwy = :zanieczyszczeniawynieuz,
--    wartoscwe = :wartosuszkiwe,
      wartzanwe = :wartzanwe,
      wartzannieuzwe = :wartzannieuzwe,
      wartzannieuzwy = :wartzannieuzwy,
      iloscwy = :sumailwy,
--      wilgotnoscwy = :wilwy,
--      iloscoswy = :osuszkawy,
      zanieczwy = :zanwy,
      ilosczanwy = :zanieczyszczeniawy,
--      wartoscwy = :wartosuszkiwy,
      wartzanwy = :wartzanwy,
--      kosztpaliwa = :kosztpaliwa,
      kosztcalk = :kosztcalk + :wartzanwe + :wartzanwy
    where raporty.ref = :ref;
end^
SET TERM ; ^
