--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_ETYKIETY_SPED_GLS(
      LISTYWYSDREF integer,
      SPOSDOSTREF integer,
      KODPNAD varchar(10) CHARACTER SET UTF8                           )
  returns (
      RSYMBOLDOK varchar(255) CHARACTER SET UTF8                           ,
      RSYMBOLSPED varchar(255) CHARACTER SET UTF8                           ,
      RNAZWAODB1 varchar(255) CHARACTER SET UTF8                           ,
      RNAZWAODB2 varchar(255) CHARACTER SET UTF8                           ,
      RMIASTOODB varchar(255) CHARACTER SET UTF8                           ,
      RADRESODB varchar(255) CHARACTER SET UTF8                           ,
      RKODPODB varchar(255) CHARACTER SET UTF8                           ,
      RTELEFONODB varchar(255) CHARACTER SET UTF8                           ,
      RPOCZTAODB varchar(255) CHARACTER SET UTF8                           ,
      RDATAAKC timestamp,
      RUWAGISPED varchar(255) CHARACTER SET UTF8                           ,
      RILOSCPACZEK integer,
      RILOSCPACZEKSTR varchar(10) CHARACTER SET UTF8                           ,
      RILOSCPALET integer,
      RILOSCPALETSTR varchar(10) CHARACTER SET UTF8                           ,
      RNRPACZKI integer,
      RNRPACZKISTR varchar(10) CHARACTER SET UTF8                           ,
      RWAGA numeric(15,4),
      RPOBRANIE numeric(14,2),
      RKODREJONUNAD varchar(3) CHARACTER SET UTF8                           ,
      RKODKRAJUNAD varchar(20) CHARACTER SET UTF8                           ,
      RKODREJONUODB varchar(3) CHARACTER SET UTF8                           ,
      RKODKRAJUODB varchar(20) CHARACTER SET UTF8                           ,
      RWAHADLO varchar(3) CHARACTER SET UTF8                           ,
      RKIERUNEKSORT varchar(3) CHARACTER SET UTF8                           ,
      RTRASA varchar(10) CHARACTER SET UTF8                           ,
      RS10 smallint,
      RSSOBOTA smallint,
      RSZWROTDOK smallint,
      RSYMBOLREF varchar(255) CHARACTER SET UTF8                           ,
      RSYMBOLPACZKI varchar(255) CHARACTER SET UTF8                           ,
      RWAGAPACZKI numeric(15,4),
      RKODKRESK varchar(255) CHARACTER SET UTF8                           ,
      RCYFRACTRL smallint,
      RPOBRANIEPRT smallint,
      RGLS2P varchar(4) CHARACTER SET UTF8                           ,
      RGLS2K varchar(4) CHARACTER SET UTF8                           ,
      RGLSLISTWYSD varchar(40) CHARACTER SET UTF8                           )
   as
declare variable slodefref integer;
declare variable slopozref integer;
declare variable flagisped varchar(255);
declare variable nazwaodb varchar(255);
declare variable miastoodb varchar(255);
begin
  select lwd.symbol, lwd.symbolsped,
         lwd.kontrahent, lwd.miasto, lwd.adres, lwd.kodp,
         lwd.iloscpaczek, lwd.waga, lwd.pobranie,
         cast(lwd.dataakc as date), substring(lwd.uwagisped from 1 for 255),
         lwd.slodef, lwd.slopoz, lwd.flagisped
    from listywysd lwd
    where lwd.ref = :listywysdref
  into :rsymboldok, :rsymbolsped,
       :nazwaodb, :rmiastoodb, :radresodb, :rkodpodb,
       :riloscpaczek, :rwaga, :rpobranie,
       :rdataakc, :ruwagisped,
       :slodefref, :slopozref, :flagisped;

  if (coalesce(:rpobranie, 0) > 0) then rpobranieprt = 1;
  else rpobranieprt = 0;

  --nadawca
  select kodrejonu, krajid
    from spedodleg
    where spedytor = :sposdostref
      and kodp = replace(replace(:kodpnad,'-',''), ' ', '')
  into :rkodrejonunad, :rkodkrajunad;

  --odbiorca
  if (coalesce(:radresodb, '') = '' or coalesce(:rmiastoodb, '') = '') then begin
    if (:slodefref = 1 and :slopozref is not null) then begin
      select k.ulica, k.miasto, k.kodp, k.poczta, k.telefon
        from klienci k where ref = :slopozref
      into :radresodb, :miastoodb, :rkodpodb, :rpocztaodb, :rtelefonodb;
    end
    else if (:slodefref = 6 and :slopozref is not null) then begin
      select d.ulica, d.miasto, d.kodp, d.poczta, d.telefon
        from dostawcy d where ref = :slopozref
      into :radresodb, :miastoodb, :rkodpodb, :rpocztaodb, :rtelefonodb;
    end

    if (:miastoodb <> :rpocztaodb) then radresodb = :radresodb||', '||:miastoodb;
  end

  execute procedure xk_usun_znaki(:rkodpodb, 2) returning_values :rkodpodb;

  select spo.kodrejonu, spo.krajid, spo.wahadlo, spo.kieruneksort, spo.trasa, spo.ssobota, spo.s10
    from spedodleg spo
    where spo.spedytor = :sposdostref
      and spo.kodp = replace(replace(:rkodpodb,'-',''), ' ', '')
  into :rkodrejonuodb, :rkodkrajuodb, :rwahadlo, :rkieruneksort, :rtrasa, :rssobota, :rs10;

  if (strmulticmp(:flagisped,';S;')>0 and coalesce(:rssobota, 0) = 1) then rssobota = 1;
  else rssobota = 0;
  if (strmulticmp(:flagisped,';D;')>0 and coalesce(:rs10, 0) = 1) then rs10 = 1;
  else rs10 = 0;
  if (strmulticmp(:flagisped,';Z;')>0) then rszwrotdok = 1;
  else rszwrotdok = 0;

  if (substring(:rkodrejonuodb from 1 for 1) = '0') then rkodrejonuodb = substring(:rkodrejonuodb from 2 for coalesce(char_length(:rkodrejonuodb),0)); -- [DG] XXX ZG119346

  execute procedure xk_usun_znaki(:nazwaodb, 1) returning_values :nazwaodb;
  execute procedure xk_usun_znaki(:rmiastoodb, 1) returning_values :rmiastoodb;
  execute procedure xk_usun_znaki(:radresodb, 1) returning_values :radresodb;
  execute procedure xk_usun_znaki(:ruwagisped, 1) returning_values :ruwagisped;
  execute procedure xk_usun_znaki(:rpocztaodb, 1) returning_values :rpocztaodb;

  rnazwaodb1 = substring(:nazwaodb from 1 for 36);
  rnazwaodb2 = substring(:nazwaodb from 37 for 72);

  --dokumenty REF
  select first 1 symbol
    from nagfak where listywysd = :listywysdref
    order by ref
  into :rsymbolref;
  if (:rsymbolref is null) then begin
    select first 1 dn.symbol
      from dokumnag dn
        join listywysdpoz lwp on (dn.ref = lwp.dokref)
      where lwp.dokument = :listywysdref
      order by dn.ref
    into :rsymbolref;
  end

  rnrpaczki = 0;
  for
    select lwo.nrpaczki, lwo.waga
      from listywysdroz_opk lwo
      where lwo.listwysd = :listywysdref
        and lwo.rodzic is null
    into :rsymbolpaczki, :rwagapaczki
  do begin
    --kod kreskowy
    rkodkresk = :rsymbolpaczki;

    rgls2p = substring(:rkodkresk from 1 for 2);
    rgls2k = substring(:rkodkresk from 3 for 4);
    rglslistwysd = substring(:rkodkresk from 5 for coalesce(char_length(:rkodkresk) , 0)); -- [DG] XXX ZG119346

    execute procedure xk_cyfractrl_gls(:rkodkresk) returning_values :rcyfractrl;

    rkodkresk = :rkodkresk||:rcyfractrl;

    rnrpaczki = :rnrpaczki + 1;
    suspend;

    rpobranie = 0;
  end
end^
SET TERM ; ^
