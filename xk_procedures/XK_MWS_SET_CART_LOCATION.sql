--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_SET_CART_LOCATION(
      MWSORD integer,
      CARTSYMB varchar(20) CHARACTER SET UTF8                           ,
      EMWSCONSTLOC varchar(20) CHARACTER SET UTF8                           )
   as
declare variable cart integer;
declare variable autoclearcarts integer;
declare variable ok smallint_id; --[PM] XXX
declare variable zrodlo smallint_id;
declare variable sposdost smallint_id;
begin
  select ref from mwsconstlocs where symbol = :cartsymb
  into :cart;



  if (:emwsconstloc is not null and :emwsconstloc <> '') then
  begin
    select distinct  d.zrodlo, d.sposdost
      from mwsordcartcolours c
        left join dokumnag d on (c.docid = d.ref)
      where c.mwsord = :mwsord
        and c.cart = :cart
        --and c.status = 1
    into  :zrodlo, :sposdost;


    ok = 0;
    emwsconstloc = upper(trim(emwsconstloc));
 --  exception universal ' odbior '||:emwsconstloc||' ok '||ok||' sposdos '|| coalesce(sposdost, 'brak');
    -- weryfikujemy czy mozemy dokument odlozyc na wskaznae miejsce
    -- dokumenty z innym sposobem dostawy niz 'ODBIÓR WŁasny' odkladamy na lokacje odkladcza typu PXX gzie XX to czyfry
     if (emwsconstloc similar to 'P[[:DIGIT:]]{3}' and  coalesce(sposdost, 0) <> 1 ) then
      ok = 1;
    -- To nie weim co to jest (jakis spadek z Logboxu jeszcze)
    if (ok = 0 and emwsconstloc similar to 'R\-[[:DIGIT:]]{2}\-[[:DIGIT:]]{2}' escape '\') then
      ok = 1;


    -- dla sposobu dostawy 'ODBIÓR WŁASNY' lokacja odkadcza to ODW
    if (ok  = 0 and emwsconstloc = 'ODW' and coalesce(sposdost, 0) = 1) then
      ok = 1;
    else if (coalesce(sposdost, 0) = 1) then
      exception universal 'WT ze sposobem dostawy odbiór własny, możesz odstawić tylko na pole odstawcze ODW.';

    if (ok = 0) then
      exception universal 'Odkladasz towar na niewlasciwa lokacje.';


    update mwsacts ma set ma.emwsconstloc = :emwsconstloc
      where ma.mwsord = :mwsord
        and ma.quantityc = ma.quantity
        and ma.mwsconstloc = :cart
        and coalesce(ma.emwsconstloc,'') = ''
        and ma.status = 5;

    update mwsacts ma set ma.mwsconstloc = null
      where ma.mwsord = :mwsord
        and ma.status < 5
        and ma.quantityc <> ma.quantity
        and ma.mwsconstloc = :cart;

    update mwsordcartcolours c
      set c.status = 1, c.mwsconstlocsymb = :emwsconstloc
      where c.mwsord = :mwsord
        and c.cart = :cart
        and c.status = 0;

    /*$$IBEC$$ select x_autoclearcarts from mwsconstlocs where symbol = :emwsconstloc
    into :autoclearcarts;
    if (coalesce(:autoclearcarts,0) = 1) then
    begin
      update mwsacts set mwsconstloc = null where mwsord = :mwsord;
      update mwsordcartcolours c set c.status = 2 where c.mwsord = :mwsord;
    end $$IBEC$$*/
  end
  else --[PM] XXX
    exception universal 'Wskaz miejsce odkladcze!'; --[PM] XXX
end^
SET TERM ; ^
