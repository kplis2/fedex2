--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_POW_OPTIMALIZE_CALC(
      WH varchar(3) CHARACTER SET UTF8                           ,
      BDATE date,
      AAREA numeric(14,4),
      BAREA numeric(14,4),
      FACTOR numeric(14,4),
      RECALC smallint)
   as
declare variable locav numeric(14,2);
declare variable alocav numeric(14,2);
declare variable blocav numeric(14,2);
declare variable clocav numeric(14,2);
declare variable cnt integer;
declare variable whtmp varchar(3);
declare variable optabc char(1);
declare variable goodf varchar(40);
declare variable vers integer;
declare variable deep smallint;
declare variable maxdist numeric(14,2);
declare variable maxcpos numeric(14,2);
declare variable maxbpos numeric(14,2);
declare variable maxapos numeric(14,2);
declare variable minaord numeric(14,2);
declare variable minbord numeric(14,2);
declare variable mincord numeric(14,2);
declare variable maxcord numeric(14,2);
declare variable takefromup smallint;
declare variable mwsconstloc integer;
declare variable poscnt integer;
begin
  if (factor is null) then factor = 0.95; -- wspolczynnik zapelnienia lokacji towarami ABC
  -- naliczamy tabele obrotow w zadanym okresie i dzielimy towary na towary A,B,C
  delete from xk_goods_relocate where (const is null or const = 0);
  insert into xk_goods_relocate (wh, good, vers, poscnt, actmwsconstloc, actmwsconstlocs, actdist, actdeep,
      bestmwsconstloc, bestmwsconstlocs, bestdist, shortage, ordered, aarea, barea, palcnt, opt,
      quantity, interval)
    select :wh, xk.good, xk.vers, xk.poscnt, xk.actmwsconstloc, xk.actmwsconstlocsymb, xk.actdist, xk.actdeep,
        xk.mwsconstlocsymb, xk.mwsconstlocsymbs, xk.dist, xk.shortage, xk.ordered, :aarea, :barea, xk.palcnt, 0,
        xk.quantity, 14
      from xk_mws_pow_optimalize_br(:wh,null, :bdate,:recalc) xk;
  select cast(locav as numeric(14,2)), :factor*cast(alocav as numeric(14,2)),
      :factor*cast(blocav as numeric(14,2)), :factor*cast(clocav as numeric(14,2))
    from xk_mws_abc_anal(:wh,:bdate,:aarea, :barea)
    into locav, alocav, blocav, clocav;
  blocav = 1.5 * blocav;
  clocav = 2 * clocav;
  locav = alocav + blocav + clocav;
  cnt = 1;
  for
    select :wh, w.ref, r.good, r.poscnt
      from xk_goods_relocate r
        join wersje w on (w.ktm = r.good)
      where r.wh = :wh
      order by r.boxcntperperiod desc
      into whtmp, vers, goodf, poscnt
  do begin
    if (cnt <= alocav) then optabc = 'A';
    else if (cnt <= blocav + alocav) then optabc = 'B';
    else optabc = 'C';
    takefromup = 0;
    update xk_goods_relocate set optabc = :optabc, ord = :cnt, takefromup = :takefromup
      where wh = :whtmp and vers = :vers;
    cnt = cnt + 1;
  end
  cnt = 0;
  deep = 0;
  -- wspolczynniki do podzialu stref A,B,C w zaleznosci od ilosci sprzedanych pozcyji
  select cast(max(poscnt) as numeric(14,2)), cast(min(ord) as numeric(14,2)), cast(max(ord) as numeric(14,2))
    from xk_goods_relocate
    where optabc = 'C' and wh = :wh
    into maxcpos, mincord, maxcord;
  if (maxcpos is null) then maxcpos = 1;
  if (mincord is null) then mincord = 1;
  if (maxcord is null) then maxcord = 1;
  select cast(max(poscnt) as numeric(14,2)), cast(min(ord) as numeric(14,2))
    from xk_goods_relocate
    where optabc = 'B' and wh = :wh
    into maxbpos, minbord;
  if (maxbpos is null) then maxbpos = 1;
  if (minbord is null) then minbord = 1;
  select cast(max(poscnt) as numeric(14,2)), cast(min(ord) as numeric(14,2))
    from xk_goods_relocate
    where optabc = 'A' and wh = :wh
    into maxapos, minaord;
  if (maxapos is null) then maxapos = 1;
  if (minaord is null) then minaord = 1;
  select max(distfromstartarea)
    from mwsconstlocs
    where wh = :wh
    into maxdist;
  if (maxdist is null) then maxdist = 100000;
  -- okrelenie najlepszej odleglosci dla towaru w zaleznosci od optymalnosci
  --update xk_goods_relocate x set x.optdist =
    --  case when x.optabc = 'A' then x.ord/(:minbord - 1) * :aarea
      --    when x.optabc = 'B' then :aarea + (x.ord - :minbord)/(:mincord - :minbord) * (:barea - :aarea)
        --  else :barea + (x.ord - :mincord)/:maxcord * (:maxdist - :barea) end;
end^
SET TERM ; ^
