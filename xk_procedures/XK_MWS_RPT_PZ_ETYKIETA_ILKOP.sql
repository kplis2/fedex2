--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_RPT_PZ_ETYKIETA_ILKOP(
      MWSORD integer,
      PALGROUP integer,
      ILKOP integer)
  returns (
      GOODSTRING varchar(120) CHARACTER SET UTF8                           ,
      GOODSTRING2 varchar(120) CHARACTER SET UTF8                           ,
      GOODSTRING3 varchar(120) CHARACTER SET UTF8                           ,
      GOODSTRING4 varchar(120) CHARACTER SET UTF8                           ,
      GOODSTRING5 varchar(120) CHARACTER SET UTF8                           ,
      GOODSTRING6 varchar(120) CHARACTER SET UTF8                           ,
      GOODSTRING7 varchar(120) CHARACTER SET UTF8                           ,
      GOODSTRING8 varchar(120) CHARACTER SET UTF8                           ,
      GOODSTRING9 varchar(120) CHARACTER SET UTF8                           ,
      PRIORITY smallint)
   as
declare variable licznik smallint;
declare variable ktm varchar(20);
declare variable QUANTITYSTRING varchar(200);
declare variable ilosc integer;
declare variable przelicznik integer;
declare variable paleta smallint;
declare variable mix smallint;
declare variable mwsconstloc integer;
declare variable cnt integer;
declare variable vers integer;
declare variable magbreak smallint;
declare variable wh varchar(3);
declare variable docid integer;
declare variable symboldostawy varchar(100);
begin
  ilkop = ilkop - 1;
  for
    select goodstring, goodstring2, goodstring3, goodstring4, goodstring5, goodstring6, goodstring7, goodstring8, goodstring9, priority
      from xk_mws_rpt_pz_etykieta(:mwsord, :palgroup), syskopie(:ilkop)
      into goodstring, goodstring2, goodstring3, goodstring4, goodstring5, goodstring6, goodstring7, goodstring8, goodstring9, priority
  do begin
    suspend;
  end
end^
SET TERM ; ^
