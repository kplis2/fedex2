--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_BEST_LOCATION(
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      REFMWSACT integer,
      REFMWSORD integer,
      MWSORDTYPE integer,
      WH varchar(3) CHARACTER SET UTF8                           ,
      WHSEC integer,
      WHAREA integer,
      MAXLEVEL smallint,
      PRIORITY smallint,
      PAL smallint,
      MIX smallint,
      MWSCONSTLOCB integer,
      PALTYPE varchar(40) CHARACTER SET UTF8                           ,
      PALW numeric(14,2),
      PALL numeric(14,2),
      PALH numeric(14,2),
      REFILLTRY smallint,
      QUANTITY numeric(14,4))
  returns (
      MWSCONSTLOCL integer,
      MWSPALLOCL integer,
      WHAREAL integer,
      WHAREALOGL integer,
      REFILL smallint)
   as
declare variable mwsortypedets smallint;
begin
  select t.mwsortypedets
    from mwsordtypes t
    where t.ref = :mwsordtype
    into mwsortypedets;
  if (mwsortypedets is null) then
    exit;
  if (refilltry is null) then refilltry = 0;
  refill = 0;
  if (paltype is not null) then
  begin
    if (pall is null) then pall = 120;
    if (palw is null) then palw = 90;
    if (palh is null) then palh = 200;
  end
  if (whsec is null) then
    select whsec from whareas where ref = :wharea into whsec;
  -- zlecenie PZ - towar w ramch sektora rozadunku
  if (mwsortypedets = 1) then
  begin
    execute procedure XK_MWS_GET_BEST_LOCATION_1(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end else if (mwsortypedets = 2) then
  begin
    execute procedure XK_MWS_GET_BEST_LOCATION_2(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end
  else if (mwsortypedets = 3) then
  begin
    execute procedure XK_MWS_GET_BEST_LOCATION_3(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end
  else if (mwsortypedets = 4) then
  begin
    execute procedure XK_MWS_GET_BEST_LOCATION_4(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end
  else if (mwsortypedets = 5) then
  begin
    execute procedure XK_MWS_GET_BEST_LOCATION_5(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end
  else if (mwsortypedets = 6) then
  begin
    execute procedure XK_MWS_GET_BEST_LOCATION_6(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end
  else if (mwsortypedets in (7,16,17,18,19)) then
  begin
    execute procedure XK_MWS_GET_BEST_LOCATION_7(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end
  else if (mwsortypedets = 8) then
  begin
    execute procedure XK_MWS_GET_BEST_LOCATION_8(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end
  else if (mwsortypedets = 9) then
  begin
    execute procedure XK_MWS_GET_BEST_LOCATION_9(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end
  else if (mwsortypedets = 10) then
  begin
    execute procedure XK_MWS_GET_BEST_LOCATION_10(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end
  else if (mwsortypedets = 11) then
  begin
    execute procedure XK_MWS_GET_BEST_LOCATION_11(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end
  else if (mwsortypedets = 12) then
  begin
    execute procedure XK_MWS_GET_BEST_LOCATION_12(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end
  else if (mwsortypedets = 13) then
  begin
    execute procedure XK_MWS_GET_BEST_LOCATION_13(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end
  else if (mwsortypedets = 14) then
  begin
    execute procedure XK_MWS_GET_BEST_LOCATION_14(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end else if (mwsortypedets = 15) then
  begin
    execute procedure XK_MWS_GET_BEST_LOCATION_15(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end
 else if (mwsortypedets = 20) then
  begin
    execute procedure XK_MWS_GET_BEST_LOCATION_20(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end
  else
    exception MWSORDTYPE_NOT_DEFINED;
end^
SET TERM ; ^
