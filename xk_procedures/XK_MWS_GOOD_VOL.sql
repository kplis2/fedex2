--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GOOD_VOL(
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      QUANTITY numeric(15,4),
      MODE smallint,
      MWSCONSTLOCREF integer)
  returns (
      VOLUME numeric(15,4),
      LOADPERCENT numeric(14,2),
      RATIO numeric(15,4),
      QUANTITYSTRING varchar(255) CHARACTER SET UTF8                           ,
      PIECES integer,
      PACKAGES integer,
      BOXES integer,
      BIGBAGS integer)
   as
declare variable boxvolume numeric(15,4);
declare variable obj_palety numeric(15,4);
declare variable obj_lokacji numeric(15,4);
begin
  obj_palety = 0.80*1.20*1.8; --objetosc jednej palety
  --objetosc towaru
  if (volume is null) then volume = 0;
  if (mode = 0) then
  begin
    loadpercent = (volume/obj_palety) * 100;
  end
  else if (mode = 1) then
  begin
    select mc.volume
      from mwsconstlocs mc
      where mc.ref = :mwsconstlocref
      into :obj_lokacji; --objetosc lokacji
    if (obj_lokacji is null or obj_lokacji = 0) then
      obj_lokacji = obj_palety;
    loadpercent = (volume/obj_lokacji) * 100;
  end
  if (volume = 0) then
    loadpercent = 0;
  suspend;
end^
SET TERM ; ^
