--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_NAZWAIKTM_DOK(
      DOKUMPOZREF integer,
      KTM varchar(255) CHARACTER SET UTF8                           ,
      NAZWA varchar(1000) CHARACTER SET UTF8                           )
  returns (
      RKTM varchar(255) CHARACTER SET UTF8                           ,
      RNAZWA varchar(1000) CHARACTER SET UTF8                           )
   as
declare variable dokumnagref integer;
DECLARE VARIABLE GRUPAKUPIECKA INTEGER;
DECLARE variable dokumnagodbiorcaid INTEGER;
declare variable wersjaref integer;
declare variable jedn integer;
declare variable klient integer;
begin
  select dokumpoz.dokument,dokumpoz.wersjaref,dokumpoz.jedno
    from dokumpoz where ref = :dokumpozref
    into :dokumnagref,:wersjaref,:jedn;
  dokumnagodbiorcaid = null;
  klient = null;
  grupakupiecka = null;
  select dokumnag.odbiorcaid, dokumnag.klient from dokumnag where ref = :dokumnagref into :dokumnagodbiorcaid, :klient;
  if(:dokumnagodbiorcaid is not null) then
    select odbiorcy.grupakup from odbiorcy where ref = :dokumnagodbiorcaid into :grupakupiecka;
  if(:grupakupiecka is null and :klient is not null) then
    select klienci.grupakup from klienci where ref = :klient into :grupakupiecka;
  if(:grupakupiecka is null) then begin
    rktm = ktm;
    rnazwa = nazwa;
  end else begin
    rktm = null;
    rnazwa = null;
    select grupykupofe.symbol,grupykupofe.nazwa
    from grupykupofe
    where grupakup=:grupakupiecka and wersjaref=:wersjaref and jedn=:jedn
    into :rktm, :rnazwa;
    if(:rktm is null or :rktm='') then rktm = :ktm;
    if(:rnazwa is null or :rnazwa='') then rnazwa = :nazwa;
  end
  suspend;
end^
SET TERM ; ^
