--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_CHECK_MWSORDCART(
      CART integer,
      CARTSYMB varchar(20) CHARACTER SET UTF8                           ,
      MWSORD integer,
      DOCID integer,
      MODE integer)
  returns (
      CURRENTCART integer,
      STATUS smallint)
   as
declare variable aref integer;
declare variable oref integer;
begin
  status = 1;

  /*$$IBEC$$ exception test_break
    coalesce(:cartsymb,'')||';'||
    coalesce(:mwsord,0)||';'||
    coalesce(:docid,0)||';'||
    coalesce(:mode,''); $$IBEC$$*/

  if (coalesce(:cart,0) = 0 and cartsymb similar to 'W[[:DIGIT:]]{3}') then
    select ref from mwsconstlocs l where l.symbol = :cartsymb
    into :cart;

  currentcart = :cart;

  if ((:mode = 0 and
        exists(select first 1 1 from mwsordcartcolours c
          where c.cart = :cart and ((c.status = 1) or (c.status = 0 and (c.mwsord <> :mwsord or c.docid <> :docid))))) or
      (:mode = 1 and
        not exists(select first 1 1 from mwsordcartcolours c
          where c.cart = :cart and (c.status = 0 and c.mwsord = :mwsord)))) then
  begin
    status = 0;
    currentcart = 0;
  end
end^
SET TERM ; ^
