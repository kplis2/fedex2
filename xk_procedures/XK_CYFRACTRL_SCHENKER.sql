--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_CYFRACTRL_SCHENKER(
      WARTOSC varchar(255) CHARACTER SET UTF8                           )
  returns (
      CYFRACTRL integer)
   as
declare variable poz integer;
declare variable suma integer;
declare variable parz smallint;
declare variable tmpint1 integer;
declare variable tmpint2 integer;
declare variable tmpint3 integer;
begin
  poz = 1;
  suma = 0;
  parz = 0;
  tmpint1 = 0;
  tmpint2 = 0;
  tmpint3 = 0;
  while(coalesce(char_length(:wartosc),0)>=:poz) do begin -- [DG] XXX ZG119346
--    tmpint1 = cast(substring(:wartosc,(coalesce(char_length(:wartosc) from 0)-:poz), (coalesce(char_length(:wartosc) for 0)-:poz)) as integer);  --od prawej do lewej -- [DG] XXX ZG119346
    tmpint1 = cast(substring(:wartosc from :poz for :poz) as integer); --od lewej do prawej
    if (:parz = 0) then begin
      tmpint1 = (:tmpint1 * 2);
      if (:tmpint1 > 9) then begin
        tmpint2 = cast(:tmpint1/10 as integer);
        tmpint3 = mod(:tmpint1,10);
        tmpint1 = :tmpint2 + :tmpint3;
      end
        suma = :suma + :tmpint1;
        parz = 1;
    end
    else begin
      suma = :suma + :tmpint1;
      parz = 0;
    end
    poz = :poz + 1;
  end
  cyfractrl = mod(:suma, 10);
  cyfractrl = 10 - :cyfractrl;
  cyfractrl = mod(:cyfractrl, 10);
  suspend;
end^
SET TERM ; ^
