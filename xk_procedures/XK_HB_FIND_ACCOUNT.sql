--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_HB_FIND_ACCOUNT(
      RKDOKNAG integer)
   as
declare variable TICACCOUNT varchar(26);
declare variable DICTPOS integer;
declare variable PM char(1);
declare variable RKDEFOPER varchar(25);
declare variable RKPOZOPER integer;
declare variable DLA varchar(255);
declare variable COMPANY integer;
declare variable OPIS varchar(1024);
declare variable I integer;
declare variable L61 varchar(1024);
declare variable STANOWISKO varchar(2);
declare variable BANK varchar(30);
begin
/* procedura do znajdowania klienta po indywidualnym rachunku bankowym
    dla kazdego klienta trzeba osobno zmodyfikowac w zaleznosci od sposobu wyszukiwania
    jaki sobie zazyczy */
  i = 0;
  select N.pm, N.ticaccount, S.company, N.mt940l61, N.opis, N.stanowisko, B.symbol
    from rkdoknag N
      join rkstnkas S on (N.stanowisko = S.kod)
      join bankacc B on (B.stanbank = S.kod)
    where N.ref = :rkdoknag
    into :pm, :ticaccount, :company, :l61, :opis, :stanowisko, :bank;
  if (ticaccount <> '') then
  begin
    if (pm = '+') then
    begin
        select dictpos from bankaccounts where replace(account, ' ', '') = :ticaccount
          and bankacc = :bank and dictdef = 1
          into :dictpos;
    end

    if (dictpos is not null) then
    begin
      rkdefoper = pm;
      if (pm = '+') then
        rkdefoper = rkdefoper || 'NALEŻNO';

      execute procedure name_from_dictposref(1, dictpos)
        returning_values dla;
      update rkdoknag set slodef = 1, slopoz = :dictpos, dla = :dla,
          operacja = :rkdefoper
        where ref = :rkdoknag;

       select dompozoper from rkdefoper
         where symbol = :rkdefoper
         into :rkpozoper;
       if (rkpozoper is not null) then
         update rkdokpoz set pozoper = :rkpozoper where dokument = :rkdoknag;
       dictpos = null;
    end
  end
end^
SET TERM ; ^
