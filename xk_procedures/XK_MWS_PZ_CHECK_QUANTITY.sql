--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_PZ_CHECK_QUANTITY(
      MWSACT integer)
  returns (
      ILOSCMAX numeric(14,4),
      ILOSCPOTW numeric(15,4))
   as
declare variable DOCPOSID integer;
declare variable wh defmagaz_id;
declare variable mwsordtype integer;
declare variable altposmode smallint;
begin
  select a.docposid, a.wh, a.mwsordtype
    from mwsacts a where a.ref = :mwsact
  into :docposid, :wh, :mwsordtype;

    select m.altposmode from mwsordtypes4wh m
    where m.mwsordtype = :mwsordtype
      and m.wh = :wh
  into :altposmode;
  if (:altposmode is null) then altposmode = 0;

  select case when dn.mwsdisposition = 1 or :altposmode = 1 then dp.ilosc else dp.iloscl end
    from dokumpoz dp
      join dokumnag dn on (dn.ref = dp.dokument)
    where dp.ref = :docposid
    into :iloscmax;

  select coalesce(sum(quantityc),0)
    from mwsacts
    where docposid = :docposid
    into :iloscpotw;
end^
SET TERM ; ^
