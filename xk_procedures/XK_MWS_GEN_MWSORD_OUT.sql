--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GEN_MWSORD_OUT(
      DOCID DOKUMNAG_ID,
      DOCGROUP DOKUMNAG_ID,
      DOCPOSID DOKUMPOZ_ID,
      DOCTYPE CHAR_1,
      MWSORD MWSORDS_ID,
      MWSACT MWSACTS_ID,
      RECALC SMALLINT_ID,
      MWSORDTYPE MWSORDTYPES_ID)
  returns (
      REFMWSORD MWSORDS_ID)
   as
declare variable REC smallint_id;
declare variable WH defmagaz_id;
declare variable DOCWH defmagaz_id;
declare variable DOCWH2 defmagaz_id;
declare variable DOCWH2DESC STRING255;
declare variable STOCKTAKING smallint_id;
declare variable PRIORITY smallint_id;
declare variable BRANCH oddzial_id;
declare variable PERIOD period_id;
declare variable FLAGS string40;
declare variable SYMBOL symbol_id;
declare variable COR smallint_id;
declare variable QUANTITY ilosci_mag;
declare variable TODISP ilosci_mag;
declare variable TOINSERT ilosci_mag;
declare variable GOOD ktm_id;
declare variable VERS wersje_id;
declare variable OPERSETTLMODE smallint;
declare variable MWSCONSTLOCP integer;
declare variable MWSPALLOCP integer;
declare variable AUTOLOCCREATE smallint;
declare variable WHAREAG integer;
declare variable POSFLAGS varchar(40);
declare variable DESCRIPTION varchar(1024);
declare variable POSDESCRIPTION varchar(1024);
declare variable LOT integer;
declare variable CONNECTTYPE smallint;
declare variable SHIPPINGAREA varchar(3);
declare variable QUANTITYONLOCATION numeric(15,4);
declare variable MAXNUMBER integer;
declare variable DOCOBJ numeric(14,4);
declare variable PALTYPE varchar(40);
declare variable SHIPPINGTYPE integer;
declare variable NEXTMWSORD smallint;
declare variable MAXPALVOL numeric(14,4);
declare variable TAKEFULLPAL smallint;
declare variable FROMMWSCONSTLOC integer;
declare variable PALGROUPMM integer;
declare variable TAKEFULLPALMM integer;
declare variable TAKEFROMMWSCONSTLOCMM integer;
declare variable TAKEFROMMWSPALLOCMM integer;
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable SLOKOD varchar(40);
declare variable DOCREAL smallint;
declare variable PALQUANTITY integer;
declare variable DIVWHENMORE integer;
declare variable DIVWHENMORES varchar(100);
declare variable DONTDIV smallint;
declare variable POSLEFT integer;
declare variable MWSPALLOCLPAL integer;
declare variable MWSGENPARTSS varchar(100);
declare variable MWSGENPARTS smallint;
declare variable POSREAL smallint;
declare variable NEXTPOS smallint;
declare variable DOCID1 integer;
declare variable DOCGROUP1 integer;
declare variable DOCPOSID1 integer;
declare variable VERS1 integer;
declare variable QUANTITY1 numeric(14,4);
declare variable TAKEFROMZEROS varchar(100);
declare variable TAKEFROMZERO smallint;
declare variable LOCSEC integer;
declare variable BUFFORS varchar(100);
declare variable BUFFOR smallint;
declare variable INDYVIDUALLOT smallint;
declare variable MAGBREAK smallint;
declare variable MULTI smallint_id;
declare variable MAXSINGLEDOCPOS smallint_id;
declare variable OUT smallint_id;
declare variable mwsordstatus smallint_id;
declare variable newmwsord smallint_id;
declare variable magbreakmag smallint_id;
declare variable multicnt smallint_id;
declare variable mwsorddivide smallint_id;
declare variable x_partia date_id;              -- XXX KBI
declare variable x_serial_no integer_id;        -- XXX KBI
declare variable x_slownik integer_id;          -- XXX KBI
declare variable refmwsordtmp integer_id;
declare variable typ string3;
declare variable mag2 string20;
declare variable mwsconstlocl integer_id;
declare variable symbolords string60;
begin
  if (:docgroup is null) then docgroup = :docid;
  if (:doctype is null) then doctype = 'M';
  if (:recalc is null) then recalc = 0;
  docposid = null;
  -- Ldz przesuniecie na lokacje marketing
  select d.typ, d.mag2
    from dokumnag d
      where d.ref = :docid
  into :typ,  :mag2;
  -- Ldz dla dokumentu RW, z magazynem2 = marketing robimy zlecenie WTM -> wydanie na marketing zdjecia
  if (coalesce(typ, '') = 'RW' and coalesce(mag2,  '') = 'MARKETING' and :mwsordtype is null) then begin
    --mwsconstlocl = 13424;   lokacja marketing, ale w tym przypadku ma zniknac z naszych stanow
    mwsordtype = 36;
  end
   --<-- Ldz
  if (:mwsordtype is null) then
    select first 1 t.ref, t.stocktaking, t.opersettlmode, t.autoloccreate
      from mwsordtypes t
      where t.ref = 6
      order by t.ref
    into :mwsordtype, :stocktaking, :opersettlmode, :autoloccreate;
  else
    select stocktaking, opersettlmode, autoloccreate
      from mwsordtypes
      where ref = :mwsordtype
    into :stocktaking, :opersettlmode, :autoloccreate;

  -- czy wolno brac towar z lokacji niedostepnych dla pickerow "0"
  execute procedure get_config('MWSTAKEFROMBUFFOR',2) returning_values :buffors;
  if (:buffors is null or: buffors = '') then
    buffor = 0;
  else
    buffor = cast(:buffors as smallint);
  execute procedure get_config('MWSTAKEFROMZERO',2) returning_values :takefromzeros;
  if (:takefromzeros is null or :takefromzeros = '') then
    takefromzero = 0;
  else
    takefromzero = cast(:takefromzeros as smallint);
  -- dziel zlecenie gdy wiecej operacji niz ..
  execute procedure get_config('MWSDIVWHENMORE',2) returning_values :divwhenmores;
  if (:divwhenmores is not null and :divwhenmores <> '') then
    divwhenmore = cast(:divwhenmores as integer);
  else
    divwhenmore = 100;
  -- pozwalaj szykowac czesciowe pozycje na dokumentach
  execute procedure get_config('MWSGENPARTS',2) returning_values :mwsgenpartss;
  if (:mwsgenpartss is not null and :mwsgenpartss <> '') then
    mwsgenparts = cast(:mwsgenpartss as smallint);
  else
    mwsgenparts = 0;

  select first 1 d.wydania, d.koryg, d.zewn,
      d.magazyn, d.mag2, trim(coalesce(d.mwsmag,d.magazyn)),
      d.uwagi, d.priorytet, d.oddzial, d.okres, d.flagi,
      d.symbol, d.kodsped, d.sposdost,
      d.takefullpal, d.palgroup, d.takefrommwsconstloc,
      d.takefrommwspalloc, 1, k.ref, substring(k.fskrot from 1 for 40),
      d.mwsdocreal, d.iloscpalet
    from dokumnag d
      left join klienci k on (k.ref = d.klient)
      left join sposdost s on (d.sposdost = s.ref)
    where d.ref = :docid
    into :rec, :cor, :out,
      :docwh, :docwh2, :wh,
      :description, :priority, :branch, :period, :flags,
      :symbol, :shippingarea, :shippingtype,
      :takefullpalmm, :palgroupmm, :takefrommwsconstlocmm,
      :takefrommwspallocmm, :slodef, :slopoz, :slokod,
      :docreal, :palquantity;

  -- cale zlecenie z towatami z gornych pieter mozna szykowac tylko jak nie
  if (takefullpalmm is null) then takefullpalmm = 0;
  if (docreal is null) then docreal = 0;
  --odwrocenie ze wzgledu na nazwe zmiennej
  if (:rec = 0) then rec = 1; else rec = 0;

  select d.mwsorddivide from defmagaz d where d.symbol = :wh
  into :mwsorddivide;

  --MKD priority = 2; --JO: dlaczego nie wiem
   if (priority is null ) then
    priority = 0;

  multi = 0;
  if (:rec = 0 and :out = 0) then
  begin
    select coalesce(opis,symbol) from defmagaz where symbol = :docwh2
    into :docwh2desc;
    description = 'Wydanie na magazyn: '||:docwh2desc||', '||:description;
    multi = 0;
  end
  --else
  if (:rec = 0) then
  begin
    select coalesce(m.multipicking,0), m.maxsingledocpos
      from mwsordtypes4wh m
      where m.mwsordtype = :mwsordtype
        and m.wh = :wh
    into :multi, :maxsingledocpos;
  multi =0; -- XXX Bo zwykle WT nie bedą chodzily jako multipiking, multipiking bedzie z likacji kardex i zlecenia sa modyfikowane na koncu tej procedury
  -- zakądamy nagowek zlecenia
  execute procedure gen_ref('MWSORDS') returning_values refmwsord;
  select okres from datatookres(current_date,0,0,0,0) into period;
  insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator, priority,
      description, wh,  regtime, timestartdecl, branch, period,
      flags, docsymbs, cor, rec, status, shippingarea, shippingtype, takefullpal,
      slodef, slopoz, slokod, palquantity, repal, multi, multicnt)
    values (:refmwsord, :mwsordtype, :stocktaking, :doctype, :docgroup, :docid, null, :priority,
        :description, :wh, current_timestamp, null, :branch, :period,
        :flags, :symbol, :cor, :rec, 0, :shippingarea, :shippingtype, 0,
        :slodef, :slopoz, :slokod, :palquantity, 0, :multi, 0);

  --sprawdzenie czy mozna pobrac pelna palete do szykowania
  frommwsconstloc = null;
  select first 1 mwsconstloc from xk_mws_take_full_pal_check(:wh,:docgroup,:refmwsord,0)
    into :frommwsconstloc;
  if (frommwsconstloc is not null) then takefullpal = 1;
  if (takefullpalmm = 1) then takefullpal = 1;
  if (takefullpal is null) then takefullpal = 0;                                -- XXX kbi

    update mwsords o set o.takefullpal = :takefullpal where o.ref = :refmwsord; -- XXX kbi
  


  if (:multi = 1 and takefullpal = 0) then     -- XXX kbi takefullpal = 0
    begin
      execute procedure xk_mws_check_multidoc(:wh, :mwsordtype, :docid, :docgroup)
        returning_values :multi;
    end
  end

  -- okreslenie czy mozna dolaczyc pozycje zlecenia do juz istniejacego
  connecttype = 0;
  if (takefullpal = 0) then
    execute procedure xk_mws_find_mwsordref(:mwsordtype, :refmwsord, null, :docgroup,
        :docid, :docposid, :wh, :branch, null, :multi, :mwsorddivide)
      returning_values :refmwsordtmp, :connecttype, :paltype, :docobj, :maxpalvol;

  -- jezeli nie mozna nigdzie dolaczyc nowych operacji do generuje nowe zlecenie
  newmwsord = 0;
  if (connecttype = 0 ) then
  begin
    newmwsord = 1;
   end else begin
    -- XXX KBI
    if (not exists(select first 1 1 from mwsacts a where a.mwsord = :refmwsord)) then
      delete from mwsords where ref = :refmwsord;
    -- XXx Koniec KBI
    refmwsord = refmwsordtmp;
  end
  -- jezeli mozna dolaczyc do zlecenia dla grupy, to dodaje symgol do opisu

  if (connecttype = 1 or connecttype = 3) then
    update mwsords set docsymbs = docsymbs||';'||:symbol where docgroup = :docgroup
      and docsymbs not like '%'||:symbol||'%' and coalesce(char_length(docsymbs),0) < 51;

  --nie dziel jesli zlecenie multi
  if (:connecttype = 4) then dontdiv = 1;
  else dontdiv = 0;

  -- zarejestrowanie co trzeba dodac do zlecenia z biezacego dokumentu magazynowego
  for
    select dp.ktm, p.docgroup, p.docid, p.docposid,
        case when (d.mwsdisposition = 1 or coalesce(t1.altposmode,0) = 1) then dp.ilosc else dp.iloscl end - coalesce(dp.ilosconmwsacts,0),
        dp.wersjaref, coalesce(dp.dostawa,0)
      from docpos2real p
        left join dokumnag d on (d.ref = p.docid)
        left join dokumpoz dp on (dp.ref = p.docposid)
        left join towary t on (t.ktm = dp.ktm)
        left join dokumpoz p1 on (p1.ref = dp.alttopoz and dp.fake = 1)
        left join towary t1 on (t1.ktm = p1.ktm)
      where /*p.docid = :docid*/  p.docgroup = :docgroup and p.doctype = 'M'
        and ((coalesce(dp.havefake,0) = 0 and coalesce(dp.fake,0) = 0) or
             (coalesce(dp.fake,0) = 1 and t1.usluga <> 1 and coalesce(t1.altposmode,0) = 1))
        and (t.paleta <> 1 or t.paleta is null)
        and ((d.akcept = 1 and d.mwsdisposition = 0)
            or (d.akcept = 0 and d.mwsdisposition = 1))
        and coalesce(d.mwsmag,d.magazyn) = :wh
        and (p.posreal = 1
           or (:mwsgenparts = 1 and p.posreal = 5)
           or p.posreal = 2)
        and dp.genmwsordsafter = 1
        and t.usluga <> 1
      into :good, :docgroup1, :docid1, :docposid1, :quantity1, :vers1, :lot
  do begin
    if (not exists (select mwsord from mwsdocpostomwsord where mwsord = :refmwsord
        and docgroup = :docgroup1 and docposid = :docposid1 and doctype = 'M')
    ) then
      insert into mwsdocpostomwsord (mwsord, docgroup, docid, docposid, doctype, quantity, vers, lot)
        values (:refmwsord, :docgroup, :docid1, :docposid1, 'M', :quantity1, :vers1, :lot);
    else
      update mwsdocpostomwsord set quantity = :quantity1, lot = :lot
        where mwsord = :refmwsord and docid = :docid1 and docposid = :docposid1 and doctype = 'M';
  end

  -- znalezienie max. numeru operacji - kolejne zwiekszamy i jeden.
  select max(number) from mwsacts where mwsord = :refmwsord into :maxnumber;
  if (:maxnumber is null) then maxnumber = 0;


  if (takefullpal = 0 and (docreal = 2 or docreal = 4)) then
  begin
    delete from mwsords where ref = :refmwsord;
    update dokumnag set genmwsordsafter = 0 where ref = :docid;
    exit;
  end

  nextmwsord = 0;
  nextpos = 1;
  recalc = 0;
  while (exists(
           select p.ref
             from mwsdocpostomwsord m
               join dokumpoz p on (p.ref = m.docposid and m.mwsord = :refmwsord and m.doctype = 'M')
               left join docpos2real pr on (pr.docposid = p.ref)
               left join dokumnag d on (d.ref = p.dokument)
               join towary t on (p.ktm = t.ktm)
               left join dokumpoz p1 on (p1.ref = p.alttopoz and p.fake = 1)
               left join towary t1 on (t1.ktm = p1.ktm)
             where p.genmwsordsafter = 1
               and ((d.akcept = 1 and d.mwsdisposition = 0)
                 or (d.akcept = 0 and d.mwsdisposition = 1))
               and m.quantity > 0
               and ((coalesce(p.havefake,0) = 0 and coalesce(p.fake,0) = 0) or
                    (coalesce(p.fake,0) = 1 and t1.usluga <> 1 and coalesce(t1.altposmode,0) = 1))
               and (((pr.posreal = 1 or pr.posreal = 5 or (pr.posreal = 2 and :frommwsconstloc is not null)))
                 or pr.docposid is null))
         and :nextmwsord = 0 and :nextpos = 1)
  do begin
    whareag = null;
    -- wybranie najlepszej pozycji dokumentu do realizacji
    -- dokumenty rozchodowe liczymy osobna procedura ze wzgledĂłw wydajnosciowych
    mwsconstlocp = null;
    good = null;
    vers = null;
    --wydanie z lokacji, czyli rowniez takefullpal
    if (:frommwsconstloc is not null and :frommwsconstloc > 0) then
    begin
      select first 1 pto.docid, dp.ref, dp.ktm, dp.wersjaref, pto.quantity,
          dp.flagi, dp.uwagi, coalesce(dp.dostawa,0), m.quantity - m.blocked,
          m.mwspalloc, m.mwsconstloc, m.wharea, t.magbreak,
          m.x_partia, m.x_serial_no, m.x_slownik    -- XXX KBI
        from mwsdocpostomwsord pto
          join dokumpoz dp on (pto.docposid = dp.ref and pto.mwsord = :refmwsord)
          left join dokumnag d on (d.ref = dp.dokument)
          left join mwsstock m on (dp.wersjaref = m.vers and m.wh = :wh and m.quantity - m.blocked > 0)
          left join mwsconstlocs ml on (ml.ref = m.mwsconstloc and ml.ref = :frommwsconstloc)
          left join towary t on (t.ktm = dp.ktm)
          left join dokumpoz p1 on (p1.ref = dp.alttopoz and dp.fake = 1)
          left join towary t1 on (t1.ktm = p1.ktm)
        where /*pto.docid = :docid*/ pto.docgroup = :docgroup and pto.doctype = 'M' and pto.quantity > 0
          and coalesce(d.mwsmag,d.magazyn) = :wh
          and ((d.akcept = 1 and d.mwsdisposition = 0)
            or (d.akcept = 0 and d.mwsdisposition = 1))
          and (m.lot = coalesce(dp.dostawa,0) or coalesce(dp.dostawa,0) = 0)
          and (m.x_partia = coalesce(dp.x_partia,'1900-01-01') or dp.x_partia is null)    -- XXX KBI
          and (m.x_serial_no = coalesce(dp.x_serial_no,0) or coalesce(dp.x_serial_no,0) = 0)  -- XXX KBI
          and (m.x_slownik = coalesce(dp.x_slownik,0) or coalesce(dp.x_slownik,0) = 0)        -- XXX KBI
          and m.ref is not null and ml.ref is not null and m.wh = :wh
          and m.mwsconstloc = :frommwsconstloc
          and ((coalesce(dp.havefake,0) = 0 and coalesce(dp.fake,0) = 0) or
               (coalesce(dp.fake,0) = 1 and t1.usluga <> 1 and coalesce(t1.altposmode,0) = 1))
          and (m.x_blocked<>1) --XXX Ldz ZG 126909
        into :docid, :docposid, :good, :vers, :quantity,
          :posflags, :posdescription, :lot, :quantityonlocation,
          :mwspallocp, :mwsconstlocp, :whareag, :magbreak,
          :x_partia, :x_serial_no, :x_slownik; -- XXX KBI
      if (:vers is null) then
        nextpos = 0;
    end
    else if (:frommwsconstloc is null or :frommwsconstloc = 0) then
    begin
      --JO: Nie ustawione - do sprawdzenia
      if (:indyviduallot is null) then indyviduallot = 0;
      select first 1 pto.docid, dp.ref, dp.ktm, dp.wersjaref, pr.posreal, pto.quantity,
          dp.flagi, dp.uwagi, coalesce(dp.dostawa,0), m.quantity - m.blocked, ml.whsec,
          m.mwspalloc, m.mwsconstloc, m.wharea, dw.indywidualdost, t.magbreak,
          m.x_partia, m.x_serial_no, m.x_slownik    -- XXX KBI

        from mwsdocpostomwsord pto
          join dokumpoz dp on (pto.docposid = dp.ref and pto.mwsord = :refmwsord)
          left join dokumnag d on (d.ref = dp.dokument)
          left join docpos2real pr on (pr.docposid = pto.docposid)
          left join mwsstock m on (dp.wersjaref = m.vers and m.wh = :wh and m.quantity - m.blocked > 0)
          left join mwsconstlocs ml on (ml.ref = m.mwsconstloc)
          left join whsecs ws on (ml.whsec = ws.ref)
          left join dostawy dw on (dp.dostawa = dw.ref)
          left join towary t on (t.ktm = dp.ktm)
          left join dokumpoz p1 on (p1.ref = dp.alttopoz and dp.fake = 1)
          left join towary t1 on (t1.ktm = p1.ktm)
        where
          /*pto.docid = :docid */ pto.docgroup = :docgroup  and pto.doctype = 'M' and pto.quantity > 0
          and ((coalesce(dp.havefake,0) = 0 and coalesce(dp.fake,0) = 0) or
               (coalesce(dp.fake,0) = 1 and t1.usluga <> 1 and coalesce(t1.altposmode,0) = 1))
          and coalesce(d.mwsmag,d.magazyn) = :wh
          and m.ref is not null and ml.ref is not null and ws.ref is not null
          and ((:indyviduallot = 1 and coalesce(dp.dostawa,0) = m.lot and ml.indyviduallot = 1)
            or (:indyviduallot = 0 and coalesce(dp.dostawa,0) = m.lot and ml.indyviduallot = 0 and t.magbreak = 2)
            or (:indyviduallot = 0 and coalesce(dp.dostawa,0) = m.lot and ml.indyviduallot = 0 and :magbreakmag = 2)
            or (coalesce(dp.dostawa,0) = 0 and ml.indyviduallot = 0)
            or (t.magbreak < 2 and dp.dostawa > 0 and coalesce(:magbreakmag,0) < 2) )
          and (m.x_partia = coalesce(dp.x_partia,'1900-01-01') or dp.x_partia is null)    -- XXX KBI
          and (m.x_serial_no = coalesce(dp.x_serial_no,0) or coalesce(dp.x_serial_no,0) = 0)  -- XXX KBI
          and (coalesce(m.x_slownik,0) = coalesce(dp.x_slownik,0)/* or coalesce(dp.x_slownik,0) = 0*/)        -- XXX KBI
          and m.wh = :wh and ws.deliveryarea = 2
          and ml.act > 0
          and ((ml.goodsav in (1,2) and :buffor = 0)
            or (:buffor = 1 and ml.goodsav in (0,1,2))
            or (:takefromzero = 1 and ml.goodsav = 4))
          and ((d.akcept = 1 and d.mwsdisposition = 0)
            or (d.akcept = 0 and d.mwsdisposition = 1))
          and (m.x_blocked<>1) --XXX Ldz ZG 126909
        order by coalesce(m.x_partia, '1900-01-01'), -- XXX KBI
          case when ml.whsec in (6,7) then 0 else 1 end,
          ml.goodsav, m.quantity - m.blocked, ml.mwsstandlevelnumber  --[PM] XXX , m.quantity - m.blocked, asysta 16.03.2016 najpierw "czyszczenie" lokacji
        into :docid, :docposid, :good, :vers, :posreal, :quantity,
            :posflags, :posdescription, :lot, :quantityonlocation, :locsec,
            :mwspallocp, :mwsconstlocp, :whareag, :indyviduallot, :magbreak,
            :x_partia, :x_serial_no, :x_slownik; -- XXX KBI
    end
    magbreak = maxvalue(:magbreak, :magbreakmag);
    todisp = :quantity;

    nextpos = 1;
    if (:mwsconstlocp is null) then
    begin
      delete from docpos2real where docgroup = :docgroup;
      delete from mwsdocpostomwsord where docgroup = :docgroup;
      quantityonlocation = 0;
      update dokumnag set mwsdocreal = 4, genmwsordsafter = 0 where ref = :docid;
      nextpos = 0;
    end
    -- zakladamy pozycje z lokacji zwyklych
    if (:quantityonlocation >= :todisp) then
      toinsert = :todisp;
    else
      toinsert = :quantityonlocation;
    -- zbadanie czy nie przekroczysz dopuszczalnej objetosci palety
    if (:nextpos = 1) then
    begin
      if (:toinsert > 0) then
      begin
        todisp = :todisp - :toinsert;
        maxnumber = :maxnumber + 1;
        insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
            mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
            regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc, plus,
            whareal, whareap, wharealogl, number, disttogo, palgroup, autogen, docgroup, x_partia, x_serial_no, x_slownik)
          values (:refmwsord, 1, :stocktaking, :good, :vers, :toinsert, :mwsconstlocp, :mwspallocp,
              :mwsconstlocl, null, :docid, :docposid, 'M', :opersettlmode, :autoloccreate, :wh, :whareag, :locsec,
              current_timestamp, null, null, null, :posflags, substring(:posdescription from 1 for 250), :priority, case when coalesce(:magbreak,0) = 2 then :lot else 0 end, :rec, 1,
              null, null, null, :maxnumber, 0, null, 1, :docgroup,
              :x_partia, :x_serial_no, :x_slownik);
        if (:takefullpal = 1) then
        begin
          mwspalloclpal = null;
          select first 1 m.mwspalloc
            from mwsstock m
              join towary t on (t.ktm = m.good)
            where m.mwsconstloc = :frommwsconstloc and m.quantity - m.blocked > 0
              and (t.paleta is null or t.paleta <> 1)
              and (m.x_blocked<>1) --XXX Ldz ZG 126909
            into :mwspalloclpal;
          if (:mwspalloclpal is null) then
            nextmwsord = 1;
        end
        update mwsdocpostomwsord set quantity = quantity - :toinsert
          where mwsord = :refmwsord and docposid = :docposid;
      end
      if (:mwsgenparts = 1 and :posreal = 5 and :frommwsconstloc is null) then
      begin
        if (not exists (
          select first 1 m.ref
            from mwsdocpostomwsord pto
              left join dokumpoz dp on (pto.docposid = dp.ref and pto.mwsord = :refmwsord)
              left join mwsstock m on (dp.wersjaref = m.vers and m.wh = :wh and m.quantity - m.blocked > 0)
              left join mwsconstlocs ml on (ml.ref = m.mwsconstloc
                  and ((ml.goodsav in (1,2) and :buffor = 0) or (:buffor = 1 and ml.goodsav in (0,1,2)) or (:takefromzero = 1 and ml.goodsav = 4)))
              left join whsecs ws on (ml.whsec = ws.ref and ws.deliveryarea = 2)
            where ml.wh = :wh and pto.quantity > 0 and m.ref is not null
              and (m.lot = coalesce(dp.dostawa,0) or coalesce(dp.dostawa,0) = 0)
              and (m.x_partia = coalesce(dp.x_partia,'1900-01-01') or dp.x_partia is null)   -- XXX KBI
              and (m.x_serial_no = coalesce(dp.x_serial_no,0) or coalesce(dp.x_serial_no,0) = 0) -- XXX KBI
              and (m.x_slownik = coalesce(dp.x_slownik,0) or coalesce(dp.x_slownik,0) = 0) -- XXX KBI
              and (m.x_blocked<>1) --XXX Ldz ZG 126909
              and ml.ref is not null and ws.ref is not null
              and pto.docposid = :docposid)
        ) then
        begin
          delete from mwsdocpostomwsord where docposid = :docposid;
          delete from docpos2real where docposid = :docposid;
        end
      end
    end
    delete from mwsdocpostomwsord where quantity <= 0 and mwsord = :refmwsord;
    delete from mwsconstlocsforbidden where docgroup = :docgroup;
    -- dla zlecen na odbior wlasny gdy jest wiecej jak 20 pozycji to generujemy kolejne zlecenie
    -- bedzie szybciej sie je realizowalo
    if (:maxnumber > :divwhenmore and :dontdiv = 0 and :frommwsconstloc is null) then
    begin
      select count(mwsord)
        from mwsdocpostomwsord
        where quantity > 0 and mwsord = :refmwsord
        into :posleft;
      if (:posleft is null) then posleft = 0;
      if (:posleft + :maxnumber > :divwhenmore) then
        nextmwsord = 1;
      else
        dontdiv = 1;
    end
  end
  update dokumnag set genmwsordsafter = 0, mwsdocreal = 4 where ref = :docid;

  -- XXX KBI 124059 sprawdzamy czy sa pozycje z lokacjami z kardexu jeli tak to
  -- przenosimy je do zlecenia zbiorczego
    execute procedure X_MWS_GEN_MWSORD_MULTI(:refmwsord,:wh);

  --ustawienie odpowiedniego statusu na zleceniu 7 lub 1
  --if (:multi > 0 or :mwsorddivide > 0) then mwsordstatus = 7;
  ---else

  select substring(list(distinct(dn.symbol))from 1 for 59)
    from mwsacts a
      join dokumpoz dp on (a.docposid = dp.ref)
      join dokumnag dn on (dp.dokument = dn.ref)
    where a.mwsord = :refmwsord
  into :symbolords;

  if (symbolords is not null) then
    begin
      update mwsords o set o.docsymbs = :symbolords where o.ref = :refmwsord;
    end 

  mwsordstatus = 1;

  update mwsords o set o.status = 0, o.multicnt = 0 where o.ref = :refmwsord;
  update mwsords o set o.status = :mwsordstatus where o.ref = :refmwsord;

--  if (:mwsordstatus = 1) then
    execute procedure xk_mws_mwsord_route_opt(:refmwsord);

  delete from mwsdocpostomwsord where mwsord = :refmwsord;
  update dokumnag set genmwsordsafter = 0 where ref = :docid;
  if (not exists (select a.ref
        from mwsacts a
        where a.mwsord = :refmwsord)) then
  begin
    update mwsacts set status = 0 where mwsord = :refmwsord;
    update mwsords set status = 0 where ref = :refmwsord;
    update dokumnag set genmwsordsafter = 0, mwsdocreal = 4 where ref = :docid;
    delete from mwsords where ref = :refmwsord;
  end else begin
    update mwsacts set status = 1 where mwsord = :refmwsord and (status = 0 or status = 7);
  end

  suspend;
end^
SET TERM ; ^
