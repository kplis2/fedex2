--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_BROWSE_GOODS(
      MWSORDREF integer,
      DOSTAWCAREF integer,
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      ACTONLY smallint,
      SEARCHCNT smallint)
  returns (
      REF integer,
      LEFTTOREALIZE varchar(20) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      WNAZWA varchar(255) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4),
      KOD_SES STRING40)
   as
declare variable TRYB smallint;
declare variable ACTCNT integer;
declare variable DOCID integer;
begin


  if (:actonly is null) then actonly = 0;
  if (:mwsordref is null) then mwsordref = 0;
  if (:dostawcaref is null) then dostawcaref = 0;
  if (:good is null) then good = '';
  if (:searchcnt is null) then searchcnt = 0;


  --exception universal MWSORDREF||' '||DOSTAWCAREF||' '||GOOD||' '||ACTONLY||' '||SEARCHCNT;

  select o.docid
    from mwsords o
    where o.ref = :mwsordref
    into docid;
  if (docid is null) then docid = 0;

  if (:mwsordref = 0 and :good = '') then exit;
  else if (:mwsordref = 0 and :good <> '') then tryb = 0;
  else if (:mwsordref > 0) then
  begin
    if (:docid > 0) then
    begin
      if (:good = '' and :searchcnt = 0) then tryb = 1;
      else if (:good <> '' and :searchcnt = 1) then tryb = 0;
      else exit;
    end
    else
    begin
      if (:good = '' and :searchcnt = 0) then tryb = 2;
      else if (:good <> '' and :searchcnt = 0) then tryb = 3;
      else if (:good <> '' and :searchcnt = 1) then tryb = 0;
    end
  end

  --szukamy w calej kartotece
  if (:tryb = 0) then
  begin
    for
      select w.REF, w.KTM,
          case when coalesce(t.MWSNAZWA,'') <> '' then t.MWSNAZWA else t.nazwa end||' '||coalesce(t.x_nazwa2,'') as nazwa,
          w.nazwa as wnazwa, t.x_kod_towaru_ses
        from WERSJE w
          join towary t on (t.ktm = w.ktm and t.usluga <> 1)
        where (upper(w.KTM) starting with upper(:good) or
            upper(coalesce(t.MWSNAZWA,'')) starting with upper(:good) or
            upper(coalesce(t.MWSNAZWA,'')) like upper('%'||:good||'%') or
            upper(t.NAZWA) starting with upper(:good) or
            upper(t.NAZWA) like upper('%'||:good||'%'))
          and (:actonly = 0 or (:actonly = 1 and w.akt = 1))
      into :ref, :ktm, :nazwa, :wnazwa, :kod_ses
    do begin
      if (kod_ses is null) then kod_ses = '-';
      if (quantity is null)then quantity = 0;
      if (LEFTTOREALIZE is null) then LEFTTOREALIZE = '-';
      suspend;
    end
  end
  --szukamy w pozycjach zlecenia
  else if (:tryb = 1) then
  begin
    for
      select t.ref, t.ktm, t.nazwa, t.wnazwa, t.quantity, t.lefttorealize
        from xk_mws_good_on_mwsord(:mwsordref,1) t
      into :ref, :ktm, :nazwa, :wnazwa, :quantity, :lefttorealize
    do begin
      if (quantity is null)then quantity  = 0;
      if (LEFTTOREALIZE is null) then LEFTTOREALIZE = '-';
      suspend;
    end
  end
  --szukamy w pozzamach wszystkich zamowien jesli NIE wpisana nazwa towaru
  else if (:tryb = 2) then
  begin
    for
      select w.ref, w.ktm, coalesce(t.MWSNAZWA,t.nazwa)||' '||coalesce(t.x_nazwa2,'') as nazwa, w.nazwa as wnazwa,
          sum(p.ilosc - p.ilzreal), t.x_kod_towaru_ses
        from nagzam n
          join pozzam p on (n.ref = p.zamowienie)
          join towary t on (t.ktm = p.ktm and t.usluga <> 1)
          join wersje w on (t.ktm = w.ktm)
        where n.dostawca = :dostawcaref
          and n.typzam = 'DST'
          and n.rejestr in ('DOS','DWY')
        group by w.ref, w.ktm, coalesce(t.MWSNAZWA,t.nazwa)||' '||coalesce(t.x_nazwa2,''), w.nazwa, t.x_kod_towaru_ses
      into :ref, :ktm, :nazwa, :wnazwa, :lefttorealize, :kod_ses
    do begin
      select sum(a.quantityc) from mwsacts a
        where a.mwsord = :mwsordref
          and a.vers = :ref
          and a.status = 5
      into :quantity;
      if (kod_ses is null) then kod_ses = '-';
      if (quantity is null)then quantity  = 0;
      if (LEFTTOREALIZE is null) then LEFTTOREALIZE = '-';
      suspend;
    end
  end
  --szukamy w pozzamach wszystkich zamowien jesli wpisana nazwa towaru
  else if (:tryb = 3) then
  begin
    for
      select w.ref, w.ktm, coalesce(t.MWSNAZWA,t.nazwa)||' '||coalesce(t.x_nazwa2,'') as nazwa, w.nazwa as wnazwa,
          sum(p.ilosc - p.ilzreal), t.x_kod_towaru_ses
        from nagzam n
          join pozzam p on (n.ref = p.zamowienie)
          join towary t on (t.ktm = p.ktm and t.usluga <> 1)
          join wersje w on (t.ktm = w.ktm)
        where n.dostawca = :dostawcaref
          and n.typzam = 'DST'
          and n.rejestr in ('DOS','DWY')
          and (upper(w.KTM) starting with upper(:good) or
            upper(coalesce(t.MWSNAZWA,'')) starting with upper(:good) or
            upper(coalesce(t.MWSNAZWA,'')) like upper('%'||:good||'%') or
            upper(t.NAZWA) starting with upper(:good) or
            upper(t.NAZWA) like upper('%'||:good||'%'))
        group by w.ref, w.ktm, coalesce(t.MWSNAZWA,t.nazwa)||' '||coalesce(t.x_nazwa2,''), w.nazwa, t.x_kod_towaru_ses
      into :ref, :ktm, :nazwa, :wnazwa, :lefttorealize, :kod_ses
    do begin
      select sum(a.quantityc) from mwsacts a
        where a.mwsord = :mwsordref
          and a.vers = :ref
          and a.status = 5
      into :quantity;
      if (kod_ses is null) then kod_ses = '-';
      if (quantity is null)then quantity  = 0;
      if (LEFTTOREALIZE is null) then LEFTTOREALIZE = '-';
      suspend;
    end
  end
end^
SET TERM ; ^
