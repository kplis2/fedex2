--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_HH_POZZAM_BROWSE(
      OPERATOR integer,
      NAGZAMREF integer)
  returns (
      REF integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4),
      STATUS varchar(3) CHARACTER SET UTF8                           ,
      PLACE varchar(20) CHARACTER SET UTF8                           ,
      DESCRIPT varchar(1024) CHARACTER SET UTF8                           ,
      KTMDICT varchar(255) CHARACTER SET UTF8                           ,
      VERSDICT varchar(50) CHARACTER SET UTF8                           )
   as
declare variable statusint smallint;
begin
  for
    select p.ref, p.ktm, p.ilosc, coalesce(p.status,0), coalesce(t.miejscewmag,''),
           coalesce(p.opis,''), t.nazwa, w.nazwa
      from pozzam p
        join towary t on(p.ktm = t.ktm)
        join wersje w on(p.wersjaref = w.ref)
      where p.zamowienie = :nagzamref
      into :ref, :ktm, :quantity, :statusint, :place, :descript, :ktmdict, :versdict
  do begin
    if (statusint = 0) then
      status = '';
    else if (statusint = 1) then
      status = ' v ';
    else if (statusint = 2) then
      status = ' x ';
    suspend;
  end
end^
SET TERM ; ^
