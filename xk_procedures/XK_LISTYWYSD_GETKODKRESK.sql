--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSD_GETKODKRESK(
      KODKRESK varchar(40) CHARACTER SET UTF8                           ,
      LISTWYSD integer,
      OPK integer,
      TRYB smallint)
  returns (
      DALEJ smallint,
      RTRYB smallint,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
begin
  dalej = 1;
  rtryb = :tryb;
  msg = '';
  kodkresk = upper(:kodkresk);

  --sprawdzenie czy jest kod kreskowy
  if (:kodkresk = '') then begin
    dalej = 0;
    rtryb = 0;
    msg = 'Nie podano kodu kreskowego';
    suspend;
    exit;
  end
  --wlaczenie trybu pakowania
  if (:kodkresk = 'PAKUJ' or (:kodkresk = 'P' and coalesce(char_length(:kodkresk),0) = 1)) then begin -- [DG] XXX ZG119346
    dalej = 0;
    rtryb = 0;
    msg = 'Podaj kod towaru';
    suspend;
    exit;
  end
  --wlaczenie trybu otwierania kartonu
  else if (:kodkresk = 'OTWORZ' or (:kodkresk = 'O' and coalesce(char_length(:kodkresk),0) = 1)) then begin -- [DG] XXX ZG119346
    dalej = 0;
    rtryb = 2;
    msg = 'Podaj kod kartonu';
    suspend;
    exit;
  end
  --wlaczenie trybu zamykania kartonu
  else if (:kodkresk = 'ZAMKNIJ' or (:kodkresk = 'Z' and coalesce(char_length(:kodkresk),0) = 1)) then begin -- [DG] XXX ZG119346
    dalej = 0;
    if (coalesce(:opk, 0) = 0) then begin
      rtryb = 0;
      msg = 'Brak aktywnego opakowania. Wskaż opakowanie';
      suspend;
      exit;
    end
    rtryb = 3;
    msg = 'Podaj kod kartonu';
    suspend;
    exit;
  end
  --wlaczenie trybu pakowania kartonu w karton lub na palete
  if (:kodkresk = 'OPKWOPK' or (:kodkresk = 'OWO' and coalesce(char_length(:kodkresk),0) = 3)) then begin -- [DG] XXX ZG119346
    dalej = 0;
    if (coalesce(:opk, 0) = 0) then begin
      rtryb = 0;
      msg = 'Brak aktywnego opakowania. Wskaż opakowanie';
      suspend;
      exit;
    end
    rtryb = 4;
    msg = 'Wskaż opakowanie';
    suspend;
    exit;
  end
  --wlaczenie trybu usuwania kartonu z kartonu lub towaru z kartonu
  else if (:kodkresk = 'USUN' or (:kodkresk = 'U' and coalesce(char_length(:kodkresk),0) = 1)) then begin -- [DG] XXX ZG119346
    dalej = 0;
    if (coalesce(:opk, 0) = 0) then begin
      rtryb = 0;
      msg = 'Brak aktywnego opakowania. Wskaż opakowanie';
      suspend;
      exit;
    end
    if (exists(select stan from listywysdroz_opk where listwysd = :listwysd and ref = :opk and stan = 1)) then begin
      rtryb = 0;
      msg = 'Nie można uswać towaru z zamkniętego opakowania';
      suspend;
      exit;
    end
    else begin
      rtryb = 5;
      if (:tryb = 5) then
      begin
        dalej = 1;
        rtryb = :tryb;
      end
      msg = 'Podaj kod towaru lub opakowania do usunięcia';
      suspend;
      exit;
    end
  end
  else if (substring(:kodkresk from  1 for  3) = 'OPK' and substring(:kodkresk from  4 for 4) = '-' and
      substring(:kodkresk from  coalesce(char_length(:kodkresk),0) for coalesce(char_length(:kodkresk),0)) = '#') then -- [DG] XXX ZG119346
  begin
    dalej = 0;
    rtryb = 1;
--    if (exists(select first 1 1 from listywysdroz_opk where kodkresk = :kodkresk and listwysd = :listwysd)) then rtryb = 0;
--    else rtryb = 1;
    if (:tryb > 2) then
    begin
      dalej = 1;
      rtryb = :tryb;
    end
    msg = 'Podaj kod towaru';
    suspend;
    exit;
  end
end^
SET TERM ; ^
