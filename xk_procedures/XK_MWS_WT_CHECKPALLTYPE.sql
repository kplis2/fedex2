--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_WT_CHECKPALLTYPE(
      MWSORD integer)
  returns (
      DOCCNT integer,
      TAKEFULLPALL smallint,
      DOCIDCNT integer)
   as
declare variable good varchar(40);
declare variable palsposdost smallint;
declare variable palquantity integer;
begin
  SELECT mo.takefullpal
    FROM MWSORDS mo
    where mo.ref = :mwsord
    into :takefullpall;

  if(takefullpall is null)
    then takefullpall = 0;

  SELECT first 1 ma.good
    from mwsacts ma
      join towary t on (ma.good = t.ktm)
     where ma.mwsord = :mwsord
     and t.paleta = 1
     into :good;

  select count(distinct(ma.docid))
    from mwsacts ma
    where ma.mwsord = :mwsord
    into :docidcnt;

  select count(mp.good)
    from mwspallparts mp
    where mp.good = :good
    into :doccnt;
SUSPEND;
end^
SET TERM ; ^
