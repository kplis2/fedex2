--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_HH_GOODS_BROWSE(
      KTMMASK varchar(20) CHARACTER SET UTF8                           )
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      KODKRESK varchar(40) CHARACTER SET UTF8                           )
   as
begin
  for
    select t.ktm, t.nazwa, t.kodkresk
      from towary t
      where t.akt = 1
        and upper(t.ktm) like upper(:ktmmask)
      into :ktm, :nazwa, :kodkresk
  do begin
    suspend;
  end
end^
SET TERM ; ^
