--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_SET_DESCRIPT_ONACT(
      REF INTEGER_ID,
      DESCRIPT STRING1024,
      MODE SMALLINT_ID)
  returns (
      REF_SLOWNIK INTEGER_ID)
   as
  declare variable ref_tmp integer_id;
begin
  descript = substring(:descript from 1 for 254); --descript ma 255 znakow w mwsacts i dokumpoz
  if (mode = 1) then
  begin
-- update desriptu na pozycji zlecenia
    update mwsacts ma set ma.descript = :descript where ma.ref = :ref;
-- pobranie refa pozycji dokumentu
    select m.docposid
      from mwsacts m
      where m.ref = :ref
    into :ref_tmp;
-- update pozycji dokumentu
    update dokumpoz d
      set d.uwagi = :descript
      where d.ref = :ref_tmp;
-- update naglowka dokumentu do wyslania
    select first 1 d.dokument
      from dokumpoz d
      where d.ref = :ref_tmp
    into :ref_tmp;

    update dokumnag d
      set d.x_do_wyslania = 1
      where d.ref = :ref_tmp and d.dostawca <> 3847;  --i jezeli dostawca nie jest HERMONem (przyjcie z reklamacji) ZG138460;

    suspend;
  end
  else if (mode = 0) then begin
-- update descriptu na naglowku zlecenia
    update mwsords mo set mo.description = :descript where mo.ref = :ref;
-- pobranie refa naglowka dokumentu
    select m.docid
      from mwsords m
      where m.ref = :ref
    into :ref_tmp;
-- update descriptu naglowka dokumentu
    update dokumnag d
      set d.uwagi = :descript, d.x_do_wyslania = 1
      where d.ref = :ref_tmp;
  end
end^
SET TERM ; ^
