--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GEN_MWSORD_OUT_OLD(
      DOCID DOKUMNAG_ID,
      DOCGROUP DOKUMNAG_ID,
      DOCPOSID DOKUMPOZ_ID,
      DOCTYPE CHAR_1,
      MWSORD MWSORDS_ID,
      MWSACT MWSACTS_ID,
      RECALC SMALLINT_ID,
      MWSORDTYPE MWSORDTYPES_ID)
  returns (
      REFMWSORD MWSORDS_ID)
   as
declare variable REC smallint_id;
declare variable WH defmagaz_id;
declare variable WH2 defmagaz_id;
declare variable WH2DESC STRING255;
declare variable STOCKTAKING smallint;
declare variable PRIORITY smallint;
declare variable BRANCH varchar(10);
declare variable PERIOD varchar(6);
declare variable FLAGS varchar(40);
declare variable SYMBOL varchar(20);
declare variable COR smallint;
declare variable QUANTITY numeric(14,4);
declare variable TODISP numeric(14,4);
declare variable TOINSERT numeric(14,4);
declare variable GOOD varchar(40);
declare variable VERS integer;
declare variable OPERSETTLMODE smallint;
declare variable MWSCONSTLOCP integer;
declare variable MWSPALLOCP integer;
declare variable AUTOLOCCREATE smallint;
declare variable WHAREAG integer;
declare variable POSFLAGS varchar(40);
declare variable DESCRIPTION varchar(1024);
declare variable POSDESCRIPTION varchar(1024);
declare variable LOT integer;
declare variable CONNECTTYPE smallint;
declare variable SHIPPINGAREA varchar(3);
declare variable QUANTITYONLOCATION numeric(15,4);
declare variable MAXNUMBER integer;
declare variable DOCOBJ numeric(14,4);
declare variable PALTYPE varchar(40);
declare variable SHIPPINGTYPE integer;
declare variable NEXTMWSORD smallint;
declare variable MAXPALVOL numeric(14,4);
declare variable TAKEFULLPAL smallint;
declare variable FROMMWSCONSTLOC integer;
declare variable PALGROUPMM integer;
declare variable TAKEFULLPALMM integer;
declare variable TAKEFROMMWSCONSTLOCMM integer;
declare variable TAKEFROMMWSPALLOCMM integer;
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable SLOKOD varchar(40);
declare variable DOCREAL smallint;
declare variable PALQUANTITY integer;
declare variable DIVWHENMORE integer;
declare variable DIVWHENMORES varchar(100);
declare variable DONTDIV smallint;
declare variable POSLEFT integer;
declare variable MWSPALLOCLPAL integer;
declare variable MWSGENPARTSS varchar(100);
declare variable MWSGENPARTS smallint;
declare variable POSREAL smallint;
declare variable NEXTPOS smallint;
declare variable DOCID1 integer;
declare variable DOCGROUP1 integer;
declare variable DOCPOSID1 integer;
declare variable VERS1 integer;
declare variable QUANTITY1 numeric(14,4);
declare variable TAKEFROMZEROS varchar(100);
declare variable TAKEFROMZERO smallint;
declare variable LOCSEC integer;
declare variable BUFFORS varchar(100);
declare variable BUFFOR smallint;
declare variable INDYVIDUALLOT smallint;
declare variable MAGBREAK smallint;
declare variable MULTI smallint_id;
declare variable MAXSINGLEDOCPOS smallint_id;
declare variable OUT smallint_id;
declare variable mwsordstatus smallint_id;
declare variable newmwsord smallint_id;
declare variable magbreakmag smallint_id;
declare variable multicnt smallint_id;
begin
  if (:docgroup is null) then docgroup = :docid;
  if (:doctype is null) then doctype = 'M';
  if (:recalc is null) then recalc = 0;
  docposid = null;

  if (:mwsordtype is null) then
    select first 1 t.ref, t.stocktaking, t.opersettlmode, t.autoloccreate
      from mwsordtypes t
      where t.mwsortypedets = 1
      order by t.ref
    into :mwsordtype, :stocktaking, :opersettlmode, :autoloccreate;
  else
    select stocktaking, opersettlmode, autoloccreate
      from mwsordtypes
      where ref = :mwsordtype
    into :stocktaking, :opersettlmode, :autoloccreate;

  -- czy wolno brac towar z lokacji niedostepnych dla pickerow "0"
  execute procedure get_config('MWSTAKEFROMBUFFOR',2) returning_values :buffors;
  if (:buffors is null or: buffors = '') then
    buffor = 0;
  else
    buffor = cast(:buffors as smallint);
  execute procedure get_config('MWSTAKEFROMZERO',2) returning_values :takefromzeros;
  if (:takefromzeros is null or :takefromzeros = '') then
    takefromzero = 0;
  else
    takefromzero = cast(:takefromzeros as smallint);
  -- dziel zlecenie gdy wiecej operacji niz ..
  execute procedure get_config('MWSDIVWHENMORE',2) returning_values :divwhenmores;
  if (:divwhenmores is not null and :divwhenmores <> '') then
    divwhenmore = cast(:divwhenmores as integer);
  else
    divwhenmore = 100;
  -- pozwalaj szykowac czesciowe pozycje na dokumentach
  execute procedure get_config('MWSGENPARTS',2) returning_values :mwsgenpartss;
  if (:mwsgenpartss is not null and :mwsgenpartss <> '') then
    mwsgenparts = cast(:mwsgenpartss as smallint);
  else
    mwsgenparts = 0;

  select d.wydania, d.koryg, d.zewn,
      coalesce(d.mwsmag,d.magazyn), d.mag2, d.uwagi,
      d.priorytet, d.oddzial, d.okres, d.flagi,
      d.symbol, d.kodsped, d.sposdost,
      d.takefullpal, d.palgroup, d.takefrommwsconstloc,
      d.takefrommwspalloc, 1, k.ref, substring(k.fskrot from 1 for 40),
      d.mwsdocreal, d.iloscpalet
    from dokumnag d
      left join klienci k on (k.ref = d.klient)
      left join sposdost s on (d.sposdost = s.ref)
    where d.ref = :docid
    into :rec, :cor, :out, :wh, :wh2, :description,
      :priority, :branch, :period, :flags,
      :symbol, :shippingarea, :shippingtype,
      :takefullpalmm, :palgroupmm, :takefrommwsconstlocmm,
      :takefrommwspallocmm, :slodef, :slopoz, :slokod,
      :docreal, :palquantity;

  -- cale zlecenie z towatami z gornych pieter mozna szykowac tylko jak nie
  if (takefullpalmm is null) then takefullpalmm = 0;
  if (docreal is null) then docreal = 0;
  --odwrocenie ze wzgledu na nazwe zmiennej
  if (:rec = 0) then rec = 1; else rec = 0;

  priority = 2; --JO: dlaczego nie wiem

  --TODO
  if (:rec = 0 and :out = 0) then
  begin
    select coalesce(opis,symbol) from defmagaz where symbol = :wh2
    into :wh2desc;
    description = 'Wydanie na magazyn: '||:wh2desc||', '||:description;
  end

  select coalesce(m.multipicking,0), m.maxsingledocpos
    from mwsordtypes4wh m
    where m.mwsordtype = :mwsordtype
      and m.wh = :wh
  into :multi, :maxsingledocpos;

  if (:multi = 1) then
  begin
    execute procedure xk_mws_check_multidoc(:wh, :mwsordtype, :docid, :docgroup)
      returning_values :multi;
  end

  -- okreslenie czy mozna dolaczyc pozycje zlecenia do juz istniejacego
  connecttype = 0;
  execute procedure xk_mws_find_mwsordref(:mwsordtype, :mwsord, null, :docgroup,
      :docid, :docposid, :wh, :branch, null, :multi)
    returning_values :refmwsord, :connecttype, :paltype, :docobj, :maxpalvol;

  -- jezeli nie mozna nigdzie dolaczyc nowych operacji do generuje nowe zlecenie
  newmwsord = 0;
  if (connecttype = 0 ) then
  begin
    newmwsord = 1;
    execute procedure gen_ref('MWSORDS') returning_values refmwsord;
    select okres from datatookres(current_date,0,0,0,0) into period;
    insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator, priority,
        description, wh,  regtime, timestartdecl, branch, period,
        flags, docsymbs, cor, rec, status, shippingarea, shippingtype, takefullpal,
        slodef, slopoz, slokod, palquantity, repal)
      values (:refmwsord, :mwsordtype, :stocktaking, :doctype, :docgroup, :docid, null, :priority,
          :description, :wh, current_timestamp, null, :branch, :period,
          :flags, :symbol, :cor, :rec, 0, :shippingarea, :shippingtype, 0,
          :slodef, :slopoz, :slokod, :palquantity, 0);
  end
  -- jezeli mozna dolaczyc do zlecenia dla grupy, to dodaje symgol do opisu
  else if (connecttype = 1 or connecttype = 3) then
    update mwsords set docsymbs = docsymbs||';'||:symbol where docgroup = :docgroup
      and docsymbs not like '%'||symbol||'%' and coalesce(char_length(docsymbs),0) < 51;

  --ustawienie czy zlecenie jest multi zawsze, niezaleznie czy udalo sie polaczyc czy nie
  update mwsords set multi = :multi where ref = :refmwsord;
  --nie dziel jesli zlecenie multi
  if (:connecttype = 4) then dontdiv = 1;
  else dontdiv = 0;

  -- zarejestrowanie co trzeba dodac do zlecenia z biezacego dokumentu magazynowego
  for
    select dp.ktm, p.docgroup, p.docid, p.docposid,
        case when (d.mwsdisposition = 1 or coalesce(t1.altposmode,0) = 1) then dp.ilosc else dp.iloscl end - coalesce(dp.ilosconmwsacts,0),
        dp.wersjaref, coalesce(dp.dostawa,0)
      from docpos2real p
        left join dokumnag d on (d.ref = p.docid)
        left join dokumpoz dp on (dp.ref = p.docposid)
        left join towary t on (t.ktm = dp.ktm)
        left join dokumpoz p1 on (p1.ref = dp.alttopoz and dp.fake = 1)
        left join towary t1 on (t1.ktm = p1.ktm)
      where p.docid = :docid and p.doctype = 'M'
        and ((coalesce(dp.havefake,0) = 0 and coalesce(dp.fake,0) = 0) or
             (coalesce(dp.fake,0) = 1 and t1.usluga <> 1 and coalesce(t1.altposmode,0) = 1))
        and (t.paleta <> 1 or t.paleta is null)
        and ((d.akcept = 1 and d.mwsdisposition = 0)
            or (d.akcept = 0 and d.mwsdisposition = 1))
        and coalesce(d.mwsmag,d.magazyn) = :wh
        and (p.posreal = 1
           or (:mwsgenparts = 1 and p.posreal = 5)
           or p.posreal = 2)
        and dp.genmwsordsafter = 1
        and t.usluga <> 1
      into :good, :docgroup1, :docid1, :docposid1, :quantity1, :vers1, :lot
  do begin
    if (not exists (select mwsord from mwsdocpostomwsord where mwsord = :refmwsord
        and docgroup = :docgroup1 and docposid = :docposid1 and doctype = 'M')
    ) then
      insert into mwsdocpostomwsord (mwsord, docgroup, docid, docposid, doctype, quantity, vers, lot)
        values (:refmwsord, :docgroup, :docid1, :docposid1, 'M', :quantity1, :vers1, :lot);
    else
      update mwsdocpostomwsord set quantity = :quantity1, lot = :lot
        where mwsord = :refmwsord and docid = :docid1 and docposid = :docposid1 and doctype = 'M';
  end

  -- znalezienie max. numeru operacji - kolejne zwiekszamy i jeden.
  select max(number) from mwsacts where mwsord = :refmwsord into :maxnumber;
  if (:maxnumber is null) then maxnumber = 0;

  --sprawdzenie czy mozna pobrac pelna palete do szykowania
  frommwsconstloc = null;
  select first 1 mwsconstloc from xk_mws_take_full_pal_check(:wh,:docgroup,:refmwsord,1)
    into :frommwsconstloc;
  if (frommwsconstloc is not null) then takefullpal = 1;
  if (takefullpalmm = 1) then takefullpal = 1;
  if (takefullpal = 0 and (docreal = 2 or docreal = 4)) then
  begin
    delete from mwsords where ref = :refmwsord;
    update dokumnag set genmwsordsafter = 0 where ref = :docid;
    exit;
  end

  -- generowanie zlecenia na towar
  --JO: DEL raczej nie uzywane
  /*$$IBEC$$ if (takefullpalmm = 1
      and (takefrommwsconstlocmm is null or takefrommwsconstlocmm = 0)
      and (takefrommwspallocmm is null or takefrommwspallocmm = 0)
  ) then
  begin
    delete from mwsdocpostomwsord where mwsord = :refmwsord;
    execute procedure xk_move_palgroup_mwsacts(:refmwsord, :docid, :palgroupmm);
    -- rozwiązanie problemu z zleceniami bez pozycji
    select count(ref) from mwsacts where mwsord = :refmwsord into maxnumber;
    if (maxnumber < 1) then
    begin
      update mwsacts set status = 0 where mwsord = :refmwsord;
      update mwsords set status = 0 where ref = :refmwsord;
      delete from mwsords where ref = :refmwsord;
    end
    update dokumnag set genmwsordsafter = 0 where ref = :docid;
    exit;
  end
  --mozliwosc generowania mm w przypadku gdy mamy podane jedynie palloc
  if (takefullpalmm = 1 and (takefrommwsconstlocmm is not null or takefrommwspallocmm is not null)
      and (takefrommwsconstlocmm > 0 or takefrommwspallocmm > 0)
  ) then
  begin
    delete from mwsdocpostomwsord where mwsord = :refmwsord;
    execute procedure xk_move_mwsconstloc(:refmwsord, :docid, :takefrommwsconstlocmm, :takefrommwspallocmm);
    update dokumnag set genmwsordsafter = 0 where ref = :docid;
    exit;
  end $$IBEC$$*/

  nextmwsord = 0;
  nextpos = 1;
  recalc = 0;
  while (exists(
           select p.ref
             from mwsdocpostomwsord m
               join dokumpoz p on (p.ref = m.docposid and m.mwsord = :refmwsord and m.doctype = 'M')
               left join docpos2real pr on (pr.docposid = p.ref)
               left join dokumnag d on (d.ref = p.dokument)
               join towary t on (p.ktm = t.ktm)
               left join dokumpoz p1 on (p1.ref = p.alttopoz and p.fake = 1)
               left join towary t1 on (t1.ktm = p1.ktm)
             where p.genmwsordsafter = 1
               and ((d.akcept = 1 and d.mwsdisposition = 0)
                 or (d.akcept = 0 and d.mwsdisposition = 1))
               and m.quantity > 0
               and ((coalesce(p.havefake,0) = 0 and coalesce(p.fake,0) = 0) or
                    (coalesce(p.fake,0) = 1 and t1.usluga <> 1 and coalesce(t1.altposmode,0) = 1))
               and (((pr.posreal = 1 or pr.posreal = 5 or (pr.posreal = 2 and :frommwsconstloc is not null)))
                 or pr.docposid is null))
         and :nextmwsord = 0 and :nextpos = 1)
  do begin
    whareag = null;
    -- wybranie najlepszej pozycji dokumentu do realizacji
    -- dokumenty rozchodowe liczymy osobna procedura ze wzgledów wydajnosciowych
    mwsconstlocp = null;
    good = null;
    vers = null;
    --wydanie z lokacji, czyli rowniez takefullpal
    if (:frommwsconstloc is not null and :frommwsconstloc > 0) then
    begin
      select first 1 pto.docid, dp.ref, dp.ktm, dp.wersjaref, pto.quantity,
          dp.flagi, dp.uwagi, coalesce(dp.dostawa,0), m.quantity - m.blocked,
          m.mwspalloc, m.mwsconstloc, m.wharea, t.magbreak
        from mwsdocpostomwsord pto
          join dokumpoz dp on (pto.docposid = dp.ref and pto.mwsord = :refmwsord)
          left join dokumnag d on (d.ref = dp.dokument)
          left join mwsstock m on (dp.wersjaref = m.vers and m.wh = :wh and m.quantity - m.blocked > 0)
          left join mwsconstlocs ml on (ml.ref = m.mwsconstloc and ml.ref = :frommwsconstloc)
          left join towary t on (t.ktm = dp.ktm)
          left join dokumpoz p1 on (p1.ref = dp.alttopoz and dp.fake = 1)
          left join towary t1 on (t1.ktm = p1.ktm)
        where pto.docid = :docid and pto.doctype = 'M' and pto.quantity > 0
          and coalesce(d.mwsmag,d.magazyn) = :wh
          and ((d.akcept = 1 and d.mwsdisposition = 0)
            or (d.akcept = 0 and d.mwsdisposition = 1))
          and (m.lot = coalesce(dp.dostawa,0) or coalesce(dp.dostawa,0) = 0)
          and m.ref is not null and ml.ref is not null and m.wh = :wh
          and m.mwsconstloc = :frommwsconstloc
          and ((coalesce(dp.havefake,0) = 0 and coalesce(dp.fake,0) = 0) or
               (coalesce(dp.fake,0) = 1 and t1.usluga <> 1 and coalesce(t1.altposmode,0) = 1))
        into :docid, :docposid, :good, :vers, :quantity,
          :posflags, :posdescription, :lot, :quantityonlocation,
          :mwspallocp, :mwsconstlocp, :whareag, :magbreak;
      if (:vers is null) then
        nextpos = 0;
    end
    else if (:frommwsconstloc is null or :frommwsconstloc = 0) then
    begin
      --JO: Nie ustawione - do sprawdzenia
      if (:indyviduallot is null) then indyviduallot = 0;
      select first 1 pto.docid, dp.ref, dp.ktm, dp.wersjaref, pr.posreal, pto.quantity,
          dp.flagi, dp.uwagi, coalesce(dp.dostawa,0), m.quantity - m.blocked, ml.whsec,
          m.mwspalloc, m.mwsconstloc, m.wharea, dw.indywidualdost, t.magbreak
        from mwsdocpostomwsord pto
          join dokumpoz dp on (pto.docposid = dp.ref and pto.mwsord = :refmwsord)
          left join dokumnag d on (d.ref = dp.dokument)
          left join docpos2real pr on (pr.docposid = pto.docposid)
          left join mwsstock m on (dp.wersjaref = m.vers and m.wh = :wh and m.quantity - m.blocked > 0)
          left join mwsconstlocs ml on (ml.ref = m.mwsconstloc)
          left join whsecs ws on (ml.whsec = ws.ref)
          left join dostawy dw on (dp.dostawa = dw.ref)
          left join towary t on (t.ktm = dp.ktm)
          left join dokumpoz p1 on (p1.ref = dp.alttopoz and dp.fake = 1)
          left join towary t1 on (t1.ktm = p1.ktm)
        where
          pto.docid = :docid and pto.doctype = 'M' and pto.quantity > 0
          and ((coalesce(dp.havefake,0) = 0 and coalesce(dp.fake,0) = 0) or
               (coalesce(dp.fake,0) = 1 and t1.usluga <> 1 and coalesce(t1.altposmode,0) = 1))
          and coalesce(d.mwsmag,d.magazyn) = :wh
          and m.ref is not null and ml.ref is not null and ws.ref is not null
          and ((:indyviduallot = 1 and coalesce(dp.dostawa,0) = m.lot and ml.indyviduallot = 1)
            or (:indyviduallot = 0 and coalesce(dp.dostawa,0) = m.lot and ml.indyviduallot = 0 and t.magbreak = 2)
            or (:indyviduallot = 0 and coalesce(dp.dostawa,0) = m.lot and ml.indyviduallot = 0 and :magbreakmag = 2)
            or (coalesce(dp.dostawa,0) = 0 and ml.indyviduallot = 0)
            or (t.magbreak < 2 and dp.dostawa > 0 and coalesce(:magbreakmag,0) < 2) )
          and m.wh = :wh and ws.deliveryarea = 2
          and ml.act > 0
          and ((ml.goodsav in (1,2) and :buffor = 0)
            or (:buffor = 1 and ml.goodsav in (0,1,2))
            or (:takefromzero = 1 and ml.goodsav = 4))
          and ((d.akcept = 1 and d.mwsdisposition = 0)
            or (d.akcept = 0 and d.mwsdisposition = 1))
        order by case when ml.whsec in (6,7) then 0 else 1 end,
          ml.goodsav, m.quantity - m.blocked, ml.mwsstandlevelnumber  --[PM] XXX , m.quantity - m.blocked, asysta 16.03.2016 najpierw "czyszczenie" lokacji
        into :docid, :docposid, :good, :vers, :posreal, :quantity,
            :posflags, :posdescription, :lot, :quantityonlocation, :locsec,
            :mwspallocp, :mwsconstlocp, :whareag, :indyviduallot, :magbreak;
    end
    magbreak = maxvalue(:magbreak, :magbreakmag);
    todisp = :quantity;

    nextpos = 1;
    if (:mwsconstlocp is null) then
    begin
      delete from docpos2real where docgroup = :docgroup;
      delete from mwsdocpostomwsord where docgroup = :docgroup;
      quantityonlocation = 0;
      update dokumnag set mwsdocreal = 4, genmwsordsafter = 0 where ref = :docid;
      nextpos = 0;
    end
    -- zakladamy pozycje z lokacji zwyklych
    if (:quantityonlocation >= :todisp) then
      toinsert = :todisp;
    else
      toinsert = :quantityonlocation;
    -- zbadanie czy nie przekroczysz dopuszczalnej objetosci palety
    if (:nextpos = 1) then
    begin
      if (:toinsert > 0) then
      begin
        todisp = :todisp - :toinsert;
        maxnumber = :maxnumber + 1;
        insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
            mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
            regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc, plus,
            whareal, whareap, wharealogl, number, disttogo, palgroup, autogen, docgroup)
          values (:refmwsord, 1, :stocktaking, :good, :vers, :toinsert, :mwsconstlocp, :mwspallocp,
              null,null, :docid, :docposid, 'M', :opersettlmode, :autoloccreate, :wh, :whareag, :locsec,
              current_timestamp, null, null, null, :posflags, substring(:posdescription from 1 for 250), :priority, case when coalesce(:magbreak,0) = 2 then :lot else 0 end, :rec, 1,
              null, null, null, :maxnumber, 0, null, 1, :docgroup);
        if (:takefullpal = 1) then
        begin
          mwspalloclpal = null;
          select first 1 m.mwspalloc
            from mwsstock m
              join towary t on (t.ktm = m.good)
            where m.mwsconstloc = :frommwsconstloc and m.quantity - m.blocked > 0
              and (t.paleta is null or t.paleta <> 1)
            into :mwspalloclpal;
          if (:mwspalloclpal is null) then
            nextmwsord = 1;
        end
        update mwsdocpostomwsord set quantity = quantity - :toinsert
          where mwsord = :refmwsord and docposid = :docposid;
      end
      if (:mwsgenparts = 1 and :posreal = 5 and :frommwsconstloc is null) then
      begin
        if (not exists (
          select first 1 m.ref
            from mwsdocpostomwsord pto
              left join dokumpoz dp on (pto.docposid = dp.ref and pto.mwsord = :refmwsord)
              left join mwsstock m on (dp.wersjaref = m.vers and m.wh = :wh and m.quantity - m.blocked > 0)
              left join mwsconstlocs ml on (ml.ref = m.mwsconstloc
                  and ((ml.goodsav in (1,2) and :buffor = 0) or (:buffor = 1 and ml.goodsav in (0,1,2)) or (:takefromzero = 1 and ml.goodsav = 4)))
              left join whsecs ws on (ml.whsec = ws.ref and ws.deliveryarea = 2)
            where ml.wh = :wh and pto.quantity > 0 and m.ref is not null
              and (m.lot = coalesce(dp.dostawa,0) or coalesce(dp.dostawa,0) = 0)
              and ml.ref is not null and ws.ref is not null
              and pto.docposid = :docposid)
        ) then
        begin
          delete from mwsdocpostomwsord where docposid = :docposid;
          delete from docpos2real where docposid = :docposid;
        end
      end
    end
    delete from mwsdocpostomwsord where quantity <= 0 and mwsord = :refmwsord;
    delete from mwsconstlocsforbidden where docgroup = :docgroup;
    -- dla zlecen na odbior wlasny gdy jest wiecej jak 20 pozycji to generujemy kolejne zlecenie
    -- bedzie szybciej sie je realizowalo
    if (:maxnumber > :divwhenmore and :dontdiv = 0 and :frommwsconstloc is null) then
    begin
      select count(mwsord)
        from mwsdocpostomwsord
        where quantity > 0 and mwsord = :refmwsord
        into :posleft;
      if (:posleft is null) then posleft = 0;
      if (:posleft + :maxnumber > :divwhenmore) then
        nextmwsord = 1;
      else
        dontdiv = 1;
    end
  end
  update dokumnag set genmwsordsafter = 0, mwsdocreal = 4 where ref = :docid;

  --ustawienie odpowiedniego statusu na zleceniu 7 lub 1
  multicnt = 0;
  if (:multi = 0) then mwsordstatus = 1;
  else if (:multi > 0) then
  begin
    select status, multicnt from mwsords where ref = :refmwsord
    into :mwsordstatus, :multicnt;

    if (:mwsordstatus = 0 and :newmwsord = 1) then
      mwsordstatus = 7;

    multicnt = :multicnt + 1;
    if (:multicnt >= 5) then
      mwsordstatus = 1;
  end

  update mwsords o
    set o.status = :mwsordstatus, o.multicnt = :multicnt
    where o.ref = :refmwsord;

  --execute procedure xk_mws_mwsord_route_opt(:refmwsord); --przeniesione do procedury DEVIDE
  delete from mwsdocpostomwsord where mwsord = :refmwsord;
  update dokumnag set genmwsordsafter = 0 where ref = :docid;
  if (not exists (select a.ref
        from mwsacts a
        where a.mwsord = :refmwsord)) then
  begin
    update mwsacts set status = 0 where mwsord = :refmwsord;
    update mwsords set status = 0 where ref = :refmwsord;
    update dokumnag set genmwsordsafter = 0, mwsdocreal = 4 where ref = :docid;
    delete from mwsords where ref = :refmwsord;
  end

  if (:mwsordstatus in (1,7) and exists(select first 1 1 from mwsords where ref = :refmwsord)) then
    execute procedure xk_mws_mwsord_out_devide(:refmwsord);

  suspend;
end^
SET TERM ; ^
