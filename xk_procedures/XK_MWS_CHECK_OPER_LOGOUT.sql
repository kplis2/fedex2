--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_CHECK_OPER_LOGOUT(
      HISTORYREF integer)
  returns (
      DISTANCE integer,
      MWSACCESSORY varchar(30) CHARACTER SET UTF8                           )
   as
declare variable mwsaccessoryref integer;
declare variable typ integer;
declare variable operator integer;
begin
  select m.mwsaccessory, m.distancefrom, ma.symbol, ma.mwsaccessorytype, o.ref
    from mwsaccessoryhist m
      left join mwsaccessories ma on(m.mwsaccessory = ma.ref)
      left join operator o on(m.operator = o.ref)
    where m.ref = :historyref
    into mwsaccessoryref, distance, mwsaccessory, typ, operator;
  if (mwsaccessoryref is null) then
    update mwsaccessoryhist m set m.timestartto = current_timestamp(0), m.distanceto = 0
      where m.ref = :historyref and m.timestartto is null;
  if (typ = 10) then
  begin
    update mwsaccessoryhist m set m.timestartto = current_timestamp(0), m.distanceto = 0
      where m.ref = :historyref and m.timestartto is null;
    --update mwsaccessories m set m.aktuoperator = null where m.ref = :mwsaccessoryref;
    mwsaccessory = null;
  end 
  suspend;
end^
SET TERM ; ^
