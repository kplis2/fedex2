--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_PR_PREPRODUCTION_LIST_GET(
      PREPROD_CLIENT integer,
      PREPROD_MPKTM varchar(40) CHARACTER SET UTF8                           ,
      PREPROD_ORDTYPE varchar(3) CHARACTER SET UTF8                           ,
      PREPROD_ORDREG varchar(100) CHARACTER SET UTF8                           ,
      PREPROD_FROMDATE varchar(100) CHARACTER SET UTF8                           ,
      PREPROD_TODATE varchar(100) CHARACTER SET UTF8                           ,
      PREPROD_ORDSLIST varchar(1024) CHARACTER SET UTF8                           )
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      NAZWAT varchar(255) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      PRSHMAT integer,
      ILOSC numeric(14,4),
      ILOSCMAT numeric(14,4),
      MIARA varchar(5) CHARACTER SET UTF8                           ,
      WORKPLACE varchar(40) CHARACTER SET UTF8                           )
   as
begin
  for select KTM,NAZWAT,WERSJAREF,PRSHMAT,SUM(ILOSC) as ILOSC,ILOSCMAT,miara,workplace
    from XK_PR_PREPRODUCTION_LIST (:PREPROD_CLIENT,:PREPROD_MPKTM,:PREPROD_ORDTYPE,:PREPROD_ORDREG,:PREPROD_FROMDATE,:PREPROD_TODATE,:PREPROD_ORDSLIST)
    group by KTM,NAZWAT,WERSJAREF,PRSHMAT,ILOSCMAT,miara,workplace
  into KTM, NAZWAT, WERSJAREF, PRSHMAT, ILOSC, ILOSCMAT, miara, workplace
  do suspend;
end^
SET TERM ; ^
