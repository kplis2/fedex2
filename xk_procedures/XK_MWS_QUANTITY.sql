--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_QUANTITY(
      BRANCH varchar(40) CHARACTER SET UTF8                           ,
      WH varchar(40) CHARACTER SET UTF8                           ,
      VERS integer)
  returns (
      NUMBER integer,
      DESCRIPT varchar(100) CHARACTER SET UTF8                           ,
      PM varchar(1) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4))
   as
declare variable WMS smallint;
declare variable TMP numeric(14,4);
begin
  number = 0;
  if (wh <> '') then
  begin
    tmp = 0;
    quantity = 0;
    select sum(s.ilosc)
      from stanyil s
        left join defmagaz m on (m.symbol = s.magazyn)
      where s.wersjaref = :vers and s.magazyn = :wh and coalesce(m.mws,0) = 0
      into quantity;
    pm = '+';
    descript = 'Stany z magazynów nie WMS';
    if (quantity <> 0) then
    begin
      number = number + 1;
      suspend;
      tmp = tmp + quantity;
    end
    quantity = 0;
    select coalesce(sum(s.quantity - s.blocked + s.ordered),0)
      from mwsstock s
        left join mwsconstlocs c on (s.mwsconstloc = c.ref)
        left join defmagaz d on (d.symbol = c.wh)
      where c.act > 0 and c.locdest in (1,2,3,4,5,9) and d.mws = 1
        and s.vers = :vers
        and s.wh = :wh
      into quantity;
    pm = '+';
    descript = 'Na lokacjach do sprzedaży';
    if (quantity <> 0) then
    begin
      number = number + 1;
      suspend;
      tmp = tmp + quantity;
    end
    quantity = 0;
    select coalesce(sum(a.quantity),0)
      from mwsords o
        left join mwsacts a on (a.mwsord = o.ref)
      where o.mwsordtypedest = 9 and o.status in (1,2) and a.status > 0 and a.status < 6
        and a.vers = :vers
        and a.wh = :wh
      into quantity;
    pm = '+';
    descript = 'Dostawienie na lok. do sprz.';
    if (quantity <> 0) then
    begin
      number = number + 1;
      suspend;
      tmp = tmp + quantity;
    end
    quantity = 0;
    select coalesce(sum(case when n.mwsdisposition = 1 then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0)),0)
      from dokumpoz p
        left join towary t on (t.ktm = p.ktm)
        left join dokumnag n on (n.ref = p.dokument)
        left join defdokummag d on (d.typ = n.typ and d.magazyn = n.magazyn)
      where p.genmwsordsafter = 1
        and coalesce(p.fake,0) = 0 and coalesce(p.havefake,0) = 0
        and coalesce(d.afterackproc,'') <> '' and d.mwsordwaiting = 1
        and n.wydania = 1
        and t.usluga <> 1 and n.mwsdoc = 1 and n.frommwsord is null
        and p.wersjaref = :vers and n.magazyn = :wh
        and (n.akcept = 1 or n.mwsdisposition = 1)
      into quantity;
    pm = '-';
    descript = 'Oczekuje na zlecenia';
    if (quantity <> 0) then
    begin
      number = number + 1;
      suspend;
      tmp = tmp - quantity;
    end
    quantity = 0;
    select sum(s.zablokow)
      from stanyil s
      where s.wersjaref = :vers and s.magazyn = :wh
      into quantity;
    pm = '-';
    descript = 'Zablokowano na zamówieniach';
    if (quantity > 0) then
    begin
      number = number + 1;
      suspend;
      tmp = tmp - quantity;
    end
    pm = '';
    descript = 'Suma';
    quantity = tmp;
    number = number + 1;
    suspend;
  end else if (branch <> '') then
  begin
    tmp = 0;
    quantity = 0;
    select sum(s.ilosc)
      from stanyil s
        left join defmagaz m on (m.symbol = s.magazyn)
      where s.wersjaref = :vers and s.oddzial = :branch and coalesce(m.mws,0) = 0
      into quantity;
    pm = '+';
    descript = 'Stany z magazynów nie WMS';
    if (quantity <> 0) then
    begin
      number = number + 1;
      suspend;
      tmp = tmp + quantity;
    end
    quantity = 0;
    select coalesce(sum(s.quantity - s.blocked + s.ordered),0)
      from mwsstock s
        left join mwsconstlocs c on (s.mwsconstloc = c.ref)
        left join defmagaz d on (d.symbol = c.wh)
      where c.act > 0 and c.locdest in (1,2,3,4,5,9) and d.mws = 1
        and s.vers = :vers
        and s.branch = :branch
      into quantity;
    pm = '+';
    descript = 'Na lokacjach do sprzedaży';
    if (quantity <> 0) then
    begin
      number = number + 1;
      suspend;
      tmp = tmp + quantity;
    end
    quantity = 0;
    select coalesce(sum(a.quantity),0)
      from mwsords o
        left join mwsacts a on (a.mwsord = o.ref)
      where o.mwsordtypedest = 9 and o.status in (1,2) and a.status > 0 and a.status < 6
        and a.vers = :vers
        and o.branch = :branch
      into quantity;
    pm = '+';
    descript = 'Dostawienie na lok. do sprz.';
    if (quantity <> 0) then
    begin
      number = number + 1;
      suspend;
      tmp = tmp + quantity;
    end
    quantity = 0;
    select coalesce(sum(case when n.mwsdisposition = 1 then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0)),0)
      from dokumpoz p
        left join towary t on (t.ktm = p.ktm)
        left join dokumnag n on (n.ref = p.dokument)
        left join defdokummag d on (d.typ = n.typ and d.magazyn = n.magazyn)
      where p.genmwsordsafter = 1
        and coalesce(p.fake,0) = 0 and coalesce(p.havefake,0) = 0
        and coalesce(d.afterackproc,'') <> '' and d.mwsordwaiting = 1
        and n.wydania = 1
        and t.usluga <> 1 and n.mwsdoc = 1
        and p.wersjaref = :vers and n.oddzial = :branch
        and (n.akcept = 1 or n.mwsdisposition = 1)
      into quantity;
    pm = '-';
    descript = 'Oczekuje na zlecenia';
    if (quantity <> 0) then
    begin
      number = number + 1;
      suspend;
      tmp = tmp - quantity;
    end
    quantity = 0;
    select sum(s.zablokow)
      from stanyil s
      where s.wersjaref = :vers and s.oddzial = :branch
      into quantity;
    pm = '-';
    descript = 'Zablokowano na zamówieniach';
    if (quantity > 0) then
    begin
      number = number + 1;
      suspend;
      tmp = tmp - quantity;
    end
    pm = '';
    descript = 'Suma';
    quantity = tmp;
    number = number + 1;
    suspend;
  end
end^
SET TERM ; ^
