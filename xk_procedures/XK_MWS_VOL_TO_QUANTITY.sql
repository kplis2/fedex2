--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_VOL_TO_QUANTITY(
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      VOLUME numeric(14,6),
      MODE smallint)
  returns (
      T1 integer,
      Q1 numeric(14,4),
      T2 integer,
      Q2 numeric(14,4),
      T3 integer,
      Q3 numeric(14,4),
      T4 integer,
      Q4 numeric(14,4),
      T5 integer,
      Q5 numeric(14,4),
      QUANTITY numeric(14,4))
   as
declare variable quantityb numeric(14,4);
declare variable przelicz numeric(14,4);
declare variable num integer;
declare variable t integer;
declare variable q numeric(14,6);
declare variable jedn varchar(5);
declare variable tvol numeric(14,6);
declare variable g smallint;
begin
  num = 0;
  tvol = 0;
  quantity = 0;
  for
    select t.ref, t.przelicz, t.jedn, (t.dlug / 100) * (t.wys / 100) * (t.szer / 100), t.glowna
      from towjedn t
      where t.ktm = :good
      order by t.przelicz desc
      into t, przelicz, jedn, tvol, g
  do begin
    q = 0;
    if (tvol is null or tvol = 0) then tvol = 1;
    if (przelicz is null) then przelicz = 0;
    q = volume / tvol;
    if (g = 0) then
      q = cast(q - 0.499999 as integer);
    if (q > 0) then
      num = num + 1;
    if (num = 1 and q > 0) then
    begin
      t1 = t;
      q1 = q;
    end else if (num = 2 and q > 0) then
    begin
      t2 = t;
      q2 = q;
    end else if (num = 3 and q > 0) then
    begin
      t3 = t;
      q3 = q;
    end else if (num = 4 and q > 0) then
    begin
      t4 = t;
      q4 = q;
    end else if (num = 5 and q > 0) then
    begin
      t5 = t;
      q5 = q;
    end
    volume = volume - q * tvol;
    if (q > 0) then
    begin
      quantity = quantity + przelicz * q;
    end
  end
  suspend;
end^
SET TERM ; ^
