--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_EAN13_KODWLASNY
  returns (
      KODWLASNY varchar(20) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE KW VARCHAR(20);
begin
/*select wartosc from konfig k where wartosc = '5904092' into :kod_z_konfiga;
to wykorzystam dla okrelonej "wartosci"*/

  select max(substring(kodkresk from 8 for 12)) FROM towkodkresk
    where coalesce(char_length(kodkresk),0) = 13 into :kw; -- [DG] XXX ZG119346
  kw = cast(kw as integer)+1;
  if (kw is null) then kodwlasny = '00001';
  if (kw between 1 and 9) then kodwlasny = '0000'||:kw;
  else if (kw between 10 and 99) then kodwlasny = '000'||:kw;
  else if (kw between 100 and 999) then kodwlasny = '00'||:kw;
  else if (kw between 1000 and 9999) then kodwlasny = '0'||:kw;
  else if (kw between 10000 and 99999) then kodwlasny = :kw;
  suspend;
end^
SET TERM ; ^
