--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_GOOD_BARCODES(
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      UNIT integer)
  returns (
      RREF integer,
      RBARCODE varchar(20) CHARACTER SET UTF8                           ,
      RUNIT integer,
      RUNITNAME varchar(5) CHARACTER SET UTF8                           ,
      RVERS integer,
      RVERSNAME varchar(255) CHARACTER SET UTF8                           )
   as
declare variable tryb smallint;
begin
  if (:good is null) then good = '';
  if (:vers is null) then vers = 0;
  if (:unit is null) then unit = 0;

  if (:good = '' and :vers = 0 and unit = 0) then
    exit;
  else if (:vers <> 0 and :unit = 0) then tryb = 0;
  else if (:vers <> 0 and :unit <> 0) then tryb = 1;
  else if (:good <> '' and :unit = 0) then tryb = 2;
  else if (:good <> '' and :unit <> 0) then tryb = 3;

  if (:tryb = 0) then
  begin
    for
      select k.ref, k.kodkresk, k.towjednref, j.jedn, w.nazwa
        from towkodkresk k
          join towjedn j on (k.towjednref = j.ref)
          join wersje w on (k.wersjaref = w.ref)
        where k.wersjaref = :vers
      into :rref, :rbarcode, :runit, :runitname, :rversname
    do begin
      rvers = :vers;
      suspend;
    end
  end
  else if (:tryb = 1) then
  begin
    for
      select k.ref, k.kodkresk, k.towjednref, j.jedn, w.nazwa
        from towkodkresk k
          join towjedn j on (k.towjednref = j.ref)
          join wersje w on (k.wersjaref = w.ref)
        where k.wersjaref = :vers
          and k.towjednref = :unit
      into :rref, :rbarcode, :runit, :runitname, :rversname
    do begin
      rvers = :vers;
      suspend;
    end
  end
  else if (:tryb = 2) then
  begin
    for
      select k.ref, k.kodkresk, k.towjednref, j.jedn, w.nazwa
        from towkodkresk k
          join towjedn j on (k.towjednref = j.ref)
          join wersje w on (k.wersjaref = w.ref)
        where k.ktm = :good
      into :rref, :rbarcode, :runit, :runitname, :rversname
    do begin
      rvers = :vers;
      suspend;
    end
  end
  else if (:tryb = 3) then
  begin
    for
      select k.ref, k.kodkresk, k.towjednref, j.jedn, w.nazwa
        from towkodkresk k
          join towjedn j on (k.towjednref = j.ref)
          join wersje w on (k.wersjaref = w.ref)
        where k.ktm = :good
          and k.towjednref = :unit
      into :rref, :rbarcode, :runit, :runitname, :rversname
    do begin
      rvers = :vers;
      suspend;
    end
  end
end^
SET TERM ; ^
