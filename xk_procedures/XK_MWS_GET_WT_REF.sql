--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_WT_REF(
      LASWTTREF integer,
      AKTUOPERATOR integer,
      WH varchar(3) CHARACTER SET UTF8                           ,
      WHSECGROUPS varchar(40) CHARACTER SET UTF8                            = '')
  returns (
      NEWWTREF integer,
      DESCRIPTION varchar(3500) CHARACTER SET UTF8                           )
   as
declare variable docgroup integer;
declare variable newref integer;
declare variable ordtype varchar(4);
declare variable mwsordtypedest smallint;
declare variable operator integer;
declare variable status smallint;
declare variable lastmwsordtypedest integer;
begin
  if (laswttref is not null) then
    select mwsordtypedest from mwsords where ref = :laswttref
      into lastmwsordtypedest;
  select first 1 x.ref, x.mwsordtypes, x.description
    from xk_mws_mwsordlist(:wh,:aktuoperator,:whsecgroups) x
      where ((x.operator is null and x.status = 1) or (x.operator = :aktuoperator and x.status = 2))
      and x.mwsordtypedest = 1
      into :newwtref, :ordtype, :description;
  if (newwtref is not null) then begin
--    update mwsords mo set mo.status = 1 where mo.operator is null mo.ref = :newwtref;
    execute procedure XK_MWS_MWSORD_CHANGE_STATUS(:newwtref,2,:aktuoperator) returning_values :status;
    -- update mwsords mo set mo.operator = :aktuoperator, mo.status = 2 where mo.ref = :newwtref and (mo.operator = :aktuoperator or mo.operator is null);
    -- select mo.operator from mwsords mo where mo.ref = :newwtref into :operator;
    if (:status = 1) then
      exception universal 'Zlecenie realizowane przez innego operatora! Pobierz ponownie.';
    select mo.operator
      from mwsords mo
      where mo.ref = :newwtref
      into :operator;
    if (operator is null) then exception universal 'Nie ustawiony operator!!';
    if (operator <> :aktuoperator) then exception universal 'Zlecenie realizuje inny operator. Przerwij realizacje zlecenia!!!'||:operator;
  end
  suspend;
end^
SET TERM ; ^
