--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_USUN_ZNAKI(
      TEKST varchar(1024) CHARACTER SET UTF8                           ,
      TRYB smallint)
  returns (
      RTEKST varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable eol varchar(3);
begin
eol = '
';
  if (coalesce(:tekst,'')='') then begin
    rtekst = '';
    suspend;
    exit;
  end

  rtekst = :tekst;
  if (:tryb = 0 or :tryb = 1) then begin
    rtekst = replace(:rtekst,  'Ą',  'A');
    rtekst = replace(:rtekst,  'ą',  'a');
    rtekst = replace(:rtekst,  'Ć',  'C');
    rtekst = replace(:rtekst,  'ć',  'c');
    rtekst = replace(:rtekst,  'Ę',  'E');
    rtekst = replace(:rtekst,  'ę',  'e');
    rtekst = replace(:rtekst,  'Ł',  'L');
    rtekst = replace(:rtekst,  'ł',  'l');
    rtekst = replace(:rtekst,  'Ń',  'N');
    rtekst = replace(:rtekst,  'ń',  'n');
    rtekst = replace(:rtekst,  'Ó',  'O');
    rtekst = replace(:rtekst,  'ó',  'o');
    rtekst = replace(:rtekst,  'Ś',  'S');
    rtekst = replace(:rtekst,  'ś',  's');
    rtekst = replace(:rtekst,  'Ż',  'Z');
    rtekst = replace(:rtekst,  'ż',  'z');
    rtekst = replace(:rtekst,  'Ź',  'Z');
    rtekst = replace(:rtekst,  'ź',  'z');
  end
  if (:tryb = 0 or :tryb = 2) then begin
    rtekst = replace(:rtekst,  '!',  '');
    rtekst = replace(:rtekst,  '@',  '');
    rtekst = replace(:rtekst,  '#',  '');
    rtekst = replace(:rtekst,  '$',  '');
    rtekst = replace(:rtekst,  '%',  '');
    rtekst = replace(:rtekst,  '^',  '');
    rtekst = replace(:rtekst,  '&',  '');
    rtekst = replace(:rtekst,  '*',  '');
    rtekst = replace(:rtekst, '(', '');
    rtekst = replace(:rtekst,  ')',  '');
    rtekst = replace(:rtekst,  '-',  '');
    rtekst = replace(:rtekst,  '=',  '');
    rtekst = replace(:rtekst,  '_',  '');
    rtekst = replace(:rtekst,  '+',  '');
    rtekst = replace(:rtekst,  '[',  '');
    rtekst = replace(:rtekst,  '{',  '');
    rtekst = replace(:rtekst,  ']',  '');
    rtekst = replace(:rtekst,  '}',  '');
    rtekst = replace(:rtekst,  '\',  '');
    rtekst = replace(:rtekst,  '|',  '');
    rtekst = replace(:rtekst,  ';',  '');
    rtekst = replace(:rtekst,  ':',  '');
    rtekst = replace(:rtekst,  '"',  '');
    rtekst = replace(:rtekst,  '''',  '');
    rtekst = replace(:rtekst, ',', '');
    rtekst = replace(:rtekst,  '<',  '');
    rtekst = replace(:rtekst,  '.',  '');
    rtekst = replace(:rtekst,  '>',  '');
    rtekst = replace(:rtekst,  '/',  '');
    rtekst = replace(:rtekst,  '?',  '');
    rtekst = replace(:rtekst,  ' ',  '');
  end
  else if (:tryb = 3) then begin
    rtekst = replace(:rtekst,  ' ',  '');
  end
  else if (:tryb = 4) then begin
    rtekst = replace(:rtekst,  :eol,  ' ');
  end
  else if (:tryb = 5) then begin
    rtekst = replace(:rtekst,  '''''',  '''');
  end

  --XXX JO: Tryb 6 - zastap znak spacja [PM] XXX #SELFSHIPPING
  else if (:tryb = 6) then begin
    rtekst = replace(:rtekst,  '!',  ' ');
    rtekst = replace(:rtekst,  '@',  ' ');
    rtekst = replace(:rtekst,  '#',  ' ');
    rtekst = replace(:rtekst,  '$',  ' ');
    rtekst = replace(:rtekst,  '%',  ' ');
    rtekst = replace(:rtekst,  '^',  ' ');
    rtekst = replace(:rtekst,  '&',  ' ');
    rtekst = replace(:rtekst,  '*',  ' ');
    rtekst = replace(:rtekst, '(', ' ');
    rtekst = replace(:rtekst,  ')',  ' ');
    rtekst = replace(:rtekst,  '-',  ' ');
    rtekst = replace(:rtekst,  '=',  ' ');
    rtekst = replace(:rtekst,  '_',  ' ');
    rtekst = replace(:rtekst,  '+',  ' ');
    rtekst = replace(:rtekst,  '[',  ' ');
    rtekst = replace(:rtekst,  '{',  ' ');
    rtekst = replace(:rtekst,  ']',  ' ');
    rtekst = replace(:rtekst,  '}',  ' ');
    rtekst = replace(:rtekst,  '\',  ' ');
    rtekst = replace(:rtekst,  '|',  ' ');
    rtekst = replace(:rtekst,  ';',  ' ');
    rtekst = replace(:rtekst,  ':',  ' ');
    rtekst = replace(:rtekst,  '"',  ' ');
    rtekst = replace(:rtekst,  '''',  ' ');
    rtekst = replace(:rtekst, ',', ' ');
    rtekst = replace(:rtekst,  '<',  ' ');
    rtekst = replace(:rtekst,  '>',  ' ');
    rtekst = replace(:rtekst,  '/',  ' ');
    rtekst = replace(:rtekst,  '?',  ' ');
    rtekst = replace(:rtekst,  ' ',  ' ');
  end
  suspend;
end^
SET TERM ; ^
