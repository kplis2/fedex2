--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_HH_GOOD_FIND(
      KODKRESK varchar(20) CHARACTER SET UTF8                           )
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      KTMDICT varchar(255) CHARACTER SET UTF8                           ,
      VERS integer)
   as
begin
  select t.ktm, t.wersjaref, o.nazwa
    from towkodkresk t
      left join towary o on(t.ktm = o.ktm)
    where t.kodkresk = :kodkresk
    into :ktm, :vers, :ktmdict;
  if (ktm is null) then
    select t.ktm, t.nazwa
      from towary t
      where upper(t.ktm) = upper(:kodkresk)
      into :ktm, :ktmdict;
  suspend;
end^
SET TERM ; ^
