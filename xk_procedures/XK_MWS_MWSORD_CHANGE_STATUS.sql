--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSORD_CHANGE_STATUS(
      MWSORDREF integer,
      STATUS integer,
      AKTUOPERATOR integer)
  returns (
      REALIZED smallint)
   as
declare variable ordref integer;
declare variable mwsordtype integer;
declare variable wtirealize smallint;
declare variable mwsordtypedest smallint;
declare variable docid integer;
begin
  execute procedure set_global_param('AKTUOPERATOR',:aktuoperator);
  select mo.ref, mo.mwsordtype, mo.mwsordtypedest, mo.docid
    from mwsords mo
    where mo.ref = :mwsordref and mo.status < 5
    and (mo.operator = :aktuoperator or mo.operator is null)
    into :ordref, mwsordtype, :mwsordtypedest, docid;
  if (mwsordtypedest = 2 and status in (4,5) and exists(select first 1 1 from mwsacts a where a.mwsord = :mwsordref and a.status = 2)) then
    exception mwsord_except 'Nie wszystkie pozycje zostały potwierdzone';
  if (ordref is not null) then
  begin
    if (mwsordtypedest = 3 and status = 5) then begin
      update mwsacts ma set ma.status = 2, ma.quantityc = ma.quantity where ma.mwsord = :mwsordref and ma.status < 5 and ma.status > 0;
    end
    if (mwsordtypedest = 2 and docid is null and status = 4) then
      status = 5;
    if (mwsordtypedest = 2 and docid > 0 and status = 4) then
    begin
      if (not exists (select first 1 1 from XK_MWS_GOOD_ON_MWSORD(:mwsordref,1))) then
        status = 5;
    end
    update mwsords mo set mo.status = :status, mo.operator = :aktuoperator where mo.ref = :ordref and (mo.operator is null or mo.operator = :aktuoperator);
    realized = 0;
  end
  else
    realized = 1;
  execute procedure clear_global_params;
  suspend;
end^
SET TERM ; ^
