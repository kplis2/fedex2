--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_CHECK_STOCKQUANTITY(
      VERSIN integer,
      LOTIN integer,
      WHIN varchar(3) CHARACTER SET UTF8                           ,
      MWSCONSTLOCIN integer,
      MWSPALLOCIN integer)
  returns (
      QUANTITY numeric(14,4))
   as
declare variable TMP numeric(14,4);
begin
  if (:lotin = 0) then lotin = null;
  if (:mwsconstlocin = 0) then mwsconstlocin = null;
  if (:mwspallocin = 0) then mwspallocin = null;

  quantity = 0;
  select coalesce(sum(s.quantity - s.blocked + s.ordered),0)
    from mwsstock s
      left join mwsconstlocs c on (s.mwsconstloc = c.ref)
      left join defmagaz d on (d.symbol = c.wh)
    where c.act > 0
      and c.locdest in (1,2,3,4,5,9)
      and d.mws = 1
      and s.vers = :versin
      and s.wh = :whin
      and (:lotin is null or (:lotin is not null and s.lot = :lotin))
      and (:mwsconstlocin is null or (:mwsconstlocin is not null and s.mwsconstloc = :mwsconstlocin))
      and (:mwspallocin is null or (:mwspallocin is not null and s.mwspalloc = :mwspallocin))
  into :quantity;

  if (:quantity is null) then quantity = 0;
  tmp = 0;

  select coalesce(sum(a.quantity),0)
    from mwsacts a
      left join dokumpoz p on (p.ref = a.docposid)
      left join dokumnag n on (n.ref = p.dokument)
    where a.vers = :versin
      and a.wh = :whin
      and a.mwsordtypedest in (1,8)
      and a.status = 0
      and a.docposid > 0
      and a.doctype = 'M'
      and p.ref is not null
      and (n.akcept in (1,9) or n.mwsdisposition = 1)
      and (:lotin is null or (:lotin is not null and a.lot = :lotin))
      and (:mwsconstlocin is null or (:mwsconstlocin is not null and a.mwsconstlocp = :mwsconstlocin))
      and (:mwspallocin is null or (:mwspallocin is not null and a.mwspallocp = :mwspallocin))
    into tmp;

  quantity = :quantity - :tmp;
end^
SET TERM ; ^
