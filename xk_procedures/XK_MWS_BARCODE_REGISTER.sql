--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_BARCODE_REGISTER(
      VERS integer,
      JEDN integer,
      BARCODE varchar(20) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable good varchar(40);
begin
  status = 0;
  msg = 'Nie dodano kodu kreskowego!';
  select ktm from wersje where ref = :vers
  into :good;
  if (exists (select first 1 1 from towkodkresk where ktm = :good and wersjaref = :vers and towjednref <> :jedn and kodkresk = :barcode)) then
  begin
    status = 2;
    msg = 'Podany kod kreskowey jest już przypisany do innej jednostki tego towaru';
  end
  else if (exists (select first 1 1 from towkodkresk where (ktm <> :good or wersjaref <> :vers or towjednref <> :jedn) and kodkresk = :barcode)) then
  begin
    status = 2;
    msg = 'Podany kod kreskowey jest już przypisany do innego towaru';
  end
  else if (not exists(select first 1 1 from towkodkresk where ktm = :good and wersjaref = :vers and towjednref = :jedn and kodkresk = :barcode)) then
  begin
    insert into towkodkresk(ktm, wersjaref, kodkresk, gl, towjednref)
      values(:good,:vers,:barcode,0,:jedn);
    status = 1;
    msg = 'Kod kreskowy został dodany prawidłowo';
  end else if (exists(select first 1 1 from towkodkresk where ktm = :good and wersjaref = :vers and towjednref = :jedn and kodkresk = :barcode)) then
  begin
    status = 1;
    msg = 'Kod kreskowy został dodany prawidłowo';
  end
end^
SET TERM ; ^
