--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_GOODNAME(
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      AKTUMAGAZYN varchar(3) CHARACTER SET UTF8                           )
  returns (
      GOODNAME varchar(255) CHARACTER SET UTF8                           ,
      VERSNAME varchar(255) CHARACTER SET UTF8                           ,
      MAGBREAK smallint,
      X_PARTIE SMALLINT_ID,
      X_SERIE SMALLINT_ID,
      X_SLOWNIKDEF INTEGER_ID)
   as
declare variable KTMMAGBREAK smallint;
declare variable MWSNAME varchar(255);
begin
  goodname = '';

  select trim(t.mwsnazwa), t.nazwa, t.magbreak
    from towary t
  where t.ktm = :good
  into :mwsname, :goodname, :ktmmagbreak;
  if (coalesce(:mwsname,'') <> '') then
    goodname = :mwsname;

  if (:vers is not null) then
    select nazwa
     ,X_MWS_SERIE ,X_MWS_Partie, X_MWS_SLOWNIK_EHRLE   --XXX MatJ
     from wersje where ref = :vers
    into :versname
    ,:x_serie, :X_partie, :x_slownikdef;  --XXX MatJ

  select coalesce(d.magbreak,0) from defmagaz d
    where d.symbol = :aktumagazyn
  into :magbreak;

  if (:magbreak < 2) then
  begin
    if (:ktmmagbreak > :magbreak) then
      magbreak = :ktmmagbreak;
  end
end^
SET TERM ; ^
