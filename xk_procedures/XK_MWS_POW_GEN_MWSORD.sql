--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_POW_GEN_MWSORD(
      WHIN DEFMAGAZ_ID)
  returns (
      STATUS SMALLINT_ID)
   as
declare variable DOCID DOKUMNAG_ID;
declare variable DOCGROUP DOKUMNAG_ID;
declare variable MWSORD MWSORDS_ID;
declare variable DOCPOSID DOKUMPOZ_ID;
declare variable DONEXT SMALLINT_ID;
declare variable STATUS3 SMALLINT_ID;
declare variable STATUS2 SMALLINT_ID;
declare variable SYMBOL SYMBOL_ID;
declare variable CNT INTEGER_ID;
declare variable CPR SMALLINT_ID;
declare variable ILLINIS STRING100;
declare variable ILLINI INTEGER_ID;
declare variable RECALC SMALLINT_ID;
declare variable DNIWSTECZ INTEGER_ID;
declare variable TMPDATE DATE_ID;
declare variable DOCCHECKCNT SMALLINT_ID;
declare variable DOCCNT SMALLINT_ID;
begin
  status = 1;
  recalc = 1;

  dniwstecz = 60;
  tmpdate = current_date - :dniwstecz;
  doccheckcnt = 5; --ile przebiegow petli za jednym przejsciem
  doccnt = 0; --licznik przerobionych dokumentow

  execute procedure get_config('MWSILOSCACTS',2) returning_values :illinis;
  if (:illinis is null or :illinis = '') then
    illini = 0;
  else
    illini = cast(:illinis as smallint);

  while(:doccnt < :doccheckcnt)
  do begin
    select count(a.ref)
      from mwsacts a
        left join mwsords o on (o.ref = a.mwsord)
      where o.wh = :whin
        and o.status < 5 and o.status > 0
        and a.status < 3 and a.status > 0
        and a.mwsordtypedest = 1
        and o.takefullpal = 0
        and (o.regtime > :tmpdate)
        and coalesce(a.ispal,0) <> 1
      into :cnt;
    if (:cnt is null) then cnt = 0;
    --cpr = null;
    if (:cnt > :illini) then
      exit; --nie generuj jesli linii jest za duzo
    --cpr = 3;
    --if (:cnt <= 300) then

    recalc = 0;

    in autonomous transaction
    do begin
      for
        select first 1 d.ref, d.grupasped
          from dokumnag d
            left join sposdost s on (s.ref = d.sposdost)
            left join defdokummag dm on (dm.typ = d.typ and dm.magazyn = dm.magazyn)
          where coalesce(d.mwsmag,d.magazyn) = :whin and dm.mwsordwaiting = 1
            and d.data > :tmpdate
            and ((d.akcept = 1 and d.mwsdisposition = 0)
              or (d.akcept = 0 and d.mwsdisposition = 1))
            and (d.genmwsordsafter = 1)
            and (d.blokada = 0 or d.blokada = 4)
            --and d.ref = 172
          order by d.priorytet, d.ref
          into :docid, :docgroup
      do begin
        status3 = 0;
        execute procedure xk_mws_pow_mwsordswaiting_doc(:docid)
          returning_values :status3;
        if (:status3 is null) then
          status3 = 0;

        mwsord = null;
        if (:status3 = 1 or :status3 = 2 or :status3 = 5) then
        begin
          execute procedure xk_mws_gen_mwsord_out(:docid,:docgroup,null,'M',null,null,:recalc,null)
            returning_values :mwsord;
          for
            select p.ref
              from dokumpoz p
                left join dokumnag d on (d.ref = p.dokument)
                join towary t on (p.ktm = t.ktm)
                left join dokumpoz p1 on (p1.ref = p.alttopoz and p.fake = 1)
                left join towary t1 on (t1.ktm = p1.ktm)
              where p.dokument = :docid
                and p.genmwsordsafter = 0
                and ((coalesce(p.havefake,0) = 0 and coalesce(p.fake,0) = 0) or
                     (coalesce(p.fake,0) = 1 and t1.usluga <> 1 and coalesce(t1.altposmode,0) = 1))
                and ((d.mwsdisposition = 0 and p.fake = 0 and p.iloscl = coalesce(p.ilosconmwsacts,0)) or
                     ((d.mwsdisposition = 1 or p.fake = 1) and p.ilosc = coalesce(p.ilosconmwsacts,0)))
                and t.usluga <> 1
              into :docposid
          do begin
            delete from mwsgoodsrefill where docposid = :docposid;
          end
          --exit;
        end
        else
          update dokumnag set genmwsordsafter = 0, mwsdocreal = 4 where ref = :docid;
      end
    end

    doccnt = :doccnt + 1;

  end

end^
SET TERM ; ^
