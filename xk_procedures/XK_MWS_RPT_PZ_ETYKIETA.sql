--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_RPT_PZ_ETYKIETA(
      MWSORD integer,
      PALGROUP integer)
  returns (
      GOODSTRING varchar(120) CHARACTER SET UTF8                           ,
      GOODSTRING2 varchar(120) CHARACTER SET UTF8                           ,
      GOODSTRING3 varchar(120) CHARACTER SET UTF8                           ,
      GOODSTRING4 varchar(120) CHARACTER SET UTF8                           ,
      GOODSTRING5 varchar(120) CHARACTER SET UTF8                           ,
      GOODSTRING6 varchar(120) CHARACTER SET UTF8                           ,
      GOODSTRING7 varchar(120) CHARACTER SET UTF8                           ,
      GOODSTRING8 varchar(120) CHARACTER SET UTF8                           ,
      GOODSTRING9 varchar(120) CHARACTER SET UTF8                           ,
      PRIORITY smallint)
   as
declare variable LICZNIK smallint;
declare variable KTM varchar(40);
declare variable QUANTITYSTRING varchar(200);
declare variable ILOSC integer;
declare variable PRZELICZNIK integer;
declare variable PALETA smallint;
declare variable MIX smallint;
declare variable MWSCONSTLOC integer;
declare variable CNT integer;
declare variable VERS integer;
declare variable MAGBREAK smallint;
declare variable WH varchar(3);
declare variable DOCID integer;
declare variable SYMBOLDOSTAWY varchar(120);
declare variable STATUS smallint;
begin
   licznik = 0;
   goodstring = '';
   goodstring2 = '';
   goodstring3 = '';
   goodstring4 = '';
   select count(distinct ma.mwsconstlocl), max(mo.priority), max(mo.wh)
     from mwsacts ma
       join mwsords mo on (mo.ref = ma.mwsord)
     where mo.palgroup = :palgroup and mo.mwsordtypedest = 11
     into :cnt, :priority, wh;
   select magbreak from defmagaz where symbol = :wh
     into magbreak;
   select status from mwspallocs where ref = :palgroup
     into status;
   if (status is null or status > 2 or status < 0) then
     status = 1;
   if (magbreak is null) then magbreak = 0;
   for
     select ma.good, ma.vers, sum(ma.quantityc), ma.mixedpallgroup
       from mwsacts ma
         left join mwsords mo on (mo.ref = ma.mwsord)
         left join towary t on(t.ktm = ma.good)
       where ma.palgroup = :palgroup and ma.mwsord = :mwsord
       group by ma.good, ma.vers, ma.mixedpallgroup
       into :ktm, :vers, ilosc, mix
   do begin
     if (mwsconstloc is null) then
       select first 1 distinct ma.mwsconstlocl
         from mwsacts ma
           join mwsords mo on (mo.ref = ma.mwsord)
         where mo.palgroup = :palgroup and mo.mwsordtypedest = 11
         into mwsconstloc;
     select QUANTITYSTRING
       from xk_mws_przelicz_jednostki(:ktm, :vers, :ilosc, 0)
       into QUANTITYSTRING;
 --      select x_opak_obj.przelicz from x_opak_obj(:ktm) into przelicznik;
   --    if (przelicznik is null or przelicznik = 0) then przelicznik = 1;
     --  ilosc = ilosc / przelicznik;
     if (licznik + coalesce(char_length(:ktm),0) <=20) then -- [DG] XXX ZG119346
     begin
       goodstring = goodstring||' '||:ktm;
       goodstring = goodstring||' ('||QUANTITYSTRING||')';
     end
     else if (licznik + coalesce(char_length(:ktm),0) > 20 and licznik + coalesce(char_length(:ktm),0) <= 40) then -- [DG] XXX ZG119346
     begin
       goodstring2 = goodstring2||' '||:ktm;
       goodstring2 = goodstring2||' ('||QUANTITYSTRING||')';
     end
     else if (licznik + coalesce(char_length(:ktm),0) > 40 and licznik + coalesce(char_length(:ktm),0) <= 60) then -- [DG] XXX ZG119346
     begin
       goodstring3 = goodstring3||' '||:ktm;
       goodstring3 = goodstring3||' ('||QUANTITYSTRING||')';
     end
     else if (licznik + coalesce(char_length(:ktm),0) > 60 and licznik + coalesce(char_length(:ktm),0) <= 80) then -- [DG] XXX ZG119346
     begin
       goodstring4 = goodstring4||' '||:ktm;
       goodstring4 = goodstring4||' ('||QUANTITYSTRING||')';
     end else
     begin
       ktm = substring(:ktm from 1 for 80-licznik);
       goodstring4 = goodstring4||' '||:ktm;
     end
     licznik = licznik + coalesce(char_length(:ktm),0); -- [DG] XXX ZG119346
     if (mwsconstloc is not null) then
       select symbol from mwsconstlocs where ref = :mwsconstloc into goodstring5;
     else if (status = 2) then
       goodstring5 = 'W POLKI !!';
     else
       goodstring5 = '';
     if (exists (select ref from mwsconstlocacces where mwsconstloc = :mwsconstloc)) then
       goodstring6 = 'DEEP';
     else
     begin
       if (cnt > 1) then
         goodstring6 = 'DOKŁ.';
       else
         goodstring6 = '';
     end
     if (mix = 1) then
       goodstring7 = 'MIX';
     else
       goodstring7 = '';
     goodstring8 = '';
     if (magbreak = 2) then
     begin
       select docid from mwsords where ref = :mwsord into docid;
       if (docid is not null) then
       begin
         select ds.symbol
           from dokumnag d
             left join dostawy ds on (ds.ref = d.dostawa)
           where d.ref = :docid
           into goodstring8;
       end
     end else if (coalesce(status,0) <> 2) then
       goodstring8 = goodstring5;
   end
   goodstring9 = palgroup;
   suspend;
end^
SET TERM ; ^
