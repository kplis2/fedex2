--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MATRIXPOZZAMINSERT(
      DOCREF integer,
      TYPEDOC varchar(1) CHARACTER SET UTF8                           ,
      TYPEPARAM varchar(20) CHARACTER SET UTF8                           ,
      ATTRIBUTS varchar(255) CHARACTER SET UTF8                           ,
      AMOUNT numeric(14,4))
  returns (
      STATUS varchar(1) CHARACTER SET UTF8                           ,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable stringout1 VARCHAR(255) ;
declare variable attribut VARCHAR(255) ;
declare variable attributtype VARCHAR(255) ;
declare variable attributvalue VARCHAR(255) ;
declare variable atrsql VARCHAR(2048) ;
declare variable sql VARCHAR(1024) ;
declare variable joinparam VARCHAR(40) ;
declare variable wersjaref INTEGER ;
begin
  status = '0';
  msg = '';
  sql = '';
  atrsql = '';
  if (typeparam = 'KTM') then
    joinparam = ' ktm = w.ktm ';
  else
    joinparam = ' wersjaref = w.ref ';

  for
    select STRINGOUT
      from parsestring (:attributs,';')
      into :attribut
  do
  begin
    select first 1 stringout
      from parsestring(:attribut,'=')
      into :attributtype;

    select first 1 stringout
      from parsestring(:attribut,'=')
      order by lp desc
      into:attributvalue;

    atrsql = atrsql || ' and exists (select ktm from atrybuty where '||joinparam;
    atrsql = atrsql || ' and cecha = '||:attributtype||' and wartosc = '||:attributvalue||')';

  end

  sql = 'select first 1 w.ref, case when '''||:typeparam||''' = ''KTM''then w.ktm else w.nazwa end from wersje w where w.akt = 1 '||atrsql;

  execute statement :sql
    into :wersjaref, :msg;

  if (coalesce(:wersjaref, 0) > 0 ) then
  begin
    status = '1';
    if (:typedoc = 'Z') then
      insert into pozzam (zamowienie, wersjaref,  ilosc)
        values (:docref, :wersjaref, :amount);
  end else
    msg = 'Brak pozycji w kartotece';
    
  suspend;
end^
SET TERM ; ^
