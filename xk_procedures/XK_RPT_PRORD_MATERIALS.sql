--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_PRORD_MATERIALS(
      NAGZAMREF integer)
  returns (
      MATERIALKTM varchar(80) CHARACTER SET UTF8                           ,
      MATERIALNAME varchar(255) CHARACTER SET UTF8                           ,
      MATERIALAMOUNT numeric(14,4),
      MATERIALMEASURE varchar(5) CHARACTER SET UTF8                           )
   as
begin
  for select pzp.ktm, t.nazwa, pzp.ilosc, tj.jedn
    from nagzam nzp
    left join pozzam pzp on nzp.ref = pzp.zamowienie and pzp.out=1
    left join towary t on pzp.ktm = t.ktm
    left join towjedn tj on pzp.jedn = tj.ref
    where nzp.ref = :nagzamref
    into :materialktm, :materialname, :materialamount, :materialmeasure
  do suspend;
end^
SET TERM ; ^
