--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSORD_ROUTE_OPT(
      MWSORD integer)
   as
declare variable WEIGHT numeric(14,4);
declare variable VOL numeric(14,4);
declare variable ORDWEIGHT numeric(14,4);
declare variable ORDVOL numeric(14,4);
declare variable CNT integer;
declare variable OPT integer;
declare variable STARTAREA integer;
declare variable AREF integer;
declare variable WH varchar(3);
declare variable CURRSEC varchar(10);
declare variable CURRROW integer;
declare variable CURRPAL integer;
declare variable SYMBOL varchar(30);
declare variable SECTOCOMP varchar(10);
begin

 --exit;
 --exception universal mwsord;
  currsec = 'A';
  currrow = 1;
  currpal = 1;
  select count(distinct a.mwsconstlocp)
    from mwsacts a where a.mwsord = :mwsord and a.status < 5
    into cnt;
  if (cnt = 1) then
    exit;
  select wh from mwsords where ref = :mwsord
    into wh;
  cnt = 0;
  select count(ref) from mwsacts where mwsord = :mwsord and status < 5
    into cnt;
  update mwsacts set ord = 5, number = -1 where mwsord = :mwsord and status < 5;
  opt = 0;
  while (opt < cnt and exists(select first 1 ref from mwsacts where mwsord = :mwsord
      and status < 5 and number = -1))
  do begin
    opt = opt + 1;
    aref = null;
    select first 1 c.symbol, a.ref, substring(c.symbol from 1 for 1),
        1,
        1
      from mwsacts a
        left join mwsconstlocs c on (c.ref = a.mwsconstlocp)
      where a.mwsord = :mwsord and a.number = -1 and a.status < 5
      order by
        c.routerownum
 /*       case -- Ldz 131619 - kolejnosc zbierania z magazynow
          when substring(c.symbol from 1 for 2) = 'M6' then 1
          when substring(c.symbol from 1 for 2) = 'M5' then 2
          when substring(c.symbol from 1 for 2) = 'M4' then 3
          when substring(c.symbol from 1 for 2) = 'M3' then 4
          when substring(c.symbol from 1 for 2) = 'M2' then 5
          when substring(c.symbol from 1 for 2) = 'M1' then 6
          when substring(c.symbol from 1 for 2) = 'N1' then 7
          when substring(c.symbol from 1 for 2) = 'P1' then 8
          else 10
        end    */
      into symbol, aref, currsec, currrow, currpal;
    if (aref is null) then
      opt = cnt;
    if (aref is not null) then
      update mwsacts set number = :opt where ref = :aref;
  select first 1 substring(c.symbol from 1 for 1)
            from mwsacts a
              left join mwsconstlocs c on (c.ref = a.mwsconstlocp)
            where a.mwsord = :mwsord
              and a.status < 5
              and a.number = -1
            order by
              c.routerownum
  /*            case -- Ldz 131619 - kolejnosc zbierania z magazynow
                when substring(c.symbol from 1 for 2) = 'M6' then 1
                when substring(c.symbol from 1 for 2) = 'M5' then 2
                when substring(c.symbol from 1 for 2) = 'M4' then 3
                when substring(c.symbol from 1 for 2) = 'M3' then 4
                when substring(c.symbol from 1 for 2) = 'M2' then 5
                when substring(c.symbol from 1 for 2) = 'M1' then 6
                when substring(c.symbol from 1 for 2) = 'N1' then 7
                when substring(c.symbol from 1 for 2) = 'P1' then 8
                else 10
              end     */
    into sectocomp;
    if (currsec > 'B' and sectocomp = 'B') then
      currrow = 1;
  end
  update mwsacts set ord = 1 where mwsord = :mwsord and ord = 5 ;
end^
SET TERM ; ^
