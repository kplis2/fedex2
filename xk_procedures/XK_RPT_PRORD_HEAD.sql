--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_PRORD_HEAD(
      NAGZAMREF integer)
  returns (
      RECIPIENT varchar(80) CHARACTER SET UTF8                           ,
      PRORDDATE timestamp,
      REALDATE date,
      PRORDID varchar(30) CHARACTER SET UTF8                           ,
      ORDERID varchar(1024) CHARACTER SET UTF8                           ,
      ORDERCOMMENTS varchar(1024) CHARACTER SET UTF8                           ,
      CLIENTORDERID varchar(1024) CHARACTER SET UTF8                           )
   as
begin
  --narazie dla jednego polaczenia
  select first 1 kl.fskrot, nzp.datawe, nzs.datareal, nzp.id, nzs.id, nzs.uwagi, nzs.znakzewn
    from nagzam nzp
    left join pozzam pzp on nzp.ref = pzp.zamowienie and pzp.out=0
    left join relations r on r.stablefrom = 'POZZAM' and r.stableto = 'POZZAM' and r.sreffrom = pzp.ref and r.act = 1
    left join pozzam pzs on pzs.ref = r.srefto
    left join nagzam nzs on pzs.zamowienie = nzs.ref
    left join klienci kl on kl.ref = nzs.klient
    where nzp.ref = :nagzamref
  into :recipient, :prorddate, :realdate, :prordid, :orderid, :ordercomments, :clientorderid;
  suspend;
end^
SET TERM ; ^
