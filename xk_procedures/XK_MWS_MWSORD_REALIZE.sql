--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSORD_REALIZE(
      MWSORDREF integer,
      MWSACTREF integer,
      VERS integer,
      QUANTITY numeric(14,4),
      QUANTITYC numeric(14,4),
      DEFICIENCY smallint,
      MWSCONSTLOCREF integer,
      AKTUOPERATOR integer,
      MWSPALLOCREF integer,
      LOT integer,
      PARTIASTR STRING20 = '',
      SERIAL INTEGER_ID = null,
      SLOWNIK INTEGER_ID = null,
      CHANGE_ACT MWSACTS_ID = 0)
  returns (
      RMWSCONSTLOCREF integer,
      MWSCONSTLOC varchar(20) CHARACTER SET UTF8                           ,
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      VERSNAME varchar(255) CHARACTER SET UTF8                           ,
      RVERS integer,
      RQUANTITY numeric(14,4),
      RQUANTITYC numeric(14,4),
      RMWSACTREF integer,
      QUANTITYSTRING varchar(80) CHARACTER SET UTF8                           ,
      ORDDESCRIPT varchar(1000) CHARACTER SET UTF8                           ,
      ACTDESCRIPT varchar(1000) CHARACTER SET UTF8                           ,
      RAKTUOPERATOR integer,
      ORDSYMBOL varchar(20) CHARACTER SET UTF8                           ,
      REALSTATUS integer,
      PREPARED varchar(10) CHARACTER SET UTF8                           ,
      VERIFYMWSORD integer,
      RMWSPALLOCREF integer,
      RMWSPALLOC varchar(20) CHARACTER SET UTF8                           ,
      RLOT integer,
      RLOTSYMB varchar(80) CHARACTER SET UTF8                           ,
      WEIGHTLEFT numeric(14,4),
      RMAXQUANTITY numeric(14,4),
      CARTCOLOR integer,
      XMWSCONSTLOCDESC SYMBOL_ID,
      RPARTIASTR STRING20,
      RSERIAL INTEGER_ID,
      RSLOWNIK INTEGER_ID,
      RSERIALSTR STRING,
      RSLOWNIKSTR STRING,
      RTYPSERIALNO SMALLINT_ID,
      RMULTI SMALLINT_ID,
      ON_LOCATION NUMERIC_14_4,
      KOD_KRESK KODKRESKOWY)
   as
declare variable ACTSTATUS smallint;
declare variable LOCQUANTITY numeric(14,4);
declare variable ALLACTS integer;
declare variable ACTSREAL integer;
declare variable LACTREF integer;
declare variable LQUANC numeric(14,4);
declare variable LQUAN numeric(14,4);
declare variable ACTREF integer;
declare variable MAXNUMBER integer;
declare variable LACTDESCRIPT string255;
declare variable NRWERSJI integer;
declare variable DOCGROUP integer;
declare variable POSDOCGROUP integer;
declare variable DREF integer;
declare variable ORDVERIFY integer;
declare variable NEXTACT integer;
declare variable WTIOPERCNT integer;
declare variable WTICNT integer;
declare variable SHIPPINGTYPE integer;
declare variable MWSCONSTLOCVERIFY smallint;
declare variable VERIFYON smallint;
declare variable VERIFYQUAN integer;
declare variable VERIFYACTSCNT integer;
declare variable TMPREF integer;
declare variable MWSORDTYPE integer;
declare variable MWSORDOUT smallint;
declare variable WH varchar(3);
declare variable GOODVERIFY varchar(10);
declare variable LOCSYMB varchar(30);
declare variable OPERGRUPA varchar(1024);
declare variable DOCTYPE varchar(1);
declare variable MWSALLOWMORE smallint;
declare variable ADDACTREF integer;
declare variable OPERATORD integer;
declare variable ORDSTATUS smallint;
declare variable UNITP integer;
declare variable ACTLOT integer;
declare variable TMPMWSPALLOC integer;
declare variable TMPQUANTITY numeric(14,4);
declare variable SYMBOLSUB STRING40;
declare variable multi smallint_id;
declare variable x_partiastr string20;
declare variable x_partia date_id;              -- XXX KBI
declare variable x_serial_no integer_id;        -- XXX KBI
declare variable x_slownik integer_id;          -- XXX KBI
begin
    if (change_act = mwsactref) then
    change_act = 0;

  if (coalesce( partiastr,'') <> '') then
    x_partia = cast(partiastr as date_id);

  verifymwsord = 0;
  select wh, mwsordtype, doctype, operator, status, multi --XXX JO: multi
    from mwsords
    where ref = :mwsordref
    into wh, mwsordtype, doctype, operatord, :ordstatus, :multi;
  --MSt: Zmiany dla ArgoCard
  select unitp from mwsacts where ref = :mwsactref into :unitp;
  if (:ordstatus = 0) then
    exception universal 'Nie zaakceptowano zlecenia.';
  realstatus = 0;
  actdescript = '';
  if (wh is null) then exception universal 'Brak wskazania magazynu !!!';
  if (mwspallocref is null) then mwspallocref = 0;
  -- potwierdzanie pozycji
  select first 1 ma.ref, coalesce(m.mwsallowmore,0)
    from mwsacts ma
      left join dokumnag d on (d.ref = ma.docid)
      left join defdokummag m on (m.magazyn = d.magazyn and d.typ = m.typ)
    where ma.mwsord = :mwsordref
      and ma.status < 5
      and ma.status > 0
      into :nextact, mwsallowmore;
  if (mwsallowmore > 0) then
    rmaxquantity = 1000000;
  else
    rmaxquantity = 0;
  if (nextact is null) then mwsactref = 0;
  if (mwsactref is not null and mwsactref <> 0) then
  begin
    select docgroup from mwsacts a where a.ref = :mwsactref
    into :posdocgroup;

    if (deficiency = 0 and quantity = 0) then
      exception MWS_ZERO_COMMITED;
-- Sprawdzamy czy jest jescze jakas ilosc do potwierdzenia
    select sum(ma.quantity)
      from mwsacts ma
      where ma.mwsord = :mwsordref
        and ma.mwsconstlocp = :mwsconstlocref
        and ma.status < 3 and ma.status > 0
        and (ma.mwspallocp = :mwspallocref or :mwspallocref = 0)
        and (ma.lot = :lot or :lot = 0)
        and (ma.x_partia = coalesce(:x_partia,'1900-01-01') or coalesce(:x_partia,'1900-01-01') = '1900-01-01')
        and (ma.x_serial_no = coalesce(:serial,0) or coalesce(:serial,0) = 0)
        and (ma.x_slownik = coalesce(:slownik,0) or coalesce(:slownik,0) = 0)
        and ma.vers = :vers
        and ma.docgroup is not distinct from :posdocgroup  --Ldz - przy nullu nie dzialalo
      into lquan;
    if ((lquan >= quantity and mwsallowmore = 0) or (quantity > 0 and quantity < rmaxquantity and mwsallowmore = 1)) then
    begin
      --MSt: Zmiany dla ArgoCard
      if (coalesce(operatord,0) = 0) then
        update mwsords set operator = :aktuoperator where ref = :mwsordref;
      -- lecimy po pozycjach zlecenia ktore mozemy potwierdzic
      for
        select ma.ref, ma.quantity, ma.lot
          from mwsacts ma
          where ma.mwsord = :mwsordref
            and ma.status < 3 and ma.status > 0
            and ma.mwsconstlocp = :mwsconstlocref
            and (ma.mwspallocp = :mwspallocref or :mwspallocref = 0)
            and (ma.lot = :lot or :lot = 0)
            and (ma.x_partia = coalesce(:x_partia,'1900-01-01') or coalesce(:x_partia,'1900-01-01') = '1900-01-01')
            and (ma.x_serial_no = coalesce(:serial,0) or coalesce(:serial,0) = 0)
            and (ma.x_slownik = coalesce(:slownik,0) or coalesce(:slownik,0) = 0)
            and ma.vers = :vers
            and ma.docgroup is not distinct from :posdocgroup  --Ldz - przy nullu nie dzialalo
          order by ma.number desc
          into lactref, lquanc, actlot
      do begin
        -- nie jest zgoszona inwentaryzacja, po prostu potwierdzamy ilosc
        if (deficiency = 0) then
        begin
          if (quantity <= lquanc) then
            update mwsacts ma set ma.status = 2, ma.quantityc = :quantity, ma.operator = :aktuoperator where ma.ref = :lactref;
          else
            update mwsacts ma set ma.status = 2, ma.quantityc = :lquanc, ma.operator = :aktuoperator where ma.ref = :lactref;
          quantity = quantity - lquanc;
          if (quantity <= 0) then
            break;
        end
        else
        begin
--XXX JO: Blokada multipickingu - nie ruszac
       /*   if (:deficiency = 2) then
          begin
            update mwsacts ma set ma.status = 6, ma.operator = :aktuoperator where ma.ref = :lactref;
            update towary t set t.mwsmultinielacz = 1
              where ktm = (select ktm from wersje where ref = :vers);
          end
          else
          -- lecimy z obsluga zalozenia zlecenia inwetaryzacji i modyfiakcja bierzacego zlecenia inwentaryzacji
          begin   */
            -- anulujemy wszystkie niewydane pozycje zlecen wydan z tej lokacji (oprocz pozycji bierzacego zlecenia)
            update mwsacts a set a.status = 6, a.operator = :aktuoperator where a.mwsconstlocp = :mwsconstlocref
                and a.status in (1,2) and a.mwsordtypedest in (1,8) and a.mwsord <> :mwsordref;
            update mwsconstlocs m set m.act = 0 where m.ref = :mwsconstlocref;
            select symbol from mwsconstlocs where ref = :mwsconstlocref into :locsymb;
            -- zakladamy zlecenie inwentaryzacji
            execute procedure  XK_MWS_GEN_MWSORD_LOC_STOCK(NULL, NULL, NULL, 'B', null, null, 1, null, null, NULL, :locsymb, NULL)
              returning_values :tmpref;

            -- jesli nic nie wydalismy z bierzacej pozycji zlecenia to ja anulujemy
            if (quantity <= 0) then
              update mwsacts ma set ma.status = 6, ma.operator = :aktuoperator where ma.ref = :lactref;
            -- jesli cos bylo wdane do kopiujemy pozycje, jedna pozycja bedzie oznacozna jako wydana druga jako anulowana
            else if (quantity < lquanc) then begin
              -- dla pozycji ktora potwierdziymy modyfikujemy ilosc do wydania na rowna ilosci potwierdoznej (wydanej)
              update mwsacts ma set ma.status = 0, ma.quantity = :quantity, ma.operator = :aktuoperator where ma.ref = :lactref;
              update mwsacts ma set ma.status = 1, ma.operator = :aktuoperator where ma.ref = :lactref;
              update mwsacts ma set ma.status = 2, ma.quantityc = ma.quantity, ma.operator = :aktuoperator where ma.ref = :lactref;
              lquanc = lquanc - quantity;
              quantity = quantity - quantity;
              select max(ma.number)
                from mwsacts ma
                where ma.mwsord = :mwsordref
                into maxnumber;
              maxnumber = maxnumber + 1;
              -- dodajemy nowa pozycje na niewydana ilosc  (ze statusem anulowana )
              insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
                  mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc,
                  wh, wharea, whsec, timestart, timestop,
                  regtime, timestartdecl, timestopdecl, mwsaccessory, realtimedecl,
                  flags, descript, priority, lot, recdoc, plus,
                  whareal, whareap, wharealogl, wharealogp, number, disttogo, palgroup, autogen,
                  operator, unitp, docgroup, mwsconstloc,   --XXX JO: Dodanie mwsconstloc - wozek szykowania
                  x_partia, x_serial_no, x_slownik)
                select ma.mwsord, 6, 0, ma.good, ma.vers, :lquanc, ma.mwsconstlocp, ma.mwspallocp,
                    ma.mwsconstlocl, ma.mwspallocl, ma.docid, ma.docposid, :doctype, ma.settlmode, ma.closepalloc,
                    ma.wh, ma.wharea,  ma.whsec, current_timestamp, current_timestamp,
                    current_timestamp, ma.timestartdecl, ma.timestopdecl, ma.mwsaccessory, ma.realtimedecl,
                    ma.flags, 'Zgloszona inwentaryzacja', ma.priority, ma.lot, 0, 1,
                    null, ma.whareap, null, ma.wharealogp, :maxnumber, ma.disttogo, ma.palgroup, ma.autogen,
                    :aktuoperator, :unitp, ma.docgroup, ma.mwsconstloc,
                    ma.x_partia, ma.x_serial_no, ma.x_slownik
                  from mwsacts ma where ma.ref = :lactref;
            end else begin
              -- zglosilismy inwentaryzacje ale wydalismy cala potrzebna ilosc, potwierdzamy calasc
              update mwsacts ma set ma.status = 2, ma.quantityc = :lquanc, ma.operator = :aktuoperator where ma.ref = :lactref;
              quantity = quantity - lquanc;
            end
        --  end
        end
      end
      -- nie zgloszono inwentaryzaji i wydano wiecej niz na zleceniu (o ile konfiguracja nam na to pozwala)
      -- ToDo KBI uwzglednic Xowe pola z Ehlrle, puki co nie pozwolic w ehrle wydawac wicej
      if (deficiency = 0 and quantity > 0 and mwsallowmore = 1) then
      begin
        exception universal 'Funkcja wydawania ilosci nadmiarowej nie dostepna dla Ehrle. Skontaktuj sie z pracownikiem SENTE';
        while(:quantity > 0) do begin
          select s.mwspalloc, sum(s.quantity)
            from mwsstock s
            where s.vers = :vers
              and s.mwsconstloc = :mwsconstlocref
              and s.mwspalloc = :mwspallocref
            group by s.mwspalloc
          into :tmpmwspalloc, :tmpquantity;

          if (:tmpmwspalloc is null or coalesce(:tmpquantity,0) = 0) then
            select first 1 s.mwspalloc, s.quantity
              from mwsstock s
              where s.vers = :vers
                and s.mwsconstloc = :mwsconstlocref
              order by s.quantity
              into :tmpmwspalloc, :tmpquantity;
          if (:tmpquantity is null) then tmpquantity = 0;
          if (:tmpquantity > :quantity) then tmpquantity = :quantity;

          quantity = :quantity - :tmpquantity;

          if (:tmpquantity > 0) then begin
            select max(ma.number)
              from mwsacts ma
              where ma.mwsord = :mwsordref
              into maxnumber;
            maxnumber = maxnumber + 1;
            execute procedure gen_ref('MWSACTS') returning_values (:addactref);
            insert into mwsacts (ref ,mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
                mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc,
                wh, wharea, whsec, timestart, timestop,
                regtime, timestartdecl, timestopdecl, mwsaccessory, realtimedecl,
                flags, descript, priority, lot, recdoc, plus,
                whareal, whareap, wharealogl, wharealogp, number, disttogo, palgroup, autogen,
                operator, unitp, docgroup, mwsconstloc)
              select :addactref, ma.mwsord, 1, 0, ma.good, ma.vers, :tmpquantity, ma.mwsconstlocp, :tmpmwspalloc, --ma.mwspallocp,
                  ma.mwsconstlocl, ma.mwspallocl, ma.docid, 0, :doctype, ma.settlmode, ma.closepalloc,
                  ma.wh, ma.wharea,  ma.whsec, current_timestamp, current_timestamp,
                  current_timestamp, ma.timestartdecl, ma.timestopdecl, ma.mwsaccessory, ma.realtimedecl,
                  ma.flags, 'Pobranie nadmiarowe', ma.priority, ma.lot, 0, 1,
                  null, ma.whareap, null, ma.wharealogp, :maxnumber, ma.disttogo, ma.palgroup, ma.autogen,
                  :aktuoperator, :unitp, ma.docgroup, ma.mwsconstloc
                from mwsacts ma where ma.ref = :lactref;
            update mwsacts a set a.status = 2, a.quantityc = a.quantity, a.operator = :aktuoperator where a.ref = :addactref;
          end
          else
            exception universal 'Nie znaleziono lokacji paletowej do rozpisania!';
          tmpmwspalloc = null;
          tmpquantity = null;
        end
      end
    end else
      exception MWS_INWALID_QUAN;
    -- weryfikacja lokacji ale dopiero na potiwerdzenie ostatniej operacji ze zlecenia z tej lokacji
    if (not exists(select ma.ref
          from mwsacts ma
          where ma.mwsord = :mwsordref
            and ma.status <> 6 and ma.quantity <> ma.quantityc
            and ma.mwsconstlocp = :mwsconstlocref)
    ) then
    begin
      update mwsconstlocs c set c.mwsoutactscnt = c.mwsoutactscnt + 1 where c.ref = :mwsconstlocref
        and (c.lastmwsord is null or c.lastmwsord <> :mwsordref);
      execute procedure xk_mws_verify_mwsconstloc(:aktuoperator, mwsconstlocref, :mwsordtype, :wh)
        returning_values (verifymwsord);
    end
         -- XXX KBI ZG122033 aby po rcznym wyborze pozycji do realizacji zrealizwoać wszytkie pozostae pozycje z tej lokalizacji
      else begin
        select first 1 ma.ref
          from mwsacts ma
          where ma.mwsord = :mwsordref
            and ma.status < 3 and ma.status > 0
            and ma.quantity <> ma.quantityc
            and ma.mwsconstlocp = :mwsconstlocref
          order by ma.number
        into :change_act;
        if (change_act is null) then change_act = 0;
      end
    if (verifymwsord is null) then verifymwsord = 0;
  end
 ------------------------------------------------------------------------------------------------------------------------------------------------------
 ------------------------------------------------------------------------------------------------------------------------------------------------------
                                                      -- pobieranie pozycji
 ------------------------------------------------------------------------------------------------------------------------------------------------------
 ------------------------------------------------------------------------------------------------------------------------------------------------------
  rmulti = 0;
  select mo.symbol, mo.description, mo.operator, mo.docgroup, mo.shippingtype, mo.wh, o.grupa,
      mo.multi -- XXX KBI ZG124059
    from mwsords mo
      left join operator o on(mo.operator = o.ref)
    where mo.ref = :mwsordref
      and mo.status < 3
      and mo.status > 0
    into ordsymbol, orddescript, raktuoperator, docgroup, shippingtype, wh, opergrupa,
      :rmulti;   -- XXX KBI ZG124059
  if (rmulti is null) then rmulti = 0;   -- XXX KBI ZG124059
  if (raktuoperator is null) then exception universal 'Nie ustawiony operator!! DZwon do SENTE';
  if (:raktuoperator <> :aktuoperator) then exception universal 'Zlecenie realizuje inny operator. Przerwij realizacje zlecenia!!!'||:raktuoperator;
  if (ordsymbol is null) then
    realstatus = 3;
  else begin
    -- XXX WN START
    select first 1 ma.vers, ma.mwsconstlocp, ma.good, ma.ref, mc.symbol, w.nrwersji, ma.docgroup,
        coalesce(t.kodprod,'')||' '||case when coalesce(trim(t.mwsnazwa),'') = '' then t.nazwa else t.mwsnazwa end||' '||coalesce(t.x_nazwa2,''), w.nazwa,
        iif(df.magbreak = 2,ma.lot,0), iif(df.magbreak = 2,d.symbol,''),
        --mc.x_descript
        ma.x_partia, ma.x_serial_no, ma.x_slownik, w.x_mws_serie, t.kodkresk
      from mwsacts ma
        left join mwsconstlocs mc on(mc.ref = ma.mwsconstlocp)
        left join mwspallocs m on(m.ref = ma.mwspallocp)
        join wersje w on(w.ref = ma.vers)
        join towary t on(t.ktm = ma.good)
        left join defmagaz df on(ma.wh = df.symbol)
        left join dostawy d on(ma.lot = d.ref)
      where ma.mwsord = :mwsordref
        and ma.status < 3 and ma.status > 0
        and (:change_act = 0 or ma.ref = :change_act)
      order by ma.number
      into :rvers, :rmwsconstlocref, :good, :rmwsactref, :mwsconstloc, :nrwersji, :posdocgroup,
        :name, :versname, :rlot, :rlotsymb,
        --:xmwsconstlocdesc;
        :rpartiastr, :rserial, :rslownik, :rtypserialno, :kod_kresk;

      -- Ldz XXX wyswietlanie na HH ile jest na stanie
      select sum(m.quantity)
        from mwsstock m
        join mwsconstlocs mm on (mm.ref = m.mwsconstloc)
        where mm.symbol = :mwsconstloc
          and m.good = :good
          and m.vers = :rvers
      into :on_location;
      --<-- Ldz

      x_partia = null;
      if (coalesce(rpartiastr,'') <> '') then
        x_partia = cast(rpartiastr as date_ID);

      if (coalesce(rserial,0) <> 0) then
        select s.serialno from x_mws_serie s where s.ref = :rserial into :rserialstr;

      rmwspallocref = 0;
      --XXX JO:
      if (:xmwsconstlocdesc is null) then xmwsconstlocdesc = '';
      --- XXX WN STOP
      -- pobieramy uwagi do pozycji zlecenia ktrore zgrupujemy
      if (rvers is not null) then begin
        for
          select distinct descript
            from mwsacts
            where mwsord = :mwsordref
              and mwsconstlocp = :rmwsconstlocref
              and vers = :vers
              and docgroup = :posdocgroup
              and (mwspallocp = :rmwspallocref or :rmwspallocref = 0)
              and (lot = :rlot or :rlot = 0)
              and (x_partia = coalesce(:x_partia,'1900-01-01') or coalesce(:x_partia,'1900-01-01') = '1900-01-01')
              and (x_serial_no = coalesce(:rserial,0) or coalesce(:rserial,0) = 0)
              and (x_slownik = coalesce(:rslownik,0) or coalesce(:rslownik,0) = 0)
              and descript <> ''
            into :lactdescript
        do begin
          actdescript = actdescript||lactdescript||', ';
        end
        -- pobieramy zgrupowana ilosc
        select sum(ma.quantity), sum(ma.quantityc)
          from mwsacts ma
          where ma.mwsord = :mwsordref
            and ma.mwsconstlocp = :rmwsconstlocref
            and ma.vers = :rvers
            and (:posdocgroup is null or (:posdocgroup is not null and docgroup = :posdocgroup))
            and (ma.mwspallocp = :rmwspallocref or :rmwspallocref = 0)
            and (lot = :rlot or :rlot = 0)
            and (x_partia = coalesce(:x_partia,'1900-01-01') or coalesce(:x_partia,'1900-01-01') = '1900-01-01')
            and (x_serial_no = coalesce(:rserial,0) or coalesce(:rserial,0) = 0)
            and (x_slownik = coalesce(:rslownik,0) or coalesce(:rslownik,0) = 0)
            and ma.status > 0 and ma.status < 3
          into :rquantity, rquantityc;
        select count(ma.ref)
          from mwsacts ma
          where ma.mwsord = :mwsordref
            and ma.status < 6 and ma.status > 0
          into :allacts;
        select count(ma.ref)
          from mwsacts ma
          where ma.mwsord = :mwsordref
            and ma.status = 5
          into :actsreal;
        actsreal = actsreal + 1;
        prepared = actsreal||'\'||allacts;
        select sum(ma.weight)
          from mwsacts ma
          where ma.mwsord = :mwsordref
            and ma.status < 5 and ma.status > 0
          into :weightleft;
        if (weightleft is null) then weightleft = 0;
        select QUANTITYSTRING from xk_mws_przelicz_jednostki(:good,:rvers,:rquantity,1)
          into :quantitystring;
          --MSt: Zmiany dla ArgoCard: zeby nie pobieralo kolejnej pozycji do realizacji
  --      update mwsacts ma set ma.status = 2, ma.operator = :aktuoperator
  --        where ma.mwsord = :mwsordref
  --        and ma.mwsconstlocp = :rmwsconstlocref
  --        and ma.vers = :rvers and ma.status in(1,2)
  --        and (ma.mwspallocp = :rmwspallocref or :rmwspallocref = 0) and (ma.lot = :rlot or :rlot = 0);
        realstatus = 0;
      end else begin
        realstatus = 1;
    end
  end

  select first 1 c.cval --XXX JO: first 1 dla multi = 2 (pojedynki)
    from mwsacts a
      left join mwsordcartcolours oc on (a.mwsconstloc = oc.cart and a.mwsord = oc.mwsord)
      left join mwscartcolours c on (oc.mwscartcolor = c.ref)
    where a.ref = :rmwsactref
  into :cartcolor;
  if (:cartcolor is null or (select multi from mwsords where ref = :mwsordref) in (0,2)) then
  begin
    cartcolor = -1; --taka wartosc poniewaz -1 daje bialy
  end
  --MSt: Zmiany dla ArgoCard
  if (:ordstatus = 1) then
    update mwsords set status = 2 where ref = :mwsordref;
  suspend;
end^
SET TERM ; ^
