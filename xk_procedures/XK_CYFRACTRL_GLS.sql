--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_CYFRACTRL_GLS(
      WARTOSC varchar(255) CHARACTER SET UTF8                           )
  returns (
      CYFRACTRL integer)
   as
declare variable poz integer;
declare variable suma integer;
declare variable parz smallint;
declare variable tmpint integer;
begin
  poz = 0;
  suma = 0;
  parz = 1;
  tmpint = 0;
  while(coalesce(char_length(:wartosc),0)>:poz) do begin -- [DG] XXX ZG119346
    tmpint = cast(substring(:wartosc from (coalesce(char_length(:wartosc),0)-:poz) for (coalesce(char_length(:wartosc),0)-:poz)) as integer); -- [DG] XXX ZG119346
    if (parz = 0) then begin
      suma = :suma + (:tmpint * 1);
      parz = 1;
    end
    else begin
      suma = :suma + (:tmpint * 3);
      parz = 0;
    end
      poz = :poz + 1;
  end
  cyfractrl = 10 - (mod((:suma+1), 10));
  if (cyfractrl = 10) then cyfractrl = 0;
  suspend;
end^
SET TERM ; ^
