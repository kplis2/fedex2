--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MOVEGOODS_STOCK(
      MWSCONSTLOC varchar(20) CHARACTER SET UTF8                           ,
      MAGBREAK smallint)
  returns (
      GOOD STRING40,
      VERS INTEGER_ID,
      NAME STRING255,
      NAZWAW STRING255,
      REF INTEGER_ID,
      SYMBOL STRING60,
      QUANTITY numeric(15,4),
      ALLQUANTITY numeric(15,4),
      BLOCKED numeric(15,4),
      ORDERED numeric(15,4),
      KODKRESK STRING1024_UTF8,
      X_PARTIA STRING20,
      X_SERIAL_NO INTEGER_ID,
      X_SLOWNIK INTEGER_ID,
      X_SERIAL_STR STRING,
      X_SLOWNIK_STR STRING)
   as
declare variable SQL varchar(1024);
begin
---XXX LDz stany kartonów i wózków na lokacjach (do ich przesuwania)
  GOOD = '';
  VERS = 0;
  NAME = '';
  NAZWAW = '';
  REF = 0;
  SYMBOL = '';
  QUANTITY = 0;
  ALLQUANTITY = 0;
  BLOCKED = 0;
  ORDERED = 0;
  KODKRESK = 0;
  X_PARTIA = '';
  X_SERIAL_NO = 0;
  X_SLOWNIK = 0;
  X_SERIAL_STR = '';
  X_SLOWNIK_STR = '';
-- szukanie kartonów na lokacji
  for
    select 'Karton: '||l.stanowisko||', Dokument: '||d.symbol, '#BOX#', l.ref
      from listywysdroz_opk l
      join listywysd d on (l.listwysd = d.ref)
      join mwsconstlocs m on (m.symbol = l.x_mwsconstlock)
      where m.symbol = :mwsconstloc
    into :name, :kodkresk, :ref
   do begin
     suspend;
   end
-- szukanie wózków na lokacji
  for
    select first 1 'Wózek: '||mwc.name||', Dokument: '||mo.symbol, '#CART#', mw.cart
      from mwsordcartcolours mw
      join mwsords mo on (mo.docid = mw.docid)
      join mwscartcolours mwc on (mwc.ref = mw.mwscartcolor)
      where mw.mwsconstlocsymb = :mwsconstloc  and mw.status <2
    into :name, :kodkresk, :ref
   do begin
     suspend;
   end
-- XXX KBI DOdanie xowych pól
  sql = '';
  sql = sql || 'select coalesce(t.mwsnazwa, t.nazwa) as name, w.nazwa as nazwaw, s.good, s.vers, ';
  sql = sql || '    sum(s.quantity - s.blocked), sum(s.quantity), sum(s.blocked), sum(s.ordered), ';
  sql = sql || '  s.x_partia, s.x_serial_no, s.x_slownik ';
  if (:magbreak = 2) then
    sql = sql || ', d.ref, d.symbol ';
  else
    sql = sql || ', 0, max(d.symbol) ';
  sql = sql || '  from mwsconstlocs m ';
  sql = sql || '    join mwsstock s on (s.mwsconstloc = m.ref) ';
  sql = sql || '    left join towary t on (s.good = t.ktm) ';
  sql = sql || '    left join dostawy d on (s.lot = d.ref) ';
  sql = sql || '    left join wersje w on (s.vers = w.ref) ';
  sql = sql || '  where m.symbol = ''' || :mwsconstloc || ''' ';
  sql = sql || '    and s.quantity - s.blocked >= 0 ';
  sql = sql || '  group by coalesce(t.mwsnazwa, t.nazwa), w.nazwa, s.good, s.vers, S.X_PARTIA, S.X_SERIAL_NO, S.X_SLOWNIK ';
  if (:magbreak = 2) then
    sql = sql || ', d.ref, d.symbol ';
  for execute statement :sql
  into :name, :nazwaw, :good, :vers,
    :quantity, :allquantity, :blocked, :ordered,
    :x_partia, :x_serial_no, :x_slownik,
    :ref, :symbol

  do begin
    kodkresk = '';
    select list(k.kodkresk,',')
      from towkodkresk k
      where k.wersjaref = :vers
      into :kodkresk;

    if (coalesce(x_serial_no,0) <> 0) then
      select s.serialno from X_MWS_SERIE s where s.ref = :x_serial_no into :x_serial_str;

    suspend;
    x_serial_str = null;
  end
end^
SET TERM ; ^
