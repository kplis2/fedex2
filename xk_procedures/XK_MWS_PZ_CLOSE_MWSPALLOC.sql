--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_PZ_CLOSE_MWSPALLOC(
      MWSORD integer,
      MWSPALLOC integer,
      STATUS integer)
   as
declare variable DOCID integer;
declare variable MWSACT MWSACTS_ID;
declare variable REFMWSORD MWSORDS_ID;
begin
  update mwspallocs m set m.status = :status where m.ref = :mwspalloc;
  update mwsacts ma set ma.quantityc = ma.quantity where ma.mwspallocl = :mwspalloc and ma.status in(1,2);
/*
  select docid from mwsords where ref = :mwsord
    into docid;
  if (docid is not null) then
  begin

  end else
  begin
    select first 1 ma.ref
      from mwsacts ma
      where ma.mwspallocl = :mwspalloc
      into :mwsact;
    execute procedure MWS_CREATE_PROD_FROM_MWSORD(null,null,null,'M',:mwsord, :mwsact,0,null)
      returning_values :REFMWSORD;
  end
  suspend;
*/
end^
SET TERM ; ^
