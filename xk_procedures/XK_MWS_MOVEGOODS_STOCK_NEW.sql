--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MOVEGOODS_STOCK_NEW(
      MWSCONSTLOC STRING20,
      FILTR STRING40 = '')
  returns (
      GOOD STRING40,
      VERS INTEGER_ID,
      NAME STRING255,
      NAZWAW STRING255,
      REF INTEGER_ID,
      SYMBOL STRING60,
      QUANTITY numeric(15,4),
      ALLQUANTITY numeric(15,4),
      BLOCKED numeric(15,4),
      ORDERED numeric(15,4),
      KODKRESK STRING1024_UTF8,
      PARTIA STRING20,
      SERIAL STRING,
      SLOWNIK STRING,
      ILOSC_LOC NUMERIC_14_4,
      ILOSC_BLOC NUMERIC_14_4)
   as
declare variable SQL varchar(1024);
declare variable x_serial_no integer_id;
declare variable x_slownik integer_id; 
declare variable myslnik smallint_id;
begin

  myslnik = position('-', :FILTR);
  if (myslnik > 0) then begin --sprawdzamy czy mamy myslnik
    FILTR = right(FILTR, CHAR_LENGTH(FILTR) - myslnik); --obcinamy od myslnika do konca

    if (CHAR_LENGTH(FILTR)=7 and left(FILTR, 2) = '54') then --jesli skrocony(7) i poczatek 54
      FILTR = '5400000' || right(FILTR, 5); --dodaj 5400000 i 5 ostatnich znakow
  end

--exception universal mwsconstloc ||' dd'||filtr;
---XXX LDz stany kartonów i wózków na lokacjach (do ich przesuwania)
  GOOD = '';
  VERS = 0;
  NAME = '';
  NAZWAW = '';
  REF = 0;
  SYMBOL = '';
  QUANTITY = 0;
  ALLQUANTITY = 0;
  BLOCKED = 0;
  ORDERED = 0;
  KODKRESK = 0;
  PARTIA = '';
  SERIAL = '';
  SLOWNIK = '';
  ilosc_loc = 0;
  ilosc_bloc = 0;
-- szukanie kartonów na lokacji
  for
    select 'Karton: '||l.stanowisko||', Dokument: '||d.symbol, '#BOX#', l.ref
      from listywysdroz_opk l
      join listywysd d on (l.listwysd = d.ref)
      join mwsconstlocs m on (m.symbol = l.x_mwsconstlock)
      where m.symbol = :mwsconstloc
    into :name, :kodkresk, :ref
   do begin
     suspend;
   end
-- szukanie wózków na lokacji
  for
    select first 1 'Wózek: '||mwc.name||', Dokument: '||mo.symbol, '#CART#', mw.cart
      from mwsordcartcolours mw
      join mwsords mo on (mo.docid = mw.docid)
      join mwscartcolours mwc on (mwc.ref = mw.mwscartcolor)
      where mw.mwsconstlocsymb = :mwsconstloc and mw.status<2
    into :name, :kodkresk, :ref
  do begin
     suspend;
  end

    for
      select coalesce(t.mwsnazwa, t.nazwa) as name, w.nazwa as nazwaw, s.good, s.vers, sum(s.quantity - s.blocked), sum(s.quantity),
        sum(s.blocked), sum(s.ordered), s.x_partia, s.x_serial_no, s.x_slownik, 0, max(d.symbol), k.kodkresk
      from mwsconstlocs m
        join mwsstock s on (s.mwsconstloc = m.ref)
        left join towary t on (s.good = t.ktm)
        left join dostawy d on (s.lot = d.ref)
        left join wersje w on (s.vers = w.ref)
        left join towkodkresk k on (s.vers = k.wersjaref)
      where m.symbol = :mwsconstloc
        and s.quantity - s.blocked >= 0
        and coalesce(t.ktm, '' )||' '||
            coalesce(t.nazwa,  '')||' '||
            coalesce(s.vers,  '')||' '||
            coalesce(t.kodprod,  '')||' '||
            coalesce(t.x_kod_towaru_ses,  '')||' '||
            coalesce(k.kodkresk,'')
            similar to ('%'||:filtr||'%')
      group by coalesce(t.mwsnazwa, t.nazwa), w.nazwa, s.good, s.vers, S.X_PARTIA, S.X_SERIAL_NO, S.X_SLOWNIK, k.kodkresk
      into :name, :nazwaw, :good, :vers, :quantity, :allquantity,
        :blocked, :ordered, :partia, :x_serial_no, :x_slownik, :ref, :symbol, :kodkresk
    do begin
-- info ile na lokacji, zablokowane
      select sum(ms.quantity), sum(ms.blocked)
        from mwsstock ms
          join mwsconstlocs mm on (ms.mwsconstloc = mm.ref)
        where  mm.symbol = :mwsconstloc
          and ms.vers = :vers
      into :ilosc_loc, :ilosc_bloc;

      if (ilosc_loc is null) then ilosc_loc = 0;
      if (ilosc_bloc is null) then ilosc_bloc = 0;

      if (coalesce(x_serial_no,0) <> 0) then
        select s.serialno from X_MWS_SERIE s where s.ref = :x_serial_no into :serial;

      if (coalesce(x_slownik,0) <> 0) then
        select x.wartosc from X_SLOWNIKI_EHRLE x where x.ref = :x_slownik into :slownik;

      if (partia is null) then partia = '';

      suspend;
      serial = '';
      slownik = '';
      kodkresk = '';
      ilosc_loc = 0;
      ilosc_bloc = 0;
    end

end^
SET TERM ; ^
