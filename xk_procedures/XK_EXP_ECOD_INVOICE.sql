--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_EXP_ECOD_INVOICE(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable symbol varchar(255);
declare variable data date;
declare variable datasprz date;
declare variable waluta varchar(10);
declare variable termzap date;
declare variable termin integer;
declare variable korekta smallint;
declare variable uwagi varchar(2048);
declare variable grupakup integer;
declare variable numer integer;
declare variable refwersji integer;
declare variable jednostka integer;
declare variable ktm varchar(255);
declare variable nazwa varchar(255);
declare variable opk integer;
declare variable ilosc numeric(14,3);
declare variable pilosc numeric(14,3);
declare variable miara varchar(5);
declare variable cenanet numeric(14,2);
declare variable pcenanet numeric(14,2);
declare variable stawka numeric(14,2);
declare variable pstawka numeric(14,2);
declare variable stawkaedi varchar(10);
declare variable pstawkaedi varchar(10);
declare variable kodkresk varchar(255);
declare variable kodtowaru varchar(255);
declare variable haspkwiu smallint;
declare variable pkwiu varchar(255);
declare variable wartbru numeric(14,2);
declare variable pwartbru numeric(14,2);
declare variable wartnet numeric(14,2);
declare variable pwartnet numeric(14,2);
declare variable zamid varchar(255);
declare variable zamznakzewn varchar(255);
declare variable zamdatazewn date;
declare variable nrdoksped varchar(255);
declare variable odbiorcagln numeric(13,0);
declare variable datawysylki date;
declare variable nrlistuprzewozowego varchar(255);
declare variable pozfakref integer;
declare variable fromdokumpoz integer;
declare variable klientgln numeric(13,0);
declare variable klientnip varchar(255);
declare variable klientrachunek varchar(255);
declare variable klientnazwa varchar(255);
declare variable klientulica varchar(255);
declare variable klientnrdomu nrdomu_id;
declare variable klientnrlokalu nrdomu_id;
declare variable klientmiasto varchar(255);
declare variable klientkodp varchar(255);
declare variable klientkraj varchar(255);
declare variable platnikgln numeric(13,0);
declare variable platniknip varchar(255);
declare variable platnikrachunek varchar(255);
declare variable platniknazwa varchar(255);
declare variable platnikulica varchar(255);
declare variable platnikmiasto varchar(255);
declare variable platnikkodp varchar(255);
declare variable platnikkraj varchar(255);
declare variable info1 varchar(255);
declare variable info2 varchar(255);
declare variable info3 varchar(255);
declare variable totallines integer;
declare variable sumwartnet numeric(14,2);
declare variable sumwartbru numeric(14,2);
declare variable psumwartnet numeric(14,2);
declare variable psumwartbru numeric(14,2);
declare variable sumkaucja numeric(14,2);
declare variable psumkaucja numeric(14,2);
declare variable sumwartvat numeric(14,2);
declare variable psumwartvat numeric(14,2);
declare variable intrdostawy varchar(255);
declare variable intrtransport varchar(255);
declare variable odbiorcanazwa varchar(255);
declare variable odbiorcaulica varchar(255);
declare variable odbiorcakodp varchar(255);
declare variable odbiorcamiasto varchar(255);
declare variable odbiorcakraj varchar(255);
declare variable dokumpozref integer;

declare variable document_invoice integer;
declare variable invoice_header integer;
declare variable invoice_parties integer;
declare variable invoice_lines integer;
declare variable line integer;
declare variable line_item integer;
declare variable taxreference integer;
declare variable invoice_summary integer;
declare variable line_order integer;
declare variable line_delivery integer;
declare variable buyer integer;
declare variable payer integer;
declare variable seller integer;
declare variable tax_summary integer;
declare variable tax_summary_line integer;
declare variable document_despatch integer;
declare variable despatch_header integer;
declare variable despatch_transport integer;
declare variable despatch_parties integer;
declare variable delivery integer;
declare variable despatch_consigment integer;
declare variable packing_sequence integer ;
begin
    -- eksport faktury
    select n.symbol,n.data,n.datasprz,n.waluta,n.termzap,n.termin,
      n.korekta,n.uwagi,n.sumwartnet,n.sumwartbru,n.psumwartnet,n.psumwartbru,n.sumkaucja,n.psumkaucja,
      k.grupakup, k.gln, k.prostynip, k.rachunek, k.nazwa, k.ulica, k.nrdomu, k.nrlokalu, k.miasto, k.kodp, k.krajid,
      p.gln, p.prostynip, p.rachunek, p.nazwa, p.ulica, p.miasto, p.kodp, p.krajid
    from nagfak n
    left join klienci k on k.ref=n.klient
    left join klienci p on p.ref=n.platnik
    where n.ref=:oref
    into :symbol,:data,:datasprz,:waluta,:termzap,:termin,
      :korekta,:uwagi,:sumwartnet,:sumwartbru,:psumwartnet,:psumwartbru,:sumkaucja,:psumkaucja,
      :grupakup, :klientgln, :klientnip, :klientrachunek, :klientnazwa, :klientulica, :klientnrdomu, :klientnrlokalu, :klientmiasto, :klientkodp, :klientkraj,
      :platnikgln, :platniknip, :platnikrachunek, :platniknazwa, :platnikulica, :platnikmiasto, :platnikkodp, :platnikkraj;
    id = 1;
    document_invoice = :id;
    parent = NULL;
    name = 'Document-Invoice';
    params = '';
    val = '';
    suspend;
    parent = :document_invoice;
    id = id + 1;
    invoice_header = :id;
    name = 'Invoice-Header';
    suspend;

-- invoice header
    parent = :invoice_header;
    id = id + 1;
    name = 'InvoiceNumber';
    val = :symbol;
    suspend;
    id = id + 1;
    name = 'InvoiceDate';
    val = :data;
    suspend;
    id = id + 1;
    name = 'SalesDate';
    val = :datasprz;
    suspend;
    id = id + 1;
    name = 'InvoiceCurrency';
    val = :waluta;
    suspend;
    id = id + 1;
    name = 'InvoicePaymentDueDate';
    val = :termzap;
    suspend;
    id = id + 1;
    name = 'InvoicePaymentTerms';
    val = :termin;
    suspend;
    id = id + 1;
    name = 'DocumentFunctionCode';
    if(:korekta=1) then val = 'C'; else val = 'O';
    suspend;
    id = id + 1;
    name = 'Remarks';
    val = :uwagi;
    suspend;

-- order (zamowienie)
-- nie wystepuje, gdyz uzywamy line-order

-- reference (faktura korygowana)
-- nie wystepuje, gdyz uzywamy line-reference

-- delivery (odbiorca)
-- nie wystepuje, gdyz uzywamy line-delivery

-- invoice parties
    parent = :document_invoice;
    id = id + 1;
    invoice_parties = :id;
    name = 'Invoice-Parties';
    val = '';
    suspend;

-- buyer (kupujacy)
    parent = :invoice_parties;
    id = id + 1;
    buyer = :id;
    name = 'Buyer';
    val = '';
    suspend;
    parent = :buyer;
    id = id + 1;
    name = 'ILN';
    val = :klientgln;
    suspend;
    id = id + 1;
    name = 'TaxID';
    val = :klientnip;
    suspend;
    id = id + 1;
    name = 'AccountNumber';
    val = :klientrachunek;
    suspend;
    id = id + 1;
    name = 'Name';
    val = :klientnazwa;
    suspend;
    id = id + 1;
    name = 'StreetAndNumber';
    val = :klientulica||iif(coalesce(:klientnrdomu,'')<>'',' '||:klientnrdomu,'')||iif(coalesce(:klientnrlokalu,'')<>'','/'||:klientnrlokalu,'');
    suspend;
    id = id + 1;
    name = 'CityName';
    val = :klientmiasto;
    suspend;
    id = id + 1;
    name = 'PostalCode';
    val = :klientkodp;
    suspend;
    id = id + 1;
    name = 'Country';
    val = :klientkraj;
    suspend;

-- payer (platnik)
    if(:platniknip<>:klientnip) then begin
      parent = :invoice_parties;
      id = id + 1;
      payer = :id;
      name = 'Payer';
      val = '';
      suspend;
      parent = :payer;
      id = id + 1;
      name = 'ILN';
      val = :platnikgln;
      suspend;
      id = id + 1;
      name = 'TaxID';
      val = :platniknip;
      suspend;
      id = id + 1;
      name = 'AccountNumber';
      val = :platnikrachunek;
      suspend;
      id = id + 1;
      name = 'Name';
      val = :platniknazwa;
      suspend;
      id = id + 1;
      name = 'StreetAndNumber';
      val = :platnikulica;
      suspend;
      id = id + 1;
      name = 'CityName';
      val = :platnikmiasto;
      suspend;
      id = id + 1;
      name = 'PostalCode';
      val = :platnikkodp;
      suspend;
      id = id + 1;
      name = 'Country';
      val = :platnikkraj;
      suspend;
    end

-- seller (wystawiajacy fakture)
    parent = :invoice_parties;
    id = id + 1;
    seller = :id;
    name = 'Seller';
    val = '';
    suspend;
    parent = :seller;
    id = id + 1;
    name = 'ILN';
    select wartosc from getconfig('INFOGLN') into :val;
    suspend;
    id = id + 1;
    name = 'TaxID';
    select wartosc from getconfig('INFONIP') into :val;
    suspend;
    id = id + 1;
    name = 'AccountNumber';
    select bankaccount from XK_GET_BANKACCOUNT(:oref,:otable) into :val;
    suspend;
    id = id + 1;
    name = 'Name';
    select wartosc from getconfig('INFO1') into :val;
    suspend;
    id = id + 1;
    name = 'StreetAndNumber';
    select wartosc from getconfig('INFO2') into :val;
    suspend;
    id = id + 1;
    name = 'CityName';
    select substring(wartosc from 8) from getconfig('INFO3') into :val;
    suspend;
    id = id + 1;
    name = 'PostalCode';
    select substring(wartosc from 1 for 6) from getconfig('INFO3') into :val;
    suspend;
    id = id + 1;
    name = 'Country';
    val = 'PL';
    suspend;
    id = id + 1;
    name = 'CourtAndCapitalInformation';
    select wartosc from getconfig('INFOREJ1') into :info1;
    select wartosc from getconfig('INFOREJ2') into :info2;
    select wartosc from getconfig('INFOREJ3') into :info3;
    val = :info1||'  KRS: '||:info2||'  Wysokosc kapitalu zakladowego: '||:info3;
    suspend;

-- invoice lines
    parent = :document_invoice;
    id = id + 1;
    invoice_lines = :id;
    name = 'Invoice-Lines';
    val = '';
    suspend;

    totallines = 0;
    for select p.ref, p.numer, p.wersjaref, p.jedn, p.ktm, p.nazwat, p.opk, p.ilosc, m.editype, p.cenanet,
      v.stawka, v.editypeecod, v.haspkwiu, p.pkwiu, p.wartnet, p.wartbru,
      p.pilosc, p.pcenanet, pv.stawka, pv.editypeecod, p.pwartnet, p.pwartbru
    from pozfak p
    left join towjedn j on j.ref=p.jedn
    left join miara m on m.miara=j.jedn
    left join vat v on v.grupa=p.gr_vat
    left join vat pv on pv.grupa=p.pgr_vat
    where p.dokument=:oref
    order by p.numer
    into :pozfakref, :numer, :refwersji, :jednostka, :ktm, :nazwa, :opk, :ilosc, :miara, :cenanet,
      :stawka, :stawkaedi, :haspkwiu, :pkwiu, :wartnet, :wartbru,
      :pilosc, :pcenanet, :pstawka, :pstawkaedi, :pwartnet, :pwartbru
    do begin
      select symbol from XK_GET_KODKRESK(:GRUPAKUP,:REFWERSJI,:JEDNOSTKA) into :kodkresk;
      select symbol from XK_GET_KTMTOGRUPYKUP(:GRUPAKUP,:REFWERSJI,:JEDNOSTKA) into :kodtowaru;
      if(:korekta=1) then begin -- przesun sie do pozfaka korygowanego
        select pozfak from POZFAK_GET_FIRSTPOZ(:pozfakref) into :pozfakref;
      end
      select nz.id, nz.znakzewn, nz.datazewn
      from pozfak p
      left join pozzam pz on pz.ref=p.frompozzam
      left join nagzam nz on nz.ref=pz.zamowienie
      where p.ref=:pozfakref
      into :zamid, :zamznakzewn, :zamdatazewn;

      odbiorcagln = NULL;
      datawysylki = NULL;
      nrlistuprzewozowego = NULL;
      nrdoksped = NULL;
      -- znajdz pozycje dok. mag. powiazana z biezaca pozycja faktury
      select p.fromdokumpoz from pozfak p where p.ref=:pozfakref into :fromdokumpoz;
      if(:fromdokumpoz is null) then select first 1 ref from dokumpoz p where p.frompozfak=:pozfakref into :fromdokumpoz;
      if(:fromdokumpoz is not null) then begin
        -- poszukaj dokumentu spedycyjnego do danej pozycji
        select l.symbol, o.gln, l.symbolsped, l.datazam
        from dokumpoz p
        left join dokumnag n on n.ref=p.dokument
        left join odbiorcy o on o.ref=n.odbiorcaid
        left join listywysd l on l.ref=n.wysylka
        where p.ref=:fromdokumpoz
        into :nrdoksped, :odbiorcagln, :nrlistuprzewozowego, :datawysylki;
        if(:nrdoksped is null) then begin
          -- jesli nie ma dok.sped. to pobierz symbok dok. mag.
          select n.symbol
          from dokumpoz p
          left join dokumnag n on n.ref=p.dokument
          where p.ref=:fromdokumpoz
          into :nrdoksped;
        end
      end
      if(:nrdoksped is null) then nrdoksped = '';
      if(:odbiorcagln is null) then odbiorcagln = :klientgln;
      if(:nrlistuprzewozowego is null) then nrlistuprzewozowego = :nrdoksped;
      if(:datawysylki is null) then datawysylki = :datasprz;

-- line (pozycja faktury)
      parent = :invoice_lines;
      id = id + 1;
      line = :id;
      name = 'Line';
      val = '';
      suspend;

-- line item (dane z pozycji faktury)
      parent = :line;
      id = id + 1;
      line_item = :id;
      name = 'Line-Item';
      val = '';
      suspend;

      parent = :line_item;
      id = id + 1;
      name = 'LineNumber';
      val = :numer;
      suspend;
      id = id + 1;
      name = 'EAN';
      val = :kodkresk;
      suspend;
      if(:kodtowaru<>'') then begin
        id = id + 1;
        name = 'BuyerItemCode';
        val = :kodtowaru;
        suspend;
      end
      id = id + 1;
      name = 'SupplierItemCode';
      val = :ktm;
      suspend;
      id = id + 1;
      name = 'ItemDescription';
      val = :nazwa;
      suspend;
      id = id + 1;
      name = 'ItemType';
      if(:opk=1) then val = 'RC'; else val = 'CU';
      suspend;
      id = id + 1;
      name = 'InvoiceQuantity';
      val = :ilosc;
      suspend;
      id = id + 1;
      name = 'UnitOfMeasure';
      val = :miara;
      suspend;
      id = id + 1;
      name = 'InvoiceUnitNetPrice';
      val = :cenanet;
      suspend;
      id = id + 1;
      name = 'TaxRate';
      val = :stawka;
      suspend;
      id = id + 1;
      name = 'TaxCategoryCode';
      val = :stawkaedi;
      suspend;
      if(:haspkwiu=1) then begin
        id = id + 1;
        taxreference = :id;
        name = 'TaxReference';
        val = '';
        suspend;
        parent = :taxreference;
        id = id + 1;
        name = 'ReferenceType';
        val = 'PKWIU';
        suspend;
        id = id + 1;
        name = 'ReferenceNumber';
        val = :pkwiu;
        suspend;
      end
      parent = :line_item;
      id = id + 1;
      name = 'TaxAmount';
      val = :wartbru-:wartnet;
      suspend;
      id = id + 1;
      if(:opk=0) then name = 'NetAmount'; else name = 'DepositAmount';
      val = :wartnet;
      suspend;
      if(:korekta=1) then begin
        id = id + 1;
        name = 'PreviousInvoiceQuantity';
        val = :pilosc;
        suspend;
        id = id + 1;
        name = 'PreviousInvoiceUnitNetPrice';
        val = :pcenanet;
        suspend;
        id = id + 1;
        name = 'PreviousTaxRate';
        val = :pstawka;
        suspend;
        id = id + 1;
        name = 'PreviousTaxCategoryCode';
        val = :pstawkaedi;
        suspend;
        id = id + 1;
        name = 'PreviousTaxAmount';
        val = :pwartbru-:pwartnet;
        suspend;
        id = id + 1;
        if(:opk=0) then name = 'PreviousNetAmount'; else name = 'PreviousDepositAmount';
        val = :pwartnet;
        suspend;
        id = id + 1;
        name = 'CorrectionInvoiceQuantity';
        val = :ilosc - :pilosc;
        suspend;
        id = id + 1;
        name = 'CorrectionInvoiceUnitNetPrice';
        val = :cenanet - :pcenanet;
        suspend;
        id = id + 1;
        name = 'CorrectionTaxAmount';
        val = (:wartbru - :wartnet) - (:pwartbru - :pwartnet);
        suspend;
        id = id + 1;
        if(:opk=0) then name = 'CorrectionNetAmount'; else name = 'CorrectionDepositAmount';
        val = :wartnet - :pwartnet;
        suspend;
      end

-- line-order
    if(:zamid is not null) then begin
      parent = :line;
      id = id + 1;
      line_order = :id;
      name = 'Line-Order';
      val = '';
      suspend;
      id = id + 1;
      parent = :line_order;
      name = 'BuyerOrderNumber';
      val = :zamznakzewn;
      suspend;
      id = id + 1;
      name = 'SupplierOrderNumber';
      val = :zamid;
      suspend;
      id = id + 1;
      name = 'BuyerOrderDate';
      val = :zamdatazewn;
      suspend;
    end


-- line-delivery
      parent = :line;
      id = id + 1;
      line_delivery = :id;
      name = 'Line-Delivery';
      val = '';
      suspend;
      id = id + 1;
      parent = :line_delivery;
      name = 'DeliveryLocationNumber';
      val = :odbiorcagln;
      suspend;
      id = id + 1;
      name = 'DeliveryDate';
      val = :datawysylki;
      suspend;
      id = id + 1;
      name = 'DespatchNumber';
      val = :nrlistuprzewozowego;
      suspend;
      id = id + 1;
      name = 'DespatchAdviceNumber';
      val = :nrdoksped;
      suspend;

-- koniec linii
    totallines = :totallines + 1;
    end

-- invoice summary
    parent = :document_invoice;
    id = id + 1;
    invoice_summary = :id;
    name = 'Invoice-Summary';
    val = '';
    suspend;
    parent = :invoice_summary;
    id = id + 1;
    name = 'TotalLines';
    val = :totallines;
    suspend;
    id = id + 1;
    name = 'TotalNetAmount';
    val = :sumwartnet;
    suspend;
    id = id + 1;
    name = 'TotalTaxableBasis';
    val = :sumwartnet;
    suspend;
    id = id + 1;
    name = 'TotalTaxAmount';
    val = :sumwartbru - :sumwartnet;
    suspend;
    id = id + 1;
    name = 'TotalGrossAmount';
    val = :sumwartbru;
    suspend;
    id = id + 1;
    name = 'TotalDepositAmount';
    val = :sumkaucja;
    suspend;
    if(:korekta=1) then begin
      id = id + 1;
      name = 'PreviousTotalNetAmount';
      val = :psumwartnet;
      suspend;
      id = id + 1;
      name = 'PreviousTotalTaxableBasis';
      val = :psumwartnet;
      suspend;
      id = id + 1;
      name = 'PreviousTotalTaxAmount';
      val = :psumwartbru - :psumwartnet;
      suspend;
      id = id + 1;
      name = 'PreviousTotalGrossAmount';
      val = :psumwartbru;
      suspend;
      id = id + 1;
      name = 'PreviousTotalDepositAmount';
      val = :psumkaucja;
      suspend;
      id = id + 1;
      name = 'CorrectionTotalNetAmount';
      val = :sumwartnet - :psumwartnet;
      suspend;
      id = id + 1;
      name = 'CorrectionTotalTaxableBasis';
      val = :sumwartnet - :psumwartnet;
      suspend;
      id = id + 1;
      name = 'CorrectionTotalTaxAmount';
      val = (:sumwartbru - :sumwartnet) - (:psumwartbru - :psumwartnet);
      suspend;
      id = id + 1;
      name = 'CorrectionTotalGrossAmount';
      val = :sumwartbru - :psumwartbru;
      suspend;
      id = id + 1;
      name = 'CorrectionTotalDepositAmount';
      val = :sumkaucja - :psumkaucja;
      suspend;
    end
-- tax summary
    parent = :invoice_summary;
    id = id + 1;
    tax_summary = :id;
    name = 'Tax-Summary';
    val = '';
    suspend;

    for select v.stawka, v.editypeecod, r.sumwartnet, r.sumwartbru, r.sumwartvat, r.psumwartnet, r.psumwartbru, r.psumwartvat
    from rozfak r
    left join vat v on v.grupa=r.vat
    where r.dokument=:oref
    into :stawka, :stawkaedi, :sumwartnet, :sumwartbru, :sumwartvat, :psumwartnet, :psumwartbru, :psumwartvat
    do begin

      parent = :tax_summary;
      id = id + 1;
      tax_summary_line = :id;
      name = 'Tax-Summary-Line';
      val = '';
      suspend;

      parent = :tax_summary_line;
      id = id + 1;
      name = 'TaxRate';
      val = :stawka;
      suspend;
      id = id + 1;
      name = 'TaxCategoryCode';
      val = :stawkaedi;
      suspend;
      id = id + 1;
      name = 'TaxAmount';
      val = :sumwartvat;
      suspend;
      id = id + 1;
      name = 'TaxableBasis';
      val = :sumwartnet;
      suspend;
      id = id + 1;
      name = 'TaxableAmount';
      val = :sumwartnet;
      suspend;
      id = id + 1;
      name = 'GrossAmount';
      val = :sumwartbru;
      suspend;

      if(:korekta=1) then begin
        id = id + 1;
        name = 'PreviousTaxRate';
        val = :stawka;
        suspend;
        id = id + 1;
        name = 'PreviousTaxCategoryCode';
        val = :stawkaedi;
        suspend;
        id = id + 1;
        name = 'PreviousTaxAmount';
        val = :psumwartvat;
        suspend;
        id = id + 1;
        name = 'PreviousTaxableAmount';
        val = :psumwartnet;
        suspend;
        id = id + 1;
        name = 'CorrectionTaxAmount';
        val = :sumwartvat - :psumwartvat;
        suspend;
        id = id + 1;
        name = 'CorrectionTaxableAmount';
        val = :sumwartnet - :psumwartnet;
        suspend;
      end

    end


end^
SET TERM ; ^
