--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_PACLISTDONE(
      LISTYWYSD INTEGER_ID)
  returns (
      KTM KTM_ID,
      ILOSCMAG numeric(14,4),
      PARTIA STRING20,
      SERIA STRING20)
   as
begin
  for
    select lis.KTM, sum(lis.ILOSCMAG), lis.X_MWS_PARTIE as PARTIA, ms.serialno as SERIA
      from LISTYWYSDROZ lis
      left join x_mws_serie ms on (ms.ref = lis.x_mws_serie)
      where lis.listywysd = :listywysd
      group by KTM, PARTIA, SERIA
      --order by lis.REF desc
  into :ktm, :iloscmag, :partia, :seria
  do begin
    if (partia is null) then
      partia ='-';
    if (seria is null) then
      seria ='-';
    suspend;
  end
  partia = '-';
  seria = '-';
  for
    select l.stanowisko, count (*)
      from listywysdroz_opk l
      where l.listwysd = :listywysd
      group by l.stanowisko
    into :ktm,  :iloscmag
  do begin
    suspend;
  end
end^
SET TERM ; ^
