--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MG_ORDLIST(
      AKTUOPERATOR integer,
      LASTORD integer,
      WH varchar(3) CHARACTER SET UTF8                           )
  returns (
      PALGROUP integer,
      LOGIN varchar(20) CHARACTER SET UTF8                           ,
      HERMON STRING255,
      DOSTAWCA STRING40,
      SYMBOL_PZ SYMBOL_ID,
      REF integer,
      MWSORDTYPE varchar(3) CHARACTER SET UTF8                           ,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      MWSFIRSTCONSTLOCP varchar(20) CHARACTER SET UTF8                           ,
      PRIORITY integer,
      DOCSYMBS varchar(60) CHARACTER SET UTF8                           ,
      STATUS smallint,
      MWSFIRSTCONSTLOCPREF integer,
      TIMESTARTDECL timestamp,
      OUTDISTFROMSTARTAREA numeric(15,4),
      MWSORDTYPEDEST smallint,
      DESCRIPTION varchar(1024) CHARACTER SET UTF8                           ,
      FROMMWSACT integer)
   as
declare variable ordref integer;
declare variable lastdistfromstartarea numeric(15,4);
declare variable distfromstartarea numeric(15,4);
declare variable maxh numeric(14,4);
declare variable maxlh numeric(14,4);
declare variable maxph numeric(14,4);
declare variable hstatus smallint;
begin
  outdistfromstartarea = 0;
  if (exists(
      select mo.operator
        from mwsordtypedest4op mo
        where mo.operator = :aktuoperator and mo.mwsordtypedest in (2,0))
  ) then
  begin
    select ms.distfromstartarea
      from mwsconstlocs ms
      where ms.ref = :lastord
      into :lastdistfromstartarea;
    maxh = null;
    select a.lifthight
      from mwsaccessoryhist h
        left join mwsaccessories a on (a.ref = h.mwsaccessory)
      where h.operator = :aktuoperator and h.timestartto is null
      into maxh;
    if (lastdistfromstartarea is null) then lastdistfromstartarea = 0;
    for
      select mo.ref, mo.palgroup, mo.timestartdecl, mo.mwsordtypes, mo.symbol,
              mo.status, mo.mwsfirstconstlocp, mo.mwsfirstconstlocpref, mo.distfromstartarea, mo.docsymbs, mo.priority, mo.mwsordtypedest, coalesce(o.login,''),
              mo.description, mo.frommwsact, coalesce(d.symbol,''), coalesce(d.int_symbol,''), coalesce(mo.slokod,'')
        from mwsords mo
          left join dokumnag d on(d.ref = mo.docid)
          left join operator o on(mo.operator = o.ref)
        where (mo.operator = :aktuoperator or mo.operator is null)
          and mo.wh = :wh
          and mo.status < 3
          and mo.status > 0
          and (mo.mwsordtypedest = 2)
          and (mo.palgroup is null or mo.operator = :aktuoperator)
          and ((d.blokada <> 1 and d.blokada <> 5) or d.blokada is null)
        order by case when mo.operator is not null then 0 else 1 end, mo.priority asc, mo.status, mo.regtime asc
        into :ref, :palgroup, :timestartdecl, :mwsordtype, :symbol, :status,
              :mwsfirstconstlocp, :mwsfirstconstlocpref, :distfromstartarea, :docsymbs, :priority, :mwsordtypedest, :login,
              :description, :frommwsact, :symbol_pz, :hermon, :dostawca
    do begin
      maxph = 0;
      maxlh = 0;
      hstatus = 1;
      outdistfromstartarea = -100;
      if (login is null) then
        outdistfromstartarea = distfromstartarea - lastdistfromstartarea;
      if (mwsordtypedest = 8) then
        outdistfromstartarea = -90;
      else if (mwsordtypedest = 1) then
        outdistfromstartarea = -80;
      else if (mwsordtypedest = 9) then
        outdistfromstartarea = -70;
      if (hstatus = 1) then
        suspend;
    end
  end
end^
SET TERM ; ^
