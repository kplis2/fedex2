--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_SHOW_MWSSTOCK(
      BARCODE varchar(40) CHARACTER SET UTF8                           ,
      WH varchar(3) CHARACTER SET UTF8                           )
  returns (
      MWSCONSTLOC integer,
      MWSCONSTLOCSYMB varchar(120) CHARACTER SET UTF8                           ,
      GOODNAME varchar(255) CHARACTER SET UTF8                           ,
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      LOT integer,
      LOTSYMB varchar(120) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4),
      BLOCKED numeric(14,4),
      ORDERED numeric(14,4),
      X_PARTIA STRING20,
      X_SERIAL_STR STRING,
      X_SLOWNIK_STR STRING)
   as
begin
  good = null;
  vers = null;
  if (:barcode = '') then
    exit;              --Ldz wyswietlalo na hh, znajdz towar pierszy towar z pustym barcodem
  select good, vers from xk_mws_barcode_validation(:barcode)
    into good, vers;
  if (coalesce(good,'') <> '' and vers is null) then
  begin
    select t.nazwa from towary t where t.ktm = :good
      into :goodname;
    for
      select s.mwsconstloc, c.symbol, s.lot, d.symbol, sum(s.quantity), sum(s.blocked), sum(s.ordered), s.x_partia, sr.serialno
        from mwsstock s
          left join mwsconstlocs c on (c.ref = s.mwsconstloc)
          left join dostawy d on (d.ref = s.lot)
          left join x_mws_serie sr on (sr.ref = s.x_serial_no)
        where s.wh = :wh and s.good = :good
        group by s.mwsconstloc, c.symbol, s.lot, d.symbol, s.x_partia, sr.serialno, d.data
        into mwsconstloc, mwsconstlocsymb, lot, lotsymb, quantity, blocked, ordered, x_partia, x_serial_str
    do begin
      if(x_partia is null) then x_partia = '-';
      if(x_serial_str is null) then x_serial_str = '-';
      if(x_slownik_str is null) then x_slownik_str = '-';
      if(lotsymb is null) then lotsymb = '-';
      suspend;
    end
  end else if (vers is not null) then
  begin
    select nazwat||', '||nazwa from wersje where ref = :vers
      into goodname;
    for
      select s.mwsconstloc, c.symbol, s.lot, d.symbol, sum(s.quantity), sum(s.blocked), sum(s.ordered), s.x_partia, sr.serialno
        from mwsstock s
          left join mwsconstlocs c on (c.ref = s.mwsconstloc)
          left join dostawy d on (d.ref = s.lot)
          left join x_mws_serie sr on (sr.ref = s.x_serial_no)
        where s.wh = :wh and s.vers = :vers
        group by s.mwsconstloc, c.symbol, s.lot, d.symbol, s.x_partia, sr.serialno
        into mwsconstloc, mwsconstlocsymb, lot, lotsymb, quantity, blocked, ordered, x_partia, x_serial_str
    do begin
      if(x_partia is null) then x_partia = '-';
      if(x_serial_str is null) then x_serial_str = '-';
      if(x_slownik_str is null) then x_slownik_str = '-';
      if(lotsymb is null) then lotsymb = '-';
      suspend;
    end
  end
end^
SET TERM ; ^
