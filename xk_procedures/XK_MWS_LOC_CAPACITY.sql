--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_LOC_CAPACITY(
      MWSCONSTLOC integer,
      VERS integer,
      QREFILL numeric(14,4),
      QUANTITYONLOCATION numeric(14,4))
  returns (
      QTOREFILL numeric(14,4))
   as
declare variable locvol numeric(14,6);
declare variable goodsvol numeric(14,6);
declare variable goodsvoltmp numeric(14,6);
declare variable locvers integer;
declare variable quantity numeric(14,4);
declare variable locgood varchar(40);
declare variable good varchar(40);
declare variable tmpvol numeric(14,6);
begin
  select ktm from wersje where ref = :vers
    into good;
  select (cast(c.l as numeric(14,6)) / 100) * (cast(c.h as numeric(14,6)) / 100) * (cast(c.w as numeric(14,6)) / 100)
    from mwsconstlocs c
    where c.ref = :mwsconstloc
    into locvol;
  if (locvol is null) then locvol = 0;
  goodsvol = 0;
  for
    select s.vers, s.good, sum(s.quantity + s.ordered)
      from mwsstock s
      where s.mwsconstloc = :mwsconstloc
      group by s.vers, s.good
      into locvers, locgood, quantity
  do begin
    select x.volume from xk_mws_przelicz_jednostki (:locgood, :locvers, :quantity, 0) x
      into tmpvol;
    if (tmpvol is null) then tmpvol = 0;
    goodsvol = goodsvol + tmpvol;
  end
  tmpvol = locvol * 0.9 - goodsvol;
  if (tmpvol > 0) then
  begin
    select x.quantity from xk_mws_vol_to_quantity (:good, :vers, :tmpvol, 0) x
      into qtorefill;
  end
end^
SET TERM ; ^
