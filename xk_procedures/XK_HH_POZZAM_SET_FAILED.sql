--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_HH_POZZAM_SET_FAILED(
      OPERATOR integer,
      POZZAMREF integer,
      QUANTITY numeric(14,4),
      DESCRIPTION varchar(255) CHARACTER SET UTF8                           )
   as
begin
  update pozzam p set p.status = 2, p.opis = :description where p.ref = :pozzamref;
end^
SET TERM ; ^
