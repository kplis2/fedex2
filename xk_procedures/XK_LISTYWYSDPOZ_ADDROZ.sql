--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSDPOZ_ADDROZ(
      KODKRESK varchar(40) CHARACTER SET UTF8                           ,
      IKODKRESK integer,
      LISTWYSD integer,
      ILOSC numeric(14,2),
      AKTOPK integer,
      TRYB smallint)
  returns (
      RTRYB smallint,
      STATUS smallint,
      MSG varchar(255) CHARACTER SET UTF8                           ,
      INFO varchar(255) CHARACTER SET UTF8                           )
   as
declare variable WERSJAREF integer;
declare variable POZYCJA integer;
declare variable REFPOZ integer;
declare variable REFROZ integer;
declare variable ILOSCROZ numeric(14,4);
declare variable ILOSCMAG numeric(14,4);
declare variable ILOSCTEST numeric(14,4);
declare variable STATUSDEL smallint;
declare variable SRV smallint;
declare variable TYPAKT smallint;
declare variable TYPPAK smallint;
declare variable REFPAK integer;
declare variable ROZNICASPR numeric(14,4);
declare variable STATUSSPR smallint;
declare variable MSGSPR varchar(255);
declare variable DODANO smallint;
declare variable tekst varchar(40);
begin
/*Tryb
0 - pakowanie (domyslny tryb)
1 - otworz karton
2 - otworz palete
3 - zamknij
4 - pakowanie opakowania w opakowanie
5 - usun
*/
  dodano = 0;
  kodkresk = upper(:kodkresk);
  rtryb = tryb;

  --ustawienie, ze dokument byl kontrolowany
  update listywysd set kontrolapak = 1
    where ref = :listwysd and (kontrolapak is null or kontrolapak = 0);
  --koniec ustawienia kontroli

  if (:tryb = 5) then begin
    rtryb = 0;
  exception universal 'Ldz - na testy';
  /*  execute procedure XK_LISTYWYSDROZ_GETTOWAR(:kodkresk, :ikodkresk, :listwysd)
      returning_values :wersjaref, :tekst;

     */

    if (coalesce(:wersjaref, 0) > 0) then begin
      execute procedure XK_LISTYWYSDPOZ_DELROZ(:listwysd, :wersjaref, :ilosc, :aktopk)
        returning_values :statusdel;

      if (:statusdel = -1) then begin
        tryb = 0;
        status = 0;
        wersjaref = 0;
        msg = 'Podano za dużą ilość do usunięcia lub brak towaru w aktywnym opakowaniu';
        suspend;
        exit;
      end
      else if (:statusdel = 0) then begin
        tryb = 0;
        status = 0;
        wersjaref = 0;
        msg = 'Towar nie został jeszcze spakowany lub brak towaru na zleceniu';
        suspend;
        exit;
      end
      else begin
        tryb = 0;
        status = 1;
        wersjaref = 0;
        msg = 'Podaj kod towaru';
      end
    end
    else if ((substring(:kodkresk from  1 for  3) = 'OPK') and substring(:kodkresk from  4 for 4) = '-' and
        substring(:kodkresk from  coalesce(char_length(:kodkresk),0) for coalesce(char_length(:kodkresk),0)) = '#' and -- [DG] XXX ZG119346
        exists(select first 1 1 from listywysdroz_opk where kodkresk = :kodkresk and listwysd = :listwysd)) then begin
      update listywysdroz_opk set rodzic = null
        where kodkresk = :kodkresk
          and listwysd = :listwysd;
    end
    else begin
      tryb = 0;
      status = 0;
      msg = 'Podano nieprawidłowy kod opakowania lub towaru';
      suspend;
      exit;
    end

    tryb = 0;
    status = 1;
    if (coalesce(:aktopk, 0) <> 0) then
      msg = 'Podaj kod towaru';
    else msg = 'Podaj kod opakowania';
    suspend;
    exit;
  end

  else if (:tryb = 4) then begin
    rtryb = 0;
    if (:aktopk > 0 and
        exists(select first 1 1 from listywysdroz_opk where kodkresk = :kodkresk and listwysd = :listwysd)) then begin
      update listywysdroz_opk set rodzic = :aktopk
        where listwysd = :listwysd
          and kodkresk = :kodkresk
--bez tego bedzie mozna przepakowywac karton w karton
          and rodzic is null;
--koniec
    end
    else if (:aktopk > 0 and (substring(:kodkresk from  1 for  3) = 'OPK') and substring(:kodkresk from  4 for 4) = '-' and
             substring(:kodkresk from  coalesce(char_length(:kodkresk),0) for coalesce(char_length(:kodkresk),0)) = '#') then begin -- [DG] XXX ZG119346
      insert into listywysdroz_opk(listwysd, kodkresk, stan, rodzic)
        values(:listwysd, :kodkresk, 0, :aktopk);
    end
    else begin
      msg = 'Nie wskazano opakowania';
      status = 0;
      suspend;
      exit;
    end
    msg = 'Wskaż opakowanie';
    status = 2;
    suspend;
    exit;
  end
--koniec pakowania kartonu w karton

--dodawania towarow oraz zakladanie opakowan
  else if (:tryb = 0 or :tryb = 1 or :tryb = 2) then
  begin
    rtryb = 0;
  --dodawanie towaru do opakowania
    if (coalesce(:wersjaref, 0) = 0) then begin
      if (exists(select nropk from listywysdroz_opk where listwysd = :listwysd and ref = :aktopk and stan = 1) or
          coalesce(:aktopk, 0) = 0) then begin
        msg = 'Nie można dodawać towaru do zamkniętego opakowania lub brak wskazania opakowania.';
        status = 0;
        suspend;
        exit;
      end

      --sprawdzenie czy istnieje towar
   --Ldz   execute procedure XK_LISTYWYSDROZ_GETTOWAR(:kodkresk, :ikodkresk, :listwysd) returning_values :wersjaref, :tekst;

      if (coalesce(:wersjaref, 0) = 0 or coalesce(:wersjaref, 0) = -1) then begin
        status = 0;
        wersjaref = 0;
        msg = 'Nie znaleziono towaru o podanym kodzie kreskowym.';
        suspend;
        exit;
      end
      --koniec sprawdzania czy istnieje towar

      for
        select rpozycja, rilosc
          from XK_LISTYWYSDROZ_GETPOZ(:listwysd, :wersjaref, :ilosc)
        into :pozycja, :ilosc
      do begin
        if (:pozycja = -1) then begin
          wersjaref = 0;
          status = 0;
          pozycja = 0;
          if (:ilosc is null) then
            msg = 'Brak towaru na dokumencie magazynowym!';
          else if(:ilosc < 0) then
            msg = 'Przekroczono ilość do spakowania o '||cast(abs(:ilosc) as numeric(14,2))||' sztuk!';
          else msg = 'Brak towaru na dokumencie lub przekroczono ilość do spakowania!';
          ilosc = 1;
          suspend;
          exit;
        end
--          if (coalesce(:rserial, 0) = 0) then begin
            insert into listywysdroz(listywysd, listywysdpoz, iloscmag, opk)
              values(:listwysd, :pozycja, :ilosc, :aktopk);

            msg = 'Zamknij opakowanie lub podaj kod towaru';
            status = 1;
            wersjaref = 0;
            pozycja = 0;

       --Ldz     execute procedure xk_listywysd_checkpak(:listwysd) returning_values :roznicaspr, :statusspr, :msgspr;
            if (:statusspr = 1) then info = :msgspr;

            dodano = 1;
--            suspend;
--            exit;
--          end
        end
        if (:dodano = 1) then begin
          dodano = 0;
          exit;
        end

        status = 1;
      end
      else status = 1;

      --sprawdzenie czy podany numer seryjny nie jest towarem
      if (:status = 1) then begin
        insert into listywysdroz(listywysd, listywysdpoz, iloscmag, opk)
          values(:listwysd, :pozycja, 1, :aktopk);

        msg = 'Zamknij opakowanie lub podaj kod towaru';

        if (:ilosc - 1 = 0) then begin
          status = 1;
          wersjaref = 0;
          pozycja = 0;

       --Ldz   execute procedure xk_listywysd_checkpak(:listwysd) returning_values :roznicaspr, :statusspr, :msgspr;
          if (:statusspr = 1) then info = :msgspr;

          suspend;
          exit;
        end
      end
      --koniec sprawdzenia czy podany numer seryjny nie jest towarem
  --koniec dodawania toawru do opakowania
  end
end^
SET TERM ; ^
