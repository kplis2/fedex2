--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_SHOW_ALL_QUANTITY(
      KTMIN varchar(80) CHARACTER SET UTF8                           ,
      MODE smallint)
  returns (
      FIRSTOPER integer,
      SHOPERNUM integer,
      KTMOUT varchar(80) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      WERSNUMBER integer,
      OPERTYPE varchar(255) CHARACTER SET UTF8                           ,
      OPERNAME varchar(255) CHARACTER SET UTF8                           ,
      GUIDESYMBOL varchar(255) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4),
      QUANTITYRES numeric(14,4),
      QUANTITYSHORTAGES numeric(14,4),
      OPERPARAM varchar(255) CHARACTER SET UTF8                           ,
      PRDEPART varchar(20) CHARACTER SET UTF8                           )
   as
declare variable mask smallint;
declare variable ktm varchar(80);
begin
  if (position('%' in ktmin) > 0) then
    mask = 1;
  else
    mask = 0;
  for
    select ktm
      from towary
      where (ktm = :ktmin and :mask = 0) or (:mask = 1 and ktm like '%'||:ktmin||'%')
      into :ktm
  do begin
    -- stan na produkcji po przewodnikach
    if (mode = 0) then
    begin
      for
        select prs.firstoper, prs.shopernum,
            prs.ktmout, prs.wersjaref, prs.wersnumber,
            prs.opertype, prs.opername, prs.guidesymbol, prs.quantity,
            prs.quantityres, prs.quantityshortages,prs.operparam, prs.prdepart
          from PR_SHOW_PR_IN_PROGRES(:ktm) prs
          into firstoper, shopernum,
            ktmout, wersjaref, wersnumber,
            opertype, opername, guidesymbol, quantity,
            quantityres, quantityshortages, operparam, prdepart
      do begin
        suspend;
      end
    end else if (mode = 1) then  -- po operacjach
    begin
      for
        select max(prs.firstoper), prs.shopernum,
            prs.ktmout, max(prs.wersjaref), max(prs.wersnumber),
            max(prs.opertype), prs.opername, max(''), sum(prs.quantity),
            sum(prs.quantityres), sum(prs.quantityshortages), prs.operparam, prs.prdepart
          from PR_SHOW_PR_IN_PROGRES(:ktm) prs
          group by prs.ktmout, prs.shopernum, prs.opername, prs.operparam, prs.prdepart
          into firstoper, shopernum,
            ktmout, wersjaref, wersnumber,
            opertype, opername, guidesymbol, quantity,
            quantityres, quantityshortages, operparam, prdepart
      do begin
        suspend;
      end
    end else if (mode = 2) then   -- po ktm
    begin
      for
        select max(0), max(0),
            prs.ktmout, max(prs.wersjaref), max(prs.wersnumber),
            max(''), max(''), max(''), sum(prs.quantity),
            sum(prs.quantityres), sum(prs.quantityshortages), max(''), prs.prdepart
          from PR_SHOW_PR_IN_PROGRES(:ktm) prs
          group by prs.ktmout, prs.prdepart
          into firstoper, shopernum,
            ktmout, wersjaref, wersnumber,
            opertype, opername, guidesymbol, quantity,
            quantityres, quantityshortages, opername, prdepart
      do begin
        suspend;
      end
    end else if (mode = 3) then -- po wydzialach
    begin
      select  sum(pg.amount - pg.amountzreal), pg.prdepart, pg.ktm, pg.wersja, w.ref
        from prschedguides pg
          left join wersje w on (w.ktm = pg.ktm and w.nrwersji = pg.wersja)
        where pg.ktm = :ktm
          and pg.status < 2
          and pg.status > 0
        group by pg.prdepart, pg.ktm, pg.wersja, w.ref
        into :quantity, :prdepart, :ktmout, :wersnumber, :wersjaref;

      suspend;
    end 

    -- stan na magazynach
    firstoper = null;
    shopernum = null;
    ktmout = null;
    wersjaref = null;
    wersnumber = null;
    opertype = null;
    opername = null;
    guidesymbol = null;
    quantity = null;
    quantityres = null;
    quantityshortages = null;
    operparam = null;
    prdepart = null;
    for
      select ktm, magazyn, wersjaref, wersja, sum(ilosc)
        from stanyil
        where (ktm = :ktm and :mask = 0) or (:mask = 1 and ktm like '%'||:ktm||'%')
        group by ktm, magazyn, wersjaref, wersja
        into ktmout, opertype, wersjaref, wersnumber, quantity
    do begin
      quantityres = quantity;
      suspend;
    end
  end
end^
SET TERM ; ^
