--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_WHSECGROUPS(
      WH DEFMAGAZ_ID)
  returns (
      WHSECGROUP STRING40,
      WHSECGROUPDESC STRING255)
   as
declare variable mwsorddivide smallint_id;
begin
  select coalesce(d.mwsorddivide,0) from defmagaz d where d.symbol = :wh
  into :mwsorddivide;

  if (:mwsorddivide < 2) then
  begin
    select first 1 g.whsecgroups, g.descript
      from whsecgroups g
      where g.wh = :wh
      order by g.whsecgroups
    into :whsecgroup, :whsecgroupdesc;

    if (coalesce(:whsecgroup,'') = '') then
      select first 1 ';'||w.ref||';', w.descript
        from whsecs w
        where w.wh = :wh
          and w.deliveryarea = 2
        order by ref
      into :whsecgroup, :whsecgroupdesc;
  end

  if (:whsecgroup is null) then whsecgroup = '';

  suspend;
end^
SET TERM ; ^
