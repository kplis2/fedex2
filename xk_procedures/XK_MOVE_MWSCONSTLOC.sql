--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MOVE_MWSCONSTLOC(
      MWSORD integer,
      DOCID integer,
      MWSCONSTLOC integer,
      MWSPALLOC integer)
   as
declare variable mwsconstlocl integer;
declare variable mwspallocl integer;
declare variable doctype varchar(1);
begin
  mwsconstlocl = null;
  mwspallocl = null;
  select o.doctype
    from mwsords o where o.ref = :mwsord
    into doctype;
  if (doctype = 'M') then
    insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
        mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
        regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority,
        lot, recdoc, plus, number, wharealogl, wharealogp, disttogo, palgroup, autogen,
        x_partia, x_serial_no, x_slownik) -- XXX KBI)
      select :mwsord, 1, 0, m.good, m.vers, m.quantity, m.mwsconstloc, m.mwspalloc,
          :mwsconstlocl, :mwspallocl, :docid, p.ref, :doctype, 0, 0, m.wh, m.wharea, m.whsec,
          current_date, current_date,current_date, null, null, null, 1,
          null, 0, 1, null, null, null, null, null, 1,
          m.x_partia, m.x_serial_no, m.x_slownik -- XXX KBI
        from mwsstock m
          left join dokumpoz p on (p.takefrommwsstock = m.ref)
        where m.mwspalloc = :mwspalloc and (m.mwsconstloc = :mwsconstloc or :mwsconstloc is null);
  if (doctype = 'Z') then
    insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
        mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
        regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority,
        lot, recdoc, plus, number, wharealogl, wharealogp, disttogo, palgroup, autogen,
        x_partia, x_serial_no, x_slownik) -- XXX KBI)
      select :mwsord, 1, 0, m.good, m.vers, m.quantity, m.mwsconstloc, m.mwspalloc,
          :mwsconstlocl, :mwspallocl, :docid, p.ref, :doctype, 0, 0, m.wh, m.wharea, m.whsec,
          current_date, current_date,current_date, null, null, null, 1,
          null, 0, 1, null, null, null, null, null, 1,
          m.x_partia, m.x_serial_no, m.x_slownik -- XXX KBI
        from mwsstock m
          left join pozzam p on (p.takefrommwsstock = m.ref)
        where m.mwspalloc = :mwspalloc and (m.mwsconstloc = :mwsconstloc or :mwsconstloc is null);
  update mwsords set status = 1, takefullpal = 1, operator = null where ref = :mwsord;
end^
SET TERM ; ^
