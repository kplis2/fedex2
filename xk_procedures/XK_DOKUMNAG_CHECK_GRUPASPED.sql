--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_DOKUMNAG_CHECK_GRUPASPED(
      DOCREF integer,
      SPOSDOST integer,
      ODBIORCASPEDID integer,
      AKTUODDZIAL varchar(20) CHARACTER SET UTF8                           ,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           )
  returns (
      DOCJOIN smallint)
   as
declare variable GRUPASPED integer;
declare variable GRREF integer;
declare variable LISTYWYS integer;
declare variable SPOSZAP integer;
declare variable FLAGISPED varchar(80);
begin
  -- wersja dla ARGO

/*  if (DOCREF = 362) then begin

    exception universal 'DOCREF '||coalesce(DOCREF ,'Brak')||

     ' POSDOST ' ||coalesce( sPOSDOST,'Brak')||

     ' ODBIORCASPEDID ' ||coalesce(ODBIORCASPEDID ,'Brak')||
     ' AKTUODDZIAL ' || coalesce(AKTUODDZIAL ,'Brak')||
     ' MAGAZYN ' ||coalesce(MAGAZYN  ,'Brak');

   end
    */
  docjoin = 1;
  select d.grupasped, d.sposzap, d.flagisped
    from dokumnag d
    where d.ref = :docref
    into grupasped, sposzap, flagisped;
  if (grupasped is not null and grupasped > 0 and grupasped <> docref) then
  begin
    docjoin = 0;
    exit;
  end
  if (docjoin = 1) then
  begin
    grref = null;
    select first 1 d.ref
      from dokumnag d
      where d.grupasped = :docref and d.ref <> d.grupasped
      into grref;
    if (grref is not null) then
    begin
      docjoin = 0;
      exit;
    end
    grref = null;
    select first 1 d.ref
      from dokumnag d
      where d.ref = :docref
      into grref;
    if (grref is null) then
    begin
      docjoin = 0;
      exit;
    end
    if (sposdost = 0 or odbiorcaspedid = 0) then
    begin
      docjoin = 0;
      exit;
    end
    listywys = null;
    select listywys from sposdost where ref = :sposdost
      into listywys;
    if (listywys is null or listywys <> 1) then
    begin
      docjoin = 0;
      exit;
    end
    grref = null;
    select first 1 g.odbiorcaidsped
      from dokumnag g
        left join defdokum d on (d.symbol = g.typ)
        left join sposdost s on (g.sposdost = s.ref)
      where g.oddzial = :aktuoddzial and d.wydania = 1 and d.zewn = 1 and d.koryg = 0
        and g.transport = 1 and g.wysylkadone = 0 and g.REF <> :docref AND g.AKCEPT = 1
        and s.listywys = 1 and g.faktura is null
        and g.odbiorcaidsped = :odbiorcaspedid
        --and coalesce( g.sposzap,0) = coalesce(:sposzap,0)
        and (g.flagisped = :flagisped or :flagisped is null or :flagisped = ''
          or g.flagisped is null or g.flagisped = '')
        and g.ref = g.grupasped
      group by g.odbiorcaidsped
      into grref;
    if (grref is null) then
    begin
      docjoin = 0;
      exit;
    end
  end
end^
SET TERM ; ^
