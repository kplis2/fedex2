--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GEN_MWSORD_STOCKTAKING(
      DOCID integer,
      DOCGROUP integer,
      DOCPOSID integer,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      MWSORD integer,
      MWSACT integer,
      RECALC smallint,
      MWSORDTYPE integer)
  returns (
      REFMWSORD integer)
   as
declare variable rec smallint;
declare variable wh varchar(3);
declare variable stocktaking smallint;
declare variable operator integer;
declare variable priority smallint;
declare variable branch varchar(10);
declare variable period varchar(6);
declare variable flags varchar(40);
declare variable symbol varchar(20);
declare variable cor smallint;
declare variable quantity numeric(14,4);
declare variable todisp numeric(14,4);
declare variable toinsert numeric(14,4);
declare variable good varchar(40);
declare variable vers integer;
declare variable opersettlmode smallint;
declare variable mwsconstlocp integer;
declare variable mwspallocp integer;
declare variable mwsconstlocl integer;
declare variable mwsconstlocltmp integer;
declare variable mwspallocltmp integer;
declare variable autoloccreate smallint;
declare variable mwspallocl integer;
declare variable whsec integer;
declare variable wharea integer;
declare variable whareag integer;
declare variable posflags varchar(40);
declare variable description varchar(1024);
declare variable posdescription varchar(255);
declare variable lot integer;
declare variable connecttype smallint;
declare variable shippingarea varchar(3);
declare variable quantityonlocation numeric(15,4);
declare variable wharealogp integer;
declare variable whareap integer;
declare variable mwsstandlevelnumber smallint;
declare variable difficulty varchar(10);
declare variable mwsaccessory integer;
declare variable operatordict varchar(80);
declare variable timestart timestamp;
declare variable maxnumber integer;
declare variable timestartdcl timestamp;
declare variable timestopdcl timestamp;
declare variable realtime double precision;
declare variable wharealogb integer;
declare variable wharealoge integer;
declare variable wharealogl integer;
declare variable dist numeric(14,4);
declare variable lifthight numeric(14,4);
declare variable actuvolume numeric(14,4);
declare variable insertvolume numeric(14,4);
declare variable przelicz numeric(14,4);
declare variable obj numeric(14,4);
declare variable docobj numeric(14,4);
declare variable refmwsactpal integer;
declare variable paltype varchar(20);
declare variable whsecout integer;
declare variable shippingtype integer;
declare variable nextmwsord smallint;
declare variable tmp integer;
declare variable docpostomwsord integer;
declare variable maxpalvol numeric(14,4);
declare variable takefullpal smallint;
declare variable frommwsconstloc integer;
declare variable palgroupmm integer;
declare variable takefullpalmm integer;
declare variable takefrommwsconstlocmm integer;
declare variable takefrommwspallocmm integer;
declare variable poscnt integer;
declare variable insertoper integer;
declare variable slodef integer;
declare variable slopoz integer;
declare variable slokod varchar(40);
declare variable docreal smallint;
declare variable mwsconstloci integer;
declare variable mwspalloci integer;
declare variable stancen integer;
declare variable takefrommwsstock integer;
declare variable mwsactdet integer;
declare variable mwsconstlocpstock integer;
declare variable mwspallocpstock integer;
declare variable autocommit smallint;
declare variable refilli smallint;
declare variable typedoc varchar(3);
declare variable pal smallint;
declare variable rozonakc smallint;
declare variable rozonakcs varchar(100);
declare variable whareali integer;
declare variable wharealogli integer;
begin
  autocommit = 1;
  wharealogb = wharea;
  wharealoge = wharea;
  mwsstandlevelnumber = 1;
  if (recalc is null) then recalc = 0;
  if (docgroup is null) then docgroup = docid;
  poscnt = 0;
  --nagówek zlecenia magazynowego
  select stocktaking, opersettlmode, autoloccreate
    from mwsordtypes
    where ref = :mwsordtype
    into stocktaking, opersettlmode, autoloccreate;
  select defdokum.wydania, defdokum.koryg,
      coalesce(dokumnag.mwsmag, dokumnag.magazyn),
      dokumnag.operator,
      dokumnag.uwagi, dokumnag.katmag,
      dokumnag.spedpilne, dokumnag.oddzial, dokumnag.okres, dokumnag.flagi,
      dokumnag.symbol, dokumnag.kodsped, dokumnag.termdost, dokumnag.sposdost,
      dokumnag.takefullpal, dokumnag.palgroup, dokumnag.takefrommwsconstloc,
      dokumnag.takefrommwspalloc, 1, klienci.ref, substring(klienci.fskrot from 1 for 40),
      dokumnag.mwsdocreal, defdokum.symbol
    from dokumnag
      left join defdokum on (defdokum.symbol = dokumnag.typ)
      left join klienci on (klienci.ref = dokumnag.klient)
    where dokumnag.ref = :docid
    into rec, cor, wh, operator,
      description, difficulty,
      priority, branch, period, :flags,
      :symbol, shippingarea, :timestart, :shippingtype,
      takefullpalmm, palgroupmm, takefrommwsconstlocmm,
      takefrommwspallocmm, slodef, slopoz, slokod,
      docreal, typedoc;
  if (rec = 0) then
    select first 1 t.ref
      from mwsordtypes t
      where position(';'||:typedoc||';' in t.doctypes) > 0 and t.mwsortypedets in (17,19)
      into mwsordtype;
  else
    select first 1 t.ref
      from mwsordtypes t
      where position(';'||:typedoc||';' in t.doctypes) > 0 and t.mwsortypedets in (16,18)
      into mwsordtype;
  select recdoc from mwsordtypes where ref = :mwsordtype into rec;
  execute procedure gen_ref('MWSORDS') returning_values refmwsord;
  select okres from datatookres(current_date,0,0,0,0) into period;
  insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator, priority,
      description, wh,  regtime, timestartdecl, mwsaccessory, branch, period,
      flags, docsymbs, cor, rec, bwharea, ewharea, status, shippingarea, SHIPPINGTYPE, takefullpal,
      slodef, slopoz, slokod)
    values (:refmwsord, :mwsordtype, :stocktaking, 'M', :docgroup, :docid, :operator, :priority,
        :description, :wh, current_timestamp(0), :timestart, :mwsaccessory, :branch, :period,
        :flags, :symbol, :cor, :rec, :wharealogb, :wharealoge, 0, :shippingarea, :SHIPPINGTYPE, 0,
        :slodef, :slopoz, :slokod);
  select max(number) from mwsacts where mwsord = :refmwsord into maxnumber;
  if (maxnumber is null) then maxnumber = 0;
  -- wybranie akcesorium operatora
  if (operator is not null and operator > 0) then
    select ref, lifthight
      from mwsaccessories where aktuoperator = :operator into mwsaccessory, lifthight;
  -- okreslenie lokacji inwentaryzacyjnej
  execute procedure XK_MWS_GET_BEST_LOCATION(null,null,null,:refmwsord,:mwsordtype,
      :wh, null, null, null, null, null, null,null,null,null,null,null,0,0)
    returning_values (mwsconstloci, mwspalloci, whareali, wharealogli, refilli);
  select min(ref) from mwspallocs where mwsconstloc = :mwsconstloci
    into mwspalloci;
  maxnumber = maxnumber + 1;
  -- generowanie pozycji zlecenia
  for
    select dokument, ref, ktm, wersjaref, case when :rozonakc = 0 then ilosc - ilosconmwsacts else iloscl - ilosconmwsacts end,
        flagi, uwagi, dostawa, takefrommwsstock
      from dokumpoz
      where dokument = :docid and  case when :rozonakc = 0 then ilosc else iloscl end > ilosconmwsacts
      into docid, docposid, good, vers, quantity,
         posflags, posdescription, lot, takefrommwsstock
  do begin
    select mwsconstloc, mwspalloc from mwsstock where ref = :takefrommwsstock
      into mwsconstloci, mwspalloci;
    if (rec = 1) then
    begin
      mwsconstlocl = mwsconstloci;
      mwspallocl = mwspalloci;
      mwsconstlocp = null;
      mwspallocp = null;
    end else
    begin
      mwsconstlocl = null;
      mwspallocl = null;
      mwsconstlocp = mwsconstloci;
      mwspallocp = mwspalloci;
    end
    if (quantity > 0) then
    begin
      maxnumber = maxnumber + 1;
      insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
          mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
          regtime, timestartdecl, timestopdecl, mwsaccessory, realtimedecl, flags, descript, priority, lot, recdoc, plus,
          whareal, whareap, wharealogl, wharealogp, number, disttogo, palgroup, autogen)
        values (:refmwsord, 1, :stocktaking, :good, :vers, :quantity, :mwsconstlocp, :mwspallocp,
            :mwsconstlocl,:mwspallocl, :docid, :docposid, 'M', :opersettlmode, :autoloccreate, :wh, :whareag, :whsec,
            current_timestamp(0), :timestartdcl, :timestopdcl, :mwsaccessory, :realtime, :posflags, :posdescription, :priority, :lot, :rec, 1,
            null, :whareap, null, :wharealogp, :maxnumber, :dist, null, 1);
      wharea = :wharealogp;
    end
  end
  -- czas dla ostatniej operacji - dojscie do punktu pozostawienia zlecenia
  update mwsords set status = 1, operator = null, nextmwsord = 0,
    mwsords.timestopdecl = :timestopdcl where ref = :refmwsord and status = 0;
  -- autopotwierdzenie zlecenia
  if (autocommit = 1) then
  begin
    for
      select distinct ref from mwsords where docgroup = :docgroup and ref = :refmwsord
        into refmwsord
    do begin
      update mwsacts set status = 2, quantityc = quantity where mwsord = :refmwsord and status <> 5;
      update mwsords set status = 5 where ref = :refmwsord and status <> 5;
    end
  end
  suspend;
end^
SET TERM ; ^
