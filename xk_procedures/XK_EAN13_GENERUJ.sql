--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_EAN13_GENERUJ(
      KTM varchar(80) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE KODKRESK VARCHAR(12);
DECLARE VARIABLE KW VARCHAR(5);
DECLARE VARIABLE KOD_Z_KONFIGA VARCHAR(1024);
DECLARE VARIABLE LK VARCHAR(2);
DECLARE VARIABLE WAGA VARCHAR(12);
DECLARE VARIABLE I INTEGER;
DECLARE VARIABLE SUMA INTEGER;
DECLARE VARIABLE M INTEGER;
begin
  select wartosc from konfig  where akronim = 'EAN13' into :kod_z_konfiga;
    execute procedure xk_ean13_kodwlasny returning_values :kw;
    kodkresk = :kod_z_konfiga||:kw;
  if (coalesce(char_length(kodkresk),0) <> 12)  then exception EAN13; -- [DG] XXX ZG119346
  waga = '131313131313';
  i=1 ;
  suma = 0;
  while (i<13) do
  begin
    suma = suma + cast(substring(kodkresk from  i for i) as integer) * cast(substring(waga from i for  i) as integer);
    i=i+1;
  end
  m =  10 - mod(suma,10);
  if (m = 10) then m = 0;
  lk = cast (m as varchar(2));
  insert into towkodkresk(kodkresk,ktm,gl) values (:kodkresk||:lk,:ktm,0);

end^
SET TERM ; ^
