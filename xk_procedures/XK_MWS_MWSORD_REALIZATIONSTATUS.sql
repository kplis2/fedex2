--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSORD_REALIZATIONSTATUS(
      MWSORDREF integer)
  returns (
      GOOD KTM_ID,
      GOODNAME STRING100,
      REALIZACJA STRING20,
      WAGA STRING10,
      NAME2 STRING100,
      LOKACJA STRING20,
      ODBIORCA STRING100,
      SYMBOL STRING20)
   as
begin
  for
    select ma.good, round(sum(ma.quantityc))||'/'||round(sum(ma.quantity)), m.symbol, w.nazwat, t.x_nazwa2, round(tj.waga), mo.symbol, case when (coalesce(d.odbiorcaid, '') = '') then  d.odbiorca else o.nazwa end
    from mwsords mo
      join mwsacts ma on (ma.mwsord = mo.ref)
        left join mwsconstlocs m on(m.ref = ma.mwsconstlocp)
        left join wersje w on (ma.vers = w.ref)
        left join towary t on (ma.good = t.ktm)
        left join towjedn tj on (tj.ktm = t.ktm and tj.jedn = w.miara)
        left join dokumnag d on (d.ref = mo.docid)
        left join odbiorcy o on (o.ref = d.odbiorcaid)
        where mo.ref = :mwsordref
    group by ma.good, m.symbol, w.nazwa, w.nazwat, t.x_nazwa2, round(tj.waga), mo.symbol, case when (coalesce(d.odbiorcaid, '') = '') then  d.odbiorca else o.nazwa end
    order by case when max(mo.rec) = 1 then max(ma.number) else 0 end desc, min(ma.number)
    into good, realizacja, lokacja, goodname, name2, waga, symbol, odbiorca
  do begin
    if (name2 is null) then name2 = '-';
    suspend;
  end
end^
SET TERM ; ^
