--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_ASSIGN_ORD_FOR_OP(
      MWSORDREF integer,
      OPERATOR varchar(10) CHARACTER SET UTF8                           )
  returns (
      INFORMATION varchar(255) CHARACTER SET UTF8                           )
   as
declare variable opref integer;
declare variable opname varchar(60);
declare variable symbol varchar(60);
declare variable ordref integer;
begin
  select o.ref, o.nazwa
    from operator o
    where o.login = :operator
    into :opref, :opname;
  if (opref is null) then
    exception universal 'Nie istnieje operator o podanym loginie.';
  select mo.ref, mo.symbol
    from mwsords mo
    where mo.ref = :mwsordref
      and mo.status = 4
    into :ordref, :symbol;
  if (ordref is null) then
    exception universal 'zlecenie nie moze byc przypisane do operatora.';
  update mwsords mo set mo.operator = :opref, mo.status = 2 where mo.ref = :ordref;
  update mwsacts ma set ma.status = 1 where ma.mwsord = :ordref and ma.status = 4;
  information = 'Przypisano zlecenie: '||:symbol||' dla operatora: '||:opname;
  suspend;
end^
SET TERM ; ^
