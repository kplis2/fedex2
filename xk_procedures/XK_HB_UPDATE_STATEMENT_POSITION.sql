--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_HB_UPDATE_STATEMENT_POSITION(
      RKDOKNAG integer)
   as
declare variable TICACCOUNT varchar(28);
declare variable DICTDEF integer;
declare variable DICTPOS integer;
declare variable DESCRIPT varchar(1024);
declare variable PM char(1);
declare variable RKDEFOPER varchar(25);
declare variable RKPOZOPER integer;
declare variable RKSTNKAS char(2);
declare variable BANKACC varchar(10);
declare variable AMOUNT numeric(14,2);
declare variable BTRANSFER integer;
declare variable DLA varchar(255);
declare variable COMPANY integer;
declare variable COUNTER integer;
begin

  select N.stanowisko, N.pm, N.kwota, N.ticaccount, N.opis, S.company
    from rkdoknag N
      join rkstnkas S on (N.stanowisko = S.kod)
    where N.ref = :rkdoknag
    into :rkstnkas, :pm, :amount, :ticaccount, :descript, :company;

  if (ticaccount <> '') then
  begin
    if (pm = '-') then
    begin
      --automatyczne podączanie BTRANSFER'a pod daną operacje
      select symbol
        from bankacc where stanbank = :rkstnkas
        into :bankacc;
      select min(ref) from btransfers b
        where bankacc = :bankacc and status = 2 and amount = :amount
          and replace(replace(toacc,' ',''),'-','') = replace(replace(:TICACCOUNT,' ',''),'-','')
          and company = :company
          and not exists(select rn.ref from rkdoknag rn where rn.btransfer = b.ref)
        into :btransfer;
      if (btransfer is not null) then
      begin

        select B.slodef, B.slopoz, T.rkdefoper, T.rkpozoper
          from btransfers B
            join btrantype T on (T.symbol = B.btype)
          where B.ref = :btransfer
          into :dictdef, :dictpos, :rkdefoper, :rkpozoper;

        execute procedure name_from_dictposref(dictdef, dictpos)
          returning_values dla;
        update rkdoknag set slodef = :dictdef, slopoz = :dictpos, dla = :dla,
            operacja = :rkdefoper, btransfer = :btransfer
          where ref = :rkdoknag;

        if (exists(select ref from btransferpos where btransfer = :btransfer)) then
        begin
          --zakądam pozycje operacji na podstawie przelewu
          delete from rkdokpoz where dokument = :rkdoknag;
          insert into rkdokpoz (dokument, pozoper, rozrachunek, kwota, kwotazl)
            select :rkdoknag,  :rkpozoper, settlement, amount, amount
              from btransferpos where btransfer = :btransfer;
        end else
        begin
          update rkdokpoz set pozoper = :rkpozoper where dokument = :rkdoknag;
        end
        --tu jeszcze rozbicie na pozycje rozrachunkow
      end
    end

    if (rkdefoper is null) then
    begin
      if (pm = '+') then
      begin
        select distinct max(dictdef), max(dictpos), count(1) from bankaccounts where company = :company
            and replace(replace(eaccount,' ',''),'-','') = replace(replace(:ticaccount,' ',''),'-','')
            and dictdef = 1
          into :dictdef, :dictpos, :counter;
      end else
      begin
        select distinct max(dictdef), max(dictpos), count(1) from bankaccounts where company = :company
            and replace(replace(eaccount,' ',''),'-','') = replace(replace(:ticaccount,' ',''),'-','')
            and dictdef = 6
          into :dictdef, :dictpos, :counter;
      end
      if (dictpos is null) then
      begin
        select distinct max(dictdef), max(dictpos), count(1) from bankaccounts where eaccount = :ticaccount
            and company = :company
          into :dictdef, :dictpos, :counter;
      end

      if (dictpos is not null and counter = 1) then
      begin
        rkdefoper = pm;
        if (dictdef = 1) then
          rkdefoper = rkdefoper || 'NALEŻNO';
        else if (dictdef = 6) then
          rkdefoper = rkdefoper || 'ZOBOWIA';
        execute procedure name_from_dictposref(dictdef, dictpos)
          returning_values dla;
        update rkdoknag set slodef = :dictdef, slopoz = :dictpos, dla = :dla,
            operacja = :rkdefoper
          where ref = :rkdoknag;

         select dompozoper from rkdefoper
           where symbol = :rkdefoper
           into :rkpozoper;
         if (rkpozoper is not null) then
           update rkdokpoz set pozoper = :rkpozoper where dokument = :rkdoknag;
      end
    end
  end
end^
SET TERM ; ^
