--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_IMP_ECOD(
      REF integer)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
   as
declare variable document integer;
begin
  select ID from EDEDOCSP
  where EDEDOCH=:ref and PARENT is null and name='Document-Invoice' into :document;
  if(:document is not null) then begin
    select OTABLE,OREF from XK_IMP_ECOD_INVOICE(:ref,:document) into :otable,:oref;
    suspend;
  end
  document = null;
  select ID from EDEDOCSP
  where EDEDOCH=:ref and PARENT is null and name='Document-Order' into :document;
  if(:document is not null) then begin
    select OTABLE,OREF from XK_IMP_ECOD_ORDER(:ref,:document) into :otable,:oref;
    suspend;
  end
  document = null;
  select ID from EDEDOCSP
  where EDEDOCH=:ref and PARENT is null and name='Document-DespatchAdvice' into :document;
  if(:document is not null) then begin
    select OTABLE,OREF from XK_IMP_ECOD_DESADV(:ref,:document) into :otable,:oref;
    suspend;
  end
end^
SET TERM ; ^
