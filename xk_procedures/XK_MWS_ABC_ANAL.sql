--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_ABC_ANAL(
      WH varchar(3) CHARACTER SET UTF8                           ,
      BDATE date,
      AAREA numeric(14,4),
      BAREA numeric(14,4))
  returns (
      LOCAV integer,
      ALOCAV integer,
      BLOCAV integer,
      CLOCAV integer,
      APOSCNT integer,
      BPOSCNT integer,
      CPOSCNT integer)
   as
declare variable poscnt integer;
begin
  select count(c.ref)
    from mwsconstlocs c
      left join whsecs s on (s.ref = c.whsec)
    where s.deliveryarea = 2 and c.wh = :wh and c.act <> 0
    into :locav;
  select count(c.ref)
    from mwsconstlocs c
      left join whsecs s on (s.ref = c.whsec)
    where s.deliveryarea = 2 and c.wh = :wh and c.abc = 'A' and c.act <> 0
    into :alocav;
  if (alocav is null) then alocav = 0;
  select count(c.ref)
    from mwsconstlocs c
      left join whsecs s on (s.ref = c.whsec)
    where s.deliveryarea = 2 and c.wh = :wh and c.abc = 'B' and c.act <> 0
    into :blocav;
  if (alocav is null) then alocav = 0;
  select count(c.ref)
    from mwsconstlocs c
      left join whsecs s on (s.ref = c.whsec)
    where s.deliveryarea = 2 and c.wh = :wh and c.abc = 'C' and c.act <> 0
    into :clocav;
  if (alocav is null) then alocav = 0;
  select count(distinct p.wersjaref)
    from dokumpoz p
      left join dokumnag d on (d.ref = p.dokument)
      left join defdokum f on (f.symbol = d.typ)
    where f.wydania = 1 and f.zewn = 1 and f.koryg = 0 and p.data >= :bdate
      and d.magazyn = :wh
    into poscnt;
  if (poscnt is null) then poscnt = 0;
  aposcnt = poscnt * alocav / locav;
  bposcnt = poscnt * blocav / locav;
  cposcnt = poscnt * clocav / locav;
  suspend;
end^
SET TERM ; ^
