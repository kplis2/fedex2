--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSDPOZ_REMOVING_TOWAR(
      BAR_CODE varchar(40) CHARACTER SET UTF8                           ,
      ILOSC numeric(14,2),
      MNOZNIK numeric(14,2),
      ACTIVE_BOX_CODE varchar(40) CHARACTER SET UTF8                           ,
      SPED_DOC integer,
      POZYCJA INTEGER_ID,
      WERSJAREF INTEGER_ID)
  returns (
      ERR_MSG varchar(255) CHARACTER SET UTF8                           ,
      INFO_MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable ACTIVE_BOX_REF integer;
declare variable ACTIVE_BOX_OPENED integer;
declare variable DEL_REC_REF integer;
declare variable BOXES_COUNT integer;
declare variable SAME_BOXES smallint;
declare variable ILOSCROZ numeric(14,2);
declare variable ILOSCMAG numeric(14,2);
declare variable ROZNICA numeric(14,2);
declare variable REFROZ integer;
declare variable REFPOZ integer;
declare variable KLIENT integer;
declare variable ODBIORCA integer;
begin
    err_msg = '';
    INFO_MSG = '';

    select l.slopoz, l.odbiorcaid from listywysd l
      where l.ref = :sped_doc and l.slodef = 1
    into klient, :odbiorca;
    if (:klient is null and :odbiorca is not null) then
      select klient from odbiorcy where ref = :odbiorca
      into :klient;

    select opk.ref, opk.stan from listywysdroz_opk opk where opk.stanowisko = :active_box_code and opk.listwysd = :sped_doc
    into :active_box_ref, :active_box_opened;

     --  select ref from XK_LISTYWYSDROZ_GETTOWAR(:bar_code, :klient) into :wersjaref;


       if (coalesce(:wersjaref, 0) = 0) then
          begin
            err_msg = 'Nie znaleziono towaru o podanym kodzie kreskowym.';
          end
       else
          begin
                boxes_count = 0;
                while (mnoznik > 0) do
                begin
                  --tutaj
                  select sum(lwr.iloscmag)
                    from listywysdroz lwr
                      join listywysdpoz lwp on (lwr.listywysdpoz = lwp.ref)
                    where lwr.listywysd = :sped_doc
                      and lwp.wersjaref = :wersjaref
                      and lwr.opk = :active_box_ref
                  into :iloscroz;

                  --error jesli iloscroz is null
                  if (iloscroz is null) then
                  begin
                    ERR_MSG = 'Nie znaleziono towaru o podanym kodzie kreskowym (ilosc).';
                    mnoznik = 0;
                  end

                  --tutaj info jesli ilosc usuwana jest wieksza od ilosci na dokumentach
                  if (:iloscroz = (:ilosc * :mnoznik) ) then
                  begin
                        for select lwr.ref
                            from listywysdroz lwr
                                join listywysdpoz lwp on (lwr.listywysdpoz = lwp.ref)
                            where lwr.listywysd = :sped_doc
                                and lwp.wersjaref = :wersjaref
                                and lwr.opk = :active_box_ref
                            into :refroz
                        do begin
                            delete from listywysdroz where ref = :refroz;
                            --tutaj info o prawidlowym usunieciu wszystkich
                            INFO_MSG = 'Wskazana ilość towaru została usunięta.';
                            mnoznik = 0;
                        end
                  end
                  else begin
                        iloscroz = (:ilosc * :mnoznik);
                        for select lwp.ref, lwr.ref, lwr.iloscmag, lwr.iloscmag - (:ilosc * :mnoznik)
                            from listywysdroz lwr
                                join listywysdpoz lwp on (lwr.listywysdpoz = lwp.ref)
                            where lwp.wersjaref = :wersjaref
                                and lwr.listywysd = :sped_doc
                                and lwr.opk = :active_box_ref
                                order by abs(lwr.iloscmag - :ilosc)
                            into :refpoz, :refroz, :iloscmag, :roznica
                        do begin
                            if (:refpoz is null or :refroz is null) then
                            begin
                                --tutaj komunikat bledu - nic nie znaleziono
                                INFO_MSG = 'Nie znaleziono towaru do usunięcia.';
                                mnoznik = 0;
                            end
                            else if (:roznica = 0) then
                            begin
                                    delete from listywysdroz where ref = :refroz;
                                    --tutaj komunikat ze usunieto OK
                                    INFO_MSG = 'Wskazana ilość towaru została usunięta.';
                                    mnoznik = 0;
                            end
                            else begin
                                if (:roznica > 0) then
                                begin
                                    update listywysdroz set iloscmag = iloscmag - (:ilosc * :mnoznik)
                                    where ref = :refroz;
    
                                    --tutaj komunikat usunieto wskazana ilosc OK
                                    INFO_MSG = 'Wskazana ilość towaru została usunięta.';
                                    mnoznik = 0;
                                end

                                if (:roznica < 0) then
                                begin
                                    if (:iloscroz > 0 and :iloscmag <= :iloscroz) then
                                    begin
                                        delete from listywysdroz where ref = :refroz;
                                        iloscroz = iloscroz - :iloscmag;
                                    end
                                    else if (:iloscroz > 0 and :iloscmag > :iloscroz) then
                                    begin
                                        update listywysdroz set iloscmag = iloscmag - :iloscroz
                                        where ref = :refroz;
    
                                        -- tutaj komunikat ze usunieto OK
                                        INFO_MSG = 'Wskazana ilość towaru została usunięta.';
                                        mnoznik = 0;
                                    end
                                    else if (:iloscroz = 0) then
                                    begin
                                        --komunikat ze usuwanie poszlo OK
                                        INFO_MSG = 'Wskazana ilość towaru została usunięta.';
                                        mnoznik = 0;
                                    end
                                end
                            end
                        end
                  end
                end
          end
    suspend;
end^
SET TERM ; ^
