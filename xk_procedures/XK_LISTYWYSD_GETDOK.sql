--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSD_GETDOK(
      WARTOSC varchar(255) CHARACTER SET UTF8                           ,
      OPERATOR integer)
  returns (
      REFDOK integer,
      STATUS smallint,
      MSG varchar(4096) CHARACTER SET UTF8                           )
   as
declare variable klient integer;
declare variable pref integer;
declare variable magazyn varchar(3);
declare variable grupasped integer;
declare variable symbol varchar(30);
declare variable eol varchar(30);
declare variable xilwyscheckedil numeric(14,4);
declare variable xilwyscheckil numeric(14,4);
declare variable wozekint integer;
declare variable operatorszyk integer;
declare variable sposdost integer;
declare variable wozki varchar(4096);
declare variable uwagi varchar(4096);
declare variable korekty varchar(4096);
declare variable tymsym varchar(4096);
declare variable platnosc varchar(255);
declare variable grupadoc varchar(4096);
declare variable docsymbol varchar(40);
declare variable gotowka smallint;
declare variable zagraniczny smallint;
declare variable intstatus smallint;
declare variable grupamagazyn varchar(3);
declare variable listywysakcept smallint;
begin
  eol = '
';
  refdok = 0;
  status = 1;
  execute procedure CHECK_STRING_TO_INTEGER (:wartosc) returning_values intstatus;
  if (intstatus is null) then
    intstatus = 0;
  if (intstatus = 1) then
  begin
    select d.ref, coalesce(p.autozap,0), p.opis, coalesce(k.zagraniczny,0), d.grupasped, d.magazyn
      from dokumnag d
        left join platnosci p on (p.ref = d.sposzap)
        left join klienci k on (k.ref = d.klient)
      where d.ref = cast(:wartosc as integer)
      into refdok, gotowka, platnosc, zagraniczny, grupasped, magazyn;
    if (refdok is null) then
    begin
      msg = 'Brak dokumentu o podanym refie';
      status = 0;
      exit;
    end
  end else
  begin
    msg = 'Aktualnie brak możliwości szukania po symbolu, podaj REF dokumentu';
    status = 0;
    exit;
  end
end^
SET TERM ; ^
