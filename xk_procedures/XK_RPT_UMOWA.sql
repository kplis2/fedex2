--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_UMOWA(
      DOKPLIK integer)
  returns (
      INFO1 varchar(1024) CHARACTER SET UTF8                           ,
      INFO2 varchar(1024) CHARACTER SET UTF8                           ,
      INFO3 varchar(1024) CHARACTER SET UTF8                           ,
      INFOREGON varchar(1024) CHARACTER SET UTF8                           ,
      INFONIP varchar(1024) CHARACTER SET UTF8                           ,
      INFOREJ1 varchar(1024) CHARACTER SET UTF8                           ,
      INFOREJ2 varchar(1024) CHARACTER SET UTF8                           ,
      INFOREJ3 varchar(1024) CHARACTER SET UTF8                           ,
      NRUMOWY varchar(40) CHARACTER SET UTF8                           ,
      KLIENT varchar(255) CHARACTER SET UTF8                           ,
      ADRES varchar(255) CHARACTER SET UTF8                           ,
      DATA varchar(30) CHARACTER SET UTF8                           )
   as
begin
  select k.wartosc from konfig k where k.akronim = 'INFO1' into info1;
  select k.wartosc from konfig k where k.akronim = 'INFO2' into info2;
  select k.wartosc from konfig k where k.akronim = 'INFO3' into info3;

  select k.wartosc from konfig k where k.akronim = 'INFOREGON' into inforegon;
  select k.wartosc from konfig k where k.akronim = 'INFONIP' into infonip;

  select k.wartosc from konfig k where k.akronim = 'INFOREJ1' into inforej1;
  select k.wartosc from konfig k where k.akronim = 'INFOREJ2' into inforej2;
  select k.wartosc from konfig k where k.akronim = 'INFOREJ3' into inforej3;

  select d.symbol, c.nazwa, c.kodp||' '||c.miasto||', '||c.ulica, cast(cast(d.data as date) as varchar(30))
    from dokplik d
    left join cpodmioty c on (c.ref = d.cpodmiot)
    where d.ref=:dokplik
    into :nrumowy, :klient,  :adres, :data;

  if (nrumowy is null) then  nrumowy = '';
  if (klient is null) then  klient = '';
  if (adres is null) then  adres = '';
  if (data is null) then  data = '';
  suspend;
end^
SET TERM ; ^
