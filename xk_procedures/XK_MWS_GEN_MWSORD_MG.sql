--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GEN_MWSORD_MG(
      MWSORDIN integer,
      MWSORDTYPE integer,
      MWSFIRSTCONSTLOCPREF integer,
      AUTOCOMMIT smallint,
      ACTUOPERATOR integer,
      MWSSTOCKCHECK smallint,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      PRIORITY smallint)
  returns (
      MWSORD integer)
   as
declare variable WH varchar(3);
declare variable BRANCH varchar(10);
declare variable PERIOD varchar(6);
declare variable MWSFIRSTCONSTLOCP varchar(20);
begin
  if (coalesce(mwsfirstconstlocpref,0) = 0) then
    exception universal 'Nie wybrano lokacji początkowej.';
  if (mwsordtype = 0 or mwsordtype is null) then
  begin
    select first 1 m.ref from mwsordtypes m where m.mwsortypedets = 11
      into :mwsordtype;
  end
  if (mwsordin = 0) then mwsordin = null;
  if (mwsstockcheck is null) then mwsstockcheck = 1;
  if (autocommit is null) then autocommit = 0;
  if (descript is null) then descript = '';
  select symbol from mwsconstlocs where ref = :mwsfirstconstlocpref into mwsfirstconstlocp;
  if (mwsordtype is null) then exception MWSORDTYPE_NOT_SET;
  if (mwsstockcheck = 1) then
  begin
    if (exists(select ref from mwsstock where mwsconstloc = :mwsfirstconstlocpref and ispal = 0 and blocked > 0)) then
      exception MWSORD_ALREADY_PLANED 'Zaplanowano zlecenie dla lokacji poczatkowej '||mwsfirstconstlocp;
  end
  select wh from mwsconstlocs where ref = :mwsfirstconstlocpref into :wh;
  select oddzial from defmagaz where symbol = :wh into :branch;
  if (mwsordin is null) then
  begin
    execute procedure gen_ref('MWSORDS') returning_values :mwsordin;
    select okres from datatookres(current_date,0,0,0,0) into :period;
    insert into mwsords (ref, mwsordtype, stocktaking, doctype, operator, priority, description, wh,
        regtime, timestartdecl, timestopdecl, branch, period, cor, rec,
        bwharea, ewharea, status, mwsfirstconstlocp, mwsfirstconstlocpref)
      values (:mwsordin, :mwsordtype, 0, 'P', :actuoperator, :priority, :descript, :wh,
        current_timestamp(0), current_timestamp(0), current_timestamp(0), :branch, :period, 0, 0,
        null, null, 0, :mwsfirstconstlocp, :mwsfirstconstlocpref);
    update mwsords set status = 1 where ref = :mwsordin and status = 0;
  end
  mwsord = mwsordin;
end^
SET TERM ; ^
