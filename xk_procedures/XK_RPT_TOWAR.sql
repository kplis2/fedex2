--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_TOWAR(
      REFPOZ integer,
      TYP varchar(3) CHARACTER SET UTF8                           ,
      FL varchar(10) CHARACTER SET UTF8                           )
  returns (
      OPIS varchar(1024) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE DOKPOZ INTEGER;
DECLARE VARIABLE KTMDOK VARCHAR(40);
DECLARE VARIABLE CECHA VARCHAR(40);
DECLARE VARIABLE TYPPAR INTEGER;
DECLARE VARIABLE SYMBOLPAR VARCHAR(40);
DECLARE VARIABLE PROC VARCHAR(2048);
DECLARE VARIABLE WARTTIME TIMESTAMP;
DECLARE VARIABLE WARTSTRING VARCHAR(1024);
DECLARE VARIABLE WARTOSCDOM VARCHAR(1024);
DECLARE VARIABLE WARTNUM NUMERIC(14,2);
DECLARE VARIABLE NAZ VARCHAR(60);
DECLARE VARIABLE DOSTAWA INTEGER;
DECLARE VARIABLE DOSTSYMB VARCHAR(60);
DECLARE VARIABLE DOSTSYMBOLD VARCHAR(60);
DECLARE VARIABLE DOKUMROZREF INTEGER;
DECLARE VARIABLE ATRYBPOZ VARCHAR(1024);
DECLARE VARIABLE FLAGA VARCHAR(40);
declare variable eol varchar(40);
declare variable d1 varchar(255);
declare variable d2 varchar(255);
declare variable d3 varchar(255);
declare variable d4 varchar(255);
declare variable dokument integer;
declare variable numer integer;
declare variable listadostaw varchar(255);
declare variable wartint integer;
begin
  eol = '
';
  opis='';
  dostsymbold = '';
  dostsymb = '';
  atrybpoz= '';
  /* ustal KTM*/
  if(:typ = 'FAK') then begin
    select pozfak.ktm from pozfak where pozfak.ref=:refpoz
    into :ktmdok;
  end else if(:typ = 'ZAM') then begin
    select pozzam.ktm from pozzam where pozzam.ref=:refpoz
    into :ktmdok;
  end else begin
    select dokumpoz.ktm from dokumpoz where dokumpoz.ref = :refpoz
    into :ktmdok;
  end
  /* ustal liste dostaw magazynowych*/
  listadostaw = '';
  dokpoz = null;
  if(:typ = 'FAK') then begin
    /* znajdz dokumpoz zwiazany z faktura */
    select max(dokumpoz.ref) from dokumpoz where dokumpoz.frompozfak = :refpoz
      into :dokpoz;
    /* znajdz dokumpoz zwiazany z faktura pierwotna */
    if(:dokpoz is null) then begin
      select dokument,numer from pozfak where ref=:refpoz into :dokument,:numer;
      select ref from nagfak where reffak=:dokument into :dokument;
      select ref from pozfak where dokument=:dokument and numer=:numer and coalesce(fake,0) = 0 into :refpoz;
      select max(dokumpoz.ref) from dokumpoz where dokumpoz.frompozfak = :refpoz
        into :dokpoz;
    end
    /* wez dostawe z pozycji faktury */
    if(:dokpoz is null) then begin
      dostawa = null;
      select dostawa from pozfak where ref=:refpoz into :dostawa;
      if(:dostawa is not null) then listadostaw = cast(:dostawa as varchar(10));
    end
  end else if(:typ = 'ZAM') then begin
    /* znajdz dokumpoz zwiazany z zamowieniem */
    select max(dokumpoz.ref) from dokumpoz where dokumpoz.frompozzam = :refpoz
      into :dokpoz;
    /* wez dostawe z pozycji zamowienia */
    if(:dokpoz is null) then begin
      dostawa = null;
      select dostawamag from pozzam where ref=:refpoz into :dostawa;
      if(:dostawa is not null) then listadostaw = cast(:dostawa as varchar(10));
    end
  end else begin
    dokpoz = :refpoz;
  end
  if(:dokpoz is not null) then begin
    for select dostawa from dokumroz where pozycja=:dokpoz
    into :dostawa
    do begin
      if(:listadostaw<>'') then listadostaw = :listadostaw||',';
      listadostaw = :listadostaw||cast(:dostawa as varchar(10));
    end
  end
  /* przejrzyj wszystkie atrybuty danego KTMu*/
  for select atrybuty.cecha,atrybuty.wartosc
    from atrybuty
    left join defcechy on (atrybuty.cecha=defcechy.symbol)
    where atrybuty.ktm = :ktmdok
    order by defcechy.lp
  into :cecha,:wartoscdom
  do begin
    flaga = '';
    typpar = 0;
    symbolpar = '';
    naz = '';
    /* sprawdź czy cecha jest atrybutem dokumentu */
    select defcechy.partyp,defcechy.parsymbol,defcechy.nazwa,defcechy.flagi
      from defcechy where defcechy.symbol = :cecha
    into :typpar,:symbolpar,:naz,:flaga;
    /* sprawdz czy ma zadana flage */
    if(flaga like '%'||:fl||'%') then
    begin
      if(typpar is null) then typpar = 0;
      if(:typpar = 0) then begin
        /* atrybut towaru, wez wartosc z atrybutow */
        if(:wartoscdom is null) then wartoscdom = '';
        if(:wartoscdom<>'') then
          atrybpoz = :atrybpoz||:naz||':'||:wartoscdom||:eol;
      end else if(typpar = 1) then begin
        /* jesli jest to parametr wolny, wez jego wartosc z pozycji dokumentu */
        wartnum = null;
        wartstring = null;
        warttime = null;
        if(:typ='FAK') then begin
          proc= 'select pozfak.'||:symbolpar||' from pozfak where pozfak.ref = '||cast(:refpoz as varchar(20));
        end else if(:typ='ZAM') then begin
          proc= 'select pozzam.'||:symbolpar||' from pozzam where pozzam.ref = '||cast(:refpoz as varchar(20));
        end else begin
          proc= 'select dokumpoz.'||:symbolpar||' from dokumpoz where dokumpoz.ref = '||cast(:refpoz as varchar(20));
        end
        if (:symbolpar like 'PARAMN%') then begin
          execute statement proc into :wartnum;
          wartint = cast(:wartnum as integer);
          if(:wartint=:wartnum) then begin
            if(:wartint is not null and :wartint<>0) then
              atrybpoz = :atrybpoz||:naz||':'||cast(:wartint as varchar(40))||:eol;
          end else begin
            if(:wartnum is not null and :wartnum<>0) then
              atrybpoz = :atrybpoz||:naz||':'||cast(:wartnum as varchar(40))||:eol;
          end
        end
        if (:symbolpar like 'PARAMS%') then begin
          execute statement proc into :wartstring;
          if(:wartstring is not null and :wartstring <>'') then
            atrybpoz = :atrybpoz||:naz||':'||:wartstring||:eol;
        end
        if (:symbolpar like 'PARAMD%') then begin
          execute statement proc into :warttime;
          if(:warttime is not null and :warttime <>'') then
            atrybpoz = :atrybpoz||:naz||':'||cast(:warttime as varchar(30))||:eol;
        end
      end else if(:listadostaw is not null and :listadostaw<>'') then begin
        /* jesli parametr dotyczy dostawy, znajdz odpowiednia dostawe*/
        proc = '';
        proc = 'select ref from dostawy where ref in ('||:listadostaw||')';
        for execute statement :proc
          into :dostawa
        do begin
          wartnum = null;
          wartstring = null;
          warttime = null;
          proc= 'select dostawy.'||:symbolpar||' from dostawy where dostawy.ref = '||cast(:dostawa as varchar(20));
          if(:symbolpar like 'PARAMN%') then begin
            execute statement proc into :wartnum;
            wartint = cast(:wartnum as integer);
            if(:wartint=:wartnum) then begin
              if(:wartint is not null and :wartint<>0) then
                atrybpoz = :atrybpoz||:naz||':'||cast(:wartint as varchar(40))||:eol;
            end else begin
              if(:wartnum is not null and :wartnum<>0) then
                atrybpoz = :atrybpoz||:naz||':'||cast(:wartnum as varchar(40))||:eol;
            end
          end else if(:symbolpar like 'PARAMS%') then begin
            execute statement proc into :wartstring;
            if(:wartstring is not null and :wartstring <>'') then
              atrybpoz = :atrybpoz||:naz||':'||:wartstring||:eol;
          end else if(:symbolpar like 'PARAMD%') then begin
            execute statement proc into :warttime;
            if(:warttime is not null and :warttime <>'') then
              atrybpoz = :atrybpoz||:naz||':'||cast(:warttime as varchar(30))||:eol;
          end
        end
      end
    end
  end
  opis = case when(coalesce(char_length(:opis),0) > 5) then :opis || :eol else '' end; -- [DG] XXX ZG119346
  opis = :opis || :atrybpoz;
  if(substring(:opis from 1 for 1)=' ') then opis=substring(:opis from 2 for 1024);
  if(:opis is null) then opis = '';
  suspend;
end^
SET TERM ; ^
