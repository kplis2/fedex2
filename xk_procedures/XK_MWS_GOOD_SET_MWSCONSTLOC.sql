--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GOOD_SET_MWSCONSTLOC(
      MWSCONSTLOCS varchar(30) CHARACTER SET UTF8                           ,
      AKTUMAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      GOOD varchar(40) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable wh varchar(3);
declare variable mwsconstloc integer;
declare variable vers integer;
declare variable bargood varchar(40);
declare variable barstatus smallint;
begin
  status = 1;
  if (mwsconstlocs = '' or good = '') then
  begin
    status = 0;
    msg = 'Nie wypełnione wymagane pola';
    exit;
  end
  select ref, wh
    from mwsconstlocs
    where symbol = :mwsconstlocs
    into mwsconstloc, wh;
  if (mwsconstloc is null) then
  begin
    status = 0;
    msg = 'Brak lokacji na magazynie';
    exit;
  end
  if (wh <> aktumagazyn) then
  begin
    status = 0;
    msg = 'Lokacja nie należy do magazynu: '||aktumagazyn;
    exit;
  end
  select good, status
    from  XK_MWS_BARCODE_VALIDATION(:good)
    into bargood, barstatus;
  if (bargood is null or bargood = '' or status = 0) then
  begin
    status = 0;
    msg = 'Podany kod kreskowy nie jest przypisany do towaru';
    exit;
  end
  for
    select ref from wersje where ktm = :bargood
      into vers
  do begin
    delete from mwsconstlocsymbs
      where mwsconstloc = :mwsconstloc and avmode = 0
        and symbol = :bargood and const = 1 and vers = :vers;
    insert into mwsconstlocsymbs (mwsconstloc, avmode, symbol, const, vers, wh)
      values(:mwsconstloc, 0, :bargood, 1, :vers, :aktumagazyn);
  end
end^
SET TERM ; ^
