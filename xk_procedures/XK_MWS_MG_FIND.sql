--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MG_FIND(
      PALGROUPS varchar(40) CHARACTER SET UTF8                           )
  returns (
      REFMWSORD integer)
   as
declare variable palgroup integer;
begin

  select first 1 o.ref
    from mwsords o
      join dokumnag d on (o.docid = d.ref)
    where d.int_symbol = :PALGROUPS and o.status > 0 and o.status < 5
      and o.mwsordtypedest in (1,2)
    into REFMWSORD;

  if (REFMWSORD is null) then
  begin
    palgroups = replace(PALGROUPS, '|', '');
    palgroup = cast(palgroups as integer);
    select first 1 o.ref
      from mwsords o
      where o.palgroup = :palgroup and o.status > 0 and o.status < 5
        and o.mwsordtypedest = 11
      into REFMWSORD;
  end

end^
SET TERM ; ^
