--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_EXP_ECOD_DESADV(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable symbol varchar(255);
declare variable data date;
declare variable datasprz date;
declare variable waluta varchar(10);
declare variable termzap date;
declare variable termin integer;
declare variable korekta smallint;
declare variable uwagi varchar(2048);
declare variable grupakup integer;
declare variable numer integer;
declare variable refwersji integer;
declare variable jednostka integer;
declare variable ktm varchar(255);
declare variable nazwa varchar(255);
declare variable opk integer;
declare variable ilosc numeric(14,3);
declare variable pilosc numeric(14,3);
declare variable miara varchar(5);
declare variable cenanet numeric(14,2);
declare variable pcenanet numeric(14,2);
declare variable stawka numeric(14,2);
declare variable pstawka numeric(14,2);
declare variable stawkaedi varchar(10);
declare variable pstawkaedi varchar(10);
declare variable kodkresk varchar(255);
declare variable kodtowaru varchar(255);
declare variable haspkwiu smallint;
declare variable pkwiu varchar(255);
declare variable wartbru numeric(14,2);
declare variable pwartbru numeric(14,2);
declare variable wartnet numeric(14,2);
declare variable pwartnet numeric(14,2);
declare variable zamid varchar(255);
declare variable zamznakzewn varchar(255);
declare variable zamdatazewn date;
declare variable nrdoksped varchar(255);
declare variable odbiorcagln numeric(13,0);
declare variable datawysylki date;
declare variable nrlistuprzewozowego varchar(255);
declare variable pozfakref integer;
declare variable fromdokumpoz integer;
declare variable klientgln numeric(13,0);
declare variable klientnip varchar(255);
declare variable klientrachunek varchar(255);
declare variable klientnazwa varchar(255);
declare variable klientulica varchar(255);
declare variable klientnrdomu nrdomu_id;
declare variable klientnrlokalu nrdomu_id;
declare variable klientmiasto varchar(255);
declare variable klientkodp varchar(255);
declare variable klientkraj varchar(255);
declare variable platnikgln numeric(13,0);
declare variable platniknip varchar(255);
declare variable platnikrachunek varchar(255);
declare variable platniknazwa varchar(255);
declare variable platnikulica varchar(255);
declare variable platnikmiasto varchar(255);
declare variable platnikkodp varchar(255);
declare variable platnikkraj varchar(255);
declare variable info1 varchar(255);
declare variable info2 varchar(255);
declare variable info3 varchar(255);
declare variable totallines integer;
declare variable sumwartnet numeric(14,2);
declare variable sumwartbru numeric(14,2);
declare variable psumwartnet numeric(14,2);
declare variable psumwartbru numeric(14,2);
declare variable sumkaucja numeric(14,2);
declare variable psumkaucja numeric(14,2);
declare variable sumwartvat numeric(14,2);
declare variable psumwartvat numeric(14,2);
declare variable intrdostawy varchar(255);
declare variable intrtransport varchar(255);
declare variable odbiorcanazwa varchar(255);
declare variable odbiorcaulica varchar(255);
declare variable odbiorcakodp varchar(255);
declare variable odbiorcamiasto varchar(255);
declare variable odbiorcakraj varchar(255);
declare variable dokumpozref integer;

declare variable document_invoice integer;
declare variable invoice_header integer;
declare variable invoice_parties integer;
declare variable invoice_lines integer;
declare variable line integer;
declare variable line_item integer;
declare variable taxreference integer;
declare variable invoice_summary integer;
declare variable line_order integer;
declare variable line_delivery integer;
declare variable buyer integer;
declare variable payer integer;
declare variable seller integer;
declare variable tax_summary integer;
declare variable tax_summary_line integer;
declare variable document_despatch integer;
declare variable despatch_header integer;
declare variable despatch_transport integer;
declare variable despatch_parties integer;
declare variable delivery integer;
declare variable despatch_consigment integer;
declare variable packing_sequence integer ;
begin
    -- eksport dokumentu spedycyjnego
    select l.symbol,l.dataakc,l.datazam,l.symbolsped,s.intrdostawy,s.intrtransport,
      k.gln,k.prostynip,k.nazwa,k.ulica,k.nrdomu,k.nrlokalu,k.miasto,k.kodp,k.krajid,k.grupakup,
      o.gln,l.kontrahent,l.adres,l.miasto,l.kodp,k.krajid
    from listywysd l
    left join sposdost s on s.ref=l.sposdost
    left join klienci k on (k.ref=l.slopoz and l.slodef=1)
    left join odbiorcy o on o.ref=l.odbiorcaid
    where l.ref=:oref
    into :nrdoksped,:data,:datawysylki,:nrlistuprzewozowego,:intrdostawy,:intrtransport,
    :klientgln,:klientnip,:klientnazwa,:klientulica,:klientnrdomu,:klientnrlokalu,:klientmiasto,:klientkodp,:klientkraj,:grupakup,
    :odbiorcagln,:odbiorcanazwa,:odbiorcaulica,:odbiorcamiasto,:odbiorcakodp,:odbiorcakraj;

    id = 1;
    document_despatch = :id;
    parent = NULL;
    name = 'Document-DespatchAdvice';
    params = '';
    val = '';
    suspend;
    parent = :document_despatch;
    id = id + 1;
    despatch_header = :id;
    name = 'DespatchAdvice-Header';
    suspend;

-- despatch header
    parent = :despatch_header;
    id = id + 1;
    name = 'DespatchAdviceNumber';
    val = :nrdoksped;
    suspend;
    id = id + 1;
    name = 'DespatchAdviceDate';
    val = :data;
    suspend;
    id = id + 1;
    name = 'EstimatedDeliveryDate';
    val = :datawysylki;
    suspend;
    id = id + 1;
    name = 'DespatchNumber';
    val = :nrlistuprzewozowego;
    suspend;
-- despatch advice transport
    parent = :document_despatch;
    id = id + 1;
    despatch_transport = :id;
    val = '';
    name = 'DespatchAdvice-Transport';
    suspend;
    parent = :despatch_transport;
    id = id + 1;
    name = 'TermsOfDelivery';
    val = :intrdostawy;
    suspend;
    id = id + 1;
    name = 'ModeOfTransport';
    val = :intrtransport||'0';
    suspend;
-- despatch advice parties
    parent = :document_despatch;
    id = id + 1;
    despatch_parties = :id;
    val = '';
    name = 'DespatchAdvice-Parties';
    suspend;
-- buyer (kupujacy)
    parent = :despatch_parties;
    id = id + 1;
    buyer = :id;
    name = 'Buyer';
    val = '';
    suspend;
    parent = :buyer;
    id = id + 1;
    name = 'ILN';
    val = :klientgln;
    suspend;
    id = id + 1;
    name = 'TaxID';
    val = :klientnip;
    suspend;
    id = id + 1;
    name = 'Name';
    val = :klientnazwa;
    suspend;
    id = id + 1;
    name = 'StreetAndNumber';
    val = :klientulica||iif(coalesce(:klientnrdomu,'')<>'',' '||:klientnrdomu,'')||iif(coalesce(:klientnrlokalu,'')<>'','/'||:klientnrlokalu,'');
    suspend;
    id = id + 1;
    name = 'CityName';
    val = :klientmiasto;
    suspend;
    id = id + 1;
    name = 'PostalCode';
    val = :klientkodp;
    suspend;
    id = id + 1;
    name = 'Country';
    val = :klientkraj;
    suspend;
-- seller (wystawiajacy fakture)
    parent = :despatch_parties;
    id = id + 1;
    seller = :id;
    name = 'Seller';
    val = '';
    suspend;
    parent = :seller;
    id = id + 1;
    name = 'ILN';
    select wartosc from getconfig('INFOGLN') into :val;
    suspend;
    id = id + 1;
    name = 'TaxID';
    select wartosc from getconfig('INFONIP') into :val;
    suspend;
    id = id + 1;
    name = 'Name';
    select wartosc from getconfig('INFO1') into :val;
    suspend;
    id = id + 1;
    name = 'StreetAndNumber';
    select wartosc from getconfig('INFO2') into :val;
    suspend;
    id = id + 1;
    name = 'CityName';
    select substring(wartosc from 8) from getconfig('INFO3') into :val;
    suspend;
    id = id + 1;
    name = 'PostalCode';
    select substring(wartosc from 1 for 6) from getconfig('INFO3') into :val;
    suspend;
    id = id + 1;
    name = 'Country';
    val = 'PL';
    suspend;
-- delivery
    parent = :despatch_parties;
    id = id + 1;
    delivery = :id;
    name = 'DeliveryPoint';
    val = '';
    suspend;
    id = id + 1;
    parent = :delivery;
    name = 'ILN';
    val = :odbiorcagln;
    suspend;
    id = id + 1;
    name = 'Name';
    val = :odbiorcanazwa;
    suspend;
    id = id + 1;
    name = 'StreetAndNumber';
    val = :odbiorcaulica;
    suspend;
    id = id + 1;
    name = 'CityName';
    val = :odbiorcamiasto;
    suspend;
    id = id + 1;
    name = 'PostalCode';
    val = :odbiorcakodp;
    suspend;
    id = id + 1;
    name = 'Country';
    val = :odbiorcakraj;
    suspend;

-- despatchadvice-consigment
    parent = :document_despatch;
    id = id + 1;
    despatch_consigment = :id;
    name = 'DespatchAdvice-Consigment';
    val = '';
    suspend;
-- packing-sequence
    parent = :despatch_consigment;
    id = id + 1;
    packing_sequence = :id;
    name = 'Packing-Sequence';
    val = '';
    suspend;

    numer = 0;
    for select w.ktm, t.nazwa, p.wersjaref, p.ilosc, p.dokpoz, p.opis, j.ref, m.editype
    from listywysdpoz p
--    left join listywysdroz r
    left join wersje w on w.ref=p.wersjaref
    left join towary t on t.ktm=w.ktm
    left join towjedn j on j.ref=t.dm
    left join miara m on m.miara=j.jedn
    where p.dokument=:oref
    into :ktm, :nazwa, :refwersji, :ilosc, :dokumpozref, :uwagi, :jednostka, :miara
    do begin
      select symbol from XK_GET_KODKRESK(:GRUPAKUP,:REFWERSJI,:JEDNOSTKA) into :kodkresk;
      select symbol from XK_GET_KTMTOGRUPYKUP(:GRUPAKUP,:REFWERSJI,:JEDNOSTKA) into :kodtowaru;
      select nz.id, nz.znakzewn, nz.datazewn
      from dokumpoz p
      left join pozzam pz on pz.ref=p.frompozzam
      left join nagzam nz on nz.ref=pz.zamowienie
      where p.ref=:dokumpozref
      into :zamid, :zamznakzewn, :zamdatazewn;

-- line (pozycja dok. sped.)
      parent = :packing_sequence;
      id = id + 1;
      line = :id;
      name = 'Line';
      val = '';
      suspend;
-- line item (dane z pozycji dok. sped.)
      parent = :line;
      id = id + 1;
      line_item = :id;
      name = 'Line-Item';
      val = '';
      suspend;

      parent = :line_item;
      id = id + 1;
      name = 'LineNumber';
      numer = :numer + 1;
      val = :numer;
      suspend;
      id = id + 1;
      name = 'EAN';
      val = :kodkresk;
      suspend;
      if(:kodtowaru<>'') then begin
        id = id + 1;
        name = 'BuyerItemCode';
        val = :kodtowaru;
        suspend;
      end
      id = id + 1;
      name = 'SupplierItemCode';
      val = :ktm;
      suspend;
      id = id + 1;
      name = 'QuantityDespatched';
      val = :ilosc;
      suspend;
      id = id + 1;
      name = 'UnitOfMeasure';
      val = :miara;
      suspend;
      id = id + 1;
      name = 'ItemDescription';
      val = :nazwa;
      suspend;
      id = id + 1;
      name = 'Remarks';
      val = :uwagi;
      suspend;

-- line-order
      if(:zamid is not null) then begin
        parent = :line;
        id = id + 1;
        line_order = :id;
        name = 'Line-Order';
        val = '';
        suspend;
        id = id + 1;
        parent = :line_order;
        name = 'BuyerOrderNumber';
        val = :zamznakzewn;
        suspend;
        id = id + 1;
        name = 'BuyerOrderDate';
        val = :zamdatazewn;
       suspend;
      end
    end
end^
SET TERM ; ^
