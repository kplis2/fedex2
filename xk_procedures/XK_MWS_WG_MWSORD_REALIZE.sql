--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_WG_MWSORD_REALIZE(
      MWSORDREF integer,
      MWSACTREF integer,
      VERS integer,
      QUANTITY integer,
      QUANTITYC integer,
      DEFICIENCY smallint,
      MWSCONSTLOCREF integer,
      AKTUOPERATOR integer,
      MWSPALLOCREF integer,
      LOT integer)
  returns (
      RMWSCONSTLOCREF integer,
      MWSCONSTLOC varchar(120) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      RVERS integer,
      RQUANTITY integer,
      RQUANTITYC integer,
      RMWSACTREF integer,
      QUANTITYSTRING varchar(80) CHARACTER SET UTF8                           ,
      ORDDESCRIPT varchar(1000) CHARACTER SET UTF8                           ,
      ACTDESCRIPT varchar(1000) CHARACTER SET UTF8                           ,
      RAKTUOPERATOR integer,
      ORDSYMBOL varchar(20) CHARACTER SET UTF8                           ,
      REALSTATUS integer,
      PREPARED varchar(10) CHARACTER SET UTF8                           ,
      VERIFYMWSCONSTLOC integer,
      VERIFYMWSCONSTLOCSYMB varchar(20) CHARACTER SET UTF8                           ,
      VERIFYMWSCONSTLOCSQUAN numeric(14,4),
      VERIFYMWSACTREF integer,
      RMWSPALLOCREF integer,
      RMWSPALLOC varchar(20) CHARACTER SET UTF8                           ,
      RLOT integer,
      RLOTSYMB varchar(120) CHARACTER SET UTF8                           ,
      WEIGHTLEFT numeric(14,4))
   as
declare variable actstatus smallint;
declare variable locquantity numeric(14,4);
declare variable allacts integer;
declare variable actsreal integer;
declare variable lactref integer;
declare variable lquanc numeric(14,4);
declare variable lquan numeric(14,4);
declare variable actref integer;
declare variable maxnumber integer;
declare variable lactdescript varchar(8191);
declare variable nrwersji integer;
declare variable docgroup integer;
declare variable dref integer;
declare variable ordverify integer;
declare variable nextact integer;
declare variable wtiopercnt integer;
declare variable wticnt integer;
declare variable shippingtype integer;
declare variable mwsconstlocverify smallint;
declare variable verifyon smallint;
declare variable verifyquan integer;
declare variable verifyactscnt integer;
declare variable mwsordout smallint;
declare variable wh varchar(3);
declare variable goodverify varchar(10);
declare variable opergrupa varchar(1024);
begin
  select wh
    from mwsords
    where ref = :mwsordref
    into :wh;
  realstatus = 0;
  actdescript = '';
  if (wh is null) then wh = 'MWS';
  if (mwspallocref is null) then mwspallocref = 0;
  -- potwierdzanie pozycji
  select first 1 ma.ref
    from mwsacts ma
    where ma.mwsord = :mwsordref
      and ma.status < 5
      and ma.status > 0
      into :nextact;
  if (nextact is null) then mwsactref = 0;
  if (mwsactref is not null and mwsactref <> 0) then
  begin
    if (deficiency = 0 and quantity = 0) then
      exception MWS_ZERO_COMMITED;
    select sum(ma.quantity)
      from mwsacts ma
      where ma.mwsord = :mwsordref
        and ma.mwsconstlocp = :mwsconstlocref
        and ma.status < 3 and ma.status > 0
        and (ma.mwspallocp = :mwspallocref or :mwspallocref = 0)
        and (ma.lot = :lot or :lot = 0)
        and ma.vers = :vers
      into lquan;
    if (lquan >= quantity) then begin
      for
        select ma.ref, ma.quantity
          from mwsacts ma
          where ma.mwsord = :mwsordref
            and ma.status < 3 and ma.status > 0
            and ma.mwsconstlocp = :mwsconstlocref
            and (ma.mwspallocp = :mwspallocref or :mwspallocref = 0)
            and (ma.lot = :lot or :lot = 0)
            and ma.vers = :vers
          order by ma.number desc
          into lactref, lquanc
      do begin
        if (deficiency = 0) then begin
          if (quantity <= lquanc) then
            update mwsacts ma set ma.status = 2, ma.quantityc = :quantity where ma.ref = :lactref;
          else
            update mwsacts ma set ma.status = 2, ma.quantityc = :lquanc where ma.ref = :lactref;
          quantity = quantity - lquanc;
          if (quantity <= 0) then
            break;
        end else begin
          update mwsconstlocs m set m.act = 0 where m.ref = :mwsconstlocref;
          if (quantity <= 0) then
            update mwsacts ma set ma.status = 6 where ma.ref = :lactref;
          else if (quantity < lquanc) then begin
            update mwsacts ma set ma.status = 0, ma.quantity = :quantity where ma.ref = :lactref;
            update mwsacts ma set ma.status = 1 where ma.ref = :lactref;
            update mwsacts ma set ma.status = 2, ma.quantityc = ma.quantity where ma.ref = :lactref;
            lquanc = lquanc - quantity;
            quantity = quantity - quantity;
            select max(ma.number)
              from mwsacts ma
              where ma.mwsord = :mwsordref
              into maxnumber;
            maxnumber = maxnumber + 1;
            insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
                mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc,
                wh, wharea, whsec, timestart, timestop,
                regtime, timestartdecl, timestopdecl, mwsaccessory, realtimedecl,
                flags, descript, priority, lot, recdoc, plus,
                whareal, whareap, wharealogl, wharealogp, number, disttogo, palgroup, autogen)
              select ma.mwsord, 6, 0, ma.good, ma.vers, :lquanc, ma.mwsconstlocp, ma.mwspallocp,
                  ma.mwsconstlocl, ma.mwspallocl, ma.docid, ma.docposid, 'M', ma.settlmode, ma.closepalloc,
                  ma.wh, ma.wharea,  ma.whsec, current_timestamp(0), current_timestamp(0),
                  current_timestamp(0), ma.timestartdecl, ma.timestopdecl, ma.mwsaccessory, ma.realtimedecl,
                  ma.flags, 'zgloszona inwentaryzacja', ma.priority, ma.lot, 0, 1,
                  null, ma.whareap, null, ma.wharealogp, :maxnumber, ma.disttogo, ma.palgroup, ma.autogen
                from mwsacts ma where ma.ref = :lactref;
          end else begin
            update mwsacts ma set ma.status = 2, ma.quantityc = :lquanc where ma.ref = :lactref;
            quantity = quantity - lquanc;
          end
        end
      end
    end else
      exception MWS_INWALID_QUAN;
  end
 -- pobieranie pozycji
  select mo.symbol, mo.description, mo.operator, mo.docgroup, mo.shippingtype, mo.wh, o.grupa
    from mwsords mo
      left join operator o on(mo.operator = o.ref)
    where mo.ref = :mwsordref
      and mo.status < 3
      and mo.status > 0
    into ordsymbol, orddescript, raktuoperator, docgroup, shippingtype, wh, opergrupa;
  if (raktuoperator is null) then exception universal 'Nie ustawiony operator!! DZwoń do SENTE';
  if (:raktuoperator <> :aktuoperator) then exception universal 'Zlecenie realizuje inny operator. Przerwij realizacje zlecenia!!!'||:raktuoperator;
  if (ordsymbol is null) then
    realstatus = 3;
  else begin
    select first 1 ma.vers, ma.mwsconstlocp, ma.good, ma.ref, mc.symbol, w.nrwersji, t.nazwa, iif(df.magbreak = 2,ma.lot,0), iif(df.magbreak = 2,d.symbol,'')
      from mwsacts ma
        left join mwsconstlocs mc on(mc.ref = ma.mwsconstlocp)
        left join mwspallocs m on(m.ref = ma.mwspallocp)
        join wersje w on(w.ref = ma.vers)
        join towary t on(t.ktm = ma.good)
        left join defmagaz df on(ma.wh = df.symbol)
        left join dostawy d on(ma.lot = d.ref)
      where ma.mwsord = :mwsordref
        and ma.status < 3 and ma.status > 0
      order by ma.number
      into :rvers, :rmwsconstlocref, :ktm, :rmwsactref, :mwsconstloc, :nrwersji, :name, :rlot, :rlotsymb;
    rmwspallocref = 0;
    if (rvers is not null) then begin
      for
        select distinct descript
          from mwsacts
          where mwsord = :mwsordref
            and mwsconstlocp = :rmwsconstlocref
            and (mwspallocp = :rmwspallocref or :rmwspallocref = 0)
            and (lot = :rlot or :rlot = 0)
            and descript <> ''
          into :lactdescript
      do begin
        actdescript = actdescript||lactdescript||', ';
      end
      select sum(ma.quantity), sum(ma.quantityc)
        from mwsacts ma
        where ma.mwsord = :mwsordref
          and ma.mwsconstlocp = :rmwsconstlocref
          and ma.vers = :rvers
          and (ma.mwspallocp = :rmwspallocref or :rmwspallocref = 0)
          and (lot = :rlot or :rlot = 0)
          and ma.status > 0 and ma.status < 3
        into :rquantity, rquantityc;
      select count(ma.ref)
        from mwsacts ma
        where ma.mwsord = :mwsordref
          and ma.status < 6 and ma.status > 0
        into :allacts;
      select count(ma.ref)
        from mwsacts ma
        where ma.mwsord = :mwsordref
          and ma.status = 5
        into :actsreal;
      actsreal = actsreal + 1;
      prepared = actsreal||'\'||allacts;
      select sum(ma.weight)
        from mwsacts ma
        where ma.mwsord = :mwsordref
          and ma.status < 5 and ma.status > 0
        into :weightleft;
      if (weightleft is null) then weightleft = 0;
      select QUANTITYSTRING from xk_mws_przelicz_jednostki(:ktm,:rvers,:rquantity,1)
        into :quantitystring;
      update mwsacts ma set ma.status = 2 where ma.mwsord = :mwsordref and ma.mwsconstlocp = :rmwsconstlocref
        and ma.vers = :rvers and ma.status in(1,2)
        and (ma.mwspallocp = :rmwspallocref or :rmwspallocref = 0) and (ma.lot = :rlot or :rlot = 0);
      realstatus = 0;
    end else begin
        realstatus = 1;
    end
  end
  suspend;
end^
SET TERM ; ^
