--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_HH_INWENTAP_ADD(
      INWENTA integer,
      VERS integer,
      QUANTITY numeric(14,4),
      OPERATOR integer)
   as
declare variable ktm varchar(17);
declare variable nrwersji integer;
declare variable inwentaref integer;
begin
  select w.ktm, w.nrwersji
    from wersje w
    where w.ref = :vers
    into :ktm, :nrwersji;
   select first 1 i.ref
     from inwentap i
     where i.inwenta = :inwenta
       and i.wersjaref = :vers
     into :inwentaref;
     if (inwentaref is null) then begin
       insert into INWENTAP(INWENTA,KTM, WERSJAREF,CENA,DOSTAWA,ILINW,STATUS,
           BLOKADANALICZ)
         values(:inwenta, :ktm, :vers, 0, null, :quantity, 0, 1);
     end else begin
       update inwentap i set i.ilinw = i.ilinw + :quantity
         where i.inwenta = :inwenta and i.wersjaref = :vers;
     end
end^
SET TERM ; ^
