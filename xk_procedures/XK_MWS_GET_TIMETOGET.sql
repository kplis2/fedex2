--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_TIMETOGET(
      WHAREALOGB integer,
      WHAREALOGE integer,
      WHAREALOGP integer,
      WHAREALOGL integer,
      MWSORDTYPE integer,
      MWSORD integer,
      MWSACCESSORY integer,
      MWSACTNUMBER integer,
      PRIORITY smallint,
      WH varchar(3) CHARACTER SET UTF8                           ,
      REC smallint,
      STOCKTAKING smallint)
  returns (
      TIMETOGET double precision,
      DIST numeric(14,4))
   as
declare variable pom double precision;
declare variable sec double precision;
declare variable whareab integer;
declare variable whareae integer;
declare variable maxnumber integer;
declare variable speed double precision;
declare variable timetogettym double precision;
declare variable disttym numeric(14,4);
declare variable addway smallint;
begin
  timetoget = 0;
  addway = 0;
  whareab = null;
  whareae = null;
  dist = 0;
  pom = cast(86400 as double precision);
  sec = cast(1/pom as double precision);
  -- znalezienie lokacji pomiedzy ktorymi nalezy sie przemiescic
  -- sprawdzenie czy pierwsza operacja
  if (not exists(select ref from mwsacts where mwsord = :mwsord and number <= :mwsactnumber
      and status <> 6 and status <> 4 and status <> 3)) then
  begin
    -- pobranie wozka
    timetoget = timetoget + 10 * sec;
    whareab = wharealogb;
    if (rec = 1) then
     whareae = wharealogl;
    else
     whareae = wharealogp;
    addway = 1;
  end
  -- sprawdzenie czy ostatnia operacja
/*  else if (not exists (select ref from mwsacts where mwsord = :mwsord and number > :mwsactnumber
      and status <> 6 and status <> 4 and status <> 3)) then
  begin
    -- czasy pobrania etykiet kontrolnych
    timetoget = timetoget + 5 * sec;
    whareae = wharealoge;
    whareab = wharealogl;
    addway = 1;
  end    */
  -- w przeciwnym wypadku biezemy poprzednia operacje
  else
  begin
    addway = 0;
    select max(number)
      from mwsacts
      where mwsord = :mwsord and number <= :mwsactnumber
        and status <> 6 and status <> 4 and status <> 3
      into maxnumber;
    if (maxnumber is not null) then
    begin
      select wharealogp, wharealogl
        from mwsacts
        where mwsord = :mwsord and number = :maxnumber
          and status <> 6 and status <> 4 and status <> 3
        into wharealogb, wharealoge;
      if (rec = 1) then
      begin
        whareae = wharealogb;
        whareab = wharealoge;
      end else
      begin
        whareab = wharealogb;
        whareae = wharealoge;
      end
    end
  end
  -- obliczenie dystansu do pokonania aby rozpoczac wykonywanie operacji
  if (addway = 1 or rec = 1) then
    execute procedure mws_check_areasdist(whareab, whareae)
      returning_values(dist);
  -- teraz trzeba okreslic droge do pokonania w ramach operacji
  execute procedure mws_check_areasdist(wharealogb, wharealogl)
    returning_values(disttym);
  if (disttym is not null and dist is not null) then
    dist = dist + disttym;
  -- obliczenie czasu pokonania drogi
  if (dist > 0 and mwsaccessory is not null) then
  begin
    select ridespeed
      from mwsaccessories
      where ref = :mwsaccessory
      into speed;
    if (speed is null) then
      exception MWSACCESSOTRY_SPEED_NOT_SET;
  end else if (dist > 0 and mwsaccessory is null) then
    speed = 1.5; -- pretkosc czlowieka bez urzadzen
  -- obliczenie czasu pokonania drogi
  if (speed > 0 and dist > 0) then
  begin                 -- dzielimy bo magazyn jest w dokladnosci do centymetra !!!!
    timetogettym = dist / 100 / speed;
    timetoget = timetoget + timetogettym * sec;
  end
  if (timetoget is null) then timetoget = 0;
  if (dist is null) then dist = 0;
end^
SET TERM ; ^
