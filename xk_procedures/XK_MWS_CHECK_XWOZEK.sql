--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_CHECK_XWOZEK(
      MWSCONSTLOC integer,
      MWSORD integer)
   as
declare variable aref integer;
begin
  select first 1 a.ref
    from mwsacts a
    where a.mwsordtypedest in (1,8) and a.mwsconstloc = :mwsconstloc
      and a.regtime > current_date - 60
      and a.mwsord <> :mwsord
    order by a.regtime desc
    into aref;
  if (aref is not null) then
    exception mwsord_empty 'Wózek zablokowany przez innego operatora';
end^
SET TERM ; ^
