--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_PRSHEET_TREE(
      PRSHEETIN integer,
      PRSHEETUPIN integer,
      SSTART smallint)
  returns (
      PRSHHETOUT integer,
      PRSHEETUPOUT integer)
   as
declare variable subsheet integer;
begin
  prshhetout = prsheetin;
  if (sstart = 1) then
    prsheetupout = null;
  else
    prsheetupout = prsheetupin;
  suspend;
  for
    select m.subsheet
      from prshmat m
      where m.subsheet is not null and m.sheet = :prsheetin and m.stopcascade = 1
      into subsheet
  do begin
    for
      select prshhetout, prsheetupout
        from xk_prsheet_tree (:subsheet, :prsheetin, 0)
        into prshhetout, prsheetupout
    do begin
      suspend;
    end
  end
end^
SET TERM ; ^
