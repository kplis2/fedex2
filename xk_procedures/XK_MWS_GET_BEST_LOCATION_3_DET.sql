--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_BEST_LOCATION_3_DET(
      WH varchar(3) CHARACTER SET UTF8                           ,
      VERS integer,
      QUANTITY numeric(14,4),
      MODE smallint,
      WG_PARTII SMALLINT_ID = 0,
      X_PARTIASTR STRING20 = '')
  returns (
      MWSCONSTLOC integer,
      LOCVOLUME numeric(14,6),
      VOLUME numeric(14,6))
   as
declare variable goodvolume numeric(14,6);
declare variable wcategory varchar(10);
declare variable abc varchar(10);
declare variable good varchar(40);
declare variable x_partia date_id; -- XXX KBI
declare variable l numeric_14_4;
declare variable h numeric_14_4;
declare variable w numeric_14_4;

begin
  if (coalesce(x_partiastr,'') <> '') then    -- XXX KBI
    x_partia = cast(x_partiastr as date_id);  -- XXX KBI

  select w.good, w.wcategory, w.optabc
    from xk_goods_relocate w
    where w.vers = :vers and w.wh = :wh
    into good, wcategory, abc;
  if (wcategory is null) then wcategory = 'A';
  if (abc is null) then abc = 'A'; -- XXX KBI byo B zmianiem na A
  if (good is null) then
    select ktm from wersje where ref = :vers
      into good;
  for
    select s.mwsconstloc,
      (cast(c.l as numeric(14,6)) / 100),
      (cast(c.h as numeric(14,6)) / 100),
      (cast(c.w as numeric(14,6)) / 100),
      sum(s.volume)
     
      -- (cast(c.l as numeric(14,6)) / 100) * (cast(c.h as numeric(14,6)) / 100) * (cast(c.w as numeric(14,6)) / 100), sum(s.volume)
      from mwsstock s
        left join mwsconstlocs c on (c.ref = s.mwsconstloc)
      where (s.vers = :vers or :mode = 1) and s.wh = :wh and c.goodsav in (1,2)
        and c.abc = :abc
        and c.wcategory = :wcategory

        and c.act > 0 and c.locdest = 1
        and (coalesce(:wg_partii,0) = 0 or (/* c.x_row_type = 2 and*/ s.x_partia = coalesce(:x_partia,'1900-01-01')))      -- XXX KBI
      group by s.mwsconstloc, c.w, c.l, c.h
      into mwsconstloc,/* locvolume,*/:l, :h,  :w,  volume
  do begin
    locvolume = l*h*w;
    if (volume is null) then volume = 0;
    select x.volume from xk_mws_przelicz_jednostki (:good, :vers, :quantity, 0) x
      into goodvolume;
    if (goodvolume is null) then goodvolume = 0;
    volume = volume + goodvolume;
    suspend;
  end
end^
SET TERM ; ^
