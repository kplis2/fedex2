--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_OPERSRAPS_DEFAULT(
      PRSCHEDOPER integer,
      MODE smallint)
  returns (
      ECONTRACTSDEF integer,
      ECONTRACTSPOSDEF integer,
      AMOUNT numeric(14,4))
   as
BEGIN
 ECONTRACTSDEF= -1;
 ECONTRACTSPOSDEF =-1;
 AMOUNT = -1;
END^
SET TERM ; ^
