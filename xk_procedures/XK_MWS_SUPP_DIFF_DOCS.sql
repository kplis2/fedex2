--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_SUPP_DIFF_DOCS(
      MWSORD integer,
      ILDNI integer,
      OPERATOR integer,
      DATADOK date)
   as
declare variable docposidplus integer;
declare variable docposidminus integer;
declare variable cenaminus numeric(14,2);
declare variable cenaplus numeric(14,2);
declare variable mwsactdetplus integer;
declare variable mwsactdetminus integer;
declare variable iloscplus numeric(14,4);
declare variable iloscminus numeric(14,4);
declare variable whplus varchar(3);
declare variable whminus varchar(3);
declare variable versplus integer;
declare variable versminus integer;
declare variable versnumplus integer;
declare variable versnumminus integer;
declare variable goodplus varchar(40);
declare variable goodminus varchar(40);
declare variable doctypeplus varchar(3);
declare variable doctypeminus varchar(3);
declare variable doctypeminusdef varchar(3);
declare variable dostawaminus integer;
declare variable dostawaplus integer;
declare variable i numeric(14,4);
declare variable docminus integer;
declare variable docplus integer;
declare variable rozrefplus integer;
declare variable rozrefminus integer;
declare variable rozrefplusnew integer;
declare variable rozrefminusnew integer;
declare variable docid integer;
declare variable grupacenplus integer;
declare variable supplier integer;
declare variable frommwsactdetplus integer;
declare variable cenatorozplus integer;
declare variable cenafromrozplus integer;
declare variable data date;
declare variable wh varchar(3);
declare variable doctype varchar(3);
declare variable branch varchar(10);
declare variable period varchar(6);
declare variable doctypes varchar(60);
declare variable doctypep varchar(3);
declare variable wydania smallint;
declare variable mwsdisposition smallint;
declare variable kortopoz dokumpoz_id;
declare variable iltokor quantity_mws;
declare variable maxilkor quantity_mws;
declare variable kordocid dokumnag_id;
declare variable maindocid dokumnag_id;
--XXX JO
declare variable int_zrodlo integer_id;
declare variable int_id string120;
declare variable int_symbol string255;
begin
/*
  exception test_break
    coalesce(:MWSORD,'null')||';'||
    coalesce(:ILDNI,'null')||';'||
    coalesce(:OPERATOR,'null')||';'||
    coalesce(:DATADOK,'null');
*/
  select d.ref, coalesce(d.mwsdisposition,0)
    from mwsords o
      left join dokumnag d on (d.ref = o.docid)
    where o.ref = :mwsord
    into docid, mwsdisposition;
  docid = null;
  -- szukamy typu dokumentu na minus dla typu zlecenia rozladunku
  select t.stockdocminus
    from mwsords o
      left join mwsordtypes t on (t.ref = o.mwsordtype)
    where o.ref = :mwsord
    into doctypeminusdef;
  -- najpierw szukamy pozycji z ktorymi mozemy rozliczyc PZ+ bez cen
  select case when o.docid is null then o.docgroup else o.docid end
    from mwsords o
    where o.ref = :mwsord
    into docid;
  if (docid is null) then
    exception universal 'Brak wskazania do dokumentu magazynowego na zleceniu';
  select dostawca, oddzial from dokumnag where ref = :docid
    into supplier, branch;
  for
    select r.ref, r.ktm, r.wersjaref, r.ilosc, r.dokument, r.pozycja,
        r.magazyn, r.frommwsactdet, r.doctype, r.wersja
      from MWSORDDOCDIFF r
      where r.mwsord = :mwsord and r.wydania = 0 and r.rozl = 0 and r.ilosc > 0
      into rozrefplus, goodplus, versplus, iloscplus, docplus, docposidplus,
        whplus, mwsactdetplus, doctypeplus, versnumplus
  do begin
    -- najpierw szukamy tego samego ktm z innych dokumentów PZ-
    for
      select r.ref, p.ktm, p.wersja, p.wersjaref, r.iloscl, r.cena
        from dokumnag d
          left join dokumpoz p on (p.dokument = d.ref)
          left join dokumroz r on (r.pozycja = p.ref)
        where d.frommwsord is not null and d.data >= current_date - 180
            and r.iloscl > 0 and r.cenatoroz is null and d.typ = :doctypeminusdef and d.akcept = 1
            and d.slopoz = :supplier and p.ktm = :goodplus
        order by d.data desc
        into rozrefminus, goodminus, versnumminus, versminus, iloscminus, cenaminus
    do begin
      if (iloscplus > 0 and iloscminus > 0) then
      begin
        if (iloscplus > iloscminus) then
          i = iloscminus;
        else
          i = iloscplus;
        execute procedure gen_ref('DOKUMROZ') returning_values rozrefplusnew;
        insert into MWSORDDOCDIFF(REF,MAGAZYN,DOKUMENT,POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,FROMMWSACTDET,
            mwsord, doctype, wydania,wersja,wersjaref,ktm,rozl,cenafromroz)
          values (:rozrefplusnew,:whplus, :docplus,:docposidplus,0,:i,:cenaminus,null,:mwsactdetplus,
              :mwsord,:doctypeplus,0,:versnumplus,:versplus,:goodplus,1,:rozrefminus);
        if (iloscminus > i) then
          execute procedure dokumroz_clone(:rozrefminus,:i) returning_values rozrefminusnew;
        else
          rozrefminusnew = rozrefminus;
        update MWSORDDOCDIFF x set cenatoroz = :rozrefplusnew where ref = :rozrefminus;
        update MWSORDDOCDIFF x set ilosc = ilosc - :i where ref = :rozrefplus;
        iloscplus = iloscplus - i;
      end
    end
  end
  delete from MWSORDDOCDIFF where mwsord = :mwsord and ilosc = 0;
  -- ustalenie ceny dla dokumentów PZ+, gdy nie ma odpowiednika w rozbieznosciach
  data = current_date - ildni;
  -- okreslenie dokumentow przychodowych
  doctypes = ';';
  for
    select symbol
      from defdokum
      where zewn = 1 and wydania = 0 and koryg = 0
      into doctypep
  do begin
    doctypes = doctypes||doctypep||';';
  end
  for
    select xk.ref, xk.wersjaref, xk.ktm, xk.grupacen
      from MWSORDDOCDIFF xk
      where xk.mwsord = :mwsord and xk.wydania = 0 and xk.rozl = 0
      into rozrefplus, versplus, goodplus, grupacenplus
  do begin
    -- najpierw probujemy znalezc cene z dostawy wczesniejszej dla grupy cenowej
    cenaplus = null;
    select first 1 p.wartosc/p.ilosc
      from dokumnag d
        left join dokumpoz p on (p.dokument = d.ref)
      where position(';'||d.typ||';' in :doctypes) > 0
        and d.data >= cast(:data as date) and p.ktm = :goodplus
        and p.ilosc > 0 and d.faktura is not null
      order by abs(cast(d.data as date) - cast(:data as date))
      into cenaplus;
    if (cenaplus is null) then cenaplus = 0;
    update MWSORDDOCDIFF set cena = :cenaplus, rozl = 1 where ref = :rozrefplus;
  end
  -- teraz jak juz mamy ceny dla wszystkich dokumentow to mozemy wyztawic PZ+/-
  for
    select d.dokument, d.magazyn, d.doctype, d.wydania, d.docid
      from MWSORDDOCDIFF d
      where d.mwsord = :mwsord and d.ilosc > 0
      group by d.dokument, d.magazyn, d.doctype, d.wydania, d.docid
      order by d.wydania desc
      into docid, wh, doctype, wydania, :maindocid
  do begin
    select okres from datatookres(:datadok,0,0,0,0) into :period;
    kordocid = null;
    --if (wydania = 1 ) then
    --begin
      select docid from mwsords where ref = :mwsord
        into kordocid;
    --end xxx mkd bo nie dopisywalo do pz+ dokumentu korygujacego
    --XXX JO:
    if (:maindocid is not null) then
      select int_zrodlo, int_id, int_symbol from dokumnag d
        where d.ref = :maindocid
      into :int_zrodlo, :int_id, :int_symbol;

    insert into dokumnag (ref, magazyn, mag2, okres, typ, data, operator, oddzial,
        slodef, slopoz, blockmwsords, frommwsord, mwsdone, operakcept, refk,
        x_dokzrodlowy, int_zrodlo, int_id, int_symbol)
      values (:docid, :wh, null, :period, :doctype, :datadok, :operator, :branch,
          6, :supplier, 1, :mwsord, 1, :operator, :kordocid,
          :maindocid, :int_zrodlo, :int_id, :int_symbol);
    --XXX JO: koniec
    if (wydania = 0) then
    begin
      for
        select p.ref, p.ktm, p.wersjaref, p.ilosc, p.cena, p.dostawa,
            p.frommwsactdet, p.cenatoroz, p.cenafromroz
          from MWSORDDOCDIFF p
          where p.mwsord = :mwsord and p.dokument = :docid 
          into rozrefplus, goodplus, versplus, iloscplus, cenaplus, dostawaplus,
            frommwsactdetplus, cenatorozplus, cenafromrozplus
      do begin
        execute procedure gen_ref('DOKUMPOZ') returning_values docposidplus;
        insert into dokumpoz (ref, dokument, ktm, wersjaref, ilosc, cena, dostawa)
          values (:docposidplus, :docid, :goodplus, :versplus, :iloscplus, :cenaplus, :dostawaplus);
      end
    end else
    begin
      for
        select p.ref, p.ktm, p.wersjaref, p.ilosc, p.cena, p.dostawa,
            p.frommwsactdet, p.cenatoroz, p.cenafromroz
          from MWSORDDOCDIFF p
          where p.mwsord = :mwsord and p.dokument = :docid 
          into rozrefplus, goodminus, versminus, iloscminus, cenaminus, dostawaminus,
            frommwsactdetplus, cenatorozplus, cenafromrozplus
      do begin
        while (iloscminus > 0)
        do begin
          kortopoz = null;
          maxilkor = null;
          iltokor = null;
          select first 1 p.ref, p.iloscl - coalesce((select sum(p1.ilosc) from dokumpoz p1 where p1.dokument = :docid and p1.wersjaref = :versminus and p1.kortopoz = p.ref),0)
            from mwsords o
              left join dokumnag d on d.ref = o.docid
              left join dokumpoz p on p.dokument = d.ref
            where o.ref = :mwsord and p.wersjaref = :versminus
              and p.iloscl - coalesce((select sum(p1.ilosc) from dokumpoz p1 where p1.dokument = :docid and p1.wersjaref = :versminus and p1.kortopoz = p.ref),0) > 0
            order by case when coalesce(p.dostawa,0) = :dostawaminus then 0 else 1 end
            into kortopoz, maxilkor;
          if (kortopoz is null) then
            exception universal 'Brak pozycji do skorygowania na dokumencie PZ !';
          if (iloscminus >= maxilkor) then
            iltokor = maxilkor;
          else
            iltokor = iloscminus;
          execute procedure gen_ref('DOKUMPOZ') returning_values docposidminus;
          insert into dokumpoz (ref, dokument, ktm, wersjaref, ilosc, cena, dostawa, kortopoz)
            values (:docposidminus, :docid, :goodminus, :versminus, :iloscminus, :cenaminus, :dostawaminus, :kortopoz);
          iloscminus = iloscminus - iltokor;
        end
      end
      update dokumnag set akcept = 1 where ref = :docid;
    end
  end
end^
SET TERM ; ^
