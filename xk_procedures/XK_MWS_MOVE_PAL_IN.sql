--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MOVE_PAL_IN(
      DOCID integer,
      DOCGROUP integer,
      DOCPOSID integer,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      MWSORD integer,
      MWSACT integer,
      RECALC smallint,
      MWSORDTYPE integer)
  returns (
      REFMWSORD integer)
   as
declare variable rec smallint;
declare variable wh varchar(3);
declare variable stocktaking smallint;
declare variable operator integer;
declare variable priority smallint;
declare variable branch varchar(10);
declare variable period varchar(6);
declare variable flags varchar(40);
declare variable symbol varchar(20);
declare variable cor smallint;
declare variable quantity numeric(14,4);
declare variable todisp numeric(14,4);
declare variable toinsert numeric(14,4);
declare variable good varchar(40);
declare variable vers integer;
declare variable opersettlmode smallint;
declare variable mwsconstlocp integer;
declare variable mwspallocp integer;
declare variable mwsconstlocl integer;
declare variable autoloccreate smallint;
declare variable mwspallocl integer;
declare variable whsec integer;
declare variable posflags varchar(40);
declare variable description varchar(1024);
declare variable posdescription varchar(255);
declare variable lot integer;
declare variable scaler numeric(14,4);
declare variable recramp integer;
declare variable connecttype smallint;
declare variable shippingarea varchar(3);
declare variable wharealogp integer;
declare variable wharealogl integer;
declare variable frommwsact integer;
declare variable wharea integer;
declare variable whareap integer;
declare variable whareal integer;
declare variable mwsstandlevelnumber smallint;
declare variable locfounded smallint;
declare variable skillslevel varchar(10);
declare variable mwsaccessory integer;
declare variable operatordict varchar(80);
declare variable difficulty varchar(10);
declare variable timestart timestamp;
declare variable maxnumber integer;
declare variable timestartdcl timestamp;
declare variable timestopdcl timestamp;
declare variable realtime double precision;
declare variable wharealogb integer;
declare variable dist numeric(14,4);
declare variable palgroup integer;
declare variable mwsactwhsec integer;
declare variable ispal smallint;
declare variable mwsactrec smallint;
declare variable mixedpallgroup smallint;
declare variable paltype varchar(40);
declare variable manmwsacts smallint;
declare variable mixwhsec integer;
declare variable mwsactref integer;
declare variable pall numeric(14,2);
declare variable palw numeric(14,2);
declare variable palh numeric(14,2);
declare variable maxlevel integer;
declare variable refill smallint;
declare variable palref integer;
declare variable palrefact integer;
declare variable manmwsactgr smallint;
declare variable cnt integer;
declare variable xwhafter varchar(3);
declare variable xwhafterpalloc integer;
declare variable xwhafterconstloc integer;
declare variable whafterdocsymb varchar(40);
declare variable pz_operator integer;
declare variable tool smallint;
declare variable xtools smallint;
declare variable status smallint;
declare variable whquantity numeric(14,4);
declare variable segtype smallint;
declare variable towsec integer;
declare variable grupaprod integer;
declare variable x_partia date_id;              -- XXX KBI
declare variable x_serial_no integer_id;        -- XXX KBI
declare variable x_slownik integer_id;          -- XXX KBI
declare variable in_shelves smallint_id;
begin
--exception universal  '-' ||coalesce(DOCID,'null') ||'-'||coalesce(DOCGROUP,'null') ||'-'||coalesce(DOCPOSID,'null') ||'-'||coalesce(DOCTYPE,'null') ||'-'||coalesce(MWSORD,'null') ||'-'||coalesce(MWSACT,'null');
 maxlevel = 5;
 -- sprawdzenie czy potrzeba robić rozwiezienie - dla autokompletacji wychodzimy
 if (mwsact is not null) then
 begin
   select d.grupaprod, a.x_inshelves
     from mwsacts a
       left join dokumnag d on (d.ref = a.docid)
     where a.ref = :mwsact
     into grupaprod, :in_shelves;
   if (grupaprod > 0) then
   begin
     suspend;
     exit;
   end
 end

 -- w przypadku przyjcia z rozwiezieniem w polki od razu wychodzimy
 if (in_shelves = 1) then
    begin
     suspend;
     exit;
   end


 -- Obsluga przesyniecia rozladowywanych towarów reazem z paleta.
 if (mwsact is not null) then
   select m.mwsord, m.palgroup, m.whsec, m.recdoc, t.paleta, o.manmwsacts, m.manmwsacts,
           iif(o.wh = coalesce(m.wh2,o.wh),null,m.wh2), m.mwspallocl, m.mwsconstlocl, o.operator
     from mwsacts m
       left join mwsords o on (o.ref = m.mwsord)
       join towary t on (t.ktm = m.good)
     where m.ref = :mwsact
     into mwsord, palgroup, mwsactwhsec, mwsactrec, ispal, manmwsacts, manmwsactgr,
          xwhafter, mwspallocl, :mwsconstlocl, pz_operator;
 if (ispal = 2) then ispal = 1;
 if (manmwsactgr is null) then manmwsactgr = 0;
 select p.manmwsacts, p.whsec
   from mwspallocs p
   where p.ref = :palgroup
   into manmwsacts, towsec;
 -- NAGLÓWEK ZLECENIA MAGAZYNOWEGO
 select first 1 ref from mwsordtypes where mwsortypedets = 11
   into mwsordtype;
 if (docgroup is null) then docgroup = docid;
 select stocktaking, opersettlmode, autoloccreate
   from mwsordtypes
   where ref = :mwsordtype
   into stocktaking, opersettlmode, autoloccreate;
 select rec, cor, wh, operator, description, difficulty,
     priority, branch, period, flags, symbol, shippingarea
   from mwsords
   where ref = :mwsord
   into rec, cor, wh, operator, description, difficulty,
     priority, branch, period, flags, symbol, shippingarea;
 select wharealogl from mwsacts where ref = :mwsact into wharealogb;
 if (rec = 0) then rec = 1; else rec = 0;
 select first 1 ref from whsecs where deliveryarea = 1 into :whsec;
 select first 1 o.ref
   from mwsords o
   where o.mwsordtype = :mwsordtype and o.status = 1 and o.palgroup = :palgroup
   into refmwsord;
 manmwsacts = 1;
 if (refmwsord is null) then
 begin
   -- wybranie akcesorium operatora
   if (operator is not null and operator > 0) then
     select ref from mwsaccessories where aktuoperator = :operator into :mwsaccessory;
   execute procedure gen_ref('MWSORDS') returning_values refmwsord;
   select okres from datatookres(current_date,0,0,0,0) into period;
   insert into mwsords (ref, frommwsact, mwsordtype, stocktaking, doctype, docgroup, operator, priority, description, wh, difficulty, palgroup, manmwsacts,
       regtime, timestartdecl, timestopdecl, mwsaccessory, branch, period, flags, docsymbs, cor, rec, bwharea, ewharea, status, shippingarea)
     values (:refmwsord, :mwsact, :mwsordtype, :stocktaking, 'O', :mwsord, null, 4, '', :wh, :difficulty, :palgroup, :manmwsacts,
       current_timestamp(0), :timestart, current_timestamp(0), :mwsaccessory, :branch, :period, :flags, :symbol, :cor, :rec, :wharealogb, :wharealogb, 1, :shippingarea);
 end
 -- znalezienie max. numeru operacji - kolejne zwiekszamy i jeden.
 select max(number)
   from mwsacts
   where mwsord = :refmwsord
   into maxnumber;
 if (maxnumber is null) then maxnumber = 0;
 if (segtype is null or segtype = 0) then segtype = 1;
 mwsconstlocl = null;
 palref = null;
 -- weryfikacja czy paleta jest mix
 if (:palgroup is not null) then
 begin
   select count(distinct a.vers)
     from mwsacts a
       left join towary t on (t.ktm = a.good)
     where t.paleta = 0 and a.palgroup = :palgroup
     into cnt;
   if (cnt is null) then cnt = 0;
   if (cnt > 1) then
     update mwsacts set mixedpallgroup = 1 where palgroup = :palgroup and mixedpallgroup = 0 and mwsord = :refmwsord;
 end
 mwsconstlocl = null;
 for
   select ma.ref, ma.good, ma.vers, ma.quantity, ma.flags, ma.descript, ma.lot, ma.mwspallocl, ma.mwsconstlocl,
       ma.mixedpallgroup, ma.l, ma.w, ma.h, ma.segtype,
       ma.x_partia, ma.x_serial_no, ma.x_slownik -- XXX KBI
     from mwsacts ma
       left join towary t on(t.ktm = ma.good)
     where /*ma.mwsord = :mwsord */ma.mwsordtypedest = 2
       and ma.ref = :mwsact
       and ma.status = 5
      into frommwsact, good, vers, quantity, posflags, posdescription, lot, mwspallocp, mwsconstlocp,
         mixedpallgroup, pall, palw, palh, segtype,
         :x_partia, :x_serial_no, :x_slownik -- XXX KBI
 do begin
   if (pall is null) then pall = 90;
   if (palw is null) then palw = 130;
   if (palh is null) then palh = 180;
   toinsert = quantity;
   mwsconstlocl = null;
   -- najpierw szukamy miejsca na lokacji przypiasnej do towaru
   -- szukanie lokacji pozostaje rowniez tutaj, gdyz moze sie zdazyc ze dla palet
   -- rejestrowanych recznie przy rozladunku bedzie potrzeba je wywiexc na magazyn i policzyc to w locie
   if (palref is null and palrefact is null) then
   begin
     if (exists (select ktm from towary where ktm = :good and paleta = 1)) then
       palref = frommwsact;
   end
   if (mwsconstlocl is null and manmwsacts = 0) then
     select first 1 a.mwsconstlocl, a.whareal, a.wharealogl, m.whsec
       from mwsacts a
         join mwsconstlocs m on (m.ref = a.mwsconstlocl)
         where a.mwsord = :refmwsord and a.status < 6 and a.mwsconstlocl is not null
       into mwsconstlocl, whareal, wharealogl, mixwhsec;
   if (mwsconstlocl is null and manmwsacts = 0) then
   begin
     execute procedure XK_MWS_GET_BEST_LOCATION(:good, :vers,:frommwsact,:refmwsord,:mwsordtype, :wh, :whsec, :recramp, :maxlevel, :priority, :segtype, :mixedpallgroup,null,:paltype,:palw,:pall,:palh,1,0)
       returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
   end else if (manmwsacts > 0) then
   begin
     select first 1 s.mwsconstloc
       from mwsstock s
         left join mwsconstlocs c on (c.ref = s.mwsconstloc)
       where s.vers = :vers and c.goodsav = 1 and c.whsec = :towsec and c.locdest = 1 and c.act = 1
       order by c.symbol
       into mwsconstlocl;
     if (mwsconstlocl is null) then
       select first 1 f.ref
         from mwsfreemwsconstlocs f
         where f.whsec = :towsec
         into mwsconstlocl;
   end
   if (mwsconstlocl is not null or manmwsacts = 1) then
   begin
     if (refill = 1) then update mwsords set description = 'dokladka do lokacji !!!' where ref = :refmwsord;
     -- czas realizacji dla kazdej operacji zlecenia
     maxnumber = maxnumber + 1;
     if (mwsconstlocl = 81) then mwsconstlocl = null;
     execute procedure gen_ref('MWSACTS') returning_values mwsactref;
     if (palref is not null) then
     begin
       palrefact = mwsactref;
       palref = null;
     end
     insert into mwsacts (ref,mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
         mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
         regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc, plus,
         whareap, whareal, wharealogp, wharealogl, number, disttogo, mixedpallgroup, palgroup, refilltry, segtype,
         x_partia, x_serial_no, x_slownik) -- XXX KBI
       values (:mwsactref,:refmwsord, case when :manmwsacts = 0 then 1 else 0 end, :stocktaking, :good, :vers, :toinsert, :mwsconstlocp,  :mwspallocp,
           :mwsconstlocl, case when :mwsconstlocl is null then null else :mwspallocp end, :mwsord, :frommwsact, 'O', :opersettlmode, :autoloccreate, :wh, :wharea, :whsec,
           current_timestamp(0), :timestartdcl, :timestopdcl, :realtime, :posflags, :posdescription, :priority, :lot, :rec, 1,
           null, :whareal, null, :wharealogl, :maxnumber, :dist, :mixedpallgroup, :palgroup, 1, :segtype,
           :x_partia, :x_serial_no, :x_slownik); -- XXX KBI
   end
   -- na koniec jest komunikat ze nie ma wolnego miejsca na magazynie
   else
     exception universal 'Brak wolnej lokacji skadowej na magazynie';
 end
 --MSt: Jakims cudem dalej znajduje lokacje koncowa G1
 update mwsacts set mwsconstlocl = null where mwsord = :refmwsord and mwsconstlocl = 81;
 suspend;
end^
SET TERM ; ^
