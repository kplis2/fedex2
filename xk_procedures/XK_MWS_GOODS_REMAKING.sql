--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GOODS_REMAKING(
      MWSCONSTLOC varchar(20) CHARACTER SET UTF8                           ,
      MGOODOUT varchar(40) CHARACTER SET UTF8                           ,
      MVERSOUT integer,
      MQUANTITYOUT numeric(14,4),
      MGOODIN varchar(40) CHARACTER SET UTF8                           ,
      MVERSIN integer,
      MQUANTITYIN numeric(14,4),
      AKTUOPERATOR integer,
      MWSPALLOC integer = 0,
      SREF integer = 0)
   as
declare variable constlocref integer;
declare variable wh varchar(3);
declare variable status smallint;
declare variable docs varchar(100);
declare variable error varchar(100);
declare variable oper varchar(10);
begin
  execute procedure set_global_param('AKTUOPERATOR',:aktuoperator);
  select m.ref, m.wh
    from mwsconstlocs m
    where m.symbol = :mwsconstloc
    into constlocref, wh;
  if (constlocref is not null) then
  begin
     oper = 'MPR';
     execute procedure mws_manufacture(:constlocref,'ZPL','KNO',:oper,:wh,
            :mgoodout,:mversout,:mquantityout,:mgoodin,:mversin,mquantityin,:mwspalloc,:sref)
      returning_values :status, :docs, :error;
  end
  execute procedure clear_global_params;
end^
SET TERM ; ^
