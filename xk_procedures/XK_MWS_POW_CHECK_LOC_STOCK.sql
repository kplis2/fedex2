--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_POW_CHECK_LOC_STOCK(
      WH varchar(3) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint)
   as
declare variable symbol varchar(40);
declare variable refmwsord integer;
declare variable aref integer;
declare variable lref integer;
declare variable osymbol varchar(30);
begin
  for
    select first 10 distinct m.symbol, o.symbol
      from mwsacts sa
        left join mwsconstlocs m on (sa.mwsconstlocp = m.ref)
        left join mwsords o on (o.ref = sa.mwsord)
        left join mwsords so on (so.mwsfirstconstlocpref = m.ref and so.status < 3 and so.mwsordtypedest = 15)
      where sa.mwsordtypedest in (1,8) and sa.wh = :wh
          and sa.status = 6 and sa.regtime > current_timestamp(0) - 50
      and m.act = 0 and so.ref is null
      into symbol, osymbol
  do begin
    select first 1 sa.ref
      from mwsconstlocs m
        join mwsacts sa on (sa.mwsconstlocp = m.ref and sa.mwsordtypedest in (1,8) and sa.status = 6)
        left join mwsords so on (so.mwsfirstconstlocpref = m.ref and so.status < 5)
      where m.wh = :wh and m.act = 0
        and so.ref is null and m.symbol = :symbol
        and sa.regtime > current_timestamp(0) - 10
      order by sa.ref desc
      into aref;
    select refmwsord from XK_MWS_GEN_MWSORD_LOC_STOCK(NULL, NULL, NULL, 'B', null, :aref, 1, null, NULL, NULL, :symbol, :osymbol)
      into refmwsord;
  end
  status = 1;
  suspend;
end^
SET TERM ; ^
