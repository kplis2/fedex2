--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_CHECK_MOVEORD_STATUS(
      MWSORD integer)
  returns (
      STATUS smallint)
   as
begin
  select count(*)
    from mwsacts a
    where a.mwsord = :mwsord
      and a.status < 3
      and (a.mwsconstlocp <> a.mwsconstlocl or a.mwsconstlocl is null)
    into :status;
  if (:status > 0) then
    status = 0;
  else
    status = 1;
end^
SET TERM ; ^
