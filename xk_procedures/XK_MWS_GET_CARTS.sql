--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_CARTS(
      MWSORD integer,
      MODE smallint)
  returns (
      CARTSQ smallint,
      CARTSQU smallint)
   as
declare variable multi smallint_id;
begin
  if (:mode = 0) then
  begin
    select multi from mwsords where ref = :mwsord
    into :multi;

    select count(distinct a.docid)
      from mwsacts a
      where a.mwsord = :mwsord
        and a.status > 0
        and a.status < 5
    into :cartsq;
    select count(distinct a.docid)
      from mwsacts a
      where a.mwsord = :mwsord
        and a.status > 0
        and a.status < 5
        and a.mwsconstloc is not null
    into :cartsqu;

    if (:multi in (0,2)) then
    begin
      if (:cartsq > 0) then cartsq = 1;
      if (:cartsqu > 0) then cartsqu = 1;
    end
  end
  else
  begin
    select count(distinct c.cart)
      from mwsordcartcolours c
      where c.mwsord = :mwsord
        and c.status = 0
    into :cartsq;
    cartsqu = 0;
  end
  if (:cartsq is null) then cartsq = 0;
  if (:cartsqu is null) then cartsqu = 0;
end^
SET TERM ; ^
