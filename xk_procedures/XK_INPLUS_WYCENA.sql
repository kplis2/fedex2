--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_INPLUS_WYCENA(
      INWENTAP integer,
      WERSJAREF integer,
      MAG varchar(3) CHARACTER SET UTF8                           ,
      DOSTAWA integer)
  returns (
      CENAPLUS numeric(14,4))
   as
declare variable zpartiami smallint;
begin
  if (inwentap is not null) then
    select i.zpartiami
      from inwentap p
        left join inwenta i on (i.ref = p.inwenta)
      where p.ref = :inwentap
      into zpartiami;
  select first 1 stanycen.cena from stanycen
    where stanycen.wersjaref = :wersjaref and stanycen.ilosc > 0 and stanycen.cena > 0
      and ((stanycen.dostawa = :dostawa and :zpartiami > 0) or :zpartiami = 0)
    order by stanycen.datafifo desc
    into cenaplus;
  if (cenaplus is null or cenaplus = 0) then
    select min(stanycen.cena) from stanycen
      where stanycen.wersjaref = :wersjaref and stanycen.ilosc > 0 and stanycen.magazyn = :mag
        and ((stanycen.dostawa = :dostawa and :zpartiami > 0) or :zpartiami = 0)
      into :cenaplus;
end^
SET TERM ; ^
