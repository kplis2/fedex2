--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_CHECK_AVQUANTITY(
      VERSIN integer,
      LOTIN integer,
      WHIN varchar(3) CHARACTER SET UTF8                           )
  returns (
      QUANTITY numeric(14,4))
   as
declare variable TMP numeric(14,4);
begin
  if (coalesce(:lotin,0) > 0) then
  begin
    select coalesce(sum(s.quantity - s.blocked + s.ordered),0)
      from mwsstock s
        left join mwsconstlocs c on (s.mwsconstloc = c.ref)
        left join defmagaz d on (d.symbol = c.wh)
      where c.act > 0 and c.locdest in (1,2,3,4,5,9) and d.mws = 1
        and s.vers = :versin
        and s.wh = :whin
        and s.lot = :lotin
      into quantity;
    select coalesce(sum(a.quantity),0)
      from mwsords o
        left join mwsacts a on (a.mwsord = o.ref)
      where o.mwsordtypedest = 9 and o.status in (1,2) and a.status > 0 and a.status < 6
        and a.vers = :versin
        and a.wh = :whin
        and a.lot = :lotin
      into tmp;
    quantity = quantity + tmp;
    tmp = 0;
    select coalesce(sum(case when n.mwsdisposition = 1 then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0)),0)
      from dokumpoz p
        left join towary t on (t.ktm = p.ktm)
        left join dokumnag n on (n.ref = p.dokument)
      where p.data > current_date - 60
        and p.genmwsordsafter = 1 and coalesce(p.fake,0) = 0 and coalesce(p.havefake,0) = 0
        and n.wydania = 1
        and t.usluga <> 1 and p.wersjaref = :versin and n.magazyn = :whin
        and p.dostawa = :lotin and n.frommwsord is null
        and (n.akcept in (1,9) or n.mwsdisposition = 1)
      into tmp;
    quantity = quantity - tmp;
    tmp = 0;
    select coalesce(sum(a.quantity),0)
      from mwsacts a
        left join dokumpoz p on (p.ref = a.docposid)
        left join dokumnag n on (n.ref = p.dokument)
      where a.vers = :versin and a.wh = :whin and a.mwsordtypedest in (1,8) and a.lot = :lotin
        and a.status = 0 and a.docposid > 0 and a.doctype = 'M'
        and p.ref is not null
        and (n.akcept in (1,9) or n.mwsdisposition = 1)
      into tmp;
    quantity = quantity - tmp;
  end else if (coalesce(:lotin,0) = 0) then
  begin
    select coalesce(sum(s.quantity - s.blocked + s.ordered),0)
      from mwsstock s
        left join mwsconstlocs c on (s.mwsconstloc = c.ref)
        left join defmagaz d on (d.symbol = c.wh)
      where c.act > 0 and c.locdest in (1,2,3,4,5,9) and d.mws = 1
        and s.vers = :versin
        and s.wh = :whin
      into quantity;
    select coalesce(sum(a.quantity),0)
      from mwsords o
        left join mwsacts a on (a.mwsord = o.ref)
      where o.mwsordtypedest = 9 and o.status in (1,2) and a.status > 0 and a.status < 6
        and a.vers = :versin
        and a.wh = :whin
      into tmp;
    quantity = quantity + tmp;
    tmp = 0;
    select coalesce(sum(case when n.mwsdisposition = 1 then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0)),0)
      from dokumpoz p
        left join towary t on (t.ktm = p.ktm)
        left join dokumnag n on (n.ref = p.dokument)
      where p.genmwsordsafter = 1
        and n.wydania = 1 and coalesce(p.fake,0) = 0 and coalesce(p.havefake,0) = 0
        and t.usluga <> 1 and p.wersjaref = :versin and n.magazyn = :whin
        and (n.akcept in (1,9) or n.mwsdisposition = 1)
        and n.frommwsord is null
      into tmp;
    quantity = quantity - tmp;
    tmp = 0;
    select coalesce(sum(a.quantity),0)
      from mwsacts a
        left join dokumpoz p on (p.ref = a.docposid)
        left join dokumnag n on (n.ref = p.dokument)
      where a.vers = :versin and a.wh = :whin and a.mwsordtypedest in (1,8)
        and a.status = 0 and a.docposid > 0 and a.doctype = 'M'
        and p.ref is not null
        and (n.akcept in (1,9) or n.mwsdisposition = 1)
      into tmp;
    quantity = quantity - tmp;
  end
end^
SET TERM ; ^
