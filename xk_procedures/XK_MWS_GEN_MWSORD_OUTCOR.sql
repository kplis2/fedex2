--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GEN_MWSORD_OUTCOR(
      DOCID integer,
      DOCGROUP integer,
      DOCPOSID integer,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      MWSORD integer,
      MWSACT integer,
      RECALC smallint,
      MWSORDTYPE integer)
  returns (
      REFMWSORD integer)
   as
declare variable REC smallint;
declare variable WH varchar(3);
declare variable STOCKTAKING smallint;
declare variable OPERATOR integer;
declare variable PRIORITY smallint;
declare variable BRANCH varchar(10);
declare variable PERIOD varchar(6);
declare variable FLAGS varchar(40);
declare variable SYMBOL varchar(20);
declare variable COR smallint;
declare variable QUANTITY numeric(14,4);
declare variable TODISP numeric(14,4);
declare variable TOINSERT numeric(14,4);
declare variable GOOD varchar(40);
declare variable VERS integer;
declare variable OPERSETTLMODE smallint;
declare variable MWSCONSTLOCP integer;
declare variable MWSPALLOCP integer;
declare variable MWSCONSTLOCL integer;
declare variable MWSCONSTLOCLTMP integer;
declare variable AUTOLOCCREATE smallint;
declare variable MWSPALLOCL integer;
declare variable MWSPALLOCLTMP integer;
declare variable WHSEC integer;
declare variable WHAREA integer;
declare variable POSFLAGS varchar(40);
declare variable DESCRIPTION varchar(1024);
declare variable POSDESCRIPTION varchar(255);
declare variable LOT integer;
declare variable SCALER numeric(14,4);
declare variable TMPSCALER numeric(14,4);
declare variable RECRAMP integer;
declare variable CONNECTTYPE smallint;
declare variable SHIPPINGAREA varchar(3);
declare variable WHAREALOGP integer;
declare variable SKILLSLEVEL varchar(10);
declare variable MWSACCESSORY integer;
declare variable OPERATORDICT varchar(80);
declare variable DIFFICULTY varchar(10);
declare variable TIMESTART timestamp;
declare variable MAXNUMBER integer;
declare variable TIMESTARTDCL timestamp;
declare variable TIMESTOPDCL timestamp;
declare variable REALTIME double precision;
declare variable WHAREALOGB integer;
declare variable WHQUANTITY numeric(14,4);
declare variable DAYSEL numeric(14,4);
declare variable WHAREALOGL integer;
declare variable DIST numeric(14,4);
declare variable REFMWSACTPAL integer;
declare variable REFMWSACT integer;
declare variable PALTYPE varchar(20);
declare variable TMPPALTYPE varchar(20);
declare variable ISPAL smallint;
declare variable MAXEMPTPALQ integer;
declare variable PALQUANTITY numeric(14,4);
declare variable TMPISPAL smallint;
declare variable PALACTSQUANTITY numeric(14,4);
declare variable STATUS smallint;
declare variable WHSECOUT integer;
declare variable PALTYPEVERS integer;
declare variable PLANWHAREALOGL integer;
declare variable PLANMWSCONSTLOCL integer;
declare variable PLANWHAREAL integer;
declare variable PALPACKMETHSHORT varchar(30);
declare variable PALPACKMETH varchar(40);
declare variable PALPACKQUANTITY numeric(14,4);
declare variable MAXH numeric(14,4);
declare variable MAXWEIGHT numeric(14,4);
declare variable MAXSCALER numeric(14,4);
declare variable DOCOBJ numeric(14,4);
declare variable MAXMIXEDPALLGOODSS varchar(100);
declare variable MAXMIXEDPALLGOODS integer;
declare variable CNT integer;
declare variable MIXCNT integer;
declare variable KEYB varchar(100);
declare variable KEYC varchar(100);
declare variable KEYD varchar(100);
declare variable REFMWSACTPALMIX integer;
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable SLOKOD varchar(40);
declare variable MAXPALOBJ numeric(14,4);
declare variable REFILL smallint;
declare variable DOCTYPENAG varchar(3);
declare variable DOCTYPEPOZ varchar(3);
declare variable DOCIDPSS integer;
declare variable DOCIDPSW integer;
declare variable DOCIDPSP integer;
declare variable PREF integer;
declare variable DOKSPED integer;
declare variable SYMBOLSPED varchar(300);
declare variable FLAGI varchar(40);
declare variable CENACEN numeric(14,4);
declare variable CENANET numeric(14,4);
declare variable CENABRU numeric(14,4);
declare variable SYMBFAK varchar(40);
declare variable PLATNIK integer;
declare variable DOCIDPS integer;
declare variable REFK integer;
declare variable FAKTURA integer;
declare variable BREAKPS smallint;
declare variable DOCTYPEPS varchar(3);
declare variable KORTRYB smallint;
declare variable KORDOC integer;
declare variable KORIL numeric(14,4);
declare variable KORPOZ integer;
declare variable GQUANTITY numeric(14,4);
declare variable GREF integer;
declare variable GOREF integer;
declare variable OPERNAME varchar(60);
declare variable CORQUANTITY numeric(14,4);
declare variable ORDQUANTIYTY numeric(14,4);
declare variable MWSCONSTLOCBACK integer;
declare variable MWSPALLOCBACK integer;
declare variable NEWMWSACT integer;
declare variable WHAREAL integer;
declare variable DESCRIPT varchar(255);
declare variable GSTATUS smallint;
declare variable ACTQ numeric(14,4);
declare variable REFILLQ numeric(14,4);
declare variable POSQUANTITY numeric(14,4);
begin
--exception universal  'DOCID '||coalesce(DOCID, 0) || 'DOCGROUP '||coalesce(DOCGROUP, 0) ||'DOCPOZID '|| coalesce(DOCPOSID, 0) ||'DOCTYPE '||coalesce(DOCTYPE, 0) ||
 --'MWSORD '||coalesce(MWSORD, 0)
 --||'MWSACTS '||coalesce(MWSACT, 0) ||'RECALC '||coalesce(RECALC, 0) ||'MWSORTYPE '||coalesce(MWSORDTYPE, 0) ;
  if (recalc is null) then recalc = 0;
  -- NAGLÓWEK ZLECENIA MAGAZYNOWEGO
  select
    first 1 t.ref
      from mwsordtypes t
      where t.mwsortypedets = 2
      into mwsordtype;
  if (docgroup is null) then docgroup = docid;
  select stocktaking, opersettlmode, autoloccreate
    from mwsordtypes
    where ref = :mwsordtype
    into stocktaking, opersettlmode, autoloccreate;
  select defdokum.wydania, defdokum.koryg, dokumnag.magazyn, dokumnag.operator,
      dokumnag.uwagisped, dokumnag.katmag, dokumnag.spedpilne, dokumnag.oddzial,
      dokumnag.okres, dokumnag.flagi, dokumnag.symbol, dokumnag.kodsped,
      dokumnag.wharealogb, 6, dokumnag.ref, substring(dostawcy.id from 1 for 40),
      dokumnag.refk
    from dokumnag
      left join dostawcy on (dokumnag.dostawa = dostawcy.ref)
      left join defdokum on (defdokum.symbol = dokumnag.typ)
    where dokumnag.ref = :docid
    into rec, cor, wh, operator,
      description, difficulty, priority, branch,
      period, flags, symbol, shippingarea,
      wharealogb, slodef, slopoz, slokod,
      kordoc;
  mwsconstlocback = null;
  mwspallocback = null;
  execute procedure XK_MWS_GET_BEST_LOCATION(null,null,null,null,:MWSORDTYPE,:WH,null,null,null,null,null,null,null,null,null,null,null,null,null)
      returning_values (mwsconstlocback, mwspallocback, whareal, wharealogl, refill);
  if (rec = 0) then rec = 1; else rec = 0;
  execute procedure gen_ref('MWSORDS') returning_values refmwsord;
  select okres from datatookres(current_date,0,0,0,0) into period;
  insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator,
      priority, description, wh, difficulty, regtime, timestartdecl, timestopdecl,
      mwsaccessory, branch, period, flags, docsymbs, cor, rec, bwharea, ewharea,
      status, shippingarea, slodef, slopoz, slokod)
    values (:refmwsord, :mwsordtype, :stocktaking, 'M', :docgroup, :docid, null,
        :priority, :description, :wh, :difficulty, current_timestamp(0), :timestart, current_timestamp(0),
        :mwsaccessory, :branch, :period, :flags, :symbol, :cor, :rec, :wharealogb, :wharealogb,
        0, :shippingarea, :slodef, :slopoz, :slokod);
  if (refmwsord is null) then exception universal 'Brak zlecenia magazynowego';
  -- mozliwe sytuacje
  for
    select p.ref, p.iloscl - p.ilosconmwsacts, p.kortopoz,
        p.ktm, p.wersjaref, p.flagi, p.uwagi, p.dostawa
      from dokumpoz p
        join dokumnag n on(p.dokument = n.ref)
      where p.dokument = :docid
      into docposid, koril, korpoz,
        good, vers, posflags, posdescription, lot
  do begin
    -- moze nie byc ani w zleceniach ani w mwsordswaiting i trzeba odjac
    -- KBI opis do poniższego: sprawdzamy ile jest w oczekujących, ile jest na pozycjach zleceń po korekcie(dowolny status porócz anulowanych)
    -- i ile jest na dokumencie po korekcie
    select sum(g.quantity)
      from mwsgoodsrefill g
      where g.docposid = :korpoz
      into gquantity;
    if (gquantity is null) then gquantity = 0;
    select sum(a.quantityl) -- kbi nie wiem dlaczego patrzymy po quantityl a nie po quantity, przecież interesuje nas ile jest ogólnie wygenerowane a nie jakie pole po korekcie (jak to pole w ogóle jest wyliczne ???? )
      from mwsacts a
      where a.docposid = :korpoz and a.status < 6
      into ordquantiyty;
    if (ordquantiyty is null) then ordquantiyty = 0;
    select p.iloscl
      from dokumpoz p
      where p.ref = :korpoz
      into posquantity;
    if (posquantity is null) then posquantity = 0;
    -- w przypadku gdy na pozycji dokumentu ilość po korekcie jest wieksza równa od ilosci na pozycjach zleceń plus oczekujące
    -- oznacza to że nie musimy nic zwracać na magazyn, bo przecież wszystko co powinsnmy wydać (nawet z uwzgldneiniem korekty)
    -- zostao już wydane albo jeszcze powinsnmy jesze cos wiecej wydać
    if (posquantity >= ordquantiyty + gquantity and koril > 0) then
      koril = 0;
    -- w przeciwnym razie, jesli na dokumencie po korekcie jest mniej niz na zleceniach plus oczekujace oznacza to ze musimy albo wykasowac oczujace
    -- albo/i cos zwrocic na magazyn co juz zostalo wygenerowane
    -- czy trzeba kasowac oczekujace, czy kasowac pozycje, czy wygenrowac zlecenie zwrotu zalezy od sytuacji i statusu mwsact i badane jest w dalszej
    -- czesi procedury
    else if (posquantity < ordquantiyty + gquantity and koril > 0) then
      koril = (ordquantiyty + gquantity) - posquantity;
    gquantity = 0;
    -- pozycja jest niewygenerowana
    if (koril > 0) then
    begin
      for
        select g.ref, g.quantity
          from mwsgoodsrefill g
          where g.docposid = :korpoz
          into gref, gquantity
      do begin
        if (gquantity > 0) then
        begin
          if (gquantity <= koril) then
            delete from mwsgoodsrefill where ref = :gref;
          else
            update mwsgoodsrefill set quantity = quantity - :koril where ref = :gref;
        end
        koril = koril - gquantity;
        if (koril < 0) then
          koril = 0;
        if (koril = 0) then
          break;
      end
    end
    -- pozycja jest wygenerowana ale ma status < 2
    if (koril > 0) then
    begin
     for
       select a.ref, a.quantityl
         from mwsacts a
         where a.docposid = :korpoz and a.status < 2 and a.quantityl > 0
           and a.mwsordtypedest in (1,8)
         into gref, gquantity
      do begin
        if (gquantity > 0) then
        begin
          if (gquantity <= koril) then
          begin
            update mwsacts set status = 0 where ref = :gref;
            delete from mwsacts where ref = :gref;
          end
          else
          begin
            update mwsacts set status = 0 where ref = :gref;
            update mwsacts set quantity = quantity - :koril, status = 1 where ref = :gref;
          end
        end
        koril = koril - gquantity;
        if (koril < 0) then
          koril = 0;
        if (koril = 0) then
          break;
      end
    end
    -- pozycja jest szykowana
    if (koril > 0) then
    begin
     for
       select first 1 a.ref, a.quantityl, p.nazwa
         from mwsacts a
           left join mwsords o on (o.ref = a.mwsord)
           left join operator p on (p.ref = o.ref)
         where a.docposid = :korpoz and a.status = 2 and a.cortomwsact is null and a.quantityl > 0
           and a.mwsordtypedest in (1,8)
         into gref, gquantity, opername
      do begin
        if (opername is null) then opername = '';
        exception UNIVERSAl 'Operator '||opername||' jest w trakcie szykowania pozycji';
      end
    end
    -- pozycja jest naszykowana ale zlecenie ma status 2
    if (koril > 0) then
    begin
      for
        select a.ref, a.quantityl, a.mwsord, a.status
          from mwsacts a
            left join mwsords o on (o.ref = a.mwsord)
          where a.docposid = :korpoz and a.status = 5 and a.cortomwsact is null and a.quantityl > 0 and o.status < 5
          into gref, gquantity, goref, gstatus
      do begin
        if (gstatus = 5) then
        begin
          goref = null;
          select first 1 a.mwsord
            from mwsacts a
              left join mwsords o on (o.ref = a.mwsord)
              left join dokumpoz p on (p.kortopoz = a.docposid and p.dokument = :docid)
            where a.docid = :kordoc and a.cortomwsact is null and a.quantityl > 0
              and p.ref is null and o.status < 5 and o.status > 0
            into goref;
        end
        if (goref is null) then
          toinsert = 0;
        else if (gquantity >= koril) then
          toinsert = koril;
        else if (gquantity < koril) then
          toinsert = gquantity;
        else
          toinsert = 0;
        if (toinsert > 0) then
        begin
          if (mwspallocback is null) then
          begin
            select first 1 ref
              from mwspallocs
              where mwsconstloc = :mwsconstlocback
              into mwspallocback;
            if (mwspallocback is null) then
            begin
              execute procedure gen_ref('MWSPALLOCS') returning_values mwspallocback;
              insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status,
                  fromdocid, fromdocposid, fromdoctype)
              select :mwspallocback, symbol, ref, 2, 0,
                null, :docposid, 'M'
              from mwsconstlocs
              where ref = :mwsconstlocback;
            end
          end
          update mwsords set status = 1 where ref = :refmwsord and status = 0;
          select max(number)
            from mwsacts
            where mwsord = :goref
            into maxnumber;
          if (maxnumber is null) then maxnumber = 0;
          maxnumber = maxnumber + 1;
          execute procedure gen_ref('MWSACTS') returning_values newmwsact;
          insert into mwsacts (ref,mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
              mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
              regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc, plus,
              whareap, whareal, wharealogp, wharealogl, number, disttogo, frommwsact, cortomwsact, stopnextmwsacts)
            select :newmwsact, :refmwsord, 0, stocktaking, good, vers, :toinsert, mwsconstlocl, mwspallocl,
                :mwsconstlocback, :mwspallocback, :docid, :docposid, 'M', settlmode, closepalloc, :wh, :wharea, :whsec,
                current_timestamp(0), current_timestamp(0), current_timestamp(0), realtime, flags, :descript, 0, null, 1, 1,
                null, :whareal, null, :wharealogl, :maxnumber, 0, null, :gref, 1
              from mwsacts where ref = :gref;
--          update mwsacts set status = 1 where ref = :newmwsact;
          koril = koril - toinsert;
        end
        if (koril <= 0) then
          break;
      end
    end
    -- pozycja naszykowana i pozostale przypadki zalatwiamy juz po staremu
    if (koril > 0) then
    begin
      for
        select a.ref, a.quantityl
          from mwsacts a
            left join mwsords o on (o.ref = a.mwsord)
          where a.docposid = :korpoz and a.status = 5 and a.cortomwsact is null and a.quantityl > 0 and a.mwsconstlocl is null
          into gref, gquantity
      do begin
        if (gquantity >= koril) then
          toinsert = koril;
        else if (gquantity < koril) then
          toinsert = gquantity;
        else
          toinsert = 0;
        if (toinsert > 0) then
        begin
          update mwsords set status = 1 where ref = :refmwsord and status = 0;
          select max(number)
            from mwsacts
            where mwsord = :refmwsord
            into maxnumber;
          if (maxnumber is null) then maxnumber = 0;
          maxnumber = maxnumber + 1;
          execute procedure gen_ref('MWSACTS') returning_values newmwsact;
          execute procedure XK_MWS_GET_BEST_LOCATION(:good,:vers,null,null,:mwsordtype,
              :wh,null,null,null,null,null,null,null,null,null,null,null,null,null)
            returning_values(mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
          insert into mwsacts (ref,mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
              mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
              regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc, plus,
              whareap, whareal, wharealogp, wharealogl, number, disttogo, frommwsact, cortomwsact, stopnextmwsacts)
            select :newmwsact, :refmwsord, 0, stocktaking, good, vers, :toinsert, mwsconstlocl, mwspallocl,
                null, :mwspallocl, :docid, :docposid, 'M', settlmode, closepalloc, :wh, :wharea, :whsec,
                current_timestamp(0), current_timestamp(0), current_timestamp(0), realtime, flags, :descript, 0, null, 1, 1,
                null, :whareal, null, :wharealogl, :maxnumber, 0, null, :gref, 0
              from mwsacts where ref = :gref;
          cnt = 0;
          select count(ref) from mwsacts where mwsord = :refmwsord
            into cnt;
--          update mwsacts set status = 1 where ref = :newmwsact;
          koril = koril - toinsert;
        end
        if (koril <= 0) then
          break;
      end
    end
  end
  if (not exists (select ref from mwsacts where mwsord = :refmwsord)) then
    delete from mwsords where ref = :refmwsord;
  suspend;
end^
SET TERM ; ^
