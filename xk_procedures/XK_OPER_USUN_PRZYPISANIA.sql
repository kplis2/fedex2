--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_OPER_USUN_PRZYPISANIA(
      OPERREF integer)
  returns (
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable eol varchar(3);
declare variable cnt integer;
begin
eol = '
';
  msg = 'Usunięto następujące przypisania:';

  select count(stansprzed) from stanoper where operator = :operref
  into :cnt;
  if (coalesce(:cnt,0) > 0) then begin
    delete from stanoper where operator = :operref;
    msg = :msg||:eol||'Rejestry sprzedaży i/lub zakupów ('||:cnt||')';
  end

  select count(magazyn) from opermag where operator = :operref
  into :cnt;
  if (coalesce(:cnt,0) > 0) then begin
    delete from opermag where operator = :operref;
    msg = :msg||:eol||'Magazyny ('||:cnt||')';
  end

  select count(stanowisko) from rkuprstn where operator = :operref
  into :cnt;
  if (coalesce(:cnt,0) > 0) then begin
    delete from rkuprstn where operator = :operref;
    msg = :msg||:eol||'Stanowiska kasowe ('||:cnt||')';
  end

  if (:msg = 'Usunięto następujące przypisania:') then msg = 'Brak dodatkowych definicji przypisań operatora';
  suspend;
end^
SET TERM ; ^
