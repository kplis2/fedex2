--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_POW_OPTIMALIZE(
      WH varchar(3) CHARACTER SET UTF8                           ,
      MWSORDTYPE integer,
      MWSORDCNT integer,
      AAREA numeric(14,4),
      BAREA numeric(14,4))
   as
declare variable cnt integer;
declare variable whtmp varchar(3);
declare variable vers integer;
declare variable optabc char(1);
declare variable goodf varchar(40);
declare variable versf integer;
declare variable mwsconstlocsymbf varchar(40);
declare variable mwsconstlocf integer;
declare variable mwsconstlocsymbd varchar(40);
declare variable mwsconstlocd integer;
declare variable mwsconstlocsymbb varchar(40);
declare variable mwsconstlocb integer;
declare variable goodd varchar(40);
declare variable versd integer;
declare variable emwsconstlocsymbf varchar(40);
declare variable emwsconstlocf integer;
declare variable emwsconstlocsymbd varchar(40);
declare variable emwsconstlocd integer;
declare variable l numeric(14,4);
declare variable h numeric(14,4);
declare variable deep smallint;
declare variable maxdist numeric(14,2);
declare variable maxdistb numeric(14,2);
declare variable maxdistc numeric(14,2);
declare variable maxcpos numeric(14,2);
declare variable maxbpos numeric(14,2);
declare variable poscnt numeric(14,2);
declare variable freedeep smallint;
declare variable upmwsconstlocsymb varchar(40);
declare variable wharealog integer;
declare variable wharea integer;
declare variable priority smallint;
declare variable genmwsord integer;
declare variable przerwa varchar(100);
declare variable pod varchar(100);
declare variable pdo varchar(100);
declare variable tod timestamp;
declare variable tdo timestamp;
declare variable configonoffs varchar(100);
declare variable wcategory varchar(10);
declare variable whareastart integer;
declare variable quantity numeric(14,4);
declare variable good varchar(40);
declare variable refill smallint;
declare variable emwspalloc integer;
begin
  execute procedure get_config('MWSPOWOPTIMALIZE',2) returning_values configonoffs;
--  if (configonoffs = '0') then
  --  exit;
  execute procedure get_config('MWSSERWERPOWPRZERWA',2)
    returning_values przerwa;
  pod = substring(przerwa from 1 for 2);
  pdo = substring(przerwa from 4 for 2);
  tod = cast(current_date || ' ' || pod || ':00:00' as timestamp);
  tdo = cast(current_date || ' ' || pdo || ':00:00' as timestamp);
  if (current_timestamp(0) > tod and current_timestamp(0) < tdo) then
    exit;
  cnt = 0;
  priority = 2;
  select whareastart
    from defmagaz where symbol = :wh
    into whareastart;
  select count(o.ref)
    from mwsords o
    where o.status > 0 and o.status < 3 and o.mwsordtype = :mwsordtype and o.regtime > current_date - 14
      and o.slodef = 333
    into cnt;
  if (cnt is null) then cnt = 0;
  if (cnt >= mwsordcnt) then
    exit;
  select max(xk.poscnt)
    from xk_goods_relocate xk
    where xk.ccategory = 'C'
    into maxcpos;
  if (maxcpos is null or maxcpos = 0) then maxcpos = 1;
  select max(xk.poscnt)
    from xk_goods_relocate xk
    where xk.ccategory = 'B'
    into maxbpos;
  if (maxbpos is null or maxbpos = 0) then maxbpos = 1;
  select max(c.distfromstartarea)
    from mwsconstlocs c
    where c.abc = 'C' and c.goodsav = 1
    into maxdistc;
  if (maxdistc is null) then maxdistc = 0;
  select max(c.distfromstartarea)
    from mwsconstlocs c
    where c.abc = 'B' and c.goodsav = 1
    into maxdistb;
  if (maxdistb is null) then maxdistb = 0;
  /*for
    select first 1 cast(xk.poscnt as numeric(14,2)), xk.optabc, xk.wh, xk.good, xk.vers, xk.actmwsconstlocs, xk.actmwsconstloc,
        s.mwsconstlocsymbol, s.mwsconstloc, cd.symbol, cd.ref, cf.l, cf.h,
        md.good, md.vers, xk.wcategory
      from xk_goods_relocate xk
        left join mwsconstlocsymbs s on (((s.symbol = xk.good and s.vers is null) or s.vers = xk.vers) and xk.bestmwsconstloc = s.mwsconstloc)
        left join mwsconstlocs cf on (s.mwsconstloc = cf.ref)
        left join mwsconstlocacces a on (a.mwsconstlocblock = xk.actmwsconstloc)
        left join mwsconstlocs cd on (cd.ref = a.mwsconstloc)
        left join mwsstock md on (md.mwsconstloc = a.mwsconstloc and md.ispal <> 1)
        left join mwsstock mf on (mf.mwsconstloc = xk.actmwsconstloc and mf.ispal <> 1)
        left join whsecrows w on (w.ref = cf.whsecrow)
      where xk.actabc <> xk.optabc and (s.mwsconstloc is null or s.const = 0)
        and xk.shortage = 0 and xk.actabc < xk.optabc and xk.wh = :wh
        and xk.opt = 0 and xk.actmwsconstloc is not null and w.nogoodassoc = 0
      group by a.mwsconstloc, xk.poscnt, xk.wh, xk.good, xk.vers, xk.actmwsconstlocs, xk.actmwsconstloc,
        s.mwsconstlocsymbol, s.mwsconstloc, cd.symbol, cd.ref, cf.l, cf.h,
        md.good, md.vers, xk.actabc, xk.optabc, xk.wcategory
      having ((sum(md.quantity) > 0 and sum(md.blocked) = 0) or sum(md.quantity) is null)
        and (sum(mf.quantity) > 0 and sum(mf.blocked) = 0)
      order by case when a.mwsconstloc is null then 'B' else xk.actabc end, xk.poscnt
      into poscnt, optabc, whtmp, goodf, versf, mwsconstlocsymbf, mwsconstlocf,
        mwsconstlocsymbb, mwsconstlocb, mwsconstlocsymbd, mwsconstlocd, l, h,
        goodd, versd, wcategory
  do begin
    vers = versf;
    -- jezeli przypisanie jest juz nieaktualne to nie optymalizujemy towaru
    if (not exists (select ref from mwsstock where mwsconstloc = :mwsconstlocf and vers = :vers)) then
    begin
      update xk_goods_relocate set opt = 1 where wh = :wh and vers = :vers;
      break;
    end
    if (h is null) then h = 200;
    if (l is null) then l = 90;
    deep = 0;
    -- jezeli w deep stoi to samo to szukamy wolnej lokacji front i deep
    if (versf = versd and versd is not null and versd is not null) then
      deep = 1;
    select first 1 cf.ref, cf.symbol, cd.ref, cd.symbol,
        case when sd.vers is null and a.mwsconstloc is not null then 1 else 0 end
      from mwsconstlocs cf
        left join whsecs w on (w.ref = cf.whsec)
        left join mwsconstlocacces a on (a.mwsconstlocblock = cf.ref)
        left join mwsconstlocs cd on (cd.ref = a.mwsconstloc)
        left join mwsstock sf on (sf.mwsconstloc = cf.ref and sf.ispal <> 1)
        left join mwsstock sd on (sd.mwsconstloc = a.mwsconstloc and sd.ispal <> 1)
      where (cd.act > 0 or cd.ref is null) and cf.act > 0 and cf.deep = 0
        and cf.goodsav = 1 and cf.wcategory = :wcategory
        and (:optabc = 'C' or :optabc = 'B') and cf.abc = :optabc
        and w.deliveryarea = 2 and cf.l = :l and cf.h = :h
        and sf.ref is null and (sd.ref is null or (sd.vers = :versf and :deep = 0))
        and ((:deep = 1 and a.mwsconstloc is not null and sd.vers is null) or :deep = 0)
        and cf.wh = :whtmp
      order by
        abs(cf.distfromstartarea
            - (1.00 - :poscnt/case when 'C' = 'C' then :maxcpos else :maxbpos end)
          * (case when 'C' = 'C' then :maxdistc else :maxdistb end))
          - case when sd.vers = :versf then 10000 else 0 end
      into emwsconstlocf, emwsconstlocsymbf, emwsconstlocd, emwsconstlocsymbd,
        freedeep;
    if (emwsconstlocf is null and deep = 0 and versf is not null) then
    begin
      -- jak nie ma miejsca podwojnego to szukamy pojedynczego, jezeli mamy tylko jedna palete
      -- najpierw probujemy dostawic gdzies na front
      select first 1 cf.ref, cf.symbol
        from mwsconstlocs cf
          left join whsecs w on (w.ref = cf.whsec)
          left join mwsconstlocacces a on (a.mwsconstlocblock = cf.ref)
          left join mwsconstlocs cd on (cd.ref = a.mwsconstloc)
          left join mwsstock sf on (sf.mwsconstloc = cf.ref and sf.ispal <> 1)
          left join mwsstock sd on (sd.mwsconstloc = a.mwsconstloc and sd.ispal <> 1)
        where (cd.act > 0 or cd.ref is null) and cf.act > 0 and cf.deep = 0
          and w.deliveryarea = 2 and cf.l = :l and cf.h = :h
          and cf.goodsav = 1 and cf.wcategory = :wcategory
          and (:optabc = 'C' or :optabc = 'B') and cf.abc = :optabc
          and sd.ref is not null and sf.ref is null
          and cf.wh = :whtmp
        order by
          abs(cf.distfromstartarea
              - (1.00 - :poscnt/case when 'C' = 'C' then :maxcpos else :maxbpos end)
            * (case when 'C' = 'C' then :maxdistc else :maxdistb end))
            - case when sd.vers = :versf then 10000 else 0 end
        into emwsconstlocf, emwsconstlocsymbf;
      if (emwsconstlocf is not null) then
      begin
        --SEKCJA M
        genmwsord = null;
        execute PROCEDURE MWS_MWSCONSTLOC_CHANGE(:mwsordtype,:mwsconstlocsymbf,null,:emwsconstlocsymbf,null,0,null,1,'',:priority)
            returning_values genmwsord;
        if (genmwsord is not null) then
          update mwsords set slokod = substring('XK_MWS_POW_OPTIMALIZE - M' from 1 for 40), slodef = 333
            where ref = :genmwsord;
        update xk_goods_relocate set opt = 1 where wh = :wh and vers = :versf;
        break;
      end
      if (emwsconstlocf is null) then
      begin
        -- potem probujemy dostawic gdzies do deep
        select first 1 cd.ref, cd.symbol
          from mwsconstlocs cf
            left join whsecs w on (w.ref = cf.whsec)
            left join mwsconstlocacces a on (a.mwsconstlocblock = cf.ref)
            left join mwsconstlocs cd on (cd.ref = a.mwsconstloc)
            left join mwsstock sf on (sf.mwsconstloc = cf.ref and sf.ispal <> 1)
            left join mwsstock sd on (sd.mwsconstloc = a.mwsconstloc and sd.ispal <> 1)
          where (cd.act > 0 or cd.ref is null) and cf.act > 0 and cf.deep = 0
            and w.deliveryarea = 2 and cf.l = :l and cf.h = :h
            and cf.goodsav = 1 and cf.wcategory = :wcategory
            and (:optabc = 'C' or :optabc = 'B') and cf.abc = :optabc
            and sd.ref is null and sf.ref is not null
            and cf.wh = :whtmp
          order by
            abs(cf.distfromstartarea
                - (1.00 - :poscnt/case when 'C' = 'C' then :maxcpos else :maxbpos end)
              * (case when 'C' = 'C' then :maxdistc else :maxdistb end))
              - case when sd.vers = :versf then 10000 else 0 end
          into emwsconstlocd, emwsconstlocsymbd;
        if (emwsconstlocd is not null) then
        begin
          --SEKCJA N
          genmwsord = null;
          execute PROCEDURE MWS_MWSCONSTLOC_CHANGE(:mwsordtype,:mwsconstlocsymbf,null,:emwsconstlocsymbd,null,0,null,1,'',:priority)
              returning_values genmwsord;
          if (genmwsord is not null) then
            update mwsords set slokod = substring('XK_MWS_POW_OPTIMALIZE - N' from 1 for 40), slodef = 333, description = 'Wstaw do deep - odstawiajac front'
              where ref = :genmwsord;
          update xk_goods_relocate set opt = 1 where wh = :wh and vers = :versf;
          break;
        end
      end
      emwsconstlocf = null;
    end
    if (emwsconstlocf is null) then
    begin
      update xk_goods_relocate set opt = 1 where wh = :wh and vers = :vers;
      break;
    end
    -- jezeli front i deep sa takie same to wysylamy obie palety w to samo miejsce
    if (deep = 1 and emwsconstlocd is not null) then
    begin
      --SEKCJA A
      genmwsord = null;
      execute PROCEDURE MWS_MWSCONSTLOC_CHANGE(:mwsordtype,:mwsconstlocsymbf,null,:emwsconstlocsymbd,null,0,null,1,'',1)
          returning_values genmwsord;
      if (genmwsord is not null) then
        update mwsords set slokod = substring('XK_MWS_POW_OPTIMALIZE - A' from 1 for 40), slodef = 333
          where ref = :genmwsord;
       --SEKCJA B
      genmwsord = null;
      execute PROCEDURE MWS_MWSCONSTLOC_CHANGE(:mwsordtype,:mwsconstlocsymbd,null,:emwsconstlocsymbf,null,0,null,1,'',2)
          returning_values genmwsord;
      if (genmwsord is not null) then
        update mwsords set slokod = substring('XK_MWS_POW_OPTIMALIZE - B' from 1 for 40), slodef = 333
          where ref = :genmwsord;
      update xk_goods_relocate set opt = 1 where wh = :wh and vers = :vers;
      if (not exists (select mwsconstloc from mwsconstlocsymbs where mwsconstloc = :emwsconstlocf
          and (const = 1 or avmode <> 0))
      ) then
      begin
        delete from mwsconstlocsymbs where mwsconstloc = :emwsconstlocf and const <> 1 and avmode = 0;
        select wharea from mwsconstlocs where ref = :emwsconstlocf into wharea;
        insert into mwsconstlocsymbs (mwsconstloc, avmode, symbol, vers, wharea, mwsstandlevelnumber, const)
            values (:emwsconstlocf, 0, :goodf, :versf, :wharea, 1, 0);
      end
      -- kasujemy przypisanie z lokacji poczatkowej
      delete from mwsconstlocsymbs
        where const <> 1 and goodsav in (1,2)
          and avmode = 0 and symbol = :goodf and (vers = :versf or vers is null)
          and mwsconstloc <> :emwsconstlocd and mwsconstloc <> :emwsconstlocf;
      break;
    end
    -- jezeli front jest pelny a deep pusty i nie ma lokacji z deep z tym samym towarem
    -- to szukamy palety do deep na gorze, a jak nie ma to do deep
    -- jak front jest wolny a deep pusty to praktycznie tak samo
    if (versd is not null and versf is null) then
    begin
      versf = versd;
      versd = null;
      mwsconstlocsymbf = mwsconstlocsymbd;
      mwsconstlocf = mwsconstlocd;
    end   
    if (versd is null and versf is not null) then
    begin
      if (freedeep = 0) then
      begin
        --SEKCJA C
        genmwsord = null;
        execute PROCEDURE MWS_MWSCONSTLOC_CHANGE(:mwsordtype,:mwsconstlocsymbf,null,:emwsconstlocsymbf,null,0,null,1,'',:priority)
            returning_values genmwsord;
        if (genmwsord is not null) then
          update mwsords set slokod = substring('XK_MWS_POW_OPTIMALIZE - C' from 1 for 40), slodef = 333
            where ref = :genmwsord;
      end
      else
      begin
        select wharealogfromstartarea from mwsconstlocs where ref = :mwsconstlocf
          into wharealog;
        if (wharealog is null) then wharealog = whareastart;
        -- zdejmuje towar z gory do deep
        upmwsconstlocsymb = null;
        select first 1 mc.symbol
          from mwsstock s
            left join mwsconstlocs mc on (mc.ref = s.mwsconstloc)
            left join whareas w on (w.ref = mc.wharea)
            left join whsecs ws on (ws.ref = w.whsec)
            left join whareasdists wd on ((mc.wharealogfromstartarea = wd.whareab and wd.whareae = :wharealog)
              or (mc.wharealogfromstartarea = wd.whareae and wd.whareab = :whareastart))
            left join mwsconstlocacces ma on (ma.mwsconstloc = mc.ref)
            left join mwsconstlocacces mab on (mab.mwsconstlocblock = mc.ref)
          where s.vers = :versf and s.quantity > 0 and s.blocked = 0 and mc.act > 0
            and mc.goodsav = 0 and s.wh = :wh and s.mixedpalgroup = 0
            and mc.ref is not null and w.ref is not null
            and (ws.deliveryarea = 2 or ws.deliveryarea = 6)
            and not exists(select sb.ref from mwsstock sb where sb.mwsconstloc = s.mwsconstloc and sb.blocked > 0)
          order by wd.distance + mc.coordzb + mc.deep +
            case when exists (select sb.ref from mwsstock sb where sb.ispal = 0
              and sb.mwsconstloc = ma.mwsconstlocblock and sb.quantity - sb.blocked > 0)
              then 15000 else 0 end -
            case when ma.ref is not null and not exists (select sb.ref from mwsstock sb where sb.ispal = 0
              and sb.mwsconstloc = ma.mwsconstlocblock and sb.quantity - sb.blocked > 0)
              then 5000 else 0 end -
            case when mab.ref is not null and not exists (select sbb.ref from mwsstock sbb where sbb.ispal = 0
              and sbb.mwsconstloc = mab.mwsconstloc and sbb.quantity - sbb.blocked > 0)
              then 7500 else 0 end +
            case when s.mwsstandlevelnumber = 1 and s.deep = 1 then 30000 else 0 end
          into upmwsconstlocsymb;
        if (upmwsconstlocsymb is not null) then
        begin
          --SEKCJA D
          genmwsord = null;
          execute PROCEDURE MWS_MWSCONSTLOC_CHANGE(:mwsordtype,:upmwsconstlocsymb,null,:emwsconstlocsymbd,null,0,null,1,'',1)
              returning_values genmwsord;
          if (genmwsord is not null) then
            update mwsords set slokod = substring('XK_MWS_POW_OPTIMALIZE - D' from 1 for 40), slodef = 333
              where ref = :genmwsord;
          --SEKCJA E
          genmwsord = null;
          execute PROCEDURE MWS_MWSCONSTLOC_CHANGE(:mwsordtype,:mwsconstlocsymbf,null,:emwsconstlocsymbf,null,0,null,1,'',2)
              returning_values genmwsord;
          if (genmwsord is not null) then
            update mwsords set slokod = substring('XK_MWS_POW_OPTIMALIZE - E' from 1 for 40), slodef = 333
              where ref = :genmwsord;
        end
        --jak nie ma na gorze to wstawiam do deep
        else
        begin
          --SEKCJA F
          genmwsord = null;
          execute PROCEDURE MWS_MWSCONSTLOC_CHANGE(:mwsordtype,:mwsconstlocsymbf,null,:emwsconstlocsymbd,null,0,null,1,'',:priority)
              returning_values genmwsord;
          if (genmwsord is not null) then
            update mwsords set slokod = substring('XK_MWS_POW_OPTIMALIZE - F' from 1 for 40), slodef = 333
              where ref = :genmwsord;
        end
      end
      update xk_goods_relocate set opt = 1 where wh = :wh and vers = :vers;
      -- kasujemy przypisanie z lokacji docelowej
      if (not exists (select mwsconstloc from mwsconstlocsymbs where mwsconstloc = :emwsconstlocf
          and (const = 1 or avmode <> 0))
      ) then
      begin
        delete from mwsconstlocsymbs where mwsconstloc = :emwsconstlocf and const <> 1 and avmode = 0;
        select wharea from mwsconstlocs where ref = :emwsconstlocf into wharea;
        insert into mwsconstlocsymbs (mwsconstloc, avmode, symbol, vers, wharea, mwsstandlevelnumber, const)
            values (:emwsconstlocf, 0, :goodf, :versf, :wharea, 1, 0);
      end
      -- kasujemy przypisanie z lokacji poczatkowej
      delete from mwsconstlocsymbs
        where const <> 1 and goodsav in (1,2)
          and avmode = 0 and symbol = :goodf and (vers = :versf or vers is null)
          and mwsconstloc <> :emwsconstlocd and mwsconstloc <> :emwsconstlocf;
      break;
    end
    -- jezeli front i deep maja rozne towary to wstawiamy front tam gdzie w deep stoi to samo
    -- lub wstawiamy palete z gory do deep, lub wstawiamy do deep
    -- towar z deep dostawiamy na front do deep, do deep jak nie ma na dole lub na gore jak juz jest na dole
    if (versd is not null and versf is not null and versd <> versf) then
    begin
      -- najpierw towar z frontu
      select wharealogfromstartarea from mwsconstlocs where ref = :mwsconstlocf
        into wharealog;
      if (wharealog is null) then wharealog = whareastart;
      -- zdejmuje towar z gory do deep
      upmwsconstlocsymb = null;
      select first 1 mc.symbol
        from mwsstock s
          left join mwsconstlocs mc on (mc.ref = s.mwsconstloc)
          left join whareas w on (w.ref = mc.wharea)
          left join whsecs ws on (ws.ref = w.whsec)
          left join whareasdists wd on ((mc.wharealogfromstartarea = wd.whareab and wd.whareae = :wharealog)
            or (mc.wharealogfromstartarea = wd.whareae and wd.whareab = :whareastart))
          left join mwsconstlocacces ma on (ma.mwsconstloc = mc.ref)
          left join mwsconstlocacces mab on (mab.mwsconstlocblock = mc.ref)
        where s.vers = :versf and s.quantity > 0 and s.blocked = 0 and mc.act > 0
          and mc.goodsav = 0 and s.wh = :wh and s.mixedpalgroup = 0
          and mc.ref is not null and w.ref is not null
          and (ws.deliveryarea = 2 or ws.deliveryarea = 6)
          and not exists(select sb.ref from mwsstock sb where sb.mwsconstloc = s.mwsconstloc and sb.blocked > 0)
        order by wd.distance + mc.coordzb + mc.deep +
          case when exists (select sb.ref from mwsstock sb where sb.ispal = 0
            and sb.mwsconstloc = ma.mwsconstlocblock and sb.quantity - sb.blocked > 0)
            then 15000 else 0 end -
          case when ma.ref is not null and not exists (select sb.ref from mwsstock sb where sb.ispal = 0
            and sb.mwsconstloc = ma.mwsconstlocblock and sb.quantity - sb.blocked > 0)
            then 5000 else 0 end -
          case when mab.ref is not null and not exists (select sbb.ref from mwsstock sbb where sbb.ispal = 0
            and sbb.mwsconstloc = mab.mwsconstloc and sbb.quantity - sbb.blocked > 0)
            then 7500 else 0 end +
          case when s.deep = 1 then 30000 else 0 end
        into upmwsconstlocsymb;
      if (upmwsconstlocsymb is not null) then
      begin
        --SEKCJA G
        genmwsord = null;
        execute PROCEDURE MWS_MWSCONSTLOC_CHANGE(:mwsordtype,:upmwsconstlocsymb,null,:emwsconstlocsymbd,null,0,null,1,'',1)
            returning_values genmwsord;
        if (genmwsord is not null) then
          update mwsords set slokod = substring('XK_MWS_POW_OPTIMALIZE - G' from 1 for 40), slodef = 333
            where ref = :genmwsord;
        --SEKCJA H
        genmwsord = null;
        execute PROCEDURE MWS_MWSCONSTLOC_CHANGE(:mwsordtype,:mwsconstlocsymbf,null,:emwsconstlocsymbf,null,0,null,1,'',2)
            returning_values genmwsord;
        if (genmwsord is not null) then
          update mwsords set slokod = substring('XK_MWS_POW_OPTIMALIZE - H' from 1 for 40), slodef = 333
            where ref = :genmwsord;
      end
      --jak nie ma na gorze to wstawiam do deep
      else begin
        --SEKCJA I
        genmwsord = null;
        execute PROCEDURE MWS_MWSCONSTLOC_CHANGE(:mwsordtype,:mwsconstlocsymbf,null,:emwsconstlocsymbd,null,0,null,1,'',:priority)
            returning_values genmwsord;
        if (genmwsord is not null) then
          update mwsords set slokod = substring('XK_MWS_POW_OPTIMALIZE - I' from 1 for 40), slodef = 333
            where ref = :genmwsord;
      end
      -- kasujemy przypisanie z lokacji docelowej
      if (not exists (select mwsconstloc from mwsconstlocsymbs where mwsconstloc = :emwsconstlocf
          and (const = 1 or avmode <> 0))
      ) then
      begin
        delete from mwsconstlocsymbs where mwsconstloc = :emwsconstlocf and const <> 1 and avmode = 0;
        select wharea from mwsconstlocs where ref = :emwsconstlocf into wharea;
        insert into mwsconstlocsymbs (mwsconstloc, avmode, symbol, vers, wharea, mwsstandlevelnumber, const)
            values (:emwsconstlocf, 0, :goodf, :versf, :wharea, 1, 0);
      end
      -- kasujemy przypisanie z lokacji poczatkowej
      delete from mwsconstlocsymbs
        where const <> 1 and goodsav in (1,2)
          and avmode = 0 and symbol = :goodf and (vers = :versf or vers is null)
          and mwsconstloc <> :emwsconstlocd and mwsconstloc <> :emwsconstlocf;
      -- potem towar z deep
      upmwsconstlocsymb = null;
      emwsconstlocd = null;
      select first 1 cf.ref, cf.symbol, cd.ref, cd.symbol,
          case when sd.vers is null and a.mwsconstloc is not null then 1 else 0 end
        from mwsconstlocs cf
          left join whsecs w on (w.ref = cf.whsec)
          left join mwsconstlocacces a on (a.mwsconstlocblock = cf.ref)
          left join mwsconstlocs cd on (cd.ref = a.mwsconstloc)
          left join mwsstock sf on (sf.mwsconstloc = cf.ref and sf.ispal <> 1)
          left join mwsstock sd on (sd.mwsconstloc = a.mwsconstloc and sd.ispal <> 1)
        where (cd.act > 0 or cd.ref is null) and cf.act > 0 and cf.deep = 0
          and w.deliveryarea = 2 and cf.l = :l and cf.h = :h
          and sf.ref is null and (sd.ref is null or (sd.vers = :versd and :deep = 0))
          and ((:deep = 1 and a.mwsconstloc is not null and sd.vers is null) or :deep = 0)
          and cf.goodsav = 1 and cf.wcategory = :wcategory
          and (:optabc = 'C' or :optabc = 'B') and cf.abc = :optabc
          and sd.ref is null and sf.ref is not null
          and cf.wh = :whtmp
        order by
          abs(cf.distfromstartarea
              - (1.00 - :poscnt/case when 'C' = 'C' then :maxcpos else :maxbpos end)
            * (case when 'C' = 'C' then :maxdistc else :maxdistb end))
            - case when sd.vers = :versf then 10000 else 0 end
        into emwsconstlocf, emwsconstlocsymbf, emwsconstlocd, emwsconstlocsymbd,
          freedeep;
      if (emwsconstlocd is null) then break;
      select first 1 mc.symbol
        from mwsstock s
          left join mwsconstlocs mc on (mc.ref = s.mwsconstloc)
          left join whareas w on (w.ref = mc.wharea)
          left join whsecs ws on (ws.ref = w.whsec)
          left join whareasdists wd on ((mc.wharealogfromstartarea = wd.whareab and wd.whareae = :wharealog)
            or (mc.wharealogfromstartarea = wd.whareae and wd.whareab = :whareastart))
          left join mwsconstlocacces ma on (ma.mwsconstloc = mc.ref)
          left join mwsconstlocacces mab on (mab.mwsconstlocblock = mc.ref)
        where s.vers = :versd and s.quantity > 0 and s.blocked = 0 and mc.act > 0
          and mc.goodsav = 0 and s.wh = :wh and s.mixedpalgroup = 0
          and mc.ref is not null and w.ref is not null
          and (ws.deliveryarea = 2 or ws.deliveryarea = 6)
          and not exists(select sb.ref from mwsstock sb where sb.mwsconstloc = s.mwsconstloc and sb.blocked > 0)
        order by wd.distance + mc.coordzb + mc.deep +
          case when exists (select sb.ref from mwsstock sb where sb.ispal = 0
            and sb.mwsconstloc = ma.mwsconstlocblock and sb.quantity - sb.blocked > 0)
            then 15000 else 0 end -
          case when ma.ref is not null and not exists (select sb.ref from mwsstock sb where sb.ispal = 0
            and sb.mwsconstloc = ma.mwsconstlocblock and sb.quantity - sb.blocked > 0)
            then 5000 else 0 end -
          case when mab.ref is not null and not exists (select sbb.ref from mwsstock sbb where sbb.ispal = 0
            and sbb.mwsconstloc = mab.mwsconstloc and sbb.quantity - sbb.blocked > 0)
            then 7500 else 0 end +
          case when s.deep = 1 then 30000 else 0 end
        into upmwsconstlocsymb;
      if (upmwsconstlocsymb is not null) then
      begin
        --SEKCJA J
        genmwsord = null;
        execute PROCEDURE MWS_MWSCONSTLOC_CHANGE(:mwsordtype,:upmwsconstlocsymb,null,:emwsconstlocsymbd,null,0,null,1,'',:priority)
            returning_values genmwsord;
        if (genmwsord is not null) then
          update mwsords set slokod = substring('XK_MWS_POW_OPTIMALIZE - J' from 1 for 40), slodef = 333
            where ref = :genmwsord;
        --SEKCJA K
        genmwsord = null;
        execute PROCEDURE MWS_MWSCONSTLOC_CHANGE(:mwsordtype,:mwsconstlocsymbd,null,:emwsconstlocsymbf,null,0,null,1,'',:priority)
            returning_values genmwsord;
        if (genmwsord is not null) then
          update mwsords set slokod = substring('XK_MWS_POW_OPTIMALIZE - K' from 1 for 40), slodef = 333
            where ref = :genmwsord;
      end
      --jak nie ma na gorze to wstawiam do deep
      else
      begin
        --SEKCJA L
        genmwsord = null;
        execute PROCEDURE MWS_MWSCONSTLOC_CHANGE(:mwsordtype,:mwsconstlocsymbd,null,:emwsconstlocsymbd,null,0,null,1,'',:priority)
            returning_values genmwsord;
        if (genmwsord is not null) then
          update mwsords set slokod = substring('XK_MWS_POW_OPTIMALIZE - L' from 1 for 40), slodef = 333
            where ref = :genmwsord;
      end
      update xk_goods_relocate set opt = 1 where wh = :wh and vers = :vers;
      -- kasujemy przypisanie z lokacji docelowej
      if (not exists (select mwsconstloc from mwsconstlocsymbs where mwsconstloc = :emwsconstlocf
          and (const = 1 or avmode <> 0))
      ) then
      begin
        delete from mwsconstlocsymbs where mwsconstloc = :emwsconstlocf and const <> 1 and avmode = 0;
        select wharea from mwsconstlocs where ref = :emwsconstlocf into wharea;
        insert into mwsconstlocsymbs (mwsconstloc, avmode, symbol, vers, wharea, mwsstandlevelnumber, const)
            values (:emwsconstlocf, 0, :goodf, :versf, :wharea, 1, 0);
      end
      -- kasujemy przypisanie z lokacji poczatkowej
      delete from mwsconstlocsymbs
        where const <> 1 and goodsav in (1,2)
          and avmode = 0 and symbol = :goodf and (vers = :versf or vers is null)
          and mwsconstloc <> :emwsconstlocd and mwsconstloc <> :emwsconstlocf;
      break;
    end
    update xk_goods_relocate set opt = 1 where wh = :wh and vers = :vers;
  end    */
  for
    select first 1 x.good, x.vers, x.actmwsconstloc, x.wh, x.optabc, x.wcategory, sum(m.quantity)
      from xk_goods_relocate x
        left join mwsconstlocs c on (c.ref = x.actmwsconstloc)
        left join mwsconstlocsymbs s on (s.mwsconstloc = x.actmwsconstloc and s.vers = x.vers and s.const = 1)
        left join mwsstock m on (m.mwsconstloc = x.actmwsconstloc and m.vers = x.vers)
      where x.actabc < x.optabc and c.goodsav in (1,2) and x.opt = 0 and s.mwsconstloc is null
        and x.wh = :wh
      group by x.good, x.vers, x.actmwsconstloc, x.wh, x.optabc, x.wcategory, x.poscnt
      having sum(m.quantity) > 0 and sum(m.blocked + m.ordered) = 0
      order by x.poscnt
      into good, vers, mwsconstlocb, whtmp, optabc, wcategory, quantity
  do begin
    execute procedure xk_mws_get_best_location(:good, :vers, null, null, :mwsordtype,
        :whtmp, null, null, null, null, 0, 0, :mwsconstlocb, null, null, null, null, 1, :quantity)
      returning_values (:emwsconstlocf, :emwspalloc, :wharea, :wharealog, :refill);
    if (emwsconstlocf is not null) then
    begin
      genmwsord = null;
      execute procedure MWS_MWSCONSTLOC_CHANGE_PART(:genmwsord,:mwsordtype,:vers,null,:quantity,null,:mwsconstlocb,null,:emwsconstlocf,0,null,0,'',2)
          returning_values genmwsord;
      if (genmwsord is not null) then
      begin
        update mwsords set slokod = substring('XK_MWS_POW_OPTIMALIZE - O' from 1 for 40), slodef = 333
          where ref = :genmwsord;
      end
    end
    update xk_goods_relocate set opt = 1 where wh = :wh and vers = :vers;
  end
end^
SET TERM ; ^
