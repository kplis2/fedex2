--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_CHECK_MWSORD_LI(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      MWSCONSTLOC integer,
      INWENTA integer)
   as
declare variable symbol varchar(20);
begin
  select symbol from mwsconstlocs where ref = :mwsconstloc
  into :symbol;

  if (coalesce(mwsconstloc,0) = 0) then
    exception MWSORD_EXCEPT 'Nie wybrano lokacji początkowej.';

  if (exists(select first 1 1 from mwsords
              where mwsordtypedest = 15 and mwsfirstconstlocpref = :mwsconstloc and status > 0 and status < 5)) then
    exception MWSORD_EXCEPT 'Istnieje już inwentaryzacja na lokację: '||:symbol;

  if (coalesce(inwenta,0) = 0 and exists(
        select first 1 1 from inwenta
          where magazyn = :magazyn and zamk = 0)) then
    exception MWSORD_EXCEPT 'Nie wybrano inwentaryzacji.';
end^
SET TERM ; ^
