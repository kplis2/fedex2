--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GEN_MWSORD_WGO(
      WH varchar(3) CHARACTER SET UTF8                           )
   as
declare variable mwsordtype integer;
declare variable refmwsord integer;
declare variable dtref integer;
declare variable aref integer;
declare variable cnt integer;
declare variable maxcnt integer;
declare variable added smallint;
declare variable period varchar(6);
declare variable palgroup integer;
declare variable mwsconstloc integer;
declare variable vers integer;
begin
  maxcnt = 2;
  mwsordtype = 28;
  select first 1 o.ref, count(a.ref)
    from mwsords o
      left join mwsacts a on (a.mwsord = o.ref)
    where o.mwsordtype = :mwsordtype and o.status = 1 and o.wh = :wh
    group by o.ref
    having count(a.ref) < :maxcnt
    order by count(a.ref)
    into refmwsord, cnt;
  if (cnt is null) then cnt = 0;
  if (refmwsord is null) then
  begin
    execute procedure gen_ref('MWSORDS') returning_values refmwsord;
    select okres from datatookres(current_date,0,0,0,0) into period;
    insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator, priority,
        description, wh,  regtime, timestartdecl, branch, period,
        flags, docsymbs, cor, rec, status, shippingarea, SHIPPINGTYPE, takefullpal,
        slodef, slopoz, slokod, palquantity, repal)
      values (:refmwsord, :mwsordtype, 0, 'U', null, null, null, 1,
          '', :wh, current_timestamp(0), null, 'CENTRALA', :period,
          '', '', 0, 0, 0, null, null, 0,
          null, null, 'XK_MWS_GEN_MWSORD_WGO', null, 0);
  end
  palgroup = null;
  if (wh = 'MWS') then
    mwsconstloc = 24539;
  added = 0;
  for
    select a.ref, a.mwsord, a.vers
      from mwsords o
        left join mwsacts a on (a.mwsord = o.ref)
      where o.mwsordtype = 16 and o.status = 3
        and a.status = 1 and a.checked = 1 and o.wh = :wh and o.priority in (2)
        and (a.mwsconstlocp <> a.mwsconstlocl and a.mwsconstlocl is not null)
      into aref, dtref, vers
  do begin
    if (palgroup is null) then
    begin
      execute procedure gen_ref('MWSPALLOCS') returning_values palgroup;
      insert into mwspallocs (ref, symbol, mwsconstloc)
        values (:palgroup, :palgroup, :mwsconstloc);
    end
    update mwsacts set status = 0, mwsconstloc = mwsconstlocl where ref = :aref;
    if (exists(select first 1 1 from mwsgoodsrefill g where g.vers = :vers and g.wh = :wh)) then
    begin
      update mwsacts set mwsord = :refmwsord, mwsordtypedest = null, mwsconstlocl = :mwsconstloc, mwspallocl = :palgroup, status = 1
        where ref = :aref;
      if (added = 0) then
        added = 1;
      cnt = cnt + 1;
    end else
      delete from mwsacts where ref = :aref;
    aref = null;
    select first 1 a.ref
      from mwsords o
        left join mwsacts a on (a.mwsord = o.ref)
      where o.ref = :dtref
        and a.status = 1 and a.checked = 1 and o.wh = :wh
        and (a.mwsconstlocp <> a.mwsconstlocl and a.mwsconstlocl is not null)
      into aref;
    if (aref is null) then
    begin
      update mwsords set status = 0 where ref = :dtref;
      update mwsacts set status = 0 where mwsord = :dtref;
      delete from mwsords where ref = :dtref;
    end
    if (aref is null and cnt > maxcnt) then
      break;
  end
  if (added = 1) then
    execute procedure xk_mws_mwsord_route_opt(:refmwsord);
  if (cnt = 0) then
    delete from mwsords where ref = :refmwsord;
  else
    update mwsords set status = 1 where ref = :refmwsord;
end^
SET TERM ; ^
