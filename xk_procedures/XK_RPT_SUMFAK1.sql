--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_SUMFAK1(
      NAGFAKREF integer)
  returns (
      WARTNET numeric(14,2),
      WARTBRU numeric(14,2),
      GR_VAT varchar(20) CHARACTER SET UTF8                           ,
      WARTVAT numeric(14,2))
   as
declare variable zaliczkowy integer;
declare variable korekta integer;
declare variable zakup integer;
declare variable walutowa integer;
begin
  select nagfak.zaliczkowy, typfak.korekta, nagfak.zakup, nagfak.walutowa
  from nagfak
  left join typfak on (typfak.symbol=nagfak.typ)
  where nagfak.ref=:nagfakref
  into :zaliczkowy,:korekta, :zakup, :walutowa;
  if(:zakup=1) then walutowa = 0;
  if(:zaliczkowy=0 and :korekta=0) then begin
    /* zwykla stopka faktury */
    for select
      (case when :walutowa=1 then ROZFAK.sumwartnet else ROZFAK.sumwartnetzl end),
      (case when :walutowa=1 then ROZFAK.sumwartbru else ROZFAK.sumwartbruzl end),
      ROZFAK.VAT,
      ROZFAK.sumwartvatzl
    from ROZFAK where ROZFAK.DOKUMENT=:nagfakref
    order by ROZFAK.VAT
    into :wartnet,:wartbru,:gr_vat,:wartvat
    do begin
      suspend;
    end
  end
  else if(:zaliczkowy=0 and :korekta=1) then begin
    /* przed korekta */
    for select
      (case when :walutowa=1 then ROZFAK.psumwartnet else ROZFAK.psumwartnetzl end),
      (case when :walutowa=1 then ROZFAK.psumwartbru else ROZFAK.psumwartbruzl end),
      ROZFAK.VAT,
      ROZFAK.psumwartvatzl
    from ROZFAK where ROZFAK.DOKUMENT=:nagfakref
    order by ROZFAK.VAT
    into :wartnet,:wartbru,:gr_vat,:wartvat
    do begin
      suspend;
    end
  end
  else if(:zaliczkowy=1) then begin
    /* faktura zaliczkowa - opis zaliczki */
    for select
      (case when :walutowa=1 then ROZFAK.esumwartnet else ROZFAK.esumwartnetzl end),
      (case when :walutowa=1 then ROZFAK.esumwartbru else ROZFAK.esumwartbruzl end),
      ROZFAK.VAT,
      ROZFAK.esumwartvatzl
    from ROZFAK where ROZFAK.DOKUMENT=:nagfakref
    order by ROZFAK.VAT
    into :wartnet,:wartbru,:gr_vat,:wartvat
    do begin
      suspend;
    end
  end
  else if(:zaliczkowy=2) then begin
    /* korekta zaliczkowa - przed korekta */
    for select
      (case when :walutowa=1 then sum(nagfakzal.pwartnet) else sum(nagfakzal.pwartnetzl) end),
      (case when :walutowa=1 then sum(nagfakzal.pwartbru) else sum(nagfakzal.pwartbruzl) end),
      nagfakzal.vat,
      (sum(nagfakzal.pwartbruzl-nagfakzal.pwartnetzl))
      from nagfakzal
      where nagfakzal.faktura=:nagfakref
      group by nagfakzal.vat
      into :wartnet,:wartbru,:gr_vat,:wartvat
    do begin
      suspend;
    end
  end
end^
SET TERM ; ^
