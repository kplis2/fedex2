--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_STANYIL_DOSTEPNE(
      DOCID integer,
      WH varchar(3) CHARACTER SET UTF8                           )
  returns (
      WERSJAREF integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      ILDOST numeric(14,4))
   as
begin
  for
    select w.wersjaref, w.ktm
      from dokumpoz w
      where w.dokument = :docid
      into wersjaref, ktm
  do begin
    magazyn = wh;
    ildost = 0;
    suspend;
  end
  for
    select s.vers as wersjaref, s.good as ktm, s.wh as magazyn, sum(s.quantity - s.blocked + s.ordered)
      from dokumpoz p
        left join mwsstock s on (s.vers = p.wersjaref and s.wh = :wh)
        left join mwsconstlocs c on (c.ref = s.mwsconstloc)
      where p.dokument = :docid and c.act > 0 and c.locdest in (1,2,3,4,5,9)
      group by s.vers, s.wh, s.good
      into wersjaref, ktm, magazyn, ildost
  do begin
    suspend;
  end
  for
    select a.vers, a.good, a.wh, sum(a.quantity)
      from mwsords o
        left join mwsacts a on (a.mwsord = o.ref)
      where o.mwsordtypedest = 9 and o.status in (1,2) and a.status > 0 and a.status < 6
      group by a.vers, a.good, a.wh
      into wersjaref, ktm, magazyn, ildost
  do begin
    suspend;
  end
  for
    select p.wersjaref, p.ktm, n.magazyn, -sum(case when n.mwsdisposition = 1 then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0))
      from dokumpoz p
        left join towary t on (t.ktm = p.ktm)
        left join dokumnag n on (n.ref = p.dokument)
      where p.data > current_date - 60 and n.wydania = 1
      --  and p.iloscl > coalesce(p.ilosconmwsacts,0)
        and p.genmwsordsafter = 1
        and t.usluga <> 1
        and (n.akcept = 1 or n.mwsdisposition = 1)
      group by p.wersjaref, p.ktm, n.magazyn
      into wersjaref, ktm, magazyn, ildost
  do begin
    suspend;
  end
end^
SET TERM ; ^
