--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSORD_WHSEC_DIVIDE(
      REFMWSORDIN MWSORDS_ID)
   as
declare variable aref mwsacts_id;
declare variable groups string40;
declare variable cnt integer_id;
declare variable refmwsord mwsords_id;
declare variable maxactsonmwsord smallint_id;
declare variable maxcartonmwsord smallint_id;
declare variable maxsingledocpos smallint_id;
declare variable maxweight waga_id;
declare variable maxvolume waga_id;
declare variable mwsordtype mwsordtypes_id;
declare variable multi smallint_id;
declare variable wh defmagaz_id;
declare variable weight waga_id;
declare variable volume waga_id;
declare variable mwsord mwsords_id;
declare variable status smallint_id;
declare variable mwsconstlocp mwsconstlocs_id;
declare variable vers wersje_id;
declare variable sumweight waga_id;
declare variable sumvolume waga_id;
begin
  cnt = 0;

  select mwsordtype, status, wh, multi from mwsords where ref = :refmwsordin
  into :mwsordtype, :status, :wh, :multi;

  select coalesce(m.maxactsonmwsord,0), coalesce(m.maxcartsonmwsord,0),
      coalesce(m.maxweight,0), coalesce(m.maxvolume,0),
      coalesce(m.maxsingledocpos,0)
    from mwsordtypes4wh m
    where m.mwsordtype = :mwsordtype
      and m.wh = :wh
  into :maxactsonmwsord, :maxcartonmwsord,
    :maxweight, :maxvolume,
    :maxsingledocpos;

  for
    select distinct g.whsecgroups
      from mwsacts a
        left join mwsconstlocs c on (c.ref = a.mwsconstlocp)
        left join whsecs s on (s.ref = c.whsec)
        left join whsecgroups g on (g.whsecgroups containing ';'||s.ref||';')
        --left join whsecgroups g on (position(';'||s.ref||';' in g.whsecgroups) > 0)
      where a.mwsord = :refmwsordin
      order by g.whsecgroups
      into :groups
  do begin
    cnt = cnt + 1;
    if (cnt = 1) then
      update mwsords o set o.whsecgroups = :groups where o.ref = :refmwsordin;
    if (cnt > 1) then
    begin
      for
        select a.ref, a.mwsord, a.weight, a.volume, a.mwsconstlocp, a.vers
          from mwsacts a
            left join mwsconstlocs c on (c.ref = a.mwsconstlocp)
            left join whsecs s on (s.ref = c.whsec)
            left join whsecgroups g on (position(';'||s.ref||';' in g.whsecgroups) > 0)
          where a.mwsord = :refmwsordin and g.whsecgroups = :groups
          order by a.number
          into :aref, :mwsord, :weight, :volume, :mwsconstlocp, :vers
      do begin
        if (:weight is null) then weight = 0;
        if (:volume is null) then volume = 0;
        if (:weight is null) then weight = 0;

        --sprawdzam czy na jakims zleceniu jest juz wydanie tego samego towaru z tej samej lokacji
        select first 1 o.ref
          from mwsords o
            left join mwsacts a on (o.ref = a.mwsord)
          where o.mwsordtype = :mwsordtype
            and o.wh = :wh
            and o.status = 7
            and o.whsecgroups = :groups
            and ((:multi > 0 and o.multi = :multi) or :multi = 0)
            and o.ref <> :mwsord
            and a.mwsconstlocp = :mwsconstlocp
            and a.vers = :vers
          group by o.ref
          having (:multi > 0 and count(a.ref) + 1 <= :maxactsonmwsord or (:multi = 0))
            and (:multi > 0 and sum(a.weight) + :weight <= :maxweight or (:multi = 0))
            and (:multi > 0 and sum(a.volume) + :volume <= :maxvolume or (:multi = 0))
          order by o.ref
        into :refmwsord;

        --sprawdzam czy na jakims zleceniu jest juz wydanie z tej samej lokacji
        if (:refmwsord is null) then
          select first 1 o.ref
            from mwsords o
              left join mwsacts a on (o.ref = a.mwsord)
            where o.mwsordtype = :mwsordtype
              and o.wh = :wh
              and o.status = 7
              and o.whsecgroups = :groups
              and ((:multi > 0 and o.multi = :multi) or :multi = 0)
              and o.ref <> :mwsord
              and a.mwsconstlocp = :mwsconstlocp
            group by o.ref
            having (:multi > 0 and count(a.ref) + 1 <= :maxactsonmwsord or (:multi = 0))
              and (:multi > 0 and sum(a.weight) + :weight <= :maxweight or (:multi = 0))
              and (:multi > 0 and sum(a.volume) + :volume <= :maxvolume or (:multi = 0))
            order by o.ref
          into :refmwsord;

        if (:refmwsord is null) then
        begin
          execute procedure gen_ref('MWSORDS') returning_values :refmwsord;
          insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator, priority,
              description, wh,  regtime, timestartdecl, branch, period,
              flags, docsymbs, cor, rec, status, shippingarea, shippingtype, takefullpal,
              slodef, slopoz, slokod, palquantity, repal, whsecgroups, multi, multicnt)
            select :refmwsord, mwsordtype, stocktaking, doctype, docgroup, docid, operator, priority,
                description, wh,  current_timestamp, current_timestamp, branch, period,
                flags, docsymbs, cor, rec, :status, shippingarea, shippingtype, takefullpal,
                slodef, slopoz, slokod, palquantity, repal, :groups, :multi, 0
              from mwsords where ref = :refmwsordin;
        end

        update mwsacts a set a.status = 0 where a.ref = :aref;
        update mwsacts a set a.mwsord = :refmwsord where a.ref = :aref;
        update mwsacts a set a.status = 1 where a.ref = :aref;
      end
      refmwsord = null;
    end
  end

  select sum(a.weight), sum(a.volume)
    from mwsacts a
    where a.mwsord = :refmwsordin
  into :sumweight, :sumvolume;

  update mwsords set weight = :sumweight, volume = :sumvolume
    where ref = :refmwsordin;
end^
SET TERM ; ^
