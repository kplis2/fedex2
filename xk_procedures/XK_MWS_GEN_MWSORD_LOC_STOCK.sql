--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GEN_MWSORD_LOC_STOCK(
      DOCID integer,
      DOCGROUP integer,
      DOCPOSID integer,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      MWSORD integer,
      MWSACT integer,
      RECALC smallint,
      MWSORDTYPE integer,
      PARAM1 integer,
      PARAM2 integer,
      PARAM3 varchar(40) CHARACTER SET UTF8                           ,
      PARAM4 varchar(40) CHARACTER SET UTF8                           ,
      STOCKCALC integer = null)
  returns (
      REFMWSORD integer)
   as
declare variable PERIOD varchar(6);
declare variable STOCKTAKING smallint;
declare variable PRIORITY smallint;
declare variable WH varchar(3);
declare variable STOCKTAKINGREGTIME timestamp;
declare variable STOCKTAKINGOPERATOR integer;
declare variable STOCKTAKINGORD integer;
declare variable DESCRIPT varchar(80);
declare variable MWSORDTYPECONF varchar(80);
declare variable OUTSYMBOL varchar(30);
declare variable BRANCH varchar(30);
declare variable OPERATOR integer;
begin
  if (docid = 0) then docid = null;
  if (docgroup = 0) then docgroup = null;
  if (docposid = 0) then docposid = null;
  if (mwsord = 0) then mwsord = null;
  if (mwsact = 0) then mwsact = null;
  if (mwsordtype = 0) then mwsordtype = null;
  if (param1 = 0) then param1 = null;
  if (param4 = '') then param4 = null;
  if (stockcalc = 0) then stockcalc = null;
  refmwsord = 0;
  -- NAGLÓWEK ZLECENIA MAGAZYNOWEGO
  priority = 2;
  operator = null;
  if (mwsordtype is null) then
    select first 1 t.ref
      from mwsordtypes t where t.mwsortypedets = 15
      into mwsordtype;
  if (param1 is not null and param1 > 0) then
  begin
    execute procedure get_config('MWSREGULARSTOCKCHECK',2) returning_values mwsordtypeconf;
    mwsordtype = cast(mwsordtypeconf as integeR);
    operator = param1;
    descript = 'Inwentaryzacja losowa';
    priority = 0;
  end
  else
  begin
    if (exists(select first 1 1 from mwsords m where m.docsymbs = :param3 and m.mwsordtype = :mwsordtype and m.status < 5)) then
    begin
      if (exists (select first 1 1 from mwsords m where m.docsymbs = :param3 and m.mwsordtype = :mwsordtype and m.status < 3)) then
        exit;
      exception mws_constloc_not_exist 'Jest już złoszona inwentaryzacja na lokację '||param3;
      exit;
    end
    if (doctype = 'B') then
      descript = 'Inwentaryzacja braków';
    if (doctype = 'K') then
      descript = 'Inwentaryzacja Końcówek';
  end
  outsymbol = '';
  if (param2 is not null and param2 = 1) then
    descript = '';
  if (param4 is not null and param4 <> '') then
    outsymbol = param4;
  select ref, wh from mwsconstlocs
    where SYMBOL = :param3  --param3 symbol lokacji
    into param1, wh; --param1 ref lokacji stalej
  if (wh is not null) then
    select d.oddzial
      from defmagaz d
      where d.symbol = :wh
      into branch;
  if (mwsact is not null) then
    select a.timestop, o.operator, o.ref
      from mwsacts a
        join mwsords o on (o.ref = a.mwsord)
      where a.ref = :mwsact
      into stocktakingregtime, stocktakingoperator, stocktakingord;
  select stocktaking
    from mwsordtypes
    where ref = :mwsordtype
    into stocktaking;
  execute procedure gen_ref('MWSORDS') returning_values refmwsord;
  select okres from datatookres(current_date,0,0,0,0) into period;
  insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, operator, priority, description, wh, difficulty, frommwsord,
      regtime, timestartdecl, timestopdecl, mwsaccessory, branch, period, flags, docsymbs, cor, rec, bwharea, ewharea, status, shippingarea,
      mwsfirstconstlocp, mwsfirstconstlocpref, stocktakingregtime, stocktakingoperator, stockcalc)
    values (:refmwsord, :mwsordtype, :stocktaking, 'L',:param1, :operator, :priority, :descript, :wh, null, :stocktakingord,
      current_timestamp(0), current_timestamp(0), current_timestamp(0), null, :branch, :period,'', :param3, 0, 0, null, null, 0, null,
      substring(:param3 from 1 for 20), :param1, :stocktakingregtime, :stocktakingoperator, :stockcalc);
  update mwsords set status = 1 where ref = :refmwsord and status = 0;
  suspend;
end^
SET TERM ; ^
