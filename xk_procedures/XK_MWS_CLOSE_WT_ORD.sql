--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_CLOSE_WT_ORD(
      MWSORDREF integer,
      ENDMWSCONSTLOC varchar(20) CHARACTER SET UTF8                           )
  returns (
      STATUS integer,
      PRINTER varchar(100) CHARACTER SET UTF8                           )
   as
declare variable docgroup integer;
begin
  printer = '';
  select mo.docgroup
    from mwsords mo
    where mo.ref = :mwsordref
    into :docgroup;
  if (exists(select first 1 mo.ref
               from mwsords mo
                 join mwsacts wt on(mo.ref = wt.mwsord)
                 join mwsacts zt on(wt.ref = zt.cortomwsact)
               where mo.docgroup = :docgroup
                 and wt.mwsconstlocl is not null
                 and wt.status > 0 and wt.status < 6
                 and zt.status > 0 and zt.status < 5)) then
  begin
    status = 2;
    suspend;
    exit;
  end
  update mwsords mo set mo.status = 5, mo.emwsconstloc = :endmwsconstloc where mo.ref = :mwsordref;
  update mwsacts mA set ma.emwsconstloc = :endmwsconstloc where ma.mwsord = :mwsordref and ma.emwsconstloc is null and ma.status = 5;
  status = 1;
  suspend;
end^
SET TERM ; ^
