--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_GENEROWANIE_ZADAN_AKCJIMARK(
      DATAGEN date)
  returns (
      STATUS smallint)
   as
declare variable uczest varchar(255);
declare variable opis varchar(1024);
declare variable procedura varchar(80);
declare variable datapocz date;
declare variable ilosccykli integer;
declare variable jedncykl varchar(40);
declare variable wykonawca integer;
declare variable podmiot integer;
declare variable cykl integer;
declare variable terminzadania timestamp;
declare variable terminncykl date;
declare variable typzadania integer;
declare variable tytulzad varchar(80);
declare variable osoba integer;
declare variable zalozyciel integer;
declare variable cpodmiotskrot varchar(40);
declare variable podmiotopiekun integer;
declare variable cpodmiotslodef integer;
declare variable cpodmtelefon varchar(255);
declare variable nazwaosoby varchar(255);
declare variable telefonosoby varchar(255);
declare variable currentcomp integer;
declare variable akcjapozref integer;
declare variable proced varchar(255);
declare variable cmonth smallint;
declare variable cday smallint;
declare variable cyear smallint;
declare variable akcja integer;
begin
  if (datagen is null) then datagen = current_date;
  execute procedure get_global_param('CURRENTCOMPANY') returning_values :currentcomp;
  for select n.ref, n.operuczest, n.domopiszad, n.procedurax, n.cyklstartdata,
            n.cklilosc, n.ckljedn, n.operwyk,n.zadanie, n.operzal
    from cakcjenag n
    where n.cykliczne = 1 and n.cyklstartdata is not null and n.cklilosc is not null and n.status = 0
            and cyklstartdata = :datagen
    into :akcja, :uczest, :opis, :procedura, :datapocz, :ilosccykli, :jedncykl, :wykonawca,
        :typzadania, :zalozyciel
  do begin
    status = 0;
    terminzadania = null;
    --if (dataostoper is null) then dataostoper = datapocz;
    if (:jedncykl = 'Dni' or jedncykl = 'Tygodnie') then begin
        cykl = 1;
        if (jedncykl = 'Tygodnie') then cykl = 7;
        if (div((:datagen - datapocz) ,cykl) = 0) then begin
            terminzadania = datapocz + (cykl * ilosccykli);
        end
    end else if (:jedncykl = 'Miesiące' or :jedncykl = 'Lata') then begin
        if (:jedncykl = 'Miesiące') then
            select datazm from datatookres(:datapocz,0,:ilosccykli,0,0) into :terminzadania;
        else
            select datazm from datatookres(:datapocz,:ilosccykli,0,0,0) into :terminzadania;
    end
    if (coalesce(procedura,'') <> '') then begin
       proced = 'select status from '||:procedura||'('||:akcja||','''||:datagen||''')';
       execute statement :proced into :status;
    end else
    -- generowanie jesli termin zadania okreslony
    if (terminzadania is not null) then begin
        for select p.cpodmiot, p.pkosoba, c.skrot,
                c.operopiek, c.slodef, c.telefon, o.nazwa, o.telefon, p.ref
                from cakcjepoz p
                    left join cpodmioty c on (c.ref = p.cpodmiot)
                    left join pkosoby o on (o.ref = p.pkosoba)
            where p.cakcja = :akcja
        into :podmiot ,:osoba,  :cpodmiotskrot, :podmiotopiekun,:cpodmiotslodef, :cpodmtelefon,
        :nazwaosoby, :telefonosoby, :akcjapozref
        do begin
            -- dodanie zadania na biezacy dzien jesli takiego brak
            if (not exists(select ref from zadania z where z.cakcjapoz = :akcjapozref and z.data = :terminzadania)) then
                INSERT INTO ZADANIA (  ZADANIE, DATA, DATADO, OPIS, OSOBA, STAN, OPERZAL, OPERUCZEST, CPODMIOT, CPODMSKROT, CPODMOPEROPIEK,
                                        CPODMSLODEF, CPODMTELEFON, PKOSOBYNAZWA, PKOSOBYTELEFON, PRIORYTET, COMPANY, CAKCJAPOZ, OPERWYK)
                    VALUES ( :typzadania, :datagen , :datagen, :opis, :osoba ,0 ,:zalozyciel, :uczest, :podmiot, :cpodmiotskrot, :podmiotopiekun,
                                         :cpodmiotslodef, substring(:cpodmtelefon from 1 for 60), :nazwaosoby, :telefonosoby, 0,  :currentcomp, :akcjapozref, :wykonawca);

            -- dodanie zadania na jeden cykl do przodu
            INSERT INTO ZADANIA (  ZADANIE, DATA, DATADO, OPIS, OSOBA, STAN, OPERZAL, OPERUCZEST, CPODMIOT, CPODMSKROT, CPODMOPEROPIEK,
                                    CPODMSLODEF, CPODMTELEFON, PKOSOBYNAZWA, PKOSOBYTELEFON, PRIORYTET, COMPANY, CAKCJAPOZ, OPERWYK)
                VALUES ( :typzadania, :terminzadania , :terminzadania, :opis, :osoba ,0 ,:zalozyciel, :uczest, :podmiot, :cpodmiotskrot, :podmiotopiekun,
                                     :cpodmiotslodef, substring(:cpodmtelefon from 1 for 60), :nazwaosoby, :telefonosoby, 0,  :currentcomp, :akcjapozref, :wykonawca);

        end
    end
    update cakcjenag n set n.cyklstartdata = :terminzadania where n.ref = :akcja;
    status = 1;
  end
end^
SET TERM ; ^
