--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_NAZWAIKTM_ZAM_DOST(
      POZZAMREF integer,
      KTM varchar(255) CHARACTER SET UTF8                           ,
      NAZWA varchar(1000) CHARACTER SET UTF8                           )
  returns (
      RKTM varchar(255) CHARACTER SET UTF8                           ,
      RNAZWA varchar(1000) CHARACTER SET UTF8                           )
   as
declare variable wersjaref integer;
declare variable dostawca integer;
declare variable waluta varchar(3);
declare variable oddzial varchar(10);
begin
  select p.wersjaref, n.dostawca, n.waluta,  n.oddzial
    from pozzam p join nagzam n on p.zamowienie = n.ref
    where p.ref = :pozzamref
    into :wersjaref, :dostawca, :waluta, :oddzial;

  select symbol_dost, nazwa_dost from get_dostcen(:wersjaref, :dostawca, :waluta, :oddzial,1)
  into :rktm, :rnazwa ;

  if(:rktm is null or :rktm='') then rktm = :ktm;
  if(:rnazwa is null or :rnazwa='') then rnazwa = :nazwa;
  suspend;

end^
SET TERM ; ^
