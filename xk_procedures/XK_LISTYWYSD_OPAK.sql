--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSD_OPAK(
      BARCODE STRING255,
      SPED_DOC INTEGER_ID)
  returns (
      IS_BOX SMALLINT_ID,
      OPENED INTEGER_ID,
      BOX_REF INTEGER_ID,
      IS_PALLETE SMALLINT_ID,
      X_BOX_KTM KTM_ID)
   as
declare variable BOX_OPENED smallint_id;
declare variable PALLETE varchar(1);
declare variable OPK smallint_id;
begin
-- exception universal barcode ||' '|| sped_doc;
  is_box = 0;
  opened = 0;
  pallete = substr(:barcode, 1, 1);

  if (substr(:barcode,1,3) = 'PAL') then begin
    x_box_ktm = substr(:barcode,1,6); -- LDz aby rejestrowac ktm palety
    is_pallete = 1;
    is_box = 0;
  end
  else if (substr(:barcode,1,3) = 'OPK') then
   begin
    x_box_ktm = substr(:barcode,1,6); -- LDz aby rejestrowac ktm boxa, zeby wystawic rw
    is_pallete = 0;
    is_box = 1;
  end
  else begin
    is_pallete = 0;
    is_box = 0;
  end

  if (:is_box = 1 or :is_pallete = 1) then
    select first 1 lw.ref, lw.stan from listywysdroz_opk lw
      where listwysd = :sped_doc
        and stanowisko = :barcode
    into :box_ref, :box_opened;
    
  if (box_ref is not null)
  then begin
        if (box_opened = 0) then opened = 1;
  end

/*
  if (( pallete = 'K' or pallete = 'P') and strlen(:barcode) = 2) then
  begin
    select first 1 lw.ref, lw.stan from listywysdroz_opk lw
    where listwysd = :sped_doc and stanowisko = :barcode
    into :box_ref, :box_opened;

    if (box_ref is not null)
    then begin
        if (box_opened = 0) then opened = 1;
    end
    is_box = 1;
  end
*/
  suspend;
end^
SET TERM ; ^
