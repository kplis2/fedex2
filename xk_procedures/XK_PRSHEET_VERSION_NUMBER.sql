--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_PRSHEET_VERSION_NUMBER(
      DOKREF integer,
      NAZWA varchar(40) CHARACTER SET UTF8                           ,
      REJESTR varchar(20) CHARACTER SET UTF8                           ,
      DOKUMENT varchar(20) CHARACTER SET UTF8                           ,
      DATA date,
      NUMBER integer,
      SYMBOL varchar(255) CHARACTER SET UTF8                           )
  returns (
      RSYMBOL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable wersja varchar(20);
begin
  select max (ps.symbolver)
    from prsheets ps
    where ps.ref = :dokref
    into :wersja;
    rsymbol = cast(:wersja as numeric(14,1)) + 0.1;
  suspend;
end^
SET TERM ; ^
