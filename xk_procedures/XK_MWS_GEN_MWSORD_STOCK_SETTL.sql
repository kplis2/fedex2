--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GEN_MWSORD_STOCK_SETTL(
      DOCID integer,
      DOCGROUP integer,
      DOCPOSID integer,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      MWSORD integer,
      MWSACT integer,
      RECALC smallint,
      MWSORDTYPE integer)
  returns (
      REFMWSORD integer)
   as
declare variable REC smallint;
declare variable WH varchar(3);
declare variable STOCKTAKING smallint;
declare variable OPERATOR integer;
declare variable PRIORITY smallint;
declare variable BRANCH varchar(10);
declare variable PERIOD varchar(6);
declare variable FLAGS varchar(40);
declare variable SYMBOL varchar(20);
declare variable COR smallint;
declare variable QUANTITY numeric(14,4);
declare variable QUANTITYA numeric(14,4);
declare variable GOOD varchar(40);
declare variable VERS integer;
declare variable OPERSETTLMODE smallint;
declare variable AUTOLOCCREATE smallint;
declare variable DESCRIPTION varchar(1024);
declare variable LOT integer;
declare variable SHIPPINGAREA varchar(3);
declare variable WHAREA integer;
declare variable WHAREAL integer;
declare variable MWSACCESSORY integer;
declare variable DIFFICULTY varchar(10);
declare variable TIMESTART timestamp;
declare variable MAXNUMBER integer;
declare variable TIMESTARTDCL timestamp;
declare variable TIMESTOPDCL timestamp;
declare variable REALTIME double precision;
declare variable WHAREALOGB integer;
declare variable DIST numeric(14,4);
declare variable SHIPPINGTYPE integer;
declare variable AUTOCOMMIT smallint;
declare variable PACKQUANTITY integer;
declare variable MWSCONSTLOCI integer;
declare variable MWSPALLOCLI integer;
declare variable WHAREALI integer;
declare variable WHAREALOGLI integer;
declare variable REFILLI integer;
declare variable MWSPALLOCI integer;
declare variable MWSCONSTLOCSTOCK integer;
declare variable MWSCONSTLOCSYMB varchar(20);
declare variable PARAM3 varchar(30);
declare variable QUANTITYSTOCK numeric(14,4);
declare variable TOINSERT numeric(14,4);
declare variable MWSPALLOCSTOCK integer;
declare variable FROMMWSORDTYPEDEST integer;
declare variable MWSCONSTLOC integer;
declare variable MAGBREAK smallint;
declare variable NEXTACT smallint;
declare variable QUANTITYI numeric(14,4);
declare variable LOTI integer;
declare variable LOTA integer;
declare variable ORGMWSORDTYPE integer;
declare variable STOCKCALC integer;
declare variable MWSREGULARSTOCKCHECKS varchar(100);
declare variable MWSREGULARSTOCKCHECK integer;
declare variable PALLOCINWENTA integer;
declare variable X_PARTIA DATE_ID;
declare variable X_SERIAL_NO integer;
declare variable X_SLOWNIK integer;
declare variable X_PARTIA_A DATE_ID;
declare variable X_PARTIA_I DATE_ID;
declare variable X_SERIAL_NO_A integer;
declare variable X_SERIAL_NO_I integer;
declare variable X_SLOWNIK_A integer;
declare variable X_SLOWNIK_I integer;
declare variable NULLPARTIA DATE_ID;
begin
 /* exception universal     ' DOCID '||coalesce(:docid,'brak')||' DOCGROUP '||coalesce(:DOCGROUP,'brak')||' DOCPOSID '||coalesce(:DOCPOSID,'brak')||
      ' DOCTYPE '||coalesce(:DOCTYPE,'brak')||' MWSORD '||coalesce(:MWSORD,'brak')||' MWSACT '||coalesce(:MWSACT,'brak')||
      ' RECALC '||coalesce(:RECALC,'brak')||' MWSORDTYPE '||coalesce(:MWSORDTYPE,'brak');     */


  -- nullpartia = cast('1.1.3000' as date_id);
  nullpartia = '1900-01-01';
  execute procedure get_config('MWSREGULARSTOCKCHECK',2) returning_values mwsregularstockchecks;
  mwsregularstockcheck = cast (mwsregularstockchecks as integer);
  if (recalc is null) then recalc = 0;
  -- NAGLÓWEK ZLECENIA MAGAZYNOWEGO
  --pobranie typu zlecenia magazynowego
  if (mwsordtype is null) then
    select first 1 t.ref
      from mwsordtypes t where t.mwsortypedets = 7
      into mwsordtype;
  if (docgroup is null) then docgroup = docid;
  select stocktaking, opersettlmode, autoloccreate
    from mwsordtypes
    where ref = :mwsordtype
    into stocktaking, opersettlmode, autoloccreate;
  select rec, cor, wh, operator, description, difficulty, doctype, docgroup, mwsordtype, docsymbs,
      priority, branch, period, flags, symbol, shippingarea, ewharea, SHIPPINGTYPE,
      autocommit, packquantity, mwsfirstconstlocpref, mwsfirstconstlocp, mwsordtypedest, stockcalc
    from mwsords 
    where ref = :mwsord
    into rec, cor, wh, operator, description, difficulty, doctype, docgroup, orgmwsordtype, param3,
      priority, branch, period, flags, symbol, shippingarea, wharealogb, SHIPPINGTYPE,
      autocommit, packquantity, mwsconstlocstock, mwsconstlocsymb, frommwsordtypedest, stockcalc;
  if (frommwsordtypedest = 15) then frommwsordtypedest = 0;
  else frommwsordtypedest = 1;
  if (mwsconstlocstock is null) then
    select ref from mwsconstlocs where symbol = :mwsconstlocsymb into mwsconstlocstock;
  if (autocommit is null) then autocommit = 0;
  if (packquantity is null) then packquantity = 0;
  select magbreak from defmagaz where symbol = :wh
    into magbreak;
  if (magbreak is null) then magbreak = 0;
  select okres from datatookres(current_date,0,0,0,0) into period;
  execute procedure gen_ref('MWSORDS') returning_values refmwsord;
  insert into mwsords (ref, mwsordtype, stocktaking, doctype, docid, docgroup, autocommit,
      operator, priority, description, wh, difficulty, frommwsord, packquantity,
      regtime, timestartdecl, timestopdecl, mwsaccessory, branch, period, flags,
      docsymbs, cor, rec, bwharea, ewharea, status, shippingarea, SHIPPINGTYPE)
    values (:refmwsord, :mwsordtype, :stocktaking, 'M', :docid, :docgroup, :autocommit,
      :operator, :priority, :description, :wh, :difficulty, :mwsord, :packquantity,
      current_timestamp(0), :timestart, current_timestamp(0), :mwsaccessory, :branch, :period, :flags,
      :symbol, :cor, :rec, :wharealogb, :wharealogb, 0, :shippingarea, :SHIPPINGTYPE);
    -- POZYCJE ZLECENIA MAGAZYNOWEGO
  if (recalc > 0) then
  begin
    update mwsacts set status = 0 where mwsord = :refmwsord
      and (status = 1 or status = 3 or status = 4 or status = 6);
    delete from mwsacts where mwsord = :refmwsord and status = 0;
  end
  -- znalezienie max. numeru operacji - kolejne zwiekszamy o jeden.
  select max(number) from mwsacts where mwsord = :refmwsord into maxnumber;
  if (maxnumber is null) then maxnumber = 0;
  -- okreslenie lokacji inwentaryzacyjnej
  if (mwsconstloci is null) then
    execute procedure XK_MWS_GET_BEST_LOCATION(null,null,null,:refmwsord,:mwsordtype,
        :wh, null, null, null, null, null, null,null,null,null,null,null,0,0)
      returning_values (mwsconstloci, mwspalloci, whareali, wharealogli, refilli);
  select min(ref) from mwspallocs where mwsconstloc = :mwsconstloci
    into mwspalloci;
  if (mwspalloci is null) then
  begin
    execute procedure gen_ref('MWSPALLOCS') returning_values mwspalloci;
    insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status,
        fromdocid, fromdocposid, fromdoctype)
      select :mwspalloci, symbol, ref, 2, 0,
          :docid, :docposid, :doctype
        from mwsconstlocs
        where ref = :mwsconstloci;
  end
  -- okreslecenie lokacji inwentaryzowanej
  select mwsconstlocl from mwsacts where ref = :mwsact into mwsconstloc;
  -- to co trzeba odniesc do lokacji inwentaryzacji
  for
    select s.good, s.vers, case when :magbreak = 2 then coalesce(s.lot,0) else 0 end,
           s.x_partia, s.x_serial_no, s.x_slownik, sum(s.quantity) --mkd
      from mwsstock s
      where s.mwsconstloc = :mwsconstlocstock  and s.ispal = 0
      group by s.good, s.vers, case when :magbreak = 2 then coalesce(s.lot,0) else 0 end,
                s.x_partia, s.x_serial_no, s.x_slownik  --mkd
      into good, vers, lota, :x_partia_a, :x_serial_no_a, :x_slownik_a, :quantity
  do begin
    --porownanie tego co jest faktycznie z tym co wyszlo podczas inventaryzacji
    select sum(a.quantityc)
      from mwsacts a
      where a.mwsord = :mwsord and a.vers = :vers and a.status = 5
        and ((coalesce(a.lot,0) = :lota and :magbreak = 2) or :magbreak in (0,1))
        and coalesce(a.x_partia, :nullpartia) = coalesce(:x_partia_a, :nullpartia)         --mkd
        and coalesce(a.x_serial_no, 0) = coalesce(:x_serial_no_a, 0)   --mkd
        and coalesce(a.x_slownik, 0) = coalesce(:x_slownik_a,0)       --mkd
      into :quantitya ;
    if (quantitya is null) then quantitya = 0;
    if (quantity is null) then quantity = 0;
    quantity = quantity - quantitya;
    if (quantity < 0) then quantity = 0;
    if (quantity > 0) then --jezeli na lokacji jest za duzo
    begin
     for
        select s.mwspalloc, s.lot, s.quantity , x_partia, x_serial_no , x_slownik
          from mwsstock  s
          where s.mwsconstloc = :mwsconstlocstock and s.vers = :vers
            and s.quantity > 0 and s.ispal = 0
            and ((coalesce(s.lot,0) = :lota and :magbreak = 2) or :magbreak in (0,1))
            and coalesce(s.x_partia, :nullpartia) = coalesce(:x_partia_a, :nullpartia)         --mkd
            and coalesce(s.x_serial_no, 0) = coalesce(:x_serial_no_a, 0)   --mkd
            and coalesce(s.x_slownik, 0) = coalesce(:x_slownik_a,0)       --mkd
          into mwspallocstock, lot, quantitystock, :x_partia, :x_serial_no ,:x_slownik
      do begin
        if (quantitystock is null) then
          quantitystock = 0;
        if (quantitystock > 0 and quantity > 0) then
        begin
          if (quantity >= quantitystock) then
            toinsert = quantitystock;
          else
            toinsert = quantity;
          nextact = 0;
          if (exists (select first 1 s.ref from mwsstock  s
              where s.mwsconstloc = :mwsconstloci and s.vers = :vers
                and s.quantity + s.ordered < 0 and s.ispal = 0
                and ((s.lot = :lot and :magbreak = 2) or :magbreak in (0,1))
                and coalesce(s.x_partia, :nullpartia) = coalesce(:x_partia, :nullpartia)         --mkd
                and coalesce(s.x_serial_no, 0) = coalesce(:x_serial_no,0)   --mkd
                and coalesce(s.x_slownik, 0) = coalesce(:x_slownik,0) )      --mkd
          ) then
            nextact = 1;
          while (toinsert > 0 and nextact = 1)
          do begin
            if (orgmwsordtype = mwsregularstockcheck) then
            begin
              delete from mwsords where ref = :refmwsord;
              execute procedure  XK_MWS_GEN_MWSORD_LOC_STOCK(NULL, NULL, NULL, 'B', null, null, 1, null, null, NULL, :param3, NULL)
                returning_values refmwsord;
              exit;
            end
            -- okreslamy ilosc jaka mozna skompensowac
            pallocinwenta = null;
            select first 1 s.mwspalloc, s.lot, -(s.quantity + s.ordered), s.x_partia, s.x_serial_no, s.x_slownik
              from mwsstock  s
              where s.mwsconstloc = :mwsconstloci and s.vers = :vers
                and s.quantity + s.ordered < 0 and s.ispal = 0
                and ((s.lot = :lot and :magbreak = 2) or :magbreak in (0,1))
                and coalesce(s.x_partia, :nullpartia) = coalesce(:x_partia, :nullpartia)         --mkd
                and coalesce(s.x_serial_no, 0) = coalesce(:x_serial_no, 0)   --mkd
                and coalesce(s.x_slownik, 0) = coalesce(:x_slownik,0)       --mkd
              into pallocinwenta, loti, quantityi, :x_partia_i, :x_serial_no_i, :x_slownik_i;
            if (toinsert < quantityi) then
              quantityi = toinsert;
            -- kompensujemy towar brakujacy na lokacji inwentaryzacyjnej
            maxnumber = maxnumber + 1;
            insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
                mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
                regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc, plus,
                whareap, whareal, wharealogp, wharealogl, number, disttogo, x_partia, x_serial_no, x_slownik)                                            --mkd
              values (:refmwsord, 1, :stocktaking, :good, :vers, :quantityi, :mwsconstlocstock, :mwspallocstock,
                  null, null, :docid, :docposid, 'M', :opersettlmode, :autoloccreate, :wh, :wharea, null,
                  current_timestamp(0), :timestartdcl, :timestopdcl, :realtime, null, 'Odniesienie na lok. inw.', :priority, :lot, 0, 1,
                  null, :whareal, null, null, :maxnumber, :dist,:x_partia, :x_serial_no, :x_slownik);                     --mkd
            maxnumber = maxnumber + 1;
            insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
                mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
                regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc, plus,
                whareap, whareal, wharealogp, wharealogl, number, disttogo, x_partia, x_serial_no, x_slownik)                                                --mkd
              values (:refmwsord, 1, :stocktaking, :good, :vers, :quantityi, null, null,
                  :mwsconstloci, :pallocinwenta, :docid, :docposid, 'M', :opersettlmode, :autoloccreate, :wh, :wharea, null,
                  current_timestamp(0), :timestartdcl, :timestopdcl, :realtime, null, 'Odniesienie na lok. inw.', :priority, :loti, 1, 1,
                  null, :whareal, null, null, :maxnumber, :dist, :x_partia_i, :x_serial_no_i, :x_slownik_i);                   --mkd
            toinsert = toinsert - quantityi;
            quantity = quantity - quantityi;
            if (not exists (select first 1 s.ref from mwsstock  s
                where s.mwsconstloc = :mwsconstloci and s.vers = :vers
                  and s.quantity + s.ordered < 0 and s.ispal = 0
                  and ((s.lot = :lot and :magbreak = 2) or :magbreak in (0,1))
                  and coalesce(s.x_partia, :nullpartia) = coalesce(:x_partia,  :nullpartia)         --mkd
                  and coalesce(s.x_serial_no, 0) = coalesce(:x_serial_no,0)   --mkd
                  and coalesce(s.x_slownik, 0) = coalesce(:x_slownik,0))       --mkd
            ) then
              nextact = 0;
          end --koniec petli
          if (toinsert > 0) then
          begin
            if (orgmwsordtype = mwsregularstockcheck) then
            begin
              delete from mwsords where ref = :refmwsord;
              execute procedure  XK_MWS_GEN_MWSORD_LOC_STOCK(NULL, NULL, NULL, 'B', null, null, 1, null, null, NULL, :param3, NULL)
                returning_values refmwsord;
              exit;
            end
           --mkd proba
            --mkd proba
            update mwsacts a set a.status = 6
            where a.mwsconstlocp = :mwsconstlocstock
                and a.status = 1
                and a.mwsordtypedest in (1,8)
                and a.vers = :vers
                and coalesce(a.x_partia,:nullpartia) = coalesce(:x_partia,:nullpartia)
                and coalesce(a.x_serial_no, 0) = coalesce(:x_serial_no,0)   --mkd
                and coalesce(a.x_slownik, 0) = coalesce(:x_slownik,0);
         --mkd proba
            --mkd proba
            maxnumber = maxnumber + 1;
            insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
                mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
                regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc, plus,
                whareap, whareal, wharealogp, wharealogl, number, disttogo, x_partia, x_serial_no, x_slownik)                                            --mkd
              values (:refmwsord, 1, :stocktaking, :good, :vers, :toinsert, :mwsconstlocstock, :mwspallocstock,
                  :mwsconstloci, :mwspalloci, :docid, :docposid, 'M', :opersettlmode, :autoloccreate, :wh, :wharea, null,
                  current_timestamp(0), :timestartdcl, :timestopdcl, :realtime, null, 'Odniesienie na lok. inw.', :priority, :lot, :rec, 1,
                  null, :whareal, null, null, :maxnumber, :dist, :x_partia, :x_serial_no, :x_slownik);                     --mkd

            quantity = quantity - toinsert;
          end
        end
      end
    end
  end

  -- to co trzeba dostawic na lokacje inwentaryzowana z lokacji inwentaryzacyjnej
  for
    select a.good, a.vers, coalesce(sum(a.quantityc),0), case when :magbreak = 2 then coalesce(a.lot,0) else 0 end,
             a.x_partia, a.x_serial_no, x_slownik
      from mwsacts a
        join towary t on (t.ktm = a.good)
      where a.mwsord = :mwsord and a.status = 5
      group by a.good, a.vers, case when :magbreak = 2 then coalesce(a.lot,0) else 0 end,
            a.x_partia, a.x_serial_no, a.x_slownik
      into good, vers, quantity, lota, :x_partia_a, :x_serial_no_a,  :x_slownik_a
  do begin
    if (lot is null) then
      lot = 0;
    if (x_partia is null) then
        x_partia = :nullpartia;
    if (x_serial_no is null) then
        x_serial_no = 0;
    if (x_slownik is null) then
        x_slownik = 0;
    select sum(s.quantity)
      from mwsstock s
        where s.vers = :vers and s.mwsconstloc = :mwsconstlocstock and s.ispal = 0
          and ((coalesce(s.lot,0) = :lota and :magbreak = 2) or :magbreak in (0,1))
          and coalesce(s.x_partia, :nullpartia) = coalesce(:x_partia_a, :nullpartia)         --mkd
          and coalesce(s.x_serial_no, 0) = coalesce(:x_serial_no_a, 0)   --mkd
          and coalesce(s.x_slownik, 0) = coalesce(:x_slownik_a,0)   --mkd
      into quantitya;
    if (quantitya is null) then quantitya = 0;
    if (quantity is null) then quantity = 0;
    quantity = quantity - quantitya;
    if (quantity is null) then quantity = 0;
    if (quantity < 0) then quantity = 0;
    if (quantity > 0) then
    begin
      toinsert = quantity;
      nextact = 0;
      if (exists (select first 1 s.ref from mwsstock  s
          where s.mwsconstloc = :mwsconstloci and s.vers = :vers
            and s.quantity - s.blocked > 0 and s.ispal = 0
            and ((s.lot = :lota and :magbreak = 2) or :magbreak in (0,1))
            and coalesce(s.x_partia, :nullpartia) = coalesce(:x_partia_a, :nullpartia)         --mkd
            and coalesce(s.x_serial_no, 0) = coalesce(:x_serial_no_a,0)   --mkd
            and coalesce(s.x_slownik, 0) = coalesce(:x_slownik_a,0))       --mkd)
      ) then
        nextact = 1;
      while (toinsert > 0 and nextact = 1)
      do begin
        if (orgmwsordtype = mwsregularstockcheck) then
        begin
          delete from mwsords where ref = :refmwsord;
          execute procedure  XK_MWS_GEN_MWSORD_LOC_STOCK(NULL, NULL, NULL, 'B', null, null, 1, null, null, NULL, :param3, NULL)
            returning_values refmwsord;
          exit;
        end
        -- okreslamy ilosc jaka mozna skompensowac
        select first 1 s.mwspalloc, s.lot, (s.quantity - s.blocked), x_partia, x_serial_no, x_slownik
          from mwsstock  s
          where s.mwsconstloc = :mwsconstloci and s.vers = :vers
            and s.quantity - s.blocked > 0 and s.ispal = 0
            and ((s.lot = :lota and :magbreak = 2) or :magbreak in (0,1))
            and coalesce(s.x_partia, :nullpartia) = coalesce(:x_partia_a, :nullpartia)         --mkd
            and coalesce(s.x_serial_no, 0) = coalesce(:x_serial_no_a,0)   --mkd
            and coalesce(s.x_slownik, 0) = coalesce(:x_slownik_a,0)      --mkd
          order by s.quantity
          into mwspalloci, loti, quantityi, :x_partia_i, x_serial_no_i, x_slownik_i;
        if (toinsert < quantityi) then
          quantityi = toinsert;
        maxnumber = maxnumber + 1;
        if (mwspallocstock is null) then
        begin
          execute procedure gen_ref('MWSPALLOCS') returning_values mwspallocstock;
          insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status,
              fromdocid, fromdocposid, fromdoctype, segtype)
            values(:mwspallocstock,cast(:mwspallocstock as varchar(20)), null, 2, 0, null, null,'D', 1);
        end

        insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
            mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
            regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc, plus,
            whareap, whareal, wharealogp, wharealogl, number, disttogo, x_partia, x_serial_no, x_slownik)
          values (:refmwsord, 1, :stocktaking, :good, :vers, :quantityi, :mwsconstloci, :mwspalloci,
              :mwsconstlocstock, :mwspallocstock, :docid, :docposid, 'M', :opersettlmode, :autoloccreate, :wh, :wharea, null,
              current_timestamp(0), :timestartdcl, :timestopdcl, :realtime, null, 'Doniesienie z lok. inw.', :priority, :loti, :rec, 1,
              null, :whareal, null, null, :maxnumber, :dist, :x_partia_i, :x_serial_no_i, :x_slownik_i);
        toinsert = toinsert - quantityi;
        if (not exists (select first 1 s.ref from mwsstock  s
            where s.mwsconstloc = :mwsconstloci and s.vers = :vers
              and s.quantity - s.blocked > 0 and s.ispal = 0
              and ((s.lot = :lota and :magbreak = 2) or :magbreak in (0,1))
              and coalesce(s.x_partia, :nullpartia) = coalesce(:x_partia_a, :nullpartia)         --mkd
              and coalesce(s.x_serial_no, 0) = coalesce(:x_serial_no_a,0)   --mkd
              and coalesce(s.x_slownik, 0) = coalesce(:x_slownik_a,0)
              )
        ) then
          nextact = 0;
      end --koniec petli while
      if (toinsert > 0) then
      begin
        if (orgmwsordtype = mwsregularstockcheck) then
        begin
          delete from mwsords where ref = :refmwsord;
          execute procedure  XK_MWS_GEN_MWSORD_LOC_STOCK(NULL, NULL, NULL, 'B', null, null, 1, null, null, NULL, :param3, NULL)
            returning_values refmwsord;
          exit;
        end
        if (coalesce(lota,0) = 0) then
          execute procedure mws_stocktakin_settl_lot(:good,:vers, :wh) returning_values :lota;
        maxnumber = maxnumber + 1;
        if (mwspallocstock is null) then
        begin
          execute procedure gen_ref('MWSPALLOCS') returning_values mwspallocstock;
          insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status,
              fromdocid, fromdocposid, fromdoctype, segtype)
            values(:mwspallocstock,cast(:mwspallocstock as varchar(20)), null, 2, 0, null, null,'D', 1);
        end

        insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
            mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
            regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc, plus,
            whareap, whareal, wharealogp, wharealogl, number, disttogo, x_partia, x_serial_no, x_slownik)
          values (:refmwsord, 0, :stocktaking, :good, :vers, :toinsert, :mwsconstloci,  :mwspalloci,
              :mwsconstlocstock, :mwspallocstock, :docid, :docposid, 'M', :opersettlmode, :autoloccreate, :wh, :wharea, null,
              current_timestamp(0), :timestartdcl, :timestopdcl, :realtime, null, 'Doniesienie z lok. inw.', :priority, :lota, 1, 1,
              null, :whareal, null, null, :maxnumber, :dist, :x_partia_a, :x_serial_no_a, :x_slownik_a);
      end
    end
  end
  update mwsords set status = 1, mwsords.timestopdecl = :timestopdcl where ref = :refmwsord and status = 0;
  update mwsacts set quantityc = quantity, status = 2 where mwsord = :refmwsord;
  update mwsords set status = 5 where ref = :refmwsord and status <> 5;
  update mwsconstlocs set act = 1 where ref = :mwsconstlocstock;
  suspend;
end^
SET TERM ; ^
