--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_GET_KTMTOGRUPYKUP(
      GRUPAKUP integer,
      REFWERSJI integer,
      JEDNOSTKA integer)
  returns (
      SYMBOL varchar(255) CHARACTER SET UTF8                           )
   as
begin
  symbol = '';
  select symbol from grupykupofe where grupakup = :grupakup and wersjaref = :refwersji and jedn = :jednostka into :symbol;
  suspend;
end^
SET TERM ; ^
