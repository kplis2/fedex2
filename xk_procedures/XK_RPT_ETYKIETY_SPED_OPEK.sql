--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_ETYKIETY_SPED_OPEK(
      LISTYWYSDREF integer,
      SPOSDOSTREF integer,
      KODPNAD varchar(10) CHARACTER SET UTF8                           )
  returns (
      RSYMBOLDOK varchar(255) CHARACTER SET UTF8                           ,
      RSYMBOLSPED varchar(255) CHARACTER SET UTF8                           ,
      RNAZWAODB1 varchar(255) CHARACTER SET UTF8                           ,
      RNAZWAODB2 varchar(255) CHARACTER SET UTF8                           ,
      RMIASTOODB varchar(255) CHARACTER SET UTF8                           ,
      RADRESODB varchar(255) CHARACTER SET UTF8                           ,
      RKODPODB varchar(255) CHARACTER SET UTF8                           ,
      RTELEFONODB varchar(255) CHARACTER SET UTF8                           ,
      RPOCZTAODB varchar(255) CHARACTER SET UTF8                           ,
      RDATAAKC timestamp,
      RUWAGISPED varchar(255) CHARACTER SET UTF8                           ,
      RILOSCPACZEK integer,
      RILOSCPALET integer,
      RNRPACZKI integer,
      RWAGA numeric(15,4),
      RPOBRANIE numeric(14,2),
      RKODREJONUNAD varchar(3) CHARACTER SET UTF8                           ,
      RKODKRAJUNAD varchar(20) CHARACTER SET UTF8                           ,
      RKODREJONUODB varchar(3) CHARACTER SET UTF8                           ,
      RKODKRAJUODB varchar(20) CHARACTER SET UTF8                           ,
      RKIERUNEKSORT varchar(3) CHARACTER SET UTF8                           ,
      RS10 smallint,
      RSSOBOTA smallint,
      RSZWROTDOK smallint,
      RSYMBOLREF varchar(255) CHARACTER SET UTF8                           ,
      RSYMBOLPACZKI varchar(255) CHARACTER SET UTF8                           ,
      RWAGAPACZKI numeric(15,4),
      RKODKRESK varchar(255) CHARACTER SET UTF8                           ,
      RSTREFAODB smallint)
   as
declare variable slodefref integer;
declare variable slopozref integer;
declare variable flagisped varchar(255);
declare variable nazwaodb varchar(255);
declare variable miastoodb varchar(255);
declare variable charpoz integer;
declare variable spedodlegref integer;
begin
  select lwd.symbol, lwd.symbolsped,
         lwd.kontrahent, lwd.miasto, lwd.adres, lwd.kodp,
         lwd.iloscpaczek, lwd.iloscpalet, lwd.waga, lwd.pobranie,
         lwd.dataakc, substring(lwd.uwagisped from 1 for 255),
         lwd.slodef, lwd.slopoz, lwd.flagisped
    from listywysd lwd
    where lwd.ref = :listywysdref
  into :rsymboldok, :rsymbolsped,
       :nazwaodb, :rmiastoodb, :radresodb, :rkodpodb,
       :riloscpaczek, :riloscpalet, :rwaga, :rpobranie,
       :rdataakc, :ruwagisped,
       :slodefref, :slopozref, :flagisped;

  execute procedure string_reverse(substring(:nazwaodb from 1 for 40)) returning_values :rnazwaodb1;
  charpoz = position(:nazwaodb in ' ');
  rnazwaodb1 = substring(:nazwaodb from 1 for 40-:charpoz);
  rnazwaodb2 = substring(:nazwaodb from 40-:charpoz for 80-:charpoz);

  riloscpaczek = :riloscpaczek + coalesce(:riloscpalet,0);

  --nadawca
  select first 1 spo.kodrejonu, spo.krajid
    from spedodleg spo
    where spo.spedytor = :sposdostref
      and spo.kodp = replace(replace(:kodpnad,'-',''), ' ', '')
  into :rkodrejonunad, :rkodkrajunad;

  --odbiorca
  if (coalesce(:radresodb, '') = '' or coalesce(:rmiastoodb, '') = '') then begin
    if (:slodefref = 1 and :slopozref is not null) then begin
      select k.ulica, k.miasto, k.kodp, k.poczta, k.telefon
        from klienci k where ref = :slopozref
      into :radresodb, :miastoodb, :rkodpodb, :rpocztaodb, :rtelefonodb;
    end
    else if (:slodefref = 6 and :slopozref is not null) then begin
      select d.ulica, d.miasto, d.kodp, d.poczta, d.telefon
        from dostawcy d where ref = :slopozref
      into :radresodb, :miastoodb, :rkodpodb, :rpocztaodb, :rtelefonodb;
    end

    if (:miastoodb <> :rpocztaodb) then radresodb = :radresodb||', '||:miastoodb;
  end

  select first 1 spo.ref, spo.kodrejonu, spo.strefa, spo.krajid, spo.kieruneksort, spo.s10, spo.ssobota
    from spedodleg spo
    where spo.spedytor = :sposdostref
      and spo.kodp = replace(replace(:rkodpodb,'-',''), ' ', '')
      and spo.miasto = upper(:rmiastoodb)
  into :spedodlegref, :rkodrejonuodb, :rstrefaodb, :rkodkrajuodb, :rkieruneksort, :rs10, :rssobota;

    if (:spedodlegref is null) then
      select first 1 spo.kodrejonu, spo.strefa, spo.krajid, spo.kieruneksort, spo.s10, spo.ssobota
        from spedodleg spo
        where spo.spedytor = :sposdostref
          and spo.kodp = replace(replace(:rkodpodb,'-',''), ' ', '')
      into :rkodrejonuodb, :rstrefaodb, :rkodkrajuodb, :rkieruneksort, :rs10, :rssobota;

    if (strmulticmp(:flagisped,';S;')>0 and coalesce(:rssobota, 0) = 1) then rssobota = 1;
    else rssobota = 0;
    if (strmulticmp(:flagisped,';D;')>0 and coalesce(:rs10, 0) = 1) then rs10 = 1;
    else rs10 = 0;
    if (strmulticmp(:flagisped,';Z;')>0) then rszwrotdok = 1;

    --dokumenty REF
    select first 1 symbol
      from nagfak where listywysd = :listywysdref
      order by ref
    into :rsymbolref;
    if (:rsymbolref is null) then begin
      select first 1 dn.symbol
        from dokumnag dn
          join listywysdpoz lwp on (dn.ref = lwp.dokref)
        where lwp.dokument = :listywysdref
        order by dn.ref
      into :rsymbolref;
    end

    rnrpaczki = 0;
    for select lwo.nrpaczki, lwo.waga
          from listywysdroz_opk lwo
          where lwo.listwysd = :listywysdref
            and lwo.rodzic is null
        into :rsymbolpaczki, :rwagapaczki
    do begin
      rkodkresk = 'Z'||:rsymbolpaczki;
      rnrpaczki = :rnrpaczki + 1;
      suspend;
    end
end^
SET TERM ; ^
