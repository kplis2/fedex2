--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_POZFAK3(
      NAGFAKREF integer)
  returns (
      LP integer,
      KTM varchar(255) CHARACTER SET UTF8                           ,
      NAZWA varchar(1000) CHARACTER SET UTF8                           ,
      PKWIU varchar(255) CHARACTER SET UTF8                           ,
      ILOSC numeric(14,4),
      MIARA varchar(20) CHARACTER SET UTF8                           ,
      RABAT numeric(14,2),
      CENANET numeric(14,4),
      WARTNET numeric(14,2),
      CENABRU numeric(14,4),
      WARTBRU numeric(14,2),
      GR_VAT varchar(20) CHARACTER SET UTF8                           ,
      WARTVAT numeric(14,2))
   as
declare variable ZALICZKOWY integer;
declare variable KOREKTA integer;
declare variable POZREF integer;
declare variable OPIS varchar(1024);
declare variable EOL varchar(10);
declare variable FROMNAGZAM integer;
declare variable SYMBZAM varchar(100);
declare variable DATAZAM timestamp;
declare variable ZAKUP integer;
declare variable WALUTOWA integer;
declare variable WERSJAREF integer;
begin
  eol='
';
  lp = 1;
  select nagfak.zaliczkowy, typfak.korekta, nagfak.zakup, nagfak.walutowa
  from nagfak
  left join typfak on (typfak.symbol=nagfak.typ)
  where nagfak.ref=:nagfakref
  into :zaliczkowy, :korekta, :zakup, :walutowa;
  if(:zakup=1) then walutowa = 0;
  if(:zaliczkowy=0 and :korekta=0) then begin
    /* zwykle pozycje faktury */
    exit;
  end
  else if(:zaliczkowy=0 and :korekta=1) then begin
    /* po korekcie */
    for select POZFAK.REF, POZFAK.WERSJAREF, POZFAK.KTM,(case when POZFAK.NAZWAT is null or POZFAK.NAZWAT = '' then TOWARY.NAZWA  else POZFAK.NAZWAT end)
      ||' '||coalesce(POZFAK.OPIS,''),POZFAK.PKWIU,POZFAK.ILOSC,
      TOWJEDN.JEDN, POZFAK.RABAT+NAGFAK.RABAT,
      (case when :walutowa=1 then POZFAK.CENANET else POZFAK.CENANETZL end),
      (case when :walutowa=1 then POZFAK.WARTNET else POZFAK.WARTNETZL end),
      (case when :walutowa=1 then POZFAK.CENABRU else POZFAK.CENABRUZL end),
      (case when :walutowa=1 then POZFAK.WARTBRU else POZFAK.WARTBRUZL end),
      POZFAK.GR_VAT,
      (POZFAK.WARTBRUZL-POZFAK.WARTNETZL)
      from POZFAK
        join NAGFAK on (NAGFAK.REF=POZFAK.DOKUMENT)
        left join TOWARY on (TOWARY.KTM = POZFAK.KTM)
        left join wersje on (wersje.ref = pozfak.wersjaref)
        left join TOWJEDN on (TOWJEDN.REF = POZFAK.JEDN)
      where POZFAK.DOKUMENT=:nagfakref and POZFAK.OPK = 0
        and coalesce(pozfak.fake,0) = 0
      order by POZFAK.NUMER
    into :pozref,:wersjaref,:ktm,:nazwa,:pkwiu,:ilosc,:miara,:rabat,:cenanet,:wartnet,:cenabru,:wartbru,:gr_vat,:wartvat
    do begin
      if(coalesce(:pkwiu,'')='') then select pkwiu from wersje where ref=:wersjaref into :pkwiu;
      if(coalesce(:pkwiu,'')='') then select pkwiu from towary where ktm=:ktm into :pkwiu;
      select rktm,rnazwa from xk_rpt_nazwaiktm(:pozref,:ktm,:nazwa) into :ktm, :nazwa;
      select opis from XK_RPT_TOWAR(:pozref,'FAK','D') into :opis;
      if(:opis<>'') then nazwa = :nazwa||:eol||:opis;
      suspend;
      lp = :lp + 1;
    end
  end
  else if(:zaliczkowy=2) then begin
    /* korekta zaliczkowa - po korekcie */
    fromnagzam = null;
    symbzam = null;
    for select '','Zaliczka','',1,'szt',NULL,
      (case when :walutowa=1 then nagfakzal.pwartnet-nagfakzal.wartnet else nagfakzal.pwartnetzl-nagfakzal.wartnetzl end),
      (case when :walutowa=1 then nagfakzal.pwartnet-nagfakzal.wartnet else nagfakzal.pwartnetzl-nagfakzal.wartnetzl end),
      NULL,
      (case when :walutowa=1 then nagfakzal.pwartbru-nagfakzal.wartbru else nagfakzal.pwartbruzl-nagfakzal.wartbruzl end),
      nagfakzal.vat,
      (nagfakzal.pwartbruzl-nagfakzal.pwartnetzl)-(nagfakzal.wartbruzl-nagfakzal.wartnetzl),
      nagfak.fromnagzam
      from nagfakzal
      join nagfak on (nagfak.ref=nagfakzal.fakturazal)
      where nagfakzal.faktura=:nagfakref
      into :ktm,:nazwa,:pkwiu,:ilosc,:miara,:rabat,:cenanet,:wartnet,:cenabru,:wartbru,:gr_vat,:wartvat,:fromnagzam
    do begin
      if(:fromnagzam is not null) then select id,datawe from nagzam where ref=:fromnagzam into :symbzam,:datazam;
      if(:symbzam is not null) then nazwa = :nazwa||' do zamówienia '||:symbzam||' z dnia '||cast(:datazam as date);
      suspend;
      lp = :lp + 1;
    end
  end
end^
SET TERM ; ^
