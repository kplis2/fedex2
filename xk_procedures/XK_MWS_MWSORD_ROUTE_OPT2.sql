--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSORD_ROUTE_OPT2(
      MWSORD integer)
   as
declare variable weight numeric(14,4);
declare variable vol numeric(14,4);
declare variable ordweight numeric(14,4);
declare variable ordvol numeric(14,4);
declare variable cnt integer;
declare variable opt integer;
declare variable startarea integer;
declare variable aref integer;
declare variable wh varchar(3);
declare variable currsec varchar(10);
declare variable currrow integer;
declare variable currpal integer;
declare variable symbol varchar(30);
declare variable sectocomp varchar(10);
begin
  currsec = 'A';
  currrow = 1;
  currpal = 1;
  select count(distinct a.mwsconstlocp)
    from mwsacts a where a.mwsord = :mwsord --and a.status < 5
    into cnt;
  if (cnt = 1) then
    exit;
  select wh from mwsords where ref = :mwsord
    into wh;
  cnt = 0;
  select count(ref) from mwsacts where mwsord = :mwsord --and status < 5
    into cnt;
  update mwsacts set ord = 5, number = -1 where mwsord = :mwsord; --and status < 5;
  opt = 0;
  while (opt < cnt and exists(select first 1 ref from mwsacts where mwsord = :mwsord
      /*and status < 5 */and number = -1))
  do begin
    opt = opt + 1;
    aref = null;
    select first 1 c.symbol, a.ref, substring(c.symbol from 1 for 1),
        coalesce(c.routerownum,cast(substring(c.symbol from 2 for 2) as integer)),
        cast(substring(c.symbol from 4 for 2) as integer)
      from mwsacts a
        left join mwsconstlocs c on (c.ref = a.mwsconstlocp)
      where a.mwsord = :mwsord and a.number = -1 --and a.status < 5
      order by substring(c.symbol from 1 for 1) desc,
        (coalesce(c.routerownum,cast(substring(c.symbol from 2 for 2) as integer)) - :currrow) * 10000
        + abs(cast(substring(c.symbol from 4 for 2) as integer) - :currpal) * 10
      into symbol, aref, currsec, currrow, currpal;
    if (aref is null) then
      opt = cnt;
    if (aref is not null) then
      update mwsacts set number = :opt where ref = :aref;
    select first 1 substring(c.symbol from 1 for 1)
            from mwsacts a
              left join mwsconstlocs c on (c.ref = a.mwsconstlocp)
            where a.mwsord = :mwsord
              and a.status < 5
              and a.number = -1
            order by substring(c.symbol from 1 for 1) desc
    into sectocomp;
    if (currsec > 'B' and sectocomp = 'B') then
      currrow = 1;
  end
  update mwsacts set ord = 1 where mwsord = :mwsord and ord = 5 ;
end^
SET TERM ; ^
