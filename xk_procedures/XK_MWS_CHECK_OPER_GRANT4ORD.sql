--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_CHECK_OPER_GRANT4ORD(
      AKTUOPERATOR integer,
      AKTUMAGAZYN varchar(3) CHARACTER SET UTF8                           )
  returns (
      GRANTS smallint)
   as
declare variable operator integer;
begin
  select first 1 mo.operator
    from mwsordtypedest4op mo
    where mo.mwsordtypedest = 0 and mo.wh = :aktumagazyn
      and mo.operator = :aktuoperator
    into :operator;
  if (operator is null) then
  begin
    select first 1 mo.operator
      from mwsordtypedest4op mo
      where mo.operator = :aktuoperator and mo.wh = :aktumagazyn and mo.mwsordtypedest = 1
      into :operator;
    if (operator is not null) then
       --grants = 2; -- xxx KBI asysta 20190518 bo przy grants = 2 automatycznie pobieral zlecenie wt dla operatora po wejsciu do moichzlece^
SET TERM ; ^
