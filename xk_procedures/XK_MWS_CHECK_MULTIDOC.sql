--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_CHECK_MULTIDOC(
      WH DEFMAGAZ_ID,
      MWSORDTYPE MWSORDTYPES_ID,
      DOCID DOKUMNAG_ID,
      DOCGROUP DOKUMNAG_ID)
  returns (
      MULTI SMALLINT_ID)
   as
declare variable MAXWEIGHT WAGA_ID;
declare variable MAXVOLUME WAGA_ID;
declare variable CNT integer_id;
declare variable SUMQUANTITY ilosci_mag;
declare variable SUMWEIGHT waga_id;
declare variable SUMVOLUME waga_id;
begin
  if (:docgroup is null) then
    select grupasped from dokumnag where ref = :docid
    into :docgroup;

  --nie laczymy dokumentow wewnetrznych i nie bedacych wydaniami lub bedacych korektami
  --lub
  --nie laczymy dokumentow jesli na pozycjach bylo cos czego nie mozna laczyc
  if (not exists(select first 1 1 from dokumnag where grupasped = :docgroup and zewn = 1 and wydania = 1 and koryg = 0) or
      exists(select first 1 1 from dokumnag n
                  left join dokumpoz p on (n.ref = p.dokument) left join towary t on (p.ktm = t.ktm)
                where n.grupasped = :docgroup and coalesce(t.mwsmultinielacz,0) = 1)
  ) then
  begin
    multi = 0;
    exit;
  end

  select coalesce(m.maxweight,0), coalesce(m.maxvolume,0)
    from mwsordtypes4wh m
    where m.mwsordtype = :mwsordtype
      and m.wh = :wh
  into :maxweight, :maxvolume;

  --w Logbox waga jest w gramach, dla generowania zlecen znaczenie maja kg
  select count(distinct p.ktm), sum(p.ilosc), sum(p.waga), sum(p.objetosc)
    from dokumnag d
      left join dokumnag dg on (d.grupasped = dg.grupasped)
      left join dokumpoz p on (p.dokument = dg.ref)
    where d.grupasped = :docgroup
  into :cnt, :sumquantity, :sumweight, :sumvolume;

  if (:sumweight is null) then sumweight = 0;
  if (:sumvolume is null) then sumvolume = 0;

  if (:sumquantity = 1) then
    multi = 2; --dokument pojedynczy
  else if((:sumweight > 0 or :sumvolume > 0) and
      (:maxweight > :sumweight or :maxweight = 0) and
      (:maxvolume > :sumvolume or :maxvolume = 0)) then
    multi = 1; --multipicking z kolorami
  else
    multi = 0; --normalne zlecenie

  suspend;
end^
SET TERM ; ^
