--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_GOODS_REFILL_ORDER(
      WHIN varchar(3) CHARACTER SET UTF8                           ,
      MAXREC integer)
  returns (
      VERS integer,
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      TAKEFROMMIX smallint,
      QUANTITY numeric(14,4),
      PRIORITY smallint)
   as
declare variable prioritydts varchar(100);
declare variable prioritydt smallint;
declare variable sposdost integer;
declare variable cnt integer;
declare variable gls integer;
declare variable schenker integer;
begin
  -- Procedura ustala w jakiej kolejnosci bdziemy badać toawary do wygenrowania WG
  cnt = 0;
  execute procedure get_config('MWSPRIORITYDT',2)
    returning_values prioritydts;
  if (prioritydts is null or prioritydts = '') then
    prioritydts = '0';
  prioritydt = cast(prioritydts as smallint);
  if (prioritydt = 2) then sposdost = :schenker;
  else if (prioritydt = 3) then sposdost = :gls;
  -- Wg daty utworzenia dokumnetu wydania
  if (prioritydt = 1) then
  begin
    for
      select first 6 m.vers, m.good, coalesce(v.mwscalcmix,0), sum(m.quantity - m.ordered),
          max(case when s.listywys is null then 0 when s.listywys = 0 then 2 else 1 end)
        from mwsgoodsrefill m
          left join wersje v on (v.ref = m.vers)
          left join dokumnag d on (d.ref = m.docid)
          left join sposdost s on (s.ref = d.sposdost)
          left join xk_goods_relocate x on (x.vers = m.vers and x.wh = m.wh)
        where d.ref is not null and m.preparegoods = 1 and m.wh = :whin
          and exists (select s.ref from mwsstock s join mwsconstlocs l on (l.ref = s.mwsconstloc and l.act > 0)
             where s.vers = m.vers and s.blocked = 0 and (s.x_blocked<>1)) --XXX Ldz ZG 126909)
          and not exists(select first 1 xk.mwsconstloc
            from xk_mws_take_full_pal_check(m.wh,d.grupasped,null,0) xk
            group by xk.mwsconstloc)
        group by m.vers, m.good, coalesce(v.mwscalcmix,0)
        having sum(m.quantity - m.ordered) > 0
        order by min(d.data)
        into vers, good, takefrommix, quantity, priority
    do begin
      cnt = cnt + 1;
      suspend;
      if (cnt > :maxrec) then
        break;
    end
  end
  -- Opek, masterlink ??
  else if (prioritydt = 2 or prioritydt = 3) then
  begin
    for
      select first 6 m.vers, m.good, coalesce(v.mwscalcmix,0), sum(m.quantity - m.ordered),
          max(case when s.listywys is null then 0 when s.listywys = 0 then 2 else 1 end)
        from mwsgoodsrefill m
          left join wersje v on (v.ref = m.vers)
          left join dokumnag d on (d.ref = m.docid)
          left join sposdost s on (s.ref = d.sposdost)
        where d.ref is not null and m.preparegoods = 1 and m.wh = :whin
          and exists (select s.ref from mwsstock s join mwsconstlocs l on (l.ref = s.mwsconstloc and l.act > 0)
             where s.vers = m.vers and s.blocked = 0and (s.x_blocked<>1)) --XXX Ldz ZG 126909)
          and not exists(select first 1 xk.mwsconstloc
            from xk_mws_take_full_pal_check(m.wh,d.grupasped,null,0) xk
            group by xk.mwsconstloc)
          and d.sposdost = :sposdost
        group by m.vers, m.good, coalesce(v.mwscalcmix,0)
        having sum(m.quantity - m.ordered) > 0
        order by min(m.priority)
        into vers, good, takefrommix, quantity, priority
    do begin
      cnt = cnt + 1;
      suspend;
      if (cnt > :maxrec) then
        break;
    end
  end
  -- Najwicej lini z z lokacji
  else if (prioritydt = 4) then
  begin
    for
      select first 6 m.vers, m.good, coalesce(v.mwscalcmix,0), sum(m.quantity - m.ordered),
          max(case when s.listywys is null then 0 when s.listywys = 0 then 2 else 1 end)
        from mwsgoodsrefill m
          left join wersje v on (v.ref = m.vers)
          left join dokumnag d on (d.ref = m.docid)
          left join sposdost s on (s.ref = d.sposdost)
          left join xk_goods_relocate x on (x.vers = m.vers and x.wh = m.wh)
        where d.ref is not null and m.preparegoods = 1 and m.wh = :whin
          and exists (select s.ref from mwsstock s join mwsconstlocs l on (l.ref = s.mwsconstloc and l.act > 0)
             where s.vers = m.vers and s.blocked = 0 and (s.x_blocked<>1)) --XXX Ldz ZG 126909))
          and exists (select first 1 xk.mwsconstloc
            from xk_mws_take_full_pal_check(m.wh,d.grupasped,null,0) xk
            group by xk.mwsconstloc)
        group by m.vers, m.good, coalesce(v.mwscalcmix,0)
        having sum(m.quantity - m.ordered) > 0
        order by count(m.ref) desc
        into vers, good, takefrommix, quantity, priority
    do begin
      cnt = cnt + 1;
      suspend;
      if (cnt > :maxrec) then
        break;
    end
  end
  -- wg priorytetu (wydania WT ??)
  if (cnt < :maxrec and prioritydt <> 1) then
  begin
    for
      select first 6 m.vers, m.good, coalesce(v.mwscalcmix,0), sum(m.quantity - m.ordered),
          max(case when s.listywys is null then 0 when s.listywys = 0 then 2 else 1 end)
        from mwsgoodsrefill m
          left join wersje v on (v.ref = m.vers)
          left join dokumnag d on (d.ref = m.docid)
          left join sposdost s on (s.ref = d.sposdost)
          left join xk_goods_relocate x on (x.vers = m.vers and x.wh = m.wh)
        where d.ref is not null and m.preparegoods = 1 and m.wh = :whin
          and exists (select s.ref from mwsstock s join mwsconstlocs l on (l.ref = s.mwsconstloc and l.act > 0)
             where s.vers = m.vers and s.blocked = 0 and (s.x_blocked<>1)) --XXX Ldz ZG 126909))
          and not exists(select first 1 xk.mwsconstloc
            from xk_mws_take_full_pal_check(m.wh,d.grupasped,null,0) xk
            group by xk.mwsconstloc)
        group by m.vers, m.good, coalesce(v.mwscalcmix,0)
        having sum(m.quantity - m.ordered) > 0
        order by min(m.priority)
        into vers, good, takefrommix, quantity, priority
    do begin
      cnt = cnt + 1;
      suspend;
      if (cnt > :maxrec) then
        break;
    end
  end
end^
SET TERM ; ^
