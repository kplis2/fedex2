--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_CHECK_MWSORDREAL(
      AKTUOPERATOR integer,
      FROMMWSACT integer,
      MWSORDTYPEDEST integer = 11)
  returns (
      STATUS smallint,
      PALLETBLOCK varchar(255) CHARACTER SET UTF8                           )
   as
declare variable mwsconstlocl integer;
declare variable mwsconstlocblock integer;
declare variable frommwsactblock integer;
declare variable locblockedpall varchar(40);
declare variable blocktype smallint;
declare variable mwsord integer;
begin
  status = 0;
  select first 1 ma.mwsconstlocl, mo.ref
    from mwsords mo
      join mwsacts ma on (ma.mwsord = mo.ref)
    where mo.frommwsact = :frommwsact
      and mo.status > 0 and mo.status < 5
    into :mwsconstlocl, :mwsord;
  for
    select mc.mwsconstloc
      from mwsconstlocacces mc
      where mc.mwsconstlocblock = :mwsconstlocl
      into :mwsconstlocblock
  do begin
    if(:mwsconstlocblock is not null) then
      begin
        blocktype = 1;
        select first 1 mo.frommwsact
          from mwsords mo
            join mwsacts ma on (ma.mwsord = mo.ref)
          where mo.mwsordtypedest = :mwsordtypedest and mo.status < 5
          and ma.mwsconstlocl = :mwsconstlocblock
          into :frommwsactblock;
        if (:frommwsactblock is null) then
        begin
          blocktype = 2;
          select first 1 mo.frommwsact
            from mwsords mo
              join mwsacts ma on (ma.mwsord = mo.ref)
            where ma.status < 5 and ma.planmwsconstlocl = :mwsconstlocblock
            into :frommwsactblock;
        end
        if (:frommwsactblock is not null) then
          begin
            select first 1 mc.symbol
              from mwsords mo
                join mwsacts ma on (ma.mwsord = mo.ref)
                join mwsconstlocs mc on (ma.mwsconstlocp = mc.ref)
              where mo.frommwsact = :frommwsactblock
              into :locblockedpall;
            status = 1;
            if (blocktype = 1) then
              palletblock = 'Realizacja tego zlecenia zablokuje realizację zlecenia: '||frommwsactblock||'. Paleta ta znajduje się na lokacji: '||locblockedpall;
            else if (blocktype = 2) then
              palletblock = 'Realizacja tego zlecenia zablokuje wywiezienie palety, ktora jest w trakcie rozladunku';
          end
        else
          status = 0;
      end
    else
     status = 0;
  end
  if (status <> 1) then begin
    update mwsacts ma set ma.status = 2 where ma.mwsord = :mwsord;
    update mwsords mo set mo.status = 2, mo.operator = :aktuoperator where mo.ref = :mwsord;
  end
  suspend;
end^
SET TERM ; ^
