--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_IMP_ECOD_ORDER(
      REF integer,
      DOCUMENT_ORDER integer)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
   as
declare variable id integer;
declare variable order_header integer;
declare variable order_parties integer;
declare variable order_lines integer;
declare variable buyer integer;
declare variable seller integer;
declare variable deliverypoint integer;
declare variable line integer;
declare variable lineitem integer;
declare variable klientgln varchar(255);
declare variable klientnazwa varchar(255);
declare variable klientref integer;
declare variable odbiorcagln varchar(255);
declare variable odbiorcanazwa varchar(255);
declare variable odbiorcaref integer;
declare variable sprzedawcagln varchar(255);
declare variable infogln varchar(255);
declare variable edifile varchar(255);
declare variable klientsposdost integer;
declare variable klientsposplat integer;
declare variable odbiorcaulica varchar(255);
declare variable odbiorcanrdomu nrdomu_id;
declare variable odbiorcanrlokalu nrdomu_id;
declare variable odbiorcakodp varchar(255);
declare variable odbiorcamiasto varchar(255);
declare variable rejestr varchar(255);
declare variable typzam varchar(255);
declare variable magazyn varchar(255);
declare variable walpln varchar(255);
declare variable znakzewn varchar(255);
declare variable datazewn varchar(255);
declare variable termdost varchar(255);
declare variable uwagi varchar(2048);
declare variable waluta varchar(255);
declare variable walutowe integer;
declare variable nagzamref integer;
declare variable numer integer;
declare variable ktoryvat integer;
declare variable ean varchar(255);
declare variable buyeritemcode varchar(255);
declare variable supplieritemcode varchar(255);
declare variable unitofmeasure varchar(255);
declare variable orderedquantity varchar(255);
declare variable ilosco numeric(14,4);
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable jedno integer;
declare variable przelicz numeric(14,4);
declare variable ilosc numeric(14,4);
declare variable gr_vat varchar(10);
begin
    select filename from EDEDOCSH where ref=:ref into :edifile;
    -- pobierz identyfikatory sekcji
    select ID from EDEDOCSP
    where EDEDOCH=:ref and PARENT=:document_order and name='Order-Header' into :order_header;
    select ID from EDEDOCSP
    where EDEDOCH=:ref and PARENT=:document_order and name='Order-Parties' into :order_parties;
    select ID from EDEDOCSP
    where EDEDOCH=:ref and PARENT=:document_order and name='Order-Lines' into :order_lines;
    select ID from EDEDOCSP
    where EDEDOCH=:ref and PARENT=:order_parties and name='Buyer' into :buyer;
    select ID from EDEDOCSP
    where EDEDOCH=:ref and PARENT=:order_parties and name='Seller' into :seller;
    select ID from EDEDOCSP
    where EDEDOCH=:ref and PARENT=:order_parties and name='DeliveryPoint' into :deliverypoint;

    -- sprawdz czy buyer jest w naszej bazie klientow
    select val from EDEDOCSP
    where EDEDOCH=:ref and PARENT=:buyer and name='ILN' into :klientgln;
    select val from EDEDOCSP
    where EDEDOCH=:ref and PARENT=:buyer and name='Name' into :klientnazwa;
    if(:klientgln is null) then klientgln = '';
    if(:klientnazwa is null) then klientnazwa = '';
    klientref = null;
    select ref from klienci where GLN=:klientgln into :klientref;
    if(:klientref is null) then exception UNIVERSAL 'Klient '||:klientnazwa||' nie znaleziony na podstawie GLN: '||:klientgln;
    select sposdost,sposplat,coalesce(vat,0) from klienci where ref=:klientref
    into :klientsposdost,:klientsposplat,:ktoryvat;
    -- sprawdz czy delivery point jest w naszej bazie odbiorcow
    select val from EDEDOCSP
    where EDEDOCH=:ref and PARENT=:deliverypoint and name='ILN' into :odbiorcagln;
    select val from EDEDOCSP
    where EDEDOCH=:ref and PARENT=:deliverypoint and name='Name' into :odbiorcanazwa;
    if(:odbiorcagln is null) then odbiorcagln = '';
    if(:odbiorcanazwa is null) then odbiorcanazwa = '';
    odbiorcaref = null;
    select ref from odbiorcy where GLN=:odbiorcagln into :odbiorcaref;
    if(:odbiorcaref is null) then exception UNIVERSAL 'Odbiorca '||:odbiorcanazwa||' nie znaleziony na podstawie GLN: '||:odbiorcagln;
    select nazwa,dulica,dkodp,dmiasto from odbiorcy where ref=:odbiorcaref
    into :odbiorcanazwa, :odbiorcaulica, :odbiorcakodp, :odbiorcamiasto;
    -- sprawdz czy seller to jest nasz GLN
    select val from EDEDOCSP
    where EDEDOCH=:ref and PARENT=:seller and name='ILN' into :sprzedawcagln;
    select wartosc from getconfig('INFOGLN') into :infogln;
    if(:sprzedawcagln<>:infogln) then exception UNIVERSAL 'GLN sprzedawcy '||:sprzedawcagln||' jest inny niz nasz GLN '||:infogln;

    -- naglowek
    select wartosc from getconfig('REJZAM4EDI') into :rejestr;
    select wartosc from getconfig('TYPZAM4EDI') into :typzam;
    select wartosc from getconfig('WAREHOUSE4EDI') into :magazyn;
    select wartosc from getconfig('WALPLN') into :walpln;
    select val from EDEDOCSP
    where EDEDOCH=:ref and PARENT=:order_header and name='OrderNumber' into :znakzewn;
    select val from EDEDOCSP
    where EDEDOCH=:ref and PARENT=:order_header and name='OrderDate' into :datazewn;
    select val from EDEDOCSP
    where EDEDOCH=:ref and PARENT=:order_header and name='ExpectedDeliveryDate' into :termdost;
    select val from EDEDOCSP
    where EDEDOCH=:ref and PARENT=:order_header and name='Remarks' into :uwagi;
    select val from EDEDOCSP
    where EDEDOCH=:ref and PARENT=:order_header and name='OrderCurrency' into :waluta;
    if(:waluta<>:walpln) then walutowe = 1; else walutowe = 0;


    execute procedure GEN_REF('NAGZAM') returning_values :nagzamref;
    insert into NAGZAM(ref,rejestr,typzam,datawe,magazyn,klient,slodef,slopoz,
      odbiorcaid,odbiorca,dulica,dnrdomu,dnrlokalu,dmiasto,dkodp,
      edifile,sposdost,sposzap,waluta,walutowe,
      znakzewn,datazewn,termdost,uwagi)
    values(:nagzamref,:rejestr,:typzam,current_date,:magazyn,:klientref,1,:klientref,
      :odbiorcaref,:odbiorcanazwa,:odbiorcaulica,:odbiorcanrdomu,:odbiorcanrlokalu,:odbiorcamiasto,:odbiorcakodp,
      :edifile,:klientsposdost,:klientsposplat,:waluta,:walutowe,
      :znakzewn,:datazewn,:termdost,:uwagi);

    -- pozycje
    numer = 0;
    for select ID from EDEDOCSP
    where EDEDOCH=:ref and PARENT=:order_lines and name='Line'
    order by ID
    into :line
    do begin
      -- pobierz wartosci z pliku
      select ID from EDEDOCSP
      where EDEDOCH=:ref and PARENT=:line and name='Line-Item' into :lineitem;
      select val from EDEDOCSP
      where EDEDOCH=:ref and PARENT=:lineitem and name='EAN' into :ean;
      select val from EDEDOCSP
      where EDEDOCH=:ref and PARENT=:lineitem and name='BuyerItemCode' into :buyeritemcode;
      select val from EDEDOCSP
      where EDEDOCH=:ref and PARENT=:lineitem and name='SupplierItemCode' into :supplieritemcode;
      select val from EDEDOCSP
      where EDEDOCH=:ref and PARENT=:lineitem and name='UnitOfMeasure' into :unitofmeasure;
      select val from EDEDOCSP
      where EDEDOCH=:ref and PARENT=:lineitem and name='OrderedQuantity' into :orderedquantity;
      ilosco = cast (:orderedquantity as numeric(14,4));
      -- ustal towar i ilosc
      select KTM,WERSJA,JEDNO from XK_GET_KTMFROMEDI(:klientref,:ean,:buyeritemcode,:supplieritemcode,:unitofmeasure)
      into :ktm,:wersja,:jedno;
      select przelicz from towjedn where ref=:jedno into :przelicz;
      ilosc = :ilosco * :przelicz;
      -- ustal stawke vat
      gr_vat = '';
      if(:ktoryvat=0) then begin
        select coalesce(vat,'') from wersje where ktm=:ktm and nrwersji=:wersja into :gr_vat;
        if(:gr_vat='') then select vat from towary where ktm=:ktm into :gr_vat;
      end else begin
        select vat2 from towary where ktm=:ktm into :gr_vat;
      end
      -- cena nadaje sie sama wg warunkow danego klienta na triggerach

      numer = :numer + 1;
      insert into POZZAM(zamowienie,numer,ord,ktm,wersja,jedno,ilosco,ilosc,gr_vat)
      values(:nagzamref,:numer,1,:ktm,:wersja,:jedno,:ilosco,:ilosc,:gr_vat);
    end

    otable = 'NAGZAM';
    oref = :nagzamref;
    suspend;
end^
SET TERM ; ^
