--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSDPOZ_ADDING_TOWAR(
      BAR_CODE varchar(40) CHARACTER SET UTF8                           ,
      ILOSC numeric(14,2),
      MNOZNIK numeric(14,2),
      ACTIVE_BOX_CODE varchar(40) CHARACTER SET UTF8                           ,
      SPED_DOC integer,
      WERSJAREF integer,
      POZYCJA integer,
      STATUS smallint,
      OPERATOR integer)
  returns (
      ERR_MSG varchar(255) CHARACTER SET UTF8                           ,
      INFO_MSG varchar(255) CHARACTER SET UTF8                           ,
      RWERSJAREF integer,
      RPOZYCJA integer,
      RSTATUS smallint)
   as
declare variable MAXNROPK integer;
declare variable ILOSCROZ numeric(14,4);
declare variable ILOSCPOZ numeric(14,4);
declare variable ILOSCMAX numeric(14,4);
declare variable ILOSCSPK numeric(14,4);
declare variable RILOSC numeric(14,4);
declare variable ILOSCROZ_TMP numeric(14,4);
declare variable ILOSCPOZ_TMP numeric(14,4);
declare variable ACTIVE_BOX_REF integer;
declare variable KLIENT integer;
declare variable ODBIORCA integer;
declare variable X_BOX_KTM ktm_id;
declare variable X_MWS_SERIE SMALLINT_ID;
declare variable X_MWS_PARTIE SMALLINT_ID;
declare variable X_MWS_SLOWNIK_EHRLE SMALLINT_ID;
begin
--    exception test_break :BAR_CODE||';'||:ILOSC||';'||:MNOZNIK||';'||:ACTIVE_BOX_CODE||';'||
--                         :SPED_DOC||';'||:WERSJAREF||';'||:POZYCJA||';'||:STATUS;

    err_msg = '';
    info_msg = '';

    select l.slopoz, l.odbiorcaid from listywysd l
      where l.ref = :sped_doc and l.slodef = 1
    into klient, :odbiorca;
    if (:klient is null and :odbiorca is not null) then
      select klient from odbiorcy where ref = :odbiorca
      into :klient;

    select ref from listywysdroz_opk opk where opk.stanowisko = :active_box_code and opk.listwysd = :sped_doc
      into :active_box_ref;

    --dodanie towaru do kartonu lub palety
    if (coalesce(:wersjaref, 0) = 0) then
    begin
            if (/*exists(select nropk from listywysdroz_opk where listwysd = :sped_doc and stanowisko = :active_box_code and stan = 1) or */
                  coalesce(:active_box_code, '') = '') then
            begin
                /*err_msg = 'Próba dodania towaru do zamkniętego opakowania lub nie wskazano opakowania.'; */
                err_msg = 'Nie wskazano opakowania, do którego dodać towar.';
                rstatus = 0;
                rwersjaref = 0;
                rpozycja = 0;
                suspend;
                exit;
            end
        
            --sprawdzenie czy istnieje towar
            select ref from XK_LISTYWYSDROZ_GETTOWAR(:bar_code, :klient) into :rwersjaref;
    
            if (coalesce(:rwersjaref, 0) = 0) then
            begin
                err_msg = 'Nie znaleziono towaru o podanym kodzie kreskowym.';
                rwersjaref = 0;
                rstatus = 0;
                rpozycja = 0;
                suspend;
                exit;
            end
    end
    else
      rwersjaref=wersjaref;

            -- Weryfikacja czy nie przekroczono ilosci towaru

            select sum(lwp.iloscmax), sum(lwp.iloscspk)
            from listywysdpoz lwp
            where lwp.dokument = :sped_doc and lwp.wersjaref = :rwersjaref
            into :iloscmax, :iloscspk;

            iloscmax = coalesce(iloscmax, 0);
            iloscspk = coalesce(iloscspk, 0);

            if (iloscspk + (ilosc * mnoznik) > iloscmax or iloscmax = 0) then --przekroczono lub brak towaru
            begin
                err_msg = 'Przekroczono ilość towaru do spakowania lub brak wskazanego towaru na dokumencie magazynowym.';
                rwersjaref = 0;
                rstatus = 0;
                rpozycja = 0;
                suspend;
                exit;
            end
            status = 1;



            /*
    
                select first 1 lwp.ref
                from listywysdpoz lwp
                left join listywysdroz lwr on (lwp.ref = lwr.listywysdpoz)
                where lwp.dokument = :sped_doc and lwp.wersjaref = :rwersjaref
                group by lwp.ref, lwp.ilosc
                having sum(coalesce(lwr.iloscmag, 0)) < lwp.ilosc
                into :rpozycja;


                if (rpozycja is null) then
                begin
                    err_msg = 'Przekroczono ilość towaru do spakowania lub brak wskazanego towaru na dokumencie magazynowym.';
                    rwersjaref = 0;
                    rstatus = 0;
                    rpozycja = 0;
                    suspend;
                    exit;
                end
                else pozycja = :rpozycja;
    

                iloscroz = 0;
                iloscpoz = 0;

                --kontrola ilosci spakowanej
                for select lwp.ref
                    from listywysdpoz lwp
                    left join listywysdroz lwr on (lwp.ref = lwr.listywysdpoz)
                    where lwp.dokument = :sped_doc and lwp.wersjaref = :rwersjaref
                    group by lwp.ref, lwp.ilosc
                    having sum(coalesce(lwr.iloscmag, 0)) < lwp.ilosc
                into :rpozycja
                do begin
                    select sum(lr.iloscmag), lp.ilosc
                        from listywysdpoz lp
                        --from listywysdroz lr
                        left join listywysdroz lr on (lr.listywysdpoz = lp.ref)
                        where lp.ref = :rpozycja
                        group by lp.ilosc
                    into :iloscroz_tmp, :iloscpoz_tmp;

                    iloscroz = iloscroz + coalesce(iloscroz_tmp,0);
                    iloscpoz = iloscpoz + coalesce(iloscpoz_tmp,0);
                end

                if (iloscroz >= iloscpoz  or  ilosc * mnoznik > :iloscpoz) then
                begin
                    err_msg = 'Przekroczono ilość towaru do spakowania.';
                    rwersjaref = 0;
                    rstatus = 0;
                    rpozycja = 0;
                    suspend;
                    exit;
                end
                else status = 1; */

    
        if (:status = 1) then
        begin
                select nropk
                  from listywysdroz_opk
                  where listwysd = :sped_doc
                    and stanowisko = :active_box_code
                    and stan = 0
                  into :maxnropk;
/*
                if (coalesce(:pozycja, 0) = 0) then
                begin
                    err_msg = 'Błąd dodawania pozycji pakowania.';
                    rwersjaref = :wersjaref;
                    rpozycja = :pozycja;
                    rstatus = 0;
                    suspend;
                    exit;
                end
*/
                --tutaj
                select first 1 lwp.ref
                  from listywysdpoz lwp
                  where lwp.wersjaref = :rwersjaref
                    and lwp.iloscmax = (:ilosc * :mnoznik)
                    and coalesce(lwp.iloscspk, 0) = 0
                    and lwp.dokument = :sped_doc
                into :rpozycja;

                if(:rpozycja is not null) then begin
                  insert into listywysdroz(listywysd, listywysdpoz, iloscmag, serialnum, nrkartonu, opakowania, opk, pakowal, x_data_pak)
                    values(:sped_doc, :rpozycja, (:ilosc * :mnoznik), '', cast(:maxnropk as varchar(20)),
                    (select path from xk_listywysd_packing_path(:active_box_ref)), :active_box_ref, :operator, current_timestamp);
                end
                else begin
                  ilosc = :ilosc * :mnoznik;
                  for select lwp.ref, lwp.iloscmax, coalesce(lwp.iloscspk, 0)
                        from listywysdpoz lwp
                        where lwp.dokument = :sped_doc
                          and lwp.wersjaref = :rwersjaref
                          and lwp.iloscmax > coalesce(lwp.iloscspk, 0)
                        order by coalesce(lwp.iloscspk, 0), lwp.iloscmax
                      into :rpozycja, :iloscmax, :iloscspk
                  do begin
                    if (:ilosc > 0) then begin
                      if (:ilosc >= (:iloscmax - :iloscspk)) then begin
                        rilosc = :iloscmax - :iloscspk;
                        ilosc = :ilosc - :rilosc;
                      end
                      else begin
                        rilosc = :ilosc;
                        ilosc = 0;
                      end
                        insert into listywysdroz(listywysd, listywysdpoz, iloscmag, serialnum, nrkartonu, opakowania, opk, pakowal, x_data_pak)
                        values(:sped_doc, :rpozycja, :rilosc, '', cast(:maxnropk as varchar(20)),
                        (select path from xk_listywysd_packing_path(:active_box_ref)), :active_box_ref, :operator, current_timestamp);
                    end
                  end
                end



                --tutaj



                /*
                -- dodawanie wszystkich wpisów (ale z uwzgldnieniem rozdzielonych pozycji tego samego produktu)
                for select lwp.ref, 1
                    from listywysdpoz lwp
                    left join listywysdroz lwr on (lwp.ref = lwr.listywysdpoz)
                    where lwp.dokument = :sped_doc and lwp.wersjaref = :rwersjaref
                    group by lwp.ref, lwp.ilosc
                    having sum(coalesce(lwr.iloscmag, 0)) < lwp.ilosc
                into: rpozycja--, ilosc
                do begin
                    if (mnoznik > 0) then
                    begin
                        insert into listywysdroz(listywysd, listywysdpoz, iloscmag, serialnum, nrkartonu, opakowania)
                        values(:sped_doc, :rpozycja, :ilosc, '', cast(:maxnropk as varchar(20)),
                        (select path from xk_listywysd_packing_path(:active_box_ref)));
                    end
                    mnoznik = mnoznik - 1;
                end  */

                err_msg = 'Wskaż opakowanie bądź towar.';
                info_msg = 'Dodano towar do opakowania.';
                rstatus = 1;
                rwersjaref = 0;
                rpozycja = 0;
                suspend;
                exit;
        end

    err_msg = 'Uwaga: Nic nie wykonano!'; -- tego nie ma być!
    rstatus = 1;
    suspend;
end^
SET TERM ; ^
