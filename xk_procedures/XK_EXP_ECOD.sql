--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_EXP_ECOD(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
begin
  if(:otable='NAGFAK') then begin
    -- eksport faktury
    for select ID,PARENT,NAME,params,VAL
      from XK_EXP_ECOD_INVOICE(:OTABLE,:OREF)
      into :id,:parent,:name,:params,:val
    do begin
      suspend;
    end
  end else if (:otable='LISTYWYSD') then begin
    -- eksport dokumentu spedycyjnego
    for select ID,PARENT,NAME,params,VAL
      from XK_EXP_ECOD_DESADV(:OTABLE,:OREF)
      into :id,:parent,:name,:params,:val
    do begin
      suspend;
    end
  end
end^
SET TERM ; ^
