--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_GET_BEST_MWSCONSTLOCL(
      GOOD varchar(20) CHARACTER SET UTF8                           ,
      VERS integer,
      REFMWSACT integer,
      REFMWSORD integer,
      MWSORDTYPE integer,
      WH varchar(3) CHARACTER SET UTF8                           ,
      WHSEC integer,
      WHAREA integer,
      MAXLEVEL smallint,
      PRIORITY smallint,
      PAL smallint,
      MIX smallint,
      MWSCONSTLOCB integer,
      PALTYPE varchar(40) CHARACTER SET UTF8                           ,
      PALW numeric(14,2),
      PALL numeric(14,2),
      PALH numeric(14,2),
      REFILLTRY smallint)
  returns (
      MWSCONSTLOCP integer,
      MWSPALLOCP integer,
      MWSCONSTLOCL integer,
      MWSPALLOCL integer,
      WHAREAL integer,
      WHAREALOGP integer,
      WHAREALOGL integer,
      NEWGOOD varchar(20) CHARACTER SET UTF8                           ,
      WHSECOUT integer,
      REFILL smallint)
   as
begin
  suspend;
end^
SET TERM ; ^
