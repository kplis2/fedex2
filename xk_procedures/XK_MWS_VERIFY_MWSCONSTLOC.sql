--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_VERIFY_MWSCONSTLOC(
      OPERATOR integer,
      MWSCONSTLOC integer,
      MWSORDTYPE integer,
      WH varchar(3) CHARACTER SET UTF8                           )
  returns (
      OREF integer)
   as
declare variable verify smallint;
declare variable verifyon integer;
declare variable verifyons varchar(100);
declare variable cnt integer;
declare variable locdest integer;
declare variable abc varchar(10);
declare variable locsymb varchar(40);
begin
  -- sprawdzamy czy globalnie jest wlaczona weryfikacja
  execute procedure get_config('MWSCONSTLOCVERIFYON',2)
    returning_values verifyons;
  if (verifyons = '' or verifyons is null) then
    verifyon = 0;
  else
    verifyon = cast(verifyons as integer);
  if (verifyon = 1) then
  begin
    -- sprawdzamy czy typ zlecenia ma wlaczona weryfikacje
    select coalesce(t.verifymwsconstloc,0)
      from mwsordtypes t
      where t.ref = :mwsordtype
      into verifyon;
    if (verifyon = 1) then
    begin
      -- sprawdzamy czy typ operator ma
      select coalesce(o.verifymwsconstloc,0)
        from opermag o
        where o.operator = :operator
          and o.magazyn = :wh
        into verifyon;
      if (verifyon = 1) then
      begin
        -- sprawdzamy czy lokacja potrzebuje inwentaryzacji
        select c.abc, c.mwsoutactscnt, c.symbol, coalesce(c.locdest,0)
          from mwsconstlocs c
          where c.ref = :mwsconstloc --and c.goodsav > 0
          into abc, cnt, locsymb, locdest;
        if (cnt is null) then cnt = 0;
        if (locdest is null) then locdest = 0;
        if (locdest not in (1,3)) then
          verifyon = 0;
        else if ((abc = 'A' or abc = 'C') and cnt < 30) then
          verifyon = 0;
        else if (abc = 'B' and cnt < 20) then
          verifyon = 0;
      end
    end
  end
  -- zakladamy zlecenie inwentaryzacji i robimy je
  if (verifyon = 1) then
  begin
    update mwsconstlocs set mwsoutactscnt = 0 where ref = :mwsconstloc;
    execute procedure  XK_MWS_GEN_MWSORD_LOC_STOCK(NULL, NULL, NULL, 'B', null, null, 1, null, :operator, NULL, :locsymb, NULL)
      returning_values oref;
  end
end^
SET TERM ; ^
