--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_TRANSFER_FXDASSETS as
declare variable S1 varchar(255);
declare variable S2 varchar(255);
declare variable S3 varchar(255);
declare variable S4 varchar(255);
declare variable S5 varchar(255);
declare variable S6 varchar(255);
declare variable S7 varchar(255);
declare variable S8 varchar(255);
declare variable S9 varchar(255);
declare variable S10 varchar(255);
declare variable S11 varchar(255);
declare variable S12 varchar(255);
declare variable S13 varchar(255);
declare variable S14 varchar(255);
declare variable S15 varchar(255);
declare variable S16 varchar(255);
declare variable S17 varchar(255);
declare variable S18 varchar(255);
declare variable S19 varchar(255);
declare variable S20 varchar(255);
declare variable S21 varchar(255);
declare variable S22 varchar(255);
declare variable S23 varchar(255);
declare variable SYMBOL varchar(20);
declare variable NAME varchar(80);
declare variable GRUPA varchar(10);
declare variable ODDZIAL varchar(10);
declare variable WYDZIAL varchar(10);
declare variable PURCHASEDT timestamp;
declare variable USINGDT timestamp;
declare variable SERIALNO varchar(20);
declare variable DESCRIPT varchar(255);
declare variable TBKSYMBOL BKSYMBOLS_ID;
declare variable FBKSYMBOL BKSYMBOLS_ID;
declare variable TAMETH varchar(3);
declare variable FAMETH varchar(3);
declare variable TARATE numeric(10,2);
declare variable FARATE numeric(10,2);
declare variable TACRATE numeric(10,2);
declare variable FACRATE numeric(10,2);
declare variable TVALUE numeric(15,2);
declare variable FVALUE numeric(15,2);
declare variable TREDEMPTION numeric(15,2);
declare variable FREDEMPTION numeric(15,2);
declare variable PERSON integer;
declare variable PVALUE numeric(15,2);
begin
  for select s1, s2,  s3,  s4,  s5,  s6,  s7,  s8,  s9,  s10,
            s11, s12, s13, s14, s15, s16, s17, s18, s19, s20,
            s21, s22, s23
    from expimp 
  into :s1, :s2,  :s3,  :s4,  :s5,  :s6,  :s7,  :s8,  :s9,  :s10,
      :s11, :s12, :s13, :s14, :s15, :s16, :s17, :s18, :s19, :s20,
      :s21, :s22, :s23
  do begin

-- symbol ST
    if(coalesce(char_length(:s1),0)>20) then -- [DG] XXX ZG119346
      exception universal 'Symbol ' || :s1 || ' jest za dlugi.';
    else begin
      symbol = substring(:s1 from  1 for  20);
      if (exists(select first 1 1 from fxdassets where symbol=:symbol)) then
        exception universal 'ST o symbolu: ' || :s1 || ' juz znajduje sie w bazie';
    end

-- nazwa ST
    if(coalesce(char_length(:s2),0)>80) then -- [DG] XXX ZG119346
      exception universal 'Nazwa ST ' || :s1 || ' jest za dluga.';
    else
      name = substring(:s2 from  1 for  80);

-- grupa ST
    if(coalesce(char_length(:s3),0)>10) then -- [DG] XXX ZG119346
      exception universal 'Grupa ST ' || :s3 || ' jest za dluga.';
    else
      grupa = substring(:s3 from  1 for  10);
      if (not exists(select first 1 1 from amortgrp where symbol=:grupa)) then
        exception universal 'Grupa ' || :grupa || ' nie istnieje.';

-- oddzial
    if(coalesce(char_length(:s4),0)>10) then -- [DG] XXX ZG119346
      exception universal 'Nazwa oddzialu ST ' || :s1 || ' jest za dluga.';
    else
      oddzial = substring(:s4 from  1 for  10);
    
    if (not exists(select first 1 1 from oddzialy where oddzial=:oddzial)) then
      exception universal 'Oddzial ' || :oddzial || ' nie istnieje.';

-- wydzial
    if(coalesce(char_length(:s5),0)>10) then -- [DG] XXX ZG119346
      exception universal 'Nazwa wydzialu ST ' || :s1 || ' jest za dluga.';
    else
      wydzial = substring(:s5 from  1 for  10);

    if (not exists(select first 1 1 from departments where symbol=:wydzial)) then
      exception universal 'Wydzial ' || :wydzial || ' nie istnieje.';

-- data zakupu
    if(s6 like '____-__-__') then
      purchasedt = cast(:s6 as date);
    else
      exception universal 'Bledny format daty zakupu przy ST: ' || :s1;

-- data uzytkowania
    if(s7 like '____-__-__') then
      usingdt = cast(:s7 as date);
    else
      exception universal 'Bledny format daty uzytkowania przy ST: ' || :s1;


-- nr seryjny
    if(coalesce(char_length(:s8),0)>20) then -- [DG] XXX ZG119346
      exception universal 'Nr seryjny  ' || :s8 || ' jest za dlugi.';
    else
      serialno = substring(:s8 from  1 for  20);

-- opis
    descript = :s9;

-- konto podatkowe
    if(coalesce(char_length(:s10),0)>20) then -- [DG] XXX ZG119346
      exception universal 'Konto podatkowe  ' || :s10 || ' jest za dlugie.';
    else begin
      tbksymbol = substring(:s10 from  1 for  20);
      if (not exists (select first 1 1 from bksymbols where symbol=:tbksymbol and sgroup='EFK')) then
        insert into bksymbols(symbol, name, sgroup)
        values (:tbksymbol, '', 'EFK');
    end

-- konto finansowe
    if(coalesce(char_length(:s11),0)>20) then -- [DG] XXX ZG119346
      exception universal 'Konto finansowe  ' || :s11 || ' jest za dlugie.';
    else begin
      fbksymbol = substring(:s11 from  1 for  20);
      if (not exists (select first 1 1 from bksymbols where symbol=:fbksymbol and sgroup='EFK')) then
        insert into bksymbols(symbol, name, sgroup)
        values (:fbksymbol, '', 'EFK');
    end

-- metoda amortyzacji podatkowej
    if(:s12='L') then
      tameth = 'LIP';
    else if (:s12='D') then
      tameth = 'DPR';
    else if (:s12='U') then
      tameth = 'UJP';
    else
      exception universal 'Metoda amortyzacji podatkowej srodka: ' || :s1 || ' jest bledna.';


-- metoda amortyzacji finansowej
    if(:s13='L') then
      fameth = 'LIF';
    else if (:s13 = 'D') then
      fameth = 'DFR';
    else if (:s13 = 'U') then
      fameth = 'UJF';
    else
      exception universal 'Metoda amortyzacji finansowej srodka: ' || :s1 || ' jest bledna.';


-- stawka amortyzacji podatkowej
    s14 = replace(:s14, ',', '.');
    tarate = cast(:s14 as numeric(10,2));
    if (tarate > 100.00 or tarate < 0.0) then
      exception universal 'Bledna stawka amortyzacji podatkowej srodka: '|| :s1;


-- stawka amortyzacji finansowej
    s15 = replace(:s15, ',', '.');
    tarate = cast(:s15 as numeric(10,2));
    if (tarate > 100.00 or tarate < 0.0) then
      exception universal 'Bledna stawka amortyzacji finansowej srodka: '|| :s1;


-- korekta amortyzacji podatkowej
    s16 = replace(:s16, ',', '.');
    tacrate = cast(:s16 as numeric(10,2));


-- korekta amortyzacji finansowej
    s17 = replace(:s17, ',', '.');
    facrate = cast(:s17 as numeric(10,2));


-- wartosc podatkowa
    s18 = replace(:s18, ',', '.');
    tvalue = cast(:s18 as numeric(15,2));


-- wartosc finansowa
    s19 = replace(:s19, ',', '.');
    fvalue = cast(:s19 as numeric(15,2));


-- umorzenie podatkowe
    s20 = replace(:s20, ',', '.');
    tredemption = cast(:s20 as numeric(15,2));


-- umorzenie finansowe
    s21 = replace(:s21, ',', '.');
    fredemption = cast(:s21 as numeric(15,2));


-- osoba
    select ref from cpersons where symbol=cast(:s22 as varchar(20))
    into :person;
    if(person is null) then
      exception universal 'Nie ma w bazie osoby o koncie ksiegowym: ' || :s22 ;


-- cena zakupu
    s23 = replace(:s23, ',', '.');
    pvalue = cast(:s23 as numeric(15,2));



-- INSERT
  insert into fxdassets (symbol, name, amortgrp, oddzial, department, purchasedt, usingdt, serialno, descript, 
                         tbksymbol, fbksymbol, tameth, fameth, tarate, farate, tacrate, facrate,
                         tvalue, fvalue, tredemption, fredemption, person, pvalue)
              values (:symbol,  :name, :grupa, :oddzial, :wydzial, :purchasedt, :usingdt, :serialno, :descript, 
                      :tbksymbol, :fbksymbol, :tameth, :fameth, :tarate, :farate, :tacrate, :facrate,
                      :tvalue, :fvalue, :tredemption, :fredemption, :person, :pvalue);
  end
end^
SET TERM ; ^
