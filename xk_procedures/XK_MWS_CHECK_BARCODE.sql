--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_CHECK_BARCODE(
      MWSACTREF integer,
      BARCODE varchar(40) CHARACTER SET UTF8                           )
  returns (
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      QUANTITY numeric(14,4),
      STATUS smallint,
      ISBARCODE smallint)
   as
declare variable tktm varchar(40);
declare variable unit varchar(5);
declare variable przelicz numeric(14,4);
declare variable mwsbarcode varchar(40);
declare variable goodbarcode varchar(40);
declare variable tymprzelicz numeric(14,4);
declare variable versbarcode integer;
declare variable unitid integer;
begin
/*
  status = 0  - KOD WYMAGANY
  status = 1  - WSZYSTKO OK
  status = 2 - NIEPRAIWDLOWY KOD
*/
  isbarcode = 0;
  status = 1;
  select ma.good, ma.vers
    from mwsacts ma
    where ma.ref = :mwsactref
    into :good, :vers;
  execute procedure xk_mws_barcode_validation(:barcode)
    returning_values :goodbarcode, :versbarcode, :przelicz, :status, :unit, :unitid;
  if (status = 0) then begin
    isbarcode = 0;
  end else begin
    if (goodbarcode <> good or versbarcode <> vers) then
    begin
      status = 2;
      isbarcode = 0;
      quantity = 0;
    end else begin
      isbarcode = 1;
      quantity = przelicz;
    end
  end
  if (isbarcode = 0 and status <> 2) then begin
    quantity = cast(:barcode as numeric(14,4));
    status = 1;
  end
  suspend;
end^
SET TERM ; ^
