--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_PALLOC_CHANGESIZE(
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      L numeric(15,4),
      W numeric(15,4),
      H numeric(15,4))
   as
begin
  update mwsconstlocs m set m.l = :l, m.w = :w, m.h = :h
    where m.symbol = :symbol;
end^
SET TERM ; ^
