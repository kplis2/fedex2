--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_GET_KODKRESK(
      GRUPAKUP integer,
      REFWERSJI integer,
      JEDNOSTKA integer)
  returns (
      SYMBOL varchar(20) CHARACTER SET UTF8                           )
   as
begin
  symbol = '';

  select coalesce(gr.kodkresk,'')
    from grupykupofe gr
  where gr.grupakup = :grupakup
    and gr.wersjaref = :refwersji
    and gr.jedn = :jednostka
  into :symbol;

  if(coalesce(:symbol,'') = '') then
  begin
    select coalesce(wr.kodkresk,'')
      from wersje wr
    where wr.ref = :refwersji
    into :symbol;
  end

  if(coalesce(:symbol,'') = '') then
  begin
    select coalesce(tr.kodkresk,'')
      from towary tr
      join wersje wr on (tr.ktm = wr.ktm)
    where wr.ref = :refwersji
    into :symbol;
  end

  symbol = coalesce(:symbol,'');
  suspend;
end^
SET TERM ; ^
