--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSORDLIST(
      WH varchar(5) CHARACTER SET UTF8                           ,
      OPER integer,
      WHSECGROUPS varchar(40) CHARACTER SET UTF8                            = '')
  returns (
      LOGIN varchar(20) CHARACTER SET UTF8                           ,
      SLOKOD varchar(40) CHARACTER SET UTF8                           ,
      INT_SYMBOL STRING255,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      DOCSYMBS varchar(100) CHARACTER SET UTF8                           ,
      STATUS smallint,
      OPERATOR integer,
      MWSORDTYPES varchar(4) CHARACTER SET UTF8                           ,
      MWSORDTYPE integer,
      STOCKTAKING smallint,
      DESCRIPTION varchar(3500) CHARACTER SET UTF8                           ,
      WEIGHT numeric(15,4),
      REF integer,
      MWSACTSNUMBER integer,
      BWHAREA varchar(20) CHARACTER SET UTF8                           ,
      SHIPPINGTYPE varchar(255) CHARACTER SET UTF8                           ,
      PRIORITY smallint,
      MWSFIRSTCONSTLOCP varchar(20) CHARACTER SET UTF8                           ,
      SORTFIELD integer,
      MWSORDTYPEDEST smallint,
      FROMMWSORD integer,
      MANMWSACTS smallint)
   as
declare variable sortpriority smallint;
declare variable sortdata smallint;
declare variable sortmasterlink smallint;
declare variable sortopek smallint;
declare variable sortodbwlasny smallint;
declare variable sortpriority0 smallint;
declare variable sortpriority1 smallint;
declare variable sortpriority17 smallint;
declare variable sortpriority7 smallint;
declare variable sortactsnum9 smallint;
declare variable sortactsnum10_30 smallint;
declare variable sortactsnum30 smallint;
declare variable sortweight70 smallint;
declare variable sortweight70_200 smallint;
declare variable sortweight200_1200 smallint;
declare variable sortweight1200 smallint;
declare variable sorttypea smallint;
declare variable sorttypeb smallint;
declare variable sorttypec smallint;
declare variable nosortodbwlas smallint;
declare variable sortadystrybutor smallint;
declare variable sql varchar(4096);
declare variable grupa varchar(1024);
declare variable palquantity integer;
begin
  if (whsecgroups = ';0000;') then whsecgroups = '';

  --XX JO:
  whsecgroups = '';

  sortfield = 0;
  select o.grupa
    from operator o
    where o.ref = :oper
    into :grupa;
  if (grupa is null) then grupa = '';
  for
    select distinct mo.symbol, mo.status, case when d.zewn = 1 and d.wydania = 0 then coalesce(mo.docsymbs,'') || coalesce(d.uwagi,'') else mo.docsymbs end, mo.mwsordtype, mo.mwsordtypes, mo.operator,  mo.stocktaking,
           coalesce(mo.description,''), mo.weight, mo.ref, mo.mwsactsnumber, mo.priority, coalesce(mo.slokod,'') , mo.mwsfirstconstlocp,
             coalesce(o.login,''), w.symbol, s.nazwa, mo.mwsordtypedest, mo.frommwsord, mo.manmwsacts, d.int_symbol
      from mwsords mo
        join mwsordtypedest4op ot on((ot.mwsordtypedest = mo.mwsordtypedest or ot.mwsordtypedest = 0)
          and ot.operator = :oper and ot.wh = :wh)
        left join mwsconstlocs mc on(mc.ref = mo.mwsfirstconstlocpref)
        left join operator o on(o.ref = mo.operator)
        left join whareas w on(w.ref = mo.bwharea)
        left join sposdost s on(s.ref = mo.shippingtype)
        left join dokumnag d on(mo.docid = d.ref and d.blokada <> 1 and d.blokada <> 5)
      where (mo.operator = :oper or mo.operator is null or :oper is null)
        and mo.wh = :wh
        and mo.status > 0 and mo.status < 3 -- or (mo.status = 7)) --Ldz
        and mo.mwsordtypedest not in(3,2)
        and mo.takefullpal <> 1
        and (mo.whsecgroups = :whsecgroups or coalesce(:whsecgroups,'') = '')
        and (cast(d.termdost as date) <= current_date or d.termdost is null or d.realfaster = 1)
      order by
      case when mo.operator is not null then 0 else 1 end,
        case -- Ldz 131465 - kolejnosc wyswietlania zlecen, WT, PT, ZT, WG, LI, IN, PD
          when mo.mwsordtype = 6 then 1
          when mo.mwsordtype = 8 then 2
          when mo.mwsordtype = 25 then 3
          when mo.mwsordtype = 22 then 4
          when mo.mwsordtype = 18 then 5
          when mo.mwsordtype = 10 then 6
          when mo.mwsordtype = 11 then 7
          when mo.mwsordtype = 14 then 8
          else 10
        end,
        mo.priority,
        mo.status desc


      into :symbol, :status, :docsymbs, :mwsordtype, :mwsordtypes, :operator, :stocktaking,
              :description, :weight, :ref, :mwsactsnumber, :priority, :slokod, :mwsfirstconstlocp,
                :login, :bwharea, :shippingtype, :mwsordtypedest, :frommwsord, :manmwsacts, :int_symbol
  do begin
    sortfield = sortfield + 1;
    suspend;
  end
end^
SET TERM ; ^
