--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSCONSTLOC_FIND_WHAREA as
declare variable w integer;
declare variable e integer;
declare variable n integer;
declare variable s integer;
declare variable r integer;
declare variable l integer;
begin
  for
    select w.ref, w.logwhareas, w.logwharean, w.logwhareaw, w.logwhareae
      from whareas w
      where w.areatype in (1,3)
      into r, s, n, w, e
  do begin
    l = w;
    if (l is null) then
      l = e;
    if (l is null) then
      l = n;
    if (l is null) then
      l = s;
    if (l is not null) then
    begin
      update mwsconstlocs c set c.wharealogfromstartarea = :l where wharea = :r;
    end
  end
end^
SET TERM ; ^
