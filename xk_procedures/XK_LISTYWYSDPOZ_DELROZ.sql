--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSDPOZ_DELROZ(
      LISTWYSD integer,
      WERSJAREF integer,
      ILOSCDEL numeric(14,4),
      OPKREF integer)
  returns (
      STATUS smallint)
   as
declare variable refpoz integer;
declare variable refroz integer;
declare variable roznica numeric(14,4);
declare variable iloscmag numeric(14,4);
declare variable ilosc numeric(14,4);
begin
  status = 0;
  select sum(lwr.iloscmag)
    from listywysdroz lwr
      join listywysdpoz lwp on (lwr.listywysdpoz = lwp.ref)
    where lwr.listywysd = :listwysd
      and lwp.wersjaref = :wersjaref
      and lwr.opk = :opkref
  into :ilosc;
  if (coalesce(:ilosc, 0) < :iloscdel) then begin
    status = -1;
    suspend;
    exit;
  end
  else if (:ilosc = :iloscdel) then begin
    for select lwr.ref
          from listywysdroz lwr
            join listywysdpoz lwp on (lwr.listywysdpoz = lwp.ref)
          where lwr.listywysd = :listwysd
            and lwp.wersjaref = :wersjaref
            and lwr.opk = :opkref
        into :refroz
    do begin
      delete from listywysdroz where ref = :refroz;
    end
    status = 1;
    suspend;
    exit;
  end
  else begin
    ilosc = :iloscdel;
    for select lwp.ref, lwr.ref, lwr.iloscmag, lwr.iloscmag - :iloscdel
          from listywysdroz lwr
            join listywysdpoz lwp on (lwr.listywysdpoz = lwp.ref)
          where lwp.wersjaref = :wersjaref
            and lwr.listywysd = :listwysd
            and lwr.opk = :opkref
          order by abs(lwr.iloscmag - :iloscdel)
        into :refpoz, :refroz, :iloscmag, :roznica
    do begin
      if (:refpoz is null or :refroz is null) then begin
        status = 0;
        suspend;
        exit;
      end

      else if (:roznica = 0) then begin
        delete from listywysdroz
          where ref = :refroz;
        status = 1;
        suspend;
        exit;
      end
      else begin
        if (:roznica > 0) then begin
          update listywysdroz set iloscmag = iloscmag - :iloscdel
            where ref = :refroz;
          status = 1;
          suspend;
          exit;
        end
        if (:roznica < 0) then begin
          if (:ilosc > 0 and :iloscmag <= :ilosc) then begin
            delete from listywysdroz
              where ref = :refroz;
            ilosc = ilosc - :iloscmag;
          end
          else if (:ilosc > 0 and :iloscmag > :ilosc ) then begin
            update listywysdroz set iloscmag = iloscmag - :ilosc
              where ref = :refroz;
            status = 1;
            suspend;
            exit;
          end
          else if (:ilosc = 0) then begin
            status = 1;
            suspend;
            exit;
          end
        end
      end
      suspend;
    end
  end
end^
SET TERM ; ^
