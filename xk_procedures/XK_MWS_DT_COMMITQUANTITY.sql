--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_DT_COMMITQUANTITY(
      MWSORD integer,
      MWSACT integer,
      VERS integer,
      QUANTITY numeric(14,4),
      MWSCONSTLOCLS varchar(20) CHARACTER SET UTF8                           ,
      X_PARTIAS STRING,
      X_SERIAL_NO STRING,
      X_SLOWNIK STRING)
  returns (
      STATUS smallint)
   as
declare variable lquan numeric(14,4);
declare variable lquanc numeric(14,4);
declare variable mwsconstlocl integer;
declare variable actmwsconstlocl integer;
declare variable lactref integer;
declare variable maxnumber integer;
declare variable mwspallocl integer;
declare variable newmwsactref integer;
declare variable actstatus smallint;
declare variable x_partia date_id;
begin

  if (X_SLOWNIK = '' or X_SLOWNIK = '0' ) then X_SLOWNIK = null;
  if (X_SERIAL_NO = '') then X_SERIAL_NO = null;
  if (coalesce(X_PARTIAS,'') <> '') then
    x_partia = cast(x_partias as date_id);

  status = 1;


  select first 1 m.ref
    from mwsconstlocs m
    where m.symbol = :mwsconstlocls
    into :mwsconstlocl;
 /* select first 1 m.mwspalloc
    from mwsstock m
    where m.mwsconstloc = :mwsconstlocl
    into :mwspallocl; */
  select a.mwspallocp
    from mwsacts a
    where a.ref = :MWSACT
  into :mwspallocl;

  if (mwspallocl is null) then begin
    execute procedure gen_ref('MWSPALLOCS') returning_values mwspallocl;
    insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status)
      select :mwspallocl, symbol, ref, 2, 0
        from mwsconstlocs
        where ref = :mwsconstlocl;
  end
  select sum(ma.quantity)
    from mwsacts ma
    where ma.mwsord = :mwsord
      and ma.vers = :vers
      and ma.status < 3 --and ma.status > 0
   --   and (ma.mwsconstlocp <> ma.mwsconstlocl or  ma.mwsconstlocl is null)
      and ((:x_partia is not null and ma.x_partia = :x_partia) or :x_partia is null)               -- XXX KBI
      and ((:x_serial_no is not null and ma.x_serial_no = :x_serial_no) or :x_serial_no is null)   -- XXX KBI
      and ((:x_slownik is not null and ma.x_slownik = :x_slownik) or :x_slownik is null)           -- XXX KBI
    into lquan;
  if (quantity <= lquan) then begin
    for
      select ma.ref, ma.quantity, coalesce(ma.mwsconstlocl,0), ma.status
        from mwsacts ma
        where ma.mwsord = :mwsord
          and ma.status < 3-- and ma.status > 0
          and ma.vers = :vers
--          and (ma.mwsconstlocp <> ma.mwsconstlocl or ma.mwsconstlocl is null)
          and ((:x_partia is not null and ma.x_partia = :x_partia) or :x_partia is null)               -- XXX KBI
          and ((:x_serial_no is not null and ma.x_serial_no = :x_serial_no) or :x_serial_no is null)   -- XXX KBI
          and ((:x_slownik is not null and ma.x_slownik = :x_slownik) or :x_slownik is null)           -- XXX KBI
        order by
          case
            when ma.ref = :mwsact then 0
            when coalesce(ma.mwsconstlocp,0) <> coalesce(ma.mwsconstlocl,0) then 1
            else 2
          end
        into lactref, lquanc, actmwsconstlocl, status
    do begin
      if (quantity >= lquanc and actmwsconstlocl = mwsconstlocl) then begin
        update mwsacts ma set ma.status = 2, ma.quantityc = :lquanc where ma.ref = :lactref;
        quantity = quantity - lquanc;
      end else if (quantity >= lquanc and actmwsconstlocl <> mwsconstlocl) then begin
        update mwsacts ma set ma.status = 0 where ma.ref = :lactref;
        update mwsacts ma set ma.mwsconstlocl = :mwsconstlocl, ma.mwspallocl = :mwspallocl where ma.ref = :lactref;
        update mwsacts ma set ma.status = 1 where ma.ref = :lactref;
        update mwsacts ma set ma.status = 2, ma.quantityc = ma.quantity where ma.ref = :lactref;
        quantity = quantity - lquanc;
      end else begin
        update mwsacts ma set ma.status = 0, ma.quantity = ma.quantity - :quantity where ma.ref = :lactref;
        update mwsacts ma set ma.status = :status where ma.ref = :lactref;
        select max(ma.number)
          from mwsacts ma
          where ma.mwsord = :mwsord
          into maxnumber;
        maxnumber = maxnumber + 1;
        execute procedure gen_ref('MWSACTS') returning_values newmwsactref;
        insert into mwsacts (ref, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
            mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc,
            wh, wharea, whsec, timestart, timestop,
            regtime, timestartdecl, timestopdecl, mwsaccessory, realtimedecl,
            flags, descript, priority, lot, recdoc, plus,
            whareal, whareap, wharealogl, wharealogp, number, palgroup, autogen,
            x_partia, x_serial_no, x_slownik)   -- XXX KBI
          select :newmwsactref, ma.mwsord, 0, 0, ma.good, ma.vers, :quantity, ma.mwsconstlocp, ma.mwspallocp,
                :mwsconstlocl, :mwspallocl, ma.docid, ma.docposid, 'O', ma.settlmode, ma.closepalloc,
                ma.wh, ma.wharea,  ma.whsec, current_timestamp(0), current_timestamp(0),
                current_timestamp(0), ma.timestartdecl, ma.timestopdecl, ma.mwsaccessory, ma.realtimedecl,
                ma.flags, ma.descript, ma.priority, lot, 0, 1,
                null, ma.whareap, null, ma.wharealogp, :maxnumber, ma.palgroup, ma.autogen,
                x_partia, x_serial_no, x_slownik    -- XXX KBI
            from mwsacts ma where ma.ref = :lactref;
        update mwsacts ma set ma.status = 1 where ma.ref = :newmwsactref;
        update mwsacts ma set ma.status = 2, ma.quantityc = :quantity where ma.ref = :newmwsactref;
        quantity = 0;
      end
      if (quantity <= 0) then
        break;
    end
  end else
    exception MWS_INWALID_QUAN;
  suspend;
end^
SET TERM ; ^
