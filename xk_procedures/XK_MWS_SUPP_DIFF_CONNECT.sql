--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_SUPP_DIFF_CONNECT(
      MWSORD integer,
      MODE smallint)
  returns (
      STATUS integer)
   as
declare variable DOCID integer;
declare variable POSID integer;
declare variable NUMER integer;
declare variable MWSACTDET integer;
declare variable ILOSC numeric(14,4);
declare variable LOT integer;
declare variable WH varchar(3);
declare variable VERS integer;
declare variable GOOD varchar(40);
declare variable DOCTYPE varchar(3);
declare variable WHTYPE char(1);
declare variable FROMORDER integer;
declare variable LDOSTAWA integer;
declare variable LCENAMAG numeric(14,4);
declare variable I numeric(14,4);
declare variable J numeric(14,4);
declare variable DOCILOSC numeric(14,4);
declare variable VERSNUM integer;
declare variable DOCREFMINUS integer;
declare variable DOCREFPLUS integer;
declare variable MWSDISPOSITION smallint;
declare variable MWSORDTYPEDEST integer;
declare variable DOCGROUP integer;
declare variable DOKUMPOZREF integer;
declare variable REALIZEDQUANTITY numeric(15,4);
declare variable doctypeorg varchar(3);
declare variable supplier integer;
begin
--  exception test_break mwsord||', '||mode;

  select o.docid, o.docgroup, o.mwsordtypedest, o.slopoz
    from mwsords o
    where o.ref = :mwsord
    into docid, docgroup, mwsordtypedest, supplier;

  if (:docid is null) then docid = :docgroup;
  if (docid is null) then
  begin
    status = 0;
    select d.typdokmag
      from dostawcy d
      where d.ref = :supplier
      into doctypeorg;
    if (doctypeorg is null) then doctypeorg = 'PZ';
    execute procedure mws_gen_doc_from_mwsord(:mwsord,:doctypeorg) returning_values :docid;
    exit;
  end

  select dm.typ, d.magazyn, d.mwsdisposition
    from dokumnag d
      left join defmagaz dm on (dm.symbol = d.magazyn)
    where d.ref = :docid
  into whtype, wh, mwsdisposition;
  if (:mwsordtypedest = 1 and :mwsdisposition = 0) then
  begin
    status = 0;
    exit;
  end

  if (:mwsordtypedest = 2) then begin
    delete from MWSORDDOCDIFF where mwsord = :mwsord;
    -- dostawienie dokumentu na róznice ujemne do dokumentu, gdy na rozpiskach zlecenia
    -- nie potwierdzono w calosci jakiejs pozycji
    docrefminus = null;
    doctype = null;
    select t.stockdocminus
      from mwsords o
        left join mwsordtypes t on (t.ref = o.mwsordtype)
      where o.ref = :mwsord
      into doctype;
    if (doctype is null or doctype = '') then
      exception mwsord_not_exist 'Brak wskazania dokumentu rozliczającego na minus';
    select dm.typ, d.magazyn, d.mwsdisposition
      from dokumnag d
        left join defdokum df on (df.symbol = d.typ)
        left join defmagaz dm on (dm.symbol = d.magazyn)
      where d.ref = :docid
      into whtype, wh, mwsdisposition;
    execute procedure GEN_REF('DOKUMNAG') returning_values :docrefminus;
    for
      select good, vers, lot, wh, docilosc, ilosc
        from mws_supp_diff_pos (:docid, :mwsdisposition, 0)
        into good, vers, lot, wh, docilosc, ilosc
    do begin
      if (ilosc is null) then ilosc = 0;
      if (docilosc is null) then docilosc = 0;
      ilosc = docilosc - ilosc;
      if (ilosc > 0) then
      begin
        execute procedure GEN_REF('DOKUMPOZ') returning_values :posid;
        if (ilosc > 0) then
        begin
          -- najpierw próbujemy sciagnac stan z dostawy, która przyszla na magazyn z brakiem
          for
            select p.cena, p.dostawa, case when d.mwsdisposition = 1 then p.ilosc else p.iloscl end, p.fromzam, w.nrwersji
              from dokumpoz p
                left join dokumnag d on (d.ref = p.dokument)
                left join wersje w on (w.ref = p.wersjaref)
              where p.dokument = :docid and p.wersjaref = :vers
              order by case when p.dostawa = :lot then 1 else 2 end
              into :lcenamag, :ldostawa, :i, :fromorder, :versnum
          do begin
            if(:i is null) then i = 0;
            if (mwsdisposition = 0) then
              execute procedure DOKUMPOZ_ILDOST(:i, :fromorder, :whtype, :wh, :good, :versnum, :lcenamag, :ldostawa, :lot, :posid)
                returning_values :i;
            if (i > ilosc) then
              j = ilosc;
            else
              j = i;
            if (j > 0 and ilosc > 0) then
            begin
              numer = null;
              select NUMER
                from MWSORDDOCDIFF
                where POZYCJA=:posid and cena = :lcenamag and DOSTAWA = :ldostawa
                into :numer;
              if(:numer is null) then
              begin
                select max(numer) from MWSORDDOCDIFF where pozycja = :posid
                  into :numer;
                if(:numer is null or (:numer = 0))then numer = 0;
                numer = numer +  1;
                insert into MWSORDDOCDIFF(MAGAZYN,DOKUMENT,POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,FROMMWSACTDET, mwsord, doctype, wydania,wersja,wersjaref,ktm)
                  values (:wh, :docrefminus,:posid,:numer,:j,:lcenamag,:ldostawa,:mwsactdet,:mwsord,:doctype,1,:versnum,:vers,:good);
              end
              else
                update MWSORDDOCDIFF set ILOSC = ILOSC + :j where pozycja = :posid
                  and numer = :numer and dostawa = :ldostawa;
            end
            ilosc = ilosc - j;
          end
        end
        if (ilosc > 0 and mode = 1) then
        begin
          -- potem próbujemy sciagnac stany wedlug FIFO
          for
            select s.CENA,s.DOSTAWA,s.ILOSC-s.ZABLOKOW, s.wersja
              from STANYCEN s
              where s.WERSJAREF = :vers
                and s.MAGAZYN = :wh
                and ILOSC>0
              order by datafifo
              into :lcenamag, :ldostawa, :i, :versnum
          do begin
            if(:i is null) then i = 0;
              execute procedure DOKUMPOZ_ILDOST(:i, :fromorder, :whtype, :wh, :good, :versnum, :lcenamag, :ldostawa, :lot, :posid)
                returning_values :i;
            if (i > ilosc) then
              j = ilosc;
            else
              j = i;
            if (j > 0 and ilosc > 0) then
            begin
              numer = null;
              select NUMER
                from MWSORDDOCDIFF
                where POZYCJA=:posid and cena = :lcenamag and DOSTAWA = :ldostawa
                into :numer;
              if(:numer is null) then
              begin
                select max(numer) from MWSORDDOCDIFF where pozycja = :posid
                  into :numer;
                if(:numer is null or (:numer = 0))then numer = 0;
                numer = numer +  1;
                insert into MWSORDDOCDIFF(MAGAZYN,DOKUMENT,POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,FROMMWSACTDET, mwsord, doctype, wydania,wersja,wersjaref,ktm,docid)
                  values (:wh, :docrefminus,:posid,:numer,:j,:lcenamag,:ldostawa,:mwsactdet,:mwsord,:doctype,1,:versnum,:vers,:good,:docid);
              end
              else
                update MWSORDDOCDIFF set ILOSC = ILOSC + :j where pozycja = :posid
                  and numer = :numer and dostawa = :ldostawa;
            end
            ilosc = ilosc - j;
          end
        end
        if (ilosc > 0 and mwsdisposition = 0) then
          exception universal 'Brak możliwosci rozpisania '||doctype;
      end
    end
    docrefplus = null;
    doctype = null;
    select t.stockdocplus
      from mwsords o
        left join mwsordtypes t on (t.ref = o.mwsordtype)
      where o.ref = :mwsord
      into doctype;
    execute procedure GEN_REF('DOKUMNAG') returning_values :docrefplus;
    for
      select good, vers, wh, ilosc, docilosc
        from mws_supp_diff_pos (:docid, :mwsdisposition, 1)
        into good, vers, wh, ilosc, docilosc
    do begin
      lot = null;
      select min(md2.lot) from mwsactdets md2 where md2.docid = :docid and md2.vers = :vers
        into lot;
      select max(ref) from mwsactdets where docid = :docid and vers = :vers
        into mwsactdet;
      if (ilosc is null) then ilosc = 0;
      if (docilosc is null) then docilosc = 0;
      ilosc = ilosc - docilosc;
      if (ilosc > 0) then
      begin
        execute procedure GEN_REF('DOKUMPOZ') returning_values :posid;
        insert into MWSORDDOCDIFF (pozycja, dokument, ilosc, cena, dostawa, mwsord, doctype, wydania, numer,magazyn,wersja,wersjaref,ktm,docid)
          values (:posid, :docrefplus, :ilosc, 0, 0, :mwsord, :doctype, 0, 1,:wh,:versnum,:vers,:good,:docid);
      end
    end
    select count(a.ref)
      from mwsorddocdiff a
      where a.mwsord = :mwsord
      into status;
    if (status is not null and status > 0) then
      status = 1;
    else
      status = 0;
    if (mwsdisposition = 1 and status = 0) then
    begin
      execute procedure mws_close_disposition(:docid, 0, 1, 0);
    end
  end
  else if (:mwsordtypedest = 1) then begin
    delete from MWSORDDOCDIFF where mwsord = :mwsord;
    if (:mwsdisposition = 0) then
      exception universal 'Dokument źródłowy nie jest dyspozycją. Rozliczenie niemożliwe!';

    for
      select dp.ref
        from dokumnag dn
          left join dokumpoz dp on (dn.ref = dp.dokument)
        where dn.grupasped = :docgroup
      into :dokumpozref
    do begin
      realizedquantity = 0;
      select coalesce(sum(a.quantityc),0)
        from mwsacts a
        where a.docposid = :dokumpozref
         and a.status = 5
      into :realizedquantity;
      insert into MWSORDDOCDIFF(MAGAZYN,DOKUMENT,POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,FROMMWSACTDET, mwsord, doctype, wydania,wersja,wersjaref,ktm)
        --values (:wh, :docid,:posid,:numer,:j,:lcenamag,:ldostawa,:mwsactdet,:mwsord,:doctype,1,:versnum,:vers,:good);
        select dn.magazyn, dn.ref, dp.ref, dp.numer, abs(dp.ilosc - :realizedquantity), dp.cena,
               dp.dostawa, null, :mwsord, 'M', 1, dp.wersja, dp.wersjaref, dp.ktm
          from dokumnag dn
            left join dokumpoz dp on (dn.ref = dp.dokument)
          where dp.ref = :dokumpozref
            and dp.ilosc <> :realizedquantity;
    end
/*
    insert into MWSORDDOCDIFF(MAGAZYN,DOKUMENT,POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,FROMMWSACTDET, mwsord, doctype, wydania,wersja,wersjaref,ktm)
      --values (:wh, :docid,:posid,:numer,:j,:lcenamag,:ldostawa,:mwsactdet,:mwsord,:doctype,1,:versnum,:vers,:good);
      select dn.magazyn, dn.ref, dp.ref, dp.numer, abs(dp.ilosc - sum(coalesce(a.quantityc,0))), dp.cena,
             dp.dostawa, null, :mwsord, 'M', 1, dp.wersja, dp.wersjaref, dp.ktm
        from dokumnag dn
          left join dokumpoz dp on (dn.ref = dp.dokument)
          left join mwsacts a on (dp.ref = a.docposid)
        where dn.grupasped = :docgroup
          and a.status = 5
        group by dn.grupasped, dn.magazyn, dn.ref, dp.ref, dp.numer, dp.ilosc, dp.cena,
           dp.dostawa, :mwsord, 'M', 1, dp.wersja, dp.wersjaref, dp.ktm
        having abs(dp.ilosc - sum(coalesce(a.quantityc,0))) > 0;
*/
    insert into MWSORDDOCDIFF(MAGAZYN,DOKUMENT,POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,FROMMWSACTDET, mwsord, doctype, wydania,wersja,wersjaref,ktm)
      select dn.magazyn, dn.ref, 0, 0, sum(coalesce(a.quantityc,0)), 0,
             0, null, :mwsord, 'M', 0, w.nrwersji, a.vers, a.good
        from dokumnag dn
          left join mwsacts a on (dn.ref = a.docid and a.docposid = 0)
          left join wersje w on (a.vers = w.ref)
        where dn.grupasped = :docgroup
          and a.status = 5
          and a.docposid = 0
        group by dn.grupasped, dn.magazyn, dn.ref, w.nrwersji, a.vers, a.good;

    select count(a.ref)
      from mwsorddocdiff a
      where a.mwsord = :mwsord
      into status;
    if (status is not null and status > 0) then
      status = 1;
    else begin
      for
        select ref from dokumnag where grupasped = :docgroup
        into :docid
      do
        execute procedure mws_close_disposition(:docid, 0, 1, 0);
      status = 0;
    end
  end
  else
    exception universal 'Rozliczenie dyspozycji nie obejmuje zamykanego zlecenia!';
--  exception universal :mwsord || ' ' || :mode || ' ' || :status;
end^
SET TERM ; ^
