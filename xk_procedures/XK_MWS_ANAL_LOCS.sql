--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_ANAL_LOCS(
      WHIN varchar(3) CHARACTER SET UTF8                           )
  returns (
      WH varchar(3) CHARACTER SET UTF8                           ,
      ROW varchar(10) CHARACTER SET UTF8                           ,
      SYMBOL varchar(40) CHARACTER SET UTF8                           ,
      LOCQ numeric(14,4),
      LOCNOTAV numeric(14,4),
      MIX numeric(14,4))
   as
declare variable cref integer;
declare variable cfree integer;
begin
  for
    select c.wh, substring(c.symbol from 1 for 2), c.ref, c.symbol, f.ref
      from mwsconstlocs c
        left join mwsfreemwsconstlocs f on (f.ref = c.ref)
      where c.locdest is not null and (c.wh = :whin or :whin is null or :whin = '')
      into wh, row, cref, symbol, cfree
  do begin
    mix = 0;
    locq = 1;
    if (cfree is null) then
      locnotav = 1;
    else
      locnotav = 0;
    select first 1 coalesce(s.mixedpalgroup,0)
      from mwsstock s
      where s.mwsconstloc = :cref
      into mix;
    suspend;
  end
end^
SET TERM ; ^
