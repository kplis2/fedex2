--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_GET_KTMFROMEDI(
      KLIENT integer,
      EAN varchar(255) CHARACTER SET UTF8                           ,
      SYMBOLZEWN varchar(255) CHARACTER SET UTF8                           ,
      SYMBOLWEWN varchar(255) CHARACTER SET UTF8                           ,
      MIARAEDI varchar(10) CHARACTER SET UTF8                           )
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      JEDNO integer)
   as
declare variable grupakup integer;
declare variable wersjaref integer;
declare variable msg varchar(255);
declare variable miara varchar(10);
declare variable editype varchar(10);
begin
  if(:klient is null) then klient = 0;
  if(:symbolzewn is null) then symbolzewn = '';
  if(:symbolwewn is null) then symbolwewn = '';
  if(:ean is null) then ean = '';
  grupakup = 0;
  ktm = null;
  wersja = null;
  jedno = null;
  -- szukaj symbolzewn w grupykupofe
  if(:klient<>0) then select grupakup from klienci where ref=:klient into :grupakup;
  if(:symbolzewn<>'' and :grupakup<>0) then begin
    select first 1 ktm,wersja,jedn from grupykupofe where grupakup=:grupakup and symbol=:symbolzewn
    into :ktm,:wersja,:jedno;
  end
  -- szukaj ean w grupykupofe
  if(:ktm is null and :ean<>'' and :grupakup<>0) then begin
    select first 1 ktm,wersja,jedn from grupykupofe where grupakup=:grupakup and kodkresk=:ean
    into :ktm,:wersja,:jedno;
  end
  -- szukaj ean w towkodkresk
  if(:ktm is null and :ean<>'') then begin
    select first 1 ktm, wersjaref, towjednref from towkodkresk where kodkresk = :ean
    into :ktm, :wersjaref, :jedno;
    if(:wersjaref is not null) then select nrwersji from wersje where ref=:wersjaref into :wersja;
  end
  -- szukaj symbolwewn w towarach
  if(:ktm is null and :symbolwewn<>'') then begin
    select first 1 ktm,0,0 from towary where ktm=:symbolwewn
    into :ktm, :wersja, :jedno;
  end
  -- szukaj symbolzewn w towarach
  if(:ktm is null and :symbolzewn<>'') then begin
    select first 1 ktm,0,0 from towary where ktm=:symbolzewn
    into :ktm, :wersja, :jedno;
  end
  if(:ktm is null) then begin
    msg = 'Nie znaleziono towaru: '||:ean;
    if(:symbolzewn<>'') then msg = :msg ||' / '||:symbolzewn;
    if(:symbolwewn<>'') then msg = :msg ||' / '||:symbolwewn;
    exception UNIVERSAL :msg;
  end
  if(:wersja is null) then wersja = 0;
  if(:jedno is null) then jedno = 0;
  -- sprawdz czy jednostka wg edi odpowiada znalezionej jednostce
  if(:jedno<>0 and :miaraedi<>'') then begin
    select jedn from towjedn where ref=:jedno into :miara;
    select editype from miara where miara=:miara into :editype;
    if(:editype<>:miaraedi) then jedno = 0;
  end
  -- znajdz jednostke domyslna
  if(:jedno=0) then begin
    select dm from towary where ktm=:ktm into :jedno;
  end
  -- sprawdz czy jednostka wg edi odpowiada jednostce domyslnej
  if(:jedno<>0 and :miaraedi<>'') then begin
    select jedn from towjedn where ref=:jedno into :miara;
    select editype from miara where miara=:miara into :editype;
    if(:editype<>:miaraedi) then jedno = 0;
  end
  -- szukaj jednostki wg edi w towjedn, na pewno nie chodzi o domyslna
  if(:jedno=0 and :miaraedi<>'') then begin
    select first 1 ref from towjedn
    left join miara on miara.miara=towjedn.jedn
    where towjedn.ktm=:ktm and miara.editype=:miaraedi
    into :jedno;
  end
  if(:jedno=0) then begin
    msg = 'Nie znaleziono jednostki '||:miaraedi||' dla towaru '||:ktm;
    exception UNIVERSAL :msg;
  end
  suspend;
end^
SET TERM ; ^
