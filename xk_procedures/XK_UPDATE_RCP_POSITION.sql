--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_UPDATE_RCP_POSITION
  returns (
      RCOUNT integer)
   as
declare variable REFEV integer;
declare variable TIMEEVENT timestamp;
declare variable ZONE varchar(20);
declare variable INOUT smallint;
declare variable EPRESENCELISTNAGREF integer;
declare variable OPERREF integer;
declare variable OSOBA integer;
declare variable PRACOWNIK integer;
declare variable OPERSTR varchar(20);
declare variable CALENDAR integer;
declare variable DATALISTY timestamp;
declare variable LISTREF integer;
declare variable WORKSTART time;
declare variable WORKEND time;
declare variable TIMESTART varchar(20);
declare variable TIMESTOP varchar(20);
declare variable REFECONTRACTSDEF integer;
declare variable REFECONTRACTPOSDEFIN integer;
declare variable REFECONTRACTPOSDEFOUT integer;
declare variable REF_EPLISTSPOS integer;
declare variable EPRESENCELISTNAGREF2 integer;
declare variable company integer;
begin
  RCOUNT = 0;
  execute procedure get_config('OPERREJRCPIN',2) returning_values REFECONTRACTPOSDEFIN;
  execute procedure get_config('OPERREJRCPOUT',2) returning_values REFECONTRACTPOSDEFOUT;

  for
    select r.ref, r.timeevent, g.zone, g.inout, p.ref
      from regisevents r
        join gatereg g on (r.idgate = g.symbol)
        join persons p on (r.idident = p.cardident)
      where r.status = 0
      order by r.timeevent, r.ref
     into :refev, :timeevent, :zone, :inout, :osoba
  do begin
    --zaokraglanie czasu pracy
    --execute procedure round_time(timeevent, 5) returning_values timeevent;
    datalisty = cast(timeevent as date);
    --ustalenia pracownika dla osoby
    --if (not exists(select ref from regisevents r
    --where r.idgate = :idgate and r.idident = :idident and abs(r.timeevent- :timeevent) < 0.002083333 /*3 minuty*/ and r.status = 1)) then
    for
      select distinct e.ref
        from employees e
          join emplcontracts m on m.employee = e.ref
        where m.fromdate <= :datalisty
          and (m.enddate >= :datalisty or m.enddate is null)
          and e.person = :osoba
        into :pracownik
    do begin
      epresencelistnagref = null;
      /*select max(ref) from operator o
        where o.employee = :pracownik
        into operref;*/
      /*if (operref is null) then
      begin
        execute procedure GET_GLOBAL_PARAM('AKTUOPERATOR') returning_values :operstr;
        if (operstr is not null and operstr<>'') then
          operref = cast(:operstr as integer);
      end*/
      -- dodanie listy w przypadku jej braku
      listref = null;
      select ref from EPRESENCELISTS
        where LISTSDATE >= :datalisty
          and LISTSDATE < dateadd(1 day to :datalisty)
        into listref;

      if (listref is null) then
      begin
        INSERT INTO EPRESENCELISTS (LISTSDATE) VALUES (:datalisty)
        returning ref into listref;
      end
      -- dodanie pracownika do listy w przypadku jego braku
      select n.ref,n.timestart, n.timestop
        from epresencelistnag n
          join epresencelists l on (n.epresencelist = l.ref)
        where n.employee = :pracownik and l.listsdate = :datalisty
        into :epresencelistnagref, :timestart, :timestop;
      calendar = null;
      select ec.ecalendar from emplcaldays ec
        where ec.employee = :pracownik and ec.cdate = :datalisty
        into :calendar;

  --    if (calendar is not null) then
      begin
      --exception brak_kalendarza 'Brak wskazanego kalendarza, lub nie wskazano żadnego dla pracownika '||:personnames;
        if (epresencelistnagref is null) then
        begin
          -- okreslenie umowy o prace
          execute procedure EWORKTIME_CALCULATE(:pracownik , calendar, 0, :listref,:datalisty)
            returning_values (workstart, workend);
          timestart = substring(cast(workstart as varchar(20)) from 1 for 5);
          timestop = substring(cast(workend as varchar(20)) from 1 for 5);
          if (timestart = '') then timestart = null;
          if (timestop = '') then timestop = null;
          -- wstawienie pracownika na liste
          if (timestart is null) then timestart = '8:00';
          if (timestop  is null) then timestop = '16:00';
          if (timestart is not null and timestop is not null) then
          begin
            execute procedure GEN_REF('epresencelistnag') returning_values :epresencelistnagref;
            insert into epresencelistnag (ref, employee, timestart, timestop, epresencelist, wastoday, shift, operator)
            values(:epresencelistnagref ,:pracownik , :timestart, :timestop, :listref, 1, 0, :operref );
          end
        end

        -- wejscie do strefy
        if (inout = 0) then
        begin
          if (REFECONTRACTPOSDEFIN is null) then
            exception test_break 'Brak zdefiniowanej operacji dla rej. czasu';

          select econtractsdef from ECONTRACTSPOSDEF where ref = :REFECONTRACTPOSDEFIN into refecontractsdef;
          -- sprawdzenie czy jest juz operacja RCP w tym samym czasie
          /*if (timeevent < cast(datalisty as date)||' '||timestart) then
          timeevent = cast(datalisty as date)||' '||timestart; */

          if (not exists(select first 1 ref from epresencelistspos p
                where p.epresencelists = :listref and employee = :pracownik and timestart <= :timeevent and timestop is null))
                then
          begin
            INSERT INTO EPRESENCELISTSPOS (EPRESENCELISTS, ECONTRACTSDEF, TIMESTART, TIMESTOP,timestartstr ,timestopstr,
                 ACCORD, EPRESENCELISTNAG, ECONTRACTPOSDEF, DESCRIPT, EMPLOYEE,operator)
              VALUES (:listref, :refecontractsdef, :timeevent , null, substring(cast(:timeevent as time) from 1 for 5),null,
                 0, :epresencelistnagref, :REFECONTRACTPOSDEFIN, 'Wejście do strefy '||:zone, :pracownik, :operref );
          end
          update regisevents set status = 1  where ref = :refev;
          rcount = rcount + 1;
        end

        -- wyjscie ze strefy
        else begin
          if (REFECONTRACTPOSDEFOUT is null) then
            exception test_break 'Brak zdefiniowanej operacji dla rej. czasu';
          select econtractsdef from ECONTRACTSPOSDEF where ref = :REFECONTRACTPOSDEFOUT
            into refecontractsdef;
          --sprawdzenie czy można aktualne wyjscie powiazac z wejsciem
          select n.ref
            from epresencelistnag n
              left join epresencelists l on (n.epresencelist = l.ref)
            where n.employee = :pracownik and l.listsdate = :datalisty-1
            into epresencelistnagref2;
    
          select max(ref)
            from epresencelistspos
            where timestop is null and econtractsdef = :refecontractsdef
              and (EPRESENCELISTNAG = :epresencelistnagref or EPRESENCELISTNAG = :epresencelistnagref2)
              and ((cast(timestart as date) = cast(:timeevent as date) and timestart <= :timeevent)
                    or (cast(timestart+1 as date) = cast(:timeevent as date)) )
            into :ref_eplistspos;

          if (ref_eplistspos is not null) then
            update epresencelistspos set timestop = :timeevent, timestopstr = substring(cast(:timeevent as time) from 1 for 5) where ref = :ref_eplistspos;
          else
            INSERT INTO EPRESENCELISTSPOS (EPRESENCELISTS, ECONTRACTSDEF, TIMESTART, TIMESTOP,timestartstr ,timestopstr , ACCORD, EPRESENCELISTNAG, ECONTRACTPOSDEF, DESCRIPT, EMPLOYEE,operator )
              VALUES (:listref, :refecontractsdef, null, :timeevent, null, substring(cast(:timeevent as time) from 1 for 5), 0, :epresencelistnagref, :REFECONTRACTPOSDEFOUT,'Wyjście ze strefy '||:zone,:pracownik,:operref );

          update regisevents set status = 1  where ref = :refev;
          rcount = rcount + 1;
        end
      end
    end
  end
  suspend;
end^
SET TERM ; ^
