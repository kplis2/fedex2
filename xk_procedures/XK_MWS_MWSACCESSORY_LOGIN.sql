--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSACCESSORY_LOGIN(
      AKTUOPERATOR integer,
      MWSACCESSORY varchar(40) CHARACTER SET UTF8                           ,
      HHSYMBOL varchar(20) CHARACTER SET UTF8                           ,
      DISTANCE numeric(14,4),
      DESCRIPT varchar(250) CHARACTER SET UTF8                           ,
      MODE smallint)
  returns (
      STATUS smallint,
      HISTORYREF integer)
   as
declare variable accessoryref integer;
declare variable operator integer;
declare variable regdist smallint;
declare variable newhistoryref integer;
declare variable actdistance numeric(14,4);
declare variable mref integer;
begin
   update mwsaccessoryhist m set m.timestartto = current_timestamp(0), m.distanceto = m.distancefrom
    where m.operator = :aktuoperator
      and m.timestartto is null;
  select first 1 m.ref, t.regdistance, m.distance
    from mwsaccessories m
      left join mwsaccessorytypes t on(t.ref = m.mwsaccessorytype)
    where m.symbol = :mwsaccessory
    into accessoryref, regdist, actdistance;
  if (accessoryref is null and mwsaccessory <> '') then
  begin    -- brak wózka o takim symbolu
    status = 1;
    exit;
  end
  else if (regdist = 1 and distance = 0) then
  begin
    status = 2;
    exit;
  end
  else if (regdist = 1 and distance < actdistance) then
  begin
    status = 4;
    exit;
  end 
  else
  begin
    select first 1 m.ref, m.operator
      from mwsaccessoryhist m
      where m.mwsaccessory = :accessoryref
        and m.timestartto is null
      into historyref, operator;
    if (operator <> aktuoperator and operator is not null and mode = 0) then
    begin
      status = 3;  -- wózek posiada inny operator
      exit;
    end
  end
  if (descript <> '') then
    descript = 'LOGOWANIE: '||descript||'; ';
  --update mwsaccessories m set m.aktuoperator = null where m.aktuoperator = :aktuoperator;
  if (historyref is not null) then
  begin
    update mwsaccessoryhist m set m.timestartto = current_timestamp(0), m.distanceto = :distance where m.ref = :historyref;
  end
  execute procedure gen_ref('MWSACCESSORYHIST') returning_values newhistoryref;
  insert into mwsaccessoryhist(ref ,operator,timestartfrom, hhsymbol, distancefrom, descript)
    values (:newhistoryref ,:aktuoperator, current_timestamp(0), :hhsymbol, :distance, :descript);
  if (accessoryref is not null) then
  begin
    update mwsaccessoryhist m set m.mwsaccessory = :accessoryref where m.ref = :newhistoryref;
  --  update mwsaccessories m set m.aktuoperator = :aktuoperator where m.ref = :accessoryref;
  end
  historyref = newhistoryref;
  suspend;
end^
SET TERM ; ^
