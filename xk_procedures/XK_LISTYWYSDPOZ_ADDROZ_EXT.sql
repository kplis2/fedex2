--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSDPOZ_ADDROZ_EXT(
      KODKRESK varchar(40) CHARACTER SET UTF8                           ,
      ILOSC numeric(14,2),
      MNOZNIK numeric(14,2),
      LISTWYSD integer,
      AKTOPK varchar(40) CHARACTER SET UTF8                           ,
      AKTOPK_REF integer,
      WERSJAREF integer,
      POZYCJA integer,
      STATUS smallint,
      TRYB smallint,
      OPERATOR integer,
      SZYBKO SMALLINT_ID)
  returns (
      RLISTWYSD integer,
      RAKTOPK varchar(40) CHARACTER SET UTF8                           ,
      RAKTOPK_REF integer,
      RWERSJAREF integer,
      RPOZYCJA integer,
      RSTATUS smallint,
      MSG varchar(255) CHARACTER SET UTF8                           ,
      INFO_MSG varchar(255) CHARACTER SET UTF8                           ,
      DRUKUJ SMALLINT_ID)
   as
declare variable IS_BOX smallint;
declare variable IS_PALLETE_ACTIVE smallint;
declare variable IS_PALLETE_BOX smallint;
declare variable BOX_OPENED smallint;
declare variable BOX_REF integer;
declare variable MOSYMBS varchar(255);
declare variable x_box_ktm ktm_id;
declare variable ISKTMFOROPK smallint_id;
declare variable szerokosc numeric_14_4;
declare variable dlugosc numeric_14_4;
declare variable wysokosc numeric_14_4;
declare variable k smallint_id;
begin
-- -----------------------------------------------------------------------------
--if (operator = 74) then
-- begin
--   in autonomous transaction do
     --  insert into expimp (s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12)
      --     values('XK_LISTYWYSDPOZ_ADDROZ_EXT',:kodkresk, :ilosc, :mnoznik, :listwysd, :aktopk, :aktopk_ref, :wersjaref, :pozycja, :status, :tryb, :operator);
-- end


  if (:aktopk is not null) then raktopk = :aktopk;
  else raktopk = '';
  if (:listwysd is not null) then rlistwysd = :listwysd;
  else rlistwysd = 0;
  if (:pozycja is not null) then rpozycja = :pozycja;
  else rpozycja = 0;
  if (:aktopk_ref is not null) then raktopk_ref = :aktopk_ref;
  else raktopk_ref = 0;

-- -----------------------------------------------------------------------------
  select first 1 mo.symbol
    from listywysdpoz lwp
      left join dokumpoz dp1 on (lwp.dokpoz = dp1.ref)
      left join dokumpoz dp2 on (dp1.ref = dp2.kortopoz)
      left join mwsacts ma on (dp2.ref = ma.docposid and ma.doctype = 'M')
      left join mwsords mo on (ma.mwsord = mo.ref)
    where lwp.dokument = :listwysd
      and ma.status < 5
  into :mosymbs;
  if (coalesce(:mosymbs,'') <> '') then begin
    msg = 'Towar do rozwiezienia na zleceniu ZT '||:mosymbs;
    rstatus = 0;
    rwersjaref = 0;
    suspend;
    exit;
  end

  -- informacje o pudelku nowym -- tylko dla pakowania powoli
  select is_box, opened, box_ref, is_pallete, x_box_ktm from xk_listywysd_opak(:kodkresk, :listwysd)
  into :is_box, :box_opened, :box_ref, :is_pallete_box, :x_box_ktm;

  -- informacje o pudelku aktywnym - czy to paleta  -- tylko dla pakowania powoli
  select is_pallete from xk_listywysd_opak(:aktopk, :listwysd)
  into :is_pallete_active;

  msg = '';
  info_msg = '';
  drukuj = 0;

-- -----------------------------------------------------------------------------
-- kontrola istnienia opakowań w kartotece towary

  if ((is_box = 1 and box_opened = 0) or (szybko = 1) ) then
    begin
      select first 1 1 --jesli nowe opakowanie, to sprawdzamy czy istnieje takie na towary Ldz
        from towary t
        where t.ktm = :x_box_ktm
      into :ISKTMFOROPK;

      if (ISKTMFOROPK is null and box_opened = 0) then
        exception universal 'Opakowanie nie istnieje w bazie opakowań: '||coalesce(x_box_ktm,'Brak kodu');
      else
        select first 1 t.szer, t.dlug, t.wys
          from towjedn t
          where t.ktm = :x_box_ktm
        into :szerokosc, :dlugosc, :wysokosc;
    end
  else  if (is_pallete_box = 1 and box_opened = 0) then
    begin
      select first 1 1 --jesli nowa paleta
        from typyopk t
        where t.symbol = :x_box_ktm
      into :ISKTMFOROPK;

      if (ISKTMFOROPK is null and box_opened = 0) then
        exception universal 'Paleta nie istnieje w bazie palet: '||coalesce(x_box_ktm,'Brak kodu');
      else
        select first 1 t.szerokosc, t.dlugosc, t.wysokosc
          from typyopk t
          where t.symbol = :x_box_ktm
        into :szerokosc, :dlugosc, :wysokosc;
    end

---- pakowanie opakowan do szybkiego pakowania Ldz
  if (coalesce(szybko, 0)=1) then begin
    k = 1;
    if (:tryb = 4) then begin
      while (k <= :ilosc*coalesce(mnoznik,1)) do begin
        insert into LISTYWYSDROZ_OPK (LISTWYSD, NROPK, STAN, TYP, STANOWISKO, X_PAKOWAL, X_DATAPACK, X_BOX_KTM, X_szybko)
          values (:listwysd, coalesce((select count(*) from listywysdroz_opk l where l.listwysd = :listwysd), 0) + 1, 0, 0, :x_box_ktm, :operator, current_timestamp(0), :X_BOX_KTM, 1);
        k = k + 1;
      end
      info_msg = 'Zarejestrowano nowe opakowanie '||:x_box_ktm;
    end else if (:tryb = 2) then begin
---- usuwanie szybkich opakowan Ldz
      while (k <= :ilosc*coalesce(mnoznik,1)) do begin
        select first 1 l.ref
          from listywysdroz_opk l
          where l.listwysd = :listwysd
            and l.x_box_ktm = :x_box_ktm
            and l.x_szybko = 1
        into :aktopk_ref;
        delete from listywysdroz_opk l
          where l.ref = :aktopk_ref;
        k = k + 1;
      end
      info_msg = 'Usunieto opakowanie '||:x_box_ktm;
    end
    rstatus = 1;
    suspend;
    exit;
  end

  /*if (tryb = 0) then -- -------------------------------------------------------- otwieranie opakowan (INACTIVE)
  begin

    --wybieranie kartonu
    if (is_box = 1 or is_pallete_box = 1) then
    begin
        --zakladanie nowego kartonu

        if (box_ref is null) then
        begin
          insert into listywysdroz_opk(listwysd, stanowisko, stan) values(:listwysd, :kodkresk, 0);
          select first 1 ref from listywysdroz_opk order by ref desc
          into :box_ref;
          info_msg = 'Dodano i otwarto nowe opakowanie.';
        end
        else if (box_opened = 0) then
        begin
           UPDATE LISTYWYSDROZ_OPK SET STAN = 0 WHERE (REF = :box_ref);
           info_msg = 'Otwarto opakowanie.';
        end
    
        rstatus = 2;
        raktopk = kodkresk;
        raktopk_ref = box_ref;
        rwersjaref = 0;
        suspend;
        exit;
    end
    else
    begin
        msg = 'Nie odnaleziono wskazanego opakowania.'; -- to nie opakowanie
        rwersjaref = 0;
        rstatus = 0;
        rpozycja = 0;
        suspend;
        exit;
    end

  end
 else
  BEGIN */

      if (tryb != 4 and raktopk_ref = 0) then
      begin
          msg = 'Najpierw otwórz opakowanie, na którym pracujesz.';
          rstatus = 0;
          rwersjaref = 0;
          suspend;
          exit;
      end

      if (tryb = 1) then  -- --------------------------------------------------- dodawanie - KARTON w KARTON, KARTON na PALETĘ
      begin
    
        if (is_box = 1 or is_pallete_box = 1) then -- DODAWANIE OPAKOWAŃ DO OPAKOWAŃ
        begin
            
            if (is_pallete_active is not null and is_pallete_box = 1 and is_pallete_active = 1 ) then
            begin
                  msg = 'Nie możesz zapakować palety w paletę.';
            end
            else if (is_pallete_active is not null and is_pallete_box = 1 and is_pallete_active = 0) then
            begin
                  msg = 'Nie możesz zapakować palety w karton.';
            end
            else
            begin
                select err_msg, info_msg from XK_LISTYWYSDPOZ_ADDING_BOX(:kodkresk, :aktopk, :listwysd, :is_pallete_box, :operator, :x_box_ktm, :szerokosc, :dlugosc, :wysokosc)
                into :msg, :info_msg;
            end

            if (msg != '') then rstatus = 0;
            else rstatus = 1;
    
            rwersjaref = 0;
            suspend;
            exit;
        end
        else -- DODAWANIE TOWARU DO OPAKOWAŃ
        begin
            select err_msg, info_msg,  rwersjaref,  rpozycja,  rstatus
              from XK_LISTYWYSDPOZ_ADDING_TOWAR(:kodkresk, :ilosc, :mnoznik, :aktopk, :listwysd, :wersjaref, :pozycja, :status, :operator)
            into  :msg, :info_msg, :rwersjaref, :rpozycja, :rstatus;
    
            suspend;
            exit;
        end
    
      end
      else if (tryb = 2) then  -- ---------------------------------------------- usuwanie
      begin
          if (is_box = 0) then
          begin
             -- usuwanie towarów --
            select err_msg, info_msg from XK_LISTYWYSDPOZ_REMOVING_TOWAR(:kodkresk, :ilosc, :mnoznik, :aktopk, :listwysd, :pozycja, :wersjaref)
             into :msg, :info_msg;
    
             if (msg != '') then rstatus = 0;
             else rstatus = 1;

             rwersjaref = 0;
             suspend;
             exit;
          end
          else
          begin
             -- usuwanie opakowań --
             select err_msg, info_msg from XK_LISTYWYSDPOZ_REMOVING_BOX(:kodkresk, :aktopk, :listwysd)
             into :msg, :info_msg;
    
             if (msg != '') then rstatus = 0;
             else rstatus = 1;
    
             rwersjaref = 0;
             suspend;
             exit;
          end
      end
      else if (tryb = 4) then  -- ---------------------------------------------- Pakowanie - ADD good, OPEN box/pallete
      begin
          if (is_box = 1 or is_pallete_box = 1) then
          begin
             if (box_opened = 1) then  -- wskazano otwarte opakowanie/palet
                begin
                    if (kodkresk != raktopk) then  -- wskazano inne opakowanie niz aktywne
                    begin
                        -- Ldz XXX
                        -- Zapisanie informacji o tym, ze nalezy odstawic gdzies opakowanie poprzednie i aktywne
                        -- uczyn to opakowanie aktywnym
                        update listywysdroz_opk l
                        set l.x_moved = 1
                        where l.stanowisko = :kodkresk;

                        update listywysdroz_opk l
                        set l.x_moved = 1
                        where l.stanowisko = :raktopk;
                         -- Ldz
                        info_msg = 'Zmieniono aktywne opakowanie.';
                        rstatus = 2;
                        raktopk = kodkresk;
                        raktopk_ref = box_ref;
                    end
                    else msg = 'To opakowanie jest już aktywne.';
                end
             else
                begin
                /*if (box_ref is null) then -- wsk
                begin  */
                    -- otwórz to opakowanie i uczyń aktywnym


                    insert into listywysdroz_opk(listwysd, stanowisko, stan, typ, x_pakowal, x_datapack, x_box_ktm, x_moved, x_szerokosc, x_dlugosc, x_wysokosc)
                      values(:listwysd, :kodkresk, 0, :is_pallete_box, :operator, current_timestamp, :x_box_ktm, 1, :szerokosc, :dlugosc, :wysokosc);
                    select first 1 ref from listywysdroz_opk order by ref desc
                    into :box_ref;
                    info_msg = 'Otwarto nowe opakowanie.';
                    rstatus = 2;
                    raktopk = kodkresk;
                    raktopk_ref = box_ref;
                    drukuj = 1;
                end

          end
          else
          begin -- obsluga wskazania na towar w trybie Pakowania

            if (raktopk_ref = 0) then -- nie ma otwartego aktywnego
                begin
                    -- uczyn to opakowanie aktywnym i otwartym
                    msg = 'Najpierw otwórz opakowanie, na którym pracujesz.';
                    --UPDATE LISTYWYSDROZ_OPK SET STAN = 0 WHERE (REF = :box_ref);
                    --info_msg = 'Otwarto opakowanie.';
                end
            else
                begin
                    select err_msg, info_msg,  rwersjaref,  rpozycja,  rstatus
                      from XK_LISTYWYSDPOZ_ADDING_TOWAR(:kodkresk, :ilosc, :mnoznik, :aktopk, :listwysd, :wersjaref, :pozycja, :status, :operator)
                    into  :msg, :info_msg, :rwersjaref, :rpozycja, :rstatus;
    
                    suspend;
                    exit;
                end
          end
    
          -- Wykonywane zawsze w tym trybie poza dodaniem towaru
          if (msg != '') then rstatus = 0;
          else rstatus = 1;
    
          rwersjaref = 0;
          suspend;
          exit;
      end
      /*else if (tryb = 5) then  -- ---------------------------------------------- Dodawanie tylko i wylacznie towarów
      begin
        if (is_box = 1) then -- wskazano opakowanie
        begin
            msg = 'Wprowadzoną ilośc można zastosować tylko do towarów.';
            info_msg = 'Ustawiono ilość spowrotem na 1.';
            rstatus = 0;
            rwersjaref = 0;
            suspend;
            exit;
        end
        else -- DODAWANIE TOWARU DO OPAKOWAŃ
        begin
            select err_msg, info_msg,  rwersjaref,  rpozycja,  rstatus from XK_LISTYWYSDPOZ_ADDING_TOWAR(:kodkresk, :ilosc, :mnoznik, :aktopk, :listwysd, :wersjaref, :pozycja, :status)
            into  :msg,    :info_msg, :rwersjaref, :rpozycja, :rstatus;
            suspend;
            exit;
        end
      end */
      else -- ------------------------------------------------------------------ nie wiadomo co za tryb
      begin
          msg = 'Przejście w nieznany tryb pracy.';
          info_msg = 'Skontaktuj się z firmą SENTE, gdyż wystąpił istotny wyjątek.';
          rstatus = 0;
          rwersjaref = 0;
          suspend;
          exit;
      end
  /*END*/

end^
SET TERM ; ^
