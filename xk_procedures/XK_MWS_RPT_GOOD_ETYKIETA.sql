--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_RPT_GOOD_ETYKIETA(
      VERS integer,
      BARCODE integer,
      CNT integer)
  returns (
      RKTM varchar(40) CHARACTER SET UTF8                           ,
      RBARCODE varchar(30) CHARACTER SET UTF8                           ,
      RNAME1 varchar(60) CHARACTER SET UTF8                           ,
      RNAME2 varchar(60) CHARACTER SET UTF8                           ,
      RQUANTITY integer,
      RTEXTVAL1 varchar(60) CHARACTER SET UTF8                           ,
      RTEXTVAL2 varchar(60) CHARACTER SET UTF8                           ,
      RTEXTVAL3 varchar(60) CHARACTER SET UTF8                           ,
      RNUMVAL1 numeric(15,4),
      RNUMVAL2 numeric(15,4),
      RNUMVAL3 numeric(15,4),
      RINTVAL1 integer,
      RINTVAL2 integer,
      RINTVAL3 integer)
   as
begin
  if (:vers is not null) then begin
    for
      select t.ktm, substring(t.nazwa from 1 for 60), substring(t.opisroz from 1 for 60), tj.przelicz, t.kodprod
        from wersje w
          join towary t on (w.ktm = t.ktm)
          join towjedn tj on (t.ktm = tj.ktm and tj.glowna = 1),
          syskopie(:cnt)
        where w.ref = :vers
      into :rktm, :rname1, :rname2, :rquantity, :rtextval1
    do begin
      select kodkresk from towkodkresk where ref = :barcode
      into :rbarcode;
      suspend;
    end
  end
end^
SET TERM ; ^
