--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSD_GETOPK(
      KODKRESK varchar(40) CHARACTER SET UTF8                           ,
      LISTWYSD integer,
      TRYB smallint,
      TYPOPK integer = 0)
  returns (
      OPK integer,
      OPKSTR varchar(40) CHARACTER SET UTF8                           ,
      STAN smallint,
      TYP integer)
   as
begin
  opk = 0;
  stan = 0;
  typ = 0;
  kodkresk = upper(:kodkresk);

  select o.ref, t.nazwa||': '||o.kodkresk, o.stan, o.typopk
    from listywysdroz_opk o
      left join typyopk t on (o.typopk = t.ref)
    where o.listwysd = :listwysd
      and upper(o.kodkresk) = :kodkresk
  into :opk, :opkstr, :stan, :typ;

  if (:tryb = 1 and :opk = 0) then
  begin
    insert into listywysdroz_opk(listwysd, kodkresk, stan, typopk)
      values(:listwysd, :kodkresk, 0, :typopk)
    returning ref into :opk;

    select nazwa from typyopk where ref = :typopk
    into :opkstr;

    typ = :typopk;
    opkstr = :opkstr||': '||:kodkresk;
  end
  else if (:tryb = 2 and :opk > 0) then
  begin
    update listywysdroz_opk set stan = 0
      where ref = :opk;
    stan = 0;
  end
end^
SET TERM ; ^
