--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_POW_OPTIMALIZE_BR(
      WH varchar(3) CHARACTER SET UTF8                           ,
      VERSIN integer,
      BDATE date,
      RECALC smallint)
  returns (
      VERS integer,
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      POSCNT integer,
      ACTMWSCONSTLOC integer,
      ACTMWSCONSTLOCSYMB varchar(30) CHARACTER SET UTF8                           ,
      ACTDIST numeric(14,4),
      ACTDEEP smallint,
      MWSCONSTLOCSYMB integer,
      MWSCONSTLOCSYMBS varchar(30) CHARACTER SET UTF8                           ,
      SHORTAGE smallint,
      DIST numeric(14,4),
      QUANTITY numeric(14,4),
      CONST smallint,
      ORDERED smallint,
      PALCNT integer)
   as
declare variable stanarch integer;
declare variable typzam varchar(40);
begin
  execute procedure getconfig('NEWZAMTYPZAK') returning_values(:typzam);
  for
    select ref, ktm
      from wersje
      where akt = 1 and usluga not in (1,2) and (ref = :versin or :versin is null)
        and coalesce(mwscalcmix,0) = 0
      into vers, good
  do begin
    quantity = 0;
    quantity = null;
    select count(p.ref), sum(case when d.mwsdisposition = 1 then p.ilosc else p.iloscl end)
      from dokumpoz p
        left join dokumnag d on (d.ref = p.dokument)
        left join defdokum f on (f.symbol = d.typ)
      where f.wydania = 1 and f.zewn = 1 and f.koryg = 0 and p.data >= :bdate
        and d.magazyn = :wh and p.wersjaref = :vers
        and p.genmwsordsafter = 1
      into poscnt, quantity;
    if (quantity is null) then quantity = 0;
    if (recalc <> 3) then
    begin
      select max(ref) from stanyarch where magazyn = :wh and wersjaref = :vers
        into stanarch;
      if (stanarch is null) then
        shortage = 1;
      else if (exists (select ref from stanyarch where ref = :stanarch and stan = 0)) then
        shortage = 1;
      if (shortage = 0 and stanarch is not null ) then
      begin
        if (exists (select ref from stanyarch where ref = :stanarch and stan = 0 and data >= :bdate)) then
          shortage = 1;
      end
      if (exists (select p.ref from pozzam p join nagzam n on (n.ref = p.zamowienie)
          where p.iloscm - p.ilrealm > 0 and n.typ < 3 and n.termdost < current_date + 30
            and n.typzam = :typzam and p.wersjaref = :vers and p.magazyn = :wh)
      ) then
        ordered = 1;
    end
    select count(distinct s.mwsconstloc)
      from mwsstock s
      where s.vers = :vers and s.wh = :wh
      into palcnt;
    if (palcnt is null) then palcnt = 0;
    if (poscnt is null) then poscnt = 0;
    if (quantity is null) then quantity = 0;
    actmwsconstloc = null;
    actmwsconstlocsymb = null;
    mwsconstlocsymb = null;
    mwsconstlocsymbs = null;
    const = null;
    shortage = 0;
    ordered = 0;
    actdist = null;
    dist = null;
    select first 1 c.ref, c.symbol, c.distfromstartarea, c.deep
      from mwsstock m
        join mwsconstlocs c on (c.ref = m.mwsconstloc)
        left join whsecs w on (w.ref = c.whsec)
      where m.mwsstandlevelnumber = 1 and m.vers = :vers and w.deliveryarea = 2
        and m.wh = :wh  and m.quantity - m.blocked > 0 and c.goodsav in (1,2)
      order by c.distfromstartarea
      into actmwsconstloc, actmwsconstlocsymb, actdist, actdeep;
    select first 1 s.mwsconstloc, s.mwsconstlocsymbol, c.distfromstartarea, s.const
      from mwsconstlocsymbs s
        left join mwsconstlocs c on (c.ref = s.mwsconstloc)
      where c.goodsav in (1,2) and c.wh = :wh
       and ((s.symbol = :good and s.vers is null) or s.vers = :vers)
      order by s.symbol
      into mwsconstlocsymb, mwsconstlocsymbs, dist, const;
    if (const is null) then const = 0;
    if ((palcnt > 0 or ordered = 1) and not exists(select first 1 1 from xk_goods_relocate x where x.vers = :vers and coalesce(x.const,0) = 1)) then
      suspend;
  end
end^
SET TERM ; ^
