--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_DISTANCE(
      MWSACCESSORY varchar(40) CHARACTER SET UTF8                           )
  returns (
      DISTANCE integer)
   as
begin
  select first 1 m.distance
    from mwsaccessories m
    where m.symbol = :mwsaccessory
    into :distance;
  if (distance is  null) then
    exception mws_accessory_not_found;
  suspend;
end^
SET TERM ; ^
