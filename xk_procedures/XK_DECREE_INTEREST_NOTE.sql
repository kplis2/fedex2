--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_DECREE_INTEREST_NOTE(
      NOTYNAG integer,
      DOCTYPE integer,
      OPERID integer,
      AKCEPT smallint,
      DATA timestamp)
  returns (
      BKDOC integer)
   as
declare variable bkreg varchar(10);
  declare variable amount numeric(14,2);
  declare variable dictdef integer;
  declare variable dictpos integer;
  declare variable account varchar(20);
  declare variable settlement varchar(20);
  declare variable docdate timestamp;
  declare variable payday timestamp;
  declare variable descript varchar(80);
  declare variable booking varchar(255);
  declare variable currency varchar(3);
  declare variable configcurrency varchar(3);
  declare variable company integer;
begin
  select bkreg, company from BKDOCTYPES where ref = :doctype into :bkreg, :company;
  execute procedure GEN_REF('BKDOCS') returning_values bkdoc;

  insert into BKDOCS(ref, company, bkreg, doctype, transdate, symbol, descript, docdate,
                     regoper, dictdef, dictpos, otable, oref)
              select :bkdoc, :company, :bkreg, :doctype, :data, symbol, symbol, :data,
                     :operid, slodef, slopoz, 'NOTYNAG', ref
              from notynag where ref = :notynag;

  select interests, slodef, slopoz, symbol, data, notynag.currency from notynag where ref=:notynag
    into :amount, :dictdef, :dictpos, :settlement, :docdate, :currency;

  payday = docdate + 14;

  execute procedure KODKS_FROM_DICTPOS(dictdef, dictpos)
    returning_values account;
  execute procedure get_config('COUNTRY_CURRENCY', 2)
    returning_values :configcurrency;

  if(:currency <> :configcurrency) then begin
    account = '203-' || account || '?KURS?';
    execute procedure insert_decree_currsettlement (bkdoc, account, 0, amount,0,0,
    :currency, descript, settlement, null, null, 1, docdate, payday, null, 'NOTYNAG', :notynag, null, null, null, 0);
    execute procedure insert_decree(bkdoc, '750-05', 1, amount, descript, 0);
  end else begin
  account = '200-' || account;

  execute procedure insert_decree_settlement(bkdoc, account, 0, amount, descript,
    settlement, null, null, 1, docdate, payday, null, null, null, null, 0);

  execute procedure insert_decree(bkdoc, '750-05', 1, amount, descript, 0);

  if (akcept = 1) then
  begin
    update bkdocs set status = 1, accoper = :operid where ref = :bkdoc;
    execute procedure get_config('BOOK_WITH_ACCEPT', 2)
      returning_values :booking;
    if (booking = '1') then
      update bkdocs set status = 2, bookoper = :operid where ref = :bkdoc;
  end
  end
  if (bkdoc > 0) then
    update notynag set status = 2, bkdoc = :bkdoc
      where ref = :notynag;
end^
SET TERM ; ^
