--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_SUMFAK3(
      NAGFAKREF integer)
  returns (
      WARTNET numeric(14,2),
      WARTBRU numeric(14,2),
      GR_VAT varchar(20) CHARACTER SET UTF8                           ,
      WARTVAT numeric(14,2))
   as
declare variable zaliczkowy integer;
declare variable korekta integer;
declare variable zakup integer;
declare variable walutowa integer;
begin
  select nagfak.zaliczkowy, typfak.korekta, nagfak.zakup, nagfak.walutowa
  from nagfak
  left join typfak on (typfak.symbol=nagfak.typ)
  where nagfak.ref=:nagfakref
  into :zaliczkowy,:korekta, :zakup, :walutowa;
  if(:zakup=1) then walutowa = 0;
  if(:zaliczkowy=0 and :korekta=0) then begin
    /* zwykle pozycje faktury - podsumowanie rozliczenia zaliczek*/
    if(exists(select refk from NAGFAK_GET_ZALICZKI(:nagfakref))) then begin
      for select
        (case when :walutowa=1 then ROZFAK.esumwartnet-ROZFAK.pesumwartnet else ROZFAK.esumwartnetzl-ROZFAK.pesumwartnetzl end),
        (case when :walutowa=1 then ROZFAK.esumwartbru-ROZFAK.pesumwartbru else ROZFAK.esumwartbruzl-ROZFAK.pesumwartbruzl end),
        ROZFAK.VAT,
        (ROZFAK.esumwartvatzl-ROZFAK.pesumwartvatzl)
      from ROZFAK where ROZFAK.DOKUMENT=:nagfakref
      order by ROZFAK.VAT
      into :wartnet,:wartbru,:gr_vat,:wartvat
      do begin
        suspend;
      end
    end
  end
  else if(:zaliczkowy=0) then begin
    /* po korekcie */
    for select
      (case when :walutowa=1 then ROZFAK.sumwartnet else ROZFAK.sumwartnetzl end),
      (case when :walutowa=1 then ROZFAK.sumwartbru else ROZFAK.sumwartbruzl end),
      ROZFAK.VAT,
      ROZFAK.sumwartvatzl
    from ROZFAK where ROZFAK.DOKUMENT=:nagfakref
    order by ROZFAK.VAT
    into :wartnet,:wartbru,:gr_vat,:wartvat
    do begin
      suspend;
    end
  end
  else if(:zaliczkowy=2) then begin
    /* koretka zaliczkowa - po korekcie */
    for select
      (case when :walutowa=1 then sum(nagfakzal.pwartnet-nagfakzal.wartnet) else sum(nagfakzal.pwartnetzl-nagfakzal.wartnetzl) end),
      (case when :walutowa=1 then sum(nagfakzal.pwartbru-nagfakzal.wartbru) else sum(nagfakzal.pwartbruzl-nagfakzal.wartbruzl) end),
      nagfakzal.vat,
      (sum(nagfakzal.pwartbruzl-nagfakzal.pwartnetzl)-sum(nagfakzal.wartbruzl-nagfakzal.wartnetzl))
      from nagfakzal
      where nagfakzal.faktura=:nagfakref
      group by nagfakzal.vat
      into :wartnet,:wartbru,:gr_vat,:wartvat
    do begin
      suspend;
    end
  end
end^
SET TERM ; ^
