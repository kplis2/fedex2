--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_BEST_SOURCE_3(
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      REFMWSACT integer,
      REFMWSORD integer,
      MWSORDTYPE integer,
      WH varchar(3) CHARACTER SET UTF8                           ,
      WHSEC integer,
      WHAREA integer,
      MAXLEVEL smallint,
      PRIORITY smallint,
      PAL smallint,
      MIX smallint,
      MWSCONSTLOCB integer,
      PALTYPE varchar(40) CHARACTER SET UTF8                           ,
      PALW numeric(14,2),
      PALL numeric(14,2),
      PALH numeric(14,2),
      REFILLTRY smallint,
      QUANTITY numeric(14,4))
  returns (
      MWSCONSTLOCP integer,
      MWSPALLOCP integer,
      WHAREAP integer,
      WHAREALOGP integer,
      REFILL smallint)
   as
declare variable mwsrefillmode smallint;
declare variable mwsrefillmodes varchar(100);
BEGIN
  -- wybieramy strategie uzupelniania towarow  (0-najstarsze,1-najmniejsze,2-najlepsze,3-wg daty partii)
  mwsrefillmode = null;
  select w.mwsrefillmode
    from wersje w
    where w.ref = :vers
    into mwsrefillmode;
  if (mwsrefillmode is null) then
  begin
    execute procedure get_config('MWSREFILLMODE',2) returning_values mwsrefillmodes;
     if (mwsrefillmodes is not null and mwsrefillmodes <> '') then
      mwsrefillmode = cast(mwsrefillmodes as smallint);
    else
      mwsrefillmode = 0;
  end
  -- najpierw szukamy nie mixów

  if (mwsrefillmode = 0) then  -- 0-najstarsze
  begin
    select first 1 s.mwsconstloc
      from mwsstock s
        left join mwsconstlocs c on (c.ref = s.mwsconstloc)
        left join dostawy d on (d.ref = s.lot)
      where s.wh = :wh and s.vers = :vers and s.goodsav = 0 and c.act > 0 and c.locdest = 1
        and (s.x_blocked<>1) --XXX Ldz ZG 126909
      group by s.mwsconstloc, d.data, d.datawazn
      having sum(s.blocked + s.ordered) = 0 and sum(s.quantity) > 0 and max(coalesce(s.mixedpalgroup,0)) = 0
      order by
        case
          when :mwsrefillmode = 0 and d.datawazn is not null then d.datawazn else d.data
        end
      into mwsconstlocp;
  end else if (mwsrefillmode = 1) then  -- 1-najmniejsze
  begin
    select first 1 s.mwsconstloc
      from mwsstock s
        left join mwsconstlocs c on (c.ref = s.mwsconstloc)
        left join dostawy d on (d.ref = s.lot)
      where s.wh = :wh and s.vers = :vers and s.goodsav = 0 and c.act > 0 and c.locdest = 1
        and (s.x_blocked<>1) --XXX Ldz ZG 126909
      group by s.mwsconstloc
      having sum(s.blocked + s.ordered) = 0 and sum(s.quantity) > 0 and max(coalesce(s.mixedpalgroup,0)) = 0
      order by sum(s.quantity)
      into mwsconstlocp;
  end  else if (mwsrefillmode = 2) then -- 2-najlepsze
  begin
    select first 1 s.mwsconstloc
      from mwsstock s
        left join mwsconstlocs c on (c.ref = s.mwsconstloc)
        left join dostawy d on (d.ref = s.lot)
      where s.wh = :wh and s.vers = :vers and s.goodsav = 0 and c.act > 0 and c.locdest = 1
        and (s.x_blocked<>1) --XXX Ldz ZG 126909
      group by s.mwsconstloc
      having sum(s.blocked + s.ordered) = 0 and sum(s.quantity) > 0 and max(coalesce(s.mixedpalgroup,0)) = 0
      order by abs(:quantity - sum(s.quantity))
      into mwsconstlocp;
  end else if (mwsrefillmode  = 3) then -- 3 - wg dat partii
  begin
    select first 1 s.mwsconstloc
      from mwsstock s
        left join mwsconstlocs c on (c.ref = s.mwsconstloc)
        left join dostawy d on (d.ref = s.lot)
      where s.wh = :wh and s.vers = :vers and s.goodsav = 0 and c.act > 0 and c.locdest = 1
        and (s.x_blocked<>1) --XXX Ldz ZG 126909
          and coalesce(c.x_flow_order,1) = 1
      group by s.mwsconstloc, s.x_partia, d.data, d.datawazn
      having sum(s.blocked + s.ordered) = 0 and sum(s.quantity) > 0 and max(coalesce(s.mixedpalgroup,0)) = 0
      order by s.x_partia,
        case
          when d.datawazn is not null then d.datawazn else d.data
        end
      into mwsconstlocp;    
  end else
    exception mwsrefilmode_notdefined;
  -- potem szukamy juz palet mix
  if (mwsconstlocp is null) then
  begin
    if (mwsrefillmode = 0) then
    begin
      select first 1 s.mwsconstloc
        from mwsstock s
          left join mwsconstlocs c on (c.ref = s.mwsconstloc)
          left join dostawy d on (d.ref = s.lot)
        where s.wh = :wh
          and s.vers = :vers
          and s.goodsav = 0
          and c.act > 0
          and c.locdest = 1
          and (s.x_blocked<>1) --XXX Ldz ZG 126909
        group by s.mwsconstloc, d.data, d.datawazn
        having sum(s.blocked + s.ordered) = 0 and sum(s.quantity) > 0 and max(coalesce(s.mixedpalgroup,0)) = 1
        order by
          case
            when :mwsrefillmode = 0 and d.datawazn is not null then d.datawazn else d.data
          end
        into mwsconstlocp;
    end else if (mwsrefillmode = 1) then
    begin
      select first 1 s.mwsconstloc
        from mwsstock s
          left join mwsconstlocs c on (c.ref = s.mwsconstloc)
          left join dostawy d on (d.ref = s.lot)
        where s.wh = :wh and s.vers = :vers and s.goodsav = 0 and c.act > 0 and c.locdest = 1
          and (s.x_blocked<>1) --XXX Ldz ZG 126909
        group by s.mwsconstloc
        having sum(s.blocked + s.ordered) = 0 and sum(s.quantity) > 0 and max(coalesce(s.mixedpalgroup,0)) = 1
        order by sum(s.quantity)
        into mwsconstlocp;
    end  else if (mwsrefillmode = 2) then
    begin
      select first 1 s.mwsconstloc
        from mwsstock s
          left join mwsconstlocs c on (c.ref = s.mwsconstloc)
          left join dostawy d on (d.ref = s.lot)
        where s.wh = :wh and s.vers = :vers and s.goodsav = 0 and c.act > 0 and c.locdest = 1
          and (s.x_blocked<>1) --XXX Ldz ZG 126909
        group by s.mwsconstloc
        having sum(s.blocked + s.ordered) = 0 and sum(s.quantity) > 0 and max(coalesce(s.mixedpalgroup,0)) = 1
        order by abs(:quantity - sum(s.quantity))
        into mwsconstlocp;
      end else if (mwsrefillmode  = 3) then -- 3 - wg dat partii
  begin
    select first 1 s.mwsconstloc
      from mwsstock s
        left join mwsconstlocs c on (c.ref = s.mwsconstloc)
        left join dostawy d on (d.ref = s.lot)
      where s.wh = :wh and s.vers = :vers and s.goodsav = 0 and c.act > 0 and c.locdest = 1
          and coalesce(c.x_flow_order,1) = 1
          and (s.x_blocked<>1) --XXX Ldz ZG 126909
      group by s.mwsconstloc, s.x_partia, d.data, d.datawazn
      having sum(s.blocked + s.ordered) = 0 and sum(s.quantity) > 0 and max(coalesce(s.mixedpalgroup,0)) = 1
      order by s.x_partia,
        case
          when d.datawazn is not null then d.datawazn else d.data
        end
      into mwsconstlocp;
    end

  end
END^
SET TERM ; ^
