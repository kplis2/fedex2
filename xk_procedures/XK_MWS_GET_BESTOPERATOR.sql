--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_BESTOPERATOR(
      BDATE date,
      WH varchar(3) CHARACTER SET UTF8                           ,
      PRESENCECHECK smallint,
      SKILLSLEVEL varchar(10) CHARACTER SET UTF8                           ,
      MWSORDREF integer,
      MWSORDTYPE integer,
      MWSACCESORY integer)
  returns (
      OPERREF integer,
      OPERNAME varchar(60) CHARACTER SET UTF8                           ,
      TIMESTART timestamp)
   as
declare variable wastoday smallint;
declare variable oref integer;
declare variable oname varchar(60);
declare variable maxtime timestamp;
declare variable mintime timestamp;
declare variable tempref integer;
declare variable tempname varchar(60);
declare variable tempstarttime timestamp;
declare variable startat timestamp;
declare variable times varchar(10);
declare variable takefullpal smallint;
declare variable opgroup varchar(60);
declare variable opgroupneed varchar(60);
declare variable mwsaccesorytmp integer;
-- MI - weryfikacja, MR - rozladunek, MS - sprzatanie, MW - wózkowy, MZ - zbiórka
begin
  if (skillslevel = '1') then takefullpal = 1;
  else takefullpal = 0;
  if (mwsordtype = 1 and takefullpal = 0) then opgroupneed = 'MZ';
  else if (mwsordtype = 1 and takefullpal = 1) then opgroupneed = 'MW';
  else if (mwsordtype = 1 ) then opgroupneed = 'MZ';
  else if (mwsordtype = 8) then opgroupneed = 'MR';
  else if (mwsordtype = 10) then opgroupneed = 'MW';
  else if (mwsordtype = 3) then  opgroupneed = 'MZ';
  else if (mwsordtype = 11) then opgroupneed = 'MZ';
  else if (mwsordtype = 12) then opgroupneed = 'MW';
  else if (mwsordtype = 13) then opgroupneed = 'MZ';
  else if (mwsordtype = 14) then opgroupneed = 'MW';
  else if (mwsordtype = 15) then opgroupneed = 'MW';
  else if (mwsordtype = 5) then opgroupneed = 'MS';
  else if (mwsordtype = 20) then opgroupneed = 'MS';
  else if (mwsordtype = 24) then opgroupneed = 'MS';
  if (:wh is null) then exception MWSORD_WH_NOT_SET;
  if (:skillslevel = '') then skillslevel = null;
  mintime = null;
  if (:skillslevel like 'M%') then
    opgroupneed = :skillslevel;
  for
    select op.ref, op.nazwa, op.grupa, ma.ref
      from operator op
        join opermag omag on (omag.operator = op.ref and omag.magazyn = :wh)
        join mwsaccessories ma on (ma.aktuoperator = op.ref)
      where omag.mwsordsassign = 1 and op.grupa <> '' and op.grupa is not null
      into :oref, :oname, :opgroup, :mwsaccesorytmp
  do
  begin
    if (:presencecheck = 1 and position(';'||opgroupneed||';' in opgroup) > 0) then --sprawdzanie obecnosci
    begin
      select nag.wastoday, nag.startat
        from epresencelistnag nag
          join epresencelists list on (nag.epresencelist = list.ref)
        where nag.operator = :oref
          and cast(list.listsdate as date) = :bdate
        into :wastoday, :startat;
      if (:wastoday = 1) then
      begin
        select max(ord.timestopdecl)
          from mwsords ord
          where ord.operator = :oref
            and ord.datestopdecl = :bdate
          into :maxtime;
        if (:maxtime is null) then --przypisz wolnemu operatorowi, wyjdz z petli
        begin
          tempname = :oname;
          tempref = :oref;
          if(:startat < current_timestamp(0)) then  --czy operator jest juz w pracy
            tempstarttime = current_timestamp(0);
          else --przypisz operatorowi czas rozpoczecia pracy w magazynie
            execute procedure get_config('MWSWHSTARTWORKTIME',-1) returning_values :times;
            tempstarttime = cast(cast(current_date as varchar(10))||' '||:times||':00' as timestamp);
          break;
        end
        if (:mintime is null) then  --zapamietaj pierwszego
        begin
          mintime = :maxtime;
          tempname = :oname;
          tempref = :oref;
          tempstarttime = :maxtime;
        end
        else if ((:maxtime < :mintime) and :maxtime is not null) then  --zapamietaj aktualnie najlepszego
        begin
          tempname = :oname;
          tempref = :oref;
          mintime = :maxtime;
          tempstarttime = :maxtime;
        end
      end
    end
    else if (position(';'||opgroupneed||';' in opgroup) > 0) then  --bez sprawdzania obecnosci
    begin
      select max(ord.timestopdecl)
        from mwsords ord
        where ord.mwsaccessory = :mwsaccesorytmp
          and ord.datestopdecl = :bdate
        into :maxtime;
      if (:maxtime is null) then --przypisz wolnemu operatorowi, wyjdz z petli
      begin
        tempname = :oname;
        tempref = :oref;
        execute procedure get_config('MWSWHSTARTWORKTIME',-1) returning_values :times;
        tempstarttime = cast(cast(current_date as varchar(10))||' '||:times||':00' as timestamp);  --godzina  rozpoczecia pracy magazynu
        if (current_timestamp(0) > :tempstarttime) then --magazyn juz pracuje
          tempstarttime = current_timestamp(0);
        break;
      end
      if (:mintime is null) then  --zapamietaj pierwszego
      begin
        mintime = :maxtime;
        tempname = :oname;
        tempref = :oref;
        tempstarttime = :maxtime;
      end
      else if ((:maxtime < :mintime) and :maxtime is not null) then   --zapamietaj aktualnie najlepszego
      begin
        tempname = :oname;
        tempref = :oref;
        mintime = :maxtime;
        tempstarttime = :maxtime;
      end
    end
  end
  operref = :tempref;   --zwroc dane najlepszego
  opername = :tempname;
  timestart = :tempstarttime;
--  if (mwsordtype = 1 and takefullpal = 0) then operref = null;
end^
SET TERM ; ^
