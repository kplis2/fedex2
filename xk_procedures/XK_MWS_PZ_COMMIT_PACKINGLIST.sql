--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_PZ_COMMIT_PACKINGLIST(
      MWSORDREF integer,
      SYMBOL varchar(100) CHARACTER SET UTF8                           ,
      PACKINGLIST integer,
      CLOSEPALLOC smallint,
      PALLGROUPIN integer,
      MWSPALLOCSIZE integer,
      MANMWSACTS smallint,
      WHAFTER varchar(3) CHARACTER SET UTF8                           ,
      AKTUOPERATOR integer)
  returns (
      PALGROUP integer,
      COUNTGOOD integer,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable docid integer;
declare variable cntgoodall integer;
declare variable wharea integer;
declare variable h numeric(14,4);
declare variable l numeric(14,4);
declare variable commitquantity numeric(14,4);
declare variable docposid integer;
declare variable wh varchar(3);
declare variable mainmwsact integer;
declare variable toinsert numeric(14,4);
declare variable quanmain numeric(14,4);
declare variable mwsordtype integer;
declare variable mwsconstlocl integer;
declare variable tymmwspallocl integer;
declare variable whareal integer;
declare variable wharealogl integer;
declare variable refill integer;
declare variable msgstatus smallint;
declare variable msgref integer;
declare variable segtype integer;
declare variable lot integer;
declare variable good varchar(40);
declare variable vers integer;
declare variable mwsactref integer;
declare variable mwspallocref integer;
declare variable doctype varchar(1);
begin
---------------------------
  msg = '';
  msgstatus = 0;
  if (packinglist is null or symbol = '') then
    exception MWS_ZERO_COMMITED 'Nie wprowadzono kartonu';
  --odczytanie rozmiarów palety
  select l, h, segtype
    from mwspallocsizes
    where ref = :mwspallocsize
    into l, h, segtype;
  select mo.docid, mo.wh, mo.mwsordtype, mo.doctype
    from mwsords mo
    where mo.ref = :mwsordref
    into :docid, :wh, :mwsordtype, doctype;
  if (whafter = wh) then whafter = null;
  if (pallgroupin = 0) then begin
    execute procedure gen_ref('MWSPALLOCS') returning_values mwspallocref;
    insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status,
        fromdocid, fromdocposid, fromdoctype, segtype)
      values(:mwspallocref,cast(:mwspallocref as varchar(20)), null, 2, 0, :docid, :docposid, :doctype, :segtype);
  end else begin
    mwspallocref = :pallgroupin;
  end
  palgroup = :mwspallocref;
 /* -- sprawdzenie czy juz drukowano etykiete
  if (exists(select first 1 a.ref from mwsacts a where a.mwsord = :mwsordref
      and a.mwspallocl = :palgroup and a.status = 2)
  ) then
    printer = '';
  else
    printer = defaultprinter;     */

  execute procedure XK_MWS_GET_BEST_LOCATION(:good, :vers,null,:mwsordref,:mwsordtype,
      :wh, null, null, 3, 1, 0, 0,null,null, 120,:l,:h,0,null)
    returning_values (mwsconstlocl, tymmwspallocl, whareal, wharealogl, refill);
  lot = null;
  if (exists(select first 1 1 from mwsacts a where a.mwsord = :MWSORDREF and packsymbol = :symbol)) then
    exception MWS_ZERO_COMMITED 'Karton: '||symbol||' został już zarejestrowany';
  for
    select GOODOUT, QUANTITYOUT, VERSOUT
      from mws_packinglist_dets(:packinglist,null,null,:symbol)
      order by goodout
      into good, commitquantity, vers
  do begin
    for
      select ma.docposid, ma.ref, ma.quantity, ma.lot
        from mwsacts ma
        where ma.mwsord = :mwsordref
          and ma.vers = :vers
          and ma.status = 0
          and ma.quantity > 0
        into :docposid, :mainmwsact, :quanmain, :lot
    do begin
      if (coalesce(lot,0) = 0) then
        exception universal 'Brak okreslonej dostawy na dokumencie magazynowym';
      if (msgstatus = 0) then
        msgstatus = 1;
      if (quanmain < commitquantity) then begin
        toinsert = :quanmain;
        commitquantity = commitquantity - quanmain;
      end else begin
        toinsert = :commitquantity;
        commitquantity = 0;
      end
      if (toinsert > 0) then begin
        execute procedure gen_ref('MWSACTS') returning_values mwsactref;
        insert into mwsacts (ref, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
              mwsconstlocl, mwspallocl, doctype, settlmode, closepalloc, wh,
              regtime, timestartdecl, timestopdecl,  lot, PALGROUP, ISPAL, MIXEDPALLGROUP, RECDOC, docid, h, l,
              docposid, wh2, mwspallocsize, segtype, packsymbol, operator)
          values (:mwsactref, :MWSORDREF, 0, 0, :good, :vers, :toinsert, null, null,
                :mwsconstlocl, :mwspallocref, :doctype, null, 0, :wh, current_timestamp(0), current_timestamp(0),
                current_timestamp(0), :lot, :mwspallocref, 0, 0, 1, :docid, :h, :l,
                :docposid, :whafter, :mwspallocsize, :segtype, :symbol, :aktuoperator);
        update mwsacts m set m.status = 1 where m.ref = :mwsactref;
        update mwsacts m set m.status = 2 where m.ref = :mwsactref;
        update mwsacts m set m.quantity = m.quantity - :toinsert where m.ref = :mainmwsact;
      end
      if (commitquantity <= 0) then break;
    end
    lot = null;
    if (commitquantity > 0) then
    begin    
      select d.dostawa
        from mwsords o
          join dokumnag d on (d.ref = o.docid)
        where o.ref = :mwsordref
        into lot;
      if (coalesce(lot,0) = 0) then
        exception universal 'Brak okreslonej dostawy na dokumencie magazynowym';
      execute procedure gen_ref('MWSACTS') returning_values mwsactref;
      insert into mwsacts (ref, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
            mwsconstlocl, mwspallocl, doctype, settlmode, closepalloc, wh,
            regtime, timestartdecl, timestopdecl,  lot, PALGROUP, ISPAL, MIXEDPALLGROUP, RECDOC,
            docid, h, l, wh2, mwspallocsize, segtype, packsymbol, operator)
        values (:mwsactref, :MWSORDREF, 0, 0, :good, :vers, :commitquantity, null, null,
              :mwsconstlocl, :mwspallocref, :doctype, null, 0, :wh, current_timestamp(0), current_timestamp(0),
              current_timestamp(0), :lot, :mwspallocref, 0, 0, 1,
              :docid, :h, :l, :whafter, :mwspallocsize, :segtype, :symbol, :aktuoperator);
      update mwsacts m set m.status = 1 where m.ref = :mwsactref;
      update mwsacts m set m.status = 2 where m.ref = :mwsactref;
    end
  end
  select count(m.good)
    from mwsacts m
      left join towary t on(t.ktm = m.good)
    where m.palgroup = :mwspallocref
      and m.mwsord = :mwsordref
      and t.paleta <> 1
    into :cntgoodall;
  if (cntgoodall > 1) then
    update mwsacts m set m.mixedpallgroup = 1 where m.palgroup = :mwspallocref;
  else
    update mwsacts m set m.mixedpallgroup = 0 where m.palgroup = :mwspallocref;
  if (closepalloc = 1) then begin
    execute procedure xk_mws_pz_close_mwspalloc(:mwsordref, :mwspallocref,iif(manmwsacts = 1,2,1));
   -- printer = defaultprinter;
  end
  select count(m.good)
    from mwsacts m
    where m.palgroup = :mwspallocref
      and m.status = 5
      and m.mwsord = :mwsordref
    into :countgood;
  select mo.bwharea
    from mwsords mo
    where mo.ref = :mwsordref
    into :wharea;
end^
SET TERM ; ^
