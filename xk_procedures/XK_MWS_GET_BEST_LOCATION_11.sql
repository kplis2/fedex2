--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_BEST_LOCATION_11(
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      REFMWSACT integer,
      REFMWSORD integer,
      MWSORDTYPE integer,
      WH varchar(3) CHARACTER SET UTF8                           ,
      WHSEC integer,
      WHAREA integer,
      MAXLEVEL smallint,
      PRIORITY smallint,
      SEGTYPE smallint,
      MIX smallint,
      MWSCONSTLOCB integer,
      PALTYPE varchar(40) CHARACTER SET UTF8                           ,
      PALW numeric(14,2),
      PALL numeric(14,2),
      PALH numeric(14,2),
      REFILLTRY smallint,
      QUANTITY numeric(14,4))
  returns (
      MWSCONSTLOCL integer,
      MWSPALLOCLL integer,
      WHAREAL integer,
      WHAREALOGL integer,
      REFILL smallint)
   as
declare variable sell smallint;
begin
  if (segtype is null) then segtype = 1;
  -- sprawdzamy czy towar jest potrzebny do sprzedazy
  select first 1 case when vers is not null then 1 else 0 end
    from xk_mws_get_best_location_11_a(:wh,:vers)
    group by vers
    having sum(quantity) < max(daysell)
    into sell;
  -- jak tak to trafia na lokacje do zbierania - lokacje dynamiczne w regalach
  if (sell > 0) then
  begin
    select first 1 c.ref
      from mwsconstlocs c
        left join mwsstock s on (s.mwsconstloc = c.ref)
      where c.wh = :wh and c.locdest = 5 and s.ref is null
      into mwsconstlocl;
    if (mwsconstlocl is null) then
      select first 1 c.ref
        from mwsconstlocs c
          left join mwsstock s on (s.mwsconstloc = c.ref)
        where c.wh = :wh and c.locdest = 9 and s.ref is null
        into mwsconstlocl;
  end
  -- jak nie to trafia w bufor
  -- szukamy lokacji zgodnej z wymiarami w regalach do tego przeznaczonych (H; EPAL)
--  exception test_break pall||' '||palw||' '||palh;
  if (mwsconstlocl is null) then
  begin
    select first 1 f.ref
      from mwsfreemwsconstlocs f
        left join mwsconstlocs c on (c.ref = f.ref)
        left join whsecrows w on (w.ref = c.whsecrow)
        left join whsecs s on (s.ref = c.whsec)
        left join mwsstands m on (m.ref = c.mwsstand)
        left join mwsstandtypes t on (t.ref = m.mwsstandtype)
      where c.goodsav = 0 and f.wh = :wh and f.l = :pall and f.w = :palw and f.h >= :palh
        and s.deliveryarea in (2,6) and t.segtype = 1
      order by f.h - :palh, w.symbol
      into mwsconstlocl;
    if (mwsconstlocl is not null) then
      exit;
  end
  -- jak nie to szukamy pietra segmentu H+1, gdzie jest juz przynajmniej jedna
  if (mwsconstlocl is null and segtype = 2) then
  begin
    execute procedure xk_mws_get_best_location_11_b(:wh,1,0,:palh)
      returning_values :mwsconstlocl;
  end
  -- jak nie ma takiego to wstawiamy do regalu i dezaktywujemy lokacje boczna
  if (mwsconstlocl is null and segtype = 2) then
  begin
    execute procedure xk_mws_get_best_location_11_b(:wh,0,1,:palh)
      returning_values :mwsconstlocl;
  end
  -- jak nie ma takiego to wstawiamy do regalu i dezaktywujemy lokacje boczna tam gdzie stoi juz towar wymiarowy
  if (mwsconstlocl is null and segtype = 2) then
  begin
    execute procedure xk_mws_get_best_location_11_b(:wh,0,2,:palh)
      returning_values :mwsconstlocl;
  end
  -- teraz szukamy w regalach wymiarowych
  if (mwsconstlocl is null and segtype = 2) then
  begin
    execute procedure xk_mws_get_best_location_11_c(:wh,1,0,:palh)
      returning_values :mwsconstlocl;
  end
  -- jak nie ma takiego to wstawiamy do regalu i dezaktywujemy lokacje boczna
  if (mwsconstlocl is null and segtype = 2) then
  begin
    execute procedure xk_mws_get_best_location_11_c(:wh,0,1,:palh)
      returning_values :mwsconstlocl;
  end
  -- jak nie ma takiego to wstawiamy do regalu i dezaktywujemy lokacje boczna tam gdzie stoi juz towar wymiarowy
  if (mwsconstlocl is null and segtype = 2) then
  begin
    execute procedure xk_mws_get_best_location_11_c(:wh,0,2,:palh)
      returning_values :mwsconstlocl;
  end
  if (mwsconstlocl is null) then
  begin
    select first 1 c.ref
      from mwsconstlocs c
        left join mwsstock s on (s.mwsconstloc = c.ref)
      where c.wh = :wh and c.locdest = 5 and s.ref is null
      into mwsconstlocl;
  end
  if (mwsconstlocl is null) then
  begin
    select first 1 c.ref
      from mwsconstlocs c
      where c.wh = :wh and c.locdest = 9
      into mwsconstlocl;
  end
end^
SET TERM ; ^
