--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_PROPERSRAPS_ADD(
      PRSCHEDOPER integer,
      CZYRAPORTOWAC smallint)
  returns (
      CLOSEOPER smallint)
   as
declare variable ilosc numeric(14,4);
declare variable propersrapref integer;
declare variable prschedoperstatus integer;

begin
  closeoper = 0;
  prschedoperstatus = null;
  if (czyraportowac = 1) then begin
    select coalesce(p.reportedfactor,1) * p.amountin
    from prschedopers p
    where p.ref = :prschedoper
    into :ilosc;
    select first 1 p.ref
      from propersraps p
      where p.prschedoper = :prschedoper
      into :propersrapref;
    if (:propersrapref is null) then
    begin
      insert into propersraps(amount, prschedoper)
        values (:ilosc,:prschedoper);
    end else
      update propersraps p set p.amount = :ilosc, p.prschedoper = :prschedoper
        where p.prschedoper = :prschedoper;
  end
  select p.status
    from prschedopers p
      join prschedoperdeps pd on (pd.depfrom = p.ref)
    where pd.depto = :prschedoper
    into :prschedoperstatus;
  if ((:prschedoperstatus is null) or (:prschedoperstatus=3)) then begin
    closeoper = 1;
  end
end^
SET TERM ; ^
