--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_HH_NAGZAM_BROWSE(
      OPERATOR integer,
      WH varchar(3) CHARACTER SET UTF8                           )
  returns (
      REF integer,
      SYMBOL varchar(40) CHARACTER SET UTF8                           ,
      STATUS varchar(3) CHARACTER SET UTF8                           )
   as
declare variable statusint smallint;
begin
  /*xxx*/
  for
    select n.ref, n.id, coalesce(n.status,0)
      from nagzam n
      where n.rejestr = 'INO'
       and n.typzam = 'ZAM'
       and n.magazyn = :wh
    into ref, symbol, statusint
  do begin
    if (statusint = 0) then
      status = '';
    else if (statusint = 1) then
      status = ' x ';
    else if (statusint = 2) then
      status = ' v ';
    suspend;
  end
  /*xxx*/
end^
SET TERM ; ^
