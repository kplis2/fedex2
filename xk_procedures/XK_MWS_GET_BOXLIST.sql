--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_BOXLIST(
      LISTYWYSD INTEGER_ID)
  returns (
      STANOWISKO STRING40,
      LOKACJA STRING40)
   as
  declare variable refdok integer_id;
begin
  for
    select STANOWISKO, X_MWSCONSTLOCK
      from LISTYWYSDROZ_OPK
      WHERE LISTWYSD = :listywysd
  into :stanowisko, :lokacja
  do begin
   if (lokacja is null) then
     lokacja ='-';
   if (stanowisko is null) then
     stanowisko ='-';
    suspend;
  end
  select l.refdok
    from listywysd l
    where l.ref = :listywysd
  into :refdok;
  for
    select distinct mm.symbol, m.mwsconstlocsymb
      from dokumnag d
      join mwsordcartcolours m on m.docid = d.ref
      join mwsconstlocs mm on mm.ref = m.cart
        where d.grupasped = :refdok
    into :stanowisko, :lokacja
    do begin
      if (lokacja is null) then
        lokacja ='-';
      if (stanowisko is null) then
        stanowisko ='-';
      suspend;
    end
end^
SET TERM ; ^
