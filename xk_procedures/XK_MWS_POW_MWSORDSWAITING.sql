--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_POW_MWSORDSWAITING(
      WHIN varchar(3) CHARACTER SET UTF8                           )
  returns (
      ACK smallint)
   as
declare variable MAXLEVEL integer;
declare variable DOCID integer;
declare variable DOCPOSID integer;
declare variable WH varchar(3);
declare variable VERS integer;
declare variable STATUS smallint;
declare variable TRYCNT integer;
declare variable TRYCNTAFTER integer;
declare variable SUMPOSTOREAL integer;
declare variable MAXDOCGEN integer;
declare variable DOCSREAL integer;
declare variable POSLEFT integer;
declare variable POSTOREAL integer;
declare variable MWSGENPARTS varchar(100);
declare variable MINPOSTOREALS varchar(100);
declare variable MINPOSTOREAL integer;
declare variable DNIWSTECZ integer;
declare variable MWSORD integer;
declare variable MREF integer;
declare variable REALFASTER smallint;
begin
  dniwstecz = 30;
  ack = 1;
  maxlevel = 20;
  -- usuwamy z tabeli oczekujących pozycje dokumentów które to pozycje zostaly w miedzy czasie usuniete
  for
    select  m.ref
      from mwsgoodsrefill m
        left join dokumpoz p on (p.ref = m.docposid)
      where p.ref is null
      into mref
  do begin
    delete from mwsgoodsrefill m where m.ref = :mref;
  end
  -- usuwamy z tabeli oczekujących wszyskie pozycje dokumentów które to dokumenty zostaly w miedzy czasie usuniete
  --(bez sensu bo przeciez w kroku wczesniej usunelismy to spradzajac czy pozycje dokuemtow nie zostaly usuniete)
  for
    select m.ref
      from mwsgoodsrefill m
        left join dokumnag n on (n.ref = m.docid and (n.akcept = 1 or n.mwsdisposition = 1))
      where n.ref is null
      into mref
  do begin
    delete from mwsgoodsrefill m where m.ref = :mref;
  end

  -- sprawdzenie czy juz nie ma naszykowanej czesci zlecenia. jak jest to pozostala czesc wyzszy priorytet
  for
    select m.ref
      from mwsords m
      where m.wh = :whin and m.mwsordtype = 1 and m.status = 1 and m.priority > 2
        and exists (select m1.ref from mwsords m1 where m1.docgroup = m.docgroup and m1.status > 1)
      into mwsord
  do begin
    update mwsords set priority = 2 where ref = :mwsord;
  end

  -- minimalna ilosc pozycji do wygenerowania zlecenia
  execute procedure get_config('MWSMINPOSTOREAL',2) returning_values minpostoreals;
  if (minpostoreals is not null and minpostoreals <> '') then
    minpostoreal = cast(minpostoreals as integer);
  else
    minpostoreal = 15;

  -- czy mozna dzielic dokuemnty na kilka zlecen
  execute procedure get_config('MWSGENPARTS',2) returning_values mwsgenparts;

  -- zmianiamy status pozycji tabeli oczekujacych ktre z jakiegos powodu wczesniej byly oznaczone jako do poxniejszej realziacji
  --(bo sie zmoianila bierzaca data lub pewnie bo sie zmianila data realziacji dokumentu)
  for
    select m.ref, d.realfaster, m.docposid, m.vers, d.magazyn
      from mwsgoodsrefill m
        left join dokumnag d on (d.ref=m.docid)
      where m.wh = :whin and m.reallater = 1 and cast(m.realdate as date) = current_date
      into mref, realfaster, docposid, vers, wh
  do begin
    delete from mwsgoodsrefill where docposid = :docposid;
    delete from docpos2real where docposid = :docposid;
    update mwsgoodsrefill m set m.reallater = 0, m.preparegoods = 1 where ref = :mref;
    execute procedure mws_docpos_waiting_pow (wh,docposid,vers);
  end

  for
    select m.ref, d.realfaster, m.docposid, m.vers, d.magazyn
      from mwsgoodsrefill m
        left join dokumnag d on (d.ref=m.docid)
      where m.wh = :whin and m.reallater = 1 and cast(m.realdate as date) > current_date and d.realfaster = 1
      into mref, realfaster, docposid, vers, wh
  do begin
    delete from mwsgoodsrefill where docposid = :docposid;
    delete from docpos2real where docposid = :docposid;
    update mwsgoodsrefill m set m.reallater = 0, m.preparegoods = 1 where ref = :mref;
    execute procedure mws_docpos_waiting_pow (wh,docposid,vers);
  end

  -- naliczamy tabele pozycji oczekujacych dla nowych pozycji dokumentów (dla takich dla ktoych nie ma pozycji w mwsgoodsrefill )
  for
    select coalesce(n.mwsmag, n.magazyn), p.wersjaref, p.ref
      from dokumpoz p
        join dokumnag n on (n.ref = p.dokument)
        join defdokummag dm on (dm.typ = n.typ and n.magazyn = dm.magazyn)
        join towary t on (t.ktm = p.ktm)
        left join mwsgoodsrefill m on (m.docposid = p.ref)
        left join dokumpoz p1 on (p1.ref = p.alttopoz and p.fake = 1)
        left join towary t1 on (t1.ktm = p1.ktm)
      where coalesce(n.mwsmag, n.magazyn) = :whin
        and dm.mwsordwaiting = 1
        and t.usluga <> 1
        and p.genmwsordsafter = 1
        and m.ref is null
        and ((coalesce(p.havefake,0) = 0 and coalesce(p.fake,0) = 0) or
             (coalesce(p.fake,0) = 1 and t1.usluga <> 1 and coalesce(t1.altposmode,0) = 1))
        and ((n.akcept = 1 and n.mwsdisposition = 0) or
             (n.akcept = 0 and n.mwsdisposition = 1))
        and n.data > '2013-11-01' --current_date - :dniwstecz
        and coalesce(n.blockmwsords,0) = 0
      into wh, vers, docposid
  do begin
    execute procedure mws_docpos_waiting_pow (wh,docposid,vers);
  end
  -- zaznaczamy rekordy do sprawdzenia czy da sie je zrealizowac
  -- obliczamy ilosc pozycji do szykwowania
  select count(a.ref)
    from mwsacts a
      join mwsords o on (o.ref = a.mwsord)
    where o.wh = :whin and o.status > 0 and o.status < 3
      and a.status > 0 and a.status < 2 and a.mwsordtypedest = 1
      and a.regtime > current_date - 3
    into sumpostoreal;
  if (sumpostoreal is null) then sumpostoreal = 0;
  if (sumpostoreal >= 1000) then maxdocgen = 12;
  else if (sumpostoreal < 1000 and sumpostoreal >= 750) then maxdocgen = 14;
  else if (sumpostoreal < 750 and sumpostoreal >= 400) then maxdocgen = 16;
  else if (sumpostoreal < 400 and sumpostoreal >= 200) then maxdocgen = 18;
  else if (sumpostoreal < 200 and sumpostoreal >= 0) then maxdocgen = 20;
  else maxdocgen = 12;
  if (maxdocgen is null or maxdocgen = 0) then
    maxdocgen = 10;
  -- wystawiamy zlecenia do dokumentow ktore da sie zrealizowac
  docsreal = 0;
  for
    select first 40 distinct m.docid, coalesce(d.mwsmag,d.magazyn) --DD
      from mwsgoodsrefill m
        left join dokumnag d on (d.ref = m.docid and m.doctype = 'M'
          and (d.blokada = 0 or d.blokada = 4))
        left join sposdost s on (s.ref = d.sposdost)
      where d.ref is not null and m.reallater = 0
        and m.wh = :whin
        and coalesce(d.blockmwsords,0) = 0
      group by m.docid, coalesce(d.mwsmag,d.magazyn)
      order by min(coalesce(m.lastcheck,current_timestamp(0) - 100)), min(d.priorytet)
      into docid, wh
  do begin
    docsreal = docsreal + 1;

    -- dlaczego kasujemy wszystkie wpisy w tabeli pozycji oczekujących dla tego dokumentu, przecież najprawdopodobniej dopiero co go naliczylismy ?
    delete from mwsgoodsrefill where docid = :docid;
    delete from docpos2real where docid = :docid;
    for
      select p.wersjaref, p.ref, coalesce(n.mwsmag, n.magazyn) 
        from dokumpoz p
          join dokumnag n on (n.ref = p.dokument)
          join towary t on (p.ktm = t.ktm)
          left join dokumpoz p1 on (p1.ref = p.alttopoz and p.fake = 1)
          left join towary t1 on (t1.ktm = p1.ktm)
        where n.ref = :docid
          and ((coalesce(p.havefake,0) = 0 and coalesce(p.fake,0) = 0) or
               (coalesce(p.fake,0) = 1 and t1.usluga <> 1 and coalesce(t1.altposmode,0) = 1))
          and p.genmwsordsafter = 1
          --and p.iloscl - coalesce(p.ilosconmwsacts,0) > 0
          and t.usluga <> 1
      into vers, docposid, wh
    do begin
      -- na nowo naliczmy tabele pozycji oczekujacych dla tego dokumentu
      execute procedure mws_docpos_waiting_pow (wh,docposid,vers);
    end
    status = 0;
    -- oznaczenie daty ostatniego sprawdzania pozycji
    update mwsgoodsrefill set lastcheck = current_timestamp(0) where docid = :docid;
    -- oznaczamy dokument magazynowy jako już wyliczony w tabeli oczekujacych
    update dokumnag set mwsdocreal = 4, genmwsordsafter = 0 where ref = :docid;

    -- naliczanie tabeli docpos2real
    -- bez sensu ustawiony stausin = 2 poniewz w srodku jest warunek   if (maxlevel > 1) then statusin = 2;  else statusin = 1;  wiec i tak statusin bedzie 1
    -- POSLEFT - ilosc pozycji na dokumencie magazynowym na ktre nie zostaly wygenerowane pozycje zlecnia magazynowego na wymagana ilosć (brane z pola dokumpoz.ilosconmwsacts )
    -- POSTOREAL  - wyliczona w  docpos2real ilosc pozycji możliwych do zrealizowania
    execute procedure MWS_CHECK_DOCPOS_REAL_POW(:wh,:docid,null,0,1,2)
      returning_values posleft, postoreal;

    -- posreal = 0 - nie można generowac zlecenia wiec usuwamy wpisy w docpos2real (teoretycznie wcale ich tam nie powinno być)
    delete from docpos2real where docid = :docid and posreal = 0;

    -- posreal = 1 mamy cala potrzebną ilosc na lokacjach dostepnych dla pikerow.
    select count(docposid) from docpos2real where docid = :docid and posreal = 1
      into postoreal;
    if (postoreal is null) then postoreal = 0;
    status = 4;
    if (postoreal = posleft) then status = 1;
    if (status = 0 or status = 4) then
    begin
      -- sprawdzamy mozliwosci pobrania towaru z wyzszych lokacji (chyba na pelne palety choc mglo to byc i tak obliczone w pierwszym przebiegu MWS_CHECK_DOCPOS_REAL_POW )
      execute procedure MWS_CHECK_DOCPOS_REAL_POW(:wh,:docid,null,1,maxlevel,2)
        returning_values posleft, postoreal;
                    --      if (docid = 383) then
                    --      exception universal 'kbi 1222' ;
      -- posreal = 2 - realizacja penej palety (moga byc wylicozna w czasie pierwszego przebiegu MWS_CHECK_DOCPOS_REAL_POW )
      select count(docposid) from docpos2real where docid = :docid and posreal = 2
        into postoreal;
      if (postoreal is not null and postoreal > 0) then
        status = 2;
    end
    if (status <> 1) then
    begin
      -- realizujemy tylko pelne palety
      select count(docposid) from docpos2real where docid = :docid and posreal = 2
        into postoreal;
      if (postoreal is null) then postoreal = 0;
      if (postoreal > 0) then
        status = 5;
      else status = 4;
      if (mwsgenparts = '1' and status <> 1 and status <> 2 and status <> 5) then
      begin
        select count(docposid) from docpos2real where docid = :docid
          and (posreal = 1 or posreal = 5)
          into postoreal;
        if (postoreal is null) then postoreal = 0;
        if (postoreal > 0) then
          status = 5;
      end
      else if (minpostoreal > 0) then
      begin
        select count(docposid) from docpos2real where docid = :docid and posreal = 1
          into postoreal;
        if (postoreal is null) then postoreal = 0;
        if (postoreal > 0 and postoreal >= minpostoreal) then
          status = 5;
      end
    end
    -- jezeli mozna zrealzowac dokument to realizujemy zlecenie
    if (status = 1 or status = 2 or status = 5) then
    begin
      update dokumnag set mwsdocreal = :status, genmwsordsafter = 1 where ref = :docid;
    end else
      update dokumnag set mwsdocreal = :status, genmwsordsafter = 0 where ref = :docid;
    if (docsreal >= maxdocgen) then
      break;
  end
  ACK = 1;
end^
SET TERM ; ^
