--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_USTAL_CENE(
      NRCENNIKA integer,
      WERSJAREF integer,
      JEDN integer,
      BN varchar(1) CHARACTER SET UTF8                           ,
      CENA numeric(14,2),
      DOSTAWA integer,
      KLIENT integer,
      ODDZIAL varchar(40) CHARACTER SET UTF8                           ,
      TYP varchar(1) CHARACTER SET UTF8                           ,
      REF integer)
  returns (
      STATUS integer,
      CENACEN numeric(14,4),
      WALCEN varchar(3) CHARACTER SET UTF8                           ,
      JEDNCEN integer)
   as
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable calcquantity numeric(14,4);
declare variable sheet integer;
declare variable calc integer;
declare variable prcalccol integer;
declare variable stawka numeric(14,2);
declare variable grvat varchar(5);
begin
  status = 0;
  walcen = 'PLN';
  jedncen = 0;
  calcquantity = 1;
  sheet = null;
  calc = null;
  prcalccol = null;
  cenacen = null;
  -- procedura szuka ceny sprzedazy dla wyrobow na podstawie domyslnej kalkulacji domyslnej karty technologicznej
  select KTM,nrwersji,vat from WERSJE where ref=:wersjaref into :ktm,:wersja,:grvat;
  if(:grvat is null) then select vat from towary where ktm=:ktm into :grvat;
  select REF,QUANTITY from PRSHEETS where KTM=:ktm and WERSJA=:wersja and STATUS=2
  into :sheet,:calcquantity;
  if(:sheet is null) then exit;
  if(:calcquantity is null or :calcquantity=0) then calcquantity = 1;
  select first 1 ref from PRSHCALCS where SHEET=:sheet and STATUS = 2 into :calc;
  if(:calc is null) then exit;
  select first 1 ref from PRCALCCOLS where CALC=:calc and PRCOLUMN=17 and CALCTYPE=0 into :prcalccol;
  if(:prcalccol is null) then exit;
  select CVALUE from PRCALCCOLS where ref=:prcalccol into :cenacen;
  if(:cenacen is null) then cenacen = 0;
  cenacen = :cenacen / :calcquantity;
  if(:bn = 'B') then begin
    select VAT.stawka from VAT where vat.grupa = :grvat into :stawka;
    cenacen = :cenacen*(100+:stawka)/100;
  end
  if(:cenacen is not null and :cenacen<>0) then status = 1;
  suspend;
end^
SET TERM ; ^
