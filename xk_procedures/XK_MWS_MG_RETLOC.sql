--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MG_RETLOC(
      MWSORD integer)
  returns (
      LOCSYMBOL varchar(20) CHARACTER SET UTF8                           )
   as
begin
  select  first 1 mc.symbol
    from mwsacts ma join mwsconstlocs mc on(ma.mwsconstlocp = mc.ref)
    where ma.mwsord = :mwsord
    into :locsymbol;
  suspend;
end^
SET TERM ; ^
