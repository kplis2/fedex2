--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MOVEALLGOODS(
      MWSORD integer,
      MWSCONSTLOCLS varchar(20) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint)
   as
declare variable mwsconstlocl integer;
begin
-- status = 0 - brak lokacji
-- status = 1 - wszystko ok
  status = 1;
  mwsconstlocl = null;
  select c.ref from mwsconstlocs c where c.symbol = :mwsconstlocls into :mwsconstlocl;
  if (:mwsconstlocl is null or coalesce(:mwsconstlocls,'') = '') then
  begin
    status = 0;
    exit;
  end
  else
  begin
    update mwsacts a
      set a.mwsconstlocl = :mwsconstlocl,
          a.quantityc = a.quantity,
          a.status = 2
      where a.mwsord = :mwsord
        and a.status < 3
        and (a.mwsconstlocp <> a.mwsconstlocl or a.mwsconstlocl is null);
    update mwsacts a
      set a.status = 5
      where a.mwsord = :mwsord
        and a.status < 3
        and a.mwsconstlocl = :mwsconstlocl;
  end
end^
SET TERM ; ^
