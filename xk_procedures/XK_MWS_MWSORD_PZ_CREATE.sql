--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSORD_PZ_CREATE(
      DOSTAWCA integer,
      AKTUOPERATOR integer,
      AUTOREALIZE smallint)
  returns (
      MWSORDREF integer)
   as
declare variable REALIZED integer;
declare variable PERIOD varchar(6);
declare variable BRANCH varchar(10);
declare variable WH varchar(3);
declare variable MWSORDTYPE integer;
declare variable STOCKTAKING smallint;
declare variable ZAGRANICZNY smallint;
declare variable MANMWSACTS smallint;
declare variable SLOKOD varchar(40);
declare variable DOKTYP varchar(3);
declare variable REC smallint;
declare variable COR smallint;
begin
  branch = 'LTD_KR';
  wh = 'M88';

  select first 1 t.ref, t.stocktaking
    from mwsordtypes t
    where t.mwsortypedets = 2
    order by t.ref
    into :mwsordtype, :stocktaking;

  select substring(d.id from 1 for 40), d.typdokmag --XXX ZG133796 MKD
    from dostawcy d
    where d.ref = :dostawca
  into :slokod, :doktyp;
  if (coalesce(:doktyp,'') = '') then doktyp = 'PZ';

  select d.wydania, d.zewn
    from defdokum d
    where d.symbol = :doktyp
  into :rec, :cor;
  if (rec = 0) then rec = 1; else rec = 0;
  if (manmwsacts is null) then manmwsacts = 0;

  execute procedure gen_ref('MWSORDS') returning_values mwsordref;
  select okres from datatookres(current_date,0,0,0,0) into period;
  insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator,
      priority, description, wh, difficulty, regtime, timestartdecl, timestopdecl,
      mwsaccessory, branch, period, flags, docsymbs, cor, rec, bwharea, ewharea,
      status, shippingarea, slodef, slopoz, slokod, manmwsacts)
    values (:mwsordref, :mwsordtype, :stocktaking, 'M', null, null, null,
      0, '', :wh, null, current_timestamp(0), null, current_timestamp(0),
      null, :branch, :period, null, '', :cor, :rec, null, null,
      1, null, 6, :dostawca, :slokod, :manmwsacts);

  if (coalesce(:autorealize,0) > 0) then
    execute procedure XK_MWS_MWSORD_CHANGE_STATUS(:mwsordref, 2, :aktuoperator)
      returning_values :realized;
  suspend;
end^
SET TERM ; ^
