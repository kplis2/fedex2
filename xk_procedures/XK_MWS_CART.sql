--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_CART(
      CARTSYMB VARCHAR_60)
  returns (
      CURRENTCART INTEGER_ID,
      STATUS SMALLINT_ID,
      DOCID INTEGER_ID)
   as
begin
status = 1;
    select first 1 m.ref, c.docid
      from mwsconstlocs m
      left join mwsordcartcolours c ON m.ref = c.cart
      where m.symbol = :cartsymb   and c.status <2
    into :CURRENTCART, :docid;
end^
SET TERM ; ^
