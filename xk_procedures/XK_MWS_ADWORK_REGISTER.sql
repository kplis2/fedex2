--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_ADWORK_REGISTER(
      SUPERVISOR smallint,
      OPERLOGIN varchar(40) CHARACTER SET UTF8                           ,
      WH varchar(3) CHARACTER SET UTF8                           ,
      WORKDICT integer,
      WORKTYPE integer,
      MWSORD integer = 0)
  returns (
      MSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable mwsordtype integer;
declare variable mwsordref integer;
declare variable period varchar(6);
declare variable branch varchar(10);
declare variable workdesc varchar(100);
declare variable operator integer;
declare variable timesum double precision;
declare variable eol varchar(10);
declare variable worktime varchar(40);
declare variable act smallint;
begin
  msg = '';
  eol = '
';
  select coalesce(akt,0), kod from slopoz where ref = :worktype
    into act, workdesc;
  if (supervisor = 0 and act = 0) then
  begin
    msg = 'Operator nie ma uprawnień do rejestracji prac dodatkowych !';
    exit;
  end
  select first 1 t.ref
    from mwsordtypes t
      left join mwsordtypes4wh w on (w.mwsordtype = t.ref)
    where t.mwsortypedets = 13 and w.wh = :wh
    into mwsordtype;
  if (mwsordtype is null) then
    exception universal 'Brak zlecenia dla prac dodatkowych';
  select first 1 o.ref
    from operator o
      left join mwsordtypedest4op p on (p.operator = o.ref and p.wh = :wh)
    where p.operator is not null and o.login = :operlogin
    into operator;
  if (:operator is null) then
    exception universal 'Brak uprawnień operatora do magazynu';
  select first 1 o.ref
    from mwsords o
    where o.mwsordtype = :mwsordtype and o.status = 2 and o.operator = :operator
      and o.slodef = :workdict and o.slopoz = :worktype
    into mwsordref;
  if (mwsordref is null) then
  begin
    select oddzial from defmagaz where symbol = :wh
      into branch;
    execute procedure gen_ref('MWSORDS') returning_values mwsordref;
    select okres from datatookres(current_date,0,0,0,0) into period;
    insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator,
        priority, description, wh, difficulty, regtime, timestartdecl, timestopdecl,
        mwsaccessory, branch, period, flags, docsymbs, cor, rec, bwharea, ewharea,
        status, shippingarea, slodef, slopoz, slokod, manmwsacts, frommwsord)
      values (:mwsordref, :mwsordtype, 0, 'P', null, null, :operator,
          0, '', :wh, '', current_timestamp(0), current_timestamp(0), current_timestamp(0),
          null, :branch, :period, '', '', 0, 0, null, null,
          1, '', :workdict, :worktype, :workdesc, 0, :mwsord);
    update mwsords set status = 2, timestart = current_timestamp(0) where ref = :mwsordref;
  end else
  begin
    update mwsords set status = 5, timestop = current_timestamp(0) where ref = :mwsordref;
    for
      select o.slokod, sum(o.timestop - case when o.timestart < current_timestamp(0) - 1 then current_timestamp(0) - 1 else o.timestart end)
        from mwsords o
        where o.mwsordtype = :mwsordtype and o.operator = :operator
          and o.slodef = :workdict and o.status = 5 and o.timestop >= current_timestamp(0) - 1
        group by o.slokod
        into workdesc, timesum
    do begin
      worktime = '';
      execute procedure XK_MWS_TIMESTAMP_DIFF_TOSTRING(timesum)
        returning_values worktime;
      msg = msg||:workdesc||': '||:worktime||eol;
    end
  end
end^
SET TERM ; ^
