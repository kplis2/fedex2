--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSD_SPOSDOST(
      LISTWYSD integer)
  returns (
      REF integer,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      PRIORYTET integer)
   as
begin
/*
Procedura zwracajaca preferowanego spedytora dla pakowania
Jest w takiej formie poniewaz zwracany jest tylko preferowany sposob dostawy
Jesli ma byc zwracana lista sposobow dostawy i ma to byc widoczne,
to nalezy zmodyfikowac rownie okno editlistywysdraportf.cpp
*/
  priorytet = 0;
  select first 1 ref, nazwa, :priorytet + 1
    from sposdost
    where listywys = 1
    order by nazwa
  into :ref, :nazwa, :priorytet;
end^
SET TERM ; ^
