--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSDROZ_GETPOZ(
      LISTWYSD integer,
      WERSJAREF integer,
      ILOSC numeric(14,4))
  returns (
      RPOZYCJA integer,
      RILOSC numeric(14,4),
      RSERIAL smallint)
   as
declare variable iloscmax numeric(14,4);
declare variable iloscspk numeric(14,4);
begin
  select sum(lwp.iloscmax), sum(coalesce(lwp.iloscspk, 0)), max(lwp.serial)
    from listywysdpoz lwp
    where lwp.dokument = :listwysd
      and lwp.wersjaref = :wersjaref
  into :iloscmax, :iloscspk, :rserial;

  if (:iloscmax is null or
      (:ilosc <= 0 or (:iloscmax - :iloscspk) < :ilosc)) then begin
    rpozycja = -1;
    if (:iloscmax is null) then rilosc = null;
    else if ((:iloscmax - :iloscspk) < :ilosc) then rilosc = (:iloscmax - :iloscspk - :ilosc);
    else rilosc = 1;
    rserial = 0;
    suspend;
  end
  else begin
    select first 1 lwp.ref, lwp.serial
      from listywysdpoz lwp
      where lwp.wersjaref = :wersjaref
        and lwp.iloscmax = :ilosc
        and coalesce(lwp.iloscspk, 0) = 0
        and lwp.dokument = :listwysd
    into :rpozycja, :rserial;

    if (:rpozycja is not null) then begin
      if (:rserial = 1) then rilosc = :ilosc;
      else rilosc = :ilosc;
      suspend;
    end
    else begin
      for select lwp.ref, lwp.serial, lwp.iloscmax, coalesce(lwp.iloscspk, 0)
            from listywysdpoz lwp
            where lwp.dokument = :listwysd
              and lwp.wersjaref = :wersjaref
              and lwp.iloscmax > coalesce(lwp.iloscspk, 0)
            order by coalesce(lwp.iloscspk, 0), lwp.iloscmax
          into :rpozycja, :rserial, :iloscmax, :iloscspk
      do begin
        if (:rserial = 1) then begin
          rilosc = :ilosc;
          suspend;
          exit;
        end
        else begin
          if (:ilosc > 0) then begin
            if (:ilosc >= (:iloscmax - :iloscspk)) then begin
              rilosc = :iloscmax - :iloscspk;
              ilosc = :ilosc - :rilosc;
            end
            else begin
              rilosc = :ilosc;
              ilosc = 0;
            end
            suspend;
          end
          else exit;
        end
      end
    end
  end
end^
SET TERM ; ^
