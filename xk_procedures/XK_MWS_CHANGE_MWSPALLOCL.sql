--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_CHANGE_MWSPALLOCL(
      MWSORD integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      WH varchar(3) CHARACTER SET UTF8                           ,
      MWSCARTCOLOR integer = null)
  returns (
      STATUS smallint,
      MWSCONSTLOC INTEGER_ID)
   as
declare variable mwspalloc integer;
declare variable mwsactref integer;
declare variable quantityc numeric(14,4);
declare variable quantityleft numeric(14,4);
declare variable newmwsactref integer;
declare variable maxnumber integer;
declare variable actstatus integer;
declare variable doctype varchar(1);
declare variable docid integer;
declare variable currentcart integer;
declare variable typ char_1; --[PM] XXX  W - wozek, K - kuweta
declare variable cartcolour integer_id; --[PM] XXX
declare variable currcartsymbol symbol_id; --[PM] XXX
declare variable mwsordtypesymb string20; -- XXX KBI           
declare variable x_docid integer_id;
begin
  mwsconstloc = null;
  status = 1;
  symbol = upper(symbol);

  --exception universal 'Kolor: '||coalesce(:mwscartcolor, '<null>');
  select s.mwsordtypes from mwsords s where s.ref = :mwsord into :mwsordtypesymb; -- XXX KBI

  if (coalesce(:symbol,'') <> '') then
  begin
    symbol = trim(symbol); --[PM] XXX start
      if (mwsordtypesymb <> 'WSC') then begin  -- KBI Wdrozenie magazyn celny, mask wózków i koszyków sprawdzamy zawsze dla magazynu MWS i dla magazynu CEL dla zleceń innychniż WSC
        if (symbol similar to 'W[[:DIGIT:]]{3}') then
          begin
            if (coalesce(mwscartcolor,0)<>0 and (select multi from mwsords where ref = :mwsord) = 1) then
              exception universal 'Chyba pomyliles wozek z koszykiem.';
            typ = 'W';
          end
        else if (symbol similar to 'K[[:DIGIT:]]{3}') then
          begin
            --exception universal 'Pobieranie koszykow zablokowane!'; --[PM] sprawzdzic jaki kolor ma bialy i dodac ponizej do wyjatku, ze jesli jest bialy to error
            if (coalesce(mwscartcolor,0) = 0) then
              exception universal 'Chyba powinienes wziac koszyk.';
            typ = 'K';
          end
        else
          exception universal 'Zeskanowany kod nie jest koszykiem ani wozkiem.'; --[PM] XXX koniec
    end

  /*  if (MWSCARTCOLOR is not null) then begin -- hh daje color ?, w x_mwscartcolor jest null i wywala blad ?
      select m.ref, m.mwsconstloc
        from mwspallocs m
        where m.symbol = :symbol
      into :mwspalloc, :mwsconstloc;
      cartcolour = MWSCARTCOLOR;
    end else     */
      select m.ref, m.mwsconstloc, m.x_mwscartcolour
        from mwspallocs m
        where m.symbol = :symbol
      into :mwspalloc, :mwsconstloc, :cartcolour;

    -- KBI Wdrozenie magazyn celny, na magazynie CEL dla zleceń WSC jakow wózek musimy podac symbol palety do przyjć
    -- (koniecznie ref mwspalloc musi byc zgodny z symbolem)
    if ( mwsordtypesymb = 'WSC') then begin
      if (mwspalloc is null) then exception universal 'Podany wózek nie jest kodem palety!';
      if (symbol <> cast(mwspalloc as string)) then exception universal 'Nieprawłowy symbol palety!';

      if (exists(select first 1 1 from mwsstock where mwspalloc = :mwspalloc)) then
        exception universal 'Podany symbol jest juz zajety!';
      if (exists(select first 1 1 from mwsacts where mwsconstloc = :symbol and mwsord <> :mwsord)) then
        exception universal 'Podany symbol jest juz zajety!';
    end

    -- XXX KBI Wdrożenia magazynu celnego  jesli jako wozek podczas skaladania WSC
    -- podamy palete wydrukowana do przyjecia wowaczas jeszcze nie mamy mwsconstloca
    /*if (mwsconstloc is null and mwspalloc is not null) then begin
      execute procedure gen_ref('MWSCONSTLOCS') returning_values mwsconstloc;
      insert into mwsconstlocs (ref, symbol, wh, mwsconstloctype, stocktaking, goodssellav, l, w, h, volume, act)
        values(:mwsconstloc, :symbol, :wh, null, 0, 0, 90, 130, 200, 2.34, 0);
    end */
    --XXX KBI koniec wdrożenia magazynu celnego


    if (typ = 'W') then --and color = bialy
      begin
        cartcolour = null;
      end
      
    if (mwspalloc is null) then
      begin
        execute procedure gen_ref('MWSCONSTLOCS') returning_values mwsconstloc;
        insert into mwsconstlocs (ref, symbol, wh, mwsconstloctype, stocktaking, goodssellav, l, w, h, volume, act)
          values(:mwsconstloc, :symbol, :wh, null, 0, 0, 90, 130, 200, 2.34, 0);
        execute procedure gen_ref('MWSPALLOCS') returning_values mwspalloc;
        insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status) --[PM] XXX
          select :mwspalloc, symbol, ref, 2, 0--,
              --case when :typ = 'K' then :mwscartcolor else null end --[PM] XXX
            from mwsconstlocs
            where ref = :mwsconstloc;
      end
    else  --[PM] XXX start
      begin
        if (cartcolour = 0) then
          cartcolour = null;
        if (typ = 'K') then
          begin
            if (cartcolour is null) then
              exception universal 'Blad podczas pobierania koloru koszyka.'||SYMBOL||'.';
            else if (cartcolour is distinct from mwscartcolor) then
              exception universal 'Niezgodnosc koloru zeskanowanej kuwety z kolorem w systemie.';
          end
        if (typ = 'W' and cartcolour is not null) then
          exception universal 'Pojawil sie kolor na wozku.';
      end --[PM] XXX koniec
  end
  for
    select ma.ref, ma.quantity - ma.quantityc, ma.quantityc, ma.status, ma.doctype
      from mwsacts ma
      where ma.mwsord = :mwsord and ma.status < 5 and ma.status > 0
        and ma.quantityc > 0
        and (ma.mwsconstlocl <> :mwsconstloc or ma.mwsconstlocl is null)
        and (ma.mwspallocl <> :mwspalloc or ma.mwspallocl is null)
        into :mwsactref, :quantityleft, :quantityc, :actstatus, :doctype
  do begin
    update mwsacts ma set ma.status = 0, ma.quantity = :quantityc where ma.ref = :mwsactref;
    update mwsacts ma set ma.status = 2, ma.quantityc = ma.quantity where ma.ref = :mwsactref;
    if (quantityleft > 0) then begin
      execute procedure gen_ref('MWSACTS') returning_values newmwsactref;
      select max(ma.number)
        from mwsacts ma
        where ma.mwsord = :mwsord
        into maxnumber;
      maxnumber = maxnumber + 1;
      insert into mwsacts (ref, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
             mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc,
             wh, wharea, whsec, timestart, timestop,
             regtime, timestartdecl, timestopdecl, mwsaccessory, realtimedecl,
             flags, descript, priority, lot, recdoc, plus,
             whareal, whareap, wharealogl, wharealogp, number, disttogo, palgroup, autogen,
             x_partia, x_serial_no, x_slownik) -- XXX KBI)
         select :newmwsactref, ma.mwsord, 0, 0, ma.good, ma.vers, :quantityleft, ma.mwsconstlocp, ma.mwspallocp,
               mwsconstlocl, mwspallocl, ma.docid, ma.docposid, :doctype, ma.settlmode, ma.closepalloc,
               ma.wh, ma.wharea,  ma.whsec, current_timestamp, current_timestamp,
               current_timestamp, ma.timestartdecl, ma.timestopdecl, ma.mwsaccessory, ma.realtimedecl,
               ma.flags, ma.descript, ma.priority, ma.lot, 0, 1,
               null, ma.whareap, null, ma.wharealogp, number + 1, ma.disttogo, ma.palgroup, ma.autogen,
               ma.x_partia, ma.x_serial_no, ma.x_slownik -- XXX KBI
           from mwsacts ma where ma.ref = :mwsactref;
      update mwsacts ma set ma.status = :actstatus where ma.ref = :newmwsactref;
    end
  end
  if (mwsconstloc is not null) then
  begin
    select first 1 a.docid from mwsacts a
      where a.mwsord = :mwsord
        and a.mwsconstloc is null
        and a.status < 3
      group by a.docid
    into :docid;
--XXX JO: Zmiana do przeniesienia na worka
    execute procedure xk_mws_check_mwsordcart(:mwsconstloc, :symbol, :mwsord, :docid, 0)
      returning_values :currentcart, :status;
--    execute procedure xk_mws_check_xwozek(:mwsconstloc, :mwsord, :docid)
--      returning_values :status;

    if (:status > 0) then
    begin
      if (coalesce(currentcart,0) > 0 and coalesce(currentcart,0) <> coalesce(mwsconstloc,0)) then --[PM] XXX start
        begin
          select symbol from mwsconstlocs where ref = :currentcart into :currcartsymbol;
          exception universal 'Zlecenie jest przypisane do koszyka: '||coalesce(currcartsymbol,'<null>');
        end  --[PM] XXX koniec
        
      for
        select a_d.ref, a_d.docid       --xxx MatJ updateowanie koszykow na ta sama grupe spedycyjna a nie tylko na dokument
          from mwsacts a
            join mwsacts a_d on    (a.docgroup=a_d.docgroup
                                and a.mwsord = a_d.mwsord
                                and a_d.status < 3
                                and ((a_d.mwsconstloc is null) or
                                     (a_d.mwsconstloc is not null and a_d.mwsconstloc <> :mwsconstloc)))
          where a.mwsord = :mwsord and a.status < 3
            and (a.mwsconstloc is null or
              (a.mwsconstloc is not null and a.mwsconstloc <> :mwsconstloc))
            and a.docid = :docid
        into :mwsactref, :x_docid
      do begin
        update or insert into mwsordcartcolours(mwsord, cart, docid, mwscartcolor, status)
          values(:mwsord, :mwsconstloc, :x_docid, :mwscartcolor, 0)
        matching (mwsord, cart, docid);
        update mwsacts ma set ma.mwsconstloc = :mwsconstloc where ma.ref = :mwsactref;
      end
    end
  end
  suspend;
end^
SET TERM ; ^
