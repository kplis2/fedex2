--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_OPER_LOGOUT(
      HISTORYREF integer,
      DISTANCE integer)
   as
declare variable accessory integer;
begin
  select m.mwsaccessory
    from mwsaccessoryhist m
    where m.ref = :historyref
    into accessory;
  update mwsaccessoryhist m set m.timestartto = current_timestamp(0), m.distanceto = :distance
    where m.ref = :historyref and m.timestartto is null;
  update mwsaccessories m set m.distance = :distance where m.ref = :accessory;
end^
SET TERM ; ^
