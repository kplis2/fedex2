--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_POW_PERIODIC_REFILL_VERS(
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      REFMWSACT integer,
      REFMWSORD integer,
      MWSORDTYPE integer,
      WH varchar(3) CHARACTER SET UTF8                           ,
      WHSEC integer,
      WHAREA integer,
      MAXLEVEL smallint,
      PRIORITY smallint,
      PAL smallint,
      MIX smallint,
      MWSCONSTLOCB integer,
      PALTYPE varchar(40) CHARACTER SET UTF8                           ,
      PALW numeric(14,2),
      PALL numeric(14,2),
      PALH numeric(14,2),
      REFILLTRY smallint,
      QUANTITY numeric(14,4),
      MODE smallint)
  returns (
      STOP smallint,
      REFMWSORDOUT integer,
      QUANTITYLEFT numeric(14,4))
   as
declare variable mwsconstlocp integer;
declare variable mwspallocp integer;
declare variable whareap integer;
declare variable wharealogp integer;
declare variable refillp smallint;
declare variable mwsrefillmode smallint;
declare variable mwsrefillmodes varchar(100);
declare variable mwsconstlocl integer;
declare variable mwspallocl integer;
declare variable whareal integer;
declare variable wharealogl integer;
declare variable refill smallint;
declare variable mwsordtypedest smallint;
declare variable quantityrefilled numeric(14,4);
declare variable quantityonlocation numeric(14,4);
declare variable quantityrest numeric(14,4);
declare variable qrefill numeric(14,4);
declare variable maxnumber integer;
declare variable mwspalloc integer;
declare variable lot integer;
declare variable autocommit smallint;
begin
  if (mode = 0) then
    autocommit = -1;
  else if (mode = 1) then
    autocommit = 0;
  else autocommit = 1;
  -- znajdujemy lokacje poczatkowa
  execute procedure XK_MWS_GET_BEST_SOURCE(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refillp);
  if (mwsconstlocp is not null) then
  begin
    -- okreslamy ilosc na lokacji poczatkowej
    select sum(s.quantity)
      from mwsstock s
      where s.mwsconstloc = :mwsconstlocp and s.vers = :vers
      into quantityonlocation;
    if (quantityonlocation is null) then quantityonlocation = null;
    refmwsordout = null;
    quantityrefilled = 0;
    stop = 0;
    while (quantityonlocation > 0 and quantity - quantityrefilled > 0 and stop = 0)
    do begin
      -- okreslamy lokacje koncowa
      execute procedure XK_MWS_GET_BEST_LOCATION(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
          returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
      -- okreslamy ilosc ktora mozna wstawic na lokacje koncowa
      execute procedure xk_mws_loc_capacity(:mwsconstlocl, :vers, :quantity - :quantityrefilled, :quantityonlocation)
        returning_values (:qrefill);
      if (qrefill is null) then qrefill = 0;
      if (qrefill > :quantity - :quantityrefilled) then
        qrefill = quantity - quantityrefilled;
      if (qrefill > quantityonlocation) then
        qrefill = quantityonlocation;
      if (qrefill > 0) then
      begin
        -- przesuwamy
        execute procedure MWS_MWSCONSTLOC_CHANGE_PART(:refmwsordout,:mwsordtype,:vers,null,:qrefill,null,:mwsconstlocp,null,:mwsconstlocl,:autocommit,null,0,'',:priority)
            returning_values refmwsordout;
        if (refmwsordout is not null) then
        begin
          update mwsords set slokod = substring('XK_MWS_POW_PERIODIC_REFILL_VERS - A' from 1 for 40)
            where ref = :refmwsordout;
          -- wyliczamy ile uzupelniono juz w sumie
          select sum(a.quantity)
            from mwsacts a
            where a.mwsord = :refmwsordout and a.vers = :vers and a.status > 0 and a.status < 6
            into quantityrefilled;
          if (quantityrefilled is null) then quantityrefilled = 0;
        end
        quantityonlocation = quantityonlocation - qrefill;
      end
      else
        stop = 1;
    end
    quantityleft = quantity - quantityrefilled;
    -- reszte odkladamy na to samo miejsce - potrzebne, zeby zablokowac towar
    if (quantityrefilled > 0) then
    begin
      select max(number) from mwsacts where mwsord = :refmwsordout
        into maxnumber;
      for
        select first 1 s.quantity - s.blocked, s.mwspalloc, s.lot, s.good, s.vers
          from mwsstock s
          where s.mwsconstloc = :mwsconstlocp and s.quantity - s.blocked > 0
          order by s.mwspalloc
          into quantityrest, mwspalloc, lot, good, vers
      do begin
        maxnumber = maxnumber + 1;
        insert into mwsacts (ref,mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
            mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
            regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, recdoc, plus,
            whareap, whareal, wharealogp, wharealogl, number, disttogo, mixedpallgroup, palgroup)
          values (null,:refmwsordout, 1, 0, :good, :vers, :quantityrest, :mwsconstlocp,  :mwspalloc,
              :mwsconstlocp, :mwspalloc, null, null, 'O', 0, 0, :wh, null, null,
              current_timestamp(0), null, null, null, null, null, 1, 0, 1,
              null, null, null, null, :maxnumber, null, null, null);
      end
    end
  end else
    stop = 1;
end^
SET TERM ; ^
