--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GEN_MWSORD_COR_IN(
      DOCID integer,
      DOCGROUP integer,
      DOCPOSID integer,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      MWSORD integer,
      MWSACT integer,
      RECALC smallint,
      MWSORDTYPE integer)
  returns (
      REFMWSORD integer)
   as
declare variable REC smallint;
declare variable WH varchar(3);
declare variable STOCKTAKING smallint;
declare variable OPERATOR integer;
declare variable PRIORITY smallint;
declare variable BRANCH varchar(10);
declare variable PERIOD varchar(6);
declare variable FLAGS varchar(40);
declare variable SYMBOL varchar(20);
declare variable COR smallint;
declare variable TOINSERT numeric(14,4);
declare variable GOOD varchar(20);
declare variable VERS integer;
declare variable OPERSETTLMODE smallint;
declare variable MWSCONSTLOCP integer;
declare variable MWSPALLOCP integer;
declare variable AUTOLOCCREATE smallint;
declare variable WHSEC integer;
declare variable WHAREA integer;
declare variable POSFLAGS varchar(40);
declare variable DESCRIPTION varchar(1024);
declare variable POSDESCRIPTION varchar(255);
declare variable LOT integer;
declare variable SHIPPINGAREA varchar(3);
declare variable WHAREALOGP integer;
declare variable MWSACCESSORY integer;
declare variable DIFFICULTY varchar(10);
declare variable MAXNUMBER integer;
declare variable WHAREALOGB integer;
declare variable WHAREALOGL integer;
declare variable CNT integer;
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable SLOKOD varchar(40);
declare variable MANMWSACTS smallint;
declare variable KORDOC integer;
declare variable KORIL numeric(14,4);
declare variable KORPOZ integer;
declare variable GQUANTITY numeric(14,4);
declare variable MWSCONSTLOCBACK integer;
declare variable MWSPALLOCBACK integer;
declare variable NEWMWSACT integer;
declare variable DESCRIPT varchar(255);
declare variable COMMITALL smallint;
declare variable FLAGINAG varchar(40);
begin
  if (recalc is null) then recalc = 0;
  -- NAGLÓWEK ZLECENIA MAGAZYNOWEGO
  mwsordtype = 29;
  if (docgroup is null) then docgroup = docid;
  select stocktaking, opersettlmode, autoloccreate
    from mwsordtypes
    where ref = :mwsordtype
    into stocktaking, opersettlmode, autoloccreate;
  select defdokum.wydania, defdokum.koryg, dokumnag.magazyn, dokumnag.operator,
      dokumnag.uwagisped, dokumnag.katmag, dokumnag.spedpilne, dokumnag.oddzial,
      dokumnag.okres, dokumnag.flagi, dokumnag.symbol, dokumnag.kodsped,
      dokumnag.wharealogb, 6, dokumnag.ref, substring(dostawcy.id from 1 for 40),
      dokumnag.refk
    from dokumnag
      left join dostawcy on (dokumnag.dostawa = dostawcy.ref)
      left join defdokum on (defdokum.symbol = dokumnag.typ)
    where dokumnag.ref = :docid
    into rec, cor, wh, operator,
      description, difficulty, priority, branch,
      period, flags, symbol, shippingarea,
      wharealogb, slodef, slopoz, slokod,
      kordoc;
  if (rec = 0) then rec = 1; else rec = 0;
  if (manmwsacts is null) then manmwsacts = 0;
  manmwsacts = 0;
  operator = 96;
  if (not exists (select first 1 o.ref from mwsords o where o.docid = :kordoc and o.status = 5) and :kordoc is not null) then
    exit;
  execute procedure gen_ref('MWSORDS') returning_values refmwsord;
  select okres from datatookres(current_date,0,0,0,0) into period;
  insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator,
      priority, description, wh, difficulty, regtime, timestartdecl, timestopdecl,
      mwsaccessory, branch, period, flags, docsymbs, cor, rec, bwharea, ewharea,
      status, shippingarea, slodef, slopoz, slokod)
    values (:refmwsord, :mwsordtype, :stocktaking, 'M', :docgroup, :docid, :operator,
        :priority, :description, :wh, :difficulty, current_timestamp(0), null, current_timestamp(0),
        :mwsaccessory, :branch, :period, :flags, :symbol, :cor, :rec, :wharealogb, :wharealogb,
        0, :shippingarea, :slodef, :slopoz, :slokod);
  if (refmwsord is null) then exception universal 'Brak zlecenia magazynowego';
  -- mozliwe sytuacje
  mwsconstlocback = null;
  mwspallocback = null;
  commitall = 0;
  mwsconstlocback = 7570;
  for
    select p.ref, p.iloscl - p.ilosconmwsacts, p.kortopoz,
        p.ktm, p.wersjaref, p.flagi, p.uwagi, n.flagi
      from dokumpoz p
        join dokumnag n on(p.dokument = n.ref)
      where p.dokument = :docid
      into docposid, koril, korpoz,
        good, vers, posflags, posdescription, flaginag
  do begin
    if (koril > 0) then
    begin
      for
        select m.mwspalloc, m.quantity - m.blocked, m.lot
          from mwsstock m
          where m.mwsconstloc = :mwsconstlocback and m.vers = :vers and m.quantity - m.blocked > 0
          into mwspallocp, gquantity, lot
      do begin
        if (gquantity >= koril) then
          toinsert = koril;
        else if (gquantity < koril) then
          toinsert = gquantity;
        else
          toinsert = 0;
        if (toinsert > 0) then
        begin
          select max(number)
            from mwsacts
            where mwsord = :refmwsord
            into maxnumber;
          if (maxnumber is null) then maxnumber = 0;
          maxnumber = maxnumber + 1;
          execute procedure gen_ref('MWSACTS') returning_values newmwsact;
          insert into mwsacts (ref, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
              mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
              regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc, plus,
              whareap, whareal, wharealogp, wharealogl, number, disttogo, frommwsact, stopnextmwsacts, cortomwsact)
            values(:newmwsact, :refmwsord, 0, 0, :good, :vers, :toinsert, :mwsconstlocback, :mwspallocp,
                null, null, :docid, :docposid, 'M', 0, 0, :wh, :wharea, :whsec,
                current_timestamp(0), current_timestamp(0), current_timestamp(0), null, :flaginag, :posdescription, 0, :lot, 0, 1,
                null, :wharea, :wharealogp, :wharealogl, :maxnumber, 0, null, 0, null);
          cnt = 0;
          select count(ref) from mwsacts where mwsord = :refmwsord
            into cnt;
          update mwsacts set status = 1 where ref = :newmwsact;
          koril = koril - toinsert;
        end
        if (koril <= 0) then
          break;
      end
    end
    if (koril > 0) then
      exception universal 'Brak stanów na lokacji do zwrotu do dostawcy dla KTM ' || :good;
  end
  update mwsacts set status = 1 where mwsord = :refmwsord and status = 0;
  if (not exists (select ref from mwsacts where mwsord = :refmwsord)) then
    delete from mwsords where ref = :refmwsord;
  else
    update mwsords set status = 1 where ref = :refmwsord;
  suspend;
end^
SET TERM ; ^
