--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSD_PACKING_PATH_UPD(
      BOX integer,
      SPED_DOC integer)
   as
declare variable box_upd integer; /* Pudelko, dla ktorego dokonac update'u */
declare variable ref_upd integer; /* REF wpisu dla towaru, ktory bedzie update'owany */
declare variable box_prev integer; /* REF poprzednio update'owanego opakowania */
declare variable current_path varchar(255); /* Aktualna, wygenerowana sciezka */
begin
    current_path = null;
    for
        select lw.ref, opk.ref from listywysdroz lw
        left join listywysdroz_opk opk on (opk.listwysd = lw.listywysd and opk.nropk = lw.nrkartonu)
        where lw.listywysd = :sped_doc and lw.opakowania LIKE '%;'||:box||';%'
        order by opk.ref
        into :ref_upd,:box_upd
    do begin

        if (box_prev is null or box_prev != box_upd) then  -- optymalizacja
        begin
           select path from xk_listywysd_packing_path(:box_upd)
           into :current_path;
           box_prev = box_upd;
        end

        update listywysdroz lw set lw.opakowania = :current_path
        where lw.ref = :ref_upd;
    end
end^
SET TERM ; ^
