--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_CENAZAKUPU(
      DOKREF integer,
      DOKTYP char(3) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      ILOSC numeric(14,4),
      DOSTAWA integer,
      PARAMD1 timestamp,
      PARAMD2 timestamp,
      PARAMD3 timestamp,
      PARAMD4 timestamp,
      PARAMN1 numeric(14,2),
      PARAMN2 numeric(14,2),
      PARAMN3 numeric(14,2),
      PARAMN4 numeric(14,2),
      PARAMS1 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS2 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS3 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS4 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS5 varchar(255) CHARACTER SET UTF8                            = null,
      PARAMS6 varchar(255) CHARACTER SET UTF8                            = null,
      PARAMS7 varchar(255) CHARACTER SET UTF8                            = null,
      PARAMS8 varchar(255) CHARACTER SET UTF8                            = null,
      PARAMS9 varchar(255) CHARACTER SET UTF8                            = null,
      PARAMS10 varchar(255) CHARACTER SET UTF8                            = null,
      PARAMS11 varchar(255) CHARACTER SET UTF8                            = null,
      PARAMS12 varchar(255) CHARACTER SET UTF8                            = null,
      PARAMN5 numeric(14,2) = null,
      PARAMN6 numeric(14,2) = null,
      PARAMN7 numeric(14,2) = null,
      PARAMN8 numeric(14,2) = null,
      PARAMN9 numeric(14,2) = null,
      PARAMN10 numeric(14,2) = null,
      PARAMN11 numeric(14,2) = null,
      PARAMN12 numeric(14,2) = null)
  returns (
      STATUS integer,
      CENA numeric(14,2),
      RABAT numeric(14,2))
   as
declare variable KTM varchar(80);
declare variable SUMAPRO numeric(14,2);
declare variable PRO numeric(14,2);
declare variable I integer;
declare variable SYMBPARAM varchar(20);
declare variable VAL numeric(14,2);
declare variable WARTDOM numeric(14,2);
declare variable MAGAZYN varchar(10);
declare variable REFCENNIKZAK integer;
declare variable DEBUG_LOG varchar(1024);
begin
  STATUS = 0;
  CENA = 0;
  RABAT = 0;
  sumapro = 0;
  i = 1;
  select wersje.ktm from WERSJE where REF=:wersjaref into :ktm;
  if(:doktyp='M') then select MAGAZYN from DOKUMNAG where ref=:dokref into :magazyn;
  if(:doktyp='F') then select MAGAZYN from NAGFAK where ref=:dokref into :magazyn;
  select min(ref) from defcennikzak 
    where KTM=:ktm and MAGAZYNY like '%'||:magazyn||';%'
    into :refcennikzak;
  if(:refcennikzak is null) then exit;
  select cena_net from defcennikzak where ref=:refcennikzak into :cena;

--  debug_log = :dokref||' '||:doktyp||' '||wersjaref||' '||:paramn1||' '||:paramn2||'
--  ';

  while (i < 13) do
   begin
     pro = 0;
     wartdom = 0;
     val = case
           when i = 1 then :paramn1
           when i = 2 then :paramn2
           when i = 3 then :paramn3
           when i = 4 then :paramn4
           when i = 5 then :paramn5
           when i = 6 then :paramn6
           when i = 7 then :paramn7
           when i = 8 then :paramn8
           when i = 9 then :paramn9
           when i = 10 then :paramn10
           when i = 11 then :paramn11
           when i = 12 then :paramn12
           else Null end;
     symbparam = 'PARAMN'||cast (i as varchar(2));

--   debug_log = :debug_log||:refcennikzak||' '||:KTM||' '||:symbparam||' '||:val||'
--  ';

     execute procedure CENAZAKUPU_procent(:refcennikzak, :symbparam, :val )
       returning_values :pro;

     sumapro =  :sumapro + :pro;

--   debug_log = :debug_log||:sumapro||' '||:pro||' '||:wartdom||'
--  ';

     i = i + 1;
   end
  CENA = CENA  *  (100 - sumapro) / 100;
  STATUS = 1;

-- exception test_break debug_log;
end^
SET TERM ; ^
