--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_BEST_LOCATION_3(
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      REFMWSACT integer,
      REFMWSORD integer,
      MWSORDTYPE integer,
      WH varchar(3) CHARACTER SET UTF8                           ,
      WHSEC integer,
      WHAREA integer,
      MAXLEVEL smallint,
      PRIORITY smallint,
      PAL smallint,
      MIX smallint,
      MWSCONSTLOCB integer,
      PALTYPE varchar(40) CHARACTER SET UTF8                           ,
      PALW numeric(14,2),
      PALL numeric(14,2),
      PALH numeric(14,2),
      REFILLTRY smallint,
      QUANTITY numeric(14,4),
      X_PARTIASTR STRING20 = '')
  returns (
      MWSCONSTLOCL integer,
      MWSPALLOCLL integer,
      WHAREAL integer,
      WHAREALOGL integer,
      REFILL smallint)
   as
declare variable wcategory varchar(10);
declare variable abc varchar(10);
declare variable typconstloc smallint_id;
declare variable lot dostawa_id; 
declare variable flowgroup integer_id;
declare variable loccount smallint_id;
declare variable x_serie smallint_id;
declare variable wg_partii smallint_id;
declare variable x_slowniki smallint_id;
declare variable lottmp dostawa_id;
declare variable quantitytmp numeric(14,4);
declare variable x_partia date_id;
declare variable x_partiatmp date_id;

begin
  refill = 0;
  -- Pobiermy wyliczoną (o lile jest wyliczna) kategori wagowa i rotacyjnoci ABC dla towaru
  select w.good, w.wcategory, w.optabc
    from xk_goods_relocate w
    where w.vers = :vers and w.wh = :wh
    into good, wcategory, abc;
  if (wcategory is null) then wcategory = 'A';
  if (abc is null) then abc = 'A';

  -- Parsujey partie na date
  if (coalesce(x_partiastr,'') <> '') then
    x_partia = cast(x_partiastr as date_id);

  -- pobieramy informacje o wersji, jaki typ lokacji jest prefoerowany, czy zapisujemy serie itp
  select w.ktm, w.x_mws_typ_lokacji, w.x_mws_serie, w.x_mws_partie, w.x_mws_slownik_ehrle
    from wersje w
    where w.ref = :vers
  into :good, :typconstloc, :x_serie, :wg_partii, :x_slowniki;

  -- jesli mamy podany mwsact to pobieramy informacje o dostawie, ilosci i parti (o ile  nie byly podane w parametrach wejsciowych)
  if (coalesce(refmwsact,0) <> 0) then begin
    select a.lot, a.quantity, a.x_partia
      from mwsacts a
      where a.ref=:refmwsact
      into :lottmp, :quantitytmp, x_partiatmp;
    if (coalesce(lot,0) = 0 and lottmp is not null) then
      lot = lottmp;

    if (coalesce(quantity,0) = 0 and quantitytmp is not null ) then
      quantity = quantitytmp;

    if (x_partia is null and x_partiatmp is not null) then
      x_partia = x_partiatmp;
  end


  -- wpakowanie towaru na glebe
  if (maxlevel = -1) then
  begin
    select first 1 c.ref
      from mwsconstlocs c
        left join mwsstock s on (s.mwsconstloc = c.ref)
      where c.wh = :wh and c.locdest = 5 and s.ref is null
      into mwsconstlocl;
    if (mwsconstlocl is null) then
      select first 1 c.ref
        from mwsconstlocs c
        where c.wh = :wh and c.locdest = 9
        into mwsconstlocl;
  end

  -- weryfikujemy towary  przeznaczeniem na regaly przeplywowy
  --  mwsordtype = 24 to MWG, rozwieznie po realizaji WG. Dla tego zlecenia powinnimy szukac lokacj poborowch (cay rega przepywowy jest buforowy)
  if (typconstloc = 1 and coalesce(mwsordtype,0) <> 24) then begin
    --- szukamy lokacji w ramach czesciowo  zajetej grupy przeplywowej
    select first 1 c.ref, c.x_flow_group--, count(distinct c.ref)
    --min(substring(c.symbol from 1 for 4)||'5'||substring(c.symbol from 6 for 6)) , c.x_flow_group, count(distinct c.ref)
      from mwsconstlocs c
        join mwsstock s on (c.ref=s.mwsconstloc)
          left join X_WMS_CHECK_LOT_AND_VERS_4_FLOW(c.ref,:vers,:x_partia) x on (1=1)     --LDz lot -> X_partia
        where c.x_row_type=1
          and c.mwsstandlevelnumber > 1 -- XXX KBI
          and x.status =1
          and s.vers=:vers
        group by c.ref,  c.x_flow_group
        having count(distinct c.ref)<5
        order by count(distinct c.ref) desc
    into :mwsconstlocl, :flowgroup; --, :loccount;
    if (mwsconstlocl is not null) then begin
      -- pobiermy ref lokacji wejsciowej do regalow przeprzywowych (w ehrle jest to regal nr 5)
      select first 1 c.ref
        from mwsconstlocs c
        where c.x_flow_group = :flowgroup
        order by c.x_flow_order desc
      into :mwsconstlocl;
      exit;
    end

    if (mwsconstlocl is null) then
    begin
      -- szukamy pustej grupy przeplywowej
      select first 1 c.ref,  c.x_flow_group
      --min(substring(c.symbol from 1 for 4)||'5'||substring(c.symbol from 6 for 6)) , c.x_flow_group, count(distinct s.mwsconstloc)
        from mwsconstlocs c
          left join mwsstock s on (c.ref=s.mwsconstloc)
        where c.x_row_type=1
          and c.act = 1
          and c.goodssellav = 1
          and s.ref is null
          and c.mwsstandlevelnumber > 1
        group by c.ref,  c.x_flow_group, c.mwsstandlevelnumber, c.mwsstandnumber
        having count(distinct s.ref)=0
        order by c.mwsstandlevelnumber, c.mwsstandnumber
      into :mwsconstlocl, :flowgroup;  --, :loccount;
      if (mwsconstlocl is not null) then begin
      -- pobiermy ref lokacji wejsciowej do regalow przeprzywowych (w ehrle jest to regal nr 5)
        select first 1 c.ref
          from mwsconstlocs c
          where c.x_flow_group = :flowgroup
          order by c.x_flow_order desc
        into :mwsconstlocl;
        exit;
      end
    end
  end else if (typconstloc = 2) then begin  -- teraz sprawddzamy towary z przeznaczeniem na regaly typu Kradex

    select first 1 c.ref        --dolozyc do istniejacych stanow jesli jest miejsce
      from mwsconstlocs c
        join mwsstock s on (c.ref=s.mwsconstloc)
        left join X_WMS_CHECK_WEIGHT_4_KARDEX(c.ref,:vers,:quantity) x on (1=1)
      where c.x_row_type=2
        and c.act = 1
        and c.goodssellav = 1
        and x.status =1
        and s.good=:vers
      order by s.quantity desc
    into :mwsconstlocl;

    if (mwsconstlocl is not null) then
      exit;

    if (mwsconstlocl is null) then
    begin
      select first 1 c.ref           --dolozyc do pustych polek nawet jak sie nie zmiesci
        from mwsconstlocs c
          left join mwsstock s on (c.ref=s.mwsconstloc)
        where c.x_row_type=2
          and c.act = 1
          and c.goodssellav = 1
          and s.ref is null
      into :mwsconstlocl;

    if (mwsconstlocl is not null) then
      exit;
    end

    if (mwsconstlocl is null) then       --dolozyc do polek z innym towerem jesli sie zmiesci
    begin
      select first 1 c.ref
        from mwsconstlocs c
          join mwsstock s on (c.ref=s.mwsconstloc)
          left join X_WMS_CHECK_WEIGHT_4_KARDEX(c.ref,:vers,:quantity) x on (1=1)
        where c.x_row_type=2
          and c.act = 1
          and c.goodssellav = 1
          and x.status =1
          and s.good <> :good
        order by s.quantity desc
        into :mwsconstlocl;

    if (mwsconstlocl is not null) then
      exit;
    end

    if (mwsconstlocl is null) then   --dolozyc do polek z tym towerem nawet jesli sie zmiesci
    begin
      select first 1 c.ref
        from mwsconstlocs c
          join mwsstock s on (c.ref=s.mwsconstloc)
        where c.x_row_type=2
          and c.act = 1
          and c.goodssellav = 1
          and s.vers = :vers
        order by s.quantity desc
        into :mwsconstlocl;
      if (mwsconstlocl is not null) then
        exit;
    end

    if (mwsconstlocl is null) then  --dolozyc do polek z innym towerem nawet jesli sie zmiesci
    begin
      select first 1 c.symbol
        from mwsconstlocs c
          join mwsstock s on (c.ref=s.mwsconstloc)
        where c.x_row_type=2
          and c.act = 1
          and c.goodssellav = 1
          and s.vers <> :vers
        order by s.quantity desc
        into :mwsconstlocl;
      if (mwsconstlocl is not null) then
        exit;
    end
  end

  -- teraz pozostale towary na regal rzedowy  lub toway z przenaczeniem na regaly przeplywowe lub kardex
  -- jesli nie udalo sie znalesc lokalcji w regalach przplywowych lub w kardexie wyrzej
  if ((mwsconstlocl is null)) then  begin
    -- procedura szukajaca lokacji koncowej dla dostawienia towarow
    if (refilltry = 1 and mwsconstlocl is null) then
    begin
      -- sprawdzamy czy mozna dolozyc towar do lokacji z tym samym towarem
      select first 1 x.mwsconstloc from XK_MWS_GET_BEST_LOCATION_3_DET(:wh,:vers,:quantity,0, :wg_partii, :x_partia) x
        where 0.9 * x.locvolume - x.volume > 0
          and (x.mwsconstloc <> :mwsconstlocb or :mwsconstlocb is null)
        order by x.locvolume - x.volume desc
        into mwsconstlocl;
      if (mwsconstlocl is not null) then
      begin
        refill = 1;
        exit;
      end
    end
    -- sprawdzamy czy mozna dolozyc towar w miejsce na stale przypisane do towaru
    if (mwsconstlocl is null) then
    begin
      select first 1 f.ref
        from mwsconstlocsymbs s
          left join mwsconstlocs c on (c.ref = s.mwsconstloc)
          left join mwsfreemwsconstlocs f on (f.ref = c.ref)
        where c.goodsav in (1,2) and f.ref is not null and s.vers = :vers and s.const = 1
          and (c.ref <> :mwsconstlocb or :mwsconstlocb is null) and c.act > 0 and c.locdest = 1
        into mwsconstlocl;
      if (mwsconstlocl is not null) then
        exit;
    end
    -- sprawdzamy czy da sie towar polozyc na wlasciwym, pustym miejscu dla grupy ABC i kategorii wagowej
    if (mwsconstlocl is null) then
    begin
      select first 1 f.ref
        from mwsfreemwsconstlocs f
          left join mwsconstlocs c on (c.ref = f.ref)
          left join mwsconstlocsymbs s on (s.mwsconstloc = c.ref and s.vers <> :vers and s.const = 1)
        where c.wcategory = :wcategory and c.goodsav in (1,2) and s.mwsconstloc is null
          and (c.ref <> :mwsconstlocb or :mwsconstlocb is null) and c.act > 0 and c.locdest = 1
        -- c.abc = :abc and c.mwsstandlevelnumber > 0 -- XXX KBI
        into mwsconstlocl;
      if (mwsconstlocl is not null) then
        exit;
    end
    -- sprawdzmy czy da sie polozyc towar na niewlasciwym, pustym miejscu ale tej samej kategorii wagowej
    if (mwsconstlocl is null) then
    begin
      select first 1 f.ref
        from mwsfreemwsconstlocs f
          left join mwsconstlocs c on (c.ref = f.ref)
          left join mwsconstlocsymbs s on (s.mwsconstloc = c.ref and s.vers <> :vers and s.const = 1)
        where c.abc <> :abc and c.wcategory = :wcategory and c.goodsav in (1,2) and s.mwsconstloc is null
          and (c.ref <> :mwsconstlocb or :mwsconstlocb is null) and c.act > 0 and c.locdest = 1
          and c.x_row_type = 0 --- XXX KBI
          and c.mwsstandlevelnumber > 0 -- XXX KBI
        into mwsconstlocl;
      if (mwsconstlocl is not null) then
        exit;
    end
    -- sprawdzamy czy da sie polozyc towar na niewlasciwym miejscu i innej kategorii wagowej
    if (mwsconstlocl is null) then
    begin
      select first 1 f.ref
        from mwsfreemwsconstlocs f
          left join mwsconstlocs c on (c.ref = f.ref)
          left join mwsconstlocsymbs s on (s.mwsconstloc = c.ref and s.vers <> :vers and s.const = 1)
        where c.abc <> :abc and c.wcategory <> :wcategory and c.goodsav in (1,2) and s.mwsconstloc is null
          and (c.ref <> :mwsconstlocb or :mwsconstlocb is null) and c.act > 0 and c.locdest = 1
          and c.x_row_type = 0 --- XXX KBI
          and c.mwsstandlevelnumber > 0 -- XXX KBI
        into mwsconstlocl;
      if (mwsconstlocl is not null) then
        exit;
    end
    -- sprawdzamy czy mozna dolozyc do innej lokacji w ramach ABC i kategorii wagowej, z innym towarem
    if (refilltry = 1 and mwsconstlocl is null) then
    begin
      select first 1 x.mwsconstloc
        from XK_MWS_GET_BEST_LOCATION_3_DET(:wh,:vers,:quantity,1,  :wg_partii, :x_partia) x
          join mwsconstlocs c on (c.ref = x.mwsconstloc)
        where 0.9 * x.locvolume - x.volume > 0
          and (x.mwsconstloc <> :mwsconstlocb or :mwsconstlocb is null)
          and c.x_row_type = 0 --- XXX KBI
          and c.mwsstandlevelnumber > 0 -- XXX KBI
        order by x.locvolume - x.volume desc
        into mwsconstlocl;
      if (mwsconstlocl is not null) then
      begin
        refill = 1;
        exit;
      end
    end
  end
end^
SET TERM ; ^
