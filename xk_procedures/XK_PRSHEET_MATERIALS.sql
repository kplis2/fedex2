--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_PRSHEET_MATERIALS(
      PRSHEET integer,
      TREESUBSHEET smallint,
      QUANTITYIN numeric(14,4),
      DRILLIN smallint,
      PROPERIN integer,
      KQUANTITYIN numeric(14,4))
  returns (
      KTMOUT varchar(40) CHARACTER SET UTF8                           ,
      VERSOUT integer,
      NAMEOUT varchar(255) CHARACTER SET UTF8                           ,
      QUANTITYOUT numeric(14,4),
      KQUANTITYOUT numeric(14,4),
      KPARTQUANTITYOUT numeric(14,4),
      SHORTAGESOUT numeric(14,4),
      REFOUT integer,
      SHEETOUT integer,
      OPERMASTEROUT integer,
      NUMBEROUT integer,
      NUMBERALLOUT integer,
      PROPEROUT varchar(40) CHARACTER SET UTF8                           ,
      STOPCASCADEOUT smallint,
      JEDNOUT integer,
      WHOUT varchar(3) CHARACTER SET UTF8                           ,
      KPARTLCALCOUT numeric(14,4),
      MATTYPE smallint,
      PRODTRANSFER smallint)
   as
declare variable numberin integer;
declare variable ilcalc numeric(14,4);
declare variable ratio numeric(14,4);
declare variable jmain smallint;
begin
  if (kquantityin is null or kquantityin = 0) then kquantityin = 1;
  if (drillin is null) then drillin = 0;
  if (treesubsheet is null) then treesubsheet = 0;
  /* zwroc wszystkie materialy do karty lacznie z rozwinieciem podrzednych*/
  for select m.REF,m.ktm,m.wersjaref,coalesce(m.OPERMASTER,0),m.NUMBER,m.DESCRIPT,
      m.SUBSHEET, coalesce(m.STOPCASCADE,0),m.grossquantity,o.oper, s.quantity,
      j.przelicz, j.glowna, s.shortage, j.ref, m.warehouse, s.quantity, m.mattype, coalesce(m.prodtransfer,0)
    from prshmat m
      left join prshopers o on (o.ref = m.opermaster)
      left join prsheets s on (s.ref = m.sheet)
      left join towjedn j on (j.ref = m.jedn)
    where m.sheet=:prsheet and (o.oper = :properin or :properin is null)
      and m.mattype < 4
    order by m.opermaster,m.number
    into refout,ktmout,versout,opermasterout,numberout,nameout,
      sheetout,stopcascadeout,quantityout,properout,ilcalc,
      ratio, jmain, shortagesout, jednout, whout, KPARTLCALCOUT, mattype, prodtransfer
  do begin
    if (ratio is null) then ratio = 1;
    if (ilcalc is null or ilcalc = 0) then ilcalc = 1;
    if (jmain = 0) then
      select j.ref
        from towjedn j
        where j.ktm = :ktmout and j.glowna = 1
        into jednout;
    if (shortagesout is null) then shortagesout = 0;
    KQUANTITYOUT = quantityout * kquantityin;
    KQUANTITYOUT = kquantityout / coalesce(KPARTLCALCOUT,1);
    kpartquantityout = quantityout;
    shortagesout = (100+ shortagesout) / 100;
    quantityout = quantityin * quantityout;
    quantityout = quantityout * ratio * shortagesout;
    quantityout = quantityout / ilcalc;
    kquantityout = kquantityout * ratio * shortagesout;
    if (stopcascadeout = 1 and drillin = 0) then
      stopcascadeout = 0;
    if (stopcascadeout is null) then stopcascadeout = 0;
    if (stopcascadeout = 0 or stopcascadeout = 2 or sheetout is null or (stopcascadeout = 1 and treesubsheet = 0)) then
    begin
      if ((stopcascadeout = 0 or stopcascadeout = 2)  and sheetout is not null and treesubsheet = 1) then
        sheetout = null;
      suspend;
    end
    if(:treesubsheet=1 and :sheetout is not null) then
    begin
     for
       select KTMOUT,VERSOUT,NAMEOUT,QUANTITYOUT,KQUANTITYOUT,REFOUT,OPERMASTEROUT,NUMBEROUT,
           NUMBERALLOUT,properout,stopcascadeout,sheetout,jednout,whout, kpartquantityout, shortagesout, KPARTLCALCOUT
         from XK_PRSHEET_MATERIALS(:sheetout,:treesubsheet,:quantityout,:drillin,:properin,:kquantityout)
         into KTMOUT,VERSOUT,NAMEOUT,QUANTITYout,kQUANTITYOUT,REFOUT,OPERMASTEROUT,NUMBEROUT,
             NUMBERALLOUT,properout,stopcascadeout,sheetout,jednout,whout, kpartquantityout, shortagesout, KPARTLCALCOUT
      do begin
        suspend;
      end
    end
  end
end^
SET TERM ; ^
