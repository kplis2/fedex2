--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_LOC_STOCK_COMMITQUANTITY(
      MWSORDREF integer,
      VERS integer,
      QUANTITY numeric(15,4),
      LOT integer,
      STOCKCALC integer,
      CLOSEPAL smallint,
      UNITP integer = null,
      X_AKTUOPERATOR INTEGER_ID = null,
      X_SERIA STRING255 = '',
      X_PARTIA STRING20 = '',
      X_SLOWNIK_REF INTEGER_ID = 0)
   as
declare variable MWSCONSTLOCREF integer;
declare variable MWSACTSREF integer;
declare variable KTM varchar(60);
declare variable REFPAL integer;
declare variable WH varchar(3);
declare variable MWSCONSTLOC varchar(40);
declare variable ACTCNT integer;
declare variable MWSORDTYPES varchar(4);
declare variable FROMMMWSORD integer;
declare variable SYMBOL varchar(20);
declare variable ILOSCPAL numeric(14,4);
declare variable MWSORDTYPEDEST integer;
declare variable X_SERIE_REF INTEGER_ID;
begin

--<<XXX MatJ Wdr   
  if (UNITP = 0) then  UNITP = null;
--XXX>>
  if (lot = 0) then
    lot = null;
  if (stockcalc = 0) then stockcalc = null;
  if (quantity = 0) then exception universal 'Wprowadzono zerową ilość.';
  select mo.mwsfirstconstlocp, mo.mwsordtypes, mo.frommwsord, mo.mwsordtypedest
    from mwsords mo
    where mo.ref = :mwsordref
    into :mwsconstloc, :mwsordtypes, :frommmwsord, mwsordtypedest;
  select mo.wh, mo.ref
    from mwsconstlocs mo
    where mo.symbol = :mwsconstloc
    into :wh, :mwsconstlocref;
  select w.ktm
    from wersje w
    where w.ref = :vers
    into :ktm;
  select first 1 m.symbol
    from mwsacts ma
      join mwsords m on(m.ref = ma.mwsord)
    where ma.mwsconstlocp = :mwsconstlocref
      and ma.status > 0
      and ma.status < 5
      and ma.mwsordtypedest not in (5,15)
    into :symbol;
  execute procedure gen_ref('MWSACTS') returning_values mwsactsref;
 
--<<XXX MatJ Wdr   
    if (coalesce(x_seria,'') <> '') then
    begin
        select first 1 ref from x_mws_serie where ktm = :ktm and wersjaref = :vers and serialno = :x_seria
        into :x_serie_ref;
        if (x_serie_ref is null) then
        begin
          insert into X_MWS_SERIE (ktm,  WERSJAREF, SERIALNO, DATAPRZYJECIA, "USER", MWSORDIN, WMSAKTIN,
                                   TYP )
          values (:ktm,  :vers, :x_seria, current_timestamp, :X_AKTUOPERATOR, :MWSORDREF, :mwsactsref,
                  ( select x_mws_serie from wersje where ref=:vers) )
          returning REF
          into :X_SERIE_REF ;
        end
    end
--XXX>>

  insert into mwsacts (ref, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
           mwsconstlocl, mwspallocl, doctype, settlmode, closepalloc, wh,
           regtime, timestartdecl, timestopdecl,  lot, PALGROUP, ISPAL, MIXEDPALLGROUP, unitp           
--<<XXX MatJ Wdr
            , x_partia, x_serial_no, x_slownik)
--XXX>>
     values (:mwsactsref, :MWSORDREF, 0, 1, :ktm, :vers, :quantity, null, null,
             :mwsconstlocref, null,  'I', null, 0, :wh, current_timestamp(0), current_timestamp(0),
             current_timestamp(0), :lot, null, 0, 0, :unitp        
--<<XXX MatJ Wdr
              , iif(:x_partia<>'',cast(:x_partia as date),null),:X_SERIE_REF, :x_slownik_ref);
--XXX>>
  update mwsacts m set m.status = 1 where m.ref = :mwsactsref;
  update mwsacts m set m.status = 2, m.quantityc = m.quantity where m.ref = :mwsactsref;
  if (closepal = 1) then
  begin
    update mwsacts set status = 0 where mwsord = :mwsordref and status < 3 and quantity = 0;
    delete from mwsacts ma where ma.mwsord = :mwsordref and ma.status = 0;
    select count(ma.ref)
       from mwsacts ma
       where ma.mwsord = :mwsordref
       and ma.status = 5
       into :actcnt;
    update mwsords mo set mo.status = 5, mo.stockcalc = :stockcalc where mo.ref = :mwsordref;
  end
end^
SET TERM ; ^
