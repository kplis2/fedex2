--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_PRORD_PRODUCTS(
      NAGZAMREF integer)
  returns (
      PRODUCTKTM varchar(80) CHARACTER SET UTF8                           ,
      PRODUCTNAME varchar(255) CHARACTER SET UTF8                           ,
      PRODUCTAMOUNT numeric(14,4),
      PRODUCTMEASURE varchar(5) CHARACTER SET UTF8                           )
   as
begin
  for select pzp.ktm, t.nazwa, pzp.ilosc, tj.jedn
    from nagzam nzp
    left join pozzam pzp on nzp.ref = pzp.zamowienie and pzp.out=0
    left join towary t on pzp.ktm = t.ktm
    left join towjedn tj on pzp.jedn = tj.ref
    where nzp.ref = :nagzamref
    into :productktm, :productname, :productamount, :productmeasure
  do suspend;
end^
SET TERM ; ^
