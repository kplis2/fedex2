--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GEN_MWSORD_IN(
      DOCID integer,
      DOCGROUP integer,
      DOCPOSID integer,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      MWSORD integer,
      MWSACT integer,
      RECALC smallint,
      MWSORDTYPE integer)
  returns (
      REFMWSORD integer)
   as
declare variable REC smallint;
declare variable WH varchar(3);
declare variable STOCKTAKING smallint;
declare variable OPERATOR integer;
declare variable PRIORITY smallint;
declare variable BRANCH varchar(10);
declare variable PERIOD varchar(6);
declare variable FLAGS varchar(40);
declare variable SYMBOL varchar(20);
declare variable COR smallint;
declare variable QUANTITY numeric(14,4);
declare variable GOOD varchar(40);
declare variable GOODTMP varchar(40);
declare variable VERS integer;
declare variable OPERSETTLMODE smallint;
declare variable MWSCONSTLOCP integer;
declare variable WHSEC integer;
declare variable RECRAMP integer;
declare variable MWSPALLOCP integer;
declare variable MWSCONSTLOCL integer;
declare variable AUTOLOCCREATE smallint;
declare variable MWSPALLOCL integer;
declare variable WHAREA integer;
declare variable POSFLAGS varchar(40);
declare variable DESCRIPTION varchar(1024);
declare variable POSDESCRIPTION varchar(255);
declare variable LOT integer;
declare variable SHIPPINGAREA varchar(3);
declare variable WHAREALOGP integer;
declare variable MWSACCESSORY integer;
declare variable DIFFICULTY varchar(10);
declare variable TIMESTART timestamp;
declare variable MAXNUMBER integer;
declare variable TIMESTARTDCL timestamp;
declare variable TIMESTOPDCL timestamp;
declare variable REALTIME double precision;
declare variable WHAREALOGB integer;
declare variable WHAREALOGL integer;
declare variable DIST numeric(14,4);
declare variable REFMWSACTPAL integer;
declare variable REFMWSACT integer;
declare variable PALTYPE varchar(20);
declare variable PALACTSQUANTITY numeric(14,4);
declare variable TOINSERT numeric(14,4);
declare variable STATUS smallint;
declare variable WHSECOUT integer;
declare variable PALTYPEVERS integer;
declare variable PLANWHAREALOGL integer;
declare variable PLANMWSCONSTLOCL integer;
declare variable PLANWHAREAL integer;
declare variable PALPACKMETHSHORT varchar(30);
declare variable PALPACKMETH varchar(40);
declare variable PALPACKQUANTITY numeric(14,4);
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable SLOKOD varchar(40);
declare variable MANMWSACTS smallint;
declare variable MAXPALOBJ numeric(14,4);
declare variable REFILL smallint;
declare variable WH2 varchar(3);
declare variable altposmode smallint;
begin
  select first 1 t.ref
    from mwsordtypes t
    where t.mwsortypedets = 2
    into mwsordtype;
  if (docgroup is null) then docgroup = docid;
  select stocktaking, opersettlmode, autoloccreate
    from mwsordtypes
    where ref = :mwsordtype
    into stocktaking, opersettlmode, autoloccreate;
  select defdokum.wydania, defdokum.koryg, coalesce(dokumnag.mwsmag,dokumnag.magazyn), dokumnag.operator,
      dokumnag.uwagisped, dokumnag.katmag, dokumnag.spedpilne, dokumnag.oddzial,
      dokumnag.okres, dokumnag.flagi, dokumnag.symbol, dokumnag.kodsped,
      dokumnag.wharealogb, 6, dokumnag.ref, substring(dostawcy.id from 1 for 40), dokumnag.mag2
    from dokumnag
      left join dostawcy on (dokumnag.dostawca = dostawcy.ref)
      left join defdokum on (defdokum.symbol = dokumnag.typ)
    where dokumnag.ref = :docid
    into rec, cor, wh, operator,
      description, difficulty, priority, branch,
      period, flags, symbol, shippingarea,
      wharealogb, slodef, slopoz, slokod, wh2;

  select m.altposmode from mwsordtypes4wh m
    where m.mwsordtype = :mwsordtype
      and m.wh = :wh
  into :altposmode;
  if (:altposmode is null) then altposmode = 0;

  if (rec = 0) then rec = 1; else rec = 0;
  if (manmwsacts is null) then manmwsacts = 0;
  refmwsord = null;
  select first 1 ref from mwsords where docid = :docid and status < 3
    into refmwsord;
  if (exists(select first 1 1 from mwsords o where o.docid = :docid and o.status in (4,5))) then
    exception universal 'Zlecenie rozładunku zostało już zamknięte.';
  if (refmwsord is null) then
  begin
    execute procedure gen_ref('MWSORDS') returning_values refmwsord;
    select okres from datatookres(current_date,0,0,0,0) into period;
    insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator,
        priority, description, wh, difficulty, regtime, timestartdecl, timestopdecl,
        mwsaccessory, branch, period, flags, docsymbs, cor, rec, bwharea, ewharea,
        status, shippingarea, slodef, slopoz, slokod, manmwsacts)
      values (:refmwsord, :mwsordtype, :stocktaking, 'M', :docgroup, :docid, null,
          :priority, :description, :wh, :difficulty, current_timestamp(0), :timestart, current_timestamp(0),
          :mwsaccessory, :branch, :period, :flags, :symbol, :cor, :rec, :wharealogb, :wharealogb,
          0, :shippingarea, :slodef, :slopoz, :slokod,:manmwsacts);
  end
  -- znalezienie max. numeru operacji - kolejne zwiekszamy i jeden.
  delete from mwsacts where status = 0 and mwsord = :refmwsord;
  select max(number) from mwsacts where mwsord = :refmwsord into maxnumber;
  if (maxnumber is null) then maxnumber = 0;
  for
    select p.ref, p.dokument, p.ktm, p.wersjaref, p.dostawa,
    sum(case when n.mwsdisposition = 1 or :altposmode = 1 then p.ilosc else p.iloscl end)
      from dokumpoz p
        join dokumnag n on (n.ref = p.dokument)
      where p.dokument = :docid
        and ((coalesce(p.fake,0) = 0 and coalesce(p.havefake,0) = 0) or
             (coalesce(p.fake,0) = 1 and coalesce(p.havefake,0) = 0 and :altposmode = 1) or
             (coalesce(p.fake,0) = 0 and coalesce(p.havefake,0) = 1) and :altposmode = 2)
        and ((n.akcept = 0 and n.mwsdisposition = 1 and p.ilosc > 0) or
             (n.akcept = 1 and n.mwsdisposition = 0 and
                ((:altposmode in (0,2) and p.iloscl > 0) or
                 (:altposmode = 1 and p.ilosc > 0))))
        and :manmwsacts = 0
      group by p.ref, p.dokument,p.ktm, p.wersjaref, p.dostawa
      order by min(p.numer) desc
      into docposid, docid, good, vers, lot, quantity
  do begin
    toinsert = quantity;
    quantity = 0;
    select sum(a.quantity)
      from mwsacts a
      where a.mwsord = :refmwsord and a.vers = :vers and a.lot = :lot and a.status > 0 and a.status < 6
      into quantity;
    if (quantity is null) then quantity = 0;
    toinsert = toinsert - quantity;
    if (toinsert > 0) then
    begin
      update mwsords set status = 1 where ref = :refmwsord and status = 0;
      execute procedure gen_ref('MWSPALLOCS') returning_values mwspallocl;
      insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status,
          fromdocid, fromdocposid, fromdoctype)
        values(:mwspallocl,cast(:mwspallocl as varchar(20)), null, 2, 0, :docid, :docposid,'M');
      insert into mwsacts (ref, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
          mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
          regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority,
          lot, recdoc, plus, wharealogl, wharealogp, number, disttogo, palgroup, ispal,
          planmwsconstlocl, planwharealogl, planwhareal, PALPACKMETHSHORT, PALPACKMETH, palpackquantity, lockedit)
        values (:refmwsact, :refmwsord, 0, :stocktaking, :good, :vers, :toinsert, null, null,
           null, :mwspallocl, :docid, :docposid, 'M', :opersettlmode, :autoloccreate, :wh, :wharea, :whsecout,
           current_timestamp(0), :timestartdcl, :timestopdcl, :realtime, :posflags, :posdescription, :priority,
           :lot, :rec, 1, :wharealogp, :wharealogl, :maxnumber, :dist, :mwspallocl, 0,
           :planmwsconstlocl, :planwharealogl, :planwhareal, :PALPACKMETHSHORT, :PALPACKMETH, :palpackquantity, 1);
    end
  end
  suspend;
end^
SET TERM ; ^
