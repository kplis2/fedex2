--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_TIMESTAMP_DIFF_TOSTRING(
      TIMETOSTRING double precision)
  returns (
      RETTIME varchar(20) CHARACTER SET UTF8                           )
   as
declare variable roznica double precision;
declare variable sekundy integer;
declare variable minuty integer;
declare variable godziny integer;
begin
   roznica = timetostring;
   roznica = timetostring * 86400;
   if (:roznica >= 60) then
   begin
    minuty = roznica/60;
    sekundy = mod (:roznica,60);
    godziny = 0;
    end
    if (:roznica > 3600) then
    begin
      godziny = minuty/60;
      minuty = mod(:minuty,60);
    end
    if (:roznica < 60) then
    begin
    godziny = 0;
    minuty = 0;
    sekundy = :roznica;
    end
  rettime = substring('0'||:godziny from iif(coalesce(char_length('0'||:godziny),0)>2,2,1) for coalesce(char_length('0'||:godziny),0))||':' -- [DG] XXX ZG119346
  ||substring('0'||:minuty from coalesce(char_length('0'||:minuty),0)-1 for coalesce(char_length('0'||:minuty),0))||':' -- [DG] XXX ZG119346
  ||substring('0'||:sekundy from coalesce(char_length('0'||:sekundy),0)-1 for coalesce(char_length('0'||:sekundy),0)); -- [DG] XXX ZG119346
  suspend;
end^
SET TERM ; ^
