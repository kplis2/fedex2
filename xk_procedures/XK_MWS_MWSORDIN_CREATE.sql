--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSORDIN_CREATE(
      WH DEFMAGAZ_ID,
      DOSTAWCA DOSTAWCA_ID,
      AKTUOPERATOR OPERATOR_ID)
  returns (
      MWSORDREF MWSORDS_ID,
      STATUS INTEGER_ID)
   as
declare variable PERIOD period_id;
declare variable BRANCH oddzial_id;
declare variable MWSORDTYPE integer_id;
declare variable STOCKTAKING smallint_id;
declare variable SLOKOD string40;
declare variable REALIZED integer_id;
begin
  select dm.oddzial from defmagaz dm where dm.symbol = :WH into :branch;

  select first 1 t.ref, t.stocktaking
    from mwsordtypes t
    where t.mwsortypedets = 2
    order by t.ref
    into :mwsordtype, :stocktaking;

  select substring(d.id from 1 for 40) --XXX ZG133796 MKD
    from dostawcy d
    where d.ref = :dostawca
    into :slokod;

  execute procedure gen_ref('MWSORDS') returning_values mwsordref;

  select okres from datatookres(current_date,0,0,0,0) into period;

  insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator,
      priority, description, wh, difficulty, regtime, timestartdecl, timestopdecl,
      mwsaccessory, branch, period, flags, docsymbs, cor, rec, bwharea, ewharea,
      status, shippingarea, slodef, slopoz, slokod, manmwsacts)
    values (:mwsordref, :mwsordtype, :stocktaking, 'M', null, null, null,
      0, '', :wh, null, current_timestamp, null, current_timestamp,
      null, :branch, :period, null, null, 1, 1, null, null,
      1, null, 6, :dostawca, :slokod, 0);

  --execute procedure XK_MWS_MWSORD_CHANGE_STATUS(:mwsordref, 2, :aktuoperator)
  --  returning_values :realized;
  status=1;
  suspend;
end^
SET TERM ; ^
