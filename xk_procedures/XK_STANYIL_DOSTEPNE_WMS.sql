--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_STANYIL_DOSTEPNE_WMS(
      DOCID integer,
      WH varchar(3) CHARACTER SET UTF8                           )
  returns (
      WERSJAREF integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      ILDOST numeric(14,4))
   as
begin
  for
    select xk.wersjaref, xk.ktm, xk.magazyn, sum(xk.ildost)
      from xk_stanyil_dostepne (:docid, :wh) xk
      group by xk.wersjaref, xk.ktm, xk.magazyn
      into wersjaref, ktm, magazyn, ildost
  do begin
    suspend;
  end
end^
SET TERM ; ^
