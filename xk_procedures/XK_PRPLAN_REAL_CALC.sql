--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_PRPLAN_REAL_CALC(
      PRDEPART varchar(20) CHARACTER SET UTF8                           ,
      PRORDSREGS varchar(80) CHARACTER SET UTF8                           ,
      FROMDATE timestamp)
   as
declare variable ktm varchar(40);
declare variable todate timestamp;
declare variable quantity numeric(14,4);
declare variable quantityr numeric(14,4);
declare variable cref integer;
begin
  for
    select c.ref
      from prdeparts dp
        join frdpersdimhier ph on (dp.frdpersdimhier = ph.ref)
        join frdpersdimvals v on (v.frdpersdimshier = ph.ref)
        join frdperspects p on (ph.frdperspect = p.ref)
        join frhdrs f on (p.frhdr = f.symbol)
        join frdhdrs fh on (fh.frhdr = f.symbol)
        join frdhdrsvers fv on (fv.frdhdr = fh.ref and fv.main = 1)
        join frpsns fp on (fp.ref = v.frpsn)
        join frdimhier dh on (ph.frdimshier = dh.ref)
        join frdimelems fd on (dh.ref = fd.frdimhier)
        join dimelemdef d on (fd.dimelem = d.ref)
        join frdimhier dht on (dht.ref = p.timefrdimhier)
        join frdimelems fdt on (dht.ref = fdt.frdimhier)
        join dimelemdef dt on (fdt.dimelem = dt.ref)
        left join frdcells c on (c.frdhdrver = fv.ref and c.timelem = fdt.dimelem and c.frdpersdimval = v.ref and c.dimelem = d.ref)
      where dp.symbol = :prdepart and dt.bdate is not null and dt.edate is not null
        and dt.edate >= :fromdate
      into cref
  do begin
    update frdcells c set c.realamount = 0, c.val1 = 0 where c.ref = :cref;
  end
  for
    select p.ktm, coalesce(p.termdost, n.termdost), sum(p.ilosc - p.ilzreal), sum(p.ilzreal)
      from nagzam n
        left join pozzam p on (p.zamowienie = n.ref)
        left join relations r on (r.stablefrom = 'POZZAM' and r.stableto = 'POZZAM' and r.sreffrom = p.ref)
        left join pozzam pn on (pn.ref = r.srefto)
        left join nagzam nn on (nn.ref = pn.zamowienie)
        left join typzam t on (t.symbol = nn.typzam)
        left join typzam tp on (tp.symbol = n.typzam)
      where n.prdepart = :prdepart and tp.wydania = 3 and position(n.rejestr in :prordsregs) > 0
        and p.out = 0 and coalesce(t.wydania,1) = 1 and coalesce(p.termdost, n.termdost) >= :fromdate
      group by p.ktm, coalesce(p.termdost, n.termdost)
      into ktm, todate, quantity, quantityr
  do begin
    if (quantity is null) then quantity = 0;
    if (quantityr is null) then quantityr = 0;
    cref = null;
    select c.ref
      from prdeparts dp
        join frdpersdimhier ph on (dp.frdpersdimhier = ph.ref)
        join frdpersdimvals v on (v.frdpersdimshier = ph.ref)
        join frdperspects p on (ph.frdperspect = p.ref)
        join frhdrs f on (p.frhdr = f.symbol)
        join frdhdrs fh on (fh.frhdr = f.symbol)
        join frdhdrsvers fv on (fv.frdhdr = fh.ref and fv.main = 1)
        join frpsns fp on (fp.ref = v.frpsn)
        join frdimhier dh on (ph.frdimshier = dh.ref)
        join frdimelems fd on (dh.ref = fd.frdimhier)
        join dimelemdef d on (fd.dimelem = d.ref)
        join frdimhier dht on (dht.ref = p.timefrdimhier)
        join frdimelems fdt on (dht.ref = fdt.frdimhier)
        join dimelemdef dt on (fdt.dimelem = dt.ref)
        left join frdcells c on (c.frdhdrver = fv.ref and c.timelem = fdt.dimelem and c.frdpersdimval = v.ref and c.dimelem = d.ref)
      where dp.symbol = :prdepart and dt.bdate is not null and dt.edate is not null
        and d.shortname = :ktm and dt.bdate <= :todate and dt.edate >= :todate
      into cref;
    if (cref is not null) then
      update frdcells c set c.realamount = coalesce(c.realamount,0) + :quantityr, c.val1 = coalesce(c.val1,0) + :quantity
        where c.ref = :cref;
  end
end^
SET TERM ; ^
