--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_CHECK_LOCATION(
      BARCODE varchar(20) CHARACTER SET UTF8                           )
  returns (
      REF integer,
      ACT smallint,
      CNT integer)
   as
begin
  ref = 0;
  act = 0;
  cnt = 0;

  select count(ref) from mwsconstlocs
    where symbol = :barcode
  into :cnt;
  if (coalesce(:cnt,0) = 0) then
    exit;
  else if (coalesce(:cnt,0) > 1) then
    exit;

  select ref, act from mwsconstlocs
    where symbol = :barcode
  into :ref, :act;

  suspend;
end^
SET TERM ; ^
