--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MOVE_PALGROUP_MWSACTS(
      MWSORD integer,
      DOCID integer,
      PALLGROUP integer)
   as
declare variable mwsconstlocl integer;
declare variable mwspallocl integer;
declare variable doctype varchar(1);
begin
  mwsconstlocl = null;
  mwspallocl = null;
  select o.doctype
    from mwsords o where o.ref = :mwsord
    into doctype;
  if (doctype = 'M') then
    insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
        mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
        regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority,
        lot, recdoc, plus, number, wharealogl, wharealogp, disttogo, palgroup, autogen,
          x_partia, x_serial_no, x_slownik) -- XXX KBI
      select :mwsord, 1, a.stocktaking, a.good, a.vers, a.quantity, a.mwsconstlocl, a.mwspallocl,
          :mwsconstlocl, :mwspallocl, :docid, p.ref, :doctype, a.settlmode, a.closepalloc, a.wh, a.wharea, a.whsec,
          current_date, current_date,current_date, a.realtimedecl, a.flags, a.descript, a.priority,
          a.lot, 0, a.plus, a.number, a.wharealogl, a.wharealogp, a.disttogo, a.palgroup, 1,
          a.x_partia, a.x_serial_no, a.x_slownik -- XXX KBI
        from dokumpoz p
          join mwsacts a on (a.ref = p.frommwsact)
          where p.dokument = :docid;
  else if (doctype = 'Z') then
    insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
        mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
        regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority,
        lot, recdoc, plus, number, wharealogl, wharealogp, disttogo, palgroup, autogen,
          x_partia, x_serial_no, x_slownik) -- XXX KBI
      select :mwsord, 1, a.stocktaking, a.good, a.vers, a.quantity, a.mwsconstlocl, a.mwspallocl,
          :mwsconstlocl, :mwspallocl, :docid, p.ref, :doctype, a.settlmode, a.closepalloc, a.wh, a.wharea, a.whsec,
          current_date, current_date,current_date, a.realtimedecl, a.flags, a.descript, a.priority,
          a.lot, 0, a.plus, a.number, a.wharealogl, a.wharealogp, a.disttogo, a.palgroup, 1,
          a.x_partia, a.x_serial_no, a.x_slownik -- XXX KBI
        from pozzam p
          join mwsacts a on (a.ref = p.frommwsact)
          where p.zamowienie = :docid;
  update mwsords set status = 1, takefullpal = 1, operator = null where ref = :mwsord;
end^
SET TERM ; ^
