--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_GET_BANKACCOUNT(
      DOKREF integer,
      TAB varchar(20) CHARACTER SET UTF8                           )
  returns (
      BANKACCOUNT varchar(255) CHARACTER SET UTF8                           )
   as
declare variable walutowy smallint;
begin
   if(tab='NAGFAK') then begin
      select t.walutowy from nagfak d
        join typfak t on t.symbol = d.typ
        where d.ref = :dokref
        into :walutowy;
   end else if(tab='NAGZAM') then begin
      select t.walutowe from nagzam d
        join typzam t on t.symbol = d.typzam
        where d.ref = :dokref
        into :walutowy;
   end
    if (walutowy = 1) then begin
      execute procedure GET_CONFIG('INFORACH2',0) returning_values :bankaccount;
    end
    else begin
      execute procedure GET_CONFIG('INFORACH',0) returning_values :bankaccount;
    end
    suspend;
end^
SET TERM ; ^
