--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_RECALC_SELLAV_FOR_STOCK(
      VERS integer,
      WH varchar(3) CHARACTER SET UTF8                           )
   as
declare variable quantity numeric(14,4);
begin
  quantity = 0;
  execute procedure xk_mws_check_avquantity(:vers,null,:wh)
    returning_values quantity;
  if (quantity is null) then quantity = 0;
  if (not exists (select q.ref from mwsversq q where q.wh = :wh and q.vers = :vers)) then
    insert into mwsversq (vers, wh, quantity) values(:vers,:wh,:quantity);
  else
    update mwsversq set quantity = :quantity where wh = :wh and vers = :vers and quantity <> :quantity;
end^
SET TERM ; ^
