--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSORDIN_GOOD_INFO(
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      MWSORD integer)
  returns (
      MSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable pom varchar(10);
declare variable good2 varchar(40);
declare variable quantity numeric(14,4);
declare variable quantityc numeric(14,4);
declare variable quantitywh numeric(14,4);
declare variable side char(1);
declare variable docid integer;
declare variable goodsrefill integer;
declare variable wh varchar(3);
declare variable nazwa varchar(255);
begin
  pom = '
';
  msg = '';
  select nazwa from towary where ktm = :good
    into nazwa;
  quantity = 0;
  quantityc = 0;
  select docid, wh from mwsords where ref = :mwsord
    into docid, wh;
  if (docid is null) then
    exit;
  select coalesce(sum(case when d.mwsdisposition = 1 then p.ilosc else p.iloscl end),0)
    from dokumpoz p
      left join dokumnag d on (d.ref = p.dokument)
    where p.dokument = :docid and p.ktm = :good and p.wersjaref = :vers
      and p.genmwsordsafter = 1
    into quantity;
  select coalesce(sum(quantity),0)
    from mwsacts
    where mwsord = :mwsord and status in (2,5) and vers = :vers
    into quantityc;
  quantity = quantity - quantityc;
  quantitywh = 0;
  select coalesce(sum(s.quantity - s.blocked + s.ordered),0)
    from mwsstock s
      left join mwsconstlocs c on (c.ref = s.mwsconstloc)
    where s.wh = :wh and s.vers = :vers and c.goodsav = 1 and c.act = 1
    into quantitywh;
  if (quantity < 0) then quantity = 0;
  msg = nazwa||' - '||cast(quantity as integer)||' ('||cast(quantitywh as integer)||')'||pom;
  side = substring(good from 9 for 9);
  good2 = '';
  quantity = 0;
  quantityc = 0;
  if (side = 'L') then
    good2 = substring(good from 1 for 8)||'R'||substring(good from 10 for 255);
  else if (side = 'R') then
    good2 = substring(good from 1 for 8)||'L'||substring(good from 10 for 255);
  if (good2 <> '') then
  begin
    select coalesce(sum(case when d.mwsdisposition = 1 then p.ilosc else p.iloscl end),0)
      from dokumpoz p
        left join dokumnag d on (d.ref = p.dokument)
      where p.dokument = :docid and p.ktm = :good2
        and p.genmwsordsafter = 1
      into quantity;
    select coalesce(sum(quantity),0)
      from mwsacts
      where mwsord = :mwsord and status in (2,5) and good = :good2
      into quantityc;
    quantity = quantity - quantityc;
    if (quantity < 0) then quantity = 0;
    quantitywh = 0;
    select coalesce(sum(s.quantity - s.blocked + s.ordered),0)
      from mwsstock s
        left join mwsconstlocs c on (c.ref = s.mwsconstloc)
      where s.wh = :wh and s.good = :good2 and c.goodsav = 1 and c.act = 1
      into quantitywh;
    msg = msg||good2||' - '||cast(quantity as integer)||' ('||cast(quantitywh as integer)||')'||pom;
  end
  goodsrefill = 0;
  select coalesce(sum(g.posquantity - g.ordered),0)
    from mwsgoodsrefill g
    where g.wh = :wh and g.vers = :vers and (g.posquantity - g.ordered) > 0
    into goodsrefill;
  msg = msg||'Ilość w półki: '||goodsrefill||pom;
  msg = msg||'Kontynuować ?';
end^
SET TERM ; ^
