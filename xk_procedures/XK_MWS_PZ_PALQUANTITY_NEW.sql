--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_PZ_PALQUANTITY_NEW(
      KODKRESK varchar(100) CHARACTER SET UTF8                           ,
      MWSORD integer,
      VERSIN INTEGER_ID = 0)
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      GOOD varchar(255) CHARACTER SET UTF8                           ,
      REFPOZ integer,
      PALGROUP integer,
      ILOSCPALET integer,
      ILOSCPALETZREAL integer,
      ILOSC numeric(15,4),
      ILOSCZREAL numeric(15,4),
      ILOSCPOZ numeric(15,4),
      PRZELICZNIK numeric(15,4),
      ISMIXED smallint,
      MIXEDWITH varchar(255) CHARACTER SET UTF8                           ,
      MIXEDWITHNOTREAL varchar(255) CHARACTER SET UTF8                           ,
      NOTREALCOUNT integer,
      PALPACKMETHSHORT varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      ORDDESCRIPT varchar(1024) CHARACTER SET UTF8                           ,
      ACTDESCRIPT varchar(1024) CHARACTER SET UTF8                           ,
      SYMBOL varchar(40) CHARACTER SET UTF8                           ,
      MAXPALWAGA numeric(14,4),
      WAGAJEDN numeric(14,4),
      JEDNS varchar(5) CHARACTER SET UTF8                           ,
      PACKINGLIST integer,
      LOT integer,
      MAGBREAK smallint,
      NEWPOSASK smallint,
      X_MWS_PARTIE SMALLINT_ID,
      X_MWS_SERIE SMALLINT_ID,
      X_SLOWNIKDEF INTEGER_ID)
   as
declare variable wersjaref integer;
declare variable poz smallint;
declare variable iloscmixed integer;
declare variable goodt varchar(40);
declare variable status smallint;
declare variable outsymbol varchar(40);
declare variable maxpalwagas varchar(40);
declare variable unitid integer;
declare variable docid integer;
declare variable goodmagbreak smallint;
begin
  mixedwith = '';
  mixedwithnotreal = '';
  packinglist = 0;
  lot = 0;
  magbreak = 0;
  newposask = 1;
  execute procedure get_config('MAXPALWAGA',0)
    returning_values :maxpalwagas;
  if (maxpalwagas is not null and maxpalwagas <> '') then
    maxpalwaga = cast(maxpalwagas as numeric(14,4));
  else
    maxpalwaga = 1000;
  select mo.description, mo.symbol, mo.docid
    from mwsords mo
    where mo.ref = :mwsord
    into :orddescript, :symbol, docid;
  if (docid is null) then newposask = 0;
  if (coalesce(kodkresk,'')='') then begin
    suspend;
    exit;
  end
  execute procedure mws_mwsordin_check_packinglist(:kodkresk, :mwsord)
    returning_values (:packinglist);
  if (packinglist is null) then packinglist = 0;
  if (packinglist > 0) then
  begin
    suspend;
    exit;
  end

  execute procedure xk_mws_barcode_validation(:kodkresk, :versin)
    returning_values :ktm, :vers, :przelicznik, :status, :jedns, :unitid;


--XXX MatJ wdr

  select coalesce(w.x_mws_serie,0), coalesce(w.x_mws_partie,0), coalesce(w.x_mws_slownik_ehrle,0)
    from wersje w
    where w.ref=:vers
  into :x_mws_serie, :x_mws_partie, :X_SLOWNIKDEF;
--XXX


  execute procedure mws_check_magbreak(:mwsord, :vers)
    returning_values :magbreak;

  if (status = 0) then begin
    suspend;
    exit;
  end
  select first 1 t.nazwa||' '||coalesce(t.x_nazwa2,'')||', '||w.nazwa, j.waga
    from wersje w
      join towary t on (w.ktm = t.ktm)
      left join towjedn j on(j.przelicz = :przelicznik and t.ktm = j.ktm)
    where w.ref = :vers
    into :good, :wagajedn;
  select first 1 ma.quantity, ma.ref, ma.mixedpallgroup, ma.palgroup, ma.palpackmethshort, ma.descript
    from mwsacts ma
    where ma.mwsord = :mwsord
      and ma.vers = :vers
      and ma.status <= 2
    order by -ma.mixedpallgroup desc
    into :iloscpoz, :refpoz, :ismixed, :palgroup, :palpackmethshort, :actdescript;
-- sprawdzenie ile jest towaru i na ilu paletach
  select sum(ma.quantity), count(distinct ma.palgroup)
    from mwsacts ma
    where ma.mwsord = :mwsord
      and ma.vers = :vers
      and ma.status = 0
    into :ilosc, :iloscpalet;
  --sprawdzenie ile jest zrealizowanego towaru i na ilu paletach
  select sum(ma.quantity), count(distinct ma.palgroup)
    from mwsacts ma
    where ma.mwsord = :mwsord
      and ma.vers = :vers
      and ma.status in (2,5)
    into :ilosczreal, :iloscpaletzreal;
  if (ilosczreal is null) then ilosczreal = 0;
  if (refpoz is null) then refpoz = 0;
  if (ilosc is null) then ilosc = 0;
  if (iloscpoz is null) then iloscpoz = 0;
  if (:ismixed = 1) then
  begin
    for select ma.good, cast(ma.quantity as integer)
      from mwsacts ma
      where ma.palgroup = :palgroup
        and ma.vers <> :vers
        and ma.ref <> ma.palgroup
      into :goodt, :iloscmixed
    do begin
      if (coalesce(char_length(:mixedwith),0) < 200) then -- [DG] XXX ZG119346
        mixedwith = mixedwith || goodt || '/' || iloscmixed || '; ';
      else
      begin
        mixedwith = mixedwith || '...';
        break;
      end
    end
    for select ma.good, cast(ma.quantity as integer)
      from mwsacts ma
      where ma.palgroup = :palgroup
        and ma.good <> :ktm
        and ma.vers <> :vers
        and ma.ref <> ma.palgroup
        and ma.status < 5
      into :goodt, :iloscmixed
    do begin
      if (coalesce(char_length(mixedwithnotreal),0) < 200) then -- [DG] XXX ZG119346
        mixedwithnotreal = mixedwithnotreal || goodt || '/' || iloscmixed || '; ';
      else
      begin
        mixedwithnotreal = mixedwithnotreal || '...';
        break;
      end
    end
    -- sprawdzenie ile zostao nie zrealizowanych towarów na palecie typu mix
    select count(ma.good)
      from mwsacts ma
      where ma.palgroup = :palgroup
        and ma.good <> :ktm
        and ma.vers <> :vers
        and ma.ref <> ma.palgroup
        and ma.status < 5
      into :notrealcount;
  end
  suspend;
end^
SET TERM ; ^
