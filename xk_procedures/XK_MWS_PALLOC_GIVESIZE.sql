--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_PALLOC_GIVESIZE(
      SYMBOL varchar(20) CHARACTER SET UTF8                           )
  returns (
      L numeric(15,4),
      W numeric(15,4),
      H numeric(15,4))
   as
begin
  SELECT m.l, m.w, m.h
    FROM mwsconstlocs m
    where m.symbol = :symbol
    into :l, :w, :h;
  suspend;
end^
SET TERM ; ^
