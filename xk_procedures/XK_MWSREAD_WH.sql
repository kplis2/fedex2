--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWSREAD_WH(
      FWH varchar(3) CHARACTER SET UTF8                           )
  returns (
      REF integer,
      COORDXB numeric(15,2),
      COORDXE numeric(15,2),
      COORDYB numeric(15,2),
      COORDYE numeric(15,2),
      AREATYPE smallint,
      LOGWHAREAS integer,
      SYMBOL varchar(40) CHARACTER SET UTF8                           ,
      HINT varchar(4000) CHARACTER SET UTF8                           ,
      RED integer,
      GREEN integer,
      BLUE integer)
   as
declare variable tmp varchar (20);
declare variable numer integer;
declare variable eol varchar(2);
declare variable quantity numeric(14,4);
declare variable good varchar(40);
declare variable locp numeric(14,4);
declare variable stockp numeric(14,4);
declare variable fill numeric(14,4);
begin
  eol = '
';
  for select w.REF, w.COORDXB, w.COORDXE, w.COORDYB, w.COORDYE, w.AREATYPE, w.LOGWHAREAS, w.SYMBOL
  from whareas w
  where w.wh = :fwh
  into :ref, :coordxb, :coordxe, :coordyb, :coordye, :areatype, :logwhareas, :symbol
  do begin
    tmp = null;
    for
      select s.symbol, s.MWSSTANDLEVELNUMBER
      from mwsconstlocsymbs s
      where wharea = :ref
      order by MWSSTANDLEVELNUMBER
      into :tmp, :numer
    do begin
      if(hint is not null and hint <> '') then
        hint = hint ||eol||cast(numer as varchar (3))||': '|| tmp;
      else
        hint = cast(numer as varchar (3))||': '||tmp;
    end
    if (hint is null) then
      hint = '';
    else
      hint = hint||eol;
    hint = hint||'Towary na lokacji:'||eol;
    for
      select s.good, sum(s.quantity)
        from mwsconstlocs c
          left join mwsstock s on (s.mwsconstloc = c.ref)
        where c.wharea = :ref and s.ispal <> 1
        group by s.good
        into good, quantity
    do begin
      hint = hint||:good||': '||:quantity||eol;
    end
    select max(t.maxpallocq)
      from mwsconstlocs c
        left join mwsconstloctypes t on (t.ref = c.mwsconstloctype)
      where c.wharea = :ref
      into locp;
    if (locp is null or locp = 0) then locp = 1;
    select count(distinct s.mwspalloc)
      from mwsconstlocs c
        left join mwsstock s on (s.mwsconstloc = c.ref)
      where c.wharea = :ref and s.ispal <> 1
      into stockp;
    if (stockp is null) then stockp = 0;
    hint = hint||'Ilość palet: '||cast(stockp as integer)||' / '||locp;
    fill = stockp / locp;
    if (fill < 0.1) then
      red = 245;
    else if (fill >= 0.1 and fill < 0.2) then
      red = 235;
    else  if (fill >= 0.2 and fill < 0.3) then
      red = 225;
    else  if (fill >= 0.3 and fill < 0.4) then
      red = 215;
    else  if (fill >= 0.4 and fill < 0.5) then
      red = 205;
    else  if (fill >= 0.5 and fill < 0.6) then
      red = 195;
    else  if (fill >= 0.6 and fill < 0.7) then
      red = 185;
    else  if (fill >= 0.7 and fill < 0.8) then
      red = 175;
    else  if (fill >= 0.8 and fill < 0.9) then
      red = 165;
    else  if (fill >= 0.9 and fill < 1) then
      red = 155;
    else  if (fill >= 1) then
      red = 145;
    green = 0;
    blue = 0;
    suspend;
    ref = null;
    coordxb = null;
    coordxe = null;
    coordyb = null;
    coordye = null;
    areatype = null;
    logwhareas = null;
    symbol = null;
    hint = null;
  end
end^
SET TERM ; ^
