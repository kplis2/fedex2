--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSDROZ_GETTOWAR(
      KODKRESK STRING30,
      KLIENT INTEGER_ID = null)
  returns (
      REF integer)
   as
begin
    select ref from wersje where kodkresk = :kodkresk
    into :ref;
    if (:ref is not null) then
    begin
        suspend;
        exit;
    end

    select ref from wersje where ktm = :kodkresk and nrwersji = 0
    into :ref;
    if (:ref is not null) then
    begin
        suspend;
        exit;
    end

    select tkk.wersjaref from towkodkresk tkk where tkk.kodkresk = :kodkresk
    into :ref;
    if (:ref is not null) then
    begin
        suspend;
        exit;
    end

    if (:klient is not null) then
    begin
      select x.wersjaref from x_towkodkresk_klient x
        where x.klient = :klient
          and x.kodkreskklienta = :kodkresk
      into :ref;
      if (:ref is not null) then
      begin
        suspend;
        exit;
      end
    end
end^
SET TERM ; ^
