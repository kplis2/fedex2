--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_LISTYWYSD_GETDOK_HH(
      WARTOSC STRING255)
  returns (
      REFDOK INTEGER_ID,
      REFPOZ INTEGER_ID,
      DOK_SYMBOL STRING255,
      STATUSOUT SMALLINT_ID,
      DOK_SYMBOLNOK STRING100,
      STATUS SMALLINT_ID,
      MSG STRING255,
      SZYBKO SMALLINT_ID)
   as
declare variable KOR smallint_id;
declare variable WYD smallint_id;
declare variable SPED smallint_id;
declare variable TYP string3;
declare variable DOCGROUPINT integer_id;
declare variable WARTOSCTMP string40;
declare variable wysylkadone smallint_id;
declare variable AKCEPT smallint_id;
declare variable PACKTYPE smallint_id;
begin
  wartosc = upper(wartosc);
  status = 0;
  msg = '';
  execute procedure X_CHECK_IS_INT(:wartosc)
    returning_values :status;
  if (:status = 1) then docgroupint = :wartosc;
  else
  begin
    if (strpos('K',:wartosc) = 1) then begin
      select c.docid
        from mwsconstlocs m
          left join mwsordcartcolours c ON m.ref = c.cart  and c.status <2
          where m.symbol = :wartosc
      into docgroupint;
    end else if (strpos('MWS-WZ',:wartosc) = 1 or strpos('ZAM',:wartosc) = 1) then begin
      select d.grupasped
        from dokumnag d
          where d.symbol = :wartosc
      into docgroupint;
    end else if (strpos('$',:wartosc) = 1 and strpos('#',:wartosc) > 1) then begin
      wartosctmp = substr(:wartosc, 2, strpos('#',:wartosc)-1);
      execute procedure X_CHECK_IS_INT(:wartosctmp)
        returning_values :status;
      if (:status = 1) then docgroupint = cast(:wartosctmp as integer);
    end
  end

  if (coalesce(docgroupint,0) = 0) then begin
    msg = 'Zeskanowno kod kreskowy, który nie jest numerem dokumentu!';
    status = 0;
    exit;
  end

  select first 1 d.ref, d.wysylkadone, d.typ, d.symbol
    from dokumnag d
    where d.grupasped = :docgroupint
      and d.koryg = 0
    into :refdok, :wysylkadone, :typ, :dok_symbol;

  if (coalesce(refdok, 0) = 0) then begin
    msg = 'Nie znaleziono dokumentu magazynowego!';
    status = 0;
    exit;
  end

  if (typ is null) then typ = '';
  if (refdok is null) then refdok = 0;
  if (wysylkadone is null) then wysylkadone = 0;
  if (dok_symbol is null) then dok_symbol = '';
  refpoz = 0;
  select first 1 l.ref, l.akcept, coalesce(l.x_szybko, '0')
    from listywysd l
    where l.refdok = :refdok
    into :refpoz, :akcept, :szybko;

  refpoz = coalesce(refpoz, 0);
  akcept = coalesce(akcept, 0);
  select d.wydania, d.koryg, d.transport
    from defdokum d
    where d.symbol = :typ
    into wyd, kor, sped;
  if (wyd <> 1 or kor = 1 or sped <> 1) then
    packtype = 0;
  else
    packtype = 1;
  if (wysylkadone = 1 and akcept > 0) then begin
    msg = 'Dokument jest już wysłany lub zamknięty!';
    status = 0;
    exit;
  end
  if (packtype <> 1) then begin
    msg = 'Niepoprawne zlecenie!';
    status = 0;
    exit;
  end

-- czy dokumenty WT i pozycje zaakceptowane Ldz XXX
  if (refpoz > 0) then
    begin
      select first 1 1, m.symbol
      from mwsords m
      join mwsacts ma on m.ref = ma.mwsord
      join listywysd l on l.refdok = ma.docid
      where l.ref = :refpoz
        and (m.status not in (1, 5, 6) or ma.status not in (1, 5, 6))
      into :statusout, :dok_symbolnok;
      if (statusout is null) then statusout = 0;
      if (dok_symbolnok is null) then dok_symbolnok = '';
    end
  status = 1;
end^
SET TERM ; ^
