--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_SET_POZQUAN_ON_INWENTAP(
      OIREF integer,
      INWENTAREF integer)
   as
declare variable good varchar(17);
declare variable vers integer;
declare variable quantity numeric(15,4);
declare variable mwsconstlocl integer;
declare variable mwspallocl integer;
declare variable inwentapref integer;
begin
  for 
    select ma.good, ma.vers, ma.quantity, ma.mwsconstlocl, ma.mwspallocl
      from mwsacts ma
      where ma.mwsord = :oiref
        and ma.status = 5
      into :good, :vers, :quantity, :mwsconstlocl, :mwspallocl
  do begin
    select first 1 i.ref
      from inwentap i
      where i.inwenta = :inwentaref
        and i.mwsconstloc = :mwsconstlocl
        and i.wersjaref = :vers
      into :inwentapref;
    if (inwentapref is not null) then
      update inwentap i set i.ilinw = :quantity where i.ref = :inwentapref;
    else
    begin
      insert into INWENTAP(INWENTA,KTM,WERSJAREF,CENA,DOSTAWA,ILSTAN,ILINW,STATUS,
          BLOKADANALICZ, MWSCONSTLOC, MWSPALLOC)
         values(:inwentaref, :good, :vers, 0, null, 0, :quantity, 0, 1,
             :mwsconstlocl, :mwspallocl);
    end
  end
end^
SET TERM ; ^
