--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_PRSHEET_TREE_OPERS(
      PRSHEET integer,
      PRSHOPERCUR integer)
  returns (
      NAMEOUT varchar(255) CHARACTER SET UTF8                           ,
      REFOUT varchar(1024) CHARACTER SET UTF8                           ,
      COMPLEXOUT smallint,
      PROPEROUT varchar(255) CHARACTER SET UTF8                           ,
      NUMBEROUT integer,
      PRSHEETOUT integer,
      TOOPEROUT integer,
      UNREPORTED smallint)
   as
declare variable subsheet integer;
declare variable tmp smallint;
declare variable tooperoutcur integer;
declare variable tooperoutcur2 integer;
begin
  for select s.ref, s.complex, s.descript, s.oper, s.number, s.sheet
    from prshopers s
    where s.sheet = :prsheet and
      (:prshopercur is null and s.opermaster is null or :prshopercur is not null and s.opermaster is not null and s.opermaster = :prshopercur)
    order by s.number
    into refout, complexout, nameout, properout, numberout, prsheetout
  do begin
    if(complexout = 1) then begin
      suspend;
      for select refout, complexout, nameout, properout, numberout, prsheetout
        from XK_PRSHEET_TREE_OPERS (:prsheet, :refout)
        into refout, complexout, nameout, properout, numberout, prsheetout
      do begin
        if (unreported is null) then unreported = 0;
        suspend;
      end
    end else
    begin
      if (unreported is null) then unreported = 0;
      suspend;
      tooperout = refout;
      for
        select m.subsheet, m.unreported
          from prshmat m
          where m.opermaster = :tooperout and m.stopcascade = 1 and m.subsheet is not null
          into subsheet, unreported
      do begin
        if (unreported is null) then unreported = 0;
        for select refout, complexout, nameout, properout, numberout, prsheetout, unreported, tooperout
          from XK_PRSHEET_TREE_OPERS (:subsheet, null)
          into refout, complexout, nameout, properout, numberout, prsheetout, tmp, tooperoutcur
        do begin
          tooperoutcur2 = tooperout;
          if(tooperoutcur is not null) then tooperout = tooperoutcur;
          if (tmp = 1 and unreported = 0) then
          begin
            unreported = 1;
            suspend;
            unreported = 0;
          end
          else begin
            suspend;
            tooperout = tooperoutcur2;
          end
        end
      end
      unreported = 0;
      tooperout = null;
    end
  end
end^
SET TERM ; ^
