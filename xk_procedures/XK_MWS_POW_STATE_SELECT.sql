--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_POW_STATE_SELECT(
      WH varchar(3) CHARACTER SET UTF8                           ,
      CALCALL smallint)
  returns (
      REF integer,
      PRIORITY integer,
      DESCRIPTION varchar(1024) CHARACTER SET UTF8                           ,
      VAL varchar(1024) CHARACTER SET UTF8                           ,
      COLINDEX smallint,
      ACTIONNAME varchar(60) CHARACTER SET UTF8                           ,
      GROUPSTATE varchar(20) CHARACTER SET UTF8                           ,
      RIGHTS varchar(255) CHARACTER SET UTF8                           ,
      PARAM0 varchar(255) CHARACTER SET UTF8                           ,
      PARAM1 varchar(255) CHARACTER SET UTF8                           ,
      GROUPSTATESORT integer,
      MODULE varchar(255) CHARACTER SET UTF8                           ,
      OPERRIGHTS varchar(255) CHARACTER SET UTF8                           ,
      OPERRIGHTSGR varchar(255) CHARACTER SET UTF8                           )
   as
declare variable cnt integer;
declare variable tmpint integer;
declare variable allpos integer;
declare variable notblocked integer;
declare variable weight numeric(14,4);
declare variable vol numeric(14,4);
begin
  if (calcall is null) then calcall = 0;
  ref = 0;
  -- WMS Monitor
  -- godzina naliczenia
  groupstate = :wh;
  groupstatesort = 1;
  ref = ref + 1;
  priority = 1;
  colindex = 0;
  description = 'Naliczono ';
  actionname = 'MonitorWMSAnal'||ref;
  select difference from TIMESTAMP_DIFF_TOSTRING(current_date,current_timestamp(0))
    into val;
  suspend;
  -- ilosc lini do naszykowania
  ref = ref + 1;
  param0 = '';
  param1 = '';
  description = 'lini do szyk.';
  priority = 1;
  colindex = 0;
  actionname = 'MonitorWMSAnal'||ref;
  cnt = 0;
  val = '';
  select count(a.ref)
    from mwsacts a
      left join mwsords o on (o.ref = a.mwsord)
    where o.wh = :wh
      and a.mwsordtypedest = 1
      and o.status < 5
      and a.status < 3
      and a.status > 0
      and o.takefullpal = 0
      and o.regtime > current_date - 1000
    into cnt;
  if (cnt < 0) then cnt = 0;
  val = ''||cnt||'';
  suspend;
  -- ilosc lini do naszykowania
  ref = ref + 1;
  param0 = '';
  param1 = '';
  description = 'lini do szyk. odb.';
  priority = 1;
  colindex = 0;
  actionname = 'MonitorWMSAnal'||ref;
  cnt = 0;
  val = '';
  select count(a.ref)
    from mwsacts a
      left join mwsords o on (o.ref = a.mwsord)
      left join dokumnag d on (a.docid = d.ref)
    where o.wh = :wh
      and a.mwsordtypedest = 1
      and d.sposdost = 1
      and o.status < 5
      and a.status < 3
      and a.status > 0
      and o.takefullpal = 0
      and o.regtime > current_date - 1000
    into cnt;
  if (cnt < 0) then cnt = 0;
  val = ''||cnt||'';
  suspend;
  -- ilosc zleceń do szykowania
  ref = ref + 1;
  param0 = '';
  param1 = '';
  description = 'Zlec. do szyk.';
  priority = 1;
  colindex = 0;
  actionname = 'MonitorWMSAnal'||ref;
  cnt = 0;
  val = '';
  select count(o.ref)
    from mwsords o 
    where o.wh = :wh
      and o.mwsordtypedest = 1
      and o.status < 5
      and o.regtime > current_date - 100
    into cnt;
  if (cnt < 0) then cnt = 0;
  val = ''||cnt||'';
  suspend;
  -- ilosc lini do naszykowania
  ref = ref + 1;
  param0 = '';
  param1 = '';
  description = 'Zreal. linii';
  priority = 1;
  colindex = 0;
  actionname = 'MonitorWMSAnal'||ref;
  cnt = 0;
  val = '';
  select count(a.ref), sum(a.weight), sum(a.volume)
    from mwsacts a
    where a.wh = :wh
      and a.mwsordtypedest = 1
      and a.status = 5
      and a.timestop > current_date and a.timestop < current_date + 1
    into cnt, weight, vol;
  if (cnt < 0) then cnt = 0;
  val = ''||cnt||'';
  suspend;
  -- ilosc lini do naszykowania
  ref = ref + 1;
  param0 = '';
  param1 = '';
  description = 'Zreal. Waga/Obj.';
  priority = 1;
  colindex = 0;
  actionname = 'MonitorWMSAnal'||ref;
  cnt = 0;
  if (weight is null) then weight = 0;
  if (vol is null) then vol = 0;
  val = cast(weight as integer)||' / '||vol;
  suspend;
  -- ilosc oczekujacych lini na DT na spedycje
  ref = ref + 1;
  description = 'linii oczek. sped.';
  priority = 1;
  colindex = 0;
  actionname = 'MonitorWMSAnal'||ref;
  val = '';
  select count(m.ref)              -- 0ms
    from mwsgoodsrefill m
      left join sposdost s on (s.ref = m.shippingtype)
    where m.wh = :wh and (s.listywys is null or s.listywys = 1)
    into allpos;
  if (allpos < 0) then allpos = 0;
  select count(m.ref)
    from mwsgoodsrefill m
      left join dokumnag d on (d.ref = m.docid)
      left join sposdost s on (s.ref = m.shippingtype)
    where m.wh = :wh and (d.blokada <> 5 and d.blokada <> 1)
      and (s.listywys is null or s.listywys = 1)
      and (cast(d.termdost as date) <= current_date or d.termdost is null)
      and m.preparegoods > 0
    into notblocked;
  if (notblocked < 0) then notblocked = 0;
  val = notblocked||'/'||allpos;
  suspend;
    -- ilosc oczekujacych lini na DT na odbiór wlasny
  ref = ref + 1;
  description = 'linii oczek. odb. wł.';
  priority = 1;
  colindex = 0;
  actionname = 'MonitorWMSAnal'||ref;
  val = '';
  select count(m.ref)              -- 0ms
    from mwsgoodsrefill m
      left join sposdost s on (s.ref = m.shippingtype)
    where m.wh = :wh and s.listywys = 0
    into allpos;
  if (allpos < 0) then allpos = 0;
  select count(m.ref)
    from mwsgoodsrefill m
      left join dokumnag d on (d.ref = m.docid)
      left join sposdost s on (s.ref = m.shippingtype)
    where m.wh = :wh and (d.blokada <> 5 and d.blokada <> 1)
      and s.listywys = 0
      and (cast(d.termdost as date) <= current_date or d.termdost is null)
      and m.preparegoods > 0
    into notblocked;
  val = notblocked||'/'||allpos;
  suspend;
  -- ilosc lokacji poborowych wolnych na magazynie
  ref = ref + 1;
  description = 'Il. wolnych pob.';
  priority = 1;
  colindex = 0;
  actionname = 'MonitorWMSAnal'||ref;
  val = '';
  cnt = 0;
  select count(f.ref)           --31ms
    from mwsfreemwsconstlocs cd
      left join mwsconstlocs f on (f.ref = cd.ref)
    where f.act > 0 and f.goodsav = 1 and cd.wh = :wh
    into cnt;
  if (cnt is null) then
    val = val||'0';
  else
    val = val||cnt;
  suspend;
  -- ilosc lokacji wolnych na magazynie
  ref = ref + 1;
  description = 'Il. wolnych lok.';
  priority = 1;
  colindex = 0;
  actionname = 'MonitorWMSAnal'||ref;
  val = '';
  cnt = 0;
  select count(f.ref)           --31ms
    from mwsfreemwsconstlocs cd
      left join mwsconstlocs f on (f.ref = cd.ref)
    where f.act > 0 and cd.wh = :wh
    into cnt;
  if (cnt is null) then
    val = val||'0';
  else
    val = val||cnt;
  suspend;
  -- ilosc lokacji ktore zostana zwolnione
  cnt = 0;
  ref = ref + 1;
  tmpint = null;
  for
    select m.mwsconstloc
      from mwsstock m
        join mwsconstlocs c on (c.ref = m.mwsconstloc)
      where m.mwsconstloc is not null and c.wh = :wh
        and c.goodsav = 1 and c.act = 1
      group by m.mwsconstloc
      having sum(m.quantity) = sum(m.blocked)
      into tmpint
  do begin
    cnt = cnt + 1;
  end
  if (cnt > 0) then
  begin
    actionname = 'MonitorWMSAnal'||ref;
    description = 'lokacje do zwol.';
    priority = 1;
    colindex = 0;
    val = cnt;
    suspend;
  end
  -- Ilosc zlecen WT w realizacji
  ref = ref + 1;
  description = 'WT w real';
  actionname = 'MonitorWMSAnal'||ref;
  priority = 1;
  colindex = 0;
  val = '';
  select count(mo.ref)
    from mwsords mo
    where mo.wh = :wh
      and mo.mwsordtypedest = 1
      and mo.status = 2
      and cast(mo.regtime as date) >= current_date -5
    into val;
  suspend;
  -- Ilosc zlecen WG/WGO
  ref = ref + 1;
  description = 'Il. WG do real.';
  actionname = 'MonitorWMSAnal'||ref;
  priority = 1;
  colindex = 0;
  val = '';
  cnt = 0;
  select count(ma.ref)              --15ms
    from mwsords mo
      left join mwsacts ma on (ma.mwsord = mo.ref)
    where mo.wh = :wh
      and mo.mwsordtypedest = 9
      and ma.status < 3
      and ma.status > 0
      and mo.status < 3
      and mo.status > 0
      and mo.regtime > current_date - 30
    into cnt;
  if (cnt is null) then cnt = 0;
  val = cnt;
  suspend;
  -- Ilosc niezakończonych zleceń PZ
  ref = ref + 1;
  description = 'Otwarte PT';
  actionname = 'MonitorWMSAnal'||ref;
  priority = 1;
  colindex = 0;
  val = '';
  select count(mo.ref)    --15ms
    from mwsords mo
    where mo.wh = :wh
    and mo.mwsordtypedest = 2
    and mo.status = 2
    into val;
  suspend;
  -- Ilosc niezakończonych zleceń PZ do wyjanienia
  ref = ref + 1;
  description = 'PZ - do wyj.';
  actionname = 'MonitorWMSAnal'||ref;
  priority = 1;
  colindex = 0;
  val = '';
  select count(mo.ref)    --15ms
    from mwsords mo
    where mo.wh = :wh
    and mo.mwsordtypedest = 2
    and mo.status in (3,4)
    into val;
  suspend;
  -- ilosc lokacji do inwentaryzacji
  ref = ref + 1;
  description = 'Il. IL suma/w real.';
  actionname = 'MonitorWMSAnal'||ref;
  priority = 1;
  colindex = 0;
  val = '';
  select count(ref)    --15ms
    from mwsords
    where wh = :wh
      and mwsordtypedest = 15
      and status < 3 and status > 0
    into val;
  select count(mo.ref)||'/'||:val    --15ms
    from mwsords mo
    where mo.wh = :wh
      and mo.mwsordtype = 15
      and mo.status < 6 and mo.status > 0
      and cast(mo.regtime as date) = current_date
    into val;
  suspend;
  -- ilosc lok. inent
  ref = ref + 1;
  description = 'Il. na lok. inw.';
  actionname = 'MonitorWMSAnal'||ref;
  priority = 1;
  colindex = 0;
  val = '';
  select count(s.ref)         --16ms
    from mwsconstlocs c
      left join mwsstock s on (s.mwsconstloc = c.ref)
    where s.quantity - s.blocked + s.ordered <> 0
      and c.wh = :wh and c.locdest = 6
    into val;
  suspend;
  ref = ref + 1;
  description = 'Zalogowanych. na HH';
  priority = 1;
  colindex = 0;
  actionname = 'MonitorWMSAnal'||ref;
  val = '';
  select count(o.nazwa)
    from mwsaccessoryhist m
      left join operator o on(m.operator = o.ref)
      left join mwsaccessories ma on(m.mwsaccessory = ma.ref)
    where m.timestartto is null
    into val;
  suspend;
  -- ilosc MG
  ref = ref + 1;
  description = 'Il. MG do real.';
  actionname = 'MonitorWMSAnal'||ref;
  priority = 1;
  colindex = 0;
  val = '';
  select count(ref)    --15ms
    from mwsords
    where wh = :wh
      and mwsordtypedest = 11
      and status < 3 and status > 0
    into val;
  suspend;
  -- ilosc towarów na magazynie
  ref = ref + 1;
  description = 'Ilosc towarów';
  actionname = 'MonitorWMSAnal'||ref;
  priority = 1;
  colindex = 0;
  val = '';
  select count(distinct vers)    --15ms
    from mwsstock s
    where s.wh = :wh
    into val;
  suspend;
/*
  -- ilosc DT
  ref = ref + 1;
  description = 'Ilosc DT/OT';
  actionname = 'MonitorWMSAnal'||ref;
  priority = 1;
  colindex = 0;
  val = '';
  select count(ref)    --15ms
    from mwsords
    where wh = :wh and mwsordtypedest = 3 and status < 3 and status > 0
    into val;
  suspend;
*/
  --ilosc towaru na lokacjach nieaktywnych
  ref = ref + 1;
  description = 'Tow. nieakt. lok.';
  actionname = 'MonitorWMSAnal'||ref;
  priority = 1;
  colindex = 0;
  val = '';
  select count(s.vers)
    from mwsstock s
    where s.actloc = 0
      and s.mwsconstloc <> 80
  into :val;
  suspend;
end^
SET TERM ; ^
