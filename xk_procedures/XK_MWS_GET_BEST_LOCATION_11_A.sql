--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_BEST_LOCATION_11_A(
      WH varchar(3) CHARACTER SET UTF8                           ,
      VERSIN integer)
  returns (
      VERS integer,
      QUANTITY numeric(14,4),
      DAYSELL numeric(14,4))
   as
begin
  for
    select x.vers, sum(case when c.goodsav in (1,2) then s.quantity + s.ordered - s.blocked else 0 end),
        x.quantity / case when x.interval is not null and x.interval > 0 then x.interval else 1 end
      from xk_goods_relocate x
        left join mwsstock s on (s.wh = x.wh and s.vers = x.vers)
        left join mwsconstlocs c on (c.ref = s.mwsconstloc)
      where x.wh = :wh and (x.vers = :versin or :versin is null)
      group by x.vers, x.interval, x.quantity, c.goodsav
      into vers, quantity, daysell
  do begin
    suspend;
  end
end^
SET TERM ; ^
