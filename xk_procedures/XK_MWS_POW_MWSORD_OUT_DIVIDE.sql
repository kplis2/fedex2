--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_POW_MWSORD_OUT_DIVIDE(
      WH DEFMAGAZ_ID)
   as
declare variable mwsorddivide smallint_id;
declare variable mwsordtype mwsordtypes_id;
declare variable multi smallint_id;
declare variable ordcnt smallint_id;
declare variable cnt smallint_id;
declare variable mwsord mwsords_id;
declare variable ordmulti smallint_id;

begin
  select d.mwsorddivide from defmagaz d where d.symbol = :wh
  into :mwsorddivide;

  for
    select t.ref, m.multipicking
      from mwsordtypes t
        join mwsordtypes4wh m on (t.ref = m.mwsordtype and m.wh = :wh)
      where t.mwsortypedets = 1  --tylko zlecenia wydania
    into :mwsordtype, :multi
  do begin
    for
      select o.ref, o.multi
        from mwsords o
        where o.mwsordtype = :mwsordtype
          and o.wh = :wh
          and o.status = 7
        order by o.regtime, o.ref
      into :mwsord, :ordmulti
    do begin
      if (:mwsorddivide > 0) then
        execute procedure xk_mws_mwsord_whsec_divide (:mwsord);
      if (:multi > 0) then
        execute procedure xk_mws_mwsord_multi_divide (:mwsord);
    end
  end
  suspend;
end^
SET TERM ; ^
