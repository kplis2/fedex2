--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_PROPERPARAM_DECODE(
      PRPARAMSTRING varchar(255) CHARACTER SET UTF8                           )
  returns (
      EMPLOYEE integer,
      PRSCHEDOPER integer,
      PRSCHEDGUIDE integer,
      PRNAGZAM integer,
      PRPOZZAM integer,
      PROPER varchar(20) CHARACTER SET UTF8                           ,
      PRSHORTAGETYPE varchar(20) CHARACTER SET UTF8                           )
   as
declare variable tmp varchar(80);
declare variable pos integer;
begin
  prnagzam = 0;
  prpozzam = 0;
  prschedguide = 0;
  proper = '';
  prschedoper = 0;
  prshortagetype = '';
  employee = 0;
  if (prparamstring is null or prparamstring = '') then
    exception propersrap_error;
  -- najpierw naglowej zamowienie
  pos = position(';' in prparamstring);
  if (pos > 0) then
  begin
    tmp = substring(prparamstring from 1 for pos - 1);
    prparamstring = substring(prparamstring from pos + 1 for 255);
    if (tmp is not null and tmp <> '') then
      prnagzam = cast(tmp as integer);
    else
      prnagzam = 0;
  end
  else begin
    prnagzam = cast(prparamstring as integer);
    prparamstring = '';
    exit;
  end
  -- potem pozycja zamowienia
  pos = position(';' in prparamstring);
  if (pos > 0) then
  begin
    tmp = substring(prparamstring from 1 for pos - 1);
    prparamstring = substring(prparamstring from pos + 1 for 255);
    if (tmp is not null and tmp <> '') then
      prpozzam = cast(tmp as integer);
    else
      prpozzam = 0;
  end
  else begin
    prpozzam = cast(prparamstring as integer);
    prparamstring = '';
    exit;
  end
  -- potem przewodnik
  pos = position(';' in prparamstring);
  if (pos > 0) then
  begin
    tmp = substring(prparamstring from 1 for pos - 1);
    prparamstring = substring(prparamstring from pos + 1 for 255);
    if (tmp is not null and tmp <> '') then
      prschedguide = cast(tmp as integer);
    else
      prschedguide = 0;
  end
  else begin
    prschedguide = cast(prparamstring as integer);
    prparamstring = '';
    exit;
  end
  -- potem typ operacji
  pos = position(';' in prparamstring);
  if (pos > 0) then
  begin
    tmp = substring(prparamstring from 1 for pos - 1);
    prparamstring = substring(prparamstring from pos + 1 for 255);
    if (tmp is not null and tmp <> '') then
      proper = tmp;
    else
      proper = '';
  end
  else begin
    proper = prparamstring;
    prparamstring = '';
    exit;
  end
  -- potem operacja
  pos = position(';' in prparamstring);
  if (pos > 0) then
  begin
    tmp = substring(prparamstring from 1 for pos - 1);
    prparamstring = substring(prparamstring from pos + 1 for 255);
    if (tmp is not null and tmp <> '') then
      prschedoper = cast(tmp as integer);
    else
      prschedoper = 0;
  end
  else begin
    prschedoper = cast(prparamstring as integer);
    prparamstring = '';
    exit;
  end
  -- potem typ braku
  pos = position(';' in prparamstring);
  if (pos > 0) then
  begin
    tmp = substring(prparamstring from 1 for pos - 1);
    prparamstring = substring(prparamstring from pos + 1 for 255);
    if (tmp is not null and tmp <> '') then
      prshortagetype = tmp;
    else
      prshortagetype = '';
  end
  else begin
    prshortagetype = prparamstring;
    prparamstring = '';
    exit;
  end
  -- potem employ
  pos = position(';' in prparamstring);
  if (pos > 0) then
  begin
    tmp = substring(prparamstring from 1 for pos - 1);
    prparamstring = substring(prparamstring from pos + 1 for 255);
    if (tmp is not null and tmp <> '') then
      employee = cast(tmp as integer);
    else
      employee = 0;
  end
  else begin
    employee = cast(prparamstring as integer);
    prparamstring = '';
    exit;
  end
end^
SET TERM ; ^
