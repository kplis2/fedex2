--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSORDOUT_CLOSE_CHECK(
      MWSORD integer)
  returns (
      COMMITMWSCONSTLOCL smallint,
      COMMITMWSCONSTLOCP smallint,
      MWSCONSTLOCLLIST varchar(1024) CHARACTER SET UTF8                           ,
      EMWSCONSTLOC SMALLINT_ID,
      PRINTER varchar(40) CHARACTER SET UTF8                           )
   as
declare variable tmp varchar(40);
declare variable pom varchar(40);
declare variable docgroup integer;
begin
  printer = '';
  commitmwsconstlocl = 1;
  commitmwsconstlocp = 1;
  mwsconstlocllist = '';
  emwsconstloc = 0;

  select coalesce(o.docgroup, 0)
    from mwsords o
    where o.ref = :mwsord
    into docgroup;
  if (docgroup > 0) then
  begin
    for
      select coalesce(a.emwsconstloc,'')
        from mwsords o
          left join mwsacts a on (a.mwsord = o.ref)
        where o.docgroup = :docgroup and a.status = 5
        group by coalesce(a.emwsconstloc,'')
        order by max(a.timestop) desc
        into tmp
    do begin
      if (tmp <> '') then
      begin
        if (mwsconstlocllist <> '') then
          mwsconstlocllist = mwsconstlocllist||' ';
         mwsconstlocllist = mwsconstlocllist||tmp;
      end
    end
  end

  if (not exists(select first 1 1 from mwsacts a where a.mwsord = :mwsord and a.status > 0 and a.status < 5) and
      exists(select first 1 1 from mwsacts a where a.mwsord = :mwsord and a.status = 5)
  ) then
    emwsconstloc = 1;
end^
SET TERM ; ^
