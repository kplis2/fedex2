--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_PRORD_OPERATIONS(
      NAGZAMREF integer)
  returns (
      OPERNAME varchar(255) CHARACTER SET UTF8                           ,
      WORKPLACECODE varchar(80) CHARACTER SET UTF8                           ,
      NUMBER integer)
   as
--declare variable number integer;
declare variable TYPZAM varchar(3);
begin

  for select pro.number, pro.oper, n.ref || ';;;'|| pro.oper
    from nagzam n
    join  prschedguides prs on (n.ref = prs.zamowienie)
    join  prschedopers pro on (pro.guide = prs.ref)
    where n.ref = :nagzamref
    order by pro.number
    into number, opername, workplacecode
  do suspend;
end^
SET TERM ; ^
