--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_PZ_COMMITQUANTITY(
      MWSACTREF integer,
      MWSORDREF integer,
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      LOT integer,
      ILOSCPOTW numeric(15,4),
      CLOSEPALLOC smallint,
      PALLGROUPIN integer,
      MWSPALLOCSIZE integer,
      MANMWSACTS smallint,
      WHAFTER varchar(3) CHARACTER SET UTF8                           ,
      AKTUOPERATOR integer,
      MWSCONSTLOCLIN integer = 0,
      X_SERIA STRING255 = '',
      X_PARTIA STRING20 = '',
      X_SLOWNIK_REF INTEGER_ID = null)
  returns (
      PALGROUP integer,
      COUNTGOOD integer,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable PALLREF integer;
declare variable ILOSC numeric(15,4);
declare variable STATUS smallint;
declare variable DOCID integer;
declare variable DOCTYPE varchar(1);
declare variable CNTGOODALL integer;
declare variable MWSPALLOCL integer;
declare variable NEWMIXPALLOC integer;
declare variable NEWMIXPAL integer;
declare variable MWSPALLOCREF integer;
declare variable MWSPALLOCFORPAL integer;
declare variable WHAREA integer;
declare variable POZ integer;
declare variable H numeric(14,4);
declare variable L numeric(14,4);
declare variable MWSORDOUT smallint;
declare variable OUTREF integer;
declare variable MAXH numeric(14,4);
declare variable MAXWEIGHT numeric(14,4);
declare variable PALPACKMETHSHORT varchar(30);
declare variable PALPACKMETH varchar(40);
declare variable PALPACKQUANTITY numeric(14,4);
declare variable MAXSCALER numeric(14,4);
declare variable OUTSYMBOL varchar(40);
declare variable OUTPALREF integer;
declare variable DOCPOSID integer;
declare variable WH varchar(3);
declare variable MAINMWSACT integer;
declare variable TOINSERT numeric(14,4);
declare variable QUANMAIN numeric(14,4);
declare variable MWSORDTYPE integer;
declare variable MWSCONSTLOCL integer;
declare variable TYMMWSPALLOCL integer;
declare variable WHAREAL integer;
declare variable WHAREALOGL integer;
declare variable REFILL integer;
declare variable MIX integer;
declare variable MSGSTATUS smallint;
declare variable MIEDZY smallint;
declare variable MSGREF integer;
declare variable SEGTYPE integer;
declare variable LOTTMP integer;
declare variable DOKMAGTYP varchar(3);
declare variable ORDDOCID integer;
declare variable ORDSTATUS smallint;
declare variable UNITP integer;
declare variable FROMMWSACT integer;
declare variable CORTOMWSACT integer;
declare variable X_SERIE_REF integer_id;
declare variable havefake_vers wersje_id;
declare variable mwsconstlocltmp integer;
declare variable in_shelves smallint_id;
begin
---------------------------
  msg = '';
  msgstatus = 0;
  if (iloscpotw = 0) then exception MWS_ZERO_COMMITED;

  if (x_partia = '') then x_partia = null; -- XXX KBI

  --odczytanie rozmiarów palety
  select l, h, segtype
    from mwspallocsizes
    where ref = :mwspallocsize
    into l, h, segtype;
  select mo.docid, mo.doctype, mo.wh, mo.mwsordtype, mo.status
    from mwsords mo
    where mo.ref = :mwsordref
    into :docid, :doctype, :wh, :mwsordtype, :ordstatus;
  --MSt: Zmiany dla ArgoCard
  frommwsact = :mwsactref;
  select unitp from mwsacts where ref = :mwsactref into :unitp;
  if (:ordstatus = 0) then
    exception universal 'Nie zaakceptowano zlecenia.';
  orddocid = docid;
  if (whafter = wh) then whafter = null;
  if (pallgroupin = 0) then begin
    execute procedure gen_ref('MWSPALLOCS') returning_values mwspallocref;
    insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status,
        fromdocid, fromdocposid, fromdoctype, segtype, manmwsacts)
      values(:mwspallocref,cast(:mwspallocref as varchar(20)), null, 2, 0, :docid, :docposid, :doctype, :segtype,
      case when coalesce(:manmwsacts,0) = 1 then 1 else 0 end);
  end else begin
    mwspallocref = :pallgroupin;
    select manmwsacts from mwspallocs where ref = :mwspallocref
    into :manmwsacts;
  end
  palgroup = :mwspallocref;
/*  -- sprawdzenie czy juz drukowano etykiete
  if (exists(select first 1 a.ref from mwsacts a where a.mwsord = :mwsordref
      and a.mwspallocl = :palgroup and a.status = 2)
  ) then
    printer = '';
  else
    printer = defaultprinter;    */
    -- szukamy lokacji gleba dla magzynu
   execute procedure XK_MWS_GET_BEST_LOCATION(:good, :vers,null,:mwsordref,:mwsordtype,
        :wh, null, null, 3, 1, 0, 0,null,null, 120,:l,:h,0,null)
      returning_values (mwsconstlocltmp, tymmwspallocl, whareal, wharealogl, refill);


  if (coalesce(mwsconstloclin,0) = 0) then
    mwsconstlocl = mwsconstlocltmp;
  else begin
    mwsconstlocl = mwsconstloclin;
  end

  -- w parametrach poczatkowych wskazana byla lokacja oznaczac to moze ze towar byl przyjomwy od razu rozwozony w polki
  if (coalesce(mwsconstloclin,0) <> 0 and  mwsconstloclin <> mwsconstlocltmp) then begin
    in_shelves = 1;
  end  else
    in_shelves = 0;

  lottmp = null;
  for
    select ma.docposid, ma.ref, ma.quantity, ma.lot, ma.cortomwsact
      from mwsacts ma
      where ma.mwsord = :mwsordref
        and ma.vers = :vers
        and ma.status = 0
        and ma.quantity > 0
        and ma.docid is not null
      into :docposid, :mainmwsact, :quanmain, :lottmp, :cortomwsact
  do begin
    if (coalesce(lottmp,0) = 0) then
      exception universal 'Brak okreslonej dostawy na dokumencie magazynowym';
    if (msgstatus = 0) then
      msgstatus = 1;
    if (quanmain < iloscpotw) then begin
      toinsert = :quanmain;
      iloscpotw = iloscpotw - quanmain;
    end else begin
      toinsert = :iloscpotw;
      iloscpotw = 0;
    end
    if (toinsert > 0) then begin
      execute procedure gen_ref('MWSACTS') returning_values mwsactref;
--<<XXX MatJ Wdr   

    execute procedure GEN_REF('DOSTAWY')
      returning_values lottmp;
    insert into DOSTAWY(ref, DOSTAWCA, DATA, STATUS, MAGAZYN, DATAWAZN, REFDOKUMNAG)
      values (:lottmp, null, current_date, 'Z', :wh, null, null);

    if (coalesce(x_seria,'') <> '') then
      insert into X_MWS_SERIE (ktm,  WERSJAREF, SERIALNO, DATAPRZYJECIA, DOSTAWA, "USER", MWSORDIN, WMSAKTIN,
                               TYP )
      values (:good,  :vers, :x_seria, current_timestamp, :lottmp, :AKTUOPERATOR, :MWSORDREF, :mwsactref,
              ( select x_mws_serie from wersje where ref=:vers) )
      returning REF
      into :X_SERIE_REF ;
      
    select first 1 n.wersjaref
      from kplpoz p
      join kplnag n on (p.nagkpl=n.ref)
    where p.wersjaref=:vers
    into :havefake_vers;
--XXX>>
      insert into mwsacts (ref, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
            mwsconstlocl, mwspallocl, doctype, settlmode, closepalloc, wh,
            regtime, timestartdecl, timestopdecl,  lot, PALGROUP, ISPAL, MIXEDPALLGROUP, RECDOC, docid, h, l,
            docposid, wh2, mwspallocsize, segtype, unitp, frommwsact, cortomwsact           
--<<XXX MatJ Wdr
            , x_partia, x_serial_no, x_slownik, x_havefake_vers, x_inshelves)
--XXX>>
        values (:mwsactref, :MWSORDREF, 0, 0, :good, :vers, :toinsert, null, null,
                :mwsconstlocl, :mwspallocref, :doctype, null, 0, :wh, current_timestamp(0), current_timestamp(0),
              current_timestamp(0), :lottmp, :mwspallocref, 0, 0, 1, :docid, :h, :l,
              :docposid, :whafter, :mwspallocsize, :segtype, :unitp, :mainmwsact, :cortomwsact        
--<<XXX MatJ Wdr
              , cast(:x_partia as date),:X_SERIE_REF, :x_slownik_ref, :havefake_vers, :in_shelves);
--XXX>>
      update mwsacts m set m.status = 1 where m.ref = :mwsactref;
      update mwsacts m set m.status = 2, m.quantityc = m.quantity where m.ref = :mwsactref;
--      update mwsacts m set m.quantity = m.quantity - :toinsert where m.ref = :mainmwsact;
    end
    if (iloscpotw <= 0) then break;
  end
  lottmp = null;
  if (iloscpotw > 0) then
  begin
    select d.dostawa, d.typ, coalesce(m.miedzy,0)
      from mwsords o
        join dokumnag d on (d.ref = o.docid)
        left join defdokum m on (m.symbol = d.typ)
      where o.ref = :mwsordref
      into lottmp, :dokmagtyp, miedzy;
    if (coalesce(lottmp,0) = 0 and miedzy = 0) then
      exception universal 'Brak okreslonej dostawy na dokumencie magazynowym';
    select first 1 ref, checked
      from mwsacts
      where mwsord = :mwsordref and vers = :vers and status = 0
      order by checked desc
      into msgref, msgstatus;
    if (msgstatus is null) then msgstatus = 0;
    if (orddocid is not null and msgstatus = 0 and msgref is not null) then
    begin
      msg = 'Przekroczono ilość z dokumentu przyjęcia';
      update mwsacts set checked = 1 where ref = :msgref;
    end
    execute procedure gen_ref('MWSACTS') returning_values mwsactref;
--<<XXX MatJ Wdr

    execute procedure GEN_REF('DOSTAWY')
      returning_values lottmp;
    insert into DOSTAWY(ref, DOSTAWCA, DATA, STATUS, MAGAZYN, DATAWAZN, REFDOKUMNAG)
      values (:lottmp, null, current_date, 'Z', :wh, null, null);

    if (coalesce(x_seria,'') <> '') then
      insert into X_MWS_SERIE (ktm,  WERSJAREF, SERIALNO, DATAPRZYJECIA, DOSTAWA, "USER", MWSORDIN, WMSAKTIN,
                               TYP )
      values (:good,  :vers, :x_seria, current_timestamp, :lottmp, :AKTUOPERATOR, :MWSORDREF, :mwsactref,
              ( select x_mws_serie from wersje where ref=:vers) )
      returning REF
      into :X_SERIE_REF ;

    select first 1 n.wersjaref
      from kplpoz p
      join kplnag n on (p.nagkpl=n.ref)
    where p.wersjaref=:vers
    into :havefake_vers;
--XXX>>
    insert into mwsacts (ref, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
          mwsconstlocl, mwspallocl, doctype, settlmode, closepalloc, wh,
          regtime, timestartdecl, timestopdecl,  lot, PALGROUP, ISPAL, MIXEDPALLGROUP, RECDOC,
          docid, h, l, wh2, mwspallocsize, segtype, unitp, frommwsact           
--<<XXX MatJ Wdr
            , x_partia, x_serial_no, x_slownik,  x_havefake_vers, x_inshelves)
--XXX>>
      values (:mwsactref, :MWSORDREF, 0, 0, :good, :vers, :ILOSCPOTW, null, null,
              :mwsconstlocl, :mwspallocref, :doctype, null, 0, :wh, current_timestamp(0), current_timestamp(0),
            current_timestamp(0), :lottmp, :mwspallocref, 0, 0, 1,
            :docid, :h, :l, :whafter, :mwspallocsize, :segtype, :unitp, :frommwsact
--<<XXX MatJ Wdr
              , cast(:x_partia as date),:X_SERIE_REF, :x_slownik_ref, :havefake_vers, :in_shelves);
--XXX>>
    update mwsacts m set m.status = 1 where m.ref = :mwsactref;
    update mwsacts m set m.status = 2, m.quantityc = m.quantity where m.ref = :mwsactref;
  end
  select count(m.good)
    from mwsacts m
      left join towary t on(t.ktm = m.good)
    where m.palgroup = :mwspallocref
      and m.mwsord = :mwsordref
      and t.paleta <> 1
    into :cntgoodall;
  if (cntgoodall > 1) then
    update mwsacts m set m.mixedpallgroup = 1 where m.palgroup = :mwspallocref;
  else
    update mwsacts m set m.mixedpallgroup = 0 where m.palgroup = :mwspallocref;
  if (closepalloc = 1) then begin
    execute procedure xk_mws_pz_close_mwspalloc(:mwsordref, :mwspallocref,iif(manmwsacts = 1,2,1));
   -- printer = defaultprinter;
  end
  select count(m.good)
    from mwsacts m
    where m.palgroup = :pallref
      and m.status = 5
      and m.mwsord = :mwsordref
    into :countgood;
  select mo.bwharea
    from mwsords mo
    where mo.ref = :mwsordref
    into :wharea;
  --MSt: Zmiany dla ArgoCard
  if (:ordstatus = 1) then
    update mwsords set status = 2 where ref = :mwsordref;

 -- printer = 'RAMPA1';     -- Ldz test drukowania

  suspend;
end^
SET TERM ; ^
