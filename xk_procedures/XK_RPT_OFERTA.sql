--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_RPT_OFERTA(
      OFERTAREF integer)
  returns (
      INFO1 varchar(1024) CHARACTER SET UTF8                           ,
      INFO2 varchar(1024) CHARACTER SET UTF8                           ,
      INFO3 varchar(1024) CHARACTER SET UTF8                           ,
      INFOREGON varchar(1024) CHARACTER SET UTF8                           ,
      INFONIP varchar(1024) CHARACTER SET UTF8                           ,
      INFOREJ1 varchar(1024) CHARACTER SET UTF8                           ,
      INFOREJ2 varchar(1024) CHARACTER SET UTF8                           ,
      INFOREJ3 varchar(1024) CHARACTER SET UTF8                           ,
      NROFERTY varchar(40) CHARACTER SET UTF8                           ,
      DATA varchar(30) CHARACTER SET UTF8                           ,
      KLIENT varchar(255) CHARACTER SET UTF8                           ,
      OSOBAKONTAKT varchar(40) CHARACTER SET UTF8                           ,
      EMAIL varchar(40) CHARACTER SET UTF8                           ,
      FAX varchar(30) CHARACTER SET UTF8                           ,
      OFERTA varchar(255) CHARACTER SET UTF8                           ,
      PLATNOSC varchar(1024) CHARACTER SET UTF8                           ,
      DOSTAWA varchar(1024) CHARACTER SET UTF8                           ,
      GWARANCJA varchar(1024) CHARACTER SET UTF8                           ,
      SERWIS varchar(1024) CHARACTER SET UTF8                           ,
      TERMINPLAT varchar(1024) CHARACTER SET UTF8                           ,
      OPIS1 varchar(80) CHARACTER SET UTF8                           ,
      OPIS2 varchar(80) CHARACTER SET UTF8                           ,
      OPIS3 varchar(80) CHARACTER SET UTF8                           ,
      OPIS4 varchar(80) CHARACTER SET UTF8                           ,
      OPIS5 varchar(80) CHARACTER SET UTF8                           ,
      OPIS6 varchar(80) CHARACTER SET UTF8                           ,
      OPIS7 varchar(80) CHARACTER SET UTF8                           ,
      ILOSC1 varchar(19) CHARACTER SET UTF8                           ,
      ILOSC2 varchar(19) CHARACTER SET UTF8                           ,
      ILOSC3 varchar(19) CHARACTER SET UTF8                           ,
      ILOSC4 varchar(19) CHARACTER SET UTF8                           ,
      ILOSC5 varchar(19) CHARACTER SET UTF8                           ,
      ILOSC6 varchar(19) CHARACTER SET UTF8                           ,
      ILOSC7 varchar(19) CHARACTER SET UTF8                           ,
      CENA1 varchar(17) CHARACTER SET UTF8                           ,
      CENA2 varchar(17) CHARACTER SET UTF8                           ,
      CENA3 varchar(17) CHARACTER SET UTF8                           ,
      CENA4 varchar(17) CHARACTER SET UTF8                           ,
      CENA5 varchar(17) CHARACTER SET UTF8                           ,
      CENA6 varchar(17) CHARACTER SET UTF8                           ,
      CENA7 varchar(17) CHARACTER SET UTF8                           ,
      WART1 varchar(17) CHARACTER SET UTF8                           ,
      WART2 varchar(17) CHARACTER SET UTF8                           ,
      WART3 varchar(17) CHARACTER SET UTF8                           ,
      WART4 varchar(17) CHARACTER SET UTF8                           ,
      WART5 varchar(17) CHARACTER SET UTF8                           ,
      WART6 varchar(17) CHARACTER SET UTF8                           ,
      WART7 varchar(17) CHARACTER SET UTF8                           ,
      WARTALL varchar(17) CHARACTER SET UTF8                           ,
      WAL varchar(6) CHARACTER SET UTF8                           )
   as
declare variable opis varchar(255);
declare variable ilosc VARCHAR(19);
declare variable cena VARCHAR(17);
declare variable wart VARCHAR(17);
declare variable licz integer;
begin
  select k.wartosc from konfig k where k.akronim = 'INFO1' into info1;
  select k.wartosc from konfig k where k.akronim = 'INFO2' into info2;
  select k.wartosc from konfig k where k.akronim = 'INFO3' into info3;

  select k.wartosc from konfig k where k.akronim = 'INFOREGON' into inforegon;
  select k.wartosc from konfig k where k.akronim = 'INFONIP' into infonip;

  select k.wartosc from konfig k where k.akronim = 'INFOREJ1' into inforej1;
  select k.wartosc from konfig k where k.akronim = 'INFOREJ2' into inforej2;
  select k.wartosc from konfig k where k.akronim = 'INFOREJ3' into inforej3;

  select o.symbol, k.nazwa, 'P. '||uk.nazwa, k.email, k.fax, cast(cast(o.data as date) as varchar(30)),
         cast(o.sumwartnet as VARCHAR(17)), o.waluta
   from oferty o
    left join klienci k on (slodef = 1 and k.ref = o.slopos)
    left join uzykli uk on (uk.ref = k.limitspruzy)
  where o.ref=:ofertaref
  into :nroferty, :klient,  :osobakontakt, :email, :fax, :data, :wartall, :wal ;

   if (nroferty is null) then  nroferty = '';
   if (klient is null) then  klient = '';
   if (osobakontakt is null) then  osobakontakt = '';
   if (email is null) then  email = '';
   if (fax is null) then  fax = '';
   if (data is null) then  data = '';
   if (oferta is null) then  oferta = '';
   if (platnosc is null) then  platnosc = '';
   if (dostawa is null) then  dostawa = '';
   if (gwarancja is null) then  gwarancja = '';
   if (serwis is null) then  serwis = '';
   if (terminplat is null) then  terminplat = '';
   if (wartall is null) then  wartall = '';
   if (wal is null) then  wal = '';
   licz=1;
   opis1 = '';  ilosc1 = '';  cena1 = '';  wart1 = '';
   opis2 = '';  ilosc2 = '';  cena2 = '';  wart2 = '';
   opis3 = '';  ilosc3 = '';  cena3 = '';  wart3 = '';
   opis4 = '';  ilosc4 = '';  cena4 = '';  wart4 = '';
   opis5 = '';  ilosc5 = '';  cena5 = '';  wart5 = '';
   opis6 = '';  ilosc6 = '';  cena6 = '';  wart6 = '';
   opis7 = '';  ilosc7 = '';  cena7 = '';  wart7 = '';

  for select coalesce(op.nazwa,''), cast(coalesce(op.ilosc,'') as varchar(19)),
             cast(coalesce(op.cenacen,'') as varchar(17)), cast(coalesce(op.wartnet,'') as varchar(17))
      from oferpoz op
      where op.oferta = :ofertaref
      into :opis, :ilosc, :cena, :wart
  do begin
    if (licz = 1) then begin
      opis1 = opis; ilosc1 = ilosc; cena1 = cena; wart1 = wart;
    end else
    if (licz = 2) then begin
      opis2 = opis; ilosc2 = ilosc; cena2 = cena; wart2 = wart;
    end else
    if (licz = 3) then begin
      opis3 = opis; ilosc3 = ilosc; cena3 = cena; wart3 = wart;
    end else
    if (licz = 4) then begin
      opis4 = opis; ilosc4 = ilosc; cena4 = cena; wart4 = wart;
    end else
    if (licz = 5) then begin
      opis5 = opis; ilosc5 = ilosc; cena5 = cena; wart5 = wart;
    end else
    if (licz = 6) then begin
      opis6 = opis; ilosc6 = ilosc; cena6 = cena; wart6 = wart;
    end else
    if (licz = 7) then begin
      opis7 = opis; ilosc7 = ilosc; cena7 = cena; wart7 = wart;
    end
    licz = licz +1;
  end
  suspend;
end^
SET TERM ; ^
