--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_MWSORDIN_GEN_PALLOC(
      MWSORD integer,
      WHSEC integer,
      PALLOCIN integer,
      MANMWSACTS smallint)
  returns (
      PALLOC integer,
      RMANMWSACTS smallint)
   as
begin
  palloc = 0;
  if (not exists (select first 1 1 from mwspallocs p where p.status < 1 and p.ref = :pallocin)) then
  exit;
    --exception mwsord_except 'Zaczytaj inną etykiete';  --Ldz bąd adnie jest opisany w źródah hh
  update mwspallocs p set p.whsec = :whsec where whsec is null and p.ref = :pallocin;
  update mwspallocs p set p.manmwsacts = :manmwsacts where p.ref = :pallocin;
  palloc = pallocin;
  rmanmwsacts = manmwsacts;
end^
SET TERM ; ^
