--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_POW_PERIODIC_REFILL(
      WH varchar(3) CHARACTER SET UTF8                           ,
      WHSEC integer,
      MWSORDTYPE integer,
      MAXORDCNT integer,
      INTERVAL integer,
      DAYSSTOCK integer,
      MODE smallint)
   as
declare variable quantity numeric(14,4);
declare variable actquantity numeric(14,4);
declare variable daysell numeric(14,4);
declare variable restquantity numeric(14,4);
declare variable quantityleft numeric(14,4);
declare variable vers integer;
declare variable abc varchar(10);
declare variable wcategory varchar(10);
declare variable stop smallint;
declare variable refmwsord integer;
declare variable cnt integer;
begin
 if (interval is null or interval = 0) then
   interval = 14;
 if (daysstock is null or daysstock = 0) then
   daysstock = 7;
 -- uzupelniamy tylko towary z lokacji niepolkowych
 cnt = 0;
 for
   select r.vers, r.quantity, r.optabc, r.wcategory
     from xk_goods_relocate r
     where r.quantity > 0 and r.quantity is not null and r.wh = :wh
       and not exists(
         select m.ref from mwsstock m
           left join mwsconstlocs c on (c.ref = m.mwsconstloc)
         where m.wh = :wh and m.vers = r.vers and c.locdest <> 1 and c.act > 0)
     into vers, quantity, abc, wcategory
 do begin
   actquantity = 0;
   select sum(s.quantity - s.blocked + s.ordered)
     from mwsstock s
     where s.wh = :wh and s.vers = :vers and s.goodsav > 0 and s.actloc > 0
     into actquantity;
   if (actquantity is null) then actquantity = 0;
   -- sprawdzamy czy starczy towaru na tyle dni ile potrzeba
   daysell = cast(quantity / interval as integer);
   if (actquantity < daysell * daysstock) then
   begin
     -- trzeba uzupelnic lokacje poborowe o brakujaca ilosc
     restquantity = daysell * daysstock - actquantity;
     stop = 0;
     while (stop = 0 and restquantity > 0)
     do begin
       refmwsord = null;
       execute procedure xk_mws_pow_periodic_refill_vers(null,:vers,null,null,:mwsordtype,
           :wh,:whsec,null,null,null,null,null,null,null,null,null,null,null,:restquantity,:mode)
         returning_values (:stop, :refmwsord, :quantityleft);
       if (quantityleft is null) then quantityleft = 0;
       restquantity = quantityleft;
     end
     if (refmwsord is not null) then
       cnt = cnt + 1;
   end
   if (cnt >= maxordcnt) then
     exit;
 end
end^
SET TERM ; ^
