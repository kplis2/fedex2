--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_PR_PREPRODUCTION(
      CLIENT integer,
      MPKTM varchar(40) CHARACTER SET UTF8                           ,
      ORDTYPE varchar(3) CHARACTER SET UTF8                           ,
      ORDREG varchar(100) CHARACTER SET UTF8                           ,
      FROMDATES varchar(100) CHARACTER SET UTF8                           ,
      TODATES varchar(100) CHARACTER SET UTF8                           ,
      ORDLIST varchar(1024) CHARACTER SET UTF8                           ,
      STMT varchar(4096) CHARACTER SET UTF8                           ,
      GORDTYPE varchar(3) CHARACTER SET UTF8                           ,
      GORDREG varchar(100) CHARACTER SET UTF8                           ,
      PKTM varchar(40) CHARACTER SET UTF8                           ,
      SEPWERSJAREF smallint)
  returns (
      REFS varchar(1024) CHARACTER SET UTF8                           ,
      ORDSGROUP integer)
   as
declare variable statements varchar(1024);
declare variable ktm varchar(40);
declare variable wersjaref integer;
declare variable prshmat integer;
declare variable prschedguidepos integer;
declare variable ilosc numeric(14,4);
declare variable nref integer;
declare variable pref integer;
declare variable wh varchar(3);
declare variable prdepart varchar(3);
declare variable techjednperdays varchar(255);
declare variable techjednperday numeric(14,2);
declare variable techjednquantity numeric(14,4);
declare variable batchquantity numeric(14,4);
declare variable jedn integer;
declare variable prschedule integer;
begin
  refs = ';';
  if (sepwersjaref is null) then sepwersjaref = 0;
  ordreg = replace(ordreg, '.', ';');
  execute procedure PRGRUPAROZL_ID returning_values ordsgroup;
  execute procedure GETCONFIG('TECHJEDNQUANTITY') returning_values :techjednperdays;
  if(techjednperdays is not null and techjednperdays <> '') then techjednperday = cast(techjednperdays as numeric(14,2));
  else techjednperday = 8;
  if (client is null) then client = 0;
  if (mpktm is null) then mpktm = '';
  if (ordtype is null) then ordtype = '';
  if (ordreg is null) then ordreg = '';
  if (fromdates is null) then fromdates = '';
  if (todates is null) then todates = '';
  if (ordlist is null) then ordlist = '';
  if (stmt is null) then stmt = '';
  statements = 'select ktm, wersjaref, prshmat, prschedguidepos, ilosc, prdepart, wh '||
      'from XK_PR_PREPRODUCTION_LIST('||:client||','''||:mpktm||''','''||
      :ordtype||''','''||:ordreg||''','''||:fromdates||''','''||:todates||''','''||:ordlist||''')'||
      ' where '||:stmt;
  for
    execute statement statements
    into ktm, wersjaref, prshmat, prschedguidepos, ilosc, prdepart, wh
  do begin
    jedn = null;
    select m.jedn, s.batchquantity
      from prshmat m
        left join prsheets s on (s.ref = m.sheet)
      where m.ref = :prshmat
      into jedn, batchquantity;
    nref = null;
    if (sepwersjaref > 0) then
      select first 1 n.ref
        from nagzam n
          left join pozzam p on (p.zamowienie = n.ref)
        where n.grupazam = :ordsgroup and n.rejestr = :gordreg and n.typzam = :ordtype
          and p.ktm = :ktm and p.wersjaref = :wersjaref
        into nref;
    else
      select first 1 n.ref
        from nagzam n
          left join pozzam p on (p.zamowienie = n.ref)
        where n.grupazam = :ordsgroup and n.rejestr = :gordreg and n.typzam = :ordtype
        into nref;
    if (nref is null) then
    begin
      execute procedure gen_ref('NAGZAM') returning_values nref;
      insert into NAGZAM(REF, REJESTR, TYPZAM, MAGAZYN, DATAZM, DATAWE, TERMDOST, KINNE, PRDEPART, KKTM, GRUPAZAM, PRPREPROD,UWAGIWEWN)
        values(:nref, :gordreg, :ordtype, :wh, current_date, current_date, current_date,1, :prdepart, :PKTM, :ordsgroup, 1,:ktm||' - zbitka');
      if (position(';'||nref||';' in refs) = 0) then
        refs = refs ||nref||';';
    end
    if(batchquantity is null or (batchquantity = 0)) then batchquantity = 1;
    techjednquantity = cast((techjednquantity /batchquantity * ilosc) as integer);
    execute procedure GEN_REF('POZZAM') returning_values :pref;
    insert into POZZAM(REF ,ZAMOWIENIE, KTM, WERSJAREF, JEDN, ILOSC, KILOSC, MAGAZYN, OUT, PRSHMAT, techjednquantity, PRSCHEDGUIDEPOS)
      values(:pref ,:nref, :KTM, :WERSJAREF, :JEDN, 1, :ilosc, :wh, 1, :prshmat, :techjednquantity, :prschedguidepos);
  end
  for
    select n.ref, n.prdepart
      from nagzam n
      where n.grupazam = :ordsgroup
      into nref, prdepart
  do begin
    prschedule = null;
    select p.ref
      from prschedules p where p.prdepart = :prdepart and p.main = 1 and p.status in (1,2)
      into prschedule;
    if (prschedule is not null) then
    begin
      insert into PRSCHEDZAM(PRSCHEDULE,ZAMOWIENIE) values (:prschedule, :nref);
      update nagzam set autoscheduled = 1 where ref = :nref;
    end
  end
end^
SET TERM ; ^
