--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_PRSCHEDGUIDE_PRODUCTS(
      PRDEPART varchar(20) CHARACTER SET UTF8                           ,
      PRNAGZAM integer,
      PRPOZZAM integer,
      PRSCHEDGUIDE integer,
      PRSCHEDOPER integer,
      TIMESTAMPTO timestamp)
  returns (
      REF integer,
      PARENT integer,
      PRNAGZAMOUT integer,
      PRPOZZAMOUT integer,
      PRSCHEDGUIDEOUT integer,
      PRSCHEDGUIDESYMOUT varchar(40) CHARACTER SET UTF8                           ,
      PRSHOPERSYMBOUT varchar(255) CHARACTER SET UTF8                           ,
      PRSCHEDGUIDEPOSOUT integer,
      PRSCHEDOPEROUT integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      WERSJAREFDICT varchar(50) CHARACTER SET UTF8                           ,
      NAZWAT varchar(80) CHARACTER SET UTF8                           ,
      PRSHMAT integer,
      OUT smallint,
      WH varchar(3) CHARACTER SET UTF8                           ,
      DOCTYPEIN varchar(3) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4),
      QUANTITYS numeric(14,4),
      QUANTITYSR numeric(14,4),
      QUANTITYB numeric(14,4),
      QUANTITYP numeric(14,4),
      QUANTITYSP numeric(14,4),
      QUANTITYR numeric(14,4),
      AUTODOC smallint,
      PRSHORTAGE integer,
      SHORTAGE smallint)
   as
declare variable przelicznik numeric(14,4);
declare variable amountresult numeric(14,4);
declare variable quantityout numeric(14,4);
declare variable isparent smallint;
declare variable prsheet integer;
begin
  -- procedura daje liste produktow do przyjecia
  prshmat = null;
  out = 0;
  ref = 0;
  parent = null;
  if (prdepart = '') then prdepart = null;
  if (prnagzam = 0) then prnagzam = null;
  if (prpozzam = 0) then prpozzam = null;
  if (prschedguide = 0) then prschedguide = null;
  if (prschedoper = 0) then prschedoper = null;
  if (prdepart is null and prnagzam is null and prpozzam is null
        and prschedguide is null and prschedoper is null
  ) then
    exception PRSCHEDGUIDEDETS_ERROR 'Wymagany wydział produkcyjny';
  if (prschedoper is not null) then
  begin
    for
      select o.ref, o.amountresult, o.amountshortages, t.przelicz, g.ktm, v.ref,
          g.ref, g.symbol, g.zamowienie, g.pozzam, g.doctypein,
          v.nazwa, v.nazwat, o.amountresult, g.warehouse, p.descript, s.autodocin, s.ref
        from prschedopers o
          left join prschedguides g on (g.ref = o.guide)
          left join wersje v on (v.ktm = g.ktm and v.nrwersji = g.wersja)
          left join prsheets s on (s.ref = g.prsheet)
          left join towjedn t on (t.ref = s.jedn)
          left join prshopers p on (p.ref = o.shoper)
          left join prschedoperdeps d on (d.depfrom = o.ref)
        where o.ref = :prschedoper and g.prsheet = p.sheet and d.ref is null and o.amountresult >= 0 --and o.activ = 1
        into prschedoperout, quantity, quantitysp, przelicznik, ktm, wersjaref,
          prschedguideout, prschedguidesymout, prnagzamout, prpozzamout, doctypein,
          wersjarefdict, nazwat, quantityp, wh, prshopersymbout, autodoc, prsheet
    do begin
      isparent = 0;
      amountresult = quantity;
      if (exists(select first 1 1 from prshmat m where m.opermaster = :prschedoperout and m.mattype in (1,2,3))) then
        isparent = 1;
      select sum(r.quantity)
        from prschedguidedets r
        where r.prschedoper = :prschedoper and r.quantity > 0 and r.out = 0
          and r.shortage = 0 and r.byproduct = 0 --and r.overlimit = 0
        into quantityr;
      if (quantityr is null) then quantityr = 0;
      quantityr = quantityr * przelicznik;
      quantity = quantity * przelicznik;
      quantity = quantity - quantityr;
      select sum(r.quantity)
        from prschedguidedets r
        where r.prschedoper = :prschedoper and r.quantity > 0 and r.out = 0
          and r.shortage = 0 and r.byproduct = 1
        into quantityb;
      if (quantityb is null) then quantityb = 0;
      select sum(r.quantity / r.shortageratio)
        from prschedguidedets r
        where r.prschedoper = :prschedoper and r.quantity > 0 and r.out = 0
          and r.shortage = 1
        into quantitysr;
      quantitys = quantitysp - quantitysr;
      if (quantitys is null) then quantitys = 0;
      if (quantitysr is null) then quantitysr = 0;
      if (quantitysp is null) then quantitysp = 0;
      if (autodoc > 0) then quantity = 0;
      if ((amountresult = 0 and isparent = 1) or amountresult > 0) then
        suspend;
      parent = ref;
      for
        select o.ref, 0, t.przelicz, m.ktm, v.ref, m.ref,
            g.ref, g.symbol, g.zamowienie, pos.pozzamref, pos.doctypein,
            v.nazwa, v.nazwat, pos.warehouse, p.descript, s.autodocin, s.ref, pos.out,
            pos.amount - pos.amountzreal, pos.amount, pos.amountzreal, pos.ref
          from prschedopers o
            left join prschedguides g on (g.ref = o.guide)
            left join prshmat m on (m.opermaster = o.shoper)
            left join prschedguidespos pos on (pos.prschedoper = o.ref and pos.prshmat = m.ref)
            left join wersje v on (v.ktm = m.ktm and v.nrwersji = m.wersja)
            left join prsheets s on (s.ref = g.prsheet)
            left join towjedn t on (t.ref = m.jedn)
            left join prshopers p on (p.ref = o.shoper)
          where o.ref = :prschedoperout and pos.out = 3 and g.prsheet = p.sheet
            and o.amountresult >= 0 and m.mattype in (1,2,3)
          into prschedoperout, quantitysp, przelicznik, ktm, wersjaref, prshmat,
            prschedguideout, prschedguidesymout, prnagzamout, prpozzamout, doctypein,
            wersjarefdict, nazwat, wh, prshopersymbout, autodoc, prsheet, out,
            quantity, quantityp, quantityr, prschedguideposout
      do begin
        ref = ref + 1;
        suspend;
      end
      ref = ref + 1;
      parent = null;
    end
  end
  else if (prschedguide is not null) then
  begin
    for
      select o.ref, o.amountresult, o.amountshortages, t.przelicz, g.ktm, v.ref,
          g.ref, g.symbol, g.zamowienie, g.pozzam, g.doctypein,
          v.nazwa, v.nazwat, o.amountresult, g.warehouse, p.descript, s.autodocin
        from prschedopers o
          left join prschedguides g on (g.ref = o.guide)
          left join wersje v on (v.ktm = g.ktm and v.nrwersji = g.wersja)
          left join prsheets s on (s.ref = g.prsheet)
          left join towjedn t on (t.ref = s.jedn)
          left join prshopers p on (p.ref = o.shoper)
          left join prschedoperdeps d on (d.depfrom = o.ref)
        where o.guide = :prschedguide and g.prsheet = p.sheet and d.ref is null and o.amountresult >= 0 --and o.activ = 1
        into prschedoperout, quantity, quantitysp, przelicznik, ktm, wersjaref,
          prschedguideout, prschedguidesymout, prnagzamout, prpozzamout, doctypein,
          wersjarefdict, nazwat, quantityp, wh, prshopersymbout, autodoc
    do begin
      isparent = 0;
      amountresult = quantity;
      if (exists(select first 1 1 from prschedguidespos p where p.prschedguide = :prschedguide and p.out = 3)) then
        isparent = 1;
      select sum(r.quantity)
        from prschedguidedets r
        where r.prschedoper = :prschedoperout and r.quantity > 0 and r.out = 0
          and r.shortage = 0 and r.byproduct = 0 --and r.overlimit = 0
        into quantityr;
      if (quantityr is null) then quantityr = 0;
      quantityr = quantityr * przelicznik;
      quantity = quantity * przelicznik;
      quantity = quantity - quantityr;
      select sum(r.quantity)
        from prschedguidedets r
        where r.prschedoper = :prschedoperout and r.quantity > 0 and r.out = 0
          and r.shortage = 0 and r.byproduct = 1
        into quantityb;
      if (quantityb is null) then quantityb = 0;
      select sum(r.quantity / r.shortageratio)
        from prschedguidedets r
        where r.prschedoper = :prschedoperout and r.quantity > 0 and r.out = 0
          and r.shortage = 1
        into quantitysr;
      quantitys = quantitysp - quantitysr;
      if (quantitys is null) then quantitys = 0;
      if (quantitysr is null) then quantitysr = 0;
      if (quantitysp is null) then quantitysp = 0;
      if (autodoc > 0) then quantity = 0;
      if ((amountresult = 0 and isparent = 1) or amountresult > 0) then
        suspend;
      parent = ref;
      for
        select o.ref, 0, t.przelicz, m.ktm, v.ref, m.ref,
            g.ref, g.symbol, g.zamowienie, pos.pozzamref, pos.doctypein,
            v.nazwa, v.nazwat, pos.warehouse, p.descript, s.autodocin, s.ref, pos.out,
            pos.amount - pos.amountzreal, pos.amount, pos.amountzreal, pos.ref
          from prschedopers o
            left join prschedguides g on (g.ref = o.guide)
            left join prshmat m on (m.opermaster = o.shoper)
            left join prschedguidespos pos on (pos.prschedoper = o.ref and pos.prshmat = m.ref)
            left join wersje v on (v.ktm = m.ktm and v.nrwersji = m.wersja)
            left join prsheets s on (s.ref = g.prsheet)
            left join towjedn t on (t.ref = m.jedn)
            left join prshopers p on (p.ref = o.shoper)
          where o.guide = :prschedguide and pos.out = 3 and g.prsheet = p.sheet
            and o.amountresult >= 0 and m.mattype in (1,2,3)
          into prschedoperout, quantitysp, przelicznik, ktm, wersjaref, prshmat,
            prschedguideout, prschedguidesymout, prnagzamout, prpozzamout, doctypein,
            wersjarefdict, nazwat, wh, prshopersymbout, autodoc, prsheet, out,
            quantity, quantityp, quantityr, prschedguideposout
      do begin
        ref = ref + 1;
        suspend;
      end
      ref = ref + 1;
      parent = null;
    end
  end
  else if (prpozzam is not null) then
  begin
    for
      select o.ref, o.amountresult, o.amountshortages, t.przelicz, g.ktm, v.ref,
          g.ref, g.symbol, g.zamowienie, g.pozzam, g.doctypein,
          v.nazwa, v.nazwat, o.amountresult, g.warehouse, p.descript, s.autodocin
        from prschedguides g
          left join prschedopers o on (g.ref = o.guide)
          left join wersje v on (v.ktm = g.ktm and v.nrwersji = g.wersja)
          left join prsheets s on (s.ref = g.prsheet)
          left join towjedn t on (t.ref = s.jedn)
          left join prshopers p on (p.ref = o.shoper)
          left join prschedoperdeps d on (d.depfrom = o.ref)
        where g.pozzam = :prpozzam and g.prsheet = p.sheet and d.ref is null and o.amountresult > 0--and o.activ = 1
        into prschedoperout, quantity, quantitysp, przelicznik, ktm, wersjaref,
          prschedguideout, prschedguidesymout, prnagzamout, prpozzamout, doctypein,
          wersjarefdict, nazwat, quantityp, wh, prshopersymbout, autodoc
    do begin
      isparent = 0;
      amountresult = quantity;
      if (exists(select first 1 1 from prschedguidespos p where p.prschedguide = :prschedguideout and p.out = 3)) then
        isparent = 1;
      select sum(r.quantity)
        from prschedguidedets r
        where r.prschedoper = :prschedoperout and r.quantity > 0 and r.out = 0
          and r.shortage = 0 and r.byproduct = 0 --and r.overlimit = 0
        into quantityr;
      if (quantityr is null) then quantityr = 0;
      quantityr = quantityr * przelicznik;
      quantity = quantity * przelicznik;
      quantity = quantity - quantityr;
      select sum(r.quantity)
        from prschedguidedets r
        where r.prschedoper = :prschedoperout and r.quantity > 0 and r.out = 0
          and r.shortage = 0 and r.byproduct = 1
        into quantityb;
      if (quantityb is null) then quantityb = 0;
      select sum(r.quantity / r.shortageratio)
        from prschedguidedets r
        where r.prschedoper = :prschedoperout and r.quantity > 0 and r.out = 0
          and r.shortage = 1
        into quantitysr;
      quantitys = quantitysp - quantitysr;
      if (quantitys is null) then quantitys = 0;
      if (quantitysr is null) then quantitysr = 0;
      if (quantitysp is null) then quantitysp = 0;
      if (autodoc > 0) then quantity = 0;
      if ((amountresult = 0 and isparent = 1) or amountresult > 0) then
        suspend;
      parent = ref;
      for
        select o.ref, 0, t.przelicz, m.ktm, v.ref, m.ref,
            g.ref, g.symbol, g.zamowienie, pos.pozzamref, pos.doctypein,
            v.nazwa, v.nazwat, pos.warehouse, p.descript, s.autodocin, s.ref, pos.out,
            pos.amount - pos.amountzreal, pos.amount, pos.amountzreal, pos.ref
          from prschedopers o
            left join prschedguides g on (g.ref = o.guide)
            left join prshmat m on (m.opermaster = o.shoper)
            left join prschedguidespos pos on (pos.prschedoper = o.ref and pos.prshmat = m.ref)
            left join wersje v on (v.ktm = m.ktm and v.nrwersji = m.wersja)
            left join prsheets s on (s.ref = g.prsheet)
            left join towjedn t on (t.ref = m.jedn)
            left join prshopers p on (p.ref = o.shoper)
          where o.guide = :prschedguideout and pos.out = 3 and g.prsheet = p.sheet
            and o.amountresult >= 0 and m.mattype in (1,2,3)
          into prschedoperout, quantitysp, przelicznik, ktm, wersjaref, prshmat,
            prschedguideout, prschedguidesymout, prnagzamout, prpozzamout, doctypein,
            wersjarefdict, nazwat, wh, prshopersymbout, autodoc, prsheet, out,
            quantity, quantityp, quantityr, prschedguideposout
      do begin
        ref = ref + 1;
        suspend;
      end
      ref = ref + 1;
      parent = null;
    end
  end
  else if (prnagzam is not null) then
  begin
    for
      select o.ref, o.amountresult, o.amountshortages, t.przelicz, g.ktm, v.ref,
          g.ref, g.symbol, g.zamowienie, g.pozzam, g.doctypein,
          v.nazwa, v.nazwat, o.amountresult, g.warehouse, p.descript, s.autodocin
        from prschedguides g
          left join prschedopers o on (g.ref = o.guide)
          left join wersje v on (v.ktm = g.ktm and v.nrwersji = g.wersja)
          left join prsheets s on (s.ref = g.prsheet)
          left join towjedn t on (t.ref = s.jedn)
          left join prshopers p on (p.ref = o.shoper)
          left join prschedoperdeps d on (d.depfrom = o.ref)
        where g.zamowienie = :prnagzam and g.prsheet = p.sheet and d.ref is null and o.amountresult > 0-- and o.activ = 1
        into prschedoperout, quantity, quantitysp, przelicznik, ktm, wersjaref,
          prschedguideout, prschedguidesymout, prnagzamout, prpozzamout, doctypein,
          wersjarefdict, nazwat, quantityp, wh, prshopersymbout, autodoc
    do begin
      isparent = 0;
      amountresult = quantity;
      if (exists(select first 1 1 from prschedguidespos p where p.prschedguide = :prschedguideout and p.out = 3)) then
        isparent = 1;
      select sum(r.quantity)
        from prschedguidedets r
        where r.prschedoper = :prschedoperout and r.quantity > 0 and r.out = 0
          and r.shortage = 0 and r.byproduct = 0 --and r.overlimit = 0
        into quantityr;
      if (quantityr is null) then quantityr = 0;
      quantityr = quantityr * przelicznik;
      quantity = quantity * przelicznik;
      quantity = quantity - quantityr;
      select sum(r.quantity)
        from prschedguidedets r
        where r.prschedoper = :prschedoperout and r.quantity > 0 and r.out = 0
          and r.shortage = 0 and r.byproduct = 1
        into quantityb;
      if (quantityb is null) then quantityb = 0;
      select sum(r.quantity / r.shortageratio)
        from prschedguidedets r
        where r.prschedoper = :prschedoperout and r.quantity > 0 and r.out = 0
          and r.shortage = 1
        into quantitysr;
      quantitys = quantitysp - quantitysr;
      if (quantitys is null) then quantitys = 0;
      if (quantitysr is null) then quantitysr = 0;
      if (quantitysp is null) then quantitysp = 0;
      if (autodoc > 0) then quantity = 0;
      if ((amountresult = 0 and isparent = 1) or amountresult > 0) then
        suspend;
      parent = ref;
      for
        select o.ref, 0, t.przelicz, m.ktm, v.ref, m.ref,
            g.ref, g.symbol, g.zamowienie, pos.pozzamref, pos.doctypein,
            v.nazwa, v.nazwat, pos.warehouse, p.descript, s.autodocin, s.ref, pos.out,
            pos.amount - pos.amountzreal, pos.amount, pos.amountzreal, pos.ref
          from prschedopers o
            left join prschedguides g on (g.ref = o.guide)
            left join prshmat m on (m.opermaster = o.shoper)
            left join prschedguidespos pos on (pos.prschedoper = o.ref and pos.prshmat = m.ref)
            left join wersje v on (v.ktm = m.ktm and v.nrwersji = m.wersja)
            left join prsheets s on (s.ref = g.prsheet)
            left join towjedn t on (t.ref = m.jedn)
            left join prshopers p on (p.ref = o.shoper)
          where o.guide = :prschedguideout and pos.out = 3 and g.prsheet = p.sheet
            and o.amountresult >= 0 and m.mattype in (1,2,3)
          into prschedoperout, quantitysp, przelicznik, ktm, wersjaref, prshmat,
            prschedguideout, prschedguidesymout, prnagzamout, prpozzamout, doctypein,
            wersjarefdict, nazwat, wh, prshopersymbout, autodoc, prsheet, out,
            quantity, quantityp, quantityr, prschedguideposout
      do begin
        ref = ref + 1;
        suspend;
      end
      ref = ref + 1;
      parent = null;
    end
  end
  else if (prdepart is not null) then
  begin
    for
      select o.ref, o.amountresult, o.amountshortages, t.przelicz, g.ktm, v.ref,
          g.ref, g.symbol, g.zamowienie, g.pozzam, g.doctypein,
          v.nazwa, v.nazwat, o.amountresult, g.warehouse, p.descript, s.autodocin
        from prschedguides g
          left join prschedopers o on (g.ref = o.guide)
          left join wersje v on (v.ktm = g.ktm and v.nrwersji = g.wersja)
          left join prsheets s on (s.ref = g.prsheet)
          left join towjedn t on (t.ref = s.jedn)
          left join prshopers p on (p.ref = o.shoper)
          left join prschedoperdeps d on (d.depfrom = o.ref)
        where g.prdepart = :prdepart and g.prsheet = p.sheet and d.ref is null and o.amountresult > 0--and o.activ = 1
        into prschedoperout, quantity, quantitysp, przelicznik, ktm, wersjaref,
          prschedguideout, prschedguidesymout, prnagzamout, prpozzamout, doctypein,
          wersjarefdict, nazwat, quantityp, wh, prshopersymbout, autodoc
    do begin
      isparent = 0;
      amountresult = quantity;
      if (exists(select first 1 1 from prschedguidespos p where p.prschedguide = :prschedguideout and p.out = 3)) then
        isparent = 1;
      select sum(r.quantity)
        from prschedguidedets r
        where r.prschedoper = :prschedoperout and r.quantity > 0 and r.out = 0
          and r.shortage = 0 and r.byproduct = 0 --and r.overlimit = 0
        into quantityr;
      if (quantityr is null) then quantityr = 0;
      quantityr = quantityr * przelicznik;
      quantity = quantity * przelicznik;
      quantity = quantity - quantityr;
      select sum(r.quantity)
        from prschedguidedets r
        where r.prschedoper = :prschedoperout and r.quantity > 0 and r.out = 0
          and r.shortage = 0 and r.byproduct = 1
        into quantityb;
      if (quantityb is null) then quantityb = 0;
      select sum(r.quantity / r.shortageratio)
        from prschedguidedets r
        where r.prschedoper = :prschedoperout and r.quantity > 0 and r.out = 0
          and r.shortage = 1
        into quantitysr;
      quantitys = quantitysp - quantitysr;
      if (quantitys is null) then quantitys = 0;
      if (quantitysr is null) then quantitysr = 0;
      if (quantitysp is null) then quantitysp = 0;
      if ((amountresult = 0 and isparent = 1) or amountresult > 0) then
        suspend;
      parent = ref;
      for
        select o.ref, 0, t.przelicz, m.ktm, v.ref, m.ref,
            g.ref, g.symbol, g.zamowienie, pos.pozzamref, pos.doctypein,
            v.nazwa, v.nazwat, pos.warehouse, p.descript, s.autodocin, s.ref, pos.out,
            pos.amount - pos.amountzreal, pos.amount, pos.amountzreal, pos.ref
          from prschedopers o
            left join prschedguides g on (g.ref = o.guide)
            left join prshmat m on (m.opermaster = o.shoper)
            left join prschedguidespos pos on (pos.prschedoper = o.ref and pos.prshmat = m.ref)
            left join wersje v on (v.ktm = m.ktm and v.nrwersji = m.wersja)
            left join prsheets s on (s.ref = g.prsheet)
            left join towjedn t on (t.ref = m.jedn)
            left join prshopers p on (p.ref = o.shoper)
          where o.guide = :prschedguideout and pos.out = 3 and g.prsheet = p.sheet
            and o.amountresult >= 0 and m.mattype in (1,2,3)
          into prschedoperout, quantitysp, przelicznik, ktm, wersjaref, prshmat,
            prschedguideout, prschedguidesymout, prnagzamout, prpozzamout, doctypein,
            wersjarefdict, nazwat, wh, prshopersymbout, autodoc, prsheet, out,
            quantity, quantityp, quantityr, prschedguideposout
      do begin
        ref = ref + 1;
        suspend;
      end
      ref = ref + 1;
      parent = null;
    end
  end
end^
SET TERM ; ^
