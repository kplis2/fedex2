--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_BEST_SOURCE(
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      REFMWSACT integer,
      REFMWSORD integer,
      MWSORDTYPE integer,
      WH varchar(3) CHARACTER SET UTF8                           ,
      WHSEC integer,
      WHAREA integer,
      MAXLEVEL smallint,
      PRIORITY smallint,
      PAL smallint,
      MIX smallint,
      MWSCONSTLOCB integer,
      PALTYPE varchar(40) CHARACTER SET UTF8                           ,
      PALW numeric(14,2),
      PALL numeric(14,2),
      PALH numeric(14,2),
      REFILLTRY smallint,
      QUANTITY numeric(14,4))
  returns (
      MWSCONSTLOCP integer,
      MWSPALLOCP integer,
      WHAREAP integer,
      WHAREALOGP integer,
      REFILL smallint)
   as
declare variable mwsordtypedest smallint;
begin
  -- znajdujemy lokacje poczatkowa dla uzupelniania
  select mwsortypedets
    from mwsordtypes where ref = :mwsordtype
    into mwsordtypedest;
  if (mwsordtypedest = 1) then
  begin
    execute procedure XK_MWS_GET_BEST_SOURCE_1(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refill);
  end else if (mwsordtypedest = 2) then
  begin
    execute procedure XK_MWS_GET_BEST_SOURCE_2(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refill);
  end
  else if (mwsordtypedest = 3) then
  begin
    execute procedure XK_MWS_GET_BEST_SOURCE_3(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refill);
  end
  else if (mwsordtypedest = 4) then
  begin
    execute procedure XK_MWS_GET_BEST_SOURCE_4(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refill);
  end
  else if (mwsordtypedest = 5) then
  begin
    execute procedure XK_MWS_GET_BEST_SOURCE_5(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refill);
  end
  else if (mwsordtypedest = 6) then
  begin
    execute procedure XK_MWS_GET_BEST_SOURCE_6(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refill);
  end
  else if (mwsordtypedest = 7) then
  begin
    execute procedure XK_MWS_GET_BEST_SOURCE_7(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refill);
  end
  else if (mwsordtypedest = 8) then
  begin
    execute procedure XK_MWS_GET_BEST_SOURCE_8(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refill);
  end
  else if (mwsordtypedest = 9) then
  begin
    execute procedure XK_MWS_GET_BEST_SOURCE_9(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refill);
  end
  else if (mwsordtypedest = 10) then
  begin
    execute procedure XK_MWS_GET_BEST_SOURCE_10(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refill);
  end
  else if (mwsordtypedest = 11) then
  begin
    execute procedure XK_MWS_GET_BEST_SOURCE_11(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refill);
  end
  else if (mwsordtypedest = 12) then
  begin
    execute procedure XK_MWS_GET_BEST_SOURCE_12(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refill);
  end
  else if (mwsordtypedest = 13) then
  begin
    execute procedure XK_MWS_GET_BEST_SOURCE_13(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refill);
  end
  else if (mwsordtypedest = 14) then
  begin
    execute procedure XK_MWS_GET_BEST_SOURCE_14(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refill);
  end else if (mwsordtypedest = 15) then
  begin
    execute procedure XK_MWS_GET_BEST_SOURCE_15(:GOOD ,:VERS ,:REFMWSACT, :REFMWSORD, :MWSORDTYPE, :WH, :WHSEC, :WHAREA, :MAXLEVEL, :PRIORITY, :PAL, :MIX, :MWSCONSTLOCB, :PALTYPE, :PALW, :PALL, :PALH, :REFILLTRY, :QUANTITY)
      returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refill);
  end
  else
    exception MWSORDTYPE_NOT_DEFINED;
end^
SET TERM ; ^
