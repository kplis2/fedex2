--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_EABSLIMITS(
      EABSENCE integer,
      SHOW_EMPTY smallint,
      EMPLOYEE integer,
      AYEAR integer,
      COMPANY integer)
  returns (
      LP integer,
      NAME varchar(30) CHARACTER SET UTF8                           ,
      COUNTER numeric(14,2),
      LIMIT numeric(14,2),
      RESTLIMIT numeric(14,2))
   as
declare variable ILL_DAYS numeric(14,2);
declare variable BENEFIT_DAYS numeric(14,2);
declare variable MOTHERLY_DAYS numeric(14,2);
declare variable PARENTAL_DAYS numeric(14,2);
declare variable FATHER_DAYS numeric(14,2);
declare variable ADDMOTHERLY_DAYS numeric(14,2);
declare variable ADULTCARE_DAYS numeric(14,2);
declare variable CHILDCARE_DAYS numeric(14,2);
declare variable MDCARE_DAYS numeric(14,2);
declare variable VACATION_DAYS numeric(14,2);
declare variable REQUIREVAC_DAYS numeric(14,2);
declare variable OCCASIONALVAC_DAYS numeric(14,2);
declare variable PARENTVAC_DAYS numeric(14,2);
declare variable UNPAIDVAC_DAYS numeric(14,2);
declare variable REHABILITATION_DAYS numeric(14,2);
declare variable WKU_DAYS numeric(14,2);
declare variable EWCERT_ILL_DAYS numeric(14,2);
declare variable EWCERT_VAC_DAYS numeric(14,2);
declare variable EWCERT_REQVAC_DAYS numeric(14,2);
declare variable EWCERT_CHILDCARE_DAYS numeric(14,2);
declare variable EWCERT_ADULTCARE_DAYS numeric(14,2);
declare variable EWCERT_MDCARE_DAYS numeric(14,2);
declare variable AFROMDATE date;
declare variable ATODATE date;
declare variable YEAR_OF_BIRTH integer;
begin
/*  MWr Personel: procedura generuje liczniki i limity w oknie nieobecnosci

 Wywolywana podczas przegladania rekordow w oknie nieobecnosic z parametrami:
  eabsence   - nr ref wybranej nieobecnosci, wartosc < 0 jezeli nie wybrano
               nieobecnosci lub jestesmy w trybie insert
  show_empty - pokaze/ukryje zerowe liczniki jezeli parametr = 1/0
  employee   - nr ref pracownika, wartosc przekazywana zawsze o ile
               mozna ja okreslic (pobierana z filtru lub niebecnosci)
  ayear      - rok, za ktory gener. liczniki, analogicznie jak par. employee */


  if (eabsence > 0) then
    select employee, ayear, fromdate, company
      from eabsences
      where ref = :eabsence
      into :employee, :ayear, :afromdate, :company;
  else if (ayear > 0) then
    afromdate = ayear || '/1/1';

  if (employee > 0 and afromdate is not null) then
  begin
    lp = 0;

    --liczniki na podstawie tabeli nieobecnosci
    select sum(case when ecolumn in (40,50,60) then days else 0 end),
           sum(case when ecolumn in (90,100,110,120) then days else 0 end),
           sum(case when ecolumn = 160 then days else 0 end),
           sum(case when ecolumn = 170 then days else 0 end),
           sum(case when ecolumn = 180 then workdays else 0 end),
           sum(case when ecolumn = 220 then workdays else 0 end),
           sum(case when ecolumn = 230 then workdays else 0 end),
           sum(case when ecolumn = 250 then workdays else 0 end),
           sum(case when ecolumn = 260 then days else 0 end),
           sum(case when ecolumn = 300 then days else 0 end),
           sum(case when ecolumn = 330 then workdays else 0 end),
           sum(case when ecolumn in (350,360,370) then days else 0 end),
           sum(case when ecolumn in (270,150) then days else 0 end),
           sum(case when ecolumn in (280,440) then days else 0 end),
           sum(case when ecolumn in (140,450) then days else 0 end),
            sum(case when ecolumn = 290 then days else 0 end)
      from eabsences
      where ecolumn in (40,50,60,90,100,110,120,140,150,160,170,180,220,230,250,260,270,280,290,300,330,350,360,370,440,450)
        and employee = :employee
        and ayear = :ayear
        and correction in (0,2)
        into :ill_days, :benefit_days, :childcare_days, :adultcare_days,
             :mdcare_days, :vacation_days, :requirevac_days, occasionalvac_days,
             :parentvac_days, :unpaidvac_days, :wku_days, :rehabilitation_days,
             :motherly_days, :addmotherly_days, :parental_days, :father_days;

    --wykorzystane liczniki na podstawie zarejestrownych swiadectw pracy
    select sum(case when ewr.ecolumn = 40  then ewr.daylimit else 0 end),
           sum(case when ewr.ecolumn = 160 then ewr.daylimit else 0 end),
           sum(case when ewr.ecolumn = 170 then ewr.daylimit else 0 end),
           sum(case when ewr.ecolumn = 180 then ewr.daylimit else 0 end),
           sum(case when ewr.ecolumn = 220 then ewr.daylimit else 0 end),
           sum(case when ewr.ecolumn = 230 then ewr.daylimit else 0 end)
      from eworkcertifs ew
        join ewrkcrtlimits ewr on (ewr.workcertif = ew.ref)
      where ew.employee = :employee
        and extract(year from todate) = :ayear
        and ewr.ecolumn in (40, 160, 170, 180, 220, 230)
      into :ewcert_ill_days, :ewcert_childcare_days, :ewcert_adultcare_days,
           :ewcert_mdcare_days, :ewcert_vac_days,:ewcert_reqvac_days;


--ilość dni choroby w danym roku
    name = 'Choroba wynagrodz.';
    counter =  coalesce(ewcert_ill_days,0) + coalesce(ill_days,0);
    if (show_empty = 1 or counter > 0) then
    begin
      select extract (year from p.birthdate)
        from employees e
          join persons p on p.ref = e.person
        where e.ref = :employee
        into :year_of_birth;
      if (ayear >= 2009 and ( ayear - 1 - year_of_birth >= 50)) then
        limit = 14;
      else
        limit = 33;
      restlimit = limit - counter;
      lp = lp + 1;
      suspend;
    end

--ilość dni choroby ZUS w danym roku
    name = 'Choroba ZUS';
    counter = coalesce(benefit_days,0);
    if (show_empty = 1 or counter > 0) then
    begin
      limit = null;
      restlimit = null;
      lp = lp + 1;
      suspend;
    end

---ilość wykorzystanych dni opieki nach chorym (dzieckiem)
    name = 'Opieka - dziecko';
    counter = coalesce(ewcert_childcare_days,0) + coalesce(childcare_days,0);
    if (show_empty = 1 or counter > 0) then
    begin
      execute procedure get_pval(:afromdate, :company, 'LIMIT_OPIEKA_DZIECKO')
        returning_values :limit;
      restlimit = limit - counter;
      lp = lp + 1;
      suspend;
    end

---ilość wykorzystanych dni opieki nach chorym (doroslym)
    name = 'Opieka - dorosły';
    counter = coalesce(ewcert_adultcare_days,0) + coalesce(adultcare_days,0);
    if (show_empty = 1 or counter > 0) then
    begin
      execute procedure get_pval(:afromdate, :company, 'LIMIT_OPIEKA_DOROSLY')
        returning_values :limit;
      restlimit = limit - counter;
      lp = lp + 1;
      suspend;
    end

---łączny limit zasiłku opiekuńczego
    name = 'Zas. opiekuńczy';
    counter =  coalesce(ewcert_childcare_days,0) + coalesce(childcare_days,0)
            +  coalesce(ewcert_adultcare_days,0) + coalesce(adultcare_days,0);
    if (show_empty = 1 or counter > 0) then
    begin
      limit = 60;
      restlimit = limit - counter;
      lp = lp + 1;
      suspend;
    end

---ilość wykorzystanych dni opieki Matka-Dziecko (Kodeks Pracy, Art 188)
    name = 'Opieka - KP.188';
    counter = coalesce(ewcert_mdcare_days,0) + coalesce(mdcare_days,0);
    if (show_empty = 1 or counter > 0) then
    begin
      select l.vacmdlimit
        from evaclimits l
        where vyear = :ayear and employee = :employee
        into :limit;
      restlimit = limit - counter;
      lp = lp + 1;
      suspend;
    end

--ilość dni urlopu wypoczynkowego
    name = 'Url. wypoczynkowy';
    counter = coalesce(ewcert_vac_days,0) + coalesce(vacation_days,0) + coalesce(requirevac_days,0);
    if (show_empty = 1 or counter > 0) then
    begin
      limit = null;
      select coalesce(restlimitd,0) + coalesce(limitconsd,0)
        from evaclimits
        where vyear = :ayear
          and employee = :employee
        into :limit;
      restlimit = limit - counter;
      lp = lp + 1;
      suspend;
    end

--ilość dni urlopu na zadanie
    name = 'Url. na żądanie';
    counter =  coalesce(ewcert_reqvac_days,0) + coalesce(requirevac_days,0);
    if (show_empty = 1 or counter > 0) then
    begin
      select l.vacreqlimit
        from evaclimits l
        where vyear = :ayear and employee = :employee
        into :limit;
      restlimit = limit - counter;
      lp = lp + 1;
      suspend;
    end

--ilość dni urlopu okoliczonosciowego
    name = 'Url. okolicznościowy';
    counter =  coalesce(occasionalvac_days,0);
    if (show_empty = 1 or counter > 0) then
    begin
      limit = null;
      restlimit = null;
      lp = lp + 1;
      suspend;
    end

--ilość dni urlopu bezplatnego
    name = 'Url. bezpłatny';
    counter =  coalesce(unpaidvac_days,0);
    if (show_empty = 1 or counter > 0) then
    begin
      limit = null;
      restlimit = null;
      lp = lp + 1;
      suspend;
    end

--ilość wykorzystanego urlopu wychowawczego
    name = 'Url. wychowawczy';
    counter = coalesce(parentvac_days,0);
    if (show_empty = 1 or counter > 0) then
    begin
      limit = null;
      restlimit = null;
      lp = lp + 1;
      suspend;
    end

--ilość dni wykorzystanego urlopu macierzyńskiego PR55454
    name = 'Url. macierzyński';
    counter = coalesce(motherly_days,0);
    if (show_empty = 1 or counter > 0) then
    begin
      limit = null;
      restlimit = null;
      lp = lp + 1;
    suspend;
    end

--ilość dni wykorzystanego urlopu macierzyńskiego dodatkowego
    name = 'Url. macierz. dodatk.';
    counter = coalesce(addmotherly_days,0);
    if (show_empty = 1 or counter > 0) then
    begin
      limit = null;
      restlimit = null;
      lp = lp + 1;
    suspend;
    end

--ilość dni wykorzystanego urlopu rodzicielskiego
    name = 'Url. rodzicielski';
    counter = coalesce(parental_days,0);
    if (show_empty = 1 or counter > 0) then
    begin
      limit = null;
      restlimit = null;
      lp = lp + 1;
    suspend;
    end

--ilość dni wykorzystanego urlopu ojcowskiego
    name = 'Url. ojcowski';
    counter = coalesce(father_days,0);
    if (show_empty = 1 or counter > 0) then
    begin
      limit = null;
      restlimit = null;
      lp = lp + 1;
    suspend;
    end

--ilość dni przebywania na swidczeniu rehabiltacyjnym
    name = 'Świadcz. rehabiltac.';
    counter =  coalesce(rehabilitation_days,0);
    if (show_empty = 1 or counter > 0) then
    begin
      limit = null;
      restlimit = null;
      lp = lp + 1;
      suspend;
    end

--ilość dni nieobecnosci spowodowanej sluzba wojskowa
    name = 'Służba wojskowa';
    counter = coalesce(wku_days,0);
    if (show_empty = 1 or counter > 0) then
    begin
      limit = null;
      restlimit = null;
      lp = lp + 1;
      suspend;
    end

--zmienne liczniki w zaleznosci od REFa nieobecnosci:
    if (coalesce(eabsence,0) > 0) then
    begin
  --ilość wykorzystanych nieprzerwanych dni absencji
      name = 'Ciągła absencja ch.';
      counter = null;
      execute procedure e_get_whole_eabsperiod(:eabsence, null, null, null, null, null, ';40;50;60;90;100;110;120;130;140;150;350;360;370;440;450;')
        returning_values :afromdate, :atodate;
      if (afromdate is not null) then
        counter = atodate - afromdate + 1 ;
      if (show_empty = 1 or coalesce(counter,0) > 0) then
      begin
        limit = null;
        restlimit = null;
        lp = lp + 1;
        suspend;
      end

  --ilość wykorzystanego okresu zasiłkowego (przerwa w absencji mniesza niż 60 dni)
      name = 'Okres zasiłkowy';
      execute procedure e_get_sickbenefitdays(:eabsence, ';40;50;60;90;100;110;120;130;140;150;350;360;370;440;450;')
        returning_values :counter;
      if (show_empty = 1 or coalesce(counter,0) > 0) then
      begin
        if (counter = 0) then counter = null;
        limit = null;
        restlimit = null;
        lp = lp + 1;
        suspend;
      end
    end
  end else
    suspend;
end^
SET TERM ; ^
