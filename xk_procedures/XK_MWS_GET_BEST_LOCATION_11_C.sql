--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_GET_BEST_LOCATION_11_C(
      WH varchar(3) CHARACTER SET UTF8                           ,
      DISABLEDCNT integer,
      MODE smallint,
      PALH numeric(14,4))
  returns (
      MWSCONSTLOCL integer)
   as
declare variable lref integer;
begin
  mwsconstlocl = null;
  -- szukamy pietra na ktorym jest wolna lokacja a na tym pietrze jest juz paleta niewymiarowa (act = -1)
  for
    select l.levelref
      from mws_single_levels l
        left join mwsconstlocs c on (c.mwsstandlevel = l.levelref)
        left join mwsconstlocs a on (a.ref = c.ref and a.act = -1)
        left join whsecrows w on (w.ref = c.whsecrow)
        left join whsecs s on (s.ref = c.whsec)
      where s.deliveryarea in (2,6) and l.wh = :wh
      group by l.wh, l.levelref, l.loccnt, l.freecnt, w.symbol
      having l.freecnt > case when :mode = 0 then 0 when :mode = 1 then l.loccnt - 1 else 1 end
        and count(a.ref) = :disabledcnt and l.freecnt > case when :disabledcnt = 0 then 1 else 0 end
      order by l.freecnt, w.symbol
      into lref
  do begin
    -- potem szukamy lokacji w ramach segmentu, na ktora mozna postawic palete
    select first 1 c.ref
      from mwsconstlocs c
        left join mwsfreemwsconstlocs f on (f.ref = c.ref)
      where c.mwsstandlevel = :lref and f.ref is not null and c.h >= :palh and c.wh = :wh
      order by c.symbol
      into mwsconstlocl;
    if (mwsconstlocl is not null) then
      exit;
  end
end^
SET TERM ; ^
