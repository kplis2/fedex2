--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_MWS_TAKE_FULL_PAL_CHECK(
      WH varchar(3) CHARACTER SET UTF8                           ,
      DOCGROUP integer,
      MWSORD integer,
      MODE smallint)
  returns (
      MWSCONSTLOC integer)
   as
declare variable vers integer;
declare variable stocklot integer;
declare variable stockquantity numeric(14,4);
declare variable stocklotquantity numeric(14,4);
declare variable doclot integer;
declare variable docquantity numeric(14,4);
declare variable ok smallint;
declare variable x_partia string20;
declare variable x_serial_no integer_id;
declare variable x_slownik integer_id;
declare variable row_type smallint_id;
declare variable flow_order smallint_id;
begin
  if (mode = 0) then
  begin
    -- dla wszystkich lokacji na ktorych jest ten towar w ilosci mniejszej niz na pozycjach dokumentow
    for
      select distinct ms.mwsconstloc
        from dokumnag d
          join dokumpoz dp on (dp.dokument = d.ref)
          join towary t on (dp.ktm = t.ktm)
          left join mwsstock ms on (dp.wersjaref = ms.vers)
          left join mwsconstlocs ml on (ml.ref = ms.mwsconstloc)
          left join dokumpoz p1 on (p1.ref = dp.alttopoz and dp.fake = 1)
          left join towary t1 on (t1.ktm = p1.ktm and t1.usluga <> 1)
        where d.grupasped = :docgroup
          and d.magazyn = :wh
          and t.usluga <> 1
          and (ms.lot = coalesce(dp.dostawa,0) or coalesce(dp.dostawa,0) = 0)
          and (ms.x_partia = dp.x_partia or dp.x_partia is null) -- XXX KBI
          and (ms.x_serial_no = dp.x_serial_no or dp.x_serial_no is null) -- XXX KBI
          and (ms.x_slownik = dp.x_slownik or coalesce(dp.x_slownik,0) = 0) -- XXX KBI
          and ms.ref is not null
          and ml.ref is not null
          and ml.wh = :wh
          and ml.goodsav = 0
          and ml.locdest = 1
          and ml.act > 0
          and (ms.x_blocked<>1) --XXX Ldz ZG 126909
        group by dp.wersjaref, coalesce(dp.dostawa,0), ms.mwsconstloc, ms.x_partia, ms.x_serial_no, ms.x_slownik
        having sum(case when (d.mwsdisposition = 1 and coalesce(t1.altposmode,0) = 1) then dp.ilosc else dp.iloscl end - coalesce(dp.ilosconmwsacts,0)) >= sum(ms.quantity)
          and sum(ms.blocked + ms.ordered) = 0
        order by ms.x_partia, (select count(distinct ms1.vers) from mwsstock ms1 where ms1.mwsconstloc = ms.mwsconstloc) desc
        into :mwsconstloc
    do begin
      ok = 1;
      stockquantity = 0;

      -- xxx Dla regalow przeplowych pobieramy tylko z regalow nr 1 (PX-01-XX-XX)
      select ml.x_row_type, ml.x_flow_order from mwsconstlocs ml where ml.ref = :mwsconstloc into :row_type, :flow_order;
      if (row_type = 1 and flow_order <> 1) then
        ok = 0;
      if (ok = 1) then begin
        -- trzeba zabrac wszystki towary z lokacji w calej ilosci
        for
          select s.vers, sum(s.quantity)
            from mwsstock s
            where s.mwsconstloc = :mwsconstloc
              and (s.x_blocked<>1) --XXX Ldz ZG 126909
            group by s.vers
            into vers, stockquantity
        do begin
          -- najpierw sprawdzamy czy jest towar z wybranymi dostawami na lokacji
          -- bez sensu jest dzialanie na WMS w trybie mieszanym wiec tego nie obsluguje
          for
            select coalesce(p.dostawa,0),
                sum(case when (d.mwsdisposition = 1 or coalesce(t1.altposmode,0) = 1) then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0))
              from dokumnag d
                join dokumpoz p on (p.dokument = d.ref)
                join towary t on (p.ktm = t.ktm)
                left join dokumpoz p1 on (p1.ref = p.alttopoz and p.fake = 1)
                left join towary t1 on (t1.ktm = p1.ktm and t1.usluga <> 1)
              where d.grupasped = :docgroup and d.magazyn = :wh
                and p.wersjaref = :vers and coalesce(p.dostawa,0) > 0
                and p.genmwsordsafter = 1
                and t.usluga <> 1
              group by p.wersjaref, coalesce(p.dostawa,0)
              --having sum(p.iloscl - coalesce(p.ilosconmwsacts,0)) > 0
              order by coalesce(p.dostawa,0) desc
              into doclot, docquantity
          do begin
            stocklotquantity = 0;
            select sum(s.quantity)
              from mwsstock s
              where s.mwsconstloc = :mwsconstloc and s.vers = :vers and s.lot = :doclot and (s.x_blocked<>1) --XXX Ldz ZG 126909
              into stocklotquantity;
            if (stocklotquantity is null) then stocklotquantity = 0;
            stockquantity = stockquantity - case when stocklotquantity < docquantity then stocklotquantity else docquantity end;
          end
          -- sprawdzamy czy na pozcyjach sa ilosci wieksze niz na lokacji
          for
            select sum(case when (d.mwsdisposition = 1 or coalesce(t1.altposmode,0) = 1) then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0))
              from dokumnag d
                left join dokumpoz p on (p.dokument = d.ref)
                join towary t on (p.ktm = t.ktm)
                left join dokumpoz p1 on (p1.ref = p.alttopoz and p.fake = 1)
                left join towary t1 on (t1.ktm = p1.ktm and t1.usluga <> 1)
              where d.grupasped = :docgroup and d.magazyn = :wh
                and p.wersjaref = :vers and coalesce(p.dostawa,0) = 0
                and (:x_partia is null or p.x_partia = :x_partia) -- XXX KBI
                and (:x_serial_no is null or p.x_serial_no = :x_serial_no) -- XXX KBI
                and (coalesce(:x_slownik,0) = 0 or p.x_slownik = :x_slownik) -- XXX KBI
                and p.genmwsordsafter = 1
                and t.usluga <> 1
              group by p.wersjaref,p.x_partia, p.x_serial_no, p.x_slownik
              having sum(case when (d.mwsdisposition = 1 or coalesce(t.altposmode,0) = 1) then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0)) > 0
              --having sum(p.iloscl - coalesce(p.ilosconmwsacts,0)) > 0
              into docquantity
          do begin
            stocklotquantity = 0;
            select sum(s.quantity)
              from mwsstock s
              where s.mwsconstloc = :mwsconstloc and s.vers = :vers and (s.x_blocked<>1) --XXX Ldz ZG 126909
              into stocklotquantity;
            if (stocklotquantity is null) then stocklotquantity = 0;
            stockquantity = stockquantity - case when stocklotquantity < docquantity then stocklotquantity else docquantity end;
          end
          if (stockquantity > 0) then
            ok = 0;
        end
      end
      if (ok = 1) then
        suspend;
    end
  end
  else if (mode = 1) then
  begin
    -- dla wszystkich lokacji na ktorych jest ten towar w ilosci mniejszej niz na pozycjach dokumentow
    for
      select distinct ms.mwsconstloc
        from mwsdocpostomwsord m
          left join dokumpoz dp on (dp.ref = m.docposid)
          left join dokumnag d on (m.docid = d.ref)
          left join mwsstock ms on (dp.wersjaref = ms.vers)
          left join mwsconstlocs ml on (ml.ref = ms.mwsconstloc)
          join towary t on (dp.ktm = t.ktm)
          left join dokumpoz p1 on (p1.ref = dp.alttopoz and dp.fake = 1)
          left join towary t1 on (t1.ktm = p1.ktm and t1.usluga <> 1)
        where m.docgroup = :docgroup and m.mwsord = :mwsord and d.magazyn = :wh
          and dp.ilosc - coalesce(dp.ilosconmwsacts,0) > 0
          and (ms.lot = coalesce(dp.dostawa,0) or coalesce(dp.dostawa,0) = 0)
          and (ms.x_partia = dp.x_partia or dp.x_partia is null) -- XXX KBI
          and (ms.x_serial_no = dp.x_serial_no or dp.x_serial_no is null) -- XXX KBI
          and (ms.x_slownik = dp.x_slownik or coalesce(dp.x_slownik,0) = 0) -- XXX KBI
          and ms.ref is not null and ml.ref is not null
          and ml.wh = :wh and ml.goodsav = 0 and ml.locdest = 1 and ml.act > 0
          and t.usluga <> 1
          and (ms.x_blocked<>1) --XXX Ldz ZG 126909

        group by dp.wersjaref, coalesce(dp.dostawa,0), ms.mwsconstloc, ms.x_partia, ms.x_serial_no, ms.x_slownik
        having sum(case when (d.mwsdisposition = 1 or coalesce(t1.altposmode,0) = 1) then dp.ilosc else dp.iloscl end - dp.ilosconmwsacts) >= sum(ms.quantity)
          and sum(ms.blocked + ms.ordered) = 0
        order by ms.x_partia, (select count(distinct ms1.vers) from mwsstock ms1 where ms1.mwsconstloc = ms.mwsconstloc) desc
        into :mwsconstloc
    do begin
      ok = 1;
      stockquantity = 0;
      -- trzeba zabrac wszystki towary z lokacji w calej ilosci
      for
        select s.vers, sum(s.quantity)
          from mwsstock s
          where s.mwsconstloc = :mwsconstloc
          group by s.vers
          into vers, stockquantity
      do begin
        -- najpierw sprawdzamy czy jest towar z wybranymi dostawami na lokacji
        -- bez sensu jest dzialanie na WMS w trybie mieszanym wiec tego nie obsluguje
        for
          select coalesce(p.dostawa,0),
              sum(case when (d.mwsdisposition = 1 or coalesce(t1.altposmode,0) = 1) then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0))
            from mwsdocpostomwsord m
              left join dokumpoz p on (p.ref = m.docposid)
              left join dokumnag d on (m.docid = d.ref)
              join towary t on (p.ktm = t.ktm)
              left join dokumpoz p1 on (p1.ref = p.alttopoz and p.fake = 1)
              left join towary t1 on (t1.ktm = p1.ktm and t1.usluga <> 1)
            where m.docgroup = :docgroup and m.mwsord = :mwsord
              and d.magazyn = :wh
              and p.wersjaref = :vers and coalesce(p.dostawa,0) > 0
              and p.genmwsordsafter = 1
              and t.usluga <> 1
            group by p.wersjaref, coalesce(p.dostawa,0)
            --having sum(p.iloscl - coalesce(p.ilosconmwsacts,0)) > 0
            order by coalesce(p.dostawa,0) desc
            into doclot, docquantity
        do begin
          stocklotquantity = 0;
          select sum(s.quantity)
            from mwsstock s
            where s.mwsconstloc = :mwsconstloc and s.vers = :vers and s.lot = :doclot
            into stocklotquantity;
          if (stocklotquantity is null) then stocklotquantity = 0;
          stockquantity = stockquantity - case when stocklotquantity < docquantity then stocklotquantity else docquantity end;
        end
        -- sprawdzamy czy na pozcyjach sa ilosci wieksze niz na lokacji
        for
          select sum(case when (d.mwsdisposition = 1 or coalesce(t1.altposmode,0) = 1) then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0))
            from mwsdocpostomwsord m
              left join dokumpoz p on (p.ref = m.docposid)
              left join dokumnag d on (m.docid = d.ref)
              join towary t on (p.ktm = t.ktm)
              left join dokumpoz p1 on (p1.ref = p.alttopoz and p.fake = 1)
              left join towary t1 on (t1.ktm = p1.ktm and t1.usluga <> 1)
            where m.docgroup = :docgroup and m.mwsord = :mwsord and d.magazyn = :wh
              and p.wersjaref = :vers and coalesce(p.dostawa,0) = 0
              and (:x_partia is null or p.x_partia = :x_partia) -- XXX KBI
              and (:x_serial_no is null or p.x_serial_no = :x_serial_no) -- XXX KBI
              and (coalesce(:x_slownik,0) = 0 or p.x_slownik = :x_slownik) -- XXX KBI
              and p.genmwsordsafter = 1
              and t.usluga <> 1
            group by p.wersjaref, p.x_partia, p.x_serial_no, p.x_slownik
            --having sum(p.iloscl - coalesce(p.ilosconmwsacts,0)) > 0
            into docquantity
        do begin
          stocklotquantity = 0;
          select sum(s.quantity)
            from mwsstock s
            where s.mwsconstloc = :mwsconstloc and s.vers = :vers and (s.x_blocked<>1) --XXX Ldz ZG 126909
            into stocklotquantity;
          if (stocklotquantity is null) then stocklotquantity = 0;
          stockquantity = stockquantity - case when stocklotquantity < docquantity then stocklotquantity else docquantity end;
        end
        if (stockquantity > 0) then
          ok = 0;
      end
      if (ok = 1) then
        suspend;
    end
  end
  suspend;
end^
SET TERM ; ^
