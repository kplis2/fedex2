--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_WF_FAKTZAK_INSERTBKDOC(
      DOKPLIKREF DOKPLIK_ID,
      WFINSTANCEREF WFINSTANCE_ID,
      SYMBOL STRING255,
      TRANSDATE date,
      DESCRIPT STRING255)
  returns (
      REF BKDOCS_ID)
   as
declare variable DOCDATE date;
declare variable PAYDAY date;
declare variable RCVDATE date;
declare variable DICTDEF integer;
declare variable DICTPOS integer;
declare variable TYP integer;
declare variable VATREG STRING10;
declare variable COMPANY COMPANIES_ID;
declare variable AMOUNT MONEY;
declare variable PERIOD type of column BKDOCS.PERIOD;
declare variable BKREG BKREGS_ID;
declare variable VATGR GRVAT_ID;
declare variable TAXGR VAT_ID;
declare variable STAWKA CENY;
declare variable ANNOTATION STRING;
begin
  company = (select pvalue from GET_GLOBAL_PARAM('CURRENTCOMPANY'));
  --sprawdzamy czy już istnieje bkdoc
  --zachowujemy dokzlacz z WF_DECREE
  select dz.zdokum from dokzlacz dz
    where dz.ztable='BKDOCS' and dokument=:dokplikref
    into :ref;

  if(ref is null) then
  begin
    --wstawiamy nowy dokument

    rcvdate = (select WFFIELDVALUE from wf_getfieldvalue(:wfinstanceref,'_rcvdate'));
    transdate = (select WFFIELDVALUE from wf_getfieldvalue(:wfinstanceref,'_transdate'));

    select c.slodef, c.slopoz, d.typ
      from dokplik d
      left join cpodmioty c on c.ref = d.cpodmiot
      where d.ref = :DOKPLIKREF
      into :dictdef, :dictpos, :typ;

    dictdef = 6;
    dictpos = (select WFFIELDVALUE from wf_getfieldvalue(:wfinstanceref,'_dostawca'));


    /* konwersja typów z bazy firmowej ?
    if (typ = 15) then
      typ = 87;
    else if (typ = 19) then
      typ = 89;
    else if (typ = 20) then
      typ = 95;
    else if (typ = 18) then
      typ = 88;
    else if (typ = 21) then
      typ = 91;
    else if (typ = 22) then
      typ = 105;
    else if (typ = 23) then
      typ = 96;
    */

    symbol = substring(symbol from 1 for 20);
    descript = substring(descript from 1 for 80);
    bkreg = 'ZAK. KOSZ.';
    vatgr = '23';
    taxgr = 'SPK';
    stawka = (select stawka from vat where grupa = :vatgr);

    select vatreg from bkdoctypes
    where ref = :typ and company=:company
    into :vatreg;

    --nie trzeba w parametrach, z sql mamy dostep do parametrow procesu
    --o ile znamy id instancji
    amount = (select WFFIELDVALUE from wf_getfieldvalueasnumber(:wfinstanceref,'_kwotabrutto'));
    docdate = (select WFFIELDVALUE from wf_getfieldvalue(:wfinstanceref,'_datawyst'));
    payday = (select WFFIELDVALUE from wf_getfieldvalue(:wfinstanceref,'_terminplat'));
    annotation = (select WFFIELDVALUE from wf_getfieldvalue(:wfinstanceref,'_uwagi'));


     --tylko dla celów demo na releasie
    if(period is null) then
    select first 1 id from bkperiods
      where status=1 and sdate <= :transdate order by sdate desc
    into period;

    company = coalesce(company,(select first 1 ref from companies));
    if(not exists(select 1 from bkregs br where br.symbol=:bkreg and br.company=:company)) then
    begin
      --bkreg = 'ZAK. KOSZ.';
      bkreg = (select first 1 br.symbol from bkregs br where br.symbol starting with 'ZAK. KOSZ');
      if(bkreg is null) then
        bkreg = 'ZAK. KOSZ';--na worku
    end

    if(not exists(select 1 from bkdoctypes bt where bt.bkreg=:bkreg and bt.company=:company and bt.ref=:typ))then
      typ = 67;

    select vatreg from bkdoctypes
      where ref = :typ and company=:company
      into :vatreg;

    if(not exists(select 1 from grvat gv where gv.symbol=:taxgr))then
      taxgr = (select first 1 symbol from grvat gv);

    if(not exists(select 1 from vat v where v.grupa=:vatgr))then
    begin
      select first 1 grupa,stawka from vat v where v.grupa=:vatgr
      into vatgr, stawka;
    end

    --/demo

    execute procedure GEN_REF('BKDOCS') returning_values :REF;

    insert into bkdocs(ref,period, company, bkreg, doctype, docdate, transdate,
        payday,dictdef,dictpos,otable,oref,vatdate,vatreg,symbol, descript, annotation)
      values (:REF,:period, :company, :bkreg , :typ, :docdate, :transdate, :payday,
        :dictdef, :dictpos, 'WFINSTANCE', :WFINSTANCEREF, :rcvdate, :vatreg, :symbol, :descript, :annotation);

    insert into bkvatpos (bkdoc, grossv, netv, vatv, vatgr, taxgr, descript, vate)
    values (:ref, :amount, :amount - (:amount * :stawka / (100+:stawka)),
      :amount * :stawka / (100+:stawka), :vatgr, :taxgr, :descript, :amount * :stawka / (100+:stawka));    --taxgr vatgr?

    insert into dokzlacz(ztable, zdokum, dokument) values('BKDOCS', :ref, :dokplikref);
  end

  --zwracamy ref dokumentu
  suspend;
end^
SET TERM ; ^
