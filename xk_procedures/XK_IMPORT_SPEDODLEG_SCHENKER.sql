--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XK_IMPORT_SPEDODLEG_SCHENKER(
      SPOSDOST integer,
      LINIA varchar(1024) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable kodp varchar(10);
declare variable kodrejonu varchar(10);
declare variable trasa varchar(10);
declare variable dziesiata smallint;
declare variable sobota smallint;
declare variable miasto varchar(255);
declare variable wartosc varchar(255);
declare variable lp integer;
begin
  status = 0;
  msg = '';

  for
    select stringout, lp
      from parsestring(:linia,';')
      order by lp
    into :wartosc, :lp
  do begin
    if (:lp = 1) then kodp = substring(:wartosc from 1 for 10);
    else if (:lp = 2) then kodrejonu = substring(:wartosc from 1 for 10);
    else if (:lp = 3) then trasa = substring(:wartosc from 1 for 10);
    else if (:lp = 38) then miasto = substring(:wartosc from 1 for 255);

    if (:kodrejonu = '54') then begin
      if (:lp = 4) then dziesiata = cast(substring(:wartosc from 1 for 1) as smallint);
      else if (:lp = 5) then sobota = cast(substring(:wartosc from 1 for 1) as smallint);
    end
    else if (:kodrejonu = '56') then begin
      if (:lp = 6) then dziesiata = cast(substring(:wartosc from 1 for 1) as smallint);
      else if (:lp = 7) then sobota = cast(substring(:wartosc from 1 for 1) as smallint);
    end
    else if (:kodrejonu = '58') then begin
      if (:lp = 8) then dziesiata = cast(substring(:wartosc from 1 for 1) as smallint);
      else if (:lp = 9) then sobota = cast(substring(:wartosc from 1 for 1) as smallint);
    end
    else if (:kodrejonu = '62') then begin
      if (:lp = 10) then dziesiata = cast(substring(:wartosc from 1 for 1) as smallint);
      else if (:lp = 11) then sobota = cast(substring(:wartosc from 1 for 1) as smallint);
    end
    else if (:kodrejonu = '66') then begin
      if (:lp = 12) then dziesiata = cast(substring(:wartosc from 1 for 1) as smallint);
      else if (:lp = 13) then sobota = cast(substring(:wartosc from 1 for 1) as smallint);
    end
    else if (:kodrejonu = '68') then begin
      if (:lp = 14) then dziesiata = cast(substring(:wartosc from 1 for 1) as smallint);
      else if (:lp = 15) then sobota = cast(substring(:wartosc from 1 for 1) as smallint);
    end
    else if (:kodrejonu = '71') then begin
      if (:lp = 16) then dziesiata = cast(substring(:wartosc from 1 for 1) as smallint);
      else if (:lp = 17) then sobota = cast(substring(:wartosc from 1 for 1) as smallint);
    end
    else if (:kodrejonu = '75') then begin
      if (:lp = 18) then dziesiata = cast(substring(:wartosc from 1 for 1) as smallint);
      else if (:lp = 19) then sobota = cast(substring(:wartosc from 1 for 1) as smallint);
    end
    else if (:kodrejonu = '82') then begin
      if (:lp = 20) then dziesiata = cast(substring(:wartosc from 1 for 1) as smallint);
      else if (:lp = 21) then sobota = cast(substring(:wartosc from 1 for 1) as smallint);
    end
    else if (:kodrejonu = '83') then begin
      if (:lp = 22) then dziesiata = cast(substring(:wartosc from 1 for 1) as smallint);
      else if (:lp = 23) then sobota = cast(substring(:wartosc from 1 for 1) as smallint);
    end
    else if (:kodrejonu = '84') then begin
      if (:lp = 24) then dziesiata = cast(substring(:wartosc from 1 for 1) as smallint);
      else if (:lp = 25) then sobota = cast(substring(:wartosc from 1 for 1) as smallint);
    end
    else if (:kodrejonu = '86') then begin
      if (:lp = 26) then dziesiata = cast(substring(:wartosc from 1 for 1) as smallint);
      else if (:lp = 27) then sobota = cast(substring(:wartosc from 1 for 1) as smallint);
    end
    else if (:kodrejonu = '90') then begin
      if (:lp = 28) then dziesiata = cast(substring(:wartosc from 1 for 1) as smallint);
      else if (:lp = 29) then sobota = cast(substring(:wartosc from 1 for 1) as smallint);
    end
    else if (:kodrejonu = '91') then begin
      if (:lp = 30) then dziesiata = cast(substring(:wartosc from 1 for 1) as smallint);
      else if (:lp = 31) then sobota = cast(substring(:wartosc from 1 for 1) as smallint);
    end
    else if (:kodrejonu = '94') then begin
      if (:lp = 32) then dziesiata = cast(substring(:wartosc from 1 for 1) as smallint);
      else if (:lp = 33) then sobota = cast(substring(:wartosc from 1 for 1) as smallint);
    end
    else if (:kodrejonu = '96') then begin
      if (:lp = 34) then dziesiata = cast(substring(:wartosc from 1 for 1) as smallint);
      else if (:lp = 35) then sobota = cast(substring(:wartosc from 1 for 1) as smallint);
    end
    else if (:kodrejonu = '99') then begin
      if (:lp = 36) then dziesiata = cast(substring(:wartosc from 1 for 1) as smallint);
      else if (:lp = 37) then sobota = cast(substring(:wartosc from 1 for 1) as smallint);
    end
  end

  insert into spedodleg(kodp,kodrejonu,rejonsped,miasto,sposdost,krajid,trasa,s10,ssobota)
    values(:kodp,:kodrejonu,:kodp,:miasto,:sposdost,'PL',:trasa,:dziesiata, :sobota);
  msg = 'Dla kodu '||:kodp||' import poprawny';

  suspend;
end^
SET TERM ; ^
