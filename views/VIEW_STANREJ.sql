--@@@***SKEY***@@@--
CREATE OR ALTER VIEW VIEW_STANREJ (
      STANSPRZED,-- STANSPRZED_ID
      REJFAK    ,-- REJFAK_ID
      ZAKUPU    ,-- SMALLINT_ID
      COMPANY   ,-- COMPANIES_ID
      OPERATOR  -- OPERATOR_ID
  )
  AS 
select sr.stansprzed, rf.symbol, rf.zakup, company, so.operator
  from stanrej sr join rejfak rf on sr.rejfak = rf.symbol
                  join stansprzed sp on sp.stanowisko = sr.stansprzed
                  join oddzialy od on sp.oddzial = od.oddzial
                  join stanoper so on so.stansprzed = sr.stansprzed
  where sr.stansprzed is not null
  order by sr.stansprzed
  ;
