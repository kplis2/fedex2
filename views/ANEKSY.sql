--@@@***SKEY***@@@--
CREATE OR ALTER VIEW ANEKSY (
      REF             ,-- ECONTRANNEXES_ID
      EMPLOYEE        ,-- EMPLOYEES_ID
      FROMDATE        ,-- TIMESTAMP_ID
      EMPLCONTRACT    ,-- EMPLCONTRACTS_ID
      WORKPOST        ,-- INTEGER_ID
      WORKDIM         ,-- FLOATINGPOINT
      DIMNUM          ,-- SMALLINT_ID
      DIMDEN          ,-- SMALLINT_ID
      BRANCH          ,-- ODDZIAL_ID
      DEPARTMENT      ,-- DEPARTMENT_ID
      SALARY          ,-- MONEY
      PAYMENTTYPE     ,-- SMALLINT_ID
      PAYMENTTYPE_OPIS,-- CHAR(48) CHARACTER SET UTF8                           
      CADDSALARY      ,-- MONEY
      FUNCSALARY      ,-- MONEY
      CALENDAR        ,-- ECALENDARS_ID
      ATYPE           ,-- SMALLINT_ID
      ATYPE_OPIS      ,-- CHAR(52) CHARACTER SET UTF8                           
      BKSYMBOL        ,-- ANALYTIC_ID
      LOCAL           ,-- STRING80
      TODATE          ,-- TIMESTAMP_ID
      RATIO           -- NUMERIC_14_3
  )
  AS 
select  REF, EMPLOYEE, FROMDATE, EMPLCONTRACT, WORKPOST, WORKDIM, DIMNUM, DIMDEN,
        BRANCH, DEPARTMENT, SALARY, PAYMENTTYPE,
        case when PAYMENTTYPE=0 then 'miesiÄ™czne' else case when paymenttype = 1 then 'godzinowe' else case when paymenttype=2 then 'akordowe' else 'caĹ‚oĹ›ciowe' end end end,
         CADDSALARY, FUNCSALARY,
        CALENDAR, ATYPE,
        case when atype=0 then 'aneks' else 'oddelegowanie' end, 
        BKSYMBOL, LOCAL, TODATE, RATIO
from econtrannexes
  ;
