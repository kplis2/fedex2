--@@@***SKEY***@@@--
CREATE OR ALTER VIEW SYS_FIELDS (
      TABLENAME    ,-- CHAR(31) CHARACTER SET UNICODE_FSS                    
      FIELDNAME    ,-- CHAR(31) CHARACTER SET UNICODE_FSS                    
      FIELDPOSITION-- 
  )
  AS 
select r.rdb$relation_name, r.rdb$field_name, r.rdb$field_position
    from rdb$relation_fields r
    order by r.rdb$relation_name,r.rdb$field_position
  ;
