--@@@***SKEY***@@@--
CREATE OR ALTER VIEW ECALENDARS_VIEW (
      REF             ,-- ECALENDARS_ID
      NAME            ,-- STRING20
      DESCRIPT        ,-- STRING
      PATTKIND        ,-- SMALLINT_ID
      PERIODB         ,-- SMALLINT_ID
      COMPANY         ,-- COMPANIES_ID
      WORKSYSTEM      ,-- EDICTWORKSYSTEMS_ID
      NORM_PER_DAY    ,-- DURATION
      NORM_PER_WEEK   ,-- DURATION
      ACCOUNT_LENGTH  ,-- SMALLINT_ID
      ADD_HOLIDAY     ,-- SMALLINT_ID
      ACCOUNT_PERIOD  ,-- SMALLINT_ID
      NIGHTHOURS_START,-- TIMEOFDAY
      STARTDATE       ,-- DATE_ID
      NIGHTHOURS_END  ,-- TIMEOFDAY
      FYEAR           ,-- INTEGER
      LYEAR           ,-- INTEGER
      YEARS           -- BIGINT
  )
  AS 
with ecalaggrs as (
  select calendar, min(distinct cyear) as mincyear, max(distinct cyear) as maxcyear
    from ecaldays
    group by calendar
)
select e.ref, e.name, e.descript, e.pattkind, e.periodb, e.company, e.worksystem, e.norm_per_day, e.norm_per_week,
       e.account_length, e.add_holiday, e.account_period, e.nighthours_start, e.startdate, e.nighthours_end,
       l.mincyear, l.maxcyear, l.maxcyear - l.mincyear + 1
  from ecalendars e
  left join ecalaggrs l on l.calendar = e.ref
  ;
