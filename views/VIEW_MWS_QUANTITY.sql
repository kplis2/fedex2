--@@@***SKEY***@@@--
CREATE OR ALTER VIEW VIEW_MWS_QUANTITY (
      GOOD    ,-- VARCHAR(40) CHARACTER SET UTF8                           
      VERS    ,-- INTEGER
      WH      ,-- VARCHAR(3) CHARACTER SET UTF8                           
      BRANCH  ,-- VARCHAR(10) CHARACTER SET UTF8                           
      QUANTITY-- NUMERIC(18,4)
  )
  AS 
select vs.good, vs.vers, vs.wh, vs.branch, sum(vs.quantity) as quantity
    from
      (select s.good, s.vers, s.wh, s.branch,
          coalesce(sum(s.quantity - s.blocked + s.ordered),0) as quantity
        from mwsstock s
          join mwsconstlocs c on (s.mwsconstloc = c.ref)
        where c.act > 0
          and c.locdest in (1,2,3,4,5,9)
        group by good, vers, wh, s.branch
      union
      select a.good, a.vers, a.wh, o.branch,
          coalesce(sum(a.quantity),0) as quantity
        from mwsords o
          left join mwsacts a on (a.mwsord = o.ref)
        where o.mwsordtypedest = 9
          and o.status > 0
          and o.status < 3
          and a.status > 0
          and a.status < 6
        group by good, vers, wh, branch
      union
      select p.ktm as good, p.wersjaref as vers, n.magazyn as wh, n.oddzial as branch,
          -coalesce(sum(case when n.mwsdisposition = 1 then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0)),0) as quantity
        from dokumnag n
          join dokumpoz p on (n.ref = p.dokument)
          left join defdokummag d on (d.typ = n.typ and d.magazyn = n.magazyn)
        where n.wydania = 1
          and n.mwsdoc = 1
          and n.mwsdone = 0
          and coalesce(n.blockmwsords,0) = 0
          and n.frommwsord is null
          and ((n.akcept = 1 and p.iloscl > p.ilosconmwsacts) or
               (n.mwsdisposition = 1 and p.ilosc > p.ilosconmwsacts))
          and p.genmwsordsafter = 1
          and coalesce(p.fake,0) = 0
          and coalesce(p.havefake,0) = 0
          and coalesce(d.afterackproc,'') <> ''
          and d.mwsordwaiting = 1
        group by good, vers, wh, branch) vs
    group by vs.good, vs.vers, vs.wh, vs.branch
    order by vs.good, vs.vers, vs.wh, vs.branch
  ;
