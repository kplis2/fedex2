--@@@***SKEY***@@@--
CREATE OR ALTER VIEW ULGI_PODATKOWE (
      REF         ,-- ETAXALLOWANCE_ID
      PERSON      ,-- PERSONS_ID
      AYEAR       ,-- INTEGER_ID
      FROMDATE    ,-- TIMESTAMP_ID
      INCALLOWANCE,-- MONEY
      INCUSED     ,-- MONEY
      TAXALLOWANCE,-- MONEY
      TAXUSED     -- MONEY
  )
  AS 
select t.REF, t.PERSON, t.AYEAR, t.FROMDATE, t.INCALLOWANCE, t.INCUSED, t.TAXALLOWANCE, t.TAXUSED
  from etaxallowances t
  ;
