--@@@***SKEY***@@@--
CREATE OR ALTER VIEW STAN_RODZINNY (
      REF           ,-- EMPLFAMILY_ID
      EMPLOYEE      ,-- EMPLOYEES_ID
      PERSON        ,-- PERSONS_ID
      CONECTTYPE    ,-- EDICTZUSCODES_ID
      CONECTTYPECODE,-- STRING20
      CONECTTYPEOPIS,-- STRING1024
      FNAME         ,-- PERSON_NAMES
      SNAME         ,-- PERSON_NAME
      BIRTHDATE     ,-- TIMESTAMP_ID
      PESEL         ,-- PESEL
      INSURANCE     ,-- CHAR(12) CHARACTER SET UTF8                           
      EVIDENCENO    ,-- EVIDENCENO
      PASSPORTNO    ,-- PASSPORTNO
      NIP           ,-- NIP
      POSTCODE      ,-- POSTCODE
      STREET        ,-- STREET_ID
      HOUSENO       ,-- STRING20
      LOCALNO       ,-- STRING20
      PHONE         ,-- PHONES_ID
      FAX           ,-- STRING20
      COMMUNE       ,-- EDICTGUSCODES_ID
      COMMUNEOPIS   ,-- STRING
      CITY          ,-- CITY_ID
      POST          ,-- STRING20
      INMANAGE      ,-- CHAR(12) CHARACTER SET UTF8                           
      INVALIDITY    ,-- INTEGER_ID
      INVALIDITYCODE,-- STRING20
      INVALIDITYOPIS,-- STRING1024
      DELIVERANCE   ,-- CHAR(76) CHARACTER SET UTF8                           
      INSURCARDNO   ,-- STRING20
      INSURCARDDATE ,-- TIMESTAMP_ID
      OTHERADDRESS  -- SMALLINT_ID
  )
  AS 
select e.REF, EMPLOYEE, PERSON, CONECTTYPE, z.code, z.descript, FNAME, SNAME, BIRTHDATE, PESEL,
    case when insurance = 1 then 'tak' else 'nie' end, 
    EVIDENCENO, PASSPORTNO, NIP, POSTCODE, STREET, HOUSENO, LOCALNO, PHONE, FAX, COMMUNE, g.descript,
    CITY, POST, case when inmanage = 1 then 'tak' else 'nie' end,
    INVALIDITY, z2.code, z2.descript,
    case when deliverance = 0 then 'nie złożył' else case when deliverance = 1 then 'złożył' else 'nie zamierza złożyć' end end,
    INSURCARDNO, INSURCARDDATE, OTHERADDRESS
  from emplfamily e
    left join edictzuscodes z on e.conecttype = z.ref
    left join edictguscodes g on e.commune = g.ref
    left join edictzuscodes z2 on z2.ref = e.invalidity
  ;
