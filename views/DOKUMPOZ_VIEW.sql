--@@@***SKEY***@@@--
CREATE OR ALTER VIEW DOKUMPOZ_VIEW (
      REF     ,-- DOKUMPOZ_ID
      KTM     ,-- KTM_ID
      NAZWA   ,-- TOWARY_NAZWA
      DOKUMENT-- DOKUMNAG_ID
  )
  AS 
select p.ref, p.ktm, t.nazwa, p.dokument
  from dokumpoz p join towary t on p.ktm = t.ktm
  where coalesce(p.fake,0) = 0
  ;
