--@@@***SKEY***@@@--
CREATE OR ALTER VIEW STOPKA_FAKTURY (
      DOKUMENT      ,-- FAKTURA_ID
      VAT           ,-- VAT_ID
      WARTOSC_NETTO ,-- BIGINT
      WARTOSC_BRUTTO,-- BIGINT
      WARTOSC_VAT   -- BIGINT
  )
  AS 
select DOKUMENT, VAT,
SUMWARTNETZL-PSUMWARTNETZL, SUMWARTBRUZL-PSUMWARTBRUZL, SUMWARTVATZL-PSUMWARTVATZL
from ROZFAK
  ;
