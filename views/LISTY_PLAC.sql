--@@@***SKEY***@@@--
CREATE OR ALTER VIEW LISTY_PLAC (
      REF        ,-- EPAYROLLS_ID
      SYMBOL     ,-- STRING20
      PRTYPE     ,-- EPROLLSTYPES_ID
      CPER       ,-- OKRES_ID
      TPER       ,-- OKRES_ID
      IPER       ,-- OKRES_ID
      NAME       ,-- STRING30
      ISADDIT    ,-- SMALLINT_ID
      DESCRIPT   ,-- STRING
      NUMBER     ,-- SMALLINT_ID
      PAYDAY     ,-- TIMESTAMP_ID
      STATUS     ,-- SMALLINT_ID
      STATUS_OPIS-- CHAR(52) CHARACTER SET UTF8                           
  )
  AS 
select REF, SYMBOL, PRTYPE, CPER, TPER, IPER, NAME, ISADDIT, DESCRIPT, NUMBER, PAYDAY, STATUS,
    case when status = 0 then 'w redakcji' else case when status = 1 then 'zaakceptowany' else 'zaksięgowany' end end
  from epayrolls
  where empltype = 1
  ;
