--@@@***SKEY***@@@--
CREATE OR ALTER VIEW DANE_PODATKOWE (
      REF          ,-- EMPLTAXINFO_ID
      EMPLOYEE     ,-- EMPLOYEES_ID
      FROMDATE     ,-- TIMESTAMP_ID
      TAXOFFICE    ,-- TAXOFFICE_ID
      TAXOFFICEOPIS,-- STRING60
      LEVELDEC     ,-- CHAR(44) CHARACTER SET UTF8                           
      ALLOWANCE    ,-- CHAR(32) CHARACTER SET UTF8                           
      COSTS        ,-- CHAR(44) CHARACTER SET UTF8                           
      EXTAMOUNT    ,-- MONEY
      EXTDATE      -- TIMESTAMP_ID
  )
  AS 
select t.REF, EMPLOYEE, FROMDATE, TAXOFFICE, i.name,
    case when leveldec = 0 then 'normalny' else case when leveldec = 1 then 'obniżony' else 'podwyższony' end end,
    case when allowance = 0 then 'brak' else case when allowance = 1 then 'normalna' else 'podwójna' end end,
    case when costs = 0 then 'brak' else case when costs = 1 then 'normalne' else 'podwyższone' end end,
    EXTAMOUNT, EXTDATE
  from empltaxinfo T
    left join einternalrevs i on i.ref = t.taxoffice
  ;
