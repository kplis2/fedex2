--@@@***SKEY***@@@--
CREATE OR ALTER VIEW ERECRUITSAPP_QUESTVIEW (
      HEADERREF   ,-- EQUESTHEADER_ID
      QUESTIONREF ,-- EQUESTPOS_ID
      LP          ,-- INTEGER_ID
      QUESTION    ,-- EQUESTIONSTRING
      QUESTIONTYPE,-- SMALLINT_ID
      ANSWERLP    ,-- SMALLINT_ID
      DESCRIPT    -- EQUESTANSWERSTRING
  )
  AS 
select qh.ref,qp.ref,qp.lp,qp.question,qp.questiontype,pad.lp as answerlp,pad.descript from equestheader qh
join equestpos qp on qh.ref=qp.equestheader
join erecruits er on qh.erecruit = er.ref
join equestposdef pd on pd.equestheaderdef=er.equestheaderdef and pd.lp=qp.lp
left join equestposansdef pad on pad.equestposdef=pd.ref
order by qp.lp,pad.lp
  ;
