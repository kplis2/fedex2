--@@@***SKEY***@@@--
CREATE OR ALTER VIEW VIEW_COMPANIES (
      REF           ,-- COMPANIES_ID
      SYMBOL        ,-- STRING20
      NAME          ,-- STRING80
      PATTERN       ,-- SMALLINT_ID
      PARENT_COMPANY-- COMPANIES_ID
  )
  AS 
select
    ref,
    symbol,
    name,
    pattern,
    parent_company
from companies
  ;
