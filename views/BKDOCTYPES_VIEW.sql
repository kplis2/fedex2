--@@@***SKEY***@@@--
CREATE OR ALTER VIEW BKDOCTYPES_VIEW (
      REF                ,-- BKDOCTYPES_ID
      COMPANY            ,-- COMPANIES_ID
      BKREG              ,-- BKREGS_ID
      NAME               ,-- STRING40
      CREDITNOTE         ,-- SMALLINT_ID
      KIND               ,-- SMALLINT_ID
      DICTDEF            ,-- SLO_ID
      ISDEFAULT          ,-- SMALLINT
      MULTICURR          ,-- MULTICURR_ID
      AUTOREGSCHEME      ,-- AUTOREGSCHEME_ID
      VPERIODPROMPT      ,-- SMALLINT_ID
      VATREG             ,-- VATREGS_ID
      DANEDODSTAT        ,-- STRING60
      ISVATCORRECTION    ,-- SMALLINT_ID
      BKVATCORRECTIONTYPE,-- BKVATCORRECTIONTYPES_ID
      CORRBKDOCTYPES     -- BKDOCTYPES_ID
  )
  AS 
select
    ref,
    company,
    bkreg,
    name,
    creditnote,
    kind,
    dictdef,
    isdefault,
    multicurr,
    autoregscheme,
    vperiodprompt,
    vatreg,
    danedodstat,
    isvatcorrection,
    bkvatcorrectiontype,
    corrbkdoctypes
from bkdoctypes
  ;
