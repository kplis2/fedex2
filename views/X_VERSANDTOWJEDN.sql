--@@@***SKEY***@@@--
CREATE OR ALTER VIEW X_VERSANDTOWJEDN (
      REF     ,-- VARCHAR(23) CHARACTER SET UTF8                           
      TOWJEDN ,-- TOWJEDN
      WERSJA  ,-- INTEGER
      KTM     ,-- KTM_ID
      PRZELICZ,-- NUMERIC(14,4)
      JEDN    -- JEDN_MIARY
  )
  AS 
select k.ref||'%'||o.ref as ref,
       o.ref as towjedn,
       k.ref as wersja,
       k.ktm,
       o.przelicz,
       o.jedn
      from wersje k
        left join towjedn o on(o.ktm = k.ktm)

  ;
