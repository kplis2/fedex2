--@@@***SKEY***@@@--
CREATE OR ALTER VIEW POZOPER_VIEW (
      REF     ,-- RKPOZOPER_ID
      OPERACJA,-- RKDEFOPER_ID
      SYMBOL  ,-- RKPOZOPER_SYM
      NAZWA   ,-- STRING40
      PM      -- CHAR(4) CHARACTER SET UTF8                           
  )
  AS 
select p.ref, p.operacja, p.symbol, p.nazwa, p.pm
  from rkpozoper p
  ;
