--@@@***SKEY***@@@--
CREATE OR ALTER VIEW POZZAMUPVIEW (
      REF   ,-- NAGZAMUP_ID
      REFPOZ,-- POZZAM_ID
      ILOSC ,-- ILOSCI_ZAM
      KTM   ,-- VARCHAR(40) CHARACTER SET UTF8                           
      JEDN  ,-- VARCHAR(5) CHARACTER SET UTF8                           
      NAZWA -- VARCHAR(80) CHARACTER SET UTF8                           
  )
  AS 
select
pozzamup.nagzamup,
pozzamup.refpoz,
pozzamup.ilosc,
case when (nagzamup.typdok = 'L')then wersje.ktm else pozzam.ktm end,
case when (tj1.jedn is null) then (case when (tj2.jedn is null) then listywysdpoz.miarao else tj2.jedn end) else tj1.jedn end,
case when (wersje.ktm is null) then (case when (t1.nazwa is not null) then t1.nazwa else listywysdpoz.opis end) else (case when (t2.nazwa is not null) then t2.nazwa else listywysdpoz.opis end) end
from nagzamup
  join pozzamup on (pozzamup.nagzamup = nagzamup.ref)
  left join listywysdpoz on (listywysdpoz.ref = pozzamup.refpoz)
  left join pozzam on (pozzam.ref = pozzamup.refpoz)
  left join wersje on (listywysdpoz.wersjaref = wersje.ref)
  left join towary t1 on (t1.ktm = pozzam.ktm)
  left join towary t2 on (t2.ktm = wersje.ktm)
  left join towjedn tj1 on (tj1.ktm = t1.ktm and tj1.glowna = 1)
  left join towjedn tj2 on (tj2.ktm = t2.ktm and tj2.glowna = 1)
  ;
