--@@@***SKEY***@@@--
CREATE OR ALTER VIEW PR_CANCELED_PRORDS (
      REF,-- INTEGER
      ID -- VARCHAR(30) CHARACTER SET UTF8                           
  )
  AS 
select n.ref, n.id
  from prschedguides g
    left join pozzam p on (p.ref = g.pozzam)
    left join nagzam n on (n.ref = p.zamowienie)
  where g.anulowano = 1 and g.status < 2 and p.anulowano = 1
    and exists(select first 1 1 from prschedguidedets d where d.prschedguide = g.ref and d.quantity > 0)
  group by n.id, n.ref
  ;
