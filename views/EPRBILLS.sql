--@@@***SKEY***@@@--
CREATE OR ALTER VIEW EPRBILLS (
      REF          ,-- EPAYROLLS_ID
      BILLDATE     ,-- TIMESTAMP_ID
      PAYDAY       ,-- TIMESTAMP_ID
      PVALUE       ,-- CENY
      STATUS       ,-- SMALLINT_ID
      EMPLCONTRACT ,-- EMPLCONTRACTS_ID
      CONTRNO      ,-- STRING20
      ECONTRTYPE   ,-- ECONTRTYPES_ID
      FROMDATE     ,-- TIMESTAMP_ID
      TODATE       ,-- TIMESTAMP_ID
      PRCOSTS      ,-- NUMERIC_14_2
      EMPLOYEE     ,-- EMPLOYEES_ID
      PERSON       ,-- PERSONS_ID
      PERSONNAMES  ,-- STRING120
      FNAME        ,-- PERSON_NAMES
      SNAME        ,-- PERSON_NAME
      COMPANY      ,-- COMPANIES_ID
      SYMBOL       ,-- STRING20
      NUMBER       ,-- SMALLINT_ID
      LUMPSUMTAX   ,-- SMALLINT_ID
      CORLUMPSUMTAX,-- EPAYROLLS_ID
      CPER         ,-- OKRES_ID
      IPER         ,-- OKRES_ID
      TPER         ,-- OKRES_ID
      EHRMSTATUS   ,-- SMALLINT_ID
      BTRANSFER    ,-- BTRANSFER_ID
      BAILIFFBTR   -- BTRANSFER_ID
  )
  AS 
select pr.ref, pr.billdate, pr.payday, pr.pvalue, pr.status, pr.emplcontract, c.contrno, c.econtrtype, c.fromdate,
    c.todate, c.prcosts, c.employee, e.person, e.personnames, e.fname, e.sname, e.company, pr.symbol, pr.number,
    pr.lumpsumtax, pr.corlumpsumtax, pr.cper, pr.iper, pr.tper, pr.ehrmstatus, pr.btransfer, pr.bailiffbtr --PR54430 
  from epayrolls pr
    join emplcontracts c on (c.ref = pr.emplcontract)
    join employees e on (c.employee = e.ref)
    join empltypes t on (t.ref = c.empltype and t.browsetype = 2) --PR64361
  ;
