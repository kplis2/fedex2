--@@@***SKEY***@@@--
CREATE OR ALTER VIEW OSOBY (
      REF           ,-- PERSONS_ID
      PERSON        ,-- STRING120
      FNAME         ,-- PERSON_NAMES
      SNAME         ,-- PERSON_NAME
      MNAME         ,-- PERSON_NAME
      FATHERNAME    ,-- PERSON_NAME
      MOTHERNAME    ,-- PERSON_NAME
      MOTHMNAME     ,-- PERSON_NAME
      SEX           ,-- SMALLINT_ID
      SEXOPIS       ,-- CHAR(36) CHARACTER SET UTF8                           
      BIRTHDATE     ,-- TIMESTAMP_ID
      BIRTHPLACE    ,-- STRING100
      NATIONALITY   ,-- STRING20
      CITIZENSHIP   ,-- STRING20
      FOREIGNER     ,-- SMALLINT_ID
      STEADYSTAYCARD,-- SMALLINT_ID
      EVIDENCENO    ,-- EVIDENCENO
      EVIDISSUEDBY  ,-- STRING60
      PASSPORTNO    ,-- PASSPORTNO
      PASSISSUEDBY  ,-- STRING60
      PESEL         ,-- PESEL
      NIP           ,-- NIP
      PROFESSION    ,-- STRING40
      PHOTOFILE     ,-- STRING
      EMAIL         ,-- EMAILS_ID
      CELLPHONE     ,-- PHONES_ID
      ACTIV         ,-- SMALLINT_ID
      OEMAIL        ,-- EMAILS_ID
      OMOBILE       ,-- PHONES_ID
      OPHONE        ,-- PHONES_ID
      EXTENSION     ,-- STRING20
      PHONE         ,-- PHONES_ID
      MARSTATUS     -- STRING
  )
  AS 
select PERSONS.REF, PERSON, FNAME, SNAME, MNAME, FATHERNAME, MOTHERNAME, MOTHMNAME,
SEX, case when sex=0 then 'kobieta' else 'mężczyzna' end, BIRTHDATE, BIRTHPLACE, NATIONALITY, CITIZENSHIP, FOREIGNER,
STEADYSTAYCARD, EVIDENCENO, EVIDISSUEDBY, PASSPORTNO, PASSISSUEDBY,
PESEL, NIP, PROFESSION, PHOTOFILE, EMAIL, CELLPHONE,
ACTIV, OEMAIL, OMOBILE, OPHONE, EXTENSION, PHONE, EDICTMARSTATUS.status
from PERSONS
left join EDICTMARSTATUS on (PERSONS.marstatus = edictmarstatus.ref)
  ;
