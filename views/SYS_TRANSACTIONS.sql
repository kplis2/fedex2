--@@@***SKEY***@@@--
CREATE OR ALTER VIEW SYS_TRANSACTIONS (
      ATTACHMENT       ,-- 
      MONUSER          ,-- CHAR(31) CHARACTER SET UNICODE_FSS                    
      MONREMOTE_ADDRESS,-- VARCHAR(255) CHARACTER SET ASCII                          
      MONTIMESTAMP     ,-- TIMESTAMP
      MONTRANSTIMESTAMP,-- TIMESTAMP
      TIMED            ,-- BIGINT
      MONREMOTE_PROCESS,-- VARCHAR(255) CHARACTER SET UNICODE_FSS                    
      MONTRANSACTION_ID,-- 
      NAZWA            ,-- STRING60
      MONSTATE         ,-- 
      MONSERVER_PID    ,-- 
      SQL_TEXT         -- BLOB SUB_TYPE 1 CHARACTER SET UNICODE_FSS                    
  )
  AS 
select a.mon$attachment_id as attachment, a.mon$user as monuser, a.mon$remote_address as monremote_address,
    a.mon$timestamp as montimestamp, s.mon$timestamp as MONTRANSTIMESTAMP, ((current_timestamp(0) - s.mon$timestamp)*1440) as TIMED,  a.mon$remote_process as mon$remote_process,
    s.mon$transaction_id as mon$transaction_id, o.nazwa as nazwa, s.mon$state as monstate,
    a.mon$server_pid as monserver_pid, st.mon$sql_text
  from mon$attachments a
    left join mon$transactions s on (s.mon$attachment_id = a.mon$attachment_id)
    left join mon$statements st on (st.mon$attachment_id = a.mon$attachment_id and st.mon$transaction_id = s.mon$transaction_id)
    left join globalparams g on (g.connectionid = a.mon$attachment_id and g.psymbol = 'AKTUOPERATOR')
    left join operator o on (o.ref = g.pvalue)
  where s.mon$transaction_id is not null
  order by s.mon$transaction_id
  ;
