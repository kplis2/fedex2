--@@@***SKEY***@@@--
CREATE OR ALTER VIEW GODZINY (
      REF         ,-- EWORKEDHOURS_ID
      PERIOD      ,-- PERIOD_ID
      EMPLOYEE    ,-- EMPLOYEES_ID
      ECOLUMN     ,-- ECOLUMNS_ID
      ECOLUMN_OPIS,-- STRING22
      AMOUNT      ,-- MONEY
      HDATE       ,-- TIMESTAMP_ID
      BKSYMBOL    -- ACCOUNT_ID
  )
  AS 
select h.REF, h.PERIOD, h.EMPLOYEE, h.ECOLUMN, c.name, h.AMOUNT, h.HDATE, h.BKSYMBOL
  from eworkedhours  h
    join ecolumns c on c.number = h.ecolumn
  ;
