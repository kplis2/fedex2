--@@@***SKEY***@@@--
CREATE OR ALTER VIEW KANDYDACI (
      REF           ,-- ECANDIDATES_ID
      FNAME         ,-- PERSON_NAMES
      SNAME         ,-- PERSON_NAME
      SEX           ,-- SMALLINT_ID
      SEX_OPIS      ,-- CHAR(36) CHARACTER SET UTF8                           
      BIRTHYEAR     ,-- SMALLINT_ID
      EDUCATION     ,-- EDICTZUSCODES_ID
      EDUCATION_OPIS,-- STRING1024
      CONTACT       ,-- STRING
      DESCRIPT      ,-- STRING
      LASTRECRUIT   ,-- ERECRUITS_ID
      BIRTHDATE     ,-- DATE_ID
      PHONE         ,-- PHONES_ID
      EMAIL         ,-- EMAILS_ID
      SUBMITDATE    ,-- DATE_ID
      AVAILABLEDATE ,-- DATE_ID
      SALEXPECTATION,-- MONEY
      SALPROPOSITION-- MONEY
  )
  AS 
select c.REF, c.FNAME, c.SNAME, c.SEX,
  case when c.sex = 1 then 'mężczyzna' else 'kobieta' end, 
  c.BIRTHYEAR, c.EDUCATION, z.descript, c.CONTACT, c.DESCRIPT, c.LASTRECRUIT,
  c.BIRTHDATE, c.PHONE, c.EMAIL, c.SUBMITDATE, c.AVAILABLEDATE, c.SALEXPECTATION, c.SALPROPOSITION
  from ecandidates c
    left join edictzuscodes z on c.education = z.ref
  where (z.dicttype = 6 or z.dicttype is null)
  ;
