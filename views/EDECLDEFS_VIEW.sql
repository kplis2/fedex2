--@@@***SKEY***@@@--
CREATE OR ALTER VIEW EDECLDEFS_VIEW (
      SYMBOL    ,-- STRING10
      VARIANT   ,-- STRING10
      CODE      ,-- STRING10
      SYSTEMCODE,-- STRING20
      REF       ,-- EDECLDEFS_ID
      ISGROUP   -- SMALLINT_ID
  )
  AS 
select SYMBOL,VARIANT,CODE,SYSTEMCODE,REF,ISGROUP
from EDECLDEFS
  ;
