--@@@***SKEY***@@@--
CREATE OR ALTER VIEW SWIADECTWA_PRACY (
      REF               ,-- EWORKCERTIFS_D
      EMPLOYEE          ,-- EMPLOYEES_ID
      FROMDATE          ,-- TIMESTAMP_ID
      TODATE            ,-- TIMESTAMP_ID
      FIRMNAME          ,-- STRING80
      WORKPOST          ,-- STRING60
      ETERMINATION      ,-- INTEGER_ID
      ETERMINATIONOPIS  ,-- STRING
      STAZ_URLOPOWY     ,-- CHAR(12) CHARACTER SET UTF8                           
      STAZ_ZEWNETRZNY   ,-- CHAR(12) CHARACTER SET UTF8                           
      STAZ_WEWNETRZNY   ,-- CHAR(12) CHARACTER SET UTF8                           
      STAZ_OGOLEM       ,-- CHAR(12) CHARACTER SET UTF8                           
      STAZ_JUBILEUSZOWY ,-- CHAR(12) CHARACTER SET UTF8                           
      STAZ_W_SL_CYWILNEJ,-- CHAR(12) CHARACTER SET UTF8                           
      SYEARS            ,-- INTEGER_ID
      SMONTHS           ,-- INTEGER_ID
      SDAYS             -- INTEGER_ID
  )
  AS 
select w.REF, EMPLOYEE, FROMDATE, TODATE, FIRMNAME, WORKPOST,
    ETERMINATION, t.reason,
    case when seniorityflags like '%;STU;%' then 'tak' else 'nie' end,
    case when seniorityflags like '%;STZ;%' then 'tak' else 'nie' end,
    case when seniorityflags like '%;STW;%' then 'tak' else 'nie' end,
    case when seniorityflags like '%;STO;%' then 'tak' else 'nie' end,
    case when seniorityflags like '%;STJ;%' then 'tak' else 'nie' end,
    case when seniorityflags like '%;STC;%' then 'tak' else 'nie' end,
    SYEARS, SMONTHS, SDAYS
  from eworkcertifs w
    join eterminations t on t.ref = w.etermination
  ;
