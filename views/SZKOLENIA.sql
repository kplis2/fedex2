--@@@***SKEY***@@@--
CREATE OR ALTER VIEW SZKOLENIA (
      REF          ,-- ETRAININGS_ID
      STATUS       ,-- CHAR(40) CHARACTER SET UTF8                           
      TRAINFIRM    ,-- ETRAINFIRMS_ID
      DESCRIPT     ,-- STRING
      TRAINER      ,-- STRING40
      SUBJECT      ,-- STRING80
      FROMDATE     ,-- TIMESTAMP_ID
      TODATE       ,-- TIMESTAMP_ID
      LOCATION     ,-- STRING60
      COMPANY      ,-- COMPANIES_ID
      TRAINNEED    ,-- ETRAINNEEDS_ID
      DURATION     ,-- NUMERIC_14_2
      TRAINTYPE    ,-- CHAR(40) CHARACTER SET UTF8                           
      COSTTYPE     ,-- CHAR(36) CHARACTER SET UTF8                           
      PERSONCOST   ,-- NUMERIC_14_2
      TOTALCOST    ,-- NUMERIC_14_2
      ADDPERSONCOST-- NUMERIC_14_2
  )
  AS 
select REF,  case when status = 0 then 'planowane' else 'zakończone' end,
    TRAINFIRM, DESCRIPT, TRAINER, SUBJECT, FROMDATE, TODATE,
    LOCATION, COMPANY, TRAINNEED, DURATION,
    case when traintype = 0 then 'wewnętrzne' else 'zewnętrzne' end,
    case when costtype = 0 then 'na osobę' else 'całkowity' end,
    PERSONCOST, TOTALCOST, ADDPERSONCOST
  from etrainings
  ;
