--@@@***SKEY***@@@--
CREATE OR ALTER VIEW KARTOTEKI_DODATKOWE (
      REF         ,-- EMPLADDFILES_ID
      EMPLOYEE    ,-- EMPLOYEES_ID
      FILETYPE    ,-- EMPLADDFILETYPES_ID
      FILETYPEOPIS,-- STRING30
      FROMDATE    ,-- TIMESTAMP_ID
      VALIDITYDATE,-- TIMESTAMP_ID
      DESCRIPT    ,-- MEMO
      TODATE      -- TIMESTAMP_ID
  )
  AS 
select a.REF, a.EMPLOYEE, a.FILETYPE, t.name, a.FROMDATE, a.VALIDITYDATE, a.DESCRIPT, a.TODATE
  from empladdfiles A
    left join empladdfiletypes t on a.filetype = t.ref
  ;
