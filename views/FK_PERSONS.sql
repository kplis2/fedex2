--@@@***SKEY***@@@--
CREATE OR ALTER VIEW FK_PERSONS (
      PERSON ,-- STRING120
      FNAME  ,-- PERSON_NAMES
      SNAME  ,-- PERSON_NAME
      SYMBOL ,-- STRING20
      COMPANY,-- COMPANIES_ID
      REF    -- PERSONS_ID
  )
  AS 
select person, fname, sname, symbol, company, ref from cpersons
  ;
