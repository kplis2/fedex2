--@@@***SKEY***@@@--
CREATE OR ALTER VIEW AKCJE_REKRUTACYJNE (
      REF        ,-- ERECRUITS_ID
      STARTDATE  ,-- TIMESTAMP_ID
      ENDDATE    ,-- TIMESTAMP_ID
      STATUS     ,-- SMALLINT_ID
      STATUS_OPIS,-- CHAR(40) CHARACTER SET UTF8                           
      DESCRIPT   ,-- STRING
      LEADER     ,-- OPERATOR_ID
      LEADER_OPIS-- STRING60
  )
  AS 
select r.REF, r.STARTDATE, r.ENDDATE, r.STATUS,
  case when r.status = 0 then 'planowane' else case when r.status = 1 then 'w trakcie' else 'zakończone' end end,
  r.DESCRIPT, r.LEADER, o.nazwa
  from erecruits r
    join operator o on r.leader = o.ref
  where (o.AKTYWNY=1 or o.aktywny is null)
  ;
