--@@@***SKEY***@@@--
CREATE OR ALTER VIEW STANYIL_DOSTEPNE_WMS (
      WERSJAREF,-- INTEGER
      KTM      ,-- VARCHAR(40) CHARACTER SET UTF8                           
      MAGAZYN  ,-- VARCHAR(3) CHARACTER SET UTF8                           
      ILDOST   -- BIGINT
  )
  AS 
select s.vers as wersjaref, s.good as ktm, s.wh as magazyn, sum(s.quantity - s.blocked + s.ordered) as ilosc
  from mwsconstlocs c
    join mwsstock s on (s.mwsconstloc = c.ref)
    left join defmagaz d on (d.symbol = c.wh)
  where c.act > 0 and c.locdest in (1,2,3,4,5,9) and d.mws = 1
  group by s.vers, s.wh, s.good
  ;
