--@@@***SKEY***@@@--
CREATE OR ALTER VIEW VIEW_PERSONS (
      REF       ,-- PERSONS_ID
      PESEL     ,-- PESEL
      NIP       ,-- NIP
      PERSON    ,-- STRING120
      FNAME     ,-- PERSON_NAMES
      MIDDLENAME,-- PERSON_NAME
      SNAME     ,-- PERSON_NAME
      MNAME     ,-- PERSON_NAME
      SEX       ,-- SMALLINT_ID
      BIRTHDATE ,-- TIMESTAMP_ID
      EVIDENCENO-- EVIDENCENO
  )
  AS 
select ref, pesel, nip, person, fname, middlename, sname, mname, sex, birthdate, p.evidenceno
  from persons p
order by ref
  ;
