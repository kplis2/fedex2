--@@@***SKEY***@@@--
CREATE OR ALTER VIEW DODATKI (
      EMPLOYEE    ,-- EMPLOYEES_ID
      ECOLUMN     ,-- ECOLUMNS_ID
      ECOLUMN_OPIS,-- STRING22
      FROMDATE    ,-- TIMESTAMP_ID
      TODATE      ,-- TIMESTAMP_ID
      EVALUE      -- MONEY
  )
  AS 
select
    en.employee,
    en.ecolumn,
    ec.name,
    en.fromdate,
    en.todate,
    en.evalue
  from econstelems en
    join ecolumns ec on ec.number = en.ecolumn
  where ec.emplcard = 1 and en.etype = 0
  ;
