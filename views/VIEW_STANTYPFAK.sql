--@@@***SKEY***@@@--
CREATE OR ALTER VIEW VIEW_STANTYPFAK (
      STANSPRZED,-- VARCHAR(10) CHARACTER SET UTF8                           
      TYPFAK    ,-- CHAR(12) CHARACTER SET UTF8                           
      ZAKUPU    -- SMALLINT
  )
  AS 
select STANTYPFAK.stansprzed, STANTYPFAK.typfak, STANTYPFAK.zakupu from STANTYPFAK
union select '' as stansprzed, TYPFAK.symbol as typfak, TYPFAK.zakup as zakupu from TYPFAK
  ;
