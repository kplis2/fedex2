--@@@***SKEY***@@@--
CREATE OR ALTER VIEW EPRULEWSCFG (
      COMPANY ,-- COMPANIES_ID
      EPAYRULE,-- EPAYRULES_ID
      WSCFGDEF,-- WSCFGDEF_ID
      FROMDATE,-- TIMESTAMP_ID
      PVALUE  -- NUMERIC_14_2
  )
  AS 
with actuval as (
   select company, epayrule, wscfgdef, max(fromdate) as fromdate
     from wscfgval
     group by company, epayrule, wscfgdef
)
  select v.company, v.epayrule, v.wscfgdef, v.fromdate, v.pvalue
    from wscfgval v
    join actuval a on (v.company = a.company and v.epayrule = a.epayrule and v.wscfgdef = a.wscfgdef and a.fromdate = v.fromdate)
  ;
