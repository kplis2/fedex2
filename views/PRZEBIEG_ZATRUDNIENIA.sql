--@@@***SKEY***@@@--
CREATE OR ALTER VIEW PRZEBIEG_ZATRUDNIENIA (
      REF             ,-- EMPLOYMENT_ID
      EMPLOYEE        ,-- EMPLOYEES_ID
      FROMDATE        ,-- TIMESTAMP_ID
      TODATE          ,-- TIMESTAMP_ID
      EMPLCONTRACT    ,-- EMPLCONTRACTS_ID
      WORKPOST        ,-- STRING80
      WORKDIM         ,-- FLOATINGPOINT
      DIMNUM          ,-- SMALLINT_ID
      DIMDEN          ,-- SMALLINT_ID
      BRANCH          ,-- STRING
      BRANCHSYMBOL    ,-- ODDZIAL_ID
      DEPARTMENT      ,-- STRING60
      DEPARTMENTSYMBOL,-- DEPARTMENT_ID
      SALARY          ,-- MONEY
      PAYMENTTYPE     ,-- SMALLINT_ID
      CADDSALARY      ,-- MONEY
      FUNCSALARY      ,-- MONEY
      ANNEXE          ,-- INTEGER_ID
      BKSYMBOL        ,-- ANALYTIC_ID
      LOCAL           ,-- STRING80
      CALENDAR        ,-- ECALENDARS_ID
      PROPORTIONAL    ,-- SMALLINT_ID
      WORKCAT         ,-- EWORKCATPOS_ID
      WORKTYPE        -- STRING
  )
  AS 
select employment.REF, employment.EMPLOYEE, employment.FROMDATE, employment.TODATE,
       employment.EMPLCONTRACT, edictworkposts.fullname,
       employment.WORKDIM, employment.DIMNUM, employment.DIMDEN, oddzialy.nazwa, oddzialy.oddzial, DEPARTMENTS.name,departments.symbol,
       employment.SALARY, employment.PAYMENTTYPE, employment.CADDSALARY,
       employment.FUNCSALARY, ANNEXE, employment.BKSYMBOL, employment.LOCAL, employment.CALENDAR, employment.PROPORTIONAL,
       employment.WORKCAT,
       edictworktypes.descript
from employment
left join edictworkposts on (employment.workpost = edictworkposts.ref)
left join oddzialy on (employment.branch = oddzialy.oddzial)
left join departments on (employment.department = departments.symbol)
left join edictworktypes on (edictworktypes.ref = edictworkposts.worktype)
left join emplcontracts on (employment.emplcontract = emplcontracts.ref)
left join econtrannexes on (employment.annexe = econtrannexes.ref)
  ;
