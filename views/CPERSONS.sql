--@@@***SKEY***@@@--
CREATE OR ALTER VIEW CPERSONS (
      REF            ,-- PERSONS_ID
      COMPANY        ,-- COMPANIES_ID
      PERSON         ,-- STRING120
      FNAME          ,-- PERSON_NAMES
      MIDDLENAME     ,-- PERSON_NAME
      SNAME          ,-- PERSON_NAME
      MNAME          ,-- PERSON_NAME
      FATHERNAME     ,-- PERSON_NAME
      MOTHERNAME     ,-- PERSON_NAME
      MOTHMNAME      ,-- PERSON_NAME
      SEX            ,-- SMALLINT_ID
      BIRTHDATE      ,-- TIMESTAMP_ID
      NATIONALITY    ,-- STRING20
      CITIZENSHIP    ,-- STRING20
      FOREIGNER      ,-- SMALLINT_ID
      STEADYSTAYCARD ,-- SMALLINT_ID
      EVIDENCENO     ,-- EVIDENCENO
      EVIDISSUEDBY   ,-- STRING60
      PASSPORTNO     ,-- PASSPORTNO
      PASSISSUEDBY   ,-- STRING60
      PESEL          ,-- PESEL
      NIP            ,-- NIP
      PROFESSION     ,-- STRING40
      EDUCATION      ,-- EDICTZUSCODES_ID
      EMAIL          ,-- EMAILS_ID
      CELLPHONE      ,-- PHONES_ID
      SYMBOL         ,-- STRING20
      ACTIV          ,-- SMALLINT_ID
      OEMAIL         ,-- EMAILS_ID
      OMOBILE        ,-- PHONES_ID
      OPHONE         ,-- PHONES_ID
      PHONE          ,-- PHONES_ID
      EVIDENCEDATE   ,-- TIMESTAMP_ID
      PASSPORTDATE   ,-- TIMESTAMP_ID
      MARSTATUS      ,-- EDICTMARSTATUS_ID
      CONVICTED      ,-- SMALLINT_ID
      BIRTHPLACE     ,-- STRING100
      EXPEVIDENCEDATE,-- TIMESTAMP_ID
      EXPPASSPORTDATE,-- TIMESTAMP_ID
      EHRM           ,-- SMALLINT_ID
      LOGIN          -- STRING20
  )
  AS 
select
    p.ref,
    s.company,
    p.person,
    p.fname,
    p.middlename,
    p.sname,
    p.mname,
    p.fathername,
    p.mothername,
    p.mothmname,
    p.sex,
    p.birthdate,
    p.nationality,
    p.citizenship,
    p.foreigner,
    p.steadystaycard,
    p.evidenceno,
    p.evidissuedby,
    p.passportno,
    p.passissuedby,
    p.pesel,
    p.nip,
    p.profession,
    p.education,
    p.email,
    p.cellphone,
    s.symbol,
    p.activ,
    p.oemail,
    p.omobile,
    p.ophone,
    p.phone,
    p.evidencedate,
    p.passportdate,
    p.marstatus,
    p.convicted,
    p.birthplace,
    p.expevidencedate,
    p.exppassportdate,
    p.ehrm,
    p.login
  from persons p
  join eperscompany s on (s.person = p.ref)
  ;
