--@@@***SKEY***@@@--
CREATE OR ALTER VIEW VIEW_MWSQ_WH_DETAILS (
      NUMBER  ,-- INTEGER
      GOOD    ,-- VARCHAR(40) CHARACTER SET UTF8                           
      VERS    ,-- INTEGER
      WH      ,-- VARCHAR(3) CHARACTER SET UTF8                           
      DESCRIPT,-- CHAR(112) CHARACTER SET UTF8                           
      PM      ,-- CHAR(4) CHARACTER SET UTF8                           
      QUANTITY-- NUMERIC(18,4)
  )
  AS 
select 0, s.good, s.vers, s.wh, 'Na lokacjach do sprzedaży' as descript, '+' as pm,
      coalesce(sum(s.quantity - s.blocked + s.ordered),0) as quantity
    from mwsstock s
      left join mwsconstlocs c on (s.mwsconstloc = c.ref)
    where c.act > 0
      and c.locdest in (1,2,3,4,5,9)
    group by good, vers, wh
  union
  select 1, a.good, a.vers, a.wh, 'Dostawienie na lok. do sprz.' as descript, '+' as pm,
      coalesce(sum(a.quantity),0) as quantity
    from mwsords o
      left join mwsacts a on (a.mwsord = o.ref)
    where o.mwsordtypedest = 9
      and o.status > 0
      and o.status < 3
      and a.status > 0
      and a.status < 6
    group by good, vers, wh
  union
  select 2, p.ktm as good, p.wersjaref as vers, n.magazyn as wh, 'Oczekuje na zlecenia' as descript, '-' as pm,
      -coalesce(sum(case when n.mwsdisposition = 1 then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0)),0)
    from dokumnag n
      left join dokumpoz p on (n.ref = p.dokument)
      left join defdokummag d on (d.typ = n.typ and d.magazyn = n.magazyn)
    where n.wydania = 1
      and n.mwsdoc = 1
      and n.mwsdone = 0
      and coalesce(n.blockmwsords,0) = 0
      and n.frommwsord is null
      and ((n.akcept = 1 and p.iloscl > p.ilosconmwsacts) or
           (n.mwsdisposition = 1 and p.ilosc > p.ilosconmwsacts))
      and p.genmwsordsafter = 1
      and coalesce(p.fake,0) = 0
      and coalesce(p.havefake,0) = 0
      and coalesce(d.afterackproc,'') <> ''
      and d.mwsordwaiting = 1
    group by good, vers, wh
  union
  select 3, s.ktm, s.wersjaref, s.magazyn, 'Zablokowane na zamówieniach', '-', -s.zablokow as quantity
    from stanyil s
  ;
