--@@@***SKEY***@@@--
CREATE OR ALTER VIEW PLANY_URLOPOWE (
      REF       ,-- EVACPLAN_ID
      EMPLOYEE  ,-- EMPLOYEES_ID
      PYEAR     ,-- SMALLINT_ID
      FROMDATE  ,-- TIMESTAMP_ID
      TODATE    ,-- TIMESTAMP_ID
      STATE     ,-- SMALLINT_ID
      STATE_OPIS,-- CHAR(60) CHARACTER SET UTF8                           
      COMPANY   -- COMPANIES_ID
  )
  AS 
select p.ref, p.employee, p.pyear, p.fromdate, p.todate, p.state,
  case when p.state = 0 then 'niezatwierdzony' else case when p.state = 1 then 'zatwierdzony' else 'zrealizowany' end end,
  p.company
  from evacplan p
  where p.vtype = 0
  ;
