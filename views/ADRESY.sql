--@@@***SKEY***@@@--
CREATE OR ALTER VIEW ADRESY (
      REF          ,-- EPERSADDR_ID
      PERSON       ,-- PERSONS_ID
      ADDRTYPE     ,-- SMALLINT_ID
      ADDRTYPE_OPIS,-- CHAR(68) CHARACTER SET UTF8                           
      CITY         ,-- STRING30
      POST         ,-- STRING30
      POSTCODE     ,-- POSTCODE
      STREET       ,-- STRING30
      HOUSENO      ,-- STRING20
      LOCALNO      ,-- STRING20
      PHONE        ,-- STRING20
      CPWOJ16M     ,-- STRING40
      POWIAT       ,-- STRING40
      STPREF       ,-- ALEJA_ID
      COMMUNITY    ,-- STRING
      FROMDATE     ,-- TIMESTAMP_ID
      TODATE       ,-- TIMESTAMP_ID
      STATUS       -- SMALLINT_ID
  )
  AS 
select EPERSADDR.REF, PERSON, addrtype,
case when addrtype = 0 then 'zameldowania' else case when addrtype = 1 then 'zamieszkania' else 'do korespondencji' end end,
CITY, POST, POSTCODE, STREET,
HOUSENO,LOCALNO, PHONE, CPWOJ16M.opis, POWIAT, STPREF, EDICTGUSCODES.descript,
FROMDATE, TODATE, STATUS
from EPERSADDR
left join CPWOJ16M on (epersaddr.cpwoj16m=cpwoj16m.ref)
left join EDICTGUSCODES on (epersaddr.communityid = EDICTGUSCODES.ref)
  ;
