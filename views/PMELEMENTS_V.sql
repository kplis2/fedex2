--@@@***SKEY***@@@--
CREATE OR ALTER VIEW PMELEMENTS_V (
      REF        ,-- STRING20
      DISPLAYNAME-- STRING20
  )
  AS 
select distinct e.symbol,e.symbol from pmelements e
join pmplans p on p.ref=e.pmplan
where e.plstartdate is not null and e.plenddate is not null
and e.symbol<>''
and p.ref=p.mainref
  ;
