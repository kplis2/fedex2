--@@@***SKEY***@@@--
CREATE OR ALTER VIEW INT_DOKUMNAG_POZYCJE_ZREAL (
      DN_REF   ,-- DOKUMNAG_ID
      DN_SYMBOL,-- SYMBOL_ID
      DN_INT_ID,-- STRING120
      DP_INT_ID,-- STRING120
      DP_KTM   ,-- KTM_ID
      DP_WERSJA,-- NRWERSJI_ID
      DP_ILOSCL,-- ILOSCI_MAG
      TJ_JEDN  ,-- JEDN_MIARY
      T_INT_ID -- STRING120
  )
  AS 
select dn.ref, dn.symbol, dn.int_id,
  dp.int_id, dp.ktm, dp.wersja, dp.iloscl, tj.jedn, t.int_id
  from dokumnag dn
    join dokumpoz dp on dn.ref = dp.dokument
    join towary t on t.ktm = dp.ktm
    join towjedn tj on dp.jedno = tj.ref
    --join listywysdpoz lwp on lwp.dokpoz = dp.ref and lwp.iloscspk = dp.iloscl --ML wykomentowane do testów
     -- and lwp.doktyp = 'M' and lwp.iloscspk = dp.iloscl
  ;
