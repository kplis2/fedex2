--@@@***SKEY***@@@--
CREATE OR ALTER VIEW PR_PRSCHEDGUIDEDET_FROM (
      PRDEPART    ,-- PRDEPARTS_ID
      REF         ,-- INTEGER
      PRSCHEDGUIDE,-- PRSCHEDGUIDES_ID
      KTM         ,-- KTM_ID
      WERSJAREF   ,-- WERSJE_ID
      NAZWAT      ,-- TOWARY_NAZWA
      SYMBOL      ,-- STRING40
      JEDN        ,-- TOWJEDN
      MIARA       ,-- JEDN_MIARY
      PRZELICZNIK ,-- NUMERIC(14,4)
      ILOSC       ,-- ILOSCI_MAG
      PONADLIMIT  -- SMALLINT_ID
  )
  AS 
select g.prdepart, d.ref, g.ref, d.ktm, d.wersjaref, t.nazwa, g.symbol, j.ref,
    j.jedn, j.przelicz, d.quantity, d.overlimit
  from prschedguidedets d
    left join prschedguides g on (g.ref = d.prschedguide)
    left join towary t on (t.ktm = g.ktm)
    left join prsheets s on (s.ref = g.prsheet)
    left join towjedn j on (j.ref = s.jedn)
  where d.prschedoper is not null and d.out = 0 and d.ssource = 1 and d.sref is null
    and (d.quantity > 0 or d.overlimit > 0)
  ;
