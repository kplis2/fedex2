--@@@***SKEY***@@@--
CREATE OR ALTER VIEW WFATTACHMENTINSTANCES (
      UUID          ,-- UUID
      WFINSTANCEUUID,-- UUID
      DESCRIPT      ,-- STRING
      ICON          ,-- STRING30
      CHANGEDFIELDS -- CHAR(56) CHARACTER SET UTF8                           
  )
  AS 
select WA.UUID, WA.WFINSTANCEUUID, WA.DESCRIPT, WA.ICON,
       (
       case
         when WI.PRIMARYATTACHMENT = WA.UUID then 'DESCRIPT=*=%B*'
         else ''
       end) as CHANGEDFIELDS
from WFATTACHMENT WA
inner join WFINSTANCE WI on WA.WFINSTANCEUUID = WI.UUID
where wa.removed=0
  ;
