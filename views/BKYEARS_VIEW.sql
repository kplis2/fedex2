--@@@***SKEY***@@@--
CREATE OR ALTER VIEW BKYEARS_VIEW (
      COMPANY     ,-- COMPANIES_ID
      YEARID      ,-- YEARS_ID
      NAME        ,-- STRING40
      PERIODAMOUNT,-- SMALLINT_ID
      SYNLEN      ,-- SMALLINT_ID
      FIRSTPERIOD ,-- SMALLINT_ID
      PATTERNREF  ,-- BKYEARS_ID
      REF         -- BKYEARS_ID
  )
  AS 
select
    company,
    yearid,
    name,
    periodamount,
    synlen,
    firstperiod,
    patternref,
    ref
from bkyears
  ;
