--@@@***SKEY***@@@--
CREATE OR ALTER VIEW DECREEMATCHINGS_VIEW (
      REF                ,-- DECREEMATCHINGS_ID
      COMPANY            ,-- COMPANIES_ID
      SLODEF             ,-- SLO_ID
      SLOPOZ             ,-- SLOPOZ_ID
      SLONAZWA           ,-- STRING
      SLOKOD             ,-- STRING40
      BKACCOUNT          ,-- BKACCOUNTS_ID
      ACCOUNT            ,-- BKSYMBOL_ID
      CREDIT             ,-- CT_AMOUNT
      DEBIT              ,-- DT_AMOUNT
      MATCHDATE          ,-- DATE_ID
      ACCOPER            ,-- OPERATOR_ID
      ACCOPERDICT        ,-- TIMESTAMP_ID
      ACCDATETIME        ,-- STRING60
      STATUS             ,-- INTEGER
      MATCHPERIOD        ,-- PERIOD_ID
      YEARID             ,-- YEARS_ID
      DECREE             ,-- DECREES_ID
      BKDOC              ,-- BKDOCS_ID
      PERIOD             ,-- PERIOD_ID
      BKDOCSYMB          ,-- DOCSYMBOL_ID
      MATCHINGSYMBOL     ,-- STRING40
      DECREEMATCHINGGROUP-- DECREEMATCHINGGROUPS_ID
  )
  AS 
select m.REF, m.company, m.SLODEF, m.SLOPOZ, m.SLONAZWA, m.slokod, m.BKACCOUNT, b.symbol, m.CREDIT, m.DEBIT,
    g.matchingdate, g.accoper, g.accdatetime, o.nazwa, coalesce(g.status,0), g.matchingperiod, b.yearid,
    m.decree, d.bkdoc, d.period, c.symbol, m.matchingsymbol, g.ref
  from DECREEMATCHINGS m
    left join bkaccounts b on (b.ref = m.bkaccount)
    left join decrees d on (d.ref = m.decree)
    left join bkdocs c on (c.ref = d.bkdoc)
    left join decreematchinggroups g on (g.ref = m.decreematchinggroup)
    left join operator o on (o.ref = g.accoper)
  ;
