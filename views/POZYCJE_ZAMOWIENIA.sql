--@@@***SKEY***@@@--
CREATE OR ALTER VIEW POZYCJE_ZAMOWIENIA (
      REF           ,-- POZZAM_ID
      ZAMOWIENIE    ,-- NAGZAM_ID
      NUMER         ,-- INTEGER
      KTM           ,-- KTM_ID
      WERSJA        ,-- NRWERSJI_ID
      ILOSC         ,-- ILOSCI_ZAM
      ILREAL        ,-- ILOSCI_ZAM
      ILZREAL       ,-- ILOSCI_ZAM
      CENA_MAG      ,-- CENY
      WARTOSC_MAG   ,-- CENY
      CENA_NETTO    ,-- CENYWAL
      WARTOSC_NETTO ,-- CENY
      CENA_BRUTTO   ,-- CENYWAL
      WARTOSC_BRUTTO,-- CENY
      PARAMS1       ,-- PARAMS
      PARAMS2       ,-- PARAMS
      PARAMS3       ,-- PARAMS
      PARAMS4       ,-- PARAMS
      PARAMN1       ,-- PARAMN
      PARAMN2       ,-- PARAMN
      PARAMN3       ,-- PARAMN
      PARAMN4       -- PARAMN
  )
  AS 
select
    REF,
    ZAMOWIENIE,
    NUMER,
    KTM,
    WERSJA,
    ILOSC,
    ILREAL,
    ILZREAL,
    CENAMAG,
    WARTMAG,
    CENANET,
    WARTNET,
    CENABRU,
    WARTBRU,
    PARAMS1,
    PARAMS2,
    PARAMS3,
    PARAMS4,
    PARAMN1,
    PARAMN2,
    PARAMN3,
    PARAMN4
 from POZZAM
  ;
