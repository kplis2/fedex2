--@@@***SKEY***@@@--
CREATE OR ALTER VIEW ERECRUITSAPP_VIEW (
      WORKPOST ,-- STRING
      COMPANY  ,-- COMPANIES_ID
      SYMBOL   ,-- STRING20
      WORKPLACE-- STRING60
  )
  AS 
select e.descript,e.company,e.symbol,e.workplace from erecruits e
where e.startdate<current_timestamp(0)
  and (e.enddate is null or e.enddate>current_timestamp(0))
  and www=1
  and status<2
  ;
