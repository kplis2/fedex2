--@@@***SKEY***@@@--
CREATE OR ALTER VIEW VIEW_MWS_KPL_MWSSTOCK_DETAILS (
      NAGKPL             ,-- KPLNAG_ID
      NAGVERS            ,-- INTEGER
      REF                ,-- MWSSTOCKS_ID
      WH                 ,-- WH_ID
      MWSCONSTLOC        ,-- MWSCONSTLOCS_ID
      MWSPALLOC          ,-- MWSPALLOCS_ID
      GOOD               ,-- KTM_ID
      VERS               ,-- WERSJE_ID
      WHSEC              ,-- WHSECS_ID
      WHAREA             ,-- WHAREAS_ID
      LOT                ,-- DOSTAWA_ID
      QUANTITY           ,-- QUANTITY_MWS
      MEASURE            ,-- JEDN_MIARY
      GOODTYPE           ,-- SMALLINT_ID
      GOODUNIT           ,-- TOWJEDN
      SELLAV             ,-- SMALLINT_ID
      RESERVED           ,-- QUANTITY_MWS
      BLOCKED            ,-- QUANTITY_MWS
      ORDSUSPENDED       ,-- QUANTITY_MWS
      SUSPENDED          ,-- QUANTITY_MWS
      ORDERED            ,-- QUANTITY_MWS
      STANCEN            ,-- INTEGER_ID
      MARKETABLE         ,-- SMALLINT_ID
      GOODGROUP          ,-- STRING60
      GOODNAME           ,-- TOWARY_NAZWA
      VERSNUMBER         ,-- SMALLINT_ID
      STOCKPLAN          ,-- SMALLINT_ID
      VOLUME             ,-- VOLUME_ID
      LOADPERCENT        ,-- NUMERIC_14_2
      DEEP               ,-- SMALLINT_ID
      MWSSTANDLEVELNUMBER,-- SMALLINT_ID
      MIXEDPALGROUP      ,-- SMALLINT_ID
      MIXTAKEPAL         ,-- SMALLINT_ID
      MIXLEAVEPAL        ,-- SMALLINT_ID
      MIXMWSCONSTLOCL    ,-- MWSCONSTLOCS_ID
      MIXMWSPALLOCL      ,-- MWSPALLOCS_ID
      MIXREFILL          ,-- SMALLINT_ID
      ISPAL              ,-- SMALLINT_ID
      ACTLOC             ,-- SMALLINT_ID
      AVPIECES           ,-- INTEGER_ID
      AVPACKAGES         ,-- INTEGER_ID
      AVBOXES            ,-- INTEGER_ID
      AVBIGBAGS          ,-- INTEGER_ID
      FINISHMWSCONSTLOC  ,-- SMALLINT_ID
      GOODSAV            ,-- SMALLINT_ID
      BRANCH             ,-- ODDZIAL_ID
      ACT                ,-- SMALLINT_ID
      LOCDEST            ,-- INTEGER_ID
      X_PARTIA           ,-- DATE_ID
      X_SLOWNIK          ,-- INTEGER_ID
      X_SERIAL_NO        ,-- INTEGER_ID
      X_BLOCKED          -- SMALLINT_ID
  )
  AS 
select n.ref as nagkpl, n.wersjaref as nagvers, s.ref, s.wh, s.mwsconstloc,
        s.mwspalloc, s.good, s.vers, s.whsec, s.wharea, s.lot, s.quantity,
        s.measure, s.goodtype, s.goodunit, s.sellav, s.reserved, s.blocked,
        s.ordsuspended, s.suspended, s.ordered, s.stancen, s.marketable,
        s.goodgroup, s.goodname, s.versnumber, s.stockplan, s.volume,
        s.loadpercent, s.deep, s.mwsstandlevelnumber, s.mixedpalgroup,
        s.mixtakepal, s.mixleavepal, s.mixmwsconstlocl, s.mixmwspallocl,
        s.mixrefill, s.ispal, s.actloc, s.avpieces, s.avpackages, s.avboxes,
        s.avbigbags, s.finishmwsconstloc, s.goodsav, s.branch, s.act, s.locdest,
        s.x_partia, s.x_slownik, s.x_serial_no, s.x_blocked
  from kplnag n
    join kplpoz p on (n.ref = p.nagkpl)
    join mwsstock s on (p.wersjaref = s.vers)
  where n.akt = 1

  ;
