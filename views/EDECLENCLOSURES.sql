--@@@***SKEY***@@@--
CREATE OR ALTER VIEW EDECLENCLOSURES (
      REF         ,-- EDECLARATIONS_ID
      EDECLDEF    ,-- EDECLDEFS_ID
      EDECLARATION,-- EDECLARATIONS_ID
      REGTIMESTAMP-- TIMESTAMP_ID
  )
  AS 
select ref, edecldef, enclosureowner, regtimestamp from edeclarations
where enclosureowner is not null
  ;
