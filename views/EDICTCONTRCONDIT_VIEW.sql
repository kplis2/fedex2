--@@@***SKEY***@@@--
CREATE OR ALTER VIEW EDICTCONTRCONDIT_VIEW (
      REF        ,-- EDICTCONTRCONDIT_ID
      SYMBOL     ,-- STRING40
      DESCRIPTION,-- STRING255
      UNIT       ,-- INTEGER_ID
      QUANTITY   ,-- NUMERIC_14_4
      VAT        ,-- VAT_ID
      ETYPE      ,-- INTEGER_ID
      COMPANY    ,-- COMPANIES_ID
      CONTRTYPE  -- CONTRTYPE_ID
  )
  AS 
select
    ed.ref,
    ed.symbol,
    ed.description,
    ed.unit,
    ed.quantity,
    ed.vat,
    ed.ETYPE,
    et.company,
    et.contrtype
from edictcontrcondit ed
  join ECONTRTYPECONDITS et on( ed.ref = et.contrcondit)
  ;
