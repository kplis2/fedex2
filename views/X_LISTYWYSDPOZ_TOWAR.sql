--@@@***SKEY***@@@--
CREATE OR ALTER VIEW X_LISTYWYSDPOZ_TOWAR (
      WERSJAREF,-- INTEGER
      KTM      ,-- KTM_ID
      MIARA    ,-- JEDN_MIARY
      NAZWAT   ,-- TOWARY_NAZWA
      MWSNAZWA ,-- STRING80
      X_KODPROD-- KTM_ID
  )
  AS 
select
    w.ref,
    w.ktm,
    w.miara,
    w.nazwat,
    t.mwsnazwa,
    w.x_kodprod
    from wersje w join towary t on w.ktm = t.ktm
  ;
