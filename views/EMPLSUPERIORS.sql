--@@@***SKEY***@@@--
CREATE OR ALTER VIEW EMPLSUPERIORS (
      EMPLOYEE          ,-- EMPLOYEES_ID
      EMPLOYEEDICT      ,-- STRING120
      SUPERIORFROMDEPART,-- INTEGER
      SUPERIOR          ,-- INTEGER
      SUPERIORDICT      ,-- VARCHAR(120) CHARACTER SET UTF8                           
      SUPERIORDEPUTY    ,-- INTEGER
      SUPERIORDEPUTYDICT,-- VARCHAR(120) CHARACTER SET UTF8                           
      COMPANY           -- COMPANIES_ID
  )
  AS 
select e.ref, e.personnames, coalesce(e.superiorfromdepart,0)
     , case when coalesce(e.superiorfromdepart,0) = 0 then s1.ref
       else (select first 1 t.superior from parsestring(e.departmentlist,';') p
               join departments t on (t.symbol = p.stringout and t.superior is not null)) end
     , case when coalesce(e.superiorfromdepart,0) = 0 then s1.personnames
       else (select first 1 personnames from employees m
               join departments d on (d.superior = m.ref)
               join parsestring(e.departmentlist,';') p on (d.symbol = p.stringout)
               order by p.lp) end
     , case when coalesce(e.superiorfromdepart,0) = 0 then s2.ref
       else (select first 1 t.superiordeputy from parsestring(e.departmentlist,';') p
               join departments t on (t.symbol = p.stringout and t.superiordeputy is not null)) end
     , case when coalesce(e.superiorfromdepart,0) = 0 then s2.personnames
       else (select first 1 personnames from employees m
               join departments d on (d.superiordeputy = m.ref)
               join parsestring(e.departmentlist,';') p on (d.symbol = p.stringout)
               order by p.lp) end
     , e.company
  from employees e
    left join employees s1 on (s1.ref = e.superior)
    left join employees s2 on (s2.ref = e.superiordeputy)
  ;
