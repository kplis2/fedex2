--@@@***SKEY***@@@--
CREATE OR ALTER VIEW WFOBJECTINSTANCE (
      DISPLAYNAME,-- STRING
      OBJECTUUID ,-- NEOS_ID
      UUID       ,-- NEOS_ID
      WFACTIVE   ,-- SMALLINT_ID
      OBJECTLABEL-- STRING255
  )
  AS 
select wi.displayname, wi.objectuuid, wi.uuid, wi.wfactive, wi.objectlabel from wfinstance wi
  ;
