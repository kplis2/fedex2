--@@@***SKEY***@@@--
CREATE OR ALTER VIEW POZFAK_NIEROZLICZ_VIEW (
      REF     ,-- INTEGER
      KOSZTFAK,-- NUMERIC(18,2)
      WARTMAG ,-- NUMERIC(18,2)
      ROZNICA ,-- BIGINT
      KOREKTA ,-- SMALLINT
      SYMBOL  ,-- VARCHAR(20) CHARACTER SET UTF8                           
      DATA    ,-- TIMESTAMP
      ILOSC   ,-- NUMERIC(18,4)
      ILOSCMAG,-- NUMERIC(18,4)
      REJESTR ,-- CHAR(12) CHARACTER SET UTF8                           
      ODDZIAL -- VARCHAR(10) CHARACTER SET UTF8                           
  )
  AS 
select pozfak.ref, max(pozfak.kosztkat) as KOSZTFAK, sum(dokumpoz.wartosc) as WARTMAG,
(max(pozfak.kosztkat) - (case when sum(dokumpoz.wartosc) is null then 0 else sum(dokumpoz.wartosc) end)) as ROZNICA,
max(typfak.korekta), max(nagfak.symbol) as SYMBOL, max(nagfak.data) as DATA, max(pozfak.ilosc) AS ILOSC,
(case when sum(dokumpoz.iloscl) is null then 0 else sum(dokumpoz.ilosc) end) as ILOSCMAG,
max(NAGFAK.rejestr), max(NAGFAK.ODDZIAL)
from nagfak
join pozfak on (nagfak.ref = pozfak.dokument)
left join dokumpoz on (dokumpoz.frompozfak = pozfak.ref)
left join towary on (pozfak.ktm = towary.ktm)
join typfak on (typfak.symbol = nagfak.typ)
where nagfak.magrozlicz = 0 and nagfak.zakup = 0 and nagfak.anulowanie = 0 and nagfak.nieobrot in (0,2)
and (nagfak.akceptacja = 1 or nagfak.akceptacja = 8) and towary.usluga <> 1
group by pozfak.ref
having (max(pozfak.ilosc - pozfak.pilosc) <> sum(dokumpoz.iloscl) or sum(dokumpoz.iloscl) is null)
  ;
