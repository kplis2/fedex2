--@@@***SKEY***@@@--
CREATE OR ALTER VIEW WORKFLOWHISTORY (
      OBJECTUUID     ,-- NEOS_ID
      TASKDEFUUID    ,-- NEOS_ID
      WFINSTANCEUUID ,-- NEOS_ID
      REF            ,-- WFCHANGELOG_ID
      USERUUID       ,-- NEOS_ID
      DESCRIPT       ,-- STRING255
      REGDATE        ,-- TIMESTAMP_ID
      TYPE           ,-- SMALLINT_ID
      TASKDISPLAYNAME-- STRING
  )
  AS 
select
wi.objectuuid, wt.taskdefuuid, wi.uuid as WFINSTANCEUUID, wc.ref, wc.useruuid, wc.descript, wc.regdate, wc."TYPE", wt.displayname
    from wfchangelog wc
    join wfinstance wi on wi.ref = wc.wfinstanceref
    left join wftask wt on wt.ref =  wc.wftaskref
  ;
