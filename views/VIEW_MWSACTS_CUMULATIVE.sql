--@@@***SKEY***@@@--
CREATE OR ALTER VIEW VIEW_MWSACTS_CUMULATIVE (
      REF                ,-- INTEGER
      NUMBER             ,-- INTEGER
      MWSORD             ,-- INTEGER
      FROMMWSACT         ,-- INTEGER
      STATUS             ,-- SMALLINT
      STOCKTAKING        ,-- SMALLINT
      GOOD               ,-- VARCHAR(40) CHARACTER SET UTF8                           
      VERS               ,-- INTEGER
      QUANTITY           ,-- NUMERIC(18,4)
      QUANTITYC          ,-- NUMERIC(18,4)
      QUANTITYL          ,-- NUMERIC(18,4)
      MWSCONSTLOCP       ,-- INTEGER
      MWSPALLOCP         ,-- INTEGER
      MWSCONSTLOCL       ,-- INTEGER
      MWSPALLOCL         ,-- INTEGER
      DOCID              ,-- INTEGER
      DOCPOSID           ,-- INTEGER
      DOCTYPE            ,-- CHAR(4) CHARACTER SET UTF8                           
      SETTLMODE          ,-- SMALLINT
      CATEGORY           ,-- CHAR(4) CHARACTER SET UTF8                           
      MINREALTIME        ,-- INTEGER
      PACKMETH           ,-- VARCHAR(40) CHARACTER SET UTF8                           
      CLOSEPALLOC        ,-- SMALLINT
      PACKSYMBOL         ,-- VARCHAR(20) CHARACTER SET UTF8                           
      MWSACCESSORY       ,-- INTEGER
      WH                 ,-- VARCHAR(3) CHARACTER SET UTF8                           
      WHAREA             ,-- INTEGER
      WHSEC              ,-- INTEGER
      REGTIME            ,-- TIMESTAMP
      ACCEPTTIME         ,-- TIMESTAMP
      COMMITTIME         ,-- TIMESTAMP
      REALTIME           ,-- DOUBLE PRECISION
      TIMESTARTDECL      ,-- TIMESTAMP
      TIMESTOPDECL       ,-- TIMESTAMP
      TIMESTART          ,-- TIMESTAMP
      TIMESTOP           ,-- TIMESTAMP
      KEYA               ,-- VARCHAR(40) CHARACTER SET UTF8                           
      KEYB               ,-- VARCHAR(40) CHARACTER SET UTF8                           
      KEYC               ,-- VARCHAR(40) CHARACTER SET UTF8                           
      KEYD               ,-- VARCHAR(40) CHARACTER SET UTF8                           
      FLAGS              ,-- VARCHAR(40) CHARACTER SET UTF8                           
      DESCRIPT           ,-- VARCHAR(255) CHARACTER SET UTF8                           
      ORD                ,-- SMALLINT
      PRIORITY           ,-- SMALLINT
      LOT                ,-- INTEGER
      RECDOC             ,-- SMALLINT
      CORTOMWSACT        ,-- INTEGER
      PLUS               ,-- SMALLINT
      STANCEN            ,-- INTEGER
      STATUSBLOCK        ,-- SMALLINT
      BGOOD              ,-- VARCHAR(40) CHARACTER SET UTF8                           
      BVERS              ,-- INTEGER
      BMWSCONSTLOCP      ,-- INTEGER
      BMWSPALLOCP        ,-- INTEGER
      BMWSCONSTLOCL      ,-- INTEGER
      BMWSPALLOCL        ,-- INTEGER
      WHAREAP            ,-- INTEGER
      WHAREAL            ,-- INTEGER
      WHAREALOGP         ,-- INTEGER
      WHAREALOGL         ,-- INTEGER
      MWSORDSTATUS       ,-- SMALLINT
      DISTTOGO           ,-- NUMERIC(18,4)
      REALTIMEDECL       ,-- DOUBLE PRECISION
      GROUPCOMMIT        ,-- INTEGER
      PALGROUP           ,-- INTEGER
      DATESTOPDECL       ,-- DATE
      STOCKREQUIRED      ,-- SMALLINT
      SLODEF             ,-- INTEGER
      SLOPOZ             ,-- INTEGER
      SLOKOD             ,-- VARCHAR(40) CHARACTER SET UTF8                           
      WEIGHT             ,-- NUMERIC(18,4)
      QUANTITYSTRING     ,-- VARCHAR(80) CHARACTER SET UTF8                           
      VOLUME             ,-- INTEGER
      MWSPALLOCSYM       ,-- VARCHAR(20) CHARACTER SET UTF8                           
      MIXEDPALLGROUP     ,-- SMALLINT
      LOADPERCENT        ,-- NUMERIC(18,2)
      MAXLEVEL           ,-- SMALLINT
      PLANMWSCONSTLOCL   ,-- INTEGER
      PLANWHAREALOGL     ,-- INTEGER
      PLANWHAREAL        ,-- INTEGER
      PACKQUANTITY       ,-- NUMERIC(18,4)
      PALPACKMETH        ,-- VARCHAR(30) CHARACTER SET UTF8                           
      PALPACKMETHSHORT   ,-- VARCHAR(20) CHARACTER SET UTF8                           
      PALPACKQUANTITY    ,-- NUMERIC(18,4)
      ISPAL              ,-- SMALLINT
      CLONEFROMMWSACT    ,-- INTEGER
      BLOCKCONSTLOCCHANGE,-- SMALLINT
      DEEPP              ,-- SMALLINT
      DEEPL              ,-- SMALLINT
      H                  ,-- NUMERIC(18,4)
      W                  ,-- NUMERIC(18,4)
      L                  ,-- NUMERIC(18,4)
      AUTOGEN            ,-- SMALLINT
      MIXTAKEPAL         ,-- SMALLINT
      MIXLEAVEPAL        ,-- SMALLINT
      MIXMWSCONSTLOCL    ,-- INTEGER
      MIXMWSPALLOCL      ,-- INTEGER
      MWSORDTYPE         ,-- INTEGER
      QUICKSTOCKTACKING  ,-- SMALLINT
      MULTIVERSGOOD      ,-- SMALLINT
      REFILLTRY          ,-- SMALLINT
      QUANTITYREM        ,-- NUMERIC(18,4)
      VOLUMEREM          ,-- INTEGER
      STOPNEXTMWSACTS    ,-- SMALLINT
      QUANTITYV          ,-- NUMERIC(18,4)
      OPERATOR           ,-- INTEGER
      MISTAKESCNT        ,-- INTEGER
      MISTAKESCNTQ       ,-- INTEGER
      MWSORDTYPEDEST     ,-- SMALLINT
      OPTDIST            ,-- NUMERIC(18,4)
      OPTNUMBER          ,-- INTEGER
      MANMWSACTS         ,-- SMALLINT
      DATESTOP           ,-- DATE
      FROMDOCPOS2PREPARE ,-- INTEGER
      MWSPALLOCSIZE      ,-- INTEGER
      SEGTYPE            ,-- SMALLINT
      WH2                ,-- VARCHAR(3) CHARACTER SET UTF8                           
      MWSCONSTLOCCHANGE  ,-- SMALLINT
      BADLABEL           ,-- SMALLINT
      BADLABELTIME       ,-- TIMESTAMP
      EMWSCONSTLOC       ,-- VARCHAR(20) CHARACTER SET UTF8                           
      CHECKED            ,-- SMALLINT
      MWSCONSTLOC        ,-- INTEGER
      QUANTITYP          ,-- NUMERIC(18,4)
      UNITP              ,-- INTEGER
      BADMWSCONSTLOC     ,-- SMALLINT
      BADBARCODE         ,-- SMALLINT
      PARAMS1            ,-- VARCHAR(40) CHARACTER SET UTF8                           
      PARAMS2            ,-- VARCHAR(40) CHARACTER SET UTF8                           
      PARAMS3            ,-- VARCHAR(40) CHARACTER SET UTF8                           
      PARAMS4            ,-- VARCHAR(40) CHARACTER SET UTF8                           
      PARAMS5            ,-- VARCHAR(40) CHARACTER SET UTF8                           
      PARAMS6            ,-- VARCHAR(40) CHARACTER SET UTF8                           
      PARAMS7            ,-- VARCHAR(40) CHARACTER SET UTF8                           
      PARAMS8            ,-- VARCHAR(40) CHARACTER SET UTF8                           
      PARAMS9            ,-- VARCHAR(40) CHARACTER SET UTF8                           
      PARAMS10           ,-- VARCHAR(40) CHARACTER SET UTF8                           
      PARAMS11           ,-- VARCHAR(40) CHARACTER SET UTF8                           
      PARAMS12           ,-- VARCHAR(40) CHARACTER SET UTF8                           
      PARAMN1            ,-- NUMERIC(18,2)
      PARAMN2            ,-- NUMERIC(18,2)
      PARAMN3            ,-- NUMERIC(18,2)
      PARAMN4            ,-- NUMERIC(18,2)
      PARAMN5            ,-- NUMERIC(18,2)
      PARAMN6            ,-- NUMERIC(18,2)
      PARAMN7            ,-- NUMERIC(18,2)
      PARAMN8            ,-- NUMERIC(18,2)
      PARAMN9            ,-- NUMERIC(18,2)
      PARAMN10           ,-- NUMERIC(18,2)
      PARAMN11           ,-- NUMERIC(18,2)
      PARAMN12           ,-- NUMERIC(18,2)
      PARAMD1            ,-- TIMESTAMP
      PARAMD2            ,-- TIMESTAMP
      PARAMD3            ,-- TIMESTAMP
      PARAMD4            ,-- TIMESTAMP
      LOCKEDIT           -- SMALLINT
  )
  AS 
select
    min(REF),
    min(NUMBER),
    MWSORD,
    min(FROMMWSACT),
    STATUS,
    min(STOCKTAKING),
    min(GOOD),
    VERS,
    sum(QUANTITY),
    sum(QUANTITYC),
    sum(QUANTITYL),
    MWSCONSTLOCP,
    min(MWSPALLOCP),
    MWSCONSTLOCL,
    min(MWSPALLOCL),
    min(DOCID),
    min(DOCPOSID),
    min(DOCTYPE),
    min(SETTLMODE),
    min(CATEGORY),
    min(MINREALTIME),
    min(PACKMETH),
    min(CLOSEPALLOC),
    min(PACKSYMBOL),
    min(MWSACCESSORY),
    min(WH),
    min(WHAREA),
    min(WHSEC),
    min(REGTIME),
    min(ACCEPTTIME),
    max(COMMITTIME),
    max(REALTIME),
    min(TIMESTARTDECL),
    max(TIMESTOPDECL),
    min(TIMESTART),
    max(TIMESTOP),
    min(KEYA),
    min(KEYB),
    min(KEYC),
    min(KEYD),
    min(FLAGS),
    max(DESCRIPT),
    min(ORD),
    min(PRIORITY),
    min(LOT),
    min(RECDOC),
    min(CORTOMWSACT),
    min(PLUS),
    min(STANCEN),
    min(STATUSBLOCK),
    min(BGOOD),
    min(BVERS),
    min(BMWSCONSTLOCP),
    min(BMWSPALLOCP),
    min(BMWSCONSTLOCL),
    min(BMWSPALLOCL),
    min(WHAREAP),
    min(WHAREAL),
    min(WHAREALOGP),
    min(WHAREALOGL),
    min(MWSORDSTATUS),
    min(DISTTOGO),
    min(REALTIMEDECL),
    min(GROUPCOMMIT),
    min(PALGROUP),
    max(DATESTOPDECL),
    min(STOCKREQUIRED),
    min(SLODEF),
    min(SLOPOZ),
    min(SLOKOD),
    min(WEIGHT),
    min(QUANTITYSTRING),
    min(VOLUME),
    min(MWSPALLOCSYM),
    min(MIXEDPALLGROUP),
    min(LOADPERCENT),
    min(MAXLEVEL),
    min(PLANMWSCONSTLOCL),
    min(PLANWHAREALOGL),
    min(PLANWHAREAL),
    min(PACKQUANTITY),
    min(PALPACKMETH),
    min(PALPACKMETHSHORT),
    min(PALPACKQUANTITY),
    min(ISPAL),
    min(CLONEFROMMWSACT),
    min(BLOCKCONSTLOCCHANGE),
    min(DEEPP),
    min(DEEPL),
    max(H),
    max(W),
    max(L),
    min(AUTOGEN),
    min(MIXTAKEPAL),
    min(MIXLEAVEPAL),
    min(MIXMWSCONSTLOCL),
    min(MIXMWSPALLOCL),
    min(MWSORDTYPE),
    min(QUICKSTOCKTACKING),
    min(MULTIVERSGOOD),
    min(REFILLTRY),
    min(QUANTITYREM),
    min(VOLUMEREM),
    min(STOPNEXTMWSACTS),
    min(QUANTITYV),
    min(OPERATOR),
    min(MISTAKESCNT),
    min(MISTAKESCNTQ),
    min(MWSORDTYPEDEST),
    min(OPTDIST),
    min(OPTNUMBER),
    min(MANMWSACTS),
    max(DATESTOP),
    min(FROMDOCPOS2PREPARE),
    min(MWSPALLOCSIZE),
    min(SEGTYPE),
    min(WH2),
    min(MWSCONSTLOCCHANGE),
    min(BADLABEL),
    min(BADLABELTIME),
    min(EMWSCONSTLOC),
    min(CHECKED),
    min(MWSCONSTLOC),
    min(QUANTITYP),
    min(UNITP),
    min(BADMWSCONSTLOC),
    min(BADBARCODE),
    min(PARAMS1),
    min(PARAMS2),
    min(PARAMS3),
    min(PARAMS4),
    min(PARAMS5),
    min(PARAMS6),
    min(PARAMS7),
    min(PARAMS8),
    min(PARAMS9),
    min(PARAMS10),
    min(PARAMS11),
    min(PARAMS12),
    min(PARAMN1),
    min(PARAMN2),
    min(PARAMN3),
    min(PARAMN4),
    min(PARAMN5),
    min(PARAMN6),
    min(PARAMN7),
    min(PARAMN8),
    min(PARAMN9),
    min(PARAMN10),
    min(PARAMN11),
    min(PARAMN12),
    min(PARAMD1),
    min(PARAMD2),
    min(PARAMD3),
    min(PARAMD4),
    min(LOCKEDIT)
  from mwsacts
  group by MWSORD, STATUS, VERS, MWSCONSTLOCP, MWSCONSTLOCL
  ;
