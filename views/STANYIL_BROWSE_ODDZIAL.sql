--@@@***SKEY***@@@--
CREATE OR ALTER VIEW STANYIL_BROWSE_ODDZIAL (
      MAGAZYN      ,-- CHAR(12) CHARACTER SET UTF8                           
      WERSJAREF    ,-- INTEGER
      KTM          ,-- VARCHAR(40) CHARACTER SET UTF8                           
      WERSJA       ,-- INTEGER
      NAZWAT       ,-- VARCHAR(80) CHARACTER SET UTF8                           
      CHODLIWY     ,-- SMALLINT
      GRUPA        ,-- VARCHAR(60) CHARACTER SET UTF8                           
      USLUGA       ,-- SMALLINT
      MIARA        ,-- VARCHAR(5) CHARACTER SET UTF8                           
      ODDZIAL      ,-- VARCHAR(10) CHARACTER SET UTF8                           
      WERSJAREFDICT,-- VARCHAR(50) CHARACTER SET UTF8                           
      ILDOSPRZ     ,-- BIGINT
      ZAMOWIONO    ,-- NUMERIC(18,4)
      ZAREZERW     ,-- NUMERIC(18,4)
      ZABLOKOW     ,-- NUMERIC(18,4)
      KOLOR        ,-- SMALLINT
      AKT          ,-- SMALLINT
      ILOSC        ,-- NUMERIC(18,4)
      ILOSCPOD     ,-- NUMERIC(18,4)
      WARTOSC      ,-- NUMERIC(18,2)
      CENA         ,-- BIGINT
      STANMIN      ,-- NUMERIC(18,4)
      STANMAX      ,-- NUMERIC(18,4)
      MINMAXSTAN   ,-- BIGINT
      SYMBOL_DOST1 ,-- VARCHAR(20) CHARACTER SET UTF8                           
      PRAWAGRUP    ,-- VARCHAR(80) CHARACTER SET UTF8                           
      ILOSCJM1     ,-- NUMERIC(18,4)
      ILOSCJM2     ,-- NUMERIC(18,4)
      MIARA1       ,-- VARCHAR(5) CHARACTER SET UTF8                           
      MIARA2       -- VARCHAR(5) CHARACTER SET UTF8                           
  )
  AS 
select min(S.MAGAZYN) as MAGAZYN, S.WERSJAREF,S.KTM,S.WERSJA,
  S.NAZWAT,S.CHODLIWY,S.GRUPA,S.USLUGA,S.MIARA, S.ODDZIAL,W.NAZWA as WERSJAREFDICT,
  sum(case when coalesce(M.MWS,0) = 0 then S.ILOSC - S.ZABLOKOW else coalesce(Q.QUANTITY,0) - S.ZABLOKOW end) as ILDOSPRZ,
  sum(S.zamowiono) as ZAMOWIONO, sum(S.ZAREZERW) as ZAREZERW, sum(S.zablokow) as ZABLOKOW, max(S.KOLOR) as KOLOR, max(S.AKT) as AKT,
  sum(S.ILOSC) as ILOSC, sum(S.ILOSCPOD) as ILOSCPOD, sum(S.WARTOSC) as WARTOSC,case when sum(S.ilosc)>0 then (sum(Wartosc) / sum(S.ilosc)) else 0 end as CENA,
  sum(S.STANMIN) as STANMIN, sum(S.STANMAX) as STANMAX,  sum(1-1) as MINMAXSTAN,
  min(W.SYMBOL_DOST) as SYMBOL_DOST1, max(S.PRAWAGRUP) as PRAWAGRUP,
  sum(S.ILOSCJM1) as ILOSCJM1,sum(S.ILOSCJM2) as ILOSCJM2,S.MIARA1,S.MIARA2
    from STANYIL S left join WERSJE W on (W.REF = S.WERSJAREF)
      left join DEFMAGAZ M on (M.SYMBOL = S.MAGAZYN)
      left join MWSVERSQ Q on (Q.WH = S.MAGAZYN and Q.VERS = S.WERSJAREF)
    group by S.WERSJAREF, S.KTM,S.WERSJA, S.NAZWAT, S.CHODLIWY,S.GRUPA,
      S.USLUGA, S.MIARA,S.MIARA1, S.MIARA2, S.ODDZIAL,W.NAZWA
  ;
