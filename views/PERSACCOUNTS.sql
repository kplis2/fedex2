--@@@***SKEY***@@@--
CREATE OR ALTER VIEW PERSACCOUNTS (
      REF            ,-- BANKACCOUNTS_ID
      DICTDEF        ,-- SLO_ID
      DICTPOS        ,-- SLOPOZ_ID
      BANK           ,-- BANK_ID
      ACCOUNT        ,-- STRING80
      ID             ,-- STRING80
      GL             ,-- SMALLINT_ID
      OLDSLOBANACCREF,-- INTEGER_ID
      EACCOUNT       ,-- BANKACCOUNT_ID
      COMPANY        ,-- COMPANIES_ID
      HBSTATUS       ,-- SMALLINT_ID
      ACCOUNTNO      ,-- NUMERIC(12,0)
      BANKACC        ,-- BANKACC_ID
      OFFDATE        -- TIMESTAMP_ID
  )
  AS 
select REF,
    DICTDEF,
    DICTPOS,
    BANK,
    ACCOUNT,
    ID,
    GL,
    OLDSLOBANACCREF,
    EACCOUNT,
    COMPANY,
    HBSTATUS,
    ACCOUNTNO,
    BANKACC,
    OFFDATE from bankaccounts
where (dictdef = 9)
  ;
