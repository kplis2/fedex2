--@@@***SKEY***@@@--
CREATE OR ALTER VIEW QUICKEDITEPRPOS (
      EMPLOYEE      ,-- EMPLOYEES_ID
      EPAYROLL      ,-- EPAYROLLS_ID
      ECOLUMN       ,-- INTEGER
      PVALUE        ,-- CENY
      COMPANY       ,-- COMPANIES_ID
      DEPARTMENT    ,-- DEPARTMENT_ID
      DEPARTMENTLIST,-- STRING
      STATUS        -- INTEGER
  )
  AS 
select m.employee, m.epayroll,
       cast(case when p.ecolumn > 0 then coalesce(p.ecolumn,0) else g.pvalue end as integer),
       p.pvalue ,
       e.company, m.department, e.departmentlist,
       iif(r.status + coalesce(m.status,0) > 0, 1, 0) --PR61951 
  from eprempl m
    join employees e on (m.employee = e.ref)
    join epayrolls r on (m.epayroll = r.ref)
    join globalparams g on (g.connectionid = current_connection and g.psymbol = 'QEDITEPRPOS_ECOLUMN')
    left join eprpos p on (p.employee = m.employee and p.payroll = m.epayroll and p.ecolumn = g.pvalue)
    left join ecolumns c on (c.number = p.ecolumn)
  where p.ecolumn is null or c.coltype = 'SRE'
  order by e.personnames
  ;
