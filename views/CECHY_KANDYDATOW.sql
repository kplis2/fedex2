--@@@***SKEY***@@@--
CREATE OR ALTER VIEW CECHY_KANDYDATOW (
      REF           ,-- ECANDIDATTR_ID
      CANDIDATE     ,-- ECANDIDATES_ID
      RATING        ,-- FLOATINGPOINT
      STRRATING     ,-- STRING20
      ATTRIBUTE     ,-- EDICTRECATTR_ID
      ATTRIBUTE_OPIS-- STRING80
  )
  AS 
select a.REF, a.CANDIDATE, a.RATING, a.STRRATING, a.ATTRIBUTE, d.attrname
  from ecandidattr a
    join EDICTRECATTR d on d.ref = a.attribute
  ;
