--@@@***SKEY***@@@--
CREATE OR ALTER VIEW WFTASKINSTANCES (
      OBJECTUUID            ,-- NEOS_ID
      UUID                  ,-- NEOS_ID
      TASKDEFUUID           ,-- NEOS_ID
      LABEL                 ,-- STRING
      DESCRIPT              ,-- VARCHAR(8000) CHARACTER SET UTF8                           
      ICON                  ,-- STRING
      STATUS                ,-- SMALLINT_ID
      FORROLE               ,-- VARCHAR(256) CHARACTER SET UTF8                           
      WFINSTANCEUUID        ,-- NEOS_ID
      TAKENBYUSER           ,-- VARCHAR(256) CHARACTER SET UTF8                           
      DISPLAYNAME           ,-- STRING
      VISIBLEAT             ,-- TIMESTAMP_ID
      TASKCREATIONDATE      ,-- VARCHAR(255) CHARACTER SET UTF8                           
      LASTTASKACTIVATIONDATE-- VARCHAR(255) CHARACTER SET UTF8                           
  )
  AS 
select WT.OBJECTUUID, WT.UUID, WT.TASKDEFUUID, WT.LABEL, coalesce(nullif(WT.DESCRIPT,''),WT.DISPLAYNAME), WT.ICON, WT.STATUS,
       coalesce(WT.FORROLENAME, WT.FORROLE), WT.WFINSTANCEUUID, coalesce(O.NAZWA, WT.TAKENBYUSER), WT.DISPLAYNAME,
       WT.VISIBLEAT, coalesce(WT.CREATEDTIME,
       (select first 1 WC.REGDATE
        from WFCHANGELOG WC
        where WC.WFTASKREF = WT.REF and
              WC.PROPERTY = 'STATUS' and
              WC.OLDVALUE is null and
              WC.DESCRIPT containing 'utworzone'
        order by coalesce(null, WC.REGDATE))), coalesce(WT.ACTIVATETIME,
       (select first 1 WC.REGDATE
        from WFCHANGELOG WC
        where WC.WFTASKREF = WT.REF and
              WC.PROPERTY = 'STATUS' and
              WC.DESCRIPT containing 'aktywne'
        order by coalesce(null, WC.REGDATE) desc))
from WFTASK WT
left join OPERATOR O on O.UUID = WT.TAKENBYUSER
  ;
