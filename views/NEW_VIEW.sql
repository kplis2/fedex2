--@@@***SKEY***@@@--
CREATE OR ALTER VIEW NEW_VIEW (
      DN_REF   ,-- DOKUMNAG_ID
      DN_SYMBOL,-- SYMBOL_ID
      DN_INT_ID,-- STRING120
      DP_INT_ID,-- STRING120
      DP_KTM   ,-- KTM_ID
      DP_WERSJA,-- NRWERSJI_ID
      DP_ILOSCL,-- ILOSCI_MAG
      TJ_JEDN  -- JEDN_MIARY
  )
  AS 
select dn.ref, dn.symbol, dn.int_id,
  dp.int_id, dp.ktm, dp.wersja, dp.iloscl, tj.jedn
  from dokumnag dn
    join dokumpoz dp on dn.ref = dp.dokument
    join towjedn tj on dp.jedno = tj.ref
    --join listywysdpoz lwp on lwp.dokpoz = dp.ref and lwp.iloscspk = dp.iloscl --ML wykomentowane tymczasowo
     -- and lwp.doktyp = 'M' and lwp.iloscspk = dp.iloscl
  ;
