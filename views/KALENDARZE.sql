--@@@***SKEY***@@@--
CREATE OR ALTER VIEW KALENDARZE (
      REF          ,-- INTEGER_ID
      EMPLOYEE     ,-- INTEGER_ID
      CDATE        ,-- TIMESTAMP_ID
      PYEAR        ,-- INTEGER_ID
      PMONTH       ,-- SMALLINT_ID
      PDAY         ,-- SMALLINT_ID
      DAYKIND      ,-- SMALLINT_ID
      DAYKIND_OPIS ,-- STRING40
      WORKSTART    ,-- TIMESTR
      WORKEND      ,-- TIMESTR
      WORKTIME     ,-- TIMESTR
      ABSENCE      ,-- INTEGER
      VACPLAN      ,-- INTEGER
      DESCRIPT     ,-- STRING30
      CALENDAR     ,-- STRING20
      PATTKIND     ,-- SMALLINT_ID
      PATTKIND_OPIS,-- CHAR(56) CHARACTER SET UTF8                           
      PERIOD       -- SMALLINT_ID
  )
  AS 
select d.ref, d.employee, d.cdate, d.pyear, d.pmonth, d.pday, edk.daykind, edk.name,
  d.workstartstr, d.workendstr, d.worktimestr,
  case when d.absence is not null then 1 else 0 end,
  case when d.vacplan is not null then 1 else 0 end,
  d.descript, cal.name, cal.pattkind,
  case when cal.pattkind = 0 then 'tygodniowy' else case when cal.pattkind = 1 then 'wielobrygadowy' else 'dwutygodniowy' end end,
  cal.periodb
  from  emplcaldays d
    join ecalendars cal on cal.ref = d.ecalendar
    join edaykinds edk on edk.ref = d.daykind
  ;
