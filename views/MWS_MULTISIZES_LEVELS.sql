--@@@***SKEY***@@@--
CREATE OR ALTER VIEW MWS_MULTISIZES_LEVELS (
      WH      ,-- VARCHAR(3) CHARACTER SET UTF8                           
      LEVELREF,-- INTEGER
      LOCCNT  ,-- INTEGER
      FREECNT ,-- INTEGER
      H       -- NUMERIC(18,4)
  )
  AS 
select l.wh, l.ref, count(c.ref), count(f.ref), max(c.h)
  from mwsstandlevels l
    left join mwsconstlocs c on (c.mwsstandlevel = l.ref)
    left join mwsfreemwsconstlocs f on (f.ref = c.ref)
    left join mwsstands s on (s.ref = l.mwsstand)
    left join mwsstandtypes t on (t.ref = s.mwsstandtype)
  where t.segtype = 2
  group by l.wh, l.ref
  ;
