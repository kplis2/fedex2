--@@@***SKEY***@@@--
CREATE OR ALTER VIEW MWSSTOCK_CONSTLOC_GROUP (
      WH                 ,-- VARCHAR(3) CHARACTER SET UTF8                           
      GOOD               ,-- VARCHAR(40) CHARACTER SET UTF8                           
      VERS               ,-- INTEGER
      MWSCONSTLOC        ,-- INTEGER
      QUANTITY           ,-- NUMERIC(18,4)
      BLOCKED            ,-- NUMERIC(18,4)
      RESERVED           ,-- NUMERIC(18,4)
      DEEP               ,-- SMALLINT
      MIXEDPALGROUP      ,-- SMALLINT
      ISPAL              ,-- SMALLINT
      GOODSAV            ,-- SMALLINT
      MWSSTANDLEVELNUMBER,-- SMALLINT
      SELLAV             -- SMALLINT
  )
  AS 
select m.wh, m.good, m.vers, m.mwsconstloc, sum(m.quantity), sum(m.blocked), sum(m.reserved), m.deep, m.mixedpalgroup, m.ispal, m.goodsav, m.mwsstandlevelnumber, m.sellav
    from mwsstock m
      group by m.wh, m.good, m.vers, m.mwsconstloc, m.deep, m.mixedpalgroup, m.ispal, m.goodsav, m.mwsstandlevelnumber, m.sellav
  ;
