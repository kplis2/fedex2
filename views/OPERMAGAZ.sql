--@@@***SKEY***@@@--
CREATE OR ALTER VIEW OPERMAGAZ (
      SYMBOL ,-- DEFMAGAZ_ID
      OPER   ,-- OPERATOR_ID
      COMPANY-- COMPANIES_ID
  )
  AS 
select d.symbol,om.operator,o.company from defmagaz d
join oddzialy o on o.oddzial = d.oddzial
join OPERMAG om on om.magazyn = d.symbol
  ;
