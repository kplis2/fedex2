--@@@***SKEY***@@@--
CREATE OR ALTER VIEW UMOWY (
      REF           ,-- EMPLCONTRACTS_ID
      CONTRNO       ,-- STRING20
      EMPLOYEE      ,-- EMPLOYEES_ID
      ECONTRTYPE    ,-- ECONTRTYPES_ID
      ECONTRTYPEDICT,-- STRING30
      CONTRDATE     ,-- TIMESTAMP_ID
      FROMDATE      ,-- TIMESTAMP_ID
      TODATE        ,-- TIMESTAMP_ID
      ENDDATE       ,-- TIMESTAMP_ID
      BRANCH        ,-- STRING
      DEPARTMENT    ,-- STRING60
      WORKPOST      ,-- STRING80
      WORKTYPE      ,-- EDICTWORKTYPES_ID
      DIMNUM        ,-- SMALLINT_ID
      DIMDEN        ,-- SMALLINT_ID
      WORKDIM       ,-- FLOATINGPOINT
      WORKCAT       ,-- EWORKCATPOS_ID
      MONTHSTOSB    ,-- SMALLINT_ID
      SALARY        ,-- MONEY
      ADDCONDIT     ,-- STRING1024
      PAYMENTTYPE   ,-- SMALLINT_ID
      COPYRIGHTPER  ,-- PERCENT
      BANKACCOUNT   ,-- STRING80
      LOCAL         ,-- STRING80
      CALENDAR      ,-- ECALENDARS_ID
      CADDSALARY    ,-- MONEY
      FUNCSALARY    ,-- MONEY
      IFLAGS        ,-- STRING30
      BKSYMBOL      ,-- ANALYTIC_ID
      PROPORTIONAL  ,-- SMALLINT_ID
      PAIDTO        ,-- STRING60
      EMPLTYPE      ,-- SMALLINT_ID
      EMPLTYPEDICT  ,-- STRING30
      ETERMINATION  ,-- INTEGER_ID
      CBRANCH       ,-- ODDZIAL_ID
      CDEPARTMENT   ,-- DEPARTMENT_ID
      CWORKPOST     ,-- EDICTWORKPOSTS_ID
      CWORKDIM      ,-- FLOATINGPOINT
      CSALARY       ,-- MONEY
      CCADDSALARY   ,-- MONEY
      CFUNCSALARY   ,-- MONEY
      COMPANY       ,-- COMPANIES_ID
      SYMBOL        -- VARCHAR(44) CHARACTER SET UTF8                           
  )
  AS 
select c.ref, c.contrno, c.employee, c.econtrtype, t.tname, c.contrdate,
    c.fromdate, c.todate, c.enddate, o.nazwa, d.name, w.fullname, c.worktype,
    c.dimnum, c.dimden, c.workdim, c.workcat, c.monthstosb, c.salary, c.addcondit,
    c.paymenttype, c.copyrightper, b.account, c.local, c.calendar, c.caddsalary,
    c.funcsalary, c.iflags, bksymbol, c.proportional, c.paidto, c.empltype, m.name,
    c.etermination, c.cbranch, c.cdepartment, c.cworkpost, c.cworkdim, c.csalary,
    c.ccaddsalary, c.cfuncsalary, c.company,
    trim(leading 'na ' from lower(t.tname)) || ' od ' || cast(c.fromdate as date) --PR61963
  from emplcontracts c
    join econtrtypes t on (c.econtrtype = t.contrtype)
    left join empltypes m on (c.empltype = m.ref)
    left join oddzialy o on (c.branch = o.symbol)
    left join departments d on (c.department = d.symbol)
    left join edictworkposts w on (c.workpost = w.ref)
    left join bankaccounts b on (c.bankaccount = b.ref)
  ;
