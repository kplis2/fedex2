--@@@***SKEY***@@@--
CREATE OR ALTER VIEW BKPERIODS_VIEW (
      REF       ,-- VARCHAR(17) CHARACTER SET UTF8                           
      COMPANY   ,-- COMPANIES_ID
      ID        ,-- PERIOD_ID
      YEARID    ,-- YEARS_ID
      ORD       ,-- SMALLINT_ID
      NAME      ,-- STRING40
      SDATE     ,-- TIMESTAMP_ID
      FDATE     ,-- TIMESTAMP_ID
      STATUS    ,-- SMALLINT_ID
      DNAME     ,-- STRING60
      PTYPE     ,-- SMALLINT_ID
      SIDBLOCKED,-- SMALLINT_ID
      VATBLOCKED-- SMALLINT_ID
  )
  AS 
select
    id||company,
    COMPANY,
    ID,
    yearid,
    ord,
    name,
    sdate,
    fdate,
    status,
    dname,
    ptype,
    sidblocked,
    vatblocked
from bkperiods
  ;
