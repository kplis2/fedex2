--@@@***SKEY***@@@--
CREATE OR ALTER VIEW VIEW_MWSSTOCK (
      GOOD           ,-- VARCHAR(40) CHARACTER SET UTF8                           
      VERS           ,-- INTEGER
      MWSCONSTLOC    ,-- INTEGER
      MWSCONSTLOCSYMB,-- VARCHAR(20) CHARACTER SET UTF8                           
      LOT            ,-- INTEGER
      QUANTITY       ,-- NUMERIC(18,4)
      BLOCKED        ,-- NUMERIC(18,4)
      ORDERED        ,-- NUMERIC(18,4)
      QUANTITYA      -- BIGINT
  )
  AS 
select s.good, s.vers, s.mwsconstloc, l.symbol,
    case when d.magbreak = 2 or (d.magbreak = 1 and t.magbreak = 2) then coalesce(s.lot,0) else 0 end,
    sum(s.quantity), sum(s.blocked), sum(s.ordered), sum(s.quantity - s.blocked) as quantitya
  from mwsstock s
    left join towary t on (s.good = t.ktm)
    left join defmagaz d on (s.wh = d.symbol)
    left join mwsconstlocs l on (s.mwsconstloc = l.ref)
  group by s.good, s.vers, s.mwsconstloc, l.symbol,
    case when d.magbreak = 2 or (d.magbreak = 1 and t.magbreak = 2) then coalesce(s.lot,0) else 0 end
  order by  s.good, s.vers, s.mwsconstloc, l.symbol,
    case when d.magbreak = 2 or (d.magbreak = 1 and t.magbreak = 2) then coalesce(s.lot,0) else 0 end
  ;
