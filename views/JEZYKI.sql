--@@@***SKEY***@@@--
CREATE OR ALTER VIEW JEZYKI (
      PERSON     ,-- PERSONS_ID
      LANGUAGE   ,-- STRING40
      SLEVEL     ,-- STRING20
      MARK       ,-- SMALLINT_ID
      CERTIFICATE-- STRING60
  )
  AS 
select PERSON, edictlangs.language, SLEVEL, MARK, CERTIFICATE
from elangskills left join edictlangs on (elangskills.language=edictlangs.ref)
  ;
