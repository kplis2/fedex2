--@@@***SKEY***@@@--
CREATE OR ALTER VIEW PR_CANCELED_PRORDS_POS (
      PRNAGZAMREF,-- INTEGER
      PRNAGZAMID ,-- VARCHAR(30) CHARACTER SET UTF8                           
      PRPOZZAMREF,-- INTEGER
      KTM        ,-- VARCHAR(40) CHARACTER SET UTF8                           
      WERSJAREF  ,-- INTEGER
      KTMNAME    ,-- VARCHAR(80) CHARACTER SET UTF8                           
      ILOSC      -- NUMERIC(18,4)
  )
  AS 
select n.ref, n.id, p.ref, p.ktm, p.wersjaref, g.ktmname, p.ilosc
  from prschedguides g
    left join pozzam p on (p.ref = g.pozzam)
    left join nagzam n on (n.ref = p.zamowienie)
  where g.anulowano = 1 and g.status < 2 and p.anulowano = 1
    and exists(select first 1 1 from prschedguidedets d where d.prschedguide = g.ref and d.quantity > 0)
  group by n.id, n.ref, p.ref, p.ktm, g.ktmname, p.wersjaref, p.ilosc
  ;
