--@@@***SKEY***@@@--
CREATE OR ALTER VIEW POZZAM_VIEW (
      REF       ,-- POZZAM_ID
      KTM       ,-- KTM_ID
      NAZWA     ,-- TOWARY_NAZWA
      ZAMOWIENIE-- NAGZAM_ID
  )
  AS 
select p.ref, p.ktm, t.nazwa, p.zamowienie
  from pozzam p join towary t on p.ktm = t.ktm
  where coalesce(p.fake,0) = 0
  ;
