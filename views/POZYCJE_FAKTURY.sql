--@@@***SKEY***@@@--
CREATE OR ALTER VIEW POZYCJE_FAKTURY (
      REF           ,-- POZFAK_ID
      DOKUMENT      ,-- NAGFAK_ID
      NUMER         ,-- INTEGER_ID
      KTM           ,-- KTM_ID
      WERSJA        ,-- NRWERSJI_ID
      ILOSC         ,-- BIGINT
      CENA_NETTO    ,-- CENYWAL
      WARTOSC_NETTO ,-- BIGINT
      CENA_BRUTTO   ,-- CENYWAL
      WARTOSC_BRUTTO-- BIGINT
  )
  AS 
select
REF, DOKUMENT, NUMER,
KTM, WERSJA, ILOSC-PILOSC,
CENANET, WARTNET-PWARTNET, CENABRU, WARTBRU-PWARTBRU
 from POZFAK
  ;
