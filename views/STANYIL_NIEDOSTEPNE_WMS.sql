--@@@***SKEY***@@@--
CREATE OR ALTER VIEW STANYIL_NIEDOSTEPNE_WMS (
      WERSJAREF,-- INTEGER
      KTM      ,-- VARCHAR(40) CHARACTER SET UTF8                           
      MAGAZYN  ,-- CHAR(12) CHARACTER SET UTF8                           
      ILNIEDOST-- BIGINT
  )
  AS 
select p.wersjaref, p.ktm, n.magazyn, sum(case when n.mwsdisposition = 1 then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0))
  from dokumpoz p
    left join towary t on (t.ktm = p.ktm)
    left join dokumnag n on (n.ref = p.dokument)
  where p.data > current_date - 14 and p.mwsordswaiting = 1
     and p.genmwsordsafter = 1
   -- and p.iloscl > coalesce(p.ilosconmwsacts,0)
    and t.usluga <> 1
  --  and n.akcept = 1
  group by p.wersjaref, p.ktm, n.magazyn
  ;
