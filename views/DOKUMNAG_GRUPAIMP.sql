--@@@***SKEY***@@@--
CREATE OR ALTER VIEW DOKUMNAG_GRUPAIMP (
      DOKREF ,-- DOKUMNAG_ID
      MAGAZYN,-- DEFMAGAZ_ID
      DOKTYP ,-- DEFDOKUM_ID
      SYMBOL ,-- SYMBOL_ID
      DATA   -- TIMESTAMP
  )
  AS 
select d.ref, d.magazyn, d.typ, d.symbol, d.data
  from dokumnag d
    left join defmagaz m on (m.symbol = d.magazyn)
    left join defdokum f on (f.symbol = d.typ)
where f.wydania = 0 and f.koryg = 0 and f.zewn = 1 and m.mws = 0
  ;
