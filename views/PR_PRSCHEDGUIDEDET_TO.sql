--@@@***SKEY***@@@--
CREATE OR ALTER VIEW PR_PRSCHEDGUIDEDET_TO (
      PRDEPART    ,-- PRDEPARTS_ID
      REF         ,-- INTEGER
      PRSCHEDGUIDE,-- PRSCHEDGUIDES_ID
      KTM         ,-- KTM_ID
      WERSJAREF   ,-- WERSJE_ID
      NAZWAT      ,-- TOWARY_NAZWA
      SYMBOL      ,-- STRING40
      ILOSC       ,-- ILOSCI_MAG
      PONADLIMIT  -- SMALLINT_ID
  )
  AS 
select g.prdepart, d.ref, g.ref, d.ktm, d.wersjaref, t.nazwa, g.symbol, d.quantity, d.overlimit
  from prschedguidedets d
    left join prschedguidespos p on (p.ref = d.prschedguidepos)
    left join prschedguides g on (g.ref = d.prschedguide)
    left join towary t on (t.ktm = d.ktm)
  where d.prschedguidepos is not null and d.out = 1 and d.ssource = 1 and d.sref is null
    and (d.quantity > 0 or d.overlimit > 0)
  ;
