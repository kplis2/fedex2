--@@@***SKEY***@@@--
CREATE OR ALTER VIEW BKACCOUNTS_VIEW (
      REF           ,-- BKACCOUNTS_ID
      NAME          ,-- VARCHAR(66) CHARACTER SET UTF8                           
      YEARID        ,-- YEARS_ID
      COMPANY       ,-- COMPANIES_ID
      MATCHABLE     ,-- SMALLINT_ID
      MATCHINGSLODEF-- SLO_ID
  )
  AS 
select b.ref, b.symbol||' - '||b.descript, b.yearid, b.company, b.matchable, b.matchingslodef
  from bkaccounts b
  ;
