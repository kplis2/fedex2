--@@@***SKEY***@@@--
CREATE OR ALTER VIEW X_STANY_HERMON (
      ILOSC    ,-- QUANTITY_MWS
      ID_TOWARU,-- INTEGER
      LOKACJA  -- STRING20
  )
  AS 
select M.quantity, cast(T.int_id as integer), C.symbol
    from MWSSTOCK M
      JOIN MWSCONSTLOCS C ON (M.mwsconstloc = C.ref)
      JOIN TOWARY T ON (M.good = T.ktm)

  ;
