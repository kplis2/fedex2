--@@@***SKEY***@@@--
CREATE OR ALTER VIEW ZUS (
      REF           ,-- ZUSDATA_ID
      PERSON        ,-- PERSONS_ID
      FROMDATE      ,-- TIMESTAMP_ID
      NFZ           ,-- STRING1024
      INSURDATE     ,-- TIMESTAMP_ID
      ASCODE        ,-- STRING1024
      INVALIDITY    ,-- STRING1024
      INVALIDITYFROM,-- TIMESTAMP_ID
      INVALIDITYTO  ,-- TIMESTAMP_ID
      DISABILITY    ,-- STRING1024
      RETIRED       ,-- STRING1024
      NFZFUND       ,-- SMALLINT_ID
      WORKFUND      ,-- SMALLINT_ID
      FGSP          ,-- SMALLINT_ID
      BASEINCYEAR   ,-- YEARS_ID
      BASEINC       ,-- MONEY
      BLOCKADE      ,-- STRING1024
      BLOCKDATE     ,-- TIMESTAMP_ID
      AID           ,-- SMALLINT_ID
      SPECCONDITION ,-- STRING1024
      SPECFROM      ,-- TIMESTAMP_ID
      SPECTO        ,-- TIMESTAMP_ID
      NINOTIFICATION-- SMALLINT_ID
  )
  AS 
select EZUSDATA.REF, PERSON, EZUSDATA.FROMDATE, NFZ.descript, INSURDATE, ASCODE.descript, INVALIDITY.descript, INVALIDITYFROM,
INVALIDITYTO, DISABILITY.descript, RETIRED.descript, NFZFUND, WORKFUND, FGSP, BASEINCYEAR,
BASEINC, BLOCKADE.descript, BLOCKDATE, AID, SPECCONDITION.descript, SPECFROM,
SPECTO, NINOTIFICATION
from EZUSDATA
left join EDICTZUSCODES nfz on (ezusdata.nfz = nfz.ref)
left join EDICTZUSCODES ascode on (ezusdata.ascode = ascode.ref)
left join EDICTZUSCODES invalidity on (ezusdata.invalidity = invalidity.ref)
left join EDICTZUSCODES disability on (ezusdata.disability = disability.ref)
left join EDICTZUSCODES retired on (ezusdata.retired = retired.ref)
left join EDICTZUSCODES blockade on (ezusdata.blockade = blockade.ref)
left join EDICTZUSCODES SPECCONDITION on (ezusdata.SPECCONDITION = SPECCONDITION.ref)
  ;
