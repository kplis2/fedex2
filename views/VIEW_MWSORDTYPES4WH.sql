--@@@***SKEY***@@@--
CREATE OR ALTER VIEW VIEW_MWSORDTYPES4WH (
      REF              ,-- MWSORDTYPES_ID
      WH               ,-- DEFMAGAZ_ID
      SYMBOL           ,-- STRING20
      NAME             ,-- STRING60
      MWSORDTYPEDETS   ,-- SMALLINT_ID
      WHMWSORDTYPEORDER-- SMALLINT_ID
  )
  AS 
select t.ref, w.wh, t.symbol, t.name, t.mwsortypedets, w.whmwsordtypeorder
  from mwsordtypes4wh w
    join mwsordtypes t on (t.ref = w.mwsordtype)
  ;
