--@@@***SKEY***@@@--
CREATE OR ALTER VIEW DEFFOLDVIEW (
      REF          ,-- DEFFOLD_ID
      NAZWA        ,-- STRING
      ROLA         ,-- SMALLINT_ID
      DOMYSLNY     ,-- SMALLINT
      OPEREDIT     ,-- STRING
      MAILBOX_NAZWA,-- STRING255
      USERNAME     ,-- STRING255
      EMAIL        -- EMAILS_ID
  )
  AS 
select DEFFOLD.ref, DEFFOLD.nazwa, DEFFOLD.rola, DEFFOLD.domyslny, DEFFOLD.operedit,
         MAILBOXES.nazwa, MAILBOXES.username, MAILBOXES.email
  from DEFFOLD
  left join MAILBOXES on (DEFFOLD.mailbox = MAILBOXES.ref)
  ;
