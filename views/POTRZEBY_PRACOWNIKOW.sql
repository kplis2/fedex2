--@@@***SKEY***@@@--
CREATE OR ALTER VIEW POTRZEBY_PRACOWNIKOW (
      REF           ,-- EMPLTRAINNEEDS_ID
      EMPLOYEE      ,-- EMPLOYEES_ID
      EMPLOYEEOPIS  ,-- STRING120
      ETRAINNEED    ,-- ETRAINNEEDS_ID
      ETRAINNEEDOPIS,-- STRING60
      ETRAINING     ,-- ETRAININGS_ID
      ETRAININGOPIS -- STRING80
  )
  AS 
select t.REF, t.EMPLOYEE, e.personnames, t.ETRAINNEED, n.subject, t.ETRAINING, r.subject
  from empltrainneeds t
  left join employees e on e.ref = t.employee
  left join etrainneeds n on n.ref = t.etrainneed
  left join etrainings r on r.ref = t.etraining
  ;
