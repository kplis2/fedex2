--@@@***SKEY***@@@--
CREATE OR ALTER VIEW RACHUNKI (
      REF         ,-- EPAYROLLS_ID
      SYMBOL      ,-- STRING20
      BILLDATE    ,-- TIMESTAMP_ID
      PAYDAY      ,-- TIMESTAMP_ID
      PVALUE      ,-- CENY
      STATUS      ,-- SMALLINT_ID
      STATUS_OPIS ,-- CHAR(52) CHARACTER SET UTF8                           
      EMPLCONTRACT,-- EMPLCONTRACTS_ID
      CONTRNO     ,-- STRING20
      ECONTRTYPE  ,-- ECONTRTYPES_ID
      FROMDATE    ,-- TIMESTAMP_ID
      TODATE      ,-- TIMESTAMP_ID
      PRCOSTS     ,-- NUMERIC_14_2
      EMPLOYEE    ,-- EMPLOYEES_ID
      PERSON      ,-- PERSONS_ID
      PERSONNAMES ,-- STRING120
      FNAME       ,-- PERSON_NAMES
      SNAME       ,-- PERSON_NAME
      NUMBER      ,-- SMALLINT_ID
      COMPANY     -- COMPANIES_ID
  )
  AS 
select REF, SYMBOL, BILLDATE, PAYDAY, PVALUE, STATUS,
    case when status = 0 then 'w redakcji' else case when status = 1 then 'zaakceptowany' else 'zaksięgowany' end end,
    EMPLCONTRACT, CONTRNO, ECONTRTYPE,
    FROMDATE, TODATE, PRCOSTS, EMPLOYEE, PERSON, PERSONNAMES, FNAME, SNAME, NUMBER, COMPANY
  from eprbills
  ;
