--@@@***SKEY***@@@--
CREATE OR ALTER VIEW STALE_SKLADNIKI (
      EMPLOYEE    ,-- EMPLOYEES_ID
      ECOLUMN     ,-- ECOLUMNS_ID
      ECOLUMN_OPIS,-- STRING22
      FROMDATE    ,-- TIMESTAMP_ID
      TODATE      ,-- TIMESTAMP_ID
      EVALUE      ,-- MONEY
      ETYPE       -- SMALLINT_ID
  )
  AS 
select h.EMPLOYEE, h.ECOLUMN, c.name, h.FROMDATE, h.TODATE, h.EVALUE, h.ETYPE
  from econstelems h
    join ecolumns c on c.number = h.ecolumn
  ;
