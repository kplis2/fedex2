--@@@***SKEY***@@@--
CREATE OR ALTER VIEW FIRMY_SZKOLENIOWE (
      REF        ,-- ETRAINFIRMS_ID
      NAME       ,-- STRING60
      DESCRIPT   ,-- STRING
      CITY       ,-- CITY_ID
      STREET     ,-- STREET_ID
      POSTCODE   ,-- POSTCODE
      POST       ,-- STRING40
      PHONENR    ,-- PHONES_ID
      FAXNR      ,-- FAX_ID
      EMAIL      ,-- EMAILS_ID
      WWW        ,-- WEBPAGES_ID
      CONTACTPERS-- STRING40
  )
  AS 
select REF, NAME, DESCRIPT, CITY, STREET, POSTCODE, POST, PHONENR, FAXNR, EMAIL, WWW, CONTACTPERS
  from etrainfirms
  ;
