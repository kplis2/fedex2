--@@@***SKEY***@@@--
CREATE OR ALTER VIEW SZKOLY (
      REF           ,-- EPERSCHOOLS_ID
      PERSON        ,-- PERSONS_ID
      NAME          ,-- STRING80
      FROMDATE      ,-- TIMESTAMP_ID
      TODATE        ,-- TIMESTAMP_ID
      ISVACBASE     ,-- SMALLINT_ID
      EDUCATION     ,-- STRING1024
      DEPARTMENT    ,-- STRING80
      FACULTY       ,-- STRING80
      SPECIALIZATION-- STRING80
  )
  AS 
select eperschools.REF, PERSON, NAME, eperschools.FROMDATE, eperschools.TODATE, ISVACBASE, EDICTZUSCODES.descript,
 DEPARTMENT, FACULTY, SPECIALIZATION
from eperschools
left join EDICTZUSCODES on (eperschools.education = EDICTZUSCODES.ref)
  ;
