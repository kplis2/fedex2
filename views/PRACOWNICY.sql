--@@@***SKEY***@@@--
CREATE OR ALTER VIEW PRACOWNICY (
      REF             ,-- EMPLOYEES_ID
      PERSON          ,-- PERSONS_ID
      FILENO          ,-- STRING20
      PERSONNAMES     ,-- STRING120
      FNAME           ,-- PERSON_NAMES
      SNAME           ,-- PERSON_NAME
      BRANCH          ,-- STRING
      BRANCHSYMBOL    ,-- ODDZIAL_ID
      DEPARTMENT      ,-- STRING60
      DEPARTMENTSYMBOL,-- DEPARTMENT_ID
      EMPLTYPE        ,-- SMALLINT_ID
      WORKTYPE        ,-- STRING
      EMPLSTATUS      ,-- SMALLINT_ID
      ECONTRTYPE      ,-- STRING30
      CALENDAR        ,-- ECALENDARS_ID
      SYMBOL          ,-- ANALYTIC_ID
      WORKPOST        ,-- STRING80
      ORDERJOB        ,-- SMALLINT_ID
      COMPANY         -- COMPANIES_ID
  )
  AS 
select employees.REF, PERSON, FILENO, PERSONNAMES, FNAME, SNAME, oddzialy.nazwa, oddzialy.oddzial, DEPARTMENTS.name,departments.symbol,
employees.EMPLTYPE, edictworktypes.descript, EMPLSTATUS, ECONTRTYPEs.tname, CALENDAR, employees.SYMBOL,
edictworkposts.fullname, ORDERJOB, employees.company
from employees
left join oddzialy on (employees.branch = oddzialy.oddzial)
left join departments on (employees.department = departments.symbol)
left join edictworkposts on (employees.workpost = edictworkposts.ref)
left join edictworktypes on (employees.worktype = edictworktypes.ref)
left join econtrtypes on (employees.econtrtype = econtrtypes.contrtype)
  ;
