--@@@***SKEY***@@@--
CREATE OR ALTER VIEW SLODEFVIEW (
      REF           ,-- SLO_ID
      NAZWA         ,-- VARCHAR(20) CHARACTER SET UTF8                           
      TYP           ,-- SLOTYP_ID
      PREDEF        ,-- SMALLINT_ID
      CRM           ,-- SMALLINT_ID
      KASA          ,-- SMALLINT_ID
      PREFIXFK      ,-- KONTO_ID
      GRIDNAME      ,-- STRING40
      GRIDNAMEDICT  ,-- STRING40
      FORMNAME      ,-- STRING40
      FIRMA         ,-- SMALLINT_ID
      TRYBRED       ,-- SMALLINT_ID
      MAG           ,-- SMALLINT_ID
      OPAK          ,-- SMALLINT_ID
      KARTOTEKA     ,-- SMALLINT_ID
      ANALITYKA     ,-- SMALLINT_ID
      KODLN         ,-- SMALLINT_ID
      STATE         ,-- STATE
      TOKEN         ,-- SMALLINT_ID
      ISDIST        ,-- SMALLINT_ID
      MASTERTABLE   ,-- STABLE_ID
      LOGISTYKA     ,-- SMALLINT_ID
      PERSONEL      ,-- SMALLINT_ID
      BKBROWSEWINDOW,-- STRING40
      PRODUKCJA     ,-- SMALLINT_ID
      SYMBOL        ,-- STRING10
      DICTPOSFIELD  ,-- STRING40
      KODFIELD      ,-- STRING40
      NAZWAFIELD    ,-- STRING40
      KODKSFIELD    ,-- STRING40
      NIPFIELD      ,-- STRING40
      ISCOMPANY     ,-- SMALLINT_ID
      MULTIDIST     ,-- SMALLINT
      PATTERN_REF   ,-- SLO_ID
      COMPANY       ,-- COMPANIES_ID
      INTERNALUPDATE,-- SMALLINT_ID
      WPKPERCOMPANY ,-- SMALLINT_ID
      PATTERNREF    -- SLO_ID
  )
  AS 
SELECT
    ref,
    nazwa,
    typ,
    predef,
    crm,
    kasa,
    prefixfk,
    gridname,
    gridnamedict,
    formname,
    firma,
    trybred,
    mag,
    opak,
    kartoteka,
    analityka,
    kodln,
    state,
    token,
    isdist,
    mastertable,
    logistyka,
    personel,
    bkbrowsewindow,
    produkcja,
    symbol,
    dictposfield,
    kodfield,
    nazwafield,
    kodksfield,
    nipfield,
    iscompany,
    multidist,
    pattern_ref,
    company,
    internalupdate,
    wpkpercompany,
    patternref
FROM slodef
  ;
