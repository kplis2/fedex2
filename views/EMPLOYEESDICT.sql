--@@@***SKEY***@@@--
CREATE OR ALTER VIEW EMPLOYEESDICT (
      REF               ,-- EMPLOYEES_ID
      BRANCH            ,-- ODDZIAL_ID
      DEPARTMENT        ,-- DEPARTMENT_ID
      WORKPOST          ,-- STRING40
      EMPLSTATUS        ,-- SMALLINT_ID
      ECONTRTYPE        ,-- STRING30
      EMPLTYPE          ,-- SMALLINT_ID
      WORKTYPE          ,-- STRING
      FNAME             ,-- PERSON_NAMES
      SNAME             ,-- PERSON_NAME
      PERSONNAMES       ,-- STRING120
      CALENDAR          ,-- ECALENDARS_ID
      FILENO            ,-- STRING20
      SYMBOL            ,-- ANALYTIC_ID
      ORDERJOB          ,-- SMALLINT_ID
      BKSYMBOL          ,-- ANALYTIC_ID
      COMPANY           ,-- COMPANIES_ID
      PRDEPART          ,-- PRDEPARTS_ID
      FROMDATE          ,-- TIMESTAMP_ID
      TODATE            ,-- TIMESTAMP_ID
      EHRM              ,-- SMALLINT_ID
      MIDDLENAME        ,-- PERSON_NAME
      ERECRUIT          ,-- ERECRUITS_ID
      OUTOFWORK30       ,-- SMALLINT_ID
      DEPARTMENTLIST    ,-- STRING
      EMPLGROUP         ,-- EDICTEMPLGROUP_ID
      SYEARS            ,-- SMALLINT_ID
      SMONTHS           ,-- SMALLINT_ID
      SDAYS             ,-- SMALLINT_ID
      SUPERIOR          ,-- EMPLOYEES_ID
      SUPERIORDEPUTY    ,-- EMPLOYEES_ID
      SUPERIORFROMDEPART-- SMALLINT_ID
  )
  AS 
select e.ref, e.branch, e.department, w.workpost, e.emplstatus, t.tname as econtrtype,
    e.empltype, p.descript as worktype, e.fname, e.sname, e.personnames, e.calendar,
    e.fileno, e.symbol, e.orderjob, e.bksymbol, e.company, e.prdepart, e.fromdate,
    e.todate, e.ehrm, e.middlename, e.erecruit, e.outofwork30, e.departmentlist,
    e.emplgroup, e.syears,  e.smonths, e.sdays, e.superior, e.superiordeputy, e.superiorfromdepart
  from employees e
  left join edictworkposts w on (w.ref = e.workpost)
  left join econtrtypes t on (t.contrtype = e.econtrtype)
  left join edictworktypes p on (p.ref = e.worktype)
  ;
