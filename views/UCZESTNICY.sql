--@@@***SKEY***@@@--
CREATE OR ALTER VIEW UCZESTNICY (
      REF         ,-- EMPLTRAININGS_ID
      EMPLOYEE    ,-- EMPLOYEES_ID
      STATUS      ,-- CHAR(48) CHARACTER SET UTF8                           
      COMPLDATE   ,-- TIMESTAMP_ID
      MARK        ,-- STRING80
      TRAINING    ,-- ETRAININGS_ID
      LOYALITYFROM,-- DATE_ID
      LOYALITYTO  -- DATE_ID
  )
  AS 
select REF, EMPLOYEE,
    case when status = 0 then 'zapisany' else case when status = 1 then 'ukończył' else 'nie ukończył' end end,
    COMPLDATE, MARK, TRAINING, LOYALITYFROM, LOYALITYTO
  from empltrainings
  ;
