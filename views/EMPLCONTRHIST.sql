--@@@***SKEY***@@@--
CREATE OR ALTER VIEW EMPLCONTRHIST (
      REF         ,-- INTEGER
      EMPLOYEE    ,-- INTEGER
      FROMDATE    ,-- TIMESTAMP
      TODATE      ,-- TIMESTAMP
      EMPLCONTRACT,-- INTEGER
      ANNEXE      ,-- INTEGER
      WORKPOST    ,-- INTEGER
      WORKDIM     ,-- FLOAT
      DIMNUM      ,-- SMALLINT
      DIMDEN      ,-- SMALLINT
      BRANCH      ,-- VARCHAR(10) CHARACTER SET UTF8                           
      DEPARTMENT  ,-- VARCHAR(10) CHARACTER SET UTF8                           
      SALARY      ,-- NUMERIC(18,2)
      PAYMENTTYPE ,-- SMALLINT
      CADDSALARY  ,-- NUMERIC(18,2)
      FUNCSALARY  ,-- NUMERIC(18,2)
      DICTDEF     ,-- INTEGER
      DICTPOS     ,-- INTEGER
      BKSYMBOL    ,-- VARCHAR(20) CHARACTER SET UTF8                           
      LOCAL       ,-- VARCHAR(80) CHARACTER SET UTF8                           
      CALENDAR    ,-- INTEGER
      PROPORTIONAL,-- SMALLINT
      EMPLGROUP   ,-- INTEGER
      WORKCAT     ,-- INTEGER
      WORKSYSTEM  ,-- INTEGER
      EPAYRULE    ,-- INTEGER
      EMPLTYPE    ,-- INTEGER
      RATIO       ,-- NUMERIC(18,3)
      BANKACCOUNT ,-- INTEGER
      PRCOSTS     -- NUMERIC(18,2)
  )
  AS 
with firstannexes as (
  select emplcontract, min(fromdate) as minfromdate
    from econtrannexes
    group by emplcontract
)
  select null, c.employee, c.fromdate, coalesce(f.minfromdate - 1, c.enddate) as todate
       , c.ref, null as annexe, c.workpost, c.workdim, c.dimnum, c.dimden, c.branch
       , c.department, c.salary, c.paymenttype, c.caddsalary, c.funcsalary, c.dictdef, c.dictpos
       , c.bksymbol, c.local, c.calendar, c.proportional, c.emplgroup, c.workcat, c.worksystem
       , c.epayrule, c.empltype, c.ratio, c.bankaccount, c.prcosts
    from emplcontracts c
    left join firstannexes f on (c.ref = f.emplcontract)
    where c.empltype > 1
union
  select null, c.employee, a.fromdate, coalesce(a2.fromdate - 1, c.enddate) as todate
       , c.ref, a.ref, a.workpost, a.workdim, a.dimnum, a.dimden, a.branch
       , a.department, a.salary, a.paymenttype, a.caddsalary, a.funcsalary, a.dictdef, a.dictpos
       , a.bksymbol, a.local, a.calendar, a.proportional, a.emplgroup, a.workcat, a.worksystem
       , a.epayrule, c.empltype, c.ratio, iif (a.ref is not null, a.bankaccount, c.bankaccount) --PR55960 
       , c.prcosts
    from emplcontracts c
    left join econtrannexes a on (c.ref = a.emplcontract)
    left join econtrannexes a2 on (a.emplcontract = a2.emplcontract and a2.fromdate > a.fromdate)
    left join econtrannexes a3 on (a.emplcontract = a3.emplcontract and a3.fromdate > a.fromdate and a3.fromdate < a2.fromdate)
    where c.empltype > 1
      and a.ref is not null
      and a3.ref is null
union
  select ref, employee, fromdate, todate
       , emplcontract, annexe, workpost, workdim, dimnum, dimden, branch
       , department, salary, paymenttype, caddsalary, funcsalary, dictdef, dictpos
       , bksymbol, local, calendar, proportional, emplgroup, workcat, worksystem
       , epayrule, 1, 0, 0, 0 as empltype
    from employment
  ;
