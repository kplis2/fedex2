--@@@***SKEY***@@@--
CREATE OR ALTER VIEW POZFAK_VIEW (
      REF     ,-- POZFAK_ID
      KTM     ,-- KTM_ID
      NAZWA   ,-- TOWARY_NAZWA
      DOKUMENT-- NAGFAK_ID
  )
  AS 
select p.ref, p.ktm, t.nazwa, p.dokument
  from pozfak p join towary t on t.ktm = p.ktm
  ;
