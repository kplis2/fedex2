--@@@***SKEY***@@@--
CREATE OR ALTER VIEW POTR_KOMORNICZE (
      REF          ,-- ECOLLDEDUCTIONS_ID
      PERSON       ,-- PERSONS_ID
      FROMDATE     ,-- DATE_ID
      DKIND        ,-- ECOLUMNS_ID
      DKIND_OPIS   ,-- STRING22
      INITDEDUCTION,-- NUMERIC_14_2
      ACTDEDUCTION ,-- NUMERIC_14_2
      DEDUCTIONRATE,-- NUMERIC_14_2
      DEDUCTED     ,-- NUMERIC_14_2
      PRIORITY     ,-- SMALLINT_ID
      BAILIFF      ,-- EBAILIFF_ID
      BAILIFF_OPIS ,-- STRING
      TODATE       -- DATE_ID
  )
  AS 
select d.ref,  d.person, d.fromdate, d.dkind, c.name, d.initdeduction, d.actdeduction,
    d.deductionrate, d.deducted, d.priority, d.bailiff, b.bailiffname, d.todate
  from ecolldeductions d
    join ecolumns c on d.dkind = c. number
    left join ebailiff b on d.bailiff = b.ref
  ;
