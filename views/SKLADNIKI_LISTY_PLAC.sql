--@@@***SKEY***@@@--
CREATE OR ALTER VIEW SKLADNIKI_LISTY_PLAC (
      PAYROLL     ,-- EPAYROLLS_ID
      EMPLOYEE    ,-- EMPLOYEES_ID
      ECOLUMN     ,-- ECOLUMNS_ID
      ECOLUMN_OPIS,-- STRING22
      PVALUE      -- CENY
  )
  AS 
select p.payroll, p.employee, p.ecolumn, c.name, p.pvalue
  from eprpos p
    join epayrolls r on p.payroll = r.ref
    join ecolumns c on c.number = p.ecolumn
  where r.empltype = 1
  ;
