--@@@***SKEY***@@@--
CREATE OR ALTER VIEW X_SYS_OLD_TRANSACTIONS (
      MONUSER          ,-- CHAR(31) CHARACTER SET UNICODE_FSS                    
      MONREMOTE_ADDRESS,-- VARCHAR(255) CHARACTER SET ASCII                          
      MONTIMESTAMP     ,-- TIMESTAMP
      MONTRANSTIMESTAMP,-- TIMESTAMP
      TIMED            ,-- BIGINT
      MONREMOTE_PROCESS,-- VARCHAR(255) CHARACTER SET UNICODE_FSS                    
      MONTRANSACTION_ID,-- 
      NAZWA            ,-- VARCHAR(255) CHARACTER SET UTF8                           
      MONSTATE         ,-- 
      MONSERVER_PID    ,-- 
      SQL_TEXT         ,-- BLOB SUB_TYPE 1 CHARACTER SET UNICODE_FSS                    
      OPERREF          ,-- OPERATOR_ID
      ATTACHMENT_ID    -- 
  )
  AS 
select a.mon$user as monuser, a.mon$remote_address as monremote_address,
       a.mon$timestamp as montimestamp, s.mon$timestamp as MONTRANSTIMESTAMP,
       ((current_timestamp -s.mon$timestamp) * 1440) as TIMED,
       a.mon$remote_process as mon$remote_process, s.mon$transaction_id as mon$transaction_id,
       coalesce(o.nazwa, ib.pvalue) as nazwa, s.mon$state as monstate,
       a.mon$server_pid as monserver_pid, st.mon$sql_text, o.ref as operref,
       a.mon$attachment_id as attachment_id
  from mon$attachments a
    left join mon$transactions s on (s.mon$attachment_id = a.mon$attachment_id)
    left join mon$statements st on (st.mon$attachment_id = a.mon$attachment_id and st.mon$transaction_id = s.mon$transaction_id)
    left join globalparams g on (g.connectionid = a.mon$attachment_id and g.psymbol = 'AKTUOPERATOR')
    left join globalparams ib on (ib.connectionid = a.mon$attachment_id and ib.psymbol = 'IBOPERATOR')
    left join operator o on (o.ref = g.pvalue)
  where s.mon$transaction_id is not null
  order by s.mon$transaction_id
  ;
