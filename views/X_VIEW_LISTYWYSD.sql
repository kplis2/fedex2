--@@@***SKEY***@@@--
CREATE OR ALTER VIEW X_VIEW_LISTYWYSD (
      REF        ,-- LISTYWYSD_ID
      TYP        ,-- CHAR(4) CHARACTER SET UTF8                           
      SYMBOL     ,-- STRING
      SYMBOLSPED ,-- STRING40
      AKCEPT     ,-- SMALLINT
      KONTRAHENT ,-- STRING
      ADRES      ,-- STRING
      MIASTO     ,-- CITY_ID
      KODP       ,-- POSTCODE
      NRDOMUM    ,-- NRDOMU_ID
      NRLOKALU   ,-- NRDOMU_ID
      TELEFON    ,-- PHONES_ID
      ILOSCPACZEK,-- SMALLINT_ID
      ILOSCPALET ,-- RPEPLICAT
      WAGA       ,-- WAGA_ID
      POBRANIE   ,-- CENY
      CHECKOPER  ,-- OPERATOR_ID
      ACCEPTOPER ,-- OPERATOR_ID
      DATAAKC    ,-- DATAAKC_ID
      DATAZM     ,-- TIMESTAMP_ID
      SPOSDOST   ,-- SPOSDOST_ID
      NAZWA      -- STRING
  )
  AS 
select l.ref, l.typ, l.symbol, l.symbolsped, l.akcept,
    l.kontrahent, l.adres, l.miasto, l.kodp, l.nrdomu, l.nrlokalu, l.telefon,
    l.iloscpaczek, l.iloscpalet, l.waga, l.pobranie,
    l.checkoper, l.acceptoper, l.dataakc, l.datazam,
    l.sposdost, s.nazwa
  from listywysd l
    left join sposdost s on (l.sposdost = s.ref)
where l.akcept > 0
  ;
