--@@@***SKEY***@@@--
CREATE OR ALTER VIEW VIEW_MWSQ_BRANCH_DETAILS (
      NUMBER  ,-- INTEGER
      VERS    ,-- INTEGER
      BRANCH  ,-- VARCHAR(10) CHARACTER SET UTF8                           
      DESCRIPT,-- CHAR(112) CHARACTER SET UTF8                           
      PM      ,-- CHAR(4) CHARACTER SET UTF8                           
      QUANTITY-- NUMERIC(18,4)
  )
  AS 
select 0, s.vers, s.branch, 'Na lokacjach do sprzedaży' as descript, '+' as pm,
      coalesce(sum(s.quantity - s.blocked + s.ordered),0) as quantity
    from mwsstock s
      left join mwsconstlocs c on (s.mwsconstloc = c.ref)
      left join defmagaz d on (d.symbol = c.wh)
    where c.act > 0
      and c.locdest in (1,2,3,4,5,9)
      and d.mws = 1
    group by vers, branch
  union
  select 1, a.vers, o.branch, 'Dostawienie na lok. do sprz.' as descript, '+' as pm,
      coalesce(sum(a.quantity),0) as quantity
    from mwsords o
      left join mwsacts a on (a.mwsord = o.ref)
    where o.mwsordtypedest = 9
      and o.status > 0
      and o.status < 3
      and a.status > 0
      and a.status < 6
    group by vers, branch
  union
  select 2, p.wersjaref as vers, m.oddzial as branch, 'Oczekuje na zlecenia' as descript, '-' as pm,
      coalesce(sum(case when n.mwsdisposition = 1 then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0)),0)
    from defmagaz m
      left join dokumnag n on (m.symbol = n.magazyn)
      left join dokumpoz p on (n.ref = p.dokument)
      left join defdokummag d on (d.typ = n.typ and d.magazyn = n.magazyn)
    where m.mws = 1
      and n.mwsdone = 0
      and n.wydania = 1
      and n.mwsdoc = 1
      and coalesce(n.blockmwsords,0) = 0
      and n.frommwsord is null
      and ((n.akcept = 1 and p.iloscl > p.ilosconmwsacts) or
           (n.mwsdisposition = 1 and p.ilosc > p.ilosconmwsacts))
      and p.genmwsordsafter = 1
      and coalesce(p.fake,0) = 0
      and coalesce(p.havefake,0) = 0
      and coalesce(d.afterackproc,'') <> ''
      and d.mwsordwaiting = 1
    group by vers, branch
  union
  select 3, s.wersjaref as vers, m.oddzial as branch, 'Zablokowano na zamówieniach' as descript, '-' as pm,
      sum(s.zablokow) as quantity
    from defmagaz m
      left join stanyil s on (m.symbol = s.magazyn)
    where m.mws = 1
    group by vers, branch
  ;
