--@@@***SKEY***@@@--
CREATE OR ALTER VIEW VIEW_MWS_QUANTITY_DETAILS_SUM (
      NUMBER  ,-- INTEGER
      GOOD    ,-- VARCHAR(40) CHARACTER SET UTF8                           
      VERS    ,-- INTEGER
      WH      ,-- VARCHAR(3) CHARACTER SET UTF8                           
      DESCRIPT,-- CHAR(112) CHARACTER SET UTF8                           
      PM      ,-- CHAR(4) CHARACTER SET UTF8                           
      QUANTITY-- DOUBLE PRECISION
  )
  AS 
select v.number, v.good, v.vers, v.wh, v.descript, v.pm, abs(v.quantity) as quantity
  from VIEW_MWS_QUANTITY_DETAILS v
union
select 4, v.good, v.vers, v.wh, 'Suma dostępnych do sprzedaży' as descript, '', sum(v.quantity) as quantity
  from VIEW_MWS_QUANTITY_DETAILS v
  group by v.wh, v.good, v.vers, v.wh
  ;
