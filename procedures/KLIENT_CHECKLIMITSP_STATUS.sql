--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KLIENT_CHECKLIMITSP_STATUS(
      ZAM integer,
      OPERATOR integer)
  returns (
      STATUS integer,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable slodef integer;
declare variable st varchar(255);
declare variable klient integer;
declare variable data timestamp;
declare variable sumwartbruzam numeric(14,2);
declare variable LIMIT NUMERIC(14,2);
declare variable LIMITUSED NUMERIC(14,2);
declare variable LIMITTOUSE NUMERIC(14,2);
declare variable LIMITTYP smallint;
begin
  status = 1;
  limit = 0;
  limittouse  = 0;
  sumwartbruzam = 0;
  select KLIENCI.limitsprz, KLIENCI.limitsprtyp, KLIENCI.REF, current_date, NAGZAM.SUMWARTBRU
   from NAGZAM join KLIENCI on (NAGZAM.KLIENT = KLIENCI.REF)
   where NAGZAM.REF=:zam
   into :limit,:limittyp, :klient, :data, :sumwartbruzam;
  if(:limit is null) then limit = 0;
  if(:limittyp is null) then limittyp = 0;
  if(:limit > 0 and :limittyp > 0) then begin
    select REF from SLODEF where SLODEF.typ='KLIENCI' into :slodef;
    st = 'select sum(WINIENZL - MAZL) from ROZRACH where SLODEF ='||:slodef||' and slopoz ='||:KLIENT||' and ';
    if(:limittyp = 1) then st = :st||' substring(rozrach.dataotw from 1 for 7) = '''||substring(:data from 1 for 7)||'''';
    if(:limittyp = 3) then st = :st||' substring(rozrach.dataotw from 1 for 4) = '''||substring(:data from 1 for 4)||'''';
    execute statement :st into :limitused;
    if(:limitused is null) then limitused = 0;
    if(:limitused + :sumwartbruzam> :limit) then begin
      status = 0;
    end
  end
end^
SET TERM ; ^
