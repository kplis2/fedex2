--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CONTROL_REGON_NUMBER(
      REGON varchar(14) CHARACTER SET UTF8                           )
  returns (
      CORRECT integer,
      MSG varchar(60) CHARACTER SET UTF8                           )
   as
declare variable total int;
declare variable i int;
declare variable digit int;
declare variable scales int;
declare variable mod int;
declare variable resultmod int;
begin
  if (regon is NULL or regon = '') then begin
    msg = ''; correct = 1;
  end else begin
    msg = '';
    correct = 0;
    mod = 11;
    total = 0;
    if (coalesce(char_length(:regon),0) = 9 or coalesce(char_length(:regon),0) = 7) then begin -- [DG] XXX ZG119346
      if((coalesce(char_length(:regon),0) = 9) and (regon <> '' or regon is not null)) then begin -- [DG] XXX ZG119346
        i = 1;
        while (i < 9) do begin
          if (i = 1) then begin digit = substring(regon from 1 for 1 ); scales = 8; end
          if (i = 2) then begin digit = substring(regon from 2 for 1 ); scales = 9; end
          if (i = 3) then begin digit = substring(regon from 3 for 1 ); scales = 2; end
          if (i = 4) then begin digit = substring(regon from 4 for 1 ); scales = 3; end
          if (i = 5) then begin digit = substring(regon from 5 for 1 ); scales = 4; end
          if (i = 6) then begin digit = substring(regon from 6 for 1 ); scales = 5; end
          if (i = 7) then begin digit = substring(regon from 7 for 1 ); scales = 6; end
          if (i = 8) then begin digit = substring(regon from 8 for 1 ); scales = 7; end
          total = total + (scales * digit);
          i = i + 1;
        end
        digit = (substring(regon from 9 for 1 ));
      end
      if (coalesce(char_length(:regon),0) = 7 and (regon <> '' or regon is not null)) then begin -- [DG] XXX ZG119346
        i = 1;
        while (i < 7) do begin
          if (i = 1) then begin digit = substring(regon from 1 for 1 ); scales = 2; end
          if (i = 2) then begin digit = substring(regon from 2 for 1 ); scales = 3; end
          if (i = 3) then begin digit = substring(regon from 3 for 1 ); scales = 4; end
          if (i = 4) then begin digit = substring(regon from 4 for 1 ); scales = 5; end
          if (i = 5) then begin digit = substring(regon from 5 for 1 ); scales = 6; end
          if (i = 6) then begin digit = substring(regon from 6 for 1 ); scales = 7; end
          total = total + (scales * digit);
          i = i + 1;
        end
        digit = (substring(regon from 7 for 1 ));
      end

      resultmod = total-(floor(total / mod) * mod);
      if (resultmod = 10) then resultmod = 0;
      if (:digit = :resultmod) then correct = 1;
      else begin
        msg = 'Nie zgadza się cyfra kontrolna';
        correct = 0;
      end
    end else begin
      msg = 'Nieprawidłowa ilość cyfr';
      correct = 0;
    end
  end
  suspend;
end^
SET TERM ; ^
