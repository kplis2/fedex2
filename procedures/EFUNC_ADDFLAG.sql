--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EFUNC_ADDFLAG(
      CURFLAGS varchar(100) CHARACTER SET UTF8                           ,
      FLAG varchar(20) CHARACTER SET UTF8                           ,
      MINUS smallint = 0)
  returns (
      OUTFLAGS varchar(100) CHARACTER SET UTF8                           )
   as
begin
/*MWr: Funkcja do operacji na flagach. Dla biezacego pola z flagami CURFLAGS
       mozemy dodac (MINUS = 0 - domyslnie) lub wylaczyc (MINUS = 1) flage FLAG */

  curflags = coalesce(trim(curflags),'');
  flag = trim(both ';' from flag);

  if (flag <> '') then
  begin
  --dodawanie flagi
    if (coalesce(minus,0) = 0) then
    begin
      if (curflags = '' or curflags = ';') then
        outflags = ';' || flag || ';';
      else if (curflags not containing ';' || flag || ';') then
        outflags =  curflags || flag || ';';
      else
        outflags = curflags;
    end else
  --usuniecie flagi
    begin
      outflags = replace(curflags,  flag || ';',  '');
      if (outflags = ';') then outflags = '';
    end
  end else
    outflags = curflags;

  suspend;
end^
SET TERM ; ^
