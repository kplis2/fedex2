--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RKRAP_OBL_WAR(
      RAPORT integer)
   as
declare variable SUMPL decimal(14,2);
declare variable SUMPLZL decimal(14,2);
declare variable SUMPLTECHZL decimal(14,2);
declare variable SUMMI decimal(14,2);
declare variable SUMMIZL decimal(14,2);
declare variable SUMMITECHZL decimal(14,2);
declare variable KWOTABOZL numeric(14,2);
declare variable SKWOTAZL numeric(14,2);
declare variable WALUTOWY smallint;
begin
  select sum(KWOTA), sum(KWOTAZL)
    from RKDOKNAG
    where RAPORT=:raport
      and ((kwota > 0 and pm = '+')or(kwota < 0 and pm = '-'))
      and ANULOWANY=0
    into :sumpl, :sumplzl;
  if (:sumpl is null) then sumpl = 0;
  if (:sumplzl is null) then sumplzl = 0;

  select sum(abs(KWOTA)), sum(abs(KWOTAZL))
    from RKDOKNAG
    where RAPORT=:raport
      and ((kwota < 0 and pm = '+')or(kwota > 0 and pm = '-'))
      and ANULOWANY=0
    into  :summi, :summizl;
  if (:summi is null) then summi = 0;
  if (:summizl is null) then summizl = 0;

  /* BS38653 */
  select R.kwotabozl, S.walutowa
    from rkrapkas R
      left join RKSTNKAS S on (R.stanowisko = S.kod)
    where R.ref = :raport
    into :kwotabozl, :walutowy;

  if (:walutowy = 1) then begin
    select sum(cast(r.kwota * s.kurs as numeric(14,2)))
      from rkdoknag n
        join rkdokpoz p on (n.ref = p.dokument)
        join rkdokroz r on (p.ref = r.rkdokpoz)
        join rkstanwal s on (s.ref = r.rkstanwal)
      where n.RAPORT=:raport
        and ((n.kwota > 0 and n.pm = '+')or(n.kwota < 0 and n.pm = '-'))
        and n.ANULOWANY=0
      into :sumplzl;

    select -sum(cast(r.kwota * s.kurs as numeric(14,2)))
      from rkdoknag n
        join rkdokpoz p on (n.ref = p.dokument)
        join rkdokroz r on (p.ref = r.rkdokpoz)
        join rkstanwal s on (s.ref = r.rkstanwal)
      where n.RAPORT=:raport
        and ((n.kwota < 0 and n.pm = '+')or(n.kwota > 0 and n.pm = '-'))
        and n.ANULOWANY=0
      into :summizl;

          --<BS59416 - dodanie operacji technicznych ktore nie maja rkdokroz
    select sum(p.kwotazl)
      from rkdoknag n
        join rkdokpoz p on (n.ref = p.dokument)
        join rkdefoper o on (o.symbol = n.operacja and o.techoper = 1)
      where n.RAPORT=:raport
        and ((n.kwotazl > 0 and n.pm = '+')or(n.kwotazl < 0 and n.pm = '-'))
        and n.ANULOWANY=0
      into :sumpltechzl;

    select sum(p.kwotazl)
      from rkdoknag n
        join rkdokpoz p on (n.ref = p.dokument)
        join rkdefoper o on (o.symbol = n.operacja and o.techoper = 1)
      where n.RAPORT=:raport
        and ((n.kwotazl < 0 and n.pm = '+')or(n.kwotazl > 0 and n.pm = '-'))
        and n.ANULOWANY=0
      into :summitechzl;

      sumplzl = coalesce(:sumplzl, 0) + coalesce(:sumpltechzl, 0);
      summizl = coalesce(:summizl, 0) + coalesce(:summitechzl, 0);
    --BS59416>

    end
    skwotazl = coalesce(kwotabozl,0) + coalesce(sumplzl,0) - coalesce(summizl,0);
    if (skwotazl is null) then
      skwotazl = 0;


  update RKRAPKAS set  PRZYCHOD = :sumpl, PRZYCHODZL = :sumplzl,
                       ROZCHOD = :summi, ROZCHODZL = :summizl,
                       SKWOTAZL = :skwotazl
      where ref=:raport;
/* BS38653 */
end^
SET TERM ; ^
