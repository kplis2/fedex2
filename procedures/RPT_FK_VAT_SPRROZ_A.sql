--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_VAT_SPRROZ_A(
      PERIODID varchar(6) CHARACTER SET UTF8                           ,
      VREG varchar(10) CHARACTER SET UTF8                           ,
      ZAKRES smallint,
      ZPOPMIES smallint,
      TYP smallint,
      COMPANY integer,
      ZAKUP integer)
  returns (
      REF integer,
      LP integer,
      VATREG varchar(10) CHARACTER SET UTF8                           ,
      VATREGNR integer,
      TRANSDATE timestamp,
      DOCDATE timestamp,
      DOCSYMBOL varchar(20) CHARACTER SET UTF8                           ,
      BKREG varchar(10) CHARACTER SET UTF8                           ,
      BKREGNR integer,
      KONTRAHKOD varchar(255) CHARACTER SET UTF8                           ,
      NIP varchar(15) CHARACTER SET UTF8                           ,
      WARTBRU numeric(14,2),
      WARTNET numeric(14,2),
      WARTVAT numeric(14,2),
      WART22 numeric(14,2),
      VAT22 numeric(14,2),
      WART23 numeric(14,2),
      VAT23 numeric(14,2),
      WART12 numeric(14,2),
      VAT12 numeric(14,2),
      WART07 numeric(14,2),
      VAT07 numeric(14,2),
      WART08 numeric(14,2),
      VAT08 numeric(14,2),
      WART03 numeric(14,2),
      VAT03 numeric(14,2),
      WART05 numeric(14,2),
      VAT05 numeric(14,2),
      WART00 numeric(14,2),
      VAT00 numeric(14,2),
      WARTZW numeric(14,2),
      VATZW numeric(14,2),
      WARTPOZ numeric(14,2),
      VATPOZ numeric(14,2),
      VATGR varchar(5) CHARACTER SET UTF8                           )
   as
declare variable NETV numeric(14,2);
declare variable VATV numeric(14,2);
declare variable DOCREF integer;
declare variable SQL varchar(5048);
declare variable VATREGTYPE integer; /* <<PR73894: PL>> */
declare variable print integer;
begin
  --TOFIX!!! styl
  if (typ = -1) then
    typ = 0; -- jak ktos wykasuje wpis w combo
  ref = 0;
  lp = 0;
  if(:zakres is null) then zakres = 0;
  if(:zpopmies is null) then zpopmies = 0;
  --BS08473
  sql = 'select B.REF, b.transdate, b.docdate, B.vatreg, b.vnumber, b.bkreg, b.number, B.NIP, b.contractor, b.symbol, v.vatregtype'; --<<PR73894: PL>>
  sql = sql ||' from bkdocs B join VATREGS V on (B.vatreg = V.symbol and B.company = V.company) join BKDOCTYPES T on (B.doctype = t.ref)';
  sql = sql ||' where B.vatperiod = '''||:periodid||''' and b.status > 0';--and v.vtype in (0,1,2,5,6)';
  --<<PR73894: PL
  sql = sql ||' and (V.VATREGTYPE in ('||:zakup||',2)
                  or (V.VATREGTYPE = 3
                    and exists(select first 1 1 from bkvatpos join grvat on (grvat.symbol = bkvatpos.taxgr) where bkvatpos.bkdoc = B.ref and grvat.vatregtype in ('||:zakup||',2))))';
  if(coalesce(typ,0)>0) then
  begin
    if(coalesce(zakup,0)>0)then
    begin
      sql = sql ||' and v.vtype = '||
      case typ
        when 1 then '3' --Zakupy krajowe
        when 2 then '4' --Dostawy importowe
        when 3 then '5' --WNT
        when 4 then '6' --Usługi z importu
      end;
    end else
    begin
      sql = sql ||' and v.vtype = '||
      case typ
        when 1 then '0' --Sprzedaż krajowa
        when 2 then '1' --WDT
        when 3 then '2' --Sprzedaż eksportowa
        when 4 then '5' --WNT
        when 5 then '6' --Usugi z importu
      end;
    end
  end
  /*if(coalesce(zakup,0)>0)then
  begin
    sql = sql ||' and v.vtype in (3,4,5,6)';
    if(coalesce(typ,0)>0) then sql = sql ||' and v.vtype = ('||:typ||' + 2)';
  end
  else
  begin
    sql = sql ||' and v.vtype in (0,1,2,5,6)';
    if(coalesce(typ,0)>0) then sql = sql ||' and (v.vtype = ('||:typ||' - 1) or ('||:typ||' in (4,5) and v.vtype = ('||:typ||' + 1)))';   -- XXX Lambda
  end */
-->>
  if(coalesce(zakres,0)=1)then sql = sql ||' and t.creditnote = 0';
  else if(coalesce(zakres,0)=2)then sql = sql ||' and t.creditnote = 1';
  if(coalesce(zpopmies,0)>0)then sql = sql ||' and (b.vatperiod <> b.period)';
  if(coalesce(vreg,'')<>'')then sql = sql ||' and b.vatreg = '''||:vreg||'''';
  sql = sql ||' and B.company ='||:company||' order by B.vatreg, b.vnumber';
  for execute statement :sql
  /*
  for
    select B.REF, b.transdate, b.docdate, B.vatreg, b.vnumber, b.bkreg, b.number,
              B.NIP, b.contractor, b.symbol
      from bkdocs B
        join VATREGS V on (B.vatreg = V.symbol and B.company = V.company)
        join BKDOCTYPES T on (B.doctype = t.ref)
      where
        B.vatperiod = :periodid and b.status > 1 and
        (
          (v.vtype in (0,1,2,5,6) -- rejestry sprzedazy (nalezny)
          and (:typ = 0 or v.vtype = (:typ - 1)) -- wszystkie albo wybrany rejestr
          and :zakup = 0
          ) or
          (v.vtype in (3,4,5,6) -- rejestry zakupu (naliczony)
          and (:typ = 0 or v.vtype = (:typ + 2)) -- wszystkie albo wybrany rejestr
          and :zakup = 1
          )
        )
        and ((:zakres = 0) or (:zakres = 1 and t.creditnote = 0) or (:zakres = 2 and t.creditnote = 1))
        and ((:zpopmies = 0) or (b.vatperiod <> b.period))
        and ((:vreg is null) or (:vreg = '') or (b.vatreg = :vreg))
        and B.company = :company
      order by B.vatreg, b.vnumber */
      into :docref, :transdate, :docdate, :vatreg, :vatregnr,:bkreg, :bkregnr,
         :nip, :kontrahkod, :docsymbol, :vatregtype   --<<PR73894: PL>>
  do begin
    wartbru = 0;
    wartnet = 0;
    wartvat = 0;
    WART22 = 0;
    VAT22 = 0;
    WART23 = 0;
    VAT23 = 0;
    WART12 = 0;
    VAT12 = 0;
    WART07 = 0;
    VAT07 = 0;
    WART08 = 0;
    VAT08 = 0;
    WART03 = 0;
    VAT03 = 0;


    WART05 = 0;
    VAT05 = 0;
    WART00 = 0;
    VAT00 = 0;
    WARTZW = 0;
    VATZW = 0;
    WARTPOZ = 0;
    VATPOZ = 0;
    print = 0;
    for select VATGR, sum(NETV), sum(VATV)
      from BKVATPOS
        join GRVAT on (GRVAT.SYMBOL = BKVATPOS.TAXGR)--<<PR73894: PL>>
      where bkdoc = :docref
        --and (debittype = :vatdebittype or :vatdebittype = 4) ?? Nie bylo takiego warunku wczesniej?? Brak parametru w ogole...
        and (:vatregtype in (:zakup,2) or (:vatregtype = 3 and (grvat.vatregtype in (:zakup,2))))--<<PR73894: PL>>
      group by VATGR
      into :vatgr, :netv, :vatv
    do begin
      print = 1;
      if(:vatgr = '22') then begin
        WART22 = :wart22 + :netv;
        VAT22 = :vat22 + :vatv;
      end else if(:vatgr = '23') then begin
        WART23 = :wart23 + :netv;
        VAT23 = :vat23 + :vatv;
      end else if(:vatgr = '12') then begin
        WART12 = :wart12 + :netv;
        VAT12 = :vat12 + :vatv;
      end else if(:vatgr = '07') then begin
        WART07 = :wart07 + :netv;
        VAT07 = :vat07 + :vatv;
      end else if(:vatgr = '08') then begin
        WART08 = :wart08 + :netv;
        VAT08 = :vat08 + :vatv;
      end else if(:vatgr = '03') then begin
        WART03 = :wart03 + :netv;
        VAT03 = :vat03 + :vatv;
      end else if(:vatgr = '05') then begin
        WART05 = :wart05 + :netv;
        VAT05 = :vat05 + :vatv;
      end else if(:vatgr = '00') then begin
        WART00 = :wart00 + :netv;
        VAT00 = :vat00 + :vatv;
      end else if(:vatgr = 'ZW') then begin
        WARTZW = :wartZW + :netv;
        VATZW = :vatZW + :vatv;



      end else begin
        WARTPOZ = :wartPOZ + :netv;
        VATPOZ = :vatPOZ + vatv;
      end
    end
    wartvat = :vat22 + :vat23 + :vat12 + :vat07 + :vat08 + :vat03 + :vat05 + :vatpoz;
    wartbru = :wartvat + :wart22 + :wart23 + :wart12 + :wart07 + :wart08 + :wart03 + :wart05 + :wart00 + :wartZW + :wartpoz;
    wartnet = :wart22 + :wart23 + :wart12 + :wart07 + :wart08  + :wart03 + :wart05 + :wart00 + :wartZW + :wartpoz;
    lp = :lp + 1;
    ref = :ref + 1;
    vat22 = :vat22 + :vat23;
    vat07 = :vat07 + vat08;
    vat03 = :vat03 + :vat05;
    wart22 = :wart22 + :wart23;
    wart07 = :wart07 + :wart08;
    wart03 = :wart03 + :wart05;
    if(print = 1) then
      suspend;
  end
end^
SET TERM ; ^
