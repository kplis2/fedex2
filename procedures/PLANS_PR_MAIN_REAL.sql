--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PLANS_PR_MAIN_REAL(
      PLANS integer,
      PLANSCOL integer,
      PLANSROW integer,
      PRSCHEDULE integer)
  returns (
      VAL numeric(14,4))
   as
DECLARE VARIABLE KTM VARCHAR(20);
declare variable stat varchar(1024);
declare variable bdata timestamp;
declare variable ldata timestamp;
begin
  val = -1;
  select pr.key1, pc.bdata,  pc.ldata from plansrow pr
    join plans p on (p.ref = pr.plans)
    join planscol pc on (p.ref = pc.plans)
    where pr.ref = :PLANSROW and p.ref = :plans and pc.ref = :PLANSCOL
  into :ktm, :bdata, :ldata;
  stat = 'select sum(nz.kilosc) from nagzam nz join prschedules ps on (ps.ref = nz.prschedule) ';
  stat = stat || ' join plans pl on (pl.ref = ps.plans) where pl.ref = '||:plans|| 'and nz.kktm = '''||:ktm||'''';
--  stat = stat || ' and nz.datawe >='''||:bdata||''' and nz.datawe <='''||:ldata||'''';
  stat = stat || ' and ps.ref = '||:prschedule || ' and nz.planscol = '||:planscol;
  execute statement :stat into :val;
  if (:val is null) then val = 0;
  suspend;
end^
SET TERM ; ^
