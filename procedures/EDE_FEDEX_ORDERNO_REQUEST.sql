--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_ORDERNO_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable transportorder integer;
declare variable transportordersymb varchar(20);
declare variable shippingdocs blob_utf8;
declare variable grandparent integer;
declare variable shipper spedytor_id;
declare variable accesscode string80;
begin
  select l.ref, l.symbol, l.spedytor
    from listywys l
    where l.ref = :oref
  into :transportorder, :transportordersymb, :shipper;

  --sprawdzanie czy istieje dokument spedycyjny
  if(:transportorder is null) then exception ede_fedex 'Brak listy spedycyjnej.';

  val = null;
  parent = null;
  params = null;
  id = 0;
  name = 'pobierzNumerDokumentuList';
  suspend;

  grandparent = :id;

  --dane autoryzacyjne
  select k.klucz
    from spedytkonta k
    where k.spedytor = :shipper
  into :accesscode;

  val = :accesscode;
  name = 'kodDostepu';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = 'PRODUCTION'; --jesli chcesz testowac zmien na TEST
  name = 'serviceUrlType';
  id = id + 1;
  parent = GrandParent;
  suspend;

  select first 1 substr(d.symbolsped,1,20)
    from listywysd d
    where d.listawys = :transportorder
  into :shippingdocs;

  val = :transportorder;
  name = 'zlecTransportowe';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = :transportordersymb;
  name = 'zlecTransportoweSymb';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = :shippingdocs;
  name = 'numerListu';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;
end^
SET TERM ; ^
