--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BROWSE_BUDGETPLAN(
      COMPANY integer,
      BUDGPLANSYMB varchar(20) CHARACTER SET UTF8                           ,
      FROMPERIOD varchar(6) CHARACTER SET UTF8                           ,
      TOPERIOD varchar(6) CHARACTER SET UTF8                           )
  returns (
      RRDEPART varchar(3) CHARACTER SET UTF8                           ,
      RDESC varchar(255) CHARACTER SET UTF8                           ,
      RAMOUNTBO numeric(14,2),
      RAMOUNT numeric(14,2),
      RAMOUNTEND numeric(14,2),
      RDECREES varchar(40) CHARACTER SET UTF8                           ,
      RDECISIONDATE timestamp,
      RTURNOWERS numeric(14,2),
      RCHAPTER varchar(20) CHARACTER SET UTF8                           ,
      RREFCHAPTER integer,
      RPARAGRAPH varchar(20) CHARACTER SET UTF8                           ,
      RREFPARAGRAPH integer,
      RAKCEPTACJA smallint)
   as
begin
  if (FROMPERIOD = '') then FROMPERIOD = null;
  if (TOPERIOD = '') then TOPERIOD = null;
  for select substring(max(sp.kod) from 1 for 3),
  --bp.chapter, bp.paragraph,
  max(sp.kod), max(sp.ref) ,max(sp1.kod), max(sp1.ref),  --narazie stringi
  max(bp.decript),
  sum(case when bp.BO = 1 then bp.amount else 0 end ), sum(bp.amount), max(bp.decrees), max(bp.decisiondate)
 --, max(sp.kod),max(sp1.kod)
  , max(b.acc)
    from BUDGPLANPOS BP
      left join budgplan b on (bp.budgplan = b.symbol)
      left join slopoz sp on (sp.ref=bp.chapter)
      left join slopoz sp1 on (sp1.ref=bp.paragraph)
    where bp.budgplan = :budgplansymb
      and (((select OKRES from datatookres(cast(cast(BP.decisiondate as date) as varchar(10)), 0,0,0,1)) >= :FROMPERIOD)
        or (:FROMPERIOD is null))
      and (((select OKRES from datatookres(cast(cast(BP.decisiondate as date) as varchar(10)), 0,0,0,1)) <= :TOPERIOD)
        or (:TOPERIOD is null) )
    group by BP.budgplan, bp.chapter, bp.paragraph
    into :RRDEPART, :rchapter, :rrefchapter, :rparagraph, :rrefparagraph, :rdesc, RAMOUNTBO, :ramountend, :rdecrees, :rdecisiondate, :rakceptacja
    --,:rchapterdict,:rparagraphdict
     do
    begin

      /* Procedure Text */
      suspend;
    end
end^
SET TERM ; ^
