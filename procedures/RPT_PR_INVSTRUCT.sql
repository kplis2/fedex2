--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PR_INVSTRUCT(
      DATA1 timestamp,
      DATA2 timestamp,
      COMPANY integer,
      TAKEABS smallint = 1)
  returns (
      INV0 integer,
      INV1 integer,
      INV2 integer,
      INV3 integer,
      OGOL_PRAC integer,
      OGOL integer,
      NDATA1 timestamp,
      INV4 numeric(14,4),
      INV5 numeric(14,4),
      INV6 numeric(14,4),
      INV7 numeric(14,4),
      OGOLEM2 numeric(14,4),
      OGOLEM_PRAC2 numeric(14,4),
      OGOLEM3 numeric(14,4))
   as
declare variable CODE integer;
declare variable INVCOUNT integer;
begin
/*MWr Personel - Procedura generuje dane do zestawienia: struktura zatrudnienia
  wedlug niepelnosprawnosci oraz do zestawien PFRON. Parametr TAKEABS=1 oznacza,
  ze bedziemy uwzgledniac(tj. wykluczac) osoby przebywajace na nieobecnosciach
  wymienionych w artykule nr 21 ust.5 ustawy o rehabilitacji (BS43887). */

  takeabs = coalesce(takeabs,0);
  ndata1 = data1;
  while (data2 >= ndata1)
  do begin
    inv0 = null;
    inv1 = null;
    inv2 = null;
    inv3 = null;
    inv4 = null;
    inv5 = null;
    inv6 = null;
    inv7 = null;
    ogolem2 = null;
    ogol_prac = null;

  --zatrudnienie w osobach -----------------------------------------------------
    select count(distinct em.person)
      from ezusdata e
        join employees em on (e.person = em.person and em.company = :company)
        join employment ep on (em.ref = ep.employee)
        left join eabsences a on (:takeabs = 1
                                and a.employee = em.ref and a.correction in (0,2)
                                and a.fromdate <= :ndata1 and a.todate >= :ndata1
                                and a.ecolumn in (140, 260, 300, 330, 350, 360, 370, 450))
      where ep.fromdate <= :ndata1 and (ep.todate >= :ndata1 or ep.todate is null)
        and e.fromdate = (select max(e2.fromdate) from ezusdata e2
                            where e2.person = em.person and e2.fromdate <= :ndata1)
        and a.ref is null
      into :ogol_prac;  --wszystkie osoby

    for
      select ez.code, count(distinct em.person)
        from ezusdata e
          join edictzuscodes ez on (ez.ref = e.invalidity and ez.dicttype = 2 and ez.code > 0)
          join employees em on (e.person = em.person and em.company = :company)
          join employment ep on (em.ref = ep.employee)
          left join eabsences a on (:takeabs = 1
                                and a.employee = em.ref and a.correction in (0,2)
                                and a.fromdate <= :ndata1 and a.todate >= :ndata1
                                and a.ecolumn = 300)
        where ep.fromdate <= :ndata1 and (ep.todate >= :ndata1 or ep.todate is null)
          and e.invalidityfrom <= :ndata1 and (e.invalidityto >= :ndata1 or e.invalidityto is null)
          and e.fromdate = (select max(e2.fromdate) from ezusdata e2
                              where e2.person = em.person and e2.fromdate <= :ndata1)
          and a.ref is null
        group by ez.code
        into :code, :invcount
    do begin
      if (code = 1) then
        inv0 = invcount; --osoby, ktore maja orzeczenie o lekkim stopniu niepelnosprawnosci
      else if (code = 2) then
        inv1 = invcount; --osoby, ktore maja orzeczenie o umiarkowaym stopniu niepelnosprawnosci
      else if (code = 3) then
        inv2 = invcount; --osoby, ktore maja orzeczenie o znacznym stopniu niepelnosprawnosci
      else if (code = 4) then
        inv3 = invcount; --osoby, ktore maja orzeczenie o niepelnosprawnosci wydane osobom do 16 roku zycia
    end

  --wymiar czasu pracy w przeliczeniu na etaty ---------------------------------
    select sum(case when coalesce(a.ecolumn,0) in (0,300) then --przy osobach pelnosprawnych nie wliczamy osob na url.wychowaczym, sluz.wojskowej, swiad.rehab. (BS43887)
               case when ez.code = 0 then ep.workdim else  -- code = 0 - osoby ktore nie posiadaja orzeczenia o niepelnosprawnosci
               case when not (e.invalidityfrom <= :ndata1 and (e.invalidityto >= :ndata1 or e.invalidityto is null)) then ep.workdim else 0 end end else 0 end),
           sum(case when ez.code = 1 and e.invalidityfrom <= :ndata1 and (e.invalidityto >= :ndata1 or e.invalidityto is null) then ep.workdim else 0 end),
           sum(case when ez.code = 2 and e.invalidityfrom <= :ndata1 and (e.invalidityto >= :ndata1 or e.invalidityto is null) then ep.workdim else 0 end),
           sum(case when ez.code = 3 and e.invalidityfrom <= :ndata1 and (e.invalidityto >= :ndata1 or e.invalidityto is null) then ep.workdim else 0 end),
           sum(case when ez.code = 4 and e.invalidityfrom <= :ndata1 and (e.invalidityto >= :ndata1 or e.invalidityto is null) then ep.workdim else 0 end)
      from ezusdata e
        join edictzuscodes ez on (ez.ref = e.invalidity and ez.dicttype = 2)
        join employees em on (e.person = em.person and em.company = :company)
        join employment ep on (em.ref = ep.employee)
        left join eabsences a on (:takeabs = 1
                                and a.employee = em.ref and a.correction in (0,2)
                                and a.fromdate <= :ndata1 and a.todate >= :ndata1
                                and a.ecolumn in (140, 260, 300, 330, 350, 360, 370, 450))
      where ep.fromdate <= :ndata1 and (ep.todate >= :ndata1 or ep.todate is null)
        and e.fromdate = (select max(e2.fromdate) from ezusdata e2
                            where e2.person = em.person and e2.fromdate <= :ndata1)
        and (ez.code = 0
          or (ez.code > 0 and coalesce(a.ecolumn,0) <> 300)) --przy niepelnosprawnych nie wliczamy osob na bezplatnym (BS43887)
      into :ogolem2, :inv4, :inv5, :inv6, :inv7;

    inv0 = coalesce(inv0, 0);
    inv1 = coalesce(inv1, 0);
    inv2 = coalesce(inv2, 0);
    inv3 = coalesce(inv3, 0);

    inv4 = coalesce(inv4, 0);
    inv5 = coalesce(inv5, 0);
    inv6 = coalesce(inv6, 0);
    inv7 = coalesce(inv7, 0);

    ogol_prac = coalesce(ogol_prac, 0); --zatrudnienie ogolem wszystkich osob
    ogol = inv0 + inv1 + inv2 + inv3;   --zatrud. ogolem niepelnosprawnych w osobach
    ogolem_prac2 = inv4 + inv5 + inv6 + inv7; --niepelnosprawni ogolem w etatach
    ogolem2 = coalesce(ogolem2, 0);
    ogolem3 = ogolem2 + ogolem_prac2;

    suspend;
    ndata1 = ndata1 + 1;
  end
end^
SET TERM ; ^
