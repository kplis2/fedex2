--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_ZLOGI_B(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      DATASTAN timestamp,
      ILDNI integer)
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      CENA numeric(14,4),
      ZLOGI numeric(14,4),
      ZLOGIWART numeric(14,2))
   as
declare variable stanp numeric(14,2);
declare variable stank numeric(14,2);
begin

  for select  k1.RKTM, k1.RWERSJA, sum(k1.STAN), sum(k2.STAN), sum(k2.wartosc)
    from MAG_STANY(:MAGAZYN, null,null,null,null, :DATASTAN-:ILDNI, 1,0) K1
    join MAG_STANY(:MAGAZYN, null,null ,null,null, :DATASTAN, 1,0) K2 on (k1.rktm = k2.rktm and k1.rwersja = k2.rwersja)
  group by k1.rktm, k1.rwersja
  into :ktm, :wersja, :stanp, :stank, :zlogiwart
  do begin
    if(:stanp > 0) then begin
      if ((not exists (select first 1 P.ktm from dokumpoz P
                         left join dokumnag N on (P.dokument=N.ref)
                         left join defdokum D on (n.typ=D.symbol)
                       where :ktm=P.ktm and p.wersja = :wersja and n.data >(:datastan - :ildni) and n.data <:datastan
                         and d.wydania=1 and d.zewn = 1 and n.akcept=1 and n.magazyn = :magazyn)))
      then begin

        zlogi = :stank;
        cena = :zlogiwart / :zlogi ;

        suspend;
      end
    end
  end
end^
SET TERM ; ^
