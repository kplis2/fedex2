--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_CREATEZAMDOST(
      ZAM integer,
      HISTZAM integer,
      DEFOPER varchar(5) CHARACTER SET UTF8                           ,
      DOSTAWCA integer,
      GRUPAZAM integer)
  returns (
      DOSTZAM integer,
      SYMBOLZAM varchar(20) CHARACTER SET UTF8                           )
   as
declare variable CENANET numeric(14,4);
declare variable JEDN integer;
declare variable KTM varchar(30);
declare variable PRZELICZ numeric(14,4);
declare variable POZZAMDREF integer;
declare variable OPERATOR integer;
declare variable WERSJA integer;
declare variable ODDZIAL varchar(20);
declare variable WALUTA varchar(10);
declare variable WERSJAREF integer;
declare variable CJEDN integer;
declare variable PREC smallint;
declare variable WALCEN varchar(3);
begin
  dostzam = null;
  if(:grupazam>0) then begin
    select max(ref) from NAGZAM where TYP=0 and DOSTAWCA=:dostawca and GRUPA=:grupazam
    into :dostzam;
    if(:dostzam=0) then dostzam=null;
  end
  /*zalozenie zamówienia*/
  if(:dostzam is null) then begin
    execute procedure GEN_REF('NAGZAM') returning_values :dostzam;
    execute procedure get_global_param('AKTUOPERATOR') returning_values :operator;
    insert into NAGZAM(REF, TYPZAM, REJESTR, MAGAZYN, STAN, TYP, DOSTAWCA, REFZRODL, symbzrodl,
          TERMDOST, OPERATOR, UWAGI,
          DATAWE,GRUPA,
          DULICA, DNRDOMU, DNRLOKALU, DMIASTO, DKODP, odbiorca)
    select :dostzam, DEFOPERZAM.ddnewtyp, DEFOPERZAM.ddnewrej, NAGZAM.MAGAZYN, 'N', 0, :dostawca, NAGZAM.REF, NAGZAM.id,
          TERMDOST, :operator, UWAGI,
          current_timestamp(0),:grupazam,
          defmagaz.dulica, defmagaz.dnrdomu, defmagaz.dnrlokalu, defmagaz.dmiasto, defmagaz.dkodp, defmagaz.opis
    from NAGZAM, DEFOPERZAM, defmagaz
    where NAGZAM.REF = :zam and DEFOPERZAM.symbol = :defoper and nagzam.magazyn = defmagaz.symbol;
  end
  select oddzial,waluta from NAGZAM where ref=:dostzam into :oddzial,:waluta;
  /*zaożnie pozycji zamówienia*/
  for select POZZAMDYSP.REF, POZZAM.JEDN, POZZAM.KTM, POZZAM.WERSJA
  from POZZAM join POZZAMDYSP on (POZZAMDYSP.pozzam = POZZAM.REF)
  where POZZAM.zamowienie = :zam and POZZAMDYSP.dostawca = :dostawca
  order by POZZAM.NUMER
  into :pozzamdref, :jedn, :ktm, :wersja
  do begin
    /*obliczanie ceny*/
    cenanet = null;
    walcen = NULL;
    prec = 0;
    select ref from WERSJE where KTM=:ktm and NRWERSJI=:wersja into :wersjaref;
    select CENANET,WALUTA,JEDN,PREC from GET_DOSTCEN(:wersjaref,:dostawca,:waluta,:oddzial,0) into :cenanet,:walcen,:cjedn,:prec;
    if(:cenanet is null) then begin
      select ref from WERSJE where KTM=:ktm and NRWERSJI=0 into :wersjaref;
      select CENANET,WALUTA,JEDN,PREC from GET_DOSTCEN(:wersjaref,:dostawca,:waluta,:oddzial,0) into :cenanet,:walcen,:cjedn,:prec;
    end
    if(:cenanet is not null and :jedn<>:cjedn) then cenanet = null;
    if(:cenanet is null) then begin
      select TOWARY.cena_zakn, TOWARY.DM from TOWARY where KTM = :ktm into :cenanet,:cjedn;
      przelicz = null;
      select przelicz from TOWJEDN where ref = :jedn into :przelicz;
      if(:przelicz is null or :przelicz = 0) then przelicz = 1;
      cenanet = :cenanet * :przelicz;
      execute procedure GETCONFIG('WALPLN') returning_values :walcen;
      if(:walcen is null) then walcen = 'PLN';
    end
    insert into POZZAM(ZAMOWIENIE, KTM, WERSJAREF, MAGAZYN, JEDNO, JEDN, ILOSCO, ILOSC, ILOSCM, GENPOZREF, CENACEN, WALCEN, CENAMAG, DOSTAWAMAG, PREC)
    select :dostzam,  POZZAM.KTM, POZZAM.wersjaref, POZZAM.magazyn, POZZAM.jedno, POZZAM.JEDN, POZzamdysp.ilosco, pozzamdysp.ilosc, pozzamdysp.iloscm, POZZAM.REF, :cenanet, :waluta, 
      (case when pozzam.hasownsupply=1 then pozzam.CENAMAG else NULL end),
      (case when pozzam.hasownsupply=1 then pozzam.DOSTAWAMAG else NULL end),
      :prec
      from POZZAMDYSP join POZZAM on (POZZAMDYSP.pozzam = POZZAM.REF)
      where POZZAMDYSP.REF = :pozzamdref;

  end
end^
SET TERM ; ^
