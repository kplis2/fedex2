--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_ROZRACH(
      DATA timestamp,
      ACCOUNT ACCOUNT_ID,
      COMPANY integer,
      WALUTOWY smallint)
  returns (
      SYMBFAK varchar(20) CHARACTER SET UTF8                           ,
      SLODEF integer,
      SLOPOZ integer,
      KONTOFK KONTO_ID,
      DATAPLAT timestamp,
      DATAOTW timestamp,
      OPIS varchar(255) CHARACTER SET UTF8                           ,
      WALUTA varchar(3) CHARACTER SET UTF8                           ,
      WINIEN numeric(14,4),
      MA numeric(14,4),
      WINIENPLN numeric(14,4),
      MAPLN numeric(14,4),
      SALDO_WINIEN numeric(14,4),
      SALDO_MA numeric(14,4))
   as
begin
  for select R.symbfak, R.slodef, R.slopoz, R.kontofk, R.dataplat, R.dataotw, R.opis, R.waluta, sum(rp.winien), sum(rp.ma), sum(rp.winienzl), sum(rp.mazl)
    from ROZRACH R
    left join rozrachp rp on (r.symbfak = rp.symbfak and r.slodef = rp.slodef and r.slopoz = rp.slopoz and r.kontofk = rp.kontofk and r.company = rp.company)
    where R.kontofk = :account and R.company = :company and R.walutowy = :walutowy and R.dataotw <= :data
      and rp.data <=:data
    group by R.symbfak, R.slodef, R.slopoz, R.kontofk, R.dataplat, R.dataotw, R.opis, R.waluta
    into :symbfak, :slodef, :slopoz, :kontofk, :dataplat, :dataotw, :opis, :waluta, :winien, :ma, :winienpln, :mapln
  do begin
    saldo_winien = :winien - :ma;
    saldo_ma = :ma - :winien;
    if(saldo_winien < 0) then
      saldo_winien = 0;
    if(saldo_ma < 0) then
      saldo_ma = 0;
    suspend;
  end
end^
SET TERM ; ^
