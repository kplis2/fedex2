--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHOPERS_COPY(
      IPRSHEET integer,
      INEWPRSHEET integer,
      IPRSHOPER integer,
      IPRSHMAT integer,
      ITOOLS smallint,
      IMEASURE smallint,
      IDEPOPERS smallint,
      IOPERMASTER integer = null)
   as
declare variable vprshmat integer;
declare variable vnewprshoper integer;
declare variable vnewprshmat integer;
declare variable vdepoperref integer;
declare variable vtoolref integer;
declare variable vnewtoolref integer;
begin
  if (:iopermaster = 0) then iopermaster = null;
  execute procedure GEN_REF('PRSHOPERS') returning_values :vnewprshoper;
  INSERT INTO PRSHOPERS (REF, SHEET, OPERMASTER, DESCRIPT, ECONDITION,
    CONDITION, OPER, EOPERFACTOR, OPERFACTOR, EOPERTIME, OPERTIME, WORKPLACE,
    EPLACETIME, PLACETIME, COMPLEX, ORD, OPERTYPE, SCHEDULED, CALCULATED,
    AMOUNTRESULTCALCTYPE, REPORTED, ORDANALIZE, HOURRATE, ECONTRACTSDEF, NOTE,
    PRQCONTROLMETH, PRQCONTROLINTERVAL, REPORTEDFACTOR, PARAMN1, PARAMN2,
    PARAMN3, PARAMN4, PARAMN5, PARAMS1, PARAMS2, PARAMS3, PARAMS4, PARAMS5,
    PARAMSI1, PARAMSI2, PARAMSI3, PARAMSI4, PARAMSI5, REPORTEDFACTORTYPE,
    OPERDEFAULT, NORMPROCEDURE)
    select :vnewprshoper, :inewprsheet, :iopermaster,
      DESCRIPT, ECONDITION, CONDITION, OPER, EOPERFACTOR, OPERFACTOR, EOPERTIME,
      OPERTIME, WORKPLACE, EPLACETIME, PLACETIME, COMPLEX, ORD, OPERTYPE, SCHEDULED,
      CALCULATED, AMOUNTRESULTCALCTYPE, REPORTED, ORDANALIZE, HOURRATE,
      ECONTRACTSDEF, NOTE, PRQCONTROLMETH, PRQCONTROLINTERVAL, REPORTEDFACTOR,
      PARAMN1, PARAMN2, PARAMN3, PARAMN4, PARAMN5, PARAMS1, PARAMS2, PARAMS3,
      PARAMS4, PARAMS5, PARAMSI1, PARAMSI2, PARAMSI3, PARAMSI4, PARAMSI5,
      REPORTEDFACTORTYPE, OPERDEFAULT, NORMPROCEDURE
      from prshopers
      where ref = :iprshoper;
  if (coalesce(:iprshmat,0) > 0) then begin -- surowce                                                                          end
    for
      select m.ref
        from prshmat m
        where m.opermaster = :iprshoper
        into :vprshmat
    do begin
      execute procedure GEN_REF('PRSHMAT') returning_values :vnewprshmat;
      INSERT INTO PRSHMAT (REF, SHEET, OPERMASTER, NUMBER, DESCRIPT, MATTYPE,
        KTM, WERSJA, WERSJAREF, JEDN, ENETQUANTITY, NETQUANTITY, EGROSSQUANTITY,
        GROSSQUANTITY, ECONDITION, CONDITION, SUBSHEET, ORD, SUPPLIER,
        WAREHOUSE, REQKTMTAG)
        select :vnewprshmat, :inewprsheet, :vnewprshoper, NUMBER, DESCRIPT,
          MATTYPE, KTM, WERSJA, WERSJAREF, JEDN, ENETQUANTITY, NETQUANTITY,
          EGROSSQUANTITY, GROSSQUANTITY, ECONDITION, CONDITION, SUBSHEET, ORD,
          SUPPLIER, WAREHOUSE, REQKTMTAG
          from prshmat
          where ref = :vprshmat;
      if (coalesce(:iprshmat,0) = 2) then begin                             -- zamienniki
        INSERT INTO PRSHMATSUBSTITUTE (PRSHMAT, SUBSTITUTE, DESCRIPT, JEDN)
          select :vnewprshmat, SUBSTITUTE, DESCRIPT, JEDN
            from prshmatsubstitute
            where prshmat = :vprshmat;
      end
    end
  end
  if (coalesce(:itools,0) > 0) then begin                                           -- narzedzia
    execute procedure prshtools_copy(null, :vnewprshoper, :inewprsheet, :iprshoper, null);
  end
  if (coalesce(:imeasure,0) > 0) then begin                                      -- czynnosci pomiarowe
    INSERT INTO PRSHMEASURETOOLS (PRSHOPER, PRTOOLTYPE, AMOUNT)                  -- + narzedzia pomiarowe
      select :vnewprshoper, PRTOOLTYPE, AMOUNT
        from PRSHMEASURETOOLS 
        where PRSHOPER = :iprshoper;
    INSERT INTO PRSHMEASUREACTS (PRSHOPER, ACTIVITY, NUMBER, ORD,
      DESCRIPT, DEFAULTVALUE, MEASURETOOLS) 
      select :vnewprshoper, ACTIVITY, NUMBER, ORD, DESCRIPT, DEFAULTVALUE, MEASURETOOLS
        from PRSHMEASUREACTS 
        where PRSHOPER = :iprshoper;
  end
  if (coalesce(:idepopers,0) > 0) then begin                                     -- operacje podrzedne (zalezne)
    for
      select o.ref
        from prshopers o
        where o.opermaster = :iprshoper
        order by o.number
        into :vdepoperref
    do begin
      execute procedure prshopers_copy(:iprsheet, :inewprsheet, :vdepoperref,
        :iprshmat, :itools, :imeasure, :idepopers, :vnewprshoper);
    end
  end
end^
SET TERM ; ^
