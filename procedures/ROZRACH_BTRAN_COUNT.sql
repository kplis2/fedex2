--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ROZRACH_BTRAN_COUNT(
      SLODEF integer,
      SLOPOZ integer,
      ACCOUNT ACCOUNT_ID,
      SETTLEMENT varchar(50) CHARACTER SET UTF8                           ,
      COMPANY COMPANIES_ID = null)
   as
declare variable BTRANAMOUNT numeric(14,2);
declare variable BTRANAMOUNTAKC numeric(14,2);
declare variable BTRANAMOUNTBANK numeric(14,2);
begin
  select sum(BTRANSFERPOS.amount)
    from BTRANSFERPOS
      join BTRANSFERS on (BTRANSFERS.ref = BTRANSFERPOS.btransfer)
    where SLODEF = :slodef
      and SLOPOZ = :slopoz
      and ACCOUNT = :account
      and SETTLEMENT = :settlement
      and BTRANSFERS.COMPANY = :company
    into :btranamount;
  select sum(BTRANSFERPOS.amount)
    from BTRANSFERPOS
      join BTRANSFERS on (BTRANSFERS.ref = BTRANSFERPOS.btransfer)
    where SLODEF = :slodef
      and SLOPOZ = :slopoz
      and ACCOUNT = :account
      and SETTLEMENT = :settlement
      and BTRANSFERS.status > 0
      and BTRANSFERS.COMPANY = :company
    into :btranamountakc;
  if(:btranamount is null) then btranamount = 0;
  if(:btranamountakc is null) then btranamountakc = 0;

  select sum(BTRANSFERPOS.amount)
    from BTRANSFERPOS
      join BTRANSFERS on (BTRANSFERS.ref = BTRANSFERPOS.btransfer)
    where SLODEF = :slodef
      and SLOPOZ = :slopoz
      and ACCOUNT = :account
      and SETTLEMENT = :settlement
      and BTRANSFERS.status < 4
      and BTRANSFERS.COMPANY = :company
  into :btranamountbank;
  if (:btranamountbank is null) then btranamountbank = 0;

  update ROZRACH
    set ROZRACH.btranamount = :btranamount,
        BTRANAMOUNTAKC = :btranamountakc,
        BTRANAMOUNTBANK = :btranamountbank
    where SLODEF = :slodef
      and :slopoz = :slopoz
      and SYMBFAK = :settlement
      and KONTOFK = :account;
end^
SET TERM ; ^
