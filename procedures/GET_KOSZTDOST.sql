--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_KOSZTDOST(
      SPOSD integer,
      SPOSZ integer,
      WAGA numeric(14,2),
      OBJETOSC numeric(14,4),
      SUMWARTNET numeric(14,2),
      SUMWARTBRU numeric(14,2),
      KODP varchar(10) CHARACTER SET UTF8                           ,
      FLAGI varchar(255) CHARACTER SET UTF8                           ,
      OPTY integer,
      SPEDYTOR varchar(80) CHARACTER SET UTF8                           ,
      DATAWYS date,
      ZWROTPAL integer,
      NILPAL integer,
      NILKART integer,
      PARAMS varchar(255) CHARACTER SET UTF8                            = '')
  returns (
      KOSZT numeric(14,2),
      OPTYSPOS integer,
      KODS varchar(10) CHARACTER SET UTF8                           ,
      ILOSCPAL integer,
      ILOSCKART integer,
      K1 numeric(14,4),
      K2 numeric(14,4),
      K3 numeric(14,4),
      K4 numeric(14,4),
      K5 numeric(14,4),
      K6 numeric(14,4),
      K7 numeric(14,4),
      K8 numeric(14,4))
   as
declare variable PROC varchar(60);
declare variable PROG numeric(14,2);
declare variable PROCED varchar(255);
declare variable GRUPA integer;
declare variable KOSZTMIN numeric(14,2);
declare variable KOSZTP numeric(14,2);
declare variable OPTYSPOSMIN integer;
declare variable KODSMIN varchar(10);
declare variable K1MIN numeric(14,4);
declare variable K2MIN numeric(14,4);
declare variable K3MIN numeric(14,4);
declare variable K4MIN numeric(14,4);
declare variable K5MIN numeric(14,4);
declare variable K6MIN numeric(14,4);
declare variable K7MIN numeric(14,4);
declare variable K8MIN numeric(14,4);
declare variable NUMPAR integer;
declare variable PARAMFIELDS varchar(255);
declare variable I integer;
declare variable REFSPOSDOST integer;
declare variable REFSPOSDOSTCENNIK integer;
declare variable ODLEGLOSC integer;
declare variable KOSZTDOD numeric(14,2);
declare variable PROCENTDOD numeric(14,2);
declare variable KWOTADOD numeric(14,2);
declare variable ILOSCPALMIN integer;
declare variable ILOSCKARTMIN integer;
begin
  if (:SPEDYTOR is null) then SPEDYTOR = '';
  if (:objetosc is null) then objetosc = 0;
  if (:datawys is null) then datawys = current_date;
  if (zwrotpal is null) then zwrotpal = 0;
  if (:sposd is null) then sposd = 0;
  if (:sposz is null) then sposz = 0;
  if (:waga is null) then waga = 0;
  if (:SUMWARTNET is null) then SUMWARTNET = 0;
  if (:SUMWARTBRU is null) then SUMWARTBRU = 0;
  if (:KODP is null) then KODP = '';
  if (:FLAGI is null) then FLAGI = '';
  if (:nilkart is null) then nilkart = 0;
  if (:nilpal is null) then nilpal = 0;
  if (:opty is null) then opty = 0;
  if (:sposd is null) then sposd=0;
  if (:sposz is null) then sposz=0;

  kosztmin = 0;
  optysposmin = 0;
  kodsmin = '';
  iloscpalmin = 0;
  ilosckartmin = 0;
  k1min = 0;
  k2min = 0;
  k3min = 0;
  k4min = 0;
  k5min = 0;
  k6min = 0;
  k7min = 0;
  k8min = 0;

  -- jesli optymalizacja jest wylaczona
  for select sposdost.ref
    from sposdost
    where (sposdost.ref=:sposd and :opty=0) or (:opty=1 and sposdost.autozmiana=1)
    into :refsposdost
  do begin
    select sposdost.kalkulatorkoszt, sposdost.kalkulatorkosztpar
      from sposdost where sposdost.ref=:refsposdost into :proc, :numpar;

    refsposdostcennik = NULL;
    odleglosc = 0;
    kosztdod = 0;
    koszt = 0;
    optyspos = :refsposdost;
    kods = '';
    iloscpal = :nilpal;
    ilosckart = :nilkart;
    k1 = 0;
    k2 = 0;
    k3 = 0;
    k4 = 0;
    k5 = 0;
    k6 = 0;
    k7 = 0;
    k8 = 0;

    if (:proc is null or (:proc = '')) then begin

      -- obliczenie kosztu dostawy w normalny sposób

      -- ustalenie cennika
      select min(ref)
        from sposdostcennik
        where sposdost=:refsposdost and :datawys>=oddaty and :datawys<=dodaty
        into :refsposdostcennik;
      -- obliczenie odleglosci
      select first 1 spedodleg.odleglosc, spedodleg.kodrejonu
        from SPEDODLEG
        where spedodleg.sposdost = :refsposdost and spedodleg.kodp=:kodp
        into :odleglosc, :kods;

      -- obliczenie podstawy z tabeli dostkoszt
      if(:refsposdostcennik is not null) then begin
        select min(koszt)
          from kosztdost
          where sposdostcennik=:refsposdostcennik
          and ((:waga>=wagap and :waga<waga) or (waga=0 and wagap=0))
          and ((:odleglosc>=odlegloscp and :odleglosc<odleglosc) or (odleglosc=0 and odlegloscp=0))
          and ((:nilpal>=iloscpalp and :nilpal<=iloscpal) or (iloscpalp=0 and iloscpal=0))
          and ((:nilkart>=ilosckartp and :nilkart<=ilosckart) or (ilosckartp=0 and ilosckart=0))
          into :koszt;
      end else begin
        select prog, koszt from sposdost where ref=:refsposdost into :prog, :koszt;
        if(:sumwartbru>=:prog and :prog<>0) then koszt = 0;
      end
      if(:koszt is null) then koszt = 0;

      -- obliczenie dodatkow wynikajacych z flag
      for select sposdostflagi.procent, sposdostflagi.kwota
        from sposdostflagi
        where sposdostflagi.sposdost=:refsposdost
        and ';'||:flagi||';' like '%;'||sposdostflagi.flaga||';%'
        into :procentdod, :kwotadod
      do begin
        if(:procentdod is null) then procentdod = 0;
        if(:kwotadod is null) then kwotadod = 0;
        kosztdod = :kosztdod + (:procentdod*:koszt/100) + :kwotadod;
      end

      -- obliczenie dodatkow innych
      for select sposdostdodatki.procent, sposdostdodatki.kwota
        from sposdostdodatki
        where sposdostdodatki.sposdost=:refsposdost
        and :datawys>=oddaty and :datawys<=dodaty
        into :procentdod,  :kwotadod
      do begin
        if(:procentdod is null) then procentdod = 0;
        if(:kwotadod is null) then kwotadod = 0;
        kosztdod = :kosztdod + (:procentdod*:koszt/100) + :kwotadod;
      end

      -- wyliczenie ostatecznego kosztu
      koszt = :koszt + :kosztdod;

    end else begin --obliczanie przy pomocy procedury zewnetrznej
      if(:numpar is null) then numpar = 0;
      paramfields = '';
      i = 1;
      while(i<=8) do begin
        if(:numpar>0) then begin
          paramfields = :paramfields || ' , k'||cast(:i as varchar(2));
          numpar = :numpar - 1;
        end else begin
          paramfields = :paramfields || ' , 0';
        end
        i = :i + 1;
      end
      koszt=0;
      proced = 'select koszt, kods, iloscpal, ilosckart'||:paramfields||' from '||:proc||'('||:sposd||','||:sposz||','||:waga||','||:objetosc||','||:sumwartnet||','||:sumwartbru||','''||:kodp||''','''||:flagi||''','''||:spedytor||''','''||:datawys||''','||:zwrotpal||','||:nilpal||','||:nilkart||','''||:params||''')';
      if (:proced is not null) then execute statement :proced into :koszt, :kods, :iloscpal, :ilosckart, :k1, :k2, :k3, :k4, :k5, :k6, :k7, :k8;
      if (:koszt is null) then koszt=0;
      if (:kods is null) then kods=0;
      if (:iloscpal is null) then iloscpal = 0;
      if (:ilosckart is null) then ilosckart = 0;
    end
    if((:kosztmin = 0) or (:koszt < :kosztmin)) then begin
      kosztmin = :koszt;
      optysposmin = :optyspos;
      kodsmin = :kods;
      ilosckartmin = ilosckart;
      iloscpalmin = iloscpal;
      k1min = :k1;
      k2min = :k2;
      k3min = :k3;
      k4min = :k4;
      k5min = :k5;
      k6min = :k6;
      k7min = :k7;
      k8min = :k8;
    end
  end
  koszt = :kosztmin;
  optyspos = :optysposmin;
  kods = :kodsmin;
  ilosckart = :ilosckartmin;
  iloscpal = :iloscpalmin;
  k1 = :k1min;
  k2 = :k2min;
  k3 = :k3min;
  k4 = :k4min;
  k5 = :k5min;
  k6 = :k6min;
  k7 = :k7min;
  k8 = :k8min;
  suspend;
end^
SET TERM ; ^
