--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_INPOST_PACKSTATUS_RESPONSE(
      EDEDOCSH_REF integer)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
   as
declare variable shippingdoc integer;
declare variable shippingno varchar(40);
declare variable statuscode varchar(20);
declare variable statusdesc varchar(255);
declare variable packageno varchar(40);
declare variable datestr varchar(40);
declare variable package_node_id integer_id;
declare variable packageresp_node_id integer_id;
declare variable track_status_ref type of column x_listywysdtrackstatus.ref;
begin
  select oref
    from ededocsh
    where ref = :ededocsh_ref
  into :shippingdoc;

  select symbolsped from listywysd where ref = :shippingdoc
  into :shippingno;

  oref = shippingdoc;
  OTABLE = 'LISTYWYSD';

  for select ep.id, trim(ep.val)
    from ededocsp ep
    where ep.ededoch = :EDEDOCSH_REF
      and ep.name = 'package'
    into :package_node_id, :packageno
    do begin
      select p.id
        from ededocsp p
        where p.ededoch = :ededocsh_ref
          and p.parent = :package_node_id
          and p.name = 'packageResponse'
        into :packageresp_node_id;

      select trim(p.val)
        from ededocsp p
        where p.ededoch = :ededocsh_ref
          and p.parent = :packageresp_node_id
          and p.name = 'status'
        into :statuscode;

      select trim(p.val)
        from ededocsp p
        where p.ededoch = :ededocsh_ref
          and p.parent = :packageresp_node_id
          and p.name = 'statusDate'
        into :datestr;

      select ts.ref, ts.descript from x_listywysdtrackstatus ts
        where ts.spedytor = 'Inpost' and ts.orgdescript = :statuscode
      into :track_status_ref, :statusdesc;

      update listywysdroz_opk opk
        set opk.x_trackstatus = :track_status_ref,
          opk.x_trackstatusmsg = :statusdesc
        where opk.listwysd = :oref
          and coalesce(opk.symbolsped,'') = :packageno;

      update listywysd lwd
        set lwd.x_trackstatus = :track_status_ref
        where lwd.ref = :oref
          and coalesce(lwd.symbolsped,'') = :packageno;

      --ml obsluga historii statusow na razie nieobsluzona
      /*
      update or insert into listywysdstatusy(listwysd, symbolsped, opksymbolsped, data, kod, kodopis, opis, kodpunktu)
        values(:shippingdoc,:shippingno,:packageno,cast(:datestr as timestamp),:statuscode,null,:statusdesc,null)
        matching(listwysd,kod);
      */
    end
  suspend;
end^
SET TERM ; ^
