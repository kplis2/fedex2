--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SPRZEDZEST_OBL_ROZRACH(
      SPRZEDZESTID integer)
   as
declare variable wplata numeric(14,2);
declare variable skrot varchar(20);
declare variable slodef integer;
declare variable slopoz integer;
declare variable company integer;
begin
  select SLODEF, SLOPOZ, SYMBFAK, company from ROZRACH
    where SPRZEDZESTID=:SPRZEDZESTID
    into :slodef, :slopoz, :skrot, :company;
  select sum(WINIEN) from ROZRACHP
    where company = :company and slodef = :slodef and slopoz = :slopoz
      and SYMBFAK = :skrot
    into :wplata;
  if (wplata is null) then wplata = 0;
  update SPRZEDZEST set WYPLACONO=:wplata where REF=:sprzedzestid;
end^
SET TERM ; ^
