--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ROZRACH_COPY(
      FSLODEF integer,
      FSLOPOZ integer,
      FKONTOFK KONTO_ID,
      FSYMBFAK varchar(20) CHARACTER SET UTF8                           ,
      TSLODEF integer,
      TSLOPOZ integer,
      TKONTOFK KONTO_ID,
      TSYMBFAK varchar(20) CHARACTER SET UTF8                           ,
      MWN numeric(15,2),
      MMA numeric(15,2),
      TDATAOTW timestamp,
      TDATAPLAT timestamp,
      COMPANY integer)
  returns (
      ISNEWROZRACH integer,
      SALDO numeric(15,2),
      STRONA varchar(10) CHARACTER SET UTF8                           )
   as
declare variable CNT integer;
declare variable FSCLRACCHREF integer;
declare variable ROZRACHPRZEN integer;
declare variable CURDATE timestamp;
declare variable WALUTA varchar(3);
declare variable KONTO  KONTO_ID;
declare variable SLOTYP varchar(15);
declare variable ADDKONTO  KONTO_ID;
declare variable TYP smallint;
declare variable KONFIGW varchar(20);
declare variable FSAKCEPT smallint;
begin
  execute procedure get_config('SIDFK_ROZLKOMPEN',2) returning_values konfigw;
  fsakcept = cast(konfigw as smallint);
  fsakcept = fsakcept + 1;

  -- jezeli nie bylo to zostanie stworzony
  select R.slodef from ROZRACH R where SLODEF = :tslodef and SLOPOZ = :tslopoz and KONTOFK = :tkontofk and SYMBFAK = :tsymbfak and company = :company
    into :isnewrozrach;
  if (isnewrozrach = 0 or isnewrozrach is null ) then
    isnewrozrach = 1;
  else
    isnewrozrach = 0;

  -- 0/1 rozliczenie/kompensata
  if (FSLODEF = TSLODEF and FSLOPOZ = TSLOPOZ and TKONTOFK = FKONTOFK) then
    typ = 0; -- to jest rozliczenie
  else
    typ = 1; -- to jest kompensata

  select R.waluta
    from rozrach R
    where R.slodef = :FSLODEF and R.slopoz = :FSLOPOZ and r.symbfak = :FSYMBFAK and r.company = :company
    INTO :waluta;
  execute procedure GEN_REF('FSCLRACCH') returning_values fsclracchref;
  insert into fsclracch(REF, DICTDEF, DICTPOS, STATUS, STABLE , currency, FSTYPE, FSTYPEAKC, company)
    values(:fsclracchref, :FSLODEF, :FSLOPOZ, 0, 'ROZRACH', :waluta, :typ, :fsakcept, :company);

  insert into fsclraccp (fsclracch, currdebit, currcredit, debit, credit,
    currdebit2c, currcredit2c, debit2c, credit2c,rate, autopos
    , ACCOUNT, SETTLEMENT, DICTDEF, DICTPOS, OPERDATE, DESCRIPTION)
  values (:fsclracchref, :MWN, :MMA, :MWN, :MMA,
    :MWN, :MMA, :MWN, :MMA, 1, 1
    ,:fKONTOFK, :fSYMBFAK , :fSLODEF, :fSLOPOZ, :TDATAOTW, 'Przeniesienie z'||:TSYMBFAK );

  if (TKONTOFK is null or TKONTOFK = '' ) then
  BEGIN
    select S.kontofk
      from slo_daneks(:TSLODEF, :TSLOPOZ) S
      into :TKONTOFK;
  end

  insert into fsclraccp (fsclracch , currdebit, currcredit, debit, credit,
    currdebit2c, currcredit2c, debit2c, credit2c,rate, autopos
    , ACCOUNT, SETTLEMENT, DICTDEF, DICTPOS, PAYDAY, OPERDATE, DESCRIPTION)
  values (:fsclracchref, :MMA, :MWN, :MMA, :MWN,
    :MMA, :MWN, :MMA, :MWN, 1, 1
    ,:tKONTOFK, :tSYMBFAK , :tSLODEF, :tSLOPOZ, :TDATAPLAT, :TDATAOTW, 'Przeniesienie z'||:FSYMBFAK);

--  execute procedure getconfig('SIDFK_KOMPENSAAUTO') returning_values :konfigw;
  if (fsakcept = 3) then
    update fsclracch set status = 1 where ref = :fsclracchref;

  -- fragment kodu z poprzedniej wersji zeby zachowac funkcjonalnosc dotychczasowa
  strona = 'Winien';
  select WINIEN - MA from ROZRACH
    where SLODEF = :tslodef and SLOPOZ = :tslopoz and KONTOFK = :tkontofk and SYMBFAK = :tsymbfak
      and company = :company
    into :saldo;

  if (saldo is null) then saldo = 0;
  if (saldo < 0) then begin
    saldo = -saldo;
    strona = 'Ma';
  end else if (saldo = 0) then
    strona = 'brak';
 -- koniec kodu z poprzedniej wersji
end^
SET TERM ; ^
