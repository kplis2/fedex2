--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_8(
      BKDOC integer)
   as
declare variable account ACCOUNT_ID;
  declare variable side integer;
  declare variable amount numeric(14,2);
  declare variable tmp varchar(255);
  declare variable settlement varchar(20);
  declare variable docdate timestamp;
  declare variable payday timestamp;
  declare variable descript varchar(255);
  declare variable descript1 varchar(255);
  declare variable descript2 varchar(255);
  declare variable dictpos integer;
  declare variable dictdef integer;
  declare variable debited smallint;
  declare variable account1 ACCOUNT_ID;
  declare variable account2 ACCOUNT_ID;
  declare variable vatv numeric(14,2);
  declare variable vate numeric(14,2);
  declare variable zagraniczny smallint;
  declare variable amountnet numeric(14,2);
  declare variable bankaccount integer;
  declare variable nagfak integer;
  declare variable crnote smallint;
  declare variable sumprocentplat numeric(14,2);
  declare variable fsopertype smallint;
  declare variable otable varchar(40);
  declare variable oref integer;
begin
  -- WDT

 select B.sumgrossv, B.symbol, B.docdate, B.payday,B.descript, B.dictdef, B.dictpos,
   B.oref, T.creditnote, bankaccount, b.otable, b.oref
    from bkdocs B
      left join bkdoctypes T on (B.doctype = T.ref)
      left join nagfak N on (B.oref = N.ref)
      left join oddzialy O on (N.oddzial = O.oddzial)
      left join vatregs VR on (B.vatreg = VR.symbol and B.company = VR.company)
    where B.ref = :bkdoc
    into :amount, :settlement, :docdate, :payday, :descript1, :dictdef, :dictpos,
    :nagfak, :crnote, :bankaccount, :otable, :oref;

  descript = descript1;

  if (crnote = 1) then
    fsopertype = 4;
  else
    fsopertype = 3;

  side = 1;
  execute procedure KODKS_FROM_DICTPOS(:dictdef, :dictpos)
    returning_values :tmp;

  select dostawcy.zagraniczny
    from dostawcy
    where dostawcy.kontofk = :tmp
    into :zagraniczny;
  if (zagraniczny = 0) then
    account = '202-' || tmp;
  else
    account = '204-' || tmp;

  execute procedure fk_autodecree_termplat(bkdoc,account, amount, 0,0,'', side , settlement,
                                          oref, otable,fsopertype,docdate,
                                          payday,descript, 0, bankaccount)
    returning_values :sumprocentplat;
 for
    select netv, vatv, vate,  account1, account2, descript, debited
      from bkvatpos where bkdoc=:bkdoc
      into :amount, :vatv, :vate, :account1, :account2, :descript2, :debited
  do begin
    descript = descript2;
    side = 0;

    amountnet= amount;

    if (debited = 1) then begin
      side = 0;
      amount = vatv;
      account = '225';
      execute procedure insert_decree(bkdoc, account, side, amount, descript, 0);
      if (vate-vatv > 0) then --jezeli nie odliczamy wszystkiego
        amountnet = amountnet + (vate - vatv);
    end else
    begin
      side = 0;
      amountnet = amountnet + vate;
    end

    descript = descript2;
    amount = amountnet;

    account = '300';
    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

    account = '300';
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

    account = account1;
    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

    account = '490';
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

    if(account2 = '') then
      account = '501';
    else
      account = account2;
    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

    if(account2 = '') then
      account = '501';
    else
      account = account2;
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

    account = '710';
    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);
  end
end^
SET TERM ; ^
