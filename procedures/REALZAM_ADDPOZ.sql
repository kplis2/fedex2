--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REALZAM_ADDPOZ(
      DOKUMENT integer,
      TYP char(1) CHARACTER SET UTF8                           ,
      IDENT integer,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      WGZAMOWIEN integer)
   as
declare variable zamowienie integer;
declare variable pozzamref integer;
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable wersjaref integer;
declare variable iloscdost numeric(14,4);
declare variable ilosc numeric(14,4);
declare variable ilosco numeric(14,4);
declare variable iloscm numeric(14,4);
declare variable isilosc numeric(14,4);
declare variable isilosco numeric(14,4);
declare variable isiloscm numeric(14,4);
declare variable cenacen numeric(14,4);
declare variable walcen varchar(3);
declare variable rabat numeric(14,4);
declare variable jedn integer;
declare variable jedno integer;
declare variable ilpoz integer;
declare variable przelicz numeric(14,4);
declare variable opk smallint;
declare variable trybopk smallint;
declare variable wydanie smallint;
declare variable cenanet numeric(14,4);
declare variable cenabru numeric(14,4);
declare variable dostawamag integer;
declare variable paramn1 numeric(14,4);
declare variable paramn2 numeric(14,4);
declare variable paramn3 numeric(14,4);
declare variable paramn4 numeric(14,4);
declare variable paramn5 numeric(14,4);
declare variable paramn6 numeric(14,4);
declare variable paramn7 numeric(14,4);
declare variable paramn8 numeric(14,4);
declare variable paramn9 numeric(14,4);
declare variable paramn10 numeric(14,4);
declare variable paramn11 numeric(14,4);
declare variable paramn12 numeric(14,4);
declare variable params1 varchar(255);
declare variable params2 varchar(255);
declare variable params3 varchar(255);
declare variable params4 varchar(255);
declare variable params5 varchar(255);
declare variable params6 varchar(255);
declare variable params7 varchar(255);
declare variable params8 varchar(255);
declare variable params9 varchar(255);
declare variable params10 varchar(255);
declare variable params11 varchar(255);
declare variable params12 varchar(255);
declare variable paramd1 timestamp;
declare variable paramd2 timestamp;
declare variable paramd3 timestamp;
declare variable paramd4 timestamp;
declare variable bn char(1);
declare variable vat numeric(14,4);
declare variable bnzam char(1);
declare variable pozzammagazyn varchar(3);
declare variable pozfakmagazyn varchar(3);
declare variable realzamildost smallint;
declare variable usluga smallint;
declare variable prec smallint;
declare variable gr_vat varchar(5);
begin
  ilpoz = 0;
  select N.bn
    from nagzam N
    where N.ref = :ident
    into :bnzam;
  if (bnzam is null) then
    execute procedure GETCONFIG('WGCEN') returning_values :bnzam;
  if(:typ = 'M') then
    select DEFDOKUM.opak, DEFDOKUM.wydania,dokumnag.bn, defdokummag.realzamildost
      from DEFDOKUM
      join DOKUMNAG on (DEFDOKUM.SYMBOL = DOKUMNAG.TYP)
      left join defdokummag on defdokummag.typ = defdokum.symbol and defdokummag.magazyn = dokumnag.magazyn
      where DOKUMNAG.ref = :dokument
    into :trybopk, :wydanie,:bn, :realzamildost;
  else
    select 1-NAGFAK.zakup,nagfak.bn, stantypfak.realzamildost
      from NAGFAK
      left join stantypfak on nagfak.typ = stantypfak.typfak and nagfak.stanowisko = stantypfak.stansprzed
      where REF=:dokument
      into :wydanie, :bn, :realzamildost;
  if (:bn is null) then
    execute procedure GETCONFIG('WGCEN') returning_values :bn;
  if(realzamildost is null) then realzamildost = 0;
  if(:wgzamowien = 1) then begin
    /*dodanoie pozycji zamowienia do dokumentu - magazynowego lub faktury*/
    for select POZZAM.zamowienie,POZZAM.REF,POZZAM.KTM,POZZAM.WERSJA,POZZAM.WERSJAREF,POZZAM.CENACEN,POZZAM.RABAT,POZZAM.CENANET,POZZAM.CENABRU,
       POZZAM.ILOSC - POZZAM.ILZREAL,POZZAM.ILOSCM - POZZAM.ILZREALM - iif (:typ = 'O',pozzam.ilosconmwsacts,0),
       POZZAM.JEDNO, POZZAM.JEDN,POZZAM.DOSTAWAMAG,
       POZZAM.CENACEN, POZZAM.WALCEN,POZZAM.RABAT,POZZAM.OPK,
       POZZAM.paramn1, POZZAM.paramn2, POZZAM.paramn3, POZZAM.paramn4,
       POZZAM.paramn5, POZZAM.paramn6, POZZAM.paramn7, POZZAM.paramn8,
       POZZAM.paramn9, POZZAM.paramn10, POZZAM.paramn11, POZZAM.paramn12,
       POZZAM.params1, POZZAM.params2, POZZAM.params3, POZZAM.params4,
       POZZAM.params5, POZZAM.params6, POZZAM.params7, POZZAM.params8,
       POZZAM.params9, POZZAM.params10, POZZAM.params11, POZZAM.params12,
       POZZAM.paramd1, POZZAM.paramd2, POZZAM.paramd3, POZZAM.paramd4, pozzam.magazyn, pozzam.prec, pozzam.gr_vat
    from POZZAM where ZAMOWIENIE = :ident
    order by POZZAM.NUMER
    into :zamowienie, :pozzamref, :ktm, :wersja, :wersjaref,:cenacen,:rabat,:cenanet,:cenabru,
         :ilosc,:iloscm,
         :jedno, :jedn, :dostawamag,
         :cenacen, :walcen, :rabat,:opk,
         :paramn1, :paramn2, :paramn3, :paramn4,
         :paramn5, :paramn6, :paramn7, :paramn8,
         :paramn9, :paramn10, :paramn11, :paramn12,
         :params1, :params2, :params3, :params4,
         :params5, :params6, :params7, :params8,
         :params9, :params10, :params11, :params12,
         :paramd1, :paramd2, :paramd3, :paramd4, :pozzammagazyn, :prec, :gr_vat

    do begin
      -- identyfikacja grupy i stawki vat
      if(:gr_vat is null) then
        select wersje.vat from wersje where wersje.ref = :wersjaref into :gr_vat;
      if(:gr_vat is null) then
        select towary.vat from towary where towary.ktm = :ktm into :gr_vat;
      select coalesce(vat.stawka,0.0) from vat where vat.grupa = :gr_vat into :vat;

       select WERSJE.usluga from WERSJE where REF=:wersjaref into :usluga;
       if(:dostawamag=0) then dostawamag = null;
       if(:jedn = :jedno) then
         ilosco = :ilosc;
       else begin
         select PRZELICZ from TOWJEDN where REF=:jedno into :przelicz;
         ilosco = :iloscm / :przelicz;
       end
       if(:typ = 'M') then begin
          isilosc = null;
          isilosco = null;
          isiloscm = null;
          --przeliczanie cen wynikajace z roznic wyceny wg brutto lub netto
          if((:bn = 'B' and :bnzam = 'N')) then begin
            cenacen = :cenacen * (100 + :vat)/100;
          end
          else if ((:bn = 'N' and :bnzam = 'B')) then begin
            cenacen = :cenacen * (1- (:vat/(100+:vat)));
          end

          if((:trybopk = 1 and :opk = 0) or (:trybopk = 0 and :opk = 1)) then
            iloscm = 0;
          if(:iloscm > 0) then begin
            select sum(ILOSC), sum(ilosco) from DOKUMPOZ where DOKUMENT = :DOKUMENT and FROMZAM = :zamowienie and FROMPOZZAM = :pozzamref into :isiloscm, :isilosco;
            if(:isiloscm > 0) then begin
              iloscm = :iloscm - :isiloscm;
              ilosco = :ilosco - :isilosco;
            end
          end
          if(:iloscm > 0 and :wydanie=1 and realzamildost=1 and :usluga<>1) then begin
            /*badanie ilosci dostepnej*/
            execute procedure MAG_CHECK_ILOSC(:zamowienie,:magazyn,:ktm,:wersja,0,0) returning_values :iloscdost;
            if(:iloscdost < :iloscm) then iloscm = :iloscdost;
          end
          if(:iloscm > 0) then begin
            if(:wydanie = 1) then begin
              insert into DOKUMPOZ(DOKUMENT,KTM,WERSJA,JEDNO,ILOSCO,ILOSC,CENACEN,RABAT,CENANET,CENABRU,FROMZAM,FROMPOZZAM,DOSTAWA,
                                   paramn1, paramn2, paramn3, paramn4,
                                   paramn5, paramn6, paramn7, paramn8,
                                   paramn9, paramn10, paramn11, paramn12,
                                   params1, params2, params3, params4,
                                   params5, params6, params7, params8,
                                   params9, params10, params11, params12,
                                   paramd1, paramd2, paramd3, paramd4, prec, gr_vat)
               values(:dokument,:ktm,:wersja,:jedno,:ilosco,:iloscm,:cenacen,:rabat,:cenanet,:cenabru,:zamowienie,:pozzamref,:dostawamag,
                      :paramn1, :paramn2, :paramn3, :paramn4,
                      :paramn5, :paramn6, :paramn7, :paramn8,
                      :paramn9, :paramn10, :paramn11, :paramn12,
                      :params1, :params2, :params3, :params4,
                      :params5, :params6, :params7, :params8,
                      :params9, :params10, :params11, :params12,
                      :paramd1, :paramd2, :paramd3, :paramd4, :prec, :gr_vat);
            end else begin
              insert into DOKUMPOZ(DOKUMENT,KTM,WERSJA,JEDNO,ILOSCO,ILOSC,CENA,FROMZAM,FROMPOZZAM,DOSTAWA,
                             paramn1, paramn2, paramn3, paramn4,
                                   paramn5, paramn6, paramn7, paramn8,
                                   paramn9, paramn10, paramn11, paramn12,
                                   params1, params2, params3, params4,
                                   params5, params6, params7, params8,
                                   params9, params10, params11, params12,
                             paramd1, paramd2, paramd3, paramd4, prec, gr_vat)
               values(:dokument,:ktm,:wersja,:jedno,:ilosco,:iloscm,:cenanet,:zamowienie,:pozzamref,:dostawamag,
                      :paramn1, :paramn2, :paramn3, :paramn4,
                      :paramn5, :paramn6, :paramn7, :paramn8,
                      :paramn9, :paramn10, :paramn11, :paramn12,
                      :params1, :params2, :params3, :params4,
                      :params5, :params6, :params7, :params8,
                      :params9, :params10, :params11, :params12,
                      :paramd1, :paramd2, :paramd3, :paramd4, :prec, :gr_vat);

            end
          end
       end else if (:typ = 'O') then begin
         insert into mwsacts (mwsord, good, vers, quantity, doctype, docid, docposid, autogen)
           values (:dokument, :ktm, :wersjaref, :iloscm, 'Z', :zamowienie, :pozzamref, 1);
       end else begin
          if(pozzammagazyn<>'') then pozfakmagazyn = :pozzammagazyn;
          else pozfakmagazyn = :magazyn;
          ilpoz = ilpoz + 1;
          isilosc = null;
          isilosco = null;
          isiloscm = null;
          --przeliczanie cen wynikajace z roznic wyceny wg brutto lub netto
          if((:bn = 'B' and :bnzam = 'N')) then begin
            cenacen = :cenacen * (100 + :vat)/100;
          end
          else if ((:bn = 'N' and :bnzam = 'B')) then begin
            cenacen = :cenacen * (1- (:vat/(100+:vat)));
          end

          select sum(ILOSC), sum(ilosco),sum(iloscm) from POZFAK where DOKUMENT = :DOKUMENT and FROMZAM = :zamowienie and FROMPOZZAM = :pozzamref and
             JEDN = :JEDN and JEDNO = :jedno and OPK = :opk into :isilosc, :isilosco, :isiloscm;
          if(:isiloscm > 0) then begin
            iloscm = :iloscm - :isiloscm;
            ilosco = :ilosco - :isilosco;
            ilosc = :ilosc - :isilosc;
          end
          if(:iloscm > 0 and :wydanie=1 and realzamildost=1 and :usluga<>1) then begin
            /*badanie ilosci dostepnej*/
            execute procedure MAG_CHECK_ILOSC(:zamowienie,:magazyn,:ktm,:wersja,0,0) returning_values :iloscdost;
            if(:iloscdost < :iloscm) then iloscm = :iloscdost;
          end
          if(:iloscm > 0) then begin
            insert into POZFAK(DOKUMENT,KTM,WERSJA,JEDNO,JEDN,ILOSCO,ILOSC,ILOSCM,CENACEN,WALCEN, RABAT,FROMZAM,FROMPOZZAM,OPK,DOSTAWA,
                                   paramn1, paramn2, paramn3, paramn4,
                                   paramn5, paramn6, paramn7, paramn8,
                                   paramn9, paramn10, paramn11, paramn12,
                                   params1, params2, params3, params4,
                                   params5, params6, params7, params8,
                                   params9, params10, params11, params12,
                                   paramd1, paramd2, paramd3, paramd4, magazyn, prec)
              values(:dokument,:ktm,:wersja,:jedno,:jedn, :ilosco,:ilosc,:iloscm,:cenacen,:WALCEN, :rabat,:zamowienie,:pozzamref,:opk,:dostawamag,
                      :paramn1, :paramn2, :paramn3, :paramn4,
                      :paramn5, :paramn6, :paramn7, :paramn8,
                      :paramn9, :paramn10, :paramn11, :paramn12,
                      :params1, :params2, :params3, :params4,
                      :params5, :params6, :params7, :params8,
                      :params9, :params10, :params11, :params12,
                      :paramd1, :paramd2, :paramd3, :paramd4, :pozfakmagazyn, :prec);
          end
       end
    end
  end else begin
    /*dodaniee jednej pozycji zamowienia do dokumentu - magazynowego lub faktury*/
    for select POZZAM.zamowienie,POZZAM.REF,POZZAM.KTM,POZZAM.WERSJA,POZZAM.WERSJAREF, POZZAM.CENANET, POZZAM.cenabru, 
       POZZAM.ILOSC - POZZAM.ILZREAL, POZZAM.ILOSCM - POZZAM.ILZREALM - iif (:typ = 'O',pozzam.ilosconmwsacts,0), POZZAM.ILOSCO - POZZAM.ILREALO,
       POZZAM.JEDNO, POZZAM.JEDN, POZZAM.DOSTAWAMAG,
       POZZAM.CENACEN, POZZAM.WALCEN,POZZAM.RABAT,POZZAM.OPK,
       POZZAM.paramn1, POZZAM.paramn2, POZZAM.paramn3, POZZAM.paramn4,
       POZZAM.paramn5, POZZAM.paramn6, POZZAM.paramn7, POZZAM.paramn8,
       POZZAM.paramn9, POZZAM.paramn10, POZZAM.paramn11, POZZAM.paramn12,
       POZZAM.params1, POZZAM.params2, POZZAM.params3, POZZAM.params4,
       POZZAM.params5, POZZAM.params6, POZZAM.params7, POZZAM.params8,
       POZZAM.params9, POZZAM.params10, POZZAM.params11, POZZAM.params12,
       POZZAM.paramd1, POZZAM.paramd2, POZZAM.paramd3, POZZAM.paramd4, POZZAM.magazyn, POZZAM.prec, POZZAM.gr_vat
    from POZZAM where REF = :ident
    into :zamowienie, :pozzamref, :ktm, :wersja, :wersjaref, :cenanet, :cenabru,
        :ilosc, :iloscm, :ilosco,
        :jedno, :jedn, :dostawamag,
        :cenacen, :walcen, :rabat, :opk,
        :paramn1, :paramn2, :paramn3, :paramn4,
        :paramn5, :paramn6, :paramn7, :paramn8,
        :paramn9, :paramn10, :paramn11, :paramn12,
        :params1, :params2, :params3, :params4,
        :params5, :params6, :params7, :params8,
        :params9, :params10, :params11, :params12,
        :paramd1, :paramd2, :paramd3, :paramd4, :pozzammagazyn, :prec, :gr_vat

    do begin
      -- identyfikacja grupy i stawki vat
      if(:gr_vat is null) then
        select wersje.vat from wersje where wersje.ref = :wersjaref into :gr_vat;
      if(:gr_vat is null) then
        select towary.vat from towary where towary.ktm = :ktm into :gr_vat;
      select coalesce(vat.stawka,0.0) from vat where vat.grupa = :gr_vat into :vat;

       select WERSJE.usluga from WERSJE where REF=:wersjaref into :usluga;
       if(:dostawamag=0) then dostawamag = null;
       if(:typ = 'M') then begin
          isilosc = null;
          isilosco = null;
          isiloscm = null;

          --przeliczanie cen wynikajace z roznic wyceny wg brutto lub netto
          if((:bn = 'B' and :bnzam = 'N')) then begin
            cenacen = :cenacen * (100 + :vat)/100;
          end
          else if ((:bn = 'N' and :bnzam = 'B')) then begin
            cenacen = :cenacen * (1- (:vat/(100+:vat)));
          end

          select sum(ILOSC), sum(ilosco) from DOKUMPOZ where DOKUMENT = :DOKUMENT and FROMZAM = :zamowienie and FROMPOZZAM = :pozzamref into :isiloscm, :isilosco;
          if(:isiloscm > 0) then begin
            iloscm = :iloscm - :isiloscm;
            ilosco = :ilosco - :isilosco;
          end
          if(:iloscm > 0 and :wydanie=1 and realzamildost=1 and :usluga<>1) then begin
            /*badanie ilosci dostepnej*/
            execute procedure MAG_CHECK_ILOSC(:zamowienie,:magazyn,:ktm,:wersja,0,0) returning_values :iloscdost;
            if(:iloscdost < :iloscm) then iloscm = :iloscdost;
          end
          if(:iloscm > 0) then begin
            if(:wydanie = 1) then begin
              insert into DOKUMPOZ(DOKUMENT,KTM,WERSJA,JEDNO,ILOSCO,ILOSC,CENACEN,RABAT,CENANET,CENABRU,FROMZAM,FROMPOZZAM,DOSTAWA,
                                   paramn1, paramn2, paramn3, paramn4,
                                   paramn5, paramn6, paramn7, paramn8,
                                   paramn9, paramn10, paramn11, paramn12,
                                   params1, params2, params3, params4,
                                   params5, params6, params7, params8,
                                   params9, params10, params11, params12,
                                   paramd1, paramd2, paramd3, paramd4, prec, gr_vat)
               values(:dokument,:ktm,:wersja,:jedno,:ilosco,:iloscm,:cenacen,:rabat,:cenanet,:cenabru,:zamowienie,:pozzamref,:dostawamag,
                      :paramn1, :paramn2, :paramn3, :paramn4,
                      :paramn5, :paramn6, :paramn7, :paramn8,
                      :paramn9, :paramn10, :paramn11, :paramn12,
                      :params1, :params2, :params3, :params4,
                      :params5, :params6, :params7, :params8,
                      :params9, :params10, :params11, :params12,
                      :paramd1, :paramd2, :paramd3, :paramd4, :prec, :gr_vat);
            end else begin
              insert into DOKUMPOZ(DOKUMENT,KTM,WERSJA,JEDNO,ILOSCO,ILOSC,CENA,FROMZAM,FROMPOZZAM,DOSTAWA,
                                   paramn1, paramn2, paramn3, paramn4,
                                   paramn5, paramn6, paramn7, paramn8,
                                   paramn9, paramn10, paramn11, paramn12,
                                   params1, params2, params3, params4,
                                   params5, params6, params7, params8,
                                   params9, params10, params11, params12,
                                   paramd1, paramd2, paramd3, paramd4, prec, gr_vat)
               values(:dokument,:ktm,:wersja,:jedno,:ilosco,:iloscm,:cenanet,:zamowienie,:pozzamref,:dostawamag,
                      :paramn1, :paramn2, :paramn3, :paramn4,
                      :paramn5, :paramn6, :paramn7, :paramn8,
                      :paramn9, :paramn10, :paramn11, :paramn12,
                      :params1, :params2, :params3, :params4,
                      :params5, :params6, :params7, :params8,
                      :params9, :params10, :params11, :params12,
                      :paramd1, :paramd2, :paramd3, :paramd4, :prec, :gr_vat);

            end
          end
       end else if (:typ = 'O') then begin
         insert into mwsacts (mwsord, good, vers, quantity, doctype, docid, docposid, autogen)
          values (:dokument, :ktm, :wersjaref, :iloscm, 'Z', :zamowienie, :pozzamref, 1);
       end else begin
          isilosc = null;
          isilosco = null;
          isiloscm = null;
          if(pozzammagazyn<>'') then pozfakmagazyn = :pozzammagazyn;
          else pozfakmagazyn = :magazyn;

          --przeliczanie cen wynikajace z roznic wyceny wg brutto lub netto
          if((:bn = 'B' and :bnzam = 'N')) then begin
            cenacen = :cenacen * (100 + :vat)/100;
          end
          else if ((:bn = 'N' and :bnzam = 'B')) then begin
            cenacen = :cenacen * (1- (:vat/(100+:vat)));
          end

          select sum(ILOSC), sum(ilosco),sum(iloscm) from POZFAK where DOKUMENT = :DOKUMENT and FROMZAM = :zamowienie and FROMPOZZAM = :pozzamref and
             JEDN = :JEDN and JEDNO = :jedno and OPK = :opk into :isilosc, :isilosco, :isiloscm;
          if(:isiloscm > 0) then begin
            iloscm = :iloscm - :isiloscm;
            ilosco = :ilosco - :isilosco;
            ilosc = :ilosc - :isilosc;
          end
          if(:iloscm > 0 and :wydanie=1 and realzamildost=1 and :usluga<>1) then begin
            /*badanie ilosci dostepnej*/
            execute procedure MAG_CHECK_ILOSC(:zamowienie,:magazyn,:ktm,:wersja,0,0) returning_values :iloscdost;
            if(:iloscdost < :iloscm) then iloscm = :iloscdost;
          end
          if(:iloscm > 0) then begin
            select PRZELICZ from TOWJEDN where REF=:jedno into :przelicz;
            ilosco = :iloscm / :przelicz;
            select PRZELICZ from TOWJEDN where REF=:jedn into :przelicz;
            ilosc = :iloscm / :przelicz;
          end
          if(:iloscm > 0) then begin
            insert into POZFAK(DOKUMENT,KTM,WERSJA,JEDNO,JEDN,ILOSCO,ILOSC,ILOSCM,CENACEN,WALCEN, RABAT,FROMZAM,FROMPOZZAM,OPK,DOSTAWA,
                                   paramn1, paramn2, paramn3, paramn4,
                                   paramn5, paramn6, paramn7, paramn8,
                                   paramn9, paramn10, paramn11, paramn12,
                                   params1, params2, params3, params4,
                                   params5, params6, params7, params8,
                                   params9, params10, params11, params12,
                                   paramd1, paramd2, paramd3, paramd4, magazyn, prec)
             values(:dokument,:ktm,:wersja,:jedno,:jedn, :ilosco,:ilosc,:iloscm,:cenacen,:WALCEN, :rabat,:zamowienie,:pozzamref,:opk,:dostawamag,
                      :paramn1, :paramn2, :paramn3, :paramn4,
                      :paramn5, :paramn6, :paramn7, :paramn8,
                      :paramn9, :paramn10, :paramn11, :paramn12,
                      :params1, :params2, :params3, :params4,
                      :params5, :params6, :params7, :params8,
                      :params9, :params10, :params11, :params12,
                      :paramd1, :paramd2, :paramd3, :paramd4, :pozfakmagazyn, :prec);

          end
       end
    end
  end
end^
SET TERM ; ^
