--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDE_CALC_TORIGHT(
      PRSCHEDGUIDE integer)
   as
declare variable curtime timestamp;
declare variable tstart timestamp;
declare variable tdepend timestamp;
declare variable tend timestamp;
declare variable machine integer;
declare variable prdepart varchar(20);
declare variable minspace integer;
declare variable round integer;
declare variable minlength double precision;
declare variable prschdate timestamp;
declare variable prschmachine smallint;
declare variable worktime double precision;
declare variable schedoper integer;

declare variable schedoperpom integer;
begin
  schedoperpom = null;
  select a.mintimebetweenopers, a.roundstarttime, a.fromdate, a.prdepart
    from prschedules a
      join prschedzam b on (a.prdepart = b.prdepart)
      join prschedguides g on (g.prschedzam = b.ref)
    where g.ref = :PRSCHEDGUIDE
    into :minspace, :round, :curtime, :prdepart;

  minlength = 1440;
  minlength = 1/:minlength;
  if(:round > :minspace or (:minspace is null) ) then minspace = coalesce(:round,0);
  minlength = :minlength * :minspace;

  if(:curtime is null or (:curtime < current_timestamp(0)))then
    curtime = current_timestamp(0);

  update prschedopers p set p.verified = 0, p.starttime = null, p.endtime = null, p.calcdeptimes = 0
    where p.guide = :prschedguide;

  for select a.ref, a.MACHINE, a.worktime, g.prschdate, g.prschmachine
    from PRSCHEDOPERS a
      join prschedguides g on (g.ref = a.guide)
    where g.ref = :prschedguide
      and coalesce(a.workplace,'') <> ''
      and a.worktime > 0
      and a.activ = 1
      and coalesce(a.prschedcalcblock,0) = 0
    order by a.number desc
    into :schedoper, :machine, :worktime, :prschdate, :prschmachine
  do begin

    if (:prschdate is null) then
      prschdate = current_timestamp(0);

    tdepend = null;

    select o.starttime  --odczytanie czasu rozpoczecia nastepnej operacji
      from prschedopers o
      where o.ref = :schedoperpom
        and o.starttime is not null
        and o.activ = 1
        and o.verified = 1
      into :tdepend;

    schedoperpom = :schedoper;

    if(:tdepend is null) then
      tdepend = :prschdate;
    else
      tdepend = :tdepend - :minlength;
    tend = cast(:tdepend as timestamp);   --przypisanie czasu rozpoczecia
                                          --nastepnej operacji jako czasu zakonczenia biezacej

    --sprawdzenie, czy koniec nie wypad w czasie przerwy pracy
    tdepend = null;
    execute procedure PRSCHED_CALENDAR_FW_TR(:prdepart,:tend)
      returning_values :tdepend;

    if(:tdepend <> :tend and :tdepend is not null)then
      tend = :tdepend;

    --obliczenie daty rozpoczecia operacji z uwzglednieniem dni wolnych od pracy
    --execute procedure pr_calendar_checkendtime_tr(:prdepart, :tend, :worktime)
    --  returning_values :tstart;
    tstart = null;
    execute procedure pr_calendar_checkendtime_tr(:prdepart, :tend, :worktime)
      returning_values (tstart);
            
    --rezerwacaj czasu maszyny, czas rozpoczecia operacji nie jest mniejszy od biezacego
    if (:prschmachine = 1 and :tstart > :curtime) then begin
      select first 1 starttime, endtime, machine  --pobranie pierwszego wolnegi i pasujacego terminu
        from prsched_find_machine_toright(:schedoper, :tend, 0)
        into :tstart,  :tend, :machine;
    end else begin
      machine = null;
    end

    update prschedopers set STARTTIME = :tstart, ENDTIME = :tend, MACHINE = :machine, verified = 1, calcdeptimes = 3
      where ref = :schedoper;

  end
end^
SET TERM ; ^
