--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COUNTWAGES_SELLINCREASE(
      STABLE varchar(31) CHARACTER SET UTF8                           ,
      STYPE varchar(3) CHARACTER SET UTF8                           ,
      ACTUDATE timestamp,
      DAYS integer,
      INCREASEPARAM integer)
  returns (
      INCREASED smallint)
   as
declare variable dayweek integer;
declare variable cnt integer;
declare variable cnt2 integer;
declare variable cnt3 integer;
declare variable actucnt numeric(14,2);
declare variable tmpcnt numeric(14,2);
declare variable lastcnt numeric(14,2);
begin
  cnt = 0;
  if (:days = 0 or :days is null) then
    increased = 0;
  if (:stable = 'DOKUMNAG' and :days > 0) then
  begin
    actucnt = 0;
    lastcnt = 0;
    cnt = 0;
    cnt2 = 0;
    while (:cnt < :days)
    do begin
      dayweek = CAST( extract(weekday from cast((cast(:actudate as date) - 1 - :cnt2) as timestamp)) as smallint);
      if (:dayweek <> 0 and :dayweek <> 6) then
      begin
        select count(dokumpoz.ref)
          from dokumnag
            join dokumpoz on (dokumpoz.dokument = dokumnag.ref)
          where dokumnag.typ = :stype and dokumnag.akcept = 1
            and cast(dokumnag.data as date) = cast(:actudate as date) - 1 - :cnt2
          into :tmpcnt;
          actucnt = :actucnt + :tmpcnt;
        cnt = :cnt + 1;
      end
      cnt2 = :cnt2 + 1;
    end
    cnt = 0;
    cnt3 = :cnt2;
    cnt2 = 0;
    while (:cnt < :days)
    do begin
      dayweek = CAST( extract(weekday from cast((cast(:actudate as date) - 1 - :cnt2 - :cnt3) as timestamp)) as smallint);
      if (:dayweek <> 0 and :dayweek <> 6) then
      begin
        select count(dokumpoz.ref)
          from dokumnag
            join dokumpoz on (dokumpoz.dokument = dokumnag.ref)
          where dokumnag.typ = :stype and dokumnag.akcept = 1
            and cast(dokumnag.data as date) = cast(:actudate as date) - 1 - :cnt2 - :cnt3
          into :tmpcnt;
          lastcnt = :lastcnt + :tmpcnt;
        cnt = :cnt + 1;
      end
      cnt2 = :cnt2 + 1;
    end
    if (:lastcnt * (1 + cast(:increaseparam as numeric(14,2))/100) < :actucnt) then
      increased = 1;
    else
      increased = 0;
  end
end^
SET TERM ; ^
