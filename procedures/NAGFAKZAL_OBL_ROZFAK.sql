--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAKZAL_OBL_ROZFAK(
      FAKTURA integer,
      VAT varchar(5) CHARACTER SET UTF8                           )
   as
declare variable wartnet numeric(14,2);
declare variable wartbru numeric(14,2);
declare variable wartnetzl numeric(14,2);
declare variable wartbruzl numeric(14,2);
declare variable local integer;
declare variable zaliczkowy integer;
begin
  execute procedure CHECK_LOCAL('NAGFAK',:faktura) returning_values :local;
  if(:local = 1) then begin
    select sum(WARTNET),sum(WARTBRU),sum(WARTNETZL),sum(WARTBRUZL)
      from NAGFAKZAL
      where FAKTURA=:faktura and VAT=:vat
      into :wartnet,:wartbru,:wartnetzl,:wartbruzl;
    if(:wartnet is null) then wartnet = 0;
    if(:wartbru is null) then wartbru = 0;
    if(:wartnetzl is null) then wartnetzl = 0;
    if(:wartbruzl is null) then wartbruzl = 0;
    if(exists(select dokument from ROZFAK where DOKUMENT=:faktura and VAT=:vat)) then begin
      update ROZFAK set PESUMWARTNET=:wartnet, PESUMWARTBRU=:wartbru,
                        PESUMWARTNETZL=:wartnetzl, PESUMWARTBRUZL=:wartbruzl,
                        PVESUMWARTNETZL=:wartnetzl, PVESUMWARTBRUZL=:wartbruzl,
                        BLOKADAPRZELICZ=1
        where DOKUMENT=:faktura and VAT=:vat;
    end else begin
      insert into ROZFAK(DOKUMENT,VAT,PESUMWARTNET,PESUMWARTBRU,PESUMWARTNETZL,PESUMWARTBRUZL,PVESUMWARTNETZL,PVESUMWARTBRUZL)
        values (:faktura, :vat, :wartnet, :wartbru, :wartnetzl, :wartbruzl, :wartnetzl, :wartbruzl);
    end
    select sum(WARTNET),sum(WARTBRU),sum(WARTNETZL),sum(WARTBRUZL)
      from NAGFAKZAL
      where FAKTURA=:faktura
      into :wartnet,:wartbru,:wartnetzl,:wartbruzl;
    if(:wartnet is null) then wartnet = 0;
    if(:wartbru is null) then wartbru = 0;
    if(:wartnetzl is null) then wartnetzl = 0;
    if(:wartbruzl is null) then wartbruzl = 0;
    select ZALICZKOWY from NAGFAK where REF=:faktura into :zaliczkowy;
    update NAGFAK set PESUMWARTNET=:wartnet, PESUMWARTBRU=:wartbru,
                      PESUMWARTNETZL=:wartnetzl, PESUMWARTBRUZL=:wartbruzl
      where REF=:faktura;
    if(:zaliczkowy=2) then update NAGFAK set KWOTAZAL=-:wartbru where REF=:faktura;
  end
end^
SET TERM ; ^
