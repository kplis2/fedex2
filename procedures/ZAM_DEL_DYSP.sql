--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ZAM_DEL_DYSP(
      ZAM integer,
      DYSP integer)
   as
declare variable ilreal decimal(14,4);
declare variable toreal decimal(14,4);
declare variable tozreal decimal(14,4);
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable cena decimal (14,2);
declare variable mag char(3);
declare variable rabat decimal(14,2);
declare variable numer integer;
begin
  /* funkja  zmniejsza ilosci zadysponowane na pozycjach dokumentu na podstawie
    wartoci pól ILOSC z pozycji realizacji */
    for select MAGAZYN,KTM,WERSJA,CENACEN,RABAT,ILOSC from POZZAM where ZAMOWIENIE=:dysp
     into :mag, :ktm, :wersja, :cena, :rabat, :ilreal
    do begin
       if(:ilreal > 0) then
       for select NUMER, ILZDYSP from POZZAM where KTM=:ktm AND WERSJA=:wersja AND ZAMOWIENIE=:zam into :numer, :tozreal
       do begin
         if(:ilreal > 0) then begin
           if(:tozreal is null) then tozreal = 0;
           if(:ilreal >  :tozreal) then toreal = tozreal;
           else toreal = ilreal;
           tozreal = tozreal - toreal;
           update POZZAM set ILZDYSP=:tozreal where ZAMOWIENIE=:zam AND NUMER=:numer;
           ilreal = ilreal - toreal;
         end
       end
    end
end^
SET TERM ; ^
