--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_PARAMS_FOR_KTM(
      WERSJAREF integer,
      TYP char(1) CHARACTER SET UTF8                           ,
      ZAKUP char(1) CHARACTER SET UTF8                           )
  returns (
      REF integer,
      EDITTYP integer,
      PARTYP integer,
      PARSYMBOL varchar(40) CHARACTER SET UTF8                           ,
      PARSKROT varchar(40) CHARACTER SET UTF8                           ,
      PARNAZWA varchar(255) CHARACTER SET UTF8                           ,
      PAREDITABLE smallint,
      PARVALUE varchar(255) CHARACTER SET UTF8                           ,
      PARDICTBASE varchar(40) CHARACTER SET UTF8                           ,
      PARDICTTABLE varchar(40) CHARACTER SET UTF8                           ,
      PARDICTFIELD varchar(40) CHARACTER SET UTF8                           ,
      PARDICTINDEX varchar(40) CHARACTER SET UTF8                           ,
      PARDICTPREFIX varchar(80) CHARACTER SET UTF8                           ,
      PARDICTFILTER varchar(80) CHARACTER SET UTF8                           ,
      PARKEYFIELDS varchar(40) CHARACTER SET UTF8                           )
   as
declare variable ok integer;
declare variable slownik integer;
declare variable symbol varchar(40);
declare variable parredzamspr integer;
declare variable parredzamzak integer;
declare variable parredfakspr integer;
declare variable parredfakzak integer;
declare variable parreddokwyd integer;
declare variable parreddokprz integer;
begin
/* EDITTYP: edit=31, number=26, date=25 */
  ref = 1;
  for select DEFCECHY.TYP,DEFCECHY.PARTYP,DEFCECHY.PARSYMBOL,DEFCECHY.PARSKROT,DEFCECHY.NAZWA,
             DEFCECHY.PARREDZAMSPR,DEFCECHY.PARREDZAMZAK,
             DEFCECHY.PARREDFAKSPR,DEFCECHY.PARREDFAKZAK,
             DEFCECHY.PARREDDOKWYD,DEFCECHY.PARREDDOKPRZ,
             ATRYBUTY.WARTOSC,DEFCECHY.SLOW,DEFCECHY.SYMBOL,
             DEFCECHY.PARDICTBASE,DEFCECHY.PARDICTTABLE,
             DEFCECHY.PARDICTFIELD,DEFCECHY.PARKEYFIELDS,
             DEFCECHY.PARDICTINDEX,DEFCECHY.PARDICTPREFIX,
             DEFCECHY.PARDICTFILTER
  from ATRYBUTY
  left join DEFCECHY on (DEFCECHY.SYMBOL=ATRYBUTY.CECHA)
  where ATRYBUTY.WERSJAREF=:wersjaref and DEFCECHY.PARTYP>0
  order by DEFCECHY.LP
  into :edittyp,:partyp,:parsymbol,:parskrot,:parnazwa,
       :parredzamspr,:parredzamzak,
       :parredfakspr,:parredfakzak,
       :parreddokwyd,:parreddokprz,
       :parvalue,:slownik,:symbol,
       :pardictbase,:pardicttable,
       :pardictfield,:parkeyfields,
       :pardictindex,:pardictprefix,
       :pardictfilter
  do begin
    ok=0;
    if(:typ='Z' and :zakup='0' and :parredzamspr=1) then ok=1;
    if(:typ='Z' and :zakup='1' and :parredzamzak=1) then ok=1;
    if(:typ='F' and :zakup='0' and :parredfakspr=1) then ok=1;
    if(:typ='F' and :zakup='1' and :parredfakzak=1) then ok=1;
    if(:typ='M' and :zakup='0' and :parreddokwyd=1) then ok=1;
    if(:typ='M' and :zakup='1' and :parreddokprz=1) then ok=1;
    if(:ok=1) then begin
      if(:edittyp=0 or :edittyp=1) then edittyp=26;
      else if(:edittyp=2 or :edittyp=5) then edittyp=31;
      else if(:edittyp=3 or :edittyp=4) then edittyp=25;
      pareditable = 1;
      if(:slownik=1) then begin
        pardictbase = 'esystem';
        pardicttable = 'DEFCECHW';
        if(:edittyp=31) then pardictfield = 'WARTSTR';
        if(:edittyp=26) then pardictfield = 'WARTNUM';
        if(:edittyp=25) then pardictfield = 'WARTDATE';
        pardictindex = pardictfield;
        pardictprefix = '';
        pardictfilter = 'CECHA='''||:symbol||'''';
        parkeyfields = pardictfield;
      end else begin
        if(pardictbase is null) then pardictbase = '';
        if(pardicttable is null) then pardicttable = '';
        if(pardictfield is null) then pardictfield = '';
        if(pardictindex is null) then pardictindex = '';
        if(pardictprefix is null) then pardictprefix = '';
        if(pardictfilter is null) then pardictfilter = '';
        if(parkeyfields is null) then parkeyfields = '';
        if(:parkeyfields='') then parkeyfields = :pardictfield;
      end
      suspend;
      ref = :ref + 1;
    end
  end
end^
SET TERM ; ^
