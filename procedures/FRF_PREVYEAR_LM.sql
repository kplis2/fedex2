--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_PREVYEAR_LM(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      COL integer)
  returns (
      AMOUNT numeric(15,2))
   as
declare variable frvhdr integer;
declare variable frpsn integer;
declare variable frcol integer;
declare variable cacheparams varchar(255);
declare variable ddparams varchar(255);
declare variable prevyear varchar(4);
declare variable prevmonth varchar(2);
declare variable prevperiod varchar(6);
declare variable prevfrvhdr integer;
declare variable company smallint;
declare variable frversion integer;
begin
  if (frvpsn = 0) then exit;
  -- wartosc z poprzedniego roku
  select frvhdr, frpsn, frcol
    from frvpsns
    where ref = :frvpsn
    into :frvhdr, :frpsn, :frcol;

  select frversion, company
    from frvhdrs
    where ref = :frvhdr
    into :frversion, :company;
  prevyear = cast(cast(substring(period from 1 for 4) as integer) - 1 as varchar(4));
  select max(bkp.id)
    from bkperiods bkp
    where bkp.yearid = cast(:prevyear as integer) and bkp.ptype = 1 and bkp.company = :company
  into :prevperiod;

  select first 1 ref
    from frvhdrs
    where frhdr = (select frhdr from frvhdrs where ref = :frvhdr) and period = :prevperiod
      and company = :company
    order by regdate desc
    into :prevfrvhdr;

  cacheparams = period;

  select amount
    from frvpsns
    where frpsn = :frpsn and frvhdr = :prevfrvhdr and col = :col
    into :amount;

  amount = coalesce(amount,0);
  insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
    values (:frvhdr, :frpsn, :frcol, 'PREVYEAR', :cacheparams, :ddparams, :amount, :frversion);

  suspend;
end^
SET TERM ; ^
