--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GETPMPOSITIONSARCHIV(
      MAINELEMENT integer,
      PLANOPER integer)
  returns (
      REF integer,
      PMELEMENT integer,
      PMPLAN integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4),
      POSITIONTYPE char(1) CHARACTER SET UTF8                           ,
      CALCPRICE numeric(14,2),
      CALCVAL numeric(14,2),
      BUDPRICE numeric(14,2),
      BUDVAL numeric(14,2),
      BUDVALR numeric(14,2),
      INTQ numeric(14,4),
      OFEQ numeric(14,4),
      EXTQ numeric(14,4),
      EXTVAL numeric(14,2),
      REALQ numeric(14,4),
      REALVAL numeric(14,2),
      USEDQ numeric(14,4),
      USEDPRICE numeric(14,2),
      USEDVAL numeric(14,2),
      READRIGHTS varchar(255) CHARACTER SET UTF8                           ,
      WRITERIGHTS varchar(255) CHARACTER SET UTF8                           ,
      READRIGHTSGROUP varchar(255) CHARACTER SET UTF8                           ,
      WRITERIGHTSGROUP varchar(255) CHARACTER SET UTF8                           ,
      UNIT varchar(5) CHARACTER SET UTF8                           ,
      VERSION varchar(20) CHARACTER SET UTF8                           ,
      VERSIONNUM integer,
      MAINREF integer)
   as
declare variable pozycja integer;
declare variable wersjaplanu integer;
begin
  select versionnum from pmplans where ref=:planoper into :wersjaplanu;
  for select ref from pmpositions
    where ref=mainref and pmelement=:mainelement
    into :pozycja
  do begin
    select first 1 REF, PMELEMENT, PMPLAN, NAME, SYMBOL, KTM, DESCRIPT, QUANTITY,
      POSITIONTYPE, CALCPRICE, CALCVAL, BUDPRICE, BUDVAL, BUDVALR, INTQ, OFEQ,
      EXTQ, EXTVAL, REALQ, REALVAL, USEDQ, USEDPRICE, USEDVAL, READRIGHTS,
      WRITERIGHTS, READRIGHTSGROUP, WRITERIGHTSGROUP, UNIT, VERSION, VERSIONNUM,
      MAINREF from pmpositions
      where mainref=:pozycja
        and versionnum<=:wersjaplanu
      order by VERSIONNUM desc
    into :REF, :PMELEMENT, :PMPLAN, :NAME, :SYMBOL, :KTM, :DESCRIPT, :QUANTITY,
        :POSITIONTYPE, :CALCPRICE, :CALCVAL, :BUDPRICE, :BUDVAL, :BUDVALR, :INTQ,
        :OFEQ, :EXTQ, :EXTVAL, :REALQ, :REALVAL, :USEDQ, :USEDPRICE, :USEDVAL,
        :READRIGHTS, :WRITERIGHTS, :READRIGHTSGROUP, :WRITERIGHTSGROUP, :UNIT,
        :VERSION, :VERSIONNUM, :MAINREF;
    suspend;
  end
end^
SET TERM ; ^
