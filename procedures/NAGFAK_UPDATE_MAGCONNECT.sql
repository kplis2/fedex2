--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_UPDATE_MAGCONNECT(
      FAKTURA integer)
   as
declare variable cnt integer;
declare variable oldfak integer;
declare variable newsymbol varchar(255);
declare variable symbol varchar(20);
declare variable newmag integer;
begin
  cnt = 0;
  newsymbol = '';
  newmag = null;
  -- sprawdz najpierw powiazania naglowkowe
  for select REF,SYMBOL from DOKUMNAG where FAKTURA = :faktura into :newmag,:symbol
  do begin
    cnt = :cnt + 1;
    if (coalesce(char_length(:newsymbol),0) < 200)then begin -- [DG] XXX ZG119346
      if(:newsymbol <> '') then newsymbol = :newsymbol || ';';
      newsymbol = :newsymbol || :symbol;
    end
  end
  -- nastepnie znajdz wszystkie dodatkowe dokumenty magazynowe do faktury dzieki powiazaniu przez pozycje
  -- dla koreslenia symboli dok mag
  for select distinct DOKUMNAG.SYMBOL
    from POZFAK
    left join DOKUMPOZ on (DOKUMPOZ.FROMPOZFAK=POZFAK.REF)
    left join DOKUMNAG on (DOKUMNAG.ref=DOKUMPOZ.dokument)
    where POZFAK.dokument=:faktura and DOKUMPOZ.dokument is not null
    into :symbol
  do begin
    if(position(:symbol in :newsymbol)=0) then begin
      if (coalesce(char_length(:newsymbol),0) < 200)then begin -- [DG] XXX ZG119346
        if(:newsymbol <> '') then newsymbol = :newsymbol || ';';
        newsymbol = :newsymbol || :symbol;
      end
    end
  end
  -- nanies powiazanie na nagfaka
  if(:cnt>1) then begin
    update NAGFAK set REFDOKM = 0,SYMBDOKM = :newsymbol where REF=:faktura and(REFDOKM <> 0 or (REFDOKM is null ) or (SYMBDOKM <> :newsymbol) or (SYMBDOKM is null));
  end else if(:cnt = 1) then begin
    update NAGFAK set REFDOKM = :newmag,SYMBDOKM = :newsymbol where REF=:faktura and(REFDOKM <> :newmag or (REFDOKM is null ) or (SYMBDOKM <> :newsymbol) or (SYMBDOKM is null));
  end else begin
    update NAGFAK set REFDOKM = NULL, SYMBDOKM = :newsymbol where REF=:faktura and (REFDOKM is not null or (SYMBDOKM is null) or (SYMBDOKM <> :newsymbol));
  end
end^
SET TERM ; ^
