--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TOWORDS(
      DOUB numeric(14,2),
      SKROT smallint,
      GR smallint,
      LANGUAGE smallint)
  returns (
      STRINGNAME varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable out_ varchar(1024);
declare variable out1_ varchar(1024);
declare variable in_ varchar(1024);
declare variable a1 varchar(1024);
declare variable a2 varchar(1024);
declare variable a3 varchar(1024);

declare variable x integer;
declare variable y integer;
declare variable a integer;
declare variable len integer;
declare variable l integer;
declare variable pom integer;
declare variable czygrosze smallint;
begin
 czygrosze = 0;
 in_ = :doub;
 len = position('.' in in_)-1;
 if (len = -1) then len = coalesce(char_length(in_),0); -- [DG] XXX ZG119346
 y = len;
 if (skrot = 1) then begin
   czygrosze = 1;
   execute procedure towords_f_skrot(in_,language) returning_values out_;

 end

 if (czygrosze = 0) then begin
   if (doub = 0.0) then out_ = 'zero';
   else begin

       -- Reszta dla 'liczba mod 3' - potrzebne dla prawidlowego rozroznienia milionow, tysiecy itp.
       -- np. liczba=34209 => pobiera 34, bo jest to liczba tysiecy.
     x =  mod(:y,3);
       -- Dlatego bo dla kazdej "trojki" jest inna nazwa (np. dla tysiecy x=2, milionow x=3, miliardow x=4.(Potrzebne dla f. dopisz_wielkosc )
     y = :y / 3;


     l = 0;
     pom = 1;
     out_ = '';
     if (x <> 0) then begin
       if (x = 2) then begin
           -- Przerobienie kodow ASCII na liczby
                if (pom = 1) then l = 10*(cast (substring(in_ from 1 for 1) as integer)) + (cast (substring(in_ from 2 for 1) as integer));
           else if (pom = 2) then l = 10*(cast (substring(in_ from 2 for 1) as integer)) + (cast (substring(in_ from 3 for 1) as integer));
           else if (pom = 3) then l = 10*(cast (substring(in_ from 3 for 1) as integer)) + (cast (substring(in_ from 4 for 1) as integer));
           else if (pom = 4) then l = 10*(cast (substring(in_ from 4 for 1) as integer)) + (cast (substring(in_ from 5 for 1) as integer));
           else if (pom = 5) then l = 10*(cast (substring(in_ from 5 for 1) as integer)) + (cast (substring(in_ from 6 for 1) as integer));
           else if (pom = 6) then l = 10*(cast (substring(in_ from 6 for 1) as integer)) + (cast (substring(in_ from 7 for 1) as integer));
           else if (pom = 7) then l = 10*(cast (substring(in_ from 7 for 1) as integer)) + (cast (substring(in_ from 8 for 1) as integer));
           else if (pom = 8) then l = 10*(cast (substring(in_ from 8 for 1) as integer)) + (cast (substring(in_ from 9 for 1) as integer));
           else if (pom = 9) then l = 10*(cast (substring(in_ from 9 for 1) as integer)) + (cast (substring(in_ from 10 for 1) as integer));
           else if (pom = 10) then l = 10*(cast (substring(in_ from 10 for 1) as integer)) + (cast (substring(in_ from 11 for 1) as integer));
           -- Zwiekszenie wskaznika pomocniczego o liczbe odczytanych cyfr z lancucha
         pom = :pom + 2;
       end
       if (x = 1) then begin
              if (pom = 1) then l = (cast (substring(in_ from 1 for 1) as integer));
         else if (pom = 2) then l = (cast (substring(in_ from 2 for 1) as integer));
         else if (pom = 3) then l = (cast (substring(in_ from 3 for 1) as integer));
         else if (pom = 4) then l = (cast (substring(in_ from 4 for 1) as integer));
         else if (pom = 5) then l = (cast (substring(in_ from 5 for 1) as integer));
         else if (pom = 6) then l = (cast (substring(in_ from 6 for 1) as integer));
         else if (pom = 7) then l = (cast (substring(in_ from 7 for 1) as integer));
         else if (pom = 8) then l = (cast (substring(in_ from 8 for 1) as integer));
         else if (pom = 9) then l = (cast (substring(in_ from 9 for 1) as integer));
         else if (pom = 10) then l = (cast (substring(in_ from 10 for 1) as integer));
         pom = :pom + 1;
       end
         -- Bo rzad zostal spisany tylko dla "pelnych trojek"(o jeden za malo)
       y = :y + 1;

       execute procedure towords_podaj_slownie(:l, :out_, :language) returning_values a, out1_;

       out_ = out1_;

       execute procedure towords_dopisz_wielkosc(:out_,:a,:y,:language) returning_values out1_;

       out_ = out1_;
       y = :y - 1;
     end
      -- Petla dla pelnych "trojek"


     while (:len >= :pom)
     do begin
       if (pom = 1) then begin
         a1 = 100 * (cast (substring(in_ from 1 for 1) as integer));
         a2 =  10 * (cast (substring(in_ from 2 for 1) as integer));
         a3 =   1 * (cast (substring(in_ from 3 for 1) as integer));
       end else
       if (pom = 2) then begin
         a1 = 100 * (cast (substring(in_ from 2 for 1) as integer));
         a2 =  10 * (cast (substring(in_ from 3 for 1) as integer));
         a3 =   1 * (cast (substring(in_ from 4 for 1) as integer));
       end else
       if (pom = 3) then begin
         a1 = 100 * (cast (substring(in_ from 3 for 1) as integer));
         a2 =  10 * (cast (substring(in_ from 4 for 1) as integer));
         a3 =   1 * (cast (substring(in_ from 5 for 1) as integer));
       end else
       if (pom = 4) then begin
         a1 = 100 * (cast (substring(in_ from 4 for 1) as integer));
         a2 =  10 * (cast (substring(in_ from 5 for 1) as integer));
         a3 =   1 * (cast (substring(in_ from 6 for 1) as integer));
       end else
       if (pom = 5) then begin
         a1 = 100 * (cast (substring(in_ from 5 for 1) as integer));
         a2 =  10 * (cast (substring(in_ from 6 for 1) as integer));
         a3 =   1 * (cast (substring(in_ from 7 for 1) as integer));
       end else
       if (pom = 6) then begin
         a1 = 100 * (cast (substring(in_ from 6 for 1) as integer));
         a2 =  10 * (cast (substring(in_ from 7 for 1) as integer));
         a3 =   1 * (cast (substring(in_ from 8 for 1) as integer));
       end else
       if (pom = 7) then begin
         a1 = 100 * (cast (substring(in_ from 7 for 1) as integer));
         a2 =  10 * (cast (substring(in_ from 8 for 1) as integer));
         a3 =   1 * (cast (substring(in_ from 9 for 1) as integer));
       end else
       if (pom = 8) then begin
         a1 = 100 * (cast (substring(in_ from 8 for 1) as integer));
         a2 =  10 * (cast (substring(in_ from 9 for 1) as integer));
         a3 =   1 * (cast (substring(in_ from 10 for 1) as integer));
       end else
       if (pom = 9) then begin
         a1 = 100 * (cast (substring(in_ from 9 for 1) as integer));
         a2 =  10 * (cast (substring(in_ from 10 for 1) as integer));
         a3 =   1 * (cast (substring(in_ from 11 for 1) as integer));
       end else
       if (pom = 10) then begin
         a1 = 100 * (cast (substring(in_ from 10 for 1) as integer));
         a2 =  10 * (cast (substring(in_ from 11 for 1) as integer));
         a3 =   1 * (cast (substring(in_ from 12 for 1) as integer));
       end

       l = cast(a1 as integer) + cast(a2 as integer) + cast(a3 as integer);
       execute procedure towords_podaj_slownie(:l, :out_, :language) returning_values a, out1_;
       out_ = out1_;

       execute procedure towords_dopisz_wielkosc(:out_, :a, :y, :language) returning_values out1_;
       out_ = out1_;
       pom = :pom + 3 ;
       y = :y - 1;

     end

   end
   czygrosze = 1;
 end
 if (czygrosze = 1) then  begin
-- label GROSZE:
   if (gr = 1) then begin
     x = position(',' in in_);
     if (x = 0) then
       x = position('.' in  in_);
--     exception test_break :x;
     out_ = :out_ || '  ';
          if (x = 1) then out_ = :out_ || substring(in_ from 2 for 2);
     else if (x = 2) then out_ = :out_ || substring(in_ from 3 for 2);
     else if (x = 3) then out_ = :out_ || substring(in_ from 4 for 2);
     else if (x = 4) then out_ = :out_ || substring(in_ from 5 for 2);
     else if (x = 5) then out_ = :out_ || substring(in_ from 6 for 2);
     else if (x = 6) then out_ = :out_ || substring(in_ from 7 for 2);
     else if (x = 7) then out_ = :out_ || substring(in_ from 8 for 2);
     else if (x = 8) then out_ = :out_ || substring(in_ from 9 for 2);
     else if (x = 9) then out_ = :out_ || substring(in_ from 10 for 2);
     else if (x = 10) then out_ = :out_ || substring(in_ from 11 for 2);
     out_ = :out_ || '/100';
   end
 end
 stringname = out_;
 suspend;
end^
SET TERM ; ^
