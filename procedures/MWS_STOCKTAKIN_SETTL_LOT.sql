--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_STOCKTAKIN_SETTL_LOT(
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      WH varchar(3) CHARACTER SET UTF8                           )
  returns (
      LOT integer)
   as
begin
  select first 1 c.dostawa
    from stanycen c
    where c.wersjaref = :vers and c.ilosc > 0 and c.magazyn = :wh
    order by c.datafifo desc
    into lot;
  if (coalesce(:lot,0) = 0) then
  begin
    select first 1 c.dostawa
      from stanycen c
      where c.wersjaref = :vers and c.magazyn = :wh
      order by c.datafifo desc
      into lot;
  end
  if (coalesce(:lot,0) = 0) then
  begin
    select first 1 c.dostawa
      from stanycen c
      where c.wersjaref = :vers and c.ilosc > 0
      order by c.datafifo desc
      into lot;
  end
  if (coalesce(:lot,0) = 0) then
  begin
    select first 1 c.dostawa
      from stanycen c
      where c.wersjaref = :vers
      order by c.datafifo desc
      into lot;
  end
  if (coalesce(:lot,0) = 0) then
  begin
    select first 1 p.dostawa
      from dokumnag d
        left join defdokum f on (f.symbol = d.typ)
        left join dokumpoz p on (p.dokument = d.ref)
      where d.wydania = 0 and f.zewn = 1 and f.koryg = 0
        and d.magazyn = :wh and p.wersjaref = :vers and p.dostawa is not null
      order by d.data desc
      into lot;
  end
  if (coalesce(:lot,0) = 0) then
  begin
    select first 1 p.dostawa
      from dokumnag d
        left join defdokum f on (f.symbol = d.typ)
        left join dokumpoz p on (p.dokument = d.ref)
      where d.wydania = 0 and f.zewn = 1 and f.koryg = 0
        and p.wersjaref = :vers and p.dostawa is not null
      order by d.data desc
      into lot;
  end
end^
SET TERM ; ^
