--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_FRVAL(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      FRSYMBOL varchar(20) CHARACTER SET UTF8                           ,
      FRROW varchar(15) CHARACTER SET UTF8                           ,
      FRCOLUMN integer)
  returns (
      AMOUNT numeric(15,2))
   as
declare variable company integer;
  declare variable frvhdr integer;
  declare variable frpsn integer;
  declare variable frcol integer;
  declare variable cacheparams varchar(255);
  declare variable ddparams varchar(255);

  declare variable psnref integer;
  declare variable hdrref integer;
  declare variable regdate timestamp;
  declare variable frversion integer;
begin
  -- wartosc innego z sprawozdania
  if (frvpsn = 0) then exit;
  select frvhdr, frpsn, frcol
    from frvpsns
    where ref = :frvpsn
    into :frvhdr, :frpsn, :frcol;

  select company, frversion
    from frvhdrs
    where ref = :frvhdr
    into :company, :frversion;

  cacheparams = period || ';' || frsymbol || ';' || frrow || ';' || cast(frcolumn as varchar(3))||';'||company;
  ddparams = frsymbol || ';' || frrow || ';' || cast(frcolumn as varchar(3));

  if (not exists (select ref from frvdrilldown where functionname = 'FRVAL' and cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr)) then
  begin
  select FRP.ref
    from frpsns FRP
    where FRP.frhdr = :frsymbol and FRP.symbol = :frrow
    into :psnref;

  select max(FRVH.ref), max(FRVH.regdate)
    from frvhdrs FRVH
    where FRVH.frhdr = :frsymbol and FRVH.period = :period and company = :company
    into :hdrref, :regdate;

  select coalesce(FRVP.amount, 0)
    from frvpsns FRVP
    where FRVP.col = :frcolumn and FRVP.frpsn = :psnref and FRVP.frvhdr = :hdrref
    into :amount;

  ddparams = ddparams  || ';' || regdate;
  end else
    select first 1 fvalue from frvdrilldown where functionname = 'FRVAL' and cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr
      into :amount;

  amount = coalesce(amount,0);
  insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
     values (:frvhdr, :frpsn, :frcol, 'FRVAL', :cacheparams, :ddparams, :amount, :frversion);
  suspend;
end^
SET TERM ; ^
