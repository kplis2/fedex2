--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_INT_EXP_UPDATE_LISTYWYSD(
      LISTYWYSD_REF type of column LISTYWYSD.REF,
      POBRANIE_BRUTTO type of column LISTYWYSD.POBRANIE)
   as
begin
  update listywysd lwd
    set pobranie = :pobranie_brutto
    where lwd.ref = :listywysd_ref;
end^
SET TERM ; ^
