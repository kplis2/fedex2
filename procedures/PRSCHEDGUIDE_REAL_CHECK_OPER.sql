--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDE_REAL_CHECK_OPER(
      PRSCHEDGUIDE integer,
      PRSCHEDOPER integer,
      OPERNUMBER integer)
  returns (
      NUMBER integer)
   as
begin
  for
    select o1.number                 --poprzednie operacje nieaktywne lub nie raportowalne z materialem
      from prschedopers o1
      join prschedguidespos pg1 on (pg1.prschedguide = o1.guide and pg1.prschedopernumber = o1.number)
      where o1.guide = :prschedguide
        and o1.number < :opernumber
        and (o1.activ = 0 or o1.reported < 2)
        and not exists (select  first 1 1
                          from prschedopers po1, prsched_calc_next_oper(o1.ref, null, 0) p1
                          where p1.activ = 1
                            and p1.reported > 0
                            and po1.ref = p1.RPRSCHEDOPER
                            and po1.number < :opernumber)
      into :number
  do
    suspend;

  number = :opernumber;
  suspend;

  for
    select o2.number               --nast .operacje nieaktywne lub nie raportowalne z materialem i brak po nich operacj aktywnej i rap.
      from prschedopers o2
      join prschedguidespos pg2 on (pg2.prschedguide = o2.guide and pg2.prschedopernumber = o2.number)
      where o2.guide = :prschedguide
        and o2.number > :opernumber
        and (o2.activ = 0 or o2.reported = 0)
        and not exists (select  first 1 1
                          from prschedopers po2, prsched_calc_next_oper(o2.ref, null, 0) p2
                          where p2.activ = 1
                            and p2.reported > 0
                            and po2.ref = p2.RPRSCHEDOPER)
                            and not exists(select  first 1 1
                                             from prschedopers po3, prsched_calc_next_oper(:prschedoper, null, 0) p3
                                             where p3.activ = 1
                                               and p3.reported > 0
                                               and po3.ref = p3.RPRSCHEDOPER)
    into :number
  do
    suspend;
end^
SET TERM ; ^
