--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_OBLICZPOBRANIE(
      DOKUMSPED integer)
   as
DECLARE VARIABLE POBRANIE NUMERIC(14,2);
declare variable rozliczrozval numeric(14,2);
begin
  if (dokumsped is not null) then
  begin
    select sum(nagfak.DOZAPLATY)
      from NAGFAK join PLATNOSCI on (NAGFAK.sposzap = PLATNOSCI.ref)
    where listywysd=:dokumsped and nagfak.anulowanie = 0 and PLATNOSCI.naspedytora = 1
    into :pobranie;
    if(:pobranie is null) then pobranie = 0;
    select LISTYWYSD.rozrachrozval from LISTYWYSD where REF=:dokumsped into :rozliczrozval;
    if(:rozliczrozval is null or (:rozliczrozval = 0)) then begin
      rozliczrozval = 0;
    end
    update LISTYWYSD set listywysd.pobranie = :pobranie - :rozliczrozval where ref=:dokumsped and listywysd.akcept < 2;
  end
end^
SET TERM ; ^
