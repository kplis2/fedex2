--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_VAT27_V2_10E_EXP(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(8191) CHARACTER SET UTF8                           )
   as
--zmianna pomocnicza
declare variable isdeclaration integer;
declare variable def varchar(20);
declare variable edeclddef integer;
declare variable code varchar(10);
declare variable pfield varchar(10);
declare variable pfieldsymbol type of column edeclpos.fieldsymbol;
declare variable psubfieldsymbol type of column edeclpos.fieldsymbol;
declare variable fieldprefixendchar char_1;
declare variable systemcode varchar(20);
declare variable obligationkind varchar(20);
declare variable schemaver varchar(10);
declare variable symbol varchar(10);
declare variable variant varchar(10);
declare variable pvalue varchar(255);
declare variable tmpval1 varchar(255);
declare variable tmpval2 varchar(255);
declare variable tmp smallint;
declare variable curr_p integer;
declare variable correction smallint;
declare variable enclosureowner integer;
declare variable zaltyp varchar(20);
declare variable iszal smallint;
declare variable parent_lvl_0 integer;
declare variable parent_lvl_1 integer;
declare variable parent_lvl_2 integer;
declare variable parent_lvl_3 integer;
begin
  --Sprawdzamy czy dana deklaracja istnieje do eksportu
  select first 1 1
    from edeclarations e
    where e.ref = :oref
  into :isDeclaration;

  if(isDeclaration is null) then
    exception universal 'Niepoprawny identyfikator e-deklaracji';

  --Pobieranie definicji
  select e.edecldef, ed.code, ed.systemcode, ed.obligationkind, ed.schemaver, ed.symbol, ed.variant, ed.ref
    from edeclarations e
      join edecldefs ed on (ed.ref = e.edecldef)
    where e.ref = :oref
  into :def, :code, :systemcode, :obligationkind, :schemaver, :symbol, :variant, :edeclddef;

  --Generujemy naglowek pliku XML
  id = 0;
  name = 'Deklaracja';
  parent = null;
  params = 'xmlns="http://crd.gov.pl/wzor/2017/01/11/3844/" xmlns:def="http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/"';
  val = null;
  suspend;

  correction = 0;
  parent_lvl_0 = 0;

  --Rozpoczynamy naglowek e-deklaracji
  id = :id + 1; --1
  name = 'Naglowek';
  parent = parent_lvl_0;
  params = null;
  val  = null;
  parent_lvl_1 = :id;
  suspend;
  --cialo naglowka

    --KodFormularza
    /* <KodFormularza
         kodSystemowy= "VAT-27 (2)"
         kodPodatku= "VAT"
         rodzajZobowiazania= "Z"
         wersjaSchemy= "1-0E">
           VAT-27
       </KodFormularza>
    */
    id = id+1;
    name = 'KodFormularza';
    parent = parent_lvl_1;
    params = '';
    if(code is not null) then
    params = params||'kodPodatku= "'||code||'" ';
    if(systemcode is not null) then
    params = params||'kodSystemowy= "'||systemcode||'" ';
    if(obligationkind is not null) then
    params = params||'rodzajZobowiazania= "'||obligationkind||'" ';
    if(schemaver is not null) then
    params = params||'wersjaSchemy= "'||schemaver||'"';
    val = symbol;
    suspend;
    
    --WariantFormularza
    /* <WariantFormularza>
         2
       </WariantFormularza>
    */
    id = id+1;
    name= 'WariantFormularza';
    parent = parent_lvl_1;
    params = null;
    val = variant;
    suspend;
    
    --CelZlozenia
    /* <CelZlozenia
         poz= "P_7">
           1
       </CelZlozenia>
    */
    id = id+1;
    name= 'CelZlozenia';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.fieldsymbol = 'K7'
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params = 'poz= "P_7"';
      val = :pvalue;
      suspend;
    end

    if(:pvalue = 1) then
      correction = 1;
    pvalue = null;

    --Rok
    /* <Rok>
         2017
       </Rok>
    */
    id = id+1;
    name= 'Rok';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.fieldsymbol = 'K5'
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params = null;
      val = :pvalue;
      suspend;
    end
    pvalue = null;

    --Miesiac
    /* <Miesiac>
         2
       </Miesiac>
    */
    id = id+1;
    name= 'Miesiac';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.fieldsymbol = 'K4'
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params = null;
      val = :pvalue;
      suspend;
    end
    pvalue = null;

    --KodUrzedu
    /* <KodUrzedu>
         0402
       </KodUrzedu>
    */
    id = id+1;
    name= 'KodUrzedu';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.fieldsymbol = 'USP'
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params = null;
      val = :pvalue;
      suspend;
    end
    --Koniec Naglowka
    
  --Podmiot
  /* <Podmiot1 rola= "Podatnik">
       <!--You have a CHOICE of the next 2 items at this level-->
       <def:OsobaFizyczna>
         ...
       </def:OsobaFizyczna>
       <def:OsobaNiefizyczna>
         ...
       </def:OsobaNiefizyczna>
     </Podmiot1>
  */
  id = id+1;
  name= 'Podmiot1';
  parent = parent_lvl_0;
  params = 'rola= "Podatnik"';
  val = null;
  parent_lvl_1 = :id;
  suspend;
    pvalue = null;
    select epos.pvalue
        from edeclpos epos
        where epos.edeclaration = :oref and epos.fieldsymbol = 'K8'
      into :pvalue;
  if(:pvalue = 2) then
  begin
    --OsobaFizyczna
    /* <def:OsobaFizyczna>
         <def:NIP>string</def:NIP>
         <def:ImiePierwsze>token</def:ImiePierwsze>
         <def:Nazwisko>token</def:Nazwisko>
         <def:DataUrodzenia>1900-01-01+01:00</def:DataUrodzenia>
       </def:OsobaFizyczna>
    */
    id = id+1;
    name= 'def:OsobaFizyczna';
    parent = parent_lvl_1;
    params = null;
    val = null;
    parent_lvl_2 = :id;
    suspend;
      pvalue = null;
      --NIP
      id = id+1;
      name= 'def:NIP';
      parent = parent_lvl_2;
      select REPLACE(epos.pvalue, '-', '')
        from edeclpos epos
        where epos.edeclaration = :oref and epos.fieldsymbol = 'K1'
      into :pvalue;
      params = null;
      val = :pvalue;
      suspend;
      pvalue = null;
      --ImiePierwsze && Nazwisko && DataUrodzenia
      tmp = 0;
      select epos.pvalue
      from edeclpos epos
        where epos.edeclaration = :oref and epos.fieldsymbol = 'K9'
      into :pvalue;
      for select outstring
        from parse_lines(:pvalue, ',')
      into :tmpval2
      do begin
      if(tmp = 0) then
      begin
        tmpval1 = :tmpval2;
      end
      if(tmp = 1) then
      begin
      --PelnaNazwa
        id = id+1;
        name= 'def:ImiePierwsze';
        parent = parent_lvl_2;
        val = tmpval2;
        params = null;
        suspend;

        --Nazwisko
        id = id+1;
        name= 'def:Nazwisko';
        parent = parent_lvl_2;
        val = tmpval1;
        params = null;
        suspend;
      end
      if(tmp = 2) then
      begin
        --DataUrodzenia
        id = id+1;
        name= 'def:DataUrodzenia';
        parent = parent_lvl_2;
        val = tmpval2;
        params = null;
        suspend;
      end
      tmp = :tmp + 1;
      end
    end
  else if(:pvalue = 1) then
    begin
    --OsobaNiefizyczna
    /* <def:OsobaNiefizyczna>
         <def:NIP>string</def:NIP>
         <def:PelnaNazwa>token</def:PelnaNazwa>
         <!--Optional:-->
         <def:REGON>string</def:REGON>
       </def:OsobaNiefizyczna>
    */
    id = id+1;
    name= 'def:OsobaNiefizyczna';
    parent = parent_lvl_1;
    params = null;
    val = null;
    parent_lvl_2 = :id;
    suspend;
      pvalue = null;
      --NIP
      id = id+1;
      name= 'def:NIP';
      parent = parent_lvl_2;
      select REPLACE(epos.pvalue, '-', '')
        from edeclpos epos
        where epos.edeclaration = :oref and epos.fieldsymbol = 'K1'
      into :pvalue;
      params = null;
      val = :pvalue;
      suspend;
      pvalue = null;
      --PelnaNazwa && REGON
      tmp = 0;
      select epos.pvalue
      from edeclpos epos
        where epos.edeclaration = :oref and epos.fieldsymbol = 'K9'
      into :pvalue;
      for select outstring
        from parse_lines(:pvalue, ',')
      into :tmpval2
      do begin
      if(tmp = 0)then
      begin
        --PelnaNazwa
        id = id+1;
        name= 'def:PelnaNazwa';
        parent = parent_lvl_2;
        val = tmpval2;
        params = null;
        suspend;
      end
      else if(tmp = 1) then
      begin
        --REGON
        id = id+1;
        name= 'def:REGON';
        parent = parent_lvl_2;
        val = trim(tmpval2);  -- zbedne spacje
        params = null;
        suspend;
      end
      tmp = :tmp + 1;
    end
  end

  --PozycjeSzczegolowe
  /*<PozycjeSzczegolowe>
    <!--Zero or more repetitio-->
      <Grupa_C typ= "G">
        ...
      </Grupa_C>
      <P_10>1000.000000000000</P_10>
      <!--Zero or more repetitio-->
      <Grupa_D typ= "G">
        ...
      </Grupa_D>
      <P_11>1000.000000000000</P_11>
      <!--Optional:-->
      <P_14>token</P_14>
      <!--Optional:-->
      <P_15>2017-01-01+01:00</P_15>
    </PozycjeSzczegolowe>
  */
  id = id+1;
  name= 'PozycjeSzczegolowe';
  parent = parent_lvl_0;
  params = null;
  val = null;
  parent_lvl_1 = :id;
  suspend;
  pvalue = null;

  fieldprefixendchar = '.';

  for select epos.pvalue, epos.fieldsymbol
    from edeclpos epos
    where epos.edeclaration = :oref
      and epos.fieldsymbol starting with 'CPOZ_'
    order by epos.field
  into :pvalue, :pfieldsymbol
  do begin
    psubfieldsymbol = substring(:pfieldsymbol
      from position(:fieldprefixendchar,:pfieldsymbol) + 1);

    if (:psubfieldsymbol = 'NUMER') then
    begin  
      -- Grupa C
      /* <Grupa_C typ= "G">
           ...
         </Grupa_C>
      */
      id = id+1;
      name= 'Grupa_C';
      parent = parent_lvl_1;
      params = 'typ= "G"';
      val = null;
      parent_lvl_2 = :id;
      suspend;
      pvalue = null;
    end
    else if (:psubfieldsymbol = 'A' and :pvalue = 1) then
    begin
      -- P_C1
      /* <!--Optional:-->
           <P_C1>
             1
           </P_C1>
      */
      id = id+1;
      name = 'P_C1';
      parent = parent_lvl_2;
      params = null;
      val = :pvalue;
      suspend;
    end
    else if (:psubfieldsymbol = 'B') then
    begin
      -- P_C2
      /* <P_C2>
           token
         </P_C2>
      */
      id = id+1;
      name = 'P_C2';
      parent = parent_lvl_2;
      params = null;
      val = :pvalue;
      suspend;
    end
    else if (:psubfieldsymbol = 'C') then
    begin
      -- P_C3
      /* <P_C3>
           string
         </P_C3>
      */
      id = id+1;
      name = 'P_C3';
      parent = parent_lvl_2;
      params = null;
      val = :pvalue;
      suspend;
    end
    else if (:psubfieldsymbol = 'D') then
    begin
      -- P_C4
      /* <P_C4>
           1000.000000000000
         </P_C4>
      */
      id = id+1;
      name = 'P_C4';
      parent = parent_lvl_2;
      params = null;
      val = :pvalue;
      suspend;
    end
  end

  -- P_10
  /* <P_10>
       1000.000000000000
     </P_10>
  */
  id = id+1;
  name= 'P_10';
  select epos.pvalue
    from edeclpos epos
    where epos.edeclaration = :oref and epos.fieldsymbol = 'K10'
  into :pvalue;
  if (:pvalue is not null) then
  begin
    parent = parent_lvl_1;
    params = null;
    val = :pvalue;
    suspend;
  end
  pvalue = null;

  fieldprefixendchar = '.';

  for select epos.pvalue, epos.fieldsymbol
    from edeclpos epos
    where epos.edeclaration = :oref
      and epos.fieldsymbol starting with 'DPOZ_'
    order by epos.field
  into :pvalue, :pfieldsymbol
  do begin
    psubfieldsymbol = substring(:pfieldsymbol
      from position(:fieldprefixendchar,:pfieldsymbol) + 1);

    if (:psubfieldsymbol = 'NUMER') then
    begin  
      -- Grupa D
      /* <Grupa_D typ= "G">
           ...
         </Grupa_D>
      */
      id = id+1;
      name= 'Grupa_D';
      parent = parent_lvl_1;
      params = 'typ= "G"';
      val = null;
      parent_lvl_2 = :id;
      suspend;
      pvalue = null;
    end
    if (:psubfieldsymbol = 'A' and :pvalue = 1) then
    begin
      -- P_D1
      /* <!--Optional:-->
           <P_D1>
             1
           </P_D1>
      */
      id = id+1;
      name = 'P_D1';
      parent = parent_lvl_2;
      params = null;
      val = :pvalue;
      suspend;
    end
    else if (:psubfieldsymbol = 'B') then
    begin
      -- P_D2
      /* <P_D2>
           token
         </P_D2>
      */
      id = id+1;
      name = 'P_D2';
      parent = parent_lvl_2;
      params = null;
      val = :pvalue;
      suspend;
    end
    else if (:psubfieldsymbol = 'C') then
    begin
      -- P_D3
      /* <P_D3>
           string
         </P_D3>
      */
      id = id+1;
      name = 'P_D3';
      parent = parent_lvl_2;
      params = null;
      val = :pvalue;
      suspend;
    end
    else if (:psubfieldsymbol = 'D') then
    begin
      -- P_D4
      /* <P_D4>
           1000.000000000000
         </P_D4>
      */
      id = id+1;
      name = 'P_D4';
      parent = parent_lvl_2;
      params = null;
      val = :pvalue;
      suspend;
    end
  end

  -- P_11
  /* <P_11>
       1000.000000000000
     </P_11>
  */
  id = id+1;
  name= 'P_11';
  select epos.pvalue
    from edeclpos epos
    where epos.edeclaration = :oref and epos.fieldsymbol = 'K11'
  into :pvalue;
  if (:pvalue is not null) then
  begin
    parent = parent_lvl_1;
    params = null;
    val = :pvalue;
    suspend;
  end
  pvalue = null;

  -- P_14
  /* <!--Optional:-->
     <P_14>
       token
     </P_14>
  */
  id = id+1;
  name= 'P_14';
  select epos.pvalue
    from edeclpos epos
    where epos.edeclaration = :oref and epos.fieldsymbol = 'K14'
  into :pvalue;
  if (:pvalue is not null) then
  begin
    parent = parent_lvl_1;
    params = null;
    val = :pvalue;
    suspend;
  end
  pvalue = null;

  -- P_15
  /* <!--Optional:-->
     <P_15>
       2017-01-01+01:00
     </P_15>
  */
  id = id+1;
  name= 'P_15';
  select epos.pvalue
    from edeclpos epos
    where epos.edeclaration = :oref and epos.fieldsymbol = 'K15'
  into :pvalue;
  if (:pvalue is not null) then
  begin
    parent = parent_lvl_1;
    params = null;
    val = :pvalue;
    suspend;
  end
  pvalue = null;

  --Pouczenie
  id = id+1;
  name= 'Pouczenie';
  parent = parent_lvl_0;
  params = null;
  val = '1'; --poważnie - 1, tak ma być
  suspend;

end^
SET TERM ; ^
