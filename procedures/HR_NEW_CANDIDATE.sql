--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE HR_NEW_CANDIDATE(
      RECRUIT ERECRUITS_ID,
      FIRSTNAME PERSON_NAME,
      NAME PERSON_NAME,
      EMAIL EMAILS_ID,
      STREET STRING,
      POSTCODE POSTCODE,
      CITY STRING,
      PHONE STRING30,
      REFERRER MEMO2000,
      SYMBOL STRING20 = null)
  returns (
      CANDIDATE_REF integer,
      ERECCAND integer)
   as
begin
  if (symbol is not null) then
    select first 1 e.ref from erecruits e where e.symbol=:symbol
    into :recruit;

  INSERT INTO ECANDIDATES (FNAME, SNAME, STREET, POSTCODE, CITY, PHONE, EMAIL, EMPLOYED, REFERRER)
    VALUES (:firstname, :name, substring(:street from 1 for 30),
       :postcode,substring(:city from 1 for 30), :phone, :email, 0, substring(:REFERRER from 1 for 255))
    returning ref into :candidate_ref;

  insert into ereccands (recruit, candidate, referrer, fromwww, phone, email)
    values (:recruit, :candidate_ref, substring(:REFERRER from 1 for 255), 1, :phone, :email)
    returning ref into :ereccand;
  suspend;
end^
SET TERM ; ^
