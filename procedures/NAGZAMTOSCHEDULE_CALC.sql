--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGZAMTOSCHEDULE_CALC as
begin
  if (exists (select first 1 1 from rdb$procedures where RDB$PROCEDURE_NAME ='XK_NAGZAMTOSCHEDULE_CALC')) then
    execute statement 'execute procedure XK_NAGZAMTOSCHEDULE_CALC';
  else
  begin
    delete from nagzamtoschedule;
    insert into nagzamtoschedule (ref, id, klient, rejestr, typzam)
      select n.ref, n.id, n.klient, n.rejestr, n.typzam
        from nagzam n
        where n.typzam = 'PRO' and n.datawe >= current_date - 10 and n.schedview is null;
    insert into nagzamtoschedule (ref, id, klient, rejestr, typzam)
      select n.ref, n.id, n.klient, n.rejestr, n.typzam
        from nagzam n
        where n.typzam = 'ZAM' and n.datawe >= current_date - 10;
  end
end^
SET TERM ; ^
