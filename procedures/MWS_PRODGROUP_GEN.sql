--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_PRODGROUP_GEN(
      DOCIDOUT integer)
   as
declare variable docposidout integer;
declare variable docidin integer;
declare variable good varchar(40);
declare variable period varchar(6);
declare variable vers integer;
declare variable quantity numeric(14,4);
declare variable refmwsordout integer;
declare variable refmwsordin integer;
declare variable mwsordtypeout integer;
declare variable docgroupin integer;
declare variable docgroupout integer;
declare variable doctypeout varchar(3);
declare variable doctypein varchar(3);
declare variable wh varchar(3);
declare variable branch varchar(10);
declare variable symbol varchar(30);
declare variable slokod varchar(40);
declare variable slopoz integer;
declare variable constlocprod integer;
declare variable pallocprod integer;
declare variable maxnumber integer;
declare variable procsql varchar(1024);
begin
  select d.typ, d.grupasped, d.magazyn, d.symbol, substring(k.fskrot from 1 for 40), k.ref
    from dokumnag d
      left join klienci k on (k.ref = d.klient)
    where d.ref = :docidout
    into doctypeout, docgroupout, wh, symbol, slokod, slopoz;
  select first 1 t.ref
    from mwsordtypes t
    where t.doctypes like '%;'||:doctypeout||';%' and t.mwsortypedets = 1
    into mwsordtypeout;
  select c.ref
    from mwsconstlocs c where c.locdest = 100 and c.wh = :wh
    into constlocprod;
  select first 1 m.ref
    from mwspallocs m
    where m.mwsconstloc = :constlocprod
    into :pallocprod;
  if (pallocprod is null) then
  begin
    execute procedure gen_ref('MWSPALLOCS') returning_values pallocprod;
    insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status)
      select :pallocprod, symbol, ref, 2, 0
        from mwsconstlocs
        where ref = :constlocprod;
  end
  for
    select p.ref, p.ktm, p.wersjaref, p.iloscl - p.ilosconmwsacts
      from dokumpoz p
      where p.dokument = :docidout and p.iloscl - p.ilosconmwsacts > 0
        and exists(select first 1 1 from dokumnag d where d.grupaprod = p.ref)
      into docposidout, good, vers, quantity
  do begin
    -- zlecenie wydania naglowek
    if (refmwsordout is null) then
    begin
      execute procedure gen_ref('MWSORDS') returning_values refmwsordout;
      select okres from datatookres(current_date,0,0,0,0) into period;
      insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, priority,
          description, wh,  regtime, timestartdecl, branch, period,
          flags, docsymbs, cor, rec, status, shippingarea, SHIPPINGTYPE, takefullpal,
          slodef, slopoz, slokod, palquantity, repal)
        values (:refmwsordout, :mwsordtypeout, 0, 'M', :docgroupout, :docidout, 1,
            'Autokompletacja', :wh, current_timestamp(0), null, :branch, :period,
            '', :symbol, 0, 0, 0, null, null, 0,
            1, :slopoz, :slokod, 0, 0);
    end
    select max(a.number)
      from mwsacts a where a.mwsord = :refmwsordout
      into maxnumber;
    if (maxnumber is null) then maxnumber = 0;
    maxnumber = maxnumber + 1;
    -- zlecenie wydania pozycje
    insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
        mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
        regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc, plus,
        whareal, whareap, wharealogl, number, disttogo, palgroup, autogen)
      values (:refmwsordout, 0, 0, :good, :vers, :quantity, :constlocprod, :pallocprod,
          null, null, :docidout, :docposidout, 'M', 0, 0, :wh, null, null,
          current_timestamp(0), null, null, null, null, 'Autokompletacja', 0, 0, 0, 1,
          null, null, null, :maxnumber, 0, null, 1);
    -- obsluga przyjecia na lokacje przeprodukowania
    for
      select d.ref, coalesce(d.grupasped,d.ref), d.typ
        from dokumnag d
        where d.grupaprod = :docposidout and d.wydania = 0 and d.koryg = 0
        into docidin, docgroupin, doctypeout
    do begin
      refmwsordin = null;
      select first 1 t.procgenmwsacts
        from mwsordtypes t
        where t.doctypes like '%;'||:doctypeout||';%' and t.mwsortypedets = 2
        into procsql;
      procsql = 'select refmwsord from '||procsql||'('||:docidin||','||:docgroupin||',null,null,null,null,1,null)';
      execute statement procsql into refmwsordin;
      -- teraz trzeba potwierdzic zlecenie przyjecia
      if (refmwsordin is not null) then
      begin
        update mwsacts a set a.mwsconstlocl = :constlocprod, a.mwspallocl = :pallocprod, a.quantityc = a.quantity, a.status = 2
          where a.mwsord = :refmwsordin and a.status = 0;
        update mwsords o set o.status = 5 where ref = :refmwsordin;
      end
    end
  end
  -- teraz dopiero mozna potwierdzic zlecenie wydania, jezeli powstalo
  if (refmwsordout is not null) then
  begin
    update mwsacts a set a.status = 2, a.quantityc = a.quantity
      where a.mwsord = :refmwsordout;
    update mwsords o set o.status = 5 where o.ref = :refmwsordout;
  end
end^
SET TERM ; ^
