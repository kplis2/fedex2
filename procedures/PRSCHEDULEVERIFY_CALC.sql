--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDULEVERIFY_CALC(
      PRSCHEDVIEW integer)
   as
begin
  if (prschedview = 0) then
  begin
    delete from prscheduleverify;
    exit;
  end
  delete from prscheduleverify;
  insert into prscheduleverify (ref, parent, pozzamref, nagzamref, nagzamid,
      ktm, wersjaref, prschedguide, prschedguidepos, prschedoper,
      quantity, quantityr, prshoper, proper, prmachine, descript, expand, prschedview, endtime)
    select ref, parent, pozzamref, nagzamref, nagzamid,
        ktm, wersjaref, prschedguide, prschedguidepos, prschedoper,
        quantity, quantityr, prshoper, proper, prmachine, descript, 1, :prschedview, termdost
      from pr_show_schedview_cascade(0,0,0,:prschedview);
end^
SET TERM ; ^
