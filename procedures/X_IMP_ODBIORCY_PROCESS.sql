--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_IMP_ODBIORCY_PROCESS(
      REF_IMP INTEGER_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING1024_UTF8)
   as
declare variable KLIENTREF INTEGER_ID;
declare variable ODBIORCAREF INTEGER_ID;
declare variable KLIENTID INTEGER_ID;
declare variable ODBIORCAID INTEGER_ID;
declare variable GLOWNY SMALLINT_ID;
declare variable NAZWA STRING255;
declare variable IMIE STRING255;
declare variable NAZWISKO STRING255;
declare variable KRAJID STRING20;
declare variable ULICA STRING255;
declare variable NRDOMU STRING10;
declare variable NRLOKALU STRING10;
declare variable KODPOCZTOWY STRING10;
declare variable MIASTO STRING255;
declare variable TELEFON STRING255;
declare variable EMAIL STRING255;
declare variable AKTYWNY SMALLINT_ID;
declare variable UWAGIDOKUMENT STRING1024;
declare variable klient_int_id string40;
declare variable odbiorca_int_id string40;
begin
-- procedura pobiera dane o odbiorcach z tabeli przejsciowej i tworzy nowego odbiorce lub edytuje starego
    status = 1;
    msg = '';
    --pobranie danych do zmiennych
    select KLIENTID, ODBIORCAID, GLOWNY, NAZWA, IMIE, NAZWISKO, KRAJID, ULICA, NRDOMU, NRLOKALU, KODPOCZTOWY, MIASTO,
           TELEFON, EMAIL, AKTYWNY, UWAGIDOKUMENT
        from X_IMP_ODBIORCY
        WHERE ref = :ref_imp
    into :KLIENTID, :ODBIORCAID, :GLOWNY, :NAZWA, :IMIE, :NAZWISKO, :KRAJID, :ULICA, :NRDOMU, :NRLOKALU, :KODPOCZTOWY,
         :MIASTO, :TELEFON, :EMAIL, :AKTYWNY, :UWAGIDOKUMENT;
    --int_id jako string
    klient_int_id =cast(klientid as string40);
    --sprawdzenie czy istnieje klient
    select k.ref from klienci k
        where k.int_id = :klient_int_id
        into :klientref;
    if (coalesce(klientref, 0) = 0) then
    begin
        status = 8;
        msg = msg || 'nie istnieje klient o int_id=' || coalesce(:klientid,'<brak>');
    end
    --sprawdzam czy istnieje krajid i czy jest poprwany
    krajid = upper(krajid);
    if (coalesce(krajid,'') = '') then
    begin
        status = 8;
        msg = 'brak id kraju';
    end else
    if (not exists (select first 1 * from countries c where c.symbol = :krajid ))
    then
    begin
        status = 8;
        msg = 'brak Kraju o kodzie alfa-2: '||coalesce(:krajid,'<brak>');
    end

    --int_id jako string
    odbiorca_int_id =cast(odbiorcaid as string40);
    select ref from odbiorcy o where o.int_id = :odbiorca_int_id
        into :odbiorcaref;

    --jezeli brak bledow to dodanie nowego odbiorcy lub edycja juz istniejacego
    if (status = 1) then
    begin
        if ( coalesce(odbiorcaref, 0) = 0) then
        begin
            insert into ODBIORCY
                (KLIENT, NAZWA, DULICA, DMIASTO, DKODP, DTELEFON, GL,
                EMAIL, COMPANY, DNRDOMU, DNRLOKALU, IMIE, KRAJID, NAZWISKO,
                INT_ID, INT_DATAOSTPRZETW, X_UWAGI_DOKUMENT, X_IMP_REF)
            values
                (:KLIENTREF, :NAZWA, :ULICA, :MIASTO, :KODPOCZTOWY, :TELEFON, :GLOWNY,
                :EMAIL, 1, :NRDOMU, :NRLOKALU, :IMIE, :KRAJID, :NAZWISKO,
                :odbiorca_int_id, current_timestamp, :UWAGIDOKUMENT, :REF_IMP);
        end
        else begin
            update ODBIORCY
                set KLIENT = :klientref,
                    NAZWA = :NAZWA,
                    GL = :glowny,
                    EMAIL = :EMAIL,
                    COMPANY = 1,
                    DNRDOMU = :NRDOMU,
                    DNRLOKALU = :NRLOKALU,
                    DULICA = :ulica,
                    DMIASTO = :miasto,
                    DKODP = :kodpocztowy,
                    DTELEFON = :telefon,
                    IMIE = :IMIE,
                    KRAJID = :KRAJID,
                    NAZWISKO = :NAZWISKO,
                    INT_ID = :odbiorca_int_id,
                    INT_DATAOSTPRZETW = current_timestamp,
                    X_UWAGI_DOKUMENT = :uwagidokument,
                    x_imp_ref = :ref_imp
                where (REF = :odbiorcaref);
        end
    end
  suspend;
end^
SET TERM ; ^
