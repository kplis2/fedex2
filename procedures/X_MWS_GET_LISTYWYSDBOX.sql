--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_GET_LISTYWYSDBOX(
      SYMBOL STRING20)
  returns (
      LISTYWYSD INTEGER_ID,
      SYMBOLDOK STRING40,
      NEWCAR SMALLINT_ID,
      MSG STRING255,
      STATUS SMALLINT_ID)
   as
  declare variable iscar string20;
  declare variable listawysref integer_id;
  declare variable car string20;
begin
  if (substr(:symbol, 1, 3) in  ('OPK', 'PAL'))then begin --szukanie na podstawie opakowania
    select first 1 l.listwysd
      from listywysdroz_opk l
      where l.stanowisko = :symbol
    into :listywysd;
  end else begin
    select first 1 l.ref  --szukanie na podstawie dokumentu
      from listywysd l
      where l.symbol = :symbol
    into :listywysd;
  end

  if (coalesce(listywysd, 0) = 0) then begin
    status = 0;
    msg = 'Nie znaleziono dokumentu spedycyjnego. '||coalesce(symbol, '');
    exit;
  end else
    status = 1;

  select first 1 ld.symbol, ld.listawys       -- symbol dok
    from listywysd ld
    where ld.ref = :listywysd
  into :symboldok, listawysref;

  if (listawysref is not null) then begin
    select l.symbol, l.x_car
      from listywys l
      where l.ref = :listawysref
    into :symboldok, :car;
    if (symboldok like '%TYM%') then
      symboldok = coalesce(symboldok,'')||' - '||coalesce(car, '');
  end
  if (symboldok is null) then symboldok = '';

  select first 1 l.x_car
    from listywys l
    where l.ref = :listawysref
  into :iscar;

  if (iscar is null or strlen(iscar)<2) then NEWCAR = 1;
  else newcar = 0;

  suspend;
end^
SET TERM ; ^
