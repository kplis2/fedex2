--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ANAL_MWS_REC_DOCS_DETS(
      DOCID integer,
      PALINCLUDE smallint)
  returns (
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      QUANTITYONPOS numeric(14,4),
      QUANTITYONPOSDETS numeric(14,4),
      QUANTITYONACTS numeric(14,4),
      QUANTITYONACTDETS numeric(14,4),
      QUANTITYONMINUS numeric(14,4),
      QUANTITYONPLUS numeric(14,4),
      BILANS numeric(14,4),
      STATUS smallint)
   as
declare variable mwsord integer;
declare variable qplusclr numeric(14,4);
declare variable qminusclr numeric(14,4);
declare variable mws smallint;
begin
  select coalesce(m.mws, 0)
    from dokumnag d
      left join defmagaz m on (m.symbol = d.magazyn)
    where d.ref = :docid
    into mws;
  select ref from mwsords where docid = :docid into mwsord;
  for
    select good, vers
      from mws_rec_doc_all_goods(:docid,:palinclude)
      group by good, vers
      into good, vers
  do begin
    quantityonpos = 0;
    quantityonposdets = 0;
    quantityonacts = 0;
    quantityonactdets = 0;
    quantityonminus = 0;
    quantityonplus = 0;
    bilans = 0;
    select sum(p.ilosc)
      from dokumpoz p
      where p.dokument = :docid and p.wersjaref = :vers
      into quantityonpos;
    if (quantityonpos is null) then quantityonpos = 0;
    select sum(dr.ilosc)
      from dokumpoz p
        join dokumroz dr on (dr.pozycja = p.ref)
      where p.dokument = :docid and p.wersjaref = :vers
      into quantityonposdets;
    if (quantityonposdets is null) then quantityonposdets = 0;
    if (mws > 0) then
    begin
      select sum(a.quantityc)
        from mwsacts a
        where a.mwsord = :mwsord and a.vers = :vers
        into quantityonacts;
      select sum(ad.quantityc)
        from mwsacts a
          join mwsactdets ad on (ad.mwsact = a.ref)
        where a.mwsord = :mwsord and a.vers = :vers
        into quantityonactdets;
      select sum(p.ilosc)
        from dokumpoz p
          join dokumnag d on (p.dokument = d.ref)
          join mwsords o on (o.ref = d.frommwsord and o.docid = :docid)
          left join defdokum f on (f.symbol = d.typ)
        where f.wydania = 0 and p.wersjaref = :vers
        into quantityonplus;
      select sum(p.ilosc)
        from dokumpoz p
          join dokumnag d on (p.dokument = d.ref)
          join mwsords o on (o.ref = d.frommwsord and o.docid = :docid)
          left join defdokum f on (f.symbol = d.typ)
        where f.wydania = 1 and p.wersjaref = :vers
        into quantityonminus;
    end else if (mws = 0) then
    begin
      quantityonactdets = quantityonposdets;
      quantityonacts = quantityonpos;
      select sum(p.ilosc)
        from dokumnag d
          left join dokumpoz p on (p.dokument = d.ref)
          left join defdokum f on (f.symbol = d.typ)
        where d.grupaimp = :docid and f.wydania = 0 and p.wersjaref = :vers
        into quantityonplus;
      select sum(p.ilosc)
        from dokumnag d
          left join dokumpoz p on (p.dokument = d.ref)
          left join defdokum f on (f.symbol = d.typ)
        where d.grupaimp = :docid and f.wydania = 1 and p.wersjaref = :vers
        into quantityonminus;
    end
    if (quantityonactdets is null) then quantityonactdets = 0;
    if (quantityonacts is null) then quantityonacts = 0;
    if (quantityonminus is null) then quantityonminus = 0;
    if (quantityonplus is null) then quantityonplus = 0;
    bilans = quantityonposdets - quantityonactdets + quantityonplus - quantityonminus;
    if (bilans <> 0 or quantityonposdets <> quantityonpos or quantityonactdets <> quantityonacts) then
      status = 0;
    else
      status = 1;
    if (quantityonplus > 0) then
    begin
      select sum(quantityfrom)
        from mwssupclr
        where docidfrom = :docid and versfrom = :vers and quantityfrom > 0 and docidto is not null
        into qplusclr;
      if (qplusclr is null) then qplusclr = 0;
      quantityonplus = quantityonplus - qplusclr;
      bilans = bilans - qplusclr;
    end
    if (quantityonminus > 0) then
    begin
      select sum(quantityto)
        from mwssupclr
        where docidto = :docid and versto = :vers and quantityto > 0 and docidfrom is not null
        into qminusclr;
      if (qminusclr is null) then qminusclr = 0;
      quantityonminus = quantityonminus - qminusclr;
      bilans = bilans + qminusclr;
    end
    suspend;
  end
end^
SET TERM ; ^
