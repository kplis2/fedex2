--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMPOZ_CHANGE_DO_CORRECTION(
      KORTOPOZ integer,
      POZDOKUM integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      ILOSC numeric(14,5),
      SERIALIZED smallint,
      BLOKOBLNAG smallint,
      BLOKPOZNALICZ integer,
      CENAMAG numeric(14,4),
      DOSTAWA integer,
      MAG_TYP char(1) CHARACTER SET UTF8                           ,
      MAG char(3) CHARACTER SET UTF8                           ,
      FIFO char(1) CHARACTER SET UTF8                           ,
      MINUS_STAN smallint,
      MAGBREAK integer,
      ZAMOWIENIE integer,
      WERSJAREF integer,
      OPK smallint,
      OPKTRYB smallint,
      SLODEF integer,
      SLOPOZ integer,
      CENASNET numeric(14,4),
      CENASBRU numeric(14,4),
      BN char(1) CHARACTER SET UTF8                           ,
      VAT numeric(14,4),
      STANOPKREF integer,
      ORG_ILOSC numeric(14,4),
      DOK_ILOSC numeric(14,4),
      CENA_KOSZT numeric(14,4))
   as
declare variable rozref integer;
declare variable j numeric(14,5);
declare variable serialnr varchar(40);
declare variable serialil numeric(14,4);
declare variable orgdokumser integer;
declare variable numer integer;
declare variable i numeric(14,5);
begin
    if(:ilosc > 0) then begin  /*zwikszenie korekty*/
      if(:serialized = 1) then begin
        for select SERIALNR, ILOSC, ORGDOKUMSER from DOKUMPOZ_SERIAL(:pozdokum,0)
        into :serialnr, :serialil, :ORGDOKUMSER
        do begin
          if(:serialnr is null) then serialnr = '';
          if(:serialil is null) then serialil = :ilosc;
          for select DOKUMROZ.REF, DOKUMROZ.iloscl, dokumroz.cena, dokumroz.dostawa, dokumroz.cenasnetto, dokumroz.cenasbrutto, dokumroz.cena_koszt
          from DOKUMROZ where DOKUMROZ.POZYCJA = :kortopoz and iloscl > 0
          order by numer desc
          into :rozref, :j, :cenamag, :dostawa, :cenasnet, :cenasbru, :cena_koszt
          do begin
            if(:j > :serialil) then j = :serialil;
            if(:j > :ilosc) then j = :ilosc;
            if(:j > 0) then begin
                numer = null;
                select NUMER from DOKUMROZ where KORTOROZ = :rozref and pozycja = :pozdokum and SERIALNR = :serialnr into :numer;
                if(:numer is null) then begin
                  select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
                  if(:numer is null or (:numer = 0))then numer = 0;
                  numer = numer +  1;
                  insert into DOKUMROZ(POZYCJA, NUMER,ILOSC, CENA, DOSTAWA, SERIALNR, CENASNETTO, CENASBRUTTO, KORTOROZ, BLOKOBLNAG, CENA_KOSZT, ORGDOKUMSER)
                  values (:pozdokum, :numer, :j, :cenamag, :dostawa, :serialnr, :cenasnet, :cenasbru, :rozref, :blokoblnag, :cena_koszt, :ORGDOKUMSER);
                end
                else
                  update DOKUMROZ set ILOSC = ILOSC + :j, PCENA = null where pozycja = :pozdokum and numer = :numer;
                ilosc = :ilosc - :j;
                serialil = :serialil - :j;
             end
          end
        end
      end else begin
        for select DOKUMROZ.REF, DOKUMROZ.iloscl, dokumroz.cena, dokumroz.dostawa, dokumroz.cenasnetto, dokumroz.cenasbrutto, dokumroz.cena_koszt
        from DOKUMROZ where DOKUMROZ.POZYCJA = :kortopoz and iloscl > 0
        order by numer desc
        into :rozref, :j, :cenamag, :dostawa, :cenasnet, :cenasbru, :cena_koszt
        do begin
          if(:j > :ilosc) then j = :ilosc;
          if(:ilosc > 0) then begin
                numer = null;
                select NUMER from DOKUMROZ where KORTOROZ = :rozref and pozycja = :pozdokum into :numer;
                if(:numer is null) then begin
                  select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
                  if(:numer is null or (:numer = 0))then numer = 0;
                  numer = numer +  1;
                  insert into DOKUMROZ(POZYCJA, NUMER,ILOSC, CENA, DOSTAWA, SERIALNR, CENASNETTO, CENASBRUTTO, KORTOROZ, BLOKOBLNAG, CENA_KOSZT)
                  values (:pozdokum, :numer, :j, :cenamag, :dostawa, '', :cenasnet, :cenasbru, :rozref, :blokoblnag, :cena_koszt);
                end
                else
                  update DOKUMROZ set ILOSC = ILOSC + :j, PCENA = null where pozycja = :pozdokum and numer = :numer;
                ilosc = :ilosc - :j;
          end
        end
      end
      if (:ilosc > 0) then begin
        execute procedure DOKUMPOZ_CHANGE_DO_CORR_ALL(:pozdokum, :ilosc, :blokoblnag) returning_values :ilosc;
      end
      if(:ilosc > 0) then exception MAGDOK_ILKORTOBIG;
    end else if(:ilosc < 0) then begin
       /*wycofanie rozpisek wasnych*/
      ilosc = - :ilosc;
      for select NUMER, ilosc from DOKUMROZ where pozycja = :pozdokum
      order by numer desc into :numer,:i do begin
         if(:i is null) then i = 0;
         if((:i > :ilosc) and (:ilosc <> 0)) then begin
           update DOKUMROZ set ILOSC = (ILOSC - :ilosc), PCENA = null where pozycja = :pozdokum and numer = :numer;
           ilosc = 0;
         end else if(:ilosc > 0) then begin
           delete from DOKUMROZ where pozycja = :pozdokum and numer = :numer;
           ilosc = :ilosc - :i;
         end
      end
    end
    if(:blokpoznalicz = 0) then
      execute procedure DOKPOZ_OBL_FROMROZ(:pozdokum);
end^
SET TERM ; ^
