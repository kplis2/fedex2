--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COPY_SLODEF_SLOPOZ_DISTS(
      DICTDEF SLO_ID,
      PATTERN_REF SLO_ID = null,
      DEST_COMPANY COMPANIES_ID = null)
  returns (
      NEW_SLODEF SLO_ID)
   as
declare variable SP_REF SLOPOZ_ID;
declare variable SP_NEWKOD varchar(40);
declare variable SP_NEWNAZWA STRING;
declare variable SP_NEWKONTOKS KONTO_ID;
declare variable SP_NEWCRM smallint;
declare variable SP_NEWTOKEN smallint;
declare variable SP_NEWSTATE STATE;
declare variable SP_NEWAKT SMALLINT_ID;
declare variable SP_NEWCPODMIOT CPODMIOTY_ID;
declare variable SP_NEWMASTERREF INTEGER_ID;
declare variable SP_NEWMASTERIDENT INTEGER_ID;
declare variable SP_NEWOPIS STRING;
declare variable SP_NEWDFLAGS STRING80;
declare variable SP_NEWRIGHTS STRING255;
declare variable SP_NEWRIGHTSGROUP STRING255;
declare variable CURRENT_COMPANY COMPANIES_ID;
declare variable NEWDICTDEF SLO_ID;
declare variable NEWSLOPOZ SLOPOZ_ID;
declare variable PATTERNREF COMPANIES_ID;
declare variable DIST_NUMBER INTEGER_ID;
declare variable DIST_DISTDEF integer;
declare variable DIST_DISTFILTER STRING;
declare variable DIST_OTABLE STRING20;
declare variable DIST_ORD SMALLINT_ID;
declare variable MASTER_REF SLO_ID;
begin
  if (dest_company is null) then
  begin
    execute procedure get_global_param('CURRENTCOMPANY')
      returning_values :current_company;
    dest_company = :current_company;
  end

  --Należy skopiować słownik, jednak sprawdzamy, czy juz nie kopiowalismy z tego slownika
  --Jak tak to nalezy go odnaleźc i skopiować ref
  select s.ref
    from slodef s
    where s.patternref = :dictdef and s.company = :dest_company
    into :master_ref;
  if(master_ref is null)then
  begin
      -- kopiowanie definicji slownika
    insert into slodef (ref,nazwa, typ, predef, crm, kasa, prefixfk, gridname,
      gridnamedict, formname, firma, trybred, mag, opak, kartoteka, analityka,
      kodln, state, token, isdist, mastertable, logistyka, personel, bkbrowsewindow,
      produkcja, symbol, dictposfield, kodfield, nazwafield, kodksfield,
      nipfield, iscompany, multidist ,company, patternref)
    select null, nazwa, typ, predef, crm, kasa, prefixfk, gridname,
      gridnamedict, formname, firma, trybred, mag, opak, kartoteka, analityka,
      kodln, state, token, isdist, mastertable, logistyka, personel, bkbrowsewindow,
      produkcja, symbol, dictposfield, kodfield, nazwafield, kodksfield,
      nipfield, iscompany, multidist , :dest_company ,  ref
      from slodef where ref = :dictdef
    returning ref into :newdictdef;

    -- kopiowanie wartosci slownika
    for
      select ref, kod, nazwa, kontoks, crm, token, state,
        akt, cpodmiot, masterref, masterident, opis, dflags,
        rights, rightsgroup
      from slopoz where slownik = :dictdef
      into :sp_ref,:sp_newkod,:sp_newnazwa,:sp_newkontoks,:sp_newcrm,:sp_newtoken,:sp_newstate,
        :sp_newakt,:sp_newcpodmiot,:sp_newmasterref,:sp_newmasterident,:sp_newopis,
        :sp_newdflags,:sp_newrights,:sp_newrightsgroup
      do begin
        newslopoz = null;
        --insert pozycji
        insert into slopoz (ref,slownik,kod,nazwa,kontoks,crm,token,state,akt,
          cpodmiot,masterref,masterident,opis,dflags,rights,
          rightsgroup, pattern_ref)
        values (:newslopoz, :newdictdef, :sp_newkod,:sp_newnazwa,:sp_newkontoks,
          :sp_newcrm,:sp_newtoken,:sp_newstate,:sp_newakt,:sp_newcpodmiot,
          :sp_newmasterref,:sp_newmasterident,:sp_newopis,:sp_newdflags,
          :sp_newrights,:sp_newrightsgroup, :sp_ref)
        returning ref into :newslopoz;

      end
      new_slodef = newdictdef;
  end else
    new_slodef = master_ref;
  suspend;
end^
SET TERM ; ^
