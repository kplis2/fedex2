--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHECK_STRING_TO_NUMERIC(
      STR varchar(100) CHARACTER SET UTF8                           ,
      SIGNED smallint = 0)
  returns (
      STATUS smallint)
   as
declare variable STRTMP varchar(100);
declare variable LEN integer;
begin
  status = 0;
  if(signed = 1 and substring(str from 1 for 1) = '-' and coalesce(char_length(str),0) > 1) then str = substring(str from 2 for 100); -- [DG] XXX ZG119346
  len = coalesce(char_length(str),0); -- [DG] XXX ZG119346
  if (len > 0) then
    status = 1;
  if(substring(str from 1 for 1) = '.' or substring(str from coalesce(char_length(str),0) for coalesce(char_length(str),0)) = '.') then status = 0; -- [DG] XXX ZG119346
  while(len > 0 and status = 1)
  do begin
    strtmp = substring(str from 1 for 1);
    if (strtmp not in ('0','1','2','3','4','5','6','7','8','9','.')) then
      status = 0;
    str = substring(str from 2 for 100);
    len = coalesce(char_length(str),0); -- [DG] XXX ZG119346
  end
  suspend;
end^
SET TERM ; ^
