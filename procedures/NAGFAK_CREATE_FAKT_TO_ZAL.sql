--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_CREATE_FAKT_TO_ZAL(
      FAKTURA integer,
      REJFAK varchar(3) CHARACTER SET UTF8                           ,
      TYPFAK varchar(3) CHARACTER SET UTF8                           ,
      STANOWISKO varchar(10) CHARACTER SET UTF8                           ,
      DATA timestamp,
      OPERATOR integer)
  returns (
      REFFAK integer)
   as
declare variable refk integer;
declare variable nieobrot integer;
declare variable akceptacja integer;
declare variable zaliczkowy integer;
declare variable kwotazal numeric(14,2);
begin
  reffak = null;
  /* sprawdzenie, czy juz nie ma korekty lub faktury lub jest nieobrotowy */
/*  select REFFAK, NIEOBROT, AKCEPTACJA, ZALICZKOWY from NAGFAK where REF=:faktura into  :REFFAK, :nieobrot, :akceptacja, :zaliczkowy;
  select count(*) from NAGFAK where REFK = :faktura into :refk;
  if(:akceptacja = 0) then exception KOREKTA_NEW_DOKNIEACK;
  if(:reffak > 0) then exception CREATE_FAK_TO_PAR_HASFAK;
  if(:refk > 0) then exception CREATE_FAK_TO_PAR_HASKOR;
  execute procedure ID_NAGFAK returning_values :reffak;
  kwotazal = 0;
  select sum(KWOTABRUZL) from NAGFAK_GET_ZALICZKI(:faktura) into :kwotazal;
  insert into NAGFAK(REF,REJESTR,TYP,STANOWISKO,DATA,AKCEPTACJA,
        KLIENT,PLATNIK,DOSTAWCA,SPOSZAP,TERMIN,UZYKLI, RABAT,
        REFK, SYMBOLK,UWAGI,
        TABKURS,WALUTA,KURS,PKURS,KWOTAZAL,
        PESUMWARTNET, PESUMWARTBRU, PESUMWARTNETZL, PESUMWARTBRUZL,
        SPRZEDAWCA, OPERATOR, GRUPADOK, BN, numpar, datafisk, MAGAZYN)
     select :reffak,:rejfak, :typfak,:stanowisko,:data,0,
        KLIENT,PLATNIK, DOSTAWCA,SPOSZAP, TERMIN, UZYKLI, RABAT,
        REF,SYMBOL,UWAGI,
        TABKURS,WALUTA,KURS,KURS,:kwotazal,
        ESUMWARTNET, ESUMWARTBRU, ESUMWARTNETZL, ESUMWARTBRUZL,
        SPRZEDAWCA, :operator, GRUPADOK, BN, NUMPAR, DATAFISK, MAGAZYN
      from NAGFAK where REF=:faktura;
  insert into POZFAK(DOKUMENT,NUMER,ORD,KTM,WERSJAREF,DOSTAWA,
         ILOSC, ILOSCO, ILOSCM,
         JEDN, JEDNO,
         CENACEN, WALCEN,
         RABAT,RABATTAB,
         PCENAMAG,
         GR_VAT, VAT, PGR_VAT, PVAT,
         CENANET, CENABRU,
         WARTNET, WARTBRU,
         CENANETZL, CENABRUZL,
         WARTNETZL, WARTBRUZL, OPK, MAGAZYN)
     select :reffak,NUMER,1,KTM,WERSJAREF,DOSTAWA,
         ILOSC, ILOSCO, ILOSCM,
         JEDN, JEDNO,
         CENACEN, WALCEN,
         RABAT, RABATTAB,
         CENAMAG,
         GR_VAT, VAT, GR_VAT, VAT,
         CENANET, CENABRU,
         WARTNET, WARTBRU,
         CENANETZL, CENABRUZL,
         WARTNETZL, WARTBRUZL, OPK, MAGAZYN
     from POZFAK where DOKUMENT = :faktura;*/
end^
SET TERM ; ^
