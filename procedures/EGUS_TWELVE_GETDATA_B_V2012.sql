--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EGUS_TWELVE_GETDATA_B_V2012(
      GYEAR varchar(4) CHARACTER SET UTF8                           ,
      EMPLOYEE integer)
  returns (
      B2 varchar(80) CHARACTER SET UTF8                           ,
      B3 varchar(6) CHARACTER SET UTF8                           ,
      B4 smallint,
      B5 smallint,
      B6 smallint,
      B7 smallint,
      B8 varchar(20) CHARACTER SET UTF8                           ,
      B9 varchar(20) CHARACTER SET UTF8                           ,
      B10 varchar(20) CHARACTER SET UTF8                           )
   as
declare variable PERSON integer;
declare variable COMPANY integer;
declare variable BMONTH integer;
declare variable FROMDATE date;
declare variable TODATE date;
declare variable CDR smallint;
declare variable CMGR smallint;
declare variable CINZ smallint;
declare variable CEDU smallint;
declare variable PERSCEDU smallint;
declare variable BIRTHDATE date;
declare variable WORKSYSTEM varchar(40);
begin
/*MWr Personel: procedura generuje dane do sprawozdania GUS Z-12 (na rok 2010)
  dla zadanego pracownika EMPLOYEE. Odpowiada za czesc B raportu - stan w dniu
  31 X zadanego roku GYEAR. Kolumny wyjciowe specyfikuje dokumentacja, ogolnie:
      B2 - nazwa zawodu wykonywanego
      B3 - symbol zawodu - specjalnosc
      B4 - symbol plci (1=M, 2=K)
      B5 - poziom ukonczonego wyksztalcenia
      B6 - symbol rodzaju umowy o prace
      B7 - symbol systemu czasu pracy
      B8 - rok urodzenia
      B9 - staz pracy ogolem
      B10 - staz pracy w jednosce sprawozdawczej */

  execute procedure period2dates(:gyear || '10')
    returning_values :fromdate, :todate;

  select first 1 e.person, w.workpost, coalesce(g.code,''), abs(coalesce(p.sex,0) - 2),
         case when c.econtrtype = 3 then 4 else c.econtrtype end, s.shortname,
         p.birthdate, e.company, z.code
    from persons p
      join employees e on (p.ref = e.person)
      join employment m on (m.employee = e.ref)
      join emplcontracts c on (c.ref = m.emplcontract)
      left join edictworksystems s on (s.ref = c.worksystem)
      left join edictworkposts w on (w.ref = m.workpost)
      left join edictguscodes g on (g.ref = w.guscode)
      left join edictzuscodes z on (z.ref = p.education)
    where m.fromdate <= :todate and (m.todate is null or m.todate >= :fromdate)
      and m.employee = :employee
    order by m.fromdate desc
    into :person, :b2, :b3, :b4, :b6, :worksystem, :birthdate, :company, :perscedu;

  if (person is not null) then
  begin
  --istnieje zatrudnienie na koniec pazdziernika, okreslamy symbol wyksztalcenia

    select max(case when lower(d.name) like '%doktor%' then 1 else 4 end),
           max(case when lower(d.name) like '%m%g%r%' then 2 else 4 end),
           max(case when lower(d.name) like '%in%ynier%' then 3 else 4 end),
           max(z.code)
      from eperschools s
        join edictzuscodes z on (z.ref = s.education)
        left join edictacademicdegrees d on (d.ref = s.academicdegree)
      where s.person = :person
        and (s.todate <= :todate or s.todate is null)
      into :cdr, :cmgr, :cinz, :cedu;

    cedu = coalesce(cedu,perscedu);
    if (cedu in (11, 12)) then b5 = 8;   --gimnazjalne (tu jako podstawowe)
    else if (cedu = 20) then b5 = 7;     --zasad. zawodowe
    else if (cedu = 32) then b5 = 6;     --srednie ogolne
    else if (cedu = 31) then b5 = 5;     --srednie zawodowe
    else if (cedu = 40) then b5 = 4;     --policelane
    else if (cedu = 50) then begin       --wyzsze (domsylnie jako magister)
      if (cdr = 1) then b5 = 1;
      else if (cmgr = 2) then b5 = 2;
      else if (cinz = 3) then b5 = 3;
      else b5 = 2;
    end

  --okreslenie symbolu systemu czasu pracy
    if (worksystem is not null) then
    begin
      worksystem = lower(worksystem);
      if (worksystem like '%podstaw%') then b7 = 10;
      else if (worksystem like '%r%wnowa%n%') then b7 = 21;
      else if (worksystem like '%przerywan%') then b7 = 30;
      else if (worksystem like '%zadaniow%') then b7 = 40;
      else if (worksystem like '%w%kendow%') then b7 = 50;
      else if (worksystem like '%skr%con%ty%d%n%') then b7 = 60;
      else if (worksystem like '%ruch%ci%g%y%') then b7 = 70;
    end

  --rok i miesiac urodzenia
    if (birthdate is not null) then
    begin
      b8 = substring(extract(year from birthdate) from 3 for 4) || ',';
      bmonth = extract(month from birthdate);
      if (bmonth < 10) then b8 = b8 || '0';
      b8 = b8 || bmonth;
    end

  /*staz pracy ogolem i w jednosce z obsluga przypadku, kiedy prac. zostal zatrudniony
    w miesiacu pazdziernik, wtedy pole miesiec z procedury = 0 a tak byc nie moze
    bo b9 i b10 musza byc niezerowe (BS108630) */
    select sumy || ',' || case when summ < 10 then '0' || case when summ = 0 then 1 else summ end else summ end,
           herey || ',' || case when herem < 10 then '0' || case when herem = 0 then 1 else herem end else herem end
      from efunc_get_jobseniority(:employee, :todate, 0, :company)
      into :b9, :b10;

    suspend;
  end
end^
SET TERM ; ^
