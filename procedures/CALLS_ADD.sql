--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CALLS_ADD(
      OPER OPERATOR_ID,
      TYP SMALLINT_ID,
      DESTNUMBER STRING,
      SOURCENUMBER STRING,
      CONNECTIONID STRING,
      STATE SMALLINT_ID,
      QUEUEID INTEGER_ID,
      BEGINCALL TIMESTAMP_ID)
   as
declare variable queueref cqueues_id;
begin
  if(queueid is not null) then
    queueref = (select ref from cqueues where queueid=:queueid);

  if(exists(select * from calls where connectionid=:connectionid)) then
  begin --mamy pelna informacje z thulium api
    update or insert into calls (OPERATOR,TYP,DESTNUMBER,SOURCENUMBER,CONNECTIONID,
      STATE,QUEUE,BEGINCALL)
    values (:oper, :typ, :destnumber,:sourcenumber,:connectionid,:state,:queueref,
      :begincall)
    matching (connectionid);
  end
  else
  begin --nie mamy kompletu informacji, tylko dane z waitinglist a thulium api nas nie wywolalo
    update or insert into calls (OPERATOR,TYP,DESTNUMBER,SOURCENUMBER,CONNECTIONID,
      STATE,QUEUE,BEGINCALL)
    values (:oper, :typ, :destnumber,:sourcenumber,:connectionid,:state,:queueref,
      :begincall)
    matching (connectionid);
  end
end^
SET TERM ; ^
