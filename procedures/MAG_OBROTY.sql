--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_OBROTY(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      ODDATY timestamp,
      DODATY timestamp)
  returns (
      STANP numeric(14,4),
      WARTP numeric(14,2),
      ILPRZY numeric(14,4),
      WARTPRZY numeric(14,2),
      ILROZ numeric(14,4),
      WARTROZ numeric(14,2),
      ILKOR numeric(14,4),
      WARTKOR numeric(14,2),
      STANK numeric(14,4),
      WARTK numeric(14,2))
   as
declare variable datapocz timestamp;
declare variable ilkorm numeric(14,4);
declare variable wartkorm numeric(14,2);
declare variable stanpm numeric(14,4);
declare variable wartpm numeric(14,2);
begin
    /*stan początkowy*/
    datapocz = :oddaty - 1;
    execute procedure MAG_STAN(:magazyn,:ktm,:wersja,NULL,NULL,:DATAPOCZ) returning_values :stanp, :wartp;
    select sum(DOKUMROZ.ilosc), sum(DOKUMROZ.wartosc)
         , sum(DOKUMROZ.ilosc * DEFDOKUM.WYDANIA), sum(dokumroz.wartosc * Defdokum.wydania)
         , sum(DOKUMROZ.ilosc * DEFDOKUM.koryg), sum(dokumroz.wartosc * defdokum.koryg)
         ,sum(dokumroz.ilosc * DEFDOKUM.KORYG * DEFDOKUM.wydania), sum(dokumroz.wartosc * defdokum.wydania * defdokum.koryg)
         from
      DOKUMNAG join DOKUMPOZ on (DOKUMPOZ.dokument = DOKUMNAG.REF and DOKUMNAG.akcept = 1 and DOKUMNAG.MAGAZYN=:MAGAZYN
                                 AND DOKUMNAG.DATA >= :ODDATY
                                 AND DOKUMNAG.DATA <= :DODATY
                                 and DOKUMPOZ.KTM = :KTM AND DOKUMPOZ.WERSJA = :WERSJA)
               join DOKUMROZ on (DOKUMROZ.pozycja = DOKUMPOZ.REF)
               join DEFDOKUM on (DOKUMNAG.typ = DEFDOKUM.symbol)
    into :ilprzy, :wartprzy
        , :ilroz, :wartroz
        , :ilkor, :wartkor
        , :ilkorm, :wartkorm;
    if(:ilprzy is null) then ilprzy = 0;
    if(:wartprzy is null) then wartprzy = 0;
    if(:ilroz is null) then ilroz = 0;
    if(:wartroz is null) then wartroz = 0;
    if(:ilkorm is null) then ilkorm = 0;
    if(:wartkorm is null) then wartkorm = 0;
    if(:ilkor is null) then ilkor = 0;
    if(:wartkor is null) then wartkor = 0;
    ilkor = :ilkor - :ilkorm;/* od calej ilosci korygujacyh odejmuje korygujace wydania - mam korygujace przyjecia*/
    wartkor = :wartkor - :wartkorm;
    ilroz = :ilroz - :ilkorm;/* od calej ilosci wydania odejmuje ilosc wydan korygujacych*/
    wartroz = :wartroz - :wartkorm;
    ilprzy = :ilprzy - :ilroz - :ilkor - :ilkorm;/* od calyc h obrotow odejmuje rozchody nie korygujace, przykody korygujace i rozchody korygujace*/
    wartprzy = :wartprzy - :wartroz - :wartkor - :wartkorm;
    /*przychody*/
/*    select sum(DOKUMROZ.ilosc), sum(DOKUMROZ.wartosc) from
      DOKUMNAG join DOKUMPOZ on (DOKUMPOZ.dokument = DOKUMNAG.REF and DOKUMNAG.akcept = 1 and DOKUMNAG.MAGAZYN=:MAGAZYN
                                 and DOKUMNAG.DATA >= :ODDATY AND DOKUMNAG.DATA <= :DODATY
                                 and DOKUMPOZ.KTM = :KTM AND DOKUMPOZ.WERSJA = :WERSJA)
               join DOKUMROZ on (DOKUMROZ.pozycja = DOKUMPOZ.REF)
               join DEFDOKUM on (DOKUMNAG.typ = DEFDOKUM.symbol and DEFDOKUM.WYDANIA=0 AND DEFDOKUM.koryg = 0)
    into :ilprzy, :wartprzy;
    if(:ilprzy is null) then ilprzy = 0;
    if(:wartprzy is null) then wartprzy = 0;*/
    /*rozchody*/
/*    select sum(DOKUMROZ.ilosc), sum(DOKUMROZ.wartosc) from
      DOKUMNAG join DOKUMPOZ on (DOKUMPOZ.dokument = DOKUMNAG.REF and DOKUMNAG.akcept = 1 and DOKUMNAG.MAGAZYN=:MAGAZYN
                                 and DOKUMNAG.DATA >= :ODDATY AND DOKUMNAG.DATA <= :DODATY
                                 and DOKUMPOZ.KTM = :KTM AND DOKUMPOZ.WERSJA = :WERSJA)
               join DOKUMROZ on (DOKUMROZ.pozycja = DOKUMPOZ.REF)
               join DEFDOKUM on (DOKUMNAG.typ = DEFDOKUM.symbol and DEFDOKUM.WYDANIA=1 AND DEFDOKUM.koryg = 0)
    into :ilroz, :wartroz;
    if(:ilroz is null) then ilroz = 0;
    if(:wartroz is null) then wartroz = 0;*/
    /*korekty +*/
/*    select sum(DOKUMROZ.ilosc), sum(DOKUMROZ.wartosc) from
      DOKUMNAG join DOKUMPOZ on (DOKUMPOZ.dokument = DOKUMNAG.REF and DOKUMNAG.akcept = 1 and DOKUMNAG.MAGAZYN=:MAGAZYN
                                 and DOKUMNAG.DATA >= :ODDATY AND DOKUMNAG.DATA <= :DODATY
                                 and DOKUMPOZ.KTM = :KTM AND DOKUMPOZ.WERSJA = :WERSJA)
               join DOKUMROZ on (DOKUMROZ.pozycja = DOKUMPOZ.REF)
               join DEFDOKUM on (DOKUMNAG.typ = DEFDOKUM.symbol and DEFDOKUM.WYDANIA=0 AND DEFDOKUM.koryg = 1)
    into :ilkor, :wartkor;
    if(:ilkor is null) then ilkor = 0;
    if(:wartkor is null) then wartkor = 0;*/
    /*korekty +*/
/*    select sum(DOKUMROZ.ilosc), sum(DOKUMROZ.wartosc) from
      DOKUMNAG join DOKUMPOZ on (DOKUMPOZ.dokument = DOKUMNAG.REF and DOKUMNAG.akcept = 1 and DOKUMNAG.MAGAZYN=:MAGAZYN
                                 and DOKUMNAG.DATA >= :ODDATY AND DOKUMNAG.DATA <= :DODATY
                                 and DOKUMPOZ.KTM = :KTM AND DOKUMPOZ.WERSJA = :WERSJA)
               join DOKUMROZ on (DOKUMROZ.pozycja = DOKUMPOZ.REF)
               join DEFDOKUM on (DOKUMNAG.typ = DEFDOKUM.symbol and DEFDOKUM.WYDANIA=1 AND DEFDOKUM.koryg = 1)
    into :ilkorm, :wartkorm;
    if(:ilkorm is null) then ilkorm = 0;
    if(:wartkorm is null) then wartkorm = 0;
    ilkor = :ilkor - :ilkorm;
    wartkor = :wartkor - :wartkorm;*/
    /*stan kocowy*/
/*    execute procedure MAG_STAN(:magazyn,:ktm,:wersja,NULL,NULL,:DODATY) returning_values :stank, :wartk;*/
    stank = :stanp+:ilprzy + :ilkor - :ilroz - :ilkorm;
    wartk = :wartp + :wartprzy + :wartkor - :wartroz - :wartkorm;
end^
SET TERM ; ^
