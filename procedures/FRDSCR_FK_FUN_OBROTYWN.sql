--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRDSCR_FK_FUN_OBROTYWN(
      KEY1 integer,
      KEY2 integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      ACCMASK ACCOUNT_ID,
      ELEMHIER varchar(40) CHARACTER SET UTF8                           ,
      TIMEELEM integer,
      SUBTIMELEM smallint,
      DICTPOS smallint,
      DICTHIERPOS varchar(40) CHARACTER SET UTF8                           ,
      COMPANYIN integer)
  returns (
      RET numeric(14,2))
   as
declare variable ACCOUNT ACCOUNT_ID;
declare variable YEARID integer;
declare variable WHEREVAL varchar(255);
declare variable COMPANY integer;
declare variable PERIOD varchar(6);
declare variable D1DICTDEF integer;
declare variable D1SYMBOL SYMBOLDIST_ID;
declare variable D2DICTDEF integer;
declare variable D2SYMBOL SYMBOLDIST_ID;
declare variable D3DICTDEF integer;
declare variable D3SYMBOL SYMBOLDIST_ID;
declare variable D4DICTDEF integer;
declare variable D4SYMBOL SYMBOLDIST_ID;
declare variable D5DICTDEF integer;
declare variable D5SYMBOL SYMBOLDIST_ID;
declare variable FRDDRILLDOWNREF integer;
declare variable DDPARAMS varchar(255);
declare variable TMP varchar(20);
begin
  ret = 0;
  if (:ELEMHIER is null) then
    select frdcells.elemhier
      from frdcells
      where frdcells.ref = :key1
      into :elemhier;
  if (:TIMEELEM is null) then
    select frdcells.timelem
      from frdcells
      where frdcells.ref = :key1
      into :timeelem;
  if (:companyin is null) then
    select frdcells.company
      from frdcells
      where frdcells.ref = :key1
      into :companyin;
  select cast(substring(dimelemdef.shortname from 1 for 4) as integer), substring(dimelemdef.shortname from 1 for 6)
    from dimelemdef
    where dimelemdef.ref = :timeelem
    into :yearid, :period;
  account = '';
  whereval = '';
  ddparams = '';
  if (:accmask is not null and :accmask <> '') then
  begin
    execute procedure FRDSCR_FK_FUN_GETACC(:accmask,:elemhier,:companyin,:yearid,:dictpos,:dicthierpos)
      returning_values(:company, :account, :d1dictdef, :d1symbol, :d2dictdef, :d2symbol,
        :d3dictdef, :d3symbol, :d4dictdef, :d4symbol, :d5dictdef, :d5symbol);

    ddparams = 'Okres='||:period||'; Konto='||:account;

    if(:d1dictdef is not null) then
    begin
      select nazwa from slodef where ref = :d1dictdef into :tmp;
      ddparams = :ddparams||'; Wyr1='||:tmp||'; Poz1='||:d1symbol;
    end

    if(:d2dictdef is not null) then
    begin
      select nazwa from slodef where ref = :d2dictdef into :tmp;
      ddparams = :ddparams||'; Wyr2='||:tmp||'; Poz2='||:d2symbol;
    end

    if(:d3dictdef is not null) then
    begin
      select nazwa from slodef where ref = :d3dictdef into :tmp;
      ddparams = :ddparams||'; Wyr3='||:tmp||'; Poz3='||:d3symbol;
    end

    if(:d4dictdef is not null) then
    begin
      select nazwa from slodef where ref = :d4dictdef into :tmp;
      ddparams = :ddparams||'; Wyr4='||:tmp||'; Poz4='||:d4symbol;
    end

    if(:d5dictdef is not null) then
    begin
      select nazwa from slodef where ref = :d5dictdef into :tmp;
      ddparams = :ddparams||'; Wyr5='||:tmp||'; Poz5='||:d5symbol;
    end

    if (:dictpos is null or :dictpos = 0) then
    begin
      dictpos = 0;
      if (:account is not null and :account <> '' and :dictpos = 0) then
        select sum(turnovers.debit)
          from turnovers
          where turnovers.period = :period and turnovers.yearid = :yearid
            and turnovers.account like :account||'%'
            and ((turnovers.company = :company and :company is not null)
               or (:company is null))
        into :ret;
      if (:ret is null) then ret = 0;
      execute procedure GEN_REF('FRDDRILLDOWN') returning_values :frddrilldownref;
      insert into frddrilldown(REF, frdcell, functionname, actionid, fvalue, ddparams, valtype, descript)
        values (:frddrilldownref, :key1, 'OBROTYWN', 'BrowseDECREESonTURNOVERSTREE', :ret, :ddparams, :key2, 'OBROTYWN');
      insert into frddrilldownp(frddrilldown, paramname, paramvalue) values (:frddrilldownref, 'TURNOVERSTREE_ACCOUNT',:account);
      insert into frddrilldownp(frddrilldown, paramname, paramvalue) values (:frddrilldownref, 'TURNOVERSTREE_FROMPERIOD',:period);
      insert into frddrilldownp(frddrilldown, paramname, paramvalue) values (:frddrilldownref, 'TURNOVERSTREE_TOPERIOD',:period);
    end

    else if (:account is not null and :account <> '' and :dictpos > 0) then
    begin
      select sum(D.debit)
        from decrees D
        where D.account like :account||'%' and D.status > 1 and D.period = :period
          and ((exists(select ref from distpos D1 where D1.decrees = D.ref and D1.dictdef = :d1dictdef
                and D1.symbol = :d1symbol)) or (:d1symbol is null))
          and ((exists(select ref from distpos D2 where D2.decrees = D.ref and D2.dictdef = :d1dictdef
                and D2.symbol = :d2symbol)) or (:d2symbol is null))
          and ((exists(select ref from distpos D3 where D3.decrees = D.ref and D3.dictdef = :d3dictdef
                and D3.symbol = :d3symbol)) or (:d3symbol is null))
          and ((exists(select ref from distpos D4 where D4.decrees = D.ref and D4.dictdef = :d4dictdef
                and D4.symbol = :d4symbol)) or (:d4symbol is null))
          and ((exists(select ref from distpos D5 where D5.decrees = D.ref and D5.dictdef = :d5dictdef
                and D5.symbol = :d5symbol)) or (:d5symbol is null))
        into :ret;
      if (:ret is null) then ret = 0;
      execute procedure GEN_REF('FRDDRILLDOWN') returning_values :frddrilldownref;
      insert into frddrilldown(REF, frdcell, functionname, actionid, fvalue, ddparams, valtype, descript)
        values (:frddrilldownref, :key1, 'OBROTYWN', 'BrowseDistDecrees', :ret, :ddparams, :key2, 'OBROTYWN');
      insert into frddrilldownp(frddrilldown, paramname, paramvalue) values (:frddrilldownref, 'ACCOUNTFORBKDECREES',:account);
      insert into frddrilldownp(frddrilldown, paramname, paramvalue) values (:frddrilldownref, 'PERIODFORBKDECREES',:period);
      insert into frddrilldownp(frddrilldown, paramname, paramvalue) values (:frddrilldownref, 'TOPERIODFORBKDECREES',:period);
      insert into frddrilldownp(frddrilldown, paramname, paramvalue) values (:frddrilldownref, 'D1DICTDEF',coalesce(:d1dictdef,0));
      insert into frddrilldownp(frddrilldown, paramname, paramvalue) values (:frddrilldownref, 'D1DICTPOS',coalesce(:d1symbol,''));
      insert into frddrilldownp(frddrilldown, paramname, paramvalue) values (:frddrilldownref, 'D2DICTDEF',coalesce(:d2dictdef,0));
      insert into frddrilldownp(frddrilldown, paramname, paramvalue) values (:frddrilldownref, 'D2DICTPOS',coalesce(:d2symbol,''));
      insert into frddrilldownp(frddrilldown, paramname, paramvalue) values (:frddrilldownref, 'D3DICTDEF',coalesce(:d3dictdef,0));
      insert into frddrilldownp(frddrilldown, paramname, paramvalue) values (:frddrilldownref, 'D3DICTPOS',coalesce(:d3symbol,''));
      insert into frddrilldownp(frddrilldown, paramname, paramvalue) values (:frddrilldownref, 'D4DICTDEF',coalesce(:d4dictdef,0));
      insert into frddrilldownp(frddrilldown, paramname, paramvalue) values (:frddrilldownref, 'D4DICTPOS',coalesce(:d4symbol,''));
    end
  end
  else
    ret = 0;
  suspend;
end^
SET TERM ; ^
