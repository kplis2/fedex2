--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_PRSCHEDGUIDEDET_DEPEND_ADD(
      PRSCHEDGUIDEDETFROM integer,
      PRSCHEDGUIDEDETTO integer,
      QUANTITY numeric(14,4),
      OUT smallint)
   as
declare variable qfrom numeric(14,4);
declare variable overfrom numeric(14,4);
declare variable qto numeric(14,4);
declare variable overto numeric(14,4);
declare variable q numeric(14,4);
declare variable o numeric(14,4);
declare variable newreffrom integer;
declare variable newrefto integer;
begin     
  newreffrom = null;
  newrefto = null;
  select coalesce(d.quantity,0), coalesce(d.overlimit,0)
    from prschedguidedets d
    where d.ref = :prschedguidedetfrom
    into qfrom, overfrom;
  select coalesce(d.quantity,0), coalesce(d.overlimit,0)
    from prschedguidedets d
    where d.ref = :prschedguidedetto
    into qto, overto;
  -- szukamy mniejszej rozpiski, aby bylo wiadomo ile podzielic
  if (quantity > qfrom + overfrom or quantity > qto + overto) then
    exception prschedguidedet_error 'Próba rozpisania zbyt dużej ilości';
  -- wskazalismy rozpiske rozchodowa dla przychodowej
  if (quantity < qfrom + overfrom) then
  begin
    if (quantity > qfrom) then
    begin
      q = qfrom;
      o = quantity - q;
    end else
    begin
      q = quantity;
      o = 0;
    end
    execute PROCEDURE PRSCHEDGUIDEDET_CLONE(prschedguidedetfrom,q,o)
        returning_values newreffrom;
    if (newreffrom is not null) then
      prschedguidedetfrom = newreffrom;  end
  if (quantity < qto + overto) then
  begin
    if (quantity > qto) then
    begin
      q = qto;
      o = quantity - q;
    end else
    begin
      q = quantity;
      o = 0;
    end
    execute PROCEDURE PRSCHEDGUIDEDET_CLONE(prschedguidedetto,q,o)
        returning_values newrefto;
    if (newrefto is not null) then
      prschedguidedetto = newrefto;
  end
  update prschedguidedets d set d.sref = :prschedguidedetto, d.accept = 1 where ref = :prschedguidedetfrom;
  update prschedguidedets d set d.sref = :prschedguidedetfrom, d.accept = 1 where ref = :prschedguidedetto;
end^
SET TERM ; ^
