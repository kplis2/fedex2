--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_AWH(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      COL integer)
  returns (
      AMOUNT numeric(14,2))
   as
declare variable FROMDATE timestamp;
declare variable TODATE timestamp;
declare variable ESYSTEM_START date;
begin
--DU: Personel - do liczenia godzin nieobecnosci roboczych

  execute procedure ef_adh(:employee,:payroll, :prefix, 'WH', :col, 0)
    returning_values :amount;

  if (amount is null) then amount = 0;

  suspend;
end^
SET TERM ; ^
