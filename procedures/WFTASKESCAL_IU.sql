--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WFTASKESCAL_IU(
      REF WFTASKESCAL_ID,
      OBJECTUUID NEOS_ID,
      WFTASKREF WFTASK_ID,
      UUID NEOS_ID,
      DUETO TIMESTAMP_ID,
      SUPERIORROLE NEOS_ID,
      WFTASKUUID NEOS_ID)
  returns (
      REFOUT WFTASKESCAL_ID)
   as
begin
  if (exists(select ref from wftaskescal where (ref = :ref))) then
  begin
    update wftaskescal
    set objectuuid = :objectuuid,
        wftaskref = :wftaskref,
        uuid = :uuid,
        dueto = :dueto,
        superiorrole = :superiorrole,
        wftaskuuid = :wftaskuuid
    where (ref = :ref);
    refout=ref;
  end
  else
    insert into wftaskescal (
        objectuuid,
        wftaskref,
        uuid,
        dueto,
        superiorrole,
        wftaskuuid)
    values (
        :objectuuid,
        :wftaskref,
        :uuid,
        :dueto,
        :superiorrole,
        :wftaskuuid)
    returning ref into :refout;
  suspend;
end^
SET TERM ; ^
