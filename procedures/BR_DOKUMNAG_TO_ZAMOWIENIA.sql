--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_DOKUMNAG_TO_ZAMOWIENIA(
      REFZAM integer)
  returns (
      REF integer,
      SYMBOL varchar(40) CHARACTER SET UTF8                           ,
      TYP varchar(3) CHARACTER SET UTF8                           )
   as
declare variable org_ref integer;
begin
  for select P.FROMZAM,n.symbol ,n.typ
    from DOKUMPOZ P
    join dokumnag N on (n.ref =P.dokument)
    where P.fromzam=:REFZAM
    into :ref, :symbol, :TYP
  do begin
    suspend;
  end
end^
SET TERM ; ^
