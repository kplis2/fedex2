--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXP2PLATNIK_GET_IDDOKUM(
      ZUSNAME STRING10)
  returns (
      ID_DOKUMENTU STRING10)
   as
begin
  /* procedura zwraca umery indywidualne deklaracji zusowskich
     pole id_dokumentu jest wymagane na deklaracjach zusowskich,
     ale brakuje specyfikacji dotyczacej sposobu jego uzupelniania
     procedura na chwile obecna zwraca zawsze 1 w przyszlosci zostanie dostosowana do wymogów platnika */
  id_dokumentu = 1;
  suspend;
end^
SET TERM ; ^
