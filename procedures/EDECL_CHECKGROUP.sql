--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDECL_CHECKGROUP(
      DECLREF integer,
      COMPANY COMPANIES_ID = null)
  returns (
      RET smallint)
   as
declare variable EYEAR integer;
begin
  ret = 0;

  if (coalesce(company,0) = 0) then
    execute procedure get_global_param('CURRENTCOMPANY')
      returning_values :company;

  select eyear from edeclarations
    where ref = :declref
    into eyear;

  --ponizsze sprawdzenie powinno byc zgodne z warunkami w akcji: eDecl_AddToGroup_params
  if (not exists(select first 1 1 from edeclarations
       where groupname <> ''
         and person is null
         and company = :company
         and state = 0
         and eyear = :eyear)
  ) then
    ret = 1;

  suspend;
end^
SET TERM ; ^
