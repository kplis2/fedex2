--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ECONTRPAYROLLS_CALCULATE(
      ECONPAYLISTNAG integer)
   as
declare variable sumpoints numeric(14,4);
declare variable summoney numeric(14,4);
declare variable bdate date;
declare variable edate date;
declare variable sumtime numeric(14,2);
declare variable operator integer;
begin               
  select sum(ECONTRPAYROLLSPOS.points), sum(ECONTRPAYROLLSPOS.topay)
    from ECONTRPAYROLLSPOS
    where ECONTRPAYROLLSPOS.econtrpayrollnag = :econpaylistnag
      and ECONTRPAYROLLSPOS.accord < 2
    into :sumpoints, :summoney;
  select econtrpayrollsnag.bdate, econtrpayrollsnag.edate, econtrpayrollsnag.operator
    from econtrpayrollsnag
    where econtrpayrollsnag.ref = :ECONPAYLISTNAG
    into :bdate, :edate, :operator;
  select sum(epresencelistnag.jobtimeq)
    from epresencelistnag
    where epresencelistnag.operator = :operator and cast(epresencelistnag.startat as date) >= :bdate
      and cast(epresencelistnag.stopat as date) <= :edate and epresencelistnag.wastoday = 1
    into :sumtime;
  update ECONTRPAYROLLSNAG set ECONTRPAYROLLSNAG.sumpoints = :sumpoints,
      ECONTRPAYROLLSNAG.sumworktime = :sumtime,
      ECONTRPAYROLLSNAG.summoney = :summoney
    where ECONTRPAYROLLSNAG.ref = :econpaylistnag;
end^
SET TERM ; ^
