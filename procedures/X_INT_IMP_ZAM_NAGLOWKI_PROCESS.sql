--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_INT_IMP_ZAM_NAGLOWKI_PROCESS(
      SESJAREF SESJE_ID,
      TABELA STRING35,
      REF INTEGER_ID,
      BLOKUJZALEZNE SMALLINT_ID = 0,
      TYLKONIEPRZETWORZONE SMALLINT_ID = 0,
      AKTUALIZUJSESJE SMALLINT_ID = 1)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable stabela string35;
--declare variable tmptabela string35;
declare variable sprocedura string255;
declare variable szrodlo zrodla_id;
declare variable skierunek smallint_id;
declare variable sesja sesje_id;
declare variable sql memo;
declare variable tmpsql memo;
declare variable tmpstatus smallint_id;
declare variable tmpmsg string255;
declare variable error_row smallint_id;
declare variable errormsg_row string255;
declare variable errormsg_all string255;
declare variable statuspoz smallint_id;
declare variable NZ_REF INTEGER_ID;
declare variable NZ_WYDANIA INTEGER_ID;
declare variable NZ_ZEWNETRZNY INTEGER_ID;
declare variable NZ_MAGAZYN STRING20;
declare variable NZ_MAGAZYN2 STRING20;
declare variable nz_tmp_typdok defdokum_id;
declare variable SPOSZAP PLATNOSC_ID;
declare variable SPOSDOST SPOSDOST_ID;
declare variable DOKUMENT DOKUMNAG_ID;
declare variable ODDZIAL ODDZIAL_ID;
declare variable COMPANY COMPANIES_ID;
declare variable magazyn defmagaz_id;
declare variable mag2 defmagaz_id;
begin
  sesja = :sesjaref;

  --pobranie parametrow sesji
  select s.tabela, s.procedura, s.zrodlo, s.kierunek
    from int_sesje s
    where ref = :sesja
  into :stabela, :sprocedura, :szrodlo, :skierunek;

  if (coalesce(:tabela,'') <> '') then --jesli podano tabele na wejsciu traktuje ja nadrzednie
    stabela = :tabela;

  --wlasciwe przetworzenie
  oddzial = null;
  company = null;
  select oddzial, company from int_zrodla where ref = :szrodlo
  into :oddzial, :company;

  if (:oddzial is null or :company is null) then
  begin
    status = -1;
    msg = 'Nie ustawiono parametrów konfiguracyjnych źródła danych: oddział, firma';
    exit; --EXIT
  end

  for
    select n.ref, n.wydania, n.zewnetrzny
      from int_imp_zam_naglowki n
      where n.sesja = :sesja
        and (n.ref = :ref or :ref is null)
      order by ref
    into :nz_ref, :nz_wydania, :nz_zewnetrzny
  do begin
    --if (:nz_wydania = 0 and :nz_zewnetrzny = 1) then
    --  execute procedure x_int_imp_zamowienia(:sesja, :stabela, :nz_ref,
    --      :blokujzalezne, :tylkonieprzetworzone, :aktualizujsesje)
    --    returning_values :status, :msg;
    --else
      execute procedure x_int_imp_dokumenty(:sesja, :stabela, :nz_ref,
          :blokujzalezne, :tylkonieprzetworzone, :aktualizujsesje)
        returning_values :status, :msg;
  end

  suspend;
end^
SET TERM ; ^
