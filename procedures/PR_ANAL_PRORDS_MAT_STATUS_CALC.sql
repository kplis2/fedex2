--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_ANAL_PRORDS_MAT_STATUS_CALC(
      PREFIN integer,
      PRREGIN varchar(40) CHARACTER SET UTF8                           ,
      ORDERSREGIN varchar(40) CHARACTER SET UTF8                           )
   as
declare variable stock numeric(14,4);
declare variable ordered numeric(14,4);
declare variable quantity numeric(14,4);
declare variable ref integer;
declare variable frompozzam integer;
declare variable fromnagzam integer;
declare variable indate date;
declare variable stm varchar(4096);
declare variable NREF integer;
declare variable ORDSYMB varchar(40);
declare variable PREF integer;
declare variable GOOD varchar(40);
declare variable GOODDICT varchar(255);
declare variable VERS integer;
declare variable ONDATE date;
declare variable MATREF integer;
declare variable MATWH varchar(3);
declare variable MATGOOD varchar(40);
declare variable MATGOODDICT varchar(255);
declare variable MATVERS integer;
declare variable MATQUANTITY numeric(14,4);
declare variable MATSTATUS smallint;
declare variable MATDATE date;
begin
  if (prefin is null) then
  begin
    delete from PRORDANALMAT where connectionid = current_connection;
    -- insert dla zlecen produkcyjnych
    stm = 'select n.id, n.ref, p.ref, p.wersjaref, coalesce(p.termdost,n.termdost)
        from nagzam n
          left join pozzam p on (p.zamowienie = n.ref)
          left join towary t on (t.ktm = p.ktm)
          left join typzam z on (z.symbol = n.typzam)
        where position('';''||n.rejestr||'';'' in '''||:prregin||''') > 0 and p.out = 0 and z.wydania = 3
          and p.ilosc - p.ilzreal > 0
        order by coalesce(p.termdost,n.termdost)';
    for
      execute statement stm
        into ordsymb, nref, pref, vers, ondate
    do begin
      for
        select pr.ref, p.ktm, p.wersjaref, p.iloscm - p.ilzrealm, p.magazyn
          from PR_POZZAM_CASCADE(:pref,0,0,0,1) pr
            left join pozzam p on (p.ref = pr.ref)
          where pr.postypeout = 1 and pr.prnagzamout = :nref
          into matref, matgood, matvers, matquantity, matwh
      do begin
        insert into prordanalmat (nref, prefprod, pref, good, vers, ondate, quantity, wh)
          values (:nref, :pref, :matref, :matgood, :matvers, :ondate, :matquantity, :matwh);
      end
    end
    -- insert dla zamówien sprzedazy TODO

    -- sciagamy ze stanow to co jest i to co przyjdzie
    for
      select p.wh, p.vers
        from prordanalmat p
        group by p.wh, p.vers
        into matwh, matvers
    do begin
      -- obliczenie stanu poczatkowego
      stock = 0;
      ordered = 0;
      select sum(s.ilosc)
        from stanyil s where s.magazyn = :matwh and s.wersjaref = :matvers
        into stock;
      if (stock is null) then stock = 0;
      -- rozdysponowanie stanu poczatkowego z magazynu
      while (stock > 0 and exists(select first 1 1 from prordanalmat p where p.wh = :matwh and p.vers = :matvers and p.quantityav = 0))
      do begin
        ref = null;
        quantity = null;
        select first 1 p.ref, p.quantity
          from prordanalmat p
          where p.wh = :matwh and p.vers = :matvers and p.quantityav = 0
          order by p.ondate
          into ref, quantity;
        if (coalesce(pref,0) = 0 or coalesce(quantity,0) = 0) then
          stock = 0;
        else
        begin
          if (stock >= quantity) then
          begin
            update prordanalmat set status = 0, qbefore = :stock, qafter = :stock - :quantity, quantityav = quantity
              where ref = :ref;
            stock = stock - quantity;
          end
          else begin
            insert into prordanalmat (nref, prefprod, pref, good, vers, ondate, quantity, wh)
              select nref, prefprod, pref, good, vers, ondate, :quantity - :stock, wh
                from prordanalmat
                where ref = :ref;
            update prordanalmat set status = 0, qbefore = :stock, qafter = 0, quantity = :stock, quantityav = :stock
              where ref = :ref;
            stock = 0;
          end
        end
      end
      -- rozdysponowanie stanu z zamowien
      stm = 'select n.termdost, p.iloscm - p.ilzrealm, p.ref, n.ref
               from nagzam n
                 left join pozzam p on (p.zamowienie = n.ref)
               where position('';''||n.rejestr||'';'' in '''||:ordersregin||''') > 0 and n.typ < 3
                 and p.wersjaref = '||:matvers||' and p.magazyn = '''||:matwh||''' and p.iloscm - p.ilzrealm > 0
               order by n.termdost';
      for
        execute statement stm
          into indate, ordered, frompozzam, fromnagzam
      do begin
        if (ordered is null) then ordered = 0;
        while (ordered > 0 and exists(select first 1 1 from prordanalmat p where p.wh = :matwh and p.vers = :matvers and p.quantityav = 0))
        do begin
          ref = null;
          quantity = null;
          select first 1 p.ref, p.quantity
            from prordanalmat p
            where p.wh = :matwh and p.vers = :matvers and p.quantityav = 0
            order by p.ondate
            into ref, quantity;
          if (coalesce(pref,0) = 0 or coalesce(quantity,0) = 0) then
            ordered = 0;
          else
          begin
            if (ordered >= quantity) then
            begin
              update prordanalmat set qbefore = :ordered, qafter = :ordered - :quantity, quantityav = quantity,
                  frompref = :frompozzam, fromnref = :fromnagzam, maxavdate = :indate,
                  status = case when :indate <= ondate then 0 else 1 end
                where ref = :ref;
              ordered = ordered - quantity;
            end
            else begin
              insert into prordanalmat (nref, prefprod, pref, good, vers, ondate, quantity, wh)
                select nref, prefprod, pref, good, vers, ondate, :quantity - :ordered, wh
                  from prordanalmat
                  where ref = :ref;
              update prordanalmat set qbefore = :ordered, qafter = 0, quantity = :ordered, quantityav = :ordered,
                  frompref = :frompozzam, fromnref = :fromnagzam, maxavdate = :indate,
                  status = case when :indate <= ondate then 0 else 1 end
                where ref = :ref;
              ordered = 0;
            end
          end
        end
        if(not exists(select first 1 1 from prordanalmat p where p.wh = :matwh and p.vers = :matvers and p.quantity > 0)) then
          break;
      end
    end
  end
end^
SET TERM ; ^
