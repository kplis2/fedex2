--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRPSNS_RECALC_COUNTORD(
      FRPSNS_FRHDR FRHDRS_ID)
   as
declare variable frpsns_ref type of column frpsns.ref;
declare variable frpsns_countord type of column frpsns.countord;
begin

  /* TS BS106559
     Przywraca domyslna kolejnosc obliczania wierszy w sprawozdaniu finansowym.
     Dzieli sie na dwie fazy:
     - resetuje wszystkie kolejnosci obliczen pol, ktore nie zaleza
       od innych wierszy (nie sa wierszami sumujacymi inne wiersze),
     - dla kazdego wiersza sumujacego szuka najdluzszej sciezki
       i zwraca jej dlugosc jako minimalna wartosc kolejnosci obliczania
       danego wiersza (najdluzszy lancuch zaleznosci + countord liscia).
  */
  update frpsns set countord = 1
    where frhdr = :frpsns_frhdr
      and algorithm <> 2;
  for
    select f.ref
      from frpsns f
        where f.frhdr = :frpsns_frhdr
          and f.algorithm = 2
      into : frpsns_ref
  do begin
    execute procedure frpsns_count_min_ord(:frpsns_ref)
      returning_values :frpsns_countord;
    update frpsns f set f.countord = :frpsns_countord
      where f.countord <> :frpsns_countord
        and f.ref = :frpsns_ref;
  end
end^
SET TERM ; ^
