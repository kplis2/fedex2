--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_GETCOMMENT(
      OBJECTNAME varchar(255) CHARACTER SET UTF8                           ,
      OBJECTTYPE integer)
  returns (
      RESULT blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           )
   as
declare variable tablename varchar(100);
declare variable descript  blob sub_type 1         segment size 80;
declare variable quotedobjectname varchar(255);
begin
  if(position('.' in :objectname) > 0) then
  begin
    tablename = substring(:objectname from 1 for position('.' in :objectname)-1);
    objectname = substring(:objectname from position('.' in :objectname)+1);
  end
  quotedobjectname = (select outname from sys_quote_reserved(:objectname));
  result = 'COMMENT ON ';
  if(objecttype = 1) then
  begin
    select rdb$description from rdb$fields where rdb$field_name = :objectname into :descript;
    result = result||'DOMAIN '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 2) then
  begin
    select rdb$description from rdb$generators where rdb$generator_name = :objectname into :descript;
    result = result||'SEQUENCE '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 3) then
  begin
    select rdb$description from rdb$exceptions where rdb$exception_name = :objectname into :descript;
    result = result||'EXCEPTION '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 4) then
  begin
    select rdb$description from rdb$relations where rdb$relation_name = :objectname and (rdb$relation_type is NULL or rdb$relation_type in (0,4,5)) into :descript;
    result = result||'TABLE '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 5) then
  begin
    select rdb$description from rdb$relation_fields where rdb$relation_name = :tablename
                                                          and rdb$field_name = :objectname and rdb$view_context is null into :descript;
    result = result||'COLUMN '||:tablename||'.'||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 6) then
  begin
    select rdb$description from rdb$procedures where rdb$procedure_name = :objectname into :descript;
    result = result||'PROCEDURE '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 7) then
  begin
    select rdb$description from rdb$relations where rdb$relation_name = :objectname and rdb$relation_type = 1 into :descript;
    result = result||'VIEW '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 8) then
  begin
    select rdb$description from rdb$triggers where rdb$trigger_name = :objectname into :descript;
    result = result||'TRIGGER '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 9) then
  begin
    select rdb$description from rdb$procedures where rdb$procedure_name = :objectname into :descript;
    result = result||'PROCEDURE '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 10 or objecttype = 11 or objecttype = 12 or objecttype = 13)  then
  begin
    select rdb$description from rdb$indices where rdb$index_name = :objectname and rdb$relation_name = :tablename into :descript;
    result = result||'INDEX '||:quotedobjectname||' IS '''||:descript||''';';
  end
  else if(objecttype = 14) then
    select val from s_appini where section = :objectname and ident = 'Signature' into :result;
  else if(objecttype = 15) then
    select val from s_appini where section = :objectname and ident = 'Signature' into :result;
  else if(objecttype = 16) then
    select val from s_appini where section = :objectname and ident = 'Signature' into :result;
  else if(objecttype = 17) then
    select val from s_appini where section = :objectname and ident = 'Signature' into :result;
  else if(objecttype = 18) then
    select val from s_appini where section = :objectname and ident = 'Signature' into :result;

  suspend;
end^
SET TERM ; ^
