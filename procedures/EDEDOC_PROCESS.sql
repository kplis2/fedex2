--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDEDOC_PROCESS(
      REF integer)
  returns (
      STATUS smallint)
   as
declare variable EDERULE type of EDERULES_ID;
declare variable PROCEDURA varchar(255);
declare variable SQL varchar(255);
declare variable OTABLE varchar(255);
declare variable OREF integer;
declare variable AKTUOPERATOR integer;
declare variable DIRECTION smallint;
declare variable NEEDSIGN smallint;
declare variable PID integer;
declare variable PPARENT integer;
declare variable PNAME STRING;
declare variable PPARAMS STRING2048;
declare variable PVAL STRING1024;
declare variable PATTRIBUTE STRING;
declare variable PSPLITIDX integer;
begin
  status = 0;
  select pvalue from GET_GLOBAL_PARAM('AKTUOPERATOR') into :aktuoperator;
  select ederule,otable,oref,direction
  from EDEDOCSH where ref=:ref
  into :ederule,:otable,:oref,:direction;
  if(:direction=1) then begin
    -- eksport
    select genprocedure, needsign from ederules where ref=:ederule into :procedura, :needsign;
    if(:procedura is not null and :procedura<>'') then begin
      delete from EDEDOCSP where ededoch =:ref;

      sql = 'select ID,PARENT,NAME,PARAMS,VAL from '||:procedura||'('''||:otable||''','||:oref||')';
      for execute statement sql
      into :pid, :pparent, :pname, :pparams, :pval
      do begin
         if (strlen(:pparams) > 255) then begin
            insert into ededocsp(EDEDOCH,ID,PARENT,NAME,PARAMS,VAL)
            values (:ref, :pid, :pparent, :pname, '', :pval);

            pparams = replace(replace(:pparams, ascii_char(10), ' '), ascii_char(13), '');
            while (strlen(:pparams) > 0) do begin
                psplitidx = position(' ' in :pparams);
                if (:psplitidx > 0) then begin
                    pattribute = substring(:pparams from 1 for psplitidx);
                    pparams = trim(substring(:pparams from strlen(:pattribute)));
                end
                else begin
                    pattribute = :pparams;
                    pparams = '';
                end

                insert into ededocpattr(EDEDOCH,EDEDOCP,PARAM)
                values (:ref, :pid, trim(:pattribute));
            end
         end
         else begin
            insert into ededocsp(EDEDOCH,ID,PARENT,NAME,PARAMS,VAL)
            values (:ref, :pid, :pparent, :pname, cast(:pparams as varchar(255)), :pval);
         end
      end

      update ededocsh set status=1, signstatus = :needsign, createdate = current_timestamp(0), createoper = :aktuoperator where ref=:ref;
      status = 1;
    end
  end else begin
    -- import
    select importprocedure from ederules where ref=:ederule into :procedura;
    if(:procedura is not null and :procedura<>'') then begin
      sql = 'select otable, oref from '||:procedura||'('||:ref||')';
      execute statement sql into :otable, :oref;
      update ededocsh set status=4, finishdate = current_timestamp(0),
      finishoper = :aktuoperator, otable=:otable, oref=:oref where ref=:ref;
      status = 1;
    end
  end
end^
SET TERM ; ^
