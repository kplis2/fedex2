--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_UPS_VOID_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable isShippingList smallint;
declare variable UserName string;
declare variable UserPassword string;
declare variable AccesLicenseNumber string;
declare variable ShipmentIdentificationNumber string;
declare variable GRANDPARENT integer;
begin
  --Sprawdzanie czy istieje dokument spedycyjny
  select first 1 1
    from listywysd l
    where l.ref = :oref
  into :isShippingList;

  if(isShippingList is null) then
    exception ede_ups_listywysd_brak;

  val = 1;
  parent = null;
  params = null;
  id = 0;
  name = 'VoidShipment';
  suspend;

  grandparent = id;

  select k.klucz, k.login, k.haslo
    from spedytkonta k
    where k.spedytor = 'UPS'
  into :AccesLicenseNumber, :username, :userpassword;

  val = UserName;
  name = 'UserName';
  id = id + 1;
  parent = GrandParent;
  suspend;

  val = UserPassword;
  name = 'UserPassword';
  id = id + 1;
  parent = GrandParent;
  suspend;

  val = AccesLicenseNumber;
  name = 'AccesLicenseNumber';
  id = id + 1;
  parent = GrandParent;
  suspend;

  val = 'TEST'; --jesli chcesz testowac zmien na TEST
  name = 'ServiceUrlType';
  id = id + 1;
  parent = GrandParent;
  suspend;

  val = '1'; --stala, nie zmieniac
  name = 'RequestOption';
  id = id + 1;
  parent = GrandParent;
  suspend;

  select first 1 l.symbolsped
    from listywysd l
    where l.ref = :oref
  into :ShipmentIdentificationNumber;

  val = ShipmentIdentificationNumber;
  name = 'ShipmentIdentificationNumber';
  id = id + 1;
  parent = GrandParent;
  suspend;
end^
SET TERM ; ^
