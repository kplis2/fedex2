--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_FAKT_ZBIORCZE_PAR(
      REFNAGFAK integer)
  returns (
      SYMBOL varchar(500) CHARACTER SET UTF8                           ,
      DONUMPAR varchar(160) CHARACTER SET UTF8                           ,
      KWZALPAR numeric(14,2),
      KWZAPPAR numeric(14,2),
      KWZAPKARTAPAR numeric(14,2),
      ZAPLACONO numeric(14,2))
   as
declare variable symbolpar varchar(20);
declare variable donumparpar varchar(20);
declare variable datapar timestamp;
begin
  symbol = '';
  donumpar = '';
  select sum(kwotazal), sum(kwotazap), sum(kwotazapkarta), sum(zaplacono) from nagfak where reffak = :refnagfak
    into :kwzalpar,kwzappar,:kwzapkartapar,:zaplacono;
  for select symbol, numpar, datafisk from nagfak where reffak = :refnagfak
    into :symbolpar, donumparpar, datapar
  do begin
    if(:donumparpar is null) then donumparpar = '';
    if(:symbol<>'') then symbol = :symbol || '; ';
    symbol = symbol||:symbolpar;
    if(:donumpar<>'') then donumpar = :donumpar || '; ';
    if(:donumparpar<>'') then donumpar = donumpar||:donumparpar||'/'||cast(substring(:datapar from 1 for 10) as varchar(30));
  end
  if(:kwzalpar is null) then kwzalpar = 0;
  if(:kwzappar is null) then kwzappar = 0;
  if(:kwzapkartapar is null) then kwzapkartapar = 0;
  if(:zaplacono is null) then zaplacono = 0;
  suspend;
end^
SET TERM ; ^
