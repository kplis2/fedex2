--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_CREATE_FAKT_TO_PARPRO(
      PARAGON integer,
      REJFAK char(3) CHARACTER SET UTF8                           ,
      TYPFAK char(3) CHARACTER SET UTF8                           ,
      DATAWYST timestamp,
      STANOWISKO varchar(10) CHARACTER SET UTF8                           ,
      OPERATOR integer,
      PROFORMA integer,
      FAKTURA integer)
  returns (
      REFFAK integer)
   as
declare variable PARAG integer;
declare variable FISSTAN integer;
declare variable FISTYP integer;
declare variable REFAK integer;
declare variable KOREKTA integer;
declare variable NEWFAK integer;
declare variable SYMBOL varchar(25);
declare variable TYPNIEO integer;
declare variable NIEO integer;
declare variable NIEOBROT smallint;
declare variable PAROBR varchar(20);
declare variable GRUPADOK integer;
declare variable HAVEFAKE smallint;
declare variable FAKE smallint;
declare variable ALTTOPOZ integer;
declare variable ALTTOPOZN integer;
declare variable POZYCJA integer;
begin
  REFFAK = -1;
  PARAG = 0;
--SN
  select STANSPRZED.FISKALNY, TYPFAK.FISKALNY, NAGFAK.REFFAK, TYPFAK.NIEOBROT, NAGFAK.NIEOBROT, nagfak.grupadok, nagfak.korekta
      from STANSPRZED join NAGFAK  on ( NAGFAK.STANOWISKO = STANSPRZED.STANOWISKO) join TYPFAK on ( NAGFAK.TYP = TYPFAK.SYMBOL)
      where NAGFAK.REF = :paragon
      into :fisstan, :fistyp, :refak, :typnieo, :nieo, :grupadok, :korekta;
--  select count(*) from NAGFAK where REFK = :paragon into :refk;
/*  if(:fisstan is null or (:fisstan = 0)) then exception CREATE_FAK_TO_PAR_NOSTANFIS;*/
  if(:proforma =1) then begin
    if(:nieo <> 3 and :typnieo <> 1) then exception CREATE_FAK_TO_PAR_NOTYPPRO;
  end else
     if(:fistyp is null or (:fistyp = 0)) then begin
       if(:paragon <> :grupadok) then
          select typfak.fiskalny
          from TYPFAK right join NAGFAK on (TYPFAK.SYMBOL = NAGFAK.TYP)
          where NAGFAK.REF=:grupadok into :fistyp;
       if(:fistyp = 0 or (:fistyp is null)) then
        exception CREATE_FAK_TO_PAR_NOTYPFIS;
     end
  if(:refak > 0) then exception CREATE_FAK_TO_PAR_HASFAK;
  if(korekta > 0) then exception CREATE_FAK_TO_PAR_HASKOR;
  nieobrot = 0;
  execute procedure GETCONFIG('PARAGONOBROT') returning_values :parobr;
  if(:proforma <> 1) then begin
    nieobrot = 1;
    if(:parobr='1') then nieobrot = 3;
  end
  if(faktura = 0) then begin
    execute procedure GEN_REF('NAGFAK') returning_values :newfak;
    insert into NAGFAK(REF,ODDZIAL,STANOWISKO,REJESTR,TYP, NUMER,SYMBOL, ODBIORCAID, ODBIORCA, DULICA, DMIASTO, DKODP,
                     KLIENT, UZYKLI, DATA, DATASPRZ, SPOSZAP, TERMIN, TERMZAP, RABAT,
                     UWAGI,WALUTA, TABKURS, KURS,
                     WALUTOWA, PKURS, GRUPADOK,
                     OPERATOR, SPRZEDAWCA , NIEOBROT,BN, KOSZT, KOSZTZAK, PKOSZT, CKONTRAKTY, SPOSDOST, MAGAZYN)
      select :newfak, ODDZIAL, :STANOWISKO , :REJFAK, :TYPFAK, NULL, NULL, ODBIORCAID, ODBIORCA, DULICA, DMIASTO, DKODP,
                     KLIENT, UZYKLI, :datawyst, case when :proforma =1 then :datawyst else DATASPRZ end, SPOSZAP, TERMIN, TERMZAP, RABAT,
                     UWAGI, WALUTA, TABKURS, KURS,
                     WALUTOWA, PKURS, GRUPADOK,
                     :OPERATOR , sprzedawca, :nieobrot, BN, KOSZT, KOSZTZAK, PKOSZT, CKONTRAKTY, SPOSDOST, MAGAZYN
          from NAGFAK where REF=:paragon;
  end else begin
    newfak = faktura;
  end
  if(:nieobrot > 0) then update NAGFAK set NIEOBROT = :nieobrot where REF=:newfak;

  -- Przenoszenie pozycji alternatywnych
  for select pf.ref, pf.havefake, pf.fake, pf.alttopoz
    from pozfak pf
    where pf.dokument = :paragon
    order by pf.numer, pf.alttopoz nulls first
    into :pozycja, :havefake, :fake, :AltToPoz
  do begin
     AltToPozN = null;
     if (:AltToPoz is not null) then
       select pf.ref
         from pozfak pf
         where pf.refparpro = :AltToPoz
         into :AltToPozN;
     insert into POZFAK(DOKUMENT, NUMER, ORD, KTM, WERSJA, CENACEN, WALCEN, RABATTAB, RABAT,
             ILOSC, ILOSCO, ILOSCM, JEDNO, JEDN, OPK,
             CENANET, CENABRU, WARTNET, WARTBRU,
             PILOSC,PILOSCO, PILOSCM, PCENACEN, PRABATTAB, PRABAT, PCENANET, PCENABRU, PNAZWAT,
             PWARTNET, PWARTBRU, GR_VAT, CENANETZL,CENABRUZL,
             WARTNETZL, WARTBRUZL, PCENANETZL, PCENABRUZL, PWARTNETZL, PWARTBRUZL, DOSTAWA,
             kosztzak, kosztkat, refparpro, prec, magazyn,
             alttopoz, fake, havefake)
     select :newfak, NUMER, 1, KTM, WERSJA, CENACEN, WALCEN, RABATTAB, RABAT,
             ILOSC, ILOSCO, ILOSCM, JEDNO, JEDN,OPK,
             CENANET, CENABRU, WARTNET, WARTBRU,
             PILOSC, PILOSCO, PILOSCM, PCENACEN, PRABATTAB, PRABAT, PCENANET, PCENABRU, PNAZWAT,
             PWARTNET, PWARTBRU, GR_VAT, CENANETZL,CENABRUZL,
             WARTNETZL, WARTBRUZL, PCENANETZL, PCENABRUZL, PWARTNETZL, PWARTBRUZL, DOSTAWA,
             case when :parobr = 1 then 0 else kosztzak end, case when :parobr = 1 then 0 else kosztkat end,
             REF, PREC, magazyn, :AltToPozN, :fake, :havefake
      from POZFAK where REF = :pozycja;
   end
      --order by POZFAK.NUMER;
   select SYMBOL from NAGFAK where REF=:newfak into :symbol;
   update NAGFAK set REFFAK = :newfak, SYMBOLFAK = :symbol  where REF=:paragon;
   /* zaakceptowanie faktury - poniewaz na podstawie paragonu, to nie powstanie zaplata ani rozrchunek*/
   if(:proforma <> 1 and :parobr<>'1') then begin
     /*zaznaczenie paragonu jako nieobrotowego */
     update NAGFAK set NIEOBROT = 2  where REF=:paragon;/* paragon jako nieobrotowy, rozliczeniowy*/
   end
   REFFAK = :newfak;
end^
SET TERM ; ^
