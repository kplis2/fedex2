--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_KBI_TEST_ZAMOWIEN(
      ZAMREF INTEGER_ID)
  returns (
      JSON STRING8191)
   as
declare variable eol string3;
declare variable nagId integer_id;
declare variable symbol string;
declare variable numer integer_id;
declare variable datazam timestamp_id;
declare variable magazyn string3;
declare variable magazyn2 string3;
declare variable klient integer_id;
declare variable klientId integer_id;
declare variable skrot string;
declare variable firma smallint_id;
declare variable nazwa string;
declare variable nip string40;
declare variable imie string;
declare variable nazwisko string;
declare variable grupa string;
declare variable grupaid string;
declare variable typ string;
declare variable kraj string;
declare variable krajid string;
declare variable odbiorca integer;
declare variable odbiorcaId string;
declare variable glowny string;
declare variable sposzap integer_id;
declare variable zaplata string;
declare variable zaplataid string;
declare variable sposdost integer_id;
declare variable dostawa string;
declare variable dostawaid string;
declare variable waluta string10;
declare variable uwagiwewn string;
declare variable uwagi string;
declare variable uwagisped string;
declare variable datareal timestamp_id;


declare variable pozid string;
declare variable poznumer string;
declare variable towarid string;
declare variable jedn string3;

declare variable jednostka string;
declare variable jednostkaid string;
declare variable vat string10;
declare variable ilosc ilosci_mag;
declare variable cenanetto numeric_14_2;
declare variable cenabrutto numeric_14_2;
declare variable wartoscnetto numeric_14_2;
declare variable wartoscbrutto numeric_14_2;
declare variable cenanettostr string;
declare variable cenabruttostr string;
declare variable wartoscnettostr string;
declare variable wartoscbruttostr string;
declare variable cnt integer_id;
declare variable ilepoz integer_id;


declare variable ulica string;
declare variable nrDomu string;
declare variable nrLokalu string;
declare variable kodPocztowy string;
declare variable miasto string;
declare variable telefon string;
declare variable fax string;
declare variable email string;
declare variable www string;
declare variable onazwa string;
declare variable nazwafirmy string;
declare variable oimie string;
declare variable onazwizko string;
declare variable okrajid string;
declare variable okraj string;
declare variable oulica string;
declare variable onrdomu string;
declare variable onrlokalu string;
declare variable okodp string;
declare variable omiasto string;
declare variable otelefon string;
declare variable oemail string;
declare variable owww string;


declare variable kuwagi string;
declare variable kaktywny smallint_id;
declare variable limitkr integer_id;
declare variable bezpiecznysppl smallint_id;
declare variable termin integer_id;
declare variable ksposplat integer_id;
declare variable kzaplata string;
declare variable kzaplataid string;
declare variable ksposdost integer_id;
declare variable kdostawa string;
declare variable kdostawaid string;
declare variable kwaluta string;



begin
eol = '
';

json = '[{'||eol;
  -- nagówek

  select nz.ref, nz.id, nz.numer, nz.datawe, nz.magazyn, nz.mag2, nz.klient, nz.odbiorcaid,
      nz.sposzap,
      nz.sposdost,
      nz.waluta, nz.uwagiwewn, nz.uwagi, nz.uwagisped, nz.termdost
    from nagzam nz
    where nz.ref = :zamref
  into :nagid,  :symbol, :numer, :datazam, :magazyn, :magazyn2, :klient, :odbiorca,
     :sposzap,
     :sposdost,
     :waluta, :uwagiwewn, :uwagi, :uwagisped, :datareal;


  -- sposob zaplaty
  select first 1 sp.wartosc, sp.wartoscid
    from int_slownik sp
    where sp.otable = 'PLATNOSCI'
      and sp.oref = :sposzap
  into :zaplata, :zaplataid;

  -- sposb dostawy
  select first 1 sp.wartosc, sp.wartoscid
    from int_slownik sp
    where sp.otable = 'SPOSDOST'
      and sp.oref = :sposdost
  into :dostawa, :dostawaid;

  magazyn2 = coalesce(magazyn2,'');
  magazyn2 = trim(magazyn2);

  json = json||'  "nagId":"'||:nagid||'",'||eol;
  json = json||'  "symbol":"'||:symbol||'",'||eol;
  json = json||'  "numer":'||:numer||','||eol;
  json = json||'  "typ":"SPR",'||eol;
  json = json||'  "dataZam":"'||cast(:datazam as date_id)||'",'||eol;
  json = json||'  "magazyn":"'||:magazyn||'",'||eol;
  json = json||'  "magazyn2":"'||coalesce(:magazyn2,'')||'",'||eol;
  json = json||'  "wydania":1,'||eol;
  json = json||'  "zewnetrzny":1,'||eol;
  json = json||'  "bn":"N",'||eol;
  json = json||'  "klientId":"",'||eol;
  json = json||'  "zaplata":"'||coalesce(:zaplata,'')||'",'||eol;
  json = json||'  "zaplataId":"'||coalesce(:zaplataid,'')||'",'||eol;
  json = json||'  "dostawa":"'||coalesce(:dostawa,'')||'",'||eol;
  json = json||'  "dostawaId":"'||coalesce(:dostawaId,'')||'",'||eol;
  json = json||'  "waluta":"'||coalesce(:waluta,'')||'",'||eol;
  json = json||'  "uwagiWewn":"'||coalesce(:uwagiwewn,'')||'",'||eol;
  json = json||'  "uwagiZewn":"'||coalesce(:uwagi,'')||'",'||eol;
  json = json||'  "uwagiSped":"'||coalesce(:uwagisped,'')||'",'||eol;
  json = json||'  "dataWysylki":"'||coalesce(cast(:datareal as date_id),'')||'",'||eol;

  json = json||'  "partia":"",'||eol;
  json = json||'  "partiaId":"",'||eol;
  json = json||'  "kurs":0,'||eol;
  json = json||'  "doZaplaty":0,'||eol;
  json = json||'  "doZaplatyZl":0,'||eol;
  json = json||'  "status":1,'||eol;
  json = json||'  "zwolnijPakowanie":1,'||eol;

  /**************************************************************************************/
                              /* KLIENT */
  /**************************************************************************************/


  select k.int_id, k.fskrot, k.firma, k.nazwa, k.nip, k.imie, k.nazwisko,
      k.grupa,k.grupanazwa, k.typ, k.kraj, k.krajid, k.ulica, k.nrdomu, k.nrlokalu,
      k.kodp, k.miasto, k.telefon, k.fax, k.email, k.www, k.uwagi, k.aktywny,
      k.limitkr, k.sposplatbezp, k.termin,
      k.sposplat, k.sposdost,
      k.waluta
    from klienci k
    where k.ref = :klient
  into :klientid, :skrot, :firma, :nazwa, :nip, :imie, :nazwisko,
      :grupaid, :grupa, :typ,  :kraj, :krajid, :ulica, :nrdomu, :nrlokalu,
      :kodpocztowy, :miasto, :telefon, :fax, :email, :www, :kuwagi, :kaktywny,
      :limitkr, :bezpiecznysppl, :termin,
      :ksposplat, :ksposdost,
      :kwaluta;
  -- sposob zaplaty
  if (ksposplat is not null) then
    select first 1 sp.wartosc, sp.wartoscid
      from int_slownik sp
      where sp.otable = 'PLATNOSCI'
        and sp.oref = :ksposplat
    into :kzaplata, :kzaplataid;

  -- sposb dostawy
  if (ksposdost is not null) then
    select first 1 sp.wartosc, sp.wartoscid
      from int_slownik sp
      where sp.otable = 'SPOSDOST'
        and sp.oref = :ksposdost
    into :kdostawa, :kdostawaid;

  skrot = replace(skrot,'"','\"');
  nazwa = replace(nazwa,'"','\"');

  json = json||'  "klient":{'||eol;
  json = json||'    "klientId":"'||:klientid||'",'||eol;
  json = json||'    "skrot":"'||:skrot||'",'||eol;
  json = json||'    "firma":'||:firma||','||eol;
  json = json||'    "nazwa":"'||:nazwa||'",'||eol;
  json = json||'    "nip":"'||:nip||'",'||eol;
  json = json||'    "imie":"'||coalesce(:imie, '')||'",'||eol;
  json = json||'    "nazwisko":"'||coalesce(:nazwisko, '')||'",'||eol;
  json = json||'    "grupa":"'||coalesce(:grupa, '')||'",'||eol;
  json = json||'    "grupaId":"'||coalesce(:grupaid, '')||'",'||eol;
  json = json||'    "typ": "",'||eol;
  json = json||'    "typId": "",'||eol;
  json = json||'    "regon": "",'||eol;
  json = json||'    "kraj":"'||coalesce(:kraj, '')||'",'||eol;
  json = json||'    "krajId":"'||coalesce(:krajId, '')||'",'||eol;
  json = json||'    "ulica":"'||coalesce(:ulica, '')||'",'||eol;
  json = json||'    "nrDomu":"'||coalesce(:nrDomu, '')||'",'||eol;
  json = json||'    "nrLokalu":"'||coalesce(:nrLokalu, '')||'",'||eol;
  json = json||'    "kodPocztowy":"'||coalesce(:kodPocztowy, '')||'",'||eol;
  json = json||'    "miasto":"'||coalesce(:miasto, '')||'",'||eol;
  json = json||'    "telefon":"'||coalesce(:telefon, '')||'",'||eol;
  json = json||'    "fax":"'||coalesce(:fax, '')||'",'||eol;
  json = json||'    "email":"'||coalesce(:email, '')||'",'||eol;
  json = json||'    "www":"'||coalesce(:www, '')||'",'||eol;
  json = json||'    "uwagi":"'||coalesce(:kuwagi, '')||'",'||eol;
  json = json||'    "aktywny":'||coalesce(:kaktywny, 1)||','||eol;
  json = json||'    "limitKredytowy":'||coalesce(:limitkr, 0)||','||eol;
  json = json||'    "bezpiecznySposobPlatnosci":'||coalesce(:bezpiecznysppl, 0)||','||eol;
  json = json||'    "terminDni":'||coalesce(:termin, 0)||','||eol;
  json = json||'    "zaplata":"'||coalesce(:kzaplata,'')||'",'||eol;
  json = json||'    "zaplataId":"'||coalesce(:kzaplataId, '')||'",'||eol;
  json = json||'    "dostawa":"'||coalesce(:kdostawa,'')||'",'||eol;
  json = json||'    "dostawaId":"'||coalesce(:kdostawaId, '')||'",'||eol;
  json = json||'    "waluta":"'||coalesce(:kwaluta, '')||'",'||eol;

  json = json||'    "rabat": 0,'||eol;
  json = json||'    "cennik": "",'||eol;
  json = json||'    "cennikId": "",'||eol;
  json = json||'    "sprzedawca": "",'||eol;
  json = json||'    "sprzedawcaId": "",'||eol;
  json = json||'    "typDokFak": "",'||eol;
  json = json||'    "typDokMag": "",'||eol;

  json = json||'    "bank": "",'||eol;
  json = json||'    "rachunek": "",'||eol;
  json = json||'    "dostawcaId": "",'||eol;
  json = json||'    "hash": "",'||eol;
  json = json||'    "del": 0,'||eol;
  json = json||'    "rec": ""'||eol;
  json = json||'  },'||eol;

  /**************************************************************************************/
                              /* Odbiorca */
  /**************************************************************************************/

  select   o.int_id, o.gl, o.nazwa, o.nazwafirmy, o.imie, o.nazwisko,
      o.krajid, o.dulica, o.dnrdomu, o.dnrlokalu, o.dkodp, o.dmiasto,
      o.dtelefon, o.email, o.www
    from odbiorcy o
    where o.ref = :odbiorca
  into :odbiorcaId, :glowny, :onazwa, :nazwafirmy, oimie, onazwizko,
      :okrajid, :oulica, :onrdomu, :onrlokalu, :okodp, :omiasto,
      :otelefon, :oemail, :owww;

  if (okrajid is not null) then
    select name from countries where symbol = :okrajid into :okraj;

  onazwa = replace(onazwa,'"','\"');
  nazwafirmy = replace(nazwafirmy,'"','\"');

  json = json||'  "odbiorca":{'||eol;
  json = json||'      "klientId":"'||coalesce(:klientid, '')||'",'||eol;
  json = json||'      "odbiorcaId": "'||coalesce(:odbiorcaid, '')||'",'||eol;
  json = json||'      "glowny": "'||coalesce(:glowny, '')||'",'||eol;
  json = json||'      "nazwa": "'||coalesce(:onazwa, '')||'",'||eol;
  json = json||'      "nazwaFirmy": "'||coalesce(:nazwafirmy, '')||'",'||eol;
  json = json||'      "imie": "'||coalesce(:oimie, '')||'",'||eol;
  json = json||'      "nazwisko": "'||coalesce(:onazwizko, '')||'",'||eol;
  json = json||'      "kraj": "'||coalesce(:okraj, '')||'",'||eol;
  json = json||'      "krajId": "'||coalesce(:okrajid, '')||'",'||eol;
  json = json||'      "ulica": "'||coalesce(:oulica, '')||'",'||eol;
  json = json||'      "nrDomu": "'||coalesce(:onrdomu, '')||'",'||eol;
  json = json||'      "nrLokalu": "'||coalesce(:onrlokalu, '')||'",'||eol;
  json = json||'      "kodPocztowy": "'||coalesce(:okodp, '')||'",'||eol;
  json = json||'      "miasto": "'||coalesce(:omiasto, '')||'",'||eol;
  json = json||'      "telefon": "'||coalesce(:otelefon, '')||'",'||eol;
  json = json||'      "email": "'||coalesce(:oemail, '')||'",'||eol;
  json = json||'      "www": "'||coalesce(:owww, '')||'",'||eol;
  json = json||'      "hash": "",'||eol;
  json = json||'      "del": 0,'||eol;
  json = json||'      "rec": ""'||eol;
  json = json||'  },'||eol;

  /**************************************************************************************/
                              /* Pozycje */
  /**************************************************************************************/
  json = json||'   "zamPozycje":[ {'||eol;

  select count(ref) from pozzam where zamowienie = :nagid into :ilepoz;
  cnt = 1;
  for
    select pz.ref, pz.numer, t.int_id, j.jedn,
        pz.gr_vat, pz.ilosc, pz.cenanet, pz.cenabru, pz.wartnet, pz.wartbru
      from pozzam pz
        join towary t on (pz.ktm = t.ktm)
        join towjedn j on (pz.jedn = j.ref)
      where pz.zamowienie = :nagid
    into :pozid, poznumer, :towarid, :jedn,
        :vat, :ilosc, :cenanetto, :cenabrutto, :wartoscnetto, :wartoscbrutto
  do begin
 /*   cenanettostr = cast(cenanetto as string);
    cenanettostr = replace(cenanettostr, '.',',');

    cenabruttostr = cast(wartoscnetto as string);
    cenabruttostr = replace(cenabruttostr, '.',',');

    wartoscnettostr = cast(cenanetto as string);
    wartoscnettostr = replace(wartoscnettostr, '.',',');

    wartoscbruttostr = cast(wartoscbrutto as string);
    wartoscbruttostr = replace(wartoscbruttostr, '.',',');   */
    -- jednoska
    select first 1 sp.wartosc, sp.wartoscid
      from int_slownik sp
      where sp.otable = 'MIARA'
        and sp.okey = :jedn
    into :jednostka, :jednostkaid;

    json = json||'    "nagId":"'||:nagid||'",'||eol;
    json = json||'    "pozId":"'||:pozid||'",'||eol;
    json = json||'    "numer":'||:poznumer||','||eol;
    json = json||'    "towarId":"'||:towarId||'",'||eol;

    json = json||'    "jednostka":"'||:jednostka||'",'||eol;
    json = json||'    "jednostkaId":"'||:jednostkaId||'",'||eol;
    json = json||'    "vat":"'||:vat||'",'||eol;
    json = json||'    "magazyn":"'||coalesce(:magazyn,'')||'",'||eol;
    json = json||'    "magazyn2":"'||coalesce(:magazyn2,'')||'",'||eol;
    json = json||'    "ilosc":"'||:ilosc||'",'||eol;
    json = json||'    "cenaNetto":"'||:cenaNetto||'",'||eol;
    json = json||'    "cenaBrutto":"'||:cenaBrutto||'",'||eol;
    json = json||'    "wartoscNetto":"'||:wartoscNetto||'",'||eol;
    json = json||'    "wartoscBrutto":"'||:wartoscBrutto||'",'||eol;

    json = json||'    "partia":"",'||eol;
    json = json||'    "partiaId":"",'||eol;

    json = json||'    "hash": "",'||eol;
    json = json||'    "del": 0,'||eol;
    json = json||'    "rec": ""'||eol;
    if (cnt = ilepoz) then
      json = json||'    }'||eol;
    else
      json = json||'    },{'||eol;
    jednostka = null; jednostkaid = null;
    cnt = cnt + 1 ;
  end

  json = json||'  ],'||eol;
  json = json||'   "hash": "",'||eol;
  json = json||'    "del": 0,'||eol;
  json = json||'    "rec": "" '||eol;
  json = json||'  }'||eol;
  json = json||']'||eol;

  suspend;
end^
SET TERM ; ^
