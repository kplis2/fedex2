--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_GET_NAVIGATOR_ORDER(
      NAMESPACE varchar(40) CHARACTER SET UTF8                           ,
      ITEMREF varchar(255) CHARACTER SET UTF8                            = null,
      ONLYIDENT varchar(255) CHARACTER SET UTF8                            = null)
  returns (
      REF varchar(255) CHARACTER SET UTF8                           ,
      PARENT varchar(255) CHARACTER SET UTF8                           )
   as
declare variable CURRLAYER integer;
declare variable SECTION varchar(255);
declare variable IDENT varchar(255);
declare variable VAL varchar(1024);
declare variable OLDSECTION varchar(255);
declare variable LAYER integer;
declare variable COMPANY COMPANIES_ID;
declare variable PATTERN SMALLINT_ID;
declare variable RIBBONTAB varchar(40);
declare variable RIBBONGROUP varchar(40);
begin
  --Ściągam currentcompany, żeby wiedzieć do jakiego company sie loguje
  execute procedure get_global_param('CURRENTCOMPANY')
    returning_values :company;
  --Szukam czy company nie jest wzorcowym, dla danej grupy company dzieci  PR60327
  select coalesce(c.pattern,0)
    from companies c
    where c.ref = :company
    into :pattern;

  if(:namespace is null or :namespace='') then namespace = 'SENTE';
  select layer from s_namespaces where namespace=:namespace into :currlayer;
  oldsection = '';
  if(:itemref is null) then itemref = '';
  if(:onlyident is null) then onlyident = '';

  for select n.layer, s.section, s.ident, s.val
  from s_appini s
  left join s_namespaces n on (n.namespace=s.namespace)
  where s.section starting with 'Navigator:'||:itemref
  and n.layer<=:currlayer
  and (:onlyident = '' or :onlyident like '%'||s.ident||'%')
  order by s.section, n.layer desc
  into :layer, :section, :ident, :val
  do begin
    -- jesli jest nowy item to zwroc rekord wynikowy
    if(:section<>:oldsection) then begin
      if((coalesce(ribbontab,'') <> '' and coalesce(ribbongroup,'') <> '')) then begin
        parent = ribbontab;
        ref = 'child_'||ribbongroup;

        if(:oldsection<>'') then suspend;
      end

      oldsection = :section;
      ref = NULL; parent = NULL; ribbontab = NULL; ribbongroup = NULL;
    end
    -- zbieraj kolejne wartosci pol
    if(:ident = 'REF' and :ref is NULL) then REF = :val;
    if(:ident = 'PARENT' and :parent is NULL) then PARENT = :val;
    if(:ident = 'RIBBONTAB' and :ribbontab is NULL) then RIBBONTAB = :val;
    if(:ident = 'RIBBONGROUP' and :ribbongroup is NULL) then RIBBONGROUP = :val;
  end
end^
SET TERM ; ^
