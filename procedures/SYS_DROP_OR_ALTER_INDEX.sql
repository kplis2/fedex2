--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_DROP_OR_ALTER_INDEX(
      TABLENAME varchar(31) CHARACTER SET UTF8                           ,
      OLD_FIELDS varchar(1024) CHARACTER SET UTF8                            = '',
      OLD_EXPRESSION varchar(1024) CHARACTER SET UTF8                            = '',
      OLD_UNIQUE smallint = 0,
      OLD_DESCENDING smallint = 0,
      NEW_FIELDS varchar(1024) CHARACTER SET UTF8                            = '',
      NEW_EXPRESSION varchar(1024) CHARACTER SET UTF8                            = '',
      NEW_UNIQUE smallint = 0,
      NEW_DESCENDING smallint = 0)
   as
  declare variable index_name varchar(80);               /* nazwa indeksu */
  declare variable first_index_name varchar(80) = '';    /* nazwa pierwszego indeksu pasującego do opisu*/
  declare variable index_fields varchar(1024);           /* kolumny tworzace indeks */
  declare variable new_index_fields varchar(1024);       /* kolumny tworzace nowy indeks */
  declare variable index_field varchar(31);              /* kolumna tworzaca indeks */
  declare variable ddl varchar(1024);                    /* polecenie tworzące nowy indeks */
  declare variable index_count integer;                  /* licznik usunietych indeksów */
begin
  /*
  1. procedura na podstawie nazwy tabeli i definicji starego indeksu wyszukuje
  wszytskie indeksy odpowiadające definicji
  2. dla każdego znalezionego dropuje ten indeks
  3. jezeli jest przekazana pelna nowa definicja(nazwy kolumn lub wyrazenie) to
   zaklada indeks zgodnie z nowa definicja i z pierwsza zwolniona nazwa
  */

  if (old_unique is null) then
    old_unique = 0;
  if (old_descending is null) then
    old_descending = 0;
  if (old_expression is null) then
    old_expression = '';
  if (new_expression is null) then
    new_expression = '';
  if (old_fields is null) then
    old_fields = '';
  if (new_fields is null) then
    new_fields = '';
  tablename = upper(:tablename);
  if (old_fields = '' and old_expression = '') then
    exception universal 'Brak wymaganych parametrów. Prosze podac liste kolumn lub wyrażenie.';
  if(:old_fields <> '') then
  begin
    old_fields = replace(:old_fields,'(','');
    old_fields = replace(:old_fields, ')', '');
    old_fields = replace(:old_fields, ' ', '');
    old_fields = upper(:old_fields);
  end
  if(:new_fields <> '') then
  begin
    new_fields = replace(:new_fields,'(','');
    new_fields = replace(:new_fields, ')', '');
    new_fields = replace(:new_fields, ' ', '');
    new_fields = upper(:new_fields);
  end
  if (:old_expression <> '' and (substring(:old_expression from 1 for 1) <> '('
    or substring(:old_expression from coalesce(char_length(:old_expression),0) for coalesce(char_length(:old_expression),0)) <> ')')) then -- [DG] XXX ZG119346
    old_expression = '(' || :old_expression || ')';
  if (:new_expression <> '' and (substring(:new_expression from 1 for 1) <> '('
    or substring(:new_expression from coalesce(char_length(:new_expression),0) for coalesce(char_length(:new_expression),0)) <> ')')) then -- [DG] XXX ZG119346
    new_expression = '(' || :new_expression || ')';
  index_count = 0;
  /* weryfikacja zgodnosci z usuwanym/zmienianym indeksem */
  if (exists(select first 1 1 from rdb$indices i where i.rdb$relation_name = :tablename
    and coalesce(i.rdb$expression_source,'') = :old_expression
    and coalesce(i.rdb$unique_flag,0) = :old_unique
    and coalesce(i.rdb$index_type,0) = :old_descending)) then
  begin
    /* Identyfikacja nazwy indeksu istniejącego w bazie */
    for
      select i.rdb$index_name from rdb$indices i
        where i.rdb$relation_name = :tablename
          and coalesce(i.rdb$expression_source,'') = :old_expression
          and coalesce(i.rdb$unique_flag,0) = :old_unique
          and coalesce(i.rdb$index_type,0) = :old_descending
      into :index_name
    do begin
      -- jeżeli istniejący indeks zostal nalozony na kolumny
      index_fields = '';
      for
        select s.rdb$field_name
          from rdb$index_segments s
          where s.rdb$index_name = :index_name
          order by s.rdb$field_position
        into :index_field
      do begin
        index_field = substring(:index_field from 1 for position(' ' in  :index_field) - 1);
        index_fields = :index_fields || :index_field || ',';
      end
      index_fields = substring(:index_fields from  1 for coalesce(char_length(:index_fields) ,0)-1); -- [DG] XXX ZG119346
      if(:index_fields = :old_fields) then -- porównanie istniejącej definicji z przekazanym argumentem
      begin
        index_name = substring(:index_name from 1 for position(' ' in  :index_name) - 1);
        if (first_index_name = '') then
          first_index_name = index_name;
        execute statement 'drop index ' || :index_name || ';'; -- usuwanie indeksu
        index_count = :index_count + 1;
      end
    end
  end
  else
    exception universal 'W tabeli ' || :tablename || ' nie ma indeksu o podanym opisie. Modyfikacja niemożliwa.';
  if (index_count = 0) then -- jesli nie usunieto żadnego indeksu
    exception universal 'W tabeli ' || :tablename || ' nie ma indeksu o podanym opisie. Modyfikacja niemożliwa.';

  -- jesli podano nowa definicje
  if (new_fields <> '' or new_expression <> '') then
  begin
    if (new_unique is null) then
      new_unique = 0;
    if (new_descending is null) then
      new_descending = 0;
     /* sprawdzenie czy nie istnieje już indeks o tych parametrach */
    if (exists(select first 1 1 from rdb$indices i where i.rdb$relation_name = :tablename
      and coalesce(i.rdb$expression_source,'') = :new_expression
      and coalesce(i.rdb$unique_flag,0) = :new_unique
      and coalesce(i.rdb$index_type,0) = :new_descending)) then
    begin
      /* Identyfikacja nazwy indeksu istniejącego w bazie */
      for
        select i.rdb$index_name from rdb$indices i
          where i.rdb$relation_name = :tablename
            and coalesce(i.rdb$expression_source,'') = :new_expression
            and coalesce(i.rdb$unique_flag,0) = :new_unique
            and coalesce(i.rdb$index_type,0) = :new_descending
        into :index_name
      do begin
        -- jeżeli istniejący indeks zostal nalozony na kolumny
        new_index_fields = '';
        for
          select s.rdb$field_name
            from rdb$index_segments s
            where s.rdb$index_name = :index_name
            order by s.rdb$field_position
          into :index_field
        do begin
          index_field = substring(:index_field from 1 for position(' ' in  :index_field) - 1);
          new_index_fields = :new_index_fields || :index_field || ',';
        end
        new_index_fields = substring(:new_index_fields from  1 for coalesce(char_length(:new_index_fields) , 0)-1); -- [DG] XXX ZG119346
        if(:new_index_fields = :new_fields) then -- porównanie istniejącej definicji z przekazanymi argumentami
          exception universal 'W tabeli ' || :tablename || ' istnieje już indeks o podanym opisie. Modyfikacja niemożliwa.';
      end
    end
  
    -- modyfikacja - zakladanie nowego indeksu
    ddl = 'CREATE ';
    if(:new_unique = 1) then
      ddl = :ddl || 'UNIQUE ';
    if(:new_descending = 1) then
      ddl = :ddl || 'DESCENDING ';
      ddl = :ddl || 'INDEX ' || :first_index_name || ' ON ' || :tablename;
    if(:new_expression <> '') then --jesli podano nową formule
      ddl = :ddl || ' COMPUTED BY ' || :new_expression || ';';
    else
      if (:new_fields <> '') then --jesli podano nowe pola
        ddl = :ddl || ' (' || :new_fields || ');';
    execute statement :ddl;   -- zalozenie nowego indeksu
  end
end^
SET TERM ; ^
