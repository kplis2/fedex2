--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_DOST_ZEST_KTM(
      DOSTAWCA integer,
      ODDATY timestamp,
      DODATY timestamp,
      ODKTM varchar(40) CHARACTER SET UTF8                           ,
      DOKTM varchar(40) CHARACTER SET UTF8                           )
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      KTMNAZWA varchar(255) CHARACTER SET UTF8                           ,
      WERSJANAZWA varchar(255) CHARACTER SET UTF8                           ,
      ILZAM numeric(14,4),
      ILREAL numeric(14,4),
      WARTBRU numeric(14,2),
      WARTRBRU numeric(14,2),
      CENANET numeric(14,2),
      KOSZTZAK numeric(14,2))
   as
declare variable wartrnet numeric(14,2);
declare variable sumkosztzak numeric(14,2);
begin
  for select
    POZZAM.KTM, max(TOWARY.NAZWA), max(WERSJE.NAZWA),
    sum(POZZAM.ILOSC), sum(POZZAM.ILREAL),
    sum(POZZAM.WARTBRU), sum(POZZAM.WARTRBRU),
    sum(POZZAM.WARTRNET),
    sum(POZZAM.cenanet * POZZAM.ILREAL)
  from NAGZAM
    left join POZZAM on (NAGZAM.REF=POZZAM.ZAMOWIENIE)
    left join TOWARY on ( POZZAM.KTM = TOWARY.KTM)
    left join WERSJE on ( POZZAM.WERSJAREF = WERSJE.REF)
  where NAGZAM.TYP < 2
     and NAGZAM.stan = 'D'
     and (NAGZAM.DOSTAWCA = :dostawca or (coalesce(:dostawca, 0) = 0))
     and ((NAGZAM.datawe >= :ODDATY) or (:ODDATY  is null))    --DD BS36558 datareal->datawe
     and ((NAGZAM.datawe <= :DODATY) or (:DODATY is null))     --DD BS36558 datareal->datawe
     and ((POZZAM.ktm >= :ODKTM) or (:ODKTM = ''))
     and ((POZZAM.ktm <= :DOKTM) or (:DOKTM = ''))
  group by POZZAM.KTM, POZZAM.WERSJA
  into :ktm, :ktmnazwa, :wersjanazwa, :ilzam, :ilreal, :wartbru, :wartrbru,:wartrnet, :sumkosztzak
  do begin
    if(:ilreal is null) then ilreal = 0;
    if(:ilreal > 0) then begin
       cenanet = :wartrnet / :ilreal;
       kosztzak = :sumkosztzak / :ilreal;
    end else begin
       cenanet = null;
       kosztzak = null;
    end
    suspend;
  end
end^
SET TERM ; ^
