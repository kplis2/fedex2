--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE M_SETTLE_DECREEMATCHING_CHECK(
      MATCHINGGROUPID DECREEMATCHINGS_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG MEMO,
      BTNS STRING255)
   as
declare variable cnt integer_id;
declare variable sumcredit ct_amount;
declare variable sumdebit dt_amount;
begin
  status = 1;
  -- weryfikacja czy zaznaczone rekordy nie pochodza z rożnych grup rozliczeniowych
  select count(distinct m.decreematchinggroup)
    from decreematchings m
    where m.matchinggroupid = :matchinggroupid and m.decreematchinggroup is not null
    into cnt;
  if (cnt is null) then cnt = 0;
  if (cnt > 1) then
  begin
    status = 0;
    msg = 'Zaznaczono rekordy z dwóch różnych grup rozliczeniowych.';
    exit;
  end
  cnt = 0;
  -- weryfikacja ile zaznaczono rekordów
  select count(m.ref)
    from decreematchings m
    where m.matchinggroupid = :matchinggroupid
    into cnt;
  if (cnt is null) then cnt = 0;
  if (cnt = 1) then
  begin
    status = 0;
    msg = 'Zaznaczono tylko jeden rekord.';
    exit;
  end
  -- weryfikacja czy nie ma zaznaczonych rekordow z kont z różnymi slownikami
  cnt = 0;
  select count(distinct a.matchingslodef)
    from decreematchings m
      left join decrees d on (d.ref = m.decree)
      left join bkdocs b on (b.ref = d.bkdoc)
      left join bkaccounts a on (a.ref = m.bkaccount)
    where m.matchinggroupid = :matchinggroupid
    into cnt;
  if (:cnt > 1) then
  begin
    status = 0;
    msg = 'Nie można rozliczyć kont z różnymi słownikami rozliczeniowymi.';
    exit;
  end
  -- TODO - pytanie czy moge dwa rozne slopoy
  cnt = 0;
  select count(distinct m.slopoz)
    from decreematchings m
    where m.matchinggroupid = :matchinggroupid and m.slopoz is not null
    into cnt;
  if (:cnt > 1) then
  begin
    status = 0;
    msg = 'Nie można rozliczyć dwóch różnych wartości słownika.';
    exit;
  end
  -- weryfikacja czy jakis rekord nie jest juz w innej grupie
  if (exists(select first 1 1 from decreematchings m where m.decreematchinggroup is not null and m.matchinggroupid = :matchinggroupid)) then
  begin
    status = 3;
    msg = 'Zaznaczono rekord z grupy rozliczeniowej. Czy dołączyć ?';
    exit;
  end
  -- weryfikacja czy sie grupa bilansuje
  select sum(m.credit), sum(m.debit)
    from decreematchings m
    where m.matchinggroupid = :matchinggroupid
    into :sumcredit, :sumdebit;
  if (:sumcredit is null) then sumcredit = 0;
  if (:sumdebit is null) then sumdebit = 0;
  if (:sumcredit <> :sumdebit) then
  begin
    status = 2;
    msg = 'Zaznaczone rekordy nie bilansują się. Rozliczyć ?';
    btns = '&Wydziel;W&szystkie;&Anuluj';
  end
end^
SET TERM ; ^
