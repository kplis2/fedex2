--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWIAD_DOKOGO(
      EMAILW integer)
   as
declare variable SPODM varchar(255);
declare variable EM varchar(255);
declare variable SEM varchar(2048);
declare variable SCC varchar(2048);
declare variable SBCC varchar(2048);
declare variable RODZ smallint;
declare variable PODM integer;
begin
  if(:emailw is not null) then begin
    sem = '';
    scc = '';
    sbcc = '';
    spodm = ';';
    for select email,rodzaj,cpodmiot from EMAILADR where (emailadr.emailwiad=:emailw) into :em,:rodz,:podm do begin
      if(:rodz=0) then begin
        if(:sem<>'') then sem = :sem || '; ';
            if(char_length(:sem || :em) < 2048) then sem = :sem || :em;
      end else if(:rodz=1) then begin
        if(:scc<>'') then scc = :scc || '; ';
            if(char_length(:scc || :em) < 2048) then scc = :scc || :em;
      end else if(:rodz=2) then begin
        if(:sbcc<>'') then sbcc = :sbcc || '; ';
        if(char_length(:sbcc || :em) < 2048) then sbcc = :sbcc || :em;
      end
      spodm = :spodm || cast(:podm as varchar(10)) || ';';
    end
    update EMAILWIAD set DOKOGO=:sem, CC=:scc, BCC=:sbcc, PODMIOTY=:spodm where emailwiad.ref=:emailw;
  end
end^
SET TERM ; ^
