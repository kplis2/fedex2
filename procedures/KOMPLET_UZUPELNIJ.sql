--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KOMPLET_UZUPELNIJ(
      KOMPLET integer,
      FROMKPL integer,
      TYPUZUP integer)
   as
declare variable cnt integer;
declare variable werref integer;
declare variable il numeric(14,4);
declare variable ill numeric(14,4);
begin
  for select KPLPOZ.WERSJAREF, KPLPOZ.ILOSC from KPLPOZ where NAGKPL = :fromkpl
  order by numer
  into :werref, :il
  do begin
    cnt = null;
    select count(*) from KPLPOZ where NAGKPL = :komplet and WERSJAREF = :werref into :cnt;
    if(:cnt is null) then cnt = 0;
    if(:cnt = 0) then
      insert into KPLPOZ(NAGKPL,WERSJAREF,ILOSC) values(:KOMPLET,:werref, :il);
    else begin
      ill = null;
      select sum(ILOSC) from KPLPOZ where NAGKPL = :komplet and WERSJAREF = :werref into :ill;
      if(:ill is null) then ill = 0;
      if(:ill <> :il) then begin
         delete from KPLPOZ where NAGKPL = :komplet and WERSJAREF = :werref;
         insert into KPLPOZ(NAGKPL,WERSJAREF,ILOSC) values(:KOMPLET,:werref, :il);
      end
    end
  end
  if(:TYPUZUP = 1) then
    delete from KPLPOZ AA1 where NAGKPL = :komplet and (select count(*) from KPLPOZ where NAGKPL = :fromkpl and WERSJAREF = AA1.WERSJAREF) = 0;
end^
SET TERM ; ^
