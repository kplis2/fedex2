--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_DOMAIN_DDL(
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      CREATE_OR_ALTER smallint)
  returns (
      DDL blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           )
   as
declare variable PARAMETER_TYPE_NAME varchar(80);
declare variable PARAMETER_LENGTH integer;
declare variable PARAMETER_PRECISION integer;
declare variable PARAMETER_FIELD_SUB_TYPE integer;
declare variable PARAMETER_SCALE integer;
declare variable PARAMETER_NULL_FLAG integer;
declare variable PARAMETER_COLLATION_NAME varchar(80);
declare variable PARAMETER_CHARACTER_SET_NAME varchar(80);
declare variable PARAMETER_FIELD_TYPE smallint;
declare variable PARAMETER_FIELD_LENGTH smallint;
declare variable PARAMETER_CHARACTER_SET_ID smallint;
declare variable PARAMETER_COLLATION_ID smallint;
declare variable PARAMETER_FLAG_NULL smallint;
declare variable PARAMETER_CHARACTER_LENGTH smallint;
declare variable PARAMETER_FIELD_SCALE smallint;
declare variable PARAMETER_SEGMENT_LENGTH smallint;
declare variable PARAMETER_DEFAULT_SOURCE blob sub_type 1 segment size 80;
declare variable PARAMETER_VALIDATION_SOURCE blob sub_type 1 segment size 80;
begin
  if(:create_or_alter = 0) then
  begin
    ddl = 'CREATE DOMAIN ' || :nazwa || ' AS ';
    select t.rdb$type_name, f.rdb$field_length, f.rdb$field_precision, f.rdb$field_sub_type,
           f.rdb$field_scale, f.rdb$null_flag, c.rdb$collation_name, ch.rdb$character_set_name, f.rdb$segment_length, f.rdb$default_source, f.rdb$validation_source
      from rdb$fields f
      join rdb$types t on (f.rdb$field_type = t.rdb$type)
      left join rdb$collations c on (c.rdb$collation_id = f.rdb$collation_id and c.rdb$character_set_id = f.rdb$character_set_id)
      left join rdb$character_sets ch on (ch.rdb$character_set_id = f.rdb$character_set_id)
      where f.rdb$field_name = :nazwa and t.rdb$field_name = 'RDB$FIELD_TYPE'
      into :parameter_type_name, :parameter_length, :parameter_precision, :parameter_field_sub_type,
           :parameter_scale, :parameter_null_flag, :parameter_collation_name, :parameter_character_set_name,
           :parameter_segment_length, :parameter_default_source, :parameter_validation_source;

    if(parameter_collation_name is not null) then
      parameter_collation_name = substring(:parameter_collation_name from 1 for position(' ', :parameter_collation_name) - 1);
    if(parameter_character_set_name is not null) then
      parameter_character_set_name = substring(:parameter_character_set_name from 1 for position(' ', :parameter_character_set_name) - 1);
  
    parameter_type_name = substring(:parameter_type_name from 1 for position(' ', :parameter_type_name) - 1);
    if(parameter_segment_length is null) then
      parameter_segment_length = 80;
    if(parameter_type_name = 'DATE') then
      ddl = :ddl || 'DATE';
    else if(parameter_type_name = 'DOUBLE') then
      ddl = :ddl || 'DOUBLE PRECISION';
    else if(parameter_type_name = 'FLOAT') then
      ddl = :ddl || 'FLOAT';
    else if(parameter_type_name = 'INT64' and (parameter_field_sub_type = 1 or parameter_scale<0 or parameter_precision>0)) then
      ddl = :ddl || 'NUMERIC(' || :parameter_precision || ',' || (-parameter_scale) || ')';
    else if(parameter_type_name = 'INT64' and parameter_field_sub_type = 0) then
      ddl = :ddl || 'BIGINT';
    else if(parameter_type_name = 'INT64' and parameter_field_sub_type = 2) then
      ddl = :ddl || 'DECIMAL(' || :parameter_precision || ',' || (-parameter_scale) || ')';
    else if(parameter_type_name = 'LONG' and (parameter_field_sub_type = 0 or parameter_field_sub_type is null)) then
      ddl = :ddl || 'INTEGER';
    else if(parameter_type_name = 'LONG' and parameter_field_sub_type = 1) then
      ddl = :ddl || 'NUMERIC(' || :parameter_precision || ',' || (-parameter_scale) || ')';
    else if(parameter_type_name = 'LONG' and parameter_field_sub_type = 2) then
      ddl = :ddl || 'DECIMAL(' || :parameter_precision || ',' || (-parameter_scale) || ')';
    else if(parameter_type_name = 'SHORT' and parameter_field_sub_type = 0) then
      ddl = :ddl || 'SMALLINT';
    else if(parameter_type_name = 'SHORT' and parameter_field_sub_type = 1) then
      ddl = :ddl || 'NUMERIC(' || :parameter_precision || ',' || (-parameter_scale) || ')';
    else if(parameter_type_name = 'SHORT' and parameter_field_sub_type = 2) then
      ddl = :ddl || 'DECIMAL(' || :parameter_precision || ',' || (-parameter_scale) || ')';
    else if(parameter_type_name = 'TEXT') then
      ddl = :ddl || 'CHAR(' || :parameter_length || ')';
    else if(parameter_type_name = 'TIMESTAMP') then
      ddl = :ddl || 'TIMESTAMP';
    else if(parameter_type_name = 'VARYING') then begin
      if(parameter_character_set_name is not null and
         parameter_character_set_name='UTF8') then
         parameter_length = parameter_length / 4;
      ddl = :ddl || 'VARCHAR(' || :parameter_length || ')';
    end
    else if(parameter_type_name = 'TIME') then
      ddl = :ddl || 'TIME';
    else if(parameter_type_name = 'BLOB') then
      ddl = :ddl || 'BLOB SUB_TYPE '||parameter_field_sub_type||' SEGMENT SIZE '||parameter_segment_length;

    if(parameter_character_set_name is not null) then
      ddl = :ddl || ' CHARACTER SET ' || :parameter_character_set_name;

    if(parameter_default_source is not null) then
      ddl = :ddl || ' ' || :parameter_default_source;

    if(parameter_null_flag is not null) then
      ddl = :ddl || ' NOT NULL';
    if(parameter_collation_name is not null and :create_or_alter = 0) then
      ddl = :ddl || ' COLLATE ' || :parameter_collation_name;
    if(parameter_validation_source is not null) then
      ddl = :ddl || ' ' || :parameter_validation_source;
  end

  if(:create_or_alter = 1) then
  begin
    select f.rdb$field_type, f.rdb$field_length, f.rdb$character_set_id, f.rdb$collation_id, f.rdb$null_flag,
           f.rdb$character_length, f.rdb$field_precision, f.rdb$field_scale
      from rdb$fields f
      where rdb$field_name = :nazwa
      into :parameter_field_type, :parameter_field_length, :parameter_character_set_id, :parameter_collation_id, :parameter_flag_null,
           :parameter_character_length, :parameter_precision, :parameter_field_scale;
      ddl = 'update rdb$fields set rdb$field_type = '   || coalesce(:parameter_field_type, 'null')        ||
            ', rdb$field_length = '                     || coalesce(:parameter_field_length, 'null')      ||
            ', rdb$character_set_id = '                 || coalesce(:parameter_character_set_id, 'null')  ||
            ', rdb$collation_id = '                     || coalesce(:parameter_collation_id, 'null')      ||
            ', rdb$null_flag = '                        || coalesce(:parameter_flag_null, 'null')         ||
            ', rdb$character_length = '                 || coalesce(:parameter_character_length, 'null')  ||
            ', rdb$field_precision = '                  || coalesce(:parameter_precision, 'null')         ||
            ', rdb$field_scale = '                      || coalesce(:parameter_field_scale, 'null')       ||
            ', rdb$field_sub_type = '                   || coalesce(:parameter_field_sub_type, 'null');
      ddl = :ddl || ' where rdb$field_name = ''' || :nazwa || '''';
  end
  ddl = :ddl || ';';
  suspend;
end^
SET TERM ; ^
