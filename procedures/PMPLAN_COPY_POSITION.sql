--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PMPLAN_COPY_POSITION(
      TOELEM integer,
      FROMPOSITION integer)
   as
declare variable toplan integer;
begin
  select pmplan from pmelements where ref = :toelem into :toplan;

  insert into pmpositions (PMELEMENT, PMPLAN, NAME, SYMBOL, KTM, DESCRIPT, QUANTITY, POSITIONTYPE, UNIT,
    CALCPRICE, BUDPRICE)
  select :toelem, :toplan, NAME, SYMBOL, KTM, DESCRIPT, QUANTITY, POSITIONTYPE, UNIT,
    CALCPRICE, BUDPRICE
  from pmpositions where ref = :fromposition;
end^
SET TERM ; ^
