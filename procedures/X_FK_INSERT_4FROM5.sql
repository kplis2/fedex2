--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_FK_INSERT_4FROM5(
      REFD integer)
   as
declare variable ACCOUNT ACCOUNT_ID;
declare variable PERIOD varchar(6);
declare variable IYEAR smallint;
declare variable IMONTH smallint;
declare variable SIDE integer;
declare variable DEBIT numeric(14,2);
declare variable CREDIT numeric(14,2);
declare variable AMOUNT numeric(14,2);
declare variable SETTLEMENT varchar(20);
declare variable CURR_DT numeric(14,2);
declare variable CURR_CT numeric(14,2);
declare variable DT numeric(14,2);
declare variable CT numeric(14,2);
declare variable WINIEN numeric(14,2);
declare variable MA numeric(14,2);
declare variable WINIENZL numeric(14,2);
declare variable MAZL numeric(14,2);
declare variable DESCRIPT varchar(80);
declare variable KONTOFK varchar(20);
declare variable SYMBFAK varchar(20);
declare variable CURRENCY varchar(3);
declare variable TMP varchar(80);
declare variable TODO_CURRDT numeric(14,2);
declare variable TODO_CURRCT numeric(14,2);
declare variable TODO_DTRATE numeric(14,4);
declare variable TODO_CTRATE numeric(14,4);
declare variable DONE_CURRDT numeric(14,2);
declare variable DONE_CURRCT numeric(14,2);
declare variable ACC_CURRDT numeric(14,2);
declare variable ACC_CURRCT numeric(14,2);
declare variable RP_KURS numeric(14,4);
declare variable RP_REF integer;
declare variable REF_DT integer;
declare variable REF_CT integer;
declare variable DT_SYMBOL varchar(20);
declare variable CT_SYMBOL varchar(20);
declare variable I smallint;
declare variable R_WINIEN numeric(14,2);
declare variable R_MA numeric(14,2);
declare variable TMP_DESCRIPT varchar(80);
declare variable DONE smallint;
declare variable BKDOCREF integer;
declare variable KONTO varchar(20);
declare variable ROZRACHUNEK varchar(80);
declare variable SLODEFP integer;
declare variable SLOPOZP integer;
declare variable CURDEBIT numeric(14,2);
declare variable CURCREDIT numeric(14,2);
declare variable RATE numeric(14,4);
declare variable STRONA smallint;


begin
  select d.account, d.settlement, d.bkdoc, d.currdebit, d.currcredit, d.rate, d.side, d.debit
    from decrees d
    where d.ref=:REFD
    into :konto, :rozrachunek, :bkdocref,:curdebit, :curcredit,:rate, :strona, :amount ;


  select B.descript, B.period, B.ref
    from bkdocs B
    where B.ref=:bkdocref
    into :tmp_descript, :period, :bkdocref;

    if (substring(konto from 1 for 1)='5') then begin
    side = 1;
    account = '490';

     /* kocowe operacje - wstawianie dekretu do bazy */
    if (side = 0) then begin debit = amount; credit = 0; end else begin debit = 0; credit = amount; end
    if (account is null) then account = '';
    if (amount is not null and amount <>0) then
    insert into decrees (bkdoc, account, side, debit, credit, descript)
    values (:bkdocref, :account, :side, :debit, :credit, :descript);
      /*  KONIEC DEKRETU */
    end

    execute procedure  x_4from5(konto)
          returning_values  :account;
    side = 0;
      /* kocowe operacje - wstawianie dekretu do bazy */
    if (side = 0) then begin debit = amount; credit = 0; end else begin debit = 0; credit = amount; end
    if (account is null) then account = '';
    if (amount is not null and amount <>0) then
    insert into decrees (bkdoc, account, side, debit, credit, descript)
    values (:bkdocref, :account, :side, :debit, :credit, :descript);
      /*  KONIEC DEKRETU */
  suspend;
end^
SET TERM ; ^
