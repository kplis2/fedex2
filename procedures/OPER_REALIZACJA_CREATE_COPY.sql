--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_REALIZACJA_CREATE_COPY(
      ZAM integer,
      HISTZAM integer,
      REJ varchar(3) CHARACTER SET UTF8                           ,
      TOSTAN integer,
      TRYB smallint,
      DKREAL smallint,
      NOTKLONEBLOK integer)
  returns (
      NEWZAM integer,
      SYMBOL varchar(30) CHARACTER SET UTF8                           )
   as
declare variable NRREAL integer;
declare variable OPERATOR integer;
declare variable NRSREAL varchar(20);
declare variable POPREF integer;
declare variable KTM varchar(40);
declare variable WERSJA integer;
declare variable WERSJAREF integer;
declare variable CENACEN numeric(14,4);
declare variable CENANET numeric(14,4);
declare variable CENABRU numeric(14,4);
declare variable CENAMAG numeric(14,4);
declare variable ILOSC numeric(14,4);
declare variable ILOSCM numeric(14,4);
declare variable ILOSCO numeric(14,4);
declare variable ILREAL numeric(14,4);
declare variable ILREALO numeric(14,4);
declare variable ILREALM numeric(14,4);
declare variable JEDN integer;
declare variable JEDNO integer;
declare variable PRZELICZ numeric(14,4);
declare variable NEWILOSC numeric(14,4);
declare variable NEWILOSCO numeric(14,4);
declare variable NEWILOSCM numeric(14,4);
declare variable NEWILREAL numeric(14,4);
declare variable NEWILREALO numeric(14,4);
declare variable NEWILREALM numeric(14,4);
declare variable NEWILZREAL numeric(14,4);
declare variable NEWILZREALM numeric(14,4);
declare variable MAGAZYN varchar(3);
declare variable MAG2 varchar(3);
declare variable ZAMMAG varchar(3);
declare variable NEWTYPZAM smallint;
declare variable NUMER integer;
declare variable RABAT numeric(14,4);
declare variable NEWPOZREF integer;
declare variable OPK smallint;
declare variable STAN char(1);
declare variable OLDSTAN char(1);
declare variable SKAD integer;
declare variable WALCEN varchar(3);
declare variable ILOSCONMONTAZM decimal(14,4);
declare variable DOSTAWAMAG integer;
declare variable PPARAMN1 numeric(14,2);
declare variable PPARAMN2 numeric(14,2);
declare variable PPARAMN3 numeric(14,2);
declare variable PPARAMN4 numeric(14,2);
declare variable PPARAMN5 numeric(14,2);
declare variable PPARAMN6 numeric(14,2);
declare variable PPARAMN7 numeric(14,2);
declare variable PPARAMN8 numeric(14,2);
declare variable PPARAMN9 numeric(14,2);
declare variable PPARAMN10 numeric(14,2);
declare variable PPARAMN11 numeric(14,2);
declare variable PPARAMN12 numeric(14,2);
declare variable PPARAMS1 varchar(40);
declare variable PPARAMS2 varchar(40);
declare variable PPARAMS3 varchar(40);
declare variable PPARAMS4 varchar(40);
declare variable PPARAMS5 varchar(40);
declare variable PPARAMS6 varchar(40);
declare variable PPARAMS7 varchar(40);
declare variable PPARAMS8 varchar(40);
declare variable PPARAMS9 varchar(40);
declare variable PPARAMS10 varchar(40);
declare variable PPARAMS11 varchar(40);
declare variable PPARAMS12 varchar(40);
declare variable PPARAMD1 timestamp;
declare variable PPARAMD2 timestamp;
declare variable PPARAMD3 timestamp;
declare variable PPARAMD4 timestamp;
declare variable PREC smallint;
declare variable FAKE smallint;
declare variable HAVEFAKE smallint;
declare variable ALTTOPOZ integer;
declare variable ALTTOPOZM integer;
begin
    if(:rej is null or (:rej = '')) then
      select REJESTR from NAGZAM where REF=:zam into :rej;
    if(:notkloneblok is null) then notkloneblok = 0;
    /* tworzenie nowej realizacji */
    select OPERATOR, SKAD from HISTZAM where REF=:HISTZAM into :operator, :skad;
    if(:operator is null) then operator=0;
    /*ustalenie stanu docelowego*/
    select STAN from NAGZAM where REF=:zam into :oldstan;
    stan = :oldstan;
    if(:tostan = 1) then
      stan = 'R';
    else if(:tostan = 2) then
      stan = 'B';
    else if(:tostan = 3) then
      stan = 'D';
    else if(:tostan = 4) then
      stan = 'N';
    /*ustalenie, czy istnieje juz podklad spelniajacy stawoane warunki rejestru i blokad*/
    newzam = null;
    select max(ref) from NAGZAM where org_ref = :zam and ref <> :zam and org_oper = :histzam and rejestr = :rej and TOSTAN = :STAN into :newzam;
    if(:newzam is null) then begin
      select max(NR_REAL) from NAGZAM where ORG_REF = :ZAM into :nrreal;
      if(:nrreal is null)then nrreal = 0;
      nrreal = nrreal + 1;
      execute procedure GEN_REF('NAGZAM') returning_values :newzam;
      select ID from NAGZAM where REF=:zam into :symbol;
      if(:histzam is null or (:histzam = 0))then
        exception OPER_CHECK_REALIZACJA_NOT_HISTZ;
      if(:tryb = 0 or (:tryb = 2) or (:dkreal = 1)) then
        newtypzam = 2;
      else
        newtypzam = 0;
      /*zalozenie naglowka realizacji */
      nrsreal = cast(:nrreal as varchar(20));

      insert into NAGZAM(REF,NR_REAL,NUMER,ID,ZNAKZEWN,REJESTR,TYPZAM,OKRES,KLIENT,DOSTAWCA,UZYKLI,MAGAZYN,MAG2,
       DATAWE,DATAZM, ODBIORCAID,
       ODBIORCA,DULICA,DNRDOMU,DNRLOKALU,DMIASTO,DKODP,DPOCZTA,SPOSDOST,TERMDOST,SPOSZAP,TERMZAP,
       RABAT,UWAGI,STAN/*zawsze nie zablokowany */, DOSTAWA,/*parametry faktury bedzie miala realizacja wlasne*/
       KOSZTDOST,FLAGI,WYSYLKA, TYP,
       WALUTOWE, WALUTA, TABKURS, KURS,
       SPRZEDAWCA, OPERATOR, TOSTAN,
       ORG_REF, ORG_ID, ORG_OPER,
       KKTM,KWERSJA,KWERSJAREF,KILOSC,KILREAL,KILZREAL, SKAD, CKONTRAKTY,PRDEPART,PRSHEET,
       KOSZTDOSTBEZ, KOSZTDOSTPLAT, KOSZTDOSTROZ, TRASADOST, FLAGISPED, UWAGISPED, ILOSCPACZEK, ILOSCPALET, int_symbol) --DTS ZG138468
       select :newzam,:nrreal,NUMER,:symbol ||'/'||:nrsreal,ZNAKZEWN,:rej,TYPZAM,OKRES,KLIENT,DOSTAWCA,UZYKLI,MAGAZYN,MAG2,
       current_date,current_date,ODBIORCAID,
       ODBIORCA,DULICA,DNRDOMU,DNRLOKALU,DMIASTO,DKODP,DPOCZTA,SPOSDOST,TERMDOST,SPOSZAP,TERMZAP,
       RABAT,UWAGI,'N',DOSTAWA,
       KOSZTDOST,FLAGI,WYSYLKA, :newtypzam,
       WALUTOWE,WALUTA,TABKURS,KURS,
       SPRZEDAWCA,:operator,:STAN,
       REF,ID,:histzam,
       KKTM,KWERSJA,KWERSJAREF,KILOSC,KILREAL,KILZREAL, :skad, CKONTRAKTY,PRDEPART,PRSHEET,
       KOSZTDOSTBEZ, KOSZTDOSTPLAT, KOSZTDOSTROZ, TRASADOST, FLAGISPED, UWAGISPED, ILOSCPACZEK, ILOSCPALET, int_symbol --DTS ZG138468
       from NAGZAM where REF=:zam;
     
       update nagzam set datazm = current_date where REF=:zam;
    end
    /* zaozenie pozycji realizacji */
    for select REF,NUMER,MAGAZYN,MAG2,KTM,WERSJA,WERSJAREF,
               ILOSC,ILOSCO, ILOSCM, ILREAL, ILREALO, ILREALM,
               JEDN, JEDNO, CENAMAG, DOSTAWAMAG, CENACEN, WALCEN, RABAT,CENANET,CENABRU, OPK,
               paramn1, paramn2, paramn3, paramn4,
               paramn5, paramn6, paramn7, paramn8,
               paramn9, paramn10, paramn11, paramn12,
               params1, params2, params3, params4,
               params5, params6, params7, params8,
               params9, params10, params11, params12,
               paramd1, paramd2, paramd3, paramd4, prec,
               fake, havefake, alttopoz
      from POZZAM where ZAMOWIENIE=:ZAM
      order by POZZAM.alttopoz nulls first, POZZAM.NUMER
    into :popref,:NUMER,:MAGAZYN,:MAG2,:KTM,:WERSJA,:wersjaref,
         :ILOSC,:ILOSCO, :ILOSCM, :ILREAL,:ILREALO, :ILREALM,
         :JEDN, :JEDNo, :cenamag, :dostawamag, :cenacen, :walcen, :rabat, :cenanet, :cenabru, :opk,
         :pparamn1, :pparamn2, :pparamn3, :pparamn4,
         :pparamn5, :pparamn6, :pparamn7, :pparamn8,
         :pparamn9, :pparamn10, :pparamn11, :pparamn12,
         :pparams1, :pparams2, :pparams3 ,:pparams4,
         :pparams5, :pparams6, :pparams7, :pparams8,
         :pparams9, :pparams10, :pparams11, :pparams12,
         :pparamd1, :pparamd2, :pparamd3 ,:pparamd4, :prec,
         :fake, :havefake, :alttopoz
    do begin
      newilosc = null;
      if(:tryb = 0 ) then begin
        newilosc = :ilreal;
        newilosco = :ilrealo;
        newiloscm = :ilrealm;
        newilreal = :ilreal;
        newilrealo = :ilrealo;
        newilrealm = :ilrealm;
        newilzreal = :ilreal;
        newilzrealm = :ilrealm;
      end else begin
        /*dla podkladów innych zamowien - ilosc zrealizowana i realizowana sa zerowe, a
         ilosc to ilosc dysponowana danycm zamówieniem*/
        newilreal = 0;
        newilrealo = 0;
        newilrealm = 0;
        newilzreal = 0;
        newilzrealm = 0;
        if(:tryb = 1) then begin/*dyspozycja porednia*/
          select sum(ilosc), sum(iloscm) from POZZAMDYSP where POZZAM = :popref and typ = :tryb into :newilosc, :newiloscm;
          /*przliczenie jednostek z magazynowych na opakowania - moga być rżne*/
          przelicz = null;
          select PRZELICZ from TOWJEDN where ref = :jedno into :przelicz;
          if(:przelicz > 0) then
            newilosco = :newiloscm / :przelicz;
        end else if (:tryb = 2) then begin
          select sum(ilosc), sum(iloscm) from POZZAMDYSP where POZZAM = :popref and typ = :tryb into :newilosc, :newiloscm;
          /*przliczenie jednostek z magazynowych na opakowania - moga być rżne*/
          przelicz = null;
          select PRZELICZ from TOWJEDN where ref = :jedno into :przelicz;
          if(:przelicz > 0) then
            newilosco = :newiloscm / :przelicz;
          newilreal = :newilosc;
          newilrealo = :newilosco;
          newilrealm = :newiloscm;
          newilzreal = :newilosc;
          newilzrealm = :newiloscm;
        end else if(:tryb = 3) then begin
          select sum(KILOSC) from NAGZAM where typ < 2 and KPOPREF=:popref into :ilosconmontazm;
          if(:ilosconmontazm is null) then ilosconmontazm = 0;
          select sum(ILOSCM) from POZZAMDYSP where POZZAM = :popref and typ = :tryb into :newiloscm;
          if(:newiloscm is null) then newiloscm = 0;
          newiloscm = :newiloscm + :ilosconmontazm;
          /*przliczenie z magazynowych na rozliczeniowe i opakowania*/
          przelicz = null;
          select PRZELICZ from TOWJEDN where ref = :jedno into :przelicz;
          if(:przelicz > 0) then
            newilosco = :newiloscm / :przelicz;
          przelicz = null;
          select PRZELICZ from TOWJEDN where ref = :jedn into :przelicz;
          if(:przelicz > 0) then
            newilosc = :newiloscm / :przelicz;
        end else
          newilosc = 0;
      end
      if(:newilosc is null) then newilosc = 0;
      if(:newilosc <> 0) then begin
        if(:notkloneblok = 0) then begin
          update POZZAM set ILTYMBLOK = :newilosc where REF=:popref;
        end
        newpozref = null;
        select ref from POZZAM where POPREF = :popref and ZAMOWIENIE = :newzam into :newpozref;
        if(:newpozref is null) then begin
          execute procedure GEN_REF('POZZAM') returning_values :newpozref;
          alttopozm = null;
          if (alttopoz is not null) then
          begin
            select d.ref
              from pozzam d
              where d.zamowienie = :newzam                   --BS68878
        and d.popref = :alttopoz
            into :alttopozm;
          end
          insert into POZZAM(REF,POPREF,ZAMOWIENIE,MAGAZYN,MAG2,KTM,WERSJA,wersjaref,
                      ILOSC,ILREAL,ILZREAL,
                      ILOSCM, ILREALM, ILZREALM,
                      ILOSCO, ILREALO, JEDN, JEDNO,
                      ILDYSP, ILZDYSP,ILZADYSP,
                      CENAMAG,DOSTAWAMAG,CENACEN,WALCEN,RABAT,CENANET,CENABRU, OPK, blokadarez,
                      paramn1, paramn2, paramn3, paramn4,
                      paramn5, paramn6, paramn7, paramn8,
                      paramn9, paramn10, paramn11, paramn12,
                      params1, params2, params3, params4,
                      params5, params6, params7, params8,
                      params9, params10, params11, params12,
                      paramd1, paramd2, paramd3, paramd4, prec,
                      fake, havefake, alttopoz)
          values (:newpozref,:popref, :newzam, :magazyn, :mag2, :ktm, :wersja, :wersjaref,
                      :newilosc, :newilreal, :newilzreal,
                      :newiloscm, :newilrealm, :newilzrealm,
                      :newilosco, :newilrealo, :jedn, :jedno,
                      0,0, 0,
                      :cenamag, :dostawamag, :cenacen, :walcen, :rabat, :cenanet, :cenabru,:opk, :notkloneblok,
                      :pparamn1, :pparamn2, :pparamn3, :pparamn4,
                      :pparamn5, :pparamn6, :pparamn7, :pparamn8,
                      :pparamn9, :pparamn10, :pparamn11, :pparamn12,
                      :pparams1, :pparams2, :pparams3 ,:pparams4,
                      :pparams5, :pparams6, :pparams7, :pparams8,
                      :pparams9, :pparams10, :pparams11, :pparams12,
                      :pparamd1, :pparamd2, :pparamd3 ,:pparamd4, :prec,
                      :fake, :havefake, :alttopozm);
        end else begin
          if(:newilosc is null) then newilosc = 0;
          if(:newilreal is null) then newilreal = 0;
          if(:newilzreal is null) then newilzreal = 0;
          if(:newiloscm is null) then newiloscm = 0;
          if(:newilrealm is null) then newilrealm = 0;
          if(:newilzrealm is null) then newilzrealm = 0;
          if(:newilosco is null) then newilosco  = 0;
          if(:newilrealo is null) then newilrealo = 0;
          update POZZAM set ILOSC = ILOSC + :newilosc, ILREAL = ILREAL + :newilreal, ILZREAL = ILZREAL + :newilzreal,
                            ILOSCM = ILOSCM + :newiloscm, ILREALM = ILREALM + :newilrealm, ILZREALM = ILZREALM + :newilzrealm,
                            ILOSCO = ILOSCO + :newilosco, ILREALO = ILREALO + :newilrealo
             where ref=:newpozref;
        end
        /*przepisanie wlasciwych elementów - blokad itp*/
        if(:tryb = 0) then
           execute procedure COPY_DOKUMSER(:popref, :newpozref, :newilosc, 'Z', null, 1);
/*        execute procedure REZ_BLOK_ZAM_PRZES(:popref, :newpozref,:newilosc,:stan);*/
        if(:tryb = 1) then
           update POZZAMDYSP set POZZAM = :newpozref where POZZAM = :popref and typ = 1;
        if(:tryb = 2) then
           update POZZAMDYSP set POZZAM = :newpozref where POZZAM = :popref and typ = 2;
        if(:tryb = 3) then begin
           update POZZAMDYSP set POZZAM = :newpozref where POZZAM = :popref and typ = 3;
           update NAGZAM set KPOPREF = :newpozref where KPOPREF = :popref;
        end
      end
      /*realizacja przepisana na podklad - wyczyszczenie ilosci realizowanych na zamowieniu glownym*/
      if(tryb = 0) then
        update POZZAM set ILREAL = 0, ILREALM = 0, ILREALO = 0 where REF=:popref;
    end
    if(:tryb = 0 or (:tryb = 2)) then
      update NAGZAM set TYP=1 where REF=:zam and TYP = 0;
    update NAGZAM set STAN = :oldstan where NAGZAM.ref = :newzam and STAN <> :oldstan;

end^
SET TERM ; ^
