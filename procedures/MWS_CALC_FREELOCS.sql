--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CALC_FREELOCS(
      MWSCONSTLOCIN integer,
      ACTIN smallint)
   as
declare variable ref integer;
declare variable lnum integer;
declare variable wharea integer;
begin
  if (actin > 0) then
  begin
    select act, wharea, mwsstandlevelnumber from mwsconstlocs where ref = :mwsconstlocin
      into actin, wharea, lnum;
    if (actin is null) then actin = 0;
    if (wharea is null or lnum is null) then actin = 0;
    if (actin > 0) then
    begin
      ref = null;
      select first 1 ref from mwsstock where mwsconstloc = :mwsconstlocin and ispal <> 1
        into ref;
      if (ref is not null) then
        actin = 0;
    end
    if (actin <= 0) then
      delete from MWSFREEMWSCONSTLOCS where ref = :mwsconstlocin;
    else
    begin
      if (not exists (select ref from mwsfreemwsconstlocs where ref = :mwsconstlocin)) then
      begin
        insert into MWSFREEMWSCONSTLOCS (ref, symbol, mwsstandlevelnumber, deep, distfromstartarea,
            levelheight, wharea, mwsconstloctype, l, w, h, areatype, coordzb, wh,
            whsec, whsecrow, deliveryarea, wharealogfromstartarea)
          select c.ref, c.symbol, c.mwsstandlevelnumber, c.deep, c.distfromstartarea,
              c.levelheight, c.wharea, c.mwsconstloctype, c.l, c.w, c.h, w.areatype, c.coordzb, c.wh,
              c.whsec, w.whsecrow, s.deliveryarea, c.wharealogfromstartarea
            from mwsconstlocs c
              left join whareas w on (w.ref = c.wharea)
              left join whsecs s on (s.ref = c.whsec)
            where c.ref = :mwsconstlocin;
      end
    end
  end
  else if (actin <= 0) then
    delete from MWSFREEMWSCONSTLOCS where ref = :mwsconstlocin;
end^
SET TERM ; ^
