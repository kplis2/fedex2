--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_CHECK_CAN_PACK(
      LISTYWYSD INTEGER_ID,
      QUANTITY NUMERIC_14_4,
      KTM KTM_ID)
  returns (
      STATUS INTEGER_ID,
      MSG STRING100)
   as
  declare variable quantityav numeric_14_4;
  declare variable quantitypac numeric_14_4;
  declare variable dokument string40;
  declare variable grupasped integer_id;
  declare variable refdok integer_id;
begin
  status = 1;
  msg = '';
-- porpawki dla grupy spedycyjnej
  select l.refdok
    from listywysd l
    where l.ref = :listywysd
  into :refdok;

  select d.grupasped
     from dokumnag d
     where d.ref = :refdok
  into :grupasped;

  select sum(ma.quantity) --dostepna ilosc na zaakceptowanych pozycjach i dokumentach w grupie spedycyjnej
      from mwsords m
      join mwsacts ma on m.ref = ma.mwsord
      join dokumnag d on ma.docid = d.ref
      where ma.good = :ktm
        and m.status = 5
        and ma.status = 5
        and d.grupasped = :grupasped
    into :quantityav;

    select sum(lis.ILOSCMAG) -- ilosc spakowana
      from LISTYWYSDROZ lis
      join listywysd l on lis.listywysd = l.ref
      join dokumnag d on d.ref = l.refdok
      where d.grupasped = :grupasped
        and lis.ktm = :ktm
    into :quantitypac;

    if (coalesce(quantitypac, 0) + coalesce(quantity, 0) <= coalesce(quantityav, 0)) then -- czy suma ilosci spakowanej i tej ktora chcemy dodac nie jest wieksza od dostepnej na zaakceptowanych dokumentach i pozycjach
      suspend;
    else begin
      select first 1 m.symbol
        from mwsords m
        join mwsacts ma on m.ref = ma.mwsord
        join listywysd l on l.refdok = ma.docid
        where l.ref = :listywysd
              and ma.good = :ktm
              and (ma.status is distinct from 5 or m.status is distinct from 5)
      into :dokument;
      status = 0;
      msg = msg ||' Dokument lub pozycja nie zaakceptowana - '||coalesce(:dokument, ' ');
      suspend;
    end

end^
SET TERM ; ^
