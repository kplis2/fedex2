--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_DELETE_MWSORD(
      MWSORDREF integer)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING)
   as
begin
  status = 1;
  msg = '';

  update mwsacts a set a.status = 0 where a.mwsord = :mwsordref;
  update mwsords o set o.status = 0 where o.ref = :mwsordref;
  delete from mwsords o where o.ref = :mwsordref;

end^
SET TERM ; ^
