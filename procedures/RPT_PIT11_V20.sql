--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PIT11_V20(
      CURRENTCOMPANY integer,
      YEARID char(4) CHARACTER SET UTF8                           ,
      PERSON_INPUT integer,
      TYP smallint,
      DATA date,
      CORRECT smallint,
      SIGNFNAME STRING255,
      SIGNSNAME STRING255,
      EDECLREFS STRING8191 = '')
  returns (
      P01 STRING100,
      P02 STRING255,
      P03 integer,
      P04 char(4) CHARACTER SET UTF8                           ,
      P05 STRING20,
      P05NAME STRING255,
      P06 integer,
      P07 integer,
      P08 STRING100,
      P09 STRING100,
      P10LEN integer,
      P10 STRING100,
      P11 STRING100,
      P12 STRING100,
      P13 STRING10,
      P14 STRING100,
      P15 STRING100,
      P16 STRING100,
      P17 STRING100,
      P18 STRING100,
      P19 STRING100,
      P20 STRING100,
      P21 STRING100,
      P22 STRING100,
      P23 STRING100,
      P24 smallint,
      P25 numeric(14,2),
      P26 numeric(14,2),
      P27 numeric(14,2),
      P29 numeric(14,2),
      P45 numeric(14,2),
      P46 numeric(14,2),
      P47 numeric(14,2),
      P48 numeric(14,2),
      P45R numeric(14,2),
      P46R numeric(14,2),
      P47R numeric(14,2),
      P48R numeric(14,2),
      P53 numeric(14,2),
      P54 numeric(14,2),
      P55 numeric(14,2),
      P56 numeric(14,2),
      P57 numeric(14,2),
      P66 numeric(14,2),
      P68 numeric(14,2),
      P70 numeric(14,2),
      P71 numeric(14,2),
      P72 numeric(14,2),
      P73 STRING255,
      P74 STRING255,
      PERSON PERSONS_ID)
   as
declare variable AKTUOPERATOR integer;
declare variable DESCRIPT varchar(255);
declare variable NIP STRING20;
declare variable PESEL STRING20;
declare variable BUSINESSACTIV smallint;
declare variable NAME STRING255;
declare variable FIELD INTEGER_ID;
declare variable VAL STRING255;
declare variable EDECLREF EDECLARATIONS_ID;
begin
--Personel: Generowanie danych do PITu-11 w wersji 20 i 21 (wydruk i e-deklaracja)

  if (edeclrefs <> '') then
  begin
  -- pobieranie danych do wydruku e-edeklracji =================================
    edeclrefs = ',' || :edeclrefs || ',';
    for
      select distinct e.ref, e.refid, e.status from edeclarations e
        join edecldefs d on (e.edecldef = d.ref)
        where (:edeclrefs containing ',' || e.ref || ','
            or :edeclrefs containing ',' || e.declgroup || ',')
          and d.isgroup = 0
          and e.person is not null
          and d.systemcode in ('PIT-11 (20)', 'PIT-11 (21)')
        into :edeclref, :p02,  :p03 --BS70284 
    do begin

      p01 = null;    p04 = null;    p05 = null;    p05name = null; p06 = null;
      p07 = null;    p08 = null;    p09 = null;    p10len = null;  p10 = null;
      p11 = null;    p12 = null;    p13 = null;    p14 = null;     p15 = null;
      p16 = null;    p17 = null;    p18 = null;    p19 = null;     p20 = null;
      p21 = null;    p22 = null;    p23 = null;    p24 = null;     p25 = null;
      p26 = null;    p27 = null;    p29 = null;    p45 = null;     p46 = null;
      p47 = null;    p48 = null;    p53 = null;    p54 = null;     p55 = null;
      p56 = null;    p57 = null;    p66 = null;    p68 = null;     p70 = null;
      p71 = null;    p72 = null;    p73 = null;    p74 = null;

      for
        select field, pvalue from edeclpos
          where edeclaration = :edeclref
          into :field, :val
      do begin
        if (field = 1) then p01 = val;
        else if (field = 4) then p04 = val;
        else if (field = 5) then
        begin
          p05 = val;
          select first 1 name || ',' || address || ','|| city || ' ' || postcode
            from einternalrevs
            where code = :p05
            into :p05name;
        end
        else if (field = 6) then p06 = val;
        else if (field = 7) then p07 = val;
        else if (field = 8) then p08 = val;
        else if (field = 9) then p09 = val;
        else if (field = 10) then
        begin
          p10 = val;
          p10len = char_length(trim(replace(replace(p10,'-',''),' ','')));
        end
        else if (field = 11) then p11 = val;
        else if (field = 12) then p12 = val;
        else if (field = 13) then p13 = val;
        else if (field = 14) then --BS69981 
        begin
          select name from countries where symbol = :val
            into :p14;
          p14 = coalesce(p14,val);
        end
        else if (field = 15) then p15 = val;
        else if (field = 16) then p16 = val;
        else if (field = 17) then p17 = val;
        else if (field = 18) then p18 = val;
        else if (field = 19) then p19 = val;
        else if (field = 20) then p20 = val;
        else if (field = 21) then p21 = val;
        else if (field = 22) then p22 = val;
        else if (field = 23) then p23 = val;
        else if (field = 24) then p24 = val;
        else if (field = 25) then p25 = val;
        else if (field = 26) then p26 = val;
        else if (field = 27) then p27 = val;
        else if (field = 29) then p29 = val;
        else if (field = 45) then p45 = val;
        else if (field = 46) then p46 = val;
        else if (field = 47) then p47 = val;
        else if (field = 48) then p48 = val;
        else if (field = 53) then p53 = val;
        else if (field = 54) then p54 = val;
        else if (field = 55) then p55 = val;
        else if (field = 56) then p56 = val;
        else if (field = 57) then p57 = val;
        else if (field = 66) then p66 = val;
        else if (field = 68) then p68 = val;
        else if (field = 70) then p70 = val;
        else if (field = 71) then p71 = val;
        else if (field = 72) then p72 = val;
        else if (field = 73) then p73 = val;
        else if (field = 74) then p74 = val;
      end
      suspend;
    end
  end else begin
  -- generowanie danych do wydruku badz do e-deklaracji ========================

    p04 = yearid;
    p06 = correct + 1;
    p72 = 2;
    p73 = signfname;
    p74 = signsname;

    select nip, phisical, descript from e_get_platnikinfo4pit
      into :p01, :p07, :name;

    if (p07 = 1) then p08 = name;
    else if (p07 = 2) then p09 = name;

    if (person_input = 0) then person_input = null;

    for
      select distinct e.person, p.sname, p.fname, f.dateout, p.nationality, replace(p.nip,'-','') as nip, p.pesel
        from epayrolls pr 
          join eprpos pp on (pr.ref = pp.payroll)
          join employees e on (pp.employee = e.ref)
          join persons p on (e.person = p.ref)
          left join efunc_get_format_date(p.birthdate) f on (1 = 1)
        where pp.pvalue <> 0 and pp.ecolumn in (3000, 5950)
          and pr.tper starting with :yearid
          and (e.person = :person_input or :person_input is null)
          and pr.company = :currentcompany
          and e.company = :currentcompany
          and pr.lumpsumtax = 0
        order by e.personnames
        into :person, :p11, :p12, :p13, :p14, :nip, :pesel
    do begin

      p25 = null;
      p26 = null;
      p27 = null;
      p29 = null;
      p45 = null;
      p46 = null;
      p47 = null;
      p48 = null;
      p45r = null;
      p46r = null;
      p47r = null;
      p48r = null;
      p53 = null;
      p54 = null;
      p55 = null;
      p56 = null;
      p57 = null;
      p66 = null;
      p68 = null;

   -- przychod pracownika ======================================================
      select sum(p.pvalue)
        from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype = 1)
          join employees e on (e.ref = p.employee)
          join ecolumns c on (p.ecolumn = c.number)
        where r.tper starting with :yearid
          and e.person = :person
          and e.company = :currentcompany
          and r.company = :currentcompany
          and ((c.number = 5950 and r.prtype = 'SOC') --przychod z list socjalnych (BS36243)
            or (c.cflags like '%;POD;%' and r.prtype <> 'SOC' and c.number <> 5950)) --przychod z list innych niz socjalne
        into :p25;

   -- koszty uzyskania i zaliczka na podatek dochodowy (z wyrownaniem) pracownika
      select sum(case when p.ecolumn = 6500 then p.pvalue else 0 end)
           , sum(case when p.ecolumn in (7350, 7370) then p.pvalue else 0 end)
        from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype = 1)
          join employees e on (e.ref = p.employee)
        where p.ecolumn in (6500, 7350, 7370)
          and r.tper starting with :yearid
          and e.person = :person
          and e.company = :currentcompany
          and r.company = :currentcompany
        into :p26, :p29;

      if (p25 is not null and p26 is not null) then
        p27 = coalesce(p25,0) - coalesce(p26,0); -- dochod pracownika

   -- przychod, koszty i zaliczka czlonka rady naddzorczej =====================
      select sum(case when p.ecolumn = 5950 then p.pvalue else 0 end)
           , sum(case when p.ecolumn = 6500 then p.pvalue else 0 end)
           , sum(case when p.ecolumn in (7350, 7370) then p.pvalue else 0 end)
        from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype = 3)
          join employees e on (e.ref = p.employee)
        where p.ecolumn in (5950, 6500, 7350, 7370)
          and r.tper starting with :yearid
          and e.person = :person
          and e.company = :currentcompany
          and r.company = :currentcompany
          and r.lumpsumtax = 0
        into :p45r, :p46r, :p48r;

      p47r = p45r - p46r;  -- dochod czlonka rady naddzorczej

   -- przychod, koszty i zaliczka umowy UCP i prawa autorskie ==================
      select sum(case when p.ecolumn = 3000 and coalesce(t.rprcosts,0) <> 50 then p.pvalue else 0 end)
           , sum(case when p.ecolumn = 6500 and coalesce(t.rprcosts,0) <> 50 then p.pvalue else 0 end)
           , sum(case when p.ecolumn in (7350, 7370) and coalesce(t.rprcosts,0) <> 50 then p.pvalue else 0 end)
           , sum(case when p.ecolumn = 3000 and coalesce(t.rprcosts,0) = 50 then p.pvalue else 0 end)
           , sum(case when p.ecolumn = 6500 and coalesce(t.rprcosts,0) = 50 then p.pvalue else 0 end)
           , sum(case when p.ecolumn in (7350, 7370) and coalesce(t.rprcosts,0) = 50 then p.pvalue else 0 end)
        from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype = 2)
          join emplcontracts c on (c.ref = r.emplcontract)
          join employees e on (e.ref = p.employee)
          left join e_get_realprcosts4ucp(r.ref, 50, 5) t on (1 = 1)
        where p.ecolumn in (3000, 6500, 7350, 7370)
          and r.tper starting with :yearid
          and e.person = :person
          and e.company = :currentcompany
          and r.company = :currentcompany
          and r.lumpsumtax = 0
        into :p45, :p46, :p48
           , :p56, :p57, :p55;

      p47 = p45 - p46;  -- dochod z umowy UCP
      p54 = p56 - p57;  -- dochod z umowy prawa autorskie
      if (p53 is not null) then p54 = coalesce(p54,0) + p53;

   -- suma wartosci UCP i RN
      p45 = coalesce(p45r,0) + coalesce(p45,0); --przychod
      p46 = coalesce(p46r,0) + coalesce(p46,0); --koszty uzyskania
      p47 = coalesce(p47r,0) + coalesce(p47,0); --dochod
      p48 = coalesce(p48r,0) + coalesce(p48,0); --zaliczka na podatek
  
   -- skladki na ubezpieczenie spoleczne i zdrowotne ===========================
      select sum(case when p.ecolumn <> 7210 then p.pvalue else 0 end)
           , sum(case when p.ecolumn = 7210 then p.pvalue else 0 end)
        from epayrolls r
          join eprpos p on (r.ref = p.payroll)
          join employees e on (e.ref = p.employee)
        where p.ecolumn in (6100, 6110, 6120, 6130, 7210)
          and r.tper starting with :yearid
          and e.person = :person
          and e.company = :currentcompany
          and r.company = :currentcompany
          and r.lumpsumtax = 0
        into :p66, :p68;

   -- nullowanie wartosci zerowych =============================================
      if (coalesce(p25,p29) = 0) then --przychod um. o prace
      begin
        p25 = null;
        if (p26 = 0) then p26 = null;
        if (p27 = 0) then p27 = null;
        if (p29 = 0) then p29 = null;
      end
  
      if (p45 = 0) then --przychod UCP <> 50%
      begin
        p45 = null;
        if (p46 = 0) then p46 = null;
        if (p47 = 0) then p47 = null;
        if (p48 = 0) then p48 = null;
      end

      p53 = coalesce(p53,0);
      p56 = coalesce(p56,0);
      if (p53 = 0 and p56 = 0) then --przychod UCP = 50%
      begin
        p53 = null;
        p56 = null;
        if (p54 = 0) then p54 = null;
        if (p55 = 0) then p55 = null;
        if (p57 = 0) then p57 = null;
      end else begin
        p54 = coalesce(p54,0);
        p55 = coalesce(p55,0);
        p57 = coalesce(p57,0);
      end

      if (p66 = 0) then p66 = null;
      if (p68 = 0) then p68 = null;

   -- pobranie informacji podatkowych (US, koszty uzyskania, itp.) =============
      select costs, uscode, businessactiv, taxoffice
        from e_get_perstaxinfo4pit(:yearid, :person, 0, :currentcompany) --BS44508
        into :p24, :p05, :businessactiv, :p05name;

      if (businessactiv = 1) then
      begin
        p10 = :nip;
        p10len = 10;
      end else begin
        p10 = :pesel;
        p10len = 11;
      end
      if (p24 = 0) then p24 = null;

   -- pobranie informacji o adresie podatnika ==================================
      select cpwoj, district, community, street, home_nr, local_nr, city, post_code, post
        from get_persaddress(-:person, null, 1, '102')
        into :p15, :p16, :p17, :p18, :p19, :p20, :p21, :p22, :p23;
  
      suspend;
  
      if (typ = 1) then
      begin
        execute procedure get_global_param ('AKTUOPERATOR')
          returning_values aktuoperator;
        descript = 'Przekazanie danych na formularzu PIT-11 do ' || coalesce(:p05name, 'Urzędu Skarbowego');
        if (not exists (select first 1 1 from epersdatasecur
                          where person = :person and descript = :descript and regdate = :data
                            and coalesce(operator,0) = coalesce(:aktuoperator,0) and kod = 2)
        ) then
          insert into epersdatasecur (person, kod, descript, operator, regdate)
            values (:person, 2, :descript, :aktuoperator, :data);
      end
    end
  end
end^
SET TERM ; ^
