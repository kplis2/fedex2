--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SRV_BUDUJ_TRASE(
      SRVREQUEST integer)
   as
DECLARE VARIABLE REF INTEGER;
DECLARE VARIABLE REF2 INTEGER;
DECLARE VARIABLE CITY VARCHAR(20);
DECLARE VARIABLE TRASA VARCHAR(255);
begin
  ref2=0;
  city='';
  select max(ref) from srvpositions where srvrequest = :srvrequest and ktm='USLUGA0002' into :ref2;
  if (:ref2>0) then begin
    trasa = 'GRANICA';
    for select substring(city from 1 for 20) from SRVLOCATIONS where srvrequest = :srvrequest and destination = 1
      order by number
      into :city
    do begin
     if(coalesce(char_length(:trasa),0)<235) then begin -- [DG] XXX ZG119346
       if(:trasa<>'') then trasa = :trasa || ' - ';
       trasa = :trasa || :city;
      end
    end
    update srvpositions set descript = :trasa where ref=:ref2;
  end
  ref=0;
  trasa = '';
  city='';
  select max(ref) from srvpositions where srvrequest = :srvrequest and ktm='USLUGA0001' into :ref;
  if (:ref>0) then begin
    for select substring(city from 1 for 20) from SRVLOCATIONS where srvrequest = :srvrequest and destination = 0
      order by number
      into :city
      do begin
        if(coalesce(char_length(:trasa),0)<235) then begin -- [DG] XXX ZG119346
          if(:trasa<>'') then trasa = :trasa || ' - ';
          trasa = :trasa || :city;
        end
      end
    if(:ref2>0) then trasa = :trasa||' - GRANICA';
    update srvpositions set descript = :trasa where ref=:ref;
  end
end^
SET TERM ; ^
