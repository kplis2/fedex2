--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNOT_OKRESY(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      TYP varchar(3) CHARACTER SET UTF8                           ,
      OKRES1 varchar(6) CHARACTER SET UTF8                           ,
      OKRES2 varchar(6) CHARACTER SET UTF8                           )
  returns (
      OKRESSID varchar(6) CHARACTER SET UTF8                           ,
      OKRESFK varchar(6) CHARACTER SET UTF8                           ,
      WARTOSC numeric(14,2))
   as
begin
  okressid = okres1;
  okresfk = okres2;
  select sum(dokumnotp.wartosc)
    from dokumnot
      join dokumnotp on (dokumnotp.dokument=dokumnot.ref)
      left join dokumroz on (dokumroz.ref=dokumnotp.dokumrozkor)
      left join dokumpoz on (dokumpoz.ref=dokumroz.pozycja)
      left join dokumnag on (dokumnag.ref=dokumpoz.dokument)
    where dokumnot.typ=:typ and dokumnot.magazyn=:magazyn
      and dokumnot.okres=:okresfk and dokumnag.okres=:okressid
    into :wartosc;
  suspend;
  okressid = okres2;
  okresfk = okres1;
  select  sum(dokumnotp.wartosc)
    from dokumnot
      join dokumnotp on (dokumnotp.dokument=dokumnot.ref)
      left join dokumroz on (dokumroz.ref=dokumnotp.dokumrozkor)
      left join dokumpoz on (dokumpoz.ref=dokumroz.pozycja)
      left join dokumnag on (dokumnag.ref=dokumpoz.dokument)
    where dokumnot.typ=:typ and dokumnot.magazyn=:magazyn
      and dokumnot.okres=:okresfk and dokumnag.okres=:okressid
    into :wartosc;
  suspend;
end^
SET TERM ; ^
