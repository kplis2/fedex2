--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_ADDCUSTOMER_RESPONSE(
      EDEDOCSH_REF integer)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
   as
declare variable shippingdoc integer;
declare variable fedexno string20;
declare variable cref integer;
begin
  otable = 'LISTYWYSD';

  select oref
    from ededocsh
    where ref = :ededocsh_ref
  into :shippingdoc;
  oref = :shippingdoc;

  select cast(p.val as integer)
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.name = 'nrExt'
  into :cref;

  select trim(p.val)
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.name = 'numer'
  into :fedexno;

/* [DG] XXX DROPS  update dostawcy d set d.fedexnrklienta = :fedexno
    where d.ref = :cref;  */

  suspend;
end^
SET TERM ; ^
