--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_GEN_DOC_FROM_MWSORD(
      MWSORD integer,
      DOCTYPE varchar(3) CHARACTER SET UTF8                           )
  returns (
      DOCID integer)
   as
declare variable dokument dokumnag_id;
declare variable ktm ktm_id;
declare variable wersjaref wersje_id;
declare variable ilosc  quantity_mws;
declare variable opk smallint_id;
declare variable frommwsact mwsacts_id;
declare variable x_partia date_id;
declare variable x_serial_no integer_id;
declare variable x_slownik integer_id;
declare variable x_havefake_vers wersje_id;
declare variable fake smallint_id;
declare variable last_havefake wersje_id;
declare variable numer integer_id;
declare variable alttopoz dokumpoz_id;
declare variable ktmkomplet ktm_id;
begin

--exception universal mwsord ||' '||doctype ;
  select o.docid
    from mwsords o where o.ref = :mwsord
    into docid;
  if (docid is not null) then exit;
  execute procedure gen_ref('DOKUMNAG') returning_values docid;
--<<XXX
  insert into DOKUMNAG(REF,MAGAZYN, TYP, DATA, DOSTAWA, DOSTAWCA, KLIENT, ZAMOWIENIE, AKCEPT, BLOKADA, MWSBLOCKMWSORDS, frommwsord, zrodlo, operator)
    select :docid, o.wh, :doctype, current_date, null, slopoz, null, null, 0, 0, 1, o.ref,99, operator --Ldz dodalem operatora
--XXX>>
    from mwsords o
    where REF = :mwsord;
--<<XXX
-- zakomentowane poniewaz wprowadzono nowe rozwiazanie ze wzgledu na myjnie MKD
 --   if (doctype <> 'PZ') then  begin
 -- insert into DOKUMPOZ(DOKUMENT, KTM, WERSJAREF, ILOSC, opk, FROMMWSACT,
 --       x_partia, x_serial_no, x_slownik)    -- XXX KBI dodaem kopiowanie xowych pól, bo sa potrzebne przy genrowaniau PZ-
  --  select :docid, a.good, a.vers, a.quantityc, 0, a.ref,
  --      x_partia, x_serial_no, x_slownik     -- XXX KBI dodaem kopiowanie xowych pól, bo sa potrzebne przy genrowaniau PZ-
  --    from mwsacts a
  --    where a.mwsord = :mwsord and a.status = 5 and a.docid = 0
--XXX>>
 --     order by a.mwsord, a.good;
 --    end
  --<<XXX
 -- else if (doctype = 'PZ') then begin
      numer = 0;
      for
        select :docid, a.good, a.vers, a.quantityc, 0, a.ref,
            x_partia, x_serial_no, x_slownik, a.x_havefake_vers
            from mwsacts a
            where a.mwsord = :mwsord and a.status = 5 and a.docid = 0
            order by x_havefake_vers
        into :dokument, :ktm, :wersjaref, :ilosc, :opk, :frommwsact,
            :x_partia, :x_serial_no,  :x_slownik, :x_havefake_vers
        do begin
            if (coalesce(x_havefake_vers,0 ) = 0) then begin
                numer = numer +1;
                insert into DOKUMPOZ(DOKUMENT, KTM, WERSJAREF, ILOSC, opk, FROMMWSACT,
                    x_partia, x_serial_no, x_slownik, fake, havefake,numer) values
                    (:dokument, :ktm, :wersjaref, :ilosc, 0, :frommwsact,
                    :x_partia, :x_serial_no, :x_slownik, 0, 0, :numer);
            end
            else  begin
                if (coalesce(last_havefake,-1) <> coalesce(x_havefake_vers,-1)) then begin
                    numer = numer +1;
                    select ktm from wersje where ref = :x_havefake_vers
                        into :ktmkomplet;
                    insert into DOKUMPOZ(DOKUMENT, KTM, WERSJAREF, ILOSC, opk, FROMMWSACT,
                            x_partia, x_serial_no, x_slownik, fake,  havefake, numer)
                        values(:dokument, :ktmkomplet, :x_havefake_vers, :ilosc, 0, :frommwsact,
                            :x_partia, :x_serial_no, :x_slownik, 0,1, :numer)
                        returning ref into :alttopoz;
                end
                last_havefake = x_havefake_vers;
    
                insert into DOKUMPOZ(DOKUMENT, KTM, WERSJAREF, ILOSC, opk, FROMMWSACT,
                        x_partia, x_serial_no, x_slownik, fake, havefake, numer, alttopoz )
                    values (:dokument, :ktm, :wersjaref, :ilosc, 0, :frommwsact,
                        :x_partia, :x_serial_no, :x_slownik, 1,0, :numer, :alttopoz);
            end
        end
   --end
  --XXX>>
  update mwsacts a set a.docid = :docid where a.status = 5 and a.docid = 0 and a.mwsord = :mwsord;
  update mwsords o set o.docid = :docid where o.ref = :mwsord;
--<<XXX

    update dokumnag set AKCEPT=1 where ref=:docid;

--XXX>>
end^
SET TERM ; ^
