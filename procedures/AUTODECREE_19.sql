--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_19(
      BKDOC integer)
   as
declare variable ACCOUNT KONTO_ID;
declare variable ACCOUNT9 KONTO_ID;
declare variable AMOUNT numeric(14,2);
declare variable SETTLEMENT varchar(20);
declare variable SETTLEMENT2 varchar(20);
declare variable DOCDATE timestamp;
declare variable PAYDAY timestamp;
declare variable DESCRIPT varchar(255);
declare variable CRNOTE smallint;
declare variable NAGFAK_REF integer;
declare variable FSOPERTYPE smallint;
declare variable SUMGROSSV numeric(14,2);
declare variable SUMNET numeric(14,2);
declare variable KONTOFK KONTO_ID;
declare variable TYPFAK varchar(3);
declare variable ZAGRANICZNY smallint;
declare variable TERMIN integer;
declare variable WALUTA varchar(3);
declare variable KURS numeric(14,3);
declare variable CURRNET numeric(14,2);
declare variable POWIAZANY varchar(1);
declare variable KD varchar(1);
declare variable OPT integer;
declare variable MAGAZYN varchar(3);
declare variable BANKACCOUNT integer;
declare variable GRUPA varchar(20);
declare variable GRUPA8 varchar(20);
declare variable KONTODOT KONTO_ID;
declare variable VATGR varchar(5);
declare variable USLUGA integer;
declare variable ZALICZKOWY integer;
declare variable BRAKKONTA smallint;
declare variable EKSPORT smallint;
declare variable DECREE integer;
declare variable COMPANY integer;
declare variable WINIENZL numeric(14,2);
declare variable MAZL numeric(14,2);
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable CURRENCY varchar(3);
declare variable REF_DT integer;
declare variable REF_CT integer;
declare variable SALDO numeric(14,2);
begin
  delete from decrees d where d.bkdoc = :bkdoc;
  select b.transdate, b.company from bkdocs b where b.ref = :bkdoc into :docdate, :company;
  saldo = 0;
  for
     select R.kontofk, R.symbfak, R.winienzl, R.mazl, r.slodef, r.slopoz,
         r.waluta
       from rozrach R
       where R.walutowy = 1 and R.winienzl - R.mazl <> 0
         and coalesce(r.winien,0) = coalesce(r.ma,0)
         and r.datazamk <= :docdate
         and r.company = :company
       into :account, :settlement, :winienzl, :mazl, :slodef, :slopoz,
         :currency
   do begin
     if(not exists(
       select first 1 1
         from rozrachp rp
         where rp.kontofk = :account
           and rp.symbfak = :settlement
           and rp.company = :company
           and rp.dtratediff is null
           and rp.ctratediff is null
           and (coalesce(rp.winien,0) <> 0 or coalesce(rp.ma,0) <> 0)
           and not exists(select first 1 1 from rozrachp rpp where 
rpp.kontofk = rp.kontofk
             and rpp.symbfak = rp.symbfak and (rpp.dtratediff = rp.ref 
or rpp.ctratediff = rp.ref)))
         or exists (select count(distinct rr.kurs) from rozrachp rr
           where rr.symbfak = :settlement and rr.kontofk = :account
             and rr.company = :company
           having count(distinct rr.kurs) = 1)
     )then
     begin
       select first 1 p.ref
         from rozrachp p
         where p.kontofk = :account
           and p.symbfak = :settlement
           and p.company = :company
           and p.dtratediff is not null
         into :ref_dt;

       ref_ct = ref_dt;

       amount = :mazl - :winienzl;


       descript = 'Rozliczenie zaokrągleń różnic kursowych';
       saldo = saldo + amount;
       if (:amount > 0) then
       begin
         execute procedure insert_decree_currsettlement(bkdoc,
           account, 0, amount, null, null, currency, descript, 
settlement, null, 9, null, null, null, null, null, null, ref_dt, ref_ct, 
0,0);
       end else begin
         amount = -1 * amount;
         execute procedure insert_decree_currsettlement(bkdoc, account, 
1, amount, null, null, currency, descript, settlement, null, 9, null, 
null, null, null, null, null, ref_dt, ref_ct, 0,0);
       end
     end
   end
   if(saldo > 0)then 
     execute procedure insert_decree(bkdoc, '750-04', 1, saldo, descript,0,1);
   else
     execute procedure insert_decree(bkdoc, '751-02',0 , -saldo, descript,0,1);
end^
SET TERM ; ^
