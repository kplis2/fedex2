--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GETDATE_AFTERWORKDAYS(
      DATEAFTERXDAYS INTEGER_ID,
      FROMDATE TIMESTAMP_ID = current_date)
  returns (
      WDATE TIMESTAMP_ID)
   as
declare variable SQL STRING1024;
begin
  SQL ='select first 1 skip '||dateafterxdays||' cdate
        from ecaldays
        where daykind = 1
          and cdate >= '''||FROMDATE||'''';
  execute statement sql into :wdate;
  if(wdate is null) then
    exception universal'Brak dni kalendarza ! Nie możliwe obliczenie dni roboczych!';
  suspend;

end^
SET TERM ; ^
