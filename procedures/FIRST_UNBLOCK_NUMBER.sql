--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FIRST_UNBLOCK_NUMBER(
      NUMERATOR varchar(40) CHARACTER SET UTF8                           ,
      REJESTR varchar(20) CHARACTER SET UTF8                           ,
      DOKUMENT varchar(20) CHARACTER SET UTF8                           ,
      DATA date)
  returns (
      NUMREF integer,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable NUMNUMBER integer;
declare variable isfcompany smallint;
declare variable current_company integer;
begin
  numref = null;
  msg = '';
  numnumber = null;
  select g.company from numbergen g where g.nazwa = :numerator into :isfcompany;
  if (isfcompany is null) then isfcompany = 0;
  if (isfcompany = 1) then
    execute procedure get_global_param('CURRENTCOMPANY')
      returning_values :current_company;
    select first 1 n.ref, n.numnumber
      from numberblock n
      where n.numerator = :NUMERATOR
        and n.data = :data
        and n.numrejdok = :rejestr
        and n.numtypdok = :DOKUMENT
        and n.doknagfak is null
        and (:isfcompany = 0 or n.company = :current_company)
    order by n.numnumber
      into :numref, :numnumber;

  if(:numref is not null) then
  begin
    msg = 'Czy użyć wolnego numeru: '||:numnumber||'?';
  end

  if (numref is null) then
  begin
    numref = 0;
  end

  suspend;
end^
SET TERM ; ^
