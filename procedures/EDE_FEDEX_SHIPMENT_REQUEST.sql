--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_SHIPMENT_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable shippingdoc integer;
declare variable rootparent integer;
declare variable grandparent integer;
declare variable subparent integer;
declare variable secondsubparent integer;
declare variable shippingcomments varchar(200);
declare variable deliverytype integer;
declare variable shipper spedytor_id;
declare variable senderlicencenumber varchar(255);
declare variable sendernumber varchar(255);
declare variable senderusername varchar(255);
declare variable senderpassword varchar(255);
declare variable shippfromplace varchar(20);
declare variable shipperfname varchar(60);
declare variable shippersname varchar(60);
declare variable shippercompanyname varchar(255);
declare variable shipperphone varchar(255);
declare variable shipperemail varchar(255);
declare variable recipientref integer;
declare variable recipientname varchar(255);
declare variable recipientnametmp varchar(255);
declare variable recipientiscompany smallint;
declare variable recipienttaxid nip;
declare variable recipientfname varchar(60);
declare variable recipientfnametmp varchar(60);
declare variable recipientsname varchar(60);
declare variable recipientsnametmp varchar(60);
declare variable recipientcity varchar(255);
declare variable recipientcitytmp varchar(255);
declare variable recipientpostcode postcode;
declare variable recipientpostcodetmp postcode;
declare variable recipientcountry country_id;
declare variable recipientcountrytmp country_id;
declare variable recipientstreet street_id;
declare variable recipientstreettmp street_id;
declare variable recipienthouseno nrdomu_id;
declare variable recipienthousenotmp nrdomu_id;
declare variable recipientlocalno nrdomu_id;
declare variable recipientlocalnotmp nrdomu_id;
declare variable recipientphone phones_id;
declare variable recipientemail emailwzor_id;
declare variable codamount ceny;
declare variable insuranceamount ceny;
declare variable packageref integer;
declare variable packagetype varchar(10);
declare variable packageweight waga_id;
declare variable packagelength waga_id;
declare variable packagewidth waga_id;
declare variable packageheight waga_id;
declare variable accesscode string80;
declare variable filepath string255;
declare variable courierno string30;
declare variable dropshippingsender integer;
declare variable fedexno string20;
declare variable x_bankacc string1024; -- [DG] XXX BOWI nr konta bankowego do pobrania
declare variable subiektsymb string255;
declare variable docgroup dokumnag_id;
begin
  select l.ref, substr(l.uwagisped,1,200), l.sposdost, l.fromplace, l.odbiorcaid,
      trim(l.kontrahent), 0, '',
      trim(l.imie), trim(l.nazwisko),
      trim(l.miasto), trim(l.kodp), trim(l.krajid),
      trim(l.adres), trim(l.nrdomu), trim(l.nrlokalu),
      l.pobranie, 0, /* [DG] XXX DROPS l.dropshippingsender */
      l.ubezpieczenie
    from listywysd l
    where l.ref = :oref
  into :shippingdoc, :shippingcomments, :deliverytype, :shippfromplace, :recipientref,
    :recipientname, :recipientiscompany, :recipienttaxid,
    :recipientfname, :recipientsname,
    :recipientcity, :recipientpostcode, :recipientcountry,
    :recipientstreet, :recipienthouseno, :recipientlocalno,
    :codamount, :dropshippingsender,
    :insuranceamount;

  --sprawdzanie czy istieje dokument spedycyjny
  if(shippingdoc is null) then
    exception ede_fedex 'Brak listy spedycyjnej.';

  --XXX JO: Pobranie grupy spedycyjnej dla danej wysylki
  select n.grupasped
    from listywysdpoz p
      left join dokumnag n on (p.dokref = n.ref and p.doktyp = 'M')
    where p.dokument = :shippingdoc
    group by n.grupasped
  into :docgroup;
  --XXX JO: Koniec

/* [DG] XXX DROPS  if (coalesce(:dropshippingsender,0) > 0) then
    select d.fedexnrklienta
      from dostawcy d where ref = :dropshippingsender
    into :fedexno; */

  val = null;
  parent = null;
  params = null;
  id = 0;
  name = 'zapiszList';
  suspend;

  rootparent = id;

  --dane autoryzacyjne
  select k.klucz, k.numerklienta, k.sciezkapliku, k.kurier
    from spedytwys w
      left join spedytkonta k on (w.spedytor = k.spedytor)
    where w.sposdost = :deliverytype
  into :accesscode, :sendernumber, :filepath, :courierno;

  val = :accesscode;
  name = 'kodDostepu';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = :filepath;
  name = 'sciezkaPliku';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = 'PRODUCTION'; --jesli chcesz testowac zmien na TEST
  name = 'serviceUrlType';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = null;
  name = 'list';
  params = 'xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"';
  parent = rootparent;
  id = id + 1;
  suspend;

  grandparent = id;

  --nr EXT, numer przesylki, w tym przypadki REF, pole to drukowane jest na wydruku
  val = :shippingdoc;
  name = 'nrExt';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  --forma platnosci my - fedex, domyslnie ustawione na P (przelew bankowy), inna mozliwosc G (gotowka)
  val = 'G';
  name = 'formaPlatnosci';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  --rodzaj przesylki, domyslna i jedyna opcja K
  val = 'K';
  name = 'rodzajPrzesylki';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  --wskazanie kto placi za przesylke, domyslnie ustawione na wysylajacego
  --inne mozliwe opcje nalezy sprawdzic w dokumentacji, gdyz konieczne jest uzupelnienie innych dodatkowo wymaganych pol
  val = iif(coalesce(:dropshippingsender,0) = 0, '1', '3');
  name = 'placi';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

/*$$IBEC$$   select list(distinct coalesce(n.x_sb_symbol, ''),',')
    from listywysdpoz p
      left join dokumnag n on (p.dokref = n.ref and p.doktyp = 'M')
    where p.dokument = :shippingdoc
  into :subiektsymb; $$IBEC$$*/ --zbdna integracja z subiektem

  --uwagi spedycyjne
  val = substr(:subiektsymb||', '||:shippingcomments, 1, 199);
  name = 'uwagi';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  --MPK wykorzystywane przy fakturze zbiorczej za uslugi od spedytora
  --pole czysto informacyjne
--  val = 'mpk';
--  name = 'mpk';
--  params = null;
--  parent = grandparent;
--  id = id + 1;
--  suspend;

/* wezel nadawcy */
  val = null;
  name = 'nadawca';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  subparent = :id;

  --numer nadawcy
  val = iif(coalesce(:dropshippingsender,0) > 0, :fedexno, :sendernumber);
  name = 'numer';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  select o.imie, o.nazwisko, d.opis, d.dtelefon, d.demail
    from defmagaz d
      left join operator o on (d.dosoba = o.ref)
    where d.symbol = :shippfromplace
  into :shipperfname, :shippersname, :shippercompanyname, :shipperphone, :shipperemail;

/* uzupelniane tylko wtedy, gdy osoba wysylajaca jest inna niz w systemie FEDEX
  --imie nadawcy
  val = substr(:shipperfname1,50);
  name = 'imie';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --nazwisko nadawcy
  val = :shippersname;
  name = 'imie';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --nazwa firmy nadawcy
  val = substr(:shippercompanyname1,150);
  name = 'nazwa';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --telefon nadawcy
  val = substr(:shipperphone,1,20);
  name = 'telKontakt';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --email nadawcy
  val = substr(:shipperemail,1,35);
  name = 'emailKontakt';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;
*/
/* koniec wezla nadawcy */

/* wezel odbiorcy */
  val = null;
  name = 'odbiorca';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  subparent = :id;

  if (:recipientref is not null) then
  begin
    select o.nazwa, k.firma, k.nip,
        o.imie, o.nazwisko,
        o.dmiasto, o.dkodp, o.krajid,
        o.dulica, o.dnrdomu, o.dnrlokalu,
        o.dtelefon, o.email
      from odbiorcy o
        join klienci k on (o.klient = k.ref)
      where o.ref = :recipientref
    into
      :recipientnametmp, :recipientiscompany, :recipienttaxid,
      :recipientfnametmp, :recipientsnametmp,
      :recipientcitytmp, :recipientpostcodetmp, :recipientcountrytmp,
      :recipientstreettmp, :recipienthousenotmp, :recipientlocalnotmp,
      :recipientphone, :recipientemail;
  end

  --nr ref w systemie odbiorcy
  if (:recipientref is not null) then
  begin
    val = :recipienttaxid; --XXX JO: Byl REF a ma byc NIP
    name = 'nrExt';
    params = null;
    parent = subparent;
    id = id + 1;
    suspend;
  end

  --nazwa odbiorcy
  if (coalesce(:recipientname,'') = '') then
    recipientname = :recipientnametmp;
  if (coalesce(:recipientname,'') = '') then
    exception ede_fedex 'Brak listy spedycyjnej.';
  val = :recipientname;
  name = 'nazwa';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --sprawczenie czy odbiorca jest firma
  val = :recipientiscompany;
  name = 'czyFirma';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --nip odbiorcy
  val = :recipienttaxid;
  name = 'nip';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --imie odbiorcy
  if (coalesce(:recipientfname,'') = '') then
    recipientname = :recipientfnametmp;
  if (coalesce(:recipientfname,'') = '') then
    recipientfname = '';
--    exception ede_ups_listywysd_brak 'Brak imienia odbiorcy';
  val = substr(:recipientfname,1,50);
  name = 'imie';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --nazwisko odbiorcy
  if (coalesce(:recipientsname,'') = '') then
    recipientname = :recipientsnametmp;
  if (coalesce(:recipientsname,'') = '') then
    recipientsname = '';
--    exception ede_ups_listywysd_brak 'Brak nazwiska odbiorcy';
  val = :recipientsname;
  name = 'nazwisko';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --miasto odbiorcy
  if (coalesce(:recipientcity,'') = '') then
    recipientcity = :recipientcitytmp;
  if (coalesce(:recipientcity,'') = '') then
    exception ede_fedex 'Brak miasta odbiorcy';
  val = substr(:recipientcity,1,70);
  name = 'miasto';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --kod pocztowy odbiorcy
  if (coalesce(:recipientpostcode,'') = '') then
    recipientpostcode = :recipientpostcodetmp;
  if (coalesce(:recipientpostcode,'') = '') then
    exception ede_fedex 'Brak kodu pocztowego odbiorcy';
  val = :recipientpostcode;
  name = 'kod';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --kod kraju odbiorcy
  if (coalesce(:recipientcountry,'') = '') then
    recipientcountry = :recipientcountrytmp;
  if (coalesce(:recipientcountry,'') = '') then
    recipientcountry = 'PL';
--    exception ede_ups_listywysd_brak 'Brak kraju odbiorcy';
  val = substr(:recipientcountry,1,2);
  name = 'kodKraju';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --ulica odbiorcy
  if (coalesce(:recipientstreet,'') = '') then
    recipientstreet = :recipientstreettmp;
  if (coalesce(:recipientstreet,'') = '') then
    exception ede_fedex 'Brak ulicy odbiorcy';
  val = substr(:recipientstreet,1,70);
  name = 'ulica';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --nr domu odbiorcy
  if (coalesce(:recipienthouseno,'') = '') then
    recipienthouseno = :recipienthousenotmp;
  if (coalesce(:recipienthouseno,'') = '') then
    recipienthouseno = '.';
  val = substr(:recipienthouseno,1,20);
  name = 'nrDom';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --nr lokalu odbiorcy
  if (coalesce(:recipientlocalno,'') = '') then
    recipientlocalno = :recipientlocalnotmp;
  if (coalesce(:recipientlocalno,'') = '') then
    recipientlocalno = '.';
  val = substr(:recipienthouseno,1,10);
  name = 'nrLokal';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --nr telefonu odbiorcy
  val = substr(:recipientphone,1,20);
  name = 'telKontakt';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --nr telefonu odbiorcy
  val = substr(:recipientemail,1,35);
  name = 'telKontakt';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;
/* koniec wezla odbiorcy */

/* wezel platnika */
--wezel ten uzupelniany jest wtedy, gdy platnikiem jest jakas firma zewnetrzna
  if (coalesce(:dropshippingsender,0) > 0) then
  begin
    val = null;
    name = 'platnik';
    params = null;
    parent = grandparent;
    id = id + 1;
    suspend;

    subparent = id;

    val = :sendernumber;
    name = 'numer';
    params = null;
    parent = grandparent;
    id = id + 1;
    suspend;
  end
/* koniec wezla platnika */

/* wezel potwierdzenia nadania */
  val = null;
  name = 'potwierdzenieNadania';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  subparent = :id;

  --nazwa nadawcy, tutaj pobrane dane osoby odpowiedzialnej za magazyn, z ktorego jest wysylka
  val = substr(:shipperfname||' '||:shippersname,1,50);
  name = 'podpisNadawcy';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --numer kuriera, jest staly
  val = :courierno;
  name = 'numerKuriera';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  --znacznik czasowy nadania przesylki
  val = datetostr(current_timestamp,'%Y-%m-%d %H:%M');
  name = 'dataNadania';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;
/* koniec wezla potwierdzenia nadania */

/* wezel uslug */
  val = null;
  name = 'uslugi';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  subparent = :id;

  /* wezel pobrania */
  -- [DG] XXX BOWI - ponizsza linia zakomentowana i zamieniona dla BOWI
  if (coalesce(:codamount,0) > 0) then
  --if (coalesce(:deliverytype,0) = 10) then -- [DG] XXX BOWI - pobranie dla sposdost = 10 (Fedex pobranie) [PM] WYCOFANIE X-ow
  begin
    val = null;
    name = 'pobranie';
    params = null;
    parent = subparent;
    id = id + 1;
    suspend;
  
    secondsubparent = :id;
  
    --forma pobrania, obslugiwany jedynie przelew bankowy
    val = 'B';
    name = 'formaPobrania';
    params = null;
    parent = secondsubparent;
    id = id + 1;
    suspend;
  
    -- [DG] XXX BOWI - obliczenie kwoty pobrania jako sumy pol DOKUMNAG.WARTSBRUTTO [PM] WYCOFANIE X-ow
    /*codamount = 0;
    with doclist as (
      select distinct lp.dokref doc
        from listywysdpoz lp
        where lp.dokument = :oref
          and lp.doktyp = 'M'
    )
    select sum(dn.wartsbrutto)
      from doclist dl
        left join dokumnag dn on (dl.doc = dn.ref)
        into :codamount;*/
    -- [DG] XXX end

    --kwota pobrania
    val = :codamount;
    name = 'kwotaPobrania';
    params = null;
    parent = secondsubparent;
    id = id + 1;
    suspend;
  
    --nr konta, na ktore nalezy przelac kwote pobrania
    execute procedure getconfig('INFORACH') -- [DG] XXX BOWI pobieranie nr. konta z konfiga
        returning_values :x_bankacc;
    val = replace(x_bankacc,' ',''); -- usuwam spacje z numeru, fedex przyjmuje bez spacji
    if (coalesce(trim(val),'') = '') then -- jezeli nie udalo sie pobrac numeru rachunku bankowego to exception
      exception ede_fedex 'Brak numeru rachunku bankowego dla pobrania! (INFORACH)';
    name = 'nrKonta';
    params = null;
    parent = secondsubparent;
    id = id + 1;
    suspend;
  end
  /* koniec wezla pobrania*/

  /* wezel ubezpieczenia */
  --XXX JO: Zawsze min ubezpieczenie 5000 zl
  if (coalesce(:insuranceamount,0) = 0) then
    insuranceamount = 5000;

  if (coalesce(:insuranceamount,0) > 0 or coalesce(:codamount,0) > 0) then
  begin
    val = null;
    name = 'ubezpieczenie';
    params = null;
    parent = subparent;
    id = id + 1;
    suspend;
  
    secondsubparent = :id;
  
    --ubezpieczenie, nie moze byc nizsze niz kwota pobrania
    --if (coalesce(:insuranceamount,0) < coalesce(:codamount,0)) then --XXX JO: Na razie stale ubezpieczenie
    --  insuranceamount = :codamount;

    val = :insuranceamount;
    name = 'kwotaUbezpieczenia';
    params = null;
    parent = secondsubparent;
    id = id + 1;
    suspend;

    --opis zawartosci, do dopracowania
    val = 'Artykuły elektroniczne'; --todo
    name = 'opisZawartosci';
    params = null;
    parent = secondsubparent;
    id = id + 1;
    suspend;
  end
  /* koniec wezla ubezpieczenia */

  /* uslugi dodatkowe w ramach wezla uslugi */
  --todo jesli beda potrzebne
  /* koniec uslg dodatkowych w ramach wezla uslugi */
/* koniec wezla ulug */

/* wezel opakowan */
  val = null;
  name = 'paczki';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  subparent = :id;

  /* wezly paczek, dla kazdej z paczki osobno */
  for
    select o.ref, t.symbol, o.waga, o.dlugosc, o.szerokosc, o.wysokosc
      from listywysdroz_opk o
        left join typyopk t on (o.typopk = t.ref)
      where o.listwysd = :shippingdoc
        and o.rodzic is null
    into :packageref, :packagetype, :packageweight, :packagelength, :packagewidth, :packageheight
  do begin
    val = null;
    name = 'paczka';
    params = null;
    parent = subparent;
    id = id + 1;
    suspend;
  
    secondsubparent = :id;

    -- [DG] XXX BOWI - jezeli typ opakowania to gabaryt, to musimy dodac dodatkowy znacznik
    if (:packagetype = 'GAB') then
    begin
      val = '1';
      name = 'ksztalt';
      params = null;
      parent = secondsubparent;
      id = id + 1;
      suspend;
    end
    -- [DG] XXX end

    --typ opakowania
    if (:packagetype = 'KOP') then packagetype = 'KP'; --koperta
    else if (:packagetype = 'KAR') then packagetype = 'PC'; --paczka
    else if (:packagetype = 'PAL') then packagetype = 'PL'; --paleta
    else if (:packagetype = 'GAB') then packagetype = 'PC'; -- [DG] XXX BOWI - typ opakowania GAB to gabaryt, paczka ale o niestandardowych wymiarach
    else exception ede_fedex 'Brak wybranego typu opakowania';
    val = :packagetype;
    name = 'typ';
    params = null;
    parent = secondsubparent;
    id = id + 1;
    suspend;

    --waga paczki
    val = strreplace(cast(cast(:packageweight as numeric(15,3)) as varchar(20)),'.',',');
    name = 'waga';
    params = null;
    parent = secondsubparent;
    id = id + 1;
    suspend;

    --dlugosc paczki
    if (coalesce(:packagelength,0) > 0) then
    begin
      val = round(:packagelength); -- [DG] XXX BOWI - zaokraglanie wymiaru do pelnych liczb, fedex nie przyjmuje miejsc po przecinku
      name = 'gab1';
      params = null;
      parent = secondsubparent;
      id = id + 1;
      suspend;
    end

    --szerokosc paczki
    if (coalesce(:packagewidth,0) > 0) then
    begin
      val = round(:packagewidth); -- [DG] XXX BOWI - zaokraglanie wymiaru do pelnych liczb, fedex nie przyjmuje miejsc po przecinku
      name = 'gab2';
      params = null;
      parent = secondsubparent;
      id = id + 1;
      suspend;
    end

    --wysokosc paczki
    if (coalesce(:packageheight,0) > 0) then
    begin
      val = round(:packageheight); -- [DG] XXX BOWI - zaokraglanie wymiaru do pelnych liczb, fedex nie przyjmuje miejsc po przecinku
      name = 'gab3';
      params = null;
      parent = secondsubparent;
      id = id + 1;
      suspend;
    end

    --ref paczki
    val = :packageref;
    name = 'nrExtPp';
    params = null;
    parent = secondsubparent;
    id = id + 1;
    suspend;
  end
  /*koniec osobnych wezlow paczek */
/* koniec wezla opakowan */
end^
SET TERM ; ^
