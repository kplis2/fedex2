--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CENNIK_AKTUKOMPLETY(
      KTM varchar(40) CHARACTER SET UTF8                           ,
      CENNIK integer)
  returns (
      STATUS integer)
   as
declare variable wersjaref integer;
declare variable kplnagref integer;
declare variable defcennikref integer;
declare variable cenanet numeric(14,2);
declare variable cenazero integer;
declare variable nowacena integer;
declare variable stat integer;
declare variable wer integer;
declare variable kplnagnazwa varchar(80);
begin
  for
    select WERSJE.REF, KPLNAG.REF, KPLNAG.NAZWA
    from WERSJE left join KPLNAG on (KPLNAG.WERSJAREF=WERSJE.REF and KPLNAG.AKT=1 and KPLNAG.GLOWNA=1)
    where (WERSJE.USLUGA=2) and (KPLNAG.REF is not null) and ((WERSJE.KTM)=:KTM or (:KTM is null) or (:KTM=''))
    into :wersjaref,:kplnagref,:kplnagnazwa
  do begin
    if((:cennik is NULL) or (:cennik=0)) then begin
      for
          select REF
          from DEFCENNIK
          where AKT=1
          into :defcennikref
      do begin
          nowacena = 0;
          select count(*) from KPLPOZ
          left join WERSJE on (WERSJE.KTM=KPLPOZ.KTM and WERSJE.NAZWA=:kplnagnazwa)
          join CENNIK on (CENNIK.CENNIK=:defcennikref and CENNIK.WERSJAREF = WERSJE.REF)
          where (KPLPOZ.NAGKPL=:kplnagref) and (cennik.datazmiany>=current_date)
          into :nowacena;
          if(:nowacena>0) then begin  
            select count(*) from KPLPOZ
            left join WERSJE on (WERSJE.KTM=KPLPOZ.KTM and WERSJE.NAZWA=:kplnagnazwa)
            left join CENNIK on (CENNIK.CENNIK=:defcennikref and CENNIK.WERSJAREF = WERSJE.REF)
            where (KPLPOZ.NAGKPL=:kplnagref) and ((cennik.cenanet=0) or (cennik.cenanet is null))
            into :cenazero;
            if(:cenazero=0) then begin
              select sum(KPLPOZ.ILOSC*CENNIK.CENANET) from KPLPOZ
              join WERSJE on (WERSJE.KTM=KPLPOZ.KTM and WERSJE.NAZWA=:kplnagnazwa)
              join CENNIK on (CENNIK.CENNIK=:defcennikref and CENNIK.WERSJAREF = WERSJE.REF)
              where (KPLPOZ.NAGKPL=:kplnagref) and (cennik.cenanet is not null)
              into :cenanet;
              if(:cenanet is NULL) then cenanet=0;
              execute procedure CENNIK_USTAW(:defcennikref,:wersjaref,NULL,:cenanet,NULL,NULL) returning_values :stat,:wer;
            end else begin
              execute procedure CENNIK_USTAW(:defcennikref,:wersjaref,NULL,0,NULL,NULL) returning_values :stat,:wer;
            end
          end
      end
    end else begin
      select sum(KPLPOZ.ILOSC*CENNIK.CENANET) from KPLPOZ
      join WERSJE on (WERSJE.KTM=KPLPOZ.KTM and WERSJE.NAZWA=:kplnagnazwa)
      join CENNIK on (CENNIK.CENNIK=:cennik and CENNIK.WERSJAREF = WERSJE.REF)
      where KPLPOZ.NAGKPL=:kplnagref
      into :cenanet;
      if(:cenanet is NULL) then cenanet=0;
      execute procedure CENNIK_USTAW(:cennik,:wersjaref,NULL,:cenanet,NULL,NULL) returning_values :stat,:wer;
    end
  end
  status = 1;
end^
SET TERM ; ^
