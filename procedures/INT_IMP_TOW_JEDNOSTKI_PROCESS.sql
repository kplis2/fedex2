--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_TOW_JEDNOSTKI_PROCESS(
      SESJAREF SESJE_ID,
      TABELA TABLE_ID,
      REF INTEGER_ID,
      BLOKUJZALEZNE SMALLINT_ID = 0,
      TYLKONIEPRZETWORZONE SMALLINT_ID = 0,
      AKTUALIZUJSESJE SMALLINT_ID = 1)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable stabela table_id;
declare variable tmptabela table_id;
declare variable sprocedura string255;
declare variable szrodlo zrodla_id;
declare variable skierunek smallint_id;
declare variable sesja sesje_id;
declare variable sql memo;
declare variable tmpsql memo;
declare variable tmpstatus smallint_id;
declare variable tmpmsg string255;
declare variable error_row smallint_id;
declare variable errormsg_row string255;
declare variable tmperror smallint_id;
declare variable errormsg_all string255;
declare variable statuspoz smallint_id;
--TOWAR
declare variable T_REF INTEGER_ID;
declare variable T_DEL INTEGER_ID;
--JEDNOSTKA
declare variable J_REF INTEGER_ID;
declare variable J_TOWARID STRING120;
declare variable J_JEDNOSTKA STRING60;
declare variable J_JEDNOSTKAID STRING120;
declare variable J_GLOWNA INTEGER_ID;
declare variable J_LICZNIK NUMERIC_14_4;
declare variable J_MIANOWNIK NUMERIC_14_4;
declare variable J_WAGA NUMERIC_14_4;
declare variable J_WYSOKOSC NUMERIC_14_4;
declare variable J_DLUGOSC NUMERIC_14_4;
declare variable J_SZEROKOSC NUMERIC_14_4;
declare variable J_HASH STRING255;
declare variable J_DEL INTEGER_ID;
declare variable J_REC STRING255;
declare variable J_SKADTABELA STRING40;
declare variable J_SKADREF INTEGER_ID;
--jednostka zmienne pomocnicze
declare variable J_TMP_PRZELICZ NUMERIC_14_4;
declare variable J_GLOWNASENTE SMALLINT_ID;
--zmienne ogolne
declare variable KTM KTM_ID;
declare variable WERSJAREF WERSJE_ID;
declare variable JEDN JEDN_MIARY;
declare variable TOWJEDN TOWJEDN;


--declare variable  jedn,
declare variable o_glowna smallint_id;
declare variable o_przelicz numeric_14_4;
declare variable o_const smallint_id;
declare variable o_s smallint_id;
declare variable o_z smallint_id;
declare variable o_m smallint_id;
declare variable o_c smallint_id;
declare variable o_waga waga_id;
declare variable o_wys waga_id;
declare variable o_dlug waga_id;
declare variable o_szer waga_id;
begin
  --zmiana pustych wartosci na null-e
  if (:sesjaref = 0) then
    sesjaref = null;
  if (trim(:tabela) = '') then
    tabela = null;
  if (:ref = 0) then
    ref = null;
  if (:blokujzalezne is null) then
    blokujzalezne = 0;
  if (:tylkonieprzetworzone is null) then
    tylkonieprzetworzone = 0;
  if (:aktualizujsesje is null) then
    aktualizujsesje =1;

  status = 1;
  msg = '';
  errormsg_all = '';

  --sprawdzenie czy wszystkie potrzebne informacje sa
  if (:sesjaref is null and :ref is null) then
  begin
    status = -1;
    msg = 'Za mało parametrow.';
    exit; --EXIT
  end

  if (:sesjaref is null) then
  begin
    if (:ref is null) then
    begin
      status = -1;
      msg = 'Jeśli nie podałes sesji to wypadałoby podać ref-a...';
      exit; --EXIT
    end
    if (:tabela is null) then
    begin
      status = -1;
      msg = 'Wypadałoby podać nazwę tabeli...';
      exit; --EXIT
    end

    sql = 'select sesja from '||:tabela||' where ref='||:ref;
    execute statement sql into :sesjaref;
  end
  else
    sesja = :sesjaref;

  if (:sesjaref is null) then
  begin
    status = -1;
    msg = 'Nie znaleziono numeru sesji.';
    exit; --EXIT
  end

  --pobranie parametrow sesji
  select s.tabela, s.procedura, s.zrodlo, s.kierunek
    from int_sesje s
    where ref = :sesja
    into :stabela, :sprocedura, :szrodlo, :skierunek;

  if (:tabela is not null) then --jesli podano tabele na wejsciu traktuje ja nadrzednie
    stabela = :tabela;

  for
    select ref, towarid, jednostka, jednostkaid, glowna, licznik, mianownik,
        waga, wysokosc, dlugosc, szerokosc,
        --"hash",
        del, rec, skadtabela, skadref, glowna_sente
      from INT_IMP_TOW_JEDNOSTKI j
      where sesja = :sesja and (ref = :ref or :ref is null)
    into :j_ref, :j_towarid, :j_jednostka, :j_jednostkaid, :j_glowna, :j_licznik, :j_mianownik,
        :j_waga, :j_wysokosc, :j_dlugosc, :j_szerokosc,
        --:j_hash,
        :j_del, :j_rec, :j_skadtabela, :j_skadref, :j_glownasente
  do begin
    error_row = 0;
    errormsg_row = '';
    tmperror = null;
    t_ref = null;
    ktm = null;
    wersjaref = null;
    jedn = null;
    towjedn = null;

    if (coalesce(:j_skadtabela,'') <> '' and coalesce(:j_skadref,0) > 0) then
    begin
      sql = 'select error from ' || :j_skadtabela || ' where ref = '|| :j_skadref;
      execute statement :sql into :tmperror;
    end

    if (:tmperror is null) then
      tmperror = 0;

    if (coalesce(trim(:j_jednostka),'') = '') then
    begin
      error_row = 1;
      errormsg_row = substring(errormsg_row || ' ' || 'Brak nazwy jednostki.' from 1 for 255);
    end

    if (coalesce(:j_del,0) = 1 and :error_row = 0) then --rekord do skasowania
    begin
      --select status, msg from INT_IMP_TOW_JEDNOSTKA_DEL(j_jednostka)
        --into :tmpstatus, :tmpmsg;
      tmpstatus = null;
    end
    else if (:error_row = 0 and coalesce(:t_del,0) = 0 and :tmperror = 0) then
    begin
      j_tmp_przelicz = coalesce(:j_licznik,1) / coalesce(:j_mianownik,1);

      if (:error_row = 0) then
      begin
        /*
        --jednostka miary start
        select miara from miara where miara = :j_jednostka into :jedn;
        if (:jedn is null) then
          select miara from miara where lower(miara) = lower(:j_jednostka) into :jedn;
        if (:jedn is null) then
          select miara from miara where miara = replace(:j_jednostka,'.','') into :jedn;
        if (:jedn is null) then
          select miara from miara where lower(miara) = lower(replace(:j_jednostka,'.','')) into :jedn;
        if (:jedn is null) then
          select miara from miara where trim(miara) = trim(:j_jednostka) into :jedn;
        if (:jedn is null) then
          select miara from miara where trim(lower(miara)) = trim(lower(:j_jednostka)) into :jedn;
        if (:jedn is null) then
          select miara from miara where id_zewn = :j_jednostka into :jedn;
        if (:jedn is null) then
          select miara from miara where miara = substring(:j_jednostka from  1 for 5) into :jedn;
        if (:jedn is null) then
          select miara from miara where lower(miara) = lower(trim(substring(:j_jednostka from  1 for 5))) into :jedn;
        if (:jedn is null) then
          select miara from miara where lower(miara) = lower(replace(trim(substring(:j_jednostka from  1 for 5)),' ', '')) into :jedn;
        if (:jedn is null) then
          select miara from miara where lower(miara) = lower(replace(replace(trim(substring(:j_jednostka from  1 for 5)),' ', ''), '.', '')) into :jedn;

        */
        --1
          select m.miara
            from int_slownik s
              join miara m on (m.miara = s.okey)
            where s.zrodlo = :szrodlo
              and s.wartoscid is not distinct from :j_jednostkaid
              --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
              and s.otable = 'MIARA'
            into :jedn;
           /*
          if (:jedn is null) then
            --2
            select m.miara from int_slownik s
            join miara m on m.miara = s.okey
            where s.zrodlo = :szrodlo
              and lower(s.wartosc) is not distinct from lower(:j_jednostka)
              --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
              and s.otable = 'MIARA'
            into :jedn;
          if (:jedn is null) then
            --3
            select m.miara from int_slownik s
            join miara m on m.miara = s.okey
            where s.zrodlo = :szrodlo
              and lower(trim('.' from s.wartosc)) is not distinct from lower(trim('.' from :j_jednostka))
              --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
              and s.otable = 'MIARA'
            into :jedn;
          if (:jedn is null) then
            --4
            select m.miara from int_slownik s
            join miara m on m.miara = s.okey
            where s.zrodlo = :szrodlo
              and s.wartosc is not distinct from lower(replace(:j_jednostka,'.',''))
              --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
              and s.otable = 'MIARA'
            into :jedn;
          if (:jedn is null) then
            --5
            select m.miara from int_slownik s
              join miara m on m.miara = s.okey
              where s.zrodlo = :szrodlo
                and lower(s.wartosc) is not distinct from lower(replace(:j_jednostka,'.',''))
                --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
                and s.otable = 'MIARA'
              into :jedn;
          if (:jedn is null) then
            --6
            select m.miara from int_slownik s
              join miara m on m.miara = s.okey
              where s.zrodlo = :szrodlo
                and trim(s.wartosc) is not distinct from trim(:j_jednostka)
                --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
                and s.otable = 'MIARA'
              into :jedn;
          if (:jedn is null) then
            --7
            select m.miara from int_slownik s
              join miara m on m.miara = s.okey
              where s.zrodlo = :szrodlo
                and trim(lower(s.wartosc)) is not distinct from trim(lower(:j_jednostka))
                --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
                and s.otable = 'MIARA'
              into :jedn;
          if (:jedn is null) then
            --8
            select m.miara from int_slownik s
              join miara m on m.miara = s.okey
              where s.zrodlo = :szrodlo
                and lower(s.wartosc) is not distinct from substring(:j_jednostka from  1 for 5)
                --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
                and s.otable = 'MIARA'
              into :jedn;
          if (:jedn is null) then
            --9
            select m.miara from int_slownik s
              join miara m on m.miara = s.okey
              where s.zrodlo = :szrodlo
                and lower(s.wartosc) is not distinct from lower(replace(trim(substring(:j_jednostka from  1 for 5)),' ', ''))
                --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
                and s.otable = 'MIARA'
              into :jedn;
          if (:jedn is null) then
            --10
            select m.miara from int_slownik s
              join miara m on m.miara = s.okey
              where s.zrodlo = :szrodlo
                and lower(s.wartosc) is not distinct from lower(replace(replace(trim(substring(:j_jednostka from  1 for 5)),' ', ''), '.', ''))
                --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
                and s.otable = 'MIARA'
              into :jedn;
             */

          if (coalesce(trim(:jedn),'') = '') then
          begin
            error_row = 1;
            errormsg_row = substring(:errormsg_row || ' ' || 'Brak jednostki miary.' from 1 for 255);
          end

        /*
        if (:jedn is null) then
          insert into miara (miara, opis, dokladnosc, id_zewn)
            values(substr(lower(trim(replace(:j_jednostka,' ',''))),1,5),
              :j_jednostka, 2, :j_jednostka)
            returning miara into :jedn; --JEDN
        */
        --jednostka miary koniec

        select first 1  w.ref, w.ktm from towary t left join wersje w on w.ktm = t.ktm
          where t.int_id = :j_towarid and coalesce(t.int_zrodlo,0) = :szrodlo
          into :wersjaref, :ktm; --WERSJAREF KTM
        if (:wersjaref is null) then
          select first 1 w.ref, w.ktm from towary t join wersje w on w.ktm = t.ktm
          where t.int_id = :j_towarid
          into :wersjaref, :ktm; --WERSJAREF KTM
        if (:wersjaref is null) then
          select first 1 w.ref, w.ktm from towary t join wersje w on w.ktm = t.ktm
            where t.ktm = :j_towarid and coalesce(t.int_zrodlo,0) = :szrodlo
            into :wersjaref, :ktm; --WERSJAREF KTM
        if (:wersjaref is null) then
          begin
            error_row = 1;
            errormsg_row = substring(errormsg_row || ' ' || 'Nie znaleziono towaru.' from 1 for 255);
          end

        if (:error_row = 0) then
          begin
            --nullowanie, zerowanie i blankowanie - jednostka
            if (:j_glowna is null) then
              j_glowna = 0;
            if (:j_licznik is null) then
              j_licznik = 1;
            if (:j_mianownik is null) then
              j_mianownik = 1;
            if (:j_waga is null) then
              j_waga = 0;
            if (:j_wysokosc is null) then
              j_wysokosc = 0;
            if (:j_dlugosc is null) then
              j_dlugosc = 0;
            if (:j_szerokosc is null) then
              j_szerokosc = 0;

            if (:j_glownasente is not null) then --przeciazenie !!!!!!
              j_glowna = :j_glownasente;

            --XXX JO: wymiary sa w milimetrach, zmiana na cm
            j_wysokosc = :j_wysokosc / 10.0000;
            j_dlugosc = :j_dlugosc / 10.0000;
            j_szerokosc = :j_szerokosc / 10.0000;
            j_waga = :j_waga / 1000.0000; --zmiana na kg
            --XXX JO: Koniec


          select ref, glowna, przelicz, const, s, z, m, c, waga, wys , dlug, szer
            from towjedn
            where ktm = :ktm
              and jedn = :jedn
          into :towjedn, :o_glowna, :o_przelicz, :o_const, :o_s, :o_z, :o_m, :o_c, :o_waga, :o_wys , :o_dlug, :o_szer;

          if (towjedn is not null) then begin
            if (o_glowna is distinct from j_glowna or
                o_przelicz is distinct from j_tmp_przelicz or
                --o_const,
                --o_s,
                --o_z,
                --o_m,
                --o_c,
                o_waga is distinct from j_waga or
                o_wys is distinct from j_wysokosc or
                o_dlug is distinct from j_dlugosc or
                o_szer is distinct from j_szerokosc) then begin
                  update towjedn set glowna = :j_glowna, przelicz = :j_tmp_przelicz, const = 1,
                      s = 2, z = 2, m = 2, c = 2,
                      waga = :j_waga,
                      wys = :j_wysokosc, dlug = :j_dlugosc, szer = :j_szerokosc,
                      int_zrodlo = :szrodlo, int_id = :j_jednostka,
                      int_dataostprzetw = current_timestamp(0), int_sesjaostprzetw = :sesja
                    where ref = :towjedn;

            end
          end else begin
            insert into towjedn (ktm, jedn, glowna, przelicz, const,
                s, z, m, c,
                waga,
                wys, dlug, szer,
                int_zrodlo, int_id,
                int_dataostprzetw, int_sesjaostprzetw)
              values(:ktm, :jedn, :j_glowna, :j_tmp_przelicz, 1,
                1, 1, 1, 1,
                :j_waga,
                :j_wysokosc, :j_dlugosc, :j_szerokosc,
                :szrodlo, :j_jednostka,
                current_timestamp(0), :sesja)

              returning ref into :towjedn; --TOWJEDN

          end

           /*
            update or insert into towjedn (ktm, jedn, glowna, przelicz, const,
                s, z, m, c,
                waga, wys, dlug, szer, int_zrodlo, int_id,
                int_dataostprzetw, int_sesjaostprzetw)
              values(:ktm, :jedn, :j_glowna, :j_tmp_przelicz, 1,
                1, 1, 1, 1,
                :j_waga, :j_wysokosc, :j_dlugosc, :j_szerokosc, :szrodlo, :j_jednostka,
                current_timestamp(0), :sesja)
              matching(ktm, jedn)
              returning ref into :towjedn; --TOWJEDN  */
            o_glowna = null; o_przelicz = null; o_const = null; o_s = null; o_z = null;
            o_m = null; o_c = null; o_waga = null; o_wys = null; o_dlug = null; o_szer = null;
            if (:j_skadtabela = 'INT_IMP_TOW_TOWARY') then
              t_ref = :j_skadref;
          end

      end

      update int_imp_tow_jednostki
        set ktm = :ktm, wersjaref = :wersjaref, jedn = :jedn, towjedn = :towjedn,
          ref_towar = :t_ref
        where ref = :j_ref and (dataostprzetw is null or towjedn is null)
          and :towjedn is not null;

    end
    if(error_row = 1)then
    begin
      status = -1; --ML jezeli nie przetworzy sie chociaz jeden wiersz przetwarzanie nieudane;
      errormsg_all = substring(errormsg_all || :errormsg_row from 1 for 255);
    end
    update int_imp_tow_jednostki
      set error = :error_row,
        errormsg = :errormsg_row,
        dataostprzetw = current_timestamp(0)
      where ref = :j_ref;
  end

  if(coalesce(status,-1) != 1) then
    msg = substring((msg || :errormsg_all) from 1 for 255);

  --aktualizacja informacji o sesji
  if (aktualizujsesje = 1) then
  begin
    select status, msg from int_sesje_aktualizuj(:sesja, :status) into :tmpstatus, :tmpmsg;
    if(coalesce(status,-1) = 1) then
      status = :tmpstatus; --ML jezeli przed aktualizacjas sesji status byl zly, to nie zmieniamy na ok
    msg = substring((msg || coalesce(:tmpmsg,'')) from 1 for 255);
  end
  suspend;
end^
SET TERM ; ^
