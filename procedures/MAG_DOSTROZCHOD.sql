--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_DOSTROZCHOD(
      KTM varchar(80) CHARACTER SET UTF8                           ,
      WERSJA integer,
      DOSTAWA integer)
  returns (
      TYP char(3) CHARACTER SET UTF8                           ,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      DATA timestamp,
      MAG2 char(3) CHARACTER SET UTF8                           ,
      FSKROT SHORTNAME_ID,
      ILOSC numeric(15,4),
      CENA numeric(15,2),
      AKCEPT smallint,
      CENASNETTO numeric(15,2),
      CENASBRUTTO numeric(15,2),
      WARTSNETTO numeric(15,2),
      WARTSBRUTTO numeric(15,2))
   as
begin
    for select N.TYP, N.SYMBOL, N.DATA, N.MAG2, K.FSKROT
          , case when D.WYDANIA=1 then R.ILOSC when D.WYDANIA=0 and D.KORYG=1 then -R.ILOSC else 0 end
          , R.CENA, N.AKCEPT, R.CENASNETTO, R.CENASBRUTTO, R.WARTSNETTO, R.WARTSBRUTTO
        from DOKUMPOZ P
          left join DOKUMNAG N on (P.DOKUMENT=N.REF)
          left join DEFDOKUM D on (D.SYMBOL=N.TYP)
          left join DOKUMROZ R on (P.REF=R.POZYCJA)
          left join KLIENCI K on (N.KLIENT=K.REF)
        where P.KTM=:ktm and P.WERSJA=:wersja
          and ((D.WYDANIA=1 ) or (D.WYDANIA=0 and D.KORYG=1) ) and R.DOSTAWA=:dostawa
        into :typ, :symbol, :data, :mag2, :fskrot, :ilosc, :cena, :akcept,
             :CENASNETTO, :cenasbrutto, :WARTSNETTO, :wartsbrutto
  do
    suspend;
end^
SET TERM ; ^
