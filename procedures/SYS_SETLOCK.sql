--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_SETLOCK(
      LSYMBOL varchar(40) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint)
   as
declare variable connId integer;
declare variable lcount integer;
begin
  /* Procedura na podstawie unikalnego klucza blokuje dany zasob przypisując mu
     Id aktualnego polączenia. Jeżeli z tego samego connection ID wicej niż raz
     ustawi sie locka  zwieksza sie licznik. Zeby usunac locka nalezy tyle samo
     razy go sciagnac. Zwraca 1 jesli sie udao lub 0 gdy ktos inny blokuje */
  select first 1 s.connectionid, s.scount
    from sys_locks s
    where s.locksymbol = :lsymbol
    into :connId, :lcount;

  if (connId is not null) then
    if(connId <> current_connection) then
      status = 0;
    else begin
      lcount = lcount +1;
      update sys_locks set scount = :lcount where locksymbol = :lsymbol;
      status = 1;
    end
  else
  begin
     insert into sys_locks
       values(:lsymbol, current_connection,1);
     status = 1;
  end
  suspend;
end^
SET TERM ; ^
