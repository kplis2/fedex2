--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NOTY_GEN(
      DATA timestamp,
      DATAOD varchar(20) CHARACTER SET UTF8                           ,
      NOTATYP varchar(5) CHARACTER SET UTF8                           ,
      DOUNCLOSED smallint,
      INTERMORETHAN numeric(14,2),
      MOREDAYSTHAN integer,
      FORACCOUNT ACCOUNT_ID,
      FORSLODEF integer,
      FORSLOPOZ integer,
      COMPANY integer,
      REFKURSU integer)
  returns (
      STATUS integer,
      GENNUMBER integer)
   as
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable KONTOFK varchar(120);
declare variable SYMBFAK varchar(20);
declare variable DATAOTW timestamp;
declare variable DATAPLAT timestamp;
declare variable REFNOTA integer;
declare variable DATAZM timestamp;
declare variable SALDOWN numeric(14,2);
declare variable KWOTAFAK numeric(14,4);
declare variable KWOTAZAP numeric(14,4);
declare variable WN numeric(14,2);
declare variable MA numeric(14,2);
declare variable DT timestamp;
declare variable HASPOZ smallint;
declare variable RPREF integer;
declare variable NOTADOKUM integer;
declare variable INTEREST numeric(14,2);
declare variable FAKTORING smallint;
declare variable REFNAGFAK integer;
declare variable TABELA integer;
declare variable SLODEFREF integer;
declare variable WALUTA varchar(3);
declare variable NKURS numeric(14,4);
declare variable WNZL numeric(14,2);
declare variable MAZL numeric(14,2);
begin
  -- uzywane do akcji generowania not odsetkowych
  status= 0;
  gennumber = 0;
  moredaysthan = coalesce(moredaysthan, 0);

  if (coalesce(dataod, '')='') then
    dataod=null;
  for
    select slodef, slopoz, waluta
      from rozrach
      where dataplat < :data
        and (:dataod is null or (dataotw >= cast(:dataod as date)))
        and ((:forslodef is null) or (:forslodef = 0) or (slodef = :forslodef))
        and ((:forslopoz is null) or (:forslopoz = 0) or (slopoz = :forslopoz))
        and ((:foraccount is null) or (:foraccount = '') or (kontofk like :foraccount))
        and ((:dounclosed = 1) or (cast(datazamk as date) <= cast(:data as date)))
        and ((cast(datazamk as date) > cast(dataplat as date) + :moredaysthan) or (datazamk is null))
/*          and rozrach.typ = 'N' */
        and company = :company
      group by slodef, slopoz, waluta
      into :slodef, :slopoz, :waluta
  do begin
    --naglówek dokument
    if (:waluta <> 'PLN' and :refkursu = 0) then
      exception TABKURSNULL;

    tabela = null;
    select min(ref) from slodef where typ = 'KLIENCI'
      into :slodefref;

    if (slodef = :slodefref) then
      select interesttables
        from klienci
        where ref = :slopoz
      into :tabela;

    if (tabela is null) then
      select ref
        from interesttables
        where isdefault = 1
      into :tabela;

    if (:tabela is null) then
      exception FK_EXPT_TAB_ODSET;

    execute procedure gen_ref('NOTYNAG')
      returning_values :refnota;

    select tp.sredni from tabkursp tp
      where tp.waluta = :waluta and tp.tabkurs = :refkursu
      into :nkurs;

    if (:nkurs is null ) then
      nkurs = 1;

    insert into notynag(ref, notakind, notatyp, slodef, slopoz, data, status, company,currency, rateavg)
      values(:refnota, 1, :notatyp, :slodef, :slopoz, :data, 0, :company,:waluta, :nkurs);
    for
      select symbfak, kontofk, dataotw, dataplat, faktura
        from rozrach
        where slodef = :slodef
          and slopoz = :slopoz
          and dataplat < :data
          and (:dataod is null or (dataotw >= cast(:dataod as date)))
          and (cast(datazamk as date) <= cast(:data as date) or (:dounclosed = 1))
          and ((cast(datazamk as date) > cast(dataplat as date) + :moredaysthan) or (datazamk is null))
          and ((:foraccount is null) or (:foraccount = '') or (kontofk like :foraccount))  -- BS95923
          and company = :company
          and waluta = :waluta
        into :symbfak, :kontofk, :dataotw, :dataplat, :refnagfak
    do begin
      --analiza pojedynczego rozrachunku
      if (refnagfak is not null and refnagfak <> 0) then
        select faktoring from nagfak where ref = :refnagfak
          into :faktoring;

      faktoring = coalesce(faktoring, 0);
      kwotafak = 0;
      kwotazap = 0;
      saldown = 0;
      datazm = :dataplat;
      haspoz = 0;

      for
        select winien, ma, coalesce(interestdate,stransdate), ref, notadokum, winienzl, mazl
          from rozrachp
          where slodef = :slodef
            and slopoz = :slopoz
            and symbfak = :symbfak
            and kontofk = :kontofk
            and waluta = :waluta
            and cast(stransdate as date) <= cast(:data as date)
            and company = :company
          order by stransdate, ma -- poprawka wymuszajaca orderowanie azeby bral najpierw winien poprawia to zauwazony blad w osadkowskim
          into :wn, :ma, :dt, :rpref, :notadokum,:wnzl, :mazl
      do begin
        --obliczenie dni i odsetek do dnia zmiany
        if (wn > 0 and kwotafak = 0) then
          kwotafak = :wn;

        if (ma > 0) then
          kwotazap = :kwotazap + :ma;

        if (ma > 0) then
          saldown = :saldown - :ma;
        else
          saldown = :saldown + :wn;

        if (ma > 0) then
        begin
          if (saldown < 0) then
            kwotazap = kwotazap + saldown; --zmniejszenie kwoty zaplaconej
          if (notadokum is null) then
          begin
            if (dt > dataplat) then
            begin
              interest = 0;
              if (cast(dt as date) >= cast(datazm as date)) then
              begin
                execute procedure interest_count(:kontofk, :symbfak, cast(:dataplat as date), cast(:dt as date), :kwotazap, :tabela, :company)
                  returning_values :interest;

                if(interest < 0) then
                  interest = 0;

                if(interest > intermorethan) then
                  insert into notypoz(dokument, account, settlement, datastl, datapay, datapaid, ammount, ammountpaid, daysof, interest, faktoring, currency, AMMOUNTZL, AMMOUNTPAIDZL, interestzl)
                    values(:refnota, :kontofk, :symbfak, :dataotw, :dataplat, :dt, :kwotafak, :kwotazap, cast(:dt - :dataplat as smallint), :interest, :faktoring,:waluta, :kwotafak*:nkurs, :kwotazap*:nkurs, :interest * :nkurs);
              end else
              begin
                if(intermorethan = 0 ) then
                  insert into notypoz(dokument, account, settlement, datastl, datapay, datapaid, ammount, ammountpaid, daysof, interest, faktoring, currency, bdebitzl, bcreditzl, AMMOUNTZL, AMMOUNTPAIDZL, interestzl)
                    values(:refnota, :kontofk, :symbfak, :dataotw, :dataplat, :dt, :kwotafak, :kwotazap, 0, :interest, :faktoring, :waluta, :wnzl, :mazl, :kwotafak*:nkurs, :kwotazap*:nkurs, 0);
              end
            end

            update rozrachp set notadokum = :refnota where ref = :rpref;

            if (saldown < 0) then
              kwotazap = :kwotazap - :saldown;  --zmniejszenie kwoty zaplconej
            haspoz = 1;
          end
          kwotazap = :kwotazap - :ma;
        end
        datazm = :dt;
      end
    end

    if (refnota > 0) then
    begin
      if (exists (select ref from notypoz where dokument = :refnota)) then
      begin
        select interests from notynag where ref = :refnota
          into :interest;
      end
      interest = coalesce(interest, 0);
      if (interest < intermorethan) then
      begin
        delete from notynag where ref = :refnota;
        refnota = 0;
      end
    end
    if (refnota > 0) then
      gennumber = gennumber + 1;
    if (not exists(select first 1 1 from notypoz np where np.dokument = :refnota)) then
      delete from notynag nn where nn.ref = :refnota;
    nkurs = NULL;
  end
  status = 1;
end^
SET TERM ; ^
