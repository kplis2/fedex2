--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GRUPYFAK
  returns (
      ID integer)
   as
begin
  id = gen_id(GRUPYFAK,1);
  execute procedure RP_CHECK_REF('GRUPYFAK', :id)
    returning_values :id;
  suspend;
end^
SET TERM ; ^
