--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ECAL_WORK(
      EMPLOYEE integer,
      STARTDATE timestamp,
      ENDDATE timestamp,
      EMPLCONTRACT integer = null)
  returns (
      WD integer,
      WS integer,
      WS_VAC integer)
   as
declare variable WORKDIM float;
declare variable DIMNUM smallint;
declare variable DIMDEN smallint;
declare variable PROPORTIONAL smallint;
declare variable CSTARTDATE timestamp;
declare variable CENDDATE timestamp;
declare variable FROMDATE timestamp;
declare variable TODATE timestamp;
declare variable PERSONNAMES varchar(120);
declare variable NORM_PER_DAY integer;
declare variable WS_PART integer;
declare variable WD_PART integer;
declare variable WS_VAC_PART integer;
declare variable STDCALENDAR integer;
declare variable ISCALEND smallint;
declare variable PYEAR integer;
begin
/*Personel: Funkcja zwraca ilosc roboczych dni WD i sekund WS dla danego pracownika
            EMPLOYEE i zadanego przedzialu czasowego STARTDATE-ENDDATE*/

  if (startdate is null or enddate is null) then
    exception ecal_work_dates;

  if (not exists(select first 1 1 from employees where ref = :employee)) then
    exception brak_pracownika;

  wd = 0;
  ws = 0;
  ws_vac = 0;
  iscalend = 0;
  emplcontract = coalesce(emplcontract,0);

  if (exists(select first 1 1 from emplcalendar
               where employee = :employee and fromdate <= :enddate)
  ) then
    iscalend = 1;

  --wyliczenie czasu pracy dla pracownika
  for
    select fromdate, todate, workdim, dimnum, dimden, proportional
      from e_get_employmentchanges(:employee, :startdate, :enddate, ';WORKDIM;PROPORTIONAL;')
      into :fromdate, :todate, :workdim, :dimnum, :dimden, proportional
  do begin
    if (iscalend = 0) then
    begin
      select personnames from employees where ref = :employee
        into :personnames;
      exception brak_kalendarza 'Brak przypisanego kalendarza dla pracownika: ' || personnames;
    end

    execute procedure emplcaldays_store('SUM', :employee, :fromdate, :todate)
      returning_values :ws_part;

    execute procedure emplcaldays_store('COUNT', :employee, :fromdate, :todate)
      returning_values :wd_part;

    if (workdim <> 1 and proportional = 1) then
      ws_part = ws_part * dimnum / dimden;
    ws = ws + ws_part;
    wd = wd + wd_part;

    ws_vac_part = 0;
    for
      select ec.pyear, c.norm_per_day, coalesce(sum(worktime),0) --BS40990
        from ecalendars c
          join emplcaldays ec on (ec.ecalendar = c.ref)
          join edaykinds edk on edk.ref = ec.daykind
        where ec.cdate >= :fromdate and ec.cdate <= :todate
          and edk.daytype = 1
          and ec.employee = :employee
        group by ec.pyear, c.norm_per_day
        into :pyear, :norm_per_day, :ws_part
    do begin
      if (pyear < 2012) then --BS45844
      begin
      /*jesli norma czasu pracy jest nizsza niz 8 godzin, pracownika traktujemy
        jakby pracowal nominalnie 8h (BS33754)*/
        if (norm_per_day < 8 * 60 * 60) then
          ws_part = ws_part * 8 * 60 * 60 / norm_per_day;
      end
      ws_vac_part = ws_vac_part + ws_part;
    end

    if (workdim <> 1 and proportional = 1) then
      ws_vac_part = ws_vac_part * dimnum / dimden;
    ws_vac = ws_vac + ws_vac_part;
  end

  if (workdim is null) then
  begin
    for
      select fromdate, coalesce(enddate,todate) from emplcontracts
        where employee = :employee
          and (ref = :emplcontract or :emplcontract = 0)
          and ((:iscalend = 1 and empltype <> 1)
            or (:iscalend = 0 and iflags containing ';DFC;'))
          and fromdate <= :enddate
          and (coalesce(enddate,todate) >= :startdate or coalesce(enddate,todate) is null)
          order by fromdate
        into :cstartdate, :cenddate
    do begin
      if (cstartdate < startdate) then --BS63056
        cstartdate = startdate;
      if (cenddate is null or cenddate > enddate) then
        cenddate = enddate;

      if (iscalend = 1) then
      begin
      --wyliczenie czasu pracy dla zleceniobiorcy z kalendarzem (BS41263)
        execute procedure emplcaldays_store('SUM', :employee, :cstartdate, :cenddate)
          returning_values :ws_part;
        execute procedure emplcaldays_store('COUNT', :employee, :cstartdate, :cenddate)
          returning_values :wd_part;
      end else begin
       --dla zleceniobiorcy bez kalendarza, oplacajacego DFC, czas pracy bierzemy ze standardowego kalendarza
        if (stdcalendar is null) then
          execute procedure get_config('STDCALENDAR',2)
            returning_values stdcalendar;

        execute procedure ecaldays_store('SUM', :stdcalendar, :cstartdate, :cenddate)
          returning_values :ws_part;
        execute procedure ecaldays_store('COUNT', :stdcalendar, :cstartdate, :cenddate)
          returning_values :wd_part;
      end
      ws = ws + ws_part;
      wd = wd + wd_part;
    end
    ws_vac = ws;
  end

  suspend;
end^
SET TERM ; ^
