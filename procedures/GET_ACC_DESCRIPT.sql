--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_ACC_DESCRIPT(
      YEARID integer,
      ACCSYMBOL varchar(255) CHARACTER SET UTF8                           ,
      SEP varchar(5) CHARACTER SET UTF8                           )
  returns (
      ACCDESCRIPT varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable ACC0 varchar(3);
declare variable COMPANY integer;
declare variable BKREF integer;
declare variable S_ANALITYKI varchar(1024);
declare variable I integer;
declare variable SEPARATOR varchar(1);
declare variable LEN integer;
declare variable DICTDEF integer;
declare variable ACC1 varchar(60);
declare variable DICTNAME varchar(255);
begin
  execute procedure get_global_param('CURRENTCOMPANY') returning_values :company;
  Acc0 = substring(:AccSymbol from 1 for 3);
  select ref, descript
      from BKACCOUNTS
      where yearid = :yearid
      and symbol = :Acc0
      and company = :company
      into :bkref,:s_analityki;
  i = 4;
  for select separator,len,dictdef from accstructure where bkaccount = :bkref order by nlevel
  into :separator,:len,:dictdef
  do begin
    if(separator <> ',') then
      i = i + 1;
    Acc1 = substring(AccSymbol from :i for :len);
    if(coalesce(s_analityki,'') != '') then
      s_analityki = s_analityki||sep;
    execute procedure SLOWNIK_NAZWA_FROMKS(:dictdef,:Acc1) returning_values :dictname;
    s_analityki = s_analityki || coalesce(dictname,'');
    i = i + :len;
  end
  accdescript = s_analityki;
  suspend;
end^
SET TERM ; ^
