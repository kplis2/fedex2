--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SERIAL_CHECKROZ(
      DOKUMSERNAG integer,
      DOKUMSERPOZ integer,
      DOKUMSERTYP varchar(3) CHARACTER SET UTF8                           )
  returns (
      STATUS integer,
      ILDO numeric(14,4),
      ILJEST numeric(14,4),
      REWDICT integer,
      KTM varchar(80) CHARACTER SET UTF8                           )
   as
declare variable akcept integer;
begin
  REWDICT = 0;
  if(:DOKUMSERNAG is null) then DOKUMSERNAG = 0;
  if(:DOKUMSERNAG<>0) then begin
    if(:DOKUMSERTYP ='M') then begin
      for select dokumpoz.ref, dokumpoz.ilosc, dokumpoz.ktm, dokumnag.akcept
        from DOKUMPOZ
        join TOWARY on (DOKUMPOZ.KTM = TOWARY.KTM)
        left join DOKUMNAG on (DOKUMNAG.REF=DOKUMPOZ.DOKUMENT)
        where DOKUMPOZ.dokument = :dokumsernag and TOWARY.SERIAL = 1
        into :dokumserpoz, :ildo, :ktm, :akcept
      do begin
        if(:akcept=1) then begin
          status = 1;
          ktm=:ktm;
          suspend;
          exit;
        end
        if(:ildo is null) then ildo = 0;
        if(:ildo < 0)then begin
          ildo = -:ildo;
          REWDICT = 1;
        end
        select sum(ILOSC) from DOKUMSER where DOKUMSER.refpoz = :dokumserpoz and DOKUMSER.typ = :dokumsertyp into :iljest;
        if(:iljest is null) then iljest = 0;
        if(:ildo = :iljest) then REWDICT = 2;
        if(:ildo <> :iljest) then begin
          status = 0;
          ktm=:ktm;
          suspend;
          exit;
        end else status = 1;
      end
    end else if(:DOKUMSERTYP ='F') then begin
      for select pozfak.ref, (pozfak.ilosc - pozfak.pilosc), pozfak.ktm, nagfak.akceptacja
        from POZFAK
        join TOWARY on (POZFAK.KTM = TOWARY.KTM)
        left join NAGFAK on (NAGFAK.REF = POZFAK.DOKUMENT)
        where POZFAK.dokument = :dokumsernag and TOWARY.serial = 1
        into :dokumserpoz, :ildo, :ktm, :akcept
      do begin
        if(:akcept=1) then begin
          status = 1;
          ktm=:ktm;
          suspend;
          exit;
        end
        if(:ildo is null) then ildo = 0;
        if(:ildo < 0)then begin
          ildo = -:ildo;
          REWDICT = 1;
        end
        select sum(ILOSC) from DOKUMSER where DOKUMSER.refpoz = :dokumserpoz and DOKUMSER.typ = :dokumsertyp into :iljest;
        if(:iljest is null) then iljest = 0;
        if(:ildo = :iljest) then REWDICT = 2;
        if(:ildo <> :iljest) then begin
          status = 0;
          ktm=:ktm;
          suspend;
          exit;
        end else status = 1;
      end
    end else begin
      for select pozzam.ref, (pozzam.ilosc - pozzam.ilzreal), pozzam.ktm
        from POZZAM
        join TOWARY on (POZZAM.KTM = TOWARY.KTM)
        where POZZAM.ZAMOWIENIE = :dokumsernag and TOWARY.SERIAL = 1
        into :dokumserpoz, :ildo, :ktm
      do begin
        if(:ildo is null) then ildo = 0;
        if(:ildo < 0)then begin
          ildo = -:ildo;
          REWDICT = 1;
        end
        select sum(ILOSC) from DOKUMSER where DOKUMSER.refpoz = :dokumserpoz and DOKUMSER.typ = :dokumsertyp into :iljest;
        if(:iljest is null) then iljest = 0;
        if(:ildo = :iljest) then REWDICT = 2;
        if(:ildo <> :iljest) then begin
          status = 0;
          ktm=:ktm;
          suspend;
          exit;
        end else status = 1;
      end
    end
  end else begin
    if(:DOKUMSERTYP ='M') then
      select ILOSC from DOKUMPOZ where DOKUMPOZ.ref = :dokumserpoz into :ildo;
    else if(:DOKUMSERTYP = 'F') then
      select ILOSC - PILOSC from POZFAK where REF=:dokumserpoz into :ildo;
    else if (:dokumsertyp = 'R') then
      select amount from propersraps where ref =:dokumserpoz into :ildo;
    else
      select ILOSC - ILZREAL from POZZAM where REF=:dokumserpoz into :ildo;
    if(:ildo is null) then ildo = 0;
    if(:ildo < 0)then begin
      ildo = -:ildo;
      REWDICT = 1;
    end
    select sum(ILOSC) from DOKUMSER where DOKUMSER.refpoz = :dokumserpoz and DOKUMSER.typ = :dokumsertyp into :iljest;
    if(:iljest is null) then iljest = 0;
    if(:ildo = :iljest) then REWDICT = 2;
    if(:ildo <> :iljest) then status = 0;
    else
      status = 1;
  end
end^
SET TERM ; ^
