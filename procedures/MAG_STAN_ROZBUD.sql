--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_STAN_ROZBUD(
      MAG varchar(3) CHARACTER SET UTF8                           ,
      ODDZIAL varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      CENA numeric(14,4),
      DOSTAWA integer,
      DATAOD timestamp,
      DATADO timestamp)
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      SPNET numeric(14,2),
      SPBRU numeric(14,2),
      ILOSC numeric(14,4),
      MARPR numeric(14,2),
      MAR numeric(14,2),
      KOSZT numeric(14,2),
      WART numeric(14,2),
      STAN numeric(14,4),
      ZAP numeric(14,2))
   as
begin
 for select POZFAK.KTM , max(TOWARY.NAZWA),
    SUM(POZFAK.ILOSC - POZFAK.PILOSC),

    SUM(POZFAK.WARTNET- POZFAK.PWARTNET),
    SUM(POZFAK.WARTBRU - POZFAK.PWARTBRU),
    SUM(CASE
       when (nagfak.magrozlicz=1) then pozfak.kosztzak
       when (nagfak.magrozlicz=0) then pozfak.kosztkat
     END),
    SUM(POZFAK.WARTNET - POZFAK.PWARTNET)
    -SUM(CASE
        when (nagfak.magrozlicz=1) then pozfak.kosztzak
        when (nagfak.magrozlicz=0) then pozfak.kosztkat
     END),
    100*(SUM(POZFAK.WARTNET-POZFAK.PWARTNET)-SUM(CASE
        when (nagfak.magrozlicz=1) then pozfak.kosztzak
        when (nagfak.magrozlicz=0) then pozfak.kosztkat
      END)) / SUM(POZFAK.WARTNET- POZFAK.PWARTNET)

  from NAGFAK
  join POZFAK on (NAGFAK.REF=POZFAK.DOKUMENT)
  left join TOWARY on (POZFAK.KTM = TOWARY.KTM)
  where (NAGFAK.ZAKUP = 0) and (NAGFAK.AKCEPTACJA = 1 or NAGFAK.AKCEPTACJA = 8)
  and (NAGFAK.ANULOWANIE = 0) and (NAGFAK.NIEOBROT < 2)
  and (NAGFAK.ODDZIAL = :oddzial)
  and (NAGFAK.DATA >= :dataod)
  and (NAGFAK.DATA <= :datado)
  group by POZFAK.KTM having SUM(POZFAK.WARTNET- POZFAK.PWARTNET)<>0
  order by POZFAK.KTM into :ktm, :nazwa, :ilosc, :spnet, :spbru, :koszt, :mar, :marpr
  do begin
    wart=0;
    stan=0;
    select SUM(STANYCEN.wartosc), sum(stanycen.ilosc) from stanycen
       where STANYCEN.ktm = :ktm and stanycen.magazyn = :MAG into :wart, :stan;
   if (ilosc = 0 or ilosc is null) then zap=0;
    else
    zap = :stan/(30/(:datado-:dataod)*:ilosc) * 30;
    suspend;
 end
end^
SET TERM ; ^
