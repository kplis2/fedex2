--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_ADD_INPOST_REQUEST(
      DOCTYPE char(1) CHARACTER SET UTF8                           ,
      DOCUMENT integer,
      EDERULESYMB varchar(40) CHARACTER SET UTF8                           )
  returns (
      EDEDOCHREF integer)
   as
declare variable EDERULE integer;
declare variable SPEDRESPONSE varchar(255);
declare variable OTABLE varchar(31);
begin
  if (:doctype = 'Z') then
    otable = 'NAGZAM';
  else if (:doctype = 'F') then
    otable = 'NAGFAK';
  else if (:doctype = 'M') then
    otable = 'DOKUMNAG';
  else
    otable = 'LISTYWYSD';

  select es.ref
    from ederules es
    where es.symbol = :ederulesymb
    into :ederule;


  if(:ederulesymb = 'EDE_INPOST_SHIPMENT' and otable = 'LISTYWYSD'
    and exists(
      select first 1 1 from listywysd lwd
        where lwd.ref = :document and coalesce(lwd.symbolsped,'') != ''
        and lwd.x_anulowany is distinct from 1
    )
  )then
  begin
    exception universal 'Przesyka została już nadana.';
  end
  else if(exists(select first 1 1 from listywysd lwd
        where lwd.ref = :document and lwd.akcept is distinct from 2)
  )then
    exception universal 'Nie można nadać niezamkniętego dokumentu';

  insert into ededocsh( ederule, direction, createdate, otable, oref, status, filename, manualinit)
    values(:ederule, 1, current_timestamp, :otable, :document, 0, :document||'exp',0)
    returning ref into :ededochref;

  suspend;
end^
SET TERM ; ^
