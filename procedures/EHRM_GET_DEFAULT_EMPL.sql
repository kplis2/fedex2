--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EHRM_GET_DEFAULT_EMPL(
      PERSONREF PERSONS_ID,
      NEWEMPLREF EMPLOYEES_ID = 0)
  returns (
      EMPLREF EMPLOYEES_ID,
      COMPANY COMPANIES_ID)
   as
BEGIN
  /* ---------------------
    PERSONREF - Parametr wejsciowy, definiuje osobe, dla której pobieramy domyslny wpis z tabeli employees
    NEWEMPLREF - Parametr wejsciowy, zaczytany z Ciasteczka, sluży do sprawdzenia czy w ciasteczku jest employeeref
      przypisany do podanego personref jesli nie to zostanie pobrany pierwszy z brzegu employeeref
    EMPLREF - Ref z tabeli EMPLOYEES, na który domyslnie zostaniemy zalogowani lub ref zczytany z ciasteczka

  */ ---------------------
  emplref = null;
  if (newemplref = 0) then
  begin
    select first 1 e.ref, e.company
      from employees e
      where e.person = :personref
        and e.empltype = 1
        and e.ehrm = 1
      order by e.empltype, e.fromdate
    INTO :emplref, :company;
  end

  if (emplref is null) then
    select first 1 e.ref, e.company
    from employees e
      where e.person = :personref
        and (e.ref = :newemplref or :newemplref = 0)
        and e.ehrm = 1
      order by e.empltype, e.fromdate
    into :emplref, :company;

   suspend;
END^
SET TERM ; ^
