--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_MINIMALWAGE(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable FROMDATE date;
declare variable TODATE date;
declare variable WS integer;
declare variable NHOURS numeric(14,2);
declare variable EHOURS numeric(14,2);
declare variable MINIMALWAGE numeric(14,2);
declare variable TMP numeric(14,2);
declare variable CALEND integer;
declare variable EWAGE numeric(16,6);
declare variable ACALDAYS integer;
declare variable AFROMDATE date;
declare variable ATODATE date;
declare variable DAYWAGE numeric(14,6);
begin
  --MW: personel - funkcja podaje wysokoć minimalnego wynagrodzenia dla pracownika

  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  execute procedure get_config('STDCALENDAR', 2)
      returning_values calend;

  if (not exists(select ref from ecalendars where ref = :calend)) then
      exception brak_kalendarza;

  execute procedure ecaldays_store('SUM', :calend, :fromdate, :todate)
    returning_values :ws;
  nhours = ws / 3600.00;

  --czas pracy pracownika
  select ws from ecal_work(:employee, :fromdate, :todate)
    into ehours;
  ehours = ehours / 3600.00;

  --nie wyrownuj do minimalnej gdy nieobecnosc nieplatna
  select sum(worksecs) / 3600.00
    from eabsences
    where fromdate >= :fromdate and todate <= :todate
      and employee = :employee
      and ecolumn in (10, 190, 260, 300, 330, 390) and correction = 0
    into :tmp;

  ehours = ehours - coalesce(tmp, 0);

  --minimalne wynagrodzenie (proporcjonalne do wymiaru czasu pracy)
  execute procedure ef_pval (employee,  payroll, prefix, 'MINIMALNAKRAJOWA')
    returning_values minimalwage;
  ewage = minimalwage;
  minimalwage = (minimalwage * ehours) / nhours;

  --obliczanie nieobecnosci liczonych w dniach kalendarzowych
  acaldays = 0;
  for
    select fromdate, todate
      from eabsences
      where employee = :employee
        and fromdate <= :todate and todate >= :fromdate
        and ecolumn in (40, 50, 60, 90, 100, 110, 120, 130, 140, 150, 160, 170, 270, 280, 290, 350, 360, 370, 440, 450)
        and correction in (0,2)
      into :afromdate, :atodate
  do begin
    if (afromdate < fromdate) then
      afromdate = fromdate;
    if (atodate > todate) then
      atodate = todate;
    acaldays = acaldays + (atodate - afromdate + 1);
  end

  daywage = 1.00000 * ewage / 30;
  minimalwage = minimalwage - daywage * acaldays;
  if (todate - fromdate  + 1 = acaldays) then
    minimalwage = 0;

  ret = minimalwage;
  suspend;
end^
SET TERM ; ^
