--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FILL_BRANCHES(
      OPER integer)
   as
declare variable oddz oddzial_id;
begin
  for
    select oddzial
      from oddzialy
      into oddz
  do begin
    if(not exists(select first 1 1
                    from operaccess
                    where oddzial = :oddz
                      and operator = :oper)) then
      insert into operaccess(oddzial,operator,defaultaccess)
        values(:oddz,:oper,1);
  end
end^
SET TERM ; ^
