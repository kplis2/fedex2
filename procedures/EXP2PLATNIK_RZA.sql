--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXP2PLATNIK_RZA(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      COMPANY integer,
      FORPERSON integer = 0)
  returns (
      ROWNUMBER integer,
      PERSON integer,
      FNAME varchar(60) CHARACTER SET UTF8                           ,
      SNAME varchar(30) CHARACTER SET UTF8                           ,
      ICODE varchar(6) CHARACTER SET UTF8                           ,
      BASE numeric(14,2),
      NFZ_P3 numeric(14,2),
      NFZ_P4 numeric(14,2),
      NFZ_P5 numeric(14,2),
      NFZ_P6 numeric(14,2),
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      DOCNO varchar(11) CHARACTER SET UTF8                           )
   as
declare variable TODATE timestamp;
declare variable FROMDATE timestamp;
declare variable ICODE_B varchar(2);
declare variable ZL_TODATE timestamp;
declare variable ZL_FROMDATE timestamp;
declare variable IS_ZUSEMPLCONTR smallint;
begin
--MWr: Personel - Eksport do Platnika deklaracji rozliczeniowej ZUS RZA (ver.xml)

/*  NFZ_P3 -Kwota należnej skadki finansowana przez platnika
    NFZ_P4 -Kwota należnej skadki finansowana z budżetu panstwa bezposrednio na zus
    NFZ_P5 -Kwota należnej skadki finansowana przez ubezpieczonego
    NFZ_P6 -Kwota należnej skadki finansowana przez fundusz koscielny   */

  if (coalesce(period,'') = '') then  --PR50885
    exit;

  if (company > 0) then  --PR57779
  begin
    for
      select person, fname, sname, icode, doctype, docno
           , sum(base), sum(nfz_p3), sum(nfz_p4), sum(nfz_p5), sum(nfz_p6)
        from exp2platnik_rza(:period, -:company, :forperson)
        group by person, fname, sname, icode, doctype, docno
        order by sname COLLATE UNICODE, fname
        into :person, :fname, :sname, :icode, :doctype, :docno
           , :base, :nfz_p3, :nfz_p4, :nfz_p5, :nfz_p6
    do begin
      rownumber = coalesce(rownumber,0) + 1; --BS66657 
      suspend;
    end
  end else begin ---------------------------------------------------------------

    execute procedure period2dates(period)
      returning_values fromdate, todate;

    company = -company;
    forperson = coalesce(forperson,0);

    for
      select e.person, e.fname, e.sname, min(c.fromdate), max(coalesce(c.enddate, c.todate, cast('3000-01-01' as timestamp))), z.code,
          sum(case when p.ecolumn = 7150 then p.pvalue else null end),
          sum(case when p.ecolumn = 7220 then p.pvalue else null end)
        from employees e
          join emplcontracts c on (c.employee = e.ref and c.empltype in (2,3)
                                     and (e.person = :forperson or :forperson = 0)) --PR50885, BS57447 
          left join epayrolls r on (r.iper = :period and ((r.empltype = 2 and r.emplcontract = c.ref) --left join bo umowa nie musi miec rachunku 
                                                           or (r.empltype = 3 and (r.cper >= (select periodout from e_func_periodinc(null,0,C.fromdate)) --powiazanie list plac z umowami
                                                             and r.cper <= (select periodout from e_func_periodinc(null,0,coalesce(c.enddate, c.todate, cast('3000-01-01' as timestamp))))))))
          left join eprpos p on (p.payroll = r.ref and p.employee = c.employee and p.ecolumn in (7150,7220))
          left join edictzuscodes z on (z.ref = c.ascode)
        where e.company = :company
          and c.iflags not like '%%FC;%%'
          and c.iflags not like '%%;FE;%%'
          and c.iflags not like '%%;FR;%%'
          and c.iflags like '%%;NFZ;%%'
          and not (p.employee is null
            and not (c.fromdate <= :todate and (coalesce(c.enddate,c.todate) >= :fromdate or coalesce(c.enddate,c.todate,'1900-01-01') = '1900-01-01')))
        group by e.person, e.fname, e.sname, z.code,
          case when r.ref is not null then 1 else 0 end --pokazuje zerowe i niezerowe RZA gdy w obu przypadkach ten sam kod ubezp.
        into :person, :fname, :sname, :zl_fromdate, :zl_todate, :icode, :base, :nfz_p5
    do begin
    
      is_zusemplcontr = null;
      select first 1 1 from emplcontracts c
          join employees e on (c.employee = e.ref and e.person = :person and c.empltype > 1)
          join epayrolls r on (r.iper = :period and r.emplcontract = c.ref) -- PR57779
        where c.iflags like '%%FC;%%'
            or c.iflags like '%%;FE;%%'
            or c.iflags like '%%;FR;%%'
        into :is_zusemplcontr; --BS45076
  
      if (is_zusemplcontr is null) then
      begin
        if (zl_todate < fromdate) then icode = '3000';
        else if (icode is null) then icode = '0000';
    
        select first 1 zc1.code || zc2.code,
           case when (p.pesel > '') then 'P' when (p.evidenceno > '') then '1' when (p.passportno > '') then '2' else '' end as doctype,
           case when (p.pesel > '') then p.pesel when (p.evidenceno > '') then p.evidenceno when (p.passportno > '') then p.passportno else '' end as docno
          from persons p
            left join ezusdata z on (z.person = p.ref and z.fromdate <= :todate)
            left join edictzuscodes zc1 on (zc1.ref = z.retired)
            left join edictzuscodes zc2 on (zc2.ref = z.invalidity)
          where p.ref = :person
          order by z.fromdate desc
          into :icode_b, :doctype, :docno;
    
        if (doctype = '') then doctype = '0';
        if (docno = '') then docno = '0';
        icode = icode || coalesce(icode_b,'00');
        suspend;
    
        icode_b = null;   --dopelnienie kodu ubez. z umowy o emeryt. i niepelnosprawn.
        icode = null;     --kod tytulu ubezpieczenia
        base = null;      --podstawa wymiaru skladki
        nfz_p5 = null;    --kwota skladki
        doctype = null;   --typ dokumentu
        docno = null;     --idendyfikator dokumentu
      end
    end
  end
end^
SET TERM ; ^
