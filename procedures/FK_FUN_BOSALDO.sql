--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_FUN_BOSALDO(
      COMPANY integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      ACCOUNTS ACCOUNT_ID,
      SIDE smallint)
  returns (
      AMOUNT numeric(15,2))
   as
declare variable YEARID integer;
declare variable S_PERIOD varchar(6);
declare variable DEBIT numeric(15,2);
declare variable CREDIT numeric(15,2);
begin
  select yearid from bkperiods where id=:period
    and company = :company
    into :yearid;
  s_period = cast(:yearid as varchar(4)) || '00';
  select sum(debit), sum(credit) from turnovers
    where account like :accounts and period=:s_period
      and company = :company
    into :debit, :credit;
  if(:debit is null) then debit = 0;
  if(:credit is null) then credit = 0;
  if (:side=0) then
    amount = :debit;
  else
    amount = :credit;
  suspend;
end^
SET TERM ; ^
