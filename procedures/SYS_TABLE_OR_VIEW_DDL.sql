--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_TABLE_OR_VIEW_DDL(
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      CREATE_OR_ALTER smallint)
  returns (
      DDL blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           )
   as
declare variable FIELD_NAME varchar(80);
declare variable FIELD_DDL varchar(1024);
declare variable VIEW_SELECT_DDL blob sub_type 1 segment size 80;
declare variable EOL varchar(2);
declare variable TAB varchar(4);
declare variable WHITESPACE varchar(64);
declare variable MAX_LEN integer;
declare variable MAX_POS integer;
declare variable FIELD_POS integer;
declare variable I integer;
declare variable TABLE_OR_VIEW smallint;
declare variable PRIMARY_KEY_NAME varchar(255);
declare variable PRIMARY_KEY_FIELD_NAME varchar(255);
declare variable PRIMARY_KEY_FIELDS_NUMBER integer;
declare variable PRIMARY_KEY_CURRENT_ROW integer;
declare variable PRIMARY_KEY_CONSTRAINT varchar(255);
begin
/* create_or_alter - czy widok był zmieniony, jeżeli 1 to stosuje konstrukcje create or alter view */
  eol ='
  ';
  tab = '    ';
  table_or_view = 1;   -- 0 - widok, 1 - zwykla tabela, 2 - temporary delete, 3 - temporary preserve 4 - virtualna

  select
    case
      when rdb$relation_type = 0 then 1  --persistent
      when rdb$relation_type = 1 then 0  --view
      when rdb$relation_type = 3 then 4  --virtual
      when rdb$relation_type = 4 then 3  --global_temporary_preserve
      when rdb$relation_type = 5 then 2  --global_temporary_delete
      when rdb$relation_type is null and rdb$view_blr is null then 1 --treated as persistent based on rdb$view_blr
      when rdb$relation_type is null and rdb$view_blr is not null then 0 --treated as view based on rdb$view_blr
      else -1
    end
    from rdb$relations
    where rdb$relation_name = :nazwa
    into :table_or_view;

  ddl = case
    when table_or_view = 0 and create_or_alter = 0 then 'CREATE VIEW ' || :nazwa || ' (' || :eol
    when table_or_view = 0 and create_or_alter = 1 then 'CREATE OR ALTER VIEW ' || :nazwa || ' (' || :eol
    when table_or_view = 1 then 'CREATE TABLE ' || :nazwa || ' (' || :eol
    when table_or_view = 2 then 'CREATE GLOBAL TEMPORARY TABLE ' || :nazwa || ' (' || :eol
    when table_or_view = 3 then 'CREATE GLOBAL TEMPORARY TABLE ' || :nazwa || ' (' || :eol
    when table_or_view = 4 then 'CREATE TABLE ' || :nazwa || ' (' || :eol
    else 'CREATE TABLE ' || :nazwa || ' (' || :eol  -- domyslnie zwykla tabela
  end;

  if (table_or_view = 0) then
    select rl.rdb$view_source
    from rdb$relations rl
    where rl.rdb$relation_name = :nazwa
    into :view_select_ddl;

  if (table_or_view = 1) then
    select rc.rdb$constraint_name, rc.rdb$index_name
    from rdb$relation_constraints rc
    where rc.rdb$constraint_type = 'PRIMARY KEY' and rc.rdb$relation_name = :nazwa
    into :primary_key_constraint, :primary_key_name;

  select max(char_length(trim(r.rdb$field_name))),
      max(r.rdb$field_position)
    from rdb$relation_fields r
    where r.rdb$relation_name = :nazwa
    into :max_len, :max_pos;

  if (table_or_view = 1) then
      begin
         select count(s.rdb$field_name)
         from rdb$index_segments s
         where s.rdb$index_name = :PRIMARY_KEY_NAME or s.rdb$index_name = :primary_key_constraint
         into :primary_key_fields_number;
         primary_key_current_row = 1;

         if(primary_key_fields_number = 0) then begin
           select first 1 trim(r.rdb$field_name)
           from rdb$relation_fields r
           where rdb$relation_name = :nazwa 
           order by rdb$field_position
           into :field_name;
           execute procedure sys_field_ddl(:nazwa||'.'||:field_name, 1) returning_values :field_ddl;
           ddl = :ddl || :tab || :field_ddl || :eol;
         end
         else
         for select trim(s.rdb$field_name)
         from rdb$index_segments s
         where s.rdb$index_name = :PRIMARY_KEY_NAME or s.rdb$index_name = :primary_key_constraint
         order by s.rdb$field_position
         into :PRIMARY_KEY_FIELD_NAME
         do begin
           execute procedure sys_field_ddl(:nazwa||'.'||:PRIMARY_KEY_FIELD_NAME, 1) returning_values :field_ddl;
           ddl = :ddl || :tab || :field_ddl;
           if(primary_key_current_row < primary_key_fields_number) then
             ddl = :ddl || ',';
           ddl = :ddl || :eol;
           primary_key_current_row = :primary_key_current_row + 1;
         end
         --ddl = substr(ddl, -1) || :eol;
      end
  else
  for select trim(r.rdb$field_name), r.rdb$field_position
    from rdb$relation_fields r
    where rdb$relation_name = :nazwa
    order by r.rdb$field_position
    into :field_name, :field_pos
  do begin
    whitespace = '';
    i = 0;
    while(i < (max_len - char_length(field_name))) do
    begin
      whitespace = :whitespace || ' ';
      i = :i + 1;
    end
    field_name = :field_name || :whitespace;

    if(table_or_view = 0) then
      field_ddl = field_name;
    else
    begin
       execute procedure sys_field_ddl(:nazwa||'.'||:field_name, 1) returning_values :field_ddl;
      field_ddl = ' ' || field_ddl;
    end

    field_name = trim(:field_name);
    if(field_pos <> max_pos) then
      ddl = :ddl || :tab || :field_ddl || ',';
    else
      ddl = :ddl || :tab || :field_ddl;

    if(table_or_view = 0) then
    begin
      execute procedure sys_field_ddl(:nazwa||'.'||:field_name, 1) returning_values :field_ddl;
      field_ddl = substring(:field_ddl from position(' ', :field_ddl));
      ddl = :ddl || '--' || coalesce(:field_ddl, '');
    end
    ddl = :ddl || :eol;
  end

  ddl = case
    when table_or_view = 0 then :ddl || ')' || :eol || 'AS ' || :view_select_ddl || eol || ';'
    when table_or_view = 1 then :ddl || ');'
    when table_or_view = 2 then :ddl || ') ON COMMIT DELETE ROWS;'
    when table_or_view = 3 then :ddl || ') ON COMMIT PRESERVE ROWS;'
    when table_or_view = 4 then :ddl || ');'
    else :ddl || ');'  -- domyslnie zwykla tabela
  end;

  suspend;
end^
SET TERM ; ^
