--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_MOVEFAKTURA(
      OLDZAM integer,
      NEWZAM integer,
      HISTORIA integer)
   as
DECLARE VARIABLE FAKTURA INTEGER;
DECLARE VARIABLE SYMBOL VARCHAR(20);
begin
  select FAKTURA, SYMBFAK from NAGZAM where REF = :oldzam into :faktura, :symbol;
  if(:faktura > 0) then begin
    update NAGZAM set FAKTURA = :faktura, SYMBFAK = :symbol where REF=:newzam;
    update NAGZAM set FAKTURA = NULL, NAGZAM.symbfak = '' where REF = :oldzam;
    update HISTZAM set OLDFAKTURA  = :faktura where REF=:historia;
  end
end^
SET TERM ; ^
