--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE UNSYNCBKYEAR(
      DEST BKYEARS_ID)
  returns (
      RET smallint)
   as
begin
  -- Procedura odwiązująca wzorzec do roku ksigowego.
  ret = 0;
  update bkyears b
    set b.patternref = 0
    where b.ref = :dest;
  ret = 1;
  suspend;
  when any
    do begin
      suspend;
    end
end^
SET TERM ; ^
