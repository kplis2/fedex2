--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_ADDFAK(
      DOKSPED integer,
      DOKMAG integer,
      MAGTRYB integer,
      USLTRYB integer,
      STANFAK varchar(255) CHARACTER SET UTF8                           ,
      STYPFAK varchar(3) CHARACTER SET UTF8                           ,
      SREJFAK varchar(3) CHARACTER SET UTF8                           ,
      OPERATOR integer,
      SPOSPLAT integer)
   as
DECLARE VARIABLE SPOSZAP INTEGER;
DECLARE VARIABLE SPOSD INTEGER;
DECLARE VARIABLE KLIENT INTEGER;
DECLARE VARIABLE DOKFAK INTEGER;
DECLARE VARIABLE TYPFAK VARCHAR(3);
DECLARE VARIABLE KOSZTDOST NUMERIC(14,2);
DECLARE VARIABLE KOSZTDOSTBEZ INTEGER;
DECLARE VARIABLE KTM VARCHAR(20);
DECLARE VARIABLE POZREF INTEGER;
DECLARE VARIABLE MAXWARTBRU NUMERIC(14,4);
DECLARE VARIABLE ILOSC NUMERIC(14,4);
DECLARE VARIABLE BN CHAR(1);
DECLARE VARIABLE TERMIN INTEGER;
DECLARE VARIABLE STYPFAK1 VARCHAR(3);
DECLARE VARIABLE POZFAKREF INTEGER;
declare variable gr_vat varchar(10);
declare variable stawka numeric(14,2);
begin
  /*fakturowanie dok. mag*/
  if(:srejfak = '' or (:srejfak is null)) then
    exception LISTYWYSD_ADDFAK_EXPT 'Brak okreslonego rejestru dok. VAT';
  if(:stanfak = '' or (:stanfak is null)) then
    exception LISTYWYSD_ADDFAK_EXPT 'Brak okreslonego stanowiska dok. VAT';
  if(:magtryb > 0 and :dokmag > 0)then begin
    /*wyszukanie nagowka dok. do fakturowania*/
    select KLIENT, SPOSZAP, TYPDOKVAT  from DOKUMNAG where REF=:dokmag into :klient, :sposzap, :typfak;
    if(:typfak is null or (:typfak = '')) then typfak = :stypfak;
    if(:typfak = '' or (:typfak is null)) then
      exception LISTYWYSD_ADDFAK_EXPT 'Brak okreslonego typu dok. VAT';
    select min(ref)
    from NAGFAK
    where KLIENT = :klient and (SPOSZAP = :sposzap or (:sposzap is null)) and AKCEPTACJA = 0 and TYP = :typfak and REJESTR = :srejfak and nagfak.listywysd=:doksped
    into :dokfak;
    execute procedure NAGFAK_FAKTURUJDOKMAG(:dokfak,:stanfak,:srejfak, :typfak,:operator,:dokmag,current_date,1,1,1, NULL,0,0,null) returning_values :dokfak;
  end
  /*fakturowanie uslugi*/
  select max(termin) from nagfak where listywysd= :doksped into :termin;


  if(usltryb > 0) then begin
    select KOSZTDOST, KOSZTDOSTBEZ, listywysd.SPOSDOST
    from LISTYWYSD where ref=:doksped into :kosztdost, :kosztdostbez, :sposd;
    if(:kosztdostbez is null) then kosztdostbez = 0;
    if(:kosztdost > 0 and :kosztdostbez = 0)then begin
      if(:usltryb = 2 or (:usltryb = 3)) then begin

        select max(ref) from NAGFAK where LISTYWYSD=:doksped and AKCEPTACJA = 0 and NAGFAK.typ=:stypfak into :dokfak;
        if(:dokfak is null and :usltryb = 3) then exception LISTYWYSD_ADDFAK_EXPT 'Brak niezzakceptowanej faktury, do której można by dopisać koszt dostawy';
      end
      if(:dokfak is null) then begin
        if(:sposplat is null) then
          select max(nagfak.sposzap) from NAGFAK where LISTYWYSD = :doksped into :sposplat;
        if(:sposplat is null) then
          select klienci.sposplat from KLIENCI where REF=:klient into :sposplat;
        execute procedure GEN_REF('NAGFAK') returning_values :dokfak;
        insert into NAGFAK(REF,STANOWISKO, REJESTR, TYP,DATA,TERMIN,KLIENT,sposzap, sposdost, LISTYWYSD)
        select :dokfak,:stanfak, :srejfak, :stypfak, current_date,:termin,LISTYWYSD.kosztdostplat, :sposplat, listywysd.sposdost, :doksped
        from LISTYWYSD left join KLIENCI on (LISTYWYSD.kosztdostplat = klienci.ref)
        where LISTYWYSD.ref=:doksped;

      end
      select NAGFAK.bn from NAGFAK where ref=:dokfak into :bn;
      select SPOSDOST.usluga from SPOSDOST where  ref=:sposd into :ktm;
      if(:bn = 'B') then begin
        if(:ktm = '' or (:ktm is null)) then exception LISTYWYSD_ADDFAK_EXPT 'Brak określonej usługi (i stawki VAT) dla zadanego sposobu dostawy';
        select vat from towary where ktm=:ktm into gr_vat;
        select stawka from vat where grupa=:gr_vat into :stawka;
        kosztdost = :kosztdost * :stawka;
      end
      if(:usltryb in(1,2)) then begin
        if(:ktm = '' or (:ktm is null)) then exception LISTYWYSD_ADDFAK_EXPT 'Brak określonej usługi dla zadanego sposobu dostawy';
        insert into POZFAK(DOKUMENT, KTM, WERSJA, ILOSC, CENACEN, OPK)
        values(:dokfak, :ktm, 0, 1, :kosztdost, 0);
      end else if(:usltryb = 3) then begin
        select max(POZFAK.wartbru) from POZFAK where DOKUMENT=:dokfak into :maxwartbru;
        select max(ref) from POZFAK where DOKUMENT=:dokfak and POZFAK.wartbru = :maxwartbru into :pozref;
        if(:pozref is null) then exception LISTYWYSD_ADDFAK_EXPT 'Brak pozycji towarowej do rozliczenia kosztów uslugi trnsportowej';
        select ILOSC from POZFAK where REF=:pozref into :ilosc;
        if(:ilosc > 0) then
          update POZFAK set CENACEN  = CENACEN + cast(:kosztdost/:ilosc as numeric(14,2)) where REF=:pozref;
      end
      update LISTYWYSD set KOSZTDOSTFAKTURA = :dokfak where LISTYWYSD.REF = :doksped and KOSZTDOSTFAKTURA is null;
    end
  end
end^
SET TERM ; ^
