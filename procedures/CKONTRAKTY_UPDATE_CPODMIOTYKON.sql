--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CKONTRAKTY_UPDATE_CPODMIOTYKON(
      CKONTRAKT integer)
   as
declare variable lista VARCHAR(255);
declare variable cpodm integer;
begin
  lista = ';';
  for
      select distinct KONTAKTY.CPODMIOT
      from KONTAKTY
      where (KONTAKTY.CKONTRAKT=:ckontrakt)
      into :cpodm
  do begin
  lista = :lista || cast(:cpodm as varchar(10)) || ';';
  end
  update CKONTRAKTY set CPODMIOTYKON = :lista where REF=:ckontrakt;
end^
SET TERM ; ^
