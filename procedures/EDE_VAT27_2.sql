--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_VAT27_2(
      EDECLARATIONREF EDECLARATIONS_ID)
  returns (
      K1 STRING255,
      K2 STRING255,
      K3 SMALLINT_ID,
      K4 STRING2,
      K5 YEARS_ID,
      K6 STRING255,
      K7 SMALLINT_ID,
      K8 SMALLINT_ID,
      K9 STRING255,
      K10 NUMERIC_14_2,
      K11 NUMERIC_14_2,
      K12 STRING255,
      K13 STRING255,
      K14 STRING255,
      K15 STRING10)
   as
declare variable edecpos_pvalue type of column edeclpos.pvalue;
declare variable edecpos_fieldsymbol type of column edeclpos.fieldsymbol;
declare variable filed_prefix char_1;
begin
/*TS: FK - pobiera dane dla wydruku na podstawie danych wskazanej e-Deklaracji.
  Zwrocone zostana tylko czesci stale wydruku (dla sekcji, ktore listuja pozycje,
  gdzie ich liczba jest zmienna, dane generuje osobna procedura z dopiskiem _POZ).

  > EDECLARATIONREF - numer e-Deklaracji, ktora chcemy wydrukowac.
*/

  /* Prefix nazw pol e-Deklaracji */
  filed_prefix = 'K';

  select distinct e.refid, e.status
    from edeclarations e
    where e.ref = :edeclarationref
  into :K2, :K3;

  for
    select ep.fieldsymbol, ep.pvalue
      from edeclarations e
        join edeclpos ep on (e.ref = ep.edeclaration)
      where e.ref = :edeclarationref
      into :edecpos_fieldsymbol, :edecpos_pvalue
  do begin
    if(:edecpos_fieldsymbol = :filed_prefix||'1') then K1 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'4') then K4 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'5') then K5 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'6') then K6 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'7') then K7 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'8') then K8 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'9') then K9 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'10') then K10 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'11') then K11 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'12') then K12 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'13') then K13 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'14') then K14 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'15') then K15 = :edecpos_pvalue;
  end
  suspend;
end^
SET TERM ; ^
