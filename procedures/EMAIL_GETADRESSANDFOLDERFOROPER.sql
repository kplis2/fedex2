--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAIL_GETADRESSANDFOLDERFOROPER(
      OPERATOR integer,
      PFOLDER integer = 0)
  returns (
      EMAILADRESS varchar(255) CHARACTER SET UTF8                           ,
      FOLDER integer,
      MAILBOX integer,
      MANYFOLDERS smallint)
   as
declare variable CNT integer;
declare variable TMP_EMAILADRESS varchar(255);
begin

  manyfolders = 0;
  -- ustalenie adresu email nadawcy  z tabeli oepratora
  select coalesce(operator.email,''), operator.emaileditfolder from operator where operator.ref = :operator
    into :emailadress, :folder;
  if(emailadress = '') then
    execute procedure get_config('INFOMAIL',1) returning_values :emailadress;
  tmp_emailadress = :emailadress;
  --foledr jest narzucony z góry
  if(:pfolder > 0) then
    folder = :pfolder;
  if(folder is null) then
  begin
    -- ustalenie folderu do zapisu nowego maila
    select count(deffold.ref) from deffold
      where deffold.rola = 2 and deffold.domyslny > 0 and deffold.operedit like '%;' || :operator || ';%'
    into :cnt;

    if(cnt > 0) then
    begin
      if(cnt > 1) then manyfolders = 1;
      else begin
        select deffold.ref, coalesce(mailboxes.email, :emailadress), mailboxes.ref from deffold
        left join mailboxes on (deffold.mailbox = mailboxes.ref)
          where deffold.rola = 2 and deffold.domyslny > 0 and deffold.operedit like '%;' || :operator || ';%'
        into :folder, :emailadress, :mailbox;
      end
    end

  end else
      --folder jest okrelony
      select DEFFOLD.mailbox, coalesce(MAILBOXES.email, :emailadress)
        from DEFFOLD join MAILBOXES on (MAILBOXES.REF = DEFFOLD.mailbox)
        where DEFFOLD.REF = :folder and DEFFOLD.mailbox > 0
      into :mailbox, :emailadress;
  if(folder > 0) then begin
    --weryfikacja czy folder jest poprawny
    if(not exists(select ref from DEFFOLD where ref=:folder and DEFFOLD.rola = 2)) then
      exception universal 'Wybrany folder wiadomosci nie jest do zapisu';
    if(not exists(select ref from DEFFOLD where ref=:folder and deffold.operedit like '%;' || :operator || ';%')) then
      exception universal 'Operator nie ma prawa edycji w wybranym folderze';
  end
  if(emailadress = '') then
    emailadress = :tmp_emailadress;
end^
SET TERM ; ^
