--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_DATASECUR(
      PERSON integer)
  returns (
      REGDATE timestamp,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      OPERATOR varchar(60) CHARACTER SET UTF8                           )
   as
declare variable aktuoperator integer;
begin
    for
      select DS.regdate, DS.descript, O.nazwa
        from epersdatasecur DS join persons P on (DS.person = P.ref)
          join operator O on (DS.operator = O.ref)
        where P.ref = :person
        order by DS.regdate
        into :regdate, :descript, :operator
    do begin
      suspend;
    end

    execute procedure get_global_param ('AKTUOPERATOR')
      returning_values aktuoperator;
    descript = 'Udzielenie informacji o zakresie przetwarzanych danych (wydruk)';
    if (not exists (select ref
                      from epersdatasecur
                      where person = :person and kod = 3 and descript = :descript
                        and operator = :aktuoperator and regdate = :regdate)) then
    begin
      insert into epersdatasecur (person, kod, descript, operator)
        values (:person, 3, :descript, :aktuoperator);
    end

end^
SET TERM ; ^
