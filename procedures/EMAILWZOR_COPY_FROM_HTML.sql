--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWZOR_COPY_FROM_HTML(
      EMAILWZORSYMBOL varchar(40) CHARACTER SET UTF8                           )
   as
declare variable EMAILWIADREF integer;
declare variable OD varchar(512);
declare variable dokogo varchar(2048);
declare variable tytul string1024;
declare variable tresc memo5000;
begin
  --przenoszenie tresci z EMAILWIAD do EMAILWZOR po redakcji
  select EMAILWIAD
   from EMAILWZOR where symbol=:emailwzorsymbol
  into :emailwiadref;

  if(:emailwiadref is null or (:emailwiadref = 0) )then
    exit;

  select OD, DOKOGO, TYTUL, TRESC
   from EMAILWIAD where ref=:emailwiadref
   into :od, :dokogo, :tytul, :tresc;

  update EMAILWZOR set od = :od, dokogo = :dokogo,
    tytul = :tytul, tresc = :tresc
    where SYMBOL=:emailwzorsymbol;
end^
SET TERM ; ^
