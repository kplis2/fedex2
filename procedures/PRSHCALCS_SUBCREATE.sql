--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHCALCS_SUBCREATE(
      SHEET integer,
      PRSHCALC integer,
      PARENTCALCCOL integer,
      METHOD integer,
      PARENTMETHODCOL integer)
   as
declare variable expr varchar(1024);
declare variable calckind smallint;
declare variable calcfilter varchar(255);
declare variable prcalccol integer;
declare variable prmethodcol integer;
declare variable stmt varchar(1024);
declare variable prshmatref integer;
declare variable wersjaref integer;
declare variable descript varchar(255);
declare variable nazwat varchar(255);
declare variable nazwaw varchar(255);
declare variable nrwersji integer;
declare variable prshoperref integer;
declare variable prshtoolref integer;
declare variable complex smallint;
declare variable opertype smallint;
declare variable isalt smallint;
declare variable calculated smallint;
declare variable prcolumn integer;
declare variable calctoparent smallint;
declare variable color smallint;
begin
  -- pobierz definicje rubryk na zadanym poziomie
  for select ref, expr, calckind, calcfilter, descript, prcolumn, calctoparent, color
    from prmethodcols where method = :method
    and (parent = :parentmethodcol or (parent is null and :parentmethodcol is null))
    order by number
    into :prmethodcol, :expr, :calckind, :calcfilter, :descript, :prcolumn, :calctoparent, :color
  do begin
    if(:calcfilter is null) then calcfilter = '';
    if(:calckind=0)  then begin
      -- dodaj zwykla rubryke do kalkulacji
      execute procedure GEN_REF('PRCALCCOLS') returning_values :prcalccol;
      insert into PRCALCCOLS(REF,PARENT,CALC,EXPR,KEY1,KEY2,PREFIX,DESCRIPT, PRCOLUMN, calctoparent, color)
      values(:prcalccol, :parentcalccol, :prshcalc, :expr, :sheet, :prcalccol, 'XP_', :descript, :prcolumn, :calctoparent, iif(coalesce(:color,0) = 0, null, :color -1));

      -- wylicz rekurencyjnie podrzedne
      execute procedure PRSHCALCS_SUBCREATE(:sheet,:prshcalc,:prcalccol,:method,:prmethodcol);

    end else if (:calckind=1) then begin
      -- select po materialach karty technologicznej
      stmt = 'select REF, WERSJAREF from PRSHMAT where PRSHMAT.SHEET='||:sheet;
      if(:calcfilter<>'') then stmt = :stmt || ' and ' || :calcfilter;
      for execute statement :stmt into :prshmatref,:wersjaref
      do begin
        select nazwat,nazwa,nrwersji from wersje where ref=:wersjaref into :nazwat,:nazwaw,:nrwersji;
        if(:nrwersji<>0) then nazwat = :nazwat || ' (' || :nazwaw || ')';
        -- dodaj rubryke materialowa do kalkulacji
        execute procedure GEN_REF('PRCALCCOLS') returning_values :prcalccol;
        insert into PRCALCCOLS(REF,PARENT,CALC,EXPR,KEY1,KEY2,PREFIX,DESCRIPT,FROMPRSHMAT, PRCOLUMN, calctoparent)
        values(:prcalccol, :parentcalccol, :prshcalc, :expr, :prshmatref, :prcalccol, 'XP_',:descript||': '||:nazwat, :prshmatref, null, 1);
      end

    end else if (:calckind=2) then begin
      -- select po operacjach karty technologicznej
      stmt = 'select REF, DESCRIPT, COMPLEX, OPERTYPE, CALCULATED from PRSHOPERS where PRSHOPERS.SHEET='||:sheet;
      if(:calcfilter<>'') then stmt = :stmt || ' and ' || :calcfilter;
      for execute statement :stmt into :prshoperref,:nazwat,:complex,:opertype, :calculated
      do begin
        if(:complex=1 and :opertype=1) then isalt = 1;
        else isalt = 0;
        -- dodaj rubryke operacji do kalkulacji
        execute procedure GEN_REF('PRCALCCOLS') returning_values :prcalccol;
        if(:calculated=1) then begin
          insert into PRCALCCOLS(REF,PARENT,CALC,EXPR,KEY1,KEY2,PREFIX,DESCRIPT,FROMPRSHOPER, ISALT, PRCOLUMN, calctoparent)
          values(:prcalccol, :parentcalccol, :prshcalc, :expr, :prshoperref, :prcalccol, 'XP_',:descript||': '||:nazwat, :prshoperref, :isalt, :prcolumn, 1);
        end else begin
          insert into PRCALCCOLS(REF,PARENT,CALC,EXPR,KEY1,KEY2,PREFIX,DESCRIPT,FROMPRSHOPER, ISALT, PRCOLUMN, calctoparent)
          values(:prcalccol, :parentcalccol, :prshcalc, '', :prshoperref, :prcalccol, 'XP_',:descript||': '||:nazwat, :prshoperref, :isalt, :prcolumn,0);
        end
      end

    end else if (:calckind=3) then begin
      -- select po pomocach karty technologicznej
      stmt = 'select REF, DESCRIPT from PRSHTOOLS where PRSHTOOLS.SHEET='||:sheet;
      if(:calcfilter<>'') then stmt = :stmt || ' and ' || :calcfilter;
      for execute statement :stmt into :prshtoolref,:nazwat
      do begin
        -- dodaj rubryke dodatku do kalkulacji
        execute procedure GEN_REF('PRCALCCOLS') returning_values :prcalccol;
        insert into PRCALCCOLS(REF,PARENT,CALC,EXPR,KEY1,KEY2,PREFIX,DESCRIPT,FROMPRSHTOOL, PRCOLUMN, calctoparent)
        values(:prcalccol, :parentcalccol, :prshcalc, :expr, :prshtoolref, :prcalccol, 'XP_',:descript||': '||:nazwat, :prshtoolref, :prcolumn, 0);
      end
    end
  end

end^
SET TERM ; ^
