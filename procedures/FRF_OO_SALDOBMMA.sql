--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_OO_SALDOBMMA(
      COMPANY integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      ACCOUNTS ACCOUNT_ID)
  returns (
      AMOUNT numeric(15,2))
   as
declare variable yearid integer;
declare variable debit numeric(15,2);
declare variable credit numeric(15,2);
declare variable accounting_ref integer;
declare variable saldo_credit numeric(14,2);
declare variable country_currency varchar(3);
declare variable balancetype smallint;
declare variable saccount account_id;
begin
  select yearid from bkperiods where id = :period and company = :company
    into :yearid;
  accounts = replace(accounts,  '?',  '_') || '%';
  amount = 0;
  execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
    returning_values country_currency;

  for
    select A.ref, B.saldotype, A.account
      from accounting A
        join bkaccounts B on (A.bkaccount = B.ref)
      where A.currency = :country_currency and A.yearid = :yearid
        and A.account like :accounts
      into :accounting_ref, :balancetype, :saccount
  do begin
    select sum(debit), sum(credit) from turnovers
      where accounting = :accounting_ref and period = :period and company = :company
      into :debit, :credit;

    if (debit is null) then debit = 0;
    if (credit is null) then credit = 0;

    saldo_credit = 0;
    if (balancetype = 2) then
      saldo_credit = credit - debit;
    else if (balancetype = 1 and abs(credit) > abs(debit)) then
      saldo_credit = credit - debit;

    amount = amount + saldo_credit;
  end
  amount = coalesce(amount,0);
  suspend;
end^
SET TERM ; ^
