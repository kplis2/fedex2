--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TOWORDS_NASCIE(
      P varchar(1024) CHARACTER SET UTF8                           ,
      A integer,
      LANGUAGE integer)
  returns (
      POUT varchar(1024) CHARACTER SET UTF8                           )
   as
begin
  if (p is null) then p = '';
  if (a = 10) then begin
    if (language = 0) then p = :p ||'dziesięć ';
    else if (language = 1) then p = :p ||'ten ';
    else if (language = 2) then p = :p ||'zehn ';
  end else if (a = 11) then begin
    if (language = 0) then p = :p ||'jede';
    else if (language = 1) then p = :p ||'eleven ';
    else if (language = 2) then p = :p ||'elf ';
  end else if (a = 12) then begin
    if (language = 0) then p = :p ||'dwa';
    else if (language = 1) then p = :p ||'twelve ';
    else if (language = 2) then p = :p ||'zwolf ';
  end else if (a = 13) then begin
    if (language = 0) then p = :p ||'trzy';
    else if (language = 1) then p = :p ||'thir ';
    else if (language = 2) then p = :p ||'drei ';
  end else if (a = 14) then begin
    if (language = 0) then p = :p ||'czter';
    else if (language = 1) then p = :p ||'four';
    else if (language = 2) then p = :p ||'vier';
  end else if (a = 15) then begin
    if (language = 0) then p = :p ||'pięt';
    else if (language = 1) then p = :p ||'fif';
    else if (language = 2) then p = :p ||'funf';
  end else if (a = 16) then begin
    if (language = 0) then p = :p ||'szes';
    else if (language = 1) then p = :p ||'six';
    else if (language = 2) then p = :p ||'sechs';
  end else if (a = 17) then begin
    if (language = 0) then p = :p ||'siedem';
    else if (language = 1) then p = :p ||'seven';
    else if (language = 2) then p = :p ||'sieb';
  end else if (a = 18) then begin
    if (language = 0) then p = :p ||'osiem';
    else if (language = 1) then p = :p ||'eight';
    else if (language = 2) then p = :p ||'acht';
  end else if (a = 19) then begin
    if (language = 0) then p = :p ||'dziewięt';
    else if (language = 1) then p = :p ||'nine';
    else if (language = 2) then p = :p ||'neun';
  end
  if (a <> 10 and language = 0) then p = :p || 'naście ';
  else if (a >= 13 and language = 1) then p = :p ||'teen ';
  else if (a >= 13 and language = 2) then p = :p ||'zehn';
  pout = p;
  suspend;
end^
SET TERM ; ^
