--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXP2PFRON_INF_D_P(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      COMPANY integer)
  returns (
      PFRON varchar(20) CHARACTER SET UTF8                           ,
      PERSON integer,
      PESEL varchar(11) CHARACTER SET UTF8                           ,
      NIP varchar(15) CHARACTER SET UTF8                           ,
      SNAME varchar(30) CHARACTER SET UTF8                           ,
      FNAME varchar(60) CHARACTER SET UTF8                           ,
      MIDDLENAME varchar(30) CHARACTER SET UTF8                           ,
      WOJEWODZTWO varchar(6) CHARACTER SET UTF8                           ,
      POWIAT varchar(255) CHARACTER SET UTF8                           ,
      GMINA varchar(255) CHARACTER SET UTF8                           ,
      CITY varchar(30) CHARACTER SET UTF8                           ,
      POSTCODE varchar(10) CHARACTER SET UTF8                           ,
      POST varchar(30) CHARACTER SET UTF8                           ,
      STREET varchar(30) CHARACTER SET UTF8                           ,
      HOUSENO varchar(20) CHARACTER SET UTF8                           ,
      LOCALNO varchar(20) CHARACTER SET UTF8                           ,
      PSYCHO smallint,
      POMNIEJSZENIA numeric(14,2),
      WYNIK_ROZW_UMOWY smallint,
      WZROST_NETTO_ZATR smallint,
      NUMBER integer,
      ZNACZNY numeric(14,3),
      UMIARKOWANY numeric(14,3),
      LEKKI numeric(14,3),
      OKRES_ZATR_OD_DNIA date,
      P42 numeric(14,2),
      KOSZTY_PLACY numeric(14,2),
      MINIMALNAKRAJOWA numeric(14,2))
   as
declare variable FROMDATE date;
declare variable TODATE date;
declare variable EMPLOYEE integer;
declare variable WORKDIM float;
declare variable EFROMDATE date;
declare variable ETODATE date;
declare variable IFROMDATE date;
declare variable ITODATE date;
declare variable DAYS integer;
declare variable INVALIDITY varchar(20);
declare variable EMPLDAYS integer;
declare variable BRUTTO numeric(14,2);
declare variable DO_WYPLATY numeric(14,2);
declare variable PAYROLL integer;
declare variable PAYDAY date;
declare variable ZATR_DZIS numeric(14,2);
declare variable ZATR_ROK numeric(14,2);
declare variable EMPLMINUSYEAR date;
begin

  psycho = 0;
  pomniejszenia = 0;

  execute procedure period2dates(:period)
    returning_values fromdate, todate;

  execute procedure get_pval(:fromdate, :company, 'MINIMALNAKRAJOWA')
    returning_values :minimalnakrajowa;

  days = todate - fromdate + 1;
  number=0;
  for
    select distinct p.ref, p.pesel, p.nip, p.sname, p.fname, p.middlename, w.guscode,
      pow.descript, g.descript, a.city, a.postcode, a.post, a.street, a.houseno, a.localno
    from persons p
      join eperscompany s on s.person = p.ref
      join ezusdata z on z.person = p.ref
      join edictzuscodes c on c.ref = z.invalidity
      left join epersaddr a on a.person = p.ref
      left join cpwoj16m w on w.ref = a.cpwoj16m
      left join edictguscodes pow on pow.ref = a.powiatid
      left join edictguscodes g on g.ref = a.communityid
    where c.dicttype = 2
      and c.code <> 0
      and z.invalidityfrom <= :todate
      and (z.invalidityto >= :fromdate or z.invalidityto is null)
      and (a.addrtype = 0 or a.addrtype is null)
      and s.company = :company
    into :person, :pesel, :nip, :sname, :fname, :middlename, :wojewodztwo,
      :powiat, :gmina, :city, :postcode, :post, :street, :houseno, :localno
  do begin
    execute procedure get_config('INFOPFRON',1) returning_values :pfron;

    --zostawie bo sie moze przydac w przyszlosci
    /*if (data_change = 0) then
    begin
      middlename = ''; wojewodztwo = ''; powiat = ''; gmina = ''; city = '';
      postcode = ''; post = ''; street = ''; houseno = ''; localno = '';
      iscity = null; workpost = ''; education = ''; sex = null;
    end */

    znaczny=0;
    umiarkowany=0;
    lekki=0;
    p42=0;
    koszty_placy=0;
    number = number + 1;
    for
      select e.ref, em.workdim, em.fromdate, em.todate, ep.ref, ep.payday
        from employees e
          join employment em on em.employee = e.ref
          left join eprempl p on p.employee = e.ref
          left join epayrolls ep on ep.ref = p.epayroll
        where e.person = :person
          and em.fromdate <= :todate
          and (em.todate >= :fromdate or em.todate is null)
          and ep.cper = :period
          and ep.empltype = 1
        into :employee, :workdim, :efromdate, :etodate, :payroll, :payday
    do begin

      if (etodate is null or etodate > todate) then
        etodate = todate;
      if (efromdate < fromdate) then
        efromdate = fromdate;

      for
        select c.code, z.invalidityfrom, z.invalidityto
          from ezusdata z
            join edictzuscodes c on c.ref = z.invalidity
          where z.person = :person
            and z.invalidityfrom <= :etodate
            and (z.invalidityto >= :efromdate or z.invalidityto is null)
            and c.dicttype = 2
          into :invalidity, :ifromdate, :itodate
      do begin
        if (itodate is null or itodate > etodate) then
          itodate = etodate;
        if (ifromdate < efromdate) then
          ifromdate = efromdate;

        empldays = itodate - ifromdate + 1;

        if (invalidity = '3') then
          znaczny = znaczny + workdim * empldays/days;
        else if (invalidity = '2') then
          umiarkowany = umiarkowany + workdim * empldays/days;
        else if (invalidity = '1') then
          lekki = lekki + workdim * empldays/days;
      end

      select ret from ef_col(:employee,:payroll,'EF_',4000) into :brutto;
      select ret from ef_col(:employee,:payroll,'EF_',9000) into :do_wyplaty;
      if (payday >= current_date) then
        do_wyplaty = 0;
      p42 = p42 + brutto;
      koszty_placy = koszty_placy + do_wyplaty;
    end

    select first 1 m.fromdate, c.dischargecontract
      from employees e
        join employment m on m.employee = e.ref
        join emplcontracts c on c.ref = m.emplcontract
      where e.person = :person
      order by m.fromdate
      into :okres_zatr_od_dnia, :wynik_rozw_umowy;

    select sum(avg_zatr) from rpt_avg_employment(:okres_zatr_od_dnia, :okres_zatr_od_dnia, :company) into :zatr_dzis;
    execute procedure date_add(okres_zatr_od_dnia,-1,0,0) returning_values :emplminusyear;
    select sum(avg_zatr) from rpt_avg_employment(:emplminusyear, :okres_zatr_od_dnia, :company) into :zatr_rok;
    wzrost_netto_zatr = 0;
    if(zatr_dzis > zatr_rok) then wzrost_netto_zatr = 1;
    suspend;
  end

end^
SET TERM ; ^
