--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYS_DOKSPEDUZUP(
      DOKSPED integer)
  returns (
      STATUS integer)
   as
declare variable dokum integer;
declare variable typ char(1);
declare variable pozref integer;
declare variable ilosc numeric(14,4);
declare variable ilosco numeric(14,4);
declare variable jedno integer;
declare variable waga numeric(14,3);
declare variable oldilosco numeric(14,4);
declare variable oldilosc numeric(14,4);
declare variable iloscwys numeric(14,4);
declare variable wersjaref integer;
declare variable cnt integer;
begin
  status = 0;
  select LISTYWYSD.typ, LISTYWYSD.refdok from LISTYWYSD where ref = :doksped into :typ, :dokum;
  if(:typ = 'M') then begin
    for select REF, ILOSC, ILOSCO, JEDNO, ilosclwys, waga, wersjaref
    from DOKUMPOZ
    where DOKUMENT = :dokum
    into :pozref, :ilosc, :ilosco, :jedno, :iloscwys, :waga, :wersjaref
    do begin
        if(:iloscwys is null) then iloscwys = 0;
        if(:iloscwys  >0) then begin
          oldilosco = 0; oldilosc = 0;
          select sum(ilosco), sum(ilosc) from LISTYWYSDPOZ where DOKPOZ = :pozref into :oldilosco, :oldilosc;
          ilosc = :ilosc - oldilosc;
          ilosco = :ilosco - :oldilosco;
          waga = 0;
        end
        cnt = null;
        select count(*) from LISTYWYSDPOZ where DOKUMENT = :doksped and DOKPOZ = :pozref into :cnt;
        if(:cnt is null) then cnt = 0;
        if(:cnt = 0) then
          insert into LISTYWYSDPOZ(DOKUMENT, DOKPOZ, WERSJAREF, ILOSCO, JEDNO, ILOSC, ILOSCMAX, WAGA)
          values (:doksped, :pozref, :wersjaref, :ilosco, :jedno, :ilosc, :ilosc, :waga);
        else begin
          if(:ilosco is null) then ilosco = 0;
          if(:ilosc is null) then ilosc = 0;
          update LISTYWYSDPOZ set ILOSCO = ILOSCO + :ilosco, ILOSC = ILOSC + :ilosc where DOKUMENT = :doksped and DOKPOZ = :pozref;
        end
    end
  end
  if(:typ = 'Z') then begin
    for select REF, ILOSC, ILOSCO, JEDNO, waga, wersjaref
    from POZZAM
    where ZAMOWIENIE = :dokum
    into :pozref, :ilosc, :ilosco, :jedno, :waga, :wersjaref
    do begin
        oldilosco = 0; oldilosc = 0;
        select sum(ilosco), sum(ilosc) from LISTYWYSDPOZ where POZZAM = :pozref into :oldilosco, :oldilosc;
        ilosc = :ilosc - oldilosc;
        ilosco = :ilosco - :oldilosco;
        waga = 0;
        cnt = null;
        select count(*) from LISTYWYSDPOZ where DOKUMENT = :doksped and POZZAM = :pozref into :cnt;
        if(:cnt is null) then cnt = 0;
        if(:cnt = 0) then
          insert into LISTYWYSDPOZ(DOKUMENT, POZZAM, WERSJAREF, ILOSCO, JEDNO, ILOSC, ILOSCMAX, WAGA)
          values (:doksped, :pozref, :wersjaref, :ilosco, :jedno, :ilosc, :ilosc, :waga);
        else begin
          if(:ilosco is null) then ilosco = 0;
          if(:ilosc is null) then ilosc = 0;
          update LISTYWYSDPOZ set ILOSCO = ILOSCO + :ilosco, ILOSC = ILOSC + :ilosc where DOKUMENT = :doksped and POZZAM = :pozref;
        end
    end
  end
end^
SET TERM ; ^
