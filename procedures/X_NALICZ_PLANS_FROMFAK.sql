--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_NALICZ_PLANS_FROMFAK(
      PLANS integer,
      PLANSCOL integer,
      PLANSROW integer)
  returns (
      VAL numeric(14,4))
   as
declare variable TVAL NUMERIC(14,4);
declare variable TKTM VARCHAR(20);
declare variable dataod timestamp;
declare variable datado timestamp;
begin
  val = 0;
  tval = 0;
  tktm = '';
  select key1
    from plansrow pr
    where pr.plans = :plans and pr.ref = :plansrow
  into :tktm;
  select pc.bdata, pc.ldata
    from planscol pc
    where pc.plans = :plans and pc.ref = :planscol
  into :dataod, :datado;

--naliczanie z pozycji faktur
  val = 0;
  select SUM(POZFAK.WARTNETZL - POZFAK.PWARTNETZL)
    from POZFAK
    join NAGFAK on (NAGFAK.REF=POZFAK.DOKUMENT)
--    left join TOWARY on (POZFAK.KTM = TOWARY.KTM)
    where (NAGFAK.ZAKUP = 0)
      and (strmulticmp(';'||NAGFAK.AKCEPTACJA||';',';1;8;')>0)
      and (NAGFAK.ANULOWANIE = 0) and (NAGFAK.NIEOBROT < 2)
      and pozfak.ktm = :tktm and (NAGFAK.DATA >= :DATAOD)
      and (NAGFAK.DATA <= :datado)
  into :val;
  if (val is null) then val = 0;
  suspend;
end^
SET TERM ; ^
