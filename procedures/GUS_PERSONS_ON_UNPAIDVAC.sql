--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GUS_PERSONS_ON_UNPAIDVAC(
      FROMDATE date,
      TODATE date,
      COMPANY integer)
  returns (
      PERSON integer)
   as
declare variable ABSENCE integer;
declare variable LAST_EMPL integer;
declare variable AFROMDATE date;
declare variable ATODATE date;
declare variable Y integer;
declare variable M integer;
declare variable D integer;
declare variable EMPLOYEE integer;
declare variable SUSP_PERSON integer;
begin
/*MWr Personel: Funkcja zwraca osoby, ktore przebywaja na urlopie wychowawczym
  lub bezplatnym, nieprzerwanie, powyzej 3 miesiecy. Funkcja wykorzystywana do
  sprawozdania GUS Z-05. Stan badania przeprowadzany jest na dzien TODATE*/

  last_empl = 0;
  susp_person = 0;
  for
    select a.ref, e.person, a.employee
      from persons p
        join employees e on (p.ref = e.person)
        join employment m on (m.employee = e.ref)
        join eabsences a on (a.employee = e.ref and a.ecolumn in (260,300))
      where e.company = :company
        and a.todate > :fromdate and a.fromdate <= :todate
        and m.fromdate <= :todate and (m.todate >= :todate or m.todate is null)
      order by e.person, a.employee, a.fromdate
    into :absence, :person, :employee
  do begin
    if (last_empl <> employee and susp_person <> person) then
    begin
      select afromdate, atodate
        from e_get_whole_eabsperiod(:absence,null,:todate,null,null,null,';260;300;')
        into :afromdate, :atodate;

      execute procedure get_ymd(afromdate, atodate) returning_values y,m,d;

      if (m >= 3 or y > 0) then begin
        suspend;
        susp_person = person;
      end
    end
    last_empl = employee;
  end
end^
SET TERM ; ^
