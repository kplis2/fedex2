--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_CHECK_VERS_IN_MWSORD(
      VERS WERSJE_ID,
      MWSORD MWSORDS_ID,
      SERIA STRING255)
  returns (
      MWSACT MWSACTS_ID,
      PARTIA DATE_ID,
      SERIA_OUT STRING255,
      SERIA_REF INTEGER_ID)
   as
begin

   if (seria ='') then seria = null;

   for select distinct a.ref, a.x_partia, x.serialno, x.ref
         from mwsacts a
           left join x_mws_serie x on (x.ref=a.x_serial_no)
         where a.mwsord=:mwsord
           and a.vers=:vers
           and coalesce(x.serialno,:seria) is not distinct from coalesce(:seria,x.serialno)
           and a.status not in (6,5,3)
   into :mwsact, :partia, :seria_out, :seria_ref
   do suspend;
end^
SET TERM ; ^
