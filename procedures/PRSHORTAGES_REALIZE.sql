--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHORTAGES_REALIZE(
      DATA varchar(10) CHARACTER SET UTF8                           )
  returns (
      SMESSAGE varchar(255) CHARACTER SET UTF8                           )
   as
declare variable ktm varchar(80);
declare variable warehouse varchar(3);
declare variable defdokum varchar(3);
declare variable amount numeric(14,4);
declare variable prshortagetype varchar(20);
declare variable backasrecource smallint;
declare variable guide integer;
declare variable whdoc varchar(3);
declare variable defdokumdoc varchar(3);
declare variable dokmagref integer;
declare variable dokpozref integer;
declare variable ent varchar(10);
declare variable dokumnagsymbol varchar(20);
declare variable price numeric(14,2);
declare variable prshortages integer;
declare variable datadoc timestamp;
declare variable okres varchar(6);
declare variable wersjaref integer;
declare variable prschedoper integer;
declare variable prnagzam integer;
declare variable prpozzam integer;
declare variable prgruparozl integer;
declare variable newref integer;
declare variable doccnt integer;
declare variable dodoc smallint;
begin
  ent = '
';
  smessage = 'Wygenerowane dokumenty: '||ent;
  dodoc = 0;
  if (coalesce(:data,'') = '') then
    datadoc = current_date;
  else
    datadoc = cast (:data as date);
  execute procedure PRGRUPAROZL_ID returning_values prgruparozl;
  for   
    select pt.symbol ,psh.ktm,  psh.amountreal, pshd.warehouse, pshd.defdokum,
        pt.backasresourse, psh.prschedguide, psh.ref, psh.wersjaref,
        psh.prschedoper, psh.prnagzam, psh.prpozzam
      from prshortages psh
        left join prshortagestypes pt on (pt.symbol = psh.prshortagestype)
        left join prshortagesprdeparts pshd on (pshd.prshortagetype = psh.prshortagestype and pshd.prdepart = psh.prdepart)
      where pshd.warehouse is not null
        and pshd.defdokum is not null
        and psh.amountreal > 0
        and pt.noamountinfluence = 0
      order by pshd.warehouse, pshd.defdokum
      into prshortagetype, ktm, amount, warehouse, defdokum,
        backasrecource, guide, prshortages, wersjaref,
        prschedoper, prnagzam, prpozzam
  do
  begin
    if (warehouse is null or warehouse = '') then
      select g.warehouse
        from prschedguides g
        where g.ref = :guide
        into warehouse;
    if (warehouse is null or warehouse = '') then
      exception dokumpoz_prshortages 'Nie wskazano magazynu dla braków';
    if (:amount <= 0 ) then
      exception dokumpoz_prshortages 'Ilosc mniejsza od 0';
    if (dodoc = 0) then
      dodoc = 1;
    execute procedure PRSCHEDGUIDEDETS_ADD(prnagzam,prpozzam,guide,
        null,prschedoper,null,ktm,wersjaref,0,
        warehouse,defdokum,amount,0,0,0,prshortages,1,'DOKUMNAG',0,null)
      returning_values newref;
    update prschedguidedets g set g.prgruparozl = :prgruparozl where g.ref = :newref;
    update prshortages p set p.amountreal = 0
        where ref = :prshortages;
  end
  if (dodoc = 1) then
  begin
    execute PROCEDURE PR_GEN_DOC_PRGRUPAROZL (:prgruparozl,:datadoc,0,1,0)
      returning_values doccnt;
  end
  -- akceptacja ostatniego dokumentu magazynowego
  if (doccnt > 0) then
  begin
    for
      select d.symbol
        from dokumnag d
        where d.prgrupaprod = :prgruparozl
        into dokumnagsymbol
    do begin
      if(coalesce(char_length(SMESSAGE),0)<220) then -- [DG] XXX ZG119346
        SMESSAGE = SMESSAGE||dokumnagsymbol||'; ';
    end
  end
  suspend;
end^
SET TERM ; ^
