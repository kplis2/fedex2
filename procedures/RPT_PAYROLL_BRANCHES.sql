--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PAYROLL_BRANCHES(
      FROMCPER char(6) CHARACTER SET UTF8                           ,
      TOCPER char(6) CHARACTER SET UTF8                           ,
      FROMTPER char(6) CHARACTER SET UTF8                           ,
      TOTPER char(6) CHARACTER SET UTF8                           ,
      FROMIPER char(6) CHARACTER SET UTF8                           ,
      TOIPER char(6) CHARACTER SET UTF8                           ,
      BRANCH varchar(10) CHARACTER SET UTF8                           ,
      DEPT varchar(10) CHARACTER SET UTF8                           ,
      EMPLTYPE smallint,
      CURRENTCOMPANY integer)
  returns (
      PAGENO integer,
      B_SYMBOL varchar(10) CHARACTER SET UTF8                           ,
      B_NAME varchar(255) CHARACTER SET UTF8                           ,
      D_SYMBOL varchar(10) CHARACTER SET UTF8                           ,
      D_NAME varchar(60) CHARACTER SET UTF8                           ,
      LINES integer)
   as
declare variable onpage integer;
declare variable col1 smallint = 0;
declare variable col2 smallint = 0;
declare variable col3 smallint = 0;
declare variable col4 smallint = 0;
declare variable col5 smallint = 0;
declare variable maxcol smallint = 0;
begin
  pageno = 1;
  onpage = 30;
  lines = 0;
  if (fromcper = '') then
    fromcper = null;
  if (tocper = '') then
    tocper = null;
  if (fromtper = '') then
    fromtper = null;
  if (totper = '') then
    totper = null;
  if (fromiper = '') then
    fromiper = null;
  if (toiper = '') then
    toiper = null;
  if (EmplType < 1) then EmplType = null;
  if (branch = '') then branch = null;
  if (dept = '') then dept = null;

  for
    select B.oddzial, B.nazwa, D.symbol, D.NAME
      from ODDZIALY B
        left join DEPARTMENTS D on 1=1
      where (B.ODDZIAL=:branch or :branch is null)
        and (D.SYMBOL=:dept or :dept is null)
        and (D.company = :currentcompany)
      order by B.oddzial, D.symbol
      into :b_symbol, :b_name, :d_symbol, :d_name
  do begin
    maxcol = 0;
    select count(distinct case when P.ecolumn < 1000 then p.ecolumn end),
      count(distinct case when P.ecolumn >= 1000 and p.ecolumn < 4000 then p.ecolumn end),
      count(distinct case when P.ecolumn > 4000 and p.ecolumn < 5900 then p.ecolumn end),
      count(distinct case when P.ecolumn > 5900 and p.ecolumn < 8000 then p.ecolumn end),
      count(distinct case when P.ecolumn > 8000 and p.ecolumn < 9050 then p.ecolumn end)
    from epayrolls ep
      join eprpos p on ep.ref = p.payroll
      join employees e on e.ref = p.employee
      join ecolumns c on c.number = p.ecolumn
      join eprempl r on ep.ref = r.epayroll and e.ref = r.employee
    where C.repsum > 0
      and ep.empltype = coalesce(:empltype,ep.empltype)
      and r.branch = :b_symbol
      and r.department = :d_symbol
      and e.company = :currentcompany
      and (ep.cper >= :fromcper or :fromcper is null)
      and (ep.cper <= :tocper or :tocper is null)
      and (ep.iper >= :fromiper or :fromiper is null)
      and (ep.iper <= :toiper or :toiper is null)
      and (ep.tper >= :fromtper or :fromtper is null)
      and (ep.tper <= :totper or :totper is null)
    into :col1, :col2, :col3, :col4, :col5;

    if (col1 > maxcol) then
      maxcol = col1;
    if (col2 > maxcol) then
      maxcol = col2;
    if (col3 > maxcol) then
      maxcol = col3;
    if (col4 > maxcol) then
      maxcol = col4;
    if (col5 > maxcol) then
      maxcol = col5;

    lines = lines + 6;   --zwiekszam tez o 6 na nazwisko, podsumowania itd. (przy 5 potrafilo sie rozjechac)

    if (lines + maxcol > onpage) then
    begin
      pageno = pageno + 1;
     lines = maxcol;
    end else
      lines = lines + maxcol;
    suspend;
  end
end^
SET TERM ; ^
