--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAV_QUERYTYPES
  returns (
      REF varchar(255) CHARACTER SET UTF8                           ,
      PARENT varchar(255) CHARACTER SET UTF8                           ,
      NUMBER integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      HINT varchar(255) CHARACTER SET UTF8                           ,
      KINDSTR varchar(20) CHARACTER SET UTF8                           ,
      ACTIONID varchar(60) CHARACTER SET UTF8                           ,
      APPLICATIONS varchar(60) CHARACTER SET UTF8                           ,
      FORMODULES varchar(60) CHARACTER SET UTF8                           ,
      RIGHTS varchar(255) CHARACTER SET UTF8                           ,
      RIGHTSGROUP varchar(255) CHARACTER SET UTF8                           ,
      ISNAVBAR smallint,
      ISTOOLBAR smallint,
      ISWINDOW smallint,
      ISMAINMENU smallint,
      ITEMTYPE smallint,
      GROUPPROCEDURE varchar(60) CHARACTER SET UTF8                           ,
      CHILDRENCOUNT smallint,
      RIBBONTAB varchar(40) CHARACTER SET UTF8                           ,
      RIBBONGROUP varchar(40) CHARACTER SET UTF8                           ,
      NATIVETABLE varchar(40) CHARACTER SET UTF8                           ,
      POPUPMENU varchar(40) CHARACTER SET UTF8                           ,
      NATIVEFIELD varchar(40) CHARACTER SET UTF8                           ,
      NATIVEVALUE varchar(40) CHARACTER SET UTF8                           ,
      FORADMIN smallint)
   as
declare variable n integer;
declare variable qref integer;
declare variable qparent varchar(255);
begin
  n = 1;
  /* najpierw zwroc liste typow zapytan */
  for select REF, NAME, ICON, APPLICATION, RIGHTS, RIGHTSGROUP
  from QUERYTYPES
  order by SYMBOL
  into :QREF, :NAME, :KINDSTR, :APPLICATIONS, :RIGHTS, :RIGHTSGROUP
  do begin
    parent = NULL;
    hint = '';
    actionid = '';
    formodules = '';
    isnavbar = 0;
    istoolbar = 1;
    iswindow = 0;
    ismainmenu = 0;
    itemtype = 0;
    childrencount = 1;
    ribbontab = '';
    ribbongroup = '';
    nativetable = 'QUERYTYPES';
    nativevalue = :qref;
    popupmenu = 'MenuQuerytypes';
    foradmin = 0;
--    groupprocedure = 'NAV_ZAPYTANIA('||:REF||')';
    groupprocedure = '';
    number = :n;
    qparent = :nativetable||cast(:qref as varchar(10));
    ref = :qparent;
    suspend;
    for select REF,:qparent,NUMBER,NAME,HINT,KINDSTR,ACTIONID,
      APPLICATIONS,FORMODULES,RIGHTS,RIGHTSGROUP,
      ISNAVBAR,ISTOOLBAR,ISWINDOW,ISMAINMENU,ITEMTYPE,
      GROUPPROCEDURE,CHILDRENCOUNT,RIBBONTAB,RIBBONGROUP,
      NATIVETABLE,POPUPMENU,NATIVEFIELD,FORADMIN
    from NAV_ZAPYTANIA(:QREF)
    order by NUMBER
    into :REF,:PARENT,:NUMBER,:NAME,:HINT,:KINDSTR,:ACTIONID,
      :APPLICATIONS,:FORMODULES,:RIGHTS,:RIGHTSGROUP,
      :ISNAVBAR,:ISTOOLBAR,:ISWINDOW,:ISMAINMENU,:ITEMTYPE,
      :GROUPPROCEDURE,:CHILDRENCOUNT,:RIBBONTAB,:RIBBONGROUP,
      :NATIVETABLE,:POPUPMENU,:NATIVEFIELD,:FORADMIN
    do begin
      nativevalue = :ref;
      suspend;
    end
    n = :n + 1;
  end
end^
SET TERM ; ^
