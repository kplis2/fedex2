--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_GLS_SHIPMENT_RESPONSE(
      EDEDOCSH_REF INTEGER_ID)
  returns (
      OTABLE STRING20,
      OREF INTEGER_ID)
   as
declare variable shippingdoc integer_id;
declare variable shippingdocno string40;
declare variable rootparent integer_id;
declare variable parent integer_id;
declare variable packageref integer_id;
declare variable packagesymb string40;
declare variable firstnum smallint_id;
declare variable label blob_utf8;
declare variable labelformat string10;
begin
  select oref
    from ededocsh
    where ref = :ededocsh_ref
  into :shippingdoc;

  select p.id
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.name = 'ArrayOfCLabel'
  into :rootparent;

  select p.val
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.parent = :rootparent
      and p.name = 'id'
  into :shippingdocno;

  select p.id
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.parent = :rootparent
      and p.name = 'label'
  into :parent;

  select trim(p.val)
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.parent = :parent
      and p.name = 'labelType'
  into :labelformat;

  firstnum = 1;
  for
    select p.id
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :rootparent
        and p.name = 'cLabel'
    into :parent
  do begin
    select p.val
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'number'
    into :packagesymb;

    if (firstnum = 1) then
    begin
      update listywysd set statussped = 1, symbolsped = :packagesymb, symbolspedid = :shippingdocno,
        x_anulowany = 0
        where ref = :shippingdoc;
      firstnum = 0;
    end

    select first 1 o.ref from listywysdroz_opk o
      where o.listwysd = :shippingdoc
        and o.rodzic is null
        and coalesce(o.symbolsped,'') = ''
      order by o.ref
    into :packageref;

    update listywysdroz_opk o set o.symbolsped = :packagesymb
      where o.ref = :packageref;

    select p.val
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'file'
    into :label;

    update or insert into listywysdopkrpt(doksped, opk, rpt, format)
      values(:shippingdoc,:packageref,:label,:labelformat)
      matching (doksped, opk);
  end

  otable = 'LISTYWYSD';
  oref = :shippingdoc;

  suspend;
end^
SET TERM ; ^
