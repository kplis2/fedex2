--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XP_WARUNEK(
      KEY1 integer,
      KEY2 integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(16,6))
   as
declare variable condition numeric(14,2);
declare variable econdition varchar(1024);
declare variable parref integer;
declare variable parvalue numeric(16,6);
declare variable parmodified smallint;
declare variable paramname varchar(255);
declare variable fromprshmat integer;
declare variable fromprshoper integer;
declare variable fromprshtool integer;
begin
  condition = 1;
  paramname='WARUNEK';
  -- czy parametr jest uprzednio zapisany i zmodyfikowany
  parref = NULL;
  select fromprshmat, fromprshoper, fromprshtool from prcalccols where ref=:key2
    into :fromprshmat, :fromprshoper, :fromprshtool;
  select first 1 ref, cvalue, modified
    from prcalccols where parent=:key2 and expr=:paramname and calctype=1
    into :parref, :parvalue, :parmodified;
  if(:parref is not null and :parmodified=1) then begin
    ret = :parvalue;
    exit;
  end

  -- nalezy obliczyc na nowo
  if(:fromprshmat is not null) then begin
    -- warunek z materialow
    select ECONDITION from PRSHMAT where REF=:fromprshmat
    into :econdition;

  end else if(:fromprshoper is not null) then begin
    -- warunek z operacji
    select ECONDITION from PRSHOPERS where REF=:fromprshoper
    into :econdition;

  end else if(:fromprshtool is not null) then begin
    -- warunek z dodatkow
    select ECONDITION from PRSHTOOLS where ref=:fromprshtool
    into :econdition;

  end

  if(:econdition='') then condition = 1;
  else execute procedure STATEMENT_PARSE(:econdition,:key1,:key2,:prefix) returning_values :condition;
  if(:condition is null) then condition = 1;
  ret = :condition;

  -- nalezy zapisac wyliczony parametr
  if(:parref is not null) then begin
    update prcalccols set cvalue=:ret where ref=:parref;
  end else begin
    insert into prcalccols(PARENT,CVALUE,EXPR,DESCRIPT,CALCTYPE,PRCOLUMN)
    values(:key2,:ret,:paramname,'Warunek',1,null);
  end
end^
SET TERM ; ^
