--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE VAT_2013_2014(
      OTABLE varchar(30) CHARACTER SET UTF8                           ,
      OREF integer,
      DATADOK timestamp,
      TERMPLAT timestamp)
  returns (
      VATDATE timestamp)
   as
declare variable DATA timestamp;
declare variable DATAODB timestamp;
begin
/*PL: Procedura wprowadza kompatybilnosc na przelomie lat 2013-2014,
      ze wzgledu na zmiany w przepisach.(PR57428)
*/
   if(OTABLE = 'NAGFAK') then
   begin
    select data,dataodbioru
      from nagfak
      where ref = :oref
    into data, dataodb;
    if (data < '2014-01-01') then
      vatdate = data;
    else
      vatdate = dataodb;
   end
  suspend;
end^
SET TERM ; ^
