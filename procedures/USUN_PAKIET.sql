--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE USUN_PAKIET(
      REFP integer,
      POZTYP char(1) CHARACTER SET UTF8                           )
   as
begin
  if(:refp is null or :refp=0) then exit;
  if(:poztyp = 'F') then begin
    delete from pozfak where refdopakiet = :refp;
  end
  else if (poztyp = 'M') then begin
    delete from dokumpoz where refdopakiet = :refp;
  end
  else if (poztyp = 'Z') then begin
    delete from pozzam where refdopakiet = :refp;
  end
end^
SET TERM ; ^
