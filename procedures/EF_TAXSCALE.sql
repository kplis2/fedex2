--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_TAXSCALE(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      TAXSCALE numeric(14,2))
   as
declare variable TPER char(6);
declare variable INCOME numeric(14,2);
declare variable AMOUNT numeric(14,2);
declare variable TMPAMOUNT numeric(14,2);
declare variable TAXYEAR years_id;
declare variable LEVELDEC smallint;
declare variable FROMDATE timestamp;
declare variable TODATE timestamp;
declare variable EXTDATE timestamp;
declare variable EXTAMOUNT numeric(14,2);
declare variable PRTYPE varchar(10);
declare variable PERSON integer;
declare variable COMPANY companies_id;
declare variable payday timestamp;
begin
  --DU: personel - liczy procent podatku z tabeli skali podatkowej

  select tper, prtype, payday from epayrolls
    where ref = :payroll
    into :tper, :prtype, :payday;

  select person, company from employees
    where ref = :employee
    into :person, :company;

  --Na listach socjalnych emerytow i rencistow obowiazuje podatek 10% (PR26581)
  --...ale pod warunkiem, ze nie sa zatrudnieni (BS49983)
  if (prtype = 'SOC') then
  begin
    execute procedure efunc_prdates(payroll, 0)
      returning_values fromdate, todate;

    if (exists(select first 1 1 from eperpension
                 where person = :person and fromdate <= :todate)
                   and (todate >= :fromdate or todate is null) --PR54458
    ) then
    if (not exists(select first 1 1 from employment
                 where employee = :employee and fromdate <= :todate
                   and (todate >= :fromdate or todate is null))
    ) then
      taxscale = 10;
  end

  if (taxscale is null) then
  begin
    execute procedure period2dates(:tper)
      returning_values :fromdate, :todate;

    taxyear = extract(year from todate);

  --Wyliczenie dochodu osiagnietego w danym roku
    select sum(case when p.ecolumn = 7000 then p.pvalue else -p.pvalue end)
      from epayrolls r
        join eprpos p on (p.payroll = r.ref and 0+p.ecolumn in (7000,7010))
        join employees e on (e.ref = p.employee and e.company = r.company)
      where e.person = :person and e.company = :company
        and r.tper < :tper and r.tper starting with cast(:taxyear as varchar(4))
        and r.empltype in (1,3)
      into :income;
    income = coalesce(income,0);

  --Pobranie informacji o danych podatkowych (BS99051)
    if (payday < todate) then todate = payday;

    select first 1 leveldec, extdate, extamount from empltaxinfo
      where employee = :employee and fromdate <= :todate
      order by fromdate desc
      into :leveldec, :extdate, :extamount;
  
    if (leveldec is null) then
      exception no_tax_info;
  
    if (extract(year from extdate) = taxyear and extdate <= todate) then
      income = income + coalesce(extamount,0);

  --Wyliczenie i ewnentualna korekta wysokosci progu podatkowego
    select max(amount) from etaxlevels
      where amount <= :income and taxyear = :taxyear
      into :amount;
  
    if (amount is null and income >= 0) then
      exception no_tax_levels_for_this_year;
  
    if (leveldec = 1) then
    begin
    -- obnizenie progu
      select max(amount) from etaxlevels
        where amount < :amount and taxyear = :taxyear
        into :tmpamount;
    end else
    if (leveldec = 2) then
    begin
    -- podwyzszenie progu
      select min(amount) from etaxlevels
        where amount > :amount and taxyear = :taxyear
        into :tmpamount;
    end

    if (tmpamount is not null) then
      amount = tmpamount;

    select max(taxscale) from etaxlevels
      where amount <= :amount and taxyear = :taxyear
      into :taxscale;
  end

  suspend;
end^
SET TERM ; ^
