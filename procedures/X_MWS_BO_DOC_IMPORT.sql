--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_BO_DOC_IMPORT(
      MAGAZYN DEFMAGAZ_ID,
      TYP STRING3,
      AKCEPT SMALLINT_ID)
   as
declare variable EXPIMPREF INTEGER_ID;
declare variable KTM KTM_ID;
declare variable JEDNTMP STRING20;
declare variable JEDN JEDN_MIARY;
declare variable ILOSC ILOSCI_MAG;
declare variable CNT INTEGER_ID;
declare variable DOKREF DOKUMNAG_ID;
declare variable MWSORD MWSORDS_ID;
declare variable DOSTAWA DOSTAWA_ID;
declare variable KTMTMP KTM_ID;
declare variable KODPROD KTM_ID;
declare variable KODPRODTMP KTM_ID;
declare variable NRWERSJI INTEGER_ID;
declare variable loc_hex string10;
declare variable ref_towar integer_id;
declare variable ref_mwsconstlock integer_id;
declare variable regal integer_id;
declare variable pozycja integer_id;
declare variable lvl integer_id;
declare variable mag string10;
declare variable symbol_tmp string20;
declare variable kod_kresk string20;
begin

--Ldz BO dla Hermona, (z EHRLE), tutaj bedzie import z lokacjami ?

--porzadkowanie expimpa, zamiana , na .
/*  update expimp set s2 = replace(s2,',','.'), s10 = null, s11 = null, s12 = null
    where 1 = 1
    and coalesce(s13, '') = '';  */ --przeniesione do xxx_bo_auto
  cnt = 1;

--ustawienie aktuoperatora, trigery nie pozwalaja dodawac dokumentow bez operatora
   execute procedure set_global_param('AKTUOPERATOR',74);
  if (:akcept = 0) then begin
--szukanie nie zaakceptowanego dokumentu BO z liczba pozycji mniejsza od 100
    select first 1 d.ref--, count(p.ref) as pozcnt
      from dokumnag d
        left join dokumpoz p on (d.ref = p.dokument)
      where d.magazyn = :magazyn and d.typ = :typ and d.akcept = 0
        and d.uwagi = 'BO'||current_date
      group by d.ref
      having count(p.ref) < 100
    into :dokref;

--jesli jest, to pobieramy liczbe pozycji
    if (dokref is not null) then
      select count(p.ref) from dokumpoz p where p.dokument = :dokref into cnt;

--lecimy po calym expimpie, s1 - kodkresk, s5 - ilosc
    for
   -- select first 100 e.ref, trim(e.s1), cast(e.s5 as numeric(14,4)), trim(upper(e.s6)), e.s7, e.s8, e.s9   -- exel
      select first 100 e.ref, trim(e.s1), cast(trim(e.s2) as numeric(14,4)), trim(upper(e.s3)), trim(e.s4), trim(e.s5), trim(e.s6)       --for script
        from expimp e
          where cast(trim(e.s2) as numeric(14,4)) > 0  -- tylko stany wieksze od zera
            and  coalesce(e.s13, '') = ''
      into :expimpref, :kod_kresk, :ilosc, :mag, :regal, :pozycja,  :lvl
    do begin
      ref_mwsconstlock = null;
      ref_towar = null;
-- sprawdzamy czy towar ma prawidlowy ktm i czy istnieje

      select first 1 t.ref, t.ktm
        from towary t
          --where t.kodkresk = :kod_kresk
          where t.int_id = :kod_kresk
      into :ref_towar, :ktm;
-- sprawdzamy czy lokacja istnieje
      if (position('MG' in :mag) > 0) then begin
        mag = substring(:mag from 3 for 1);
        symbol_tmp = 'M'||mag||'%_'||lpad(regal, 2, 0)||'_'||lpad(pozycja, 2, 0)||'_'||lpad(lvl, 2, 0);
      end else if (:mag = 'NAMIOT') then
        symbol_tmp = 'N1_01_01_01';
      else
        exception universal mag;

      select m.ref  --first 1 dodane do debugu
        from mwsconstlocs m
          where m.symbol similar to :symbol_tmp
      into :ref_mwsconstlock;

      if (:ref_towar is null) then
        update expimp set s10 = 1 where ref = :expimpref; --brak towaru w systemie lub towar niezgodny
      else if (:ref_mwsconstlock is null) then
        update expimp set s10 = 2 where ref = :expimpref; --podana lokacja bledna lub pusta
      else
      begin
-- jesli nie ma dokumentu lub ma 100 pozycji
        if (:dokref is null or :cnt = 100) then begin
          dokref = gen_id(GEN_DOKUMNAG,1);
          insert into dokumnag(ref, magazyn, data, typ, operator, uwagi)
            values(:dokref, :magazyn, current_date, :typ, 74, 'BO'||current_date);
          cnt = 0;
        end
        select first 1 w.nrwersji from  wersje w where w.ktm = :ktm order by w.nrwersji into :nrwersji;

        insert into dokumpoz(dokument, ktm, wersja, ilosc, dostawa, int_zrodlo) --dostawa ?
          values(:dokref, :ktm, :nrwersji, :ilosc, :dostawa, :ref_mwsconstlock); -- w int_zrodlo przenosimy ref lokacji dla x_mwsord_bo, zrobi inwente na prawidowe lokacje
        cnt = :cnt + 1;
        nrwersji = null;
      end
      ktm  = null;
      ilosc = null;
      regal = null;
      pozycja = null;
      lvl = null;
      mag = null;
      symbol_tmp = null;
      kod_kresk = null;
      update expimp e
        set e.s13 = '1'
      where e.ref = :expimpref;
    end
  end
  if (:akcept = 1) then begin
    for
      select first 3 ref from dokumnag
        where magazyn = :magazyn and typ = :typ -- and uwagi = 'BO'||current_date
          and akcept = 0
      into :dokref
     do begin
       update dokumnag d set d.akcept = 1 where d.ref = :dokref;

       execute procedure x_mwsord_bo(:dokref, :dokref, null, 'M', null, null, 0, 17)
         returning_values :mwsord;

       update dokumpoz d set d.int_zrodlo = null where d.dokument = :dokref; --czyszczenie ref_mwsconstloc z int_zrodlo
    end
  end
end^
SET TERM ; ^
