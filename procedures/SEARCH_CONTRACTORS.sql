--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SEARCH_CONTRACTORS(
      SEARCH varchar(255) CHARACTER SET UTF8                           )
  returns (
      KLIENT integer,
      CPODMIOT integer,
      PKOSOBA integer,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      SKROT varchar(255) CHARACTER SET UTF8                           ,
      ADRES varchar(255) CHARACTER SET UTF8                           ,
      NIP NIP,
      ICON varchar(16) CHARACTER SET UTF8                           )
   as
declare variable N integer;
begin
  n = 50;

  if(coalesce(char_length(:search),0)<3) then exit; -- [DG] XXX ZG119346
  search = lower(:search);

  --lista klientów
  for select
        k.ref, null , null, k.nazwa, k.fskrot,
        coalesce(k.miasto, '') || ', ' || coalesce(k.ulica, ''),
        k.nip, 'MI_OPERATOR'
    from klienci k
    where k.crm = 0 and
          ((lower(fskrot) starting with :search) or
          (lower(nazwa) starting with :search) or
          (lower(miasto) starting with :search) or
          (lower(ulica) starting with :search))
    into :klient, :cpodmiot,  pkosoba, :nazwa, :skrot, :adres, :nip, :icon
   do begin
    suspend;
     n = :n - 1;
    if(:n=0) then exit;
  end

  --lista podmiotów
  for select
        case when c.slodef = 1 then c.slopoz else null end,
        c.ref, null, c.nazwa, c.skrot,
        coalesce(c.miasto, '') || ', ' || coalesce(c.ulica, ''),
        c.nip, 'MI_USERS'
    from CPODMIOTY c
    where (lower(skrot) starting with :search) or
          (lower(nazwa) starting with :search) or
          (lower(miasto) starting with :search) or
          (lower(ulica) starting with :search)
    into :klient, :cpodmiot,  pkosoba, :nazwa, :skrot, :adres, :nip, :icon
   do begin
    suspend;
     n = :n - 1;
    if(:n=0) then exit;
  end

  --lista osób
  for select
        case when c.slodef = 1 then c.slopoz else null end,
        o.cpodmiot, o.ref , c.nazwa,
        coalesce(o.nazwa, '') || ' - ' || coalesce(c.skrot, ''),
        coalesce(c.miasto, '') || ', ' || coalesce(c.ulica, ''),
        c.nip, 'MI_PER'
    from pkosoby o
    join cpodmioty c on o.cpodmiot = c.ref
    where (lower(cpodmskrot) starting with :search) or
          (lower(o.nazwa) starting with :search) or
          (lower(c.nazwa) starting with :search) or
          (lower(c.skrot) starting with :search) or
          (lower(o.miasto) starting with :search) or
          (lower(o.ulica) starting with :search)
    into :klient, :cpodmiot,  pkosoba, :nazwa, :skrot, :adres, :nip, :icon
   do begin
    suspend;
     n = :n - 1;
    if(:n=0) then exit;
  end
end^
SET TERM ; ^
