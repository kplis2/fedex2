--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DEFCENZAK_WARTDOM(
      DEFCENNIKZAK_REF integer,
      PARSYMBOL varchar(20) CHARACTER SET UTF8                           )
   as
declare variable wartdom numeric(14,2);
declare variable odwart numeric(14,2);
declare variable dowart numeric(14,2);
declare variable proc numeric(14,2);
declare variable defcenzak_ref integer;
declare variable debug_log varchar(1024);
begin
  debug_log = parsymbol||'
';
  dowart = 100;
  wartdom = 0;
  for
    select ref, odwart from DEFCENZAK
      where  defcennikzak=:defcennikzak_ref and parsymbol = :parsymbol
      order by odwart desc
      into defcenzak_ref, :odwart
  do begin
    update DEFCENZAK set dowart = :dowart
      where ref = :defcenzak_ref;
    dowart = :odwart;
  end
  for
    select ref, odwart, dowart, procent from DEFCENZAK
      where defcennikzak=:defcennikzak_ref and parsymbol = :parsymbol
      order by odwart
      into defcenzak_ref, :odwart, :dowart, :proc
  do begin
--   debug_log = debug_log || wartdom;
    debug_log = debug_log ||' '||:defcennikzak_ref||' '||:PARSYMBOL||' '||:dowart;

    update DEFCENZAK set wartdom = :wartdom where ref = :defcenzak_ref;
    wartdom = coalesce(cast((dowart - odwart) * :proc as integer) + :wartdom,0);
    debug_log = debug_log ||' '||:wartdom||'
';
  end
--  exception test_break debug_log;
end^
SET TERM ; ^
