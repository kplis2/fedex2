--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BROWSE_STANYARCH(
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      DOSTAWA integer,
      MAG varchar(255) CHARACTER SET UTF8                           ,
      DATA timestamp,
      ZEROWE smallint = 1)
  returns (
      RKTM varchar(40) CHARACTER SET UTF8                           ,
      RWERSJA integer,
      RDOSTAWA integer,
      STAN numeric(14,4),
      WARTOSC numeric(14,2),
      KWARTOSC numeric(14,2))
   as
declare variable sql varchar(1024);
declare variable rktm_old varchar(40);
declare variable rwersja_old integer;
declare variable rdostawa_old integer;
begin
--PR63453
  sql = 'select a.ktm, a.wersja, a.dostawa, a.stan, a.wartosc, a.kwartosc from stanyarch a ';
  if(position(';' in :mag)>0) then sql = sql || 'where '''||:mag||''' like ''%;''||trim(a.magazyn)||'';%'' ';
  else sql = sql || 'where a.magazyn = '''||:mag||''' ';
  if (coalesce(:ktm,'') <> '' and position('%' in :ktm)>0 ) then
    sql = sql || ' and a.ktm like '''||replace(:ktm, '''', '''''')||'''';
  else if (coalesce(:ktm,'') <> '' and position('%' in :ktm)=0 ) then
    sql = sql || ' and a.ktm = '''||replace(:ktm, '''', '''''')||'''';
  if (wersja is not null) then
    sql = sql || ' and a.wersja = '||:wersja;
  if (coalesce(:dostawa,0) <> 0) then
    sql = sql || ' and a.dostawa = '||:dostawa;
  sql = sql || ' and a.data <= '''||:data||'''';
  sql = sql || ' order by a.ktm, a.wersja, a.dostawa, a.data desc';

  rktm_old = null;
  rwersja_old = null;
  rdostawa_old = null;

  for
    execute statement :sql
      into :rktm, :rwersja ,:rdostawa, :stan, :wartosc, :kwartosc
  do begin
    if (:rktm is distinct from :rktm_old or :rwersja is distinct from :rwersja_old or :rdostawa is distinct from :rdostawa_old) then
      if (:stan <> 0 or :zerowe = 1) then
        suspend;
    stan = null;
    wartosc = null;
    kwartosc = null;

    rktm_old = :rktm;
    rwersja_old = :rwersja;
    rdostawa_old = :rdostawa;
  end
  rktm = null;
  rwersja = null;
  rdostawa = null;
end^
SET TERM ; ^
