--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXP2PLATNIK_DOCRA(
      EMPL_REF integer,
      DTYPE smallint = null)
  returns (
      ROWNUMBER integer,
      INVALIDITY varchar(1) CHARACTER SET UTF8                           ,
      FROMTODATE date,
      PESEL varchar(11) CHARACTER SET UTF8                           ,
      NIP varchar(15) CHARACTER SET UTF8                           ,
      FNAME varchar(60) CHARACTER SET UTF8                           ,
      SNAME varchar(30) CHARACTER SET UTF8                           ,
      BIRTHDATE date,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      DOCNO varchar(11) CHARACTER SET UTF8                           ,
      CODE varchar(20) CHARACTER SET UTF8                           ,
      INMANAGE smallint,
      COMMUNE varchar(255) CHARACTER SET UTF8                           ,
      CITY varchar(255) CHARACTER SET UTF8                           ,
      POSTCODE varchar(10) CHARACTER SET UTF8                           ,
      HOUSENO varchar(20) CHARACTER SET UTF8                           ,
      LOCALNO varchar(20) CHARACTER SET UTF8                           ,
      PHONE varchar(255) CHARACTER SET UTF8                           ,
      FAX varchar(255) CHARACTER SET UTF8                           ,
      STREET varchar(255) CHARACTER SET UTF8                           ,
      COUNTRY_SYMBOL varchar(2) CHARACTER SET UTF8                           ,
      FOREIGN_POSTCODE varchar(9) CHARACTER SET UTF8                           )
   as
declare variable PERSONNAMES varchar(120);
begin
/*MWr: Personel - Eksport do Platnika deklaracji ZUS ZCNA (Dane członkow rodziny)
       Jezeli EMPL_REF < 0 to pobierz dane z EMPLFAMILY.REF = -EMPL_REF (BS70987) */

  rownumber = 0;
  for
    select :rownumber + 1, z2.code, p.pesel, p.nip, upper(p.fname), upper(p.sname), p.birthdate,
        z.code, p.inmanage, upper(g.descript), upper(p.city), p.postcode, upper(p.houseno), upper(p.localno), p.phone, p.fax, upper(p.street),
        case when (p.evidenceno > '') then '1' when (p.passportno > '') then '2' else '' end as doctype,
        case when (p.evidenceno > '') then p.evidenceno when (p.passportno > '') then p.passportno else '' end as docno,
        case when (:dtype = 0) then p.insuredfrom when (:dtype = 1) then p.insuredto + 1 else null end as fromtodate,  --PR55138, BS66605
        e.sname || ' ' || e.fname
      from emplfamily p
        join employees e on (e.ref = p.employee)
        left join edictzuscodes z on (p.conecttype = z.ref)
        left join edictguscodes g on (p.commune = g.ref)
        left join edictzuscodes z2 on z2.ref = p.invalidity
      where iif(:empl_ref < 0, -p.ref , p.employee) = :empl_ref   --BS70987
        and (:empl_ref < 0 or p.insurance = 1)
      into :rownumber, :invalidity, :pesel, :nip, :fname, :sname, :birthdate,
        :code, :inmanage, :commune, :city, :postcode, :houseno, :localno, :phone, :fax, :street,
        :doctype, :docno, :fromtodate, :personnames
  do begin
    if (invalidity is null) then --BS66605 
      invalidity = '0';

    if (coalesce(char_length(pesel),0) > 0 and coalesce(char_length(pesel),0) < 11) then --PR52595 -- [DG] XXX ZG119346
      exception universal personnames || ' - nieprawidłowy PESEL członka rodziny (' || sname || ' ' || fname || ')';

    suspend;
  end
end^
SET TERM ; ^
