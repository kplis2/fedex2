--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDOPERS_MANADD(
      GUIDE integer,
      PROPER varchar(20) CHARACTER SET UTF8                           )
   as
declare variable operreffirst integer;
declare variable operrefnew integer;
declare variable operched integer;
begin
  select min(ref) from prschedopers where guide = :guide and opermanadded = -1 into :operreffirst;
  if(operreffirst is null) then exception prschedopers_error 'Brak wskazania operacji pierwotnej';
 
  --nowa operacja
  execute procedure GEN_REF('PRSCHEDOPERS') returning_values :operrefnew;
  insert into prschedopers(ref, number, oper, zamowienie, schedule, schedzam, status, guide, activ, reported)
  select :operrefnew, number, :proper, zamowienie, schedule, schedzam, status, guide, 0, 0
    from prschedopers where ref = :operreffirst;

  --generowanie dependencies
    --z operacji
  insert into prschedoperdeps(DEPFROM,DEPTO)
    select ofrom.ref, :operrefnew
    from prschedopers oto
    left join prschedoperdeps d on (oto.ref = d.depto)
    left join prschedopers ofrom on (ofrom.ref = d.depfrom)
  where oto.guide = :guide and oto.opermanadded = -1
    and (ofrom.opermanadded is null or ofrom.opermanadded > 0)
   and ofrom.ref is not null;
    --na operacje
  insert into prschedoperdeps(DEPFROM,DEPTO)
    select :operrefnew, oto.ref
    from prschedopers ofrom
    left join prschedoperdeps d on (ofrom.ref = d.depfrom)
    left join prschedopers oto on (oto.ref = d.depto)
  where ofrom.guide = :guide and ofrom.opermanadded = -1
    and (oto.opermanadded is null or oto.opermanadded > 0)
    and oto.ref is not null;
  --odznaczenie na nieaktywne starych
  update prschedopers set opermanadded = :operrefnew, activ = 0 where guide = :guide and opermanadded = -1;
  update prschedopers set reported = 2, activ = 1 where ref = :operrefnew and guide = :guide;
  execute procedure prschedoper_amountin(:operrefnew);
end^
SET TERM ; ^
