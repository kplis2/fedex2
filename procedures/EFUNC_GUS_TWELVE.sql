--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EFUNC_GUS_TWELVE(
      GYEAR varchar(4) CHARACTER SET UTF8                           ,
      COMPANY integer,
      NOTRANDOMLY smallint = 0)
  returns (
      FILIA varchar(100) CHARACTER SET UTF8                           ,
      B1 integer,
      B1_FNAME varchar(30) CHARACTER SET UTF8                           ,
      B1_SNAME varchar(60) CHARACTER SET UTF8                           ,
      B1_ID varchar(10) CHARACTER SET UTF8                           ,
      B2 varchar(60) CHARACTER SET UTF8                           ,
      B3 varchar(6) CHARACTER SET UTF8                           ,
      B4 smallint,
      B5 smallint,
      B6 smallint,
      B7 smallint,
      B8 varchar(20) CHARACTER SET UTF8                           ,
      B9 varchar(20) CHARACTER SET UTF8                           ,
      B10 varchar(20) CHARACTER SET UTF8                           ,
      C1 numeric(14,3),
      C2 numeric(14,2),
      C3 numeric(14,2),
      C4 numeric(14,2),
      C5 numeric(14,2),
      C6 numeric(14,2),
      C7 numeric(14,2),
      C8 numeric(14,2),
      C9 numeric(14,2),
      C10 numeric(14,2),
      C11 numeric(14,2),
      C12 numeric(14,2),
      C13 numeric(14,2),
      C14 numeric(14,2),
      D1 numeric(14,3),
      D2 numeric(14,2),
      D3 numeric(14,2),
      D4 numeric(14,2),
      D5 numeric(14,2),
      D6 numeric(14,2),
      D7 numeric(14,2),
      D8 numeric(14,2),
      D9 numeric(14,2),
      D10 numeric(14,2),
      D11 numeric(14,2),
      D12 numeric(14,2),
      D13 numeric(14,2),
      D14 numeric(14,2),
      D15 numeric(14,2),
      D16 numeric(14,2),
      D17 numeric(14,2),
      ALERT smallint)
   as
declare variable GEXPKEY varchar(40);
declare variable EMPLOYEE integer;
declare variable STATEMENTTXT varchar(1024);
declare variable PYEAR_B varchar(4);
declare variable PYEAR_C varchar(4);
declare variable PYEAR_D varchar(4);
declare variable ALG varchar(4);
declare variable ctrlstr varchar(2000);
begin
/*Przygotowywanie algorytmów i regul do generowania sprawozdania*/
-----------------------------------------------------------------

  --Szukanie najbardziej aktualnych regul generowania sekcji w sprawozdaniu
  select first 1 substring(rdb$procedure_name from 24 for 27) from rdb$procedures
    where lower(rdb$procedure_name) like 'egus_twelve_getdata_b_v%'
      and substring(rdb$procedure_name from 24 for 27) <= :gyear
    order by rdb$procedure_name desc
    into :pyear_b;
  
  select first 1 substring(rdb$procedure_name from 24 for 27) from rdb$procedures
    where lower(rdb$procedure_name) like 'egus_twelve_getdata_c_v%'
      and substring(rdb$procedure_name from 24 for 27) <= :gyear
    order by rdb$procedure_name desc
    into :pyear_c;
  
  select first 1 substring(rdb$procedure_name from 24 for 27) from rdb$procedures
    where lower(rdb$procedure_name) like 'egus_twelve_getdata_d_v%'
      and substring(rdb$procedure_name from 24 for 27) <= :gyear
    order by rdb$procedure_name desc
    into :pyear_d;

  if (pyear_b is null or pyear_c is null or pyear_d is null) then
   exception egus_brak_procedury;

  --Szukanie najbardziej aktualnego algorytmu losowania
  select first 1 substring(rdb$procedure_name from 24 for 27) from rdb$procedures
    where lower(rdb$procedure_name) like 'egus_twelve_employees_v%'
      and substring(rdb$procedure_name from 24 for 27) <= :gyear
    order by rdb$procedure_name desc
    into :alg;

  if (notrandomly = 1 and gyear < 2014) then  --BS108630
    exception egus_brak_algo_los 'Opcja wyłączenia losowania nie jest obsługiwana dla zadanego roku!';
  else if (alg is null) then
    exception egus_brak_algo_los;

/*Losowanie praconików do sprawozdania*/
-----------------------------------------------------------------

  --pobranie klucza identyfikujacego w EXPIMP wylosowanych pracownikow do sprawozdania
  statementtxt = 'select first 1 * from egus_twelve_employees_V'||:alg||'('||:gyear||','|| :company || iif(notrandomly = 1, ',1','') || ')';
  execute statement statementtxt into :gexpkey;

/*Generowanie sprawozdania*/
-----------------------------------------------------------------

  if (gexpkey is not null) then
  begin
    filia = '000';  --BS108630
    alert = 0;
    for
      select s2, s3, s7, s8, s9 from expimp
        where s1 = :gexpkey and s2 > ''
        order by ref
      into :b1, :employee, :b1_fname, :b1_sname, :b1_id
    do begin
      statementtxt = 'select first 1 * from egus_twelve_getdata_b_V'||:pyear_b||'('''||:gyear||''','||:employee||')';
      execute statement statementtxt into :b2, :b3, :b4, :b5, :b6, :b7, :b8, :b9, :b10;

      statementtxt = 'select first 1 g.* ' || trim(iif(gyear < 2014, ', null', ''));
      statementtxt = statementtxt || ' from egus_twelve_getdata_c_V'||:pyear_c||'('''||:gyear||''','||:employee||') g';
      execute statement statementtxt into :c1, :c2, :c3, :c4, :c5, :c6, :c7, :c8, :c9, :c10, :c11, :c12, :c13, :c14;

      statementtxt = 'select first 1 g.* ' || trim(iif(gyear < 2014, ', null, null', ''));
      statementtxt = statementtxt || ' from egus_twelve_getdata_d_V'||:pyear_d||'('''||:gyear||''','||:employee||') g';
      execute statement statementtxt into :d1, :d2, :d3, :d4, :d5, :d6, :d7, :d8, :d9, :d10, :d11, :d12, :d13, :d14, :d15, :d16, :d17;

    --BS108630:
      if (notrandomly = 1) then b1 = null;
      if (gyear >= '2014') then
      begin
        ctrlstr = (  ';'|| coalesce(b3,'')||';'||coalesce(b4,'')||';'||coalesce(b5,'')||';'||coalesce(b6,'')||';'||coalesce(b7,'')||';'||coalesce(b8,'')||';'||coalesce(b9,'')||';'||coalesce(b10,'')
                   ||';'|| coalesce(c1,'')||';'||coalesce(c2,'')||';'||coalesce(c3,'')||';'||coalesce(c4,'')||';'||coalesce(c8,'')||';'||coalesce(c9,'')||';'||coalesce(c10,'')
                   ||';'|| coalesce(d1,'')||';'||coalesce(d2,'')||';'||coalesce(d3,'')||';'||coalesce(d4,'')||';'||coalesce(d10,'')||';'||coalesce(d11,'') );

        ctrlstr = replace(ctrlstr, ',','');
        ctrlstr = replace(ctrlstr, '.','');
        ctrlstr = replace(ctrlstr, '0','');
        ctrlstr = replace(ctrlstr, ' ','');

        if (trim(ctrlstr) containing ';;') then alert = 1;
        else alert = 0;
      end
      suspend;
    end

  --czyszczenie danych tymczasowych
    delete from expimp where s1 = :gexpkey;
  end
end^
SET TERM ; ^
