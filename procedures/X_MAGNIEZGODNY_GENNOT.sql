--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MAGNIEZGODNY_GENNOT
  returns (
      GRUPA integer)
   as
DECLARE VARIABLE ROZREF INTEGER;
DECLARE VARIABLE NEWCEN NUMERIC(14,4);
DECLARE VARIABLE NEWIL NUMERIC(14,4);
DECLARE VARIABLE NEWDOST NUMERIC(14,4);
DECLARE VARIABLE NEWDOST2 NUMERIC(14,4);
DECLARE VARIABLE OLDIL NUMERIC(14,4);
DECLARE VARIABLE OLDCEN NUMERIC(14,4);
DECLARE VARIABLE OLDDOST NUMERIC(14,4);
DECLARE VARIABLE POZYCJA INTEGER;
DECLARE VARIABLE ILOSC NUMERIC(14,4);
DECLARE VARIABLE NEWREF INTEGER;
DECLARE VARIABLE NUMER INTEGER;
DECLARE VARIABLE NOTREF INTEGER;
DECLARE VARIABLE TYP VARCHAR(3);
DECLARE VARIABLE MAGAZYN VARCHAR(3);
DECLARE VARIABLE MAG2 VARCHAR(3);
DECLARE VARIABLE KTM VARCHAR(40);
DECLARE VARIABLE ODDZIAL VARCHAR(17);
begin
  update EXPIMP set S1=replace(S1,',','.');
  update EXPIMP set S2=replace(S2,',','.');
  update EXPIMP set S3=replace(S3,',','.');
  update EXPIMP set S4=replace(S4,',','.');
  update EXPIMP set S6=replace(S6,',','.');
  update EXPIMP set S1=null where s1='';
  update EXPIMP set S2=null where s2='';
  update EXPIMP set S3=null where s3='';
  update EXPIMP set S4=null where s4='';
  update EXPIMP set S6=null where s6='';
/*s1 - ref rozpiski, której parametry mają być poprawione,
  s2 - nowa cena na rozpisce (moze byc taka, jak byla)
  s3 - nowa ilosc, jaka ma byc na rozpisce - wowczas rozpiska jest rozbiajana, a wskazana ilosc jest przenoszona na nowa rozpise
  s4 - nowy numer dostawy - jest podmieniany
  jesli s1 jest pusty, to znaczy, ze to polecenie poprawienia wiersza stanu cenowego
  s2 - ref rekordu stanu cenowego (0 oznacza stworzenie nowego rekordu stanu cenowego)
  s3 - cena stanu cenowego
  s4 - ilosc stanu cenowego
  s5 - towar, jakiego dotyczy poprawka  - w celu weryfikacji lub do zalozenia nowego rekordu
  s6 - numer dostawy, jaki ma byc na rekordie
  s7 - oddzial do tworzonego stanu cenowego
  s8 - magazyn tworzonego stanu cenowego

*/
  for select s1, s2, s3, s4, s5, s6, s7, s8
   from EXPIMP
   order by ref
   into :rozref, :newcen, :newil, :newdost, :ktm, :newdost2, :oddzial, :magazyn
   do begin
     if(:rozref > 0) then begin
       select ILOSC, CENA, DOSTAWA from DOKUMROZ where ref=:rozref into :oldil, :oldcen, :olddost;
       if(newdost is null) then newdost = olddost;
       if(newil is null) then newil = oldil;
       if(oldil <> newil or (newdost <> olddost))then begin
          select pozycja from DOKUMROZ where ref=:rozref into :pozycja;
          if(oldil > newil) then begin
            ilosc = :newil;
            execute procedure GEN_REF('DOKUMROZ') returning_values :newref;
            select max(NUMER) from DOKUMROZ where POZYCJA=:pozycja into :numer;
            numer = numer + 1;
            insert into DOKUMROZ(REF, POZYCJA, NUMER, ILOSC, CENA, DOSTAWA, SERIALNR,
                ACK, BLOKOBLNAG, CENASNETTO, CENASBRUTTO, ktm,  MAGAZYN, WYDANIA)
              select :newref, POZYCJA, :NUMER, :ilosc, CENA, :newdost, SERIALNR,
                  7, 1, CENASNETTO, CENASBRUTTO,KTM,MAGAZYN,WYDANIA
                from DOKUMROZ where ref=:rozref;
            update DOKUMROZ set ILOSC = ILOSC - :ilosc, ILOSCL = ILOSCL - :ILOSC,
            ACK = 7, BLOKOBLNAG = 1 where ref=:rozref;
            update DOKUMROZ set BLOKOBLNAG = 0, ACK = 1 where REF in (:rozref, :newref);
            rozref = :newref;
          end else if(newdost <> olddost) then begin
             update DOKUMROZ set DOSTAWA = :newdost, ack = 7, blokoblnag = 1 where ref=:rozref;
             update DOKUMROZ set BLOKOBLNAG = 0, ACK = 1 where REF in (:rozref);
          end

       end
       if(oldcen <> newcen) then begin
         select DOKUMNAG.typ, DOKUMNAG.MAGAZYN, dokumnag.mag2
           from DOKUMNAG join DOKUMPOZ on (DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
             join dokumroz on (DOKUMROZ.POZYCJA = DOKUMPOZ.REF)
          where DOKUMROZ.REF = :ROZREF
          into :typ, :magazyn, :mag2;
          if(:mag2 is null) then mag2 = '';
         if(:grupa is null) then execute procedure GEN_REF('DOKUMNOT') returning_values :grupa;
         notref = NULL;
         select REF from DOKUMNOT where DOKUMNOT.grupa = :grupa and DOKUMNOT.TYP = :typ and DOKUMNOT.MAGAZYN = :magazyn and DOKUMNOT.mag2 = :mag2 and DOKUMNOT.akcept = 0
         into :notref;
         if(:notref is null) then begin
           execute procedure gen_ref('DOKUMNOT') returning_values :notref;
           insert into DOKUMNOT(REF,MAGAZYN, TYP, MAG2, DATA, GRUPA)
             values (:notref, :magazyn, :typ, :mag2, current_date, :grupa);
         end
         if(:notref is null) then notref = 0;
         if(:notref <= 0) then exception OPER_ERROR;
         insert into DOKUMNOTP(DOKUMENT, DOKUMROZKOR,CENANEW, FROMDOKUMNOTP)
           values (:notref, :rozref, :newcen, NULL);
       end

     end else if(:newcen >= 0) then begin
       rozref = :newcen;
       newcen = :newil;
       newil = :newdost;
       newdost = :newdost2;
       if (:rozref <> 0) then begin
         select ILOSC, CENA, MAGAZYN
           from STANYCEN
           where ref=:rozref and (:ktm is null or (:ktm ='') or (KTM=:ktm))
           into :oldil, :oldcen, :magazyn;
         if(:newil is null) then newil = :oldil;
         if(:newcen is null) then newcen = :oldcen;
         if(newcen <> oldcen or (newil <> oldil)) then begin
           if(exists (select dostawa from STANYCEN where MAGAZYN=:magazyn and KTM=:ktm and CENA=:newcen and DOSTAWA = :newdost)) then
             exception TEST_BREAK;
           update STANYCEN set ILOSC = :newil, CENA = :newcen where ref=:rozref
             and (:ktm is null or (:ktm ='') or (KTM=:ktm))
             and MAGAZYN=:magazyn;
         end
       end else begin
         insert into stanycen (magazyn, ktm, wersja, cena, dostawa, ilosc, oddzial)
                        values(:magazyn, :ktm, 0, :newcen, :newdost, :newil, :oddzial);
       end
     end
   end
   if(:grupa > 0) then begin
     update DOKUMNOT set dokumnot.akcept = 1 where grupa = :grupa;
   end
end^
SET TERM ; ^
