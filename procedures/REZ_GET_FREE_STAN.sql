--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REZ_GET_FREE_STAN(
      MAGAZYN char(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      CENA numeric(14,2),
      DOSTAWA integer)
  returns (
      ILOSC numeric(14,4))
   as
declare variable ilstanyil numeric(14,4);
declare variable ilstancen numeric(14,4);
declare variable ilzablokow numeric(14,4);
declare variable typ char(1);
begin
  select sum(ILOSC) - sum(ZABLOKOW) from STANYCEN where
    MAGAZYN=:MAGAZYN AND KTM = :KTM AND WERSJA=:WERSJA AND
    (CENA = :CENA or (:cena is null ) or(:cena = 0)) AND
    (DOSTAWA = :DOSTAWA or(:dostawa is null) or(:dostawa = 0))
   into :ilosc;
 if(:ilosc is null) then ilosc = 0;
 select sum(ilosc)-sum(zablokow) from STANYIL where
    MAGAZYN=:MAGAZYN AND KTM = :KTM AND WERSJA=:WERSJA
   into :ilstanyil;
 if(:ilstanyil is null) then ilstanyil = 0;
 if(:ilstanyil < :ilosc) then ilosc = :ilstanyil;
 /*sprawdzenie blokad na stany cenowe dla magazynow P*/
 if(:cena > 0) then begin
   select TYP from DEFMAGAZ where SYMBOL=:magazyn into :typ;
   if(:typ = 'P') then begin
     select sum(ilosc - zablokow) from STANYCEN where MAGAZYN=:magazyn and KTM=:ktm AND WERSJA=:wersja and cena=:cena into :ilstancen;
     if(:ilstancen is null) then ilstancen  = 0;
     select sum(ilosc) from STANYREZ where MAGAZYN=:magazyn and KTM=:ktm AND WERSJA=:wersja and cena=:cena and STATUS='B' and ZREAL=0 and ((dostawa=0)or (dostawa is null)) into :ilzablokow;
     if(:ilzablokow is null) then ilzablokow = 0;
     ilstancen  = :ilstancen - :ilzablokow;
     if(:ilstancen < 0) then ilstancen = 0;
     if(:ilstancen < :ilosc) then ilosc = :ilstancen;
   end
 end
end^
SET TERM ; ^
