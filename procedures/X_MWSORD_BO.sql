--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWSORD_BO(
      DOCID integer,
      DOCGROUP integer,
      DOCPOSID integer,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      MWSORD integer,
      MWSACT integer,
      RECALC smallint,
      MWSORDTYPE integer)
  returns (
      REFMWSORD integer)
   as
declare variable REC smallint;
declare variable WH varchar(3);
declare variable STOCKTAKING smallint;
declare variable OPERATOR integer;
declare variable PRIORITY smallint;
declare variable BRANCH varchar(10);
declare variable PERIOD varchar(6);
declare variable FLAGS varchar(40);
declare variable SYMBOL varchar(20);
declare variable COR smallint;
declare variable QUANTITY numeric(14,4);
declare variable GOOD varchar(40);
declare variable GOODTMP varchar(40);
declare variable VERS integer;
declare variable OPERSETTLMODE smallint;
declare variable MWSCONSTLOCP integer;
declare variable WHSEC integer;
declare variable RECRAMP integer;
declare variable MWSPALLOCP integer;
declare variable MWSCONSTLOCL integer;
declare variable AUTOLOCCREATE smallint;
declare variable MWSPALLOCL integer;
declare variable WHAREA integer;
declare variable POSFLAGS varchar(40);
declare variable DESCRIPTION varchar(1024);
declare variable POSDESCRIPTION varchar(255);
declare variable LOT integer;
declare variable SHIPPINGAREA varchar(3);
declare variable WHAREALOGP integer;
declare variable MWSACCESSORY integer;
declare variable DIFFICULTY varchar(10);
declare variable TIMESTART timestamp;
declare variable MAXNUMBER integer;
declare variable TIMESTARTDCL timestamp;
declare variable TIMESTOPDCL timestamp;
declare variable REALTIME double precision;
declare variable WHAREALOGB integer;
declare variable WHAREALOGL integer;
declare variable DIST numeric(14,4);
declare variable REFMWSACTPAL integer;
declare variable REFMWSACT integer;
declare variable PALTYPE varchar(20);
declare variable PALACTSQUANTITY numeric(14,4);
declare variable TOINSERT numeric(14,4);
declare variable STATUS smallint;
declare variable WHSECOUT integer;
declare variable PALTYPEVERS integer;
declare variable PLANWHAREALOGL integer;
declare variable PLANMWSCONSTLOCL integer;
declare variable PLANWHAREAL integer;
declare variable PALPACKMETHSHORT varchar(30);
declare variable PALPACKMETH varchar(40);
declare variable LOKACJA varchar(40);
declare variable PALPACKQUANTITY numeric(14,4);
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable SLOKOD varchar(40);
declare variable MANMWSACTS smallint;
declare variable QUANTITYONLOC numeric(14,4);
declare variable MAXPALOBJ numeric(14,4);
declare variable REFILL smallint;
declare variable WH2 varchar(3);
begin
  if (docgroup is null) then docgroup = docid;
  mwsordtype = 17;
 /* select stocktaking, opersettlmode, autoloccreate
    from mwsordtypes
    where ref = :mwsordtype
    into stocktaking, opersettlmode, autoloccreate;  */
    stocktaking = 0;
    opersettlmode = 0;
    autoloccreate = 0;
  select first 1 defdokum.wydania, defdokum.koryg, coalesce(dokumnag.mwsmag, dokumnag.magazyn), dokumnag.operator,
      dokumnag.uwagisped, dokumnag.katmag, dokumnag.spedpilne, dokumnag.oddzial,
      dokumnag.okres, dokumnag.flagi, dokumnag.symbol, dokumnag.kodsped,
      dokumnag.wharealogb, 6, dokumnag.ref, substring(dostawcy.id from 1 for 40), dokumnag.mag2
    from dokumnag
      left join dostawcy on (dokumnag.dostawa = dostawcy.ref)
      left join defdokum on (defdokum.symbol = dokumnag.typ)
    where dokumnag.ref = :docid
    order by dokumnag.ref
    into rec, cor, wh, operator,
      description, difficulty, priority, branch,
      period, flags, symbol, shippingarea,
      wharealogb, slodef, slopoz, slokod, wh2;
  if (rec = 0) then rec = 1; else rec = 0;
  if (manmwsacts is null) then manmwsacts = 0;
  refmwsord = null;
  --select first 1 ref from mwsords where docid = :docid and status = 0
  --  into refmwsord;
  if (refmwsord is null) then
  begin
    execute procedure gen_ref('MWSORDS') returning_values refmwsord;
    select okres from datatookres(current_date,0,0,0,0) into period;
    insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator,
        priority, description, wh, difficulty, regtime, timestartdecl, timestopdecl,
        mwsaccessory, branch, period, flags, docsymbs, cor, rec, bwharea, ewharea,
        status, shippingarea, slodef, slopoz, slokod, manmwsacts)
      values (:refmwsord, :mwsordtype, :stocktaking, 'M', :docgroup, :docid, :operator,
          :priority, :description, :wh, :difficulty, current_timestamp, :timestart, current_timestamp,
          :mwsaccessory, :branch, :period, :flags, :symbol, :cor, :rec, :wharealogb, :wharealogb,
          0, :shippingarea, :slodef, :slopoz, :slokod,:manmwsacts);
  end
  -- znalezienie max. numeru operacji - kolejne zwiekszamy i jeden.
  --delete from mwsacts where status = 0 and mwsord = :refmwsord;
 -- select max(number) from mwsacts where mwsord = :refmwsord into maxnumber;
  /*if (wh = 'MWS') then --[PM] UZUPELNIC !!!
    mwsconstlocl = 2; --[PM] UZUPELNIC !!! */ -- BO robimy z lokacjami, zapisane sa w dokumpoz -> int_zrodlo
  if (wh <> 'MWS') then
    exception universal 'Nieobslugiwany magazyn.';
--  if (maxnumber is null) then
  maxnumber = 0;
  for
    select n.ref, p.ref, p.ktm, p.wersjaref, p.dostawa, p.iloscl, p.int_zrodlo
      from dokumpoz p
        join towary t on p.ktm = t.ktm --[PM] XXX
        join dokumnag n on (n.ref = p.dokument and n.akcept = 1)
      where n.grupasped = :docgroup --p.dokument = :docid
        and p.iloscl > 0
        and t.usluga <> 1 --[PM] XXX na wszelki wypadek
      order by n.ref, p.ref --[PM] XXX kolejnosc ;-)
      into :docid, :docposid, :good, :vers, :lot, :quantity, :mwsconstlocl
  do begin
    -- najpierw probujemy skompensowac lokacje inwentaryzacyjna
   /* for
      select s.mwspalloc, -s.quantity, s.lot
        from mwsstock s
          left join defmagaz d on d.symbol = s.wh
        where s.mwsconstloc = :mwsconstlocl and s.vers = :vers and s.quantity < 0
          and (d.magbreak < 2 or (d.magbreak = 2 and s.lot = :lot))
        into mwspallocl, quantityonloc, lot
    do begin
      if (quantityonloc > quantity) then
        toinsert = quantity;
      else
        toinsert = :quantityonloc;
      execute procedure gen_ref('MWSACTS') returning_values refmwsact;
      insert into mwsacts (ref, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
          mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
          regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority,
          lot, recdoc, plus, wharealogl, wharealogp, number, disttogo, palgroup, ispal,
          planmwsconstlocl, planwharealogl, planwhareal, PALPACKMETHSHORT, PALPACKMETH, palpackquantity)
        values (:refmwsact, :refmwsord, 1, :stocktaking, :good, :vers, :toinsert, null, null,
           :mwsconstlocl, :mwspallocl, :docid, :docposid, 'M', :opersettlmode, :autoloccreate, :wh, :wharea, :whsecout,
           current_timestamp, :timestartdcl, :timestopdcl, :realtime, :posflags, :posdescription, :priority,
           :lot, :rec, 1, :wharealogp, :wharealogl, :maxnumber, :dist, :refmwsact, 0,
           :planmwsconstlocl, :planwharealogl, :planwhareal, :PALPACKMETHSHORT, :PALPACKMETH, :palpackquantity);
      quantity = quantity - toinsert;
      if (quantity <= 0) then
        break;
    end  */
    if (quantity > 0 and mwsconstlocl is not null) then
    begin
      mwspallocl = null;
      if (mwspallocl is null) then
      begin
        execute procedure gen_ref('MWSPALLOCS') returning_values mwspallocl;
        insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status,
            fromdocid, fromdocposid, fromdoctype)
          select :mwspallocl, :mwspallocl, ref, 2, 0,
              :docid, :docposid, 'M'
            from mwsconstlocs
            where ref = :mwsconstlocl;
      end
      execute procedure gen_ref('MWSACTS') returning_values refmwsact;
      insert into mwsacts (ref, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
          mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
          regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority,
          lot, recdoc, plus, wharealogl, wharealogp, number, disttogo, palgroup, ispal,
          planmwsconstlocl, planwharealogl, planwhareal, PALPACKMETHSHORT, PALPACKMETH, palpackquantity)
        values (:refmwsact, :refmwsord, 1, :stocktaking, :good, :vers, :quantity, null, null,
           :mwsconstlocl, :mwspallocl, :docid, :docposid, 'M', :opersettlmode, :autoloccreate, :wh, :wharea, :whsecout,
           current_timestamp, :timestartdcl, :timestopdcl, :realtime, :posflags, :posdescription, :priority,
           :lot, :rec, 1, :wharealogp, :wharealogl, :maxnumber, :dist, :refmwsact, 0,
           :planmwsconstlocl, :planwharealogl, :planwhareal, :PALPACKMETHSHORT, :PALPACKMETH, :palpackquantity);
           maxnumber = maxnumber + 1;
    end
  end
  update mwsords o set status = 1 where ref = :refmwsord;
  update mwsacts a set a.status = 2, a.quantityc = a.quantity where a.mwsord = :refmwsord;
  update mwsords o set status = 5 where ref = :refmwsord;
  suspend;
end^
SET TERM ; ^
