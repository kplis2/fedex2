--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WFCLEAR as
begin
    delete from wfchangelog;
    delete from wfqueue;
    delete from wftask;
    delete from wffield;
    delete from wfinstance;
    delete from wfevents;
    delete from wfattachment;
end^
SET TERM ; ^
