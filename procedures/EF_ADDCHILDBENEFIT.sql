--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_ADDCHILDBENEFIT(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      COL integer)
  returns (
      RET numeric(14,2))
   as
DECLARE VARIABLE FROMDATE DATE;
DECLARE VARIABLE TODATE DATE;
declare variable rehability smallint;
declare variable birthdate date;
declare variable tyear smallint;
declare variable tmonth smallint;
declare variable byear smallint;
declare variable bmonth smallint;
declare variable smonth smallint;
declare variable schoolaid smallint;
declare variable careaid smallint;
begin
  --MW: personel - liczenie dodatków do zasilków rodzinnych

  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  ret = 0;

  -- 1. Dodatek z tytułu kształcenia i rehabilitacji dziecka niepełnosprawnego
  if (COL = 4430) then
  begin
    for
      select rehabilityaid, birthdate
        from emplfamily
        where rehabilityaid = 1 and employee = :employee
          and raidfrom <= :fromdate
          and raidto >= :todate
        into :rehability, :birthdate
        do begin
          tyear = substring (todate from 1 for 4);
          byear = substring (birthdate from 1 for 4);
          tmonth = substring (todate from 6 for 2);
          bmonth = substring (birthdate from 6 for 2);

          if (tyear - byear > 5 or (tyear - byear = 5 and tmonth - bmonth >=0)) then
          begin
            ret = ret + 70;
          end else
            ret = ret + 50;
        end
  end

  -- 2. Dodatek z tytułu rozpoczęcia roku szkolnego
  smonth = substring (todate from 6 for 2);
  if (col = 4420 and smonth = 9) then
  begin
    for
      select schoolaid
        from emplfamily
        where schoolaid = 1 and employee = :employee
          and saidfrom <= :fromdate
          and saidto >= :todate
        into :schoolaid
        do begin
          ret = ret + 90;
        end
  end

  suspend;
end^
SET TERM ; ^
