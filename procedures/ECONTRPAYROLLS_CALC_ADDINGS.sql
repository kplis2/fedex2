--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ECONTRPAYROLLS_CALC_ADDINGS(
      ECONPAYLISTNAG integer)
   as
declare variable summoney numeric(14,4);
begin
  select sum(ECONTRPAYROLLSPOS.topay)
    from ECONTRPAYROLLSPOS
      join econtractsdef on (econtractsdef.ref = ECONTRPAYROLLSPOS.econtractsdef)
    where ECONTRPAYROLLSPOS.econtrpayrollnag = :econpaylistnag
      and econtractsdef.docgroup > 4
    into :summoney;
  if (:summoney is null) then summoney = 0;
  update econtrpayrollsnag set econtrpayrollsnag.addings = :summoney
      where econtrpayrollsnag.ref = :econpaylistnag;
end^
SET TERM ; ^
