--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_PVAL(
      DATA date,
      COMPANY integer,
      PARAM varchar(20) CHARACTER SET UTF8                           ,
      EMPLOYEE integer = null)
  returns (
      RET numeric(14,2))
   as
declare variable FROMDATE timestamp;
declare variable TODATE timestamp;
declare variable ISVALIDRULE smallint;
declare variable PAYRULE integer;
declare variable EPAYRULE integer = 0;
declare variable DATA2PERIOD smallint = 0;
begin
/*MWr: Personel - Pobieranie aktualnej wartosci parametru placowego PARAM przy
  zadanym pracowniku EMPLOYEE. Jezeli:
    EMPLOYEE <= 0 - bierz wartosc parametru z regulaminu o refie = abs(EMPLOYEE)
    EMPLOYEE > 0 - bierz regulamin z przebiegu zatrudnienia pracownika EMPLOYEE
     COMPANY > 0 - bierz regulamin z EMPLOYMENTu z konca okresu wynikajacego z DATA,
            wpp. bierz regululamin bezposrednio przypadajacy na dany dzien DATA */

  data2period = iif(company < 0, 0, 1);
  company = abs(company);

  if (not exists (select first 1 1 from wscfgdef where symbol = :param and company = :company)) then
    exception ef_expt 'Brak definicji parametru o akronimie: ' || param;

  if (employee is not null) then --PR60011
  begin
    if (data2period = 1) then
      execute procedure period2dates(extract(year from :data) || extract(month from :data))
        returning_values fromdate, todate;
    else begin
      fromdate = data;
      todate = data;
    end

    employee = coalesce(employee,0);
    if (employee > 0) then
    begin
      select first 1 epayrule from emplcontrhist
        where employee = :employee
          and fromdate <= :todate
          and (todate >= :fromdate or todate is null)
        order by fromdate desc
        into :epayrule;
  
      epayrule = coalesce(epayrule,0);
    end else begin
      payrule = abs(employee);
      epayrule = payrule;
      employee = 0;
    end

    if (epayrule <> 0) then
    begin
    /*Jezelie dla zadanego pracownika regulamin jest niewazny, lub nie zawiera
      aktualnego parametru, to okresl wartosc parametru z regulaminu domyslnego.
      Gdy regulamin nie jest wazny, to informuj o tym tylko, gdy wywolanie procedury
      odbywa dla konkretnego regulaminu */

      select 1 from epayrules
        where ref = :epayrule
          and company = :company
          and (fromdate <= :todate or fromdate is null)
          and (todate >= :fromdate or todate is null)
         into :isvalidrule;
      isvalidrule = coalesce(isvalidrule,0);

      if (isvalidrule = 0) then
      begin
        if (payrule is not null) then
          exception universal  'Brak aktualnego regulaminu dla parametru: ' || param;
        else
          epayrule = 0;
      end else begin
        select first 1 pvalue from wscfgval
          where wscfgdef = :param and company = :company and fromdate <= :data
            and epayrule = :epayrule
          order by fromdate desc
          into :ret;

        if (ret is null) then
          epayrule = 0;
      end
    end
  end else if (data2period = 0) then  --company < 0 tylko gdy zadany jest pracownik/regulamin
    exception universal 'Nieprawidłowa parametryzacja GET_PVAL';

  if (ret is null) then
  begin
    if (epayrule = 0) then
    begin
      select ref from epayrules
        where isdefault = 1 and company = :company
        into :epayrule; --regulamin domyslny/podstawowy
    end
  
    select first 1 pvalue from wscfgval
      where wscfgdef = :param and company = :company and fromdate <= :data
        and epayrule = :epayrule
      order by fromdate desc
      into :ret;
  end

  if (ret is null) then
    exception get_pval_expt 'Brak aktualnej wartości parametru o akronimie: ' || param;

  suspend;
end^
SET TERM ; ^
