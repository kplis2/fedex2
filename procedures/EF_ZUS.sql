--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_ZUS(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      FLAG varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable fromdate timestamp;
  declare variable todate timestamp;
  declare variable fdate timestamp;
  declare variable person integer;
  declare variable nfzfund smallint;
  declare variable workfund smallint;
  declare variable fgsp smallint;
begin
  --DU: personel - sprawdza flagi w danych zusowskich pracownika

  execute procedure efunc_prdates(payroll, 2)
    returning_values fromdate, todate;

  select person
    from employees
    where ref = :employee
    into :person;

  select max(fromdate)
    from ezusdata
    where person = :person and fromdate <= :todate
    into :fdate;

  if (fdate is null) then
    exception no_zus_info;


  select nfzfund, workfund, fgsp
    from ezusdata
    where person = :person and fromdate = :fdate
    into :nfzfund, :workfund, :fgsp;

  ret = 0;
  if (flag = 'NFZ' and nfzfund = 1) then
    ret = 1;
  if (flag = 'FP' and workfund = 1) then
    ret = 1;
  if (flag = 'FGSP' and fgsp = 1) then
    ret = 1;

  suspend;
end^
SET TERM ; ^
