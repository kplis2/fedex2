--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_PERSADDRESS(
      PERSON integer,
      ADDRTYPE smallint = null,
      ONELINE smallint = 1,
      DEFORMAT varchar(3) CHARACTER SET UTF8                            = null)
  returns (
      STREET varchar(80) CHARACTER SET UTF8                           ,
      CITY varchar(60) CHARACTER SET UTF8                           ,
      POST varchar(60) CHARACTER SET UTF8                           ,
      CPWOJ varchar(40) CHARACTER SET UTF8                           ,
      COMMUNITY varchar(60) CHARACTER SET UTF8                           ,
      DISTRICT varchar(60) CHARACTER SET UTF8                           ,
      PHONE varchar(20) CHARACTER SET UTF8                           ,
      ALLADDRESS varchar(255) CHARACTER SET UTF8                           ,
      CITY_ORG varchar(60) CHARACTER SET UTF8                           ,
      ATYPE smallint,
      REF integer,
      HOME_NR varchar(40) CHARACTER SET UTF8                           ,
      LOCAL_NR varchar(7) CHARACTER SET UTF8                           ,
      POST_CODE varchar(10) CHARACTER SET UTF8                           )
   as
declare variable PREFIX varchar(10);
declare variable POST_OFFICE varchar(30);
declare variable ISADDRESS smallint;
declare variable DELIMITER varchar(10);
declare variable ATYPELIST varchar(3);
begin
/*DS/MWr: Personel: Funkcja zwraca adres danej osoby w czytelnym formacie, dla
  ADDRTYPE: 0 - zameldowania,
            1 - zamieszkania,
            2 - korespondencyjny,
            null lub wartosc < 0 - pierwszy znaleziony adres wg zadanego formatu DEFORMAT,
                gdzie definiujemy kolejnosc sprawdzania typow adresow - domyslnie '012'.

  Parametr ONELINE decyduje czy zwracana cala postac adresu ALLADDRESS jest
  pisana:
   - w jednej lini (ONELINE > 0, wartoscia domyslna jest 1) lub
   - w kolejnych wierszach ze spacja na poczatku (ONELINE = 0) lub bez spacji (<0)

  ALLADDRESS jest zlozone z pol: STREET, CITY, POST z zalozeniem, ze CITY ma
  wartosc pusta gdy nazwa miasta jest taka sama jak nazwa poczty lub nie ma nazwy
  ulicy. Wyswietlenie oryginalnego miasta z rekordu oferuje zmienna CITY_ORG.
*/
  oneline = coalesce(oneline, -1);
  addrtype = coalesce(addrtype, -1);
  isaddress = 0;

  if (addrtype < 0) then begin
    if (coalesce(deformat,'') = '') then deformat = '012';
    atypelist = deformat;
  end else
    atypelist = addrtype;

  while (coalesce(char_length(atypelist),0) > 0 and isaddress = 0) --PR37200 -- [DG] XXX ZG119346
  do begin
    atype = substring(atypelist from 1 for 1);   --BS41602

    select first 1 coalesce(a.stpref,''), coalesce(a.street,''), a.houseno, coalesce(a.localno,''),
        coalesce(a.postcode,''), coalesce(a.post,''), coalesce(a.city,''), lower(coalesce(c.opis,'')),
        a.phone, 1, g.descript, p.descript, a.ref
      from epersaddr a
        left join cpwoj16m c on (c.ref = a.cpwoj16m)
        left join edictguscodes g on (g.ref = a.communityid)
        left join edictguscodes p on (p.ref = a.powiatid)
      where a.addrtype = :atype
        and a.status = 1
        and a.person = abs(:person)
      into :prefix, :street, :home_nr, :local_nr,
        :post_code, :post_office, :city, :cpwoj,
        :phone, :isaddress, :community, :district, :ref;

    atypelist = substring(atypelist from 2 for coalesce(char_length(atypelist),0)); -- [DG] XXX ZG119346
  end

  if (isaddress = 1 and person > 0) then
  begin
    if (prefix = '') then
      prefix = 'ul.';
    else
      prefix = trim(prefix);
    street = trim(street);
    home_nr = trim(home_nr);
    local_nr = trim(local_nr);
    post_code = trim(post_code);
    post_office = trim(post_office);
    city = trim(city);
    city_org = city;

    if (street = '-') then
      street = '';
  
    if (local_nr not in ('', '-', '_')) then
      home_nr = home_nr || '/' || local_nr;
  
    if (upper(city) = upper(post_office) or upper(city) = upper(street))
      then city = '';

    post =  post_code || ' ' || post_office;
  
    if (street = '') then
    begin
      if (city = '') then city = city_org; --PR40394
      street = city || ' ' || home_nr;
      city = '';
    end else
    begin
      street = prefix||' '||street;
      if (home_nr <> '') then street = street || ' ' || home_nr;
    end

    alladdress = street;
    if (oneline <= 0) then
    begin
      if (oneline = 0) then
      begin
        alladdress = ' ' || alladdress;
        delimiter = '
 ';
      end else
        delimiter = '
';
    end else
      delimiter = '; ';
    if (street <> '') then alladdress = alladdress || delimiter;
    alladdress = alladdress || city;
    if (city <> '') then alladdress = alladdress || delimiter;
    alladdress = alladdress || post;
  end else
    post = post_office;

  if (isaddress = 1) then
    suspend;

end^
SET TERM ; ^
