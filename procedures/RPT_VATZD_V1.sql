--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_VATZD_V1(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      YEARID varchar(4) CHARACTER SET UTF8                           ,
      COMPANY integer,
      RODZAJ integer,
      ILE integer)
  returns (
      A integer,
      B varchar(255) CHARACTER SET UTF8                           ,
      C varchar(20) CHARACTER SET UTF8                           ,
      D1 varchar(20) CHARACTER SET UTF8                           ,
      D2 date,
      E date,
      F numeric(14,2),
      G numeric(14,2),
      SUMF numeric(14,2),
      SUMG numeric(14,2))
   as
declare variable ILET integer;
declare variable PERIODT varchar(6);
begin
  if (ile > 1) then
  begin
    if (period = 0) then period = yearid || '01';
    else if (period = 1) then period = yearid || '04';
    else if (period = 2) then period = yearid || '07';
    else if (period = 3) then period = yearid || '10';
  end
  a=0;
  ilet = ile;
  periodt = period;
  sumf = 0;
  sumg = 0;
  while(ilet > 0) do
  begin
    for
      select sum(coalesce(F,0)), sum(coalesce(G,0))
        from rpt_vatzd_pos (:periodt,  :yearid, :company, :rodzaj)
        into :F,:G
    do begin
      sumf = sumf + coalesce(f,0);
      sumg = sumg + coalesce(g,0);
    end
    select p.periodout
      from e_func_periodinc(:periodt, 1) p
      into :periodt;
    ilet = ilet - 1;
  end
  f = null;
  g = null;
  while(ile > 0) do
  begin
    for
      select B,C,D1,D2,E,F,G
        from rpt_vatzd_pos (:period,  :yearid, :company, :rodzaj)
        into :B,:C,:D1,:D2,:E,:F,:G
    do begin
      a=a+1;
      suspend;
      if (a=8) then  a=0;
      b= null;
      c=null;
      d1=null;
      d2=null;
      e=null;
      f=null;
      g=null;
    end
    select p.periodout
      from e_func_periodinc(:period, 1) p
      into :period;
    ile = ile - 1;
  end
  b= null;
  c=null;
  d1=null;
  d2=null;
  e=null;
  f=null;
  g=null;
  while(a < 8) do
  begin
    a = a+1;
    suspend;
  end
end^
SET TERM ; ^
