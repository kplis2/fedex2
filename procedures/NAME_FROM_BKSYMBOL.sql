--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAME_FROM_BKSYMBOL(
      COMPANY integer,
      DICTDEF integer,
      SYMBOLK varchar(20) CHARACTER SET UTF8                           )
  returns (
      NAME varchar(80) CHARACTER SET UTF8                           ,
      DICTPOS integer)
   as
declare variable TYP varchar(20);
declare variable DNAME varchar(255);
declare variable iscomp smallint;
begin
  select typ, iscompany from slodef where ref= :dictdef
    into :typ, iscomp;
  if (typ = 'ESYSTEM') then
    select nazwa, ref from slopoz where slownik = :dictdef and kontoks = :symbolk
      into :dname, :dictpos;
  else if (typ = 'KLIENCI') then
  begin
    if (iscomp = 1) then
      select nazwa, ref from klienci where company = :company and kontofk = :symbolk
        into :dname, :dictpos;
    else
      select nazwa, ref from klienci where kontofk = :symbolk
        into :dname, :dictpos;
  end
  else if (typ = 'DOSTAWCY') then
  begin
    if (iscomp = 1) then
      select nazwa, ref from dostawcy where company = :company and kontofk = :symbolk
        into :dname, :dictpos;
    else
      select nazwa, ref from dostawcy where kontofk = :symbolk
        into :dname, :dictpos;
  end
  else if (typ = 'VAT') then
    select name from vat where grupa = :symbolk
      into :dname;
  else if (typ = 'GRVAT') then
    select descript from grvat where symbol = :symbolk
      into :dname;
  else if (typ = 'ODDZIALY') then
    select nazwa from oddzialy where symbol = :symbolk
      into :dname;
  else if (typ = 'WALUTY') then
    select nazwa from waluty where symbol = :symbolk
      into :dname;
  else if (typ = 'DEFMAGAZ') then
    select opis from defmagaz where bksymbol = :symbolk
      into :dname;
  else if (typ = 'FXDASSETS') then
    select name, ref from FXDASSETS where company = :company and symbol = :symbolk
      into :dname, :dictpos;
  else if (typ = 'PERSONS') then
  begin
    if(iscomp = 1) then
      select substring(person from 1 for 80), ref from cpersons where company = :company and symbol = :symbolk --PR59074 
        into :dname, :dictpos;
    else
      select substring(person from 1 for 80), ref from cpersons where symbol = :symbolk --PR59074
        into :dname, :dictpos;
  end
  else if (typ = 'EMPLOYEES') then
  begin
    if(iscomp = 1) then
      select substring(personnames from 1 for 80), ref from employees where company = :company and symbol = :symbolk
        into :dname, :dictpos;
    else
      select substring(personnames from 1 for 80), ref from employees where symbol = :symbolk
        into :dname, :dictpos;
  end
  else if (typ = 'DEPARTMENTS') then
  begin
    if(iscomp = 1) then
      select name from departments where company = :company and symbol = :symbolk
        into :dname;
    else
      select name from departments where symbol = :symbolk
        into :dname;
  end
  else if (typ = 'SPRZEDAWCY') then
  begin
    if(iscomp = 1) then
      select nazwa, ref from sprzedawcy where company = :company and kontofk = :symbolk
      into :dname, :dictpos;
    else
      select nazwa, ref from sprzedawcy where kontofk = :symbolk
      into :dname, :dictpos;
  end
  if (coalesce(char_length(dname),0)>80) then -- [DG] XXX ZG119346
    name = substring(dname from 1 for 80);
  else
    name = dname;
  suspend;
end^
SET TERM ; ^
