--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_FIRST_OPENED_AMPERIOD(
      COMPANY integer)
  returns (
      REF integer,
      AMPERIOD_NAME varchar(20) CHARACTER SET UTF8                           )
   as
declare variable i integer;
begin
  select min(amyear * 12 + ammonth)
    from amperiods where status = 0 and amperiods.company = :company
    into :i;
  if (i is not null) then
  begin
    select ref, name from amperiods
      where (amyear * 12 + ammonth) = :i and amperiods.company = :company
      into :ref, :amperiod_name;
  end
  suspend;
end^
SET TERM ; ^
