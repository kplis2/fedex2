--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_ANAL_WZ_PT_PZ(
      REF_WZ INTEGER_ID)
  returns (
      KTM KTM_ID,
      ILOSC_WZ NUMERIC_14_4,
      ILOSC_SID NUMERIC_14_4,
      ILOSC_PT NUMERIC_14_4,
      ILOSC_PZ NUMERIC_14_4,
      SYMBOL_WZ STRING,
      SYMBOL_PT STRING,
      SYMBOL_PZ STRING,
      NAZWA STRING,
      NAZWA2 STRING,
      INDEKSKAT STRING)
   as
  declare variable vers integer_id;
begin
    ilosc_wz = 0;
    ilosc_pz = 0;
    ilosc_pt = 0;
    ilosc_sid = 0;
    symbol_wz = '';
    symbol_pt = '';
    symbol_pz = '';
-- po pozycjach dokumentu WZ
  for
    select dp.ktm, dp.wersjaref, sum(dp.ilosc), d.symbol, w.nazwa, w.x_nazwa2, w.x_indeks_kat
      from dokumnag d
        join dokumpoz dp on (d.ref = dp.dokument)
        left join towary w on (w.ktm = dp.ktm)
      where d.ref = :ref_wz
      group by dp.ktm, dp.wersjaref, d.symbol, w.nazwa, w.x_nazwa2, w.x_indeks_kat
    into :ktm, :vers, :ilosc_wz, :symbol_wz, :nazwa, :nazwa2, :indekskat
  do begin
-- sprawdamy co jest w SID
    select sum(m.quantity - m.blocked - m.reserved)
      from mwsstock m
      where m.good = :ktm
        and m.vers = :vers
    into :ilosc_sid;
    if (ilosc_sid is null) then ilosc_sid = 0;
-- tylko wyswietlamy dla ilosci dostepnych mniejszych niz te ktore chcemy wydac na WZ
    if (ilosc_sid < ilosc_wz) then begin
-- co jest na niezaakceptowanym PZ
      select list(d.symbol,','),  sum(dp.ilosc)
        from dokumnag d
          join dokumpoz dp on (d.ref = dp.dokument)
        where d.akcept = 0
          and dp.ktm = :ktm
          and dp.wersjaref = :vers
          and d.typ = 'PZ'
          group by d.symbol, dp.ktm, dp.wersjaref
       into :symbol_pz, :ilosc_pz;
-- co jest na PT
      select list(mo.symbol,','),  sum(ma.quantity)
        from mwsords mo
          join mwsacts ma on (mo.ref = ma.mwsord)
        where ma.status < 2
          and ma.good = :ktm
          and ma.vers = :vers
          and mo.mwsordtypes = 'PT'
        group by mo.symbol, ma.good, ma.vers
      into :symbol_pt, :ilosc_pt;

      suspend;
    end
    ilosc_wz = 0;
    ilosc_pz = 0;
    ilosc_pt = 0;
    ilosc_sid = 0;
    symbol_wz = '';
    symbol_pt = '';
    symbol_pz = '';
  end
end^
SET TERM ; ^
