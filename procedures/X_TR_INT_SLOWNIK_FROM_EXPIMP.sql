--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_TR_INT_SLOWNIK_FROM_EXPIMP as
declare variable ZRODLO     ZRODLA_ID;
declare variable OKEY       STRING60;
declare variable OREF       INTEGER_ID;
declare variable OTABLE     TABLE_ID;
declare variable WARTOSC    STRING;
declare variable WARTOSCID  STRING60;
begin
  for
    select s1, s2, s3, s4, s5, s6
      from expimp e
    into ZRODLO, OKEY, OREF, OTABLE, WARTOSC, WARTOSCID
  do begin
    if (not exists(select first 1 1 from INT_SLOWNIK i
                      where i.zrodlo = :zrodlo and i.okey = :okey and i.oref = :oref and i.otable = :otable
                      and i.wartosc = :wartosc and i.wartoscid = :wartoscid)) then begin
      insert into INT_SLOWNIK (ZRODLO, OKEY, OREF, OTABLE, WARTOSC, WARTOSCID)
        values(:ZRODLO, :OKEY, :OREF, :OTABLE, :WARTOSC, :WARTOSCID);
    end
  end
end^
SET TERM ; ^
