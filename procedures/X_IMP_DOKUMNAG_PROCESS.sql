--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_IMP_DOKUMNAG_PROCESS(
      REF_IMP INTEGER_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING1024)
   as
declare variable SYMBOL_IMP STRING20;
declare variable NUMER_IMP INTEGER_ID;
declare variable TYP_IMP STRING3;
declare variable DATA_IMP TIMESTAMP_ID;
declare variable MAGAZYN_IMP STRING3;
declare variable MAG2_IMP STRING3;
declare variable WYDANIA_IMP SMALLINT_ID;
declare variable ZEWN_IMP SMALLINT_ID;
declare variable DOSTAWCA_IMP STRING120;
declare variable KLIENT_IMP STRING120;
declare variable ODBIORCAID_IMP STRING120;
declare variable DULICA_IMP STRING255;
declare variable DNRDOMU_IMP STRING10;
declare variable DNRLOKALU_IMP STRING10;
declare variable DKRAJID_IMP STRING20;
declare variable DMIASTO_IMP STRING255;
declare variable DKODP_IMP STRING10;
declare variable DTELEFON_IMP STRING255;
declare variable PLATNOSC_IMP STRING40;
declare variable DOSTAWA_IMP STRING255;
declare variable UWAGIWEW_IMP STRING1024;
declare variable UWAGIZEW_IMP STRING1024;
declare variable UWAGI_IMP STRING1024;
declare variable UWAGISPED_IMP STRING255;
declare variable DATAWYDANIA_IMP TIMESTAMP_ID;
declare variable WARTSBRUTTO_IMP NUMERIC_14_4;
declare variable PRIORYTET_IMP INTEGER_ID;
declare variable SOBOTA_IMP SMALLINT_ID;
declare variable IDDOKORYGOWANEGO_IMP INTEGER_ID;
declare variable ODROCZONEWYDANIE_IMP SMALLINT_ID;
declare variable MWSORDSTATUS_IMP INTEGER_ID;
declare variable TYPSENTE_IMP STRING3;
declare variable NAGID_IMP STRING120;
declare variable REF_LOC INTEGER_ID;
declare variable STATUS_POZ_LOC SMALLINT_ID;
declare variable MSG_POZ_LOC STRING1024;
declare variable ZEWNETRZNY_IMP SMALLINT_ID;
declare variable ODBIORCA INTEGER_ID;
declare variable KLIENT INTEGER_ID;
declare variable DOSTAWCA INTEGER_ID;
declare variable DATAPRZET_IMP TIMESTAMP_ID;
declare variable GRUPASPED DOKUMNAG_ID;
declare variable OKRES OKRES_ID;
declare variable SPOSZAP PLATNOSC_ID;
declare variable SPOSDOST SPOSDOST_ID;
declare variable KORYG SMALLINT_ID;
declare variable REFDOCKORYG INTEGER_ID;
begin
  status = 8;
  msg = '';

  if (not exists(select first 1 1 from x_imp_dokumnag x where x.ref = :ref_imp)) then begin
    msg = 'Bledny ref_imp :(';
    status = 8;
    suspend;
    exit;
  end

  select NAGID,SYMBOL, NUMER, TYP, data, MAGAZYN, MAG2, WYDANIA, zewnetrzny, DOSTAWCAID, KLIENTID, ODBIORCAID, DULICA,
        DNRDOMU, DNRLOKALU, DKRAJID, DMIASTOID, DKODPOCZTOWY, DTELEFON, UWAGIWEW, UWAGIZEW, UWAGISPED, DATAWYSYLKI,PRIORYTET,
        MWSSTATUS, TYPSENTE, DATAPRZET, PLATNOSC, DOSTAWA, DOZAPLATY, SOBOTA, iddokorygowanego,
        ODROCZONEWYDANIE, DATAPRZET
    from X_IMP_DOKUMNAG
    where ref = :ref_imp
  into :nagid_imp, :SYMBOL_imp, :NUMER_imp, :typ_imp, :data_imp, :magazyn_imp, :MAG2_imp, :wydania_imp, :zewnetrzny_imp ,:dostawca_imp, :klient_imp, :odbiorcaid_imp, :DULICA_imp,
       :DNRDOMU_imp, :DNRLOKALU_imp, :DKRAJID_imp, :dmiasto_imp, :dkodp_imp, :DTELEFON_imp, :uwagiwew_imp, :uwagizew_imp, :uwagisped_imp, :datawydania_imp,  :PRIORYTET_imp,
       :mwsordstatus_imp, :typsente_imp, :dataprzet_imp, :platnosc_imp, :dostawa_imp, :wartsbrutto_imp, :sobota_imp, :iddokorygowanego_imp,
       :ODROCZONEWYDANIE_imp, dataprzet_imp;
   --jezeli odroczone wydanie to odpalenie innego mechanizmu importu
  if (coalesce(:odroczonewydanie_imp,0) = 1) then begin
    execute procedure x_IMP_NAGZAM_PROCESS(ref_imp)
      returning_values  :status, :msg;
    suspend;
    exit;
  end
-- sprawdzenie czy dokument juz jest zaimportowany
  if (exists(select first 1 1 from dokumnag d where d.int_id = :nagid_imp)) then begin
    msg = 'Dokument jest juz zaimportowany, nie mozna ponownie importowac!';
    status = 8;
    suspend;
    exit;
  end

-- wertyfikacja Magazyn -> DEFMAGAZ
  if (coalesce(magazyn_imp, '') = '') then begin
    msg = 'Brak podanego magazynu';
    status = 8;
    suspend;
    exit;
  end

  --if (not exists(select first 1 1 from defmagaz d where d.symbol = :magazyn_imp)) then begin --MKD

  if (magazyn_imp = '1') then begin
    magazyn_imp ='MWS';
  end else if(magazyn_imp <> '1') then begin
    msg = 'Bledny magazyn' ||coalesce(magazyn_imp,'');
    status = 8;
    suspend;
    exit;
  end
  --weryfikacja sposobu zaplaty
  if (coalesce(platnosc_imp,'') <> '') then begin
    select p.ref
        from platnosci p
        where upper(p.opis) = upper(:platnosc_imp)
    into :sposzap;
    if (sposzap is null) then begin
        msg = 'Bledny sposob zaplaty' ||coalesce(platnosc_imp,'');
        status = 8;
        suspend;
        exit;
    end
  end
  --weryfikacja sposobu dostawy
  if (coalesce(dostawa_imp,'') <> '') then begin
    select s.ref
        from sposdost s
        where upper(s.nazwa) = upper(:dostawa_imp)
    into :sposdost;
    if (sposdost is null) then begin
        msg = 'Bledny sposob dostawy' ||coalesce(dostawa_imp,'');
        status = 8;
        suspend;
        exit;
    end
  end
  -- jezeli
  if (:typsente_imp is not null) then
  begin
    if (typsente_imp in ('WZ','PZ')) then
      typ_imp = typsente_imp;
  end
  if (:typ_imp in ('PZ')) then begin
-- wertyfikacja Dostawca -> DOSTAWCY
    if (coalesce(dostawca_imp, 0) = 0) then begin
      msg = 'Brak dostawcy';
      status = 8;
      suspend;
      exit;
    end

    if (not exists(select first 1 1 from dostawcy d where d.int_id = :dostawca_imp)) then begin
      msg = 'Bledny dostawca' ||coalesce(dostawca_imp, 0);
      status = 8;
      suspend;
      exit;
    end
  end
-- wertyfikacja Klient -> KLIENCI
  if (:typ_imp in ('WZ')) then begin
    if (coalesce(klient_imp, 0) = 0) then begin
      msg = 'Brak klienta';
      status = 8;
      suspend;
      exit;
    end

    if (not exists(select first 1 1 from klienci d where d.int_id = :klient_imp)) then begin
      msg = 'Bledny klient' ||coalesce(klient_imp, 0);
      status = 8;
      suspend;
      exit;
    end
  end
/*
-- wertyfikacja KrajWysylki -> COUNTRIES
  if (coalesce(dkrajid_imp, '') = '') then begin
    msg = 'Brak kraju wysylki';
    status = 8;
    suspend;
    exit;
  end

  if (not exists(select first 1 1 from countries d where d.symbol = :dkrajid_imp)) then begin
    msg = 'Bledny kraj wysylki' ||coalesce(dkrajid_imp,'');
    status = 8;
    suspend;
    exit;
  end
*/
  if (:typ_imp in ('WZ')) then begin
-- wertyfikacja Odbiorca -> ODBIORCY
    if (coalesce(odbiorcaid_imp, 0) = 0) then begin
      msg = 'Brak odbiorcy';
      status = 8;
      suspend;
      exit;
    end

    if (not exists(select first 1 1 from odbiorcy d where d.int_id = :odbiorcaid_imp)) then begin
      msg = 'Bledny odbiorcy' ||coalesce(odbiorcaid_imp, 0);
      status = 8;
      suspend;
      exit;
    end
  end

 /*
-- wertyfikacja SposDost -> SPOSDOST
  if (coalesce(sposdost_imp, '') = '') then begin
    msg = 'Brak sposobu dostawy';
    status = 8;
    suspend;
    exit;
  end

  if (not exists(select first 1 1 from sposdost d where d.nazwa = :sposdost_imp)) then begin
    msg = 'Bledny sposob dostawy' ||coalesce(sposdost_imp,'');
    status = 8;
    suspend;
    exit;
  end

-- wertyfikacja typ -> SPOSDOST
  if (coalesce(x_typ_imp, '') = '') then begin
    msg = 'Brak typu dokumentu';
    status = 8;
    suspend;
    exit;
  end

  if (not exists(select first 1 1 from defdokum d where d.symbol = :x_typ_imp)) then begin
    msg = 'Bledny typ dokumentu' ||coalesce(x_typ_imp,'');
    status = 8;
    suspend;
    exit;
  end  */
  select ref from odbiorcy o where o.int_id = :odbiorcaid_imp into :odbiorca;
  select ref from klienci k where k.int_id = :klient_imp into :klient;
  select ref from dostawcy d where d.int_id = :dostawca_imp into :dostawca;
  -- weryfikacja czy znany typ dokumentu
  if (typ_imp not in ('WZ','WZk','NP','PZ','MW','RW','PZk','PW') ) then begin
    status = 8;
    msg = 'Nieznany typ_imp dokumentu';
    suspend;
    exit;
  end
  --jezeli typNP to szukam grupy spedycyjnej ZG131655
  if(coalesce(typ_imp,'')= 'NP') then
  begin
    typsente_imp = 'NP';
    --znalezenie zatwierdzonego dokumentu wt o nieskonczonym zbieraniu
    select first 1 d.grupasped
        from dokumnag d
            join dokumpoz p on(d.ref = p.dokument)
            join mwsacts a on(a.docposid = p.ref)
        where d.odbiorcaid = :odbiorca
            and d.status = 1
            and a.status <3
            and a.status >0
    into :grupasped;
  end
  if (coalesce(typ_imp, '') = 'MW' and coalesce(upper(MAG2_imp), '') = '720') then begin --rozpoznanie dokumentu przesuniecia na marketing w celu zrobienia zdjec Ldz
  -- rozchod wewnetrzny z sente na MARKETING
    typ_imp = 'RW';
    mag2_imp = null;
    uwagi_imp = uwagi_imp|| ' Na marketing';
  end
  --Jezeli typ WZk to wystaiwmy ZZ
  koryg = 0;
  if(coalesce(typ_imp,'') in ( 'WZk','PZk') )then
  begin
    --odnajduje dokument ktory jest korygowany
    select ref from dokumnag dg
      where int_id = cast(:iddokorygowanego_imp as string40)
      into :refdockoryg;
    koryg = 1;
    typ_imp = 'ZZ';
    if (coalesce(refdockoryg,0) = 0) then begin
      status = 8;
      msg = msg || 'Dokument korygowany nie istnieje w Sente:'||coalesce(iddokorygowanego_imp,0);
      suspend;
      exit;
end
  end
  if(coalesce(typ_imp,'')= 'MW' and coalesce(upper(MAG2_imp), '') <> '1') then
  begin
    typ_imp = 'RW';
    mag2_imp = null;

  end
  if(coalesce(typ_imp,'')= 'MW' and coalesce(upper(MAG2_imp), '') = '1') then
  begin
    typ_imp = 'PW';
    mag2_imp = null;
  end

  select okres from datatookres( cast( :data_imp as date),null,null,null,null) into :okres;
  
  if (coalesce(odbiorca,0) <> 0 ) then begin
    select o.dulica, o.dmiasto, o.dkodp, o.krajid, o.dnrdomu, o.dnrlokalu, o.dtelefon
      from odbiorcy o where o.ref = :odbiorca
    into :dulica_imp, :dmiasto_imp, :dkodp_imp, :dkrajid_imp, :dnrdomu_imp, :dnrlokalu_imp, :dtelefon_imp;
  end

  insert into DOKUMNAG (koryg, MAGAZYN, MAG2, TYP, SYMBOL, data, DOSTAWCA, KLIENT, REFK, WARTSBRUTTO, UWAGI, OPERATOR,
                        ODBIORCAID, DULICA, DMIASTO, DKODP, SPOSDOST, SPOSZAP, UWAGISPED, ACCEPTOPER,
                        DATAWYDANIA, PRIORYTET, MWSSTATUS, DKRAJID, DNRDOMU, DNRLOKALU, DTELEFON,
                        INT_ID, INT_DATAOSTPRZETW, X_SOBOTA, X_ODROCZONEWYDANIE,grupasped, okres, int_symbol, x_imp_ref,  x_pobranie)
  values (:koryg, :magazyn_imp, :mag2_imp, :typ_imp, :symbol_imp, :data_imp, :dostawca, :klient, :refdockoryg, :wartsbrutto_imp, :uwagi_imp, 74,      ---kpi
          :odbiorca, :dulica_imp, :dmiasto_imp, :dkodp_imp, :sposdost, :sposzap, :uwagisped_imp, 74,
         :datawydania_imp,  :priorytet_imp, :mwsordstatus_imp, :dkrajid_imp, :dnrdomu_imp, :dnrlokalu_imp, :dtelefon_imp,
          :nagid_imp, current_timestamp(0), :sobota_imp, :odroczonewydanie_imp,:grupasped, :okres,:symbol_imp, :ref_imp,:wartsbrutto_imp)  ---kpi  zg137134
  returning REF
  into :ref_loc;

-- proba wykonania dodania pozycji do DOKUMPOZ
  select status, msg  from X_IMP_DOKUMPOZ_PROCESS (:ref_imp)
    into :status_poz_loc, :msg_poz_loc;

-- weryfikacja sukcesu dodawania pozycji
  if (:status_poz_loc <> 0) then begin
-- jesli blad --> czyszczenie pozycji i naglowka
    delete from dokumpoz d
      where d.dokument = :ref_loc;

    delete from dokumnag d
      where d.ref = :ref_loc;

    msg = coalesce(msg_poz_loc, '') ||' Usuniete wpisy pozycji i naglowek.';
    status = 8;
    suspend;
    exit;
  end
  else
  if (typ_imp in ('WZ','PZ')) then
  begin
    if (sposdost = 1) then
    begin
      update dokumnag d set d.akcept =1 where d.ref = :ref_loc;
      when any do begin
         msg = coalesce(:symbol_imp, '') ||' Problem przy akceptacji dokumentu.';
        status = 8;
        suspend;
      exit;
      end
    end
  end
-- update -> przetworzony
  status = 0;
  msg = 'ok :)';
  suspend;

end^
SET TERM ; ^
