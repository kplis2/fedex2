--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_CHECK_VERS_IN_MWSCONSTLOC(
      VERS WERSJE_ID,
      MWSCONSTLOC MWSCONSTLOCS_ID,
      SERIA STRING255 ='')
  returns (
      X_MWSSTOCK MWSACTS_ID,
      PARTIA DATE_ID,
      SERIA_OUT STRING255,
      SERIA_REF INTEGER_ID)
   as
begin

   if (seria ='') then seria = null;

   for select distinct s.ref, s.x_partia, x.serialno, x.ref
     from mwsstock S  
       left join x_mws_serie x on (x.ref=s.x_serial_no)
     where s.mwsconstloc=:mwsconstloc
       and s.vers=:vers      
       and s.quantity>0
       and coalesce(x.serialno,:seria) is not distinct from coalesce(:seria,x.serialno)

   into :X_MWSSTOCk, :partia, :seria_out, :seria_ref
   do suspend;
end^
SET TERM ; ^
