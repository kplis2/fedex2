--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_SEARCH_IN_METADATA(
      TEXT STRING255)
  returns (
      NAME STRING35,
      OBJECT STRING20)
   as
begin
 -- procedury
 object = 'procedure';
 for
   select r.rdb$procedure_name
     from rdb$procedures r
     where r.rdb$procedure_source containing :text
     into :name
 do begin
   suspend;
 end

 -- triggery
 name = null;
 object = 'trigger';
 for
   select t.rdb$trigger_name
     from rdb$triggers t
     where t.rdb$trigger_source containing :text
     into :name
 do begin
   suspend;
 end
end^
SET TERM ; ^
