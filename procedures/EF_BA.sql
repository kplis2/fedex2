--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_BA(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      ECOLUMN integer)
  returns (
      AMOUNT numeric(14,2))
   as
declare variable atype smallint;
begin
  --DU: personel - zwraca informacje o wyplatach na konto

  select atype, amount from emplbankaccounts
    where employee = :employee and ecolumn = :ecolumn
    into :atype, :amount;

  if (atype = 0) then
    amount = 1;
  else if (atype = 2) then
    amount = amount / 100;
  suspend;
end^
SET TERM ; ^
