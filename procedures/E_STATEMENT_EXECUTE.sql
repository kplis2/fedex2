--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_STATEMENT_EXECUTE(
      PROCDECL varchar(1024) CHARACTER SET UTF8                           ,
      PROCBODY varchar(1024) CHARACTER SET UTF8                           ,
      KEY1 integer,
      KEY2 integer,
      TEST integer)
  returns (
      RET numeric(16,6))
   as
declare variable proc varchar(8191);
begin
  proc = 'create or alter procedure X_E_CALC' ||
'(key1 integer, key2 integer)
returns (ret numeric(16,6))
as
' || procdecl || '
begin
' || procbody || '
  suspend;
end;';
  execute statement proc;
  if (test = 0) then
  begin
    execute statement 'select ret from X_E_CALC' ||'('|| key1 ||', '|| key2 ||')'
      into ret;
  end
  suspend;
end^
SET TERM ; ^
