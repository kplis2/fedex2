--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDELEGATIONS_COPY_FROM_SRV(
      SRVREQUEST integer)
  returns (
      EDELEGATION integer)
   as
begin /*$$IBE$$ 
  execute procedure GEN_REF('EDELEGATIONS') returning_values edelegation;
  insert into EDELEGATIONS(REF, TYP, REGDATE, FROMDATE, TODATE, SPEDYTOR1, SPEDYTOR2, COUNTRY)
    select :edelegation, 1, crecdate, srecdate, current_date, regnumber, regnumber2, region
    from srvrequests where ref = :srvrequest;
  suspend;
 $$IBE$$*/ SUSPEND;
end^
SET TERM ; ^
