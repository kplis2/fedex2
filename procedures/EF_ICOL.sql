--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_ICOL(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      COL integer)
  returns (
      RET numeric(14,2))
   as
declare variable IPER char(6);
declare variable PAYDAY timestamp;
declare variable PERSON integer;
declare variable separate integer;
begin
/*DU: Personel - funkcja podaje wartosc skladnika COL z innych list plac
                 tego samego okresu ubezpieczeniowego*/

  select iper, payday, coalesce(separate,0)
    from epayrolls
    where ref = :payroll
    into :iper, :payday, separate;
 
  if (separate = 0) then
  begin
    select person
      from employees
      where ref = :employee
      into :person;

    select sum(p.pvalue)
      from eprpos p
        join epayrolls r on (p.payroll = r.ref)
        join eprempl e on (e.epayroll = p.payroll and e.employee = p.employee)
      where e.person = :person
        and p.payroll <> :payroll
        and r.iper = :iper
        and p.ecolumn = :col
        and (r.payday < :payday or (r.payday = :payday and r.ref < :payroll))
        and coalesce(r.separate,0) = 0 --PR30950
      into :ret;
  end

  if (ret is null) then
    ret = 0;

  suspend;
end^
SET TERM ; ^
