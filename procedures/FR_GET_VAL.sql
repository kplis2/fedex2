--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FR_GET_VAL(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      FRHDR varchar(20) CHARACTER SET UTF8                           ,
      FRPSNSYMBOL varchar(15) CHARACTER SET UTF8                           ,
      COL integer)
  returns (
      AMOUNT numeric(15,2))
   as
declare variable psnref integer;
declare variable hdrref integer;
declare variable regdate timestamp;
declare variable frhref varchar(20);
declare variable gener integer;
declare variable company integer;
begin
  execute procedure get_global_param('CURRENTCOMPANY') returning_values :company;

  select FRP.ref, FRP.frhdr
    from frpsns FRP
    where FRP.frhdr = :frhdr and FRP.symbol = :frpsnsymbol
    into :psnref, :frhref;

  select FRH.generalization
    from frhdrs FRH
    where FRH.symbol = :frhref
    into :gener;

  select max(FRVH.ref), max(FRVH.regdate)
    from frvhdrs FRVH
    where FRVH.frhdr = :frhdr and FRVH.period = :period
      and FRVH.company = :company
    into :hdrref, :regdate;

  select FRVP.amount
    from frvpsns FRVP
    where FRVP.col = :col and FRVP.frpsn = :psnref and FRVP.frvhdr = :hdrref
    into :amount;

  amount = coalesce(amount, 0);
  gener = coalesce(gener, 0);
  amount = amount / power(10,:gener);
  suspend;
end^
SET TERM ; ^
