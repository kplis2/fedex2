--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_CHECKKOREKTACEN(
      FAK integer)
  returns (
      STATUS integer)
   as
declare variable korekta integer;
declare variable zakup integer;
declare variable skad smallint;
declare variable sblokadadelay varchar(255);
declare variable blokadadelay integer;
declare variable blokada smallint;
declare variable ktm varchar(40);
declare variable wersjaref integer;
declare variable dostawa integer;
declare variable datadoc timestamp;
declare variable docref integer;
declare variable cnt integer;
declare variable cenanet numeric(14,4);
begin
  status = 0;
  select TYPFAK.KOREKTA, TYPFAK.ZAKUP, NAGFAK.SKAD from TYPFAK join NAGFAK on (NAGFAK.TYP = TYPFAK.SYMBOL) where NAGFAK.REF=:fak into :korekta, :zakup, :skad;
  if( :korekta = 1 or (:zakup <> 1)) then begin
    status = -2;
    exit;
  end
  if(:skad <> 2) then begin
    status = -1;
    exit;
  end
  cnt = null;
  select count(*) from DOKUMNAG where FAKTURA = :fak and zrodlo = 2 into :cnt;
  if(:cnt > 0)then begin
    status = -3;
    exit;
  end
  /*sprawdzenie, czy wystpują różnice cenowe*/
  for select WERSJAREF, CENANET from POZFAK where DOKUMENT=:fak
    into :wersjaref, :cenanet
  do begin
    if(:status= 0) then begin
      cnt = null;
      select count(*) from DOKUMPOZ join DOKUMNAG on (DOKUMNAG.REF=DOKUMPOZ.dokument and DOKUMNAG.FAKTURA = :fak)
        where CENA <> :cenanet and DOKUMPOZ.WERSJAREF =:wersjaref
      into :cnt;

      if(:cnt > 0) then
        status = 1;
    end
  end
  if(:status = 0) then exit;
  /*sprawdzenie, czy zaden dokument zaakceptowany (akcept > 0)
    ktory by podlega omdyfikacjom, nie jest zablokowany ksiegowo*/
  /*dokumenty bezposrednio fakturowane*/
  execute procedure GETCONFIG('SIDFK_BLOKADA') returning_values :sblokadadelay;
  if(:sblokadadelay <> '') then
     blokadadelay = cast(:sblokadadelay as integer);
  for select blokada, data from DOKUMNAG where AKCEPT > 0 and FAKTURA = :fak
    into :blokada, :datadoc
  do begin
    if(:status = 1) then begin
     if(bin_and(:blokada,4)>0 and current_date >= :datadoc + :blokadadelay) then begin
       status = 2;
     end
    end
  end
  /*sprawdzenie pozostaych dokumentów korygowanych*/
  if(:status = 1) then begin
    for select distinct DOD.REF, DOD.BLOKADA,DOD.DATA
       from DOKUMNAG DOS join dokumpoz POS on (DOS.REF = POS.DOKUMENT and DOS.FAKTURA = :fak and DOS.AKCEPT > 0)
                     join DOKUMROZ ROS on (ROS.POZYCJA = POS.REF)
                     join DOKUMROZ ROD on (ROD.cena = ROS.cena and ROD.dostawa = ROS.DOSTAWA)
                     join DOKUMPOZ POD on (POD.KTM = POS.KTM and POD.wersjaref = POS.wersjaref and ROD.POZYCJA = POD.REF)
                     join DOKUMNAG DOD on (DOD.REF = POD.dokument and DOD.MAGAZYN = DOS.MAGAZYN and DOD.AKCEPT > 0)
                     join DEFDOKUM on (DEFDOKUM.symbol = DOD.TYP and DEFDOKUM.wydania = 1)
    into :docref, :blokada, :datadoc
    do begin
      if(:status = 1) then begin
       if(bin_and(:blokada,4)>0 and current_date >= :datadoc + :blokadadelay) then
         status = 2;
      end
    end
  end
end^
SET TERM ; ^
