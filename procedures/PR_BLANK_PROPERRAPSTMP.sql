--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_BLANK_PROPERRAPSTMP(
      PRDEPART varchar(20) CHARACTER SET UTF8                           ,
      EMPLOYEE integer,
      PRSCHEDOPER integer,
      PRSCHEDGUIDE integer,
      PRNAGZAM integer,
      PRPOZZAM integer,
      PROPER varchar(20) CHARACTER SET UTF8                           ,
      PRSHORTAGETYPE varchar(20) CHARACTER SET UTF8                           ,
      MAXQ smallint)
   as
declare variable PRSCHEDOPERRAP integer;
declare variable PRSCHEDGUIDERAP integer;
declare variable QUANTITY numeric(14,4);
declare variable QUANTITYTMP numeric(14,4);
declare variable KTM varchar(40);
declare variable WERSJA integer;
declare variable PRSCHEDGUIDESYMB varchar(40);
declare variable NAGZAMID varchar(40);
declare variable PRNAGZAMRAP integer;
declare variable PRPOZZAMRAP integer;
begin
  for
    select o.ref, o.guide, o.amountin - o.amountresult - o.amountshortages,
        g.ktm, g.wersja, g.symbol, n.id, g.zamowienie, g.pozzam
      from prschedopers o
        left join prschedguides g on (g.ref = o.guide)
        left join nagzam n on (n.ref = g.zamowienie)
      where o.prdepart = :prdepart and o.activ = 1 and o.status = 2
        and (o.ref = :prschedoper or :prschedoper is null or :prschedoper = 0)
        and (o.oper = :proper or :proper is null or :proper = '')
        and (g.ref = :prschedguide or :prschedguide is null or :prschedguide = 0)
        and (g.zamowienie = :prnagzam or :prnagzam is null or :prnagzam = 0)
        and (g.pozzam = :prpozzam or :prpozzam is null or :prpozzam = 0)
      into prschedoperrap, prschedguiderap, quantity,
        ktm, wersja, prschedguidesymb, nagzamid, prnagzamrap, prpozzamrap
  do begin
    quantitytmp = 0;
    if (quantity is null) then quantity = 0;
    if (maxq = 0) then
      quantity = 0;
    select sum(t.amount)
      from propersrapstmp t
        left join prshortagestypes s on (s.symbol = t.prshortagetype)
      where t.prschedoper = :prschedoperrap
        and (t.prshortagetype is null or t.prshortagetype = '' or s.noamountinfluence = 0 or s.symbol is null)
      into quantitytmp;
    if (quantitytmp is null) then quantitytmp = 0;
    quantity = quantity - quantitytmp;
    if (quantity > 0) then
      insert into propersrapstmp (connection, employee, amount, prschedoper,
          prschedguide, prnagzam, prpozzam, proper, prshortagetype, prdepart,
          ktm, wersja, prschedguidesymb, nagzamid)
        values (current_connection, :employee, :quantity, :prschedoperrap,
            :prschedguiderap, :prnagzamrap, :prpozzamrap, :proper, :prshortagetype, :prdepart,
            :ktm, :wersja, :prschedguidesymb, :nagzamid);
  end
end^
SET TERM ; ^
