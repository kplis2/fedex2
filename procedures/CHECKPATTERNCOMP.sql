--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHECKPATTERNCOMP
  returns (
      PATTERN smallint)
   as
declare variable CURRENT_COMAPNY COMPANIES_ID;
declare variable PATT SMALLINT_ID;
begin
   -- ściągamy obecny zakład
  execute procedure get_global_param('CURRENTCOMPANY')
    returning_values :current_comapny;
  select c.pattern
    from companies c
    where c.ref = :current_comapny
    into :patt;
  if (patt = 1) then
    pattern = 1;
  else
    pattern = 0;
  suspend;
end^
SET TERM ; ^
