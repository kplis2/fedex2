--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_IMP_TOWARY_PROCESS(
      REF_IMP INTEGER_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING1024)
   as
declare variable IDARTYKULU_IMP INTEGER_ID;
declare variable INDEKSKATALOGOWY_IMP STRING40;
declare variable INDEKSHANDLOWY_IMP STRING40;
declare variable NAZWA_IMP STRING80;
declare variable NAZWA2_IMP STRING255;
declare variable UWAGI_IMP STRING1024;
declare variable UWAGIDLAMAGAZYNYPZ_IMP STRING2500;
declare variable UWAGIDLAMAGAZYNYWZ_IMP STRING2500;
declare variable KATEGORIAARTYKULU_IMP STRING60;
declare variable RODZAJ_IMP STRING20;
declare variable AKT_IMP SMALLINT_ID;
declare variable JEDN_IMP STRING255;
declare variable WAGA_IMP NUMERIC_14_4;
declare variable WYSOKOWSC_IMP NUMERIC_14_4;
declare variable SZEROKOSC_IMP NUMERIC_14_4;
declare variable DLUGOSC_IMP NUMERIC_14_4;
declare variable KODKRESK_IMP BARCODE;
declare variable STATUS_IMP SMALLINT_ID;
declare variable DATAPRZET_IMP TIMESTAMP_ID;
declare variable IDSTATUS_IMP SMALLINT_ID;
declare variable MIARA_LOC STRING10;
declare variable INT_ID_LOC INTEGER_ID;
declare variable REF_TOWJEDN_LOC INTEGER_ID;
declare variable NUMER_TOWTYPES_LOC INTEGER_ID;
declare variable JEDN JEDN_MIARY;
declare variable USLUGA integer_id;
declare variable IDART STRING40;
declare variable numer_towtypes integer_id;
begin
  status = 8;
  msg = '';

  if (not exists(select first 1 1 from x_imp_towary x where x.ref = :ref_imp)) then begin
    msg = 'Bledny ref_imp';
    status = 8;
    suspend;
    exit;
  end

  select x.idartykulu, x.indekskatalogowy, x.indekshandlowy, x.nazwa, x.nazwa2, x.uwagi, x.uwagidlamagazynupz, x.uwagidlamagazynuwz, x.kategoriaartykulu,
      x.rodzaj, x.akt, x.jedn, x.waga, x.wysokosc, x.szerokosc, x.dlugosc, x.kodkresk, x.status, x.dataprzet, x.idstatus
      from x_imp_towary x
      where x.ref = :ref_imp --do przetworzenia
  into  :idartykulu_imp, :indekskatalogowy_imp, :indekshandlowy_imp, :nazwa_imp, :nazwa2_imp, :uwagi_imp, :uwagidlamagazynypz_imp, :uwagidlamagazynywz_imp, :kategoriaartykulu_imp,
     :rodzaj_imp, :akt_imp, :jedn_imp, :waga_imp, :WYSOKOWSC_IMP, :szerokosc_imp, :dlugosc_imp, :kodkresk_imp, :status_imp, :dataprzet_imp, :idstatus_imp;



-- wertyfikacja KTM -> TOWARY
  if (coalesce(idartykulu_imp, 0) = 0) then begin
    msg = 'Brak IdArtykulu';
    status = 8;
    suspend;
    exit;
  end
-- weryfikacja miary -> MIARA -> TOWJEDN
  if (coalesce(jedn_imp, '') = '') then begin
    msg = 'Brak Jednostki';
    status = 8;
    suspend;
    exit;
  end
-- weryfikacja uslugi -> TOWTYPES
  if (coalesce(rodzaj_imp, '') = '') then begin
    msg = 'Brak Rodzaju';
    status = 8;
    suspend;
    exit;
  end

-- jesli nie ma jednostki to dodajemy
    select first 1 miara from miara m where upper(m.opis) = upper(trim(:jedn_imp))
        into :miara_loc;
  --akt w Hermonie jest to pole zablokowany wiec znaczy dokladnie odwrotnie stad potrzebna zamiana
  if (coalesce(akt_imp,0) = 0 ) then
    akt_imp = 1;
  else
    akt_imp = 0;
  if (coalesce(:miara_loc, '') = '') then begin
    jedn = substring(replace(trim(:jedn_imp),  ' ','') from 1 for 5);
    insert into MIARA (MIARA, OPIS, DOKLADNOSC, STATE, TOKEN)
      values (:jedn, :jedn_imp, 3, 0, 9)
    returning miara into :miara_loc;
  end
  --znalezienie typu towaru
  select tt.numer
      from towtypes tt
      where upper(tt.id_zewn) like  trim(upper(:rodzaj_imp))
    into :usluga;
  --jezeli nie ma takiego typu towaru to dodaje nowy
  if (coalesce(usluga,0) = -1) then begin
    --ustalam numer
    select max(numer) from towtypes 
      into :numer_towtypes;
    numer_towtypes = coalesce(:numer_towtypes,10);  --ponizej 10 sa systemowe
    numer_towtypes = numer_towtypes + 1;
    insert into TOWTYPES (NUMER, NAZWA, SYSTEMOWY,  NAZWAPOJ, SYMBOL, ID_ZEWN)
                values (:numer_towtypes, :rodzaj_imp, 0, :rodzaj_imp, 'M',:rodzaj_imp);
    usluga = numer_towtypes;
  end
-- jesli nie ma towaru
  idart= cast( :idartykulu_imp as string40);
 if (not exists (select first 1 1 from towary t where t.int_id = :idart )) then begin
-- insert -
    insert into TOWARY (KTM, NAZWA, MIARA, AKT, GRUPA, KODKRESK, KODPROD, INT_ID, INT_DATAOSTPRZETW, VAT, usluga,
                        X_INDEKS_KAT, X_NAZWA2, X_UWAGI, X_UWAGI_MAG_PZ, X_UWAGI_MAG_WZ, x_imp_ref)
      values (:idartykulu_imp, :nazwa_imp, :miara_loc, :akt_imp, :kategoriaartykulu_imp, :kodkresk_imp, :indekshandlowy_imp, :idart, current_timestamp(0), '23', :usluga,
            :indekskatalogowy_imp, :nazwa2_imp, :uwagi_imp, :uwagidlamagazynypz_imp, :uwagidlamagazynywz_imp, :ref_imp)
    returning int_id into :int_id_loc;
  end else begin
-- update -
    update towary set KTM=:idartykulu_imp, NAZWA=:nazwa_imp, MIARA=:miara_loc,
            AKT=:akt_imp, GRUPA = :kategoriaartykulu_imp, KODKRESK=:kodkresk_imp,
            KODPROD= :indekshandlowy_imp, INT_ID=:idart, INT_DATAOSTPRZETW =current_timestamp(0),
            VAT ='23' , usluga= :usluga, X_INDEKS_KAT =:indekskatalogowy_imp,
            X_NAZWA2 = :nazwa2_imp, X_UWAGI = :uwagi_imp,
            X_UWAGI_MAG_PZ = :uwagidlamagazynypz_imp, X_UWAGI_MAG_WZ = :uwagidlamagazynywz_imp,
            x_imp_ref = :ref_imp
        where ktm = :idart;
  end

-- insert update towjedn
  update or insert into TOWJEDN (KTM, GLOWNA, JEDN, WAGA, KODKRESK, WYS, DLUG, SZER, INT_ID, INT_DATAOSTPRZETW,
    przelicz, const)
    values (:idartykulu_imp, 1, :miara_loc, :waga_imp, :kodkresk_imp, :wysokowsc_imp, :dlugosc_imp, :szerokosc_imp, :int_id_loc, current_timestamp(0),
    1.0, 1)
    matching (KTM, GLOWNA)
  returning (REF) into :ref_towjedn_loc;

-- update -> przetworzony
  status = 0;
  msg = 'ok :)';
  suspend;
end^
SET TERM ; ^
