--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CRM_FAKTURY_NIEZAP(
      KLIENT integer,
      PROG1 integer,
      PROG2 integer,
      PROG3 integer)
  returns (
      ZAKRES varchar(255) CHARACTER SET UTF8                           ,
      ILDOKUM integer,
      WARTNET numeric(14,2),
      WARTBRU numeric(14,2),
      SUMZAP numeric(14,2),
      ZAPLACONO numeric(14,2),
      DOZAP numeric(14,2))
   as
declare variable ildni integer;
declare variable datod timestamp;
declare variable datdo timestamp;
begin
  ildni = 0;
  ZAKRES = 'Faktury nieprzeterminowane';
  ildokum = 0;
  select count(*),sum(SUMWARTNETZL), sum(SUMWARTBRUZL), sum(SUMWARTBRU),sum(DOZAPLATY*KURS),sum(ZAPLACONO*KURS)
    from NAGFAK where KLIENT = :KLIENT AND AKCEPTACJA=1 AND DOZAPLATY <> ZAPLACONO AND
      current_date < termzap
    into :ildokum,:wartnet, :wartbru, :sumzap, :dozap, :zaplacono;

  suspend;
  if(:prog1 > ildni) then begin
    ZAKRES='Naleznosci przeterminowane do '||:prog1||' dni';
    datdo = current_date - :ildni;
    datod = current_date - :prog1;
    ildokum = 0;
    select count(*),sum(SUMWARTNETZL), sum(SUMWARTBRUZL), sum(SUMWARTBRU),sum(DOZAPLATY*KURS),sum(ZAPLACONO*KURS)
       from NAGFAK where KLIENT = :KLIENT AND AKCEPTACJA=1 AND DOZAPLATY <> ZAPLACONO AND
         termzap > :datod AND termzap <= :datdo
       into :ildokum,:wartnet, :wartbru, :sumzap, :dozap,:zaplacono;
/*    dozap = :sumzap - :zaplacono;*/
    ildni = :prog1;
    suspend;
  end
  if(:prog2>ildni) then begin
    datdo = current_date - :ildni;
    datod = current_date - :prog2;
    ZAKRES='Naleznosci przeterminowane do '||:prog2||' dni';
    ildokum = 0;
    select count(*),sum(SUMWARTNETZL), sum(SUMWARTBRUZL), sum(SUMWARTBRU), sum(DOZAPLATY*KURS),sum(ZAPLACONO*KURS)
       from NAGFAK where KLIENT = :KLIENT AND AKCEPTACJA=1 AND DOZAPLATY <> ZAPLACONO AND
         termzap > :datod AND termzap <= :datdo
       into :ildokum,:wartnet, :wartbru, :sumzap, :dozap, :zaplacono;
    ildni = :prog2;
    suspend;
  end
  if(:prog3>ildni) then begin
    datdo = current_date - :ildni;
    datod = current_date - :prog3;
    ildokum = 0;
    ZAKRES='Naleznosci przeterminowane do '||:prog3||' dni';
    select count(*),sum(SUMWARTNETZL), sum(SUMWARTBRUZL),sum(SUMWARTBRU), sum(DOZAPLATY*KURS),sum(ZAPLACONO*KURS)
       from NAGFAK where KLIENT = :KLIENT AND AKCEPTACJA=1 AND DOZAPLATY <> ZAPLACONO AND
         termzap > :datod AND termzap <= :datdo
       into :ildokum,:wartnet, :wartbru, :sumzap, :dozap, :zaplacono;
    ildni = :prog3;
    suspend;
  end
  ildokum = 0;
  datdo = current_date - :ildni;
  select count(*),sum(SUMWARTNETZL), sum(SUMWARTBRUZL), sum(SUMWARTBRU),sum(DOZAPLATY*KURS),sum(ZAPLACONO*KURS)
     from NAGFAK where KLIENT = :KLIENT AND AKCEPTACJA=1 AND DOZAPLATY <> ZAPLACONO AND
       termzap <= :datdo
     into :ildokum,:wartnet, :wartbru, :sumzap, :dozap, :zaplacono;
  if(:ildni > 0) then
    ZAKRES='Naleznosci przeterminowane powyżej '||:ildni||' dni';
  else
    ZAKRES='Naleznosci peterminowane';
  suspend;
end^
SET TERM ; ^
