--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EGUS_TWELVE_GETDATA_D_V2010(
      GYEAR varchar(4) CHARACTER SET UTF8                           ,
      EMPLOYEE integer)
  returns (
      D1 numeric(4,2),
      D2 numeric(14,2),
      D3 numeric(14,2),
      D4 numeric(14,2),
      D5 numeric(14,2),
      D6 numeric(14,2),
      D7 integer,
      D8 numeric(14,2),
      D9 numeric(14,2),
      D10 numeric(14,2),
      D11 numeric(14,2),
      D12 numeric(14,2),
      D13 numeric(14,2),
      D14 numeric(14,2),
      D15 numeric(14,2))
   as
declare variable period varchar(6);
declare variable pnomworkdays integer;
declare variable fromdate date;
declare variable todate date;
declare variable wdimdays numeric(14,2);
declare variable pwdimdays numeric(14,2);
declare variable brutto numeric(14,2);
declare variable outbrutto numeric(14,2);
begin
/*MWr Personel: procedura generuje dane do sprawozdania GUS Z-12 (na rok 2010)
  dla zadanego pracownika EMPLOYEE. Odpowiada za czesc D raportu - czas pracy
  oraz wynagr. za rok GYEAR. Specyfikacja zmiennych wyjsciowych w dokumenacji*/

  d2 = 0;
  wdimdays = 0;
  period = gyear || '01';

/*D1: suma miesiecznych wskaznikow wymiaru czasu pracy za rok GYEAR oraz
  D2: liczba dni roboczych w roku GYEAR */
  while (substring(period from 1 for 4) = gyear) do
  begin
    execute procedure period2dates(period)
      returning_values :fromdate, :todate;

    select wdimdays, nomworkdays
      from egus_twelve_worktime(1, :employee, :fromdate, :todate)
      into :pwdimdays, :pnomworkdays;

    wdimdays = wdimdays + pwdimdays;
    d2 = d2 + pnomworkdays;

    execute procedure e_func_periodinc(period,1)
      returning_values :period;
  end

  if (d2 > 0) then
    d1 = (12.00 * wdimdays) / (1.00 * d2);

  fromdate = cast(gyear || '/1/1' as date);
  todate = cast(gyear || '/12/31' as date);

/*D3: czas faktycznie przepracowny w godz nominalnych,
  D4: czas faktycznie przepracowny w godz nadliczbowych,
  D5: czas nieprzepracowany ogolem,
  D6: czas nieprzepracowany oplacony tylko przez zaklad pracy (D5 >= D6)*/
  select workedhours, extrahours, notworkedhours, notwhours_works
    from egus_twelve_worktime(2, :employee, :fromdate, :todate)
    into :d3, :d4, :d5, :d6;

--D7: liczba dni urlopow wypoczynkowych wykorzystanych w roku
  select limitconsd from evaclimits
    where employee = :employee and vyear = :gyear
    into :d7;
  d7 = coalesce(d7,0);

--WYNAGRODZENIA ZA CALY ROK ---------------------------------------------------
  select sum(case when p.ecolumn = 4000 then p.pvalue else 0 end) --brutto do wyliczenia D9
    , sum(case when p.ecolumn in (1600,2100,2110,2120,2200,2300,2400)then p.pvalue else 0 end) --wykluczenia z brutto
    , 0 --D10: tylko premia REGULAMINOWA (np 1/3 premi za kwartal) [BRAK W SYSTEMIE]
    , sum(case when p.ecolumn in (1400,2000) then p.pvalue else 0 end) --D11: premie i nagrody uznaniowe
    , sum(case when p.ecolumn in (1200,1210) then p.pvalue else 0 end) --D12: wyn. za prace w godz. nadliczowych
    , 0 --D13: honoraria [BRAK W SYSTEMIE]
    , 0 --D14: dodatkowe wynagr. dla pracownikow budzetowki [BRAK W SYSTEMIE]
    , 0 --D15: udzial w zysku lub w nadwyzce bilansowej w spoldzielniach [BRAK W SYSTEMIE]
    from epayrolls r
      join eprpos p on (p.payroll = r.ref and r.cper starting with :gyear and r.empltype = 1)
    where p.employee = :employee
    into :brutto, :outbrutto, :d10, :d11, :d12, :d13, :d14, :d15;

  d9 = brutto - outbrutto - d10 - d11 - d12 - d13 - d14 - d15;
  d8 = d9 + d12;

  suspend;
end^
SET TERM ; ^
