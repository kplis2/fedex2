--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDOPER_ALT_SET(
      PRSCHEDGUIDE integer,
      PRSHOPER integer,
      ACTIV smallint,
      PRSHOPERDOWN integer)
   as
declare variable prshoperm integer;
declare variable prshopermtype smallint;
declare variable prshopercur integer;
declare variable prshopercomplex smallint;
declare variable prshoperscheduled integer;
declare variable prshopertype integer;
declare variable prshoperdownfirst integer;
declare variable prsheet integer;
begin
  if(activ = -1) then begin
  --aktywacja/deaktywacja raportowalnych w dol
    select shm.opertype, shm.ref
      from prshopers sh
      left join prshopers shm on (sh.opermaster = shm.ref)
      where sh.ref = :prshoper
    into :prshopermtype, :prshoperm;
    if(prshoperm is not null) then begin
      if(:prshopermtype = 1) then
        execute procedure PRSCHEDOPER_ALT_SET(:prschedguide, :prshoperm, 0, :prshoper);
      else
        execute procedure PRSCHEDOPER_ALT_SET(:prschedguide, :prshoperm, 1, :prshoper);
      execute procedure prschedoper_alt_set(:prschedguide, :prshoperm, -1, null);
    end
    --aktywacja/deaktywacja raportowalnych w gore
  end else if(activ = 0) then begin
    for select ref, complex, scheduled
      from prshopers
      where opermaster = :prshoper and (:prshoperdown is null or ref <> :prshoperdown)
      into :prshopercur, :prshopercomplex, :prshoperscheduled
    do begin
      if(prshoperscheduled = 1) then
        update prschedopers set activ = 0, verified = 1 where guide = :prschedguide and activ = 1 and shoper = :prshopercur;
      else if(:prshopercomplex = 1) then execute procedure prschedoper_alt_set(:prschedguide, :prshopercur, 0, null);
    end
  end else if (activ = 1 or activ = 2) then begin
    for select ref, complex, scheduled, opertype
      from prshopers
      where opermaster = :prshoper and (:prshoperdown is null or :activ = 1 and ref <> :prshoperdown
        or :activ = 2 and ref = :prshoperdown)
      into :prshopercur, :prshopercomplex, :prshoperscheduled, :prshopertype
    do begin

      if(prshoperscheduled = 1) then begin
        update prschedopers set activ = 1, verified = 1 where guide = :prschedguide and activ = 0 and shoper = :prshopercur;
      end else if(:prshopercomplex = 1) then begin
        --jesli cos jest już aktywnego to nie weryfikujemy aktywnosci
        select sheet from prshopers where ref = :prshopercur into :prsheet;
        if(not exists (select ref from prschedopers where guide = :prschedguide and activ = 1 and shoper in (select prshoper from prshopers_down(:prsheet,:prshopercur)))) then begin
          if(:prshopertype = 1) then begin
            select operdefault from prshopers where ref = :prshopercur into :prshoperdownfirst;
            execute procedure prschedoper_alt_set(:prschedguide, :prshopercur, 2, :prshoperdownfirst);
            execute procedure prschedoper_alt_set(:prschedguide, :prshopercur, 0, :prshoperdownfirst);
          end else begin
            execute procedure prschedoper_alt_set(:prschedguide, :prshopercur, 1, null);
          end
        end
      end
    end
  end
end^
SET TERM ; ^
