--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_DIFFVAL_AFTER_EABSCORRECT(
      CORRECTED integer,
      AREF integer,
      IN_DAYS integer = null,
      IN_ISZUS smallint = null,
      IN_WORKPAY numeric(14,2) = null,
      IN_ZUSPAY numeric(14,2) = null)
  returns (
      DAYS integer,
      WORKPAY numeric(14,2),
      ZUSPAY numeric(14,2),
      ISZUS smallint,
      CORRECT_INFO varchar(10) CHARACTER SET UTF8                           )
   as
declare variable T_DAYS integer;
declare variable T_WORKPAY numeric(14,2);
declare variable T_ZUSPAY numeric(14,2);
declare variable T_ISZUS smallint;
begin
/*MWr Personel: Procedura zwraca roznice jakie wprowdza korekta CORRECTED
    nieobecnosci AREF. Wartosci AREF mozemy zadac w zmiennych o prefiksie IN_
    badz pobrac bezposrednio z tab. nieobecnosci wpisujac w  AREF ref nieob. */

  if (aref is not null) then
  begin
    select days,
        case when ecolumn in (90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450) then 1 else 0 end,
        case when ecolumn in (90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450) then payamount else null end,
        case when ecolumn in (90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450) then null else payamount end
      from eabsences
      where ref = :aref
      into :days, iszus, :zuspay, :workpay;
  end else begin
    days = in_days;
    iszus = in_iszus;
    workpay = in_workpay;
    zuspay = in_zuspay;
  end

  select days,
      case when ecolumn in (90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450) then 1 else 0 end,
      case when ecolumn in (90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450) then payamount else null end,
      case when ecolumn in (90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450) then null else payamount end
    from eabsences
    where ref = :corrected
    into :t_days, :t_iszus, :t_zuspay, :t_workpay;

  correct_info = 'K';
  if (iszus = t_iszus) then
    days = days - coalesce(t_days,0);
  if (iszus = 1) then begin
    zuspay = coalesce(zuspay,0) - coalesce(t_zuspay,0);
    workpay = null;
  end else begin
    workpay = coalesce(workpay,0) - coalesce(t_workpay,0);
    zuspay = null;
  end
  iszus = t_iszus;

  suspend;

end^
SET TERM ; ^
