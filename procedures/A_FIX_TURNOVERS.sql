--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE A_FIX_TURNOVERS(
      COMPANY integer)
   as
declare variable PERIOD varchar(6);
declare variable DEBIT numeric(14,2);
declare variable CREDIT numeric(14,2);
declare variable ACCOUNTING integer;
declare variable EBDEBIT numeric(14,2);
declare variable EBCREDIT numeric(14,2);
begin
  --jeszcze nie obsluguje przypadku ze jest dekret a konto jest nieotwarte (brak w accounting)

  insert into turnovers (accounting, period, company)
    select a.ref, p.id, a.company
      from accounting a
        join bkperiods p on (a.yearid = p.yearid and a.company = p.company)
        left join turnovers t on (t.period = p.id and a.ref = t.accounting)
      where t.accounting is null and a.company = :company;

  --PLN
  for
    select a.ref, t.period, sum(d.debit), sum(d.credit), sum(case when d.status = 3 then d.debit else 0 end),sum(case when d.status = 3 then d.credit else 0 end)
      from accounting a
        join turnovers t on (a.ref = t.accounting)
        left join decrees d on (t.account = d.account and t.period = d.period and d.company = t.company and d.status > 1)
      where a.currency = 'PLN' and a.company = :company
      group by a.ref, t.period
      having sum(coalesce(d.debit,0)) <> max(coalesce(t.debit,0))
        or sum(coalesce(d.credit,0)) <> max(coalesce(t.credit,0))
        or sum(case when d.status = 3 then d.debit else 0 end) <> max(coalesce(t.ebdebit,0))
        or sum(case when d.status = 3 then d.credit else 0 end) <> max(coalesce(t.ebcredit,0))
      into :accounting, :period, :debit, :credit, :ebdebit, :ebcredit
  do begin
    update turnovers
      set debit = coalesce(:debit, 0), credit = coalesce(:credit, 0), ebdebit = coalesce(:ebdebit, 0), ebcredit = coalesce(:ebcredit, 0)
      where accounting = :accounting and period = :period and company = :company;
  end

  --walutowe
  for
    select a.ref, t.period, sum(d.currdebit), sum(d.currcredit), sum(case when d.status = 3 then d.currdebit else 0 end),sum(case when d.status = 3 then d.currcredit else 0 end)
      from accounting a
        join turnovers t on (a.ref = t.accounting)
        left join decrees d on (a.currency = d.currency and t.account = d.account and t.period = d.period and d.company = t.company and d.status > 1)
      where a.currency <> 'PLN' and a.company = :company
      group by a.ref, t.period
      having sum(coalesce(d.currdebit,0)) <> max(coalesce(t.debit,0))
        or sum(coalesce(d.currcredit,0)) <> max(coalesce(t.credit,0))
        or sum(case when d.status = 3 then d.currdebit else 0 end) <> max(coalesce(t.ebdebit,0))
        or sum(case when d.status = 3 then d.currcredit else 0 end) <> max(coalesce(t.ebcredit,0))
      into :accounting, :period, :debit, :credit, :ebdebit, :ebcredit
  do begin
    update turnovers
      set debit = coalesce(:debit, 0), credit = coalesce(:credit, 0), ebdebit = coalesce(:ebdebit, 0), ebcredit = coalesce(:ebcredit, 0)
      where accounting = :accounting and period = :period and company = :company;
  end
end^
SET TERM ; ^
