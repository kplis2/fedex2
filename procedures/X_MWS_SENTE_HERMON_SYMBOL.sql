--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_SENTE_HERMON_SYMBOL(
      HERMON_SYMBOL STRING20)
  returns (
      SENTE_SYMBOL STRING20,
      STATUS SMALLINT_ID)
   as
begin
  SENTE_SYMBOL = '';
  status = 1;

  select m.symbol        -- dla symboli hermon
    from mwsconstlocs m
     where m.x_ref_hermon = :hermon_symbol
  into :sente_symbol;

  if (sente_symbol <> '') then begin
    suspend;
    exit;
  end

  select m.symbol        -- jesli ktos wpisze symbol sente
    from mwsconstlocs m
     where m.symbol = :hermon_symbol
  into :sente_symbol;

  if (sente_symbol <> '') then begin
    suspend;
    exit;
  end

-- dla koszykow nie potrzebujemy ? koszyk powinno znalezc wyzej
  if (hermon_symbol similar to 'P[[:DIGIT:]]{3}') then
    sente_symbol = hermon_symbol;

  if (sente_symbol <> '') then begin
    suspend;
    exit;
  end

  if (sente_symbol = '') then begin
    status = 0;
    exit;
    -- nie ma takiej lokacji
  end

end^
SET TERM ; ^
