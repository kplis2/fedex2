--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXP2PLATNIK_ZIUA(
      PERSONREF integer,
      COMPANY integer)
  returns (
      PERSON integer,
      PESEL varchar(11) CHARACTER SET UTF8                           ,
      NIP varchar(15) CHARACTER SET UTF8                           ,
      FNAME varchar(60) CHARACTER SET UTF8                           ,
      SNAME varchar(30) CHARACTER SET UTF8                           ,
      MIDDLENAME varchar(30) CHARACTER SET UTF8                           ,
      BIRTHDATE date,
      OLDPESEL varchar(11) CHARACTER SET UTF8                           ,
      OLDNIP varchar(15) CHARACTER SET UTF8                           ,
      OLDFNAME varchar(60) CHARACTER SET UTF8                           ,
      OLDSNAME varchar(30) CHARACTER SET UTF8                           ,
      OLDBIRTHDATE date,
      DATA date,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      DOCNO varchar(11) CHARACTER SET UTF8                           ,
      OLDDOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      OLDDOCNO varchar(11) CHARACTER SET UTF8                           )
   as
begin
  --MWr: Personel - Zgloszenie zmiany danych identyfikacyjnych osoby ubezpieczonej

  for
    select p.ref,
        case when (ez.ispesel = 1) then p.pesel else '' end as pesel,
        case when (ez.isnip = 1) then p.nip else '' end as nip,
        p.fname, p.sname, p.middlename, p.birthdate,
        case when (ez.isother = 1 and p.evidenceno > '') then '1' when (ez.isother = 2 and p.passportno > '') then '2' else '' end as doctype,
        case when (ez.isother = 1 and p.evidenceno > '') then p.evidenceno when (ez.isother = 2 and p.passportno > '') then p.passportno else '' end as docno,
        case when (ez.ispesel = 1) then ep.pesel else '' end as OldPesel, case when (ez.isnip = 1) then p.nip else '' end as OldNip,
        ep.fname as OldFname, ep.sname as OldSname,  ep.birthdate as OldBirthdate, ep.data,
        case when (ez.isother = 1 and ep.evidenceno > '') then '1' when (ez.isother = 2 and ep.passportno > '') then '2' else '' end as OldDoctype,
        case when (ez.isother = 1 and ep.evidenceno > '') then ep.evidenceno when (ez.isother = 2 and ep.passportno > '') then ep.passportno else '' end as OldDocno
      from persons p
        join eperscompany s on (p.ref = s.person and s.company = :company)
        join eperchanges ep on (p.ref = ep.person and ep.status is null) --wylaczenie wnioskow (BS48023)
        left join ezusdata ez on (ez.person=p.ref)
      where p.ref = :personref
        and ez.fromdate = (select max(ez1.fromdate) from ezusdata ez1 where ez1.person = ez.person)
      order by ep.data, ep.person
      into :person, :pesel, :nip, :fname, :sname, :middlename, :birthdate,  :doctype, :docno,
        :OldPesel, :OldNip, :OldFname, :OldSname, :OldBirthdate, :Data , :olddoctype, :olddocno
  do
    suspend;

end^
SET TERM ; ^
