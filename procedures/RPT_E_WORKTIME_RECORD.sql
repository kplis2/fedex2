--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_E_WORKTIME_RECORD(
      EMPL integer,
      YEARID char(4) CHARACTER SET UTF8                           ,
      MONTHID char(2) CHARACTER SET UTF8                           )
  returns (
      WORKTIME numeric(14,6),
      RWORKTIME numeric(14,6),
      MDAYKIND smallint,
      JWORKTIME numeric(14,6),
      NJWORKTIME numeric(14,6),
      ABSENCETYPE varchar(20) CHARACTER SET UTF8                           ,
      EXTRAHOURS numeric(14,6),
      FIFTYHOURS numeric(14,6),
      HUNDREDHOURS numeric(14,6),
      NIGHTHOURS numeric(14,6))
   as
declare variable daykind smallint;
declare variable tmpstddate timestamp;
declare variable absence integer;
declare variable proportional integer;
declare variable dimnum numeric(14,2);
declare variable dimden numeric(14,2);
declare variable i integer = 0;
declare variable daytype smallint;
declare variable stdcalendar integer;
BEGIN

  execute procedure get_config('STDCALENDAR',2)
    returning_values :stdcalendar;
  --przechodzimy przez wszystkie dni (z danego roku i miesiaca) z kalendarza firmowego
  for
    select CD.cdate, dk.daykind --edk.daytype
      from ecaldays CD
        join edaykinds dk on dk.ref = CD.daykind
        join ecalendars C on (C.ref = CD.calendar)
      where C.ref = :stdcalendar  --BS81336
        and CD.cyear = :yearid
        and (CD.cmonth = :monthid or :monthid='13')
      order by cd.cdate
      into :tmpstddate, :mdaykind
  do begin
    i = :i +1;
    jworktime = 0;
    njworktime = 0;
    extrahours = 0;
    nighthours = 0;
    fiftyhours = 0;
    hundredhours = 0;
    worktime = 0;
    rworktime = 0;

    select CD2.worktime, dk.daykind, dk.daytype, EM.proportional, EM.dimnum, EM.dimden
      --dni kalendarza pracownika (stad pobierany jest m.in. czas pracy)
      from emplcaldays CD2
        join employment EM on (EM.employee = CD2.employee) and EM.fromdate <= :tmpstddate
              and coalesce(EM.todate, :tmpstddate) >= :tmpstddate
        join edaykinds dk on dk.ref = CD2.daykind
      where CD2.employee = :empl
        and (CD2.cdate = :tmpstddate)
      into :worktime, :daykind, :daytype, :proportional, :dimnum, :dimden;

    absencetype = '';
    if (worktime is not null and daykind is not null) then
    begin
      --pracownik w danym dniu kalendarza pracowal, wiec ustawiamy mdaykind na daykind
      --kalendarza pracownika
      mdaykind = daykind;

      if (worktime = 0) then
        worktime = null;
      else
        worktime = worktime / 3600;  -- liczba godzin przepracowanych w danym dniu

      if (proportional = 1) then
        worktime = worktime * dimnum / dimden;

      select sum(W.amount)
        from eworkedhours W
        where W.employee = :empl and hdate = :tmpstddate and ecolumn = 600
        into :fiftyhours;

      select sum(W.amount)
        from eworkedhours W
        where W.employee = :empl and hdate = :tmpstddate and ecolumn = 610
        into :hundredhours;

      fiftyhours = coalesce(fiftyhours, 0);
      hundredhours = coalesce(hundredhours, 0);

      extrahours = fiftyhours + hundredhours;

      select sum(W.amount)
        from eworkedhours W
        where W.employee = :empl and hdate = :tmpstddate and ecolumn = 640
        into :nighthours;

      extrahours = coalesce (extrahours, 0);
      nighthours = coalesce (nighthours, 0);

      worktime = worktime + extrahours;

      if (exists(select ref from eabsences A
            where A.employee = :empl and A.fromdate <= :tmpstddate and A.todate >= :tmpstddate and A.correction in (0,2))
      ) then begin

        select C.number, coalesce(P.pval, substring(C.name from 1 for 2)) from eabsences A
            join ecolumns C on (C.number = A.ecolumn)
            left join ecolparams P on (P.ecolumn = C.number and P.param = 'SYMBOL')
          where A.employee = :empl
            and A.fromdate <= :tmpstddate and A.todate >= :tmpstddate
            and A.correction in (0,2)
          into :absence, :absencetype;

        --jest nieobecnosc wiec 0 (bylo -1) (jesli dzien roboczy)
        if (daytype = 1) then
          rworktime = 0;
        else
          rworktime = null;

        if (absence = 1) then
        --obecnosc nieusprawiedliwiona
          njworktime = worktime;
        else
        --pozostale nieobecnosci
          jworktime = worktime;
      end else  --pracownik byl obecny w danym dniu
      begin
        rworktime = worktime;
        worktime = worktime - extrahours;
      end
    end
    suspend;
  end --for (petla przechodzenia przez dni kalendarza glownego
end^
SET TERM ; ^
