--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_GEN_DOC_PRGRUPAROZL(
      PRGRUPAROZL integer,
      DATADOK date,
      SEPARATEGUIDES smallint,
      ACCEPT smallint,
      OUT smallint,
      DOKMAGREF integer = null)
  returns (
      DOCCNT integer)
   as
declare variable ktm varchar(40);
declare variable prshortagetype varchar(20);
declare variable wersjaref integer;
declare variable quantity numeric(14,4);
declare variable cena numeric(14,4);
declare variable shortageratio numeric(14,4);
declare variable lot integer;
declare variable wh varchar(3);
declare variable doctype varchar(3);
declare variable skad smallint;
declare variable okres varchar(6);
declare variable prschedguidedet integer;
declare variable prschedguide integer;
declare variable aktuoperator integer;
declare variable prshortage integer;
declare variable pref integer;
declare variable prschedoper integer;
declare variable shortage smallint;
declare variable paramn1 numeric(14,4);
declare variable paramn2 numeric(14,4);
declare variable paramn3 numeric(14,4);
declare variable paramn4 numeric(14,4);
declare variable paramn5 numeric(14,4);
declare variable paramn6 numeric(14,4);
declare variable paramn7 numeric(14,4);
declare variable paramn8 numeric(14,4);
declare variable paramn9 numeric(14,4);
declare variable paramn10 numeric(14,4);
declare variable paramn11 numeric(14,4);
declare variable paramn12 numeric(14,4);
declare variable params1 varchar(255);
declare variable params2 varchar(255);
declare variable params3 varchar(255);
declare variable params4 varchar(255);
declare variable params5 varchar(255);
declare variable params6 varchar(255);
declare variable params7 varchar(255);
declare variable params8 varchar(255);
declare variable params9 varchar(255);
declare variable params10 varchar(255);
declare variable params11 varchar(255);
declare variable params12 varchar(255);
declare variable paramd1 timestamp;
declare variable paramd2 timestamp;
declare variable paramd3 timestamp;
declare variable paramd4 timestamp;
begin
  if (separateguides is null) then separateguides = 0;
  if (out = 1) then skad = 4;
  else skad = 3;
  doccnt = 0;
  update prgruparozltmp set status = 1 where prgruparozl = :prgruparozl;
  delete from prgruparozltmp where status = 1 and prgruparozl = :prgruparozl;
  execute procedure get_global_param('AKTUOPERATOR') returning_values aktuoperator;
  if (:dokmagref = 0) then
    dokmagref = null;
  if (:dokmagref is not null) then
  begin
    if (not exists (select first 1 1 from dokumnag n
        join prschedguidedets g on (n.typ = g.doctype and n.magazyn = g.wh and g.prgruparozl = :prgruparozl)
        where n.ref = :dokmagref and n.wydania = :out)
    ) then
      exception PRSCHEDGUIDEDET_ERROR 'Parametry rozppiski niezgodne z dokuemntem magazynowym';
    separateguides = 0;
    doccnt = -1;
    update dokumnag n set n.prgrupaprod = :prgruparozl where n.ref = :dokmagref;
  end
  -- generowanie dokumentów
  for
    select d.ref, d.ktm, d.wersjaref, d.quantity, d.wh, d.doctype, d.lot,
        d.prschedguide, d.shortage, d.prshortage, d.prschedoper, d.shortageratio,
        paramn1,paramn2,paramn3,paramn4,
        paramn5,paramn6,paramn7,paramn8,
        paramn9,paramn10,paramn11,paramn12,
        params1,params2,params3,params4,
        params5,params6,params7,params8,
        params9,params10,params11,params12,
        paramd1,paramd2,paramd3,paramd4
      from prschedguidedets d
      where d.prgruparozl = :prgruparozl and d.accept = 0 and d.ssource = 0 and d.out = :out
      order by d.wh, d.doctype
      into prschedguidedet, ktm, wersjaref, quantity, wh, doctype, lot,
          prschedguide, shortage, prshortage, prschedoper, shortageratio,
          paramn1,paramn2,paramn3,paramn4,
          paramn5,paramn6,paramn7,paramn8,
          paramn9,paramn10,paramn11,paramn12,
          params1,params2,params3,params4,
          params5,params6,params7,params8,
          params9,params10,params11,params12,
          paramd1,paramd2,paramd3,paramd4
  do begin
    dokmagref = null;
    if (separateguides = 0) then
    begin
      if (:dokmagref is null ) then
        select d.ref
          from dokumnag d
          where d.magazyn = :wh and d.typ = :doctype and d.akcept = 0 and d.prgrupaprod = :prgruparozl
          into dokmagref;
    end else
      select d.ref
        from dokumnag d
        where d.magazyn = :wh and d.typ = :doctype and d.akcept = 0 and d.prgrupaprod = :prgruparozl
          and d.dokumentmain = :prschedguide
        into dokmagref;
    -- zakadamy dokument
    if (dokmagref is null) then
    begin
      doccnt = doccnt + 1;
      select datatookres.okres
        from datatookres(cast(:datadok as date),null,null,null,1)
        into :okres;
      execute procedure GEN_REF('DOKUMNAG') returning_values :dokmagref;
      insert into DOKUMNAG(REF,MAGAZYN, TYP, OKRES, DATA, AKCEPT,prgrupaprod,skad,
          dokumentmain,OPERATOR)
        values (:dokmagref, :wh, :doctype, :okres, :datadok, 0, :prgruparozl, :skad,
            case when :separateguides = 1 then :prschedguide else null end,:aktuoperator);
    end else
    --aktulizuje pole skad bo moze byc tak ze dokument jest wystawiony recznie i dodawane sa rozpiski
      update dokumnag n set n.skad = :skad where n.ref = :dokmagref and n.skad <> :skad;
    -- zakladamy pozycje
    if (out = 0) then
    begin
      if (shortage = 1) then
      begin
        select prshortagestype
          from prshortages
          where ref = :prshortage
          into prshortagetype;
        execute procedure XK_PRSCHORTAGES_PRICE(prschedguide, prshortage, prshortagetype, ktm, quantity)
          returning_values cena;
        quantity = quantity * shortageratio;
      end
      else
       execute procedure XK_PRPRODUCT_PRICE(prschedguide, prschedoper, ktm, quantity)
         returning_values cena;
    end
    else
      cena = 0;
    execute procedure GEN_REF('DOKUMPOZ') returning_values :pref;
    update prschedguidedets set sref = :pref where ref = :prschedguidedet;
    insert into DOKUMPOZ(REF,DOKUMENT, KTM, WERSJAREF, ILOSC, dostawa,PRSCHEDGUIDEDET,cena,
        paramn1,paramn2,paramn3,paramn4,
        paramn5,paramn6,paramn7,paramn8,
        paramn9,paramn10,paramn11,paramn12,
        params1,params2,params3,params4,
        params5,params6,params7,params8,
        params9,params10,params11,params12,
        paramd1,paramd2,paramd3,paramd4)
      values(:pref,:dokmagref, :ktm, :wersjaref, :quantity, :lot,:prschedguidedet,:cena,
          :paramn1,:paramn2,:paramn3,:paramn4,
          :paramn5,:paramn6,:paramn7,:paramn8,
          :paramn9,:paramn10,:paramn11,:paramn12,
          :params1,:params2,:params3,:params4,
          :params5,:params6,:params7,:params8,
          :params9,:params10,:params11,:params12,
          :paramd1,:paramd2,:paramd3,:paramd4);
  end
  -- akceptowanie dokumentów
  if (accept > 0) then
  begin
    for
      select d.ref
        from dokumnag d
        where d.akcept = 0 and d.prgrupaprod = :prgruparozl
        order by d.magazyn, d.typ, d.akcept, d.prgrupaprod
        into dokmagref
    do begin
      update dokumnag set akcept = 1, operakcept = :aktuoperator where ref = :dokmagref;
    end
  end
  -- akceptowanie rozpisek przekazujacych towary pomiedzy przewodnikami
  for
    select d.ref
      from prschedguidedets d
      where d.prgruparozl = :prgruparozl and d.accept = 1 and d.ssource = 1 and d.out = :out
      order by d.wh, d.doctype
      into prschedguidedet
  do begin
    update prschedguidedets d set d.accept = 2 where ref = :prschedguidedet;
  end
end^
SET TERM ; ^
