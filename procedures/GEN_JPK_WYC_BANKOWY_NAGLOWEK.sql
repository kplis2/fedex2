--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GEN_JPK_WYC_BANKOWY_NAGLOWEK(
      REF INTEGER_ID)
  returns (
      CEL_ZLOZENIA SMALLINT_ID,
      DATA_OD DATE_ID,
      DATA_DO DATE_ID,
      DATA_WYTWORZENIA TIMESTAMP_ID,
      KOD_WALUTY INTEGER_ID,
      KOD_URZEDU INTEGER_ID,
      WARIANT_FORMULARZA INTEGER_ID)
   as
begin
  /* Procedure Text */
  cel_zlozenia = 0;
  data_od = dateadd(-2 day to current_date);
  data_do = dateadd(1 day to current_date);
  data_wytworzenia = current_timestamp;
  kod_waluty = 1;
  kod_urzedu = 1;
  wariant_formularza = ref;
  suspend;
end^
SET TERM ; ^
