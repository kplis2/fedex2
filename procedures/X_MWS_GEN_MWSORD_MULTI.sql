--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_GEN_MWSORD_MULTI(
      MWSORD INTEGER_ID,
      WH STRING3)
   as
declare variable kardex smallint_id;
declare variable mwsordnew smallint_id;
declare variable period period_id;
declare variable STOCKTAKING smallint_id;
declare variable OPERSETTLMODE smallint;
declare variable AUTOLOCCREATE smallint;
declare variable MWSORDTYPE MWSORDTYPES_ID;
declare variable mwsact integer_id;
begin
  -- Procedura sprawdza czy w przekazanym zleceniu sa pozycje do wydnia z lokacji typu Kardex
  -- jesli sa to wydizela takie pzycje do jednego zbiorczego zlecenia
  select first 1 a.ref
    from mwsacts a
      join mwsconstlocs c on (a.mwsconstlocp = c.ref)
    where a.mwsord = :mwsord
      and c.x_row_type = 2
   into :kardex;

   -- nie ma na zleceniu pozycji z lokacji Kardex, wychodzimy
  if (coalesce(kardex,0) = 0) then
    exit;

  -- szukamy otwartego zlecenia z lokacjami typu kardex
  select first 1  o.ref
    from mwsords o
      join mwsacts a on (o.ref = a.mwsord)
      join mwsconstlocs c on (a.mwsconstlocp = c.ref)
    where o.wh = :wh
      and o.status in (0,1,7)
      and o.mwsordtypedest = 1
      and o.ref <> :mwsord
      and c.x_row_type = 2
      and o.regtime > '2018-09-01' --- XX KBi tymczasowo, bo jakie testowe miecowe zlecenia psuy mi weryfikacje porawnoci
    order by o.ref
   into :mwsordnew;

  if (coalesce(:mwsordnew, 0) = 0) then begin
    -- zakądamy nagowek zlecenia
    select first 1 t.ref, t.stocktaking, t.opersettlmode, t.autoloccreate
      from mwsordtypes t
      where t.mwsortypedets = 1
      order by t.ref
    into :mwsordtype, :stocktaking, :opersettlmode, :autoloccreate;

    execute procedure gen_ref('MWSORDS') returning_values :mwsordnew;
    insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator, priority,
        description, wh,  regtime, timestartdecl, branch, period,
        flags, docsymbs, cor, rec, status, shippingarea, shippingtype, takefullpal,
        slodef, slopoz, slokod, palquantity, repal, multi, multicnt)
    select :mwsordnew, :mwsordtype, :stocktaking, o.doctype, o.docgroup, o.docid, null, o.priority,
          /*o.description*/'', :wh, current_timestamp, null, o.branch, o.period,
          o.flags, o.docsymbs, o.cor, o.rec, 0, o.shippingarea, o.shippingtype, 0,
          o.slodef, o.slopoz, o.slokod, o.palquantity, 0, 1, 0
      from mwsords o
      where o.ref = :mwsord;
  end

  for
    select a.ref
      from mwsacts a
        join mwsconstlocs c on (a.mwsconstlocp = c.ref)
      where a.mwsord = :mwsord
        and c.x_row_type = 2
     into :mwsact
  do begin
    update mwsacts a set a.status = 0 where a.ref = :mwsact;
    update mwsacts a set a.mwsord = :mwsordnew where a.ref = :mwsact;
    update mwsacts a set a.status = 1 where a.ref = :mwsact;
  end


  execute procedure xk_mws_mwsord_route_opt(:mwsordnew);
  update mwsords o set o.status = 1 where o.ref = :mwsordnew and o.status = 0;
end^
SET TERM ; ^
