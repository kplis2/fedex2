--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_INVALIDITY(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable invalidity smallint;
  declare variable fromdate date;
  declare variable todate date;
  declare variable invalidityfrom date;
  declare variable invalidityto date;
begin
--MW: personel - obliczenie wysokosci dodatku dla niepelnosprawnych

  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  select Z.invalidity, Z.invalidityfrom, Z.invalidityto
    from ezusdata Z join employees E on (Z.person = E.person)
    where E.ref = :employee
    into :invalidity, :invalidityfrom, :invalidityto;

  if (invalidityfrom < todate and (invalidityto is null or invalidityto > fromdate)) then
  begin
    if (invalidity = 84) then
      ret = 90;
    if (invalidity = 85) then
      ret = 120;
  end

  if (ret is null) then
    ret = 0;

  suspend;
end^
SET TERM ; ^
