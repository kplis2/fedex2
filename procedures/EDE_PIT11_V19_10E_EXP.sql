--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_PIT11_V19_10E_EXP(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
--zmianna pomocnicza
declare variable ISDECLARATION integer;
declare variable DEF varchar(20);
declare variable CODE varchar(10);
declare variable PFIELD varchar(10);
declare variable SYSTEMCODE varchar(20);
declare variable OBLIGATIONKIND varchar(20);
declare variable SCHEMAVER varchar(10);
declare variable SYMBOL varchar(10);
declare variable VARIANT varchar(10);
declare variable PVALUE varchar(255);
declare variable TMPVAL1 varchar(255);
declare variable TMPVAL2 varchar(255);
declare variable TMP smallint;
declare variable CURR_P integer;
declare variable CORRECTION smallint;
declare variable ENCLOSUREOWNER integer;
declare variable ZALTYP varchar(20);
declare variable ISZAL smallint;
declare variable PARENT_LVL_0 integer;
declare variable PARENT_LVL_1 integer;
declare variable PARENT_LVL_2 integer;
declare variable PARENT_LVL_3 integer;
begin
  --Sprawdzamy czy dana deklaracja istnieje do eksportu
  select first 1 1
    from edeclarations e
    where e.ref = :oref
  into :isDeclaration;

  if(isDeclaration is null) then
    exception universal'Niepoprawny identyfikator e-deklaracji';

  --Pobieranie definicji
  select e.edecldef, ed.code, ed.systemcode, ed.obligationkind, ed.schemaver, ed.symbol, ed.variant
    from edeclarations e join edecldefs ed on (ed.systemcode = e.edecldef)
    where e.ref = :oref
  into :def, :code, :systemcode, :obligationkind, :schemaver, :symbol, :variant;

  --Generujemy naglowek pliku XML
  id = 0;
  name = 'Deklaracja';
  parent = null;
  params = 'xmlns="http://crd.gov.pl/wzor/2011/12/12/725/" xmlns:etd="http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2011/06/21/eD/DefinicjeTypy/"';
  val = null;
  suspend;

  correction = 0;
  parent_lvl_0 = 0;
  --Rozpoczynamy naglowek e-deklaracji
  id = :id + 1; --1
  name = 'Naglowek';
  parent = parent_lvl_0;
  params = null;
  val  = null;
  parent_lvl_1 = :id;
  suspend;
  --cialo naglowka

    --KodFormularza
    id = id+1;
    name ='KodFormularza';
    parent = parent_lvl_1;
    params ='';
    if(code is not null) then
    params = params||'kodPodatku="'||code||'" ';
    if(systemcode is not null) then
    params = params||'kodSystemowy="'||systemcode||'" ';
    if(obligationkind is not null) then
    params = params||'rodzajZobowiazania="'||obligationkind||'" ';
    if(schemaver is not null) then
    params = params||'wersjaSchemy="'||schemaver||'"';
    val = symbol;
    suspend;
    
    --WariantFormularza
    id = id+1;
    name='WariantFormularza';
    parent = parent_lvl_1;
    params = null;
    val = variant;
    suspend;

    pvalue = null;
    --CelZlozenia
    id = id+1;
    name='CelZlozenia';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.field = 6
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params = 'poz="P_6"';
      val = :pvalue;
      suspend;
    end

    if(:pvalue = 2) then
      correction = 1;

    pvalue = null;
    --Rok
    id = id+1;
    name='Rok';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.field = 4
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params =null;
      val = :pvalue;
      suspend;
    end

    pvalue = null;
    --KodUrzedu
    id = id+1;
    name='KodUrzedu';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.field = 5
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params = null;
      val = :pvalue;
      suspend;
    end
    --Koniec Naglowka

  pvalue = null;
  --Podmiot1
  id = id+1;
  name='Podmiot1';
  parent = parent_lvl_0;
  params = 'rola="Płatnik"';
  val = null;
  parent_lvl_1 = :id;
  suspend;

    select epos.pvalue
        from edeclpos epos
        where epos.edeclaration = :oref and epos.field = 7
      into :pvalue;
    if(:pvalue = 1) then
    begin
    --etd:OsobaNiefizyczna
    id = id+1;
    name='etd:OsobaNiefizyczna';
    parent = parent_lvl_1;
    params = null;
    val = null;
    parent_lvl_2 = :id;
    suspend;
      pvalue = null;
      --etd:NIP
      id = id+1;
      name='etd:NIP';
      parent = parent_lvl_2;
      select REPLACE(epos.pvalue, '-', '')
        from edeclpos epos
        where epos.edeclaration = :oref and epos.field = 1
      into :pvalue;
      params = null;
      val = :pvalue;
      suspend;
      pvalue = null;
      --etd:PelnaNazwa && etd:REGON
      tmp =0;
      select epos.pvalue
      from edeclpos epos
        where epos.edeclaration = :oref and epos.field = 8
      into :pvalue;
      for select outstring
        from parse_lines(:pvalue, ';')
      into :tmpval2
      do begin
      if(tmp = 0)then
      begin
        --etd:PelnaNazwa
        id = id+1;
        name='etd:PelnaNazwa';
        parent = parent_lvl_2;
        val = tmpval2;
        params = null;
        suspend;
      end
      else if(tmp = 1) then
      begin
        --etd:REGON
        id = id+1;
        name='etd:REGON';
        parent = parent_lvl_2;
        val = tmpval2;
        params = null;
        suspend;
      end
      tmp = :tmp + 1;
    end
    end
    else if(:pvalue = 2) then
    begin
    --etd:OsobaNiefizyczna
    id = id+1;
    name='etd:OsobaFizyczna';
    parent = parent_lvl_1;
    params = null;
    val = null;
    parent_lvl_2 = :id;
    suspend;
      pvalue = null;
      --etd:NIP
      id = id+1;
      name='etd:NIP';
      parent = parent_lvl_2;
      select REPLACE(epos.pvalue, '-', '')
        from edeclpos epos
        where epos.edeclaration = :oref and epos.field = 1
      into :pvalue;
      params = null;
      val = :pvalue;
      suspend;
      pvalue = null;
      --etd:ImiePierwsze && etd:Nazwisko && etd:DataUrodzenia
      tmp =0;
      select epos.pvalue
      from edeclpos epos
        where epos.edeclaration = :oref and epos.field = 9
      into :pvalue;
      for select outstring
        from parse_lines(:pvalue, ';')
      into :tmpval2
      do begin
      if(tmp = 0) then
      begin
        tmpval1 = :tmpval2;
      end
      if(tmp = 1) then
      begin
      --etd:PelnaNazwa
        id = id+1;
        name='etd:ImiePierwsze';
        parent = parent_lvl_2;
        val = tmpval2;
        params = null;
        suspend;

        --etd:REGON
        id = id+1;
        name='etd:Nazwisko';
        parent = parent_lvl_2;
        val = tmpval1;
        params = null;
        suspend;
      end
      if(tmp = 2) then
      begin
        --etd:DataUrodzenia
        id = id+1;
        name='etd:DataUrodzenia';
        parent = parent_lvl_2;
        val = tmpval2;
        params = null;
        suspend;
      end
      tmp = :tmp + 1;
      end
    end

    --etd:AdresZamieszkania
    id = id+1;
    name='AdresZamieszkaniaSiedziby';
    parent = parent_lvl_1;
    params = 'rodzajAdresu="RAD"';
    val = null;
    parent_lvl_2 = :id;
    suspend;

      --etd:AdresPol
      id = id+1;
      name='etd:AdresPol';
      parent = parent_lvl_2;
      params = null;
      val = null;
      parent_lvl_3 = :id;
      suspend;
        pvalue = null;
        --etd:KodKraju
        select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 10
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:KodKraju';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:Wojewodztwo
        select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 11
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:Wojewodztwo';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:Powiat
        select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 12
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:Powiat';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:Gmina
        select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 13
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:Gmina';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:Ulica
        select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 14
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:Ulica';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:NrDomu
        select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 15
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:NrDomu';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:NrLokalu
         select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 16
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:NrLokalu';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:Miejscowosc
         select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 17
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:Miejscowosc';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:KodPocztowy
        select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 18
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:KodPocztowy';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:Poczta
        select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 19
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:Poczta';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end

  --Podmiot2
  id = id+1;
  name='Podmiot2';
  parent = parent_lvl_0;
  params = 'poz="P_20A" rola="Podatnik"';
  val = null;
  parent_lvl_1 = :id;
  suspend;

    --etd:OsobaFizyczna
    id = id+1;
    name='etd:OsobaFizyczna';
    parent = parent_lvl_1;
    params =null;
    val = null;
    parent_lvl_2 = :id;
    suspend;
      pvalue = null;
      --etd:NIP || etd:PESEL
      select epos.pvalue
        from edeclpos epos
        where epos.edeclaration = :oref and epos.field = 20
      into :pvalue;
      if(:pvalue is not null) then
      begin
        if (char_length(:pvalue) = 10) then
        begin
          --etd:NIP
          id = id+1;
          name='etd:NIP';
          parent = parent_lvl_2;
          params =null;
          val = upper(:pvalue);
          suspend;
        end else begin
          --etd:PESEL
          id = id+1;
          name='etd:PESEL';
          parent = parent_lvl_2;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
      end
      pvalue = null;
      --etd:ImiePierwsze
      select epos.pvalue
        from edeclpos epos
        where epos.edeclaration = :oref and epos.field = 22
      into :pvalue;
      if(:pvalue is not null) then
      begin
        id = id+1;
        name='etd:ImiePierwsze';
        parent = parent_lvl_2;
        params =null;
        val = upper(:pvalue);
        suspend;
      end
      pvalue = null;
      --etd:Nazwisko
      select epos.pvalue
        from edeclpos epos
        where epos.edeclaration = :oref and epos.field = 21
      into :pvalue;
      if(:pvalue is not null) then
      begin
        id = id+1;
        name='etd:Nazwisko';
        parent = parent_lvl_2;
        params =null;
        val = upper(:pvalue);
        suspend;
      end
      pvalue = null;
      --etd:DataUrodzenia
      select epos.pvalue
        from edeclpos epos
        where epos.edeclaration = :oref and epos.field = 23
      into :pvalue;
      if(:pvalue is not null) then
      begin
        id = id+1;
        name='etd:DataUrodzenia';
        parent = parent_lvl_2;
        params =null;
        val = upper(:pvalue);
        suspend;
      end

    --etd:AdresZamieszkania
    id = id+1;
    name='etd:AdresZamieszkania';
    parent = parent_lvl_1;
    params ='rodzajAdresu="RAD"';
    val = null;
    parent_lvl_2 = :id;
    suspend;

      --etd:AdresPol
      id = id+1;
      name='etd:AdresPol';
      parent = parent_lvl_2;
      params =null;
      val = null;
      parent_lvl_3 = :id;
      suspend;
        pvalue = null;
          --etd:KodKraju
        select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 24
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:KodKraju';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:Wojewodztwo
        select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 25
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:Wojewodztwo';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:Powiat
        select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 26
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:Powiat';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:Gmina
        select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 27
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:Gmina';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:Ulica
        select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 28
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:Ulica';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:NrDomu
        select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 29
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:NrDomu';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:NrLokalu
         select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 30
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:NrLokalu';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:Miejscowosc
         select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 31
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:Miejscowosc';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:KodPocztowy
        select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 32
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:KodPocztowy';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        pvalue = null;
        --etd:Poczta
        select epos.pvalue
          from edeclpos epos
          where epos.edeclaration = :oref and epos.field = 33
        into :pvalue;
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='etd:Poczta';
          parent = parent_lvl_3;
          params =null;
          val = upper(:pvalue);
          suspend;
        end

    --PozycjeSzczegolowe
    id = id+1;
    name='PozycjeSzczegolowe';
    parent = parent_lvl_0;
    params =null;
    val = null;
    parent_lvl_1 = :id;
    suspend;
      pvalue = null;
      --pozycja Top - 34
      select epos.pvalue
        from edeclpos epos
        where epos.edeclaration = :oref and epos.field = 34
      into :pvalue;
      if(:pvalue is not null) then
      begin
        id = id+1;
        name='P_34';
        parent = parent_lvl_1;
        params =null;
        val = upper(:pvalue);
        suspend;
      end

      --Pozycje od first do last -  35 - 82
      pvalue = null;
      for select epos.pvalue,epos.field
        from edeclpos epos
        where epos.edeclaration = :oref and epos.field > 33
                                        and epos.field < 83
                                        and epos.field <> 34
        order by epos.field
      into :pvalue,:pfield
      do begin
        if(:pvalue is not null) then
        begin
          id = id+1;
          name='P_'||:pfield;
          parent = parent_lvl_1;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
      end

    --Oswiadczenie
    id = id+1;
    name='Oswiadczenie';
    parent = parent_lvl_0;
    params =null;
    val ='Oświadczam, że są mi znane przepisy Kodeksu karnego skarbowego o odpowiedzialności za uchybienie obowiązkom płatnika.';
    suspend;

    if (correction = 1) then
    begin
      select e.ref
        from edeclarations e
        where e.enclosureowner = :oref
      into :enclosureowner;

      if (:enclosureowner is not null) then
      begin
        select e.edecldef
          from edeclarations e
          where e.enclosureowner = :enclosureowner
        into :zaltyp;

        select first 1 1
          from edeclarations e
          where e.ref = :oref and e.edecldef = :zaltyp
        into :iszal;

        if(:iszal is not null)then
        begin
          if (substring(:zaltyp from 1 for 6 )='ORD-ZU') then
          begin
            --Zalacznik_ORD-ZU
            id = id+1;
            name='Zalacznik_ORD-ZU';
            parent = parent_lvl_0;
            params ='xmlns:zzu="http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2011/10/07/eD/ORDZU/"';
            val =null;
            parent_lvl_1 = :id;
            suspend;

              --Pobieranie definicji do ORD_ZU
              select ed.code, ed.systemcode, ed.obligationkind, ed.schemaver, ed.symbol, ed.variant
                from  edecldefs ed
                where ed.systemcode = :zaltyp
              into :code, :systemcode, :obligationkind, :schemaver, :symbol, :variant;

              --Rozpoczynamy naglowek e-deklaracji ORD_ZU
              id = :id + 1;
              name = 'zzu:Naglowek';
              parent = parent_lvl_1;
              params = null;
              val  = null;
              parent_lvl_2 = :id;
              suspend;
              --cialo naglowka
            
                --KodFormularza
                id = id+1;
                name ='zzu:KodFormularza';
                parent = parent_lvl_2;
                params ='';
                if(code is not null) then
                params = params||'kodPodatku="'||code||'" ';
                if(systemcode is not null) then
                params = params||'kodSystemowy="'||systemcode||'" ';
                if(obligationkind is not null) then
                params = params||'rodzajZobowiazania="'||obligationkind||'" ';
                if(schemaver is not null) then
                params = params||'wersjaSchemy="'||schemaver||'"';
                val = symbol;
                suspend;
                
                --WariantFormularza
                id = id+1;
                name='zzu:WariantFormularza';
                parent = parent_lvl_2;
                params = null;
                val = variant;
                suspend;

              --PozycjeSzczegolowe
              id = :id + 1;
              name = 'zzu:PozycjeSzczegolowe';
              parent = parent_lvl_1;
              params = null;
              val  = null;
              parent_lvl_2 = :id;
              suspend;
              pvalue = null;
              --Pozycje od first do last -  35 - 82
              curr_p = 13;
              for select epos.pvalue
                from edeclpos epos
                where epos.edeclaration = :oref and epos.field > 12
                                                and epos.field < 14
              into :pvalue
              do begin
                if(:pvalue is not null) then
                begin
                  id = id+1;
                  name='zzu:P_'||curr_p;
                  parent = parent_lvl_2;
                  params =null;
                  val = upper(:pvalue);
                  suspend;
                end
                curr_p = curr_p +1;
              end

          end

        end 

      end

    end

end^
SET TERM ; ^
