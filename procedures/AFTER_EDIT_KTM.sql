--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AFTER_EDIT_KTM(
      TYPDOK varchar(1) CHARACTER SET UTF8                           ,
      REFDOK integer,
      REFPOZ integer,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      KTMIN varchar(120) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint,
      MSG varchar(1024) CHARACTER SET UTF8                           ,
      KTM varchar(100) CHARACTER SET UTF8                           ,
      WERSJA integer,
      WERSJAREF integer,
      JEDNO integer,
      CENA numeric(14,4),
      DOSTAWA integer,
      ILOSC numeric(14,4))
   as
declare variable STATEMENTTXT varchar(1024);
declare variable DOSTAWCA integer;
declare variable WYDANIA smallint;
declare variable TYP varchar(3);
declare variable FROMDOKUMPOZ integer;
begin
  -- status = 0 - nie udalo sie znalezc towaru
  -- status = 1 - udalo sie okreslic KTM i inne dane
  -- status = 2 - wszystko ok, ale nie dodajemy nowego towaru
  -- msg - jesli niepusty to wyswietlany zawsze
  -- ktm - jesli niepusty to zostal okreslony
  -- wersja, wersjaref - jesli niezerowa to zostala okreslona
  -- jedno - jestni niezerowe to zostalo okreslone
  -- cena, dostawa - jesli niezerowa to zostaly okreslone
  -- ilosc - jesli niezerowa to zostala okreslona
  ktmin = upper(:ktmin);
  status = 0;
  msg = '';
  ktm = '';
  wersja = 0;
  wersjaref = 0;
  jedno = 0;
  cena = 0;
  dostawa = 0;
  ilosc = 0;
  dostawca = null;
  wydania = 0;
  fromdokumpoz = null;
  -- pobierz dane z naglowka dokumentu
  if(:typdok='M') then begin
    select DOSTAWCA,TYP,WYDANIA from DOKUMNAG where REF=:refdok into :dostawca, :typ, :wydania;
  end else if (:typdok='F') then begin
    select DOSTAWCA,TYP,1-ZAKUP from NAGFAK where REF=:refdok into :dostawca, :typ, :wydania;
  end else if (:typdok='Z') then begin
    select DOSTAWCA,TYPZAM from NAGZAM where REF=:refdok into :dostawca, :typ;
    select WYDANIA from TYPZAM where SYMBOL=:typ into :wydania;
  end

  -- ewentualnie odpal x-owa procedure
  if (exists(select RDB$PROCEDURE_NAME from RDB$PROCEDURES where upper(RDB$PROCEDURE_NAME)='X_AFTER_EDIT_KTM')) then
  begin
    statementtxt = 'select first 1 * from x_after_edit_ktm('''||:typdok||''','||:refdok||','||:refpoz||','''||:magazyn||''','''||:ktmin||''')';
    execute statement statementtxt into :status, :msg, :ktm, :wersja, :wersjaref, :jedno, :cena, :dostawa, :ilosc;

    if(:status is null) then status = 0;
    if(:msg is null) then msg = '';
    if(:ktm is null) then ktm = '';
    if(:wersja is null) then wersja = 0;
    if(:wersjaref is null) then wersjaref = 0;
    if(:jedno is null) then jedno = 0;
    if(:cena is null) then cena = 0;
    if(:dostawa is null) then dostawa = 0;
    if(:ilosc is null) then ilosc = 0;

  end

  if(:ktm='' and :status=0) then begin --znajdz w towarach na podstawie KTM
    select first 1 ktm from TOWARY where KTM like :ktmin||'%' into :ktm;
    if(:ktm<>'') then status = 1;
  end
  if(:ktm='' and :status=0) then begin --znajdz w towarach na podstawie nazwy
    select first 1 ktm from TOWARY where upper(NAZWA) like :ktmin||'%' into :ktm;
    if(:ktm<>'') then status = 1;
  end
  if(:ktm='' and :status=0 and :dostawca is not null) then begin --znajdz w DOSTCEN na podstawie symbolu u dostawcy
    select first 1 ktm from DOSTCEN where DOSTAWCA=:dostawca and SYMBOL_DOST like :ktmin||'%' into :ktm;
    if(:ktm<>'') then status = 1;
  end
  if(:ktm='' and :status=0 and :wydania=1) then begin --znajdz na podstawie symbolu dostawy magazynowej
    select first 1 ref,fromdokumpoz from DOSTAWY where SYMBOL=:ktmin into :dostawa,:fromdokumpoz;
    if(:fromdokumpoz is not null) then begin --znaleziono dokumpoza zrodlowego
      select KTM,wersja,WERSJAREF,CENA
        from DOKUMPOZ
        where REF=:fromdokumpoz
        into :ktm, :wersja, :wersjaref, :cena;
      if(:ktm<>'') then begin --podpowiedz maksymalna dostepna ilosc
        execute procedure MAG_CHECKILWOL(:refpoz,:magazyn,:wersjaref,:typdok, :dostawa)
          returning_values :ilosc;
      end
      if(:ktm<>'') then status = 1;
    end else dostawa = 0;
  end
end^
SET TERM ; ^
