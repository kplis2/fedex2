--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INTRASTAT_DANE_GRUP(
      INTRASTATH integer)
   as
declare variable przelicznik numeric(14,4);
declare variable varue smallint;
declare variable varsymbdok varchar(20);
declare variable varmagazyn char(3);
declare variable vardataod date;
declare variable vardatado date;
declare variable varokres char(6);
declare variable vardecltype varchar(255);
declare variable progstat smallint;
declare variable sql varchar(4096);
declare variable nazwa varchar(255);
declare variable krajwysylki varchar(20);
declare variable wardostawy varchar(255);
declare variable rodztransakcja varchar(255);
declare variable rodztransport varchar(255);
declare variable kodtowaru varchar(20);
declare variable masa numeric(14,4);
declare variable iloscjednpomoc integer;
declare variable wartosc numeric(14,2);
declare variable wartoscstat numeric(14,2);
declare variable ilosc numeric(14,4);
declare variable krajpochodzenia varchar(20);
declare variable jednpomoc varchar(5);
declare variable otable varchar(40);    /* Źródlo deklaracji Intrastat*/
declare variable oref integer;          /* Ref naglówka dokumentu źródlowego */
declare variable sref integer;          /* Ref pozycji dokumentu źródlowego */
declare variable data date;
declare variable slodef integer;
declare variable slopoz integer;
declare variable locintrastath integer;
declare variable locstatus smallint;
declare variable varcompany integer;
declare variable varktm varchar(40);
declare variable varwydania integer;
declare variable varnota integer;
declare variable vartnazwa varchar(255);
declare variable vartypdeklaracji smallint;
declare variable statusflag smallint;
declare variable refdokumnag integer;   /* Ref naglowka dokumentu magazynowego */
declare variable wariant smallint;      /* Wariant naliczania deklaracji: 0 - z DOKUMPOZ, 1 - z POZFAK */
begin

  select ith.fromdate, ith.todate, ith.period, ith.mag, ith.intrtype,
         ith.ue, ith.progstat, ith.company, ith.decltype, ith.statusflag
    from intrastath ith
    where ith.ref = :intrastath
  into  :vardataod, :vardatado, :varokres, :varmagazyn, :vardecltype,
        :varue, :progstat, :varcompany, :vartypdeklaracji, :statusflag;

  if(:statusflag <> 0) then   /* Jeżeli deklaracja nie jest otwarta to nie można naliczać pozycji */
    exception intrastat_docum_notopened;

  if(:vartypdeklaracji = 1) then
  begin
    delete from intrastatp p where p.intrastath = :intrastath;
  end 

  /* Usuniecie z pozycji deklaracji dokumentow naliczonych z innego typu dokumentow */
  execute procedure get_config('INTRASTAT_DOKUMENTY', 2)
    returning_values wariant;
  if(:wariant=0) then   /* naliczanie z dokumpoz */
    delete from intrastatp where intrastath=:intrastath and otable = 'NAGFAK';
  else                  /* naliczanie z pozfak */
    delete from intrastatp where intrastath=:intrastath and otable <> 'NAGFAK';

  sql = ' select ktm, nazwa, symbdok, krajwysylki, wardostawy, rodztransakcja, rodztransport, kodtowaru, ';
  sql = sql || ' masa, iloscjednpomoc, wartosc, wartoscstat, ilosc, krajpochodzenia, jednpomoc, ';
  sql = sql || ' otable, oref, sref, data, slodef, slopoz, wydania, nota, tnazwa ,refdokumnag ';
  sql = sql || ' from INTRASTAT_DANE(';
  if (:vardataod is not null) then sql = sql || '''' || :vardataod || ''',';
  else sql = sql || 'NULL,';
  if (:vardatado is not null) then sql = sql || '''' || :vardatado || ''',';
  else sql = sql  || 'NULL,';
  sql = sql || '''' ||:varokres || ''',''' || :varmagazyn || ''',''' || :vardecltype || ''',';
  sql = sql || 'NULL,NULL,NULL,NULL,' || :varue || ',' || :progstat || ',';
  if (:varcompany is not null) then sql = sql || :varcompany || ')';
  else sql = sql ||  'NULL) ';

  for execute statement sql
    into    :varktm,  :nazwa, :varsymbdok, :krajwysylki, :wardostawy, :rodztransakcja, :rodztransport, :kodtowaru,
            :masa, :iloscjednpomoc, :wartosc, :wartoscstat, :ilosc, :krajpochodzenia, :jednpomoc,
            :otable, :oref, :sref, :data, :slodef, :slopoz, :varwydania, :varnota, :vartnazwa, :refdokumnag
  do begin
    locintrastath = null;
    select first 1 p.intrastath, h.statusflag
      from intrastatp p
      left join intrastath h on p.intrastath = h.ref
      where p.otable = :otable and p.oref = :oref and p.sref = :sref
      into :locintrastath,:locstatus;

    -- zgloszenie deklaracji
    if(:vartypdeklaracji = 0) then begin
      if(:locintrastath is not null) then begin /* Aktualizacja deklaracji ... */
        if(:locstatus=0) then begin             /* ... ale tylko otwartej */
          update intrastatp set
            name = :nazwa,
            krajwysylki = :krajwysylki,
            wardostawy = :wardostawy,
            intrdostawy = :rodztransakcja,
            intrtransport = :rodztransport,
            ktc = :kodtowaru,
            weight = :masa,
            quantityhelpunit = :iloscjednpomoc,
            val = :wartosc,
            valstat = :wartoscstat,
            quantity = :ilosc,
            krajpochodzenia = :krajpochodzenia,
            helpunit = :jednpomoc,
            fillpozdate = :data,
            slodef = :slodef,
            slopoz = :slopoz,
            symbdok = :varsymbdok,
            ktm = :varktm,
            otable = :otable,
            oref = :oref,
            wydania = :varwydania,
            okres = :varokres,
            nota = :varnota,
            tnazwa = :vartnazwa,
            sref = :sref,
            refdokumnag = :refdokumnag
          where intrastath = :locintrastath
            and otable = :otable
            and oref = :oref
            and sref = :sref;
        end
      end else begin                    /* Dodanie nowych pozycji do deklaracji */
        insert into intrastatp
          (intrastath, name, symbdok, krajwysylki, wardostawy, intrdostawy, intrtransport, ktc, weight,
          quantityhelpunit,  val, valstat, quantity,  krajpochodzenia,  helpunit, fillpozdate,
          slodef, slopoz, ktm, wydania, okres, nota, tnazwa, otable, oref, sref, refdokumnag)
          values(:intrastath, :nazwa, :varsymbdok, :krajwysylki, :wardostawy, :rodztransakcja, :rodztransport, :kodtowaru, :masa,
          :iloscjednpomoc, :wartosc, :wartoscstat, :ilosc, :krajpochodzenia, :jednpomoc, :data,
          :slodef, :slopoz, :varktm, :varwydania, :varokres, :varnota, :vartnazwa, :otable, :oref, :sref, :refdokumnag);
      end
    -- zamiana deklaracji
    end else if (:vartypdeklaracji = 1) then begin
        insert into intrastatp        /* Dodanie wszytskich pozycji do deklaracji */
          (intrastath, name, symbdok, krajwysylki, wardostawy, intrdostawy, intrtransport, ktc, weight,
          quantityhelpunit,  val, valstat, quantity,  krajpochodzenia,  helpunit, fillpozdate,
          slodef, slopoz, ktm, wydania, okres, nota, tnazwa, otable, oref, sref, refdokumnag)
          values(:intrastath, :nazwa, :varsymbdok, :krajwysylki, :wardostawy, :rodztransakcja, :rodztransport, :kodtowaru, :masa,
          :iloscjednpomoc, :wartosc, :wartoscstat, :ilosc, :krajpochodzenia, :jednpomoc, :data,
          :slodef, :slopoz, :varktm, :varwydania, :varokres, :varnota, :vartnazwa, :otable, :oref, :sref, :refdokumnag);
    -- korekta deklaracji
    end else begin
    -- tu ma być kod odnośnie korekty deklaracji poprzez korekte pozycji
    end
  end
end^
SET TERM ; ^
