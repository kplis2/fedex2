--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CREATE_MWSORD_IN(
      OPERATOR integer,
      WH varchar(3) CHARACTER SET UTF8                           )
  returns (
      REFMWSORD integer)
   as
declare variable rec smallint;
declare variable stocktaking smallint;
declare variable priority smallint;
declare variable branch varchar(10);
declare variable period varchar(6);
declare variable flags varchar(40);
declare variable symbol varchar(20);
declare variable cor smallint;
declare variable quantity numeric(14,4);
declare variable good varchar(40);
declare variable goodtmp varchar(40);
declare variable vers integer;
declare variable opersettlmode smallint;
declare variable mwsconstlocp integer;
declare variable whsec integer;
declare variable recramp integer;
declare variable mwspallocp integer;
declare variable mwsconstlocl integer;
declare variable autoloccreate smallint;
declare variable mwspallocl integer;
declare variable wharea integer;
declare variable posflags varchar(40);
declare variable description varchar(1024);
declare variable posdescription varchar(255);
declare variable lot integer;
declare variable shippingarea varchar(3);
declare variable wharealogp integer;
declare variable mwsaccessory integer;
declare variable difficulty varchar(10);
declare variable timestart timestamp;
declare variable maxnumber integer;
declare variable timestartdcl timestamp;
declare variable timestopdcl timestamp;
declare variable realtime double precision;
declare variable wharealogb integer;
declare variable wharealogl integer;
declare variable dist numeric(14,4);
declare variable refmwsactpal integer;
declare variable refmwsact integer;
declare variable paltype varchar(20);
declare variable palactsquantity numeric(14,4);
declare variable toinsert numeric(14,4);
declare variable status smallint;
declare variable whsecout integer;
declare variable paltypevers integer;
declare variable planwharealogl integer;
declare variable planmwsconstlocl integer;
declare variable planwhareal integer;
declare variable palpackmethshort varchar(30);
declare variable palpackmeth varchar(40);
declare variable palpackquantity numeric(14,4);
declare variable slodef integer;
declare variable slopoz integer;
declare variable slokod varchar(40);
declare variable manmwsacts smallint;
declare variable maxpalobj numeric(14,4);
declare variable refill smallint;
declare variable wh2 varchar(3);
declare variable mwsordtype integer;
begin
  select d.oddzial
    from defmagaz d
    where d.symbol = :wh
    into branch;
  select first 1 t.ref, t.stocktaking, t.opersettlmode, t.autoloccreate
    from mwsordtypes t
    where t.mwsortypedets = 2
    into mwsordtype, stocktaking, opersettlmode, autoloccreate;
  rec = 1;
  cor = 0;
  if (manmwsacts is null) then manmwsacts = 0;
  refmwsord = null;
  if (refmwsord is null) then
  begin
    execute procedure gen_ref('MWSORDS') returning_values refmwsord;
    select okres from datatookres(current_date,0,0,0,0) into period;
    insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator,
        priority, wh, regtime, timestartdecl, timestopdecl,
        branch, period, cor, rec, status, manmwsacts)
      values (:refmwsord, :mwsordtype, :stocktaking, 'M', null, null, null,
          0, :wh, current_timestamp(0), current_timestamp(0), current_timestamp(0),
          :branch, :period, :cor, :rec, 1, :manmwsacts);
  end
end^
SET TERM ; ^
