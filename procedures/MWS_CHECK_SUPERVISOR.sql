--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CHECK_SUPERVISOR(
      OPERATOR integer,
      WH varchar(3) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint)
   as
begin
  select coalesce(o.supervisor,0)
    from opermag o
    where o.operator = :operator and o.magazyn = :wh
    into status;
end^
SET TERM ; ^
