--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_ILLPAY(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      ECOLUMN integer)
  returns (
      RET numeric(14,2))
   as
declare variable COMPANY integer;
declare variable CORRECTION numeric(14,2);
declare variable CPER varchar(6);
begin
  --DU: personel - funkcja liczy wynagrodzenie chorobowe

  select cper, company from epayrolls
    where ref = :payroll
    into :cper, :company;

  select sum(payamount) from eabsences
    where employee = :employee and ecolumn = :ecolumn
      and epayroll = :payroll and correction = 0
    into :ret;

  if (ret <> 0) then
  begin
    if (exists(select first 1 1 from epayrolls r
                 join eprempl m on (m.epayroll = r.ref and m.employee = :employee) --BS38837
                 left join emplcontracts c on (c.ref = r.emplcontract and c.iflags like '%FC;%')  --PR48961
          where r.status = 0
            and /* r.empltype = 1 and */ (r.tosbase = 1 or (r.tosbase is null and c.ref is not null)) --PR48961
            and r.company = :company
            and r.cper < :cper)
    ) then
      exception ef_expt 'Nie wszystkie poprzednie listy płac lub rachunki są zamknięte!';
  end

--MW:korekty
  select sum(case when correction = 1 then -payamount else payamount end)
    from eabsences
    where employee = :employee and ecolumn = :ecolumn and correction in (1,2)
      and (ayear || case when amonth <= 9 then '0' || amonth else amonth end) <= :cper
      and (/*corepayroll is null or wylaczone przy PR48961*/ corepayroll = :payroll)
    into :correction;

  ret = coalesce(ret, 0) + coalesce(correction, 0);
  suspend;
end^
SET TERM ; ^
