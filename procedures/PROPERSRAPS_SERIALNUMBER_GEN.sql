--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PROPERSRAPS_SERIALNUMBER_GEN(
      PROPERSRAP integer)
   as
declare variable gen varchar(40);
declare variable amount numeric(14,4);
declare variable serjedn smallint;
declare variable i numeric(14,4);
declare variable prdepard varchar(20);
begin
  select so.serialnumbergen, r.amount, t.serjedn, g.prdepart
    from propersraps r
      join prschedopers o on (r.prschedoper = o.ref)
      join prshopers so on (o.shoper = so.ref)
      join prschedguides g on (g.ref = o.guide)
      join towary t on (g.ktm = t.ktm)
    where r.ref = :propersrap
    into :gen, :amount, :serjedn, :prdepard;

  if (coalesce(:serjedn,0) = 0) then
    exception PROPERSRAP_ERROR 'Generowanie numerów dozwolone dla pojedyńczych wyrobów';

  if (coalesce(:gen,'') = '') then
    exception PROPERSRAP_ERROR 'Operacja produkcyjna nie ma zdefiniowanego generatora num.ser.';

  select sum(s.ilosc)
    from dokumser s
    where s.typ = 'R'
      and s.refpoz = :propersrap
    into :i;

  i = coalesce(i,0);
  while (i <:amount) do
  begin
    insert into dokumser(refpoz, typ, ilosc, numbergen,  genrej)
      values (:propersrap, 'R', 1, :gen, :prdepard);
    i = i + 1;
  end
end^
SET TERM ; ^
