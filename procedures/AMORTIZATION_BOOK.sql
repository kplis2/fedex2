--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AMORTIZATION_BOOK(
      AMPERIOD integer,
      DOCTYPE integer,
      OPERID integer,
      AKCEPT smallint,
      DATA timestamp)
  returns (
      BKDOC integer)
   as
DECLARE VARIABLE BKREG VARCHAR(10);
DECLARE VARIABLE BOOKING VARCHAR(355);
begin
  --- TOFIX - do dokończenia
  BKDOC = 0;
  select BKDOCTYPES.bkreg from BKDOCTYPES where REF=:doctype
    into :bkreg;
  execute procedure GEN_REF('BKDOCS') returning_values bkdoc;
  insert into BKDOCS(REF,BKREG,DOCTYPE, TRANSDATE, SYMBOL, DESCRIPT, DOCDATE,
                     REGOPER, OTABLE, OREF)
              values (:bkdoc, :bkreg, :doctype, :DATA, 'AM/1/2003', 'amortyzacja', :DATA,
                     :OPERID, 'AMPERIOD', :amperiod);

--  execute procedure x_autodecree(bkdoc, 13);

  if (akcept = 1) then begin
    update BKDOCS set STATUS = 1, ACCOPER = :operid, ACCDATE = current_date where REF=:bkdoc;
    execute procedure GET_CONFIG('BOOK_WITH_ACCEPT', 2) returning_values booking;
    if (booking = '1') then
      update BKDOCS set STATUS = 2, BOOKOPER = :operid, BOOKDATE = current_date where REF=:bkdoc;
  end
  if (bkdoc > 0) then
    update AMPERIODS set STATUS=2 where REF=:amperiod;
end^
SET TERM ; ^
