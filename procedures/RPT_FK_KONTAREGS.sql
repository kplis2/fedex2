--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_KONTAREGS(
      FROMPERIOD varchar(6) CHARACTER SET UTF8                           ,
      TOPERIOD varchar(6) CHARACTER SET UTF8                           ,
      CURRENCY varchar(3) CHARACTER SET UTF8                           ,
      BKREG varchar(15) CHARACTER SET UTF8                           ,
      ACCTYP smallint,
      ACCOUNTMASK ACCOUNT_ID,
      FROMACCOUNT ACCOUNT_ID,
      TOACCOUNT ACCOUNT_ID,
      COMPANY integer)
  returns (
      REF integer,
      SYMBOL varchar(3) CHARACTER SET UTF8                           ,
      ACCOUNT ACCOUNT_ID,
      DESCRIPT varchar(60) CHARACTER SET UTF8                           ,
      DEBIT numeric(15,2),
      CREDIT numeric(15,2),
      BDEBIT numeric(15,2),
      BCREDIT numeric(15,2),
      ISACCOUNT smallint)
   as
declare variable YEARID integer;
begin
  --TOFIX!!! - currency
  ref = 0;
  execute procedure create_account_mask(accountmask) returning_values accountmask;
  if (accountmask = '%') then accountmask = null;
  select yearid from BKPERIODS where company = :company and ID = :fromperiod
    into :yearid;
  if (toaccount is not null and toaccount <> '') then
  begin
    select max(account)
      from accounting
      where (account <= :toaccount or account like :toaccount || '%')
        and yearid = :yearid and accounting.company = :company
      into :toaccount;
  end

  for
    select B.symbol, max(B.descript), sum(D.debit), sum(d.credit)
      from bkaccounts B
        join decrees D on (B.yearid = :yearid and D.accref = B.ref
            and D.period <= :toperiod and D.period >= :fromperiod and D.status > 1
            and D.bkreg = :bkreg)
      where ((:ACCOUNTMASK is null) or (:accountmask = '') or (D.account like :accountmask))
        and ((:fromaccount is null) or (:fromaccount = '') or (:fromaccount <= D.account))
        and ((:toaccount is null) or (:toaccount = '') or (:toaccount >= D.account))
        and ((:acctyp = 0 and B.bktype < 2) or (:acctyp=1 and (B.bktype = 2 or B.bktype = 3)) or :acctyp = 2)
        and B.company = :company
      group by B.symbol
      order by B.symbol
      into :account, :descript, :debit, :credit
  do begin
    symbol = cast(substring(account from 1 for 3) as varchar(3));
    bdebit = null;
    bcredit = null;
    isaccount = 0;
    if (debit is null) then debit = 0;
    if (credit is null) then credit = 0;
    if (abs(credit) > abs(debit)) then
      bcredit = credit - debit;
    else
      bdebit = debit - credit;
    if (bdebit is null) then bdebit = 0;
    if (bcredit is null) then bcredit = 0;
    ref = ref + 1;
    isaccount = 1;
    suspend;
  end
end^
SET TERM ; ^
