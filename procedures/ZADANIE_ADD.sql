--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ZADANIE_ADD(
      ZADANIE DEFZADAN_ID,
      OPERROLEZAL NEOS_ID,
      NAZWA STRING80,
      OPIS STRING2500 = null,
      DATADO date = current_date,
      OPERROLEWYK NEOS_ID = null,
      DATA date = current_date)
  returns (
      REF ZADANIA_ID)
   as
declare variable OPERZAL OPERATOR_ID;
declare variable OPERWYK OPERATOR_ID;
begin
  operzal = (select ref from operator where uuid = :operrolezal);
  if(operrolewyk is null) then
    operwyk = (select first 1 ref from operator order by rand() desc);
  else
    operwyk = (select ref from operator where uuid = :operrolewyk);

  insert into zadania (ZADANIE, DATA, DATADO, NAZWA, OPERZAL, OPERWYK, OPIS)
  values (:zadanie, :data, :datado, :nazwa, :operzal, :operwyk, :opis)
  returning ref into :ref;

  suspend;
end^
SET TERM ; ^
