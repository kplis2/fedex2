--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_GET_LISTYWYSD_ZAL(
      MODE SMALLINT_ID,
      REFRODZICA INTEGER_ID,
      CAR STRING20)
   as
  declare variable refpaczki integer_id;
  declare variable rodzic integer_id;
begin
  if (refrodzica is not null) then
    for
      select l.ref
        from listywysdroz_opk l
        where l.rodzic = :refrodzica
      into :refpaczki
    do begin
      execute procedure x_mws_get_listywysd_zal (mode, refpaczki ,car);
      update listywysdroz_opk o
        set o.x_zaladowane = :mode , o.x_mwsconstlock = :car
        where o.ref = :refpaczki;
    end
end^
SET TERM ; ^
