--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDEDET_CLONE(
      REFFROM integer,
      QUANTITY numeric(14,4),
      OVERLIMIT numeric(14,4))
  returns (
      REFTO integer)
   as
declare variable actquantity numeric(14,4);
declare variable actoverlimit numeric(14,4);
begin
  if (coalesce(:quantity,0) <= 0) then
    exception prschedguidedet_error 'Ilość musi być większa od 0';
  select quantity, overlimit from prschedguidedets where ref = :reffrom
    into actquantity, actoverlimit;
  if (actquantity <= quantity) then
    exception prschedguidedet_error 'Nie zostanie nic na oryginalnej pozycji rozpiski';
  else
    actquantity = actquantity - quantity;
  execute procedure GEN_REF('PRSCHEDGUIDEDETS') returning_values refto;
  update prschedguidedets set sourceupdate = 1, quantity = :actquantity where ref = :reffrom;
  insert into prschedguidedets (ref,prnagzam, prpozzam, prschedguide, prschedguidepos, stable, sref,
      ktm, wersjaref, quantity, wh, doctype, out, overlimit, ssource, accept, prshmat, reportfactor,
      prgruparozl, lot, shortage, shortageratio, prschedoper, byproduct, prshortage, autodoc, sourceupdate, prgengroup)
    select :refto,prnagzam, prpozzam, prschedguide, prschedguidepos, stable, sref,
        ktm, wersjaref, :quantity, wh, doctype, out, :actoverlimit, ssource, accept, prshmat, reportfactor,
        prgruparozl, lot, shortage, shortageratio, prschedoper, byproduct, prshortage, autodoc, sourceupdate, prgengroup
      from prschedguidedets
      where ref = :reffrom;
  update prschedguidedets set sourceupdate = 0 where ref in (:reffrom,:refto);
end^
SET TERM ; ^
