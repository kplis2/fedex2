--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_FILL_SYSTABLES as
declare variable table_name varchar(31);
begin
  for
    select rdb$relation_name
      from rdb$relations where rdb$system_flag = 0
      into :table_name
  do begin
    if (not exists (select table_name from sys_tables where table_name = :table_name)) then
      insert into sys_tables (table_name, ttype) values (:table_name, 2);
  end
end^
SET TERM ; ^
