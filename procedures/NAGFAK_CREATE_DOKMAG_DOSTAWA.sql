--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_CREATE_DOKMAG_DOSTAWA(
      FAK integer,
      DATA timestamp,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           )
  returns (
      DOSTAWA integer)
   as
declare variable korekta smallint;
declare variable zakup smallint;
declare variable grupa integer;
declare variable dostawca integer;
begin
  select TYPFAK.KOREKTA, TYPFAK.ZAKUP,  NAGFAK.GRUPADOK, NAGFAK.dostawca
   from TYPFAK join NAGFAK on (TYPFAK.symbol = NAGFAK.TYP) where NAGFAK.REF = :fak into :korekta, :zakup, :grupa, :dostawca;
  if(:korekta = 1 and :zakup  = 1) then begin
    /* wyciagniecie dostawy z historii */
    select max(dostawa) from DOKUMNAG join NAGFAK on ( DOKUMNAG.FAKTURA = NAGFAK.REF) where NAGFAK.grupadok  = :GRUPA into :dostawa;
    if(:dostawa is null or (:dostawa = 0)) then exception NAGFAK_DOKNAG_CREATE_DOSTAWAWR;
  end else begin
    execute procedure DOKUMNAG_CREATE_DOKMAG_DOSTAWA(NULL,:dostawca,:data,:magazyn,NULL,NULL,NULL,NULL)
    returning_values :dostawa;
  end
end^
SET TERM ; ^
