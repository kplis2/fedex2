--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_FUN_DG111(
      PERIOD varchar(6) CHARACTER SET UTF8                           )
  returns (
      AMOUNT numeric(15,2))
   as
begin

  select sum(p.pvalue)
    from epayrolls e
    join EPRPOS p on (e.ref = p.payroll)
    where e.empltype = 1
      and e.tper = :PERIOD
      and (p.ecolumn = 6100
        or p.ecolumn = 6110
        or p.ecolumn = 6120
        or p.ecolumn = 6130
        )
  into :AMOUNT;
  AMOUNT = AMOUNT/1000;
  suspend;
end^
SET TERM ; ^
