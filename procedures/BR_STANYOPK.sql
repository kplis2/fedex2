--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_STANYOPK(
      ASLODEF integer,
      ASLOPOZ integer,
      ZAKRES smallint,
      ODDATY timestamp,
      DODATY timestamp,
      TRYB smallint)
  returns (
      REF integer,
      SLODEF integer,
      SLOPOZ integer,
      DOKUMNAG integer,
      POZDOKUM integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      WERSJAREF integer,
      ILOSC numeric(14,4),
      WARTMAG numeric(14,2),
      CENAMAG numeric(14,4),
      CENASPR numeric(14,4),
      KAUCJA numeric(14,4),
      ZREAL smallint,
      ILZREAL numeric(14,4),
      WARTMAGZREAL numeric(14,2),
      KAUCJAZREAL numeric(14,2),
      KAUCJASTAN numeric(14,2),
      WARTMAGSTAN numeric(14,2),
      STAN numeric(14,4),
      DATA timestamp,
      DATAZAMK timestamp,
      NAZWAT varchar(80) CHARACTER SET UTF8                           ,
      NAZWAW varchar(50) CHARACTER SET UTF8                           ,
      KOD varchar(100) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      KODFK varchar(255) CHARACTER SET UTF8                           ,
      SYMBOLDOKMAG varchar(20) CHARACTER SET UTF8                           ,
      USLUGA integer)
   as
begin
  if(:tryb is null) then tryb = 0;
  if(:tryb = 0) then begin
    for select REF, SLODEF, SLOPOZ, POZDOKUM, KTM,WERSJA,WERSJAREF,
        ILOSC, CENAMAG, WARTMAG, CENASPR, KAUCJA,
        ZREAL,DATAZAMK, data,
        ILZREAL, WARTMAGZREAL, KAUCJAZREAL, KAUCJASTAN, WARTMAGSTAN, STAN, USLUGA
    from stanyopk
    where (:aslodef = 0 or (:aslodef is null) or (:aslodef = SLODEF))
      and (:aslopoz = 0 or (:aslopoz is null) or (:aslopoz = SLOPOZ))
      and (:zakres = 0 or (:zakres = 1 and zreal = 0) or (:zakres = 2 and zreal = 1))
      and (:oddaty is null or (stanyopk.datazamk is null) or (stanyopk.datazamk >= :oddaty))
      and (:dodaty is null or (stanyopk.data <= :dodaty))
    into :ref, :slodef, :slopoz, :pozdokum, :ktm, :wersja, :wersjaref,
         :ilosc, :cenamag, :wartmag, :cenaspr, :kaucja,
         :zreal, :datazamk, :data,
         :ilzreal, :wartmagzreal, :kaucjazreal,:kaucjastan, :wartmagstan, :stan, :usluga
    do begin
    --if (:pozdokum = 1091680) then  exception test_break 'stan ' || :stan || '  ilosc '||:ilosc;
      select NAZWA, NAZWAT from WERSJE where ref = :wersjaref into :nazwaw, :nazwat;
      select DOKUMPOZ.DOKUMENT, DOKUMNAG.SYMBOL from DOKUMPOZ left join DOKUMNAG on DOKUMNAG.REF = DOKUMPOZ.DOKUMENT
       where DOKUMPOZ.REF=:pozdokum into :dokumnag, :symboldokmag;
      select KOD, NAZWA, KONTOFK from SLO_DANE(:slodef, :slopoz) into :kod, :nazwa, :kodfk;
      suspend;
    end
  end else begin
    ref = 0;
    for select SLODEF, SLOPOZ, count(*),
         sum(WARTMAG), sum(KAUCJA),
        min(data),
        sum(WARTMAGZREAL), sum(KAUCJAZREAL), sum(WARTMAGSTAN), sum(KAUCJASTAN)
    from stanyopk
    where (:aslodef = 0 or (:aslodef is null) or (:aslodef = SLODEF))
      and (:aslopoz = 0 or (:aslopoz is null) or (:aslopoz = SLOPOZ))
      and (:zakres = 0 or (:zakres = 1 and zreal = 0) or (:zakres = 2 and zreal = 1))
      and (:oddaty is null or (stanyopk.datazamk is null) or (stanyopk.datazamk >= :oddaty))
      and (:dodaty is null or (stanyopk.data <= :dodaty))
    group by slodef, slopoz
    into :slodef, :slopoz, :ilosc,
         :wartmag, :kaucja,
         :data,
         :wartmagzreal, :kaucjazreal, :wartmagstan, :kaucjastan
    do begin
      ref = ref + 1;
      select KOD, NAZWA, KONTOFK from SLO_DANE(:slodef, :slopoz) into :kod, :nazwa, :kodfk;
      suspend;
    end
  end
end^
SET TERM ; ^
