--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_IMP_NAGZAM_PROCESS(
      REF_IMP INTEGER_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING1024)
   as
declare variable REF_LOC INTEGER_ID;
declare variable NAGID_IMP STRING120;
declare variable SYMBOL_IMP STRING20;
declare variable NUMER_IMP INTEGER_ID;
declare variable TYP_IMP STRING3;
declare variable DATA_IMP timestamp;
declare variable MAGAZYN_IMP STRING6;
declare variable MAG2_IMP STRING6;
declare variable WYDANIA_IMP SMALLINT_ID;
declare variable ZEWNETRZNY_IMP SMALLINT_ID;
declare variable DOSTAWCAID_IMP STRING120;
declare variable KLIENTID_IMP STRING120;
declare variable ODBIORCAID_IMP STRING120;
declare variable DULICA_IMP STRING255;
declare variable DNRDOMU_IMP STRING10;
declare variable DNRLOKALU_IMP STRING10;
declare variable DKRAJID_IMP STRING20;
declare variable DMIASTOID_IMP STRING20;
declare variable DKODPOCZTOWY_IMP STRING10;
declare variable DTELEFON_IMP STRING255;
declare variable DNAZWA_IMP STRING255;
declare variable PLATNOSC_IMP STRING40;
declare variable DOSTAWA_IMP STRING255;
declare variable UWAGIWEW_IMP STRING1024;
declare variable UWAGIZEW_IMP STRING1024;
declare variable UWAGISPED_IMP STRING255;
declare variable DATAWYSYLKI_IMP timestamp;
declare variable DOZAPLATY_IMP NUMERIC_14_4;
declare variable PRIORYTET_IMP INTEGER_ID;
declare variable SOBOTA_IMP SMALLINT_ID;
declare variable IDDOKORYGOWANEGO_IMP INTEGER_ID;
declare variable ODROCZONEWYDANIE_IMP SMALLINT_ID;
declare variable SREF_IMP INTEGER_ID;
declare variable MWSSTATUS_IMP INTEGER_ID;
declare variable TYPSENTE_IMP STRING3;
declare variable STATUS_IMP INTEGER_ID;
declare variable DATAPRZET_IMP timestamp;
declare variable IDSTATUS_IMP INTEGER_ID;
declare variable ERRMSG_IMP STRING1024;
declare variable OKRES STRING6;
declare variable ODBIORCA STRING40;
declare variable KLIENT INTEGER_ID;
declare variable DOSTAWCA INTEGER_ID;
declare variable STATUS_POZ_LOC SMALLINT_ID;
declare variable MSG_POZ_LOC STRING1024;
declare variable sposzap platnosc_id;
declare variable sposdost sposdost_id;
declare variable odbiorca_ref integer_id;
begin
  status = 8;
  msg = '';

  if (not exists(select first 1 1 from x_imp_dokumnag x where x.ref = :ref_imp)) then begin
    msg = 'Bledny ref_imp :(';
    status = 8;
    suspend;
    exit;
  end

  select NAGID, SYMBOL, NUMER, TYP, data, MAGAZYN, MAG2, WYDANIA, ZEWNETRZNY, DOSTAWCAID, KLIENTID, ODBIORCAID,
         DULICA, DNRDOMU, DNRLOKALU, DKRAJID, DMIASTOID, DKODPOCZTOWY, DTELEFON, DNAZWA, PLATNOSC, DOSTAWA, UWAGIWEW,
         UWAGIZEW, UWAGISPED, DATAWYSYLKI, DOZAPLATY, PRIORYTET, SOBOTA, IDDOKORYGOWANEGO, ODROCZONEWYDANIE, SREF,
         MWSSTATUS, TYPSENTE, STATUS, DATAPRZET, IDSTATUS, ERRMSG
  from X_IMP_DOKUMNAG
  where ref = :ref_imp
  into   :nagid_imp, :SYMBOL_imp, :NUMER_imp, :TYP_imp, :data_imp, :MAGAZYN_imp, :MAG2_imp, :WYDANIA_imp, :ZEWNETRZNY_imp, :DOSTAWCAID_imp, :KLIENTID_imp,
         :ODBIORCAID_imp, :DULICA_imp, :DNRDOMU_imp, :DNRLOKALU_imp, :DKRAJID_imp, :DMIASTOID_imp, :DKODPOCZTOWY_imp, :DTELEFON_imp, :DNAZWA_imp, :PLATNOSC_imp,
         :DOSTAWA_imp, :UWAGIWEW_imp, :UWAGIZEW_imp, :UWAGISPED_imp, :DATAWYSYLKI_imp, :DOZAPLATY_imp, :PRIORYTET_imp, :SOBOTA_imp, :IDDOKORYGOWANEGO_imp,
         :odroczonewydanie_imp, :SREF_imp, :MWSSTATUS_imp, :TYPSENTE_imp, :STATUS_imp, :DATAPRZET_imp, :IDSTATUS_imp, :ERRMSG_imp;

-- sprawdzenie czy zamowienie juz jest zaimportowane
  if (exists(select first 1 1 from nagzam n where n.int_id = :nagid_imp)) then begin
    msg = 'Dokument jest juz zaimportowany, nie mozna ponownie importowac!';
    status = 8;
    suspend;
    exit;
  end

-- wertyfikacja Magazyn -> DEFMAGAZ
  if (coalesce(magazyn_imp, '') = '') then begin
    msg = 'Brak podanego magazynu';
    status = 8;
    suspend;
    exit;
  end

  if (:magazyn_imp <> '1') then begin
    msg = 'Bledny magazyn' ||coalesce(magazyn_imp,'');
    status = 8;
    suspend;
    exit;
  end
  else begin
    magazyn_imp = 'MWS';
  end

  select nazwa, ref from odbiorcy o where o.int_id = :odbiorcaid_imp into :odbiorca, :odbiorca_ref;
  select ref from klienci k where k.int_id = :KLIENTID_imp into :klient;
  select ref from dostawcy d where d.int_id = :DOSTAWCAID_imp into :dostawca;

  select okres from datatookres( cast( :data_imp as date),null,null,null,null) into :okres;

  if (coalesce(platnosc_imp,'') <> '') then begin
    select p.ref
        from platnosci p
        where upper(p.opis) = upper(:platnosc_imp)
    into :sposzap;
    if (sposzap is null) then begin
        msg = 'Bledny sposob zaplaty' ||coalesce(platnosc_imp,'');
        status = 8;
        suspend;
        exit;
    end
  end
  --weryfikacja sposobu dostawy
  if (coalesce(dostawa_imp,'') <> '') then begin
    select s.ref
        from sposdost s
        where upper(s.nazwa) = upper(:dostawa_imp)
    into :sposdost;
    if (sposdost is null) then begin
        msg = 'Bledny sposob dostawy' ||coalesce(dostawa_imp,'');
        status = 8;
        suspend;
        exit;
    end
  end
  insert into nagzam  (rejestr, typzam, okres, klient, datawe, sposzap, odbiorcaid,  sposdost, uwagi, magazyn, odbiorca,
   dulica, dmiasto, dkodp,  dpoczta, org_ref, x_imp_ref,int_id, int_symbol, id) --DTS ZG138468
    values('SPR', 'ZAM', :okres, :klient, :data_imp, :sposzap, :odbiorca_ref, :sposdost, :uwagisped_imp, :magazyn_imp, :odbiorca,
    :dulica_imp, :dmiastoid_imp, :dkodpocztowy_imp, :dmiastoid_imp, :ref_imp, :ref_imp, :nagid_imp, :symbol_imp, :symbol_imp) --DTS ZG138468
  returning REF
  into :ref_loc;

-- proba wykonania dodania pozycji do DOKUMPOZ
  select status, msg  from X_IMP_POZZAM_PROCESS (:ref_imp)
     into :status_poz_loc, :msg_poz_loc;

-- weryfikacja sukcesu dodawania pozycji
  if (:status_poz_loc <> 0) then begin
-- jesli blad --> czyszczenie pozycji i naglowka
    delete from pozzam n
      where n.zamowienie = :ref_loc;

    delete from nagzam n
      where n.ref = :ref_loc;

    msg = coalesce(msg_poz_loc, '') ||' Usuniete wpisy pozycji i naglowek.';
    status = 8;
    suspend;
    exit;
  end
-- update -> przetworzony
  status = 0;
  msg = 'ok :)';
  suspend;

end^
SET TERM ; ^
