--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_SCHEDULER_RET_MSG(
      INSTANCE SYS_SCHEDULEHIST_ID,
      ERROR_LVL smallint,
      MSG blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           ,
      SCHEDULER_SYMBOL SYS_SCHEDULER_SYMBOL_ID = null)
   as
begin
  /*Procedura ustawia error lvl i message*/
  update sys_schedulehist s set s.return_msg = :msg,
                                s.error_level = :error_lvl,
                                s.scheduler_symbol = :SCHEDULER_SYMBOL
    where s.ref = :instance;
  suspend;
end^
SET TERM ; ^
