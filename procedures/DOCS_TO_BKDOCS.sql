--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOCS_TO_BKDOCS(
      TYP smallint,
      DOCUMENT_REF integer,
      OPERATOR integer,
      TRYB smallint,
      AUTODECREE smallint)
   as
declare variable BKDOCNUMBER integer;
declare variable BKDOCVATNUMBER integer;
declare variable ILE integer;
declare variable DALEJ integer;
declare variable PARAM1 varchar(20);
declare variable PARAM2 varchar(20);
declare variable PARAM3 varchar(20);
begin
--sprawdzenie czy przypadkiem nie jest zablowowany w ksiegowosci
  dalej = 1;
  if (typ = 1) then
  begin
    for select param1, param2, param3
      from docnottobkdocs
      where typ = 1
      into :param1, :param2,  :param3
      do begin
        select count(*)
          from dokumnag D
          where d.ref = :document_ref and
            not ((TYP= :param1 or :param1 = '')
              and (MAGAZYN= :param2 or :param2 = '') and
               (ODDZIAL = :param3 or :param3 = ''))
         into :ile;
         if (ile = 0 or ile is null) then
           dalej = 0;
      end

  end
-- kod wlasciwy
  if (dalej = 1) then
  begin
    execute procedure DOCUMENTS_TO_BKDOCS(:TYP, :DOCUMENT_REF, :OPERATOR, :TRYB, :AUTODECREE)
    returning_values :BKDOCNUMBER, :BKDOCVATNUMBER;

    if (bkdocnumber>0) then
    begin
      if (typ=0) then
        update NAGFAK set blokada=3, OLDBKDOCNUM=:BKDOCNUMBER, OLDBKVDOCNUM=:BKDOCVATNUMBER where ref=:document_ref;
      else if (typ=1) then
        update DOKUMNAG set BLOKADA = bin_or(BLOKADA,4), OLDBKDOCNUM=:BKDOCNUMBER where ref=:document_ref;
      else if (typ=2) then
        update RKRAPKAS set STATUS = 2, OLDBKDOCNUM=:BKDOCNUMBER where ref=:document_ref;
      else if (typ=3) then
        update DOKUMNOT set BLOKADA = bin_or(BLOKADA,4), OLDBKDOCNUM=:BKDOCNUMBER where ref=:document_ref;
      else if (typ=7) then
        update VALDOCS set status = 1, OLDBKDOCNUM=:BKDOCNUMBER where ref=:document_ref;
    end
  end
--
end^
SET TERM ; ^
