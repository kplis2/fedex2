--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_STOCKTAKIN_SETTL_PRICE(
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      WH varchar(3) CHARACTER SET UTF8                           ,
      STOPWH smallint = 0)
  returns (
      PRICE numeric(14,4),
      OUTWH varchar(3) CHARACTER SET UTF8                           )
   as
begin
  outwh = wh;
  select first 1 c.cena
    from stanycen c
      left join dostawy d on (d.ref = c.dostawa)
    where c.wersjaref = :vers and c.ilosc > 0 and c.magazyn = :wh and c.cena > 0
    order by d.dataotw desc
    into price;
  if (coalesce(:price,0) = 0) then
  begin
    select first 1 c.cena
      from stanycen c
        left join dostawy d on (d.ref = c.dostawa)
      where c.wersjaref = :vers and c.magazyn = :wh and c.cena > 0
      order by d.dataotw desc
      into price;
  end
  if (coalesce(:price,0) = 0) then
  begin
    select first 1 c.cena, c.magazyn
      from stanycen c
        left join dostawy d on (d.ref = c.dostawa)
      where c.wersjaref = :vers and c.ilosc > 0 and c.cena > 0
      order by d.dataotw desc
      into price, outwh;
  end
  if (coalesce(:price,0) = 0) then
  begin
    select first 1 c.cena, c.magazyn
      from stanycen c
        left join dostawy d on (d.ref = c.dostawa)
      where c.wersjaref = :vers and c.cena > 0
      order by d.dataotw desc
      into price, outwh;
  end
  if (coalesce(:price,0) = 0) then
  begin
    select t.cena_zakn
      from wersje t
      where t.ref = :vers
      into price;
  end
  if (coalesce(:price,0) = 0) then
  begin
    select t.cena_zakn
      from towary t
      where t.ktm = :good
      into price;
  end
  if (coalesce(:price,0) = 0 and :stopwh = 0) then
  begin
    select first 1 p.cena, d.magazyn
      from dokumnag d
        join dokumpoz p on (p.dokument = d.ref and p.wersjaref = :vers )
      where d.wydania = 0 and d.zewn = 1 and d.koryg = 0
        and d.magazyn = :wh and p.dostawa is not null
        and p.cena > 0
      order by d.data desc
      into price, outwh;
  end
  if (coalesce(:price,0) = 0 and :stopwh = 0) then
  begin
    select first 1 p.cena, d.magazyn
      from dokumnag d
        join dokumpoz p on (p.dokument = d.ref and p.wersjaref = :vers )
      where d.wydania = 0 and d.zewn = 1 and d.koryg = 0
        and p.dostawa is not null
        and p.cena > 0
      order by d.data desc
      into price, outwh;
  end
  if (price > 0 and stopwh = 1) then
    suspend;
end^
SET TERM ; ^
