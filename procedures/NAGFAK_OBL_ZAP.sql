--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_OBL_ZAP(
      FAKTURA integer)
   as
declare variable zaplacono numeric(14,2);
declare variable dozaplaty numeric(14,2);
declare variable zaplacono2 numeric(14,2);
declare variable potracenie numeric(14,2);
declare variable kwotazal numeric(14,2);
declare variable zakup integer;
declare variable klient integer;
declare variable nieobrot integer;
begin
  if(:faktura is null or (:faktura = 0))then exit;
  select esumwartbru - pesumwartbru, kwotazal, zakup, klient, nieobrot
    from NAGFAK
    where REF=:faktura
    into :dozaplaty, :kwotazal, :zakup, :klient, :nieobrot;
  if((:dozaplaty is null) or (:dozaplaty = 0))then exit;
  if(:dozaplaty > 0) then begin
    if(:zakup = 0) then begin
      select sum(MA) from ROZRACHP where faktura = :faktura and MA > 0 and stable <> 'POZFAK' into :zaplacono;
      select sum(WINIEN) from ROZRACHP where faktura = :faktura and WINIEN < 0 and stable <> 'POZFAK' into :zaplacono2;
      select sum(WINIEN) from ROZRACHP where faktura = :faktura and STABLE <> 'NAGFAK' and stable <> 'POZFAK'
        and SREF <> :faktura  and  SKAD <> 1 and WINIEN > 0 into :potracenie;
    end else begin
      select sum(WINIEN) from ROZRACHP where faktura = :faktura and WINIEN > 0 into :zaplacono;
      select sum(MA) from ROZRACHP where faktura = :faktura and MA < 0 into :zaplacono2;
    end
    if(:zaplacono2 is null) then zaplacono2 = 0;
    if(:zaplacono is null) then zaplacono = 0;
    if(:potracenie is null) then potracenie = 0;
    zaplacono = :zaplacono - :zaplacono2 - :potracenie;/*bo zaplacono2 zawsze ujemne*/
  end else begin
    if(:zakup = 0) then begin
      select sum(WINIEN) from ROZRACHP where faktura = :faktura and WINIEN > 0 and stable <> 'POZFAK' into :zaplacono;
      select sum(MA) from ROZRACHP where faktura = :faktura and MA < 0 and stable <> 'POZFAK' into :zaplacono2;
    end else begin
      select sum(MA) from ROZRACHP where faktura = :faktura and MA > 0 into :zaplacono;
      select sum(WINIEN) from ROZRACHP where faktura = :faktura and WINIEN < 0 into :zaplacono2;
    end
    if(:zaplacono2 is null) then zaplacono2 = 0;
    if(:zaplacono is null) then zaplacono = 0;
    zaplacono = :zaplacono - :zaplacono2;/*bo zaplacono2 zawsze ujemne*/
    zaplacono = -:zaplacono;
  end
  if(:zaplacono is null) then zaplacono =0;
/*  if(:kwotazal > 0) then
    zaplacono = :zaplacono + :kwotazal;*/ -- MS: teraz do tego jest rozrachroz
  update NAGFAK set zaplacono = :zaplacono where ref=:faktura;
  -- rozbicie platnosci na pozycje faktury
  if(:nieobrot<2) then
    execute procedure POZFAK_ZAPLACONO_CALC(:faktura);
end^
SET TERM ; ^
