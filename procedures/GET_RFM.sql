--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_RFM
  returns (
      KLIENT integer,
      RECENCY integer,
      FREQUENCY integer,
      MONEY integer,
      RFM integer)
   as
declare variable data timestamp;
declare variable cnt integer;
begin

  for select ref from klienci
  into :klient
  do begin
    recency = 0;
    frequency = 0;
    money = 0;

    select max(datasprz)
    from nagfak
    where klient=:klient and zakup=0 and akceptacja=1 and nieobrot<2
    into :data;
    if(current_timestamp(0)-:data<90) then recency = 24;
    else if(current_timestamp(0)-:data<180) then recency = 12;
    else if(current_timestamp(0)-:data<270) then recency = 6;
    else if(current_timestamp(0)-:data<360) then recency = 3;
    else recency = 0;

    cnt = 0;
    select count(*)
    from nagfak
    where klient=:klient and zakup=0 and akceptacja=1 and nieobrot<2
    and datasprz>current_date-365
    into :cnt;
    frequency = 4 * :cnt;

    select sum(sumwartnetzl-psumwartnetzl)
    from nagfak
    where klient=:klient and zakup=0 and akceptacja=1 and nieobrot<2
    and datasprz>current_date-365
    into :money;
    if(:money is null) then money = 0;
    money = :money/100;

    rfm = :recency + :frequency + :money;
    suspend;
  end
end^
SET TERM ; ^
