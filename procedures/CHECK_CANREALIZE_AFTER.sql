--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHECK_CANREALIZE_AFTER(
      REJZAM varchar(10) CHARACTER SET UTF8                           ,
      DOKUMENT integer,
      TYP char(1) CHARACTER SET UTF8                           )
  returns (
      SAREAL smallint)
   as
declare variable cnt integer;
begin
  sareal = 0;
  if(exists(select first 1 1 from BR_REALZAMPOZZAM_AFTER(:rejzam, :dokument,:typ))) then sareal = 1;
end^
SET TERM ; ^
