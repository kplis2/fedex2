--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INWENTAP_ILPLUS_WYCENA(
      INWENTAP integer,
      WERSJAREF integer,
      MAG varchar(3) CHARACTER SET UTF8                           ,
      DOSTAWA integer)
  returns (
      CENAPLUS numeric(14,2))
   as
declare variable strategia varchar(10);
declare variable cenapom numeric(14,2);
declare variable zpartiami smallint;
declare variable magbreak smallint;
begin
  zpartiami = null;
  if (inwentap is not null) then
    select i.zpartiami
      from inwentap p
        left join inwenta i on (i.ref = p.inwenta)
      where p.ref = :inwentap
      into zpartiami;
  select magbreak
    from defmagaz
    where symbol = :mag
    into magbreak;
  if (zpartiami is null) then zpartiami = 0;
  if (magbreak = 2 and zpartiami = 0) then
    zpartiami = 1;
  if (zpartiami is null) then zpartiami = 0;
  execute procedure getconfig('INPLUSWYCENA') returning_values :strategia;
  cenaplus = NULL;
  if (:strategia = '1') then    -- ostatnia cena zakupu
  begin
    select first 1 stanycen.cena from stanycen
      where stanycen.wersjaref = :wersjaref and stanycen.ilosc > 0 and stanycen.cena > 0
        and ((stanycen.dostawa = :dostawa and :zpartiami > 0) or :zpartiami = 0)
      order by stanycen.datafifo desc
    into :cenaplus;
  end
  else if (:strategia = '2') then  -- wedug najnizszej ceny
  begin
    select min(stanycen.cena) from stanycen
      where stanycen.wersjaref = :wersjaref and stanycen.ilosc > 0 and stanycen.magazyn = :mag
        and ((stanycen.dostawa = :dostawa and :zpartiami > 0) or :zpartiami = 0)
      into :cenaplus;
  end if (:strategia = '3') then  -- wedlug procedury xk_inplus_wycena
  begin
    execute procedure xk_inplus_wycena(inwentap, wersjaref, mag, dostawa)
      returning_values cenaplus;
  end
  /*domylna strategia - cena katalogowa*/
  if (:cenaplus is null) then
    select cena_zakn from wersje where ref=:wersjaref into :cenaplus;
end^
SET TERM ; ^
