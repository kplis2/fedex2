--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_CHECKWYS(
      DOKMAG integer)
   as
declare variable WYSLANE integer;
begin
  select count(*)
    from dokumpoz p
      left join towary t on (t.ktm = p.ktm)
    where p.dokument = :dokmag
      and p.iloscl > p.ilosclwys
      and t.usluga <> 1
      and coalesce(p.havefake,0) = 0
  into :wyslane;
  if(:wyslane > 0) then wyslane = 0;
  else wyslane = 1;
  update DOKUMNAG set WYSYLKADONE = :wyslane, WYDANO = :wyslane
    where ref=:dokmag and wysylkadone <> :wyslane;
end^
SET TERM ; ^
