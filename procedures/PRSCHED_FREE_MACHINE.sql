--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_FREE_MACHINE(
      WORKPLACE varchar(20) CHARACTER SET UTF8                           ,
      MACHINE integer,
      MODE integer,
      PRSCHEDULE integer = null)
  returns (
      RMACHINE integer,
      RSTART timestamp,
      REND timestamp)
   as
declare variable fromdate timestamp;
  declare variable todate timestamp;
  declare variable inactionfrom timestamp;
  declare variable inactionto timestamp;
  declare variable sql varchar(1024);
  declare variable operstart timestamp;
  declare variable operend timestamp;
  declare variable tmpend timestamp;
begin
  -- mode 0 -> z uwzgldnieniem operacji produkcyjnych
  -- mode 1 -> tylko przypisania, bez operacji produkcyjnych

  sql = 'select mwp.prmachine, mwp.fromdate, mwp.todate';
  sql = sql || ' from prmachineswplaces mwp';
  sql = sql || '   join prmachines p on (p.ref = mwp.prmachine)';
  sql = sql || ' where mwp.prworkplace = '''||:workplace||'''';
  if (:machine is not null) then
    sql = sql || ' and mwp.prmachine = '||:machine;
  sql = sql || ' order by mwp.fromdate, p.numer';


  for
    execute statement :sql
      into :rmachine, :fromdate, :todate
  do begin
    -- operacje ktore juz zajely ta mazyne
    rstart = :fromdate;
    rend = :todate;
    operstart = null;
    operend = null;
    for
      select o.starttime, o.endtime
        from prschedopers o
        where o.machine = :rmachine
          and coalesce(o.schedzamstatus,0) <= 1
          and o.status < 3
          and o.verified > 0
          and (o.starttime < :todate or :todate is null)
          and (o.endtime > :fromdate)
          and :mode = 0
          and (o.schedule = :prschedule or :prschedule is null)
       order by o.starttime
       into :operstart, :operend
    do begin
      if (:rstart  < :operstart and (:rstart < :todate or :todate is null)) then
      begin
        if (:operend < :todate or :todate is null) then
          rend = :operstart;
        else
          rend = :todate;

        -- awarie, remonty, wypadki
        inactionfrom = null;
        inactionto = null;
        tmpend = :rend;
        for
          select r.inactionfrom, r.inactionto
            from prmachrepairs r
            where r.machine = :rmachine
              and r.inactionfrom < :rend
              and r.inactionto > :rstart
            into :inactionfrom, :inactionto
        do begin
          if (:rstart < :inactionfrom and :rstart < :tmpend) then
          begin
            if (:inactionto < :tmpend) then
              rend = :inactionfrom;
            else
              rend = :inactionto;
            suspend;
          end 
          rstart = :inactionto;
        end
        if (:inactionfrom is null and :inactionto is null) then  --brak remontow ipt
          suspend;
        else if (:inactionto < :tmpend) then
        begin
          rend = :tmpend;
          suspend;
        end
      end
      rstart = :operend;
    end
    if (:operstart is not null and :todate is null) then
    begin
      rend = null;
      suspend;
    end

    if (:operstart is null and :operend is null) then -- brak przypisac
    begin
      --remonty bez maszyn
      inactionfrom = null;
      inactionto = null;
      tmpend = :rend;
      for
        select r.inactionfrom, r.inactionto
          from prmachrepairs r
          where r.machine = :rmachine
            and (r.inactionfrom < :rend or :rend is null)
            and r.inactionto > :rstart
          into :inactionfrom, :inactionto
      do begin
        if (:rstart < :inactionfrom and (:rstart < :tmpend or :tmpend is null)) then
        begin
          if (:inactionto < :tmpend or :tmpend is null) then
            rend = :inactionfrom;
          else
            rend = :inactionto;
          suspend;
        end
        rstart = :inactionto;
      end
      if (:inactionfrom is null and :inactionto is null) then  --brak remontow ipt
        suspend;
      else if (:inactionto < :tmpend or :tmpend is null) then
      begin
        rend = :tmpend;
        suspend;
      end
    end
  end
end^
SET TERM ; ^
