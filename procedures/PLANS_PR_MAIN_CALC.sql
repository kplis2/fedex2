--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PLANS_PR_MAIN_CALC(
      PLANS integer,
      PLANSCOL integer,
      UZUPELNIANIE smallint,
      AKTUALIZACJA smallint,
      PLANSROW integer)
  returns (
      VAL numeric(14,4))
   as
declare variable czasprod smallint;
declare variable stanymag smallint;
DECLARE VARIABLE KEY1 VARCHAR(20);
DECLARE VARIABLE TEMPODNI NUMERIC(14,4);
DECLARE VARIABLE TEMPOOKR NUMERIC(14,4);
DECLARE VARIABLE DATAPOCZ TIMESTAMP;
DECLARE VARIABLE DATAKON TIMESTAMP;
DECLARE VARIABLE CNT INTEGER;
declare variable realtime numeric(14,2);
declare variable ref integer;
declare variable income numeric(14,2);
declare variable expend numeric(14,2);
begin
  val = -1;
  if (:AKTUALIZACJA is null) then AKTUALIZACJA = 0;
  if (:uzupelnianie is null) then uzupelnianie = 0;
  if (:czasprod is null) then czasprod = 0;
  if (:stanymag is null) then stanymag = 0;
  select planscol.bdata, planscol.ldata from planscol where planscol.ref = :PLANSCOL
    into :datapocz, :datakon;
  select plansrow.KEY1 from plansrow
    where plansrow.plans = :PLANS and PLANSrow.ref = :PLANSROW into :key1;
  select p.realtime, p.ref from prsheets p where p.ktm = :key1 and p.status = 2 into :realtime, :ref;
  if(:ref>0) then begin
    val = 0;
    if(:czasprod = 1 and :realtime is not null and :realtime > 0)then begin
      if(floor(:realtime)<:realtime) then realtime = floor(:realtime) + 1;
      datapocz = :datapocz + :realtime;
      datakon = :datakon + :realtime;
    end
    execute procedure plans_ktm_from_to(:key1, :datapocz, :datakon) returning_values :income, :expend;
    val = :expend - :income;
    if (:val is null) then val = -1;
    else if (:val<0) then val = 0;
  end
  suspend;
end^
SET TERM ; ^
