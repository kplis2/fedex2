--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_SRVGOODDEF(
      CONTRTYPE integer,
      REFSREQ integer)
  returns (
      FIELD varchar(80) CHARACTER SET UTF8                           ,
      NVALUE numeric(14,4),
      DVALUE timestamp,
      SVALUE varchar(256) CHARACTER SET UTF8                           ,
      LABEL varchar(80) CHARACTER SET UTF8                           ,
      VALUETYPE smallint)
   as
declare variable typpola char;
declare variable wyrazenie varchar(1024);
begin
  for select srvgooddef.symbol, srvgooddef.descript from SRVGOODDEF
    where SRVGOODDEF.CONTRTYPE = :CONTRTYPE
    order by SRVGOODDEF.NUMBER
    into :field, :label
  do begin
    typpola = substring(:field from 1 for 1);
    wyrazenie = 'select '||:field||' from srvrequests where srvrequests.ref = '||:refsreq;
    if (typpola = 'N') then begin
      valuetype = 1;
      execute statement :wyrazenie into :nvalue;
    end else if (typpola = 'D') then begin
      valuetype = 2;
      execute statement :wyrazenie into :dvalue;
    end else if (typpola = 'S') then begin
      valuetype = 3;
      execute statement :wyrazenie into :svalue;
    end
    suspend;
  end
end^
SET TERM ; ^
