--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_NAGFAKZALKLI(
      KLIENT integer,
      TRYB integer)
  returns (
      REF integer,
      VAT varchar(5) CHARACTER SET UTF8                           ,
      SYMBOL varchar(100) CHARACTER SET UTF8                           ,
      OKRES varchar(6) CHARACTER SET UTF8                           ,
      ODDZIAL varchar(20) CHARACTER SET UTF8                           ,
      TYP varchar(10) CHARACTER SET UTF8                           ,
      EWARTNET numeric(14,2),
      EWARTBRU numeric(14,2),
      PEWARTNET numeric(14,2),
      PEWARTBRU numeric(14,2),
      WARTNET numeric(14,2),
      WARTBRU numeric(14,2),
      EWARTNETZL numeric(14,2),
      EWARTBRUZL numeric(14,2),
      PEWARTNETZL numeric(14,2),
      PEWARTBRUZL numeric(14,2),
      WARTNETZL numeric(14,2),
      WARTBRUZL numeric(14,2),
      KWOTAZAL numeric(14,2))
   as
begin
    for select nagfak.ref,rozfak.vat,nagfak.symbol,nagfak.okres,
      nagfak.oddzial,nagfak.typ,nagfak.kwotazal,
      rozfak.esumwartnet ,rozfak.esumwartbru,
      rozfak.esumwartnetzl,rozfak.esumwartbruzl
      from nagfak join rozfak on (rozfak.dokument=nagfak.ref)
      where nagfak.zaliczkowy>0 and nagfak.klient=:klient
      into :ref,:vat,:symbol,:okres,
        :ODDZIAL,:typ,:kwotazal,
        :ewartnet,:ewartbru,:ewartnetzl,:ewartbruzl
    do begin
      select sum(nagfakzal.wartnet),sum(nagfakzal.wartbru),sum(nagfakzal.wartnetzl),sum(nagfakzal.wartbruzl)
        from nagfakzal
        left join nagfak on (nagfak.ref=nagfakzal.faktura)
        where nagfakzal.fakturazal=:ref and nagfakzal.vat=:vat and nagfak.anulowanie=0
        into :pewartnet, :pewartbru, :pewartnetzl, :pewartbruzl;
      if(:pewartnet is null) then pewartnet = 0;
      if(:pewartbru is null) then pewartbru = 0;
      if(:pewartnetzl is null) then pewartnetzl = 0;
      if(:pewartbruzl is null) then pewartbruzl = 0;
      wartnet = :ewartnet - :pewartnet;
      wartbru = :ewartbru - :pewartbru;
      wartnetzl = :ewartnetzl - :pewartnetzl;
      wartbruzl = :ewartbruzl - :pewartbruzl;
      if(:tryb = 1 and :wartbru = 0) then
        suspend;
      else if(:TRYB = 2 and :wartbru <> 0) then
        suspend;
      else if(:tryb = 0) then
        suspend;
    end
end^
SET TERM ; ^
