--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_DATA_FROM_VAT7_16(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PAR_K37 numeric(14,2),
      PAR_K41 numeric(14,2),
      PAR_K46 numeric(14,2),
      PAR_K50 numeric(14,2),
      PAR_K53 numeric(14,2),
      PAR_K56 numeric(14,2),
      PAR_K57 numeric(14,2),
      PAR_K58 numeric(14,2),
      PAR_K60 SMALLINT_ID,
      PAR_K61 SMALLINT_ID,
      PAR_K62 SMALLINT_ID,
      PAR_K63 SMALLINT_ID,
      PAR_K64 SMALLINT_ID,
      PAR_K65 SMALLINT_ID,
      PAR_K66 SMALLINT_ID,
      PAR_K67 SMALLINT_ID,
      PAR_K68 INTEGER_ID,
      DOCDATE STRING255,
      COMPANY INTEGER_ID,
      CORRECT SMALLINT_ID,
      USERFNAME STRING255,
      USERSNAME STRING255)
  returns (
      WSP35 numeric(14,2),
      K1 STRING255,
      K4 STRING255,
      K5 STRING255,
      K6 STRING255,
      K7 SMALLINT_ID,
      K8 SMALLINT_ID,
      K9 STRING255,
      K10 numeric(14,2),
      K11 numeric(14,2),
      K12 numeric(14,2),
      K13 numeric(14,2),
      K14 numeric(14,2),
      K15 numeric(14,2),
      K16 numeric(14,2),
      K17 numeric(14,2),
      K18 numeric(14,2),
      K19 numeric(14,2),
      K20 numeric(14,2),
      K21 numeric(14,2),
      K22 numeric(14,2),
      K23 numeric(14,2),
      K24 numeric(14,2),
      K25 numeric(14,2),
      K26 numeric(14,2),
      K27 numeric(14,2),
      K28 numeric(14,2),
      K29 numeric(14,2),
      K30 numeric(14,2),
      K31 numeric(14,2),
      K32 numeric(14,2),
      K33 numeric(14,2),
      K34 numeric(14,2),
      K35 numeric(14,2),
      K36 numeric(14,2),
      K37 numeric(14,2),
      K38 numeric(14,2),
      K39 numeric(14,2),
      K40 numeric(14,2),
      K41 numeric(14,2),
      K42 numeric(14,2),
      K43 numeric(14,2),
      K44 numeric(14,2),
      K45 numeric(14,2),
      K46 numeric(14,2),
      K47 numeric(14,2),
      K48 numeric(14,2),
      K49 numeric(14,2),
      K50 numeric(14,2),
      K51 numeric(14,2),
      K52 numeric(14,2),
      K53 numeric(14,2),
      K54 numeric(14,2),
      K55 numeric(14,2),
      K56 numeric(14,2),
      K57 numeric(14,2),
      K58 numeric(14,2),
      K59 numeric(14,2),
      K60 SMALLINT_ID,
      K61 SMALLINT_ID,
      K62 SMALLINT_ID,
      K63 SMALLINT_ID,
      K64 SMALLINT_ID,
      K65 SMALLINT_ID,
      K66 SMALLINT_ID,
      K67 SMALLINT_ID,
      K68 INTEGER_ID,
      K69 STRING255,
      K70 STRING255,
      K72 STRING255,
      K73 STRING255,
      USP STRING255)
   as
declare variable VATGR varchar(6);
declare variable NETV numeric(14,2);
declare variable VATV numeric(14,2);
declare variable TAXGR varchar(10);
declare variable BKREG varchar(10);
declare variable WRONGDOC varchar(20);
declare variable BKDOCNUMBER integer;
declare variable VAT_NALEZNY varchar(255);
declare variable VAT_NALICZONY varchar(255);
declare variable SDATE date;
declare variable FDATE date;
declare variable NP numeric(14,2);
declare variable K15_3 numeric(14,2);
declare variable K15_5 numeric(14,2);
declare variable K17_7 numeric(14,2);
declare variable K17_8 numeric(14,2);
declare variable K19_22 numeric(14,2);
declare variable K19_23 numeric(14,2);
declare variable VREGTYPE smallint;
declare variable VGRTYPE smallint;
declare variable NIP STRING255;
declare variable EMONTH STRING40;
declare variable EYEAR STRING40;
declare variable PODUS STRING255;
declare variable INFO1 STRING255;
declare variable INFOREGON STRING255;
declare variable PODTEL STRING255;
declare variable INFOUSP STRING255;
begin

  -- paramert Konfiguracji
  execute procedure get_config('VAT7_NALEZNY', 0) -- naliczanie wg rejestrów
    returning_values :vat_nalezny;
  execute procedure get_config('VAT7_NALICZONY', 0) -- naliczanie staweK <> 0
    returning_values :vat_naliczony;
  execute procedure get_config('INFONIP',0)
      returning_values :NIP;

  EYEAR = substring(PERIOD from 1 for 4);
  EMONTH = substring(PERIOD from 5 for 2);

  execute procedure get_config('PODUS',0) -- urzad skarbowy
      returning_values :PODUS;
  execute procedure get_config('INFO1',0) -- nazwa pelna
      returning_values :INFO1;
  execute procedure get_config('INFOREGON',0) -- regon
      returning_values :INFOREGON;
  execute procedure get_config('PODTEL',0) -- telefon
      returning_values :PODTEL;
  execute procedure get_config('INFONUSP',0) -- kod urzedu
      returning_values :INFOUSP;

  if (PAR_K41 is null) then
    PAR_K41 = 0;
  if (PAR_K50 is null) then
    PAR_K50 = 0;
  if (PAR_K53 is null) then
    PAR_K53 = 0;
  if (PAR_K56 is null) then
    PAR_K56 = 0;
  if (PAR_K57 is null) then
    PAR_K57 = 0;
  if (PAR_K58 is null) then
    PAR_K58 = 0;
  WSP35 = 100; --domyslnie caly mozna odliczyc
  K1 = NIP;
  K4 = EMONTH;
  K5 = EYEAR;
  K6 = PODUS;
  K7 = CORRECT;
  K8 = 1;
  K9 = INFO1 || ',' || INFOREGON;
  K10 = 0;
  K11 = 0;
  K12 = 0;
  K13 = 0;
  K14 = 0;
  K15 = 0;
  K15_3 = 0;
  K15_5 = 0;
  K16 = 0;
  K17 = 0;
  K17_7 = 0;
  K17_8 = 0;
  K18 = 0;
  K19 = 0;
  K19_22 = 0;
  K19_23 = 0;
  K20 = 0;
  K21 = 0;
  K22 = 0;
  K23 = 0;
  K24 = 0;
  K25 = 0;
  K26 = 0;
  K27 = 0;
  K28 = 0;
  K29 = 0;
  K30 = 0;
  K31 = 0;
  K32 = 0;
  K33 = 0;
  K34 = 0;
  K35 = 0;
  K36 = 0;
  K37 = PAR_K37;
  K38 = 0;
  K39 = 0;
  K40 = 0;
  K41 = PAR_K41;
  K42 = 0;
  K43 = 0;
  K44 = 0;
  K45 = 0;
  K46 = par_k46;
  K47 = 0;
  K48 = 0;
  K49 = 0;
  K50 = par_k50;
  K51 = 0;
  K52 = 0;
  K53 = PAR_K53;
  K54 = 0;
  K55 = 0;
  K56 = PAR_K56;
  K57 = PAR_K57;
  K58 = PAR_K58;
  K59 = 0;
  K60 = PAR_K60;
  K61 = PAR_K61;
  K62 = PAR_K62;
  K63 = PAR_K63;
  K64 = PAR_K64;
  K65 = PAR_K65;
  K66 = PAR_K66;
  K67 = PAR_K67;
  K68 = PAR_K68;
  K69 = userfname;
  K70 = usersname;
  K72 = PODTEL;
  K73 = DOCDATE;
  USP = infousp;



  select cast(B.sdate as date), cast(B.fdate as date)
    from bKperiods B
      where B.id = :period and B.company = :company
  into :sdate, :fdate;

/* 
  SPRZEDAŻ KRAJOWA WG STAWEK (p. 10 - 20)
  cała sprzedaż krajowa z wyj. grupy C41
  vatregs.vtype = 0 and bkvatpos.taxgr <> 'C41'
*/
  for select bKvatpos.vatgr, sum(bKvatpos.netv), sum(bKvatpos.vatv)
      from bKdocs
      join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
        and vatregs.vtype = 0 and bKdocs.status > 0 and bKdocs.company= vatregs.company)
      join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
      join grvat on grvat.symbol = bkvatpos.taxgr   --<<PL:PR73894>>
    where bKdocs.company = :company
    and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1)) --<<PL:PR73894>>
    and bkvatpos.taxgr <> 'C41' and bkvatpos.taxgr <> 'C36' and bkvatpos.taxgr <> 'EXP'
    group by bKvatpos.vatgr, bKvatpos.taxgr
    into :vatgr, :netv, :vatv
  do begin
    if (vatgr = '22') then
    begin
      K19 = K19 + netv;
      K19_22 = K19_22 + netv;
      K20 = K20 + vatv;
    end
    else if (vatgr = '23') then
    begin
      K19 = K19 + netv;
      K19_23 = K19_23 + netv;
      K20 = K20 + vatv;
    end
    else if (vatgr = '07') then
    begin
      K17 = K17 + netv;
      K17_7 = K17_7 + netv;
      K18 = K18 + vatv;
    end
    else if (vatgr = '08') then
    begin
      K17 = K17 + netv;
      K17_8 = K17_8 + netv;
      K18 = K18 + vatv;
    end
    else if (vatgr = '03') then
    begin
      K15 = K15 + netv;
      K15_3 = k15_3 + netv;
      K16 = K16 + vatv;
    end
    else if (vatgr = '05') then
    begin
      K15 = K15 + netv;
      K15_5 = k15_5 + netv;
      K16 = K16 + vatv;
    end else if (vatgr = 'ZW') then
    begin
      K10 = K10 + netv;
    end
    else if (vatgr = '00') then
    begin
      K13 = K13 + netv;
      if (taxgr = 'P23') then
      begin
        K14 = K14 + netv;
      end
    end
    --else
      --exception test_breaK 'Nieznana stawKa podatKowa ' || vatgr;
  end

/*
  EKSPORT TOWARÓW (p.35)
  cały eksport z wyj. grup C21 i C22(11)
  vatregs.vtype = 2 and bKvatpos.taxgr <> 'C21' and bKvatpos.taxgr <> 'C22(11)'
*/
  K22 = 0;
  select sum(bKvatpos.netv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg
      and bKdocs.status > 0 and bKdocs.company= vatregs.company and bKdocs.vatperiod = :period)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on (grvat.symbol = bkvatpos.taxgr)--<<PL:PR73894>>
    where ((vatregs.vtype = 2 and ((bKvatpos.taxgr <> 'C21' and bKvatpos.taxgr <> 'C22(11)') or bKvatpos.taxgr is null))
      or (bKvatpos.taxgr = 'EXP'))
      and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1))--<<PL:PR73894>>
      and bKdocs.company = :company and bKdocs.vatperiod = :period
    into :K22;

/*
  DOSTAWA TOWARÓW ORAZ ŚWIADCZENIE USŁUG POZA TERYTORIUM KRAJU (p. 21)
  tylko eksport z grupą C21 lub C22(11)
  vatregs.vtype = 2 and (bKvatpos.taxgr = 'C21' or bKvatpos.taxgr = 'C22(11)')
*/
  select sum(bKvatpos.netv)
    from bKdocs
    join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
      and vatregs.vtype = 2 and bKdocs.status > 0 and bKdocs.company= vatregs.company)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on (grvat.symbol = bkvatpos.taxgr)--<<PL:PR73894>>
    where (bKvatpos.taxgr = 'C21' or bKvatpos.taxgr = 'C22(11)') and bKdocs.company = :company
    and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1))--<<PL:PR73894>>
    into :K11;

/*
  DOSTAWA TOWARÓW ORAZ ŚWIADCZENIE USŁUG POZA TERYTORIUM KRAJU art. 100 (p. 22)
  tylko eksport z grupą C22(11)
  vatregs.vtype = 2 and bKvatpos.taxgr = 'C22(11)'
*/
  select sum(bKvatpos.netv)
    from bKdocs
    join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
      and vatregs.vtype = 2 and bKdocs.status > 0 and bKdocs.company= vatregs.company)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on (grvat.symbol = bkvatpos.taxgr)--<<PL:PR73894>>
    where bKvatpos.taxgr = 'C22(11)' and bKdocs.company = :company
    and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1))--<<PL:PR73894>>
    into :K12;

/*
  DOSTAWA TOWARÓW ORAZ ŚWIADCZENIE USŁUG POZA TERYTORIUM KRAJU (p. 21)
  tylko WDT ze stawką NP
  vatregs.vtype = 1 and bKvatpos.vatgr = 'NP'  
*/
  select sum(bKvatpos.netv)
    from bKdocs
    join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
      and vatregs.vtype = 1 and bKdocs.status > 0 and bKdocs.company= vatregs.company)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on (grvat.symbol = bkvatpos.taxgr)--<<PL:PR73894>>
    where bKvatpos.vatgr = 'NP' and bKdocs.company = :company
    and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1))--<<PL:PR73894>>
    into :np;

  K11 = coalesce(K11, 0) + coalesce(np, 0);

/*
  WEWNĄTRZWSPÓLNOTOWA DOSTAWA TOWARÓW (p. 38)
  cała WDT z wyj. stawki NP
  vatregs.vtype = 1 and bKvatpos.vatgr <> 'NP'
*/
  select sum(bKvatpos.netv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg
      and vatregs.vtype = 1 and bKdocs.status > 0 and bKdocs.company= vatregs.company and bKdocs.vatperiod = :period)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on (grvat.symbol = bkvatpos.taxgr)--<<PL:PR73894>>
    where bKdocs.company = :company and bKvatpos.vatgr <> 'NP'
    and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1))--<<PL:PR73894>>
  into :K21;

/*
  WEWNĄTRZWSPÓLNOTOWE NABYCIE TOWARU (p. 34, 35)
  całe WNT z wyj. grup C36, C41 i C42(11)
  vatregs.vtype = 5 and (bKvatpos.taxgr <> 'C36' and bKvatpos.taxgr <> 'C42(11)' and bKvatpos.taxgr <> 'C41')
*/
    select sum(bKvatpos.netv),
      sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
      and vatregs.vtype = 5 and bKdocs.status > 0 and bKdocs.company= vatregs.company)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on (grvat.symbol = bkvatpos.taxgr)--<<PL:PR73894>>
    where ((bKvatpos.taxgr <> 'C36' and bKvatpos.taxgr <> 'C42(11)' and bKvatpos.taxgr <> 'C41') or bKvatpos.taxgr is null)
      and bKdocs.company = :company
      and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1))--<<PL:PR73894>>
    into :K23, :K24;

/*
  DOSTAWA TOWARÓW ORAZ ŚWIADCZENIE USŁUG, DLA KTÓRYCH PODATNIKIEM JEST NABYWCA (p. 41, 42)
  wszystkie dokumenty z grupą C36 lub C41 lub C42(11)
  bKvatpos.taxgr = 'C36' or bKvatpos.taxgr = 'C42(11)' or bKvatpos.taxgr = 'C41'
*/
/*  select sum(bKvatpos.netv),
      sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
      and bKdocs.status > 0 and bKdocs.company= vatregs.company)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    where (bKvatpos.taxgr = 'C36' or bKvatpos.taxgr = 'C42(11)' or bKvatpos.taxgr = 'C41')
      and bKdocs.company = :company
    into :K31, :K32;
*/

/*
  KWOTA PODATKU NALEŻNEGO OD TOWARÓW I USŁUG OBJĘTYCH SPISEM Z NATURY (p. 44)
  sprzedaż krajowa z grupą P40 lub C43(11)
  vatregs.vtype = 0 and (bKvatpos.taxgr = 'P40' or bKvatpos.taxgr = 'C43(11)')
*/
  select sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
     and vatregs.vtype = 0 and bKdocs.status > 0 and bKdocs.company= vatregs.company)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on (grvat.symbol = bkvatpos.taxgr)--<<PL:PR73894>>
    where (bKvatpos.taxgr = 'P40' or bKvatpos.taxgr = 'C43(11)' or bKvatpos.taxgr is null)
      and bKdocs.company = :company
      and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1))--<<PL:PR73894>>
    into :K36;

/* 
  KWOTA PODATKU NALICZONEGO WYNIKAJĄCEGO ZE SPISU Z NATURY (p. 49)
  zakupy krajowe z grupą P45 lub D48(11)
  vatregs.vtype = 3 and (bKvatpos.taxgr = 'P45' or bKvatpos.taxgr = 'D48(11)')
*/
 /* select sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
      and vatregs.vtype = 3 and bKdocs.status > 0 and bKdocs.company= vatregs.company)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    where (bKvatpos.taxgr = 'P45' or bKvatpos.taxgr = 'D48(11)' or bKvatpos.taxgr is null)
      and bKdocs.company = :company
    into :K38;*/

/*
  WNT - NABYCIE ŚRODKÓW TRANSPORTU (p. 45)
  tylko WNT z grupą P39 lub C44(11)
  vatregs.vtype = 5 and (bKvatpos.taxgr = 'P39' or bKvatpos.taxgr = 'C44(11)')
*/
    select sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
      and vatregs.vtype = 5 and bKdocs.status > 0 and bKdocs.company= vatregs.company)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on (grvat.symbol = bkvatpos.taxgr)--<<PL:PR73894>>
    where (bKvatpos.taxgr = 'P39' or bKvatpos.taxgr = 'C44(11)' or bKvatpos.taxgr is null)
      and bKdocs.company = :company
      and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1))--<<PL:PR73894>>
    into :K38;

/*
  IMPORT USŁUG (p. 38, 39)
  dokumenty z rejestru VAT typu Import usług
  vatregs.vtype = 6
*/
    select sum(bKvatpos.netv),
      sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
      and vatregs.vtype = 6 and bKdocs.status > 0 and bKdocs.company= vatregs.company)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on (grvat.symbol = bkvatpos.taxgr)--<<PL:PR73894>>
    where bKdocs.company = :company
    and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1))--<<PL:PR73894>>
    into :K27, :K28;

/*
  IMPORT USŁUG art. 28b (p. 40, 41)
  dokumenty z rejestru VAT typu Import usług z grupą C39(11)
  vatregs.vtype = 6 and bKvatpos.taxgr = 'C39(11)'
*/
    select sum(bKvatpos.netv),
      sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
      and vatregs.vtype = 6 and bKdocs.status > 0 and bKdocs.company= vatregs.company)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on (grvat.symbol = bkvatpos.taxgr)--<<PL:PR73894>>
    where bKdocs.company = :company and bKvatpos.taxgr = 'C39(11)'
    and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1))--<<PL:PR73894>>
    into :K29, :K30;

/* Dostawa towarów, dla której podatnikiem jest nabywca (sprzedaż)
  wszystkie dokumenty z grupą C41 lub każda inna grupa zdefiniowana u klienta, która to oznacza
*/

    select sum(bKvatpos.netv)
    from bKdocs
    join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
      and vatregs.vtype = 0 and bKdocs.status > 0 and bKdocs.company= vatregs.company)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on (grvat.symbol = bkvatpos.taxgr)--<<PL:PR73894>>
    where bKdocs.company = :company
    and (bKvatpos.taxgr = 'C41')
      and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1))--<<PL:PR73894>>
    into :K31;

/* Dostawa towarów, dla której podatnikiem jest nabywca (zakup)
  wszystkie dokumenty z grupą C36 i oznaczajacymi to samo jak C42 i C42(11) lub każda inna grupa zdefiniowana u klienta, która to oznacza
*/
    select sum(bKvatpos.netv), sum(bKvatpos.vate)
    from bKdocs
    join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
      and vatregs.vtype = 3 and bKdocs.status > 0 and bKdocs.company= vatregs.company)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on (grvat.symbol = bkvatpos.taxgr)--<<PL:PR73894>>
    where bKdocs.company = :company
    and (bKvatpos.taxgr = 'C36' or bKvatpos.taxgr = 'C42' or bKvatpos.taxgr = 'C42(11)')
    and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1))--<<PL:PR73894>>
    into :K34, :K35;


  if (period <= '200512') then
  begin
    K10 = cast(K10 * 100 as integer) / 100;
    K11 = cast(K11 * 100 as integer) / 100;
    K12 = cast(K12 * 100 as integer) / 100;
    K13 = cast(K13 * 100 as integer) / 100;
    K14 = cast(K14 * 100 as integer) / 100;
    K15 = cast(K15 * 100 as integer) / 100;
    K16 = cast(K16 * 100 as integer) / 100;
    K17 = cast(K17 * 100 as integer) / 100;
    K18 = cast(K18 * 100 as integer) / 100;
    K19 = cast(K19 * 100 as integer) / 100;
    K20 = cast(K20 * 100 as integer) / 100;
    K21 = cast(K21 * 100 as integer) / 100;
    K22 = cast(K22 * 100 as integer) / 100;
    K23 = cast(K23 * 100 as integer) / 100;
    K24 = cast(K24 * 100 as integer) / 100;
    K25 = cast(K25 * 100 as integer) / 100;
    K26 = cast(K26 * 100 as integer) / 100;
    K27 = cast(K27 * 100 as integer) / 100;
    K28 = cast(K28 * 100 as integer) / 100;
    K29 = cast(K29 * 100 as integer) / 100;
    K31 = cast(K31 * 100 as integer) / 100;
    K34 = cast(K34 * 100 as integer) / 100;
  end else
  begin
    K10 = round(K10);
    K11 = round(K11);
    K12 = round(K12);
    K13 = round(K13);
    K14 = round(K14);
    K15 = round(K15);
    if (vat_nalezny = '1') then
    begin
      K16 = round(0.03 * K15_3) + round(0.05 * K15_5);
      K18 = round(0.07 * K17_7) + round(0.08 * K17_8);
      K20 = round(0.22 * K19_22) + round(0.23 * K19_23);
    end else
    begin
      K16 = round(K16);
      K18 = round(K18);
      K20 = round(K20);
    end
    K17 = round(K17);
    K19 = round(K19);
    K21 = round(K21);
    K22 = round(K22);
    K23 = round(K23);
    K24 = round(K24);
    K25 = round(K25);
    K26 = round(K26);
    K27 = round(K27);
    K28 = round(K28);
    K29 = round(K29);
    K30 = round(K30);
    K31 = round(K31);
    K34 = round(K34);
    K35 = round(K35);
  end

  -- zeby liczyl odpowiednio
  if (K10 is null) then K10 = 0;
  if (K11 is null) then K11 = 0;
  if (K12 is null) then K12 = 0;
  if (K13 is null) then K13 = 0;
  if (K14 is null) then K14 = 0;
  if (K15 is null) then K15 = 0;
  if (K16 is null) then K16 = 0;
  if (K17 is null) then K17 = 0;
  if (K18 is null) then K18 = 0;
  if (K19 is null) then K19 = 0;
  if (K20 is null) then K20 = 0;
  if (K21 is null) then K21 = 0;
  if (K22 is null) then K22 = 0;
  if (K23 is null) then K23 = 0;
  if (K24 is null) then K24 = 0;
  if (K25 is null) then K25 = 0;
  if (K26 is null) then K26 = 0;
  if (K27 is null) then K27 = 0;
  if (K28 is null) then K28 = 0;
  if (K29 is null) then K29 = 0;
  if (K30 is null) then K30 = 0;
  if (K31 is null) then K31 = 0;
  if (K32 is null) then K32 = 0;
  if (K33 is null) then K33 = 0;
  if (K34 is null) then K34 = 0;
  if (K35 is null) then K35 = 0;
  if (K36 is null) then K36 = 0;
  if (K37 is null) then K37 = 0;
  if (K38 is null) then K38 = 0;
  if (K39 is null) then K39 = 0;
  if (K40 is null) then K40 = 0;
  if (K41 is null) then K41 = 0;

  K39 = K10 + K11 + K13 + K15 + K17 + K19 + K21 + K22 + K23 + K25 + K27 + K29 + K31 + K32 + K34;
  K40 = K16 + K18 + K20 + K24 + K26 + K28 + K30 + K33 + K35 + K36 + K37 - K38;
  -- zaKupy z tego miesiaca
  for
    select  bKvatpos.taxgr, sum(bKvatpos.netv), sum(bKvatpos.vatv), vatregs.vatregtype, grvat.vatregtype --<<PL:PR73894>>
      from bKdocs
        join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
          and vatregs.vtype > 2 and bKdocs.status > 0 and bKdocs.company= vatregs.company)
        join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref and bKvatpos.debited = 1)
        join grvat on (grvat.symbol = bkvatpos.taxgr)--<<PL:PR73894>>
      where bKdocs.company = :company and bKvatpos.taxgr<>'P45'
      and (vatregs.vatregtype = 1 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 0))--<<PL:PR73894>>
      group by bKvatpos.taxgr, vatregs.vatregtype, grvat.vatregtype  --<<PL:PR73894>>
      into :taxgr, :netv, :vatv, vregtype, vgrtype
  do begin
    if (taxgr in ('I11','I21','INW')) then
    begin
      if (vat_naliczony = '1') then
        if (vatv = 0) then netv = 0;
      K42 = K42 + netv;
      K43 = K43 + vatv;
    --end else if (taxgr in ('P11','P21','POZ','C36','P39','C42(11)','C39(11)')) then  --<<PL:PR73894>>
    end else if (vregtype = 1 or vregtype = 2 or vregtype = 3 and coalesce(vgrtype,0) <> 0) then
    begin
      if (vat_naliczony <> '' and vat_naliczony = '1') then
        if (vatv = 0) then netv = 0;
      K44 = K44 + netv;
      K45 = K45 + vatv;
    end
    else begin
      select first 1 bKdocs.symbol, bKdocs.bKreg, bKdocs.number
        from bKdocs
          join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
            and vatregs.vtype > 2 and bKdocs.status > 0 and bKdocs.company= vatregs.company)
          join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref and bKvatpos.debited = 1)
        where bKdocs.company = :company and (bKvatpos.taxgr = :taxgr or bKvatpos.taxgr is null)
        into :wrongdoc, :bKreg, :bKdocnumber;

--      exception universal 'Wystąpiła grupa podatKowa nieuwzględniona w deKlaracji!';
      exception universal 'Błędna gr. podatK. na doKumen. ' || :wrongdoc || ' ('|| :bKreg ||','|| :bKdocnumber ||')';
      -- exception fK_vat7_unKnown_taxgr;
    end
  end

 -- zsumowanie calego do odliczenia
  if (K43 is null) then
    K43 = 0;
  if (K44 is null) then
    K44 = 0;
  if (period <= '200512') then
  begin       
    K38 = cast(K38 * 100 as integer) / 100;
    K42 = cast(K42 * 100 as integer) / 100;
    K41 = cast(K41 * 100 as integer) / 100;
    K42 = cast(K42 * 100 as integer) / 100;
    K43 = cast(K43 * 100 as integer) / 100;
    K44 = cast(K44 * 100 as integer) / 100;
    K45 = cast(K45 * 100 as integer) / 100;
  end else
  begin
    K38 = round(K38);
    
    
    K42 = round(K42);
    K43 = round(K43);
    K44 = round(K44);
    K45 = round(K45);
  end
--<<BS69296 JK 19052015
--  K43 = (K40) * ((WSP38 - 100) / 100); --wyliczenie KoreKt
-->>
  K47 = (K45) * ((WSP35 - 100) / 100);

  if (period <= '200512') then
  begin
    K46 = cast(K46 * 100 as integer) / 100;
    K47 = cast(K47 * 100 as integer) / 100;
  end else
  begin
    K46 = round(K46);
    K47 = round(K47);
  end

  K49 = K41 + K43 + K45 + K46 + K47 + K48; -- caly odliczony

  if (K40 > K49) then --WARUNEK SPRAWDZIC!!!
    K52 = K40 - K49 - K50 - K51;
  else
    K52 = 0;

  if (K49 > K40) then
    K54 = K49 - K40 + K53;
  else
    K54 = 0;

  K55 = K56 + K57 + K58;
  K59 = K54 - K55;

  /*$$IBEC$$ if (K10 = 0) then K10 = 0;
  if (K11 = 0) then K11 = 0;
  if (K12 = 0) then K12 = 0;
  if (K13 = 0) then K13 = 0;
  if (K14 = 0) then K14 = 0;
  if (K15 = 0) then K15 = 0;
  if (K16 = 0) then K16 = 0;
  if (K17 = 0) then K17 = 0;
  if (K18 = 0) then K18 = 0;
  if (K19 = 0) then K19 = 0;
  if (K20 = 0) then K20 = 0;
  if (K21 = 0) then K21 = 0;
  if (K22 = 0) then K22 = 0;
  if (K23 = 0) then K23 = 0;
  if (K24 = 0) then K24 = 0;
  if (K25 = 0) then K25 = 0;
  if (K26 = 0) then K26 = 0;
  if (K27 = 0) then K27 = 0;
  if (K28 = 0) then K28 = 0;
  if (K29 = 0) then K29 = 0;
  if (K30 = 0) then K30 = 0;
  if (K31 = 0) then K31 = 0;
  if (K32 = 0) then K32 = 0;
  if (K33 = 0) then K33 = 0;
  if (K34 = 0) then K34 = 0;
  if (K35 = 0) then K35 = 0;
  if (K36 = 0) then K36 = 0;
  if (K37 = 0) then K37 = 0;
  if (K38 = 0) then K38 = 0;
  if (K39 = 0) then K39 = 0;
  if (K40 = 0) then K40 = 0;
  if (K41 = 0) then K41 = 0;
  if (K42 = 0) then K42 = 0;
  if (K43 = 0) then K43 = 0;
  if (K44 = 0) then K44 = 0;
  if (K45 = 0) then K45 = 0;
  if (K46 = 0) then K46 = 0;
  if (K47 = 0) then K47 = 0;
  if (K48 = 0) then K48 = 0;
  if (K49 = 0) then K49 = 0;
  if (K50 = 0) then K50 = 0;
  if (K51 = 0) then K51 = 0;
  if (K52 = 0) then K52 = 0;
  if (K53 = 0) then K53 = 0;
  if (K54 = 0) then K54 = 0;
  if (K55 = 0) then K55 = 0;
  if (K56 = 0) then K56 = 0;
  if (K57 = 0) then K57 = 0;
  if (K58 = 0) then K58 = 0;
  if (K59 = 0) then K59 = 0; $$IBEC$$*/

  suspend;
end^
SET TERM ; ^
