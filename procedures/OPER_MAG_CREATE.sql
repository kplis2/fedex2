--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_MAG_CREATE(
      ZAM integer,
      OPER char(3) CHARACTER SET UTF8                           ,
      HISTZAM integer,
      RODZAJ smallint,
      BLOCKAKCEPT smallint,
      DATA timestamp)
  returns (
      STATUS integer,
      DOKUMENTY varchar(255) CHARACTER SET UTF8                           ,
      ERROR varchar(100) CHARACTER SET UTF8                           )
   as
declare variable MAGAZYN char(3);
declare variable MAG2 char(3);
declare variable TYP char(3);
declare variable AKCEPT smallint;
declare variable BLOKADA smallint;
declare variable RETDOKUM varchar(30);
declare variable RETERROR varchar(30);
declare variable WYDANIA smallint;
declare variable COPYSER smallint;
declare variable FORNAG smallint;
declare variable REALZAM smallint;
declare variable OPKTRYB smallint;
declare variable COPYWYS smallint;
declare variable ZNAGZAMALL smallint;
declare variable TONAGFAK smallint;
declare variable FILTR varchar(1024);
declare variable OPERMAGAZYN varchar(3);
declare variable OPERMAGAZYN2 varchar(3);
declare variable ZNAGZAMALLPROC varchar(40);
declare variable MAGNOJOIN smallint;
begin

  status = -1;
  dokumenty = '';
  error = '';
  select NAGZAM.MAGAZYN,NAGZAM.MAG2,TYPZAM.WYDANIA
  from NAGZAM
  left join TYPZAM on (NAGZAM.TYPZAM=TYPZAM.SYMBOL)
  where NAGZAM.REF = :zam into :magazyn,:mag2,:wydania;
  for select magazyn, mag2, typ, akcept, blokada, COPYSER, znagzam, wartreal, opktryb, wysylkadod, ZNAGZAMALL, GETfaktura, filtr, znagzamallprocedura, magnojoin
  from DEFOPERMAG where operacja = :oper and RODZAJ=:rodzaj
  order by numer
  into :opermagazyn, :opermagazyn2, :typ, :akcept, :blokada, :copyser,:fornag,:realzam,:opktryb, :copywys, :znagzamall, :tonagfak ,:filtr, :ZNAGZAMALLPROC, :magnojoin
  do begin
    if(:blockakcept > 0) then akcept = 0;
    if(:blockakcept > 0) then blokada = 0;
    if(:magazyn is null) then magazyn = '';
    if(:opermagazyn is null) then opermagazyn = '';
    if(:opermagazyn2 is null) then opermagazyn2 = '';
    if((:mag2 is null) or (:mag2=:magazyn)) then mag2 = '';
    if(:magnojoin is null) then magnojoin = 0;
    if(:fornag = 1) then begin
       select NAGZAM.MAGAZYN,NAGZAM.MAG2
         from NAGZAM
         where NAGZAM.REF = :zam
         into :magazyn, :mag2;
       if(:mag2 <> :magazyn and mag2 <> '') then magazyn = :mag2;/*zamiana magazynó dla zlecen montazu, dla dokumentu na naglowku*/
       execute procedure OPER_MAG_CREATE_DOKUM(:zam, :magazyn, :mag2,:typ, :akcept, :blokada, :histzam,0,:wydania, :copyser,1,:realzam,:opktryb, :copywys, :znagzamall, :tonagfak , :filtr, :data, :rodzaj, :ZNAGZAMALLPROC, :magnojoin)
            returning_values :retdokum, :reterror;
       if(:reterror <> '')then begin
          error = reterror;
           status = -1;
           exit;
       end
       if(:dokumenty <> '' AND :retdokum <> '' and coalesce(char_length(dokumenty),0)<254) then -- [DG] XXX ZG119346
         dokumenty = dokumenty || ';';
       if(coalesce(char_length(dokumenty||retdokum),0)<255) then dokumenty = dokumenty || retdokum; -- [DG] XXX ZG119346
    end else
    if(:opermagazyn<>'')then begin
        reterror = '';
        retdokum = '';
        execute procedure OPER_MAG_CREATE_DOKUM(:zam, :opermagazyn, :opermagazyn2,:typ, :akcept, :blokada, :histzam,1,:wydania, :copyser,0,:realzam,:opktryb, :copywys, :znagzamall, :tonagfak , :filtr, :data, :rodzaj, :ZNAGZAMALLPROC, :magnojoin)
          returning_values :retdokum, :reterror;
        if(:reterror <> '')then begin
           error = reterror;
           status = -1;
           exit;
        end
        if(:dokumenty <> '' AND :retdokum <> '' and coalesce(char_length(dokumenty),0)<254) then -- [DG] XXX ZG119346
          dokumenty = dokumenty || ';';
        if(coalesce(char_length(dokumenty||retdokum),0)<255) then dokumenty = dokumenty || retdokum; -- [DG] XXX ZG119346
    end else begin
      -- petla po wszystkich parach magazynow na POZZAM, przy czym MAG2=NULL traktujemy jako MAG2=MAGAZYN
      for select distinct magazyn,coalesce(mag2,magazyn) from POZZAM where zamowienie = :zam into :magazyn,:mag2
      do begin
        if(:magazyn is null or (:magazyn = '')) then begin
          error='Brak okreslonego magazynu na pozycjach';
          status = -1;
          exit;
        end
        reterror = '';
        retdokum = '';
        execute procedure OPER_MAG_CREATE_DOKUM(:zam, :magazyn, :mag2,:typ, :akcept, :blokada, :histzam,0,:wydania, :copyser,0,:realzam,:opktryb, :copywys, :znagzamall, :tonagfak , :filtr, :data, :rodzaj, :ZNAGZAMALLPROC, :magnojoin)
          returning_values :retdokum, :reterror;
        if(:reterror <> '')then begin
           error = reterror;
           status = -1;
           exit;
        end
        if(:dokumenty <> '' AND :retdokum <> '' and coalesce(char_length(dokumenty),0)<254) then -- [DG] XXX ZG119346
          dokumenty = dokumenty || ';';
        if(coalesce(char_length(dokumenty||retdokum),0)<255) then dokumenty = dokumenty || retdokum; -- [DG] XXX ZG119346
      end
    end
  end
  status = 1;
end^
SET TERM ; ^
