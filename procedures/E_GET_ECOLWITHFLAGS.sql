--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_ECOLWITHFLAGS(
      FLAGS varchar(255) CHARACTER SET UTF8                           ,
      MODE smallint = 1,
      NOTPARSED smallint = 0)
  returns (
      ECOLS varchar(255) CHARACTER SET UTF8                           )
   as
declare variable ecol integer;
declare variable flag varchar(5);
declare variable proc varchar(511);
declare variable filtr varchar(255);
begin
/* Procedura zwracająca ECOLUMN posiadajaca konkretna flage
  Parametry:
  flags - string w formacie ';FLG;FLG;FLG;'
  mode - 0: zawiera wszystkie flagi,
         1: zawiera jedna z flag
  notparsed - zwraca wyniki w postaci selecta*/
  flags = replace(flags,'"','');
  ecols = '';
  ecol = 0;
  proc = 'select number from ecolumns where ';
  filtr = '';
  for
    select stringout
    from parsestring(:flags, ';')
    into flag
  do begin
    if(mode = 0) then
    begin
      if(filtr = '') then
        filtr = 'cflags containing '''||:flag||'''';
      else
        filtr = filtr||' and cflags containing '''||:flag||'''';
    end
    else if(mode = 1) then
    begin
      if(filtr = '') then
        filtr = 'cflags containing '''||:flag||'''';
      else
        filtr = filtr||' or cflags containing '''||:flag||'''';
    end
  end
  proc = proc||filtr;
  for
   execute statement proc
   into ecol
  do begin
    if(notparsed = 0) then
    begin
      if(ecols not containing ecol||',') then
        ecols = ecols || ecol || ',';
    end
    else
    begin
      ecols = ecol;
      suspend;
    end
  end
  if(notparsed = 0) then
  begin
    ecols = trim(',' from ecols);
    ecols = '('||ecols||')';
    suspend;
  end
end^
SET TERM ; ^
