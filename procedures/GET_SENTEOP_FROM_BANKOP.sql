--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_SENTEOP_FROM_BANKOP(
      BANKOPERATIONCODE varchar(4) CHARACTER SET UTF8                           ,
      BANKACCTYP varchar(10) CHARACTER SET UTF8                           ,
      SIDE varchar(1) CHARACTER SET UTF8                           )
  returns (
      RKDOKOPERATIONCODE varchar(25) CHARACTER SET UTF8                           ,
      RKDOKTRANSACTION integer)
   as
begin
  if (bankacctyp = 'MULTICASH') then begin  ----------MULTICASH-------------
   if (bankoperationcode is null) then
       rkdokoperationcode = null;
   else if (bankoperationcode = 'S081') then
     if (side = '+') then begin
       rkdokoperationcode = '+NALEŻNO';
     end else begin
       rkdokoperationcode = '-ZOBOWIA';
     end
   else if (bankoperationcode = 'S020') then
     if (side = '+') then begin
       rkdokoperationcode = '+NALEŻNO';
     end else begin
       rkdokoperationcode = '-ZOBOWIA';
     end
   else if (bankoperationcode = 'S034') then
     if (side = '+') then begin
       rkdokoperationcode = '+NALEŻNO';
     end else begin
       rkdokoperationcode = '-ZOBOWIA';
     end

  end else if (bankacctyp = 'CITIDIRECT') then begin --------CITY DIRECT-------------
   if (bankoperationcode is null) then begin
     if (side = '+') then
       rkdokoperationcode = null;
   end else if (bankoperationcode = 'NMSC') then begin
     if (side = '+') then begin
       rkdokoperationcode = '+NALEŻNO';
     end else begin
       rkdokoperationcode = '-ZOBOWIA';
     end
   end else if (bankoperationcode = 'NTRF') then  begin
     if (side = '+') then begin
       rkdokoperationcode = '+NALEŻNO';
     end else begin
       rkdokoperationcode = '-ZOBOWIA';
     end
   end
  end else if (bankacctyp = 'CICOMP') then begin --------BPS-------------
    if (bankoperationcode is null) then
      if (side = '+') then
        rkdokoperationcode = null;
      else if (bankoperationcode = 'FMSC') then
        if (side = '+') then begin
          rkdokoperationcode = '+NALEŻNO';
        end else begin
          rkdokoperationcode = '-ZOBOWIA';
        end
  end
  select min(ref) from rkpozoper
    where rkpozoper.operacja = :rkdokoperationcode
    into :rkdoktransaction;
  if (rkdoktransaction is null) then rkdoktransaction = 0;
  suspend;
end^
SET TERM ; ^
