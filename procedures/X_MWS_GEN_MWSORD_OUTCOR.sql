--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_GEN_MWSORD_OUTCOR(
      DOCID integer,
      DOCGROUP integer,
      DOCPOSID integer,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      MWSORD integer,
      MWSACT integer,
      RECALC smallint,
      MWSORDTYPE integer)
  returns (
      REFMWSORD integer)
   as
declare variable rec smallint;
declare variable wh varchar(3);
declare variable stocktaking smallint;
declare variable operator integer;
declare variable priority smallint;
declare variable branch varchar(10);
declare variable period varchar(6);
declare variable flags varchar(40);
declare variable symbol varchar(20);
declare variable cor smallint;
declare variable quantity numeric(14,4);
declare variable todisp numeric(14,4);
declare variable toinsert numeric(14,4);
declare variable good varchar(40);
declare variable vers integer;
declare variable opersettlmode smallint;
declare variable mwsconstlocp integer;
declare variable mwspallocp integer;
declare variable mwsconstlocl integer;
declare variable mwsconstlocltmp integer;
declare variable autoloccreate smallint;
declare variable mwspallocl integer;
declare variable mwspallocltmp integer;
declare variable whsec integer;
declare variable wharea integer;
declare variable posflags varchar(40);
declare variable description varchar(1024);
declare variable posdescription varchar(255);
declare variable lot integer;
declare variable scaler numeric(14,4);
declare variable tmpscaler numeric(14,4);
declare variable recramp integer;
declare variable connecttype smallint;
declare variable shippingarea varchar(3);
declare variable wharealogp integer;
declare variable skillslevel varchar(10);
declare variable mwsaccessory integer;
declare variable operatordict varchar(80);
declare variable difficulty varchar(10);
declare variable timestart timestamp;
declare variable maxnumber integer;
declare variable timestartdcl timestamp;
declare variable timestopdcl timestamp;
declare variable realtime double precision;
declare variable wharealogb integer;
declare variable whquantity numeric(14,4);
declare variable daysel numeric(14,4);
declare variable wharealogl integer;
declare variable dist numeric(14,4);
declare variable refmwsactpal integer;
declare variable refmwsact integer;
declare variable paltype varchar(20);
declare variable tmppaltype varchar(20);
declare variable ispal smallint;
declare variable maxemptpalq integer;
declare variable palquantity numeric(14,4);
declare variable tmpispal smallint;
declare variable palactsquantity numeric(14,4);
declare variable status smallint;
declare variable whsecout integer;
declare variable paltypevers integer;
declare variable planwharealogl integer;
declare variable planmwsconstlocl integer;
declare variable planwhareal integer;
declare variable palpackmethshort varchar(30);
declare variable palpackmeth varchar(40);
declare variable palpackquantity numeric(14,4);
declare variable maxh numeric(14,4);
declare variable maxweight numeric(14,4);
declare variable maxscaler numeric(14,4);
declare variable docobj numeric(14,4);
declare variable maxmixedpallgoodss varchar(100);
declare variable maxmixedpallgoods integer;
declare variable cnt integer;
declare variable mixcnt integer;
declare variable keyb varchar(100);
declare variable keyc varchar(100);
declare variable keyd varchar(100);
declare variable refmwsactpalmix integer;
declare variable slodef integer;
declare variable slopoz integer;
declare variable slokod varchar(40);
declare variable maxpalobj numeric(14,4);
declare variable refill smallint;
declare variable doctypenag varchar(3);
declare variable doctypepoz varchar(3);
declare variable docidpss integer;
declare variable docidpsw integer;
declare variable docidpsp integer;
declare variable pref integer;
declare variable doksped integer;
declare variable symbolsped varchar(300);
declare variable flagi varchar(40);
declare variable cenacen numeric(14,4);
declare variable cenanet numeric(14,4);
declare variable cenabru numeric(14,4);
declare variable symbfak varchar(40);
declare variable platnik integer;
declare variable docidps integer;
declare variable refk integer;
declare variable faktura integer;
declare variable breakps smallint;
declare variable doctypeps varchar(3);
declare variable kortryb smallint;
declare variable kordoc integer;
declare variable koril numeric(14,4);
declare variable korpoz integer;
declare variable gquantity numeric(14,4);
declare variable gref integer;
declare variable goref integer;
declare variable opername varchar(60);
declare variable corquantity numeric(14,4);
declare variable ordquantiyty numeric(14,4);
declare variable mwsconstlocback integer;
declare variable mwspallocback integer;
declare variable newmwsact integer;
declare variable whareal integer;
declare variable descript varchar(255);
declare variable gstatus smallint;
declare variable actq numeric(14,4);
declare variable refillq numeric(14,4);
declare variable posquantity numeric(14,4);
declare variable mws smallint;
begin
exception universal  'DOCID '||coalesce(DOCID, 0) || 'DOCGROUP '||coalesce(DOCGROUP, 0) ||'DOCPOZID '|| coalesce(DOCPOSID, 0) ||'DOCTYPE '||coalesce(DOCTYPE, 0) ||
 'MWSORD '||coalesce(MWSORD, 0)
 ||'MWSACTS '||coalesce(MWSACT, 0) ||'RECALC '||coalesce(RECALC, 0) ||'MWSORTYPE '||coalesce(MWSORDTYPE, 0) ;
  if (recalc is null) then recalc = 0;
  -- NAGLÓWEK ZLECENIA MAGAZYNOWEGO
  select
    first 1 t.ref
      from mwsordtypes t
      where t.mwsortypedets = 2
      into mwsordtype;
  if (docgroup is null) then docgroup = docid;
  select stocktaking, opersettlmode, autoloccreate
    from mwsordtypes
    where ref = :mwsordtype
    into stocktaking, opersettlmode, autoloccreate;
  select defdokum.wydania, defdokum.koryg, dokumnag.magazyn, dokumnag.operator,
      dokumnag.uwagisped, dokumnag.katmag, dokumnag.spedpilne, dokumnag.oddzial,
      dokumnag.okres, dokumnag.flagi, dokumnag.symbol, dokumnag.kodsped,
      dokumnag.wharealogb, 6, dokumnag.ref, substring(dostawcy.id from 1 for 40),
      dokumnag.refk
    from dokumnag
      left join dostawcy on (dokumnag.dostawa = dostawcy.ref)
      left join defdokum on (defdokum.symbol = dokumnag.typ)
    where dokumnag.ref = :docid
    into rec, cor, wh, operator,
      description, difficulty, priority, branch,
      period, flags, symbol, shippingarea,
      wharealogb, slodef, slopoz, slokod,
      kordoc;
  mwsconstlocback = null;
  mwspallocback = null;
  execute procedure XK_MWS_GET_BEST_LOCATION(null,null,null,null,:MWSORDTYPE,:WH,null,null,null,null,null,null,null,null,null,null,null,null,null)
      returning_values (mwsconstlocback, mwspallocback, whareal, wharealogl, refill);
  if (rec = 0) then rec = 1; else rec = 0;
  execute procedure gen_ref('MWSORDS') returning_values refmwsord;
  select okres from datatookres(current_date,0,0,0,0) into period;
  insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator,
      priority, description, wh, difficulty, regtime, timestartdecl, timestopdecl,
      mwsaccessory, branch, period, flags, docsymbs, cor, rec, bwharea, ewharea,
      status, shippingarea, slodef, slopoz, slokod)
    values (:refmwsord, :mwsordtype, :stocktaking, 'M', :docgroup, :docid, null,
        :priority, :description, :wh, :difficulty, current_timestamp, :timestart, current_timestamp,
        :mwsaccessory, :branch, :period, :flags, :symbol, :cor, :rec, :wharealogb, :wharealogb,
        0, :shippingarea, :slodef, :slopoz, :slokod);
  if (refmwsord is null) then exception universal 'Brak zlecenia magazynowego';
  -- XXX MSt: Zmiany pod pozycje skladowe kompletow
  -- mozliwe sytuacje
  for
    select p.ref, p.iloscl - p.ilosconmwsacts, p.kortopoz,
        p.ktm, p.wersjaref, p.flagi, p.uwagi, p.dostawa
      from dokumpoz p
        join dokumnag n on(p.dokument = n.ref)
      where p.dokument = :docid
      into docposid, koril, korpoz,
        good, vers, posflags, posdescription, lot
  do begin
    -- moze nie byc ani w zleceniach ani w mwsordswaiting i trzeba odjac
    select sum(g.quantity)
      from mwsgoodsrefill g
      where g.docposid = :korpoz
      into gquantity;
    if (gquantity is null) then gquantity = 0;
    select sum(a.quantityl)
      from mwsacts a
      where a.docposid = :korpoz and a.status < 6
        and a.doctype = 'M'
      into ordquantiyty;
    if (ordquantiyty is null) then ordquantiyty = 0;
    select p.iloscl
      from dokumpoz p
      where p.ref = :korpoz
      into posquantity;
    if (posquantity is null) then posquantity = 0;
    if (posquantity > ordquantiyty + gquantity and koril > 0) then
      koril = 0;
    else if (posquantity < ordquantiyty + gquantity and koril > 0) then
      koril = (ordquantiyty + gquantity) - posquantity;
    gquantity = 0;
    -- pozycja jest niewygenerowana
    if (koril > 0) then
    begin
      for
        select g.ref, g.quantity
          from mwsgoodsrefill g
          where g.docposid = :korpoz
          into gref, gquantity
      do begin
        if (gquantity > 0) then
        begin
          if (gquantity <= koril) then
            delete from mwsgoodsrefill where ref = :gref;
          else
            update mwsgoodsrefill set quantity = quantity - :koril where ref = :gref;
        end
        koril = koril - gquantity;
        if (koril < 0) then
          koril = 0;
        if (koril = 0) then
          break;
      end
    end
    -- pozycja jest wygenerowana ale ma status < 2
    if (koril > 0) then
    begin
     for
       select a.ref, a.quantityl
         from mwsacts a
         where a.docposid = :korpoz and a.status < 2 and a.quantityl > 0
           and a.mwsordtypedest in (1,8)
and a.doctype = 'M'
         into gref, gquantity
      do begin
        if (gquantity > 0) then
        begin
          if (gquantity <= koril) then
          begin
            update mwsacts set status = 0 where ref = :gref;
            delete from mwsacts where ref = :gref;
          end
          else
          begin
            update mwsacts set status = 0 where ref = :gref;
            update mwsacts set quantity = quantity - :koril, status = 1 where ref = :gref;
          end
        end
        koril = koril - gquantity;
        if (koril < 0) then
          koril = 0;
        if (koril = 0) then
          break;
      end
    end
    -- pozycja jest szykowana
    if (koril > 0) then
    begin
     for
       select first 1 a.ref, a.quantityl, p.nazwa
         from mwsacts a
           left join mwsords o on (o.ref = a.mwsord)
           left join operator p on (p.ref = o.ref)
         where a.docposid = :korpoz and a.status = 2 and a.cortomwsact is null and a.quantityl > 0
           and a.mwsordtypedest in (1,8)
           and a.doctype = 'M'
         into gref, gquantity, opername
      do begin
        if (opername is null) then opername = '';
        exception UNIVERSAl 'Operator '||opername||' jest w trakcie szykowania pozycji';
      end
    end
    -- pozycja jest naszykowana ale zlecenie ma status 2
    if (koril > 0) then
    begin
      for
        select a.ref, a.quantityl, a.mwsord, a.status
          from mwsacts a
            left join mwsords o on (o.ref = a.mwsord)
          where a.docposid = :korpoz and a.status = 5 and a.cortomwsact is null and a.quantityl > 0 and o.status < 5
            and a.doctype = 'M'
          into gref, gquantity, goref, gstatus
      do begin
        if (gstatus = 5) then
        begin
          goref = null;
          select first 1 a.mwsord
            from mwsacts a
              left join mwsords o on (o.ref = a.mwsord)
              left join dokumpoz p on (p.kortopoz = a.docposid and p.dokument = :docid)
            where a.docid = :kordoc and a.cortomwsact is null and a.quantityl > 0
              and p.ref is null and o.status < 5 and o.status > 0
              and a.doctype = 'M'
            into goref;
        end
        if (goref is null) then
          toinsert = 0;
        else if (gquantity >= koril) then
          toinsert = koril;
        else if (gquantity < koril) then
          toinsert = gquantity;
        else
          toinsert = 0;
        if (toinsert > 0) then
        begin
          if (mwspallocback is null) then
          begin
            select first 1 ref
              from mwspallocs
              where mwsconstloc = :mwsconstlocback
              into mwspallocback;
            if (mwspallocback is null) then
            begin
              execute procedure gen_ref('MWSPALLOCS') returning_values mwspallocback;
              insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status,
                  fromdocid, fromdocposid, fromdoctype)
              select :mwspallocback, symbol, ref, 2, 0,
                null, :docposid, 'M'
              from mwsconstlocs
              where ref = :mwsconstlocback;
            end
          end
          update mwsords set status = 1 where ref = :refmwsord and status = 0;
          select max(number)
            from mwsacts
            where mwsord = :goref
            into maxnumber;
          if (maxnumber is null) then maxnumber = 0;
          maxnumber = maxnumber + 1;
          execute procedure gen_ref('MWSACTS') returning_values newmwsact;
          insert into mwsacts (ref,mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
              mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
              regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc, plus,
              whareap, whareal, wharealogp, wharealogl, number, disttogo, frommwsact, cortomwsact, stopnextmwsacts,
              docgroup)
            select :newmwsact, :refmwsord, 0, stocktaking, good, vers, :toinsert, mwsconstlocl, mwspallocl,
                :mwsconstlocback, :mwspallocback, :docid, :docposid, 'M', settlmode, closepalloc, wh, wharea, whsec,
                current_timestamp, current_timestamp, current_timestamp, realtime, flags, :descript, 0, null, 1, 1,
                null, whareal, null, wharealogl, :maxnumber, 0, null, :gref, 1,
                :docgroup
              from mwsacts where ref = :gref;
--          update mwsacts set status = 1 where ref = :newmwsact;
          koril = koril - toinsert;
        end
        if (koril <= 0) then
          break;
      end
    end
    -- pozycja naszykowana i pozostale przypadki zalatwiamy juz po staremu
    if (koril > 0) then
    begin
      for
        select a.ref, a.quantityl
          from mwsacts a
            left join mwsords o on (o.ref = a.mwsord)
          where a.docposid = :korpoz and a.status = 5 and a.cortomwsact is null and a.quantityl > 0 and a.mwsconstlocl is null
            and a.doctype = 'M'
          into gref, gquantity
      do begin
        if (gquantity >= koril) then
          toinsert = koril;
        else if (gquantity < koril) then
          toinsert = gquantity;
        else
          toinsert = 0;
        if (toinsert > 0) then
        begin
          update mwsords set status = 1 where ref = :refmwsord and status = 0;
          select max(number)
            from mwsacts
            where mwsord = :refmwsord
            into maxnumber;
          if (maxnumber is null) then maxnumber = 0;
          maxnumber = maxnumber + 1;
          execute procedure gen_ref('MWSACTS') returning_values newmwsact;
          execute procedure XK_MWS_GET_BEST_LOCATION(:good,:vers,null,null,:mwsordtype,
              :wh,null,null,null,null,null,null,null,null,null,null,null,null,null)
            returning_values(mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
          insert into mwsacts (ref,mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
              mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
              regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc, plus,
              whareap, whareal, wharealogp, wharealogl, number, disttogo, frommwsact, cortomwsact, stopnextmwsacts,
              docgroup)  -- XXX KBI ZG127707
            select :newmwsact, :refmwsord, 0, stocktaking, good, vers, :toinsert, mwsconstlocl, mwspallocl,
                null, :mwspallocl, :docid, :docposid, 'M', settlmode, closepalloc, wh, wharea, whsec,
                current_timestamp, current_timestamp, current_timestamp, realtime, flags, :descript, 0, null, 1, 1,
                null, whareal, null, wharealogl, :maxnumber, 0, null, :gref, 0,
                :docgroup -- XXX KBI ZG127707
              from mwsacts where ref = :gref;
          cnt = 0;
          select count(ref) from mwsacts where mwsord = :refmwsord
            into cnt;
--          update mwsacts set status = 1 where ref = :newmwsact;
          koril = koril - toinsert;
        end
        if (koril <= 0) then
          break;
      end
    end
  end
  if (not exists (select ref from mwsacts where mwsord = :refmwsord)) then
    delete from mwsords where ref = :refmwsord;
  suspend;
end^
SET TERM ; ^
