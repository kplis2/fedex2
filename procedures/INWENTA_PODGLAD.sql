--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INWENTA_PODGLAD(
      INWENTA integer,
      OGRANICZ smallint,
      ZEROWE smallint,
      ZGODNE smallint)
  returns (
      KTM varchar(80) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      WERSJA varchar(255) CHARACTER SET UTF8                           ,
      JEDNMIAR varchar(5) CHARACTER SET UTF8                           ,
      ILOSC numeric(14,4),
      ILOSCSTAN numeric(14,4),
      ILOSCINW numeric(14,4),
      CENA numeric(14,2),
      WARTOSC numeric(14,2),
      WARTOSCINW numeric(14,2),
      WARTOSCSTAN numeric(14,2),
      VAT varchar(5) CHARACTER SET UTF8                           ,
      STVAT numeric(14,2))
   as
DECLARE VARIABLE DATA TIMESTAMP;
DECLARE VARIABLE MAGAZYN VARCHAR(3);
DECLARE VARIABLE RWERSJA INTEGER;
DECLARE VARIABLE IL DECIMAL(14,4);
DECLARE VARIABLE ST DECIMAL(14,4);
DECLARE VARIABLE CENAINWENTAP NUMERIC(14,2);
declare variable ok smallint;
begin
  select I.DATA, I.MAGAZYN from INWENTA I where REF=:INWENTA into :data, :magazyn;
  for select
    MK0.RWERSJAREF,max(MK0.RKTM),max(MK0.RNAZWAT),max(MK0.RNAZWA),max(T.MIARA), max(T.VAT), max(V.STAWKA),
    sum(MK0.STAN),sum(MK0.WARTOSC),sum(MK0.STAN)+sum(MK0.STANINW),sum(MK0.WARTOSC)+sum(MK0.WARTOSCINW)
    from MAG_STANYK(:magazyn, :data, :inwenta) MK0
    left join TOWARY T on (MK0.RKTM = T.KTM)
    left join VAT V on (V.grupa=T.vat)
    group by MK0.RWERSJAREF
    into :rwersja, :KTM, :NAZWA, :WERSJA, :JEDNMIAR, :VAT, :STVAT,
    :ILOSCSTAN,:wartoscstan,:ILOSCINW,:wartoscinw
  do begin
    ok = 1;
    if(:ogranicz=1 and not exists(select ref from inwentap where inwenta=:inwenta and wersjaref=:rwersja)) then ok = 0;
    if(:zerowe=1 and :iloscstan=0 and :iloscinw=0 and :wartoscstan=0 and :wartoscinw=0) then ok = 0;
    if(:zgodne=1 and :iloscstan=:iloscinw) then ok = 0;
    if(ok=1) then begin
      cena = 0;
      if(:iloscstan<>0) then cena = :wartoscstan/:iloscstan;
      ilosc = :iloscinw-:iloscstan;
      wartosc = :wartoscinw-:wartoscstan;
      suspend;
    end
  end
/*  for select IP.KTM , T.NAZWA, W.NAZWA as WERSJA, T.MIARA, IP.WERSJAREF, IP.ILSTAN-IP.ILINW as ILOSC, IP.ILSTAN, IP.ILINW, IP.cena
    from INWENTAP IP
      left join TOWARY T on (IP.KTM = T.KTM)
      left join WERSJE w on (IP.WERSJAREF= W.REF)
    where (IP.INWENTA = :INWENTA)
      and (IP.ILSTAN <> IP.ILINW)
    order by IP.KTM
    into :KTM, :NAZWA, :WERSJA, :JEDNMIAR, :rwersja, :il, :ILOSCSTAN, :ILOSCINW, :CENAINWENTAP
  do begin
    select vat.stawka from towary left join vat on (towary.vat = vat.grupa)
      where towary.ktm = :ktm into :stvat;
  if (:il <> 0) then begin
      if (:ZPARTIAMI = 0) then begin
        for select SC.CENA, SC.ILOSC as STAN
          from STANYCEN SC
          where (SC.MAGAZYN = :magazyn)and(SC.KTM = :KTM)and(SC.WERSJAREF = :rwersja)and(SC.ILOSC > 0)
          order by SC.CENA desc, SC.DATAFIFO
          into :CENA, :st
        do begin
          if (:il > 0) then begin
            if (:il >= :st) then begin
              ILOSC = - :st;
              WARTOSC = (:CENA * :ILOSC);
              WARTOSCSTAN = (:CENA * :ILOSCSTAN);
              WARTOSCINW = (:CENA * :ILOSCINW);
              vat = :WARTOSC * :stvat/100;
              il = :il - :st;
            end
            else begin
              ILOSC = -:il;
              WARTOSC = (:CENA * :IlOSC);
              WARTOSCSTAN = (:CENA * :ILOSCSTAN);
              WARTOSCINW = (:CENA * :ILOSCINW);
              vat = :WARTOSC * :stvat/100;
              il = 0;
            end
            suspend;
          end
        end
      end
      else begin
        ilosc = -:il;
        WARTOSC = :CENAINWENTAP * :ILOSC;
        WARTOSCSTAN = :CENAINWENTAP * :ILOSCSTAN;
        WARTOSCINW = :CENAINWENTAP * :ILOSCINW;
        vat = :WARTOSC * :stvat/100;
        CENA = :CENAINWENTAP;
        suspend;
      end
    end
    else begin
      execute procedure INWENTAP_ILPLUS_WYCENA(:rwersja,:magazyn) returning_values :cena;
      if (:cena is null or (:cena < 0) ) then
        cena = 0;
      WARTOSC = :CENA * :ILOSC;
      WARTOSCSTAN = :CENA * :ILOSCSTAN;
      WARTOSCINW = :CENA * :ILOSCINW;
      vat = :WARTOSC * :stvat/100;
      suspend;
    end
  end*/
end^
SET TERM ; ^
