--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_BLANK_KODFK(
      COMPANY integer,
      TYP varchar(15) CHARACTER SET UTF8                           ,
      REF integer,
      SLODEF integer)
  returns (
      NUMER varchar(15) CHARACTER SET UTF8                           )
   as
declare variable dl integer;
begin
  if (typ = 'KLIENCI' or typ = 'DOSTAWCY' or typ = 'PERSONS' or typ = 'EMPLOYEES') then
    select kodln from slodef where typ = :typ into :dl;
  else if (slodef is not null) then
    select kodln from slodef where ref = :slodef into :dl;
  if (dl is null) then dl = 5;
  while(1=1) do begin
--    ref = gen_id(X_GEN_KODFK,1);
    numer = cast(ref as varchar(10));
    execute procedure get_symbol_from_number(numer, dl)
      returning_values numer;
    if (typ = 'KLIENCI') then
    begin
       if(not exists(select ref from klienci where kontofk=:numer)) then begin
         suspend;
         exit;
       end
    end else if (typ = 'DOSTAWCY') then
    begin
       if(not exists(select ref from dostawcy where kontofk=:numer)) then begin
         suspend;
         exit;
       end
    end else if(:slodef is not null) then begin
      suspend;
      exit;
    end
    ref = ref+1;
  end
end^
SET TERM ; ^
