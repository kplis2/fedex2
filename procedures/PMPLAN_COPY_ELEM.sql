--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PMPLAN_COPY_ELEM(
      PLANDOC integer,
      ELEM integer,
      COPYPOS smallint = 1)
   as
declare variable elem_ref integer;
begin
  execute procedure GEN_REF('PMELEMENTS') returning_values :elem_ref;
  insert into pmelements ( REF,NAME, PMPLAN, SYMBOL, COPYWITH)
    select  :elem_ref, NAME, :plandoc,   SYMBOL, :elem
           from pmelements where ref = :elem;

  if(:copypos=1) then begin
    insert into pmpositions (PMELEMENT, PMPLAN, NAME, SYMBOL, KTM, DESCRIPT, QUANTITY, POSITIONTYPE, UNIT,
      CALCPRICE, BUDPRICE)
    select :elem_ref, :plandoc, NAME, SYMBOL, KTM, DESCRIPT, QUANTITY, POSITIONTYPE, UNIT,
      CALCPRICE, BUDPRICE
    from pmpositions where pmelement = :elem;
  end
end^
SET TERM ; ^
