--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_SETTLEMENTS_STR_DRILL(
      PACCOUNT varchar(256) CHARACTER SET UTF8                           ,
      P1 numeric(14,2),
      P2 numeric(14,2),
      D1 timestamp,
      C smallint)
  returns (
      ACCOUNT ACCOUNT_ID,
      VAL1 numeric(14,2),
      VAL2 numeric(14,2),
      VAL3 numeric(14,2),
      VAL4 numeric(14,2),
      SUMVAL numeric(14,2),
      SUMP numeric(14,2),
      PAYDATE timestamp,
      SYMBFAK varchar(255) CHARACTER SET UTF8                           )
   as
declare variable valw numeric(14,2);
declare variable valm numeric(14,2);
declare variable val numeric(14,2);
declare variable d timestamp;
declare variable dictdef integer;
declare variable dictpos integer;
begin
    d=cast(d1 as timestamp);
  for
  select R.kontofk, sum(P.winienzl),sum(P.mazl), R.dataplat, R.slodef, R.slopoz, R.symbfak
    from rozrachp P
    join rozrach R on (R.slodef = P.slodef and R.slopoz = P.slopoz
          and R.kontofk = P.kontofk and R.symbfak = P.symbfak
          and R.company = P.company)
           where
          substring(R.kontofk from 1 for 3) = :paccount
    and P.data<=:D and R.company = :c
    group by R.kontofk , R.dataplat, R.slodef, R.slopoz, R.symbfak
    having  sum(P.winienzl)> sum(P.mazl)
    into :account, :valw,:valm ,:paydate, :dictdef, :dictpos, :symbfak
  do begin
  val1 = 0;
  val2 = 0;
  val3 = 0;
  val4= 0;
  sumval = 0;
  if (:D-paydate <=0) then
    val1 = valw - valm;
  else if (D-paydate>0 and D-paydate<=p1) then
    val2 = valw - valm;
  else if (D-paydate>p1 and D-paydate<=p2) then
    val3 = valw - valm;
  else if (D-paydate>p2) then
    val4 = valw - valm;
  sumval = valw - valm;
  sump = val2+val3+val4;
  suspend;
  end
end^
SET TERM ; ^
