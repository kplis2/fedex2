--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_ANAL_WORK_STATS(
      OPERATORIN integer,
      WH varchar(3) CHARACTER SET UTF8                           ,
      BDATE date,
      EDATE date)
  returns (
      OPERATOR integer,
      OPERATORNAME varchar(255) CHARACTER SET UTF8                           ,
      WOQUANTITY integer,
      WTQUANTITY integer,
      WGQUANTITY integer,
      ALLQUANTITY integer,
      MISTAKESCNT integer,
      MISTAKESCNTQUAN integer,
      MISTAKEONGET integer,
      MISTAKEONPACKQ integer,
      TIMESTART timestamp,
      TIMESTOP timestamp,
      WORKTIME varchar(40) CHARACTER SET UTF8                           ,
      NOWORKTIME varchar(40) CHARACTER SET UTF8                           ,
      TIMETOGETPALL varchar(40) CHARACTER SET UTF8                           ,
      AVGTIMETOGETPOS varchar(40) CHARACTER SET UTF8                           ,
      AVGVERIFYTIME varchar(40) CHARACTER SET UTF8                           ,
      BRAKETIME varchar(40) CHARACTER SET UTF8                           ,
      LIQUANTITY integer,
      MGQUANTITY integer,
      TIMEWORKPZ varchar(40) CHARACTER SET UTF8                           ,
      TIMEWORKPZA varchar(40) CHARACTER SET UTF8                           ,
      TIMEWORKPZB varchar(40) CHARACTER SET UTF8                           ,
      PZQUANTITY integer,
      MISTAKESUM integer,
      LONGESTBRAKE varchar(40) CHARACTER SET UTF8                           ,
      PACKVERIFY varchar(20) CHARACTER SET UTF8                           ,
      CONSTLOCVERIVYQ varchar(20) CHARACTER SET UTF8                           ,
      CONSTLOCVERIVYTIME varchar(40) CHARACTER SET UTF8                           ,
      WOWORKTIME varchar(40) CHARACTER SET UTF8                           ,
      ACTQONLOGTIME integer,
      ACTQONWORKTIME integer,
      MGDTWORKTIME varchar(40) CHARACTER SET UTF8                           ,
      LIWORKTIME varchar(40) CHARACTER SET UTF8                           ,
      ANOTHERWORKTIME varchar(40) CHARACTER SET UTF8                           )
   as
declare variable worktimenum double precision;
declare variable realtimenum double precision;
declare variable getpalnum double precision;
declare variable braketimenum double precision;
declare variable logtimenum double precision;
declare variable avgtimetogetposnum double precision;
declare variable avgverifytimenum double precision;
declare variable timeworkpznum double precision;
declare variable timeworkpzanum double precision;
declare variable timeworkpzbnum double precision;
declare variable tymtimestart timestamp;
declare variable tymtimestop timestamp;
declare variable tymordref integer;
declare variable tymminustime double precision;
declare variable longestbrakenum double precision;
declare variable constlocverivytimenum double precision;
declare variable woworktimenum double precision;
declare variable woverifytimenum double precision;
declare variable worktimeinsec double precision;
declare variable oneacttime double precision;
declare variable realtimenum2 double precision;
declare variable realtimeinsec double precision;
declare variable constlocverivyfinddiv integer;
declare variable mgdtworktimenum double precision;
declare variable liworktimenum double precision;
declare variable anotherworktimenum double precision;
begin
  for
    select o.nazwa, who.operator
      from opermag who
        join operator o on (o.ref = who.operator)
      where who.magazyn = :wh --and who.cantakemwsords = 1
        and (o.ref = :operatorin or :operatorin is null or :operatorin = 0)
        and o.aktywny = 1
        and exists (select first 1 1 from mwsordtypedest4op p where p.wh = who.magazyn and p.operator = o.ref)
      into operatorname, operator
  do begin
    select m.outship, m.outown, m.up, m.stock, m.palsmove, m.palsin
      from xk_mws_day_stats(:operator, :wh, :bdate, :edate) m
      into wtquantity, woquantity, wgquantity, liquantity, mgquantity, pzquantity;
    select sum(a1.mistakescnt)
      from  mwsords o1
       left join mwsacts a1 on (a1.mwsord = o1.ref)
      where o1.operator = :operator
        and o1.status = 5
        and o1.timestop >= :bdate
        and o1.timestop <= :edate + 1
        and a1.mistakescnt is not null
      into :mistakescnt;
    select sum(a1.mistakescntq)           --ok
      from  mwsords o1
        left join mwsacts a1 on (a1.mwsord = o1.ref )
      where o1.status = 5
        and o1.operator = :operator
        and  o1.takefullpal = 0
        and o1.timestop >= :bdate
        and o1.timestop <= :edate + 1
        and a1.mistakescntq is not null
      into :mistakescntquan;
    if (pzquantity is null) then pzquantity = 0;
    if (mgquantity is null) then mgquantity = 0;
    if (liquantity is null) then liquantity = 0;
    if (mistakeonpackq is null) then mistakeonpackq = 0;
    if (mistakeonget is null) then mistakeonget = 0;
    if (mistakescnt is null) then mistakescnt = 0;
    if (mistakescntquan is null) then mistakescntquan = 0;
    if (wtquantity is null) then wtquantity = 0;
    ---- nowe kolumny
    timestart = null;
    constlocverivyq = '';
    packverify = '';
    constlocverivyfinddiv = 0;
    constlocverivytimenum = 0;
    constlocverivytime = '';
    timestop = null;
    worktimenum = 0;
    realtimenum = 0;
    logtimenum = 0;
    woworktimenum = 0;
    woverifytimenum = 0;
    avgtimetogetposnum = 0;
    avgverifytimenum = 0;
    liworktimenum = 0;
    mgdtworktimenum = 0;
    worktimeinsec = 0;
    worktimenum = 0;
    oneacttime = 0;
    realtimenum2 = 0;
    realtimeinsec = 0;
    noworktime = '';
    woworktime ='';
    worktime = '';
    braketime = '';
    avgtimetogetpos = '';
    avgverifytime = '';
    timeworkpz = '';
    timeworkpza = '';
    timeworkpzb = '';
    liworktime = '';
    mgdtworktime = '';
    timeworkpznum = 0;
    timeworkpzanum = 0;
    timeworkpzbnum = 0;
    anotherworktime = '';
    anotherworktimenum = 0;
    actqonlogtime = 0;
    actqonworktime = 0;
    mistakesum = coalesce(mistakescnt,0) + coalesce(mistakescntquan,0)+coalesce(mistakeonget,0) + coalesce(mistakeonpackq,0);
    select sum(coalesce(m.timestop,current_timestamp(0)) - m.timestart)       --ok                                                                       -- ok
      from mwsordsemployees m
        left join employees e on(e.ref = m.employee)
        left join persons p on(p.ref = e.person)
        join mwsords mo on(mo.ref = m.mwsord and mo.mwsordtype = 8)
      where p.sref = :operator
        and m.timestart >= :bdate
        and m.timestop  <= :edate + 1
      into :timeworkpznum;
    if (coalesce(timeworkpznum,0) > 0) then
    execute procedure XK_MWS_TIMESTAMP_DIFF_TOSTRING(:timeworkpznum)
      returning_values :timeworkpz;
    select sum(coalesce(m.timestop,current_timestamp(0)) - m.timestart)     --ok                                                                       -- ok
      from mwsordsemployees m
        left join employees e on(e.ref = m.employee)
        left join persons p on(p.ref = e.person)
        join mwsords mo on(mo.ref = m.mwsord and mo.mwsordtype = 8)
        join slopoz s on(s.ref = m.slopoz and s.kod = 'TYPA')
      where p.sref = :operator
        and m.timestart >= :bdate
        and m.timestop  <= :edate + 1
      into :timeworkpzanum;
    if (coalesce(timeworkpzanum,0) > 0) then
    execute procedure XK_MWS_TIMESTAMP_DIFF_TOSTRING(:timeworkpzanum)
      returning_values :timeworkpza;
    select sum(coalesce(m.timestop,current_timestamp(0)) - m.timestart)     --ok                                                                       -- ok
      from mwsordsemployees m
        left join employees e on(e.ref = m.employee)
        left join persons p on(p.ref = e.person)
        join mwsords mo on(mo.ref = m.mwsord and mo.mwsordtype = 8)
        join slopoz s on(s.ref = m.slopoz and s.kod = 'TYPB')
      where p.sref = :operator
        and m.timestart >= :bdate
        and m.timestop  <= :edate + 1
      into :timeworkpzbnum;
    if (coalesce(timeworkpzbnum,0) > 0) then
    execute procedure XK_MWS_TIMESTAMP_DIFF_TOSTRING(:timeworkpzbnum)
      returning_values :timeworkpzb;
    select first 1 m.timestartfrom                  --ok
      from mwsaccessoryhist m
      where m.timestartfrom >= :bdate
        and m.timestartfrom <= :edate + 1
        and m.operator = :operator
      order by m.timestartfrom asc
      into timestart;
    select first 1 m.timestartto               -- ok 
      from mwsaccessoryhist m
      where m.timestartfrom >= :bdate
        and m.timestartfrom <= :edate + 1
        and m.operator = :operator
      order by m.timestartfrom desc
      into timestop;
    select sum(coalesce(m.timestartto,current_timestamp(0)) - m.timestartfrom)
      from mwsaccessoryhist m
      where m.timestartfrom >= :bdate
        and m.timestartfrom <= :edate + 1
        and m.operator = :operator
       into :worktimenum;
    if (worktimenum is not null) then
      execute procedure XK_MWS_TIMESTAMP_DIFF_TOSTRING(:worktimenum)
        returning_values :worktime;
    select sum(o1.realtime) from mwsords o1       --ok
      where o1.operator = :operator
        and o1.status = 5
        and o1.timestop >= :bdate
        and o1.timestop <= :edate + 1
      into :realtimenum;
--  początek
    for
       select mo.ref, ma.timestart, ma.timestop              --ok
        from mwsords mo
          left join mwsacts ma on(ma.mwsord = mo.ref and ma.timestop is not null)
        where mo.operator = :operator
          and mo.timestop >= :bdate
          and mo.timestop <= :edate + 1
        group by mo.ref, ma.timestart, ma.timestop
        having ma.timestop - ma.timestart > 0.00348
        into :tymordref, :tymtimestart, :tymtimestop
    do begin
      for
        select mo.timestop - mo.timestart           --ok
          from mwsords mo
          where mo.operator = :operator
            and mo.timestart > :tymtimestart
            and mo.timestop < :tymtimestop
            and mo.timestop is not null
          into :tymminustime
      do begin
        realtimenum = realtimenum - tymminustime;
      end
  end
  realtimenum2 = realtimenum;
  realtimenum = worktimenum - realtimenum;
  if (realtimenum < 0) then realtimenum = 0;
  execute procedure xk_MWS_TIMESTAMP_DIFF_TOSTRING(:realtimenum)
    returning_values :noworktime;
   select sum(a1.realtime)      --ok
    from  mwsords o1
       left join mwsacts a1 on (a1.mwsord = o1.ref)
     where o1.wh = :wh
      and o1.operator = :operator
      and o1.status = 5
      and o1.timestop >= :bdate
      and o1.timestop <= :edate + 1
      and a1.ispal = 1
    into :getpalnum;
  execute procedure xk_MWS_TIMESTAMP_DIFF_TOSTRING(:getpalnum)
    returning_values :timetogetpall;
  logtimenum = timestop - timestart;
  braketimenum = logtimenum - worktimenum;
  execute procedure xk_MWS_TIMESTAMP_DIFF_TOSTRING(:braketimenum)
    returning_values :braketime;
  allquantity = wtquantity + woquantity + wgquantity;
  if (allquantity > 0) then
  begin
    avgtimetogetposnum = worktimenum/allquantity;
    execute procedure xk_MWS_TIMESTAMP_DIFF_TOSTRING(:avgtimetogetposnum)
      returning_values :avgtimetogetpos;
    select sum(o1.timestop - (select first 1 ma.timestop from mwsacts ma where ma.mwsord = o1.ref and ma.status = 5 order by ma.number desc))     --ok
      from mwsords o1
      where o1.operator = :operator
        and (o1.mwsordtype = 1 or o1.mwsordtype = 21)
        and o1.status = 5
        and o1.timestop >= :bdate
        and o1.timestop <= :edate + 1
      into :avgverifytimenum;
    avgverifytimenum = avgverifytimenum / allquantity;
    execute procedure xk_MWS_TIMESTAMP_DIFF_TOSTRING(:avgverifytimenum)
      returning_values :avgverifytime;
  end
  select first 1 mo1.ref, min(mo2.timestart) - min(mo1.timestop)
    from mwsords mo1
      left join mwsords mo2 on (mo2.operator = mo1.operator and mo2.timestart > mo1.timestop)
    where mo1.operator = :operator
      and mo1.timestart >= :bdate
      and mo1.timestart <= :edate + 1
      and mo2.timestart >= :bdate
      and mo2.timestart <= :edate + 1
    group by mo1.ref
    having min(mo2.timestart) - min(mo1.timestop) > 0.00348
    order by min(mo1.timestart), min(mo2.timestart) - min(mo1.timestop) desc
    into tymordref, longestbrakenum;
  execute procedure xk_MWS_TIMESTAMP_DIFF_TOSTRING(:longestbrakenum)
    returning_values :longestbrake;
  select count(distinct ma.mwsord)||' / '
    from mwsacts ma
    where ma.operator = :operator
      and ma.status = 5
      and ma.wh = :wh
      and ma.timestop >= :bdate
      and ma.timestop <= :edate + 1
      and ma.quantityv is not null
    into :packverify;
  select :packverify||count (ma.ref)||' / '
    from mwsacts ma
    where ma.operator = :operator
      and ma.status = 5
      and ma.wh = :wh
      and ma.timestop >= :bdate
      and ma.timestop <= :edate + 1
      and ma.quantityv is not null
    into packverify;
  select :packverify||count (ma.ref)
    from mwsacts ma
    where ma.operator = :operator
      and ma.status = 5
      and ma.wh = :wh
      and ma.timestop >= :bdate
      and ma.timestop <= :edate + 1
      and coalesce(ma.quantityv,ma.quantityc) <> ma.quantityc
    into packverify;
  select sum(ma.realtime)              --ok
    from mwsords mo
      left join mwsacts ma on(ma.mwsord = mo.ref and ma.timestop is not null)
    where mo.wh = :wh
      and mo.mwsordtypedest = 8
      and mo.operator = :operator
      and mo.timestop >= :bdate
      and mo.timestop <= :edate+1
    into :woworktimenum;
  if (woworktimenum is null) then woworktimenum = 0;
  if (woworktimenum > 0) then begin
    execute procedure xk_MWS_TIMESTAMP_DIFF_TOSTRING(:woworktimenum)
      returning_values :woworktime;
  end
  select sum(mo.realtime)              --ok
    from mwsords mo
    where mo.wh = :wh
      and mo.mwsordtypedest = 11
      and mo.operator = :operator
      and mo.timestop >= :bdate
      and mo.timestop <= :edate + 1
    into :mgdtworktimenum;
  if (mgdtworktimenum > 0) then
    execute procedure xk_MWS_TIMESTAMP_DIFF_TOSTRING(:mgdtworktimenum)
      returning_values :mgdtworktime;
  select sum(mo.realtime)              --ok
    from mwsords mo
    where mo.wh = :wh
      and mo.mwsordtypedest = 15
      and mo.operator = :operator
      and mo.timestop >= :bdate
      and mo.timestop <= :edate + 1
    into :liworktimenum;
  if (liworktimenum > 0) then
    execute procedure xk_MWS_TIMESTAMP_DIFF_TOSTRING(:liworktimenum)
      returning_values :liworktime;
  select sum(coalesce(m.timestop,current_timestamp(0)) - m.timestart)       --ok                                                                       -- ok
    from mwsordsemployees m
      left join employees e on(e.ref = m.employee)
      left join persons p on(p.ref = e.person)
    where p.sref = :operator
      and m.timestart >= :bdate
      and m.timestop  <= :edate + 1
      and m.mwsord is null
    into :anotherworktimenum;
  if (anotherworktimenum > 0) then
    execute procedure xk_MWS_TIMESTAMP_DIFF_TOSTRING(:anotherworktimenum)
      returning_values :anotherworktime;
   if (worktime <> '' or timeworkpz <> '' OR anotherworktime <> '' or timeworkpza <> '' or timeworkpzb <> '') then
      suspend;
   allquantity = 0;
  end
end^
SET TERM ; ^
