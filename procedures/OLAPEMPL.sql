--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OLAPEMPL
  returns (
      WYDZIAL varchar(10) CHARACTER SET UTF8                           ,
      ODDZIAL varchar(10) CHARACTER SET UTF8                           ,
      PLEC varchar(1) CHARACTER SET UTF8                           ,
      WYKSZTALCENIE varchar(40) CHARACTER SET UTF8                           ,
      STANOWISKO varchar(40) CHARACTER SET UTF8                           ,
      NADZIEN date,
      ILOSC smallint)
   as
declare variable fdate date;
begin
  for
    select B.sdate, B.fdate
    from bkperiods B
    where B.ord <> 0 and B.ord <> 13
    into :nadzien, :fdate
    do begin
      for
        select EM.department, EM.branch, case when P.sex = 1 then 'M' else 'K' end, substring (Z.descript from 1 for 40), ED.workpost, 1
        from employment EM
          join employees E on (EM.employee = E.ref)
          join persons P on (E.person = P.ref)
          left join edictworkposts ED on (ED.ref = EM.workpost)
          left join eperschools EP on (P.ref = EP.person and EP.fromdate = (select max(EP1.fromdate) from eperschools EP1 where EP.person = EP1.person))
          left join edictzuscodes Z on (EP.education = Z.ref)
        where EM.fromdate <= :nadzien and (EM.todate >= :nadzien or EM.todate is null)
        into :wydzial, :oddzial, :plec, :wyksztalcenie, :stanowisko, :ilosc
        do begin
          suspend;
        end
    end
end^
SET TERM ; ^
