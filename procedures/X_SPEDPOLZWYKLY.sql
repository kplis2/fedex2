--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_SPEDPOLZWYKLY(
      SPOSD integer,
      SPOSZ integer,
      WAGA numeric(14,2),
      KODP varchar(6) CHARACTER SET UTF8                           ,
      SUMWARTNET numeric(14,2),
      SUMWARTBRU numeric(14,2),
      FLAGI varchar(255) CHARACTER SET UTF8                           ,
      SPEDYTOR varchar(80) CHARACTER SET UTF8                           )
  returns (
      KOSZTC numeric(14,2),
      KOD varchar(10) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE KOSZTM NUMERIC(14,2);
DECLARE VARIABLE ILPACZEKNUM NUMERIC(14,2);
DECLARE VARIABLE ODLEG NUMERIC(14,2);
DECLARE VARIABLE ILPACZEK INTEGER;
begin
  select spedodleg.odleglosc, spedodleg.kodrejonu from SPEDODLEG
    where spedodleg.spedytor = :sposd and spedodleg.kodp=:kodp
    into :odleg, :kod;

  select kosztdost.koszt from  kosztdost
    where kosztdost.spedytor=:sposd and kosztdost.odleglosc>=:odleg
      and kosztdost.odlegloscp<:odleg and kosztdost.waga>=:waga
      and kosztdost.wagap<:waga
    into :kosztc;
  suspend;
end^
SET TERM ; ^
