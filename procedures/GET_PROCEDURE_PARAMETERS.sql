--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_PROCEDURE_PARAMETERS(
      PROCEDURENAME STRING40)
  returns (
      PARAMETERNAME STRING40,
      PARAMETERNUMBER SMALLINT_ID)
   as
begin
  for
    select p.rdb$parameter_name, p.rdb$parameter_number +1
      from rdb$procedure_parameters p
        where p.rdb$procedure_name = :procedurename and p.rdb$parameter_type = 1
      order by p.rdb$parameter_number
    into :parametername, :parameternumber
  do begin
    suspend;
  end
end^
SET TERM ; ^
