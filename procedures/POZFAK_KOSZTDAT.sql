--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZFAK_KOSZTDAT(
      POZFAKREF integer)
  returns (
      KOSZT numeric(14,2))
   as
declare variable mies integer;
declare variable rok integer;
declare variable datatmp varchar(10);
declare variable date_end timestamp;
declare variable date_rozlicz timestamp;
declare variable koszt_katal numeric(14,2);
declare variable koszt_pierwotny numeric(14,2);
declare variable zmiana_kosztow numeric(14,2);
declare variable koryg smallint;
begin
    select extract(month from nagfak.data),  extract(year from nagfak.data),
        magrozliczdata, pozfak.kosztkat, pozfak.pkosztzak
       from pozfak
       join nagfak on(pozfak.dokument=nagfak.ref)
       where pozfak.ref = :pozfakref
       into :mies, :rok, :DATE_ROZLICZ,  :koszt_katal, :koszt_pierwotny;
    if(:mies = 12) then begin
      mies = 1;
      rok = :rok + 1;
    end
    else
      mies = mies + 1;

    datatmp = :mies;
    if(:mies < 10) then
      datatmp = '0'||:mies;
    datatmp = :rok||'-'||:datatmp||'-01';
    date_end = cast(:datatmp as timestamp);
    date_end = :date_end - 1;
      select sum(dokumroz.ilosc * (dokumnotp.cenanew - dokumnotp.cenaold))
        from pozfak
          left join nagfak on (pozfak.dokument = nagfak.ref)
          left join dokumpoz on (pozfak.ref = dokumpoz.frompozfak)
          left join dokumroz on (dokumpoz.ref = dokumroz.pozycja)
          left join dokumnotp on (dokumroz.ref = dokumnotp.dokumrozkor)
          left join dokumnot on (dokumnotp.dokument = dokumnot.ref)
        where pozfak.ref = :pozfakref and
          dokumnot.data <= :date_end into :zmiana_kosztow;
    if(:zmiana_kosztow is null) then begin
      select (dokumroz.ilosc * (dokumnotp.cenanew - dokumnotp.cenaold))
        from pozfak
          left join nagfak on (pozfak.dokument = nagfak.ref)
          left join dokumpoz on (pozfak.fromdokumpoz = dokumpoz.ref)
          left join dokumroz on (dokumpoz.ref = dokumroz.pozycja)
          left join dokumnotp on (dokumroz.ref = dokumnotp.dokumrozkor)
          left join dokumnot on (dokumnotp.dokument = dokumnot.ref)
        where pozfak.ref = :pozfakref and
          dokumnot.data <= :date_end into :zmiana_kosztow;
     end

    if(:date_rozlicz is null or :date_rozlicz > :date_end) then koszt_pierwotny = :koszt_katal;
    if(:koszt_pierwotny is null) then koszt_pierwotny = 0;
    if(:zmiana_kosztow is null) then zmiana_kosztow = 0;
    koszt = :koszt_pierwotny + :zmiana_kosztow;
  suspend;
end^
SET TERM ; ^
