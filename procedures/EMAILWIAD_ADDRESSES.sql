--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWIAD_ADDRESSES(
      SEARCH varchar(255) CHARACTER SET UTF8                           )
  returns (
      ID varchar(255) CHARACTER SET UTF8                           ,
      EMAIL varchar(255) CHARACTER SET UTF8                           ,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      OPERATOR integer,
      PODMIOT integer,
      PKOSOBA integer)
   as
declare variable n integer;
begin
  n = 20;
 /* -- element wyszukiwany
  id = 'SEARCH';
  email = :search;
  name = :search;
  descript = :search;
  operator = null;
  podmiot = null;
  pkosoba = null;
  suspend;*/
  if(coalesce(char_length(:search),0)<3) then exit; -- [DG] XXX ZG119346
  search = lower(:search);
  -- lista operatorów
  for select 'O'||OPERATOR.ref, OPERATOR.email, OPERATOR.nazwa, OPERATOR.nazwa||' <'||operator.email||'>',  OPERATOR.REF, 0,0
    from OPERATOR
    where email <> '' and ((lower(email) starting with :search) or (lower(nazwisko) starting with :search) or (lower(imie) starting with :search) )
    into :id,:email,:name,:descript,:operator,:podmiot,:pkosoba
  do begin
    suspend;
    n = :n - 1;
    if(:n=0) then exit;
  end

  -- lista osób CRM
  for select 'PK'||pkosoby.ref, pkosoby.email, pkosoby.nazwa, pkosoby.nazwa||' <'||pkosoby.email||'>', 0, pkosoby.cpodmiot,pkosoby.ref
    from pkosoby
    where email <> '' and ((lower(email) starting with :search) or (lower(nazwa) starting with :search))
    into :id,:email,:name,:descript,:operator,:podmiot,:pkosoba
  do begin
    suspend;
    n = :n - 1;
    if(:n=0) then exit;
  end
  -- TODO lista pracowników
end^
SET TERM ; ^
