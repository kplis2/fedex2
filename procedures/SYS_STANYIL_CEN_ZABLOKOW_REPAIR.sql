--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_STANYIL_CEN_ZABLOKOW_REPAIR as
DECLARE VARIABLE MAG VARCHAR(3);
DECLARE VARIABLE WERSJAREF INTEGER;
DECLARE VARIABLE CENA NUMERIC(14,4);
DECLARE VARIABLE DOSTAWA INTEGER;
DECLARE VARIABLE ZABLOKOW NUMERIC(14,4);
begin
--naprawia blednie naliczone zablokow na stanycen oraz zablokow i zamowiona na stanyil
--aby sprawdzic czy takie bledne naliczenia wystepuja wystarczy wykonac selecty z forów (miedzy for a into)

  for select  sc.magazyn, sc.wersjaref, sc.cena, sc.dostawa from stanycen sc where sc.zablokow <>0 and
    ((select sum(sr.ilminus) from stanyrez sr where sr.status ='B' and sc.dostawa =sr.dostawa and sc.wersjaref=sr.wersjaref and zreal = 0) is null
     or sc.zablokow <> (select sum(sr.ilminus) from stanyrez sr where sr.status ='B' and sc.dostawa =sr.dostawa and sc.wersjaref=sr.wersjaref and zreal = 0))
    into :mag, :wersjaref, :cena, :dostawa
  do begin
    zablokow = 0;
    select sum(ilminus) from stanyrez where magazyn = :mag and wersjaref = :wersjaref
      and status = 'B' and zreal = 0 and (cena is null or cena = 0 or cena = :cena)
      and (dostawa is null or dostawa =0 or dostawa = :dostawa)
    into zablokow;
    if(zablokow is null) then zablokow = 0;
    update stanycen set zablokow = :zablokow where magazyn = :mag and wersjaref = :wersjaref and dostawa = :dostawa and cena = :cena;
  end

  for select  si.magazyn, si.wersjaref from stanyil si where si.zablokow <>0 and
    ((select sum(sr.ilminus) from stanyrez sr where sr.status ='B' and si.wersjaref=sr.wersjaref and zreal = 0) is null
     or si.zablokow <> (select sum(sr.ilminus) from stanyrez sr where sr.status ='B' and si.wersjaref=sr.wersjaref and zreal = 0))
    into :mag, :wersjaref
  do begin
    zablokow = 0;
    select sum(ilminus) from stanyrez where magazyn = :mag and wersjaref = :wersjaref
      and status = 'B' and zreal = 0
    into :zablokow;
    if(zablokow is null) then zablokow = 0;
    update stanyil set zablokow = :zablokow where magazyn = :mag and wersjaref = :wersjaref;
  end

  for select  si.magazyn, si.wersjaref from stanyil si where si.zamowiono <>0 and
    ((select sum(sr.ilosc) from stanyrez sr where sr.status ='D' and si.wersjaref=sr.wersjaref and zreal = 0) is null
     or si.zamowiono <> (select sum(sr.ilosc) from stanyrez sr where sr.status ='D' and si.wersjaref=sr.wersjaref and zreal = 0))
    into :mag, :wersjaref
  do begin
    zablokow = 0;
    select sum(ilosc) from stanyrez where magazyn = :mag and wersjaref = :wersjaref
      and status = 'D' and zreal = 0
    into :zablokow;
    if(zablokow is null) then zablokow = 0;
    update stanyil set zamowiono = :zablokow where magazyn = :mag and wersjaref = :wersjaref;
  end
end^
SET TERM ; ^
