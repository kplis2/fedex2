--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EALGORITHM_RUN_GEN(
      GENEMPTY smallint = 0)
   as
declare variable PROC blob sub_type 0 segment size 80;
declare variable STM varchar(512);
declare variable EALGNAME varchar(31);
declare variable ECOLUMN integer;
declare variable FREESIGNS integer;
declare variable EALGTYPE integer;
declare variable ISLEAVE smallint = 0;
declare variable ISNOPROC smallint = 0;
declare variable NL varchar(3);
begin
/*MWr: Personel - Aktualizacja listy algorytmow placowych w EALGORITHM_RUN.
  Procedura dodatkowo dokleja informacje w przypadku: wykrycia brakow w algorytmach
  badz oscylowania na granicy przekroczenia limitu liczby znakow w procedurze */

  nl = '
';
  proc = 'create or alter procedure EALGORITHM_RUN' ||
'(id varchar(10), em integer, pr integer, ablock varchar(4096) = null)
returns (ret numeric(14,2)) as begin ' || nl;

  if (genempty = 0) then
  begin
    stm = '';
    for
      select ecolumn, ealgtype from ealgorithms
        order by ealgtype, ecolumn
        into :ecolumn, :ealgtype
    do begin
      ealgname = 'XX_EF_ALGORITHM_' || ealgtype || '_' || ecolumn ;
      if (not exists(select first 1 1 from rdb$procedures where rdb$procedure_name = :ealgname)) then
        isnoproc = 1;
      else begin
        if (stm <> '') then stm = ' else';
        stm = stm || (' if (id=''') || ealgtype || '_' || ecolumn || ''') then execute procedure ' || ealgname || '(em,pr) returning_values ret;' || nl;
        proc = proc || stm;
        freesigns = 8191 - char_length(proc);  --BS65875 
        if (freesigns < 200) then
        begin
          isleave = 1;
          leave;
        end
      end
    end
  
    if (isleave = 1 or isnoproc = 1) then
    begin
      if (stm <> '') then proc = proc || ' else';
      proc = proc || ' execute statement :ablock into :ret;';
      if (isleave = 1) then proc = proc || '   /* wyczerpany limit znakow */';
      if (isnoproc = 1) then proc = proc || '   /* braki w algorytmach */';
      proc = proc || nl;
    end
  end
  proc = proc || ' suspend; ' || nl || 'end';
  execute statement proc;
end^
SET TERM ; ^
