--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NOTIFY_CRM
  returns (
      ID varchar(20) CHARACTER SET UTF8                           ,
      PARENTID varchar(20) CHARACTER SET UTF8                           ,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           ,
      NUMVAL numeric(14,2),
      WATCHBELOW smallint,
      COLORINDEX integer,
      PRIORITY integer,
      ICON varchar(255) CHARACTER SET UTF8                           ,
      MODULE varchar(255) CHARACTER SET UTF8                           ,
      ACTIONID varchar(255) CHARACTER SET UTF8                           ,
      ACTIONPARAMS varchar(255) CHARACTER SET UTF8                           ,
      ACTIONNAME varchar(255) CHARACTER SET UTF8                           ,
      RIGHTSGROUP varchar(255) CHARACTER SET UTF8                           ,
      RIGHTS varchar(255) CHARACTER SET UTF8                           )
   as
declare variable refoper integer;
declare variable nazwaoper varchar(255);
declare variable sumval numeric(14,2);
begin
  -- zadania niewykonane
  watchbelow = 0;
  colorindex = 2;
  priority = 1;
  icon = 'MI_ZADANIA';
  module = 'DMS';
  actionid = 'DisplayMainZADANIA';
  actionparams = '';
  actionname = 'Terminarz zadań';
  rightsgroup = '';
  rights = '';
  sumval = 0;
  for select count(*),z.operwyk,max(o.nazwa)
    from zadania z
    join operator o on o.ref=z.operwyk
    where z.stan=0
    group by z.operwyk
    into :numval, :refoper, :nazwaoper
  do begin
    id = 'ZADANIA'||:refoper;
    parentid = 'ZADANIA';
    val = cast(:numval as integer);
    descript = :nazwaoper;
    suspend;
    sumval = :sumval + :numval;
  end
  id = 'ZADANIA';
  parentid = '';
  numval = :sumval;
  val = cast(:numval as integer);
  descript = 'Zadania niewykonane';
  suspend;

  -- zadania przeterminowane
  watchbelow = 0;
  colorindex = 2;
  priority = 1;
  icon = 'MI_ZADANIA';
  module = 'DMS';
  actionid = 'DisplayMainZADANIA';
  actionparams = '';
  actionname = 'Terminarz zadań';
  rightsgroup = '';
  rights = '';
  sumval = 0;
  for select count(*),z.operwyk,max(o.nazwa)
    from zadania z
    join operator o on o.ref=z.operwyk
    where z.stan=0 and data<current_date
    group by z.operwyk
    into :numval, :refoper, :nazwaoper
  do begin
    id = 'ZADANIAP'||:refoper;
    parentid = 'ZADANIAP';
    val = cast(:numval as integer);
    descript = :nazwaoper;
    suspend;
    sumval = :sumval + :numval;
  end
  id = 'ZADANIAP';
  parentid = '';
  numval = :sumval;
  val = cast(:numval as integer);
  descript = 'Zadania zalegle';
  suspend;

/*  -- dokumenty w obiegu
  zadania where wfworkflow is not null

  -- dokumenty do dekretacji
  wfworkflows where status=2
    from dokzlacz z
    left join wfworkflows w on z.ztable = 'WFWORKFLOWS' and z.zdokum = w.ref
    where z.dokument = :dokplik*/

  -- dokumenty zablokowane
  watchbelow = 0;
  colorindex = 2;
  priority = 1;
  icon = 'MI_DOC';
  module = 'DMS';
  actionid = 'DisplayDOKUMENTYMojeDokumenty';
  actionparams = '';
  actionname = 'Moje dokumenty';
  rightsgroup = '';
  rights = '';
  sumval = 0;
  for select count(*),d.operblok,max(o.nazwa)
    from dokplik d
    join operator o on o.ref=d.operblok
    where d.blokada=1
    group by d.operblok
    into :numval, :refoper, :nazwaoper
  do begin
    id = 'DOKBLOK'||:refoper;
    parentid = 'DOKBLOK';
    val = cast(:numval as integer);
    descript = :nazwaoper;
    suspend;
    sumval = :sumval + :numval;
  end
  id = 'DOKBLOK';
  parentid = '';
  numval = :sumval;
  val = cast(:numval as integer);
  descript = 'Dokumenty pobrane';
  suspend;
end^
SET TERM ; ^
