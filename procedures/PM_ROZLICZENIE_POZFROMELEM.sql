--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PM_ROZLICZENIE_POZFROMELEM(
      PMELEMENT integer,
      DATAOD date,
      DATADO date)
  returns (
      REF integer,
      STYP varchar(2) CHARACTER SET UTF8                           ,
      KWOTA numeric(14,2),
      NUMERPOZ integer,
      NUMERDOK integer,
      SYMBOL varchar(40) CHARACTER SET UTF8                           ,
      ILOSC numeric(14,4),
      KTM varchar(40) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           )
   as
declare variable ref_poz integer;
begin
  for select ref
    from pmpositions
    where pmpositions.pmelement = :pmelement
    into :ref_poz
  do begin
    for select ref, styp, kwota, numerpoz, numerdok, symbol, ilosc, ktm, nazwa
      from PM_ROZLICZENIE_POZ(:REF_POZ, :dataod, :datado)
      into :ref, :styp, :kwota, :numerpoz, :numerdok, :symbol, :ilosc, :ktm, :nazwa
      do begin
        suspend;
      end
  end
end^
SET TERM ; ^
