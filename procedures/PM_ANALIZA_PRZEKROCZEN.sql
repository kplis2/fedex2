--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PM_ANALIZA_PRZEKROCZEN(
      PMPLAN integer,
      MASKA varchar(255) CHARACTER SET UTF8                           ,
      NADZIEN date)
  returns (
      PROJEKT varchar(255) CHARACTER SET UTF8                           ,
      DZIEN date,
      REF integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      NUMBER varchar(255) CHARACTER SET UTF8                           ,
      SYMBOL varchar(255) CHARACTER SET UTF8                           ,
      WARTPLAN numeric(14,2),
      WARTRZECZYW numeric(14,2),
      ROZNICA numeric(14,2),
      WARTRZECZYWNADZIEN numeric(14,2),
      ROZNICANADZIEN numeric(14,2),
      KOLOR smallint)
   as
begin

  for
   select p.symbol as projekt, e.REF, e.NAME, e.NUMBER,e.SYMBOL, e.BUDSUMVAL, e.REALSUMVAL,
         (e.BUDSUMVAL-e.REALSUMVAL) as ROZNICA,
         case when (e.REALMVAL>e.BUDMVAL) or (e.REALSVAL>e.BUDSVAL) or (e.REALWVAL>e.BUDWVAL) or (e.REALEVAL>e.BUDEVAL)
         then 1
         else 0
         end as KOLOR
   from pmelements e
   left join pmplans p on (p.ref = e.pmplan)
   where pmplan = :pmplan
         and (e.symbol like :maska||'%' or :maska is null)
   into :projekt, :ref, :name,  :number, :symbol, :wartplan, :wartrzeczyw, :roznica, :kolor
  do begin
    if (nadzien is not null) then
    begin
      select sum(coalesce((select sum(kwota) from pm_rozliczenie_poz(pp.ref,null,:nadzien)),0))
        from pmpositions pp where pp.pmelement = :ref
        into :wartrzeczywnadzien;

      roznicanadzien = wartplan - wartrzeczywnadzien;
      dzien = nadzien;
     end
    suspend;
  end
end^
SET TERM ; ^
