--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ROZRACH_OBL_NAG(
      KONTOFK KONTO_ID,
      SLODEF integer,
      SLOPOZ integer,
      SYMBFAK varchar(20) CHARACTER SET UTF8                           ,
      COMPANY integer)
   as
declare variable wn  DECIMAL(14,2);
declare variable ma DECIMAL(14,2);
declare variable wnzl  DECIMAL(14,2);
declare variable mazl DECIMAL(14,2);
declare variable cnt integer;
begin
  select count(*), sum(winien), sum(ma), sum(winienzl), sum(mazl)
    from ROZRACHP
    where KONTOFK = :kontofk and slodef = :slodef and slopoz = :slopoz
      and symbfak  = :symbfak and company = :company
    into :cnt, :wn, :ma, :wnzl, :mazl;
  if (cnt = 0) then
  begin
    delete from rozrach
      where kontofk = :kontofk and slodef = :slodef and slopoz = :slopoz
      and symbfak = :symbfak and company = :company and TOKEN <> 7;
  end else
  begin
    if (wn is null) then wn = 0;
    if (ma is null) then ma = 0;
    if (wnzl is null) then wnzl = 0;
    if (mazl is null) then mazl = 0;
    update ROZRACH set WINIEN = :WN, MA=:ma, WINIENZL=:wnzl, MAZL=:mazl
      where kontofk = :kontofk and slodef = :slodef and slopoz = :slopoz
        and symbfak = :symbfak and company = :company;
  end
end^
SET TERM ; ^
