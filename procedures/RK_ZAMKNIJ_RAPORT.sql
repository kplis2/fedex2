--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RK_ZAMKNIJ_RAPORT(
      RAPORT integer)
   as
declare variable DOKUMENT integer;
declare variable CZY_OB_DOK smallint;
declare variable POPRZED integer;
declare variable STANOWISKO varchar(3);
declare variable RAPNUM integer;
declare variable RAPDAT timestamp;
declare variable RAPDAT2 timestamp;
declare variable NUMER integer;
declare variable DOKREF integer;
declare variable KASABANK char(1);
begin
  -- sprawdzenie, czy raport ma poprzedzajÄ…ce otwarte
  select R.STANOWISKO, R.NUMER, R.DATAOD, S.kasabank
    from RKRAPKAS R
      join RKSTNKAS S on (S.kod = R.stanowisko)
    where REF=:raport
    into :stanowisko, :rapnum, :rapdat, :kasabank;
  poprzed = 0;

  for
    select numer, dataod
      from RKRAPKAS where STANOWISKO = :stanowisko and STATUS = 0 and DATAOD <= :rapdat
      order by DATAOD, NUMER
      into :numer, :rapdat2
  do begin
    if (rapdat2 < rapdat or numer < rapnum) then
      poprzed = 1;
  end

  if (poprzed > 0 and kasabank = 'K') then -- kontrola chronologii tylko dla raportĂłw kasowych
    exception RPRAPKAS_ZAMKNIJPOPRZED;

  for
    select RKDOKNAG.NUMER, RKDEFOPER.OB_DOK, RKDOKNAG.ref
      from RKDOKNAG
      join RKDEFOPER on (RKDOKNAG.OPERACJA=RKDEFOPER.SYMBOL)
      where RKDOKNAG.RAPORT=:raport and RKDOKNAG.anulowany = 0
      into :dokument, :czy_ob_dok, :dokref
  do begin
    if (dokument is null) then
      dokument = 0;
    if (not exists(select ref from RKDOKPOZ where RKDOKPOZ.dokument=:dokref)) then
      exception RK_DOKUMENTBEZPOZYCJI;
    if (czy_ob_dok > 0 and dokument = 0 and kasabank = 'K') then
      exception RK_NIE_WYSTAWIONY_DOKUMENT;
  end

  if (exists(select ref from rkdoknag where raport = :raport
      and status < 2)) then
    exception RK_ZAMKNIJ_RAPORT_EXPT;

  update RKRAPKAS set STATUS = 1
    where REF = :raport and STATUS = 0;
  update RKDOKNAG set BLOKADA = 1 where BLOKADA = 0 and RAPORT = :raport;
end^
SET TERM ; ^
