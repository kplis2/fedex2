--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TURN_ACCEPT(
      OKRES_KONC varchar(6) CHARACTER SET UTF8                           ,
      STATUS smallint)
  returns (
      ROKMIESIAC varchar(10) CHARACTER SET UTF8                           ,
      ZREAL smallint,
      NAZWAUCZESTNIKA varchar(255) CHARACTER SET UTF8                           ,
      SUMA integer,
      ILKWART integer,
      ILMIES integer,
      UCZESTNIK integer)
   as
declare variable rok varchar(4);
declare variable mie varchar(4);
declare variable rok_konc varchar(4);
declare variable mie_konc varchar(4);
declare variable okres varchar(6);
begin
--  ustalenie daty
  rok_konc = substring(okres_konc from 1 for 4) ;
  mie_konc = substring(okres_konc from 5 for 6) ;
  rokmiesiac = okres_konc;

  if (mie_konc = '01') then
    begin
      rok = cast(rok_konc as integer) - 1 ;
      mie = 11;
    end
  else if (mie_konc = '02') then
    begin
      rok = cast(rok_konc as integer) - 1 ;
      mie = 12;
    end
  else
    begin
      rok = rok_konc;
      mie = cast(mie_konc as integer) - 2 ;
    end

  if (mie < 10) then okres = rok || '0' || mie;
  else  okres = rok || mie;

-- wylapywanie
  for
    select cp.cpluczest, sum(cp.ilpkt), cu.skrot, cp.zreal  from cploper cp left join cpluczest cu on (cu.ref = cp.cpluczest)
    where (extract(year from cp.data) || case when extract(month from cp.data)<=9 then '0' || extract(month from cp.data) else extract(month from cp.data) end) >= :okres and  (extract(year from cp.data) || case when extract(month from cp.data)<=9 then '0' || extract(month from cp.data) else extract(month from cp.data) end) <= :okres_konc and cp.zreal = :status and cp.typpkt = 14
    group by cp.cpluczest, cu.skrot , cp.zreal
    --union
    --select CK.cpluczest3 from Ckontrakty CK
    --where (extract(year from CK.datapodum) || case when extract(month from CK.datapodum)<=9 then '0' || extract(month from CK.datapodum) else extract(month from CK.datapodum) end) >= :okres and  (extract(year from CK.datapodum) || case when extract(month from CK.datapodum)<=9 then '0' || extract(month from CK.datapodum) else extract(month from CK.datapodum) end) <= :okres_konc  and CK.cpluczest3 is not null
    --group by CK.cpluczest3
    into :uczestnik, :suma, :nazwauczestnika, :zreal

  do begin
    select count(CK.REF) from Ckontrakty CK where (extract(year from CK.datapodum) || case when extract(month from CK.datapodum)<=9 then '0' || extract(month from CK.datapodum) else extract(month from CK.datapodum) end) >= :okres and  (extract(year from CK.datapodum) || case when extract(month from CK.datapodum)<=9 then '0' || extract(month from CK.datapodum) else extract(month from CK.datapodum) end) <= :okres_konc and CK.cpluczest = :uczestnik into :ilkwart;
    select count(CK.REF) from Ckontrakty CK where (case when extract(month from CK.datapodum)<=9 then '0' || extract(month from CK.datapodum) else extract(month from CK.datapodum) end) = :mie_konc and CK.cpluczest = :uczestnik into :ilmies;
    suspend;
  end

end^
SET TERM ; ^
