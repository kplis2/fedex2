--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_VAT_SPRROZ(
      PERIODID varchar(6) CHARACTER SET UTF8                           ,
      VREG varchar(10) CHARACTER SET UTF8                           ,
      ZAKRES smallint,
      ZPOPMIES smallint,
      TYP smallint,
      COMPANY integer,
      ZAKUP integer,
      VATDEBITTYPE integer)
  returns (
      REF integer,
      LP integer,
      VATREG varchar(10) CHARACTER SET UTF8                           ,
      VATREGNR integer,
      TRANSDATE timestamp,
      DOCDATE timestamp,
      DOCSYMBOL DOCSYMBOL_ID,
      BKREG varchar(10) CHARACTER SET UTF8                           ,
      BKREGNR integer,
      KONTRAHKOD varchar(255) CHARACTER SET UTF8                           ,
      NIP varchar(15) CHARACTER SET UTF8                           ,
      WARTBRU numeric(14,2),
      WARTNET numeric(14,2),
      WARTVATN numeric(14,2),
      WARTVATV numeric(14,2),
      WARTVATE numeric(14,2),
      WART22 numeric(14,2),
      WART23 numeric(14,2),
      WART12 numeric(14,2),
      WART07 numeric(14,2),
      WART08 numeric(14,2),
      WART03 numeric(14,2),
      WART05 numeric(14,2),
      WART00 numeric(14,2),
      VAT00 numeric(14,2),
      WARTZW numeric(14,2),
      VATZW numeric(14,2),
      WARTPOZ numeric(14,2),
      VATPOZ numeric(14,2),
      VATV22 numeric(14,2),
      VATV23 numeric(14,2),
      VATV12 numeric(14,2),
      VATV07 numeric(14,2),
      VATV08 numeric(14,2),
      VATV03 numeric(14,2),
      VATV05 numeric(14,2),
      VATVPOZ numeric(14,2),
      VATN numeric(14,2),
      VATE22 numeric(14,2),
      VATE23 numeric(14,2),
      VATE12 numeric(14,2),
      VATE07 numeric(14,2),
      VATE08 numeric(14,2),
      VATE03 numeric(14,2),
      VATE05 numeric(14,2),
      VATE00 numeric(14,2),
      VATEZW numeric(14,2),
      VATEPOZ numeric(14,2),
      VATN22 numeric(14,2),
      VATN23 numeric(14,2),
      VATN12 numeric(14,2),
      VATN07 numeric(14,2),
      VATN08 numeric(14,2),
      VATN03 numeric(14,2),
      VATN05 numeric(14,2),
      VATNPOZ numeric(14,2),
      VATGR varchar(5) CHARACTER SET UTF8                           )
   as
declare variable NETV numeric(14,2);
declare variable VATV numeric(14,2);
declare variable VATE numeric(14,2);
declare variable DOCREF integer;
declare variable SQL varchar(5048);
declare variable VATREGTYPE integer; /* <<PR73894: PL>> */
declare variable print integer;
begin
  if(typ = -1) then
    typ = 0; -- jak ktos wykasuje wpis w combo
  ref = 0;
  lp = 0;
  if(zakres is null) then zakres = 0;
  if(zpopmies is null) then zpopmies = 0;
  --BS08473
  sql = 'select B.REF, b.transdate, b.docdate, B.vatreg, b.vnumber, b.bkreg, b.number, B.NIP, b.contractor, b.symbol, v.vatregtype'; --<<PR73894: PL>>
  sql = sql ||' from bkdocs B join VATREGS V on (B.vatreg = V.symbol and B.company = V.company) join BKDOCTYPES T on (B.doctype = t.ref)';
  sql = sql ||' where B.vatperiod = '''||periodid||''' and b.status > 0';
--<<PR73894: PL
  sql = sql ||' and (V.VATREGTYPE in ('||:zakup||',2)
                  or (V.VATREGTYPE = 3
                    and exists(select first 1 1 from bkvatpos join grvat on (grvat.symbol = bkvatpos.taxgr) where bkvatpos.bkdoc = B.ref and grvat.vatregtype in ('||:zakup||',2))))';
  if(coalesce(typ,0)>0) then
  begin
    if(coalesce(zakup,0)>0)then
    begin
      sql = sql ||' and v.vtype = '||
      case typ
        when 1 then '3' --Zakupy krajowe
        when 2 then '4' --Dostawy importowe
        when 3 then '5' --WNT
        when 4 then '6' --Usługi z importu
      end;
    end else
    begin
      sql = sql ||' and v.vtype = '||
      case typ
        when 1 then '0' --Sprzedaż krajowa
        when 2 then '1' --WDT
        when 3 then '2' --Sprzedaż eksportowa
        when 4 then '5' --WNT
        when 5 then '6' --Usugi z importu
      end;
    end
  end
-->>
  if(coalesce(zakres,0)=1)
    then sql = sql ||' and t.creditnote = 0';
  else if(coalesce(zakres,0)=2)then
    sql = sql ||' and t.creditnote = 1';
  if(coalesce(zpopmies,0)>0)then
    sql = sql ||' and (b.vatperiod <> b.period)';
  if(coalesce(vreg,'')<>'')then
    sql = sql ||' and b.vatreg = '''||:vreg||'''';
  sql = sql ||' and B.company ='||:company||'  order by B.vatreg, b.vnumber';
  for execute statement :sql
    into :docref, :transdate, :docdate, :vatreg, :vatregnr,:bkreg, :bkregnr,
      :nip, :kontrahkod, :docsymbol, :vatregtype   --<<PR73894: PL>>
  do begin
    wartbru = 0;
    wartnet = 0;
    wartvatn = 0;
    WART22 = 0;
    VATV22 = 0;
    VATN22 = 0;
    VATE22 = 0;
    WART23 = 0;
    VATV23 = 0;
    VATE23 = 0;
    VATN23 = 0;
    WART12 = 0;
    VATV12 = 0;
    VATE12 = 0;
    VATN12 = 0;
    WART07 = 0;
    VATV07 = 0;
    VATE07 = 0;
    VATN07 = 0;
    WART08 = 0;
    VATV08 = 0;
    VATE08 = 0;
    VATN08 = 0;
    WART03 = 0;
    VATV03 = 0;
    VATE03 = 0;
    VATN03 = 0;
    WART05 = 0;
    VATV05 = 0;
    VATE05 = 0;
    VATN05 = 0;
    WART00 = 0;
    VAT00 = 0;
    VATE00 = 0;
    WARTZW = 0;
    VATZW = 0;
    vatezw = 0;
    WARTPOZ = 0;
    VATPOZ = 0;
    VATVPOZ = 0;
    VATEPOZ = 0;
    VATNPOZ = 0;
    print = 0;
    for select vatgr, sum(netv), sum(vatv), sum(vate), sum(vate)-sum(vatv) as vatn
      from BKVATPOS
        join GRVAT on (GRVAT.SYMBOL = BKVATPOS.TAXGR)--<<PR73894: PL>>
      where bkdoc = :docref
        and (debittype = :vatdebittype or :vatdebittype = 4)
        and (:vatregtype in (:zakup,2) or (:vatregtype = 3 and (grvat.vatregtype in (:zakup,2))))--<<PR73894: PL>>
      group by vatgr
      into :vatgr, :netv, :vatv, :vate, :vatn
    do begin
      print = 1;
      if(vatgr = '22') then begin
        wart22 = wart22 + netv;
        vatv22 = vatv22 + vatv;
        vate22 = vate22 + vate;
        vatn22 = vate22 - vatv22;
      end else if(vatgr = '23') then begin
        wart23 = wart23 + netv;
        vatv23 = vatv23 + vatv;
        vate23 = vate23 + vate;
        vatn23 = vate23 - vatv23;
      end else if(vatgr = '12') then begin
        wart12 = wart12 + netv;
        vatv12 = vatv12 + vatv;
        vate12 = vate12 + vate;
        vatn12 = vate12 - vatv12;
      end else if(vatgr = '07') then begin
        wart07 = wart07 + netv;
        vatv07 = vatv07 + vatv;
        vate07 = vate07 + vate;
        vatn07 = vate07 - vatv07;
      end else if(vatgr = '08') then begin
        wart08 = wart08 + netv;
        vatv08 = vatv08 + vatv;
        vate08 = vate08 + vate;
        vatn08 = vate08 - vatv08;
      end else if(vatgr = '03') then begin
        wart03 = wart03 + netv;
        vatv03 = vatv03 + vatv;
        vate03 = vate03 + vate;
        vatn03 = vate03 - vatv03;
      end else if(vatgr = '05') then begin
        wart05 = wart05 + netv;
        vatv05 = vatv05 + vatv;
        vate05 = vate05 + vate;
        vatn05 = vate05 + vatv05;
      end else if(vatgr = '00') then begin
        wart00 = wart00 + netv;
        vate00 = vate00 + vate;
      end else if(vatgr = 'ZW') then begin
        WARTZW = wartZW + netv;
        VATZW = vatZW + vate;
      end else begin
        wartpoz = wartpoz + netv;
        vatvpoz = vatvpoz + vatv;
        vatepoz = vatepoz + vate;
        vatnpoz = vatepoz  - vatvpoz;
      end
    end
    wartvate = vate22 + vate23 + vate12 + vate07 + vate08 + vate03 + vate05 + vatepoz;
    wartvatv = vatv22 + vatv23 + vatv12 + vatv07 + vatv08 + vatv03 + vatv05 + vatvpoz;
    wartvatn = wartvate - wartvatv;
    wartnet = wart22 + wart23 + wart12 + wart07 + wart08 + wart03 + wart05 + wart00 + wartZW + wartpoz;
    wartbru = wartvate + wartnet;

    lp = lp + 1;
    ref = ref + 1;
    vatv22 = vatv22 + vatv23;
    vatv07 = vatv07 + vatv08;
    vatv03 = vatv03 + vatv05;
    vate22 = vate22 + vate23;
    vate07 = vate07 + vate08;
    vate03 = vate03 + vate05;
    vatn22 = vate22 - vatv22;
    vatn07 = vate07 - vatv07;
    vatn03 = vate03 - vatv03;
    wart22 = wart22 + wart23;
    wart07 = wart07 + wart08;
    wart03 = wart03 + wart05;
    if (print = 1) then
      suspend;
  end
end^
SET TERM ; ^
