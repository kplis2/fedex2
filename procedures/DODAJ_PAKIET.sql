--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DODAJ_PAKIET(
      KTM varchar(40) CHARACTER SET UTF8                           ,
      REFN integer,
      REFP integer,
      WERSJA integer,
      POZTYP char(1) CHARACTER SET UTF8                           ,
      ILOSCZA numeric(14,4),
      ILOSCPAK numeric(14,4),
      GRATIS integer,
      CENNIK integer,
      RABAT numeric(14,4),
      PROMOREF integer)
  returns (
      STATUS integer)
   as
declare variable cenacen numeric(14,4);
begin
  status = 0;
  if(:cennik=0) then cennik = NULL;
  cenacen = 0; -- domyslnie wstaw 0 i nie przeliczaj
  if(:gratis=0) then cenacen = NULL; -- przelicz cene uatomatycznie na pozycji dokumentu
  if (poztyp = 'F') then begin
    if (not exists(select ref from pozfak where refdopakiet = :refp and frompromoref = :promoref and  ktm = :ktm and wersjaref = :wersja and opk = :gratis and refcennik = :cennik and rabat = :rabat)) then begin
      insert into pozfak(dokument, ktm, wersjaref, cenacen, rabat, ilosc, opk, refdopakiet, ilosczadopakiet, refcennik, frompromoref)
        values(:refn, :ktm, :wersja, :cenacen, :rabat, :iloscpak, :gratis, :refp, :iloscza, :cennik, :promoref);
      status = 1;
    end
    else begin
      update pozfak P set P.ilosc = P.ilosc + :iloscpak where P.refdopakiet = :refp;
      status = 1;
    end
  end
  else if (poztyp = 'M') then begin
    if (not exists(select ref from dokumpoz where refdopakiet = :refp and frompromoref = :promoref and ktm = :ktm and wersjaref = :wersja and opk = :gratis and refcennik = :cennik and rabat = :rabat)) then begin
      insert into dokumpoz(dokument, ktm, wersjaref, cenacen, rabat, ilosc, opk, refdopakiet, ilosczadopakiet, refcennik, frompromoref)
        values(:refn, :ktm, :wersja, :cenacen, :rabat, :iloscpak, :gratis, :refp, :iloscza, :cennik, :promoref);
      status = 1;
    end
    else begin
      update dokumpoz P set P.ilosc = P.ilosc + :iloscpak where P.refdopakiet = :refp;
      status = 1;
    end
  end
  else if (poztyp = 'Z') then begin
    if (not exists(select ref from pozzam where refdopakiet = :refp and ktm = :ktm and wersjaref = :wersja and opk = :gratis and refcennik = :cennik and rabat = :rabat)) then begin
      insert into pozzam(zamowienie, ktm, wersjaref, cenacen, rabat, ilosc, opk, refdopakiet, ilosczadopakiet, refcennik, frompromoref)
        values(:refn, :ktm, :wersja, :cenacen, :rabat, :iloscpak, :gratis, :refp, :iloscza, :cennik, :promoref);
      status = 1;
    end
    else begin
      update pozzam P set P.ilosc = P.ilosc + :iloscpak where P.refdopakiet = :refp;
      status = 1;
    end
  end
  suspend;
end^
SET TERM ; ^
