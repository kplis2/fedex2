--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMPOZ_CHANGE(
      POZDOKUM integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      CENAMAG numeric(14,4),
      DOSTAWA integer,
      ILOSC numeric(14,5),
      BLOKPOZNALICZ integer,
      BLOKOBLNAG smallint)
   as
declare variable mag_typ char(1);
declare variable mag char(3);
declare variable wydania integer;
declare variable dokument integer;
declare variable typ_dok char(3);
declare variable fifo char(1);
declare variable i numeric(14,5);
declare variable dok_ilosc numeric(14,4);
declare variable minus_stan smallint;
declare variable magbreak integer;
declare variable dokmagbreak integer;
declare variable zamowienie integer;
declare variable serialized smallint;
declare variable ilserial numeric(14,4);
declare variable wersjaref integer;
declare variable autoserrozpisz smallint;
declare variable opk smallint;
declare variable opktryb smallint; /* 0 - bez, 1 - naliczanie stanow, 2 - rozliczanei stanow */
declare variable slodef integer;
declare variable slopoz integer;
declare variable cenasnet numeric(14,4);
declare variable cenasbru numeric(14,4);
declare variable wartsnet numeric(14,2);
declare variable wartsbru numeric(14,2);
declare variable bn char(1);
declare variable vat numeric(14,4);
declare variable stanopkref integer;
declare variable kortopoz integer;
declare variable org_ilosc numeric(14,4);
declare variable cena_koszt numeric(14,4);
declare variable nieobrot smallint;
declare variable fake smallint;
begin
  /* ilosc to roznica na dokumencie. Czy dopisac do stanow, czy zdjac,
  decyduje rodzaj dokumentu */
  select DF.NIEOBROT, DP.fake
    from DOKUMPOZ DP
    left join DOKUMNAG D on (DP.dokument = D.REF)
    left join DEFDOKUM DF on (D.TYP = DF.symbol)
  where DP.ref = :pozdokum  into :nieobrot, :fake;
  if((nieobrot is not null and nieobrot = 1 ) or fake = 1) then exit;
  select dokument, ilosc, serialized, OPK, cenanet,cenabru, wartsnetto, wartsbrutto, stanopkroz, kortopoz, fromzam, cena_koszt from
      DOKUMPOZ where REF=:pozdokum into :dokument, :dok_ilosc, :serialized, :opk, :cenasnet, :cenasbru, :wartsnet, :wartsbru, :stanopkref, :kortopoz, :zamowienie, :cena_koszt;
  if(:stanopkref is null) then stanopkref = 0;
  select MAGAZYN, typ, slodef, slopoz, magbreak from DOKUMNAG where ref = :dokument into :mag, :typ_dok, :slodef, :slopoz, :dokmagbreak;
  if(:dokmagbreak is null) then dokmagbreak = 0;
  if(:zamowienie is null or (:zamowienie = 0)) then
    select zamowienie  from DOKUMNAG  where ref = :dokument into :zamowienie;
  if(:zamowienie is null) then zamowienie = 0;
  select WYDANIA, OPAK , KAUCJABN from DEFDOKUM where SYMBOL=:typ_dok into :wydania, :opktryb,:bn;
  if(:slodef is null or (:slodef = 0) or (:slopoz is null) or (:slopoz = 0)) then opktryb = 0;
  if(:opktryb > 0) then begin
    select VAT.STAWKA from VAT join TOWARY on (TOWARY.KTM = :ktm and VAT.GRUPA = TOWARY.VAT) into :vat;
  end
  if(:vat is null) then vat = 0;
  select TYP, FIFO, STANYCEN from DEFMAGAZ where SYMBOL=:mag into :mag_typ, :fifo, :minus_stan;
  magbreak = null;
  select MAGBREAK from TOWARY where KTM=:ktm into :magbreak;
  if(:magbreak is null) then
    execute procedure GETCONFIG('MAGBREAK') returning_values :magbreak;
  if(:magbreak is null) then magbreak = 0;
  if(:dokmagbreak>:magbreak) then magbreak = :dokmagbreak;
  select WERSJE.REF, TOWARY.serautowyd from WERSJE join TOWARY on (TOWARY.KTM = WERSJE.KTM) where WERSJE.KTM = :ktm and WERSJE.nrwersji = :wersja into :wersjaref, :autoserrozpisz;
  if(:ktm is null or (:ktm = '') or (:wersjaref  is null)) then exception DOKUMPOZ_BRAKKTM;
  if(:wersja is null or (:wersja < 0)) then exception DOKUMPOZ_BRAKWERSJA;
  if(:mag_typ = 'P' and :wydania = 0 and ((:dostawa is null) or (:dostawa = 0)) and :kortopoz is null) then exception DOKUMPOZ_BRAKDOSTAWY;
  if(:dostawa is null) then dostawa = 0;
  if(:magbreak=2 and :mag_typ = 'P' and :wydania = 1 and ((:dostawa is null) or (:dostawa = 0)) and :kortopoz is null) then
    exception DOKUMPOZ_BRAKDOSTAWY 'Towar '||:ktm||' wymaga wskazania dostawy. Akceptacja niemożliwa.';
  if(:serialized = 1) then begin
    select sum(ilosc) from DOKUMSER where REFPOZ = :POZDOKUM and TYP='M' into :ilserial;
    if(:ilserial is null) then ilserial = 0;
    if(:ilserial <> :dok_ilosc) then begin
      if(:autoserrozpisz = 1 and :ilserial < :dok_ilosc) then begin
        i = :dok_ilosc - :ilserial;
        execute procedure SERNR_AUTOROZpisz(:pozdokum,'M',:mag, :wersjaref,:i);
        select sum(ilosc) from DOKUMSER where REFPOZ = :POZDOKUM and TYP='M' into :ilserial;
      end
      if(:ilserial <> :dok_ilosc) then
        exception DOKUMPOZ_ILSERIALWRONG;
    end
  end
  org_ilosc = :ilosc;
  /*korekta ilosciowa - generowanie rozpisek na podstawie rozpisek z pozycji korygowanej - korygowanie wydania*/
  if(:kortopoz > 0 and (:wydania = 0 or :serialized = 1)) then begin
    execute procedure DOKUMPOZ_CHANGE_DO_CORRECTION(:kortopoz, :pozdokum, :ktm, :wersja, :ilosc, :serialized, :blokoblnag, :blokpoznalicz, :cenamag, :dostawa, :mag_typ, :mag, :fifo, :minus_stan, :magbreak, :zamowienie, :wersjaref, :opk, :opktryb, :slodef, :slopoz, :cenasnet, :cenasbru, :bn, :vat, :stanopkref, :org_ilosc, :dok_ilosc, :cena_koszt);
  /* przyjecie towaru - dokument przychodowy*/
  end else if(:ilosc > 0 and :wydania=0) then begin
    /*rozpisywanie niezależnie od typu magazynu*/
    execute procedure DOKUMPOZ_CHANGE_INC_RECEPTION(:kortopoz, :pozdokum, :ktm, :wersja, :ilosc, :serialized, :blokoblnag, :blokpoznalicz, :cenamag, :dostawa, :mag_typ, :mag, :fifo, :minus_stan, :magbreak, :zamowienie, :wersjaref, :opk, :opktryb, :slodef, :slopoz, :cenasnet, :cenasbru, :bn, :vat, :stanopkref, :org_ilosc, :dok_ilosc, :cena_koszt);
  end
  /* wycofanie przyjcia towaru - zdjecie rozpisek o tej samej cenie magazynowej i parti*/
   else if(:ilosc < 0 and :wydania = 0)then begin
    execute procedure DOKUMPOZ_CHANGE_DEC_RECEPTION(:kortopoz, :pozdokum, :ktm, :wersja, :ilosc, :serialized, :blokoblnag, :blokpoznalicz, :cenamag, :dostawa, :mag_typ, :mag, :fifo, :minus_stan, :magbreak, :zamowienie, :wersjaref, :opk, :opktryb, :slodef, :slopoz, :cenasnet, :cenasbru, :bn, :vat, :stanopkref, :org_ilosc, :dok_ilosc, :cena_koszt);
  end
  /* dokument rozchodzowy - zwikszenie rozchodu*/
  else if(:ilosc > 0 and :wydania = 1) then begin
    execute procedure DOKUMPOZ_CHANGE_INC_DELIVERY(:kortopoz, :pozdokum, :ktm, :wersja, :ilosc, :serialized, :blokoblnag, :blokpoznalicz, :cenamag, :dostawa, :mag_typ, :mag, :fifo, :minus_stan, :magbreak, :zamowienie, :wersjaref, :opk, :opktryb, :slodef, :slopoz, :cenasnet, :cenasbru, :bn, :vat, :stanopkref, :org_ilosc, :dok_ilosc, :cena_koszt);
  end
  else if(:ilosc < 0 and :wydania = 1) then begin
  /*================== zmiejszenie rozchodu -cofniecie rozpisek */
    execute procedure DOKUMPOZ_CHANGE_DEC_DELIVERY(:kortopoz, :pozdokum, :ktm, :wersja, :ilosc, :serialized, :blokoblnag, :blokpoznalicz, :cenamag, :dostawa, :mag_typ, :mag, :fifo, :minus_stan, :magbreak, :zamowienie, :wersjaref, :opk, :opktryb, :slodef, :slopoz, :cenasnet, :cenasbru, :bn, :vat, :stanopkref, :org_ilosc, :dok_ilosc, :cena_koszt);
  end
  /* dokument rozpisany, trzeba uzgodnić wartosci na pozycjach - magazyny typu C i P */

  /*else begin
    dokument rozpisywany recznie - zamagazynowanie lub odmagazynowanie rozpisek
          wykonywane w dokum_ack
  end*/
end^
SET TERM ; ^
