--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_GET_LISTYWYSDBOXREF(
      REF INTEGER_ID)
  returns (
      SYMBOLDOK STRING40)
   as
begin
  select first 1 ld.symbol
    from listywysd ld
    where ld.ref = :ref
  into :symboldok;
  if (symboldok is null) then symboldok = '';
  suspend;
end^
SET TERM ; ^
