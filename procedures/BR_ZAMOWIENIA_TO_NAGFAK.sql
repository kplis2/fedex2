--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_ZAMOWIENIA_TO_NAGFAK(
      REFFAK integer)
  returns (
      REF integer,
      SYMBOL varchar(40) CHARACTER SET UTF8                           ,
      REJESTR varchar(3) CHARACTER SET UTF8                           )
   as
declare variable grupadok integer;
declare variable org_ref integer;
begin
  select GRUPADOK from NAGFAK where ref=:reffak into :grupadok;
-- zamówienia powiazane bezposrednio z fakturą
  for select distinct NAGZAM.REF, NAGZAM.ID, NAGZAM.org_ref, NAGZAM.REJESTR
    from NAGZAM
      join NAGFAK on (NAGZAM.FAKTURA = NAGFAK.REF)
    where NAGFAK.GRUPADOK = :grupadok
    into :ref, :symbol, :org_ref, :rejestr
  do begin
    if(:org_ref <> :ref) then begin
      select REF, ID, REJESTR from NAGZAM where ref=:org_ref into :ref, :symbol, :rejestr;
    end
    suspend;
  end
-- faktury powiązane z dok. mag. zamówienia
  for
    select distinct nagzam.ref, nagzam.id, nagzam.org_ref, nagzam.rejestr
      from NAGFAK N
        join DOKUMNAG on (DOKUMNAG.FAKTURA = N.REF)
        join NAGZAM on (DOKUMNAG.zamowienie = NAGZAM.REF)
      where n.ref = :reffak
      into :ref, :symbol, :org_ref, :rejestr
  do begin
    suspend;
  end
-- zamówienia powiązane poprzez pozycje z pozycjami faktur
  for select distinct n.REF, n.id, n.org_ref, n.rejestr
     from nagzam n
       join pozzam p on (p.zamowienie = n.ref)
       join pozfak pf on (pf.frompozzam = p.ref)
       join nagfak nf on (nf.ref = pf.dokument)
     where nf.grupadok = :grupadok
     into :ref, :symbol, :org_ref, :rejestr
  do begin
    suspend;
  end
end^
SET TERM ; ^
