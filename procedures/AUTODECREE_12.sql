--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_12(
      BKDOC integer)
   as
declare variable account varchar(20);
declare variable period varchar(6);
declare variable yearid smallint;
declare variable monthid smallint;
declare variable amount numeric(14,2);
declare variable settlement varchar(20);
declare variable curr_dt numeric(14,2);
declare variable curr_ct numeric(14,2);
declare variable dt numeric(14,2);
declare variable ct numeric(14,2);
declare variable winien numeric(14,2);
declare variable ma numeric(14,2);
declare variable winienzl numeric(14,2);
declare variable mazl numeric(14,2);
declare variable descript varchar(80);
declare variable kontofk varchar(20);
declare variable symbfak varchar(20);
declare variable currency varchar(3);
declare variable tmp varchar(80);
declare variable todo_currdt numeric(14,2);
declare variable todo_currct numeric(14,2);
declare variable todo_dtrate numeric(14,4);
declare variable todo_ctrate numeric(14,4);
declare variable done_currdt numeric(14,2);
declare variable done_currct numeric(14,2);
declare variable acc_currdt numeric(14,2);
declare variable acc_currct numeric(14,2);
declare variable rp_kurs numeric(14,4);
declare variable rp_ref integer;
declare variable ref_dt integer;
declare variable ref_ct integer;
declare variable dt_symbol varchar(20);
declare variable ct_symbol varchar(20);
declare variable i smallint;
declare variable r_winien numeric(14,2);
declare variable r_ma numeric(14,2);
declare variable tmp_descript varchar(80);
declare variable done smallint;
declare variable rate_date integer;
declare variable company integer;
begin

  -- schemat dekretowania - ksiegowanie różnic kursowych

  select B.descript, B.period, B.company
    from bkdocs B
    where B.ref=:bkdoc
    into :tmp_descript, :period, :company;


  yearid = cast(substring(period from 1 for 4) as smallint);
  monthid = cast(substring(period from 5 for 2) as smallint);
  if (monthid = 12) then
  begin
    yearid = yearid + 1;
    monthid = 1;
  end else
    monthid = monthid + 1;

  for
    select R.kontofk, R.symbfak, R.winien, R.ma, R.waluta
      from rozrach R
      where R.walutowy = 1 and R.winienzl - R.mazl <> 0 and R.company = :company
      into :kontofk, :symbfak, :r_winien, :r_ma, :currency
  do begin
    descript = tmp_descript;

    dt = 0; ct = 0; curr_dt = 0; curr_ct = 0;
    done_currdt = 0; done_currct = 0;

    todo_currdt = 1; todo_currct = 1; -- zeby warunek petli przy pierwszym przebiegu byl spelniony
    while (todo_currdt <> 0 and todo_currct <> 0) do
    begin
      todo_currdt = 0; todo_currct = 0;
      acc_currdt = 0; acc_currct = 0;
      done = 0;
      execute procedure getconfig('RATE_DIFIRENT_DATE') returning_values :rate_date;
      for
        select BD.symbol, RP.winien, RP.ma, RP.winienzl, RP.mazl, RP.ref, RP.kurs
          from rozrachp RP
            left join fsclracch fscl on(rp.fsclracch=fscl.ref)
            left join bkdocs BD on (RP.bkdoc = BD.ref)
          where RP.kontofk = :kontofk and RP.symbfak = :symbfak and RP.company = :company and
           (CASE WHEN :rate_date=1 then coalesce(fscl.regdate,RP.data) else RP.data end) < cast(:yearid || '/' || :monthid || '/1' as timestamp)
          order by (CASE WHEN :rate_date=1 then coalesce(fscl.regdate,RP.data) else RP.data end), RP.ref
          into :tmp, :winien, :ma, :winienzl, :mazl, :rp_ref, :rp_kurs
      do begin
        if (winien < 0) then acc_currct = acc_currct - winien; -- zwiekszenie ma
          else acc_currdt = acc_currdt + winien; -- zwiekszenie winien
        if (ma < 0) then acc_currdt = acc_currdt - ma; -- zwiekszenie winien
          else acc_currct = acc_currct + ma; -- zwiekszenie ma

        if ((winien-ma) > 0 and todo_currdt = 0 and acc_currdt > done_currdt) then
        begin
          todo_currdt = acc_currdt - done_currdt;
          ref_dt = rp_ref;
          todo_dtrate = rp_kurs;
          dt_symbol = tmp;
        end

        if ((ma-winien) > 0 and todo_currct = 0 and acc_currct > done_currct) then
        begin
          todo_currct = acc_currct - done_currct;
          ref_ct = rp_ref;
          todo_ctrate = rp_kurs;
          ct_symbol = tmp;
        end

        if (todo_currdt > 0 and todo_currct > 0 and done = 0) then
        begin
          done = 1;
          -- sprawdzam czy juz czasem nie bylo rozliczone
          select count(*)
            from rozrachp
            where dtratediff = :ref_dt and ctratediff = :ref_ct
            into :i;
          if (i = 0) then
          begin
            -- rozliczaj
            if (dt_symbol is null) then
              dt_symbol = '';
            if (ct_symbol is null) then
              ct_symbol = '';

            descript = substring(dt_symbol || ' ' || ct_symbol || ' :  ' from 1 for 80);

            if (todo_currdt > todo_currct) then
            begin
              done_currct = done_currct + todo_currct;
              done_currdt = done_currdt + todo_currct;
              amount = todo_currct * (todo_ctrate - todo_dtrate);
              descript = substring(descript || cast(todo_currct as varchar(25)) || ' * (' || cast(todo_ctrate as varchar(25)) || '-' || cast(todo_dtrate as varchar(25)) || ')' from 1 for 80);
            end else
            begin
              done_currct = done_currct + todo_currdt;
              done_currdt = done_currdt + todo_currdt;
              amount = todo_currdt * (todo_ctrate - todo_dtrate);
              descript = substring(descript || cast(todo_currdt as varchar(25)) || ' * (' || cast(todo_ctrate as varchar(25)) || '-' || cast(todo_dtrate as varchar(25)) || ')' from 1 for 80);
            end
            descript = substring(descript || tmp_descript from 1 for 80);

            if (amount > 0) then
            begin
              account = kontofk;
              settlement = symbfak;
              execute procedure insert_decree_currsettlement(bkdoc, account, 0, amount, null, null, currency, descript, settlement, null, 9, null, null, null, null, null, null, ref_dt, ref_ct, 0,0);

              if (account starting with '203') then
                account = '750-03';
              else
                account = '750-06';
              execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

            end else
            begin
              amount = -1 * amount;
              account = kontofk;
              settlement = symbfak;
              execute procedure insert_decree_currsettlement(bkdoc, account, 1, amount, null, null, currency, descript, settlement, null, 9, null, null, null, null, null, null, ref_dt, ref_ct, 0,0);

              if (account starting with '203') then
                account = '755-06';
              else
                account = '755-07';
              execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);
            end
          end else
          begin
           if (todo_currdt > todo_currct) then
            begin
              done_currct = done_currct + todo_currct;
              done_currdt = done_currdt + todo_currct;
            end else
            begin
              done_currct = done_currct + todo_currdt;
              done_currdt = done_currdt + todo_currdt;
            end
          end
        end
      end
    end
  end
end^
SET TERM ; ^
