--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TOWARY_KTM_CHECK(
      KTM varchar(40) CHARACTER SET UTF8                           )
  returns (
      FREE integer)
   as
declare variable cnt integer;
begin
  FREE = 0;
  cnt = 0;
  if(exists(select first 1 1 from ANALDOSTP where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from CENNIKIZEWN where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from CPLNAGRODY where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from DOKUMNOTP where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from DOKUMPOZ where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from DOSTCEN where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from GRUPYKUPOFE where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from INWENTAP where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from KPLNAG where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from KPLPOZ where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from NAGZAM where KKTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from OFERPOZ where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from POZFAK where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from POZZAM where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from PROMOCJE where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from PROMOCT where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from SPOSDOST where USLUGA = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from SRVPOSITIONS where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from STANYCEN where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from STANYIL where KTM = :ktm and ILOSC <> 0)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from STANYOPK where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from STANYREZ where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from STKRPOZ where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from TOWAKCES where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from TOWAKCES where AKTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from TOWJEDN where OPKKTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from TOWPLIKI where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  if(exists(select first 1 1 from ZLECPOZ where KTM = :ktm)) then cnt = 1;
  if(:cnt > 0) then exit;
  FREE = 1;

end^
SET TERM ; ^
