--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_FAK_CREATE(
      ZAM integer,
      STANOW varchar(20) CHARACTER SET UTF8                           ,
      REJ char(3) CHARACTER SET UTF8                           ,
      ODDZIAL varchar(10) CHARACTER SET UTF8                           ,
      TYPFAK char(3) CHARACTER SET UTF8                           ,
      DATA timestamp,
      DATASPRZ timestamp,
      HISTZAM integer,
      GRUPAFAK integer,
      RODZAJFAK smallint,
      COPYWYS smallint,
      GETDOKMAG integer,
      FAKDEPEND integer,
      FAKNOJOIN integer,
      FAKOPERATOR integer,
      FAKZALICZKA integer,
      FAKREAL integer,
      FAKNOCUMUL integer,
      FAKNAPLATNIKA integer)
  returns (
      STATUS integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      REFFAK integer)
   as
declare variable OKRESFAK varchar(6);
declare variable RABAT decimal(14,2);
declare variable NUMER integer;
declare variable KTM varchar(40);
declare variable WERSJA integer;
declare variable ILOSC decimal(14,4);
declare variable ILOSCO decimal(14,4);
declare variable ILOSCM decimal(14,4);
declare variable ZILOSC decimal(14,4);
declare variable ZILOSCO decimal(14,4);
declare variable ZILOSCM decimal(14,4);
declare variable JEDN integer;
declare variable JEDNO integer;
declare variable CENACEN decimal(14,4);
declare variable RABATP decimal(14,2);
declare variable RABATTAB decimal(14,2);
declare variable KOSZTDOST decimal(14,2);
declare variable VAT decimal(14,2);
declare variable BN char(1);
declare variable WGCEN char(1);
declare variable MOTH integer;
declare variable SPOSDOST integer;
declare variable KTM_USLUGA varchar(40);
declare variable ACKFAK integer;
declare variable OPERATOR integer;
declare variable KLIENT integer;
declare variable DOSTAWCA integer;
declare variable SPOSZAP integer;
declare variable TERMIN integer;
declare variable SPRZEDAWCA integer;
declare variable WALUTA varchar(3);
declare variable WALUTOWA integer;
declare variable KURS numeric(14,4);
declare variable REFPOZ integer;
declare variable WALCEN varchar(3);
declare variable DOKMAGCNT integer;
declare variable POZZAMREF integer;
declare variable MAGAZYN varchar(3);
declare variable OPK smallint;
declare variable CNT integer;
declare variable NIEWAZNE integer;
declare variable FAKDATA timestamp;
declare variable TYP smallint;
declare variable DOKMAGREF integer;
declare variable DOKPOZREF integer;
declare variable ORG_REF integer;
declare variable SKAD integer;
declare variable FROMZAM integer;
declare variable OBLICZCENZAK integer;
declare variable ZAKUPU integer;
declare variable FLAGI varchar(255);
declare variable OPIS varchar(1024);
declare variable GRUPADOK integer;
declare variable ZAMREFFAK integer;
declare variable ZAMSYMBOL varchar(30);
declare variable ZALICZKOWY smallint;
declare variable KLIENTZAM integer;
declare variable PLATNIKZAM integer;
declare variable PARAMN1 numeric(14,4);
declare variable PARAMN2 numeric(14,4);
declare variable PARAMN3 numeric(14,4);
declare variable PARAMN4 numeric(14,4);
declare variable PARAMN5 numeric(14,4);
declare variable PARAMN6 numeric(14,4);
declare variable PARAMN7 numeric(14,4);
declare variable PARAMN8 numeric(14,4);
declare variable PARAMN9 numeric(14,4);
declare variable PARAMN10 numeric(14,4);
declare variable PARAMN11 numeric(14,4);
declare variable PARAMN12 numeric(14,4);
declare variable PARAMS1 varchar(255);
declare variable PARAMS2 varchar(255);
declare variable PARAMS3 varchar(255);
declare variable PARAMS4 varchar(255);
declare variable PARAMS5 varchar(255);
declare variable PARAMS6 varchar(255);
declare variable PARAMS7 varchar(255);
declare variable PARAMS8 varchar(255);
declare variable PARAMS9 varchar(255);
declare variable PARAMS10 varchar(255);
declare variable PARAMS11 varchar(255);
declare variable PARAMS12 varchar(255);
declare variable PARAMD1 timestamp;
declare variable PARAMD2 timestamp;
declare variable PARAMD3 timestamp;
declare variable PARAMD4 timestamp;
declare variable GR_VAT varchar(10);
declare variable RSLODEF integer;
declare variable RSLOPOZ integer;
declare variable RWINIEN numeric(14,2);
declare variable RMA numeric(14,2);
declare variable SUMWINIEN numeric(14,2);
declare variable SUMMA numeric(14,2);
declare variable RKONTOFK KONTO_ID;
declare variable RSYMBFAK varchar(100);
declare variable WAGA numeric(15,4);
declare variable SPOSOBLROZRACH smallint;
declare variable TERMZAP timestamp;
declare variable FROMPROMOREF integer;
declare variable ILOSCZADOPAKIET numeric(14,4);
declare variable PREC smallint;
declare variable ALTTOPOZ POZZAM_ID;
declare variable ALTTOPOZN POZFAK_ID;
declare variable FAKE smallint;
declare variable HAVEFAKE POZFAK_ID;
begin
  skad = 1;
  if(:faknojoin is null) then faknojoin = 0;
  if(:faknojoin > 0) then BEGIN
    skad = 0;
  end
  if(:faknocumul is null) then faknocumul = 0;
  /*kuba - podczepienie fromzam zawsze, bo inaczej nei dziala realizacja zamowienia: 15.08.2004*/
  fromzam = :zam;
  if(:data is null) then data = current_date;
  if(:rodzajfak is null) then rodzajfak = 0;
  if(:fakdepend is null) then fakdepend = 0;
  select FAKTURA, SYMBFAK,RABAT, KOSZTDOST, SPOSDOST, MAGAZYN, TYP, ORG_REF, OPERATOR, KLIENT, PLATNIK, BN
  from NAGZAM where REF=:ZAM
  into :reffak, :symbol,:rabat, :kosztdost, :sposdost, :MAGAZYN,  :typ, :org_ref, :operator, :klientzam, :platnikzam, wgcen;
  select TYPFAK.zakup, OBLICZCENZAK, ZALICZKOWY, SPOSOBLROZRACH from TYPFAK where symbol = :typfak into :zakupu, :obliczcenzak, :zaliczkowy, :sposoblrozrach;
  if(:sposoblrozrach is null) then sposoblrozrach = 0;
  if(:reffak > 0 or (:symbol <> '' and symbol is not null)) then begin
    /*istnieje juz faktura*/
    STATUS = -2;
    REFFAK = 0;
    exit;
  end
  if(:faknaplatnika=1 and platnikzam is not null) then klientzam = :platnikzam;
  STATUS= -1;
  moth = extract(month from :data);
  okresfak = cast( extract(year from :data) as char(4));
  if(:fakoperator is null or :fakoperator<>1) then begin
    select HISTZAM.operator from HISTZAM where REF=:histzam into :operator;
  end
  if(:operator is null) then operator = 0;
  if(:moth < 10) then begin
    okresfak = okresfak || '0' ||cast(moth as char(1) );
  end else
    okresfak = okresfak || cast(moth as char(2));
  if(:datasprz is null) then datasprz = :data;
  if(:stanow is null or (:stanow = ''))then
    exception OPERFAK_NOSTANOW;

  if (exists(select first 1 1 from pozzam where zamowienie = :zam and fake = 1) and
      exists(select first 1 1 from stantypfak where stansprzed = :stanow and typfak = :typfak and coalesce(altallow,0) = 0)) then
    exception FAKE_NOT_ALLOW 'Pozycje aleternatywne nie dozwolone dla stanowiska i typu dokumentu sprzedaży!';

  if(:grupafak > 0) then begin
    /* fakturowanie w obrbie grupy - szukanie faktury pasującej */
    select DOSTAWCA, SPOSZAP, TERMzap, SPRZEDAWCA, RABAT, WALUTA, WALUTOWE,KURS from NAGZAM
      where REF=:zam into
      :dostawca, :sposzap, :termin, :sprzedawca, :rabat, :waluta, :walutowa, :kurs;
    select ref from NAGFAK where
        (STANOWISKO = :stanow or (:stanow is null))AND REJESTR = :rej and TYP = :typfak and ((KLIENT = :klientzam) or (:klientzam is null)) and ((DOSTAWCA = :dostawca) or (:dostawca is null))and
        DATA = :data and DATASPRZ = :datasprz AND SPOSZAP = :sposzap and TERMZAP = :data + :termin and
        (SPRZEDAWCA = :sprzedawca or (:sprzedawca is null)) and RABAT = :rabat and WALUTA = :waluta AND WALUTOWA = :walutowa and KURS = :kurs and
        GRUPAFAK = :grupafak into :reffak;


  end
  if(:reffak is null or (:reffak = 0)) then begin
    /* pobranie generatora*/
     execute procedure GEN_REF('NAGFAK') returning_values :reffak;
    /* zalozenie naglowka faktury */
/*    if(:FAKZALICZKA > 0) then begin
      if(:zaliczkowy is null) then zaliczkowy = 0;
      if(:zaliczkowy = 0) then
        select sum(KWOTANET), sum(KWOTABRU), sum(KWOTANETZL), sum(KWOTABRUZL)
          from NAGFAK_GET_ZALICZKI(:fakzaliczka)
          into :zalkwotanet, :zalkwotabru, :zalkwotanetzl, :zalkwotabruzl;
      refk = :fakzaliczka;
    end*/
    insert into NAGFAK(REF,ODDZIAL,STANOWISKO,REJESTR,TYP,KLIENT,DOSTAWCA,PLATNIK,
              UZYKLI,DATA,DATASPRZ,SPOSZAP,TERMZAP,TERMIN,
--              PESUMWARTNET, PESUMWARTBRU, PESUMWARTNETZL, PESUMWARTBRUZL,
              SUMWARTNET, SUMWARTBRU, PSUMWARTNET, PSUMWARTBRU,
              RABAT,UWAGI,AKCEPTACJA, /*REFK,*/ SYMBOLK, REFDOKM, HISTORIA,
              WALUTA, TABKURS, KURS, WALUTOWA, --KWOTAZAL,
              SPRZEDAWCA, OPERATOR, GRUPAFAK, SKAD, MAGAZYN,
              TERMDOST, SPOSDOST, TRASADOST, WYSYLKA, ODBIORCA, ODBIORCAID, DULICA, DNRDOMU, DNRLOKALU, DMIASTO, DKODP,
              KATEGA, KATEGB, KATEGC, SPRZEDAWCAZEW, FLAGI, ZLECNAZWA, CINOUT, FROMNAGZAM,  CKONTRAKTY, KOSZTDOSTROZ, KOSZTDOSTBEZ, KOSZTDOSTPLAT, KOSZTDOST,
              ILOSCPACZEK, ILOSCPALET, ILOSCPALETBEZ, FLAGISPED, UWAGISPED, SPOSDOSTAUTO,
              ODBIORCAIDWYDR, ODBIORCAWYDR, DULICAWYDR, DMIASTOWYDR, DKODPWYDR, KWOTAZAL, SRVREQUEST)
       select :reffak,:oddzial,:stanow,:rej,:typfak,:klientzam,DOSTAWCA,:platnikzam,
              UZYKLI,:data,:DATASPRZ,SPOSZAP,:data  +TERMZAP,TERMZAP,
--              :zalkwotanet, :zalkwotabru, :zalkwotanetzl, :zalkwotabruzl,
              0,0,0,0,
              RABAT,UWAGI,0,/*NULL,*/NULL,NULL,:histzam,
              WALUTA, TABKURS,KURS, WALUTOWE,--:zalkwotabruzl,
              SPRZEDAWCA,:operator, :grupafak,:skad, :MAGAZYN, 
              TERMDOST, SPOSDOST, TRASADOST, WYSYLKA * :copywys, ODBIORCA, ODBIORCAID, DULICA, DNRDOMU, DNRLOKALU, DMIASTO, DKODP,
              KATEGA, KATEGB, KATEGC, SPRZEDAWCAZEW, FLAGI, ZLECNAZWA, CINOUT, :zam, CKONTRAKTY, KOSZTDOSTROZ, KOSZTDOSTBEZ, KOSZTDOSTPLAT, KOSZTDOST,
              ILOSCPACZEK, ILOSCPALET, ILOSCPALETBEZ, FLAGISPED, UWAGISPED, SPOSDOSTAUTO,
              ODBIORCAIDWYDR, ODBIORCAWYDR, DULICAWYDR, DMIASTOWYDR, DKODPWYDR, SUMAZALICZEKZL - KWOTAFAKZALNIEROZ, SRVREQUEST
         from NAGZAM where ref=:zam;
    INSERT INTO NAGFAKTERMPLAT (DOKTYP, DOKREF, TERMZAP, PROCENTPLAT, SPOSPLAT)
      select 'F', :reffak, TERMZAP, PROCENTPLAT, SPOSPLAT from NAGFAKTERMPLAT where DOKTYP='Z' and DOKREF=:zam;

  end
  if(:rodzajfak = 0)then begin
    select BN, SYMBOL from NAGFAK where NAGFAK.REF =:reffak into :BN, :SYMBOL;
    if (wgcen is null) then
      execute procedure GETCONFIG('WGCEN') returning_values :wgcen;
    if(:wgcen is null) then wgcen = 'N';
    for select ref, numer, ktm, wersja, cenacen, rabat, rabattab, WALCEN,
      ilreal, ILREALO, ILREALM,
      ILOSC, ILOSCO, ILOSCM,
      JEDN, JEDNO, MAGAZYN, opk, FLAGI, OPIS, GR_VAT,
      POZZAM.paramn1, POZZAM.paramn2, POZZAM.paramn3, POZZAM.paramn4,
      POZZAM.paramn5, POZZAM.paramn6, POZZAM.paramn7, POZZAM.paramn8,
      POZZAM.paramn9, POZZAM.paramn10, POZZAM.paramn11, POZZAM.paramn12,
      POZZAM.params1, POZZAM.params2, POZZAM.params3, POZZAM.params4,
      POZZAM.params5, POZZAM.params6, POZZAM.params7, POZZAM.params8,
      POZZAM.params9, POZZAM.params10, POZZAM.params11, POZZAM.params12,
      POZZAM.paramd1, POZZAM.paramd2, POZZAM.paramd3, POZZAM.paramd4, POZZAM.waga,
      pozzam.termzap, pozzam.frompromoref, pozzam.ilosczadopakiet, pozzam.prec, pozzam.alttopoz, pozzam.fake, pozzam.havefake
      from POZZAM
      where ZAMOWIENIE=:zam and ((:typ < 2 and ilosc > 0) or (:typ > 1 and ilreal > 0))
      order by POZZAM.NUMER, POZZAM.ALTTOPOZ nulls first
      into :pozzamref, :numer, :ktm, :wersja, cenacen,:rabatp,:rabattab, :walcen,
          :ilosc, :ilosco,:iloscm,
          :zilosc, :zilosco, :ziloscm,
          :jedn, :jedno, :magazyn, :opk, :flagi, :opis, :gr_vat,
          :paramn1, :paramn2, :paramn3, :paramn4,
          :paramn5, :paramn6, :paramn7, :paramn8,
          :paramn9, :paramn10, :paramn11, :paramn12,
          :params1, :params2, :params3, :params4,
          :params5, :params6, :params7, :params8,
          :params9, :params10, :params11, :params12,
          :paramd1, :paramd2, :paramd3, :paramd4, :waga,
          :termzap, :frompromoref, :ilosczadopakiet, :prec, :AltToPoz, :fake, :havefake
    do begin
    --jesli generujemy na podstawie nie-realizacji, to ilosci oryginalne
    -- jak na bazie realizacji - to ilosci realizowane
      if((:typ < 2) and (:fakreal = 0)) then begin
        ilosc = :zilosc;
        ilosco = :zilosco;
        iloscm = :ziloscm;
      end
      /*obliczenie wartosci CENACEN*/
      if(:gr_vat is null) then select TOWARY.VAT from TOWARY where KTM = :ktm into :gr_vat;
      if(:bn <> :wgcen) then begin
        select STAWKA from VAT where GRUPA = :gr_vat into :vat;
        if(:bn = 'N') then begin
           /* przeksztacenie z brutto na netto*/
           cenacen = cenacen - cast((cenacen * (vat / 100))/(1 + (vat / 100)) as DECIMAL(14,2));
        end else begin
          cenacen = cenacen + cast(cenacen * (vat / 100) as DECIMAL(14,2));
        end
      end
      refpoz = null;
      /*kumulacja pozycji zamówienia na fakturze*/
      if(:faknocumul=0 and :fake is null and :HaveFake is null) then begin
        for select REF from POZFAK where DOKUMENT = :reffak and KTM = :ktm and WERSJA = :wersja and
           CENACEN = :cenacen and RABAT = :rabat and RABATTAB = :RABATTAB and MAGAZYN = :magazyn and TERMZAP=:termzap into :refpoz
        do begin
          refpoz = :refpoz;
        end
      end
      if(:ilosc > 0) then begin
        if(:refpoz > 0) then begin
          update POZFAK set ILOSC = ILOSC + :ilosc, ILOSCO = ILOSCO + ILOSCO, ILOSCM = ILOSCM+:iloscm where ref = :refpoz;
          update DOKUMPOZ set FROMPOZFAK=:refpoz where DOKUMPOZ.FROMPOZZAM=:pozzamref;
        end else begin
          numer = 0;
          select max(numer) from POZFAK where DOKUMENT = :reffak into :numer;
          if(:numer is null) then numer = 0;
          numer = :numer + 1;
          execute procedure GEN_REF('POZFAK') returning_values :refpoz;
          opis = substring(:opis from 1 for 255);
          AltToPozN = null;
          if (AltToPoz is not null) then
          begin
            select pf.ref
              from pozfak pf
              where pf.frompozzam = :AltToPoz
                and pf.dokument = :reffak     --BS68878
              into :AltToPozN;
          end
          insert into pozfak(REF,DOKUMENT,NUMER,ORD,KTM,WERSJA,ILOSC,GR_VAT,PGR_VAT,CENACEN,RABAT,RABATTAB,WALCEN,
                           ILOSCO, ILOSCM,JEDN,JEDNO,MAGAZYN,
                           PILOSC,PCENACEN,PRABAT,PCENANET,PCENABRU,PWARTNET,PWARTBRU,OPK, FROMPOZZAM, FROMZAM,
                           OBLICZCENZAK, FLAGI, OPIS,
                           paramn1, paramn2, paramn3, paramn4,
                           paramn5, paramn6, paramn7, paramn8,
                           paramn9, paramn10, paramn11, paramn12,
                           params1, params2, params3, params4,
                           params5, params6, params7, params8,
                           params9, params10, params11, params12,
                           paramd1, paramd2, paramd3, paramd4, waga, termzap, frompromoref, ilosczadopakiet, prec, AltToPoz, fake)
           values(:refpoz,:reffak,:numer,1,:ktm,:wersja,:ilosc,:gr_vat,:gr_vat,:cenacen,:rabatp,:rabattab,:walcen,
                  :ILOSCO, :ILOSCM,:jedn, :jedno,:magazyn,
                  0,0,0,0,0,0,0,:OPK, :pozzamref, :fromzam,
                  :OBLICZCENZAK, :flagi, :OPIS,
                  :paramn1, :paramn2, :paramn3, :paramn4,
                  :paramn5, :paramn6, :paramn7, :paramn8,
                  :paramn9, :paramn10, :paramn11, :paramn12,
                  :params1, :params2, :params3, :params4,
                  :params5, :params6, :params7, :params8,
                  :params9, :params10, :params11, :params12,
                  :paramd1, :paramd2, :paramd3, :paramd4,:waga, :termzap, :frompromoref, :ilosczadopakiet, :prec, :AltToPozN, :fake);
        end
        if(:refpoz > 0) then
        -- SERFIX1 --
        execute procedure COPY_DOKUMSER(:pozzamref, :refpoz, :ilosc, 'Z', 'F', 0);
        -- SERFIX1 --
      end
    end
  end else begin
    cnt = 0;
    if(:faknaplatnika<>1) then platnikzam = NULL;
    for select DOKUMNAG.REF, DOKUMNAG.data
       from DOKUMNAG join DEFDOKUM on ( DOKUMNAG.TYP = DEFDOKUM.SYMBOL and (DEFDOKUM.WYDANIA < 2) AND DEFDOKUM.ZEWN = 1)
       where ((:rodzajfak = 1 and DOKUMNAG.historia = :histzam) or
              (:rodzajfak = 2 and DOKUMNAG.ZAMOWIENIE = :zam))
           and (DOKUMNAG.FAKTURA is null or (DOKUMNAG.FAKTURA = 0))
       into :dokmagcnt, :fakdata
    do begin
      execute procedure NAGFAK_FAKTURUJDOKMAG(:reffak,:stanow,:rej,:typfak,:operator,:dokmagcnt, :fakdata, 1,0,0,:platnikzam,:fakzaliczka,0,null) returning_values :niewazne;
      cnt = :cnt + 1;
    end
    if(:cnt = 0) then begin
      status = 2;
      reffak = 0;
      symbol = '';
      exit;
    end
  end
  if(:kosztdost > 0) then begin
    select USLUGA from SPOSDOST where ref=:sposdost into :ktm_usluga;
    if(:ktm_usluga is not null and :ktm_usluga <> '') then
    insert into pozfak(DOKUMENT,NUMER,ORD,KTM,WERSJA,ILOSC,CENACEN,RABAT,
                       PILOSC,PCENACEN,PRABAT,PCENANET,PCENABRU,PWARTNET,PWARTBRU,OPK)
       values(:reffak,:numer+1,1,:ktm_usluga,0,1,:kosztdost,0,
              0,0,0,0,0,0,0,0);

  end

--SB  przepisanie rozliczenia tylko w przypadku kiedy sposoblrozrach < 2

  -- przepisanie rozliczenia zaliczek
  if (:sposoblrozrach < 2) then
  begin
    for select SLODEF, SLOPOZ, SYMBFAK, KONTOFK, sum(WINIEN), sum(MA)
      from ROZRACHROZ
      where DOKTYP = 'Z' and DOKREF = :zam
      group by slodef,slopoz,symbfak,kontofk
      into :rslodef, :rslopoz, :rsymbfak, :rkontofk, :rwinien, :rma
    do begin
      select sum(winien),sum(ma)
      from ROZRACHROZ
      where DOKTYP='F' and SLODEF=:rslodef and SLOPOZ=:rslopoz and KONTOFK=:rkontofk and SYMBFAK=:rsymbfak and DOKREF<>:reffak
      into :sumwinien,:summa;
      if(:sumwinien is null) then sumwinien = 0;
      if(:summa is null) then summa = 0;
      rwinien = :rwinien-:sumwinien;
      rma = :rma-:summa;
      if(:rwinien<>0 or :rma<>0) then
        insert into ROZRACHROZ(DOKTYP, DOKREF, SLODEF, SLOPOZ, SYMBFAK, KONTOFK, WINIEN, MA)
        values ('F',:reffak,:rslodef,:rslopoz,:rsymbfak,:rkontofk,:rwinien,:rma);
    end
  end
--SB

  if(:typ > 1) then
   fakdepend = 0;
  if(:FAKNOJOIN = 0) then begin
    zamreffak = :reffak;
    zamsymbol = :symbol;
  end
  update NAGZAM set FAKTURA=:zamreffak, FAKDEPEND = :fakdepend,  SYMBFAK=:zamsymbol where REF=:zam;
  if(:getdokmag > 0) then begin
    for select dokumnag.ref, NAGZAM.org_ref
    from DOKUMNAG join NAGZAM on (DOKUMNAG.zamowienie = nagzam.ref)
                  join DEFDOKUM on (DOKUMNAG.TYP = DEFDOKUM.SYMBOL)
    where DOKUMNAG.FAKTURA is null and
          DEFDOKUM.ZEWN = 1 and
          DEFDOKUM.koryg = 0 and
          (:getdokmag <> 1 or (NAGZAM.REF = :zam and DOKUMNAG.historia = :histzam)) and
          (:getdokmag <> 2 or (NAGZAM.ref = :zam)) and
          (:getdokmag <> 3 or (nagzam.ref = :zam or (nagzam.org_ref = :zam and nagzam.typ > 1)))
    into :dokmagref, :org_ref
    do begin
      update DOKUMNAG set FAKTURA = :reffak where DOKUMNAG.ref = :dokmagref and FAKTURA is null;
      /*dla każdej pozycji dokumetntu okrel pozycj dok. VAT*/
      for select DOKUMPOZ.REF, DOKUMPOZ.frompozzam, POZFAK.ref
      from DOKUMPOZ left join POZFAK on (DOKUMPOZ.FROMPOZZAM = POZFAK.frompozzam and POZFAK.dokument = :reffak)
      where DOKUMPOZ.DOKUMENT = :dokmagref
      into :dokpozref, :pozzamref, :refpoz
      do begin
          if(:refpoz is null) then begin
            select max(POZFAK.REF)
              from POZFAK join POZZAM  POZORG on (POZFAK.FROMPOZZAM = POZORG.REF and POZORG.zamowienie = :org_ref)
                          join POZZAM  POZYC on (POZORG.REF = POZYC.POPREF)
            where POZYC.REF = :pozzamref
            into :refpoz;
          end
          if(:refpoz > 0) then begin
            update DOKUMPOZ set FROMPOZFAK = :refpoz where DOKUMPOZ.REF = :dokpozref;
          end
      end
    end
  end
  STATUS = 1;
end^
SET TERM ; ^
