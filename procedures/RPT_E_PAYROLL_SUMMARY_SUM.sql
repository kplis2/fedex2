--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_E_PAYROLL_SUMMARY_SUM(
      PAYROLL integer)
  returns (
      NUM1 integer,
      NAME1 varchar(22) CHARACTER SET UTF8                           ,
      VAL1 numeric(14,2),
      REPSUM1 integer,
      NUM2 integer,
      NAME2 varchar(22) CHARACTER SET UTF8                           ,
      VAL2 numeric(14,2),
      REPSUM2 integer,
      NUM3 integer,
      NAME3 varchar(22) CHARACTER SET UTF8                           ,
      VAL3 numeric(14,2),
      REPSUM3 integer,
      NUM4 integer,
      NAME4 varchar(22) CHARACTER SET UTF8                           ,
      VAL4 numeric(14,2),
      REPSUM4 integer,
      NUM5 integer,
      NAME5 varchar(22) CHARACTER SET UTF8                           ,
      VAL5 numeric(14,2),
      REPSUM5 integer)
   as
begin

  name1 = null;
  val1 = null;
  NUM1 = null;
  repsum1 = null;

  val2 = 0;
  select COALESCE(SUM(P.pvalue),0)
    from eprpos P
      join ecolumns C on (C.number = P.ecolumn)
    where P.payroll=:payroll
      and ecolumn = 4000
    group by C.number
    into val2;

  select C.number, C.name, C.repsum
    from ecolumns C
    where C.number = 4000
    into :num2, :name2, :repsum2;

  val3 = 0;
  select COALESCE(SUM(P.pvalue),0)
    from eprpos P
     join ecolumns C on (C.number = p.ecolumn)
    where P.payroll=:payroll
      and ecolumn = 5900
    group by C.number
    into val3;

  select C.number, C.name, C.repsum
    from ecolumns C
    where C.number = 5900
    into :num3, :name3, :repsum3;

  val4 = 0;
  select COALESCE(SUM(P.pvalue),0)
    from eprpos P
      join ecolumns C on (C.number = P.ecolumn)
    where P.payroll=:payroll
      and ecolumn = 8000
    group by C.number
    into val4;

  select C.number, C.name, C.repsum
    from ecolumns C
    where C.number = 8000
    into :num4, :name4, :repsum4;

  val5 = 0;
  select COALESCE(SUM(P.pvalue),0)
    from eprpos p
     join ecolumns C on (C.number = p.ecolumn)
    where ecolumn = 9050 and P.payroll=:payroll
    group by C.number
    into val5;

  select C.number, C.name, C.repsum
    from ecolumns C
    where C.number = 9050
    into :num5, :name5, :repsum5;

  suspend;
end^
SET TERM ; ^
