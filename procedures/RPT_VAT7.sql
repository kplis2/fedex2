--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_VAT7(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      DK39 numeric(14,2))
  returns (
      K19 numeric(14,2),
      K20 numeric(14,2),
      K20A numeric(14,2),
      K22 numeric(14,2),
      K24 varchar(6) CHARACTER SET UTF8                           ,
      K25 numeric(14,2),
      K26 numeric(14,2),
      K27 varchar(6) CHARACTER SET UTF8                           ,
      K28 numeric(14,2),
      K29 numeric(14,2),
      K30 numeric(14,2),
      K31 numeric(14,2),
      K32 numeric(14,2),
      K33 numeric(14,2),
      K37 numeric(14,2),
      K38 numeric(14,2),
      K39 numeric(14,2),
      K40 numeric(14,2),
      K41 numeric(14,2),
      K42 numeric(14,2),
      K43 numeric(14,2),
      K44 numeric(14,2),
      K45 numeric(14,2),
      K46 numeric(14,2),
      K47 numeric(14,2),
      K48 numeric(14,2),
      K49 numeric(14,2),
      K50 numeric(14,2),
      K51 numeric(14,2),
      K52 numeric(14,2),
      K53 numeric(14,2),
      K54 numeric(14,2),
      K55 numeric(14,2),
      K56 numeric(14,2),
      K57 numeric(14,2),
      K58 numeric(14,2),
      K59 numeric(14,2),
      K60 numeric(14,2),
      K61 numeric(14,2),
      K62 numeric(14,2),
      K63 numeric(14,2),
      K64 numeric(14,2),
      K65 numeric(14,2),
      K66 numeric(14,2),
      K67 numeric(14,2),
      K68 numeric(14,2))
   as
declare variable vatgr varchar(6);
  declare variable netv numeric(14,2);
  declare variable vatv numeric(14,2);
  declare variable taxgr varchar(10);
begin
  k19 = 0;
  k37 = 0;
  k38 = 0;
  k39 = :dK39;
  k40 = 0;
  k42 = 0;
  k44 = 0;
  k46 = 0;
  k48 = 0;
  k50 = 0;
  k52 = 0;
  k54 = 0;
  k56 = 0;
  k57 = 0;
  k58 = 0;
  k61 = 0;
  k62 = 0;
  k64 = 0;
  k65 = 0;
  k67 = 0;

  /* sprzedaz */
  for
    select bkvatpos.vatgr, bkvatpos.taxgr, cast(sum(bkvatpos.netv)*100 as integer)/100, cast(sum(bkvatpos.vatv)*100 as integer)/100
    from bkdocs
    join vatregs on (vatregs.symbol=bkdocs.vatreg and vatregs.vtype=0)
    join bkvatpos on (bkvatpos.bkdoc=bkdocs.ref)
    where bkdocs.vatperiod=:period and bkdocs.status>0
    group by bkvatpos.vatgr, bkvatpos.taxgr
    into :vatgr, :taxgr, :netv, :vatv
  do begin
    if (:vatgr='22') then
    begin
      k32 = :netv;
      k33 = :vatv;
    end
    else if (:vatgr='07') then
    begin
      k30 = :netv;
      k31 = :vatv;
    end
    else if (:vatgr='ZW') then
    begin
      k19 = :netv;
    end
    else if (:vatgr='00') then
    begin
      if (taxgr='E21') then
      begin
        k20a = netv;
      end else
        k22 = :netv;
    end
    else if (:k27 is null) then
    begin
      k27 = :vatgr;
      k28 = :netv;
      k29 = :vatv;
    end
    else if (:k24 is null) then
    begin
      k24 = :vatgr;
      k25 = :netv;
      k26 = :vatv;
    end
    else
      exception test_break;
    k37 = :k37 + :netv;
    k38 = :k38 + :vatv;
  end

  /* jeszcze eksport*/
  select cast(sum(bkvatpos.netv)*100 as integer)/100
    from bkdocs
    join vatregs on (bkdocs.vatperiod=:period and vatregs.symbol=bkdocs.vatreg and vatregs.vtype=1 and bkdocs.status>0)
    join bkvatpos on (bkvatpos.bkdoc=bkdocs.ref)
    into :k20;

  if (k20 is not null) then
    k20 = k20 + k20a;
  else
    k20 = k20a;

  /* zakupy nie rozliczone w poprzednich miesiacach

  tu byl blad zgloszony przez p.Grzegorowskiego
  for
    select bkvatpos.taxgr, cast(sum(bkvatpos.netv)*100 as integer)/100, cast(sum(bkvatpos.vatv)*100 as integer)/100
    from bkdocs
    join vatregs on (bkdocs.period<>bkdocs.vatperiod and bkdocs.vatperiod=:period and vatregs.symbol=bkdocs.vatreg and vatregs.vtype>1 and bkdocs.status>0)
    join bkvatpos on (bkvatpos.bkdoc=bkdocs.ref)
    group by bkvatpos.taxgr
    into :taxgr, :netv, :vatv
  do begin
    if (:taxgr = 'I11') then
    begin
      k41 = :netv;
      k42 = :vatv;
    end
    else if (:taxgr = 'I21') then
    begin
      k43 = :netv;
      k44 = :vatv;
    end
    else if (:taxgr = 'P11') then
    begin
      k45 = :netv;
      k46 = :vatv;
    end
    else if (:taxgr = 'P21') then
    begin
      k47 = :netv;
      k48 = :vatv;
    end
    else
      exception fk_vat7_unknown_taxgr;
  end
*/

  /* zakupy z tego miesiaca*/
  for
    select  bkvatpos.taxgr, cast(sum(bkvatpos.netv)*100 as integer)/100, cast(sum(bkvatpos.vatv)*100 as integer)/100
    from bkdocs
    join vatregs on (bkdocs.vatperiod=:period and vatregs.symbol=bkdocs.vatreg and vatregs.vtype>1 and bkdocs.status>0)
    join bkvatpos on (bkvatpos.bkdoc=bkdocs.ref and bkvatpos.debited=1)
    group by bkvatpos.taxgr
    into :taxgr, :netv, :vatv
  do begin
    if (:taxgr = 'I11') then
    begin
      k49 = :netv;
      k50 = :vatv;
    end
    else if (:taxgr = 'I21') then
    begin
      k51 = :netv;
      k52 = :vatv;
    end
    else if (:taxgr = 'P11') then
    begin
      k53 = :netv;
      k54 = :vatv;
    end
    else if (:taxgr = 'P21') then
    begin
      k55 = :netv;
      k56 = :vatv;
    end
    else
      exception fk_vat7_unknown_taxgr;
  end

  if (:k44+:k52<>0) then
    exception test_break;

  if (k19>0) then
  begin
    if (k55 is null) then k55 = 0;
    if (k53 is not null and k53<>0) then
      k55 = k55 + k53;
    if (k56 is null) then k56 = 0;
    if (k54 is not null and k54<>0) then
      k56 = k56 + k54;
    k53 = 0;
    k54 = 0;
  end else
  begin
    if (k53 is null) then k53 = 0;
    if (k55 is not null and k55<>0) then
      k53 = k53 + k55;
    if (k54 is null) then k54 = 0;
    if (k56 is not null and k56<>0) then
      k54 = k54 + k56;
    k55 = 0;
    k56 = 0;
  end

  if (:k19<0) then
    k58 = :k48 + :k56;
  else
    if (:k37 <> 0) then
      k58 = (:k48 + :k56)*(:k37 - :k19)/:k37;

  k59 = :k39 + :k40 + :k42 + :k46 + :k50 + :k54 + :k57 + :k58;

  if (:k38>:k59) then
    k60 = :k38 - :k59;
  else
    k60 = 0;

  k63 = :k60 - :k61 - :k62;

  if (:k59>:k38) then
    k64 = :k59 - :k38;
  else
    k64 = 0;

  k66 = :k64 + :k65;

  k68 = :k66 - :k67;

  if (:k39 = 0) then
    k39 = null;
  if (:k40 = 0) then
    k40 = null;
  if (:k42 = 0) then
    k42 = null;
  if (:k46 = 0) then
    k46 = null;
  if (:k48 = 0) then
    k48 = null;
  if (:k50 = 0) then
    k50 = null;
  if (:k53 = 0) then
    k53 = null;
  if (:k55 = 0) then
    k55 = null;
  if (:k56 = 0) then
    k56 = null;
  if (:k57 = 0) then
    k57 = null;
  if (:k58 = 0) then
    k58 = null;
  if (:k61 = 0) then
    k61 = null;
  if (:k62 = 0) then
    k62 = null;
  if (:k65 = 0) then
    k65 = null;
  if (:k67 = 0) then
    k67 = null;

  suspend;
end^
SET TERM ; ^
