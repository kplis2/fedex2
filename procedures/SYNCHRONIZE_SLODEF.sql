--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYNCHRONIZE_SLODEF(
      PATTERN BKACCOUNTS_ID,
      DEST BKACCOUNTS_ID,
      INTERNAL SMALLINT_ID = 0)
   as
declare variable vnazwa varchar(20);
declare variable vtyp slotyp_id;
declare variable vpredef smallint;
declare variable vcrm smallint;
declare variable vkasa smallint;
declare variable vprefixfk konto_id;
declare variable vgridname string40;
declare variable vgridnamedict string40;
declare variable vformname string40;
declare variable vfirma smallint;
declare variable vtrybred smallint;
declare variable vmag smallint;
declare variable vopak smallint;
declare variable vkartoteka smallint;
declare variable vanalityka smallint;
declare variable vkodln smallint;
declare variable vstate state;
declare variable vtoken smallint;
declare variable visdist smallint_id;
declare variable vmastertable stable_id;
declare variable vlogistyka smallint_id;
declare variable vpersonel smallint_id;
declare variable vbkbrowsewindow string40;
declare variable vprodukcja smallint_id;
declare variable vsymbol string40;
declare variable vdictposfield string40;
declare variable vkodfield string40;
declare variable vnazwafield string40;
declare variable vkodksfield string40;
declare variable vnipfield string40;
declare variable viscompany smallint_id;
declare variable vmultidist smallint;
begin
  -- Procedura synchronizująca dwa slodefy. Flaga internal = 1 powoduje uruchomienie upodatu bez kontroli praw przy
  -- sychronizacji - powinno być stosowane tylko dla wzorcowego planu kont.
  select nazwa, typ, predef, crm, kasa, prefixfk, gridname, gridnamedict, formname, firma,
    trybred, mag, opak, kartoteka, analityka, kodln, state, token, isdist, mastertable,
    logistyka, personel, bkbrowsewindow, produkcja, symbol, dictposfield, kodfield, nazwafield,
    kodksfield, nipfield, iscompany, multidist
  from slodef s
  where s.ref = :pattern
  into :vnazwa, :vtyp, :vpredef, :vcrm, :vkasa, :vprefixfk, :vgridname, :vgridnamedict, :vformname, :vfirma,
    :vtrybred, :vmag, :vopak, :vkartoteka, :vanalityka, :vkodln, :vstate, :vtoken, :visdist, :vmastertable,
    :vlogistyka, :vpersonel, :vbkbrowsewindow, :vprodukcja, :vsymbol, :vdictposfield, :vkodfield, :vnazwafield,
    :vkodksfield, :vnipfield, :viscompany, :vmultidist;
  update slodef
    set nazwa = :vnazwa, typ = :vtyp, predef = :vpredef, crm = :vcrm, kasa = :vkasa, prefixfk = :vprefixfk, gridname = :vgridname, gridnamedict = :vgridnamedict, formname = :vformname, firma = :vfirma,
      trybred = :vtrybred, mag = :vmag, opak = :vopak, kartoteka = :vkartoteka, analityka = :vanalityka, kodln = :vkodln, state = :vstate, token = :vtoken, isdist = :visdist, mastertable = :vmastertable,
      logistyka = :vlogistyka, personel = :vpersonel, bkbrowsewindow = :vbkbrowsewindow, produkcja = :vprodukcja, symbol = :vsymbol, dictposfield = :vdictposfield, kodfield = :vkodfield, nazwafield = :vnazwafield,
      kodksfield = :vkodksfield, nipfield = :vnipfield, iscompany = :viscompany, multidist = :vmultidist, internalupdate = :internal
    where ref = :dest;
end^
SET TERM ; ^
