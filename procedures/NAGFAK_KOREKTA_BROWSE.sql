--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_KOREKTA_BROWSE(
      NAGFAKKOR integer)
  returns (
      NAGFAK integer)
   as
declare variable kontrahent integer;
begin
  select case when zakup=1 then dostawca else klient end
    from nagfak where ref =:nagfakkor
  into :kontrahent;
  if (kontrahent is null) then
    exception universal 'Brak wskazania kontrahenta na nagłówku korekty';
  for select distinct n.ref
    from nagfak n
    left join nagfak nk on (n.korekta = nk.ref)
    where (n.korekta is null or nk.anulowanie > 0)
      and n.nieobrot = 0 and n.anulowanie = 0 and n.akceptacja in (1,8)
      and n.ref <> :nagfakkor
      and (n.klient = :kontrahent or n.dostawca = :kontrahent)
    into :nagfak
  do begin
    suspend;
  end
end^
SET TERM ; ^
