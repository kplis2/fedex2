--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NOTIFY_WINDYKACJA(
      DNIPRZETERMKON integer,
      DNIMIEDZYKON integer,
      KONTOFK KONTO_ID = '',
      PREFIX varchar(10) CHARACTER SET UTF8                            = 'KONTAKTW')
  returns (
      ID varchar(20) CHARACTER SET UTF8                           ,
      PARENTID varchar(20) CHARACTER SET UTF8                           ,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           ,
      NUMVAL numeric(14,2),
      WATCHBELOW smallint,
      COLORINDEX integer,
      PRIORITY integer,
      ICON varchar(255) CHARACTER SET UTF8                           ,
      MODULE varchar(255) CHARACTER SET UTF8                           ,
      ACTIONID varchar(255) CHARACTER SET UTF8                           ,
      ACTIONPARAMS varchar(255) CHARACTER SET UTF8                           ,
      ACTIONNAME varchar(255) CHARACTER SET UTF8                           ,
      RIGHTSGROUP varchar(255) CHARACTER SET UTF8                           ,
      RIGHTS varchar(255) CHARACTER SET UTF8                           ,
      COMPANY integer,
      ROZRACHREF integer,
      SLODEF integer,
      SLOPOZ integer)
   as
declare variable dni integer;
declare variable planpayday timestamp;
declare variable datakon timestamp;
declare variable dnikon integer;
declare variable sumval numeric(14,2);
declare variable przeterm numeric(14,2);
declare variable cpodmiot integer;
begin
  -- parametry
  -- DNIPRZETERMKON - minimalna ilosc dni przeterminowania rozrachunku
  -- DNIMIEDZYKON - ilosc dni w przeciagu ktorych nie powtarzamy kontaktu windykacyjnego

  -- klienci przeznaczeni do kontaktu windykacyjnego
  watchbelow = 0;
  colorindex = 2;
  priority = 1;
  icon = 'MI_USERS';
  module = 'EFK';
  rightsgroup = '';
  rights = '';
  for select ref
  from companies
  into :company
  do begin
    sumval = 0;
    for select slodef, slopoz, max(slonazwa), max(dni), min(planpayday), min(REF), sum(SALDOWM)
    from ROZRACH
    where SALDOWM>0 and DNI>:dniprzetermkon and TYP='N' and PLANPAYDAY<current_date
    and company = :company and slodef>0 and slopoz>0
    and (KONTOFK like :kontofk||'%' or :kontofk='')
    group by slodef,slopoz,company
    into :slodef, :slopoz, :descript, :dni, :planpayday, :rozrachref, :przeterm
    do begin
      cpodmiot = NULL;
      select first 1 ref from cpodmioty where slodef=:slodef and slopoz=:slopoz into :cpodmiot;
      if(:cpodmiot is not null) then begin
        datakon = NULL;
        select max(k.data) from kontakty k
        join ctypykon t on t.ref=k.rodzaj
        where k.cpodmiot=:cpodmiot and t.windykacja=1
        into :datakon;
        if(:datakon is not null) then 
          dnikon = current_timestamp(0) - :datakon;
        else
          dnikon = 1000;
      end else begin
        dnikon = 1000;
      end
      if(:dnikon>=:dnimiedzykon) then begin
        id = :prefix||:slodef||:slopoz;
        parentid = :prefix;
        val = :przeterm;
        numval = :przeterm;
        sumval = :sumval + :numval;
        actionid = 'BrowseFkRozrachFromNOTIFSTATE';
        actionparams = 'BRS_SETTLEMENTS_DICTDEF='||:slodef||';BRS_SETTLEMENTS_DICTPOS='||:slopoz;
        actionname = 'Pokaż rozrachunki';
        suspend;
      end
    end
    -- naglowek drzewa
    descript = 'Klienci przeterm. pow. '||:dniprzetermkon||' dni';
    id = :prefix;
    parentid = '';
    icon = 'MI_KONTAKTY';
    val = :sumval;
    numval = :sumval;
    actionid = 'BrowseFkRozrach2';
    actionparams = '';
    actionname = 'Rozrachunki wg kontrahentów';
    slodef = null;
    slopoz = null;
    suspend;
  end
end^
SET TERM ; ^
