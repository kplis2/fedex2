--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_OO_SALDOBMWN(
      COMPANY integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      ACCOUNTS ACCOUNT_ID)
  returns (
      AMOUNT numeric(15,2))
   as
declare variable YEARID integer;
declare variable DEBIT numeric(15,2);
declare variable CREDIT numeric(15,2);
declare variable ACCOUNTING_REF integer;
declare variable SALDO_DEBIT numeric(14,2);
declare variable COUNTRY_CURRENCY varchar(3);
declare variable BALANCETYPE smallint;
declare variable SACCOUNT ACCOUNT_ID;
begin
  select yearid from bkperiods where id = :period and company = :company
    into :yearid;
  accounts = replace(accounts,  '?',  '_') || '%';
  amount = 0;
  execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
    returning_values country_currency;

  for
    select A.ref, B.saldotype, A.account
      from accounting A
        join bkaccounts B on (A.bkaccount = B.ref)
      where A.currency = :country_currency and A.yearid = :yearid
        and A.account like :accounts
      into :accounting_ref, :balancetype, :saccount
  do begin
    select sum(debit), sum(credit) from turnovers
      where accounting = :accounting_ref and period = :period and company = :company
      into :debit, :credit;

    if (debit is null) then debit = 0;
    if (credit is null) then credit = 0;

    saldo_debit = 0;
    if (balancetype = 0) then
      saldo_debit = debit - credit;
    else if (balancetype = 1 and abs(debit) > abs(credit)) then
      saldo_debit = debit - credit;

    amount = amount + saldo_debit;
  end
  amount = coalesce(amount,0);
  suspend;
end^
SET TERM ; ^
