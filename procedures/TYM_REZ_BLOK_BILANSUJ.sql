--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TYM_REZ_BLOK_BILANSUJ as
declare variable magazyn varchar(3);
declare variable wersjaref integer;
declare variable pozzam1 integer;
declare variable status1 char(1);
declare variable zreal1 smallint;
declare variable dokummag1 integer;
declare variable data timestamp;
declare variable datad timestamp;
declare variable iloscmag numeric(14,4);
declare variable pozzam integer;
declare variable status char(1);
declare variable zreal smallint;
declare variable dokummag integer;
declare variable numer integer;
begin
  update stanyrez set numer = null;
  for select magazyn, wersjaref
    from stanyrez
    where status = 'R' or status = 'Z' or status = 'D' and zreal <> 1
    group by magazyn, wersjaref
    into :magazyn, :wersjaref
  do begin
    pozzam1 = null;
    select min(data) from stanyrez where wersjaref = :wersjaref and magazyn = :magazyn
    and status = 'D' and zreal <> 1 into :datad;
    select min(data) from stanyrez where wersjaref = :wersjaref and magazyn = :magazyn
    and (status = 'R' or status = 'Z') into :data;
    if(datad is not null and (data is null or data is not null and datad <= data)) then begin
      select first 1 s.pozzam, s.status, s.zreal, s.dokummag from stanyrez s where
        s.wersjaref = :wersjaref and s.magazyn = :magazyn and s.status = 'D' and s.zreal <> 1 and s.data = :datad
        into :pozzam1, :status1, :zreal1, :dokummag1;
    end else if (data is not null) then begin
      select first 1 s.pozzam, s.status, s.zreal, s.dokummag from stanyrez s where
        s.wersjaref = :wersjaref and s.magazyn = :magazyn and (s.status = 'R' or status = 'Z') and s.data = :data
        into :pozzam1, :status1, :zreal1, :dokummag1;
    end
    if(pozzam1 is not null) then begin
      numer = 1;
      update stanyrez s set s.numer = 1 where s.pozzam = :pozzam1 and s.zreal = :zreal1 and s.status = :status1 and s.dokummag = :dokummag1;
      for select s.pozzam, s.status, s.zreal, s.dokummag from stanyrez s
            where (s.status = 'Z' or s.status = 'R' or s.zreal <> 1 and s.status = 'D') and (s.numer is null or s.numer <> 1) and
                  s.wersjaref = :wersjaref and s.magazyn = :magazyn
            order by s.data, s.status desc
          into :pozzam, :status, :zreal, :dokummag
      do begin
        numer = numer +1;
        update stanyrez s set s.numer = :numer where s.pozzam = :pozzam and s.zreal = :zreal and s.status = :status and s.dokummag = :dokummag;
      end
    end
  end
end^
SET TERM ; ^
