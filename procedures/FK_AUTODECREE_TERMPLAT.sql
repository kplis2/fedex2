--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_AUTODECREE_TERMPLAT(
      BKDOC integer,
      ACCOUNT ACCOUNT_ID,
      AMOUNT numeric(15,2),
      CURRAMOUNT numeric(15,2),
      RATE numeric(15,4),
      CURRENCY varchar(3) CHARACTER SET UTF8                           ,
      SIDE smallint,
      SETTLEMENT varchar(20) CHARACTER SET UTF8                           ,
      OREF integer,
      OTABLE varchar(40) CHARACTER SET UTF8                           ,
      FSOPERTYPE smallint,
      DOCDATE timestamp,
      PAYDAY timestamp,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      DONLYTERMZAP smallint = 0,
      BANKACCOUNT integer = null)
  returns (
      SUMPROCENTPLAT numeric(14,2))
   as
  declare variable numer smallint;
  declare variable procentplat numeric(14,2);
  declare variable procamount numeric(14,2);
  declare variable sumprocamount numeric(14,2);
  declare variable currprocamount numeric(14,2);
  declare variable currsumprocamount numeric(14,2);
  declare variable settlementnext varchar(20);
  declare variable termzap timestamp;
  declare variable country_currency varchar(3);
begin
  numer = 1;
  sumprocentplat = 0;
  sumprocamount = 0;
  execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
      returning_values :country_currency;
  if(coalesce(:currency,'') = '') then
    currency = country_currency;
  -- pobranie terminu platnosci oraz procentu jaki ma byc splacony. Dla kazdego z nich
  -- jest odpowiednio dodawany settlement
  for
    select termzap, sum(procentplat)
      from nagfaktermplat
      where nagfak = :oref and termzap is not null and procentplat > 0
      group by termzap
      order by termzap
      into :termzap, :procentplat
  do begin
    -- sprawdzamy czy ogolna suma procentow nie przekroczyla 100
    sumprocentplat = sumprocentplat + procentplat;
    if (sumprocentplat < 100) then  begin
      procamount = procentplat * amount / 100;
      currprocamount = procentplat * curramount / 100;
    end else begin-- jezeli tak czesciowa zaplate liczymy ze wzoru (kwota - naliczone_dotychczas)
      procamount = amount - sumprocamount;
      currprocamount = curramount - currsumprocamount;
    end


    sumprocamount = sumprocamount + procamount;
    currsumprocamount = currsumprocamount + currprocamount;

    select settlementout from reg_settlement_name_create(:settlement, :numer) into :settlementnext;
    if(:currency <> :country_currency) then
      execute procedure insert_decree_currsettlement(bkdoc, account, side, procamount, currprocamount, rate, currency, descript,
        :settlementnext, null, fsopertype, 1, docdate, :termzap, null, otable, oref, null, null, bankaccount, 0);
    else
      execute procedure insert_decree_settlement(bkdoc, account, side, procamount, descript,
        :settlementnext, null, fsopertype, 1, docdate, :termzap, null, otable, oref, bankaccount, 0);
    numer = numer + 1;
  end

  if (amount <> sumprocamount and (donlytermzap = 0 or (sumprocamount > 0) ) ) then
  begin
    if (sumprocentplat = 100) then --zostaly roznice groszowe
      if(:currency <> :country_currency) then
        execute procedure insert_decree_currsettlement(bkdoc, account, side, amount - sumprocamount, curramount - currsumprocamount, rate, currency,
        descript,:settlementnext, null, fsopertype, 0, docdate, :termzap, null, otable, oref, null,null,bankaccount,  1);
      else
        execute procedure insert_decree_settlement(bkdoc, account, side, amount - sumprocamount, descript,
          :settlementnext, null, fsopertype, 0, docdate, :termzap, null, otable, oref, bankaccount, 1);
    else --zaksiegowanie pozostalej czesci wedlug daty platnosci z naglowka faktury
    begin
      select settlementout from reg_settlement_name_create(:settlement, :numer) into :settlementnext;
      if(:currency <> :country_currency) then
        execute procedure insert_decree_currsettlement(bkdoc, account, side, amount - sumprocamount, curramount - currsumprocamount, rate, currency,
          descript, :settlementnext, null, fsopertype, 1, docdate, payday, null, otable, oref, null,null,bankaccount, 0);
      else
        execute procedure insert_decree_settlement(bkdoc, account, side, amount - sumprocamount, descript,
          :settlementnext, null, fsopertype, 1, docdate, payday, null, otable, oref, bankaccount, 0);
      sumprocentplat = 100;
    end
  end
end^
SET TERM ; ^
