--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_UCPTAX(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(16,2))
   as
declare variable taxyear integer;
declare variable taxscale numeric(14,2);
declare variable przychod numeric(14,2);
declare variable lumpsumtax smallint;
declare variable tper varchar(6);
declare variable payday date;
declare variable person integer;
declare variable zus numeric(14,2);
declare variable koszty numeric(14,2);
declare variable nfz numeric(14,2);
declare variable prevtax numeric(14,2);
begin
--MW: stary algorytm: ret = #ROUND(18 * #ROUND(#COL(300) - #COL(650) - #COLSUM(610,613), 0)/100 - #COL(721), 0);

  select PR.tper, PR.payday, PR.lumpsumtax
    from epayrolls PR join emplcontracts C
      on (C.ref=PR.emplcontract) join econtrtypes T
      on (T.contrtype = C.econtrtype)
    where PR.ref=:payroll
    into :tper, :payday, :lumpsumtax;

  taxyear = cast(substring(:tper from 1 for 4) as integer);

  select min(taxscale)
    from etaxlevels
    where taxyear = :taxyear
    into :taxscale;

  if (taxscale is null) then
    exception NO_TAX_LEVELS_FOR_THIS_YEAR;

  if (lumpsumtax = 1) then
  begin
  -- podatek ryczatowy 18% przychodu
  --Zryczałtowany 18% podatek pobiera się od przychodu bez pomniejszania go o koszty uzyskania.
  --Ponadto u osób, u których umowa zlecenia podlega oskładkowaniu, podstawa opodatkowania nie może
  --być pomniejszona o potrącone przez płatnika składki na ubezpieczenia społeczne,
  --a podatek nie jest pomniejszany o składkę na ubezpieczenie zdrowotne.

    execute procedure EF_COL(employee, payroll,'EF_', 3000)
      returning_values przychod;

    ret = round(taxscale * przychod / 100.00);
    exit;
  end else
  begin
    select person
      from employees
      where ref = :employee
    into :person;

    select sum(P.pvalue)
      from eprpos P
        join epayrolls R on (P.payroll = R.ref)
        join eprempl E on (E.epayroll = P.payroll and E.employee = P.employee)
      where E.person = :person and R.tper = :tper and P.ecolumn = 3000
        and (R.ref = :payroll or R.corlumpsumtax = :payroll)
      into :przychod;

    przychod = coalesce(przychod, 0);

    select sum(P.pvalue)
      from eprpos P
        join epayrolls R on (P.payroll = R.ref)
        join eprempl E on (E.epayroll = P.payroll and E.employee = P.employee)
      where E.person = :person and R.tper = :tper and P.ecolumn = 6500
        and (R.ref = :payroll or R.corlumpsumtax = :payroll)
      into :koszty;

    koszty = coalesce(koszty, 0);

    select sum(P.pvalue)
      from eprpos P
        join epayrolls R on (P.payroll = R.ref)
        join eprempl E on (E.epayroll = P.payroll and E.employee = P.employee)
      where E.person = :person and R.tper = :tper and P.ecolumn >= 6100 and P.ecolumn <= 6130
        and (R.ref = :payroll or R.corlumpsumtax = :payroll)
      into :zus;

    zus = coalesce(zus, 0);

    select sum(P.pvalue)
      from eprpos P
        join epayrolls R on (P.payroll = R.ref)
        join eprempl E on (E.epayroll = P.payroll and E.employee = P.employee)
      where E.person = :person and R.tper = :tper and P.ecolumn = 7210
        and (R.ref = :payroll or R.corlumpsumtax = :payroll)
      into :nfz;

    nfz = coalesce(nfz, 0);

    select sum(P.pvalue)
      from eprpos P
        join epayrolls R on (P.payroll = R.ref)
        join eprempl E on (E.epayroll = P.payroll and E.employee = P.employee)
      where E.person = :person and R.tper = :tper
        and P.ecolumn = 7350 and R.corlumpsumtax = :payroll
      into :prevtax;

    prevtax = coalesce(prevtax, 0);

    ret = round(taxscale * round(przychod - koszty - zus)/100 - nfz) - prevtax;
  end
  suspend;
end^
SET TERM ; ^
