--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WF_GETFIELDVALUEASNUMBER(
      WFINSTANCEREF WFINSTANCE_ID,
      WFFIELDNAME STRING60,
      WFFIELDUUID NEOS_ID = null)
  returns (
      WFFIELDVALUE NUMERIC_14_4)
   as
declare variable "FOUND" SMALLINT_ID;
begin
  found = 0;

  select 1,replace(wf.fieldvalue,',','.') from wffield wf
  where wf.wfinstanceref=:wfinstanceref
    and (:wffielduuid is null or (:wffielduuid is not null and wf.uuid=:wffielduuid))
    and (:wffieldname is null or (:wffieldname is not null and wf.fieldname=:wffieldname))
  into :found,  :wffieldvalue;

  if(found=1)then
    suspend;
end^
SET TERM ; ^
