--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NORMALIZE_YMD(
      SYEARS smallint,
      SMONTHS smallint,
      SDAYS smallint)
  returns (
      NEW_SYEARS smallint,
      NEW_SMONTHS smallint,
      NEW_SDAYS smallint)
   as
begin
  --DS: funkcja uzywana do normalizacji liczby dni, misiecy, lat w sytuacji gdy dodajemy nieciagle okresy
  while (sdays >= 30)
  do begin
    smonths = smonths + 1;
    sdays = sdays - 30;
  end
  while (sdays < 0)
  do begin
    smonths = smonths - 1;
    sdays = sdays + 30;
  end

  while (smonths >= 12)
  do begin
    syears = syears + 1;
    smonths = smonths - 12;
  end
  while (smonths < 0)
  do begin
    syears = syears - 1;
    smonths = smonths + 12;
  end

  new_syears = :syears;
  new_smonths = :smonths;
  new_sdays = :sdays;

  suspend;
end^
SET TERM ; ^
