--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SEGREGATORY_TREE(
      REFSEGREGATOR integer)
  returns (
      SEG integer)
   as
declare variable PARENTSEG integer;
begin
  for select s.ref, s.rodzic from segregator s
  where s.ref = :refsegregator
  into :seg, :parentseg
  do begin
    suspend;
    for select seg from SEGREGATORY_TREE(:parentseg) into :seg
    do begin
      suspend;
    end
  end
end^
SET TERM ; ^
