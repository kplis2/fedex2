--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWSORD_SET_CARTS_LOCATION(
      MWSORD MWSORDS_ID,
      EMWSCONSTLOC SYMBOL_ID)
   as
declare variable CARTSYMB SYMBOL_ID;
declare variable outdoc smallint_id;
declare variable out smallint_id;
declare variable wh2 defmagaz_id;
begin
  for
    select l.symbol, d.wydania, d.zewn, d.mag2
      from mwsordcartcolours c
        left join mwsconstlocs l on (c.cart = l.ref)
        left join dokumnag d on (c.docid = d.ref)
      where c.mwsord = :mwsord
        and c.status = 0
    into :cartsymb, :outdoc, :out, :wh2
  do begin
--TODO
    if (:outdoc = 1 and :out = 1 and :wh2 in ('JC1','OC1') and :emwsconstloc != 'COPY') then
      exception universal 'Odstaw do COPY';

    execute procedure XK_MWS_SET_CART_LOCATION(:mwsord, :cartsymb, :emwsconstloc);
  end
end^
SET TERM ; ^
