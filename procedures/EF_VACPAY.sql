--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_VACPAY(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      ECOLUMN integer)
  returns (
      RET numeric(14,2))
   as
declare variable fromdate timestamp;
declare variable todate timestamp;
declare variable company integer;
declare variable correction numeric(14,2);
begin
  --DU: personel - funkcja liczy wynagrodzenie urlopowe
  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

   select company
    from epayrolls
    where ref = :payroll
    into  :company;

  select sum(payamount)
    from eabsences
    where employee = :employee and ecolumn = :ecolumn and epayroll = :payroll
      and correction = 0
    into :ret;

  if (ret is null) then
    ret = 0;
  else begin
    if (exists (select ref from epayrolls
      where status = 0 and empltype = 1  and company = :company
        and cper < (select cper from epayrolls
                      where ref = :payroll))) then
      exception EF_EXPT 'Nie wszystkie poprzednie listy płac są zamknięte!';
  end
--MW:korekty
  select sum(case when correction = 1 then -payamount else payamount  end)
    from eabsences
    where employee = :employee and ecolumn = :ecolumn and
      todate <= :todate and (corepayroll is null or corepayroll = :payroll) and correction in (1,2)
    into :correction;

  ret = coalesce(ret, 0) + coalesce(correction, 0);
  suspend;
end^
SET TERM ; ^
