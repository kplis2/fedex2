--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMPOZ_CHANGE_INC_RECEPTION(
      KORTOPOZ integer,
      POZDOKUM integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      ILOSC numeric(14,5),
      SERIALIZED smallint,
      BLOKOBLNAG smallint,
      BLOKPOZNALICZ integer,
      CENAMAG numeric(14,4),
      DOSTAWA integer,
      MAG_TYP char(1) CHARACTER SET UTF8                           ,
      MAG char(3) CHARACTER SET UTF8                           ,
      FIFO char(1) CHARACTER SET UTF8                           ,
      MINUS_STAN smallint,
      MAGBREAK integer,
      ZAMOWIENIE integer,
      WERSJAREF integer,
      OPK smallint,
      OPKTRYB smallint,
      SLODEF integer,
      SLOPOZ integer,
      CENASNET numeric(14,4),
      CENASBRU numeric(14,4),
      BN char(1) CHARACTER SET UTF8                           ,
      VAT numeric(14,4),
      STANOPKREF integer,
      ORG_ILOSC numeric(14,4),
      DOK_ILOSC numeric(14,4),
      CENA_KOSZT numeric(14,4))
   as
declare variable is_ilosc numeric(14,4);
declare variable serialnr varchar(40);
declare variable serialil numeric(14,4);
declare variable orgdokumser integer;
declare variable numer integer;
declare variable oilosc numeric(14,4);
declare variable ocenamag numeric(14,4);
declare variable ocenasprz numeric(14,4);
begin
    select sum(ilosc) from DOKUMROZ where pozycja=:pozdokum into :is_ilosc;
    if(:is_ilosc is null) then is_ilosc = 0;
    /* jesli rozpiska reczna, to trzeba zaakceptowac istniejace rozpiski */
    if(:is_ilosc > 0) then
      update DOKUMROZ set ACK =1 where pozycja=:pozdokum and ACK=0;
    ilosc = dok_ilosc - is_ilosc;
    if(:ilosc < 0) then exception MAGDOK_ROZ_TO_BIG;
    if(:ilosc > 0) then begin
      if(:serialized = 1) then
        for select SERIALNR, ILOSC, ORGDOKUMSER from DOKUMPOZ_SERIAL(:pozdokum,0)
        into :serialnr, :serialil ,:orgdokumser
        do begin
          if(:ilosc > 0) then begin
            if(:serialnr is null) then serialnr = '';
            if(:serialil is null) then serialil = :ilosc;
            numer = null;
            select NUMER from DOKUMROZ where POZYCJA=:pozdokum and cena = :cenamag and DOSTAWA = :dostawa  and SERIALNR = :serialnr into :numer;
            if(:numer is null) then begin
              select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
              if(:numer is null or (:numer = 0))then numer = 0;
              numer = numer +  1;
              insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,SERIALNR, BLOKOBLNAG, CENA_KOSZT, ORGDOKUMSER) values (:pozdokum,:numer,:serialil,:cenamag,:dostawa,:serialnr,:blokoblnag,:cena_koszt,:orgdokumser);
            end
            else
              update DOKUMROZ set ILOSC = ILOSC + :serialil, PCENA = null where pozycja = :pozdokum and numer = :numer;
            ilosc = :ilosc - :serialil;
          end
        end
      else begin
        if(:opktryb = 2)then begin
          /*przyjecie wedlug rozliczenia opakowan - np. zwrot od klienta - wartosci kaucji, dostaw, magazynowe*/
          while(:stanopkref >= 0) do begin
            for select STAN,CENAMAG,CENASPR
            from STANYOPK
            where SLODEF = :SLODEF and SLOPOZ = :slopoz and KTM = :ktm
                  and WERSJA = :wersja and ZREAL = 0
                  and ((:stanopkref = 0) or (:stanopkref = STANYOPK.REF))
             order by data
             into :oilosc,:ocenamag, :ocenasprz
            do begin
              if(:ilosc > 0) then begin
                if(:oilosc > :ilosc) then oilosc = :ilosc;
                if(:bn = 'B') then begin
                  cenasbru = :ocenasprz;
                  cenasnet = :cenasbru/(1+:vat/100);
                end else begin
                  cenasnet = :ocenasprz;
                  cenasbru = :cenasbru*(1+:vat/100);
                end
                numer = null;
                select NUMER from DOKUMROZ where POZYCJA=:pozdokum and cena = :ocenamag and DOSTAWA = :dostawa   and CENASNETTO = :cenasnet into :numer;
                if(:numer is null) then begin
                  select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
                  if(:numer is null or (:numer = 0))then numer = 0;
                  numer = numer +  1;
                  insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,SERIALNR,CENASNETTO, CENASBRUTTO,BLOKOBLNAG,CENA_KOSZT) values (:pozdokum,:numer,:oilosc,:ocenamag,:dostawa,'',:cenasnet, :cenasbru, :blokoblnag, :cena_koszt);
                end
                else
                  update DOKUMROZ set ILOSC = ILOSC + :oilosc, PCENA = null where pozycja = :pozdokum and numer = :numer;
                ilosc = :ilosc - :oilosc;
              end
            end
            if(:stanopkref > 0) then stanopkref = 0;
            else stanopkref = -1;
          end
/*          if(:ilosc > 0) then
           exception OPKSTANY_BRAKSTANOWDOREAL;      */
        end
        if(:ilosc > 0) then begin
            numer = null;
            select NUMER from DOKUMROZ where POZYCJA=:pozdokum and cena = :cenamag and DOSTAWA = :dostawa   into :numer;
            if(:numer is null) then begin
              select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
              if(:numer is null or (:numer = 0))then numer = 0;
              numer = numer +  1;

             /*
              exception universal 'pozdokum '||coalesce( :pozdokum, 'null')||' numer '||coalesce(:numer, 'null')||' ilosc '||coalesce(:ilosc, 'null')||
              ' cenamag ' ||coalesce(:cenamag, 'null')||' dostawa '||coalesce(:dostawa, 'null')||
              ' cenasnet '||coalesce(:cenasnet, 'null')||' cenasbru'||coalesce(:cenasbru, 'null')||
              ' blokoblnag '||coalesce( :blokoblnag, 'null')||' cena_koszt '||coalesce(:cena_koszt, 'null');
*/
              insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,SERIALNR,CENASNETTO, CENASBRUTTO,BLOKOBLNAG,CENA_KOSZT) values (:pozdokum,:numer,:ilosc,:cenamag,:dostawa, '',:cenasnet, :cenasbru, :blokoblnag,:cena_koszt);
            end
            else
              update DOKUMROZ set ILOSC = ILOSC + :ilosc, PCENA = null where pozycja = :pozdokum and numer = :numer;
            ilosc = 0;
        end
      end
    end
    if(:blokpoznalicz = 0) then
        execute procedure DOKPOZ_OBL_FROMROZ(:pozdokum);
end^
SET TERM ; ^
