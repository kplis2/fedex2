--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INSERT_DECREEMATCHINGGROUP(
      REFIN DECREEMATCHINGS_ID)
  returns (
      REFOUT DECREEMATCHINGS_ID)
   as
declare variable operator operator_id;
declare variable operators string;
begin
  if (:refin > 0 and not exists(select first 1 1 from decreematchinggroups where ref = :refin)) then
  begin
    execute procedure get_global_param('AKTUOPERATOR') returning_values (operators);
    if (coalesce(operators,'') <> '') then
      operator = cast(operators as integer);
    insert into decreematchinggroups (ref, accoper, accdatetime)
      values (:refin, :operator, current_timestamp(0));
    refout = :refin;
  end else
  begin
    execute procedure get_global_param('AKTUOPERATOR') returning_values (operators);
    if (coalesce(operators,'') <> '') then
      operator = cast(operators as integer);
    insert into decreematchinggroups (accoper, accdatetime)
      values (:operator, current_timestamp(0))
      returning ref
      into :refout;
  end
end^
SET TERM ; ^
