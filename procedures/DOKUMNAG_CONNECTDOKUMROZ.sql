--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_CONNECTDOKUMROZ(
      DOKUMENT integer)
   as
declare variable fromrozref integer;
declare variable topozref integer;
declare variable ilosc numeric(14,4);
declare variable cnt integer;
declare variable torozref integer;
declare variable fromcnt integer;
declare variable tocnt integer;
begin
  for select DOKUMPOZ.REF
  from DOKUMPOZ
  where DOKUMPOZ.dokument = :dokument
  into :topozref
  do begin
    fromcnt = 0;
    tocnt = 0;
    select count(*) from DOKUMROZ where POZYCJA=:topozref into :tocnt;
    select count(*) from DOKUMROZ where CENATOPOZ=:topozref into :fromcnt;
    if(:tocnt>1 and :fromcnt>1) then begin
      -- rozpiski N:N wiec laczymy wg ilosci
      for select REF,ILOSC
      from DOKUMROZ where CENATOPOZ=:topozref
      into :fromrozref, :ilosc
      do begin
        cnt = 0;
        select count(*), max(DOKUMROZ.REF)
        from dokumroz
        where DOKUMROZ.pozycja=:topozref
         and DOKUMROZ.ILOSC = :ilosc
        into :cnt, :torozref;
        if(:cnt <> 1) then torozref = null;
        update DOKUMROZ set CENATOROZ = :torozref where ref=:fromrozref;
      end

    end else if(:tocnt=1) then begin
      -- rozpiski N:1 lub 1:1 wiec wszystkie laczymy do docelowej rozpiski
      select max(REF) from DOKUMROZ where POZYCJA=:topozref into :torozref;
      update DOKUMROZ set CENATOROZ=:torozref where CENATOPOZ=:topozref;

    end else begin
      -- jesli jest 1:N to nie da sie polaczyc rozpisek
      update DOKUMROZ set CENATOROZ=NULL where CENATOPOZ=:topozref;

    end

  end

/*  for select DOKUMPOZ.REF, DOKUMROZ.REF, DOKUMROZ.ILOSC
  from DOKUMPOZ join DOKUMROZ on (DOKUMROZ.cenatopoz = DOKUMPOZ.REF)
  where DOKUMPOZ.dokument = :dokument
  into :topozref, :fromrozref,  :ilosc
  do begin
    cnt = 0;
    select count(*), max(DOKUMROZ.REF)
    from dokumroz
    where DOKUMROZ.pozycja=:topozref
         and DOKUMROZ.ILOSC = :ilosc
    into :cnt, :torozref;
    if(:cnt <> 1) then torozref = null;
    update DOKUMROZ set CENATOROZ = :torozref where ref=:fromrozref;
  end*/
end^
SET TERM ; ^
