--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_PROCESS_UPS_SHIP_RESPONSE(
      EDEDOCSH_REF EDEDOCH_ID)
  returns (
      OTABLE TABLE_NAME,
      OREF INTEGER_ID)
   as
declare variable LISTAWYSD integer_id;
declare variable NAME string255;
declare variable PACKAGEID integer_id;
declare variable LABEL blob_utf8;
declare variable CODTURNIN blob_utf8;
declare variable HIGHVALUEREPORTDECODE blob_utf8;
declare variable FORMAT STRING10;
declare variable PACZKA integer_id;
declare variable TRACKINGNUMBER STRING;
declare variable SHIPTRACKING STRING;
declare variable package_id_node integer_id;
declare variable statussped smallint_id;
begin
    select oref
      from ededocsh
      where ref = :ededocsh_ref
      into :listawysd;

    for
      select ededocsp.id
      from ededocsp
      where ededoch = :ededocsh_ref and name like 'Package%'
      into :PackageId
    do begin
      Label = null;
      Format = null;
      Paczka = null;
      TrackingNumber = null;

      for
        select id
          from ededocsp
          where ededoch = :ededocsh_ref and parent = :PackageId
          into :package_id_node
        do begin
          select p.val
            from ededocsp p
            where p.ededoch = :ededocsh_ref
              and p.parent = :package_id_node
              and p.name = 'Label'
          into :Label;

          select p.val
            from ededocsp p
            where p.ededoch = :ededocsh_ref
              and p.parent = :package_id_node
              and p.name = 'LabelFormat'
          into :Format;

          select p.val
            from ededocsp p
            where p.ededoch = :ededocsh_ref
              and p.parent = :package_id_node
              and p.name = 'REF'
          into :Paczka;

          select p.val
            from ededocsp p
            where p.ededoch = :ededocsh_ref
              and p.parent = :package_id_node
              and p.name = 'TrackingNumber'
          into :TrackingNumber;
         -- if (not exists (select first 1 1 from listywysdroz_opk where ref = :Paczka)) then
         --   exception universal'Różnica midzy numerami referencyjnymi paczki';
        end

       update or insert into listywysdopkrpt (format, rpt, opk, doksped)
           values (:format, :label, :Paczka, :listawysd)
           matching (doksped, opk);

      update listywysdroz_opk
        set nrpaczkistr = :TrackingNumber,
          symbolsped = :TrackingNumber
        where ref = :Paczka;
    end

    select val
      from ededocsp
      where ededoch = :ededocsh_ref and name ='Result'
      into :statussped;

    select val
      from ededocsp
      where ededoch = :ededocsh_ref and name ='ShipTracking'
      into :ShipTracking;

    update listywysd
      set spedresponse = :statussped, numerprzesylkistr = :ShipTracking, symbolsped = :ShipTracking, x_anulowany = 0
      where ref = :listawysd;

    CODTurnIn = null;

    select name, val
      from ededocsp
      where ededoch = :ededocsh_ref and name = 'CODTurnIn'
      into :name, :CODTurnIn;

   if ( CODTurnIn is not null) then
     insert into listywysdopkrpt (format, rpt, opk, doksped)
        values ('html', :CODTurnIn, null, :listawysd) ;


    HighValueReportDecode = null;

    select name, val
      from ededocsp
      where ededoch = :ededocsh_ref and name = 'HighValueReportDecoded'
      into :name, :HighValueReportDecode;

    if (HighValueReportDecode is not null) then
      insert into listywysdopkrpt (format, rpt, opk, doksped)
         values (:format, :HighValueReportDecode, null, :listawysd) ;

   otable = 'LISTYWYSD';
   oref = listawysd;

   suspend;
end^
SET TERM ; ^
