--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_VAT7_17(
      EDECLARATIONREF EDECLARATIONS_ID)
  returns (
      K1 STRING255,
      K2 STRING255,
      K3 SMALLINT_ID,
      K4 STRING2,
      K5 YEARS_ID,
      K6 STRING255,
      K7 SMALLINT_ID,
      K8 SMALLINT_ID,
      K9 STRING255,
      K70 STRING255,
      K71 STRING255,
      K73 STRING255,
      K74 STRING255,
      USP STRING255)
   as
declare variable edecpos_pvalue type of column edeclpos.pvalue;
declare variable edecpos_fieldsymbol type of column edeclpos.fieldsymbol;
declare variable filed_prefix char_1;
begin
/*TS: FK - pobiera dane dla wydruku na podstawie danych wskazanej e-Deklaracji.
  Zwrocone zostana tylko czesci stale wydruku (dla sekcji, ktore listuja pozycje,
  gdzie ich liczba jest zmienna, dane generuje osobna procedura z dopiskiem _POZ).

  > EDECLARATIONREF - numer e-Deklaracji, ktora chcemy wydrukowac.
*/

  /* Prefix nazw pol e-Deklaracji */
  filed_prefix = 'K';

  select distinct e.refid, e.status
    from edeclarations e
    where e.ref = :edeclarationref
  into :K2, :K3;

  for
    select ep.fieldsymbol, ep.pvalue
      from edeclarations e
        join edeclpos ep on (e.ref = ep.edeclaration)
      where e.ref = :edeclarationref
      into :edecpos_fieldsymbol, :edecpos_pvalue
  do begin
    if(:edecpos_fieldsymbol = :filed_prefix||'1') then K1 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'4') then K4 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'5') then K5 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'6') then K6 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'7') then K7 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'8') then K8 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'9') then K9 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'70') then K70 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'71') then K71 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'73') then K73 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'74') then K74 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = 'USP') then USP = :edecpos_pvalue;

  end
  suspend;
end^
SET TERM ; ^
