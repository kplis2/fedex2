--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_CLEARSYSLOCKS as
  declare variable lock_symbol varchar(40); --unikalny klucz blokady
begin
  /* Procedura usuwa blokady danych, do ktorych nie ma aktywnych polaczen */
  for
    select l.locksymbol
      from sys_locks l
        left join mon$attachments a
          on l.connectionid = a.mon$attachment_id
      where a.mon$attachment_id is null
    into :lock_symbol
  do begin
    delete from sys_locks s
      where s.locksymbol = :lock_symbol;
  end
end^
SET TERM ; ^
