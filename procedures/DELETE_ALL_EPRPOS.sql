--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DELETE_ALL_EPRPOS(
      PAYROLL integer,
      EMPLOYEE integer)
   as
begin
  rdb$set_context('USER_TRANSACTION', 'EPRPOS_DONOTCALC', 1); --PR53533
  update eprpos set cauto = 2 where employee = :employee and payroll = :payroll;
  delete from eprpos where payroll = :payroll and employee = :employee;
  rdb$set_context('USER_TRANSACTION', 'EPRPOS_DONOTCALC', 0);

--aktualizacja na nieobecnosci pracownika usuwanego z listy plac info o rozliczeniu tej nieobecnosci (BS48457)
  update eabsences set epayroll = null where epayroll = :payroll and employee = :employee;
  update eabsences set corepayroll = null where corepayroll = :payroll and employee = :employee;
end^
SET TERM ; ^
