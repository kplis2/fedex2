--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_VAT27_2_POZ(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      COMPANY COMPANIES_ID,
      EDECLARATIONREF EDECLARATIONS_ID,
      EDECPOSFIELD EDECLPOS_ID,
      RPTSECTION CHAR_1,
      TESTPOZROWCOUNT SMALLINT_ID = 0)
  returns (
      OUTEDECPOSFIELD EDECLPOS_ID)
   as
declare variable numer integer_id;
declare variable a smallint_id;
declare variable b string255;
declare variable c varchar(15);
declare variable d numeric(14,2);
declare variable data_row smallint_id;
declare variable subquery_id string10;
declare variable field_prefix string30;
declare variable field_prefix_end_char char_1;
declare variable sum_field_no smallint_id;
declare variable sum_field_val numeric(14,2);
declare variable field_root_prefix char_1;
begin
/*TS: FK - generuje pozycje dla e-Deklaracji dla wskazanej sekcji dokumentu.

  > EDECLARATIONREF - numer generowanej deklaracji z procedury nadrzednej (ta
    sama nazwa, bez '_POZ'), wartosc pola EDECLARATIONS.REF,
  > EDECPOSFIELD - pierwszy w kolejnosci wolny numer pola z tabeli EDECLPOS.
    Procedura nadrzedna wywoluje kolejno ta procedure dla wszystkich sekcji
    wydruku o zmiennej liczbie pozycji i kazda kolejna generuje kolejno numerowane pola.
  > RPTSECTION - dla ktorej sekcji wydruku generujemy dane do e-Deklaracji.
  > TESTPOZROWCOUNT - liczba wierszy z testowymi danymi, ktore zostana
    wygenerowane dla wskazanej sekcji dokumentu.
*/

  -- Ustawiamy prefixy pol, ktore beda dodawane do EDECLPOS.
  data_row = 1;
  subquery_id = :rptsection||'POZ';
  field_prefix_end_char = '.';
  field_prefix = :subquery_id||'_';
  field_root_prefix = 'K';

  -- Dokument VAT-27(2) posiada dwie sekcje ze zmienna liczba pozycji - C i D.
  -- Numer pola, do ktorego trafi suma pozycji z danej sekcji jest scisle
  -- powiazany z definicja wydruku. Dla sekcji C jest to pole K10, D - K11.
  -- Numer pola jest zawsze mniejszy od EDECPOSFIELD.
  sum_field_no = case
    when :rptsection = 'C' then 10
    when :rptsection = 'D' then 11
    else 0
  end;

  if (sum_field_no = 0) then
  begin
   exception universal 'Dla deklaracji VAT-27(2) nie istnieje sekcja danych: '
     ||:rptsection||'! Podaj jedną z następujących: C, D.';
  end

  -- Wartosc pola K10 lub K11 (zaleznie od sekcji).
  sum_field_val = 0;

  for
    execute statement 'select d.numer, d.a, d.b, d.c, d.d '
      || 'from get_data_vat27_2_'||:subquery_id
      || '('||:period||', '||:company||', 0, '||:testpozrowcount||') d'
    into :numer, :a, :b, :c, :d
  do begin
    field_prefix = :subquery_id||'_'||:data_row||:field_prefix_end_char;

    /* Symbole pol sa formatu XPOZ_Y.Z, gdzie:
     > X to nazwa sekcji wydruku,
     > Y to numer wiersza w danej sekcji,
     > Z to nazwa pola danego wiersza.
    */
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (
      :edeclarationref, :edecposfield + 0, :numer, 0, :field_prefix||'NUMER');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (
      :edeclarationref, :edecposfield + 1, :a, 0, :field_prefix||'A');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (
      :edeclarationref, :edecposfield + 2, :b, 0, :field_prefix||'B');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (
      :edeclarationref, :edecposfield + 3, :c, 0, :field_prefix||'C');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (
      :edeclarationref, :edecposfield + 4, :d, 0, :field_prefix||'D');

    -- Utawiamy licznik pol w EDECLPOS dla e-Deklaracji.
    edecposfield = :edecposfield + 5;
    -- Wymuszamy zmiane prefixu pola.
    data_row = :data_row + 1;

    sum_field_val = :sum_field_val + cast(:d as numeric(14,2));
  end

  insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (
    :edeclarationref, :sum_field_no, :sum_field_val, 0, :field_root_prefix||:sum_field_no);

  outedecposfield = :edecposfield;
  suspend;
end^
SET TERM ; ^
