--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ORDER_CHANGE_AMOUNT(
      NAGZAM integer,
      POZZAMREF integer,
      WERSJAREF integer,
      CENNIK integer,
      WORK2DO varchar(3) CHARACTER SET UTF8                           ,
      AMOUNT numeric(14,4),
      JEDNOSTKAREF integer = null)
   as
declare variable cenacen numeric(14,2);
declare variable ref integer;
declare variable bn char(1);
declare variable ktm varchar(21);
declare variable klient integer;
declare variable rabat numeric(14,2);
declare variable rabattab numeric(14,2);
declare variable iscena smallint;
declare variable refpromocji integer;
declare variable jednostkaglowna integer;
declare variable przelicznik numeric(14,4);
declare variable cenapodstawowa numeric(14,2);
begin
  select klient, bn from nagzam where ref = :nagzam into :klient, :bn;
  select ktm from wersje where ref = :wersjaref into :ktm;
  if (bn is null) then
    execute procedure GET_CONFIG('WGCEN', 2)
      returning_values :bn;


  if (work2do='add') then
  begin
    if (jednostkaref is null) then
      select ref from towjedn TJ where TJ.ktm = :ktm and TJ.glowna = 1 into :jednostkaref;
    if (jednostkaref is null) then
      exception universal 'brak jednostki podstawowej';

    select case when :bn = 'N' then cenanet else cenabru end
      from cennik where cennik=:cennik and wersjaref=:wersjaref and jedn=:jednostkaref
      into :cenacen;
    if (cenacen is null) then
    begin
      select ref from towjedn TJ where TJ.ktm = :ktm and TJ.glowna = 1 into :jednostkaglowna;
      if (jednostkaref is null) then
        exception universal 'brak jednostki podstawowej';
      select case when :bn = 'N' then cenanet else cenabru end
        from cennik where cennik=:cennik and wersjaref=:wersjaref and jedn=:jednostkaglowna
          into :cenapodstawowa;
      if (cenapodstawowa is null) then
        exception universal 'cena nie zostala pobrana';
      select przelicz from towjedn TJ where TJ.ref = :jednostkaref into :przelicznik;
      if (przelicznik is null) then
        exception universal 'brak przelicznika';
      cenacen = cenapodstawowa * przelicznik;
    end
    if (cenacen is null) then
        exception universal 'cena nie zostala pobrana';


    select rabat, iscena, refpromocji
      from oblicz_rabat(:ktm, :wersjaref, :klient, 1, :cenacen, :bn, '', null, null, null)
      where waluta = 'PLN'
      into :rabat, :iscena, :refpromocji;

    rabattab = 0;
    if (iscena = 1) then
    begin
      cenacen = rabat;
      rabattab = rabat;
      rabat = 0;
    end

    select ref from pozzam where wersjaref=:wersjaref and zamowienie=:nagzam
      into :ref;
    if (:ref is null) then begin
      insert into pozzam (zamowienie, ilosc, wersjaref, cenacen, rabat, rabattab, refpromocji)
        values (:nagzam, :amount, :wersjaref, :cenacen, :rabat, :rabattab, :refpromocji);
    end
    else begin
      update pozzam
        set ilosc=ilosc + :amount, cenacen=:cenacen, rabat=:rabat, rabattab=:rabattab, refpromocji=:refpromocji
        where ref=:ref;
    end
  end else
  if (work2do='del') then
  begin
    delete from pozzam where ref=:pozzamref and zamowienie=:nagzam and zamowienie=:nagzam;
  end else
  if (work2do='chg') then
  begin
    update pozzam set ilosc=:amount where ref=:pozzamref and zamowienie=:nagzam;
  end
end^
SET TERM ; ^
