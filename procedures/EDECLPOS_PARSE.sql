--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDECLPOS_PARSE(
      SOURCENAME varchar(255) CHARACTER SET UTF8                           ,
      MODE smallint = 0,
      EDECLREF EDECLARATIONS_ID = null)
  returns (
      PVALUE varchar(255) CHARACTER SET UTF8                           )
   as
declare variable CONFIG varchar(255);
declare variable SOURCENAME_PART varchar(60);
declare variable PERCENT integer;
declare variable QUESTIONMARK integer;
declare variable STMT varchar(1024);
declare variable THIS smallint;
begin
/*Personel: Parsowanie tresci pozycji definicji edeklaracji EDECLPOS.
    MODE = 0 - poczatkowe podanie tekstu
         = 1 - pobranie wartosci z wyniku
         = 2 - przypadek z '?'
         = 3 - poczatek "="
         = 4 - poczatek "THIS." - pobranie wartosci z aktualnego rekordu */

    questionmark = position('?' in sourcename);
    this = position('THIS.',sourcename);
    pvalue = '';

    if (mode = 0) then
    begin
      if (substring(sourcename from 1 for 1) = '=') then
        execute procedure edeclpos_parse(:sourcename, 3) returning_values :pvalue;
      else if(this > 0) then
      begin
        execute procedure edeclpos_parse(:sourcename, 4, :edeclref) returning_values :pvalue;
      end else begin
        if (questionmark = 0) then
          execute procedure edeclpos_parse(:sourcename, 1) returning_values :pvalue;
        else if (questionmark > 0) then
          execute procedure edeclpos_parse(:sourcename, 2) returning_values :pvalue;
      end
    end else
    if (mode = 1) then
    begin
      percent = position('%' in sourcename);
      while (percent>0)
      do begin
        if (percent=1) then
        begin
          sourcename=substring(sourcename from  2 for coalesce(char_length(sourcename),0)); -- [DG] XXX ZG119346
          percent = position('%' in sourcename);
          sourcename_part=substring(sourcename from  1 for  percent-1);
          sourcename = substring(sourcename from  percent+1 for coalesce(char_length(sourcename),0)); -- [DG] XXX ZG119346
          execute procedure get_config(sourcename_part,1) returning_values :config;
          pvalue = :pvalue || :config;
        end else
        begin
          config = substring(sourcename from  1 for percent-1);
          sourcename = substring(sourcename from  percent for  coalesce(char_length(sourcename) , 0)); -- [DG] XXX ZG119346
          pvalue = :pvalue || :config;
        end
        percent = position('%' in sourcename);
      end
      if (coalesce(char_length(sourcename),0)>0) then -- [DG] XXX ZG119346
        pvalue = :pvalue ||:sourcename;
    end else
    if (mode = 2) then
    begin
      while (questionmark>0)
      do begin
        select first 1 outstring
          from parse_lines(:sourcename, '?')
          into :sourcename_part;
        select first 1 skip 1 outstring
          from parse_lines(:sourcename, '?')
          into :sourcename;
        select first 1 outstring
          from parse_lines(:sourcename_part, '%')
          where outstring <> ''
          into :sourcename_part;
        execute procedure get_config(sourcename_part,1) returning_values :config;
        --Jesli config nie jest pusty, wycinamy do wyniku czesc przed :
        --Jesli config jest pusty, wycinamy czesc po :
        if (config <> '') then
          sourcename = substring(sourcename from 1 for position(':' in sourcename)-1);
        else
          sourcename = substring(sourcename from position(':' in sourcename)+1 for coalesce(char_length(sourcename),0)); -- [DG] XXX ZG119346
        --Szukamy czy w naszym wycietym tekscie sa jeszcze znaki zapytania
        questionmark = position('?' in sourcename);
      end
        execute procedure edeclpos_parse(:sourcename, 1) returning_values pvalue;
    end else
    if (mode = 3) then
    begin
      sourcename = substring(sourcename from 2 for coalesce(char_length(sourcename), 0)); -- [DG] XXX ZG119346
      execute procedure edeclpos_parse(:sourcename, 1) returning_values pvalue;
    end
    if(mode = 4) then  --PR65868
    begin
      sourcename = substring(sourcename from position('.' in sourcename)+1);
      if (coalesce(edeclref,'')<>'') then
      begin
        stmt = 'select '||:sourcename||' from edeclarations where ref = '||:edeclref;
        execute statement stmt
        into :pvalue;
      end
    end
end^
SET TERM ; ^
