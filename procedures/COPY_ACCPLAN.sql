--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COPY_ACCPLAN(
      FROMCOMPANY integer,
      FROMYEAR integer,
      TOCOMPANY integer,
      TOYEAR integer)
  returns (
      Q integer)
   as
declare variable ISPATTERN smallint = 0;
declare variable SYMBOL varchar(3);
declare variable DESCRIPT varchar(60);
declare variable AFILTER varchar(80);
declare variable BKTYPE smallint;
declare variable SALDOTYPE smallint;
declare variable SETTL smallint;
declare variable MULTICURR smallint;
declare variable REF integer;
declare variable NREF integer;
declare variable SEP char(1);
declare variable DICTDEF integer;
declare variable NLEVEL smallint;
declare variable STTLTYPE varchar(1);
declare variable PRD smallint;
declare variable DOOPISU smallint;
declare variable AKU integer;
declare variable ACCFROMPATERN BKACCOUNTS_ID;
declare variable PATTERN COMPANIES_ID;
declare variable PATTERNYEAR BKYEARS_ID;
begin
  /*exception test_break''||' FROMCOMPANY: '||coalesce(FROMCOMPANY,'NULL')||'
FROMYEAR: '||coalesce(
FROMYEAR,'NULL')||' 
TOCOMPANY: '||coalesce(
TOCOMPANY,'NULL')||' 
TOYEAR: '||coalesce(
TOYEAR,'NULL'); */
  --Procedura kopiująca plan kont na nowy rok ksigowy z zachowaniem idei WPK
  q = 0; -- licznik dodanych nowych kont

  --Należy sprawdzić, czy jestemy wzorcem, czy podlegamy pod wzorzec
  -- 1 - wzorzec 0 - podlegly
  select iif(coalesce(bky.patternref,0) = 0,1,0), bky.patternref
    from bkyears bky
    where bky.company = :fromcompany and bky.yearid = :fromyear
    into :ispattern, :patternyear;
  if (ispattern = 0) then
  begin
    select company
      from bkyears b
      where b.ref = :patternyear
      into :pattern;
    for
      select a.bref
        from avaliblebkaccountsfrompattern(:tocompany,:toyear) a
        where a.bsymbol in (select b.symbol
                           from bkaccounts b
                           where b.company = :tocompany and b.yearid = :fromyear
                             and b.pattern_ref is not null)
      into :accfrompatern
    do begin
      execute procedure add_bkaccount_from_pattern(:pattern,:tocompany,:accfrompatern);
      q = q + 1;
    end
  end
  for
    select ref, symbol, descript, bktype, saldotype, settl, multicurr, sttltype, prd, coalesce(doopisu,0)
      from bkaccounts
      where company = :fromcompany and yearid = :fromyear and pattern_ref is null
      into :ref, :symbol, :descript, :bktype, :saldotype, :settl, :multicurr, :sttltype, :prd, :doopisu
  do begin
     select count_new_bkacconts
       from copy_accplan_part(:ref,:tocompany, :toyear, :symbol, :descript, :bktype, :saldotype, :settl,
         :multicurr, :sttltype, :prd, :doopisu)
       into :aku;
     q = q + coalesce(aku,0);
  end
  suspend;
end^
SET TERM ; ^
