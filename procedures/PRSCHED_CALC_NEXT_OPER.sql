--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_CALC_NEXT_OPER(
      PRSCHEDOPER integer,
      NR integer,
      MODE smallint,
      LEVEL_IN integer = 0)
  returns (
      RPRSCHEDOPER integer,
      RPOPSCHEDOPER integer,
      RNR integer,
      ACTIV smallint,
      REPORTED smallint,
      LEVEL_OUT integer)
   as
declare variable sql varchar(1024);
begin
  --mode 0 -> nastepne operacje
  --     1 -> poprzednie
  if (nr > 1000) then
    exception universal 'zapentlone operacje '||:prschedoper;

  if (nr is null) then
    nr = 0;
  rnr = nr + 1;
  if(:prschedoper is null) then
    prschedoper = 0;
  if (coalesce(:mode,0) = 0) then
  begin
    sql = 'select o.ref, o.activ, o.reported ';
    sql = sql ||'  from prschedoperdeps d';
    sql = sql ||'    join prschedopers o on (o.ref = d.depto)';
    sql = sql ||'  where d.depfrom = 0'||:prschedoper;
  end else begin
    sql = 'select o.ref, o.activ, o.reported ';
    sql = sql ||'  from prschedoperdeps d';
    sql = sql ||'    join prschedopers o on (o.ref = d.depfrom)';
    sql = sql ||'  where d.depto = 0'|| :prschedoper;
  end
  for
      execute statement :sql
      into :rprschedoper, :activ, :reported
  do begin
    level_out = level_in;
    rpopschedoper = :prschedoper;
    suspend;
    for
      select p.rprschedoper, p.rnr, p.activ, p.reported, p.rpopschedoper, p.level_out
        from prsched_calc_next_oper(:rprschedoper, :rnr, :mode, :level_in +1) p
        into :rprschedoper, :rnr, :activ, :reported, :rpopschedoper, :level_out
    do
      suspend;
  end
end^
SET TERM ; ^
