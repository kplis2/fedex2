--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CALCULATE_AMORTPLAN(
      FXDASSET integer)
  returns (
      AMPERIOD integer,
      AMYEAR smallint,
      AMMONTH smallint,
      NAME varchar(20) CHARACTER SET UTF8                           ,
      TAX_AMORT numeric(15,2),
      FIN_AMORT numeric(15,2),
      TTYPE smallint,
      FTYPE smallint,
      TARATE numeric(10,5),
      FARATE numeric(10,5))
   as
declare variable acc_tax_amort NUMERIC(15,2);
  declare variable acc_fin_amort NUMERIC(15,2);
  declare variable sum_tax_amort NUMERIC(15,2);
  declare variable sum_fin_amort NUMERIC(15,2);
  declare variable company integer;
begin
  select company from fxdassets where ref = :fxdasset
    into :company;
  sum_tax_amort = 0;
  sum_fin_amort = 0;
  for
    select ref, amyear, ammonth, name
      from amperiods
      where company = :company
      order by amyear, ammonth
    into :AMPERIOD, AMYEAR, AMMONTH, NAME
  do begin
    execute procedure calculate_acc_amortization(:FXDASSET, :AMPERIOD)
      returning_values :acc_tax_amort, :acc_fin_amort, ttype, ftype, tarate, farate;

    tax_amort = acc_tax_amort - sum_tax_amort;
    fin_amort = acc_fin_amort - sum_fin_amort;
    if (tax_amort<>0 or fin_amort<>0) then
      suspend;
    sum_tax_amort = acc_tax_amort;
    sum_fin_amort = acc_fin_amort;
  end
end^
SET TERM ; ^
