--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CONTROL_BANKACCOUNT_NUMBER(
      BANKACCOUNT varchar(80) CHARACTER SET UTF8                           ,
      COUNTRYCODE varchar(2) CHARACTER SET UTF8                            ='')
  returns (
      CORRECT integer,
      MSG varchar(60) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE NEWBANKACC VARCHAR(30);
DECLARE VARIABLE OLDBANKACC VARCHAR(30);
DECLARE VARIABLE I INTEGER;
DECLARE VARIABLE CHARCODE VARCHAR(1);
DECLARE VARIABLE VALUECHARCODE VARCHAR(2);
DECLARE VARIABLE MOD INTEGER;
DECLARE VARIABLE RESULTMOD INTEGER;
DECLARE VARIABLE TEMPBANKACC INTEGER;
begin
  msg = '';
  correct = 0;
  mod = 97;
  i = 1;
  bankaccount = replace(bankaccount, '-', '');
  bankaccount = replace(bankaccount, ' ', '');
  if (bankaccount is NULL) then bankaccount = '';
  if (coalesce(char_length(bankaccount),0) <> 26 and bankaccount <> '') then -- [DG] XXX ZG119346
    msg = 'Nieprawidłowa długość konta bankowego.';
  else begin
    newbankacc = substring(bankaccount from 3 for 24);
    if (countrycode is null) then countrycode = '';
    if (countrycode = '') then countrycode = 'PL';
    while (i <= 2) do begin
      if (i = 1) then charcode = substring(countrycode from 1 for 1);
      else if (i = 2) then charcode = substring(countrycode from 2 for 1);
      charcode = upper(charcode);
      if (charcode = 'A') then valuecharcode = '10';
      else if (charcode = 'B') then valuecharcode = '11';
      else if (charcode = 'C') then valuecharcode = '12';
      else if (charcode = 'D') then valuecharcode = '13';
      else if (charcode = 'E') then valuecharcode = '14';
      else if (charcode = 'F') then valuecharcode = '15';
      else if (charcode = 'G') then valuecharcode = '16';
      else if (charcode = 'H') then valuecharcode = '17';
      else if (charcode = 'I') then valuecharcode = '18';
      else if (charcode = 'J') then valuecharcode = '19';
      else if (charcode = 'K') then valuecharcode = '20';
      else if (charcode = 'L') then valuecharcode = '21';
      else if (charcode = 'M') then valuecharcode = '22';
      else if (charcode = 'N') then valuecharcode = '23';
      else if (charcode = 'O') then valuecharcode = '24';
      else if (charcode = 'P') then valuecharcode = '25';
      else if (charcode = 'Q') then valuecharcode = '26';
      else if (charcode = 'R') then valuecharcode = '27';
      else if (charcode = 'S') then valuecharcode = '28';
      else if (charcode = 'T') then valuecharcode = '29';
      else if (charcode = 'U') then valuecharcode = '30';
      else if (charcode = 'W') then valuecharcode = '31';
      else if (charcode = 'V') then valuecharcode = '32';
      else if (charcode = 'X') then valuecharcode = '33';
      else if (charcode = 'Y') then valuecharcode = '34';
      else if (charcode = 'Z') then valuecharcode = '35';
      newbankacc = newbankacc || valuecharcode;
      i = i + 1;
    end
    i = 0;
    newbankacc = newbankacc || substring( bankaccount from 1 for 2);
    while (coalesce(char_length(newbankacc),0) > 9) do begin -- [DG] XXX ZG119346
      tempbankacc = substring(newbankacc from 1 for 9);
      oldbankacc = newbankacc;
      resultmod = tempbankacc - (floor(tempbankacc / mod) * mod);
      newbankacc = resultmod || substring(oldbankacc from 10);
    end
    tempbankacc = newbankacc;
    resultmod = tempbankacc - (floor(tempbankacc / mod) * mod);
    if (resultmod = 1 or bankaccount = '') then correct = 1;
    else if (resultmod <> 1) then msg = 'Nie zgadzają się cyfry kontrolne.';
  end
  suspend;
end^
SET TERM ; ^
