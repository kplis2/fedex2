--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMSER_ROZBIJ(
      REFDOKUMSER integer,
      REFPOZ integer,
      TYP char(1) CHARACTER SET UTF8                           ,
      NUMER integer,
      ORD smallint,
      ODSERIAL varchar(40) CHARACTER SET UTF8                           ,
      DOSERIAL varchar(40) CHARACTER SET UTF8                           ,
      ODSERIALNR integer,
      DOSERIALNR integer,
      ILOSC numeric(14,4),
      ILOSCROZ numeric(14,4),
      ORGDOKUMSER numeric(14,4))
  returns (
      STATUS smallint)
   as
declare variable cnt integer;
begin

  status = 0;

  cnt = 0;
--  exception test_break ' dokumser_rozbij Ilosc='||:ilosc;
  while (ilosc > 1) do begin
    cnt = :cnt + 1;
    numer = :numer + 1;
    odserial = odserialnr + cnt;
    
    insert into dokumser( refpoz, typ, numer, ord, odserial, doserial,
        odserialnr, doserialnr, ilosc, iloscroz, ORGDOKUMSER)
      values (:refpoz, :typ, :numer, :ord, :odserial, '',
      null, null, 1, 0, :ORGDOKUMSER);
    ilosc = :ilosc -1;
  end

  status = 1;
end^
SET TERM ; ^
