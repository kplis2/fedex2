--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAV_OLAPLAYOUTS(
      OLAPCUBE integer)
  returns (
      REF integer,
      PARENT integer,
      NUMBER integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      HINT varchar(255) CHARACTER SET UTF8                           ,
      KINDSTR varchar(20) CHARACTER SET UTF8                           ,
      ACTIONID varchar(60) CHARACTER SET UTF8                           ,
      APPLICATIONS varchar(60) CHARACTER SET UTF8                           ,
      FORMODULES varchar(60) CHARACTER SET UTF8                           ,
      RIGHTS varchar(255) CHARACTER SET UTF8                           ,
      RIGHTSGROUP varchar(255) CHARACTER SET UTF8                           ,
      ISNAVBAR smallint,
      ISTOOLBAR smallint,
      ISWINDOW smallint,
      ISMAINMENU smallint,
      ITEMTYPE smallint,
      GROUPPROCEDURE varchar(60) CHARACTER SET UTF8                           ,
      CHILDRENCOUNT smallint,
      RIBBONTAB varchar(40) CHARACTER SET UTF8                           ,
      RIBBONGROUP varchar(40) CHARACTER SET UTF8                           ,
      NATIVETABLE varchar(40) CHARACTER SET UTF8                           ,
      POPUPMENU varchar(40) CHARACTER SET UTF8                           ,
      NATIVEFIELD varchar(40) CHARACTER SET UTF8                           ,
      FORADMIN smallint)
   as
declare variable SQL varchar(1024) = '';
declare variable AKTUOPERATOR varchar(255) = '';
declare variable AKTUOPERADMIN varchar(255) = '';
declare variable AKTUOPERGRUPY varchar(255);
begin
  parent = NULL;
  hint = '';
  actionid = 'ExecuteOLAPLAYOUTFromNavigator';
  applications = '';
  formodules = '';
  kindstr = 'MI_MYSLNIK';
  isnavbar = 0;
  istoolbar = 1;
  iswindow = 0;
  ismainmenu = 0;
  itemtype = 0;
  childrencount = 0;
  groupprocedure = '';
  ribbontab = '';
  ribbongroup = '';
  nativetable = 'OLAPCUBESLAYOUTS';
  popupmenu = '';
  foradmin = 0;
  number = 1;
  execute procedure get_global_param('AKTUOPERATOR') returning_values :aktuoperator;
  execute procedure get_global_param('AKTUOPERGRUPY') returning_values :AKTUOPERGRUPY;
  execute procedure get_global_param('AKTUOPERADMIN') returning_values :AKTUOPERADMIN;
  sql = 'select REF, LAYOUTNAME, RIGHTS, RIGHTSGROUP ';
  sql = sql || ' from OLAPCUBESLAYOUTS where OLAPCUBE='||:olapcube||' ';
  sql = sql || ' and ((OLAPCUBESLAYOUTS.RIGHTS = '''' and OLAPCUBESLAYOUTS.RIGHTSGROUP = '''') or (OLAPCUBESLAYOUTS.RIGHTS like ''%;'||coalesce(:aktuoperator,'')||';%'') OR (strmulticmp('';'||coalesce(:AKTUOPERGRUPY,'')||';'',OLAPCUBESLAYOUTS.RIGHTSGROUP)=1))';
  sql = sql || ' and (';
  if(:AKTUOPERADMIN <> '1') then
    sql = sql || 'OLAPCUBESLAYOUTS.OWNER = 0'|| coalesce(:AKTUOPERATOR,'') ||' or ';
  sql = sql || ' OLAPCUBESLAYOUTS.OWNER = 0)';
  sql = sql|| ' order by LAYOUTNAME ';
  for execute statement :sql
  into :REF, :NAME, :RIGHTS, :RIGHTSGROUP
  do begin
    suspend;
    number = :number + 1;
  end

end^
SET TERM ; ^
