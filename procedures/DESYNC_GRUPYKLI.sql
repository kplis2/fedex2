--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DESYNC_GRUPYKLI(
      REFF integer)
   as
DECLARE VARIABLE IL INTEGER;
begin
   select count(*) from LOG_FILE where TABLE_NAME='GRUPYKLI' and KEY1=:reff into :il;
   if(:il is null) then il = 0;
   if(:il = 0) then
      insert into LOG_FILE(TABLE_NAME,TIME_STAMP,KEY1) values ('GRUPYKLI',CURRENT_TIMESTAMP(0),:reff);
   else
     update LOG_FILE set TIME_STAMP=CURRENT_TIMESTAMP(0) where TABLE_NAME='GRUPYKLI' and KEY1=:reff;
end^
SET TERM ; ^
