--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_FP_FGSP_UCP(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
begin
/*DS: Personel - funkcja sprawdza czy dany zleceniobiorca powinien oplacac skladke
      na FP i FGSP. Pprzy modyfikacji uwzglednic procedure EF_FP_FGSP */

  execute procedure ef_fp_fgsp(employee, payroll, prefix)
    returning_values ret;

  suspend;
end^
SET TERM ; ^
