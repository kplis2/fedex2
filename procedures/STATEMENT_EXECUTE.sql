--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STATEMENT_EXECUTE(
      PROCDECL varchar(1024) CHARACTER SET UTF8                           ,
      PROCBODY varchar(1024) CHARACTER SET UTF8                           ,
      KEY1 integer,
      KEY2 integer,
      TEST integer)
  returns (
      RET numeric(14,4))
   as
declare variable proc varchar(8191);
declare variable number varchar(10);
begin
/*
  number = cast(gen_id(GEN_STATEMENT,1) as varchar(10));
  proc = 'CREATE OR ALTER PROCEDURE X_STATEMENT_' || :number ||
'(key1 integer, key2 integer)
returns (ret numeric(14,4))
as
' || :procdecl || '
begin
' || :procbody || '
  suspend;
end;';
  execute statement proc;
  if(:test=0) then begin
    if(key1 is null) then key1 = 0;
    if(key2 is null) then key2 = 0;
    execute statement 'select ret from X_STATEMENT_' || :number
        ||'('|| :key1 ||', '|| :key2 ||')'
    into :ret;
  end
  execute statement 'drop procedure X_STATEMENT_' || :number;*/
  --  exception test_break :procdecl||' '||:procbody||' '||:key1||' '||:key2||' '||:test;
  if(key1 is null) then key1 = 0;
  if(key2 is null) then key2 = 0;
  proc = 'execute block
returns (ret numeric(14,4))
as
declare variable key1 integer;
declare variable key2 integer;
' || :procdecl || '
begin
  key1 = '||:key1||';
  key2 = '||:key2||';
' || :procbody || '
suspend;
end;';
  if(:test=0) then begin
    execute statement :proc
    into :ret;
  end
  suspend;
end^
SET TERM ; ^
