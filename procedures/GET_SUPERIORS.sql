--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_SUPERIORS(
      OPERATORS varchar(255) CHARACTER SET UTF8                           )
  returns (
      NEWOPERATORS varchar(255) CHARACTER SET UTF8                           )
   as
declare variable tmp varchar(255);
declare variable i integer;
declare variable j integer;
declare variable oper integer;
declare variable oper2 integer;
begin
  tmp = :operators;
  newoperators = :operators;
  i = position(';' in :tmp);
  if(:i=0) then exit;
  if(substring(:newoperators from coalesce(char_length(:newoperators),0) for coalesce(char_length(:newoperators),0))<>';') then newoperators = :newoperators||';'; -- [DG] XXX ZG119346
  -- przejdz po wszystkich elementach listy
  while(i>0) do begin
    tmp = substring(:tmp from i+1 for coalesce(char_length(:tmp),0)); -- [DG] XXX ZG119346
    j = position(';' in :tmp);
    if(j>1) then begin
      -- wytnij jeden element
      oper = cast(substring(:tmp from 1 for j-1) as integer);
      while(oper>0) do begin
        -- przejdz po przelozonych
        oper2 = null;
        select przelozony from operator where ref=:oper into :oper2;
        if(:oper2 is null) then oper2 = 0;
        if(:oper2>0) then begin
          -- jesli przelozonego nie ma jeszcze na liscie to go doloz
          if(position(';'||:oper2||';' in :newoperators)=0) then begin
            newoperators = :newoperators||:oper2||';';
          end
        end
        oper = :oper2;
      end
    end
    i = :j;
  end
  suspend;
end^
SET TERM ; ^
