--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_GETBKDOCPARAMVAL(
      BKDOC integer,
      PARAMETR varchar(20) CHARACTER SET UTF8                           )
  returns (
      WARTOSC varchar(255) CHARACTER SET UTF8                           )
   as
begin
  select b.wartosc
    from bkdocparams b
    where b.bkdoc = :bkdoc
      and b.parametr = :parametr
    into :wartosc;
end^
SET TERM ; ^
