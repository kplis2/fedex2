--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_SET_ALL_GRANTS_FOR as
begin
  in autonomous transaction do
    execute procedure set_all_grants_for('SENTE');
  in autonomous transaction do
    execute procedure set_all_grants_for('SYSDBA');
  in autonomous transaction do
    execute procedure set_all_grants_for('HH');      
  in autonomous transaction do
    execute procedure set_all_grants_for('CRON');
end^
SET TERM ; ^
