--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_GEN_PRODORDS(
      PRORD integer,
      MODE smallint,
      PRSHEET integer,
      AMOUNT numeric(14,4),
      REJESTR varchar(10) CHARACTER SET UTF8                           ,
      NAGZAM integer,
      POZZAM integer,
      PRNAGZAM integer,
      PRPOZZAM integer,
      STOPCASCADEIN smallint,
      GRUPAZAM integer = 0,
      ACCUMULATIONMODE smallint = 0,
      KILPOP numeric(14,4) = 1,
      GRUPAGEN integer = 0,
      PRODTRANSFER smallint = 0,
      SCHEDVIEW smallint = 0)
  returns (
      PRORDOUT integer,
      GRUPAZAMOUT integer,
      GRUPAGENOUT integer)
   as
declare variable TYPZAM varchar(3);
declare variable QUANTITY numeric(14,4);
declare variable PRDEPART varchar(20);
declare variable WERSJAREF integer;
declare variable ZLECPRODNEW integer;
declare variable PRPOZZAMNEW integer;
declare variable WAREHOUSESHE varchar(3);
declare variable WAREHOUSEMAT varchar(3);
declare variable PRSHMAT integer;
declare variable MATGROSSQUANTITY numeric(14,4);
declare variable JEDN integer;
declare variable ILOSC numeric(14,4);
declare variable ILDOST numeric(14,4);
declare variable KILOSC numeric(14,4);
declare variable CNT smallint;
declare variable DOSTAWA integer;
declare variable CENA numeric(14,2);
declare variable BRAKDOSTAW smallint;
declare variable TECHJEDNPERDAYS varchar(255);
declare variable TECHJEDNPERDAY numeric(14,2);
declare variable TECHJEDNQUANTITY numeric(14,4);
declare variable BATCHQUANTITY numeric(14,4);
declare variable SHORTAGE numeric(14,4);
declare variable PRSHEETSYMBOL varchar(60);
declare variable DATAWE timestamp;
declare variable JEDNM integer;
declare variable PRZELICZ numeric(14,4);
declare variable KTM varchar(40);
declare variable ODDZIAL varchar(40);
declare variable OUT smallint;
declare variable SUBSHEET integer;
declare variable STOPCASCADE smallint;
declare variable POPREF integer;
declare variable PREF integer;
declare variable PRODREF integer;
declare variable NEWPRORD integer;
declare variable KILORG numeric(14,4);
declare variable SHORTAGES numeric(14,4);
declare variable KILCALCORG numeric(14,4);
declare variable ACCUMULATE smallint;
declare variable STATUS smallint;
declare variable INSERTMATERIALS smallint;
declare variable grupagentmp integer;
declare variable mattype smallint;
begin
  insertmaterials = 1;
  if (mode > 1000) then
  begin
    mode = mode - 1000;  -- zmniejszamy o 1000 bo zwiekszono w kodzie przed generowaniem na potrzeby wykonania kontroli podczas pierwszej petli
    execute procedure pr_gen_prodords_check(PRORD,MODE,PRSHEET,AMOUNT,REJESTR,
        NAGZAM,POZZAM,PRNAGZAM,PRPOZZAM,STOPCASCADEIN,GRUPAZAM,ACCUMULATIONMODE)
      returning_values status;
  end
  if (grupazam is null or grupazam = 0) then
    execute procedure grupazam returning_values grupazam;
  if (grupagen is null or grupagen = 0) then
    execute procedure grupazam returning_values grupagen;
  grupazamout = grupazam;
  grupagenout = grupagen;
  accumulate = 0;
  if (kilpop is null or kilpop = 0) then kilpop = 1;
  if (stopcascadein is null) then stopcascadein = 0;
  if(prsheet is null or prsheet = 0) then
    exception PRSHEETS_GENNAGZAM 'Nie można zidentyfikować domyslnej karty techn. dla towaru.';
  select p.symbol, p.wersjaref, pt.typzam, p.warehouse, p.techjednquantity, p.quantity, p.prdepart, p.batchquantity, p.jedn
    from prsheets p
      left join prshtypes pt on (p.shtype = pt.symbol)
    where p.ref = :prsheet
    into :prsheetsymbol, :wersjaref, :typzam, :warehouseshe, :techjednquantity, :quantity, :prdepart, :batchquantity, :jedn;
  execute procedure GETCONFIG('TECHJEDNQUANTITY') returning_values :techjednperdays;
  if(techjednperdays is not null and techjednperdays <> '') then techjednperday = cast(techjednperdays as numeric(14,2));
  else techjednperday = 8;
  -- zalozenie naglowka zlecenia produkcyjnego
  if ((prord is null or prord = 0) and stopcascadein = 0) then
  begin
    if (accumulationmode = 0) then
    begin
      select first 1 n.ref
        from nagzam n
          left join pozzam p on (p.zamowienie = n.ref)
        where n.rejestr = :rejestr and n.typzam = :typzam and n.grupazam = :grupazam
          and p.out = 0 and p.prsheet = :prsheet
        into prord;
      if (prord is not null) then accumulate = 1;
    end
    if (prord is null or prord = 0) then
    begin
      execute procedure GEN_REF('NAGZAM') returning_values :prord;
      select datawe, oddzial from nagzam where ref = :nagzam into :datawe, oddzial;
      if (nagzam is null) then
        nagzam = prord;
      insert into NAGZAM(REF, REJESTR, TYPZAM, MAGAZYN, DATAZM, DATAWE, TERMDOST, KPOPREF,
          KINNE, PRDEPART, KPOPZAM, TECHJEDNQUANTITY, kpopprzam, kpopprref, GRUPAZAM, ODDZIAL, SCHEDVIEW)
        values(:prord, :rejestr, :typzam, :warehouseshe, :datawe, :datawe, CURRENT_DATE, :pozzam,
            1, :prdepart, :nagzam, :techjednquantity, :prnagzam, :prpozzam, :grupazam, :oddzial, :schedview);
    end
  end
  if (nagzam is null and prord is not null) then
    nagzam = prord;
  -- dodawanie wyrobu
  if (stopcascadein = 0 and mode <> 3) then
  begin
    popref = null;
    if (prpozzam is null or prpozzam = 0) then
      popref = pozzam;
    else
      popref = prpozzam;
    if(batchquantity is null or (batchquantity = 0)) then batchquantity = 1;
    techjednquantity = cast((techjednquantity /batchquantity * amount) as integer);
    prodref = null;
    if (accumulationmode = 0 and grupazam > 0) then
    begin
      select first 1 p.ref
        from nagzam n
          left join pozzam p on (p.zamowienie = n.ref)
        where n.rejestr = :rejestr and n.typzam = :typzam and n.grupazam = :grupazam
          and p.out = 0 and p.prsheet = :prsheet and coalesce(p.prodtransfer,0) = :prodtransfer
        into prodref;
    end
    if (prodref is null) then
    begin
      execute procedure GEN_REF('POZZAM') returning_values :prodref;
      insert into POZZAM(REF ,ZAMOWIENIE, WERSJAREF, JEDN, ILOSC, MAGAZYN, OUT, PRSHEET, techjednquantity, prgrupagen, prodtransfer)
        values(:prodref ,:prord, :WERSJAREF, :JEDN, :amount, :warehouseshe, 0, :prsheet, :techjednquantity, :grupagen, :prodtransfer);
      kilpop = 1;
    end else
    begin
      update pozzam set ilosc = ilosc + :amount, techjednquantity = techjednquantity + techjednquantity, prrecalcdepend = 1
        where ref = :prodref;
      insertmaterials = 0;
    end
    execute procedure INSERT_RELATION(1,'POZZAM',:prodref,'POZZAM',:popref,:amount,:amount);
  end else
    prodref = prpozzam;
  if (insertmaterials = 1) then
  begin
    -- dodanie surowców i pozycji posrednich
    if(not exists (select PS.ref
        from PRSHMAT PS
        left join prsheets P on (ps.sheet = p.ref)
        where PS.sheet = :prsheet and PS.ktm is not null and PS.ktm <> '')
    ) then begin
      exception prsheets_error substring('Brak pozycji materiałowej dla karty '||:prsheetsymbol from 1 for 70);
    end
   for
     select versout, quantityout, kquantityout, refout, sheetout, stopcascadeout, jednout, whout,
          kpartquantityout, shortagesout, KPARTLCALCOUT, mattype, prodtransfer
        from XK_PRSHEET_MATERIALS(:prsheet,0,:amount,1,null,:kilpop)
        into wersjaref, ilosc, kilosc, prshmat, subsheet, stopcascade, jedn, warehousemat,
            kilorg, shortages, KILCALCORG, mattype, prodtransfer
    do begin
      if (mattype in (1,2,3)) then
        out = 3;
      else if (subsheet is null or subsheet = 0) then
        out = 1;
      else if (stopcascade = 1) then
        out = 2;
      else
        out = 1;
      prpozzam = prodref;
      pref = null;
      if (accumulate = 1) then
      begin
        if (out = 2) then
          select p.ref
            from pozzam p
              left join relations r on (r.srefto = :prodref and r.stableto = 'POZZAM' and r.act > 0)
            where p.zamowienie = :prord and p.prsheet = :subsheet and r.ref is not null
            into pref;
        if (out in (1,3)) then
        begin
          select p.ref
            from pozzam p
              left join relations r on (r.srefto = :prodref and r.stableto = 'POZZAM' and r.act > 0  and r.sreffrom = p.ref and r.stablefrom = 'POZZAM')
            where p.zamowienie = :prord and r.ref is not null and p.wersjaref = :wersjaref and coalesce(p.prodtransfer,0) = :prodtransfer
            into pref;
        end
      end
      if (pref is null) then
      begin
        execute procedure GEN_REF('POZZAM') returning_values :pref;
        insert into POZZAM(REF, ZAMOWIENIE, WERSJAREF, JEDN, ILOSC, kilosc, MAGAZYN, PRSHMAT, OUT,
            KILORG, SHORTAGES, PRSHEET, KILCALCORG, PRODTRANSFER)
          values(:pref, :prord, :WERSJAREF, :JEDN, :ilosc, :kilosc, :warehousemat, :prshmat, :out,
              :kilorg, :shortages, case when :out = 2 then :subsheet else null end, :KILCALCORG, :prodtransfer);
      end else
        update pozzam set ilosc = ilosc + :ilosc where ref = :pref;
      execute procedure insert_relation(2,'POZZAM',:pref,'POZZAM',:prpozzam,:ilosc,:amount);
      if (subsheet is not null and stopcascade < 2) then
      begin
        newprord = null;
        if (stopcascade = 1) then
          newprord = :prord;
        execute procedure PR_GEN_PRODORDS (:newprord,0,:subsheet,:ilosc,:rejestr,
            :nagzam,:pozzam,:prord,:pref,:stopcascade,:grupazam,:accumulationmode,:kilosc,-1,:prodtransfer,:schedview)
          returning_values :prordout, :GRUPAZAMOUT, grupagentmp;
      end
    end
  end
  prordout = prord;
end^
SET TERM ; ^
