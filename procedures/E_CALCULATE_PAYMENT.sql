--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_CALCULATE_PAYMENT(
      EMPLOYEE integer,
      PAYROLL integer)
  returns (
      X integer)
   as
declare variable status smallint;
begin
 -- DU: Personel - funkcja liczy kalkulacje wynagrodzenia dla pracownika

  select status from epayrolls
    where ref = :payroll
    into :status;

  if (status > 0) then exception payroll_is_closed;

  select status from eprempl
    where epayroll = :payroll and employee = :employee
    into :status;

  if (status > 0) then exception eprempl_accepted;

  execute procedure e_calculate_eprpos(:employee, :payroll, 0);


  suspend;
end^
SET TERM ; ^
