--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_GET_FIELD_NAMES_FOR_TABLE(
      NAME STRING)
  returns (
      FIELD_NAME STRING,
      DESCRIPT blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           )
   as
begin
  for select rdb$field_name, rdb$description from rdb$relation_fields
  where rdb$relation_name=:name into :field_name,:descript
  do
  begin
    suspend;
  end
end^
SET TERM ; ^
