--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_KOREKTACENAZAK_GUI(
      FAK integer,
      TYPWZ varchar(3) CHARACTER SET UTF8                           ,
      TYPZZ varchar(3) CHARACTER SET UTF8                           ,
      TYPPZ varchar(3) CHARACTER SET UTF8                           ,
      TYPZPZ varchar(3) CHARACTER SET UTF8                           ,
      TYPWZO varchar(3) CHARACTER SET UTF8                           ,
      TYPZZO varchar(3) CHARACTER SET UTF8                           ,
      TYPPZO varchar(3) CHARACTER SET UTF8                           ,
      TYPZPZO varchar(3) CHARACTER SET UTF8                           ,
      DATA timestamp,
      AKCEPT smallint)
  returns (
      STATUS integer,
      DOKSYM varchar(255) CHARACTER SET UTF8                           )
   as
declare variable magazyn varchar(3);
declare variable sstatus integer;
declare variable sdoksym varchar(20);
declare variable dokmag integer;
declare variable dokmagblok smallint;
begin
  status = 1;
  doksym = '';
  for select distinct MAGAZYN
  from POZFAK where DOKUMENT = :FAK
  into MAGAZYN
  do begin
    if(:status > 0 and :magazyn is not null) then begin
      execute procedure NAGFAK_KOREKTACENNEWDOK(:FAK,:MAGAZYN,:TYPWZ, :TYPZZ,:TYPPZ, :TYPZPZ,:TYPWZO, :TYPZZO, :TYPPZO, :TYPZPZO, :DATA,:AKCEPT) 

returning_values :sstatus, :sdoksym;
      if(:sstatus >0) then begin
        for select SYMBOL from DOKUMNAG where FAKTURA = :fak and zrodlo = 2 into  :sdoksym
        do begin
          if(:doksym <>'')then
            doksym = :doksym||';';
          doksym = :doksym || :sdoksym;
        end
      end else
        status = 0;
    end else if(:magazyn is null) then status = 2;
  end
end^
SET TERM ; ^
