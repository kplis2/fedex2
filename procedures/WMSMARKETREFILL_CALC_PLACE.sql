--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WMSMARKETREFILL_CALC_PLACE(
      WH DEFMAGAZ_ID)
   as
declare variable vers wersje_id;
declare variable mwsconstloc mwsconstlocs_id;
declare variable mwsconstlocsymbol string40;
declare variable good ktm_id;
declare variable whsec whsecs_id;
begin
  for
    select s.good, s.vers, min(c.symbol)
      from mwsstock s
        left join mwsconstlocsymbs l on l.wh = s.wh and l.vers = s.vers
        left join mwsconstlocs c on c.ref = s.mwsconstloc
        left join whsecs w on w.ref = c.whsec
      where s.wh = :wh and l.vers is null and w.autogoodsrefill = 1
      group by s.good, s.vers, s.wh
      into good, vers, mwsconstlocsymbol
  do begin
    mwsconstloc = null;
    select c.ref, whsec
      from mwsconstlocs c
      where c.symbol = :mwsconstlocsymbol
      into mwsconstloc, whsec;
    update or insert into mwsconstlocsymbs (mwsconstloc, avmode, symbol, const, vers, wh)
      values (:mwsconstloc, 1, :good, 1, :vers, :wh)
      matching (mwsconstloc, avmode, symbol);
    update or insert into wmsmarketrefill (wh, whsec, vers, good)
      values (:wh, :whsec, :vers, :good)
      matching (wh, whsec, vers);
  end
end^
SET TERM ; ^
