--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NOTIFSTATE_SET(
      NOTIFSTEPIN integer,
      IDIN varchar(20) CHARACTER SET UTF8                           ,
      PARENTIDIN varchar(20) CHARACTER SET UTF8                           ,
      NOTIFSHEETIN integer,
      DESCRIPTIN varchar(255) CHARACTER SET UTF8                           ,
      VALIN varchar(255) CHARACTER SET UTF8                           ,
      NUMVALIN varchar(255) CHARACTER SET UTF8                           ,
      WATCHBELOWIN varchar(20) CHARACTER SET UTF8                           ,
      COLORINDEXIN varchar(20) CHARACTER SET UTF8                           ,
      PRIORITYIN varchar(20) CHARACTER SET UTF8                           ,
      ICONIN varchar(255) CHARACTER SET UTF8                           ,
      MODULEIN varchar(255) CHARACTER SET UTF8                           ,
      ACTIONIDIN varchar(255) CHARACTER SET UTF8                           ,
      ACTIONPARAMSIN varchar(255) CHARACTER SET UTF8                           ,
      ACTIONNAMEIN varchar(255) CHARACTER SET UTF8                           ,
      RIGHTSGROUPIN varchar(255) CHARACTER SET UTF8                           ,
      RIGHTSIN varchar(255) CHARACTER SET UTF8                           ,
      COMPANYIN varchar(20) CHARACTER SET UTF8                           )
   as
declare variable oldid varchar(20);
declare variable oldnumval numeric(14,2);
declare variable oper integer;
declare variable defmedium integer;
declare variable msgtext varchar(255);
declare variable msgid integer;
declare variable symbol varchar(40);
declare variable operators varchar(255);
declare variable groups varchar(255);
declare variable sheetname varchar(255);
declare variable subject varchar(255);
declare variable aktuopergrupy varchar(1024);
declare variable notifcolorindex integer;
declare variable parentdescript varchar(255);
declare variable path varchar(255);
declare variable eol varchar(2);
declare variable numval numeric(14,2);
declare variable watchbelow smallint;
declare variable colorindex integer;
declare variable priority integer;
declare variable company integer;
begin
  eol='
';
  if(:numvalin<>'') then numval = cast(:numvalin as numeric(14,2));
  else numval = NULL;
  if(:watchbelowin='1') then watchbelow = 1;
  else watchbelow = 0;
  if(:colorindexin<>'') then colorindex = cast(:colorindexin as integer);
  else colorindex = 2;
  if(:priorityin<>'') then priority = cast(:priorityin as integer);
  else priority = 0;
  if(:companyin<>'') then company = cast(:companyin as integer);
  else company = NULL;
  oldid = null;
  oldnumval = null;
  select first 1 id,numval from notifstate where notifstep=:notifstepin and id=:idin and company=:company
  into :oldid, :oldnumval;
  if(:oldnumval is null) then oldnumval = 0;
  if(:oldid is null) then begin
    -- jesli nie ma tego rekordu w bazie to szukamy innego id do wykorzystania (ukrytego)
    select first 1 id from notifstate where notifstep=:notifstepin and hidden=1 and company=:company into :oldid;
  end
  if(:oldid is not null) then begin
    update NOTIFSTATE
     set ID = :idin,
         PARENTID = :parentidin,
         NOTIFSHEET = :notifsheetin,
         DESCRIPT = :descriptin,
         VAL = :valin,
         NUMVAL = :numval,
         WATCHBELOW = :watchbelow,
         COLORINDEX = :colorindex,
         PRIORITY = :priority,
         ICON = :iconin,
         MODULE = :modulein,
         ACTIONID = :actionidin,
         ACTIONPARAMS = :actionparamsin,
         ACTIONNAME = :actionnamein,
         rightsgroup = :rightsgroupin,
         RIGHTS = :rightsin,
         COMPANY = :company,
         HIDDEN = 0
     where notifstep=:notifstepin and id=:oldid and company=:company;
  end else begin
   insert into NOTIFSTATE(
         notifstep,
         ID,
         PARENTID,
         NOTIFSHEET,
         DESCRIPT,
         VAL,
         NUMVAL,
         WATCHBELOW,
         COLORINDEX,
         PRIORITY,
         ICON,
         MODULE,
         ACTIONID,
         ACTIONPARAMS,
         ACTIONNAME,
         rightsgroup,
         RIGHTS,
         COMPANY,
         HIDDEN)
   values (
         :notifstepin,
         :idin,
         :parentidin,
         :NOTIFSHEETIN,
         :DESCRIPTIN,
         :VALIN,
         :NUMVAL,
         :watchbelow,
         :COLORINDEX,
         :PRIORITY,
         :ICONIN,
         :MODULEIN,
         :ACTIONIDIN,
         :ACTIONPARAMSIN,
         :ACTIONNAMEIN,
         :rightsgroupin,
         :RIGHTSIN,
         :COMPANY,
         0);
  end

  if(:numval is not null and :numval>:oldnumval) then begin
    select name from notifsheetstates where ref=:notifsheetin into :sheetname;
    path = null;
    --jesli wzrosla (spadla) wartosc liczbowa to sprawdz czy nie nastapilo przekroczenie jakiegos progu
    for select operator, defmedium, operators, groups, colorindex
      from notifoperparams
      where notifstep=:notifstepin and id=:idin
      and ((threshold<=:numval and threshold>:oldnumval and :watchbelow=0) or
          (threshold>=:numval and threshold<:oldnumval and :watchbelow=1))
      and defmedium is not null
      order by colorindex
      into :oper,:defmedium, :operators, :groups, :notifcolorindex
    do begin
      if(:path is null) then begin
        -- uloz tresc powiadomienia
        path = '';
        while(coalesce(:parentidin,'')<>'') do begin
          parentdescript = '';
          select first 1 descript,parentid from notifstate
            where notifsheet=:notifsheetin and hidden=0
            and notifstep=:notifstepin and id=:parentidin
            into :parentdescript,:parentidin;
          if(:parentdescript<>'') then path = :parentdescript||' -> '||:path;
          else parentidin = '';
        end
        subject = 'Powiadomienie ('||:sheetname||')';
        msgtext = :descriptin ||' = '||:valin;
        if(:path<>'') then msgtext = :path||:eol||:msgtext;
        symbol = '';
        if(:idin<>'') then symbol = cast(:notifstepin as varchar(10))||:idin;
      end
      notifcolorindex = coalesce(:notifcolorindex,0);
      if(:colorindex>:notifcolorindex) then notifcolorindex = :colorindex;
      priority = :notifcolorindex-2;
      if(:priority<0) then priority = 0;
      if(:oper is not null) then begin
        execute procedure MESSAGE_ADD(0, :oper,'',:subject,:msgtext,:symbol,
          :actionidin,:actionparamsin,:actionnamein, '','','',0,
          :priority,:defmedium,:company) returning_values :msgid;
      end else begin
        for select ref,grupa from OPERATOR
        into :oper,:aktuopergrupy
        do begin
          if((:OPERATORS = '' and :GROUPS = '') or (:OPERATORS like '%%;'||:oper||';%%') OR (strmulticmp(';'||:aktuopergrupy||';',:GROUPS)=1)) then begin
            execute procedure MESSAGE_ADD(0, :oper,'',:subject,:msgtext,:symbol,
              :actionidin,:actionparamsin,:actionnamein, '','','',0,
              :priority,:defmedium,:company) returning_values :msgid;
          end
        end
      end
    end
  end
end^
SET TERM ; ^
