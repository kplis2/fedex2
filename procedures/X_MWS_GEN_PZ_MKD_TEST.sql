--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_GEN_PZ_MKD_TEST(
      NAGZAMREF NAGZAM_ID)
  returns (
      XREFNAG INTEGER_ID)
   as
begin
-- PROCEDURA DO TESTOWANIA
-- dodanie do tabeli przejsciowych x_imp_dokumnag i x_imp_dokumpoz
-- dokumentu PZ wygenerowanego z zamowienia zakupu.
    insert into X_IMP_DOKUMNAG (NAGID, SYMBOL, NUMER, TYP, DATA, MAGAZYN, MAG2, WYDANIA, ZEWNETRZNY, DOSTAWCAID,
                                DOSTAWA, UWAGIWEW, UWAGIZEW, UWAGISPED,
                                PRIORYTET, SOBOTA, ODROCZONEWYDANIE, STATUS,
                                DATAPRZET, IDSTATUS)
        select  N.REF, N.ID, N.NUMER, 'PZ', current_timestamp, N.MAGAZYN, N.MAG2, 0, 1, d.int_id,
                N.DOSTAWA, N.UWAGIWEWN, 'uwaga zewnetrzna' || N.ref, N.UWAGISPED,
                N.PRPRIORITY, 0, 0, 9, -- status 9 czyli do przetworzenia
                current_timestamp, N.ref
        from NAGZAM N
            left join dostawcy d on n.dostawca = d.ref
        where n.ref = :nagzamref
        returning ref into :xrefnag;
    insert into X_IMP_DOKUMPOZ (NAGID, POZID, NUMER, IDARTYKULU, JEDNOSTKA, VAT, MAGAZYN, MAG2, ILOSC, CENANETTO,
                                CENABRUTTO, WARTOSCNETTO, WARTOSCBRUTTO,
                                STATUS, DATAPRZET, IDSTATUS, nagimpref)
        select
                p.ZAMOWIENIE,p.REF, p.NUMER, t.int_id , tj.jedn, p.GR_VAT, p.MAGAZYN, p.MAG2, p.ILOSC, p.CENANET,
                p.CENABRU, p.WARTRNET, p.WARTRBRU,
                9, current_timestamp, p.ref, :xrefnag
        from POZZAM p
            left join towjedn tj on p.jedn = tj.ref
            join towary t on p.ktm = t.ktm
        where p.zamowienie = :nagzamref;
            
    suspend;
end^
SET TERM ; ^
