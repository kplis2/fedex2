--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SEARCH_SENSITIVE(
      SOURCE STRING,
      FROMREPLACE STRING,
      TOREPLACE STRING)
  returns (
      REPLACED STRING)
   as
declare variable SOURCE_LOWER STRING;
declare variable len integer;
declare variable i integer;
declare variable fi integer;
begin
  fromreplace = lower(fromreplace);
  replaced = source;
  if (source is null or fromreplace is null or toreplace is null) then
     exit;
  source_lower = lower(source);
  len = char_length(source);
  fi = char_length(fromreplace);
  i = position(fromreplace in source_lower);
  while (i > 0) do
  begin
    replaced = overlay(replaced placing toreplace from i for fi);
    source_lower = overlay(source_lower placing toreplace from i for fi);
    i = position(fromreplace in source_lower);
  end
  suspend;
end^
SET TERM ; ^
