--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CENYZAM_PRZELICZ(
      REFZAM integer)
  returns (
      TRYB varchar(1) CHARACTER SET UTF8                           )
   as
declare variable cnt integer;
declare variable wersjaref integer;
declare variable dostawca integer;
declare variable walutowe smallint;
declare variable waluta varchar(3);
declare variable jedn integer;
declare variable jedndost integer;
declare variable pozref integer;
declare variable ktm varchar(20);
declare variable przelzam numeric(14,4);
declare variable przeldost numeric(14,4);
declare variable iloscm numeric(14,4);
declare variable cena numeric(14,4);
declare variable kurs numeric(14,4);
declare variable walcen varchar(3);
declare variable oddzial varchar(20);
begin
  tryb = '0';
  select nagzam.dostawca, nagzam.walutowe, nagzam.waluta, nagzam.kurs, nagzam.oddzial from nagzam
    where ref = :REFZAM
    into :dostawca, :walutowe, :waluta, :kurs, :oddzial;
  if (:kurs is null or (:kurs = 0) ) then kurs = 1;
  if (:dostawca is null) then begin
    tryb = '1';
    exit;
  end
  for select ref, ktm, jedn, iloscm, wersjaref, walcen from pozzam where pozzam.zamowienie = :refzam
    into :pozref, :ktm, :jedn, :iloscm, :wersjaref, :walcen
  do begin
    select cenanet, jedn from get_dostcen(:wersjaref,:dostawca,:walcen,:oddzial,0)
      into :cena, :jedndost;
    if (:cena is null) then cena = 0;
    select przelicz from towjedn where towjedn.ref = :jedndost
      into :przeldost;
    select przelicz from towjedn where towjedn.ref = :jedn
      into :przelzam;
      update pozzam set cenacen = :cena*(:przelzam/:przeldost)/:kurs where ref = :pozref;
  end
end^
SET TERM ; ^
