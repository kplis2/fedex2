--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EHRM_GET_EMPL_PROFILES(
      PERSONREF PERSONS_ID)
  returns (
      REF EMPLOYEES_ID,
      PROFILE STRING255)
   as
begin
/*  ---------------------
    Procedura pobierająca refy z tabeli employees oraz opisy stanowiska pracy
    PERSONREF parametr wejsciowy, dla której osoby z tabeli PERSONS zwracamy wpisy z tabeli EMPLOYEES
    REF zwracany ref z tabeli EMPLOYEES
    PROFILE zwracany indywidualny opis profilu dla każdego wpisu w tabeli EMPLOYEES
*/  ---------------------
  for
    select e.ref, case when e.fromdate is null or coalesce(s.workpost,'') = ''  --e.empltype <> 1
                    then 'Umowa '||coalesce(t.tname,'')
                  else 'Umowa z dnia '|| cast(e.fromdate as date)||', '||
                    s.workpost end||', '||c.name
      from employees e
        join companies c on (e.company = c.ref)
        left join edictworkposts s on (e.workpost = s.ref)
        left join econtrtypes t on (e.econtrtype = t.contrtype)
      where e.ehrm = 1
        and e.person = :personref
        and e.empltype <> 0
    into :ref, :profile
  do
  begin
    suspend;
  end
end^
SET TERM ; ^
