--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKLINK_COPY(
      DOKLINKOLD integer,
      DOKLINKNEW integer,
      NEWFILE varchar(255) CHARACTER SET UTF8                           ,
      NEWLOCALFILE varchar(255) CHARACTER SET UTF8                           )
   as
declare variable WERSJA smallint;
begin
  select max(coalesce(dl.wersja,0)+1)
    from doklink dl
    join doklink dlo on dl.dokplik = dlo.dokplik and dl.symbol = dlo.symbol
    where dlo.ref = :doklinkold
  into :wersja;
  insert into doklink(ref, wersja, plik, dokszabl, format, rozmiar, dokplik, status, localfile, symbol, versioning)
    select :doklinknew, coalesce(:wersja,0), :newfile, dokszabl, format, rozmiar, dokplik, status, :newlocalfile, symbol, versioning
    from doklink
    where ref = :doklinkold;
  update doklink set status = 0 where ref = :doklinkold;
  suspend;
end^
SET TERM ; ^
