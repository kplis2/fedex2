--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REALZAM_JOINPOZ(
      DOKUMENT integer,
      TYP varchar(1) CHARACTER SET UTF8                           ,
      IDENT integer,
      WERSJAREF integer,
      ILOSC numeric(14,4))
   as
declare variable dokumpozref integer;
declare variable dokumpozref2 integer;
declare variable nagzamref integer;
declare variable iloscpoz numeric(14,4);
begin
  select zamowienie
    from pozzam
    where ref = :ident
    into :nagzamref;
  if (:typ = 'M') then
  begin
    select first 1 p.ref
      from dokumpoz p
      where p.dokument = :dokument
        and p.wersjaref = :wersjaref
        and p.frompozzam is null
        and p.ilosc = :ilosc
      into :dokumpozref;

    if (:dokumpozref is not null) then
      update dokumpoz p set p.fromzam = :nagzamref, p.frompozzam = :ident where p.ref = :dokumpozref;
    else begin
      for
        select p.ref, p.ilosc
          from dokumpoz p
          where p.dokument = :dokument
            and p.wersjaref = :wersjaref
            and p.frompozzam is null
            and p.ilosc > 0
          order by p.ilosc
          into :dokumpozref, :iloscpoz
      do begin
        if (:ilosc = 0) then break;
        if (:iloscpoz < :ilosc) then
          ilosc = ilosc - :iloscpoz;
        else begin
          execute procedure dokumpoz_clone(:dokumpozref, :ilosc) returning_values :dokumpozref2;
          ilosc = 0;
        end
        update dokumpoz p set p.fromzam = :nagzamref, p.frompozzam = :ident where p.ref = :dokumpozref;
      end
    end
  end
end^
SET TERM ; ^
