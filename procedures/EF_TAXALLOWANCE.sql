--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_TAXALLOWANCE(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      COL integer)
  returns (
      RET numeric(14,2))
   as
declare variable person integer;
  declare variable tyear integer;
  declare variable period varchar(6);
  declare variable currentvalue numeric(14,2);
  declare variable used numeric(14,2);
  declare variable allowance numeric(14,2);
  declare variable income numeric(14,2);
  declare variable nominal numeric(14,2);
  declare variable nfz numeric(14,2);
  declare variable tax numeric(14,2);
  declare variable payday timestamp;
begin
  --MW: personel - zwolnienia dochodu z opodatkowania i ulgi podatkowe
  select person
    from employees
    where ref = :employee
    into :person;

  select tper, payday
    from epayrolls
    where ref = :payroll
    into :period, :payday;

  select pvalue
    from eprpos
    where employee = :employee and payroll = :payroll and ecolumn = :col
    into :currentvalue;

  currentvalue = coalesce(currentvalue, 0);

  select sum(PP.pvalue)
    from eprpos PP join epayrolls PR on (PP.payroll = PR.ref)
      join employees E on (PP.employee = E.ref)
    where E.person = :person and PP.ecolumn = :col and PR.tper starting with substring(:period from 1 for 4)
    into :used;

  used = coalesce(used, 0);

  tyear = coalesce(cast(substring(period from 1 for 4) as integer),0);

--zwolnienia dochodu
  if (col = 7010) then
  begin
    select incallowance
      from etaxallowances
      where person = :person and ayear = :tyear
      into :allowance;

    select pvalue
      from eprpos
      where employee = :employee and payroll = :payroll and ecolumn = 7000
      into :income;

    allowance = coalesce(allowance, 0);
    income = coalesce(income, 0);
    ret = allowance - (used - currentvalue);

    if (ret > income) then
      ret = income;

    ret = round(ret);

    update etaxallowances set incused = :used - :currentvalue + :ret
      where person = :person and ayear = :tyear;
  end

--ulgi podatkowe
  if (col = 7300) then
  begin
    select taxallowance
      from etaxallowances
      where person = :person and ayear = :tyear
      into :allowance;

    select sum(P.pvalue)
      from eprpos P
        join epayrolls R on (P.payroll = R.ref)
      where P.employee = :employee and R.tper = :period
        and R.empltype = 1 and P.ecolumn = 7100
        and (R.payday < :payday or (R.payday = :payday and R.ref <= :payroll))
      into :nominal;

    select sum(P.pvalue)
      from eprpos P
        join epayrolls R on (P.payroll = R.ref)
      where P.employee = :employee and R.tper = :period
        and R.empltype = 1 and P.ecolumn = 7210
        and (R.payday < :payday or (R.payday = :payday and R.ref <= :payroll))
      into :nfz;

    select sum(P.pvalue)
      from eprpos P
        join epayrolls R on (P.payroll = R.ref)
      where P.employee = :employee and P.payroll <> :payroll and R.tper = :period
        and R.empltype = 1 and P.ecolumn = 7350
        and (R.payday < :payday or (R.payday = :payday and R.ref < :payroll))
      into :tax;

    allowance = coalesce(allowance, 0);
    nominal = coalesce(nominal, 0);
    nfz = coalesce(nfz, 0);
    tax = coalesce(tax, 0);

    ret = allowance - (used - currentvalue);

    if (ret > nominal - nfz - tax) then
      ret = nominal - nfz - tax;

    ret = round(ret);

    update etaxallowances set taxused = :used - :currentvalue + :ret
      where person = :person and ayear = :tyear;
  end

  suspend;
end^
SET TERM ; ^
