--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_GEN_LI_REALIZE as
declare variable expimpref integer_id;
declare variable mwsconstlocsymb string20;
declare variable mwsord mwsords_id;
declare variable kodkresk string20;
declare variable iai string20;
declare variable iaitmp string20;
declare variable ilosc ilosci_mag;
declare variable wersjaref wersje_id;
begin
  update expimp e set e.s4 = replace(e.s4,',','.');

  for
    select e.ref, e.s1, e.s2, e.s3, cast(e.s4 as ilosci_mag)
      from expimp e
      order by e.s1
    into :expimpref, :kodkresk, :iai, :mwsconstlocsymb, :ilosc
  do begin
    wersjaref = null;
    iaitmp = null;

    select w.ref, t.x_iai_id
      from towary t
        left join wersje w on (t.ktm = w.ktm and w.nrwersji = 0)
      where t.kodkresk = :kodkresk
    into :wersjaref, :iaitmp;

    if (:wersjaref is null) then
      select w.ref, t.x_iai_id
        from towary t
          left join wersje w on (t.ktm = w.ktm and w.nrwersji = 0)
        where t.x_iai_id = :iai
      into :wersjaref, :iaitmp;

    if (:wersjaref is not null and :iaitmp is distinct from :iai) then
      wersjaref = null;

    if (:wersjaref is null) then
    begin
      update expimp e set e.s10 = '1' where e.ref = :expimpref;
    end
    else
    begin
      mwsord = null;
  
      select o.ref
        from mwsords o
        where o.doctype = 'L'
          and o.status > 0 and o.status < 5
          and o.docsymbs = :mwsconstlocsymb
      into :mwsord;
  
      if (:mwsord is null) then
        execute procedure xk_mws_gen_mwsord_loc_stock(null, null, null, 'B', null, null, 1, null, null, 74, :mwsconstlocsymb, null, null)
          returning_values :mwsord;
  
      execute procedure xk_mws_loc_stock_commitquantity(:mwsord, :wersjaref, :ilosc, 0, null, 0, null);
    end
  end
  suspend;
end^
SET TERM ; ^
