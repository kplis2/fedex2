--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PAYROLLBELTS_UCP(
      BILLFROMDATE date,
      BILLTODATE date,
      PAYDAY date,
      STATUS smallint,
      COMPANY integer)
  returns (
      PAGENO integer,
      REF integer,
      PERSONNAMES varchar(120) CHARACTER SET UTF8                           ,
      PESEL varchar(11) CHARACTER SET UTF8                           ,
      NFZ varchar(20) CHARACTER SET UTF8                           )
   as
declare variable onpage integer;
declare variable lines integer;
declare variable i integer;
declare variable pref integer;
declare variable fdate timestamp;
begin
--MWr: Personel - Generowanie wydruku list plac dla rachunkow UCP

  status = coalesce(status,0);
  company = coalesce(company,1);
  pageno = 1;
  onpage = 41;
  lines = 0;

  for
   select distinct e.ref, e.personnames, p.pesel, p.ref
     from  epayrolls l
       join eprpos r on (l.ref = r.payroll and l.empltype = 2)
       join employees e on (r.employee = e.ref and e.company = :company)
       join persons p on (p.ref = e.person)
     where l.billdate >= :billfromdate
       and l.billdate <= :billtodate
       and (l.payday = :payday or :payday is null)
       and l.status >= :status
     order by e.sname, e.fname
     into :ref, :personnames, :pesel, :pref
  do begin
    select count(*)
      from rpt_e_payroll_ucp(:billfromdate, :billtodate, :payday, :status, :ref)
      into :i;

    if (lines + i > onpage) then
    begin
      pageno = pageno + 1;
      lines = i;
    end else
      lines = lines + i;

    lines = lines + 5;   --zwiekszam tez o 5 na nazwisko, podsumowania itd.

    fdate = null;
    nfz = null;
    select max(fromdate)
      from ezusdata where person = :pref and fromdate <= :billtodate
      into :fdate;

    if (fdate is not null) then
      select c.code
        from ezusdata z
          join edictzuscodes c on (z.nfz = c.ref)
        where z.person = :pref and z.fromdate = :fdate
        into :nfz;

    suspend;
  end
end^
SET TERM ; ^
