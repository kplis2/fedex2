--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_VAT27_2_POZ(
      EDECLARATIONREF EDECLARATIONS_ID,
      RPTSECTION CHAR_1)
  returns (
      NUMER INTEGER_ID,
      A SMALLINT_ID,
      B STRING255,
      C varchar(15) CHARACTER SET UTF8                           ,
      D numeric(14,2))
   as
declare variable edecpos_fieldsymbol type of column edeclpos.fieldsymbol;
declare variable edecpos_pvalue type of column edeclpos.pvalue;
declare variable subquery_id string10;
declare variable field_prefix string30;
declare variable field_prefix_end_char char_1;
begin
/*TS: FK - pobiera dane dla wydruku na podstawie danych wskazanej
  e-Deklaracji, dla wskazanej sekcji dokumentu.

  > EDECLARATIONREF - numer generowanej deklaracji z procedury nadrzednej (ta
    sama nazwa, bez '_POZ'), wartosc pola EDECLARATIONS.REF,
  > RPTSECTION - dla ktorej sekcji wydruku generujemy dane do e-Deklaracji.
*/

  -- Ustawiamy prefixy pol, ktore beda dodawane do EDECLPOS.
  subquery_id = :rptsection||'POZ';
  field_prefix_end_char = '.';
  field_prefix = :subquery_id||'_';

  -- Dokument VAT-27(2) posiada dwie sekcje ze zmienna liczba pozycji - C i D.
  if (:rptsection not in ( 'C', 'D' )) then
  begin
   exception universal 'Dla deklaracji VAT-27(2) nie istnieje sekcja danych: '||:rptsection||'! Podaj jedną z następujących: C, D.';
  end

  /* Przepisywanie danych z e-Deklaracji do wydruku.
     Przetwarzamy wszystkie pozycje dla eskazanej e-Deklaracji.
     Symbole pol sa formatu XPOZ_Y.Z, gdzie:
     > X to nazwa sekcji wydruku,
     > Y to numer wiersza w danej sekcji,
     > Z to nazwa pola danego wiersza.
  */
  for
    select ep.fieldsymbol, ep.pvalue
      from edeclarations e
        join edeclpos ep on (e.ref = ep.edeclaration)
      where e.ref = :edeclarationref
        and ep.fieldsymbol starting with :field_prefix
      order by ep.field
    into :edecpos_fieldsymbol, :edecpos_pvalue
  do begin
    edecpos_fieldsymbol = substring(:edecpos_fieldsymbol
      from position(:field_prefix_end_char,:edecpos_fieldsymbol) + 1);
    if(:edecpos_fieldsymbol = 'NUMER') then numer = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = 'A') then a = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = 'B') then b = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = 'C') then c = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = 'D') then begin
      d = :edecpos_pvalue;
      suspend;
    end
  end
end^
SET TERM ; ^
