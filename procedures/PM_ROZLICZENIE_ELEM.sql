--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PM_ROZLICZENIE_ELEM(
      PMELEMENT integer,
      DATAOD date,
      DATADO date)
  returns (
      REF integer,
      STYP varchar(1) CHARACTER SET UTF8                           ,
      KWOTA numeric(14,2),
      NUMERPOZ integer,
      NUMERDOK integer,
      SYMBOL varchar(40) CHARACTER SET UTF8                           ,
      ILOSC numeric(14,4),
      KTM varchar(40) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      DATA timestamp,
      ZAPLACONO numeric(14,2))
   as
declare variable dozaplaty numeric(14,2);
declare variable zaplfak numeric(14,2);
begin
  for
  select nf.ref, 'U', pfu.kwota, pf.numer, nf.numer, nf.symbol, pf.ilosc, pf.ktm, pf.nazwat, nf.dataotrz, nf.esumwartbru-nf.pesumwartbru, nf.zaplacono
    from pozfakusl pfu
    left join pozfak pf on (pf.ref = pfu.refpozfak)
    left join nagfak nf on (nf.ref = pf.dokument)
    left join typfak tf on (tf.symbol = nf.typ)
    where pfu.pmelement = :pmelement
          and nf.akceptacja = 1 and tf.rozliczpo = 1 and tf.zakup = 0
          and (:dataod <= nf.dataotrz or :dataod is null)
          and (:datado >= nf.dataotrz or :datado is null)
  into :ref, :styp, :kwota, :numerpoz, :numerdok, :symbol, :ilosc, :ktm, :nazwa, :data, :dozaplaty, :zaplfak
  do begin
    if(:dozaplaty=0) then zaplacono = 0;
    else if(:zaplfak=:dozaplaty) then zaplacono = :kwota;
    else zaplacono = :kwota * :zaplfak/:dozaplaty;
    suspend;
  end
end^
SET TERM ; ^
