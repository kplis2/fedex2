--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_ST_TR2(
      ROK smallint,
      WHERE_GRUPA varchar(10) CHARACTER SET UTF8                           ,
      COMPANY integer)
  returns (
      GRUPA varchar(10) CHARACTER SET UTF8                           ,
      G varchar(1) CHARACTER SET UTF8                           ,
      NRINWENT varchar(20) CHARACTER SET UTF8                           ,
      NAZWA varchar(80) CHARACTER SET UTF8                           ,
      WARTOSCPODATKOWAPOCZATEK numeric(14,2),
      WARTOSCFINANSOWAPOCZATEK numeric(14,2),
      ZMIANAWARTOSCPODATKOWA numeric(14,2),
      ZMIANAWARTOSCFINANSOWA numeric(14,2),
      SKRESLONEPODATKOWO numeric(14,2),
      SKRESLONEFINANSOWO numeric(14,2),
      WARTOSCPODATKOWAKONIEC numeric(14,2),
      WARTOSCFINANSOWAKONIEC numeric(14,2),
      UMORZENIEPODATKOWEPOCZATEK numeric(14,2),
      UMORZENIEFINANSOWEPOCZATEK numeric(14,2),
      UMORZENIEPODATKOWE numeric(14,2),
      UMORZENIEFINANSOWE numeric(14,2),
      ZMIANAUMORZENIEPODATKOWE numeric(14,2),
      ZMIANAUMORZENIEFINANSOWE numeric(14,2),
      SKRESLONEUMORZENIEPODATKOWO numeric(14,2),
      SKRESLONEUMORZENIEFINANSOWO numeric(14,2),
      UMORZENIEPODATKOWEKONIEC numeric(14,2),
      UMORZENIEFINANSOWEKONIEC numeric(14,2))
   as
declare variable LIQUIDATIONDATE timestamp;
declare variable FXDASSETSREF integer;
declare variable TMP1 numeric(14,2);
declare variable TMP2 numeric(14,2);
begin
  for
    select D.ref, D.amortgrp, D.name, D.symbol, D.liquidationdate
      from fxdassets D
      where D.amortgrp like :WHERE_GRUPA || '%' and D.company = :company
      order by D.amortgrp
      into fxdassetsREF, grupa, nazwa, nrInwent, liquidationdate
  do begin
    --obsuga skrelenia rodka z kartoteki przed rozpoczciem analizowanego roku
    if (cast(substring(liquidationdate from 1 for 4)as integer) >= :ROK or liquidationdate is null  ) then begin
      g = substring(grupa from 1 for 1);
  
      wartoscPodatkowaPoczatek = 0;
      wartoscFinansowaPoczatek = 0;
      zmianaWartoscPodatkowa = 0;
      zmianaWartoscFinansowa = 0;
      wartoscPodatkowaPoczatek = 0;
      zmianaWartoscFinansowa = 0;
      SKRESLONEPODATKOWO = 0;
      SKRESLONEFINANSOWO = 0;
      WARTOSCPODATKOWAKONIEC =0;
      WARTOSCFINANSOWAKONIEC = 0;
      UMORZENIEPODATKOWEPOCZATEK = 0 ;
      UMORZENIEFINANSOWEPOCZATEK = 0;
      UMORZENIEPODATKOWE = 0;
      UMORZENIEFINANSOWE = 0;
      ZMIANAUMORZENIEPODATKOWE = 0;
      ZMIANAUMORZENIEFINANSOWE = 0;
      SKRESLONEUMORZENIEPODATKOWO = 0;
      SKRESLONEUMORZENIEFINANSOWO = 0;
      UMORZENIEPODATKOWEKONIEC = 0;
      UMORZENIEFINANSOWEKONIEC = 0;
  
      -- wartosci:
      tmp1 = 0;
      tmp2 = 0;
  
      select D.tvalue, D.fvalue
        from fxdassets D
        where D.ref = :fxdassetsREF and (substring(D.usingdt from 1 for 4) < :rok or D.usingdt = :rok || '-01-01')
        into :wartoscPodatkowaPoczatek, :wartoscFinansowaPoczatek;
  
      if (wartoscPodatkowaPoczatek is null) then wartoscPodatkowaPoczatek = 0;
      if (wartoscFinansowaPoczatek is null) then wartoscFinansowaPoczatek = 0;
  
      select sum(VD.tvalue), sum(VD.fvalue)
        from valdocs VD
        left join vdoctype VT on (VT.symbol = VD.doctype)
        where ((VT.tvalue+VT.fvalue)>0 or (VT.liquidation = 1)) and VD.fxdasset = :fxdassetsREF and substring(VD.docdate from 1 for 4) < :ROK
        into :tmp1, :tmp2;  -- dokumenty zmieniajace wartosc finansowo i podatkowo na poczatek roku
  
      if (tmp1 is not null) then
        wartoscPodatkowaPoczatek = wartoscPodatkowaPoczatek + tmp1;
      if (tmp2 is not null) then
        wartoscFinansowaPoczatek = wartoscFinansowaPoczatek + tmp2;
  
      zmianaWartoscPodatkowa = 0;
      zmianaWartoscFinansowa = 0;

      -- wszystkie zakupy w ciagu roku ewidencjonowane jako zmiana, a nie wartosc poczatkowa
      select D.tvalue, D.fvalue
        from fxdassets D
        where D.ref = :fxdassetsREF and substring(D.usingdt from 1 for 4) = :rok and D.usingdt > :rok || '-01-01'
        into :zmianaWartoscPodatkowa, :zmianaWartoscFinansowa;
  
      if (zmianaWartoscPodatkowa is null) then zmianaWartoscPodatkowa = 0;
      if (zmianaWartoscFinansowa is null) then zmianaWartoscFinansowa = 0;
  
      tmp1 = 0;
      tmp2 = 0;
  
      select sum(VD.tvalue), sum(VD.fvalue)
        from valdocs VD
        left join vdoctype VT on (VT.symbol = VD.doctype)
        where (VT.tvalue+VT.fvalue)>0 and VT.liquidation <> 1 and VD.fxdasset = :fxdassetsREF and substring(VD.docdate from 1 for 4) = :ROK
        into :tmp1, :tmp2;  -- dokumenty zwiekszajace wartosc finansowo i podatkowo w danym roku
  
  --      into :zmianaWartoscPodatkowa, :zmianaWartoscFinansowa;  -- dokumenty zwiekszajace wartosc finansowo i podatkowo w danym roku
  
      if (tmp1 is not null) then
        zmianaWartoscPodatkowa = zmianaWartoscPodatkowa + tmp1;
      if (tmp2 is not null) then
        zmianaWartoscFinansowa = zmianaWartoscFinansowa + tmp2;
  
  
  --    if (zmianaWartoscPodatkowa is null) then zmianaWartoscPodatkowa = 0;
  --    if (zmianaWartoscFinansowa is null) then zmianaWartoscFinansowa = 0;
  
      select sum(VD.tvalue), sum(VD.fvalue)
        from valdocs VD
        left join vdoctype VT on (VT.symbol = VD.doctype)
        where VT.liquidation = 1 and VD.fxdasset = :fxdassetsREF and substring(VD.docdate from 1 for 4) = :ROK
        into :skreslonePodatkowo, :skresloneFinansowo;  -- dokumenty skreslajace w danym roku
  
      if (skreslonePodatkowo is null) then skreslonePodatkowo = 0;
      if (skresloneFinansowo is null) then skresloneFinansowo = 0;
  
      wartoscPodatkowaKoniec = wartoscPodatkowaPoczatek + zmianaWartoscPodatkowa + skreslonePodatkowo;
      wartoscFinansowaKoniec = wartoscFinansowaPoczatek + zmianaWartoscFinansowa + skresloneFinansowo;
  
      -- umorzenia:
      --watosc umorzenia w chwili wprowadzenia rodka
      select f.tredemption, f.fredemption
        from fxdassets f
        where f.ref = :fxdassetsREF and (substring(f.usingdt from 1 for 4) < :rok or f.usingdt = :rok || '-01-01')
      into :umorzeniePodatkowePoczatek, :umorzenieFinansowePoczatek; -- umorzenie przed poczatkiem roku z amortyzacji
  
      if (umorzeniePodatkowePoczatek is null) then umorzeniePodatkowePoczatek = 0;
      if (umorzenieFinansowePoczatek is null) then umorzenieFinansowePoczatek = 0;
  
      tmp1 = 0;
      tmp2 = 0;
  
      select sum(A.tamort), sum(A.famort)
        from amortization A
        where A.fxdasset = :fxdassetsREF and A.amyear < :ROK
      into :tmp1, :tmp2;
      
      if (tmp1 is not null) then umorzeniePodatkowePoczatek = umorzeniePodatkowePoczatek + tmp1;
      if (tmp2 is not null) then umorzenieFinansowePoczatek = umorzenieFinansowePoczatek + tmp2;
  
      tmp1 = 0;
      tmp2 = 0;
  
      select sum(VD.tredemption), sum(VD.fredemption)
         from valdocs VD
         left join vdoctype VT on (VT.symbol = VD.doctype)
        where ((VT.tredemption+VT.fredemption)>0 or VT.liquidation = 1) and coalesce(Vt.amcorrection, 0) = 0 and VD.fxdasset = :fxdassetsREF and substring(VD.docdate from 1 for 4) < :ROK
        into :tmp1, :tmp2; -- umorzenie przed poczatkiem roku z dokumentow
  
      if (tmp1 is not null) then umorzeniePodatkowePoczatek = umorzeniePodatkowePoczatek + tmp1;
      if (tmp2 is not null) then umorzenieFinansowePoczatek = umorzenieFinansowePoczatek + tmp2;
  
      select sum(A.tamort), sum(A.famort)
        from amortization A
        where A.fxdasset = :fxdassetsREF and A.amyear = :ROK
        into :umorzeniePodatkowe, :umorzenieFinansowe; -- umorzenie w danym roku
  
      if (umorzeniePodatkowe is null) then umorzeniePodatkowe = 0;
      if (umorzenieFinansowe is null) then umorzenieFinansowe = 0;
  
      select sum(VD.tredemption), sum(VD.fredemption)
        from valdocs VD
        left join vdoctype VT on (VT.symbol = VD.doctype)
        where (VT.tredemption+VT.fredemption)>0 and VT.liquidation <> 1 and coalesce(Vt.amcorrection, 0) = 0 and  VD.fxdasset = :fxdassetsREF and substring(VD.docdate from 1 for 4) = :ROK
        into :zmianaUmorzeniePodatkowe, :zmianaUmorzenieFinansowe;  -- dokumenty zwiekszajace umorzenie finansowo i podatkowo w danym roku
  
      if (zmianaUmorzeniePodatkowe is null) then zmianaUmorzeniePodatkowe = 0;
      if (zmianaUmorzenieFinansowe is null) then zmianaUmorzenieFinansowe = 0;

      tmp1 = 0;
      tmp2 = 0;

      select D.tredemption, D.fredemption
        from fxdassets D
        where D.ref = :fxdassetsREF and substring(D.usingdt from 1 for 4) = :rok and D.usingdt > :rok || '-01-01'
        into :tmp1, :tmp2;
  
      if (tmp1 is not null) then UmorzeniePodatkowe = UmorzeniePodatkowe + tmp1;
      if (tmp2 is not null) then UmorzenieFinansowe = UmorzenieFinansowe + tmp2;
  
      select sum(VD.tredemption), sum(VD.fredemption)
        from valdocs VD
        left join vdoctype VT on (VT.symbol = VD.doctype)
        where VT.liquidation = 1 and VD.fxdasset = :fxdassetsREF and substring(VD.docdate from 1 for 4) = :ROK
        into :skresloneUmorzeniePodatkowo, :skresloneUmorzenieFinansowo;  -- dokumenty skreslajace umorzenie finansowo i podatkowo w danym roku
  
      if (skresloneUmorzeniePodatkowo is null) then skresloneUmorzeniePodatkowo = 0;
      if (skresloneUmorzenieFinansowo is null) then skresloneUmorzenieFinansowo = 0;
  
      umorzeniePodatkoweKoniec = umorzeniePodatkowePoczatek + umorzeniePodatkowe + zmianaUmorzeniePodatkowe + skresloneUmorzeniePodatkowo;
      umorzenieFinansoweKoniec = umorzenieFinansowePoczatek + umorzenieFinansowe + zmianaUmorzenieFinansowe + skresloneUmorzenieFinansowo;
  
      suspend;
    end      
  end
end^
SET TERM ; ^
