--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_FAKTURUJDOKMAG(
      FAKTURA integer,
      STAN varchar(20) CHARACTER SET UTF8                           ,
      REJFAK varchar(3) CHARACTER SET UTF8                           ,
      TYPFAK varchar(3) CHARACTER SET UTF8                           ,
      OPERATOR integer,
      DOKMAG integer,
      DATA timestamp,
      COPYCEN smallint,
      DOMAGKOR smallint,
      DOMAGOPKROZ smallint,
      PLATNIK integer,
      FAKTURAZAL integer,
      KUMULACJA smallint,
      DOKWOLNY integer)
  returns (
      NEWFAK integer)
   as
declare variable BN char(1);
declare variable TYPDOK varchar(3);
declare variable DOSTAWCA integer;
declare variable KLIENT integer;
declare variable ISDOSTAWCA integer;
declare variable ISKLIENT integer;
declare variable ISPLATNIK integer;
declare variable ZAKUP smallint;
declare variable SPOSZAP integer;
declare variable TERMIN integer;
declare variable SPOSZAP2 integer;
declare variable TERMIN2 integer;
declare variable DATAZAP timestamp;
declare variable RABNAG numeric(14,4);
declare variable RABAT numeric(14,4);
declare variable RABATTAB numeric(14,4);
declare variable WALUTA varchar(3);
declare variable WALUTA2 varchar(3);
declare variable ISWALUTA varchar(3);
declare variable SPRZEDAWCA integer;
declare variable SPRZEDAWCAZEW integer;
declare variable SPRZEDAWCA2 integer;
declare variable SPRZEDAWCAZEW2 integer;
declare variable WALPLN varchar(3);
declare variable WALUTOWE smallint;
declare variable CENNIK integer;
declare variable KTM varchar(40);
declare variable WERSJA integer;
declare variable WERSJAREF integer;
declare variable CENACEN numeric(14,4);
declare variable CENAMAG numeric(14,4);
declare variable PCENAMAG numeric(14,4);
declare variable CENASPRZ numeric(14,4);
declare variable WALCEN varchar(3);
declare variable JEDN integer;
declare variable JEDNO integer;
declare variable JEDNM integer;
declare variable ILOSC numeric(14,4);
declare variable ILOSCM numeric(14,4);
declare variable ILOSCO numeric(14,4);
declare variable ILOSCMAGMIN numeric(14,4);
declare variable ISCENA smallint;
declare variable WALRABCEN varchar(3);
declare variable MAGAZYN varchar(3);
declare variable OPK smallint;
declare variable DOKOPK smallint;
declare variable DOKCENSPRZ smallint;
declare variable KOSZTDOST numeric(14,4);
declare variable SUMZAL numeric(14,2);
declare variable DOKPOZ integer;
declare variable ROK integer;
declare variable MIESIAC integer;
declare variable DOKBN char(1);
declare variable VAT numeric(14,4);
declare variable KURS numeric(14,4);
declare variable ISKURS numeric(14,4);
declare variable SAD integer;
declare variable CENAWAL numeric(14,4);
declare variable FOC integer;
declare variable PRZELICZO numeric(14,4);
declare variable PRZELICZC numeric(14,4);
declare variable DOKMAGKOR integer;
declare variable ISFAKTURA integer;
declare variable KATEGA integer;
declare variable KATEGB integer;
declare variable KATEGC integer;
declare variable ZAM integer;
declare variable POZFAKREF integer;
declare variable DOKPOZKORREF integer;
declare variable OBLCENZAK integer;
declare variable OBLCENKOSZT integer;
declare variable OBLCENFAB integer;
declare variable DOKUMSPED integer;
declare variable SSPOSDOST integer;
declare variable SKOSZTDOSTPLAT integer;
declare variable STERMDOST timestamp;
declare variable SDULICA varchar(255);
declare variable SDNRDOMU NRDOMU_ID;
declare variable SDNRLOKALU NRDOMU_ID;
declare variable SDMIASTO varchar(255);
declare variable SDKODP varchar(255);
declare variable SODBIORCA varchar(255);
declare variable SODBIORCAID integer;
declare variable CNT integer;
declare variable SLODEFREF integer;
declare variable SLOPOZREF integer;
declare variable KOD varchar(100);
declare variable NAZWA varchar(255);
declare variable KONTOFK KONTO_ID;
declare variable SYMBOL varchar(40);
declare variable DOKDATA timestamp;
declare variable DATASPRZ timestamp;
declare variable DATAOTRZ timestamp;
declare variable CKONTRAKTY integer;
declare variable WYSYLKA integer;
declare variable WYDANIA integer;
declare variable PARAMN1 numeric(14,4);
declare variable PARAMN2 numeric(14,4);
declare variable PARAMN3 numeric(14,4);
declare variable PARAMN4 numeric(14,4);
declare variable PARAMN5 numeric(14,4);
declare variable PARAMN6 numeric(14,4);
declare variable PARAMN7 numeric(14,4);
declare variable PARAMN8 numeric(14,4);
declare variable PARAMN9 numeric(14,4);
declare variable PARAMN10 numeric(14,4);
declare variable PARAMN11 numeric(14,4);
declare variable PARAMN12 numeric(14,4);
declare variable PARAMS1 varchar(255);
declare variable PARAMS2 varchar(255);
declare variable PARAMS3 varchar(255);
declare variable PARAMS4 varchar(255);
declare variable PARAMS5 varchar(255);
declare variable PARAMS6 varchar(255);
declare variable PARAMS7 varchar(255);
declare variable PARAMS8 varchar(255);
declare variable PARAMS9 varchar(255);
declare variable PARAMS10 varchar(255);
declare variable PARAMS11 varchar(255);
declare variable PARAMS12 varchar(255);
declare variable PARAMD1 timestamp;
declare variable PARAMD2 timestamp;
declare variable PARAMD3 timestamp;
declare variable PARAMD4 timestamp;
declare variable KRAJWYSYLKI varchar(20);
declare variable KRAJPOCHODZENIA varchar(20);
declare variable FAKTURARR integer;
declare variable SPOSZAPSTR varchar(255);
declare variable FROMPOZZAM integer;
declare variable POZZAMJEDN integer;
declare variable POZZAMILOSC numeric(14,4);
declare variable POZZAMILOSCM numeric(14,4);
declare variable POZNUMER integer;
declare variable DOKMAGWOKRESIEKONTROLA smallint;
declare variable DOKMAGWOKRESIEFAKTURA integer;
declare variable DOKMAGWOKRESIEOKRESCNT integer;
declare variable UNITTOUNIT varchar(255);
declare variable ZPAKIETEM smallint;
declare variable PROMOCJA integer;
declare variable ODDZIAL varchar(20);
declare variable PLATNIKZAM integer;
declare variable REFDOPAKIET integer;
declare variable POZFREF integer;
declare variable DOKPOZREF integer;
declare variable WARTNET numeric(14,2);
declare variable WARTBRU numeric(14,2);
declare variable PREC integer;
declare variable SRVREQUEST integer;
declare variable SLODEFR integer;
declare variable SLOPOZR integer;
declare variable KONTOFKR KONTO_ID;
declare variable SYMBFAKR varchar(20);
declare variable WALUTAR varchar(3);
declare variable WINIENR numeric(14,2);
declare variable MAR numeric(14,2);
declare variable PAYDAYR timestamp;
declare variable ZAMWALUTA varchar(3);
declare variable ZAMWALUTOWE smallint;
declare variable ZEWN smallint;
declare variable DOSAD smallint;
declare variable OPIS varchar(1024);
declare variable DOKMAGPLATNIK integer;
declare variable FROMPROMOREF integer;
declare variable ILOSCZADOPAKIET numeric(14,4);
declare variable FAKTURANAPLATNIKA smallint;
declare variable MINDOKUMROZ integer;
declare variable MAXDOKUMROZ integer;
declare variable CENANOTZAKUPU numeric(14,4);
declare variable GR_VAT varchar(5);
declare variable SKAD smallint;
declare variable VAKCEPTACJA integer;
declare variable ALTTOPOZ DOKUMPOZ_ID;
declare variable ALTTOPOZN POZFAK_ID;
declare variable FAKE smallint;
declare variable HAVEFAKE POZFAK_ID;
begin
  if (exists(select first 1 1 from dokumpoz where dokument = :dokmag and fake = 1) and
      exists(select first 1 1 from stantypfak where stansprzed = :stan and typfak = :typfak and coalesce(altallow,0) = 0)) then
    exception FAKE_NOT_ALLOW 'Pozycje aleternatywne nie dozwolone dla stanowiska i typu dokumentu sprzedaży!';

  fakturanaplatnika = 0;
  if(:platnik<>0) then fakturanaplatnika = 1;
  if (dokwolny is null) then dokwolny = 0;
  select SAD,FAKTURARR, ZAKUP from TYPFAK where SYMBOL=:typfak into :sad,:fakturarr, :zakup;
  if(:kumulacja=1 and :sad=1) then kumulacja = 0; /* MS: na SAD nie moze byc kumulacji bo kursy sa z PZI */
  if(:copycen is null) then copycen = 0;
  execute procedure GETCONFIG('WALPLN') returning_values :walpln;
  if(:walpln is null) then walpln = 'PLN';
  select data, ckontrakty, klient, platnik, dostawca, magazyn, typ, kosztdost, kwotazal, waluta, kurs, walutowy, zamowienie, symbol, wysylka,
         dulica, dnrdomu, dnrlokalu, dmiasto, dkodp, odbiorca, odbiorcaid, krajwysylki, sposdost, srvrequest, sposzap, dnizap, sprzedawca,
         sprzedawcazew, bn, akcept
  from DOKUMNAG where ref=:dokmag
  into :dokdata, :CKONTRAKTY, :klient, :dokmagplatnik, :dostawca, :magazyn, :typdok, :kosztdost, :sumzal, :waluta, :kurs, :walutowe, :zam, :symbol, :wysylka,
       :sdulica, :sdnrdomu, sdnrlokalu, :sdmiasto, :sdkodp, :sodbiorca, :sodbiorcaid, :krajwysylki, :ssposdost, :srvrequest, :sposzap, :termin, :sprzedawca,
       :sprzedawcazew, :dokbn, :vakceptacja;
  -- sprawdzenie czy dany dokumnag jest zaakceptowany
  if (:vakceptacja<>1) then
    exception dokumnag_akcept 'Dokument magazynowy jest niezaakceptowany. Nie można fakturować.';
  if (:zakup = 1) then dataotrz = :data;
  else dataotrz = :dokdata;
  if(:dokbn is null or (:dokbn = '') or (:dokbn = ' '))then dokbn = 'N';
  if(:fakturanaplatnika=0) then begin
    if(:dokmagplatnik is not null) then platnik = :dokmagplatnik;
    else platnik = :klient;
  end else begin
    if(:KLIENT > 0) then
      klient = :platnik;
    if(:dostawca > 0) then begin
      dostawca = :platnik;
      platnik = null;
    end
  end
  if(:kosztdost is null) then kosztdost = 0;
  if(:sumzal is null) then sumzal = 0;
  if(:waluta is null or (:waluta = ''))then waluta = :walpln;
  if(:walutowe is null) then walutowe = 0;
  if(:kurs is null or (:kurs = 0)) then kurs = 1;
  if(:sad=1) then kurs = 1; /*MS: usuwamy kurs na SADzie, ceny PLN przenoszone sa z DOKUMPOZ*/
  select OPAK, WYDANIA, ZEWN, DOSAD from DEFDOKUM where symbol = :typdok into :dokopk, :wydania, :zewn, :dosad;

  if(:dokopk is null) then dokopk = 0;
  if(:sad=1 and :dosad<>1) then exception NAGFAK_EXPT 'Do wybranego dokumentu nie można wystawić SAD.';
  select redcensprz from DEFDOKUMMAG where magazyn = :magazyn and typ = :typdok into :dokcensprz;
  if(:dokcensprz is null) then dokcensprz = 0;
  if(:dokopk > 0) then dokcensprz = 1;
  if(:zewn=0 and :wydania=0 and :dokcensprz=0) then copycen = -1; /*MS: kopiuj ceny magazynowe*/
  else if(:dokcensprz = 0) then copycen = 0;  /*wymuszenie nadawania cen z automatu*/
  /*jesli dok. mag. zwiazany z zamowieniem, to skopiowanie warunkow z zamowienia*/
  platnikzam = null;
  if(:zam > 0) then begin
    select NAGZAM.SPRZEDAWCA, NAGZAM.SPRZEDAWCAZEW, NAGZAM.KATEGA, NAGZAM.KATEGB, NAGZAM.KATEGC, NAGZAM.PLATNIK,
          NAGZAM.sposzap, nagzam.termzap, nagzam.rabat, NAGZAM.WALUTOWE, NAGZAM.WALUTA
    from NAGZAM where REF=:zam
    into :sprzedawca,:sprzedawcazew, :katega, :kategb, :kategc, :platnikzam,
      :sposzap, :termin, :rabnag, :zamwalutowe, :zamwaluta;

    if(:walutowe=0) then begin --jesli nie ma waluty na dok mag to ustal walute z zamowienia
      walutowe = :zamwalutowe;
      waluta = :zamwaluta;
    end
    if(:waluta is null or (:waluta = ''))then waluta = :walpln;
    if(:walutowe is null) then walutowe = 0;
    if(:platnikzam is not null) then platnik = :platnikzam;
  end
  if(:faktura = 0 or (:faktura is null))then begin
    if(:dokwolny = 0 and exists(select first 1 1 from nagfak where nagfak.refdokm=:dokmag)) then --sprawdzenie czy juz nie ma faktur do dok.magazynowego
      exception dokumnag_posiadafak 'Do dokumentu: '||:symbol||' faktura została już wystawiona.';
    execute procedure GEN_REF('NAGFAK') returning_values :faktura;
    if(:klient > 0) then begin
      select KLIENCI.sposplat, klienci.termin, klienci.upust, klienci.waluta, klienci.sprzedawca, klienci.sprzedawcazew
      from KLIENCI where REF=:klient
      into :sposzap2, :termin2, :rabnag,:waluta2,:sprzedawca2, :sprzedawcazew2;
    end else if(:dostawca > 0) then begin
      select dostawcy.sposplat, dostawcy.dniplat, dostawcy.rabat, dostawcy.waluta
        from DOSTAWCY where REF=:dostawca
        into :sposzap2, :termin2, :rabnag,:waluta2;
      if(:fakturarr=1) then begin
        execute procedure GETCONFIG('PLATNOSCRR') returning_values :sposzapstr;
        if(:sposzapstr<>'') then begin
          select REF,TERMIN from PLATNOSCI where REF=:sposzapstr into :sposzap, :termin;
        end
      end
    end else if(:sad=0) then
      exception NAGFAK_DOKFROMMAG_BK;
    if(:walutowe=0) then begin --jesli nie ma waluty na dok mag ani na zamowieniu to ustal walute z klienta lub polska
      waluta = :waluta2;
    end
    if(:rabnag is null) then rabnag = 0;
    if(:waluta is null or (:waluta = ''))then waluta = :walpln;
    if(:waluta <> :walpln) then walutowe = 1;
    if(:sposzap is null) then sposzap = :sposzap2;
    if(:termin is null) then termin = :termin2;
    if(:sprzedawca is null) then sprzedawca = :sprzedawca2;
    if(:sprzedawcazew is null) then sprzedawcazew = :sprzedawcazew2;
    if(:sposzap is null or (:sposzap = 0))then
      execute procedure GETCONFIG('SPOSZAP') returning_values :sposzap;
    if((:termin is null) or (:termin = 0)) then
      select PLATNOSCI.termin from PLATNOSCI where ref=:sposzap into :termin;
    if(:termin is null) then termin = 0;
    if(:data is null) then data = current_date;
    datazap = :data + :termin;
    if(:sad=1) then dostawca = NULL;
    select min(LISTYWYSD.ref)
    from LISTYWYSDPOZ join LISTYWYSD on (LISTYWYSDPOZ.dokument = LISTYWYSD.ref)
    where LISTYWYSD.akcept = 1 and LISTYWYSDPOZ.DOKTYP = 'M' and LISTYWYSDPOZ.DOKREF=:dokmag
    into :dokumsped;
    if(:dokumsped > 0) then begin
      select sposdost, kosztdostplat, termdost, adres, miasto, kodp, odbiorcaid
      from LISTYWYSD where ref=:dokumsped
      into :ssposdost, :skosztdostplat, :stermdost, :sdulica, :sdmiasto, :sdkodp, :sodbiorcaid;
    end
/*    if(:fakturazal > 0) then begin
      select sum(KWOTANET), sum(KWOTABRU), sum(KWOTANETZL), sum(KWOTABRUZL)
        from NAGFAK_GET_ZALICZKI(:fakturazal)
        into :zalkwotanet, :zalkwotabru, :zalkwotanetzl, :zalkwotabruzl;
      refk = :fakturazal;
    end*/
    insert into NAGFAK(REF, STANOWISKO,REJESTR,TYP, DATA, DATASPRZ, DATAOTRZ, KLIENT,DOSTAWCA,PLATNIK,SPOSZAP,TERMZAP,TERMIN,
              SUMWARTNET, SUMWARTBRU, PSUMWARTNET, PSUMWARTBRU,
--              PESUMWARTNET, PESUMWARTBRU, PESUMWARTNETZL, PESUMWARTBRUZL,
              RABAT,AKCEPTACJA, /*REFK,*/ SYMBOLK, REFDOKM, ZAPLACONO,
              WALUTA, WALUTOWA, KURS, KRAJWYSYLKI,
              SPRZEDAWCA, SPRZEDAWCAZEW, OPERATOR, KOSZTDOST, KWOTAZAL, SKAD,
              KATEGA, KATEGB, KATEGC, MAGAZYN, LISTYWYSD, CKONTRAKTY,
              SPOSDOST, KOSZTDOSTPLAT, KOSZTDOSTROZ, TERMDOST, DULICA, DNRDOMU, DNRLOKALU,
              DMIASTO, DKODP, ODBIORCA, ODBIORCAID, WYSYLKA, SRVREQUEST)
       values( :faktura,:stan,:rejfak,:typfak, :data, :dokdata, :dataotrz, :KLIENT,:DOSTAWCA,:platnik,:SPOSZAP,:datazap,:termin,
              0,0,0,0,
--              :zalkwotanet, :zalkwotabru, :zalkwotanetzl, :zalkwotabruzl,
              :rabnag,0,/*NULL,*/'',NULL,0,
              :WALUTA, :WALUTOWE, :kurs, :krajwysylki,
              :SPRZEDAWCA, :sprzedawcazew, :operator, :KOSZTDOST,:sumzal, 2,
              :katega, :kategb, :kategc, :magazyn, :dokumsped, :CKONTRAKTY,
              :ssposdost,:skosztdostplat, 3, :stermdost, :sdulica, :sdnrdomu, :sdnrlokalu,
              :sdmiasto, :sdkodp, :sodbiorca, :sodbiorcaid, :wysylka, :srvrequest);
    INSERT INTO NAGFAKTERMPLAT (DOKTYP, DOKREF, TERMZAP, PROCENTPLAT, SPOSPLAT)
      select 'F', :faktura, TERMZAP, PROCENTPLAT, SPOSPLAT from NAGFAKTERMPLAT where DOKTYP='M' and DOKREF=:dokmag;

  end else begin
    /*sprawdzenie, czy klient lub dostawca sie zgadzają*/
    select DOSTAWCA, KLIENT, MAGAZYN, WALUTA, KURS, SLODEF, SLOPOZ from DOKUMNAG where REF=:DOKMAG
    into :DOSTAWCA, :klient, :magazyn, :waluta, :kurs, :slodefref, :slopozref;
    select ZAKUP, KLIENT, PLATNIK, DOSTAWCA, RABAT, WALUTOWA, WALUTA, KURS, SAD, DATASPRZ, ODDZIAL, SKAD from NAGFAK where REF=:faktura
    into :zakup, :isklient, :isplatnik, :isdostawca, :rabnag, :walutowe, :iswaluta, :iskurs, :sad, :datasprz, :oddzial, :skad;
    if (:skad<>2) then exception universal 'Można wskazać tylko fakturę wygenerowaną z dok. mag.'; --BS47151
    if(:DOKDATA < :DATASPRZ) then
      update nagfak set ckontrakty = :ckontrakty, datasprz = :dokdata where ref = :faktura;
    if(:fakturanaplatnika=1) then begin
      select count(*) from platnicy where klient=:klient and platnik=:platnik into :cnt;
      if(:cnt>0) then klient = :platnik;
      else if(:slodefref=1 and :slopozref=:platnik) then klient = :platnik;
    end else if(:zakup = 1 and :sad <> 1 and :dostawca <> :isdostawca) then begin
      if(exists(select ref from DOSTAWCYFAKT where DOSTAWCA = :dostawca and FAKTURANT = :isdostawca)) then
        dostawca = :isdostawca;
      else if(:slodefref=6 and :slopozref=:isdostawca) then dostawca = :isdostawca;
    end

    if(:zakup = 1 and :sad<>1 and ((:dostawca <> :isdostawca) or (:dostawca is null) or (:isdostawca is null))) then
      exception NAGFAK_DOKFORMMAG_DWRONG;
    else if(:zakup = 0 and :sad<>1 and ((:klient <> :isklient) or (:klient is null) or (:isklient is null))) then
      exception NAGFAK_DOKFROMMAG_KWRONG;
    else if(:zakup = 0 and :sad<>1 and ((:platnik <> :isplatnik) or (:platnik is null) or (:isplatnik is null))) then
      exception NAGFAK_DOKFROMMAG_KWRONG 'Niezgodność platników na wybranych dok. magazynowych.';
    else if(:zakup = 1 and :sad = 1 and :waluta <> :iswaluta) then
      exception NAGFAK_DOKFROMMAG_WALUTA; /*MS: usunieto kontrole kursu na sadzie*/
    if((:kosztdost > 0) or (:sumzal > 0)) then
      update NAGFAK set KOSZTDOST = KOSZTDOST + :kosztdost, KWOTAZAL = KWOTAZAL + :sumzal where ref=:faktura;
  end
  if(:rabnag is null) then rabnag = 0;
  select bn,sad from NAGFAK where nagfak.ref = :faktura into :bn, :sad;
  if(:klient > 0) then
      select grupykli.cennik from klienci join grupykli on(klienci.grupa = grupykli.ref) where KLIENCI.REF=:klient
        into :cennik;
  cenacen = null;
  walcen = null;
  select max(numer) from pozfak where dokument = :faktura into :poznumer;
  if (poznumer is null) then poznumer = 0;
  for select REF,KTM, WERSJA, WERSJAREF, OPIS, JEDNO,ILOSCO,ILOSC,CENA,PCENASR, CENACEN,walcen, RABAT, RABATTAB,
             OPK, CENAWAL, FOC, OBLICZCENZAK, OBLICZCENKOSZT, OBLICZCENFAB,
             PARAMN1, PARAMN2, PARAMN3, PARAMN4,
             PARAMN5, PARAMN6, PARAMN7, PARAMN8,
             PARAMN9, PARAMN10, PARAMN11, PARAMN12,
             PARAMS1, PARAMS2, PARAMS3, PARAMS4,
             PARAMS5, PARAMS6, PARAMS7, PARAMS8,
             PARAMS9, PARAMS10, PARAMS11, PARAMS12,
             PARAMD1, PARAMD2, PARAMD3, PARAMD4,
             KRAJPOCHODZENIA, FROMPOZZAM, wartsnetto, wartsbrutto, frompromoref, ilosczadopakiet, prec, gr_vat, alttopoz, fake, havefake
  from DOKUMPOZ where
    DOKUMENT =:dokmag and ILOSC > 0
  order by DOKUMPOZ.alttopoz nulls first ,DOKUMPOZ.NUMER
  into :dokpoz, :ktm, :wersja, :wersjaref, :opis, :jedno, :ilosco,:iloscm,:cenamag,:pcenamag,:cenacen, :walcen, :rabat,
       :rabattab, :opk, :cenawal, :foc, OBLCENZAK, OBLCENKOSZT, OBLCENFAB,
       :paramn1, :paramn2, :paramn3, :paramn4,
       :paramn5, :paramn6, :paramn7, :paramn8,
       :paramn9, :paramn10,  :paramn11, :paramn12,
       :params1, :params2, :params3, :params4,
       :params5, :params6, :params7, :params8,
       :params9, :params10,  :params11, :params12,
       :paramd1, :paramd2, :paramd3, :paramd4,
       :krajpochodzenia, :frompozzam, :wartnet, :wartbru, :frompromoref, :ilosczadopakiet, :prec, :gr_vat, :alttopoz, :fake, :havefake
  do begin
    /*obliczenie rabatu do pozycji*/
    ilosc = null;
    jedn = NULL;
    przeliczo = null;
    przeliczc = null;
    if (:sad = 1) then begin
      OBLCENZAK = 0;
      OBLCENKOSZT = 0;
      OBLCENFAB = 0;
    end
    if(:copycen = 0 ) then begin
      WALCEN = NULL;
      cenacen = null;
    end
    if((:jedno is null) or (:ilosco is null))then begin
      ilosco = :iloscm;
    end
    /*zmniejszenie ilosci o korekty*/
    if(:domagkor = 1) then begin
      iloscmagmin = 0;
      select sum(POZKOR.ILOSC)
        from DOKUMPOZ POZKOR
        left join DOKUMNAG on (POZKOR.DOKUMENT = DOKUMNAG.REF )
        where POZKOR.KORTOPOZ = :dokpoz and DOKUMNAG.FAKTURA is null and DOKUMNAG.AKCEPT > 0
      into :iloscmagmin;
      if(:iloscmagmin > 0) then begin
        iloscm = :iloscm - :iloscmagmin;
        if(:przeliczo is null and jedno is not null) then
          select PRZELICZ from TOWJEDN where ref = :jedno into :przeliczo;
        else
          przeliczo = 1;
        ilosco = :iloscm / :przeliczo;
      end
    end
    /*zmniejszenie ilosci o rozliczenia*/
    if(:domagopkroz = 1) then begin
      iloscmagmin = 0;
      select sum(ROZKOR.ILOSC)
      from DOKUMROZ ROZKOR
      join STANYOPK on (STANYOPK.ref = ROZKOR.opkrozid)
      join DOKUMPOZ on (DOKUMPOZ.REF = STANYOPK.POZDOKUM )
      join DOKUMNAG on (DOKUMNAG.REF = DOKUMPOZ.DOKUMENT and DOKUMNAG.AKCEPT > 0)
      where DOKUMPOZ.REF = :dokpoz
      into :iloscmagmin;
      if(:iloscmagmin > 0) then begin
        iloscm = :iloscm - :iloscmagmin;
        if(:przeliczo is null and jedno is not null) then
          select PRZELICZ from TOWJEDN where ref = :jedno into :przeliczo;
        else
          przeliczo = 1;
        ilosco = :iloscm / :przeliczo;
      end
    end
      /*przyjcie jednostki magazynowej - na razie*/
    select ref from TOWJEDN where KTM = :ktm and towjedn.glowna = 1 into :jednm;
    if(:copycen > 0 and ((:cenacen is not null and :opk > 0) or (:cenacen > 0)))then begin
      /*kopiowanie ceny sprzedazy z dokumentu magazynowego*/
      if(:bn = 'B' and :dokbn = 'N') then begin
        select coalesce(vat.stawka,0.0) from vat where vat.grupa = :gr_vat into :vat;
        cenacen = :cenacen * (100 + :vat)/100;
      end else if(:bn = 'N' and :dokbn = 'B')then begin
        select coalesce(vat.stawka,0.0) from vat where vat.grupa = :gr_vat into :vat;
        cenacen = :cenacen / ((100 + :vat)/100);
      end
      ilosc = :iloscm;
      jedn = :jednm;
    end else if(:klient > 0 and :wydania>0) then begin
      /*pobranie ceny sprzedazy z cennika dla danego klienta*/
      execute procedure get_config('UNITTOUNIT',-1) returning_values :UNITTOUNIT;
      if (:UNITTOUNIT = '1') then
      begin
        jedn = jedno;
        ilosc = ilosco;
      end else begin
        select ref, przelicz from TOWJEDN where KTM = :ktm and towjedn.c = 2 into :jedn, :przeliczc;
        if(:przeliczc is null) then przeliczc = 1;
        if(:jedn = :jednm) then
          ilosc = :iloscm;
        else if(:jedn = :jedno) then
          ilosc = :ilosco;
        else
          ilosc = :iloscm / :przeliczc;
      end
      execute procedure cennik_znajdz(null,:wersjaref,:jedn,:klient,:oddzial,:stan) returning_values :cennik;
      if(:bn = 'B') then
        select  cenabru, waluta from cennik where cennik = :cennik and ktm = :ktm and wersja = :wersja and jedn = :jedn into :cenacen, :walcen;
      else
        select  cenanet, waluta from cennik where cennik = :cennik and ktm = :ktm and wersja = :wersja and jedn = :jedn into :cenacen, :walcen;
      if(exists(select ref from dokumpoz where refdopakiet=:dokpoz)) then
        zpakietem = 1;
      else
        zpakietem = 0;
      execute procedure OBLICZ_RABAT(:ktm, :wersja, :klient,:iloscm,:cenacen,:bn, :magazyn, :cennik, 0, :zpakietem) returning_values :rabat,:iscena, :walrabcen, :promocja;
      if(:iscena = 1) then begin
        cenacen = :rabat;
        walcen = :walrabcen;
        rabat = 0;
      end
      rabattab = :rabat;
    end else if(:dostawca > 0 and :walutowe<>1) then begin
      select jedn, rabat, waluta, cenanet
        from GET_DOSTCEN(:wersjaref, :dostawca, :walcen,  :oddzial, 0)
      into :jedn,:rabat,:walcen, :cenacen;
      if(:walcen is null or (:walcen = '')) then walcen = :walpln;
      if(:jedn is null or (:jedn = 0))then begin
        jedn = :jednm;
        ilosc = :iloscm;
      end

      cenanotzakupu = 0;
      select min(ref),max(ref) from dokumroz where pozycja=:dokpoz into :mindokumroz,:maxdokumroz;
      if(:mindokumroz=:maxdokumroz and :mindokumroz is not null) then begin
        select sum(p.cenanew - p.cenaold)
        from dokumnotp p
        left join dokumnot d on (d.ref=p.dokument)
        where p.dokumrozkor=:mindokumroz
        and d.rodzaj=0
        and d.ref=d.grupa
        into :cenanotzakupu;
        if(:cenanotzakupu is null) then cenanotzakupu = 0;
      end

      if(:walcen = :walpln) then begin
        if(:jedn = :jednm) then begin
          cenacen = :pcenamag + :cenanotzakupu;
          ilosc = :iloscm;
        end else begin
          select PRZELICZ from TOWJEDN where REF=:jedn into :przeliczc;
          if(:przeliczc is null) then przeliczc = 1;
          ilosc = :iloscm /:przeliczc;
          cenacen = :cenacen * :przeliczc;
        end
      end else begin
        /*cena zakupu walutowa - niech to uzytkownik wpisze recznie*/
        cenacen = 0;
      end
      /*dostcen sa w cenach netto zawsze */
      if(:bn = 'B') then begin
        select coalesce(vat.stawka,0.0) from vat where vat.grupa = :gr_vat into :vat;
        cenacen = :cenacen * (100 + :vat)/100;
      end
    end else begin
      ilosc = :iloscm;
      jedn = :jednm;
      cenanotzakupu = 0;
      select min(ref),max(ref) from dokumroz where pozycja=:dokpoz into :mindokumroz,:maxdokumroz;
      if(:mindokumroz=:maxdokumroz and :mindokumroz is not null) then begin
        select sum(p.cenanew - p.cenaold)
        from dokumnotp p
        left join dokumnot d on (d.ref=p.dokument)
        where p.dokumrozkor=:mindokumroz
        and d.rodzaj=0
        and d.ref=d.grupa
        into :cenanotzakupu;
        if(:cenanotzakupu is null) then cenanotzakupu = 0;
      end

      if(:copycen=-1) then
        cenacen = :pcenamag + :cenanotzakupu;
      else if(:walutowe=1) then
        cenacen = :cenawal;
      else
        cenacen = null;
    end
    if(:cenacen is null) then cenacen = 0;
    if(:opk > 0) then opk = 1;
    -- przeliczenie do jednostki rozliczeniowej z pozycji zamówienia
    if(:frompozzam is not null and :frompozzam <> 0 and jedn = jednm) then begin
      select ilosc, iloscm, jedn from pozzam where ref = :frompozzam into :pozzamilosc, :pozzamiloscm, :pozzamjedn;
      if(pozzamjedn is not null and pozzamjedn <> jednm and pozzamiloscm > 0 and pozzamilosc > 0) then begin
        ilosc = ilosc * pozzamilosc/pozzamiloscm;
        jedn = pozzamjedn;
        cenacen = cenacen * pozzamiloscm/pozzamilosc;
      end
    end
    if(:prec<>4) then cenacen = cast(:cenacen as numeric(14,2));
    if(:ilosc > 0) then begin
       pozfakref = null;
       if(:kumulacja=1 and coalesce(:fake,0) = 0 and coalesce(:havefake,0) = 0) then begin
         if(:rabat is null) then rabat = 0;
         if(:rabattab is null) then rabattab = 0;
         if(:zakup = 1) then begin
           select first 1 ref from pozfak
             where dokument=:faktura and ktm=:ktm and wersja=:wersja and cenacen=:cenacen
             and rabat=:rabat and rabattab=:rabattab and walcen=:walcen and jedn=:jedn and jedno=:jedno
             and magazyn=:magazyn and cenamag=:cenamag
             and opk=:opk and foc=:foc
--           and paramn1=:paramn1 and paramn2=:paramn2 and paramn3=:paramn3 and paramn4=:paramn4
--           and paramn5=:paramn5 and paramn6=:paramn6 and paramn7=:paramn7 and paramn8=:paramn8
--           and paramn9=:paramn9 and paramn10=:paramn10 and paramn11=:paramn11 and paramn12=:paramn12
--           and params1=:params1 and params2=:params2 and params3=:params3 and params4=:params4
--           and params5=:params5 and params6=:params6 and params7=:params7 and params8=:params8
--           and params9=:params9 and params10=:params10 and params11=:params11 and params12=:params12
--           and paramd1=:paramd1 and paramd2=:paramd2 and paramd3=:paramd3 and paramd4=:paramd4
           order by ref
           into :pozfakref;
         end else begin
           select first 1 ref from pozfak
             where dokument=:faktura and ktm=:ktm and wersja=:wersja and cenacen=:cenacen
             and rabat=:rabat and rabattab=:rabattab and walcen=:walcen and jedn=:jedn and jedno=:jedno
             and magazyn=:magazyn
             and opk=:opk and foc=:foc
           order by ref
           into :pozfakref;
         end
         if(:pozfakref is not null) then begin
           if(:zakup=0) then begin
             update pozfak set ilosc=ilosc+:ilosc, ilosco=ilosco+:ilosco, iloscm=iloscm+:iloscm
             where ref=:pozfakref;
           end else begin
             update pozfak set ilosc=ilosc+:ilosc, ilosco=ilosco+:ilosco, iloscm=iloscm+:iloscm,
               rabat = -101 -- to jest po to aby przeliczyly sie wartosci
             where ref=:pozfakref;
           end
           update dokumpoz set frompozfak=:pozfakref where ref=:dokpoz;
         end
       end
       if(:pozfakref is null) then begin
         poznumer = poznumer + 1;
         execute procedure GEN_REF('POZFAK') returning_values :pozfakref;
         AltToPozN = null;
         if (AltToPoz is not null) then
         begin
           select ref
             from pozfak
             where fromdokumpoz = :ALtToPoz
             into :AltToPozN;
         end
         insert into pozfak(REF, DOKUMENT,KTM,WERSJA, OPIS, ILOSC,CENACEN,RABAT,RABATTAB,WALCEN,
                       ILOSCO,ILOSCM,JEDN,JEDNO,MAGAZYN, KRAJPOCHODZENIA,
                       PILOSC,PCENACEN,PRABAT,PCENANET,PCENABRU,CENAMAG,PCENAMAG,
                       PWARTNET,PWARTBRU,OPK, FROMDOKUMPOZ, FOC, obliczcenzak, obliczcenkoszt, obliczcenfab,
                       paramn1, paramn2, paramn3, paramn4,
                       paramn5, paramn6, paramn7, paramn8,
                       paramn9, paramn10, paramn11, paramn12,
                       params1, params2, params3, params4,
                       params5, params6, params7, params8,
                       params9, params10, params11, params12,
                       paramd1, paramd2, paramd3, paramd4,
                       numer, frompromoref, ilosczadopakiet, ord, prec, gr_vat, fake, alttopoz)
         values(:pozfakref, :faktura,:ktm,:wersja, :opis, :ilosc,:cenacen,:rabat,:rabattab,:walcen,
              :ILOSCO, :ILOSCM,:jedn, :jedno,:magazyn, :krajpochodzenia,
              0,0,0,0,0,:cenamag,:pcenamag,
              0,0,:opk,case when :dokwolny = 0 then :dokpoz else null end, :foc, :oblcenzak, :oblcenkoszt, :oblcenfab,
              :paramn1, :paramn2, :paramn3, :paramn4,
              :paramn5, :paramn6, :paramn7, :paramn8,
              :paramn9, :paramn10, :paramn11, :paramn12,
              :params1, :params2, :params3, :params4,
              :params5, :params6, :params7, :params8,
              :params9, :params10, :params11, :params12,
              :paramd1, :paramd2, :paramd3, :paramd4,
              :poznumer, :frompromoref, :ilosczadopakiet, 1, :prec, :gr_vat,:fake, :AltToPozN);
      end
      if(:domagkor = 1) then begin
        for
          select POZKOR.REF
            from DOKUMPOZ POZKOR
              left join DOKUMNAG on (POZKOR.DOKUMENT = DOKUMNAG.REF)
            where POZKOR.KORTOPOZ = :dokpoz and DOKUMNAG.FAKTURA is null and DOKUMNAG.AKCEPT > 0
        into :dokpozkorref
        do begin
          if (dokwolny = 0) then
            update DOKUMPOZ set FROMPOZFAK = :pozfakref where ref=:dokpozkorref;
        end
      end
    end
    cenacen = null;
    walcen = null;
  end
  /* przenoszenie refdopakiet z dokumentow magazynowych na faktury sprzedazy */
  for select p.ref, p.fromdokumpoz from pozfak p where p.opk = 1 and p.dokument = :faktura into :pozfref, :dokpozref
  do begin
    refdopakiet = null;
    select dp.refdopakiet from dokumpoz dp where dp.ref = :dokpozref and opk = 1 into :refdopakiet;
    if(:refdopakiet is not null) then begin
      update pozfak pf1 set pf1.refdopakiet = (select ref from pozfak pf2 where fromdokumpoz = :refdopakiet)
        where pf1.ref = :pozfref;
    end
  end
  if (dokwolny = 0) then update DOKUMNAG set FAKTURA = :faktura where REF=:dokmag;
  else if (dokwolny =1) then update DOKUMNAG set FAKTURAWOLNA = :faktura where REF=:dokmag;
  /* dodanie do faktury rozliczen deklarowanych */
  if(:sumzal>0) then begin
    if(:klient > 0) then begin
      select min(slodef.ref) from slodef where slodef.typ = 'KLIENCI' into :slodefref;
      slopozref = :klient;
    end else begin
      select min(slodef.ref) from slodef where slodef.typ = 'DOSTAWCY' into :slodefref;
      slopozref = :dostawca;
    end
    execute procedure SLO_DANE(:slodefref,:slopozref) returning_values :kod, :nazwa, :kontofk;
    insert into ROZRACHROZ(SLODEF,SLOPOZ,KONTOFK,SYMBFAK,DOKTYP,DOKREF,WALUTA,WINIEN,MA)
    values(:slodefref,:slopozref,:kontofk,:symbol,'F',:faktura,:waluta,:sumzal,0);
  end

  if (:dokmag is not null) then
  begin
    for select slodef, slopoz, kontofk, symbfak, waluta, winien, ma , payday
      from rozrachroz r
      where r.dokref = :dokmag
      into :slodefr, :slopozr, :kontofkr, :symbfakr, :walutar, :winienr, :mar, :paydayr
    do begin
      insert into rozrachroz(slodef, slopoz, kontofk, symbfak, waluta, winien, ma , payday, doktyp, dokref, zrodlo)
        values(:slodefr, :slopozr,  :kontofkr, :symbfakr, :walutar, :winienr, :mar, :paydayr, 'F', :faktura, 'M');
    end
  end

  if(:domagkor = 1)then begin
    /*zaznaczanie wszystkich dokumentów mag., które sa powiązane z pozycjami danego dokumentu*/
    for
      select DOKUMNAG.REF
        from DOKUMNAG
        where DOKUMNAG.REFK = :dokmag and DOKUMNAG.FAKTURA is null and DOKUMNAG.AKCEPT > 0
    into :DOKMAGKOR
    do begin
        if (dokwolny = 0) then update DOKUMNAG set FAKTURA = :faktura where ref=:dokmagkor;
        else if (dokwolny = 1) then update DOKUMNAG set FAKTURAWOLNA = :faktura where ref=:dokmagkor;
    end
  end
  newfak = :faktura;
  --weryfikacja okresów dokumentów magazynowych
  select coalesce(t.dokmagwokresie,0)
    from typfak t
    where t.symbol = :typfak
  into :dokmagwokresiekontrola;
  if (:dokmagwokresiekontrola = 1) then begin
    select n.ref
      from nagfak n
      where n.ref = :faktura
    into :dokmagwokresiefaktura;
    if (:dokmagwokresiefaktura is not null) then begin
      select count(distinct d.okres)
        from dokumnag d
        where d.faktura = :dokmagwokresiefaktura
      into :dokmagwokresieokrescnt;
      if (:dokmagwokresieokrescnt > 1) then
        exception NAGFAK_DOKMAGWOKRESIE;
    end
  end
  --koniec weryfikacji okresów dokumentów magazynowych
end^
SET TERM ; ^
