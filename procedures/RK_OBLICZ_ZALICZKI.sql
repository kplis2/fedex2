--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RK_OBLICZ_ZALICZKI(
      ZAMOWIENIE integer)
   as
declare variable PKWOTA numeric(14,2);
declare variable PKWOTAZL numeric(14,2);
declare variable MKWOTA numeric(14,2);
declare variable MKWOTAZL numeric(14,2);
declare variable WINIEN numeric(14,2);
declare variable MA numeric(14,2);
declare variable RKDOKPOZREF integer;
declare variable ROZRACHROZREF integer;
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable SYMBFAK varchar(30);
declare variable KONTOFK ACCOUNT_ID;
declare variable KOD varchar(100);
declare variable NAZWA varchar(255);
declare variable WALUTA varchar(3);
begin
  for select rkdokpoz.ref, case when ((RKDOKNAG.STATUS>0 or RKDOKNAG.NUMER>0)  and RKDOKNAG.ANULOWANY=0) then RKDOKPOZ.KWOTA else 0 end,
    RKDOKNAG.SLODEF, RKDOKNAG.SLOPOZ, RKDOKPOZ.rozrachunek, RKDOKPOZ.konto, RKDOKNAG.WALUTA,
    rozrachroz.ref, rozrachroz.winien
  from RKDOKPOZ
  left join RKDOKNAG on (RKDOKNAG.REF=RKDOKPOZ.DOKUMENT)
  left join ROZRACHROZ on (ROZRACHROZ.STABLE = 'RKDOKPOZ' and ROZRACHROZ.sref = rkdokpoz.ref and rozrachroz.doktyp='Z' and rozrachroz.dokref=:zamowienie)
  where RKDOKPOZ.PM='+' and RKDOKPOZ.ZAMOWIENIE=:zamowienie
  into :rkdokpozref, :pkwota,
    :slodef, :slopoz, :symbfak, :kontofk, :waluta,
    :rozrachrozref, :winien
  do begin
    if (kontofk is null or kontofk='' or kontofk = '???' or coalesce(char_length(kontofk),0)<=4 ) then -- [DG] XXX ZG119346
       execute procedure SLO_DANE(:slodef, :slopoz)
            returning_values :kod, :nazwa, :kontofk;
    if(:rozrachrozref is null) then
        insert into ROZRACHROZ(DOKTYP,DOKREF, SLODEF, SLOPOZ, SYMBFAK, KONTOFK, WALUTA, WINIEN, MA, STABLE, SREF)
        values('Z',:zamowienie, :slodef, :slopoz, :symbfak, :kontofk, :waluta, :pkwota,  0, 'RKDOKPOZ',:rkdokpozref);
    else if(:winien <> :pkwota) then
      update rozrachroz set WINIEN = :pkwota where REF=:rozrachrozref;
  end
  for select rkdokpoz.ref, RKDOKPOZ.KWOTA,
      RKDOKNAG.SLODEF, RKDOKNAG.SLOPOZ, RKDOKPOZ.rozrachunek, RKDOKPOZ.konto, RKDOKNAG.WALUTA,
      rozrachroz.ref, rozrachroz.winien
    from RKDOKPOZ
    left join RKDOKNAG on (RKDOKNAG.REF=RKDOKPOZ.DOKUMENT)
    left join ROZRACHROZ on (ROZRACHROZ.STABLE = 'RKDOKPOZ' and ROZRACHROZ.sref = rkdokpoz.ref and rozrachroz.doktyp='Z' and rozrachroz.dokref=:zamowienie)
    where RKDOKPOZ.PM='-' and RKDOKPOZ.ZAMOWIENIE=:zamowienie
    and RKDOKNAG.NUMER>0 and RKDOKNAG.ANULOWANY=0
    into :rkdokpozref, :mkwota,
      :slodef, :slopoz, :symbfak, :kontofk, :waluta,
      :rozrachrozref, :ma
  do begin
    if (kontofk is null or kontofk='' or kontofk = '???' or coalesce(char_length(kontofk),0)<=4 ) then -- [DG] XXX ZG119346
       execute procedure SLO_DANE(:slodef, :slopoz)
            returning_values :kod, :nazwa, :kontofk;
    if(:rozrachrozref is null) then
        insert into ROZRACHROZ(DOKTYP,DOKREF, SLODEF, SLOPOZ, SYMBFAK, KONTOFK, WALUTA, WINIEN, MA, STABLE, SREF)
        values('Z',:zamowienie, :slodef, :slopoz, :symbfak, :kontofk, :waluta, 0,  :mkwota, 'RKDOKPOZ', :rkdokpozref);
    else if(:ma <> :mkwota) then
      update rozrachroz set MA = :mkwota where REF=:rozrachrozref;
  end
  for select ROZRACHROZ.REF
    from ROZRACHROZ left join rkdokpoz on (RKDOKPOZ.ref = ROZRACHROZ.SREF)
  where ROZRACHROZ.DOKTYP='Z' and ROZRACHROZ.DOKREF=:zamowienie and ROZRACHROZ.STABLE='RKDOKPOZ'  and RKDOKPOZ.ref is null
  into :rozrachrozref
  do begin
    delete from ROZRACHROZ where ref=:rozrachrozref;
  end
end^
SET TERM ; ^
