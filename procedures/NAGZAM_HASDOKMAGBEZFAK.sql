--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGZAM_HASDOKMAGBEZFAK(
      ZAM integer)
   as
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE ORGZAM INTEGER;
begin
  select first 1 1
    from NAGZAM
      left join DOKUMNAG on (DOKUMNAG.ZAMOWIENIE = NAGZAM.REF)
      left join DEFDOKUM on (DEFDOKUM.symbol = DOKUMNAG.TYP)
    where (NAGZAM.REF = :zam or (NAGZAM.org_ref = :zam))
      and DOKUMNAG.FAKTURA is NULL
      and dokumnag.blokadafak = 0
      and DEFDOKUM.zewn = 1 and DEFDOKUM.koryg = 0
  into :cnt;
  if(:cnt > 0) then cnt = 1;
  else cnt = 0;
  for select distinct ORG_REF from NAGZAM where REF=:zam
  into :orgzam
  do begin
   if(:orgzam is null) then
     orgzam = :zam;
    update NAGZAM set HASDOKMAGBEZFAK = :cnt where (REF=:orgzam) and (REF=ORG_REF or ORG_REF is null)
      and (HASDOKMAGBEZFAK is null or (HASDOKMAGBEZFAK <> :cnt));
  end
end^
SET TERM ; ^
