--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_DISABLE_PERIODS(
      EMPLOYEE integer,
      PERIODTYPE smallint)
  returns (
      FROMDATE timestamp,
      TODATE timestamp)
   as
declare variable EFROMDATE timestamp;
declare variable ETODATE timestamp;
declare variable IFROMDATE timestamp;
declare variable ITODATE timestamp;
declare variable TTODATE timestamp;
declare variable TFROMDATE timestamp;
declare variable CTODATE timestamp;
declare variable CFROMDATE timestamp;
declare variable NULLDATE timestamp;
declare variable Y smallint;
declare variable M smallint;
declare variable D smallint;
declare variable SUMY smallint;
declare variable SUMM smallint;
declare variable SUMD smallint;
declare variable ZUSFROMDATE timestamp;
declare variable DISABLED smallint;
declare variable LASTDISABLED smallint;
declare variable NEEDSUSPEND smallint;
declare variable CTXTODATESTR varchar(20);
declare variable CTXTODATE timestamp;
begin
/*
  DS/MWr Personel: Procedura zwraca okresy, w ktorych zadanemu pracownikowi EMPLOYEE,
  w zleznosci od parametru PERIODTYPE:
    = -1 : przypisana jest niepelnosprawnosc srednia lub znaczna
    =  0 : przysluguje prawo do 10 dni urlopu rehabilitacyjnego
    =  1 : pracownik ma dzienny wymiar urlopu obnizany do max 7h
*/

  if (periodtype not in (-1,0,1)) then
    exception universal 'Nieznana wartość parametru!';

  if (periodtype = 0) then
  begin
  -- wyliczenie okresow, w ktorych pracownikowi nalezy sie 10 dni urlopu rehabilitacyjnego

    ctxtodatestr = rdb$get_context('USER_TRANSACTION', 'RPT_WORKREFERENCE_TODATE');  --PR54992
    if (ctxtodatestr <> '') then ctxtodate = cast(ctxtodatestr as timestamp);

    nulldate = '3000-01-01';
    for
      select g.fromdate, g.todate, e.fromdate, coalesce(:ctxtodate,e.todate)
        from employees e cross join get_disable_periods(:employee,-1) g
      where e.ref = :employee
      order by g.fromdate
      into :ifromdate, :itodate, :efromdate, :etodate
    do begin
      d = 0;
      sumy = 0;
      summ = 0;
      sumd = 0;
      fromdate = null;
  
    --Analiza wczesniejszych swiadectw pracy
      for
        select s.fromdate, s.todate from eworkcertifs s
          where s.employee = :employee
            and s.todate >= :ifromdate
            and (s.fromdate <= :itodate or :itodate is null)
            and s.fromdate < :efromdate
          into :cfromdate, :ctodate
      do begin
        if (fromdate is null) then
        begin
          if (ctodate >= efromdate) then ctodate = efromdate - 1; --nie wchodzimy na okres zatrudnienia
    
          if (cfromdate >= ifromdate) then tfromdate = cfromdate;
          else tfromdate = ifromdate;
    
          if (ctodate >= itodate) then ttodate = itodate;
          else ttodate = ctodate;
    
          select y, m, d from get_ymd(:tfromdate, coalesce(:ttodate,:nulldate), 1, :d)
            into :y ,:m, :d;
    
          sumy = sumy + y;
          summ = summ + m;
          sumd = sumd + d;
  
          select new_syears, new_smonths, new_sdays from normalize_ymd(:sumy, :summ, :sumd)
            into :sumy,  :summ , :sumd;
  
          if (sumy >= 1) then
          begin
            sumy = sumy - 1;
    
            select fdate from date_add(coalesce(:ttodate,:nulldate) + 1, -:sumy, -:summ, -:sumd )
              into :fromdate;
    
            todate = itodate;
          end
        end
      end
  
      if (fromdate is not null) then
      begin
        if (etodate <= todate or todate is null) then todate = etodate;
      end else
      begin
      --Analiza zatrudnienia
        if (efromdate >= ifromdate or ifromdate is null) then tfromdate = efromdate;
        else tfromdate = ifromdate;
    
        if (etodate >= itodate or etodate is null) then ttodate = itodate;
        else ttodate = etodate;
  
        select y, m, d from get_ymd(:tfromdate, coalesce(:ttodate,:nulldate), 1, :d)
          into :y ,:m, :d;
    
        sumy = sumy + y;
        summ = summ + m;
        sumd = sumd + d;
  
        select new_syears, new_smonths, new_sdays from normalize_ymd(:sumy, :summ, :sumd)
          into :sumy,  :summ , :sumd;
  
        if (sumy >= 1) then
        begin
          sumy = sumy - 1;
    
          select fdate from date_add(coalesce(:ttodate,:nulldate) + 1, -:sumy, -:summ, -:sumd)
            into :fromdate;
    
          todate = ttodate;
        end
      end
  
      if (fromdate is not null and (todate >= fromdate or todate is null)) then
        suspend;
    end
  end else
  ------------------------------------------------------------------------------
  if (periodtype in (-1,1)) then
  begin
    lastdisabled = 0;
    needsuspend = 0;
    for
      select z.invalidityfrom, z.invalidityto, z.fromdate
           , case when (c.code > '1' and (z.fulldim = 0 or :periodtype < 0)) then 1 else 0 end
        from employees e
          join ezusdata z on (z.person = e.person)
          join edictzuscodes c on (c.ref = z.invalidity)
        where e.ref = :employee
        order by z.fromdate
        into :ifromdate, :itodate, :zusfromdate, :disabled
    do begin
      if (disabled = 1 and lastdisabled = 0) then
      begin
        fromdate = ifromdate;
        todate = itodate;
        needsuspend = 1;
      end else
      if (disabled = 1 and lastdisabled = 1) then
      begin
        if (todate is null or ifromdate <= todate + 1) then   --BS81354
        begin
          todate = itodate;
          needsuspend = 1;
        end else begin
          suspend;
          needsuspend = 0;
          disabled = 0;
        end
      end else
      if (lastdisabled = 1 and disabled = 0 and (todate is null or todate >= zusfromdate)) then
      begin
        todate = zusfromdate - 1;
        if (fromdate <= todate) then
          suspend;
        needsuspend = 0;
      end
      lastdisabled = disabled;
    end
  
    if (needsuspend = 1) then
      suspend;
  end
end^
SET TERM ; ^
