--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_BROWSE_SUMDISTPOS(
      COMPANY integer,
      FROMPERIOD varchar(6) CHARACTER SET UTF8                           ,
      TOPERIOD varchar(6) CHARACTER SET UTF8                           ,
      DICTDEF integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      ACCPREFIX varchar(20) CHARACTER SET UTF8                           )
  returns (
      DISTSYMBOL varchar(20) CHARACTER SET UTF8                           ,
      DISTNAME varchar(80) CHARACTER SET UTF8                           ,
      ACCOUNT varchar(20) CHARACTER SET UTF8                           ,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      DEBIT numeric(14,2),
      CREDIT numeric(14,2),
      ACCOUNTDESCRIPT varchar(255) CHARACTER SET UTF8                           )
   as
declare variable DICTPOS integer;
declare variable NUMBER smallint;
declare variable POLE varchar(55);
declare variable PROCED varchar(1024);
begin
  if (symbol <> '') then
  begin
    execute procedure name_from_bksymbol(company, dictdef, symbol)
      returning_values distname, dictpos;
    distsymbol = symbol;
    select isdist from slodef where ref=:dictdef into :number;
    if(number = 1) then pole = ' and DP.symbol = '''||:symbol||'''';
    else pole = ' and DP.symbol'||:number||' = '''||:symbol||'''';
    proced = 'select DP.account, DP.period, sum(DP.debit), sum(DP.credit)
        from distpos DP
        where (DP.status = 2 or DP.status = 3) and DP.company = ' || :company || :pole ;
    if(coalesce(:fromperiod,'') <> '') then proced = proced || ' and DP.period >='''|| :fromperiod||'''';
    if(coalesce(:toperiod,'') <> '') then proced = proced || ' and DP.period <= '''|| :toperiod || '''';
    if (accprefix <> '') then
      proced = proced || 'and ('''||:accprefix||''' = '''' or DP.account like'''|| '%' ||:accprefix || '%' ||''')';
    proced = proced || ' group by DP.account, DP.period';
    proced = proced || ' order by DP.account, DP.period';


    for execute statement :proced into :account, :period, :debit, :credit
    do begin
      execute procedure fk_get_account_descript(company, period, account)
        returning_values accountdescript;
      suspend;
    end
  end
  else
  begin
    select isdist from slodef where ref=:dictdef into :number;
    if(number = 1) then pole = 'DP.symbol';
    else pole = 'DP.symbol'||:number;
    proced = 'select ' || :pole ||', DP.account, DP.period, sum(DP.debit), sum(DP.credit)';
    proced = proced || ' from distpos DP where DP.company = '||:company;
    if(coalesce(:fromperiod,'') <> '') then proced = proced || ' and DP.period >='''|| :fromperiod||'''';
    if(coalesce(:toperiod,'') <> '') then proced = proced || ' and DP.period <= '''|| :toperiod || '''';
    proced = proced || ' and (''' || :accprefix || ''' = '''' or DP.account like''' || '%' ||:accprefix || '%' || ''')';
    proced = proced || ' and '|| :pole ||'<>''''';
    proced = proced || ' group by ' || :pole || ', DP.account, DP.period';
    proced = proced || ' order by ' || :pole || ', DP.account, DP.period';
    for execute statement :proced
    into :distsymbol, :account, :period, :debit, :credit
    do begin
      execute procedure fk_get_account_descript(company, period, account)
        returning_values accountdescript;
      execute procedure name_from_bksymbol(company, dictdef, distsymbol)
        returning_values distname, dictpos;
      suspend;
    end
  end
end^
SET TERM ; ^
