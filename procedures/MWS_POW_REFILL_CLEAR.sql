--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_POW_REFILL_CLEAR(
      MWSORDTYPE integer,
      WHIN varchar(3) CHARACTER SET UTF8                           ,
      MAXCNT integer)
  returns (
      STATUS smallint)
   as
declare variable MWSCONSTLOC integer;
declare variable DEEP smallint;
declare variable MAGBREAK smallint;
declare variable MAXLEVEL integer;
declare variable DOCID integer;
declare variable MWSDOC integer;
declare variable DOCGROUP integer;
declare variable MWSORD integer;
declare variable DOCPOSID integer;
declare variable BLOCKGOOD varchar(40);
declare variable VERS integer;
declare variable QUANTITYWAITING numeric(14,4);
declare variable MWSGOODREFILL integer;
declare variable QUANTITYREFILL numeric(14,4);
declare variable QUANTITYREFILLED numeric(14,4);
declare variable QUANTITYSTOCK numeric(14,4);
declare variable OREF integer;
declare variable LOT integer;
declare variable ALOT integer;
declare variable AVERS integer;
begin
  status = 1;
  select magbreak from defmagaz where symbol = :whin
    into magbreak;
  if (magbreak is null) then magbreak = 0;
  -- sprawdzamy czy dla towaró nie ma zlecen z dolu na gore
  -- jak sa to je anulujemy i nie generujemy nowych w tym przebiegu
  for
    select vers, case when :magbreak = 2 then coalesce(lot,0) else 0 end
      from mwsgoodsrefill
      where wh = :whin and preparegoods = 1 and quantity - ordered > 0
      group by vers, case when :magbreak = 2 then coalesce(lot,0) else 0 end
      into vers, lot
  do begin
    -- Lecimy po zleceniach MG i MWG z lokacji poborowej (cp.goodsav <> 0) na lokacje buforowa  (cl.goodsav = 0)
    for
      select o.ref
        from mwsacts a
          join mwsords o on (o.ref = a.mwsord)
          left join mwsconstlocs cp on (cp.ref = a.mwsconstlocp)
          left join mwsconstlocs cl on (cl.ref = a.mwsconstlocl)
          left join whsecs w on (w.ref = cp.whsec)
        where a.status < 5 and a.status <> 2 and cp.goodsav <> 0 and cl.goodsav = 0
          and a.vers = :vers and cp.act > 0 and w.deliveryarea = 2 and o.status < 5
          and o.status <> 2 and o.mwsordtypedest = 11 and o.wh = :whin
          and case when :magbreak = 2 then coalesce(a.lot,0) else 0 end = :lot
        group by o.ref
        order by o.ref
        into oref
    do begin
      -- Jesli zlecenie przesunicia na lokacje buforowe nie jaet mixem to lecimy dalej
      if (not exists (select a.ref from mwsacts a left join towary t on (t.ktm = a.good)
          where a.mwsord = :oref and a.vers <> :vers and coalesce(t.paleta,0) = 0
            and case when :magbreak = 2 then coalesce(a.lot,0) else 0 end = :lot)
      ) then
      begin
        -- lecimy po pozycjach zlecenia przeuniecia z lokacji poborowych na buforowe
        for
          select a.vers, case when :magbreak = 2 then coalesce(a.lot,0) else 0 end, sum(a.quantity)
            from mwsacts a
            where a.mwsord = :oref
            group by a.vers, case when :magbreak = 2 then coalesce(a.lot,0) else 0 end
            into avers, alot, quantitystock
        do begin
          if (quantitystock is null) then quantitystock = 0;
          -- zaktualizowanie stanu tabeli niemixa
          for
            -- sprawdzamy czy dla danego towaru jest nalicozna tabela oczekujacych, jesli tak to ja modyfikujemy o ilosc ktra mieliscy przesunac na gore
            select mg.ref, mg.quantity - mg.ordered
              from mwsgoodsrefill mg
                left join dokumnag d on (d.ref = mg.docid)
                left join sposdost s on (s.ref = d.sposdost)
              where mg.preparegoods = 1 and mg.wh = :whin
                and mg.vers = :avers and mg.quantity - mg.ordered > 0
                and case when :magbreak = 2 then coalesce(mg.lot,0) else 0 end = :alot
              into mwsgoodrefill, quantityrefill
          do begin
            if (quantityrefill >= quantitystock) then
              quantityrefilled = quantitystock;
            else
              quantityrefilled = quantityrefill;
            quantitystock = quantitystock - quantityrefilled;
            update mwsgoodsrefill set ordered = ordered + :quantityrefilled
              where ref = :mwsgoodrefill;
            if (quantitystock <= 0) then
              break;
          end
        end
        -- kasujemy zlecenie przesuniecia z lokacji poborowych na buforow
        update mwsords set status = 0 where ref = :oref;
        update mwsacts set status = 0 where mwsord = :oref;
        delete from mwsords where ref = :oref;
      end
      if (not exists (select ref from mwsgoodsrefill where preparegoods = 1
          and wh = :whin and vers = :vers and quantity - ordered > 0
          and case when :magbreak = 2 then coalesce(lot,0) else 0 end = :lot)
      ) then
        break;
    end
  end
  -- w pierwszej kolejnosci obslugujemy biezace potrzeby magazynu
  select max(mwsstandlevelnumber) from mwsconstlocs where wh = :whin
    into :maxlevel;
  execute procedure MWS_GOODSREFILL_CLEAR(:whin,:mwsordtype,null,:maxlevel,:deep,:maxcnt)
    returning_values status;
  status = 1;
  suspend;
end^
SET TERM ; ^
