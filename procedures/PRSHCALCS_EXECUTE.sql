--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHCALCS_EXECUTE(
      PRSHCALC integer)
  returns (
      STATUS integer)
   as
declare variable prcalccol integer;
declare variable ret numeric(16,6);
begin
  for select ref from prcalccols where calc=:prshcalc and calctype=0
  into :prcalccol
  do begin
    if(not exists(select first 1 1 from prcalccols where parent=:prcalccol and calctype=0)) then begin
      execute procedure prshcalcs_subexecute(:prcalccol) returning_values :ret;
      update prcalccols set cvalue=:ret where ref=:prcalccol;
    end
  end
  status = 1;
end^
SET TERM ; ^
