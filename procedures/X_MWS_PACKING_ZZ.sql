--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_PACKING_ZZ(
      SPED_DOC INTEGER_ID,
      ACTUOPERATOR INTEGER_ID)
  returns (
      DOCID INTEGER_ID,
      MSG STRING255)
   as
declare variable docpoz integer_id;
declare variable wersjaref integer_id;
declare variable iloscdospk numeric_14_4;
declare variable ktm ktm_id;
declare variable nrwersji smallint_id;
--declare variable x_mws_serie string20;
--declare variable x_mws_partie string20;
--declare variable x_mws_slownik_ehrle string20;
declare variable serialno string20;
declare variable sloref integer_id;
declare variable jedno integer_id;
declare variable doknagk integer_id;
declare variable dokpozk integer_id;
declare variable grupasped integer_id;
declare variable msg1 string255;
declare variable topack1 smallint_id;
declare variable status1 smallint_id;
declare variable altposmode smallint_id;


begin
    execute procedure set_global_param('AKTUOPERATOR', cast (:actuoperator as string255), null );

    --execute procedure setconfig( 'OPERATOR',:actuoperator,null);
--XXX LDz MKD wystawianie ZZ na nie zapakowane towary
    msg = '';
    if (sped_doc is not null and sped_doc <> 0) then begin
        --szukam grupy spedycyjnej listywysd i wystawiam zz na grupe spedycyjna
        select refdok from listywysd where ref = :sped_doc into :doknagk;
        select n.grupasped from dokumnag n where n.ref = :doknagk into :grupasped;
        select newdok from mag_create_dok_koryg(:grupasped, CURRENT_TIMESTAMP, null) into :docid;
        --po liscie wysylkowej tworze pozycje zz
        for
          select lwp.wersjaref, (coalesce(lwp.iloscmax, 0)- coalesce(lwp.iloscspk, 0)) as iloscdospk,
                  w.ktm, w.nrwersji, ms.serialno, lwp.jedno, lwp.dokpoz, lwp.altposmode
            from listywysdpoz lwp
              left join wersje w on (lwp.wersjaref = w.ref)
              left join x_mws_serie ms on (ms.ref = w.x_mws_serie)
            where lwp.dokument = :sped_doc and (coalesce(lwp.iloscmax, 0)- coalesce(lwp.iloscspk, 0) )> 0
            order by w.ktm
          into :wersjaref, :iloscdospk, :ktm, :nrwersji, :serialno, :jedno, :dokpozk, :altposmode
        do begin
          if (coalesce(altposmode,0) > 0) then begin --jesli mamy doczynienia z elementem skladowym myjni
            select d.ktm, d.wersja, d.wersjaref
              from dokumpoz d
                where d.ref = :altposmode
            into :ktm, :nrwersji, :wersjaref; --na dok ZZ cala myjnia
          end
          if (not exists(select first 1 1 from dokumpoz d where d.dokument = :docid and d.wersjaref <>:wersjaref)) then begin    --dodaje myjnie tylko raz
            execute procedure gen_ref('DOKUMPOZ') returning_values docpoz;
            insert into DOKUMPOZ(REF, DOKUMENT, wersjaref, ilosc, ktm, wersja, x_serial_no, x_slownik, jedno, kortopoz)
              values (:docpoz, :docid, :wersjaref, :iloscdospk, :ktm, :nrwersji, :serialno, :sloref, :jedno, :dokpozk);
          end
        end
    
        update dokumnag set AKCEPT=9 where ref=:docid;
        update dokumnag set AKCEPT=1 where ref=:docid;
    
        select 'Wystawiono dokument '||d.symbol||' . Dokument zaakceptowany.'
          from dokumnag d
          where d.ref = :docid
        into :msg;

        execute procedure XK_LISTYWYSD_CHECKPAK (sped_doc,1) --wywolywana jest przed X_MWS_PACKING_ZZ, ale tylko zwraca -> pakowanie nie skonczone, wiec trzeba ja odpalic po ZZ
          returning_values (topack1, msg1,  status1);
        if (status1 = 0) then begin
          msg = msg1;
          suspend;
          exit;
        end

        execute procedure X_MWS_RELASE_CART(doknagk); -- tak jak X_MWS_PACKING_ZZ
    end
  suspend;
end^
SET TERM ; ^
