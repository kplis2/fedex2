--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_RCP_ZEST_SUM(
      EMPL integer,
      FROMDATE date,
      TODATE date)
  returns (
      JOBTIMESTR varchar(8) CHARACTER SET UTF8                           ,
      NOMINALSTR varchar(8) CHARACTER SET UTF8                           ,
      AVERAGESTR varchar(8) CHARACTER SET UTF8                           )
   as
declare variable jobtime integer;
  declare variable nominal integer;
  declare variable days integer;
  declare variable average integer;
BEGIN
  --DS: funkcja zwraca liczbe godzin w poszczegolnych dniach, jaka zostala zarejestrowana w systemie
  if (empl = 0) then exit;
  select sum(p.jobtime)
    from epresencelists n
      join epresencelistspos p on n.ref = p.epresencelists
    where n.listsdate >= :FROMDATE
      and n.listsdate <= :TODATE
      and p.employee = :empl
    into :jobtime;

  select ws, wd
    from ecal_work(:empl,:FROMDATE,:TODATE)
    into :nominal, :days;
  average = 1.00 * jobtime / days;

  execute procedure durationstr2(nominal) returning_values nominalstr;
  execute procedure durationstr2(jobtime) returning_values jobtimestr;
  execute procedure durationstr2(average) returning_values averagestr;
  suspend;
end^
SET TERM ; ^
