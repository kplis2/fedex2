--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_10(
      BKDOC integer)
   as
declare variable amount numeric(14,2);
declare variable amount1 numeric(14,2);
declare variable account varchar(20);
declare variable descript varchar(80);
declare variable period varchar(6);
declare variable status integer;
declare variable symbol varchar(20);
declare variable department varchar(10);
declare variable kto varchar(255);
declare variable current_company integer;
begin
  -- schemat dekretowania - place
  select period, company from bkdocs where ref = :bkdoc
    into :period, :current_company;

  --sprawdzanie czy wszystkie listy plac sa zamkniete
  for
    select status
    from epayrolls
    where cper = :period
    into :status
  do begin
    if (status = 0) then
      exception payroll_is_open;
  end
  -- BRUTTO I CHOROBOWE

  select sum(P.pvalue)
    from eprpos P
      join epayrolls PR on (P.payroll = PR.ref and PR.cper = :period and PR.empltype = 1)
    where P.ecolumn = 4000
    into :amount;

  select sum(P.pvalue)
    from eprpos P
      join epayrolls PR on (P.payroll = PR.ref and PR.cper = :period and PR.empltype = 1)
    where P.ecolumn >= 3500 and P.ecolumn <= 3610
    into :amount1;

  if (amount1 is null) then
    amount1 = 0;

  amount = amount - amount1;
  descript = 'koszty wynagrodzenia';
  account = '430-01';
  execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

  descript = 'koszty wyn. chorobowego';
  amount = amount1;
  account = '430-02';
  execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);
  
  -- zasilki ZUS
  descript = 'zasiłki ZUS';
  select sum(P.pvalue)
    from eprpos P
      join epayrolls PR on (P.payroll = PR.ref and PR.cper = :period and PR.empltype = 1)
    where P.ecolumn = 4900
    into :amount;
  account = '227-51';
  execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);


  -- ryczalt samochodowy
  descript = 'ryczałty samochodowe';
  select sum(P.pvalue)
    from eprpos P
      join epayrolls PR on (P.payroll = PR.ref and PR.cper = :period and PR.empltype = 1)
    where P.ecolumn = 5100
    into :amount;
  account = '460-05';
  execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

  -- ZUS pracodawcy
  descript = 'ubezpieczenia społeczne pracowcy';
  select sum(P.pvalue)
    from eprpos P
      join epayrolls PR on (P.payroll = PR.ref and PR.cper = :period and PR.empltype = 1)
    where P.ecolumn >= 8200 and P.ecolumn <= 8220
    into :amount;
  account = '440-01';
  execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);
  
  -- FP i FGŚP
  descript = 'FP i FGŚP';
  select sum(P.pvalue)
    from eprpos P
      join epayrolls PR on (P.payroll = PR.ref and PR.cper = :period and PR.empltype = 1)
    where P.ecolumn >= 8810 and P.ecolumn <= 8820
    into :amount;
  account = '440-02';
  execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

  account = '227-53';
  execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

  -- ZUS
  descript = 'ubezpieczenia społeczne';
  select sum(P.pvalue)
    from eprpos P
      join epayrolls PR on (P.payroll = PR.ref and PR.cper = :period and PR.empltype = 1)
    where (P.ecolumn >= 6100 and P.ecolumn <= 6130) or (P.ecolumn >= 8200 and P.ecolumn <= 8220)
    into :amount;
  account = '227-51';
  execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

  -- NFZ
  descript = 'ubezpieczenie zdrowotne';
  select sum(P.pvalue)
    from eprpos P
      join epayrolls PR on (P.payroll = PR.ref and PR.cper = :period and PR.empltype = 1)
    where P.ecolumn = 7220
    into :amount;
  account = '227-52';
  execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

  -- Podatek
  descript = 'podatek dochodowy';
  select sum(P.pvalue)
    from eprpos P
      join epayrolls PR on (P.payroll = PR.ref and PR.cper = :period and PR.empltype = 1)
    where P.ecolumn >= 7350 and P.ecolumn <= 7380
    into :amount;
  account = '222-01';
  execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

  -- pozostale potracenia
  descript = 'pozostałe potrącenia';
  select sum(P.pvalue)
    from eprpos P
      join epayrolls PR on (P.payroll = PR.ref and PR.cper = :period and PR.empltype = 1)
    where P.ecolumn = 7850
    into :amount;
  account = '???';
  execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

  --do wyplaty
  descript = 'do wypłaty';
  for
    select E.symbol, sum(P.pvalue), e.personnames
      from eprpos P
        join epayrolls PR on (P.payroll = PR.ref and PR.cper = :period and PR.empltype = 1)
        join employees E on (E.ref = P.employee)
      where P.ecolumn = 9000 or P.ecolumn = 7800
      group by E.symbol, e.personnames
      into :symbol, :amount, :kto
  do begin
    descript = 'do wypłaty ' || substring(kto from 1 for 50);
    account = '231-' || symbol;
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);
  end

  -- PZU
  descript = 'PZU';
  select sum(P.pvalue)
    from eprpos P
      join epayrolls PR on (P.payroll = PR.ref and PR.cper = :period and PR.empltype = 1)
    where P.ecolumn = 7600
    into :amount;
  account = '240-004';
  execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

  -- Commercial
  descript = 'Commercial Union';
  select sum(P.pvalue)
    from eprpos P
      join epayrolls PR on (P.payroll = PR.ref and PR.cper = :period and PR.empltype = 1)
    where P.ecolumn = 7610
    into :amount;
  account = '240-005';
  execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

  -- komornik
  descript = 'zajęcia sądowe';
  select sum(P.pvalue)
    from eprpos P
      join epayrolls PR on (P.payroll = PR.ref and PR.cper = :period and PR.empltype = 1)
    where P.ecolumn = 7710
    into :amount;
  account = '240-008';
  execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);
  
  --Piątki
  descript = 'koszty wynagrodzenia';
  for
    select E.department, sum(P.pvalue)
      from eprpos P
        join epayrolls PR on (P.payroll = PR.ref and PR.cper = :period and PR.tper = :period
          and PR.empltype = 1)
        join employees E on (E.ref = P.employee)
      where P.ecolumn = 4000 or P.ecolumn = 5100 or (P.ecolumn >= 8200 and P.ecolumn <= 8220)
        or (P.ecolumn >= 8810 and P.ecolumn <= 8820)
      group by E.department
      into :department, :amount
  do begin
    account = '???';
    if (department = 'HURTOWNIA') then
      account = '502';
    else if (department = 'KSIĘGOWOŚĆ') then
      account = '550';
    else if (department = 'USŁUGI') then
      account = '501-000';
    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

    if (account = '501-000') then
    begin
      execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);
      account = '701';
      execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);
    end
    account = '490';
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);
  end

  -- z poprzednich miesiecy
  for
    select E.department, sum(P.pvalue)
      from eprpos P
        join epayrolls PR on (P.payroll = PR.ref and PR.cper < :period and PR.tper = :period
          and PR.empltype = 1)
        join employees E on (E.ref = P.employee)
      where P.ecolumn = 4000 or P.ecolumn = 5100 or (P.ecolumn >= 8200 and P.ecolumn <= 8220)
        or (P.ecolumn >= 8810 and P.ecolumn <= 8820)
      group by E.department
      into :department, :amount
  do begin
    account = '???';
    if (department = 'HURTOWNIA') then
      account = '502';
    else if (department = 'KSIĘGOWOŚĆ') then
      account = '550';
    else if (department = 'USŁUGI') then
      account = '501-000';
    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

    if (account = '501-000') then
    begin
      execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);
      account = '701';
      execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);
    end
    
    account = '645';
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);
  end

  select sum(P.pvalue)
    from eprpos P
      join epayrolls PR on (P.payroll = PR.ref and PR.cper = :period and PR.tper > :period
        and PR.empltype = 1)
      join employees E on (E.ref = P.employee)
    where P.ecolumn = 4000 or P.ecolumn = 5100 or (P.ecolumn >= 8200 and P.ecolumn <= 8220)
      or (P.ecolumn >= 8810 and P.ecolumn <= 8820)
    into :amount;

  account = '645';
  execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

  account = '490';
  execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

  update bkdocs set otable = 'EPAYROLLS', oref = :period where ref=:bkdoc;
  update epayrolls set status = 2 where cper = :period and empltype = 1 and company = :current_company;
end^
SET TERM ; ^
