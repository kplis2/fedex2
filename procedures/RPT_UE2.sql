--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_UE2(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      YEARID varchar(4) CHARACTER SET UTF8                           ,
      COMPANY integer,
      RODZAJ integer)
  returns (
      CA1 varchar(2) CHARACTER SET UTF8                           ,
      CB1 varchar(15) CHARACTER SET UTF8                           ,
      CC1 numeric(14,2),
      CD1 varchar(1) CHARACTER SET UTF8                           ,
      CA2 varchar(2) CHARACTER SET UTF8                           ,
      CB2 varchar(15) CHARACTER SET UTF8                           ,
      CC2 numeric(14,2),
      CD2 varchar(1) CHARACTER SET UTF8                           ,
      CA3 varchar(2) CHARACTER SET UTF8                           ,
      CB3 varchar(15) CHARACTER SET UTF8                           ,
      CC3 numeric(14,2),
      CD3 varchar(1) CHARACTER SET UTF8                           ,
      CA4 varchar(2) CHARACTER SET UTF8                           ,
      CB4 varchar(15) CHARACTER SET UTF8                           ,
      CC4 numeric(14,2),
      CD4 varchar(1) CHARACTER SET UTF8                           ,
      CA5 varchar(2) CHARACTER SET UTF8                           ,
      CB5 varchar(15) CHARACTER SET UTF8                           ,
      CC5 numeric(14,2),
      CD5 varchar(1) CHARACTER SET UTF8                           ,
      CA6 varchar(2) CHARACTER SET UTF8                           ,
      CB6 varchar(15) CHARACTER SET UTF8                           ,
      CC6 numeric(14,2),
      CD6 varchar(1) CHARACTER SET UTF8                           ,
      CA7 varchar(2) CHARACTER SET UTF8                           ,
      CB7 varchar(15) CHARACTER SET UTF8                           ,
      CC7 numeric(14,2),
      CD7 varchar(1) CHARACTER SET UTF8                           ,
      CA8 varchar(2) CHARACTER SET UTF8                           ,
      CB8 varchar(15) CHARACTER SET UTF8                           ,
      CC8 numeric(14,2),
      CD8 varchar(1) CHARACTER SET UTF8                           ,
      CA9 varchar(2) CHARACTER SET UTF8                           ,
      CB9 varchar(15) CHARACTER SET UTF8                           ,
      CC9 numeric(14,2),
      CD9 varchar(1) CHARACTER SET UTF8                           ,
      CA10 varchar(2) CHARACTER SET UTF8                           ,
      CB10 varchar(15) CHARACTER SET UTF8                           ,
      CC10 numeric(14,2),
      CD10 varchar(1) CHARACTER SET UTF8                           ,
      CA11 varchar(2) CHARACTER SET UTF8                           ,
      CB11 varchar(15) CHARACTER SET UTF8                           ,
      CC11 numeric(14,2),
      CD11 varchar(1) CHARACTER SET UTF8                           ,
      CA12 varchar(2) CHARACTER SET UTF8                           ,
      CB12 varchar(15) CHARACTER SET UTF8                           ,
      CC12 numeric(14,2),
      CD12 varchar(1) CHARACTER SET UTF8                           ,
      CA13 varchar(2) CHARACTER SET UTF8                           ,
      CB13 varchar(15) CHARACTER SET UTF8                           ,
      CC13 numeric(14,2),
      CD13 varchar(1) CHARACTER SET UTF8                           ,
      CA14 varchar(2) CHARACTER SET UTF8                           ,
      CB14 varchar(15) CHARACTER SET UTF8                           ,
      CC14 numeric(14,2),
      CD14 varchar(1) CHARACTER SET UTF8                           ,
      CA15 varchar(2) CHARACTER SET UTF8                           ,
      CB15 varchar(15) CHARACTER SET UTF8                           ,
      CC15 numeric(14,2),
      CD15 varchar(1) CHARACTER SET UTF8                           ,
      CA16 varchar(2) CHARACTER SET UTF8                           ,
      CB16 varchar(15) CHARACTER SET UTF8                           ,
      CC16 numeric(14,2),
      CD16 varchar(1) CHARACTER SET UTF8                           ,
      CA17 varchar(2) CHARACTER SET UTF8                           ,
      CB17 varchar(15) CHARACTER SET UTF8                           ,
      CC17 numeric(14,2),
      CD17 varchar(1) CHARACTER SET UTF8                           ,
      CA18 varchar(2) CHARACTER SET UTF8                           ,
      CB18 varchar(15) CHARACTER SET UTF8                           ,
      CC18 numeric(14,2),
      CD18 varchar(1) CHARACTER SET UTF8                           ,
      CA19 varchar(2) CHARACTER SET UTF8                           ,
      CB19 varchar(15) CHARACTER SET UTF8                           ,
      CC19 numeric(14,2),
      CD19 varchar(1) CHARACTER SET UTF8                           ,
      CA20 varchar(2) CHARACTER SET UTF8                           ,
      CB20 varchar(15) CHARACTER SET UTF8                           ,
      CC20 numeric(14,2),
      CD20 varchar(1) CHARACTER SET UTF8                           ,
      CA21 varchar(2) CHARACTER SET UTF8                           ,
      CB21 varchar(15) CHARACTER SET UTF8                           ,
      CC21 numeric(14,2),
      CD21 varchar(1) CHARACTER SET UTF8                           ,
      CA22 varchar(2) CHARACTER SET UTF8                           ,
      CB22 varchar(15) CHARACTER SET UTF8                           ,
      CC22 numeric(14,2),
      CD22 varchar(1) CHARACTER SET UTF8                           ,
      CA23 varchar(2) CHARACTER SET UTF8                           ,
      CB23 varchar(15) CHARACTER SET UTF8                           ,
      CC23 numeric(14,2),
      CD23 varchar(1) CHARACTER SET UTF8                           ,
      CA24 varchar(2) CHARACTER SET UTF8                           ,
      CB24 varchar(15) CHARACTER SET UTF8                           ,
      CC24 numeric(14,2),
      CD24 varchar(1) CHARACTER SET UTF8                           ,
      CA25 varchar(2) CHARACTER SET UTF8                           ,
      CB25 varchar(15) CHARACTER SET UTF8                           ,
      CC25 numeric(14,2),
      CD25 varchar(1) CHARACTER SET UTF8                           ,
      CA26 varchar(2) CHARACTER SET UTF8                           ,
      CB26 varchar(15) CHARACTER SET UTF8                           ,
      CC26 numeric(14,2),
      CD26 varchar(1) CHARACTER SET UTF8                           ,
      CA27 varchar(2) CHARACTER SET UTF8                           ,
      CB27 varchar(15) CHARACTER SET UTF8                           ,
      CC27 numeric(14,2),
      CD27 varchar(1) CHARACTER SET UTF8                           ,
      CA28 varchar(2) CHARACTER SET UTF8                           ,
      CB28 varchar(15) CHARACTER SET UTF8                           ,
      CC28 numeric(14,2),
      CD28 varchar(1) CHARACTER SET UTF8                           ,
      CA29 varchar(2) CHARACTER SET UTF8                           ,
      CB29 varchar(15) CHARACTER SET UTF8                           ,
      CC29 numeric(14,2),
      CD29 varchar(1) CHARACTER SET UTF8                           ,
      CA30 varchar(2) CHARACTER SET UTF8                           ,
      CB30 varchar(15) CHARACTER SET UTF8                           ,
      CC30 numeric(14,2),
      CD30 varchar(1) CHARACTER SET UTF8                           ,
      CA31 varchar(2) CHARACTER SET UTF8                           ,
      CB31 varchar(15) CHARACTER SET UTF8                           ,
      CC31 numeric(14,2),
      CD31 varchar(1) CHARACTER SET UTF8                           ,
      CA32 varchar(2) CHARACTER SET UTF8                           ,
      CB32 varchar(15) CHARACTER SET UTF8                           ,
      CC32 numeric(14,2),
      CD32 varchar(1) CHARACTER SET UTF8                           ,
      CA33 varchar(2) CHARACTER SET UTF8                           ,
      CB33 varchar(15) CHARACTER SET UTF8                           ,
      CC33 numeric(14,2),
      CD33 varchar(1) CHARACTER SET UTF8                           ,
      CA34 varchar(2) CHARACTER SET UTF8                           ,
      CB34 varchar(15) CHARACTER SET UTF8                           ,
      CC34 numeric(14,2),
      CD34 varchar(1) CHARACTER SET UTF8                           ,
      CA35 varchar(2) CHARACTER SET UTF8                           ,
      CB35 varchar(15) CHARACTER SET UTF8                           ,
      CC35 numeric(14,2),
      CD35 varchar(1) CHARACTER SET UTF8                           ,
      CA36 varchar(2) CHARACTER SET UTF8                           ,
      CB36 varchar(15) CHARACTER SET UTF8                           ,
      CC36 numeric(14,2),
      CD36 varchar(1) CHARACTER SET UTF8                           ,
      CA37 varchar(2) CHARACTER SET UTF8                           ,
      CB37 varchar(15) CHARACTER SET UTF8                           ,
      CC37 numeric(14,2),
      CD37 varchar(1) CHARACTER SET UTF8                           ,
      CA38 varchar(2) CHARACTER SET UTF8                           ,
      CB38 varchar(15) CHARACTER SET UTF8                           ,
      CC38 numeric(14,2),
      CD38 varchar(1) CHARACTER SET UTF8                           ,
      CA39 varchar(2) CHARACTER SET UTF8                           ,
      CB39 varchar(15) CHARACTER SET UTF8                           ,
      CC39 numeric(14,2),
      CD39 varchar(1) CHARACTER SET UTF8                           ,
      CA40 varchar(2) CHARACTER SET UTF8                           ,
      CB40 varchar(15) CHARACTER SET UTF8                           ,
      CC40 numeric(14,2),
      CD40 varchar(1) CHARACTER SET UTF8                           ,
      CA41 varchar(2) CHARACTER SET UTF8                           ,
      CB41 varchar(15) CHARACTER SET UTF8                           ,
      CC41 numeric(14,2),
      CD41 varchar(1) CHARACTER SET UTF8                           ,
      CA42 varchar(2) CHARACTER SET UTF8                           ,
      CB42 varchar(15) CHARACTER SET UTF8                           ,
      CC42 numeric(14,2),
      CD42 varchar(1) CHARACTER SET UTF8                           ,
      CA43 varchar(2) CHARACTER SET UTF8                           ,
      CB43 varchar(15) CHARACTER SET UTF8                           ,
      CC43 numeric(14,2),
      CD43 varchar(1) CHARACTER SET UTF8                           ,
      CA44 varchar(2) CHARACTER SET UTF8                           ,
      CB44 varchar(15) CHARACTER SET UTF8                           ,
      CC44 numeric(14,2),
      CD44 varchar(1) CHARACTER SET UTF8                           ,
      CA45 varchar(2) CHARACTER SET UTF8                           ,
      CB45 varchar(15) CHARACTER SET UTF8                           ,
      CC45 numeric(14,2),
      CD45 varchar(1) CHARACTER SET UTF8                           ,
      CA46 varchar(2) CHARACTER SET UTF8                           ,
      CB46 varchar(15) CHARACTER SET UTF8                           ,
      CC46 numeric(14,2),
      CD46 varchar(1) CHARACTER SET UTF8                           ,
      CA47 varchar(2) CHARACTER SET UTF8                           ,
      CB47 varchar(15) CHARACTER SET UTF8                           ,
      CC47 numeric(14,2),
      CD47 varchar(1) CHARACTER SET UTF8                           ,
      CA48 varchar(2) CHARACTER SET UTF8                           ,
      CB48 varchar(15) CHARACTER SET UTF8                           ,
      CC48 numeric(14,2),
      CD48 varchar(1) CHARACTER SET UTF8                           ,
      CA49 varchar(2) CHARACTER SET UTF8                           ,
      CB49 varchar(15) CHARACTER SET UTF8                           ,
      CC49 numeric(14,2),
      CD49 varchar(1) CHARACTER SET UTF8                           ,
      CA50 varchar(2) CHARACTER SET UTF8                           ,
      CB50 varchar(15) CHARACTER SET UTF8                           ,
      CC50 numeric(14,2),
      CD50 varchar(1) CHARACTER SET UTF8                           ,
      CA51 varchar(2) CHARACTER SET UTF8                           ,
      CB51 varchar(15) CHARACTER SET UTF8                           ,
      CC51 numeric(14,2),
      CD51 varchar(1) CHARACTER SET UTF8                           ,
      CA52 varchar(2) CHARACTER SET UTF8                           ,
      CB52 varchar(15) CHARACTER SET UTF8                           ,
      CC52 numeric(14,2),
      CD52 varchar(1) CHARACTER SET UTF8                           ,
      CA53 varchar(2) CHARACTER SET UTF8                           ,
      CB53 varchar(15) CHARACTER SET UTF8                           ,
      CC53 numeric(14,2),
      CD53 varchar(1) CHARACTER SET UTF8                           ,
      CA54 varchar(2) CHARACTER SET UTF8                           ,
      CB54 varchar(15) CHARACTER SET UTF8                           ,
      CC54 numeric(14,2),
      CD54 varchar(1) CHARACTER SET UTF8                           ,
      CA55 varchar(2) CHARACTER SET UTF8                           ,
      CB55 varchar(15) CHARACTER SET UTF8                           ,
      CC55 numeric(14,2),
      CD55 varchar(1) CHARACTER SET UTF8                           ,
      CA56 varchar(2) CHARACTER SET UTF8                           ,
      CB56 varchar(15) CHARACTER SET UTF8                           ,
      CC56 numeric(14,2),
      CD56 varchar(1) CHARACTER SET UTF8                           ,
      CA57 varchar(2) CHARACTER SET UTF8                           ,
      CB57 varchar(15) CHARACTER SET UTF8                           ,
      CC57 numeric(14,2),
      CD57 varchar(1) CHARACTER SET UTF8                           ,
      CA58 varchar(2) CHARACTER SET UTF8                           ,
      CB58 varchar(15) CHARACTER SET UTF8                           ,
      CC58 numeric(14,2),
      CD58 varchar(1) CHARACTER SET UTF8                           ,
      CA59 varchar(2) CHARACTER SET UTF8                           ,
      CB59 varchar(15) CHARACTER SET UTF8                           ,
      CC59 numeric(14,2),
      CD59 varchar(1) CHARACTER SET UTF8                           ,
      DA1 varchar(2) CHARACTER SET UTF8                           ,
      DB1 varchar(15) CHARACTER SET UTF8                           ,
      DC1 numeric(14,2),
      DD1 varchar(1) CHARACTER SET UTF8                           ,
      DA2 varchar(2) CHARACTER SET UTF8                           ,
      DB2 varchar(15) CHARACTER SET UTF8                           ,
      DC2 numeric(14,2),
      DD2 varchar(1) CHARACTER SET UTF8                           ,
      DA3 varchar(2) CHARACTER SET UTF8                           ,
      DB3 varchar(15) CHARACTER SET UTF8                           ,
      DC3 numeric(14,2),
      DD3 varchar(1) CHARACTER SET UTF8                           ,
      DA4 varchar(2) CHARACTER SET UTF8                           ,
      DB4 varchar(15) CHARACTER SET UTF8                           ,
      DC4 numeric(14,2),
      DD4 varchar(1) CHARACTER SET UTF8                           ,
      DA5 varchar(2) CHARACTER SET UTF8                           ,
      DB5 varchar(15) CHARACTER SET UTF8                           ,
      DC5 numeric(14,2),
      DD5 varchar(1) CHARACTER SET UTF8                           ,
      DA6 varchar(2) CHARACTER SET UTF8                           ,
      DB6 varchar(15) CHARACTER SET UTF8                           ,
      DC6 numeric(14,2),
      DD6 varchar(1) CHARACTER SET UTF8                           ,
      DA7 varchar(2) CHARACTER SET UTF8                           ,
      DB7 varchar(15) CHARACTER SET UTF8                           ,
      DC7 numeric(14,2),
      DD7 varchar(1) CHARACTER SET UTF8                           ,
      DA8 varchar(2) CHARACTER SET UTF8                           ,
      DB8 varchar(15) CHARACTER SET UTF8                           ,
      DC8 numeric(14,2),
      DD8 varchar(1) CHARACTER SET UTF8                           ,
      DA9 varchar(2) CHARACTER SET UTF8                           ,
      DB9 varchar(15) CHARACTER SET UTF8                           ,
      DC9 numeric(14,2),
      DD9 varchar(1) CHARACTER SET UTF8                           ,
      DA10 varchar(2) CHARACTER SET UTF8                           ,
      DB10 varchar(15) CHARACTER SET UTF8                           ,
      DC10 numeric(14,2),
      DD10 varchar(1) CHARACTER SET UTF8                           ,
      DA11 varchar(2) CHARACTER SET UTF8                           ,
      DB11 varchar(15) CHARACTER SET UTF8                           ,
      DC11 numeric(14,2),
      DD11 varchar(1) CHARACTER SET UTF8                           ,
      DA12 varchar(2) CHARACTER SET UTF8                           ,
      DB12 varchar(15) CHARACTER SET UTF8                           ,
      DC12 numeric(14,2),
      DD12 varchar(1) CHARACTER SET UTF8                           ,
      DA13 varchar(2) CHARACTER SET UTF8                           ,
      DB13 varchar(15) CHARACTER SET UTF8                           ,
      DC13 numeric(14,2),
      DD13 varchar(1) CHARACTER SET UTF8                           ,
      DA14 varchar(2) CHARACTER SET UTF8                           ,
      DB14 varchar(15) CHARACTER SET UTF8                           ,
      DC14 numeric(14,2),
      DD14 varchar(1) CHARACTER SET UTF8                           ,
      DA15 varchar(2) CHARACTER SET UTF8                           ,
      DB15 varchar(15) CHARACTER SET UTF8                           ,
      DC15 numeric(14,2),
      DD15 varchar(1) CHARACTER SET UTF8                           ,
      DA16 varchar(2) CHARACTER SET UTF8                           ,
      DB16 varchar(15) CHARACTER SET UTF8                           ,
      DC16 numeric(14,2),
      DD16 varchar(1) CHARACTER SET UTF8                           ,
      DA17 varchar(2) CHARACTER SET UTF8                           ,
      DB17 varchar(15) CHARACTER SET UTF8                           ,
      DC17 numeric(14,2),
      DD17 varchar(1) CHARACTER SET UTF8                           ,
      DA18 varchar(2) CHARACTER SET UTF8                           ,
      DB18 varchar(15) CHARACTER SET UTF8                           ,
      DC18 numeric(14,2),
      DD18 varchar(1) CHARACTER SET UTF8                           ,
      DA19 varchar(2) CHARACTER SET UTF8                           ,
      DB19 varchar(15) CHARACTER SET UTF8                           ,
      DC19 numeric(14,2),
      DD19 varchar(1) CHARACTER SET UTF8                           ,
      DA20 varchar(2) CHARACTER SET UTF8                           ,
      DB20 varchar(15) CHARACTER SET UTF8                           ,
      DC20 numeric(14,2),
      DD20 varchar(1) CHARACTER SET UTF8                           ,
      DA21 varchar(2) CHARACTER SET UTF8                           ,
      DB21 varchar(15) CHARACTER SET UTF8                           ,
      DC21 numeric(14,2),
      DD21 varchar(1) CHARACTER SET UTF8                           ,
      DA22 varchar(2) CHARACTER SET UTF8                           ,
      DB22 varchar(15) CHARACTER SET UTF8                           ,
      DC22 numeric(14,2),
      DD22 varchar(1) CHARACTER SET UTF8                           ,
      DA23 varchar(2) CHARACTER SET UTF8                           ,
      DB23 varchar(15) CHARACTER SET UTF8                           ,
      DC23 numeric(14,2),
      DD23 varchar(1) CHARACTER SET UTF8                           ,
      DA24 varchar(2) CHARACTER SET UTF8                           ,
      DB24 varchar(15) CHARACTER SET UTF8                           ,
      DC24 numeric(14,2),
      DD24 varchar(1) CHARACTER SET UTF8                           ,
      DA25 varchar(2) CHARACTER SET UTF8                           ,
      DB25 varchar(15) CHARACTER SET UTF8                           ,
      DC25 numeric(14,2),
      DD25 varchar(1) CHARACTER SET UTF8                           ,
      DA26 varchar(2) CHARACTER SET UTF8                           ,
      DB26 varchar(15) CHARACTER SET UTF8                           ,
      DC26 numeric(14,2),
      DD26 varchar(1) CHARACTER SET UTF8                           ,
      DA27 varchar(2) CHARACTER SET UTF8                           ,
      DB27 varchar(15) CHARACTER SET UTF8                           ,
      DC27 numeric(14,2),
      DD27 varchar(1) CHARACTER SET UTF8                           ,
      DA28 varchar(2) CHARACTER SET UTF8                           ,
      DB28 varchar(15) CHARACTER SET UTF8                           ,
      DC28 numeric(14,2),
      DD28 varchar(1) CHARACTER SET UTF8                           ,
      DA29 varchar(2) CHARACTER SET UTF8                           ,
      DB29 varchar(15) CHARACTER SET UTF8                           ,
      DC29 numeric(14,2),
      DD29 varchar(1) CHARACTER SET UTF8                           ,
      DA30 varchar(2) CHARACTER SET UTF8                           ,
      DB30 varchar(15) CHARACTER SET UTF8                           ,
      DC30 numeric(14,2),
      DD30 varchar(1) CHARACTER SET UTF8                           ,
      DA31 varchar(2) CHARACTER SET UTF8                           ,
      DB31 varchar(15) CHARACTER SET UTF8                           ,
      DC31 numeric(14,2),
      DD31 varchar(1) CHARACTER SET UTF8                           ,
      DA32 varchar(2) CHARACTER SET UTF8                           ,
      DB32 varchar(15) CHARACTER SET UTF8                           ,
      DC32 numeric(14,2),
      DD32 varchar(1) CHARACTER SET UTF8                           ,
      DA33 varchar(2) CHARACTER SET UTF8                           ,
      DB33 varchar(15) CHARACTER SET UTF8                           ,
      DC33 numeric(14,2),
      DD33 varchar(1) CHARACTER SET UTF8                           ,
      DA34 varchar(2) CHARACTER SET UTF8                           ,
      DB34 varchar(15) CHARACTER SET UTF8                           ,
      DC34 numeric(14,2),
      DD34 varchar(1) CHARACTER SET UTF8                           ,
      DA35 varchar(2) CHARACTER SET UTF8                           ,
      DB35 varchar(15) CHARACTER SET UTF8                           ,
      DC35 numeric(14,2),
      DD35 varchar(1) CHARACTER SET UTF8                           ,
      DA36 varchar(2) CHARACTER SET UTF8                           ,
      DB36 varchar(15) CHARACTER SET UTF8                           ,
      DC36 numeric(14,2),
      DD36 varchar(1) CHARACTER SET UTF8                           ,
      DA37 varchar(2) CHARACTER SET UTF8                           ,
      DB37 varchar(15) CHARACTER SET UTF8                           ,
      DC37 numeric(14,2),
      DD37 varchar(1) CHARACTER SET UTF8                           ,
      DA38 varchar(2) CHARACTER SET UTF8                           ,
      DB38 varchar(15) CHARACTER SET UTF8                           ,
      DC38 numeric(14,2),
      DD38 varchar(1) CHARACTER SET UTF8                           ,
      DA39 varchar(2) CHARACTER SET UTF8                           ,
      DB39 varchar(15) CHARACTER SET UTF8                           ,
      DC39 numeric(14,2),
      DD39 varchar(1) CHARACTER SET UTF8                           ,
      DA40 varchar(2) CHARACTER SET UTF8                           ,
      DB40 varchar(15) CHARACTER SET UTF8                           ,
      DC40 numeric(14,2),
      DD40 varchar(1) CHARACTER SET UTF8                           ,
      DA41 varchar(2) CHARACTER SET UTF8                           ,
      DB41 varchar(15) CHARACTER SET UTF8                           ,
      DC41 numeric(14,2),
      DD41 varchar(1) CHARACTER SET UTF8                           ,
      DA42 varchar(2) CHARACTER SET UTF8                           ,
      DB42 varchar(15) CHARACTER SET UTF8                           ,
      DC42 numeric(14,2),
      DD42 varchar(1) CHARACTER SET UTF8                           ,
      DA43 varchar(2) CHARACTER SET UTF8                           ,
      DB43 varchar(15) CHARACTER SET UTF8                           ,
      DC43 numeric(14,2),
      DD43 varchar(1) CHARACTER SET UTF8                           ,
      DA44 varchar(2) CHARACTER SET UTF8                           ,
      DB44 varchar(15) CHARACTER SET UTF8                           ,
      DC44 numeric(14,2),
      DD44 varchar(1) CHARACTER SET UTF8                           ,
      DA45 varchar(2) CHARACTER SET UTF8                           ,
      DB45 varchar(15) CHARACTER SET UTF8                           ,
      DC45 numeric(14,2),
      DD45 varchar(1) CHARACTER SET UTF8                           ,
      DA46 varchar(2) CHARACTER SET UTF8                           ,
      DB46 varchar(15) CHARACTER SET UTF8                           ,
      DC46 numeric(14,2),
      DD46 varchar(1) CHARACTER SET UTF8                           ,
      DA47 varchar(2) CHARACTER SET UTF8                           ,
      DB47 varchar(15) CHARACTER SET UTF8                           ,
      DC47 numeric(14,2),
      DD47 varchar(1) CHARACTER SET UTF8                           ,
      DA48 varchar(2) CHARACTER SET UTF8                           ,
      DB48 varchar(15) CHARACTER SET UTF8                           ,
      DC48 numeric(14,2),
      DD48 varchar(1) CHARACTER SET UTF8                           ,
      DA49 varchar(2) CHARACTER SET UTF8                           ,
      DB49 varchar(15) CHARACTER SET UTF8                           ,
      DC49 numeric(14,2),
      DD49 varchar(1) CHARACTER SET UTF8                           ,
      DA50 varchar(2) CHARACTER SET UTF8                           ,
      DB50 varchar(15) CHARACTER SET UTF8                           ,
      DC50 numeric(14,2),
      DD50 varchar(1) CHARACTER SET UTF8                           ,
      DA51 varchar(2) CHARACTER SET UTF8                           ,
      DB51 varchar(15) CHARACTER SET UTF8                           ,
      DC51 numeric(14,2),
      DD51 varchar(1) CHARACTER SET UTF8                           ,
      DA52 varchar(2) CHARACTER SET UTF8                           ,
      DB52 varchar(15) CHARACTER SET UTF8                           ,
      DC52 numeric(14,2),
      DD52 varchar(1) CHARACTER SET UTF8                           ,
      DA53 varchar(2) CHARACTER SET UTF8                           ,
      DB53 varchar(15) CHARACTER SET UTF8                           ,
      DC53 numeric(14,2),
      DD53 varchar(1) CHARACTER SET UTF8                           ,
      DA54 varchar(2) CHARACTER SET UTF8                           ,
      DB54 varchar(15) CHARACTER SET UTF8                           ,
      DC54 numeric(14,2),
      DD54 varchar(1) CHARACTER SET UTF8                           ,
      DA55 varchar(2) CHARACTER SET UTF8                           ,
      DB55 varchar(15) CHARACTER SET UTF8                           ,
      DC55 numeric(14,2),
      DD55 varchar(1) CHARACTER SET UTF8                           ,
      DA56 varchar(2) CHARACTER SET UTF8                           ,
      DB56 varchar(15) CHARACTER SET UTF8                           ,
      DC56 numeric(14,2),
      DD56 varchar(1) CHARACTER SET UTF8                           ,
      DA57 varchar(2) CHARACTER SET UTF8                           ,
      DB57 varchar(15) CHARACTER SET UTF8                           ,
      DC57 numeric(14,2),
      DD57 varchar(1) CHARACTER SET UTF8                           ,
      DA58 varchar(2) CHARACTER SET UTF8                           ,
      DB58 varchar(15) CHARACTER SET UTF8                           ,
      DC58 numeric(14,2),
      DD58 varchar(1) CHARACTER SET UTF8                           ,
      DA59 varchar(2) CHARACTER SET UTF8                           ,
      DB59 varchar(15) CHARACTER SET UTF8                           ,
      DC59 numeric(14,2),
      DD59 varchar(1) CHARACTER SET UTF8                           ,
      EA1 varchar(2) CHARACTER SET UTF8                           ,
      EB1 varchar(15) CHARACTER SET UTF8                           ,
      EC1 numeric(14,2),
      ED1 varchar(1) CHARACTER SET UTF8                           ,
      EA2 varchar(2) CHARACTER SET UTF8                           ,
      EB2 varchar(15) CHARACTER SET UTF8                           ,
      EC2 numeric(14,2),
      ED2 varchar(1) CHARACTER SET UTF8                           ,
      EA3 varchar(2) CHARACTER SET UTF8                           ,
      EB3 varchar(15) CHARACTER SET UTF8                           ,
      EC3 numeric(14,2),
      ED3 varchar(1) CHARACTER SET UTF8                           ,
      EA4 varchar(2) CHARACTER SET UTF8                           ,
      EB4 varchar(15) CHARACTER SET UTF8                           ,
      EC4 numeric(14,2),
      ED4 varchar(1) CHARACTER SET UTF8                           ,
      EA5 varchar(2) CHARACTER SET UTF8                           ,
      EB5 varchar(15) CHARACTER SET UTF8                           ,
      EC5 numeric(14,2),
      ED5 varchar(1) CHARACTER SET UTF8                           ,
      EA6 varchar(2) CHARACTER SET UTF8                           ,
      EB6 varchar(15) CHARACTER SET UTF8                           ,
      EC6 numeric(14,2),
      ED6 varchar(1) CHARACTER SET UTF8                           ,
      EA7 varchar(2) CHARACTER SET UTF8                           ,
      EB7 varchar(15) CHARACTER SET UTF8                           ,
      EC7 numeric(14,2),
      ED7 varchar(1) CHARACTER SET UTF8                           ,
      EA8 varchar(2) CHARACTER SET UTF8                           ,
      EB8 varchar(15) CHARACTER SET UTF8                           ,
      EC8 numeric(14,2),
      ED8 varchar(1) CHARACTER SET UTF8                           ,
      EA9 varchar(2) CHARACTER SET UTF8                           ,
      EB9 varchar(15) CHARACTER SET UTF8                           ,
      EC9 numeric(14,2),
      ED9 varchar(1) CHARACTER SET UTF8                           ,
      EA10 varchar(2) CHARACTER SET UTF8                           ,
      EB10 varchar(15) CHARACTER SET UTF8                           ,
      EC10 numeric(14,2),
      ED10 varchar(1) CHARACTER SET UTF8                           ,
      EA11 varchar(2) CHARACTER SET UTF8                           ,
      EB11 varchar(15) CHARACTER SET UTF8                           ,
      EC11 numeric(14,2),
      ED11 varchar(1) CHARACTER SET UTF8                           ,
      EA12 varchar(2) CHARACTER SET UTF8                           ,
      EB12 varchar(15) CHARACTER SET UTF8                           ,
      EC12 numeric(14,2),
      ED12 varchar(1) CHARACTER SET UTF8                           ,
      EA13 varchar(2) CHARACTER SET UTF8                           ,
      EB13 varchar(15) CHARACTER SET UTF8                           ,
      EC13 numeric(14,2),
      ED13 varchar(1) CHARACTER SET UTF8                           ,
      EA14 varchar(2) CHARACTER SET UTF8                           ,
      EB14 varchar(15) CHARACTER SET UTF8                           ,
      EC14 numeric(14,2),
      ED14 varchar(1) CHARACTER SET UTF8                           ,
      EA15 varchar(2) CHARACTER SET UTF8                           ,
      EB15 varchar(15) CHARACTER SET UTF8                           ,
      EC15 numeric(14,2),
      ED15 varchar(1) CHARACTER SET UTF8                           ,
      EA16 varchar(2) CHARACTER SET UTF8                           ,
      EB16 varchar(15) CHARACTER SET UTF8                           ,
      EC16 numeric(14,2),
      ED16 varchar(1) CHARACTER SET UTF8                           ,
      EA17 varchar(2) CHARACTER SET UTF8                           ,
      EB17 varchar(15) CHARACTER SET UTF8                           ,
      EC17 numeric(14,2),
      ED17 varchar(1) CHARACTER SET UTF8                           ,
      EA18 varchar(2) CHARACTER SET UTF8                           ,
      EB18 varchar(15) CHARACTER SET UTF8                           ,
      EC18 numeric(14,2),
      ED18 varchar(1) CHARACTER SET UTF8                           ,
      EA19 varchar(2) CHARACTER SET UTF8                           ,
      EB19 varchar(15) CHARACTER SET UTF8                           ,
      EC19 numeric(14,2),
      ED19 varchar(1) CHARACTER SET UTF8                           ,
      EA20 varchar(2) CHARACTER SET UTF8                           ,
      EB20 varchar(15) CHARACTER SET UTF8                           ,
      EC20 numeric(14,2),
      ED20 varchar(1) CHARACTER SET UTF8                           ,
      EA21 varchar(2) CHARACTER SET UTF8                           ,
      EB21 varchar(15) CHARACTER SET UTF8                           ,
      EC21 numeric(14,2),
      ED21 varchar(1) CHARACTER SET UTF8                           ,
      EA22 varchar(2) CHARACTER SET UTF8                           ,
      EB22 varchar(15) CHARACTER SET UTF8                           ,
      EC22 numeric(14,2),
      ED22 varchar(1) CHARACTER SET UTF8                           ,
      EA23 varchar(2) CHARACTER SET UTF8                           ,
      EB23 varchar(15) CHARACTER SET UTF8                           ,
      EC23 numeric(14,2),
      ED23 varchar(1) CHARACTER SET UTF8                           ,
      EA24 varchar(2) CHARACTER SET UTF8                           ,
      EB24 varchar(15) CHARACTER SET UTF8                           ,
      EC24 numeric(14,2),
      ED24 varchar(1) CHARACTER SET UTF8                           ,
      EA25 varchar(2) CHARACTER SET UTF8                           ,
      EB25 varchar(15) CHARACTER SET UTF8                           ,
      EC25 numeric(14,2),
      ED25 varchar(1) CHARACTER SET UTF8                           ,
      EA26 varchar(2) CHARACTER SET UTF8                           ,
      EB26 varchar(15) CHARACTER SET UTF8                           ,
      EC26 numeric(14,2),
      ED26 varchar(1) CHARACTER SET UTF8                           ,
      EA27 varchar(2) CHARACTER SET UTF8                           ,
      EB27 varchar(15) CHARACTER SET UTF8                           ,
      EC27 numeric(14,2),
      ED27 varchar(1) CHARACTER SET UTF8                           ,
      EA28 varchar(2) CHARACTER SET UTF8                           ,
      EB28 varchar(15) CHARACTER SET UTF8                           ,
      EC28 numeric(14,2),
      ED28 varchar(1) CHARACTER SET UTF8                           ,
      EA29 varchar(2) CHARACTER SET UTF8                           ,
      EB29 varchar(15) CHARACTER SET UTF8                           ,
      EC29 numeric(14,2),
      ED29 varchar(1) CHARACTER SET UTF8                           ,
      EA30 varchar(2) CHARACTER SET UTF8                           ,
      EB30 varchar(15) CHARACTER SET UTF8                           ,
      EC30 numeric(14,2),
      ED30 varchar(1) CHARACTER SET UTF8                           ,
      EA31 varchar(2) CHARACTER SET UTF8                           ,
      EB31 varchar(15) CHARACTER SET UTF8                           ,
      EC31 numeric(14,2),
      ED31 varchar(1) CHARACTER SET UTF8                           ,
      EA32 varchar(2) CHARACTER SET UTF8                           ,
      EB32 varchar(15) CHARACTER SET UTF8                           ,
      EC32 numeric(14,2),
      ED32 varchar(1) CHARACTER SET UTF8                           ,
      EA33 varchar(2) CHARACTER SET UTF8                           ,
      EB33 varchar(15) CHARACTER SET UTF8                           ,
      EC33 numeric(14,2),
      ED33 varchar(1) CHARACTER SET UTF8                           ,
      EA34 varchar(2) CHARACTER SET UTF8                           ,
      EB34 varchar(15) CHARACTER SET UTF8                           ,
      EC34 numeric(14,2),
      ED34 varchar(1) CHARACTER SET UTF8                           ,
      EA35 varchar(2) CHARACTER SET UTF8                           ,
      EB35 varchar(15) CHARACTER SET UTF8                           ,
      EC35 numeric(14,2),
      ED35 varchar(1) CHARACTER SET UTF8                           ,
      EA36 varchar(2) CHARACTER SET UTF8                           ,
      EB36 varchar(15) CHARACTER SET UTF8                           ,
      EC36 numeric(14,2),
      ED36 varchar(1) CHARACTER SET UTF8                           ,
      EA37 varchar(2) CHARACTER SET UTF8                           ,
      EB37 varchar(15) CHARACTER SET UTF8                           ,
      EC37 numeric(14,2),
      ED37 varchar(1) CHARACTER SET UTF8                           ,
      EA38 varchar(2) CHARACTER SET UTF8                           ,
      EB38 varchar(15) CHARACTER SET UTF8                           ,
      EC38 numeric(14,2),
      ED38 varchar(1) CHARACTER SET UTF8                           ,
      EA39 varchar(2) CHARACTER SET UTF8                           ,
      EB39 varchar(15) CHARACTER SET UTF8                           ,
      EC39 numeric(14,2),
      ED39 varchar(1) CHARACTER SET UTF8                           ,
      EA40 varchar(2) CHARACTER SET UTF8                           ,
      EB40 varchar(15) CHARACTER SET UTF8                           ,
      EC40 numeric(14,2),
      ED40 varchar(1) CHARACTER SET UTF8                           ,
      EA41 varchar(2) CHARACTER SET UTF8                           ,
      EB41 varchar(15) CHARACTER SET UTF8                           ,
      EC41 numeric(14,2),
      ED41 varchar(1) CHARACTER SET UTF8                           ,
      EA42 varchar(2) CHARACTER SET UTF8                           ,
      EB42 varchar(15) CHARACTER SET UTF8                           ,
      EC42 numeric(14,2),
      ED42 varchar(1) CHARACTER SET UTF8                           ,
      EA43 varchar(2) CHARACTER SET UTF8                           ,
      EB43 varchar(15) CHARACTER SET UTF8                           ,
      EC43 numeric(14,2),
      ED43 varchar(1) CHARACTER SET UTF8                           ,
      EA44 varchar(2) CHARACTER SET UTF8                           ,
      EB44 varchar(15) CHARACTER SET UTF8                           ,
      EC44 numeric(14,2),
      ED44 varchar(1) CHARACTER SET UTF8                           ,
      EA45 varchar(2) CHARACTER SET UTF8                           ,
      EB45 varchar(15) CHARACTER SET UTF8                           ,
      EC45 numeric(14,2),
      ED45 varchar(1) CHARACTER SET UTF8                           ,
      EA46 varchar(2) CHARACTER SET UTF8                           ,
      EB46 varchar(15) CHARACTER SET UTF8                           ,
      EC46 numeric(14,2),
      ED46 varchar(1) CHARACTER SET UTF8                           ,
      EA47 varchar(2) CHARACTER SET UTF8                           ,
      EB47 varchar(15) CHARACTER SET UTF8                           ,
      EC47 numeric(14,2),
      ED47 varchar(1) CHARACTER SET UTF8                           ,
      EA48 varchar(2) CHARACTER SET UTF8                           ,
      EB48 varchar(15) CHARACTER SET UTF8                           ,
      EC48 numeric(14,2),
      ED48 varchar(1) CHARACTER SET UTF8                           ,
      EA49 varchar(2) CHARACTER SET UTF8                           ,
      EB49 varchar(15) CHARACTER SET UTF8                           ,
      EC49 numeric(14,2),
      ED49 varchar(1) CHARACTER SET UTF8                           ,
      EA50 varchar(2) CHARACTER SET UTF8                           ,
      EB50 varchar(15) CHARACTER SET UTF8                           ,
      EC50 numeric(14,2),
      ED50 varchar(1) CHARACTER SET UTF8                           ,
      EA51 varchar(2) CHARACTER SET UTF8                           ,
      EB51 varchar(15) CHARACTER SET UTF8                           ,
      EC51 numeric(14,2),
      ED51 varchar(1) CHARACTER SET UTF8                           ,
      EA52 varchar(2) CHARACTER SET UTF8                           ,
      EB52 varchar(15) CHARACTER SET UTF8                           ,
      EC52 numeric(14,2),
      ED52 varchar(1) CHARACTER SET UTF8                           ,
      EA53 varchar(2) CHARACTER SET UTF8                           ,
      EB53 varchar(15) CHARACTER SET UTF8                           ,
      EC53 numeric(14,2),
      ED53 varchar(1) CHARACTER SET UTF8                           ,
      EA54 varchar(2) CHARACTER SET UTF8                           ,
      EB54 varchar(15) CHARACTER SET UTF8                           ,
      EC54 numeric(14,2),
      ED54 varchar(1) CHARACTER SET UTF8                           ,
      EA55 varchar(2) CHARACTER SET UTF8                           ,
      EB55 varchar(15) CHARACTER SET UTF8                           ,
      EC55 numeric(14,2),
      ED55 varchar(1) CHARACTER SET UTF8                           ,
      EA56 varchar(2) CHARACTER SET UTF8                           ,
      EB56 varchar(15) CHARACTER SET UTF8                           ,
      EC56 numeric(14,2),
      ED56 varchar(1) CHARACTER SET UTF8                           ,
      EA57 varchar(2) CHARACTER SET UTF8                           ,
      EB57 varchar(15) CHARACTER SET UTF8                           ,
      EC57 numeric(14,2),
      ED57 varchar(1) CHARACTER SET UTF8                           ,
      EA58 varchar(2) CHARACTER SET UTF8                           ,
      EB58 varchar(15) CHARACTER SET UTF8                           ,
      EC58 numeric(14,2),
      ED58 varchar(1) CHARACTER SET UTF8                           )
   as
declare variable ile integer;
declare variable niptmp varchar(15);
declare variable nipue varchar(15);
declare variable nipuetmp varchar(15);
declare variable netto numeric(14,2);
declare variable odokresu varchar(2);
declare variable dookresu varchar(2);
declare variable sql varchar(4096);
begin
  ile = 1;
  cc1 = 0;
  cc2 = 0;
  cc3 = 0;
  cc4 = 0;
  cc5 = 0;
  cc6 = 0;
  cc7 = 0;
  cc8 = 0;
  cc9 = 0;
  cc10 = 0;
  cc11 = 0;
  cc12 = 0;
  cc13 = 0;
  cc14 = 0;
  cc15 = 0;
  cc16 = 0;
  cc17 = 0;
  cc18 = 0;
  cc19 = 0;
  cc20 = 0;
  cc21 = 0;
  cc22 = 0;
  cc23 = 0;
  cc24 = 0;
  cc25 = 0;
  cc26 = 0;
  cc27 = 0;
  cc28 = 0;
  cc29 = 0;
  cc30 = 0;
  cc31 = 0;
  cc32 = 0;
  cc33 = 0;
  cc34 = 0;
  cc35 = 0;
  cc36 = 0;
  cc37 = 0;
  cc38 = 0;
  cc39 = 0;
  cc40 = 0;
  cc41 = 0;
  cc42 = 0;
  cc43 = 0;
  cc44 = 0;
  cc45 = 0;
  cc46 = 0;
  cc47 = 0;
  cc48 = 0;
  cc49 = 0;
  cc50 = 0;
  cc51 = 0;
  cc52 = 0;
  cc53 = 0;
  cc54 = 0;
  cc55 = 0;
  cc56 = 0;
  cc57 = 0;
  cc58 = 0;
  cc59 = 0;
  DC1 = 0;
  DC2 = 0;
  DC3 = 0;
  DC4 = 0;
  DC5 = 0;
  DC6 = 0;
  DC7 = 0;
  DC8 = 0;
  DC9 = 0;
  DC10 = 0;
  DC11 = 0;
  DC12 = 0;
  DC13 = 0;
  DC14 = 0;
  DC15 = 0;
  DC16 = 0;
  DC17 = 0;
  DC18 = 0;
  DC19 = 0;
  DC20 = 0;
  DC21 = 0;
  DC22 = 0;
  DC23 = 0;
  DC24 = 0;
  DC25 = 0;
  DC26 = 0;
  DC27 = 0;
  DC28 = 0;
  DC29 = 0;
  DC30 = 0;
  DC31 = 0;
  DC32 = 0;
  DC33 = 0;
  DC34 = 0;
  DC35 = 0;
  DC36 = 0;
  DC37 = 0;
  DC38 = 0;
  DC39 = 0;
  DC40 = 0;
  DC41 = 0;
  DC42 = 0;
  DC43 = 0;
  DC44 = 0;
  DC45 = 0;
  DC46 = 0;
  DC47 = 0;
  DC48 = 0;
  DC49 = 0;
  DC50 = 0;
  DC51 = 0;
  DC52 = 0;
  DC53 = 0;
  DC54 = 0;
  DC55 = 0;
  DC56 = 0;
  DC57 = 0;
  DC58 = 0;
  DC59 = 0;
  EC1 = 0;
  EC2 = 0;
  EC3 = 0;
  EC4 = 0;
  EC5 = 0;
  EC6 = 0;
  EC7 = 0;
  EC8 = 0;
  EC9 = 0;
  EC10 = 0;
  EC11 = 0;
  EC12 = 0;
  EC13 = 0;
  EC14 = 0;
  EC15 = 0;
  EC16 = 0;
  EC17 = 0;
  EC18 = 0;
  EC19 = 0;
  EC20 = 0;
  EC21 = 0;
  EC22 = 0;
  EC23 = 0;
  EC24 = 0;
  EC25 = 0;
  EC26 = 0;
  EC27 = 0;
  EC28 = 0;
  EC29 = 0;
  EC30 = 0;
  EC31 = 0;
  EC32 = 0;
  EC33 = 0;
  EC34 = 0;
  EC35 = 0;
  EC36 = 0;
  EC37 = 0;
  EC38 = 0;
  EC39 = 0;
  EC40 = 0;
  EC41 = 0;
  EC42 = 0;
  EC43 = 0;
  EC44 = 0;
  EC45 = 0;
  EC46 = 0;
  EC47 = 0;
  EC48 = 0;
  EC49 = 0;
  EC50 = 0;
  EC51 = 0;
  EC52 = 0;
  EC53 = 0;
  EC54 = 0;
  EC55 = 0;
  EC56 = 0;
  EC57 = 0;
  EC58 = 0;


  if (rodzaj = 0) then
  begin
    if (period = 0) then
    begin
      odokresu = '01';
      dookresu = '03';
    end
    if (period = 1) then
    begin
      odokresu = '04';
      dookresu = '06';
    end
    if (period = 2) then
    begin
      odokresu = '07';
      dookresu = '09';
    end
    if (period = 3) then
    begin
      odokresu = '10';
      dookresu = '12';
    end
  end

  nipuetmp = null;
  sql = '';
--  k42 = cast(k42*100 as integer)/100;  --, B.sumvatv, B.sumgrossv
  sql ='select K.nip, sum(BV.netv)
        from bkdocs B
        join vatregs V on (V.symbol = B.vatreg and V.company = B.company)
        left join klienci K on (K.ref = B.dictpos)
        join bkvatpos BV on (BV.bkdoc = B.ref)
          where v.vtype = 1 and BV.vatgr <> ''NP''
          and B.status > 0';
  if (rodzaj = 1) then sql = sql||' and b.vatperiod ='''||:period||'''';
  else sql = sql||' and substring(b.vatperiod from 1 for 4) = '''||:yearid||'''
          and substring(b.vatperiod from 5 for 2) >= '''||:odokresu||'''
          and substring(b.vatperiod from 5 for 2) <= '''||:dookresu||'''';

  sql = sql||'  and B.company = '||:company||'
          group by K.nip';

  for execute statement sql
        into :nipue, :netto
  do begin
    if (nipuetmp is null) then
      nipuetmp = nipue;
    else if (nipuetmp = nipue) then
      ile = ile -1;

    if (ile = 1) then
    begin
      cb1 = nipue;
      cc1 = cc1 + netto;
      cc1 = cast(cc1 as integer);
    end
    else if (ile = 2) then
    begin
      cb2 = nipue;
      cc2 = cc2 + netto;
      cc2 = cast(cc2 as integer);
    end
    else if (ile = 3) then
    begin
      cb3 = nipue;
      cc3 = cc3 + netto;
      cc3 = cast(cc3 as integer);
    end
    else if (ile = 4) then
    begin
      cb4 = nipue;
      cc4 = cc4 + netto;
      cc4 = cast(cc4 as integer);
    end
    else if (ile = 5) then
    begin
      cb5 = nipue;
      cc5 = cc5 + netto;
      cc5 = cast(cc5 as integer);
    end
    else if (ile = 6) then
    begin
      cb6 = nipue;
      cc6 = cc6 + netto;
      cc6 = cast(cc6 as integer);
    end
    else if (ile = 7) then
    begin
      cb7 = nipue;
      cc7 = cc7 + netto;
      cc7 = cast(cc7 as integer);
    end
    else if (ile = 8) then
    begin
      cb8 = nipue;
      cc8 = cc8 + netto;
      cc8 = cast(cc8 as integer);
    end
    else if (ile = 9) then
    begin
      cb9 = nipue;
      cc9 = cc9 + netto;
      cc9 = cast(cc9 as integer);
    end
    else if (ile = 10) then
    begin
      cb10 = nipue;
      cc10 = cc10 + netto;
      cc10 = cast(cc10 as integer);
    end
    else if (ile = 11) then
    begin
      cb11 = nipue;
      cc11 = cc11 + netto;
      cc11 = cast(cc11 as integer);
    end
    else if (ile = 12) then
    begin
      cb12 = nipue;
      cc12 = cc12 + netto;
      cc12 = cast(cc12 as integer);
    end
    else if (ile = 13) then
    begin
      cb13 = nipue;
      cc13 = cc13 + netto;
      cc13 = cast(cc13 as integer);
    end
    else if (ile = 14) then
    begin
      cb14 = nipue;
      cc14 = cc14 + netto;
      cc14 = cast(cc14 as integer);
    end
    else if (ile = 15) then
    begin
      cb15 = nipue;
      cc15 = cc15 + netto;
      cc15 = cast(cc15 as integer);
    end
    else if (ile = 16) then
    begin
      cb16 = nipue;
      cc16 = cc16 + netto;
      cc16 = cast(cc16 as integer);
    end
    else if (ile = 17) then
    begin
      cb17 = nipue;
      cc17 = cc17 + netto;
      cc17 = cast(cc17 as integer);
    end
    else if (ile = 18) then
    begin
      cb18 = nipue;
      cc18 = cc18 + netto;
      cc18 = cast(cc18 as integer);
    end
    else if (ile = 19) then
    begin
      cb19 = nipue;
      cc19 = cc19 + netto;
      cc19 = cast(cc19 as integer);
    end
    else if (ile = 20) then
    begin
      cb20 = nipue;
      cc20 = cc20 + netto;
      cc20 = cast(cc20 as integer);
    end
    else if (ile = 21) then
    begin
      cb21 = nipue;
      cc21 = cc21 + netto;
      cc21 = cast(cc21 as integer);
    end
    else if (ile = 22) then
    begin
      cb22 = nipue;
      cc22 = cc22 + netto;
      cc22 = cast(cc22 as integer);
    end
    else if (ile = 23) then
    begin
      cb23 = nipue;
      cc23 = cc23 + netto;
      cc23 = cast(cc23 as integer);
    end
    else if (ile = 24) then
    begin
      cb24 = nipue;
      cc24 = cc24 + netto;
      cc24 = cast(cc24 as integer);
    end
    else if (ile = 25) then
    begin
      cb25 = nipue;
      cc25 = cc25 + netto;
      cc25 = cast(cc25 as integer);
    end
    else if (ile = 26) then
    begin
      cb26 = nipue;
      cc26 = cc26 + netto;
      cc26 = cast(cc26 as integer);
    end
    else if (ile = 27) then
    begin
      cb27 = nipue;
      cc27 = cc27 + netto;
      cc27 = cast(cc27 as integer);
    end
    else if (ile = 28) then
    begin
      cb28 = nipue;
      cc28 = cc28 + netto;
      cc28 = cast(cc28 as integer);
    end
    else if (ile = 29) then
    begin
      cb29 = nipue;
      cc29 = cc29 + netto;
      cc29 = cast(cc29 as integer);
    end
    else if (ile = 30) then
    begin
      cb30 = nipue;
      cc30 = cc30 + netto;
      cc30 = cast(cc30 as integer);
    end
    else if (ile = 31) then
    begin
      cb31 = nipue;
      cc31 = cc31 + netto;
      cc31 = cast(cc31 as integer);
    end
    else if (ile = 32) then
    begin
      cb32 = nipue;
      cc32 = cc32 + netto;
      cc32 = cast(cc32 as integer);
    end
    else if (ile = 33) then
    begin
      cb33 = nipue;
      cc33 = cc33 + netto;
      cc33 = cast(cc33 as integer);
    end
    else if (ile = 34) then
    begin
      cb34 = nipue;
      cc34 = cc34 + netto;
      cc34 = cast(cc34 as integer);
    end
    else if (ile = 35) then
    begin
      cb35 = nipue;
      cc35 = cc35 + netto;
      cc35 = cast(cc35 as integer);
    end
    else if (ile = 36) then
    begin
      cb36 = nipue;
      cc36 = cc36 + netto;
      cc36 = cast(cc36 as integer);
    end
    else if (ile = 37) then
    begin
      cb37 = nipue;
      cc37 = cc37 + netto;
      cc37 = cast(cc37 as integer);
    end
    else if (ile = 38) then
    begin
      cb38 = nipue;
      cc38 = cc38 + netto;
      cc38 = cast(cc38 as integer);
    end
    else if (ile = 39) then
    begin
      cb39 = nipue;
      cc39 = cc39 + netto;
      cc39 = cast(cc39 as integer);
    end
    else if (ile = 40) then
    begin
      cb40 = nipue;
      cc40 = cc40 + netto;
      cc40 = cast(cc40 as integer);
    end
    else if (ile = 41) then
    begin
      cb41 = nipue;
      cc41 = cc41 + netto;
      cc41 = cast(cc41 as integer);
    end
    else if (ile = 42) then
    begin
      cb42 = nipue;
      cc42 = cc42 + netto;
      cc42 = cast(cc42 as integer);
    end
    else if (ile = 43) then
    begin
      cb43 = nipue;
      cc43 = cc43 + netto;
      cc43 = cast(cc43 as integer);
    end
    else if (ile = 44) then
    begin
      cb44 = nipue;
      cc44 = cc44 + netto;
      cc44 = cast(cc44 as integer);
    end
    else if (ile = 45) then
    begin
      cb45 = nipue;
      cc45 = cc45 + netto;
      cc45 = cast(cc45 as integer);
    end
    else if (ile = 46) then
    begin
      cb46 = nipue;
      cc46 = cc46 + netto;
      cc46 = cast(cc46 as integer);
    end
    else if (ile = 47) then
    begin
      cb47 = nipue;
      cc47 = cc47 + netto;
      cc47 = cast(cc47 as integer);
    end
    else if (ile = 48) then
    begin
      cb48 = nipue;
      cc48 = cc48 + netto;
      cc48 = cast(cc48 as integer);
    end
    else if (ile = 49) then
    begin
      cb49 = nipue;
      cc49 = cc49 + netto;
      cc49 = cast(cc49 as integer);
    end
    else if (ile = 50) then
    begin
      cb50 = nipue;
      cc50 = cc50 + netto;
      cc50 = cast(cc50 as integer);
    end
    else if (ile = 51) then
    begin
      cb51 = nipue;
      cc51 = cc51 + netto;
      cc51 = cast(cc51 as integer);
    end
    else if (ile = 52) then
    begin
      cb52 = nipue;
      cc52 = cc52 + netto;
      cc52 = cast(cc52 as integer);
    end
    else if (ile = 53) then
    begin
      cb53 = nipue;
      cc53 = cc53 + netto;
      cc53 = cast(cc53 as integer);
    end
    else if (ile = 54) then
    begin
      cb54 = nipue;
      cc54 = cc54 + netto;
      cc54 = cast(cc54 as integer);
    end
    else if (ile = 55) then
    begin
      cb55 = nipue;
      cc55 = cc55 + netto;
      cc55 = cast(cc55 as integer);
    end
    else if (ile = 56) then
    begin
      cb56 = nipue;
      cc56 = cc56 + netto;
      cc56 = cast(cc56 as integer);
    end
    else if (ile = 57) then
    begin
      cb57 = nipue;
      cc57 = cc57 + netto;
      cc57 = cast(cc57 as integer);
    end
    else if (ile = 58) then
    begin
      cb58 = nipue;
      cc58 = cc58 + netto;
      cc58 = cast(cc58 as integer);
    end
    else if (ile = 59) then
    begin
      cb59 = nipue;
      cc59 = cc59 + netto;
      cc59 = cast(cc59 as integer);
    end
    ile = ile + 1;
  end

  ile = 1;
  nipuetmp = null;

  sql ='select D.nip, sum(B.sumnetv)
        from bkdocs B
        join vatregs V on (V.symbol = B.vatreg and V.company = B.company)
        left join dostawcy D on (D.ref = B.dictpos)
          where v.vtype = 5
          and B.status > 1';
  if (rodzaj = 1) then sql = sql||' and b.vatperiod ='''||:period||'''';
  else sql = sql||' and substring(b.vatperiod from 1 for 4) = '''||:yearid||'''
          and substring(b.vatperiod from 5 for 2) >= '''||:odokresu||'''
          and substring(b.vatperiod from 5 for 2) <= '''||:dookresu||'''';

  sql = sql||'  and B.company = '||:company||'
          group by D.nip';

  for execute statement sql
        into :nipue, :netto
  do begin
    if (nipuetmp is null) then
      nipuetmp = nipue;
    else if (nipuetmp = nipue) then
      ile = ile -1;

    if (ile = 1) then
    begin
      db1 = nipue;
      dc1 = dc1 + netto;
      dc1 = cast(dc1 as integer);
    end
    else if (ile = 2) then
    begin
      db2 = nipue;
      dc2 = dc2 + netto;
      dc2 = cast(dc2 as integer);
    end
    else if (ile = 3) then
    begin
      db3 = nipue;
      dc3 = dc3 + netto;
      dc3 = cast(dc3 as integer);
    end
    else if (ile = 4) then
    begin
      db4 = nipue;
      dc4 = dc4 + netto;
      dc4 = cast(dc4 as integer);
    end
    else if (ile = 5) then
    begin
      db5 = nipue;
      dc5 = dc5 + netto;
      dc5 = cast(dc5 as integer);
    end
    else if (ile = 6) then
    begin
      db6 = nipue;
      dc6 = dc6 + netto;
      dc6 = cast(dc6 as integer);
    end
    else if (ile = 7) then
    begin
      db7 = nipue;
      dc7 = dc7 + netto;
      dc7 = cast(dc7 as integer);
    end
    else if (ile = 8) then
    begin
      db8 = nipue;
      dc8 = dc8 + netto;
      dc8 = cast(dc8 as integer);
    end
    else if (ile = 9) then
    begin
      db9 = nipue;
      dc9 = dc9 + netto;
      dc9 = cast(dc9 as integer);
    end
    else if (ile = 10) then
    begin
      db10 = nipue;
      dc10 = dc10 + netto;
      dc10 = cast(dc10 as integer);
    end
    else if (ile = 11) then
    begin
      db11 = nipue;
      dc11 = dc11 + netto;
      dc11 = cast(dc11 as integer);
    end
    else if (ile = 12) then
    begin
      db12 = nipue;
      dc12 = dc12 + netto;
      dc12 = cast(dc12 as integer);
    end
    else if (ile = 13) then
    begin
      db13 = nipue;
      dc13 = dc13 + netto;
      dc13 = cast(dc13 as integer);
    end
    else if (ile = 14) then
    begin
      db14 = nipue;
      dc14 = dc14 + netto;
      dc14 = cast(dc14 as integer);
    end
    else if (ile = 15) then
    begin
      db15 = nipue;
      dc15 = dc15 + netto;
      dc15 = cast(dc15 as integer);
    end
    else if (ile = 16) then
    begin
      db16 = nipue;
      dc16 = dc16 + netto;
      dc16 = cast(dc16 as integer);
    end
    else if (ile = 17) then
    begin
      db17 = nipue;
      dc17 = dc17 + netto;
      dc17 = cast(dc17 as integer);
    end
    else if (ile = 18) then
    begin
      db18 = nipue;
      dc18 = dc18 + netto;
      dc18 = cast(dc18 as integer);
    end
    else if (ile = 19) then
    begin
      db19 = nipue;
      dc19 = dc19 + netto;
      dc19 = cast(dc19 as integer);
    end
    else if (ile = 20) then
    begin
      db20 = nipue;
      dc20 = dc20 + netto;
      dc20 = cast(dc20 as integer);
    end
    else if (ile = 21) then
    begin
      db21 = nipue;
      dc21 = dc21 + netto;
      dc21 = cast(dc21 as integer);
    end
    else if (ile = 22) then
    begin
      db22 = nipue;
      dc22 = dc22 + netto;
      dc22 = cast(dc22 as integer);
    end
    else if (ile = 23) then
    begin
      db23 = nipue;
      dc23 = dc23 + netto;
      dc23 = cast(dc23 as integer);
    end
    else if (ile = 24) then
    begin
      db24 = nipue;
      dc24 = dc24 + netto;
      dc24 = cast(dc24 as integer);
    end

    else if (ile = 25) then
    begin
      db25 = nipue;
      dc25 = dc25 + netto;
      dc25 = cast(dc25 as integer);
    end
    else if (ile = 26) then
    begin
      db26 = nipue;
      dc26 = dc26 + netto;
      dc26 = cast(dc26 as integer);
    end
    else if (ile = 27) then
    begin
      db27 = nipue;
      dc27 = dc27 + netto;
      dc27 = cast(dc27 as integer);
    end
    else if (ile = 28) then
    begin
      db28 = nipue;
      dc28 = dc28 + netto;
      dc28 = cast(dc28 as integer);
    end
    else if (ile = 29) then
    begin
      db29 = nipue;
      dc29 = dc29 + netto;
      dc29 = cast(dc29 as integer);
    end
    else if (ile = 30) then
    begin
      db30 = nipue;
      dc30 = dc30 + netto;
      dc30 = cast(dc30 as integer);
    end
    else if (ile = 31) then
    begin
      db31 = nipue;
      dc31 = dc31 + netto;
      dc31 = cast(dc31 as integer);
    end
    else if (ile = 32) then
    begin
      db32 = nipue;
      dc32 = dc32 + netto;
      dc32 = cast(dc32 as integer);
    end
    else if (ile = 33) then
    begin
      db33 = nipue;
      dc33 = dc33 + netto;
      dc33 = cast(dc33 as integer);
    end
    else if (ile = 34) then
    begin
      db34 = nipue;
      dc34 = dc34 + netto;
      dc34 = cast(dc34 as integer);
    end
    else if (ile = 35) then
    begin
      db35 = nipue;
      dc35 = dc35 + netto;
      dc35 = cast(dc35 as integer);
    end
    else if (ile = 36) then
    begin
      db36 = nipue;
      dc36 = dc36 + netto;
      dc36 = cast(dc36 as integer);
    end
    else if (ile = 37) then
    begin
      db37 = nipue;
      dc37 = dc37 + netto;
      dc37 = cast(dc37 as integer);
    end
    else if (ile = 38) then
    begin
      db38 = nipue;
      dc38 = dc38 + netto;
      dc38 = cast(dc38 as integer);
    end
    else if (ile = 39) then
    begin
      db39 = nipue;
      dc39 = dc39 + netto;
      dc39 = cast(dc39 as integer);
    end
    else if (ile = 40) then
    begin
      db40 = nipue;
      dc40 = dc40 + netto;
      dc40 = cast(dc40 as integer);
    end
    else if (ile = 41) then
    begin
      db41 = nipue;
      dc41 = dc41 + netto;
      dc41 = cast(dc41 as integer);
    end
    else if (ile = 42) then
    begin
      db42 = nipue;
      dc42 = dc42 + netto;
      dc42 = cast(dc42 as integer);
    end
    else if (ile = 43) then
    begin
      db43 = nipue;
      dc43 = dc43 + netto;
      dc43 = cast(dc43 as integer);
    end
    else if (ile = 44) then
    begin
      db44 = nipue;
      dc44 = dc44 + netto;
      dc44 = cast(dc44 as integer);
    end
    else if (ile = 45) then
    begin
      db45 = nipue;
      dc45 = dc45 + netto;
      dc45 = cast(dc45 as integer);
    end
    else if (ile = 46) then
    begin
      db46 = nipue;
      dc46 = dc46 + netto;
      dc46 = cast(dc46 as integer);
    end
    else if (ile = 47) then
    begin
      db47 = nipue;
      dc47 = dc47 + netto;
      dc47 = cast(dc47 as integer);
    end
    else if (ile = 48) then
    begin
      db48 = nipue;
      dc48 = dc48 + netto;
      dc48 = cast(dc48 as integer);
    end
    else if (ile = 49) then
    begin
      db49 = nipue;
      dc49 = dc49 + netto;
      dc49 = cast(dc49 as integer);
    end
    else if (ile = 50) then
    begin
      db50 = nipue;
      dc50 = dc50 + netto;
      dc50 = cast(dc50 as integer);
    end
    else if (ile = 51) then
    begin
      db51 = nipue;
      dc51 = dc51 + netto;
      dc51 = cast(dc51 as integer);
    end
    else if (ile = 52) then
    begin
      db52 = nipue;
      dc52 = dc52 + netto;
      dc52 = cast(dc52 as integer);
    end
    else if (ile = 53) then
    begin
      db53 = nipue;
      dc53 = dc53 + netto;
      dc53 = cast(dc53 as integer);
    end
    else if (ile = 54) then
    begin
      db54 = nipue;
      dc54 = dc54 + netto;
      dc54 = cast(dc54 as integer);
    end
    else if (ile = 55) then
    begin
      db55 = nipue;
      dc55 = dc55 + netto;
      dc55 = cast(dc55 as integer);
    end
    else if (ile = 56) then
    begin
      db56 = nipue;
      dc56 = dc56 + netto;
      dc56 = cast(dc56 as integer);
    end
    else if (ile = 57) then
    begin
      db57 = nipue;
      dc57 = dc57 + netto;
      dc57 = cast(dc57 as integer);
    end
    else if (ile = 58) then
    begin
      db58 = nipue;
      dc58 = dc58 + netto;
      dc58 = cast(dc58 as integer);
    end
    else if (ile = 59) then
    begin
      db59 = nipue;
      dc59 = dc59 + netto;
      dc59 = cast(dc59 as integer);
    end


    ile = ile + 1;
  end

  ile = 1;
  nipuetmp = null;
  sql ='select K.nip, sum(BV.netv)
        from bkdocs B
        join vatregs V on (V.symbol = B.vatreg and V.company = B.company)
        left join klienci K on (K.ref = B.dictpos)
        join bkvatpos BV on (BV.bkdoc = B.ref)
          where v.vtype = 1 and BV.vatgr = ''NP''
          and B.status > 0';
  if (rodzaj = 1) then sql = sql||' and b.vatperiod ='''||:period||'''';
  else sql = sql||' and substring(b.vatperiod from 1 for 4) = '''||:yearid||'''
          and substring(b.vatperiod from 5 for 2) >= '''||:odokresu||'''
          and substring(b.vatperiod from 5 for 2) <= '''||:dookresu||'''';

  sql = sql||'  and B.company = '||:company||'
          group by K.nip';

  for execute statement sql
        into :nipue, :netto
  do begin
    if (nipuetmp is null) then
      nipuetmp = nipue;
    else if (nipuetmp = nipue) then
      ile = ile -1;

    if (ile = 1) then
    begin
      eb1 = nipue;
      ec1 = ec1 + netto;
      ec1 = cast(ec1 as integer);
    end
    else if (ile = 2) then
    begin
      eb2 = nipue;
      ec2 = ec2 + netto;
      ec2 = cast(ec2 as integer);
    end
    else if (ile = 3) then
    begin
      eb3 = nipue;
      ec3 = ec3 + netto;
      ec3 = cast(ec3 as integer);
    end
    else if (ile = 4) then
    begin
      eb4 = nipue;
      ec4 = ec4 + netto;
      ec4 = cast(ec4 as integer);
    end
    else if (ile = 5) then
    begin
      eb5 = nipue;
      ec5 = ec5 + netto;
      ec5 = cast(ec5 as integer);
    end
    else if (ile = 6) then
    begin
      eb6 = nipue;
      ec6 = ec6 + netto;
      ec6 = cast(ec6 as integer);
    end
    else if (ile = 7) then
    begin
      eb7 = nipue;
      ec7 = ec7 + netto;
      ec7 = cast(ec7 as integer);
    end
    else if (ile = 8) then
    begin
      eb8 = nipue;
      ec8 = ec8 + netto;
      ec8 = cast(ec8 as integer);
    end
    else if (ile = 9) then
    begin
      eb9 = nipue;
      ec9 = ec9 + netto;
      ec9 = cast(ec9 as integer);
    end
    else if (ile = 10) then
    begin
      eb10 = nipue;
      ec10 = ec10 + netto;
      ec10 = cast(ec10 as integer);
    end
    else if (ile = 11) then
    begin
      eb11 = nipue;
      ec11 = ec11 + netto;
      ec11 = cast(ec11 as integer);
    end
    else if (ile = 12) then
    begin
      eb12 = nipue;
      ec12 = ec12 + netto;
      ec12 = cast(ec12 as integer);
    end
    else if (ile = 13) then
    begin
      eb13 = nipue;
      ec13 = ec13 + netto;
      ec13 = cast(ec13 as integer);
    end
    else if (ile = 14) then
    begin
      eb14 = nipue;
      ec14 = ec14 + netto;
      ec14 = cast(ec14 as integer);
    end
    else if (ile = 15) then
    begin
      eb15 = nipue;
      ec15 = ec15 + netto;
      ec15 = cast(ec15 as integer);
    end
    else if (ile = 16) then
    begin
      eb16 = nipue;
      ec16 = ec16 + netto;
      ec16 = cast(ec16 as integer);
    end
    else if (ile = 17) then
    begin
      eb17 = nipue;
      ec17 = ec17 + netto;
      ec17 = cast(ec17 as integer);
    end
    else if (ile = 18) then
    begin
      eb18 = nipue;
      ec18 = ec18 + netto;
      ec18 = cast(ec18 as integer);
    end
    else if (ile = 19) then
    begin
      eb19 = nipue;
      ec19 = ec19 + netto;
      ec19 = cast(ec19 as integer);
    end
    else if (ile = 20) then
    begin
      eb20 = nipue;
      ec20 = ec20 + netto;
      ec20 = cast(ec20 as integer);
    end
    else if (ile = 21) then
    begin
      eb21 = nipue;
      ec21 = ec21 + netto;
      ec21 = cast(ec21 as integer);
    end
    else if (ile = 22) then
    begin
      eb22 = nipue;
      ec22 = ec22 + netto;
      ec22 = cast(ec22 as integer);
    end
    else if (ile = 23) then
    begin
      eb23 = nipue;
      ec23 = ec23 + netto;
      ec23 = cast(ec23 as integer);
    end
    else if (ile = 24) then
    begin
      eb24 = nipue;
      ec24 = ec24 + netto;
      ec24 = cast(ec24 as integer);
    end
    else if (ile = 25) then
    begin
      eb25 = nipue;
      ec25 = ec25 + netto;
      ec25 = cast(ec25 as integer);
    end
    else if (ile = 26) then
    begin
      eb26 = nipue;
      ec26 = ec26 + netto;
      ec26 = cast(ec26 as integer);
    end
    else if (ile = 27) then
    begin
      eb27 = nipue;
      ec27 = ec27 + netto;
      ec27 = cast(ec27 as integer);
    end
    else if (ile = 28) then
    begin
      eb28 = nipue;
      ec28 = ec28 + netto;
      ec28 = cast(ec28 as integer);
    end
    else if (ile = 29) then
    begin
      eb29 = nipue;
      ec29 = ec29 + netto;
      ec29 = cast(ec29 as integer);
    end
    else if (ile = 30) then
    begin
      eb30 = nipue;
      ec30 = ec30 + netto;
      ec30 = cast(ec30 as integer);
    end
    else if (ile = 31) then
    begin
      eb31 = nipue;
      ec31 = ec31 + netto;
      ec31 = cast(ec31 as integer);
    end
    else if (ile = 32) then
    begin
      eb32 = nipue;
      ec32 = ec32 + netto;
      ec32 = cast(ec32 as integer);
    end
    else if (ile = 33) then
    begin
      eb33 = nipue;
      ec33 = ec33 + netto;
      ec33 = cast(ec33 as integer);
    end
    else if (ile = 34) then
    begin
      eb34 = nipue;
      ec34 = ec34 + netto;
      ec34 = cast(ec34 as integer);
    end
    else if (ile = 35) then
    begin
      eb35 = nipue;
      ec35 = ec35 + netto;
      ec35 = cast(ec35 as integer);
    end
    else if (ile = 36) then
    begin
      eb36 = nipue;
      ec36 = ec36 + netto;
      ec36 = cast(ec36 as integer);
    end
    else if (ile = 37) then
    begin
      eb37 = nipue;
      ec37 = ec37 + netto;
      ec37 = cast(ec37 as integer);
    end
    else if (ile = 38) then
    begin
      eb38 = nipue;
      ec38 = ec38 + netto;
      ec38 = cast(ec38 as integer);
    end
    else if (ile = 39) then
    begin
      eb39 = nipue;
      ec39 = ec39 + netto;
      ec39 = cast(ec39 as integer);
    end
    else if (ile = 40) then
    begin
      eb40 = nipue;
      ec40 = ec40 + netto;
      ec40 = cast(ec40 as integer);
    end
    else if (ile = 41) then
    begin
      eb41 = nipue;
      ec41 = ec41 + netto;
      ec41 = cast(ec41 as integer);
    end
    else if (ile = 42) then
    begin
      eb42 = nipue;
      ec42 = ec42 + netto;
      ec42 = cast(ec42 as integer);
    end
    else if (ile = 43) then
    begin
      eb43 = nipue;
      ec43 = ec43 + netto;
      ec43 = cast(ec43 as integer);
    end
    else if (ile = 44) then
    begin
      eb44 = nipue;
      ec44 = ec44 + netto;
      ec44 = cast(ec44 as integer);
    end
    else if (ile = 45) then
    begin
      eb45 = nipue;
      ec45 = dc45 + netto;
      ec45 = cast(ec45 as integer);
    end
    else if (ile = 46) then
    begin
      eb46 = nipue;
      ec46 = ec46 + netto;
      ec46 = cast(ec46 as integer);
    end
    else if (ile = 47) then
    begin
      eb47 = nipue;
      ec47 = ec47 + netto;
      ec47 = cast(ec47 as integer);
    end
    else if (ile = 48) then
    begin
      eb48 = nipue;
      ec48 = ec48 + netto;
      ec48 = cast(ec48 as integer);
    end
    else if (ile = 49) then
    begin
      eb49 = nipue;
      ec49 = ec49 + netto;
      ec49 = cast(ec49 as integer);
    end
    else if (ile = 50) then
    begin
      eb50 = nipue;
      ec50 = ec50 + netto;
      ec50 = cast(ec50 as integer);
    end
    else if (ile = 51) then
    begin
      eb51 = nipue;
      ec51 = ec51 + netto;
      ec51 = cast(ec51 as integer);
    end
    else if (ile = 52) then
    begin
      eb52 = nipue;
      ec52 = ec52 + netto;
      ec52 = cast(ec52 as integer);
    end
    else if (ile = 53) then
    begin
      eb53 = nipue;
      ec53 = ec53 + netto;
      ec53 = cast(ec53 as integer);
    end
    else if (ile = 54) then
    begin
      eb54 = nipue;
      ec54 = ec54 + netto;
      ec54 = cast(ec54 as integer);
    end
    else if (ile = 55) then
    begin
      eb55 = nipue;
      ec55 = ec55 + netto;
      ec55 = cast(ec55 as integer);
    end
    else if (ile = 56) then
    begin
      eb56 = nipue;
      ec56 = ec56 + netto;
      ec56 = cast(ec56 as integer);
    end
    else if (ile = 57) then
    begin
      eb57 = nipue;
      ec57 = ec57 + netto;
      ec57 = cast(ec57 as integer);
    end
    else if (ile = 58) then
    begin
      eb58 = nipue;
      ec58 = ec58 + netto;
      ec58 = cast(ec58 as integer);
    end

    ile = ile + 1;
  end

  suspend;
end^
SET TERM ; ^
