--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_KOREKTACENNEWDOK(
      FAK integer,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      TYPWZ varchar(3) CHARACTER SET UTF8                           ,
      TYPZZ varchar(3) CHARACTER SET UTF8                           ,
      TYPPZ varchar(3) CHARACTER SET UTF8                           ,
      TYPZPZ varchar(3) CHARACTER SET UTF8                           ,
      TYPWZO varchar(3) CHARACTER SET UTF8                           ,
      TYPZZO varchar(3) CHARACTER SET UTF8                           ,
      TYPPZO varchar(3) CHARACTER SET UTF8                           ,
      TYPZPZO varchar(3) CHARACTER SET UTF8                           ,
      DATA timestamp,
      AKCEPT smallint)
  returns (
      STATUS integer,
      DOKSYM varchar(20) CHARACTER SET UTF8                           )
   as
declare variable dokmag integer;
declare variable numer integer;
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable cena numeric(14,2);
declare variable wydania integer;
declare variable typdok varchar(3);
declare variable ilosc numeric(14,4);
declare variable ilosco numeric(14,4);
declare variable ilstan numeric(14,4);
declare variable dostawa integer;
declare variable symbol varchar(30);
declare variable korekta integer;
declare variable zewn integer;
declare variable koryg integer;
declare variable zrodlo smallint;
declare variable akceptacja integer;
declare variable zakup integer;
declare variable typmag char(1);
declare variable dostawca integer;
declare variable iddostawcy varchar(20);
declare variable ilwz numeric(14,4);
declare variable ilwzo numeric(14,4);
declare variable cenawz numeric(14,2);
declare variable dostwz integer;
declare variable ilzz numeric(14,4);
declare variable ilzzo numeric(14,4);
declare variable cenazz numeric(14,2);
declare variable dostzz integer;
declare variable ilpz numeric(14,4);
declare variable ilpzo numeric(14,4);
declare variable cenapz numeric(14,2);
declare variable dostpz integer;
declare variable ilzpz numeric(14,4);
declare variable ilzpzo numeric(14,4);
declare variable cenazpz numeric(14,2);
declare variable dostzpz integer;
declare variable ilpzns numeric(14,4);
declare variable ilpznso numeric(14,4);
declare variable cenapzns numeric(14,2);
declare variable dostpzns integer;
declare variable ilzpzns numeric(14,4);
declare variable ilzpznso numeric(14,4);
declare variable cenazpzns numeric(14,2);
declare variable dostzpzns integer;
declare variable magstan numeric(14,4);
declare variable pilosc numeric(14,4);
declare variable pilosco numeric(14,4);
declare variable pcenanet numeric(14,2);
declare variable cenanet numeric(14,2);
declare variable magwartosc numeric(14,2);
declare variable dokwz integer;
declare variable dokzz integer;
declare variable dokpz integer;
declare variable dokzpz integer;
declare variable dokwzo integer;
declare variable dokzzo integer;
declare variable dokpzo integer;
declare variable dokzpzo integer;
declare variable ldok integer;
declare variable serialized smallint;
declare variable dokpzns integer;
declare variable dokzpzns integer;
declare variable dokpznso integer;
declare variable dokzpznso integer;
declare variable wzser smallint;
declare variable zzser smallint;
declare variable wzbreak smallint;
declare variable pozref integer;
declare variable dokpozref integer;
declare variable serialil numeric(14,4);
declare variable toserialil numeric(14,4);
declare variable autoserrozpisz smallint;
declare variable i numeric(14,4);
declare variable wersjaref integer;
declare variable dokmagblok smallint;
declare variable dostnotser varchar(255);
declare variable notser smallint;
declare variable jedn integer;
declare variable przelicz numeric(14,4);
declare variable fromzam integer;
declare variable opk smallint;
declare variable oddzial varchar(20);
declare variable aktuoddzial varchar(20);
begin
  /* sprawdzenie typu dok. mag. z typem dok. sprzedazy */
  select TYPFAK.KOREKTA, NAGFAK.AKCEPTACJA, NAGFAK.REFDOKM, NAGFAK.ZAKUP, NAGFAK.DOSTAWCA from TYPFAK join NAGFAK on ( typfak.SYMBOL = NAGFAK.TYP) where NAGFAK.REF =:fak into :korekta, :akceptacja, :dokmag, :zakup, :dostawca;
  execute procedure GETCONFIG('DOSTNOTSER') returning_values :dostnotser;
  select ODDZIAL from DEFMAGAZ where symbol = :magazyn into :oddzial;
  execute procedure GETCONFIG('AKTUODDZIAL') returning_values :aktuoddzial;
  if(:oddzial <> :aktuoddzial)then exception NAGFAK_DOKMAG_WRONGODDZIAL;
  if(:dostnotser <> '1') then notser = 0;
  else notser = 1;
  if(:akceptacja is null) then akceptacja = 0;
  if(:akceptacja = 0) then exception FAK_DOK_CREATE_NOACK;
  if(:akcept is null) then akcept = 0;
  if(:korekta is null) then korekta = 0;
  if(:akcept > 1) then zrodlo = 3;
  else zrodlo = 2;
  select TYP from DEFMAGAZ where SYMBOL=:MAGAZYN into :typmag;
  for select POZFAK.KTM, POZFAK.WERSJA, POZFAK.NUMER,
    POZFAK.ILOSCm,POZFAK.ILOSCm, POZFAK.ILOSCO, POZFAK.ILOSCO, POZFAK.JEDNO,
    POZFAK.CENANET, DOKUMPOZ.CENA, POZFAK.SERIALIZED, POZFAk.REF, POZFAK.WERSJAREF, POZFAK.FROMZAM, POZFAK.OPK
    from POZFAK join TOWARY on ( TOWARY.KTM = POZFAK.KTM ) join DOKUMPOZ on (DOKUMPOZ.REF = POZFAK.FROMDOKUMPOZ)
    where POZFAK.DOKUMENT = :FAK and TOWARY.USLUGA <>1 and MAGAZYN = :magazyn
    order by POZFAK.NUMER
    into :ktm, :wersja, :numer, :ilosc, :pilosc, :ilosco, :pilosco, :jedn,
         :cenanet, :pcenanet, :serialized, :pozref, :wersjaref, :fromzam, :opk
  do begin
    if(:opk is null) then opk = 0;
    if(:serialized = 1) then begin
      select TOWARY.serautowyd from TOWARY where KTM = :ktm into :autoserrozpisz;
    end
    /*okrelenie typu operacji*/
    przelicz = null;
    select PRZELICZ from TOWJEDN where REF=:jedn into :przelicz;
    if(:przelicz is null)then przelicz = 0;
    if(:przelicz = 0) then przelicz = 1;
    ilwz = 0; ilwzo = 0;cenawz = null;dostwz = null;
    ilpz = 0; ilpzo = 0; cenapz = null;dostpz = null;
    ilzz = 0; ilzzo = 0; cenazz = null;dostzz = null;
    ilzpz = 0; ilzpzo = 0; cenazpz = null; dostzpz = null;
    ilpzns = 0; ilpznso = 0; cenapzns = null;dostpzns = null;
    ilzpzns = 0; ilzpznso = 0; cenazpzns = null; dostzpzns = null;
    wzser = :notser;
    zzser =:notser;
    wzbreak = 0;
    if(:zakup = 1)then begin
      /* faktua zakupu lub korekta zakupu */
      wzser = 1;
      zzser = 1;
      wzbreak = 1;
      if(:dostawa is null and :typmag = 'P') then execute procedure NAGFAK_CREATE_DOKMAG_DOSTAWA(:fak, :data,:magazyn) returning_values :dostawa;
      if((:pcenanet = :cenanet) or (:typmag = 'E')) then begin
        /* operacja tylko ilosciowa*/
        if(:ilosc > :pilosc) then begin /*przyjecie*/
           ilpz = :ilosc - :pilosc;
           ilpzo = :ilosco - :pilosco;
           cenapz = :cenanet;
           dostpz = :dostawa;
        end else begin /* korekta ilosciowa - zwrot do dostawcy */
          ilzpz = :pilosc - :ilosc;
          ilzpzo = :pilosco - :ilosco;
          cenazpz = :pcenanet;
          dostzpz = :dostawa;
           /*dostawa i cena powinny zostac okreslona poprzez znalezienie dokumentu typu PZ w grupie*/
        end
      end else begin
        /*różniece cen - zwort sprzedazy, zwot dostawy, nowa dostawa, sprzedaz*/
        if(:pilosc > 0) then begin
          /*wycofanie sprzedazy*/
          execute procedure MAG_STAN(:magazyn, :ktm,:wersja,:pcenanet,:dostawa,NULL) returning_values :magstan, :magwartosc;
          if(:magstan is null) then magstan = 0;
          if(:magstan < :pilosc) then begin
            ilzz = :pilosc - :magstan;
            ilzzo = :ilzz / :przelicz;
            cenazz = :pcenanet;
            dostzz = :dostawa;
            ilwz = :ilzz;
            ilwzo = :ilzzo;
            cenawz = :cenanet;
            dostwz = :dostawa;
          end
          if(:serialized = 1) then begin
            ilzpzns = :pilosc;
            ilzpznso = :pilosco;
            cenazpzns = :pcenanet;
            dostzpzns = :dostawa;
          end else begin
            ilzpz = :pilosc;
            ilzpzo = :pilosco;
          end
          cenazpz = :pcenanet;
          dostzpz = :dostawa;
        end
        if(:serialized = 1) then begin
          ilpzns = :ilosc - :ilpz;
          ilpznso = :ilosco - :ilpzo;
          cenapzns = :cenanet;
          dostpzns = :dostawa;
        end else begin
          ilpz = :ilosc;
          ilpzo = :ilosco;
        end
        cenapz = :cenanet;
        dostpz = :dostawa;
      end
    end else begin
      /*faktua sprzedazy lub korekta sprzedazy*/
      if(:ilosc > :pilosc)then begin
        ilwz = :ilosc - :pilosc;/* sprzedaz*/
        ilwzo = :ilosco - :pilosco;
      end else begin
        ilzz = :pilosc - :ilosc;/*zwrot sprzedazy - ceny i dostawy zdjete z grupy dokumentow*/
        ilzzo = :pilosco - :ilosco;
      end
    end
    if(:ilwz > 0) then begin
      /*sprawdzenie, czy jest wystawiony dokument WZ*/
      if(:opk = 0) then ldok = :dokwz;
      else ldok = :dokwzo;
      if(:ldok is null) then begin
        koryg = null;zewn = null;wydania = null;
        if(:opk = 0) then typdok = :typwz;
        else typdok = :typwzo;
        select WYDANIA, ZEWN, KORYG from DEFDOKUM where SYMBOL = :TYPDOK into :wydania, :zewn, :koryg;
        if(:koryg is null) then koryg = 0;if(:zewn is null) then zewn = 0; if(:wydania is null) then wydania = 0;
        if(((:zewn <> 1) or (:wydania <> 1) or (:koryg <> 0))) then
          exception FAK_DOK_CREATE_WRONGTYPWZ;
        select REF from DOKUMNAG where FAKTURA = :FAK and MAGAZYN = :MAGAZYN and TYP = :typdok and BLOKADA = 2 into :ldok;
        if(:ldok is null) then begin
          execute procedure GEN_REF('DOKUMNAG') returning_values :ldok;
          insert into DOKUMNAG(REF, MAGAZYN, TYP, DATA,  KLIENT, DOSTAWCA,FAKTURA, zrodlo, DOSTAWA, NOTSERIALIZED,MAGBREAK)
          select :ldok, :magazyn, :typdok, :data,  KLIENT, DOSTAWCA,ref, :zrodlo, :dostwz, :wzser, :wzbreak
             from NAGFAK where REF=:FAK;
        end
        if(:opk = 0) then dokwz = :ldok;
        else dokwzo = :ldok;
      end
      execute procedure GEN_REF('DOKUMPOZ') returning_values :dokpozref;
      insert into DOKUMPOZ(REF,DOKUMENT, KTM, WERSJA, ILOSC,ILOSCO, JEDNO, CENA, FROMZAM)
          values(:dokpozref, :ldok, :ktm, :wersja, :ilwz,:ilwzo, :jedn, :cenawz, :FROMZAM);
      if(:serialized = 1 and :wzser = 0) then begin
        /*sprawdzenie ilosci serlizowanych zgodnych z pozycj*/
        serialil = null;
        select sum(ilosc) from DOKUMSER where REFPOZ =:pozref and TYP='F' into :serialil;
        if(:serialil is null) then serialil = 0;
        toserialil = :ilosc - :pilosc;
        if(:toserialil < 0) then toserialil = - :toserialil;
        if(:toserialil > :serialil and :autoserrozpisz = 1) then begin
          i = :toserialil - :serialil;
          execute procedure SERNR_AUTOROZPISZ(:pozref, 'F',:magazyn,:wersjaref,:i);
          serialil = null;
          select sum(ilosc) from DOKUMSER where REFPOZ =:pozref and TYP='F' into :serialil;
          if(:serialil is null) then serialil = 0;
        end
        if(:toserialil <> :serialil) then exception FAK_DOK_CREATE_ILSERWR 'Serializacja dla tow.: '||:ktm||' niekompletna';
        -- SERFIX1 --
        insert into DOKUMSER(REFPOZ,TYP,NUMER,ODSERIAL,DOSERIAL,ODSERIALNR,DOSERIALNR,ILOSC,ORD,ORGDOKUMSER)
          select :dokpozref, 'M',NUMER,ODSERIAL, DOSERIAL,ODSERIALNR,DOSERIALNR,ILOSC,1 ,coalesce(orgdokumser, ref)
          from DOKUMSER where REFPOZ = :pozref and TYP = 'F';
        -- SERFIX1 --
      end
    end
    if(:ilzz > 0) then begin
      /*sprawdzenie, czy jest wystawiony dokument ZZ*/
      if(:opk = 0) then ldok = :dokzz;
      else ldok = :dokzzo;
      if(:ldok is null) then begin
        koryg = null;zewn = null;wydania = null;
        if(:opk = 0) then typdok = :typzz;
        else typdok = :typzzo;
        select WYDANIA, ZEWN, KORYG from DEFDOKUM where SYMBOL = :TYPDOK into :wydania, :zewn, :koryg;
        if(:koryg is null) then koryg = 0;if(:zewn is null) then zewn = 0; if(:wydania is null) then wydania = 0;
        if(((:zewn <> 1) or (:wydania <> 0) or (:koryg <> 1))) then
          exception FAK_DOK_CREATE_WRONGTYPZZ;
        select REF from DOKUMNAG where FAKTURA = :FAK and MAGAZYN = :MAGAZYN and TYP = :TYPDOK and BLOKADA = 2 into :ldok;
        if(:ldok is null) then begin
          execute procedure GEN_REF('DOKUMNAG') returning_values :ldok;
          insert into DOKUMNAG(REF, MAGAZYN, TYP, DATA,  KLIENT, DOSTAWCA,FAKTURA, zrodlo, DOSTAWA, NOTSERIALIZED)
          select :ldok, :magazyn, :TYPDOK, :data,  KLIENT, DOSTAWCA,ref, :zrodlo, :dostZZ, :zzser
             from NAGFAK where REF=:FAK;
        end
        if(:opk = 0) then dokzz = :ldok;
        else dokzzo = :ldok;
      end
      if((:cenazz is not null) or (:serialized = 1 and zzser = 0)) then begin
        execute procedure GEN_REF('DOKUMPOZ') returning_values :dokpozref;
        insert into DOKUMPOZ(REF,DOKUMENT, KTM, WERSJA, ILOSC, ILOSCO, JEDNO, CENA)
          values(:dokpozref, :ldok, :ktm, :wersja, :ilzz, :ilzzo, :jedn, :cenazz);
        if(:serialized = 1 and :zzser = 0) then begin
          /*sprawdzenie ilosci serlizowanych zgodnych z pozycj*/
          serialil = null;
          select sum(ilosc) from DOKUMSER where REFPOZ =:pozref and TYP='F' into :serialil;
          if(:serialil is null) then serialil = 0;
          toserialil = :ilosc - :pilosc;
          if(:toserialil < 0) then toserialil = - :toserialil;
          if(:toserialil <> :serialil) then exception FAK_DOK_CREATE_ILSERWR 'Serializacja dla tow.: '||:ktm||' niekompletna';
          insert into DOKUMSER(REFPOZ,TYP,NUMER,ODSERIAL,DOSERIAL,ODSERIALNR,DOSERIALNR,ILOSC,ORD)
            select :dokpozref, 'M',NUMER,ODSERIAL, DOSERIAL,ODSERIALNR,DOSERIALNR,ILOSC,1
            from DOKUMSER where REFPOZ = :pozref and TYP = 'F';
        end
      end else begin
        for select cena, dostawa, ilosc from NAGFAK_STAN_MAG(:fak, :ktm, :wersja) into :cenazz, :dostzz, :ilstan
        do begin
          if(:ilzz > 0) then begin
            if(:ilstan < 0 or (:ilstan is null)) then ilstan = 0;
            if(:ilstan > :ilzz) then ilstan = :ilzz;
            if(:ilstan > 0) then
              insert into DOKUMPOZ(DOKUMENT, KTM, WERSJA, ILOSC, JEDNO, CENA, DOSTAWA)
                 values( :ldok, :ktm, :wersja, :ilstan, :jedn, :cenazz, :dostzz);
            ilzz = :ilzz - :ilstan;
          end
          ilstan = 0;
        end
        if(:ilzz > 0) then exception FAK_DOK_CERETATE_ZZTOMUCH;
      end
    end
    if(:ilpz > 0) then begin
      /*sprawdzenie, czy jest wystawiony dokument PZ*/
      if(:opk = 0) then ldok = :dokpz;
      else ldok = :dokpzo;
      if(:ldok is null) then begin
        koryg = null;zewn = null;wydania = null;
        if(:opk = 0) then typdok = :typpz;
        else typdok = :typpzo;
        select WYDANIA, ZEWN, KORYG from DEFDOKUM where SYMBOL = :TYPDOK into :wydania, :zewn, :koryg;
        if(:koryg is null) then koryg = 0;if(:zewn is null) then zewn = 0; if(:wydania is null) then wydania = 0;
        if(((:zewn <> 1) or (:wydania <> 0) or (:koryg <> 0))) then
          exception FAK_DOK_CREATE_WRONGTYPPZ;
        select REF from DOKUMNAG where FAKTURA = :FAK and MAGAZYN = :MAGAZYN and TYP = :typdok and BLOKADA = 2 into :ldok;
        if(:ldok is null) then begin
          execute procedure GEN_REF('DOKUMNAG') returning_values :ldok;
          insert into DOKUMNAG(REF, MAGAZYN, TYP, DATA,  KLIENT, DOSTAWCA,FAKTURA, zrodlo, DOSTAWA, NOTSERIALIZED)
          select :ldok, :magazyn, :typdok, :data,  KLIENT, DOSTAWCA,ref, :zrodlo, :dostpz, :notser
             from NAGFAK where REF=:FAK;
        end
        if(:opk = 0) then dokpz = :ldok;
        else dokpzo = :ldok;
      end
      execute procedure GEN_REF('DOKUMPOZ') returning_values :dokpozref;
      insert into DOKUMPOZ(REF,DOKUMENT, KTM, WERSJA, ILOSC,  ILOSCO, JEDNO, CENA, FROMZAM)
          values( :dokpozref,:ldok, :ktm, :wersja, :ilpz, :ilpzo, :jedn, :cenapz, :fromzam);
      if(:serialized = 1 and :notser = 0) then begin
        /*sprawdzenie ilosci serlizowanych zgodnych z pozycj*/
        serialil = null;
        select sum(ilosc) from DOKUMSER where REFPOZ =:pozref and TYP='F' into :serialil;
        if(:serialil is null) then serialil = 0;
        toserialil = :ilosc - :pilosc;
        if(:toserialil < 0) then toserialil = - :toserialil;
        if(:toserialil <> :serialil) then exception FAK_DOK_CREATE_ILSERWR 'Serializacja dla tow.: '||:ktm||' niekompletna';
        insert into DOKUMSER(REFPOZ,TYP,NUMER,ODSERIAL,DOSERIAL,ODSERIALNR,DOSERIALNR,ILOSC,ORD)
          select :dokpozref, 'M',NUMER,ODSERIAL, DOSERIAL,ODSERIALNR,DOSERIALNR,ILOSC,1
          from DOKUMSER where REFPOZ = :pozref and TYP = 'F';
      end
    end
    if(:ilzpz > 0) then begin
      /*sprawdzenie, czy jest wystawiony dokument PZ*/
      if(:opk = 0) then ldok = dokzpz;
      else ldok = dokzpzo;
      if(:ldok is null) then begin
        koryg = null;zewn = null;wydania = null;
        if(:opk = 0) then typdok = :typzpz;
        else typdok = :typzpzo;
        select WYDANIA, ZEWN, KORYG from DEFDOKUM where SYMBOL = :typdok into :wydania, :zewn, :koryg;
        if(:koryg is null) then koryg = 0;if(:zewn is null) then zewn = 0; if(:wydania is null) then wydania = 0;
        if(((:zewn <> 1) or (:wydania <> 1) or (:koryg <> 1))) then
          exception FAK_DOK_CREATE_WRONGTYPZPZ;
        select REF from DOKUMNAG where FAKTURA = :FAK and MAGAZYN = :MAGAZYN and TYP = :typdok and BLOKADA = 2 into :ldok;
        if(:ldok is null) then begin
          execute procedure GEN_REF('DOKUMNAG') returning_values :ldok;
          insert into DOKUMNAG(REF, MAGAZYN, TYP, DATA,  KLIENT, DOSTAWCA,FAKTURA, zrodlo, DOSTAWA, NOTSERIALIZED)
          select :ldok, :magazyn, :typdok, :data,  KLIENT, DOSTAWCA,ref, :zrodlo, :dostzpz, :notser
             from NAGFAK where REF=:FAK;
        end
        if(:opk = 0) then dokzpz = :ldok;
        else dokzpzo = :ldok;
      end
      execute procedure GEN_REF('DOKUMPOZ') returning_values :dokpozref;
      insert into DOKUMPOZ(REF,DOKUMENT, KTM, WERSJA, ILOSC,  ILOSCO, JEDNO, CENA)
          values(:dokpozref, :ldok, :ktm, :wersja, :ilzpz, :ilzpzo, :jedn, :cenazpz);
      if(:serialized = 1 and :notser=0) then begin
        /*sprawdzenie ilosci serlizowanych zgodnych z pozycj*/
        serialil = null;
        select sum(ilosc) from DOKUMSER where REFPOZ =:pozref and TYP='F' into :serialil;
        if(:serialil is null) then serialil = 0;
        toserialil = :ilosc - :pilosc;
        if(:toserialil < 0) then toserialil = - :toserialil;
        if(:toserialil <> :serialil) then exception FAK_DOK_CREATE_ILSERWR 'Serializacja dla tow.: '||:ktm||' niekompletna';
        insert into DOKUMSER(REFPOZ,TYP,NUMER,ODSERIAL,DOSERIAL,ODSERIALNR,DOSERIALNR,ILOSC,ORD)
          select :dokpozref, 'M',NUMER,ODSERIAL, DOSERIAL,ODSERIALNR,DOSERIALNR,ILOSC,1
          from DOKUMSER where REFPOZ = :pozref and TYP = 'F';
      end
    end
    if(:ilpzns > 0) then begin
      /*sprawdzenie, czy jest wystawiony dokument PZ*/
      if(:opk = 0) then ldok = :dokzpzns;
      else ldok = :dokzpznso;
      if(:ldok is null) then begin
        koryg = null;zewn = null;wydania = null;
        if(:opk = 0) then typdok = :typpz;
        else typdok = :typpzo;
        select WYDANIA, ZEWN, KORYG from DEFDOKUM where SYMBOL = :typdok into :wydania, :zewn, :koryg;
        if(:koryg is null) then koryg = 0;if(:zewn is null) then zewn = 0; if(:wydania is null) then wydania = 0;
        if(((:zewn <> 1) or (:wydania <> 0) or (:koryg <> 0))) then
          exception FAK_DOK_CREATE_WRONGTYPPZ;
        select REF from DOKUMNAG where FAKTURA = :FAK and MAGAZYN = :MAGAZYN and TYP = :typdok and BLOKADA = 2 and NOTSERIALIZED = 1 into :ldok;
        if(:ldok is null) then begin
          execute procedure GEN_REF('DOKUMNAG') returning_values :ldok;
          insert into DOKUMNAG(REF, MAGAZYN, TYP, DATA,  KLIENT, DOSTAWCA,FAKTURA, zrodlo, DOSTAWA, NOTSERIALIZED)
          select :ldok, :magazyn, :typdok, :data,  KLIENT, DOSTAWCA,ref, :zrodlo, :dostpzns,1
             from NAGFAK where REF=:FAK;
        end
        if(:opk = 0) then dokpzns = :ldok;
        else dokpznso = :ldok;
      end
      insert into DOKUMPOZ(DOKUMENT, KTM, WERSJA, ILOSC,  ILOSCO, JEDNO, CENA)
          values( :ldok, :ktm, :wersja, :ilpzns, :ilpznso, :jedn, :cenapzns);
    end
    if(:ilzpzns > 0) then begin
      /*sprawdzenie, czy jest wystawiony dokument PZ*/
      if(:opk = 0) then ldok = :dokzpzns;
      else ldok = :dokzpznso;
      if(:ldok is null) then begin
        koryg = null;zewn = null;wydania = null;
        if(:opk = 0) then typdok = :typzpz;
        else typdok = :typzpzo;
        select WYDANIA, ZEWN, KORYG from DEFDOKUM where SYMBOL = :typdok into :wydania, :zewn, :koryg;
        if(:koryg is null) then koryg = 0;if(:zewn is null) then zewn = 0; if(:wydania is null) then wydania = 0;
        if(((:zewn <> 1) or (:wydania <> 1) or (:koryg <> 1))) then
          exception FAK_DOK_CREATE_WRONGTYPZPZ;
        select REF from DOKUMNAG where FAKTURA = :FAK and MAGAZYN = :MAGAZYN and TYP = :typdok and BLOKADA = 2 and NOTSERIALIZED = 1 into :ldok;
        if(:ldok is null) then begin
          execute procedure GEN_REF('DOKUMNAG') returning_values :ldok;
          insert into DOKUMNAG(REF, MAGAZYN, TYP, DATA,  KLIENT, DOSTAWCA,FAKTURA, zrodlo, DOSTAWA, NOTSERIALIZED)
          select :ldok, :magazyn, :typdok, :data,  KLIENT, DOSTAWCA,ref, :zrodlo, :dostzpzns,1
             from NAGFAK where REF=:FAK;
        end
        if(:opk = 0) then dokzpzns = :ldok;
        else dokzpznso = :ldok;
      end
      insert into DOKUMPOZ(DOKUMENT, KTM, WERSJA, ILOSC,  ILOSCO, JEDNO, CENA)
          values( :ldok, :ktm, :wersja, :ilzpzns, :ilzpznso, :jedn, :cenazpzns);
    end
  end
  if(:akcept =1 or (:akcept = 3)) then begin
   if(:dokzz is not null) then   update DOKUMNAG set AKCEPT = 1, BLOKADA = 0 where REF=:dokZZ;
   if(:dokzpz is not null) then   update DOKUMNAG set AKCEPT = 1, BLOKADA = 0 where REF=:dokZPZ;
   if(:dokzpzns is not null) then   update DOKUMNAG set AKCEPT = 1, BLOKADA = 0  where REF=:dokZPZns;
   if(:dokpz is not null) then   update DOKUMNAG set AKCEPT = 1, BLOKADA = 0 where REF=:dokPZ;
   if(:dokpzns is not null) then   update DOKUMNAG set AKCEPT = 1, BLOKADA = 0 where REF=:dokPZns;
   if(:dokwz is not null) then   update DOKUMNAG set AKCEPT = 1, BLOKADA = 0 where REF=:dokwz;

   if(:dokzzo is not null) then   update DOKUMNAG set AKCEPT = 1 , BLOKADA = 0 where REF=:dokZZo;
   if(:dokzpzo is not null) then   update DOKUMNAG set AKCEPT = 1 , BLOKADA = 0 where REF=:dokZPZo;
   if(:dokzpznso is not null) then   update DOKUMNAG set AKCEPT = 1 , BLOKADA = 0 where REF=:dokZPZnso;
   if(:dokwzo is not null) then   update DOKUMNAG set AKCEPT = 1 , BLOKADA = 0 where REF=:dokwzo;
   if(:dokpzo is not null) then   update DOKUMNAG set AKCEPT = 1 , BLOKADA = 0 where REF=:dokPZo;
   if(:dokpznso is not null) then   update DOKUMNAG set AKCEPT = 1 , BLOKADA = 0 where REF=:dokPZnso;
  end
  doksym = :symbol;
  status = 1;
end^
SET TERM ; ^
