--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_KTMISKPL(
      KTM KTM_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
begin
  status = 0;
  -- XXX Ldz sprawdzanie przed wydrukowaniem na przyjeciu czy KTM jest kpl
  select first 1 1
    from kplnag k
    where k.ktm = :ktm
  into :status;
  if (status is null) then status = 0;
  suspend;
end^
SET TERM ; ^
