--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PROD_WORKTIME_RECORD(
      EMPL integer,
      PERIOD char(6) CHARACTER SET UTF8                           )
  returns (
      HOURNAME varchar(255) CHARACTER SET UTF8                           ,
      T1 smallint,
      T2 smallint,
      T3 smallint,
      T4 smallint,
      T5 smallint,
      T6 smallint,
      T7 smallint,
      T8 smallint,
      T9 smallint,
      T10 smallint,
      T11 smallint,
      T12 smallint,
      T13 smallint,
      T14 smallint,
      T15 smallint,
      T16 smallint,
      T17 smallint,
      T18 smallint,
      T19 smallint,
      T20 smallint,
      T21 smallint,
      T22 smallint,
      T23 smallint,
      T24 smallint,
      T25 smallint,
      T26 smallint,
      T27 smallint,
      T28 smallint,
      T29 smallint,
      T30 smallint,
      T31 smallint,
      TPERIOD smallint,
      TSUM smallint)
   as
DECLARE VARIABLE CURDAY TIMESTAMP;
DECLARE VARIABLE LASTDAY TIMESTAMP;
DECLARE VARIABLE ECOLUMN INTEGER;
BEGIN
  for select number, name  from ecolumns where coltype = 'GDZ'
    order by number
    into :ecolumn, :hourname
  do begin
    select fday,lday from datatookres(:period,null,null,null,'1') into :curday, :lastday;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t1; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t2; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t3; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t4; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t5; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t6; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t7; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t8; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t9; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t10; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t11; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t12; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t13; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t14; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t15; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t16; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t17; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t18; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t19; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t20; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t21; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t22; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t23; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t24; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t25; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t26; curday = curday + 1;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t27; curday = curday + 1;
    if(curday <= lastday) then select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t28; curday = curday + 1;
    if(curday <= lastday) then select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t29; curday = curday + 1;
    if(curday <= lastday) then select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t30; curday = curday + 1;
    if(curday <= lastday) then select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate = :curday into :t31;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and hdate is null and period = :period into :tperiod;
    select sum(amount) from eworkedhours where employee = :empl and ecolumn = :ecolumn and period = :period into :tsum;
    suspend;
  end
end^
SET TERM ; ^
