--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_STANYREZ_DATA(
      MAGAZYNY varchar(255) CHARACTER SET UTF8                           ,
      MASKAKTM varchar(40) CHARACTER SET UTF8                           ,
      DATAOD date,
      DATADO date)
  returns (
      DATA date,
      ZAREZERW numeric(14,4),
      ZABLOKOW numeric(14,4),
      ZAMOWIONO numeric(14,4),
      ZAREZERWADD numeric(14,4),
      ZABLOKOWADD numeric(14,4),
      ZAMOWIONOADD numeric(14,4))
   as
declare variable PREVDATA date;
declare variable NEWDATA date;
declare variable NEWZAREZERW numeric(14,4);
declare variable NEWZABLOKOW numeric(14,4);
declare variable NEWZAMOWIONO numeric(14,4);
begin
  prevdata = :dataod - 1;
  zarezerw = 0;
  zablokow =0;
  zamowiono = 0;
  for select r.databl,
      sum(case when r.status='R' then r.ilosc else 0 end),
      sum(case when r.status in ('Z','B') then r.ilosc else 0 end),
      sum(case when r.status='D' then r.ilosc else 0 end)
    from stanyrez r
    where cast(r.databl as date)<=:datado
      and (r.ktm like :maskaktm or :maskaktm='')
      and (:magazyny like '%;'||trim(r.magazyn)||';%')
      and r.zreal<>1
    group by r.databl
    order by r.databl
    into :newdata,
      :newzarezerw,
      :newzablokow,
      :newzamowiono
    do begin
      zarezerwadd = 0;
      zablokowadd = 0;
      zamowionoadd = 0;
      while(:prevdata<:newdata-1) do begin
        prevdata = :prevdata + 1;
        data = :prevdata;
        if(:data>=:dataod) then suspend;
      end
      prevdata = :newdata;
      data = :prevdata;
      zarezerw = :zarezerw + :newzarezerw;
      zablokow = :zablokow + :newzablokow;
      zamowiono = :zamowiono + :newzamowiono;
      zarezerwadd = :newzarezerw;
      zablokowadd = :newzablokow;
      zamowionoadd = :newzamowiono;
      if(:data>=:dataod) then suspend;
    end
  while(:prevdata<:datado) do begin
    prevdata = :prevdata + 1;
    data = :prevdata;
    zarezerwadd = 0;
    zablokowadd = 0;
    zamowionoadd = 0;
    if(:data>=:dataod) then suspend;
  end
end^
SET TERM ; ^
