--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BHPSORT_GEN_EMPL(
      BHPEMPLREF integer,
      SORTOVERWRITE smallint = 0)
   as
declare variable bhpsortsymbol varchar(20);
begin
/*MWr: Procedura generuje dla pracownika jego artykuly BHP na podstawie
       przypisanego domyslnego sortu odziezowego

  SortOverWrite - jezeli istnieja artykuly BHP przypisane pracownikowi, to znacznik
          = 0: pojawi sie komunikat z info. o tym fakcie, generowanie anulowane
          = 1: artykuly zostana usuniete i wygenerowane na nowo */


  if (bhpemplref is null) then
    exception bhp_expt;
  else if (sortoverwrite = 0 and exists (select first 1 1 from bhpemplsortpos where bhpemplsortdef = :bhpemplref)) then
    exception bhp_expt 'Nie można generować artykułów BHP dla pracownika z artyk. BHP';
  else begin
    if (sortoverwrite = 1) then
      delete from bhpemplsortpos
      where bhpemplsortdef = :bhpemplref;

    select bhpsortdef
      from bhpemplsortdef
      where ref = :bhpemplref
      into :bhpsortsymbol;

    insert into bhpemplsortpos (bhpemplsortdef, ktm, vers, period, periodtunit,
       amount, unit, washamount, washtunit, washprice)
    select :bhpemplref, ktm, versiondefault, period, periodtunit,
       amount, unit, washamount, washtunit, washprice
      from bhpsortpos
      where bhpsortdef = :bhpsortsymbol;
  end
end^
SET TERM ; ^
