--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_TAXYEAR(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET integer)
   as
declare variable period char(6);
begin
  --MW: personel - Procedura do obliczania roku podatkowego

  select tper
    from epayrolls
    where ref = :payroll
    into :period;

  ret = cast(substring(period from 1 for 4) as integer);

  if (ret is null) then
    ret = 0;
  suspend;
end^
SET TERM ; ^
