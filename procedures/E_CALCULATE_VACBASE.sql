--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_CALCULATE_VACBASE(
      EMPLREF integer,
      FROMBASEPERIOD varchar(6) CHARACTER SET UTF8                           ,
      TOBASEPERIOD varchar(6) CHARACTER SET UTF8                           ,
      CURRENTCOMPANY integer)
   as
declare variable EMPLOYEE integer;
declare variable VBASE numeric(14,2);
declare variable VWORKEDDAYS integer;
declare variable PERIOD varchar(6);
declare variable VWORKEDHOURS numeric(14,2);
begin
  for
    select p.employee, pr.cper,
        sum(case when p.ecolumn in (510, 520, 600, 610) then 0 else p.pvalue end),
        sum(case when p.ecolumn = 510 then p.pvalue else 0 end),
        sum(case when p.ecolumn in (520, 600, 610) then p.pvalue else 0 end)
      from epayrolls pr
        join eprpos p on (p.payroll = pr.ref)
        join ecolumns c on (c.number = p.ecolumn)
        join employees e on (e.ref = p.employee and (e.ref = :EmplRef or :EmplRef is NULL))
      where pr.cper <= :ToBasePeriod
        and pr.cper >= :FromBasePeriod
        and (c.cflags containing ';URL;' or p.ecolumn in (510, 520, 600, 610))
        and pr.company = :currentcompany
        and pr.tovbase = 1 --PR30950
      group by p.employee, pr.cper
      into :employee, :period, :vbase, :vworkeddays, :vworkedhours
  do begin
    if ((exists(select first 1 1 from eabsemplbases
           where employee = :employee and period = :period))) then
      update eabsemplbases
        set vbase = :vbase, vworkeddays = :vworkeddays, vworkedhours = :vworkedhours
        where period = :period and employee = :employee;
    else if (vbase > 0) then
      insert into eabsemplbases (employee, period, vbase, vworkeddays, takeit, vworkedhours)
        values (:employee, :period, :vbase, :vworkeddays, 1, :vworkedhours);
  end
end^
SET TERM ; ^
