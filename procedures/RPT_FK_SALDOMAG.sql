--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_SALDOMAG(
      ACCOUNT ACCOUNT_ID,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      FROMDATE timestamp,
      TODATE timestamp)
  returns (
      LP integer,
      BKREF integer,
      BKREG varchar(10) CHARACTER SET UTF8                           ,
      BKSYMBOL varchar(40) CHARACTER SET UTF8                           ,
      BKDATE timestamp,
      DEBIT numeric(14,2),
      CREDIT numeric(14,2),
      SALDO numeric(14,2),
      DOKMAGDATE timestamp,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           )
   as
declare variable OTABLE varchar(20);
declare variable OREF integer;
declare variable DOKTABLE varchar(20);
declare variable DOKREF integer;
declare variable DOKSYMBOL varchar(40);
declare variable LCREDIT numeric(14,2);
declare variable DOKMAGREF integer;
declare variable NAGWARTOSC numeric(14,2);
declare variable POZWARTOSC numeric(14,2);
declare variable ROZWARTOSC numeric(14,2);
declare variable WYDANIA integer;
declare variable OK integer;
declare variable CNT integer;
declare variable MAG varchar(3);
begin
  saldo = 0;
  lp = 1;
  for
    select distinct bkdocs.ref,bkdocs.bkreg,bkdocs.symbol,bkdocs.transdate,bkdocs.otable,bkdocs.oref
      from decrees
        left join bkdocs on (decrees.bkdoc=bkdocs.ref)
      where decrees.transdate>=:fromdate and decrees.transdate<=:todate
        and decrees.status > 1 and decrees.account like :account||'%'
        and decrees.autobo <> 1
      order by bkdocs.ref
      into :bkref,:bkreg,:bksymbol,:bkdate,:otable,:oref
  do begin
      /* przejdz przez wszystkie bkdocs z tym kontem*/
      descript = '';
      dokmagdate = null;
      credit = 0;
      debit = 0;
      select sum(debit-credit)
        from decrees
        where bkdoc=:bkref and status>1 and account like :account||'%'
        into :debit;
      if(:debit is null) then debit = 0;
      if(:otable is null) then otable = '';
      if(:otable<>'DOKUMNAG' and :otable<>'DOKUMNOT' and :debit<>0) then begin
        /* BKDOCS wystawiony recznie */
        credit = 0;
        descript = 'Dokument wystawiony recznie';
        saldo = :saldo + :debit - :credit;
        suspend;
        lp = :lp + 1;
      end else if(:otable='DOKUMNAG') then begin
        /* BKDOCS ma odmienne kwoty lub date od DOKUMNAG */
        credit = 0;
        select dokumnag.pwartosc,defdokum.wydania,dokumnag.data,dokumnag.magazyn
        from dokumnag
        left join defdokum on (defdokum.symbol=dokumnag.typ)
        where dokumnag.ref=:oref
        into :nagwartosc,:wydania,:dokmagdate,:mag;
        select sum(pwartosc)
        from dokumpoz where dokument=:oref
        into :pozwartosc;
        select sum(dokumroz.pwartosc)
        from dokumroz
        join dokumpoz on (dokumpoz.ref=dokumroz.pozycja)
        where dokumpoz.dokument=:oref
        into :rozwartosc;
        nagwartosc = :nagwartosc*(1-2*:wydania);
        pozwartosc = :pozwartosc*(1-2*:wydania);
        rozwartosc = :rozwartosc*(1-2*:wydania);
        ok = 1;
        if(:debit<>:nagwartosc or :debit<>:pozwartosc or :debit<>:rozwartosc) then begin
          credit = :pozwartosc;
          descript = :descript || ' - rozbiezne kwoty: '||:nagwartosc||'/'||:pozwartosc||'/'||:rozwartosc;
          saldo = :saldo + :debit - :pozwartosc;
          ok = 0;
        end
        if(:dokmagdate is null or :dokmagdate>:todate) then begin
          descript = :descript || ' - rozbiezne daty: '||:dokmagdate;
          saldo = :saldo + :debit;
          ok = 0;
        end
        if(:magazyn<>:mag) then begin
          descript = :descript || ' - rozbiezne magazyny: '||:mag;
          saldo = :saldo + :debit;
          ok = 0;
        end
        if(ok=0) then begin
          suspend;
          lp = :lp + 1;
        end
      end else if(:otable='DOKUMNOT') then begin
        /* BKDOCS ma odmienne kwoty lub date od DOKUMNOT */
        credit = 0;
        nagwartosc = 0;
        select dokumnot.wartosc,defdokum.wydania,dokumnot.data,dokumnot.magazyn
        from dokumnot
        left join defdokum on (defdokum.symbol=dokumnot.typ)
        where dokumnot.ref=:oref
        into :nagwartosc,:wydania,:dokmagdate,:mag;
        select sum(wartosc)
        from dokumnotp where dokument=:oref
        into :pozwartosc;
        nagwartosc = :nagwartosc*(1-2*:wydania);
        pozwartosc = :pozwartosc*(1-2*:wydania);
        ok = 1;
        if(:debit<>:nagwartosc or :debit<>:pozwartosc) then begin
          credit = :pozwartosc;
          descript = :descript || ' - rozbiezne kwoty: '||:nagwartosc||'/'||:pozwartosc;
          saldo = :saldo + :debit - :pozwartosc;
          ok = 0;
        end
        if(dokmagdate is null or :dokmagdate>:todate) then begin
          descript = :descript || ' - rozbiezne daty: '||:dokmagdate;
          saldo = :saldo + :debit;
          ok = 0;
        end
        if(:magazyn<>:mag) then begin
          descript = :descript || ' - rozbiezne magazyny: '||:mag;
          saldo = :saldo + :debit;
          ok = 0;
        end
        if(ok=0) then begin
          suspend;
          lp = :lp + 1;
        end
      end
    end
  for select dokumnag.ref,dokumnag.symbol,dokumnag.data,dokumnag.pwartosc*(1-2*defdokum.wydania)
    from dokumnag
    left join defdokum on (defdokum.symbol=dokumnag.typ)
    where dokumnag.data>=:fromdate and dokumnag.data<=:todate
    and dokumnag.magazyn=:magazyn
    and dokumnag.pwartosc<>0 and dokumnag.pwartosc is not null
    order by dokumnag.ref
    into :dokmagref,:descript,:dokmagdate,:credit
    do begin
      /* przejdz przez wszystkie dokumnagi*/
      debit = 0;
      bkref = null;
      bksymbol = '';
      bkreg = '';
      bkdate = null;
      select bkdocs.ref,bkdocs.bkreg,bkdocs.symbol,bkdocs.transdate
      from bkdocs
      where bkdocs.otable='DOKUMNAG' and bkdocs.oref=:dokmagref
      into :bkref,:bkreg,:bksymbol,:bkdate;
      if(:bkref is null) then begin
        /* DOKUMNAG nie ma powizanego BKDOCS*/
        descript = :descript|| ' brak dokumentu ksiegowego';
        saldo = :saldo + :debit - :credit;
        suspend;
        lp = :lp + 1;
      end else if(:bkdate>:todate) then begin
        /* powiazany BKDOCS do DOKUMNAG jest poza okresem */
        descript = :descript|| ' dokument ksiegowy poza okresem';
        saldo = :saldo + :debit - :credit;
        suspend;
        lp = :lp + 1;
      end else begin
        cnt = 0;
        select count(*)
        from decrees
        where decrees.bkdoc=:bkref
        and decrees.status>1
        and decrees.account like :account||'%'
        into :cnt;
        if(:cnt=0) then begin
          /* powiazany BKDOCS do DOKUMNAG nie ma dekretow na to konto */
          descript = :descript|| ' dokument ksiegowy nie dotyczy badanego konta';
          saldo = :saldo + :debit - :credit;
          suspend;
          lp = :lp + 1;
        end
      end
    end

  for select dokumnot.ref,dokumnot.symbol,dokumnot.data,dokumnot.wartosc*(1-2*defdokum.wydania)
    from dokumnot
    left join defdokum on (defdokum.symbol=dokumnot.typ)
    where dokumnot.data>=:fromdate and dokumnot.data<=:todate
    and dokumnot.magazyn=:magazyn
    and dokumnot.wartosc<>0
    order by dokumnot.ref
    into :dokmagref,:descript,:dokmagdate,:credit
    do begin
      /* przejdz przez wszystkie dokumnoty*/
      debit = 0;
      bkref = null;
      bksymbol = '';
      bkreg = '';
      bkdate = null;
      select bkdocs.ref,bkdocs.bkreg,bkdocs.symbol,bkdocs.transdate
      from bkdocs
      where bkdocs.otable='DOKUMNOT' and bkdocs.oref=:dokmagref
      into :bkref,:bkreg,:bksymbol,:bkdate;
      if(:bkref is null) then begin
        /* DOKUMNOT nie ma powizanego BKDOCS*/
        descript = :descript|| ' brak dokumentu ksiegowego';
        saldo = :saldo + :debit - :credit;
        suspend;
        lp = :lp + 1;
      end else if(:bkdate>:todate) then begin
        /* powiazany BKDOCS do DOKUMNOT jest poza okresem */
        descript = :descript|| ' dokument ksiegowy poza okresem';
        saldo = :saldo + :debit - :credit;
        suspend;
        lp = :lp + 1;
      end else begin
        cnt = 0;
        select count(*)
        from decrees
        where decrees.bkdoc=:bkref
        and decrees.status>1
        and decrees.account like :account||'%'
        into :cnt;
        if(:cnt=0) then begin
          /* powiazany BKDOCS do DOKUMNOT nie ma dekretow na to konto */
          descript = :descript|| ' dokument ksiegowy nie dotyczy badanego konta';
          saldo = :saldo + :debit - :credit;
          suspend;
          lp = :lp + 1;
        end
      end
    end

end^
SET TERM ; ^
