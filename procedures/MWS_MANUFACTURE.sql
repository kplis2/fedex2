--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_MANUFACTURE(
      MWSCONSTLOC integer,
      ORDERTYPE varchar(3) CHARACTER SET UTF8                           ,
      ORDERREG varchar(3) CHARACTER SET UTF8                           ,
      ORDEROPER varchar(3) CHARACTER SET UTF8                           ,
      WH varchar(3) CHARACTER SET UTF8                           ,
      MGOODOUT varchar(40) CHARACTER SET UTF8                           ,
      MVERSOUT integer,
      MQUANTITYOUT numeric(14,4),
      MGOODIN varchar(40) CHARACTER SET UTF8                           ,
      MVERSIN integer,
      MQUANTITYIN numeric(14,4),
      MWSPALLOC integer = 0,
      MWSSTOCK integer = 0)
  returns (
      STATUS smallint,
      DOCS varchar(100) CHARACTER SET UTF8                           ,
      ERROR varchar(100) CHARACTER SET UTF8                           )
   as
declare variable orderref integer;
declare variable measure integer;
declare variable mversnumout integer;
declare variable orderhist integer;
declare variable histstatus smallint;
begin
  select ref from towjedn where ktm = :mgoodin and glowna = 1 into measure;
  select nrwersji from wersje where ref = :mversout into mversnumout;
  execute procedure gen_ref('NAGZAM') returning_values orderref;
  insert into nagzam (ref, rejestr, typzam, datawe, magazyn, termdost, walutowe,
      /*kktm, kwersja*/kwersjaref, kilosc, kmwsconstloc, kmwspalloc, typ, kmwsstock)
    values(:orderref, :orderreg, :ordertype, current_date, :wh, current_date, 0,
        /*:mgoodout, :mversnumout*/:mversout, :mquantityout, :mwsconstloc, :mwspalloc, 2, :mwsstock);
  insert into pozzam (wersjaref, ktm, ilosc, jedn, magazyn, zamowienie)
    values(:mversin, :mgoodin, :mquantityin, :measure, :wh, :orderref);
  execute PROCEDURE OPER_CREATE_HIST_ZAM(:orderref,:orderoper,null,1,null,0,0)
    returning_values(histstatus, orderhist);
  update nagzam set kilosc = :mquantityout, kilreal = :mquantityout where ref = :orderref;
  update pozzam set ilreal = :mquantityin where zamowienie = :orderref;
  execute procedure oper_mag_create(:orderref,:orderoper,:orderhist,1,0, current_date)
    returning_values(:status,:docs,:error);
end^
SET TERM ; ^
