--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMPL_PAIDCARD(
      REF integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      IS_EMPL smallint)
  returns (
      ECOLUMN integer,
      ECOLNAME varchar(40) CHARACTER SET UTF8                           ,
      PVALUE numeric(14,2))
   as
begin
  if (is_empl is null) then is_empl = 0;
  for
    select EPRPOS.ECOLUMN, max(ECOLUMNS.NAME),sum(EPRPOS.PVALUE)
      from EPRPOS
        join EPAYROLLS on (EPRPOS.PAYROLL=EPAYROLLS.REF)
        join ECOLUMNS on (EPRPOS.ECOLUMN=ECOLUMNS.NUMBER and ECOLUMNS.PAIDCARD=1)
        join employees on employees.ref = eprpos.employee
      where
        ((employees.person = :ref and :is_empl = 0)
        or (employees.ref = :ref and :is_empl = 1))
        and EPAYROLLS.CPER=:period
      group by EPRPOS.ECOLUMN
      order by EPRPOS.ECOLUMN
      into :ecolumn,:ecolname,:pvalue
  do begin
    if(:pvalue is null) then pvalue = 0;
    suspend;
  end
end^
SET TERM ; ^
