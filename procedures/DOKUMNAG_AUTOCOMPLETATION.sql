--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_AUTOCOMPLETATION(
      REFDOK integer,
      POZDOK integer,
      ACK smallint,
      INWENTA smallint)
   as
declare variable koryg smallint;
declare variable zewn smallint;
declare variable wydania smallint;
declare variable prodtryb smallint;
declare variable dokrozch varchar(3);
declare variable dokprzych varchar(3);
declare variable magprzych varchar(3);
declare variable magrozch varchar(3);
declare variable towtypekpl varchar(100);
declare variable ktm varchar(40);
declare variable wersja smallint;
declare variable wersjaref integer;
declare variable refrozch integer;
declare variable jedno integer;
declare variable ilosc numeric(14,4);
declare variable iloscdost numeric(14,4);
declare variable dokumref integer;
declare variable sumwartrmag numeric(14,2);
declare variable cenamag numeric(14,4);
declare variable refpoz integer;
declare variable refroz integer;
declare variable refrozuzup integer;
declare variable ref2 integer;
declare variable symbol2 varchar(20);
declare variable magprzych2 varchar(3);
declare variable magrozch2 varchar(3);
declare variable refpozzam integer;
declare variable alttopoz dokumpoz_id;
declare variable targetref dokumpoz_id;
declare variable fakepoz smallint;
declare variable altpoz smallint;
declare variable grupasped integer;
declare variable dostawa integer;
declare variable termdost timestamp;
begin
  ref2 = null;
  symbol2 = null;
  altpoz = 0;
  -- kompletacja na zadanie, automatyczne laczenie w komplety towarow z dokumentow mag. wydania
  if (:inwenta is null or :inwenta = 0) then begin
    select defdokum.koryg, defdokum.wydania, defdokum.zewn, defdokummag.prodtryb,
       defdokummag.proddokprzych, defdokummag.proddokrozch, defdokummag.magazyn, defdokummag.magazyn,
       dokumnag.grupasped, dokumnag.termdost
      from dokumnag
        join defdokum on (dokumnag.typ = defdokum.symbol)
        join defdokummag on (defdokummag.typ = defdokum.symbol)
      where dokumnag.ref = :refdok and defdokummag.magazyn = dokumnag.magazyn
      into :koryg, :wydania, :zewn, :prodtryb, :dokprzych, :dokrozch, :magprzych, :magrozch,
        :grupasped, :termdost;
    magprzych2 = null;
    magrozch2 = null;
    if (exists(select first 1 1 from dokumpoz where dokument = :refdok and fake = 1 and alttopoz is not null)) then
      altpoz = 1;
  end else if (:inwenta > 0) then begin
    select defdokum.koryg, defdokum.wydania, defdokum.zewn, -1,
       oddzialy.przmaginwent, oddzialy.rozmaginwent, oddzialy.maginwent, dokumnag.magazyn
      from dokumnag
        join defdokum on (dokumnag.typ = defdokum.symbol)
        join oddzialy on (dokumnag.oddzial = oddzialy.oddzial)
      where dokumnag.ref = :refdok and oddzialy.przmaginwent is not null and
        oddzialy.rozmaginwent is not null and oddzialy.maginwent is not null
        and oddzialy.przmaginwent <> '' and oddzialy.rozmaginwent <> ''
        and oddzialy.maginwent <> ''
      into :koryg, :wydania, :zewn, :prodtryb, :dokprzych, :dokrozch, :magprzych, :magrozch;
    magprzych2 = :magrozch;
    magrozch2 = :magprzych;
  end
  if (:prodtryb is null) then prodtryb = 0;
  -- wartosci dla prodtryb 0 - brak, 1 - cala ilosc okreslona na dokumencie, 2 - tylko brakujaca ilosc, 3 - tryb alternatyw
  if ((:prodtryb < 0 or :prodtryb > 3) and :prodtryb <> -1) then
    exception DOKUMNAG_EXPT 'Nieznany tryb kompletacji!!!' ;
  if ( ((:wydania = 1 and :prodtryb > 0) or (:wydania = 0 and:prodtryb = -1)) or (:prodtryb > 2)
        and (:ack = 1 or :ack = 5) ) then
  begin
    if (:dokprzych is null or :dokprzych = '' or :dokprzych = '   ') then
      exception DOKUMNAG_EXPT 'Nie określono dok. przych. dla dokumentu na magazynie';
    if (not exists (select magazyn from defdokummag where defdokummag.magazyn = :magprzych
          and defdokummag.typ = :dokprzych)) then
      exception DOKUMNAG_EXPT 'Nie zdefiniowany dokument przych. dla magazynu';
    if (:dokrozch is null or :dokrozch = '' or :dokrozch = '   ') then
      exception DOKUMNAG_EXPT 'Nie określono dok. rozch. dla dokumentu na magazynie';
    if (not exists (select magazyn from defdokummag where defdokummag.magazyn = :magrozch
          and defdokummag.typ = :dokrozch)) then
      exception DOKUMNAG_EXPT 'Nie zdefiniowany dokument rozch. dla magazynu';
    execute procedure get_config('TOWTYPEKOMPLET',2) returning_values :towtypekpl;
    if (:towtypekpl is null or :towtypekpl = '') then
      towtypekpl = '2';
    -- produkacja nastepuje dla kazdej pozycji
    -- 1 pozycja dokumentu to jeden cykl kompletacji (dokument rozhodowy i przychodowy)
    select dokumpoz.ktm, dokumpoz.wersja, dokumpoz.wersjaref, dokumpoz.ilosc, dokumpoz.jedno, dokumpoz.dostawa
      from dokumpoz
        left join towary on (towary.ktm = dokumpoz.ktm)
      where dokumpoz.ref = :pozdok and (towary.usluga = cast(:towtypekpl as integer) or :prodtryb in (-1,3))
      order by dokumpoz.ref
      into :ktm, :wersja, :wersjaref, :ilosc, :jedno, :dostawa;
    if ((not exists (select ref from kplnag where kplnag.wersjaref = :wersjaref and kplnag.glowna = 1))
         and :prodtryb in (1,2)) then
      exit;
    if (:ilosc is null) then ilosc = 0;
    -- okreslenie ilosci do kompletacji dla trybu kompletacji tylko brakujacych ilosci
    if (:prodtryb = 2) then
    begin
      select frompozzam from dokumpoz dp where ref = :pozdok into refpozzam;
      if (refpozzam is null) then
      begin
        select frompozfak from dokumpoz where ref = :pozdok into :refpozzam;
        if (refpozzam is null) then refpozzam = 0;
        execute procedure mag_checkilwol(:refpozzam, :magrozch, :wersjaref, 'F', null) returning_values :iloscdost;
      end else
        execute procedure mag_checkilwol(:refpozzam, :magrozch, :wersjaref, 'Z', null) returning_values :iloscdost;
      if (:iloscdost is null) then iloscdost = 0;
      if (:iloscdost >= :ilosc) then
        ilosc = 0;
      else
        ilosc = :ilosc - :iloscdost;
        if (ilosc <= 0) then exit;
    end 
    -- w trybie kompletacji calej ilosci na dokument przychodowy trafi cala ilosc okreslona na akceptowanym dokumencie
    -- rejestracja dokumentu rozchodiwego na podstawie glownej receptury
    -- sprawdzenie czy dokument byl wycofywany do poprawy jezeli tak to mamy jego naglowek
    if (:ack = 5) then
      select dokumnag.ref from dokumnag
        where dokumnag.typ = :dokrozch and dokumnag.grupaprod = :pozdok and dokumnag.akcept = 9
        into :dokumref;
    if (:dokumref is null) then
    begin
      execute procedure GEN_REF('DOKUMNAG') returning_values :dokumref;
      insert into dokumnag (ref, magazyn, mag2, typ, oddzial, operator, okres, data, grupadok, grupaprod, sposdost, grupasped, termdost)
        select :dokumref, :magrozch, :magrozch2, :dokrozch, dokumnag.oddzial, dokumnag.operator, dokumnag.okres, dokumnag.data, :refdok, :pozdok, dokumnag.sposdost, :grupasped, :termdost
          from dokumnag where dokumnag.ref = :refdok;
    end
    if (coalesce(:altpoz,0) > 0) then begin
      select dokumpoz.alttopoz, dokumpoz.ref from dokumpoz where dokumpoz.ref = :pozdok into :AltToPoz, :TargetRef;
      if (:alttopoz is null) then begin
        insert into dokumpoz (dokument, ktm, wersja, wersjaref, ilosc, fake, havefake, fromzam, frompozzam)
          select :dokumref, dokumpoz.ktm, dokumpoz.wersja, dokumpoz.wersjaref, dokumpoz.ilosc, 0, 0, dokumpoz.fromzam, dokumpoz.frompozzam
            from dokumpoz
            where dokumpoz.alttopoz = :TargetRef and dokumpoz.ilosc > 0;
      end
    end
    else if (:prodtryb in (1,2)) then begin
      insert into dokumpoz (dokument, ktm, wersja, wersjaref, ilosc)
        select :dokumref, kplpoz.ktm, kplpoz.wersja, kplpoz.wersjaref, kplpoz.ilosc * :ilosc
          from kplpoz
            left join kplnag on (kplnag.ref = kplpoz.nagkpl)
          where kplnag.glowna = 1 and kplnag.wersjaref = :wersjaref and kplpoz.ilosc > 0;
    end else if (:prodtryb = -1) then begin
      insert into dokumpoz (dokument, ktm, wersja, wersjaref, ilosc)
        select :dokumref, dokumpoz.ktm, dokumpoz.wersja, dokumpoz.wersjaref, dokumpoz.ilosc
          from dokumpoz
          where dokumpoz.ref = :pozdok and dokumpoz.ilosc > 0;
    end
    if (exists(select first 1 1 from dokumpoz d where d.dokument = :dokumref)) then begin
      update dokumnag set dokumnag.akcept = 1 where dokumnag.ref = :dokumref;
    end else
      delete from dokumnag where dokumnag.ref = :dokumref;
    if (:prodtryb = -1) then begin
      ref2 = :dokumref;
      select dokumnag.symbol from dokumnag where dokumnag.ref = :ref2 into :symbol2;
    end
    refrozch = :dokumref;
    if (:refrozch is null) then
      exception DOKUMNAG_EXPT 'brak dokumentu rozchodowego produkcji';
    -- rejestracja dokumentu przychodowego na podstawie rejestrowanego dokumentu
    -- sprawdzenie czy dokument byl wycofywany do poprawy jezeli tak to mamy jego naglowek
    dokumref = null;
    if (ack = 5) then
      select dokumnag.ref from dokumnag
        where dokumnag.typ = :dokprzych and dokumnag.grupaprod = :pozdok and dokumnag.akcept = 9
        into :dokumref;
    if (:dokumref is null) then
    begin
      execute procedure GEN_REF('DOKUMNAG') returning_values :dokumref;
      insert into dokumnag (ref, magazyn, mag2, typ, oddzial, operator, okres, data, grupadok, grupaprod, sposdost, grupasped, termdost)
        select :dokumref, :magprzych, :magprzych2, :dokprzych, dokumnag.oddzial, dokumnag.operator, dokumnag.okres, dokumnag.data, dokumnag.ref, :pozdok, dokumnag.sposdost, :grupasped, termdost
          from dokumnag where dokumnag.ref = :refdok;
    end
    select dkp.fake
      from dokumpoz dkp
      where dkp.ref = :pozdok
      into :FakePoz;
    if ( (coalesce(FakePoz,0) = 0) ) then
    begin
    execute procedure GEN_REF('DOKUMPOZ') returning_values :refpoz;
    -- wycena dokumentu przychodowego
      select sum(WARTOSC) from DOKUMNAG
        left join DEFDOKUM on (DEFDOKUM.SYMBOL = DOKUMNAG.typ)
        where dokumnag.grupaprod = :pozdok and DEFDOKUM.WYDANIA = 1
          and dokumnag.ref <> :dokumref and dokumnag.akcept = 1
        into :sumwartrmag;
      if(:ilosc > 0) then
        cenamag = :sumwartrmag /:ilosc;
      else
        cenamag = 0;
    insert into dokumpoz (ref, dokument, ktm, wersja, wersjaref, ilosc, cena, dostawa)
      values (:refpoz, :dokumref, :ktm, :wersja, :wersjaref, :ilosc, :cenamag, :dostawa);
    update dokumnag set dokumnag.akcept = 1, dokumnag.ref2 = :ref2, dokumnag.symbol2 = :symbol2
      where dokumnag.ref = :dokumref;
    end
    if (:prodtryb = -1) then begin
      select dokumnag.symbol from dokumnag where dokumnag.ref = :dokumref into :symbol2;
      update dokumnag set dokumnag.ref2 = :dokumref, dokumnag.symbol2 = :symbol2
        where dokumnag.ref = :ref2;
    end
    -- podlaczenie rozpisek rozchodowych i przychodowych
    select dokumroz.ref from dokumroz where dokumroz.pozycja = :refpoz into :refroz;
    for
      select dokumroz.ref
        from dokumnag
          join dokumpoz on (dokumpoz.dokument = dokumnag.ref)
          join dokumroz on (dokumroz.pozycja = dokumpoz.ref)
        where dokumnag.ref = :refrozch
      into :refrozuzup
    do begin
      update dokumroz set dokumroz.cenatoroz = :refroz, dokumroz.cenatopoz = :refpoz
        where dokumroz.ref = :refrozuzup;
    end
  end
end^
SET TERM ; ^
