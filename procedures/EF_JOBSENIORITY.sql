--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_JOBSENIORITY(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable heredays integer;
  declare variable outdays integer;
  declare variable jobseniority integer;
  declare variable fromdate date;
  declare variable todate date;
  declare variable payday date;
  declare variable lastday date;
  declare variable sstart date;
  declare variable nextmonth smallint;
  declare variable iyear smallint;
begin

--MW: personel - wyliczanie stawki nagrody jubileuszowej

  select payday
    from epayrolls
    where ref = :payroll
    into :payday;

  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, lastday;

--staz pracy w firmie
  heredays = 0;
  for
    select fromdate, todate
      from employment
      where employee = :employee
      order by fromdate
      into :fromdate, :todate
  do begin
    if (todate > payday or todate is null) then
      todate = lastday;
    heredays = heredays + (todate - fromdate + 1);
  end

--staz pracy na podstawie swiadectw pracy
  outdays = 0;
  sstart = '1900-01-01';

  for
    select fromdate, todate
      from eworkcertifs
      where employee = :employee and (etermination != 20 or etermination is null)
      order by fromdate
      into :fromdate, :todate
  do begin
    if (fromdate < sstart) then
      fromdate = sstart;
    if (todate < sstart) then
      todate = sstart;
    if (fromdate = sstart and todate = sstart) then
      outdays = outdays;
    else if (fromdate = sstart and todate != sstart) then
    begin
      outdays = outdays + (todate - fromdate);
      sstart = todate;
    end else
    begin
      outdays = outdays + (todate - fromdate + 1);
      sstart = todate;
    end
  end

--staz pracy w latach
  if (extract(month from lastday) = extract(month from lastday - heredays - outdays)) then
    jobseniority = extract(year from lastday) - extract(year from lastday - heredays - outdays);
  else
    jobseniority = 0;

  ret = 0;
  if (jobseniority = 25) then ret = 1.2;
  if (jobseniority = 30) then ret = 1.4;
  if (jobseniority = 35) then ret = 1.6;
  if (jobseniority = 40) then ret = 1.8;
  if (jobseniority = 45) then ret = 2;

  suspend;
end^
SET TERM ; ^
