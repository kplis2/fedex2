--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EVACPLAN_REALIZ(
      EVACREF integer)
   as
declare variable efdate timestamp;
declare variable etdate timestamp;
declare variable vaccolumn varchar(255);
declare variable fbase varchar(6);
declare variable lbase varchar(6);
declare variable firstabsence integer;
declare variable baseabsence integer;
declare variable emplref integer;
declare variable fromdate timestamp;
declare variable todate timestamp;
begin
/*Personel: Procedura wywolywana przy realizacji wniosku urlopowego. Przenosi
    wniosek urlopowy do kartoteki nieobecnosci. Jezeli w zadanym zakresie
    wystepuja inne nieobecnosci procedura zarejestruje w kartotece nieobecnosci
    urlop w 'wolnych dniach' zadanego okresu wniosku*/

  select fromdate, todate, employee, ecolumn
    from evacplan
    where ref = :evacref
  into :fromdate, :todate, :emplref, :vaccolumn ;  --BS50555

  for
    select fromdate, todate
      from eabsences
      where employee = :emplref
        and fromdate <= :todate and todate >= :fromdate --BS49115
        and correction in (0,2)
      into efdate, etdate
  do begin
    if (fromdate <= todate) then
    begin
      if (efdate > fromdate) then
      begin
        select fbase, lbase, firstabsence, baseabsence
          from e_get_absence_periods_bases(:emplref, :vaccolumn, :fromdate, null, null)
          into :fbase, :lbase, :firstabsence, :baseabsence;

        insert into eabsences (ecolumn, employee, fromdate, todate, fbase, lbase, firstabsence, baseabsence)
          values (:vaccolumn, :emplref, :fromdate, :efdate -1, :fbase, :lbase, :firstabsence, :baseabsence);
      end
      fromdate = etdate +1;
    end
  end

  if (fromdate <= todate) then
  begin
    select fbase, lbase, firstabsence, baseabsence
      from e_get_absence_periods_bases(:emplref, :vaccolumn, :fromdate, null, null)
      into :fbase, :lbase, :firstabsence, :baseabsence;

    insert into eabsences ( ecolumn, employee, fromdate, todate, fbase, lbase, firstabsence, baseabsence)
      values (:vaccolumn, :emplref, :fromdate, :todate, :fbase, :lbase, :firstabsence, :baseabsence);
  end

  update evacplan v set v.state = 2 where v.ref = :evacref and v.state is distinct from 2; --BS102861

end^
SET TERM ; ^
