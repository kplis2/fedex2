--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAV_CFGWINDOWH
  returns (
      REF varchar(40) CHARACTER SET UTF8                           ,
      PARENT varchar(40) CHARACTER SET UTF8                           ,
      NUMBER integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      HINT varchar(255) CHARACTER SET UTF8                           ,
      KINDSTR varchar(20) CHARACTER SET UTF8                           ,
      ACTIONID varchar(60) CHARACTER SET UTF8                           ,
      APPLICATIONS varchar(60) CHARACTER SET UTF8                           ,
      FORMODULES varchar(60) CHARACTER SET UTF8                           ,
      RIGHTS varchar(255) CHARACTER SET UTF8                           ,
      RIGHTSGROUP varchar(255) CHARACTER SET UTF8                           ,
      ISNAVBAR smallint,
      ISTOOLBAR smallint,
      ISWINDOW smallint,
      ISMAINMENU smallint,
      ITEMTYPE smallint,
      GROUPPROCEDURE varchar(60) CHARACTER SET UTF8                           ,
      CHILDRENCOUNT smallint,
      RIBBONTAB varchar(40) CHARACTER SET UTF8                           ,
      RIBBONGROUP varchar(40) CHARACTER SET UTF8                           ,
      NATIVETABLE varchar(40) CHARACTER SET UTF8                           ,
      POPUPMENU varchar(40) CHARACTER SET UTF8                           ,
      NATIVEFIELD varchar(40) CHARACTER SET UTF8                           ,
      FORADMIN smallint)
   as
begin
  hint = '';
  actionid = 'ConfigFromGlobal';
  isnavbar = 0;
  istoolbar = 1;
  iswindow = 0;
  ismainmenu = 0;
  itemtype = 0;
  childrencount = 0;
  ribbontab = '';
  ribbongroup = '';
  nativetable = 'CFGWINDOWH';
  popupmenu = '';
  groupprocedure = '';
  applications = '';
  nativefield = 'SYMBOL';
  foradmin = 0;
  kindstr = '';
  rights = '';
  rightsgroup = '';
  number = 1;
  for select SYMBOL, PARENT, CAPTION, FORMODULES, APPLICATION
  from CFGWINDOWH
  order by CAPTION
  into :REF, :PARENT, :NAME, :FORMODULES, :APPLICATIONS
  do begin
    if(:parent='') then parent = NULL;
    select count(SYMBOL) from CFGWINDOWH where parent=:ref into :childrencount;
    if(:childrencount is null) then childrencount = 0;
    suspend;
    number = :number + 1;
  end
end^
SET TERM ; ^
