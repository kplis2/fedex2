--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXP2PLATNIK_RSA(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      COMPANY integer,
      FORPERSON integer = 0)
  returns (
      ROWNUMBER integer,
      PERSON integer,
      SNAME varchar(30) CHARACTER SET UTF8                           ,
      FNAME varchar(60) CHARACTER SET UTF8                           ,
      DOCTYPE char(1) CHARACTER SET UTF8                           ,
      DOCNO varchar(11) CHARACTER SET UTF8                           ,
      ICODE varchar(60) CHARACTER SET UTF8                           ,
      BCODE varchar(20) CHARACTER SET UTF8                           ,
      FROMDATE date,
      TODATE date,
      DAYS integer,
      AMOUNT numeric(18,2),
      SCODE varchar(20) CHARACTER SET UTF8                           ,
      ECOLUMN integer)
   as
declare variable COUNTER integer;
declare variable PERSON_TEMP integer;
declare variable SNAME_TEMP varchar(30);
declare variable FNAME_TEMP varchar(60);
declare variable DOCTYPE_TEMP char(1);
declare variable DOCNO_TEMP varchar(11);
declare variable ICODE_TEMP varchar(60);
declare variable BCODE_TEMP varchar(20);
declare variable FROMDATE_TEMP date;
declare variable TODATE_TEMP date;
declare variable DAYS_TEMP integer;
declare variable SCODE_TEMP varchar(20);
declare variable AMOUNT_TEMP numeric(18,2);
declare variable PERSON_TEMP2 integer;
declare variable SNAME_TEMP2 varchar(30);
declare variable FNAME_TEMP2 varchar(60);
declare variable DOCTYPE_TEMP2 char(1);
declare variable DOCNO_TEMP2 varchar(11);
declare variable ICODE_TEMP2 varchar(60);
declare variable BCODE_TEMP2 varchar(20);
declare variable FROMDATE_TEMP2 date;
declare variable TODATE_TEMP2 date;
declare variable DAYS_TEMP2 integer;
declare variable SCODE_TEMP2 varchar(20);
declare variable AMOUNT_TEMP2 numeric(18,2);
declare variable AFROMDATE date;
declare variable ATODATE date;
declare variable ZPZUS numeric(14,2);
declare variable PAYDAY timestamp;
declare variable EMPLTODATE timestamp;
declare variable CORRECTION smallint;
declare variable CORRECTION_TEMP smallint;
declare variable CORRECTION2 smallint;
declare variable CFLAGS varchar(40);
declare variable ECOLUMN_TEMP integer;
declare variable ECOLUMN_TEMP2 integer;
begin
/*MWr: Personel - Eksport do Platnika deklaracji rozliczeniowej ZUS RSA (ver.xml)
  SCODE nie jest czytane przy imporcie do Platnika ver.8 (PR30893)*/

  if (coalesce(period,'') = '') then  --PR50885
    exit;

  execute procedure period2dates(:period)
    returning_values afromdate, atodate;

  execute procedure get_pval(:afromdate, :company, 'ZPZUS')
    returning_values :zpzus;

  counter = 0;
  rownumber = 0;
  forperson = coalesce(forperson,0);

  for
    select p.ref, e.sname, e.fname,
        case when (p.pesel > '') then 'P' when (p.evidenceno > '') then '1' when (p.passportno > '') then '2' else '' end,
        case when (p.pesel > '') then p.pesel when (p.evidenceno > '') then p.evidenceno when (p.passportno > '') then p.passportno else '' end,
        zc2.code||zc3.code||zc4.code, zc1.code, a.fromdate, a.todate, coalesce(a.days,0), zc5.code,
        coalesce(a.payamount, 0) - coalesce(ac.payamount, 0), a.ecolumn, pr.payday, a.correction, c.cflags
      from persons p
        join employees e on (p.ref = e.person)
        join eabsences a on (a.employee = e.ref)
        join ecolumns c on (c.number = a.ecolumn)
        left join epayrolls pr on (a.epayroll = pr.ref)
        left join ezusdata ez on (ez.person = p.ref and ez.fromdate = (select max(fromdate) from ezusdata where person = p.ref and fromdate < :atodate))
        left join edictzuscodes zc1 on (zc1.ref = a.bcode)
        left join edictzuscodes zc2 on (zc2.ref = ez.ascode)
        left join edictzuscodes zc3 on (zc3.ref = ez.retired)
        left join edictzuscodes zc4 on (zc4.ref = ez.invalidity)
        left join edictzuscodes zc5 on (zc5.ref = a.scode)
        left join eabsences ac on (ac.ref = a.corrected)
        --left join epayrolls prc on prc.ref = ac.epayroll
      where a.bcode > 0
        --and a.correction in (0,2)
        and e.company = :company
        and (p.ref = :forperson or :forperson = 0) --PR50885
        and ((a.ecolumn in (40, 50, 60, 90, 100, 110, 120, 140, 150, 160, 170, 270, 280, 290, 350, 360, 370, 440, 450) and /*(prc.iper = :period or (prc.ref is null and */pr.iper = :period/*))*/)
          or (a.ecolumn in (10, 130, 190, 260, 300, 330, 390) and a.fromdate >= :afromdate and a.todate <= :atodate))
      order by e.sname, e.fname, a.fromdate
      into :person, :sname, :fname, :doctype, :docno,
           :icode, :bcode, :fromdate, :todate, :days, :scode,
           :amount, :ecolumn, :payday, :correction, :cflags
  do begin

    if (zpzus = 0 and cflags containing ';ZAS;') then --BS45310
    begin
      amount = 0; 
      days = null; --BS52658
    end else
    if (amount < 0) then
    begin
    --BS38306 Platnik nie zezwala na minusowe wartosci, a w jakis sposob chcemy o bledzie zakomunikowac
      amount = -amount;
      days = 999;
    end

    if (doctype = '') then doctype = '0';
    if (docno = '') then docno = '0';
    if (correction = 2 and amount >= 0) then correction = 0;

    if (bcode not in ('151','152','111','121','122')) then
    begin
      --jezeli zostal zwolniony przed wyplata to kod 3000
      select max(todate) from (
          select max(coalesce(m.todate,:payday)) as todate
            from employment m
              join employees e on e.ref = m.employee
            where e.person = :person
            group by e.ref)
        into :empltodate;
  
      --jezeli wyplata nastapila w miesiacu nastepnym -> kod 3000
      execute procedure monthlastday(empltodate) returning_values empltodate;
      if (empltodate < payday) then
        icode = '3000'||substring(icode from 5 for 6);
    end

    if (counter = 0) then
    begin
      person_temp = person;
      sname_temp = sname;
      fname_temp =  fname;
      doctype_temp = doctype;
      docno_temp = docno;
      icode_temp = icode;
      bcode_temp = bcode;
      fromdate_temp = fromdate;
      todate_temp = todate;
      amount_temp = amount;
      days_temp = days;
      scode_temp = scode;
      correction_temp = correction;
      ecolumn_temp = ecolumn;
    end else
    begin
      --jesli nalezy zbic nieobecnosci
      if (person_temp = person and counter > 0 and icode_temp = icode 
        and bcode_temp = bcode and coalesce(scode_temp,'') = coalesce(scode,'') 
        and (todate_temp + 1) >= :fromdate and correction = correction_temp
        and ecolumn_temp = ecolumn --PR50885
     ) then begin
        todate_temp = todate;
        amount_temp = amount + amount_temp;
        days_temp = days + days_temp;
        if (days_temp > 999) then days_temp = 999; --BS56023
      end else
      begin
        person_temp2 = person;
        sname_temp2 = sname;
        fname_temp2 = fname ;
        doctype_temp2 = doctype;
        docno_temp2 = docno;
        icode_temp2 = icode;
        bcode_temp2 = bcode;
        fromdate_temp2 = fromdate;
        todate_temp2 = todate;
        amount_temp2 = amount;
        days_temp2 = days;
        scode_temp2 = scode;
        correction2 = correction;
        ecolumn_temp2 = ecolumn;

        person = person_temp;
        sname = sname_temp;
        fname = fname_temp;
        doctype = doctype_temp;
        docno = docno_temp;
        icode = icode_temp;
        bcode = bcode_temp;
        fromdate = fromdate_temp;
        todate = todate_temp;
        amount = amount_temp;
        days = days_temp;
        scode = scode_temp;
        correction = correction_temp;
        ecolumn = ecolumn_temp;

        rownumber = rownumber + 1;
        suspend;

        person_temp = person_temp2;
        sname_temp = sname_temp2;
        fname_temp = fname_temp2;
        doctype_temp = doctype_temp2;
        docno_temp = docno_temp2;
        icode_temp = icode_temp2;
        bcode_temp = bcode_temp2;
        fromdate_temp = fromdate_temp2;
        todate_temp = todate_temp2;
        amount_temp = amount_temp2;
        days_temp = days_temp2;
        scode_temp = scode_temp2;
        correction_temp = correction2;
        ecolumn_temp = ecolumn_temp2;
       end
     end
     counter = counter +1;
  end
  if (counter >= 1) then
  begin
    person = person_temp;
    sname = sname_temp;
    fname = fname_temp;
    doctype = doctype_temp;
    docno = docno_temp;
    icode = icode_temp;
    bcode = bcode_temp;
    fromdate = fromdate_temp;
    todate = todate_temp;
    amount = amount_temp;
    days = days_temp;
    scode = scode_temp;
    correction = correction_temp;
    ecolumn = ecolumn_temp;
    rownumber = rownumber + 1;
    suspend;
  end

  if (rownumber = 0) then
  begin
    --jesli nie bylo danych w danych miesiacu byc moze wszystkie zostaly wystornowane
    --jezeli tak nalezy wartosci skorygowane zamiescic na RSA dajac nullowa kwote i liczbe dni 0
    amount = null;
    days = 0;
    for
      select p.ref, e.sname, e.fname,
          case when (p.pesel > '') then 'P' when (p.evidenceno > '') then '1' when (p.passportno > '') then '2' else '' end,
          case when (p.pesel > '') then p.pesel when (p.evidenceno > '') then p.evidenceno when (p.passportno > '') then p.passportno else '' end,
          zc2.code||zc3.code||zc4.code, zc1.code, a.fromdate, a.todate, zc5.code, a.ecolumn,
          pr.payday
      from persons p
          join employees e on (p.ref = e.person)
          join eabsences a on (a.employee = e.ref)
          left join epayrolls pr on (a.epayroll = pr.ref)
          left join ezusdata ez on (ez.person = p.ref and ez.fromdate = (select max(fromdate) from ezusdata where person = p.ref and fromdate < :atodate))
          left join edictzuscodes zc1 on (zc1.ref = a.bcode)
          left join edictzuscodes zc2 on (zc2.ref = ez.ascode)
          left join edictzuscodes zc3 on (zc3.ref = ez.retired)
          left join edictzuscodes zc4 on (zc4.ref = ez.invalidity)
          left join edictzuscodes zc5 on (zc5.ref = a.scode)
          left join eabsences ac on (ac.ref = a.corrected)
          left join epayrolls prc on (prc.ref = ac.epayroll)
        where a.bcode > 0
          and a.correction = 1
          and e.company = :company
          and (p.ref = :forperson or :forperson = 0)
          and ((a.ecolumn in (40, 50, 60, 90, 100, 110, 120, 140, 150, 160, 170, 270, 280, 290, 350, 360, 370, 440, 450) and (prc.iper = :period or (prc.ref is null and pr.iper = :period)))
            or (a.ecolumn in (10, 130, 190, 260, 300, 330, 390) and a.fromdate >= :afromdate and a.todate <= :atodate))
        order by e.sname, e.fname, a.fromdate
        into :person, :sname, :fname, :doctype, :docno, :icode, :bcode, :fromdate, :todate, :scode, :ecolumn, :payday
    do begin

      if (doctype = '') then doctype = '0';
      if (docno = '') then docno = '0';
      if (bcode not in ('151','152','111','121','122')) then
      begin
        --jezeli zostal zwolniony przed wyplata to kod 3000
        select max(todate) from (
            select max(coalesce(m.todate,:payday)) as todate
              from employment m
                join employees e on e.ref = m.employee
              where e.person = :person
              group by e.ref)
          into :empltodate;
    
        --jezeli wyplata nastapila w miesiacu nastepnym -> kod 3000
        execute procedure monthlastday(empltodate) returning_values empltodate;
        if (empltodate < payday) then
          icode = '3000'||substring(icode from 5 for 6);
      end
  
      if (counter = 0) then
      begin
        person_temp = person;
        sname_temp = sname;
        fname_temp =  fname ;
        doctype_temp = doctype;
        docno_temp = docno;
        icode_temp = icode;
        bcode_temp = bcode;
        fromdate_temp = fromdate;
        todate_temp = todate;
        scode_temp = scode;
        ecolumn_temp = ecolumn;
      end else
      begin
        if (person_temp = person and counter > 0 and icode_temp = icode
          and bcode_temp = bcode and coalesce(scode_temp,'') = coalesce(scode,'') 
          and (todate_temp + 1) >= :fromdate and ecolumn_temp = ecolumn
        ) then begin
          todate_temp = todate;
        end else
        begin
          person_temp2 = person;
          sname_temp2 = sname;
          fname_temp2 = fname;
          doctype_temp2 = doctype;
          docno_temp2 = docno;
          icode_temp2 = icode;
          bcode_temp2 = bcode;
          fromdate_temp2 = fromdate;
          todate_temp2 = todate;
          scode_temp2 = scode;
          ecolumn_temp2 = ecolumn;
          person = person_temp;
          sname = sname_temp;
          fname = fname_temp;
          doctype = doctype_temp;
          docno = docno_temp;
          icode = icode_temp;
          bcode = bcode_temp;
          fromdate = fromdate_temp;
          todate = todate_temp;
          scode = scode_temp;
          ecolumn = ecolumn_temp;
          rownumber = rownumber + 1;
          suspend;
  
          person_temp = person_temp2;
          sname_temp = sname_temp2;
          fname_temp = fname_temp2;
          doctype_temp = doctype_temp2;
          docno_temp = docno_temp2;
          icode_temp = icode_temp2;
          bcode_temp = bcode_temp2;
          fromdate_temp = fromdate_temp2;
          todate_temp = todate_temp2;
          scode_temp = scode_temp2;
          ecolumn_temp = ecolumn_temp2;
         end
       end
       counter = counter +1;
    end
    if (counter >= 1) then
    begin
      person = person_temp;
      sname = sname_temp;
      fname = fname_temp;
      doctype = doctype_temp;
      docno = docno_temp;
      icode = icode_temp;
      bcode = bcode_temp;
      fromdate = fromdate_temp;
      todate = todate_temp;
      scode = scode_temp;
      ecolumn = ecolumn_temp;
      rownumber = rownumber + 1;
      suspend;
    end
  end
end^
SET TERM ; ^
