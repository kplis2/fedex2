--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FAK_POZ_CHANGED(
      REFFAK integer,
      REFPOZ integer,
      OLD_GR_VAT varchar(5) CHARACTER SET UTF8                           ,
      POLD_GR_VAT varchar(5) CHARACTER SET UTF8                           )
   as
declare variable gr_vat varchar(5) ;
declare variable pgr_vat varchar(5) ;
declare variable is_nag integer;
declare variable wartnet numeric(14,2);
declare variable wartbru numeric(14,2);
declare variable wartnetzl numeric(14,2);
declare variable wartbruzl numeric(14,2);
declare variable ewartnet numeric(14,2);
declare variable ewartbru numeric(14,2);
declare variable ewartnetzl numeric(14,2);
declare variable ewartbruzl numeric(14,2);
declare variable vewartnetzl numeric(14,2);
declare variable vewartbruzl numeric(14,2);
declare variable cnt integer;
declare variable kornagfak integer=0;
declare variable zakup integer;
declare variable zaliczkowy integer;
declare variable kurs numeric(14,4);
declare variable korsamo smallint;
declare variable sad integer;
begin
  select N.REF, N.ZAKUP, N.ZALICZKOWY, N.KURS, T.KORSAMO, N.SAD from NAGFAK N
    join TYPFAK T on T.SYMBOL = N.typ
    where N.ref=:reffak into :is_nag, :zakup, :zaliczkowy, :kurs, :korsamo, :sad;
  if(:korsamo is null) then korsamo = 0;
  if(:sad is null) then sad = 0;
  if(exists(select ref from pozfak where dokument = :reffak and refk is not null)) then kornagfak = 1;
  if(:is_nag is null) then is_nag = 0;
  if (:is_nag =  0) then exit;

  if(:zaliczkowy is null) then zaliczkowy = 0;
  if(:kurs=0 or :kurs is null) then kurs = 1;
  select gr_vat, pgr_vat from POZFAK where POZFAK.REF = :REFPOZ into :gr_vat, :pgr_vat;
  if(((:gr_vat <> :old_gr_vat) or (:gr_vat is null) or (:gr_vat='')) and :old_gr_vat is not null and :old_gr_vat<>'') then begin
    /* zmienila sie stawka vat po korekcie */
    /*naliczenie pozycji w ROZFAK starej stawki */
    wartnet = null; wartbru = null; wartnetzl = null; wartbruzl = null;
    select sum(WARTNET), sum(wartbru), sum(WARTNETZL), sum(wartbruzl)
      from POZFAK
      where dokument = :reffak and gr_vat = :old_gr_vat and opk = 0
      into :wartnet, :wartbru, :wartnetzl, :wartbruzl;
    vewartnetzl = 0; vewartbruzl = 0;
    if(:wartnet is null) then wartnet = 0;
    if(:wartbru is null) then wartbru = 0;
    /* pozycje po korekcie przeliczamy na zlotowki z sumy waluty a nie z pojedynczych pozycji*/
    /* wyjatkiem jest SAD, gdzie kursy sa brane z PZI, wiec nie przeliczamy */
    if(:sad=0) then begin
      wartnetzl = :wartnet * :kurs;
      wartbruzl = :wartbru * :kurs;
    end
    if(:wartnetzl is null) then wartnetzl = 0;
    if(:wartbruzl is null) then wartbruzl = 0;
    ewartnet = 0; ewartbru = 0; ewartnetzl = 0; ewartbruzl = 0;
    /* jesli to korekta to wartosci walutowe i zlotowkowe bierzemy z faktury oryginalnej, biezacy kurs jest nieistotny*/
    if(:kornagfak > 0 and :zakup=1 and :wartnet<>0) then begin
      if(not exists(
           select ref from POZFAK where DOKUMENT=:reffak and pozfak.pgr_vat = :old_gr_vat and pozfak.opk = 0
              and ((pilosc <> ilosc) or (pozfak.pcenanet<> cenanet) or (pozfak.pgr_vat<>pozfak.gr_vat))
           )
      ) then begin
        select sum(R.sumwartnet), sum(r.SUMWARTBRU), sum(r.SUMWARTNETZL), sum(r.SUMWARTBRUZL)
          from ROZFAK R
          join nagfak n on (r.dokument = n.ref)
          where n.korekta = :is_nag and r.VAT=:old_gr_vat
        into :wartnet, :wartbru, :wartnetzl, :wartbruzl;
      end
    end
    if(:kornagfak>0 and :zaliczkowy=0 and :korsamo=0 ) then begin
       select sum(r.ESUMWARTNET), sum(r.ESUMWARTBRU), sum(r.ESUMWARTNETZL), sum(r.ESUMWARTBRUZL), sum(r.VESUMWARTNETZL), sum(VESUMWARTBRUZL)
         from ROZFAK R
         join nagfak n on (r.dokument = n.ref)
         where n.korekta = :is_nag and r.VAT=:old_gr_vat
       into :ewartnet, :ewartbru, :ewartnetzl, :ewartbruzl, :vewartnetzl, :vewartbruzl;
    end
-----------------------------------------------------
    /* jesli korekta samodzielna to nie siegamy do faktury pierwotnej */
    if(:korsamo>0 and :zaliczkowy=0) then begin -- PR09761
      select sum(PWARTNET), sum(PWARTBRU), sum(PWARTNETZL), sum(PWARTBRUZL)
      from POZFAK
         where dokument = :reffak and pgr_vat = :old_gr_vat and opk = 0
      into :ewartnet, :ewartbru, :ewartnetzl, :ewartbruzl;
    end
-----------------------------------------------------
    cnt = null; -- ile pozycji z grupa vat :old_gr_vat
    select count(*) from ROZFAK where dokument = :reffak and vat = :old_gr_vat into :cnt;

    if((:kornagfak>0 or :korsamo>0) and :zaliczkowy=0) then begin
      if(:cnt > 0) then
        update ROZFAK set
          sumwartnet   = :wartnet,  sumwartbru   = :wartbru,  sumwartnetzl   = :wartnetzl,  sumwartbruzl   = :wartbruzl,
          pesumwartnet = :ewartnet, pesumwartbru = :ewartbru, pesumwartnetzl = :ewartnetzl, pesumwartbruzl = :ewartbruzl,
          pvesumwartnetzl = :vewartnetzl, pvesumwartbruzl = :vewartbruzl
        where dokument = :reffak and vat = :old_gr_vat;
      else
        -- wart  -> S
        --       -> PS
        -- ewart -> PES
        insert into ROZFAK(DOKUMENT, VAT,
          SUMWARTNET,   SUMWARTBRU,   SUMWARTNETZL,   SUMWARTBRUZL,
          PSUMWARTNET,  PSUMWARTBRU,  PSUMWARTNETZL,  PSUMWARTBRUZL,
          PESUMWARTNET, PESUMWARTBRU, PESUMWARTNETZL, PESUMWARTBRUZL,
          PVESUMWARTNETZL, PVESUMWARTBRUZL)
        values (:reffak, :old_gr_vat,
          :wartnet,     :wartbru,     :wartnetzl,     :wartbruzl,
          0,            0,            0,              0,
          :ewartnet,    :ewartbru,    :ewartnetzl,    :ewartbruzl,
          :vewartnetzl,    :vewartbruzl);
    end else begin
      if(:cnt > 0) then
        update ROZFAK set sumwartnet = :wartnet, sumwartbru = :wartbru,sumwartnetzl = :wartnetzl, sumwartbruzl = :wartbruzl
        where dokument = :reffak and vat = :old_gr_vat;
      else
        insert into ROZFAK(DOKUMENT, VAT, SUMWARTNET, SUMWARTBRU, SUMWARTNETZL, SUMWARTBRUZL,
        PSUMWARTNET, PSUMWARTBRU, PSUMWARTNETZL, PSUMWARTBRUZL)
        values (:reffak, :old_gr_vat, :wartnet, :wartbru, :wartnetzl, :wartbruzl,0,0,0,0);
    end
  end
  if(((:pgr_vat <> :pold_gr_vat) or (:pgr_vat is null) or (:pgr_vat='')) and :pold_gr_vat is not null and :pold_gr_vat<>'') then begin
    /* gdy zmienila sie stawka vat przed korekta */
    /*naliczenie pozycji w ROZFAK starej stawki  - poprzedniej*/
    wartnet = null; wartbru = null; wartnetzl = null; wartbruzl = null;
    ewartnet = 0; ewartbru = 0; ewartnetzl = 0; ewartbruzl = 0;
    vewartnetzl = 0; vewartbruzl = 0;
    if(:kornagfak > 0 and :korsamo=0) then begin
       select sum(r.sumwartnet), sum(r.SUMWARTBRU), sum(r.SUMWARTNETZL), sum(r.SUMWARTBRUZL)
         from ROZFAK R
         join nagfak n on (r.dokument = n.ref)
         where n.korekta = :is_nag and r.VAT=:pold_gr_vat
       into :wartnet, :wartbru, :wartnetzl, :wartbruzl;
    end
    if(:kornagfak>0 and :zaliczkowy=0) then begin
       select sum(r.ESUMWARTNET), sum(r.ESUMWARTBRU), sum(r.ESUMWARTNETZL), sum(r.ESUMWARTBRUZL), sum(VESUMWARTNETZL), sum(VESUMWARTBRUZL)
         from ROZFAK R
         join nagfak n on (r.dokument = n.ref)
         where n.korekta = :is_nag and r.VAT=:pold_gr_vat
       into :ewartnet, :ewartbru, :ewartnetzl, :ewartbruzl, :vewartnetzl, :vewartbruzl;
    end
    if(:wartnet is null) then begin
      select sum(pWARTNET), sum(pwartbru),sum(pwartnetzl), sum(pwartbruzl)
      from POZFAK
          where dokument = :reffak and pgr_vat = :pold_gr_vat and opk = 0
      into :wartnet, :wartbru, :wartnetzl, :wartbruzl;
    end
    if(:wartnet is null) then wartnet = 0;
    if(:wartbru is null) then wartbru = 0;
    if(:wartnetzl is null) then wartnetzl = 0;
    if(:wartbruzl is null) then wartbruzl = 0;
-----------------------------------------------------
    if(:korsamo>0 and :zaliczkowy=0) then begin -- PR09761
      select sum(PWARTNET), sum(PWARTBRU), sum(PWARTNETZL), sum(PWARTBRUZL)
      from POZFAK
         where dokument = :reffak and pgr_vat = :pold_gr_vat and opk = 0
      into :ewartnet, :ewartbru, :ewartnetzl, :ewartbruzl;
    end
-----------------------------------------------------
    cnt = null;
    select count(*) from ROZFAK where dokument = :reffak and vat = :pold_gr_vat into :cnt;
    if((:kornagfak>0 or :korsamo>0) and :zaliczkowy=0) then begin
        --       -> S
        -- wart  -> PS
        -- ewart -> PES
      if(:cnt > 0) then
        update ROZFAK set
        psumwartnet  = :wartnet,  psumwartbru  = :wartbru,  psumwartnetzl  = :wartnetzl,  psumwartbruzl  = :wartbruzl,
        pesumwartnet = :ewartnet, pesumwartbru = :ewartbru, pesumwartnetzl = :ewartnetzl, pesumwartbruzl = :ewartbruzl,
        pvesumwartnetzl = :vewartnetzl, pvesumwartbruzl = :vewartbruzl
        where dokument = :reffak and vat = :pold_gr_vat;
      else
        insert into ROZFAK(DOKUMENT, VAT,
          SUMWARTNET,   SUMWARTBRU,   SUMWARTNETZL,   SUMWARTBRUZL,
          pSUMWARTNET,  pSUMWARTBRU,  pSUMWARTNETZL,  pSUMWARTBRUZL,
          PESUMWARTNET, PESUMWARTBRU, PESUMWARTNETZL, PESUMWARTBRUZL,
          PVESUMWARTNETZL, PVESUMWARTBRUZL)
        values (:reffak, :pold_gr_vat,
          0,            0,            0,              0,
          :wartnet,     :wartbru,     :wartnetzl,     :wartbruzl,
          :ewartnet,    :ewartbru,    :ewartnetzl,    :ewartbruzl,
          :vewartnetzl,    :vewartbruzl);
    end else begin
      if(:cnt > 0) then
        update ROZFAK set psumwartnet = :wartnet, psumwartbru = :wartbru, psumwartnetzl = :wartnetzl, psumwartbruzl = :wartbruzl
        where dokument = :reffak and vat = :pold_gr_vat;
      else
        insert into ROZFAK(DOKUMENT, VAT, pSUMWARTNET, pSUMWARTBRU, pSUMWARTNETZL, pSUMWARTBRUZL,
        SUMWARTNET, SUMWARTBRU, SUMWARTNETZL, SUMWARTBRUZL)
        values (:reffak, :pold_gr_vat, :wartnet, :wartbru, :wartnetzl, :wartbruzl,0,0,0,0);
    end
  end
  if(:gr_vat is not null and :gr_vat<>'') then begin
    /* gdy zmienila sie stawka vat po korekcie */
    /* naliczenie pozycji ROZFAK bieżącej grupy VAT */
    wartnet = null; wartbru = null; wartnetzl = null; wartbruzl = null;
    /* wez wartosci z biezacej faktury, zlotowki policz od sumy w walucie*/
    select sum(WARTNET), sum(wartbru), sum(WARTNETZL), sum(wartbruzl)
    from POZFAK
         where dokument = :reffak and gr_vat = :gr_vat and opk = 0
    into :wartnet, :wartbru, :wartnetzl, :wartbruzl;
    if(:wartnet is null) then wartnet = 0;
    if(:wartbru is null) then wartbru = 0;
    /* jesli jest SAD to kursy moga byc rozne na pozycjach wiec wez sume zlotowek z pozycji, w przeciwnym razie policz z sumy w walucie */
    if(:sad=0) then begin
      wartnetzl = :wartnet * :kurs;
      wartbruzl = :wartbru * :kurs;
    end
    if(:wartnetzl is null) then wartnetzl = 0;
    if(:wartbruzl is null) then wartbruzl = 0;
    /* przyjmij zerowe wartosci przed korekta */
    ewartnet = 0; ewartbru = 0; ewartnetzl = 0; ewartbruzl = 0;
    vewartnetzl = 0; vewartbruzl = 0;
    /* jesli to korekta i jest albo calosciowa albo pusta to wez wartosci walutowe i zlotowkowe z korekty*/
    if(:kornagfak > 0 and :zakup=1 and :wartnet<>0) then begin
      if(not exists(
           select ref from POZFAK where DOKUMENT=:reffak and pozfak.gr_vat = :gr_vat and pozfak.opk = 0
              and ((pilosc <> ilosc) or (pozfak.pcenanet<> cenanet) or (pozfak.pgr_vat<>pozfak.gr_vat) )
           )
      ) then begin
       select sum(r.sumwartnet), sum(r.SUMWARTBRU), sum(r.SUMWARTNETZL), sum(r.SUMWARTBRUZL)
         from ROZFAK R
         join nagfak n on (r.dokument = n.ref)
         where n.korekta = :is_nag and r.VAT=:gr_vat
       into :wartnet, :wartbru, :wartnetzl, :wartbruzl;
      end
    end
    /* policz wartosci przed korekta z faktury korygowanej */
    if(:kornagfak>0 and :zaliczkowy=0 and :korsamo=0) then begin
       select sum(r.ESUMWARTNET), sum(r.ESUMWARTBRU), sum(r.ESUMWARTNETZL), sum(r.ESUMWARTBRUZL), sum(VESUMWARTNETZL), sum(VESUMWARTBRUZL)
         from ROZFAK R
         join nagfak n on (r.dokument = n.ref)
         where n.korekta = :is_nag and r.VAT=:gr_vat
       into :ewartnet, :ewartbru, :ewartnetzl, :ewartbruzl, :vewartnetzl, :vewartbruzl;
    end
-----------------------------------------------------
    /* dla korekty samodzielnej wez wartosci przed korekta z faktury biezacej */
    if(:korsamo>0 and :zaliczkowy=0) then begin -- PR09761
      select sum(PWARTNET), sum(PWARTBRU), sum(PWARTNETZL), sum(PWARTBRUZL)
      from POZFAK
         where dokument = :reffak and pgr_vat = :gr_vat and opk = 0
      into :ewartnet, :ewartbru, :ewartnetzl, :ewartbruzl;
    end
-----------------------------------------------------
    cnt = null;
    select count(*) from ROZFAK where dokument = :reffak and vat = :gr_vat into :cnt;
    if((:kornagfak>0 or :korsamo>0) and :zaliczkowy=0) then begin
    -- wart  -> S
    --       -> PS
    -- ewart -> PES
      if(:cnt > 0) then begin
        update ROZFAK set
        sumwartnet   = :wartnet,  sumwartbru   = :wartbru,  sumwartnetzl   = :wartnetzl,  sumwartbruzl   = :wartbruzl,
        pesumwartnet = :ewartnet, pesumwartbru = :ewartbru, pesumwartnetzl = :ewartnetzl, pesumwartbruzl = :ewartbruzl,
        pvesumwartnetzl = :vewartnetzl, pvesumwartbruzl = :vewartbruzl
        where dokument  = :reffak and vat = :gr_vat;
      end else
        insert into ROZFAK(DOKUMENT, VAT,
          SUMWARTNET,   SUMWARTBRU,   SUMWARTNETZL,   SUMWARTBRUZL,
          PSUMWARTNET,  PSUMWARTBRU,  PSUMWARTNETZL,  PSUMWARTBRUZL,
          PESUMWARTNET, PESUMWARTBRU, PESUMWARTNETZL, PESUMWARTBRUZL,
          PVESUMWARTNETZL, PVESUMWARTBRUZL)
        values (:reffak, :gr_vat,
          :wartnet,     :wartbru,     :wartnetzl,     :wartbruzl,
          0,            0,            0,              0,
          :ewartnet,    :ewartbru,    :ewartnetzl,    :ewartbruzl,
          :vewartnetzl,    :vewartbruzl);
    end else begin
      if(:cnt > 0) then
        update ROZFAK set sumwartnet = :wartnet, sumwartbru = :wartbru,sumwartnetzl = :wartnetzl, sumwartbruzl = :wartbruzl
        where dokument = :reffak and vat = :gr_vat;
      else
        insert into ROZFAK(DOKUMENT, VAT, SUMWARTNET, SUMWARTBRU, SUMWARTNETZL, SUMWARTBRUZL,
        PSUMWARTNET, PSUMWARTBRU, PSUMWARTNETZL, PSUMWARTBRUZL)
        values (:reffak, :gr_vat, :wartnet, :wartbru, :wartnetzl, :wartbruzl,0,0,0,0);
    end
  end
  /* gdy zmienila sie stawka vat przed korekta */
  /* naliczenie pozycji ROZFAK bieżącej grupy VAT */
  if(:pgr_vat is not null and :pgr_vat<>'') then begin
    wartnet = null; wartbru = null; wartnetzl = null; wartbruzl = null;
    ewartnet = 0; ewartbru = 0; ewartnetzl = 0; ewartbruzl = 0;
    vewartnetzl = 0; vewartbruzl = 0;
    /* wartosci przed korekta walutowe i zlotowkowe bierz bezposrednio z rozfakow*/
    if(:kornagfak>0 and :korsamo=0) then begin
       select sum(r.sumwartnet), sum(r.SUMWARTBRU), sum(r.SUMWARTNETZL), sum(r.SUMWARTBRUZL)
--       from ROZFAK where DOKUMENT = :kornagfak and VAT=:pgr_vat
         from ROZFAK R
         join nagfak n on (r.dokument = n.ref)
         where n.korekta = :is_nag and r.VAT=:pgr_vat
       into :wartnet, :wartbru, :wartnetzl, :wartbruzl;
    end
    if(:kornagfak>0 and :zaliczkowy=0) then begin
       select sum(r.ESUMWARTNET), sum(r.ESUMWARTBRU), sum(r.ESUMWARTNETZL), sum(r.ESUMWARTBRUZL), sum(VESUMWARTNETZL), sum(VESUMWARTBRUZL)
--       from ROZFAK where DOKUMENT = :kornagfak and VAT=:pgr_vat
         from ROZFAK R
         join nagfak n on (r.dokument = n.ref)
         where n.korekta = :is_nag and r.VAT=:pgr_vat
       into :ewartnet, :ewartbru, :ewartnetzl, :ewartbruzl, :vewartnetzl, :vewartbruzl;
    end
    if(:wartnet is null or :korsamo>0) then begin
      select sum(pWARTNET), sum(pwartbru),sum(pwartnetzl), sum(pwartbruzl)
      from POZFAK
           where dokument = :reffak and pgr_vat = :pgr_vat and opk = 0
      into :wartnet, :wartbru, :wartnetzl, :wartbruzl;
    end
    if(:wartnet is null) then wartnet = 0;
    if(:wartbru is null) then wartbru = 0;
    if(:wartnetzl is null) then wartnetzl = 0;
    if(:wartbruzl is null) then wartbruzl = 0;
-----------------------------------------------------
    if(:korsamo>0 and :zaliczkowy=0) then begin -- PR09761
      select sum(PWARTNET), sum(PWARTBRU), sum(PWARTNETZL), sum(PWARTBRUZL)
      from POZFAK
         where dokument = :reffak and pgr_vat = :pgr_vat and opk = 0
      into :ewartnet, :ewartbru, :ewartnetzl, :ewartbruzl;
    end
-----------------------------------------------------
    cnt = null;
    select count(*) from ROZFAK where dokument = :reffak and vat = :pgr_vat into :cnt;
    if((:kornagfak>0 or :korsamo>0) and :zaliczkowy=0) then begin
        --       -> S
        -- wart  -> PS
        -- ewart -> PES
      if(:cnt > 0) then
        update ROZFAK set
        psumwartnet  = :wartnet,  psumwartbru  = :wartbru,  psumwartnetzl  = :wartnetzl,  psumwartbruzl  = :wartbruzl,
        pesumwartnet = :ewartnet, pesumwartbru = :ewartbru, pesumwartnetzl = :ewartnetzl, pesumwartbruzl = :ewartbruzl,
        pvesumwartnetzl = :vewartnetzl, pvesumwartbruzl = :vewartbruzl
        where dokument = :reffak and vat = :pgr_vat;
      else
        insert into ROZFAK(DOKUMENT, VAT,
          SUMWARTNET,   SUMWARTBRU,   SUMWARTNETZL,   SUMWARTBRUZL,
          pSUMWARTNET,  pSUMWARTBRU,  pSUMWARTNETZL,  pSUMWARTBRUZL,
          PESUMWARTNET, PESUMWARTBRU, PESUMWARTNETZL, PESUMWARTBRUZL,
          PVESUMWARTNETZL, PVESUMWARTBRUZL)
        values (:reffak, :pgr_vat,
          0,            0,            0,              0,
          :wartnet,     :wartbru,     :wartnetzl,     :wartbruzl,
          :ewartnet,    :ewartbru,    :ewartnetzl,    :ewartbruzl,
          :vewartnetzl, :vewartbruzl);
    end else begin
      if(:cnt > 0) then
        update ROZFAK set psumwartnet = :wartnet, psumwartbru = :wartbru, psumwartnetzl = :wartnetzl, psumwartbruzl = :wartbruzl
        where dokument = :reffak and vat = :pgr_vat;
      else
        insert into ROZFAK(DOKUMENT, VAT, pSUMWARTNET, pSUMWARTBRU, pSUMWARTNETZL, pSUMWARTBRUZL,
        SUMWARTNET, SUMWARTBRU, SUMWARTNETZL, SUMWARTBRUZL)
        values (:reffak, :pgr_vat, :wartnet, :wartbru, :wartnetzl, :wartbruzl,0,0,0,0);
    end
  end
  delete from ROZFAK where dokument=:reffak
    and sumwartnet=0 and sumwartbru=0
    and psumwartnet=0 and psumwartbru=0
    and esumwartnet=0 and esumwartbru=0
    and pesumwartnet=0 and pesumwartbru=0
    and sumwartnetzl=0 and sumwartbruzl=0
    and psumwartnetzl=0 and psumwartbruzl=0
    and esumwartnetzl=0 and esumwartbruzl=0
    and pesumwartnetzl=0 and pesumwartbruzl=0;
  execute procedure FAK_OBL_NAG(:reffak);
end^
SET TERM ; ^
