--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BROWSE_DECREEMATCHINGS_4_DECREE(
      DECREE DECREES_ID)
  returns (
      DECREEMATCHINGREF DECREEMATCHINGS_ID,
      BKDOCSYMBOL STRING20,
      ACCOUNT ACCOUNT_ID,
      SLODEF SLO_ID,
      SLOPOZ SLOPOZ_ID,
      SLOKOD STRING40,
      SLONAZWA STRING,
      MATCHINGSYMBOL STRING40,
      CREDIT CT_AMOUNT,
      DEBIT DT_AMOUNT,
      STATUS SMALLINT_ID)
   as
declare variable decreematchinggroup decreematchings_id;
begin
  -- najpierw pokazujemy nierozliczone
  for
    select m.ref, b.symbol, d.account, m.slodef, m.slopoz, m.slokod,
        m.slonazwa, d.matchingsymbol, m.credit, m.debit
      from decreematchings m
        left join decrees d on (d.ref = m.decree)
        left join bkdocs b on (b.ref = d.bkdoc)
      where m.decree = :decree and m.decreematchinggroup is null
      into :decreematchingref, :bkdocsymbol, :account, :slodef, :slopoz, :slokod,
        :slonazwa, :matchingsymbol, :credit, :debit
  do begin
    status = 0;
    suspend;
  end
  -- potem pokazujemy rekordy z rozliczenia
  for
    select distinct m.decreematchinggroup
      from decreematchings m
      where m.decree = :decree
      into :decreematchinggroup
  do begin
    for
      select m.ref, b.symbol, d.account, m.slodef, m.slopoz, m.slokod,
          m.slonazwa, d.matchingsymbol, m.credit, m.debit, coalesce(g.status,0)
        from decreematchings m
          left join decrees d on (d.ref = m.decree)
          left join bkdocs b on (b.ref = d.bkdoc)
          left join decreematchinggroups g on (g.ref = m.decreematchinggroup)
        where m.decreematchinggroup = :decreematchinggroup
        into :decreematchingref, :bkdocsymbol, :account, :slodef, :slopoz, :slokod,
          :slonazwa, :matchingsymbol, :credit, :debit, :status
    do begin
      suspend;
    end
  end
end^
SET TERM ; ^
