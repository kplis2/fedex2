--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE UPDATE_DECREEMATCHING(
      DECREE DECREES_ID,
      CREDIT CT_AMOUNT,
      DEBIT DT_AMOUNT)
   as
declare variable mref decreematchings_id;
declare variable coramount money;
declare variable mcredit ct_amount;
declare variable mdebit dt_amount;
begin
  if (:credit > 0 or :debit > 0) then
    execute procedure insert_decreematching(:decree, :credit, :debit);
  else if (:credit < 0) then
  begin
    coramount = -:credit;
    for
      select m.ref, m.credit
        from decreematchings m
        where m.decree = :decree and m.credit > 0
        order by m.decree, m.decreematchinggroup, m.credit
        into :mref, :mcredit
    do begin
      if (:mcredit <= :coramount and :coramount > 0) then
      begin
        -- obsuga kasowania rozliczonych jest na trigerach
        delete from decreematchings m where m.ref = :mref;
        coramount = coramount - mcredit;
      end else if (:coramount > 0) then
      begin
        -- obsluga zmniejszenia kwoty gdy jest rozliczony to na trigerach
        update decreematchings m set m.credit = m.credit - :coramount where m.ref = :mref;
        coramount = 0;
      end
      if (:coramount <= 0) then
        break;
    end
  end
  else if (:debit < 0) then
  begin
    coramount = -:debit;
    for
      select m.ref, m.debit
        from decreematchings m
        where m.decree = :decree and m.debit > 0
        order by m.decree, m.decreematchinggroup, m.debit
        into :mref, :mdebit
    do begin
      if (:mdebit <= :coramount and :coramount > 0) then
      begin
        -- obsuga kasowania rozliczonych jest na trigerach
        delete from decreematchings m where m.ref = :mref;
        coramount = coramount - mdebit;
      end else if (:coramount > 0) then
      begin
        -- obsluga zmniejszenia kwoty gdy jest rozliczony to na trigerach
        update decreematchings m set m.debit = m.debit - :coramount where m.ref = :mref;
        coramount = 0;
      end
      if (:coramount <= 0) then
        break;
    end
  end
end^
SET TERM ; ^
