--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE UNREG_SETTLEMENT(
      DREF integer)
   as
begin
  delete from rozrachp where decrees = :dref and skad = 6;
  update rozrachp set decrees = null, bkdoc = null
    where decrees = :dref;
end^
SET TERM ; ^
