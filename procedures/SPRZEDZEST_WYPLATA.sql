--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SPRZEDZEST_WYPLATA(
      SPRZEDZEST integer,
      KWOTAWYP numeric(14,2),
      OPERACJA varchar(20) CHARACTER SET UTF8                           ,
      TRESC varchar(40) CHARACTER SET UTF8                           )
   as
declare variable slodef integer;
declare variable cnt integer;
declare variable rozrach varchar(100);
declare variable skrot varchar(20);
declare variable data timestamp;
declare variable sprzedawca integer;
begin
  execute procedure GETCONFIG('OBSSPRZEDROZRACH') returning_values :rozrach;
  if(:rozrach is null or (:rozrach = ''))then rozrach = '0';
  if(:rozrach = '1') then begin
      select ref from SLODEF where TYP = 'SPRZEDAWCY' into :slodef;
      if(:slodef is null) then exception SLODEF_BEZSPRZEDAWCY;
      select SPRZEDAWCA, DATA from SPRZEDZEST where REF=:sprzedzest into :sprzedawca, :data;
      select SYMBFAK from ROZRACH where SPRZEDZESTID=:sprzedzest into :skrot;
      insert into ROZRACHP(SLODEF, SLOPOZ,KLIENT,SYMBFAK,OPERACJA,FAKTURA,DATA,TRESC,WINIEN,MA,WALUTA,KURS, SKAD)
        values(:slodef,:sprzedawca,NULL,:skrot,:operacja,NULL,current_date,:tresc,:kwotawyp,0,NULL, 1,0);
  end else
    update SPRZEDZEST set WYPLACONO = WYPLACONO+:kwotawyp where REF=:sprzedzest;
end^
SET TERM ; ^
