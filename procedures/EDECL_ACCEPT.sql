--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDECL_ACCEPT(
      DECLREF integer)
  returns (
      LOGMSG varchar(8191) CHARACTER SET UTF8                           )
   as
declare variable STATE integer;
declare variable ISGROUP integer;
declare variable SCODE varchar(20);
declare variable NEWREF integer;
declare variable PERS varchar(200);
declare variable DECLGROUP integer;
declare variable STATUS integer;
declare variable MSG varchar(1024) = '';
declare variable GROUPNAME varchar(40);
declare variable PERSMSG varchar(255);
declare variable EYEAR integer;
declare variable cnt_allpos integer;
declare variable cnt_editpos integer;
declare variable isaccept smallint;
declare variable GROUPSTATE smallint;
begin
  logmsg = '';

  select d.state, df.isgroup, df.systemcode, coalesce(p.person,''), d.declgroup
       , d.status, coalesce(d.groupname,''), d.eyear
    from edeclarations d
      join edecldefs df on df.ref = d.edecldef
      left join persons p on d.person = p.ref
    where d.ref = :declref
  into state, isgroup, scode, pers, declgroup, status, groupname, eyear;

  if (declgroup > 0) then
    select state, coalesce(groupname,ref) from edeclarations where ref = :declgroup
      into :groupstate, :groupname;
  else
    groupstate = state;

  if (pers <> '') then persmsg = pers;
  else if (groupname <> '') then persmsg = groupname;
  else persmsg = eyear;

  persmsg = coalesce(' ' || scode || ' : ' || persmsg || ' - ', '');

  if (status = 200) then
    logmsg = ascii_char(215) || persmsg || 'deklaracja jest już zatwierdzona przez Ministerstwo Finansów.';
  else if(state > 0) then
    logmsg = ascii_char(164) || persmsg || 'deklaracja jest już zatwierdzona';
  else if (isgroup = 0) then
  begin
    execute procedure edecl_check_required_fields(:declref)
      returning_values msg;

    if (msg <> '') then
      logmsg = ascii_char(215)|| persmsg || 'nie udało się zatwierdzić deklaracji
   ' || msg;
  end
  else if (isgroup = 1) then
  begin

    select count(*), sum(case when state = 0 then 1 else 0 end)
      from edeclarations
      where declgroup = :declref
      into :cnt_allpos, cnt_editpos;

    if (cnt_allpos = 0) then
     logmsg = ascii_char(215)|| persmsg || 'nie można zatwierdzić deklaracji zbiorczej bez pozycji';
    else if (cnt_editpos > 0) then
    begin
      for
        select e.ref, p.person from edeclarations e
          join persons p on (e.person = p.ref)
          where e.declgroup = :declref and e.state = 0
          into :newref, :pers
      do begin
        execute procedure edecl_check_required_fields(:newref)
          returning_values msg;

        if (msg <> '') then begin
          logmsg = logmsg || '
  > '                     || pers || '
         '                || msg;
        end else begin
          update edeclarations set state = 1 where ref = :newref;
          isaccept = 1;
        end
      end

      if (logmsg <> '') then
        logmsg = ascii_char(215)|| persmsg || 'nie udało się ' || trim (leading ' ' from iif(isaccept = 1, 'w pełni ', '')) || 'zatwierdzić deklaracji' || logmsg;
    end
  end

  if (logmsg = '') then
  begin
    update edeclarations set state = 1 where ref = :declref;
    logmsg = ascii_char(149)|| persmsg || 'zatwierdzono deklarację';
    suspend;
  end

  when any do
  begin
    logmsg = ascii_char(215) || persmsg || 'nie udało się zatwierdzić deklaracji dla: ' || pers;
    suspend;
  end

end^
SET TERM ; ^
