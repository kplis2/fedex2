--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XP_SUM(
      KEY1 integer,
      KEY2 integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(16,6))
   as
begin
  ret = 0;
  select sum(coalesce(CVALUE,0)) from PRCALCCOLS where PARENT=:key2 and CALCTYPE=0 into :ret;
  if(:ret is null) then ret = 0;
end^
SET TERM ; ^
