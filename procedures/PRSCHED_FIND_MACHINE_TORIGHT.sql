--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_FIND_MACHINE_TORIGHT(
      SCHEDOPER integer,
      TEND timestamp,
      TRYB smallint)
  returns (
      STARTTIME timestamp,
      ENDTIME timestamp,
      MACHINE integer)
   as
declare variable defmachine integer;
declare variable workplace varchar(20);
declare variable schedule integer;
declare variable schedzamnum integer;
declare variable gapfound smallint;
declare variable localtime timestamp;
declare variable schedzam integer;
declare variable worktime double precision;
declare variable cnt integer;
declare variable minspace integer;
declare variable minlength double precision;
declare variable prdepart varchar(20);
declare variable tmpendtime timestamp;
declare variable s1 timestamp;
declare variable s2 timestamp;
declare variable s3 timestamp;
declare variable m1 integer;
begin
  select po.MACHINE, po.worktime, ph.workplace, p.mintimebetweenopers, p.prdepart
    from PRSCHEDOPERS po
      join prshopers  ph on (po.shoper = ph.ref)
      join prschedzam  pz on (pz.ref = po.schedzam)
      join prschedules p on (p.ref = pz.prschedule)
    where po.ref=:schedoper
    into :machine, :worktime, :workplace, :minspace, :prdepart;

  minlength = 1440;
  minlength = coalesce(minspace,0)/:minlength;
  --minlength = 1/:minlength;
  --minlength = :minlength * :minspace;

  starttime = :tend;
  endtime = :tend;
  defmachine = :machine;--sprawdzenie, czy maszyna juz jest przypisana

  /*if(:defmachine is null) then begin
    --sprawdzenie, czy dla tego stanowiska byla juz wyboerana maszyna w ramach zlecenia
    --  - jesli tak, to zachowanie tej samej maszyny
    select max(m.ref)
      from PRMACHINES m
        join PRSCHEDOPERS po on (po.machine = m.ref)
      where po.schedzam = :schedzam
        and m.prworkplace = :workplace
     into :defmachine;
  end*/

  for
    select p.rmachine, p.rstart, coalesce(p.rend, :tend)
      from prsched_free_machine(:workplace, :machine, :tryb) p
      where (cast(:tend - P.rstart as double precision) >= :worktime )
      order by coalesce(p.rend, :tend) desc
      into :m1, :s1, :s2
  do begin
    --przesuniecie konca na dzien pracujacy
    execute procedure prsched_calendar_fw_tr(:prdepart,cast(:s2 as timestamp))
      returning_values :s3;

    if (cast(:s3 - :s1 as double precision) - :minlength >= :worktime) then
    begin
      -- przesuniecie poczatku w zwiazku z wolnym przypadajcym w czasie produkcji
      execute procedure pr_calendar_checkendtime_tr(:prdepart, :s3, :worktime)
        returning_values :tmpendtime;
      if (:tmpendtime <= :s3 ) then
      begin
        s1 = :tmpendtime;
      end
    end
    endtime = s3;
    starttime = s1;
    machine = m1;
    suspend;
  end
end^
SET TERM ; ^
