--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POTWIERDZENIA_GEN(
      DATA date,
      NOTATYP varchar(5) CHARACTER SET UTF8                           ,
      FORACCOUNT ACCOUNT_ID,
      FORSLODEF integer,
      FORSLOPOZ integer,
      COMPANY integer,
      EMPTYBALANCE smallint,
      DEBTONLY smallint,
      DATAZEROWE date)
  returns (
      STATUS integer,
      GENNUMBER integer)
   as
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable KONTOFK KONTO_ID;
declare variable SYMBFAK varchar(20);
declare variable DATAOTW timestamp;
declare variable DATAPLAT timestamp;
declare variable REFNOTA integer;
declare variable WN numeric(15,2);
declare variable MA numeric(15,2);
declare variable WNZL numeric(15,2);
declare variable MAZL numeric(15,2);
declare variable CNT integer;
declare variable DORECORD smallint;
declare variable KLISLO integer;
declare variable DOSTSLO integer;
declare variable NEWSLOPOZ integer;
declare variable FAKTORING smallint;
declare variable REFNAGFAK integer;
declare variable LASTYEAR integer;
declare variable CURRENCY varchar(3);
declare variable saldo numeric(15,2);
begin
 --exception test_break ''||:data||' '||:notatyp||' '||:foraccount||' '||:forslodef||' '||:forslopoz||' '||:company||' '||:emptybalance||' '||:debtonly;
  if (emptybalance is null) then emptybalance = 0;
  if (debtonly is null) then debtonly = 0;
  status = 0;
  gennumber = 0;
  select ref from slodef where TYP='KLIENCI' into :klislo;
  select ref from slodef where TYP='DOSTAWCY' into :dostslo;
  for
    select SLODEF, SLOPOZ, kontofk, cast(max(extract(year from datazamklastoper)) as integer)
      from ROZRACH
        where company = :company
          and dataotw < :data + 1
          and ((:forslodef is null) or (:forslodef = 0) or (slodef = :forslodef))
          and ((:forslopoz is null) or (:forslopoz = 0) or (slopoz = :forslopoz))
          and ((:foraccount is null) or (:foraccount = '') or (kontofk like :foraccount))
          and ((:emptybalance = 0 and (datazamklastoper >= :data + 1 or datazamklastoper is null)) or (:emptybalance = 1))
        group by slodef, slopoz, kontofk
        into :slodef, :slopoz, :kontofk, :lastyear
  do begin
     -- exception test_break;
    --sprawdzenie, czy dokument tego dnia już istnieje
    dorecord = 1;
    refnota = 0;
    cnt = null;
    if (exists(select first 1 n.ref from NOTYNAG n
                 left join notypoz p on (n.ref = p.dokument)
                 where n.notakind = 2 and n.company = :company
                 and n.slodef = :slodef and n.slopoz = :slopoz
                 and p.account = :kontofk
                 and cast(n.data as date) = cast(:data as date))
    ) then
      dorecord = 0;
    -- exception test_break''||dorecord;
    if (dorecord = 1) then
    begin
   -- exception test_break''||dorecord;
      -- wystawienie naglówka dokumentu
      execute procedure GEN_REF('NOTYNAG') returning_values :refnota;
      insert into NOTYNAG(REF, NOTAKIND, NOTATYP, SLODEF, SLOPOZ, DATA, STATUS, company)
        values(:refnota, 2,:notatyp, :slodef, :slopoz, :data, 0, :company);

      -- przegląd rozrachunków danego podmiotu
      for
        select SYMBFAK, KONTOFK, dataotw, dataplat, faktura, waluta, saldo
          from ROZRACH
            where SLODEF = :slodef and slopoz = :slopoz and company = :company
              and ((:emptybalance = 0 and (datazamklastoper >= :data + 1 or datazamklastoper is null)) or (:emptybalance = 1))
              and dataotw < :data + 1
              and ((:foraccount is null) or (:foraccount = '') or (kontofk like :foraccount))
            into :symbfak, :kontofk, :dataotw, :dataplat, :refnagfak, :currency, :saldo
      do begin
        -- analiza pojedynczego rozrachunku
        faktoring = 0;
        if (refnagfak is not null and refnagfak <> 0) then
          select FAKTORING from NAGFAK where REF = :refnagfak into :faktoring;

        select sum(P.winien), sum(P.ma), sum(P.winienzl), sum(P.mazl)
          from ROZRACHP P
            left join decrees D on (P.decrees = D.ref)
          where P.kontofk = :kontofk and P.symbfak = :symbfak and P.slodef = :slodef
            and P.slopoz = :slopoz and P.company = :company
            and (cast(D.transdate as date)<=cast(:data as date) or D.transdate is null)
            and P.data < :data + 1
            and (:saldo > 0 or (:saldo=0 and p.data > :datazerowe))
          into :wn, :ma, :wnzl, :mazl;

        if (wn is null) then wn = 0;
        if (ma is null) then ma = 0;
        if (wnzl is null) then wnzl = 0;
        if (mazl is null) then mazl = 0;

        if (wn > 0 and ma > 0 and ma > wn) then
        begin
          ma = ma;
          wn = wn;
          mazl = mazl;
          wnzl = wnzl;
        end

        if (wn > 0 and ma > 0 and ma < wn) then
        begin
          wn = wn;
          ma = ma;
          wnzl = wnzl;
          mazl = mazl;
        end

        if ((wn <> 0 or ma <> 0) and ((:debtonly = 0) or (:debtonly = 1 and (wn - ma) > 0) )) then
        begin
          -- zapis pozycji potwierdzenia
          insert into NOTYPOZ(DOKUMENT, ACCOUNT, SETTLEMENT,DATASTL, DATAPAY, BDEBIT, BCREDIT, FAKTORING, currency, bdebitzl, bcreditzl)
            values(:refnota, :kontofk, :symbfak, :dataotw, :dataplat, :wn, :ma, :faktoring, :currency, :wnzl, :mazl);
        end
      end

      -- generowanie pustego wpisu dla zerowych sald
       --if(coalesce(:lastyear,0)<>0 and :lastyear =cast(extract(year from :data) as integer)) then
       -- exception test_break ''||:lastyear||' '||:data;
      if (not exists(select ref from notypoz where dokument = :refnota) and :lastyear =cast(extract(year from :data) as integer)) then
        insert into NOTYPOZ(DOKUMENT, ACCOUNT, SETTLEMENT,DATASTL, DATAPAY, BDEBIT, BCREDIT, FAKTORING, currency, bdebitzl, bcreditzl)
          values(:refnota, :kontofk, 'brak rozrachunków', null, null, 0, 0, :faktoring, :currency, 0, 0);

      if (slodef = klislo) then
      begin
        -- dla kartoteki klientów sprawdzenie dostawcy powiązanego
        newslopoz = null;
        select KLIENCI.dostawca from KLIENCI where ref = :slopoz into :newslopoz;
        slopoz = :newslopoz;
        slodef = :dostslo;
      end
      else if (slodef = dostslo) then
      begin
        -- dla kartoteki dostawców sprawdzenie klienta powiązanego
        newslopoz = null;
        select max(klienci.ref) from KLIENCI where dostawca = :slopoz into :newslopoz;
        slopoz = :newslopoz;
        slodef = :klislo;
      end else
      begin
        slodef = 0;
        slopoz = 0;
      end

      -- generowanie not dla podmiotu powiazanej kartoteki
      if (slodef > 0 and slopoz > 0) then
      begin
        -- sprawdzenie, czy nota w danym dniu nie byla już wystawiona
        if (exists(select first 1 n.ref from NOTYNAG n
                     left join notypoz p on (n.ref = p.dokument)
                     where n.notakind = 2 and n.company = :company
                       and p.account = :kontofk
                       and n.slodef = :slodef and n.slopoz = :slopoz
                       and cast(n.data as date) = cast(:data as date))
        ) then
          dorecord = 0;

        if (dorecord = 1) then
        -- przegląd rozrachunków danego podmiotu
        for
          select SYMBFAK, KONTOFK, dataotw, dataplat, faktura, waluta
            from ROZRACH
              where SLODEF = :slodef and slopoz = :slopoz
                and ((:emptybalance = 1) or (rozrach.datazamklastoper is null) or  (rozrach.datazamklastoper >= :data + 1))
                and rozrach.dataotw < :data + 1 and company = :company
                and ((:foraccount is null) or (:foraccount = '') or (kontofk like :foraccount))
              into :symbfak, :kontofk, :dataotw, :dataplat, :refnagfak, :currency
        do begin
          -- analiza pojedynczego rozrachunku
          faktoring = 0;
          if (:refnagfak is not null and :refnagfak <> 0) then
            select FAKTORING
              from NAGFAK
              where REF = :refnagfak
              into :faktoring;
          select sum(winien), sum(ma), sum(winienzl), sum(mazl)
            from ROZRACHP P
              left join decrees D on (P.decrees = D.ref)
            where kontofk = :kontofk
              and symbfak = :symbfak and slodef = :slodef and slopoz = :slopoz
              and data < :data +1 and P.company = :company --and D.company = :company
              and (D.transdate < :data + 1 or D.transdate is null)
            into :wn, :ma, :wnzl, :mazl;

          if (wn is null) then wn = 0;
          if (ma is null) then ma = 0;
          if (wnzl is null) then wnzl = 0;
          if (mazl is null) then mazl = 0;
          if (wn > 0 and ma > 0 and ma > wn) then
          begin
            ma = ma;
            wn = wn;
            mazl = mazl;
            wnzl = wnzl;
          end
          if (wn > 0 and ma > 0 and ma < wn) then
          begin
            wn = wn;
            ma = ma;
            wnzl = wnzl;
            mazl = mazl;
          end

          if ((wn <> 0 or ma <> 0) and ((:debtonly = 0) or (:debtonly = 1 and (wn - ma) > 0) )) then
          begin
            -- zapis pozycji potwierdzenia gdy istnieje saldo
            insert into NOTYPOZ(DOKUMENT, ACCOUNT, SETTLEMENT,DATASTL, DATAPAY, BDEBIT, BCREDIT, FAKTORING, currency, bdebitzl, bcreditzl )
              values(:refnota, :kontofk, :symbfak, :dataotw, :dataplat, :wn, :ma, :faktoring, :currency, :wnzl, :mazl);
          end
        end
      -- generowanie pustego wpisu dla zerowych sald
      --if(coalesce(:lastyear,0)<>0 and :lastyear =cast(extract(year from :data) as integer)) then
       -- exception test_break ''||:refnota||''||:lastyear||''||:data;
      if (not exists(select ref from notypoz where dokument = :refnota) and :lastyear =cast(extract(year from :data) as integer)) then
        insert into NOTYPOZ(DOKUMENT, ACCOUNT, SETTLEMENT,DATASTL, DATAPAY, BDEBIT, BCREDIT, FAKTORING, currency, bdebitzl, bcreditzl)
          values(:refnota, :kontofk, 'brak rozrachunków', null, null, 0, 0, :faktoring, :currency,0,0);
      end

      -- usunicie dokumentu jesli nie ma pozycji
      if (refnota > 0) then
      begin
        if (not exists(select ref from notypoz where dokument = :refnota) or (:dorecord = 0)) then
        begin
          delete from NOTYNAG where ref = :refnota;
          refnota = 0;
        end
      end
      if (refnota > 0) then
        -- ustawienie nr kolejnego potwierdzenia
        gennumber = :gennumber + 1;
    end
  end
  status = 1;
end^
SET TERM ; ^
