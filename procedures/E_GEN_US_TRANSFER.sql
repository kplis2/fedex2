--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GEN_US_TRANSFER(
      US integer,
      TPER char(6) CHARACTER SET UTF8                           ,
      TDATE date,
      BANKACC varchar(10) CHARACTER SET UTF8                           ,
      CURRENTCOMPANY integer)
  returns (
      COUNTER smallint)
   as
declare variable dictdef integer;
declare variable toacc varchar(60);
declare variable amount numeric(14,2);
declare variable descript varchar(255);
declare variable descript1 varchar(6);
declare variable descript2 varchar(6);
declare variable toadress varchar(255);
declare variable address varchar(255);
declare variable postcode varchar(10);
declare variable city varchar(20);
declare variable towho varchar(255);
declare variable przelewtype varchar(10);
begin
  counter = 0;
  select ref
    from slodef
    where nazwa = 'Urzędy skarbowe'
    into :dictdef;  

  descript = 'Zaliczka na podatek dochodowy za okres ' ||
    substring(tper from 5 for 2) || '/' || substring(tper from 1 for 4);

  descript1 = substring(tper from 3 for 2)||'M'|| substring(tper from 5 for 2);
  descript2 = 'PIT4';

  select account
    from bankaccounts
    where dictdef = :dictdef and dictpos = :us and gl=1
    into :toacc;
  if (toacc is null) then exception SLOBANACC_EMPTYACC;

  select sum(P.pvalue)
  from epayrolls PR
    join eprpos P on (P.payroll = PR.ref)
    where PR.tper = :tper and P.ecolumn >= 7350 and P.ecolumn <= 7370
       and PR.company = :currentcompany
    into :amount;


  select name, address, city, postcode
    from einternalrevs
    where ref = :us
    into :towho, :address, :city, :postcode;

  toadress = coalesce(address,'') || '; ' || coalesce(postcode,'') || ' ' || coalesce(city,'');

  if (tdate < current_timestamp(0)) then
    tdate = current_date;

  execute procedure getconfig('PRZELEWPOD') returning_values :przelewtype;

  insert into BTRANSFERS (BANKACC, typ, BTYPE, slodef, slopoz, data,
      towho, toacc, toadress, todescript, todescript1, todescript2, curr, amount, autobtransfer, company)
    values (:bankacc, 1, :przelewtype, :dictdef, :us, :tdate,
      :towho, :toacc, :toadress, :descript, :descript1, :descript2, 'PLN', :amount, 1, :currentcompany);
  counter = counter + 1;
  suspend;
end^
SET TERM ; ^
