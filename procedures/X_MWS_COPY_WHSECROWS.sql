--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_COPY_WHSECROWS(
      WHSECROWS_REF INTEGER_ID,
      ILOSC INTEGER_ID,
      SYMBOL STRING)
   as
declare variable symbol_old string3;
declare variable symbol_new string3;
declare variable symbol_int integer_id;
declare variable new_whsecrows_ref integer_id;
declare variable i integer_id;
begin
--  exception universal ' WHSECROWS_REF '||WHSECROWS_REF||
--   ' ILOSC '||ILOSC||' symbol '||symbol;

  i = 0;
    if ( coalesce(ilosc,0) > 0 ) then
    begin
        while ( i < ilosc ) do begin
            if (coalesce(symbol, '') = '') then begin
              select first 1 wr.symbol from whsecrows wr where wr.ref = :whsecrows_ref
                  into :symbol_old;
              symbol_int = cast ( symbol_old as integer_id);
              symbol_int = symbol_int + 1;
              symbol_new = symbol_int;
              symbol_new = lpad (symbol_new,2,'0');
            end else begin
              symbol_new = symbol;
              symbol_new = lpad (symbol_new,2,'0');

            end
            insert into WHSECROWS ( WH, WHSEC, SYMBOL, ROWTYPE, L, W,  ORD, whsecdict,
                                 GOODAERANUMSTART, NOGOODASSOC, PENALTY, ENDSCOORDS, X_TYPE)
            select  WH, WHSEC, :symbol_new, ROWTYPE, L, W,  ORD, '',
                 GOODAERANUMSTART, NOGOODASSOC, PENALTY, ENDSCOORDS, X_TYPE
                from WHSECROWS wr
                where wr.ref = :whsecrows_ref
                returning ref into :new_whsecrows_ref;
            
            insert into MWSSTANDS ( WH, WHSEC, STORIESQUANT, LOADCAPACITY, MWSSTANDTYPE, L, W, WHSECROW, ORD)
            select  WH, WHSEC,  STORIESQUANT, LOADCAPACITY, MWSSTANDTYPE, L, W, :new_whsecrows_ref, ORD
                from MWSSTANDS sd
                where sd.whsecrow = :whsecrows_ref;
            i = i + 1;
            whsecrows_ref = new_whsecrows_ref;
        end
    end
end^
SET TERM ; ^
