--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_NUMBER_BKDOCS(
      COMPANY integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      BKREG varchar(10) CHARACTER SET UTF8                           )
   as
declare variable REF integer;
declare variable NUMBER integer;
declare variable TRANSDATE_Z timestamp;
declare variable TRANSDATE_P timestamp;
declare variable YEARNUM smallint;
begin
  if (bkreg = '') then exception universal 'Nie wybrano rejestru ksiegowego!';
  select first 1 transdate
    from bkdocs
    where company = :company
      and substring(period from 1 for 4)=substring(:period from 1 for 4) and bkreg = :bkreg
      and status = 3
    order by transdate desc, number desc
  into :transdate_z;

  select first 1 transdate
    from bkdocs
    where company = :company
      and substring(period from 1 for 4)=substring(:period from 1 for 4) and bkreg = :bkreg
      and status <> 3
    order by transdate asc, number asc
  into :transdate_p;

  if(:transdate_z > :transdate_p) then
    exception BKDOCS_ZAKSIEGOWANY 'Nie można przenumerować dokumentów w okresie, w którym są dokumenty zaksięgowane końcowo.';
  select yearnum
    from bkregs
    where symbol=:bkreg
      and company = :company
  into :yearnum;
  if (yearnum = 1) then
  begin
      select first 1 number
        from bkdocs
          where company = :company
            and substring(period from 1 for 4)=substring(:period from 1 for 4)
            and bkreg = :bkreg
            and status <> 3
        order by number asc
      into :number;
      for
        select ref
          from bkdocs
          where company = :company
            and substring(period from 1 for 4)=substring(:period from 1 for 4)
            and bkreg = :bkreg
            and status <> 3 and transdate >= coalesce(:transdate_z,:transdate_p)
          order by transdate, number
        into :ref
      do begin
        update bkdocs set oldnumber=number, number=:number where ref = :ref;
        number = number +1;
      end
  end else
  begin
      select first 1 number
        from bkdocs
        where company = :company
          and period=:period and bkreg = :bkreg
          and status <> 3
        order by number asc
      into :number;
      for
        select ref
          from bkdocs
          where company = :company
            and period=:period
            and bkreg = :bkreg
            and status <> 3
            and transdate >= coalesce(:transdate_z,:transdate_p)
          order by transdate, number
        into :ref
      do begin
        update bkdocs set oldnumber=number, number=:number where ref = :ref;
        number = number + 1;
    end
  end
end^
SET TERM ; ^
