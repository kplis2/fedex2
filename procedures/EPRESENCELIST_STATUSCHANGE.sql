--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EPRESENCELIST_STATUSCHANGE(
      EPRESENCELIST integer,
      STATUS smallint)
  returns (
      INFO varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable epresencelistnag integer;
declare variable jobtimeq numeric(14,2);
declare variable posjobtimeq numeric(14,2);
declare variable nazwa varchar(60);
declare variable closelist smallint;
declare variable pom varchar(10);
begin
  INFO = '';
  closelist = 1;
  pom = '
';
  if (:status = 1) then
  begin
    update epresencelists set epresencelists.status = 0 where epresencelists.ref = :epresencelist;
    info = 'Otworzono listę';
  end else
  begin
    info = 'Błędy na pozycjach';
    for
      select epresencelistnag.ref, epresencelistnag.jobtimeq
        from epresencelistnag
        where epresencelistnag.epresencelist = :epresencelist
        into :epresencelistnag, jobtimeq
    do begin
      if (not exists (select ref from epresencelistspos
        where epresencelistspos.epresencelistnag = :epresencelistnag
          and epresencelistspos.accord = 1)) then
      begin
        select operator.nazwa, sum(epresencelistspos.jobtimeq)
          from epresencelistspos
            join operator on (epresencelistspos.operator = operator.ref)
          where epresencelistspos.epresencelistnag = :epresencelistnag
            and epresencelistspos.accord = 0
          group by operator.nazwa
          into :nazwa, :posjobtimeq;
        if (:posjobtimeq <> :jobtimeq) then
        begin
          info = :info||:pom||:nazwa;
          closelist = 0;
        end
      end
    end
    if (:closelist = 1) then
    begin
      update epresencelists set epresencelists.status = 1
        where epresencelists.ref = :epresencelist;
      info = 'Zamknięto listę';
    end
  end
end^
SET TERM ; ^
