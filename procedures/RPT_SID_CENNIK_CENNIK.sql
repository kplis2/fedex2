--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_SID_CENNIK_CENNIK(
      CENNIK integer,
      ODKTM varchar(40) CHARACTER SET UTF8                           ,
      DOKTM varchar(40) CHARACTER SET UTF8                           ,
      CECHA varchar(20) CHARACTER SET UTF8                           ,
      WARTCECHY varchar(20) CHARACTER SET UTF8                           ,
      DATA timestamp)
  returns (
      REF integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      NAZWAT varchar(80) CHARACTER SET UTF8                           ,
      NAZWAW varchar(80) CHARACTER SET UTF8                           ,
      JEDN varchar(5) CHARACTER SET UTF8                           ,
      CENANET numeric(14,2),
      STVAT varchar(5) CHARACTER SET UTF8                           ,
      CENABRU numeric(14,2),
      ATRKOD varchar(70) CHARACTER SET UTF8                           ,
      ATRNAZWA varchar(255) CHARACTER SET UTF8                           )
   as
declare variable cechtyp smallint;
declare variable cnt integer;
declare variable oldatrkod varchar(70);
begin
  atrkod = '';
  atrnazwa = '';
  oldatrkod = '';
  ref = 1;
  if(:cecha <> '')then
    select count(*),max(typ) from DEFCECHY where SYMBOL = :cecha into :cnt, :cechtyp;
  if(:cechtyp = 2 and :wartcechy <> '') then wartcechy = :wartcechy||'%';
  /* Procedure Text */
  if(:cnt > 0) then begin
    for select CENNIK.KTM, CENNIK.WERSJAREF, CENNIK.NAZWAT, WERSJE.nazwa,
      TOWJEDN.jedn, cennik.cenanet, cennik.cenabru, towary.vat, atrybuty.wartosc
    from CENNIK
      join towjedn on (towjedn.ref = CENNIK.jedn)
      join TOWARY on (towary.ktm = cennik.ktm)
      join wersje on (cennik.wersjaref = wersje.ref)
      left join atrybuty on (atrybuty.cecha = :cecha and atrybuty.wersjaref = cennik.wersjaref)
    where cennik.CENNIK = :cennik and cennik.cenanet > 0
      and ((:odktm is null) or (:odktm = '') or (:odktm <= cennik.KTM))
      and ((:doktm is null) or (:doktm = '') or (:doktm >= cennik.KTM))
      and ((:wartcechy is null) or (:wartcechy = '') or (atrybuty.wartosc like :wartcechy))
      and (:data is null or cennik.datazmiany >= :data)
    order by atrybuty.wartosc, cennik.ktm, cennik.wersja
    into :ktm, :wersjaref, :nazwat, :nazwaw,
      :jedn, :cenanet, :cenabru, :stvat, :atrkod
    do begin
      if(:atrkod is null) then atrkod = '';
      if(:atrkod <> :oldatrkod) then begin
        atrnazwa = '';
        if(:cechtyp < 2) then
          select defcechw.opis from DEFCECHW where DEFCECHW.cecha = :cecha and defcechw.wartnum = :atrkod into :atrnazwa;
        else if(:cechtyp = 2) then
          select defcechw.opis from DEFCECHW where DEFCECHW.cecha = :cecha and defcechw.wartstr = :atrkod into :atrnazwa;
        else
          select defcechw.opis from DEFCECHW where DEFCECHW.cecha = :cecha and defcechw.wartdate = :atrkod into :atrnazwa;
        if(:atrnazwa is null) then atrnazwa = '';
        oldatrkod = :atrkod;
      end

      ref = :ref + 1;
      suspend;
    end
  end else begin
   for select CENNIK.KTM, CENNIK.WERSJAREF, CENNIK.NAZWAT, WERSJE.nazwa,
      TOWJEDN.jedn, cennik.cenanet, cennik.cenabru, towary.vat
    from CENNIK
      join towjedn on (towjedn.ref = CENNIK.jedn)
      join TOWARY on (towary.ktm = cennik.ktm)
      join wersje on (cennik.wersjaref = wersje.ref)
    where cennik.CENNIK = :cennik  and cennik.cenanet > 0 
      and ((:odktm is null) or (:odktm = '') or (:odktm <= cennik.KTM))
      and ((:doktm is null) or (:doktm = '') or (:doktm >= cennik.KTM))
      and (:data is null or cennik.datazmiany >= :data)
    order by cennik.ktm, cennik.wersja
    into :ktm, :wersjaref, :nazwat, :nazwaw,
      :jedn, :cenanet, :cenabru, :stvat
    do begin
      ref = :ref + 1;
      suspend;
    end
   end
end^
SET TERM ; ^
