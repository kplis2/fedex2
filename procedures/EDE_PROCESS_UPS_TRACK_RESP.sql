--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_PROCESS_UPS_TRACK_RESP(
      EDEDOCSH_REF EDEDOCH_ID)
  returns (
      OTABLE TABLE_NAME,
      OREF INTEGER_ID)
   as
declare variable SHIPMENTID integer_id;
declare variable SHIPMENTREF integer_id;
declare variable PACKAGEID integer_id;
declare variable ACTIVITYID integer_id;
declare variable PACKAGETRACKINGNUMBER string30;
declare variable SHIPMENTTRACKINGNUMBER string30;
declare variable TRACKINGSTATUS integer_id;
declare variable DATESTRING string10;
declare variable TIMESTRING string10;
declare variable requestoption smallint_id;
declare variable status_code string20;
declare variable tarckstatus_ref integer_id;
begin
  --ML UWAGA! wersja uproszczona. Oryginalana wersja z lobosu to:  EDE_PROCESS_UPS_TRACK_RESPONSE
  select otable, oref
    from ededocsh
    where ref = :ededocsh_ref
    into :otable, :oref;

  for select id
    from ededocsp
    where ededoch = :ededocsh_ref
      and name = 'Shipment'
    into :ShipmentId
  do begin
    ShipmentTrackingNumber = null;
    PackageTrackingNumber = null;
    trackingstatus = null;
    datestring = null;
    timestring = null;

    select val
      from ededocsp
      where ededoch = :ededocsh_ref
        and parent = :ShipmentId
        and name = 'DocRef'
      into :shipmentref;

    select trim(val)
      from ededocsp
      where ededoch = :ededocsh_ref
        and parent = :ShipmentId
        and name = 'ShipmentTrackingNumber'
      into :ShipmentTrackingNumber;

    select val
      from ededocsp
      where ededoch = :ededocsh_ref
        and parent = :ShipmentId
        and name = 'RequestOption'
    into :requestoption;

    for select id
        from ededocsp
        where ededoch = :ededocsh_ref
          and parent = :ShipmentId
          and name = 'Package'
      into :PackageId
    do begin
      select trim(val)
        from ededocsp
        where ededoch = :ededocsh_ref
          and parent = :PackageId
          and name = 'PackageTrackingNumber'
      into :PackageTrackingNumber;

      for select id
        from ededocsp
        where ededoch = :ededocsh_ref
          and parent = :PackageId
          and name = 'Activity'
      into :ActivityId
      do begin
        select trim(val)
          from ededocsp
          where ededoch = :ededocsh_ref
            and parent = :ActivityId
            and name = 'StatusCode'
          into :status_code;

        --if(coalesce(:status_code, '')= 'BRAK')then
          --exception universal 'Brak danych o paczce';

        select ts.ref from x_listywysdtrackstatus ts
          where ts.spedytor = 'UPS' and ts.symbol = :status_code
          into :tarckstatus_ref;

        update LISTYWYSDROZ_OPK opk
          set opk.x_trackstatus = :tarckstatus_ref
          where opk.nrpaczkistr = :PackageTrackingNumber;


      end
    end
    --ML jezeli przynajmniej jedna paczka jest doreczona, to list ustawiamy jako doreczony
    select ts.ref from x_listywysdtrackstatus ts
      where ts.spedytor = 'UPS' and ts.symbol = 'D'
      into :tarckstatus_ref;
    if(exists(
      select first 1 1 from LISTYWYSDROZ_OPK opk
        where opk.listwysd = :shipmentref
          and coalesce(opk.x_trackstatus,0) = :tarckstatus_ref
    ))then
    begin
      update listywysd lwd set lwd.x_trackstatus = :tarckstatus_ref
        where lwd.ref = :shipmentref;
    end
    --ML w innym wypadku jezeli jest chciaz jeden brak danych to tak samo na listywysd
    else
    begin
      select ts.ref from x_listywysdtrackstatus ts
      where ts.spedytor = 'UPS' and ts.symbol = 'BRAK'
      into :tarckstatus_ref;

      if(exists(
      select first 1 1 from LISTYWYSDROZ_OPK opk
        where opk.listwysd = :shipmentref
          and coalesce(opk.x_trackstatus,0) = :tarckstatus_ref
      ))then
      begin
        update listywysd lwd set lwd.x_trackstatus = :tarckstatus_ref
          where lwd.ref = :shipmentref;
      end
    end
  end
  suspend;
end^
SET TERM ; ^
