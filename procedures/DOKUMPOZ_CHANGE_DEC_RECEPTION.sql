--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMPOZ_CHANGE_DEC_RECEPTION(
      KORTOPOZ integer,
      POZDOKUM integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      ILOSC numeric(14,5),
      SERIALIZED smallint,
      BLOKOBLNAG smallint,
      BLOKPOZNALICZ integer,
      CENAMAG numeric(14,4),
      DOSTAWA integer,
      MAG_TYP char(1) CHARACTER SET UTF8                           ,
      MAG char(3) CHARACTER SET UTF8                           ,
      FIFO char(1) CHARACTER SET UTF8                           ,
      MINUS_STAN smallint,
      MAGBREAK integer,
      ZAMOWIENIE integer,
      WERSJAREF integer,
      OPK smallint,
      OPKTRYB smallint,
      SLODEF integer,
      SLOPOZ integer,
      CENASNET numeric(14,4),
      CENASBRU numeric(14,4),
      BN char(1) CHARACTER SET UTF8                           ,
      VAT numeric(14,4),
      STANOPKREF integer,
      ORG_ILOSC numeric(14,4),
      DOK_ILOSC numeric(14,4),
      CENA_KOSZT numeric(14,4))
   as
declare variable numer integer;
declare variable oldcena numeric(14,4);
declare variable i numeric(14,5);
declare variable lcenamag numeric(14,4);
begin
      if(:ilosc < 0)then ilosc = -ilosc;
      numer = null;
      oldcena = null;
      if(:mag_typ = 'E') then begin
        select CENA from STANYIL where MAGAZYN=:mag and KTM = :KTM and WERSJA=:WERSJA and ILOSC <> 0 into :oldcena;
        if(:oldcena = 0) then oldcena = null;
      end
      for select NUMER,ilosc,cena from DOKUMROZ where pozycja = :pozdokum and ((cena = :cenamag and DOSTAWA = :dostawa) or (:opktryb = 2))
      order by numer desc into :numer,:i,:lcenamag do begin
         if(:i is null) then i = 0;
         if(:oldcena <> :lcenamag) then exception DOKUM_ROZ_CENA_E_NIEAKTUALNA;
         if((:i > :ilosc) and (:ilosc <> 0)) then begin
           update DOKUMROZ set ILOSC = (ILOSC - :ilosc), PCENA = null where pozycja = :pozdokum and numer = :numer;
           ilosc = 0;
         end else if(:ilosc > 0) then begin
           delete from DOKUMROZ where pozycja = :pozdokum and numer = :numer;
           ilosc = ilosc - i;
         end
      end
    if(:blokpoznalicz = 0) then
      execute procedure DOKPOZ_OBL_FROMROZ(:pozdokum);
end^
SET TERM ; ^
