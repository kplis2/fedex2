--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_EMPTY_PACKAGES(
      SHIPPINGDOC integer)
   as
declare variable packref integer;
begin
  for
    select distinct o.ref
      from listywysdroz_opk o
        left join listywysdroz r on (o.listwysd = r.listywysd and o.ref = r.opk)
        left join listywysdroz_opk o1 on (o.listwysd = o1.listwysd and o.ref = o1.rodzic)
      where o.listwysd = :shippingdoc
        and o.rodzic is null
        and r.ref is null
        and o1.ref is null
    into :packref
  do
    delete from listywysdroz_opk where ref = :packref;
end^
SET TERM ; ^
