--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_TRANSFER_TOWARY as
declare variable s1 varchar(255); /* wersja */
declare variable s2 varchar(255); /* nazwa wersji */
declare variable s3 varchar(255); /* ktm */
declare variable s4 varchar(255); /* kod zewnetrzny */
declare variable s5 varchar(255); /* PKWiU */
declare variable s6 varchar(255); /* nazwa towaru */
declare variable s7 varchar(255); /* miara */
declare variable s8 varchar(255); /* vat */
declare variable s9 varchar(255); /* czy towar aktywny ? (0/1) */
declare variable s10 varchar(255); /* opis podstawowy */
declare variable s11 varchar(255); /* opis rozszerzony */
declare variable s12 varchar(255); /* kod kreskowy */
declare variable s13 varchar(255); /* typ */
declare variable s14 varchar(255); /* symbol dostawy */
declare variable wersja integer;
declare variable nazwa_wersji varchar(40);
declare variable ktm varchar(40);
declare variable kodzewn varchar(20);
declare variable pkwiu varchar(20);
declare variable nazwa varchar(80);
declare variable miara varchar(5);
declare variable vat varchar(2);
declare variable aktywny smallint;
declare variable opispod varchar(2048);
declare variable opisroz varchar(2048);
declare variable kodkresk varchar(255);
declare variable typ smallint;
declare variable symbol_dost varchar(20);
declare variable tmp integer;
begin

-- sprawdzanie dlugoscie danych w tabeli expimp
  select count(ref) from expimp where coalesce(char_length(s2),0)>40 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich nazw wersji.';

  select count(ref) from expimp where coalesce(char_length(s3),0)>40 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich KTM.';

  select count(ref) from expimp where coalesce(char_length(s4),0)>20 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich kodow zewnetrznych.';

  select count(ref) from expimp where coalesce(char_length(s5),0)>20 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich PKWiU.';

  select count(ref) from expimp where coalesce(char_length(s6),0)>80 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich nazw towaru.';

  select count(ref) from expimp where coalesce(char_length(s7),0)>5 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich jednostek miar.';

  select count(ref) from expimp where coalesce(char_length(s8),0)>2 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich stawek VAT.';

  select count(ref) from expimp where cast(:s9 as smallint)<>0 and cast(:s9 as smallint)<>1
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba zlych wartosci w kolumnie AKTYWNY.';

  select count(ref) from expimp
    where cast(:s13 as smallint)<0 or cast(:s13 as smallint)>3
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba zlych wartosci w kolumnie TYP.';

  select count(ref) from expimp where coalesce(char_length(s14),0)>20 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich symboli dostaw.';


-- import

  for select s1, s2,  s3,  s4,  s5,  s6,  s7,  s8,  s9,  s10,
            s11, s12, s13, s14
    from expimp 
  into :s1, :s2,  :s3,  :s4,  :s5,  :s6,  :s7,  :s8,  :s9,  :s10,
      :s11, :s12, :s13, :s14
  do begin

/* wersja */
    wersja = cast(:s1 as integer);

/* nazwa wersji */
    nazwa_wersji = substring(:s2 from  1 for  40);

/* ktm */
    ktm = substring(:s3 from  1 for  40);

/* kod zewnetrzny */
    kodzewn = substring(:s4 from  1 for  20);

/* PKWiU */
    pkwiu = substring(:s5 from  1 for  20);

/* nazwa towaru */
    nazwa = substring(:s6 from  1 for  80);

/* miara */
    miara = substring(:s7 from 1 for  5);
    if(not exists (select first 1 1 from miara where miara=:miara)) then
      exception universal 'Jednostka miary: ' || :miara || ' nie znajduje sie w bazie.';

/* vat */
    vat = substring(:s8 from  1 for  2);
    if(not exists (select first 1 1 from vat where grupa=:vat)) then
      exception universal 'Stawka VAT: ' || :vat || ' nie znajduje sie w bazie.';

/* czy towar aktywny ? (0/1) */
    aktywny = cast(:s9 as smallint);

/* opis podstawowy */
    opispod = :s10;

/* opis rozszerzony */
    opisroz = :s11;

/* kod kreskowy */
    kodkresk = :s12;

/* typ */
    typ = cast(:s13 as smallint);

/* symbol dostawy */
    symbol_dost = substring(:s14 from  1 for  20);

-- INSERT
    if(not exists (select first 1 1 from towary where ktm=:ktm)) then
    begin
      insert into towary(ktm, kodzewn, pkwiu, nazwa, miara, vat, akt, opispod, opisroz,
                         kodkresk, usluga, symbol_dost)
                 values (:ktm, :kodzewn, :pkwiu, :nazwa, :miara, :vat, :aktywny, :opispod, :opisroz,
                         :kodkresk, :typ, :symbol_dost);
    end
    else if(wersja is not null and nazwa_wersji is not null) then
    begin
      insert into wersje(nrwersji, nazwa, pkwiu, nazwat, miara, vat, akt,
                         opis, kodkresk, usluga, symbol_dost)
                 values (:wersja, :nazwa_wersji, :pkwiu, :nazwa, :miara, :vat, :aktywny,
                         substring(:opispod from 1 for 1024), :kodkresk, :typ, :symbol_dost);
    end
    else
      exception universal 'Towar jest juz w kartotece a pola wersji w expimp sa puste.';


  end
end^
SET TERM ; ^
