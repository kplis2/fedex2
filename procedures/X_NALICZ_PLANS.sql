--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_NALICZ_PLANS(
      PLANS integer,
      PLANSCOL integer,
      UZUPELNIANIE smallint,
      AKTUALIZACJA smallint,
      PLANSROW integer)
  returns (
      VAL numeric(14,4))
   as
DECLARE VARIABLE KEY1 VARCHAR(20);
DECLARE VARIABLE TEMPODNI NUMERIC(14,4);
DECLARE VARIABLE TEMPOOKR NUMERIC(14,4);
DECLARE VARIABLE DATAPOCZ TIMESTAMP;
DECLARE VARIABLE DATAKON TIMESTAMP;
DECLARE VARIABLE CNT INTEGER;
begin
  if (:AKTUALIZACJA is null) then AKTUALIZACJA = 0;
  if (:uzupelnianie is null) then uzupelnianie = 0;
  select planscol.bdata, planscol.ldata from planscol where planscol.ref = :PLANSCOL
    into :datapocz, :datakon;
  select plansrow.KEY1 from plansrow
    where plansrow.plans = :PLANS and PLANSrow.ref = :PLANSROW into :key1;
  execute procedure x_obl_temposprz(:key1) returning_values(:tempookr, :tempodni);
  if (:tempodni is not null and :datakon is not null and :datapocz is not null) then
    VAL = (:datakon - :datapocz)*:tempodni;
  if (:val is null) then val = 0;
  val = 100;
  suspend;
END^
SET TERM ; ^
