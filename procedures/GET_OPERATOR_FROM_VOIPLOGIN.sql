--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_OPERATOR_FROM_VOIPLOGIN(
      VOIPLOGIN STRING)
  returns (
      OPER OPERATOR_ID,
      UUID STRING,
      QUEUES STRING)
   as
begin
  select first 1 o.ref, o.uuid, o.voipqueues
    from operator o
    where o.voiplogin = :voiplogin
    into :oper, :uuid, :queues;
  suspend;
end^
SET TERM ; ^
