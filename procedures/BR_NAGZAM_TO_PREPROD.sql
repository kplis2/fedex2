--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_NAGZAM_TO_PREPROD(
      PRNAGZAM integer,
      PRWERSJAREF integer)
  returns (
      REF integer,
      SYMBOL varchar(40) CHARACTER SET UTF8                           )
   as
declare variable prschedguidepos integer;
begin
  for
    select p.prschedguidepos
      from pozzam p
      where p.prschedguidepos is not null and p.zamowienie = :prnagzam
        and p.wersjaref = :prwersjaref
      into prschedguidepos
  do begin
    ref = null;
    select n.ref, n.id
      from prschedguidespos d
        left join pozzam p on (p.ref = d.pozzamref)
        left join nagzam n on (n.ref = p.zamowienie)
      where d.ref = :prschedguidepos
      into ref, symbol;
    if (ref is not null) then
      suspend;
    ref = null;
  end
end^
SET TERM ; ^
