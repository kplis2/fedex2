--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDOPER_TIME_TO_END(
      PRSCHEDGUIDE integer,
      PRSCHEDOPER integer,
      TIMETOEND double precision,
      PREFIX varchar(40) CHARACTER SET UTF8                           )
   as
declare variable ref integer;
declare variable tmp double precision;
begin
  if (prschedoper = 0) then
  begin
    for
      select o.ref, o.worktime
        from prschedopers o
          left join prschedoperdeps d on (d.depfrom = o.ref)
        where o.guide = :prschedguide and o.activ = 1 and d.depto is null
        into prschedoper, timetoend
    do begin
      execute procedure prschedoper_time_to_end(:prschedguide, :prschedoper, :timetoend, :prefix);
    end
  end else begin
    update prgantt set worktimetoend = :timetoend where prschedoper = :prschedoper;
    for
      select f.ref, f.worktime
        from prschedoperdeps d
          left join prschedopers f on (f.ref = d.depfrom)
        where d.depto = :prschedoper and f.activ = 1
        into ref, tmp
    do begin
      if (tmp is null) then tmp = 0;
      execute procedure prschedoper_time_to_end(:prschedguide, :ref, :timetoend + :tmp, :prefix);
    end
  end
end^
SET TERM ; ^
