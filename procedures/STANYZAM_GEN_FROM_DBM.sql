--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STANYZAM_GEN_FROM_DBM(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           )
   as
declare variable ktm varchar(40);
declare variable wersjaref integer;
declare variable zamowic numeric(14,4);
declare variable rmagazyn varchar(3);
declare variable rmag2 varchar(3);
declare variable dostawca integer;
begin
  delete from STANYZAM where magazyn=:magazyn;
  for select KTM,WERSJAREF,ZAMOWIC,RMAGAZYN,RMAG2,DOSTAWCA
    from GET_DBM(:magazyn,0)
    where ZAMOWIC>0
    into :ktm, :wersjaref, :zamowic, :rmagazyn, :rmag2, :dostawca
  do begin
    update or insert into STANYZAM(MAGAZYN,KTM,WERSJAREF,ILOSCSUGEROWANA,ILOSCDOZAM,STATUS,ZMAGAZYNU,ZMAG2,DOSTAWCA)
    values (:magazyn,:ktm,:wersjaref,:zamowic,:zamowic,0,:rmagazyn,:rmag2,:dostawca)
    matching (MAGAZYN, WERSJAREF);
  end
end^
SET TERM ; ^
