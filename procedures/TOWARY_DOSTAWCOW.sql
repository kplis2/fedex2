--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TOWARY_DOSTAWCOW(
      KTM varchar(80) CHARACTER SET UTF8                           )
  returns (
      KTMDOST varchar(80) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      JEDNOSTKA varchar(100) CHARACTER SET UTF8                           ,
      CENAWAL numeric(14,4),
      WALUTA varchar(3) CHARACTER SET UTF8                           ,
      DOSTAWCA varchar(255) CHARACTER SET UTF8                           ,
      REFD integer,
      POTENCJALNY smallint)
   as
DECLARE VARIABLE REF INTEGER;
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE DATA DATE;
DECLARE VARIABLE GRUPA VARCHAR(60);
DECLARE VARIABLE GRUPACENZEW VARCHAR(60);
DECLARE VARIABLE SLODEF INTEGER;
DECLARE VARIABLE SLOPOZ INTEGER;
begin
  select atrybuty.wartosc from atrybuty
    where atrybuty.cecha = 'GRUPA' and atrybuty.ktm = :ktm and atrybuty.nrwersji = 0
    into :grupa;
  FOR SELECT DOSTAWCY.ref FROM DOSTAWCY order by REF INTO :REFD
  DO BEGIN
    select max(datawe) from nagzam
      join pozzam on (pozzam.zamowienie = nagzam.ref)
      where nagzam.typzam = 'DST' and pozzam.ktm = :ktm AND NAGZAM.dostawca = :REFD
      into :data;
    select max(pozzam.ref) from pozzam
      join nagzam on (pozzam.zamowienie = nagzam.ref)
      where nagzam.datawe = :data and pozzam.ktm = :ktm  AND NAGZAM.dostawca = :REFD
      into :ref;
    select DOSTAWCY.id, towary.nazwa, towjedn.jedn, pozzam.cenanet, nagzam.waluta
      from pozzam
      join towary on (pozzam.ktm = towary.ktm)
      join towjedn on (towjedn.ref = pozzam.jedn)
      join nagzam on (pozzam.zamowienie = nagzam.ref)
      JOIN dostawcy ON (NAGZAM.dostawca = DOSTAWCY.ref)
      where pozzam.ref = :ref AND NAGZAM.dostawca = :REFD
      into :DOSTAWCA, :nazwa, :jednostka, :cenawal, :waluta;
    select count(*) from pozzam join nagzam on (pozzam.zamowienie = nagzam.ref)
      where pozzam.ktm = :KTM and nagzam.dostawca = :refd into :cnt;
    if (:cnt > 0) then
    begin
      potencjalny = 0;
      KTMDOST = :KTM;
      suspend;
    end
  END
  for
    select cennikizewn.grupa, cennikizewn.ktm, cennikizewn.cena_fabn,
        cennikizewn.waluta, towary.nazwa, cennikizewn.miara, cennikizewn.slodef, cennikizewn.slopoz
      from cennikizewn
      left join towary on (cennikizewn.ktm = towary.ktm)
      order by cennikizewn.ref
      into :GRUPACENZEW, :KTMDOST, :CENAWAL, :WALUTA, :NAZWA, :JEDNOSTKA, :SLODEF, :SLOPOZ
  do begin
    if (:KTMDOST = :KTM and KTM is not null) then begin
      select CPODMIOTY.nazwa from CPODMIOTY where CPODMIOTY.slodef = :slodef and Cpodmioty.slopoz = :SLOPOZ
        into :DOSTAWCA;
      POTENCJALNY = 1;
      suspend;
    end
    /*else if (:GRUPACENZEW = :GRUPA and :GRUPA is not null) then  suspend;*/
  end
end^
SET TERM ; ^
