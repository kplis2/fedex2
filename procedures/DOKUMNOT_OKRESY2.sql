--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNOT_OKRESY2(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      OKRES1 varchar(6) CHARACTER SET UTF8                           )
  returns (
      OKRESSID varchar(6) CHARACTER SET UTF8                           ,
      OKRESFK varchar(6) CHARACTER SET UTF8                           ,
      WARTOSC numeric(14,2))
   as
DECLARE VARIABLE OKRES2 VARCHAR(6);
begin
  for select BK.ID from BKPERIODS BK WHERE BK.ID< :okres1 into :okres2
  do begin
    okressid = okres1;
    okresfk = okres2;
    select sum(dokumnotp.wartosc)
      from dokumnot
        join dokumnotp on (dokumnotp.dokument=dokumnot.ref)
        left join dokumroz on (dokumroz.ref=dokumnotp.dokumrozkor)
        left join dokumpoz on (dokumpoz.ref=dokumroz.pozycja)
        left join dokumnag on (dokumnag.ref=dokumpoz.dokument)
        left join defdokum on (dokumnot.typ=defdokum.symbol)
      where defdokum.wydania =1 and defdokum.zewn =1 and dokumnot.magazyn=:magazyn
        and dokumnot.okres=:okresfk and dokumnag.okres=:okressid
      into :wartosc;
    if (:wartosc is not null) then  suspend;
    okressid = okres2;
    okresfk = okres1;
    select sum(dokumnotp.wartosc)
      from dokumnot
        join dokumnotp on (dokumnotp.dokument=dokumnot.ref)
        left join dokumroz on (dokumroz.ref=dokumnotp.dokumrozkor)
        left join dokumpoz on (dokumpoz.ref=dokumroz.pozycja)
        left join dokumnag on (dokumnag.ref=dokumpoz.dokument)
        left join defdokum on (dokumnot.typ=defdokum.symbol)
      where defdokum.wydania =1 and defdokum.zewn =1  and dokumnot.magazyn=:magazyn
        and dokumnot.okres=:okresfk and dokumnag.okres=:okressid
      into :wartosc;
    if (:WARTOSC is not null) then suspend;
  end
end^
SET TERM ; ^
