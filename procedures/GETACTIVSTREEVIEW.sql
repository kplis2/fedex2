--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GETACTIVSTREEVIEW(
      POZYCJA integer,
      PLANREF integer)
  returns (
      REF integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4),
      STARTDATE timestamp,
      ENDDATE timestamp,
      PARENT integer)
   as
declare variable pmpositionref integer;
declare variable wersja integer;
begin
  select mainref from pmpositions
    where ref=:pozycja
    into :pmpositionref;
  select versionnum from pmplans
    where ref=:planref
    into :wersja;
  for select ref from pmactivs
        where pmposition=:pmpositionref
          and ref=mainref
        into :ref
  do begin
    select first 1 ref, symbol, name, quantity, startdate, enddate, parent from pmactivs
      where mainref=:ref
        and versionnum<=:wersja
      order by versionnum desc
      into :ref, :symbol, :name, :quantity, :startdate, :enddate, :parent;
    suspend;
    while(exists(select * from pmactivs where ref=:parent))
    do begin
      select ref, symbol, name, quantity, startdate, enddate, parent from pmactivs
        where ref=:parent
        into :ref, :symbol, :name, :quantity, :startdate, :enddate, :parent;
      suspend;
    end
  end
end^
SET TERM ; ^
