--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_DEPARTMENTS(
      DEPARTMENT varchar(10) CHARACTER SET UTF8                           ,
      FROMDATE date,
      TODATE date,
      CURRENTCOMPANY smallint,
      NO_PER_PAGE integer)
  returns (
      SYMBOL varchar(10) CHARACTER SET UTF8                           ,
      NAME varchar(60) CHARACTER SET UTF8                           ,
      FULL_DEPARTMENT varchar(1024) CHARACTER SET UTF8                           ,
      I integer)
   as
declare variable counter integer;
  declare variable curr_name varchar(60);
  declare variable parent integer;
begin
  --DS: funkcja zwraca liste wydzialow, w ktorych zatrudnieni byli pracownicy w zadanym okresie czasu
  --mozna wskazac tydzien lub miesiac i rok
  --no_per_page wskazuje maksymalnie na jednej stronie zmiesci sie pracowikow z danego wydzialu
  --jezeli nas to nie dotyczy zostawiamy null
  if (department = '') then department = null;
  no_per_page = coalesce(no_per_page,-1);
  --w parametrze powinien byc albo tydzien albo miesiac i rok, wyznaczamy pierwszy i ostatni dzien okresow
  for
    select d.symbol, d.name, round(count(e.ref) / :no_per_page + 0.49), d.parent
      from employees e
        left join departments d on e.department = d.symbol
        left join employment em on (e.ref= em.employee)
      where e.company = :CURRENTCOMPANY
        and em.fromdate <= :TODATE
        and (em.todate >= :FROMDATE or em.todate is null)
        and e.empltype=1
        and (d.departmentlist like '%;'||:department||';%' or :department is null)
      group by d.symbol, d.name, d.parent
      into symbol, name, counter, parent
  do begin
    full_department = name;
    curr_name = full_department;
    while(curr_name is not null) do
    begin
    curr_name = null;
    select d.name, d.parent
      from departments d
      where d.ref = :parent
      into :curr_name, parent;
      if (curr_name is not null) then
        full_department = curr_name || ' > ' || full_department;
    end

    i = 0;
    if (no_per_page = -1) then suspend;
    else
      while (i <= counter) do
      begin
        suspend;
        i = i + 1;
      end
  end
end^
SET TERM ; ^
