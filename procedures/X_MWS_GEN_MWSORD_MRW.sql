--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_GEN_MWSORD_MRW(
      DOCID integer,
      DOCGROUP integer,
      DOCPOSID integer,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      MWSORD integer,
      MWSACT integer,
      RECALC smallint,
      MWSORDTYPE integer)
  returns (
      REFMWSORD integer)
   as
declare variable rec smallint;
declare variable wh varchar(3);
declare variable stocktaking smallint;
declare variable operator integer;
declare variable priority smallint;
declare variable branch varchar(10);
declare variable period varchar(6);
declare variable flags varchar(40);
declare variable symbol varchar(20);
declare variable cor smallint;
declare variable quantity numeric(14,4);
declare variable todisp numeric(14,4);
declare variable toinsert numeric(14,4);
declare variable good varchar(20);
declare variable vers integer;
declare variable opersettlmode smallint;
declare variable mwsconstlocp integer;
declare variable mwspallocp integer;
declare variable mwsconstlocl integer;
declare variable mwsconstlocltmp integer;
declare variable mwspallocltmp integer;
declare variable autoloccreate smallint;
declare variable mwspallocl integer;
declare variable whsec integer;
declare variable wharea integer;
declare variable whareag integer;
declare variable posflags varchar(40);
declare variable description varchar(1024);
declare variable posdescription varchar(255);
declare variable lot integer;
declare variable connecttype smallint;
declare variable shippingarea varchar(3);
declare variable quantityonlocation numeric(15,4);
declare variable wharealogp integer;
declare variable whareap integer;
declare variable mwsstandlevelnumber smallint;
declare variable difficulty varchar(10);
declare variable mwsaccessory integer;
declare variable operatordict varchar(80);
declare variable timestart timestamp;
declare variable maxnumber integer;
declare variable timestartdcl timestamp;
declare variable timestopdcl timestamp;
declare variable realtime double precision;
declare variable wharealogb integer;
declare variable wharealoge integer;
declare variable wharealogl integer;
declare variable dist numeric(14,4);
declare variable lifthight numeric(14,4);
declare variable actuvolume numeric(14,4);
declare variable insertvolume numeric(14,4);
declare variable przelicz numeric(14,4);
declare variable obj numeric(14,4);
declare variable docobj numeric(14,4);
declare variable refmwsactpal integer;
declare variable paltype varchar(20);
declare variable whsecout integer;
declare variable shippingtype integer;
declare variable nextmwsord smallint;
declare variable tmp integer;
declare variable docpostomwsord integer;
declare variable maxpalvol numeric(14,4);
declare variable takefullpal smallint;
declare variable frommwsconstloc integer;
declare variable palgroupmm integer;
declare variable takefullpalmm integer;
declare variable takefrommwsconstlocmm integer;
declare variable takefrommwspallocmm integer;
declare variable poscnt integer;
declare variable insertoper integer;
declare variable slodef integer;
declare variable slopoz integer;
declare variable slokod varchar(40);
declare variable docreal smallint;
declare variable repal smallint;
declare variable palquantity integer;
declare variable refill smallint;
declare variable defdoc varchar(3);
declare variable mwswarning varchar(255);
declare variable orgtermdost timestamp;
declare variable ordforop integer;
declare variable actref integer;
declare variable cntactonord integer;
declare variable docsymbs varchar(60);
declare variable autocommit smallint;
declare variable price numeric(14,4);
declare variable rozonakc smallint;
declare variable rozonakcs varchar(100);
begin
/*$$IBEC$$ 
  MWSORDTYPE = 22;
  autocommit = 1;
  wharea = 7343;
  wharealogb = wharea;
  wharealoge = wharea;
  mwsstandlevelnumber = 1;
  execute procedure get_config('ROZDOKAKC',2) returning_values rozonakcs;
  if (rozonakcs is null or rozonakcs = '') then
    rozonakc = 0;
  else
    rozonakc = cast(rozonakcs as smallint);
  if (recalc is null) then recalc = 0;
  if (docgroup is null) then docgroup = docid;
  poscnt = 0;
  --nagĂłwek zlecenia magazynowego
  select stocktaking, opersettlmode, autoloccreate
    from mwsordtypes
    where ref = :mwsordtype
    into stocktaking, opersettlmode, autoloccreate;
  select defdokum.wydania, defdokum.koryg, dokumnag.magazyn, dokumnag.operator,
      dokumnag.uwagi, dokumnag.katmag, dokumnag.typ,
      dokumnag.priorytet, dokumnag.oddzial, dokumnag.okres, dokumnag.flagi,
      dokumnag.symbol, dokumnag.kodsped, dokumnag.termdost, dokumnag.sposdost,
      dokumnag.takefullpal, dokumnag.palgroup, dokumnag.takefrommwsconstloc,
      dokumnag.takefrommwspalloc, 1, klienci.ref, substring(klienci.fskrot from 1 for 40),
      dokumnag.docreal, dokumnag.iloscpal, dokumnag.zwrotpal --, dokumnag.xorgtermdost
    from dokumnag
      left join defdokum on (defdokum.symbol = dokumnag.typ)
      left join klienci on (klienci.ref = dokumnag.klient)
    where dokumnag.ref = :docid
    into rec, cor, wh, operator,
      description, difficulty, defdoc,
      priority, branch, period, :flags,
      :symbol, shippingarea, :timestart, :shippingtype,
      takefullpalmm, palgroupmm, takefrommwsconstlocmm,
      takefrommwspallocmm, slodef, slopoz, slokod,
      docreal, palquantity, repal; --, orgtermdost;
  if (takefrommwspallocmm = 0) then takefrommwspallocmm = null;
  execute procedure XK_MWS_FIND_MWSORDREF(:mwsordtype, :mwsord, :mwsaccessory, :docgroup,
      :docid, :docposid, :wh, :branch, :whsec)
    returning_values :refmwsord, :connecttype, :paltype, :docobj, :maxpalvol;
  if (rec = 0) then rec = 1; else rec = 0;
  -- jezeli nie mozna nigdzie dolaczyc nowych operacji do generuje nowe zlecenie
  execute procedure gen_ref('MWSORDS') returning_values refmwsord;
  select okres from datatookres(current_date,0,0,0,0) into period;
  insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator, priority,
      description, wh,  regtime, timestartdecl, mwsaccessory, branch, period,
      flags, docsymbs, cor, rec, bwharea, ewharea, status, shippingarea, SHIPPINGTYPE, takefullpal,
      slodef, slopoz, slokod, palquantity, repal)
    values (:refmwsord, :mwsordtype, :stocktaking, 'M', :docgroup, :docid, :operator, :priority,
        :description, :wh, current_timestamp(0), :orgtermdost, :mwsaccessory, :branch, :period,
        :flags, :symbol, :cor, :rec, :wharealogb, :wharealoge, 0, :shippingarea, :SHIPPINGTYPE, 0,
        :slodef, :slopoz, :slokod, :palquantity, :repal);
  -- okreslenie operatora ktory ma przygotowac towar
  execute procedure xk_mws_get_bestoperator(:timestart,:WH,0,cast(takefullpal as varchar(10)),:refmwsord,1,:mwsaccessory)
    returning_values(:operator, :operatordict, :timestart);
  -- wybranie akcesorium operatora
  mwsconstlocl = takefrommwsconstlocmm;
  mwspallocl = takefrommwspallocmm;
  if (mwspallocl is null) then
    select first 1 mwspalloc from mwsstock where mwsconstloc = :mwsconstlocl
      order by ref desc
      into mwspallocl;
  if (mwspallocl is null) then
  begin
    execute procedure gen_ref('MWSPALLOCS') returning_values mwspallocl;
      insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status,
          fromdocid, fromdocposid, fromdoctype)
        select :mwspallocl, symbol, ref, 2, 0,
            :docid, :docposid, :doctype
          from mwsconstlocs
          where ref = :mwsconstlocl;
  end
  while (exists(
           select p.ref
             from dokumpoz p
             where p.dokument = :docid and p.ilosconmwsacts < case when :rozonakc = 0 then p.ilosc else p.iloscl end))
  do begin
    whareag = null;
    -- wybranie najlepszej pozycji dokumentu do realizacji
    -- dokumenty rozchodowe liczymy osobna procedura ze wzgledĂłw wydajnosciowych
    mwsconstlocp = null;
    good = null;
    vers = null;
    select first 1 dp.ref, dp.ktm, dp.wersjaref, case when :rozonakc = 0 then dp.ilosc - dp.ilosconmwsacts else dp.iloscl - dp.ilosconmwsacts end,
        dp.flagi, dp.uwagi, dp.dostawa, m.quantity - m.blocked,
        m.mwspalloc, m.mwsconstloc, m.wharea
      from dokumpoz dp
        left join mwsstock m on (dp.wersjaref = m.vers and m.wh = :wh and m.quantity - m.blocked > 0)
        left join mwsconstlocs ml on (ml.ref = m.mwsconstloc)
      where
        case when :rozonakc = 0 then dp.ilosc - dp.ilosconmwsacts else dp.iloscl - dp.ilosconmwsacts end > 0 and dp.dokument = :docid
        and m.ref is not null and ml.ref is not null
        and ml.ref = :mwsconstlocl
        and (m.mwspalloc = :takefrommwspallocmm or :takefrommwspallocmm is null)
      into docposid, good, vers, quantity,
         posflags, posdescription, lot, quantityonlocation,
         mwspallocp, mwsconstlocp, whareag;
    todisp = quantity;
    if (:quantityonlocation >= todisp) then
      toinsert = todisp;
    else
      toinsert = quantityonlocation;
    -- zbadanie czy nie przekroczysz dopuszczalnej objetosci palety
    if (toinsert > 0) then
    begin
      if (takefullpal = 1) then mwspallocl = mwspallocp;
      todisp = todisp - toinsert;
      execute procedure XK_MWS_GET_REALTIME(:wharealogb, :wharealoge, :mwsordtype, :refmwsord,
          :mwsaccessory, :maxnumber, :good, :vers, :quantity, :priority, :timestart,
          :operator, 1, :wh, :wharealogp, :wharealogp, :rec, :stocktaking, :mwspallocp, :mwspallocl)
        returning_values(:timestartdcl, :timestopdcl, :realtime, :operator, :mwsaccessory, :dist);
      maxnumber = maxnumber + 1;
      insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
          mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
          regtime, mwsaccessory, flags, descript, priority, lot, recdoc, plus,
          whareal, whareap, wharealogl, wharealogp, number, disttogo, palgroup, autogen)
        values (:refmwsord, 1, :stocktaking, :good, :vers, :toinsert, :mwsconstlocp, :mwspallocp,
            null,null, :docid, :docposid, 'M', :opersettlmode, :autoloccreate, :wh, :whareag, :whsec,
            current_timestamp(0), :mwsaccessory, :posflags, :posdescription, :priority, :lot, :rec, 1,
            null, :whareap, null, :wharealogp, :maxnumber, :dist, null, 1);
      wharea = :wharealogp;
      timestart = timestartdcl;
    end
  end
  -- czas dla ostatniej operacji - dojscie do punktu pozostawienia zlecenia
  execute procedure XK_MWS_GET_REALTIME(:wharealogb, :wharealoge, :mwsordtype, :mwsord,
      :mwsaccessory, :maxnumber, :good, :vers, :quantity, :priority, :timestart,
      :operator, 1, :wh, :wharealogp, :wharealogp, :rec, :stocktaking, :mwspallocp, :mwspallocl)
    returning_values(:timestartdcl, :timestopdcl, :realtime, :operator, :mwsaccessory, :dist);
  update mwsords set takefullpal = :takefullpal, status = 1, operator = null, nextmwsord = :nextmwsord
    where ref = :refmwsord and status = 0;
  if (autocommit = 1) then
  begin
    for
      select distinct ref from mwsords where docgroup = :docid and ref = :refmwsord
        into refmwsord
    do begin
      update mwsacts set status = 2, quantityc = quantity where mwsord = :refmwsord and status <> 5;
      update mwsords set status = 5 where ref = :refmwsord and status <> 5;
    end
  end
  -- przeliczenie pwartosci
  for
    select ref from dokumpoz where dokument = :docid
      into docposid
  do begin
    price = 0;
    select sum(pwartosc) from dokumroz where pozycja = :docposid
      into price;
    if (price is null) then price = 0;
    update dokumpoz set pwartosc = :price, wartosc = :price where ref = :docposid;
  end
  price = 0;
  select sum(pwartosc) from dokumpoz where dokument = :docid
    into price;
  if (price is null) then price = 0;
  update dokumnag set pwartosc = :price, wartosc = :price where ref = :docid;
 $$IBEC$$*/
  suspend;

end^
SET TERM ; ^
