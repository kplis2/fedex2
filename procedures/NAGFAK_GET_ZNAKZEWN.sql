--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_GET_ZNAKZEWN(
      NAGFAKREF integer)
  returns (
      ZAMOWZEWN varchar(255) CHARACTER SET UTF8                           )
   as
declare variable nagzamref int;
declare variable tym varchar(80);
declare variable separator varchar(5);
begin
  zamowzewn='';
  separator='; ';
  for select pozfak.fromzam from pozfak
    join nagfak on (nagfak.ref = pozfak.dokument)
    where nagfak.ref = :nagfakref
    group by pozfak.fromzam
    into :nagzamref
   do begin
     --tym='';
     select nagzam.znakzewn from nagzam where nagzam.ref = :nagzamref
     into :tym;
     if(:tym is null) then tym = '';
     if (:tym <>'') then
     zamowzewn = :zamowzewn||:tym||:separator;
   end
  suspend;
end^
SET TERM ; ^
