--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXP2PLATNIK_ZCNA(
      EMPLFAMILY integer,
      EMPL_FROMDATE date,
      EMPL_TODATE date,
      COMPANY integer,
      DTYPE integer = 0)
  returns (
      REF integer,
      PERSON integer,
      PESEL varchar(11) CHARACTER SET UTF8                           ,
      NIP varchar(15) CHARACTER SET UTF8                           ,
      FNAME varchar(60) CHARACTER SET UTF8                           ,
      SNAME varchar(30) CHARACTER SET UTF8                           ,
      MIDDLENAME varchar(30) CHARACTER SET UTF8                           ,
      MNAME varchar(30) CHARACTER SET UTF8                           ,
      BIRTHDATE date,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      DOCNO varchar(11) CHARACTER SET UTF8                           ,
      EMAIL varchar(255) CHARACTER SET UTF8                           ,
      ADATE date)
   as
declare variable TYP integer;
begin
/* MWr: Personel - Zgloszenie danych o czlonkach rodziny, których adres zamieszkania
                   nie jest zgodny z adresem zamieszkania ubezpieczonego

        DTYPE  = 0 - zakres dat dotyczy rozpoczetych umow  //PR52595     -- rejestracja nowego czlonka
               = 1 - zakres dat dotyczy zakonczonych umow                -- wyrejestrowanie czlonka rodziny
 EMPL_FROMDATE = data 'Od' zakresu dat danego typu, istotne dla EMPLFAMILY <= 0
   EMPL_TODATE = data 'Do' zakresu dat danego typu, istotne dla EMPLFAMILY <= 0*/

  nip = ''; --PR57778
  ADATE = null;
  if (emplfamily > 0) then
  begin
    empl_fromdate = null;  --gorny przedzial okresu, oraz
    empl_todate = null;    --dolny przedzial okresu, w ktorym ma sie zawierac data zatrudnienia pracownika
    typ = dtype;           --przechwycenie znacznika czy rejestracja czy wyrejestrowanie
    dtype = -1;
  end else
    emplfamily = null;

  for
    select distinct p.ref, e.ref,
        case when (ez.ispesel = 1) then p.pesel else '' end as pesel,
        upper(p.fname), upper(p.sname), upper(p.middlename), upper(p.mname), p.birthdate, upper(p.email),
        case when (ez.isother = 1 and p.evidenceno > '') then '1' when (ez.isother = 2 and p.passportno > '') then '2' else '' end as doctype,
        case when (ez.isother = 1 and p.evidenceno > '') then p.evidenceno when (ez.isother = 2 and p.passportno > '') then p.passportno else '' end as docno,
        case when (:typ = 0) then f.insuredfrom when (:typ = 1) then f.insuredto else null end as adate
      from persons p
        join employees e on (p.ref = e.person)
        join emplfamily f on (f.employee = e.ref)
        left join emplcontracts c on c.employee = e.ref
        left join ezusdata ez on (ez.person=p.ref)
      where (f.ref = :emplfamily or :emplfamily is null)
        and (:dtype = -1
          or (:dtype = 0 and c.fromdate >= :empl_fromdate
                         and c.fromdate <= :empl_todate)
          or (:dtype = 1 and coalesce(c.enddate,c.todate,cast('1900-01-01' as timestamp)) >= :empl_fromdate
                         and coalesce(c.enddate,c.todate,cast('3000-01-01' as timestamp)) <= :empl_todate))

        and e.company = :company
        and ez.fromdate = (select max(ez1.fromdate) from ezusdata ez1 where ez1.person = ez.person)
        and (f.insurance = 1 or :emplfamily > 0)
      into :person, :ref, :pesel, :fname, :sname, :middlename, :mname, :birthdate, :email, :doctype, :docno, :adate
  do
  begin
    if (emplfamily > 0) then
      ref = -emplfamily; /* parametr ref jest później przekazywany do procedury EXP2PLATNIK_DOCRA jako argument EMPL_REF
                            w przypadku gdy EMPL_REF < 0 to generowany xml zawiera tylko czlonka rodziny takiego, że:
                            EMPLFAMILY.REF = -EML_REF (patrz BS70987) */
    suspend;
  end
end^
SET TERM ; ^
