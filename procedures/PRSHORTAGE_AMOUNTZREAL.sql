--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHORTAGE_AMOUNTZREAL(
      PRSHORTAGE integer)
   as
declare variable amountshortage numeric(14,4);
begin
  select sum(d.quantity)
    from prschedguidedets d
    where d.prshortage = :prshortage and d.out = 0 and d.overlimit = 0 and d.shortage = 1 and d.byproduct = 0
    into amountshortage;
  if (amountshortage is null) then amountshortage = 0;
  update prshortages set amountzreal = :amountshortage where ref = :prshortage;
end^
SET TERM ; ^
