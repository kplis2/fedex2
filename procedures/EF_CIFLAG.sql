--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_CIFLAG(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      FLAG varchar(4) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable emplcontract integer;
begin
  --DU: personel  - sprawdza obecnosc flagi na umowie cywilnoprawnej
  select emplcontract from epayrolls
    where ref = :payroll
    into :emplcontract;
  if (exists(select ref from emplcontracts
        where ref = :emplcontract and iflags containing ';'||:flag||';')) then
    ret = 1;
  else
    ret = 2;
  suspend;
end^
SET TERM ; ^
