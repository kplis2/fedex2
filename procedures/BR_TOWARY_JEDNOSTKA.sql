--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_TOWARY_JEDNOSTKA(
      KTM varchar(40) CHARACTER SET UTF8                           ,
      ILOSC numeric(14,4))
  returns (
      ILOSCR numeric(14,4),
      MIARAR varchar(5) CHARACTER SET UTF8                           ,
      JEDNR integer)
   as
begin
  for select :ILOSC/PRZELICZ,JEDN,REF
  from TOWJEDN
  where KTM=:ktm and PRZELICZ<>0
  into ILOSCR,MIARAR,JEDNR
  do begin
    suspend;
  end
end^
SET TERM ; ^
