--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INTRASTATP_SET_STATUS(
      INTRASTATPREF integer)
   as
declare variable statusflag smallint;
begin
  select intrastatp.statusflag
  from intrastatp
  where intrastatp.ref = :intrastatpref
  into :statusflag;

  if (:statusflag = 0) then statusflag = 1;
  else statusflag = 0;

  update intrastatp
  set
  statusflag = :statusflag
  where ref = :intrastatpref;
end^
SET TERM ; ^
