--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_LISTYWYSD_GET_INFO(
      LISTWYSD LISTYWYSD_ID)
  returns (
      INFO STRING2048)
   as
declare variable typdok char_1;
declare variable odbiorca odbiorcy_id;
declare variable klient klienci_id;
declare variable krajid country_id;
declare variable sposdostref sposdost_id;
declare variable sposzapref platnosc_id;
begin
  info = 'BRAK';

  select l.typ, l.odbiorcaid, k.ref, k.krajid, l.sposdost
    from listywysd l
      left join odbiorcy o on (l.odbiorcaid = o.ref)
      left join klienci k on (o.klient = k.ref)
    where l.ref = :listwysd
  into :typdok, :odbiorca, :klient, :krajid, :sposdostref;

  select first 1 d.sposzap --zalozenie ze dla calej grupysped jest taki sam sposob zaplaty
    from listywysdpoz p
      left join dokumnag d on (p.doktyp = 'M' and p.dokref = d.ref)
    where p.dokument = :listwysd
  into :sposzapref;

  if (:typdok = 'M') then
  begin
    if (:klient is null) then
    begin
      select first 1 d.klient, k.krajid
        from listywysdpoz p
          left join dokumnag d on (p.doktyp = 'M' and p.dokref = d.ref)
          left join klienci k on (d.klient = k.ref)
        where p.dokument = :listwysd
      into :klient, :krajid;
    end
    if (:klient is null) then
      info = 'Brak informacji o pakowaniu';
    else
    begin
      if (:info <> '' and :info <> 'BRAK') then info = :info||'; ';
      else info = '';
      --Jesli klient z kraju GB lub NL i na dokumencie znajduje sie towar, ktorego kod spelnia warunek (substring(Twr_Kod,6,1) in (‘1’,’2’,’3’,’4’)) to:
      if (:krajid in ('GB','NL') and
          exists(select first 1 1 from listywysdpoz p left join wersje w on (p.wersjaref = w.ref) where substring(w.kodprod from 6 for 1) in ('1','2','3','4'))) then
      begin
        --Jesli kraj GB to wyswietlamy „UKARA!”
        if (:krajid = 'GB') then info = :info||'UKARA';
        --Jesli kraj NL to wyswietlamy „LICENCJA!”
        else if (:krajid = 'NL') then info = :info||'LICENCJA';
      end
      --Jesli klient z kraju DE lub NL to wyswietlamy info „SZARA TASMA, CZARNA FOLIA!”
      else if (:krajid in ('DE','NL')) then
        info = :info||'SZARA TAŚMA, CZARNA FOLIA';

      -- Jesli na zamowieniu jest atrybut „Pakowane paletowo” z wartoscia TAK to wyswietlamy info „Na PALETE”
      if (
        exists(
          select first 1 1
            from listywysdpoz p
              left join dokumnag d on (p.doktyp = 'M' and p.dokref = d.ref)
              left join dokumnag g on (d.grupasped = g.grupasped)
              left join x_atrybuty a on (a.otable = 'DOKUMNAG' and a.oref = g.ref)
            where p.dokument = :listwysd
              and lower(a.nazwacechy) = 'pakowane paletowo'
              and lower(a.wartosc) = 'tak'
        )
      ) then
      begin
        if (:info <> '' and :info <> 'BRAK') then info = :info||'; Na PALETĘ';
        else info = 'Na PALETĘ';
      end

      -- Jesli sposob dostawy to odbior osobisty to wyswietlamy info „DOKUMENTY TYLKO GDY JEST JUZ KLIENT!”
      if (:sposdostref = 1) then
      begin
        if (:info <> '' and :info <> 'BRAK') then info = :info||'; DOKUMENTY TYLKO GDY JEST JUŻ KLIENT';
        else info = 'DOKUMENTY TYLKO GDY JEST JUŻ KLIENT';
      end

      if (:info <> '' and :info <> 'BRAK') then info = :info||'; ';
      else info = '';
      -- Jesli sposob dostawy Poczta Polska Pocztex Kurier 48 to wyswietlamy „POCZTA PUNKT!!”
      if (:sposdostref = 12) then
      begin
        if (:info <> '' and :info <> 'BRAK') then info = :info||'; POCZTA PUNKT';
        else info = 'POCZTA PUNKT';
      end

      -- Jesli forma platnosci Pobranie i sposob dostawy <> „Odbior osobisty” i seria zamowienia IN (H, PRE) to wyswietlamy „FAKTURA!”
      if (:sposdostref <> 1 and :sposzapref = 28 and
        exists(
          select first 1 1
            from listywysdpoz p
              left join dokumnag d on (p.doktyp = 'M' and p.dokref = d.ref)
              left join dokumnag g on (d.grupasped = g.grupasped)
              left join x_atrybuty a on (a.otable = 'DOKUMNAG' and a.oref = g.ref)
            where p.dokument = :listwysd
              and lower(a.nazwacechy) = 'seria'
              and lower(a.wartosc) in ('h','pre'))
        ) then
      begin
        if (:info <> '' and :info <> 'BRAK') then info = :info||'; FAKTURA';
        else info = 'FAKTURA';
      end

      -- Jesli istnieje towar na zamowieniu, gdzie jego ID w systemie IN (23093,23094,23095) to wyswietlamy „NAKLEJKA NA KARTON!”
      if (exists(select first 1 1 from listywysdpoz p left join wersje w on (p.wersjaref = w.ref) where p.dokument = :listwysd and w.ktm in (23093,23094,23095))) then
      begin
        if (:info <> '' and :info <> 'BRAK') then info = :info||'; NAKLEJKA NA KARTON';
        else info = 'NAKLEJKA NA KARTON';
      end

    end
  end
  else if (:typdok = 'Z') then
  begin
    --TODO
  end
  else
    info = 'Typ dokumentu bez obsługi informacji';
end^
SET TERM ; ^
