--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_AKTUCENZAK(
      FAK integer)
   as
declare variable wersjaref integer;
declare variable cenanet numeric(14,2);
begin
  for select WERSJAREF, POZFAK.cenanetzl from POZFAK where DOKUMENT = :fak into
    :wersjaref, :cenanet
  do begin
    update WERSJE set CENA_ZAKNL = :cenanet where REF=:wersjaref;
  end
end^
SET TERM ; ^
