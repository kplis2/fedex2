--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_SETTLEMENT_HISTORY(
      COMPANY integer,
      SLOPOZ integer,
      SLODEF integer,
      SYMBFAK varchar(20) CHARACTER SET UTF8                           ,
      KONTOFK KONTO_ID)
  returns (
      STRANSDATE date,
      DOCDATE date,
      DEBIT numeric(18,2),
      CREDIT numeric(18,2),
      BALDEBIT numeric(18,2),
      BALCREDIT numeric(18,2),
      DESCRIPT varchar(1024) CHARACTER SET UTF8                           )
   as
begin
  baldebit = 0;
  balcredit = 0;

 for
   select stransdate, data, winien, ma, tresc
     from  rozrachp
     where slopoz =:slopoz and slodef = :slodef  and symbfak = :symbfak
       and company = :company and kontofk = :kontofk
     order by stransdate
     into  :stransdate, :docdate,:debit, :credit, :descript

 do begin
   baldebit = baldebit + debit;
   balcredit = balcredit + credit;

   if (baldebit > balcredit) then
   begin
     baldebit = baldebit - balcredit;
     balcredit = 0;
   end else
   begin
     balcredit = balcredit - baldebit;
     baldebit = 0;
   end
   suspend;
 end

end^
SET TERM ; ^
