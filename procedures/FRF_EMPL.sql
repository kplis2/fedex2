--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_EMPL(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      AMOUNT numeric(15,2))
   as
declare variable frvhdr integer;
  declare variable frpsn integer;
  declare variable frcol integer;
  declare variable cacheparams varchar(255);
  declare variable ddparams varchar(255);
  declare variable ondate timestamp;
  declare variable imonth integer;
  declare variable iyear integer;
  declare variable company integer;
  declare variable frversion integer;
  declare variable maternity_leave integer;
begin
-- Pracujący w osobach stan w ostatnim dniu miesiąca (DG1 wiersz 06)
  select frvhdr, frpsn, frcol
    from frvpsns
    where ref = :frvpsn
    into :frvhdr, :frpsn, :frcol;

  select company, frversion
    from frvhdrs
    where ref = :frvhdr
    into :company, :frversion;
  cacheparams = period||';'||company;
  ddparams = '';

  if (not exists (select ref from frvdrilldown where functionname = 'EMPL' and cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr)) then
  begin

    iyear = cast(substring(period from 1 for 4) as integer);
    imonth = cast(substring(period from 5 for 2) as smallint);

    if (imonth = 12) then
      ondate = cast((iyear + 1) || '/1/1' as date) - 1;
    else
      ondate = cast(iyear || '/' || (imonth + 1) || '/1' as date) - 1;

    select count(E.ref)
      from employees E join employment EM on (E.ref = EM.employee)
      where E.econtrtype <> 9 and EM.fromdate <= :ondate
        and (EM.todate is null or EM.todate >= :ondate) and E.company = :company
      into :amount;

  end else
    select first 1 fvalue from frvdrilldown where cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr
      into :amount;

  select count(1) from eabsences ab where ab.todate = :ondate and ab.ecolumn = 260 and company = :company into :maternity_leave;
  amount = coalesce(amount,0) - coalesce(:maternity_leave,0);

  insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
     values (:frvhdr, :frpsn, :frcol, 'EMPL', :cacheparams, :ddparams, :amount, :frversion);
  suspend;
end^
SET TERM ; ^
