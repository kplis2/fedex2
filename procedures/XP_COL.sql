--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XP_COL(
      KEY1 integer,
      KEY2 integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      SYMBOL varchar(80) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable parref integer;
declare variable parvalue numeric(16,6);
declare variable parmodified smallint;
declare variable paramname varchar(255);
declare variable descript varchar(255);
declare variable colref integer;
begin
  paramname = 'COL('||:symbol||')';
  select first 1 ref, cvalue, modified
    from prcalccols where parent=:key2 and expr=:paramname and calctype=1
    into :parref, :parvalue, :parmodified;
  if(:parref is not null and :parmodified=1) then begin
    ret = :parvalue;
    exit;
  end

  select c.cvalue, c.descript, c.ref
    from prcalccols c
    where c.key1 = :key1
      and c.prcolumn = cast(:symbol as integer)
    into :ret, :descript, :colref;
  if (:ret is null) then
    ret = 0;

  -- nalezy zapisac wyliczony parametr
  if(:parref is not null) then begin
    update prcalccols set cvalue=:ret where ref=:parref;
  end else begin
    insert into prcalccols(PARENT,CVALUE,EXPR,DESCRIPT,CALCTYPE,PRCOLUMN, FROMPRCALCCOLPAR)
      values(:key2,:ret,:paramname,:descript,1,null, :colref);

  end
end^
SET TERM ; ^
