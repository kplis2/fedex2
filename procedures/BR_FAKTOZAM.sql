--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_FAKTOZAM(
      ZAM integer)
  returns (
      REF integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      DATA timestamp,
      WARTBRU NUMERIC_14_2,
      WYDRUKOWANO smallint,
      ZAFISK smallint)
   as
begin
  -- faktury powiązane z dok. mag. zamówienia
  for
    select  N.REF, N.symbol, N.DATA, N.wartbru, N.wydrukowano, N.zafisk
      from NAGFAK N
      join DOKUMNAG on (DOKUMNAG.FAKTURA = N.REF)
      join NAGZAM DOKZAM on (DOKUMNAG.zamowienie = DOKZAM.REF)
      where (DOKZAM.org_ref = :zam)
      into :ref, :symbol, :data, :WARTBRU, :WYDRUKOWANO, :ZAFISK
  do begin
    suspend;
  end
  -- faktury powiazane bezposrednio z zamówieniem
  for
    select N.REF, N.symbol, N.DATA, N.wartbru, N.wydrukowano, N.zafisk
      from NAGZAM DOKZAM
      left join NAGFAK N on (N.ref = DOKZAM.FAKTURA or N.fromnagzam = DOKZAM.ref)
      where DOKZAM.REF = :zam or (DOKZAM.org_ref = :zam)
      into :ref, :symbol, :data, :WARTBRU, :WYDRUKOWANO, :ZAFISK
  do begin
    if(:ref is not null and :symbol is not null) then suspend;
  end
  -- faktury powiązane poprzez pozycje z pozycjami zamówienia
  for select nf.REF, nf.symbol, nf.DATA, nf.wartbru, nf.wydrukowano, nf.zafisk
     from nagzam n
     join pozzam p on (p.zamowienie = n.ref)
     join pozfak pf on (pf.frompozzam = p.ref)
     join nagfak nf on (nf.ref = pf.dokument)
     where n.ref = :zam
     group by nf.ref, nf.symbol, nf.data, nf.wartbru, nf.wydrukowano, nf.zafisk
     into :ref, :symbol, :data, :WARTBRU, :WYDRUKOWANO, :ZAFISK
  do begin
    if(:ref is not null and :symbol is not null) then suspend;
  end
end^
SET TERM ; ^
