--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_PIT40_V19_10E_EXP(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable ISDECLARATION integer;
declare variable DEF varchar(20);
declare variable CODE varchar(10);
declare variable PFIELD varchar(10);
declare variable SYSTEMCODE varchar(20);
declare variable OBLIGATIONKIND varchar(20);
declare variable SCHEMAVER varchar(10);
declare variable SYMBOL varchar(10);
declare variable VARIANT varchar(10);
declare variable PVALUE varchar(255);
declare variable TMPVAL1 varchar(255);
declare variable TMPVAL2 varchar(255);
declare variable TMPVAL3 varchar(255);
declare variable TMP smallint;
declare variable CURR_P integer;
declare variable CORRECTION smallint;
declare variable ENCLOSUREOWNER integer;
declare variable ZALTYP varchar(20);
declare variable ISZAL smallint;
declare variable PARENT_LVL_0 integer;
declare variable PARENT_LVL_1 integer;
declare variable PARENT_LVL_2 integer;
declare variable PARENT_LVL_3 integer;
declare variable field integer;
declare variable tmp72 integer;
declare variable tmp73 integer;
begin
  --Sprawdzamy czy dana deklaracja istnieje do eksportu
  select first 1 1
    from edeclarations e
    where e.ref = :oref
  into :isDeclaration;

  if(isDeclaration is null) then
    exception universal'Niepoprawny identyfikator e-deklaracji';

  --Pobieranie definicji
  select e.edecldef, ed.code, ed.systemcode, ed.obligationkind, ed.schemaver, ed.symbol, ed.variant
    from edeclarations e join edecldefs ed on (ed.ref = e.edecldef)
    where e.ref = :oref
  into :def, :code, :systemcode, :obligationkind, :schemaver, :symbol, :variant;

  --Generujemy naglowek pliku XML
  id = 0;
  name = 'Deklaracja';
  parent = null;
  params = 'xmlns="http://crd.gov.pl/wzor/2013/11/25/1389/" xmlns:etd="http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2011/06/21/eD/DefinicjeTypy/"';
  val = null;
  suspend;

  correction = 0;
  parent_lvl_0 = 0;
  --Rozpoczynamy naglowek e-deklaracji
  id = :id + 1; --1
  name = 'Naglowek';
  parent = parent_lvl_0;
  params = null;
  val  = null;
  parent_lvl_1 = :id;
  suspend;
  ----------------------------
    id = id+1;
    name = 'KodFormularza';
    parent = parent_lvl_1;
    params = '';
    if(code is not null) then
      params = params||'kodPodatku="'||code||'" ';
    if(systemcode is not null) then
      params = params||'kodSystemowy="'||systemcode||'" ';
    if(obligationkind is not null) then
      params = params||'rodzajZobowiazania="'||obligationkind||'" ';
    if(schemaver is not null) then
      params = params||'wersjaSchemy="'||schemaver||'"';
    val = symbol;
    suspend;
    
    --WariantFormularza
    id = id+1;
    name = 'WariantFormularza';
    parent = parent_lvl_1;
    params = null;
    val = variant;
    suspend;
    --  A  ---------------------------------->
    -- miejsce i cel skladania formularza
    val = null;
    select e.field,e.pvalue from edeclpos e
      where e.edeclaration = :oref
        and field = 6
        and pvalue is not null
      order by field
      into :field, :val;
    if (field = 6) then
    begin
      id = id+1;
      params = 'poz="P_6"';
      name = 'CelZlozenia';
      suspend;
    end
    params = null;
    for
      select e.field,e.pvalue from edeclpos e
        where e.edeclaration = :oref
          and field in (4,5)
          and pvalue is not null
        order by field
        into :field, :val
    do begin
     id = id+1;
     if (field = 4) then name = 'Rok';
     else if (field = 5) then name = 'KodUrzedu';
     suspend;
    end
    

  id = id+1;
  name = 'Podmiot1';
  parent = parent_lvl_0;
  params = 'rola="Płatnik"';
  val = null;
  parent_lvl_1 = :id;
  suspend;
    --  B  -------------------------------------------------------->
    -- Dane identygikacyjne platnika
    select e.pvalue from edeclpos e
      where e.edeclaration = :oref
        and field = 7
        and pvalue is not null
      into :PVALUE;
    id = id+1;
    if (PVALUE = 1) then name = 'etd:OsobaNiefizyczna';
    else if (PVALUE = 2) then name = 'etd:OsobaFizyczna';
    params = null;
    parent = parent_lvl_1;
    parent_lvl_2 = :id;
    suspend;
    --  B  -------------------------------------------------------->
      val = null;
      params = null;
      parent = parent_lvl_2;
      for
        select e.field,e.pvalue from edeclpos e
          where e.edeclaration = :oref
            and field in (1,8)
            and pvalue is not null
          order by field
          into :field, :val
      do begin
        if (field = 1) then
        begin
          name = 'etd:NIP';
          id = id+1;
          suspend;
        end
        else if (field = 8) then
        begin
          for select upper(stringout), lp
            from PARSESTRING(:val, ', ')
            order by lp
            into :tmpval2, tmp
          do begin
            id = id+1;
            val = tmpval2;
            if (tmp = 1 and pvalue = 1) then name = 'etd:PelnaNazwa';
            else if (tmp = 1 and pvalue = 2) then
            begin
              tmpval1 = tmpval2;
              id = id - 1;
              name = null;
            end 
            else if (tmp = 2 and pvalue = 1) then name = 'etd:REGON';
            else if (tmp = 2 and pvalue = 2) then 
            begin
              name = 'etd:ImiePierwsze';
              val = tmpval2;             
              suspend;
              id = id+1;
              name = 'etd:Nazwisko';
              val = tmpval1;
            end
            else if (tmp = 3 and pvalue = 2) then name = 'etd:DataUrodzenia';
            if (name is not null) then
              suspend;
          end
        end
      end


  --  C  ---------------------------------------->
  id = id+1;
  name = 'Podmiot2';
  parent = parent_lvl_0;
  params = 'poz="P_9A" rola="Podatnik"';
  val = null;
  parent_lvl_1 = :id;
  suspend;
  -- Dane podatnika
    id = id + 1;
    name = 'etd:OsobaFizyczna';
    parent = parent_lvl_1;
    params = null;
    val = null;
    parent_lvl_2 = :id;
    suspend;
    --  C1 Dane identyfikacyjne -------------------------
      parent = parent_lvl_2;
      for
        select e.field, upper(e.pvalue)
          from edeclpos e
          where e.edeclaration = :oref
            and e.pvalue is not null
            and e.field in (9,10,12)
          order by field
          into :field, :val
      do begin
        id = id + 1;
        if (field = 9) then
        begin
          if (char_length(:val) <> 11) then name = 'etd:NIP';
          else name = 'etd:PESEL';
          suspend;
        end
        else if (field = 10) then tmpval3 = val; --name = 'etd:DataUrodzenia';
        else if (field = 12) then   -- bramka wymaga najpierw podania imienia nastepnie nazwiska stad taki brzydki zapis
        begin
          name = 'etd:ImiePierwsze';
          suspend;
          id = id + 1;
          name = 'etd:Nazwisko';    -- po imieniu pobieram nazwisko nazwisko
          select upper(e.pvalue)
          from edeclpos e
          where e.edeclaration = :oref
            and e.pvalue is not null
            and e.field = 11
          into :val;
          suspend;
          id = id + 1;
          name = 'etd:DataUrodzenia';
          val = tmpval3;
          suspend;
        end
      end
    --  C2  Adres Zamieszkania -------------------------
    id = id+1;
    name = 'etd:AdresZamieszkania';
    parent = parent_lvl_1;
    params = 'rodzajAdresu="RAD"';
    val = null;
    parent_lvl_2 = :id;
    suspend;
    ------------------------------------------->
      id = id+1;
      name = 'etd:AdresPol';
      parent = parent_lvl_2;
      params = null;
      --val = null;  jest juz nullem
      parent_lvl_3 = :id;
      suspend;
      --------------------------------------------->
        parent = parent_lvl_3;
        for
          select e.field, upper(e.pvalue)
            from edeclpos e
            where e.edeclaration = :oref
              and e.pvalue is not null
              and e.field in (13,14,15,16,17,18,19,20,21,22)
            order by field
            into :field, :val
        do begin
          id = id + 1;
          if (field = 13) then name = 'etd:KodKraju';
          else if (field = 14) then name = 'etd:Wojewodztwo';
          else if (field = 15) then name = 'etd:Powiat';
          else if (field = 16) then name = 'etd:Gmina';
          else if (field = 17) then name = 'etd:Ulica';
          else if (field = 18) then name = 'etd:NrDomu';
          else if (field = 19) then name = 'etd:NrLokalu';
          else if (field = 20) then name = 'etd:Miejscowosc';
          else if (field = 21) then name = 'etd:KodPocztowy';
          else if (field = 22) then name = 'etd:Poczta';
          suspend;
        end

    id = id+1;
    name = 'PozycjeSzczegolowe';
    parent = parent_lvl_0;
    params = null;
    val = null;
    parent_lvl_1 = :id;
    suspend;
      -- E,F,G,H  ------------------------------->
      parent = parent_lvl_1;
        for
          select e.field, upper(e.pvalue)
            from edeclpos e
            where e.edeclaration = :oref
              and e.pvalue is not null
              and e.field >= 24
              and e.field <= 73
            order by field
            into :field, :val
        do begin
          if (field <= 71) then
          begin
            id = id + 1;
            name = 'P_'||field;
            suspend;
          end else
          if (field = 72) then
            tmp72 = val;
          else
            tmp73 = val;
        end

        tmp72 = coalesce(tmp72,0);
        tmp73 = coalesce(tmp73,0);

        if (not(tmp73 > 0 and tmp72 > 0)) then
        begin
        -- pola 72 i 73 nie moga byc uzupelnione rownoczesnie, obowiazkowo jedno z pol ma byc uzupelnione, gdy oba puste to 72 = 0
          if (tmp73 > tmp72) then
          begin
            name = 'P_73';
            val = tmp73;
          end else begin
            name = 'P_72';
            val = tmp72;
          end
          id = id + 1;
          suspend;
        end

    id = id+1;
    name = 'Pouczenie';
    parent = parent_lvl_0;
    val = 'Za uchybienie obowiązkom płatnika grozi odpowiedzialność przewidziana w Kodeksie karnym skarbowym.';
    suspend;

    if (correction = 1) then
    begin
      select e.ref
        from edeclarations e
        where e.enclosureowner = :oref
      into :enclosureowner;

      if (:enclosureowner is not null) then
      begin
        select e.edecldef
          from edeclarations e
          where e.enclosureowner = :enclosureowner
        into :zaltyp;

        select first 1 1
          from edeclarations e
          where e.ref = :oref and e.edecldef = :zaltyp
        into :iszal;

        if(:iszal is not null)then
        begin
          if (substring(:zaltyp from 1 for 6 ) = 'ORD-ZU') then
          begin
            --Zalacznik_ORD-ZU
            id = id+1;
            name = 'Zalacznik_ORD-ZU';
            parent = parent_lvl_0;
            params = 'xmlns:zzu = "http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2011/10/07/eD/ORDZU/"';
            val = null;
            parent_lvl_1 = :id;
            suspend;

              --Pobieranie definicji do ORD_ZU
              select ed.code, ed.systemcode, ed.obligationkind, ed.schemaver, ed.symbol, ed.variant
                from  edecldefs ed
                where ed.systemcode = :zaltyp
              into :code, :systemcode, :obligationkind, :schemaver, :symbol, :variant;

              --Rozpoczynamy naglowek e-deklaracji ORD_ZU
              id = :id + 1;
              name = 'zzu:Naglowek';
              parent = parent_lvl_1;
              params = null;
              val  = null;
              parent_lvl_2 = :id;
              suspend;
              --cialo naglowka
            
                --KodFormularza
                id = id+1;
                name = 'zzu:KodFormularza';
                parent = parent_lvl_2;
                params = '';
                if(code is not null) then
                params = params||'kodPodatku = "'||code||'" ';
                if(systemcode is not null) then
                params = params||'kodSystemowy = "'||systemcode||'" ';
                if(obligationkind is not null) then
                params = params||'rodzajZobowiazania = "'||obligationkind||'" ';
                if(schemaver is not null) then
                params = params||'wersjaSchemy = "'||schemaver||'"';
                val = symbol;
                suspend;
                
                --WariantFormularza
                id = id+1;
                name = 'zzu:WariantFormularza';
                parent = parent_lvl_2;
                params = null;
                val = variant;
                suspend;

              --PozycjeSzczegolowe
              id = :id + 1;
              name = 'zzu:PozycjeSzczegolowe';
              parent = parent_lvl_1;
              params = null;
              val  = null;
              parent_lvl_2 = :id;
              suspend;

              --Pozycje od first do last -  35 - 82
              curr_p = 13;
              for select epos.pvalue
                from edeclpos epos
                where epos.edeclaration = :oref and epos.field > 12
                                                and epos.field < 14
              into :pvalue
              do begin
                if(:pvalue is not null) then
                begin
                  id = id+1;
                  name = 'zzu:P_'||curr_p;
                  parent = parent_lvl_2;
                  params = null;
                  val = upper(:pvalue);
                  suspend;
                end
                curr_p = curr_p +1;
              end
          end
        end 
      end
    end
end^
SET TERM ; ^
