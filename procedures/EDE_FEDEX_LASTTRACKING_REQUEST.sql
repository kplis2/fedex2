--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_LASTTRACKING_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable shippingdoc integer;
declare variable shippingdocno varchar(20);
declare variable grandparent integer;
begin
  select l.ref, substr(l.symbolsped,1,20)
    from listywysd l
    where l.ref = :oref
  into :shippingdoc, :shippingdocno;

  --sprawdzanie czy istieje dokument spedycyjny
  if(shippingdoc is null) then
    exception ede_fedex 'Brak listy spedycyjnej.';

  val = null;
  parent = null;
  params = null;
  id = 0;
  name = 'pobierzStatusyPrzesyłki';
  suspend;

  val = :shippingdocno;
  name = 'numerPrzesylki';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = '1';
  name = 'czyOstatni';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;
end^
SET TERM ; ^
