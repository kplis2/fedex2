--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOCUMENTS_TO_BKDOCS(
      TYP smallint,
      DOCUMENT_REF integer,
      OPERATOR integer,
      TRYB smallint,
      AUTODECREE smallint)
  returns (
      BKDOCNUMBER integer,
      BKDOCVATNUMBER integer)
   as
declare variable PARAM1 varchar(255);
declare variable PARAM2 varchar(255);
declare variable BRANCH varchar(10);
declare variable OTABLE varchar(10);
declare variable BKREG varchar(10);
declare variable VATREG varchar(10);
declare variable BKDOCTYPE integer;
declare variable CNT integer;
declare variable BKDOC integer;
declare variable DESCRIPT varchar(80);
declare variable DOCDATE timestamp;
declare variable TRANSDATE timestamp;
declare variable VATDATE timestamp;
declare variable PAYDAY timestamp;
declare variable KLIENT integer;
declare variable DOSTAWCA integer;
declare variable ZAKUP smallint;
declare variable NIP varchar(40);
declare variable CONTRACTOR varchar(255);
declare variable CURRENCY varchar(3);
declare variable CORRSYMBOL varchar(20);
declare variable GRUPADOK integer;
declare variable FAKFIRST integer;
declare variable SYMBOL varchar(20);
declare variable KOREKTA integer;
declare variable PERIOD varchar(6);
declare variable VPERIOD varchar(6);
declare variable OLDBKNUMBER integer;
declare variable OLDBKVNUMBER integer;
declare variable CURRATE numeric(14,4);
declare variable SLOPOZ integer;
declare variable SLODEF integer;
declare variable WALUTOWA smallint;
declare variable WALPLN varchar(255);
declare variable RKOKRES varchar(6);
declare variable RKNUMER integer;
declare variable PAYMENTMETH integer;
declare variable TAXGR varchar(10);
declare variable ODR smallint;
declare variable ILPARAM smallint;
declare variable AUTOREGSCHEME integer;
declare variable COSTSALES numeric(14,2);
declare variable I smallint;
declare variable PLATNIK integer;
declare variable VPERIODPROMPT smallint;
declare variable SAD smallint;
declare variable RKSYMBOL varchar(20);
declare variable YEARNUM smallint;
declare variable COMPANY integer;
declare variable NAGKOREKTA smallint;
declare variable DATAOTRZKOR timestamp;
declare variable POM1 timestamp;
declare variable POM2 timestamp;
declare variable POM3 timestamp;
declare variable VTYPE integer;
declare variable PTRANSDATE smallint;
declare variable STMT varchar(60);
declare variable CURVATRATE numeric(14,4);
declare variable DATA_OTRZ timestamp;
declare variable BANKACCOUNT integer;
declare variable NIP_NF varchar(15);
declare variable OREF integer;
begin
  paymentmeth = null;
  bankaccount = null;
  transdate = null;
  -- dokumenty z NAGFAK - sprzedaĹĽy i zakupu
  if (typ = 0) then
  begin
    select REJESTR, TYP, ODDZIAL,  SYMBOL,
        KLIENT, PLATNIK, DOSTAWCA, ZAKUP,
        DATA, DATASPRZ, termzap, sposzap,
        waluta,GRUPADOK,KURS,WALUTOWA, koszt,
        OLDBKDOCNUM, OLDBKVDOCNUM, sad, company,dataotrzkor, vkurs, dataotrz,
        slobankacc, nip
      from NAGFAK where REF=:doCument_ref
      into :param1, :param2, :branch, :symbol,
        :KLIENT, :platnik, :DOSTAWCA, :ZAKUP,
        :docdate, :transdate, :payday, :paymentmeth,
        :currency, :grupadok, :currate,:walutowa, :costsales,
        :oldbknumber, :oldbkvnumber, :sad, :company, :dataotrzkor, :curvatrate, :data_otrz,
        :bankaccount, :nip_nf;
    otable = 'NAGFAK';
    if(data_otrz is null) then
      data_otrz = vatdate;
    if (zakup = 0) then
    begin
      -- w przypadku sprzedaĹĽy data vat jest okreslana na podstawie daty wystawienia dokumentu
      vatdate = :docdate;
      if (platnik > 0 ) then
        klient = :platnik;
      select REF, NIP, NAZWA from KLIENCI where REF=:klient
        into :slopoz, :nip, :contractor;
      select REF from SLODEF where TYP='KLIENCI'
        into :slodef;
      select typfak.korekta from typfak where typfak.symbol = :param2
        into :nagkorekta;
      if (:nagkorekta = 1) then begin
        if (:dataotrzkor is not null) then
          vatdate = :dataotrzkor;
        else
          vatdate = null;
      end
    end else
    begin
      vatdate = :data_otrz;
      select REF, NIP, NAZWA from DOSTAWCY where REF=:dostawca
        into :slopoz, :nip, :contractor;
      select REF from SLODEF where TYP='DOSTAWCY'
        into :slodef;
    end

   if (coalesce(nip_nf,'') <> '') then
      nip=nip_nf;

    if (walutowa is null) then
      walutowa = 0;

    if (walutowa=0) then
    begin
      currency = NULL;
      currate = NULL;
      curvatrate = null;
    end

    if (zakup=1) then
      select substring(symbfak from 1 for 20) from NAGFAK where ref = :document_ref
        into :symbol;

    select KOREKTA from TYPFAK where SYMBOL = :param2
      into :korekta;

    if (korekta=1) then
    begin
      select first 1 ref from NAGFAK n join TYPFAK t on n.typ = t.symbol
        where GRUPADOK = :grupadok and t.nieobrot=0
          into :fakfirst;
      if (zakup = 0) then
        select SYMBOL from NAGFAK where REF=:fakfirst
          into :corrsymbol;
      else                 
        select substring(symbfak from 1 for 20) from NAGFAK where REF=:fakfirst
          into :corrsymbol;
    end
    descript = :symbol;
  end

  -- DOKUMENTY MAGAZYNOWE
  else if (typ = 1) then
  begin
    select  TYP, MAGAZYN, ODDZIAL, SYMBOL,
         KLIENT, DOSTAWCA,
         DATA,OLDBKDOCNUM, COMPANY
      from DOKUMNAG where REF=:DOCUMENT_REF
      into :param1, :param2, :branch, :symbol,
            :KLIENT, :DOSTAWCA,
            :docdate, :oldbknumber, :company;

    otable = 'DOKUMNAG';

    if (klient>0) then
    begin
      select REF, NIP, NAZWA from KLIENCI where REF=:klient
        into :slopoz, :nip, :contractor;
      select REF from SLODEF where TYP='KLIENCI' into :slodef;
    end
    else if (dostawca>0) then
    begin
      select REF, NIP, NAZWA from DOSTAWCY where REF=:dostawca
        into :slopoz, :nip, :contractor;
      select REF from SLODEF where TYP='DOSTAWCY' into :slodef;
    end

    currency = NULL;
    currate = null;
    vatreg = null;
    oldbkvnumber = null;
    descript = :symbol;
  end

  -- RAPORTY KASOWE I BANKOWE
  else if (typ=2) then
  begin
    select stanowisko, datado, oldbkdocnum, okres, numer, symbol
      from RKRAPKAS where ref = :document_ref
      into :param2, :docdate, :oldbknumber, :rkokres, :rknumer, :rksymbol;

    otable = 'RKRAPKAS';
    slodef = null;
    slopoz = null;

    select nazwa, waluta, kasabank, oddzial, company from RKSTNKAS where KOD=:param2
      into :descript, :currency, :param1, :branch, :company;

    execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
      returning_values :walpln;

    if (currency = walpln) then
      currency = null;

    if (coalesce(char_length(descript),0)<10) then -- [DG] XXX ZG119346
      symbol = substring(descript || '-' || rksymbol || '/' || rkokres || '-' || param2 from 1 for 20);
    else
      symbol = substring('R' || param1 || ' ' || rksymbol || '/' || rkokres || '-' ||param2 from 1 for 20);

    descript = descript || '/' || rksymbol;
  end

  -- NOTY MAGAZYNOWE
  else if (typ = 3) then
  begin
    select D.company, D.typ, D.magazyn, D.oddzial, D.symbol, D.data, D.oldbkdocnum, DN.klient, DN.dostawca, 'korekta do ' || DN.symbol
      from DOKUMNOT D
      left join dokumnag DN on (D.dokumnagkor=DN.ref)
      where D.ref = :document_ref
      into :company, :param1, :param2, :branch, :symbol,
            :docdate, :oldbknumber, :klient, :dostawca, :descript;

    slodef = null;
    slopoz = null;

    if (klient>0) then
    begin
      select REF, NIP, NAZWA from KLIENCI where REF=:klient
        into :slopoz, :nip, :contractor;
      select REF from SLODEF where TYP='KLIENCI' into :slodef;
    end
    else if (dostawca>0) then
    begin
      select REF, NIP, NAZWA from DOSTAWCY where REF=:dostawca
        into :slopoz, :nip, :contractor;
      select REF from SLODEF where TYP='DOSTAWCY' into :slodef;
    end

    otable = 'DOKUMNOT';
    currency = NULL;
    currate = null;
    vatreg = null;
    oldbkvnumber = null;
    if (descript is null) then
      descript = :symbol;
  end
  -- ĹšT
  else if (typ = 7) then
  begin
    select f.company, v.symbol, v.operdate, v.oldbkdocnum, v.descript, f.oddzial, v.doctype
      from valdocs v
      left join fxdassets f on (v.fxdasset = f.ref)
      where v.ref = :document_ref
    into :company, :symbol,:docdate, :oldbknumber, descript, :param2, :param1;

    slodef = null;
    slopoz = null;

    otable = 'VALDOCS';
    currency = NULL;
    currate = null;
    vatreg = null;
    oldbkvnumber = null;
    if (descript is null) then
      descript = :symbol;
  end

  select count(*) from BKDOCS where otable = :otable and oref = :document_ref
    into :cnt;

  if (cnt > 0) then
    exception DOCTOBKDOC_EXISTS;
  if (param2 is null) then
    param2 = '';
  if (branch is null) then
    branch = '';

  -- okreslenie reguly przepisania
--exception test_break :param1 || '; ' ||:param2;
  cnt = null;
  ilparam = 3;
  select count(*) from doctobkdocs
    where param1 = :param1 and param2 = :param2 and param3 = :branch
      and typ = :typ and company = :company
    into :cnt;

  if (cnt is null or cnt=0) then
  begin
    ilparam = 2;
    cnt = null;
    select count(*) from doctobkdocs
      where param1 = :param1 and param2 = :param2 and (param3 is null or (param3 = ''))
        and typ = :typ and company = :company
      into :cnt;
    if (cnt is null or cnt=0) then
    begin
      ilparam = 1;
      cnt = null;
      select count(*) from doctobkdocs
        where param1 = :param1 and (param2 is null or (param2 = '')) and (param3 is null or (param3 = ''))
          and typ = :typ and company = :company
        into :cnt;
      if (cnt is null or cnt=0) then
        ilparam = 0;
    end
  end

  if (cnt>1 or ilparam=0) then
  begin
    if (tryb=1) then
      exception DOCTOBKDOC_NOTDOCTOBKDOC;
    bkdocnumber = 0;
    bkdocvatnumber = 0;
    exit;
  end

  if (ilparam=3) then
    select BKREG, BKDOCTYPE, VATREG, taxgr, ptransdate from doctobkdocs where param1 = :param1 and param2 = :param2  and param3 = :branch and typ = :typ
      and company = :company
      into :bkreg, :bkdoctype, :vatreg, :taxgr, :ptransdate;
  else if (ilparam=2) then
    select BKREG, BKDOCTYPE, VATREG, taxgr, ptransdate from doctobkdocs where param1 = :param1 and param2 = :param2 and (param3 is null or (param3 ='')) and typ = :typ
      and company = :company
      into :bkreg, :bkdoctype, :vatreg, :taxgr, :ptransdate;
  else
    select BKREG, BKDOCTYPE, VATREG, taxgr, ptransdate from doctobkdocs where param1 = :param1 and (param2 is null or (param2 = '')) and (param3 is null or (param3 = '')) and typ = :typ
      and company = :company
      into :bkreg, :bkdoctype, :vatreg, :taxgr, :ptransdate;

  --pobranie danych do wsadzenia dokumentu
  if (ptransdate = 0) then transdate = docdate;
  else if (ptransdate = 1 and transdate is null) then
    transdate = docdate;
  else if(ptransdate = 2) then
    transdate = data_otrz;

  select id
    from BKPERIODS
    where company = :company and SDATE <= :transdate and FDATE >=:transdate
      and PTYPE=1
    into :period;

  select vperiodprompt from bkdoctypes where ref=:bkdoctype
    into :vperiodprompt;

  select vatperiod, vatdate from BKDOC_VATDATE_OBL(:otable,:doCument_ref,:docdate,:payday,:company,:vperiodprompt)
  into :vperiod, :vatdate;
 /*
  if (vperiodprompt=1) then begin
    select ID from BKPERIODS where company = :company and SDATE <= :docdate and FDATE >=:docdate and PTYPE=1
      into :vperiod;
    vatdate = :docdate;
  end else if (vperiodprompt=2) then begin
    select ID from BKPERIODS where company = :company and SDATE <= :vatdate and FDATE >=:vatdate and PTYPE=1
      into :vperiod;
  end else if (vperiodprompt=3) then begin
    select ID from BKPERIODS where company = :company and SDATE <= :payday and FDATE >=:payday and PTYPE=1
      into :vperiod;
    vatdate = :payday;
  end
   */
  if (oldbknumber>0) then
  begin
    select yearnum from bkregs where symbol = :bkreg and company = :company into :yearnum;
    cnt = null;
    if(:yearnum=1) then
      select count(*) from BKDOCS where company = :company and BKREG = :bkreg and NUMBER = :oldbknumber and substring(period from 1 for 4)=substring(:period from 1 for 4) into :cnt;
    else
      select count(*) from BKDOCS where company = :company and BKREG = :bkreg and NUMBER = :oldbknumber and period=:period into :cnt;
    if (cnt>0) then
      oldbknumber = null;
    cnt = null;
    if(:yearnum=1) then
      select max(number) from BKDOCS where company = :company and BKREG = :bkreg and substring(period from 1 for 4)=substring(:period from 1 for 4) into :cnt;
    else
      select max(number) from BKDOCS where company = :company and BKREG = :bkreg and period=:period into :cnt;
    if (cnt is null) then
      cnt = 0;
    if (cnt<oldbknumber) then
      oldbknumber = null;
  end

  if (oldbkvnumber>0) then
  begin
    cnt = null;
    select count(*) from BKDOCS where company = :company and VATREG = :vatreg and VNUMBER = :oldbkvnumber and period=:period
      into :cnt;
    if (cnt>0) then
      oldbkvnumber = null;
    cnt = null;
    select max(vnumber) from BKDOCS where company = :company and VATREG = :vatreg and period=:period
      into :cnt;
    if (cnt is null) then
      cnt = 0;
    if (cnt< oldbkvnumber) then
      oldbkvnumber = null;
  end

  if (vatreg= '') then
    vatreg = null;

  if (coalesce(nip_nf,'') <> '') then
      nip=nip_nf;

  -- wpisanie dokumentu ksiegoweg
  execute procedure GEN_REF('BKDOCS')
    returning_values :bkdoc;
  insert into BKDOCS(REF,PERIOD, BKREG, NUMBER, DOCTYPE, SYMBOL, DESCRIPT,
                   DOCDATE, TRANSDATE, VATDATE, PAYDAY,
                   REGOPER, VATREG, DICTDEF, DICTPOS, NIP,
                   CONTRACTOR, CURRENCY, CURRATE, CORRSYMBOL, PAYMENTMETH, COSTSALES,
                   VATPERIOD, VNUMBER,
                   OTABLE, OREF, AUTODOC, branch, company, curvatrate, bankaccount)
  values(:bkdoc, :period, :bkreg, :oldbknumber, :bkdoctype, :symbol, :descript,
         :docdate, :transdate, :vatdate, :payday,
         :operator, :vatreg, :slodef, :slopoz, :nip,
         substring(:contractor from 1 for 80), :currency, :currate, :corrsymbol, :paymentmeth, :costsales,
         :vperiod, :oldbkvnumber,
        :otable, :document_ref, 1, :branch, :company, :curvatrate, :bankaccount);

  select number, vnumber from BKDOCS where ref =:bkdoc
    into :bkdocnumber, :bkdocvatnumber;

  if (taxgr = '') then
    taxgr = null;

  -- teraz pozycje VAT
  if (typ=0) then
  begin
    -- czy SAD
    if (sad=1) then
    begin
      insert into BKVATPOS(BKDOC, NUMBER, NETV, VATE, GROSSV, TAXGR, VATGR,VATREG)
        select :bkdoc, :odr, WARTNET - pwartnet, VAT - pvat, (WARTNET-pwartnet+VAT-pvat), :taxgr, GR_VAT, :vatreg
          from POZSAD where FAKTURA = :document_ref;
    end else
    begin
      select V.vtype
        from vatregs V
          where V.symbol = :vatreg and V.company = :company
      into :vtype;
      if (walutowa = 0) then
        if (:vtype in (1,2)) then
        begin
          insert into BKVATPOS(BKDOC, NUMBER, NETV, VATE, GROSSV, TAXGR, VATGR,
            VATREG, VATVF, VATGRF)
            select :bkdoc, :odr, (ESUMWARTNETZL - PESUMWARTNETZL), 0,
            (ESUMWARTBRUZL - PESUMWARTBRUZL), :taxgr, vat, :vatreg, (ESUMWARTVATZL - PESUMWARTVATZL), vat
              from ROZFAK where DOKUMENT = :document_ref
                and ((ESUMWARTNETZL - PESUMWARTNETZL) <> 0 or (ESUMWARTVATZL - PESUMWARTVATZL) <> 0
                  or (ESUMWARTBRUZL - PESUMWARTBRUZL) <> 0);
        end else
        begin
          insert into BKVATPOS(BKDOC, NUMBER, NETV, VATE, GROSSV, TAXGR, VATGR, VATREG)
            select :bkdoc, :odr, (ESUMWARTNETZL - PESUMWARTNETZL), (ESUMWARTVATZL - PESUMWARTVATZL),
            (ESUMWARTBRUZL - PESUMWARTBRUZL), :taxgr, vat, :vatreg
              from ROZFAK where DOKUMENT = :document_ref
                and ((ESUMWARTNETZL - PESUMWARTNETZL) <> 0 or (ESUMWARTVATZL - PESUMWARTVATZL) <> 0
                  or (ESUMWARTBRUZL - PESUMWARTBRUZL) <> 0);
        end
      if (walutowa = 1) then
        if (:vtype in (1,2)) then
        begin
          insert into BKVATPOS(BKDOC, NUMBER, NETV, VATE, GROSSV, TAXGR, VATGR,
            VATREG, CURRNETV, VATVF, VATGRF, NET)
            select :bkdoc, :odr, (VESUMWARTNETZL - PVESUMWARTNETZL), 0,
              (VESUMWARTBRUZL - PVESUMWARTBRUZL), :taxgr, vat, :vatreg, (ESUMWARTNET - PESUMWARTNET),
              (VESUMWARTVATZL - PVESUMWARTVATZL), vat, (ESUMWARTNETZL - PESUMWARTNETZL)
              from ROZFAK where DOKUMENT = :document_ref
                and ((VESUMWARTNETZL - PVESUMWARTNETZL) <> 0 or (VESUMWARTVATZL - PVESUMWARTVATZL) <> 0
                  or (VESUMWARTBRUZL - PVESUMWARTBRUZL) <> 0 or (ESUMWARTNET - PESUMWARTNET) <> 0
                  or (ESUMWARTNETZL - PESUMWARTNETZL) <> 0);
        end else
        begin
          insert into BKVATPOS(BKDOC, NUMBER, NETV, VATE, GROSSV, TAXGR, VATGR, VATREG, CURRNETV, NET)
            select :bkdoc, :odr, (VESUMWARTNETZL - PVESUMWARTNETZL), (VESUMWARTVATZL - PVESUMWARTVATZL),
            (VESUMWARTBRUZL - PVESUMWARTBRUZL), :taxgr, vat, :vatreg, (ESUMWARTNET - PESUMWARTNET),
            (ESUMWARTNETZL - PESUMWARTNETZL)
              from ROZFAK where DOKUMENT = :document_ref
                and ((VESUMWARTNETZL - PVESUMWARTNETZL) <> 0 or (VESUMWARTVATZL - PVESUMWARTVATZL) <> 0
                  or (VESUMWARTBRUZL - PVESUMWARTBRUZL) <> 0 or (ESUMWARTNET - PESUMWARTNET) <> 0
                  or (ESUMWARTNETZL - PESUMWARTNETZL) <> 0);
        end
    end
    bkdocvatnumber = :bkdocnumber;
  end

  -- ewentualne dekretowanie
  if (autodecree = 1) then
  begin
    execute procedure FK_AUTODECREE(:bkdoc,:typ,-1);
  end

  execute procedure GET_CONFIG('ACCEPT_WITH_IMPORT', 2) returning_values :i;

  if (i=1 and autodecree = 1) then
  begin
    execute procedure GET_CONFIG('BOOK_WITH_ACCEPT', 2) returning_values :i;
    if (i=1) then
      update bkdocs set status=2, accoper=:operator, bookoper=:operator where ref=:bkdoc;
    else
      update bkdocs set status=1, accoper=:operator where ref=:bkdoc;
  end
end^
SET TERM ; ^
