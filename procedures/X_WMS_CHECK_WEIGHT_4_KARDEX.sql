--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_WMS_CHECK_WEIGHT_4_KARDEX(
      MWSCONSTLOC_IN MWSCONSTLOCS_ID,
      VERS_IN WERSJE_ID,
      QUANTITY_IN DOSTAWA_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable whsec whsecs_id;
declare variable whsecrow whsecrows_id;
declare variable wh wh_ID;           
declare variable maxweight weight_ID; 
declare variable currweight weight_ID; 
declare variable addweight weight_ID;  
declare variable unitweight weight_ID;   
declare variable jedn jedn_miary;
begin
  if (MWSCONSTLOC_IN is null) then
  begin
    STATUS=0;
    msg='Brak podanej lokacji';
    suspend;
    exit;
  end 
  if (VERS_IN is null) then
  begin
    STATUS=0;
    msg='Brak podanej wersji';  
    suspend;
    exit;
  end
  if (QUANTITY_IN is null) then
  begin
    STATUS=0;
    msg='Brak podanej ilosci';
    suspend;
    exit;
  end

  select  c.whsec, c.whsecrow, c.wh
    from mwsconstlocs c
    where c.ref = :mwsconstloc_in
    into  :whsec, :whsecrow, :wh;

  maxweight=0;
  select sum(t.loadcapacity)
    from whsecrows r
      join mwsstands s on (s.whsecrow = r.ref)
      join mwsstandtypes t on (s.mwsstandtype = t.ref)
    where r.ref = :whsecrow
      and r.x_type = 2
    into :maxweight;

  if (maxweight>0) then
  begin
    select sum((s.quantity+s.ordered)*coalesce(j.waga_brutto,j.waga,0))
      from mwsconstlocs c
        join mwsstock s on (c.ref=s.mwsconstloc)
        join wersje w on (s.vers = w.ref)
        join towjedn j on (j.ktm=w.ktm and j.glowna=1)
      where c.whsec=:whsec
        and c.whsecrow=:whsecrow
        and c.wh=:wh
      into :currweight;
      if (currweight is null) then currweight =0;
      --wyc wage

    select :QUANTITY_IN*coalesce(j.waga_brutto,j.waga,0), coalesce(j.waga_brutto,j.waga,0), j.jedn
      from wersje w
        join towjedn j on (j.ktm=w.ktm and j.glowna=1)
      where w.ref=:VERS_IN
      into :addweight, :unitweight, :jedn;

    if ((maxweight-currweight)>=addweight) then
    begin

      STATUS=1;
      msg='OK';

    end
    else begin

      STATUS=0;
      msg='Za ciezki towar dla polki. Z '||QUANTITY_IN||' '||jedn||' zmiesci sie '|| cast(floor((maxweight-currweight)/unitweight) as integer_id)||' '||jedn||'.';

    end
  end
  else begin

    STATUS=1;
    msg='To nie polka Karex';

  end
  suspend;
end^
SET TERM ; ^
