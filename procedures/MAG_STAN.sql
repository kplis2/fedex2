--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_STAN(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      CENA numeric(14,2),
      DOSTAWA integer,
      DATA timestamp)
  returns (
      STAN numeric(14,4),
      WARTOSC numeric(14,2))
   as
begin
  if(:cena = 0) then cena = NULL;
  if(:dostawa = 0) then dostawa = NULL;
  if(:data is null) then data = current_date;
  select SUM(STAN), SUM(WARTOSC) from MAG_STANY(:magazyn,:ktm, :wersja, :cena, :dostawa,:data,0,0) into :stan, :wartosc;
  if(:stan is null) then stan = 0;
  if(:wartosc is null) then wartosc = 0;
  suspend;
end^
SET TERM ; ^
