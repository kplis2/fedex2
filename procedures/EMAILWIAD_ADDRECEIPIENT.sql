--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWIAD_ADDRECEIPIENT(
      EMAILWIADREF integer,
      EMAIL varchar(255) CHARACTER SET UTF8                           ,
      RODZAJ smallint = 0)
   as
begin
  update or insert into EMAILADR(EMAILWIAD,EMAIL,RODZAJ)
  values(:emailwiadref,:email,:rodzaj)
  matching(EMAILWIAD,EMAIL);
end^
SET TERM ; ^
