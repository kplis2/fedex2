--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COMPUTE_EBTURNOVERS(
      DREF integer)
   as
declare variable DEBIT numeric(15,2);
declare variable CREDIT numeric(15,2);
declare variable CURR_DEBIT numeric(15,2);
declare variable CURR_CREDIT numeric(15,2);
declare variable ACCOUNT ACCOUNT_ID;
declare variable PERIOD varchar(6);
declare variable YEARID integer;
declare variable ACCOUNTING integer;
declare variable COUNTRY_CURRENCY varchar(3);
declare variable DECREE_CURRENCY varchar(3);
declare variable COMPANY integer;
declare variable TRNDATE timestamp;
declare variable TRNPERIOD varchar(6);
declare variable BKTYPE integer;
begin
  select D.account, D.debit, D.credit, B.period, P.yearid, D.currency, D.currdebit, D.currcredit, B.company, D.transdate, D.period
    from decrees D
      join bkdocs B on (D.bkdoc=B.ref)
      join bkperiods P on (P.id = D.period and p.company = b.company)
    where D.ref = :DREF
    into :account, :debit, :credit, :period, :yearid, :decree_currency, :curr_debit, :curr_credit, :company, :trndate, :trnperiod;

  execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
    returning_values country_currency;

  select ref
    from accounting
    where company = :company and account = :account and yearid = :yearid and currency = :country_currency
    into :accounting;

  if (accounting is null) then
  begin
    execute procedure GEN_REF('ACCOUNTING') returning_values accounting;
    insert into accounting (ref, company, yearid, account, currency)
      values (:accounting, :company, :yearid, :account, :country_currency);
  end

  if (exists(select first 1 1 from turnoversd where accounting = :accounting and period = :period and trndate = :trndate)) then
  begin
    update turnoversd set ebdebit = ebdebit + :debit, ebcredit = ebcredit + :credit
      where accounting = :accounting and period = :period  and trndate = :trndate;
  end else
  begin
    select bktype from accounting where ref = :accounting into :bktype;
      insert into turnoversd (period, accounting, ebdebit, ebcredit, account, yearid, bktype, company, trndate)
        values (:trnperiod, :accounting, :debit, :credit, :account, :yearid, :bktype, :company, :trndate);
  end

  if (not exists(select first 1 1 from turnovers where accounting = :accounting and period = :period)) then
    exception test_break 'Błąd spójności danych. Brak zapisu w TURNOVERS dla konta ' || account || ' i okresu ' || period;

  update turnovers set ebdebit = ebdebit + :debit, ebcredit = ebcredit + :credit
    where accounting = :accounting and period = :period;

  --ewentualnie jeszcze obroty na koncie w walucie obcej
  if (decree_currency is not null and decree_currency <> country_currency) then
  begin
    accounting = null;
    select ref from accounting
      where company = :company and account = :account and yearid = :yearid and currency = :decree_currency
      into :accounting;

    if (accounting is null) then
    begin
      execute procedure GEN_REF('ACCOUNTING') returning_values accounting;
      insert into accounting (ref, company, yearid, account, currency)
      values (:accounting, :company, :yearid, :account, :decree_currency);
    end

    if(exists(select first 1 1 from turnoversd where accounting = :accounting and period = :period and trndate = :trndate)) then
      update turnoversd set ebdebit = ebdebit + :curr_debit, ebcredit = ebcredit + :curr_credit
        where accounting = :accounting and period = :period  and trndate = :trndate;
    else
    begin
      select bktype from accounting where ref = :accounting into :bktype;
      insert into turnoversd (period, accounting, ebdebit, ebcredit, account, yearid, bktype, company, trndate)
        values (:trnperiod, :accounting, :curr_debit, :curr_credit, :account, :yearid, :bktype, :company, :trndate);
    end

    if (not exists(select first 1 1 from turnovers where accounting = :accounting and period = :period)) then
      exception test_break 'Błąd spójności danych. Brak zapisu w TURNOVERS dla konta ' || account || ' i okresu ' || period;

    update turnovers set ebdebit = ebdebit + :curr_debit, ebcredit = ebcredit + :curr_credit
      where accounting = :accounting and period = :period;
  end
end^
SET TERM ; ^
