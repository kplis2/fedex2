--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_X_SLOWNIKI_PROCESS(
      SESJAREF SESJE_ID,
      TABELA STRING35,
      REF INTEGER_ID,
      BLOKUJZALEZNE SMALLINT_ID = 0,
      TYLKONIEPRZETWORZONE SMALLINT_ID = 0,
      AKTUALIZUJSESJE SMALLINT_ID = 1)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable stabela table_id;
declare variable tmptabela table_id;
declare variable sprocedura string255;
declare variable szrodlo zrodla_id;
declare variable skierunek smallint_id;
declare variable sesja sesje_id;
declare variable sql memo;
declare variable tmpsql memo;
declare variable tmpstatus smallint_id;
declare variable tmpmsg string255;
declare variable error smallint_id;
declare variable errormsg string255;
declare variable error2 smallint_id;
declare variable tmperror smallint_id;
declare variable errormsg2 string255;
declare variable statuspoz smallint_id;
--X_SLOWNIKI
declare variable O_REF INTEGER_ID;
declare variable O_AKT integer_id;
declare variable O_DATAPOCZATEK STRING120;
declare variable O_DATAKONIEC string120;
declare variable O_IDSES string40;
declare variable O_OREF string40;
declare variable O_OTABLE string40;
declare variable O_TYP integer_id;
declare variable O_WARTOSC string255;
declare variable O_TYP_ID string40;
declare variable O_GLOWNY smallint_id;
--zmienne ogolne
declare variable ref_slo integer_id;
declare variable ref_tow integer_id;
declare variable tow_miara jedn_miary;
declare variable tow_kodkresk kodkreskowy;
declare variable tow_ktm kodkreskowy;
declare variable ref_wer integer_id;
declare variable ref_odb integer_id;

begin
  --zmiana pustych wartosci na null-e
  if (sesjaref = 0) then
    sesjaref = null;
  if (trim(tabela) = '') then
    tabela = null;
  if (ref = 0) then
    ref = null;
  if (blokujzalezne is null) then
    blokujzalezne = 0;
  if (tylkonieprzetworzone is null) then
    tylkonieprzetworzone = 0;
  if (aktualizujsesje is null) then
    aktualizujsesje =1;

  status = 1;
  msg ='';
  error = 0;

  --sprawdzenie czy wszystkie potrzebne informacje sa
  if (sesjaref is null and ref is null) then
  begin
    status = -1;
    msg = 'Za malo parametrow.';
    exit; --EXIT
  end

  if (sesjaref is null) then
  begin
    if (ref is null) then
      begin
        status = -1;
        msg = 'Jesli nie podales sesji to wypadaloby podac ref-a...';
        exit; --EXIT
      end
    if (tabela is null) then
    begin
      status = -1;
      msg = 'Wypadaloby podac nazwe tabeli...';
      exit; --EXIT
    end
    else
    begin
      sql = 'select sesja from '||:tabela||' where ref='||:ref;
      execute statement sql into :sesja;
    end
  end
  else
    sesja = sesjaref;

  if (sesja is null) then
  begin
    status = -1;
    msg = 'Nie znaleziono numeru sesji.';
    exit; --EXIT
  end

  --pobranie parametrow sesji
  select s.tabela, s.procedura, s.zrodlo, s.kierunek
    from int_sesje s
    where ref = :sesja
    into :stabela, :sprocedura, :szrodlo, :skierunek;

  --if (tabela is not null) then --jesli podano tabele na wejsciu traktuje ja nadrzednie
    --stabela = tabela;

  if(coalesce(:szrodlo,-1) = -1)then
  begin
    status = -1;
    msg = 'Brakuje id zrodla';
    exit;
  end


  --wlasciwe przetworzenie
  if (stabela = 'INT_IMP_X_SLOWNIKI') then
  begin
    errormsg = '';
    for
      select k.ref, k.akt, k.datapoczatek, k.datakoniec, k.idses, k.oref, k.otable, k.typ, k.wartosc, k.typ_id, k.glowny
          from int_imp_x_slowniki k
          where sesja = :sesja and (ref = :ref or :ref is null)
          order by ref
          into :o_ref, :o_akt, :o_datapoczatek, :o_datakoniec, :o_idses, :o_oref, :o_otable, :o_typ, :o_wartosc, :o_typ_id, :o_glowny
    do begin
      ref_tow = null;
      ref_slo = null;
      -- bledy dla slownikow programow i kontraktow
      if (coalesce(o_wartosc, '')='') then
        begin
          status = -1;
          error = 1;
          errormsg = :errormsg || 'Wartosc slownika jest pusta o id_ses '||coalesce(:o_idses, '')||'.';
        end
      if (coalesce(o_idses, '') = '') then
        begin
          status = -1;
          error = 1;
          errormsg = :errormsg || 'Wartosc idses jest pusta.';
        end
      if (coalesce(o_typ, '') = '') then
        begin
          status = -1;
          error = 1;
          errormsg = :errormsg || 'Wartosc typ slownika dla typ_id '||coalesce(:o_typ_id, '')||' jest pusta.';
        end
      if (coalesce(o_typ_id, '') = '') then
        begin
          status = -1;
          error = 1;
          errormsg = :errormsg || 'Wartosc slownika typ_id jest pusta.';
        end
      if (o_typ = 1) then -- bledy slownika programow
        begin
          if (not exists(select t.ref
            from towary t
            where t.int_id = :o_typ_id))then
          begin
            status = -1;
            error = 1;
            errormsg = :errormsg || 'Brak towaru o typ_id '||coalesce(:o_typ_id,'');
          end
        end
      else if(o_typ = 2) then -- bledy slownika kontraktow
        begin
          if (not exists(select o.ref
            from odbiorcy o
            where o.int_id = :o_typ_id))then
          begin
            status = -1;
            error = 1;
            errormsg = :errormsg || 'Brak odbiorcy o typ_id '||coalesce(:o_typ_id,'');
          end
        end
      if (error = 0) then --dodawanie slownika
        begin
          update or insert into x_slowniki_ehrle (typ, data, akt, data_koniec, wartosc, id_ses, int_dataostprzetw, int_sesjaostprzetw, typ_id, glowny)
            values(:o_typ, :o_datapoczatek, :o_akt, :o_datakoniec, :o_wartosc, :o_idses, current_timestamp(0), :sesja, :o_typ_id, :o_glowny)
            matching (typ, id_ses, typ_id)
          returning ref into :ref_slo;

          if (o_datapoczatek = '')then --kontrola daty
            o_datapoczatek = current_timestamp;
          else
            o_datapoczatek = cast(:o_datapoczatek as timestamp_id);

          if (o_datakoniec = '') then  --kontrola daty
            o_datakoniec = null;
          else
            o_datakoniec = cast(:o_datakoniec as timestamp_id);

          if (o_typ = 1) then --slownik programow
            begin
              if (exists (select first 1 1 from x_slowniki_ehrle se where se.typ = 1 and se.ref <>:ref_slo)
                and :o_glowny = 1
                and :o_akt = 1)
              then
                update x_slowniki_ehrle se --jesli nowy aktywny i glowny, inne glowne = 0
                  set se.glowny = 0
                  where se.id_ses = :o_idses
                  and se.ref <> :ref_slo;
              else
                update x_slowniki_ehrle se --ustaw najnowszy glowny, ktory jest aktywny
                  set se.glowny = 1
                  where se.akt = 1
                  order by se.ref desc
                  rows 1;
              --update wersje
              select t.miara, t.kodkresk, t.ktm
                from towary t
                where t.int_id = :o_typ_id
              into :tow_miara, :tow_kodkresk, :tow_ktm;
              update or insert into wersje (ktm, nazwa, akt, kodkresk,  miara)
                values(:tow_ktm, :o_wartosc, :o_akt, :tow_kodkresk, :tow_miara)
                matching (ktm, nazwa)
              returning ref into :ref_wer;
              update x_slowniki_ehrle se
                set se.otable = 'WERSJE', se.oref = :ref_wer
                where ref = :ref_slo;
            end
          else if (o_typ = 2) then  --slownik kontraktow
            begin
              select o.ref
                from odbiorcy o
                where o.int_id = :o_typ_id
              into :ref_odb;
              update x_slowniki_ehrle se
                set se.otable = 'ODBIORCY', se.oref = :ref_odb
                where ref = :ref_slo;
            end
        end
      end
   end
  if (status < 1 and errormsg is not null) then
    msg = errormsg;
  --aktualizacja informacji o sesji
  if (:aktualizujsesje = 1) then
  begin
    select status, msg from int_sesje_aktualizuj(:sesja, 1) into :tmpstatus, :tmpmsg;
  end

  suspend;
end^
SET TERM ; ^
