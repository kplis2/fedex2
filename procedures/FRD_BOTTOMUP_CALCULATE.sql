--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRD_BOTTOMUP_CALCULATE(
      FRDCELL integer,
      VALTYPEIN varchar(30) CHARACTER SET UTF8                           )
   as
declare variable CREF integer;
declare variable SUMVAL numeric(14,4);
declare variable BOTTOMUP smallint;
declare variable FRDPERSDIMVAL integer;
declare variable FIELDTYPE integer;
declare variable PROC varchar(1024);
declare variable RET numeric(16,2);
declare variable ALG varchar(1024);
declare variable ADDVALTYPE varchar(30);
declare variable VALTYPE varchar(30);
declare variable DECL varchar(8191);
declare variable BODY varchar(8191);
begin
  for
    select frdcellsbottomup.frdcell, frdcellsbottomup.field,
        frdcellsbottomup.bottomup, frdcellsbottomup.addfield
      from frdcellsbottomup
      where frdcellsbottomup.frdcelladd = :frdcell and frdcellsbottomup.addfield = :valtypein
      into :cref, :valtype, :bottomup, :addvaltype
  do begin
    --czyszczenie drilldowna
    if (valtype = 'PLANAMOUNT') then
      delete from frddrilldown where frdcell = :cref and valtype = 0;
    else if (valtype = 'REALAMOUNT') then
      delete from frddrilldown where frdcell = :cref and valtype = 1;
    else if (valtype = 'VAL1') then
      delete from frddrilldown where frdcell = :cref and valtype = 2;
    else if (valtype = 'VAL2') then
      delete from frddrilldown where frdcell = :cref and valtype = 3;
    else if (valtype = 'VAL3') then
      delete from frddrilldown where frdcell = :cref and valtype = 4;
    else if (valtype = 'VAL4') then
      delete from frddrilldown where frdcell = :cref and valtype = 5;
    else if (valtype = 'VAL5') then
      delete from frddrilldown where frdcell = :cref and valtype = 6;

    -- zaleznosci bottom-up -> naliczanie zaleznosci w gore
    if (:bottomup = 1) then
    begin
      if (:addvaltype = 'PLANAMOUNT') then
      begin
        select sum(frdcells.planamount)
          from frdcells
            join frdcellsbottomup on (frdcellsbottomup.frdcelladd = frdcells.ref
                and frdcellsbottomup.addfield = :addvaltype and frdcellsbottomup.field = :valtype)
          where frdcellsbottomup.frdcell = :cref and frdcellsbottomup.bottomup = 1
          into :sumval;
      end
      if (:addvaltype = 'REALAMOUNT') then
      begin
        select sum(frdcells.realamount)
          from frdcells
            join frdcellsbottomup on (frdcellsbottomup.frdcelladd = frdcells.ref
                and frdcellsbottomup.addfield = :addvaltype and frdcellsbottomup.field = :valtype)
          where frdcellsbottomup.frdcell = :cref
            and frdcellsbottomup.bottomup = 1
          into :sumval;
      end
      if (:addvaltype = 'VAL1') then
      begin
        select sum(frdcells.val1)
          from frdcells
            join frdcellsbottomup on (frdcellsbottomup.frdcelladd = frdcells.ref
              and frdcellsbottomup.addfield = :addvaltype and frdcellsbottomup.field = :valtype)
          where frdcellsbottomup.frdcell = :cref and frdcellsbottomup.bottomup = 1
          into :sumval;
      end
      if (:addvaltype = 'VAL2') then
      begin
        select sum(frdcells.val2)
          from frdcells
            join frdcellsbottomup on (frdcellsbottomup.frdcelladd = frdcells.ref
                and frdcellsbottomup.addfield = :addvaltype and frdcellsbottomup.field = :valtype)
          where frdcellsbottomup.frdcell = :cref and frdcellsbottomup.bottomup = 1
          into :sumval;
      end
      if (:addvaltype = 'VAL3') then
      begin
        select sum(frdcells.val3)
          from frdcells
            join frdcellsbottomup on (frdcellsbottomup.frdcelladd = frdcells.ref
                and frdcellsbottomup.addfield = :addvaltype and frdcellsbottomup.field = :valtype)
          where frdcellsbottomup.frdcell = :cref and frdcellsbottomup.bottomup = 1
          into :sumval;
      end
      if (:addvaltype = 'VAL4') then
      begin
        select sum(frdcells.val4)
          from frdcells
            join frdcellsbottomup on (frdcellsbottomup.frdcelladd = frdcells.ref
                and frdcellsbottomup.addfield = :addvaltype and frdcellsbottomup.field = :valtype)
          where frdcellsbottomup.frdcell = :cref and frdcellsbottomup.bottomup = 1
          into :sumval;
      end
      if (:addvaltype = 'VAL5') then
      begin
        select sum(frdcells.val5)
          from frdcells
            join frdcellsbottomup on (frdcellsbottomup.frdcelladd = frdcells.ref
                and frdcellsbottomup.addfield = :addvaltype and frdcellsbottomup.field = :valtype)
          where frdcellsbottomup.frdcell = :cref and frdcellsbottomup.bottomup = 1
          into :sumval;
      end
      if (:sumval is not null) then
      begin
        if (:valtype = 'PLANAMOUNT') then
          update frdcells set frdcells.planamount = :sumval where frdcells.ref = :cref;
        else if (:valtype = 'REALAMOUNT') then
          update frdcells set frdcells.realamount = :sumval where frdcells.ref = :cref;
        else if (:valtype = 'VAL1') then
          update frdcells set frdcells.val1 = :sumval where frdcells.ref = :cref;
        else if (:valtype = 'VAL2') then
          update frdcells set frdcells.val2 = :sumval where frdcells.ref = :cref;
        else if (:valtype = 'VAL3') then
          update frdcells set frdcells.val3 = :sumval where frdcells.ref = :cref;
        else if (:valtype = 'VAL4') then
          update frdcells set frdcells.val4 = :sumval where frdcells.ref = :cref;
        else if (:valtype = 'VAL5') then
          update frdcells set frdcells.val5 = :sumval where frdcells.ref = :cref;
      end
    -- zaleznosci wynikajace z dependenses -> adresy wzgledne z arkuszy
    end else if (:bottomup = 2) then
    begin
      if (:valtype = 'PLANAMOUNT') then
      begin
        fieldtype = 0;
        select c.frdpersdimval, p.planformula, p.body, p.decl
          from frdcells c
            join frdpersdimvals p on (p.ref = c.frdpersdimval)
          where c.ref = :cref
          into :frdpersdimval, :alg, :body, :decl;
      end else if (:valtype = 'REALAMOUNT') then
      begin
        fieldtype = 1;
        select c.frdpersdimval, p.realformula, p.bodyreal, p.declreal
          from frdcells c
            join frdpersdimvals p on (p.ref = c.frdpersdimval)
          where c.ref = :cref
          into :frdpersdimval, :alg, :body, :decl;
      end else if (:valtype = 'VAL1') then
      begin
        fieldtype = 2;
        select c.frdpersdimval, p.val1formula, p.bodyval1, p.declval1
          from frdcells c
            join frdpersdimvals p on (p.ref = c.frdpersdimval)
          where c.ref = :cref
          into :frdpersdimval, :alg, :body, :decl;
      end else if (:valtype = 'VAL2') then
      begin
        fieldtype = 3;
        select c.frdpersdimval, p.val2formula, p.bodyval2, p.declval2
          from frdcells c
            join frdpersdimvals p on (p.ref = c.frdpersdimval)
          where c.ref = :cref
          into :frdpersdimval, :alg, :body, :decl;
      end else if (:valtype = 'VAL3') then
      begin
        fieldtype = 4;
        select c.frdpersdimval, p.val3formula, p.bodyval3, p.declval3
          from frdcells c
            join frdpersdimvals p on (p.ref = c.frdpersdimval)
          where c.ref = :cref
          into :frdpersdimval, :alg, :body, :decl;
      end else if (:valtype = 'VAL4') then
      begin
        fieldtype = 5;
        select c.frdpersdimval, p.val4formula, p.bodyval4, p.declval4
          from frdcells c
            join frdpersdimvals p on (p.ref = c.frdpersdimval)
          where C.ref = :cref
          into :frdpersdimval, :alg, :body, :decl;
      end else if (:valtype = 'VAL5') then
      begin
        fieldtype = 6;
        select c.frdpersdimval, p.val5formula, p.bodyval5, p.declval5
          from frdcells c
            join frdpersdimvals p on (p.ref = c.frdpersdimval)
          where c.ref = :cref
          into :frdpersdimval, :alg, :body, :decl;
      end
      if (:alg is not null and :alg <> '') then
      begin
        update frdcellsbottomup set frdcellsbottomup.isactual = 0
          where frdcellsbottomup.frdcell = :cref and frdcellsbottomup.bottomup = 2
            and frdcellsbottomup.field = :valtype;

        execute procedure STATEMENT_EXECUTE(:decl, :body, :cref, :fieldtype, 0)
          returning_values :ret;
        if (:valtype = 'PLANAMOUNT') then
          update frdcells set frdcells.planamount = :ret where frdcells.ref = :cref;
        else if (:valtype = 'REALAMOUNT') then
          update frdcells set frdcells.realamount = :ret where frdcells.ref = :cref;
        else if (:valtype = 'VAL1') then
          update frdcells set frdcells.val1 = :ret where frdcells.ref = :cref;
        else if (:valtype = 'VAL2') then
          update frdcells set frdcells.val2 = :ret where frdcells.ref = :cref;
        else if (:valtype = 'VAL3') then
          update frdcells set frdcells.val3 = :ret where frdcells.ref = :cref;
        else if (:valtype = 'VAL4') then
          update frdcells set frdcells.val4 = :ret where frdcells.ref = :cref;
        else if (:valtype = 'VAL5') then
          update frdcells set frdcells.val5 = :ret where frdcells.ref = :cref;
        delete from frdcellsbottomup where frdcellsbottomup.isactual = 0
          and frdcellsbottomup.frdcell = :cref and frdcellsbottomup.bottomup = 2
          and frdcellsbottomup.field = :valtype;
      end
    end
  end
end^
SET TERM ; ^
