--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWSCONSTLOCS_SET_GOODSAV(
      WH varchar(3) CHARACTER SET UTF8                           ,
      DEEP smallint,
      ALLDEEP smallint,
      UPLEVEL smallint)
   as
declare variable mwsconstloc integer;
begin
  update mwsconstlocs set goodsav = 0 where wh = :wh and mwsconstloctype <> 10;
  update mwsstock set goodsav = 0 where wh = :wh and goodsav <> 4;
  for
    select m.ref
      from mwsconstlocs m
        join whareas w on (w.ref = m.wharea)
      where w.areatype = 1 and m.deep = 0 and (m.coordzb = 0 or m.mwsconstloctype = 9)
        and (m.mwsstandlevelnumber = 1 or m.mwsconstloctype = 9) and m.wh = :wh
      into mwsconstloc
  do begin
    update mwsconstlocs set goodsav = 1 where ref = :mwsconstloc;
    update mwsstock set goodsav = 1 where mwsconstloc = :mwsconstloc;
  end
  for
    select m.ref
      from mwsconstlocs m
        join whareas w on (w.ref = m.wharea)
      where w.areatype = 1 and m.deep = 1 and :deep = 1
        and (m.coordzb = 0 or m.mwsconstloctype = 9)
        and (m.mwsstandlevelnumber = 1 or m.mwsconstloctype = 9) and m.wh = :wh
      into mwsconstloc
  do begin
    update mwsconstlocs set goodsav = 2 where ref = :mwsconstloc;
    update mwsstock set goodsav = 2 where mwsconstloc = :mwsconstloc;
  end
  for
    select m.ref
      from mwsconstlocs m
        join whareas w on (w.ref = m.wharea)
      where w.areatype = 1 and m.deep = 0 and m.mwsstandlevelnumber <= :uplevel
        and m.mwsstandlevelnumber > 1 and m.wh = :wh
        and m.mwsconstloctype <> 9
      into mwsconstloc
  do begin
    update mwsconstlocs set goodsav = 3 where ref = :mwsconstloc;
    if (exists (select s.ref from mwsstock s join mwsconstlocsymbs ms on (ms.mwsconstloc = s.mwsconstloc)
      where s.mwsconstloc = :mwsconstloc and s.good = ms.symbol and s.mixedpalgroup = 0
      and s.ispal = 0 and ms.const = 1)
    ) then
      update mwsstock set goodsav = 3 where mwsconstloc = :mwsconstloc;
  end
end^
SET TERM ; ^
