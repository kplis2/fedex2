--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NWD(
      A integer,
      B integer)
  returns (
      RET integer)
   as
begin
 --DS: obliczanie najwiekszego wspolnego dzielnika
  if (a=b) then ret = a;
  else if (a > b) then
    select ret from nwd(:a-:b,:b) into :ret;
  else select ret from nwd(:a,:b-:a) into :ret;
  suspend;
end^
SET TERM ; ^
