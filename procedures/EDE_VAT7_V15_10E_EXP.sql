--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_VAT7_V15_10E_EXP(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(8191) CHARACTER SET UTF8                           )
   as
--zmianna pomocnicza
declare variable ISDECLARATION integer;
declare variable DEF varchar(20);
declare variable edeclddef integer;
declare variable CODE varchar(10);
declare variable PFIELD varchar(10);
declare variable SYSTEMCODE varchar(20);
declare variable OBLIGATIONKIND varchar(20);
declare variable SCHEMAVER varchar(10);
declare variable SYMBOL varchar(10);
declare variable VARIANT varchar(10);
declare variable PVALUE varchar(255);
declare variable TMPVAL1 varchar(255);
declare variable TMPVAL2 varchar(255);
declare variable TMP smallint;
declare variable CURR_P integer;
declare variable CORRECTION smallint;
declare variable ENCLOSUREOWNER integer;
declare variable ZALTYP varchar(20);
declare variable ISZAL smallint;
declare variable PARENT_LVL_0 integer;
declare variable PARENT_LVL_1 integer;
declare variable PARENT_LVL_2 integer;
declare variable PARENT_LVL_3 integer;
begin
  --Sprawdzamy czy dana deklaracja istnieje do eksportu
  select first 1 1
    from edeclarations e
    where e.ref = :oref
  into :isDeclaration;

  if(isDeclaration is null) then
    exception universal'Niepoprawny identyfikator e-deklaracji';

  --Pobieranie definicji
  select e.edecldef, ed.code, ed.systemcode, ed.obligationkind, ed.schemaver, ed.symbol, ed.variant, ed.ref
    from edeclarations e
      join edecldefs ed on (ed.ref = e.edecldef)
    where e.ref = :oref
  into :def, :code, :systemcode, :obligationkind, :schemaver, :symbol, :variant, :edeclddef;

  --Generujemy naglowek pliku XML
  id = 0;
  name = 'Deklaracja';
  parent = null;
  params = 'xmlns="http://crd.gov.pl/wzor/2015/09/04/2567/" xmlns:etd="http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2011/06/21/eD/DefinicjeTypy/"';
  val = null;
  suspend;

  correction = 0;
  parent_lvl_0 = 0;
  --Rozpoczynamy naglowek e-deklaracji
  id = :id + 1; --1
  name = 'Naglowek';
  parent = parent_lvl_0;
  params = null;
  val  = null;
  parent_lvl_1 = :id;
  suspend;
  --cialo naglowka

    --KodFormularza
    id = id+1;
    name ='KodFormularza';
    parent = parent_lvl_1;
    params ='';
    if(code is not null) then
    params = params||'kodPodatku="'||code||'" ';
    if(systemcode is not null) then
    params = params||'kodSystemowy="'||systemcode||'" ';
    if(obligationkind is not null) then
    params = params||'rodzajZobowiazania="'||obligationkind||'" ';
    if(schemaver is not null) then
    params = params||'wersjaSchemy="'||schemaver||'"';
    val = symbol;
    suspend;
    
    --WariantFormularza
    id = id+1;
    name='WariantFormularza';
    parent = parent_lvl_1;
    params = null;
    val = variant;
    suspend;
    
    --CelZlozenia
    id = id+1;
    name='CelZlozenia';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.field = 7
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params = 'poz="P_7"';
      val = :pvalue;
      suspend;
    end

    if(:pvalue = 2) then
      correction = 1;
    pvalue = null;
    --Rok
    id = id+1;
    name='Rok';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.field = 5
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params =null;
      val = :pvalue;
      suspend;
    end
    pvalue = null;
    --Miesiac
    id = id+1;
    name='Miesiac';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.field = 4
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params =null;
      val = :pvalue;
      suspend;
    end
    pvalue = null;
    --KodUrzedu
    id = id+1;
    name='KodUrzedu';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.field = 6
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params = null;
      val = :pvalue;
      suspend;
    end
    --Koniec Naglowka
    
  --Podmiot
  id = id+1;
  name='Podmiot1';
  parent = parent_lvl_0;
  params = 'rola="Podatnik"';
  val = null;
  parent_lvl_1 = :id;
  suspend;
    pvalue = null;
    select epos.pvalue
        from edeclpos epos
        where epos.edeclaration = :oref and epos.field = 8
      into :pvalue;
    if(:pvalue = 1) then
    begin
    --OsobaNiefizyczna
    id = id+1;
    name='OsobaNiefizyczna';
    parent = parent_lvl_1;
    params = null;
    val = null;
    parent_lvl_2 = :id;
    suspend;
      pvalue = null;
      --NIP
      id = id+1;
      name='NIP';
      parent = parent_lvl_2;
      select REPLACE(epos.pvalue, '-', '')
        from edeclpos epos
        where epos.edeclaration = :oref and epos.field = 1
      into :pvalue;
      params = null;
      val = :pvalue;
      suspend;
      pvalue = null;
      --PelnaNazwa && REGON
      tmp =0;
      select epos.pvalue
      from edeclpos epos
        where epos.edeclaration = :oref and epos.field = 9
      into :pvalue;
      for select outstring
        from parse_lines(:pvalue, ';')
      into :tmpval2
      do begin
      if(tmp = 0)then
      begin
        --PelnaNazwa
        id = id+1;
        name='PelnaNazwa';
        parent = parent_lvl_2;
        val = tmpval2;
        params = null;
        suspend;
      end
      else if(tmp = 1) then
      begin
        --REGON
        id = id+1;
        name='REGON';
        parent = parent_lvl_2;
        val = tmpval2;
        params = null;
        suspend;
      end
      tmp = :tmp + 1;
    end
    end
    else if(:pvalue = 2) then
    begin
    --OsobaNiefizyczna
    id = id+1;
    name='etd:OsobaFizyczna';
    parent = parent_lvl_1;
    params = null;
    val = null;
    parent_lvl_2 = :id;
    suspend;
      pvalue = null;
      --NIP
      id = id+1;
      name='etd:NIP';
      parent = parent_lvl_2;
      select REPLACE(epos.pvalue, '-', '')
        from edeclpos epos
        where epos.edeclaration = :oref and epos.field = 1
      into :pvalue;
      params = null;
      val = :pvalue;
      suspend;
      pvalue = null;
      --ImiePierwsze && Nazwisko && DataUrodzenia
      tmp =0;
      select epos.pvalue
      from edeclpos epos
        where epos.edeclaration = :oref and epos.field = 9
      into :pvalue;
      for select outstring
        from parse_lines(:pvalue, ';')
      into :tmpval2
      do begin
      if(tmp = 0) then
      begin
        tmpval1 = :tmpval2;
      end
      if(tmp = 1) then
      begin
      --PelnaNazwa
        id = id+1;
        name='etd:ImiePierwsze';
        parent = parent_lvl_2;
        val = tmpval2;
        params = null;
        suspend;

        --Nazwisko
        id = id+1;
        name='etd:Nazwisko';
        parent = parent_lvl_2;
        val = tmpval1;
        params = null;
        suspend;
      end
      if(tmp = 2) then
      begin
        --DataUrodzenia
        id = id+1;
        name='etd:DataUrodzenia';
        parent = parent_lvl_2;
        val = tmpval2;
        params = null;
        suspend;
      end
      tmp = :tmp + 1;
      end
    end

    --PozycjeSzczegolowe
    id = id+1;
    name='PozycjeSzczegolowe';
    parent = parent_lvl_0;
    params =null;
    val = null;
    parent_lvl_1 = :id;
    suspend;
    pvalue = null;
      -- algorytm uzupelniajacy pozycje
      -- od 10 do 55
      -- dodanie pol obowiazkowych
      update edeclarations e set e.state = 0 where e.ref = :oref;
      --rdb$set_context('USER_TRANSACTION', 'EDE_PROCESS_RESPOND_Run', '1');
      for
        select d.field, d.ref, case d.datatype when 0 then '' when 1 then '0' when 2 then current_date when 3 then '0' else '' end
          from edeclposdefs d
            left join edeclpos p on (d.ref = p.edeclposdef)
          where d.edecldef = :edeclddef
            and d.field in (35, 36, 45, 48, 50, 51, 55, 69)
            and p.ref is null
          into :pfield, :tmp, :pvalue
      do
        INSERT INTO EDECLPOS (EDECLARATION, FIELD, PVALUE, EDECLPOSDEF)
          VALUES (:oref, :pfield, :pvalue, :tmp);

      --rdb$set_context('USER_TRANSACTION', 'EDE_PROCESS_RESPOND_Run', '0');
      update edeclarations e set e.state = 0 where e.ref = :oref;
      for select epos.pvalue, epos.field
        from edeclpos epos
        where epos.edeclaration = :oref
          and ((epos.field >= 10 and epos.field <= 67)
          or epos.field in (71,72))
        order by epos.field
        into :pvalue, :pfield
      do begin
        if((:pvalue is not null and (:pfield between 10 and 58)) or (:pfield between 59 and 62 and :pvalue<>0)) then
        begin
          id = id+1;
          name='P_'||:pfield;
          parent = parent_lvl_1;
          params =null;
          val = upper(:pvalue);
          suspend;
        end
        if(:pvalue is not null and :pfield between 63 and 66) then
        begin
          id = id+1;
          name='P_'||:pfield;
          parent = parent_lvl_1;
          params =null;
          if(:pvalue = 0) then val = 2;
            else val = upper(:pvalue);
          suspend;
        end
        if(:pvalue > 0 and :pfield = 67) then
        begin
          id = id+1;
          name='P_'||:pfield;
          parent = parent_lvl_1;
          params =null;
          if(:pvalue = 0) then val = 2;
            else val = upper(:pvalue);
          suspend;
        end
      end

    --Pouczenie
    id = id+1;
    name='Pouczenie1';
    parent = parent_lvl_0;
    params =null;
    val =  'W przypadku niewpłacenia w obowiązującym terminie kwoty z poz. 51 lub wpłacenia jej w niepełnej wysokości, niniejsza deklaracja stanowi podstawę do wystawienia tytułu wykonawczego zgodnie z przepisami ustawy z dnia 17 czerwca 1966 r. o postępowaniu egzekucyjnym w administracji (Dz. U. z 2014 r. poz. 1619, z późn. zm.).';
    suspend;

    --Oswiadczenie
    id = id+1;
    name='Pouczenie2';
    parent = parent_lvl_0;
    params =null;
    val = 'Za podanie nieprawdy lub zatajenie prawdy i przez to narażenie podatku na uszczuplenie grozi odpowiedzialność przewidziana w Kodeksie karnym skarbowym.';
    suspend;

end^
SET TERM ; ^
