--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_R_OBROT(
      DATASTAN timestamp,
      ZAKRES smallint,
      ACCOUNTMASK ACCOUNT_ID,
      COMPANY integer)
  returns (
      REF integer,
      SETTLEMENT varchar(20) CHARACTER SET UTF8                           ,
      ACCOUNT ACCOUNT_ID,
      PAYDATE timestamp,
      DESCRIPT varchar(400) CHARACTER SET UTF8                           ,
      ODEBIT numeric(15,2),
      OCREDIT numeric(15,2),
      DEBIT numeric(15,2),
      CREDIT numeric(15,2),
      ADEBIT numeric(15,2),
      ACREDIT numeric(15,2),
      BDEBIT numeric(15,2),
      BCREDIT numeric(15,2),
      ISROZRACH smallint,
      NRROZRACH integer)
   as
declare variable yearid integer;
declare variable old_account ACCOUNT_ID;
declare variable tmp_debit numeric(15,2);
declare variable tmp_credit numeric(15,2);
declare variable bkaccounts_ref integer;
declare variable sep varchar(1);
declare variable dictdef integer;
declare variable len smallint;
declare variable tmp ACCOUNT_ID;
declare variable descripttmp varchar(80);
declare variable i smallint;
declare variable j smallint;
declare variable k smallint;
declare variable dictref integer;
declare variable periodid varchar(6);
declare variable slodef integer;
declare variable slopoz integer;
declare variable typ char(1);
declare variable dataod timestamp;
declare variable datado timestamp;
begin
  ref = 0;
  NRROZRACH = 0;
  old_account = '';
  execute procedure create_account_mask(accountmask) returning_values accountmask;
  if (accountmask <> '') then accountmask = accountmask || '%';
  else if (accountmask = '') then accountmask = null;
  select ID, SDATE, FDATE
    from BKPERIODS
    where company = :company and SDATE <= :datastan and FDATE >= :datastan and PTYPE = 1
      and bkperiods.company = :company
    into :periodid, :dataod, :datado;
  select yearid
    from BKPERIODS where ID = :periodid
      and company = :company
    into :yearid;
  for select kontofk, symbfak, slodef, slopoz, dataplat, typ
    from ROZRACH
    where (datazamk is null or datazamk >= :dataod)
      and dataotw <= :datastan
      and ((:zakres <> 1) or (:zakres = 1 and dataplat < :datastan))
      and ((:accountmask is null) or (:accountmask = '') or (kontofk like :accountmask))
      and rozrach.company = :company
    order by kontofk
    into :account, :settlement, :slodef, :slopoz, :paydate, :typ
  do begin
    debit = null;
    credit = null;
    odebit = null;
    ocredit = null;
    bdebit = null;
    bcredit = null;
    if (account <> old_account) then NRROZRACH = 0;
    NRROZRACH = :NRROZRACH + 1;

    isrozrach = 0;
    select sum(winienzl), sum(mazl)
      from ROZRACHP
        where KONTOFK = :account and SYMBFAK = :settlement and (rozrachp.data >= :dataod and rozrachp.data <= :datastan)
          and rozrachp.company = :company
      into :debit, :credit;
    if (debit is null) then debit = 0;
    if (credit is null) then credit = 0;
    isrozrach = 1;
    if (zakres = 0) then begin -- nie zbilansowane na koniec daty wprowadzonej jako parametr a nie zbilansowane w okresie danego miesiąca
      select sum(winienzl), sum(mazl) from ROZRACHP
        where KONTOFK = :account and SYMBFAK = :settlement and rozrachp.data <= :datastan
          and rozrachp.company = :company
      into :tmp_debit, :tmp_credit;
      if (tmp_credit = tmp_debit) then isrozrach = 0;
    end
    if (isrozrach = 1) then begin
      select sum(winienzl), sum(mazl)
        from ROZRACHP where KONTOFK = :account and SYMBFAK = :settlement
        and (rozrachp.data < :dataod) and rozrachp.company = :company
      into :odebit, :ocredit;
      if (odebit is null) then odebit = 0;
      if (ocredit is null) then ocredit = 0;
      acredit = ocredit + credit;
      adebit = odebit + debit;
      if (acredit > adebit) then
        bcredit = acredit - adebit;
      else
        bdebit = adebit - acredit;
      if (bdebit is null) then bdebit = 0;
      if (bcredit is null) then bcredit = 0;
      ref = ref + 1;
      isrozrach = 1;


      descript = '';
      select descript, ref
        from bkaccounts
        where symbol=substring(:account from 1 for 3)
        and yearid = :yearid
        and company = :company
      into :descript, :bkaccounts_ref;

      dictdef = null;
      i = 0;
      -- zliczam ilosc poziomów nalitycznych
      for select separator, dictdef, len
        from accstructure
        where bkaccount = :bkaccounts_ref
        order by nlevel
        into :sep, :dictdef, :len
      do begin
        i = i + 1;
      end
      if (i > 0) then
      begin
        j = -1;
        k = 4;
        for select separator, dictdef, len
          from accstructure
          where bkaccount = :bkaccounts_ref
          order by nlevel
        into :sep, :dictdef, :len
        do begin
          j = j + 1;
          if (j < i) then
          begin
            if (sep <> ',') then
              k = k +1;
            tmp = substring(:account from  k for  k + len - 1);
            execute procedure name_from_bksymbol(company, dictdef, tmp)
              returning_values descripttmp, dictref;
            ref = ref + 1;
            descript = descript || ' ' || descripttmp;
            k = k + len;
          end
        end
      end
      suspend;
    end
    old_account = account;
  end
end^
SET TERM ; ^
