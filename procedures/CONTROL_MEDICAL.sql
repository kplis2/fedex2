--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CONTROL_MEDICAL
  returns (
      PERSONNAMES varchar(120) CHARACTER SET UTF8                           ,
      TODATE date)
   as
declare variable columns varchar(255);
  declare variable ecolumn integer;
  declare variable absence integer;
  declare variable afromdate date;
  declare variable atodate date;
begin
  --DS: procedura wykorzystywana przez powiadomienie o badaniach kontrolnych
  columns = ';';
  for
    select c.number
      from ecolumns c
      where c.typ containing ';CHR;'
      into :ecolumn
  do
    columns = columns||ecolumn||';';

  for
    select max(a.ref), max(a.todate), e.personnames
      from employees e
        join eabsences a on a.employee = e.ref
        join ecolumns c on c.number = a.ecolumn
      where c.typ containing ';CHR;'
        and a.todate > current_date - 7
        and a.todate <= current_date + 7
      group by a.employee, e.personnames
      into :absence, :todate, :personnames
  do begin
    execute procedure e_get_whole_eabsperiod(:absence, null, null, null,null,  null, :columns)
      returning_values :afromdate, :atodate;
    if (atodate = todate and atodate - afromdate + 1 > 30) then
      suspend;
  end
end^
SET TERM ; ^
