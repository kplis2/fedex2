--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TOWARY_DANE(
      KTM varchar(60) CHARACTER SET UTF8                           ,
      WERSJA integer)
  returns (
      WCENA_ZAKN numeric(14,2),
      WMARZAMIN numeric(14,2),
      TCENA_ZAKN numeric(14,2),
      TMARZAMIN numeric(14,2))
   as
begin
 wcena_zakn = 0;
 wmarzamin = 0;
 tcena_zakn = 0;
 tmarzamin = 0;
 select coalesce(w.cena_zakn,0), coalesce(w.marzamin,0), coalesce(t.cena_zakn,0), coalesce(t.marzamin,0)
   from wersje w
   join towary t on (w.ktm = t.ktm)
   where w.ktm = :ktm
   and w.nrwersji = :wersja
   into :wcena_zakn, :wmarzamin, :tcena_zakn, :tmarzamin;
end^
SET TERM ; ^
