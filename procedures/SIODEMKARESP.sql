--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SIODEMKARESP(
      EDEDOCSH_REF integer)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
   as
  declare variable LISTYWYSDREF integer;
  declare variable NAME varchar(255);
  declare variable VAL varchar(8191);
  declare variable numerpaczki numeric(14);
  declare variable paczkaref LISTYWYSDROZ_OPK_ID;
  declare variable row1 integer;
  declare variable row2 integer;
  declare variable numerator integer;
  declare variable listapaczek string1024;
  declare variable listaededocsp string1024;
begin
  select oref from ededocsh where ref = :ededocsh_ref
    into :listywysdref;
  for select name,val from ededocsp where ededoch = :ededocsh_ref
    into :name,:val
    do begin
      if(:name = 'NrPrzesylki') then  begin
      --        exception test_break :val ||' NRPRZE';
        update listywysd set NUMERPRZESYLKI = :val where ref = :listywysdref;          end
      else if(:name = 'EtykietaEPL') then begin
        update listywysd set ETYKIETAEPL = :ededocsh_ref where ref = :listywysdref;
        select list(ref,';') from listywysdroz_opk where listwysd = :listywysdref into :listapaczek;
        select list(substring(val from position('A35,188,0,3,2,2,N,"' in val)+19 for 13),';') from ededocsp where ededoch = :ededocsh_ref and name = 'EtykietaEPL' into :listaededocsp;
        for select lp.STRINGOUT, ld.stringout from parsestring(:listapaczek, ';') lp
          left join parsestring(:listaededocsp, ';') ld on (1=1 and lp.lp = ld.lp)
          into :paczkaref, :numerpaczki
        do begin
          update LISTYWYSDROZ_OPK set nrpaczki = :numerpaczki, waga = 31.5 where ref = :paczkaref;
        end
        exit;
      end
    end
  otable = 'listywysd';
  oref = :listywysdref;
  suspend;
end^
SET TERM ; ^
