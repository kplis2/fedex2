--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHECK_DICTPOS_SYMBOL(
      COMPANY integer,
      DICTDEF integer,
      SYMBOL varchar(30) CHARACTER SET UTF8                           )
  returns (
      RES integer)
   as
declare variable result varchar(255);
begin
  result = null;
  execute procedure name_from_bksymbol(company,  dictdef, symbol)
    returning_values result, res;
  if (result is null) then
    res = 0;
  else
    res = 1;
  suspend;
end^
SET TERM ; ^
