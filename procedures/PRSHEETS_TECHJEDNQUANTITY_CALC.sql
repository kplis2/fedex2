--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHEETS_TECHJEDNQUANTITY_CALC(
      PRSHEET integer)
   as
declare variable konfigs varchar(255);
declare variable konfig numeric(14,4);
declare variable quantity numeric(14,4);
declare variable opertime numeric(14,4);
declare variable batchquantity numeric(14,4);
begin
  execute procedure getconfig('TECHJEDNQUANTITY') returning_values :konfigs;
  konfig = cast(konfigs as numeric(14,4));
  select quantity, batchquantity from prsheets where ref = :prsheet into :quantity, :batchquantity;
  select sum(opertime) from prshopers where sheet = :prsheet into opertime;
  if(quantity > 0) then
    update prsheets
      set techjednquantity = cast ((cast(:opertime as double precision) * :konfig * :batchquantity)  / (24 * :quantity) as numeric(14,4))
      where ref = :prsheet;
end^
SET TERM ; ^
