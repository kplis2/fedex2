--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE VATDEBITEDSTRUCTPROCENT(
      BKDOC BKDOCS_ID)
  returns (
      PROCENTVALUE PROCENT)
   as
begin
  procentvalue = null;
  --Wyszukiwanie wspóczynnika struktury który bdzie uzupeniany przez operatora
  --Wspomagać w tym ma analiza
  select byr.factorstruct
    from bkdocs bd join bkperiods bp on ( bd.period = bp.id and bd.company = bp.company)
      join bkyears byr on (byr.company = bd.company and byr.yearid = bp.yearid)
     where bd.ref = :bkdoc
     into :procentvalue;
  if(procentvalue is null) then
    exception vat_odl_struct;
   suspend;
end^
SET TERM ; ^
