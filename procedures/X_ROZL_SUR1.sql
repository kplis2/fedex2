--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_ROZL_SUR1(
      KTM varchar(80) CHARACTER SET UTF8                           ,
      DATA_OD timestamp,
      DATA_DO timestamp)
  returns (
      KTM_SUR varchar(80) CHARACTER SET UTF8                           ,
      JEDN integer,
      OPIS_SUR varchar(255) CHARACTER SET UTF8                           ,
      ZUZ_FAKT numeric(14,4))
   as
DECLARE VARIABLE PRSHEET INTEGER;
DECLARE VARIABLE PRSHMAT INTEGER;
DECLARE VARIABLE REF_RWP INTEGER;
begin
  select max(ref) from prsheets where ktm = :ktm and status = 2 into :prsheet;
  for select ktm, descript, jedn   from prshmat
    where sheet=:prsheet
    into :ktm_sur, :opis_sur,  :jedn
  do begin
/*    select descript from prshmat where ref=:prshmat into:opis_sur;
      select jedn from prshmat  where ref=:prshmat into :jedn;

      select ref from dokumnag
      where typ = 'RWP' and ref in
          (select ref2 from dokumnag where typ = 'PWP' and ref in
          (select dokument from dokumpoz where ktm=:ktm)) into: ref_rwp;
  */

    select SUM(dp.ilosc)
      from dokumpoz dp

          left join  dokumnag d on (dp.dokument = d.ref)
           left join dokumnag d2 on (d.ref = d2.ref2)
           left join dokumpoz dp2 on (dp2.dokument = d2.ref and dp2.ktm =:ktm)
      where dp.ktm = :ktm_sur and d.typ='RWP' and d2.typ = 'PWP'
      and d2.data between :data_od and :data_do

      group by dp.ktm
    into :zuz_fakt;
    suspend ;
  end
end^
SET TERM ; ^
