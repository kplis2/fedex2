--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_E_ANNEX(
      ANNEX integer)
  returns (
      NUMBER smallint,
      NAME varchar(80) CHARACTER SET UTF8                           ,
      FROMVAL varchar(80) CHARACTER SET UTF8                           ,
      TOVAL varchar(80) CHARACTER SET UTF8                           ,
      BANNEX integer)
   as
declare variable abranch varchar(10);
declare variable cbranch varchar(10);
declare variable adept varchar(10);
declare variable cdept varchar(10);
declare variable aworkpost integer;
declare variable cworkpost integer;
declare variable alocal varchar(80);
declare variable clocal varchar(80);
declare variable apaymenttype smallint;
declare variable cpaymenttype smallint;
declare variable asalary numeric(14,2);
declare variable csalary numeric(14,2);
declare variable acaddsalary numeric(14,2);
declare variable ccaddsalary numeric(14,2);
declare variable afuncsalary numeric(14,2);
declare variable cfuncsalary numeric(14,2);
declare variable adimnum smallint;
declare variable cdimnum smallint;
declare variable adimden smallint;
declare variable cdimden smallint;
declare variable company integer;
declare variable before_ann integer;
declare variable worksystem integer;
declare variable cworksystem integer;
begin
  number = 0;
  name = '';
  fromval = '';
  toval = '';
  bannex = -1;
 SELECT first 1 a.ref
  FROM econtrannexes a
 where a.emplcontract = (SELECT ea.emplcontract from econtrannexes ea WHERE ea.ref = :ANNEX)
   and a.fromdate <= (SELECT eb.fromdate from econtrannexes eb WHERE eb.ref = :ANNEX)
   and a.todate is null -- w ten sposób pomijamy oddelegowania --mjelen
   and a.ref <> :ANNEX
 order by a.fromdate desc into :before_ann;

   before_ann = coalesce(:before_ann,-1);
   bannex = before_ann;

  if (:before_ann <> -1) then
    begin
        select A.branch, A.department, A.workpost, A.local,
          A.paymenttype, A.salary, coalesce(A.caddsalary,0), coalesce(A.funcsalary,0), coalesce(A.dimnum,0), coalesce(A.dimden,0)
        from econtrannexes A
        where A.ref = :ANNEX
        into :abranch, :adept, :aworkpost, :alocal,
          :apaymenttype, :asalary, :acaddsalary, :afuncsalary, :adimnum, :adimden;

      select C.branch, C.department, C.workpost, C.local,
          C.paymenttype, C.salary, coalesce(C.caddsalary,0), coalesce(C.funcsalary,0), coalesce(C.dimnum,0), coalesce(C.dimden,0), D.company
        from econtrannexes C
          join emplcontracts D on (D.ref = C.emplcontract)
        where C.ref = :before_ann
        into :cbranch, :cdept, :cworkpost, :clocal,
          :cpaymenttype, :csalary, :ccaddsalary, :cfuncsalary, :cdimnum, :cdimden, :company;
    end
  else
    begin
      select A.branch, A.department, A.workpost, A.local,
          A.paymenttype, A.salary, coalesce(A.caddsalary,0), coalesce(A.funcsalary,0), coalesce(A.dimnum,0), coalesce(A.dimden,0),
          C.branch, C.department, C.workpost, C.local,
          C.paymenttype, C.salary, coalesce(C.caddsalary,0), coalesce(C.funcsalary,0), coalesce(C.dimnum,0), coalesce(C.dimden,0), C.company,
          coalesce(a.worksystem,1), coalesce(c.worksystem,1)  -- 1-podstawowy system czasu pracy
        from econtrannexes A
          join emplcontracts C on (C.ref = A.emplcontract)
        where A.ref = :annex
        into :abranch, :adept, :aworkpost, :alocal,
          :apaymenttype, :asalary, :acaddsalary, :afuncsalary, :adimnum, :adimden,
          :cbranch, :cdept, :cworkpost, :clocal,
          :cpaymenttype, :csalary, :ccaddsalary, :cfuncsalary, :cdimnum, :cdimden, :company,
          :worksystem, :cworksystem;
    end
  if (abranch <> cbranch) then
  begin
    select nazwa from oddzialy where oddzial = :cbranch and company = :company into :fromval;
    select nazwa from oddzialy where oddzial = :abranch and company = :company into :toval;
    number = number + 1;
    name = 'oddział';
    bannex = before_ann;
    suspend;
  end
  if (adept <> cdept) then
  begin
    select name from departments where symbol = :cdept and company = :company into :fromval;
    select name from departments where symbol = :adept and company = :company into :toval;
    number = number + 1;
    name = 'wydział';
    bannex = before_ann;
    suspend;
  end
  if (aworkpost <> cworkpost) then
  begin
    select coalesce(fullname,workpost)
      from edictworkposts
      where ref = :cworkpost
      into :fromval;
    select coalesce(fullname,workpost)
      from edictworkposts
      where ref = :aworkpost
      into :toval;
    number = number + 1;
    name = 'stanowisko';
    bannex = before_ann;
    suspend;
  end
  if (alocal <> clocal) then
  begin
    number = number + 1;
    name = 'lokalizacja';
    fromval = clocal;
    toval = alocal;
    bannex = before_ann;
    suspend;
  end
  if (apaymenttype <> cpaymenttype) then
  begin
    number = number + 1;
    name = 'typ wynagrodzenia';
    if (cpaymenttype = 0) then
      fromval = 'miesięczne';
    else if (cpaymenttype = 1) then
      fromval = 'godzinowe';
    else if (cpaymenttype = 2) then
      fromval = 'akordowe';
    else if (cpaymenttype = 3) then
      fromval = 'całościowe';
    if (apaymenttype = 0) then
      toval = 'miesięczne';
    else if (apaymenttype = 1) then
      toval = 'godzinowe';
    else if (apaymenttype = 2) then
      toval = 'akordowe';
    else if (apaymenttype = 3) then
      toval = 'całościowe';
    bannex = before_ann;
    suspend;
  end
  if (asalary <> csalary) then
  begin
    number = number + 1;
    name = 'płaca zasadnicza';
    fromval = cast(csalary as varchar(80)) || 'zł';
    toval = cast(asalary as varchar(80)) || 'zł';
    bannex = before_ann;
    suspend;
  end
  if (acaddsalary <> ccaddsalary) then
  begin
    number = number + 1;
    name = 'dodatek stały';
    fromval = cast(ccaddsalary as varchar(80)) || 'zł';
    toval = cast(acaddsalary as varchar(80)) || 'zł';
    bannex = before_ann;
    suspend;
  end
  if (afuncsalary <> cfuncsalary) then
  begin
    number = number + 1;
    name = 'dodatek funkcyjny';
    fromval = cast(cfuncsalary as varchar(80)) || 'zł';
    toval = cast(afuncsalary as varchar(80)) || 'zł';
    bannex = before_ann;
    suspend;
  end
  if (adimden <> cdimden or adimnum <> cdimnum) then
  begin
    number = number + 1;
    name = 'wymiar etatu';
    fromval = cast(cdimnum as varchar(80)) || '/' || cast(cdimden as varchar(80));
    toval = cast(adimnum as varchar(80)) || '/' || cast(adimden as varchar(80));
    bannex = before_ann;
    suspend;
  end
  if (worksystem <> cworksystem) then
  begin
    number = number + 1;
    name = 'system czasu pracy';
    select shortname from edictworksystems where ref = :cworksystem into :fromval;
    select shortname from edictworksystems where ref = :worksystem into :toval;
    bannex = before_ann;
    suspend;
  end
end^
SET TERM ; ^
