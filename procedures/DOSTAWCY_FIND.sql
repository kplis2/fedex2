--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOSTAWCY_FIND(
      INCOMPANY integer,
      INSEARCH varchar(255) CHARACTER SET UTF8                           )
  returns (
      OUTREF integer)
   as
begin
  outref = null;
  -- szukanie dostawcy po jego konkretnym ID
  select first 1 ref
    from dostawcy
    where company = :incompany
      and id = :insearch
      and aktywny = 1
    into :outref;

  -- szukanie dostawcy po jego ID wsrod aktywnych
  if(:outref is null) then begin
  select first 1 ref
    from dostawcy
    where company = :incompany
      and id starting with :insearch
      and aktywny = 1
    into :outref;
  end

  -- szukanie dostawcy po jego ID
  if(:outref is null) then begin
    select first 1 ref
      from dostawcy
      where company = :incompany
        and id starting with :insearch
      into :outref;
  end

  -- szukanie dostawcy po nipie wsord aktywnych
  if(:outref is null) then begin
    select first 1 ref
      from dostawcy
      where company = :incompany
        and prostynip starting with :insearch
        and aktywny = 1
      into :outref;
  end

  -- szukanie dostawcy po nipie
  if(:outref is null) then begin
    select first 1 ref
      from dostawcy
      where company = :incompany
        and prostynip starting with :insearch
      into :outref;
  end

  -- jeżeli nie znalazl zadnego zwroc -1 (zeby obsluzyc w kodzie)
  if(:outref is null) then
    outref = -1;
  suspend;
end^
SET TERM ; ^
