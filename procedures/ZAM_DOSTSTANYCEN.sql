--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ZAM_DOSTSTANYCEN(
      MAGAZYN char(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      DOSTAWA integer)
   as
declare variable cnt integer;
begin
 cnt = null;
 select count(*) from STANYCEN where MAGAZYN=:magazyn and KTM = :ktm AND WERSJA=:WERSJA AND DOSTAWA=:DOSTAWA into :cnt;
 if(cnt is null) then cnt = 0;
 if(cnt = 0) then
   insert into STANYCEN(MAGAZYN, KTM, WERSJA, CENA, DOSTAWA,ILOSC)
     values(:MAGAZYN, :KTM, :WERSJA, 0, :DOSTAWA, 0);
end^
SET TERM ; ^
