--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NEOS_ON_CONNECT as
declare variable connid varchar(40);
begin
    connid = (select pvalue from globalparams g where g.connectionid = current_connection
        and g.psymbol='CONNECTIONID');

    delete from globalparams g where g.connectionid = current_connection;
    if(connid is not null) then
    begin
      /* nie ważne z jakiego neosid, bo to i tak jezeli byl to byl
         poprzedni neos wiec i tak nalezy wpisy wyczyscic */
      delete from globalparams g where g.connectionid = :connid;
    end
end^
SET TERM ; ^
