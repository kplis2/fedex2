--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SLODEF_CHANGE(
      SLODEFFROM integer,
      REFFROM integer,
      SLODEFTO integer)
  returns (
      REFOUT integer,
      STATUS integer,
      MESS varchar(200) CHARACTER SET UTF8                           )
   as
declare variable cpodmiot integer;
declare variable company integer;
declare variable aktywny smallint;
declare variable poczta varchar(255);
declare variable kraj varchar(40);
declare variable komorka varchar(255);
declare variable nip varchar(20);
declare variable regon varchar(20);
declare variable firma smallint;
declare variable niewyplac smallint;
declare variable typ integer;
declare variable kodzewn varchar(40);
declare variable miasto varchar(255);
declare variable kodp varchar(10);
declare variable ulica varchar(255);
declare variable nrdomu nrdomu_id;
declare variable nrlokalu nrdomu_id;
declare variable telefon varchar(255);
declare variable fax varchar(255);
declare variable email varchar(255);
declare variable www varchar(255);
declare variable imie varchar(40);
declare variable nazwisko varchar(255);
declare variable branza integer;
declare variable bazain varchar(50);
declare variable bazaout varchar(50);
declare variable typpodm integer;
begin
  status = 0;
  mess = 'Operacja zakonczona niepowodzeniem.';
  refout = 0;
  cpodmiot = null;
  company = null;
  typ = null;
  select ref,typ,company from cpodmioty where slodef=:slodeffrom and slopoz=:reffrom into :cpodmiot,:typpodm,:company;
  select TYP from SLODEF where REF=:slodeffrom into :bazain;
  select TYP from SLODEF where REF=:slodefto into :bazaout;
  if(:company is null or :company=0) then execute procedure get_global_param('CURRENTCOMPANY') returning_values company;

  if(bazain='KLIENCI') then begin
    if(bazaout='DOSTAWCY') then begin
      execute procedure GEN_REF('DOSTAWCY') returning_values :refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into DOSTAWCY(REF, NAZWA, ID, CRM, KONTOFK,MIASTO, KODP, ULICA, NRDOMU, NRLOKALU,
          TELEFON, FAX, EMAIL, WWW, AKTYWNY, POCZTA, KRAJ, NIP, REGON, FIRMA,  TYP, KODZEWN, WALUTA, COMPANY, UWAGI)
        select :refout, NAZWA, FSKROT, CRM, KONTOFK,MIASTO, KODP, ULICA, NRDOMU, NRLOKALU,
            TELEFON, FAX, EMAIL, WWW, AKTYWNY, POCZTA, KRAJ, NIP, REGON, FIRMA,  TYP, KODZEWN, WALUTA, COMPANY, UWAGI
          from KLIENCI where REF= :reffrom;
      delete from KLIENCI where REF=:reffrom;
      status = 1;
    end
    else if(bazaout='PKLIENCI') then begin
      execute procedure GEN_REF('PKLIENCI') returning_values :refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into PKLIENCI(REF, NAZWA, SKROT, CRM,MIASTO, KODP, ULICA, NRDOMU, NRLOKALU,
          TELEFON, FAX, EMAIL, WWW)
        select :refout, NAZWA, FSKROT, CRM,MIASTO, KODP, ULICA, NRDOMU, NRLOKALU,
            TELEFON, FAX, EMAIL, WWW
          from KLIENCI where REF= :reffrom;
      delete from KLIENCI where REF=:reffrom;
      status = 1;
    end
    else if(bazaout='SPRZEDAWCY') then begin
      execute procedure GEN_REF('SPRZEDAWCY') returning_values :refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into SPRZEDAWCY(REF, NAZWA, SKROT, CRM, KONTOFK, MIASTO, KODPOCZT, ADRES, NRDOMU, NRLOKALU,
          TELEFON, EMAIL, AKT, POCZTA, NIP, FIRMA, COMPANY)
        select :refout, NAZWA, FSKROT, CRM, KONTOFK,MIASTO, KODP, ULICA, NRDOMU, NRLOKALU,
            TELEFON, EMAIL, AKTYWNY, POCZTA, NIP, FIRMA, COMPANY
          from KLIENCI where REF= :reffrom;
      delete from KLIENCI where REF=:reffrom;
      status = 1;
    end
    else if(bazaout='ESYSTEM') then begin
      execute procedure GEN_REF('SLOPOZ') returning_values :refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into SLOPOZ(REF, SLOWNIK, KOD, NAZWA, CRM, KONTOKS)
      select :refout, :slodefto, substring(FSKROT from 1 for 40), NAZWA, CRM, KONTOFK  --XXX ZG133796 MKD
      from KLIENCI where REF= :reffrom;
      delete from KLIENCI where REF=:reffrom;
      status = 1;
    end
  end
  else if(bazain='DOSTAWCY') then begin
    if(bazaout='KLIENCI') then begin
      execute procedure GEN_REF('KLIENCI') returning_values :refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into KLIENCI(REF, NAZWA, FSKROT, CRM, KONTOFK,MIASTO, KODP, ULICA, NRDOMU, NRLOKALU,
          TELEFON, FAX, EMAIL, WWW, AKTYWNY, POCZTA, KRAJ, NIP, REGON, FIRMA,  TYP, KODZEWN, WALUTA, COMPANY, UWAGI)
        select :refout, NAZWA, ID, CRM, KONTOFK,MIASTO, KODP, ULICA, NRDOMU, NRLOKALU,
            TELEFON, FAX, EMAIL, WWW, AKTYWNY, POCZTA, KRAJ, NIP, REGON, FIRMA,  TYP, KODZEWN, WALUTA, COMPANY, UWAGI
          from DOSTAWCY where REF= :reffrom;
      delete from DOSTAWCY where REF=:reffrom;
      status = 1;
    end
    else if(bazaout='PKLIENCI') then begin
      execute procedure GEN_REF('PKLIENCI') returning_values :refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into PKLIENCI(REF, NAZWA, SKROT, CRM,MIASTO, KODP, ULICA, NRDOMU, NRLOKALU,
          TELEFON, FAX, EMAIL, WWW)
        select :refout, NAZWA, ID, CRM,MIASTO, KODP, ULICA, NRDOMU, NRLOKALU,
            TELEFON, FAX, EMAIL, WWW
          from DOSTAWCY where REF= :reffrom;
      delete from DOSTAWCY where REF=:reffrom;
      status = 1;
    end
    else if(bazaout='ESYSTEM') then begin
      execute procedure GEN_REF('SLOPOZ') returning_values refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into SLOPOZ(REF, SLOWNIK, KOD, NAZWA, CRM, KONTOKS)
      select :refout, :slodefto, ID, NAZWA, CRM, KONTOFK
      from DOSTAWCY where REF= :reffrom;
      delete from DOSTAWCY where REF=:reffrom;
      status = 1;
    end
  end
  else if(bazain='PKLIENCI') then begin
    if(bazaout='KLIENCI') then begin
      execute procedure GEN_REF('KLIENCI') returning_values :refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into KLIENCI(REF,NAZWA, FSKROT, MIASTO, KODP, ULICA, NRDOMU, NRLOKALU,
          TELEFON, FAX, EMAIL, WWW, CRM, TYP, COMPANY)
        select :refout, NAZWA, SKROT, MIASTO, KODP, ULICA, NRDOMU, NRLOKALU,
            TELEFON, FAX, EMAIL, WWW, CRM, :typpodm, :company
          from PKLIENCI where REF= :reffrom;
      delete from PKLIENCI where REF=:reffrom;
      status = 1;
    end
    else if(bazaout='DOSTAWCY') then begin
      execute procedure GEN_REF('DOSTAWCY') returning_values :refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into DOSTAWCY(REF, NAZWA, ID, CRM,MIASTO, KODP, ULICA, NRDOMU, NRLOKALU,
          TELEFON, FAX, EMAIL, WWW, TYP, COMPANY)
        select :refout, NAZWA, SKROT, CRM, MIASTO, KODP, ULICA, NRDOMU, NRLOKALU,
            TELEFON, FAX, EMAIL, WWW, :typpodm, :company
          from PKLIENCI where REF= :reffrom;
      delete from PKLIENCI where REF=:reffrom;
      status = 1;
    end
    else if(bazaout='SPRZEDAWCY') then begin
      execute procedure GEN_REF('SPRZEDAWCY') returning_values :refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into SPRZEDAWCY(REF, NAZWA, SKROT, CRM, MIASTO, KODPOCZT, ADRES, NRDOMU, NRLOKALU,
          TELEFON, EMAIL)
        select :refout, NAZWA, SKROT, CRM, MIASTO, KODP, ULICA, NRDOMU, NRLOKALU,
            TELEFON, EMAIL
          from PKLIENCI where REF= :reffrom;
      delete from PKLIENCI where REF=:reffrom;
      status = 1;
    end
    else if(bazaout='ESYSTEM') then begin
      execute procedure GEN_REF('SLOPOZ') returning_values :refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into SLOPOZ(REF, SLOWNIK, KOD, NAZWA, CRM)
      select :refout, :slodefto, SKROT, NAZWA, CRM
      from PKLIENCI where REF= :reffrom;
      delete from PKLIENCI where REF=:reffrom;
      status = 1;
    end
  end
  else if(bazain='SPRZEDAWCY') then begin
    if(bazaout='KLIENCI') then begin
      execute procedure GEN_REF('KLIENCI') returning_values :refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into KLIENCI(REF, NAZWA, FSKROT, CRM, KONTOFK, MIASTO, KODP, ULICA, NRDOMU, NRLOKALU,
          TELEFON, EMAIL, AKTYWNY, POCZTA, NIP, FIRMA, TYP, COMPANY)
        select :refout, NAZWA, SKROT, CRM, KONTOFK, MIASTO, KODPOCZT, ADRES, NRDOMU, NRLOKALU,
            TELEFON, EMAIL, AKT, POCZTA, NIP, FIRMA, :typpodm, :company
          from SPRZEDAWCY where REF= :reffrom;
      delete from SPRZEDAWCY where REF=:reffrom;
      status = 1;
    end
    else if(bazaout='PKLIENCI') then begin
      execute procedure GEN_REF('PKLIENCI') returning_values :refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into PKLIENCI(REF, NAZWA, SKROT, CRM,MIASTO, KODP, ULICA, NRDOMU, NRLOKALU, TELEFON, EMAIL)
        select :refout, NAZWA, SKROT, CRM, MIASTO, KODPOCZT, ADRES, NRDOMU, NRLOKALU, TELEFON, EMAIL
        from SPRZEDAWCY where REF= :reffrom;
      delete from SPRZEDAWCY where REF=:reffrom;
      status = 1;
    end
    else if(bazaout='ESYSTEM') then begin
      execute procedure GEN_REF('SLOPOZ') returning_values :refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into SLOPOZ(REF, SLOWNIK, KOD, NAZWA, CRM, KONTOKS)
      select :refout, :slodefto, SKROT, NAZWA, CRM, KONTOFK
      from SPRZEDAWCY where REF= :reffrom;
      delete from SPRZEDAWCY where REF=:reffrom;
      status = 1;
    end
  end
  else if(bazain='ESYSTEM') then begin
    if(bazaout='KLIENCI') then begin    --aktywny=1
      execute procedure GEN_REF('KLIENCI') returning_values :refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into KLIENCI(REF, NAZWA, FSKROT, CRM, KONTOFK, TYP, AKTYWNY, COMPANY)
      select :refout, NAZWA, KOD, CRM, KONTOKS, :typpodm, 1, :company
      from SLOPOZ where REF= :reffrom;
      delete from SLOPOZ where REF=:reffrom;
      status = 1;
    end
    else if(bazaout='DOSTAWCY') then begin  --aktywny=1
      execute procedure GEN_REF('DOSTAWCY') returning_values :refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into DOSTAWCY(REF, NAZWA, ID, CRM, KONTOFK, TYP, AKTYWNY, COMPANY)
      select :refout, NAZWA, KOD, CRM, KONTOKS, :typpodm, 1, :company
      from SLOPOZ where REF= :reffrom;
      delete from SLOPOZ where REF=:reffrom;
      status = 1;
    end
    else if(bazaout='PKLIENCI') then begin
      execute procedure GEN_REF('PKLIENCI') returning_values :refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into PKLIENCI(REF, NAZWA, SKROT, CRM)
      select :refout, NAZWA, KOD, CRM
      from SLOPOZ where REF= :reffrom;
      delete from SLOPOZ where REF=:reffrom;
      status = 1;
    end
    else if(bazaout='SPRZEDAWCY') then begin    --akt=1
      execute procedure GEN_REF('SPRZEDAWCY') returning_values :refout;
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto, SLOPOZ=:refout where REF=:cpodmiot;
      insert into SPRZEDAWCY(REF, NAZWA, SKROT, CRM, KONTOFK, AKT, COMPANY)
      select :refout, NAZWA, KOD, CRM, KONTOKS, 1, :company
      from SLOPOZ where REF= :reffrom;
      delete from SLOPOZ where REF=:reffrom;
      status = 1;
    end
    else if (bazaout='ESYSTEM' and slodefto<>slodeffrom) then begin
      if(:cpodmiot is not null) then update CPODMIOTY set SLODEF=:slodefto where REF=:cpodmiot;
      update SLOPOZ set SLOWNIK=:slodefto where REF=:reffrom;
      status = 1;
    end
  end
  else begin
    status = 0;
    mess = 'Dla zadanej pary kartotek nie znaleziono definicji przeniesienia.';
  end
  if (status = 1) then begin
    update srvrequests set repdictdef = :slodefto, repdictpos = :refout
      where repdictdef = :slodeffrom and repdictpos = :reffrom;
    update srvcontracts set dictdef = :slodefto, dictpos = :refout
      where dictdef = :slodeffrom and dictpos = :reffrom;
    update oferty set slodef = :slodefto, slopos = :refout
      where slodef = :slodeffrom and slopos = :reffrom;
  end
end^
SET TERM ; ^
