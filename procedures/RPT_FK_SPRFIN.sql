--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_SPRFIN(
      FRHDR varchar(20) CHARACTER SET UTF8                           ,
      REF integer)
  returns (
      NUMBER integer,
      SYMBOL varchar(15) CHARACTER SET UTF8                           ,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      VALUE1 numeric(14,5),
      COLOURS smallint)
   as
declare variable gen integer;
declare variable prec integer;
declare variable amount numeric(15,2);
declare variable temp1 numeric(18,6);
declare variable strtemp varchar(18);
declare variable gdziep integer;
declare variable ilmsc integer;
declare variable tmpvalue1 varchar(20);
begin

  for
    select P.number, P.symbol, P.name, VP.amount, FP.generalization, FP.frprecision, P.colours
      from frpsns P
        left join FRHDRS FP on (FP.symbol=P.frhdr)
          join frvpsns VP on (VP.frpsn=P.ref and VP.col=1 and VP.frvhdr=:ref)
      where P.frhdr=:FRHDR
      order by P.number
    into :number, :symbol, :name, :amount, :gen, :prec, :colours
  do begin

    temp1 = amount;
    temp1 = temp1 / power(10,gen);
    tmpvalue1 = cast(temp1 as varchar(20));
    gdziep = position('.' in  tmpvalue1);
    ilmsc = coalesce(char_length(tmpvalue1),0); -- [DG] XXX ZG119346
    strtemp = substring(tmpvalue1 from  gdziep+1 for  ilmsc);
    ilmsc = coalesce(char_length(strtemp),0); -- [DG] XXX ZG119346
    tmpvalue1 = substring(tmpvalue1 from  1 for  gdziep+prec);
    if(tmpvalue1 = '') then
      tmpvalue1 = NULL;
    value1 = cast(coalesce(tmpvalue1,0) as numeric(14,5));
    suspend;

  end
end^
SET TERM ; ^
