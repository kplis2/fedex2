--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZFAK_TO_POZSAD(
      NAGFAK integer,
      POZSAD integer)
  returns (
      STATUS integer)
   as
declare variable stawkacla numeric(14,2);
declare variable pozfakref integer;
begin
  select stawkacla from pozsad where ref=:pozsad into :stawkacla;

  for
    select pozfak.ref
      from pozfak
        left join towary on (pozfak.ktm=towary.ktm)
      where pozfak.dokument=:nagfak
        and pozfak.refpozsad is null
        and coalesce(pozfak.fake,0) = 0
        and towary.stawkacla=:stawkacla
    into :pozfakref
  do begin
    update pozfak set refpozsad=:pozsad where ref=:pozfakref;
  end
  status=1;
end^
SET TERM ; ^
