--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_DBM_ADJUST(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           )
  returns (
      RMAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      ITCLASS smallint,
      ROICLASS smallint,
      STANMAX numeric(14,4),
      DNIBEZZMIANY integer,
      DNIZIELONE integer,
      DNIZOLTE integer,
      DNICZERWONE integer,
      DNICZARNE integer,
      WAGAZIELENI numeric(14,4),
      WAGACZERWIENI numeric(14,4),
      WAGAZOLCI numeric(14,4),
      NOWYSTANMAX numeric(14,4),
      KOLOR smallint,
      IKONA varchar(40) CHARACTER SET UTF8                           )
   as
declare variable data date;
declare variable k smallint;
declare variable d integer;
declare variable s numeric(14,4);
declare variable waga numeric(14,4);
declare variable ilosc numeric(14,4);
begin
  rmagazyn = :magazyn;
  for select s.ktm, s.nazwat, s.wersjaref, s.itclass, s.roiclass, coalesce(s.stanmax,0), s.ilosc
    from STANYIL S
    left join TOWARY T on T.KTM=S.KTM
    where S.MAGAZYN=:magazyn
    and T.supplymethod=3
    into :ktm, :nazwa, :wersjaref, :itclass, :roiclass, :stanmax, :ilosc
  do begin
    dnibezzmiany = 0;
    dnizielone = 0;
    dnizolte = 0;
    dniczerwone = 0;
    dniczarne = 0;
    wagazieleni = 0;
    wagaczerwieni = 0;
    wagazolci = 0;
    nowystanmax = NULL;
    kolor = 0;
    ikona = '';
    for select data,kolor,stanmax
      from GET_STANYHIST(:magazyn,:ktm, current_date - 30, current_date, 0)
      order by data desc
      into :data,:k,:s
    do begin
      -- jesli bufor sie zmienil w danym dniu to nie analizujemy dalej
      if(:s<>:stanmax or :stanmax=0) then break;
      -- ustalamy wage dnia
      d = cast((current_date - :data) as integer);
--      waga = 100 - 3*:d;
      -- obliczamy poziom zieleni i czerwieni
      if(:k=0) then begin
        dniczarne = :dniczarne + 1;
        wagaczerwieni = :wagaczerwieni + 3.0;
      end
      if(:k=1) then begin
        dniczerwone = :dniczerwone + 1;
        wagaczerwieni = :wagaczerwieni + 1.5;
      end
      if(:k=2) then begin
        dnizolte = :dnizolte + 1;
        wagazolci = :wagazolci + 1.0;
      end
      if(:k=3) then begin
        dnizielone = :dnizielone + 1;
        wagazieleni = :wagazieleni + 1.0;
      end
      dnibezzmiany = :dnibezzmiany + 1;
    end
    if(:dnibezzmiany>0 and :stanmax>0) then begin
      -- jesli uplynal conajmniej tydzien od zmiany wagi
      if(:wagaczerwieni>:wagazieleni+:wagazolci) then begin
        -- przewaga niedoborow - zwiekszenie bufora
        nowystanmax = :stanmax + 0.33 * :stanmax * (:dniczerwone + dniczarne) / cast(:dnibezzmiany as numeric(14,4));
        ikona = 'MI_UP';
      end
      if(:wagazieleni>:wagaczerwieni+:wagazolci) then begin
        -- przewaga nadwyzek - zmniejszenie bufora
        nowystanmax = :stanmax - 0.33 * :stanmax * :dnizielone / cast(:dnibezzmiany as numeric(14,4));
        ikona = 'MI_DOWN';
      end
      nowystanmax = cast(:nowystanmax as integer);
    end else begin
      -- zbyt krotki czas na wyciagnie wnioskow
      if(:stanmax = 0 and :ilosc>0) then begin
        nowystanmax = :ilosc;
        ikona = 'MI_INFORMATION';
      end else begin
        ikona = '';
      end
    end
    if(:stanmax=0) then stanmax = null;
    if(:dnibezzmiany=0) then dnibezzmiany = null;
    if(:dniczarne=0) then dniczarne = null;
    if(:dniczerwone=0) then dniczerwone = null;
    if(:dnizolte=0) then dnizolte = null;
    if(:dnizielone=0) then dnizielone = null;
    suspend;
  end
end^
SET TERM ; ^
