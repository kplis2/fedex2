--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHECK_MARZAMIN(
      STANSPRZED varchar(10) CHARACTER SET UTF8                           ,
      REFFAK integer)
  returns (
      BLOKADA smallint,
      AKTUMARZA numeric(14,2))
   as
DECLARE VARIABLE MARZAMIN NUMERIC(14,2);
DECLARE VARIABLE SUMWARTNET NUMERIC(14,2);
DECLARE VARIABLE KOSZTANAL NUMERIC(14,2);
begin
  blokada = 0;
  aktumarza = 0;
  if(:stansprzed is not null) then begin
    select stansprzed.sprzmarzamin
      from stansprzed
      where stansprzed.stanowisko = :stansprzed
    into :marzamin;
    if (:marzamin is null) then
      marzamin = 0;
  end else begin
    blokada = 2;
  end
  if(:marzamin > 0) then begin
    if(:reffak > 0) then begin
      select nagfak.sumwartnet - nagfak.psumwartnet, nagfak.kosztanal
        from nagfak where nagfak.ref=:reffak
      into :sumwartnet, :kosztanal;
      if(:sumwartnet is null) then
        sumwartnet = 0;
      if(:kosztanal is null) then
        kosztanal = 0;
      if(:kosztanal > 0) then
        aktumarza = ((:sumwartnet - :kosztanal)/:kosztanal)*100;
      else
        aktumarza = 100;
      if(:aktumarza < :marzamin) then
        blokada = 1;
    end else begin
      blokada = 3;
    end
  end else begin
    blokada = 0;
    aktumarza = 0;
  end
end^
SET TERM ; ^
