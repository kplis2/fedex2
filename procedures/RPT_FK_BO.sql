--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_BO(
      I_YEARID integer,
      CURRENCY varchar(3) CHARACTER SET UTF8                           ,
      COMPANY integer,
      ACCTYP smallint)
  returns (
      REF integer,
      SYMBOL varchar(3) CHARACTER SET UTF8                           ,
      ACCOUNT ACCOUNT_ID,
      DESCRIPT varchar(80) CHARACTER SET UTF8                           ,
      DEBIT numeric(15,2),
      CREDIT numeric(15,2),
      ISACCOUNT smallint)
   as
DECLARE VARIABLE OLD_SYMBOL VARCHAR(3);
DECLARE VARIABLE OLD_ACCOUNT ACCOUNT_ID;
DECLARE VARIABLE S_ACCOUNT ACCOUNT_ID;
DECLARE VARIABLE N_DEBIT NUMERIC(15,2);
DECLARE VARIABLE N_CREDIT NUMERIC(15,2);
DECLARE VARIABLE I_REF INTEGER;
DECLARE VARIABLE S_SEP VARCHAR(1);
DECLARE VARIABLE I_DICTDEF INTEGER;
DECLARE VARIABLE I_LEN SMALLINT;
DECLARE VARIABLE S_TMP ACCOUNT_ID;
DECLARE VARIABLE OLD_PREFIX ACCOUNT_ID;
DECLARE VARIABLE I_I SMALLINT;
DECLARE VARIABLE I_J SMALLINT;
DECLARE VARIABLE I_K SMALLINT;
DECLARE VARIABLE DICTREF INTEGER;
DECLARE VARIABLE PERIOD VARCHAR(6);
begin
  --TOFIX!!! - styl
  ref = 0;
  old_symbol = '';
  old_account = '';
  old_prefix = '';
  period = cast(:i_yearid as varchar(4)) || '00';
  for
    select cast(substring(A.account from 1 for 3) as varchar(3)), A.account, T.debit, T.credit
      from accounting A
        join turnovers T on (T.accounting=A.ref and T.period=:period and (T.debit<>0 or T.credit<>0))
      where A.yearid = :i_yearid and A.currency=:currency and A.company = :company
            and  ((:acctyp = 0 and T.bktype < 2) or (:acctyp = 1 and (T.bktype = 2 or T.bktype = 3)) or :acctyp = 2)
      order by A.account
      into :symbol, :s_account, :n_debit, :n_credit
  do begin
    isaccount = 0;
    debit = null;
    credit = null;
    if (:old_symbol <>:symbol) then -- szukamy nazwy syntetyki
    begin
      select descript, ref
        from bkaccounts where symbol=:symbol and yearid = :i_yearid
          and bkaccounts.company = :company
        into :descript, :i_ref;
      account = :symbol;
      if (:symbol <> :s_account) then begin
        ref = :ref + 1;
        suspend;
      end
      i_dictdef = null;
      i_i = 0;
      --zliczam ilosc poziomów nalitycznych
      for select separator, dictdef, len
        from accstructure
        where bkaccount=:i_ref
        order by nlevel
        into :s_sep, :i_dictdef, :i_len
      do begin
        i_i = :i_i + 1;
      end
    end
    if (:i_i>0) then
    begin
      if (:i_i>1 and :old_prefix<>substring(:s_account from  1 for coalesce(char_length(:s_account),0)-:i_len)) then -- [DG] XXX ZG119346
      -- drukowanie opisow posrednich analityk
      begin
        i_j = 0;
        i_k = 4;
        for select separator, dictdef, len
          from accstructure
          where bkaccount=:i_ref
          order by nlevel
          into :s_sep, :i_dictdef, :i_len
        do begin
          i_j = i_j + 1;
          if (:i_j < :i_i) then
          begin
            if (:s_sep<>',') then
              i_k = :i_k +1;
            s_tmp = substring(:s_account from  :i_k for  :i_k+:i_len-1);
            account = '      ' || :s_tmp;
            execute procedure name_from_bksymbol(company, i_dictdef, s_tmp) returning_values descript, dictref;
            ref = :ref + 1;
            debit = null;
            credit = null;
            suspend;
            i_k = i_k + i_len;
          end
        end
        old_prefix = substring(:s_account from  1 for coalesce(char_length(:s_account),0)-:i_len); -- [DG] XXX ZG119346
      end
      s_tmp = substring(:s_account from  coalesce(char_length(:s_account),0)-:i_len+1 for coalesce(char_length(:s_account),0)); -- [DG] XXX ZG119346
      execute procedure name_from_bksymbol(company, i_dictdef, s_tmp) returning_values descript, dictref;
    end
    debit = :n_debit;
    credit = :n_credit;
    account = :s_account;
    old_symbol = symbol;
    old_account = account;
    ref = :ref + 1;
    isaccount = 1;
    suspend;
  end
end^
SET TERM ; ^
