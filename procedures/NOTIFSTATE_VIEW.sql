--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NOTIFSTATE_VIEW(
      SHOWTREE smallint = 1,
      SHOWALL smallint = 0)
  returns (
      REF varchar(40) CHARACTER SET UTF8                           ,
      PARENT varchar(40) CHARACTER SET UTF8                           ,
      NOTIFSTEP integer,
      ID varchar(20) CHARACTER SET UTF8                           ,
      NOTIFSHEET integer,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           ,
      NUMVAL numeric(14,2),
      WATCHBELOW smallint,
      COLORINDEX integer,
      PRIORITY integer,
      ICON varchar(255) CHARACTER SET UTF8                           ,
      STATEICON varchar(255) CHARACTER SET UTF8                           ,
      ACTIONID varchar(255) CHARACTER SET UTF8                           ,
      ACTIONPARAMS varchar(255) CHARACTER SET UTF8                           ,
      ACTIONNAME varchar(255) CHARACTER SET UTF8                           ,
      DYNAMICPICKLIST varchar(255) CHARACTER SET UTF8                           ,
      EXPAND smallint,
      PSOUND smallint)
   as
declare variable AKTUOPERATOR integer;
declare variable SAKTUOPERATOR varchar(255);
declare variable SAKTUOPERGRUPY varchar(1024);
declare variable SMODULES varchar(1024);
declare variable ISADMIN smallint;
declare variable SHOWHIDDEN smallint;
declare variable USERCOLORINDEX smallint;
declare variable SHEETNAME varchar(255);
declare variable SHEETICON varchar(20);
declare variable SHEETPRIORITY integer;
declare variable CNTGROUP integer;
declare variable CNTIMPGROUP integer;
declare variable CNTVIMPGROUP integer;
declare variable PARENTID varchar(20);
declare variable NEWREF integer;
declare variable UNIQUEID varchar(40);
declare variable PARENTDESCRIPT varchar(255);
declare variable CURRENTCOMPANY integer;
declare variable SCURRENTCOMPANY varchar(255);
begin
  -- pobierz aktualnego operatora
  execute procedure GET_GLOBAL_PARAM('AKTUOPERATOR') returning_values :saktuoperator;
  if(:saktuoperator<>'') then aktuoperator = cast(:saktuoperator as integer);
--  else exit;
  -- pobierz aktualne company
  execute procedure GET_GLOBAL_PARAM('CURRENTCOMPANY') returning_values :scurrentcompany;
  if(:scurrentcompany<>'') then currentcompany = cast(:scurrentcompany as integer);
  -- pobierz jego grupy, znacznik administratora i moduly
  select GRUPA,ADMINISTRATOR from OPERATOR where ref=:aktuoperator into :saktuopergrupy, :isadmin;
  if(:isadmin>0) then isadmin = 1;
  execute procedure GET_GLOBAL_PARAM('GLOBALAPPLICATIONIDENTS') returning_values :smodules;
  if (smodules is null) then smodules = '';
  -- jesli podano showall to pokaz takze ukryte powiadomienia
  showhidden = :showall;
  if(:isadmin=0) then showall = 0;
  cntimpgroup = 0;
  cntvimpgroup = 0;
  newref = 0;
  expand = 0;
    -- zwroc liste zakladek
    for select ref,name,icon,number
      from notifsheetstates
      where (:showall=1) or (RIGHTS = '' and RIGHTSGROUP = '') or (RIGHTS like '%%;'||:saktuoperator||';%%') OR (strmulticmp(';'||:saktuopergrupy||';',RIGHTSGROUP)=1)
      order by number
      into :notifsheet, :sheetname, :sheeticon, :sheetpriority
    do begin
      cntgroup = 0;
      dynamicpicklist = '';
      -- zwroc elementy w kazdej zakladce
      for select notifstep,id,parentid,descript,val,numval,watchbelow,colorindex,priority,icon,
                 actionid,actionparams,actionname, PSOUND
        from notifstate
        where notifsheet=:notifsheet
        and ((:showall=1) or (RIGHTS = '' and RIGHTSGROUP = '') or (RIGHTS like '%%;'||:saktuoperator||';%%') OR (strmulticmp(';'||:saktuopergrupy||';',RIGHTSGROUP)=1))
        and ((coalesce(MODULE,'')<>'') and (strmulticmp(';'||:smodules||';',';'||MODULE||';')=1) or coalesce(MODULE,'')='')
        and (hidden=0 or :showhidden=1)
        and (company=:currentcompany)
        order by priority
        into :notifstep,:id,:parentid,:descript,:val,:numval,:watchbelow,:colorindex,:priority,:icon,
             :actionid,:actionparams,:actionname, :PSOUND
      do begin
        -- okresl ref rekordu
        if(coalesce(:id,'')<>'') then begin
          uniqueid = 'NOTIFSTATE'||cast(:notifstep as varchar(10))||'ID'||:id;
        end else begin
          newref = :newref + 1;
          uniqueid = 'NOTIFSTATEREF'||cast(:newref as varchar(10));
        end
        -- sprawdz ustawienia indywidualne (progi kolorow)
        if(:numval is not null and :id<>'') then begin
          usercolorindex = null;
          select max(colorindex)
            from notifoperparams
            where notifstep=:notifstep and ((id=:id and forgroup = 0) or (id = :parentid and forgroup = 1 ) )
            and ((OPERATOR=:aktuoperator) or (OPERATORS = '' and GROUPS = '') or (OPERATORS like '%%;'||:saktuoperator||';%%') OR (strmulticmp(';'||:saktuopergrupy||';',GROUPS)=1))
            and ((threshold<=:numval and :watchbelow=0) or
                 (threshold>=:numval and :watchbelow=1))
            into :usercolorindex;
          if(:usercolorindex is not null) then colorindex = :usercolorindex;
        end
        -- powiadomienie zwracaj w ramach wlasnej zakladki
        ref = :uniqueid;
        if(coalesce(:parentid,'')<>'') then begin
          parent = 'NOTIFSTATE'||cast(:notifstep as varchar(10))||'ID'||:parentid;
        end else begin
          parent = 'NOTIFSHEET'||cast(:notifsheet as varchar(10));
        end
        if(:colorindex>2) then dynamicpicklist = 'VAL=*=%B*';
        stateicon = '';
        if(:colorindex=3) then stateicon = 'MI_EXCLAMATION';
        if(:colorindex=4) then stateicon = 'MI_STOP';
        else dynamicpicklist = '';
        cntgroup = :cntgroup + 1;
        expand = 0;
        suspend;
        -- a takze w ramach zakladek specjalnych (wazne, bardzo wazne)
        if(:colorindex in (3,4)  and :showtree=1) then begin
          while(coalesce(:parentid,'')<>'') do begin
            parentdescript = '';
            select first 1 descript,parentid from notifstate
              where notifsheet=:notifsheet and hidden=0
              and notifstep=:notifstep and id=:parentid
              into :parentdescript,:parentid;
            if(:parentdescript<>'') then descript = :descript||' <- '||:parentdescript;
            else parentid = '';
          end
          if(:colorindex=3) then begin
            parent = 'IMPORTANT';
            ref = :uniqueid||'I';
            dynamicpicklist = 'VAL=*=%B*';
            cntimpgroup = :cntimpgroup + 1;
            suspend;
          end
          if(:colorindex=4) then begin
            parent = 'VERYIMPORTANT';
            ref = :uniqueid||'V';
            dynamicpicklist = 'VAL=*=%B*';
            cntvimpgroup = :cntvimpgroup + 1;
            suspend;
          end
        end
      end
      if(:cntgroup>0 and :showtree=1) then begin
        ref = 'NOTIFSHEET'||cast(:notifsheet as varchar(10));
        parent = NULL;
        notifstep = NULL;
        id = NULL;
        descript = :sheetname;
        val = '['||:cntgroup||']';
        numval = :cntgroup;
        colorindex = 2;
        priority = :sheetpriority;
        icon = :sheeticon;
        if(coalesce(:icon,'')='') then icon = 'MI_INFORMATION';
        stateicon = '';
        actionid = '';
        actionparams = '';
        actionname = '';
        dynamicpicklist = 'DESCRIPT=*=%B*';
        expand = 1;
        suspend;
      end
      update notifstate s set
        s.psound = 0
      where s.notifstep = :notifsheet and s.psound = 1;
    end
  if(:cntimpgroup>0 and :showtree=1) then begin
    ref = 'IMPORTANT';
    parent = NULL;
    notifstep = NULL;
    id = NULL;
    descript = ' Ważne';
    val = '['||:cntimpgroup||']';
    numval = :cntimpgroup;
    colorindex = 2;
    priority = -1;
    icon = '';
    stateicon = 'MI_EXCLAMATION';
    actionid = '';
    actionparams = '';
    actionname = '';
    dynamicpicklist = 'DESCRIPT=*=%B*';
    expand = 1;
    suspend;
  end
  if(:cntvimpgroup>0 and :showtree=1) then begin
    ref = 'VERYIMPORTANT';
    parent = NULL;
    notifstep = NULL;
    id = NULL;
    descript = ' Bardzo ważne';
    val = '['||:cntvimpgroup||']';
    numval = :cntvimpgroup;
    colorindex = 2;
    priority = -2;
    icon = '';
    stateicon = 'MI_STOP';
    actionid = '';
    actionparams = '';
    actionname = '';
    dynamicpicklist = 'DESCRIPT=*=%B*';
    expand = 1;
    suspend;
  end
end^
SET TERM ; ^
