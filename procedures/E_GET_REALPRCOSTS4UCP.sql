--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_REALPRCOSTS4UCP(
      PAYROLL integer,
      VAL2SETROUND numeric(14,2) = null,
      VALROUND numeric(14,2) = null)
  returns (
      RPRCOSTS numeric(14,2))
   as
begin
/*Perosnel - MWr: Procedura ma za zadanie pobrac realna wartosc procentowa kosztow
   uzyskania, jaka zostala zastosowana na danym rachunku PAYROLL.
   Aby uniknac bledow wynikajacych z zaokreglen w skali miesiaca mozna okreslic
   dokladnosc spodziewanego wyniku: np. jezeli oczekujemy wyniku RPRCOSTS = 50,
   a zapytanie zwraca RPRCOSTS = 49, to RPRCOSTS bedzie = 50 jezeli VAL2SETROUND = 50,
   a VALROUND >= 1 i mniejsze od granic rozsadku ;)  */

  select cast(sum(case when p.ecolumn = 6500 then p.pvalue else 0 end) * 100
         / (sum(case when p.ecolumn = 3000 then p.pvalue else 0 end)
         -  sum(case when p.ecolumn not in (3000,6500) then p.pvalue else 0 end)) as integer)
    from eprpos p
      join epayrolls r on ((p.payroll = r.ref or p.payroll = r.corlumpsumtax) and r.empltype = 2 and r.lumpsumtax = 0)
      join emplcontracts c on (c.ref = r.emplcontract and r.empltype = 2)
   -- join eprempl e on (e.epayroll = p.payroll and e.employee = p.employee)
    where p.payroll = :payroll
      and p.ecolumn in (3000,6100,6110,6120,6130,6500)
  --group by r.ref
    having (sum(case when p.ecolumn = 3000 then p.pvalue else 0 end)
         -  sum(case when p.ecolumn not in (3000,6500) then p.pvalue else 0 end)) <> 0
    into :rprcosts;

  if (abs(val2setround - rprcosts) <= valround) then
    rprcosts = val2setround;

  suspend;
end^
SET TERM ; ^
