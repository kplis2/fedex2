--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_GET_USERINFO_HANDLOWIEC(
      USR varchar(40) CHARACTER SET UTF8                           ,
      PSW varchar(40) CHARACTER SET UTF8                           ,
      OLDPSW varchar(40) CHARACTER SET UTF8                           ,
      ID varchar(255) CHARACTER SET UTF8                           )
  returns (
      USERID varchar(255) CHARACTER SET UTF8                           ,
      USERLOGIN varchar(255) CHARACTER SET UTF8                           ,
      IMIE varchar(255) CHARACTER SET UTF8                           ,
      NAZWISKO varchar(255) CHARACTER SET UTF8                           ,
      UWAGI varchar(1024) CHARACTER SET UTF8                           ,
      SUPERVISOR smallint,
      SUPERVISEGROUP varchar(255) CHARACTER SET UTF8                           ,
      EMAIL varchar(255) CHARACTER SET UTF8                           ,
      GRUPA varchar(1024) CHARACTER SET UTF8                           ,
      SENTE smallint,
      MODULES varchar(1024) CHARACTER SET UTF8                           ,
      APPLICATIONS varchar(255) CHARACTER SET UTF8                           ,
      OTABLE varchar(40) CHARACTER SET UTF8                           ,
      OREF integer,
      MUST_CHANGE smallint,
      DAYS_TO_CHANGE integer,
      LAST_CHANGE timestamp,
      NAMESPACE varchar(40) CHARACTER SET UTF8                           ,
      USERUUID varchar(40) CHARACTER SET UTF8                           ,
      LICENCELIMIT SMALLINT_ID)
   as
declare variable CRYPTEDPSW varchar(255);
declare variable LAYER integer;
begin
  select userid, userlogin, imie, nazwisko, uwagi, supervisor, supervisegroup,
    email, grupa, sente, modules, applications, otable, oref, must_change, days_to_change, last_change,
    namespace, useruuid, licencelimit
  from sys_get_userinfo(:usr, :psw, :oldpsw, :id)
  into :userid, :userlogin, :imie, :nazwisko, :uwagi, :supervisor, :supervisegroup,
    email, :grupa, :sente, :modules, :applications, :otable, :oref, :must_change, :days_to_change, :last_change,
    namespace, :useruuid, :licencelimit;
  -- wyczysc parametry
  execute procedure SET_GLOBAL_PARAM('UZYKLI','');
  execute procedure SET_GLOBAL_PARAM('KLIENT','');
  suspend;
end^
SET TERM ; ^
