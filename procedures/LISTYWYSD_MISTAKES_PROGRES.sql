--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_MISTAKES_PROGRES(
      DOKREF integer,
      OPERTRYB varchar(1) CHARACTER SET UTF8                           ,
      OPERREF integer,
      OPERMISTAKE smallint)
   as
declare variable dokmag integer;
begin
  for
    select distinct listywysdpoz.dokref
      from LISTYWYSDPOZ
      where LISTYWYSDPOZ.dokument = :dokref
      order by listywysdpoz.dokref
      into :dokmag
  do begin
    if(:opertryb = 'A') then  /*blad akceptujacego - sprawdzajacego przesylke*/
      update dokumnag set dokumnag.acceptoper = :operref, dokumnag.acceptmistake = :opermistake
        where dokumnag.ref = :dokmag;
    if(:opertryb = 'C') then  /*blad sprawdzajacego(szykujacego) - szykujacego przesylke*/
      update dokumnag set dokumnag.checkoper = :operref, dokumnag.checkmistake = :opermistake
        where dokumnag.ref = :dokmag;
    if(:opertryb = 'S') then  /*blad sprzedawcy - rejestrujacego przesylke*/
      update dokumnag set dokumnag.selloper = :operref, dokumnag.sellmistake = :opermistake
        where dokumnag.ref = :dokmag;
  end
end^
SET TERM ; ^
