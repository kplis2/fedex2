--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ANALYSISOFSTRUCTFACTOR(
      TAXGR STRING1024,
      VATGR STRING1024,
      VOD TIMESTAMP_ID,
      VDO TIMESTAMP_ID)
  returns (
      FACTORYSTRUCT PROCENT)
   as
declare variable SQL string8191;
declare variable nettoniezw numeric_14_2;
declare variable nettospr numeric_14_2;
begin
  taxgr = replace(TAXGR, ';',''',''');
  vatgr = replace(VATGR, ';',''',''');
  taxgr = substring(taxgr from 3);
  taxgr = substring(taxgr from 1 for (char_length(taxgr)-2));
  vatgr = substring(vatgr from 3);
  vatgr = substring(vatgr from 1 for (char_length(vatgr)-2));
  sql = 'select sum((case when bv.vatgr in('||vatgr||') then bv.netv else 0 end)),';
  sql = sql || 'sum((case when bv.vatgr in('||vatgr||') then bv.netv else netv end))';
  sql = sql || 'from bkvatpos bv join bkdocs b on (b.ref=bv.bkdoc)';
  sql = sql || 'where bv.taxgr in ('||taxgr||')  and  '''||:vod||''' < b.vatdate and '''||:vdo||''' > b.vatdate';
  execute statement sql
    into :nettoniezw, :nettospr;

  if (nettoniezw is null) then nettoniezw=0;
  if (nettospr is null) then nettospr=0;
  if (nettospr = 0) then
    exception universal'Zerowa sprzedaż w podanych przedziałach i rejestrach!';
  factorystruct=nettoniezw/nettospr;
  suspend;
end^
SET TERM ; ^
