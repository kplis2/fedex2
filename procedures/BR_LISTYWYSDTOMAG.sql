--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_LISTYWYSDTOMAG(
      MAG integer)
  returns (
      REF integer,
      AKCEPTACJA smallint,
      SYMBOL varchar(255) CHARACTER SET UTF8                           ,
      WARTOSC numeric(14,2),
      KINDSTR varchar(20) CHARACTER SET UTF8                           )
   as
declare variable POZFAK_REF integer;
declare variable GRUPASPED integer;
begin
  select grupasped from dokumnag where ref=:mag into :grupasped;
  if(grupasped is not null) then mag = :grupasped;
  for
    select l.ref, l.akcept, substring(l.symbol||' ('||l.symbolsped||' - '||coalesce(s.opis,'')||')' from 1  for 255)
       from listywysdpoz p
         left join  listywysd l on (l.ref = p.dokument)
         left join sposdost s on (s.ref = l.sposdost)
       where p.dokref = :mag and p.doktyp='M'
       group by l.ref, l.akcept, l.symbol||' ('||l.symbolsped||' - '||coalesce(s.opis,'')||')'
  into :ref, :akceptacja, :symbol
  do begin
    if(:akceptacja=0) then kindstr = 'MI_REDX';
    else if(:akceptacja=1) then kindstr = '14';
    else if(:akceptacja=2) then kindstr = 'MI_PADLOCK';
    suspend;
  end
end^
SET TERM ; ^
