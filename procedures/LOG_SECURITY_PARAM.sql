--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LOG_SECURITY_PARAM(
      SECURITY_LOG integer,
      ACRONYM varchar(20) CHARACTER SET UTF8                           ,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      DICTVALUE varchar(255) CHARACTER SET UTF8                           ,
      USERVALUE varchar(255) CHARACTER SET UTF8                           )
   as
begin
  insert into security_log_params(sECURITY_LOG,ACRONYM,NAME,DICTVALUE,USERVALUE)
    values(:security_log, :acronym, :name, :dictvalue, :uservalue);
end^
SET TERM ; ^
