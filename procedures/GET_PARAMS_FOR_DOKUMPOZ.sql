--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_PARAMS_FOR_DOKUMPOZ(
      DOKUMPOZREF integer,
      DOKUMROZREF integer)
  returns (
      PARAMD1 timestamp,
      PARAMD2 timestamp,
      PARAMD3 timestamp,
      PARAMD4 timestamp,
      PARAMN1 numeric(14,2),
      PARAMN2 numeric(14,2),
      PARAMN3 numeric(14,2),
      PARAMN4 numeric(14,2),
      PARAMN5 numeric(14,2),
      PARAMN6 numeric(14,2),
      PARAMN7 numeric(14,2),
      PARAMN8 numeric(14,2),
      PARAMN9 numeric(14,2),
      PARAMN10 numeric(14,2),
      PARAMN11 numeric(14,2),
      PARAMN12 numeric(14,2),
      PARAMS1 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS2 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS3 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS4 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS5 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS6 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS7 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS8 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS9 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS10 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS11 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS12 varchar(255) CHARACTER SET UTF8                           )
   as
declare variable ktm varchar(80);
declare variable zparamd1 timestamp;
declare variable zparamd2 timestamp;
declare variable zparamd3 timestamp;
declare variable zparamd4 timestamp;
declare variable zparamn1 numeric(14,2);
declare variable zparamn2 numeric(14,2);
declare variable zparamn3 numeric(14,2);
declare variable zparamn4 numeric(14,2);
declare variable zparamn5 numeric(14,2);
declare variable zparamn6 numeric(14,2);
declare variable zparamn7 numeric(14,2);
declare variable zparamn8 numeric(14,2);
declare variable zparamn9 numeric(14,2);
declare variable zparamn10 numeric(14,2);
declare variable zparamn11 numeric(14,2);
declare variable zparamn12 numeric(14,2);
declare variable zparams1 varchar(255);
declare variable zparams2 varchar(255);
declare variable zparams3 varchar(255);
declare variable zparams4 varchar(255);
declare variable zparams5 varchar(255);
declare variable zparams6 varchar(255);
declare variable zparams7 varchar(255);
declare variable zparams8 varchar(255);
declare variable zparams9 varchar(255);
declare variable zparams10 varchar(255);
declare variable zparams11 varchar(255);
declare variable zparams12 varchar(255);
declare variable dparamd1 timestamp;
declare variable dparamd2 timestamp;
declare variable dparamd3 timestamp;
declare variable dparamd4 timestamp;
declare variable dparamn1 numeric(14,2);
declare variable dparamn2 numeric(14,2);
declare variable dparamn3 numeric(14,2);
declare variable dparamn4 numeric(14,2);
declare variable dparamn5 numeric(14,2);
declare variable dparamn6 numeric(14,2);
declare variable dparamn7 numeric(14,2);
declare variable dparamn8 numeric(14,2);
declare variable dparamn9 numeric(14,2);
declare variable dparamn10 numeric(14,2);
declare variable dparamn11 numeric(14,2);
declare variable dparamn12 numeric(14,2);
declare variable dparams1 varchar(255);
declare variable dparams2 varchar(255);
declare variable dparams3 varchar(255);
declare variable dparams4 varchar(255);
declare variable dparams5 varchar(255);
declare variable dparams6 varchar(255);
declare variable dparams7 varchar(255);
declare variable dparams8 varchar(255);
declare variable dparams9 varchar(255);
declare variable dparams10 varchar(255);
declare variable dparams11 varchar(255);
declare variable dparams12 varchar(255);
declare variable parsymbol varchar(20);
declare variable partyp integer;
begin
  /* wyczysc zwracane parametry */
  paramd1 = null;
  paramd2 = null;
  paramd3 = null;
  paramd4 = null;
  paramn1 = null;
  paramn2 = null;
  paramn3 = null;
  paramn4 = null;
  paramn5 = null;
  paramn6 = null;
  paramn7 = null;
  paramn8 = null;
  paramn9 = null;
  paramn10 = null;
  paramn11 = null;
  paramn12 = null;
  params1 = null;
  params2 = null;
  params3 = null;
  params4 = null;
  params5 = null;
  params6 = null;
  params7 = null;
  params8 = null;
  params9 = null;
  params10 = null;
  params11 = null;
  params12 = null;
  /* pobierz parametry z dokumpoza */
  select KTM,
         PARAMD1,PARAMD2,PARAMD3,PARAMD4,
         PARAMN1,PARAMN2,PARAMN3,PARAMN4,
         PARAMN5,PARAMN6,PARAMN7,PARAMN8,
         PARAMN9,PARAMN10,PARAMN11,PARAMN12,
         PARAMS1,PARAMS2,PARAMS3,PARAMS4,
         PARAMS5,PARAMS6,PARAMS7,PARAMS8,
         PARAMS9,PARAMS10,PARAMS11,PARAMS12
  from DOKUMPOZ where REF=:dokumpozref
  into :ktm,
       :zparamd1,:zparamd2,:zparamd3,:zparamd4,
       :zparamn1,:zparamn2,:zparamn3,:zparamn4,
       :zparamn5,:zparamn6,:zparamn7,:zparamn8,
       :zparamn9,:zparamn10,:zparamn11,:zparamn12,
       :zparams1,:zparams2,:zparams3,:zparams4,
       :zparams5,:zparams6,:zparams7,:zparams8,
       :zparams9,:zparams10,:zparams11,:zparams12;
  /* pobierz parametry z dostawy dokumroza */
  if(:dokumrozref is not null) then begin
    select D.PARAMD1,D.PARAMD2,D.PARAMD3,D.PARAMD4,
         D.PARAMN1,D.PARAMN2,D.PARAMN3,D.PARAMN4,
         D.PARAMN5,D.PARAMN6,D.PARAMN7,D.PARAMN8,
         D.PARAMN9,D.PARAMN10,D.PARAMN11,D.PARAMN12,
         D.PARAMS1,D.PARAMS2,D.PARAMS3,D.PARAMS4,
         D.PARAMS5,D.PARAMS6,D.PARAMS7,D.PARAMS8,
         D.PARAMS9,D.PARAMS10,D.PARAMS11,D.PARAMS12
    from DOKUMROZ
    left join DOSTAWY D on (D.REF=DOKUMROZ.DOSTAWA)
    where DOKUMROZ.REF=:dokumrozref
    into :dparamd1,:dparamd2,:dparamd3,:dparamd4,
       :dparamn1,:dparamn2,:dparamn3,:dparamn4,
       :dparamn5,:dparamn6,:dparamn7,:dparamn8,
       :dparamn9,:dparamn10,:dparamn11,:dparamn12,
       :dparams1,:dparams2,:dparams3,:dparams4,
       :dparams5,:dparams6,:dparams7,:dparams8,
       :dparams9,:dparams10,:dparams11,:dparams12;
  end else begin
    dparamd1 = null;
    dparamd2 = null;
    dparamd3 = null;
    dparamd4 = null;
    dparamn1 = null;
    dparamn2 = null;
    dparamn3 = null;
    dparamn4 = null;
    dparamn5 = null;
    dparamn6 = null;
    dparamn7 = null;
    dparamn8 = null;
    dparamn9 = null;
    dparamn10 = null;
    dparamn11 = null;
    dparamn12 = null;
    dparams1 = null;
    dparams2 = null;
    dparams3 = null;
    dparams4 = null;
    dparams5 = null;
    dparams6 = null;
    dparams7 = null;
    dparams8 = null;
    dparams9 = null;
    dparams10 = null;
    dparams11 = null;
    dparams12 = null;

  end
  /* nadaj odpowiednie parametry wynikowe */
  for select DEFCECHY.PARSYMBOL,DEFCECHY.PARTYP
  from ATRYBUTY
  left join DEFCECHY on (DEFCECHY.SYMBOL=ATRYBUTY.CECHA)
  where ATRYBUTY.KTM=:ktm and DEFCECHY.PARTYP>0
  into :parsymbol,:partyp
  do begin
    if(:parsymbol='PARAMD1') then begin if(:partyp>1) then paramd1 = :dparamd1; else paramd1 = :zparamd1; end
    if(:parsymbol='PARAMD2') then begin if(:partyp>1) then paramd2 = :dparamd2; else paramd2 = :zparamd2; end
    if(:parsymbol='PARAMD3') then begin if(:partyp>1) then paramd3 = :dparamd3; else paramd3 = :zparamd3; end
    if(:parsymbol='PARAMD4') then begin if(:partyp>1) then paramd4 = :dparamd4; else paramd4 = :zparamd4; end
    if(:parsymbol='PARAMN1') then begin if(:partyp>1) then paramn1 = :dparamn1; else paramn1 = :zparamn1; end
    if(:parsymbol='PARAMN2') then begin if(:partyp>1) then paramn2 = :dparamn2; else paramn2 = :zparamn2; end
    if(:parsymbol='PARAMN3') then begin if(:partyp>1) then paramn3 = :dparamn3; else paramn3 = :zparamn3; end
    if(:parsymbol='PARAMN4') then begin if(:partyp>1) then paramn4 = :dparamn4; else paramn4 = :zparamn4; end
    if(:parsymbol='PARAMN5') then begin if(:partyp>1) then paramn5 = :dparamn5; else paramn5 = :zparamn5; end
    if(:parsymbol='PARAMN6') then begin if(:partyp>1) then paramn6 = :dparamn6; else paramn6 = :zparamn6; end
    if(:parsymbol='PARAMN7') then begin if(:partyp>1) then paramn7 = :dparamn7; else paramn7 = :zparamn7; end
    if(:parsymbol='PARAMN8') then begin if(:partyp>1) then paramn8 = :dparamn8; else paramn8 = :zparamn8; end
    if(:parsymbol='PARAMN9') then begin if(:partyp>1) then paramn9 = :dparamn9; else paramn9 = :zparamn9; end
    if(:parsymbol='PARAMN10') then begin if(:partyp>1) then paramn10 = :dparamn10; else paramn10 = :zparamn10; end
    if(:parsymbol='PARAMN11') then begin if(:partyp>1) then paramn11 = :dparamn11; else paramn11 = :zparamn11; end
    if(:parsymbol='PARAMN12') then begin if(:partyp>1) then paramn12 = :dparamn12; else paramn12 = :zparamn12; end

    if(:parsymbol='PARAMS1') then begin if(:partyp>1) then params1 = :dparams1; else params1 = :zparams1; end
    if(:parsymbol='PARAMS2') then begin if(:partyp>1) then params2 = :dparams2; else params2 = :zparams2; end
    if(:parsymbol='PARAMS3') then begin if(:partyp>1) then params3 = :dparams3; else params3 = :zparams3; end
    if(:parsymbol='PARAMS4') then begin if(:partyp>1) then params4 = :dparams4; else params4 = :zparams4; end
    if(:parsymbol='PARAMS5') then begin if(:partyp>5) then params5 = :dparams5; else params5 = :zparams5; end
    if(:parsymbol='PARAMS6') then begin if(:partyp>1) then params6 = :dparams6; else params6 = :zparams6; end
    if(:parsymbol='PARAMS7') then begin if(:partyp>1) then params7 = :dparams7; else params7 = :zparams7; end
    if(:parsymbol='PARAMS8') then begin if(:partyp>1) then params8 = :dparams8; else params8 = :zparams8; end
    if(:parsymbol='PARAMS9') then begin if(:partyp>9) then params9 = :dparams9; else params9 = :zparams9; end
    if(:parsymbol='PARAMS10') then begin if(:partyp>1) then params10 = :dparams10; else params10 = :zparams10; end
    if(:parsymbol='PARAMS11') then begin if(:partyp>1) then params11 = :dparams11; else params11 = :zparams11; end
    if(:parsymbol='PARAMS12') then begin if(:partyp>1) then params12 = :dparams12; else params12 = :zparams12; end
  end
end^
SET TERM ; ^
