--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_CHECKGRUPABLOCK(
      DOKUM integer,
      TRYB smallint)
  returns (
      RESULT integer)
   as
declare variable blokada integer;
declare variable grupasped integer;
declare variable cnt integer;
declare variable laczenie smallint;
declare variable magazyn varchar(3);
begin

  /*tryb - 0 - sprawdzenie, czy w ramach grupy są dokumenty inne niz dany, które są zablokowane
           1 - zablokowanie wszystkich dokumentó z grupy
           2 - odblokowanie wszystkich dokumentow w grupie*/
  /*result - 0 - bez akceji
             1 - istnieją dok. w grupie, ktore są zablokowane, a dany nie jest
             2 - istnieją dok. w grupie, które nei są zablokowane, a dany jest*/
  result = 0;
  select blokada, grupasped, magazyn
    from DOKUMNAG
    where ref=:dokum
    into :blokada, :grupasped, :magazyn;
  if(:tryb = 0) then begin
    select d.spedgrupalacz
      from defmagaz d
      where d.symbol = :magazyn
      into :laczenie;
    if (:laczenie > 0) then
    begin
      if(:blokada = 1 or (:blokada = 9)) then begin
        select count(*)
          from DOKUMNAG
          where GRUPASPED = :grupasped
            and ref <> :dokum
            and BLOKADA = 0
            and magazyn = :magazyn
            and blokadafak = 0
          into :cnt;
        if(:cnt > 0) then
          result = 2;
       end else begin
        select count(*)
          from DOKUMNAG
          where GRUPASPED = :grupasped
            and ref <> :dokum
            and BLOKADA = 1
            and magazyn = :magazyn
            and blokadafak = 0
          into :cnt;
        if(:cnt > 0) then
          result = 1;
       end
     end
  end else if(:tryb = 1) then begin
    update DOKUMNAG set BLOKADA = 0 where GRUPASPED = :grupasped and BLOKADA = 1;
  end else if(:tryb = 2) then begin
    update DOKUMNAG set BLOKADA = 1 where GRUPASPED = :grupasped and BLOKADA = 0 and REF<>:dokum;
    update DOKUMNAG set BLOKADA = 9 where GRUPASPED = :grupasped and BLOKADA = 0 and REF=:dokum;
  end

end^
SET TERM ; ^
