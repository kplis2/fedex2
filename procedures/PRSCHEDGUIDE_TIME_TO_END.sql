--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDE_TIME_TO_END(
      PRSCHEDGUIDE integer,
      PREFIX varchar(40) CHARACTER SET UTF8                           )
   as
begin
  -- na każdej operacji wyznaczamy ile bedzie trwac dla harmonogr do lewej
  execute procedure prschedoper_time_to_end(:prschedguide, 0, 0, :prefix);
  -- wyznaczamy najdluzsze galezie w drzewie dla harmonogr do prawej
  execute procedure prschedoper_longest_time(:prschedguide, 0, 0, :prefix);
end^
SET TERM ; ^
