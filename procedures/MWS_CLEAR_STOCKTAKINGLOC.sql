--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CLEAR_STOCKTAKINGLOC(
      WH varchar(3) CHARACTER SET UTF8                           ,
      POSCNTSETTL integer,
      POSCNTONDOC integer,
      DOCTYPEIN varchar(3) CHARACTER SET UTF8                           ,
      DOCTYPEOUT varchar(3) CHARACTER SET UTF8                           )
   as
declare variable cnt integer;
declare variable poscnt integer;
declare variable stockgroup integer;
declare variable mwsstock integer;
declare variable operator integer;
declare variable docgroup integer;
declare variable docsymbs varchar(255);
begin
  operator = 2;
  cnt = 0;
  poscnt = 0;
  execute procedure set_global_param('AKTUOPERATOR',2);
  for
    select s.ref
      from mwsconstlocs c
        left join mwsstock s on (s.mwsconstloc = c.ref)
      where c.locdest = 6 and c.wh = :wh
      order by s.quantity
      into :mwsstock
  do begin
    cnt = cnt + 1;
    poscnt = poscnt + 1;
    execute procedure mws_stocktakingloc_clearing(:mwsstock,:operator,:doctypeout,:doctypein,:docgroup,0)
      returning_values (:docgroup, :docsymbs);
    if (poscnt >= :poscntondoc) then
    begin
      poscnt = 0;
      update dokumnag d set d.akcept = 1 where d.grupasped = :docgroup and d.akcept = 0;
      docgroup = null;
    end
    if (cnt >= :poscntsettl) then
    begin
       update dokumnag d set d.akcept = 1 where d.grupasped = :docgroup and d.akcept = 0;
       exit;
    end
  end
  update dokumnag d set d.akcept = 1 where d.grupasped = :docgroup and d.akcept = 0;
end^
SET TERM ; ^
