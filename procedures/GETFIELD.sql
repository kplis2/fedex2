--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GETFIELD(
      FIELDNAME varchar(40) CHARACTER SET UTF8                           ,
      QUERY varchar(1024) CHARACTER SET UTF8                           )
  returns (
      VAL varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable stmt varchar(2000);
begin
  stmt = 'select first 1 '||:fieldname||' from '||:query;
  execute statement :stmt into :val;
  suspend;
end^
SET TERM ; ^
