--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_SELF_SHIPPING_LABEL(
      SHIPPINGDOC integer)
   as
declare variable label blob_utf8;
declare variable eol string3;
declare variable shippingmethod string20;
declare variable ordersymb string255;
declare variable recipientid integer_id;
declare variable recipientname string255;
declare variable recipientfname string255;
declare variable recipientsname string255;
declare variable recipientstreet street_id;
declare variable recipienthouseno nrdomu_id;
declare variable recipientlocalno nrdomu_id;
declare variable recipientcity string255;
declare variable recipientpostcode postcode;
declare variable packageref integer_id;
declare variable packagecnt smallint_id;
declare variable packageno smallint_id;
declare variable packagetype string20;
declare variable packageweight waga_id;
declare variable charpos smallint_id;
declare variable subiektsymb string255;
begin
  --[PM] XXX #SELFSHIPPING

eol = '
';

  select s.nazwa, l.odbiorcaid,
      trim(coalesce(l.kontrahent,'')), trim(coalesce(l.imie,'')), trim(coalesce(l.nazwisko,'')),
      trim(coalesce(l.adres,'')), trim(coalesce(l.nrdomu,'')), trim(coalesce(l.nrlokalu,'')),
      trim(coalesce(l.miasto,'')), trim(coalesce(l.kodp,''))
    from listywysd l
      left join sposdost s on (l.sposdost = s.ref)
    where l.ref = :shippingdoc
  into :shippingmethod, :recipientid,
    :recipientname, :recipientfname, :recipientsname,
    :recipientstreet, :recipienthouseno, :recipientlocalno,
    :recipientcity, :recipientpostcode;

  execute procedure xk_usun_znaki(:recipientname,6)
    returning_values :recipientname;
  execute procedure xk_usun_znaki(:recipientfname,6)
    returning_values :recipientfname;
  execute procedure xk_usun_znaki(:recipientsname,6)
    returning_values :recipientsname;
  execute procedure xk_usun_znaki(:recipientstreet,6)
    returning_values :recipientstreet;

  select list(distinct coalesce(z.id, n.symbol),','),
      list(distinct coalesce(n.symbol, ''),',')
    from listywysdpoz p
      left join dokumnag n on (p.dokref = n.ref and p.doktyp = 'M')
      left join nagzam z on (n.zamowienie = z.ref)
    where p.dokument = :shippingdoc
  into :ordersymb, :subiektsymb;

  charpos = 0;
  if (:ordersymb is null) then ordersymb = '';
  else charpos = position(',' in :ordersymb);

  select count(o.ref)
    from listywysdroz_opk o
    where o.listwysd = :shippingdoc
      and o.rodzic is null
  into :packagecnt;

  packageno = 1;
  for
    select o.ref, t.nazwa, o.waga
      from listywysdroz_opk o
        left join typyopk t on (o.typopk = t.ref)
      where o.listwysd = :shippingdoc
        and o.rodzic is null
    into :packageref, :packagetype, :packageweight
  do begin

    label = 'I8,2'||:eol||'Q1215,024'||:eol||'q863'||:eol||'rN'||:eol;
    label = :label||'S5'||:eol||'D10'||:eol||'ZT'||:eol||'JF'||:eol||'O'||:eol;
    label = :label||'R24,0'||:eol||'f100'||:eol||'N'||:eol;
  
    label = :label||'A120,26,0,2,3,3,N,"'||:shippingmethod||'"'||:eol;

    if (coalesce(:ordersymb,'') <> '') then
    begin
      label = :label||'A15,100,0,1,3,3,N,"Zamówienia:"'||:eol;
      label = :label||'A15,145,0,2,3,3,N,"'||iif(:charpos > 0, substring(:ordersymb from  1 for  :charpos), :ordersymb)||'"'||:eol;
      if (:charpos > 0) then
        label = :label||'A15,190,0,2,3,3,N,"'||substring(:ordersymb from  :charpos + 1 for  :charpos + 25)||'"'||:eol;
    end
    if (coalesce(:subiektsymb,'') <> '') then
      label = :label||'A15,245,0,2,3,3,N,"'||substring(:subiektsymb from  :charpos + 1 for  :charpos + 25)||'"'||:eol;
  
    if (:recipientid is not null) then
    begin
      label = :label||'A15,292,0,1,3,3,N,"Numer odbiorcy:"'||:eol;
      label = :label||'A15,338,0,2,2,2,N,"'||:recipientid||'"'||:eol;
    end
  
    label = :label||'A15,392,0,1,3,3,N,"Nazwa odbiorcy:"'||:eol;
    label = :label||'A15,437,0,2,2,2,N,"'||substring(:recipientname from  1 for  30)||'"'||:eol;
    label = :label||'A15,477,0,2,2,2,N,"'||substring(:recipientname from  31 for  60)||'"'||:eol;
    label = :label||'A15,518,0,2,2,2,N,"'||substring(:recipientfname||' '||:recipientsname from  1 for  30)||'"'||:eol;
  
    label = :label||'A15,571,0,1,3,3,N,"Adres:"'||:eol;
    label = :label||'A15,616,0,2,2,2,N,"'||:recipientstreet||' '||iif(:recipienthouseno <> '', :recipienthouseno, '')||iif(:recipientlocalno <> '', '/'||:recipientlocalno,'')||'"'||:eol;
    label = :label||'A15,658,0,2,2,2,N,"'||:recipientpostcode||' '||:recipientcity||'"'||:eol;
  
    label = :label||'B140,731,0,1,4,12,224,B,"'||:packageref||'"'||:eol;
  
    label = :label||'A15,1038,0,2,4,4,N,"'||:packagetype||' '||:packageno||'/'||:packagecnt||'"'||:eol;

    label = :label||'A15,1129,0,2,3,3,N,"Waga: '||cast(:packageweight as numeric(14,2))||'"'||:eol;
  
    label = :label||'P1'||:eol;

    update or insert into listywysdopkrpt(doksped, opk, format, rpt)
      values (:shippingdoc, :packageref, 'EPL', :label)
      matching (doksped, opk);

    packageno = :packageno + 1;
  end
end^
SET TERM ; ^
