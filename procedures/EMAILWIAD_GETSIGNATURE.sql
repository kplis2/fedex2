--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWIAD_GETSIGNATURE(
      OPERATORID integer,
      MAILBOXID integer)
  returns (
      FOOTER varchar(2048) CHARACTER SET UTF8                           )
   as
declare variable GETOPERATORFOOTER smallint;
begin
  select mb.getoperatorfooter from mailboxes mb where mb.ref=:mailboxid into :GETOPERATORFOOTER;

  if(GETOPERATORFOOTER=1) then
    begin
      select o.emailfooter from operator o where o.ref=:operatorid into :footer;
    end
  else
    begin
      select mb.emailfooter from mailboxes mb where mb.ref=:mailboxid into :footer;
    end

  suspend;
end^
SET TERM ; ^
