--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_ORDER_APPINI(
      NAMESPACE varchar(40) CHARACTER SET UTF8                           ,
      PARENT varchar(255) CHARACTER SET UTF8                           ,
      PREFIX varchar(255) CHARACTER SET UTF8                           ,
      REF varchar(255) CHARACTER SET UTF8                           ,
      OLDPREV varchar(255) CHARACTER SET UTF8                           ,
      PREV varchar(255) CHARACTER SET UTF8                           )
   as
declare variable currlayer integer;
declare variable minlayer integer;
declare variable newnamespace varchar(40);
declare variable n integer;
declare variable ok smallint;
declare variable ok2 smallint;
declare variable cntbefore integer;
declare variable cntafter integer;
declare variable symbol varchar(255);
declare variable prevsymbol varchar(255);
declare variable nextsymbol varchar(255);
declare variable currsymbol varchar(255);
declare variable oldnumber varchar(255);
declare variable newnumber integer;
declare variable changed smallint;
begin
  select layer from s_namespaces where namespace=:namespace into :currlayer;
  if(:currlayer is null) then currlayer = 0;
  if(:prev is null) then prev = '';
  if(:oldprev is null) then oldprev = '';
  -- ustal kolejnosc na biezacym namespace
  -- odtworz pelne powiazania PREV
  if(:ref is not null) then begin
    -- okresl bylego nastepnika od PREV
    prevsymbol = NULL;
    if(:prev<>'') then begin
      select first 1 s2.val
      from s_appini s1
      join s_appini s2 on (s1.section=s2.section and s2.ident='REF')
      left join s_appini s3 on (s1.section=s3.section and s3.ident='PARENT')
      left join s_namespaces n on (n.namespace=s1.namespace)
      where s1.section starting with :prefix and s1.ident='PREV' and s1.val=:prev
      and n.layer<=:currlayer
      and s2.val<>:ref
      and (coalesce(s3.val,'NULL')=coalesce(:parent,'NULL'))
      order by n.layer desc
      into :prevsymbol;
    end

    -- okresl nastepnika od REF
    nextsymbol = NULL;
    if(:oldprev<>'') then begin
      select first 1 s2.val
        from s_appini s1
        join s_appini s2 on (s1.section=s2.section and s2.ident='REF')
        left join s_appini s3 on (s1.section=s3.section and s3.ident='PARENT')
        left join s_namespaces n on (n.namespace=s1.namespace)
        where s1.section starting with :prefix and s1.ident='PREV' and s1.val=:ref
        and n.layer<=:currlayer
        and (coalesce(s3.val,'NULL')=coalesce(:parent,'NULL'))
        order by n.layer desc
        into :nextsymbol;
    end

    -- przypisz mu nowego poprzednika REF
    if(:prevsymbol is not null) then begin
      execute procedure SYS_SET_APPINI(:namespace,:prefix||:prevsymbol, 'PREV', :ref);
    end
    -- przypisz mu nowego poprzednika OLDPREV
    if(:nextsymbol is not null) then begin
      execute procedure SYS_SET_APPINI(:namespace,:prefix||:nextsymbol, 'PREV', :oldprev);
    end

  end

  -- odtworz NUMBER dla biezacej warstwy
  -- 1. umieszczamy wszystkie elementy do posortowania w tabeli tymczasowej
  delete from SYS_ORDERAPPINI;
  insert into SYS_ORDERAPPINI(SYMBOL,LAYER)
    select s1.VAL, max(n.layer)
      from S_APPINI s1
      left join S_APPINI s2 on (s2.section=s1.section and s2.ident='PARENT')
      left join S_APPINI s3 on (s3.section=s1.section and s3.ident='PREV')
      left join s_namespaces n on (n.namespace=s3.namespace)
      where s1.section starting with :prefix and s1.ident='REF'
      and n.layer<=:currlayer
      and (coalesce(s2.val,'NULL') = coalesce(:parent,'NULL'))
      group by s1.VAL;
--                           insert into expimp(s1,s2) select 'I',symbol from sys_orderappini;

  -- usun poprzednio naliczony NUMBER
  for select symbol from sys_orderappini into :symbol
  do begin
    delete from S_APPINI s1
      where s1.section=:prefix||:symbol
      and s1.ident='NUMBER'
      and s1.namespace=:namespace;
  end
  ok = 100;
  while(ok>0) do begin
    symbol = NULL;
    -- 2. bierzemy 1-szy element bez numeru
    select first 1 symbol from SYS_ORDERAPPINI
    where DISPLACEMENT is NULL
    order by LAYER desc
    into :symbol;
    if(:symbol is not null) then begin
      -- 3a. idziemy w gore szukajac poprzednikow
      cntbefore = 0;
      currsymbol = :symbol;
      ok2 = 200;
      while(:currsymbol is not null and :ok2>0) do begin
        ok2 = :ok2 - 1;
        prevsymbol = NULL;
        select first 1 s.val
          from s_appini s left join s_namespaces n on (n.namespace=s.namespace)
          where s.section=:prefix||:currsymbol and s.ident='PREV' and n.layer<=:currlayer
          order by n.layer desc
          into :prevsymbol;
        if(:prevsymbol is not null and :prevsymbol<>:currsymbol and :prevsymbol<>:symbol and :prevsymbol<>'' and :prevsymbol<>'<NONE>') then begin
          if(exists(select first 1 1 from sys_orderappini where symbol=:prevsymbol and displacement is null)) then begin
            cntbefore = :cntbefore + 1;
            update sys_orderappini set FROMSYMBOL=:symbol, DISPLACEMENT = -:cntbefore where symbol=:prevsymbol;
--                           insert into expimp(s1,s2,s3,s4) values ('U',:prevsymbol,:symbol, -:cntbefore);
          end
          currsymbol = :prevsymbol;
        end else begin
          currsymbol = NULL;
        end
      end
      -- 3b. idziemy w dol szukajac nastepnikow
      cntafter = 0;
      currsymbol = :symbol;
      ok2 = 200;
      while(:currsymbol is not null and :ok2>0) do begin
        ok2 = :ok2 - 1;
        nextsymbol = NULL;
        select first 1 s2.val
          from s_appini s1
          join s_appini s2 on (s1.section=s2.section and s2.ident='REF')
          left join s_appini s3 on (s1.section=s3.section and s3.ident='PARENT')
          left join s_namespaces n on (n.namespace=s1.namespace)
          where s1.section starting with :prefix and s1.ident='PREV' and s1.val=:currsymbol and n.layer<=:currlayer
          and (coalesce(s3.val,'NULL')=coalesce(:parent,'NULL'))
          order by n.layer desc
          into :nextsymbol;
        if(:nextsymbol is not null and :nextsymbol<>:currsymbol and :nextsymbol<>:symbol and :nextsymbol<>'') then begin
          if(exists(select first 1 1 from sys_orderappini where symbol=:nextsymbol and displacement is null)) then begin
            cntafter = :cntafter + 1;
           update sys_orderappini set FROMSYMBOL=:symbol, DISPLACEMENT = :cntafter where symbol=:nextsymbol;
--                           insert into expimp(s1,s2,s3,s4) values ('U',:nextsymbol,:symbol, :cntafter);
          end
          currsymbol = :nextsymbol;
        end else begin
          currsymbol = NULL;
        end
      end

      -- 3c. zapamietujemy licznosc utworzonej grupy
      execute procedure SYS_GET_APPINI(:namespace,:prefix||:symbol,'NUMBER') returning_values :oldnumber, :changed;
      if(:oldnumber='') then oldnumber = '1';
      update sys_orderappini set
        FROMSYMBOL=NULL,
        DISPLACEMENT=0,
        COUNTBEFORE=:cntbefore,
        COUNTAFTER=:cntafter,
        NUMBER=:oldnumber
      where SYMBOL=:symbol;
--                           insert into expimp(s1,s2,s3,s4,s5,s6,s7) values ('U',:symbol,NULL,0,:cntbefore,:cntafter,:oldnumber);
      ok = :ok - 1;
    end else begin
      -- 4. powtarzamy szukanie, chyba ze wszystkie elementy sa polaczone
      ok = 0;
    end

  end
  -- 5. numerujemy elementy z FROMSYMBOL=null i dana grupe
  n = 0;
  for select SYMBOL, COUNTBEFORE, COUNTAFTER
    from sys_orderappini
    where FROMSYMBOL is null
    order by NUMBER
    into :symbol, :cntbefore, :cntafter
  do begin
    n = n + :cntbefore + 1;
    -- zanumeruj biezacy element
    update sys_orderappini set number=:n where symbol=:symbol;
    -- zanumeruj elementy z grupy
    update sys_orderappini set number=:n+DISPLACEMENT where fromsymbol=:symbol;
    n = n + :cntafter;
  end
  -- 6. przepisujemy numery z tabeli tymczasowej to appini
  for select SYMBOL,NUMBER
    from sys_orderappini
    into :symbol,:newnumber
  do begin
    update or insert into S_APPINI(NAMESPACE,SECTION,IDENT,VAL)
      values (:namespace,:prefix||:symbol,'NUMBER',:newnumber)
      matching (NAMESPACE,SECTION,IDENT);
  end

  -- potem odtworz kolejnosc na warstwach potomnych
  if(:currlayer is not null) then begin
    for select distinct s1.namespace
    from s_appini s1
    left join s_appini s2 on (s1.section=s2.section and s2.ident='PARENT')
    left join s_namespaces n on (n.namespace=s1.namespace)
    where s1.section starting with :prefix and s1.ident='PREV' and n.layer>:currlayer
    and (coalesce(s2.val,'NULL')=coalesce(:parent,'NULL'))
    order by n.layer
    into :newnamespace
    do begin
      execute procedure SYS_ORDER_APPINI(:newnamespace,:parent,:prefix,NULL,NULL,NULL);
    end
  end
end^
SET TERM ; ^
