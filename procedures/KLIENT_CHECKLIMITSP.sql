--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KLIENT_CHECKLIMITSP(
      REF integer)
  returns (
      LIMIT numeric(14,2),
      LIMITUSED numeric(14,2),
      LIMITTOUSE numeric(14,2),
      LIMITTYP smallint)
   as
declare variable slodef integer;
declare variable st varchar(255);
declare variable klient integer;
declare variable data timestamp;
begin
  limit = 0;
  limittouse  = 0;
  select KLIENCI.limitsprz, KLIENCI.limitsprtyp, KLIENCI.REF, current_date
   from NAGZAM join KLIENCI on (NAGZAM.KLIENT = KLIENCI.REF)
   where NAGZAM.REF=:ref
   into :limit,:limittyp, :klient, :data;
  if(:limit is null) then limit = 0;
  if(:limittyp is null) then limittyp = 0;
  if(:limit > 0 and :limittyp > 0) then begin
    select REF from SLODEF where SLODEF.typ='KLIENCI' into :slodef;
    st = 'select sum(WINIENZL - MAZL) from ROZRACH where SLODEF ='||:slodef||' and slopoz ='||:KLIENT||' and ';
    if(limittyp = 1) then st = :st||' substring(rozrach.dataotw from 4 for 10) = '||substring(:data from 4 for 10);

    if(limittyp = 3) then st = :st||' substring(rozrach.dataotw from 7 for 10) = '||substring(:data from 7 for 10);
    execute statement :st into :limitused;
    if(:limitused is null) then limitused = 0;
    if(:limitused < 0) then exit;
    if(:limitused > :limit) then begin
      limittouse = 0;
      exit;
    end else begin
        limittouse = :limit - :limitused;
    end
  end else begin
    limitused = 0;
    limittouse = 0;
  end
end^
SET TERM ; ^
