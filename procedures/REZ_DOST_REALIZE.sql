--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REZ_DOST_REALIZE(
      ZAMOWIENIE integer,
      DOKUMNAG integer,
      MAG char(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      CENA numeric(14,4),
      DOSTAWA integer,
      ILOSC numeric(14,4))
  returns (
      ILZBLOK numeric(14,4))
   as
declare variable pozref integer;
declare variable rezilosc numeric(14,4);
declare variable zdejilosc numeric(14,4);
declare variable cnt integer;
declare variable org_ref integer;
begin
  /*zdejmowanie blokad*/
  if(:ilosc is null) then ilosc = 0;
  ilzblok = 0;
  if(:ilosc <=0) then exit;
  --jesli dokument jest do przewodnika, to podmiana zamowienia - bo STANYREZ do przewodnikow sa trzymane orzy zamowieniu oryginalnym
  select nagzam.org_ref
     from dokumnag left join nagzam on (nagzam.ref = dokumnag.zamowienie)
     where dokumnag.skad > 3 and dokumnag.ref = :dokumnag
  into :org_ref;
  if(:org_ref > 0) then
    zamowienie = org_ref;

 for select STANYREZ.ILOSC,STANYREZ.POZZAM
   from STANYREZ
   where KTM = :KTM and wersja = :wersja and magazyn = :mag and ZAMOWIENIE=:zamowienie and
      ((cena = :cena) or (cena is null) or (cena = 0)) and
      ((dostawa = :dostawa) or(dostawa is null) or (dostawa = 0) or(:dostawa is null) or (:dostawa = 0) or (:dostawa is null)) and
      status = 'D' and zreal = 0
   order by cena desc, dostawa desc
   into :rezilosc,:pozref
 do begin
   if(:ilosc > 0) then begin
     if(:ilosc > :rezilosc) then zdejilosc = :rezilosc;
     else zdejilosc = :ilosc;
     if(:zdejilosc > 0) then begin
        cnt = null;
        select count(*) from STANYREZ where ZAMOWIENIE = :zamowienie and POZZAM=:pozref and DOKUMMAG=:DOKUMNAG
           and STATUS='D' and zreal = 1
           into  :cnt;
        if(:cnt is null) then cnt = 0;
        if(:cnt > 0) then
           update STANYREZ set ILOSC = ILOSC + :zdejilosc where ZAMOWIENIE = :zamowienie and POZZAM=:pozref and DOKUMMAG=:DOKUMNAG
             and STATUS='D' and zreal = 1;
        else
          insert into STANYREZ(POZZAM,STATUS,ZREAL,DOKUMMAG,MAGAZYN,KTM,WERSJA,ZAMOWIENIE,DATA,ILOSC,CENA,DOSTAWA,DATABL,PRIORYTET)
           select POZZAM,STATUS,1,:dokumnag,MAGAZYN,KTM,WERSJA,ZAMOWIENIE,DATA,:zdejilosc,CENA,DOSTAWA,DATABL,PRIORYTET from STANYREZ
            where POZZAM=:pozref AND STATUS = 'D' AND ZREAL = 0;

       if(:zdejilosc = :rezilosc) then
         delete from STANYREZ where ZAMOWIENIE = :zamowienie and POZZAM=:pozref and STATUS='D' and ZREAL=0;
       else if(:zdejilosc > 0) then
          update STANYREZ set ILOSC = ILOSC-:zdejilosc where ZAMOWIENIE = :zamowienie and POZZAM=:pozref and STATUS='B' and ZREAL=0;
       ilosc = :ilosc - :zdejilosc;
       ilzblok = :ilzblok + :zdejilosc;
     end
   end
 end
end^
SET TERM ; ^
