--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_15(
      BKDOC integer)
   as
declare variable ACCOUNT varchar(20);
declare variable AMOUNT numeric(14,2);
declare variable CURRAMOUNT numeric(14,2);
declare variable VATV numeric(14,2);
declare variable VATE numeric(14,2);
declare variable TMP varchar(255);
declare variable SETTLEMENT varchar(20);
declare variable DOCDATE timestamp;
declare variable PAYDAY timestamp;
declare variable DESCRIPT varchar(255);
declare variable DICTPOS integer;
declare variable DICTDEF integer;
declare variable OREF integer;
declare variable DEBITED smallint;
declare variable CRNOTE smallint;
declare variable FSOPERTYPE smallint;
declare variable BRANCH_SYMBOL varchar(20);
declare variable PERIOD varchar(6);
declare variable VATPERIOD varchar(6);
declare variable VPERIOD varchar(1);
declare variable TAXGR varchar(10);
declare variable VATGR varchar(5);
declare variable CURRENCY varchar(3);
declare variable CURRATE numeric(15,4);
declare variable SUMPROCENTPLAT numeric(14,2);
declare variable OTABLE varchar(40);
begin
  -- DEMO: standardowy schemat dekretowania - wewnątrzwspólnotowy zakup towaru

  select B.sumgrossv, B.symbol, B.docdate, B.payday, B.descript, B.dictdef,
      B.dictpos, B.oref,  T.creditnote, O.symbol, B.period, B.vatperiod,
      B.currency, B.currate, B.otable
    from bkdocs B
      left join bkdoctypes T on (B.doctype = T.ref)
      left join nagfak N on (B.oref = N.ref)
      left join oddzialy O on (B.branch = O.oddzial)
    where B.ref = :bkdoc
    into :amount, :settlement, :docdate, :payday, :descript, :dictdef, :dictpos,
       :oref, :crnote, :branch_symbol, :period, :vatperiod,
       :currency, :currate, :otable;

  select sum (currnetv) from bkvatpos where bkdoc = :bkdoc
    into :curramount;

  if (coalesce(vatperiod,'') = period) then
    vperiod = '0';
  else
    vperiod = '1';

  execute procedure KODKS_FROM_DICTPOS(dictdef, dictpos)
    returning_values tmp;
  account = '203-' || tmp;


  if (crnote = 1) then
    fsopertype = 4;
  else
    fsopertype = 3;

  execute procedure fk_autodecree_termplat(bkdoc,account, amount, curramount, currate,  currency,  1 , settlement,
                                          oref, otable,fsopertype,docdate,
                                          payday,descript)
    returning_values :sumprocentplat;

  for
    select netv, vatv, vate, debited, taxgr, vatgr
      from bkvatpos  where bkdoc=:bkdoc
      into :amount, :vatv, :vate, :debited, :taxgr, :vatgr
  do begin
    execute procedure insert_decree(bkdoc, '300-' || branch_symbol, 0, amount, descript, 1);

    if (debited = 1) then
    begin
      execute procedure insert_decree(bkdoc, '221-' || vperiod || '-' || taxgr, 0, vatv, descript, 1); -- VAT odliczony
      if (vate-vatv > 0) then
        execute procedure insert_decree(bkdoc, '300' || branch_symbol, 0, vate-vatv, descript, 1); -- Reszta jeżeli co zostalo bo bylo czesciowe odliczenie
    end
    else
      execute procedure insert_decree(bkdoc, '300' || branch_symbol, 0, vate, descript, 1);

    execute procedure insert_decree(bkdoc, '222-' || vperiod || '-' || taxgr || '-' || vatgr, 1, vate, descript, 1);
  end
end^
SET TERM ; ^
