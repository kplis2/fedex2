--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CHANGE_EMWSCONSTLOC_CART(
      CART varchar(40) CHARACTER SET UTF8                           ,
      EMWSCONSTLOC varchar(40) CHARACTER SET UTF8                           )
  returns (
      MSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable mwsconstloc integer;
declare variable cnt integer;
declare variable aref integer;
begin
  select c.ref
    from mwsconstlocs c
    where c.symbol = :cart
    into mwsconstloc;
  if (mwsconstloc is null) then
  begin
    msg = 'Nie zdefiniowano takiego wózka';
    exit;
  end
  cnt = 0;
  for
    select a.ref
      from mwsacts a
      where a.mwsconstloc = :mwsconstloc
      into aref
  do begin
    cnt = cnt + 1;
    update mwsacts a set a.emwsconstloc = :emwsconstloc where a.ref = :aref;
  end
  if (cnt = 0) then
    msg = 'Na tym wózku nic nie było';
end^
SET TERM ; ^
