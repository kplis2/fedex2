--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDOPERS_STATUSCHECK(
      PRSCHEDZAM integer,
      PRSCHEDGUIDE integer,
      PRSCHEDOPER integer)
   as
declare variable OPERSTATUS smallint;
declare variable GUIDESTATUS smallint;
declare variable OPERNAME varchar(20);
declare variable REPORTED smallint;
declare variable ISREPORT smallint;
declare variable OPERAMOUNTIN numeric(14,4);
declare variable OPERAMOUNTRESULT numeric(14,4);
begin
  if(prschedzam is null and prschedguide is null and prschedoper is not null) then
    select schedzam, guide from prschedopers where ref = :prschedoper into :prschedzam, :prschedguide;
  select psg.status from prschedguides psg where psg.ref = :prschedguide into :guidestatus;
  if(guidestatus = 0) then exit;
--SN w sumie może być ewidencja bez pobrania  if(guidestatus = 0) then exit;
  --statusy operacji początkowych
  update prschedopers po set po.status = 2 where po.status = 0 and po.schedzam = :prschedzam
    and (:prschedguide is null or po.guide = :prschedguide)
    and (not exists (select pod.ref from prschedoperdeps pod where pod.depto = po.ref));
--  update prschedopers po set po.status = 0 where po.status = 2 and po.schedzam = :prschedzam
--    and (exists (select pod.ref from prschedoperdeps pod where pod.depto = po.ref));

--ustawianie statusów operacji nieraportowanych
  operstatus = -1;
  isreport = 0;
  select oper, status, reported, amountin, amountresult
    from prschedopers
    where ref = :prschedoper
  into :opername, :operstatus, :reported, :operamountin, :operamountresult;

  if(exists(select ref from propersraps where prschedoper = :prschedoper)
      or exists(select ref from prshortages where prschedoper = :prschedoper)) then
    isreport = 1;

    --jezeli to jest ostatnia operacja i nie jest aktywna to zamykam
  if (:operstatus = 2 and :reported = 0
    and not exists(select first 1 1 from prschedoperdeps d where d.depfrom = :prschedoper)
    and not exists (select first 1 1 from prschedoperdeps d join prschedopers o on (o.ref = d.depfrom)
      where d.depto = :prschedoper and o.status < 3)
  ) then begin
    update prschedopers o set o.status = 3 where o.ref = :prschedoper;
    operstatus = 3;
  end
  -- jezeli operacja jest zamknieta a jest raportowalna i nie ma raportu to zmiana na w toku
  if (:operstatus = 3 and :reported > 0 and :isreport = 0) then
  begin
    update prschedopers o set o.status = 2 where o.ref = :prschedoper;
    operstatus = 2;
  end

  if(operstatus = 0) then begin
    --wszystkie nieraportowane nastepne nierozpoczete gdy obecna nierozpoczeta
      update prschedopers po set po.status = 0 where po.status > 0 and po.reported = 0 and po.guide = :prschedguide
        and (exists(select pd.ref from prschedoperdeps pd where pd.depto = po.ref and pd.depfrom = :prschedoper));
  end else if(operstatus = 2) then begin
    --wszystkie nieraportowane nastepne w trakcie realizacji gdy obecna ma raport
    if(reported = 0 or reported > 0 and isreport = 1) then
    begin
      update prschedopers po set po.status = 2 where po.reported = 0 and po.activ = 1 and po.guide = :prschedguide
        and (exists(select pd.ref from prschedoperdeps pd where pd.depto = po.ref and pd.depfrom = :prschedoper));
      -- wszystkie nastepne zrealizowane gdy nie sa  aktywne
      update prschedopers po set po.status = 3 where po.reported = 0 and po.activ = 0 and po.guide = :prschedguide
        and (exists(select pd.ref from prschedoperdeps pd where pd.depto = po.ref and pd.depfrom = :prschedoper));
    end else begin
    --wszystkie nieraportowane nastepne nierozpoczete gdy obecna nierozpoczeta
      update prschedopers po set po.status = 0 where po.status > 0 and po.reported = 0 and po.guide = :prschedguide
        and (exists(select pd.ref from prschedoperdeps pd where pd.depto = po.ref and pd.depfrom = :prschedoper));
    end
    --wszystkie nieraportowane poprzednie w trakcie realizacji jesli nie ma nastepnej zrealizowanej
      update prschedopers po set po.status = 2 where po.status > 2 and po.reported = 0 and po.guide = :prschedguide
        and (exists(select pd.ref from prschedoperdeps pd where pd.depfrom = po.ref and pd.depto = :prschedoper))
        and (not exists(select pd2.ref from prschedoperdeps pd2 where pd2.depfrom = po.ref and
               (select po2.status from prschedopers po2 where po2.ref = pd2.depto) = 3));
  end else if(operstatus = 3) then begin
    --nieraportowane zrealizowana gdy jakas poprzednia zrealizowana
    update prschedopers po set po.status = 3 where po.status < 3 and po.reported = 0 and po.guide = :prschedguide
      and (exists(select pd.ref from prschedoperdeps pd where pd.depfrom = po.ref and pd.depto = :prschedoper));
    --nieraportowalne nastepne zrealizowane gdy obecna zamknieta
    update prschedopers o set o.status = 3 where o.status < 3 and o.reported = 0 and o.guide = :prschedguide and
      exists (select first 1 1 from prschedoperdeps d where d.depto = o.ref and d.depfrom = :prschedoper);
  end
--ustawianie statusów operacji raportowanych
  --sprawdzenie czy można
  if(operstatus = 0 and isreport = 1) then begin
    exception prschedopers_error ' "'||:opername||'" ma raport. Wycofanie niemożliwe !';
  end else if(operstatus = 3) then begin
      opername = '';
      select pow.oper
        from prschedoperdeps powd
        left join prschedopers pow on (powd.depfrom = pow.ref)
        where powd.depto = :PRSCHEDOPER and pow.status < 3 and pow.reported > 0
      into :opername;
      if(opername <> '') then
        exception prschedopers_error 'Poprzednia operacja "'||:opername||'" niezrealizowana !';
  end
  --na w toku
  if(operstatus = 2) then begin
    -- wszystkie nastepne raportowane w toku jesli biezaca nieraportowalna lub ma raport
    if(:reported = 0 or :reported > 0 and :isreport = 1) then begin
      update prschedopers po set po.status = 2 where po.status <> 2 and reported > 0 and po.guide = :prschedguide
       and exists(select pd.ref from prschedoperdeps pd where pd.depto = po.ref and pd.depfrom = :prschedoper);
    end else begin
      update prschedopers po set po.status = 0 where po.status > 0 and reported > 0 and po.guide = :prschedguide
        and exists(select pd.ref from prschedoperdeps pd where pd.depto = po.ref and pd.depfrom = :prschedoper);
    end
  end else if(operstatus = 0) then begin
    update prschedopers po set po.status = 0 where po.status > 0 and reported > 0 and po.guide = :prschedguide
      and exists(select pd.ref from prschedoperdeps pd where pd.depto = po.ref and pd.depfrom = :prschedoper);
  end else if(operstatus = 3 and operamountresult = 0) then begin
  --zrealizowane wszystkie nastepne jesli biezaca zrealizowana i na wyjsciu 0
    update prschedopers po set po.status = 3 where reported > 0 and po.guide = :prschedguide
      and exists(select pd.ref from prschedoperdeps pd where pd.depto = po.ref and pd.depfrom = :prschedoper);
  end
end^
SET TERM ; ^
