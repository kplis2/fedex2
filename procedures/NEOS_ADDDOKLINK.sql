--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NEOS_ADDDOKLINK(
      DOKPLIKREF integer,
      FILEPATH varchar(255) CHARACTER SET UTF8                           ,
      FILENAME varchar(255) CHARACTER SET UTF8                           ,
      SYMBOL varchar(255) CHARACTER SET UTF8                           ,
      EXT varchar(20) CHARACTER SET UTF8                           )
  returns (
      NEWREF integer)
   as
declare variable PLIK varchar(255);
declare variable FOLDER varchar(10);
declare variable newplik varchar(255);
declare variable aktuoper varchar(255);
declare variable blokada smallint;
declare variable operblok integer;
declare variable l integer;
begin
  filepath = coalesce(:filepath,'');
  dokplikref = coalesce(:dokplikref,0);
  if (:filepath = '' or :dokplikref = 0) then exit;
  execute procedure GET_GLOBAL_PARAM('AKTUOPERATOR') returning_values :aktuoper;
  -- sprawdz czy to nie jest link do www
  if(:filepath like 'http://%' or :filepath like 'https://%') then begin
    -- sprawdz czy taki link nie istnieje
    if(not exists(select first 1 1 from doklink where dokplik=:dokplikref and plik=:filepath)) then begin
      -- dodaj link
      execute procedure GEN_REF('DOKLINK') returning_values :newref;
      insert into doklink(REF, DOKPLIK, LOCALFILE,PLIK,SYMBOL)
        values(:newref, :dokplikref, null, :filepath, substring(:filepath from 1 for 60));
    end
    exit;
  end

  -- usun liczbe w nawiasach z konca symbolu
  l = coalesce(char_length(:symbol),0); -- [DG] XXX ZG119346
  if(:l>0 and substring(:symbol from (:l) for 1)=')') then begin
    while(:l>0 and substring(:symbol from (:l) for 1)<>'(') do begin
      l = :l - 1;
    end
    while(:l>0 and substring(:symbol from (:l) for 1)<>' ') do begin
      l = :l - 1;
    end
    if(:l>0) then symbol = substring(:symbol from 1 for (:l-1));
  end
  -- najpierw sprawdzamy czy taki doklink juz nie istnieje
  for select l.ref,l.plik,d.blokada,d.operblok
    from doklink l
    left join dokplik d on d.ref=l.dokplik
    where l.dokplik = :dokplikref
  into :newref,:plik,:blokada,:operblok
  do begin
    execute procedure gen_doklinkplik(:newref, :symbol, :ext) returning_values :newplik, :folder;
    if(:plik=:newplik) then begin
      --znaleziono doklinka. Jesli jest on pobrany przez nas lub wcale to go aktualizujemy
      if(:blokada=0 or :operblok=:aktuoper) then begin
        update doklink set localfile = :filepath, nowy = 1 where ref=:newref;
        exit;
      end else begin
        -- W przeciwnym razie zwracamy blad
        exception universal 'Nie można modyfikować pliku pobranego przez innego operatora.';
      end
    end
  end

  -- zakladamy nowego doklinka
  execute procedure GEN_REF('DOKLINK') returning_values :newref;
  execute procedure gen_doklinkplik(:newref, :symbol, :ext) returning_values :plik, :folder;

  insert into doklink(REF, DOKPLIK, LOCALFILE,PLIK,SYMBOL)
   values(:newref, :dokplikref, :filepath, :plik, substring(:symbol from 1 for 60));
end^
SET TERM ; ^
