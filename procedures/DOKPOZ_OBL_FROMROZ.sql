--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKPOZ_OBL_FROMROZ(
      POZDOKUM integer)
   as
declare variable ilosc numeric(14,4);
declare variable iloscroz numeric(14,4);
declare variable iloscl numeric(14,4);
declare variable wartosc numeric(14,2);
declare variable pwartosc numeric(14,2);
declare variable wart_koszt numeric(14,2);
declare variable ack smallint;
begin
  select AKCEPT from DOKUMPOZ left join DOKUMNAG on (DOKUMPOZ.DOKUMENT = DOKUMNAG.REF) where DOKUMPOZ.REF= :POZDOKUM into :ack;
  if(:ack is null) then ack = 0;
  if(:ack = 1) then begin
    select sum(wartosc), sum(pwartosc), sum(wart_koszt), sum(iloscl), sum(ilosc) from DOKUMROZ where POZYCJA=:pozdokum
      into :wartosc, :pwartosc, :wart_koszt, :iloscl, :iloscroz;
    if(:wartosc is null) then wartosc = 0;
    if(:pwartosc is null) then pwartosc = 0;
    if(:wart_koszt is null) then wart_koszt = 0;
    if(:iloscl is null) then iloscl = 0;
    select ilosc from dokumpoz where ref=:pozdokum into :ilosc;
    if(:ilosc is null) then ilosc = 0;
    if(:ilosc <> 0)then begin
      update DOKUMPOZ set ILOSCL = :iloscl, ILOSCROZ = :iloscroz, WARTOSC=:wartosc, PWARTOSC=:pwartosc, WART_KOSZT=:wart_koszt,
        CENASR = :wartosc/:ilosc, PCENASR = :pwartosc/:ilosc, CENASR_KOSZT = :wart_koszt/:ilosc,
        ROZ_BLOK = 1 where ref = :pozdokum;
    end
  end else
    update DOKUMPOZ set CENASR = CENA, WARTOSC = CENA*ILOSC, CENASR_KOSZT = CENA_KOSZT, WART_KOSZT = CENA_KOSZT*ILOSC, ILOSCLWYS = 0 where ref = :pozdokum;
end^
SET TERM ; ^
