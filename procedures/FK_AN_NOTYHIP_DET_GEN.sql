--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_AN_NOTYHIP_DET_GEN(
      DATA timestamp,
      DOUNCLOSED smallint,
      FORACCOUNT KONTO_ID,
      FORSLODEF integer,
      FORSLOPOZ integer,
      COMPANY integer)
  returns (
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      SLODEF integer,
      SLOPOZ integer,
      KONTOFK KONTO_ID,
      MAINWN numeric(14,2),
      MAINMA numeric(14,2),
      SYMBFAK varchar(20) CHARACTER SET UTF8                           ,
      DATAOTW timestamp,
      DATAPLAT timestamp,
      SUMINTEREST numeric(14,2))
   as
declare variable datazm timestamp;
    declare variable saldown numeric(14,2);
    declare variable kwotafak numeric(14,4);
    declare variable kwotazap numeric(14,4);
    declare variable wn numeric(14,2);
    declare variable ma numeric(14,2);
    declare variable dt timestamp;
    declare variable notadokum integer;
    declare variable interest numeric(14,2);
    declare variable refnagfak integer;
    declare variable tabela integer;
    declare variable topay numeric(14,2);
begin
  -- procedura uzywana w analizach rozrachunkow

  for select ROZRACH.SLODEF, ROZRACH.SLOPOZ
    from ROZRACH
    where rozrach.dataplat < :data
          and ((:forslodef is null) or (:forslodef = 0) or (rozrach.slodef = :forslodef))
          and ((:forslopoz is null) or (:forslopoz = 0) or (rozrach.slopoz = :forslopoz))
          and ((:FORACCOUNT is null) or (:FORACCOUNT = '') or (rozrach.kontofk like :FORACCOUNT))
          and ((:dounclosed = 1) or (cast(rozrach.datazamk as date) <= :data) or (rozrach.datazamk is null))
          and ((rozrach.datazamk > rozrach.dataplat) or (rozrach.datazamk is null))
          and company = :company
    group by rozrach.slodef, rozrach.slopoz
    into :slodef, :slopoz
  do begin
    --nagówek dokument
    select nazwa from slo_dane(:slodef, :slopoz) into :nazwa;
    tabela = null;
    if (slodef = 1) then
      select interesttables
        from klienci
          where ref = :slopoz
      into :tabela;
    if (tabela is null) then
      select ref
        from interesttables
        where isdefault = 1
      into :tabela;
    if (:tabela is null) then
      exception FK_EXPT_TAB_ODSET;

    for
      select r.SYMBFAK, r.KONTOFK, r.dataotw, r.dataplat, r.faktura, sum(p.winien), sum(p.ma)
        from ROZRACH r
          join rozrachp p on (r.slodef = p.slodef and r.slopoz = p.slopoz
            and r.kontofk = p.kontofk and r.symbfak = p.symbfak and r.company = p.company)
        where r.SLODEF = :slodef and r.slopoz = :slopoz
           and r.dataplat < :data and (r.datazamk <= :data or (:dounclosed = 1) or (r.datazamk is null))
           and ((r.datazamk > r.dataplat) or (r.datazamk is null))
           and r.company = :company
           and p.data <= :data
        group by r.SYMBFAK, r.KONTOFK, r.dataotw, r.dataplat, r.faktura
        into :symbfak, :kontofk, :dataotw, :dataplat, :refnagfak, :mainwn, :mainma
    do begin
      --analiza pojedynczego rozrachunku
      kwotafak = 0;
      kwotazap = 0;
      saldown = 0;
      datazm = :dataplat;

      suminterest = 0;
      --
      for
        select WINIEN, MA, stransdate, notadokum
          from ROZRACHP
          where slodef = :slodef and slopoz = :slopoz
            and symbfak = :symbfak and kontofk = :kontofk and company = :company
            and cast(data as date) <= :data
          order by data
          into :wn, :ma, :dt, :notadokum
      do begin
        --obliczenie dni i odsetek do dnia zmiany
        if (wn > 0 and kwotafak = 0) then
          kwotafak = wn;

        if (ma > 0) then
          kwotazap = kwotazap + ma;

        if (ma > 0) then
          saldown = saldown - ma;
        else
          saldown = saldown + wn;

        if (ma > 0 and kwotafak > 0) then
        begin
          if (saldown < 0) then
            kwotazap = kwotazap + saldown; --zmniejszenie kwoty zaplconej
          if (notadokum is null) then
          begin
            if (dt > dataplat) then
            begin
              interest = 0;
              if (dt >= datazm ) then
              begin
                execute procedure INTEREST_COUNT(:kontofk, :symbfak, :dataplat,:dt,:kwotazap, :tabela, :company) returning_values :interest;
                if (interest is not null) then
                  suminterest = suminterest + interest;
              end
            end

            if (saldown < 0) then
              kwotazap = kwotazap - saldown;  --zmniejszenie kwoty zaplconej
          end
          kwotazap = kwotazap - ma;
        end
        datazm = dt;
      end
      -- koniec naliczania pozycji
      -- naliczanie pozostalsci do zapaty
      topay = mainwn - mainma;
      if (topay > 0) then
      begin
        execute procedure INTEREST_COUNT(:kontofk, :symbfak, :dataplat,:data,:topay, :tabela, :company) returning_values :interest;
        if (interest is not null) then
          suminterest = suminterest + interest;
      end
       if (suminterest <> 0) then
         suspend;
      -- koniec pojedynczego rozrachunku
    end
  end
end^
SET TERM ; ^
