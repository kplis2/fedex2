--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_AUTODECREE(
      BKDOC integer,
      TYP integer,
      AUTOREGSCHEME integer)
   as
declare variable bkdoctype integer;
declare variable otable varchar(255);
declare variable oref integer;
declare variable stmt varchar(255);
declare variable procname varchar(255);
begin
    select b.otable, b.oref, b.doctype
    from BKDOCS B
    where ref = :bkdoc
    into :otable, :oref, :bkdoctype;
  --najpier okreslam typ, jesli niepodany
  if(typ = -1) then begin
    if(:otable = 'VALDOCS') then
      typ = 7;
  end
    if(:autoregscheme = -1) then begin
      if(typ = 7)then begin
        --pobieram schemat dekretacji wskazany na typie dokumentu ST
        select VT.AUTOREGSCHEME
        from valdocs V
        left join vdoctype VT on (v.doctype = vt.symbol)
        where v.ref = :oref
        into :autoregscheme;
      end else
        --pobieram schemat dekretacji z typu dokumentu FK
        select AUTOREGSCHEME
        from bkdoctypes
        where ref = :bkdoctype
        into :autoregscheme;
    end

    if (:autoregscheme > 0) then
    begin
      execute procedure FK_AUTOREGSCHEME_GETPROCNAME(:autoregscheme) returning_values (:procname);
      stmt = 'execute procedure '|| procname || '(' || :bkdoc|| ')';
      execute statement stmt;
    end
end^
SET TERM ; ^
