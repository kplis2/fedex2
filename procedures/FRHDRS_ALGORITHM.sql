--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRHDRS_ALGORITHM(
      FRHDRS varchar(20) CHARACTER SET UTF8                           ,
      FRPSN integer,
      FRCOL integer)
  returns (
      ALGORITHM varchar(8191) CHARACTER SET UTF8                           )
   as
declare variable talg varchar(8191);
declare variable algtype smallint;
declare variable frversion integer;
begin
  algorithm = '';
  select fr.algorithm from frpsns fr where fr.ref = :frpsn into :algtype;
  if (:algtype = 0) then begin
    --puste
    algorithm = '';
  end else if (:algtype = 1) then begin
    --recznie
    algorithm = '';
  end else if (:algtype = 2) then begin
    --suma wierszy
    select first 1 fs.frversion from frsumpsns fs where fs.frpsn = :frpsn order by fs.frversion desc into :frversion;
    for select iif(fs.ratio = 1,'+','-')||' '||fr.symbol
      from frsumpsns fs
        join frpsns fr on (fr.ref = fs.sumpsn)
      where fs.frpsn = :frpsn
        and fs.frversion = :frversion
      into :talg
    do begin
      if (:talg is null) then talg = '';
      if (:talg <> '') then begin
        if (:algorithm = '') then algorithm = :talg;
        else algorithm = :algorithm || ' ' || :talg;
      end
    end
  end else if (:algtype = 3) then begin
    select first 1 fa.proced
      from frpsnsalg fa
      where fa.frpsn = :frpsn
        and fa.frcol = :frcol
      order by fa.frversion desc
    into :talg;
    if (talg is null) then  talg = '';
    if (talg <> '') then algorithm = :talg;
  end
  suspend;
end^
SET TERM ; ^
