--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_COMIT_WT_SERWIS(
      MWSORDREF INTEGER_ID,
      MODE SMALLINT_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING)
   as
begin
  status = 1;
  msg = '';

  if (mode = 0) then begin
    update mwsacts a set a.status = 0 where a.mwsord = :mwsordref;
    update mwsords o set o.status = 0 where o.ref = :mwsordref;
    delete from mwsords o where o.ref = :mwsordref;
  end else begin
    update mwsords o set  o.status = 1 where o.ref =:MWSORDREF;
    update mwsords o set  o.status = 5 where o.ref =:MWSORDREF;

  end

end^
SET TERM ; ^
