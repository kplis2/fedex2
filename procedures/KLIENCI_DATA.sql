--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KLIENCI_DATA(
      KLIENTZFAK integer,
      DATAFAK timestamp,
      OTABLE STRING40 = null,
      OREF INTEGER_ID = null)
  returns (
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      KODP char(10) CHARACTER SET UTF8                           ,
      MIASTO varchar(255) CHARACTER SET UTF8                           ,
      ULICA varchar(255) CHARACTER SET UTF8                           ,
      POCZTA varchar(255) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE LICZBAREK INTEGER;
DECLARE VARIABLE MINDATA TIMESTAMP;
begin
  select count(*), min(data)  from kliencihist
    where klient = :klientzfak and data > :datafak  into :liczbarek ,:mindata ;
  if (liczbarek = 0) then
    select nazwa, kodp, miasto,
        ulica||iif(coalesce(nrdomu,'')<>'',' '||nrdomu,'')||iif(coalesce(nrlokalu,'')<>'','/'||nrlokalu,''),
        poczta
      from klienci
      where ref = :klientzfak
    into :nazwa, :kodp, :miasto, :ulica, :poczta;
  else if (liczbarek >0) then
    select nazwa, kodp, miasto, ulica, poczta from kliencihist
      where data = :mindata and klient = :klientzfak
    into :nazwa, :kodp, :miasto, :ulica, :poczta;
  suspend;
end^
SET TERM ; ^
