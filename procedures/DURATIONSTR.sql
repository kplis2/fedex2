--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DURATIONSTR(
      S timestamp,
      E timestamp)
  returns (
      TS varchar(8) CHARACTER SET UTF8                           )
   as
declare variable h integer;
declare variable m integer;
begin
  m = (e - s)*1440; /*minut w sumie*/
  h = m/60; /*godzin*/
  m = m-h*60; /* minuty pozostale */
  if( h=0 AND m=0 ) then
    ts = '';
  else
    begin
    /* dodaje godziny */
    ts = CAST( h as VARCHAR(5))||':';
    /* dodaje minuty */
    if( h<10 ) then
      ts  = ts||'0';
    ts  = ts||CAST( m as VARCHAR(3));
    end
  suspend;
end^
SET TERM ; ^
