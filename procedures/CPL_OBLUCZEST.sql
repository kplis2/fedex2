--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CPL_OBLUCZEST(
      UCZEST integer,
      TYP integer)
   as
declare variable ilpktp integer;
declare variable ilpktm integer;
declare variable konto integer;
begin
   select sum(ilpkt) from CPLOPER where CPLUCZEST = :UCZEST and PM = '+' and TYPPKT = :TYP
   into :ilpktp;
   if(:ilpktp is null) then ilpktp = 0;
   select sum(ilpkt) from CPLOPER where CPLUCZEST = :UCZEST and PM = '-' and TYPPKT = :TYP
   into :ilpktm;
   if(:ilpktm is null) then ilpktm = 0;
   select count(*) from CPLKONTA where CPLUCZEST = :UCZEST and TYPPKT = :TYP into :konto;
   if(konto=0) then begin
     insert into CPLKONTA (CPLUCZEST,TYPPKT,SUMPKTP,SUMPKTM) values (:UCZEST,:TYP,:ilpktp,:ilpktm);
   end else begin
     update CPLKONTA set SUMPKTP = :ilpktp, SUMPKTM = :ilpktm where CPLUCZEST=:UCZEST and TYPPKT = :TYP;
   end
end^
SET TERM ; ^
