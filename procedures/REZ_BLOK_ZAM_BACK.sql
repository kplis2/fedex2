--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REZ_BLOK_ZAM_BACK(
      ZAM integer,
      REALIZ integer)
   as
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable mag varchar(3);
declare variable cena numeric(14,2);
declare variable dostawa integer;
declare variable ilosc numeric(14,4);
declare variable zdejblok integer;
declare variable pozref integer;
declare variable data date;
declare variable status integer;
declare variable oldilosc numeric(14,4);
declare variable oldpozref integer;
declare variable zdejilosc numeric(14,2);
declare variable stan char;
declare variable stat integer;
begin
/* Kuba - wyglada na nieuzywana, tymczasowo wyremowuje
  select TERMDOST from NAGZAM where REF=:realiz into :data;
  for select REF, MAGAZYN, KTM, WERSJA, CENAMAG, DOSTAWAMAG, ILREAL
   from POZZAM where ZAMOWIENIE=:REALIZ
   into :pozref, :mag, :ktm, :wersja, :cena, :dostawa, :ilosc
  do begin
    select sum(ilosc) from STANYREZ where POZZAM=:pozref and ZREAL = 0 into :ilosc;
    for select STANYREZ.ILOSC,STANYREZ.POZZAM from STANYREZ
      where KTM = :KTM and wersja = :wersja and magazyn = :mag and ZAMOWIENIE=:zam
        and status = 'B' and zreal = 1 and DOKUMMAG = 0
        and ((cena = :cena) or (cena is null) or (cena = 0))
        and ((dostawa = :dostawa) or(dostawa is null) or (dostawa = 0) or(:dostawa is null) or (:dostawa = 0) or (:dostawa is null))
    order by cena , dostawa
    into :oldilosc, :oldpozref
    do begin
      if(:ilosc > 0) then begin
        if(:ilosc > :oldilosc) then zdejilosc = :oldilosc;
        else zdejilosc = :ilosc;
        if(:zdejilosc > 0) then begin
          if(:zdejilosc = :oldilosc) then
            delete from STANYREZ where POZZAM=:oldpozref and STATUS='B' and ZREAL=1 and DOKUMMAG = 0;
          else if(:zdejilosc > 0) then
             update STANYREZ set ILOSC = ILOSC-:zdejilosc where POZZAM=:oldpozref and STATUS='B' and ZREAL=1 and DOKUMMAG= 0;
          ilosc = :ilosc - :zdejilosc;
        end
      end
    end
    if(:ilosc > 0) then
    for select STANYREZ.ILOSC,STANYREZ.POZZAM from STANYREZ
      where KTM = :KTM and wersja = :wersja and magazyn = :mag and ZAMOWIENIE=:zam
        and status = 'Z' and zreal = 1 and DOKUMMAG = 0
        and ((cena = :cena) or (cena is null) or (cena = 0))
        and ((dostawa = :dostawa) or(dostawa is null) or (dostawa = 0) or(:dostawa is null) or (:dostawa = 0) or (:dostawa is null))
    order by cena , dostawa
    into :oldilosc, :oldpozref
    do begin
      if(:ilosc > 0) then begin
        if(:ilosc > :oldilosc) then zdejilosc = :oldilosc;
        else zdejilosc = :ilosc;
        if(:zdejilosc > 0) then begin
          if(:zdejilosc = :oldilosc) then
            delete from STANYREZ where POZZAM=:oldpozref and STATUS='Z' and ZREAL=1 and DOKUMMAG = 0;
          else if(:zdejilosc > 0) then
             update STANYREZ set ILOSC = ILOSC-:zdejilosc where POZZAM=:oldpozref and STATUS='Z' and ZREAL=1 and DOKUMMAG= 0;
          ilosc = :ilosc - :zdejilosc;
        end
      end
    end
    if(:ilosc > 0) then
    for select STANYREZ.ILOSC,STANYREZ.POZZAM from STANYREZ
      where KTM = :KTM and wersja = :wersja and magazyn = :mag and ZAMOWIENIE=:zam
        and status = 'R' and zreal = 1 and DOKUMMAG = 0
        and ((cena = :cena) or (cena is null) or (cena = 0))
        and ((dostawa = :dostawa) or(dostawa is null) or (dostawa = 0) or(:dostawa is null) or (:dostawa = 0) or (:dostawa is null))
    order by cena , dostawa
    into :oldilosc, :oldpozref
    do begin
      if(:ilosc > 0) then begin
        if(:ilosc > :oldilosc) then zdejilosc = :oldilosc;
        else zdejilosc = :ilosc;
        if(:zdejilosc > 0) then begin
          if(:zdejilosc = :oldilosc) then
            delete from STANYREZ where POZZAM=:oldpozref and STATUS='R' and ZREAL=1 and DOKUMMAG = 0;
          else if(:zdejilosc > 0) then
             update STANYREZ set ILOSC = ILOSC-:zdejilosc where POZZAM=:oldpozref and STATUS='R' and ZREAL=1 and DOKUMMAG= 0;
          ilosc = :ilosc - :zdejilosc;
        end
      end
    end
  end
  */
end^
SET TERM ; ^
