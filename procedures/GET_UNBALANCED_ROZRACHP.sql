--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_UNBALANCED_ROZRACHP(
      ACCOUNT ACCOUNT_ID,
      SETTLEMENT varchar(20) CHARACTER SET UTF8                           ,
      DICTDEF integer,
      DICTPOS integer)
  returns (
      REF integer,
      OPERDATE timestamp,
      DEBIT numeric(14,2),
      CREDIT numeric(14,2),
      CURRDEBIT numeric(14,2),
      CURRCREDIT numeric(14,2),
      RATE numeric(12,4))
   as
declare variable amount numeric(14,2);
  declare variable currsamount numeric(14,2);
  declare variable samount numeric(14,2);
  declare variable amountpln numeric(14,2);
begin
  select sum(winien - ma), sum(winienzl - mazl)
    from rozrachp
    where kontofk = :account and symbfak = :settlement and slodef = :dictdef
      and slopoz = :dictpos
    into :amount, :amountpln;

  currdebit = 0;
  currcredit = 0;
  debit = 0;
  credit = 0;


  for
    select ref, winien - ma, winienzl - mazl, stransdate, kurs
      from rozrachp
      where kontofk = :account and symbfak = :settlement and slodef = :dictdef
        and slopoz = :dictpos
        and winien - ma <> 0 -- w ten sposob omijam zapisy dotyczące różnic kursowych
      order by stransdate desc
      into :ref, :currsamount, :samount, :operdate, :rate
  do begin
    if (amount = 0 and amountpln = 0) then
      exit;
    if (amount * currsamount > 0 or amountpln * samount > 0) then
    begin
      if ((currsamount > amount and currsamount > 0)
          or (currsamount < amount and currsamount < 0))
      then
         currsamount = amount;
      amount = amount - currsamount;

      if ((samount > amountpln and samount > 0)
          or (samount < amountpln and samount < 0))
      then
        samount = amountpln;
      amountpln = amountpln - samount;

      if (currsamount > 0) then
      begin
        currdebit = currsamount;
        debit = samount;
      end else
      begin
        currcredit = -currsamount;
        credit = -samount;
      end
      suspend;
    end
  end
end^
SET TERM ; ^
