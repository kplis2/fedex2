--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AVALIBLESLOPOZFROMPATTERN(
      PATTERN SLO_ID,
      DEST SLO_ID)
  returns (
      BREF SLOPOZ_ID,
      BNAME STRING255,
      BSYMBOL ANALYTIC_ID)
   as
begin
  for
    select b.ref, b.nazwa, b.kontoks
      from slopoz b
      where slownik = :pattern
        and b.ref not in (select pattern_ref
                            from slopoz
                            where slownik = :dest
                              and pattern_ref is not null)
    into :bref, :bname, :bsymbol
  do begin
    suspend;
  end
end^
SET TERM ; ^
