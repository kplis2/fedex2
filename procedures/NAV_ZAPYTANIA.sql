--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAV_ZAPYTANIA(
      QUERYTYPE varchar(255) CHARACTER SET UTF8                           )
  returns (
      REF varchar(255) CHARACTER SET UTF8                           ,
      PARENT varchar(255) CHARACTER SET UTF8                           ,
      NUMBER integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      HINT varchar(255) CHARACTER SET UTF8                           ,
      KINDSTR varchar(20) CHARACTER SET UTF8                           ,
      ACTIONID varchar(60) CHARACTER SET UTF8                           ,
      APPLICATIONS varchar(60) CHARACTER SET UTF8                           ,
      FORMODULES varchar(60) CHARACTER SET UTF8                           ,
      RIGHTS varchar(255) CHARACTER SET UTF8                           ,
      RIGHTSGROUP varchar(255) CHARACTER SET UTF8                           ,
      ISNAVBAR smallint,
      ISTOOLBAR smallint,
      ISWINDOW smallint,
      ISMAINMENU smallint,
      ITEMTYPE smallint,
      GROUPPROCEDURE varchar(60) CHARACTER SET UTF8                           ,
      CHILDRENCOUNT smallint,
      RIBBONTAB varchar(40) CHARACTER SET UTF8                           ,
      RIBBONGROUP varchar(40) CHARACTER SET UTF8                           ,
      NATIVETABLE varchar(40) CHARACTER SET UTF8                           ,
      POPUPMENU varchar(40) CHARACTER SET UTF8                           ,
      NATIVEFIELD varchar(40) CHARACTER SET UTF8                           ,
      FORADMIN smallint)
   as
begin
  parent = NULL;
  hint = '';
  actionid = 'ExecuteQueryFromGlobal';
  applications = '';
  formodules = '';
  kindstr = 'MI_MYSLNIK';
  isnavbar = 0;
  istoolbar = 1;
  iswindow = 0;
  ismainmenu = 1;
  itemtype = 0;
  childrencount = 0;
  groupprocedure = '';
  ribbontab = '';
  ribbongroup = '';
  nativetable = 'ZAPYTANIA';
  popupmenu = 'MenuZAPYTANIA';
  foradmin = 0;
  number = 1;
  for select REF, NAZWA, RIGHTS, RIGHTSGROUP
  from ZAPYTANIA where (TYP=:querytype or cast(QUERYTYPE as varchar(255))=:querytype) and GRUPA=0
  order by NAZWA
  into :REF, :NAME, :RIGHTS, :RIGHTSGROUP
  do begin
    suspend;
    number = :number + 1;
  end
end^
SET TERM ; ^
