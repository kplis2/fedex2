--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_ANAL_PRORDS_MAT_STATUS
  returns (
      REF integer,
      PARENT integer,
      NREF integer,
      ORDSYMB varchar(40) CHARACTER SET UTF8                           ,
      PREF integer,
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      GOODDICT varchar(255) CHARACTER SET UTF8                           ,
      VERS integer,
      QUANTITY numeric(14,4),
      WH varchar(3) CHARACTER SET UTF8                           ,
      ONDATE date,
      STATUS smallint,
      MATREF integer,
      COLINDEX smallint)
   as
begin
  ref = 0;
  for
    select p.prefprod
      from prordanalmat p
      where p.connectionid = current_connection
      group by p.prefprod
      into pref
  do begin
    ref = ref + 1;
    parent = null;
    matref = null;
    status = 10;
    colindex = 0;
    select n.id, n.ref, p.ilosc - p.ilzreal, p.ktm, p.wersjaref, t.nazwa, p.termdost, p.magazyn
      from pozzam p
        left join nagzam n on (n.ref = p.zamowienie)
        left join towary t on (t.ktm = p.ktm)
      where p.ref = :pref
      into ordsymb, nref, quantity, good, vers, gooddict, ondate, wh;
    if (quantity > 0) then
    begin
      select max(p.status)
        from prordanalmat p where p.prefprod = :pref and p.connectionid = current_connection
        into status;
      if (status = 1) then colindex = 1;
      else if (status = 10) then colindex = 2;
      suspend;
      ordsymb = null;
      parent = ref;
      for
        select p.pref, p.wh, p.good, p.vers, p.quantity, p.maxavdate, p.status, t.nazwa
          from prordanalmat p
            left join towary t on (t.ktm = p.good)
          where p.prefprod = :pref and p.connectionid = current_connection
          order by p.status
          into matref, wh, good, vers, quantity, ondate, status, gooddict
      do begin
        colindex = 0;
        if (status = 0 and ondate is null) then ondate = current_date;
        ref = ref + 1;
        if (status = 1) then colindex = 1;
        else if (status = 10) then colindex = 2;
        suspend;
      end
    end
  end
end^
SET TERM ; ^
