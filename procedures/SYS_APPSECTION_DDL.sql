--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_APPSECTION_DDL(
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      CREATE_OR_ALTER smallint)
  returns (
      DDL blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           )
   as
declare variable EOL varchar(2);
declare variable IDENT varchar(50);
declare variable VAL varchar(2048);
declare variable PREFIX varchar(32);
declare variable NAMESPACE varchar(100);
begin
  eol = '
';
  ddl = '['||:nazwa||']'||:eol;
  for
    select trim(ident), trim(val), trim(prefix), trim(namespace) from s_appini where section = :nazwa and (prefix is null or upper(prefix) not starting with 'U')
    order by 1, 3 into :ident, :val, :prefix, :namespace
  do begin
    ddl = ddl || iif(prefix is null or prefix = '', '', :prefix || '#');
    if(namespace is null or namespace = '') then
      ddl = ddl || 'NULL~';
    if(namespace <> 'SENTE') then
      ddl = ddl || :namespace||'~';
    --eskejpowanie kluczy i wartości, konieczne
    val = replace(val, '\', '\\');
    val = replace(val, '~', '\~');
    val = replace(val, '#', '\#');
    val = replace(val, '=', '\=');
    ident = replace(ident, '\', '\\');
    ident = replace(ident, '~', '\~');
    ident = replace(ident, '#', '\#');
    ident = replace(ident, '=', '\=');

    ddl = ddl || :ident||'='||:val||:eol;
  end
  suspend;
end^
SET TERM ; ^
