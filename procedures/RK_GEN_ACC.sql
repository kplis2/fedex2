--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RK_GEN_ACC(
      I_DOC integer,
      I_POZOPER integer)
  returns (
      ACCOUNT varchar(20) CHARACTER SET UTF8                           )
   as
declare variable i_dictdef integer;
declare variable i_dictpos integer;
declare variable s_klient varchar(20);
declare variable s_oddzial varchar(20);
declare variable s_account varchar(80);
declare variable i integer;
declare variable ksiegowane smallint;
begin
  /*Procedura jest aktualnie niewykorzystywana
  zostaa zastapiona przez XK_RK_GEN_ACC*/

  select slodef, slopoz from rkdoknag where ref=:i_doc
    into :i_dictdef, :i_dictpos;
  if (i_dictdef is not null) then begin
    execute procedure KODKS_FROM_DICTPOS(:i_dictdef, :i_dictpos)
      returning_values :s_klient;
  end
  select ksiegowane, konto from rkpozoper where ref=:i_pozoper
    into :ksiegowane, :s_account;

  if (ksiegowane =1) then
  begin
    if (s_oddzial is null) then
      select O.symbol
        from rkdoknag DN
        join rkrapkas RK on (DN.raport = RK.ref and DN.ref=:i_doc)
        join rkstnkas STN on (RK.stanowisko = STN.kod)
        join oddzialy O on (O.oddzial = STN.oddzial)
       into :s_oddzial;

    if (s_account is not null) then
    begin
      s_account = replace(s_account,  'KLIENT',  s_klient);
      s_account = replace(s_account,  'KONTRAHENT',  s_klient);
      s_account = replace(s_account,  'ODDZIAL',  s_oddzial);
    end
    if (s_account is null or s_account='') then
      s_account = '???';

    account = substring(s_account from 1 for 20);
  end
  suspend;
end^
SET TERM ; ^
