--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RP_ITEM_CONNECT(
      FROMITEM integer,
      TOITEM integer)
   as
DECLARE VARIABLE TREF INTEGER;
DECLARE VARIABLE TFIELD VARCHAR(20);
DECLARE VARIABLE SREF INTEGER;
begin
  for select REF_ID, FIELD
  from RP_ITEM
  where PARENT_ID = :TOITEM and FIELD <> '' and (TYP = 2 or TYP = 3) and (depends is null or depends = 0)
  into :tref, :tfield
  do begin
    sref = null;
    select REF_ID from RP_ITEM where PARENT_ID = :fromitem and (TYP = 2 or TYP = 3)  and FIELD = :tfield
    into :sref;
    if(:sref > 0) then begin
      update RP_ITEM set DEPENDS = :sref, REPKIND = 2 where REF_ID = :tref;
    end
  end
end^
SET TERM ; ^
