--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_FAKTURUJSRV(
      FAKTURA integer,
      STAN varchar(20) CHARACTER SET UTF8                           ,
      REJFAK varchar(3) CHARACTER SET UTF8                           ,
      TYPFAK varchar(3) CHARACTER SET UTF8                           ,
      OPERATOR integer,
      SRVREQUEST integer,
      DATA timestamp,
      COPYCEN smallint,
      STANZAKUPU varchar(20) CHARACTER SET UTF8                           ,
      DATAWYST timestamp)
  returns (
      NEWFAK integer)
   as
declare variable bn char(1);
declare variable typdok varchar(3);
declare variable dostawca integer;
declare variable klient integer;
declare variable isdostawca integer;
declare variable isklient integer;
declare variable zakup smallint;
declare variable sposzap integer;
declare variable termin integer;
declare variable datazap timestamp;
declare variable rabnag numeric(14,4);
declare variable rabat numeric(14,4);
declare variable rabattab numeric(14,4);
declare variable waluta varchar(3);
declare variable waluta2 varchar(3);
declare variable iswaluta varchar(3);
declare variable sprzedawca integer;
declare variable walpln varchar(3);
declare variable walutowe smallint;
declare variable cennik integer;
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable wersjaref integer;
declare variable cenacen numeric(14,4);
declare variable cenamag numeric(14,4);
declare variable cenasprz numeric(14,4);
declare variable walcen varchar(3);
declare variable jedn integer;
declare variable jedno integer;
declare variable jednm integer;
declare variable ilosc numeric(14,4);
declare variable iloscm numeric(14,4);
declare variable ilosco numeric(14,4);
declare variable iloscmagmin numeric(14,4);
declare variable iscena smallint;
declare variable walrabcen varchar(3);
declare variable magazyn varchar(3);
declare variable opk smallint;
declare variable dokopk smallint;
declare variable dokcensprz smallint;
declare variable kosztdost numeric(14,4);
declare variable sumzal numeric(14,2);
declare variable dokpoz integer;
declare variable lokres varchar(6);
declare variable rok integer;
declare variable miesiac integer;
declare variable dokbn char(1);
declare variable vat numeric(14,4);
declare variable kurs numeric(14,4);
declare variable iskurs numeric(14,4);
declare variable sad integer;
declare variable cenawal numeric(14,4);
declare variable foc integer;
declare variable przeliczo numeric(14,4);
declare variable przeliczc numeric(14,4);
declare variable dokmagkor integer;
declare variable isfaktura integer;
declare variable odbiorca varchar(255);
declare variable dulica varchar(255);
declare variable dmiasto varchar(255);
declare variable dkodp varchar(10);
declare variable trasa varchar(255);
declare variable reqdate timestamp;
declare variable tabkurs integer;
declare variable tabkursuzywaj smallint;
declare variable bankdomyslny varchar(64);
declare variable stanowisko varchar(20);
declare variable serial varchar(40);
declare variable pozfakref integer;
declare variable slodef integer;
declare variable contrchar smallint;
declare variable fakref integer;
declare variable faksymbol varchar(80);
declare variable srvsymbol varchar(80);
declare variable promocja integer;
begin
  select SAD, WALUTOWY from TYPFAK where SYMBOL=:typfak into :sad, :walutowe;
  if(:copycen is null) then copycen = 0;
  execute procedure GETCONFIG('WALPLN') returning_values :walpln;
  if(:walpln is null) then walpln = 'PLN';
  if(:stanzakupu is not null and :stanzakupu <>'') then begin
      stanowisko = :stanzakupu;
      zakup = 1;
      select NULL, S.EXEDICTPOS, NULL, S.EXENAME, NULL, NULL, NULL, S.REQDATE, S.EXEDICTDEF, C.CONTRCHARACTER, S.FAKTURAZAK, S.SYMBOL
      from SRVREQUESTS S left join CONTRTYPES C on (S.contrtype = C.ref)
      where S.ref=:srvrequest
      into :klient, :dostawca, :magazyn, :odbiorca, :dulica, :dmiasto, :dkodp, :reqdate, :slodef, :contrchar, :fakref, :srvsymbol;
      if(:slodef <> 6) then exception slodef_bezdostawcy 'Zrodlo wykonawcy nie jest slownikiem dostawców.';
      if (fakref is not null and fakref<>0) then begin
        select symbol from nagfak where nagfak.ref = :fakref into :faksymbol;
        exception NAGFAK_IS_FAKT 'Do zlecenia '||srvsymbol||' wystawiono ju fakture '||faksymbol;
      end
  end else begin
      stanowisko = :stan;
      zakup = 0;
      select S.REPDICTPOS, NULL, NULL, S.REPNAME, S.RECSTREET, S.RECCITY, S.RECPOSTCODE, S.REQDATE, S.REPDICTDEF, C.CONTRCHARACTER, S.FAKTURA, S.SYMBOL
      from SRVREQUESTS S left join CONTRTYPES C on (S.contrtype = C.ref)
      where S.ref=:srvrequest
      into :klient, :dostawca, :magazyn, :odbiorca, :dulica, :dmiasto, :dkodp, :reqdate, :slodef, :contrchar, :fakref, :srvsymbol;
      if(:slodef <> 1) then exception slodef_bezdostawcy 'Zrodlo zleceniodawcy nie jest slownikiem klientów.';
      if (fakref is not null and fakref<>0) then begin
        select symbol from nagfak where nagfak.ref = :fakref into :faksymbol;
        exception NAGFAK_IS_FAKT 'Do zlecenia '||srvsymbol||' wystawiono ju fakture '||faksymbol;
      end
  end
  if(:faktura = 0 or (:faktura is null))then begin
    execute procedure GEN_REF('NAGFAK') returning_values :faktura;
    if(:klient > 0) then begin
      select KLIENCI.sposplat, klienci.termin, klienci.upust, klienci.waluta, klienci.sprzedawca from KLIENCI where REF=:klient
       into :sposzap, :termin, :rabnag,:waluta,:sprzedawca;
    end else if(:dostawca > 0) then begin
      select dostawcy.sposplat, dostawcy.dniplat, dostawcy.rabat, dostawcy.waluta from DOSTAWCY where REF=:dostawca
       into :sposzap, :termin, :rabnag,:waluta2;
    end else
      exception NAGFAK_DOKFROMMAG_BK;
    if(:rabnag is null) then rabnag = 0;
    if(:waluta is null or (:waluta = ''))then waluta = :walpln;
    --if(:waluta <> :walpln) then walutowe = 1;
    if(:sposzap is null or (:sposzap = 0) or not exists (select ref from platnosci where ref = :sposzap))then
      execute procedure GETCONFIG('SPOSZAP') returning_values :sposzap;
    if((:termin is null) or (:termin = 0)) then
      select PLATNOSCI.termin from PLATNOSCI where ref=:sposzap into :termin;
    if(:termin is null) then termin = 0;
    data = :datawyst;
    /*okreslenie okresu n apodstawie daty*/
    lokres = cast(extract(year from :datawyst) as varchar(4));
    miesiac = cast(extract(month from :datawyst) as integer);
    if(:miesiac < 10) then lokres = :lokres || '0';
    lokres = :lokres || cast(:miesiac as varchar(2));
    datazap = :data + :termin;
    if(:sad=1) then dostawca = NULL;
    insert into NAGFAK(REF,STANOWISKO,REJESTR,TYP,DATA, DATASPRZ, KLIENT,DOSTAWCA,SPOSZAP,TERMZAP,TERMIN,
              SUMWARTNET, SUMWARTBRU, PSUMWARTNET, PSUMWARTBRU,
              RABAT,AKCEPTACJA, /*REFK,*/ SYMBOLK, REFDOKM, ZAPLACONO,
              WALUTA, WALUTOWA, KURS,
              SPRZEDAWCA, OPERATOR, SKAD,
              ODBIORCA,DULICA,DMIASTO,DKODP,SRVREQUEST)
       values( :faktura,:stanowisko,:rejfak,:typfak,:datawyst, :datawyst,:KLIENT,:DOSTAWCA,:SPOSZAP,:datazap,:termin,
              0,0,0,0,
              :rabnag,0,/*NULL,*/'',NULL,0,
              :WALUTA, :WALUTOWE, 1,
              :SPRZEDAWCA,:operator, 2,
              :odbiorca,:dulica,:dmiasto,:dkodp,:srvrequest);
  end else begin
    /*sprawdzenie, czy klient lub dostawca sie zgadzają*/
    select ZAKUP, KLIENT, DOSTAWCA from NAGFAK where REF=:faktura into :zakup, :isklient, :isdostawca;

    if(:zakup = 1 and ((:dostawca <> :isdostawca) or (:dostawca is null) or (:isdostawca is null))) then
      exception NAGFAK_DOKFORMMAG_DWRONG;
    else if(:zakup = 0 and ((:klient <> :isklient) or (:klient is null) or (:isklient is null))) then
      exception NAGFAK_DOKFROMMAG_KWRONG;
    update NAGFAK set ODBIORCA=:odbiorca, DULICA=:dulica, DMIASTO=:dmiasto, DKODP=:dkodp
    where REF=:faktura;
  end
  if(:rabnag is null) then rabnag = 0;
  select bn,sad from NAGFAK where nagfak.ref = :faktura into :bn, :sad;
  if(:klient > 0) then
      select grupykli.cennik from klienci join grupykli on(klienci.grupa = grupykli.ref) where KLIENCI.REF=:klient
        into :cennik;
  cenacen = null;
  walcen = null;
  for select SRVPOSITIONS.REF,SRVPOSITIONS.KTM, 0,SRVPOSITIONS.AMOUNT, SRVPOSITIONS.PRICE, SRVPOSITIONS.NETSALEPRICE, SRVPOSITIONS.CURRENCY, SRVPOSITIONS.DESCRIPT, SRVPOSITIONS.serialnnew
  from SRVPOSITIONS
  join SRVDEFPOS on (SRVDEFPOS.REF=SRVPOSITIONS.SRVDEFPOS)
  where SRVPOSITIONS.SRVREQUEST =:srvrequest and SRVPOSITIONS.AMOUNT > 0
  and SRVDEFPOS.CREATEFAK=1 and ((:zakup = 1 and SRVPOSITIONS.purchase = 1) or
      (:ZAKUP = 0 and (srvpositions.purchase is null or srvpositions.purchase = 0)))
  order by srvpositions.ref
  into :dokpoz, :ktm, :wersja, :iloscm,:cenamag,:cenacen, :walcen, :trasa, :serial
  do begin
    /*przyjcie jednostki magazynowej - na razie*/
    select ref from TOWJEDN where KTM = :ktm and towjedn.glowna = 1 into :jednm;
    if(:copycen > 0 and :cenacen > 0) then begin
      /*kopiowanie ceny z dokumentu*/
      if(:bn = 'B') then begin
        select vat.stawka from VAT join TOWARY on (TOWARY.VAT = VAT.GRUPA) where TOWARY.ktm = :ktm into :vat;
        if(:vat is null) then vat = 0;
        cenacen = :cenacen * (100 + :vat)/100;
      end
      jedn = jednm;
      ilosc = iloscm;
    end else if(:klient > 0) then begin
      /*pobranie ceny z cennika dla danego klienta*/
      select ref, przelicz from TOWJEDN where KTM = :ktm and towjedn.c = 2 into :jedn, :przeliczc;
      if(:przeliczc is null) then przeliczc = 1;
      if(:jedn = :jednm) then
        ilosc = :iloscm;
      else
        ilosc = :iloscm / :przeliczc;
      if(:bn = 'B') then
        select  cenabru, waluta from cennik where cennik = :cennik and ktm = :ktm and wersja = :wersja and jedn = :jedn into :cenacen, :walcen;
      else
        select  cenanet, waluta from cennik where cennik = :cennik and ktm = :ktm and wersja = :wersja and jedn = :jedn into :cenacen, :walcen;
      execute procedure OBLICZ_RABAT(:ktm, :wersja, :klient,:iloscm,:cenacen,:bn, :magazyn, :cennik, 0, 0) returning_values :rabat,:iscena, :walrabcen, :promocja;
      if(:iscena = 1) then begin
        cenacen = :rabat;
        walcen = :walrabcen;
      end
      rabattab = :rabat;
    end else begin
      ilosc = :iloscm;
    end
    if(:cenacen is null) then cenacen = 0;
    if(:walcen is null or :walcen = '') then walcen = :walpln;
    if(:walcen<>:walpln) then begin
      tabkurs = 0;
      tabkursuzywaj =0;
      select tabkursuzywaj, tabkurs, bankdomyslny from stansprzed where stanowisko=:stan into tabkursuzywaj, tabkurs, bankdomyslny;
      if(:tabkursuzywaj = 0) then begin
        if(:tabkurs = 0 or (:tabkurs is null) ) then exception KURS_BRAKWARTOSCI 'Niezdefiniowana tabela kursów do przeliczeń.';
      end
      else if(:tabkursuzywaj = 1) then begin
        select max(ref) from tabkurs where bank = :bankdomyslny and data = :datawyst into :tabkurs;
        if(:tabkurs = 0 or :tabkurs is null) then exception KURS_BRAKWARTOSCI 'Niezdefiniowana tabela kursów dla banku: '||:bankdomyslny||' na date: '||cast(:reqdate as date);
      end
    end
    if(:ilosc > 0) then begin
       pozfakref = 0;
       execute procedure GEN_REF('POZFAK') returning_values :pozfakref;
       if(:contrchar is null or :contrchar <> 1) then trasa = '';
       if(:pozfakref is null or :POZFAKREF = 0) then exception BRAK_PDSTAWOWYCH_DANYCH;
       insert into pozfak(REF,DOKUMENT,KTM,WERSJA,ILOSC,CENACEN,RABAT,RABATTAB,WALCEN,
                       ILOSCM,JEDN,MAGAZYN,
                       PILOSC,PCENACEN,PRABAT,PCENANET,PCENABRU,CENAMAG,PCENAMAG,
                       PWARTNET,PWARTBRU,OPK,OPIS,TABKURSCEN)
       values(:POZFAKREF,:faktura,:ktm,:wersja,:ilosc,:cenacen,:rabat,:rabattab,:walcen,
              :ILOSCM,:jedn,:magazyn,
              0,0,0,0,0,:cenamag,:cenamag,
              0,0,0,:trasa,:tabkurs);
       update SRVPOSITIONS set REALIZED = 1, POZFAK = :pozfakref where REF=:dokpoz;
    end
    cenacen = null;
    walcen = null;
  end
  newfak = :faktura;
end^
SET TERM ; ^
