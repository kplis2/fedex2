--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGZAM_CHECK_CAN_REAL_PREDICT(
      DATETO timestamp)
  returns (
      POZZAM integer)
   as
declare variable ktm varchar(80);
declare variable magazyn varchar(3);
declare variable stan numeric(14,4);
declare variable licznik integer;
declare variable data timestamp;
declare variable standif numeric(14,4);
begin
  for select p.ktm, p.magazyn
    from nagzam n
    join pozzam p on (p.zamowienie = n.ref)
    left join typzam t on (n.typzam = t.symbol)
    where t.wydania > 0 and n.typ<>3 and (n.stan='B' or n.stan='R')
    group by p.magazyn, p.ktm
    into :ktm, :magazyn
  do begin
    data = current_date;
    select s.ilosc-s.zablokow from stanyil s where s.ktm = :ktm and s.magazyn = :magazyn into :stan;
    if (:stan is null) then stan = 0;
    select sum(ilplus) from stanyrez s where (s.status = 'R' or s.status = 'Z')and s.magazyn = :magazyn and s.ktm=:ktm and s.data < :data into :standif;
    if(:standif is not null) then stan = :stan - :standif;
    licznik = 0;
    while (data<=dateto and :licznik <= 1000) do begin
      select sum(ilplus) from stanyrez s where s.status = 'D' and s.zreal = 0
        and s.magazyn = :magazyn and s.ktm=:ktm and s.data = :data into :standif;
      if(:standif is null) then standif = 0;
      stan = :stan + standif;
      for select ilminus, pozzam from stanyrez s where (s.status = 'R' or s.status = 'Z') and s.zreal = 0
        and s.magazyn = :magazyn and s.ktm=:ktm and s.data = :data
        into :standif, :pozzam
      do begin
        if (:standif is null) then standif = 0;
        stan = :stan - :standif;
        if(:stan < 0) then begin
         stan = 0;
         suspend;
        end
      end
      licznik = :licznik +1;
      data = :data + 1;
    end
  end
end^
SET TERM ; ^
