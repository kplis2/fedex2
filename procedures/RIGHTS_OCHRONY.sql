--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RIGHTS_OCHRONY(
      OPERATOR integer = null,
      RODZAJ smallint = null)
  returns (
      OPER varchar(80) CHARACTER SET UTF8                           ,
      ZB_DANYCH varchar(255) CHARACTER SET UTF8                           ,
      EL_ZB varchar(255) CHARACTER SET UTF8                           ,
      RODZ smallint)
   as
declare variable el_zb_tab varchar(255);
declare variable tabela varchar(40);
declare variable ref_g integer;
declare variable window_g varchar(80);
declare variable control_g varchar(80);
declare variable rproc varchar(255);
begin

--MENU--
  for select distinct o.nazwa,  'Menu', s.val, g.protect
    from operator o
      left join opergrupy op on ((strmulticmp(';'||OP.symbol||';',o.grupa)>0))
      join s_groupprotect g on ((strmulticmp(o.grupa,g.groups)=0) and coalesce(g.groups,'')<>'')
      join s_appini s on (s.ident = g.control||':Label' and s.section = g.window)
    where g.window like 'Menu:%' and (g.protect = :rodzaj or coalesce(:rodzaj,0) = 0 )
    and (o.ref=:operator or coalesce(:operator,0) = 0)
    into :oper,:zb_danych, :el_zb, :rodz
  do begin
  --wszystkie ochrony
    if(coalesce(:rodzaj,0) = 0) then
    suspend;
  --w zaleznosci od rodzaju( 1 wyszarzone, 2 niewidoczne)
    else if(:rodz = :rodzaj) then
    suspend;
  end
--Tabele
  for select distinct o.nazwa, g.control ,replace(g.window, 'Table:', ''), g.protect
    from operator o
      left join opergrupy op on ((strmulticmp(';'||OP.symbol||';',o.grupa)>0))
      join s_groupprotect g on ((strmulticmp(o.grupa,g.groups)=0) and coalesce(g.groups,'')<>'')
    where g.window like 'Table:%' and (g.protect = :rodzaj or coalesce(:rodzaj,0) = 0 )
    and (o.ref=:operator or coalesce(:operator,0) = 0)
    into :oper, :el_zb_tab, :tabela, :rodz
  do begin
    zb_danych = 'Rekord w tabeli';
    el_zb = :tabela||'.'||:el_zb_tab;
    --wszystkie ochrony
    if(coalesce(:rodzaj,0) = 0) then
    suspend;
    --w zaleznosci od rodzaju( 1 wyszarzone, 2 niewidoczne)
    else if(:rodz = :rodzaj) then
    suspend;
  end
--Kontrolki
select mon$remote_process from mon$attachments where mon$attachment_id = current_connection
into :rproc;
if(:rproc containing 'esystem.exe') then
begin
    for select distinct o.nazwa, g.ref, substring(g.window from 8 for 100), replace(replace(g.control, substring(g.window from 8 for 100), ''),':',''), g.protect
      from operator o
        left join opergrupy op on ((strmulticmp(';'||OP.symbol||';',o.grupa)>0))
        join s_groupprotect g on ((strmulticmp(o.grupa,g.groups)=0) and coalesce(g.groups,'')<>'')
      where g.window like 'Window:%' and (g.protect = :rodzaj or coalesce(:rodzaj,0) = 0 )
      and (o.ref=:operator or coalesce(:operator,0) = 0)
      into :oper, :ref_g, :window_g, :control_g, :rodz
    do begin
      if (:window_g  not like 'esystem_%') then
      begin
        zb_danych = 'Kontrolki';
        el_zb = :window_g||':'||:control_g;
        --wszystkie ochrony
        if(coalesce(:rodzaj,0) = 0) then
        suspend;
        --w zaleznosci od rodzaju( 1 wyszarzone, 2 niewidoczne)
        else if(:rodz = :rodzaj) then
        suspend;
      end else
      begin
        for select distinct o.nazwa,  'Kontrolki', s.val, g.protect
          from operator o
            left join opergrupy op on ((strmulticmp(';'||OP.symbol||';',o.grupa)>0))
            join s_groupprotect g on ((strmulticmp(o.grupa,g.groups)=0) and coalesce(g.groups,'')<>'')
            join s_appini s on ( s.ident = :control_g||':Label')
          where g.ref=:ref_g  and substring(s.section from 10 for 100)=:window_g
            and (g.protect = :rodzaj or coalesce(:rodzaj,0) = 0)
            and (o.ref=:operator or coalesce(:operator,0) = 0)
          into :oper,:zb_danych , :el_zb, :rodz
          do begin
            el_zb = :window_g||':'||:el_zb;
            --wszystkie ochrony
            if(coalesce(:rodzaj,0) = 0) then
            suspend;
            --w zaleznosci od rodzaju( 1 wyszarzone, 2 niewidoczne)
            else if(:rodz = :rodzaj) then
            suspend;
          end
      end
    end
  end
end^
SET TERM ; ^
