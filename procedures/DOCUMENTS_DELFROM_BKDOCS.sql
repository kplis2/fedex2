--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOCUMENTS_DELFROM_BKDOCS(
      TYP smallint,
      DOCUMENT_REF integer)
   as
declare variable otable varchar(10);
begin
  if(:typ = 0) then
    otable = 'NAGFAK';
  else if(:typ = 1) then
    otable = 'DOKUMNAG';
  else if(:typ = 2) then
    otable = 'RKRAPKAS';
  else if(:typ = 3) then
    otable = 'DOKUMNOT';
  update BKDOCS set BKDOCS.status = 0 where status > 0 and otable = :otable and oref = :document_ref;
  delete from BKDOCS where otable = :otable and oref = :document_ref;
end^
SET TERM ; ^
