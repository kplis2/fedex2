--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDECLARATION_ADD_HEADER(
      EDECLDEF EDECLDEFS_ID,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      ENCLOSUREOWNER integer = null)
  returns (
      REF integer)
   as
declare variable regoperator integer;
declare variable encloscount integer;
declare variable edeclsymbol varchar(10);
declare variable emonth varchar(2);
declare variable eyear varchar(4);
declare variable fromdate date_id;
declare variable todate date_id;
declare variable sqlgenerated smallint_id;
begin
  select symbol, sqlgenerated from edecldefs
    where ref = :edecldef
    into :edeclsymbol, :sqlgenerated;

  if (edeclsymbol is null) then
    exception universal 'Brak definicji dla danej deklaracji';

  select pvalue from get_global_param('AKTUOPERATOR')
    into :regoperator;

  if (coalesce(char_length(:period),0) = 6) then -- [DG] XXX ZG119346
  begin
    eyear = substring(:period from  1 for  4);
    emonth = substring(:period from  5 for  6);
  end

  execute procedure gen_ref('EDECLARATIONS')
    returning_values :ref;

  if (enclosureowner > 0) then
  begin
  /*Parametr ENCLOSUREOWNER wskazuje na e-deklaracje, do ktorej tworzymy
    zalacznik o typie EDECLDEF. Pilnujemy, aby nie rejestrowano wiecej
    niz jeden zalacznik danego typu, po czym tworzymy naglowek zalacznika
    na podstawie naglowka glownej deklaracji (BS52201)*/

    select count(*) from edeclarations e
      join edecldefs d on (d.ref = e.edecldef)
      where e.enclosureowner = :enclosureowner
      into :encloscount;

    if (encloscount >= 1) then
      exception universal 'Można dołączyć tylko jeden załącznik typu: ' || edeclsymbol;

    insert into edeclarations (ref, regoperator, state, edecldef, period, person,
           fromdate, todate, enclosureowner, company, eyear)
       select :ref, :regoperator, 0, :edecldef, period, person,
           fromdate, todate, :enclosureowner, company, eyear
         from edeclarations
         where ref = :enclosureowner;
  end else begin
    execute procedure period2dates(period)
      returning_values :fromdate, :todate;  -- BS100979

    insert into edeclarations (ref, regoperator, state, edecldef, period,
      emonth, eyear, fromdate, todate, sqlgenerated)
    values (:ref, :regoperator, 0, :edecldef, :period,
      :emonth, :eyear, :fromdate, :todate, :sqlgenerated);
  end
  suspend;
end^
SET TERM ; ^
