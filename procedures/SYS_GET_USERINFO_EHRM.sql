--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_GET_USERINFO_EHRM(
      USR varchar(40) CHARACTER SET UTF8                           ,
      PSW varchar(40) CHARACTER SET UTF8                           )
  returns (
      REF integer,
      USERID varchar(255) CHARACTER SET UTF8                           ,
      USERLOGIN varchar(255) CHARACTER SET UTF8                           ,
      IMIE varchar(255) CHARACTER SET UTF8                           ,
      NAZWISKO varchar(255) CHARACTER SET UTF8                           ,
      SUPERVISOR smallint,
      SUPERVISEGROUP varchar(255) CHARACTER SET UTF8                           ,
      EMAIL varchar(255) CHARACTER SET UTF8                           ,
      GRUPA varchar(1024) CHARACTER SET UTF8                           ,
      SENTE smallint,
      MUST_CHANGE smallint,
      DAYS_TO_CHANGE integer,
      LAST_CHANGE timestamp,
      NAMESPACE varchar(40) CHARACTER SET UTF8                           )
   as
declare variable CRYPTEDPSW varchar(255);
declare variable LAYER integer;
begin
  cryptedpsw = shash_hmac('hex','sha1',:psw,:usr);

  -- odczytaj dane usera pod warunkiem zgodnosci loginu i hasla
  if(exists(select first 1 1 from s_users u where u.userlogin = :usr and u.userpass = :cryptedpsw)) then begin
    -- odczytaj dane usera ktory autoryzuje wywolanie
    select first 1 p.ref, u.userid, u.userlogin, p.fname, p.sname, u.supervisor, u.supervisegroup,
        u.email, u.grupa, u.sente, u.must_change, u.days_to_change, u.last_change
      from s_users u
        join persons p on (u.userid = p.userid)
      where u.userlogin = :usr and u.userpass = :cryptedpsw
        and p.ehrm = 1
    into :ref, :userid, :userlogin, :imie, :nazwisko, :supervisor, :supervisegroup,
      :email, :grupa, :sente, :must_change, :days_to_change, :last_change;

    -- ustal biezacy namespace
    if(:supervisor=2) then namespace = 'SENTE';
    else if(:supervisor=1) then namespace = 'ADMIN';
    else namespace = 'USER_'||:userlogin;
    if(not exists(select first 1 1 from s_namespaces where namespace=:namespace)) then begin
      -- jesli nie ma wpisu w s_namespaces to go dodaj
      if(:namespace='SENTE') then layer = 0;
      else if(:namespace='ADMIN') then layer = 1000;
      else layer = 1001;
      insert into s_namespaces(namespace,layer) values (:namespace, :layer);
    end
    suspend;
  end
end^
SET TERM ; ^
