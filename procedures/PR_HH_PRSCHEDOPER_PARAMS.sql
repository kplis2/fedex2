--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_HH_PRSCHEDOPER_PARAMS(
      PRSCHEDOPER integer)
  returns (
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      PRMACHINE varchar(40) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4),
      QUANTITYR numeric(14,4),
      QUANTITYC numeric(14,4),
      MSYMBOL varchar(80) CHARACTER SET UTF8                           ,
      GSYMBOL varchar(80) CHARACTER SET UTF8                           ,
      MAKETIMEFROM varchar(40) CHARACTER SET UTF8                           ,
      MAKETIMETO varchar(40) CHARACTER SET UTF8                           ,
      MSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable maketimefromt timestamp;
declare variable maketimetot timestamp;
begin
  select m.symbol, g.symbol, o.amountin, o.amountresult, o.amountin - o.amountresult - o.amountshortages,
      g.ktm, w.ref, o.machine, o.starttime, o.endtime
    from prschedopers o
      left join prschedguides g on (g.ref = o.guide)
      left join prmachines m on (m.ref = o.machine)
      left join wersje w on (w.ktm = g.ktm and w.nrwersji = g.wersja)
    where o.ref = :prschedoper and o.status = 2 and o.amountin - o.amountresult - o.amountshortages > 0
    into msymbol, gsymbol, quantity, quantityr, quantityc,
      good, vers, prmachine, maketimefromt, maketimetot;
  if (maketimefromt is null) then
    maketimefrom = datetostr(current_timestamp(0),'%Y-%m-%d %H:%M:%S');
  else
    maketimefrom = datetostr(maketimefromt,'%Y-%m-%d %H:%M:%S');
  if (maketimetot is null) then
    maketimeto = datetostr(current_timestamp(0),'%Y-%m-%d %H:%M:%S');
  else
    maketimeto = datetostr(maketimetot,'%Y-%m-%d %H:%M:%S');
end^
SET TERM ; ^
