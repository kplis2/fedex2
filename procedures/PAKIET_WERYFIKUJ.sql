--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PAKIET_WERYFIKUJ(
      DOKUMENT integer,
      DOKUMENTTYP char(10) CHARACTER SET UTF8                           ,
      DOKUMENTPOZ integer,
      DOKUMENTPOZNEWILOSC numeric(14,4),
      AFTEREDIT smallint)
   as
declare variable promocja integer;
declare variable sourcepoz integer;
declare variable iloscwymagana numeric(14,4);
declare variable iloscdostepna numeric(14,4);
declare variable iloscnadmiar numeric(14,4);
declare variable ilosc numeric(14,4);
declare variable przelicznik numeric(14,4);
declare variable numer integer;
declare variable ktm varchar(40);
declare variable mustcheck integer;
declare variable flagi varchar(60);
declare variable flagaomin varchar(10);
begin
   mustcheck = 0;
   if(:afteredit = 1) then begin
     if(:dokumenttyp = 'M'
       and exists(select P.REF from DOKUMPOZ P where P.dokument = :dokument
                       and (p.refdopakiet = :dokumentpoz
                          or (p.ref = :dokumentpoz and p.frompromoref > 0)))
     ) then
       mustcheck = 1;
     if(:dokumenttyp = 'F'
       and exists(select P.REF from POZFAK P where P.dokument = :dokument
                       and (p.refdopakiet = :dokumentpoz
                          or (p.ref = :dokumentpoz and p.frompromoref > 0)))
     ) then
       mustcheck = 1;

   end else
     mustcheck = 1;--caly dokument
   if(:mustcheck = 0) then
     exit;
   execute procedure get_config('PAKIETYWERYFIKUJOMIN',0) returning_values :flagaomin;
   flagaomin = '%;'||:flagaomin||';%';
   --sprawdzanie na dok. magazynowych
   if(:dokumenttyp = 'M') then begin
     for select  p.refdopakiet, p.frompromoref, p.ilosczadopakiet, p.ilosc , p.ktm,  p.numer, p.flagi
     from DOKUMPOZ P
        join PROMOCJE PR on (pr.ref = p.frompromoref)
     where p.dokument = :dokument
       and p.refdopakiet > 0
       and (:dokumentpoz = 0 or (p.ref = :dokumentpoz or p.refdopakiet = :dokumentpoz ))
     into :sourcepoz, :promocja, :przelicznik, :ilosc, :ktm, :numer, :flagi
     do begin
       --jest pozycja promocyjna - sprawdzenie, czy suma pozycji promocyjnych z tej samej promocji do tej samej pozycji promowanej nie jest za duza
       -- obliczanie, jaka jest ilosc wymagana zrodla, by starczylo na wielkosc promocji
       if(:flagi is null) then
         flagi = '';
       select sum(case when :afteredit = 1 and p.ref=:dokumentpoz then :dokumentpoznewilosc else  p.ilosc end * p.ilosczadopakiet)
         from DOKUMPOZ P
         where p.dokument = :dokument and p.refdopakiet = :sourcepoz  and p.frompromoref = :promocja
       into :iloscwymagana;
       select case when :afteredit = 1 and p.ref=:dokumentpoz then 0 else p.ilosc end
         from DOKUMPOZ P where ref=:sourcepoz
         into :iloscdostepna;
       if(:iloscdostepna is NULL) then
         iloscdostepna = 0;
       if(:afteredit = 1 and :sourcepoz = :dokumentpoz) then
         iloscdostepna = :iloscdostepna + :dokumentpoznewilosc;
       if(:iloscwymagana > :iloscdostepna
         and (:afteredit = 1 or (:flagi not like :flagaomin))
       ) then  begin
         iloscnadmiar = (coalesce(iloscwymagana,0) - coalesce(iloscdostepna,0))/:przelicznik;
         exception UNIVERSAL  'Na poz.'||:numer||' jest o '||iloscnadmiar||' za duzo gratisu.';
       end
     end
   end
   --sprawdzanie na fakturach
   if(:dokumenttyp = 'F') then begin
     for select  p.refdopakiet, p.frompromoref, p.ilosczadopakiet, p.ilosc , p.ktm,  p.numer, p.flagi
     from POZFAK P
        join PROMOCJE PR on (pr.ref = p.frompromoref)
     where p.dokument = :dokument
       and p.refdopakiet > 0
       and (:dokumentpoz = 0 or p.ref = :dokumentpoz or p.refdopakiet = :dokumentpoz )
     into :sourcepoz, :promocja, :przelicznik, :ilosc, :ktm, :numer, :flagi
     do begin
       --jest pozycja promocyjna - sprawdzenie, czy suma pozycji promocyjnych z tej samej promocji do tej samej pozycji promowanej nie jest za duza
       -- obliczanie, jaka jest ilosc wymagana zrodla, by starczylo na wielkosc promocji
       if(:flagi is null) then
         flagi = '';
       select sum(case when :afteredit = 1 and p.ref=:dokumentpoz then :dokumentpoznewilosc else  p.ilosc end * p.ilosczadopakiet)
         from POZFAK P
         where p.dokument = :dokument and p.refdopakiet = :sourcepoz  and p.frompromoref = :promocja
       into :iloscwymagana;
       select case when :afteredit = 1 and p.ref=:dokumentpoz then 0 else p.ilosc end
         from POZFAK P where ref=:sourcepoz
         into :iloscdostepna;
       if(:iloscdostepna is NULL) then
         iloscdostepna = 0;
       if(:afteredit = 1 and :sourcepoz = :dokumentpoz) then
         iloscdostepna = :iloscdostepna + :dokumentpoznewilosc;
       if(:iloscwymagana > coalesce(:iloscdostepna,0)
         and (:afteredit = 1 or (:flagi not like :flagaomin))
       ) then  begin
         iloscnadmiar = (coalesce(iloscwymagana,0) - coalesce(iloscdostepna,0))/:przelicznik;
         exception UNIVERSAL  'Na poz.'||:numer||' jest o '||iloscnadmiar||' za duzo gratisu.';
       end
     end
   end
end^
SET TERM ; ^
