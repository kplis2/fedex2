--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_CHECK_TOWJEDN(
      GOOD KTM_ID)
  returns (
      STATUS smallint,
      PRZELICZNIK NUMERIC_14_4,
      REFJEDN integer)
   as
declare variable cnt integer;
begin
  -- XXX KBI ASysta 20170124
  -- Procedura sprawdza ile jednostek ma dany KTM.
  -- Jeli wicej niż jedną to np podczas przyjcia i wyborze towaru z listy wywietli sie okno to do wyboru opakowania
  -- jesli jedna to od razu zwróci przelicznik

  status = 0;

  select count(t.ktm) from towjedn t where t.ktm = :good group by t.ktm into cnt;

  if (cnt is null) then exception universal 'Nie znaleziono jednostki dla KTM '||:good;

  if (cnt = 1) then begin
    status = 1;
    select t.ref, t.przelicz from towjedn t where t.ktm = :good into :refjedn, :przelicznik;
  end else
    status = 2;

end^
SET TERM ; ^
