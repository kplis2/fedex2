--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PMELEMENTS_CALC(
      REF integer)
   as
declare variable sumapp_m numeric(14,2); -- przychodowe planowane
declare variable sumapr_m numeric(14,2); -- przychodowe rzeczywiste
declare variable sumakp_m numeric(14,2); -- kosztowe planowane
declare variable sumakr_m numeric(14,2);  -- kosztowe rzeczywiste
declare variable sumapp_u numeric(14,2);
declare variable sumapr_u numeric(14,2);
declare variable sumakp_u numeric(14,2);
declare variable sumakr_u numeric(14,2);
declare variable sumab_r numeric(14,2);
declare variable sumapp_s numeric(14,2);
declare variable sumapr_s numeric(14,2);
declare variable sumakp_s numeric(14,2);
declare variable sumakr_s numeric(14,2);
declare variable sumak_r numeric(14,2);
declare variable ilosc_r numeric(14,4);
declare variable iloscr_r numeric(14,4);
declare variable sumakr_r numeric(14,2);
declare variable ilosc_s numeric(14,4);
declare variable iloscr_s numeric(14,4);
declare variable pmplan integer;
begin
-- sumowanie cen kosztorysowych i budżetowych materiaów z pozycji elementu
 select sum(p.calcval), sum(p.erealval), sum(p.budval), sum(p.usedval) from pmpositions p
   where p.pmelement = :ref and p.positiontype='M' and ref=mainref
   into :sumapp_m, :sumapr_m ,:sumakp_m, :sumakr_m;

-- sumowanie cen kosztorysowych, budżetowych i rzeczywistych uslug z pozycji elementu
 select sum(p.calcval), sum(p.erealval), sum(p.budval), sum(p.usedval) from pmpositions p
   where p.pmelement = :ref and p.positiontype='U' and ref=mainref
   into :sumapp_u, :sumapr_u, :sumakp_u, :sumakr_u;

-- sumowanie ilosci oraz cen kosztorysowych i budżetowych roboczogodzin robocizny z pozycji elementu
 select sum(p.quantity), sum(p.usedq), sum(p.calcval), sum(p.budval), sum(p.usedval) from pmpositions p
   where p.pmelement = :ref and p.positiontype='R' and ref=mainref
   into :ilosc_r, :iloscr_r, :sumak_r, :sumab_r, :sumakr_r;

-- sumowanie ilosci oraz cen kosztorysowych i budżetowych motogodzin sprzetu z pozycji elementu
 select sum(p.quantity), sum(p.usedq), sum(p.calcval), sum(p.erealval), sum(p.budval), sum(p.usedval) from pmpositions p
   where p.pmelement = :ref and p.positiontype='S' and ref=mainref
   into :ilosc_s, :iloscr_s, :sumapp_s, :sumapr_s, :sumakp_s, :sumakr_s;

 update pmelements e set
                   e.calcmval = :sumapp_m,
                   --e.erealmval= :sumapr_m,   --sprzedaż jest na elementach a nie pozycjach
                   e.budmval  = :sumakp_m,
                   e.realmval = :sumakr_m,
                   e.calcsval = :sumapp_u,
                   --e.erealsval= :sumapr_u,
                   e.budsval  = :sumakp_u,
                   e.realsval = :sumakr_u,
                   e.calcw    = :ilosc_r,
                   e.calcwval = :sumak_r,
                   e.budw     = :ilosc_r,
                   e.budwval  = :sumab_r,
                   e.realw    = :iloscr_r,
                   e.realwval = :sumakr_r,
                   e.calce    = :ilosc_s,
                   e.calceval = :sumapp_s,
                   --e.erealeval= :sumapr_s,
                   e.bude     = :ilosc_s,
                   e.budeval  = :sumakp_s,
                   e.reale    = :iloscr_s,
                   e.realeval = :sumakr_s
   where e.ref = :ref;
  select pmplan from pmelements e where e.ref = :ref into :pmplan;
  execute procedure pmplans_calc(:pmplan);
end^
SET TERM ; ^
