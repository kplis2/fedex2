--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_SESJE_ERROR(
      REF SESJE_ID,
      ERROR INTEGER_ID,
      ERRORMSG STRING255)
   as
begin
  update int_sesje s
    set s.error = :error, errormsg = :errormsg
    where s.ref = :ref;
end^
SET TERM ; ^
