--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_PRCOSTS(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(16,2))
   as
declare variable COSTTYPE smallint;
declare variable PRCOST numeric(14,2);
declare variable BRUTTO numeric(14,2);
declare variable LUMPSUMTAX smallint;
declare variable TPER varchar(6);
declare variable PAYDAY date;
declare variable PERSON integer;
declare variable ZUS numeric(14,2);
declare variable PREVCOST numeric(14,2);
declare variable PROG numeric(14,2);
declare variable AUTDATE date;
declare variable KUP numeric(14,2);
begin

  select r.tper, r.payday, r.lumpsumtax, c.prcosts,t.prcoststype
    from epayrolls r
      join emplcontracts c on (c.ref = r.emplcontract)
      join econtrtypes t on (t.contrtype = c.econtrtype)
    where r.ref = :payroll
    into :tper, :payday, :lumpsumtax, :prcost, :costtype;

  if (lumpsumtax = 1) then
  begin
    ret = 0;
    exit;
  end else
  if (costtype = 1) then
  begin
    execute procedure ef_pval(employee, payroll, 'EF_', 'KOSZTYNORMALNE')
      returning_values ret;
    exit;
  end else
  begin

    select person from employees
      where ref = :employee
      into :person;

    select sum(case when p.ecolumn = 3000 then p.pvalue else 0 end)
         , sum(case when p.ecolumn = 6500 and r.corlumpsumtax = :payroll then p.pvalue else 0 end)
         , sum(case when p.ecolumn not in (3000,6500) then p.pvalue else 0 end)
      from eprpos p
        join epayrolls r on (p.payroll = r.ref)
        join eprempl e on (e.epayroll = p.payroll and e.employee = p.employee)
      where e.person = :person and r.tper = :tper
        and p.ecolumn in (3000,6100,6110,6120,6130,6500)
        and (r.ref = :payroll or r.corlumpsumtax = :payroll)
      into :brutto, :prevcost, :zus;

    if (prcost = 50 and brutto is not null) then --PR50483
    begin
      select first 1 autdate from empltaxinfo
        where employee = :employee
          and extract(year from autdate) = extract(year from :payday)
        order by fromdate desc
        into :autdate;
  
      if (payday > autdate and extract(month from payday) <> extract(month from autdate)) then
        prcost = 20;
      else begin
        select min(amount) from etaxlevels
          where taxyear = extract(year from :payday) and amount > 0
          into :prog;
    
        select sum(p.pvalue)
          from eprpos p
            join epayrolls r on (p.payroll = r.ref and r.empltype = 2)
            join eprempl m on (m.epayroll = p.payroll and m.employee = p.employee)
            join emplcontracts e on (r.emplcontract = e.ref and e.empltype = 2 and e.prcosts = 50)
          where m.person = :person
            and p.payroll <> :payroll
            and p.ecolumn = 6500
            and (r.payday < :payday or (r.payday = :payday and r.ref < :payroll))
            and r.tper starting with extract(year from :payday)
          into :kup;
  
        if (prog/2.00 < kup) then
          prcost = 20;
      end
    end

    ret = coalesce(prcost * (brutto - zus), 0) / 100 - coalesce(prevcost,0);
  end

  suspend;
end^
SET TERM ; ^
