--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_HASHTOWAR
  returns (
      NAZWA varchar(255) CHARACTER SET UTF8                           )
   as
declare variable zenski integer;
declare variable r integer;
declare variable s varchar(255);
begin
  -- poczatek nazwy
  zenski = cast(rand() as integer);
  select max(number)-1 from s_hashdata where datatype=4+:zenski into :r;
  select first 1 val from s_hashdata where datatype=4+:zenski and number>=cast(rand()*:r as integer)+1 into :nazwa;

  -- dopelnienie
  if(rand()>0.3) then begin
    select max(number)-1 from s_hashdata where datatype=6 into :r;
    select first 1 val from s_hashdata where datatype=6 and number>=cast(rand()*:r as integer)+1 into :s;
    nazwa = :nazwa || ' ' || :s;
  end

  -- przymiotnik
  if(rand()>0.3) then begin
    select max(number)-1 from s_hashdata where datatype=7+:zenski into :r;
    select first 1 val from s_hashdata where datatype=7+:zenski and number>=cast(rand()*:r as integer)+1 into :s;
    nazwa = :nazwa || ' ' || :s;
  end

  suspend;
end^
SET TERM ; ^
