--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_WYDRUK_ADR(
      LISTYWYSREF INTEGER_ID,
      NAZWAPARAMGRUPA STRING20,
      NAZWAPARAMKLASA STRING20,
      NAZWAPARAMNALEPKA STRING20,
      NAZWAPARAMOPIS STRING20,
      NAZWAPARAMSTAN STRING20)
  returns (
      LISTYWYSDREF INTEGER_ID,
      ADRTYP CHAR_1,
      ADRES STRING255,
      KODPOCZTOWY STRING10,
      MIASTO STRING255,
      ILOSC NUMERIC_14_4,
      UWAGISPED STRING1024,
      FROMPLACE STRING20,
      TOPLACE STRING20,
      KTM KTM_ID,
      JMIARY JEDN_MIARY,
      NAZWA STRING255,
      WERSJAREF INTEGER_ID,
      ADRGRUPA STRING255,
      ADRKLASA STRING255,
      ADRNALEPKA STRING255,
      ADROPIS STRING255,
      ADRSTAN STRING255,
      CECHA STRING20,
      WARTOSC STRING255,
      WAGA NUMERIC_14_4,
      SUMAWAGIADR NUMERIC_14_4,
      NRDOMU NRDOMU_ID,
      NRLOKALU NRDOMU_ID,
      BOX STRING20)
   as
begin
  SUMAWAGIADR = 0;
  box = '';
  for select LISTYWYSD.REF, trim(LISTYWYSD.adrtyp),LISTYWYSD.adres,
    LISTYWYSD.kodp,LISTYWYSD.miasto, LISTYWYSDPOZ.ilosc,
    LISTYWYSD.UWAGISPED,LISTYWYSD.fromplace,LISTYWYSD.toplace,
    wersje.ktm,wersje.miara,towary.nazwa,listywysdpoz.wersjaref,
    listywysdpoz.waga, LISTYWYSD.nrdomu, LISTYWYSD.nrlokalu, l.stanowisko
  from listywysd
    left join listywysdpoz on (LISTYWYSD.REF = LISTYWYSDPOZ.DOKUMENT)
    left join wersje on (listywysdpoz.wersjaref = wersje.ref)
    left join towary on (towary.ktm = wersje.ktm)
    left join listywysdroz_opk l on (listywysd.ref = l.listwysd)
  where listywysd.listawys = :LISTYWYSREF
  order by LISTYWYSD.adrtyp
  into :LISTYWYSDREF,:ADRTYP,:ADRES,:KODPOCZTOWY,:MIASTO,:ILOSC,:UWAGISPED,
    :FROMPLACE,:TOPLACE,:KTM,:JMIARY,:NAZWA,:wersjaref,:waga, :nrdomu, :nrlokalu, :box
  do begin
    ADRGRUPA = '';
    ADRKLASA = '';
    ADRNALEPKA = '';
    ADROPIS = '';
    ADRSTAN = '';
    adres = adres || ' ' || coalesce(nrdomu,'') || iif(coalesce(nrlokalu,'')<>'','/'||nrlokalu,'');
   for select atrybuty.cecha,atrybuty.wartosc from atrybuty
      where atrybuty.wersjaref = :wersjaref --and atrybuty.cecha like ('ADR%')
      into :cecha,:wartosc
    do begin
        if(:cecha = :NAZWAPARAMGRUPA) then ADRGRUPA = :wartosc;
        if(:cecha = :NAZWAPARAMKLASA) then
          begin
            ADRKLASA = :wartosc;
            if (:wartosc <> 'FREE') then SUMAWAGIADR = :SUMAWAGIADR + :waga;
          end
        if(:cecha = :NAZWAPARAMNALEPKA) then ADRNALEPKA = :wartosc;
        if(:cecha = :NAZWAPARAMOPIS) then ADROPIS = :wartosc;
        if(:cecha = :NAZWAPARAMSTAN) then ADRSTAN = :wartosc;
    end
    suspend;
  end
end^
SET TERM ; ^
