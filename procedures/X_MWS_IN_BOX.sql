--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_IN_BOX(
      LISTYWYSDROZ INTEGER_ID)
  returns (
      REF INTEGER_ID,
      ILOSCMAG NUMERIC_14_4,
      JEDNOSTKA STRING10,
      KTM KTM_ID,
      NAZWA STRING40,
      NAZWAWERSJI STRING40,
      DATAPAKOWANIA TIMESTAMP_ID,
      PAKOWAL STRING20,
      OPAKOWANIE SMALLINT_ID)
   as
begin
  ref = 0;
  iloscmag = 1;
  jednostka = 'kar';
  nazwa = 'Karton w kartonie';
  nazwawersji = '<---';
  datapakowania = null;
  pakowal = '';
  opakowanie = 1;
  for
    select distinct o.stanowisko
      from listywysdroz_opk o
      where o.rodzic = :listywysdroz
    into :ktm
  do begin
    suspend;
  end
  for
    select r.ref, r.iloscmag, j.jedn, r.ktm, r.nazwat, w.nazwa, r.x_data_pak, o.nazwa, 0
      from listywysdroz r
      left join listywysdpoz p on (p.ref = r.listywysdpoz)
      left join wersje w on (w.ref = p.wersjaref)
      left join towjedn j on (j.ref = p.jedno)
      left join operator o on (o.ref = r.pakowal)
      where r.opk = :listywysdroz
    into :ref, :iloscmag, :jednostka, :ktm, :nazwawersji, :nazwa, :datapakowania, :pakowal, :opakowanie
  do begin
    suspend;
  end
end^
SET TERM ; ^
