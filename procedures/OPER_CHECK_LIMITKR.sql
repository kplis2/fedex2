--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_CHECK_LIMITKR(
      ZAM integer,
      TYP integer,
      ISREAL integer)
  returns (
      STATUS integer,
      BLOKADA integer,
      NIEWYP integer,
      LIMITKR numeric(14,2),
      LIMITOVER numeric(14,2),
      POPLAT numeric(14,2),
      POPLATDNI integer,
      SPOSPLATBEZP integer,
      MAXTERMIN integer,
      MAXTERMINOVER integer)
   as
DECLARE VARIABLE KLIENT INTEGER;
DECLARE VARIABLE SPOSZAP INTEGER;
DECLARE VARIABLE KWOTA NUMERIC(14,2);
DECLARE VARIABLE TERMIN INTEGER;
begin
  STATUS = -1;
  BLOKADA = 0;
  NIEWYP = 0;
  LIMITKR = 0;
  LIMITOVER = 0;
  POPLAT = 0;
  POPLATDNI = 0;
  SPOSPLATBEZP = 0;
  MAXTERMIN = 0;
  MAXTERMINOVER = 0;
  select PLATNIK from NAGZAM where REF=:zam into :klient;
  if(:klient is null) then select KLIENT from NAGZAM where REF=:zam into :klient;
  if(:klient is null) then begin
    STATUS=1;
    exit;
  end
  if(:isreal > 0) then
    SELECT NAGZAM.sumwartrbruzl, NAGZAM.sposzap, NAGZAM.TERMZAP from NAGZAM where ref =:zam into :kwota, :sposzap, :termin;
  else
    SELECT NAGZAM.sumwartbruzl, NAGZAM.sposzap, nagzam.termzap from NAGZAM where ref =:zam into :kwota, :sposzap, :termin;
  if(:kwota is null) then begin
    status = 1;
    exit;
  end
  execute procedure KLIENT_CHECK_PLAT(:klient,:typ, :sposzap, :kwota, :termin)
  returning_values :blokada, :niewyp,:limitkr, :limitover,:poplat, :poplatdni, :sposplatbezp, :maxtermin, :maxterminover;
  status = 1;
end^
SET TERM ; ^
