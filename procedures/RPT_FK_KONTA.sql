--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_KONTA(
      PERIODID varchar(6) CHARACTER SET UTF8                           ,
      CURRENCY varchar(3) CHARACTER SET UTF8                           ,
      ACCWOPEN smallint,
      ACCTYP smallint,
      ACCOUNTMASK ACCOUNT_ID,
      FROMACCOUNT ACCOUNT_ID,
      TOACCOUNT ACCOUNT_ID,
      COMPANY integer)
  returns (
      REF integer,
      SYMBOL varchar(3) CHARACTER SET UTF8                           ,
      ACCOUNT ACCOUNT_ID,
      DESCRIPT varchar(80) CHARACTER SET UTF8                           ,
      ODEBIT numeric(15,2),
      OCREDIT numeric(15,2),
      DEBIT numeric(15,2),
      CREDIT numeric(15,2),
      ADEBIT numeric(15,2),
      ACREDIT numeric(15,2),
      BDEBIT numeric(15,2),
      BCREDIT numeric(15,2),
      ISACCOUNT smallint,
      ZMIENNA ACCOUNT_ID,
      S_TMP ACCOUNT_ID)
   as
declare variable old_symbol varchar(3);
declare variable yearid integer;
declare variable old_account account_id;
declare variable s_account account_id;
declare variable n_debit numeric(15,2);
declare variable n_credit numeric(15,2);
declare variable i_ref integer;
declare variable s_sep varchar(1);
declare variable i_dictdef integer;
declare variable i_len smallint;
declare variable old_prefix account_id;
declare variable i_i smallint;
declare variable i_j smallint;
declare variable i_k smallint;
declare variable dictref integer;
declare variable r_account integer;
declare variable sql varchar(2048);
begin
  -- JUZ AKTUALNIE NIEUZYWANA - zastapila ja procedura RPT_FK_ACCOUNTS_ANALITICS(
  ref = 0;
  old_symbol = '';
  old_account = '';
  old_prefix = '';
  execute procedure create_account_mask(:accountmask) returning_values :accountmask;
  if (accountmask = '%') then accountmask = null;

  select yearid from BKPERIODS where company = :company and ID = :periodid into :yearid;
  if (toaccount is not null and toaccount <> '') then
  begin
    select max(account)
      from accounting
      where (account <= :toaccount or account like :toaccount || '%')
        and yearid = :yearid and accounting.company = :company
      into :toaccount;
  end

  for select A.ref,A.account, T.debit, T.credit
    from accounting A
    join turnovers T on (T.accounting=A.ref and T.period=:periodid)
    where A.yearid = :yearid and A.currency=:currency
      and ((:ACCOUNTMASK is null) or (:accountmask = '') or (A.account like :accountmask))
      and ((:fromaccount is null) or (:fromaccount = '') or (:fromaccount <= A.Account))
      and ((:toaccount is null) or (:toaccount = '') or (:toaccount >= A.Account))
      and ((:acctyp=0 and A.bktype<2) or (:acctyp=1 and A.bktype=2) or :acctyp=2)
      and A.company = :company
    order by A.account
    into :r_account, :s_account, :n_debit, :n_credit
  do begin
    symbol = cast(substring(:s_account from 1 for 3) as varchar(3));
    odebit = null;
    ocredit = null;
    adebit = null;
    acredit = null;
    debit = null;
    credit = null;
    bdebit = null;
    bcredit = null;
    if (n_debit is null) then n_debit = 0;
    if (n_credit is null) then n_credit = 0;
    isaccount = 0;
    if (old_symbol <>:symbol) then -- szukamy nazwy syntetyki
    begin
      select descript, ref
        from bkaccounts where symbol=:symbol and yearid = :yearid and bkaccounts.company = :company
        into :descript, :i_ref;
      debit = null;
      credit = null;
      account = :symbol;
      if (symbol <> :s_account) then begin
        ref = ref + 1;
        suspend;
      end
      i_dictdef = null;
      i_i = 0;
      -- zliczam ilosc poziomów nalitycznych
      for select separator, dictdef, len
        from accstructure
        where bkaccount = :i_ref
        order by nlevel
        into :s_sep, :i_dictdef, :i_len
      do begin
        i_i = i_i + 1;
      end
    end
    if (:i_i>0) then
    begin
    --zmienna = substring(:s_account from  1, coalesce(char_length(:s_account) for 0)-:i_len); -- [DG] XXX ZG119346
      if (:i_i>1 and :old_prefix<>substring(:s_account from  1 for coalesce(char_length(:s_account),0)-:i_len)) then -- [DG] XXX ZG119346
      -- drukowanie opisow posrednich analityk
      begin
        i_j = 0;
        i_k = 4;
        for select separator, dictdef, len
          from accstructure
          where bkaccount = :i_ref
          order by nlevel
          into :s_sep, :i_dictdef, :i_len
        do begin
          i_j = i_j + 1;
          if (i_j < i_i) then
          begin
            if (s_sep<>',') then
              i_k = i_k +1;
            s_tmp = substring(:s_account from  i_k for  i_k + i_len - 1);
            account = '      ' || s_tmp;
            execute procedure name_from_bksymbol(company, i_dictdef, s_tmp)
              returning_values descript, dictref;
            ref = ref + 1;
            suspend;
            i_k = i_k + i_len;
          end
        end
        old_prefix = substring(s_account from  1 for coalesce(char_length(s_account) ,0) - i_len); -- [DG] XXX ZG119346
        end
      s_tmp = substring(s_account from  coalesce(char_length(s_account) ,0) - i_len+1 for coalesce(char_length(s_account),0)); -- [DG] XXX ZG119346
      execute procedure name_from_bksymbol(company, i_dictdef, s_tmp)
        returning_values descript, dictref;
    end
    --zmienna=null;
    debit = n_debit;
    credit = n_credit;
    select sum(T.debit), sum(T.credit)
      from turnovers T
      where  (T.YEARID = :yearid and T.ACCOUNTING = :r_account
        and T.PERIOD = substring(:periodid from 1 for 4) || '00')
      into :odebit, :ocredit;
    if(:odebit is null) then odebit = 0;
    if(:ocredit is null) then ocredit = 0;
      select sum(T.debit), sum(T.credit)
      from turnovers T
      where (t.yearid = :yearid and T.ACCOUNTING = :r_account and T.PERIOD > substring(:periodid from 1 for 4)||'00' and T.PERIOD < :periodid )
      into :adebit, :acredit;
    if (adebit is null) then adebit = 0;
    if (acredit is null) then acredit = 0;
    acredit = acredit + credit;
    adebit = adebit + debit;
    if (ACCWOPEN = 1) then begin
      acredit = acredit + ocredit;
      adebit = adebit + odebit;
    end
    if (abs(acredit) > abs(adebit)) then
      bcredit = acredit - adebit;
    else
      bdebit = adebit - acredit;
    if (bdebit is null) then bdebit = 0;
    if (bcredit is null) then bcredit = 0;
    account = s_account;
    old_symbol = symbol;
    old_account = account;
    ref = ref + 1;
    isaccount = 1;
    suspend;
    s_tmp=null;
  end
end^
SET TERM ; ^
