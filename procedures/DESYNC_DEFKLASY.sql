--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DESYNC_DEFKLASY(
      REFF integer)
   as
DECLARE VARIABLE IL INTEGER;
begin
   select count(*) from LOG_FILE where TABLE_NAME='DEFKLASY' and KEY1=:REFF into :il;
   if(:il is null) then il = 0;
   if(:il = 0) then
      insert into LOG_FILE(TABLE_NAME,TIME_STAMP,KEY1) values ('DEFKLASY',CURRENT_TIMESTAMP(0),:REFF);
   else
     update LOG_FILE set TIME_STAMP=CURRENT_TIMESTAMP(0) where TABLE_NAME='DEFKLASY' and KEY1=:REFF;
end^
SET TERM ; ^
