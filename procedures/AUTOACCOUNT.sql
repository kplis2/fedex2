--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTOACCOUNT(
      BACCOUNTREF integer)
   as
declare variable symbol varchar(10);
declare variable slenght integer;
declare variable poczatek_konta varchar(60);
declare variable stymkonto varchar(80);
declare variable kodpl varchar(4);
declare variable tymkonto integer;
declare variable cnt integer;
declare variable cnt2 integer;
declare variable modulo integer;
declare variable skonto varchar(80);
declare variable numer numeric(12,0);
begin
  if(:baccountref is null) then exit;
  select bankacc from bankaccounts where ref = :baccountref into :symbol;
  kodpl = '2521'; --kod dla PL: P=25, L=21
  select bankacc.autoaccount from bankacc
    where bankacc.symbol = :symbol
      and bankacc.autogen = 1
    into :poczatek_konta;
  select max(accountno) from bankaccounts
    where bankacc = :symbol
    into :numer;
  if(numer is null) then numer = 1;
  else numer = numer + 1;
  if(numer > 999999999998) then
    exception universal 'Nie można wygenerowac konta bankowego. Przekroczenie zakresu.';
  skonto = '00000000000'||cast(:numer as varchar(12));  --zwiekszenie numeru do 12 znakow
  slenght = coalesce(char_length(skonto),0); -- [DG] XXX ZG119346
  skonto = :poczatek_konta||substring(:skonto from  slenght-11 for  slenght);
  stymkonto = :skonto||:kodpl||'00'; --polaczenie wygenerow. numeru z poczatkiem konta (tabela bankacc) oraz kodem Polski; 00-poczatkowa cyfra konrolna
  modulo = 0;
  cnt = 1;
  /*dzielenie konta modulo 97 */
  while (:cnt < coalesce(char_length(:stymkonto),0)) do begin -- [DG] XXX ZG119346
    tymkonto = cast((cast(:modulo as varchar(4))||substring(:stymkonto from :cnt for :cnt+5)) as integer);
    modulo = mod(:tymkonto,97);
    cnt = cnt + 6;
    tymkonto = 0;
  end
  modulo = 98 - modulo;  --korekta wyniku zgodnie z algorytmem liczenia cyfry kontrolnej
  cnt = 1;
  cnt2 = 1;
  stymkonto = '';
  /*rozbicie konta na grupy po 4 cyfry */
  while (:cnt <= coalesce(char_length(:skonto),0)) do begin -- [DG] XXX ZG119346
    stymkonto = :stymkonto||substring(:skonto from :cnt for :cnt);
    cnt = cnt + 1;
    cnt2 = cnt2 + 1;
    if(cnt2 > 4) then begin
      stymkonto = :stymkonto||' ';
      cnt2 = 1;
    end
  end
  if(modulo < 10) then        -- korekta cyfry kontrolnej jesli cyfra < 10
    skonto = '0'||:modulo||' '||:stymkonto;
  else
    skonto = :modulo||' '||:stymkonto;

  update bankaccounts
    set bankaccounts.account = :skonto,  bankaccounts.accountno = :numer
    where bankaccounts.ref = :baccountref;

end^
SET TERM ; ^
