--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_VATUE_V4_10E_EXP(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(8191) CHARACTER SET UTF8                           )
   as
--zmianna pomocnicza
declare variable isdeclaration integer;
declare variable iscorrection smallint_id;
declare variable def varchar(20);
declare variable edeclddef integer;
declare variable code varchar(10);
declare variable pfield varchar(10);
declare variable pfieldsymbol type of column edeclpos.fieldsymbol;
declare variable psubfieldsymbol type of column edeclpos.fieldsymbol;
declare variable fieldprefixendchar char_1;
declare variable systemcode varchar(20);
declare variable obligationkind varchar(20);
declare variable schemaver varchar(10);
declare variable symbol varchar(10);
declare variable variant varchar(10);
declare variable pvalue varchar(255);
declare variable tmpval1 varchar(255);
declare variable tmpval2 varchar(255);
declare variable tmp smallint;
declare variable curr_p integer;
declare variable correction smallint;
declare variable enclosureowner integer;
declare variable zaltyp varchar(20);
declare variable iszal smallint;
declare variable parent_lvl_0 integer;
declare variable parent_lvl_1 integer;
declare variable parent_lvl_2 integer;
declare variable parent_lvl_3 integer;
begin
  --Sprawdzamy czy dana deklaracja istnieje do eksportu
  select first 1 ref, iif(coalesce(e.correction, '') = '', 1, 2)  -- 1 - zlozenie dokumentu, 2 - korekta
    from edeclarations e
    where e.ref = :oref
  into :isDeclaration, isCorrection;

  if(isDeclaration is null) then
    exception universal 'Niepoprawny identyfikator e-deklaracji';

  --Pobieranie definicji
  select e.edecldef, ed.code, ed.systemcode, ed.obligationkind, ed.schemaver, ed.symbol, ed.variant, ed.ref
    from edeclarations e
      join edecldefs ed on (ed.ref = e.edecldef)
    where e.ref = :oref
  into :def, :code, :systemcode, :obligationkind, :schemaver, :symbol, :variant, :edeclddef;

  if (:symbol = 'VAT-UEK') then
  begin
    systemcode = 'VATUEK (4)';
  end

  --Generujemy naglowek pliku XML
  id = 0;
  name = 'Deklaracja';
  parent = null;
  if (:isCorrection = 1) then
  begin
    params = 'xmlns="http://crd.gov.pl/wzor/2017/01/11/3846/" xmlns:def="http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/"';
  end else
  begin
    params = 'xmlns="http://crd.gov.pl/wzor/2017/01/11/3845/" xmlns:def="http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/"';
  end
  val = null;
  suspend;

  correction = 0;
  parent_lvl_0 = 0;

  --Rozpoczynamy naglowek e-deklaracji
  id = :id + 1; --1
  name = 'Naglowek';
  parent = parent_lvl_0;
  params = null;
  val  = null;
  parent_lvl_1 = :id;
  suspend;
  --cialo naglowka

    --KodFormularza
    /* <ns:KodFormularza
         kodSystemowy= "VAT-UE (4)|VAT-UEK (4)"
         wersjaSchemy= "1-0E">
           VAT-UE|VAT-UEK
       </ns:KodFormularza>
    */
    id = id+1;
    name = 'KodFormularza';
    parent = parent_lvl_1;
    params = '';
    if(code is not null) then
    params = params||'kodPodatku= "'||code||'" ';
    if(systemcode is not null) then
    params = params||'kodSystemowy= "'||systemcode||'" ';
    if(obligationkind is not null) then
    params = params||'rodzajZobowiazania= "'||obligationkind||'" ';
    if(schemaver is not null) then
    params = params||'wersjaSchemy= "'||schemaver||'"';
    val = symbol;
    suspend;
    
    --WariantFormularza
    /* <ns:WariantFormularza>
         4
       </ns:WariantFormularza>
    */
    id = id+1;
    name= 'WariantFormularza';
    parent = parent_lvl_1;
    params = null;
    val = variant;
    suspend;

    --Rok
    /* <ns:Rok>
         2017
       </ns:Rok>
    */
    id = id+1;
    name= 'Rok';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.fieldsymbol = 'K5'
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params = null;
      val = :pvalue;
      suspend;
    end
    pvalue = null;

    --Miesiac
    /* <ns:Miesiac>
         2
       </ns:Miesiac>
    */
    id = id+1;
    name= 'Miesiac';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.fieldsymbol = 'K4'
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params = null;
      val = :pvalue;
      suspend;
    end
    pvalue = null;

    --CelZlozenia
    /* <ns:CelZlozenia>
         1
       </ns:CelZlozenia>
    */
    id = id+1;
    name= 'CelZlozenia';
    parent = parent_lvl_1;
    params = null;
    val = 1;
    suspend;

    --KodUrzedu
    /* <ns:KodUrzedu>
         0402
       </ns:KodUrzedu>
    */
    id = id+1;
    name= 'KodUrzedu';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.fieldsymbol = 'USP'
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params = null;
      val = :pvalue;
      suspend;
    end
    --Koniec Naglowka
    
  --Podmiot
  /* <Podmiot1 rola= "Podatnik">
       <!--You have a CHOICE of the next 2 items at this level-->
       <def:OsobaFizyczna>
         ...
       </def:OsobaFizyczna>
       <def:OsobaNiefizyczna>
         ...
       </def:OsobaNiefizyczna>
     </Podmiot1>
  */
  id = id+1;
  name= 'Podmiot1';
  parent = parent_lvl_0;
  params = 'rola= "Podatnik"';
  val = null;
  parent_lvl_1 = :id;
  suspend;
    pvalue = null;
    select epos.pvalue
        from edeclpos epos
        where epos.edeclaration = :oref and epos.fieldsymbol = 'K7'
      into :pvalue;
  if(:pvalue = 2) then
  begin
    --OsobaFizyczna
    /* <def:OsobaFizyczna>
         <def:NIP>string</def:NIP>
         <def:ImiePierwsze>token</def:ImiePierwsze>
         <def:Nazwisko>token</def:Nazwisko>
         <def:DataUrodzenia>1900-01-01+01:00</def:DataUrodzenia>
       </def:OsobaFizyczna>
    */
    id = id+1;
    name= 'def:OsobaFizyczna';
    parent = parent_lvl_1;
    params = null;
    val = null;
    parent_lvl_2 = :id;
    suspend;
      pvalue = null;
      --NIP
      id = id+1;
      name= 'def:NIP';
      parent = parent_lvl_2;
      select REPLACE(epos.pvalue, '-', '')
        from edeclpos epos
        where epos.edeclaration = :oref and epos.fieldsymbol = 'K1'
      into :pvalue;
      params = null;
      val = :pvalue;
      suspend;
      pvalue = null;
      --ImiePierwsze && Nazwisko && DataUrodzenia
      tmp = 0;
      select epos.pvalue
      from edeclpos epos
        where epos.edeclaration = :oref and epos.fieldsymbol = 'K8'
      into :pvalue;
      for select outstring
        from parse_lines(:pvalue, ',')
      into :tmpval2
      do begin
      if(tmp = 0) then
      begin
        tmpval1 = :tmpval2;
      end
      if(tmp = 1) then
      begin
      --PelnaNazwa
        id = id+1;
        name= 'def:ImiePierwsze';
        parent = parent_lvl_2;
        val = tmpval2;
        params = null;
        suspend;

        --Nazwisko
        id = id+1;
        name= 'def:Nazwisko';
        parent = parent_lvl_2;
        val = tmpval1;
        params = null;
        suspend;
      end
      if(tmp = 2) then
      begin
        --DataUrodzenia
        id = id+1;
        name= 'def:DataUrodzenia';
        parent = parent_lvl_2;
        val = tmpval2;
        params = null;
        suspend;
      end
      tmp = :tmp + 1;
      end
    end
  else if(:pvalue = 1) then
    begin
    --OsobaNiefizyczna
    /* <def:OsobaNiefizyczna>
         <def:NIP>string</def:NIP>
         <def:PelnaNazwa>token</def:PelnaNazwa>
         <!--Optional:-->
         <def:REGON>string</def:REGON>
       </def:OsobaNiefizyczna>
    */
    id = id+1;
    name= 'def:OsobaNiefizyczna';
    parent = parent_lvl_1;
    params = null;
    val = null;
    parent_lvl_2 = :id;
    suspend;
      pvalue = null;
      --NIP
      id = id+1;
      name= 'def:NIP';
      parent = parent_lvl_2;
      select REPLACE(epos.pvalue, '-', '')
        from edeclpos epos
        where epos.edeclaration = :oref and epos.fieldsymbol = 'K1'
      into :pvalue;
      params = null;
      val = :pvalue;
      suspend;
      pvalue = null;
      --PelnaNazwa && REGON
      tmp = 0;
      select epos.pvalue
      from edeclpos epos
        where epos.edeclaration = :oref and epos.fieldsymbol = 'K8'
      into :pvalue;

        --PelnaNazwa
        id = id+1;
        name= 'def:PelnaNazwa';
        parent = parent_lvl_2;
      tmpval1 = reverse(:pvalue);
      tmpval2 = reverse(substring(:tmpval1 from 1 for position(',',:tmpval1)-1));
      val = substring(:pvalue from 1 for coalesce(char_length(:pvalue),0) - (coalesce(char_length(:tmpval2),0)+1)); -- [DG] XXX ZG119346
        params = null;
        suspend;

        --REGON
        id = id+1;
        name= 'def:REGON';
        parent = parent_lvl_2;
      tmpval2 = trim(:tmpval2);
      if (tmpval2 <> '') then
      begin
        val = tmpval2;
        params = null;
        suspend;
      end
  end

  --PozycjeSzczegolowe

  /*<PozycjeSzczegolowe>
      <!--Zero or more repetitions-->
      <ns:Grupa1>
        ...
      </ns:Grupa1>
    <!--Zero or more repetitions-->
      <ns:Grupa2>
        ...
      </ns:Grupa2>
    <!--Zero or more repetitions-->
      <ns:Grupa3>
        ...
      </ns:Grupa3>
 </PozycjeSzczegolowe>
  */
  id = id+1;
  name= 'PozycjeSzczegolowe';
  parent = parent_lvl_0;
  params = null;
  val = null;
  parent_lvl_1 = :id;
  suspend;
  pvalue = null;

  fieldprefixendchar = '.';

  for select epos.pvalue, epos.fieldsymbol
    from edeclpos epos
    where epos.edeclaration = :oref
      and epos.fieldsymbol starting with 'CPOZ_'
    order by epos.field
  into :pvalue, :pfieldsymbol
  do begin
    psubfieldsymbol = substring(:pfieldsymbol
      from position(:fieldprefixendchar,:pfieldsymbol) + 1);

    if (:iscorrection = 1) then
    begin
      if (:psubfieldsymbol = 'NUMER') then
      begin
        -- Grupa 1
        /* <!--Zero or more repetitions-->
           <ns:Grupa1>
             ...
           </ns:Grupa1>
        */
        id = id+1;
        name= 'Grupa1';
        parent = parent_lvl_1;
        params = null;
        val = null;
        parent_lvl_2 = :id;
        suspend;
        pvalue = null;
      end
      else if (:psubfieldsymbol = 'A' and coalesce(:pvalue,'') <> '' ) then
      begin
        -- P_Da
        /* <ns:P_Da>
             IT
           </ns:P_Da>
        */
        id = id+1;
        name = 'P_Da';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'B') then
      begin
        -- P_Db
        /* <ns:P_Db>
             string
           </ns:P_Db>
        */
        id = id+1;
        name = 'P_Db';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'C') then
      begin
        -- P_Dc
        /* <ns:P_Dc>
             100
           </ns:P_Dc>
        */
        id = id+1;
        name = 'P_Dc';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'D') then
      begin
        -- P_Dd
        /* <ns:P_Dd>
             2
           </ns:P_Dd>
        */
        id = id+1;
        name = 'P_Dd';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
    end else
    begin
      if (:psubfieldsymbol = 'NUMER') then
      begin
        -- Grupa 1
        /* <!--Zero or more repetitions-->
           <ns:Grupa1>
             ...
           </ns:Grupa1>
        */
        id = id+1;
        name= 'Grupa1';
        parent = parent_lvl_1;
        params = null;
        val = null;
        parent_lvl_2 = :id;
        suspend;
        pvalue = null;
      end
      else if (:psubfieldsymbol = 'A_OLD' and coalesce(:pvalue,'') <> '' ) then
      begin
        -- P_DBa
        /* <ns:P_DBa>
             IT
           </ns:P_DBa>
        */
        id = id+1;
        name = 'P_DBa';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'B_OLD') then
      begin
        -- P_DBb
        /* <ns:P_DBb>
             string
           </ns:P_DBb>
        */
        id = id+1;
        name = 'P_DBb';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'C_OLD') then
      begin
        -- P_DBc
        /* <ns:P_DBc>
             string
           </ns:P_DBc>
        */
        id = id+1;
        name = 'P_DBc';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'D_OLD') then
      begin
        -- P_DBd
        /* <ns:P_DBd>
             string
           </ns:P_DBd>
        */
        id = id+1;
        name = 'P_DBd';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'A') then
      begin
        -- P_DJa
        /* <ns:P_DJa>
             string
           </ns:P_DJa>
        */
        id = id+1;
        name = 'P_DJa';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'B') then
      begin
        -- P_DJb
        /* <ns:P_DJb>
             string
           </ns:P_DJb>
        */
        id = id+1;
        name = 'P_DJb';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'C') then
      begin
        -- P_DJc
        /* <ns:P_DJc>
             string
           </ns:P_DJc>
        */
        id = id+1;
        name = 'P_DJc';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'D') then
      begin
        -- P_DJd
        /* <ns:P_DJd>
             string
           </ns:P_DJd>
        */
        id = id+1;
        name = 'P_DJd';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
    end
  end

  fieldprefixendchar = '.';

  for select epos.pvalue, epos.fieldsymbol
    from edeclpos epos
    where epos.edeclaration = :oref
      and epos.fieldsymbol starting with 'DPOZ_'
    order by epos.field
  into :pvalue, :pfieldsymbol
  do begin
    psubfieldsymbol = substring(:pfieldsymbol
      from position(:fieldprefixendchar,:pfieldsymbol) + 1);
    if (:iscorrection = 1) then
    begin
      if (:psubfieldsymbol = 'NUMER') then
      begin
        -- Grupa 2
        /* <!--Zero or more repetitions-->
           <ns:Grupa2>
             ...
           </ns:Grupa2>
        */
        id = id+1;
        name= 'Grupa2';
        parent = parent_lvl_1;
        params = null;
        val = null;
        parent_lvl_2 = :id;
        suspend;
        pvalue = null;
      end
      else if (:psubfieldsymbol = 'A' and coalesce(:pvalue,'') <> '' ) then
      begin
        -- P_Na
        /* <ns:P_Na>
             IT
           </ns:P_Na>
        */
        id = id+1;
        name = 'P_Na';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'B') then
      begin
        -- P_Nb
        /* <ns:P_Nb>
             string
           </ns:P_Nb>
        */
        id = id+1;
        name = 'P_Nb';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'C') then
      begin
        -- P_Nc
        /* <ns:P_Nc>
             100
           </ns:P_Nc>
        */
        id = id+1;
        name = 'P_Nc';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'D') then
      begin
        -- P_Nd
        /* <ns:P_Nd>
             2
           </ns:P_Nd>
        */
        id = id+1;
        name = 'P_Nd';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
    end else
    begin
      if (:psubfieldsymbol = 'NUMER') then
      begin
        -- Grupa 2
        /* <!--Zero or more repetitions-->
           <ns:Grupa2>
             ...
           </ns:Grupa2>
        */
        id = id+1;
        name= 'Grupa2';
        parent = parent_lvl_1;
        params = null;
        val = null;
        parent_lvl_2 = :id;
        suspend;
        pvalue = null;
      end
      else if (:psubfieldsymbol = 'A_OLD' and coalesce(:pvalue,'') <> '' ) then
      begin
        -- P_NBa
        /* <ns:P_NBa>
             IT
           </ns:P_NBa>
        */
        id = id+1;
        name = 'P_NBa';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'B_OLD') then
      begin
        -- P_NBb
        /* <ns:P_NBb>
             string
           </ns:P_NBb>
        */
        id = id+1;
        name = 'P_NBb';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'C_OLD') then
      begin
        -- P_NBc
        /* <ns:P_NBc>
             string
           </ns:P_NBc>
        */
        id = id+1;
        name = 'P_NBc';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'D_OLD') then
      begin
        -- P_NBd
        /* <ns:P_NBd>
             string
           </ns:P_NBd>
        */
        id = id+1;
        name = 'P_NBd';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'A') then
      begin
        -- P_NJa
        /* <ns:P_NJa>
             string
           </ns:P_NJa>
        */
        id = id+1;
        name = 'P_NJa';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'B') then
      begin
        -- P_NJb
        /* <ns:P_NJb>
             string
           </ns:P_NJb>
        */
        id = id+1;
        name = 'P_NJb';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'C') then
      begin
        -- P_NJc
        /* <ns:P_NJc>
             string
           </ns:P_NJc>
        */
        id = id+1;
        name = 'P_NJc';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'D') then
      begin
        -- P_NJd
        /* <ns:P_NJd>
             string
           </ns:P_NJd>
        */
        id = id+1;
        name = 'P_NJd';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
    end
  end


  fieldprefixendchar = '.';

  for select epos.pvalue, epos.fieldsymbol
    from edeclpos epos
    where epos.edeclaration = :oref
      and epos.fieldsymbol starting with 'EPOZ_'
    order by epos.field
  into :pvalue, :pfieldsymbol
  do begin
    psubfieldsymbol = substring(:pfieldsymbol
      from position(:fieldprefixendchar,:pfieldsymbol) + 1);

    if (:iscorrection = 1) then
    begin
      if (:psubfieldsymbol = 'NUMER') then
      begin
        -- Grupa 3
        /* <!--Zero or more repetitions-->
           <ns:Grupa3>
             ...
           </ns:Grupa3>
        */
        id = id+1;
        name= 'Grupa3';
        parent = parent_lvl_1;
        params = null;
        val = null;
        parent_lvl_2 = :id;
        suspend;
        pvalue = null;
      end
      else if (:psubfieldsymbol = 'A' and coalesce(:pvalue,'') <> '' ) then
      begin
        -- P_Ua
        /* <ns:P_Ua>
             IT
           </ns:P_Ua>
        */
        id = id+1;
        name = 'P_Ua';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'B') then
      begin
        -- P_Ub
        /* <ns:P_Ub>
             string
           </ns:P_Ub>
        */
        id = id+1;
        name = 'P_Ub';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'C') then
      begin
        -- P_Uc
        /* <ns:P_Uc>
             100
           </ns:P_Uc>
        */
        id = id+1;
        name = 'P_Uc';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
    end else
    begin
      if (:psubfieldsymbol = 'NUMER') then
      begin
        -- Grupa 3
        /* <!--Zero or more repetitions-->
           <ns:Grupa3>
             ...
           </ns:Grupa3>
        */
        id = id+1;
        name= 'Grupa3';
        parent = parent_lvl_1;
        params = null;
        val = null;
        parent_lvl_2 = :id;
        suspend;
        pvalue = null;
      end
      else if (:psubfieldsymbol = 'A_OLD' and coalesce(:pvalue,'') <> '' ) then
      begin
        -- P_UBa
        /* <ns:P_UBa>
             IT
           </ns:P_UBa>
        */
        id = id+1;
        name = 'P_UBa';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'B_OLD') then
      begin
        -- P_UBb
        /* <ns:P_UBb>
             string
           </ns:P_UBb>
        */
        id = id+1;
        name = 'P_UBb';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'C_OLD') then
      begin
        -- P_UBc
        /* <ns:P_UBc>
             string
           </ns:P_UBc>
        */
        id = id+1;
        name = 'P_UBc';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'A') then
      begin
        -- P_UJa
        /* <ns:P_UJa>
             string
           </ns:P_UJa>
        */
        id = id+1;
        name = 'P_UJa';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'B') then
      begin
        -- P_UJb
        /* <ns:P_UJb>
             string
           </ns:P_UJb>
        */
        id = id+1;
        name = 'P_UJb';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
      else if (:psubfieldsymbol = 'C') then
      begin
        -- P_UJc
        /* <ns:P_UJc>
             string
           </ns:P_UJc>
        */
        id = id+1;
        name = 'P_UJc';
        parent = parent_lvl_2;
        params = null;
        val = :pvalue;
        suspend;
      end
    end
  end

  --Pouczenie
  id = id+1;
  name= 'Pouczenie';
  parent = parent_lvl_0;
  params = null;
  val = '1'; --poważnie - 1, tak ma być
  suspend;

end^
SET TERM ; ^
