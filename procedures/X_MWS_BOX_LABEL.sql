--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_BOX_LABEL(
      KODKRESK STRING100)
  returns (
      BOXKTM STRING100,
      LISTYWYSD INTEGER_ID)
   as
begin
  select first 1 l.listwysd, l.x_box_ktm
    from listywysdroz_opk l
    where l.stanowisko = :kodkresk
  into :listywysd,  :boxktm;
  suspend;
end^
SET TERM ; ^
