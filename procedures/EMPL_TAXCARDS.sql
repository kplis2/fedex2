--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMPL_TAXCARDS(
      REF integer,
      IS_EMPL smallint)
  returns (
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      P700 numeric(14,2),
      P650 numeric(14,2),
      P706 numeric(14,2),
      P735 numeric(14,2),
      P595 numeric(14,2))
   as
begin
  --DS: procedura do wyliczania karty zarobkowej pracownika lub osoby
  if (is_empl is null) then is_empl = 0;
  --aby pokazaly sie wszystkie okresy, takze te z zerowymi wartosciami na liscie plac
  for
    select distinct p.tper
      from epayrolls p
        join eprpos r on r.payroll = p.ref
        join employees e on e.ref = r.employee
      where (e.person = :ref and :is_empl = 0)
        or (e.ref = :ref and :is_empl = 1)
      order by p.tper
      into :period
  do begin
    p700=null;p650=null;p706=null;p735=null;p595=null;

    select sum(P.pvalue) as P700
    from EPAYROLLS PR
      left join EPRPOS P on (P.payroll = PR.ref and P.ecolumn = 7000)
      left join employees e on (e.ref = p.employee)
    where ((e.person = :ref and :is_empl = 0)
        or (e.ref = :ref and :is_empl = 1))
      and pr.tper = :period
    into :p700;

    select sum(P.pvalue) as P650
      from EPAYROLLS PR
        left join EPRPOS P on (P.payroll = PR.ref and P.ecolumn = 6500)
        left join employees e on (e.ref = p.employee)
      where ((e.person = :ref and :is_empl = 0)
          or (e.ref = :ref and :is_empl = 1))
        and pr.tper = :period
      into :p650;

    select sum(P.pvalue) as P706
      from EPAYROLLS PR
        left join EPRPOS P on (P.payroll = PR.ref and P.ecolumn = 7060)
        left join employees e on (e.ref = p.employee)
      where ((e.person = :ref and :is_empl = 0)
          or (e.ref = :ref and :is_empl = 1))
        and pr.tper = :period
      into :p706;

    select sum(P.pvalue) as P735
      from EPAYROLLS PR
        left join EPRPOS P on (P.payroll = PR.ref and P.ecolumn = 7350)
        left join employees e on (e.ref = p.employee)
      where ((e.person = :ref and :is_empl = 0)
          or (e.ref = :ref and :is_empl = 1))
        and pr.tper = :period
      into :p735;

      select sum(P.pvalue) as P595
      from EPAYROLLS PR
        left join EPRPOS P on (P.payroll = PR.ref and P.ecolumn = 5950)
        left join employees e on (e.ref = p.employee)
      where ((e.person = :ref and :is_empl = 0)
          or (e.ref = :ref and :is_empl = 1))
        and pr.tper = :period
      into :p595;



    if (P700 is null) then P700 = 0;
    if (P650 is null) then P650 = 0;
    if (P706 is null) then P706 = 0;
    if (P735 is null) then P735 = 0;
    if (P595 is null) then P595 = 0;
    suspend;
  end
end^
SET TERM ; ^
