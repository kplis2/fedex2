--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_E_PAYROLL_SUM(
      PAYROLL integer,
      EMPLOYEE integer)
  returns (
      NUM1 integer,
      NAME1 varchar(22) CHARACTER SET UTF8                           ,
      VAL1 numeric(14,2),
      REPSUM1 integer,
      NUM2 integer,
      NAME2 varchar(22) CHARACTER SET UTF8                           ,
      VAL2 numeric(14,2),
      REPSUM2 integer,
      NUM3 integer,
      NAME3 varchar(22) CHARACTER SET UTF8                           ,
      VAL3 numeric(14,2),
      REPSUM3 integer,
      NUM4 integer,
      NAME4 varchar(22) CHARACTER SET UTF8                           ,
      VAL4 numeric(14,2),
      REPSUM4 integer,
      NUM5 integer,
      NAME5 varchar(22) CHARACTER SET UTF8                           ,
      VAL5 numeric(14,2),
      REPSUM5 integer)
   as
begin
  name1 = null;
  val1 = null;
  NUM1 = null;
  repsum1 = null;
  select  c.name, COALESCE(P.pvalue,0), c.repsum
    from ecolumns C
      left join eprpos P on (C.number=P.ecolumn  and payroll=:payroll and employee=:employee)
    where C.number=4000
    into :name2, val2, repsum2;
  NUM2 = 4000;
  select  c.name, COALESCE(P.pvalue,0), c.repsum
    from ecolumns C
      left join eprpos P on (P.ecolumn=C.number  and payroll=:payroll and employee=:employee)
    where C.number=5900
    into :name3, val3, repsum3;
  NUM3 = 5900;
  select  c.name, COALESCE(P.pvalue,0), c.repsum
    from ecolumns C
      left join eprpos P on (C.number=P.ecolumn  and payroll=:payroll and employee=:employee)
    where C.number=8000
    into :name4, val4, repsum4;
  NUM4 = 8000;
  select  c.name, COALESCE(P.pvalue,0), c.repsum
    from ecolumns C
      left join eprpos P on (C.number=P.ecolumn  and payroll=:payroll and employee=:employee)
    where C.number=9050
    into :name5, val5, repsum5;
  NUM5 = 9050;
  suspend;
end^
SET TERM ; ^
