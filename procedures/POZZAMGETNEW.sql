--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZZAMGETNEW(
      WERSJAREF integer,
      NAGZAMREF NAGZAM_ID,
      ILOSCIN numeric(14,4) = null)
  returns (
      CENABAZOWA CENY_CEN,
      CENANET CENY_CEN,
      RABAT numeric(14,4),
      WALUTA varchar(10) CHARACTER SET UTF8                           ,
      JEDN integer,
      MIARA varchar(10) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      CENAZAKUPU CENY_CEN,
      ILOSC numeric(14,4),
      POZZAMREF POZZAM_ID,
      KTM varchar(255) CHARACTER SET UTF8                           ,
      SUMWARTNETZL numeric(14,2),
      CNT integer)
   as
declare variable klient integer;
DECLARE VARIABLE AKTUODDZIAL VARCHAR(10);
DECLARE VARIABLE KLICENNIK INTEGER;
DECLARE VARIABLE GRUPAKLI INTEGER;
DECLARE VARIABLE KLIRABAT NUMERIC(14,4);
declare variable defcen integer;
declare variable status smallint;
declare variable prec smallint;
declare variable wersja integer;
declare variable iscena smallint;
declare variable promocja integer;
declare variable wal2 varchar(10);
begin
  execute procedure GETCONFIG('AKTUODDZIAL') returning_values :aktuoddzial;
  select klient, sumwartnetzl from nagzam where ref=:nagzamref into :klient,:sumwartnetzl;
  select count(*) from pozzam where zamowienie=:nagzamref into :cnt;
  -- jesli towar jest na pozzamie to zwracamy jego dane do aktualizacji (nie jesli byl dodany z promocji wiazanej)
  select first 1 p.cenacen, p.cenanet, p.rabat, p.ilosc, p.ref, p.ktm
    from pozzam p
    where p.zamowienie = :nagzamref and
          p.wersjaref=:wersjaref
    order by p.cenanet asc
    into :cenabazowa, :cenanet, :rabat, :ilosc, :pozzamref, :ktm;
  if(:cenanet is null) then begin
    --jesli nie ma takiej pozycji na zamowieniu, to dla kontrahenta pobieramy aktualne warunki cenowe
    select grupykli.cennik, grupykli.ref, klienci.upust
    from GRUPYKLI join KLIENCI on (KLIENCI.grupa = GRUPYKLI.ref)
    where KLIENCI.REF = :klient into :klicennik, :grupakli, :klirabat;
    if(:klirabat is null) then klirabat = 0;
    defcen = 0;
    execute procedure CENNIK_ZNAJDZ(:klicennik,:wersjaref,null,:klient,:aktuoddzial,'') returning_values :defcen;
    if(:defcen > 0) then begin
      execute procedure pobierz_cene(:defcen,:wersjaref,null, 'N',null,null,2,null,null,:klient,null,null)
        returning_values :cenabazowa, :waluta, :jedn, :status, :prec;
      if(coalesce(:waluta,'')='') then execute procedure getconfig('WALPLN') returning_values :waluta;
      cenanet = :cenabazowa;
      select TOWJEDN.jedn from TOWJEDN where REF = :jedn into :miara;
      select ktm,nrwersji from WERSJE where REF=:wersjaref into :ktm,:wersja;
      execute procedure OBLICZ_RABAT(:ktm, :wersja, :klient,0,:cenanet,'N', '', :defcen, 0, 0) returning_values :rabat, :iscena, :wal2, :promocja;
      if(:iscena > 0) then begin
        cenanet = :rabat;
        rabat = 0;
      end else begin
        rabat = :rabat + :klirabat;
        if(:rabat > 0) then begin
          cenanet = cast(:cenanet * ( 100-:rabat)/100 as numeric(14,2));
        end
      end
    end
    if(:iloscin is not null) then
      ilosc = :iloscin;
    else
      ilosc = 1;
    pozzamref = 0;
  end
  --katalogowa cena zakupu
  select w.cena_zakn, w.nazwat
    from wersje w
    where w.ref = :wersjaref
    into :cenazakupu, :nazwa;
  if(:cenabazowa is null) then cenabazowa = 0;
  if(:cenanet is null) then cenanet = 0;
  if(:rabat is null) then rabat = 0;
  suspend;
end^
SET TERM ; ^
