--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMPL_INSURCARDS(
      REF integer,
      IS_EMPL smallint)
  returns (
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      P600 numeric(14,2),
      P610 numeric(14,2),
      P611 numeric(14,2),
      P820 numeric(14,2))
   as
begin
  --DS: procedura do wyliczania karty zarobkowej pracownika lub osoby
  if (is_empl is null) then is_empl = 0;
  --aby pokazaly sie wszystkie okresy, takze te z zerowymi wartosciami na liscie plac
  for
    select distinct p.iper
      from epayrolls p
        join eprpos r on r.payroll = p.ref
        join employees e on e.ref = r.employee
      where (e.person = :ref and :is_empl = 0)
        or (e.ref = :ref and :is_empl = 1)
      order by p.iper
      into :period
  do begin
    p600=null;p610=null;p611=null;p820=null;

    select sum(P.pvalue) as P600
    from EPAYROLLS PR
      left join EPRPOS P on (P.payroll = PR.ref and P.ecolumn = 6000)
      left join employees e on (e.ref = p.employee)
    where ((e.person = :ref and :is_empl = 0)
        or (e.ref = :ref and :is_empl = 1))
      and pr.iper = :period
    into :p600;

    select sum(P.pvalue) as P610
      from EPAYROLLS PR
        left join EPRPOS P on (P.payroll = PR.ref and P.ecolumn = 6100)
        left join employees e on (e.ref = p.employee)
      where ((e.person = :ref and :is_empl = 0)
          or (e.ref = :ref and :is_empl = 1))
        and pr.iper = :period
      into :p610;

    select sum(P.pvalue) as P611
      from EPAYROLLS PR
        left join EPRPOS P on (P.payroll = PR.ref and P.ecolumn = 6110)
        left join employees e on (e.ref = p.employee)
      where ((e.person = :ref and :is_empl = 0)
          or (e.ref = :ref and :is_empl = 1))
        and pr.iper = :period
      into :p611;

    select sum(P.pvalue) as P820
      from EPAYROLLS PR
        left join EPRPOS P on (P.payroll = PR.ref and P.ecolumn = 8200)
        left join employees e on (e.ref = p.employee)
      where ((e.person = :ref and :is_empl = 0)
          or (e.ref = :ref and :is_empl = 1))
        and pr.iper = :period
      into :p820;

    if (p600 is null) then p600 = 0;
    if (p610 is null) then p610 = 0;
    if (p611 is null) then p611 = 0;
    if (p820 is null) then p820 = 0;
    suspend;
  end
end^
SET TERM ; ^
