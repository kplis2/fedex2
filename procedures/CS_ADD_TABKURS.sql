--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CS_ADD_TABKURS(
      DATA date,
      BANK varchar(64) CHARACTER SET UTF8                           ,
      SYMBOL varchar(220) CHARACTER SET UTF8                           )
  returns (
      TABKURSREF integer)
   as
begin
    if (not exists (select ref from  tabkurs where cast(data as date)=:data and tabkurs.bank=:bank)) then
        begin
            execute procedure gen_ref('TABKURS')
                returning_values :tabkursref;
            insert into tabkurs(ref, data, bank, symbol, akt) values(:tabkursref, :data, :bank, :symbol, 1);
        end
    else
        begin
            select ref
                from tabkurs
                where data=:data and tabkurs.bank=:bank
                into :tabkursref;
            update tabkurs
                set data=:data, symbol=:symbol
                where ref=:tabkursref;
       end
  suspend;
end^
SET TERM ; ^
