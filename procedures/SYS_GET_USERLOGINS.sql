--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_GET_USERLOGINS(
      USR varchar(40) CHARACTER SET UTF8                           ,
      PSW varchar(40) CHARACTER SET UTF8                           ,
      LOGININAD varchar(255) CHARACTER SET UTF8                            = 'NO')
  returns (
      DBASE varchar(40) CHARACTER SET UTF8                           ,
      DBUSR varchar(40) CHARACTER SET UTF8                           ,
      DBPSW varchar(40) CHARACTER SET UTF8                           )
   as
declare variable CRYPTEDPSW varchar(255);
declare variable LOGINFROMDOMAIN varchar(255);
begin
  cryptedpsw = shash_hmac('hex','sha1',:psw,:usr);

  if(:LOGININAD = 'OK') then
  begin
    select o.login
      from operator o
      where o.domainlogin = :usr
      into :loginFromDomain;
  end

  for
    select l.dbase, l.userlogin, l.userpass
    from s_users u
      join s_userlogins l on (u.userid = l.userid)
    where (u.userlogin = :usr and u.userpass = :cryptedpsw)
      or( :LOGININAD = 'OK' and u.userlogin = :loginFromDomain)
    into :dbase, :dbusr, :dbpsw
  do
    suspend;
end^
SET TERM ; ^
