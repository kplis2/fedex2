--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INTRASTAT_DECLXML(
      INTRASTATH integer)
  returns (
      KTC varchar(20) CHARACTER SET UTF8                           ,
      KRAJWYSYLKI varchar(20) CHARACTER SET UTF8                           ,
      WARDOSTAWY varchar(255) CHARACTER SET UTF8                           ,
      INTRDOSTAWY varchar(255) CHARACTER SET UTF8                           ,
      INTRTRANSPORT varchar(255) CHARACTER SET UTF8                           ,
      KRAJPOCHODZENIA varchar(20) CHARACTER SET UTF8                           ,
      WARTOSC numeric(14,2),
      WARTOSC_STATYSTYCZNA numeric(14,2),
      MASA_NETTO numeric(14,4),
      ILOSCUZUPELNIAJACAJM integer,
      OPISTOWARU varchar(255) CHARACTER SET UTF8                           ,
      JM varchar(20) CHARACTER SET UTF8                           )
   as
begin
  for
    select ktc, krajwysylki, wardostawy, intrdostawy, intrtransport, krajpochodzenia,
           coalesce(round(sum(val)),0) as wartosc, coalesce(round(sum(valstat)),0) as wartosc_statystyczna,
           coalesce(round(sum(weight)),0) as masa_netto, coalesce(round(sum(quantityhelpunit)),0) as iloscuzupelniajacajm,
           coalesce(max(name),'') as opistowaru, coalesce(max(helpunit),'') as jedn_uzupelniajaca
      from intrastatp itp
      where itp.intrastath = :intrastath
        and itp.statusflag = 0
      group by ktc, krajwysylki, wardostawy, intrdostawy, intrtransport, krajpochodzenia, helpunit
      order by ktc, krajwysylki, wardostawy, intrdostawy, intrtransport, krajpochodzenia
    into  :ktc, :krajwysylki, :wardostawy, :intrdostawy, :intrtransport, :krajpochodzenia,
          :wartosc, :wartosc_statystyczna, :masa_netto, :iloscuzupelniajacajm, :opistowaru, :jm
  do suspend;
end^
SET TERM ; ^
