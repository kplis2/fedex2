--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_ST_WAU(
      AMPERIOD integer,
      COMPANY integer)
  returns (
      AMGRP varchar(1) CHARACTER SET UTF8                           ,
      AMORTGRP varchar(10) CHARACTER SET UTF8                           ,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      NAME varchar(80) CHARACTER SET UTF8                           ,
      PURCHASEDT timestamp,
      USINGDT timestamp,
      TARATE numeric(10,5),
      FARATE numeric(10,5),
      ACCTAMORT numeric(10,5),
      ACCFAMORT numeric(10,5),
      MTAMORT numeric(10,5),
      MFAMORT numeric(10,5),
      TVALUE numeric(15,2),
      FVALUE numeric(15,2),
      TREDEMPTION numeric(15,2),
      FREDEMPTION numeric(15,2),
      SUMTAMORT numeric(15,2),
      SUMFAMORT numeric(15,2),
      DEPARTMENT varchar(10) CHARACTER SET UTF8                           ,
      FAMETOD varchar(1) CHARACTER SET UTF8                           ,
      TAMETOD varchar(1) CHARACTER SET UTF8                           ,
      FACRATE numeric(10,5),
      TACRATE numeric(10,5),
      ODDZIAL varchar(10) CHARACTER SET UTF8                           ,
      COMPARMENT varchar(10) CHARACTER SET UTF8                           ,
      FXDTYPE smallint,
      ISINTASSET smallint)
   as
begin /*CHNGDMN*//*
declare variable vtvalue numeric(15,2);
declare variable vfvalue numeric(15,2);
declare variable vtredemption numeric(15,2);
declare variable vfredemption numeric(15,2);
declare variable tmetoda varchar(3);
declare variable fmetoda varchar(3);
declare variable atmetoda varchar(3);
declare variable afmetoda varchar(3);
declare variable data timestamp;
declare variable refa integer;
declare variable fa_ref integer;
declare variable amyear integer;
declare variable ammonth integer;
declare variable lastday date;
declare variable stwnipdata smallint;
begin
  execute procedure get_config('STWNIPDATA',2)
    returning_values :stwnipdata;
  stwnipdata = cast(:stwnipdata as smallint);
  select amyear, ammonth from amperiods where ref = :amperiod
    into :amyear, :ammonth;

  if (ammonth = 12) then
    lastday = cast(amyear||'/12/31' as date);
  else
    lastday = cast(amyear||'/'||(ammonth + 1)||'/1' as date) - 1;
  for
    select F.department, F.ref, F.amgrp, F.amortgrp, F.symbol, F.name, F.purchasedt, F.usingdt, F.tarate, F.farate,
     F.tvalue+F.tvallim, F.fvalue, F.tredemption, F.fredemption, F.fameth
     ,F.tameth,F.facrate,F.tacrate, F.oddzial, F.comparment, F.fxdtype, g.isintasset
      from fxdassets F
        left join amperiods p on (F.liquidationperiod = p.ref)
        left join amortgrp g on (g.symbol = F.amortgrp)
      where (F.liquidationperiod is null or p.amyear >= :amyear)
        and ((f.usingdt <= :lastday and :stwnipdata = 1)
          or (f.purchasedt <= :lastday and :stwnipdata = 0))
        and F.company = :company
      order by F.amortgrp
      into :department, :fa_ref, :amgrp, :amortgrp, :symbol, :name, :purchasedt, :usingdt,
       :tarate, :farate, :tvalue, :fvalue, :tredemption, :fredemption,:fmetoda
       ,:tmetoda, :facrate, :tacrate, :oddzial, :comparment, :fxdtype, :isintasset
  do begin
    Atmetoda = null;
    Afmetoda = null;
    select max(operdate),max(ref)
    from AMCHNGMETH
    where amchngmeth.OPERIOD<=:AMPERIOD
    and FXDASSET=:fa_ref
    into :data,:refa;
    if (data is not null) then begin
      select TAMETH, FAMETH
      from AMCHNGMETH
      where amchngmeth.OPERIOD<=:AMPERIOD
      and FXDASSET=:fa_ref
      and operdate=:data
      and ref=:refa
      into :Atmetoda,:Afmetoda;
    end
    if (Atmetoda is not null) then
      tmetoda=Atmetoda;
    if (Afmetoda is not null) then
      fmetoda=Afmetoda;
    Vtvalue=0;
    Vfvalue=0;
    Vtredemption=0;
    Vfredemption=0;
    FAMETOD=substring(fmetoda from 1 for 1);
    TAMETOD=substring(tmetoda from 1 for 1);
    select sum(tvalue+tvallim), sum(fvalue), sum(tredemption), sum(fredemption)
      from valdocs
      where fxdasset=:fa_ref and amperiod<=:amperiod
        AND COALESCE(VALDOCS.corperiod, 0)  = 0
      into :Vtvalue, :Vfvalue, :Vtredemption ,:Vfredemption;
    if (Vtvalue is not null) then
      tvalue=tvalue+Vtvalue;
    if (Vfvalue is not null) then
      fvalue=fvalue+Vfvalue;
    if (Vtredemption is not null) then
      tredemption=tredemption+Vtredemption;
    if (Vfredemption is not null) then
      fredemption=fredemption+Vfredemption;

    acctamort = null; accfamort = null;
    select sum(tamort), sum(famort)
      from amortization AM
      join amperiods AP on (AP.ref = :amperiod and AM.amyear = AP.amyear)
      where AM.fxdasset = :fa_ref and AM.amperiod<=:amperiod
      into :acctamort, :accfamort;

    mtamort = null; mfamort = null;
    select coalesce(sum(tamort),0), coalesce(sum(famort),0)
      from amortization AM
      where AM.fxdasset = :fa_ref and AM.amperiod=:amperiod
        and  coalesce(am.correction,0) = 0
      into :mtamort, :mfamort;

    sumtamort = null; sumfamort = null;
    select coalesce(sum(tamort),0), coalesce(sum(famort),0)
      from amortization AM
      where AM.fxdasset = :fa_ref and AM.amperiod<=:amperiod
      into :sumtamort, :sumfamort;

    suspend;
  end
end*/end^
SET TERM ; ^
