--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_GET_ROUTE(
      SHIPPINGDOC integer)
  returns (
      ROUTE integer,
      ROUTESYMB varchar(20) CHARACTER SET UTF8                           )
   as
begin
  route = 0;
  routesymb = '';

  if (exists(select first 1 1 from rdb$procedures p where p.rdb$procedure_name = 'X_LISTYWYSD_GET_ROUTE')) then
  begin
    execute statement 'execute procedure X_LISTYWYSD_GET_ROUTE('''||:shippingdoc||''')'
    into :route, :routesymb;
    exit;
  end
  else
  begin
    select coalesce(l.trasa,0), coalesce(t.symbol,'')
      from listywysd lwd
        join listywys l on (lwd.listawys = l.ref)
        join trasy t on (l.trasa = t.ref)
      where lwd.ref = :shippingdoc
    into :route, :routesymb;
  end
end^
SET TERM ; ^
