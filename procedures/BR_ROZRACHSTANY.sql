--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_ROZRACHSTANY(
      KONTOFK KONTO_ID,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      COMPANY integer)
  returns (
      ROK integer,
      MIESIAC varchar(20) CHARACTER SET UTF8                           ,
      WINIENZL numeric(14,2),
      MAZL numeric(14,2),
      WALUTA varchar(3) CHARACTER SET UTF8                           ,
      WINIEN numeric(14,2),
      MA numeric(14,2))
   as
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE WALUTOWY SMALLINT;
DECLARE VARIABLE LMA NUMERIC(14,2);
DECLARE VARIABLE LMAZL NUMERIC(14,2);
DECLARE VARIABLE LWINIEN NUMERIC(14,2);
DECLARE VARIABLE LWINIENZL NUMERIC(14,2);
DECLARE VARIABLE DATA TIMESTAMP;
DECLARE VARIABLE LMIES INTEGER;
DECLARE VARIABLE MIES INTEGER;
DECLARE VARIABLE LROK INTEGER;
begin
  select count(*) from ROZRACH where KONTOFK = :kontofk and SYMBFAK = :symbol and company = :company into :cnt;
  if(:cnt <> 1 or (:cnt is null)) then exit;
  select WALUTA, WALUTOWY from ROZRACH where KONTOFK = :kontofk and SYMBFAK = :symbol and company = :company into :waluta, :walutowy;
  if(:walutowy = 0) then waluta = NULL;
  mazl = 0;
  winienzl = 0;
  if(:walutowy = 1) then begin
    ma = 0;
    winien = 0;
  end
  for select DATA, MAZL, WINIENZL, MA, WINIEN from ROZRACHP where
    KONTOFK = :kontofk and SYMBFAK = :symbol and company = :company
    order by DATA
    into :data, :lmazl, :lwinienzl, :lma, :lwinien
  do begin
    rok = extract(year from :data);
    mies = extract(month from :data);
    if(:rok <> :lrok or (:mies <> :lmies))then begin
      if(:lmies = 1) then miesiac = 'styczeń';
      else if(:lmies = 2) then miesiac = 'luty';
      else if(:lmies = 3) then miesiac = 'marzec';
      else if(:lmies = 4) then miesiac = 'kwieceń';
      else if(:lmies = 5) then miesiac = 'maj';
      else if(:lmies = 6) then miesiac = 'czerwiec';
      else if(:lmies = 7) then miesiac = 'lipiec';
      else if(:lmies = 8) then miesiac = 'sierpień';
      else if(:lmies = 9) then miesiac = 'wrzesień';
      else if(:lmies = 10) then miesiac = 'pazdziernik';
      else if(:lmies = 11) then miesiac = 'listopad';
      else if(:lmies = 12) then miesiac = 'grudzień';
      rok = lrok;
      suspend;
      rok = extract(year from :data);
      lmies = :mies;
      lrok = :rok;
    end
    winienzl = :winienzl + :lwinienzl;
    mazl = :mazl + :lmazl;
    winien = :winien + :lwinien;
    ma = :ma + :lma;
  end
  if(:rok is not null) then begin
     if(:mies = 1) then miesiac = 'styczeń';
     else if(:mies = 2) then miesiac = 'luty';
     else if(:mies = 3) then miesiac = 'marzec';
     else if(:mies = 4) then miesiac = 'kwieceń';
     else if(:mies = 5) then miesiac = 'maj';
     else if(:mies = 6) then miesiac = 'czerwiec';
     else if(:mies = 7) then miesiac = 'lipiec';
     else if(:mies = 8) then miesiac = 'sierpień';
     else if(:mies = 9) then miesiac = 'wrzesień';
     else if(:mies = 10) then miesiac = 'pazdziernik';
     else if(:mies = 11) then miesiac = 'listopad';
     else if(:mies = 12) then miesiac = 'grudzień';
     suspend;
  end
end^
SET TERM ; ^
