--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_DOCLABEL_RESPONSE(
      EDEDOCSH_REF integer)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
   as
declare variable shippingdoc integer;
declare variable shippinglabel blob_utf8;
declare variable format string20;
begin
  otable = 'LISTYWYSD';

  select oref
    from ededocsh
    where ref = :ededocsh_ref
  into :shippingdoc;
  oref = :shippingdoc;

  select p.val
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.name = 'format'
  into :format;

  select p.val
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.name = 'etykieta'
  into :shippinglabel;

  update or insert into listywysdopkrpt(doksped, opk, rpt, format)
    values(:shippingdoc,null,:shippinglabel,:format)
    matching (doksped, format);

  suspend;
end^
SET TERM ; ^
