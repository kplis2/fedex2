--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ANAL_MWS_DAY_STATS(
      BDATE date,
      EDATE date,
      WH varchar(3) CHARACTER SET UTF8                           )
  returns (
      DATEOUT date,
      DAYOFWEEKS varchar(100) CHARACTER SET UTF8                           ,
      ACTSOUT integer,
      ACTSOUTSELF integer,
      ACTSUP integer,
      ACTSOUTSHIPPING integer,
      ORDSOUT integer,
      OPERCNTOUT integer,
      WORKSTART timestamp,
      STARTTIME time,
      WORKEND timestamp,
      ENDTIME time,
      WORKTIME varchar(100) CHARACTER SET UTF8                           ,
      PALSIN integer,
      PALSMOVED integer,
      STOCKCNT integer,
      DIST numeric(14,4),
      ALLDIST numeric(14,4),
      ALLDISTPERACT numeric(14,4),
      EMPTYDIST numeric(14,4),
      DISTPERACT numeric(14,4),
      APER numeric(14,4),
      BPER numeric(14,4),
      CPER numeric(14,4))
   as
declare variable dayofweek smallint;
declare variable acnt integer;
declare variable bcnt integer;
declare variable ccnt integer;
begin
  for
    select distinct cast(o.timestop as date)
      from mwsords o
      where o.timestop > :bdate and o.timestop < :edate + 1 and o.wh = :wh
      into dateout
  do begin
    select sum(xk.dist), sum(xk.emptydist), sum(xk.acts), sum(xk.outship),
        sum(xk.outown), sum(xk.palsin), sum(xk.palsmove), sum(xk.up), sum(xk.stock)
      from XK_MWS_DAY_STATS (null,:wh,:dateout,:dateout) xk
      into alldist, emptydist, actsout, actsoutshipping,
        actsoutself, palsin, palsmoved, actsup, stockcnt;
    dist = alldist - emptydist;
    if (emptydist is null) then emptydist = 0;
    select count(distinct o.operator)
      from mwsords o
      where o.timestop > :dateout and o.timestop < :dateout + 1 and o.wh = :wh
      into opercntout;
    if (opercntout is null) then opercntout = 0;
    select count(o.ref), min(o.timestart), max(o.timestop)
      from mwsords o
      where o.timestop > :dateout and o.timestop < :dateout + 1 and o.wh = :wh and o.status = 5
        and o.mwsordtypedest in (1,8,11,15,2,3,9)
      into ordsout, workstart, workend;
    select
        sum(case when c.abc = 'A' then 1 else 0 end),
        sum(case when c.abc = 'B' then 1 else 0 end) ,
        sum(case when c.abc = 'C' then 1 else 0 end)
      from mwsacts a
        left join mwsconstlocs c on (c.ref = a.mwsconstlocp)
      where a.mwsordtypedest in (1,8)
        and a.status = 5 and a.timestop > :dateout and a.timestop < :dateout + 1 and a.wh = :wh
      into acnt,  bcnt, ccnt;
    if (acnt is null) then acnt = 0;
    if (bcnt is null) then bcnt = 0;
    if (ccnt is null) then ccnt = 0;
    if (acnt + bcnt + ccnt = 0) then
    begin
      aper = 0;
      bper = 0;
      cper = 0;
    end
    else
    begin
      aper = 100 * acnt / (acnt + bcnt + ccnt);
      bper = 100 * bcnt / (acnt + bcnt + ccnt);
      cper = 100 * ccnt / (acnt + bcnt + ccnt);
    end
    select max(o.timestop)
      from mwsords o
      where o.timestop > :dateout and o.timestop < :dateout + 1 and o.wh = :wh and o.status = 5
        and o.mwsordtypedest in (1,8,11,15,2,3,9)
      into workend;
    select min(o.timestart)
      from mwsords o
      where o.timestart > :dateout and o.timestop < :dateout + 1 and o.wh = :wh and o.status = 5
        and o.mwsordtypedest in (1,8,11,15,2,3,9)
      into workstart;
    starttime = cast(workstart as time);
    endtime = cast(workend as time);
    select difference from TIMESTAMP_DIFF_TOSTRING(:workstart,:workend)
      into worktime;
    dayofweek = extract(weekday from :dateout);
    if (dayofweek = 0) then dayofweeks = 'niedziela';
    else if (dayofweek = 1) then dayofweeks = 'poniedziałek';
    else if (dayofweek = 2) then dayofweeks = 'wtorek';
    else if (dayofweek = 3) then dayofweeks = 'środa';
    else if (dayofweek = 4) then dayofweeks = 'czwartek';
    else if (dayofweek = 5) then dayofweeks = 'piątek';
    else if (dayofweek = 6) then dayofweeks = 'sobota';
    if (actsout is null or actsout = 0) then
    begin
      distperact = 0;
      alldistperact = 0;
    end
    else
    begin
      distperact = dist/actsout;
      alldistperact = alldist/actsout;
    end
    if (distperact is null) then distperact = 0;
    if (actsout <> 0 or palsin <> 0 or palsmoved <> 0) then
      suspend;
  end
end^
SET TERM ; ^
