--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_DHL_SHIPMENT_RES(
      EDEDOCSH_REF INTEGER_ID)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF INTEGER_ID)
   as
declare variable root_node integer_id;
--identyfikator przesyki, musi byc zawsze uzupelniony, nawet gdy nie otrzymalismy potwierdzenia kuriera
declare variable shipmentno string40;
--identyfikator zamówienia kuriera, uzupelniony jesli udalo sie zalozyc zamowienie i otrzyma sie do niego przypisanie kuriera (jesli pole jest
--puste nalezy wyslac zamowienie samego kuriera na inny okres lub usuniecie zaomwienia i wyslanie na nowo zlecenia)
declare variable ordercuriershipmentno string40;
declare variable filepath string;
declare variable label_node_id string;
declare variable labeltype string;


begin
  --procedura zapisująca numer przesylki z DHL
  select eh.otable, eh.oref from ededocsh eh where eh.ref = :ededocsh_ref
    into :otable, :oref;

  if(lower(coalesce(:otable,'')) != 'listywysd')then
    exception universal 'Podano błędną tabelę: '|| coalesce(:otable,'') || ', gdy oczekiwano LISTYWYSD';
  
  select ep.id from ededocsp ep
    where ep.ededoch = :ededocsh_ref
      and coalesce(ep.name,'') = 'CreateShipmentResponse'
    into :root_node;

  if(coalesce(:root_node,0) = 0)then
  begin
    exception universal 'Nie znaleziono węzła głównego odpowiedzi.';
  end

  select trim(ep.val) from ededocsp ep
    where ep.ededoch = :ededocsh_ref
      and ep.parent = :root_node
      and ep.name = 'shipmentNotificationNumber'
    into :shipmentNo;

  if(coalesce(:shipmentNo,'') = '')then
  begin
    exception universal 'Brak identyfikatora przesyki DHL';
  end

  select trim(ep.val) from ededocsp ep
  where ep.ededoch = :ededocsh_ref
    and ep.parent = :root_node
    and ep.name = 'dispatchNotificationNumber'
  into :ordercuriershipmentno;

  select trim(ep.id) from ededocsp ep
  where ep.ededoch = :ededocsh_ref
    and ep.parent = :root_node
    and ep.name = 'label'
  into :label_node_id;

  select trim(ep.val) from ededocsp ep
  where ep.ededoch = :ededocsh_ref
    and ep.parent = :label_node_id
    and ep.name = 'labelType'
  into :labeltype;

  --Jesli etykieta jest typu BLP - etykieta w formacie pdf, to zapisujemy dla niej plik, jego nazwa jest polu labelContent
 -- if(:labelType = 'BLP') then
 -- begin
    select trim(ep.val) from ededocsp ep
    where ep.ededoch = :ededocsh_ref
      and ep.parent = :label_node_id
      and ep.name = 'labelContent'
    into :filepath;

    UPDATE EDEDOCSH e SET e.filename = :filepath WHERE e.ref = :ededocsh_ref;
 -- end

  update LISTYWYSD lwd
    set lwd.symbolsped = :shipmentNo,
     lwd.statussped = 1,
     lwd.x_ordercuriershipmentno = :ordercuriershipmentno
    where lwd.ref = :oref;
  suspend;
end^
SET TERM ; ^
