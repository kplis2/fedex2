--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_Z3(
      INFOTYPE smallint,
      EMPLOYEE integer,
      ONDATE date,
      COMPANY integer)
  returns (
      FROMDATE date,
      TODATE date,
      DIMNUM smallint,
      DIMDEN smallint,
      WFROMDATE date,
      CODE varchar(20) CHARACTER SET UTF8                           )
   as
declare variable AREF integer;
begin
/*MWr: Procedura generuje dane dla wybranych punktow wydruku zaswiadczenia Z-3.
       Numer punktu okresla zmienna InfoType */

  if (infotype = 1) then
  begin
  --zatrudnienie pracownika

    select min(fromdate) from employment
      where employee = :employee
        and fromdate <= :ondate
      into :fromdate;

    select first 1 fromdate, todate, dimnum, dimden
      from e_get_employmentchanges(:employee, null, null, ';WORKDIM;')
      where fromdate <= :ondate
      order by fromdate desc
      into :wfromdate, :todate, :dimnum, :dimden;

    suspend;
  end else
  ------------------------------------------------------------------------------
  if (infotype = 6) then
  begin
  --wynagrodzenia na podstawie art.92 k.p.

    for
      select e.fromdate, e.todate, z.code
        from eabsences e
          left join edictzuscodes z on (z.ref = e.scode)
        where e.ecolumn in (40,50,60)
          and e.fromdate <= :ondate
          and e.employee = :employee
          and e.correction in (0,2)
          and e.fbase in (select e1.fbase from eabsences e1
                            where e1.ecolumn in (40,50,60,90,100,110,120,140,150,270,280,290,350,360,370,440,450)
                              and e1.employee = :employee
                              and e1.ayear = extract(year from :ondate)
                              and e1.correction in (0,2))
        order by e.fromdate
        into: fromdate, :todate, :code
    do
      suspend;
  end else
  ------------------------------------------------------------------------------
  if (infotype = 7) then
  begin
  --zasilek chorobowy/macierzynski/swiad. rehabilitacyjne

    for
      select r.fromdate, r.todate, r.code
        from rpt_z3_7(:employee, :ondate) r
          left join get_pval(r.fromdate, :company, 'ZPZUS') p on (1 = 1) --BS45839
        where p.ret = 1
        order by r.fromdate
        into: fromdate, :todate, :code
    do
      suspend;
  end else
  ------------------------------------------------------------------------------
  if (infotype = 8) then
  begin
  --zalaczone zaswiadczenie lekarskie

    for
      select first 1 certserial || ' ' || certnumber, coalesce(dokdate,fromdate)
        from eabsences 
        where employee = :employee
          and ecolumn in (40,50,60,90,100,110,120,140,150,160,170,270,280,290,350,360,370,440,450)
          and correction in (0,2)
          and fromdate <= :ondate
          and certserial <> '' and certserial is not null 
        order by fromdate desc
        into :code, :fromdate
    do
      suspend;
  end else
  ------------------------------------------------------------------------------
  if (infotype = 10) then
  begin
  --udzielony urlop (dodatkowy urlop) macierzyski/ojcowski

    select first 1 ref from eabsences
      where employee = :employee
        and ecolumn in (140,150,270,280,290,440,450)
        and correction in (0,2)
        and fromdate <= :ondate
      order by fromdate desc
      into :aref;

    for
      select first 2 afromdate, atodate
        from e_get_whole_eabsperiod(:aref, null, current_date, null, null, null, ';140;150;270;280;290;440;450;')
        where afromdate is not null
        into :fromdate, :todate
    do
      suspend;
  end
end^
SET TERM ; ^
