--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CHECK_INWENTA_STATUS(
      MWSSTOCK integer,
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      LOT integer,
      WH varchar(3) CHARACTER SET UTF8                           )
   as
declare variable zpartiami smallint;
declare variable inwentanad integer;
declare variable dostawa integer;
declare variable cena numeric(14,4);
begin
  zpartiami = null;
  inwentanad = null;
  select first 1 nad.ref, coalesce(nad.zpartiami,0)
    from inwenta nad
    where nad.inwentaparent is null and nad.zamk = 0 and nad.magazyn = :wh
    into inwentanad, zpartiami;
  if (inwentanad is not null) then
  begin
    dostawa = null;
    cena = null;
    if (zpartiami > 0) then
    begin
      dostawa = lot;
      cena = 0;
    end
    if (not exists (select first 1 1 from inwentap p where p.inwenta = :inwentanad
        and p.wersjaref = :vers and (p.dostawa = :lot or :zpartiami = 0)
        and :mwsstock is not null)
    ) then
    begin
      execute procedure INWENTAP_ILPLUS_WYCENA(null,:vers,:wh,:dostawa) returning_values cena;
      insert into INWENTAP(INWENTA,KTM,WERSJAREF,CENA,DOSTAWA,ILSTAN,ILINW,STATUS, BLOKADANALICZ)
        values(:inwentanad, :good, :vers, :cena, :dostawa, 0, 0, 1, 1);
    end else
    begin
      execute procedure mws_inwenta_diff(:inwentanad,:vers,:dostawa);
    end
  end
end^
SET TERM ; ^
