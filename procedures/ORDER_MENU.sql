--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ORDER_MENU(
      RODZIC integer,
      SYMBOL varchar(32) CHARACTER SET UTF8                           ,
      OLDN integer,
      NEWN integer)
   as
declare variable ref integer;
  declare variable number integer;
  declare variable numergl integer;
begin
  --firts we find minor number
  if (oldn < newn) then
    newn = oldn;

  number = newn;
  for
    select ref
      from MENU
      where rodzic = :rodzic and symbol = :symbol and numer >= :number
      order by numer, ord
      into :ref
  do begin
    update MENU set numer = :number, ord = 2
      where ref = :ref;
    number = number + 1;
  end

  update MENU set ord = 1
    where rodzic = :rodzic and numer >= :newn;

  execute procedure ORDERGL_MENU(0,:SYMBOL) returning_values :numergl;
end^
SET TERM ; ^
