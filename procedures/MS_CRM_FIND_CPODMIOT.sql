--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MS_CRM_FIND_CPODMIOT(
      MAIL varchar(255) CHARACTER SET UTF8                           )
  returns (
      PODMIOT_REF integer)
   as
DECLARE VARIABLE ILE INTEGER;
begin
  select count(*)
    from cpodmioty
    where email=:mail or pemail=:mail
    into ile;
  if (:ile=1) then
    select ref
      from cpodmioty
      where email=:mail or pemail=:mail
      into podmiot_ref;
  else
    podmiot_ref=0;
  suspend;
end^
SET TERM ; ^
