--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SHOW_FRDPLANS_VALUE(
      FRDHDRVER integer,
      FRDPERSDIMVAL integer,
      PERSDIMHIER integer,
      DIMELEM integer,
      TIMELEM integer,
      ELEMHIER varchar(40) CHARACTER SET UTF8                           )
  returns (
      PLANAMOUNT numeric(14,2),
      REALAMOUNT numeric(14,2),
      PERCPLANREAL numeric(14,2),
      DEVIATION numeric(14,2))
   as
begin
  select frdcells.planamount, frdcells.realamount, frdcells.percplanreal, frdcells.deviation
    from frdcells
    where frdcells.frdhdrver = :frdhdrver and frdcells.frdpersdimval = :frdpersdimval
      and frdcells.dimelem = :dimelem and frdcells.timelem = :timelem
      and frdcells.elemhier = :elemhier
    into :planamount, :realamount, :percplanreal, :deviation;
end^
SET TERM ; ^
