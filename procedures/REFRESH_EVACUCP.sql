--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REFRESH_EVACUCP(
      ONDATE timestamp,
      EMPLCONTRACT integer)
   as
declare variable vyear integer;
declare variable fdate timestamp;
declare variable tdate timestamp;
declare variable maxvyear integer;
begin
  vyear = extract(year from ondate);
  select fromdate,coalesce(enddate,todate)
    from emplcontracts
    where ref = :emplcontract
    into fdate,tdate;
  if(tdate is null) then
    select cast(max(vyear)||'/1/1' as timestamp)
      from evaclimitsucp
      where emplcontract = :emplcontract
    into tdate;
  if(tdate is null) then
    tdate = current_date;
  maxvyear = extract(year from tdate);
  execute procedure e_vac_card_ucp(:ondate, null,:emplcontract);
  vyear = vyear + 1;
  while (vyear <= maxvyear) do
  begin
   -- exception  test_break''||:vyear;
    execute procedure e_vac_card_ucp(cast(:vyear||'/1/1' as date),null,:emplcontract);
    vyear = vyear + 1;
  end
  delete from evaclimitsucp
    where emplcontract = :emplcontract
      and (vyear < extract(year from :fdate)
        or vyear > extract(year from :tdate));
end^
SET TERM ; ^
