--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_AN_ROZRACHEXP2Y(
      DATA timestamp,
      COMPANY integer)
  returns (
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      SLODEF integer,
      SLOPOZ integer,
      KONTOFK KONTO_ID,
      SYMBFAK varchar(20) CHARACTER SET UTF8                           ,
      SALDO numeric(14,2),
      ISINTEREST integer)
   as
declare variable dataotw timestamp;
    declare variable dataplat timestamp;
    declare variable datazamk timestamp;
    declare variable refnagfak integer;
    declare variable counted integer;
    declare variable startexpdate timestamp;
    declare variable finishexpdate timestamp;

begin
  select fday, datazm
    from datatookres(cast(:data as date), 0, 2, 0, 1)
    into :startexpdate, :finishexpdate;

  for select ROZRACH.SLODEF, ROZRACH.SLOPOZ
    from ROZRACH
    where (cast(rozrach.dataplat as date) >= :data and cast(rozrach.dataplat as date) <= :finishexpdate)
          and ((rozrach.datazamk is null) or (cast(rozrach.datazamk as date) <= :data))
          and ((rozrach.datazamk > rozrach.dataplat) or (rozrach.datazamk is null))
          and company = :company
    group by rozrach.slodef, rozrach.slopoz
    into :slodef, :slopoz
  do begin
    --nagówek dokument
    select nazwa from slo_dane(:slodef, :slopoz) into :nazwa;

    for
      select SYMBFAK, KONTOFK, dataotw, dataplat, datazamk, faktura, saldowm
        from ROZRACH
        where SLODEF = :slodef and slopoz = :slopoz
           and (cast(dataplat as date) >= :data and cast(dataplat as date) <= :finishexpdate)
           and ((datazamk is null) or (cast(datazamk as date) <= :data))
           and ((datazamk > dataplat) or (datazamk is null))
           and company = :company
        into :symbfak, :kontofk, :dataotw, :dataplat, :datazamk, :refnagfak, :saldo
    do begin
      isinterest = 0;
      if (datazamk is not null) then
        if (exists(select ref from ROZRACHP
                        where slodef = :slodef and slopoz = :slopoz
                          and symbfak = :symbfak and kontofk = :kontofk and company = :company
                          and notadokum is null)) then
        begin

          if (saldo = 0) then
            isinterest = 1;
          else isinterest =0;
          suspend;
        end
        else counted = 0;
      else suspend;
    end
  end
end^
SET TERM ; ^
