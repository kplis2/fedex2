--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CREATE_DOKUMNAG(
      MAGAZYN char(3) CHARACTER SET UTF8                           ,
      TYP char(3) CHARACTER SET UTF8                           ,
      DOSTAWCA integer,
      KLIENT integer,
      MAG2 char(3) CHARACTER SET UTF8                           ,
      HISTORIA integer,
      ZLECREF integer,
      ZLECNAZWA varchar(60) CHARACTER SET UTF8                           ,
      UWAGI varchar(255) CHARACTER SET UTF8                           ,
      SRVREQUEST integer)
  returns (
      DOKUMREF integer)
   as
declare variable magtyp char(1);
declare variable magwydania smallint;
declare variable magzewn smallint;
declare variable dostawa integer;
begin
  execute procedure GEN_REF('DOKUMNAG') returning_values :dokumref;
  select TYP from DEFMAGAZ where SYMBOL=:magazyn into :magtyp;
  select WYDANIA,zewn from DEFDOKUM where SYMBOL = :typ into :magwydania,:magzewn;
    /*zalozenie dostawy*/
  dostawa = NULL;
/*  if(:magwydania = 0 and :magtyp = 'P') then begin
    execute procedure DOKUMNAG_CREATE_DOKMAG_DOSTAWA(:dokumref,:dostawca,current_date,:magazyn,NULL,NULL,NULL,NULL)
    returning_values :dostawa;
  end else dostawa = 0;*/
  insert into DOKUMNAG(REF,MAGAZYN, TYP, DATA, DOSTAWA, DOSTAWCA, KLIENT,AKCEPT,
        BLOKADA,MAG2, HISTORIA, ZLECREF, ZLECNAZWA, UWAGI, SRVREQUEST)
    values (:dokumref,:magazyn, :typ, current_date, :dostawa,:dostawca,:klient,0,
         0, :mag2, :historia, :zlecref, :zlecnazwa, :uwagi, :srvrequest);

end^
SET TERM ; ^
