--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ZAMJEDNO_ZMIANA(
      REFZAM integer,
      JEDNO varchar(5) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE JEDN INTEGER;
DECLARE VARIABLE NEWTOWJEDN INTEGER;
DECLARE VARIABLE POZREF INTEGER;
DECLARE VARIABLE KTM VARCHAR(20);
DECLARE VARIABLE PRZELJED NUMERIC(14,4);
DECLARE VARIABLE PRZELZAM NUMERIC(14,4);
DECLARE VARIABLE CENACEN NUMERIC(14,4);
DECLARE VARIABLE ILOSCM NUMERIC(14,4);
DECLARE VARIABLE ildozam NUMERIC(14,4);
DECLARE VARIABLE reszta NUMERIC(14,4);
DECLARE VARIABLE zaokraglenie NUMERIC(14,4);
DECLARE VARIABLE licznik NUMERIC(14,4);
begin
  select count(*) from pozzam where pozzam.zamowienie = :REFZAM into :cnt;
  if (:cnt > 0) then
  begin
    for select ref, ktm, jedn, iloscm, cenacen from pozzam where pozzam.zamowienie = :refzam
      into :pozref, :ktm, :jedn, :iloscm, :cenacen
    do begin
      select przelicz from towjedn where towjedn.REF = :JEDN
        into :przelzam;
      select przelicz, ref from towjedn where towjedn.ktm = :ktm and towjedn.jedn = :JEDNO
        into :przeljed, :newtowjedn;
      if (:przeljed is null or (:przeljed = 0) ) then
        exception universal 'towar '||:ktm||' nie posiada zadanej jednostki';
      else begin
        update pozzam set ilosc = cast (:iloscm/:przeljed as integer), jedn = :newtowjedn,
          cenacen = cenacen*(:przeljed/:przelzam) where ref = :pozref;
      end
    end
  end
end^
SET TERM ; ^
