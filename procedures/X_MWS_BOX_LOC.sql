--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_BOX_LOC(
      SPED_DOC INTEGER_ID)
  returns (
      STATUS INTEGER_ID,
      KODKRESK STRING100,
      PALETE SMALLINT_ID)
   as
declare variable opk_ref integer_id;
begin
status  = 1;
  select first 1 opk.ref, opk.stanowisko, opk.typ -- wyszukuje opakowania ktore zostaly otworzone podczas pakowania
    from listywysdroz_opk opk
    where opk.listwysd = :SPED_DOC
      and opk.x_moved = 1
  into :opk_ref, :kodkresk, :palete;

  if (:opk_ref is null) then
    status = 0;
  suspend;
end^
SET TERM ; ^
