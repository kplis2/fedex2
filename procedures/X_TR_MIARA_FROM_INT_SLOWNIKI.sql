--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_TR_MIARA_FROM_INT_SLOWNIKI as
declare variable okey string60;
declare variable wartosc string;
declare variable wartoscid string;
declare variable miara string10;
declare variable zrodlo integer;
declare variable oref integer;
begin
  for
    select s.zrodlo, s.okey, s.oref, s.wartosc, s.wartoscid
      from int_slownik s
      where s.otable = 'MIARA'
    into :zrodlo, :okey, :oref, :wartosc, :wartoscid
  do begin


    okey = trim(okey);
    okey = replace(okey,  ' ','');
    okey = substring(okey from 1 for 5);

    if (not exists(select first 1 1 from miara m where m.miara = :okey)) then begin

      insert into miara  (miara, opis, dokladnosc, id_zewn)
        values (:okey, :wartosc, 2, :wartoscid);
    end else begin
      select miara from miara where miara = :okey into :miara;
      update int_slownik s set s.okey = :miara where s.zrodlo = :zrodlo and s.okey = :okey and s.oref = :oref and s.wartosc = :wartosc and s.wartoscid = :wartoscid;
      miara = null;
    end
  end
end^
SET TERM ; ^
