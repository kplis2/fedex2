--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_16(
      BKDOC integer)
   as
declare variable account account_id;
  declare variable amount numeric(14,2);
  declare variable curramount numeric(14,2);
  declare variable tmp varchar(255);
  declare variable settlement varchar(20);
  declare variable docdate timestamp;
  declare variable payday timestamp;
  declare variable descript varchar(255);
  declare variable dictpos integer;
  declare variable dictdef integer;
  declare variable currency varchar(3);
  declare variable currate numeric(15,5);
  declare variable otable varchar(31);
  declare variable oref integer;
  declare variable branch varchar(10);
  declare variable spr varchar(20);
  declare variable odd varchar(10);
  declare variable sumprocentplat numeric(14,2);
  declare variable crnote smallint;
  declare variable fsopertype smallint;
begin
  -- DEMO: standardowy schemat dekretowania - eksport

  select B.sumgrossv, B.symbol, B.docdate, B.payday, B.descript,
      B.dictdef, B.dictpos, B.currency, B.currate, B.otable, B.oref,
      B.branch,O.symbol, S.kontofk, t.creditnote
    from bkdocs B
        left join nagfak N on (B.oref = N.ref)
        left join sprzedawcy S on (S.ref = N.sprzedawca)
        left join oddzialy O on (N.oddzial = O.oddzial)
        left join bkdoctypes T on (B.doctype = T.ref)
    where B.ref = :bkdoc
    into :amount, :settlement, :docdate, :payday, :descript,
      :dictdef, :dictpos, :currency, :currate, :otable, :oref,
      :branch, :odd, :spr, :crnote;

  if (spr is null) then
    spr = '01';

  if (crnote = 1) then
    fsopertype = 2;
  else
    fsopertype = 1;

  select sum(currnetv) from bkvatpos where bkdoc = :bkdoc into :curramount;

  execute procedure KODKS_FROM_DICTPOS(:dictdef, :dictpos) returning_values tmp;
  account = '202-' || tmp;

  execute procedure fk_autodecree_termplat(bkdoc,account, amount, curramount, currate, currency, 1 , settlement,
                                          oref, otable,fsopertype,docdate,
                                          payday,descript)
      returning_values sumprocentplat;
    for
      select netv, descript
        from bkvatpos where bkdoc = :bkdoc
        into :amount, :descript
    do begin
      execute procedure insert_decree_dists(bkdoc, '702-02', 1, amount, descript, null, null, null, null, null, null, null, null, null, null, 0);
    end

end^
SET TERM ; ^
