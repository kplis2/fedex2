--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAKEVACPLAN_INS(
      S varchar(24) CHARACTER SET UTF8                           ,
      D varchar(2) CHARACTER SET UTF8                           ,
      POS integer)
  returns (
      S2 varchar(24) CHARACTER SET UTF8                           )
   as
begin
  if (:pos=1) then s2 = d||substring(s from 3 for 22);
  else if (:pos=12) then s2 = substring(s from 1 for 22)||d;
  else s2 = substring(s from  1 for  pos*2-2)||d||substring(s from  pos*2+1 for 24);
  suspend;
end^
SET TERM ; ^
