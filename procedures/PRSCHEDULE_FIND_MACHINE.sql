--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDULE_FIND_MACHINE(
      PRGANTTREF integer,
      TSTART timestamp,
      TRYB smallint)
  returns (
      STARTTIME timestamp,
      ENDTIME timestamp,
      MACHINE integer)
   as
declare variable workplace varchar(20);
declare variable worktime double precision;
declare variable minspace integer;
declare variable minlength double precision;
declare variable prdepart varchar(20);
declare variable tmpendtime timestamp;
declare variable s1 timestamp;
declare variable s2 timestamp;
declare variable s3 timestamp;
declare variable m1 integer;
begin
  select g.prmachine, g.worktime, g.workplace, p.mintimebetweenopers, g.prdepart
    from prgantt g
      left join prschedules p on (p.ref = g.prschedule)
    where g.ref = :prganttref
    into :machine, :worktime, :workplace, :minspace, :prdepart;
  minlength = 1440;
  minlength = coalesce(minspace,0)/:minlength;
  starttime = :tstart;
  endtime = :tstart;
  for
    select p.rmachine, p.rstart, p.rend
      from PRSCHEDULE_FREE_MACHINE_avg(:workplace, :machine, :tryb, :tstart) p
      where (cast(p.rend - :starttime as double precision) >= :worktime or p.rend is null)
      order by p.rstart
      into :m1, :s1, :s2
  do begin
    if (:s1 < :tstart) then
      s1 = :tstart;
    --przesuniecie poczatku na dzien pracujacy
    execute procedure PRSCHED_CALENDAR_FIRSTWORKTIME(:prdepart,cast(:s1 as timestamp))
      returning_values :s3;--:starttime;
    --zaokrąaglenie i odsuniecie od poprzedniej
    execute procedure prschedule_checkoperstarttime(:prganttref,:s3)
      returning_values :s3;
    if (:s2 is null or cast(:s2 - :s3 as double precision) - :minlength >= :worktime) then--czas zakonczenia przerwy nie jest okreslony lub potencjalnei sie miesci
    begin
      execute procedure pr_calendar_checkendtime(:prdepart, :s3, :worktime)
        returning_values :tmpendtime;
      if (:tmpendtime <= :s2 or :s2 is null) then
      begin
        s2 = :tmpendtime;
        machine = :m1;
        starttime = :s3;
        endtime = :s2;
         suspend;
      end
    end 
  end
end^
SET TERM ; ^
