--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAV_TOWTYPES
  returns (
      REF varchar(40) CHARACTER SET UTF8                           ,
      PARENT integer,
      NUMBER integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      HINT varchar(255) CHARACTER SET UTF8                           ,
      KINDSTR varchar(20) CHARACTER SET UTF8                           ,
      ACTIONID varchar(60) CHARACTER SET UTF8                           ,
      APPLICATIONS varchar(60) CHARACTER SET UTF8                           ,
      FORMODULES varchar(60) CHARACTER SET UTF8                           ,
      RIGHTS varchar(255) CHARACTER SET UTF8                           ,
      RIGHTSGROUP varchar(255) CHARACTER SET UTF8                           ,
      ISNAVBAR smallint,
      ISTOOLBAR smallint,
      ISWINDOW smallint,
      ISMAINMENU smallint,
      ITEMTYPE smallint,
      GROUPPROCEDURE varchar(60) CHARACTER SET UTF8                           ,
      CHILDRENCOUNT smallint,
      RIBBONTAB varchar(40) CHARACTER SET UTF8                           ,
      RIBBONGROUP varchar(40) CHARACTER SET UTF8                           ,
      NATIVETABLE varchar(40) CHARACTER SET UTF8                           ,
      POPUPMENU varchar(40) CHARACTER SET UTF8                           ,
      NATIVEFIELD varchar(40) CHARACTER SET UTF8                           ,
      BEGINGROUP smallint,
      FORADMIN smallint)
   as
begin
  hint = '';
  actionid = 'BrowseAsortyment';
  isnavbar = 0;
  istoolbar = 1;
  iswindow = 0;
  ismainmenu = 0;
  itemtype = 0;
  childrencount = 0;
  ribbontab = '';
  ribbongroup = '';
  nativetable = '';
  popupmenu = '';
  groupprocedure = '';
  applications = '';
  formodules = '';
  nativefield = 'NUMER';
  foradmin = 0;
--  kindstr = 'MI_PROGRAMY';
  parent = NULL;
  number = 1;
  ref = 'TOWYPESALL';
  name = 'Wszystko';
  kindstr = 'MI_TOWARY';
  rights = '';
  rightsgroup = '';
  begingroup = 0;
  suspend;
  begingroup = 1;
  actionid = 'BrowseTowaryFromTowtypes';
  nativetable = 'TOWTYPES';
  number = 2;
  for select NUMER, NAZWA, KIND, RIGHTS, RIGHTSGROUP
  from TOWTYPES
  order by NUMER
  into :REF, :NAME, :KINDSTR, :RIGHTS, :RIGHTSGROUP
  do begin
    suspend;
    number = :number + 1;
    begingroup = 0;
  end
end^
SET TERM ; ^
