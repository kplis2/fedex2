--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_ZLOGI_TERM(
      LMAG varchar(255) CHARACTER SET UTF8                           )
  returns (
      MAGRET varchar(3) CHARACTER SET UTF8                           ,
      KTMRET varchar(40) CHARACTER SET UTF8                           ,
      VERRET integer,
      ZLOGI1 numeric(14,2),
      ZLOGI2 numeric(14,2),
      ZLOGI3 numeric(14,2),
      ZLOGI4 numeric(14,2))
   as
declare variable symmag varchar(3);
declare variable ktm varchar(40);
declare variable wersja integer;
begin
   --exception test_break substring(:lmag from 1 for 40);
   for select symbol from defmagaz
        where :LMAG like '%;'||symbol||';%'
   into :SYMMAG
   do begin
      for select RKTM, rwersja from MAG_STANY(:SYMMAG,NULL,NULL,NULL,NULL,current_Date,0,0)
              where mag_Stany.stan > 0
          into :ktm, :wersja
      do begin
                exception test_break ktm;
          select ZLOGIWART from mag_zlogi(:SYMMAG,current_Date, 0, :ktm, :wersja) into zlogi1;
          select ZLOGIWART from mag_zlogi(:SYMMAG,current_Date, 90, :ktm, :wersja) into zlogi2;
          select ZLOGIWART from mag_zlogi(:SYMMAG,current_Date, 180, :ktm, :wersja) into zlogi3;
          select ZLOGIWART from mag_zlogi(:SYMMAG,current_Date, 360, :ktm, :wersja) into zlogi4;
      zlogi3 = zlogi3 - zlogi2;
      zlogi4 = zlogi4 - zlogi3;
      zlogi1 = zlogi1 - zlogi4;
      magret = symmag;
      ktmret = ktm;
      verret = wersja;
      suspend;
      end
   end
end^
SET TERM ; ^
