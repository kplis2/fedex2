--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRNAGZAM_CREATEFROMPRSCH_EXACT(
      PLANVAL numeric(14,2),
      PLANDYSP numeric(14,2),
      PLANREAL numeric(14,2),
      PLANFROM timestamp,
      PLANTO timestamp,
      PRSCHEDULEFROM timestamp,
      PRSCHEDULETO timestamp)
  returns (
      ILOSC numeric(14,2))
   as
declare variable st numeric(14,2);
declare variable maxfrom timestamp;
declare variable minto timestamp;
begin
/*okreslenie okresu produkcji - wezszy z harmonogramu i kolumny planu*/
  if(:prschedulefrom > :planfrom) then maxfrom = :prschedulefrom;
  else maxfrom = :planfrom;
  if(:prscheduleto < :planto) then minto = :prscheduleto;
  else minto = :planto;
/*okreslenie do wyprodukowania - ilosc planowana - zrealizowana w okresie*/
  st = cast((:minto - :maxfrom + 1)/(:planto - :planfrom + 1) as numeric(14,2));
  ilosc = (:planval - :plandysp - :planreal) * st;
  suspend;
  suspend;
end^
SET TERM ; ^
