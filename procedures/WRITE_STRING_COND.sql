--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WRITE_STRING_COND(
      SECTION varchar(100) CHARACTER SET UTF8                           ,
      IDENT varchar(50) CHARACTER SET UTF8                           ,
      VAL varchar(1024) CHARACTER SET UTF8                           ,
      CHECKCUSTOMIZED smallint,
      PREFIX varchar(20) CHARACTER SET UTF8                            = null)
   as
declare variable cnt integer;
begin
  if(:prefix='') then prefix = null;
  if(:checkcustomized=1) then begin
    select count(*) from S_APPINI where SECTION=:section AND IDENT='Customized'
      and coalesce(prefix,'') = coalesce(:prefix,'') into :cnt;
    if(:cnt is null) then cnt = 0;
    if(:cnt=0) then begin
      execute procedure WRITE_STRING(SECTION,IDENT,VAL, :prefix);
    end
  end else begin
    execute procedure WRITE_STRING(SECTION,IDENT,VAL, :prefix);
  end
end^
SET TERM ; ^
