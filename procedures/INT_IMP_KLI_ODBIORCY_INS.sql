--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_KLI_ODBIORCY_INS(
      KLIENTID type of column INT_IMP_KLI_ODBIORCY.KLIENTID,
      ODBIORCAID type of column INT_IMP_KLI_ODBIORCY.ODBIORCAID,
      GLOWNY type of column INT_IMP_KLI_ODBIORCY.GLOWNY,
      NAZWA type of column INT_IMP_KLI_ODBIORCY.NAZWA,
      NAZWAFIRMY type of column INT_IMP_KLI_ODBIORCY.NAZWAFIRMY,
      IMIE type of column INT_IMP_KLI_ODBIORCY.IMIE,
      NAZWISKO type of column INT_IMP_KLI_ODBIORCY.NAZWISKO,
      KRAJ type of column INT_IMP_KLI_ODBIORCY.KRAJ,
      KRAJID type of column INT_IMP_KLI_ODBIORCY.KRAJID,
      ULICA type of column INT_IMP_KLI_ODBIORCY.ULICA,
      NRDOMU type of column INT_IMP_KLI_ODBIORCY.NRDOMU,
      NRLOKALU type of column INT_IMP_KLI_ODBIORCY.NRLOKALU,
      KODPOCZTOWY type of column INT_IMP_KLI_ODBIORCY.KODPOCZTOWY,
      MIASTO type of column INT_IMP_KLI_ODBIORCY.MIASTO,
      TELEFON type of column INT_IMP_KLI_ODBIORCY.TELEFON,
      EMAIL type of column INT_IMP_KLI_ODBIORCY.EMAIL,
      WWW type of column INT_IMP_KLI_ODBIORCY.WWW,
      HASH type of column INT_IMP_KLI_ODBIORCY.HASH,
      DEL type of column INT_IMP_KLI_ODBIORCY.DEL,
      REC type of column INT_IMP_KLI_ODBIORCY.REC,
      SESJA type of column INT_IMP_KLI_ODBIORCY.SESJA,
      SKADTABELA type of column INT_IMP_KLI_ODBIORCY.SKADTABELA,
      SKADREF type of column INT_IMP_KLI_ODBIORCY.SKADREF)
  returns (
      REF type of column INT_IMP_KLI_ODBIORCY.REF)
   as
begin
  insert into int_imp_kli_odbiorcy (klientid, odbiorcaid, glowny, nazwa, nazwafirmy,
    imie, nazwisko, kraj, krajid, ulica, nrdomu, nrlokalu, kodpocztowy, miasto,
    telefon, email, www, "HASH", del, rec, sesja, skadtabela,skadref)
  values (:klientid, :odbiorcaid, :glowny, :nazwa, :nazwafirmy,
    :imie, :nazwisko, :kraj, :krajid, :ulica, :nrdomu, :nrlokalu, :kodpocztowy,
    :miasto, :telefon, :email, :www, :"HASH", :del, :rec, :sesja, :skadtabela, :skadref)
  returning ref into :ref;
end^
SET TERM ; ^
