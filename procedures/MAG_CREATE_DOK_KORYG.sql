--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_CREATE_DOK_KORYG(
      DOKREF integer,
      DATA timestamp,
      INWENT smallint)
  returns (
      STATUS integer,
      NEWDOK integer,
      NEWDOKSYM varchar(20) CHARACTER SET UTF8                           )
   as
declare variable NEWTYP varchar(3);
declare variable CNT integer;
begin
  if (:inwent is null) then
    inwent = 0;
  status = 0;
  select DEFDOKUM.KORYGUJACY from DEFDOKUM join DOKUMNAG on (DEFDOKUM.SYMBOL = DOKUMNAG.typ) where DOKUMNAG.REF = :dokref into :newtyp;
  if(:newtyp is null or (:newtyp = '')) then exception DOK_KORYG_BRAKDOKTYP;
  /*sprawdzenie, czy sa dokumenty korygujace do danego, nei zaakceptowane*/
  select count(*) from DOKUMNAG where REFK = :dokref and dokumnag.akcept = 0 into :cnt;
  if(:cnt > 0) then
    exception DOK_KORYG_SAKORNIEACK;
  execute procedure GEN_REF('DOKUMNAG') returning_values :newdok;
  insert into DOKUMNAG(REF,TYP,MAGAZYN,MAG2,DATA,REFK,SYMBOLK,SLODEF,SLOPOZ,KLIENT,DOSTAWCA,ZLECNAZWA,UWAGI, INWENTAAFTER, PMPLAN, PMELEMENT, PMPOSITION, GRUPASPED, ODBIORCA, ODBIORCAID)
  select :newdok, :newtyp,  MAGAZYn,MAG2, :data, REF, SYMBOL,SLODEF,SLOPOZ,KLIENT,DOSTAWCA,ZLECNAZWA,UWAGI, :inwent, PMPLAN, PMELEMENT, PMPOSITION, GRUPASPED, ODBIORCA, ODBIORCAID from DOKUMNAG where REF=:dokref;
  select symbol from DOKUMNAG where ref=:newdok into :newdoksym;
  status = 1;
  suspend;
end^
SET TERM ; ^
