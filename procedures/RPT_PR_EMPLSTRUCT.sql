--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PR_EMPLSTRUCT(
      DATA date,
      COMPANY integer)
  returns (
      STANOWISKO varchar(75) CHARACTER SET UTF8                           ,
      SUMA integer,
      SUMA2 integer,
      OGOLEM integer)
   as
declare variable sid integer;
begin
  for
    select workpost, ref
      from edictworkposts
      order by workpost
      into :stanowisko, :sid
  do begin
    select count(distinct p.ref)
      from edictworkposts  EW
        join employment EMP on (EW.ref= EMP.workpost)
        join employees EP on (EMP.employee = EP.ref)
        join persons P on (EP.person = P.ref)
      where :data >= EMP.fromdate and (:data <= EMP.todate or EMP.todate is null)
        and EMP.workpost = :sid and P.sex = 1 and ep.company = :company
        and ep.empltype=1
      into :suma;
    suma=coalesce(:suma, 0);


    select count(distinct p.ref)
      from edictworkposts  EW
        join EMPLOYMENT EMP ON (EW.ref= EMP.workpost)
        join employees ep on (EMP.employee = EP.ref)
        join persons p on (EP.person = P.ref)
      where :data >= EMP.fromdate and (:data <= EMP.todate or EMP.todate is null)
        and EMP.workpost = :sid and P.sex=0  and ep.company = :company
        and ep.empltype=1
      into :suma2;
    suma2=coalesce(:suma2, 0);
    ogolem = suma + suma2;
    suspend;
  end
end^
SET TERM ; ^
