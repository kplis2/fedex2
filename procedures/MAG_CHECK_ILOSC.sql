--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_CHECK_ILOSC(
      ZAM integer,
      MAG char(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      CENAMAG numeric(14,4),
      DOSTAWA integer)
  returns (
      ILOSC numeric(14,4))
   as
declare variable wersjaref integer;
begin
  select ref from wersje where ktm = :ktm and nrwersji = :wersja
    into wersjaref;
  execute procedure mag_checkilwol_c(:zam,null,:mag,:wersjaref,'Z',:dostawa,:cenamag)
    returning_values :ilosc;
end^
SET TERM ; ^
