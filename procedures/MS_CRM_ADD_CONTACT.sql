--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MS_CRM_ADD_CONTACT(
      OPERATOR_REF integer,
      CPODMIOT_REF integer)
  returns (
      KONTAKT_REF integer)
   as
DECLARE VARIABLE REF INTEGER;
DECLARE VARIABLE INOUT SMALLINT;
DECLARE VARIABLE DATA TIMESTAMP;
DECLARE VARIABLE RODZAJ INTEGER;
DECLARE VARIABLE TWORZNOTATKE SMALLINT;
DECLARE VARIABLE PRZECZYTANO SMALLINT;
DECLARE VARIABLE CPODMIOT INTEGER;
DECLARE VARIABLE OPERATOR INTEGER;
DECLARE VARIABLE CPODMSKROT VARCHAR(40);
DECLARE VARIABLE OPER_IMIE VARCHAR(40);
DECLARE VARIABLE OPER_NAZWISKO VARCHAR(80);
DECLARE VARIABLE NAZWA VARCHAR(80);
begin
  inout=1;   --przychodzaca wiadomosc
  execute procedure getconfig('MSERVCONTACTRODZAJ')
    returning_values :rodzaj;  --typ wioadomosci: email
  tworznotatke=0; --nie tworzy notatki
  przeczytano=0;  --nieprzeczytano
  data=current_date;  --data
  execute procedure gen_ref('kontakty')
    returning_values :ref;
   cpodmiot=cpodmiot_ref;
  operator=operator_ref;
  select skrot
    from cpodmioty
    where ref=:cpodmiot
    into cpodmskrot;
  nazwa=:data||' '||'email od'||:cpodmskrot||'('||:oper_imie||' '||:oper_nazwisko||')';
  insert into kontakty(ref, data, inout, rodzaj, tworznotatke, przeczytano, nazwa, operator, cpodmiot, cpodmskrot, plik)
    values(:ref, :data, :inout, :rodzaj, :tworznotatke,  :przeczytano, :nazwa,  :operator,  :cpodmiot,  :cpodmskrot, :ref||'.eml');
  kontakt_ref=:ref;
  suspend;
end^
SET TERM ; ^
