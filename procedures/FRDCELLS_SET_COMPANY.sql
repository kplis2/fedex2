--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRDCELLS_SET_COMPANY(
      FRHDR varchar(20) CHARACTER SET UTF8                           )
   as
declare variable dimelem integer;
declare variable elemhier varchar(40);
declare variable company integer;
declare variable frdhdrver integer;
begin
  for
    select distinct frdcells.dimelem, frdcells.elemhier, dimelemdef.company, frdcells.frdhdrver
      from frdcells
        left join frdhdrsvers on (frdhdrsvers.ref = frdcells.frdhdrver)
        left join frdhdrs on (frdhdrs.ref = frdhdrsvers.frdhdr)
        left join dimelemdef on (dimelemdef.ref = frdcells.dimelem)
      where frdhdrs.frhdr = :frhdr and dimelemdef.company is not null
      into :dimelem, :elemhier, company, :frdhdrver
  do begin
    if (:company is not null and :company > 0) then
      update frdcells set frdcells.company = :company
        where frdcells.frdhdrver = :frdhdrver and frdcells.elemhier like :elemhier||'%';
  end
end^
SET TERM ; ^
