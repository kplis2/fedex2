--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PLANS_KTM_FROM_TO(
      KTM varchar(80) CHARACTER SET UTF8                           ,
      DATEFROM timestamp,
      DATETO timestamp)
  returns (
      INCOME numeric(14,2),
      EXPEND numeric(14,2))
   as
declare variable plansincome numeric(14,2);
declare variable plansexpend numeric(14,2);
declare variable planscolsref integer;
declare variable bdate timestamp;
declare variable ldate timestamp;
declare variable termin integer;
begin
  income = 0;
  expend = 0;
  for select pc.bdata, pc.ldata, pc.ref
   from plans p
   join planscol pc on (p.ref = pc.plans)
   join plansrow pr on (p.ref = pr.plans)
   where p.status = 1 and pc.akt=1
         and pc.bdata<:dateto and pc.ldata>:datefrom
         and pr.key1 = :ktm
   into :bdate, :ldate, :planscolsref
  do begin
    execute procedure plans_ktm_amount(:KTM, :planscolsref) returning_values :plansincome, :plansexpend;
    if(:plansincome is null) then plansincome = 0;
    if(:plansexpend is null) then plansexpend = 0;
    termin = :ldate - :bdate + 1;
    if(:datefrom>:bdate) then termin = :termin - (:datefrom-:bdate);
    if(:dateto<:ldate) then termin = :termin - (:ldate-:dateto);
    plansincome = :plansincome * termin / (:ldate - :bdate+1);
    plansexpend = :plansexpend * termin / (:ldate - :bdate+1);
    if(:plansincome < 0) then plansincome = 0;
    if(:plansexpend < 0) then plansexpend = 0;
    income = :income + :plansincome;
    expend = :expend + :plansexpend;
    if(floor(:income)<:income) then income = floor(:income) + 1;
    if(floor(:expend)<:expend) then expend = floor(:expend) + 1;
    plansincome = 0;
    plansexpend = 0;
    planscolsref = 0;
  end
  suspend;
end^
SET TERM ; ^
