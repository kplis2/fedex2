--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_PENALTY(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      AMOUNT integer)
   as
begin
  --DS: personel - rozliczanie kar porzadkowych
  select sum(a.amount)
    from empladdfiles a
    where a.filetype = 5 --kara
      and a.kind = 2     --pieniezna
      and (a.epayroll is null or a.epayroll = :payroll)
      and a.employee = :employee
    into :amount;
  amount = coalesce(amount,0);

  update empladdfiles a set a.epayroll = :payroll
    where a.employee = :employee
      and a.filetype = 5 --kara
      and a.kind = 2     --pieniezna
      and a.epayroll is null;
  suspend;
end^
SET TERM ; ^
