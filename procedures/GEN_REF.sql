--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GEN_REF(
      TABLENAME varchar(31) CHARACTER SET UTF8                           )
  returns (
      ID integer)
   as
declare variable id2 bigint;
begin
  -- DU: pobiera kolejny numer z generatora do wykorzystania w polu REF
  execute statement 'select gen_id(GEN_' || tablename
      || ', 1) from rdb$database'
    into :id2;
  execute procedure RP_CHECK_REF(:tablename, :id2)
    returning_values :id;
  suspend;
end^
SET TERM ; ^
