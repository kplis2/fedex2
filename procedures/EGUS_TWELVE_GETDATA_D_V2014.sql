--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EGUS_TWELVE_GETDATA_D_V2014(
      GYEAR varchar(4) CHARACTER SET UTF8                           ,
      EMPLOYEE integer)
  returns (
      D1 numeric(4,3),
      D2 numeric(14,2),
      D3 numeric(14,2),
      D4 numeric(14,2),
      D5 numeric(14,2),
      D6 numeric(14,2),
      D7 numeric(14,2),
      D8 numeric(14,2),
      D9 integer,
      D10 numeric(14,2),
      D11 numeric(14,2),
      D12 numeric(14,2),
      D13 numeric(14,2),
      D14 numeric(14,2),
      D15 numeric(14,2),
      D16 numeric(14,2),
      D17 numeric(14,2))
   as
declare variable PERIOD varchar(6);
declare variable PNOMWORKDAYS integer;
declare variable FROMDATE date;
declare variable TODATE date;
declare variable WDIMDAYS numeric(14,2);
declare variable PWDIMDAYS numeric(14,2);
declare variable BRUTTO numeric(14,2);
declare variable OUTBRUTTO numeric(14,2);
declare variable D2TMP numeric(14,2);
declare variable VC integer;
declare variable VC2 integer;
declare variable WORKDIM float;
declare variable DIMNUM smallint;
declare variable DIMDEN smallint;
declare variable PROPORTIONAL smallint;
declare variable FROMDATEPART timestamp;
declare variable TODATEPART timestamp;
declare variable ISCALEND smallint;
declare variable PERSONNAMES varchar(120);
declare variable WS_PART integer;
declare variable WS integer;
declare variable CALENDAR integer;
begin
/*MatJ Personel: procedura generuje dane do sprawozdania GUS Z-12 (na rok 2014)
  dla zadanego pracownika EMPLOYEE. Odpowiada za czesc D raportu - czas pracy
  oraz wynagr. za rok GYEAR. Specyfikacja zmiennych wyjsciowych w dokumenacji*/

  d2tmp = 0;
  wdimdays = 0;
  period = gyear || '01';

  fromdate = cast(gyear || '/1/1' as date);
  todate = cast(gyear || '/12/31' as date);

/*D1: wymiaru czasu pracy za rok GYEAR oraz oraz
  D2: liczba dni roboczych w roku GYEAR */
    select wdimdays, nomworkdays
      from egus_twelve_worktime(1, :employee, :fromdate, :todate)
      into :wdimdays, :d2;

  if (d2 > 0) then
    d1 = (12.00 * wdimdays) / (1.00 * d2);

/*D3: Roczna liczba godzin obowiązkowego pelnego wymiaru pracy */

  if (not(exists(select first 1 1 from emplcalendar
               where employee = :employee and fromdate <= :todate))
  ) then begin
    select personnames from employees where ref = :employee
      into :personnames;
    exception brak_kalendarza 'Brak przypisanego kalendarza dla pracownika: ' || personnames;
  end

  ws = 0;
  for
    select fromdate, todate, workdim, dimnum, dimden, proportional
      from e_get_employmentchanges(:employee, :fromdate, :todate, ';DAYWORKDIM;WORKDIM;PROPORTIONAL;')
      into :fromdatepart, :todatepart, :workdim, :dimnum, :dimden, proportional
  do begin
    for
      select calendar
        from emplcalendar where employee = :employee and fromdate <= :todatepart and coalesce(todate,'9999/12/30')>=:fromdatepart
        into :calendar
    do begin
      execute procedure ecaldays_store('SUM', :calendar , :fromdatepart, :todatepart)
        returning_values :ws_part;
      ws = ws + ws_part;
    end
  end

  if(todatepart < todate) then
  begin
    execute procedure ecaldays_store('SUM', :calendar, :todatepart+1, :todate)
      returning_values :ws_part;
    ws = ws + ws_part;
  end

  select first 1 fromdate, todate, workdim, dimnum, dimden, proportional
    from e_get_employmentchanges(:employee, :fromdate, :todate, ';DAYWORKDIM;WORKDIM;PROPORTIONAL;')
     into :fromdatepart, :todatepart, :workdim, :dimnum, :dimden, proportional;

  if(fromdatepart > fromdate) then
  begin
    execute procedure ecaldays_store('SUM', :calendar, :fromdate, :fromdatepart-1)
      returning_values :ws_part;
    ws = ws + ws_part;
  end

  D3=ws / 3600.00;

/*D4: czas faktycznie przepracowny w godz nominalnych,
  D5: czas faktycznie przepracowny w godz nadliczbowych,
  D6: czas nieprzepracowany ogolem,
  D7: czas nieprzepracowany oplacony tylko przez zaklad pracy (D4 >= D5)*/
  select workedhours, extrahours, notworkedhours, notwhours_works
    from egus_twelve_worktime(2, :employee, :fromdate, :todate)
    into :d4, :d5, :d6, :d7;

--D9: liczba dni urlopow wypoczynkowych wykorzystanych w roku
  select limitconsd
  ,0 -- D8 dni przestoi ekonomicznych-- brak danych w systemie
  from evaclimits
    where employee = :employee and vyear = :gyear
    into :d9, :d8;
  d8 = coalesce(d8,0);

  if (d9 = 0) then
  begin
    execute procedure get_config('VACCOLUMN', 2) returning_values :vc;
    execute procedure get_config('VACREQCOLUMN', 2) returning_values :vc2;
  
    select sum(workdays) from eabsences
      where employee = :employee
        and ayear = :gyear
        and ecolumn in (:vc,:vc2)
        and correction in (0,2)
     into: d9;
    d9 = coalesce(d9,0);
  end

--WYNAGRODZENIA ZA CALY ROK ---------------------------------------------------
  select sum(case when p.ecolumn = 4000 then p.pvalue else 0 end) --brutto do wyliczenia D11
    , sum(case when p.ecolumn in (1600 /*ekw. za urlop*/  ,2100,2110,2120,2300,2400 /*odprawy*/ ,2200 /*nagroda jubi.*/)then p.pvalue else 0 end) --wykluczenia z brutto
    , sum(case when p.ecolumn = 2000 then p.pvalue else 0 end) --D12: tylko premia REGULAMINOWA (np 1/3 premi za kwartal), !zgodnie z EGUS_TWELVE_GETDATA_C_V2014 (BS108630)
    , sum(case when p.ecolumn = 1400 then p.pvalue else 0 end) --D13: premie i nagrody uznaniowe
    , sum(case when p.ecolumn in (1200,1210) then p.pvalue else 0 end) --D14: wyn. za prace w godz. nadliczowych
    , 0 --D15: honoraria [BRAK W SYSTEMIE]
    , 0 --D16: dodatkowe wynagr. dla pracownikow budzetowki [BRAK W SYSTEMIE]
    , 0 --D17: udzial w zysku lub w nadwyzce bilansowej w spoldzielniach [BRAK W SYSTEMIE]
    from epayrolls r
      join eprpos p on (p.payroll = r.ref and r.cper starting with :gyear and r.empltype = 1)
    where p.employee = :employee
    into :brutto, :outbrutto, :d12, :d13, :d14, :d15, :d16, :d17;

  d11 = brutto - outbrutto - d14 - d15 - d16 - d17;   --BS108630
  d10 = d11 + d14;

  suspend;
end^
SET TERM ; ^
