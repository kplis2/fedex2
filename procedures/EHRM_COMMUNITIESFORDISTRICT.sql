--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EHRM_COMMUNITIESFORDISTRICT(
      DISTRICTID ZUSCODE_ID)
  returns (
      COMMUNITYID ZUSCODE_ID,
      NAME STRING)
   as
declare variable districtecode type of varchar_6;
begin
    --pobieramy guscode dla danego wojewodztwa
    select g.code from edictguscodes g
    where g.ref = :districtid
    into :districtecode;

    for select min(gc.ref), min(gc.descript)
     from edictguscodes gc
     where gc.dicttype = 1 and
           (substring(gc.code from 1 for 4) = :districtecode or :districtecode is null)
     group by gc.descript
     into :communityid, :name
     do begin
        suspend;
     end
end^
SET TERM ; ^
