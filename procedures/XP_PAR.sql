--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XP_PAR(
      KEY1 integer,
      KEY2 integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      SYMBOL varchar(80) CHARACTER SET UTF8                           ,
      PARTYPE varchar(1) CHARACTER SET UTF8                            = 'M')
  returns (
      RET numeric(14,2))
   as
declare variable parref integer;
declare variable parvalue numeric(16,6);
declare variable parmodified smallint;
declare variable paramname varchar(255);
declare variable prcalccol integer;
declare variable calc integer;
declare variable fromprshmat integer;
declare variable fromprshoper integer;
declare variable fromprshtool integer;
declare variable sheet integer;
declare variable descript varchar(255) ;
begin
  prcalccol = NULL;
  paramname = 'PAR('||:symbol||')';

  if(:key2<>0) then begin
    select calc from prcalccols where ref=:key2 into :calc;
    select sheet from prshcalcs where ref=:calc into :sheet;

    -- czy parametr jest uprzednio zapisany i zmodyfikowany
    parref = NULL;
    select fromprshmat, fromprshoper, fromprshtool from prcalccols where ref=:key2
      into :fromprshmat, :fromprshoper, :fromprshtool;
    select first 1 ref, cvalue, modified
      from prcalccols where parent=:key2 and expr=:paramname and calctype=1
      into :parref, :parvalue, :parmodified;
    if(:parref is not null and :parmodified=1) then begin
      ret = :parvalue;
      exit;
    end

    ret = NULL;
    select PVALUE from PRCALCPARS where CALC=:calc and PARTYPE=:PARTYPE and SYMBOL=:symbol into :ret;
    if(:ret is null) then ret = 0;

    -- nalezy zapisac wyliczony parametr
    if(:parref is not null) then begin
      update prcalccols set cvalue=:ret where ref=:parref;
    end else begin    
      select p.descript
        from prmethodpars p
        where p.symbol = :symbol
        into :descript;
        
      descript = 'Parametr "'||coalesce(:descript,:symbol)||'"';
      insert into prcalccols(PARENT,CVALUE,EXPR,DESCRIPT,CALCTYPE,PRCOLUMN)
      values(:key2,:ret,:paramname, :descript,1,null);
    end

  end else begin
    calc = NULL;
    sheet = key1;
    select DEFVALUE from PRSHPARS where sheet=:sheet and SYMBOL=:symbol into :ret;
    if(:ret is null) then ret = 0;

  end
end^
SET TERM ; ^
