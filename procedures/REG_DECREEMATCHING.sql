--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REG_DECREEMATCHING(
      DECREE DECREES_ID)
   as
declare variable SUMCREDIT CT_AMOUNT;
declare variable SUMDEBIT DT_AMOUNT;
declare variable CREDIT CT_AMOUNT;
declare variable DEBIT DT_AMOUNT;
declare variable CORAMOUNT MONEY;
declare variable MREF DECREEMATCHINGS_ID;
declare variable MCREDIT CT_AMOUNT;
declare variable MDEBIT DT_AMOUNT;
begin
  select d.credit, d.debit
    from decrees d
    where d.ref = :decree
    into :credit, :debit;
  if (:credit is null) then credit = 0;
  if (:debit is null) then debit = 0;

  select sum(m.credit), sum(m.debit)
    from decreematchings m
    where m.decree = :decree
    into :sumcredit, :sumdebit;
  if (sumcredit is null) then sumcredit = 0;
  if (sumdebit is null) then sumdebit = 0;
  -- teraz procedura ma dodać lub wyprostować zapisy w decreematching
  -- najpierw kasujemy wszystko jeżeli zmienila sie strona na dekrecie
  if ((:credit > 0 and :sumdebit > 0) or (:debit > 0 and :sumcredit > 0)) then
  begin
    -- jeżeli rekord już by rozliczony o na trigerze sie posprzataja grupy rozliczeniowe ze sparowanych rekordow
    execute procedure delete_decreematching(:decree);
    sumcredit = 0;
    sumdebit = 0;
  end
  -- credit obsluga zwiekszenia wartosci
  if (:credit > :sumcredit) then
  begin
    execute procedure insert_decreematching(:decree, :credit-:sumcredit,0);
  end else if (:credit < :sumcredit) then
  begin
    coramount = :credit - :sumcredit;
    execute procedure update_decreematching(:decree, :coramount, 0);
  end else if (:debit > :sumdebit) then
  begin
    execute procedure insert_decreematching(:decree, 0, :debit - :sumdebit);
  end
  else if (:debit < :sumdebit) then
  begin
    coramount = :debit - :sumdebit;
    execute procedure update_decreematching(:decree, 0, :coramount);
  end
end^
SET TERM ; ^
