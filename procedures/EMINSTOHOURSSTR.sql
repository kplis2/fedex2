--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMINSTOHOURSSTR(
      MINUTES integer)
  returns (
      STR varchar(20) CHARACTER SET UTF8                           )
   as
declare variable H integer;
declare variable M integer;
begin
/*Funkcja konwertuje zadana liczbe minut do formatu H:MM, gdy MM = 00 to zera sa
  wygaszane. Alternatywna realizacja danej funkcjonalnosci, w bardziej rozszerzonym
  zakresie, moze byc zapewniona przez DURATIONSTR2*/

  h = minutes / 60;     --godziny
  m = mod(minutes, 60); --minuty

  str = '';
  if (m < 0) then
  begin
    if (h = 0) then str = '-';
    m = -m;
  end

  str = str || h;
  if (m <> 0) then --gdy dopiszemy "...or (m=0 and h<>0)" to dla pelnych godzin uzyskamy format wyjsciowy 'H:00'
  begin
    if (m < 10) then
      str = str||':0'||m;
    else
      str = str||':'||m;
  end

  suspend;
end^
SET TERM ; ^
