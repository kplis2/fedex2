--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_SPOSDOST_KOSZT(
      PLATNOSC integer,
      WARTOSC numeric(14,2))
  returns (
      REF integer,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      KOSZT numeric(14,2))
   as
declare variable prog numeric(14,2);
begin
  for
    select ref, nazwa, koszt
    from sposdost
    where witryna=1
    order by lp
    into :ref, :nazwa, :koszt
  do begin
    prog = null;
    select max(prog)
      from sdostkoszt
      where platnosc=:platnosc and sposdost=:ref and prog<=:wartosc
      into :prog;

    if (prog is not null) then
      select koszt
        from sdostkoszt
        where platnosc=:platnosc and sposdost=:ref and prog=:prog
        into :koszt;

    suspend;
  end
end^
SET TERM ; ^
