--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_GET_PACKOPER(
      OPERLOGIN varchar(60) CHARACTER SET UTF8                           )
  returns (
      OPERREF integer,
      OPERNAME varchar(60) CHARACTER SET UTF8                           )
   as
declare variable intstatus smallint;
begin
  operref = 0;

  if (exists(select first 1 1 from rdb$procedures p where p.rdb$procedure_name = 'X_LISTYWYSD_GET_PACKOPER')) then
  begin
    execute statement 'execute procedure X_LISTYWYSD_GET_PACKOPER('''||:operlogin||''')'
    into :operref, :opername;
    exit;
  end
  else
  begin
    select ref, nazwa from operator where login = :operlogin
    into :operref, :opername;

    if (:operref = 0) then
      select ref, nazwa from operator where nazwa = :operlogin
      into :operref, :opername;

    if (:operref = 0) then
    begin
      execute procedure CHECK_STRING_TO_INTEGER (:operlogin) returning_values :intstatus;
      if (:intstatus is null) then intstatus = 0;

      if (:intstatus = 1) then
        select ref, nazwa from operator where ref = cast(:operlogin as integer)
        into :operref, :opername;
    end
  end
end^
SET TERM ; ^
