--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EFUNC_GET_NUMBERS_FROM_STRING(
      STR varchar(1024) CHARACTER SET UTF8                           ,
      DELIMITER varchar(1) CHARACTER SET UTF8                            = null,
      ALLOWSINGS varchar(20) CHARACTER SET UTF8                            = '',
      ENDWHENDELIMITER smallint = 1)
  returns (
      NUMBERS varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable I smallint;
declare variable SSIGN varchar(1);
declare variable BEFORESSIGN varchar(1);
declare variable AFTERSSIGN varchar(1);
declare variable STOPLOOP smallint;
begin
/*MWr: porcedura z zadanego tekstu STR pobiera cyfry i tworzy tym samym
 string NUMBERS zlozony z samych liczb. Można rowniez okreslic inne dopuszlane
 znaki w parametrze ALLOWSINGS (np '.,'), ktore moge wchodzic w opis liczby.
 Analiza tekstu zostanie przerwana jezeli natrafimy na znak DELIMITER i parametr
 ENDWHENDELIMITER = 1. Jeżeli ENDWHENDELIMITER = 0, to DELIMITER traktowany jest
 jako znak dopuszczalny (np. spacja, aby oddzielic ciag cyfr) */

  numbers = '';
  beforessign = '';
  str = trim(str);
  endwhendelimiter = coalesce(endwhendelimiter,1);
  i = 1;
  stoploop = 0;

  while (i <= coalesce(char_length(str),0) and stoploop = 0) do -- [DG] XXX ZG119346
  begin
    ssign = substring(str from i for 1);
    afterssign = substring(str from i + 1 for 1);
    if (ssign = delimiter and endwhendelimiter = 1) then
      stoploop = 1;
    else if ((ssign >= '0' and ssign <= '9')
           or ssign = delimiter  --PR55109
           or (allowsings containing ssign --PR54992
             and afterssign >= '0' and afterssign <= '9'
             and beforessign >= '0' and beforessign <= '9')
         ) then
           numbers = numbers || ssign;

    beforessign = ssign;
    i = i + 1;
  end

  suspend;
end^
SET TERM ; ^
