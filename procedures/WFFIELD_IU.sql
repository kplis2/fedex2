--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WFFIELD_IU(
      REF WFFIELD_ID,
      WFINSTANCEREF WFINSTANCE_ID,
      UUID NEOS_ID,
      FIELDVALUE STRING1024,
      WFINSTANCEUUID NEOS_ID,
      FIELDNAME STRING60)
  returns (
      REFOUT WFFIELD_ID)
   as
begin
  if (exists(select ref from wffield where (ref = :ref))) then
  begin
    update wffield
    set wfinstanceref = :wfinstanceref,
        uuid = :uuid,
        fieldvalue = :fieldvalue,
        wfinstanceuuid = :wfinstanceuuid,
        fieldname = :fieldname
    where (ref = :ref);
    refout=ref;
  end
  else
    insert into wffield (
        wfinstanceref,
        uuid,
        fieldvalue,
        wfinstanceuuid,
        fieldname)
    values (
        :wfinstanceref,
        :uuid,
        :fieldvalue,
        :wfinstanceuuid,
        :fieldname)
   returning ref into :refout;
   suspend;
end^
SET TERM ; ^
