--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPK_STANYAKTUREAL(
      OPKREF integer)
   as
declare variable ilreal numeric(14,4);
declare variable magreal numeric(14,2);
declare variable kaucjareal numeric(14,2);
declare variable rilreal numeric(14,4);
declare variable rmagreal numeric(14,2);
declare variable rkaucjareal numeric(14,2);
declare variable usluga smallint;
declare variable tmpref integer;
declare variable stan numeric(18,4);
declare variable ack smallint;
begin
  select stanyopk.usluga from stanyopk where stanyopk.ref = :opkref into :usluga;  --
  if (:usluga <> 1) then                                                           -- SB
  begin
    select sum(DOKUMROZ.iloscl),sum(DOKUMROZ.WARTOSC),sum(DOKUMROZ.wartsbrutto)
      from DOKUMROZ
      left join DOKUMPOZ on(DOKUMROZ.POZYCJA = DOKUMPOZ.REF)
      left join DOKUMNAG on (DOKUMNAG.REF=DOKUMPOZ.DOKUMENT)
      left join DEFDOKUM on (DEFDOKUM.symbol = DOKUMNAG.TYP)
      where DOKUMROZ.opkrozid = :opkref
        and ((DEFDOKUM.KAUCJABN <> 'N') or (DEFDOKUM.kaucjabn is null))
        and dokumroz.ack > 0
    into :rilreal,:rmagreal,:rkaucjareal;
    if(:rilreal is null) then rilreal = 0;
    if(:rmagreal is null) then rmagreal = 0;
    if(:rkaucjareal is null) then rkaucjareal = 0;
    select sum(DOKUMROZ.iloscl),sum(DOKUMROZ.WARTOSC),sum(DOKUMROZ.wartsnetto)
      from DOKUMROZ
      left join DOKUMPOZ on(DOKUMROZ.POZYCJA = DOKUMPOZ.REF)
      left join DOKUMNAG on (DOKUMNAG.REF=DOKUMPOZ.DOKUMENT)
      left join DEFDOKUM on (DEFDOKUM.symbol = DOKUMNAG.TYP)
      where DOKUMROZ.opkrozid = :opkref and DEFDOKUM.KAUCJABN = 'N'
        and dokumroz.ack > 0
    into :ilreal,:magreal,:kaucjareal;
    if(:ilreal is null) then ilreal = 0;
    if(:magreal is null) then magreal = 0;
    if(:kaucjareal is null) then kaucjareal = 0;
    ilreal = :ilreal + :rilreal;
    magreal = :magreal + :rmagreal;
    kaucjareal = :kaucjareal + :rkaucjareal;
    update STANYOPK set ILZREAL = :ilreal,WARTMAGZREAL = :magreal,KAUCJAZREAL = :kaucjareal where REF=:opkref;
  end
  else begin       --SB
    select sum(DOKUMPOZ.ilosc),sum(DOKUMPOZ.WARTOSC),sum(DOKUMPOZ.wartsbrutto)
      from DOKUMPOZ
      left join DOKUMNAG on (DOKUMNAG.REF=DOKUMPOZ.DOKUMENT)
      left join DEFDOKUM on (DEFDOKUM.symbol = DOKUMNAG.TYP)
      where DOKUMPOZ.opkrozid = :opkref
        and ((DEFDOKUM.KAUCJABN <> 'N') or (DEFDOKUM.kaucjabn is null))
        and dokumnag.akcept > 0
    into :rilreal,:rmagreal,:rkaucjareal;
    if(:rilreal is null) then rilreal = 0;
    if(:rmagreal is null) then rmagreal = 0;
    if(:rkaucjareal is null) then rkaucjareal = 0;
    select sum(DOKUMPOZ.ilosc),sum(DOKUMPOZ.WARTOSC),sum(DOKUMPOZ.wartsnetto)
      from DOKUMPOZ
      left join DOKUMNAG on (DOKUMNAG.REF=DOKUMPOZ.DOKUMENT)
      left join DEFDOKUM on (DEFDOKUM.symbol = DOKUMNAG.TYP)
      where DOKUMPOZ.opkrozid = :opkref and DEFDOKUM.KAUCJABN = 'N'
        and dokumnag.akcept > 0
    into :ilreal,:magreal,:kaucjareal;
    if(:ilreal is null) then ilreal = 0;
    if(:magreal is null) then magreal = 0;
    if(:kaucjareal is null) then kaucjareal = 0;
    ilreal = :ilreal + :rilreal;
    magreal = :magreal + :rmagreal;
    kaucjareal = :kaucjareal + :rkaucjareal;
    update STANYOPK set ILZREAL = :ilreal,WARTMAGZREAL = :magreal,KAUCJAZREAL = :kaucjareal where REF=:opkref;
  end                   --SB
end^
SET TERM ; ^
