--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDE_GET_DOKMAGDATE(
      PRSCHEDGUIDE integer)
  returns (
      DOKMAGDATA timestamp)
   as
begin
  select max(p.maketime)
    from prschedopers o
      left join propersraps p on (p.prschedoper = o.ref)
    where o.guide = :prschedguide
    into dokmagdata;
  suspend;
end^
SET TERM ; ^
