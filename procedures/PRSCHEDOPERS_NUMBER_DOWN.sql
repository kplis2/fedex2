--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDOPERS_NUMBER_DOWN(
      SCHOPERTO integer,
      NUMBER integer)
  returns (
      NUMBEROUT integer,
      SCHOPERFROM integer)
   as
declare variable depfrom integer;
begin
--zerowanie na nieaktywnych operacjach alternatywnych
  for
    select d.depfrom from prschedoperdeps d
      join  prschedopers o on d.depfrom = o.ref and o.activ = 0
      where d.depto = :SCHOPERTO
      into :depfrom
  do begin
    if (:depfrom is not null) then
      update prschedopers o set o.number = null where o.ref = :depfrom;
  end
  depfrom = null;
  --przejcie po aktywnych operacjach
  for
    select distinct d.depfrom from prschedoperdeps d
      join  prschedopers o on d.depfrom = o.ref and o.activ = 1
      where d.depto = :SCHOPERTO
      into :depfrom
  do begin
    execute procedure PRSCHEDOPERS_NUMBER_DOWN (:depfrom, :number)
      returning_values :number, :SCHOPERFROM;
  end
    update prschedopers o set o.number = :number where o.ref = :SCHOPERTO;
    NUMBEROUT = :number + 1;
    if(:depfrom is null) then SCHOPERFROM = :SCHOPERTO;
end^
SET TERM ; ^
