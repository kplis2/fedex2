--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_ADDYEARINFO4PIT(
      EYEAR integer,
      USCODE varchar(6) CHARACTER SET UTF8                           ,
      NAME varchar(20) CHARACTER SET UTF8                            = null,
      LNAME varchar(60) CHARACTER SET UTF8                            = null,
      CORRECTION smallint = null,
      ANNOTATION varchar(2048) CHARACTER SET UTF8                            = null)
  returns (
      NIP varchar(255) CHARACTER SET UTF8                           ,
      PNAME varchar(255) CHARACTER SET UTF8                           ,
      PHISICAL smallint,
      PYEAR integer,
      USNAME varchar(255) CHARACTER SET UTF8                           ,
      SIGNFNAME varchar(20) CHARACTER SET UTF8                           ,
      SIGNSNAME varchar(60) CHARACTER SET UTF8                           ,
      PCORRECTION smallint,
      ANN varchar(2048) CHARACTER SET UTF8                           )
   as
begin

  select first 1 name || ', ' || address || ', '|| city || ' ' || postcode
    from einternalrevs
    where code = :uscode
    into :usname;

  select nip, phisical, descript from e_get_platnikinfo4pit
    into :nip, :phisical, :pname;

  pyear = eyear;
  signfname = name;
  signsname = lname;
  pcorrection = correction;
  ann = annotation;

  suspend;
end^
SET TERM ; ^
