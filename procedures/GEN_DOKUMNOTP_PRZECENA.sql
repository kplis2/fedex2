--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GEN_DOKUMNOTP_PRZECENA(
      DOKUMNOTREF integer)
  returns (
      STATUS integer)
   as
declare variable magazyn varchar(3);
declare variable typ varchar(1);
declare variable datadok timestamp;
declare variable wartosc numeric(14,2);
declare variable ktm varchar(40);
declare variable wersjaref integer;
begin
  status = 0;
  select d.magazyn, d.data, dm.typ from dokumnot d
    join defmagaz dm on (dm.symbol = d.magazyn)
    where d.ref=:dokumnotref
    into :magazyn, :datadok, :typ;
  if (typ = 'P') then begin
    delete from dokumnotp where DOKUMENT=:dokumnotref;
    for
      select sum(wartosc), rktm, rwersjaref
        from mag_stanyk(:magazyn,:datadok,0)
        group by rktm, rwersjaref
        having sum(stan) = 0 and sum(wartosc) <> 0
        into :wartosc, :ktm, :wersjaref
    do begin
      insert into DOKUMNOTP(DOKUMENT,CENAOLD,CENANEW,KTM,WERSJAREF,ILOSC)
        values (:dokumnotref, :wartosc, 0, :ktm, :wersjaref, 1);
      status = status + 1;
    end
  end else begin
    select count(*)
      from STANYIL
      where STANYIL.MAGAZYN = :magazyn and stanyil.ilosc=0 and stanyil.wartosc<>0
      into :status;
    if(:status>0) then begin
      delete from dokumnotp where DOKUMENT=:dokumnotref;
      insert into DOKUMNOTP(DOKUMENT,CENAOLD,CENANEW,KTM,WERSJAREF,ILOSC)
        select :dokumnotref,stanyil.wartosc, 0, stanyil.ktm, stanyil.wersjaref, 1
          from STANYIL
          where STANYIL.MAGAZYN = :magazyn and stanyil.ilosc=0 and stanyil.wartosc<>0
          order by STANYIL.WERSJAREF;
    end
  end
end^
SET TERM ; ^
