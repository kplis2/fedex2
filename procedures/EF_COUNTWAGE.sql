--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_COUNTWAGE(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      STYPE varchar(10) CHARACTER SET UTF8                           )
  returns (
      SUMWAGE numeric(14,4))
   as
declare variable FROMDATE date;
declare variable TODATE date;
declare variable WAGE numeric(14,6);
declare variable EFROMDATE date;
declare variable ETODATE date;
declare variable EWAGE numeric(16,6);
declare variable ACALDAYS integer;
declare variable AFROMDATE date;
declare variable ATODATE date;
declare variable TFROMDATE date;
declare variable TTODATE date;
declare variable SALARY numeric(14,6);
declare variable CADDSALARY numeric(14,6);
declare variable FUNCSALARY numeric(14,6);
declare variable DAYWAGE numeric(14,6);
declare variable PAYMENTTYPE smallint;
declare variable PROPORTIONAL smallint;
declare variable DIMNUM smallint;
declare variable DIMDEN smallint;
declare variable EWORKHOURS numeric(14,2);
declare variable AWORKHOURS numeric(14,2);
declare variable AWAGE numeric(14,2);
declare variable WORKHOURS numeric(14,2);
declare variable EWORKDAYS smallint;
declare variable WORKDAYS smallint;
declare variable STDCALENDAR integer;
declare variable CORRECTED integer;
declare variable OTHERDAYS smallint;
declare variable UNIQWORKDAY date;
declare variable AMONTH smallint;
declare variable AYEAR integer;
declare variable APERIOD varchar(6);
declare variable ALLACALDAYS integer = 0;
declare variable ALLECALDAYS integer = 0;
declare variable COUNTER integer;
declare variable TWORKHOURS numeric(14,2);
begin
--DU: personel - do liczenia placy zasadniczej pracownikowi na liscie plac

--MW: nie usuwać naliczania zmiennych workhours, aworkhours, eworkhours
--ponieważ są używane w modyfikacjach indywidualnych u klientów
 
  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  execute procedure get_config('STDCALENDAR',2)
    returning_values :stdcalendar;

  execute procedure ecaldays_store('COUNT', :stdcalendar, :fromdate, :todate) --PR53533
    returning_values :workdays;

  sumwage = 0;
  for
    select fromdate, todate, paymenttype, salary, caddsalary,
        funcsalary, dimnum, dimden, proportional
      from employment
      where employee = :employee and fromdate <= :todate and (todate is null or todate >= :fromdate)
      order by fromdate
      into :efromdate, :etodate, :paymenttype, :salary, :caddsalary,
        :funcsalary, :dimnum, :dimden, :proportional
  do begin
    if (stype = 'SALARY') then
      ewage = salary;
    else if (stype = 'CADDSALARY') then
      ewage = caddsalary;
    else if (stype = 'FUNCSALARY') then
      ewage = funcsalary;
    else
      ewage = 0;

    --policzenie pensji za ten miesiac
    --umowa moze nie zawierac pelnego miesiaca pracy
    if (workdays is null) then
      wage = 0;
    else begin
      if (etodate is null or etodate > todate) then
        etodate = todate;
      if (efromdate < fromdate) then
        efromdate = fromdate;

      execute procedure emplcaldays_store('SUM', :employee, :efromdate, :etodate)
        returning_values :eworkhours;
      eworkhours = coalesce(eworkhours,0) / 3600.00;

      workhours = eworkhours;

      execute procedure ecaldays_store('COUNT', :stdcalendar, :efromdate, :etodate)
       returning_values :eworkdays;

      if (paymenttype = 1 and stype = 'SALARY') then
      begin
        -- platni godzinowo
        for
          select fromdate, todate
            from eabsences
            where employee = :employee
              and fromdate <= :etodate and todate >= :efromdate
              and ecolumn not in (180, 250, 310, 320)
              and correction in (0,2)
            into :afromdate, :atodate
        do begin
          if (afromdate < efromdate) then
            afromdate = efromdate;
          if (atodate > etodate) then
            atodate = etodate;

          execute procedure emplcaldays_store('SUM', :employee, :afromdate, :atodate)
            returning_values :aworkhours;
          aworkhours = aworkhours / 3600.00;

          eworkhours = eworkhours - coalesce(aworkhours, 0);
        end

        wage = ewage * eworkhours;
        if (proportional = 1) then
          wage = cast(wage * dimnum / dimden as numeric(14,2));

      end else
      begin
        --obliczenie nieobecnosci liczonych w dniach roboczych
        -- 1 - nieobecnosc nieusprawiedliwiona, 19 - opieka bezplatna,
        -- 26 - urlop wychowawczy, 30 - urlop bezplatny, 33 - sluzba wojskowa,
        -- 39 - nieobecnosc uspr. bezpl.
        for
          select fromdate, todate
            from eabsences
            where employee = :employee
              and fromdate <= :etodate and todate >= :efromdate
              and ecolumn in  (10, 190, 260, 300, 330, 390)
              and correction in (0,2)
            into :afromdate, :atodate
        do begin
          if (afromdate < efromdate) then
            afromdate = efromdate;
          if (atodate > etodate) then
            atodate = etodate;

          execute procedure emplcaldays_store('SUM', :employee, :afromdate, :atodate)
            returning_values :aworkhours;
          aworkhours = aworkhours / 3600.00;

          eworkhours = eworkhours - coalesce(aworkhours,0);
        end

        if (workhours <> 0) then
          wage = ewage * eworkdays / workdays * eworkhours / workhours;
        else
          wage = 0; --BS39552

        --obliczanie nieobecnosci liczonych w dniach kalendarzowych
        -- 4 - choroba wyn. 80%, 5 - choroba wyn. 100%, 6 - choroba wyn. wyp
        -- 9 - choroba ZUS 80%, 10 - choroba ZUS 100%, 11 - choroba ZUS wyp.
        -- 12 - choroba szpital 70%, 13 - choroba oczekiwanie,
        -- 16 - opieka nad dzieckiem, 17 - opieka na doroslym
        -- 27 - urlop macierzyski, 28 - dodtkowy macierzynski, 29 - url. ojcowski
        -- 35, 36, 37 - swiadczenie rehabilitacyjne
        acaldays = 0;
        for
          select fromdate, todate
            from eabsences
            where employee = :employee
              and fromdate <= :etodate and todate >= :efromdate
              and ecolumn in (40, 50, 60, 90, 100, 110, 120, 130, 140, 150, 160, 170, 270, 280, 290, 350, 360, 370, 440, 450)
              and correction in (0,2)
            into :afromdate, :atodate
        do begin
          if (afromdate < efromdate) then
            afromdate = efromdate;
          if (atodate > etodate) then
            atodate = etodate;
          acaldays = acaldays + (atodate - afromdate + 1);
        end

        daywage = 1.00000 * ewage / 30;
        wage = wage - daywage * acaldays;
        if (etodate - efromdate + 1 = acaldays) then
          wage = 0;

        allacaldays = allacaldays + acaldays;
        allecaldays = allecaldays + etodate - efromdate + 1;
      end
    end
    sumwage = sumwage + wage;
  end
  if (sumwage < 0) then
    sumwage = 0;

--obsluga przypadku: 30 dni choroby i 1 dzien pracy (UniqWorkDay), za ktory nalezy sie wynagr. (BS39585)
  if (allacaldays = 30 and allecaldays = 31 and paymenttype <> 1) then
  begin
    select d.cdate from emplcaldays d
        join edaykinds k on (k.ref = d.daykind and k.daytype = 1)
        left join eabsences a on (d.absence = a.ref and a.correction in (0,2))
        left join ecolparams p on (p.ecolumn = a.ecolumn and p.param = 'ABSTYPE' and p.pval = '1') --BS52704
      where d.employee = :employee
        and d.cdate >= :fromdate and d.cdate <= :todate
        and (d.absence is null or p.ecolumn is null)
      into :uniqworkday;

    if (uniqworkday is not null) then
    begin
      select case :stype when 'SALARY' then salary
                         when 'CADDSALARY' then caddsalary
                         when 'FUNCSALARY' then funcsalary
                         else 0 end
        from employment
        where employee = :employee and fromdate <= :uniqworkday
          and (todate is null or todate >= :uniqworkday)
        into :ewage;

      sumwage = ewage / workdays;
    end
  end

--potracenie za nieobecnosci zalegle rozliczane w dniach kalendarzowych
  awage = 0;
  for
    select distinct ayear, amonth
      from eabsences
      where employee = :employee and fromdate < :fromdate
        and epayroll = :payroll and correction in (0,2)
        and ecolumn in (40, 50, 60, 90, 100, 110, 120, 130, 140, 150, 160, 170, 270, 280, 290, 350, 360, 370, 440, 450)
    order by ayear desc, amonth desc
    into :ayear, :amonth
  do begin 

     select sum(days) from eabsences
        where employee = :employee and fromdate < :fromdate
          and ayear = :ayear and amonth = :amonth
          and ecolumn in (40, 50, 60, 90, 100, 110, 120, 130, 140, 150, 160, 170, 270, 280, 290, 350, 360, 370, 440, 450)
          and correction in (0,2)
      into :acaldays;

    uniqworkday = null;
    if (acaldays = 30 and stype = 'SALARY') then
    begin
    --obsluga przypadku: 30 dni choroby i 1 dzien pracy (BS39585)
      select cdate from emplcaldays cd
      join edaykinds edk on edk.ref = cd.daykind
        where cd.employee = :employee
          and cd.pyear = :ayear and cd.pmonth = :amonth
          and cd.absence is null
          and edk.daytype = 1
          into :uniqworkday;

      if (uniqworkday is not null) then
      begin
        select salary from employment
          where employee = :employee and fromdate <= :uniqworkday
            and (todate is null or todate >= :uniqworkday)
          into :ewage;

        select count(*) from ecaldays cd
        join edaykinds edk on edk.ref = cd.daykind
          where cd.cyear = :ayear and cd.cmonth = :amonth
            and edk.daytype = 1 and cd.calendar = :stdcalendar
          into :workdays;

        if (coalesce(workdays,0) <> 0) then
        begin
          awage = awage - ewage / workdays;  --tyle pracownik powinien byl dostac w miesiacu poprzednim

          select periodout from e_func_periodinc(:ayear || :amonth, 0)
            into :aperiod;

          ewage = null;
          select sum(p.pvalue) from eprpos p
            join epayrolls r on (r.ref = p.payroll and p.employee = :employee
                               and p.ecolumn = 1000 and r.cper = :aperiod)
            where r.ref in (select epayroll from eabsences
                              where employee = :employee and correction in (0,2)
                                and ayear = :ayear and amonth = :amonth)
            into :ewage;  --tyle pracownik dostal w miesiacu poprzednim

          awage = awage + coalesce(ewage,0);
        end
      end
    end

    if (uniqworkday is null) then
    --wyliczenie 'standardowe' bez szczegolnego przypadku: 30 dni chor. 1 dzien pracy (BS39585)
      for
        select fromdate, todate, corrected
          from eabsences
          where employee = :employee and fromdate < :fromdate
            and epayroll = :payroll and correction in (0,2)
            and ayear = :ayear and amonth = :amonth
            and ecolumn in (40, 50, 60, 90, 100, 110, 120, 130, 140, 150, 160, 170, 270, 280, 290, 350, 360, 370, 440, 450)
          into :afromdate, :atodate, :corrected
      do begin
        for
          select fromdate, todate, salary, caddsalary, funcsalary
            from employment
            where employee = :employee and fromdate <= :atodate
              and (todate is null or todate >= :afromdate)
              and paymenttype <> 1 --BS36211
            order by fromdate
            into :efromdate, :etodate, :salary, :caddsalary, :funcsalary
        do begin
          if (stype = 'SALARY') then ewage = salary;
          else if (stype = 'CADDSALARY') then ewage = caddsalary;
          else if (stype = 'FUNCSALARY') then ewage = funcsalary;
          else ewage = 0;
          if (afromdate >= efromdate) then tfromdate = afromdate;
          else tfromdate = efromdate;
          if (etodate is null or atodate <= etodate) then ttodate = atodate;
          else ttodate = etodate;
          acaldays = ttodate - tfromdate + 1;

          if (extract(day from atodate) = 31) then --obsluga 31 dnia
          begin
            select sum(days)
              from eabsences
              where employee = :employee
                and ayear = :ayear and amonth = :amonth 
                and ecolumn in (40, 50, 60, 90, 100, 110, 120, 130, 140, 150, 160, 170, 270, 280, 290, 350, 360, 370, 440, 450)
                and correction in (0,2)
              into :otherdays;
            if (otherdays = 31) then acaldays = acaldays - 1;
          end

          if (corrected is not null) then
          begin
            tfromdate = null;
            ttodate = null;
            select fromdate, todate from eabsences
              where ref = :corrected
              into :tfromdate, :ttodate;
            acaldays = acaldays - (ttodate - tfromdate + 1);
          end
          daywage = 1.00000 * ewage / 30;
          awage = awage + daywage * acaldays;
        end
      end
  end
  sumwage = sumwage - awage;

--potracenie za nieobecnosci zalegle rozliczane w dniach roboczych (BS26316)
  awage = 0;
  for
    select fromdate, todate, cast(ayear || '/' || amonth || '/1' as date)
      from eabsences
      where employee = :employee and fromdate < :fromdate
        and epayroll = :payroll and correction in (0,2)
        and ecolumn in (10, 190, 260, 300, 330, 390)
      into :afromdate, :atodate, :tfromdate
  do begin

    execute procedure MonthLastDay(afromdate)
      returning_values :ttodate;

    for
      select fromdate, todate, salary, caddsalary, funcsalary
        from employment
        where employee = :employee and fromdate <= :atodate
          and (todate is null or todate >= :afromdate)
          and paymenttype <> 1 --BS36211
        order by fromdate
        into :efromdate, :etodate, :salary, :caddsalary, :funcsalary
    do begin
      if (stype = 'SALARY') then ewage = salary;
      else if (stype = 'CADDSALARY') then ewage = caddsalary;
      else if (stype = 'FUNCSALARY') then ewage = funcsalary;
      else ewage = 0;

    /*wyliczamy czas trwania okresu (w ktorym wystapila nieobecnosc) dla danych
      warunkow finansowych; zakres okresu to maksymalnie miesiac*/
      if (efromdate < tfromdate) then efromdate = tfromdate;
      if (etodate > ttodate or etodate is null) then etodate = ttodate;

      select ws/3600.00 from ecal_work(:employee, :efromdate, :etodate)
        into :eworkhours;

      if (eworkhours <> 0) then
      begin
      /*wykorzytsamy ponownie zmienne efromdate i etodate aby wyliczyc czas
        trwania nieobecnosci w danym okresie przebiegu zatrudnienia*/
        if (afromdate >= efromdate) then efromdate = afromdate;
        if (atodate <= etodate) then etodate = atodate;

        select ws/3600.00 from ecal_work(:employee, :efromdate, :etodate)
          into :aworkhours;

        wage = ewage * aworkhours / eworkhours;
        awage = awage + wage;
      end
    end
  end
  sumwage = sumwage - awage;

--potracenie za nieobecnosci zalegle dla pracownikow godzinowych (BS36211;BS53334)
  if (stype = 'SALARY') then
  begin
    awage = 0;
    for
      select fromdate, todate, corrected, coalesce(worksecs,0)/3600.00 from eabsences
        where employee = :employee and fromdate < :fromdate
          and epayroll = :payroll and correction in (0,2)
          and ecolumn not in (180, 250, 310, 320)
        into :afromdate, :atodate, :corrected, :aworkhours
    do begin
      tfromdate = null;
      ttodate = null;
      if (corrected is not null) then
      begin
        select fromdate, todate, coalesce(worksecs,0)/3600.00 from eabsences
          where ref = :corrected
          into :tfromdate, :ttodate, :tworkhours ;
      end else
        tworkhours = 0;

      if (aworkhours - tworkhours <> 0) then
      begin
      /*sprawdzenie czy zmienila sie stawka w przebiegu zatrudnienia, jezeli tak,
        to w wyliczeniu potracenia musimy odwolywac sie do ponownego przeliczenia
        czasu nieobecnosci z uwzglednieniem biezacej stawki godzinowej*/
        counter = 0;
        select count(distinct salary), max(salary) from employment
          where employee = :employee and paymenttype = 1
            and ((fromdate <= :atodate and (todate is null or todate >= :afromdate))
              or (fromdate <= :ttodate and (todate is null or todate >= :tfromdate)))
          into :counter, :ewage;

       if (counter > 1) then
       begin
       --jest zmiana stawaki godzinowej w okresie trwania nieobecnosci
        if (aworkhours <> 0) then
        --obsluga nieobecnosci korygujacej badz normalnej
          for
            select fromdate, todate, salary from employment
              where employee = :employee and fromdate <= :atodate
                and (todate is null or todate >= :afromdate)
                and paymenttype = 1
              order by fromdate
              into :efromdate, :etodate, :ewage
          do begin
            if (afromdate > efromdate) then efromdate = afromdate;
            if (atodate < etodate or etodate is null) then etodate = atodate;
      
            select ws/3600.00 from ecal_work(:employee, :efromdate, :etodate)
              into :aworkhours;
            awage = awage + aworkhours * ewage;
          end

        if (tworkhours <> 0) then
        --obsluga nieobecnosci skorygowanej
          for
            select fromdate, todate, salary from employment
              where employee = :employee and fromdate <= :ttodate
                and (todate is null or todate >= :tfromdate)
                and paymenttype = 1
              order by fromdate
              into :efromdate, :etodate, :ewage
          do begin
            if (tfromdate > efromdate) then efromdate = tfromdate;
            if (ttodate < etodate or etodate is null) then etodate = ttodate;
        
            select ws/3600.00 from ecal_work(:employee, :efromdate, :etodate)
              into :aworkhours;
    
            awage = awage - aworkhours * ewage;
          end
        end else
        if (counter = 1) then begin
        --nie ma zmiany stawki w przebiegu zatrudnienia, wyliczenie proste
          awage = awage + ewage * (aworkhours - tworkhours);
        end
      end
    end
    sumwage = sumwage - awage;
  end

  suspend;
end^
SET TERM ; ^
