--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_TERMINATE(
      OPER OPERATOR_ID = null)
   as
begin
  post_event 'TERMINATE'||coalesce(:oper,'');
end^
SET TERM ; ^
