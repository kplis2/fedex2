--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_GET_MWSORDCARTCOLOR(
      WH DEFMAGAZ_ID,
      MWSORD integer,
      CURRENTCART integer,
      MODE smallint,
      CURRENTCARTNO integer = null,
      ALLCARTS smallint = 1)
  returns (
      MWSCARTCOLOR integer,
      COLORNAME STRING20,
      COLORVAL STRING20,
      COLORHTMLVAL STRING20)
   as
begin
  if (coalesce(:currentcart,0) > 0) then
  begin
    select distinct c.ref, c.cname, c.cval, c.htmlval
      from mwsordcartcolours m
        left join mwscartcolours c on (m.mwscartcolor = c.ref)
      where m.mwsord = :mwsord
        and m.cart = :currentcart
    into :mwscartcolor, :colorname, :colorval, :colorhtmlval;
  end
  else
  begin
    if (allcarts = 0 and mode = 0 and (select count(distinct a.docid) from mwsacts a where a.mwsord = :mwsord) > 1) then
      select first 1 c.ref, c.cname, c.cval, c.htmlval
        from mwscartcolourswhorder co
          left join mwscartcolours c on (co.mwsordcartcolor = c.ref)
          left join mwsordcartcolours mc on mc.mwsord = :mwsord and mc.mwscartcolor = c.ref and mc.status = 0
          left join mwsordcartcolours mcr on mcr.mwsord = :mwsord and mcr.mwscartcolor = c.ref and mcr.status = 1
      where co.wh = :wh and mc.mwsord is null and mcr.mwsord is not null
      order by mcr.accepttime desc, co.colorno
      into :mwscartcolor, :colorname, :colorval, :colorhtmlval;
    else
      select c.ref, c.cname, c.cval, c.htmlval
        from mwscartcolourswhorder co
          left join mwscartcolours c on (co.mwsordcartcolor = c.ref)
      where co.wh = :wh
        and co.colorno = :currentcartno
      into :mwscartcolor, :colorname, :colorval, :colorhtmlval;
  end

  --if (:mwscartcolor is null or (select count(distinct a.docid) from mwsacts a where a.mwsord = :mwsord) = 1) then
  if (:mwscartcolor is null or (select multi from mwsords where ref = :mwsord) in (0,2)) then
  begin
    mwscartcolor = 1;
    colorname = 'White';
    colorval = '-1';
  end
  suspend;
end^
SET TERM ; ^
