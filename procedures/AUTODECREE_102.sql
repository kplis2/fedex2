--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_102(
      REFF integer)
   as
declare variable transdate timestamp;
declare variable period varchar(6);
declare variable account KONTO_ID;
declare variable settlement varchar(20);
declare variable currency varchar(3);
declare variable currate numeric(14,4);
declare variable scurrbalance numeric(15,2);
declare variable sbalance numeric(15,2);
declare variable tobook numeric(15,2);
declare variable diffdebit numeric(15,2);
declare variable diffcredit numeric(15,2);
declare variable bkdoc integer;
declare variable company integer;
declare variable tbank varchar(64);
declare variable diffaccount ACCOUNT_ID;
begin
  --DU: statystyczne roznice kursowe

  select transdate, period, company from bkdocs where ref = :reff
    into :transdate, :period, :company;

  execute procedure getconfig('BANK_FOR_RATEDIFF') returning_values :tbank;
  if (coalesce(tbank,'')='') then exception fk_expt 'Brak podanego banku dla statystycznych różnic kursowych!';
  execute procedure getconfig('ACCOUNT_FOR_RATEDIFF') returning_values :diffaccount;
  if (coalesce(diffaccount,'')='') then exception fk_expt 'Brak podanego konta dla statystycznych różnic kursowych!';


  diffdebit = 0;
  diffcredit = 0;

  execute procedure gen_ref('BKDOCS') returning_values bkdoc;

  insert into bkdocs (ref, bkreg, period, transdate, docdate, company)
    values (:bkdoc, 'PK', cast(cast(substring(:period from 1 for 4) as integer) + 1 as varchar(4)) || '01',
      cast(cast(substring(:period from 1 for 4) as integer) + 1 as varchar(4)) || '/1/2',
      cast(cast(substring(:period from 1 for 4) as integer) + 1 as varchar(4)) || '/1/2', :company);


  for
    select R.kontofk, R.symbfak, R.waluta, sum(P.winien - P.ma), sum(P.winienzl - P.mazl)
      from rozrach R
        join rozrachp P on (R.slodef = P.slodef and R.slopoz = P.slopoz
          and R.kontofk = P.kontofk and R.symbfak = P.symbfak and R.company = P.company)
      where cast(P.data as date) <= :transdate and R.walutowy = 1
        and R.company = :company
      group by R.slodef, R.slopoz, R.kontofk, R.symbfak, R.waluta
      having sum(P.winien - P.ma) <> 0
      into :account, :settlement, :currency, :scurrbalance ,:sbalance
  do begin
    select first 1 P.sredni
      from tabkursp P
        join tabkurs T on (P.tabkurs = T.ref)
      where T.bank = :tbank and T.data = :transdate and P.waluta = :currency
      into :currate;
    if (currate is null) then exception fk_expt 'Brak kursu dla waluty: '|| :currency||' na dzień ' || :transdate;
    tobook = sbalance - currate * scurrbalance;

    if (tobook > 0) then begin
      execute procedure insert_decree_currsettlement(reff, account, 1, tobook,
        0, 0, currency, 'statystyczne roznice kursowe', settlement, null, 9, 0,
        null, null, null, null, null, null, null, null, 0);
      execute procedure insert_decree_currsettlement(bkdoc, account, 1, -tobook,
        0, 0, currency, 'statystyczne roznice kursowe', settlement, null, 9, 0,
        null, null, null, null, null, null, null, null, 0);

      diffdebit = diffdebit + tobook;
    end else
    begin
      execute procedure insert_decree_currsettlement(reff, account, 0, -tobook,
        0, 0, currency, 'statystyczne roznice kursowe', settlement, null, 9, 0,
        null, null, null, null, null, null, null, null, 0);
      execute procedure insert_decree_currsettlement(bkdoc, account, 0, tobook,
        0, 0, currency, 'statystyczne roznice kursowe', settlement, null, 9, 0,
        null, null, null, null, null, null, null, null, 0);
      diffcredit = diffcredit - tobook;
    end
  end
  execute procedure insert_decree(reff, diffaccount, 0, diffdebit, 'statystyczne roznice kursowe', 0);
  execute procedure insert_decree(reff, diffaccount, 1, diffcredit, 'statystyczne roznice kursowe', 0);

  execute procedure insert_decree(bkdoc, diffaccount, 0, -diffdebit, 'statystyczne roznice kursowe', 0);
  execute procedure insert_decree(bkdoc, diffaccount, 1, -diffcredit, 'statystyczne roznice kursowe', 0);


end^
SET TERM ; ^
