--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR92121_TEST_FILL as
declare variable i integer;
begin
  i = 0;
  while(i < 100000) do
  begin
    insert into pr92121_test(item,timest)
    values('Item '||:i, cast('now' as timestamp));
    i=i+1;
  end
  suspend;
end^
SET TERM ; ^
