--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XP_WSPOLCZYNNIK(
      KEY1 integer,
      KEY2 integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(16,6))
   as
declare variable operfactor numeric(16,6);
declare variable condition numeric(14,2);
declare variable eoperfactor varchar(1024);
declare variable econdition varchar(1024);
declare variable parref integer;
declare variable parvalue numeric(16,6);
declare variable parmodified smallint;
declare variable paramname varchar(255);
declare variable fromprshmat integer;
declare variable fromprshoper integer;
declare variable fromprshtool integer;
begin
  operfactor = 0;
  paramname='WSPOLCZYNNIK';
  -- czy parametr jest uprzednio zapisany i zmodyfikowany
  parref = NULL;
  select fromprshmat, fromprshoper, fromprshtool from prcalccols where ref=:key2
    into :fromprshmat, :fromprshoper, :fromprshtool;
  select first 1 ref, cvalue, modified
    from prcalccols where parent=:key2 and expr=:paramname and calctype=1
    into :parref, :parvalue, :parmodified;
  if(:parref is not null and :parmodified=1) then begin
    ret = :parvalue;
    exit;
  end

  -- nalezy obliczyc na nowo
  if(:fromprshoper is not null) then begin
    select EOPERFACTOR from PRSHOPERS where REF=:fromprshoper
    into :eoperfactor;
  end else if(:fromprshtool is not null) then begin
    select EHOURFACTOR from PRSHTOOLS where REF=:fromprshtool
    into :eoperfactor;
  end
  operfactor = NULL;
  if(:eoperfactor is not null) then begin
    execute procedure STATEMENT_PARSE(:eoperfactor,:key1,:key2,:prefix) returning_values :operfactor;
  end
  if(:operfactor is null) then operfactor = 0;
  ret = :operfactor;
  if (ret is null) then ret = 0;
  -- nalezy zapisac wyliczony parametr
  if(:parref is not null) then begin
    update prcalccols set cvalue=:ret where ref=:parref;
  end else begin
    insert into prcalccols(PARENT,CVALUE,EXPR,DESCRIPT,CALCTYPE,PRCOLUMN)
    values(:key2,:ret,:paramname,'Wspólczynnik',1,null);
  end
end^
SET TERM ; ^
