--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ZAM_OBL_REAL(
      ZAM integer,
      REAAL integer)
   as
declare variable ilreal decimal(14,4);
declare variable toreal decimal(14,4);
declare variable toilosc decimal(14,4);
declare variable tozreal decimal(14,4);
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable cena decimal (14,2);
declare variable mag char(3);
declare variable rabat decimal(14,2);
declare variable numer integer;
begin
  /* funkja zwiksza ilosci zrealizowane na pozycjach dokumentu na podstawie
    wartoci pól ILREAL z pozycji realizacji */
    for select MAGAZYN,KTM,WERSJA,CENACEN,RABAT,ILREAL from POZZAM where ZAMOWIENIE=:reaal
     into :mag, :ktm, :wersja, :cena, :rabat, :ilreal
    do begin
       if(:ilreal > 0) then
       for select NUMER, ILOSC, ILZREAL from POZZAM where MAGAZYN=:mag AND KTM=:ktm AND WERSJA=:wersja AND ZAMOWIENIE=:zam into :numer, :toilosc, :tozreal
       do begin
         if(:ilreal > 0) then begin
           if(:tozreal is null) then tozreal = 0;
           if(:toilosc is null) then toilosc = 0;
           if(:ilreal > (:toilosc - :tozreal)) then toreal = (toilosc - tozreal);
           else toreal = ilreal;
           toreal = toreal + tozreal;
           update POZZAM set ILZREAL=:tozreal where ZAMOWIENIE=:zam AND NUMER=:numer;
           ilreal = ilreal - toreal;
         end
       end
    end
end^
SET TERM ; ^
