--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_HH_PRSCHEDOPER_COMMIT(
      PRSCHEDOPER integer,
      EMPLOYEE integer,
      QUANTITY numeric(14,4),
      MAKETIMEFROM varchar(40) CHARACTER SET UTF8                           ,
      MAKETIMETO varchar(40) CHARACTER SET UTF8                           )
   as
declare variable prschedguide integer;
declare variable prschedoperfrom integer;
begin
  select guide from prschedopers where ref = :prschedoper
    into prschedguide;
  insert into propersraps (employee, amount, prschedoper, regtime, maketimefrom, maketimeto, prschedguide)
    values (:employee, :quantity, :prschedoper, current_timestamp(0), :maketimefrom, :maketimeto, :prschedguide);
  prschedoperfrom = null;
  select first 1 f.ref
    from prschedoperdeps d
      left join prschedopers f on (f.ref = d.depfrom)
    where d.depto = :prschedoper and f.status < 3
    into prschedoperfrom;
  if (prschedoperfrom is null) then
    update prschedopers o set o.status = 3 where o.ref = :prschedoper and o.amountin = o.amountresult;
end^
SET TERM ; ^
