--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_TAXSCALEMIN(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable taxyear integer;
begin
  --MWr Personel: zwraca minimalna wartosc skali podatkowej w danym roku TAXYEAR

  execute procedure ef_taxyear(:employee, :payroll,:prefix)
    returning_values taxyear;

  select min(taxscale)
    from etaxlevels
    where taxyear = :taxyear
    into :ret;

  if (ret is null) then
    exception NO_TAX_LEVELS_FOR_THIS_YEAR;

  suspend;
end^
SET TERM ; ^
