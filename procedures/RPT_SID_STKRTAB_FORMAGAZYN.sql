--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_SID_STKRTAB_FORMAGAZYN(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      ODKTM varchar(40) CHARACTER SET UTF8                           ,
      DOKTM varchar(40) CHARACTER SET UTF8                           ,
      DATA timestamp,
      ZAKRES smallint)
  returns (
      REF integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      WERSJAREF integer,
      NAZWAT varchar(100) CHARACTER SET UTF8                           ,
      NAZWAW varchar(30) CHARACTER SET UTF8                           ,
      MIARA varchar(5) CHARACTER SET UTF8                           ,
      STAN numeric(14,4),
      WARTOSC numeric(14,2),
      STANMIN numeric(14,4),
      STANMAX numeric(14,4),
      TYP smallint)
   as
begin
  ref = 0;
  for select MAG_STANY.RKTM, MAG_STANY.rwersja, sum(MAG_STANY.STAN), sum(MAG_STANY.wartosc)
  from mag_stany(:magazyn, NULL, NULL,NULL,NULL,:data,0,0)
  where
       ((:odKTM is null) or (:odktm = '') or (:odktm <= MAG_STANY.RKTM))
   and ((:doKTM is null) or (:doktm = '') or (:doktm >= MAG_STANY.RKTM))
  group by MAG_STANY.rktm, MAG_STANY.rwersja
  order by RKTM,RWERSJA
  into :ktm, :wersja, :stan, :wartosc
  do begin
    execute procedure STKRTAB_GETMINMAX(:magazyn, :ktm, :wersja) returning_values :stanmin, :stanmax;
    select WERSJE.REF, WERSJE.nazwa, WERSJE.NAZWAT, WERSJE.MIARA from WERSJE where WERSJE.ktm = :ktm and wersje.nrwersji = :wersja into :wersjaref, :nazwaw, 

:nazwat, :miara;
    if(:stan < :stanmin) then typ = 0;
    else if(:stan > :stanmax) then typ = 2;
    else typ = 1;
    if((:zakres = 0)
     or (:zakres = 1 and :typ = 0)
     or (:zakres = 2 and :typ = 2)
     or (:zakres = 3 and :typ <> 1)
    )then begin
      ref = :ref + 1;
      suspend;
    end
  end
end^
SET TERM ; ^
