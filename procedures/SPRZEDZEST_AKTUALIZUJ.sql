--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SPRZEDZEST_AKTUALIZUJ(
      ZESTAWIENIE integer)
  returns (
      STATUS integer,
      NUMROWS integer)
   as
DECLARE VARIABLE RODZAJ INTEGER;
DECLARE VARIABLE SPRZEDAWCA INTEGER;
DECLARE VARIABLE WARTNET NUMERIC(14,2);
DECLARE VARIABLE WARTBRU NUMERIC(14,2);
DECLARE VARIABLE WARTNETZAM NUMERIC(14,2);
DECLARE VARIABLE WARTBRUZAM NUMERIC(14,2);
DECLARE VARIABLE KOSZT NUMERIC(14,2);
DECLARE VARIABLE TYP CHAR(1);

DECLARE VARIABLE ODDATY TIMESTAMP;
DECLARE VARIABLE DODATY TIMESTAMP;
DECLARE VARIABLE ZAMFAK INTEGER;
DECLARE VARIABLE DATA TIMESTAMP;
DECLARE VARIABLE ROZLI INTEGER;
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE RES INTEGER;
DECLARE VARIABLE ODBRUTTO INTEGER;
DECLARE VARIABLE STAT CHAR(1);
DECLARE VARIABLE KOREKTA INTEGER;
DECLARE VARIABLE SPRZEDNAG INTEGER;
DECLARE VARIABLE SPRZEDDEFREF INTEGER;
DECLARE VARIABLE PROCEDURAPOZ VARCHAR(100);
DECLARE VARIABLE TYPPOZ SMALLINT;
DECLARE VARIABLE PROCENTDEF NUMERIC(14,2);
DECLARE VARIABLE WSPOLCZYNNIKKOR NUMERIC(14,2);
DECLARE VARIABLE SQL VARCHAR(255);
DECLARE VARIABLE REFFAK INTEGER;
DECLARE VARIABLE REFZAM INTEGER;
declare variable val1 numeric(14,2);
declare variable val2 numeric(14,2);
declare variable val3 numeric(14,2);
begin
  STATUS = -1;
  NUMROWS = 0;
  select SPRZEDZEST.SPRZEDNAG from SPRZEDZEST where SPRZEDZEST.REF=:ZESTAWIENIE into :sprzednag;
  select SPRZEDDEF, ODDATY,DODATY from SPRZEDNAG where SPRZEDNAG.REF=:sprzednag into :sprzeddefref, :oddaty,:dodaty;
  select sprzeddef.procedurapoz from sprzeddef where REF=:sprzeddefref into :procedurapoz;
  select RODZAJ,SPRZEDAWCA,TYP,STATUS from SPRZEDZEST where REF=:ZESTAWIENIE into :rodzaj, :sprzedawca, :typ, :stat;
  if(:stat is null) then stat = 'O';
  if(:stat <> 'O') then exception SPRZEDZEST_AKTUALIZUJ_STATUS_NI;
  if(:rodzaj is null) then rodzaj = 0;
  if(:sprzedawca is null) then exit;
  select PROBRUTTO from SPRZEDAWCY where ref=:sprzedawca into :odbrutto;
  if((:oddaty is null) or (:dodaty is null)) then exception SPRZEDZEST_AKTUALIZUJ_BEZ_DAT;
  update SPRZEDZESTP set CHECKED = 0 where ZESTAWIENIE=:ZESTAWIENIE;
  if(:rodzaj = 0) then begin
    for select  REF, DATA, SUMWARTNETZL, SUMWARTBRUZL, KOSZT, SPRZEDROZL, TYPFAK.KOREKTA
    from NAGFAK join TYPFAK on ( NAGFAK.TYP = TYPFAK.SYMBOL) where
          NAGFAK.akceptacja in (1,8) AND
          (DATASPRZ >= :ODDATY) and
          (DATASPRZ <= :DODATY) and
          (SPRZEDAWCA=:sprzedawca)
       into :zamfak, :data, :wartnet, :wartbru, :koszt, :rozli, :korekta
    do begin
       if(:rozli is null) then rozli = 0;
       if(:rozli > 0) then begin
        /*pozycja rozliczona - na bieżącym lub na inny - aktualizacja tylko w trybie uzupenienia danych */
         cnt = null;
         select count(*) from sprzedzestp where fak=:zamfak and zestawienie=:zestawienie into :cnt;
         if(:cnt is null) then cnt =0;
           if(:typ = 'R' and :cnt > 0) then begin
           execute procedure SPRZEDZEST_ADDPOZ(:zestawienie,:sprzedawca,0,:zamfak,0,
             :data,:wartnet,:wartbru,:koszt, :korekta, :odbrutto,NULL, NULL,NULL, NULL, NULL) returning_values :res;
           numrows = :numrows + :res;
         end
       end else begin
           execute procedure SPRZEDZEST_ADDPOZ(:zestawienie,:sprzedawca,0,:zamfak,0,
             :data,:wartnet,:wartbru,:koszt, :korekta, :odbrutto,NULL, NULL,NULL, NULL, NULL) returning_values :res;
           numrows = :numrows + :res;
       end
    end
  end else if(:rodzaj = 1) then begin
    for select ref,DATAWE,SUMWARTRNET, SUMWARTRBRU,SPRZEDROZL from NAGZAM
      where ((TYP=2) or (TYP = 3)) AND
             (DATAREAL >= :oddaty) and
             (DATAREAL <= :dodaty) and
             (sprzedawca = :sprzedawca)
      into :zamfak, :data, :wartnet, :wartbru, :rozli
    do begin
       if(:rozli is null) then rozli = 0;
       if(:rozli > 0) then begin
         /*pozycja rozliczona - na bieżącym lub na inny - aktualizacja tylko w trybie uzupenienia danych */
         cnt = null;
         select count(*) from sprzedzestp where fak=:zamfak and zestawienie=:zestawienie into :cnt;
         if(:cnt is null) then cnt =0;
         if(:typ = 'Z' and cnt > 0) then begin
           execute procedure SPRZEDZEST_ADDPOZ(:zestawienie,:sprzedawca,:zamfak,0,0,
             :data,:wartnet,:wartbru,:koszt, :korekta, :odbrutto, NULL, NULL, NULL, NULL, NULL) returning_values :res;
           numrows = :numrows + :res;
         end
       end else begin
           execute procedure SPRZEDZEST_ADDPOZ(:zestawienie,:sprzedawca,:zamfak,0,0,
             :data,:wartnet,:wartbru,:koszt, :korekta, :odbrutto, NULL, NULL, NULL, NULL, NULL) returning_values :res;
           numrows = :numrows + :res;
       end
    end
  end else if((:rodzaj = 2) or (:rodzaj=3))then exception SPRZEDZEST_AKTUALIZUJ_NOT_DONE;
  else if(:rodzaj = 4) then begin
    sql = 'select * from '||:procedurapoz||'('||:zestawienie||','||:sprzedawca||','''||cast(:oddaty as date)||''','''||cast(:dodaty as date)||''')';
    for execute statement :sql
    into :reffak, :refzam, :typpoz,:data,:wartnet,:wartbru,:koszt, :korekta, :odbrutto, :procentdef, :wspolczynnikkor, :val1, :val2, :val3
    do begin
      if(:reffak > 0 or (:refzam > 0)) then begin
        execute procedure SPRZEDZEST_ADDPOZ(:zestawienie,:sprzedawca,:refzam,:reffak,:typpoz,
          :data,:wartnet,:wartbru,:koszt, :korekta, :odbrutto, :procentdef, :wspolczynnikkor, :val1, :val2, :val3) returning_values :res;
        numrows = :numrows + :res;
      end
    end
  end
  /* usuniecie rekordów, których juz nie powinno byc - nie sprawdzone */
  delete from SPRZEDZESTP where CHECKED=0;
  update SPRZEDZEST set DATALAST=current_date where REF=:zestawienie;
  status = 1;
end^
SET TERM ; ^
