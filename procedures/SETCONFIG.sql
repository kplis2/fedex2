--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SETCONFIG(
      AKRONIM varchar(255) CHARACTER SET UTF8                           ,
      CVALUE varchar(1024) CHARACTER SET UTF8                           ,
      FROMDATE varchar(20) CHARACTER SET UTF8                            default null)
   as
declare variable multi smallint;
declare variable hist smallint;
declare variable currentbranch varchar(255);
declare variable symbol varchar(255);
begin
  select akronim, multi, historia
    from konfig where akronim = :akronim
    into :symbol, :multi, :hist;
  if (multi > 0) then
  begin
    select pvalue from globalparams
      where connectionid=current_connection and psymbol= 'KONFIGDOMAIN:'||:multi
    into :currentbranch;
    if (currentbranch is null or currentbranch = '') then
      currentbranch = user;
    if (:hist = 1) then
    begin
      if (:fromdate is null or :fromdate = '') then
        fromdate = current_date;
      update or insert into konfigvals(AKRONIM,ODDZIAL,WARTOSC,FROMDATE) values (:symbol, :currentbranch, :cvalue, :fromdate)
        matching(AKRONIM,ODDZIAL,FROMDATE);
    end
    else
    begin
      update or insert into konfigvals(AKRONIM,ODDZIAL,WARTOSC,FROMDATE) values (:symbol, :currentbranch, :cvalue, '1900-01-01')
        matching(AKRONIM,ODDZIAL);
    end
  end else
    update or insert into konfig(AKRONIM,WARTOSC) values (:symbol, :cvalue)
      matching(AKRONIM);
end^
SET TERM ; ^
