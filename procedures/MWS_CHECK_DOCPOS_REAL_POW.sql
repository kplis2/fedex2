--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CHECK_DOCPOS_REAL_POW(
      WH varchar(3) CHARACTER SET UTF8                           ,
      DOCID integer,
      VERSIN integer,
      DEEP smallint,
      MAXLEVEL integer,
      STATUSIN smallint)
  returns (
      POSLEFT integer,
      POSTOREAL integer)
   as
declare variable VERS integer;
declare variable QUANTITY numeric(14,4);
declare variable QUANTITYWH numeric(14,4);
declare variable POSSTATUS smallint;
declare variable MWSGENPARTS varchar(100);
declare variable QUICK smallint;
declare variable TAKEFROMZEROS varchar(100);
declare variable TAKEFROMZERO smallint;
declare variable QUANTITYWHZERO numeric(14,4);
declare variable QUANTITYPREPARED numeric(14,4);
declare variable QUANTITYPREPAREDC numeric(14,4);
declare variable DOCGROUP integer;
declare variable NOTPREPARED numeric(14,4);
declare variable BUFFOR smallint;
declare variable BUFFORS varchar(100);
declare variable LOT integer;
declare variable MWSCONSTLOC integer;
declare variable DOCPOSID integer;
declare variable INDYVIDUALLOT smallint;
declare variable MAGBREAK smallint;
declare variable x_partia string20;
declare variable x_serial_no integer_id;
declare variable np string3;
declare variable partiapoborowa date_id;
declare variable partiabuforowa date_id;
begin
  /*
  POSLEFT - ilosc pozycji na dokumencie magazynowym na ktre nie zostaly wygenerowane pozycje zlecnia magazynowego na wymagana ilosć (brane z pola dokumpoz.ilosconmwsacts )
  POSTOREAL  - wyliczona w  docpos2real ilosc pozycji możliwych do zrealizowania

  POSSTATUS:
  0 = nie mozna generować zlecen (brak stanów na lokacjach dostepnych dla pikerow lub stany sa ale niewystarczajace a nie mozna realizowac zleceń czeciowych)
  1 = mamy cala potrzebną ilosc na lokacjach dostepnych dla pikerow
  2 = realizacja na penej palecie
  3 =
  4 =
  5 = mozna generowac czesc zlecenia (przy ustawionym parametrze dopuszczajacym generowanie czesciowych zlecen)




  */
  select d.grupasped
    from dokumnag d
    where ref = :docid
    into docgroup;
  -- czy mozna wydawac z lokacji buforowych
  execute procedure get_config('MWSTAKEFROMBUFFOR',2) returning_values buffors;
  if (buffors is null or buffors = '') then
    buffor = 0;
  else
    buffor = cast(buffors as smallint);

  -- czy mozna dzilic dokuemty magazynowe na kilka zleceń
  execute procedure get_config('MWSGENPARTS',2) returning_values mwsgenparts;

  -- czy mozna pobierac z lokacji "0"
  execute procedure get_config('MWSTAKEFROMZERO',2) returning_values takefromzeros;
  if (takefromzeros is null or takefromzeros = '') then
    takefromzero = 0;
  else
    takefromzero = cast(takefromzeros as smallint);
  if (deep = 0 and maxlevel = 1) then
    quick = 1;
  else
    quick = 0;
  if (maxlevel > 1) then
    statusin = 2;
  else
    statusin = 1;
  posstatus = 0;
  posleft = 0;
  postoreal = 0;

  --  Przeciez już wczeniej pobralimy sobie docgroup (w Abakusiw wykomentowaem poniższe zapytanie)
  select d.grupasped
    from dokumnag d
    where d.ref = :docid
    into docgroup;
  if (statusin is null) then statusin = 1;
  -- pobieramy ilosc na jaka  potrzebyjemy wygenerowac zlecenia
  for
    select p.wersjaref, coalesce(p.dostawa,0),
        sum(case when (d.mwsdisposition = 1 or coalesce(t1.altposmode,0) = 1) then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0)),
        t.magbreak,p.x_partia, p.x_serial_no, d.typ
      from dokumpoz p
        join towary t on (t.ktm = p.ktm)
        join dokumnag d on (d.ref = p.dokument)
        left join dokumpoz p1 on (p1.ref = p.alttopoz and p.fake = 1)
        left join towary t1 on (t1.ktm = p1.ktm and t1.usluga <> 1)
      where p.dokument = :docid
        and p.genmwsordsafter = 1
        and ((coalesce(p.havefake,0) = 0 and coalesce(p.fake,0) = 0) or
             (coalesce(p.fake,0) = 1 and t1.usluga <> 1 and coalesce(t1.altposmode,0) = 1))
        --and p.iloscl - coalesce(p.ilosconmwsacts,0) > 0
        and ((p.mwsposreal <> 1 and p.mwsposreal <> 2 and p.mwsposreal <> 5) or (p.mwsposreal is null))
        and (p.wersjaref = :versin or :versin is null)
        and t.usluga <> 1
      group by p.dokument, p.wersjaref, coalesce(p.dostawa,0), t.magbreak,
        p.x_partia, p.x_serial_no, d.typ
      into vers, lot, quantity,
        magbreak,
        :x_partia, :x_serial_no, :np
  do begin
    if (magbreak is null) then magbreak = 0;
    if (x_serial_no = 0) then x_serial_no = null;
    if (coalesce(np,'')='') then np = null;

    posleft = posleft + 1;
    posstatus = 0;
    quantitywh = 0;
    quantityprepared = 0;
    notprepared = 0;
    indyviduallot = 0;
    if(:lot > 0) then
    begin
      select d.indywidualdost
        from dostawy d
        where d.ref = :lot
        into :indyviduallot;
    end
    if (vers is not null and quantity > 0 and quantity is not null) then
    begin
      if (quantity is null) then quantity = 0;

      -- pobieramy dostepne ilosci na lokacjach dostepnych dla pikerow
      select sum (m.quantity - m.blocked)
        from mwsstock m
          left join mwsconstlocs c on (c.ref = m.mwsconstloc and c.act > 0)
          left join whareas w on (w.ref = c.wharea)
          left join wersje v on (v.ref = m.vers)
        where m.wh = :wh and m.vers = :vers
          and c.act > 0 and c.locdest in (1,2,3,9,5)
          --and (m.lot = :lot or :lot = 0)
          and ( (:indyviduallot = 1 and m.lot = :lot and c.indyviduallot = 1)
            or (:indyviduallot = 0 and m.lot = :lot and c.indyviduallot = 0)
            or (:lot = 0 and c.indyviduallot = 0)
            or (:lot > 0 and :magbreak < 2)
            or (:lot > 0 and :magbreak = 2 and m.lot = :lot) )
          and (:x_partia is null or m.x_partia = :x_partia)
          and (:x_serial_no is null or m.x_serial_no = :x_serial_no)
          and (:np <> 'NP' or c.ref = 13424)
          and ((m.goodsav in (1,2) and :buffor = 0) or :buffor = 1 or :np = 'NP' )
          and (m.mixedpalgroup = 0 or v.mwscalcmix = 1 or c.takefrommix = 1)
          and (m.x_blocked<>1) --XXX Ldz ZG 126909
        into quantitywh;
      if (quantitywh is null) then quantitywh = 0;

     -- pobieramy ilosci z wstepnej konfekcji (w lobosie i abakusie nie uzywane - chyba bo tabela jest pusta i nigdzie nie wyliczana)
      select sum(coalesce(p.dquantity,0))
        from docpos2prepare p
        where p.docid = :docid and p.vers = :vers and p.lot = :lot
        into quantityprepared;

     -- tu nigdy chyba w abakusie (jak bdzie Ehrle ? jak bedzie w Hermonie ?) nie wejdziemy bo nie wyliczamy docpos2prepare
      if (quantityprepared > 0) then
      begin
        quantityprepared = 0;
        select sum(a.quantityc)
          from mwsacts a
            left join mwsords o on (o.ref = a.mwsord)
          where a.mwsordtypedest = 2 and a.doctype = 'P' and a.docid = :docid
            and a.vers = :vers and a.status = 5 and o.status = 5 and a.regtime > current_date - 7
            and (a.lot = :lot or :lot = 0)
          into quantityprepared;
        if (quantityprepared is null) then quantityprepared = 0;
        select sum(a.quantity)
          from mwsacts a
            left join mwsords o on (o.ref = a.mwsord)
          where a.mwsordtypedest = 2 and a.doctype = 'P' and a.docid = :docid
            and a.vers = :vers and o.status < 5 and a.regtime > current_date - 7
            and (a.lot = :lot or :lot = 0)
          into notprepared;
        if (notprepared is null) then notprepared = 0;
        if (notprepared > 0) then notprepared = 1;
      end
      -- tu nigdy chyba w abakusie (jak bdzie Ehrle ?) nie wejdziemy bo nie wyliczamy docpos2prepare
      if (quantityprepared > 0) then
      begin
        quantitypreparedc = 0;
        select sum(m.quantity - m.blocked)
          from mwsconstlocs c
            left join whsecs s on (s.ref = c.whsec)
            left join mwsstock m on (m.mwsconstloc = c.ref)
            left join mwspallocs p on (p.ref = m.mwspalloc)
          where c.locdest = 9 and c.wh = :wh
            and m.vers = :vers and (p.docgroup = :docgroup or p.docgroup is null)
            and (m.lot = :lot or :lot = 0)
            and (m.x_blocked<>1) --XXX Ldz ZG 126909
            and (:np <> 'NP' or c.ref = 13424)
          into quantitypreparedc;
        if (quantitypreparedc is null) then quantitypreparedc = 0;
        if (quantitypreparedc < quantityprepared) then
          quantityprepared = quantitypreparedc;
      end
      if (quantityprepared is null) then quantityprepared = 0;
      quantitywh = quantitywh + quantityprepared;

      -- jezlei mozemy brac z lokacji "0" to pobieramy stan z tych lokacji
      if (quick = 1 and takefromzero > 0) then
      begin
        select sum (m.quantity - m.blocked)
          from mwsstock m
            left join mwsconstlocs c on (c.ref = m.mwsconstloc)
            left join wersje v on (v.ref = m.vers)
          where m.wh = :wh and c.locdest = 0
            and m.vers = :vers
            and ( (:indyviduallot = 1 and m.lot = :lot and c.indyviduallot = 1)
              or (:indyviduallot = 0 and m.lot = :lot and c.indyviduallot = 0)
              or (:lot = 0 and c.indyviduallot = 0)
              or (:lot > 0 and :magbreak < 2)
              or (:lot > 0 and :magbreak = 2 and m.lot = :lot) )
            and (:x_partia is null or m.x_partia = :x_partia)
            and (:x_serial_no is null or m.x_serial_no = :x_serial_no)
            and m.goodsav = 4 and m.actloc > 0
            and (m.mixedpalgroup = 0 or v.mwscalcmix = 1 or c.takefrommix = 1)
            and (m.x_blocked<>1) --XXX Ldz ZG 126909
            and (:np <> 'NP' or c.ref = 13424)
          into quantitywhzero;
        if (quantitywhzero is null) then quantitywhzero = 0;
        quantitywh = quantitywh + quantitywhzero;
      end

      -- XXX KBI ZG126343 bezwgledne fifo dla towarów wg partii
      if (quantitywh > 0 and :buffor = 0 and exists(select first 1 1 from wersje w where w.ref = :vers and w.x_mws_partie = 1)) then begin
        -- sprawdzamy jaka najstarsza data jest na lokacjach poborowych
        select first 1 m.x_partia
          from mwsstock m
            left join mwsconstlocs c on (c.ref = m.mwsconstloc and c.act > 0)
            left join whareas w on (w.ref = c.wharea)
            left join wersje v on (v.ref = m.vers)
          where m.wh = :wh and m.vers = :vers
            and c.act > 0 and c.locdest in (1,2,3,9,5)
            --and (m.lot = :lot or :lot = 0)
            and ( (:indyviduallot = 1 and m.lot = :lot and c.indyviduallot = 1)
              or (:indyviduallot = 0 and m.lot = :lot and c.indyviduallot = 0)
              or (:lot = 0 and c.indyviduallot = 0)
              or (:lot > 0 and :magbreak < 2)
              or (:lot > 0 and :magbreak = 2 and m.lot = :lot) )
            and (:x_partia is null or m.x_partia = :x_partia)
            and (:x_serial_no is null or m.x_serial_no = :x_serial_no)
            and (m.goodsav in (1,2))
            and (m.mixedpalgroup = 0 or v.mwscalcmix = 1 or c.takefrommix = 1)
            and (m.x_blocked<>1) --XXX Ldz ZG 126909
            and (:np  <> 'NP' or c.ref = 13424)
          order by m.x_partia
        into :partiapoborowa;
    
        -- sprawdzamy jaka najstarsza data jest na lokacjach poborowych
        select first 1 m.x_partia
          from mwsstock m
            left join mwsconstlocs c on (c.ref = m.mwsconstloc and c.act > 0)
            left join whareas w on (w.ref = c.wharea)
            left join wersje v on (v.ref = m.vers)
          where m.wh = :wh and m.vers = :vers
            and c.act > 0 and c.locdest in (1,2,3,9,5)
            --and (m.lot = :lot or :lot = 0)
            and ( (:indyviduallot = 1 and m.lot = :lot and c.indyviduallot = 1)
              or (:indyviduallot = 0 and m.lot = :lot and c.indyviduallot = 0)
              or (:lot = 0 and c.indyviduallot = 0)
              or (:lot > 0 and :magbreak < 2)
              or (:lot > 0 and :magbreak = 2 and m.lot = :lot) )
            and (:x_partia is null or m.x_partia = :x_partia)
            and (:x_serial_no is null or m.x_serial_no = :x_serial_no)
            and (m.goodsav = 0)
            and (m.mixedpalgroup = 0 or v.mwscalcmix = 1 or c.takefrommix = 1)
            and (m.x_blocked<>1) --XXX Ldz ZG 126909
            and (:np  <> 'NP' or c.ref = 13424)
          into :partiabuforowa;
          if (partiabuforowa is not null and partiabuforowa < partiapoborowa) then
            quantitywh = 0;
      end
      -- XXX Koniec KBI ZG126343 bezwgledne fifo dla towarów wg partii

      -- jesli ilosc potrzebna jest wieksza od ilosci na lokacjach dostepnych dla pikerow
      if (quantity > quantitywh) then
      begin
        if (maxlevel = 1 and quantitywh > 0 and mwsgenparts = '1') then
          -- mozemy genrowac zlecenia cesciowe
          posstatus = 5;
        else
         -- nie możemy generowac zlecen czesciowych
          posstatus = 0;
      end
      else
        posstatus = statusin;
      if (notprepared = 1) then posstatus = 0;

      -- Naliczamy tabele pozycji dokumentow do realizacji
      -- posstatus 1 = mamy cala potrzebna ilosc na lokacjach dostepnych dla pikerow, posstatus 5 - mozemy realizowac zlecenia czesciowe
      if (posstatus > 0) then
        insert into docpos2real (docposid, docid, vers, posreal, genmwsordafter, lot, docgroup, doctype)
          select p.ref, p.dokument, p.wersjaref, :posstatus, 1, coalesce(p.dostawa,0), d.grupasped, 'M'
            from dokumpoz p
              left join dokumnag d on (d.ref = p.dokument)
              left join docpos2real pr on (pr.docposid = p.ref)
            where p.dokument = :docid
              and p.genmwsordsafter = 1
             -- and p.iloscl - coalesce(p.ilosconmwsacts,0) > 0
              and p.wersjaref = :vers
              and pr.docposid is null;
    end
  end
  -- sprawdzamy czy mozna wygenerowac zlecenie na pelna palete
  if (versin is null) then
  begin
    for
      select xk.mwsconstloc
        from xk_mws_take_full_pal_check(:wh,:docgroup,null,0) xk
        group by xk.mwsconstloc
        into mwsconstloc
    do begin
      for
        select s.vers, s.lot
          from mwsstock s
          where s.mwsconstloc = :mwsconstloc
            and (s.x_blocked<>1) --XXX Ldz ZG 126909
          group by s.vers, s.lot
          into vers, lot
      do begin
        for
          select p.ref
            from dokumpoz p
              join towary t on (p.ktm = t.ktm)
              left join dokumpoz p1 on (p1.ref = p.alttopoz and p.fake = 1)
              left join towary t1 on (t1.ktm = p1.ktm and t1.usluga <> 1)
            where p.dokument = :docid
              and p.wersjaref = :vers
              and (coalesce(p.dostawa,0) = :lot or coalesce(p.dostawa,0) = 0)
              and ((coalesce(p.havefake,0) = 0 and coalesce(p.fake,0) = 0) or
                   (coalesce(p.fake,0) = 1 and t1.usluga <> 1 and coalesce(t1.altposmode,0) = 1))
              and t.usluga <> 1
            into docposid
        do begin
          delete from docpos2real where docposid = :docposid;
          insert into docpos2real (docposid, docid, vers, posreal, genmwsordafter, lot, docgroup, doctype)
            select p.ref, p.dokument, p.wersjaref, 2, 1, coalesce(p.dostawa,0), d.grupasped, 'M'
              from dokumpoz p
                join dokumnag d on (d.ref = p.dokument)
                join towary t on (p.ktm = t.ktm)
                left join docpos2real pr on (pr.docposid = p.ref)
                left join dokumpoz p1 on (p1.ref = p.alttopoz and p.fake = 1)
                left join towary t1 on (t1.ktm = p1.ktm and t1.usluga <> 1)
              where p.ref = :docposid
                and p.genmwsordsafter = 1
                and ((p.havefake = 0 and p.fake = 0 and coalesce(t1.altposmode,0) = 0) or
                     (p.fake = 1 and t1.usluga <> 1 and coalesce(t1.altposmode,0) = 1))
                and t.usluga <> 1;
                --and p.iloscl - coalesce(p.ilosconmwsacts,0) > 0;
        end
      end
    end
  end
  select count(p.ref)
    from dokumpoz p
      left join towary t on (t.ktm = p.ktm)
    where dokument = :docid
      and coalesce(p.havefake,0) = 0
      and p.genmwsordsafter = 1
      --and iloscl - coalesce(ilosconmwsacts,0) > 0
      and t.usluga <> 1
    into posleft;
  select count(docposid) from docpos2real where docid = :docid
    into postoreal;
end^
SET TERM ; ^
