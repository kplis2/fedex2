--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WF_GETFIELDSVALUES(
      WFINSTANCEREF WFINSTANCE_ID NOT NULL)
  returns (
      WFFIELDNAME STRING60,
      WFFIELDVALUE STRING)
   as
begin
  for select wf.fieldname,wf.fieldvalue from wffield wf
  where wf.wfinstanceref=:wfinstanceref
  into wffieldname, wffieldvalue
  do suspend;
end^
SET TERM ; ^
