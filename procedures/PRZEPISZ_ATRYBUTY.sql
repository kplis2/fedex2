--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRZEPISZ_ATRYBUTY(
      KTM varchar(40) CHARACTER SET UTF8                           )
   as
declare variable i smallint;
  declare variable cecha varchar(30);
begin
  select ref from wersje where (ktm = :ktm and nrwersji = 0) into :i;
  insert into expimp (ref) values (:i);
  for select A.cecha from atrybuty A
      left join defcechy D on (D.symbol = A.cecha and D.wersje=0)
      where (A.ktm = :ktm and A.nrwersji <> 0)
      into :cecha
  do begin
    update atrybuty set nrwersji = 0, wersjaref = :i
      where ktm = :ktm and cecha = :cecha;
  end
/*  update towpliki set wersja=0, wersjaref=:i
    where ktm = :ktm and wersja<>0;*/
  suspend;
end^
SET TERM ; ^
