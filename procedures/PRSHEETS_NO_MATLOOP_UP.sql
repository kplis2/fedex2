--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHEETS_NO_MATLOOP_UP(
      SHEET integer,
      SHEETUP integer,
      PETLA integer)
  returns (
      TEST integer)
   as
DECLARE VARIABLE SYMBOLDOWN VARCHAR(20);
DECLARE VARIABLE LSHEET INTEGER;
DECLARE VARIABLE LSHEETUP INTEGER;
DECLARE VARIABLE LTEST INTEGER;
begin
  petla = :petla - 1;
  if(:PETLA <= 0) then exception prsheets_loop 'Istnieje petla w strukturze. Akceptacja niemożliwa.';
  for
    select ps.symbol, ps.ref
      from prshmat pm
        join prsheets ps on (pm.sheet = ps.ref)
      where pm.subsheet = :sheetup
      into :symboldown, :lsheet
  do begin
    if(:sheet = :lsheet) then exception prsheets_loop 'Niedozwolony cykl z kartą: '||:symboldown||'. Akceptacja niemożliwa.';
    for
      select prsheets_no_matloop_up.test
        from  prsheets_no_matloop_up(:sheet, :lsheet, :petla)
        into :test
     do begin
       test = 1;
     end
  end
  suspend;
END^
SET TERM ; ^
