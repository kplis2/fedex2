--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CLOSE_DISPOSITION_LIST(
      DOCIDIN integer,
      CHECKORDER smallint)
  returns (
      DOCID integer)
   as
declare variable ORDERID integer;
begin
  if (checkorder = 0) then begin
    docid = docidin;
    suspend;
  end
  else begin
    for
      select p.fromzam
        from dokumpoz p
        where p.dokument = :docidin
        group by p.dokument, p.fromzam
        into orderid
    do begin
      for
        select p.dokument
          from dokumpoz p
          where p.fromzam = :orderid
          group by p.fromzam, p.dokument
          into docid
      do begin
        suspend;
      end
    end
  end
end^
SET TERM ; ^
