--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CALCULATE_FSBALANCES(
      SLODEF integer,
      SLOPOZ integer)
   as
declare variable DEBIT numeric(14,2);
declare variable CREDIT numeric(14,2);
declare variable FSBALANCES integer;
declare variable KONTOFK KONTO_ID;
declare variable WALUTA varchar(3);
declare variable DICTGRP integer;
declare variable COMPANY integer;
declare variable VBTRANAMOUNTBANK numeric(14,2);
begin
  select first 1 r.dictgrp
    from rozrach r
    where r.slodef = :slodef
      and r.slopoz = :slopoz
    into :dictgrp;

  delete from fsbalances f where f.dictgrp = :dictgrp;

  for
    select r.baldebit, r.balcredit, r.kontofk, r.company, r.waluta, r.slodef, r.slopoz, r.btranamountbank
      from rozrach r
      where r.dictgrp = :dictgrp
        and r.winien<>r.ma
      order by r.kontofk,r.symbfak
      into :debit, :credit, :kontofk, :company, :waluta, :slodef, :slopoz, :vbtranamountbank
  do begin

    -- dodanie sald
    fsbalances = null;
    select ref from fsbalances
      where account=:kontofk and dictdef=:slodef and dictpos=:slopoz and currency=:waluta
        and company = :company
      into :fsbalances;

    if (fsbalances is null) then
      insert into fsbalances (company, account, dictdef, dictpos, debit, credit, currency, dictgrp)
        values (:company, :kontofk, :slodef, :slopoz, :debit, :credit, :waluta, :dictgrp);
    else
      update fsbalances
        set debit=debit+:debit, credit=credit+:credit, dictgrp=:dictgrp, btranamountbank = :vbtranamountbank
        where ref = :fsbalances;

    if (dictgrp is not null) then
      update fsbalances
        set grpdebit=grpdebit+:debit, grpcredit=grpcredit+:credit, grpbtranamountbank = grpbtranamountbank + :vbtranamountbank
        where dictgrp=:dictgrp and company = :company and currency = :waluta;
  end
end^
SET TERM ; ^
