--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PARSESTRING(
      STRINGIN varchar(1024) CHARACTER SET UTF8                           ,
      SEPARATOR varchar(5) CHARACTER SET UTF8                           )
  returns (
      STRINGOUT varchar(1024) CHARACTER SET UTF8                           ,
      LP integer)
   as
declare variable newstringin varchar(1024);
declare variable tmp varchar(1024);
begin
  lp = 1;
  if (position(:separator in  :stringin)= 0 or position(:separator in  :stringin) = coalesce(char_length(:stringin),0)) then -- [DG] XXX ZG119346
  begin
    stringout = replace(:stringin, :separator, '');
    suspend;
    lp = lp + 1;
  end else begin
    while (position(:separator in  :stringin)>0) do
    begin
      stringout = substring(stringin from 1 for position(:separator in  :stringin) -1);
      stringin = substring(stringin from position(:separator in  :stringin) +coalesce(char_length(:separator),0) for coalesce(char_length(stringin),0)); -- [DG] XXX ZG119346
      if (coalesce(:stringout,'') <> '') then
      begin
        suspend;
        lp = lp + 1;
      end
      if (position(:separator in  :stringin)= 0 and coalesce(char_length(stringin),0) > 0) then -- [DG] XXX ZG119346
      begin
        stringout = replace(:stringin, :separator, '');
        suspend;
        lp = lp + 1;
      end
    end
  end
end^
SET TERM ; ^
