--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWIAD_GET_2ND_DB(
      FORID varchar(1024) CHARACTER SET UTF8                            = null,
      TAKEFULLBODY smallint = 1,
      FORMAILBOX integer = null,
      FORFOLDER integer = null,
      FORSEARCHSTRING varchar(255) CHARACTER SET UTF8                            = null)
  returns (
      REF integer,
      TYTUL STRING2048_UTF8,
      DATA timestamp,
      OD STRING1024_UTF8,
      DOKOGO STRING2048_UTF8,
      CC STRING2048_UTF8,
      DELIVEREDTO varchar(1024) CHARACTER SET UTF8                           ,
      IDWATKU integer,
      STAN smallint,
      OPERATOR integer,
      MAILBOX integer,
      FOLDER varchar(1024) CHARACTER SET UTF8                           ,
      ID varchar(1024) CHARACTER SET UTF8                           ,
      UID integer,
      IID integer,
      TRYB integer,
      TRESC MEMO5000_UTF8,
      FULLHTMLBODY BLOB_UTF8,
      FORMATFIELD varchar(255) CHARACTER SET UTF8                           ,
      HASATTACHMENTS smallint)
   as
declare variable POS integer;
declare variable stmt string255;
declare variable CONNECTION string255 not null;
declare variable USR string255 not null;
declare variable PASSWD string255 not null;
declare variable fullbodyfield string255;
begin
  connection = (select cvalue from GET_CONFIG('IMAP.2DB.CONNECTION',0));
  usr = (select cvalue from GET_CONFIG('IMAP.2DB.USER',0));
  passwd = (select cvalue from GET_CONFIG('IMAP.2DB.PASSWORD',0));

  if(takefullbody=1) then
    fullbodyfield = 'e.fullhtmlbody';
  else
    fullbodyfield = 'null fullhtmlbody';

  if(:forid is not null and :forid<>'') then begin
    -- zwr?c podjedynczego maila
    select e.REF, e.DATA,   e.IDWATKU, e.STAN, e.OPERATOR,e.DELIVEREDTO, e.MAILBOX, d.mailfolder,
         e.ID, e.UID, e.IID, e.TRYB
    from emailwiad e
    left join deffold d on d.ref=e.folder
    where id=:forid
    into :ref, :data, :idwatku, :stan, :operator,DELIVEREDTO, :mailbox, :folder,
         :id, :uid, :iid, :tryb;

    if(ref is not null) then
    begin
      execute statement ('select e.TYTUL,e.OD, e.DOKOGO, e.CC, '||
        'e.tresc,'||fullbodyfield||' from emailwiad e where e.id = :id') (id := :id)
        on external :connection
        as user :usr
        password :passwd
        into TYTUL,OD, DOKOGO, CC, tresc, fullhtmlbody;
    end

    formatfield = '';
    if(:stan=0) then formatfield = '*=*=%B*';
    hasattachments = 0;
    if(exists(select first 1 1 from emailzal where emailwiad=:ref)) then hasattachments = 1;
    suspend;
  end else begin

    stmt = 'select e.id, e.TYTUL,e.OD, e.DOKOGO, e.CC, '||
        'e.tresc,'||fullbodyfield||' from emailwiad e where e.mailbox=:mailbox';

    if(:forsearchstring<>'') then
    begin
      forsearchstring = '%'||lower(:forsearchstring)||'%';
      stmt = stmt || ' and (e.tytul||e.od||e.dokogo||e.cc||e.tresc like :search)';
    end else
    begin
      stmt = stmt || ' or :search is null';
    end

    for execute statement (stmt) (mailbox:=formailbox, search:=forsearchstring)
        on external :connection
        as user :usr
        password :passwd
        into id,TYTUL,OD, DOKOGO, CC, tresc, fullhtmlbody
    do begin
      REF=null;
      select e.REF, e.DATA, e.IDWATKU, e.STAN, e.OPERATOR, e.MAILBOX, d.mailfolder,
        e.UID, e.IID, e.TRYB, e.deliveredto
      from emailwiad e
        left join deffold d on d.ref=e.folder
      where e.id=:id and e.mailbox=:formailbox and e.folder = :forfolder and e.stan<>5
      into :ref, :data, :idwatku, :stan, :operator, :mailbox, :folder,
        :uid, :iid, :tryb, deliveredto;

      pos = position ('<' in :od);
      if(:pos>0) then od = substring(:od from 1 for :pos-1);
      formatfield = '';
      if(:stan=0) then formatfield = '*=*=%B*';
      hasattachments = 0;
      if(exists(select first 1 1 from emailzal where emailwiad=:ref)) then hasattachments = 1;
      if(ref is not null) then
        suspend;
    end
  end
end^
SET TERM ; ^
