--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_PRINT_LOC(
      MWSCONSTLOC STRING20,
      MODE SMALLINT_ID)
  returns (
      ZPL STRING1024)
   as
declare variable tmp smallint_id;
declare variable mwsconstloc_ref integer_id;
declare variable mwspalloc_ref integer_id;
declare variable len integer_id;
begin
-- do drukowania malych 35x75 i duzych etykiet 100x150
  zpl = '';
  tmp = 0;
  mwsconstloc = upper(trim(:mwsconstloc));
  len = strlen(:mwsconstloc);
-- zakladanie wozkow, jesli nie ma
  if ((MWSCONSTLOC similar to 'W[[:DIGIT:]]{3}' or MWSCONSTLOC similar to 'K[[:DIGIT:]]{3}') and
   (not exists(select first 1 1 from mwsconstlocs m where m.symbol = :mwsconstloc))) then begin

    execute procedure gen_ref('MWSCONSTLOCS') returning_values mwsconstloc_ref;
    insert into mwsconstlocs (ref, symbol, wh, mwsconstloctype, stocktaking, goodssellav, l, w, h, volume, act)
      values(:mwsconstloc_ref, :mwsconstloc, 'MWS', null, 0, 0, 90, 130, 200, 2.34, 0);

    execute procedure gen_ref('MWSPALLOCS') returning_values mwspalloc_ref;
    insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status)
      select :mwspalloc_ref, :mwsconstloc, ref, 2, 0
        from mwsconstlocs
        where ref = :mwsconstloc_ref;
  end
  if (mode = 0) then begin --mala etykieta
    zpl = 'CT~~CD,~CC^~CT~
^XA~TA000~JSN^LT0^MNW^MTD^PON^PMN^LH0,0^JMA^PR5,5~SD15^JUS^LRN^CI0^XZ
^XA
^MMT
^PW599
^LL0280
^LS0';
  if (len <= 4) then -- centrownie dla 4 znakow
    zpl = zpl ||'^BY4,3,210^FT130,240^BCN,,Y,N';
  else
    zpl = zpl ||'^BY3,3,210^FT30,240^BCN,,Y,N';
  zpl = zpl ||'^FD>:'||coalesce(mwsconstloc, '')||'^FS
^PQ1,0,1,Y^XZ';
  end

  if (mode = 1) then begin --duza etykieta
    zpl = 'CT~~CD,~CC^~CT~
^XA~TA000~JSN^LT0^MNW^MTD^PON^PMN^LH0,0^JMA^PR5,5~SD15^JUS^LRN^CI0^XZ
^XA
^MMT
^PW599
^LL0280
^LS0';
  if (len <= 4) then -- centrownie dla 4 znakow
    zpl = zpl ||'^BY5,3,550^FT50,400^BCR,,Y,N';
  else
    zpl = zpl ||'^BY5,3,550^FT50,200^BCR,,Y,N';
  zpl = zpl ||'^FD>:'||coalesce(mwsconstloc, '')||'^FS
^PQ1,0,1,Y^XZ';
  end
  suspend;
end^
SET TERM ; ^
