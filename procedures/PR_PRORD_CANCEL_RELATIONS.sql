--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_PRORD_CANCEL_RELATIONS(
      PRORD integer,
      PRPOZZAM integer)
   as
declare variable wydania smallint;
declare variable rref integer;
declare variable refto integer;
begin
  if (prpozzam = 0) then prpozzam = null;
  select t.wydania
    from nagzam n
      left join typzam t on (t.symbol = n.typzam)
    where n.ref = :prord
    into wydania;
  if (wydania <> 3) then
    exception prnagzam_error 'Operacja dozwolona tylko dla zleceń produkcyjnych.';
  for
    select r.ref, r.srefto
      from pozzam p
        left join relations r on (r.sreffrom = p.ref and r.stablefrom = 'POZZAM')
      where p.zamowienie = :prord and p.out = 0 and r.relationdef = 1 and r.act > 0
        and (p.ref = :prpozzam or :prpozzam is null)
      into rref, refto
  do begin
    update relations set act = 0 where ref = :rref;
  end
end^
SET TERM ; ^
