--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_E_PAYROLL_SUMMARY(
      PAYROLL integer)
  returns (
      NUM1 integer,
      NAME1 varchar(22) CHARACTER SET UTF8                           ,
      VAL1 numeric(14,2),
      REPSUM1 integer,
      NUM2 integer,
      NAME2 varchar(22) CHARACTER SET UTF8                           ,
      VAL2 numeric(14,2),
      REPSUM2 integer,
      NUM3 integer,
      NAME3 varchar(22) CHARACTER SET UTF8                           ,
      VAL3 numeric(14,2),
      REPSUM3 integer,
      NUM4 integer,
      NAME4 varchar(22) CHARACTER SET UTF8                           ,
      VAL4 numeric(14,2),
      REPSUM4 integer,
      NUM5 integer,
      NAME5 varchar(22) CHARACTER SET UTF8                           ,
      VAL5 numeric(14,2),
      REPSUM5 integer)
   as
DECLARE VARIABLE TMP INTEGER;
DECLARE VARIABLE COL1 SMALLINT = 0;
DECLARE VARIABLE COL2 SMALLINT = 0;
DECLARE VARIABLE COL3 SMALLINT = 0;
DECLARE VARIABLE COL4 SMALLINT = 0;
DECLARE VARIABLE COL5 SMALLINT = 0;
DECLARE VARIABLE MAXCOL SMALLINT = 0;
begin
  for
    select P.ecolumn
      from eprpos P
        join ecolumns C on (C.number = P.ecolumn)
      where P.payroll=:payroll
        and P.ecolumn < 1000 and C.repsum > 0
      group by P.ecolumn
      into :tmp
  do
    col1 = col1 + 1;


  if (col1 > maxcol) then
    maxcol = col1;

  for
   select P.ecolumn
     from eprpos P
       join ecolumns C on (C.number = P.ecolumn)
     where payroll=:payroll
       and P.ecolumn >= 1000 and P.ecolumn < 4000 and C.repsum > 0
     group by P.ecolumn
     into :tmp
  do
    col2 = col2  +1;

  if (col2 > maxcol) then
  maxcol = col2;

  for
   select P.ecolumn
     from eprpos P
       join ecolumns C on (C.number = P.ecolumn)
     where payroll=:payroll
       and ecolumn > 4000 and ecolumn < 5900 and repsum > 0
     group by P.ecolumn
     into :tmp
  do col3 = col3 + 1;
  if (col3 > maxcol) then
  maxcol = col3;

  for
   select P.ecolumn
     from eprpos P
       join ecolumns C on (C.number = P.ecolumn)
     where payroll=:payroll
       and ecolumn > 5900 and ecolumn < 8000 and repsum > 0
     group by P.ecolumn
     into :tmp
  do col4 = col4 + 1;
  if (col4 > maxcol) then
  maxcol = col4;

  for
   select P.ecolumn
     from eprpos P
       join ecolumns C on (C.number = P.ecolumn)
     where payroll=:payroll
       and ecolumn > 8000 and ecolumn < 9050 and repsum > 0
     group by P.ecolumn
     into :tmp
  do col5 = col5 + 1;
  if (col5 > maxcol) then
    maxcol = col5;

  while (maxcol > 0) do
  begin
    if (col1 >= maxcol) then
      select first 1 skip (:col1 - :maxcol) c.number, c.name, sum(P.pvalue), c.repsum
        from eprpos P
          join ecolumns C on (C.number = P.ecolumn)
        where P.payroll =: payroll
          and ecolumn < 1000 and repsum > 0
        group by C.number, C.name, C.repsum
        into :num1, :name1, val1, :REPSUM1;

    if (col2 >= maxcol) then
      select first 1 skip (:col2 - :maxcol) c.number, c.name, sum(P.pvalue), c.repsum
        from eprpos P
          join ecolumns C on (C.number = P.ecolumn)
        where P.payroll =: payroll
          and ecolumn >= 1000 and ecolumn < 4000 and repsum > 0
        group by C.number, C.name, C.repsum
        into :num2, :name2, val2, :REPSUM2;

    if (col3 >= maxcol) then
      select first 1 skip (:col3 - :maxcol) c.number, c.name, sum(P.pvalue), c.repsum
        from eprpos P
          join ecolumns C on (C.number = P.ecolumn)
        where P.payroll =: payroll and ecolumn > 4000 and ecolumn < 5900 and repsum > 0
        group by C.number, C.name, C.repsum
        into :num3, :name3, val3, :REPSUM3;

    if (col4 >= maxcol) then
      select first 1 skip (:col4 - :maxcol) c.number, c.name, sum(P.pvalue), c.repsum
        from eprpos P
          join ecolumns C on (C.number = P.ecolumn)
        where payroll =: payroll and ecolumn > 5900 and ecolumn < 8000 and repsum > 0
        group by C.number, C.name, C.repsum
        into :num4, :name4, :val4, :REPSUM4;

    if (col5 >= maxcol) then
      select first 1 skip (:col5 - :maxcol) c.number, c.name, sum(P.pvalue), c.repsum
        from eprpos P
          join ecolumns C on (C.number = P.ecolumn)
        where payroll =: payroll and ecolumn > 8000 and ecolumn < 9050 and repsum > 0
        group by C.number, C.name, C.repsum
        into :num5, :name5, :val5, :REPSUM5;

    suspend;
    maxcol = maxcol - 1;
  end
end^
SET TERM ; ^
