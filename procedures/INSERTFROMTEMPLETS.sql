--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INSERTFROMTEMPLETS(
      BKDOC integer,
      TEMPLET integer,
      AMOUNT numeric(15,2))
   as
declare variable STATUS integer;
declare variable ACCOUNT ACCOUNT_ID;
declare variable SIDE smallint;
declare variable DEBIT numeric(15,2);
declare variable CREDIT numeric(15,2);
declare variable SETTLEMENT varchar(20);
declare variable DESCRIPT varchar(80);
declare variable DECREE integer;
declare variable DIST1DEF integer;
declare variable DIST1SYMBOL SYMBOLDIST_ID;
declare variable DIST2DEF integer;
declare variable DIST2SYMBOL SYMBOLDIST_ID;
declare variable DIST3DEF integer;
declare variable DIST3SYMBOL SYMBOLDIST_ID;
declare variable DIST4DEF integer;
declare variable DIST4SYMBOL SYMBOLDIST_ID;
declare variable DIST5DEF integer;
declare variable DIST5SYMBOL SYMBOLDIST_ID;
declare variable DIST6DEF integer;
declare variable DIST6SYMBOL SYMBOLDIST_ID;
declare variable DCTEMPLETPOS integer;
declare variable DISTAMOUNT numeric(15,2);
declare variable TRANSDATE timestamp;
declare variable COMPANY integer;
begin
  select status, transdate, company from bkdocs where ref=:BKDOC into :status, :transdate, :company;
  if (:status>0) then
    exception FK_DOCUMENT_ACCEPTED;
  for
    select ref, account, side, debit, credit, settlement, descript, dist1def, dist1symbol, dist2def, dist2symbol, dist3def, dist3symbol, dist4def, dist4symbol, dist5def, dist5symbol, dist6def, dist6symbol
    from dctempletpos
    where dctemplet=:templet
    into :dctempletpos, :account, :side, :debit, :credit, :settlement, :descript, :dist1def, :dist1symbol, :dist2def, :dist2symbol, :dist3def, :dist3symbol, :dist4def, :dist4symbol, :dist5def, :dist5symbol, :dist6def, :dist6symbol
  do begin
    if (:amount <> 0) then
      if (:side=0) then
        debit = :amount;
      else
        credit = :amount;
    execute procedure GEN_REF('DECREES') returning_values decree;
    insert into decrees (ref, bkdoc, account, side, debit, credit, settlement, descript, dist1ddef, dist1symbol, dist2ddef, dist2symbol, dist3ddef, dist3symbol, dist4ddef, dist4symbol, dist5ddef, dist5symbol, dist6ddef, dist6symbol)
      values (:decree, :BKDOC, :account, :side, :debit, :credit, :settlement, :descript, :dist1def, :dist1symbol, :dist2def, :dist2symbol, :dist3def, :dist3symbol, :dist4def, :dist4symbol, :dist5def, :dist5symbol, :dist6def, :dist6symbol);
    insert into distpos (decrees, bkdoc, account, debit, credit, transdate, company, descript,
                         tempdictdef, tempsymbol, tempdictdef2, tempsymbol2,tempdictdef3, tempsymbol3,
                         tempdictdef4, tempsymbol4, tempdictdef5, tempsymbol5, tempdictdef6, tempsymbol6)
    select :decree, :bkdoc, :account, (case when d.withamount = 1 and :side = 0 then :amount else dpd.debit end), (case when d.withamount = 1 and :side = 1 then :amount else dpd.credit end), :transdate, :company, dpd.descript,
           dpd.dist1def, dpd.dist1symbol, dpd.dist2def, dpd.dist2symbol, dpd.dist3def, dpd.dist3symbol,
           dpd.dist4def, dpd.dist4symbol, dpd.dist5def, dpd.dist5symbol, dpd.dist6def, dpd.dist6symbol
      from dctempletposdist dpd
      left join dctempletpos dp on dpd.dctempletpos = dp.ref
      left join dctemplets d on dp.dctemplet = d.ref
      where dpd.dctempletpos = :dctempletpos;
  end
  suspend;
end^
SET TERM ; ^
