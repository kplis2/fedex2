--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_OBL_MAGROZLICZ(
      FAKREF integer)
   as
declare variable koszt numeric(14,2);
declare variable kosztm numeric(14,2);
declare variable local integer;
declare variable magrozlicz smallint;
declare variable skad smallint;
declare variable data timestamp;
declare variable datafak timestamp;
declare variable datamag timestamp;
declare variable sazgodne smallint;
declare variable pozfakref integer;
declare variable poziloscm numeric(14,4);
declare variable dokilosc numeric(14,4);
declare variable dokiloscm numeric(14,4);
declare variable pkoszt numeric(14,2);
declare variable pkosztm numeric(14,2);
declare variable poznumer integer;
declare variable grupadok integer;
declare variable sapozycje integer;
declare variable nieobrot integer;
declare variable dokiloscorg numeric(14,4);
declare variable pozfakreforg integer;
declare variable poziloscmorg numeric(14,4);
declare variable akceptacja integer;
declare variable cenamagf numeric(14,4);
declare variable zakup integer;
declare variable cenamagmin numeric(14,4);
declare variable cenamagmax numeric(14,4);
begin
  --procedura nie oblicza kosztu tylko znacznik rozliczenia magazynowego
  execute procedure CHECK_LOCAL('NAGFAK',:fakref) returning_values :local;
  if(:local = 1) then begin
     select DATA, SKAD, grupadok, nieobrot, akceptacja, zakup
       from NAGFAK
       where REF=:fakref
       into :datafak,  :skad, :grupadok, :nieobrot,:akceptacja, :zakup;
     data = null;
     if(:akceptacja<>1 and :akceptacja<>8) then begin
       -- dokument niezaakceptowany nie jest rozliczony magazynowo
       magrozlicz = 0;
       -- zaktualizuj znacznik na biezacym dokumencie
       update NAGFAK set MAGROZLICZ = :magrozlicz, MAGROZLICZDATA = :data
         where REF=:fakref and ((MAGROZLICZ <> :magrozlicz) or (magrozliczdata <> :data));
     end else if(:skad = 2 or :nieobrot = 1 or :nieobrot = 3) then begin
       -- dokument pochodzacy z dok.mag. z definicji jest rozliczony magazynowo
       magrozlicz = 1;
       data = :datafak;
       -- zaktualizuj znacznik na biezacym dokumencie
       update NAGFAK set MAGROZLICZ = :magrozlicz, MAGROZLICZDATA = :data
         where REF=:fakref and ((MAGROZLICZ <> :magrozlicz) or (magrozliczdata <> :data));
     end else begin
       -- okrelenie, czy dokument jest wydany calosciowo
       magrozlicz = 1;
       sapozycje = 0;
       -- przejdz po pozycjach towarowych biezacej faktury
       for select POZFAK.REF, POZFAK.ILOSCM, POZFAK.NUMER, POZFAK.CENAMAG
       from POZFAK
       where  dokument = :fakref and POZFAK.opk = 0 and POZFAK.usluga<>1
       into :pozfakref, :poziloscm, :poznumer, :cenamagf
       do begin
         if(:magrozlicz = 1) then begin
           sapozycje = 1;
           /*faktura moze byc roliczona magazynowo, jesli ona lub jej korekty sa rozliczone magazynowo w calosci jako grupa dokumentow.
             Dlatego porwonuje ilosc ostateczną po korektach*/
           /*znalezienie pozycji pierwotnej*/
           -- przejdz po pozycjach korygowanych dla tej pozycji
           dokilosc = 0;
           select sum(case when DEFDOKUM.KORYG=0 then DOKUMPOZ.ILOSC else -DOKUMPOZ.ILOSC end), max(DOKUMNAG.DATA), min(DOKUMPOZ.CENA), max(DOKUMPOZ.CENA)
             from DOKUMPOZ
             join POZFAK_GET_FIRSTPOZ_UP(:pozfakref) on (DOKUMPOZ.FROMPOZFAK=POZFAK_GET_FIRSTPOZ_UP.POZFAK)
             left join DOKUMNAG on (DOKUMNAG.REF = DOKUMPOZ.DOKUMENT)
             left join DEFDOKUM on (DEFDOKUM.SYMBOL = DOKUMNAG.TYP)
             where strmulticmp(';'||DOKUMNAG.AKCEPT||';',';1;8;')>0
           into :dokilosc, :datamag, :cenamagmin, :cenamagmax;
           -- poziloscm zawiera finalna ilosc wydana na fakturze po wszystkich korektach
           -- dokilosc zawiera sume ilosci wydana na dokumentach magazynowych zwiazanych z cala transakcja
           if (:dokilosc is null) then dokilosc = 0;
           if(:poziloscm <> :dokilosc) then begin
             magrozlicz = 0;
             data = null;
           end else if(:datamag>:data or :data is null) then begin
             data = :datamag;
           end
           -- dla zakupu sprawdzamy jeszcze zgodnosc cen magazynowych
           -- jesli nie sa zgodne to nie wystawiono not, a wiec nie ma rozliczenia
           if(:zakup=1 and :magrozlicz=1) then begin
             if(:cenamagf<>:cenamagmin or :cenamagf<>:cenamagmax) then begin
               magrozlicz = 0;
               data = null;
             end
           end
         end
       end
       if(:sapozycje = 0) then begin
         -- jesli faktura nie ma pozycji to sila rzeczy nie jest rozliczona magazynowo
         magrozlicz = 0;
         data = null;
       end
       if(:magrozlicz = 1) then begin
         if(:data is null) then data = :datafak;
         -- wez wczesniejsza date z daty faktury i daty dok. mag.
         if(:data < :datafak) then data = :datafak;
        if(:data is null) then data = current_date;
       end
       -- zaktualizuj znacznik na wszystkich dokumentach w grupie
       update NAGFAK set MAGROZLICZ = :magrozlicz, MAGROZLICZDATA = :data
         where GRUPADOK=:grupadok and ((MAGROZLICZ <> :magrozlicz) or (magrozliczdata <> :data));
     end
  end
end^
SET TERM ; ^
