--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRDPLANSVER_CALCULATE(
      FRDHDSVER integer,
      VALTYPE smallint,
      TIMEELEM integer)
   as
declare variable FRDCELL integer;
declare variable FRDPERSDIMVAL integer;
declare variable RET numeric(16,2);
declare variable PROC varchar(1024);
declare variable DECL varchar(8191);
declare variable BODY varchar(8191);
begin
  for
    select frdcells.ref, frdcells.frdpersdimval
      from frdcells
        join frdpersdimvals on (frdpersdimvals.ref = frdcells.frdpersdimval)
      where ((frdpersdimvals.planformula is not null and
          frdpersdimvals.planformula <> '' and :valtype = 0)
        or (frdpersdimvals.realformula is not null and
          frdpersdimvals.realformula <> '' and :valtype = 1)
        or (frdpersdimvals.val1formula is not null and
          frdpersdimvals.val1formula <> '' and :valtype = 2)
        or (frdpersdimvals.val2formula is not null and
          frdpersdimvals.val2formula <> '' and :valtype = 3)
        or (frdpersdimvals.val3formula is not null and
          frdpersdimvals.val3formula <> '' and :valtype = 4)
        or (frdpersdimvals.val4formula is not null and
          frdpersdimvals.val4formula <> '' and :valtype = 5)
        or (frdpersdimvals.val5formula is not null and
          frdpersdimvals.val5formula <> '' and :valtype = 6))
        and frdcells.frdhdrver = :frdhdsver and frdcells.timelem = :timeelem
      into :frdcell, :frdpersdimval
  do begin
     delete from frddrilldown where frdcell=:frdcell and valtype=:valtype;
    if (:valtype = 0) then
    begin
      select decl, body
        from FRDPERSDIMVALS
        where ref = :frdpersdimval
        into :decl, :body;

      update frdcellsbottomup set frdcellsbottomup.isactual = 0
        where frdcellsbottomup.frdcell = :frdcell and frdcellsbottomup.bottomup = 2
          and frdcellsbottomup.field = 'PLANAMOUNT';
    end else if (:valtype = 1) then
    begin
      select declreal, bodyreal
        from FRDPERSDIMVALS
        where ref = :frdpersdimval
        into :decl, :body;

      update frdcellsbottomup set frdcellsbottomup.isactual = 0
        where frdcellsbottomup.frdcell = :frdcell and frdcellsbottomup.bottomup = 2
          and frdcellsbottomup.field = 'REALAMOUNT';
    end else if (:valtype = 2) then
    begin
      select declval1, bodyval1
        from FRDPERSDIMVALS
        where ref = :frdpersdimval
        into :decl, :body;

      if (frdpersdimval = 634) then
        exception test_break '  '||:body;

      update frdcellsbottomup set frdcellsbottomup.isactual = 0
        where frdcellsbottomup.frdcell = :frdcell and frdcellsbottomup.bottomup = 2
          and frdcellsbottomup.field = 'VAL1';
    end else if (:valtype = 3) then
    begin
      select declval2, bodyval2
        from FRDPERSDIMVALS
        where ref = :frdpersdimval
        into :decl, :body;

      update frdcellsbottomup set frdcellsbottomup.isactual = 0
        where frdcellsbottomup.frdcell = :frdcell and frdcellsbottomup.bottomup = 2
          and frdcellsbottomup.field = 'VAL2';
    end else if (:valtype = 4) then
    begin
      select declval3, bodyval3
        from FRDPERSDIMVALS
        where ref = :frdpersdimval
        into :decl, :body;

      update frdcellsbottomup set frdcellsbottomup.isactual = 0
        where frdcellsbottomup.frdcell = :frdcell and frdcellsbottomup.bottomup = 2
          and frdcellsbottomup.field = 'VAL3';
    end else if (:valtype = 5) then
    begin
      select declval4, bodyval4
        from FRDPERSDIMVALS
        where ref = :frdpersdimval
        into :decl, :body;

      update frdcellsbottomup set frdcellsbottomup.isactual = 0
        where frdcellsbottomup.frdcell = :frdcell and frdcellsbottomup.bottomup = 2
          and frdcellsbottomup.field = 'VAL4';
    end else if (:valtype = 6) then
    begin
      select declval5, bodyval5
        from FRDPERSDIMVALS
        where ref = :frdpersdimval
        into :decl, :body;

      update frdcellsbottomup set frdcellsbottomup.isactual = 0
        where frdcellsbottomup.frdcell = :frdcell and frdcellsbottomup.bottomup = 2
          and frdcellsbottomup.field = 'VAL5';
    end

    execute procedure STATEMENT_EXECUTE(:decl, :body, :frdcell, :valtype, 0)
            returning_values :ret;
    if (:valtype = 0) then
    begin
      update frdcells set frdcells.planamount = :ret where frdcells.ref = :frdcell;
      delete from frdcellsbottomup where frdcellsbottomup.isactual = 0
        and frdcellsbottomup.frdcell = :frdcell and frdcellsbottomup.bottomup = 2
        and frdcellsbottomup.field = 'PLANAMOUNT';
    end
    else if (:valtype = 1) then
    begin
      update frdcells set frdcells.realamount = :ret where frdcells.ref = :frdcell;
      delete from frdcellsbottomup where frdcellsbottomup.isactual = 0
        and frdcellsbottomup.frdcell = :frdcell and frdcellsbottomup.bottomup = 2
        and frdcellsbottomup.field = 'REALAMOUNT';
    end
    else if (:valtype = 2) then
    begin
      update frdcells set frdcells.val1 = :ret where frdcells.ref = :frdcell;
      delete from frdcellsbottomup where frdcellsbottomup.isactual = 0
        and frdcellsbottomup.frdcell = :frdcell and frdcellsbottomup.bottomup = 2
        and frdcellsbottomup.field = 'VAL1';
    end
    else if (:valtype = 3) then
    begin
      update frdcells set frdcells.val2 = :ret where frdcells.ref = :frdcell;
      delete from frdcellsbottomup where frdcellsbottomup.isactual = 0
        and frdcellsbottomup.frdcell = :frdcell and frdcellsbottomup.bottomup = 2
        and frdcellsbottomup.field = 'VAL2';
    end
    else if (:valtype = 4) then
    begin
      update frdcells set frdcells.val3 = :ret where frdcells.ref = :frdcell;
      delete from frdcellsbottomup where frdcellsbottomup.isactual = 0
        and frdcellsbottomup.frdcell = :frdcell and frdcellsbottomup.bottomup = 2
        and frdcellsbottomup.field = 'VAL3';
    end
    else if (:valtype = 5) then
    begin
      update frdcells set frdcells.val4 = :ret where frdcells.ref = :frdcell;
      delete from frdcellsbottomup where frdcellsbottomup.isactual = 0
        and frdcellsbottomup.frdcell = :frdcell and frdcellsbottomup.bottomup = 2
        and frdcellsbottomup.field = 'VAL4';
    end
    else if (:valtype = 6) then
    begin
      update frdcells set frdcells.val5 = :ret where frdcells.ref = :frdcell;
      delete from frdcellsbottomup where frdcellsbottomup.isactual = 0
        and frdcellsbottomup.frdcell = :frdcell and frdcellsbottomup.bottomup = 2
        and frdcellsbottomup.field = 'VAL5';
    end
  end
end^
SET TERM ; ^
