--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_ADD_DOC(
      DOKUMENT integer,
      TYPDOK char(1) CHARACTER SET UTF8                           ,
      LISTWYSD integer = null,
      KONTROLAPAK smallint = 0,
      X_PACKOPER OPERATOR_ID = null)
  returns (
      LISTWYSDREF integer)
   as
declare variable nowy smallint;
declare variable grupasped integer;
declare variable magazyn defmagaz_id;
--declare variable magazyn string20;
declare variable magazynstr string20; --MKD bo zle sciagalo skrot z magazynu
declare variable mag2 defmagaz_id;
declare variable oddzial oddzial_id;
declare variable slodef integer;
declare variable slopoz integer;
declare variable symbol varchar(255);
declare variable kodsped varchar(3);
declare variable zewn smallint;
declare variable uwagi varchar(1024);
declare variable uwagisped varchar(255);
declare variable flagi varchar(40);
declare variable flagisped varchar(40);
declare variable klient integer;
declare variable odbiorcaid integer;
declare variable imie varchar(60);
declare variable tmpimie varchar(60);
declare variable nazwisko varchar(60);
declare variable tmpnazwisko varchar(60);
declare variable odbiorca varchar(255);
declare variable tmpodbiorca varchar(255);
declare variable miasto city_id;
declare variable tmpmiasto city_id;
declare variable kodp postcode;
declare variable tmpkodp postcode;
declare variable ulica street_id;
declare variable tmpulica street_id;
declare variable nrdomu nrdomu_id;
declare variable tmpnrdomu nrdomu_id;
declare variable nrlokalu nrdomu_id;
declare variable tmpnrlokalu nrdomu_id;
declare variable krajid country_id;
declare variable tmpkrajid country_id;
declare variable telefon phones_id;
declare variable tmptelefon phones_id;
declare variable email STRING255;
declare variable tmpemail STRING255;
declare variable sposdost sposdost_id;
declare variable sposdosttmp sposdost_id;
declare variable sposdostpunktodbioru string20;
declare variable punktyodbioru smallint;
declare variable wartoscbrutto ceny;
declare variable kfirma smallint;
declare variable knip nip;
declare variable dokmagref integer;
declare variable dokpozref integer;
declare variable wersjaref integer;
declare variable jedno integer;
declare variable waga waga_id;
declare variable ilosc ilosci_mag;
declare variable ilosco ilosci_mag;
declare variable lwpilosc ilosci_mag;
declare variable lwpilosco ilosci_mag;
declare variable zamow integer;
declare variable serwis integer;
declare variable afterackaction string255;
declare variable xinfo memo; --[PM] XXX
declare variable x_opoznionepakowanie smallint_id; --[PM] XXX #OPOZNIONEPAKOWANIE
declare variable sposzap platnosc_id; --[PM] XXX
declare variable termdost timestamp_id;   --MKD xxx
declare variable pobranie ceny;  --MKD xxx
declare variable ubezpieczenie ceny;  --MKD xxx
begin
--XXX JO: Nie ruszac bez konsultacji
  nowy = 0;
  if (coalesce(:listwysd,0) > 0) then
    select ref from listywysd where ref = :listwysd
    into :listwysdref;
  else
  begin
    execute procedure gen_ref('LISTYWYSD')
      returning_values :listwysdref;
    nowy = 1;
  end

  if (:typdok = 'M') then
  begin
    select first 1  d.grupasped, trim(coalesce(d.mwsmag, d.magazyn)), d.mag2, d.oddzial, d.slodef, d.slopoz, d.symbol, d.kodsped,
        d.zewn, substring(d.uwagi from  1 for  255), d.uwagisped, d.flagi, d.flagisped,--XXX MB obciecie uwag do 255 bo dluzsze nie wchodza do LISTYWYSD
        d.klient, d.odbiorcaid,
        coalesce(d.odbiorca,''), coalesce(d.dmiasto,''), coalesce(d.dkodp,''),
        coalesce(d.dulica,''), coalesce(d.dnrdomu,''), coalesce(d.dnrlokalu,''),
        coalesce(d.dimie, ''), coalesce(d.dnazwisko, ''), coalesce(d.dkrajid,''),
        coalesce(d.dtelefon, ''), coalesce(d.demail, ''),
        d.wartsbrutto, k.firma, k.nip,
        d.sposdost, d.sposdostpunktodbioru,
        d.sposzap --[PM] XXX
        ,d.termdost--MKD XXX
      from dokumnag d 
        left join klienci k on (d.klient = k.ref)
        left join odbiorcy o on (d.odbiorcaid = o.ref)
      where d.grupasped = :dokument
      order by case d.typ when 'WZ' then 0 else 1 end --MKD xxx ZG13069 aby dane pobieraly sie z dokumentu WZ a nie z NP
    into :grupasped, :magazyn, :mag2, :oddzial, :slodef, :slopoz, :symbol, :kodsped,
      :zewn, :uwagi, :uwagisped, :flagi, :flagisped,
      :klient, :odbiorcaid,
      :odbiorca, :miasto, :kodp, :ulica, :nrdomu, :nrlokalu,
      :imie, :nazwisko, :krajid,
      :telefon, :email,
      :wartoscbrutto, :kfirma, :knip,
      :sposdost, :sposdostpunktodbioru,
      :sposzap, --[PM] XXX
      :termdost; --MKD XXX
      --<<< XXX MKD na wypadek wiekszej ilosci dokumentow z pobraniem
      select sum(d.x_pobranie)
        from dokumnag d
        where d.grupasped = :dokument
        group by d.grupasped into :pobranie;
      ----XXX MKD >>>
    select s.ref, s.punktyodbioru, s.afterackaction
      from sposdost s
      where ref = :sposdost
    into :sposdosttmp, :punktyodbioru, :afterackaction;
    if (coalesce(:punktyodbioru,0) = 1 and coalesce(:sposdostpunktodbioru,'') = '') then
      exception universal 'Brak wskazania wymaganego punktu odbioru! Skontaktuj się z BOK!';


    if (:zewn = 1) then
    begin
      if (:odbiorcaid is not null) then
        select
            case when coalesce(trim(o.nazwa),'') = '' then k.nazwa else o.nazwa end,
            case when coalesce(trim(o.dmiasto),'') = '' then k.miasto else o.dmiasto end,
            case when coalesce(trim(o.dkodp),'') = '' then k.kodp else o.dkodp end,
            case when coalesce(trim(o.dulica),'') = '' then k.ulica else o.dulica end,
            case when coalesce(trim(o.dnrdomu),'') = '' then k.nrdomu else o.dnrdomu end,
            case when coalesce(trim(o.dnrlokalu),'') = '' then k.nrlokalu else o.dnrlokalu end,
            case when coalesce(trim(o.krajid),'') = '' then k.krajid else o.krajid end,
            case when coalesce(trim(o.dtelefon),'') = '' then k.telefon else o.dtelefon end,
            case when coalesce(trim(o.email),'') = '' then k.email else o.email end,
            case when coalesce(trim(o.imie),'') = '' then k.imie else o.imie end,
            case when coalesce(trim(o.nazwisko),'') = '' then k.nazwisko else o.nazwisko end
          from odbiorcy o
            left join klienci k on (o.klient = k.ref)
          where o.ref = :odbiorcaid
        into :tmpodbiorca, :tmpmiasto, :tmpkodp, :tmpulica, :tmpnrdomu, :tmpnrlokalu,
          :tmpkrajid, :tmptelefon, :tmpemail, :tmpimie, :tmpnazwisko;

      if (:odbiorcaid is null) then
        select k.nazwa, k.miasto, k.kodp, k.ulica, k.nrdomu, k.nrlokalu,
            k.krajid, k.telefon, k.email, k.imie, k.nazwisko
          from klienci k
          where k.ref = :klient
        into :tmpodbiorca, :tmpmiasto, :tmpkodp, :tmpulica, :tmpnrdomu, :tmpnrlokalu,
          :tmpkrajid, :tmptelefon, :tmpemail, :tmpimie, :tmpnazwisko;

      if (coalesce(trim(:odbiorca),'') = '') then
        odbiorca = :tmpodbiorca;
      if (coalesce(trim(:imie),'') = '') then
        imie = :tmpimie;
      if (coalesce(trim(:nazwisko),'') = '') then
        nazwisko = :tmpnazwisko;
      if (coalesce(trim(:odbiorca),'') = '') then
        odbiorca = :imie||' '||:nazwisko;
      if (coalesce(trim(:ulica),'') = '' or coalesce(trim(:miasto),'') = '') then
      begin
        kodp = :tmpkodp;
        miasto = :tmpmiasto;
        ulica = :tmpulica;
        nrdomu = :tmpnrdomu;
        nrlokalu = :tmpnrlokalu;
      end
      if (coalesce(trim(:krajid),'') = '') then
        krajid = :tmpkrajid;
      if (coalesce(trim(:krajid),'') = '') then
        krajid = 'PL';
      if (coalesce(trim(:telefon),'') = '') then
        telefon = :tmptelefon;
      if (coalesce(trim(:email),'') = '') then
        email = :tmpemail;

    end
    else
    begin
      execute procedure getconfig('INFO1')
        returning_values :odbiorca;
      if (coalesce(:odbiorca,'') = '') then
      begin
        select wartosc from konfigvals k
          where k.akronim = 'INFO1'
            and k.oddzial = :oddzial
            and k.fromdate <= current_date
        into :odbiorca;

        if (coalesce(:odbiorca, '') = '') then
          select wartosc from konfig where akronim = 'INFO1'
          into :odbiorca;
      end

      select m.dmiasto, m.dkodp, m.dulica, m.dnrdomu, m.dnrlokalu,
          m.dtelefon, m.demail, o.imie, o.nazwisko
        from defmagaz m
          left join operator o on (m.dosoba = o.ref)
        where m.symbol = :mag2
      into :miasto, :kodp, :ulica, :nrdomu, :nrlokalu,
        :telefon, :email, :imie, :nazwisko;
    end
  end
  else if (:typdok = 'Z') then
  begin
    zamow = null;
  end
  else if (:typdok = 'S') then
  begin
    serwis = null;
  end
  else
    exception universal 'Nie obsługiwany typ dokumentu';

  if (:nowy = 1) then
  begin
    if (:x_packoper is null) then
      execute procedure get_global_param('AKTUOPERATOR') -- [DG] XXX ZG97597
        returning_values :x_packoper;

    --[PM] XXX start #OPOZNIONEPAKOWANIE
    select max(x_opoznionepakowanie) from dokumnag
      where grupasped = :grupasped and akcept = 1
      into :x_opoznionepakowanie;
    --[PM] XXX koniec

    --<< MKD XXX
     --ustawienie kwoty ubezpieczenia , zgodnie z ustaleniami ma byc rowna kwocie pobrania lub jezeli bez pobrania to na 500 zl
    if (coalesce(:pobranie,0) > 0) then
      ubezpieczenie = pobranie;
    else
      ubezpieczenie = 500;

    -- przestawiamy termin dostawy na dzisiejsza date tak aby nie bylo problemu z kontaktem ze spedytorami
    if (termdost < current_date) then
      termdost = current_date;
    --MKD XXX >>
    insert into listywysd (ref, listawys, refdok, refzam, refsrv, symbol, fromplace, toplace, kodsped,
        kontrahent, odbiorcaid, adrtyp, adres, nrdomu, nrlokalu, miasto, kodp, slodef, slopoz,
        imie, nazwisko, uwagi, uwagisped, flagi, flagisped, wartdeklar, kontrolapak, oddzial, telefon, krajid,
        sposdost, sposdostpunktodbioru, x_packoper,
        x_grupasped, x_opoznionepakowanie, sposzap, --[PM] XXX , #OPOZNIONEPAKOWANIE , PR121869
        termdost,pobranie,ubezpieczenie) -- MKD XXX
      values (:listwysdref, null, :dokument, :zamow, :serwis, :symbol, :magazyn, substring(:odbiorca from  1 for  20), :kodsped,
        :odbiorca, :odbiorcaid, 'T', :ulica, :nrdomu, :nrlokalu, :miasto, :kodp, :slodef, :slopoz,
        :imie, :nazwisko, :uwagi, :uwagisped , :flagi, :flagisped, :wartoscbrutto, :kontrolapak, :oddzial, :telefon, :krajid,
        :sposdost, :sposdostpunktodbioru, :x_packoper, -- [DG] XXX ZG97597: dodano zapisywanie operatora pakujacego
        :grupasped, :x_opoznionepakowanie, :sposzap, --[PM] XXX , #OPOZNIONEPAKOWANIE, PR121869
        :termdost,:pobranie, :ubezpieczenie); --MKD XXX
  end

  if (:typdok = 'M') then
  begin
    for
      select p.ref, p.wersjaref, p.jedno, p.waga,
          case when coalesce(p.fake, 0) = 0 then p.iloscl else p.ilosc end,
          p.ilosco
        from dokumnag d
          join dokumpoz p on (d.ref = p.dokument)
          join towary t on (p.ktm = t.ktm)
        where d.ref = :dokument
          and d.koryg = 0
          and t.usluga <> 1
          and coalesce(p.havefake, 0) = 0
      into :dokpozref, :wersjaref, :jedno, :waga, :ilosc,
        :ilosco
    do begin
      lwpilosc = 0;
      lwpilosco = 0;
      select sum(ilosc), sum(ilosco) from listywysdpoz where dokpoz = :dokpozref into :lwpilosc, :lwpilosco;
      ilosc = :ilosc - coalesce(:lwpilosc,0);
      ilosco = :ilosco - coalesce(:lwpilosco,0);

      if (:ilosc > 0) then
      begin
        if (exists(select first 1 1 from listywysdpoz where dokument = :listwysdref and dokref = :dokument and dokpoz = :dokpozref)) then
          update listywysdpoz
            set ilosc = ilosc + :ilosc, ilosco = ilosco + :ilosco
            where dokument = :listwysdref
              and dokref = :dokument
              and dokpoz = :dokpozref;
        else
          xinfo = null; --[PM] XXX start
          select distinct cast(substring(list(coalesce(mc.symbol,'')||'-'||coalesce(op.nazwa,'')) from 1 for 1024) as memo)
            from mwsacts a join mwsords o on a.mwsord = o.ref
              left join mwsconstlocs mc on mc.ref = a.mwsconstlocp
              left join operator op on op.ref = o.operator
            where o.mwsordtypedest = 1 and
              a.doctype = :typdok and a.docid = :dokument and a.docposid = :dokpozref
              into :xinfo; --[PM] XXX koniec

          insert into listywysdpoz(dokument, doktyp, dokref, dokpoz, wersjaref,
              ilosco, jedno, ilosc, iloscmax, waga,
              x_info) --[PM] XXX
            values (:listwysdref, :typdok, :dokument, :dokpozref, :wersjaref,
              :ilosco, :jedno, :ilosc, :ilosc, :waga,
              substring(:xinfo from 1 for 1024)); --[PM] XXX
      end
    end
    
    if (:listwysdref is not null) then
    begin
      for
        select d.ref from dokumnag d
          where d.grupasped = :grupasped and d.ref <> :dokument and d.wydania = 1 and d.wysylkadone = 0
        into :dokmagref
      do begin
        execute procedure listywysd_add_doc(:dokmagref, 'M', :listwysdref)
          returning_values :listwysdref;
      end
    end
  end
end^
SET TERM ; ^
