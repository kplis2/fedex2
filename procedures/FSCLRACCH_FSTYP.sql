--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FSCLRACCH_FSTYP(
      REF_FH integer)
  returns (
      FSTYPEE integer)
   as
declare variable MINDEF integer;
declare variable MINPOS integer;
declare variable MINACC ACCOUNT_ID;
declare variable MAXDEF integer;
declare variable MAXPOS integer;
declare variable MAXACC ACCOUNT_ID;
begin
  select min(p.dictdef), min(p.dictpos), min(p.account),
        max(p.dictdef), max(p.dictpos), max(p.account)
    from fsclraccp P
    where P.fsclracch = :ref_fh
    into :mindef, :minpos, :minacc, :maxdef, :maxpos, :maxacc;

   -- 0/1 rozliczenie/kompensata
  if (minDEF = maxDEF and minPOs = maxPOs and minacc = maxacc) then
        fstypee = 0;
    else
        fstypee = 1;
  --zapis do tabeli
  update fsclracch P
    set fstype = :fstypee
    where P.ref = :ref_fh
      and P.FSTYPE <> :fstypee;
end^
SET TERM ; ^
