--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_GET_LISTYWYSD(
      NRIDENT STRING40,
      KODOPAK STRING1024,
      MODE SMALLINT_ID,
      FAST SMALLINT_ID,
      LOCATION STRING20)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING1024)
   as
declare variable EOL STRING2;
declare variable SYMBOL STRING20;
declare variable NRIDENTTMP STRING40;
declare variable REFLISTWYSD LISTYWYSD_ID;
declare variable REFLISTWYS LISTYWYS_ID;
declare variable AKTUOPERATOR INTEGER_ID;
declare variable CONTRACTOR STRING20;
declare variable ILOSCOPAK INTEGER_ID;
declare variable ZALADOWANO INTEGER_ID;
declare variable kodpaczki string40;
declare variable rozopk_ref integer_id;
declare variable x_zaladowane smallint_id;
begin
  /*

  MODE
  0 - załadunek
  1 - rozładunek

  */

  /* XXX SO
  jesli podany kod listu, to z automatu pakowane są wszystkie paczki i oznaczenie na X_ZAL_AUTOMAT ze podano kod listu, x_zaladowano na listywysdroz_opk na 2
  jesli podany kod palety lub paczki, to x_zaladowana na 1
  wtedy bedziemy wiedziec ktore zaladowano beposrednim wskazaniem a ktore na latwizne
  */
  eol ='
';
  status = 1;
  msg = '';
  if (LOCATION is null) then LOCATION = '';

  if (nrident is null) then begin
    status = 0;
    msg = 'Nie podano numeru identyfikacyjnego. Operacja niemożliwa';
    suspend;
    exit;
  end

  if (coalesce(kodopak, '') = '') then begin
    status = 0;
    msg = 'Nie podano numeru paczki. Operacja niemożliwa';
    suspend;
    exit;
  end

  kodpaczki = kodopak;
  select first 1 d.ref, o.ref, coalesce(o.x_zaladowane, 0)
    from listywysdroz_opk o
    left join listywysd d on d.ref = o.listwysd
    where o.stanowisko = :kodpaczki
    order by d.ref desc
  into :kodopak, :rozopk_ref, :x_zaladowane;

  if (kodpaczki =kodopak) then begin
    status = 0;
    msg = 'Paczka o podanym kodzie nie istnieje '||kodpaczki;
    suspend;
    exit;
  end

  if (rozopk_ref is not null and mode = 0 and x_zaladowane = 1) then
  begin
    msg = 'Opakowanie '||kodpaczki||' zostało juz załadowane';
    status = 0;
    suspend;
    exit;
  end

  if (rozopk_ref is not null and mode = 1 and x_zaladowane = 0) then
  begin
    msg = 'Opakowanie '||kodpaczki||' jest już rozladowane ';
    status = 0;
    suspend;
    exit;
  end
-- select
  select coalesce(ls.x_nr_ident, :nrident)
    from listywys ls
    where :nrident like  '%'||ls.symbol||'%'
  into :nrident;

-- sprawdzamy czy istnieje kod spedycyjny
  if (exists(select first 1 1 from listywysd d where d.ref = :kodopak)) then
  begin
-- sprawdzamy czy kod spedycyjny jest wydawany na podanym zleceniu transportowym
    if (exists(select first 1 1 from listywys s join listywysd d on (s.ref = d.listawys)
                                where
                                  lower(s.x_nr_ident) = lower(:nrident)
                                  and s.stan = 'O'
                                  and d.ref = :kodopak)) then
-- załadunek lub rozładunek na istniejącym zleceniu transportowym
    begin
      select d.ref,  d.toplace, d.x_zaladowano,
          /*coalesce(d.x_polpaleta,0) + coalesce(d.iloscpalet,0) + coalesce(d.iloscpaletbez,0) +
          coalesce(d.iloscpaczek,0) + coalesce(d.x_drobnica,0)*/ --xxx KBI 96095
          count(1)
        from listywys s
          join listywysd d on (s.ref = d.listawys)
          left join listywysdroz_opk o on o.listwysd = d.ref
        where lower(s.x_nr_ident) =  lower(:nrident)
          and s.stan = 'O'
          and d.ref =  :kodopak
          --and coalesce(o.x_zaladowane,0) > 0
        group by d.ref,  d.toplace, d.x_zaladowano
        into :reflistwysd, :contractor, :zaladowano, :iloscopak;
      -- załadunek
      if (mode = 0) then begin
        if (coalesce(iloscopak,0) - coalesce(zaladowano,0) = 0) then begin
          status = 0;
          msg = 'Wszyskie opakowania o podanym kodzie zostały już załadowane. Sprawdź wszytkie niezaładowane opakowania. Operacja niemożliwa!';
          contractor = '';
          zaladowano = null;
          iloscopak = null;
        end else begin
          if (kodpaczki <> '') then
          begin
            update listywysdroz_opk o set o.x_zaladowane = 1, o.x_mwsconstlock = '' where o.ref = :rozopk_ref;
            if (fast = 1) then
              execute procedure x_mws_get_listywysd_zal(1, rozopk_ref, '');
            update listywysd set x_zaladowano = coalesce(x_zaladowano,0) + 1 where ref = :reflistwysd;
          end
         /* else begin
            update listywysdroz_opk o set o.x_zaladowane = 2, o.x_mwsconstlock = :car where o.listwysd = :reflistwysd and coalesce(o.x_zaladowane,0) = 0;
            select count(1) from listywysdroz_opk o where o.listwysd = :reflistwysd into :zaladowano;
            update listywysd set x_zaladowano = :zaladowano where ref = :reflistwysd;
          end        */
        end
      -- rozladunek
      end else
      begin
        if (coalesce(zaladowano,0) = 0) then begin
          status = 0;
          msg = 'Wszyskie opakowania o podanym kodzie zosatay już rozładowane. Sprawdź wszytkie załadowane i rozładowane opakowania. Operacja niemożliwa!';
          contractor = '';
          zaladowano = null;
          iloscopak = null;
        end else
        begin
        if (kodpaczki <> '') then
          begin
            update listywysdroz_opk o set o.x_zaladowane = 0, o.x_mwsconstlock = :LOCATION where o.ref = :rozopk_ref;
            if (fast = 1) then
              execute procedure x_mws_get_listywysd_zal(0, rozopk_ref, LOCATION);
            update listywysd set x_zaladowano = coalesce(x_zaladowano,0) - 1 where ref = :reflistwysd
              returning x_zaladowano into :zaladowano;
            if (zaladowano = 0) then
              update listywysd set listawys = null where ref = :reflistwysd;  
          end
         /* else begin
            update listywysdroz_opk o set o.x_zaladowane = 0, o.x_mwsconstlock = :car where o.listwysd = :reflistwysd;
            -- Jesli rozladowano wszyskie opakowania rozpinamy dokument spedycyjny od zlecenia spedycyjnego
            update listywysd set listawys = null where ref = :reflistwysd;
          end      */
        end
      end
    end
-- sprawdzamy czy kod spedycyjny nie byl wydany na innym zleceniu transportowym
    else if (not exists(select first 1 1 from listywys s join listywysd d on (s.ref = d.listawys)
                                            where lower(s.x_nr_ident) <> lower(:nrident) and d.ref = :kodopak)) then
    -- jeli nie to dodanie dokumentu spedycyjnego do istniejącego zlecenia transportowego
    begin
      if (mode = 0) then
      begin
        if (exists(select first 1 1 from listywys s where lower(s.x_nr_ident) = lower(:nrident) and s.stan = 'O')) then
        begin
          select s.ref from listywys s where lower(s.x_nr_ident) = lower(:nrident) and s.stan = 'O' into :reflistwys;
          select d.ref, d.toplace,
              /*coalesce(d.x_polpaleta,0) + coalesce(d.iloscpalet,0) + coalesce(d.iloscpaletbez,0) +
              coalesce(d.iloscpaczek,0) + coalesce(d.x_drobnica,0)*/ --xxx KBI 96095
              count(1)
            from listywysd d
            where d.ref = :kodopak
            group by d.ref,  d.toplace
            into :reflistwysd, :contractor, :iloscopak;
          if (kodpaczki <> '') then
          begin
            update listywysdroz_opk o set o.x_zaladowane = 1, o.x_mwsconstlock = '' where o.ref = :rozopk_ref;
            if (fast = 1) then
              execute procedure x_mws_get_listywysd_zal(1, rozopk_ref, '');
            update listywysd set x_zaladowano = coalesce(x_zaladowano,0) + 1, listawys = :reflistwys where ref = :reflistwysd;
          end
         /* else begin
            --update listywysd set x_zaladowano =  1, listawys = :reflistwys where ref = :reflistwysd;
            update listywysdroz_opk o set o.x_zaladowane = 2, o.x_mwsconstlock = :car where o.listwysd = :reflistwysd and coalesce(o.x_zaladowane,0) = 0;
            select count(1) from listywysdroz_opk o where o.listwysd = :reflistwysd into :zaladowano;
            update listywysd set x_zaladowano = :zaladowano, listawys = :reflistwys where ref = :reflistwysd;
          end  */
      -- lub zakadanie nowego zlecenia transportowego
        end else
        begin
          execute procedure get_global_param ('AKTUOPERATOR') returning_values :aktuoperator;
          execute procedure GEN_REF('LISTYWYS') returning_values :reflistwys;
          insert into LISTYWYS(REF, DATAOTW, DATAPLWYS, STAN, BLOKADA, TYP, NIEBEZPIECZNY, OPERATOR, X_NR_IDENT)
            values (:reflistwys, current_timestamp, current_timestamp, 'O', 0, 1, 0, :aktuoperator, :nrident);
          select ref, d.toplace,
              /*coalesce(d.x_polpaleta,0) + coalesce(d.iloscpalet,0) + coalesce(d.iloscpaletbez,0) +
              coalesce(d.iloscpaczek,0) + coalesce(d.x_drobnica,0)*/ --xxx KBI 96095
              count(1)
            from listywysd d
            where d.ref = :kodopak
            group by d.ref,  d.toplace
            into :reflistwysd, :contractor, :iloscopak;
          if (kodpaczki <> '') then
          begin
            update listywysdroz_opk o set o.x_zaladowane = 1, o.x_mwsconstlock = '' where o.ref = :rozopk_ref;
            if (fast = 1) then
              execute procedure x_mws_get_listywysd_zal(1, rozopk_ref, '');
            update listywysd set x_zaladowano = coalesce(x_zaladowano,0) + 1, listawys = :reflistwys where ref = :reflistwysd;
          end
         /* else begin
            --update listywysd set x_zaladowano =  1, listawys = :reflistwys where ref = :reflistwysd;
            update listywysdroz_opk o set o.x_zaladowane = 2, o.x_mwsconstlock = :car where o.listwysd = :reflistwysd and coalesce(o.x_zaladowane,0) = 0;
            select count(1) from listywysdroz_opk o where o.listwysd = :reflistwysd into :zaladowano;
            update listywysd set x_zaladowano = :zaladowano, listawys = :reflistwys where ref = :reflistwysd;
          end  */
        end
      end else begin
        status = 0;
        msg = 'Opakowania nie zostały załadowane. Rozładunek niemożliwy';
      end
-- kod spedycyjny jest wydany/wydawany na innym zleceniu transportowym
    end else if (exists(select first 1 1 from listywys s join listywysd d on (s.ref = d.listawys)
                          where lower(s.x_nr_ident) <> lower(:nrident) and  s.stan = 'O' and  d.ref = :kodopak)) then
    -- kod spedycyjny jest wydawana na innym otwartym zleceniu transportowym
    begin
      select s.symbol, s.x_nr_ident
        from listywys s
          join listywysd d on (s.ref = d.listawys)
        where lower(s.x_nr_ident) <> lower(:nrident)
          and s.stan = 'O'
          and d.ref = :kodopak
      into :symbol, :nridenttmp;

      status = 0;
      msg = 'Opakowania o padanym kodzie są ładowane zleceniu transportowym o symbolu '||symbol ||' i numerze identyfikacyjnym '||
        nridenttmp||'. Operacja niemożliwa';
    -- kod spedycyjny jest już wydany na innym zamknitym zleceniu transportowym
    end else if (exists(select first 1 1 from listywys s join listywysd d on (s.ref = d.listawys)
                        where lower(s.x_nr_ident) <> lower(:nrident) and s.stan <> 'O' and d.ref = :kodopak)) then
    begin
      select s.symbol, s.x_nr_ident
        from listywys s
          join listywysd d on (s.ref = d.listawys)
        where lower(s.x_nr_ident) <> lower(:nrident)
          and s.stan <> 'O'
          and d.ref = :kodopak
        into :symbol, :nridenttmp;
    
      status = 0;
      msg = 'Opakowania o padanym kodzie już załadowane na zamknietym zleceniu transportowym o symbolu '||symbol ||' i numerze identyfikacyjnym '||
        nridenttmp||'. Operacja niemożliwa';      
    end
-- nie znaleziono kodu spedycyjnego
  end else begin
    status = 0;
    msg = 'Podany kod opakowania nie istnieje. Operacja niemożliwa';
  end
  suspend;
end^
SET TERM ; ^
