--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EFUNC_CHNGTEXTSIZE(
      TEXTIN varchar(255) CHARACTER SET UTF8                           ,
      MODE smallint,
      MAXLEN smallint = 255)
  returns (
      TEXTOUT varchar(255) CHARACTER SET UTF8                           )
   as
declare variable i smallint;
declare variable iscapital smallint;
declare variable textsign varchar(1);
begin
/* MWr - Procedura pozwala zmienic wielkosc liter w zadanym tekscie TEXTIN wedlug
   zadanego trybu MODE:
                        = 0 - male litery w calym tekscie
                        = 1 - wielkie litery w calym tekscie
                        = 2 - kazdy wyraz od wielkiej litery (odzielony spacja, myslnikiem czy kropka)
                        = 3 - jak w zdaniu, pierwsza litera wielka, pozostale male
   TEXTIN podlega operacji trim'owania. Paramter MAXLEN pozwala okreslic jaka
   dopuszczamy maksymalna dlugosc dla tekstu wyjsciowego: TEXTOUT.
*/

  if (mode not in (1,2,3,0) or mode is null) then
    exception universal 'Nieznany parametr w procedurze zamiany liter!';

  textout = '';

  if (textin is null) then
    textin = '';
  else
    textin = trim(textin);

  if (textin <> '') then
  begin
    if (maxlen is null or maxlen > 255) then
      maxlen = 255;
    if (mode in (0,3)) then
      textout = lower(textin) COLLATE UNICODE;
    else if (mode = 1) then
      textout = upper(textin) COLLATE UNICODE;
    else if (mode = 2) then
    begin
      i = 1;
      iscapital = 1;
      while (i <= coalesce(char_length(textin),0)) do -- [DG] XXX ZG119346
      begin
        textsign = substring(textin from i for 1)  ;
        if (textsign in ('', '-', ' ', '.')) then
        begin
          iscapital = 1;
          textout = textout || textsign;
        end else
        begin
          if (iscapital = 1) then textout = textout || (upper(textsign) COLLATE UNICODE);
          else textout = textout || (lower(textsign) COLLATE UNICODE);
          iscapital = 0;
        end
        i = i + 1;
      end
    end
    if (mode = 3) then
      textout = upper(substring(textout from 1 for 1)) || substring(textout from 2 for coalesce(char_length(textin),0)); -- [DG] XXX ZG119346

    --ograniczenie dlugosci
    textout = substring(:textout from  1 for  :maxlen);
  end
  suspend;
end^
SET TERM ; ^
