--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_CREATE_DOK_OPKZWR(
      NAGREF integer,
      POZREF integer,
      DATA timestamp)
  returns (
      STATUS integer,
      NEWDOK integer,
      NEWDOKSYM varchar(20) CHARACTER SET UTF8                           ,
      WARTOSC numeric(14,2))
   as
declare variable newtyp varchar(3);
declare variable stan numeric(18,4);
declare variable dokref integer;
declare variable dokpozref integer;
declare variable ktm varchar(20);
declare variable wersjaref integer;
declare variable wersja integer;
declare variable ilosc numeric(14,4);
declare variable cenacen numeric(14,2);
declare variable cenasr numeric(14,2);
declare variable opk smallint;
declare variable jedno integer;
declare variable dostawa integer;
declare variable cenabru numeric(14,2);
declare variable cenanet numeric(14,2);
declare variable refpoz integer;
declare variable usluga smallint;
declare variable opkref integer;
declare variable gr_vat varchar(5);
begin
  status = 0;
  wartosc = 0;
  if (:pozref is not null and :pozref <> 0) then
  begin
    select DF.KORYGUJACY, dn.ref, dp.ktm, dp.wersja, dp.wersjaref, dp.opk, dp.cenacen, dp.dostawa, dp.jedno, stopk.stan, dp.cenasr, dp.cenabru, dp.cenanet, tow.usluga, stopk.ref, dp.gr_vat
      from DEFDOKUM df
        join DOKUMNAG dn on (DF.SYMBOL = DN.typ)
        join dokumpoz dp on dp.dokument = dn.ref
        left join towary tow on tow.ktm = dp.ktm
        left join stanyopk stopk on stopk.pozdokum = dp.ref
      where dp.ref = :pozref into :newtyp, :dokref, :ktm, :wersja, :wersjaref, :opk, :cenacen, :dostawa, :jedno, :ilosc, :cenasr, :cenabru, :cenanet, :usluga, :opkref, :gr_vat;
    if(:newtyp is null or (:newtyp = '')) then exception DOK_KORYG_BRAKDOKTYP;
  --sprawdzenie, czy pozycja nie jest juz rozliczona
      if (:ilosc = 0) then exception STANYOPK_ROZLICZONE;
  wartosc = wartosc + (:ilosc)*(:cenabru);
  -- zakladanie dokumnag
    execute procedure GEN_REF('DOKUMNAG') returning_values :newdok;
    insert into DOKUMNAG(REF,TYP,MAGAZYN,MAG2,DATA,REFK,SYMBOLK,SLODEF,SLOPOZ,KLIENT,DOSTAWCA,ZLECNAZWA,UWAGI)
    select :newdok, :newtyp,  MAGAZYn,MAG2, :data, REF, SYMBOL,SLODEF,SLOPOZ,KLIENT,DOSTAWCA,ZLECNAZWA,UWAGI from DOKUMNAG where REF=:dokref;
    select symbol from DOKUMNAG where ref=:newdok into :newdoksym;

  --uzupelnianie pozycji
    execute procedure GEN_REF('DOKUMPOZ') returning_values :dokpozref;
    if (:usluga <> 1 and :opk <> 1) then
      insert into dokumpoz (ref, dokument, ktm, wersja, wersjaref, opk, cenacen, dostawa, jedno, ilosc, cenasr, cenabru, cenanet, kortopoz, stanopkroz, gr_vat)
        values (:dokpozref, :newdok, :ktm, :wersja, :wersjaref, :opk, :cenacen, :dostawa, :jedno, :ilosc, :cenasr, :cenabru, :cenanet, NULL, :opkref, :gr_vat);
    else begin
      insert into dokumpoz (ref, dokument, ktm, wersja, wersjaref, opk, cenacen, dostawa, jedno, ilosc, cenasr, cenabru, cenanet, kortopoz, stanopkroz, gr_vat)
        values (:dokpozref, :newdok, :ktm, :wersja, :wersjaref, :opk, :cenacen, :dostawa, :jedno, :ilosc, :cenasr, :cenabru, :cenanet, :pozref, :opkref, :gr_vat);
    end
  end else if (:nagref is not null and :nagref <>0) then
  begin
    select defdok.korygujacy
      from dokumnag dn
        left join defdokum defdok on defdok.symbol = dn.typ
      where dn.ref = :nagref
      into :newtyp;
    if(:newtyp is null or (:newtyp = '')) then exception DOK_KORYG_BRAKDOKTYP;

  -- zakladanie dokumnag
    execute procedure GEN_REF('DOKUMNAG') returning_values :newdok;
    insert into DOKUMNAG(REF,TYP,MAGAZYN,MAG2,DATA,REFK,SYMBOLK,SLODEF,SLOPOZ,KLIENT,DOSTAWCA,ZLECNAZWA,UWAGI)
    select :newdok, :newtyp,  MAGAZYn,MAG2, :data, REF, SYMBOL,SLODEF,SLOPOZ,KLIENT,DOSTAWCA,ZLECNAZWA,UWAGI from DOKUMNAG where REF=:nagref;
    select symbol from DOKUMNAG where ref=:newdok into :newdoksym;

    for select dp.ref ,dp.ktm, dp.wersja, dp.wersjaref, dp.opk, dp.cenacen, dp.dostawa, dp.jedno, stopk.stan, dp.cenasr, dp.cenabru, dp.cenanet, tow.usluga, stopk.ref , dp.gr_vat
      from dokumnag dnag
        left join dokumpoz dp on dp.dokument = dnag.ref
        left join towary tow on tow.ktm = dp.ktm
        left join stanyopk stopk on stopk.pozdokum = dp.ref
      where dnag.ref = :nagref and dp.opk >= 1
      into :refpoz, :ktm, :wersja, :wersjaref, :opk, :cenacen, :dostawa, :jedno, :ilosc, :cenasr, :cenabru, :cenanet, :usluga, :opkref, :gr_vat
    do begin
      if (:ilosc <> 0) then
      begin
  --uzupelnianie pozycji
        execute procedure GEN_REF('DOKUMPOZ') returning_values :dokpozref;
        if (:usluga <> 1 and :opk <> 1) then
          insert into dokumpoz (ref, dokument, ktm, wersja, wersjaref, opk, cenacen, dostawa, jedno, ilosc, cenasr, cenabru, cenanet, kortopoz, stanopkroz, gr_vat)
            values (:dokpozref, :newdok, :ktm, :wersja, :wersjaref, :opk, :cenacen, :dostawa, :jedno, :ilosc, :cenasr, :cenabru, :cenanet, NULL, :opkref, :gr_vat);
        else begin
        insert into dokumpoz (ref, dokument, ktm, wersja, wersjaref, opk, cenacen, dostawa, jedno, ilosc, cenasr, cenabru, cenanet, kortopoz, stanopkroz, gr_vat)
          values (:dokpozref, :newdok, :ktm, :wersja, :wersjaref, :opk, :cenacen, :dostawa, :jedno, :ilosc, :cenasr, :cenabru, :cenanet, :pozref, :opkref, :gr_vat);
    end
        wartosc = wartosc + (:ilosc)*(:cenabru);
      end
    end
  end 
  status = 1;
end^
SET TERM ; ^
