--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_LISTYWYSD_GETOPK(
      KODKRESK varchar(40) CHARACTER SET UTF8                           ,
      LISTWYSD integer,
      TYPOPK integer = 0)
  returns (
      AKTOPK integer,
      AKTOPKSTR STRING40)
   as
declare variable opkkodkresk kodkreskowy;
declare variable opknazwa string30;
declare variable opkstan smallint;
declare variable numer integer;
begin

  kodkresk = upper(:kodkresk);

  --[PM] XXX start , jesli mam otwarte jakies opakowanie, zaczynam od niego
  select first 1 ref, kodkresk from listywysdroz_opk where listwysd = :listwysd
    and coalesce(stan,0) = 0 into :aktopk, :aktopkstr;
  if (:aktopk is not null) then
    exit;
  --[PM] XXX koniec

  aktopk = 0;
  aktopkstr = '';

  select o.ref, t.nazwa||': '||o.kodkresk, o.stan
    from listywysdroz_opk o
      left join typyopk t on (o.typopk = t.ref)
    where o.listwysd = :listwysd
      and t.ref = :typopk
      and o.kodkresk = :kodkresk
  into :aktopk, :aktopkstr, :opkstan;
  if (:aktopk is null) then aktopk = 0;

  if (:aktopk = 0 and coalesce(:typopk,0) > 0) then
  begin
    select o.nazwa, o.kodkresk from typyopk o where o.ref = :typopk
    into :opknazwa, :opkkodkresk;

    select max(o.nropk)
      from listywysdroz_opk o
      where o.listwysd = :listwysd
        and o.rodzic is null
    into :numer;
    if (:numer is null) then numer = 0;

    if (substring(:kodkresk from  1 for  3) = 'OPK' and substring(:kodkresk from  4 for 4) = '-' and
        substring(:kodkresk from  coalesce(char_length(:kodkresk),0) for coalesce(char_length(:kodkresk),0)) = '#') then
      opkkodkresk = :kodkresk;
    else
      opkkodkresk = :opkkodkresk||(:numer + 1);
    /*  --<<<mkd
    insert into listywysdroz_opk(listwysd, kodkresk, stan, typopk)
      values(:listwysd, :opkkodkresk, 0, :typopk)
    returning ref, kodkresk into :aktopk, :aktopkstr;
    */
    insert into listywysdroz_opk(listwysd,  stan, typopk)
      values(:listwysd,  0, :typopk)
    returning ref  into :aktopk ;
    -->> MKD
    aktopkstr = :opknazwa||': '||:aktopkstr;
  end
  else
  begin
    if (:opkstan > 0) then
      update listywysdroz_opk set stan = 0 where ref = :aktopk;
  end
end^
SET TERM ; ^
