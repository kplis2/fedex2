--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKMAG_OBL_WAR(
      DOKUMENT integer,
      ONAKCEPT integer)
   as
DECLARE VARIABLE SUMA DECIMAL(14,2);
DECLARE VARIABLE PSUMA DECIMAL(14,2);
--DECLARE VARIABLE POZYCJA INTEGER;
DECLARE VARIABLE ACK SMALLINT;
DECLARE VARIABLE SUMAKAUCJA NUMERIC(14,2);
DECLARE VARIABLE SUMWARTBRU NUMERIC(14,2);
DECLARE VARIABLE SUMWARTNET NUMERIC(14,2);
DECLARE VARIABLE SUMWARTWAL NUMERIC(14,2);
DECLARE VARIABLE SUMFOCWAL NUMERIC(14,2);
DECLARE VARIABLE SUMWARTZL NUMERIC(14,2);
DECLARE VARIABLE SUMFOCZL NUMERIC(14,2);
DECLARE VARIABLE KAUCJABN CHAR(1);
DECLARE VARIABLE WAGA NUMERIC(14,3);
declare variable objetosc numeric(15,4);
begin
  suma = 0;
  select DOKUMNAG.akcept , DEFDOKUM.KAUCJABN from DOKUMNAG join DEFDOKUM on (DOKUMNAG.TYP = DEFDOKUM.symbol) where ref=:dokument into :ack, :kaucjabn;
  if(:ack = 1) then begin
    select sum(DOKUMROZ.wartosc), sum(dokumroz.pwartosc)
    from DOKUMROZ join DOKUMPOZ on (DOKUMROZ.pozycja = DOKUMPOZ.REF)
    where DOKUMPOZ.DOKUMENT = :dokument
    into :suma, :psuma;
    select sum(waga), sum(objetosc)
    from DOKUMPOZ where dokument = :dokument
    into :waga, :objetosc;
  end else if(:ack <> 7 and :ack <> 8) then begin
    select sum(wartosc), sum(waga), sum(objetosc)
    from DOKUMPOZ where dokument = :dokument
    into :suma, :waga, :objetosc;
    psuma = suma;
  end else begin
    select wartosc, waga, objetosc from DOKUMNAG where ref=:dokument into :suma, :waga, :objetosc;
    psuma = suma;
  end
  if(:waga is null) then waga = 0;
  if(:objetosc is null) then objetosc = 0;
  if(:kaucjabn = 'B')then
    select sum(WARTSBRUTTO) from DOKUMPOZ where DOKUMENT = :dokument and OPK > 0 into :sumakaucja;
  else
    select sum(WARTSNETTO) from DOKUMPOZ where DOKUMENT = :dokument and OPK > 0 into :sumakaucja;
  execute procedure oblicz_sumwartnetbru('M',:dokument) returning_values :SUMWARTNET, :sumwartbru;
  select sum(WARTOSCWAL),sum(WARTOSC) from DOKUMPOZ where DOKUMENT = :dokument and FOC<>1 into :sumwartwal, :sumwartzl;
  select sum(WARTOSCWAL),sum(WARTOSC) from DOKUMPOZ where DOKUMENT = :dokument and FOC=1 into :sumfocwal,:sumfoczl;
  if(:sumwartnet is null) then sumwartnet = 0;
  if(:sumwartbru is null) then sumwartbru = 0;
  if(:sumakaucja is null) then sumakaucja = 0;
  if(:sumwartwal is null) then sumwartwal = 0;
  if(:ack = 0 or :ack = 9 or (:onakcept = 1))then begin
    /*update pelnej informacji*/
    update DOKUMNAG set WARTOSC=:suma, PWARTOSC = :psuma, WARTKAUCJA = :sumakaucja,
         WARTSNETTO = :sumwartnet,  WARTSBRUTTO = :sumwartbru,
         SUMWARTWAL = :sumwartwal, SUMWARTZL = :sumwartzl,
         SUMFOCWAL = :sumfocwal, SUMFOCZL = :sumfoczl,
         WAGA = :waga, OBJETOSC = :objetosc
    where ref=:dokument;
  end else begin
    /*tylko zmiany po akceptacji*/
    update DOKUMNAG set WARTOSC=:suma, WARTKAUCJA = :sumakaucja,
         WARTSNETTO = :sumwartnet,  WARTSBRUTTO = :sumwartbru
    where ref=:dokument;
  end
end^
SET TERM ; ^
