--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_DOCUMENTS_LOG_INFO(
      NAGID STRING120)
  returns (
      REF INTEGER_ID,
      IDSTATUS INTEGER_ID,
      TABELA_SENTE STRING35,
      TABELA_WAPRO STRING35,
      DATA TIMESTAMP_ID,
      STATUS SMALLINT_ID,
      ERROR_MSG STRING1024,
      DANE BLOB_UTF8,
      INFO STRING100)
   as
begin
  for
    select xd.ref ,xe.ref, xe.idstatus, xe.tabela_sente, xe.tabela_wapro, xe.DATa, xe.status, xe.error_msg, xe.dane
        from x_imp_dokumnag xd
            left join x_mws_integracja_log xe on ( xd.idstatus = xe.idstatus and xe.tabela_sente = 'X_IMP_DOKUMNAG')
        where xd.nagid = :nagid and xe.ref is not null
  into :info, :ref, :idstatus, :tabela_sente, :tabela_wapro, :DATa, :status, :error_msg, :dane
  do begin
    suspend;
  end
  for
    select xd.ref, xe.ref, xe.idstatus, xe.tabela_sente, xe.tabela_wapro, xe.DATa, xe.status, xe.error_msg, xe.dane
        from x_imp_dokumnag xd
            left join x_mws_integracja_log xe on ( xd.idstatus = xe.idstatus and xe.tabela_wapro = 'SENTE_DOKUMNAG')
        where xd.nagid = :nagid and xe.ref is not null
  into :info,:ref, :idstatus, :tabela_sente, :tabela_wapro, :DATa, :status, :error_msg, :dane
  do begin
    suspend;
  end

  for
    select xp.ref ,xe.ref, xe.idstatus, xe.tabela_sente, xe.tabela_wapro, xe.DATa, xe.status, xe.error_msg, xe.dane
        from x_imp_dokumpoz xp
            left join x_mws_integracja_log xe on ( xp.idstatus = xe.idstatus and xe.tabela_wapro = 'SENTE_DOKUMPOZ')
        where xp.nagid = :nagid  and xe.ref is not null
  into :info,:ref, :idstatus, :tabela_sente, :tabela_wapro, :DATa, :status, :error_msg, :dane
  do begin
    suspend;
  end

  for
    select xp.ref, xe.ref, xe.idstatus, xe.tabela_sente, xe.tabela_wapro, xe.DATa, xe.status, xe.error_msg, xe.dane
        from x_imp_dokumpoz xp
            left join x_mws_integracja_log xe on ( xp.idstatus = xe.idstatus and xe.tabela_sente = 'X_IMP_DOKUMPOZ')
        where xp.nagid = :nagid  and xe.ref is not null
  into :info,:ref, :idstatus, :tabela_sente, :tabela_wapro, :DATa, :status, :error_msg, :dane
  do begin
    suspend;
  end
end^
SET TERM ; ^
