--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_GETDOC_2(
      DOCUMENT STRING30,
      PACKOPER OPERATOR_ID,
      AKTUMAG STRING3,
      MODE SMALLINT_ID = 0)
  returns (
      MWSORDREF DOKUMNAG_ID,
      MWSORDSYMB STRING30,
      STATUS SMALLINT_ID,
      MSG STRING1024)
   as
declare variable INTSTATUS smallint;
declare variable DESCRIPT varchar(1024);
declare variable WH DEFMAGAZ_ID;
declare variable ACCEPT smallint;
declare variable MWS smallint;
declare variable EOL varchar(3);
declare variable DOCWHREF integer;
declare variable DOCSYMBS varchar(30);
declare variable DOCWH varchar(3);
declare variable POSREF integer;
declare variable MWSACTREF integer;
declare variable EMWSCONSTLOCS memo;
declare variable CART integer;
--declare variable MWSORD MWSORDS_ID;
declare variable ACTOPER OPERATOR_ID;
declare variable BLOKADAPAK SMALLINT_ID;
declare variable DOCGROUP DOKUMNAG_ID;
declare variable MAGAZYN STRING3;
declare variable TYPDOKVAT STRING20;
declare variable EMWSCONSTLOCSTMP MEMO; /* [PM] XXX */
declare variable CURCOMPANY COMPANIES_ID;
declare variable DOCCOMPANY COMPANIES_ID;
declare variable DOCCOMPANYNAME STRING20;
declare variable X_PACKOPER OPERATOR_ID;
declare variable X_PACKOPERLOGIN STRING20;
declare variable BRANCH ODDZIAL_ID;
declare variable X_STARTPAKSTATUS SMALLINT_ID;
declare variable X_LASTPACKDOK DOKUMNAG_ID;
declare variable DOCMAGAZYN DEFMAGAZ_ID; /* [PM] XXX */
declare variable multi smallint_id; --[PM] XXX #POJEDYNKI
begin

  --[PM] XXX #POJEDYNKI
  --pakowanie pojedynek - na podstawie LISTYWYSD_GETDOC
  --zalozenie ze jest tylko JEDNO STANOWISKO do pakowania pojedynek (JO)

/* STATUS
0 - otwarty dokument spedydyjny
1 - zamkniety dokument spedydyjny
*/
eol = '
';

  mwsordref = 0;
  mwsordsymb = '';
  status = 1;
  msg = '';

  --blokowanie pobrania tego samego dokumentu przez kilku operatorow jednoczesnie
  --celowe wywolanie dead-lock'a
  --jesli nie potrzebne mozna wykomentowac
  --update konfig set wartosc = :packoper
    --where akronim = 'MWSACTPACKOPER';
  execute procedure set_global_param ('MWSACTUPACKOPERATOR',:packoper);
  execute procedure get_global_param ('CURRENTCOMPANY') returning_values curcompany;
  magazyn = AKTUMAG; -- KBI wdrożenie magazynu celnego

  if (coalesce(magazyn,'') = '') then exception universal 'Brak magazynu';

--  if (magazyn not in ('MWS','GF1')) then --[PM] XXX poki co pakujemy tylko na tych magazynach
--    exception universal 'Pakowanie na magazynie '||:magazyn||' ?!'; --[PM] XXX

  select oddzial from defmagaz where symbol = :magazyn -- [DG] XXX ZG97597
    into :branch;
 -- execute procedure get_global_param('AKTUOPERATOR') returning_values :actoper;
  --if (:actoper is null) then actoper = 74;
  if (exists(select first 1 1 from rdb$procedures p where p.rdb$procedure_name = 'X_LISTYWYSD_GETDOC_2')) then
  begin
    execute statement 'execute procedure X_LISTYWYSD_GETDOC_2('''||:document||''')'
      into :mwsordref, :mwsordsymb, :status, :msg;
    exit;
  end
  else
  begin
    if (coalesce(:document,'') = 'STARTPAK') then
    begin
      status = 0;
        select first 1 o.symbol, o.ref, o.wh
          from
            dokumnag d
            left join sposdost sd on (sd.ref = d.sposdost)
            left join mwsacts a on (a.docid = d.ref and a.doctype = 'M')
            left join mwsords o on (a.mwsord = o.ref) --[PM] #POJEDYNKI
            --left join mwsconstlocs l on (l.ref = a.mwsconstloc)
            left join mwsordcartcolours mcc on mcc.mwsord = o.ref  --[PM] #POJEDYNKI
            left join listywysdpoz p on (p.doktyp = 'M' and p.dokref = a.docid and p.dokpoz = a.docposid)
            left join listywysd ld on p.dokument = ld.ref --[PM] #POJEDYNKI
          where
            o.wh = :magazyn --[PM] #POJEDYNKI
            and d.oddzial = :branch
            and (d.wysylkadone = 0 or (d.wysylkadone = 1 and d.x_blokadapak = 999 and :packoper = 74)) --[PM] XXX #URUCHOMIENIE #ZWROTY
            and d.akcept = 1
            and d.wydania = 1
            and d.koryg = 0
            and o.mwsordtypedest = 1
            and o.status = 5
            and coalesce(o.multi,0) = 2 --[PM] XXX #POJEDYNKI
            and a.mwsconstloc is not null
            --and coalesce(a.emwsconstloc,'') <> ''
            and (ld.akcept is null or ld.akcept < 2) --[PM] #POJEDYNKI
            order by
              case when d.x_blokadapak = 999 then 0 else 1 end, --[PM] XXX #URUCHOMIENIE
              coalesce(sd.listywys,0), --[PM] XXX
              case when coalesce(d.sposdost, 0) = 1 then 0  --[PM] XXX #PRIORYTET  najpierw odbior osobisty
                when coalesce(d.sposdost, 0) = 2 then 1 --transport wlasny
                else 2 end,
                d.dataakc
        into :mwsordsymb, :mwsordref, :wh;
    end
  end

    if (coalesce(:document,'') = 'STARTPAK' and coalesce(:status,0) = 0 and coalesce(:mwsordref,0) = 0) then
    begin
      msg = 'Brak pojedynek do pakowania na tym stanowisku';
      status = 0;
      exit;
    end

    if (mwsordref is null) then
    begin
      execute procedure CHECK_STRING_TO_INTEGER (:document) returning_values intstatus;
      if (intstatus is null) then intstatus = 0;
    
      if (intstatus = 1) then
        select o.ref, o.symbol, o.wh from mwsords o
          where o.ref = :document
          into :mwsordref, :mwsordsymb, :wh;
      else
      begin
        select o.ref, o.symbol, o.wh from mwsords o
          where o.symbol = :document
          into :mwsordref, :mwsordsymb, :wh;
    end

      if (:mwsordref is null) then
      begin
        select c.ref from mwsconstlocs c where c.symbol = upper(:document)
          into :cart;
        if (:cart is null) then
        begin
          msg = 'Brak wozka zdefiniowanego w systemie';
          status = 0;
          exit;
        end

        select first 1 o.ref, o.symbol, o.wh
          from mwsordcartcolours mcc left join mwsords o on mcc.mwsord = o.ref
          where mcc.cart = :cart and mcc.status = 1 and o.status = 5 and coalesce(o.multi,0) = 2
          into :mwsordref, :mwsordsymb, :wh;

        if (:docgroup is null) then
        begin
          msg = 'Brak zlecenia szykowanego na tym wozku lub nieskompletowane w calosci.';
          status = 0;
          exit;
        end
      end
    end

    --[PM] XXX #POJEDYNKI start , sprawdzenie czy na pewno jest to zlecenie na pojedynki
    select multi from mwsords where ref = :mwsordref into :multi;
    if (coalesce(multi,0) <> 2) then
      begin
        msg = 'To NIE JEST zlecenie na pojedynki!';
        status = 0;
        exit;
      end  
    --[PM] XXX koniec


    if (:magazyn <> :wh) then
    begin
      status = 0;
      msg = 'Dokument z '||:docmagazyn||' a pakujesz na '||:magazyn||' !';
      exit;
    end

    accept = 0;

    select cast(list(distinct iif(a.mwsconstloc is not null,l.symbol||' z ','')||coalesce(c.mwsconstlocsymb, 'Brak miejsca odstawienia'), '; ') as memo)
      from mwsords o
        left join mwsacts a on a.mwsord = o.ref
        left join mwsconstlocs l on (a.mwsconstloc = l.ref)
        left join mwsordcartcolours c on (c.mwsord = o.ref)
      where o.ref = :mwsordref
    into :emwsconstlocstmp; --[PM] XXX
    --emwsconstlocstmp = cast (emwsconstlocstmp as string10240); --[PM] XXX start ZG105282
    if (coalesce(:emwsconstlocstmp,'') <> '') then
      begin
        if (coalesce(char_length(:emwsconstlocstmp),0) > 255) then
          begin
            msg = :msg || 'Lista wozkow nie miesci sie na ekranie. Pobierz liste z "Raportu do spakowania".';
          end
        else
          begin
            emwsconstlocs = emwsconstlocstmp;
            msg = :msg||'Pobierz koszyki:'||:eol||:emwsconstlocs;
          end
      end --[PM] XXX koniec ZG105282

  if (coalesce(char_length(:msg),0) > 255) then
    msg = substring(:msg from 1 for 250)||'...';
end^
SET TERM ; ^
