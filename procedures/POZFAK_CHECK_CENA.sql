--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZFAK_CHECK_CENA(
      DOKREF integer)
   as
declare variable cenacen decimal(14,2);
declare variable opk smallint;
declare variable ktm varchar(80);
begin
  for
    select pozfak.cenanet, pozfak.opk, pozfak.ktm
      from pozfak
      where pozfak.dokument = :dokref
    into :cenacen, :opk, :ktm
  do begin
    if(:cenacen is null) then cenacen = 0;
    if(:opk is null) then opk = 0;
    if(:cenacen = 0 and :opk > 0) then
      exception universal 'wprowadzono cenę zerową dla towaru: '||:ktm;
  end
end^
SET TERM ; ^
