--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDEL_COUNTDAYVALUE(
      HOURS numeric(14,2),
      DAYSCNT smallint,
      INCOUNTRY smallint)
  returns (
      AMOUNTPART numeric(4,2))
   as
begin
  if (incountry = 0) then
  begin
    amountpart = cast((
      case when dayscnt =1 then
        (case when hours<8 then 0 when hours < 12 then 33.33 else 50 end)
      else
        (case when hours<8 then 33.33 when hours <12 then 50 else 100 end) end) as numeric(4,2));
  end else
  begin
    amountpart = cast((
      case when dayscnt = 1 then
        (case when hours < 8 then 0 when hours > 12 then 100 else 50 end)
      else
        (case when hours > 8 then 100 else 50 end) end) as numeric(4,2));
  end
  suspend;
end^
SET TERM ; ^
