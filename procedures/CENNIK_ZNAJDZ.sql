--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CENNIK_ZNAJDZ(
      NRCENNIKA integer,
      WERSJAREF integer,
      JEDN integer,
      KLIENT integer,
      ODDZIAL varchar(20) CHARACTER SET UTF8                           ,
      STANSPRZED varchar(20) CHARACTER SET UTF8                           ,
      DATACEN timestamp = null)
  returns (
      NEWCENNIK integer)
   as
declare variable GRUPAKLI integer;
declare variable TESTCENNIK integer;
declare variable CNT integer;
declare variable REFCENNIK integer;
declare variable IFCENNIK integer;
begin
  grupakli=NULL;
  if (:datacen is null) then datacen = current_date;
  if(:jedn is NULL) then jedn = 0;
  ifcennik = 0;
  if(:stansprzed<>'') then begin
    select s.ISCENNIK, s.CENNIK
    from STANSPRZED s
    join defcennik d on (d.REF = s.CENNIK)
    where s.STANOWISKO = :stansprzed
    and (d.dataod is null or d.dataod <= :datacen)
    and (d.datado is null or d.datado >= :datacen)
    into :ifcennik, :refcennik;
  end
  if (ifcennik = 1 and coalesce(refcennik,0)>0) then begin
    newcennik = :refcennik;
    exit;
  end
  if(:klient<>0 and :klient is not NULL) then begin
    /*przeszukiwanie hierarchii cennikow przy zalozeniu zgodnosci jednostki cennika*/
    select k.GRUPA, d.ref
     from KLIENCI k
     left join defcennik d on (d.REF = k.CENNIK)
      where k.REF=:klient
      and (d.dataod is null or d.dataod <= :datacen)
      and (d.datado is null or d.datado >= :datacen)
    into :grupakli, :newcennik;
    if(coalesce(newcennik,0)>0) then begin
      if(:jedn<>0) then begin
        if(exists(select first 1 1 from CENNIK
        where CENNIK=:newcennik and WERSJAREF=:wersjaref and JEDN=:jedn and CENANET<>0 and AKT=1))
        then exit;
      end else begin
        if(exists(select first 1 1 from CENNIK
        where CENNIK=:newcennik and WERSJAREF=:wersjaref and CENANET<>0 and AKT=1))
        then exit;
      end
    end
    for select dk.CENNIK
        from DEFCENKLI dk
        join defcennik d on (d.REF = dk.CENNIK)
        where (dk.GRUPAKLI=:grupakli) and (dk.ODDZIAL=:ODDZIAL or dk.ODDZIAL='' or dk.ODDZIAL IS NULL)
        and (d.dataod is null or d.dataod <= :datacen)
        and (d.datado is null or d.datado >= :datacen)
        order by dk.NUMER
        into :testcennik
    do begin
      if(exists(select first 1 1 from CENNIK
      where CENNIK=:testcennik and WERSJAREF=:wersjaref and JEDN=:jedn and CENANET<>0 and AKT=1))
      then begin
        newcennik = :testcennik;
        exit;
      end
    end
    /*przeszukiwanie hierarchii cennikow przy braku zgodnosci jednostki -
      uzytkownik procedury bedzie msuial poszukac rowniez tej ceny i ewentualnie sobie przeliczyc jednostki*/
    for select dk.CENNIK
        from DEFCENKLI dk
        join defcennik d on (d.REF = dk.CENNIK)
        where (dk.GRUPAKLI=:grupakli) and (dk.ODDZIAL=:ODDZIAL or dk.ODDZIAL='' or dk.ODDZIAL IS NULL)
        and (d.dataod is null or d.dataod <= :datacen)
        and (d.datado is null or d.datado >= :datacen)
        order by dk.NUMER
        into :testcennik
    do begin
      if(exists(select first 1 1 from CENNIK
      where CENNIK=:testcennik and WERSJAREF=:wersjaref and CENANET<>0 and akt = 1))
      then begin
        newcennik = :testcennik;
        exit;
      end
    end
    /*przeszukanie grup klientów przy zalozeniu zgodnosci cennika*/
    for select g.cennik
      from grupykli g
      join defcennik d on (d.REF = g.CENNIK)
      where g.ref = :grupakli
      and (d.dataod is null or d.dataod <= :datacen)
      and (d.datado is null or d.datado >= :datacen)
      into :testcennik
    do begin
      if(exists(select first 1 1 from CENNIK
      where CENNIK=:testcennik and WERSJAREF=:wersjaref and JEDN=:jedn and CENANET<>0 and akt = 1))
      then begin
        newcennik = :testcennik;
        exit;
      end
    end
    /*przeszukiwanie hierarchii cennikow przy braku zgodnosci jednostki -
      uzytkownik procedury bedzie msuial poszukac rowniez tej ceny i ewentualnie sobie przeliczyc jednostki*/
    for select g.cennik
      from grupykli g
      join defcennik d on (d.REF = g.CENNIK)
      where g.ref = :grupakli
      and (d.dataod is null or d.dataod <= :datacen)
      and (d.datado is null or d.datado >= :datacen)
      into :testcennik
    do begin
      if(exists(select first 1 1 from CENNIK
      where CENNIK=:testcennik and WERSJAREF=:wersjaref and CENANET<>0 and akt = 1))
      then begin
        newcennik = :testcennik;
        exit;
      end
    end
  end
  newcennik=:nrcennika;
end^
SET TERM ; ^
