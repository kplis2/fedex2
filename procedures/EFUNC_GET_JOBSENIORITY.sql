--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EFUNC_GET_JOBSENIORITY(
      INPUT_EMPLOYEE integer,
      FORDATE timestamp,
      EMPLSTATUS smallint,
      COMPANY integer,
      USESCHOOLS smallint = 1)
  returns (
      EMPLOYEE integer,
      PERSONNAMES varchar(120) CHARACTER SET UTF8                           ,
      HEREY smallint,
      HEREM smallint,
      HERED smallint,
      OUTY smallint,
      OUTM smallint,
      OUTD smallint,
      SUMY smallint,
      SUMM smallint,
      SUMD smallint,
      SENIORITY_DATE date,
      SENIORITY_YEARS smallint)
   as
declare variable wew_years integer;
declare variable wew_months integer;
declare variable wew_days integer;
declare variable zew_years integer;
declare variable zew_months integer;
declare variable zew_days integer;
declare variable fromdate date;
declare variable todate date;
declare variable y smallint;
declare variable m smallint;
declare variable d smallint;
declare variable afromdate date;
declare variable atodate date;
declare variable wew_nieskl_days integer;
declare variable wew_nieskl_months integer;
declare variable wew_nieskl_years integer;
declare variable sy integer;
declare variable sm integer;
declare variable sd integer;
declare variable i smallint;
declare variable nieskl_ref integer;
declare variable fordate_const date;
declare variable withschool smallint;
declare variable efromdate date;
declare variable etodate date;
declare variable nieskl_ref_tmp integer;
declare variable atodate_tmp date;
declare variable is_break smallint;
begin
  --DS: funkcja oblicza staz pracy na podstawie przebiegu zatrudnienia oraz swiadectw pracy
  if (input_employee = 0) then
    input_employee = null;
  if (fordate is null) then
    fordate = current_date;
  fordate_const = fordate;
  --gdy nie poda sie pracownika wtedy wyswietli dla wszystkich
  for
    select distinct e.ref, e.personnames
      from employees e
        left join employment m on (e.ref = m.employee)
      where (:emplstatus = 0 or
              (m.fromdate <= :fordate and (m.todate >= :fordate or m.todate is null)))
        and (e.ref = :input_employee or :input_employee is null)
        and e.company = :company
      into :employee, :personnames
  do begin
    fordate = fordate_const;
    nieskl_ref = null;
    --jezeli pracownik na dzien naliczania analizy przebywa na urlopie nieskladkowym
    --nalicz staz pracy na dzien przed rozpoczeciem nieobecnosci
    select ref
      from eabsences a
      where a.employee = :employee
        and correction in (0,2)
        and a.fromdate < :fordate and a.todate >= :fordate
        and a.ecolumn in (10,300,330)
      into :nieskl_ref;
    if (nieskl_ref is not null) then
      select afromdate from e_get_whole_eabsperiod(:nieskl_ref,null,null,null,null,null, ';10;300;330;')
        into fordate;

  --staz pracy w firmie (sumowane metoda liczenie pelnych lat, miesiecy, dni)
    select min(c.fromdate), max(coalesce(c.todate,:fordate))
      from employment c
      where c.employee = :employee
      into :fromdate, :todate;
    if (todate >= fordate) then
      todate = fordate - 1;
    execute procedure get_ymd(fromdate, todate)
      returning_values herey, herem, hered;

    --minus okresy nieskladkowe (bez urlopu wychowawczego, ktory jest brany do stazu) oraz ewentualnie szkola
    wew_nieskl_days = 0;
    wew_nieskl_months = 0;
    wew_nieskl_years = 0;

    execute procedure get_config('SENIORITYWITHSCHOOL', 2) returning_values withschool;
    --jezeli w danym okresie pracownik chodzil do szkoly ktora ma byc brana do urlopu, nalezy odjac pokrywajace sie okresy
    if (withschool = 1 and coalesce(:useschools,0) <> 0) then
    begin
      for
        select p.fromdate, p.todate
          from employees e
            join eperschools p on p.person = e.person
          where e.ref = :employee
            and p.isvacbase = 1
            and p.fromdate <= :todate
            and p.todate >= :fromdate
          into :efromdate, :etodate
      do begin
        if (etodate > todate) then
          etodate = todate;
        if (efromdate < fromdate) then
          efromdate = fromdate;
  
        execute procedure get_ymd(efromdate, etodate)
          returning_values y, m, d;
        wew_nieskl_years = wew_nieskl_years + y;
        wew_nieskl_months = wew_nieskl_months + m;
        wew_nieskl_days = wew_nieskl_days + d;
      end
    end

    atodate = fromdate;
    atodate_tmp = null;
    nieskl_ref_tmp = null;
    is_break = 0; --BS44176
    while (is_break = 0) do
    begin
      nieskl_ref = null;
      select first 1 ref
        from eabsences
        where employee = :employee and ecolumn in (10,300,330)
          and fromdate < :fordate and fromdate > :atodate
          and correction in (0,2)
        order by fromdate
        into :nieskl_ref;

      if (nieskl_ref is not null) then
      begin
        atodate = null;
        select afromdate, atodate from e_get_whole_eabsperiod(:nieskl_ref,:fromdate,:todate,null,null,null, ';10;300;330;')
          into :afromdate, :atodate;

        if (nieskl_ref_tmp is not null and (nieskl_ref_tmp = nieskl_ref or atodate_tmp = atodate)) then
          is_break = 1;
        else begin
          execute procedure get_ymd(afromdate, atodate)
            returning_values y, m, d;
    
          wew_nieskl_years = wew_nieskl_years + y;
          wew_nieskl_months = wew_nieskl_months + m;
          wew_nieskl_days = wew_nieskl_days + d;
  
          atodate_tmp = atodate;
          nieskl_ref_tmp = nieskl_ref;
        end
      end else
        is_break = 1;
    end

    herey = herey - wew_nieskl_years;
    herem = herem - wew_nieskl_months;
    hered = hered - wew_nieskl_days;
  
    --wewnetrzny staz pracy na podstawie swiadectw pracy
    select sum(syears), sum(smonths), sum(sdays)
      from eworkcertifs
      where employee = :employee
        and strmulticmp(seniorityflags,';STW;') > 0
      into :wew_years, :wew_months, :wew_days;
  
    wew_years = coalesce(wew_years,0);
    wew_months = coalesce(wew_months,0);
    wew_days = coalesce(wew_days,0);

    herey = herey + wew_years;
    herem = herem + wew_months;
    hered = hered + wew_days;

  --zewnetrzny staz pracy na podstawie swiadectw pracy
    select sum(syears_zewn), sum(smonths_zewn), sum(sdays_zewn)
      from eworkcertifs
      where employee = :employee
        and strmulticmp(seniorityflags,';STZ;') > 0
      into :outy, :outm, :outd;
  
    outy = coalesce(outy,0);
    outm = coalesce(outm,0);
    outd = coalesce(outd,0);

  --zewnetrzny staz pracy na podstawie swiadectw pracy
  --po odjeciu nachodzacych sie okresow swiadectwa pracy i zatrudnienia
    select sum(syears), sum(smonths), sum(sdays)
      from eworkcertifs
      where employee = :employee
        and strmulticmp(seniorityflags,';STZ;') > 0
      into :zew_years, :zew_months, :zew_days;

    zew_years = coalesce(zew_years,0);
    zew_months = coalesce(zew_months,0);
    zew_days = coalesce(zew_days,0);

    --suma okresow nieciaglych (przyjete za miesiac 30 dni, za rok 365 dni)
    execute procedure normalize_ymd(:outy, :outm, :outd)
      returning_values :outy, :outm, :outd;

    execute procedure normalize_ymd(:herey, :herem, :hered)
      returning_values :herey, :herem, :hered;

  --staż pracy ogólem (do nagród jubileuszowych)
    sumy = herey + zew_years;
    summ = herem + zew_months;
    sumd = hered + zew_days;
  
    execute procedure normalize_ymd(:sumy, :summ, :sumd)
      returning_values :sumy, :summ, :sumd;

    --jezeli bedziemy chcieli naliczyc staz na dzien, kiedy jeszcze jest wpisane swiadectwo pracy,
    --wtedy mozemy dostac wynik, ze stazu lacznie jest mniej niz stazu zewnetrznego
    --jezeli taka sytuacja sie zdazy, wyrownuje staz ogolem do stazu zewnetrznego

    if (sumy < outy or (sumy = outy and (summ < outm or (summ = outm and sumd < outd)))) then
    begin
      sumy = outy;
      summ = outm;
      sumd = outd;
    end

    --jezeli w okresie zatrudnienia wystepowaly okresy nieskladkowe
    --przesun date zatrudnienia o odpowiednia liczbe lat miesiecy dni
    execute procedure date_add(fromdate, wew_nieskl_years, wew_nieskl_months, wew_nieskl_days)
      returning_values fromdate;

    --seniority_date wskazuje date nastepnej zmiany liczby lat stazu
    --seniority_years natomiast ile tego stazu bedzie po tej dacie
    --odejmujemy caly staz nabyty przed zatrudnieniem, aby liczyc staz tylko od okresow ciaglych
    --(pod warukiem ze nie bylo okresow nieskladkowych, ale wtedy to juz nie ma ratunku)
    --dzieki temu eliminujemy roznicy rzedu 1-2 dni (zaokraglanie miesiecy do 30 dni)
    sy = sumy + 1 - zew_years - wew_years;
    sm = -zew_months - wew_months;
    sd = -zew_days - wew_days;
    execute procedure normalize_ymd(:sy, :sm, :sd)
      returning_values :sy, :sm, :sd;

    execute procedure date_add(fromdate, sy, sm, sd-1) returning_values seniority_date;
    i = extract(year from seniority_date) - extract(year from fordate);
    execute procedure date_add(seniority_date, -i, 0,0) returning_values seniority_date;
    seniority_years = sumy + 1 - i;
    suspend;
  end
end^
SET TERM ; ^
