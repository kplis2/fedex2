--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_PACKINGLIST_DETS(
      PACKINGLIST integer,
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      SYMBOL varchar(100) CHARACTER SET UTF8                           )
  returns (
      REFOUT integer,
      SYMBOLOUT varchar(100) CHARACTER SET UTF8                           ,
      GOODOUT varchar(40) CHARACTER SET UTF8                           ,
      VERSOUT integer,
      QUANTITYOUT numeric(14,4))
   as
begin
  if (good = '') then good = null;
  if (symbol = '') then symbol = null;
  if (vers = 0) then vers = null;
  for
    select o.ref, r.nrkartonu, r.ktm, r.iloscmag
      from listywysdroz_opk o
        left join listywysdroz r on (r.listywysd = o.listwysd and r.nrkartonu = o.nropk)
      where o.listwysd = :packinglist
        and (r.nrkartonu = :symbol or :symbol is null)
        and (r.ktm = :good or :good is null)
      into refout, symbolout, goodout, quantityout
  do begin
    select first 1 ref from wersje where ktm = :goodout
      into versout;
    suspend;
  end
end^
SET TERM ; ^
