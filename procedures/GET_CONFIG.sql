--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_CONFIG(
      SYMBOL varchar(255) CHARACTER SET UTF8                           ,
      MODE smallint,
      FROMDATE varchar(20) CHARACTER SET UTF8                            default null)
  returns (
      CVALUE varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable multi smallint;
declare variable hist smallint;
declare variable val1 varchar(1024);
declare variable currentbranch varchar(255);
declare variable typ smallint;
declare variable y varchar(10);
declare variable m varchar(10);
declare variable d varchar(10);
declare variable symbol2 varchar(20);
begin
  -- DU: procedura wyciągająca wartosc z KONFIG'a

  select akronim, multi, wartosc, typ, historia
    from konfig where akronim = :symbol
    into :symbol2, :multi, :cvalue, :typ, :hist;

  if (mode > 0 and symbol2 is null) then
    exception no_config 'Brak definicji parametru KONFIG: ' || :symbol;

  if (cvalue is null) then
      cvalue = '';

  if (multi > 0) then
  begin
    select pvalue from globalparams
      where connectionid=current_connection and psymbol= 'KONFIGDOMAIN:'||:multi
    into :currentbranch;
    if (currentbranch is null or currentbranch = '') then
      currentbranch = user;
    if (:hist = 1) then
    begin
      if (:fromdate is null or :fromdate = '') then
        fromdate = current_date;
      select first 1 wartosc
        from konfigvals k
          where k.akronim = :symbol and k.oddzial = :currentbranch and k.fromdate <= :fromdate
          order by k.fromdate desc
        into :val1;
    end
    else
    begin
      select wartosc
        from konfigvals
          where akronim = :symbol and oddzial = :currentbranch
      into :val1;
    end
    if (val1 is not null) then
      cvalue = val1;
  end

  if (mode = 2 and cvalue = '') then
    exception no_config 'Brak ustawienia wartości parametru KONFIG: ' || :symbol;

  if (cvalue is null) then cvalue = '';

  if (:typ=3) then begin --jesli typ jest data, to ewentualna konwersja na rrrr-mm-dd
    if (cvalue like '__-__-__') then
      cvalue =  '20'||cvalue;
    else if (cvalue like '__-__-____') then
      cvalue = substring(cvalue from 7 for 4)||substring(cvalue from 3 for 4)||substring(cvalue from 1 for 2);
  end

  suspend;
end^
SET TERM ; ^
