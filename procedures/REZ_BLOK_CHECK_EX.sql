--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REZ_BLOK_CHECK_EX(
      MAGAZYN char(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      CENA numeric(14,2),
      DOSTAWA integer,
      ILOSC numeric(14,4))
   as
declare variable ildost numeric(14,4);
begin
   execute procedure REZ_GET_FREE_STAN(:magazyn,:ktm, :wersja, :cena, :dostawa) returning_values :ildost;
   if(:ildost <:ilosc) then begin
     /*wyjatek tylko wtedy, kiedy rzeczywiscie sa jakies blokady
      - inaczej to moze byc zwykle zejscie na stan ujemny magazynu */
     ildost = null;
     select sum(ilosc) from STANYREZ where MAGAZYN=:magazyn and KTM=:ktm AND WERSJA=:wersja and STATUS='B' and ZREAL=0
           into :ildost;
     if(:ildost > 0) then
        exception ROZ_ILDOST_CHECK;
   end
end^
SET TERM ; ^
