--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DATE2PERIOD(
      DATA timestamp,
      COMPANY integer)
  returns (
      PERIOD varchar(6) CHARACTER SET UTF8                           )
   as
begin
  select b.id from bkperiods b where b.company = :company and :data >= b.sdate and :data <= b.fdate and b.ptype = 1
  into :period;
  suspend;
end^
SET TERM ; ^
