--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RIGHTS_KONFIGURACJA_SYSTEMU(
      OPERATOR integer = null)
  returns (
      OPER varchar(255) CHARACTER SET UTF8                           ,
      ZB_DANYCH varchar(800) CHARACTER SET UTF8                           ,
      EL_ZB varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable rejfak varchar(5);
declare variable typfak varchar(5);
declare variable zakup smallint;
declare variable k_b varchar(1);
declare variable oper_ref integer;
declare variable el_zb1 varchar(255);
declare variable el_zb2 varchar(255);
declare variable el_zb3 varchar(255);
declare variable el_zb4 varchar(255);
begin
  for select ref,nazwa from operator where aktywny = 1 and (ref=:operator or :operator is null or :operator =0) into :oper_ref, :oper
    do begin
    --Stanowiska spzredazy/zakupu
      for select so.stansprzed, st.zakupu
        from stansprzed st  join stanoper so on (st.stanowisko = so.stansprzed)
        where so.operator = :oper_ref
        order by st.zakupu
        into :el_zb , :zakup
      do begin
        if (:zakup = 0) then zb_danych = 'Stanowisko sprzedaży'; else zb_danych = 'Stanowisko zakupu';
        suspend;
      end
    --Magazyny
    for select distinct om.magazyn||'- '||coalesce(om.dokumtypesnewright,'wszystkie; '),
        om.magazyn||'- '||coalesce(om.dokumtypesacceptright,'wszystkie; '),
        om.magazyn||'- '||coalesce(om.dokumtypeschangeright,'wszystkie; '),
        om.magazyn||'- '||coalesce(om.dokumtypesacceptbackright,'wszystkie; ')
      from opermag om
        join operator op on (om.operator = op.ref)
        where op.ref = :oper_ref
       into  :el_zb1,:el_zb2, :el_zb3, :el_zb4
     do begin
       if(:el_zb1 <> '') then begin
         zb_danych = 'Magazyn- Wystawianie dok:' ;
         el_zb = :el_zb1;
         suspend;
       end
       if(:el_zb2 <> '') then begin
         zb_danych = 'Magazyn- Akceptowanie dok:' ;
         el_zb = :el_zb2;
         suspend;
       end
       if(:el_zb3 <> '') then begin
         zb_danych = 'Magazyn- Wycofywanie do poprawy:' ;
         el_zb = :el_zb3;
         suspend;
       end
       if(:el_zb4 <> '') then begin
         zb_danych = 'Magazyn- Wycofywanie akceptacji:' ;
         el_zb = :el_zb4;
         suspend;
       end

     end

    --Stanowiska kasowe/bankowe
      for select  rk.nazwa , rk.kasabank
        from rkstnkas rk join rkuprstn ru on (rk.kod = ru.stanowisko)
        where ru.operator = :oper_ref
      into :el_zb, :k_b
      do begin
        if (:k_b = 'K') then zb_danych = 'Stanowisko kasowe'; else zb_danych = 'Stanowisko bankowe';
        suspend;
      end

    end

end^
SET TERM ; ^
