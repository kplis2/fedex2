--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EGUS_TWELVE_GETDATA_C_V2014(
      GYEAR varchar(4) CHARACTER SET UTF8                           ,
      EMPLOYEE integer)
  returns (
      C1 numeric(14,3),
      C2 integer,
      C3 numeric(14,2),
      C4 numeric(14,2),
      C5 numeric(14,2),
      C6 numeric(14,2),
      C7 numeric(14,2),
      C8 numeric(14,2),
      C9 numeric(14,2),
      C10 numeric(14,2),
      C11 numeric(14,2),
      C12 numeric(14,2),
      C13 numeric(14,2),
      C14 numeric(14,2))
   as
declare variable FROMDATE date;
declare variable TODATE date;
declare variable PERIOD varchar(6);
declare variable STDEMPLCAL integer;
declare variable HOURPAY numeric(14,2);
declare variable ADDNOMINALPAY numeric(14,2);
declare variable WDIMDAYS numeric(14,2);
declare variable NOMWORKDAYS integer;
begin
/*MatJ Personel: procedura generuje dane do sprawozdania GUS Z-12
  dla zadanego pracownika EMPLOYEE. Odpowiada za czesc C raportu - czas pracy
  oraz wynagr. za pazdzernik roku GYEAR. Specyfikacja zm. wyjsc. w dokumenacji*/

--okreslenie zmiennych pomocniczych
  period = :gyear || '10';
  execute procedure period2dates(period)
    returning_values :fromdate, :todate;

/*C1: miesieczny wskaznik wymiaru czasu pracy za pazdziernik oraz
  C2: liczba dni roboczych w pazdzierniku */
  select wdimdays, nomworkdays, nomworkdays, stdemplcal
    from egus_twelve_worktime(1, :employee, :fromdate, :todate)
    into :wdimdays, :nomworkdays, :c2, :stdemplcal;  --pobranie kalendarza wg ktorego bedzie rozliczany pracownik

  if(coalesce(nomworkdays,0)<>0) then
    c1 = wdimdays /(1.00 * nomworkdays);

--C3: tygodniowa liczba godzin obowiazkowego czasu pracy
  select norm_per_week /3600.00 from ecalendars
    where ref = :stdemplcal
    into :c3;
  c3 = coalesce(c3,40.00);

/*C4: czas faktycznie przepracowny w godz nominalnych,
  C5: czas faktycznie przepracowny w godz nadliczbowych
  C6: czas nieprzepracowany ogolem,
  C7: czas nieprzepracowany oplacony tylko przez zaklad pracy (C5 >= C6)*/
  select workedhours, extrahours, notworkedhours, notwhours_works
    from egus_twelve_worktime(2, :employee, :fromdate, :todate)
    into :c4, :c5, :c6, :c7;

--WYNAGRODZENIA ZA PAZDZIERNIK -------------------------------------------------
  select sum(case when p.ecolumn in (920,930,940,960) then p.pvalue else 0 end) --dodatkowe wynagr. nominl. do C9
    , sum(case when p.ecolumn = 900 then p.pvalue else 0 end) --C10: wynagrodzenie zasad. brutto (nominalne)
    , sum(case when p.ecolumn = 910 then p.pvalue else 0 end) --stawka za godzine, aby wyliczyc C10 dla godzinowych
    , 0 --C11: dodatek za prace zmianowa (za czas nominalny) [BRAK W SYSTEMIE]
    , sum(case when p.ecolumn = 2000 then p.pvalue else 0 end) --C12: tylko premia REGULAMINOWA (np 1/3 premi za kwartal), !zgodnie z EGUS_TWELVE_GETDATA_D_V2014 (BS108630)
    , sum(case when p.ecolumn in (1200, 1210) then p.pvalue else 0 end) --C13: wyn. za prace w godz. nadliczowych
    from epayrolls r
      join eprpos p on (p.payroll = r.ref and r.cper = :period and r.empltype = 1)
    where p.employee = :employee
    into :addnominalpay, :c10, :hourpay, :c11, :c12, :c13;

   c10 = c10 + (hourpay * (c4 + c6 /*czas nominalny w godz*/));

   c9 = addnominalpay + c10 + c11 + c12; --wynagrodzenie osobowe brutto za czas nominalny

   c8 = c9 + c13; --wynagr. osobowe ogolem brutto

   c14 = 0.0;
  suspend;
end^
SET TERM ; ^
