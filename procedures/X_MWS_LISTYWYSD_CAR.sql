--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_LISTYWYSD_CAR(
      LISTYWYSD INTEGER_ID,
      CAR STRING20)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
  declare variable other_data_otw timestamp_id;
  declare variable other_doc_symbol string100;
  declare variable listywysref integer_id;
begin
  status = 1;
  msg = '';

  if (car is null or strlen(car) < 3) then begin
    status = 0;
    msg = 'Numer identyfikacyjny samochodu za krótki '|| coalesce(car,'');
    suspend;
    exit;
  end

  select l.listawys
    from listywysd l
    where l.ref = :listywysd
  into listywysref;

  for
    select l.dataotw, l.symbol
      from listywys l
        where l.x_car = :car
          and l.ref <> :listywysref
     into :other_data_otw, other_doc_symbol
  do begin
    if (datediff(day, current_timestamp, other_data_otw) < 2) then begin
      status = 0;
      msg = 'Samochód o podanym identyfikatorze jest przypisany do innego dokumentu '||other_doc_symbol;
      suspend;
      exit;
    end
  end

  update listywys l
    set l.x_car = :car
    where l.ref = :listywysref;
  suspend;

end^
SET TERM ; ^
