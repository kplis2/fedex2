--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_FUN_DG082(
      PERIOD varchar(6) CHARACTER SET UTF8                           )
  returns (
      AMOUNT numeric(15,2))
   as
declare variable datazm timestamp;
declare variable fday timestamp;
declare variable lday timestamp;
declare variable okres varchar(6);
declare variable wynik numeric(14,2);
declare variable ile integer;
declare variable miesiac integer;
declare variable tmpstddate timestamp;
declare variable absence integer;
declare variable empl integer;
declare variable yearid char(4);
declare variable monthid char(2);
declare variable worktime smallint;
declare variable rworktime smallint;
declare variable daykind smallint;
declare variable stdcalendar integer;
begin

  if (substring(period from 5 for 6) = '03'
    or substring(period from 5 for 6) = '06'
    or substring(period from 5 for 6) = '09'
    or substring(period from 5 for 6) = '12'
  ) then
  begin
    execute procedure get_config('STDCALENDAR',2)
      returning_values :stdcalendar;

    okres = substring(period from 1 for 4)||'01';
    miesiac = 1;
    wynik = 0;
    ile = 0;
    AMOUNT = 0;
    while (okres<=PERIOD) do
    begin
      yearid = substring(okres from 1 for 4);
      MONTHID = substring(okres from 5 for 6);
      ile = ile + 1;
      miesiac = miesiac +1;
      for -- kazdemu pracownikowi
        select e.ref
          from employees e
          left join emplcontracts c on (c.employee = e.ref)
          left join ECONTRTYPES t on (t.contrtype = c.econtrtype)
          where t.empltype =1
          group by e.ref
        into :empl
      do begin
        --przechodzimy przez wszystkie dni (z danego roku i miesiaca)
        --kalendarza firmowego
        for
          select CD.cdate                     --dzien z danego miesiaca i roku
            from ecaldays CD
              join ecalendars C on (C.ref = CD.calendar)
            where C.ref = :stdcalendar  --BS81336
              and CD.cyear = :yearid
              and CD.cmonth = :monthid
            into :tmpstddate
        do begin
          select CD2.worktime, CD2.daykind
            --dni kalendarza pracownika (stad pobierany jest m.in. czas pracy)
            from emplcaldays CD2
            join emplcontracts C1 on (C1.employee = CD2.employee) and C1.econtrtype in (1,2,3)
              and C1.fromdate <= :tmpstddate and coalesce(C1.enddate, :tmpstddate) >= :tmpstddate
            where CD2.employee=:empl
            and (CD2.cdate = :tmpstddate)
            into :worktime, :daykind;

          if (worktime is not null) then
            worktime = worktime / 3600;  --liczba godzin przepracowanych w danym dniu

          if (worktime is null) then
            worktime = 0;

          AMOUNT = AMOUNT + worktime;

          --sprawdzamy, czy dany dzien widnieje na liscie nieobecnosci pracownika
          --jesli tak to rworktime ustawiamy na null, w przeciwnym razie na worktime
--          if (absence = 0) then
--            rworktime = worktime;
--          else --jest nieobecnosc wiec null
--            rworktime = null;
      --    stddate = tmpstddate;
        end --for (petla przechodzenia przez dni kalendarza glownego
      end -- for po pracowniku

      select sum(A.worksecs) / 3660 -- suma nieobecnosci z okresu
        from eabsences A
        where (A.ayear = :yearid) and (A.amonth = :monthid) and A.correction in (0,2)
        into :worktime;

      if (worktime is null) then
        worktime = 0;
      AMOUNT = AMOUNT + worktime;

      if (miesiac > 9) then
        okres = substring(period from 1 for 4)||miesiac;
      else
        okres = substring(period from 1 for 4)||'0'||miesiac;
    end -- konec whaila
  end

  AMOUNT = AMOUNT/1000;
  suspend;
end^
SET TERM ; ^
