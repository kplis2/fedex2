--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKPLIKSEGREGATORY_REFRESH(
      REFDOKPLIK integer)
   as
declare variable SEG integer;
declare variable SEGT integer;
begin
  delete from dokpliksegregatory d where d.segdefault = 2 and d.dokplik = :refdokplik;
  for select ds.segregator from dokpliksegregatory ds
    where ds.segdefault in (0,1) and ds.dokplik = :refdokplik
  into :seg
  do begin
    for select st.seg from SEGREGATORY_TREE(:seg) st
    into :segt
    do begin
      if(not exists (select first 1 1 from dokpliksegregatory dp where dp.segregator = :segt and dp.dokplik = :refdokplik))
       then insert into dokpliksegregatory(DOKPLIK, SEGREGATOR, SEGDEFAULT) values (:REFDOKPLIK, :segt, 2);
    end
  end
end^
SET TERM ; ^
