--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COUNTWAGES_ACCORD(
      PRLNAG integer,
      EMPLOYEE integer,
      WGDATAWYD smallint,
      CHECKINCREASE smallint)
  returns (
      STABLE varchar(31) CHARACTER SET UTF8                           ,
      SREF integer,
      STYP varchar(3) CHARACTER SET UTF8                           ,
      SSYMBOL varchar(40) CHARACTER SET UTF8                           ,
      POINTS numeric(14,4),
      AMOUNT numeric(14,4),
      ACCORD smallint,
      ACT integer,
      ACTPOS integer,
      SDATE timestamp,
      SLISTPOS integer)
   as
DECLARE VARIABLE PROCSQL VARCHAR(1024);
DECLARE VARIABLE CP VARCHAR(255);
DECLARE VARIABLE PPU NUMERIC(14,4);
DECLARE VARIABLE PPH NUMERIC(14,4);
DECLARE VARIABLE MPP NUMERIC(14,4);
DECLARE VARIABLE PPULIST NUMERIC(14,4);
DECLARE VARIABLE TSTART TIMESTAMP;
DECLARE VARIABLE TSTOP TIMESTAMP;
DECLARE VARIABLE ACC SMALLINT;
DECLARE VARIABLE DOCGROUP SMALLINT;
DECLARE VARIABLE DOCTYPES VARCHAR(40);
DECLARE VARIABLE PL INTEGER;
DECLARE VARIABLE BDATE DATE;
DECLARE VARIABLE EDATE DATE;
DECLARE VARIABLE BDOKDATE DATE;
DECLARE VARIABLE EDOKDATE DATE;
DECLARE VARIABLE JOBTIMEQ NUMERIC(14,2);
DECLARE VARIABLE CM SMALLINT;
DECLARE VARIABLE DEFDOKUM VARCHAR(3);
DECLARE VARIABLE DOKREF INTEGER;
DECLARE VARIABLE NORM NUMERIC(14,2);
DECLARE VARIABLE DOCTYPE VARCHAR(1);
DECLARE VARIABLE DAYWEEK SMALLINT;
DECLARE VARIABLE CONFDAYDELAYPEN VARCHAR(100);
DECLARE VARIABLE CONFBIGDELAYPEN VARCHAR(100);
DECLARE VARIABLE CONFSUNBONUS VARCHAR(100);
DECLARE VARIABLE CONFSELLINC VARCHAR(100);
DECLARE VARIABLE CONFANALYSISDAYS VARCHAR(100);
DECLARE VARIABLE CONFDOCTABLE VARCHAR(100);
DECLARE VARIABLE CONFDOCTYPE VARCHAR(100);
DECLARE VARIABLE DAYDELAYPEN INTEGER;
DECLARE VARIABLE BIGDELAYPEN INTEGER;
DECLARE VARIABLE SUNBONUS INTEGER;
DECLARE VARIABLE SELLINC INTEGER;
DECLARE VARIABLE ANALYSISDAYS INTEGER;
DECLARE VARIABLE INCREASED SMALLINT;
DECLARE VARIABLE DOCDATE TIMESTAMP;
DECLARE VARIABLE DOCTERMDOST TIMESTAMP;
DECLARE VARIABLE SPOSDOSTSPED SMALLINT;
DECLARE VARIABLE DOCSYMBOL VARCHAR(40);
DECLARE VARIABLE WYDANIA SMALLINT;
DECLARE VARIABLE POINTSSOURCE SMALLINT;
DECLARE VARIABLE OP integer;
begin
  if (:checkincrease is null) then
    checkincrease = 0;
  execute procedure get_config('ONEDAYDELAYPEN',2) returning_values :confdaydelaypen;
  execute procedure get_config('BIGDELAYPEN',2) returning_values :confbigdelaypen;
  execute procedure get_config('SUTSUNBONUS',2) returning_values :confsunbonus;
  execute procedure get_config('NORMALSELLINCREESE',2) returning_values :confsellinc;
  execute procedure get_config('ANALYSISDAYS',2) returning_values :confanalysisdays;
  execute procedure get_config('SELLINCREASEDOCTABLE',2) returning_values :confdoctable;
  execute procedure get_config('SELLINCREASEDOCTYPE',2) returning_values :confdoctype;
  daydelaypen = cast(:confdaydelaypen as integer);
  bigdelaypen = cast(:confbigdelaypen as integer);
  sunbonus = cast(:confsunbonus as integer);
  sellinc = cast(:confsellinc as integer);
  analysisdays = cast(:confanalysisdays as integer);
  if (:wgdatawyd is null) then wgdatawyd = 0;
  --po pozycjach listach obecnosci w okresie listy plac dla nieakordowych
  select econtrpayrollsnag.bdate, econtrpayrollsnag.edate
    from econtrpayrollsnag
    where econtrpayrollsnag.ref = :prlnag
    into :bdate, :edate;
  if (:bdate is null or :edate is null) then
    exception econtrpayrollsnag_date_not_def;
  -- a zeby dzilalo w tabeli econtractsassign nalezy zadbac o odpowiednie wpisy !!!!
  for
    select econtractsdef.ref, econtractsdef.pointperhour, econtractsdef.pointsperunit,
       econtractsdef.docgroup, econtractsdef.doctypes, econtractsdef.name,
       econtractsdef.calcmethod, econtractsdef.calcproc, operator.ref, econtractsassign.accord
      from econtractsdef
        left join econtractsassign on (econtractsdef.ref = econtractsassign.econtractsdef)
        left join operator on (operator.employee = econtractsassign.employees)
      where (econtractsassign.employees = :employee /*and operator.ref is not null*/)
    into :act, :pph, :ppu,
        :docgroup,:doctypes, :docsymbol,
        :cm, :cp, :op, :acc
  do
  begin
    select econtractsassign.moneyperpoint
      from econtractsassign
      where econtractsassign.employees = :employee and econtractsassign.econtractsdef = :act
      into :mpp;
    if (:mpp is null) then mpp = 0.01;
    dayweek = CAST( extract( weekday from cast(:bdokdate as timestamp)) as smallint);
    if (:checkincrease = 1) then
      execute procedure countwages_sellincrease(substring(:confdoctable from 1 for 31), substring(:confdoctype from 1 for 3),:bdokdate, :analysisdays, :sellinc)
        returning_values :increased;
    else
      increased = 0;
    begin
      -- obliczanie dla dokumentów magazynowych
      if (:docgroup = 0) then
      begin
        for
          select defdokum.symbol, defdokum.wydania
            from defdokum
            into :defdokum, :wydania
        do begin
          if (position(';'||:defdokum||';' in :doctypes)>0) then
          begin
            for
              select dokumnag.ref, dokumnag.symbol, dokumnag.data, dokumnag.termdost, sposdost.listywys
                from dokumnag
                  left join sposdost on (sposdost.ref = dokumnag.sposdost)
                where dokumnag.typ = :defdokum and dokumnag.akcept = 1
                  and ((cast(dokumnag.data as date) >= :bdate and cast(dokumnag.data as date) <= :edate
                           and ((:wgdatawyd = 0 and :wydania = 0) or (:wgdatawyd = 1 and :wydania = 0)))
                        or (cast(dokumnag.datawydania as date) >= :bdate
                            and cast(dokumnag.datawydania as date) <= :edate
                            and :wgdatawyd = 1 and :wydania = 1))
                into :dokref, :docsymbol, :docdate, :doctermdost, :sposdostsped
            do begin
              if (:wydania = 0) then
                doctermdost = docdate;
              stable = 'DOKUMNAG';
              styp = :defdokum;
              sref = :dokref;
              accord = :acc;
              sdate = :bdokdate;
              -- jezeli liczymy z czynnosci
              if (:cm = 0) then
              begin
                points = :ppu;
               if (:points > 0) then
               begin
                  ssymbol = :docsymbol;
                  if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) = 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) > 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 1) then
                  begin
                    points = :points * (1 + cast(:daydelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'+'||cast(:daydelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) = 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) > 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                  end
                  amount = :points * :mpp;
                  actpos = null;
                  suspend;
                end else if (:points < 0) then
                begin
                  ssymbol = :docsymbol;
                  amount = :points * :mpp;
                  actpos = null;
                  suspend;
                end
              -- jezeli liczymy z operacji
              end else if (:cm = 1) then
              begin
                for
                  select econtractsposdef.ref, econtractsposdef.points, econtractsposdef.norm, econtractsposdef.procedurex
                    from econtractsposdef
                    where econtractsposdef.econtractsdef = :act
                    into :actpos, :ppu, :norm, :cp
                do begin
                  if (:pph is null) then pph = 0;
                  if (:norm is null) then norm = 0;
                  pph = :ppu;
                  -- jezeli liczymy z procedury X dla operacji
                  if (:cp is not null and :cp <> '' and :cp <> ' ') then
                  begin
                    procsql = 'select points from '||:cp||'('''||:stable||''','||:dokref||','||:pph||','||:ppu||','||:op||')';
                    if (:procsql is not null) then
                      execute statement :procsql into :points;
                    else 
                      points = 0;
                  end
                  -- jezeli liczymy z normatywu czynnosci
                  else
                    points = :ppu * :norm;
                  if (:points > 0) then
                  begin
                    ssymbol = :docsymbol;
                    if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                         and cast(:docdate as date) - cast(:doctermdost as date) = 1
                         and :sposdostsped = 1) then
                    begin
                      points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                      ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                    end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                         and cast(:docdate as date) - cast(:doctermdost as date) > 1
                         and :sposdostsped = 1) then
                    begin
                      points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                      ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                    end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 1) then
                    begin
                      points = :points * (1 + cast(:daydelaypen as numeric(14,2))/100);
                      ssymbol = :ssymbol||'+'||cast(:daydelaypen as numeric(14,2))||'%';
                    end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                         and cast(:docdate as date) - cast(:doctermdost as date) = 1
                         and :sposdostsped = 1) then
                    begin
                      points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                      ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                    end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                         and cast(:docdate as date) - cast(:doctermdost as date) > 1
                         and :sposdostsped = 1) then
                    begin
                      points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                      ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                    end
                    amount = :points * :mpp;
                    suspend;
                  end else if (:points < 0) then
                  begin
                    ssymbol = :docsymbol;
                    amount = :points * :mpp;
                    suspend;
                  end
                end
              -- jezeli liczymy z procedury X
              end else if (:cm = 2) then
              begin
                procsql = 'select points from '||:cp||'('''||:stable||''','||:dokref||','||:pph||','||:ppu||','||:op||')';
                if (:procsql is not null) then
                  execute statement :procsql into :points;
                else
                  points = 0;
                if (:points > 0) then
                begin
                 ssymbol = :docsymbol;
                 if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) = 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) > 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 1) then
                  begin
                    points = :points * (1 + cast(:daydelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'+'||cast(:daydelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) = 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) > 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                  end
                  amount = :points * :mpp;
                  actpos = null;
                  suspend;
                end else if (:points < 0) then
                begin
                  ssymbol = :docsymbol;
                  amount = :points * :mpp;
                  actpos = null;
                  suspend;
                end
              end
            end
          end
        end
      end
      -- obliczanie dla zamówień sprzedazy
      -- obliczanie dla grupy dokumentów magazynowcyh
      else if (:docgroup = 2) then
      begin
        for
          select defdokum.symbol, defdokum.wydania
            from defdokum
            into :defdokum, :wydania
        do begin
          if (position(';'||:defdokum||';' in :doctypes)>0) then
          begin
            for
              select distinct dokumnag.grupasped
                from dokumnag
                where dokumnag.typ = :defdokum and dokumnag.akcept = 1
                  and ((cast(dokumnag.data as date) >= :bdate and cast(dokumnag.data as date) <= :edate
                           and ((:wgdatawyd = 0 and :wydania = 0) or (:wgdatawyd = 1 and :wydania = 0)))
                        or (cast(dokumnag.datawydania as date) >= :bdate
                            and cast(dokumnag.datawydania as date) <= :edate
                            and :wgdatawyd = 1 and :wydania = 1))
                into :dokref
            do begin
              select dokumnag.symbol, dokumnag.data, dokumnag.termdost, sposdost.listywys
                from dokumnag
                  left join sposdost on (sposdost.ref = dokumnag.sposdost)
                where dokumnag.ref = :dokref
                into :docsymbol, :docdate, :doctermdost, :sposdostsped;
              stable = 'DOKUMNAGGROUP';
              styp = :defdokum;
              sref = :dokref;
              accord = :acc;
              sdate = :bdokdate;
              -- jezeli liczymy z czynnosci
              if (:cm = 0) then
              begin
                points = :ppu;
                amount = :points * :mpp;
                actpos = null;
                if (:points > 0) then
                begin
                  ssymbol = :docsymbol;
                  if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) = 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) > 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 1) then
                  begin
                    points = :points * (1 + cast(:daydelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'+'||cast(:daydelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) = 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) > 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                  end
                  amount = :points * :mpp;
                  actpos = null;
                  suspend;
                end else if (:points < 0) then
                begin
                  ssymbol = :docsymbol;
                  amount = :points * :mpp;
                  actpos = null;
                  suspend;
                end
              -- jezeli liczymy z operacji
              end else if (:cm = 1) then
              begin
                for
                  select econtractsposdef.ref, econtractsposdef.points, econtractsposdef.norm, econtractsposdef.procedurex
                    from econtractsposdef
                    where econtractsposdef.econtractsdef = :act
                    into :actpos, :ppu, :norm, :cp
                do begin
                  if (:pph is null) then pph = 0;
                  if (:norm is null) then norm = 0;
                  pph = :ppu;
                  -- jezeli liczymy z procedury X dla operacji
                  if (:cp is not null and :cp <> '' and :cp <> ' ') then
                  begin
                    procsql = 'select points from '||:cp||'('''||:stable||''','||:dokref||','||:pph||','||:ppu||','||:op||')';
                    if (:procsql is not null) then
                      execute statement :procsql into :points;
                    else 
                      points = 0;
                  end
                  -- jezeli liczymy z normatywu czynnosci
                  else
                    points = :ppu * :norm;
                  amount = :points * :mpp;
                  if (:points > 0) then
                  begin
                    ssymbol = :docsymbol;
                    if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                         and cast(:docdate as date) - cast(:doctermdost as date) = 1
                         and :sposdostsped = 1) then
                    begin
                      points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                      ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                    end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                         and cast(:docdate as date) - cast(:doctermdost as date) > 1
                         and :sposdostsped = 1) then
                    begin
                      points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                      ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                    end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 1) then
                    begin
                      points = :points * (1 + cast(:daydelaypen as numeric(14,2))/100);
                      ssymbol = :ssymbol||'+'||cast(:daydelaypen as numeric(14,2))||'%';
                    end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                         and cast(:docdate as date) - cast(:doctermdost as date) = 1
                         and :sposdostsped = 1) then
                    begin
                      points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                      ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                    end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                         and cast(:docdate as date) - cast(:doctermdost as date) > 1
                         and :sposdostsped = 1) then
                    begin
                      points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                      ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                    end
                    amount = :points * :mpp;
                    suspend;
                  end else if (:points < 0) then
                  begin
                    ssymbol = :docsymbol;
                    amount = :points * :mpp;
                    suspend;
                  end
                end
              -- jezeli liczymy z procedury X
              end else if (:cm = 2) then
              begin
                procsql = 'select points from '||:cp||'('''||:stable||''','||:dokref||','||:pph||','||:ppu||','||:op||')';
                if (:procsql is not null) then
                  execute statement :procsql into :points;
                else
                  points = 0;
                amount = :points * :mpp;
                actpos = null;
                if (:points > 0) then
                begin
                 ssymbol = :docsymbol;
                 if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) = 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) > 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 1) then
                  begin
                    points = :points * (1 + cast(:daydelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'+'||cast(:daydelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) = 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) > 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                  end
                  amount = :points * :mpp;
                  actpos = null;
                  suspend;
                end else if (:points < 0) then
                begin
                  ssymbol = :docsymbol;
                  amount = :points * :mpp;
                  actpos = null;
                  suspend;
                end
              end
            end
          end
        end
      end
      -- obliczanie dla faktur sprzedazy
      -- obliczanie dla dokumentów spedycyjnych
      else if (:docgroup = 4) then
      begin
        for
          select listywysd.ref, listywysd.typ, substring(listywysd.symbolsped from 1 for 20), dokumnag.data, dokumnag.termdost
            from listywysd
              left join dokumnag on (listywysd.refdok = dokumnag.ref)
            where cast(listywysd.dataakc as date) >= :bdate
              and cast(listywysd.dataakc as date) <= :edate
            into :dokref, :doctype, :docsymbol, :docdate, :doctermdost
        do begin
          if (position(';'||:doctype||';' in :doctypes)>0 or :doctype = '' or doctype = ' ') then
          begin
            sposdostsped = 1;
            stable = 'LISTYWYSD';
            styp = :doctype;
            sref = :dokref;
            accord = :acc;
            sdate = :bdokdate;
            -- jezeli liczymy z czynnosci
            if (:cm = 0) then
            begin
              points = :ppu;
              amount = :points * :mpp;
              actpos = null;
              if (:points > 0) then
              begin
                ssymbol = :docsymbol;
                if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                     and cast(:docdate as date) - cast(:doctermdost as date) = 1
                     and :sposdostsped = 1) then
                begin
                  points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                  ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                     and cast(:docdate as date) - cast(:doctermdost as date) > 1
                     and :sposdostsped = 1) then
                begin
                  points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                  ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 1) then
                begin
                  points = :points * (1 + cast(:daydelaypen as numeric(14,2))/100);
                  ssymbol = :ssymbol||'+'||cast(:daydelaypen as numeric(14,2))||'%';
                end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                     and cast(:docdate as date) - cast(:doctermdost as date) = 1
                     and :sposdostsped = 1) then
                begin
                  points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                  ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                     and cast(:docdate as date) - cast(:doctermdost as date) > 1
                     and :sposdostsped = 1) then
                begin
                  points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                  ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                end
                amount = :points * :mpp;
                actpos = null;
                suspend;
              end else if (:points < 0) then
              begin
                ssymbol = :docsymbol;
                amount = :points * :mpp;
                actpos = null;
                suspend;
              end
            -- jezeli liczymy z operacji
            end else if (:cm = 1) then
            begin
              for
                select econtractsposdef.ref, econtractsposdef.points, econtractsposdef.norm, econtractsposdef.procedurex
                  from econtractsposdef
                  where econtractsposdef.econtractsdef = :act
                  into :actpos, :ppu, :norm, :cp
              do begin
                if (:pph is null) then pph = 0;
                if (:norm is null) then norm = 0;
                pph = :ppu;
                -- jezeli liczymy z procedury X dla operacji
                if (:cp is not null and :cp <> '' and :cp <> ' ') then
                begin
                  procsql = 'select points from '||:cp||'('''||:stable||''','||:dokref||','||:pph||','||:ppu||','||:op||')';
                  if (:procsql is not null) then
                    execute statement :procsql into :points;
                  else
                    points = 0;
                end
                -- jezeli liczymy z normatywu czynnosci
                else
                  points = :ppu * :norm;
                amount = :points * :mpp;
                if (:points > 0) then
                begin
                  ssymbol = :docsymbol;
                  if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) = 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) > 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 1) then
                  begin
                    points = :points * (1 + cast(:daydelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'+'||cast(:daydelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) = 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                  end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                       and cast(:docdate as date) - cast(:doctermdost as date) > 1
                       and :sposdostsped = 1) then
                  begin
                    points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                    ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                  end
                  amount = :points * :mpp;
                  suspend;
                end else if (:points < 0) then
                begin
                  ssymbol = :docsymbol;
                  amount = :points * :mpp;
                  actpos = null;
                  suspend;
                end
              end
            -- jezeli liczymy z procedury X
            end else if (:cm = 2) then
            begin
              procsql = 'select points from '||:cp||'('''||:stable||''','||:dokref||','||:pph||','||:ppu||','||:op||')';
              if (:procsql is not null) then
                execute statement :procsql into :points;
              else
                points = 0;
              amount = :points * :mpp;
              actpos = null;
              if (:points > 0) then
              begin
                ssymbol = :docsymbol;
                if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                     and cast(:docdate as date) - cast(:doctermdost as date) = 1
                     and :sposdostsped = 1) then
                begin
                  points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                  ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 0
                     and cast(:docdate as date) - cast(:doctermdost as date) > 1
                     and :sposdostsped = 1) then
                begin
                  points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                  ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                end else if ((:dayweek = 6 or :dayweek = 0) and :increased = 1) then
                begin
                  points = :points * (1 + cast(:daydelaypen as numeric(14,2))/100);
                  ssymbol = :ssymbol||'+'||cast(:daydelaypen as numeric(14,2))||'%';
                end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                     and cast(:docdate as date) - cast(:doctermdost as date) = 1
                     and :sposdostsped = 1) then
                begin
                  points = :points * (1 - cast(:daydelaypen as numeric(14,2))/100);
                  ssymbol = :ssymbol||'-'||cast(:daydelaypen as numeric(14,2))||'%';
                end else if ((:dayweek <> 6 and :dayweek <> 0) and :increased = 0
                     and cast(:docdate as date) - cast(:doctermdost as date) > 1
                     and :sposdostsped = 1) then
                begin
                  points = :points * (1 - cast(:bigdelaypen as numeric(14,2))/100);
                  ssymbol = :ssymbol||'-'||cast(:bigdelaypen as numeric(14,2))||'%';
                end
                amount = :points * :mpp;
                actpos = null;
                suspend;
              end else if (:points < 0) then
              begin
                ssymbol = :docsymbol;
                amount = :points * :mpp;
                actpos = null;
                suspend;
              end
            end
          end
        end
      end
    end
  end
end^
SET TERM ; ^
