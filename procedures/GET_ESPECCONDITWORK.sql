--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_ESPECCONDITWORK(
      PERSON_REF integer,
      T_FROMDATE date,
      T_TODATE date,
      COMPANY integer,
      WITH_WORKPOST smallint = 0,
      CONSIDER_ABSENCES smallint = 0)
  returns (
      PERSON integer,
      EMPLOYEE integer,
      PERSONNAMES varchar(120) CHARACTER SET UTF8                           ,
      SFROMDATE date,
      STODATE date,
      CODE varchar(20) CHARACTER SET UTF8                           ,
      DIMNUM smallint,
      DIMDEN smallint,
      WORKPOST integer)
   as
declare variable mfromdate date;
declare variable mtodate date;
declare variable workpost_temp integer;
declare variable workpost_temp2 integer;
declare variable person_temp integer;
declare variable person_temp2 integer;
declare variable personnames_temp varchar(120);
declare variable personnames_temp2 varchar(120);
declare variable employee_temp integer;
declare variable employee_temp2 integer;
declare variable dimnum_temp smallint;
declare variable dimnum_temp2 smallint;
declare variable dimden_temp smallint;
declare variable dimden_temp2 smallint;
declare variable code_temp varchar(20);
declare variable code_temp2 varchar(20);
declare variable sfromdate_temp date;
declare variable sfromdate_temp2 date;
declare variable stodate_temp date;
declare variable stodate_temp2 date;
declare variable counter integer;
declare variable loop smallint;
declare variable sfromdate_current date;
declare variable stodate_oryginal date;
declare variable absence_start date;
declare variable absence_end date;
declare variable recol_list varchar(255);
declare variable absence_ref eabsences_id;
begin
/*MWr: Personel - podstawa eksportu do Platnika ZUS ZSWA; procedura zwraca liste
  osob, ktore w zdanym okresie T_FROMDATE-T_TODATE odbywaly prace w szczegolnych
  warunkach lub o oszczegolnym charakterze, podaje kod i wymiar pracy oraz okres
  jej trwania. Jezeli parametr WITH_WORKPOST = 1 to powyzsza informacja dodatkowo
  grupowana jest wedlug stanowiska. Jeżeli CONSIDER_ABSENCES = 1 to bierzemy pod
  uwage nieobecnosci (BS70985) */

  if (consider_absences = 0) then
  begin
    counter = 0;
    person_ref = coalesce(person_ref, 0);
    company  = coalesce(company, 0);
    with_workpost = coalesce(with_workpost, 0);
    for
      select distinct e.person, e.ref, e.personnames, s.fromdate, s.todate, m.fromdate, m.todate,
        m.dimnum, m.dimden, z.code, case when :with_workpost = 1 then m.workpost else 0 end
      from emplspeccondit s
        join employment m on (m.employee = s.employee)
        join employees e on (e.ref = m.employee and e.company = :company
                           and (e.person = :person_ref or :person_ref = 0))
        join edictzuscodes z on (z.ref = s.workcode)
      where m.fromdate <= :t_todate
        and (m.todate is null or m.todate >= :t_fromdate)
        and s.fromdate <= :t_todate
        and (s.todate is null or s.todate >= :t_fromdate)
        and (s.fromdate <= m.todate or m.todate is null)
        and (s.todate >= m.fromdate or s.todate is null)
      order by e.personnames, e.person, e.ref, s.fromdate, m.fromdate
      into :person, :employee, :personnames, :sfromdate, :stodate,
           :mfromdate, :mtodate, :dimnum, :dimden, :code, :workpost
    do begin
      if (sfromdate < mfromdate) then
        sfromdate = mfromdate;
      if (stodate > mtodate or stodate is null) then
        stodate = mtodate;
  
      if (counter = 0) then
      begin
        person_temp = person;
        employee_temp = employee;
        personnames_temp = personnames;
        dimnum_temp = dimnum;
        dimden_temp = dimden;
        code_temp = code;
        sfromdate_temp = sfromdate;
        stodate_temp = stodate;
        workpost_temp = workpost;
      end else
      begin
        if (employee_temp = employee and counter > 0
          and dimnum_temp = dimnum and dimden_temp = dimden
          and code_temp = code and (stodate_temp + 1) >= sfromdate
          and ((with_workpost = 1 and workpost_temp = workpost) or with_workpost = 0)) then
        begin
          stodate_temp = stodate;
        end else
        begin
          person_temp2 = person;
          employee_temp2 = employee;
          personnames_temp2 = personnames;
          dimnum_temp2 = dimnum;
          dimden_temp2 = dimden;
          code_temp2 = code;
          sfromdate_temp2 = sfromdate;
          stodate_temp2 = stodate;
          workpost_temp2 = workpost;
  
          person = person_temp;
          employee = employee_temp;
          personnames = personnames_temp;
          dimnum = dimnum_temp;
          dimden = dimden_temp;
          code = code_temp;
          sfromdate = sfromdate_temp;
          stodate = stodate_temp;
          workpost = workpost_temp;
          suspend;
  
          person_temp = person_temp2;
          employee_temp = employee_temp2;
          personnames_temp = personnames_temp2;
          dimnum_temp = dimnum_temp2;
          dimden_temp = dimden_temp2;
          code_temp = code_temp2;
          sfromdate_temp = sfromdate_temp2;
          stodate_temp = stodate_temp2;
          workpost_temp = workpost_temp2;
        end
      end
      counter = counter +1;
    end
  
    if (counter >= 1) then
    begin
      person = person_temp;
      employee = employee_temp;
      personnames = personnames_temp;
      dimnum = dimnum_temp;
      dimden = dimden_temp;
      code = code_temp;
      sfromdate = sfromdate_temp;
      stodate = stodate_temp;
      workpost = workpost_temp;
      suspend;
    end
  end else
  -- obsluga nieobecnosci ------------------------------------------------------
  begin
    recol_list = ';10;20;30;40;50;60;90;100;110;120;140;150;160;170;260;270;280;290;300;330;350;360;370;390;440;450;'; --lista wykluczen z ZSWA
    for
      select person,employee, personnames,  sfromdate, stodate, code, dimnum, dimden,workpost
        from get_especconditwork(:person_ref, :t_fromdate, :t_todate,:company, 0,0)
        order by personnames, person, employee, sfromdate
      into :person,:employee,:personnames, :sfromdate_current, :stodate_oryginal, :code, :dimnum, :dimden,:workpost
    do begin
      loop = 1;
      while (loop <> 0 and sfromdate_current <= stodate_oryginal) do
      begin
        absence_ref = null;
        select first 1 a.ref, a.fromdate
          from eabsences a
          where a.employee = :employee
            and a.correction in (0,2)
            and :recol_list like '%;' || a.ecolumn || ';%'
            and ((a.fromdate >= :sfromdate_current and a.fromdate <= :stodate_oryginal) or
                 (a.todate   >= :sfromdate_current and a.todate   <= :stodate_oryginal))
          order by a.fromdate
        into :absence_ref, :absence_start;

        if (absence_ref is not null) then
        begin
          select atodate from  e_get_whole_eabsperiod(:absence_ref,null,null,null,null,null,:recol_list)
            into :absence_end;

          if(absence_start > sfromdate_current) then
          begin
            sfromdate = sfromdate_current;
            stodate = absence_start - 1;
            suspend;
          end
          sfromdate_current = absence_end + 1;
        end else
        begin
          loop = 0;
          sfromdate = sfromdate_current;
          stodate = stodate_oryginal;
          suspend;
        end
      end
    end
  end
end^
SET TERM ; ^
