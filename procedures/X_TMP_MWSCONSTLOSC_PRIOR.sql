--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_TMP_MWSCONSTLOSC_PRIOR as
declare variable mwsconstloc mwsconstlocs_id;
declare variable prior integer_id;          
declare variable delta integer_id;
begin
  prior = 100;
  delta = 10;

  for select m.ref  from mwsconstlocs m
    where m.whsecdict ='M6'
    order by
      case
        when m.whsecrowdict in ('06','05') then 0
        when m.whsecrowdict in ('04','03') then 1
        when m.whsecrowdict in ('02','01') then 2
      else 3 
      end,
       m.whareanumber, m.whsecrowdict desc, m.symbol
  into :mwsconstloc
  do begin
    update mwsconstlocs m
      set m.routerownum=:prior
      where m.ref =:mwsconstloc;
    prior=prior+:delta;
  end

  for select m.ref  from mwsconstlocs m
    where m.whsecdict ='M5'  
    order by m.symbol
  into :mwsconstloc
  do begin
    update mwsconstlocs m
      set m.routerownum=:prior
      where m.ref =:mwsconstloc;
    prior=prior+:delta;
  end


  for select m.ref  from mwsconstlocs m
    where m.whsecdict ='N1'
    order by m.symbol
  into :mwsconstloc
  do begin
    update mwsconstlocs m
      set m.routerownum=:prior
      where m.ref =:mwsconstloc;
    prior=prior+:delta;
  end

  for select m.ref  from mwsconstlocs m
    where m.whsecdict ='P1'  
    order by m.symbol
  into :mwsconstloc
  do begin
    update mwsconstlocs m
      set m.routerownum=:prior
      where m.ref =:mwsconstloc;
    prior=prior+:delta;
  end

  for select m.ref  from mwsconstlocs m
    where m.whsecdict ='M4'  
    order by
      case
        when m.whsecrowdict in ('02','04') then 0
        when m.whsecrowdict in ('01','03') then 1
        when m.whsecrowdict in ('05','07') then 2  
        when m.whsecrowdict in ('06','08') then 3
        when m.whsecrowdict in ('10','12') then 4
        when m.whsecrowdict in ('09','11') then 5
        when m.whsecrowdict in ('13','15') then 6
        when m.whsecrowdict in ('14','16') then 7
        when m.whsecrowdict in ('18') then 8
        when m.whsecrowdict in ('17') then 9
      else 10
      end,
      case
        when m.whsecrowdict in ('02') then -m.whareanumber
        when m.whsecrowdict in ('04') then -m.whareanumber
        when m.whsecrowdict in ('01') then iif(m.whareanumber>19,iif(m.whareanumber>35,m.whareanumber+7,m.whareanumber+6),m.whareanumber)
        when m.whsecrowdict in ('03') then m.whareanumber
        when m.whsecrowdict in ('05') then -m.whareanumber
        when m.whsecrowdict in ('07') then -m.whareanumber
        when m.whsecrowdict in ('08') then m.whareanumber
        when m.whsecrowdict in ('06') then m.whareanumber
        when m.whsecrowdict in ('12') then -m.whareanumber
        when m.whsecrowdict in ('10') then -m.whareanumber
        when m.whsecrowdict in ('11') then m.whareanumber
        when m.whsecrowdict in ('09') then m.whareanumber
        when m.whsecrowdict in ('15') then -m.whareanumber
        when m.whsecrowdict in ('13') then -m.whareanumber
        when m.whsecrowdict in ('16') then m.whareanumber
        when m.whsecrowdict in ('14') then m.whareanumber
        when m.whsecrowdict in ('18') then -m.whareanumber
        when m.whsecrowdict in ('17') then m.whareanumber
      else 10
      end, 
      m.whsecrowdict, m.symbol
  into :mwsconstloc
  do begin
    update mwsconstlocs m
      set m.routerownum=:prior
      where m.ref =:mwsconstloc;
    prior=prior+:delta;
  end

  for select m.ref  from mwsconstlocs m
    where m.whsecdict ='M1G'
    order by
      m.whsecrowdict, m.whareanumber, m.whsecrowdict desc, m.symbol
  into :mwsconstloc
  do begin
    update mwsconstlocs m
      set m.routerownum=:prior
      where m.ref =:mwsconstloc;
    prior=prior+:delta;
  end

  for select m.ref  from mwsconstlocs m
    where m.whsecdict ='M1D'
    order by
      m.whsecrowdict, m.whareanumber, m.whsecrowdict desc, m.symbol
  into :mwsconstloc
  do begin
    update mwsconstlocs m
      set m.routerownum=:prior
      where m.ref =:mwsconstloc;
    prior=prior+:delta;
  end

  for select m.ref  from mwsconstlocs m
    where m.whsecdict ='M2G'  
    order by
      m.whsecrowdict, m.whareanumber, m.whsecrowdict desc, m.symbol
  into :mwsconstloc
  do begin
    update mwsconstlocs m
      set m.routerownum=:prior
      where m.ref =:mwsconstloc;
    prior=prior+:delta;
  end

  for select m.ref  from mwsconstlocs m
    where m.whsecdict ='M2D'   
    order by
      m.whsecrowdict, m.whareanumber, m.whsecrowdict desc, m.symbol
  into :mwsconstloc
  do begin
    update mwsconstlocs m
      set m.routerownum=:prior
      where m.ref =:mwsconstloc;
    prior=prior+:delta;
  end

  for select m.ref  from mwsconstlocs m
    where m.whsecdict ='M3G'   
    order by
      m.whsecrowdict, m.whareanumber, m.whsecrowdict desc, m.symbol
  into :mwsconstloc
  do begin
    update mwsconstlocs m
      set m.routerownum=:prior
      where m.ref =:mwsconstloc;
    prior=prior+:delta;
  end

  for select m.ref  from mwsconstlocs m
    where m.whsecdict ='M3D'   
    order by
      m.whsecrowdict, m.whareanumber, m.whsecrowdict desc, m.symbol
  into :mwsconstloc
  do begin
    update mwsconstlocs m
      set m.routerownum=:prior
      where m.ref =:mwsconstloc;
    prior=prior+:delta;
  end

  for select m.ref  from mwsconstlocs m
    where m.symbol ='MARKETING'
    order by
      m.whsecrowdict, m.whareanumber, m.whsecrowdict desc, m.symbol
  into :mwsconstloc
  do begin
    update mwsconstlocs m
      set m.routerownum=:prior
      where m.ref =:mwsconstloc;
    prior=prior+:delta;
  end

end^
SET TERM ; ^
