--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_ZAM_NAGLOWKI_DEL(
      DOKREF INTEGER_ID)
  returns (
      STATUS SMALLINT_ID,
      ERROR SMALLINT_ID,
      MSG STRING)
   as
declare variable grupasped integer_id;
declare variable mwsord integer_id;
declare variable mwsordstatus smallint_id;
begin
  status = 1;
  msg = '';

  select dn.grupasped from dokumnag dn where dn.ref = :dokref into :grupasped;

  if (grupasped is null) then begin
    status = 0;
    msg = 'dokument nie posiada grupy spedycyjnej';
  end else begin
    for
      select distinct o.ref, o.status
        from mwsacts a
          join mwsords o on (a.mwsord = o.ref)
        where a.doctype = 'M' and o.docgroup = :grupasped -- a.docid = new.ref
        order by o.ref desc
        into mwsord, mwsordstatus
    do begin
      if (mwsordstatus > 1 or exists(select ref from mwsacts where status > 1 and status < 6 and mwsord = :mwsord)) then begin
       status = 0;
       msg = 'Zlecenie jest w realizacji';
      end else begin
        status = 1;
        msg = '';
      end
    end
  end


  if (status = 1) then begin

    update dokumnag dn set dn.akcept = 0 where dn.ref = :dokref and dn.akcept <> 0;
    rdb$set_context('USER_TRANSACTION', 'INT_IMP_ZAM_NAGLOWKI_DEL', 1); --PR53533
    delete from dokumnag dn where dn.ref = :dokref;
    rdb$set_context('USER_TRANSACTION', 'INT_IMP_ZAM_NAGLOWKI_DEL', 0); --PR53533
  end
  suspend;
end^
SET TERM ; ^
