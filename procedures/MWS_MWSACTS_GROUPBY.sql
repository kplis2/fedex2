--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_MWSACTS_GROUPBY(
      VERS integer)
  returns (
      KEYA varchar(40) CHARACTER SET UTF8                           ,
      KEYB varchar(40) CHARACTER SET UTF8                           ,
      KEYC varchar(40) CHARACTER SET UTF8                           ,
      KEYD varchar(40) CHARACTER SET UTF8                           )
   as
declare variable keyas varchar(100);
declare variable keybs varchar(100);
declare variable keycs varchar(100);
declare variable keyds varchar(100);
begin
  execute procedure get_config('MWSATRA',0) returning_values :keyas;
  execute procedure get_config('MWSATRB',0) returning_values :keybs;
  execute procedure get_config('MWSATRC',0) returning_values :keycs;
  execute procedure get_config('MWSATRD',0) returning_values :keyds;
  if (:keyas is not null and :keyas <> '' and :vers > 0) then
    select substring(atrybuty.wartosc from 1 for 40)
      from atrybuty
      where atrybuty.wersjaref = :vers and atrybuty.cecha = :keyas
      into :keya;
  if (:keybs is not null and :keybs <> '' and :vers > 0) then
    select substring(atrybuty.wartosc from 1 for 40)
      from atrybuty
      where atrybuty.wersjaref = :vers and atrybuty.cecha = :keybs
      into :keyb;
  if (:keycs is not null and :keycs <> '' and :vers > 0) then
    select substring(atrybuty.wartosc from 1 for 40)
      from atrybuty
      where atrybuty.wersjaref = :vers and atrybuty.cecha = :keycs
      into :keyc;
  if (:keyds is not null and :keyds <> '' and :vers > 0) then
    select substring(atrybuty.wartosc from 1 for 40)
      from atrybuty
      where atrybuty.wersjaref = :vers and atrybuty.cecha = :keyds
      into :keyd;
end^
SET TERM ; ^
