--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SRV_CANCEL(
      SRVHISTORY integer)
  returns (
      STATUS integer,
      SYMBOLMAG varchar(255) CHARACTER SET UTF8                           ,
      SYMBOLFAK varchar(255) CHARACTER SET UTF8                           )
   as
declare variable srvrequest integer;
declare variable oldstatus integer;
declare variable getnumber smallint;
declare variable number integer;
declare variable contrtype integer;
declare variable numberg varchar(40);
declare variable reqdate timestamp;
declare variable symbolc varchar(20);
declare variable maxref integer;
declare variable dokumref integer;
declare variable dokumsym varchar(40);
declare variable getnumber2 smallint;
declare variable number2 integer;
declare variable numberg2 varchar(40);
declare variable symbolc2 varchar(20);
declare variable oddzial varchar(10);
declare variable blockref integer;
declare variable ckontrakt integer;
begin

  srvrequest = null;
  ckontrakt = null;
  symbolmag = '';
  symbolfak = '';

  select srvrequest,oldstatus,getnumber,getnumber2, ckontrakt
  from srvhistory where ref=:srvhistory
  into :srvrequest,:oldstatus,:getnumber,:getnumber2, :ckontrakt;

  /* Operacja dotyczy zgloszenia serwisowego */
  if (:srvrequest is not null) then begin
    select max(ref) from srvhistory where srvrequest=:srvrequest into :maxref;
    if(:srvhistory<>:maxref) then exception srvhistory_notlast;

    select number,contrtype,reqdate,number2,oddzial
    from srvrequests where ref=:srvrequest
    into :number,:contrtype,:reqdate,:number2,:oddzial;

    for
      select ref,symbol
      from dokumnag
      where (srvrequest=:srvrequest and historia=:srvhistory)
      order by ref desc
      into :dokumref,:dokumsym
    do begin
      update dokumnag set akcept=0 where ref=:dokumref;
      delete from dokumnag where ref=:dokumref;
      if(:dokumsym is not null and :dokumsym<>'') then begin
        if(symbolmag<>'') then symbolmag = :symbolmag || '; ';
        symbolmag = :symbolmag || :dokumsym;
      end
    end

    if(:getnumber=1) then begin
      if((:number>0) and (:number is not null)) then begin
        select numbergen,symbol from contrtypes where contrtypes.ref=:contrtype into :numberg,:symbolc;
        execute procedure free_number(:numberg,:oddzial,:symbolc,:reqdate,:number, 0) returning_values :blockref;
        update srvrequests set number=null,symbol='' where ref=:srvrequest;
      end
    end

    if(:getnumber2=1) then begin
      if((:number2>0) and (:number2 is not null)) then begin
        select numbergen2,symbol from contrtypes where contrtypes.ref=:contrtype into :numberg2,:symbolc2;
        execute procedure free_number(:numberg2,:oddzial,:symbolc2,:reqdate,:number2,0) returning_values :blockref;
        update srvrequests set number2=null,symbol2='' where ref=:srvrequest;
      end
    end

    update srvpositions set realized=0, srvhistory=null
    where srvrequest=:srvrequest and srvhistory=:srvhistory;

    update srvrequests set status=:oldstatus where ref=:srvrequest;

  end

  /* Operacja dotyczy sprawy */
  else if(:ckontrakt is not null) then begin
    select max(ref) from srvhistory where ckontrakt=:ckontrakt into :maxref;
    if(:srvhistory<>:maxref) then exception srvhistory_notlast;
    update ckontrakty set faza = :oldstatus where ref = :ckontrakt;
  end

  /* Operacja dotyczy zgloszenia serwisowego lub sprawy */
  delete from srvhistory where ref=:srvhistory;
  status = 1;

end^
SET TERM ; ^
