--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRMACHINE_FREE_WORKTIME(
      PRMACHINE integer,
      TIMESTART timestamp,
      TIMEEND timestamp)
  returns (
      WORKTIME double precision)
   as
declare variable calendar integer;
declare variable pause double precision;
declare variable repairtime double precision;
declare variable cdate date;
declare variable notworktime double precision;
declare variable repairpause double precision;
declare variable dayrepairpause double precision;
declare variable inactionfrom timestamp;
declare variable inactionto timestamp;
declare variable worktimefrom timestamp;
declare variable worktimeto timestamp;
begin
  if (timeend is null) then timeend = timestart + 365;
  -- kalendarz moze byc na maszynie albo na wydziale
  select first 1 coalesce(m.calendar, d.calendar)
    from prmachines m
      left join prmachineswplaces mw on (mw.prmachine = m.ref)
      left join prworkplaces w on (w.symbol = mw.prworkplace)
      left join prdeparts d on (w.prdepart = d.symbol)
    where m.ref = :prmachine and mw.fromdate <= :timestart
      and (mw.todate >= :timeend or mw.todate is null)
    into calendar;
  if (calendar is not null) then
  begin
    worktime = timeend - timestart;
    pause = 0;
    repairpause = 0;
    -- teraz trzeba wyliczyc ilosc przerw w kalendarzu, aby odjac od czasu calkowitego
    for
      select d.cdate, 1 - coalesce(d.prworktime,0),
          cast(cast(d.cdate as date) || ' ' || d.workstart as timestamp),
          cast(cast(d.cdate as date) || ' ' || d.workend as timestamp)
        from ecaldays d
        where d.calendar = :calendar
          and d.cdate >= cast(:timestart as date)
          and d.cdate < cast(:timeend as date)
        into cdate, notworktime, worktimefrom, worktimeto
    do begin
    --  if (cdate < cast(timeend as date)) then
        pause = pause + coalesce(notworktime,0);
      dayrepairpause = 0;
      -- teraz jeszcze trzeba odjac czas remontow i awarii w danym dniu
      for
        select r.inactionfrom, r.inactionto
          from prmachrepairs r
          where r.machine = :prmachine
            and ((r.inactionfrom >= :worktimefrom and r.inactionfrom <= :worktimeto)
              or (r.inactionto >= :worktimefrom and r.inactionto <= :worktimeto)
              or (:worktimefrom >= r.inactionfrom and :worktimeto <= r.inactionto))
            into inactionfrom, inactionto
      do begin
        if (inactionfrom >= worktimefrom and inactionto <= worktimeto) then
          dayrepairpause = inactionfrom - inactionto;
        else if (worktimefrom >= inactionfrom and worktimefrom <= inactionto) then
        begin
          if (inactionto >= worktimeto) then
            dayrepairpause = worktimeto - worktimefrom;
          else
            dayrepairpause = inactionto - worktimefrom;
        end else if (worktimeto >= inactionfrom and worktimeto <= inactionto) then
        begin
          if (inactionfrom <= worktimefrom) then
            dayrepairpause = worktimeto - worktimefrom;
          else
            dayrepairpause = inactionfrom - worktimefrom;
        end
      end
      repairpause = repairpause + dayrepairpause;
    end
    worktime = worktime - pause - repairpause;
  end else
    worktime = timeend - timestart;
end^
SET TERM ; ^
