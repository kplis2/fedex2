--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POR_DOK(
      REF integer)
  returns (
      KTM varchar(80) CHARACTER SET UTF8                           ,
      ILOSC1 numeric(14,4),
      CENA1 numeric(14,2),
      WARTOSC1 numeric(14,2),
      PWARTOSC1 numeric(14,2),
      WARTOSCL1 numeric(14,2),
      ILOSC2 numeric(14,4),
      CENA2 numeric(14,2),
      WARTOSC2 numeric(14,2),
      PWARTOSC2 numeric(14,2),
      WARTOSCL2 numeric(14,2))
   as
DECLARE VARIABLE REF2 INTEGER;
begin
  for  select ktm from dokumpoz join dokumnag on (dokumnag.ref = dokumpoz.dokument)
  where dokumnag.ref = :ref or (dokumnag.ref2 = :ref) group by ktm
  into :ktm
  do begin
    ILOSC1 = null;
    WARTOSC1 = null;
    PWARTOSC1 = null;
    WARTOSCL1 = null;
    ILOSC2 = null;
    CENA1 = null;
    CENA2 = null;
    WARTOSC2 = null;
    PWARTOSC2 = null;
    WARTOSCL2 = null;
    select sum(dokumpoz.ilosc), avg(dokumpoz.cenasr), sum(dokumpoz.wartosc), sum(dokumpoz.pwartosc),
        sum(dokumpoz.wartoscl) from dokumpoz where dokument = :ref and ktm = :ktm
      into :ilosc1, cena1, :wartosc1, :pwartosc1,: wartoscl1;
    select sum(dokumpoz.ilosc), avg(dokumpoz.cenasr), sum(dokumpoz.wartosc), sum(dokumpoz.pwartosc),
        sum(dokumpoz.wartoscl) from dokumpoz join dokumnag on (dokumnag.ref = dokumpoz.dokument)
      where dokumnag.ref2 = :ref and dokumpoz.ktm = :ktm
      into :ilosc2, :cena2, :wartosc2, :pwartosc2, :wartoscl2;
    if(:ilosc1 <> :ilosc2  or :pwartosc1 <> :pwartosc2 or :WARTOSC1 <> :wartosc2) then
      suspend;
  end
end^
SET TERM ; ^
