--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_PRINTLABEL_BOX(
      BOX_KTM_GEN STRING100,
      BOX_KTM STRING100,
      LISTYWYSD INTEGER_ID)
  returns (
      ZPL STRING1024)
   as
  declare variable eol string3;
  declare variable kodkresk string1024;

begin
eol = '
';
zpl = '';
-- exception universal 'test2';
if (BOX_ktm_gen is not null and LISTYWYSD is not null) then begin
-- budowanie kodu kreskowego
    kodkresk = BOX_ktm_gen;
-- budowanie etykiety
    zpl = 'CT~~CD,~CC^~CT~
          ^XA~TA000~JSN^LT0^MNW^MTD^PON^PMN^LH0,0^JMA^PR5,5~SD15^JUS^LRN^CI0^XZ
          ^XA
          ^MMT
          ^PW799
          ^LL1199
          ^LS0'||:eol;

      zpl = zpl||'^FT400,133^A0R,45,45^FH\^FDBox ktm:^FS
          ^FT540,133^A0R,45,45^FH\^FDBox SN:^FS
          ^FT630,133^A0R,45,45^FH\^FDData: ^FS
          ^FT720,133^A0R,45,45^FH\^FDL:^FS
          ^FO450,16^GB0,1147,4^FS
          ^FT400,356^A0R,56,55^FH\^FD'||coalesce(box_ktm, '')||'^FS
          ^FT540,356^A0R,56,55^FH\^FD'||coalesce(box_ktm_gen, '')||'^FS
          ^FT630,356^A0R,56,55^FH\^FD'||current_date||'^FS
          ^FT720,356^A0R,56,55^FH\^FD'||coalesce(listywysd, '')||'^FS
          ^BY4,3,160^FT137,133^BCR,,Y,N
          ^FD>:'||coalesce(kodkresk, '')||'^FS
          ^PQ1,0,1,Y^XZ';
  end
  suspend;
end^
SET TERM ; ^
