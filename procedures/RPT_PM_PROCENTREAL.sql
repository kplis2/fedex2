--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PM_PROCENTREAL(
      REF integer)
  returns (
      WYNIK numeric(14,4))
   as
declare variable mp numeric(14,4);
declare variable sp numeric(14,4);
declare variable rp numeric(14,4);
declare variable up numeric(14,4);
declare variable mrz numeric(14,4);
declare variable srz numeric(14,4);
declare variable spi numeric(14,4);
declare variable rrz numeric(14,4);
declare variable rpi numeric(14,4);
declare variable urz numeric(14,4);
begin
     select sum(case when (PO.POSITIONTYPE = 'M') then PO.CALCVAL end) as mp,
            sum(case when (PO.POSITIONTYPE = 'S') then PO.CALCVAL end) as sp,
            sum(case when (PO.POSITIONTYPE = 'R') then PO.CALCVAL end) as rp,
            sum(case when (PO.POSITIONTYPE = 'U') then PO.CALCVAL else 0 end) as up,
            sum(case when (PO.POSITIONTYPE = 'M') then PO.REALVAL end) as mrz,
            sum(case when (PO.POSITIONTYPE = 'S') then PO.REALQ end) as srz,
            sum(case when (PO.POSITIONTYPE = 'S') then PO.QUANTITY end) as spi,
            sum(case when (PO.POSITIONTYPE = 'R') then PO.REALQ end) as rrz,
            sum(case when (PO.POSITIONTYPE = 'R') then PO.QUANTITY end) as rpi,
            sum(case when (PO.POSITIONTYPE = 'U') then PO.REALVAL else 0 end) as urz
     from PMELEMENTS EL
        join PMPOSITIONS PO on (EL.REF = PO.PMELEMENT)
     where EL.PMPLAN = :ref
     into :mp, :sp, :rp, :up, :mrz, :srz, :spi, :rrz, :rpi, :urz;

     wynik = (mrz / (mp + sp + rp + up)) + ((sp * srz) / ((mp + sp + rp + up) * spi)) + ((rp * rrz) / ((mp + sp + rp + up) * rpi)) + (urz / (mp + sp + rp + up));

  suspend;
end^
SET TERM ; ^
