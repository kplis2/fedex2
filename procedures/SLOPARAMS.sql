--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SLOPARAMS(
      SLODEFREF integer,
      FILTRIN varchar(1024) CHARACTER SET UTF8                           )
  returns (
      RESULT smallint,
      SLONAZWA varchar(255) CHARACTER SET UTF8                           ,
      INDEKS varchar(255) CHARACTER SET UTF8                           ,
      PREFIX varchar(255) CHARACTER SET UTF8                           ,
      REFFIELD varchar(255) CHARACTER SET UTF8                           ,
      KODFIELD varchar(255) CHARACTER SET UTF8                           ,
      KODKSFIELD varchar(255) CHARACTER SET UTF8                           ,
      NAZWAFIELD varchar(255) CHARACTER SET UTF8                           ,
      NIPFIELD varchar(255) CHARACTER SET UTF8                           ,
      FILTR varchar(1024) CHARACTER SET UTF8                           ,
      FILTRNAME varchar(255) CHARACTER SET UTF8                           )
   as
declare variable SLODEFTYP varchar(255);
declare variable MSTABLE varchar(255);
declare variable FMSTABLE varchar(255);
declare variable CURRCOMPANY varchar(255);
declare variable AKTUOP varchar(255);
declare variable APPLIC varchar(255);
declare variable CONSTFILTER varchar(1024);
declare variable CFILTER varchar(255);
declare variable ISCOMP smallint;
begin
  if(slodefref <= 0) then
    result = 0;
  else
  begin
    execute procedure get_global_param('CURRENTCOMPANY') returning_values :currcompany;
    execute procedure get_global_param('AKTUOPERATOR') returning_values :aktuop;
    execute procedure get_global_param('SLOWNIKDICT_FILTER') returning_values :constfilter;

    if(exists(select first 1 1 from slodef where ref = :slodefref)) then
    begin
      select s.typ, s.mastertable, s.kodfield, s.nazwafield, s.kodksfield, s.nipfield, s.iscompany
      from slodef s
      where s.ref = :slodefref
      into :slodeftyp, :mstable, :kodfield, :nazwafield, :kodksfield, :nipfield, :iscomp;
    
      if(:slodeftyp = 'ESYSTEM') then
      begin
        indeks = 'SLOWNIK';
        prefix = slodefref;
        slonazwa = 'SLOPOZ';
      end else begin
        slonazwa = slodeftyp;
        mstable = '';
      end
    
      reffield = 'REF';
      nipfield = '';
      filtrname = '';
      filtr = coalesce(filtrin,'');    
      if(:slonazwa = 'KLIENCI') then
      begin
        kodfield = 'FSKROT';
        nazwafield = 'NAZWA';
        kodksfield = 'KONTOFK';
        nipfield = 'NIP';
        if(coalesce(iscomp,0) <> 0) then
        begin
        indeks = 'COMPANY';
        prefix = currcompany;
          if (coalesce(filtr,'') <> '') then  
            filtr = filtr || ' and ';
          filtr = filtr || 'COMPANY = '||:prefix;
        end
      end
      else if(:slonazwa = 'PKLIENCI') then
      begin
        kodfield = 'SKROT';
        nazwafield = 'NAZWA';
        kodksfield = '';
        nipfield = '';
      end
      else if(:slonazwa = 'DOSTAWCY') then
      begin
        kodfield = 'ID';
        nazwafield = 'NAZWA';
        kodksfield = 'KONTOFK';
        nipfield = 'NIP';
        if(coalesce(iscomp,0) <> 0) then
        begin
        indeks = 'COMPANY';
        prefix = currcompany;
          if (coalesce(filtr,'') <> '') then  
            filtr = filtr || ' and ';
          filtr = filtr || 'COMPANY = '||:prefix;
        end
      end
      else if(slonazwa = 'ODBIORCY') then
      begin
        kodfield = 'NAZWA';
        nazwafield = 'NAZWA';
        kodksfield = '';
        nipfield = '';
      end
      else if(slonazwa = 'SPRZEDAWCY') then
      begin
        kodfield = 'SKROT';
        nazwafield = 'NAZWA';
        kodksfield = 'KONTOFK';
        nipfield = 'NIP';
      end
      else if(slonazwa = 'VAT') then
      begin
        kodfield = 'GRUPA';
        nazwafield = 'NAME';
        kodksfield = 'GRUPA';
        nipfield = '';
      end
      else if(slonazwa = 'GRVAT') then
      begin
        kodfield = 'SYMBOL';
        nazwafield = 'DESCRIPT';
        kodksfield = 'SYMBOL';
        nipfield = '';
      end
      else if(slonazwa = 'WALUTY') then
      begin
        kodfield = 'SYBMOL';
        nazwafield = 'NAZWA';
        kodksfield = 'SYMBOL';
        nipfield = '';
      end
      else if(slonazwa = 'ODDZIALY') then
      begin
        kodfield = 'SYMBOL';
        nazwafield = 'NAZWA';
        kodksfield = 'SYMBOL';
        nipfield = '';
        indeks = 'COMPANY';
        prefix = :currcompany;
        if (coalesce(filtr,'') <> '') then
          filtr = filtr || ' and ';
        filtr = filtr || 'COMPANY = '||:prefix;
      end
      else if(slonazwa = 'FXDASSETS') then
      begin
        kodfield = 'SYMBOL';
        nazwafield = 'NAME';
        kodksfield = 'SYMBOL';
        nipfield = '';
        indeks = 'COMPANY';
        prefix = :currcompany;
        if (coalesce(filtr,'') <> '') then
          filtr = filtr || ' and ';
        filtr = filtr || 'COMPANY = '||:prefix;
      end
      else if(slonazwa = 'DEFMAGAZ') then
      begin
        kodfield = 'SYMBOL';
        nazwafield = 'OPIS';
        kodksfield = 'BKSYMBOL';
        nipfield = '';
      end
      else if(slonazwa = 'PERSONS') then
      begin
        kodfield = 'SYMBOL';
        nazwafield = 'PERSON';
        kodksfield = 'SYMBOL';
        nipfield = '';
        select applications from operator
        where ref = :aktuop into :applic;
        if(position('EP' in coalesce(applic,'')) > 0) then
          slonazwa = 'FK_PERSONS';
        else
          slonazwa = 'CPERSONS'; --PR59074 
        if(coalesce(iscomp,0) <> 0) then
        begin
        indeks = 'COMPANY';
        prefix = currcompany;
          if (coalesce(filtr,'') <> '') then  
            filtr = filtr || ' and ';
          filtr = filtr || 'COMPANY = '||:prefix;
        end
      end
      else if(slonazwa = 'EMPLOYEES') then
      begin
        kodfield = 'SYMBOL';
        nazwafield = 'PERSONNAMES';
        kodksfield = 'SYMBOL';
        nipfield = '';
        if(coalesce(iscomp,0) <> 0) then
        begin
        indeks = 'COMPANY';
        prefix = currcompany;
          if (coalesce(filtr,'') <> '') then  
            filtr = filtr || ' and ';
          filtr = filtr || 'COMPANY = '||:prefix;
        end
      end
      else if(slonazwa = 'EINTERNALREVS') then
      begin
        kodfield = 'CODE';
        nazwafield = 'NAME';
        kodksfield = 'CODE';
        nipfield = '';
      end
      else if(slonazwa = 'DEPARTMENTS') then
      begin
        kodfield = 'SYMBOL';
        nazwafield = 'NAME';
        kodksfield = 'SYMBOL';
        nipfield = '';
        indeks = 'COMPANY';
        prefix = :currcompany;
        if (coalesce(filtr,'') <> '') then
          filtr = filtr || ' and ';
        filtr = filtr || 'COMPANY = '||:prefix;
      end
      else if(slonazwa = 'SLOPOZ') then
      begin
        kodfield = 'KOD';
        nazwafield = 'NAZWA';
        kodksfield = 'KONTOKS';
        nipfield = '';
      end
      else if(slonazwa = 'CKONTRAKTYE') then
      begin
        kodfield = 'KONTRETAP';
        nazwafield = '';
        kodksfield = '';
        nipfield = '';
      end
    
      if(coalesce(:constfilter,'') <> '') then
      begin
        if (:constfilter <> ';' and :constfilter <> ';;') then
          begin
            for select outstring from parse_lines(:constfilter,';') into :cfilter
            do begin
              fmstable = substring(cfilter from 1 for (position(cfilter,':')-1));
              if(fmstable = mstable) then
              begin
                cfilter = substring(cfilter from (position(cfilter,':')+1));
                if(position(cfilter,'[') > 0) then
                begin
                  filtr = substring(cfilter from 1 for (position(cfilter,'[')-1));
                  cfilter = substring(cfilter from (position(cfilter,'[')+1));
                end else
                begin
                  filtr = cfilter;
                  cfilter = '';
                end
                if(CHAR_LENGTH(cfilter) > 0 and position(cfilter,']') > 0) then
                begin
                  filtrname = substring(cfilter from 1 for (position(cfilter,']')-1));
                end
              end
            end
          end
      end
      result = 1;
    end
    else
      result = 0;
  end
  suspend;
end^
SET TERM ; ^
