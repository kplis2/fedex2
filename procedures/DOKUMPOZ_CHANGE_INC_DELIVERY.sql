--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMPOZ_CHANGE_INC_DELIVERY(
      KORTOPOZ integer,
      POZDOKUM integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      ILOSC numeric(14,5),
      SERIALIZED smallint,
      BLOKOBLNAG smallint,
      BLOKPOZNALICZ integer,
      CENAMAG numeric(14,4),
      DOSTAWA integer,
      MAG_TYP char(1) CHARACTER SET UTF8                           ,
      MAG char(3) CHARACTER SET UTF8                           ,
      FIFO char(1) CHARACTER SET UTF8                           ,
      MINUS_STAN smallint,
      MAGBREAK integer,
      ZAMOWIENIE integer,
      WERSJAREF integer,
      OPK smallint,
      OPKTRYB smallint,
      SLODEF integer,
      SLOPOZ integer,
      CENASNET numeric(14,4),
      CENASBRU numeric(14,4),
      BN char(1) CHARACTER SET UTF8                           ,
      VAT numeric(14,4),
      STANOPKREF integer,
      ORG_ILOSC numeric(14,4),
      DOK_ILOSC numeric(14,4),
      CENA_KOSZT numeric(14,4))
   as
declare variable numer integer;
declare variable i numeric(14,5);
declare variable j numeric(14,5);
declare variable j2 numeric(14,5);
declare variable is_ilosc numeric(14,4);
declare variable lcenamag numeric(14,4);
declare variable ldostawa integer;
declare variable prevcenamag numeric(14,4);
declare variable previldost numeric(14,4);
declare variable jeststcen smallint;
declare variable serialnr varchar(40);
declare variable serialil numeric(14,4);
declare variable rozref integer;
declare variable korilosc numeric(14,4);
declare variable koriloscl numeric(14,4);
declare variable orgdokumser integer;
declare variable dokument integer;
declare variable data timestamp;
declare variable symbdost varchar(255);
declare variable nazwa_ktm towary_nazwa; --KPI
begin 
      ---KP ZG137130
      select twk.nazwa from towary twk
      where twk.ktm = :ktm
      into :nazwa_ktm;
      --- end KP

      for select ILOSC,CENASNETTO,CENASBRUTTO
      from OPKSTAN_FIFO(:slodef,:slopoz,:ktm,:wersja,:opktryb,:ilosc,:CENASNET, :CENASBRU,:mag,:bn,:vat,:stanopkref,1)
      into :ilosc,:cenasnet,:cenasbru
      do begin
      /*============= magazyn ewidencyjny i wielu cen fifo wedlug */

        /*akceptowanie rozpisek zaozonych rcznie*/
        if(:fifo = 'R') then begin
            is_ilosc = null;
            select sum(ilosc) from DOKUMROZ where pozycja=:pozdokum and ACK = 0 into :is_ilosc;
            if(:is_ilosc is null) then is_ilosc = 0;
            /* jesli rozpiska reczna, to trzeba zaakceptowac istniejace rozpiski */
            if(:is_ilosc > 0) then begin
              update DOKUMROZ set ACK =1 where pozycja=:pozdokum and ACK=0;
              ilosc = :ilosc - :is_ilosc;
              if(:ilosc < 0) then exception MAGDOK_ROZ_TO_BIG;
            end
        end

        if(:kortopoz>0) then begin
          /* rozpisanie z partii dokumentu korygowanego */
          for select DOKUMROZ.REF, DOKUMROZ.iloscl, dokumroz.cena, dokumroz.dostawa, dokumroz.cenasnetto, dokumroz.cenasbrutto, dokumroz.cena_koszt
          from DOKUMROZ
          where DOKUMROZ.POZYCJA = :kortopoz and DOKUMROZ.iloscl > 0
          order by numer desc
          into :rozref, :j, :lcenamag, :ldostawa, :cenasnet, :cenasbru, :cena_koszt
          do begin
            if(:j is null) then j = 0;
            j2 = 0;
            select ILOSC-ZABLOKOW from STANYCEN
              where STANYCEN.MAGAZYN=:mag and STANYCEN.KTM=:ktm and STANYCEN.WERSJA=:wersja and STANYCEN.cena=:lcenamag and STANYCEN.dostawa = :ldostawa
            into :j2;
            execute procedure DOKUMPOZ_ILDOST(:j, :zamowienie, :mag_typ, :mag, :ktm, :wersja, :lcenamag, :ldostawa, :dostawa, :pozdokum) returning_values :j;
            if(:j > :ilosc) then j = :ilosc;
            if(:j > :j2) then j = :j2;
            if(:j > 0) then begin
                numer = null;
                select NUMER from DOKUMROZ where KORTOROZ = :rozref and pozycja = :pozdokum into :numer;
                if(:numer is null) then begin
                  select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
                  if(:numer is null or (:numer = 0))then numer = 0;
                  numer = numer +  1;
                  insert into DOKUMROZ(POZYCJA, NUMER,ILOSC, CENA, DOSTAWA, SERIALNR, CENASNETTO, CENASBRUTTO, KORTOROZ, BLOKOBLNAG, CENA_KOSZT)
                  values (:pozdokum, :numer, :j, :lcenamag, :ldostawa, '', :cenasnet, :cenasbru, :rozref, :blokoblnag, :cena_koszt);
                end
                else
                  update DOKUMROZ set ILOSC = ILOSC + :j, PCENA = null where pozycja = :pozdokum and numer = :numer;
                ilosc = :ilosc - :j;
            end
          end
        end

      /* rozpisanie wedlug zaożeń wskazanych na pozycji dokumentu magazynowego */
      if(((:serialized = 1) or (:mag_typ = 'E') or (:mag_typ = 'S') or ((:mag_typ = 'C' or (:mag_typ = 'P'))and ((:fifo='S') or (:fifo='R')))) and (:ilosc > 0)) then begin
        prevcenamag = -1;
        previldost = 0;
        jeststcen = 0;
        if(:serialized = 1) then begin
          /*zdjcie konkretnych numerów seryjnych - wszystko jasne*/
          for select SERIALNR, ILOSC, ORGDOKUMSER from DOKUMPOZ_SERIAL(:pozdokum,0)
          into :serialnr, :serialil, :orgdokumser
          do begin
            if(:ilosc > 0 and :serialnr is not null) then begin
              if(:serialil is null) then serialil = 1;
              /*petla po stanach seryjnych - bo dana seria może być w róznych cenach-dostawach*/
              for select STANYCEN.CENA, STANYCEN.DOSTAWA, STANYSER.ILOSC, STANYCEN.CENA_KOSZT
              from STANYCEN join STANYSER on (stanyser.stancen = stanycen.ref)
              where STANYCEN.wersjaref = :wersjaref and STANYCEN.MAGAZYN = :mag and STANYSER.SERIALNR = :serialnr
              and STANYSER.ILOSC>0
              order by STANYSER.DATA
              into :lcenamag, :ldostawa, :i, :cena_koszt
              do begin
                if(:i > :serialil) then begin
                   j = :serialil; /* zdjecie calej ilosci potrzebnej*/
                end else begin
                   j = i;
                end
                /*uzupelnienie istnijacej rozpiski lub dodanie nowej*/
                if(:j > 0) then begin
                  numer = null;
                  select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa and serialnr = :serialnr into :numer;
                  if(numer is null) then begin
                     select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
                     if(:numer is null or (:numer = 0))then numer = 0;
                     numer = numer +  1;
                     insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,SERIALNR,BLOKOBLNAG,CENA_KOSZT,ORGDOKUMSER) values (:pozdokum,:numer,:j,:lcenamag,:ldostawa, :serialnr,:blokoblnag,:cena_koszt,:orgdokumser);
                  end else begin
                     update DOKUMROZ set ILOSC = ILOSC + :j, PCENA = null where pozycja = :pozdokum and numer = :numer;
                  end
                end
                ilosc = :ilosc - :j;
                serialil = :serialil - :j;
              end
              if(:serialil > 0) then exception STANYCEN_BRAKSERIAL;

            end
          end
        /* rozpisywanie zgodnei z zasadą rozchodu*/
        end else for select CENA,DOSTAWA,ILOSC-ZABLOKOW,CENA_KOSZT
          from STANYCEN where KTM = :ktm and WERSJA = :wersja and MAGAZYN = :mag
          AND (CENA = :CENAMAG or (:CENAMAG is null) or (:cenamag = 0) or (:magbreak = 0 and :mag_typ <> 'E'))
          AND ( DOSTAWA = :DOSTAWA or (:DOSTAWA is null) or (:dostawa = 0) or (:magbreak = 0))
          AND ILOSC>0
          order by datafifo into :lcenamag, :ldostawa, :i, :cena_koszt
        do begin
          jeststcen = 1;
          if(:ilosc > 0) then begin
            if(:i is null) then i = 0;
            execute procedure DOKUMPOZ_ILDOST(:i, :zamowienie, :mag_typ, :mag, :ktm, :wersja, :lcenamag, :ldostawa, :dostawa, :pozdokum) returning_values :i;
            if(:i > :ilosc) then begin
               j = ilosc; /* zdjecie calej ilosci potrzebnej*/
            end else begin
              j = i;
            end
            /*uzupelnienie istnijacej rozpiski lub dodanie nowej*/
            if(:j > 0) then begin
              numer = null;
              if(:opktryb = 1 and :opk = 1) then
              select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa and CENASNETTO = :cenasnet into :numer;
              else
              select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa into :numer;
              if(numer is null) then begin
                 select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
                 if(:numer is null or (:numer = 0))then numer = 0;
                 numer = numer +  1;
                 insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO, CENASBRUTTO, BLOKOBLNAG, CENA_KOSZT) values (:pozdokum,:numer,:j,:lcenamag,:ldostawa,:cenasnet, :cenasbru, :blokoblnag, :cena_koszt);
              end else begin
                  update DOKUMROZ set ILOSC = ILOSC + :j, PCENA = null where pozycja = :pozdokum and numer = :numer;
              end
            end
            ilosc = :ilosc - :j;
          end
        end
        /*------------*/
        /* dodatkowo szukamy z innej dostawy ale po tej samej cenie */
        if(:magbreak = 1 AND :ilosc > 0) then begin
          for select CENA,DOSTAWA,ILOSC -ZABLOKOW, CENA_KOSZT
          from STANYCEN where KTM = :ktm and WERSJA = :wersja and MAGAZYN = :mag
          and(CENA=:CENAmag and (:mag_typ <> 'E'))
          and ILOSC>0
          order by datafifo into :lcenamag, :ldostawa, :i, :cena_koszt
          do begin
            jeststcen = 1;
            if(:ilosc > 0) then begin
              if(:i is null) then i = 0;
              execute procedure DOKUMPOZ_ILDOST(:i, :zamowienie, :mag_typ, :mag, :ktm, :wersja, :lcenamag, :ldostawa, :dostawa, :pozdokum) returning_values :i;
              if(:i > :ilosc) then begin
                 j = ilosc; /* zdjecie calej ilosci potrzebnej*/
              end else begin
                j = i;
              end
              /*uzupelnienie istnijacej rozpiski lub dodanie nowej*/
              if(:j > 0) then begin
                numer = null;
                if(:opktryb = 1 and :opk = 1) then
                select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa and CENASNETTO = :cenasnet into :numer;
                else
                select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa into :numer;
                if(numer is null) then begin
                   select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
                   if(:numer is null or (:numer = 0))then numer = 0;
                   numer = numer +  1;
                  insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO,CENASBRUTTO,blokoblnag,CENA_KOSZT) values (:pozdokum,:numer,:j,:lcenamag,:ldostawa,:cenasnet,:cenasbru,:blokoblnag,:cena_koszt);
                end else
                    update DOKUMROZ set ILOSC = ILOSC + :j, PCENA = null where pozycja = :pozdokum and numer = :numer;
              end
              ilosc = ilosc - j;
            end
          end
        end
        /*------------*/
        /* dodatkowo szukamy z innej dostawy po dowolnej cenie */
        if(:magbreak = 1 AND :ilosc > 0) then begin
          for select CENA,DOSTAWA,ILOSC -ZABLOKOW, CENA_KOSZT
          from STANYCEN where KTM = :ktm and WERSJA = :wersja and MAGAZYN = :mag
          and(CENA<>:CENAmag and (:mag_typ <> 'E'))
          and ILOSC>0
          order by datafifo into :lcenamag, :ldostawa, :i, :cena_koszt
          do begin
            jeststcen = 1;
            if(:ilosc > 0) then begin
              if(:i is null) then i = 0;
              execute procedure DOKUMPOZ_ILDOST(:i, :zamowienie, :mag_typ, :mag, :ktm, :wersja, :lcenamag, :ldostawa, :dostawa, :pozdokum) returning_values :i;
              if(:i > :ilosc) then begin
                 j = ilosc; /* zdjecie calej ilosci potrzebnej*/
              end else begin
                j = i;
              end
              /*uzupelnienie istnijacej rozpiski lub dodanie nowej*/
              if(:j > 0) then begin
                numer = null;
                if(:opktryb = 1 and :opk = 1) then
                select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa and CENASNETTO = :cenasnet into :numer;
                else
                select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa into :numer;
                if(numer is null) then begin
                   select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
                   if(:numer is null or (:numer = 0))then numer = 0;
                   numer = numer +  1;
                  insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO,CENASBRUTTO,blokoblnag,CENA_KOSZT) values (:pozdokum,:numer,:j,:lcenamag,:ldostawa,:cenasnet,:cenasbru,:blokoblnag,:cena_koszt);
                end else
                    update DOKUMROZ set ILOSC = ILOSC + :j, PCENA = null where pozycja = :pozdokum and numer = :numer;
              end
              ilosc = ilosc - j;
            end
          end
        end
        if(:ilosc > 0 AND :minus_stan <> 1) then exception DOKUM_ROZ_NOT_ENOUGHT_SC 'Brak stanów magazynowych dla towaru: '||:ktm || ' ' || :nazwa_ktm;
        if(:ilosc > 0 and :minus_stan = 1 and :jeststcen = 0) then begin
          /* nie ma w ogole stanu magazynowego wiec schodzimy na minus po cenie katalogowej ze sztucznej dostawy */
          select dokument from dokumpoz where ref=:pozdokum into :dokument;
          select data from dokumnag where ref=:dokument into :data;
          symbdost = '-'||:mag||'/'||cast(:data as date);
          execute procedure DOKUMNAG_CREATE_DOKMAG_DOSTAWA(:dokument,NULL,:data,:mag,NULL,:pozdokum,:symbdost,NULL) returning_values :ldostawa;
          select cena_zakn from wersje where KTM=:ktm and NRWERSJI=:wersja into :lcenamag;
          if(:lcenamag=0) then select cena_zakn from towary where KTM=:ktm into :lcenamag;
          cena_koszt = :lcenamag;
          jeststcen = 1;
        end
        if(:ilosc > 0 and :jeststcen = 1) then begin
          /* dodanie ilosci ponizej stanow na bazie ostatniego stanu cenowego*/
            numer = null;
            if(:opktryb = 1 and :opk = 1) then
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa and CENASNETTO = :cenasnet into :numer;
            else
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa into :numer;
            if(numer is null) then begin
               select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
               if(:numer is null or (:numer = 0))then numer = 0;
               numer = numer +  1;
               insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO, CENASBRUTTO,blokoblnag,CENA_KOSZT) values (:pozdokum,:numer,:ilosc,:lcenamag,:ldostawa,:CENASNET,:cenasbru,:blokoblnag,:cena_koszt);
            end else begin
                update DOKUMROZ set ILOSC = ILOSC + :ilosc, PCENA = null where pozycja = :pozdokum and numer = :numer;
            end
            ilosc = 0;
        end else if(:ilosc > 0 and :mag_typ = 'E') then begin
          /*zejcie bez stanów - poniżej zera - szukanie na stanach ilociowych ceny do ewidencyjnego */
          lcenamag = null;
          i = null;
          select cena, ilosc from STANYIL where MAGAZYN=:mag and KTM=:ktm AND WERSJA=:WERSJA and ilosc <> 0 into :lcenamag, :i;
          if(:i is null) then i = 0;
          if(:cenamag > 0 and :cenamag <> :lcenamag and i <> 0) then exception DOKUM_ROZ_E_ZLA_CENA;
          if(:lcenamag > 0) then begin
            numer = null;
            if(:opktryb = 1 and :opk = 1) then
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and CENASNETTO = :cenasnet into :numer;
            else
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag into :numer;
            if(numer is null) then begin
               select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
               if(:numer is null or (:numer = 0))then numer = 0;
               numer = numer +  1;
               insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO,CENASBRUTTO,blokoblnag) values (:pozdokum,:numer,:ilosc,:lcenamag,NULL,:cenasnet,:cenasbru,:blokoblnag);
            end else begin
                update DOKUMROZ set ILOSC = ILOSC + :ilosc, PCENA = null where pozycja = :pozdokum and numer = :numer;
            end
            ilosc = 0;
          end
        end
        if(:ilosc > 0) then begin
          /* zejcie poniżej stanów bez stanów zgodnie z danycmi w parametrach */
          if((:cenamag > 0) and(:dostawa > 0 or (:mag_typ <> 'P'))) then begin
            numer = null;
            if(:opktryb = 1 and :opk = 1) then
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:cenamag and (dostawa = :dostawa or (:mag_typ <> 'P')) and CENASNETTO = :cenasnet into :numer;
            else
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:cenamag and (dostawa = :dostawa or (:mag_typ <> 'P'))into :numer;
            if(numer is null) then begin
               select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
               if(:numer is null or (:numer = 0))then numer = 0;
               numer = numer +  1;
               insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,cenasnetto,cenasbrutto,blokoblnag,CENA_KOSZT) values (:pozdokum,:numer,:ilosc,:cenamag,:dostawa,:cenasnet, :cenasbru,:blokoblnag,:cena_koszt);
            end else begin
                update DOKUMROZ set ILOSC = ILOSC + :ilosc, PCENA = null where pozycja = :pozdokum and numer = :numer;
            end
            ilosc = 0;
          end
        end
        if(:ilosc > 0) then
          exception DOKUM_ROZ_BEZ_STC substring('Brak stanów cenowych na ' || :mag || ' dla: ' || :ktm from 1 for 75);
      end else
-- gosp.magazynowa wg LIFO (od najmlodszych dostaw)
      if((:mag_typ = 'C' or :mag_typ = 'P') and (:fifo='M') and (:ilosc > 0)) then begin
        prevcenamag = -1;
        previldost = 0;
        jeststcen = 0;
        /*akceptowanie rozpisek zaozonych rcznie*/
        if(:serialized <> 1) then
          for select CENA,DOSTAWA,ILOSC-ZABLOKOW,CENA_KOSZT
            from STANYCEN where KTM = :ktm and WERSJA = :wersja and MAGAZYN = :mag
            AND (CENA = :CENAMAG or (:CENAMAG is null) or (:cenamag = 0) or (:magbreak = 0 and :mag_typ <> 'E'))
            AND ( DOSTAWA = :DOSTAWA or (:DOSTAWA is null) or (:dostawa = 0) or (:magbreak = 0))
            AND ILOSC>0
            order by datafifo desc into :lcenamag, :ldostawa, :i, :cena_koszt
          do begin
            jeststcen = 1;
            if(:ilosc > 0) then begin
              if(:i is null) then i = 0;
              execute procedure DOKUMPOZ_ILDOST(:i, :zamowienie, :mag_typ, :mag, :ktm, :wersja, :lcenamag, :ldostawa, :dostawa, :pozdokum) returning_values :i;
              if(:i > :ilosc) then begin
                 j = ilosc; /* zdjecie calej ilosci potrzebnej*/
              end else begin
                j = i;
              end
              /*uzupelnienie istnijacej rozpiski lub dodanie nowej*/
              if(:j > 0) then begin
                numer = null;
                if(:opktryb = 1 and :opk = 1) then
                select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa and CENASNETTO = :cenasnet into :numer;
                else
                select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa into :numer;
                if(numer is null) then begin
                   select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
                   if(:numer is null or (:numer = 0))then numer = 0;
                   numer = numer +  1;
                   insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO, CENASBRUTTO, BLOKOBLNAG, CENA_KOSZT) values (:pozdokum,:numer,:j,:lcenamag,:ldostawa,:cenasnet, :cenasbru, :blokoblnag, :cena_koszt);
                end else begin
                    update DOKUMROZ set ILOSC = ILOSC + :j, PCENA = null where pozycja = :pozdokum and numer = :numer;
                end
              end
              ilosc = :ilosc - :j;
            end
          end
        /*------------*/
        if(:magbreak = 1 AND :ilosc > 0) then begin
          for select CENA,DOSTAWA,ILOSC -ZABLOKOW, CENA_KOSZT
          from STANYCEN where KTM = :ktm and WERSJA = :wersja and MAGAZYN = :mag
          and(CENA=:CENAmag and (:mag_typ <> 'E'))
          and ILOSC>0
          order by datafifo desc into :lcenamag, :ldostawa, :i, :cena_koszt
          do begin
            jeststcen = 1;
            if(:ilosc > 0) then begin
              if(:i is null) then i = 0;
              execute procedure DOKUMPOZ_ILDOST(:i, :zamowienie, :mag_typ, :mag, :ktm, :wersja, :lcenamag, :ldostawa, :dostawa, :pozdokum) returning_values :i;
              if(:i > :ilosc) then begin
                 j = ilosc; /* zdjecie calej ilosci potrzebnej*/
              end else begin
                j = i;
              end
              /*uzupelnienie istnijacej rozpiski lub dodanie nowej*/
              if(:j > 0) then begin
                numer = null;
                if(:opktryb = 1 and :opk = 1) then
                select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa and CENASNETTO = :cenasnet into :numer;
                else
                select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa into :numer;
                if(numer is null) then begin
                   select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
                   if(:numer is null or (:numer = 0))then numer = 0;
                   numer = numer +  1;
                  insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO,CENASBRUTTO,blokoblnag,CENA_KOSZT) values (:pozdokum,:numer,:j,:lcenamag,:ldostawa,:cenasnet,:cenasbru,:blokoblnag,:cena_koszt);
                end else
                    update DOKUMROZ set ILOSC = ILOSC + :j, PCENA = null where pozycja = :pozdokum and numer = :numer;
              end
              ilosc = ilosc - j;
            end
          end
        end
        /*------------*/
        if(:magbreak = 1 AND :ilosc > 0) then begin
          for select CENA,DOSTAWA,ILOSC -ZABLOKOW, CENA_KOSZT
          from STANYCEN where KTM = :ktm and WERSJA = :wersja and MAGAZYN = :mag
          and(CENA<>:CENAmag and (:mag_typ <> 'E'))
          and ILOSC>0
          order by datafifo desc into :lcenamag, :ldostawa, :i, :cena_koszt
          do begin
            jeststcen = 1;
            if(:ilosc > 0) then begin
              if(:i is null) then i = 0;
              execute procedure DOKUMPOZ_ILDOST(:i, :zamowienie, :mag_typ, :mag, :ktm, :wersja, :lcenamag, :ldostawa, :dostawa, :pozdokum) returning_values :i;
              if(:i > :ilosc) then begin
                 j = ilosc; /* zdjecie calej ilosci potrzebnej*/
              end else begin
                j = i;
              end
              /*uzupelnienie istnijacej rozpiski lub dodanie nowej*/
              if(:j > 0) then begin
                numer = null;
                if(:opktryb = 1 and :opk = 1) then
                select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa and CENASNETTO = :cenasnet into :numer;
                else
                select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa into :numer;
                if(numer is null) then begin
                   select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
                   if(:numer is null or (:numer = 0))then numer = 0;
                   numer = numer +  1;
                  insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO,CENASBRUTTO,blokoblnag,CENA_KOSZT) values (:pozdokum,:numer,:j,:lcenamag,:ldostawa,:cenasnet,:cenasbru,:blokoblnag,:cena_koszt);
                end else
                    update DOKUMROZ set ILOSC = ILOSC + :j, PCENA = null where pozycja = :pozdokum and numer = :numer;
              end
              ilosc = ilosc - j;
            end
          end
        end
        if(:ilosc > 0 AND :minus_stan <> 1) then exception DOKUM_ROZ_NOT_ENOUGHT_SC 'Brak stanów magazynowych dla towaru: '||:ktm|| ' ' || :nazwa_ktm;
        if(:ilosc > 0 and :minus_stan = 1 and :jeststcen = 0) then begin
          /* nie ma w ogole stanu magazynowego wiec schodzimy na minus po cenie katalogowej ze sztucznej dostawy */
          select dokument from dokumpoz where ref=:pozdokum into :dokument;
          select data from dokumnag where ref=:dokument into :data;
          symbdost = '-'||:mag||'/'||cast(:data as date);
          execute procedure DOKUMNAG_CREATE_DOKMAG_DOSTAWA(:dokument,NULL,:data,:mag,NULL,:pozdokum,:symbdost,NULL) returning_values :ldostawa;
          select cena_zakn from wersje where KTM=:ktm and NRWERSJI=:wersja into :lcenamag;
          if(:lcenamag=0) then select cena_zakn from towary where KTM=:ktm into :lcenamag;
          cena_koszt = :lcenamag;
          jeststcen = 1;
        end
        if(:ilosc > 0 and :jeststcen = 1) then begin
          /* dodanie ilosci ponizej stanow na bazie ostatniego stanu cenowego*/
            numer = null;
            if(:opktryb = 1 and :opk = 1) then
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa and CENASNETTO = :cenasnet into :numer;
            else
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa into :numer;
            if(numer is null) then begin
               select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
               if(:numer is null or (:numer = 0))then numer = 0;
               numer = numer +  1;
               insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO, CENASBRUTTO,blokoblnag,CENA_KOSZT) values (:pozdokum,:numer,:ilosc,:lcenamag,:ldostawa,:CENASNET,:cenasbru,:blokoblnag,:cena_koszt);
            end else begin
                update DOKUMROZ set ILOSC = ILOSC + :ilosc, PCENA = null where pozycja = :pozdokum and numer = :numer;
            end
            ilosc = 0;
        end else if(:ilosc > 0 and :mag_typ = 'E') then begin
          /*zejcie bez stanów - poniżej zera - szukanie na stanach ilociowych ceny do ewidencyjnego */
          lcenamag = null;
          i = null;
          select cena, ilosc from STANYIL where MAGAZYN=:mag and KTM=:ktm AND WERSJA=:WERSJA and ilosc <> 0 into :lcenamag, :i;
          if(:i is null) then i = 0;
          if(:cenamag > 0 and :cenamag <> :lcenamag and i <> 0) then exception DOKUM_ROZ_E_ZLA_CENA;
          if(:lcenamag > 0) then begin
            numer = null;
            if(:opktryb = 1 and :opk = 1) then
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and CENASNETTO = :cenasnet into :numer;
            else
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag into :numer;
            if(numer is null) then begin
               select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
               if(:numer is null or (:numer = 0))then numer = 0;
               numer = numer +  1;
               insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO,CENASBRUTTO,blokoblnag) values (:pozdokum,:numer,:ilosc,:lcenamag,NULL,:cenasnet,:cenasbru,:blokoblnag);
            end else begin
                update DOKUMROZ set ILOSC = ILOSC + :ilosc, PCENA = null where pozycja = :pozdokum and numer = :numer;
            end
            ilosc = 0;
          end
        end
        if(:ilosc > 0) then begin
          /* zejcie poniżej stanów bez stanów zgodnie z danycmi w parametrach */
          if((:cenamag > 0) and(:dostawa > 0 or (:mag_typ <> 'P'))) then begin
            numer = null;
            if(:opktryb = 1 and :opk = 1) then
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:cenamag and (dostawa = :dostawa or (:mag_typ <> 'P')) and CENASNETTO = :cenasnet into :numer;
            else
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:cenamag and (dostawa = :dostawa or (:mag_typ <> 'P'))into :numer;
            if(numer is null) then begin
               select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
               if(:numer is null or (:numer = 0))then numer = 0;
               numer = numer +  1;
               insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,cenasnetto,cenasbrutto,blokoblnag,CENA_KOSZT) values (:pozdokum,:numer,:ilosc,:cenamag,:dostawa,:cenasnet, :cenasbru,:blokoblnag,:cena_koszt);
            end else begin
                update DOKUMROZ set ILOSC = ILOSC + :ilosc, PCENA = null where pozycja = :pozdokum and numer = :numer;
            end
            ilosc = 0;
          end
        end
        if(:ilosc > 0) then
          exception DOKUM_ROZ_BEZ_STC;
      end
--gosp. magaz. wg najnizszych cen
      /*=========== magazyn wielu cen bez fifo - inna kolejnosc sortowania stanow */
      if((:mag_typ = 'C' or (:mag_typ = 'P'))and :fifo='N') then begin
        prevcenamag = -1;
        previldost = 0;
        jeststcen = 0;
        for select CENA,DOSTAWA,ILOSC-ZABLOKOW,CENA_KOSZT
        from STANYCEN where KTM = :ktm and WERSJA = :wersja and MAGAZYN = :mag
        AND ((CENA = :CENAMAG) or (:CENAMAG is null) or (:cenamag = 0) or (:magbreak = 0))
        AND ( DOSTAWA = :DOSTAWA or (:DOSTAWA is null) or (:dostawa = 0) or (:magbreak = 0))
        AND ILOSC>0
        order by cena into :lcenamag, :ldostawa, :i, :cena_koszt
        do begin
          if(:i is null) then i = 0;
          jeststcen = 1;
          /*dodanie ilosci zarezerwowanych prze to zamowienie na tym stanie cenowym */
          if(:ilosc > 0) then begin
            execute procedure DOKUMPOZ_ILDOST(:i, :zamowienie, :mag_typ, :mag, :ktm, :wersja, :lcenamag, :ldostawa, :dostawa, :pozdokum) returning_values :i;
            if(:i > :ilosc) then begin
              j = ilosc; /* zdjecie calej ilosci potrzebnej*/
            end else begin
              j = i;
            end
            /*uzupelnienie istnijacej rozpiski lub dodanie nowej*/
            if(:j > 0) then begin
              numer = null;
              if(:opktryb = 1 and :opk = 1) then
              select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and dostawa = :ldostawa and CENASNETTO = :cenasnet into :numer;
              else
              select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and dostawa = :ldostawa into :numer;
              if(:numer is null) then begin
                 select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
                 if(:numer is null or (:numer = 0))then numer = 0;
                 numer = numer +  1;
                 insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO,CENASBRUTTO,blokoblnag,CENA_KOSZT) values (:pozdokum,:numer,:j,:lcenamag,:ldostawa,:cenasnet,:cenasbru,:blokoblnag,:cena_koszt);
              end else begin
                  update DOKUMROZ set ILOSC = ILOSC + :j, PCENA = null where pozycja = :pozdokum and numer = :numer;
              end
            end
            ilosc = ilosc - j;
          end
        end
        /*------------*/
        if(:magbreak = 1 AND :ilosc > 0) then begin
          for select CENA,DOSTAWA,ILOSC -ZABLOKOW, CENA_KOSZT
          from STANYCEN where KTM = :ktm and WERSJA = :wersja and MAGAZYN = :mag
          and ILOSC>0
          order by cena into :lcenamag, :ldostawa, :i, :cena_koszt
          do begin
            jeststcen = 0;
            if(:ilosc > 0) then begin
              if(:i is null) then i = 0;
              execute procedure DOKUMPOZ_ILDOST(:i, :zamowienie, :mag_typ, :mag, :ktm, :wersja, :lcenamag, :ldostawa, :dostawa, :pozdokum) returning_values :i;
              if(:i > :ilosc) then begin
                 j = ilosc; /* zdjecie calej ilosci potrzebnej*/
              end else begin
                j = i;
              end
              /*uzupelnienie istnijacej rozpiski lub dodanie nowej*/
              if(:j > 0) then begin
                numer = null;
                if(:opktryb = 1 and :opk = 1) then
                select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and dostawa = :ldostawa and cenasnetto = :cenasnet into :numer;
                else
                select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and dostawa = :ldostawa into :numer;
                if(:numer is null) then begin
                   select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
                   if(:numer is null or (:numer = 0))then numer = 0;
                   numer = numer +  1;
                   insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO,CENASBRUTTO,blokoblnag,CENA_KOSZT) values (:pozdokum,:numer,:j,:lcenamag,:ldostawa,:cenasnet, :cenasbru,:blokoblnag,:cena_koszt);
                end else begin
                   update DOKUMROZ set ILOSC = ILOSC + :j, PCENA = null where pozycja = :pozdokum and numer = :numer;
                end
              end
              ilosc = ilosc - j;
            end
          end
        end
        if(:ilosc > 0 AND :minus_stan <> 1) then exception DOKUM_ROZ_NOT_ENOUGHT_SC 'Brak stanów magazynowych dla towaru: '||:ktm|| ' ' || :nazwa_ktm;
        if(:ilosc > 0 and :minus_stan = 1 and :jeststcen = 0) then begin
          /* nie ma w ogole stanu magazynowego wiec schodzimy na minus po cenie katalogowej ze sztucznej dostawy */
          select dokument from dokumpoz where ref=:pozdokum into :dokument;
          select data from dokumnag where ref=:dokument into :data;
          symbdost = '-'||:mag||'/'||cast(:data as date);
          execute procedure DOKUMNAG_CREATE_DOKMAG_DOSTAWA(:dokument,NULL,:data,:mag,NULL,:pozdokum,:symbdost,NULL) returning_values :ldostawa;
          select cena_zakn from wersje where KTM=:ktm and NRWERSJI=:wersja into :lcenamag;
          if(:lcenamag=0) then select cena_zakn from towary where KTM=:ktm into :lcenamag;
          cena_koszt = :lcenamag;
          jeststcen = 1;
        end
        if(:ilosc > 0 and :jeststcen = 1) then begin
          /* dodanie ilosci ponizej stanow na bazie ostatniego stanu cenowego*/
            numer = null;
            if(:opktryb = 1 and :opk = 1) then
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa and CENASNETTO = :cenasnet into :numer;
            else
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa into :numer;
            if(numer is null) then begin
               select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
               if(:numer is null or (:numer = 0))then numer = 0;
               numer = numer +  1;
               insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO,CENASBRUTTO,blokoblnag,CENA_KOSZT) values (:pozdokum,:numer,:ilosc,:lcenamag,:ldostawa,:cenasnet,:cenasbru,:blokoblnag,:cena_koszt);
            end else begin
                update DOKUMROZ set ILOSC = ILOSC + :ilosc, PCENA = null where pozycja = :pozdokum and numer = :numer;
            end
            ilosc = 0;
        end
        if(:ilosc > 0) then begin
          /* zejcie poniżej stanów bez stanów zgodnie z danycmi w parametrach */
          if((:cenamag > 0) and(:dostawa > 0 or (:mag_typ <> 'P'))) then begin
            numer = null;
            if(:opktryb = 1 and :opk = 1) then
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:cenamag and (dostawa = :dostawa or (:mag_typ <> 'P')) and CENASNETTO = :cenasnet into :numer;
            else
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:cenamag and (dostawa = :dostawa or (:mag_typ <> 'P'))into :numer;
            if(numer is null) then begin
               select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
               if(:numer is null or (:numer = 0))then numer = 0;
               numer = numer +  1;
               insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO,CENASBRUTTO,blokoblnag,CENA_KOSZT) values (:pozdokum,:numer,:ilosc,:cenamag,:dostawa,:cenasnet,:cenasbru,:blokoblnag,:cena_koszt);
            end else begin
                update DOKUMROZ set ILOSC = ILOSC + :ilosc, PCENA = null where pozycja = :pozdokum and numer = :numer;
            end
            ilosc = 0;
          end
        end
        if(:ilosc > 0) then
          exception DOKUM_ROZ_BEZ_STC;
      end else if((:mag_typ = 'C' or (:mag_typ = 'P'))and :fifo='W') then begin
-- gosp. magazynowa wg najwyzszych cen
        prevcenamag = -1;
        previldost = 0;
        jeststcen = 0;
        for select CENA,DOSTAWA,ILOSC-ZABLOKOW,CENA_KOSZT
        from STANYCEN where KTM = :ktm and WERSJA = :wersja and MAGAZYN = :mag
        AND ((CENA = :CENAMAG) or (:CENAMAG is null) or (:cenamag = 0) or (:magbreak = 0))
        AND ( DOSTAWA = :DOSTAWA or (:DOSTAWA is null) or (:dostawa = 0) or (:magbreak = 0))
        AND ILOSC>0
        order by cena desc into :lcenamag, :ldostawa, :i, :cena_koszt
        do begin
          jeststcen = 1;
          /*dodanie ilosci zarezerwowanych prze to zamowienie na tym stanie cenowym */
          if(:ilosc > 0) then begin
            if(:i is null) then i = 0;
            execute procedure DOKUMPOZ_ILDOST(:i, :zamowienie, :mag_typ, :mag, :ktm, :wersja, :lcenamag, :ldostawa, :dostawa, :pozdokum) returning_values :i;
            if(:i > :ilosc) then begin
              j = ilosc; /* zdjecie calej ilosci potrzebnej*/
            end else begin
              j = i;
            end
            /*uzupelnienie istnijacej rozpiski lub dodanie nowej*/
            if(:j > 0) then begin
              numer = null;
              if(:opktryb = 1 and :opk = 1) then
              select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and dostawa = :ldostawa and CENASNETTO = :cenasnet into :numer;
              else
              select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and dostawa = :ldostawa into :numer;
              if(:numer is null) then begin
                 select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
                 if(:numer is null or (:numer = 0))then numer = 0;
                 numer = numer +  1;
                 insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO,CENASBRUTTO,blokoblnag,CENA_KOSZT) values (:pozdokum,:numer,:j,:lcenamag,:ldostawa,:cenasnet,:cenasbru,:blokoblnag,:cena_koszt);
              end else begin
                  update DOKUMROZ set ILOSC = ILOSC + :j, PCENA = null where pozycja = :pozdokum and numer = :numer;
              end
            end
            ilosc = ilosc - j;
          end
        end
        /*------------*/
        if(:magbreak = 1 AND :ilosc > 0) then begin
          for select CENA,DOSTAWA,ILOSC -ZABLOKOW, CENA_KOSZT
          from STANYCEN where KTM = :ktm and WERSJA = :wersja and MAGAZYN = :mag
          and ILOSC>0
          order by cena desc into :lcenamag, :ldostawa, :i, :cena_koszt
          do begin
            jeststcen = 0;
            if(:ilosc > 0) then begin
              if(:i is null) then i = 0;
              execute procedure DOKUMPOZ_ILDOST(:i, :zamowienie, :mag_typ, :mag, :ktm, :wersja, :lcenamag, :ldostawa, :dostawa, :pozdokum) returning_values :i;
              if(:i > :ilosc) then begin
                 j = ilosc; /* zdjecie calej ilosci potrzebnej*/
              end else begin
                j = i;
              end
              /*uzupelnienie istnijacej rozpiski lub dodanie nowej*/
              if(:j > 0) then begin
                numer = null;
                if(:opktryb = 1 and :opk = 1) then
                select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and dostawa = :ldostawa and cenasnetto = :cenasnet into :numer;
                else
                select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and dostawa = :ldostawa into :numer;
                if(:numer is null) then begin
                   select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
                   if(:numer is null or (:numer = 0))then numer = 0;
                   numer = numer +  1;
                   insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO,CENASBRUTTO,blokoblnag,CENA_KOSZT) values (:pozdokum,:numer,:j,:lcenamag,:ldostawa,:cenasnet, :cenasbru,:blokoblnag,:cena_koszt);
                end else begin
                   update DOKUMROZ set ILOSC = ILOSC + :j, PCENA = null where pozycja = :pozdokum and numer = :numer;
                end
              end
              ilosc = ilosc - j;
            end
          end
        end
        if(:ilosc > 0 AND :minus_stan <> 1) then exception DOKUM_ROZ_NOT_ENOUGHT_SC 'Brak stanów magazynowych dla towaru: '||:ktm|| ' ' || :nazwa_ktm;
        if(:ilosc > 0 and :minus_stan = 1 and :jeststcen = 0) then begin
          /* nie ma w ogole stanu magazynowego wiec schodzimy na minus po cenie katalogowej ze sztucznej dostawy */
          select dokument from dokumpoz where ref=:pozdokum into :dokument;
          select data from dokumnag where ref=:dokument into :data;
          symbdost = '-'||:mag||'/'||cast(:data as date);
          execute procedure DOKUMNAG_CREATE_DOKMAG_DOSTAWA(:dokument,NULL,:data,:mag,NULL,:pozdokum,:symbdost,NULL) returning_values :ldostawa;
          select cena_zakn from wersje where KTM=:ktm and NRWERSJI=:wersja into :lcenamag;
          if(:lcenamag=0) then select cena_zakn from towary where KTM=:ktm into :lcenamag;
          cena_koszt = :lcenamag;
          jeststcen = 1;
        end
        if(:ilosc > 0 and :jeststcen = 1) then begin
          /* dodanie ilosci ponizej stanow na bazie ostatniego stanu cenowego*/
            numer = null;
            if(:opktryb = 1 and :opk = 1) then
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa and CENASNETTO = :cenasnet into :numer;
            else
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:lcenamag and DOSTAWA = :ldostawa into :numer;
            if(numer is null) then begin
               select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
               if(:numer is null or (:numer = 0))then numer = 0;
               numer = numer +  1;
               insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO,CENASBRUTTO,blokoblnag,CENA_KOSZT) values (:pozdokum,:numer,:ilosc,:lcenamag,:ldostawa,:cenasnet,:cenasbru,:blokoblnag,:cena_koszt);
            end else begin
                update DOKUMROZ set ILOSC = ILOSC + :ilosc, PCENA = null where pozycja = :pozdokum and numer = :numer;
            end
            ilosc = 0;
        end
        if(:ilosc > 0) then begin
          /* zejcie poniżej stanów bez stanów zgodnie z danycmi w parametrach */
          if((:cenamag > 0) and(:dostawa > 0 or (:mag_typ <> 'P'))) then begin
            numer = null;
            if(:opktryb = 1 and :opk = 1) then
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:cenamag and (dostawa = :dostawa or (:mag_typ <> 'P')) and CENASNETTO = :cenasnet into :numer;
            else
            select numer from DOKUMROZ where POZYCJA=:pozdokum and cena=:cenamag and (dostawa = :dostawa or (:mag_typ <> 'P'))into :numer;
            if(numer is null) then begin
               select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
               if(:numer is null or (:numer = 0))then numer = 0;
               numer = numer +  1;
               insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,CENASNETTO,CENASBRUTTO,blokoblnag,CENA_KOSZT) values (:pozdokum,:numer,:ilosc,:cenamag,:dostawa,:cenasnet,:cenasbru,:blokoblnag,:cena_koszt);
            end else begin
                update DOKUMROZ set ILOSC = ILOSC + :ilosc, PCENA = null where pozycja = :pozdokum and numer = :numer;
            end
            ilosc = 0;
          end
        end
        if(:ilosc > 0) then
          exception DOKUM_ROZ_BEZ_STC;
      end
      end
      /*po stanach opakowan*/
      if(:blokpoznalicz = 0) then
        execute procedure DOKPOZ_OBL_FROMROZ(:pozdokum);
      /*jesli to korekta ilosciowa, to poprawienie rozpisek na pozycji korygowanej*/
/*      if(:ilosc < :org_ilosc and :kortopoz > 0) then begin
           org_ilosc = :org_ilosc - :ilosc;
           for select REF, ILOSC, ILOSCL
           from DOKUMROZ where POZYCJA = :kortopoz
           order by NUMER
           into :rozref, :korilosc, :koriloscl
           do begin
            j = :koriloscl;
            if(:j > 0) then begin
              if(:j > :org_ilosc) then j = :org_ilosc;
              if(:j > 0) then
                update DOKUMROZ set ILOSCL = ILOSCL - :j, PCENA = null where ref = :rozref;
              org_ilosc = :org_ilosc - :j;
            end
           end
      end*/ --MS wylaczam w ramach BS32921
end^
SET TERM ; ^
