--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MSGORDS_SET_STATUS(
      MSGORDSREF integer,
      STATUS smallint)
   as
begin
  update msgords set status = :status where ref = :msgordsref;
end^
SET TERM ; ^
