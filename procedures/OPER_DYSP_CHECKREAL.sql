--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_DYSP_CHECKREAL(
      ZAM integer,
      OPERZAM varchar(3) CHARACTER SET UTF8                           )
  returns (
      DYSPPART integer,
      OPERDORE integer,
      OPERDODP integer,
      OPERDODB integer,
      OPERDODK integer,
      OPERDODD integer)
   as
declare variable wydania integer;
declare variable kilosc numeric(14,4);
declare variable kilreal numeric(14,4);
declare variable kilzreal numeric(14,4);
declare variable dore integer;
declare variable dodp integer;
declare variable dodb integer;
declare variable dodk integer;
declare variable dodd integer;
declare variable pozref integer;
declare variable ilreal numeric(14,4);
declare variable ildysp numeric(14,4);
declare variable ilzdysp numeric(14,4);
declare variable kktm varchar(80);
declare variable prschedzam integer;
begin
  dysppart = 0;
  operdore = 0;
  operdodp = 0;
  operdodb = 0;
  operdodk = 0;
  operdodd = 0;
  select TYPZAM.wydania, KILOSC, KILREAL, KILZREAL, kktm
  from TYPZAM join NAGZAM on (NAGZAM.typzam = typzam.symbol) where NAGZAM.REF = :zam
  into :wydania, :kilosc, :kilreal, :kilzreal, :kktm;
  select REALIZACJA, DYSPP, DYSPB, DYSPK, DYSPD from DEFOPERZAM
  where SYMBOL = :operzam into :dore, :dodp, :dodb, :dodk, :dodd;
  /*sprawdzenie, czy dyspozycja jest czesciowa*/
  if(:wydania in (2,3) and :dore>0) then begin
    if(kktm is not null and kktm <> '') then begin
      if(:kilosc > :kilreal + :kilzreal ) then dysppart = 1;
    end else if(exists(select ref
                     from pozzam
                     where zamowienie = :zam and (ILREAL + ILZREAL < ILOSC)))
    then begin
      dysppart = 1;
    end
    --KN wracam do poporzedniej wersji, nie wiem co to jest i kto to zrobil -> brak wpisu w Taskmanie, w operazam dalej jest wywolanie z 2 parametrami wejsciowymi
    /*if(kktm is not null and kktm <> '') then begin
      if(:kilosc > :kilreal + :kilzreal) then begin
        if(fromdokumtyp <> 'P') then dysppart = 1;
        else begin
          select prschedzam from prschedguides where ref = :fromdokum into :prschedzam;
          if(exists (select ref from prschedguides where prschedzam = :prschedzam and ref <> :fromdokum and status = 0)) then
            dysppart = 1;
        end
      end
    end else if(exists(select ref
                     from pozzam
                     where zamowienie = :zam and (ILREAL + ILZREAL < ILOSC)))
    then begin
      dysppart = 1;
    end*/
  end else begin
    if((:dore > 0) and ((:dodp + :dodb + :dodk + :dodd) = 0 )) then
      /*tylko operacja realizacji*/
      select count(*) from POZZAM
      join TOWARY on (TOWARY.KTM=POZZAM.KTM)
      where ZAMOWIENIE = :zam and
        (ILREAL + ILZREAL < ILOSC)
      into :dysppart;
    else
      /*sa wtkonywane operacje dyspozycji*/
      select count(*) from POZZAM
      join TOWARY on (TOWARY.KTM=POZZAM.KTM)
      where ZAMOWIENIE = :zam and
      (ILREAL + ILZREAL + ILDYSP + ILZDYSP < ILOSC)
      and not (/*dla zlecen tylko kompletacji sa brane pod uwage tylko komplety*/
               (:dodk > 0) and
               ((:dodp + :dodb + :dore + :dodd) = 0) and
               (TOWARY.usluga not in (select numer from towtypes where isproduct = 1))
              )
      into :dysppart;
    if(:dysppart > 0) then
      dysppart = 1;
  end
  /*sprawdzenie dla poszczególnych mozliwych typów realizacji, czy bdzie wystpować*/
  if(:wydania in (2,3) and KILREAL > 0 and :dore > 0) then operdore = 1;
  else begin
    for select REF, ILREAL, ILDYSP, ILZDYSP
    from POZZAM where ZAMOWIENIE = :zam
    order by NUMER
    into :pozref, :ilreal, :ildysp, :ilzdysp
    do begin
      if(:dore = 1 and :ilreal > 0) then operdore = 1;
      if(:dodp = 1 and :ildysp > 0 and :operdodp = 0) then begin
        select count(*) from POZZAMDYSP where POZZAM = :pozref and TYP = 1 into :operdodp;
        if(:operdodp > 0) then operdodp = 1;
        else operdodp = 0;
      end
      if(:dodb = 1 and :ildysp > 0 and :operdodb = 0) then begin
        select count(*) from POZZAMDYSP where POZZAM = :pozref and TYP = 2 into :operdodb;
        if(:operdodb > 0) then operdodb = 1;
        else operdodb = 0;
      end
      if(:dodk = 1 and (:ildysp > 0 or :ilzdysp > 0) and :operdodk = 0) then begin
        select count(*) from POZZAMDYSP where POZZAM = :pozref and TYP = 3 into :operdodk;
        if(:operdodk = 0) then begin
          select count(*) from NAGZAM where KPOPREF = :pozref into :operdodk;
          if(:operdodk > 0) then operdodk = 1;
          else operdodk = 0;
        end
      end
      if(:dodd = 1 and :ildysp > 0 and :operdodd = 0) then begin
        select count(*) from POZZAMDYSP where POZZAM = :pozref and TYP = 4 into :operdodd;
        if(:operdodd > 0) then operdodd = 1;
        else operdodd = 0;
      end
    end
  end
  if (:operdore = 0 and :operdodp = 0 and :operdodb = 0 and :operdodk = 0 and :operdodd = 0) then
    exception universal :operzam||'Suma realizowanych ilości musi być większa od 0';
  update POZZAM set AUTOPOZZAMDYSP = '' where ZAMOWIENIE = :zam and AUTOPOZZAMDYSP <> '';
end^
SET TERM ; ^
