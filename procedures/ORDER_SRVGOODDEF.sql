--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ORDER_SRVGOODDEF(
      CONTRTYPE integer,
      OLDN integer,
      NEWN integer)
   as
declare variable symbol VARCHAR(60);
  declare variable number integer;
begin
  --firts we find minor number
  if (oldn < newn) then
    newn = oldn;

  number = :newn;
  for
    select srvgooddef.symbol
      from SRVGOODDEF
      where srvgooddef.contrtype = :contrtype and srvgooddef.number >= :number
      order by srvgooddef.number, srvgooddef.ord
      into :symbol
  do begin
    update SRVGOODDEF set srvgooddef.number = :number, srvgooddef.ord = 2
      where srvgooddef.contrtype = :contrtype and srvgooddef.symbol = :symbol;
    number = :number + 1;
  end

  update SRVGOODDEF set ord = 1
    where srvgooddef.contrtype = :contrtype and srvgooddef.number >= :newn;
end^
SET TERM ; ^
