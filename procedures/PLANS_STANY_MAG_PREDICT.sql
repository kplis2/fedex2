--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PLANS_STANY_MAG_PREDICT(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      KTM varchar(80) CHARACTER SET UTF8                           ,
      WERSJA integer,
      DATA timestamp)
  returns (
      STAN numeric(14,4))
   as
declare variable koryg numeric(14,4);
begin
  stan = 0;
--stan bieżacy
  if (:magazyn is not null and :magazyn <> '') then begin
    select sum(s.ilosc - s.zablokow) from stanyil s
     where s.ktm = :ktm and s.wersja = wersja and s.magazyn = :MAGAZYN
     into :stan;
  end else begin
    select sum(s.ilosc - s.zablokow) from stanyil s where s.ktm = :ktm and s.wersja = wersja
    into :stan;
  end
  if(:data <= current_date) then begin
    suspend;
    exit;
  end
-- + plany produkcji
/*  select sum(pv.planval)
         from plans p
         join planscol pc on (p.ref = pc.plans)
         join plansrow pr on (p.ref = pr.plans and pr.key1 = :ktmmain)
         join plansval pv on (p.ref = pv.plans and pv.planscol = pc.ref and pv.plansrow = pr.ref)
         where p.status = 1 and pc.akt=1 and p.planstype like('%PRPLAN%') and
         (pc.bdata >= :oddaty and pc.bdata < :dodaty or pc.ldata > :oddaty and pc.ldata <= :dodaty)
         into :planval, :bdata, :ldata
        do begin
          termin = :ldata - :bdata;
          if(:oddaty>:bdata) then termin = :termin - (:oddaty-:bdata);
          if(:dodaty<:ldata) then termin = :termin - (:ldata-dodaty);
          planval = :planval * termin / (:ldata - :bdata);
          if(:planval < 0) then planval = 0;
          val = :val + :planval;
          if(floor(:val)<val) then val = floor(:val) + 1;
          planval = 0;
        end
*/
--  select sum()


--  select sum()






  suspend;
end^
SET TERM ; ^
