--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_DOK_BRAKTOWARU_KOREKTA(
      REFDOK DOKUMNAG_ID,
      REFPOZ DOKUMPOZ_ID,
      TRYB SMALLINT_ID,
      DATA DATE_ID)
  returns (
      CNT INTEGER_ID,
      NEWDOK DOKUMNAG_ID)
   as
declare variable dok dokumnag_id;
declare variable poz dokumpoz_id;
declare variable STATUS SMALLINT_ID;
declare variable NEWDOKSYM SYMBOL_ID;
declare variable ktm ktm_id;
declare variable wersjaref wersje_id;
declare variable iloscl ilosci_mag;
declare variable brak smallint_id;
declare variable iloscbrak ilosci_mag;
declare variable iloscinsert ilosci_mag;
declare variable doktyp defdokum_id;
begin
  --[PM] korekta WZ-ki z poziomu aplikacji
  --tryb: 1 - korygujemy o ilosc brakujaca, 2 - zerujemy pozycje jezeli czegokolwiek brakuje, 3 - zerujemy caly dokument (anulowany)
  --troszke na podstawie MWS_DOK_BRATOWARU_OZNACZ

  cnt = 0;

  if (tryb is null) then
    tryb = 0;
  if (data is null) then
    data = current_date;
  if (tryb not in (1,2,3)) then
    exception universal 'Nieprawidlowy tryb.';
  --if (tryb = 3 and refpoz is not null) then
    --exception universal 'Dla trybu 3 nie podajemy pozycji.';
  if (refpoz = 0) then
    refpoz = null;
  if (refdok = 0) then
    refdok = null;
  if (refdok is null and refpoz is null) then
    exception universal 'Wypadaloby podac, co sie koryguje.';
  if (refpoz is not null and refdok is not null) then
    refdok = null;

  if (refdok is not null) then
    dok = refdok;
  else
    select dokument from dokumpoz where ref = :refpoz into :dok;

  select typ from dokumnag where ref = :dok into :doktyp;
  if (doktyp <> 'WZ') then
    exception universal 'Nie korygujemy tego typu. Jesli musisz to zrobic skontaktuj sie z Sente.';

  execute procedure MAG_CREATE_DOK_KORYG (:dok, :data, 0)
    returning_values(:status, :newdok, :newdoksym);


  for select ref, dp.ktm, dp.wersjaref, dp.iloscl, coalesce(dp.braktowaru,0),
          coalesce(dp.braktowaru_ilosc,0)
        from dokumpoz dp
        where dp.dokument = :dok and (:refpoz is null or dp.ref = :refpoz)
          and dp.iloscl > 0
        into :poz, :ktm, :wersjaref, :iloscl, :brak, :iloscbrak
    do begin
      iloscinsert = 0;
      if (tryb = 1 and brak = 1) then
        begin
          iloscinsert = :iloscbrak;
        end
      if (tryb = 2 and brak = 1) then
        begin
          iloscinsert = iloscl;
        end
      if (tryb = 3) then
        begin
          iloscinsert = iloscl;
        end 
      if (iloscinsert > 0) then
        begin
          insert into dokumpoz(dokument, ktm, wersjaref, ilosc, kortopoz)
            values(:newdok, :ktm, :wersjaref, :iloscinsert, :poz);
          cnt = cnt + 1;
        end
    end

  if (cnt = 0) then
    begin
      delete from dokumnag where ref = :newdok;
      newdok = null;
    end
  else
    update dokumnag set akcept = 1 where ref = :newdok;

  suspend;
end^
SET TERM ; ^
