--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_CALCULATE_VACLIMIT(
      EMPLOYEE integer,
      VYEAR integer)
  returns (
      VLIMITM integer,
      ADDVLIMITM integer,
      DISABLELIMITM integer,
      LIMITCONSM integer,
      ADDVDATE date,
      FIRSTJOB smallint,
      DISVFROMDATE date,
      DISVTODATE date)
   as
declare variable ENDDATE date;
declare variable FORDATE date;
declare variable T_FORDATE date;
declare variable FROMDATE date;
declare variable TODATE date;
declare variable M smallint;
declare variable DIMNUM smallint;
declare variable DIMDEN smallint;
declare variable FIRSTDAY date;
declare variable PREVWORK smallint;
declare variable T_DATE1 date;
declare variable T_DATE2 date;
declare variable VACLIMIT integer;
declare variable VACLIMITM integer;
declare variable END_VACLIMIT integer;
declare variable VAC_DIM_MINUTES integer;
declare variable T_LIMIT integer;
declare variable CLIMITH integer;
declare variable CLIMITM integer;
declare variable BREAK_MONTHS smallint;
declare variable BREAK_DAYS smallint;
declare variable USEDVAC integer;
declare variable USEDVAC_PART integer;
declare variable LAST_TODATE date;
declare variable T_MONTH smallint;
declare variable VACDAY smallint;
declare variable ALLYEAR smallint;
declare variable ENDDATE_FIRSTJOB date;
declare variable ONDAY date;
declare variable ROUND_FIRSTJOB smallint;
declare variable LOW_LIMIT smallint;
declare variable HIGH_LIMIT smallint;
declare variable ADDLIMIT_NOM integer;
declare variable PREVLIMIT integer;
declare variable PREV_CALENDAR smallint;
declare variable VYEAR_START date;
declare variable VYEAR_END date;
declare variable BIRTHDATE date;
declare variable AGE integer;
declare variable SENIORITY_YOUNG date;
declare variable ADDVSTARTMONTH smallint;
declare variable ADDVENDMONTH smallint;
declare variable ADDVMONTHS smallint;
declare variable TEMPDATE date;
declare variable EFROMDATE date;
declare variable ETODATE date;
declare variable VC integer;
declare variable VC2 integer;
declare variable USEDVAC_FROMYEARSTART integer;
declare variable T_USEDVAC integer;
declare variable BREAKECOL_LIST varchar(100);
declare variable ADDVDATE_CUST smallint;
declare variable DISABLEYEARSTART integer;
declare variable CTXTODATESTR varchar(20);
declare variable CTXTODATE timestamp;
begin
  execute procedure get_config('ACTLIMIT_FOR_ALLYEAR', 2) returning_values allyear;
  execute procedure get_config('VACCOLUMN', 2) returning_values :vc;
  execute procedure get_config('VACREQCOLUMN', 2) returning_values :vc2;

  ctxtodatestr = rdb$get_context('USER_TRANSACTION', 'RPT_WORKREFERENCE_TODATE');  --PR54992
  if (ctxtodatestr <> '') then ctxtodate = cast(ctxtodatestr as timestamp);

  breakecol_list = ';10;300;330;';  --lista rodzajow nieobecnosci, ktore pomniejszaja wymiar urlopu
  if (vyear < 2013) then  breakecol_list = breakecol_list || '260;';  --PR50484

  fordate = vyear || '/1/1';
  vyear_start = vyear || '/1/1';
  vyear_end = vyear || '/12/31';
  firstjob = 0;
  vac_dim_minutes = 8 * 60; /*nawet dla niepelnosprawnych pomimo dobowej normy czasu pracy
                              obnizonej do 7h  stosujemy dzienny wymiar urlopu 8h (art 154bis par3) */
  if (allyear = 1) then
    onday = vyear_end;
  else if (vyear = extract(year from current_date)) then
    onday = current_date;
  else if (vyear > extract(year from current_date)) then
    onday = vyear_start;  --liczymy karty na nastepne lata (01.01)
  else
    onday = vyear_end; --liczymy karty na zeszle lata (31.12)

--na wypadek jakby wiek nie byl uzupelniony w kartotece, zakladam ze jest pelnoletni (wiecej niz 18)
  select p.birthdate, 30, e.fromdate, coalesce(:ctxtodate, e.todate) from persons p  --PR54992
    join employees e on (p.ref = e.person and e.ref = :employee)
    into :birthdate, :age, :efromdate, :etodate;

  if (birthdate is not null) then
    select y from get_ymd(:birthdate, :onday) into :age;

--pobieramy informacje o ustawieniach indywidualnych dla url. uzupelniajacego
  select addvdate, addvdate_cust from evaclimits
    where employee = :employee and vyear = :vyear
    into :addvdate, :addvdate_cust;

  addvdate_cust = coalesce(addvdate_cust,0);

--wyliczamy dzien kiedy pracownik nabywa prawo do 26 dni urlopu, jeżeli uzytkownik sam go nie ustawil
  if (addvdate_cust = 0) then
  begin
    select seniority_date
      from empl_vac_seniority(:employee, :fordate)
      into :addvdate;

  --nie wykazujemy daty uzupelnienia wyznaczonej na dany rok przed data zatrudnienia
    if (addvdate <= efromdate) then
      addvdate = vyear_start - 1;
  end

  if (age > 18) then
  begin
    select first 1 coalesce(limit,0)
      from evaclimdefs
      where fromdate <= :fordate
      order by fromdate desc, worktime asc
      into :low_limit;
  
    select first 1 coalesce(limit,0)
      from evaclimdefs
      where fromdate <= :fordate
      order by fromdate desc, worktime desc
      into :high_limit;
  
    vaclimit = iif(addvdate < vyear_start, high_limit, low_limit);
    end_vaclimit = iif(addvdate > vyear_end, low_limit, high_limit);

    if (extract(year from efromdate) = vyear) then
    begin
      if (not exists(select first 1 1 from eworkcertifs
                       where employee = :employee
                         and extract(year from fromdate) < :vyear
                         and seniorityflags like '%;STU;%')
      ) then
        firstjob = 1;
    end
  end else
  begin
  /*gdy mamy do czynienia z mlodocianym pierwszy urlop przysluguje po 6 miesiacach
    pracy; addvdate wskazuje kiedy ma 10 lat stazu*/
    vaclimit = 0;
    execute procedure date_add(addvdate,-9,-6,0) returning_values seniority_young;
  /*jesli w danym roku kalendarzowym mlodociany konczy 18 lat i uzyskal prawo do
    urlopu to wymiar urlopu obnizamy mu do 20 dni rocznie*/
    if (age = 18) then
    begin
      vaclimit = 20;
      execute procedure date_add(birthdate,18,0,0) returning_values birthdate;
      select seniority_date
        from empl_vac_seniority(:employee, :birthdate)
        into :seniority_young;
      execute procedure date_add(seniority_young,-9,-6,0) returning_values seniority_young;
      --jak nie uzyskal prawa do urlopu to chyba od zera liczymy pierwsza prace
      if (seniority_young > birthdate) then
        firstjob = 1;
    end else
    begin
      if (seniority_young >= vyear_start and seniority_young <= onday) then
        vaclimit = 12;
    --po roku pracy przysluguje 26 dni, ktore sie sumuje do wczesniejszych 12
      execute procedure date_add(seniority_young,0,6,0) returning_values seniority_young;
      if (seniority_young >= vyear_start and seniority_young <= onday) then
        vaclimit = vaclimit + 14; -- 14 = 26 - 12
    end
    vaclimitm = vaclimit * vac_dim_minutes;
    vlimitm = vaclimitm;
    addvlimitm = 0;
    disablelimitm = 0;
  --aby nie naliczylo uzupelnienia
    end_vaclimit = vaclimit;
  end
  --jesli jest mlodociany i uzyskal prawo do urlopu nie dzielimy go proporcjonalnie

  if (age >= 18 or firstjob = 1) then   --?? przed BS45844 bylo >18, nie mam jednak pewnosci co do zasadnosci zmiany
  begin
  /*w danym roku nie powinny wystapic 2 rekordy, bo jezeli osoba straci prawo
    do urlopu, to pozniej musi czekac min rok na przywrocenie */
    select first 1 fromdate, todate
      from get_disable_periods(:employee,0)
      where fromdate <= :vyear_end
        and (todate >= :vyear_start or todate is null)
      into :disvfromdate, :disvtodate;
      --exception universal '' ||employee||' '|| disvfromdate;

    select sum(l.hourlimit)
      from eworkcertifs c
        join ewrkcrtlimits l on (l.workcertif = c.ref)
      where c.employee = :employee
        and c.seniorityflags like '%;STU;%'
        and extract(year from c.todate) = :vyear
        and l.ecolumn = :vc
      into :climith;
  
    climitm = 60 * coalesce(climith, 0);
  
    if (end_vaclimit <> vaclimit) then
    begin
      tempdate = etodate;
      if (tempdate > vyear_end or tempdate is null) then tempdate = vyear_end;
      addvendmonth = extract(month from tempdate);
  
      tempdate = efromdate;
      if (tempdate < vyear_start) then tempdate = vyear_start;
      addvstartmonth = extract(month from tempdate);
      firstday = tempdate - extract(day from tempdate) + 1;

    /*jezeli w miesiacu zatrudnienia pracownik odbywal prace u innego pracodawcy,
      to ten miesac kwalifikowany jest na rzecz tego pracodawcy*/
      prevwork = 0;
      if (exists(select first 1 1 from eworkcertifs
                    where employee = :employee
                      and todate between :firstday and :tempdate
                      and seniorityflags like '%;STU;%')
      ) then
        prevwork = 1;

      addvmonths = addvendmonth - addvstartmonth + 1 - prevwork;
    end
  
  --okreslenie urlopu zaleglego
    select restlimit from evaclimits
      where employee = :employee and vyear = :vyear - 1
      into :prevlimit;
    prevlimit = coalesce(prevlimit,0);
  
    vlimitm = 0;
    addvlimitm = 0;
    disablelimitm = 0;
    prev_calendar = 0;
    usedvac_fromyearstart = 0;

  /*DS: na wymiar urlopu maja wplyw zmiany etatu zgodnie z art 154 bis par 3; dobowa
    norma czasu pracy nie wplywa na stosunek dni do godzin urlopu, czyli nawet jak jest
    obnizenie np za niepelnosprawnosc) za 1 dzien przyjmujemy 8 godzin (dla calego etatu) */
    for
      select fromdate, todate, dimnum, dimden, case when :vyear < 2012 then :vac_dim_minutes else dayworkdim/60 end
        from e_get_employmentchanges(:employee,:vyear_start,:vyear_end,';WORKDIM;'||iif(:vyear<2012,'','DAYWORKDIM;'))
        where todate >= fromdate  --warunek do usuniecia po realizacji BS53146
        into :fromdate, :todate, :dimnum, :dimden, :vac_dim_minutes
    do begin
      if (addvdate < fromdate) then
        vaclimitm = end_vaclimit * vac_dim_minutes;
      else
        vaclimitm = vaclimit * vac_dim_minutes;
      addlimit_nom = (end_vaclimit - vaclimit) * vac_dim_minutes;
  
      if (firstjob = 1) then
      begin
        fordate = fromdate - 1;  -- przydzielamy limit ostatniego dnia przepracowanego miesiaca
        t_month = extract(month from fordate);
        vacday = extract(day from fordate);
        if (t_month = 12) then t_month = 0;
        if (extract(month from fromdate) < 12 or extract(month from fordate) < 12) then --obsluga zatrudnienia 1 grudnia
        begin
          t_month = t_month + 1;
          if (vacday < 29) then
          begin
            fordate = extract(year from fromdate) || '/' || t_month || '/' || vacday;
          end else
          begin
            --tu jest trudniejszy przypadek, bo bo moze byc np. 30 luty, 31 czerwiec itp. - inaczej mowiac ostatni dzien miesiaca
            t_fordate = cast(extract(year from fromdate) || '/' || t_month || '/28' as date) + 5; -- na pewno wejde w kolejny miesiac
            --potrzebny ostatni dzien miesiaca
            t_fordate = cast(extract(year from t_fordate) || '/' || extract(month from t_fordate) || '/1' as date) - 1;
            if (vacday < extract(day from t_fordate))
            then
              fordate = extract(year from fromdate) || '/' || t_month || '/' || vacday;
            else
              fordate = t_fordate;
          end
  
          if (allyear = 1) then
            enddate_firstjob = extract(year from current_date)||'-12-31';
          else
            enddate_firstjob = current_date;

          if (todate is null or todate > enddate_firstjob) then
            enddate = enddate_firstjob;
          else
            enddate = todate;

          m = 0;
          while (fordate <= enddate and extract(year from fordate) = vyear) do
          begin
            m = m + 1;
            t_month = t_month + 1;
            if (extract(month from fordate) < 12) then
            begin
              if (vacday < 29) then
              begin
                fordate = extract(year from fromdate) || '/' || t_month || '/' || vacday;
              end else
              begin
              --tu jest trudniejszy przypadek, bo bo moze byc np. 30 luty, 31 czerwiec itp. - inaczej mowiac ostatni dzien miesiaca
                t_fordate = cast(extract(year from fromdate) || '/' || t_month || '/28' as date) + 5; -- na pewno wejde w kolejny miesiac
              --potrzebny ostatni dzien miesiaca
                t_fordate = cast(extract(year from t_fordate) || '/' || extract(month from t_fordate) || '/1' as date) - 1;
                if (vacday < extract(day from t_fordate)) then
                  fordate = extract(year from fromdate) || '/' || t_month || '/' || vacday;
                else
                  fordate = t_fordate;
              end
            end else
              break;
          end
          t_limit = 1.0000 * vaclimitm/12 * m * dimnum / dimden + 0.4999;
          if (vlimitm + t_limit > vaclimitm) then
            t_limit = vaclimitm - vlimitm;
          if (t_limit < 0 ) then
            t_limit = 0;
  
          vlimitm = vlimitm + t_limit;
  
          --urlop uzupelniajacy
          if (fromdate <= addvdate and (addvdate <= todate or todate is null)) then
          begin
            if (m < 12) then
              t_limit = 1.0000 * addvendmonth/12 * addlimit_nom/addvmonths * m * dimnum / dimden + 0.4999;
            else
              t_limit = 1.0000 * addvendmonth/12 * addlimit_nom * dimnum / dimden + 0.4999;
        
            if (t_limit > addlimit_nom) then
              t_limit = addlimit_nom - addvlimitm;
            if (t_limit < 0) then
              t_limit = 0;

            addvlimitm = addvlimitm + t_limit;
          end
        end
      end else
      begin
      --praca nie jest pierwsza
        if (fromdate < vyear_start) then
          fromdate = vyear_start;
        if (todate is null or todate > vyear_end) then
          todate = vyear_end;
  
        prevwork = 0;
        if (extract(day from fromdate) > 1) then
        begin
        /*szukamy czy za ten miesiac urlop ma byc z tego limitu, czy już jest w poprzednim
          poprzedniego pracodawcy, lub u nas z poprzedniego rekordu zatrudnienia */

          if (prev_calendar = 1) then
            prevwork = 1;
          else begin
            firstday = fromdate - extract(day from fromdate) + 1;
            if (exists(select first 1 1 from eworkcertifs
                         where employee = :employee
                           and todate between :firstday and :fromdate
                           and seniorityflags like '%;STU;%')
            ) then
              prevwork = 1;
           end
        end
  
      --jesli dany pracownik przebywal na urlopie wychowawczym itp
        if (exists(select first 1 1 from eabsences a
                      join ecolumns c on (c.number = a.ecolumn)
                    where c.cflags containing ';OKN;'
                      and a.correction in (0,2)
                      and a.employee = :employee
                      and a.fromdate <= :todate
                      and a.todate >= :fromdate)
        ) then begin

          break_months = 0;
          break_days = 0;
          usedvac = 0;
          last_todate = vyear_start - 1;

        /*za pelne miesiace nieskladkowe potraca sie 1/12 urlopu,
          za niepelne sumuje sie dni do 30 i jak sie uzbiera to o kolejne 1/12 */
          for
            select distinct afromdate, atodate
               from eabsences
               left join e_get_whole_eabsperiod(ref, :fromdate, :todate, null, null, null, :breakecol_list) on (1 = 1)
              where :breakecol_list containing ';' || ecolumn || ';'
                and correction in (0,2)
                and employee = :employee
                and fromdate <= :todate
                and todate >= :fromdate
              order by afromdate
              into :t_date1, :t_date2
          do begin

            select y*12 + m, d from get_ymd(:t_date1, :t_date2, 1, :break_days + :break_months * 30) --BS56218
              into :break_months, :break_days;

            usedvac_part = null;
            select sum(worksecs) / 60 from eabsences
              where ecolumn in (:vc,:vc2)
                and correction in (0,2)
                and employee = :employee
                and ayear = :vyear
                and fromdate > :last_todate
                and todate < :t_date2
              into :usedvac_part;
            usedvac = usedvac + coalesce(usedvac_part,0);
            last_todate = t_date2;
          end
          usedvac_fromyearstart = usedvac_fromyearstart + usedvac;

          --urlop wykorzystany mogl zostac rozpatrzony we wczesniejszym przebiegu petli
          if (usedvac > 0) then
          begin
            t_usedvac = usedvac_fromyearstart - prevlimit;
            if (t_usedvac < 0) then
              usedvac = 0;
            else if (t_usedvac < usedvac) then
              usedvac = t_usedvac;
          end

        /*teraz trzeba policzyc urlop i zobaczyc czy czasem nie zostal wczesniej
          wykorzystany, bo jesli zostal wykorzystany w calosci to nie potraca sie */
          m = extract(month from todate) - extract(month from fromdate) + 1 - prevwork - coalesce(break_months,0);
          if (m < 12) then
            t_limit = 1.0000 * vaclimitm/12 * m * dimnum / dimden + 0.4999;
          else
            t_limit = 1.0000 * vaclimitm * dimnum / dimden + 0.4999;

          if (vlimitm + t_limit < usedvac) then
            t_limit = usedvac - vlimitm;
          if (vlimitm + t_limit > vaclimitm - climitm) then
            t_limit = vaclimitm - vlimitm - climitm;
          if (t_limit < 0 ) then
            t_limit = 0;

          vlimitm = vlimitm + t_limit;

          --urlop uzupelniajacy
          if (fromdate <= addvdate and (addvdate <= todate or todate is null)) then
          begin
            if (m < 12) then
              t_limit = 1.0000 * addvendmonth/12 * addlimit_nom/addvmonths * m * dimnum / dimden + 0.4999;
            else
              t_limit = 1.0000 * addvendmonth/12 * addlimit_nom * dimnum / dimden + 0.4999;
        
            if (t_limit > addlimit_nom - climitm) then
              t_limit = addlimit_nom - climitm - addvlimitm;
            if (t_limit < 0) then
              t_limit = 0;

            addvlimitm = addvlimitm + t_limit;
          end
        end else
        begin
        --nie bylo wylaczen - liczenie proste
  
          m = extract(month from todate) - extract(month from fromdate) + 1 - prevwork;
          if (m < 12) then
            t_limit = 1.0000 * vaclimitm/12 * m * dimnum / dimden + 0.4999;
          else
            t_limit = 1.0000 * vaclimitm * dimnum / dimden + 0.4999;

          if (vlimitm + t_limit > vaclimitm - climitm) then
            t_limit = vaclimitm - vlimitm - climitm;
          if (t_limit < 0) then
            t_limit = 0;
  
          vlimitm = vlimitm + t_limit;
  
          --urlop uzupelniajacy
          if (fromdate <= addvdate and (addvdate <= todate or todate is null)) then
          begin
            -- 6 dni * (miesiac w ktorym przestal pracowac / liczba miesiecy w roku)
            -- * (liczba miesiecy przepracowanych / laczna liczba miesiecy przepracowanych w danym roku)
            -- * wymiar etatu
            if (m < 12) then
              t_limit = 1.0000 * addvendmonth/12 * addlimit_nom/addvmonths * m * dimnum / dimden + 0.4999;
            else
              t_limit = 1.0000 * addvendmonth/12 * addlimit_nom * dimnum / dimden + 0.4999;

            if (t_limit > addlimit_nom) then
              t_limit = addlimit_nom - addvlimitm;
            if (t_limit < 0) then
              t_limit = 0;

            addvlimitm = addvlimitm + t_limit;
          end
        end
      end
  
   -----------urlop rehabilitacyjny-----------------
     if (disvfromdate is not null) then
     begin
       t_limit = 0;
     --jezeli jest to pierwszy rok uzyskania prawa do urlopu to ma od razu komplet dni
       select extract(year from min(fromdate))
           from get_disable_periods(:employee,0)
          into :disableyearstart;

       if (vyear = disableyearstart) then
          disablelimitm = 10.0000 * vac_dim_minutes * dimnum / dimden + 0.4999;
        else if (m < 12) then
          t_limit = 10.0000 * vac_dim_minutes/12 * m * dimnum / dimden + 0.4999;
        else
          t_limit = 10.0000 * vac_dim_minutes * dimnum / dimden + 0.4999;
  
        disablelimitm = disablelimitm + t_limit;
      end
      prev_calendar = 1;
    end
  end

  --zapisy w KP sa tak ogolnikowe ze nie wiem czy na pewno to tak powinno byc
  execute procedure get_config('ROUND_FIRSTJOB', 2) returning_values round_firstjob;
  if ((round_firstjob = 1 or firstjob = 0) and vac_dim_minutes > 0) then
  begin
    --zaokraglanie wymiaru urlopu w gore do pelnych dni
    if (mod(vlimitm,vac_dim_minutes) <> 0) then
      vlimitm = vlimitm + vac_dim_minutes - mod(vlimitm,vac_dim_minutes);
    if (mod(addvlimitm,vac_dim_minutes) <> 0) then
      addvlimitm = addvlimitm + vac_dim_minutes - mod(addvlimitm,vac_dim_minutes);
    if (mod(disablelimitm,vac_dim_minutes) <> 0) then
      disablelimitm = disablelimitm + vac_dim_minutes - mod(disablelimitm,vac_dim_minutes);
  end
  
  /*Jesli w tym roku nie bylo zmiany wymiaru uprawnien urlopowych to nie pokazuj daty.
    Warunek addvdate_cust=0 zapewnia, aby data addvdate nie zostala nadpisana, kiedy zostaje
    ono ustalona biezaca procedura i robiony jest update evalimits bez addvdate_cust=2 (BS51114)*/
  if (vaclimit = end_vaclimit and addvdate_cust = 0) then
    addvdate = null;

  --PR32157 odswiezanie urlopu wykorzystanego
  select sum(coalesce(vacsecs,worksecs)) / 60
    from eabsences
    where ecolumn in (:vc,:vc2)
      and correction in (0,2)
      and employee = :employee
      and ayear = :vyear
      and fromdate <= coalesce(cast(:ctxtodate as date), :vyear_end) -- PR54992
    into :limitconsm;
  limitconsm = coalesce(limitconsm,0);
  suspend;
end^
SET TERM ; ^
