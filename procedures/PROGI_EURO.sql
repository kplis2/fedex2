--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PROGI_EURO(
      KLIENT integer,
      DATA timestamp,
      WARTOSC numeric(14,2))
  returns (
      KWOTA numeric(14,2))
   as
declare variable pokres varchar(6);
declare variable mies integer;
declare variable rok integer;
declare variable licznik_pop integer;
declare variable kurs numeric(14,2);
declare variable spzeur INTEGER;
declare variable dostawca INTEGER;
declare variable spzap INTEGER;
declare variable suma numeric(14,2);
declare variable wart numeric(14,2);
declare variable tersuma numeric(14,2);
declare variable tersumawz numeric(14,2);
declare variable prog numeric(14,2);
declare variable progwart numeric(14,2);
declare variable ostkurs numeric(14,2);

begin
  /*wyznaczenie poprzedniego okresu*/
  if (:data is null) then data = '0001-01-01';
  if (:wartosc is null) then wartosc = 0;
  rok = extract(year from :data);
  mies = extract(month from :data);
  mies = cast(:mies as integer);
  rok = cast(:rok as integer);
  mies = mies - 1;
  if (mies <= 0) then begin
    mies = 12;
    rok = rok -1;
  end
  if (mies>10) then
    pokres = cast (rok as varchar(4)) || cast (mies as varchar(2));
  else
    pokres = cast (rok as varchar(4)) || '0' || cast (mies as varchar(2));
  if (klient is null) then klient = 0;
  /* obliczenie obrotu z poprzedniego miesiaca - gotówka*/
  licznik_pop = 0;
  suma = 0;
  kwota = 0;
  select konfig.wartosc from konfig where konfig.akronim = 'SPOSZAPEUR' into :spzeur;
  for select nagfak.sumwartbruzl, nagfak.sposzap from nagfak where
    position(';'||nagfak.akceptacja||';' in ';1;8;')>0 and nagfak.nieobrot < 2
    and nagfak.anulowanie = 0 and nagfak.okres = :pokres
    and (nagfak.klient=:klient or nagfak.dostawca = :klient) into :wart, :spzap
  do begin
    suma = :suma + :wart;
    if (spzap = :spzeur) then licznik_pop = :licznik_pop + 1;
  end
  select konfig.wartosc from konfig where konfig.akronim = 'OSTKURSEURO' into :kurs;
  if (:kurs is null) then kurs = 1;
  select konfig.wartosc from konfig where konfig.akronim = 'PROGEUR' into :prog;
  if (:prog is null) then prog = 0;
  /*okreslenie progu euro zakupu*/
  if (:suma/:kurs > :prog) then
    select konfig.wartosc from konfig where konfig.akronim = 'PROG1' into :progwart;
  else
    select konfig.wartosc from konfig where konfig.akronim = 'PROG2' into :progwart;
  if (:progwart is null) then progwart = 0;

  if (licznik_pop > 0) then begin
    /*obliczenie dotychczasowego obrotu w biezacym dniu faktury + wydania i przyjcia zewntrane*/
    select klienci.dostawca from klienci where klienci.ref = :klient into :dostawca;
    select sum(nagfak.sumwartbruzl)
      from nagfak where (nagfak.klient=:klient or nagfak.dostawca=:dostawca)
      and position(';'||nagfak.akceptacja||';' in ';1;8;')>0 and nagfak.nieobrot < 2
      and nagfak.anulowanie = 0 and nagfak.dataakc = :data into :tersuma;
    if (tersuma is null) then tersuma=0;
    select sum(dokumnag.wartsbrutto) from dokumnag
      join defdokum on (defdokum.symbol = dokumnag.typ)
      where (dokumnag.klient = :klient or dokumnag.dostawca = :dostawca) and dokumnag.akcept = 1
      and defdokum.koryg = 0 and defdokum.zewn = 1
      and (dokumnag.termdost = :data or ((dokumnag.termdost is null or dokumnag.termdost<>:data)
      and dokumnag.data = :data))
      and dokumnag.faktura is null into :tersumawz;
    if (tersumawz is null) then tersumawz=0;
    ostkurs = cast(:kurs as numeric(14,2));
    progwart = : progwart * :kurs;
    kwota = :tersuma + :tersumawz - :progwart;
  end
  else kwota = 0 - :progwart * :kurs;
  kwota = :kwota + :wartosc;
end^
SET TERM ; ^
