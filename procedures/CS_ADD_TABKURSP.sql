--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CS_ADD_TABKURSP(
      TABKURSREF integer,
      WALUTA varchar(3) CHARACTER SET UTF8                           ,
      PRZELICZNIK integer,
      KURSSPRZED varchar(20) CHARACTER SET UTF8                           ,
      KURSZAK varchar(20) CHARACTER SET UTF8                            = null,
      KURSSR varchar(20) CHARACTER SET UTF8                            = null)
  returns (
      TABKURSPREF integer)
   as
begin
  tabkurspref=0;
  if (exists(select symbol from waluty where symbol=:waluta)) then
  begin
    KURSZAK=replace(:KURSZAK,',','.');
    KURSSPRZED=replace(:KURSSPRZED,',','.');
    KURSSR=replace(KURSSR,',','.');

    if (przelicznik=0) then
      przelicznik=1;
    if (not exists(select tabkurs from tabkursp where tabkurs=:tabkursref and waluta=:waluta)) then
    begin
      tabkurspref=1;
      --dodaj
      if (cast(KURSSPRZED as numeric(14,4))>0 and cast(KURSZAK as numeric(14,4))> 0) then
        insert into tabkursp(tabkurs, waluta, zakupu, sprzedazy)
          values(:tabkursref, :waluta,  cast(:KURSZAK as numeric(14,4))/:przelicznik, cast(:KURSSPRZED as numeric(14,4))/:przelicznik);
      else if (cast(KURSSR as numeric(14,4))>0) then
        insert into tabkursp(tabkurs, waluta, zakupu, sprzedazy)
          values(:tabkursref, :waluta, cast(:KURSSR as numeric(14,4))/:przelicznik, cast(:KURSSR as numeric(14,4))/:przelicznik);
    end else begin
      tabkurspref=2;
      --uaktualnij
      if (cast(KURSSR as numeric(14,4)) >0 and cast(KURSZAK as numeric(14,4)) > 0) then
        update tabkursp
          set zakupu=cast(:KURSZAK as numeric(14,4))/:przelicznik, sprzedazy=cast(:KURSSR as numeric(14,4))/:przelicznik
          where tabkurs=:tabkursref and waluta=:waluta;
      else if (cast(KURSSR as numeric(14,4)) >0) then
        update tabkursp
          set zakupu=cast(:KURSSR as numeric(14,4))/:przelicznik, sprzedazy=cast(:KURSSR as numeric(14,4))/:przelicznik
          where tabkurs=:tabkursref and waluta=:waluta;
    END
  end
  suspend;
end^
SET TERM ; ^
