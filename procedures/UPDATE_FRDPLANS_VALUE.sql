--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE UPDATE_FRDPLANS_VALUE(
      FRDHDRVER integer,
      FRDPERSDIMVAL integer,
      PERSDIMHIER integer,
      DIMELEM integer,
      TIMELEM integer,
      ELEMHIER varchar(40) CHARACTER SET UTF8                           ,
      SETVALUE numeric(14,2),
      LASTTIME timestamp,
      VALTYPE varchar(3) CHARACTER SET UTF8                           )
   as
begin
  if (VALTYPE = 'PLA') then
    update frdcells set frdcells.planamount = :setvalue, frdcells.timeofchange = :lasttime
      where frdcells.frdhdrver = :frdhdrver and frdcells.frdpersdimval = :frdpersdimval
        and frdcells.dimelem = :dimelem and frdcells.timelem = :timelem
        and frdcells.elemhier = :elemhier;
  if (VALTYPE = 'VA1') then
    update frdcells set frdcells.val1 = :setvalue, frdcells.timeofchange = :lasttime
      where frdcells.frdhdrver = :frdhdrver and frdcells.frdpersdimval = :frdpersdimval
        and frdcells.dimelem = :dimelem and frdcells.timelem = :timelem
        and frdcells.elemhier = :elemhier;
  if (VALTYPE = 'VA2') then
    update frdcells set frdcells.val2 = :setvalue, frdcells.timeofchange = :lasttime
      where frdcells.frdhdrver = :frdhdrver and frdcells.frdpersdimval = :frdpersdimval
        and frdcells.dimelem = :dimelem and frdcells.timelem = :timelem
        and frdcells.elemhier = :elemhier;
  if (VALTYPE = 'VA3') then
    update frdcells set frdcells.val3 = :setvalue, frdcells.timeofchange = :lasttime
      where frdcells.frdhdrver = :frdhdrver and frdcells.frdpersdimval = :frdpersdimval
        and frdcells.dimelem = :dimelem and frdcells.timelem = :timelem
        and frdcells.elemhier = :elemhier;
  if (VALTYPE = 'VA4') then
    update frdcells set frdcells.val4 = :setvalue, frdcells.timeofchange = :lasttime
      where frdcells.frdhdrver = :frdhdrver and frdcells.frdpersdimval = :frdpersdimval
        and frdcells.dimelem = :dimelem and frdcells.timelem = :timelem
        and frdcells.elemhier = :elemhier;
  if (VALTYPE = 'VA5') then
    update frdcells set frdcells.val5 = :setvalue, frdcells.timeofchange = :lasttime
      where frdcells.frdhdrver = :frdhdrver and frdcells.frdpersdimval = :frdpersdimval
        and frdcells.dimelem = :dimelem and frdcells.timelem = :timelem
        and frdcells.elemhier = :elemhier;
end^
SET TERM ; ^
