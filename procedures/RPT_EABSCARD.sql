--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_EABSCARD(
      COMPANY integer,
      EMPLOYEE integer,
      DEPARTMENT varchar(10) CHARACTER SET UTF8                           ,
      AFROMDATE date,
      ATODATE date,
      AFROMCPER varchar(6) CHARACTER SET UTF8                           ,
      ATOCPER varchar(6) CHARACTER SET UTF8                           ,
      ABSGROUP varchar(256) CHARACTER SET UTF8                           ,
      ABSELECT varchar(1024) CHARACTER SET UTF8                           ,
      MIN_BENEFITPERIOD smallint,
      MIN_ABSCONTINUITY smallint,
      RESULT_TYPE smallint = 0)
  returns (
      EMPLOYEE_REF integer,
      DEPARTMENT_NAME varchar(60) CHARACTER SET UTF8                           ,
      FNAME varchar(60) CHARACTER SET UTF8                           ,
      SNAME varchar(30) CHARACTER SET UTF8                           ,
      ECOLUMN integer,
      NAME varchar(25) CHARACTER SET UTF8                           ,
      FROMDATE date,
      TODATE date,
      DAYS integer,
      WORKDAYS integer,
      WORKHOURS varchar(20) CHARACTER SET UTF8                           ,
      MONTHPAYBASE numeric(14,2),
      WORKPAY numeric(14,2),
      ZUSPAY numeric(14,2),
      CORRECT_INFO varchar(5) CHARACTER SET UTF8                           ,
      ISZUS smallint,
      WORKSECS integer,
      ECOLUMN_NAMES varchar(1024) CHARACTER SET UTF8                           ,
      ABS_GROUP_NAME varchar(30) CHARACTER SET UTF8                           ,
      ACPER varchar(6) CHARACTER SET UTF8                           )
   as
declare variable CORRECTED integer;
declare variable CORRECTION smallint;
declare variable T_DAYS integer;
declare variable T_WORKDAYS integer;
declare variable T_WORKSECS integer;
declare variable T_WORKPAY numeric(14,2);
declare variable T_ZUSPAY numeric(14,2);
declare variable T_ISZUS smallint;
declare variable AREF integer;
declare variable ISSUSPEND smallint;
declare variable TMPDAYS integer;
declare variable EC_NAME varchar(22);
declare variable I integer;
begin
--TM: Procedura wykorzystywana przy wydruku kartoteki nieobecnosci w rozszerzonej wersji ('kartoteka nieobecnosci place')

  absgroup = coalesce(absgroup,'');

  if (coalesce(result_type,0) = 0) then
  begin
    if (employee = 0) then employee = null;
    if (trim(department) = '') then department = null;
    if (trim(afromcper) = '') then afromcper = null;
    if (trim(atocper) = '') then atocper = null;
    min_benefitperiod = coalesce(min_benefitperiod,0);
    min_abscontinuity = coalesce(min_abscontinuity,0);
    company = coalesce(company,1);

    abselect = coalesce(abselect,'');
    if (abselect <> '' and position(abselect in '"') = 0) then abselect = '"' || abselect || '"';
  
    afromcper = coalesce(afromcper,'000000');
    atocper = coalesce(atocper,'999999');

    for
      select a.ref, a.correction , a.ecolumn, c.name, a.fromdate, a.todate,
            a.days, a.workdays, a.monthpaybase, a.corrected, a.workhours, a.worksecs,
            case when a.ecolumn in (90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450) then 1 else 0 end,
            case when a.ecolumn in (90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450) then a.payamount else null end,
            case when a.ecolumn in (90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450) then null else a.payamount end,
            e.ref, trim(d.name), e.fname, e.sname, r.cper
        from ecolumns c
          join eabsences a on (a.ecolumn = c.number
                            and (:abselect = '' or :abselect like '%"' || a.ecolumn || '"%'))
                            and (c.typ containing :absgroup or :absgroup = '')
          join employees e on (e.ref = a.employee)
          join persons p on (p.ref = e.person)
          left join departments d on (d.symbol = e.department)
          left join epayrolls r on (r.ref = a.epayroll)
          left join epayrolls r2 on (r2.ref = a.corepayroll)
        where e.company = :company
          and (:employee is null or a.employee = :employee)
          and (:department is null or d.symbol = :department)
          and (:afromdate is null or a.todate >= :afromdate)
          and (:atodate is null or a.fromdate <= :atodate)
          and (coalesce(r.cper,'000000') >= :afromcper
            or coalesce(r2.cper,'000000') >= :afromcper)
          and (coalesce(r.cper,'999999') <= :atocper
            or coalesce(r2.cper,'999999') <= :atocper)
        order by /*ep.payday,*/ a.fromdate, a.correction, case when a.corrected is null then 0 else 1 end
        into :aref, :correction, :ecolumn, :name, :fromdate, :todate, :days, :workdays, :monthpaybase,
             :corrected, :workhours, :worksecs, :iszus, :zuspay, :workpay, :employee_ref, :department_name,
             :fname, :sname, :acper
    do begin
      issuspend = 1;
      if (min_benefitperiod > 0 and ecolumn in (40,50,60,90,100,110,120,130,140,150,160,170,270,280,290,440,450)) then
      begin
         select sickbenefitdays
           from e_get_sickbenefitdays(:aref, ';40;50;60;90;100;110;120;130;140;150;160;170;270;280;290;440;450;',0,1)
           into :tmpdays;
         if (tmpdays <= min_benefitperiod) then
           issuspend = 0;
      end
  
      if (issuspend = 1 and min_abscontinuity > 0 and ecolumn in (40,50,60,90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450)) then
      begin
        select atodate - afromdate + 1
          from e_get_whole_eabsperiod(:aref, null, null, null, null, null, ';40;50;60;90;100;110;120;130;140;150;160;170;270;280;290;350;360;370;440;450;',null,1)
          into :tmpdays;
        if (tmpdays <= min_abscontinuity) then
          issuspend = 0;
      end
  
      if(issuspend  = 1) then
      begin
        if (correction in (0,1) and (coalesce(acper,'000000') >= :afromcper
          and coalesce(acper,'999999') <= :atocper))
        then begin
          correct_info = '';
          suspend;
        end
    
        if (correction = 1) then
        begin
           corrected = null;
           select ref from eabsences
             where corrected = :aref
             into :corrected;
    
          t_iszus = null;
          select case when ecolumn in (90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450) then 1 else 0 end
             from eabsences
             where ref = :corrected
             into :t_iszus;
    
          if (iszus <> :t_iszus) then
          begin
            correct_info = 'K-';
            days = -days;
            workdays = -workdays;
            workhours = '-' || workhours;
            worksecs = -worksecs;
            workpay = -workpay;
            zuspay = -zuspay;
            suspend;
          end
        end else
        if (correction = 2) then
        begin
          t_iszus = null;
          t_zuspay = null;
          t_workpay = null;
          t_days = null;
          t_workdays = null;
          t_worksecs = null;
    
          select days, workdays, worksecs,
             case when ecolumn in (90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450) then 1 else 0 end,
             case when ecolumn in (90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450) then payamount else null end,
             case when ecolumn in (90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450) then null else payamount end
             from eabsences
             where ref = :corrected
             into :t_days, :t_workdays, :t_worksecs, :t_iszus, :t_zuspay, :t_workpay;
    
          correct_info = 'K';
          if (iszus = t_iszus) then begin
            days = days - coalesce(t_days,0);
            workdays = workdays - coalesce(t_workdays,0);
            worksecs = worksecs - coalesce(t_worksecs,0);
            execute procedure durationstr2(:worksecs,0,1)
              returning_values :workhours;
          end
          if (iszus = 1) then begin
            zuspay = coalesce(zuspay,0) - coalesce(t_zuspay,0);
            workpay = null;
          end else begin
            workpay = coalesce(workpay,0) - coalesce(t_workpay,0);
            zuspay = null;
          end
          iszus = t_iszus;
          suspend;
        end 
      end
    end
  end else
  begin
    if (abselect <> '') then
    begin
      ecolumn_names = '';
      i = 0;
      for
        select name from ecolumns
          where :abselect like '%"' || number || '"%'
          into ec_name
      do begin
        if (i > 0) then  ecolumn_names = ecolumn_names || '; ';
        ecolumn_names = ecolumn_names || ec_name;
        i = i + 1;
      end
    end

    select f.name
      from fldflagsdef f
      where f.fldflagsgrp = 'ECOLUMNSF'
        and f.symbol = :absgroup
      into :abs_group_name;

    if(department <> '') then
      select trim(name) from departments
        where symbol = :department
        into department_name;
    suspend;
  end
end^
SET TERM ; ^
