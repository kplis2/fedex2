--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COPY_CPODMIOTY(
      PODMIOTZ integer,
      PODMIOTDO integer)
   as
begin
   --KOPIOWANIE OSÓB CRM
   --OSOBY
        update pkosoby p
          set p.cpodmiot = :podmiotdo
          where p.cpodmiot = :podmiotz;
    --KONTAKTY
        update kontakty k
          set k.cpodmiot = :podmiotdo
          where k.cpodmiot = :podmiotz;

    --ZADANIA
        update zadania z
          set z.cpodmiot = :podmiotdo
          where z.cpodmiot = :podmiotz;

     --SPRAWY
        update ckontrakty c
          set c.cpodmiot = :podmiotdo
          where c.cpodmiot = :podmiotz;

     -- DEZAKTYWACJA PODMIOTU
        update cpodmioty c
          set c.aktywny = 0
          where c.ref = :podmiotz;
end^
SET TERM ; ^
