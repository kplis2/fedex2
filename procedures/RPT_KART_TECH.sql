--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_KART_TECH(
      MASKA varchar(40) CHARACTER SET UTF8                           )
  returns (
      SHEETS integer,
      OKTM varchar(40) CHARACTER SET UTF8                           )
   as
declare variable KTM VARCHAR(40);
declare variable refktm integer;
begin
  FOR SELECT PM.KTM  FROM PRSHMAT PM
WHERE PM.KTM like :maska||'%' group by KTM

    INTO :KTM
  DO BEGIN
     for select ref from prshmat where ktm = :ktm into :refktm
     do begin
      SELECT SHEET, KTM
        FROM PRSHMAT
        WHERE ref = :refktm
          into :SHEETS,:OKTM;
            suspend;
      end
  END

end^
SET TERM ; ^
