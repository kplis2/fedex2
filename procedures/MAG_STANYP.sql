--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_STANYP(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      CENA numeric(14,4),
      DOSTAWA integer,
      DATA timestamp)
  returns (
      RKTM varchar(40) CHARACTER SET UTF8                           ,
      RWERSJA integer,
      RCENA numeric(14,4),
      RDOSTAWA integer,
      STAN numeric(14,4),
      WARTOSC numeric(14,2))
   as
declare variable typ char(1);
declare variable stmin numeric(14,4);
declare variable wartmin numeric(14,2);
declare variable datal timestamp;
declare variable datam timestamp;
declare variable numerlast integer;
declare variable rref integer;
declare variable cenanew numeric(14,4);
declare variable wydania smallint;
begin
  select TYP from DEFMAGAZ where SYMBOL=:MAGAZYn into :typ;
  if(:typ = 'E' or :typ = 'S') then begin
    /* okrelenie, co byo do tego momentu na plus */
    for select DOKUMPOZ.KTM, DOKUMPOZ.WERSJA , SUM(DOKUMROZ.ILOSC), sum(DOKUMROZ.WARTOSC), max(DOKUMNAG.DATA)
       from DOKUMROZ join DOKUMPOZ on ( DOKUMROZ.pozycja = DOKUMPOZ.ref)
                        join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.dokument)
                        join DEFDOKUM on ( DOKUMNAG.typ = DEFDOKUM.symbol)
       where DOKUMNAG.MAGAZYN = :MAGAZYN and (DOKUMPOZ.KTM = :ktm or (:ktm is null)) and (DOKUMPOZ.WERSJA = :wersja or (:wersja is null))
            and DOKUMNAG.DATA <= :DATA and (DOKUMNAG.akcept = 1 or DOKUMNAG.AKCEPT=8) AND DEFDOKUM.wydania = 0
       group by DOKUMPOZ.KTM, DOKUMPOZ.WERSJA
       into :RKTM, :RWERSJA, :stan, :wartosc, :datal
    do begin
       /*okrelenie rozchodów*/
       stmin = null;
       datam = null;
       wartmin = null;
       select sum(DOKUMROZ.ilosc),sum(dokumroz.wartosc), max(dokumnag.data) from DOKUMROZ join DOKUMPOZ on ( DOKUMROZ.pozycja = DOKUMPOZ.ref)
                        join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.dokument)
                        join DEFDOKUM on ( DOKUMNAG.typ = DEFDOKUM.symbol)
       where DOKUMNAG.MAGAZYN = :MAGAZYN and (DOKUMPOZ.KTM = :rktm)  and (DOKUMPOZ.WERSJA = :rwersja)
                   and DOKUMNAG.DATA <= :DATA and (DOKUMNAG.akcept = 1 or DOKUMNAG.AKCEPT=8) AND DEFDOKUM.wydania = 1
       into :stmin, :wartmin, :datam;
       if(:wartmin is null) then wartmin = 0;
       if(:stmin is null) then stmin = 0;
       stan = :stan - :stmin;
       wartosc = :wartosc - :wartmin;
       if(:datam > :datal) then datal = :datam;
       /* teraz tylko okrelić ilosc - cena z ostatniej rozpiski w tym okresie*/
       numerlast = null;
       if(:stan <> 0) then begin
         select max(DOKUMNAG.NUMER) from DOKUMROZ join DOKUMPOZ on ( DOKUMROZ.pozycja = DOKUMPOZ.ref)
                        join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.dokument)
           where DOKUMNAG.MAGAZYN = :MAGAZYN and (DOKUMPOZ.KTM = :rktm)  and (DOKUMPOZ.WERSJA = :rwersja)
                   and DOKUMNAG.DATA =:datal and (DOKUMNAG.akcept = 1 or DOKUMNAG.AKCEPT=8)
         into :numerlast;
         rcena = null;
         select max(DOKUMROZ.cena) from DOKUMROZ join DOKUMPOZ on ( DOKUMROZ.pozycja = DOKUMPOZ.ref)
                         join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.dokument)
           where DOKUMNAG.MAGAZYN = :MAGAZYN and (DOKUMPOZ.KTM = :rktm)  and (DOKUMPOZ.WERSJA = :rwersja)
                   and DOKUMNAG.DATA =:datal and (DOKUMNAG.NUMER = :numerlast or (:numerlast is null))and (DOKUMNAG.akcept = 1 or DOKUMNAG.AKCEPT=8)
           into :rcena;
         if(:rcena is null) then rcena = 0;
         suspend;
       end
    end
    exit;
  end
  if(:typ = 'C' or (:typ = 'P')) then begin
    for select RKTM,RWERSJA,RCENA,RDOSTAWA,
               sum(STAN),sum(WARTOSC),sum(STMIN),sum(WARTMIN)
        from MAG_OBROTYP(:magazyn,:ktm,:wersja,:cena,:dostawa,:data)
        group by
           RKTM, RWERSJA, RCENA, RDOSTAWA
        into :rktm, :rwersja, :rcena, :rdostawa
             ,:stan, :wartosc, :stmin, :wartmin
    do begin
      if(:stan is null) then stan = 0;
      if(:wartosc is null) then wartosc = 0;
      if(:stmin is null) then stmin = 0;
      if(:wartmin is null) then wartmin = 0;
      stan = :stan - :stmin;
      wartosc = :wartosc - :wartmin;/* to nam daje po prostu rozchody */
      stan = :stan - :stmin;
      wartosc = :wartosc - :wartmin;
      if((:stan <> 0) or (:wartosc<>0)) then suspend;
      rktm = null;
      rwersja = null;
      rcena = null;
      rdostawa = null;
      stan = null;
      wartosc = null;
    end
  end
end^
SET TERM ; ^
