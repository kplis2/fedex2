--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_VAT_ZAKUPR(
      PERIODID varchar(6) CHARACTER SET UTF8                           ,
      VREG varchar(10) CHARACTER SET UTF8                           ,
      ZAKRES smallint,
      ZPOPMIES smallint,
      TYP smallint,
      COMPANY integer,
      VATDEBITTYPE integer)
  returns (
      REF integer,
      LP integer,
      VATREG VATREGS_ID,
      VATREGNR integer,
      TRANSDATE timestamp,
      DOCDATE timestamp,
      DOCSYMBOL DOCSYMBOL_ID,
      BKREG BKREGS_ID,
      BKREGNR integer,
      KONTRAHKOD varchar(255) CHARACTER SET UTF8                           ,
      NIP varchar(15) CHARACTER SET UTF8                           ,
      VATGR VAT_ID,
      NETV numeric(14,2),
      VATV numeric(14,2),
      VATE numeric(14,2),
      VATN numeric(14,2),
      BRUE numeric(14,2),
      NR integer,
      TOSUMMARY smallint)
   as
declare variable docref integer;
declare variable sql varchar(5048);
declare variable vatregtype integer; /* <<PR73894: PL>> */
declare variable print integer;
begin
  -- TYP wczytywany z pola kombo w ktorym jest nastepujaca kolejnosc
-- 0 zakupy cale
-- 1 Zakupy krajowe
-- 2 Dostawy importowe
-- 3 WNT
-- 4 Usługi z importu
  if (typ = -1) then
    typ = 0; -- jak ktos wykasuje wpis w combo
  ref = 0;
  lp = 0;
  if(:zakres is null) then zakres = 0;
  if(:zpopmies is null) then zpopmies = 0;

  --BS08473
  sql = 'select B.REF, b.transdate, b.docdate, B.vatreg, b.vnumber, b.bkreg, b.number, B.NIP, b.contractor, b.symbol, v.vatregtype'; --<<PR73894: PL>>
  sql = sql || ' from bkdocs B join VATREGS V on (B.vatreg = V.symbol and B.company = V.company) join BKDOCTYPES T on (B.doctype = t.ref)';
  sql = sql ||' where B.vatperiod = '''||:periodid||''' and b.status > 0';-- and v.vtype in (3,4,5,6)';
   --<<PR73894: PL
  sql = sql ||'  and (v.vatregtype in (1,2)
    or (V.VATREGTYPE = 3
         and exists(select first 1 1 from bkvatpos join grvat on (grvat.symbol = bkvatpos.taxgr) where bkvatpos.bkdoc = B.ref and grvat.vatregtype in (1,2))))';
  if(coalesce(typ,0)>0) then
  begin
    sql = sql ||' and v.vtype = '||
    case typ
      when 1 then '3' --Zakupy krajowe
      when 2 then '4' --Dostawy importowe
      when 3 then '5' --WNT
      when 4 then '6' --Usługi z importu
    end;
  end
-->>
  if(coalesce(zakres,0)=1)then sql = sql ||' and t.creditnote = 0';
  else if(coalesce(zakres,0)=2)then sql = sql ||' and t.creditnote = 1';
  if(coalesce(zpopmies,0)>0)then sql = sql ||' and (b.vatperiod <> b.period)';
  if(coalesce(vreg,'')<>'')then sql = sql ||' and b.vatreg = '''||:vreg||'''';
  sql = sql ||' and B.company ='||:company||' order by B.vatreg, b.vnumber';
  for execute statement :sql

  /*for select B.REF, b.transdate, b.docdate, B.vatreg, b.vnumber, b.bkreg, b.number,
              B.NIP, b.contractor, b.symbol
    from bkdocs B
    join VATREGS V on (B.vatreg = V.symbol and B.company = V.company)
    join BKDOCTYPES T on (B.doctype = t.ref)
    where
      B.vatperiod = :periodid and b.status > 1
      and v.vtype in (3,4,5,6) --rejestry zakupu
      and (:typ = 0 or v.vtype = (:typ + 2)) -- wszystkie albo wybrany rejestr
      and ((:zakres = 0) or (:zakres = 1 and t.creditnote = 0) or (:zakres = 2 and t.creditnote = 1))
      and ((:zpopmies = 0) or (b.vatperiod <> b.period))
      and ((:vreg is null) or (:vreg = '') or (b.vatreg = :vreg))
      and B.company = :company
    order by B.vatreg, b.vnumber*/
  into :docref, :transdate, :docdate, :vatreg, :vatregnr,:bkreg, :bkregnr,
       :nip, :kontrahkod, :docsymbol, :vatregtype   --<<PR73894: PL>>
  do begin
    nr = 0;
    lp = :lp + 1;
    print = 0;
    for select VATGR,sum(NETV), sum(VATV), sum(VATE),sum(VATE) - sum(VATV) as VATN
      from BKVATPOS
        join GRVAT on (GRVAT.SYMBOL = BKVATPOS.TAXGR)  --<<PR73894: PL>>
      where bkdoc = :docref and (debittype = :vatdebittype or :vatdebittype = 4)
        and (:vatregtype in (1,2) or (:vatregtype = 3 and (grvat.vatregtype in (1,2))))--<<PR73894: PL>>
      group by VATGR
      into :vatgr, :netv, :vatv, :vate, :vatn
    do begin
    print = 1;
      brue = :netv + :vate;
      ref = :ref + 1;
      nr = :nr + 1;
      tosummary = 1;
      suspend;
    end
    tosummary = 0;
    vatgr = null;
    netv = null;
    vatv = null;
    vate = null;
    vatn = null; -- BS69817

    brue = null;
    if(:nr < 2) then begin
      nr = nr + 1;
      ref = ref + 1;
      if (print = 1) then
        suspend;
    end
  end
end^
SET TERM ; ^
