--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PRSHEETSOPER_NEW(
      SHEET integer)
  returns (
      OSHEET integer,
      OOPER integer,
      OLICZNIK integer,
      OPOZIOM integer,
      ONUMER integer,
      TYP varchar(1) CHARACTER SET UTF8                           ,
      P0 varchar(80) CHARACTER SET UTF8                           ,
      P1 varchar(80) CHARACTER SET UTF8                           ,
      P2 varchar(80) CHARACTER SET UTF8                           ,
      P3 varchar(80) CHARACTER SET UTF8                           ,
      P4 varchar(80) CHARACTER SET UTF8                           ,
      P5 varchar(80) CHARACTER SET UTF8                           ,
      P6 varchar(80) CHARACTER SET UTF8                           ,
      P7 varchar(80) CHARACTER SET UTF8                           ,
      WCIECIA varchar(20) CHARACTER SET UTF8                           ,
      REF integer,
      OOST integer,
      OZLOZ integer,
      OOSTATNIA integer,
      RYSUJ1 bigint,
      RYSUJ2 integer,
      RYSUJ3 integer,
      RYSUJ4 integer,
      RYSUJ5 integer)
   as
DECLARE VARIABLE OREF INTEGER;
DECLARE VARIABLE I INTEGER;
DECLARE VARIABLE M INTEGER;
DECLARE VARIABLE T INTEGER;
declare VARIABLE TOOLTYPE varchar(20);
DECLARE VARIABLE MATTYPE INTEGER;
declare variable MATCNT INTEGER;
DECLARE variable TOOLCNT INTEGER;
begin
  Rysuj1=0;
  rysuj2=0;
  rysuj3=0;
  rysuj4=0;
  rysuj5=0;
  --OOSTPOZIOM=-1;
  wciecia=null;
  for select OSHEET, OOPER, OLICZNIK, OPOZIOM, ONUMER, TYP, P0, P1, P2, P3, P4, P5, P6, P7, REF, OZLOZONA, OOSTATNIAWGRUPIE,
             OR1, OR2, OR3, OR4, OR5
    from RPT_PRSHEETSOPER(:SHEET, null, -1,-1,-1,:RYSUJ1,:RYSUJ2,:RYSUJ3,:RYSUJ4,:RYSUJ5 )
    into :OSHEET, :OOPER, :OLICZNIK, :OPOZIOM,  :ONUMER, :TYP, :P0, :P1, :P2, :P3, :P4, :P5, :P6, :P7, :REF, :OZLOZ, :OOST,
         :rysuj1, :rysuj2, :rysuj3, :rysuj4, :rysuj5

  do begin
--    RYSUJ1 = RYSUJ1 +RYSUJ1;


    i=0;
    wciecia='';
    while (:i<=:opoziom) do begin
      wciecia=:wciecia||'    ';
      i=:i+1;
    end
--    IF (opoziom = 0 ) then rysuj1 = oost ;
--   IF (opoziom = 1 ) then rysuj2 = oost ;
--    else IF (opoziom = 2 ) then rysuj3 = oost ;
    suspend;
    wciecia='';
    select  count(*)
      from prshmat PRM
        left join wersje on (wersje.ref = PRM.wersjaref)
        left join towjedn on (towjedn.ref = PRM.jedn)
        left join prsheets on (prsheets.ref = PRM.subsheet)
      where PRM.sheet=:SHEET and PRM.opermaster = :REF
      into :matcnt;
    if (matcnt>0) then opoziom = opoziom +1;

i=0;
    wciecia='';
    while (:i<=:opoziom) do begin
      wciecia=:wciecia||'    ';
      i=:i+1;
    end
    m=0;oost=0;
    for
      select  PRM.mattype, PRM.descript, PRM.KTM||'   '||WERSJE.nazwa,
              PRM.netquantity, PRM.grossquantity, TOWJEDN.jedn, prsheets.symbol, ''
        from prshmat PRM
        left join wersje on (wersje.ref = PRM.wersjaref)
        left join towjedn on (towjedn.ref = PRM.jedn)
        left join prsheets on (prsheets.ref = PRM.subsheet)
        where PRM.sheet=:SHEET and PRM.opermaster = :REF
        order by PRM.number
        INTO  MATTYPE, P1, P2,
              P3, P4, P5, P6, P7
    do begin
      if (rysuj1=1) then rysuj1=rysuj1+1;
      if (rysuj2=1) then rysuj2=rysuj2+1;
      if (rysuj3=1) then rysuj3=rysuj3+1;
      if (rysuj4=1) then rysuj4=rysuj4+1;
      if (rysuj5=1) then rysuj5=rysuj5+1;
      m=m+1;
      TYP='M';
      P0='';
      if (:MATTYPE = 0) then P0=P0||'Surowiec';
      else if (:MATTYPE = 1) then P0=P0||'Odpad';
      else if (:MATTYPE = 2) then P0=P0||'Produkt uboczny';
      if (m=matcnt) then OOST=1;

 --     if (opoziom = 1 ) then rysuj2 = oost ;
  --  else IF (opoziom = 2 ) then rysuj3 = oost ;
      suspend;
    end
    wciecia='';
    if (matcnt > 0) then opoziom = opoziom -1;
    select count(*)
      from prshtools PRT
        left join prtools on (prtools.symbol = PRT.symbol)
      where PRT.sheet=:SHEET and PRT.opermaster = :REF
      into toolcnt;
    if (toolcnt > 0 ) then opoziom = opoziom + 1;
    i=0;
    wciecia='';
    while (:i<=:opoziom) do begin
      wciecia=:wciecia||'    ';
      i=:i+1;
    end
    t=0;oost=0;
    for
      select prtools.tooltype, PRT.symbol, prtools.descript,
             PRT.quantity, prtools.miara, prt.hourfactor,
             prtools.price,''
        from prshtools PRT
        left join prtools on (prtools.symbol = PRT.symbol)
        where PRT.sheet=:SHEET and PRT.opermaster = :REF
        into TOOLTYPE, P1, P2,
             P3, P4, P5,
             P6, P7
    do begin
      if (rysuj1=1) then rysuj1=rysuj1+1;
      if (rysuj2=1) then rysuj2=rysuj2+1;
      if (rysuj3=1) then rysuj3=rysuj3+1;
      if (rysuj4=1) then rysuj4=rysuj4+1;
      if (rysuj5=1) then rysuj5=rysuj5+1;
      t=t+1;
      TYP='T';
      P0='';
      if (:tooltype is not null)then
        P0=P0||:tooltype;
      if (t=toolcnt) then oost=1;
    --IF (opoziom = 0 ) then rysuj1 = oost ;
  --   IF (opoziom = 1 ) then rysuj2 = oost ;
   -- else IF (opoziom = 2 ) then rysuj3 = oost ;
      suspend;
    end
    wciecia='';
    if (toolcnt > 0) then opoziom = opoziom -1;
  end
end^
SET TERM ; ^
