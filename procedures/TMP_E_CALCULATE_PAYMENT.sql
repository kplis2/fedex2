--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TMP_E_CALCULATE_PAYMENT(
      EPAYROL integer)
   as
declare variable employee integer;
declare variable x integer;
begin
  for
    select e.employee
      from eprempl e
      where e.epayroll = :epayrol
      into employee
  do begin
    execute procedure E_CALCULATE_PAYMENT (:employee,:epayrol)
      returning_values(x);
  end
end^
SET TERM ; ^
