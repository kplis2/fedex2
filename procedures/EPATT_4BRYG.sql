--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EPATT_4BRYG(
      CALENDAR integer,
      PYEAR integer,
      PERIODB smallint,
      ADD_HOLIDAY smallint)
   as
declare variable d date;
  declare variable workstart time;
  declare variable workend time;
  declare variable worktime integer;
  declare variable daykind smallint;
  declare variable tym smallint;
  declare variable patternyear integer;
  declare variable patternday integer;
begin
  --DS: dodawania wzorca wielobrygadowego
  if (pyear is null or pyear<0) then
    exception BAD_PARAMETER;
  -- usuwam stary wzorzec
  delete from ECALPATTDAYS where calendar=:calendar and pyear=:pyear;

  if (exists(select p.ref
    from ecalpattdays p
    where p.calendar = :calendar
      and p.pyear = :pyear - 1)) then
    patternyear = pyear - 1;
  --poki co z interfejsu nie mozna dodac lat wstecz, ale czesto klientom takie operacje sie robi,
  --aby mogli rejestrowac nieobecnosci archiwalne
  else if (exists(select p.ref
    from ecalpattdays p
    where p.calendar = :calendar
      and p.pyear = :pyear + 1)) then
    patternyear = pyear + 1;

  --na wypadek roku przestepnego
  patternday = (cast(patternyear||'-12-31' as date) - cast(patternyear||'-01-01' as date) + 1);
  --wyliczenie przesuniecia dni przy dodawaniu wzorca na nowy rok
  --aby zachowac ciaglosc na przelomie roku
  if (patternyear < pyear) then
    patternday = mod(patternday,periodb);
  else
    patternday = periodb - mod(patternday,periodb);
  d = (cast(pyear as varchar(4)) || '-01-01');
  TYM  = 0;
  while (periodb > TYM)
  do begin
    if (patternyear is not null) then
    begin
      select first 1 skip (:patternday) p.workstart, p.workend, p.daykind
        from ecalpattdays p
        where p.calendar = :calendar
          and p.pyear = :patternyear
          and p.pattkind = 2
        order by p.pdate
        into :workstart, :workend, :daykind;
      patternday = mod(patternday +1, periodb);
    end else
    begin
      DAYKIND = 1; -- dni sa robocze
      WORKSTART = '06:00';
      WORKEND = '14:00';
      WORKTIME = WORKEND - WORKSTART; /* ilosc sekund*/
    end
    TYM =TYM +1;
    insert into ECALPATTDAYS (CALENDAR,PDATE , DAYKIND, WORKSTART, WORKEND, WORKTIME, DESCRIPT, PATTKIND)
      values (:calendar, :d, :DAYKIND, :WORKSTART, :WORKEND, :WORKTIME, '', 2);
    d = d + 1; -- kolejny dzień
  end
  if (add_holiday = 1) then
    execute procedure add_holidays(calendar, pyear);
end^
SET TERM ; ^
