--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BLANK_EMPLCALENDAR_FROMDATE(
      EMPLOYEE integer)
  returns (
      FROMDATE timestamp)
   as
begin
/*MWr: Procedura wywolywana poprzez blank: BL_EMPLCALENDAR_FROMDATE; odpowiada
  za ustawienie 'Daty od', przy przypisywaniu nowego kalendarza pracownikowi,
  jako 'Data od' ostatniej umowy tego pracownika (PR30635) */

  select max(fromdate) as fromdate
    from emplcontracts where employee = :employee
    into: fromdate;

  suspend;
end^
SET TERM ; ^
