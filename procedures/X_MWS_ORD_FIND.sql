--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_ORD_FIND(
      IN_STRING STRING255,
      IN_OPER OPERATOR_ID)
  returns (
      REFMWSORD MWSORDS_ID,
      INFO STRING511)
   as
declare variable palgroup integer_id;
declare variable status status_id;
declare variable nazwa string60;
declare variable oper operator_id;
begin

  select first 1 o.ref, o.status, op.nazwa, op.ref
    from  dokumnag d
      join mwsacts a on (a.docid = d.ref)
      join mwsords o on (a.mwsord = o.ref)
      left join operator op on (o.operator = op.ref)
    where d.int_symbol = :IN_STRING
      and o.mwsordtypedest = 1 and o.status < 5
    into REFMWSORD, :status, :nazwa, :oper;

  if (REFMWSORD is null) then         -- jezeli nie ma zlecenia do zrobienia to moze jest zlecenie ktore bylo wykonane
    select first 1 o.ref, o.status, op.nazwa, op.ref
      from  dokumnag d
        join mwsacts a on (a.docid = d.ref)
        join mwsords o on (a.mwsord = o.ref)
        left join operator op on (o.operator = op.ref)
      where d.int_symbol = :IN_STRING
        and o.mwsordtypedest = 1 and o.status < 5
      into REFMWSORD, :status, :nazwa, :oper;

  if (REFMWSORD is null) then
  begin
    IN_STRING = replace(IN_STRING, '|', '');

    if (exists ( select FIRST 1 1
                   from NUMERICIS(:IN_STRING) WHERE ret=0))
      then begin
        REFMWSORD = 0;
        info= 'Brak zlecenia.';
        exit;
      end

    palgroup = cast(IN_STRING as integer);
    select first 1 o.ref, o.status, op.nazwa, op.ref
      from mwsords o                         
        left join operator op on (o.operator = op.ref)
      where o.palgroup = :palgroup and o.status > 0 and o.status < 5
        and o.mwsordtypedest = 11
      into REFMWSORD, :status, :nazwa, :oper;
  end

  if (REFMWSORD is not null) then
  begin
    if (status = 0) then
    begin
      REFMWSORD = 0;  --zwrocenie 0 blokuje realizacje
      info= 'Zlecenie nie jest zaakceptowane.';
    end
    else if (status = 1) then
    begin
      info = null;  --HH oczekuje refa i braku info
    end
    else if (status = 2) then
    begin                              
      if (oper=IN_OPER) then
      begin
        info= null;
      end
      else begin
        REFMWSORD = 0;  --zwrocenie 0 blokuje realizacje
        info= 'Zlecenie jest realizowane przez '''||:nazwa||'''.';
      end

    end
    else if (status = 3) then
    begin           
      REFMWSORD = 0;   --zwrocenie 0 blokuje realizacje
      info= 'Zlecenie zostlo zawieszone.';
    end
    else if (status = 4) then
    begin           
      REFMWSORD = 0;   --zwrocenie 0 blokuje realizacje
      info= 'Zlecenie jest zablokowane.';
    end
    else if (status = 5) then
    begin           
      REFMWSORD = 0;    --zwrocenie 0 blokuje realizacje
      info= 'Zlecenie zostalo zrealizowane.';
    end

  end
end^
SET TERM ; ^
