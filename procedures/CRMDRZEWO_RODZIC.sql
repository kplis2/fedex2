--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CRMDRZEWO_RODZIC(
      REFDZIECKA integer,
      REFRODZICA integer)
   as
declare variable refr integer;
begin
  select rodzic from crmdrzewo where ref = :refdziecka into :refr;
  if (:refr is null) then exit;
  else if (:refr = :refrodzica) then
    exception universal 'Operacja niedozwolona. Może dojść do zapętlenia drzewa CRM!';
  else if (:refr is not null) then begin
    execute procedure CRMDRZEWO_RODZIC(:refr, :refrodzica);
  end
end^
SET TERM ; ^
