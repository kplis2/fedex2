--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDULE_FREE_MACHINE_AVG(
      WORKPLACE varchar(20) CHARACTER SET UTF8                           ,
      MACHINE integer,
      MODE integer,
      STARTTIME timestamp)
  returns (
      RMACHINE integer,
      RSTART timestamp,
      REND timestamp,
      WORKTIME double precision)
   as
declare variable fromdate timestamp;
  declare variable todate timestamp;
  declare variable inactionfrom timestamp;
  declare variable inactionto timestamp;
  declare variable sql varchar(1024);
  declare variable operstart timestamp;
  declare variable operend timestamp;
  declare variable tmpend timestamp;
  declare variable gtype smallint;
begin
  -- mode 0 -> z uwzgldnieniem operacji produkcyjnych
  -- mode 1 -> tylko przypisania, bez operacji produkcyjnych

  sql = 'select mwp.prmachine, mwp.fromdate, mwp.todate';
  sql = sql || ' from prmachineswplaces mwp';
  sql = sql || ' where mwp.prworkplace = '''||:workplace||'''';
  if (:machine is not null) then
    sql = sql || ' and mwp.prmachine = '||:machine;
  sql = sql || ' order by mwp.fromdate';


  for
    execute statement :sql
      into :rmachine, :fromdate, :todate
  do begin
    -- operacje ktore juz zajely ta mazyne
    rstart = :fromdate;
    rend = :todate;
    operstart = null;
    operend = null;
    for
      select g.timefrom, g.timeto, g.gtype
        from prgantt g
        where g.prmachine = :rmachine
          and ((g.status < 3 and g.verified > 0) or g.gtype > 0)
          and (g.timefrom < :todate or :todate is null)
          and (g.timeto > :starttime)
          and :mode = 0
       order by g.timefrom
       into :operstart, :operend, gtype
    do begin
      if (:rstart  < :operstart and (:rstart < :todate or :todate is null)) then
      begin
        if (:operend < :todate or :todate is null) then
          rend = :operstart;
        else
          rend = :todate;

        -- awarie, remonty, wypadki
        inactionfrom = null;
        inactionto = null;
        tmpend = :rend;
        if (gtype = 0) then
        begin
          for
            select r.inactionfrom, r.inactionto
              from prmachrepairs r
              where r.machine = :rmachine
                and r.inactionfrom < :rend
                and r.inactionto > :rstart
              into :inactionfrom, :inactionto
          do begin
            if (:rstart < :inactionfrom and :rstart < :tmpend) then
            begin
              if (:inactionto < :tmpend) then
                rend = :inactionfrom;
              else
                rend = :inactionto;
              worktime = null;
              execute procedure prmachine_free_worktime(:rmachine, case when :rstart < :starttime then :starttime else :rstart end, :rend) returning_values :worktime;
              suspend;
            end 
            rstart = :inactionto;
          end
        end
        if (:inactionfrom is null and :inactionto is null) then  --brak remontow ipt
        begin
          worktime = null;
          execute procedure prmachine_free_worktime(:rmachine, case when :rstart < :starttime then :starttime else :rstart end, :rend) returning_values :worktime;
          suspend;
        end
        else if (:inactionto < :tmpend) then
        begin
          rend = :tmpend;
          worktime = null;
          execute procedure prmachine_free_worktime(:rmachine, case when :rstart < :starttime then :starttime else :rstart end, :rend) returning_values :worktime;
          suspend;
        end
      end
      rstart = :operend;
    end
    if (:operstart is not null and :todate is null) then
    begin
      rend = null;
      worktime = null;
      execute procedure prmachine_free_worktime(:rmachine, case when :rstart < :starttime then :starttime else :rstart end, :rend) returning_values :worktime;
      suspend;
    end

    if (:operstart is null and :operend is null) then -- brak przypisac
    begin
      --remonty bez maszyn
      inactionfrom = null;
      inactionto = null;
      tmpend = :rend;
      for
        select r.inactionfrom, r.inactionto
          from prmachrepairs r
          where r.machine = :rmachine
            and (r.inactionfrom < :rend or :rend is null)
            and r.inactionto > :rstart
          into :inactionfrom, :inactionto
      do begin
        if (:rstart < :inactionfrom and (:rstart < :tmpend or :tmpend is null)) then
        begin
          if (:inactionto < :tmpend or :tmpend is null) then
            rend = :inactionfrom;
          else
            rend = :inactionto;
          worktime = null;
          execute procedure prmachine_free_worktime(:rmachine, case when :rstart < :starttime then :starttime else :rstart end, :rend) returning_values :worktime;
          suspend;
        end
        rstart = :inactionto;
      end
      if (:inactionfrom is null and :inactionto is null) then
      begin --brak remontow ipt
        worktime = null;
        execute procedure prmachine_free_worktime(:rmachine, case when :rstart < :starttime then :starttime else :rstart end, :rend) returning_values :worktime;
        suspend;
      end
      else if (:inactionto < :tmpend or :tmpend is null) then
      begin
        rend = :tmpend;
        worktime = null;
        execute procedure prmachine_free_worktime(:rmachine, case when :rstart < :starttime then :starttime else :rstart end, :rend) returning_values :worktime;
        suspend;
      end
    end
  end
end^
SET TERM ; ^
