--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_FUN_DG011(
      PERIOD varchar(6) CHARACTER SET UTF8                           )
  returns (
      AMOUNT numeric(15,2))
   as
DECLARE VARIABLE obrot NUMERIC(14,2);
begin
--  exception test_break;
  AMOUNT = 0;
  execute procedure fk_fun_obrotybm(1, :PERIOD,'701%',1)
    returning_values :obrot;
  if (obrot is not null) then
    AMOUNT = AMOUNT + obrot;
  execute procedure fk_fun_obrotybm(1, :PERIOD,'703%',1)
    returning_values :obrot;
  if (obrot is not null) then
    AMOUNT = AMOUNT + obrot;
  execute procedure fk_fun_obrotybm(1, :PERIOD,'733%',1)
    returning_values :obrot;
  if (obrot is not null) then
    AMOUNT = AMOUNT + obrot;
  execute procedure fk_fun_obrotybm(1, :PERIOD,'740%',1)
    returning_values :obrot;
  if (obrot is not null) then
    AMOUNT = AMOUNT + obrot;
  AMOUNT = AMOUNT/1000;
  suspend;
end^
SET TERM ; ^
