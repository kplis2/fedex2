--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_OBROTYBMWN(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      ACCOUNTS ACCOUNT_ID)
  returns (
      AMOUNT numeric(15,2))
   as
declare variable FRVHDR integer;
declare variable FRPSN integer;
declare variable FRCOL integer;
declare variable COUNTRY_CURRENCY varchar(3);
declare variable COMPANY integer;
declare variable CACHEPARAMS varchar(255);
declare variable DDPARAMS varchar(255);
declare variable FRVERSION integer;
declare variable SACCOUNT ACCOUNT_ID;
declare variable FRHDR varchar(20);
declare variable FRVTYPE smallint;
declare variable PSYMBOL varchar(20);
begin
  if (frvpsn = 0) then exit;
  -- obroty WN danego miesiaca
  select frvhdr, frpsn, frcol, symbol
    from frvpsns
    where ref = :frvpsn
    into :frvhdr, :frpsn, :frcol, :psymbol;

  select company, frversion, frhdr, frvtype
    from frvhdrs
    where ref = :frvhdr
    into :company, :frversion, frhdr, :frvtype;
  cacheparams = period||';'||accounts||';'||company;
  ddparams = accounts;

  if ((not exists (select ref from frvdrilldown where functionname = 'OBROTYBMWN' and cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr)) or (frvtype = 1))

then
  begin
    accounts = replace(accounts,  '?',  '_') || '%';
    execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
      returning_values country_currency;
    select sum(T.debit)
      from turnovers T
        join accounting A on (T.accounting = A.ref)
      where A.currency = :country_currency and T.account like :accounts and T.period = :period
        and T.company = :company
      into :amount;
    for select distinct A.account
          from turnovers T
          join accounting A on (T.accounting = A.ref)
          where A.currency = :country_currency and T.account like :accounts and T.period = :period
            and T.company = :company
          into :saccount
      do begin
        insert into frchkaccounts (account, proced, frhdr, frpsn, period, psymbol)
          values (:saccount, 'OBROTYBMWN', :frhdr, :frpsn, :period, :psymbol);
      end
  end else
    select first 1 fvalue from frvdrilldown where functionname = 'OBROTYBMWN' and cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr
      into :amount;

  amount = coalesce(amount,0);
  insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
     values (:frvhdr, :frpsn, :frcol, 'OBROTYBMWN', :cacheparams, :ddparams, :amount, :frversion);
  suspend;
end^
SET TERM ; ^
