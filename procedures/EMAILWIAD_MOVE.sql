--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWIAD_MOVE(
      ID STRING255,
      TOFOLDER DEFFOLD_ID,
      MAILBOX INTEGER_ID,
      TOUID INTEGER_ID = null)
   as
declare variable stan        smallint;
declare variable oldfolderid deffold_id;
begin
    select stan, folder from emailwiad where id=:id and mailbox=:mailbox
    into stan,oldfolderid;
    if(stan=0) then
    begin
      update deffold d set d.nowewiadomosci = maxvalue(d.nowewiadomosci-1,0) where d.ref=:oldfolderid;
      update deffold d set d.nowewiadomosci = d.nowewiadomosci+1 where d.ref=:TOFOLDER;
    end
    else
    if(stan=1) then
    begin
      update deffold d set d.nieprzeczytane = maxvalue(d.nieprzeczytane-1,0) where d.ref=:oldfolderid;
      update deffold d set d.nieprzeczytane = d.nieprzeczytane+1 where d.ref=:TOFOLDER;
    end
    update emailwiad set folder=:TOFOLDER, uid = coalesce(:touid,uid) where id=:id;

end^
SET TERM ; ^
