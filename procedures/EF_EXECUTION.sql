--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_EXECUTION(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      DKIND integer,
      USERVALUE numeric(14,2) = null)
  returns (
      RET numeric(14,2))
   as
declare variable FROMDATE timestamp;
declare variable TODATE timestamp;
declare variable PTAX integer;
declare variable COST numeric(14,2);
declare variable ALLOWANCE numeric(14,2);
declare variable CTAX numeric(14,2);
declare variable FREEWAGE numeric(14,2);
declare variable NETTO numeric(14,2);
declare variable BRUTTO numeric(14,2);
declare variable ZUSBASE numeric(14,2);
declare variable NFZBASE numeric(14,2);
declare variable ZUS numeric(14,2);
declare variable ALIMENTY numeric(14,2);
declare variable SBENEFIT numeric(14,2);
declare variable TVAL numeric(14,2);
declare variable SBDAYS integer;
declare variable ART140U1P3 smallint;
declare variable LIMITKP4UCP smallint;
declare variable KOMORNIK numeric(14,2);
declare variable EVALUE numeric(14,2);
declare variable INITDEDUCT numeric(14,2);
declare variable DREF integer;
declare variable PRIORITY integer;
declare variable OLD_PRIORITY integer;
declare variable OLD_SEQUENCE integer;
declare variable PERSON integer;
declare variable "SEQUENCE" integer;
begin
--MWr: Personel - naliczenie potracen komorniczych

  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  update ecolldedinstals d set insvalue = 0
    where payroll = :payroll and employee = :employee and insvalue <> 0
      and d.colldeduction in (select t.ref from ecolldeductions t
                                left join employees e on (e.person = t.person)
                                where t.dkind = :dkind and e.ref = :employee);

  if (prefix = 'DELEPRPOS') then exit;

  delete from execution_tmp;

  if (uservalue = 0) then uservalue = null;

  ret = 0;
  for
    select d.ref, sequence, priority, art140u1p3, initdeduction, limitkp4ucp, e.person,
           case when ((abs(coalesce(actdeduction,0)) > abs(coalesce(deductionrate,0)) or coalesce(initdeduction,0) = 0)
             and coalesce(deductionrate,0) <> 0) then coalesce(deductionrate,0) else coalesce(actdeduction,0) end
    from ecolldeductions d
      left join employees e on (e.person = d.person and d.fromdate <= :todate)
    where e.ref = :employee
      and d.dkind = :dkind
      and (abs(d.deducted) < abs(coalesce(d.initdeduction,0)) or coalesce(d.initdeduction,0) = 0)
      and (sign(coalesce(actdeduction,0)) * sign(coalesce(deductionrate,0)) >= 0)
      and (d.todate is null or d.todate >= :fromdate)
      and d.ref is not null
    order by sequence, d.priority, d.fromdate
    into :dref, :sequence, :priority, :art140u1p3, :initdeduct, :limitkp4ucp, :person, :komornik
  do begin

    if (old_priority <> priority or old_sequence <> :sequence) then
    begin
      execute procedure execution_set_div(:old_sequence, :old_priority, :payroll, :employee)
        returning_values :evalue;
      ret = ret + evalue;

      if (old_sequence <> :sequence) then
      begin
      --sprawdzenie, czy mozna przejsc do obslugi potracen z nastepnej kolejnosci
        if (exists(select first 1 1 from ecolldeductions
              where person = :person
                and dkind = :dkind
                and sequence = :old_sequence
                and (todate is null or todate > :todate)
                and (abs(coalesce(actdeduction,0)) > 0
                  or (coalesce(initdeduction,0) = 0 and abs(coalesce(deductionrate,0)) >= 0)))
        ) then begin
          dref = 0;
          break;
        end
      end
    end

    if (sign(komornik) >= 0) then
    begin
      --okreslenie granicy potracenia :tval
      select brutto, sbdays, sbenefit, zus, cost, ptax, ctax, allowance,
             netto, alimenty, nfzbase, zusbase, freewage, ret
        from execution_get_tval(:employee, :payroll, :dkind, :art140u1p3,
               :limitkp4ucp, :brutto, :sbdays, :sbenefit, :zus, :cost, :ptax,
               :allowance, :netto, :alimenty, :nfzbase, :zusbase, :freewage)
        into :brutto, :sbdays, :sbenefit, :zus, :cost, :ptax, :ctax, :allowance,
             :netto, :alimenty, :nfzbase, :zusbase, :freewage, :tval;

      if (komornik = 0) then
      --gdy nie okreslono kwoty poczatkowej i raty
        komornik = tval;
      else if (komornik > 0 and komornik < 1 and initdeduct > 0) then
      --gdy potracenie okreslono procentowo
        komornik = komornik * ((brutto + sbenefit) - :zus - ctax);
      if (uservalue > 0) then
      --gdy uzytkownik zmodyfikowal wysokosc zajecia jako > 0; przy warunku (tval > uservalue) pilnowane beda limity KP
        tval = uservalue;
    end else begin
      --gdy zajecie komornicze typu zwrot (minusowe)
      if (uservalue > komornik) then tval = uservalue;
      else tval = komornik;
    end

    tval = tval - ret;
    insert into execution_tmp (dref, dvalue, tval, sequence, priority, art140u1p3, limitkp4ucp)
      values (:dref, :komornik, :tval, :sequence, :priority, :art140u1p3, :limitkp4ucp);

    old_priority = priority;
    old_sequence = sequence;
  end

  if (dref > 0) then
  begin
    execute procedure execution_set_div(:old_sequence, :old_priority, :payroll, :employee)
      returning_values :evalue;
    ret = ret + evalue;
  end

  delete from ecolldedinstals where payroll = :payroll and employee = :employee and insvalue = 0;

  if (dref is null) then ret = null;

  suspend;
end^
SET TERM ; ^
