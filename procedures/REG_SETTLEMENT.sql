--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REG_SETTLEMENT(
      DREF integer)
   as
declare variable ACCOUNT ACCOUNT_ID;
declare variable SETTLEMENT varchar(20);
declare variable DESCRIPT varchar(255);
declare variable PERIOD varchar(6);
declare variable OPENDATE timestamp;
declare variable PAYDAY timestamp;
declare variable DOCPAYDAY timestamp;
declare variable DEBIT numeric(15,2);
declare variable CREDIT numeric(15,2);
declare variable CURRDEBIT numeric(15,2);
declare variable CURRCREDIT numeric(15,2);
declare variable DOCDATE timestamp;
declare variable DOCREF integer;
declare variable DOCSYMBOL varchar(20);
declare variable DICTDEF integer;
declare variable DICTPOS integer;
declare variable ACCREF integer;
declare variable NAGFAK integer;
declare variable COUNTRY_CURRENCY varchar(3);
declare variable DECREE_CURRENCY varchar(3);
declare variable MULTICURR smallint;
declare variable RATE numeric(14,4);
declare variable STABLE varchar(40);
declare variable SREF integer;
declare variable RP_KONTOFK KONTO_ID;
declare variable RP_SYMBFAK varchar(20);
declare variable RP_REF integer;
declare variable DTRATEDIFF integer;
declare variable CTRATEDIFF integer;
declare variable STRANSDATE timestamp;
declare variable SETTLCREATE smallint;
declare variable BRANCH varchar(10);
declare variable FSOPERTYPE integer;
declare variable SSACCOUNT ACCOUNT_ID;
declare variable SSSETTLEMENT varchar(20);
declare variable SSDEBIT numeric(15,2);
declare variable SSCREDIT numeric(15,2);
declare variable SSCURRDEBIT numeric(15,2);
declare variable SSCURRCREDIT numeric(15,2);
declare variable SSDECREE_CURRENCY varchar(3);
declare variable BOTABLE varchar(40);
declare variable BOREF integer;
declare variable SSRATE numeric(14,4);
declare variable BANKACCOUNT integer;
declare variable PAYMENTMETH integer;
declare variable ODECREE integer;
declare variable COMPANY integer;
begin

  select D.account, D.settlement, D.descript, D.opendate, D.payday, D.soddzial,
      D.debit, D.credit, D.currency, D.rate, D.currdebit, D.currcredit, B.transdate,
      B.ref, B.symbol, D.accref, B.payday, D.stable, D.sref,
      D.dtratediff, D.ctratediff, D.stransdate, B.period, D.settlcreate,
      D.ssaccount, D.sssettlement, D.ssdebit, D.sscredit,
      D.sscurrdebit, D.sscurrcredit, D.sscurrency,
      B.otable, B.oref, D.ssrate, D.fsopertype, D.bankaccount, B.paymentmeth, D.company
    from decrees D
      join bkdocs B on (D.bkdoc = B.ref)
    where D.ref=:DREF
    into :account, :settlement, :descript, :opendate, :payday, :branch, :debit, :credit,
      :decree_currency, :rate, :currdebit, :currcredit, :docdate, :docref,
      :docsymbol, :accref, :docpayday, :stable, :sref, :dtratediff,
      :ctratediff, :stransdate, :period, :settlcreate,
      :ssaccount, :sssettlement, :ssdebit, :sscredit,
      :sscurrdebit, :sscurrcredit, :ssdecree_currency,
      :botable, :boref, :ssrate, :fsopertype, :bankaccount, :paymentmeth, :company;

  execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
    returning_values country_currency;

  if (stable = 'NAGFAK') then
    nagfak = sref;

  if ((ssaccount is null or ssaccount = '')
    and (sssettlement is null or sssettlement = '')) then
  begin
    select slodef, slopoz
      from rozrach
      where company = :company and kontofk = :account and symbfak = :settlement
      into :dictdef, :dictpos;

    if (country_currency<>decree_currency) then
      multicurr = 1;
    else begin
      multicurr = 0;
      currdebit = debit;
      currcredit = credit;
    end

    if (dictdef is null) then
    begin
      execute procedure get_dictdefpos4settlement(company, period, account)
        returning_values dictdef, dictpos;
      if (opendate is null) then
        opendate = stransdate;

      if (settlcreate = 1) then odecree = dref;
      insert into rozrach (company, kontofk, symbfak, opis, dataotw, dataplat, oddzial, slodef,
          slopoz, faktura, skad, waluta, walutowy, bankaccount, sposzap,
          fsopertype, odecree)
        values (:company, :account, :settlement, :descript, :opendate, :payday, :branch, :dictdef,
          :dictpos, :nagfak, 6, :decree_currency, :multicurr, :bankaccount, :paymentmeth,
          :fsopertype, :odecree);
    end else
    begin
      if (settlcreate = 1) then
      begin
        update rozrach set dataotw = :opendate, dataplat = :payday,
            faktura = :nagfak, oddzial = :branch, bankaccount = :bankaccount,
            opis = :descript, odecree = :dref, sposzap = :paymentmeth,
            fsopertype = :fsopertype
          where kontofk = :account and symbfak = :settlement and company = :company;
      end
    end

    if (stable is not null and stable<>'') then
    begin
      rp_ref = null;
      select ref, kontofk, symbfak from rozrachp
        where decrees=:dref
        into :rp_ref, :rp_kontofk, :rp_symbfak;
      if (rp_ref is null) then
        select ref, kontofk, symbfak from rozrachp
          where (stable=:stable and sref=:sref and symbfak = :settlement
            and kontofk = :account and decrees is null)
          into :rp_ref, :rp_kontofk, :rp_symbfak;
      if (rp_ref is not null) then
        update rozrachp set kontofk = :account, symbfak=:settlement, decrees=:dref,
            data=:docdate, winienzl=:debit, mazl=:credit, slodef=:dictdef,
            slopoz=:dictpos, bkdoc=:docref, winien=:currdebit,
            ma=:currcredit, kurs=:rate, dtratediff=:dtratediff,
            ctratediff=:ctratediff, tresc=:descript, fsopertype = :fsopertype,
            stransdate=:stransdate, faktura = coalesce(faktura, :nagfak)
          where ref=:rp_ref;
      else
        insert into rozrachp (company, kontofk, symbfak, decrees, data, winienzl, mazl,
            slodef, slopoz, operacja, tresc, bkdoc, skad, winien, ma, kurs,
            dtratediff, ctratediff, stable, sref, fsopertype, stransdate, faktura)
          values (:company, :account, :settlement, :DREF,:docdate, :debit, :credit,
            :dictdef, :dictpos, :docsymbol, :descript, :docref, 6, :currdebit,
            :currcredit, :rate, :dtratediff, :ctratediff, :stable, :sref, :fsopertype,
            :stransdate, :nagfak);
    end else
    begin
      rp_ref = null;
      select ref, kontofk, symbfak from rozrachp
        where decrees=:dref
        into :rp_ref, :rp_kontofk, :rp_symbfak;
      if (rp_ref is not null) then
        update rozrachp set kontofk = :account, symbfak=:settlement, decrees=:dref,
            winienzl=:debit, mazl=:credit, slodef=:dictdef,
            slopoz=:dictpos, operacja=:docsymbol, bkdoc=:docref,
            winien=:currdebit, ma=:currcredit, kurs=:rate, dtratediff=:dtratediff,
            ctratediff=:ctratediff, tresc=:descript, fsopertype = :fsopertype,
            stransdate=:stransdate, data=:docdate, faktura = coalesce(faktura, :nagfak)
          where ref=:rp_ref;
      else
        insert into rozrachp (company, kontofk, symbfak, decrees, winienzl, mazl,
          slodef, slopoz, operacja, tresc, bkdoc, skad, winien, ma, kurs,
          dtratediff, ctratediff, fsopertype, stransdate, data, faktura)
          values (:company, :account, :settlement, :DREF, :debit, :credit,
            :dictdef, :dictpos, :docsymbol, :descript, :docref, 6, :currdebit,
            :currcredit, :rate, :dtratediff, :ctratediff, :fsopertype,:stransdate,:docdate, :nagfak);
    end
  end else
  begin
    -- PW: analizujc dane uznalem ze nie ma potrzeby sprawdzania nulla
    if (ssaccount <> account or sssettlement <> settlement or settlement is null
      or ssdebit <> debit or sscredit <> credit or sscurrdebit <> currdebit
      or sscurrcredit <> currcredit or ssdecree_currency <> decree_currency
      or ssrate <> rate) then
    begin
      select slodef, slopoz
        from rozrach
        where kontofk = :account and symbfak = :settlement and company = :company
        into :dictdef, :dictpos;

      if (country_currency<>ssdecree_currency) then
        multicurr = 1;
      else begin
        multicurr = 0;
        currdebit = debit;
        currcredit = credit;
        sscurrdebit = ssdebit;
        sscurrcredit = sscredit;
      end

      if (dictdef is null) then
      begin
        execute procedure get_dictdefpos4settlement(company, period, account)
          returning_values dictdef, dictpos;
        if (opendate is null) then
          opendate = stransdate;
      end else
      begin
        if (settlcreate = 1) then
        begin
          update rozrach set dataotw=:opendate, dataplat=:payday, faktura=:nagfak,
              oddzial = :branch, bankaccount = :bankaccount,
              opis = :descript
            where kontofk = :account and symbfak = :settlement and company = :company;
        end
      end
      ssdebit = -ssdebit;
      sscredit = - sscredit;
      sscurrdebit = -sscurrdebit;
      sscurrcredit = -sscurrcredit;
      insert into rozrachp (company, kontofk, symbfak, decrees, winienzl, mazl,
        slodef, slopoz, operacja, tresc, bkdoc, skad, winien, ma, kurs,
        dtratediff, ctratediff, stable, sref, fsopertype, stransdate, data, faktura)
        values (:company, :ssaccount, :sssettlement, :DREF, :ssdebit, :sscredit,
          :dictdef, :dictpos, :docsymbol, :descript, :docref, 6, :sscurrdebit,
          :sscurrcredit, :ssrate, :dtratediff, :ctratediff, :stable, :sref, :fsopertype,:stransdate,:docdate,:nagfak);
      ssdebit = -ssdebit;
      sscredit = - sscredit;
      sscurrdebit = -sscurrdebit;
      sscurrcredit = -sscurrcredit;
      insert into rozrach (company, kontofk, symbfak, opis, dataotw, dataplat, oddzial, slodef,
        slopoz, faktura, skad, waluta, walutowy, bankaccount)
        values (:company, :account, :settlement, :descript, :opendate, :payday, :branch, :dictdef,
          :dictpos, :nagfak, 6, :decree_currency, :multicurr, :bankaccount);
      insert into rozrachp (company, kontofk, symbfak, decrees, winienzl, mazl,
        slodef, slopoz, operacja, tresc, bkdoc, skad, winien, ma, kurs,
        dtratediff, ctratediff, stable, sref, fsopertype,stransdate, data, faktura)
      values (:company, :account, :DREF, :stransdate, :debit, :credit,
        :dictdef, :dictpos, :docsymbol, :descript, :docref, 6, :currdebit,
        :currcredit, :rate, :dtratediff, :ctratediff, :stable, :sref, :fsopertype,:stransdate,:docdate,:nagfak);

    end else
    begin
      select slodef, slopoz
        from rozrach
        where kontofk = :ssaccount and symbfak = :sssettlement and company = :company
        into :dictdef, :dictpos;

      if (botable = 'NAGFAK') then begin
         update rozrachp set decrees = :dref, bkdoc = :docref
           where faktura = :boref and skad = 1;
      end
      else if (botable = 'RKRAPKAS') then begin
         update rozrachp p set decrees = :dref, bkdoc = :docref
           where p.stable = 'RKDOKPOZ' and p.sref = :sref and skad = 2;
--         update rozrachp p set decrees = :dref, bkdoc = :docref
--           where p.stable = 'RKDOKROZ' and p.sref = :sref and skad = 2;
      end
      else
      begin
        update rozrachp p set decrees = :DREF, bkdoc = :docref
          where p.symbfak = :SETTLEMENT and p.kontofk = :ACCOUNT
            and p.winienzl = :DEBIT and p.mazl = :CREDIT
            and p.slodef = :DICTDEF and p.slopoz = :DICTPOS;
        end
    end
  end
end^
SET TERM ; ^
