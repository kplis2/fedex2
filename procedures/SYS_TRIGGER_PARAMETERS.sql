--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_TRIGGER_PARAMETERS(
      NAZWA varchar(50) CHARACTER SET UTF8                           )
  returns (
      PARAMETERS blob sub_type 0 SEGMENT SIZE 80)
   as
declare variable trigger_type     integer;
declare variable trigger_relation char(31);
declare variable trigger_sequence integer;
declare variable trigger_inactive integer;
declare variable eol              varchar(2);
begin

/*
trigger_type:
  1  PRE_STORE (BEFORE INSERT)
  2  POST_STORE (AFTER INSERT)
  3  PRE_MODIFY (BEFORE UPDATE)
  4  POST_MODIFY  (AFTER UPDATE)
  5  PRE_ERASE  (BEFORE DELETE)
  6  POST_ERASE (AFTER DELETE)
  8192  CONNECT (ON CONNECT)
  8193  DISCONNECT (ON DISCONNECT)
  8194  TRANSACTION_START (ON TRANSACTION START)
  8195  TRANSACTION_COMMIT (ON TRANSACTION COMMIT)
  8196  TRANSACTION_ROLLBACK (ON TRANSACTION ROLLBACK)
*/

  parameters = '';
  eol = '
  ';

  if(exists (select first 1 1 from RDB$TRIGGERS where RDB$TRIGGER_NAME = :nazwa)) then
  begin
    select RDB$TRIGGER_TYPE, RDB$RELATION_NAME, RDB$TRIGGER_SEQUENCE, RDB$TRIGGER_INACTIVE
      from RDB$TRIGGERS
      where RDB$TRIGGER_NAME = :nazwa
      into :trigger_type, :trigger_relation, :trigger_sequence, :trigger_inactive;

      if(:trigger_type < 8192) then
        parameters = 'FOR ' || :trigger_relation;
      parameters = :parameters  || :eol;

      if(trigger_inactive = 1) then
        parameters = :parameters || 'INACTIVE ';
      else
        parameters = :parameters || 'ACTIVE ';

      if(trigger_type = 1) then
        parameters = :parameters || 'BEFORE INSERT ';
      else if(trigger_type = 2) then
        parameters = :parameters || 'AFTER INSERT ';
      else if(trigger_type = 3) then
        parameters = :parameters || 'BEFORE UPDATE ';
      else if(trigger_type = 4) then
        parameters = :parameters || 'AFTER UPDATE ';
      else if(trigger_type = 5) then
        parameters = :parameters || 'BEFORE DELETE ';
      else if(trigger_type = 6) then
        parameters = :parameters || 'AFTER DELETE ';
      else if(trigger_type = 17) then
        parameters = :parameters || 'BEFORE INSERT OR UPDATE ';
      else if(trigger_type = 18) then
        parameters = :parameters || 'AFTER INSERT OR UPDATE ';
      else if(trigger_type = 25) then
        parameters = :parameters || 'BEFORE INSERT OR DELETE ';
      else if(trigger_type = 26) then
        parameters = :parameters || 'AFTER INSERT OR DELETE ';
      else if(trigger_type = 27) then
        parameters = :parameters || 'BEFORE UPDATE OR DELETE ';
      else if(trigger_type = 28) then
        parameters = :parameters || 'AFTER UPDATE OR DELETE ';
      else if(trigger_type = 113) then
        parameters = :parameters || 'BEFORE INSERT OR UPDATE OR DELETE ';
      else if(trigger_type = 114) then
        parameters = :parameters || 'AFTER INSERT OR UPDATE OR DELETE ';
      else if(trigger_type = 8192) then
        parameters = :parameters || 'ON CONNECT ';
      else if(trigger_type = 8193) then
        parameters = :parameters || 'ON DISCONNECT ';
      else if(trigger_type = 8194) then
        parameters = :parameters || 'ON TRANSACTION START ';
      else if(trigger_type = 8195) then
        parameters = :parameters || 'ON TRANSACTION COMMIT ';
      else if(trigger_type = 8196) then
        parameters = :parameters || 'ON TRANSACTION ROLLBACK ';

      parameters = :parameters || 'POSITION ' || :trigger_sequence || ' ';
    end
  suspend;
end^
SET TERM ; ^
