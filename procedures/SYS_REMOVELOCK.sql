--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_REMOVELOCK(
      LSYMBOL varchar(40) CHARACTER SET UTF8                           )
   as
declare variable lcount integer;
begin
  /* Procedura na podstawie unikalnego klucza usuwa blokade danego zasobu. */
  select s.scount
    from sys_locks s
    where s.locksymbol = :lsymbol
    into :lcount;
  if (lcount > 1) then
  begin
    lcount = lcount - 1;
    update sys_locks s set s.scount = :lcount
      where s.locksymbol = :lsymbol;
  end else
    delete from SYS_LOCKS s
      where s.locksymbol = :lsymbol ;
end^
SET TERM ; ^
