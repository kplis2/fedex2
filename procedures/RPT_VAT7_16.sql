--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_VAT7_16(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PAR_K37 numeric(14,2),
      PAR_K41 numeric(14,2),
      PAR_K46 numeric(14,2),
      PAR_K50 numeric(14,2),
      PAR_K53 numeric(14,2),
      PAR_K56 numeric(14,2),
      PAR_K57 numeric(14,2),
      PAR_K58 numeric(14,2),
      PAR_K60 SMALLINT_ID,
      PAR_K61 SMALLINT_ID,
      PAR_K62 SMALLINT_ID,
      PAR_K63 SMALLINT_ID,
      PAR_K64 SMALLINT_ID,
      PAR_K65 SMALLINT_ID,
      PAR_K66 SMALLINT_ID,
      PAR_K67 SMALLINT_ID,
      PAR_K68 INTEGER_ID,
      DOCDATE STRING255,
      COMPANY INTEGER_ID,
      CORRECT SMALLINT_ID,
      USERFNAME STRING255,
      USERSNAME STRING255,
      GEN SMALLINT_ID = 1,
      EDECLARATIONREF EDECLARATIONS_ID = null)
  returns (
      WSP35 INTEGER_ID,
      K1 STRING255,
      K4 STRING255,
      K5 STRING255,
      K6 STRING255,
      K7 SMALLINT_ID,
      K8 SMALLINT_ID,
      K9 STRING255,
      K10 INTEGER_ID,
      K11 INTEGER_ID,
      K12 INTEGER_ID,
      K13 INTEGER_ID,
      K14 INTEGER_ID,
      K15 INTEGER_ID,
      K16 INTEGER_ID,
      K17 INTEGER_ID,
      K18 INTEGER_ID,
      K19 INTEGER_ID,
      K20 INTEGER_ID,
      K21 INTEGER_ID,
      K22 INTEGER_ID,
      K23 INTEGER_ID,
      K24 INTEGER_ID,
      K25 INTEGER_ID,
      K26 INTEGER_ID,
      K27 INTEGER_ID,
      K28 INTEGER_ID,
      K29 INTEGER_ID,
      K30 INTEGER_ID,
      K31 INTEGER_ID,
      K32 INTEGER_ID,
      K33 INTEGER_ID,
      K34 INTEGER_ID,
      K35 INTEGER_ID,
      K36 INTEGER_ID,
      K37 INTEGER_ID,
      K38 INTEGER_ID,
      K39 INTEGER_ID,
      K40 INTEGER_ID,
      K41 INTEGER_ID,
      K42 INTEGER_ID,
      K43 INTEGER_ID,
      K44 INTEGER_ID,
      K45 INTEGER_ID,
      K46 INTEGER_ID,
      K47 INTEGER_ID,
      K48 INTEGER_ID,
      K49 INTEGER_ID,
      K50 INTEGER_ID,
      K51 INTEGER_ID,
      K52 INTEGER_ID,
      K53 INTEGER_ID,
      K54 INTEGER_ID,
      K55 INTEGER_ID,
      K56 INTEGER_ID,
      K57 INTEGER_ID,
      K58 INTEGER_ID,
      K59 INTEGER_ID,
      K60 SMALLINT_ID,
      K61 SMALLINT_ID,
      K62 SMALLINT_ID,
      K63 SMALLINT_ID,
      K64 SMALLINT_ID,
      K65 SMALLINT_ID,
      K66 SMALLINT_ID,
      K67 SMALLINT_ID,
      K68 INTEGER_ID,
      K69 STRING255,
      K70 STRING255,
      K72 STRING255,
      K73 STRING255,
      USP STRING255)
   as
declare variable EDECLDEFREF EDECLDEFS_ID;
declare variable EDECPOSFIELD STRING40;
declare variable EDECPOSVAL STRING255;
begin

  --sprawdzenie, czy istnieją juz wygenerowane dane do wydruku
  select e.ref
    from edecldefs e
    where e.systemcode = 'VAT-7 (16)'
    into :edecldefref;

  -- jesli nie istnieje deklaracja dla tego okresu, company i tego edecldef
  -- lub zaznaczona jest korekta
  if (((not exists (select first 1 1
    from edeclarations ed
    where ed.company = :company
      and ed.period = :period
      and ed.edecldef = :edecldefref))
    or (:correct = 1)) and :edeclarationref is null ) then

  --generowanie danych do wydruku

  begin
    execute procedure EDECLARATION_ADD_HEADER(:edecldefref, :period)
      returning_values :edeclarationref;


    select d.wsp35, d.k1, d.k4, d.k5, d.k6, d.k7, d.k8, d.k9, d.k10, d.k11, d.k12, d.k13, d.k14, d.k15, d.k16, d.k17, d.k18,
        d.k19, d.k20, d.k21, d.k22, d.k23, d.k24, d.k25, d.k26, d.k27, d.k28, d.k29, d.k30, d.k31, d.k32, d.k33, d.k34, d.k35, d.k36,
        d.k37, d.k38, d.k39, d.k40, d.k41, d.k42, d.k43, d.k44, d.k45, d.k46, d.k47, d.k48, d.k49, d.k50, d.k51, d.k52, d.k53, d.k54,
        d.k55, d.k56, d.k57, d.k58, d.k59, d.k60, d.k61, d.k62, d.k63, d.k64,  d.k65, d.k66, d.k67, d.k68, d.k69, d.k70, d.k72, d.k73, d.USP
      from get_data_from_vat7_16(:PERIOD, :PAR_K37, :PAR_K41, :PAR_K46, :PAR_K50, :PAR_K53,
        :PAR_K56, :PAR_K57, :PAR_K58, :PAR_K60, :PAR_K61, :PAR_K62, :PAR_K63, :PAR_K64, :PAR_K65,
        :PAR_K66, :PAR_K67, :PAR_K68, :DOCDATE, :COMPANY, :CORRECT, :userfname, :usersname) d
      into wsp35, k1, k4, k5, k6, k7, k8, k9, k10, k11, k12, k13, k14, k15, k16, k17, k18,
        k19, k20, k21, k22, k23, k24, k25, k26, k27, k28, k29, k30, k31, k32, k33, k34, k35, k36,
        k37, k38, k39, k40, k41, k42, k43, k44, k45, k46, k47, k48, k49, k50, k51, k52, k53, k54,
        k55, k56, k57, k58, k59, k60, k61, k62, k63, k64,  k65, k66, k67, k68, k69, k70, k72, k73, USP;




     
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 0, :USP, 0, 'USP');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 1, :K1, 0, 'K1');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 4, :K4, 0, 'K4');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 5, :K5, 0, 'K5');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 6, :K6, 0, 'K6');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 7, :K7, 0, 'K7');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 8, :K8, 0, 'K8');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 9, :K9, 0, 'K9');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 10, :K10, 0, 'K10');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 11, :K11, 0, 'K11');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 12, :K12, 0, 'K12');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 13, :K13, 0, 'K13');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 14, :K14, 0, 'K14');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 15, :K15, 0, 'K15');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 16, :K16, 0, 'K16');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 17, :K17, 0, 'K17');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 18, :K18, 0, 'K18');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 19, :K19, 0, 'K19');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 20, :K20, 0, 'K20');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 21, :K21, 0, 'K21');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 22, :K22, 0, 'K22');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 23, :K23, 0, 'K23');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 24, :K24, 0, 'K24');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 25, :K25, 0, 'K25');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 26, :K26, 0, 'K26');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 27, :K27, 0, 'K27');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 28, :K28, 0, 'K28');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 29, :K29, 0, 'K29');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 30, :K30, 0, 'K30');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 31, :K31, 0, 'K31');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 32, :K32, 0, 'K32');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 33, :K33, 0, 'K33');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 34, :K34, 0, 'K34');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 35, :K35, 0, 'K35');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 36, :K36, 0, 'K36');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 37, :K37, 0, 'K37');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 38, :K38, 0, 'K38');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 39, :K39, 0, 'K39');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 40, :K40, 0, 'K40');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 41, :K41, 0, 'K41');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 42, :K42, 0, 'K42');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 43, :K43, 0, 'K43');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 44, :K44, 0, 'K44');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 45, :K45, 0, 'K45');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 46, :K46, 0, 'K46');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 47, :K47, 0, 'K47');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 48, :K48, 0, 'K48');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 49, :K49, 0, 'K49');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 50, :K50, 0, 'K50');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 51, :K51, 0, 'K51');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 52, :K52, 0, 'K52');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 53, :K53, 0, 'K53');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 54, :K54, 0, 'K54');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 55, :K55, 0, 'K55');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 56, :K56, 0, 'K56');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 57, :K57, 0, 'K57');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 58, :K58, 0, 'K58');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 59, :K59, 0, 'K59');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 60, :K60, 0, 'K60');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 61, :K61, 0, 'K61');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 62, :K62, 0, 'K62');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 63, :K63, 0, 'K63');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 64, :K64, 0, 'K64');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 65, :K65, 0, 'K65');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 66, :K66, 0, 'K66');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 67, :K67, 0, 'K67');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 68, :K68, 0, 'K68');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 69, :K69, 0, 'K69');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 70, :K70, 0, 'K70');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 72, :K72, 0, 'K72');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 73, :K73, 0, 'K73');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (:edeclarationref, 74, :WSP35, 0, 'WSP35');


  end

  --pobieranie danych
  if(:edeclarationref is null) then
  begin
    select ed.ref
    from edeclarations ed
    where ed.company = :company
      and ed.period = :period
      and ed.edecldef = :edecldefref
    into :edeclarationref;
  end

  for
    select ep.fieldsymbol, ep.pvalue
      from edeclarations e
        join edeclpos ep on (e.ref = ep.edeclaration)
      where e.ref = :edeclarationref
      into :edecposfield, :edecposval
  do begin
    if(:edecposfield = 'USP') then
      USP = :edecposval;
    else if(:edecposfield = 'K1') then
      K1 = :edecposval;
    else if(:edecposfield = 'K4') then
      K4 = :edecposval;
    else if(:edecposfield = 'K5') then
      K5 = :edecposval;
    else if(:edecposfield = 'K6') then
      K6 = :edecposval;
    else if(:edecposfield = 'K7') then
      K7 = :edecposval;
    else if(:edecposfield = 'K8') then
      K8 = :edecposval;
    else if(:edecposfield = 'K9') then
      K9 = :edecposval;
    else if(:edecposfield = 'K10') then
      K10 = :edecposval;
    else if(:edecposfield = 'K11') then
      K11 = :edecposval;
    else if(:edecposfield = 'K12') then
      K12 = :edecposval;
    else if(:edecposfield = 'K13') then
      K13 = :edecposval;
    else if(:edecposfield = 'K14') then
      K14 = :edecposval;
    else if(:edecposfield = 'K15') then
      K15 = :edecposval;
    else if(:edecposfield = 'K16') then
      K16 = :edecposval;
    else if(:edecposfield = 'K17') then
      K17 = :edecposval;
    else if(:edecposfield = 'K18') then
      K18 = :edecposval;
    else if(:edecposfield = 'K19') then
      K19 = :edecposval;
    else if(:edecposfield = 'K20') then
      K20 = :edecposval;
    else if(:edecposfield = 'K21') then
      K21 = :edecposval;
    else if(:edecposfield = 'K22') then
      K22 = :edecposval;
    else if(:edecposfield = 'K23') then
      K23 = :edecposval;
    else if(:edecposfield = 'K24') then
      K24 = :edecposval;
    else if(:edecposfield = 'K25') then
      K25 = :edecposval;
    else if(:edecposfield = 'K26') then
      K26 = :edecposval;
    else if(:edecposfield = 'K27') then
      K27 = :edecposval;
    else if(:edecposfield = 'K28') then
      K28 = :edecposval;
    else if(:edecposfield = 'K29') then
      K29 = :edecposval;
    else if(:edecposfield = 'K30') then
      K30 = :edecposval;
    else if(:edecposfield = 'K31') then
      K31 = :edecposval;
    else if(:edecposfield = 'K32') then
      K32 = :edecposval;
    else if(:edecposfield = 'K33') then
      K33 = :edecposval;
    else if(:edecposfield = 'K34') then
      K34 = :edecposval;
    else if(:edecposfield = 'K35') then
      K35 = :edecposval;
    else if(:edecposfield = 'K36') then
      K36 = :edecposval;
    else if(:edecposfield = 'K37') then
      K37 = :edecposval;
    else if(:edecposfield = 'K38') then
      K38 = :edecposval;
    else if(:edecposfield = 'K39') then
      K39 = :edecposval;
    else if(:edecposfield = 'K40') then
      K40 = :edecposval;
    else if(:edecposfield = 'K41') then
      K41 = :edecposval;
    else if(:edecposfield = 'K42') then
      K42 = :edecposval;
    else if(:edecposfield = 'K43') then
      K43 = :edecposval;
    else if(:edecposfield = 'K44') then
      K44 = :edecposval;
    else if(:edecposfield = 'K45') then
      K45 = :edecposval;
    else if(:edecposfield = 'K46') then
      K46 = :edecposval;
    else if(:edecposfield = 'K47') then
      K47 = :edecposval;
    else if(:edecposfield = 'K48') then
      K48 = :edecposval;
    else if(:edecposfield = 'K49') then
      K49 = :edecposval;
    else if(:edecposfield = 'K50') then
      K50 = :edecposval;
    else if(:edecposfield = 'K51') then
      K51 = :edecposval;
    else if(:edecposfield = 'K52') then
      K52 = :edecposval;
    else if(:edecposfield = 'K53') then
      K53 = :edecposval;
    else if(:edecposfield = 'K54') then
      K54 = :edecposval;
    else if(:edecposfield = 'K55') then
      K55 = :edecposval;
    else if(:edecposfield = 'K56') then
      K56 = :edecposval;
    else if(:edecposfield = 'K57') then
      K57 = :edecposval;
    else if(:edecposfield = 'K58') then
      K58 = :edecposval;
    else if(:edecposfield = 'K59') then
      K59 = :edecposval;
    else if(:edecposfield = 'K60') then
      K60 = :edecposval;
    else if(:edecposfield = 'K61') then
      K61 = :edecposval;
    else if(:edecposfield = 'K62') then
      K62 = :edecposval;
    else if(:edecposfield = 'K63') then
      K63 = :edecposval;
    else if(:edecposfield = 'K64') then
      K64 = :edecposval;
    else if(:edecposfield = 'K65') then
      K65 = :edecposval;
    else if(:edecposfield = 'K66') then
      K66 = :edecposval;
    else if(:edecposfield = 'K67') then
      K67 = :edecposval;
    else if(:edecposfield = 'K68') then
      K68 = :edecposval;
    else if(:edecposfield = 'K69') then
      K69 = :edecposval;
    else if(:edecposfield = 'K70') then
      K70 = :edecposval;
    else if(:edecposfield = 'K72') then
      K72 = :edecposval;
    else if(:edecposfield = 'K73') then
      K73 = :edecposval;
    else if(:edecposfield = 'WSP35') then
      WSP35 = :edecposval;
  end
  suspend;
end^
SET TERM ; ^
