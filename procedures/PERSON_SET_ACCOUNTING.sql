--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PERSON_SET_ACCOUNTING(
      PERSON integer,
      COMPANY integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           )
   as
declare variable slodef integer;
begin

--wywolanie EMPLOYEES_BIU_BLANK aby zaktualizowac dane osobowe/symbol na EMPLOYEES
  update employees set person = person where person = :person;

--aktulizacja opisu kont ksiegowych dla bizacego roku
  if (symbol is null) then
  begin
    select symbol from eperscompany
      where person = :person
        and company = :company
      into :symbol;
  end

  if (company is null or symbol is null) then
    exception universal 'Nieokreślone parametry wywołania';

  for
    select ref from slodef 
      where typ = 'PERSONS'
      into :slodef
  do
    execute procedure accounting_recalc_descript(:slodef, extract(year from current_timestamp(0)), company, symbol);

end^
SET TERM ; ^
