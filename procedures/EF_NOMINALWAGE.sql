--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_NOMINALWAGE(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      STYPE varchar(10) CHARACTER SET UTF8                           )
  returns (
      SUMWAGE numeric(14,4))
   as
declare variable FROMDATE date;
declare variable TODATE date;
declare variable SALARY numeric(14,2);
declare variable HSALARY numeric(14,2);
declare variable CADDSALARY numeric(14,2);
declare variable FUNCSALARY numeric(14,2);
declare variable EWORKHOURS numeric(14,2);
declare variable WORKHOURS numeric(14,2);
declare variable EFROMDATE date;
declare variable ETODATE date;
declare variable WAGE numeric(14,4);
declare variable MINFROMDATE date;
declare variable MAXFROMDATE date;
declare variable MAXTODATE date;
declare variable DAYS smallint;
declare variable STD_DAYS smallint;
declare variable ECALENDAR integer;
begin
  --DU: personel - do liczenia placy zasadniczej nominalnej pracownikowi

  sumwage = 0;

  select min(fromdate), max(fromdate)
    from employment
    where employee = :employee
    into :minfromdate, :maxfromdate;

  if (minfromdate is not null) then
  begin
    --ustalenie dat od-do na podstawie listy plac
    execute procedure efunc_prdates(payroll, 0)
      returning_values fromdate, todate;

    --jezeli wynagr. godz. lub miesieczne bierz rzeczywisty zakres czasu pracy
    if (stype = 'HSALARY') then
    begin
      select todate from employment
        where employee = :employee
          and fromdate = :maxfromdate
        into :maxtodate;

      if (fromdate < minfromdate) then
        fromdate = minfromdate;
      if (todate is not null and todate > maxtodate) then
        todate = maxtodate;
    end

    execute procedure emplcaldays_store('SUM', :employee, :fromdate, :todate)
      returning_values :workhours;

    --kalendarz moze nie byc uzupelniony na caly miesiac
    if (stype <> 'HSALARY') then
    begin
      execute procedure emplcaldays_store('COUNT', :employee, :fromdate, :todate, null)
        returning_values :days;

      select first 1 ecalendar from emplcaldays
        where cdate >= :fromdate and cdate <= :todate
          and employee = :employee
        order by cdate
        into :ecalendar;

      execute procedure ecaldays_store('COUNT', :ecalendar, :fromdate, :todate, null)
        returning_values :std_days;

      if (std_days <> days) then
        execute procedure ecaldays_store('SUM', :ecalendar, :fromdate, :todate)
          returning_values :workhours;
    end

    workhours = workhours / 3600.00;

    --okreslenie wynagr. nominalnego z uwzglednieniem zmian w przebiegu zatr.
    for
      select case when paymenttype in (0,4) then salary else 0 end,
             case when paymenttype = 1 then salary else 0 end,
             caddsalary, funcsalary, fromdate, todate
        from employment
        where employee = :employee
          and fromdate <= :todate
          and (todate is null or todate >= :fromdate)
        order by fromdate
        into :salary, :hsalary, :caddsalary, :funcsalary, :efromdate, :etodate
    do begin
      if (stype = 'SALARY') then
        wage = coalesce(salary,0);
      else if (stype = 'HSALARY') then
        wage = coalesce(hsalary,0);
      else if (stype = 'CADDSALARY') then
        wage = coalesce(caddsalary,0);
      else if (stype = 'FUNCSALARY') then
        wage = coalesce(funcsalary,0);
      else
        wage = 0;

      if (etodate is null or etodate > todate) then
        etodate = todate;
      if (efromdate < fromdate) then
        efromdate = fromdate;

      execute procedure emplcaldays_store('SUM', :employee, :efromdate, :etodate)
        returning_values :eworkhours;
      eworkhours = eworkhours /3600.00;

      if (workhours <> 0) then
        sumwage = sumwage + coalesce(wage * eworkhours / workhours,0);
    end
  end
  suspend;
end^
SET TERM ; ^
