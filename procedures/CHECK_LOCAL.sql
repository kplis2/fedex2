--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHECK_LOCAL(
      TABELA varchar(10) CHARACTER SET UTF8                           ,
      REFF integer)
  returns (
      LOCAL integer)
   as
declare variable cnt integer;
declare variable saktulocat varchar(255);
declare variable aktulocat integer;
begin
  if (exists(select first 1 1 from RP_SYNCDEF where TABELA = :tabela)) then begin
    execute procedure GETCONFIG('AKTULOCAT') returning_values :saktulocat;
    aktulocat = cast(:saktulocat as integer);
    cnt = :reff / 16;
    if ((:cnt * 16) > :reff) then cnt = :cnt - 1;
    if (:cnt < 0) then exception CHECKLOCAL_ERROR;
    if (((:reff - (:cnt * 16)) +1) = :aktulocat) then
      local = 1;
    else
      local = 0;
  end else
    local = 1;
end^
SET TERM ; ^
