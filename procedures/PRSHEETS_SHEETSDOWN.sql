--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHEETS_SHEETSDOWN(
      SHEET integer,
      PETLA integer)
  returns (
      SHEETCUR integer,
      GROSSQUANTITY numeric(14,4))
   as
declare variable LSHEET INTEGER;
declare variable LGROSSQUANTITY NUMERIC(14,4);
declare variable quantity numeric(14,4);
declare variable shortage numeric(14,4);
begin
  sheetcur = sheet;
  grossquantity = 1;
  suspend;
  petla = :petla - 1;
  if(:PETLA <= 0) then exit;
  for select pm.subsheet, pm.grossquantity, p.quantity
    from prshmat pm
    left join prsheets p on (pm.sheet = p.ref)
    where pm.sheet = :sheet and pm.subsheet is not null and pm.subsheet <> 0
    into :lsheet, :lgrossquantity, :quantity
  do begin
    for select sheetcur, grossquantity
      from prsheets_sheetsdown(:lsheet, :petla) into  :sheetcur, grossquantity do
    begin
--      select shortage from prsheets where ref = :sheetcur into :shortage;
--      if(shortage is null) then shortage = 0;
--      shortage = (100+ shortage) / 100;
      if(quantity = 0) then quantity = 1;
      grossquantity = grossquantity * lgrossquantity  / quantity;
--      grossquantity = grossquantity * shortage;
--if(grossquantity = 0.6655) then exception test_Break lgrossquantity||' '||quantity||' '||test;
      suspend;
    end
  end
end^
SET TERM ; ^
