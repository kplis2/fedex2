--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_FIELD_DDL(
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      CREATE_OR_ALTER smallint)
  returns (
      DDL blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           )
   as
declare variable FIELD_SOURCE varchar(80);
declare variable FIELD_TYPE_NAME varchar(80);
declare variable FIELD_LENGTH integer;
declare variable FIELD_PRECISION integer;
declare variable FIELD_SCALE integer;
declare variable FIELD_NULL_FLAG integer;
declare variable FIELD_SUB_TYPE integer;
declare variable FIELD_COLLATION_NAME varchar(80);
declare variable FIELD_CHARACTER_SET_NAME varchar(80);
declare variable FIELD_CHECK smallint;
declare variable FIELD_COLLATION_ID smallint;
declare variable FIELD_DEFAULT_SRC blob sub_type 1 segment size 80;
declare variable DOMAIN_DDL varchar(1024);
declare variable EOL varchar(2);
declare variable TABELA varchar(64);
declare variable POLE varchar(64);
declare variable POLE_ESCAPED varchar(64);
declare variable FIELD_NULL integer;
declare variable RELATION_NULL integer;
declare variable DEFAULT_COLLATE_NAME varchar(64);
begin
  eol ='
  ';
  tabela = substring(:nazwa from 1 for position('.' in :nazwa)-1);
  pole = substring(:nazwa from position('.' in :nazwa)+1);

  pole_escaped = (select outname from sys_quote_reserved(:pole));

  if (:create_or_alter <= 1 or create_or_alter >=3) then
  begin
    field_check = 0;
    if(create_or_alter = 1) then
      ddl = :pole_escaped || ' ';
    else if(create_or_alter = 0) then
      ddl = 'ALTER TABLE ' || :tabela || ' ADD ' || :pole_escaped || ' ';
    else
      ddl = '';
    select trim(r.rdb$field_source), coalesce(nullif(r.rdb$null_flag,0),f.rdb$null_flag),
         t.rdb$type_name, f.rdb$field_length, f.rdb$field_precision,
         f.rdb$field_scale, c.rdb$collation_name, ch.rdb$character_set_name,
         f.rdb$field_sub_type, r.rdb$default_source, f.rdb$null_flag, r.rdb$null_flag,
         trim(ch.rdb$default_collate_name)
      from rdb$relation_fields r
      join rdb$fields f on (r.rdb$field_source = f.rdb$field_name)
      join rdb$types t on (f.rdb$field_type = t.rdb$type)
      left join rdb$collations c on (c.rdb$collation_id = r.rdb$collation_id and c.rdb$character_set_id = f.rdb$character_set_id)
      left join rdb$character_sets ch on (ch.rdb$character_set_id = f.rdb$character_set_id)
      where r.rdb$field_name = trim(:pole) and r.rdb$relation_name = :tabela and t.rdb$field_name = 'RDB$FIELD_TYPE'
      into :field_source, :field_null_flag, :field_type_name, :field_length,
           :field_precision, :field_scale, :field_collation_name, :field_character_set_name,
           :field_sub_type, :field_default_src , :field_null, :relation_null, :default_collate_name;

    if(field_type_name is null) then
      exception universal('Błąd generowania DDL. Nieznany typ pola.');

    if(position('RDB$' in field_source) = 0) then
      ddl = :ddl || :field_source;
    else
    begin
      field_type_name = trim(:field_type_name);
      if(field_type_name = 'DATE') then
        ddl = :ddl || 'DATE';
      else if(field_type_name = 'DOUBLE') then
        ddl = :ddl || 'DOUBLE PRECISION';
      else if(field_type_name = 'FLOAT') then
        ddl = :ddl || 'FLOAT';
      else if(field_type_name = 'INT64' and field_sub_type = 0) then
        ddl = :ddl || 'BIGINT';
      else if(field_type_name = 'INT64' and field_sub_type = 1) then
        ddl = :ddl || 'NUMERIC(' || coalesce(:field_precision,18) || ',' || (-field_scale) || ')';
      else if(field_type_name = 'INT64' and field_sub_type = 2) then
        ddl = :ddl || 'DECIMAL(' || :field_precision || ',' || (-field_scale) || ')';
      else if(field_type_name = 'LONG' and field_sub_type = 0) then
        ddl = :ddl || 'INTEGER';
      else if(field_type_name = 'LONG' and field_sub_type = 1) then
        ddl = :ddl || 'NUMERIC(' || :field_precision || ',' || (-field_scale) || ')';
      else if(field_type_name = 'LONG' and field_sub_type = 2) then
        ddl = :ddl || 'DECIMAL(' || :field_precision || ',' || (-field_scale) || ')';
      else if(field_type_name = 'SHORT' and field_sub_type = 0) then
        ddl = :ddl || 'SMALLINT';
      else if(field_type_name = 'SHORT' and field_sub_type = 1) then
        ddl = :ddl || 'NUMERIC(' || :field_precision || ',' || (-field_scale) || ')';
      else if(field_type_name = 'SHORT' and field_sub_type = 2) then
        ddl = :ddl || 'DECIMAL(' || :field_precision || ',' || (-field_scale) || ')';
      else if(field_type_name = 'TEXT') then
        ddl = :ddl || 'CHAR(' || :field_length || ')';
      else if(field_type_name = 'TIMESTAMP') then
        ddl = :ddl || 'TIMESTAMP';
      else if(field_type_name = 'VARYING') then begin
        if(field_character_set_name is not null and
           field_character_set_name='UTF8') then
           field_length = field_length / 4;
        ddl = :ddl || 'VARCHAR(' || :field_length || ')';
      end
      else if(field_type_name = 'TIME') then
        ddl = :ddl || 'TIME';
      else if(field_type_name = 'BLOB') then
        ddl = :ddl || 'BLOB SUB_TYPE '||field_sub_type;
      if(field_character_set_name is not null) then
        ddl = :ddl || ' CHARACTER SET ' || :field_character_set_name;
    end

    if(field_default_src is not null) then
      ddl = :ddl || ' ' || :field_default_src;

    if(relation_null is not null) then
         ddl = :ddl || ' NOT NULL';

    if(position('RDB$' in field_source) > 0) then
    begin
      if(field_collation_name is not null and field_collation_name<>default_collate_name) then
        ddl = :ddl || ' COLLATE ' || :field_collation_name;
    end
  end else
  if(:create_or_alter = 2) then
  begin
    select r.rdb$field_source, r.rdb$null_flag, r.rdb$collation_id
      from rdb$relation_fields r
      where rdb$field_name = :pole and rdb$relation_name = :tabela
      into  :field_source, :field_null_flag, field_collation_id;
      field_source = substring(:field_source from 1 for position(' ', :field_source) - 1);
      ddl = 'update rdb$relation_fields set rdb$field_source = '''   || coalesce(:field_source, 'null')     ||
            ''', rdb$null_flag = '                                   || coalesce(:field_null_flag, 'null')  ||
            ', rdb$collation_id = '                                  || coalesce(:field_collation_id, 'null');
      ddl = :ddl || ' where rdb$field_name = ''' || trim(:pole) || ''' and rdb$relation_name = ''' || :tabela || '''';
  end
  if(create_or_alter = 0 or create_or_alter=2) then
    ddl = :ddl || ';';
  suspend;
end^
SET TERM ; ^
