--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PR_PLANS(
      PLANSREF integer,
      DATAOD timestamp,
      DATADO timestamp,
      CHBOX1 integer,
      CHBOX2 integer,
      CHBOX3 integer,
      CHBOX4 integer,
      MASKA varchar(20) CHARACTER SET UTF8                           )
  returns (
      VALKEY varchar(60) CHARACTER SET UTF8                           ,
      VAL1 varchar(60) CHARACTER SET UTF8                           ,
      VAL2 varchar(60) CHARACTER SET UTF8                           ,
      VAL3 varchar(60) CHARACTER SET UTF8                           ,
      VAL4 varchar(60) CHARACTER SET UTF8                           ,
      VAL5 varchar(60) CHARACTER SET UTF8                           ,
      VAL6 varchar(60) CHARACTER SET UTF8                           ,
      VAL7 varchar(60) CHARACTER SET UTF8                           ,
      VAL8 varchar(60) CHARACTER SET UTF8                           ,
      VAL9 varchar(60) CHARACTER SET UTF8                           ,
      VAL10 varchar(60) CHARACTER SET UTF8                           ,
      VAL11 varchar(60) CHARACTER SET UTF8                           ,
      VAL12 varchar(60) CHARACTER SET UTF8                           ,
      VAL13 varchar(60) CHARACTER SET UTF8                           ,
      VAL14 varchar(60) CHARACTER SET UTF8                           ,
      VAL15 varchar(60) CHARACTER SET UTF8                           ,
      VAL16 varchar(60) CHARACTER SET UTF8                           ,
      VAL17 varchar(60) CHARACTER SET UTF8                           ,
      VAL18 varchar(60) CHARACTER SET UTF8                           ,
      VAL19 varchar(60) CHARACTER SET UTF8                           ,
      VAL20 varchar(60) CHARACTER SET UTF8                           ,
      VAL21 varchar(60) CHARACTER SET UTF8                           ,
      VAL22 varchar(60) CHARACTER SET UTF8                           ,
      VAL23 varchar(60) CHARACTER SET UTF8                           ,
      VAL24 varchar(60) CHARACTER SET UTF8                           )
   as
declare variable firstrow int;
declare variable colnum int;
declare variable tempplanval varchar(60);
declare variable temprealval varchar(60);
declare variable tempdyspval varchar(60);
declare variable tempplancur numeric(14,2);
declare variable temprealcur numeric(14,2);
declare variable tempdyspcur numeric(14,2);
declare variable tempporcur numeric(14,2);
declare variable tempval varchar(60);
declare variable chboxstates int;
declare variable chboxquantity int;
declare variable valrow int;
declare variable valcol int;
declare variable howmuch int;
begin
VAL1 = ''; VAL2 = ''; VAL3 = ''; VAL4 = ''; VAL5 = ''; VAL6 = ''; VAL7 = '';
VAL8 = ''; VAL9 = ''; VAL10 = ''; VAL11 = ''; VAL12 = ''; VAL13 = ''; VAL14 = '';
VAL15 = ''; VAL16 = ''; VAL17 = ''; VAL18 = ''; VAL19 = ''; VAL20 = ''; VAL21 = '';
VAL22 = ''; VAL23 = ''; VAL24 = '';
  chboxstates = 0; chboxquantity = 0;
  firstrow = 1;
  if (chbox1 = 1) then chboxstates = chboxstates + 1;
  if (chbox2 = 1) then chboxstates = chboxstates + 2;
  if (chbox3 = 1) then chboxstates = chboxstates + 4;
  if (chbox4 = 1) then chboxstates = chboxstates + 8;
  chboxquantity = chboxstates;
  colnum = 0;
  howmuch = 2;
  while (:howmuch > 0) do begin
    if (firstrow = 1) then begin
      tempval = '';
      for select planscol.bdata
        from planscol
        where planscol.plans = :plansref --plans.ref
        order by planscol.bdata
        into :tempval
      do begin
        chboxstates = chboxquantity;
        while (chboxstates > 0) do begin
          if (chboxstates = 1) then      begin tempval = substring (:tempval from 1 for 10)||' Plan';chboxstates = chboxstates - 1; end
          else if (chboxstates = 2) then begin tempval = substring (:tempval from 1 for 10)||' Dysp';chboxstates = chboxstates - 2; end
          else if (chboxstates = 4) then begin tempval = substring (:tempval from 1 for 10)||' Real';chboxstates = chboxstates - 4; end
          else if (chboxstates = 8) then begin tempval = substring (:tempval from 1 for 10)||' Por';chboxstates = chboxstates - 8; end
          else if (chboxstates = 3) then begin tempval = substring (:tempval from 1 for 10)||' Plan';chboxstates = chboxstates - 1; end
          else if (chboxstates = 5) then begin tempval = substring (:tempval from 1 for 10)||' Plan';chboxstates = chboxstates - 1; end
          else if (chboxstates = 9) then begin tempval = substring (:tempval from 1 for 10)||' Plan';chboxstates = chboxstates - 1; end
          else if (chboxstates = 6) then begin tempval = substring (:tempval from 1 for 10)||' Dysp';chboxstates = chboxstates - 2; end
          else if (chboxstates = 10) then begin tempval = substring (:tempval from 1 for 10)||' Dysp';chboxstates = chboxstates - 2; end
          else if (chboxstates = 12) then begin tempval = substring (:tempval from 1 for 10)||' Real';chboxstates = chboxstates - 4; end
          else if (chboxstates = 7) then begin tempval = substring (:tempval from 1 for 10)||' Plan';chboxstates = chboxstates - 1; end
          else if (chboxstates = 11) then begin tempval = substring (:tempval from 1 for 10)||' Plan';chboxstates = chboxstates - 1; end
          else if (chboxstates = 14) then begin tempval = substring (:tempval from 1 for 10)||' Dysp';chboxstates = chboxstates - 2; end
          else if (chboxstates = 13) then begin tempval = substring (:tempval from 1 for 10)||' Plan';chboxstates = chboxstates - 1; end
          else if (chboxstates = 15) then begin tempval = substring (:tempval from 1 for 10)||' Plan';chboxstates = chboxstates - 1; end
          colnum = colnum +1;
          if (colnum = 1) then val1 = :tempval;
          else if (colnum = 2) then val2 = :tempval;
          else if (colnum = 3) then val3 = :tempval;
          else if (colnum = 4) then val4 = :tempval;
          else if (colnum = 5) then val5 = :tempval;
          else if (colnum = 6) then val6 = :tempval;
          else if (colnum = 7) then val7 = :tempval;
          else if (colnum = 8) then val8 = :tempval;
          else if (colnum = 9) then val9 = :tempval;
          else if (colnum = 10) then val10 = :tempval;
          else if (colnum = 11) then val11 = :tempval;
          else if (colnum = 12) then val12 = :tempval;
          else if (colnum = 13) then val13 = :tempval;
          else if (colnum = 14) then val14 = :tempval;
          else if (colnum = 15) then val15 = :tempval;
          else if (colnum = 16) then val16 = :tempval;
          else if (colnum = 17) then val17 = :tempval;
          else if (colnum = 18) then val18 = :tempval;
          else if (colnum = 19) then val19 = :tempval;
          else if (colnum = 20) then val20 = :tempval;
          else if (colnum = 21) then val21 = :tempval;
          else if (colnum = 22) then val22 = :tempval;
          else if (colnum = 23) then val23 = :tempval;
          else if (colnum = 24) then val24 = :tempval;
        end
      end
      valkey = 'Klucz';
      firstrow = 0;
      suspend;

    end else if (firstrow = 0) then begin
/* wiersze z danymi */

      for select plansrow.key1, plansrow.ref
        from plansrow
        where (plansrow.key1 like :maska or :maska is NULL) and plansrow.plans = :plansref --plans.ref
        into  :tempval, :valrow
      do begin
        valkey = :tempval;
        colnum = 0;

/* kolumny */
        for select planscol.ref
          from planscol
          where planscol.plans = :plansref --plans.ref
            order by planscol.bdata
          into :valcol
        do begin

          for select plansval.planval, plansval.realval, plansval.dyspval
            from plansval
            where plansval.planscol = :valcol and plansval.plansrow = :valrow and plansval.plans = :plansref /*plan*/
            into : tempplancur, temprealcur, tempdyspcur
          do begin
            tempplanval = tempplancur;
            temprealval = temprealcur;
            tempdyspval = tempdyspcur;
            chboxstates = :chboxquantity;
            while (chboxstates > 0) do begin
              tempval='';
              if (chboxstates = 1) then begin tempval = :tempplanval; chboxstates = chboxstates - 1; end
              else if (chboxstates = 2) then begin tempval = :temprealval; chboxstates = chboxstates - 2; end
              else if (chboxstates = 4) then begin tempval = :tempdyspval; chboxstates = chboxstates - 4; end
              else if (chboxstates = 8) then begin
                /* Jesli wartosc ma byc w % to poniżej * 100 */
                if (:tempplanval <> 0) then begin
                  tempporcur = (:temprealcur / :tempplancur) * 100;
                  tempval = :tempporcur;
                end else tempval = '';
                chboxstates = chboxstates - 8;
              end
              else if (chboxstates = 3) then begin tempval = :tempplanval; chboxstates = chboxstates - 1; end
              else if (chboxstates = 5) then begin tempval = :tempplanval; chboxstates = chboxstates - 1; end
              else if (chboxstates = 9) then begin tempval = :tempplanval; chboxstates = chboxstates - 1; end
              else if (chboxstates = 6) then begin tempval = :temprealval; chboxstates = chboxstates - 2; end
              else if (chboxstates = 10) then begin tempval = :temprealval; chboxstates = chboxstates - 2; end
              else if (chboxstates = 12) then begin tempval = :tempdyspval; chboxstates = chboxstates - 4; end
              else if (chboxstates = 7) then begin tempval = :tempplanval; chboxstates = chboxstates - 1; end
              else if (chboxstates = 11) then begin tempval = :tempplanval; chboxstates = chboxstates - 1; end
              else if (chboxstates = 14) then begin tempval = :temprealval; chboxstates = chboxstates - 2; end
              else if (chboxstates = 13) then begin tempval = :tempplanval; chboxstates = chboxstates - 1; end
              else if (chboxstates = 15) then begin tempval = :tempplanval; chboxstates = chboxstates - 1; end
              colnum = colnum +1;
              if (colnum = 1) then val1 = :tempval;
              else if (colnum = 2) then val2 = :tempval;
              else if (colnum = 3) then val3 = :tempval;
              else if (colnum = 4) then val4 = :tempval;
              else if (colnum = 5) then val5 = :tempval;
              else if (colnum = 6) then val6 = :tempval;
              else if (colnum = 7) then val7 = :tempval;
              else if (colnum = 8) then val8 = :tempval;
              else if (colnum = 9) then val9 = :tempval;
              else if (colnum = 10) then val10 = :tempval;
              else if (colnum = 11) then val11 = :tempval;
              else if (colnum = 12) then val12 = :tempval;
              else if (colnum = 13) then val13 = :tempval;
              else if (colnum = 14) then val14 = :tempval;
              else if (colnum = 15) then val15 = :tempval;
              else if (colnum = 16) then val16 = :tempval;
              else if (colnum = 17) then val17 = :tempval;
              else if (colnum = 18) then val18 = :tempval;
              else if (colnum = 19) then val19 = :tempval;
              else if (colnum = 20) then val20 = :tempval;
              else if (colnum = 21) then val21 = :tempval;
              else if (colnum = 22) then val22 = :tempval;
              else if (colnum = 23) then val23 = :tempval;
              else if (colnum = 24) then val24 = :tempval;
            end
          end
        end
        suspend;
      end
    end
  howmuch = :howmuch - 1;
  end
end^
SET TERM ; ^
