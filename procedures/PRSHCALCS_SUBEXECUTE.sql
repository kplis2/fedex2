--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHCALCS_SUBEXECUTE(
      PRCALCCOL integer)
  returns (
      RET numeric(16,6))
   as
declare variable expr varchar(1024);
declare variable key1 integer;
declare variable key2 integer;
declare variable prefix varchar(255);
declare variable modified smallint;
declare variable cvalue numeric(14,2);
declare variable excluded smallint;
declare variable calctype smallint;
begin
  -- pobieramy parametry do liczenia
  select expr, key1, key2, prefix, modified, cvalue, excluded, calctype
  from prcalccols where ref=:prcalccol
  into :expr, :key1, :key2, :prefix, :modified, :cvalue, :excluded, :calctype;

  -- wyliczamy wyrazenie
  if(:expr is null) then expr = '';
  ret = 0;
  if(:expr<>'') then begin
    execute procedure statement_parse(:expr,:key1,:key2,:prefix) returning_values :ret;
    if(:ret is null) then ret = 0;
  end else begin
    select sum(coalesce(CVALUE,0)) from PRCALCCOLS where PARENT=:prcalccol and CALCTYPE=0 and calctoparent = 1 into :ret;
    if(:ret is null) then ret = 0;
  end

  -- jesli wiersz jest wykluczony to zwracamy 0
  if(:excluded=1) then begin
    ret = 0;
    exit;
  end

  -- jesli wiersz jest zmodyfikowany recznie to ignorujemy wynik wyrazenia
  if(:modified=1) then begin
    ret = :cvalue;
    exit;
  end

end^
SET TERM ; ^
