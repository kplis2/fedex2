--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_SIGNATURE(
      REF integer,
      SIGNATURE varchar(255) CHARACTER SET UTF8                           )
  returns (
      STATUS integer)
   as
declare variable sec varchar(255);
declare variable id varchar(255);
declare variable id1 varchar(255);
declare variable pos integer;
begin
  status = 0;
  select section,ident from s_appini where ref=:ref into :sec,:id;
  /* sprawdz czy dana sekcja ma wpis Module*/
  if(exists(select ref from s_appini where section=:sec and ident='Signature' and val like '%'||:signature||'%')) then begin
    status = 1;
    suspend;
    exit;
  end
  /* sprawdz czy dany element menu ma atrybut ForModule*/
  pos = position(':' in :id);
  if(pos>0) then id1 = substring(:id from 1 for :pos);
  if(exists(select ref from s_appini where section=:sec and ident=:id1||'Signature' and val like '%'||:signature||'%')) then begin
    status = 1;
    suspend;
  end
end^
SET TERM ; ^
