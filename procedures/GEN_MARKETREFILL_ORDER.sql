--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GEN_MARKETREFILL_ORDER(
      WH DEFMAGAZ_ID,
      REFILLMWSORDTYPE MWSORDTYPES_ID,
      VERS WERSJE_ID,
      QUANTITY QUANTITY_MWS,
      MAXCNT INTEGER_ID)
  returns (
      GENQ QUANTITY_MWS)
   as
declare variable REFMWSORD integer;
declare variable DTREF integer;
declare variable AREF integer;
declare variable CNT integer;
declare variable ADDED smallint;
declare variable PERIOD varchar(6);
declare variable PALGROUP integer;
declare variable MWSCONSTLOC integer;
declare variable MWSORDTYPES STRING10;
declare variable BUFFORS STRING100;
declare variable TAKEFROMZEROS STRING100;
declare variable BUFFOR SMALLINT_ID;
declare variable TAKEFROMZERO SMALLINT_ID;
declare variable GOOD KTM_ID;
declare variable QUANTITYONLOCATION QUANTITY_MWS;
declare variable TOINSERT QUANTITY_MWS;
declare variable LOCSEC SMALLINT_ID;
declare variable MWSPALLOCP MWSPALLOCS_ID;
declare variable MWSCONSTLOCP MWSCONSTLOCS_ID;
declare variable WHAREAG WHAREAS_ID;
declare variable MAGBREAK SMALLINT_ID;
declare variable MAXNUMBER INTEGER_ID;
declare variable planmwsconstloc mwsconstlocs_id;
begin
  genq = 0;
  execute procedure get_config('MWSTAKEFROMBUFFOR',2) returning_values :buffors;
  if (:buffors is null or: buffors = '') then
    buffor = 0;
  else
    buffor = cast(:buffors as smallint);
  execute procedure get_config('MWSTAKEFROMZERO',2) returning_values :takefromzeros;
  if (:takefromzeros is null or :takefromzeros = '') then
    takefromzero = 0;
  else
    takefromzero = cast(:takefromzeros as smallint);
  if (coalesce(maxcnt,0) = 0) then maxcnt = 10;
  select first 1 o.ref, count(a.ref)
    from mwsords o
      left join mwsacts a on (a.mwsord = o.ref)
    where o.mwsordtype = :refillmwsordtype and o.status in (0,1) and o.wh = :wh
    group by o.ref
    having count(a.ref) < :maxcnt
    order by count(a.ref)
    into refmwsord, cnt;
  if (cnt is null) then cnt = 0;
  maxnumber = cnt;
  if (refmwsord is null) then
  begin
    execute procedure gen_ref('MWSORDS') returning_values refmwsord;
    select okres from datatookres(current_date,0,0,0,0) into period;
    insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator, priority,
        description, wh,  regtime, timestartdecl, branch, period,
        flags, docsymbs, cor, rec, status, shippingarea, SHIPPINGTYPE, takefullpal,
        slodef, slopoz, slokod, palquantity, repal)
      values (:refmwsord, :refillmwsordtype, 0, 'U', null, null, null, 1,
          '', :wh, current_timestamp(0), null, 'CENTRALA', :period,
          '', '', 0, 0, 0, null, null, 0,
          null, null, 'GEN_MARKETREFILL_ORDER', null, 0);
  end
  palgroup = null;
  select t.symbol
    from mwsordtypes t
    where t.ref = :refillmwsordtype
    into mwsordtypes;
  select c.ref
    from mwsconstlocs c where c.symbol = 'X'||:wh||:mwsordtypes and c.wh = :wh and c.locdest = 10
    into mwsconstloc;
  if (mwsconstloc is null) then
  begin
    execute procedure gen_ref('MWSCONSTLOCS') returning_values mwsconstloc;
    insert into mwsconstlocs (ref, symbol, wh, whsec, wharea, stocktaking, goodssellav, goodsav, act, locdest)
      select first 1 :mwsconstloc, 'X'||:wh||:mwsordtypes, wh, whsec, wharea, stocktaking, goodssellav, goodsav, act, 10
        from mwsconstlocs c1 where c1.wh = :wh and c1.locdest = 4;
  end
  palgroup = null;
  select first 1 mwspallocl from mwsacts a where a.mwsord = :refmwsord
    into palgroup;
  if (palgroup is null) then
    select first 1 ref from mwspallocs p where p.mwsconstloc = :mwsconstloc
      into palgroup;
  if (palgroup is null) then
  begin
    execute procedure gen_ref('MWSPALLOCS') returning_values palgroup;
    insert into mwspallocs (ref, symbol, mwsconstloc)
      values (:palgroup, :palgroup, :mwsconstloc);
  end
  added = 0;
  while (quantity > 0)
  do begin
    select first 1 m.good, m.quantity - m.blocked, ml.whsec,
        m.mwspalloc, m.mwsconstloc, m.wharea, t.magbreak
      from  mwsstock m
        left join mwsconstlocs ml on (ml.ref = m.mwsconstloc)
        left join whsecs ws on (ml.whsec = ws.ref)
        left join towary t on (t.ktm = m.good)
      where m.wh = :wh and m.vers = :vers and ws.deliveryarea = 2
        and ml.act > 0 and m.quantity - m.blocked > 0 and coalesce(ws.autogoodsrefill,0) = 0
        and ((ml.goodsav in (1,2) and :buffor = 0)
          or (:buffor = 1 and ml.goodsav in (0,1,2))
          or (:takefromzero = 1 and ml.goodsav = 4))
      order by case when ml.whsec in (6,7) then 0 else 1 end,
        ml.goodsav, m.quantity - m.blocked, ml.mwsstandlevelnumber
      into :good, :quantityonlocation, :locsec,
          :mwspallocp, :mwsconstlocp, :whareag, :magbreak;
    if (mwsconstlocp is null) then
    begin
      quantity = 0;
    end else
    begin
      if (:quantityonlocation >= :quantity) then
        toinsert = :quantity;
      else
        toinsert = :quantityonlocation;
      if (:toinsert > 0) then
      begin
        quantity = :quantity - :toinsert;
        genq = :genq + :toinsert;
        maxnumber = :maxnumber + 1;
        planmwsconstloc = null;
        select s.mwsconstloc
          from mwsconstlocsymbs s
            left join mwsconstlocs c on c.ref = s.mwsconstloc
            left join whsecs w on w.ref = c.whsec
          where s.vers = :vers and s.avmode = 1 and s.wh = :wh and w.autogoodsrefill = 1 and w.refillmwsordtype = :refillmwsordtype
          into planmwsconstloc;
        if (added = 0) then added = 1;
        insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
            mwsconstlocl, mwspallocl, docid, docposid, doctype, wh, wharea, whsec,
            regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc, plus,
            whareal, whareap, wharealogl, number, disttogo, palgroup, autogen, planmwsconstlocl)
          values (:refmwsord, 1, 0, :good, :vers, :toinsert, :mwsconstlocp, :mwspallocp,
              :mwsconstloc, :palgroup, null, null, 'R', :wh, :whareag, :locsec,
              current_timestamp, null, null, null, null, null, null, 0, 0, 1,
              null, null, null, :maxnumber, 0, null, 1, :planmwsconstloc);
      end
      mwsconstlocp = null;
    end
  end
  if (added = 1) then
  begin
    execute procedure xk_mws_mwsord_route_opt(:refmwsord);
    update mwsords set status = 1 where ref = :refmwsord and status = 0;
  end
end^
SET TERM ; ^
