--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_ROZRACH(
      COMPANY integer,
      TRYB integer,
      RODZAJ smallint,
      OGRSLODEF integer,
      OGRSLOPOZ integer,
      ZAKRES smallint,
      STRONA smallint,
      KONTO KONTO_ID,
      ODDATY timestamp,
      DODATY timestamp,
      ODDZIAL varchar(20) CHARACTER SET UTF8                           ,
      ILDNIPO integer)
  returns (
      SLODEF integer,
      SLOPOZ integer,
      KONTOFK KONTO_ID,
      SYMBFAK varchar(20) CHARACTER SET UTF8                           ,
      WINIEN numeric(14,2),
      MA numeric(14,2),
      WINIENZL numeric(14,2),
      MAZL numeric(14,2),
      WALUTA varchar(3) CHARACTER SET UTF8                           ,
      WALUTOWY smallint,
      SALDOWINIENZL numeric(14,2),
      SALDOMAZL numeric(14,2),
      SALDOWINIEN numeric(14,2),
      SALDOMA numeric(14,2),
      SALDO numeric(14,2),
      DATAOTW timestamp,
      DATAPLAT timestamp,
      SLODEFDICT varchar(20) CHARACTER SET UTF8                           ,
      SLOKOD varchar(100) CHARACTER SET UTF8                           ,
      SLOKODFK KONTO_ID,
      SLONAZWA varchar(255) CHARACTER SET UTF8                           ,
      TYP char(1) CHARACTER SET UTF8                           ,
      KOLOR smallint,
      ILDNIMAX integer,
      ILDOK integer,
      ODSETKI numeric(14,4),
      RIGHTS varchar(40) CHARACTER SET UTF8                           ,
      RIGHTSGROUP varchar(40) CHARACTER SET UTF8                           ,
      LIMIT numeric(14,2))
   as
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE DATA TIMESTAMP;
DECLARE VARIABLE KONTOLIKE KONTO_ID;
DECLARE VARIABLE RSLODEF INTEGER;
DECLARE VARIABLE RSLOPOZ INTEGER;
DECLARE VARIABLE RKONTOFK KONTO_ID;
DECLARE VARIABLE ILROZL INTEGER;
DECLARE VARIABLE RO_SLOPOZ INTEGER;
DECLARE VARIABLE RO_SLODEF INTEGER;
DECLARE VARIABLE RO_WALUTA VARCHAR(3);
DECLARE VARIABLE RO_KONTOFK KONTO_ID;
DECLARE VARIABLE RO_SALDO INTEGER;
DECLARE VARIABLE L_SALDO INTEGER;
DECLARE VARIABLE RO_HASKOMP SMALLINT;
begin
  rights = ''; rightsgroup = '';

  --TOFIX!!! styl, a najlepiej to sie jej pozbyc
  LIMIT = 0;
  ro_slodef = 0;
  ro_slopoz = 0;
  ro_waluta = '';
  ro_saldo = 0;
  ro_kontofk = '';
  ro_haskomp = 0;
  data = current_date;
  kontolike = :konto||'%';
  slokodfk = '';
  if (ildnipo is null) then ildnipo = 0;
  if (rodzaj = 0) then
  begin
    if ((:tryb = 0) or (:tryb = 2)) then
    begin
      for select SLODEF, SLOPOZ, ROZRACH.KONTOFK, SYMBFAK, WINIEN, MA, WINIENZL, MAZL, ROZRACH.waluta as waluta, WALUTOWY,
        (winien - ma ), DATAOTW, DATAPLAT, SLODEF.NAZWA, ROZRACH.TYP, ROZRACH.ODSETKI,LIMITKR, ROZRACH.DNI, ROZRACH.rights, ROZRACH.rightsgroup
      from ROZRACH
      left join SLODEF on ( SLODEF.REF = ROZRACH.SLODEF)
      left join klienci on (klienci.ref =rozrach.slopoz and slodef.typ='KLIENCI')
      where rozrach.company = :company
         and ((:OGRSLODEF is null) or (:ogrslodef = 0)or (:ogrslodef = ROZRACH.SLODEF))
         and ((:OGRSLOPOZ is null) or (:OGRSLOPOZ = 0)or (:OGRSLOPOZ = ROZRACH.slopoz))
         and ((:strona = 0) or (:strona = 1 and (ROZRACH.TYP = 'N' or (ROZRACH.WINIEN  - ROZRACH.MA >0)))
                            or (:strona = 2 and (ROZRACH.TYP = 'Z' or (ROZRACH.MA - ROZRACH.WINIEN > 0)))
             )
         and ((:zakres = 0) or
              (:zakres = 1 and ROZRACH.WINIEN <> ROZRACH.MA) or
              (:zakres = 2 and ROZRACH.WINIEN <> ROZRACH.MA and ROZRACH.DATAPLAT < current_date)
             )
         and (:oddaty is null or (rozrach.datazamk is null) or (rozrach.datazamk >= :oddaty))
         and (:dodaty is null or (rozrach.dataotw <= :dodaty))
         and ((:konto is null) or (:konto = '') or (rozrach.kontofk like :kontolike))
         and ((:oddzial is null) or (:oddzial = '') or (rozrach.oddzial = :oddzial))
      into :slodef, :slopoz, :kontofk, :symbfak, :winien, :ma, :winienzl, :mazl, :waluta , :walutowy,
        :saldo, :dataotw, :dataplat, :slodefdict, :typ, :odsetki,:LIMIT, :ildnimax, :rights, :rightsgroup
      do begin
        if (:saldo <> 0 and :dataplat < current_date) then kolor = 2;
        else if (:saldo <> 0 and :dataplat < current_date) then kolor = 1;
        else kolor = 0;
        if (:kontofk is null) then kontofk = '';
        if (:tryb = 0) then begin
           execute procedure SLO_DANE(:slodef, :slopoz) returning_values :slokod, :slonazwa, :slokodfk;
           suspend;
        end else begin
          if (:winien - :ma < 0) then l_saldo = -1;
          else if (:winien - :ma > 0) then l_saldo = 1;
          else l_saldo = 0;
          if (:kontofk is null) then kontofk = '';
          if ((:ro_slodef = :slodef) and (:ro_slopoz = :slopoz)
             and (:ro_saldo = :l_saldo) and (:ro_kontofk = :kontofk) and (:ro_waluta = :waluta))
          then begin
               cnt = :ro_haskomp;
          end else begin
            cnt = null;
            select count(*) from ROZRACH_KOMP(:slodef, :slopoz, :kontofk , :waluta, :winien - :ma,NULL,NULL,0) into :cnt;
            if (:cnt is null) then cnt = 0;
            ro_slodef = :slodef; ro_slopoz = :slopoz;
            ro_waluta = :waluta; ro_kontofk = :kontofk;
            if (:winien - :ma < 0) then ro_saldo = -1;
            else if (:winien > :ma) then ro_saldo = 1;
            else ro_saldo = 0;
            ro_haskomp = :cnt;
          end
          if (:cnt > 0) then begin
            execute procedure SLO_DANE(:slodef, :slopoz) returning_values :slokod, :slonazwa, :slokodfk;
            suspend;
          end
        end
      end
    end else begin -- rodzaj = 0; tryb = 1 - dla kompensat
      symbfak = '';
      for select SLODEF, SLOPOZ, KONTOFK
        , sum((WINIEN - MA)*((abs(winien - ma)/(winien - ma)+1)/2))
        , sum((MA-WINIEN)*((abs(ma - winien)/(ma - winien)+1)/2))
        , sum((WINIENZL-MAZL)*((abs(winienzl - mazl)/(winienzl - mazl)+1)/2))
        , sum((MAZL - WINIENZL)*((abs(mazl - winienzl)/(mazl - winienzl)+1)/2))
        , ROZRACH.waluta, max(WALUTOWY)
        , min(DATAOTW), min(DATAPLAT), max(SLODEF.NAZWA), max(ROZRACH.TYP), count(*)
      from ROZRACH  left join SLODEF on (SLODEF.REF = ROZRACH.SLODEF)
      where rozrach.company = :company
         and ((:OGRSLODEF is null) or (:ogrslodef = 0)or (:ogrslodef = ROZRACH.SLODEF))
         and ((:OGRSLOPOZ is null) or (:OGRSLOPOZ = 0)or (:OGRSLOPOZ = ROZRACH.slopoz))
         and ((:strona = 0) or (:strona = 1 and (ROZRACH.TYP = 'N' or (ROZRACH.WINIEN  - ROZRACH.MA >0)))
                            or (:strona = 2 and (ROZRACH.TYP = 'Z' or (ROZRACH.MA - ROZRACH.WINIEN > 0)))
             )
         and ((:zakres = 0) or
              (:zakres = 1 and ROZRACH.WINIEN <> ROZRACH.MA) or
              (:zakres = 2 and ROZRACH.WINIEN <> ROZRACH.MA and ROZRACH.DATAPLAT < current_date)
             )
         and (:oddaty is null or (rozrach.datazamk is null) or (rozrach.datazamk >= :oddaty))
         and (:dodaty is null or (rozrach.dataotw <= :dodaty))
         and ROZRACH.WINIEN <> ROZRACH.MA
         and ROZRACH.WINIENZL <> ROZRACH.MAZL
         and ((:konto is null) or (:konto = '') or (rozrach.kontofk like :kontolike))
         and ((:oddzial is null) or (:oddzial = '') or (rozrach.oddzial = :oddzial))
         and slodef<>0 and slopoz<>0
      group by slodef, slopoz, kontofk, waluta
      into :slodef, :slopoz, :kontofk, :winien, :ma, :winienzl, :mazl, :waluta , :walutowy,
        :dataotw, :dataplat, :slodefdict,:typ,:ildok
    do begin
        saldo = :winien - :ma;
        if (:kontofk is null) then kontofk = '';
        -- sprawdzenie, czy dla danego slodef, slopoz, konto istnieją rozrachunki na innych kontach
        cnt = 0;
        if (:winien > 0) then
          for select SLODEF, SLOPOZ, KONTOFK
          from ROZRACH_KOMP(:slodef, :slopoz, NULL, :waluta, :winien ,NULL,NULL,1)
          into :rslodef, :rslopoz, :rkontofk
          do begin
            if ((:rslodef <> :slodef)
            or(:rslopoz <> :slopoz)
            or(:rkontofk <> :kontofk)) then begin
              cnt = :cnt + 1;
            end
          end
        if (:ma > 0 and :cnt = 0) then
          for select SLODEF, SLOPOZ, KONTOFK
          from ROZRACH_KOMP(:slodef, :slopoz, NULL, :waluta, -:ma ,NULL,NULL,1)
          into :rslodef, :rslopoz, :rkontofk
          do begin
            if ((:rslodef <> :slodef)
            or(:rslopoz <> :slopoz)
            or(:rkontofk <> :kontofk)) then begin
              cnt = :cnt + 1;
            end
          end
        if (:saldo <> 0 and :dataplat < current_date) then kolor = 2;
        else if (:saldo <> 0 and :dataplat < current_date) then kolor = 1;
        else kolor = 0;
        if (:cnt > 0) then begin
          execute procedure SLO_DANE(:slodef, :slopoz)
            returning_values :slokod, :slonazwa, :slokodfk;
          suspend;
        end
      end
    end
  end else begin
    symbfak = '';
    kontofk = '';
    for
      select SLODEF, SLOPOZ, count(*), sum(WINIEN) , sum(MA), sum(WINIENZL), sum(MAZL), ROZRACH.waluta as waluta, max(WALUTOWY),
      sum (winien - ma ), min(DATAOTW), min(DATAPLAT), max(SLODEF.NAZWA),
      max(ROZRACH.datazamk - ROZRACH.dataplat), max(LIMITKR)
     from ROZRACH left join SLODEF on ( SLODEF.REF = ROZRACH.SLODEF)
     left join klienci on (klienci.ref =rozrach.slopoz and slodef.typ='KLIENCI')
     where rozrach.company = :company
       and ((:OGRSLODEF is null) or (:ogrslodef = 0)or (:ogrslodef = ROZRACH.SLODEF))
         and ((:OGRSLOPOZ is null) or (:OGRSLOPOZ = 0)or (:OGRSLOPOZ = ROZRACH.slopoz))
         and ((:strona = 0) or (:strona = 1 and (ROZRACH.TYP = 'N' or (ROZRACH.WINIEN  - ROZRACH.MA >0)))
                            or (:strona = 2 and (ROZRACH.TYP = 'Z' or (ROZRACH.MA - ROZRACH.WINIEN > 0)))
             )
         and ((:zakres = 0) or
              (:zakres = 1 and ROZRACH.WINIEN <> ROZRACH.MA) or
              (:zakres = 2 and ROZRACH.WINIEN <> ROZRACH.MA and ROZRACH.DATAPLAT < current_date and
                (:ildnipo = 0  or (rozrach.dataplat + :ildnipo = current_date)))
             )
         and (:oddaty is null or (rozrach.datazamk is null) or (rozrach.datazamk >= :oddaty))
         and (:dodaty is null or (rozrach.dataotw <= :dodaty))
         and ((:konto is null) or (:konto = '') or (rozrach.kontofk like :kontolike))
         and ((:oddzial is null) or (:oddzial = '') or (rozrach.oddzial = :oddzial))
     group by SLODEF, SLOPOZ, rozrach.WALUTA
     into :slodef, :slopoz, :ildok,:winien, :ma, :winienzl, :mazl, :waluta , :walutowy,
      :saldo, :dataotw, :dataplat, :slodefdict, :ildnimax
      , :limit
    do begin
        if (limit = 0) then
          limit = null;
        saldowinienzl = winienzl - mazl;
        if (saldowinienzl < 0) then
          saldowinienzl = 0;

        saldomazl = mazl - winienzl;
        if (saldomazl < 0) then
          saldomazl = 0;

        saldowinien = winien - ma;
        if (saldowinien < 0) then
          saldowinien = 0;

        saldoma = ma - winien;
        if (saldoma < 0) then
          saldoma = 0;

      if (:slodef > 0 and :SLOPOZ > 0) then
      begin
        execute procedure SLO_DANE(:slodef, :slopoz)
          returning_values :slokod, :slonazwa, :slokodfk;
        if (:ilrozl is null) then ilrozl = 0;
        if (:ildok > :ilrozl) then
          ildnimax = :data - :dataplat;
        if (:ildnimax <= 0) then ildnimax = null;
        if (:saldo <> 0 and :dataplat < current_date) then kolor = 2;
        else if (:saldo <> 0 and :dataplat < current_date) then kolor = 1;
        else kolor = 0;
        suspend;
      end
    end
  end
end^
SET TERM ; ^
