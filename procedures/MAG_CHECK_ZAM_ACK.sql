--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_CHECK_ZAM_ACK(
      ZAMOWIENIE integer)
   as
declare variable akc integer;
declare variable ilosc integer;
begin
  select count(*), sum(DOKUMNAG.akcept) from DOKUMNAG where DOKUMNAG.zamowienie=:zamowienie into
    :ilosc, :akc;
  if(:ilosc is null) then ilosc =  0;
  if(:akc is null) then akc = 0;
  if(:ilosc <> :akc) then akc = 0;
  else akc = 1;
  update NAGZAM set DOKMAGAKC = :akc where REF=:zamowienie and DOKMAGAKC<>:akc;

end^
SET TERM ; ^
