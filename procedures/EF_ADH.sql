--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_ADH(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      DHTYPE varchar(2) CHARACTER SET UTF8                           ,
      COL integer,
      EABSUPDATE smallint = 1)
  returns (
      AMOUNT numeric(14,2))
   as
declare variable FROMDATE date;
declare variable TODATE date;
declare variable ESYSTEM_START date;
declare variable STORNO_AMOUNT numeric(14,2);
declare variable AREF integer;
declare variable COREPAYROLL integer;
begin
/*MWr: Personel - Procedura do naliczenia na liscie plac czasu trwania nieobecnosci
     o numerze skadnika COL. Przypisuje nieobecnosci informacje o liscie, na jakiej
     zostala ona rozliczona, o ile EABSUPDATE = 1. Wg parametru DHTYPE rozliczamy:
       = CD - dni kalendarzowe
       = WD - dni robocze
       = WH - godziny robocze  */

  if (coalesce(dhtype,'') not in ('CD', 'WD', 'WH')) then
    exception universal 'Nieznany typ parametru!';

  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  execute procedure get_config('ESYSTEM_START', 2)
    returning_values esystem_start;


  select sum(case when :dhtype = 'CD' then days
                  when :dhtype = 'WD' then workdays
                  when :dhtype = 'WH' then worksecs/3600.00 end)
    from eabsences
    where employee = :employee
      and ecolumn = :col
      and correction in (0,2)
      and todate <= :todate
      and (epayroll is null and fromdate >= :esystem_start
        or epayroll = :payroll)
    into :amount;

  if (amount is not null and eabsupdate = 1) then
  begin
    update eabsences
      set epayroll = :payroll
      where employee = :employee
        and ecolumn = :col
        and correction in (0,2)
        and todate <= :todate
        and fromdate >= :esystem_start
        and epayroll is null;
  end

  for
    select a.ref, a.corepayroll,
         - case when :dhtype = 'CD' then a.days
                when :dhtype = 'WD' then a.workdays
                when :dhtype = 'WH' then a.worksecs/3600.00 end
      from eabsences a
    where a.employee = :employee
      and a.ecolumn = :col
      and a.correction = 1
      and a.todate <= :todate
      and (a.corepayroll is null and a.fromdate >= :esystem_start
        or a.corepayroll = :payroll)
    into :aref, :corepayroll, :storno_amount
  do begin
    amount = coalesce(amount,0) + storno_amount;

    if (corepayroll is null and eabsupdate = 1) then
    begin
    /*Uzupelnienie na stornie informacji o liscie, na ktorej rozliczono korekte.
      W 99% ponizszy update wykona sie dla samych storn bez korekt, gdyz inne
      przypadki powinny zalatwic triggery EABSENCES_AU/D_CORRECTION (po przejsciu
      wyzej zdefiniowanego update), w przeciwnym razie cos nie tak z triggerem*/

      update eabsences set corepayroll = :payroll where ref = :aref and corepayroll is null;
    end
  end

  suspend;
end^
SET TERM ; ^
