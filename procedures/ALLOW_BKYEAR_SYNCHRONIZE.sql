--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ALLOW_BKYEAR_SYNCHRONIZE(
      BKYEAR YEARS_ID,
      PATTERN_REF COMPANIES_ID)
  returns (
      ALLOW SMALLINT_ID)
   as
declare variable current_company companies_id;
begin
  -- Procedura sprawzdająca czy dany rok ksigowy może zostać synchronizowany
  -- z danym wzorcem. Badamy czy dany bkyear w company zależnym nie ma już planu kont
  -- żeby si synchronizować trzeba wykosić plan kont
  allow = 1;

  --szukam company do któego należy nowy rok księgowy
  execute procedure get_global_param('CURRENTCOMPANY')
    returning_values :current_company;

   --Szukam, czy istnieje plan kont
   if (exists (select first 1 1
                 from bkaccounts ba
                 where ba.company = :current_company
                   and ba.yearid = :bkyear)) then
   begin
     allow = 0;
   end
 suspend;
end^
SET TERM ; ^
