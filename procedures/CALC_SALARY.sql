--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CALC_SALARY(
      RATIO numeric(14,3),
      PDATE timestamp,
      COMPANY integer)
  returns (
      SALARY numeric(14,2))
   as
declare variable pvalue numeric(18,2);
begin
  execute procedure GET_PVAL (pdate, company, 'KWB')
    returning_values pvalue;
  salary = pvalue * ratio;
  suspend;
end^
SET TERM ; ^
