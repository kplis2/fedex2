--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COUNT_SENIORITY(
      WORKCERTIF integer,
      EMPLOYEE integer,
      FROMDATE timestamp,
      TODATE timestamp,
      STAZ_ZEWN smallint)
  returns (
      SYEARS smallint,
      SMONTHS smallint,
      SDAYS smallint)
   as
declare variable efromdate date;
declare variable etodate date;
declare variable afromdate date;
declare variable atodate date;
declare variable sy smallint;
declare variable sm smallint;
declare variable sd smallint;
declare variable startdate date;
declare variable wfromdate date;
declare variable wtodate date;
declare variable wref integer;
declare variable ewfromdate date;
declare variable ewtodate date;
declare variable mfromdate date;
declare variable to_normalize smallint;
declare variable withschool smallint;
begin
  --DS: funkcja oblicza staz pracy dla danego swiadectwa pracy,
  --nie moze jednak aktualizowac zadnego pola w bazie, poniewaz uzytkownik moze sie jeszcze wycofac
  --z wprowadzania zmian w swiadectwach

  --pewne rzeczy dalej nie beda sie liczyc poprawnie!
  --bedzie zle gdy:
  ----pracownik na pozniejszym swiadectwie ma okres nieskladkowy a na wczesniejszym w tym samym okresie nie
  --mozna to ominac dzielac swiadectwo na czesci i oznaczajac na poszczegolnych czesciach czy ma byc brane do stazu
  --jest problem aby zrobic to raz a dobrze, poniewaz kombinacji moze byc sporo, w takiej postaci raczej klienci nie powinni narzekac

  --konfig senioritywithschool steruje czy ukonczone szkoly uwzgledniac przy liczeniu stazu,
  --jezeli u klienta wartosc nalezy zmienic, nalezy przeliczyc wszystkie swiadectwa pracy

  syears = 0; smonths = 0; sdays = 0;
  to_normalize = 0;
  --fromdate i todate przekazywane sa z programu nie jako nulle a jako daty 1899-12-30 (data nigdy nullem nie bedzie)
  if (coalesce(fromdate,'1899-12-30') > '1899-12-31' and coalesce(todate,'1899-12-30') > '1899-12-31') then
  begin
    if (fromdate > todate) then
      exception data_do_musi_byc_wieksza;
    if (staz_zewn = 0) then
    begin
      select min(fromdate)
        from employment m
        where m.employee = :employee
        into :mfromdate;
  
      execute procedure date_add(mfromdate, 0,-1,0) returning_values startdate;
      --plus okresy nieskladkowe (bez urlopu wychowawczego, ktory jest brany do stazu)
      --okres zatrudnienia odejmuje od stazu ale bez nieskladkowych

      for
        select fromdate, todate
          from eabsences
          where employee = :employee and ecolumn in (10,300,330)
            and fromdate <= : todate and todate >= :mfromdate
          order by fromdate
          into :afromdate, :atodate
      do begin
        to_normalize = 1;
        --zeby nie dodawal nieobecnosci na kilku swiadectwach pracy
        --ustawiam ate poczatkowa na pierwszy dzien po zakonczeniu wczesniejszego swiadectwa
        select first 1 w.todate + 1
          from eworkcertifs w
          where w.employee = :employee
            and w.ref <> :workcertif
            and w.fromdate <= :atodate
            and w.todate >= :afromdate
            --w kolejnosci wiekszy fromdate, todate, ref
            and   (w.fromdate < :fromdate
              or (w.fromdate = :fromdate and
                (w.todate < :todate
                or (w.todate = :todate and w.ref < :workcertif))))
          order by fromdate desc, w.todate desc, ref desc
          into :afromdate;

        if (afromdate > startdate) then
        begin
        startdate = atodate;
        while (atodate = startdate) do
        begin
          startdate = null;
          select todate
            from eabsences
            where employee = :employee and ecolumn in (10,300,330)
              and fromdate = :atodate + 1
            into :startdate;
            if (startdate is not null) then
              atodate = startdate;
        end
        startdate = atodate;
  
        if (afromdate < mfromdate) then afromdate = mfromdate;
        if (atodate > todate) then atodate = todate;
        execute procedure get_ymd(afromdate, atodate)
          returning_values sy, sm, sd;
        syears = syears + sy;
        smonths = smonths + sm;
        sdays = sdays + sd;
        end
      end
  
      --do daty zatrudnienia
      if (todate >= mfromdate) then
        todate = mfromdate - 1;
    end

    execute procedure get_ymd(fromdate, todate)
      returning_values sy, sm, sd;
    syears = syears + sy;
    smonths = smonths + sm;
    sdays = sdays + sd;
  
    startdate = '1899-12-30';
    --sprawdza poprzednie swiadectwa jezeli maja mniejszy fromdate, pozniej todate, pozniej ref
    --poniewaz moga byc 2 swiadectwa o takim samym zakresie dat a kolejnosc trzeba ustalic
    for
      select  w.fromdate, w.todate, w.ref
        from eworkcertifs w
        --czy przecina
        where w.fromdate <= :todate
          and w.todate >= :fromdate
          --czy jest wczesniejsze niz aktualne
          and (w.fromdate < :fromdate
            or (w.fromdate = :fromdate and
            (w.todate < :todate
               or w.todate = :todate and w.ref < :workcertif)))
  
          and w.ref <> :workcertif
          and w.employee = :employee
        order by w.fromdate, w.todate, w.ref
        into wfromdate, wtodate, wref
    do begin
      to_normalize = 1;
      if (wfromdate < fromdate) then
        wfromdate = fromdate;
      if (wtodate > todate) then
        wtodate = todate;
      if (wfromdate < startdate) then
        wfromdate = startdate;
      startdate = wtodate + 1;
      execute procedure get_ymd(wfromdate, wtodate)
        returning_values sy, sm, sd;
      syears = syears - sy;
      smonths = smonths - sm;
      sdays = sdays - sd;
      --dodaje do stazu okresy nieskladkowe z wczesniejszych swiadectw w okresie obemujacym stare i aktualne swiadectwo
      for
        select c.fromdate, c.todate
          from ewrkcrtnocontr c
          where c.workcertif = :wref
            and c.ecolumn in (10,300,330)
            and c.fromdate <= :wtodate
          and c.todate >= :wfromdate
          into :ewfromdate, :ewtodate
      do begin
        if (ewfromdate < wfromdate) then
          wfromdate = efromdate;
        if (ewtodate > etodate) then
          ewtodate = etodate;
        execute procedure get_ymd(ewfromdate, ewtodate)
          returning_values sy, sm, sd;
        syears = syears + sy;
        smonths = smonths + sm;
        sdays = sdays + sd;
      end
    end

  --skladnik 26 - urlop wychowawczy jest okresem nieskladkowym , ale liczy sie go do stazu pracy
    for
      select fromdate, todate
        from ewrkcrtnocontr
        where workcertif = :workcertif
          and ecolumn in (10,300,330)
        into :efromdate, :etodate
    do begin
      to_normalize = 1;
      if (etodate > todate) then
        etodate = todate;

        execute procedure get_ymd(efromdate, etodate)
          returning_values sy, sm, sd;
        syears = syears - sy;
        smonths = smonths - sm;
        sdays = sdays - sd;
    end

    execute procedure get_config('SENIORITYWITHSCHOOL', 2) returning_values withschool;
    --jezeli w danym okresie pracownik chodzil do szkoly ktora ma byc brana do urlopu, nalezy odjac pokrywajace sie okresy
    if (withschool = 1) then
    begin
      for
        select p.fromdate, p.todate
          from employees e
            join eperschools p on p.person = e.person
          where e.ref = :employee
            and p.isvacbase = 1
            and p.fromdate <= :todate
            and p.todate >= :fromdate
          into :efromdate, :etodate
      do begin
        to_normalize = 1;
        if (etodate > todate) then
          etodate = todate;
        if (efromdate < fromdate) then
          efromdate = fromdate;
  
        execute procedure get_ymd(efromdate, etodate)
          returning_values sy, sm, sd;
        syears = syears - sy;
        smonths = smonths - sm;
        sdays = sdays - sd;
      end
    end

    --jesli nie bylo okresow nieskladkowych, nie powinnismy normalizowac (okres od-do jest ciagly)
    if (to_normalize = 1) then
      execute procedure normalize_ymd(syears, smonths, sdays)
        returning_values syears, smonths, sdays;
    --moga byc przypadki ze swiadectwo calkowicie sie pokrywa z innym, czyli juz wychodzi staz 0
    --a jak do tego dojda nieskladkowe na aktualnym to wejdzie na minus
    if (syears < 0 or (syears = 0 and smonths < 0) or (syears = 0 and smonths = 0 and sdays < 0)) then
    begin
      syears = 0;
      smonths = 0;
      sdays = 0;
    end
  end
  suspend;
end^
SET TERM ; ^
