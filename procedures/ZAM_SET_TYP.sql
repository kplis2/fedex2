--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ZAM_SET_TYP(
      ZAM integer)
   as
declare variable typ smallint;
declare variable org_ref integer;
declare variable ref integer;
declare variable cnt integer;
begin

  select REF, ORG_REF from NAGZAM where REF=:zam into :ref, :org_ref;
  if(:ref <> :zam) then exit;
  if(:org_ref is null) then org_ref = 0;
  if(:org_ref <> 0) then begin
    if(:org_ref = :zam) then
       typ = 3;
    else
       typ = 2;
  end else begin
    select count(*) from NAGZAM where ORG_REF=:zam into :cnt;
    if(:cnt > 0) then typ = 1;
      else typ = 0;
  end
  update NAGZAM set TYP = :typ where REF=:zam AND TYP <> :typ;
end^
SET TERM ; ^
