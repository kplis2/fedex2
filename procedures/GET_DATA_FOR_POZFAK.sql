--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_DATA_FOR_POZFAK(
      REF integer)
  returns (
      DATA varchar(255) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE D VARCHAR(40);
begin
 data='';
 for select distinct substring(cast(dostawy.datawazn as varchar(40)) from 3 for 10)
 from pozfak join dokumpoz on (dokumpoz.frompozfak = pozfak.ref)
      join  dokumroz on (dokumpoz.ref = dokumroz.pozycja)
      join dostawy on (dokumroz.dostawa = dostawy.ref)
 where pozfak.ref = :ref into :d
 do begin
   if (:data='') then data = :d;
   else data = :data ||';'|| :d;
 end
 suspend;
end^
SET TERM ; ^
