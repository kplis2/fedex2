--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_FILLWHAREASDISTS(
      REF1 integer,
      REF2 integer,
      DIST numeric(15,4))
   as
begin
  if (not exists (select whareab from whareasdists where whareab = :ref1 and whareae = :ref2)) then
    insert into whareasdists (whareab, whareae, distance) values (:ref1, :ref2, :dist);
end^
SET TERM ; ^
