--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BTRANSFER_WITHOUT_ROZRACHP(
      COMPANY integer,
      SLODEF integer,
      SLOPOZ integer,
      ACCOUNT ACCOUNT_ID,
      SETTLEMENT varchar(50) CHARACTER SET UTF8                           )
  returns (
      AMOUNT numeric(14,2))
   as
declare variable BPAMOUNT numeric(14,2);
declare variable BPREF integer;
declare variable BREF integer;
declare variable RKDOKREF integer;
declare variable BSTATUS integer;
declare variable ROZRACHP_EXISTS integer;
declare variable RKRAPORTREF integer;
begin
  /* procedura zwraca sum wartoci przelewów dla danego rozrachunku,
  które nie mają swoich odpowiednikó w ROZRACHP, czyli oczekują na ksiegowanie,
  poączenie z RKDOKNAGIEM czy cokolwiek takiego */
  amount = 0;
  for select BP.amount, bp.ref, b.ref, rdok.ref, b.status, rdok.raport
  from BTRANSFERPOS BP
    join BTRANSFERS B on (B.ref = bp.btransfer)
    left join RKDOKNAG RDOK on (RDOK.btransfer = b.ref)
  where
        B.slodef = :slodef
    and B.slopoz = :slopoz
    and b.company = :company
    and BP.account = :account
    and BP.settlement = :settlement
    and b.status < 4
  into :bpamount, :bpref, :bref, :rkdokref, :bstatus, :rkraportref

  do begin
     rozrachp_exists = 0;
     if(bstatus < 3) then
       rozrachp_exists = 0;--nie ma pawa istniec
     else if (bstatus = 5) then
       rozrachp_exists = 1;--traktujemy, jakby istnial, bo po prostu tej wartosci na pewno nie mamy doliczac do wyniku
     else begin
       --sprawdzenie, czy sam zalozyl rozrachp
       if(exists(select first 1 1 from ROZRACHP RP
         where RP.company = :company and RP.SLODEF = :slodef and RP.slopoz = :slopoz
           and RP.kontofk = :account and RP.symbfak = :settlement
           and RP.stable = 'BTRANSFERPOS' and RP.SREF = :bpref))
       then
         rozrachp_exists = 1;
       else if(:rkdokref > 0) then begin
         --jesli jest polaczony z operacja bankową, to pytanie, czy operacja naniosla rozrachunki
         if(exists(select first 1 1
           from ROZRACHP RP
             join RKDOKPOZ RDPOZ on (RDPOZ.ref = RP.sref and RDPOZ.dokument = :rkdokref)
           where RP.company = :company and RP.SLODEF = :slodef and RP.slopoz = :slopoz
             and RP.kontofk = :account and RP.symbfak = :settlement
             and RP.stable = 'RKDOKPOZ' ))
         then
           rozrachp_exists = 1;
         else begin
           --sprawdzenie, czy przypadkiem raport, na którym ten dokument jest, nie zosta zaksigowany  czyli czy DECREESY nie trzymają jaki rozrachp
         if(exists(select first 1 1
           from ROZRACHP RP
             join DECREES D on (RP.decrees = d.ref)
             join BKDOCS BK on (D.bkdoc = BK.REF and bk.otable = 'RKRAPKAS' and bk.oref = :rkraportref)
           where RP.company = :company and RP.SLODEF = :slodef and RP.slopoz = :slopoz
             and RP.kontofk = :account and RP.symbfak = :settlement
             and RP.stable = 'DECREES' ))
          then begin
            -- TODO JCZ
          end

         end
       end
     end


     if(rozrachp_exists = 0) then
       amount = :amount + :bpamount;
  end

end^
SET TERM ; ^
