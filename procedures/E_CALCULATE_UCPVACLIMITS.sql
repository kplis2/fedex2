--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_CALCULATE_UCPVACLIMITS(
      EMPLCONTRACT integer,
      ONDATE date)
  returns (
      NOPAYLIMIT integer,
      PAYLIMIT integer)
   as
declare variable annexe integer;
begin
  nopaylimit = null;
  paylimit = null;
  select first 1 c.annexe
      from emplcontrhist c
      where c.emplcontract = :emplcontract
      and c.fromdate <= :ondate
      and (c.todate is null or c.todate >= cast(extract(year from :ondate) || '/1/1' as date))
    order by fromdate desc
    into annexe;

  annexe = coalesce(:annexe,0);
  select o.val_numeric
    from emplcontrcondit o
      left join edictcontrcondit d on d.ref = o.contrcondit
    where d.symbol = 'DNI_WOLNE_PLATNE'
      and o.emplcontract = :emplcontract
      and (o.annexe = :annexe or (:annexe = 0 and o.annexe is null))
    into :paylimit;
  select o.val_numeric
    from emplcontrcondit o
      left join edictcontrcondit d on d.ref = o.contrcondit
    where d.symbol = 'DNI_WOLNE_NIEPLATNE'
      and o.emplcontract = :emplcontract
      and (o.annexe = :annexe or (:annexe = 0 and o.annexe is null))
    into :nopaylimit;
  suspend;
end^
SET TERM ; ^
