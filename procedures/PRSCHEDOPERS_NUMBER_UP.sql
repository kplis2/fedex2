--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDOPERS_NUMBER_UP(
      SCHOPERTO integer,
      NUMBER integer)
   as
declare variable depto integer;
declare variable number_tmp integer;
begin
 --przejcie po aktywnych operacjach
  update prschedopers o set o.number = :number where o.ref = :SCHOPERTO;
  for
    select distinct d.depto, o.number from prschedoperdeps d
      join  prschedopers o on d.depto = o.ref and o.activ = 1 and o.number is not null
      where d.depfrom = :SCHOPERTO
      into :depto, :number_tmp
  do begin
    execute procedure PRSCHEDOPERS_NUMBER_UP (:depto, :number+1);
  end
end^
SET TERM ; ^
