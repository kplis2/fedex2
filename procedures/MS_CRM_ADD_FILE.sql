--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MS_CRM_ADD_FILE(
      PLIK varchar(255) CHARACTER SET UTF8                           ,
      SEGREGATOR integer,
      FORMAT integer,
      TYP integer,
      OPERATOR integer,
      CPODMIOT integer,
      KONTAKT integer)
  returns (
      DOKPLIK_REF integer)
   as
declare variable lancuch varchar(255);
declare variable indeks integer;
declare variable data timestamp;
begin
  execute procedure gen_ref('DOKPLIK')
    returning_values :dokplik_ref;

  execute procedure string_reverse(:plik)
    returning_values :lancuch;
  indeks=coalesce(char_length(plik),0)-position(lancuch in '.'); -- [DG] XXX ZG119346
  lancuch=substring(:plik from 1 for :indeks)||'_'||:dokplik_ref||substring(:plik from :indeks+1 for coalesce(char_length(:plik), 0)); -- [DG] XXX ZG119346

  data=current_timestamp(0);
  if (cpodmiot=0) then
    cpodmiot=NULL;
  if (kontakt=0) then
    kontakt=NULL;

  insert into dokplik(ref, symbol, plik, segregator, data, format, typ, operator, cpodmiot, kontakt)
    values(:dokplik_ref, :plik, :lancuch, :segregator, :data, :format, :typ, :operator, :cpodmiot, :kontakt);
  suspend;
end^
SET TERM ; ^
