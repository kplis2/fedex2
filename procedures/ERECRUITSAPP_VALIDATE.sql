--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ERECRUITSAPP_VALIDATE(
      SYMBOL STRING20,
      ECANDIDATE ECANDIDATES_ID,
      QUESTHEADER EQUESTHEADERDEF_ID)
  returns (
      VALID SMALLINT_ID)
   as
declare variable RECRUIT ERECRUITS_ID;
declare variable EHREF EQUESTHEADERDEF_ID;
begin
  valid = 0;
  select first 1 e.ref,eh.ref from erecruits e
    join equestheader eh on eh.erecruit=e.ref
    where e.symbol=:symbol and eh.ecandidate=:ecandidate
    into recruit,ehref;
  if(ehref is not distinct from questheader) then
  begin
    if(exists(select * from ereccands er where er.candidate=:ecandidate and
    er.recruit=:recruit)) then
    begin
      valid=1;
    end
  end

  suspend;
end^
SET TERM ; ^
