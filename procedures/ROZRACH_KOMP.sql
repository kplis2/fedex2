--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ROZRACH_KOMP(
      SSLODEF integer,
      SSLOPOZ integer,
      SKONTOFK KONTO_ID,
      SWALUTA varchar(3) CHARACTER SET UTF8                           ,
      SALDO numeric(14,4),
      FROMSLODEF integer,
      FROMSLOPOZ integer,
      FORKOMPENSA integer)
  returns (
      SLODEF integer,
      SLOPOZ integer,
      SYMBFAK varchar(20) CHARACTER SET UTF8                           ,
      KONTOFK KONTO_ID,
      WALUTA varchar(3) CHARACTER SET UTF8                           ,
      DATAOTW timestamp,
      DATAPLAT timestamp,
      MA numeric(14,2),
      WINIEN numeric(14,2),
      MAZL numeric(14,2),
      WINIENZL numeric(14,2))
   as
declare variable typ varchar(10);
begin
  --TOFIX: wielozakladowosc, styl
   if(:fromslodef is null) then fromslodef = 0;
   if(:fromslopoz is null) then fromslopoz = 0;
   forkompensa = 1;
   if((     (:fromslodef = 0)
        and (:fromslopoz = 0)
      )
      or (:skontofk <>'')
   ) then begin
     for select SLODEF, SLOPOZ, SYMBFAK, KONTOFK, WALUTA,
              DATAOTW, DATAPLAT, MA, WINIEN, MAZL, WINIENZL
     from ROZRACH where (WINIEN-MA)*:SALDO <0
       AND SLODEF = :SSLODEF
       AND SLOPOZ = :SSLOPOZ
       AND WALUTA = :SWALUTA
       AND ((:skontofk is null) or (:skontofk = '') or (:skontofk = KONTOFK))
     into :slodef, :slopoz, :symbfak, :kontofk, :WALUTA,
        :DATAOTW, :DATAPLAT, :MA, :WINIEN, :MAZL, :WINIENZL
     do begin
       suspend;
     end

     if(:forkompensa = 1) then begin

       select TYp from SLODEF where REF=:SSLODEF into :typ;
       if(:typ = 'KLIENCI') then begin
         slopoz = null; slodef = NULL;
         select DOSTAWCA from KLIENCI where REF=:sslopoz into :slopoz;
         select ref from SLODEF where TYP='DOSTAWCY' into :slodef;
         if(:slopoz > 0 and :slodef > 0) then begin
           for select SYMBFAK, KONTOFK, WALUTA, DATAOTW, DATAPLAT,MA, WINIEN, MAZL, WINIENZL
              from ROZRACH where (WINIEN-MA) *:SALDO <0
              AND SLODEF = :SLODEF
              AND SLOPOZ = :SLOPOZ
              AND WALUTA = :SWALUTA
              AND ((:skontofk is null) or (:skontofk = '') or (:skontofk = KONTOFK))
                 into :symbfak,:kontofk, :WALUTA, :DATAOTW, :DATAPLAT, :MA, :WINIEN, :MAZL, :WINIENZL
           do begin
             suspend;
           end
         end
       end

       if(:typ = 'DOSTAWCY') then begin
         slopoz = null;  slodef = NULL;
         select ref from SLODEF where TYP='KLIENCI' into :slodef;
         for select REF from KLIENCI where DOSTAWCA = :sslopoz into :slopoz
         do begin
           if(:slopoz > 0 and :slodef > 0) then begin
             for select SYMBFAK, KONTOFK, WALUTA, DATAOTW, DATAPLAT,MA, WINIEN, MAZL, WINIENZL
                from ROZRACH where (WINIEN-MA) *:SALDO <0
                   AND SLODEF = :SLODEF
                   AND SLOPOZ = :SLOPOZ
                   AND WALUTA = :SWALUTA
                   AND ((:skontofk is null) or (:skontofk = '') or (:skontofk = KONTOFK))
                   into :symbfak,:kontofk, :WALUTA, :DATAOTW, :DATAPLAT, :MA, :WINIEN, :MAZL, :WINIENZL
             do begin
              suspend;
             end
           end
         end
       end
     end
   end else begin
     /* kompensata z wymuszonych slownikow i pozycji*/
     /* MS: osobne warianty ze wzgledow wydajnosciowych */
     if(:fromslodef<>0 and :fromslopoz<>0) then begin
       for select SLODEF, SLOPOZ, SYMBFAK, KONTOFK, WALUTA,
                DATAOTW, DATAPLAT,MA, WINIEN, MAZL, WINIENZL
       from ROZRACH where (WINIEN-MA)*:SALDO <0 AND
           (SLODEF = :FROMSLODEF)AND
           (SLOPOZ = :FROMSLOPOZ) AND
           WALUTA = :SWALUTA
       into :slodef, :slopoz, :symbfak, :kontofk, :WALUTA,
          :DATAOTW, :DATAPLAT,:MA, :WINIEN, :MAZL, :WINIENZL
       do begin
         suspend;
       end
     end else if(:fromslodef=0 and :fromslopoz<>0) then begin
       for select SLODEF, SLOPOZ, SYMBFAK, KONTOFK, WALUTA,
                DATAOTW, DATAPLAT,MA, WINIEN, MAZL, WINIENZL
       from ROZRACH where (WINIEN-MA)*:SALDO <0 AND
           (SLOPOZ = :FROMSLOPOZ) AND
           WALUTA = :SWALUTA
       into :slodef, :slopoz, :symbfak, :kontofk, :WALUTA,
          :DATAOTW, :DATAPLAT,:MA, :WINIEN, :MAZL, :WINIENZL
       do begin
         suspend;
       end
     end else if(:fromslodef<>0 and :fromslopoz=0) then begin
       for select SLODEF, SLOPOZ, SYMBFAK, KONTOFK, WALUTA,
                DATAOTW, DATAPLAT,MA, WINIEN, MAZL, WINIENZL
       from ROZRACH where (WINIEN-MA)*:SALDO <0 AND
           (SLODEF = :FROMSLODEF)AND
           WALUTA = :SWALUTA
       into :slodef, :slopoz, :symbfak, :kontofk, :WALUTA,
          :DATAOTW, :DATAPLAT,:MA, :WINIEN, :MAZL, :WINIENZL
       do begin
         suspend;
       end
     end else if(:fromslodef=0 and :fromslopoz=0) then begin
       for select SLODEF, SLOPOZ, SYMBFAK, KONTOFK, WALUTA,
                DATAOTW, DATAPLAT,MA, WINIEN, MAZL, WINIENZL
       from ROZRACH where (WINIEN-MA)*:SALDO <0 AND
           WALUTA = :SWALUTA
       into :slodef, :slopoz, :symbfak, :kontofk, :WALUTA,
          :DATAOTW, :DATAPLAT,:MA, :WINIEN, :MAZL, :WINIENZL
       do begin
         suspend;
       end
     end
   end
end^
SET TERM ; ^
