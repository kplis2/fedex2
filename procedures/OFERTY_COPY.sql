--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OFERTY_COPY(
      ORGREF integer,
      CPODMIOT integer,
      CKONTRAKT integer,
      STATUS smallint,
      OLDSTATUS smallint,
      KOPPOZ integer,
      TRYB integer)
  returns (
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable ok smallint;
declare variable newref INTEGER ;
begin
  -- tyrb 0 -> kopa oferty
  -- tryb 1 -> nowa wersja oferty
  msg = '';
  ok = 0;
  if (cpodmiot = 0) then cpodmiot = null;
  if (ckontrakt = 0) then ckontrakt = null;

  if (coalesce(orgref,0) > 0) then
  begin
    if (tryb = 0) then
    begin
      execute procedure gen_ref('OFERTY') returning_values :newref;
      insert into oferty (REF, SLODEF, SLOPOS, CPODMIOT,
          CKONTRAKT, STATUS, MARZAKOSZT, MARZAKOSZTPR, OFETYP, waluta,
          DATA, DATAWAZN)
        select :newref, cp.SLODEF, cp.slopoz, :CPODMIOT,
           :CKONTRAKT, :STATUS, MARZAKOSZT, MARZAKOSZTPR, OFETYP, waluta,
           current_date, current_date
          from oferty o
            left join cpodmioty cp on (cp.ref = :cpodmiot)
          where o.ref = :orgref;
      msg = 'Kopiowanie oferty powiodlo sie';
    end else if (tryb = 1) then
    begin
      execute procedure gen_ref('OFERTY') returning_values :newref;
      insert into oferty (REF, NUMER, SYMBOL, SLODEF, SLOPOS, CPODMIOT,
          CKONTRAKT, SPOSDOST, ODBIORCAID,  ODBNAZIM, ODBTELEFON, ODBEMAIL,
          ODBNAZWA, ODBULICA, ODBMIASTO, ODBKODP, PLATNOSC, TERMIN, ZADATEK,
          TYPFAK, RABATWEWN, RABATZEWN, SPOSKALK, FLAGI, OPISWEWN, OPISZEWN,
          OPAKZWR, STATUS, ZAMOWIENIE, ZRODLO, WYSYLKA, WALUTA, TABKURS, KURS,
          MAGAZYN, KOSZTDOST, MARZAKOSZT, MARZAKOSZTPR, SYMBOLZEWN, OFETYP,
          ORGOFERTA, POPOFERTA, DATA, DATAWAZN,
          SLONAZWA)
        select :newref, NUMER, SYMBOL, SLODEF, SLOPOS, CPODMIOT,
            CKONTRAKT, SPOSDOST, ODBIORCAID,  ODBNAZIM, ODBTELEFON, ODBEMAIL,
            ODBNAZWA, ODBULICA, ODBMIASTO, ODBKODP, PLATNOSC, TERMIN, ZADATEK,
            TYPFAK, RABATWEWN, RABATZEWN, SPOSKALK, FLAGI, OPISWEWN, OPISZEWN,
            OPAKZWR, :STATUS, ZAMOWIENIE, ZRODLO, WYSYLKA, WALUTA, TABKURS, KURS,
            MAGAZYN, KOSZTDOST, MARZAKOSZT, MARZAKOSZTPR, SYMBOLZEWN, OFETYP,
            case when coalesce(ORGOFERTA,0) = 0 then ref else ORGOFERTA end, ref, current_date, current_date,
            SLONAZWA
          from oferty
          where ref = :orgref;
      update oferty set status = :oldstatus where ref = :orgref;
      msg = 'Zakończone generowanie
nowej wersji oferty';
    end 
    if (koppoz = 1) then
    begin
      insert into OFERPOZ (OFERTA, KTM, WERSJAREF, NAZWA, JEDN, MIARA, ILOSC, CENA_FABN,
          RABAT_FABN, CENA_ZAKN, MARZAPR, CENACEN, RABAT, CENANET, GR_VAT, VAT,
          CENABRU, WARTZAK, WARTNET, WARTBRU, MARZA, OPIS, DOSTAWCA, TYP, FAZA,
          ROZDZIAL, FLAGI, UKRYTA, WALCEN)
        select :newref, KTM, WERSJAREF, NAZWA, JEDN, MIARA, ILOSC, CENA_FABN,
            RABAT_FABN, CENA_ZAKN, MARZAPR, CENACEN, RABAT, CENANET, GR_VAT, VAT,
            CENABRU, WARTZAK, WARTNET, WARTBRU, MARZA, OPIS, DOSTAWCA, TYP, FAZA,
            ROZDZIAL, FLAGI, UKRYTA, WALCEN
          from oferpoz
          where oferta = :orgref;
    end 
  end
  suspend;
end^
SET TERM ; ^
