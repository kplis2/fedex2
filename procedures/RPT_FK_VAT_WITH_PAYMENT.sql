--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_VAT_WITH_PAYMENT(
      PERIODID varchar(6) CHARACTER SET UTF8                           ,
      VREG varchar(10) CHARACTER SET UTF8                           ,
      DATA date,
      COMPANY integer)
  returns (
      NAG integer,
      REF integer,
      LP integer,
      VATREG varchar(10) CHARACTER SET UTF8                           ,
      VATREGNR integer,
      TRANSDATE date,
      DOCDATE date,
      DATAOP date,
      DOCSYMBOL varchar(20) CHARACTER SET UTF8                           ,
      KODKS varchar(20) CHARACTER SET UTF8                           ,
      BKREG varchar(10) CHARACTER SET UTF8                           ,
      BKREGNR integer,
      KONTRAHKOD varchar(255) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      ADRES varchar(255) CHARACTER SET UTF8                           ,
      NIP varchar(15) CHARACTER SET UTF8                           ,
      NETV numeric(14,2),
      VATV numeric(14,2),
      BRUVW numeric(14,2),
      BRUV numeric(14,2),
      NR integer,
      SLODEF integer,
      SLOPOZ integer,
      SYMBFAK varchar(20) CHARACTER SET UTF8                           ,
      KONTOFK KONTO_ID)
   as
declare variable docref integer;
declare variable docsymbol2 varchar(20);
declare variable bkreg2 varchar(10);
declare variable bkregnr2 integer;
declare variable refp integer;
declare variable VATREGTYPE integer; --<<PR73894: PL>>
begin
  -- PAWEL W REALIZACJI
  -- TO FIX
  ref = 0;
  lp = 0;

  for select B.REF, cast(b.transdate as date), cast(b.docdate as date), B.vatreg,
      b.vnumber, b.bkreg, b.number, B.NIP, b.contractor, b.symbol,
      P.slodef, P.slopoz, P.symbfak, P.kontofk, P.ref, v.vatregtype --<<PR73894: PL>>
    from bkdocs B
    join VATREGS V on (B.vatreg = V.symbol and B.company = V.company)
    join BKDOCTYPES T on (B.doctype = t.ref)
    left join decrees D on (D.bkdoc = B.ref)
    join rozrachp P on (P.decrees = D.ref )
    left join rozrach R on (P.slodef = R.slodef and P.slopoz = R.slopoz and
      P.symbfak = R.symbfak and P.kontofk = R.kontofk and B.company = R.company)
    where
      B.vatperiod = :periodid and b.status > 0
      and ((:vreg is null) or (:vreg = '') or (b.vatreg = :vreg))
      and B.company = :company --and v.vtype in (3,4,5,6)
--<<PR73894: PL
      and (V.VATREGTYPE in (1,2)
                  or (V.VATREGTYPE = 3
                    and exists(select first 1 1 from bkvatpos join grvat on (grvat.symbol = bkvatpos.taxgr) where bkvatpos.bkdoc = B.ref and grvat.vatregtype in (1,2))))
-->>
    order by B.vatreg, b.vnumber
  into :docref, :transdate, :docdate, :vatreg,
       :vatregnr,:bkreg, :bkregnr, :nip, :kontrahkod, :docsymbol,
       :slodef, :slopoz, :symbfak, :kontofk, :refp, :vatregtype   --<<PR73894: PL>>
  do begin
    nr = 0;
    select substring(kontofk from 1 for 20), adres, nazwa from get_dict_data(:slodef, :slopoz)
      into :kodks, :adres, :nazwa;
    lp = :lp + 1;
    nag = 1;
    ref = ref + 1;
    select sum(NETV), sum(VATV)
      from BKVATPOS
        join GRVAT on (GRVAT.SYMBOL = BKVATPOS.TAXGR)--<<PR73894: PL>>
      where bkdoc = :docref
        and (:vatregtype in (1,2) or (:vatregtype = 3 and (grvat.vatregtype in (1,2))))--<<PR73894: PL>>
      into :netv, :vatv;
    if (exists(select p.data, b1.symbol, b1.bkreg, b1.number, p.winienzl, p.mazl
        , b2.symbol, b2.bkreg, b2.number
      from rozrachp p
        left join decrees d on (P.decrees = D.ref and d.status > 0) -- TOFIX kompensaty i inne badziewia
        left join bkdocs b1 on (b1.ref = d.bkdoc)
        left join fsclracch f on (f.ref = p.fsclracch)
        left join bkdocs b2 on (b2.otable = 'FSCLRACCH' and b2.oref = f.ref)
      where P.slodef = :slodef and P.slopoz = :slopoz and
        P.symbfak = :symbfak and P.kontofk = :kontofk and P.company = :company
        and cast(p.data as date) <= :data)) then
      suspend;
    nag = 0;
    for select p.data, b1.symbol, b1.bkreg, b1.number, p.winienzl, p.mazl
        , b2.symbol, b2.bkreg, b2.number
      from rozrachp p
        left join decrees d on (P.decrees = D.ref and d.status > 0) -- TOFIX kompensaty i inne badziewia
        left join bkdocs b1 on (b1.ref = d.bkdoc)
        left join fsclracch f on (f.ref = p.fsclracch)
        left join bkdocs b2 on (b2.otable = 'FSCLRACCH' and b2.oref = f.ref)
      where P.slodef = :slodef and P.slopoz = :slopoz and
        P.symbfak = :symbfak and P.kontofk = :kontofk and P.company = :company
        and cast(p.data as date) <= :data
    into :dataop, :docsymbol, :bkreg, bkregnr, :bruvw, :bruv, :docsymbol2, :bkreg2, bkregnr2
    do begin
      if (docsymbol2 is not null and :bkreg2 is not null and bkregnr2 > 0) then
      begin
        docsymbol = docsymbol;
        bkreg = bkreg2;
        bkregnr = bkregnr2;
      end
      ref = ref + 1;
      suspend;
      bruvw = null;
      bruv = null;
    end
    netv = null;
    vatv = null;
    bruv = null;
    nag = 2;
    suspend;
  end
end^
SET TERM ; ^
