--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WFQUEUE_INSERT(
      TARGETUUID NEOS_ID,
      METHODUUID NEOS_ID,
      EVENTREF WFEVENTS_ID,
      WDEFUUID NEOS_ID)
  returns (
      QUEUE_REF WFQUEUE_ID)
   as
begin
  if (not exists(select *
    from WFQUEUE WQ
    where WQ.state = 0 and
      WQ.TARGETUUID = :TARGETUUID and
      WQ.METHODUUID = :METHODUUID)) then
    insert into WFQUEUE (TARGETUUID, METHODUUID, EVENTREF, wdefuuid)
    values (:TARGETUUID, :METHODUUID, :EVENTREF, :wdefuuid)
    returning ref
    into :QUEUE_REF;
  --post_event 'NEOS.WORKFLOW.QUEUE';
  suspend;
end^
SET TERM ; ^
