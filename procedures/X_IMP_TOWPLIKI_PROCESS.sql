--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_IMP_TOWPLIKI_PROCESS(
      REF_IMP INTEGER_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING1024)
   as
declare variable IDZDJECIA_IMP INTEGER_ID;
declare variable IDARTYKULU_IMP INTEGER_ID;
declare variable INDEKSKAT_IMP STRING40;
declare variable ZDJECIE_IMP FOTO_BINARY;
declare variable NAZWA_IMP STRING40;
declare variable STATUS_IMP SMALLINT_ID;
declare variable DATAPRZET_IMP TIMESTAMP_ID;
declare variable ID_STATUS_IMP INTEGER_ID;
declare variable glowne_imp integer_id;
declare variable KTM KTM_ID;
declare variable WERSJA INTEGER_ID;
declare variable WERSJAREF INTEGER_ID;
declare variable NUMER SMALLINT_ID;
declare variable IDART STRING40;
begin
  status = 0;
  msg = '';

  if (not exists(select first 1 1 from x_imp_towpliki x where x.ref = :ref_imp)) then begin
    msg = ' Bledny ref_imp:'||coalesce(REF_IMP,0);
    status = 8;
    suspend;
    exit;
  end

  select xt.idzdjecia, xt.idartykulu, xt.indekskatalogowy, xt.nazwa, xt.ZDJECIE, xt.STATUS, xt.IDSTATUS, xt.DATAPRZET, xt.glowne
    from X_IMP_TOWPLIKI xt
      where ref = :ref_imp
  into  :idzdjecia_imp, :idartykulu_imp, :indekskat_imp, :nazwa_imp, :ZDJECIE_imp, :STATUS_imp, :id_STATUS_imp, :DATAPRZET_imp, :glowne_imp;

  if (coalesce(:idartykulu_imp, 0) = 0) then begin
    msg = 'Brak przypisanego towaru';
    status = 8;
    suspend;
    exit;
  end

/*  if (:zdjecie_imp is null) then begin
    msg = 'Brak zdjecia';
    status = 8;
    suspend;
    exit;
  end     */
  idart = cast(idartykulu_imp as string40);

  select first 1 t.ktm
    from towary t
    where t.int_id = :idart
  into :ktm;

  select first 1 w.ref
    from wersje w
    where   w.ktm = :ktm
    order by w.nrwersji

  into :wersjaref;
  if (:wersjaref is null ) then begin
    msg = 'Nie ma zalozonego towaru';
    status = 8;
    suspend;
    exit;
  end

-- numerowanie zdjec // poprawic // przeniesione do trigera
  /*select max(t.numer)
    from towpliki t
    where t.ktm = :ktm
  into :numer;
  if (numer is null) then numer = 1;
  else
    numer = numer + 1; */
  if (not exists(select first 1 1 from towpliki t where t.wersjaref = :wersjaref and t.ktm = :ktm and t.int_id = :idzdjecia_imp)) then
    insert into TOWPLIKI ( KTM, WERSJA, WERSJAREF, INT_ID, INT_DATAOSTPRZETW, X_INDEKS_KAT, X_ZDJECIE,TYTUL, TYP, x_glowne,x_imp_ref)
      values (:ktm, 0, :wersjaref, :idzdjecia_imp, current_timestamp(0), :indekskat_imp, :zdjecie_imp, :nazwa_imp, 1, :glowne_imp,:ref_imp);
  else
    update TOWPLIKI t
      set KTM = :ktm,
          WERSJA = 0,
          TYP = 1,
          WERSJAREF = :wersjaref,
          INT_ID = :idzdjecia_imp,
          INT_DATAOSTPRZETW = current_timestamp(0),
          X_INDEKS_KAT = :indekskat_imp,
          X_ZDJECIE = :zdjecie_imp,
          X_GLOWNE = :glowne_imp,
          x_imp_ref = :ref_imp
      where t.ktm = :ktm
        and t.wersjaref = :wersjaref
        and t.int_id = :idzdjecia_imp;

  status = 1;
  msg = 'ok';
  suspend;
end^
SET TERM ; ^
