--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_INPOST_CANCELPACK_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable DOCUMENT integer;
declare variable root_node integer_id;
declare variable listywysd_ref listywysd_id;
declare variable userlogin string40;
declare variable userpassword string40;
declare variable packageno string40;
begin
  select k.login, k.haslo
    from listywysd lwd
      join spedytwys w on w.sposdost = lwd.sposdost
      join spedytkonta k on (k.spedytor = w.spedytor)
    where lwd.ref = :oref
    into :userlogin, :userpassword;

  if(not exists(select first 1 1 from listywysd where ref is not distinct from :oref))then
    exception ede_ups_listywysd_brak;

  if(coalesce(userlogin,'') ='')then
    exception universal 'Brak loginu użytkownika';

  if(coalesce(userpassword,'') ='')then
    exception universal 'Brak hasła użytkownika';

  val = null;
  parent = null;
  params = null;
  id = 0;
  name = 'CancelPacks';
  suspend;

  root_node = :id;

  val = :userlogin;
  parent = :root_node;
  params = null;
  id = id + 1;
  name = 'email';
  suspend;

  val = :userpassword;
  parent = :root_node;
  params = null;
  id = id + 1;
  name = 'password';
  suspend;

  val = 'TEST'; --jesli chcesz testowac zmien na TEST inaczej PRODUCTION
  name = 'ServiceUrlType';
  id = id + 1;
  parent = root_node;
  suspend;

  for select opk.symbolsped
    from listywysdroz_opk opk
    where opk.listwysd = :oref
    into :packageno
    do begin
      val = :packageno;
      parent = :root_node;
      params = null;
      id = id + 1;
      name = 'packageno';
      suspend;
    end
end^
SET TERM ; ^
