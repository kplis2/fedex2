--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PIT4(
      CURRENTCOMPANY integer,
      PYEAR smallint,
      PMONTH smallint,
      R44 numeric(14,2),
      R45 numeric(14,2),
      R52 numeric(14,2))
  returns (
      P19 integer,
      P20 numeric(14,2),
      P21 numeric(14,2),
      P43 numeric(14,2),
      P46 numeric(14,2),
      P47 numeric(14,2),
      P50 numeric(14,2),
      P51 numeric(14,2),
      P53 numeric(14,2),
      P52 numeric(14,2))
   as
declare variable PERIOD varchar(6);
declare variable PERSON integer;
declare variable INCOME numeric(14,2);
declare variable ZUS numeric(14,2);
declare variable TAX numeric(14,2);
declare variable PREVTAX numeric(14,2);
declare variable edeclaration edeclarations_id;
begin

  if(currentcompany < 0) then
  begin
    edeclaration = abs(currentcompany);

    select p.pvalue from edeclpos p
        join edeclposdefs d on (d.ref = p.edeclposdef)
      where p.edeclaration = :edeclaration
        and d.field = case(:pmonth)
          when 1 then 9
          when 2 then 10
          when 3 then 11
          when 4 then 12
          when 5 then 13
          when 6 then 14
          when 7 then 21
          when 8 then 22
          when 9 then 23
          when 10 then 24
          when 11 then 25
          when 12 then 26
        end
      into P19;

    select p.pvalue from edeclpos p
        join edeclposdefs d on (d.ref = p.edeclposdef)
      where p.edeclaration = :edeclaration
        and d.field = case(:pmonth)
          when 1 then 15
          when 2 then 16
          when 3 then 17
          when 4 then 18
          when 5 then 19
          when 6 then 20
          when 7 then 27
          when 8 then 28
          when 9 then 29
          when 10 then 30
          when 11 then 31
          when 12 then 32
        end
      into P43;

    select p.pvalue from edeclpos p
        join edeclposdefs d on (d.ref = p.edeclposdef)
      where p.edeclaration = :edeclaration
        and d.field = case(:pmonth)
          when 1 then 81
          when 2 then 82
          when 3 then 83
          when 4 then 84
        end
      into P46;

    select p.pvalue from edeclpos p
        join edeclposdefs d on (d.ref = p.edeclposdef)
      where p.edeclaration = :edeclaration
        and d.field = case(:pmonth)
          when 1 then 85
          when 2 then 86
          when 3 then 87
          when 4 then 88
          when 5 then 89
          when 6 then 90
          when 7 then 91
          when 8 then 92
          when 9 then 93
          when 10 then 94
          when 11 then 95
          when 12 then 96
        end
      into P47;

    select p.pvalue from edeclpos p
        join edeclposdefs d on (d.ref = p.edeclposdef)
      where p.edeclaration = :edeclaration
        and d.field = case(:pmonth)
          when 1 then 109
          when 2 then 110
          when 3 then 111
          when 4 then 112
          when 5 then 113
          when 6 then 114
          when 7 then 115
          when 8 then 116
          when 9 then 117
          when 10 then 118
          when 11 then 119
          when 12 then 120
        end
      into P50;

    select p.pvalue from edeclpos p
        join edeclposdefs d on (d.ref = p.edeclposdef)
      where p.edeclaration = :edeclaration
        and d.field = case(:pmonth)
          when 1 then 133
          when 2 then 134
          when 3 then 135
          when 4 then 136
          when 5 then 137
          when 6 then 138
          when 7 then 139
          when 8 then 140
          when 9 then 141
          when 10 then 142
          when 11 then 143
          when 12 then 144
        end
      into P51;

    select p.pvalue from edeclpos p
        join edeclposdefs d on (d.ref = p.edeclposdef)
      where p.edeclaration = :edeclaration
        and d.field = case(:pmonth)
          when 1 then 145
          when 2 then 146
          when 3 then 147
          when 4 then 148
          when 5 then 149
          when 6 then 150
          when 7 then 151
          when 8 then 152
          when 9 then 153
          when 10 then 154
          when 11 then 155
          when 12 then 156
        end
      into P52;

    select p.pvalue from edeclpos p
        join edeclposdefs d on (d.ref = p.edeclposdef)
      where p.edeclaration = :edeclaration
        and d.field = case(:pmonth)
          when 1 then 157
          when 2 then 158
          when 3 then 159
          when 4 then 160
          when 5 then 161
          when 6 then 162
          when 7 then 163
          when 8 then 164
          when 9 then 165
          when 10 then 166
          when 11 then 167
          when 12 then 168
        end
      into P53;
  end else
  ------------------------------------------------------------------------------
  begin
    p19 = 0;
    p20 = 0;
    p21 = 0;
    p46 = 0;
    p47 = 0;
    p52 = r52;
    execute procedure e_func_periodinc(:pyear || :pmonth, 0)
      returning_values :period;
  
    for
      select e.person, sum(p.pvalue)
        from epayrolls pr
          join eprpos p on (p.payroll = pr.ref and pr.tper = :period and pr.empltype = 1)
          join employees e on (e.ref = p.employee)
          join ecolumns c on (p.ecolumn = c.number)
        where pr.company = :currentcompany
          and e.company = :currentcompany
          and ((c.number = 5950 and pr.prtype = 'SOC') --(BS36243)
            or (c.cflags like '%;POD;%' and pr.prtype <> 'SOC' and c.number <> 5950))
        group by e.person
        having sum(p.pvalue) <> 0
        into :person, :income
    do begin
      p19 = p19 + 1;
      p20 = p20 + income;
  
      zus = null;
      tax = null;
      prevtax = null;
      select sum(case when p.ecolumn >= 6100 and p.ecolumn <= 6120 then p.pvalue else 0 end),
             sum(case when p.ecolumn >= 7350 and p.ecolumn <= 7370 then p.pvalue else 0 end),
             sum(case when p.ecolumn = 7380 then p.pvalue else 0 end)
        from epayrolls pr
          join eprpos p on (p.payroll = pr.ref and pr.tper = :period and pr.empltype = 1)
          join employees e on (e.ref = p.employee and e.person = :person)
        where pr.company = :currentcompany
          and e.company = :currentcompany
          and ((p.ecolumn >= 6100 and p.ecolumn <= 6120)
            or (p.ecolumn >= 7350 and p.ecolumn <= 7370)
            or p.ecolumn = 7380)
        into :zus, :tax, :prevtax;
  
      p20 = p20 - coalesce(zus,0);
      p21 = p21 + coalesce(tax,0);
      prevtax = coalesce(prevtax,0);
  
      if (prevtax > 0) then
        p46 = p46 + prevtax;
      else if (prevtax < 0) then
        p47 = p47 - prevtax;
    end
  
    select sum(p.pvalue)
      from epayrolls pr
        join eprpos p on (p.payroll = pr.ref and pr.tper = :period and pr.empltype >= 2)
      where p.ecolumn = 7350
        and pr.company = :currentcompany
        and pr.lumpsumtax = 0
      into :p50;
    p50 = coalesce(p50,0);
  
    p43 = p21;
    p51 = p43 + r45 + p46 - r44 - p47 + p50;
    if (p51 < 0) then
      p51 = 0;
    p53 = p51 - r52;
    if (p53 < 0) then
      p53 = 0;
  end
  suspend;
end^
SET TERM ; ^
