--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWIAD_GETEMAILZAL(
      FORID varchar(1024) CHARACTER SET UTF8                           ,
      FORMAILBOX integer)
  returns (
      TYP smallint,
      PATH varchar(255) CHARACTER SET UTF8                           ,
      REPOSITORY varchar(255) CHARACTER SET UTF8                           ,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      CONTENTID varchar(255) CHARACTER SET UTF8                           )
   as
declare variable ref integer;
begin
  select ref from emailwiad where id = :forid and mailbox=:formailbox into :ref;
  if(:ref is not null) then begin
    for select typ,path,repository,name,contentid
    from emailzal
    where emailwiad=:ref
    into :typ,:path,:repository,:name,:contentid
    do begin
      suspend;
    end
  end
end^
SET TERM ; ^
