--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OFERTY_CREATE_NAGZAM(
      OFERTA integer,
      REJZAM varchar(3) CHARACTER SET UTF8                           ,
      TYPZAM varchar(3) CHARACTER SET UTF8                           ,
      OPERATOR integer,
      DATA timestamp)
  returns (
      NEWZAM integer)
   as
declare variable OFERTPOZ integer;
declare variable NEWPOZZAM integer;
declare variable SLODEF integer;
declare variable SLODEFREF integer;
declare variable SLODEFREF1 integer;
begin
slodef = 0;
select slodef from oferty where ref  = :oferta into :slodef;
execute procedure GEN_REF('NAGZAM') returning_values :newzam;
  select min(slodef.ref) from slodef where slodef.typ = 'KLIENCI' into :slodefref1;
  if(:slodef = :slodefref1) then
    insert into NAGZAM(REF, REJESTR, TYPZAM, KLIENT, MAGAZYN, SPOSDOST,
      SPOSZAP, WALUTA, tabkurs, KURS, OPERATOR, SPRZEDAWCA)
    select :NEWZAM, :REJZAM, :TYPZAM, SLOPOS, MAGAZYN, SPOSDOST,
      PLATNOSC, WALUTA, tabkurs, KURS, :OPERATOR, SPRZEDAWCA
    from oferty where ref = :oferta;
  select min(slodef.ref) from slodef where slodef.typ = 'DOSTAWCY' into :slodefref;
  if(:slodef = :slodefref) then
    insert into NAGZAM(REF, REJESTR, TYPZAM, DOSTAWCA, MAGAZYN, SPOSDOST,
      SPOSZAP, WALUTA, tabkurs, KURS, OPERATOR, SPRZEDAWCA)
    select :NEWZAM, :REJZAM, :TYPZAM, SLOPOS, MAGAZYN, SPOSDOST,
      PLATNOSC, WALUTA, tabkurs, KURS, :OPERATOR, SPRZEDAWCA
    from oferty where ref = :oferta;
  if (:slodef <> :slodefref and :slodef <> :slodefref1) then
    insert into NAGZAM(REF, REJESTR, TYPZAM, MAGAZYN, SPOSDOST,
      SPOSZAP, WALUTA, tabkurs, KURS, OPERATOR, SPRZEDAWCA)
    select :NEWZAM, :REJZAM, :TYPZAM, MAGAZYN, SPOSDOST,
      PLATNOSC, WALUTA, tabkurs, KURS, :OPERATOR, SPRZEDAWCA
    from oferty where ref = :oferta;
  for select REF from OFERPOZ where oferta = :oferta
  into :ofertpoz
  do begin
    execute procedure GEN_REF('POZZAM') returning_values :newpozzam;
    insert into POZZAM(REF, ZAMOWIENIE, NUMER, ORD, WERSJAREF,
       JEDN, ILOSC, CENACEN, RABAT, WALCEN, GR_VAT)
    select :newpozzam, :newzam, numer, 1, wersjaref,
      jedn, ilosc, cenacen, rabat, walcen, gr_vat
    from oferpoz
    where ref= :ofertpoz
      and ukryta = 0
    order by oferpoz.numer;
  end
end^
SET TERM ; ^
