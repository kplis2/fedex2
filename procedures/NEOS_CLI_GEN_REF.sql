--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NEOS_CLI_GEN_REF
  returns (
      REF INTEGER_ID)
   as
begin
  ref = gen_id(gen_neoscli_data_id,1);
  suspend;
end^
SET TERM ; ^
