--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REZ_BLOK_BILANSUJ(
      MAGAZYN char(3) CHARACTER SET UTF8                           ,
      KTM varchar(80) CHARACTER SET UTF8                           ,
      WERSJA integer,
      NUMER integer,
      NUMERPLUS smallint)
   as
declare variable ilminus numeric(14,4);
declare variable ilplus numeric(14,4);
declare variable pozzam integer;
declare variable status char(1);
declare variable zreal smallint;
declare variable dokummag integer;
declare variable zam integer;
begin
  for select s.ilminus, s.ilplus, s.zamowienie, s.pozzam, s.status, s.zreal, s.dokummag
    from STANYREZ S
    where S.MAGAZYN = :MAGAZYN and s.ktm = :ktm and s.wersja = :wersja and s.numer > :numer
    order by numer asc
    into :ilminus, :ilplus, :zam, :pozzam, :status, :zreal, :dokummag
  do begin
    update stanyrez s set s.numer = s.numer +1 * :numerplus
      where s.zamowienie = :zam and s.pozzam = :pozzam and s.zreal = :zreal and s.status = :status and s.dokummag = :dokummag;
  end
end^
SET TERM ; ^
