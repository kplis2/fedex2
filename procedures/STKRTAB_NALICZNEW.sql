--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STKRTAB_NALICZNEW(
      TABELA integer,
      ODDATY timestamp,
      DODATY timestamp,
      ODKTM varchar(40) CHARACTER SET UTF8                           ,
      DOKTM varchar(40) CHARACTER SET UTF8                           ,
      MINDNIDOST integer,
      MINCYKLDOST integer,
      MINREZTYP smallint,
      MINREZ numeric(14,2),
      MINROZNICA numeric(14,4),
      TYLKOZEWNETRZNE smallint,
      ZAKRESSTR varchar(20) CHARACTER SET UTF8                           ,
      TRYB smallint)
  returns (
      STATUS integer,
      NUMROWS integer)
   as
declare variable zakres smallint;
declare variable ktm varchar(40);
declare variable wersja integer ;
declare variable minstan numeric(14,4);
declare variable maxstan numeric(14,4);
declare variable pilosc numeric(14,4);
declare variable pwartosc numeric(14,2);
declare variable rilosc numeric(14,4);
declare variable rwartosc numeric(14,2);
declare variable cnt integer;
begin
  select ZAKRES from STKRTAB where ref = :tabela into :zakres;
  status = 0;
  numrows = 0;
  for select KTM,WERSJA,ILPRZY,WARTPRZY,ILROZ,WARTROZ
  from MAG_OBROTY_LISTA(:zakresstr, :oddaty, :dodaty)
  where ((:odktm = '') or (:odktm is null) or (:odktm <= KTM))
    and ((:doktm = '') or (:doktm is null) or (:doktm >= KTM))
  order by KTM,WERSJA
  into :ktm, :wersja, :pilosc, :pwartosc, :rilosc, :rwartosc
  do begin
    minstan = null; maxstan = null;
    select MINTAB,MAXTAB
    from  STKRTAB_POZINFO(:tabela, :zakresstr, :oddaty, :dodaty, :ktm, :wersja, :mindnidost, :mincykldost, :minrez, :minreztyp,  :minroznica, :TYLKOZEWNETRZNE,
                              :pilosc, :pwartosc, :rilosc, :rwartosc)
    into :minstan, :maxstan;
    cnt = null;
    if(:minstan > 0 and :maxstan > 0)then begin
      select count(*) from stkrpoz where tabela = :tabela and ktm = :ktm and wersja = :wersja into :cnt;
      if(:cnt is null) then cnt = 0;
      if(:cnt = 0 and :tryb<> 1)then begin
        insert into stkrpoz(tabela, ktm, wersja,ilmin, ilmax, status) values (:tabela, :ktm, :wersja, :minstan, :maxstan,1);
        numrows = :numrows +1;
      end else if(:cnt > 0 and :tryb <> 0) then begin
        update stkrpoz set status = 1, ilmin = :minstan, ilmax = :maxstan
        where tabela = :tabela and ktm = :ktm and wersja = :wersja and ((ilmin <> :minstan) or (ilmax <> :maxstan));
        numrows = :numrows +1;
       end
    end
  end
  UPDATE STKRTAB set LDATAaktu=CURRENT_DATE WHERE ref=:TABELA;
  status = 1;
end^
SET TERM ; ^
