--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ORDER_PRSCHEDZAM(
      PRSCHEDULE integer,
      OLDN integer,
      NEWN integer)
   as
declare variable ref integer;
declare variable number integer;
begin
  --firts we find minor number
  if (oldn < newn) then
    newn = oldn;

  number = newn;
  for
    select ref
      from PRSCHEDZAM
      where prschedule = :prschedule and number >= :number
        and status < 2
      order by number, ord
      into :ref
  do begin
    update PRSCHEDZAM set number = :number, ord = 2
      where ref = :ref;
    number = number + 1;
  end

  update PRSCHEDZAM set ord = 1
    where prschedule = :prschedule and number >= :newn;
end^
SET TERM ; ^
