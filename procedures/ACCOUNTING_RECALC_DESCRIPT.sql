--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ACCOUNTING_RECALC_DESCRIPT(
      SLOWNIK integer,
      YEARID integer,
      COMPANY smallint,
      BKSYMBOL BKSYMBOLS_ID)
   as
  declare variable descript varchar(80);
  declare variable account account_id;
  declare variable accounting integer;
  declare variable fulldescript varchar(255);
  declare variable sql varchar(1024);
  declare variable bkaccount integer;
  declare variable nlevel smallint;
  declare variable separator char(1);
  declare variable len smallint;
  declare variable accmask account_id;
  declare variable k smallint;
  declare variable tmplevel smallint;
  declare variable sql2 varchar(1024);
begin
  if(:slownik is null) then slownik = 0;
  if(:yearid is null) then yearid = 0;

  sql = ' select distinct a.bkaccount, a.nlevel ';
  sql = sql || ' from accstructure a join accounting ac ';
  sql = sql || ' on ac.bkaccount = a.bkaccount ';
  sql = sql || ' where a.dictdef = ' || :slownik;
  sql = sql || ' and ac.yearid = ' || :yearid;
  if(:company is not null) then
    sql = sql || ' and ac.company = ' || :company;
  for execute statement sql
    into :bkaccount, :nlevel
  do begin
    -- budujemy maske na podstawie konta syntetycznego i jezeli wskazano na podstawie symbolu syntetyki
    select bkaccounts.symbol
      from bkaccounts
      where bkaccounts.ref = :bkaccount
    into :accmask;
      for select distinct nlevel, coalesce(separator,''), len
      from accstructure a
      where a.bkaccount = :bkaccount
      order by nlevel asc
      into  :tmplevel, :separator, :len
      do begin
        -- początek maski numeru konta tworzy symbol konta syntetycznego
        accmask = :accmask || :separator;
        k=0;
        while(:k < :len)
        do begin
          -- jeżeli mamy symbol tworzący konto to możemy uszczególowić mask o ten symbol
          if(:tmplevel=:nlevel and :bksymbol is not null) then begin
            accmask = :accmask || :bksymbol;
            break;
          end
          accmask = :accmask || '_';
          k = :k + 1;
        end
      end

     -- Mając zbudowaną mask możemy zaktualizować konta dla danego slownika/kartoteki, roku, company i symbolu
     sql2 = ' select ac.account, ac.ref, ac.company ';
     sql2 = sql2 || ' from accounting ac ';
     sql2 = sql2 || ' where ac.yearid = ' || :yearid;
     sql2 = sql2 || ' and ac.account like ''' || :accmask || '''';
     if(:company is not null) then             -- niektore kartoteki nie posiadaja rozroznienia na company...
       sql2 = sql2 || ' and ac.company = ' || :company;
     for execute statement sql2
       into :account, :accounting, :company    -- ...stad odczytanie wartosc company z accounting
     do begin
       execute procedure ACCOUNTING_GET_DESCRIPT(:account, :yearid, :company)
         returning_values :descript, :fulldescript;
       update accounting set descript = :descript where ref = :accounting;
       update accounting set fulldescript = :fulldescript where ref = :accounting;

       account = '';
       accounting = null;
       descript = '';
       fulldescript = '';
     end
  end
end^
SET TERM ; ^
