--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_MAKE_TRIAL_YEARCLOSING(
      COMPANY integer,
      FORYEAR integer)
   as
declare variable ACCOUNT ACCOUNT_ID;
declare variable CURRENCY varchar(3);
declare variable DEBIT numeric(14,2);
declare variable CREDIT numeric(14,2);
declare variable BO_DEBIT numeric(14,2);
declare variable BO_CREDIT numeric(14,2);
declare variable ACCOUNTING_REF integer;
declare variable PERIODBO varchar(6);
declare variable NACCOUNT ACCOUNT_ID;
declare variable RES smallint;
declare variable DIST integer;
declare variable DECREE integer;
declare variable BALANCETYPE smallint;
declare variable MULTIVALUEDIST smallint;
declare variable EBDEBIT numeric(14,2);
declare variable EBCREDIT numeric(14,2);
declare variable EBBO_DEBIT numeric(14,2);
declare variable EBBO_CREDIT numeric(14,2);
begin
  periodbo = cast(foryear + 1 as varchar(4)) || '00';

  if (exists(select ref from bkdocs where period = :periodbo
                and autobo = 1 and company = :company)) then
  begin
    exception AUTOBO_EXISTS;
  end

  update turnovers set debit = 0, credit = 0, ebdebit = 0, ebcredit = 0
    where period = :periodbo and company = :company;

  for
    select A.account, A.currency, b.saldotype, sum(T.debit), sum(T.credit), sum(T.ebdebit), sum(T.ebcredit)
      from accounting A
        join turnovers T on (T.accounting = A.ref)
        join bkaccounts b on (b.ref = a.bkaccount)
      where A.yearid = :foryear and A.bktype in (0, 3)
        and a.company = :company
      group by A.account, A.currency, b.saldotype
      into :account, :currency, :balancetype, :debit, :credit, :ebdebit, :ebcredit
  do begin
    bo_debit = 0;
    bo_credit = 0;
    ebbo_debit = 0;
    ebbo_credit = 0;

    naccount = null;
    select naccount from bkaccontschng
      where oaccount = :account and yearid = :foryear
      and company = :company
      into :naccount;
    if (naccount is not null) then
      account = naccount;

    if (abs(debit) > abs(credit)) then begin
      bo_debit = debit - credit;
      ebbo_debit = ebdebit - ebcredit;
    end else begin
      bo_credit = credit - debit;
      ebbo_credit = ebcredit - ebdebit;
    end
    if (balancetype = 0) then
    begin
      bo_debit = bo_debit - bo_credit;
      bo_credit = 0;
      ebbo_debit = ebbo_debit - ebbo_credit;
      ebbo_credit = 0;
    end else if (balancetype = 2) then
    begin
      bo_credit = bo_credit - bo_debit;
      bo_debit = 0;
      ebbo_credit = ebbo_credit - ebbo_debit;
      ebbo_debit = 0;
    end

    if (bo_debit <> 0 or bo_credit <> 0) then
    begin
      execute procedure check_account(company, account, foryear + 1, 0)
        returning_values res, dist, dist, dist, dist, dist, dist, multivaluedist;
      accounting_ref = null;
      select ref from accounting
        where account = :account and currency = :currency and yearid = :foryear + 1 and company = :company
          into :accounting_ref;
      if (accounting_ref is null) then
      begin
        execute procedure GEN_REF('ACCOUNTING') returning_values accounting_ref;
        insert into accounting (ref, yearid, account, currency, company)
          values (:accounting_ref, :foryear+1, :account, :currency, :company);
      end
      if (not exists (select first 1 1 from turnovers t where t.accounting = :accounting_ref
        and t.period = :periodbo and t.company = :company)
      ) then
        insert INTo turnovers (accounting, period, debit, credit, ebdebit, ebcredit, company, account, yearid, bktype)
          values (:accounting_ref, :periodbo, :bo_debit, :bo_credit, :ebbo_debit, :ebbo_credit, :company, :account, :foryear + 1, 0);
      else
        update turnovers set debit = debit + :bo_debit, credit = credit + :bo_credit,
          ebdebit = ebdebit + :ebbo_debit, ebcredit = ebcredit + :ebbo_credit
          where accounting = :accounting_ref and period = :periodbo;
    end
  end

  for
    select ref
      from decrees where period = :periodbo and status > 1 and company = :company
      into :decree
  do
    execute procedure compute_turnovers(decree);
end^
SET TERM ; ^
