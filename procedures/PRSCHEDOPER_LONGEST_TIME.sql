--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDOPER_LONGEST_TIME(
      PRSCHEDGUIDE integer,
      PRSCHEDOPER integer,
      TIMETOEND double precision,
      PREFIX varchar(40) CHARACTER SET UTF8                           )
   as
declare variable ref integer;
declare variable tmp double precision;
begin
  if (prschedoper = 0) then
  begin
    for
      select o.ref
        from prschedopers o
          left join prschedoperdeps d on (d.depto = o.ref)
          left join prschedopers f on (f.ref = d.depfrom)
        where o.guide = :prschedguide and o.activ = 1
          and (d.depfrom is null or f.status > 2 or f.activ = 0)
        into prschedoper
    do begin
      execute procedure prschedoper_longest_time(:prschedguide, :prschedoper, 0, :prefix);
    end
  end else begin
    timetoend = 0;
    select max(g.longesttimetoend)
      from prschedoperdeps d
        left join prgantt g on (g.prschedoper = d.depfrom)
      where d.depto = :prschedoper
      into timetoend;
    update prgantt set longesttimetoend = coalesce(:timetoend,0) + coalesce(worktime,0) where prschedoper = :prschedoper;
    for
      select f.ref
        from prschedoperdeps d
          left join prschedopers f on (f.ref = d.depto)
        where d.depfrom = :prschedoper and f.activ = 1
        into ref
    do begin
      execute procedure prschedoper_longest_time(:prschedguide, :ref, 0, :prefix);
    end
  end
end^
SET TERM ; ^
