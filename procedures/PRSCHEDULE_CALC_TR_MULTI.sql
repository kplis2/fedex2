--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDULE_CALC_TR_MULTI(
      PRSCHEDULE integer,
      LASTOPER integer,
      OPERCOUNT integer)
  returns (
      SCHEDOPER integer,
      CNT integer)
   as
declare variable curtime timestamp;
declare variable schednum integer;
declare variable isoper smallint;
declare variable status smallint;
declare variable tstart timestamp;
declare variable tdepend timestamp;
declare variable tend timestamp;
declare variable machine integer;
declare variable verifiednum integer;
declare variable shoper integer;
declare variable worktime double precision;
declare variable workplace varchar(20);
declare variable prdepart varchar(20);
declare variable matreadytime timestamp;
declare variable laststatus smallint;
declare variable workload double precision;
declare variable tprevend timestamp;
declare variable rstart timestamp;
declare variable rend timestamp;
declare variable rmachine integer;
declare variable rschedoper integer;
declare variable latenes double precision;
declare variable minlatenes double precision;
declare variable minspace integer;
declare variable "ROUND" integer;
declare variable minlength double precision;
declare variable rthoper integer;
declare variable tdependr timestamp;
declare variable rstartfwt timestamp;
declare variable tstartfwt timestamp;
declare variable tendfwt timestamp;
declare variable machinecount integer;
declare variable altoper smallint;
declare variable brakmat smallint;
declare variable sql varchar(1024);
declare variable c integer;
declare variable depfrom integer;
declare variable prganttref bigint;
declare variable rprganttref bigint;
declare variable realtime timestamp;
declare variable prschedguide integer;
begin
  c = 0;
  laststatus = 2;
  select mintimebetweenopers, roundstarttime,  fromdate, prdepart
      from prschedules
      where ref = :prschedule
      into :minspace, :round, :curtime, :prdepart;
  minlength = 1440;
  minlength = 1/:minlength;
  if(:round > :minspace or (:minspace is null) ) then minspace = :round;
  minlength = :minlength * :minspace;
  if(:lastoper = 0) then
  begin
    verifiednum = 0;
    --ustawianie ostatniej operacji readytoharm
    update prgantt set readytosched = 0 where prschedule = :prschedule and gtype = 0;
    for select c.ref
      from prgantt c
        left join prschedopers o on (o.ref = c.prschedoper)
        left join prschedoperdeps d on (d.depfrom = o.ref)
      where c.PRSCHEDULE = :prschedule and c.VERIFIED = 0 and c.gtype = 0
        and d.depto is null --and c.prschedoper = 6036
      into :prganttref
    do begin
      update prgantt set readytosched = 1 where ref = :prganttref;
    end
    --wyliczenie czasu do zakończenia przewodnika dla każdej operacji
    for
      select prschedguide
        from prgantt
        where prschedule = :prschedule and verified = 0
        group by prschedguide
        order by prschedguide
        into prschedguide
    do begin
      execute procedure prschedguide_time_to_end(:prschedguide, '');
      update prgantt set longesttimetoend = worktime where prschedguide = :prschedguide and longesttimetoend is null;
    end
  end else
    select VERIFIED, STATUS from prgantt where prschedoper=:lastoper into :verifiednum, :laststatus;
  cnt = 0;
  if(:curtime is null or (:curtime < current_timestamp(0)) ) then curtime = current_timestamp(0);
  while(:cnt < :opercount and :lastoper >= 0)
  do begin
    verifiednum = :verifiednum + 1;
    -- dla wszystkich operacji niezweryfikowanych, ktore nie posiadaja niezweryfikowanych operacji poprzedzajacych, okresl czas dostepnosci materialow
    for select first 1 b.ref, b.realtime
      from prgantt b
      where b.PRSCHEDULE = :prschedule and b.VERIFIED = 0
        and b.readytosched = 1 --and b.prschedoper = 6036
      order by b.realtime,
        b.prschedule, b.verified,
        b.status desc
      into :prganttref, :realtime
    do begin
      execute procedure PRSCHEDULE_CHECKMATERIALSTATE(:prganttref);
    end
    schedoper = null;
    rschedoper = null;
    minlatenes = null;
    --dla kazdejoperacji okrel czas "opoznienia" startu w stosunku do miejsca, w ktorym mozna posadzic operacje
    if(exists (select ref from prgantt where PRSCHEDULE = :prschedule and VERIFIED = 0 and readytosched = 1 and materialreadytime is not null)) then
       brakmat = 1;
    else
       brakmat = 0;
    for
      select a.ref, a.depfrom, a.timefrom, a.timeto, a.STATUS, a.prMACHINE,
          a.prshoper, a.materialreadytime, a.workplace, a.worktime, a.prschedoper, a.realtime
        from PRGANTT A
      where A.PRSCHEDULE = :prschedule and A.VERIFIED = 0 and a.readytosched = 1 --and a.prschedoper = 6036
            and (a.materialreadytime is not null or a.verified=:brakmat)
      order by a.realtime, a.longesttimetoend, a.worktime desc
      into :prganttref, depfrom, :tstart,  :tend, :status, :machine,
          :shoper, :matreadytime, :workplace, worktime, schedoper, :realtime
    do begin
      if(:minlatenes < :minlength) then
        break;  -- jesli ostatni odstep jest wystarczajaco maly, to nei szukam dalej
      tdepend = null;
      --okreslenie max czasu zakończenia
      select min(f.timefrom)
        from prgantt p
          left join prschedoperdeps d on (d.depfrom = p.prschedoper)
          left join prgantt f on (f.prschedoper = d.depto)
        where p.prschedoper = :schedoper
        into :tdepend;
      if(tdepend is null) then tdepend = realtime;
      if(tend is null or tend > tdepend) then
        tend = tdepend;
      --sprawdzenie, czy start nie wypad w czasie przerwy pracy - jesli tak, to cofamy dzien do tylu
      execute procedure PRSCHED_CALENDAR_FW_TR(:prdepart,:tend) returning_values
         :tendfwt;
      tstart = null;
      execute procedure pr_calendar_checkendtime_tr(:prdepart, :tendfwt, :worktime)
        returning_values tstart;
      --sprawdzenie zaleznosci materialowej
      if(:matreadytime > :tstart) then
        tstart  = :matreadytime;
      if(:workplace <>'') then
      begin
        select first 1 starttime, endtime, machine
          from PRSCHEDULE_FIND_MACHINE_TORIGHT(:prganttref,:tendfwt, 0)
          into tstart, tend, machine;
        if(:machine is null) then
        begin
          execute procedure prschedule_checkoperstarttime(:prganttref, :tstart) returning_values :tstart;
          execute procedure pr_calendar_checkendtime(:prdepart, :tstart, :worktime) returning_values :tend;
        end
        rschedoper = schedoper;
        rprganttref = prganttref;
        rmachine = :machine;
        rstart = :tstart;
        rend = :tend;
        minlatenes = :latenes;
        rstartfwt = :tstartfwt;
      end else begin
        machine = null;
        execute procedure prschedule_checkoperstarttime(:prganttref, :tstart) returning_values :tstart;
        execute procedure pr_calendar_checkendtime(:prdepart, :tstart, :worktime) returning_values :tend;
        rschedoper = schedoper;
        rprganttref = prganttref;
        rmachine = machine;
        rstart = :tstart;
        rstartfwt = :tstart;
        rend = :tend;
        minlatenes = 0;
      end
      c = :c + 1;
      if (rschedoper is not null and rstart is not null and rend is not null) then
        break;
    end
    if(:rschedoper is null) then
    begin
      -- zakończenie harmonogramowania operacji
      prganttref = -1;
      exit;
    end
    update prgantt set timefrom = :rstart, timeto = :rend, prMACHINE = :rmachine, verified  = :verifiednum
      where ref=:rprganttref;
    --ustawianie readytoharm na innych
    for select o.ref
      from prschedoperdeps d
      left join prschedopers o on (o.ref = d.depfrom)
      where d.depto = :rschedoper and o.activ = 1
      into :rthoper
    do begin
      update prgantt t set t.readytosched = 1 where t.prschedoper = :rthoper;
    end
    schedoper = rschedoper;
    cnt = :cnt + 1;
  end
end^
SET TERM ; ^
