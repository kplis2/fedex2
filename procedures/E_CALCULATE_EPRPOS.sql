--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_CALCULATE_EPRPOS(
      EMPLOYEE integer,
      PAYROLL integer,
      FROMECOL integer)
   as
declare variable ALGTYPE integer;
declare variable ECOLUMN integer;
declare variable CABLOCK varchar(20);
begin
/*MWr: Personel - funkcja oblicza skladniki placowe dla pracownika EMPLOYEE na
  liscie plac PAYROLL poczawszy od skladnika wiekszego od zadanego FROMECOL.
  Jezeli FROMECOL jest > 0 przeliczone zostana tylko skladniki 'autoprzeliczalne',
  w przecwinym wypadku zostana uruchomione wszystkie algorytmy placowe. Funkcja
  obsluguje blokade modyfikacji skladnikow autoprzeliczalnych (PR31748)*/

  execute procedure get_config('EPRPOS_CAUTOBLOCK',2)
    returning_values :cablock;

  select ealgtype from epayrolls where ref = :payroll
    into :algtype;

  rdb$set_context('USER_TRANSACTION', 'EPRPOS_DONOTCALC', 1);  --PR53533 (zastepuje ustawienie cauto=1, co implikuje
                                                               --koniecznosc wylaczenia ustawienia cauto=1 w E_CALC_POSITION)
  for
    select a.ecolumn from ealgorithms a
      left join eprpos p on (:cablock = '1' and p.ecolumn = a.ecolumn and p.payroll = :payroll and p.employee = :employee)
      where a.ealgtype = :algtype
        and (a.autocalculate = 1 or :fromecol = 0)
        and a.ecolumn > :fromecol
        and p.chgoperator is null
      order by a.ecolumn
      into :ecolumn
  do
    execute procedure e_calc_position(:employee, :payroll, :algtype, :ecolumn);

  rdb$set_context('USER_TRANSACTION', 'EPRPOS_DONOTCALC', 0);

end^
SET TERM ; ^
