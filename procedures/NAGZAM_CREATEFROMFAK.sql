--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGZAM_CREATEFROMFAK(
      FAKTURA integer,
      TYPZAM varchar(3) CHARACTER SET UTF8                           ,
      REJZAM varchar(3) CHARACTER SET UTF8                           ,
      STANZAM integer,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           )
  returns (
      ZAM integer,
      ZAMSYMBOL varchar(30) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE STAN CHAR(1);
DECLARE VARIABLE POZZAMREF INTEGER;
DECLARE VARIABLE POZFAKREF INTEGER;
DECLARE VARIABLE PROFORMA INTEGER;
begin
  zam = 0;
  zamsymbol = '';
  if(:magazyn is null) then magazyn = '';
  /*nagowek zamówienia*/
  select TYPFAK.nieobrot from TYPFAK join NAGFAK on (NAGFAK.typ = TYPFAK.symbol)
  where nagfak.ref = :faktura into :proforma;
  if(:proforma is null) then proforma = 0;
  execute procedure GEN_REF('NAGZAM') returning_values :zam;
  insert into NAGZAM(REF, REJESTR, TYPZAM, KLIENT, PLATNIK, DOSTAWCA, FAKTURANT,
      UZYKLI, MAGAZYN, DATAZM, DATAWE, ODBIORCAID, ODBIORCA,
      DULICA, DNRDOMU, DNRLOKALU, DMIASTO, DPOCZTA,
      TERMDOST, SPOSDOST, TRASADOST, WYSYLKA,
      SPOSZAP, TERMZAP, RABAT,
      STAN, WALUTA, TABKURS, KURS, WALUTOWE,
      KATEGA, KATEGB, KATEGC, SPRZEDAWCAZEW, CKONTRAKTY,
      OPERATOR, SPRZEDAWCA, UWAGI, FAKDEPEND, FAKTURA, SKAD,BN)
  select :zam, :rejzam, :typzam, KLIENT, PLATNIK, DOSTAWCA, DOSTAWCA,
      UZYKLI, (case :magazyn  when '' then MAGAZYN else :magazyn end), DATA, DATA, ODBIORCAID, ODBIORCA,
      DULICA, DNRDOMU, DNRLOKALU, DMIASTO, DMIASTO,
      TERMDOST, SPOSDOST, TRASADOST, WYSYLKA,
      SPOSZAP, cast((DATA - TERMZAP) as integer), RABAT,
      'N', WALUTA, TABKURS, KURS, WALUTOWA,
      KATEGA, KATEGB, KATEGC, SPRZEDAWCAZEW, CKONTRAKTY,
      OPERATOR, SPRZEDAWCA, UWAGI, (case :proforma when 0 then 1 else  0 end) , (case :proforma when 0 then ref else null end), (case :proforma when 0 then 2 else  0 end),BN
  from NAGFAK where ref=:faktura;
  /*dodanie pozycji*/
  for select REF from POZFAK where dokument = :faktura
  order by POZFAK.numer
  into :pozfakref
  do begin
    execute procedure GEN_REF('POZZAM') returning_values :pozzamref;
    insert into POZZAM(REF, ZAMOWIENIE, NUMER, ORD, KTM, WERSJA, WERSJAREF,
       JEDN, JEDNO, ILOSCO, ILOSC, ILOSCM,
       CENACEN, WALCEN,  RABAT, RABATTAB,
       CENANET, CENABRU, WARTNET, WARTBRU,
       OPK, PREC)
    select :pozzamref, :zam, NUMER, 1, KTM, WERSJA, WERSJAREF,
       JEDN, JEDNO, ILOSCO, ILOSC, ILOSCM,
       CENACEN, WALCEN,  RABAT, RABATTAB,
       CENANET, CENABRU, WARTNET, WARTBRU,
       OPK, PREC
    from POZFAK where ref = :pozfakref;
    update POZFAK set FROMPOZZAM = :pozzamref where REF=:pozfakref;
  end
  select ID from NAGZAM where ref=:zam into :zamsymbol;
end^
SET TERM ; ^
