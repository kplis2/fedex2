--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_RCP_IMP_ROGER(
      REF integer)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
   as
declare variable DOCUMENT integer;
declare variable CARDID varchar(40);
declare variable GATEID varchar(40);
declare variable DATA timestamp;
declare variable EVENTID varchar(40);
declare variable CODE integer;
declare variable RCOUNT integer;
begin

  for select ID from EDEDOCSP
      where EDEDOCH=:ref and coalesce(PARENT,1) = 1
      into :document
  do begin
      select substring(val from 1 for 40) from ededocsp edp where ededoch=:ref and edp.parent = :document and edp.name = '3' into :code;
    if (code = 1) then
    begin
      select substring(val from 1 for 40) from ededocsp edp where ededoch=:ref and edp.parent = :document and edp.name = '0' into :eventid;

      select cast(substring(val from 1 for 10) as date) from ededocsp edp where ededoch=:ref and edp.parent = :document and edp.name = '1' into :data;
      select substring(:data from 1 for 10)||' '||substring(val from 1 for 5) from ededocsp edp where ededoch=:ref and edp.parent = :document and edp.name = '2' into :data;

      select substring(val from 1 for 40) from ededocsp edp where ededoch=:ref and edp.parent = :document and edp.name = '4' into :cardid;
      select substring(val from 1 for 40) from ededocsp edp where ededoch=:ref and edp.parent = :document and edp.name = '6' into :gateid;

      if (coalesce(eventid,'') <> '') then
        insert into regisevents (IDIDENT, IDGATE, TIMEEVENT, IDEVENT, STATUS)
          values (:cardid, :gateid, :data, :eventid, 0);
    end
    when any do
      begin
        exception;
      end
  end
 -- execute procedure XK_UPDATE_RCP_POSITION returning_values :rcount;
  suspend;
end^
SET TERM ; ^
