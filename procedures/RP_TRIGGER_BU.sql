--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RP_TRIGGER_BU(
      TABLENAME varchar(31) CHARACTER SET UTF8                           ,
      OLD_KEY1 varchar(40) CHARACTER SET UTF8                           ,
      OLD_KEY2 varchar(40) CHARACTER SET UTF8                           ,
      OLD_KEY3 varchar(40) CHARACTER SET UTF8                           ,
      OLD_KEY4 varchar(40) CHARACTER SET UTF8                           ,
      OLD_KEY5 varchar(40) CHARACTER SET UTF8                           ,
      OLD_TOKEN integer,
      OLD_STATE integer,
      NEW_KEY1 varchar(40) CHARACTER SET UTF8                           ,
      NEW_KEY2 varchar(40) CHARACTER SET UTF8                           ,
      NEW_KEY3 varchar(40) CHARACTER SET UTF8                           ,
      NEW_KEY4 varchar(40) CHARACTER SET UTF8                           ,
      NEW_KEY5 varchar(40) CHARACTER SET UTF8                           ,
      NEW_TOKEN integer,
      NEW_STATE integer,
      WASCHANGE smallint)
  returns (
      RET_TOKEN integer,
      RET_STATE integer)
   as
begin
  if (new_token is null) then
    new_token = 9;
  if(:waschange = 1 or new_state = -1) then
  begin
    if(new_token <> 7) then -- rekord nie jest w trakcie replikacji
    begin
       if(new_token <> 8) then  -- sprawdzanie uprawnienia nie jest zablokowane - to go sprawdzam
         execute procedure RP_CHECK_TOKEN(tablename, new_token)
           returning_values new_token;
       if (new_key1 <> old_key1
         or  new_key2 <> old_key2
         or  new_key3 <> old_key3
         or  new_key4 <> old_key4
         or  new_key5 <> old_key5
         ) then
       begin
         execute procedure rp_desync_multistate(tablename, old_key1, old_key2, old_key3, old_key4, old_key5, new_state);
       end
       execute procedure rp_check_multistate(tablename, new_key1, new_key2,  new_key3, new_key4, new_key5, new_token, new_state)
         returning_values new_state;
    end
  end
  if(new_token = 8) then
    new_token = old_token;
  if(new_token = 6) then --przeliczenie wartoci TOKEN, ale bez zglaszania komunikatu
    execute procedure RP_CHECK_TOKEN(tablename, new_token)
      returning_values new_token;
  if(new_token <> 7 ) then
    new_state = 0;
  ret_token = new_token;
  ret_state = new_state;
end^
SET TERM ; ^
