--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGKPL_OBL(
      NAGKPL integer)
   as
declare variable sumwartmag numeric(14,2);
declare variable sumwartspr numeric(14,2);
begin
  select sum(WARTMAG), sum(WARTSPR) from KPLPOZ where NAGKPL = :nagkpl
  into :sumwartmag, :sumwartspr;
  update KPLNAG set SUMWARTMAG = :sumwartmag, SUMWARTSPRZ = :sumwartspr where REF=:nagkpl;
end^
SET TERM ; ^
