--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SET_ALL_GRANTS_FOR(
      USERNAME varchar(20) CHARACTER SET UTF8                           )
   as
declare variable TABLENAME varchar(31);
declare variable PROCNAME varchar(31);
begin
  for
    select rdb$relation_name
      from rdb$relations
      where rdb$system_flag=0
      and rdb$relation_name not in ('S_USERS','S_USERLOGINS')
      into :tablename
  do
    execute statement 'GRANT ALL ON ' || tablename || ' TO "' || username || '"';

  for
    select rdb$procedure_name
      from rdb$procedures
      into :procname
  do
    execute statement 'GRANT EXECUTE ON PROCEDURE ' || procname || ' TO "' || username || '"';



  execute statement 'grant all on S_USERS to procedure SYS_GET_USERLOGINS';
  execute statement 'grant all on S_USERS to procedure SYS_SET_USERLOGIN';
  execute statement 'grant all on S_USERS to procedure SYS_GET_USERINFO';
  execute statement 'grant all on S_USERS to procedure SYS_SET_USERINFO';
  execute statement 'grant all on S_USERS to procedure SYS_DEL_USERINFO';
  execute statement 'grant all on S_USERS to procedure COPY_USERLOGINS';
  execute statement 'grant all on S_USERS to procedure LOGIN_USER';

  execute statement 'grant all on S_USERLOGINS to procedure SYS_GET_USERLOGINS';
  execute statement 'grant all on S_USERLOGINS to procedure SYS_SET_USERLOGIN';
  execute statement 'grant all on S_USERLOGINS to procedure SYS_GET_USERINFO';
  execute statement 'grant all on S_USERLOGINS to procedure SYS_SET_USERINFO';
  execute statement 'grant all on S_USERLOGINS to procedure SYS_DEL_USERINFO';
  execute statement 'grant all on S_USERLOGINS to procedure COPY_USERLOGINS';
  execute statement 'grant all on S_USERLOGINS to procedure LOGIN_USER';


  execute statement 'grant all on S_SESSIONS to "PUBLIC"';

  execute statement 'grant all on GLOBALPARAMS to procedure CLEAR_GLOBAL_PARAMS';

  execute statement 'grant all on S_NAMESPACES to procedure SYS_GET_USERINFO';
  execute statement 'grant all on OPERATOR to procedure SYS_GET_USERINFO';

  execute statement 'grant execute on procedure SYS_GET_USERLOGINS to "PUBLIC"';
  execute statement 'grant execute on procedure SYS_SET_USERLOGIN to "PUBLIC"';
  execute statement 'grant execute on procedure SYS_GET_USERINFO to "PUBLIC"';
  execute statement 'grant execute on procedure SYS_SET_USERINFO to "PUBLIC"';
  execute statement 'grant execute on procedure SYS_DEL_USERINFO to "PUBLIC"';
  execute statement 'grant execute on procedure COPY_USERLOGINS to "PUBLIC"';
  execute statement 'grant execute on procedure REFRESH_USERS to "PUBLIC"';
  execute statement 'grant execute on procedure REFRESH_USERS2 to "PUBLIC"';
  execute statement 'grant execute on procedure CLEAR_GLOBAL_PARAMS to "PUBLIC"';
  execute statement 'grant execute on procedure SYS_REMOVELOCK to "PUBLIC"';
  execute statement 'grant execute on procedure SYS_CLEARSYSLOCKS to "PUBLIC"';

  execute statement 'grant execute on procedure XK_RPT_OFERTA to "OFFICE"';
  execute statement 'grant all on KONFIG to procedure XK_RPT_OFERTA';
  execute statement 'grant all on OFERTY to procedure XK_RPT_OFERTA';
  execute statement 'grant all on OFERPOZ to procedure XK_RPT_OFERTA';
  execute statement 'grant all on KLIENCI to procedure XK_RPT_OFERTA';
  execute statement 'grant all on UZYKLI to procedure XK_RPT_OFERTA';
  execute statement 'grant execute on procedure XK_RPT_UMOWA to "OFFICE"';
  execute statement 'grant all on KONFIG to procedure XK_RPT_UMOWA';
  execute statement 'grant all on DOKPLIK to procedure XK_RPT_UMOWA';
  execute statement 'grant all on CPODMIOTY to procedure XK_RPT_UMOWA';
  execute statement 'grant all on SYS_LOCKS to procedure sys_clearsyslocks';


end^
SET TERM ; ^
