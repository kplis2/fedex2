--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_CREATE_HIST_ZAM(
      ZAM integer,
      OPER char(3) CHARACTER SET UTF8                           ,
      OPERATOR integer,
      SKAD smallint,
      DOKUMENT integer,
      PROPERSRAP integer,
      PRSHORTAGE integer)
  returns (
      STATUS smallint,
      HISTZAM integer)
   as
declare variable brakhistzam smallint;
begin
  select BRAKHISTZAM from DEFOPERZAM where SYMBOL=:oper into :brakhistzam;
  if((:brakhistzam=0) or (:brakhistzam is null)) then begin
    execute procedure GEN_REF('HISTZAM')
      returning_values :histzam;
    insert into HISTZAM (ref, ZAMOWIENIE, OPERACJA,  DATA,
               DATAZM, FAKTURA, ORG_ID, ORG_REJ, ORG_BLOKADA, ORG_STAN, OPERATOR, WYSYLKA, CINOUT, SKAD, DOKUMENTMAIN, STATUS, ORG_KSTAN,
               propersrap,  prshortage)
    select  :histzam,:ZAM,:OPER,current_timestamp(0),
               DATAZM, FAKTURA, ORG_REF, REJESTR, BLOKADA, STAN, :OPERATOR, WYSYLKA, CINOUT, :skad,:dokument, STATUS, KSTAN,
               :propersrap, :prshortage
    from NAGZAM where REF=:zam;
  end else histzam = 0;
  STATUS = 1;
end^
SET TERM ; ^
