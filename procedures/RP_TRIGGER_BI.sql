--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RP_TRIGGER_BI(
      TABLENAME varchar(31) CHARACTER SET UTF8                           ,
      KEY1 varchar(40) CHARACTER SET UTF8                           ,
      KEY2 varchar(40) CHARACTER SET UTF8                           ,
      KEY3 varchar(40) CHARACTER SET UTF8                           ,
      KEY4 varchar(120) CHARACTER SET UTF8                           ,
      KEY5 varchar(40) CHARACTER SET UTF8                           ,
      CUR_TOKEN integer,
      CUR_STATE integer)
  returns (
      NEW_TOKEN integer,
      NEW_STATE integer)
   as
begin
  execute procedure RP_CHECK_TOKEN(:tablename, cur_token)
    returning_values new_token;
  if (new_token <> 7) then
    execute procedure rp_check_multistate(:tablename, :key1, :key2, :key3, :key4, :key5, new_token, new_state)
      returning_values new_state;
end^
SET TERM ; ^
