--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZFAK_GET_FIRSTPOZ_UP(
      POZREF integer)
  returns (
      POZFAK integer)
   as
declare variable refk integer;
begin
  pozfak = null;
  while(:pozref is not null) do begin
    pozfak = pozref;
    pozref = null;
    select REFK from POZFAK where REF=:pozfak into :pozref;
    suspend;
  end
end^
SET TERM ; ^
