--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DIGITIZE(
      VAL numeric(14,4),
      STEP varchar(20) CHARACTER SET UTF8                           ,
      MINVAL numeric(14,4))
  returns (
      RES numeric(14,4))
   as
DECLARE VARIABLE STEPN NUMERIC(14,4);
begin
  if(:val<:minval and :minval is not null and :val>0) then val=:minval;
  if(:val>0) then
    if(:step is null or :step='') then res = val;
    else begin
      stepn = cast(step as numeric(14,4));
      if(:stepn=0) then res = val;
      else res = cast(val/stepn as numeric(14,0))*stepn;
    end
  else res=0;
  suspend;
end^
SET TERM ; ^
