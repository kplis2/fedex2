--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EFUNC_DATESFROMPERIOD(
      PERIOD char(6) CHARACTER SET UTF8                           )
  returns (
      FROMDATE timestamp,
      TODATE timestamp)
   as
declare variable iyear integer;
  declare variable imonth smallint;
begin
  iyear = cast(substring(period from 1 for 4) as integer);
  imonth = cast(substring(period from 5 for 2) as smallint);

  fromdate = cast(iyear || '/' || imonth || '/1' as date);
  if (imonth = 12) then
    todate = cast((iyear+1) || '/1/1' as date) - 1;
  else
    todate = cast(iyear || '/' || (imonth + 1) || '/1' as date) - 1;
  suspend;
end^
SET TERM ; ^
