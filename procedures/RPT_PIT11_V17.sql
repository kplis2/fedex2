--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PIT11_V17(
      CURRENTCOMPANY integer,
      YEARID char(4) CHARACTER SET UTF8                           ,
      PERSON_INPUT integer,
      TYP smallint,
      DATA date,
      ISPRINT_VER18 smallint = 0)
  returns (
      P36 numeric(14,2),
      P37 numeric(14,2),
      P38 numeric(14,2),
      P40 numeric(14,2),
      P52 numeric(14,2),
      P53 numeric(14,2),
      P54 numeric(14,2),
      P55 numeric(14,2),
      P56R numeric(14,2),
      P57R numeric(14,2),
      P58R numeric(14,2),
      P59R numeric(14,2),
      P64 numeric(14,2),
      P65 numeric(14,2),
      P66 numeric(14,2),
      P67 numeric(14,2),
      P76 numeric(14,2),
      P78 numeric(14,2),
      P56 numeric(14,2),
      P57 numeric(14,2),
      P58 numeric(14,2),
      P59 numeric(14,2),
      PERSON integer,
      FROMDATE timestamp,
      TODATE timestamp,
      TAXOFFICE varchar(200) CHARACTER SET UTF8                           ,
      USCODE varchar(4) CHARACTER SET UTF8                           ,
      COSTS smallint)
   as
declare variable EMPL integer;
declare variable AKTUOPERATOR integer;
declare variable DESCRIPT varchar(255);
declare variable COSTS_TEMP smallint;
declare variable COSTS_NR smallint;
declare variable YFROMDATE timestamp;
declare variable YTODATE timestamp;
declare variable CTODATE date;
begin
--Proc. generuje dane do PITu-11 w wersji 17-stej i 18-stej (ISPRINT_VER18 = 1)

  if (person_INPUT = 0) then person_INPUT = null;
  yfromdate = yearid || '-01-01';
  ytodate = yearid || '-12-31';

  for
    select e.person
      from epayrolls pr 
        join eprpos pp on (pr.ref = pp.payroll)
        join employees e on (pp.employee = e.ref)
      where pp.pvalue <> 0 and pp.ecolumn in (3000, 5950)
        and pr.tper starting with :yearid
        and (e.person = :person_input or :person_input is null)
        and pr.company = :currentcompany
        and pr.lumpsumtax = 0
      group by e.person, e.personnames
      order by e.personnames
      into :person
  do begin
    if (coalesce(isprint_ver18,0) = 0) then
    begin
    --okreslenie zakresu dat, dotyczy tylko wersji 17-stej PITu-11

      fromdate = null;
      select min(ec.fromdate), max(coalesce(ec.enddate, :ytodate))
        from emplcontracts ec
          join employees e on (e.ref = ec.employee)
        where e.person = :person
          and (ec.enddate >= :yfromdate or ec.enddate is null)
        into :fromdate, :todate;
    
      if (fromdate < yfromdate or fromdate is null) then
        fromdate = yfromdate;
    
      if (todate < ytodate or todate is null) then
      begin
      --okreslenie 'daty do' jako daty otrzymania ostatniego wynagrodzenia
        select max(pr.payday) from epayrolls pr
            join eprpos pp on (pr.ref = pp.payroll and pp.ecolumn = 7450)
            join employees e on (e.ref = pp.employee)
          where e.person = :person
          into :todate;
    
      /*nalezy sprawdzic, czy ostatnie wynagrodzenie nie zostalo wyplacone przed
        data konca umowy (np w sytuacji gdy dzien konca umowy wypada w weekend);
        w takim przypadku data konca umowy stanowi 'date do'*/
        select max(coalesce(ec.enddate,ec.todate))
          from emplcontracts ec
            join employees e on (e.ref = ec.employee)
             and coalesce(ec.enddate,ec.todate) > :todate --ograniczenie dziedziny
          where e.person = :person
          into :ctodate;
    
        if (ctodate is not null) then
        begin
        --nalezy sprawdzic czy nie ma aktywanych umow na czas nieokreslny
          if (exists(select first 1 1 from employment m join employees e on (m.employee = e.ref)
            where m.todate is null and e.person = :person)) then
          ctodate = null;
        end
    
        if (ctodate > todate) then
          todate = ctodate;
      end
    
      if (todate is null or todate > ytodate) then
        todate = ytodate;
    end

    p36 = null;
    p37 = null;
    p38 = null;
    p40 = null;
    p52 = null;
    p53 = null;
    p54 = null;
    p55 = null;
    p56r = null;
    p57r = null;
    p58r = null;
    p59r = null;
    p64 = null;
    p65 = null;
    p66 = null;
    p67 = null;
    p76 = null;
    p78 = null;
    p56 = null;
    p57 = null;
    p58 = null;
    p59 = null;
    costs = null;
    taxoffice = null;
    uscode = null;

-- przychod pracownika =========================================================
    select sum(pp.pvalue)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
        join employees e on (e.ref = pp.employee)
        join ecolumns c on (pp.ecolumn = c.number)
      where pr.tper starting with :yearid
        and e.person = :person
        and ((c.number = 5950 and pr.prtype = 'SOC') --przychod z list socjalnych (BS36243)
          or (c.cflags like '%;POD;%' and pr.prtype <> 'SOC' and c.number <> 5950)) --przychod z list innych niz socjalne
      into :p36;
    p36 = coalesce(p36,0);

-- koszty uzyskania pracownika
    select sum(pp.pvalue)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn = 6500
        and pr.tper starting with :yearid
        and e.person = :person
      into :p37;
    p37 = coalesce(p37,0);

-- dochod pracownika
    p38 = :p36 - :p37;

-- zaliczka na podatek dochodowy (razem z wyrownaniem) pracownika
    select sum(pp.pvalue)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn in (7350, 7370)
        and pr.tper starting with :yearid
        and e.person = :person
      into :p40;
    p40 = coalesce(p40,0);

-- przychod czlonka rady naddzorczej ===========================================
    select sum(pp.pvalue)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 3)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn = 5950
        and pr.tper starting with :yearid
        and e.person = :person
        and pr.lumpsumtax = 0
      into :p56r;
    p56r = coalesce(p56r,0);

-- koszty uzyskania czlonka rady naddzorczej
    select sum(pp.pvalue)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 3)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn = 6500
        and pr.tper starting with :yearid
        and e.person = :person
        and pr.lumpsumtax = 0
      into :p57r;
    p57r = coalesce(p57r,0);

-- dochod czlonka rady naddzorczej
    p58r = :p56r - :p57r;

-- zaliczka na podatek dochodowy (razem z wyrownaniem) rady naddzorczej
    select sum(pp.pvalue)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 3)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn in (7350, 7370)
        and pr.tper starting with :yearid
        and e.person = :person
        and pr.lumpsumtax = 0
      into :p59r;
    p59r = coalesce(p59r,0);

-- przychod z umowy UCP i z umowy prawa autorskie ==============================
    select sum(case when coalesce(c.prcosts,0) <> 50 then coalesce(pp.pvalue,0) else 0 end),
           sum(case when coalesce(c.prcosts,0) = 50 then coalesce(pp.pvalue,0) else null end)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 2)
        join emplcontracts c on (c.ref = pr.emplcontract)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn = 3000
        and pr.tper starting with :yearid
        and e.person = :person
        and pr.lumpsumtax = 0
      into :p56, :p64;
    p56 = coalesce(p56,0);

-- koszty uzyskania z umowy UCP i z umowy prawa autorskie
    select sum(case when coalesce(c.prcosts,0) <> 50 then coalesce(pp.pvalue,0) else 0 end),
           sum(case when coalesce(c.prcosts,0) = 50 then coalesce(pp.pvalue,0) else null end)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 2)
        join emplcontracts c on (c.ref = pr.emplcontract)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn = 6500
        and pr.tper starting with :yearid
        and e.person = :person
        and pr.lumpsumtax = 0
      into :p57, :p65;
    p57 = coalesce(p57,0);

-- dochod z umowy UCP
    p58 = p56 - p57;

-- dochod z umowy prawa autorskie
    if (p64 is not null or p65 is not null) then
      p66 = coalesce(p64,0) - coalesce(p65,0);

-- zaliczka na podatek doch. (z wyrownaniem) z um. UCP i z um. prawa autorskie
    select sum(case when coalesce(c.prcosts,0) <> 50 then coalesce(pp.pvalue,0) else 0 end),
           sum(case when coalesce(c.prcosts,0) = 50 then coalesce(pp.pvalue,0) else null end)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 2)
        join emplcontracts c on (c.ref = pr.emplcontract)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn in (7350, 7370)
        and pr.tper starting with :yearid
        and e.person = :person
        and pr.lumpsumtax = 0
      into :p59, :p67;
    p59 = coalesce(p59,0);

-- suma wartosci na pit11(V17)
    p56 = p56r + p56; --przychod RN i UCP
    p57 = p57r + p57; --koszty uzyskania RN i UCP
    p58 = p58r + p58; --dochod RN i UCP
    p59 = p59r + p59; --zaliczka na podatek RN i UCP

-- skladki na ubezpieczenie spoleczne ==========================================
    select sum(pp.pvalue)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn in (6100, 6110, 6120, 6130)
        and pr.tper starting with :yearid
        and e.person = :person
        and pr.lumpsumtax = 0
      into :p76;
    p76 = coalesce(p76,0);

-- skladki na ubezpieczenie zdrowotne
    select sum(pp.pvalue)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn = 7210
        and pr.tper starting with :yearid
        and e.person = :person
        and pr.lumpsumtax = 0
      into :p78;
    p78 = coalesce(p78,0);

 /* zapisz do 'costs' koszty uzyskania danego pracownika. dane pobieraj
    z ostatniego okresu nie przekraczajacego podanego roku (yearid) */
    costs = 0;
    costs_nr = 0;
    for
      select distinct e.ref
        from epayrolls pr
          join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
          join employees e on (e.ref = pp.employee)
        where pp.ecolumn = 6500
          and pr.tper starting with :yearid
          and e.person = :person
        into :empl
    do begin
      select first 1 t.costs
        from empltaxinfo t
          join employees e on (t.employee = e.ref)
        where e.ref = :empl
          and t.fromdate < :ytodate
        order by t.fromdate desc
        into :costs_temp;

      if (costs_temp > costs) then
        costs = costs_temp;
      costs_nr = costs_nr + 1;
    end

    if (costs_nr > 2) then
      costs_nr = 2;

    if (costs = 2 and costs_nr = 1) then
      costs = 3;
    else if (costs = 1 and costs_nr = 2) then
      costs = 2;
    else if (costs = 2 and costs_nr = 2) then
      costs = 4;

    if (coalesce(p37,0) = 0) then
      costs = null;

    select first 1 coalesce(ir.name,'') || ', ' ||  coalesce(ir.address,'') || ', ' ||  coalesce(ir.postcode,'') || ' ' ||  coalesce(ir.city,''), ir.code
      from empltaxinfo t
        join employees e on (t.employee = e.ref)
        left join einternalrevs ir on (t.taxoffice = ir.ref)
      where e.person = :person
        and t.fromdate < :ytodate
      order by t.fromdate desc
      into :taxoffice, :uscode;

    if (p36 = 0) then
    begin
      p36 = null;
      if (p37 = 0) then p37 = null;
      if (p38 = 0) then p38 = null;
      if (p40 = 0) then p40 = null;
    end

    if (p52 = 0) then
    begin
      p52 = null;
      if (p53 = 0) then p53 = null;
      if (p54 = 0) then p54 = null;
      if (p55 = 0) then p55 = null;
    end

    if (p56 = 0) then
    begin
      p56 = null;
      if (p57 = 0) then p57 = null;
      if (p58 = 0) then p58 = null;
      if (p59 = 0) then p59 = null;
    end

    if (p64 = 0) then
    begin
      p64 = null;
      if (p65 = 0) then p65 = null;
      if (p66 = 0) then p66 = null;
      if (p67 = 0) then p67 = null;
    end

    if (p76 = 0) then p76 = null;
    if (p78 = 0) then p78 = null;

      if (typ = 1 and person is not null) then
    begin
      execute procedure get_global_param ('AKTUOPERATOR')
        returning_values aktuoperator;
      taxoffice = coalesce(taxoffice, 'Urzędu Skarbowego');
      descript = 'Przekazanie danych na formularzu PIT-11 do ' || :taxoffice;

      if (not exists (select ref from epersdatasecur
                        where person = :person and kod = 2 and descript = :descript
                        and operator = :aktuoperator and regdate = :data)
      ) then
        insert into epersdatasecur (person, kod, descript, operator, regdate)
          values (:person, 2, :descript, :aktuoperator, :data);
    end

    suspend;
  end
end^
SET TERM ; ^
