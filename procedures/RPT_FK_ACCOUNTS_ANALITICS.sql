--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_ACCOUNTS_ANALITICS(
      PERIODID varchar(6) CHARACTER SET UTF8                           ,
      CURRENCY varchar(3) CHARACTER SET UTF8                           ,
      ACCWOPEN smallint,
      ACCTYPE smallint,
      ACCOUNTMASK ACCOUNT_ID,
      FROMACCOUNT ACCOUNT_ID,
      TOACCOUNT ACCOUNT_ID,
      COMPANY integer,
      RANGE smallint,
      ENDBOOKED smallint = 0)
  returns (
      REF integer,
      SYMBOL varchar(3) CHARACTER SET UTF8                           ,
      ACCOUNT ACCOUNT_ID,
      DESCRIPT varchar(80) CHARACTER SET UTF8                           ,
      ODEBIT numeric(15,2),
      OCREDIT numeric(15,2),
      DEBIT numeric(15,2),
      CREDIT numeric(15,2),
      ADEBIT numeric(15,2),
      ACREDIT numeric(15,2),
      BDEBIT numeric(15,2),
      BCREDIT numeric(15,2),
      ISACCOUNT smallint,
      SUMACCOUNT smallint,
      YEARNAME varchar(40) CHARACTER SET UTF8                           ,
      ROWCOUNT integer)
   as
declare variable YEARID integer;
declare variable OLD_SYMBOL varchar(3);
declare variable TMP_ACCOUNT ACCOUNT_ID;
declare variable TMP_DEBIT numeric(15,2);
declare variable TMP_CREDIT numeric(15,2);
declare variable BKACCOUNT_REF integer;
declare variable SEP varchar(1);
declare variable DICTDEF integer;
declare variable LEN smallint;
declare variable S_TMP ACCOUNT_ID;
declare variable OLD_PREFIX ACCOUNT_ID;
declare variable I smallint;
declare variable J smallint;
declare variable K smallint;
declare variable DICTREF integer;
declare variable ACCOUNTING_REF integer;
declare variable BALANCETYPE smallint;
declare variable ACCOUNT_TMP ACCOUNT_ID;
declare variable SUMODEBIT numeric(14,2);
declare variable SUMOCREDIT numeric(14,2);
declare variable SUMADEBIT numeric(14,2);
declare variable SUMACREDIT numeric(14,2);
declare variable SUMDEBIT numeric(14,2);
declare variable SUMCREDIT numeric(14,2);
declare variable SUMBDEBIT numeric(14,2);
declare variable SUMBCREDIT numeric(14,2);
declare variable DESCRIPT_TMP varchar(80);
declare variable SUMODEBIT1 numeric(14,2);
declare variable SUMOCREDIT1 numeric(14,2);
declare variable SUMADEBIT1 numeric(14,2);
declare variable SUMACREDIT1 numeric(14,2);
declare variable SUMDEBIT1 numeric(14,2);
declare variable SUMCREDIT1 numeric(14,2);
declare variable SUMBDEBIT1 numeric(14,2);
declare variable SUMBCREDIT1 numeric(14,2);
declare variable IFRANGE smallint;
declare variable EB smallint;
declare variable BB smallint;
declare variable DESCRIPTMAXLEN integer;
begin
  ref = 0;
  descriptmaxlen = 40;
  if (accountmask is null) then accountmask = '';
  if (fromaccount is null) then fromaccount = '';
  if (toaccount is null) then toaccount = '';
  old_symbol = '';
  old_prefix = '';
  ifrange = 1;
  rowcount = 1;

  execute procedure create_account_mask(:accountmask) returning_values :accountmask;
  if (accountmask = '%') then accountmask = null;

  select yearid from BKPERIODS where company = :company and ID = :periodid into :yearid;
  select yearid from bkyears where company = :company and yearid = :yearid into :yearname;
  if (toaccount is not null and toaccount <> '') then
  begin
    select max(account)
      from accounting
      where (account <= :toaccount or account like :toaccount || '%')
        and yearid = :yearid and company = :company
      into :toaccount;
  end

  if(endbooked = 0) then begin
    eb = 0;
    bb = 1;
  end else begin
    eb = 1;
    bb = 0;
  end

  for
    select A.ref, A.account, T.debit * :bb + T.ebdebit * :eb, T.credit * :bb + T.ebcredit * :eb
      from accounting A
        join turnovers T on (T.accounting = A.ref and T.period = :periodid)
      where A.yearid = :yearid and A.currency=:currency
        and ((:ACCOUNTMASK is null) or (:accountmask = '') or (A.account like :accountmask))
        and ((:fromaccount is null) or (:fromaccount = '') or (:fromaccount <= A.Account))
        and ((:toaccount is null) or (:toaccount = '') or (:toaccount >= A.account))
        and ((:acctype = 0 and A.bktype < 3) or (:acctype = 1 and A.bktype = 3) or :acctype = 2)
        and A.company = :company
      order by A.account
      into :accounting_ref, :tmp_account, :tmp_debit, :tmp_credit
  do begin
    symbol = cast(substring(:tmp_account from 1 for 3) as varchar(3));
    odebit = null;
    ocredit = null;
    adebit = null;
    acredit = null;
    debit = null;
    credit = null;
    bdebit = null;
    bcredit = null;
    sumaccount=1;

    if (range = 2) then -- sprawdzanie zakresu dla opcji 'z obrotami'
    begin
      if (exists(select accounting from turnovers where period >= substring(:periodid from 1 for 4) || '00' and period <= :periodid
        and account = :tmp_account and (debit <> 0 or credit <>0) and turnovers.company = :company)) then --company
        ifrange = 1;
      else ifrange = 0;
    end else
    if (range = 1) then -- sprawdzenie zakresu dla opcji 'otwarte'
    begin
      if (exists(select ref from accounting where yearid = :yearid
        and account = :tmp_account and accounting.company = :company)) then --company
        ifrange = 1;
      else ifrange = 0;
    end

    if (tmp_debit is null) then tmp_debit = 0;
    if (tmp_credit is null) then tmp_credit = 0;
    isaccount = 0;
    if (ifrange = 1) then -- szukamy nazwy syntetyki
    begin
      select descript, ref, saldotype
        from bkaccounts where symbol = :symbol and yearid = :yearid and company = :company
        into :descript, :bkaccount_ref, :balancetype;
      debit = null;
      credit = null;
      account = :symbol;
      if (symbol <> :tmp_account and symbol <> old_symbol) then
      begin
        old_symbol = symbol;
        odebit = null;
        ocredit = null;
        adebit = null;
        acredit = null;
        debit = null;
        credit = null;
        bdebit = null;
        bcredit = null;
        account_tmp = account;
        descript_tmp=descript;
        ref = ref + 1;
        rowcount = coalesce(char_length(:descript),0) / :descriptmaxlen; -- [DG] XXX ZG119346
        if(rowcount * descriptmaxlen < coalesce(char_length(:descript),0)) then -- [DG] XXX ZG119346
          rowcount = rowcount + 1;
        if (ifrange = 1) then suspend;
      end

      i = 0;
      -- zliczam ilosc poziomów nalitycznych
      for select separator, dictdef, len
        from accstructure
        where bkaccount = :bkaccount_ref
        order by nlevel
        into :sep, :dictdef, :len
      do begin
        i = i + 1;
      end
    end
    if (i > 0) then
    begin
      if (i > 1 and old_prefix <> substring(tmp_account from  1 for coalesce(char_length(tmp_account),0) - len)) then -- [DG] XXX ZG119346
      -- drukowanie opisow posrednich analityk jesli konto zmienilo sie na wczesniejszej analityce
      begin
        j = 0;
        k = 4;
        for
          select separator, dictdef, len
            from accstructure
            where bkaccount = :bkaccount_ref
            order by nlevel
            into :sep, :dictdef, :len
        do begin
          j = j + 1;
          if (j < i) then
          begin
            if (sep <> ',') then
            k = k +1;
            s_tmp = substring(tmp_account from  k for  k + len - 1);
            account = '     -' || s_tmp;
            execute procedure name_from_bksymbol(company, dictdef, s_tmp)
              returning_values descript, dictref;
            odebit = null;
            ocredit = null;
            adebit = null;
            acredit = null;
            debit = null;
            credit = null;
            bdebit = null;
            bcredit = null;
            account_tmp = account;
            ref = ref + 1;
        rowcount = coalesce(char_length(:descript),0) / descriptmaxlen; -- [DG] XXX ZG119346
        if(rowcount * descriptmaxlen < coalesce(char_length(:descript),0)) then -- [DG] XXX ZG119346
          rowcount = rowcount + 1;
            if (ifrange = 1) then suspend;
            k = k + len;
          end
        end
        old_prefix = substring(tmp_account from  1 for coalesce(char_length(tmp_account) , 0) - len); -- [DG] XXX ZG119346
      end
      s_tmp = substring(tmp_account from  coalesce(char_length(tmp_account), 0) - len + 1 for coalesce(char_length(tmp_account),0)); -- [DG] XXX ZG119346
      execute procedure name_from_bksymbol(company, dictdef, s_tmp)
        returning_values descript, dictref;
    end

    debit = tmp_debit;
    credit = tmp_credit;
    select sum(T.debit * :bb + T.ebdebit * :eb), sum(T.credit * :bb + T.ebcredit * :eb)
      from turnovers T
      where  (T.yearid = :yearid and T.accounting = :accounting_ref
        and T.period = substring(:periodid from 1 for 4) || '00')
      into :odebit, :ocredit;

    if (odebit is null) then odebit = 0;
    if (ocredit is null) then ocredit = 0;



    select sum(T.debit * :bb + T.ebdebit * :eb), sum(T.credit * :bb + T.ebcredit * :eb)
      from turnovers T
      where t.yearid = :yearid and T.accounting = :accounting_ref
        and T.period > substring(:periodid from 1 for 4) || '00' and T.period < :periodid
      into :adebit, :acredit;

    if (adebit is null) then adebit = 0;
    if (acredit is null) then acredit = 0;

    acredit = acredit + credit;
    adebit = adebit + debit;


    bdebit = 0;
    bcredit = 0;
    if (abs(acredit + ocredit) > abs(adebit + odebit)) then
      bcredit = (ocredit + acredit) - (odebit + adebit);
    else
      bdebit = (odebit + adebit) - (ocredit + acredit);

    if (balancetype = 0) then
    begin
      bdebit = bdebit - bcredit;
      bcredit = 0;
    end else
    if (balancetype = 2) then
    begin
      bcredit = bcredit- bdebit;
      bdebit = 0;
    end


    if (accwopen = 1) then
    begin
      acredit = acredit + ocredit;
      adebit = adebit + odebit;
    end


    account = tmp_account;
    --old_symbol = symbol;
    isaccount = 1;
    ref = ref + 1;
        rowcount = coalesce(char_length(:descript),0) / descriptmaxlen; -- [DG] XXX ZG119346
        if(rowcount * descriptmaxlen < coalesce(char_length(:descript),0)) then -- [DG] XXX ZG119346
          rowcount = rowcount + 1;
    if (ifrange = 1) then suspend;
  end
end^
SET TERM ; ^
