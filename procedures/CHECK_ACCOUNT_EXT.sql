--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHECK_ACCOUNT_EXT(
      COMPANY integer,
      ACCOUNT ACCOUNT_ID,
      YEARID integer,
      MODE smallint,
      CONTEXT_ID integer = 1)
  returns (
      RESULT smallint,
      HASDISTS smallint,
      MULTI smallint)
   as
declare variable TMP ACCOUNT_ID;
declare variable BKACCOUNT integer;
declare variable I smallint;
declare variable SEP varchar(1);
declare variable DICTDEF integer;
declare variable DISTDEF integer;
declare variable DISTFILTER varchar(255);
declare variable DISTSLOPOZ integer;
declare variable SLOTYP varchar(20);
declare variable LEN smallint;
declare variable RES smallint;
declare variable SYNLEN smallint;
begin
  result = 1;
  multi = 0;
  hasdists = 0;

  select synlen
    from bkyears
    where yearid = :yearid
      and company = :company
    into :synlen;

  if (coalesce(char_length(account),0) < synlen) then -- [DG] XXX ZG119346
    if (mode = 1) then
      result = 0;
    else
      exception empty_account;
  tmp = substring(account from  1 for  synlen);
  select ref
    from bkaccounts
    where symbol = :tmp
      and yearid = :yearid
      and company = :COMPANY
    into :bkaccount;
  if (bkaccount is null) then
  begin
    if (mode = 1) then
      result = 0;
    else -- przy koncie 20 znakowym wywala sie bo tekst exeptiona moze miec tylko i wylacnie 70 znaków !!!!!!! dlatego skrucilem komunikat
      exception no_account 'Brak konta (' || account || ') w planie kont, lub błędnie zapisane';
  end
  else
  begin
    i = synlen + 1;
    for
      select distdef, DISTFILTER
        from accstructuredists
        where OTABLE = 'BKACCOUNTS'
          and OREF = :bkaccount
        PLAN(ACCSTRUCTUREDISTS INDEX(ACCSTRUCTUREDISTS_IDX1)) --BS69875 indeks i wymuszenie planu zapytania
        order by NUMBER
        into :distdef, :distfilter
    do begin
      -- BS69913  UPDATE OR INSERT niestety idzie po zlym indeksie, wiec dluzsza forma
      if (exists(select first 1 1
                   from FK_CHECK_ACCOUNT_DISTDEF fkc
                   where fkc.distdef = :distdef
                     and fkc.context_id = :context_id)) then
        update FK_CHECK_ACCOUNT_DISTDEF fkc set DISTFILTER = :distfilter
          where fkc.distdef = :distdef
            and fkc.context_id = :context_id;
      else
        insert into FK_CHECK_ACCOUNT_DISTDEF (DISTDEF, DISTFILTER, CONTEXT_ID)
          values (
            :distdef, :distfilter, :context_id);
      -- /BS69913

      hasdists = 1;
    end
    --analiza poziomów analityki
    for
      select separator, dictdef, len
        from accstructure
        where bkaccount = :bkaccount
        order by nlevel
        into :sep, :dictdef, :len
    do begin
      if (:sep <> ',') then
      begin
        if (coalesce(char_length(account),0) < i) then -- [DG] XXX ZG119346
          if (mode = 1) then
            result = 0;
          else -- przy koncie 20 znakowym wywala sie bo tekst exeptiona moze miec tylko i wylacnie 70 znaków !!!!!!! dlatego skrucilem komunikat
            exception no_account 'Brak konta (' || account || ') w planie kont, lub błędnie zapisane';
        tmp = substring(account from  i for  i);
        if (tmp <> sep) then
          if (mode = 1) then
            result = 0;
          else -- przy koncie 20 znakowym wywala sie bo tekst exeptiona moze miec tylko i wylacnie 70 znaków !!!!!!! dlatego skrucilem komunikat
            exception no_account 'Brak konta (' || account || ') w planie kont, lub błędnie zapisane';
        i = i + 1;
      end
      if (coalesce(char_length(account),0) < i + len - 1) then -- [DG] XXX ZG119346
      begin
        if (mode = 1) then
          result = 0;
        else -- przy koncie 20 znakowym wywala sie bo tekst exeptiona moze miec tylko i wylacnie 70 znaków !!!!!!! dlatego skrucilem komunikat
          exception no_account 'Brak konta (' || account || ') w planie kont, lub błędnie zapisane';
      end
      tmp = null;
      tmp = substring(account from  i for  i + len - 1);
      execute procedure check_dictpos_symbol(company, dictdef, tmp)
        returning_values res;

      if (res = 0) then
      begin
        if (mode = 1) then
          result = 0;
        else -- przy koncie 20 znakowym wywala sie bo tekst exeptiona moze miec tylko i wylacnie 70 znaków !!!!!!! dlatego skrucilem komunikat
          exception no_account 'Brak konta (' || account || ') w planie kont, lub błędnie zapisane';
      end
      --odczytanie wyróżników
      select typ
        from slodef
        where ref = :dictdef
        into :slotyp;
      if (slotyp = 'ESYSTEM') then
      begin
        distslopoz = 0;
        select REF
          from slopoz
          where slownik = :dictdef
            and kontoks = :tmp
          into :distslopoz;
        if (:distslopoz > 0) then
        begin
          --odczytanie wyroznikw z danego SLOPOZ'a
          for
            select distdef, DISTFILTER
              from accstructuredists
              where OTABLE = 'SLOPOZ'
                and OREF = :distslopoz
              PLAN(ACCSTRUCTUREDISTS INDEX(ACCSTRUCTUREDISTS_IDX1)) --BS69875 indeks i wymuszenie planu zapytania
              order by NUMBER
              into :distdef, :distfilter
          do begin
            hasdists = 1;
            -- BS69913 UPDATE OR INSERT niestety idzie po zlym indeksie, wiec dluzsza forma
            if (exists(select first 1 1
                         from FK_CHECK_ACCOUNT_DISTDEF fkc
                         where fkc.distdef = :distdef
                           and fkc.context_id = :context_id)) then
              update FK_CHECK_ACCOUNT_DISTDEF fkc set DISTFILTER = :distfilter
                where fkc.distdef = :distdef
                  and fkc.context_id = :context_id;
            else
              insert into FK_CHECK_ACCOUNT_DISTDEF (DISTDEF, DISTFILTER, CONTEXT_ID)
                values (
                  :distdef, :distfilter, :context_id);
            -- /BS69913
          end
        end
      end
      i = i + len;
    end
    if (coalesce(char_length(account),0) >= i) then -- [DG] XXX ZG119346
      if (mode = 1) then
        result = 0;
      else -- przy koncie 20 znakowym wywala sie bo tekst exeptiona moze miec tylko i wylacnie 70 znaków !!!!!!! dlatego skrucilem komunikat
        exception no_account 'Brak konta (' || account || ') w planie kont, lub błędnie zapisane';
  end
  if (hasdists = 1) then
    if (exists(select S.REF
                 from SLODEF S
                   join FK_CHECK_ACCOUNT_DISTDEF F on (F.distdef = S.ref) and f.context_id = :context_id -- BS69913
                 where coalesce(S.multidist, 0) = 1)) then
      multi = 1;
end^
SET TERM ; ^
