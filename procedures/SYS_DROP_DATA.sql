--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_DROP_DATA(
      DORDER smallint)
  returns (
      PROBLEMS integer)
   as
declare variable table_name varchar(31);
declare variable twhere varchar(255);
declare variable status smallint;
begin
  problems = 0;
  for
    select table_name, twhere
      from sys_tables where ttype = 0
      and dorder=:dorder
      order by dorder
      into :table_name, :twhere
  do begin
    if (exists(select first 1 1 from rdb$relations where RDB$RELATION_NAME = :table_name)) then begin
      execute procedure sys_drop_data_2(:table_name, :twhere) returning_values :status;
      if(:status=0) then problems = :problems + 1;
    end
  end
end^
SET TERM ; ^
