--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DEL_EDEDOCSH_FROM_EDECL(
      DECLLIST varchar(8191) CHARACTER SET UTF8                           )
   as
declare variable STRINGOUT varchar(255);
declare variable SYM varchar(10);
declare variable VAR varchar(10);
declare variable SCHM varchar(10);
declare variable NUM varchar(10);
declare variable PROC varchar(4096);
begin
  for select stringout from parsestring(:decllist,';')
  into :stringout
  do begin
    delete from ededocsh where oref = :stringout and otable = 'EDECLARATIONS' and status <> 4;
  end
end^
SET TERM ; ^
