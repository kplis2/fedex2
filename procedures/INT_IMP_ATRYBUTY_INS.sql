--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_ATRYBUTY_INS(
      TABELAID type of column INT_IMP_ATRYBUTY.TABELAID,
      OBIEKTID type of column INT_IMP_ATRYBUTY.OBIEKTID,
      SKADTABELA type of column INT_IMP_ATRYBUTY.SKADTABELA,
      SKADREF type of column INT_IMP_ATRYBUTY.SKADREF,
      ATRYBUT type of column INT_IMP_ATRYBUTY.ATRYBUT,
      ATRYBUTID type of column INT_IMP_ATRYBUTY.ATRYBUTID,
      WARTOSC type of column INT_IMP_ATRYBUTY.WARTOSC,
      WARTOSCID type of column INT_IMP_ATRYBUTY.WARTOSCID,
      DEL type of column INT_IMP_ATRYBUTY.DEL,
      HASHVALUE type of column INT_IMP_ATRYBUTY.HASH,
      REC type of column INT_IMP_ATRYBUTY.REC,
      SESJA type of column INT_IMP_ATRYBUTY.SESJA)
  returns (
      REF type of column INT_IMP_ATRYBUTY.REF)
   as
begin
  insert into int_imp_atrybuty (tabelaid, obiektid,
      atrybut, atrybutid, wartosc, wartoscid,
      "HASH", del, rec, skadtabela, skadref, sesja)
    values (:tabelaid, :obiektid,
      :atrybut, :atrybutid, :wartosc, :wartoscid,
      :hashvalue, :del, :rec, :skadtabela, :skadref, :sesja)
    returning ref into :ref;
  suspend;
end^
SET TERM ; ^
