--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_PERSALDOWN(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      ACCOUNTS ACCOUNT_ID)
  returns (
      AMOUNT numeric(15,2))
   as
declare variable COMPANY integer;
declare variable YEARID integer;
declare variable PERIODBO varchar(6);
declare variable DEBIT numeric(15,2);
declare variable CREDIT numeric(15,2);
declare variable BKACCOUNTS_REF integer;
declare variable SALDO_DEBIT numeric(14,2);
declare variable COUNTRY_CURRENCY varchar(3);
declare variable BALANCETYPE smallint;
declare variable FRVHDR integer;
declare variable FRPSN integer;
declare variable FRCOL integer;
declare variable CACHEPARAMS varchar(255);
declare variable DDPARAMS varchar(255);
declare variable FRVERSION integer;
begin
  if (frvpsn = 0) then exit;
  select frvhdr, frpsn, frcol
    from frvpsns
    where ref = :frvpsn
    into :frvhdr, :frpsn, :frcol;

  select company, frversion
    from frvhdrs
    where ref = :frvhdr
    into :company, :frversion;
  cacheparams = period||';'||accounts||';'||company;
  ddparams = accounts;

  if (not exists (select ref from frvdrilldown where functionname = 'PERSALDOWN' and cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr)) then
  begin

    select yearid from bkperiods where id = :period and company = :company
      into :yearid;
    periodbo = cast(yearid as varchar(4)) || '00';
    accounts = replace(accounts,  '?',  '_') || '%';
    amount = 0;
    execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
      returning_values country_currency;
    for
      select b.ref, b.saldotype
        from accounting A
          join bkaccounts B on (A.bkaccount = B.ref)
        where A.currency = :country_currency and A.yearid = :yearid
          and A.account like :accounts
        group by b.ref, b.saldotype
        into :bkaccounts_ref, :balancetype
    do begin
      select sum(t.debit), sum(t.credit)
        from accounting a
          join turnovers t on (a.ref = t.accounting)
        where a.bkaccount = :bkaccounts_ref and a.account like :accounts
          and t.period >= :periodbo and t.period <= :period and a.company = :company
        into :debit, :credit;

      if (debit is null) then
        debit = 0;
      if (credit is null) then
        credit = 0;

      saldo_debit = 0;
      if (balancetype = 0) then
        saldo_debit = debit - credit;
      else if (balancetype = 1 and abs(debit) > abs(credit)) then
        saldo_debit = debit - credit;

      amount = amount + saldo_debit;

    end
  end else
    select first 1 fvalue from frvdrilldown where functionname = 'PERSALDOWN' and cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr
      into :amount;

  amount = coalesce(amount,0);
  insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
     values (:frvhdr, :frpsn, :frcol, 'PERSALDOWN', :cacheparams, :ddparams, :amount, :frversion);
  suspend;
end^
SET TERM ; ^
