--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRDCELLS_SETRIGHTS(
      CELL integer,
      FRDPDVREADRIGHT varchar(80) CHARACTER SET UTF8                           ,
      FRDPDVWRITERIGHT varchar(80) CHARACTER SET UTF8                           ,
      ELEMREADRIGHT varchar(80) CHARACTER SET UTF8                           ,
      ELEMWRITERIGHT varchar(80) CHARACTER SET UTF8                           ,
      CELLREADWRIGHTP varchar(80) CHARACTER SET UTF8                           ,
      CELLWRITERIGHTP varchar(80) CHARACTER SET UTF8                           )
   as
declare variable cellreadrights varchar(80);
declare variable cellwriterights varchar(80);
declare variable operator integer;
begin
  cellreadrights = '';
  cellwriterights = '';
  if ((:FRDPDVREADRIGHT <> '' and :FRDPDVREADRIGHT <> ';')
      or (:ELEMREADRIGHT <> '' and :ELEMREADRIGHT <> ';')) then
  begin
    for
      select operator.ref from operator
      where position(';'||operator.ref||';' in :FRDPDVREADRIGHT)>0
        or position(';'||operator.ref||';' in :ELEMREADRIGHT)>0
      into :operator
    do begin
      cellreadrights = :cellreadrights||';'||:operator||';';
    end
  end
  if ((:FRDPDVWRITERIGHT <> '' and :FRDPDVWRITERIGHT <> ';')
      or (:ELEMWRITERIGHT <> '' and :ELEMWRITERIGHT <> ';')) then
  begin
    for
      select operator.ref from operator
      where position(';'||operator.ref||';' in :FRDPDVREADRIGHT)>0
        or position(';'||operator.ref||';' in :ELEMREADRIGHT)>0
      into :operator
    do begin
      cellwriterights = :cellwriterights||';'||:operator||';';
    end
  end
  if (:cellreadrights <> :cellreadwrightp or :cellwriterights <> :cellwriterightp) then
    update frdcells set frdcells.readrights = :cellreadrights, frdcells.writerights = :cellwriterights
      where frdcells.ref = :cell;
end^
SET TERM ; ^
