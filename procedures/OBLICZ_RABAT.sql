--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OBLICZ_RABAT(
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      KLIENT integer,
      ILOSC numeric(14,4),
      CENA numeric(14,4),
      BN char(1) CHARACTER SET UTF8                           ,
      MAG varchar(3) CHARACTER SET UTF8                           ,
      CENNIK integer,
      ZAST smallint,
      ZPAKIETEM smallint)
  returns (
      RABAT numeric(14,4),
      ISCENA integer,
      WALUTA varchar(3) CHARACTER SET UTF8                           ,
      REFPROMOCJI integer)
   as
declare variable cnt integer;
declare variable refprom integer;
declare variable typprom char(1);
declare variable cechaprom varchar(30);
declare variable cechatabela varchar(30);
declare variable cechapole varchar(30);
declare variable cecharodzaj integer;
declare variable wartcechyprom varchar(255);
declare variable getprom integer;
declare variable rabil numeric(14,4);
declare variable rabwar numeric(14,2);
declare variable rabup numeric(14,4);
declare variable grupakli integer;
declare variable cenamin numeric(14,4);
declare variable cenaconst numeric(14,4);
declare variable walmin varchar(3);
declare variable ktmmaska varchar(40);
declare variable odktm varchar(40);
declare variable doktm varchar(40);
declare variable lastprior integer;
declare variable curprior integer;
declare variable kliprocentrab numeric(14,4);
declare variable towprocentrab numeric(14,4);
declare variable towconstcena numeric(14,4);
declare variable promobn char(1);
declare variable tpromobn char(1);
declare variable vat numeric(14,2);
declare variable rabatprog numeric(14,4);
declare variable cenylicz smallint;
declare variable refpromocjicen integer;
declare variable grupa_tow string60;
begin
  /* Procedure Text */
  /* bierzemy te PROMOCJE, których TABRAB=1 */
  /* dla których tabela stowarzyszona PROMOKLI zawiera klienta lub jego grupe */
  /* w zależnosci od PROMOCJE.TYP: */
  /* 'T' - towar musi być zgodny z towarem w tabeli stowarzyszonej PROMOCT (bierzemy pierwszy i jedyny rekord pod uwage */
  /* 'G' - towar musi posiadac ceche PROMOCJE.CECHA o wartosci PROMOCJE.WARTCECHY */
  /* 'Z' - towar musi zawierac sie miedzy ODKTM,DOKTM i podpadac pod KTMMASKA */

  /* dla speniających w/w kryteria tabel wybieramy najkorzystniejszy upust rabatowy lub najniższą cene. */
  /* Upust bierzemy z tabeli stowarzyszonej PROMOCR z pola PROCENT, */
  /* przy czym pola ILOSC i WARTOSC stanowia progi ilosciowe i wartosciowe */
  /* szukamy obu rodzajow progow i wybieramy wariant korzystniejszy dla klienta */
  /* uwzgledniajac priorytet tabel rabatowych */
  RABAT = 0;
  cenamin = -1;
  iscena = 0;
  lastprior = -1;
  refpromocji = 0;
  refpromocjicen = 0;
  if(:ilosc is null) then ilosc=0;
  if(:cena is null) then cena=0;
  if(:mag is null) then mag = '';
  if(:zpakietem is null) then zpakietem = 0;
  if(:zast is null or :zast = 0) then zast = 1;
  if(:klient < 0) then begin
    grupakli = - :klient;
    klient = 0;
  end else begin
    select GRUPA from KLIENCI where REF=:KLIENT into :grupakli;
    if(:grupakli is null) then grupakli = 0;
  end
  select defcennik.rabatstalacena from defcennik where defcennik.ref = : cennik
    into :cenylicz;
  if (cenylicz is null) then cenylicz = 0;
  if (cennik is null or cennik = 0 or CENYLICZ = 0) then begin
    for select PROMOCJE.REF, PROMOCJE.TYP, PROMOCJE.CECHA, PROMOCJE.WARTCECHY, PROMOCJE.KTMMASKA, PROMOCJE.ODKTM, PROMOCJE.DOKTM, PROMOCJE.PRIORYTET, PROMOKLI.PROCENTRAB, PROMOCJE.BN
      from PROMOCJE
        join PROMOKLI on (PROMOCJE.REF = PROMOKLI.PROMOCJA)
       where TABRAB=1 AND ((PROMOKLI.GRUPAKLI = :grupakli) or (PROMOKLI.KLIENT=:KLIENT))
        and (PROMOCJE.MAGAZYN is null or PROMOCJE.MAGAZYN = '' or PROMOCJE.MAGAZYN = :mag) and bin_and(power(2,PROMOCJE.PROMTYP),:zast) > 0
        and (:zpakietem=0 or PROMOCJE.ZPAKIETEM=:zpakietem)
        and coalesce(PROMOCJE.DATAOD,current_timestamp(0)-1) < current_timestamp(0)
        and coalesce(PROMOCJE.DATADO, current_timestamp(0)+1) > current_timestamp(0)-1
      order by PROMOCJE.PRIORYTET DESC
      into :refprom, :typprom, :cechaprom, :wartcechyprom, :ktmmaska, :odktm, :doktm, :curprior, :kliprocentrab, :tpromobn
    do begin
      getprom = 0;
      towprocentrab = 0;
      towconstcena = -1;
      if(:curprior is null) then curprior = 0;
      if(:kliprocentrab is null) then kliprocentrab = 0;
      if(:curprior>=:lastprior) then begin
        if(:typprom = 'T') then begin
          cnt = null;

          grupa_tow = null;
          select t.grupa from towary t where t.ktm = :ktm
            into :grupa_tow;

          select first 1 1 from PROMOCT pr where pr.PROMOCJA=:refprom AND
              ((:KTM like pr.KTM and coalesce(pr.KTM,'') <> '')
              or (coalesce(pr.ktm,'') = '' and pr.grupatow = :grupa_tow))
            into :cnt;
          if(:cnt is null) then cnt = 0;
          if(:cnt > 0) then begin
            getprom = 1;

          select max(pr.PROCENT),min(pr.CONSTCENA) from PROMOCT pr
            where pr.PROMOCJA=:refprom AND
              ((:KTM like pr.KTM and coalesce(pr.KTM,'') <> '')
              or (coalesce(pr.ktm,'') = '' and pr.grupatow = :grupa_tow))
            into :towprocentrab, :towconstcena;

            if(:towprocentrab is null) then towprocentrab = 0;
            if((:towconstcena is not null) and (:towconstcena>0) and ((:towconstcena < :cenamin) or (:cenamin = -1))) then begin
              cenamin = :towconstcena;
              promobn = :tpromobn;
              refpromocjicen = :refprom;
            end
          end
        end else if(:typprom = 'G') then begin
          cnt = null;
          select DEFCECHY.rodzaj, DEFCECHY.tabela, DEFCECHY.pole from DEFCECHY where DEFCECHY.symbol=:cechaprom into :cecharodzaj, :cechatabela, :cechapole;
          if(:cecharodzaj = 1) then begin
            select first 1 1 from ATRYBUTY where KTM=:KTM and NRWERSJI=:WERSJA AND CECHA=:cechaprom AND WARTOSC=:wartcechyprom into :cnt;
          end
          if(:cecharodzaj = 0 and :cechatabela='TOWARY' and :cechapole='GRUPA') then begin
            select first 1 1 from TOWARY where KTM=:KTM and GRUPA=:wartcechyprom into :cnt;
          end
          if(:cnt is null) then cnt = 0;
          if(:cnt > 0) then getprom = 1;
        end else if(:typprom = 'Z') then begin
          getprom = 1;
          if(:getprom=1 and :odktm is not null and :odktm<>'' and :KTM<:odktm) then getprom = 0;
          if(:getprom=1 and :doktm is not null and :doktm<>'' and :KTM>:doktm) then getprom = 0;
          if(:getprom=1 and :ktmmaska is not null and :ktmmaska<>'' and not(:KTM like (:ktmmaska || '%'))) then getprom = 0;
/*        end else if(:typprom = 'U') then begin
          execute procedure X_OBLICZ_RABAT(:ktm,:wersja,:klient,:ilosc,:cena,:bn)
          returning_values :getprom,:towprocentrab,:towconstcena;
          if(:getprom = 1) then begin
            if(:towprocentrab is null) then towprocentrab = 0;
            if((:towconstcena is not null) and (:towconstcena>0) and ((:towconstcena < :cenamin) or (:cenamin = -1))) then begin
              cenamin = :towconstcena;
              promobn = :tpromobn;
            end
          end*/
        end else if(:typprom = 'U') then begin
          getprom = 1;
        end
      end
      if(:getprom > 0) then begin
        rabatprog = 0;
        for select PROMOCR.ilosc, PROMOCR.wartosc, PROMOCR.procent, PROMOCR.constcena, PROMOCR.constwal
        from PROMOCR
          left join PROMOKLI on (PROMOKLI.REF = PROMOCR.dlakontrahenta)
        where PROMOCR.PROMOCJA=:refprom
          and((PROMOKLI.REF is null) or (PROMOKLI.KLIENT = :klient) or (PROMOKLI.GRUPAKLI = :grupakli))
        into :rabil, :rabwar, :rabup, :cenaconst, :walmin
        do begin

          if(:rabil is null) then rabil = 0;
          if(:rabwar is null) then rabwar = 0;
          if(:cenaconst is null) then cenaconst = 0;

          if(:rabwar=0 or (:rabil<>0 and :rabwar<>0)) then begin
            if(:rabil <= :ilosc and :rabil >=0 and :rabup > :rabatprog) then rabatprog = :rabup;
            if(:rabil <= :ilosc AND :rabil >=0 and :cenaconst>0 and ((:cenaconst < :cenamin) or (:cenamin = -1))) then begin
              cenamin = :cenaconst;
              waluta = :walmin;
              promobn = :tpromobn;
              refpromocjicen = :refprom;
            end
          end
          if(:rabil=0 or (:rabil<>0 and :rabwar<>0)) then begin
            if((:rabwar <= :ilosc * :cena) and (:rabwar >= 0) and (:rabup > :rabatprog)) then rabatprog = :rabup;
            if((:rabwar <= :ilosc * :cena) and (:rabwar >= 0) and (:cenaconst>0) and ((:cenaconst < :cenamin) or (:cenamin = -1))) then begin
              cenamin = :cenaconst;
              waluta = :walmin;
              promobn = :tpromobn;
              refpromocjicen = :refprom;
            end
          end
        end
        rabatprog = :kliprocentrab + :towprocentrab + :rabatprog;
        if(:lastprior <> :curprior or :rabatprog > :rabat) then begin
          rabat = :rabatprog;
          refpromocji = :refprom;
        end
        lastprior = :curprior;
      end
    end
  end
-- ustal czy dajemy klientowi rabat procentowy, czy stala cene - wybierz to co korzystniejsze
  if (:rabat > 0 and :cena > 0 and :cenamin > 0) then
  begin
    cena = cast((:cena * (100 - :rabat)) / 100 as numeric(14,2));
    if (:cena < :cenamin) then
    begin
      cenamin = 0;
      iscena = 0;
    end
  end
  if(:cenamin > 0)then begin
    -- wybrano stala cene
    iscena = 1;
    if(:bn <> :promobn) then begin
      select vat.stawka from VAT join TOWARY on (TOWARY.VAT = VAT.grupa) where TOWARY.ktm = :ktm into :vat;
      if(:vat is null) then vat = 0;
      if(:bn = 'B') then
            cenamin = cast((:cenamin *(100 + :vat) / 100) as numeric(14,2));
      else
            cenamin = cast((:cenamin * 100/(:vat + 100)) as numeric(14,2));
    end
    rabat = :cenamin;
    refpromocji = :refpromocjicen;
  end
  suspend;
end^
SET TERM ; ^
