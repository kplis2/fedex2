--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_DEPARTMENTS(
      DPARENT varchar(40) CHARACTER SET UTF8                           )
  returns (
      DPARENTLIST varchar(255) CHARACTER SET UTF8                           )
   as
declare variable tmp varchar(255);
declare variable i integer;
declare variable j integer;
declare variable oper integer;
begin
  tmp = :dparent;
  dparentlist = ';'||:dparent||';';
  while(1=1) do begin
    tmp = null;
    select dparent from departments where symbol=:dparent into :tmp;
    if(:tmp is null) then exit;
    dparent = :tmp;
    if(position(';'||:dparent||';' in :dparentlist)>0) then exception universal 'Niedozwolone powiązanie wydzialów';
    dparentlist = :dparentlist||:dparent||';';
  end
end^
SET TERM ; ^
