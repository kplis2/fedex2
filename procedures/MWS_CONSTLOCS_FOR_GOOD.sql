--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CONSTLOCS_FOR_GOOD(
      WHIN varchar(3) CHARACTER SET UTF8                           ,
      GOODIN varchar(40) CHARACTER SET UTF8                           ,
      VERSIN integer)
  returns (
      WHOUT varchar(3) CHARACTER SET UTF8                           ,
      GOODOUT varchar(40) CHARACTER SET UTF8                           ,
      GOODNAME varchar(100) CHARACTER SET UTF8                           ,
      VERSOUT integer,
      VERSNAME varchar(50) CHARACTER SET UTF8                           ,
      MWSCONSTLOCREF integer,
      MWSCONSTLOCS varchar(20) CHARACTER SET UTF8                           ,
      QUANTITY numeric(15,4),
      RESERVED numeric(15,4),
      BLOCKED numeric(15,4),
      ORDSUSPENDED numeric(15,4),
      SUSPENDED numeric(15,4),
      ORDERED numeric(15,4),
      MWSORDREF integer,
      MWSORDS varchar(20) CHARACTER SET UTF8                           ,
      OPERATORREF integer,
      OPERATORNAME varchar(60) CHARACTER SET UTF8                           ,
      TIMESTOP timestamp)
   as
begin
  for select ms.wh, ms.good, t.nazwa, ms.vers, w.nazwa,
      ms.mwsconstloc, mc.symbol,
      sum(ms.quantity), sum(ms.reserved), sum(ms.blocked),
      sum(ms.ordsuspended), sum(ms.suspended), sum(ms.ordered)
    from mwsstock ms
      left join towary t on (t.ktm = ms.good)
      left join wersje w on (w.ref = ms.vers)
      left join mwsconstlocs mc on (mc.ref = ms.mwsconstloc)
      left join mwspallocs mp on (mp.ref = ms.mwspalloc)
    where ms.wh = :whin and ms.good = :goodin and ms.vers = :versin
      and mc.locdest <> 6
    group by ms.wh, ms.good, t.nazwa, ms.vers, w.nazwa,
        ms.mwsconstloc, mc.symbol
    into :whout, :goodout, :goodname, :versout, :versname,
        :mwsconstlocref, :mwsconstlocs,
        :quantity, :reserved, :blocked,
        :ordsuspended, :suspended, :ordered
  do begin
    mwsordref = null;
    mwsords = null;
    operatorref = null;
    operatorname = null;
    timestop = null;
    select first 1 mo.ref, mo.symbol, mo.operator, o.nazwa, mo.timestop
      from mwsords mo
        left join mwsordtypes mt on (mt.ref = mo.mwsordtype)
        left join operator o on (o.ref = mo.operator)
      where mt.mwsortypedets = 15 and mo.status = 5 and mo.docsymbs = :mwsconstlocs
      order by mo.timestop desc
      into :mwsordref, :mwsords, :operatorref, :operatorname, :timestop;
    suspend;
  end
end^
SET TERM ; ^
