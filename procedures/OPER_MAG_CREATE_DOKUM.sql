--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_MAG_CREATE_DOKUM(
      ZAM integer,
      MAGAZYN char(3) CHARACTER SET UTF8                           ,
      MAG2 char(3) CHARACTER SET UTF8                           ,
      TYP char(3) CHARACTER SET UTF8                           ,
      AKCEPT smallint,
      BLOKADA smallint,
      HISTZAM integer,
      TYPPOZ smallint,
      WYDANIA smallint,
      COPYSER smallint,
      ONEPOZ smallint,
      REALWART smallint,
      OPKTRYB smallint,
      COPYWYS smallint,
      ZNAGZAMALL smallint,
      TONAGFAK smallint,
      FILTR varchar(1024) CHARACTER SET UTF8                           ,
      DATA timestamp,
      RODZAJ integer,
      ZNAGZAMALLPROC varchar(40) CHARACTER SET UTF8                           ,
      MAGNOJOIN smallint)
  returns (
      DOKUMENT varchar(30) CHARACTER SET UTF8                           ,
      ERROR varchar(100) CHARACTER SET UTF8                           )
   as
declare variable DOKUMREF integer;
declare variable KTM varchar(40);
declare variable WERSJA integer;
declare variable ILOSC numeric(14,4);
declare variable ILREAL numeric(14,4);
declare variable ILDYSP numeric(14,4);
declare variable ILOSCO numeric(14,4);
declare variable ILREALO numeric(14,4);
declare variable JEDNM integer;
declare variable JEDN integer;
declare variable JEDNO integer;
declare variable TYPP integer;
declare variable SAPOZYCJE integer;
declare variable DOKPOZREF integer;
declare variable POZREF integer;
declare variable MAGWYDANIA smallint;
declare variable MAGZEWN smallint;
declare variable MAGTYP char(1);
declare variable DOSTAWA integer;
declare variable DOSTAWCA integer;
declare variable CENAMAG numeric(14,4);
declare variable CENAWAL numeric(14,4);
declare variable WARTREALZAM integer;
declare variable WARTREALPOZ integer;
declare variable KWARTMAG numeric(14,2);
declare variable SUMWARTRMAG numeric(14,2);
declare variable OPK smallint;
declare variable CENACEN numeric(14,4);
declare variable CENANET numeric(14,4);
declare variable CENABRU numeric(14,4);
declare variable WALCEN varchar(3);
declare variable OPKM smallint;
declare variable RABAT numeric(14,4);
declare variable RABATTAB numeric(14,4);
declare variable RABNAG numeric(14,4);
declare variable WARTNETTO numeric(14,4);
declare variable WARTBRUTTO numeric(14,4);
declare variable DOKPOZROZ integer;
declare variable REFFAK integer;
declare variable POZFAKREF integer;
declare variable ORG_REF integer;
declare variable DOKUMROZREF integer;
declare variable OBLICZCENZAK integer;
declare variable PRZELICZ numeric(14,4);
declare variable NUMBERPOZ integer;
declare variable WALUTOWY integer;
declare variable OPERATOR integer;
declare variable OPISWYD varchar(255);
declare variable PARAMN1 numeric(14,4);
declare variable PARAMN2 numeric(14,4);
declare variable PARAMN3 numeric(14,4);
declare variable PARAMN4 numeric(14,4);
declare variable PARAMN5 numeric(14,4);
declare variable PARAMN6 numeric(14,4);
declare variable PARAMN7 numeric(14,4);
declare variable PARAMN8 numeric(14,4);
declare variable PARAMN9 numeric(14,4);
declare variable PARAMN10 numeric(14,4);
declare variable PARAMN11 numeric(14,4);
declare variable PARAMN12 numeric(14,4);
declare variable PARAMS1 varchar(255);
declare variable PARAMS2 varchar(255);
declare variable PARAMS3 varchar(255);
declare variable PARAMS4 varchar(255);
declare variable PARAMS5 varchar(255);
declare variable PARAMS6 varchar(255);
declare variable PARAMS7 varchar(255);
declare variable PARAMS8 varchar(255);
declare variable PARAMS9 varchar(255);
declare variable PARAMS10 varchar(255);
declare variable PARAMS11 varchar(255);
declare variable PARAMS12 varchar(255);
declare variable PARAMD1 timestamp;
declare variable PARAMD2 timestamp;
declare variable PARAMD3 timestamp;
declare variable PARAMD4 timestamp;
declare variable SQL varchar(2048);
declare variable WARTOSC numeric(14,2);
declare variable ILOSCROZ numeric(14,4);
declare variable ILOSCOROZ numeric(14,4);
declare variable POZZAMROZ integer;
declare variable ILOSCROZUZYTE numeric(14,2);
declare variable POPREF integer;
declare variable PROCSTRING varchar(1024);
declare variable ZRODLO smallint;
declare variable BN char(1);
declare variable DOKBN char(1);
declare variable VAT numeric(14,4);
declare variable PRSHEET integer;
declare variable JEDNPRZELICZ numeric(14,4);
declare variable JEDNCONST integer;
declare variable ILOSCZADOPAKIET numeric(14,4);
declare variable FROMPROMOREF integer;
declare variable OPIS varchar(255);
declare variable PRCALCCOL integer;
declare variable REFDOPAKIET integer;
declare variable DOKUMPOZREF integer;
declare variable POZZAMREF integer;
declare variable PREC smallint;
declare variable GR_VAT varchar(5);
declare variable FAKEPOZZAM SMALLINT_ID;
declare variable ALTTOPOZ POZZAM_ID;
declare variable ALTTOPOZM DOKUMPOZ_ID;
declare variable havefake smallint;
begin
  if (exists(select first 1 1 from pozzam where zamowienie = :zam and fake = 1) and
      exists(select first 1 1 from defdokummag where magazyn = :magazyn and typ = :typ and coalesce(altallow,0) = 0)) then
    exception FAKE_NOT_ALLOW 'Pozycje aleternatywne nie dozwolone dla magazynu i typu dokumentu!';

  execute procedure GEN_REF('DOKUMNAG') returning_values :dokumref;
  select TYP, RABAT, ORG_REF, BN from NAGZAM where REF=:zam into :typp, :rabnag, :org_ref, :bn;
  if(:rabnag is null) then rabnag = 0;
  sapozycje = 0;
  if(:opktryb is null) then opktryb = 0;
  if(:wydania > 1) then wydania = 1;
  if(:typp is null) then TYPP = 0;
  if(:rodzaj is null) then rodzaj = 0;
  if(:znagzamall is null) then znagzamall = 0;
  select TYP from DEFMAGAZ where SYMBOL=:magazyn into :magtyp;
  select WYDANIA,zewn, OBLICZCENZAK, WALUTOWY from DEFDOKUM where SYMBOL = :typ into :magwydania,:magzewn, :obliczcenzak, :walutowy;
  if(:walutowy is null) then walutowy = 0;
  dostawa = NULL;
  /*zaożenie dostawy*/
/*  if(:magwydania = 0 and :magtyp = 'P') then begin
    execute procedure DOKUMNAG_CREATE_DOKMAG_DOSTAWA(NULL,:dostawca,current_date,:magazyn,NULL,NULL,NULL,(case when onepoz=1 then :zam else null end))
    returning_values :dostawa;
  end else
   dostawa = 0;*/
  if(:histzam is null) then histzam = 0;
  /* jesli operacja nie towrzy historii to dokument powstaje samodzielny*/
  if(:histzam=0) then begin
    execute procedure get_global_param('AKTUOPERATOR') returning_values :operator;
  end else begin
    select OPERATOR from HISTZAM where ref=:histzam into :operator;
  end
  zrodlo = 1;
  if(:magnojoin = 1) then zrodlo = 0;
  insert into DOKUMNAG(REF,MAGAZYN, TYP, DATA, DOSTAWA, DOSTAWCA, KLIENT,ZAMOWIENIE,AKCEPT,
        BLOKADA,MAG2, HISTORIA, ZAMZRODL, ZLECREF, ZLECNAZWA, UWAGI, KURS, WALUTA, tabkurs,
        TERMDOST, SPOSDOST, SPOSDOSTAUTO, TRASADOST, WYSYLKA, DULICA, DNRDOMU, DNRLOKALU, DMIASTO, DKODP, ODBIORCAID, RABAT, ZRODLO,
        KOSZTDOST, KOSZTDOSTBEZ, KOSZTDOSTROZ, KOSZTDOSTPLAT, FLAGISPED, UWAGISPED, ILOSCPACZEK, ILOSCPALET, ILOSCPALETBEZ, ODBIORCA,
        ZRODLOZAM, SPOSZAP, operator, CINOUT, SPRZEDAWCA, SPRZEDAWCAZEW, CKONTRAKTY, SRVREQUEST, BN, INT_SYMBOL) --DTS ZG138468 
    select :dokumref,:magazyn, :typ, :data, :dostawa,DOSTAWCA, KLIENT, REF, 0,
         0, :mag2, :histzam, REFZRODL, ZLECREF, ZLECNAZWA, UWAGI, case :walutowy when 1 then KURS else null end, case :walutowy when 1 then WALUTA else null end, case :walutowy when 1 then TABKURS else null end,
         TERMDOST, SPOSDOST, SPOSDOSTAUTO, TRASADOST, WYSYLKA * :copywys, DULICA, DNRDOMU, DNRLOKALU, DMIASTO, DKODP, ODBIORCAID, :rabnag, :zrodlo,
         KOSZTDOST, KOSZTDOSTBEZ, KOSZTDOSTROZ, KOSZTDOSTPLAT, FLAGISPED, UWAGISPED, ILOSCPACZEK, ILOSCPALET, ILOSCPALETBEZ, ODBIORCA,
         ZRODLOZAM, SPOSZAP, :operator, CINOUT, SPRZEDAWCA, SPRZEDAWCAZEW, CKONTRAKTY, SRVREQUEST, BN, INT_SYMBOL --DTS ZG138468
    from nagzam where REF=:zam;

  INSERT INTO NAGFAKTERMPLAT (DOKTYP, DOKREF, TERMZAP, PROCENTPLAT, SPOSPLAT)
    select 'M', :dokumref, TERMZAP, PROCENTPLAT, SPOSPLAT from NAGFAKTERMPLAT where DOKTYP='Z' and DOKREF=:zam;
  numberpoz = 0;

  if(:onepoz = 1) then begin
     if(:realwart>0) then wartrealzam = :zam;
     execute procedure GEN_REF('DOKUMPOZ') returning_values :dokpozref;
     sql = 'select KKTM, KWERSJA,KILOSC,KILREAL,KSUMWARTMAG,SUMWARTRMAG,PRSHEET from NAGZAM where REF='||:zam;
     if(filtr is not null and filtr <> '' ) then sql = sql||' and '||filtr;
     execute statement sql into :ktm, :wersja,:ilosc, :ilreal, :KWARTMAG, :SUMWARTRMAG, :PRSHEET;
     if(:ktm is not null and :ktm <> '') then begin
       if(:typp > 1 or :rodzaj = 1) then begin
       /*realizacja zamowienia*/
        ilosc = :ilreal;
       end
       if(:prsheet is not null) then begin
          select PRZELICZ, CONST
            from PRSHEETS p
              left join TOWJEDN t on(t.ref = p.jedn)
            where p.REF=:prsheet into :jednprzelicz, :jednconst;
          if(:jednconst = 1) then
            ilosc = ilosc *  jednprzelicz;
       end
       if(:magwydania = 0) then begin
         if(:znagzamall=2) then begin
           execute procedure GET_COSTOFPROD( :ktm, :wersja, NULL) returning_values :sumwartrmag, :prcalccol;
           sumwartrmag = :sumwartrmag*:ilosc;
         end else if(:znagzamall=1) then begin
           select sum(wartosc)
           from DOKUMNAG left join DEFDOKUM on (DEFDOKUM.SYMBOL = DOKUMNAG.typ)
           where ZAMOWIENIE = :zam and DEFDOKUM.WYDANIA = 1  and dokumnag.ref <> :dokumref
           into :sumwartrmag;
         end else if(:znagzamall=3) then begin
           if (znagzamallproc is null or znagzamallproc = '') then
             exception ZNAGZAMALLPROC_IS_NOT_DEFINED;
           procstring = 'select * from '||:znagzamallproc||'(0'||','''||:ktm||''','||:ilosc||','||:zam||',0'||:dokumref||')';
           execute statement procstring into :sumwartrmag;
         end else begin
           if(:histzam=0) then exception ZNAGZAMALLPROC_IS_NOT_DEFINED 'Operacja bez historii. Brak możliwości wyliczenia ceny mag.';
           select sum(WARTOSC) from DOKUMNAG left join DEFDOKUM on (DEFDOKUM.SYMBOL = DOKUMNAG.typ)
           where ZAMOWIENIE = :zam and HISTORIA = :histzam and DEFDOKUM.WYDANIA = 1 and dokumnag.ref <> :dokumref
           into :sumwartrmag;
         end
       end
       if(:typp > 1) then begin
       /*realizacja zamowienia*/
         if(:ilosc > 0) then
           cenamag = :sumwartrmag /:ilosc;
         else
           cenamag = 0;
       end else begin
         cenamag = :kwartmag;
       end
       insert into DOKUMPOZ(REF,DOKUMENT, KTM, WERSJA, ILOSC,CENA,WARTREALZAM)
         values (:dokpozref,:dokumref, :ktm, :wersja, :ilosc,:cenamag,:wartrealzam);
       sapozycje = 1;
     end
  end else begin
    sql = 'select POZZAM.ref, POZZAM.ktm, POZZAM.wersja, POZZAM.iloscm, POZZAM.ilrealm, POZZAM.ildysp,';
    sql = sql || '   POZZAM.ilosco, POZZAM.ilrealo,  POZZAM.JEDN, POZZAM.jedno,';
    sql = sql || '   POZZAM.CENANET, POZZAM.CENANET, POZZAM.opk, POZZAM.cenacen, POZZAM.cenanet, POZZAM.cenabru, POZZAM.walcen, POZZAM.wartnet, POZZAM.wartbru,';
    sql = sql || '   POZZAM.RABAT, POZZAM.rabattab , TOWARY.dm, POZZAM.opiswyd, substring(POZZAM.opis from 1 for 254),';
    sql = sql || '   POZZAM.paramn1, POZZAM.paramn2, POZZAM.paramn3, POZZAM.paramn4,';
    sql = sql || '   POZZAM.paramn5, POZZAM.paramn6, POZZAM.paramn7, POZZAM.paramn8,';
    sql = sql || '   POZZAM.paramn9, POZZAM.paramn10, POZZAM.paramn11, POZZAM.paramn12,';
    sql = sql || '   POZZAM.params1, POZZAM.params2, POZZAM.params3, POZZAM.params4,';
    sql = sql || '   POZZAM.params5, POZZAM.params6, POZZAM.params7, POZZAM.params8,';
    sql = sql || '   POZZAM.params9, POZZAM.params10, POZZAM.params11, POZZAM.params12,';
    sql = sql || '   POZZAM.paramd1, POZZAM.paramd2, POZZAM.paramd3, POZZAM.paramd4,';
    sql = sql || '   POZZAM.frompromoref, POZZAM.ilosczadopakiet, POZZAM.prec, POZZAM.gr_vat, POZZAM.fake, POZZAM.alttopoz, POZZAM.havefake ';
    sql = sql || '   from POZZAM left join TOWARY on(TOWARY.ktm = pozzam.ktm)';
    sql = sql || 'where POZZAM.zamowienie=0'||:zam;
    if(typpoz <> 1) then begin
      -- tryb w ktorym uwzgledniamy MAGAZYN i MAG2 przekazane w parametrach
      sql = sql || '  and POZZAM.MAGAZYN='''||:magazyn||'''';
      if(:magazyn=:mag2) then
        sql = sql || '  and (POZZAM.MAG2='''||:mag2||''' or POZZAM.MAG2 is null)';
      else
        sql = sql || '  and (POZZAM.MAG2='''||:mag2||''')';
    end
    if(opktryb = 1) then   sql = sql || '  and POZZAM.OPK = 0 ';
    if(opktryb = 2) then   sql = sql || '  and and POZZAM.OPK = 1';
    if (opktryb = 3) then sql = sql || ' and POZZAM.FAKE = 1 ';
  --  sql = sql || ') and ('||:opktryb||' = 0 or ('||:opktryb||' = 1 and OPK = 0) or ('||:opktryb||' = 2 and POZZAM.opk = 1)) ';
    if(filtr is not null and filtr <> '' ) then sql = sql || ' and '|| filtr;
    if(filtr is not null and filtr <> '' ) then sql = sql || ' and '|| filtr;
    sql = sql || '   order by POZZAM.NUMER, POZZAM.ALTTOPOZ nulls first';
    for execute statement :sql
    into :pozref,:ktm, :wersja, :ilosc, :ilreal, :ildysp,
      :ilosco, :ilrealo, :jedn, :jedno,
      :cenamag,:cenawal, :opk,:cenacen,:cenanet,:cenabru,:walcen,:wartnetto, :wartbrutto,
      :rabat, :rabattab, :jednm, :opiswyd, :opis,
      :paramn1, :paramn2, :paramn3, :paramn4,
      :paramn5, :paramn6, :paramn7, :paramn8,
      :paramn9, :paramn10, :paramn11, :paramn12,
      :params1, :params2, :params3, :params4,
      :params5, :params6, :params7, :params8,
      :params9, :params10, :params11, :params12,
      :paramd1, :paramd2, :paramd3, :paramd4,
      :frompromoref, :ilosczadopakiet, :prec, :gr_vat, :fakepozzam, :alttopoz, :havefake
      do begin
        alttopozm = null;
        if(:typp > 1 or :rodzaj = 1) then begin
          ilosc = :ilreal;
          ilosco = :ilrealo;
        end else if (:typp<>2 and :rodzaj in (2,3)) then begin
          -- jesli robimy dyspozycje ale bez klona to bierzemy ilosci dysponowane
          ilosc = :ildysp;
          ilosco = :ildysp;
        end
        if(:ilosc > 0) then begin
          if(:realwart>0) then
            wartrealpoz = :pozref;
          if(:opk = 0) then
            opkm = 0;
          else if(:cenanet > 0) then
            opkm = 1;
          else
            opkm = 1;
          /*przeliczenie jednostki sprzedaży na jednostk magazynową*/
          if(:jedn <> :jednm)then begin
            select TOWJEDN.przelicz from TOWJEDN where ref=:jedn into :przelicz;
            if(:przelicz is null or (:przelicz < 0))then przelicz = 1;
            if(:przelicz <> 1) then begin
              cenacen = :cenacen / :przelicz;
              cenanet = :cenanet / :przelicz;
              cenabru = :cenabru / :przelicz;
              cenamag = :cenamag / :przelicz;
              cenawal = :cenawal / :przelicz;
            end
          end
      --jesli zakup walutowy to wyczysc rabat bo cena walutowa jest po rabacie
      if(magwydania = 0 and walutowy = 1) then rabat = 0;
      --ustalanie ceny magazynowej
      if (magwydania  = 0 and znagzamall = 4) then begin
      --generowanie pozycji wg rozpisek z dokumentu wydania
        select max(p.ref) from pozzam p where p.prpozzam = :pozref into :pozzamroz;
        select popref from pozzam where ref = :pozref into :popref;
        if(popref is null) then popref = :pozref;
        --dla klonów
        if(pozzamroz is null) then select max(p.ref) from pozzam p where p.prpozzam = (select popref from pozzam where ref = :pozref) into :pozzamroz;
        cenamag = 0;
        while (ilosc > 0) do begin
          iloscroz = 0;
          --wyszukanie ceny z której pozostala ilosc nie rozpisana
          select first 1 dr.cena, sum(dr.ilosc)
            from dokumpoz dp
            left join dokumroz dr on (dr.pozycja = dp.ref)
            where dp.frompozzam = :pozzamroz and dr.cena > :cenamag
            group by dr.cena
          into :cenamag, :iloscroz;
          if(iloscroz = 0) then begin
            cenamag = 0;
            iloscroz = ilosc;
            iloscoroz = ilosco;
            ilosc = 0;
          end else begin
            --stornowanie ilosci o juz wydany rozchod z tej ceny
            select sum(dp.ilosc)
              from pozzam p
              left join dokumpoz dp on (dp.frompozzam = p.ref)
            where (p.popref is not null and p.popref = :popref or p.ref = :pozref) and dp.cena  = :cenamag into :iloscrozuzyte;
            if(iloscrozuzyte is not null) then iloscroz = iloscroz - iloscrozuzyte;
            if(ilosc < iloscroz) then iloscroz = ilosc;
            iloscoroz = ilosco * iloscroz / ilosc;
            ilosc = ilosc - iloscroz;
          end
          if(iloscroz > 0) then begin
            execute procedure GEN_REF('DOKUMPOZ') returning_values :dokpozref;
            if (alttopoz is not null) then
            begin
              select d.ref
                from dokumpoz d
                where d.dokument = :dokumref
                  and d.frompozzam = :alttopoz
                into :alttopozm;
            end 
            insert into DOKUMPOZ(REF,DOKUMENT, NUMER, ORD, KTM, WERSJA, ILOSC, ILOSCO, JEDNO,CENA,CENAWAL,WARTREALPOZ,WARTREALTRYB,CENACEN,cenanet,cenabru,walcen, wartsnetto, wartsbrutto, RABAT, RABATTAB, opk, FROMPOZZAM, OPISWYD, OPIS,
                             paramn1, paramn2, paramn3, paramn4,
                             paramn5, paramn6, paramn7, paramn8,
                             paramn9, paramn10, paramn11, paramn12,
                             params1, params2, params3, params4,
                             params5, params6, params7, params8,
                             params9, params10, params11, params12,
                             paramd1, paramd2, paramd3, paramd4, frompromoref, ilosczadopakiet, prec, gr_vat,
                             alttopoz, fake, havefake)
              values (:dokpozref,:dokumref, :numberpoz, 1, :ktm, :wersja, :iloscroz, :iloscoroz, :jedno,:cenamag, case :walutowy when 1 then :cenawal else 0 end,:wartrealpoz,:realwart,:cenacen,:cenanet,:cenabru,:walcen, :wartnetto, :wartbrutto, :rabat, :rabattab, :opkm, :pozref, :opiswyd, :opis,
                  :paramn1, :paramn2, :paramn3, :paramn4,
                  :paramn5, :paramn6, :paramn7, :paramn8,
                  :paramn9, :paramn10, :paramn11, :paramn12,
                  :params1, :params2, :params3, :params4,
                  :params5, :params6, :params7, :params8,
                  :params9, :params10, :params11, :params12,
                  :paramd1, :paramd2, :paramd3, :paramd4, :frompromoref, :ilosczadopakiet, :prec, :gr_vat,
                  :alttopozm, :fakepozzam, :havefake);
          end
        end
      end else if(magwydania = 1) then begin
        cenamag = 0;
        wartosc = 0;
      end
      execute procedure GEN_REF('DOKUMPOZ') returning_values :dokpozref;
      if (coalesce(:fakepozzam,0) = 0) then
      numberpoz = :numberpoz + 1;
      if (alttopoz is not null) then
      begin
        select d.ref
          from dokumpoz d
          where d.dokument = :dokumref
            and d.frompozzam = :alttopoz
          into :alttopozm;
      end
      if(:wydania = 0 and znagzamall <> 4) then
        insert into DOKUMPOZ(REF,DOKUMENT, NUMER, ORD, KTM, WERSJA, ILOSC, ILOSCO, JEDNO,CENA,CENAWAL,WARTREALPOZ,WARTREALTRYB,
                             CENACEN,cenanet,cenabru,walcen, wartsnetto, wartsbrutto, RABAT, RABATTAB, opk, FROMPOZZAM, OPISWYD, OPIS,
                             paramn1, paramn2, paramn3, paramn4,
                             paramn5, paramn6, paramn7, paramn8,
                             paramn9, paramn10, paramn11, paramn12,
                             params1, params2, params3, params4,
                             params5, params6, params7, params8,
                             params9, params10, params11, params12,
                             paramd1, paramd2, paramd3, paramd4, frompromoref, ilosczadopakiet, prec, gr_vat, obliczcenzak,
                             alttopoz , fake, havefake)
          values (:dokpozref,:dokumref, :numberpoz, 1, :ktm, :wersja, :ilosc, :ilosco, :jedno,:cenamag, case :walutowy when 1 then :cenawal else 0 end,:wartrealpoz,:realwart,
                  :cenacen,:cenanet,:cenabru,:walcen, :wartnetto, :wartbrutto, :rabat, :rabattab, :opkm, :pozref, :opiswyd, :opis,
                  :paramn1, :paramn2, :paramn3, :paramn4,
                  :paramn5, :paramn6, :paramn7, :paramn8,
                  :paramn9, :paramn10, :paramn11, :paramn12,
                  :params1, :params2, :params3, :params4,
                  :params5, :params6, :params7, :params8,
                  :params9, :params10, :params11, :params12,
                  :paramd1, :paramd2, :paramd3, :paramd4, :frompromoref, :ilosczadopakiet, :prec, :gr_vat, :obliczcenzak,
                  :alttopozm, :fakepozzam, :havefake);
      else if(znagzamall <> 4) then
        insert into DOKUMPOZ(REF,DOKUMENT, NUMER, ORD, KTM, WERSJA, ILOSC, ILOSCO, JEDNO, WARTREALPOZ,WARTREALTRYB,
                             CENACEN,cenanet,cenabru,walcen, wartsnetto, wartsbrutto, RABAT, RABATTAB, opk, frompozzam, OPISWYD, OPIS,
                             paramn1, paramn2, paramn3, paramn4,
                             paramn5, paramn6, paramn7, paramn8,
                             paramn9, paramn10, paramn11, paramn12,
                             params1, params2, params3, params4,
                             params5, params6, params7, params8,
                             params9, params10, params11, params12,
                             paramd1, paramd2, paramd3, paramd4, frompromoref, ilosczadopakiet, prec, gr_vat,
                             alttopoz , fake, havefake)
          values (:dokpozref,:dokumref, :numberpoz, 1, :ktm, :wersja, :ilosc, :ilosco, :jedno, :wartrealpoz,:realwart,
                  :cenacen,:cenanet,:cenabru,:walcen,:wartnetto, :wartbrutto, :rabat, :rabattab, :opkm, :pozref, :opiswyd, :opis,
                  :paramn1, :paramn2, :paramn3, :paramn4,
                  :paramn5, :paramn6, :paramn7, :paramn8,
                  :paramn9, :paramn10, :paramn11, :paramn12,
                  :params1, :params2, :params3, :params4,
                  :params5, :params6, :params7, :params8,
                  :params9, :params10, :params11, :params12,
                  :paramd1, :paramd2, :paramd3, :paramd4, :frompromoref, :ilosczadopakiet, :prec, :gr_vat,
                  :alttopozm, :fakepozzam, :havefake);
        sapozycje = 1;
        if(:copyser = 1) then begin
          -- SERFIX1 --
          execute procedure COPY_DOKUMSER(:pozref, :dokpozref, :ilosc, 'Z', 'M', 0);
          -- SERFIX1 --
        end
      end
    end
  end

  if(:sapozycje > 0) then begin
    if(:tonagfak > 0) then begin
      /*okrslenie faktury, z ktora ma byc zlaczenie*/
      select faktura from NAGZAM where ref=:zam into :reffak;
      if(:reffak is null and :tonagfak = 2) then
        select FAKTURA from NAGZAM where NAGZAM.ref = :org_ref into :reffak;
      if(:reffak > 0) then begin
        /*zaznaczenie naglowka dokumentu*/
        update DOKUMNAG set FAKTURA = :reffak where REF=:dokumref;
        /*dla każdej pozycji dokumetntu okrel pozycj dok. VAT*/
        for select DOKUMPOZ.REF, DOKUMPOZ.frompozzam, POZFAK.ref
        from DOKUMPOZ left join POZFAK on (DOKUMPOZ.FROMPOZZAM = POZFAK.frompozzam and POZFAK.dokument = :reffak)
        where DOKUMPOZ.DOKUMENT = :dokumref
        into :dokpozref, :pozref, :pozfakref
        do begin
          if(:pozfakref is null) then begin
            select max(POZFAK.REF)
              from POZFAK  join POZZAM  POZORG on (POZFAK.FROMPOZZAM = POZORG.REF and POZORG.zamowienie = :org_ref)
                           join POZZAM  POZYC on (POZORG.REF = POZYC.POPREF)
            where POZYC.REF = :pozref
            into :pozfakref;
          end
          if(:pozfakref > 0) then begin
            update DOKUMPOZ set FROMPOZFAK = :pozfakref where DOKUMPOZ.REF = :dokpozref;
          end
        end
      end
    end
      /* przenoszenie refdopakiet z zamowien na dokumenty magazynowe */
    for select dp.ref, dp.frompozzam from dokumpoz dp where dp.opk = 1 and dp.dokument = :dokumref
        into :dokumpozref, :pozzamref
    do begin
      refdopakiet = null;
      select pz.refdopakiet from pozzam pz where pz.ref = :pozzamref and opk = 1
      into :refdopakiet;
      if(:refdopakiet is not null) then begin
        update dokumpoz dp1 set dp1.refdopakiet = (select ref from dokumpoz dp2 where frompozzam = :refdopakiet)
          where dp1.ref = :dokumpozref;
      end
    end
    if(:akcept = 1) then
      update DOKUMNAG set AKCEPT =1 where REF=:dokumref;
    if(:blokada = 1) then
      update DOKUMNAG set BLOKADA=bin_or(BLOKADA,1) where REF=:dokumref;
    select SYMBOL from DOKUMNAG where REF=:dokumref into :dokument;


    if(:onepoz > 0 and :magwydania = 0 and :typp > 1) then begin
      /*uzupelnienie polaczenia pomiedzy rozpiskami w celach odtwarzania cen*/
      select ref from DOKUMROZ where DOKUMROZ.pozycja = :dokpozref into :dokpozroz;
      if(:dokpozroz > 0 and :histzam > 0) then
        for select DOKUMROZ.REF
          from DOKUMROZ
          join DOKUMPOZ on (DOKUMPOZ.REF = DOKUMROZ.POZYCJA)
          join DOKUMNAG on (DOKUMNAG.REF=DOKUMPOZ.DOKUMENT)
          left join defdokum on (DEFDOKUM.SYMBOL = DOKUMNAG.TYP)
          where DOKUMNAG.historia = :histzam and defdokum.wydania = 1 and dokumnag.akcept > 0
          into :dokumrozref
        do begin
          update DOKUMROZ set CENATOROZ = :DOKPOZROZ, CENATOPOZ = :dokpozref where ref=:dokumrozref;
        end
    end
  end else begin
    delete from DOKUMNAG where REF=:dokumref;
    dokument = '';
  end
  error = '';
end^
SET TERM ; ^
