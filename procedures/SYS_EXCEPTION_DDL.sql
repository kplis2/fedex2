--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_EXCEPTION_DDL(
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      CREATE_OR_ALTER smallint)
  returns (
      DDL blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           )
   as
declare variable msg varchar(255);
begin
  SELECT RDB$MESSAGE FROM RDB$EXCEPTIONS WHERE RDB$EXCEPTION_NAME = :nazwa
  into :msg;
  ddl = 'CREATE OR ALTER EXCEPTION '||:nazwa||' '''||:msg||''';';
  suspend;
end^
SET TERM ; ^
