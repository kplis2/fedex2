--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PAYROLLBELTS(
      PAYROLL integer)
  returns (
      PAGENO integer,
      REF integer,
      PERSONNAMES varchar(120) CHARACTER SET UTF8                           ,
      PESEL varchar(11) CHARACTER SET UTF8                           ,
      NFZ varchar(20) CHARACTER SET UTF8                           ,
      NPODSTZUS numeric(14,2),
      NDOCHOD numeric(14,2))
   as
declare variable ONPAGE integer;
declare variable LINES integer;
declare variable I integer;
declare variable PREF integer;
declare variable FDATE timestamp;
declare variable FROMDATE timestamp;
declare variable TODATE timestamp;
declare variable TPER char(6);
declare variable IPER char(6);
declare variable SHORTVER smallint;
begin
  pageno = 1;
  onpage = 41;
  lines = 0;

  if (payroll < 0) then begin
  --drukujemy skrocona liste plac, wywolywanie RPT_E_PAYROLL nie jest konieczne (BS52425)
    shortver = 1;
    payroll = abs(payroll);
  end else
    shortver = 0;

  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  select tper, iper from epayrolls
    where ref = :payroll
    into :tper, :iper;

  for
   select distinct e.ref, e.personnames, p.pesel, p.ref
     from employees e
       join eprpos r on (r.employee = e.ref)
       join persons p on (p.ref = e.person)
     where payroll = :payroll
     order by e.sname, e.fname
     into :ref, :personnames, :pesel, :pref
  do begin

    if (shortver = 0) then
    begin
      select count(*)
        from rpt_e_payroll(:payroll, :ref)
        into :i;

      if (lines + i > onpage) then begin
        pageno = pageno + 1;
        lines = i;
      end else
        lines = lines + i;

      lines = lines + 5;   --zwiekszam tez o 5 na nazwisko, podsumowania itd.
    end

    fdate = null;
    nfz = null;
    select max(fromdate)
      from ezusdata where person = :pref and fromdate <= :todate
      into :fdate;
    if (fdate is not null) then
      select C.code
        from ezusdata Z
          join edictzuscodes C on (Z.nfz = C.ref)
        where Z.person = :pref and Z.fromdate = :fdate
        into :nfz;

    npodstzus = 0;
    ndochod = 0;
    select sum(P.pvalue)
      from epayrolls PR
        join eprpos P on (P.payroll = PR.ref and P.ecolumn = 6000)
      where P.employee = :ref and PR.iper >= substring(:iper from 1 for 4) || '01'
        and PR.iper <= :iper
      into :npodstzus;

    select sum(P.pvalue)
      from epayrolls PR
        join eprpos P on (P.payroll = PR.ref and P.ecolumn = 7000)
      where P.employee = :ref and PR.tper >= substring(:tper from 1 for 4) || '01'
        and PR.tper <= :tper
      into :ndochod;

    npodstzus = coalesce(npodstzus,0);
    ndochod = coalesce(ndochod,0);
    suspend;
  end
end^
SET TERM ; ^
