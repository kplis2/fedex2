--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INWENTA_OBLNAG(
      INWENTAREF integer)
   as
declare variable wartmag numeric(14,2);
declare variable maroznice smallint;
declare variable inwp integer;
declare variable inwm integer;
declare variable wartplus numeric(14,2);
declare variable wartminus numeric(14,2);
declare variable zpartiami integer;
begin
  wartmag = 0.00;
  maroznice = 0;
  if(exists(select first 1 1 from inwentap where inwenta=:inwentaref and status=2)) then
    maroznice = 1;

  select inwp,inwm, coalesce(zpartiami,0)
    from inwenta where ref=:inwentaref
  into :inwp,:inwm, :zpartiami;
  wartplus = 0;
  wartminus = 0;
  if(:inwp is null and :inwm is null) then begin
     if(zpartiami=0) then begin
       select sum(wartosc)
         from (select ((sum(ilinw) - max(ilstan)) * max(cenaplus)) as wartosc
           from inwentap where inwenta = :inwentaref
           group by wersjaref)
       into :wartmag;
     end else begin
       select sum(wartosc)
         from (select ((sum(ilinw) - max(ilstan)) * max(cenaplus)) as wartosc
           from inwentap where inwenta = :inwentaref
           group by wersjaref, dostawa)
       into :wartmag;
     end
     if(wartmag is null) then
       wartmag = 0.00;
  end else begin
    if(:inwp is not null) then select wartosc from dokumnag where ref=:inwp into :wartplus;
    if(:inwm is not null) then select wartosc from dokumnag where ref=:inwm into :wartminus;
    wartmag = :wartplus - :wartminus;
  end

  update inwenta set wartmag = :wartmag, maroznice = :maroznice where ref=:inwentaref;
end^
SET TERM ; ^
