--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SLOWNIK_NAZWA_FROMKS(
      SLODEFREF integer,
      SLOKSKOD varchar(255) CHARACTER SET UTF8                           )
  returns (
      RESULT varchar(255) CHARACTER SET UTF8                           )
   as
declare variable RES integer;
declare variable SLONAZWA varchar(255);
declare variable INDEKS varchar(255);
declare variable PREFIX varchar(255);
declare variable REFFIELD varchar(255);
declare variable KODFIELD varchar(255);
declare variable KODKSFIELD varchar(255);
declare variable NAZWAFIELD varchar(255);
declare variable NIPFIELD varchar(255);
declare variable FILTR varchar(255);
declare variable FILTRNAME varchar(255);
declare variable SQL varchar(1024);
declare variable CURRCOMPANY varchar(255);
begin
  if(slodefref > 0) then
  begin
    execute procedure get_global_param('CURRENTCOMPANY') returning_values :currcompany;
    execute procedure SLOPARAMS(:slodefref,null)
    returning_values :res,:slonazwa,:indeks,:prefix,:reffield,:kodfield,
    :kodksfield,:nazwafield,:nipfield,:filtr,:filtrname;
    --exception test_break''||:nazwafield||' '||:slonazwa||' '||coalesce(:currcompany,'null')||' '||:kodksfield||' '||:slokskod;

    if(:res > 0) then
    begin
      if(:slonazwa = 'SLOPOZ') then
      begin
        sql = 'select first 1 '||:nazwafield||' from '||:slonazwa||' where SLOWNIK='||replace(:slodefref,'''','''''')||' and '||replace(:kodksfield,'''','''''')||'='''||replace(:slokskod,'''','''''')||'''';
        execute statement sql into :result;
      end 
      else
      begin
        if (slonazwa in ('KLIENCI', 'DOSTAWCY', 'CPERSONS', 'EMPLOYEES', 'FK_PERSONS')) then  --PR59074
        begin
          sql = 'select first 1 '||:nazwafield||' from '||:slonazwa||' where '||replace(:kodksfield,'''','''''')||'='''||replace(:slokskod,'''','''''')||''''||
          case when coalesce(:filtr,'') <> '' then ' and '||:filtr else '' end;
          execute statement sql into :result;
        end
        else
        begin
          sql = 'select first 1 '||:nazwafield||' from '||:slonazwa||' where '||replace(:kodksfield,'''','''''')||'='''||replace(:slokskod,'''','''''')||'''';
          execute statement sql into :result;
        end
      end
    end
  end
  else
    result  = '';
  suspend;
end^
SET TERM ; ^
