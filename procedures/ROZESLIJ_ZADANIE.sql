--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ROZESLIJ_ZADANIE(
      REFZAD integer,
      SYMBGR varchar(40) CHARACTER SET UTF8                           )
   as
declare variable refop integer;
declare variable zad integer;
declare variable opzal integer;
declare variable naz varchar(80);
declare variable data timestamp;
declare variable datado timestamp;
declare variable opis varchar(1024);
declare variable osoba integer;
declare variable cpodmiot integer;
declare variable cpodmskrot varchar(40);
declare variable cpodmoperopiek integer;
declare variable cpodmslodef integer;
declare variable cpodmtelefon varchar(60);
declare variable wfworkflow integer;
begin
  select z.zadanie, z.nazwa, z.operzal, z.data, z.datado, z.opis, z.osoba, z.cpodmiot, z.cpodmskrot,
         z.cpodmoperopiek, z.cpodmslodef, z.cpodmtelefon, z.wfworkflow
  from zadania z
  where z.ref=:refzad
  into :zad, :naz, :opzal, :data, :datado, :opis, :osoba, :cpodmiot, :cpodmskrot,
       :cpodmoperopiek, :cpodmslodef, :cpodmtelefon, :wfworkflow;
  for select operator.ref
  from operator
  where operator.grupa like '%;'||:SYMBGR||';%'
  into :refop
  do begin
    begin
      insert into zadania (ZADANIE, DATA, DATADO, NAZWA, OPERZAL, OPERWYK, OPIS, OSOBA,
                          CPODMIOT, CPODMSKROT, CPODMOPEROPIEK, CPODMSLODEF, CPODMTELEFON, WFWORKFLOW)
      values (:zad, :data, :datado, :naz, :opzal, :refop, :opis, :osoba,
              :cpodmiot, :cpodmskrot, :cpodmoperopiek, :cpodmslodef, :cpodmtelefon, :wfworkflow);
    end
  end
end^
SET TERM ; ^
