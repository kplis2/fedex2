--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_STOCKDOCS_POS(
      MWSORD integer,
      LOTREG smallint)
  returns (
      VERS integer,
      LOT integer,
      GOOD varchar(20) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4),
      QUANTITYC numeric(14,4),
      MWSCONSTLOCP integer,
      MWSPALLOCP integer,
      MWSCONSTLOCL integer,
      MWSPALLOCL integer)
   as
begin
  if (lotreg = 1) then
  begin
    for
      select vers, lot, max(good), sum(quantity), sum(quantityc), max(mwsconstlocp),
          max(mwspallocp), max(mwsconstlocl), max(mwspallocl)
        from mwsacts
        where mwsord = :mwsord and status = 5
        group by vers, lot
        having sum(quantity) <> sum(quantityc)
        into vers, lot, good, quantity, quantityc, mwsconstlocp,
          mwspallocp, mwsconstlocl, mwspallocl
    do begin
      suspend;
    end
  end else
  begin
    for
      select vers, null, max(good), sum(quantity), sum(quantityc), max(mwsconstlocp),
          max(mwspallocp), max(mwsconstlocl), max(mwspallocl)
        from mwsacts
        where mwsord = :mwsord and status = 5
        group by vers
        having sum(quantity) <> sum(quantityc)
        into vers, lot, good, quantity, quantityc, mwsconstlocp,
          mwspallocp, mwsconstlocl, mwspallocl
    do begin
      suspend;
    end
  end
end^
SET TERM ; ^
