--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_IMP_PRZEPROGRAMUJ(
      DOKPOZ DOKUMPOZ_ID,
      Z_NRWERSJI NRWERSJI_ID,
      MODE SMALLINT_ID)
  returns (
      DOCIDRW DOKUMNAG_ID,
      DOCIDPW DOKUMNAG_ID,
      MSG STRING255,
      STATUS SMALLINT_ID)
   as
declare variable ildosprz quantity_mws;
declare variable ilpotrzebna quantity_mws;
declare variable ildoprzepr quantity_mws;
declare variable tymil quantity_mws;
declare variable ktm ktm_id;
declare variable celwersja nrwersji_id;
declare variable fromwersja nrwersji_id;
declare variable aktuoperator string255;
declare variable poz_int_id string120;
declare variable dok_int_id string120;
declare variable docpozRW dokumpoz_id;
declare variable docpozPW dokumpoz_id;
declare variable dokumnagWZ dokumnag_id;

begin
-- EHRLE MKD Automatyczne wydanie RW i PW jezeli potrzebne przeprogramowanie
-- mode = 0 przeprogramowanie wymuszone z podanej wersji chyba że nie podano wersji wtedy z pierwszej dostepnej
-- mode = 1 przeprogramowanie najpierw z podanej wersji a potem z pierwszej dostepnej
    msg='';
    status = 0;
    select p.ilosc, p.ktm, w.nrwersji, p.int_id, d.int_id, d.ref  --szukam ile i jakiego towaru potrzeba
        from dokumpoz p
            join wersje w on (w.ref = p.wersjaref and w.ktm = p.ktm)
            join dokumnag d on(d.ref = p.dokument)
        where p.ref = :dokpoz
        into :ilpotrzebna, :ktm, :celwersja, :poz_int_id, :dok_int_id, :dokumnagWZ;

    select first 1      -- szukam czy taki towar w tej wersji jest dostepny
       coalesce(sum((case when VS.ILOSC <= 0 then 0 else
       (case when VS.ALTPOSMODE = 1 then
       (select KPL.QUANTITY from MWS_CALC_KPL_QUANTITY(VS.MAGAZYN, VS.WERSJAREF, null) KPL)
       else (case when VS.ILOSC < coalesce(Q.QUANTITY, 0) then VS.ILOSC
       else coalesce(Q.QUANTITY, 0) end) end) - VS.ZABLOKOW end)),0)
            from STANYIL VS
            join WERSJE W on (W.REF = VS.WERSJAREF)
            left join DEFMAGAZ M on (M.SYMBOL = VS.MAGAZYN)
            left join VIEW_MWS_QUANTITY Q on (Q.WH = VS.MAGAZYN and Q.VERS = VS.WERSJAREF)
            left join TOWARY T on (T.KTM = VS.KTM)
            where VS.MAGAZYN = 'MWS' and VS.ILOSC <> 0
            and t.ktm is not distinct from :ktm
            and w.nrwersji = :celwersja
            group by t.ktm, w.ref,t.int_id, vs.miara
    into :ildosprz;
    if (:ildosprz is null) then
        ildosprz =0;
    ildoprzepr = ilpotrzebna -ildosprz; --ustalam ile towaru trzeba przeprogramowac
    --wlasciwe tworzenie RW i PW
    if(coalesce(ildoprzepr,0) > 0 )then
    begin
        select * from get_global_param('AKTUOPERATOR') into aktuoperator;
        if(:aktuoperator is null) then aktuoperator ='74';
        --naglowek RW
        execute procedure gen_ref('DOKUMNAG') returning_values docidRW;
        insert into DOKUMNAG(REF, MAGAZYN, TYP, DATA, DOSTAWA, DOSTAWCA, KLIENT, ZAMOWIENIE, AKCEPT, BLOKADA, MWSBLOCKMWSORDS,BLOCKMWSORDS,  zrodlo, operator, operakcept, int_id, grupaprod)
          values (:docidRW, 'MWS', 'RW', current_date, null, null, null, null, 0, 0, 0,0, 99,:aktuoperator, :aktuoperator, :dok_int_id, :dokpoz );


        if (:z_nrwersji is not null) then begin
            select first 1  -- ustalam ile jest towaru z ktorego mam zrobic przeprogramowanie
               coalesce(sum((case when VS.ILOSC <= 0 then 0 else
               (case when VS.ALTPOSMODE = 1 then
               (select KPL.QUANTITY from MWS_CALC_KPL_QUANTITY(VS.MAGAZYN, VS.WERSJAREF, null) KPL)
               else (case when VS.ILOSC < coalesce(Q.QUANTITY, 0) then VS.ILOSC
               else coalesce(Q.QUANTITY, 0) end) end) - VS.ZABLOKOW end)),0) , w.nrwersji
                    from STANYIL VS
                    join WERSJE W on (W.REF = VS.WERSJAREF)
                    left join DEFMAGAZ M on (M.SYMBOL = VS.MAGAZYN)
                    left join VIEW_MWS_QUANTITY Q on (Q.WH = VS.MAGAZYN and Q.VERS = VS.WERSJAREF)
                    left join TOWARY T on (T.KTM = VS.KTM)
                    where VS.MAGAZYN = 'MWS' and VS.ILOSC <> 0
                    and t.ktm is not distinct from :ktm
                    and w.nrwersji = :z_nrwersji
                    group by t.ktm, w.ref,w.nrwersji,t.int_id, vs.miara
            into :ildosprz, :fromwersja;
            if (ildosprz is null) then
                ildosprz = 0;
            --wpisuje na RW
            execute procedure gen_ref('DOKUMPOZ') returning_values docpozRW;
            insert into DOKUMPOZ(REF, DOKUMENT, KTM, ILOSC,  WERSJA, int_id)
            values (:docpozRW, :docidRW, :ktm, minvalue(:ildosprz,:ildoprzepr), :fromwersja, :poz_int_id);

            ildoprzepr = ildoprzepr - minvalue(:ildosprz,:ildoprzepr);
        end
        if((mode = 1 and ildoprzepr > 0) or :z_nrwersji is null )then
        begin
            fromwersja = 0;
            ildosprz = null;
            while( ildoprzepr > 0 and fromwersja is not null) do
            begin
                fromwersja = null;
                select first 1  -- ustalam ilosc towaru i wersje z jakiej przeprogramowuje
                   coalesce(sum((case when VS.ILOSC <= 0 then 0 else
                   (case when VS.ALTPOSMODE = 1 then
                   (select KPL.QUANTITY from MWS_CALC_KPL_QUANTITY(VS.MAGAZYN, VS.WERSJAREF, null) KPL)
                   else (case when VS.ILOSC < coalesce(Q.QUANTITY, 0) then VS.ILOSC
                   else coalesce(Q.QUANTITY, 0) end) end) - VS.ZABLOKOW end)),0) , w.nrwersji
                        from STANYIL VS
                        join WERSJE W on (W.REF = VS.WERSJAREF)
                        left join DEFMAGAZ M on (M.SYMBOL = VS.MAGAZYN)
                        left join VIEW_MWS_QUANTITY Q on (Q.WH = VS.MAGAZYN and Q.VERS = VS.WERSJAREF)
                        left join TOWARY T on (T.KTM = VS.KTM)
                        where VS.MAGAZYN = 'MWS' and VS.ILOSC <> 0
                        and t.ktm is not distinct from :ktm
                        group by t.ktm, w.ref,w.nrwersji,t.int_id, vs.miara
                        having     coalesce(sum((case when VS.ILOSC <= 0 then 0 else
                           (case when VS.ALTPOSMODE = 1 then
                           (select KPL.QUANTITY from MWS_CALC_KPL_QUANTITY(VS.MAGAZYN, VS.WERSJAREF, null) KPL)
                           else (case when VS.ILOSC < coalesce(Q.QUANTITY, 0) then VS.ILOSC
                           else coalesce(Q.QUANTITY, 0) end) end) - VS.ZABLOKOW end)),0) >0
                        order by w.nrwersji
                into :ildosprz, :fromwersja;
                if (ildosprz is null ) then
                    ildosprz = 0;
    
                if (fromwersja is not null) then
                begin
                    --wpisuje na RW
                    execute procedure gen_ref('DOKUMPOZ') returning_values docpozRW;
                    insert into DOKUMPOZ(REF, DOKUMENT, KTM, ILOSC,  WERSJA, int_id)
                    values (:docpozRW, :docidRW, :ktm,minvalue(:ildosprz,:ildoprzepr), :fromwersja, :poz_int_id);
                    ildoprzepr = ildoprzepr - minvalue(:ildosprz,:ildoprzepr);
                end
    
            end

        end
        else if (mode = 0 and :ildoprzepr >0 ) then
        begin
            msg = 'Niewystarczajaca ilosc towaru do przeprogramowania';
            status = 0;
            suspend;
            EXIT;
        end



    end
    else
    begin
        msg = 'Towar dostepny na magazynie, przeprogramowanie zbedne.';
        status = 0;
        suspend;
        EXIT;
    end
    --akceptacja RW
            update dokumnag d set d.akcept=1 where d.ref = :docidRW;

            --naglowek PW
            execute procedure gen_ref('DOKUMNAG') returning_values docidPW;
            insert into DOKUMNAG(
                REF, MAGAZYN, TYP, DATA, DOSTAWA, DOSTAWCA,
                KLIENT, ZAMOWIENIE, AKCEPT, BLOKADA, MWSBLOCKMWSORDS, BLOCKMWSORDS,
                zrodlo, operator, operakcept, int_id, grupaprod,wydania)
            values (
                :docidPW, 'MWS', 'PW', current_date, null, null,
                null, null, 0, 0, 1,1,
                99,:aktuoperator, :aktuoperator, :dok_int_id, :dokpoz,0);
            --pozycje PW
            for 
                select  KTM, ILOSC, int_id
                    from dokumpoz dp
                    where dp.dokument = :docidRW
                    order by ref
            into :ktm, :tymil, :poz_int_id do
            begin
                execute procedure gen_ref('DOKUMPOZ') returning_values docpozPW;
                insert into DOKUMPOZ(REF, DOKUMENT, KTM, ILOSC,  WERSJA, int_id)
                values (:docpozPW, :docidPW, :ktm,:tymil, :celwersja, :poz_int_id);
            end
           --akceptacja PW
           update dokumnag d set d.akcept=1 where d.ref = :docidPW;
    status = 1;
    suspend;
end^
SET TERM ; ^
