--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SRV_REALIZE_ACK(
      SRVREQUEST integer,
      SRVHISTORY integer,
      WYDANIA integer)
  returns (
      STATUS integer,
      SYMBOLMAG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable ktm varchar(40);
declare variable cenamag numeric(14,4);
declare variable srvpos integer;
declare variable symbol varchar(80);
begin
  update DOKUMNAG set AKCEPT=1 where HISTORIA=:srvhistory and SRVREQUEST=:srvrequest;


  if(:wydania=1) then begin
    for
      select DOKUMPOZ.KTM, DOKUMPOZ.CENASR, DOKUMPOZ.FROMSRVPOSITION
      from DOKUMNAG join DOKUMPOZ on (DOKUMPOZ.DOKUMENT=DOKUMNAG.REF)
      where (DOKUMNAG.HISTORIA=:srvhistory and DOKUMNAG.SRVREQUEST=:srvrequest and DOKUMNAG.AKCEPT=1)
      order by DOKUMNAG.REF, DOKUMPOZ.REF
      into :ktm, :cenamag, :srvpos
    do begin
      if(:srvpos is not null) then begin
        update SRVPOSITIONS set PRICE=:cenamag where ref=:srvpos;
      end else begin
        update SRVPOSITIONS set PRICE=:cenamag
        where SRVHISTORY=:srvhistory and SRVREQUEST=:srvrequest and KTM=:ktm;
      end
    end
  end
  symbolmag='';
  for select SYMBOL from DOKUMNAG where HISTORIA=:srvhistory and SRVREQUEST=:srvrequest and wydania = :wydania
  into :symbol
  do begin
    if (symbolmag = '') then
      symbolmag = symbol;
    else
      symbolmag = ';' || symbol;
  end
  status=1;
end^
SET TERM ; ^
