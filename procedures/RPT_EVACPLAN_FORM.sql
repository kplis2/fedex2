--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_EVACPLAN_FORM(
      EMPLOYEE integer,
      CURRENTCOMPANY integer,
      VYEAR integer)
  returns (
      REF integer,
      FILENO varchar(20) CHARACTER SET UTF8                           ,
      PERSONNAMES varchar(120) CHARACTER SET UTF8                           ,
      BRANCH varchar(255) CHARACTER SET UTF8                           ,
      ACTLIMIT integer,
      PREVLIMIT integer,
      USEDLIMIT integer)
   as
begin

  if (employee = 0) then
    employee = null;

  for
    select E.ref, E.fileno, E.personnames, OD.nazwa
      from employees E
        left join oddzialy OD on (OD.oddzial = E.branch)
      where (E.ref = :employee or :employee is null) and E.company = :currentcompany
      order by E.personnames
      into :ref, :fileno, :personnames, :branch
  do begin

    actlimit = null;
    prevlimit = null;
    usedlimit = null;

    select actlimith, prevlimith, limitconsh
      from evaclimits
      where employee = :ref and vyear = :vyear
      into :actlimit, :prevlimit, :usedlimit;

    suspend;
  end
end^
SET TERM ; ^
