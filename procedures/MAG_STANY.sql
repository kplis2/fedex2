--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_STANY(
      MAGAZYN varchar(255) CHARACTER SET UTF8                           ,
      KTM varchar(80) CHARACTER SET UTF8                           ,
      WERSJA integer,
      CENA numeric(14,4),
      DOSTAWA integer,
      DATA timestamp,
      GROUPBYKTM smallint,
      ZEROWE smallint)
  returns (
      RKTM varchar(80) CHARACTER SET UTF8                           ,
      RWERSJA integer,
      RCENA numeric(14,4),
      RDOSTAWA integer,
      STAN numeric(14,4),
      WARTOSC numeric(14,4),
      RMAGAZYN varchar(255) CHARACTER SET UTF8                           )
   as
declare variable TYP char(1);
declare variable TYP1 char(1);
declare variable STMIN numeric(14,4);
declare variable WARTMIN numeric(14,4);
declare variable DATAL timestamp;
declare variable DATAM timestamp;
declare variable NUMERLAST integer;
declare variable SQL varchar(1024);
declare variable DEFMAGAZ varchar(3);
begin
  if(groupbyktm is null) then groupbyktm = 1;
  defmagaz = null;
  typ = null;
  select symbol from defmagaz where symbol = :magazyn into :defmagaz;
  /*sprawdzenie typw mag. gdy przekazano wiele magazynw ;MG1;MG2;...*/
  if(defmagaz is null) then
    if(magazyn like '%;%;%;%') then begin
      select max(typ) from defmagaz d where position(';'||d.symbol||';' in :magazyn) > 0 and d.typ = 'E' into :typ;
      select max(typ) from defmagaz d where position(';'||d.symbol||';' in :magazyn) > 0 and (d.typ = 'C' or d.typ = 'P') into :typ1;
      if(typ is not null and typ1 is not null and typ <> typ1) then exception MAGSTN_EXPT 'Różne typy magazynów. Sumowanie stanów niedozwolone';
      else if(typ is null) then typ = typ1;
    end else begin
      magazyn = replace(magazyn, ';', '');
  end
  if(typ is null) then select TYP from DEFMAGAZ where SYMBOL=:MAGAZYn into :typ;

  if(data is null ) then exit;
  if(:typ = 'E' or :typ = 'S') then begin
    /* okrelenie, co byo do tego momentu na plus */
    for select DOKUMPOZ.KTM, DOKUMPOZ.WERSJA , SUM(DOKUMROZ.ILOSC), sum(DOKUMROZ.WARTOSC), max(DOKUMNAG.DATA)
       from DOKUMROZ join DOKUMPOZ on ( DOKUMROZ.pozycja = DOKUMPOZ.ref)
                        join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.dokument)
                        join DEFDOKUM on ( DOKUMNAG.typ = DEFDOKUM.symbol)
       where DOKUMNAG.MAGAZYN = :MAGAZYN and (DOKUMPOZ.KTM = :ktm or (:ktm is null)) and (DOKUMPOZ.WERSJA = :wersja or (:wersja is null))
            and DOKUMNAG.DATA <= :DATA and strmulticmp(';'||DOKUMNAG.AKCEPT||';',';1;8;')>0 AND DEFDOKUM.wydania = 0
       group by DOKUMPOZ.KTM, DOKUMPOZ.WERSJA
       into :RKTM, :RWERSJA, :stan, :wartosc, :datal
    do begin
       /*okrelenie rozchodów*/
       stmin = null;
       datam = null;
       wartmin = null;
       select sum(DOKUMROZ.ilosc),sum(dokumroz.wartosc), max(dokumnag.data) from DOKUMROZ join DOKUMPOZ on ( DOKUMROZ.pozycja = DOKUMPOZ.ref)
                        join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.dokument)
                        join DEFDOKUM on ( DOKUMNAG.typ = DEFDOKUM.symbol)
       where DOKUMNAG.MAGAZYN = :MAGAZYN and (DOKUMPOZ.KTM = :rktm)  and (DOKUMPOZ.WERSJA = :rwersja)
                   and DOKUMNAG.DATA <= :DATA and strmulticmp(';'||DOKUMNAG.AKCEPT||';',';1;8;')>0 AND DEFDOKUM.wydania = 1
       into :stmin, :wartmin, :datam;
       if(:wartmin is null) then wartmin = 0;
       if(:stmin is null) then stmin = 0;
       stan = :stan - :stmin;
       wartosc = :wartosc - :wartmin;
       if(:datam > :datal) then datal = :datam;
       /* teraz tylko okrelić ilosc - cena z ostatniej rozpiski w tym okresie*/
       numerlast = null;
       if(:stan <> 0) then begin
         select max(DOKUMNAG.NUMER) from DOKUMROZ join DOKUMPOZ on ( DOKUMROZ.pozycja = DOKUMPOZ.ref)
                        join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.dokument)
           where DOKUMNAG.DATA =:datal and strmulticmp(';'||DOKUMNAG.AKCEPT||';',';1;8;')>0
             and DOKUMNAG.MAGAZYN = :MAGAZYN and DOKUMPOZ.KTM = :rktm  and DOKUMPOZ.WERSJA = :rwersja
           group by DOKUMNAG.MAGAZYN, DOKUMPOZ.KTM, DOKUMPOZ.WERSJA
         into :numerlast;
         rcena = null;
         select max(DOKUMROZ.cena) from DOKUMROZ join DOKUMPOZ on ( DOKUMROZ.pozycja = DOKUMPOZ.ref)
                         join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.dokument)
           where DOKUMNAG.DATA =:datal and (DOKUMNAG.NUMER = :numerlast or (:numerlast is null))and strmulticmp(';'||DOKUMNAG.AKCEPT||';',';1;8;')>0
            and DOKUMNAG.MAGAZYN = :MAGAZYN and DOKUMPOZ.KTM = :rktm  and DOKUMPOZ.WERSJA = :rwersja
           group by DOKUMNAG.MAGAZYN, DOKUMPOZ.KTM, DOKUMPOZ.WERSJA
           into :rcena;
         if(:rcena is null) then rcena = 0;
         suspend;
       end
    end
    exit;
  end
  if(:typ = 'C' or (:typ = 'P')) then begin
/*    for select DOKUMPOZ.KTM, DOKUMPOZ.WERSJA, DOKUMROZ.cena, DOKUMROZ.dostawa
        ,sum(DOKUMROZ.ilosc), sum(DOKUMROZ.WARTOSC), sum(DOKUMROZ.ILOSC * DEFDOKUM.WYDANIA), sum(DOKUMROZ.WARTOSC * DEFDOKUM.WYDANIA)
        from DOKUMROZ join DOKUMPOZ on( DOKUMROZ.pozycja = DOKUMPOZ.ref)
                         join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.dokument)
                         join DEFDOKUM on ( DOKUMNAG.typ = DEFDOKUM.symbol)
        where DOKUMNAG.MAGAZYN=:MAGAZYN and ((DOKUMPOZ.KTM = :KTM) or (:ktm is null)) and ((DOKUMPOZ.WERSJA = :wersja) or (:wersja is null))
              and (DOKUMNAG.AKCEPT = 1 or DOKUMNAG.akcept = 8) AND DOKUMNAG.data <= :DATA and (DOKUMROZ.cena = :cena or (:cena is null)) and (DOKUMROZ.DOSTAWA = :dostawa or (:dostawa is null))
        group by
           DOKUMPOZ.KTM, DOKUMPOZ.WERSJA, DOKUMROZ.CENA, DOKUMROZ.DOSTAWA
        into :rktm, :rwersja, :rcena, :rdostawa
             ,:stan, :wartosc, :stmin, :wartmin
*/
    if(groupbyktm = 1) then sql='select DOKUMPOZ.KTM, DOKUMPOZ.WERSJA, cast(null as numeric(14,4)), cast(null as integer), cast('''||:magazyn||''' as varchar(255))';
    else sql='select DOKUMPOZ.KTM, DOKUMPOZ.WERSJA, DOKUMROZ.cena, DOKUMROZ.dostawa, cast(dokumnag.magazyn as varchar(255))';
    sql=:sql||',sum(DOKUMROZ.ilosc), sum(DOKUMROZ.WARTOSC), sum(DOKUMROZ.ILOSC * DEFDOKUM.WYDANIA), sum(DOKUMROZ.WARTOSC * DEFDOKUM.WYDANIA)';
    if(:ktm is not null) then begin
      sql=:sql ||  ' from DOKUMPOZ join DOKUMROZ on( DOKUMROZ.pozycja = DOKUMPOZ.ref)';
      sql=:sql ||  ' join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.dokument ';


      sql=:sql ||  ') left join DEFDOKUM on ( DOKUMNAG.typ = DEFDOKUM.symbol)';
    end else begin
      sql=:sql ||  ' from DOKUMPOZ join DOKUMROZ on( DOKUMROZ.pozycja = DOKUMPOZ.ref)';
      sql=:sql ||  ' join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.dokument) left join DEFDOKUM on ( DOKUMNAG.typ = DEFDOKUM.symbol)';
    end
    sql=:sql||'where DOKUMROZ.REF is not null ';

    if(defmagaz is null and magazyn like '%;%;%;%') then begin
      sql = :sql || 'and position('';''||DOKUMNAG.MAGAZYN||'';'' in '''||:MAGAZYN||''')>0 ';
    end else begin
      sql = :sql || 'and DOKUMNAG.MAGAZYN='''||:MAGAZYN||''' ';
    end

    if(:ktm is not null) then sql=:sql||'and DOKUMPOZ.KTM = '''||:KTM||''' ';
    if(:wersja is not null) then sql=:sql||'and DOKUMPOZ.WERSJA = '||:wersja;
    if(:cena is not null) then sql=:sql||'and DOKUMROZ.CENA = '||:cena;
    if(:dostawa is not null) then sql=:sql||'and DOKUMROZ.DOSTAWA = 0'||:dostawa||' ';
    sql=:sql||' and strmulticmp('''||';'||'''||DOKUMNAG.AKCEPT||'''||';'||''','''||';1;8;'||''')>0';
    sql=:sql||' and DOKUMNAG.data <= '''||:DATA||''' ';
    if(groupbyktm = 1) then sql=:sql||' group by DOKUMPOZ.KTM, DOKUMPOZ.WERSJA';
    else sql=:sql||' group by DOKUMPOZ.KTM, DOKUMPOZ.WERSJA, DOKUMROZ.CENA, DOKUMROZ.DOSTAWA, DOKUMNAG.MAGAZYN';
    if (:zerowe = 0) then sql=:sql|| ' having sum(DOKUMROZ.ilosc) != 0';
    for execute statement :sql into :rktm, :rwersja, :rcena, :rdostawa, :rmagazyn, :stan, :wartosc, :stmin, :wartmin
    do begin
      if(:stan is null) then stan = 0;
      if(:wartosc is null) then wartosc = 0;
      if(:stmin is null) then stmin = 0;
      if(:wartmin is null) then wartmin = 0;
      stan = :stan - 2 * :stmin;
      wartosc = :wartosc - 2 * :wartmin;/* to nam daje po prostu rozchody */
      if(groupbyktm = 1 and :stan <> 0) then rcena = :wartosc / :stan;
      if(:stan <> 0 or :zerowe = 1) then suspend;
      rktm = null;
      rwersja = null;
      rcena = null;
      rdostawa = null;
      stan = null;
      wartosc = null;
      rmagazyn = null;
    end
  end
end^
SET TERM ; ^
