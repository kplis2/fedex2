--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE VAT_BY_PAYDAY(
      OTABLE varchar(30) CHARACTER SET UTF8                           ,
      OREF integer,
      DATADOK timestamp,
      TERMPLAT timestamp)
  returns (
      VATDATE timestamp)
   as
begin
/*PL: Procedura zwraca date vat jako date dokumentu przekazana
      w parametrze termplat(PR57428)
*/
    vatdate = termplat;
  suspend;
end^
SET TERM ; ^
