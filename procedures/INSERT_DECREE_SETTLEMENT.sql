--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INSERT_DECREE_SETTLEMENT(
      BKDOC integer,
      ACCOUNT ACCOUNT_ID,
      SIDE smallint,
      AMOUNT numeric(15,2),
      DESCRIPT varchar(80) CHARACTER SET UTF8                           ,
      SETTLEMENT varchar(20) CHARACTER SET UTF8                           ,
      STRANSDATE timestamp,
      FSOPERTYPE integer,
      SETTLCREATE smallint,
      OPENDATE timestamp,
      PAYDAY timestamp,
      SODDZIAL varchar(10) CHARACTER SET UTF8                           ,
      STABLE varchar(40) CHARACTER SET UTF8                           ,
      SREF integer,
      BANKACCOUNT integer,
      OPTIMALIZE smallint,
      OPTLAST smallint = 1,
      MATCHINGSYMBOL varchar(40) CHARACTER SET UTF8                            = null)
   as
declare variable DEBIT numeric(14,2);
declare variable CREDIT numeric(14,2);
declare variable DECREE integer;
declare variable NUMBER integer;
begin
  if (amount is not null and amount <> 0) then
  begin
    if (side = 0) then
    begin
      debit = amount;
      credit = 0;
    end else
    begin
     debit = 0;
     credit = amount;
    end
    if (account is null) then account = '';

    if (optimalize = 1) then
    begin
      decree = null;
      if (side = 0) then
      begin
        select max(ref) from decrees
          where bkdoc = :bkdoc and account = :account and debit <> 0
            and (settlement = :settlement or (settlement is null and :settlement is null))
            and descript = :descript
            and (matchingsymbol = :matchingsymbol or :matchingsymbol is null)
          into :decree;
      end else
      begin
        select max(ref) from decrees
          where bkdoc = :bkdoc and account = :account and credit <> 0
            and (settlement = :settlement or (settlement is null and :settlement is null))
            and descript = :descript
            and (matchingsymbol = :matchingsymbol or :matchingsymbol is null)
          into :decree;
      end
      if (decree is not null) then
      begin
        select max(number) from decrees where bkdoc = :bkdoc
          into :number;
        update decrees set debit = debit + :debit, credit = credit + :credit,
          number = iif(:optlast = 1, :number, number)
          where ref = :decree;
      end else
      begin
        insert into decrees (bkdoc, account, side, debit, credit, descript, settlement,
            stransdate, fsopertype, settlcreate, opendate, payday, soddzial, stable, sref, bankaccount, matchingsymbol)
          values (:bkdoc, :account, :side, :debit, :credit, :descript, :settlement,
            :stransdate, :fsopertype, :settlcreate, :opendate, :payday, :soddzial, :stable, :sref, :bankaccount, :matchingsymbol);
      end
    end else
    begin
      insert into decrees (bkdoc, account, side, debit, credit, descript, settlement,
          stransdate, fsopertype, settlcreate, opendate, payday, soddzial, stable, sref, bankaccount, matchingsymbol)
        values (:bkdoc, :account, :side, :debit, :credit, :descript, :settlement,
          :stransdate, :fsopertype, :settlcreate, :opendate, :payday, :soddzial, :stable, :sref, :bankaccount, :matchingsymbol);
    end
  end
end^
SET TERM ; ^
