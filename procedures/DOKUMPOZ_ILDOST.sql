--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMPOZ_ILDOST(
      I numeric(14,4),
      ZAMOWIENIE integer,
      MAG_TYP char(1) CHARACTER SET UTF8                           ,
      MAG varchar(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      LCENAMAG numeric(14,4),
      LDOSTAWA integer,
      DOSTAWA integer,
      POZDOKUM integer)
  returns (
      ILDOST numeric(14,4))
   as
declare variable previldost numeric(14,4);
declare variable ilzablokow numeric(14,4);
begin
          /*dodanie ilosci zarezerwowanych prze to zamowienie na tym stanie cenowym */
          previldost = null;
          if(:mag_typ = 'C') then
            select sum(ilosc) from STANYREZ where ZAMOWIENIE=:zamowienie and MAGAZYN=:mag and KTM=:ktm and wersja=:wersja and cena=:lcenamag and status='B' and zreal=0 and onstcen=1 into :previldost;
          else if(:mag_typ = 'P') then
            select sum(ilosc) from STANYREZ where ZAMOWIENIE=:zamowienie and MAGAZYN=:mag and KTM=:ktm and wersja=:wersja and ((cena=:lcenamag) or (cena = 0)) and dostawa=:ldostawa and status='B' and zreal=0 and onstcen=1 into :previldost;
          else
            select sum(ilosc) from STANYREZ where ZAMOWIENIE=:zamowienie and MAGAZYN=:mag and KTM=:ktm and wersja=:wersja and status='B' and zreal=0 and onstcen=1 into :previldost;
          if(:previldost is null) then previldost = 0;
          i = :i + :previldost;
          if(:mag_typ = 'P' and ((:dostawa is null) or (:dostawa = 0)) and :lcenamag > 0) then begin
            previldost = null;
            /* obsluga przypadkow blokad na ceny w magazynie typu P */
            /*ilosci wolne stanach z dostawami w danej cenie*/
            select sum(ilosc-zablokow) from stanycen where KTM=:ktm and WERSJA=:WERSJA and MAGAZYN=:mag and CENA=:lcenamag into :previldost;
            if(:previldost is null) then previldost = 0;
            /*blokady cenowe na dana cene, bez podanych dostaw, wylaczajac wlasne, ktore sie zwolnia*/
            ilzablokow = null;
            select sum(ilosc) from STANYREZ where MAGAZYN=:mag and KTM=:ktm and WERSJA=:wersja and status = 'B' and cena=:lcenamag and zreal = 0 and ((dostawa is null) or (dostawa = 0)) and (zamowienie <> :zamowienie) into :ilzablokow;
            if(:ilzablokow is null) then ilzablokow = 0;
            previldost = :previldost - :ilzablokow;
            if(:previldost < 0 and :ilzablokow > 0) then exception DOKUMPOZ_CHANGE_WRONG_BLOK;
            /*ilosci na rozpiskach danej pozycji magazynowej juz wygenerowanej w danej cenie
             oczekujace na zajecie pozycji na stanach - nie zaakceptowane */
            ilzablokow = null;
            select sum(ilosc) from DOKUMROZ where pozycja = :pozdokum and cena = :lcenamag and ack = 0 into :ilzablokow;
            if(:ilzablokow is null) then ilzablokow = 0;
            previldost = :previldost - :ilzablokow;
            if(:previldost < 0 and :ilzablokow > 0) then exception DOKUMPOZ_CHANGE_WRONG_BLOK;
            if(:previldost < :i) then i = :previldost;
          end
          if(:i < 0) then i = 0;
          ildost = :i;
end^
SET TERM ; ^
