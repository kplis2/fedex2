--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRNAGZAM_GEN_GROUPING(
      NAGZAM integer,
      POZZAM integer)
   as
declare variable prnagzam integer;
declare variable wersjaref integer;
declare variable prsheet integer;
declare variable termdost timestamp;
begin
  if (nagzam is not null and nagzam <> 0 and pozzam is not null and pozzam <> 0) then begin
    select wersjaref from pozzam where ref = :pozzam into :wersjaref;
    select max(ref) from nagzam n where n.kpopref = :pozzam and n.kwersjaref = :wersjaref into :prnagzam;
    select termdost from nagzam where ref = :nagzam into :termdost;
    select prsheet from nagzam where ref = :prnagzam into :prsheet;
    if(prsheet is not null and prsheet > 0) then
      execute procedure prnagzam_gen_grouping_down(:prsheet, :nagzam, :pozzam, :prnagzam, null);
  -- propagacja deklarowanego terminu realizacji zamówienia na zlecenia produkcyjneg
    if(termdost is not null ) then
      update nagzam set termdostdecl = :termdost where kpopzam = :nagzam and kpopprzam is null;
  end
end^
SET TERM ; ^
