--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_INPOST_SHIPMENT_RESPONSE(
      EDEDOCSH_REF integer)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
   as
declare variable shippingdoc integer;
declare variable shippingdocno varchar(40);
declare variable parent integer;
declare variable packageref integer;
declare variable packageno varchar(40);
declare variable cnt integer;
begin
  cnt = 0;
  otable = 'LISTYWYSD';


  select oref
    from ededocsh
    where ref = :ededocsh_ref
  into :shippingdoc;

  oref = :shippingdoc;

  for
    select p.id
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.name = 'pack'
    into :parent
  do begin
    select p.val
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'id'
    into :packageref;

    select p.val
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'packcode'
    into :packageno;

    if (:cnt = 0) then
      update listywysd
        set spedresponse = 1, symbolsped = :packageno, numerprzesylkistr = :packageno,
          x_anulowany = 0
        where ref = :shippingdoc;
    cnt = :cnt + 1;

    update listywysdroz_opk set symbolsped = :packageno
      where ref = :packageref;
  end

  suspend;
end^
SET TERM ; ^
