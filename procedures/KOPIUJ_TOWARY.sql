--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KOPIUJ_TOWARY(
      SOURCE_KTM varchar(40) CHARACTER SET UTF8                           ,
      DEST_KTM varchar(40) CHARACTER SET UTF8                           )
   as
declare variable cecha varchar(255);
declare variable wartosc varchar(255);
declare variable il integer;
declare variable wersjaref integer;
declare variable cena_zakn DECIMAL(14,2);
declare variable cena_zakb DECIMAL(14,2);
declare variable marzamin DECIMAL(14,2);
declare variable nazwa varchar(255);
declare variable opis varchar(1024);
declare variable symbol_dost varchar(255);
declare variable kodkresk varchar(255);
declare variable cennik integer;
declare variable akt smallint;
declare variable s_ref integer;
declare variable d_ref integer;
declare variable numer integer;
declare variable cnt integer;
declare variable nazwat varchar(255);
begin
  if(SOURCE_KTM <> DEST_KTM) then begin
     /* kopiowanie atrybutow */
/*     for select CECHA, WARTOSC, WERSJAREF from ATRYBUTY where KTM=:SOURCE_KTM and NRWERSJI=0 into :cecha, :wartosc, :wersjaref do begin
       select count(*) from ATRYBUTY where KTM=:DEST_KTM and NRWERSJI=0 and CECHA=:cecha into :il;
       if(il = 0 or (il is NULL)) then
         insert into ATRYBUTY(KTM,NRWERSJI,CECHA,WARTOSC, WERSJAREF) values(:DEST_KTM,0,:cecha,:wartosc,:wersjaref);
     end
     for select CENNIK, CENANET, CENABRU, CENAPNET, CENAPBRU, AKT, WERSJAREF from CENNIK  where KTM=:DEST_KTM AND WERSJA=0
       into :cennik, :cenanet, :cenabru, :cenapnet, :cenapbru, :akt, :wersjaref
     do begin
       select count(*) from CENNIK where KTM=:DEST_KTM and WERSJA=0 and CENNIK = :cennik into :il;
       if(il = 0 or (il is NULL)) then
         insert into CENNIK(CENNIK,KTM,WERSJA,CENANET, CENABRU, CENAPNET, CENAPBRU, AKT, WERSJAREF)
           values(:cennik,:DEST_KTM,0,:cenanet, :cenabru, :cenapnet, :cenapbru,:akt,:wersjaref);
     end*/

     for select NRWERSJI, REF, nazwa from WERSJE where KTM =:SOURCE_KTm
     order by NRWERSJI
     into :numer, :s_ref, :nazwat
     do begin
       d_ref = null;
       select ref from WERSJE where NRWERSJI = :numer and KTM = :DEST_KTM into :d_ref;
       if(:d_ref is null) then begin
         insert into WERSJE(KTM, NRWERSJI, NAZWA, OPIS, AKT, CENA_ZAKB, CENA_ZAKN,
             SYMBOL_DOST, KODKRESK, MARZAMIN, NIEOBROT)
           select :dest_ktm, :numer, NAZWA, OPIS, AKT, CENA_ZAKB, CENA_ZAKN, SYMBOL_DOST,KODKRESK,MARZAMIN,NIEOBROT
           from WERSJE where REF=:s_ref;


       end else begin
         select NAZWA, OPIS
         from WERSJE where REF=:s_ref
         into :nazwa, :opis;
         update WERSJE set
           NAZWA = :nazwa, opis = :opis
         where NRWERSJI = :numer and KTM = :DEST_KTM;
       end
       select ref from WERSJE where NRWERSJI = :numer and KTM = :DEST_KTM into :d_ref;
       execute procedure KOPIUJ_WERSJE(:s_ref, :d_ref);
     end
  end
end^
SET TERM ; ^
