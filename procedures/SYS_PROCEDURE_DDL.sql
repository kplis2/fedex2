--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_PROCEDURE_DDL(
      NAZWA varchar(50) CHARACTER SET UTF8                           ,
      CREATE_OR_ALTER smallint)
  returns (
      DDL blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           )
   as
declare variable coa      varchar(80);
declare variable source   blob sub_type 1        segment size 80;
declare variable params   blob sub_type 1        segment size 80;
declare variable descript blob sub_type 1        segment size 80;
declare variable transfer integer;
declare variable eol      varchar(2);
begin
 eol = '
';
  coa = 'SET TERM ^;'||:eol;
  coa = :coa || 'CREATE OR ALTER PROCEDURE '||:nazwa;
  select rdb$procedure_source from rdb$procedures
    where rdb$procedure_name = :nazwa
  into source;
  if(create_or_alter = 1) then
  begin
    source = :source || 'begin'||:eol||'  suspend;'||:eol||'end^'||:eol||'SET TERM ; ^';
    suspend;
  end
  execute procedure SYS_PROCEDURE_PARAMETERS(:nazwa) returning_values :params;
  ddl = :coa||:params||' as'||:eol||:source;
  ddl = trim(trailing
'
' from :ddl) || '^'||:eol||'SET TERM ; ^';
  suspend;
end^
SET TERM ; ^
