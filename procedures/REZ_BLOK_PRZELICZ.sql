--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REZ_BLOK_PRZELICZ(
      MAGAZYN char(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer)
   as
declare variable zam integer;
declare variable refpoz integer;
declare variable ilzab numeric(14,4);
declare variable cnt integer;
begin
-- procedura na nowo uklada kolejke blokad dla zadanych parametrow. Dotyka tylko Z i B
-- zamiana wszystkich B na Z
  for select ZAMOWIENIE, POZZAM, ILOSC
  from STANYREZ where MAGAZYN=:MAGAZYN AND KTM=:KTM AND WERSJA=:WERSJA AND STATUS='B' and ZREAL=0
  into :zam, :refpoz, :ilzab
  do begin
    cnt = null;
    select count(*) from STANYREZ where ZAMOWIENIE = :zam and POZZAM=:refpoz and STATUS='Z' and ZREAL=0 into :cnt;
    if(:cnt is null) then cnt = 0;
    if(:cnt > 0) then
      update STANYREZ set ILOSC=ILOSC+:ilzab where ZAMOWIENIE = :zam and POZZAM=:refpoz and STATUS='Z' and ZREAL=0;
    else
      insert into STANYREZ(POZZAM,STATUS,ZREAL,DOKUMMAG,MAGAZYN,KTM,WERSJA,ZAMOWIENIE,DATA,ILOSC,CENA,DOSTAWA,DATABL,ONSTCEN,PRIORYTET)
        select POZZAM,'Z',0,NULL,MAGAZYN,KTM,WERSJA,ZAMOWIENIE,DATA,:ilzab,CENA,DOSTAWA,DATABL,ONSTCEN,PRIORYTET
        from STANYREZ where POZZAM=:refpoz AND STATUS='B' AND ZREAL = 0;
    delete from STANYREZ where ZAMOWIENIE = :zam and POZZAM=:refpoz and STATUS='B' and ZREAL=0;
  end
-- zamiana Z na B, ale tylko tych ktore sie zalapia
  execute procedure REZ_BLOK_ROZPISZ(:magazyn, :ktm, :wersja);
end^
SET TERM ; ^
