--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_BOS(
      I_YEARID integer,
      CURRENCY varchar(3) CHARACTER SET UTF8                           ,
      COMPANY integer,
      ACCTYP smallint)
  returns (
      REF integer,
      SYMBOL varchar(3) CHARACTER SET UTF8                           ,
      ACCOUNT varchar(20) CHARACTER SET UTF8                           ,
      DESCRIPT varchar(80) CHARACTER SET UTF8                           ,
      DEBIT numeric(15,2),
      CREDIT numeric(15,2))
   as
declare variable old_symbol varchar(3);
declare variable old_account varchar(20);
declare variable s_account varchar(20);
declare variable n_debit numeric(15,2);
declare variable n_credit numeric(15,2);
declare variable old_prefix varchar(20);
declare variable period varchar(6);
begin
  ref = 0;
  old_symbol = '';
  old_account = '';
  old_prefix = '';
  period = cast(:i_yearid as varchar(4)) || '00';

  for select b.symbol, max(b.descript), sum(T.debit), sum(T.credit)
    from BKACCOUNTS B
      join accounting A on (A.bkaccount=B.ref and A.currency=:currency)
      join turnovers T on (T.accounting=A.ref and T.period=:period and (T.debit<>0 or T.credit<>0))
    where B.yearid=:i_yearid and B.company = :company
      and  ((:acctyp = 0 and B.bktype < 2) or (:acctyp = 1 and (B.bktype = 2 or B.bktype = 3)) or :acctyp = 2)
    group by b.symbol
    order by b.symbol
    into :symbol, :descript, :n_debit, :n_credit
  do begin
    debit = n_debit;
    credit = n_credit;
    account = s_account;
    old_symbol = symbol;
    old_account = account;
    ref = :ref + 1;
    suspend;
  end
end^
SET TERM ; ^
