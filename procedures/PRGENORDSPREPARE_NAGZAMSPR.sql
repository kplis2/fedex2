--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRGENORDSPREPARE_NAGZAMSPR(
      REJESTR varchar(3) CHARACTER SET UTF8                           ,
      TYPZAM varchar(3) CHARACTER SET UTF8                           ,
      DATAOD varchar(10) CHARACTER SET UTF8                           ,
      DATADO varchar(10) CHARACTER SET UTF8                           ,
      KLIENT integer,
      MASKA varchar(40) CHARACTER SET UTF8                           ,
      PRREJESTR varchar(3) CHARACTER SET UTF8                           ,
      BYPOZZAM smallint,
      ILWMS smallint,
      SHOWEMPTY smallint,
      AUTOSCHED smallint,
      PRDEPART PRDEPARTS_ID = null)
   as
declare variable sql varchar(4000);
declare variable wersjaref integer;
declare variable amount numeric(14,4);
declare variable descript varchar(1024);
declare variable nagzam integer;
declare variable pozzam integer;
declare variable prsheet integer;
declare variable amountinwh numeric(14,4);
declare variable amountinprord numeric(14,4);
declare variable amountinord numeric(14,4);
declare variable amountmin numeric(14,4);
declare variable amountmax numeric(14,4);
declare variable termdost timestamp;
declare variable actuprdepart prdeparts_id;
begin
  delete from PRGENORDSPREPARE;
  if(:showempty is null) then showempty = 0;
  if(:ilwms is null) then ilwms =0;
  if( not exists (select first 1 1 from prdeparts pd where pd.symbol = :prdepart))
    then prdepart = null;
  sql = 'select p.wersjaref, sum(p.ilosc - p.ilzreal),';
  if (:bypozzam = 1) then
    sql = sql ||' p.ref, n.ref, min(k.fskrot), min(n.termdostdecl)';
  else
    sql = sql ||'min(null), min(null), min(null), min(n.termdostdecl)';
  sql = sql||'    from nagzam n
        join pozzam p on (p.zamowienie = n.ref)
        left join klienci k on (k.ref = n.klient)
      where 1 = 1';
  if (coalesce(:rejestr,'') <> '') then
    sql = sql ||' and n.rejestr = '''||:rejestr||'''';
  if (coalesce(:typzam,'') <> '') then
    sql = sql ||' and n.typzam = '''||:typzam||'''';
  if (coalesce(:dataod, '') <> '' and coalesce(:datado, '') <> '' and dataod = datado) then
    sql = sql ||' and /*n.datazm*/ n.termdostdecl = '''||:dataod||'''';
  else begin
    if (coalesce(:dataod, '') <> '' ) then
      sql = sql ||' and /*n.datazm*/ n.termdostdecl >= '''||:dataod||'''';
    if (coalesce(:datado, '') <> '' ) then
      sql = sql ||' and /*n.datazm*/ n.termdostdecl <= '''||:datado||'''';
  end
  if (coalesce(:klient,0) > 0) then
    sql = sql ||' and n.klient = '||:klient;
  if (coalesce(:maska,'') <> '') then
    sql = sql ||' and p.ktm like '''||:maska||'%''';
  sql = sql ||' group by p.wersjaref ';
  if (:bypozzam = 1) then
    sql = sql ||', p.ref,  n.ref';
  sql = sql ||' having sum(p.ilosc - p.ilzreal) > 0';

  --insert into expimp (s71)values(:sql);
  for
    execute statement :sql
      into :wersjaref, :amountinord, :pozzam, :nagzam, :descript, :termdost
  do begin
  prsheet =null;
    select p.ref, p.prdepart
      from prsheets p
      where p.wersjaref = :wersjaref and p.status = 2
        and (:PRDEPART is null or p.prdepart = :PRDEPART)
      into :prsheet, :actuprdepart;

    if(ilwms = 0) then
    begin
      select sum(s.ilosc), sum(s.stanmin), sum(s.stanmax)
        from stanyil s
        where s.wersjaref = :wersjaref
        into :amountinwh, :amountmin, :amountmax;
    end
    else begin
      select sum(s.quantity - s.blocked + s.ordered)
        from mwsstock s
        where s.vers = :wersjaref
        into :amountinwh;

      select sum(s.stanmin), sum(s.stanmax)
        from stanyil s
        where s.wersjaref = :wersjaref
        into :amountmin, :amountmax;
    end
    amountmin = coalesce(amountmin,0);
    amountmax = coalesce(amountmax,0);
    amountinwh = coalesce(amountinwh,0);

    amount = amountinord - amountinwh;

    amountinprord = null;
    if (:prsheet is not null and (:amount < 0 or coalesce(:showempty,0) = 1)) then
      -- jezeli nie ma karty techn to nie ma zlecen czynnych lub ilosc na magazynie pokrywa ilosc na zamowieniach
      select sum(p.ilosc - p.ilzreal)
        from pozzam p
          left join nagzam n on (p.zamowienie = n.ref)
        where p.wersjaref = :wersjaref and p.out = 0
          and n.rejestr = :prrejestr
        into :amountinprord;
    amountinprord = coalesce(amountinprord,0);
    amount = amount - :amountinprord;
    if ((:amount > 0 or :showempty = 1) and (:PRDEPART is null or :prsheet is not null)) then
    begin
      if (coalesce(:amount,-1) < 0) then amount = 0;
      insert into PRGENORDSPREPARE(MODE, PRSHEET, AMOUNT, REJESTR, NAGZAM, POZZAM, PRNAGZAM, PRPOZZAM,
          OTABLE, OREF, WERSJAREF, DESCRIPT, PRSCHEDZAMDATE, AMOUNTINPRORD, AMOUNTINWH, AMOUNTINORD, AUTOSCHED,
          amountmin,  amountmax, termdost, prdepart)
        values(2, :prsheet, :amount, :prrejestr, :nagzam, :pozzam, null, null,
          null, null, :wersjaref, :descript, current_date, :amountinprord, :amountinwh, :amountinord, :AUTOSCHED,
          :amountmin, :amountmax, :termdost, :actuprdepart);
    end
  end
end^
SET TERM ; ^
