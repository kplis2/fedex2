--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_VAT_SPRUPR(
      PERIODID varchar(6) CHARACTER SET UTF8                           ,
      VREG varchar(10) CHARACTER SET UTF8                           ,
      ZAKRES smallint,
      ZPOPMIES smallint,
      TYP smallint,
      COMPANY integer)
  returns (
      REF integer,
      LP integer,
      VATREG varchar(10) CHARACTER SET UTF8                           ,
      VATREGNR integer,
      TRANSDATE timestamp,
      DOCDATE timestamp,
      DOCSYMBOL varchar(20) CHARACTER SET UTF8                           ,
      BKREG varchar(10) CHARACTER SET UTF8                           ,
      BKREGNR integer,
      KONTRAHKOD varchar(255) CHARACTER SET UTF8                           ,
      NIP varchar(15) CHARACTER SET UTF8                           ,
      VATGR varchar(5) CHARACTER SET UTF8                           ,
      NETV numeric(14,2),
      VATV numeric(14,2),
      BRUV numeric(14,2),
      NR integer,
      TOSUMMARY smallint)
   as
declare variable DOCREF integer;
declare variable SQL varchar(5048);
declare variable VATREGTYPE integer; /* <<PR73894: PL>> */
declare variable print integer;
begin
  -- TYP wczytywany z pola kombo w ktorym jest nastepujaca kolejnosc:
  -- 0 Całość|
  -- 1 Sprzedaż krajowa|
  -- 2 WDT|
  -- 3 Sprzedaż eksportowa|
  -- 4 WNT|
  -- 5 Usługi z importu
  if (typ = -1) then
    typ = 0; -- jak ktos wykasuje wpis w combo
  ref = 0;
  lp = 0;
  if (zakres is null) then zakres = 0;
  if (zpopmies is null) then zpopmies = 0;

--BS08473
  sql = 'select B.ref, B.transdate, B.docdate, B.vatreg, B.vnumber, B.bkreg, B.number, B.NIP, b.contractor, b.symbol, v.vatregtype'; --<<PR73894: PL>>
  sql = sql || ' from bkdocs B join VATREGS V on (B.vatreg = V.symbol and B.company = V.company) join BKDOCTYPES T on (B.doctype = t.ref)';
  sql = sql ||' where B.vatperiod = '''||:periodid||''' and b.status > 0';--and v.vtype in (0,1,2,5,6)';
--<<PR73894: PL
  sql = sql ||'  and (v.vatregtype in (0,2)
    or (V.VATREGTYPE = 3
         and exists(select first 1 1 from bkvatpos join grvat on (grvat.symbol = bkvatpos.taxgr) where bkvatpos.bkdoc = B.ref and grvat.vatregtype in (0,2))))';
  if(coalesce(typ,0)>0) then
  begin
    sql = sql ||' and v.vtype = '||
    case typ
      when 1 then '0' --Sprzedaż krajowa
      when 2 then '1' --WDT
      when 3 then '2' --Sprzedaż eksportowa
      when 4 then '5' --WNT
      when 5 then '6' --Usugi z importu
    end;
  end
-->>
  if(coalesce(zakres,0)=1)then sql = sql ||' and t.creditnote = 0';
  else if(coalesce(zakres,0)=2)then sql = sql ||' and t.creditnote = 1';
  if(coalesce(zpopmies,0)>0)then sql = sql ||' and (b.vatperiod <> b.period)';
  if(coalesce(vreg,'')<>'')then sql = sql ||' and b.vatreg = '''||:vreg||'''';
  sql = sql ||' and B.company ='||:company||' order by B.vatreg, b.vnumber';
  for execute statement :sql

  /*for
    select B.ref, B.transdate, B.docdate, B.vatreg, B.vnumber, B.bkreg, B.number,
        B.NIP, b.contractor, b.symbol
      from bkdocs B
        join VATREGS V on (B.vatreg = V.symbol and B.company = V.company)
        join BKDOCTYPES T on (B.doctype = t.ref)
      where B.vatperiod = :periodid and B.status > 1
        and v.vtype in (0,1,2,5,6) -- zeby napewno byla sprzedaz (należny)
        and (:typ = 0 or v.vtype = (:typ -1)) -- wszystkie albo wybrany rejestr
        and B.company = :company
        and ((:zakres = 0) or (:zakres = 1 and T.creditnote = 0) or (:zakres = 2 and T.creditnote = 1))
        and ((:zpopmies = 0) or (B.vatperiod <> B.period))
        and ((:vreg is null) or (:vreg = '') or (B.vatreg = :vreg))
      order by B.vatreg, b.vnumber */
      into :docref, :transdate, :docdate, :vatreg, :vatregnr, :bkreg, :bkregnr,
         :nip, :kontrahkod, :docsymbol, :vatregtype   --<<PR73894: PL>>
  do begin
    nr = 0;
    lp = lp + 1;
    print = 0;
    for
      select vatgr, sum(netv), sum(vatv)
        from BKVATPOS
          join GRVAT on (GRVAT.SYMBOL = BKVATPOS.TAXGR)--<<PR73894: PL>>
        where bkdoc = :docref
          --and (debittype = :vatdebittype or :vatdebittype = 4) ?? Nie bylo takiego warunku wczesniej?? Brak parametru w ogole...
          and (:vatregtype in (0,2) or (:vatregtype = 3 and (grvat.vatregtype in (0,2))))--<<PR73894: PL>>
      group by VATGR
        into :vatgr, :netv, :vatv
    do begin
      print = 1;
      bruv = netv + :vatv;
      ref = ref + 1;
      nr = nr + 1;
      tosummary = 1;
      suspend;
    end
    tosummary = 0;
    vatgr = null;
    netv = null;
    vatv = null;
    bruv = null;
    if (nr < 2) then
    begin
      nr = nr + 1;
      ref = ref + 1;
      if (print = 1) then
        suspend;
    end
  end
end^
SET TERM ; ^
