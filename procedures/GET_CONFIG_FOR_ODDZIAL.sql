--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_CONFIG_FOR_ODDZIAL(
      SYMBOL varchar(255) CHARACTER SET UTF8                           ,
      ODDZIAL ODDZIAL_ID,
      FROMDATE varchar(20) CHARACTER SET UTF8                            = null)
  returns (
      CVALUE varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable MULTI smallint;
declare variable HIST smallint;
declare variable VAL1 varchar(1024);
declare variable TYP smallint;
declare variable Y varchar(10);
declare variable M varchar(10);
declare variable D varchar(10);
declare variable SYMBOL2 varchar(20);
begin
  -- DU: procedura wyciągająca wartosc z KONFIG'a

  select akronim, multi, wartosc, typ, historia
    from konfig where akronim = :symbol
    into :symbol2, :multi, :cvalue, :typ, :hist;

  if (cvalue is null) then
      cvalue = '';

  if (multi > 0) then
  begin

    if (:hist = 1) then
    begin
      if (:fromdate is null or :fromdate = '') then
        fromdate = current_date;
      select first 1 wartosc
        from konfigvals k
          where k.akronim = :symbol and k.oddzial = :oddzial and k.fromdate <= :fromdate
          order by k.fromdate desc
        into :val1;
    end
    else
    begin
      select wartosc
        from konfigvals
          where akronim = :symbol and oddzial = :oddzial
      into :val1;
    end

    if (val1 is not null) then
      cvalue = val1;
  end


  if (cvalue is null) then cvalue = '';

  if (:typ=3) then begin --jesli typ jest data, to ewentualna konwersja na rrrr-mm-dd
    if (cvalue like '__-__-__') then
      cvalue =  '20'||cvalue;
    else if (cvalue like '__-__-____') then
      cvalue = substring(cvalue from 7 for 4)||substring(cvalue from 3 for 4)||substring(cvalue from 1 for 2);
  end

  suspend;
end^
SET TERM ; ^
