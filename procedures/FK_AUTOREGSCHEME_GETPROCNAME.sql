--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_AUTOREGSCHEME_GETPROCNAME(
      REF integer)
  returns (
      PROCNAME varchar(255) CHARACTER SET UTF8                           )
   as
begin
  if (:ref > 0) then
    begin
      procname = 'X_AUTODECREE_' || ref;
      if (not exists(select first 1 1 from RDB$PROCEDURES where RDB$PROCEDURE_NAME=:procname)) then
      begin
        procname = 'AUTODECREE_' || ref;
      end
      suspend;
    end
end^
SET TERM ; ^
