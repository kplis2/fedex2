--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RIGHT_ANALIZY(
      OPERATOR integer = null)
  returns (
      OPER varchar(80) CHARACTER SET UTF8                           ,
      ZB_DANYCH varchar(255) CHARACTER SET UTF8                           ,
      EL_ZB varchar(255) CHARACTER SET UTF8                           )
   as
declare variable grupa varchar(255);
declare variable oper_ref integer;
declare variable o_ref integer;
declare variable o_grupa varchar(80);
begin
--------------Analizy------------

   for select distinct o.nazwa,'Analiza', z.nazwa
      from operator o left join zapytania z on ((strmulticmp(';'||o.ref||';',Z.rights)>0))
      where  (o.ref=:operator or :operator is null or :operator = 0)   --right
      union
      select distinct o2.nazwa,'Analiza', z2.nazwa
        from operator o2
        left join opergrupy op on ((strmulticmp(';'||OP.symbol||';',o2.grupa)>0))             --rightsgroup
        left join zapytania z2 on ((strmulticmp(';'||OP.symbol||';',Z2.rightsgroup)>0))
      where (o2.ref=:operator or :operator is null or :operator = 0)
      into :oper, :zb_danych,:el_zb
    do begin
      if (coalesce(:el_zb,'')<>'') then
      suspend;
    end

    for select o.nazwa,'Analiza' from operator o where o.aktywny = 1  and (o.ref=:operator or :operator is null or :operator = 0)
    into :oper, :zb_danych   --zapytania, do których wszyscy maja dostp
      do begin
        for select z3.nazwa
          from zapytania z3
          where coalesce(z3.rights,'')='' and coalesce(z3.rightsgroup,'')=''
        into :el_zb
        do begin
          suspend;
        end
      end
----------








---------------Konta ksiegowe-----------
  /*  for select distinct o.nazwa,'Konta ksiegowe', z.descript
      from operator o left join accounting z on ((strmulticmp(';'||o.ref||';',Z.rights)>0))
      union
      select distinct o.nazwa,'Konta ksiegowe', z2.descript
        from operator o
        left join opergrupy op on ((strmulticmp(';'||OP.symbol||';',o.grupa)>0))
        left join accounting z2 on ((strmulticmp(';'||OP.symbol||';',Z2.rightsgroup)>0))
      into :oper, :zb_danych,:el_zb
    do begin
      if (coalesce(:el_zb,'')<>'') then
      suspend;
    end
    --itd

    ACCOUNTING
BKACCOUNTS
BKDOCS
BKREGS
BTRANSFERS
BTRANTYPE
CRMDRZEWO
DOKPLIK
DOKPLIKTYP
FRDCELLS
FRDHDRS
FRDIMELEMS
FSCLRACCH
OLAPCUBES
QUERYTYPES
ROZRACH
SEGREGATOR
SLOPOZ
SRVOPERATIONS
S_NAVIGATOR
TOWTYPES
ZAPYTANIA


    */

end^
SET TERM ; ^
