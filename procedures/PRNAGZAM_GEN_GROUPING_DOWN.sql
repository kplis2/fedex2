--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRNAGZAM_GEN_GROUPING_DOWN(
      PRSHEET integer,
      NAGZAM integer,
      POZZAM integer,
      PRNAGZAM integer,
      PRPOZZAM integer)
   as
declare variable wersjaref integer;
declare variable prnagzamdown integer;
declare variable lsheet integer;
declare variable symbol varchar(20);
begin
  for select pm.subsheet, pm.wersjaref from prshmat pm
  where pm.sheet = :prsheet and pm.subsheet is not null and pm.subsheet <> 0
   into :lsheet, :wersjaref
  do begin
    prnagzamdown = null;
    select max(n.ref) from nagzam n where n.kpopref = :pozzam and n.prsheet = :lsheet into :prnagzamdown;
    select max(symbol) from prsheets where ref = :lsheet into :symbol;
    if(prnagzamdown is null or prnagzamdown = 0) then exception prsheets_gennagzam 'Brak zlecenia dla karty '||:lsheet;
    update nagzam set kpopprzam = :prnagzam where ref = :prnagzamdown;
    execute procedure PRNAGZAM_GEN_GROUPING_down(:lsheet, :nagzam, :pozzam, :prnagzamdown, null);
  end
end^
SET TERM ; ^
