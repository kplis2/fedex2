--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_DATA_VAT27_2_DPOZ(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      COMPANY COMPANIES_ID,
      POZROWMIN SMALLINT_ID = 0,
      TESTPOZROWCOUNT SMALLINT_ID = 0)
  returns (
      NUMER INTEGER_ID,
      A SMALLINT_ID,
      B STRING255,
      C varchar(15) CHARACTER SET UTF8                           ,
      D numeric(14,2))
   as
begin
/*TS: FK - pobiera dane dla wydruku.

  > POZROWMIN - procedura zwroci nie mniej niz dana liczbe wierszy.
  > TESTPOZROWCOUNT - liczba wierszy z testowymi danymi, ktore zostana
    wygenerowane dla wskazanej sekcji dokumentu i zwrocone razem z
    danymi zwroconymi z zapytania.
*/

  numer = 0;

  --D. INFORMACJA O SWIADCZONYCH USLUGACH nie jest obslugiwana, dotyczy gazów cieplarnianych, a takich klientów nie mamy 25-06-2015

  --/*
  --Dane testowe
  while(numer < testpozrowcount) do
  begin
    a = mod(rand(),2);
    b = 'Nazwa nabywcy: '||:numer;
    c = 1111111111 + :numer;
    d = 100.01 * :numer;
    numer = numer + 1;
    suspend;
  end
  --*/

  a = 0;
  b = '';
  c = '';
  d = 0.00;

  while(numer < :pozrowmin) do
  begin
    numer = numer + 1;
    suspend;
  end
end^
SET TERM ; ^
