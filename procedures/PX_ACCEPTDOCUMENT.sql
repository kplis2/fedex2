--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PX_ACCEPTDOCUMENT(
      DOCREF integer,
      OPER varchar(60) CHARACTER SET UTF8                           )
   as
declare variable currentoper integer;
begin

    select o.ref from operator  o   -- wbor ref operatora na podstawie nazwy oper
    where o.nazwa=:oper
    into :currentoper;

    if( not exists (select first 1 g.connectionid from globalparams g
               where g.connectionid=current_connection and g.psymbol='AKTUOPERATOR' )) then
        begin

          INSERT INTO GLOBALPARAMS (REGDATE, PSYMBOL, PVALUE) VALUES (current_timestamp(0), 'AKTUOPERATOR', :currentoper);

        end


        --UPDATE DOKUMNAG
     --   SET  DOSTAWCA = 1520
      --  WHERE (REF = :docref);


       UPDATE DOKUMNAG
        SET  AKCEPT = 1,  OPERATOR = :currentoper,  OPERAKCEPT = :currentoper,   ACCEPTOPER = :currentoper
        WHERE (REF = :docref);


end^
SET TERM ; ^
