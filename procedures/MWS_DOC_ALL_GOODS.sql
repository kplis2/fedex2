--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_DOC_ALL_GOODS(
      DOCID integer,
      DOCTYPE char(1) CHARACTER SET UTF8                           ,
      PALINCLUDE smallint,
      VERSIN integer)
  returns (
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      DOCPOSID DOCPOS_ID)
   as
declare variable mwsord integer;
declare variable pal smallint;
declare variable wydania smallint;
begin
  if(:doctype = 'M') then
  begin
    select d.wydania
      from dokumnag d
      where d.ref = :docid
      into :wydania;
  
    if (palinclude is null) then palinclude = 0;
    -- najpierw z dokumentu magazynowego
    for
      select distinct p.ref, p.ktm, p.wersjaref, coalesce(t.paleta,0)
        from dokumpoz p
          left join towary t on (t.ktm = p.ktm)
        where p.dokument = :docid
          and (p.wersjaref = :versin or :versin is null)
        into docposid, good, vers, pal
    do begin
      if (palinclude = 1 and pal >=0) then
        suspend;
      else if (palinclude = 0 and pal = 0) then
        suspend;
    end
  end
  -- potem ze zlecenia
  -- zlecenia wydania
  if(:wydania = 1) then
  begin
   for
      select distinct a.docposid, a.good, a.vers, a.mwsord, coalesce(t.paleta,0)
        from mwsacts a
          left join towary t on (t.ktm = a.good)
        where a.docid = :docid
          and a.doctype = :doctype and a.status = 5
          and a.mwsconstlocl is null and a.mwspallocl is null
          and (a.vers = :versin or :versin is null)
        into docposid, good, vers, mwsord, pal
    do begin
      if (palinclude = 1 and pal >=0) then
        suspend;
      else if (palinclude = 0 and pal = 0) then
        suspend;
    end
  end
  -- zlecenia przyjecia
  else
  begin
    for
      select distinct a.docposid, a.good, a.vers, a.mwsord, coalesce(t.paleta,0)
        from mwsacts a
          left join towary t on (t.ktm = a.good)
        where a.docid = :docid
          and a.doctype = :doctype and a.status = 5
          and a.mwsconstlocp is null and a.mwspallocp is null
          and (a.vers = :versin or :versin is null)
        into docposid, good, vers, mwsord, pal
    do begin
      if (palinclude = 1 and pal >=0) then
        suspend;
      else if (palinclude = 0 and pal = 0) then
        suspend;
    end
  end
end^
SET TERM ; ^
