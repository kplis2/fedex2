--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NOTIFY_INNE
  returns (
      ID varchar(20) CHARACTER SET UTF8                           ,
      PARENTID varchar(20) CHARACTER SET UTF8                           ,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           ,
      NUMVAL numeric(14,2),
      WATCHBELOW smallint,
      COLORINDEX integer,
      PRIORITY integer,
      ICON varchar(255) CHARACTER SET UTF8                           ,
      MODULE varchar(255) CHARACTER SET UTF8                           ,
      ACTIONID varchar(255) CHARACTER SET UTF8                           ,
      ACTIONPARAMS varchar(255) CHARACTER SET UTF8                           ,
      ACTIONNAME varchar(255) CHARACTER SET UTF8                           ,
      RIGHTSGROUP varchar(255) CHARACTER SET UTF8                           ,
      RIGHTS varchar(255) CHARACTER SET UTF8                           ,
      COMPANY integer)
   as
declare variable slodef integer;
declare variable slopoz integer;
declare variable dni integer;
declare variable planpayday timestamp;
declare variable pkosoba integer;
declare variable cpodmiot integer;
declare variable telefon varchar(255);
declare variable komorka varchar(255);
declare variable fax varchar(255);
declare variable email varchar(255);
declare variable temp_komorka varchar(255);
declare variable temp_telefon varchar(255);
declare variable temp_fax varchar(255);
declare variable temp_email varchar(255);
declare variable nazwa_tabeli varchar(20);
declare variable status smallint;
declare variable cnt integer;
begin
  -- klienci przeznaczeni do poinformowania mailowego
  status = 0;
  watchbelow = 0;
  colorindex = 2;
  priority = 1;
  icon = 'MI_USERS';
  module = 'EFK';
  rightsgroup = '';
  rights = '';
  for select ref
  from companies
  into :company
  do begin
  cnt = 0;
    for select slodef, slopoz, max(slonazwa), max(dni), min(planpayday)
      from ROZRACH
      where SALDOWM>0 and DNI>0 and TYP='N' and PLANPAYDAY<current_date
        and company = :company and slodef>0 and slopoz>0
      group by slodef,slopoz,company
      into :slodef, :slopoz, :descript, :dni, :planpayday
    do begin
      cpodmiot = NULL;
      pkosoba = NULL;
      telefon = 0;
      fax = 0;
      email = 0;
      komorka = 0;
      select first 1 ref, telefon, email, fax, komorka
        from cpodmioty
        where slodef=:slodef
          and slopoz=:slopoz
        into :cpodmiot, :temp_telefon, :temp_email, :temp_fax, :temp_komorka;
      if(:cpodmiot is not null) then begin
        select first 1 ref, telefon, email, fax,komorka
          from PKOSOBY
          where cpodmiot = :cpodmiot
            and windykacja = 1
          into :pkosoba, :telefon, :email, :fax, :komorka;
          if(:email is not null and :email <> '') then
            status = 1;
          else
            status = 0;
        if (:pkosoba is null) then begin
          telefon = temp_telefon;
          email = temp_email;
          fax = temp_fax;
          komorka = temp_komorka;
          status = 0;
        end
      end
        if (:status = 0) then begin
          id = 'KONTAKTINNE'||:slodef||:slopoz;
          parentid = 'KONTAKTINNE';
          val = :dni;
          numval = :dni;
          cnt = cnt + 1;
          suspend;
        end
    end
    -- naglowek drzewa
    if(:cnt>0) then begin
      descript = 'Klienci do rozpatrzenia.';
      id = 'KONTAKTINNE';
      parentid = '';
      icon = 'MI_KONTAKTY';
      --val = :maxval;
      --numval = :maxval;
      actionid = 'BrowseFkRozrach2';
      actionparams = '';
      actionname = 'Rozrachunki wg kontrahentów';
      suspend;
    end
  end
end^
SET TERM ; ^
