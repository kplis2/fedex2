--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INSERT_DECREE_CURRENCY(
      BKDOC integer,
      ACCOUNT ACCOUNT_ID,
      SIDE smallint,
      AMOUNT numeric(15,2),
      CURRAMOUNT numeric(15,2),
      RATE numeric(15,4),
      CURRENCY varchar(3) CHARACTER SET UTF8                           ,
      DESCRIPT varchar(80) CHARACTER SET UTF8                           ,
      OPTIMALIZE smallint,
      OPTLAST smallint = 1,
      MATCHINGSYMBOL varchar(40) CHARACTER SET UTF8                            = null)
   as
declare variable DEBIT numeric(14,2);
declare variable CREDIT numeric(14,2);
declare variable CURRDEBIT numeric(14,2);
declare variable CURRCREDIT numeric(14,2);
declare variable DECREE integer;
declare variable NUMBER integer;
begin
  if (amount is not null and amount <> 0) then
  begin
    if (side = 0) then
    begin
      debit = amount;
      credit = 0;
      currdebit = curramount;
      currcredit = 0;
    end else
    begin
     debit = 0;
     credit = amount;
     currdebit = 0;
     currcredit = curramount;
    end
    if (account is null) then account = '';

    if (optimalize = 1) then
    begin
      decree = null;
      if (side = 0) then
      begin
        select max(ref) from decrees
          where bkdoc = :bkdoc and account = :account and debit <> 0
            and currency = :currency and rate = :rate
            and descript = :descript
            and (matchingsymbol = :matchingsymbol or :matchingsymbol is null)
          into :decree;
      end else
      begin
        select max(ref) from decrees
          where bkdoc = :bkdoc and account = :account and credit <> 0
            and currency = :currency and rate = :rate
            and descript = :descript
            and (matchingsymbol = :matchingsymbol or :matchingsymbol is null)
          into :decree;
      end
      if (decree is not null) then
      begin
        select max(number) from decrees where bkdoc = :bkdoc
          into :number;
        update decrees set debit = debit + :debit, credit = credit + :credit,
            currdebit = currdebit + :currdebit, currcredit = currcredit + :currcredit,
            number = iif(:optlast = 1, :number, number)
          where ref = :decree;
      end else
      begin
        insert into decrees (bkdoc, account, side, debit, credit, currency,
            currdebit, currcredit, rate, descript, matchingsymbol)
          values (:bkdoc, :account, :side, :debit, :credit, :currency, 
            :currdebit, :currcredit, :rate, :descript, :matchingsymbol);
      end
    end else
    begin
        insert into decrees (bkdoc, account, side, debit, credit, currency,
            currdebit, currcredit, rate, descript, matchingsymbol)
          values (:bkdoc, :account, :side, :debit, :credit, :currency, 
            :currdebit, :currcredit, :rate, :descript, :matchingsymbol);
    end
  end
end^
SET TERM ; ^
