--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_SALDOBMMA(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      ACCOUNTS ACCOUNT_ID)
  returns (
      AMOUNT numeric(15,2))
   as
declare variable COMPANY integer;
declare variable YEARID integer;
declare variable DEBIT numeric(15,2);
declare variable CREDIT numeric(15,2);
declare variable ACCOUNTING_REF integer;
declare variable SALDO_CREDIT numeric(14,2);
declare variable COUNTRY_CURRENCY varchar(3);
declare variable BALANCETYPE smallint;
declare variable FRVHDR integer;
declare variable FRPSN integer;
declare variable FRCOL integer;
declare variable CACHEPARAMS varchar(255);
declare variable DDPARAMS varchar(255);
declare variable FRVERSION integer;
declare variable SACCOUNT ACCOUNT_ID;
declare variable FRHDR varchar(20);
declare variable FRVTYPE smallint;
declare variable PSYMBOL varchar(20);
begin
  if (frvpsn = 0) then exit;
  select frvhdr, frpsn, frcol, symbol
    from frvpsns
    where ref = :frvpsn
    into :frvhdr, :frpsn, :frcol, :psymbol;

  select company, frversion, frhdr, frvtype
    from frvhdrs
    where ref = :frvhdr
    into :company, :frversion, :frhdr, :frvtype;
  cacheparams = period||';'||accounts||';'||company;
  ddparams = accounts;

  if ((not exists (select ref from frvdrilldown where functionname = 'SALDOBMMA' and cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr)) or (frvtype = 1))  then
  begin
    select yearid from bkperiods where id = :period and company = :company
      into :yearid;
    accounts = replace(accounts,  '?',  '_') || '%';
    amount = 0;
    execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
      returning_values country_currency;

    for
      select A.ref, B.saldotype, A.account
        from accounting A
          join bkaccounts B on (A.bkaccount = B.ref)
        where A.currency = :country_currency and A.yearid = :yearid
          and A.account like :accounts
        into :accounting_ref, :balancetype, :saccount
    do begin
      insert into frchkaccounts (account, proced, frhdr, frpsn, period, psymbol)
        values (:saccount, 'SALDOBMMA', :frhdr, :frpsn, :period, :psymbol);
      select sum(debit), sum(credit) from turnovers
        where accounting = :accounting_ref and period = :period and company = :company
        into :debit, :credit;

      if (debit is null) then
        debit = 0;
      if (credit is null) then
        credit = 0;

      saldo_credit = 0;
      if (balancetype = 2) then
        saldo_credit = credit - debit;
      else if (balancetype = 1 and abs(credit) > abs(debit)) then
        saldo_credit = credit - debit;

      amount = amount + saldo_credit;
    end
  end else
    select first 1 fvalue from frvdrilldown
      where functionname = 'SALDOBMMA' and cacheparams = :cacheparams
        and frversion = :frversion and frvhdr = :frvhdr
      into :amount;

  amount = coalesce(amount,0);
  insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
     values (:frvhdr, :frpsn, :frcol, 'SALDOBMMA', :cacheparams, :ddparams, :amount, :frversion);
  suspend;
end^
SET TERM ; ^
