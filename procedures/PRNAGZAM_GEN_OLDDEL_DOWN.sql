--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRNAGZAM_GEN_OLDDEL_DOWN(
      PRNAGZAM integer)
  returns (
      REF integer)
   as
DECLARE VARIABLE CURREF INTEGER;
begin
  for select ref from nagzam where kpopprzam = :prnagzam into :curref
  do begin
    ref = curref;
    delete from nagzam where ref = :ref;
    execute procedure prnagzam_gen_olddel_down(:curref) returning_values :ref;
  end
end^
SET TERM ; ^
