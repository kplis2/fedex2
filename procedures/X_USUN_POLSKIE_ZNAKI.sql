--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_USUN_POLSKIE_ZNAKI(
      TEKST varchar(255) CHARACTER SET UTF8                           ,
      TRYB smallint)
  returns (
      NTEKST varchar(255) CHARACTER SET UTF8                           )
   as
begin
  ntekst = tekst;
  if (:tryb = 0 or :tryb = 1) then begin
    ntekst = replace(ntekst,  'ą',  'a');
    ntekst = replace(ntekst,  'Ą',  'A');
    ntekst = replace(ntekst,  'ć',  'c');
    ntekst = replace(ntekst,  'Ć',  'C');
    ntekst = replace(ntekst,  'ę',  'e');
    ntekst = replace(ntekst,  'Ę',  'E');
    ntekst = replace(ntekst,  'ł',  'l');
    ntekst = replace(ntekst,  'Ł',  'L');
    ntekst = replace(ntekst,  'ń',  'n');
    ntekst = replace(ntekst,  'Ń',  'N');
    ntekst = replace(ntekst,  'ó',  'o');
    ntekst = replace(ntekst,  'Ó',  'O');
    ntekst = replace(ntekst,  'ś',  's');
    ntekst = replace(ntekst,  'Ś',  'S');
    ntekst = replace(ntekst,  'ż',  'z');
    ntekst = replace(ntekst,  'Ż',  'Z');
    ntekst = replace(ntekst,  'ź',  'z');
    ntekst = replace(ntekst,  'Ź',  'Z');
  end

  if (:tryb = 0 or :tryb = 2) then begin
    ntekst = replace(:ntekst,  '!',  '');
    ntekst = replace(:ntekst,  '@',  '');
    ntekst = replace(:ntekst,  '#',  '');
    ntekst = replace(:ntekst,  '$',  '');
    ntekst = replace(:ntekst,  '%',  '');
    ntekst = replace(:ntekst,  '^',  '');
    ntekst = replace(:ntekst,  '&',  '');
    ntekst = replace(:ntekst,  '*',  '');
    ntekst = replace(:ntekst, '(', '');
    ntekst = replace(:ntekst,  ')',  '');
  end

  if (:tryb = 0 or :tryb = 3) then begin
    ntekst = replace(:ntekst,  '-',  '');
    ntekst = replace(:ntekst,  '_',  '');
    ntekst = replace(:ntekst,  '=',  '');
    ntekst = replace(:ntekst,  '+',  '');
    ntekst = replace(:ntekst,  '[',  '');
    ntekst = replace(:ntekst,  '{',  '');
    ntekst = replace(:ntekst,  ']',  '');
    ntekst = replace(:ntekst,  '}',  '');
    ntekst = replace(:ntekst,  '\',  '');
    ntekst = replace(:ntekst,  '|',  '');
    ntekst = replace(:ntekst,  ';',  '');
    ntekst = replace(:ntekst,  ':',  '');
    ntekst = replace(:ntekst,  '''',  '');
    ntekst = replace(:ntekst,  '"',  '');
    ntekst = replace(:ntekst, ',', '');
    ntekst = replace(:ntekst,  '<',  '');
    ntekst = replace(:ntekst,  '.',  '');
    ntekst = replace(:ntekst,  '>',  '');
    ntekst = replace(:ntekst,  '/',  '');
    ntekst = replace(:ntekst,  '?',  '');
  end
  suspend;
end^
SET TERM ; ^
