--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_DELETE_OPAK_SPEEDPAK(
      LISTWYSD LISTYWYSD_ID,
      TYPOPK INTEGER_ID)
   as
begin
  if (listwysd is not null and typopk is not null) then
  begin
  delete from listywysdroz_opk lo
    where   lo.listwysd = :listwysd and
            lo.typopk = :typopk;
  end
end^
SET TERM ; ^
