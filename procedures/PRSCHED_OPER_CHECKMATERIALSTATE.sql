--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_OPER_CHECKMATERIALSTATE(
      SCHEDOPER integer)
   as
DECLARE VARIABLE STARTTIME TIMESTAMP;
DECLARE VARIABLE SCHEDULE INTEGER;
DECLARE VARIABLE ZAM INTEGER;
DECLARE VARIABLE SCHEDZAM INTEGER;
DECLARE VARIABLE GUIDE INTEGER;
DECLARE VARIABLE MAG VARCHAR(3);
DECLARE VARIABLE KTM VARCHAR(40);
DECLARE VARIABLE WERSJAREF INTEGER;
DECLARE VARIABLE MATREADYTIME TIMESTAMP;
DECLARE VARIABLE MATSTATUS INTEGER;
DECLARE VARIABLE MAINSCHEDULE INTEGER;
DECLARE VARIABLE WERSJA INTEGER;
DECLARE VARIABLE MAGAZYN VARCHAR(3);
DECLARE VARIABLE GUIDEPOS INTEGER;
DECLARE VARIABLE ILOSC NUMERIC(14,4);
DECLARE VARIABLE ILBLOCK NUMERIC(14,4);
declare variable REZILOSC numeric(14,4);
declare variable rezstatus char(1);
declare variable rezdata timestamp;
declare variable techjedn double precision;
declare variable stechjedn varchar(200);
declare variable shoper integer;
declare variable worktime double precision;
declare variable prdepart varchar(20);
declare variable matprdepart varchar(20);
declare variable preptechtime numeric(14,4);
declare variable dodmatused smallint;
declare variable endtime timestamp;
declare variable ilafterstart numeric(14,4);
begin
  dodmatused = 0;--znacznik, czy byly w trakcie robione zalozenie, ze cos zostanie doprodukowane - brak zabezpieczenia w planach produkcyjnych
  matstatus = 0;
  select PRSCHEDOPERS.schedule, prschedopers.starttime, PRSCHEDOPERS.schedzam, PRSCHEDOPERS.guide,
    prschedules.main, prschedopers.zamowienie,   prschedopers.shoper, prschedules.prdepart
  from PRSCHEDOPERS join prschedules on (PRSCHEDULES.REF = PRSCHEDOPERS.schedule)
  where PRSCHEDOPERS.REF = :schedoper
  into :schedule,  :starttime, :schedzam, :guide, :mainschedule, :zam, :shoper, :prdepart;
--  if(:matreadytime is null) then matreadytime = current_timestamp(0);
  /*TODO: implementacja procedury
   dla kazdej pozycji materiaowej potrzebnej do wykoannai operacji
    naliaza STANYREZ pod katem, kiedy dana ilosc bdzie dostpna na magazynie ( z pominieciem stanyrez harmonogramu gownego, jesli dany nie jest glownym)
  */
  if(:mainschedule = 1) then begin
    --dla kazdej wymaganej pozycji materialowej dla danej operacji
    for select prschedguidespos.ref, prschedguidespos.ktm, prschedguidespos.wersja,
       prschedguidespos.amount - prschedguidespos.amountzreal, prschedguidespos.warehouse
      from PRSCHEDGUIDESPOS
      where prschedguidespos.prschedoper = :schedoper
      into :guidepos, :ktm, :wersja, :ilosc, :magazyn
    do begin
      --zmniejszam zapotrzebowanie o ilosci dostepne na magazynie - skoro są dostpne, to znaczy, ze nie ma oczekujacych blokad
      ilblock = null;
      select stanyil.iloscwolna from stanyil where KTM=:ktm and wersja = :wersja and magazyn = :magazyn into :ilblock;
      if(:ilblock > 0) then
        ilosc = :ilosc - :ilblock;
      if(:ilosc > 0) then begin
        --zmniejszam zapotrzebowanie o ilosci, ktore sam zablokowalem
        ilblock = null;
        select sum(stanyrez.ilosc) from STANYREZ where ZAMOWIENIE = :zam and POZZAM = :guidepos and ZREAL = 0 and STATUS = 'B' into :ilblock;
        if(:ilblock > 0) then
          ilosc = :ilosc - :ilblock;
        ilafterstart = :ilosc;
        if(:ilosc > 0) then begin
          --przegldanie przyszlosci obrotw, czy jest taki dzien, kiedy mi starczy z tego, co planowane
          for select stanyrez.ilosc, stanyrez.status, stanyrez.data
            from stanyrez
            where magazyn = :magazyn and ktm = :ktm and wersja = :wersja
              and (zamowienie <> :zam or (pozzam <> :guidepos))--pomijam wlasne zapisy na stanyrez
              and STATUS <> 'B'--blokady juz uwzglednilem w ilsoci dostepnej
              and zreal = 0
              order by priorytet,data
              into :rezilosc, :rezstatus, :rezdata
          do begin
            if(:ilosc > 0) then begin
             if(:rezstatus = 'D') then
                 ilosc = :ilosc - :rezilosc;
             else if (:rezstatus <> 'N') then
               ilosc = :ilosc + :rezilosc;
             if(:rezdata < :starttime) then
               ilafterstart = :ilosc;--jeszcze jestem przed startem, wiec ta ilosc to jeszcze brakujaca przed planowanym startem
             --ilosc < 0 - wiec juz mi starcza
             if(:ilosc <= 0 and (rezdata > :matreadytime or (:matreadytime is null) )) then begin
                matreadytime = :rezdata;
             end
            end
          end
        end
        if(:starttime is not null and :ilafterstart > 0) then begin
            --brakuje przed momentem startu - tyle trzeba doprodukwoac przed startem
            ilosc = :ilafterstart;
        end
        if(:ilosc > 0) then begin
          --nie starczyo planowanych dostaw - pytanie, ile czasu ptorzeba od teraz, by wyprodukowac BRAKUJACA ilosc
           if(:stechjedn is null) then begin
             execute procedure get_config('TECHJEDNQUANTITY',0) returning_values :stechjedn;
             --czas jednostki technologicznej
             techjedn = 1;
             techjedn = techjedn / cast(stechjedn as double precision);
           end
           select prsheets.techjednquantity/ prsheets.batchquantity * :ilosc, prsheets.prdepart
              from prsheets join prshopers on (prshopers.sheet = prsheets.ref)
              where prshopers.ref = :shoper
              into :preptechtime, :matprdepart;
           if(:preptechtime > 0) then begin
              preptechtime = :preptechtime + 1;--jedna jednostka tachnologiczna na potrzeby czasu zlozenia zamowienia
              worktime = :preptechtime * :techjedn;
              if(:matprdepart is null or (:matprdepart is null))then
                matprdepart = :prdepart;--ten sam wydzial produkcyjny, co trzeba bedzei kiedys zmienic - to powinein byc jakis "poprzedni" wydzial produkcyjny
              execute procedure pr_calendar_checkendtime(:matprdepart,current_timestamp(0),:worktime) returning_values :endtime;

              if(:endtime > :matreadytime or (:matreadytime is null)) then
               matreadytime = :endtime;
              if(:matstatus = 0) then--jest wykorzystanie doprodukowania - wiec zaznaczam to
                matstatus = 1;
              if(:starttime < :endtime) then
                matstatus = 2;--zakonczenie doprodukwoanai nie zdazy przed planowanym czasem
           end
        end
      end
    end
  end
  update prschedopers set MATERIALREADYTIME = :matreadytime, STATUSMAT = :matstatus where ref=:schedoper;
end^
SET TERM ; ^
