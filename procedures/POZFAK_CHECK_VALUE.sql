--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZFAK_CHECK_VALUE(
      DOKREF integer)
   as
declare variable cenacen numeric(14,4);
declare variable opk smallint;
declare variable ktm varchar(80);
declare variable ilosc numeric(14,4);
begin
  for
    select pozfak.cenanet, pozfak.opk, pozfak.ktm, pozfak.ilosc
      from pozfak
      where pozfak.dokument = :dokref  and pozfak.foc<>1 and coalesce(pozfak.fake,0)=0
    into :cenacen, :opk, :ktm, :ilosc
  do begin
    if(:cenacen is null) then cenacen = 0;
    if(:opk is null) then opk = 0;
    if(:cenacen = 0 and :opk = 0) then
      exception POZFAK_EXPT 'wprowadzono cenę zerową dla towaru: '||:ktm;
    if(:ilosc = 0) then
      exception POZFAK_EXPT 'wprowadzono ilość = 0 dla towaru: '||:ktm;
  end
end^
SET TERM ; ^
