--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_PRSH_MULTIOPER(
      SHEET integer,
      OPERMASTER integer,
      POZIOM integer,
      NUMER varchar(255) CHARACTER SET UTF8                           )
  returns (
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      NUMBER integer,
      OPIS varchar(255) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE opermaster1 INTEGER;
  DECLARE VARIABLE number1 INTEGER;
  DECLARE VARIABLE ref INTEGER;
BEGIN
  IF (:opermaster IS NULL) THEN BEGIN
    FOR SELECT prshopers.ref, prshopers.descript, prshopers.number FROM prshopers
      WHERE (prshopers.sheet =:sheet AND prshopers.opermaster IS NULL)
        ORDER BY prshopers.sheet
          INTO :ref, :descript, :number
    DO BEGIN
              numer = :number;
      opis = :numer;
      SUSPEND;
      IF ( EXISTS (SELECT ref FROM prshopers
        WHERE prshopers.sheet = :sheet AND prshopers.opermaster = :ref)) THEN
      BEGIN
        opermaster1 = :ref;
--        exception test_break 'ref:'||:ref||' numer:'||:numer;
        EXECUTE PROCEDURE X_PRSH_MULTIOPER(:sheet, :ref, :poziom, :numer) RETURNING_VALUES :descript, :number, :opis;
      END
    END
  END
  ELSE BEGIN
    FOR SELECT prshopers.ref, prshopers.descript, prshopers.number FROM prshopers
         WHERE (prshopers.sheet =:sheet AND prshopers.opermaster= :opermaster )
          ORDER BY prshopers.sheet
            INTO :ref, :descript, :number
    DO BEGIN
      numer=numer||'.'||(:number);
      opis = :numer;
      SUSPEND;


      IF ( EXISTS (SELECT ref FROM prshopers
        WHERE prshopers.sheet = :sheet AND prshopers.opermaster = :ref)) THEN BEGIN
          opermaster1 = :ref;
          exception test_break 'opermaster'||:opermaster1||'ref:'||:ref||' numer:'||:numer;
          EXECUTE PROCEDURE X_PRSH_MULTIOPER(:sheet, :opermaster1, :poziom, :numer) RETURNING_VALUES :descript, :number, :opis;

      END
    END
  END
END^
SET TERM ; ^
