--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GENERATEBTRANSFER(
      ROZRACH ROZRACH_ID,
      CUMULATION smallint,
      GROUPNUMBER INTEGER_ID,
      TRANTYPE BTRANTYPE_ID,
      BANKACCO BANKACC_ID,
      TERMTOLERATIN INTEGER_ID,
      BEFORETERM INTEGER_ID)
  returns (
      SUCCESS smallint,
      CUMMUL smallint)
   as
declare variable VSLODEF SLO_ID;
declare variable VSLOPOZ SLOPOZ_ID;
declare variable VBTRANSFER BTRANSFER_ID;
declare variable VFIELDNAZWA STRING;
declare variable VTOACC STRING60;
declare variable VTYP SLOTYP_ID;
declare variable VTOADRESS STRING;
declare variable VDATAPLAT TIMESTAMP_ID;
declare variable VCURR CURRENCY_ID;
declare variable VWALUTA WALUTA_ID;
declare variable VACCOUNT ACCOUNT_ID;
declare variable VSETTLEMENT SETTLEMENT_ID;
declare variable VSLOWNIK STRING;
declare variable SQL STRING1024;
declare variable RES integer;
declare variable SLONAZWA varchar(255);
declare variable INDEKS varchar(255);
declare variable PREFIX varchar(255);
declare variable REFFIELD varchar(255);
declare variable KODFIELD varchar(255);
declare variable KODKSFIELD varchar(255);
declare variable NAZWAFIELD varchar(255);
declare variable NIPFIELD varchar(255);
declare variable FILTR varchar(255);
declare variable FILTRNAME varchar(255);
begin
  vbtransfer = null;
  cummul = 1;

 --Najpierw należy sciągnąć odpowiednie dane z rocrachunku
 select r.slodef, r.slopoz, r.dataplat, r.waluta, r.kontofk, r.symbfak
   from rozrach r
   where r.ref = :rozrach and dateadd(- :beforeterm day to r.dataplat) <= current_timestamp(0)
   into :vslodef, :vslopoz, :vdataplat, :vwaluta, :vaccount, :vsettlement;
 --Jeżeli jest kumulacja szukamy btransferu do którego możemy dokleić pozycje
 if (cumulation = 1) then
 begin
   select b.ref
     from btransfers b
     where b.slodef = :vslodef and b.slopoz = :vslopoz and b.bankacc = :bankacco
       and b.gengroup = :groupnumber and( b.data <=  dateadd(:termtoleratin day to :vdataplat)
         and b.data >= dateadd(-:termtoleratin day to :vdataplat))
     into :vbtransfer;
 end
 --Jeżeli nie nie mamy naglówka przelewu ( bo nie ma kumulacji, lub pierwsza pozycja w kumulacji)
 --to musimy go zalożyć
 if (vbtransfer is null) then
 begin
   --oznaczamy, że tworzorzny jest nowy przelew
   cummul=0;
   --sciagamy potrzebne dane

   --nazwa ze slownika dla BTRANSFERS.TOWHO oraz typ dla ustalenia adresu potem
   execute procedure slownik_nazwa_Fromks(:vslodef, :vslopoz)
     returning_values :vfieldnazwa;

  execute procedure SLOPARAMS(:vslodef,null)
    returning_values :res,:vtyp,:indeks,:prefix,:reffield,:kodfield,
      :kodksfield,:nazwafield,:nipfield,:filtr,:filtrname;

  if(:res > 0) then
  begin
    if(:vtyp = 'SLOPOZ') then
    begin
      sql = 'select first 1 '||:nazwafield||' from '||:vtyp||' where SLOWNIK='||vslodef||' and ref ='||:vslopoz;
      execute statement sql into :vfieldnazwa;
    end 
    else
    begin
      if (vtyp in ('KLIENCI', 'DOSTAWCY', 'CPERSONS', 'EMPLOYEES')) then  --PR59074
      begin
        sql = 'select first 1 '||:nazwafield||' from '||:vtyp||' where ref ='||vslopoz||
        case when coalesce(:filtr,'') <> '' then ' and '||:filtr else '' end;
        ---exception universal''||sql;
        execute statement sql into :vfieldnazwa;
      end
      else
      begin
        sql = 'select first 1 '||:nazwafield||' from '||:vtyp||' where ref ='||vslopoz;
        execute statement sql into :vfieldnazwa;
      end
    end
  end

   --teraz nalezy sciagnac adres w zaleznosci jaki mamy slownik
   if (vtyp ='KLIENCI') then
   begin
     select k.ulica||iif(coalesce(k.nrdomu,'') <> '', ' '||k.nrdomu,'')||iif(coalesce(k.nrlokalu,'') <> '', '/'||k.nrlokalu,'')||'; '||k.kodp||' '||k.miasto
       from klienci k
       where k.ref = :vslopoz
       into :vtoadress;
   end else if (vtyp = 'DOSTAWCY') then
   begin
     select d.ulica||iif(coalesce(d.nrdomu,'') <> '', ' '||d.nrdomu,'')||iif(coalesce(d.nrlokalu,'') <> '', '/'||d.nrlokalu,'')||'; '||d.kodp||' '||d.miasto
       from dostawcy d
       where d.ref = :vslopoz
       into :vtoadress;
   end else if (vtyp = 'EMPLOYEES') then
   begin
     select ea.street||' '||ea.houseno||iif(ea.localno <> '' ,'/'||ea.localno,'')||' '||ea.postcode||' '||ea.post
       from employees e join epersaddr ea on (e.person = ea.person)
       where e.ref = :vslopoz and ea.addrtype = 0 and ea.status = 1
       into :vtoadress;
   end else if (vtyp = 'PERSONS') then
   begin
     select ea.street||' '||ea.houseno||iif(ea.localno <> '' ,'/'||ea.localno,'')||' '||ea.postcode||' '||ea.post
       from epersaddr ea
       where ea.person = :vslopoz and ea.addrtype = 0 and ea.status = 1
       into :vtoadress;
   end
   --teraz należy znalezc
   --rachunek z rozrachunku dla BTRANSFERS.TOACC
   execute procedure btransfers_get_bankacc(:rozrach,:vslodef, :vslopoz)
     returning_values :vtoacc;
   --szukamy waluty
   select bac.curr
     from bankacc bac
     where bac.symbol = :bankacco
     into :vcurr;
   vdataplat = coalesce(:vdataplat,current_timestamp(0));
   --insertujemy
   insert into btransfers(BANKACC,BTYPE,slodef,slopoz,gengroup,
     data,dataopen,toadress,towho,toacc,curr,status)
     values(:bankacco,:trantype,:vslodef,:vslopoz,:groupnumber,iif(:vdataplat > current_timestamp(0),:vdataplat,current_timestamp(0)),
       current_timestamp(0),:vtoadress,:vfieldnazwa,:vtoacc,coalesce(:vcurr,:vwaluta),0)
     returning ref into :vbtransfer;  
 end

 -- teraz to już na pewno mamy naglowek przelewu to pora na pozycje  
  insert into btransferpos(btransfer,account,settlement)
    values(:vbtransfer,:vaccount,:vsettlement);
  success = 1;
  suspend;

  when any do
  begin
    success = 0;
    suspend;
  end
end^
SET TERM ; ^
