--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_TRANSFER_EVACLIMITS(
      LYEAR smallint)
   as
declare variable s1 varchar(255); /* nr ewidencyjny(KOD_ZEWN) */
declare variable s2 varchar(255); /* ilosc dni */
declare variable tmp integer;
declare variable limit integer;
declare variable employee integer;
declare variable evaclimit integer;
declare variable kodzewn varchar(40);
begin
  exception test_break 'procedura nie powinna byc uzywana';      --BS19954

  /*
  select count(ref) from expimp where coalesce(char_length(s1),0)>40 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich kodow zewnetrznych.';

  for select s1, s2
    from expimp 
  into :s1, :s2
  do begin
 --nr ewidencyjny(KOD_ZEWN)
    kodzewn=substring(:s1 from 1 for 40);

--rodzaj umowy
    limit = cast(:s2 as integer);

-- pracownik
    select ref from employees where kodzewn=:kodzewn
    into :employee;

-- INSERT
    insert into evaclimits(employee, vyear)
      values(:employee, :lyear);

    select max(ref) from evaclimits
    into :evaclimit;

    insert into evaclimincs(employee, evaclimit,  iyear, fromdate, limit)
      values (:employee, :evaclimit, :lyear, cast('31-12-'||:lyear as date), :limit);


  end
  */
end^
SET TERM ; ^
