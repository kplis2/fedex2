--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDECL_CHECK_REQUIRED_FIELDS(
      NREF integer)
  returns (
      MSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable SCODE varchar(20);
declare variable EDECLDEF integer;
declare variable FIELD integer;
declare variable FIELDNAME varchar(80);
declare variable PVALUE varchar(255);
declare variable F_KOSZTY smallint;
declare variable F_PRZYCHOD smallint;
declare variable F_DOCHOD smallint;
declare variable F_ZALICZKA smallint;
begin
  msg = '';

  select f.systemcode, d.edecldef
    from edeclarations d
      join edecldefs f on (d.edecldef = f.ref)
    where d.ref = :nref
    into scode, edecldef;

  for
    select field, fieldname from edeclposdefs
      where edecldef = :edecldef
        and required = 1
      into :field, :fieldname
  do begin
    if (not exists(select 1 from edeclpos where edeclaration = :nref and field = :field)) then
      msg = 'Nieuzupełnione pole nr '||field||': '||coalesce(fieldname,'');
  end

  if (msg = '' and scode in ('PIT-11 (19)', 'PIT-11 (20)', 'PIT-11 (21)')) then
  begin
  --weryfikacja osoba fizyczna/niefizyczna BS55095
    select pvalue from edeclpos
      where edeclaration = :nref and field = 7
      into :pvalue;

    if (pvalue = 1 and not exists(select 1 from edeclpos where edeclaration = :nref and field = 8)) then
      msg = 'Nieuzupełnione pole nr 8: Nazwa pełna, REGON';
    else if (pvalue = 2 and not exists(select 1 from edeclpos where edeclaration = :nref and field = 9)) then
      msg = 'Nieuzupełnione pole nr 9: Nazwisko, pierwsze imię, data urodzenia';

  --jezeli jest uzupelniona informacja o kosztach, to koszty (34) to polaprzychod, dochod i zaliczka 35,37,39 sa obowiazkowe i vice versa
    if (scode = 'PIT-11 (19)') then
    begin
      f_koszty = 34;
      f_przychod = 35;
      f_dochod = 37;
      f_zaliczka = 39;
    end else
    if (scode in ('PIT-11 (20)', 'PIT-11 (21)')) then
    begin
      f_koszty = 24;
      f_przychod = 25;
      f_dochod = 27;
      f_zaliczka = 29;
    end

    if (exists(select 1 from edeclpos where edeclaration = :nref and field = :f_koszty)) then
    begin
      if (not exists(select 1 from edeclpos where edeclaration = :nref and field = :f_przychod)) then
        msg = 'Nieuzupełnione pole nr ' || f_przychod ||': Przychód';
      else if (not exists(select 1 from edeclpos where edeclaration = :nref and field = :f_dochod)) then
        msg = 'Nieuzupełnione pole nr ' || f_dochod ||': Dochód';
      else if (not exists(select 1 from edeclpos where edeclaration = :nref and field = :f_zaliczka)) then
        msg = 'Nieuzupełnione pole nr ' || f_zaliczka ||': Zaliczka pobrana przez płatnika';
    end else
    if (exists(select 1 from edeclpos where edeclaration = :nref
          and field in (:f_przychod, :f_dochod, :f_zaliczka))
    ) then
      msg = 'Nieuzupełnione pole nr ' || f_koszty ||': Koszty uzyskania przychodów';
  end

  msg = coalesce(msg, '');

  suspend;
end^
SET TERM ; ^
