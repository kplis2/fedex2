--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DATECS_CLOSE_PARAGON(
      NAGFAK integer,
      LANG CODEISO639)
  returns (
      MSG STRING1024)
   as
begin
--Funkcja powinna zwracac podsumowanie oraz stopkę, czyli od zakończenia pozycji do końca paragonu
    msg = 'close paragon';
    suspend;
end^
SET TERM ; ^
