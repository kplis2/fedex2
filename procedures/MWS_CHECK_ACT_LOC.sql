--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CHECK_ACT_LOC(
      MWSCONSTLOCLS varchar(40) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint,
      MWSCONSTLOCREF INTEGER_ID)
   as
declare variable ref integer;
declare variable act smallint;
begin
  status = 0;
  select c.ref, c.act
    from mwsconstlocs c where c.symbol = :mwsconstlocls
    into :mwsconstlocref, act;
  if (:mwsconstlocref is null) then
    status = 2;
  else if (:mwsconstlocref is not null and act = 0) then
    status = 1;
end^
SET TERM ; ^
