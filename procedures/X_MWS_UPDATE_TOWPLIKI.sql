--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_UPDATE_TOWPLIKI(
      KTM KTM_ID,
      MODE SMALLINT_ID)
   as
  declare variable wersjaref integer_id;
  declare variable ref integer_id;
  declare variable num integer_id;
begin
--mode 1 - numerowanie tylko wybranego ktm
--mode 2 - numerowanie calej tabeli
  num = 0;
  if (mode = 1) then begin
  -- Ldz numerowanie zdjec w kartotece towpliki, dla calego ktm
    for
      select  t.ref
        from towpliki t
        where t.ktm = :ktm
        order by t.x_glowne desc
      into :ref
    do begin
      num = num + 1;
      update towpliki tt
        set tt.numer = :num
        where  tt.ref = :ref;
    end
  end
  if (mode = 2) then begin
  --numerowanie calej tabeli po ktm
    for
      select distinct t.ktm
        from towpliki t
        order by t.x_glowne desc
      into :ktm
    do begin
      num = 0;
      for
        select  t.ref
          from towpliki t
          where t.ktm = :ktm
          order by t.x_glowne desc
        into :ref
      do begin
        num = num + 1;
        update towpliki tt
          set tt.numer = :num
          where  tt.ref = :ref;
      end
    end
  end
end^
SET TERM ; ^
