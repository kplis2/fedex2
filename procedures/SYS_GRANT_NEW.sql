--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_GRANT_NEW(
      NAME varchar(255) CHARACTER SET UTF8                           ,
      TYPE smallint)
  returns (
      GRANTED varchar(31) CHARACTER SET UTF8                           ,
      GRANTOR varchar(31) CHARACTER SET UTF8                           ,
      PRIVILEGE char(6) CHARACTER SET UTF8                           ,
      USERTYPE smallint,
      OBJECTTYPE smallint)
   as
begin
    if(TYPE in (4,6,7)) then begin -- 4 - tabele, 6-procedury, 7-widoki
        for select
           trim(rdb$user),
           trim(rdb$grantor),
           trim(rdb$privilege),
           rdb$object_type,
           rdb$user_type
       from rdb$user_privileges
       where rdb$relation_name = upper(:name)
        into :granted, :grantor, :privilege, :objecttype, :usertype
        do begin
            suspend;
    end
end
  suspend;
end^
SET TERM ; ^
