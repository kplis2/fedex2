--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_AUTO_FLOWIN(
      MWSCONSTLOC INTEGER_ID,
      AKTUOPERATOR OPERATOR_ID,
      FROMMWSORD INTEGER_ID = 0,
      MWSACTREF INTEGER_ID = 0)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING1024)
   as
declare variable X_FLOW_GROUP integer_id;
declare variable X_FLOW_ORDER smallint_id;
declare variable ref_mwsconstlocp integer_id;
declare variable ref_mwsconstlocl integer_id;
declare variable ref_mwsconstlocunite integer_id;
declare variable ref_mwsact mwsords_id;
declare variable ref_mwsactfrom mwsords_id;
declare variable ref_mwsord mwsords_id;
declare variable ref_mwsordunite mwsords_id;
declare variable mwspalloc integer_id;
declare variable wh string3;
declare variable timestart timestamp_id;
declare variable timestartdcl timestamp_id;
declare variable timestopdcl timestamp_id;
declare variable mwsaccessory integer_id;
declare variable period period_id;
declare variable branch string10;
declare variable good string40;
declare variable vers integer_id;
declare variable whareap integer_id;
declare variable maxnumber integer_id;
declare variable quantity quantity_mws;
declare variable realtime double precision;
declare variable mixtakepal integer_id;
declare variable mixleavepal integer_id;
declare variable mixmwsconstlocl integer_id;
declare variable mixmwspallocl integer_id;
declare variable mixrefill smallint_id;
declare variable x_partia date_id;
declare variable x_serial_no integer_id;
declare variable x_slownik integer_id;
declare variable lot integer_id;
begin
  status = 1;
  maxnumber = 1;
  msg = '';
  if (mwsactref is null) then
    select first 1 m.ref
      from mwsacts m
      where m.mwsord = :frommwsord
    into :mwsactref;
  if (frommwsord is null) then
    select first 1 m.mwsord
      from mwsacts m
      where m.ref = :mwsactref
    into :frommwsord;
-- exception universal  coalesce(MWSCONSTLOC, 'MWS')||' ' || coalesce(AKTUOPERATOR, 'OPR') ||' ' ||coalesce(FROMMWSORD, 'ORD') ||' ' ||coalesce(MWSACTREF, 'ACT') ;
-- pobranie grupy z pozycji magazynowej, wh, przeplywowy ?, poz 5 ?
  select first 1 mc.x_flow_group, mc.wh
    from mwsconstlocs mc
    where mc.ref = :mwsconstloc
      and mc.x_row_type = 1
      and mc.x_flow_order = 5
  into :x_flow_group, :wh;

-- szukamy czy wczesniej nie byl przsuwany towar z tego samego zlecenia
  if (x_flow_group is not null) then
    select first 1 a.mwsord, a.mwsconstlocl
      from mwsacts a
        join mwsords o on (a.mwsord = o.ref)
      where o.frommwsord = :frommwsord
        and a.mwsconstlocp = :mwsconstloc
    into :ref_mwsordunite, :ref_mwsconstlocl;

-- nie byo przesuwane wczesniej to szukamy najmniejszej pozycji wolnej w regale przeplywowym
  if (x_flow_group is not null and :ref_mwsconstlocl is null) then
    select  first 1 mc.x_flow_order, mc.ref
      from mwsconstlocs mc
      where (not exists(select * from mwsstock m where  mc.ref = m.mwsconstloc))
      and mc.x_flow_group = :x_flow_group
      order by mc.x_flow_order asc
    into :x_flow_order, :ref_mwsconstlocl;
  if (:x_flow_order <> 5) then begin -- dla poz 5, gdy inne zajete nie przesuwamy
-- generowanie mwsord
    if (:ref_mwsordunite is null) then begin
      select oddzial from defmagaz where symbol = :wh into branch;   --branch
      select ref from mwsaccessories where aktuoperator = :aktuoperator into :mwsaccessory;  --mwsaccessory
      select okres from datatookres(current_date,0,0,0,0) into period; --period
      execute procedure gen_ref('MWSORDS') returning_values ref_mwsord; --ref_mwsord
      insert into mwsords (ref, mwsordtype, stocktaking, doctype, operator, priority, description, wh, regtime, timestartdecl,
              timestopdecl, mwsaccessory, branch, period, cor, rec, bwharea, ewharea, status, frommwsord )
      values (:ref_mwsord, 9, 0, 'O', :aktuoperator, 0, 'auto przeplyw palet', :wh, current_timestamp(0), :timestart,
             current_timestamp(0), :mwsaccessory, :branch, :period, 0, 0, null, null, 0, :frommwsord);
    end else
      ref_mwsord = ref_mwsordunite;
    select wharea from mwsconstlocs where ref = :ref_mwsconstlocl into :whareap; -- whareap
-- tworzenie nowego mwsact na podstawie poprzedniego
    select good, vers, quantityc, mwspallocl, mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, mixtakepal, x_partia, x_serial_no, x_slownik, lot
        from mwsacts a
        where a.ref = :mwsactref
    into :good, :vers, :quantity, :mwspalloc, :mixtakepal, :mixleavepal, :mixmwsconstlocl, :mixmwspallocl, :mixrefill, :x_partia, :x_serial_no, :x_slownik, :lot;
-- znalezienie max. numeru operacji - kolejne zwiekszamy o jeden
    select max(number) from mwsacts where mwsord = :ref_mwsord into maxnumber;
    if (maxnumber is null) then maxnumber = 0;
    maxnumber = maxnumber + 1;
    execute procedure gen_ref('MWSACTS') returning_values ref_mwsact;  --ref_mwsact
    insert into mwsacts (ref, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp, mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode,
              closepalloc, wh, wharea, whsec, regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, recdoc, plus, whareap, whareal, wharealogp,
              wharealogl, number, disttogo, mixedpallgroup, palgroup, mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, refilltry, x_partia, x_serial_no, x_slownik,
              lot, frommwsact)
    values (:ref_mwsact, :ref_mwsord, 0, 0, :good, :vers, :quantity, :mwsconstloc, :mwspalloc, :ref_mwsconstlocl, :mwspalloc, null, null, 'O', 0,
              0, :wh, :whareap, null, current_timestamp(0), :timestartdcl, :timestopdcl, :realtime, null, null, 1, 0, 1, null, null, null,
              null, :maxnumber, 0, null, null, :mixtakepal, :mixleavepal, :mixmwsconstlocl, :mixmwspallocl, :mixrefill, :x_partia, :x_serial_no, :x_slownik,
              :lot, :mwsactref);
-- auto commit mwsacts
    update mwsacts ma set ma.status = 1
      where ma.ref  = :ref_mwsact;
    update mwsacts ma set ma.status = 2, ma.quantityc = ma.quantity
      where ma.ref  = :ref_mwsact;
-- auto commit mwsord
    if (exists(select first 1 1 from mwsacts ma where ma.mwsord = :ref_mwsord)) then begin
      update mwsords mo set mo.status = 1, mo.timestopdecl = :timestopdcl, mo.operator = null, mo.mwsaccessory = :mwsaccessory
        where mo.ref = :ref_mwsord
          and mo.status = 0;
      update mwsords mo set mo.status = 5, mo.operator = :aktuoperator
        where mo.ref = :ref_mwsord
          and mo.status = 1;
    end
    else begin
-- kasowanie pustego mwsord
      delete from mwsords where ref = :ref_mwsord;
    end
  end
end^
SET TERM ; ^
