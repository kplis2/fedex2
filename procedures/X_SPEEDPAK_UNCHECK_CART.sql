--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_SPEEDPAK_UNCHECK_CART(
      LISTWYSD LISTYWYSD_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
  declare variable refdok integer_id;
  declare variable grupasped integer_id;
begin
  status = 1;
  msg ='';
  --ZG 138017
  --procedura sluzaca do odznaczenia zaznaczonych wozkow
  --uzywana przy wychodzeniu z okna szybkiego pakowania

  --pobranie grupy spedycyjnej
  select ld.refdok
    from listywysd ld
    where ld.ref = :listwysd
  into :grupasped;

  --odznaczenie wszystkich wozkow z danej grupy spedycyjneh
  for
    select dn.ref
      from dokumnag dn
      where dn.grupasped = :grupasped
    into :refdok
  do begin
    update mwsordcartcolours mc
      set mc.x_check_on_pack = 0
      where mc.docid = :refdok;
  end
  suspend;
end^
SET TERM ; ^
