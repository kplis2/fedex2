--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_TRANSPORT_ORDER_CLOSE(
      NRIDENT STRING40)
  returns (
      STATUS smallint,
      MSG STRING1024)
   as
declare variable REFLISTYWYS LISTYWYS_ID;
declare variable ZALADOWANO INTEGER_ID;
declare variable ILOSCOPK INTEGER_ID;
declare variable EOL STRING2;
declare variable LSYMBOL STRING20;
declare variable ROZNICA INTEGER_ID;
declare variable TOPLACE STRING20;
declare variable SYMBOLSPED STRING1024;
begin
  eol ='
';
  status = 1;
  msg = '';

  -- szukam ref zlecenia transportowego
  select first 1 l.ref
    from listywys l
    where lower(l.x_nr_ident) = lower(:nrident)
      and l.stan = 'O'
    order by l.dataplwys desc, l.ref desc -- zeby zlapac najnowszy, jakby ktos otworzyl stary
  into :reflistywys;

  -- sprawdzamy czy wszystkie paczki dokumentu spedycyjnego zostały załadowane
  select first 1 d.toplace, d.symbolsped, d.x_zaladowano,
      /*coalesce(d.x_polpaleta,0) + coalesce(d.iloscpalet,0) + coalesce(d.iloscpaletbez,0) +
          coalesce(d.iloscpaczek,0) + coalesce(d.x_drobnica,0)*/ --xxx KBI 96095
          count(1)
    from listywysd d
      left join listywysdroz_opk o on o.listwysd = d.ref
    where d.listawys = :reflistywys
      and o.x_zaladowane > 0
    group by d.toplace, d.symbolsped, d.x_zaladowano
    order by d.toplace
  into :toplace, :symbolsped, :zaladowano, :iloscopk;

  if (coalesce(iloscopk,0) = 0) then
  begin
    status = 0;
    msg = 'Nic nie zostało jeszcze załadowane.';
    exit;
  end

  roznica = coalesce(iloscopk,0) - coalesce(zaladowano, 0);
  if (roznica <> 0) then begin
    status = 0;
    if (roznica = 1) then
      msg = ' opakowanie za mało';
    else if (roznica > 1 and roznica < 5) then
      msg = ' opakowania za mało';
    else if (roznica >= 5) then
      msg = ' opakowań za mało';
  end

  if (status < 1) then begin
    msg = 'Nie udało się zamknąć załadunku samochodu!'||eol||
          'Dla listu spedycyjnego nr '||symbolsped||' utworzonego dla odbiorcy '||
          toplace||' załadowano o '||roznica||msg;
  end else begin
    update listywys set stan = 'A' where ref = :reflistywys;
    select l.symbol from listywys l where l.ref = :reflistywys into :lsymbol;
    msg = 'Zamknięcie załadunku samochodu zakończone sukcesem.'||eol||'Nr zlecenia transportowego '||lsymbol;
  end
end^
SET TERM ; ^
