--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LOG_SECURITY(
      TABLENAME varchar(60) CHARACTER SET UTF8                           ,
      LOGFIL varchar(60) CHARACTER SET UTF8                           ,
      ITEM varchar(20) CHARACTER SET UTF8                           ,
      OPIS varchar(2048) CHARACTER SET UTF8                           ,
      KEY1 varchar(120) CHARACTER SET UTF8                           ,
      KEY2 varchar(80) CHARACTER SET UTF8                           ,
      KEY3 varchar(10) CHARACTER SET UTF8                           )
  returns (
      RREF integer)
   as
begin

  if (key1 is null) then key1='';
  if (key2 is null) then key2='';
  if (key3 is null) then key3='';
  if (item is null or item = '') then item= 'RUN';
  if (logfil is null) then
    logfil = substring(tablename from 1 for 15);
  else
    logfil = substring(logfil from 1 for 15);
  if (tablename is not null and tablename <> '') then
  begin
    insert into security_log (logfil, tablename, item, key1, key2, key3, opis)
      values (upper(:logfil), upper(:tablename), :item, :key1, :key2, :key3, :opis)
      returning ref into :rref;
  end
  else rref = 0;
end^
SET TERM ; ^
