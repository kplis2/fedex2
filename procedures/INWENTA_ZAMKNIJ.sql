--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INWENTA_ZAMKNIJ(
      INWENTA integer,
      DOKPLU varchar(3) CHARACTER SET UTF8                           ,
      DOKMIN varchar(3) CHARACTER SET UTF8                           ,
      OPERATOR integer)
  returns (
      STATUS integer,
      DOKSPLU varchar(30) CHARACTER SET UTF8                           ,
      DOKSMIN varchar(30) CHARACTER SET UTF8                           )
   as
declare variable dokummin integer;
declare variable dokumplu integer;
declare variable data timestamp;
declare variable magazyn varchar(3);
declare variable cnt integer;
declare variable zpartiami integer;
declare variable dummy integer;
declare variable wrongktm varchar(40);
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable dostawa integer;
declare variable ilinw numeric(14,4);
declare variable ilstan numeric(14,4);
declare variable cenaplus numeric(14,4);
declare variable datawazn timestamp;
declare variable cena numeric(14,4);
declare variable numer integer;
begin
  STATUS=-1;
  DOKSPLU='';
  DOKSMIN='';
  select DATA, MAGAZYN, ZPARTIAMI from INWENTA where REF=:inwenta into :data, :magazyn, :zpartiami;
  if(:data is null) then exit;
  select count(*) from INWENTAP where INWENTA = :inwenta into :cnt;
  if(:cnt is null) then cnt = 0;
  if(:cnt = 0) then exception INWENTAACK_NOPOZ;
  /*sprawdzenie cen przychodowych*/
  wrongktm = '';
  select first 1 INWENTAP.KTM
     from INWENTAP
     where INWENTAP.INWENTA=:inwenta
     group by inwentap.ktm,inwentap.wersja,inwentap.cena,inwentap.dostawa
     having min(INWENTAP.CENAPLUS)<>max(INWENTAP.CENAPLUS) and sum(INWENTAP.ILINW)>min(INWENTAP.ILSTAN)
     into :wrongktm;
  if(:wrongktm is not null and :wrongktm<>'') then exception INWENTAACK_NOPOZ 'Niejednoznaczne ceny przychodowe dla: '||:wrongktm;
  /*zaktualizowanie stanu wg dokumentow*/
  execute procedure INWENTA_NALICZ_STANY(:inwenta,NULL,NULL,NULL,NULL,1,0,NULL) returning_values :dummy,:dummy,:dummy;
  /*dokument przychodowy*/
  dokumplu = null;
  if(:dokplu <> '') then begin
     for select INWENTAP.KTM,INWENTAP.WERSJA,INWENTAP.DOSTAWA,
       sum(INWENTAP.ILINW),max(INWENTAP.ILSTAN),max(INWENTAP.CENAPLUS),max(DOSTAWY.DATAWAZN)
     from INWENTAP
       left join DOSTAWY on (DOSTAWY.REF=INWENTAP.DOSTAWA)
       where INWENTAP.INWENTA=:inwenta
       group by inwentap.ktm,inwentap.wersja,inwentap.cena,inwentap.dostawa
       having max(INWENTAP.ILSTAN)<sum(INWENTAP.ILINW)
       into :ktm,:wersja,:dostawa,:ilinw,:ilstan,:cenaplus,:datawazn
     do begin
       if(:dokumplu is null) then begin
         execute procedure GEN_REF('DOKUMNAG') returning_values :dokumplu;
         numer = 1;
         insert into DOKUMNAG(REF,MAGAZYN,TYP,DATA, OPERATOR)
           values(:dokumplu, :magazyn, :dokPLU, :data, :operator);
       end
       insert into DOKUMPOZ(DOKUMENT,NUMER,ORD,KTM,WERSJA,ILOSC,CENA,DOSTAWA,DATAWAZN)
       values(:dokumplu, :numer, 1, :ktm, :wersja, :ilinw-:ilstan, :cenaplus, :dostawa, :datawazn);
       numer = :numer + 1;
     end
  end
  /*dokument rozchodowy*/
  dokummin = null;
  if(:dokmin <> '') then begin
     for select INWENTAP.KTM,INWENTAP.WERSJA,INWENTAP.DOSTAWA,
       sum(INWENTAP.ILINW),max(INWENTAP.ILSTAN),INWENTAP.CENA
     from INWENTAP
       left join DOSTAWY on (DOSTAWY.REF=INWENTAP.DOSTAWA)
       where INWENTAP.INWENTA=:inwenta
       group by inwentap.ktm,inwentap.wersja,inwentap.cena,inwentap.dostawa
       having max(INWENTAP.ILSTAN)>sum(INWENTAP.ILINW)
       into :ktm,:wersja,:dostawa,:ilinw,:ilstan,:cena
     do begin
       if(:dokummin is null) then begin
         execute procedure GEN_REF('DOKUMNAG') returning_values :dokummin;
         numer = 1;
         insert into DOKUMNAG(REF,MAGAZYN,TYP,DATA, OPERATOR)
           values(:dokummin, :magazyn, :dokmin, :data, :operator);
       end
       if(:zpartiami<>1) then cena = null;
       if(:zpartiami<>1) then dostawa = null;
       insert into DOKUMPOZ(DOKUMENT,NUMER,ORD,KTM,WERSJA,ILOSC,CENA,DOSTAWA)
       values(:dokummin, :numer, 1, :ktm, :wersja, :ilstan-:ilinw, :cena, :dostawa);
       numer = :numer + 1;
     end
  end
  if(:dokumplu > 0) then begin
    update DOKUMNAG set AKCEPT=1 where REF=:dokumplu;
    select SYMBOL from DOKUMNAG where REF=:dokumplu into :doksplu;
    update INWENTA set INWP=:dokumplu, DATAZAMK=current_date  where REF=:inwenta;
  end
  if(:dokummin > 0) then begin
    update DOKUMNAG set AKCEPT=1 where REF=:dokummin;
    select SYMBOL from DOKUMNAG where REF=:dokummin into :doksmin;
    update INWENTA set INWM=:dokummin, DATAZAMK=current_date  where REF=:inwenta;
  end
  execute procedure INWENTA_OBLNAG(:inwenta);
  STATUS=1;
end^
SET TERM ; ^
