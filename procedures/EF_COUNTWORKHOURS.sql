--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_COUNTWORKHOURS(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable FROMDATE date;
declare variable TODATE date;
declare variable HOURS integer;
declare variable CONTRACT integer;
declare variable WORKDIM float;
declare variable DIMNUM smallint;
declare variable DIMDEN smallint;
declare variable PROPORTIONAL smallint;
begin
  --DU: personel - funkcja podaje liczbe godzin w miesiacu,
  --które powinien przepracowac pracownik

  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  execute procedure emplcaldays_store('SUM', :employee, :fromdate, :todate)  --PR53533
    returning_values :hours;

  select max(ref)
    from emplcontracts C
      join econtrtypes T on (C.econtrtype = T.contrtype)
    where employee = :employee and T.empltype = 1
      and C.fromdate <= :todate and (C.enddate is null or C.enddate >= :todate)
    into :contract;

  if (contract is not null) then
  begin
    select workdim, dimnum, dimden, proportional
      from emplcontracts
      where ref = :contract
      into :workdim, :dimnum, :dimden, proportional;
    if (workdim <> 1 and proportional = 1) then
      hours = hours * dimnum / dimden;
  end

  if (hours is null) then
    hours = 0;
  ret = hours / 3600.00;
  suspend;
end^
SET TERM ; ^
