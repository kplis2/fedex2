--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CALCULATE_AMORTIZATION(
      FXDASSET integer,
      AMPERIOD integer)
   as
declare variable USING_DATE timestamp;
declare variable USING_DATE_YEAR integer;
declare variable USING_DATE_MONTH integer;
declare variable A_YEAR integer;
declare variable A_MONTH integer;
declare variable PERIOD_REF integer;
declare variable PERIOD_YEAR integer;
declare variable PERIOD_MONTH integer;
declare variable TAX_VALUE numeric(15,2);
declare variable T_VALUE numeric(15,2);
declare variable F_VALUE numeric(15,2);
declare variable TAX_REDEMPTION numeric(15,2);
declare variable ACC_TAX_REDEMPTION numeric(15,2);
declare variable TAX_VALUE_DOC numeric(15,2);
declare variable TAX_REDEMPTION_DOC numeric(15,2);
declare variable FIN_VALUE numeric(15,2);
declare variable FIN_REDEMPTION numeric(15,2);
declare variable ACC_FIN_REDEMPTION numeric(15,2);
declare variable FIN_VALUE_DOC numeric(15,2);
declare variable FIN_REDEMPTION_DOC numeric(15,2);
declare variable TAX_INITIAL_VALUE numeric(15,2);
declare variable FIN_INITIAL_VALUE numeric(15,2);
declare variable TAX_AMORT_RATE numeric(15,2);
declare variable FIN_AMORT_RATE numeric(15,2);
declare variable TAX_AMORT_CRATE numeric(15,2);
declare variable FIN_AMORT_CRATE numeric(15,2);
declare variable TAX_AMORT_CHANGE_DATE timestamp;
declare variable FIN_AMORT_CHANGE_DATE timestamp;
declare variable TAX_AMORT_METH_TYPE integer;
declare variable TAX_AMORT_METH_MONTH integer;
declare variable FIN_AMORT_METH_TYPE integer;
declare variable FIN_AMORT_METH_MONTH integer;
declare variable TAX_AMORT numeric(15,2);
declare variable FIN_AMORT numeric(15,2);
declare variable TA numeric(15,2);
declare variable FA numeric(15,2);
declare variable TTYPE smallint;
declare variable FTYPE smallint;
declare variable REF integer;
declare variable COMPANY integer;
declare variable EX_MONTH integer;
declare variable EX_MONTH2 integer;
declare variable EX_MONTHCORR numeric(14,2);
declare variable TAX_AMORT_MONTH numeric(14,2);
declare variable FIN_AMORT_MONTH numeric(14,2);
declare variable E_MONTH integer;
declare variable E_MONTHS integer;
declare variable LAST_MONTH integer;
declare variable FIRST_MONTH integer;
begin
  acc_tax_redemption = 0;
  acc_fin_redemption = 0;

  -- znaduje rok, okres w AMPERIOD
  select AMYEAR, AMMONTH, company
    from AMPERIODS where REF=:AMPERIOD
    into :a_year, :a_month, :company;

  -- usuwam poprzednio obliczona amortyzacje
  delete from AMORTIZATION
    where FXDASSET=:FXDASSET and AMPERIOD=:AMPERIOD and coalesce(correction,0) = 0;
  --jesli okres jest wykluczony, to koncze wyliczenia
  if(exists(select REF from DQFAMPERIODS D where d.fxdasset  = :fxdasset and d.amperiod = :amperiod)) then
    exit;

  -- odczytuje poczatkowa wartosc srodka trwalego i stope amortyzacji
  -- odczytuje date wprowadzenia do exploatacji z FXDASSETS
  select TVALUE, TREDEMPTION, TARATE, TACRATE, AM1.typ, AM1.ammonth,
         FVALUE, FREDEMPTION, FARATE, FACRATE, AM2.typ, AM2.ammonth,
         USINGDT, extract(year from USINGDT), extract(month from USINGDT)
    from FXDASSETS FA
      left join amortmeth AM1 on (AM1.symbol=FA.tameth)
      left join amortmeth AM2 on (AM2.symbol=FA.fameth)
    where ref = :FXDASSET
    into :tax_value, :tax_redemption, :tax_amort_rate, :tax_amort_crate, :tax_amort_meth_type, :tax_amort_meth_month,
         :fin_value, :fin_redemption, :fin_amort_rate, :fin_amort_crate, :fin_amort_meth_type, :fin_amort_meth_month,
         :using_date, using_date_year, using_date_month;

  ex_month = 0;
  select count(dq.ref)
    from dqfamperiods dq
    left join amperiods am on dq.amperiod = am.ref
    where am.amyear=:a_year and dq.fxdasset = :fxdasset and dq.dqftype = 1
      and (AM.AMYEAR>:using_date_year or (AM.AMYEAR=:using_date_year and AM.AMMONTH>:using_date_month))
    into :ex_month;

  ex_month2 = 0;
  select count(a.ref) from amperiods a
    where A.AMYEAR=:using_date_year and A.AMMONTH<=:using_date_month
      and a.AMYEAR=:a_year
    into :ex_month2;

  e_months = 0;
  select count(dq.ref)
    from dqfamperiods dq
    left join amperiods am on dq.amperiod = am.ref
    where am.amyear=:a_year and dq.fxdasset = :fxdasset and dq.dqftype = 0
      and (AM.AMYEAR>:using_date_year or (AM.AMYEAR=:using_date_year and AM.AMMONTH>:using_date_month))
    into :e_months;

  e_month = 12 - ex_month - e_months - ex_month2 - 1;      --ilosc miesiecy 'zwyklych' - 1
  ex_monthcorr = 12 - ex_month2;
  ex_month = e_month + :ex_month + 1;        -- il. mc do naliczenia



  -- korekta stawki miesieczne w sytuacji gdy srodek zakupiony w danym roku a jest ustawiona amortyzacja sezonowa
  ex_monthcorr = ex_monthcorr/12;

  tax_amort_meth_month = 1 - tax_amort_meth_month;
  fin_amort_meth_month = 1 - fin_amort_meth_month;

  -- przepisanie podstawy do amortyzacji liniowej i jednorazowej
  tax_initial_value = :tax_value;
  fin_initial_value = :fin_value;


  using_date_year = extract(year from using_date);
  using_date_month = extract(month from using_date);


  -- USTALAM KWOTE DOTYCHCZASOWEJ
  tax_amort = 0;
  fin_amort = 0;

  last_month = 12;     --wyliczanie ostatniego niewykluczonego okresu w roku
  while(exists (select 1
          from dqfamperiods dq
          left join amperiods am on dq.amperiod = am.ref
          where am.amyear=:a_year and dq.fxdasset = :fxdasset and am.ammonth=:last_month
            and (AM.AMYEAR>:using_date_year or (AM.AMYEAR=:using_date_year and AM.AMMONTH>:using_date_month))))
  do begin
    last_month = last_month - 1;
  end

  for
    select AMPERIODS.REF, AMPERIODS.AMYEAR, AMPERIODS.AMMONTH
      from AMPERIODS
      left join DQFAMPERIODS on (AMPERIODS.REF=dqfamperiods.amperiod and dqfamperiods.fxdasset = :fxdasset)
      where
        -- wiekszy niz UD
        (AMPERIODS.AMYEAR>:using_date_year or (AMPERIODS.AMYEAR=:using_date_year and AMPERIODS.AMMONTH>=:using_date_month)) and
        -- mniejszy LUB ROWNY niz okres z parametrow
        (AMPERIODS.AMYEAR<:a_year  or (AMPERIODS.AMYEAR=:a_year and AMPERIODS.AMMONTH<=:a_month))
        and (DQFAMPERIODS.REF is null)
        and AMPERIODS.company = :company
      order by amyear, ammonth
      into :period_ref, :period_year, :period_month
  do begin

    select sum(vd.tvalue), sum(vd.tredemption), sum(vd.fvalue), sum(vd.fredemption)
      from valdocs vd
      join vdoctype vt on (vd.doctype = vt.symbol)
     where vd.fxdasset = :fxdasset and vd.operiod = :period_ref
       and coalesce(vt.amcorrection,0) <> 1
      into :tax_value_doc, :tax_redemption_doc, :fin_value_doc, :fin_redemption_doc;

    if (tax_value_doc is not null) then
      tax_value = tax_value + tax_value_doc;
    if (tax_redemption_doc is not null) then
      tax_redemption = tax_redemption + tax_redemption_doc;
    if (fin_value_doc is not null) then
      fin_value = fin_value + fin_value_doc;
    if (fin_redemption_doc is not null) then
      fin_redemption = fin_redemption + fin_redemption_doc;

    ref = null;
    select max(A.ref)
      from amchngmeth A
        join amperiods P1 on (A.operiod = P1.ref)
        join amperiods P2 on (A.amperiod = P2.ref)
      where (P1.AMYEAR<:period_year or (P1.AMYEAR=:period_year and P1.AMMONTH<=:period_month)) and (P2.AMYEAR<:a_year or (P2.AMYEAR=:a_year and P2.AMMONTH<=:a_month)) and fxdasset=:FXDASSET
      into :ref;
    if (ref is not null) then
    begin
      select AM1.typ, AC.tarate, AC.tacrate, AC.tachdate,
             AM2.typ, AC.farate, AC.facrate, AC.fachdate
        from amchngmeth AC
        left join amortmeth AM1 on (AM1.symbol=AC.tameth)
        left join amortmeth AM2 on (AM2.symbol=AC.fameth)
        where ref=:ref
        into :tax_amort_meth_type, :tax_amort_rate, :tax_amort_crate, :tax_amort_change_date,
             :fin_amort_meth_type, :fin_amort_rate, :fin_amort_crate, :fin_amort_change_date;
    end

    tax_amort_rate = coalesce(tax_amort_rate, 0);
    fin_amort_rate = coalesce(fin_amort_rate, 0);


    first_month = 1;     --wyliczanie ostatniego niewykluczonego okresu w roku
    while(exists (select 1
          from dqfamperiods dq
          left join amperiods am on dq.amperiod = am.ref
          where am.amyear=:a_year and dq.fxdasset = :fxdasset and am.ammonth=:first_month
            and (AM.AMYEAR>:using_date_year or (AM.AMYEAR=:using_date_year and AM.AMMONTH>:using_date_month))))
    do begin
    first_month = first_month + 1;
    end
    if (period_month=first_month) then
    begin
      acc_tax_redemption = tax_redemption + tax_amort;
      acc_fin_redemption = fin_redemption + fin_amort;
    end

    --*********** AMORTYZACJA PODATKOWA **************
    if (tax_amort_meth_type=0) then
    -- LINIOWA
    begin
      if ((using_date_year*12+using_date_month)-(period_year*12+period_month)<=-tax_amort_meth_month) then
      begin
        if (:period_month = :last_month and :period_ref = :amperiod) then begin
          --obliczanie dla ostatniego miesiąca roku - nowa metoda liczenia z castowaniem wartosci rocznej amortyzacji do 2 miejsc po przecinku
          tax_amort = tax_amort + (cast( cast((tax_value * tax_amort_rate/100.00 * tax_amort_crate) as numeric(14,2)) / 12.00
              * (12 - iif(:using_date_year = :period_year, :using_date_month,0) - e_months) as numeric(14,2))
              - (e_month) * cast((tax_value * tax_amort_rate * tax_amort_crate * ex_month)/(12*100*(e_month+1)) as numeric(14,2)));
        end
        else if (:period_month < :last_month
        and :period_ref = :amperiod
        ) then begin
          --obliczanie dla pozostaych miesiący roku
          tax_amort = tax_amort + cast((tax_value * tax_amort_rate * tax_amort_crate * ex_month)/(12*100*(e_month+1)) as numeric(14,2));
        end
        else begin
             tax_amort_month = 0;
              tax_amort_month = cast(((tax_value-acc_tax_redemption) * tax_amort_rate * tax_amort_crate * ex_month)/(12*100*(e_month+1)) as numeric(14,2));

          --Jezeli w miesiacu wczesniej amortyzacja nie zostala naliczona (np. amortyzacja kwartalna), to nalicz ja teraz
          if (coalesce(tax_amort_month, 0) = 0) then
            tax_amort_month = cast((tax_value * tax_amort_rate * tax_amort_crate * ex_month)/(12*100*(e_month+1)) as numeric(14,2));
          tax_amort = tax_amort + coalesce(tax_amort_month, 0);
        end
      end
      ttype = 0;
      if (tax_amort>tax_value-tax_redemption) then
        tax_amort = tax_value-tax_redemption;

    end else
    if (tax_amort_meth_type=1) then
    -- DEGRESYWNA
    begin
      if ((using_date_year*12+using_date_month)-(period_year*12+period_month)<=-tax_amort_meth_month) then
      begin
        if (((tax_value-acc_tax_redemption) * tax_amort_rate * tax_amort_crate) > (tax_value * tax_amort_rate)) then
        begin
          --tax_amort = tax_amort + ((tax_value-acc_tax_redemption) * tax_amort_rate * tax_amort_crate)/(ex_month*100)*(ex_monthcorr);
          if (:period_month = :last_month and :period_ref = :amperiod) then begin
          --obliczanie dla ostatniego miesiąca roku - nowa metoda liczenia z castowaniem wartosci rocznej amortyzacji do 2 miejsc po przecinku
          tax_amort = tax_amort + (cast( cast(((tax_value-acc_tax_redemption) * tax_amort_rate/100.00 * tax_amort_crate) as numeric(14,2)) / 12.00
              * (12 - iif(:using_date_year = :period_year, :using_date_month,0) - e_months) as numeric(14,2))
              - (e_month) * cast(((tax_value-acc_tax_redemption) * tax_amort_rate * tax_amort_crate * ex_month)/(12*100*(e_month+1)) as numeric(14,2)));
          end
          else if (:period_month < :last_month and :period_ref = :amperiod) then begin
            --obliczanie dla pozostaych miesiący roku
            tax_amort = tax_amort + cast(((tax_value-acc_tax_redemption) * tax_amort_rate * tax_amort_crate * ex_month)/(12*100*(e_month+1)) as numeric(14,2));
          end
          else begin
              tax_amort_month = 0;
            tax_amort_month = cast(((tax_value-acc_tax_redemption) * tax_amort_rate * tax_amort_crate * ex_month)/(12*100*(e_month+1)) as numeric(14,2));
          --Jezeli w miesiacu wczesniej amortyzacja nie zostala naliczona (np. amortyzacja kwartalna), to nalicz ja teraz
            if (coalesce(tax_amort_month, 0) = 0) then
              tax_amort_month = cast(((tax_value-acc_tax_redemption) * tax_amort_rate * tax_amort_crate * ex_month)/(12*100*(e_month+1)) as numeric(14,2));
            tax_amort = tax_amort + coalesce(tax_amort_month, 0);
          end
          ttype = 1;
        end else
        begin
          --tax_amort = tax_amort + (tax_value * tax_amort_rate)/(ex_month*100)*(ex_monthcorr);
          if (tax_value * tax_amort_rate/100 < (tax_value-acc_tax_redemption)) then
            t_value = tax_value * tax_amort_rate/100;
          else
            t_value = tax_value-acc_tax_redemption;
          if (:period_month = :last_month and :period_ref = :amperiod) then begin
          --obliczanie dla ostatniego miesiąca roku - nowa metoda liczenia z castowaniem wartosci rocznej amortyzacji do 2 miejsc po przecinku
          tax_amort = tax_amort + (cast( cast((t_value) as numeric(14,2)) / 12.00
              * (12 - iif(:using_date_year = :period_year, :using_date_month,0) - e_months) as numeric(14,2))
              - (e_month) * cast((t_value * ex_month)/(12*(e_month+1)) as numeric(14,2)));
          end
          else if (:period_month < :last_month and :period_ref = :amperiod) then begin
            --obliczanie dla pozostaych miesiący roku
            tax_amort = tax_amort + cast((t_value * ex_month)/(12*(e_month+1)) as numeric(14,2));
          end
          else begin
              tax_amort_month = 0;
            tax_amort_month = cast((t_value * ex_month)/(12*(e_month+1)) as numeric(14,2));
            --Jezeli w miesiacu wczesniej amortyzacja nie zostala naliczona (np. amortyzacja kwartalna), to nalicz ja teraz
            if (coalesce(tax_amort_month, 0) = 0) then
              tax_amort_month = cast((t_value * ex_month)/(12*(e_month+1)) as numeric(14,2));
            tax_amort = tax_amort + coalesce(tax_amort_month, 0);
          end
          ttype = 0;
        end
      end  else
      begin
        ttype = 1;
        tax_amort = 0;
      end
      if (tax_amort>tax_value-tax_redemption) then
        tax_amort = tax_value-tax_redemption;
    end else
    if (tax_amort_meth_type=2) then
    -- JEDNORAZOWA
    begin
--      if ((using_date_year*12+using_date_month)-(period_year*12+period_month) + 1 <=tax_amort_meth_month) then
      if ((using_date_year*12+using_date_month)-(period_year*12+period_month)<=-tax_amort_meth_month) then
        tax_amort = tax_value;
      ttype = 2;
    end


    --*********** AMORTYZACJA FINANSOWA **************
    if (fin_amort_meth_type=0) then
    -- LINIOWA
    begin
      if ((using_date_year*12+using_date_month)-(period_year*12+period_month)<=-fin_amort_meth_month) then
      begin
        if (:period_month = :last_month and :period_ref = :amperiod) then begin
          --obliczanie dla ostatniego miesiąca roku - nowa metoda liczenia z castowaniem wartosci rocznej amortyzacji do 2 miejsc po przecinku
          fin_amort = fin_amort + (
              cast( cast((fin_value * fin_amort_rate/100.00 * fin_amort_crate) as numeric(14,2)) / 12.00 * (12 - iif(:using_date_year = :period_year, :using_date_month,0) - e_months) as numeric(14,2))
              - (e_month) * cast((fin_value * fin_amort_rate * fin_amort_crate*ex_month)/(12*100*(e_month+1)) as numeric(15,2)));
        end
        else if (:period_month < :last_month and :period_ref = :amperiod) then begin
          --obliczanie dla pozostaych miesiący roku
          fin_amort = fin_amort + cast((fin_value * fin_amort_rate * fin_amort_crate*ex_month)/(12*100*(e_month+1)) as numeric(15,2));
        end
        else begin
          fin_amort_month = 0;
          fin_amort_month = cast((fin_value * fin_amort_rate * fin_amort_crate*ex_month)/(12*100*(e_month+1)) as numeric(15,2));
          --Jezeli w miesiacu wczesniej amortyzacja nie zostala naliczona (np. amortyzacja kwartalna), to nalicz ja teraz
          if (coalesce(fin_amort_month, 0) = 0) then
            fin_amort_month = cast((fin_value * fin_amort_rate * fin_amort_crate*ex_month)/(12*100*(e_month+1)) as numeric(15,2));
          fin_amort = fin_amort + coalesce(fin_amort_month, 0);
        end
      end
      ftype = 0;
      if (fin_amort>fin_value-fin_redemption) then
        fin_amort = fin_value-fin_redemption;
    end else
    if (fin_amort_meth_type=1) then
    -- DEGRESYWNA
    begin
      if ((using_date_year*12+using_date_month)-(period_year*12+period_month)<=-fin_amort_meth_month) then
      begin
        if (((fin_value - acc_fin_redemption) * fin_amort_rate * fin_amort_crate) > (fin_value * fin_amort_rate)) then  --degres.
        begin
          --fin_amort = fin_amort + ((fin_value - acc_fin_redemption) * fin_amort_rate * fin_amort_crate)/(ex_month*100)*(ex_monthcorr);
          if (:period_month = :last_month and :period_ref = :amperiod) then begin
          --obliczanie dla ostatniego miesiąca roku - nowa metoda liczenia z castowaniem wartosci rocznej amortyzacji do 2 miejsc po przecinku
          fin_amort = fin_amort + (
              cast( cast(((fin_value - acc_fin_redemption) * fin_amort_rate/100.00 * fin_amort_crate) as numeric(14,2)) / 12.00 * (12 - iif(:using_date_year = :period_year, :using_date_month,0) - e_months) as numeric(14,2))
              - (e_month) * cast(((fin_value - acc_fin_redemption) * fin_amort_rate * fin_amort_crate*ex_month)/(12*100*(e_month+1)) as numeric(15,2)));
          end
          else if (:period_month < :last_month and :period_ref = :amperiod) then begin
            --obliczanie dla pozostaych miesiący roku
            fin_amort = fin_amort + cast(((fin_value - acc_fin_redemption) * fin_amort_rate * fin_amort_crate*ex_month)/(12*100*(e_month+1)) as numeric(15,2));
          end
          else begin
            fin_amort_month = 0;
            fin_amort_month = cast(((fin_value - acc_fin_redemption) * fin_amort_rate * fin_amort_crate*ex_month)/(12*100*(e_month+1)) as numeric(15,2));
            --Jezeli w miesiacu wczesniej amortyzacja nie zostala naliczona (np. amortyzacja kwartalna), to nalicz ja teraz
            if (coalesce(fin_amort_month, 0) = 0) then
              fin_amort_month = cast(((fin_value - acc_fin_redemption) * fin_amort_rate * fin_amort_crate*ex_month)/(12*100*(e_month+1)) as numeric(15,2));
            fin_amort = fin_amort + coalesce(fin_amort_month, 0);
          end
          ftype = 1;
        end else  --zm. na lin.
        begin
          --fin_amort = fin_amort + (fin_value * fin_amort_rate)/(ex_month*100)*(ex_monthcorr);
          if (fin_value * fin_amort_rate/100 < fin_value - acc_fin_redemption) then
            f_value = fin_value * fin_amort_rate/100;
          else
            f_value = fin_value - acc_fin_redemption;
          if (:period_month = :last_month and :period_ref = :amperiod) then begin
          --obliczanie dla ostatniego miesiąca roku - nowa metoda liczenia z castowaniem wartosci rocznej amortyzacji do 2 miejsc po przecinku
          fin_amort = fin_amort + (
              cast( cast((f_value) as numeric(14,2)) / 12.00 * (12 - iif(:using_date_year = :period_year, :using_date_month,0) - e_months) as numeric(14,2))
              - (e_month) * cast((f_value * ex_month)/(12*(e_month+1)) as numeric(15,2)));
          end
          else if (:period_month < :last_month and :period_ref = :amperiod) then begin
            --obliczanie dla pozostaych miesiący roku
            fin_amort = fin_amort + cast((f_value *ex_month)/(12*(e_month+1)) as numeric(15,2));
          end
          else begin
            fin_amort_month = 0;
            fin_amort_month = cast((f_value * ex_month)/(12*(e_month+1)) as numeric(15,2));
            --Jezeli w miesiacu wczesniej amortyzacja nie zostala naliczona (np. amortyzacja kwartalna), to nalicz ja teraz
            if (coalesce(fin_amort_month, 0) = 0) then
              fin_amort_month = cast((f_value * ex_month)/(12*(e_month+1)) as numeric(15,2));
            fin_amort = fin_amort + coalesce(fin_amort_month, 0);
          end
          ftype = 0;
        end
      end else
      begin
        ftype = 1;
        fin_amort = 0;
      end
      if (fin_amort>fin_value-fin_redemption) then
        fin_amort = fin_value-fin_redemption;
    end else
    if (fin_amort_meth_type=2) then
    -- JEDNORAZOWA
    begin
--      if ((using_date_year*12+using_date_month)-(period_year*12+period_month)+1<=fin_amort_meth_month) then
      if ((using_date_year*12+using_date_month)-(period_year*12+period_month)<=-fin_amort_meth_month) then
        fin_amort = fin_value;
      ftype = 2;
    end
  end

 -- sprawdzam czy zostalo cos do umorzenia
  if (tax_amort>tax_value-tax_redemption) then
    tax_amort = tax_value-tax_redemption;
  if (fin_amort>fin_value-fin_redemption) then
    fin_amort = fin_value-fin_redemption;
  select sum(tamort), sum(famort)
    from amortization
    where fxdasset = :fxdasset and (AMYEAR<:a_year or (AMYEAR=:a_year and AMMONTH<=:a_month))
    into :ta, :fa;

  if (ta is null) then
    ta = 0;
  if (fa is null) then
    fa = 0;

  tax_amort = tax_amort - ta;
  fin_amort = fin_amort - fa;

  -- zapisuje obliczona amortyzacje
  if (tax_amort<>0 or fin_amort<>0) then
    insert into AMORTIZATION (fxdasset, amyear, amperiod, tamort, ttype, tarate, famort, ftype, farate)
      values (:FXDASSET, :a_year, :AMPERIOD, :tax_amort, :ttype, :tax_amort_rate, :fin_amort, :ftype, :fin_amort_rate);

end^
SET TERM ; ^
