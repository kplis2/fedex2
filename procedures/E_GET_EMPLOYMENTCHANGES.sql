--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_EMPLOYMENTCHANGES(
      EMPLOYEE EMPLOYEES_ID,
      RFROMDATE date,
      RTODATE date,
      RGR_FIELDS varchar(100) CHARACTER SET UTF8                           )
  returns (
      FROMDATE date,
      TODATE date,
      DIMNUM smallint,
      DIMDEN smallint,
      WORKDIM float,
      SALARY numeric(14,2),
      PROPORTIONAL smallint,
      CADDSALARY numeric(14,2),
      FUNCSALARY numeric(14,2),
      DAYWORKDIM integer,
      PAYMENTTYPE smallint,
      WORKPOST EDICTWORKPOSTS_ID)
   as
declare variable TFROMDATE timestamp;
declare variable TTODATE timestamp;
declare variable TWORKDIM float;
declare variable TDIMDEN smallint;
declare variable TDIMNUM smallint;
declare variable TPROPORTIONAL smallint;
declare variable TSALARY numeric(14,2);
declare variable TCADDSALARY numeric(14,2);
declare variable TFUNCSALARY numeric(14,2);
declare variable TDAYWORKDIM integer;
declare variable EFROMDATE timestamp;
declare variable CTXTODATESTR varchar(20);
declare variable CTXTODATE timestamp;
declare variable TPAYMENTTYPE smallint;
declare variable tworkpost edictworkposts_id;
begin
/*MWr Personel: procedura pozwala podzielic/pogrupowac przebieg zatrudnienia pracownika
  EMPLOYEE, w zadanym okresie RFROMDATE - RTODATE, z uwagi na zmiane wybranych pol
  wyszczegolnionych w RGR_FIELDS jako: ';Field_1;Field_2;...;Fileld_N;', kolejnosc pol
  dowolna. Daty Od-Do nie sa wymagane, a podane, stanowia granice czasowe wyniku.*/

  rgr_fields = upper(trim(coalesce(rgr_fields,'')));
  if (rgr_fields > '') then
  begin
    if (rgr_fields not like ';%;') then
      rgr_fields = ';' || trim(';' from rgr_fields) || ';';

    if (strmulticmpand(:rgr_fields, upper(';WORKDIM;PROPORTIONAL;SALARY;CADDSALARY;FUNCSALARY;DAYWORKDIM;PAYMENTTYPE;WORKPOST;')) = 0) then  --BS81781
      exception universal 'Nieznany parametr z listy: ' || rgr_fields;
  end

  ctxtodatestr = rdb$get_context('USER_TRANSACTION', 'RPT_WORKREFERENCE_TODATE');  --PR54992
  if (ctxtodatestr <> '') then ctxtodate = cast(ctxtodatestr as timestamp);

  for
    select case when e.fromdate <= m.fromdate and (m.fromdate <= e.todate or e.todate is null) then m.fromdate else e.fromdate end,
        case when e.fromdate <= cast(m.todate as timestamp) and (cast(m.todate as timestamp) <= e.todate or e.todate is null) then cast(m.todate as timestamp) else e.todate end,
        dimnum, dimden, coalesce(workdim,0),
        coalesce(proportional,0), coalesce(salary,0), coalesce(caddsalary,0),
        coalesce(funcsalary,0), coalesce(c.norm_per_day,28800), e.fromdate,
        paymenttype, workpost --BS81781 
      from employment e
        left join emplcalendar m on (m.employee = e.employee
                                   and (e.todate is null or m.fromdate <= e.todate)
                                   and (m.todate is null or m.todate >= e.fromdate))
        left join ecalendars c on (c.ref = m.calendar)
      where e.employee = :employee
        and (:rtodate is null or e.fromdate <= :rtodate)
        and (:rfromdate is null or e.todate is null or e.todate >= :rfromdate)
        and (:ctxtodate is null or e.fromdate <= :ctxtodate) --PR54992
        and (m.ref is null --BS59098
          or ((m.todate is null or m.todate >= :rfromdate or :rfromdate is null) --BS53146
            and (m.fromdate <= :rtodate or :rtodate is null))) --BS53146
      order by e.fromdate, m.fromdate
      into :tfromdate, :ttodate, :tdimnum, :tdimden, :tworkdim,
        :tproportional, :tsalary, :tcaddsalary, :tfuncsalary, :tdayworkdim, :efromdate,
        :tpaymenttype, :tworkpost
  do begin
    if (tfromdate < rfromdate) then tfromdate = rfromdate;
    if (ttodate > rtodate or ttodate is null) then ttodate = rtodate;

    if (fromdate is null) then
    begin
    --wejscie do sekcji gwarantuje ze w EFROMDATE mamy poczatek zatrudnienia pracownika
      if(rfromdate is null or rfromdate < efromdate) then
        fromdate = efromdate; --BS53146
      else
        fromdate = tfromdate;
    end else
      if ((rgr_fields containing ';WORKDIM;' and workdim <> tworkdim)
        or (rgr_fields containing ';PROPORTIONAL;' and proportional <> tproportional)
        or (rgr_fields containing ';SALARY;' and salary <> tsalary)
        or (rgr_fields containing ';CADDSALARY;' and caddsalary <> tcaddsalary)
        or (rgr_fields containing ';FUNCSALARY;' and funcsalary <> tfuncsalary)
        or (rgr_fields containing ';DAYWORKDIM;' and dayworkdim <> tdayworkdim) --dzienna/dobowa norma czasu pracy
        or (rgr_fields containing ';PAYMENTTYPE;' and paymenttype <> tpaymenttype)
        or (rgr_fields containing ';WORKPOST;' and workpost <> tworkpost)
      ) then begin

        suspend;
        fromdate = tfromdate;
      end

    todate = ttodate;
    workdim = tworkdim;
    dimnum = tdimnum;
    dimden = tdimden;
    proportional = tproportional;
    salary = tsalary;
    caddsalary = tcaddsalary;
    funcsalary = tfuncsalary;
    dayworkdim = tdayworkdim;
    paymenttype = tpaymenttype;
    workpost = tworkpost;
  end
  if (fromdate is not null) then
    suspend;
end^
SET TERM ; ^
