--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_ADD_FEDEX_SHIP_REQUEST(
      SHIPPINGDOC integer)
  returns (
      EDEDOCHREF integer)
   as
declare variable EDERULE integer;
declare variable SPEDRESPONSE varchar(255);
begin
  select spedresponse from listywysd where ref = :shippingdoc
  into :spedresponse;
  if (coalesce(:spedresponse,'') = '1') then
    exception universal 'Dokument został już wysłany. Anuluj wysyłkę i nadaj ją jeszcze raz.';

  select es.ref
    from ederules es
    where es.symbol = 'EDE_FEDEX_SHIPMENT'
    into : ederule;

  insert into ededocsh( ederule, direction, createdate, otable, oref, status, filename, manualinit)
    values(:ederule, 1, current_timestamp, 'LISTYWYSD', :shippingdoc, 0, :shippingdoc||'exp',0)
    returning ref into :ededochref;

  suspend;
end^
SET TERM ; ^
