--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_MWSCONSTLOC_CHECK(
      MWSCONSTLOCSIN varchar(40) CHARACTER SET UTF8                           ,
      WHIN varchar(3) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint,
      MWSCONSTLOC integer,
      MSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable quantity numeric(14,4);
declare variable blocked numeric(14,4);
declare variable isbox smallint_id;
declare variable iscart smallint_id;
begin
  status = 1;
  mwsconstloc = null;
  select ref from mwsconstlocs where symbol = :mwsconstlocsin and wh = :whin
    into mwsconstloc;
  if (mwsconstloc is null) then
    status = 0;
  else
  begin
    quantity = null;
    blocked = null;
    isbox = 0;
    iscart = 0;
    select sum(quantity), sum(blocked)
      from mwsstock s where s.mwsconstloc = :mwsconstloc
      into quantity, blocked;
    if (quantity is null) then quantity = 0;
    if (blocked is null) then blocked = 0;
    if (quantity = 0) then
      status = 2;
    else if (quantity = blocked) then
      status = 3;
    --XXX LDz sprawdzanie w koszykach i wozkach
    if (status > 1) then begin
      select first 1 1
       from listywysdroz_opk l
       where l.x_mwsconstlock = :mwsconstlocsin
      into :isbox;

      select first 1 1
        from mwsordcartcolours m
        where m.mwsconstlocsymb = :mwsconstlocsin and m.status <2
      into :iscart;

      if ((coalesce(isbox, 0) + coalesce(iscart, 0) >=1))  then
       status =1;
    end
    --XXX LDz sprawdzanie w koszykach i wozkach
  end
  if (status = 0) then
    msg = 'Lokacja nie należy do magazynu '||whin;
  else if (status = 1) then
    msg = '';
  else if (status = 2) then
    msg = 'Na tej lokacji nie powinno być towaru';
  else if (status = 3) then
    msg = 'Cały towar na tej lokacji jest zablokowany';
end^
SET TERM ; ^
