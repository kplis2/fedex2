--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRCALCCOLS_TREE(
      PRSHCALC integer,
      VALIDTILL timestamp)
  returns (
      REF integer,
      PARENT integer,
      CALC integer,
      PRCOLUMN integer,
      CVALUE numeric(14,2),
      EXPR varchar(256) CHARACTER SET UTF8                           ,
      MODIFIED smallint,
      FROMPRCALCCOL integer,
      KEY1 integer,
      KEY2 integer,
      PREFIX varchar(20) CHARACTER SET UTF8                           ,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      CALCTYPE smallint,
      FROMPRSHMAT integer,
      FROMPRSHOPER integer,
      FROMPRSHTOOL integer,
      ISALT smallint,
      PRSHOPERALT integer,
      PRSHOPERALTDICT varchar(255) CHARACTER SET UTF8                           ,
      EXCLUDED smallint,
      COLOR smallint,
      ICON varchar(40) CHARACTER SET UTF8                           )
   as
declare variable historycvalue numeric(14,2);
declare variable subcalc integer;
declare variable curref integer;
begin
  for select ref, parent, calc, prcolumn, cvalue, expr, modified, fromprcalccol, key1, key2, prefix,
             descript, calctype, fromprshmat, fromprshoper, fromprshtool, isalt, prshoperalt,
             excluded, color, icon
      from prcalccols
      where calc=:prshcalc
      into :ref, :parent, :calc, :prcolumn, :cvalue, :expr, :modified, :fromprcalccol, :key1, :key2, :prefix,
           :descript, :calctype, :fromprshmat, :fromprshoper, :fromprshtool, :isalt, :prshoperalt,
           :excluded, :color, :icon
  do
  begin
    -- pobierz wartosc z historii zmian
    if(:validtill is not null) then begin
      historycvalue = null;
      select first 1 cvalue from prcalccolslog where prcalccol=:ref and validtill>:validtill
      order by prcalccol,validtill
      into :historycvalue;
      if(:historycvalue is not null) then cvalue = :historycvalue;
    end
    -- ustal opis operacji alternatywnej
    prshoperaltdict = '';
    if(:prshoperalt is not null) then begin
      select descript from prshopers where ref=:prshoperalt into :prshoperaltdict;
    end

    suspend;

    -- pokaz kalkulacje podrzedna
    if(:fromprcalccol is not null) then begin
      curref = :ref;
      select calc from prcalccols where ref=:fromprcalccol into :subcalc;
      for select ref, parent, calc, prcolumn, cvalue, expr, modified, fromprcalccol, key1, key2, prefix,
             descript, calctype, fromprshmat, fromprshoper, fromprshtool, isalt, prshoperalt,
             excluded, color, icon
      from prcalccols_tree(:subcalc, :validtill)
      into :ref, :parent, :calc, :prcolumn, :cvalue, :expr, :modified, :fromprcalccol, :key1, :key2, :prefix,
           :descript, :calctype, :fromprshmat, :fromprshoper, :fromprshtool, :isalt, :prshoperalt,
           :excluded, :color, :icon
      do begin
        if(:parent is null) then parent = :curref;
        suspend;
      end
    end

  end
end^
SET TERM ; ^
