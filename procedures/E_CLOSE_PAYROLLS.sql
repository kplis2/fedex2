--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_CLOSE_PAYROLLS(
      CPERIOD char(6) CHARACTER SET UTF8                           ,
      CURRENTCOMPANY integer)
   as
begin

  update epayrolls set status = 1
    where cper = :cperiod and status = 0 and company = :currentcompany;

  execute procedure e_calculate_sbase(null,cperiod,cperiod,currentcompany);
  execute procedure e_calculate_vacbase(null,cperiod,cperiod,currentcompany);

  delete from ecolldedinstals  --PR55393
    where insvalue = 0 and payroll in (select ref from epayrolls
       where company = :currentcompany and cper = :cperiod and status = 1);
end^
SET TERM ; ^
