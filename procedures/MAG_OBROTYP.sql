--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_OBROTYP(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      CENA numeric(14,2),
      DOSTAWA integer,
      DATA timestamp)
  returns (
      RKTM varchar(40) CHARACTER SET UTF8                           ,
      RWERSJA integer,
      RCENA numeric(14,2),
      RDOSTAWA integer,
      STAN numeric(14,4),
      WARTOSC numeric(14,2),
      STMIN numeric(14,4),
      WARTMIN numeric(14,2))
   as
declare variable rref integer;
declare variable wydania smallint;
declare variable cenanew numeric(14,2);
begin
    for select DOKUMROZ.REF, DOKUMPOZ.KTM, DOKUMPOZ.WERSJA, DOKUMROZ.PCENA, DOKUMROZ.dostawa
        ,DOKUMROZ.ilosc, DOKUMROZ.PWARTOSC, DOKUMROZ.ILOSC * DEFDOKUM.WYDANIA, DOKUMROZ.PWARTOSC * DEFDOKUM.WYDANIA
        ,DEFDOKUM.WYDANIA
        from DOKUMROZ
        join DOKUMPOZ on( DOKUMROZ.pozycja = DOKUMPOZ.ref)
        join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.dokument)
        join DEFDOKUM on ( DOKUMNAG.typ = DEFDOKUM.symbol)
        where DOKUMNAG.MAGAZYN=:MAGAZYN and ((DOKUMPOZ.KTM = :KTM) or (:ktm is null)) and ((DOKUMPOZ.WERSJA = :wersja) or (:wersja is null))
              and (DOKUMNAG.AKCEPT =1 or DOKUMNAG.AKCEPT=8) AND DOKUMNAG.data <= :DATA and (DOKUMROZ.pcena = :cena or (:cena is null)) and (DOKUMROZ.DOSTAWA = :dostawa or (:dostawa is null))
        order by
           DOKUMPOZ.KTM, DOKUMPOZ.WERSJA, DOKUMROZ.DOSTAWA
        into :rref, :rktm, :rwersja, :rcena, :rdostawa
             ,:stan, :wartosc, :stmin, :wartmin
             ,:wydania
    do begin
      cenanew = null;
      select first 1 DOKUMNOTP.CENANEW
      from DOKUMNOTP left join DOKUMNOT on (DOKUMNOT.REF=DOKUMNOTP.DOKUMENT)
      where DOKUMNOTP.DOKUMROZKOR=:rref and
        ((DOKUMNOT.AKCEPT=1) or (DOKUMNOT.AKCEPT=8)) and DOKUMNOT.DATA<= :DATA
      order by DOKUMNOT.DATA DESC, DOKUMNOT.REF DESC
      into :cenanew;
      if(:cenanew is not null) then begin
        rcena = :cenanew;
        wartosc = :stan*:cenanew;
        wartmin = :wartosc*:wydania;
      end
      suspend;
    end
end^
SET TERM ; ^
