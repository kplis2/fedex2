--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ADD_BKACCOUNT_FROM_PATTERN(
      PATTERN_COMP COMPANIES_ID,
      DIST_COMP COMPANIES_ID,
      BKACC BKACCOUNTS_ID)
   as
declare variable CURRENT_COMPANY COMPANIES_ID;
begin
  --procedura dodająca konto z kompany wzorcowego

  execute procedure get_global_param('CURRENTCOMPANY')
    returning_values :current_company;

  --Sprawdzamy, czy taki wzorzec istnieje
  if (not exists (select first 1 1
                    from bkaccounts b
                    where b.ref = :bkacc
                      and b.company = :pattern_comp) ) then
  begin
    exception brak_kona_we_wzorcu;
  end

  -- czy czasami nie chcemy kopiować z zakładu wzorcowego do wzorcowego
  -- raczej niemozliwe z interfacu, ale jakby ktos taki automat wymyslil jakims
  -- cudem.
  if (dist_comp is not distinct from pattern_comp) then
    exception add_bkacc_from_pattern_compan;

  --Kopiujemy konto z wzorca do company docelowego. Kopiujemy wszystko poza refem, company, oraz pattern_ref
  --Po dodaniu konta, na trigerach na bkaccount będziemy trzepać accstructure ( na BKACCOUNTS_AI_WPK )
  insert into bkaccounts (company, yearid, symbol, descript, bktype, saldotype,
    settl, multicurr, sttltype, prd, multiprd, rights, rightsgroup,
    doopisu, pattern_ref,matchable, matchingslodef)
  select :dist_comp, b.yearid, b.symbol, b.descript, b.bktype, b.saldotype,
    b.settl, b.multicurr, b.sttltype, b.prd, b.multiprd, b.rights, b.rightsgroup,
    b.doopisu, b.ref, b.matchable, b.matchingslodef
    from bkaccounts b
    where b.ref = :bkacc and b.company = :pattern_comp;

end^
SET TERM ; ^
