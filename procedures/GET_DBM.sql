--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_DBM(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      ZAMOWIENIE integer)
  returns (
      LP integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      WERSJAREF integer,
      RMAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      RMAG2 varchar(3) CHARACTER SET UTF8                           ,
      DOSTAWCA integer,
      NAZWAT varchar(100) CHARACTER SET UTF8                           ,
      MIARA varchar(5) CHARACTER SET UTF8                           ,
      ILOSCWOPAK varchar(20) CHARACTER SET UTF8                           ,
      STAN numeric(14,4),
      ZAREZERW numeric(14,4),
      ZABLOKOW numeric(14,4),
      ZAMOWIONO numeric(14,4),
      WOLNE numeric(14,4),
      NAGZAMREF integer,
      POZZAMREF integer,
      ILOSCNAZAM numeric(14,4),
      STANMAX numeric(14,4),
      ITCLASS smallint,
      ROICLASS smallint,
      KOLOR smallint,
      ZAMOWIC numeric(14,4))
   as
declare variable zamzewn smallint;
begin
  lp = 0;
  iloscnazam = NULL;
  nagzamref = :zamowienie;
  -- okresl magazyny zamowienia
  -- dla zamowien do dostawcow RMAGAZYN okresla magazyn docelowy
  -- dla zamowien wewn. wydania RMAGAZYN okresla magazyn zrodlowy a RMAG2 - magazyn docelowy
  rmagazyn = '';
  rmag2 = '';
  select MAGDOST from DEFMAGAZ where SYMBOL=:magazyn into :rmagazyn;
  if(:rmagazyn<>'') then zamzewn = 0; else zamzewn = 1;
  if(:zamzewn=1) then rmagazyn = :magazyn;
  else rmag2 = :magazyn;
  for select STANYIL.ktm, STANYIL.wersja, STANYIL.wersjaref,
   STANYIL.ilosc,
   STANYIL.nazwat, STANYIL.miara, TOWARY.iloscwopak,
   STANYIL.zarezerw, STANYIL.zablokow, STANYIL.zamowiono,
   STANYIL.ilosc-STANYIL.zarezerw-STANYIL.zablokow+STANYIL.zamowiono,
   STANYIL.stanmax, STANYIL.itclass, STANYIL.roiclass, STANYIL.kolor
  from STANYIL
  left join TOWARY on (TOWARY.ktm=STANYIL.ktm)
  where STANYIL.MAGAZYN = :magazyn and TOWARY.supplymethod=3
  and STANYIL.ilosc<STANYIL.stanmax
  order by STANYIL.ktm,STANYIL.WERSJA
  into :ktm, :wersja, :wersjaref,
   :stan,
   :nazwat, :miara, :iloscwopak,
   :zarezerw, :zablokow, :zamowiono,
   :wolne,
   :stanmax, :itclass, :roiclass, :kolor
  do begin
    dostawca = NULL;
    if(:zamzewn=1) then begin
      select dostawca from dostcen where dostcen.wersjaref = :wersjaref AND dostcen.gl = 1 into :dostawca;
    end
    pozzamref = NULL;
    iloscnazam = NULL;
    if(:stanmax is null) then stanmax = 0;
    if(:zamowienie is not null and :zamowienie<>0) then begin
      select min(POZZAM.REF), sum(POZZAM.ILOSC) from POZZAM
      where (POZZAM.ZAMOWIENIE=:zamowienie) and (POZZAM.MAGAZYN=:magazyn) and
            (POZZAM.WERSJAREF=:wersjaref)
      into :pozzamref, :iloscnazam;
    end
    execute procedure DIGITIZE(:stanmax-:stan-:zamowiono,:iloscwopak,NULL) returning_values :zamowic;
    lp = :lp + 1;
    suspend;
  end
end^
SET TERM ; ^
