--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SAVEASTEMPLET(
      BKDOC integer,
      TEMPLET integer)
   as
declare variable DECREE integer;
declare variable NUMBER integer;
declare variable ACCOUNT ACCOUNT_ID;
declare variable SIDE smallint;
declare variable DEBIT numeric(14,2);
declare variable CREDIT numeric(14,2);
declare variable SETTLEMENT varchar(20);
declare variable DESCRIPT varchar(80);
declare variable DIST1DEF integer;
declare variable DIST1SYMBOL SYMBOLDIST_ID;
declare variable DIST2DEF integer;
declare variable DIST2SYMBOL SYMBOLDIST_ID;
declare variable DIST3DEF integer;
declare variable DIST3SYMBOL SYMBOLDIST_ID;
declare variable DIST4DEF integer;
declare variable DIST4SYMBOL SYMBOLDIST_ID;
declare variable DIST5DEF integer;
declare variable DIST5SYMBOL SYMBOLDIST_ID;
declare variable DIST6DEF integer;
declare variable DIST6SYMBOL SYMBOLDIST_ID;
declare variable DCTEMPLETPOS integer;
begin
  for select ref, number, account, side, debit, credit, settlement, descript, dist1ddef, dist1symbol, dist2ddef, dist2symbol, dist3ddef, dist3symbol, dist4ddef, dist4symbol, dist5ddef, dist5symbol, dist6ddef, dist6symbol
    from decrees
    where bkdoc=:BKDOC
    order by number
    into :decree, :number, :account, :side, :debit, :credit, :settlement, :descript, :dist1def, :dist1symbol, :dist2def, :dist2symbol, :dist3def, :dist3symbol, :dist4def, :dist4symbol, :dist5def, :dist5symbol, :dist6def, :dist6symbol
  do begin
    execute procedure GEN_REF('DCTEMPLETPOS') returning_values :dctempletpos;
    insert into dctempletpos (ref, dctemplet, number, account, side, debit, credit, settlement, descript, dist1def, dist1symbol, dist2def, dist2symbol, dist3def, dist3symbol, dist4def, dist4symbol, dist5def, dist5symbol, dist6def, dist6symbol)
      values(:dctempletpos, :templet, :number, :account, :side, :debit, :credit, :settlement, :descript, :dist1def, :dist1symbol, :dist2def, :dist2symbol, :dist3def, :dist3symbol, :dist4def, :dist4symbol, :dist5def, :dist5symbol, :dist6def, :dist6symbol);
    if(dist1def is null and dist2def is null and dist3def is null and dist4def is null and dist5def is null and dist6def is null) then
      insert into dctempletposdist (dctempletpos, debit, credit, descript, dist1def, dist1symbol, dist2def, dist2symbol, dist3def, dist3symbol, dist4def, dist4symbol, dist5def, dist5symbol, dist6def, dist6symbol)
        select :dctempletpos, debit, credit, descript, tempdictdef, tempsymbol, tempdictdef2, tempsymbol2, tempdictdef3, tempsymbol3, tempdictdef4, tempsymbol4, tempdictdef5, tempsymbol5, tempdictdef6, tempsymbol6
        from distpos
        where decrees = :decree;
  end
end^
SET TERM ; ^
