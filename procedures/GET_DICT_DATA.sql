--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_DICT_DATA(
      SLODEF integer,
      SLOPOZ integer)
  returns (
      KOD varchar(100) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      KONTOFK ANALYTIC_ID,
      ADRES varchar(255) CHARACTER SET UTF8                           ,
      MIASTO varchar(255) CHARACTER SET UTF8                           ,
      ULICA varchar(255) CHARACTER SET UTF8                           ,
      NIP varchar(40) CHARACTER SET UTF8                           )
   as
declare variable typ varchar(10);
begin
  kontofk = '';
  adres = '';

  select lower(TYP) from SLODEF
    where ref = :SLODEF
  into :typ;

  if (:typ = 'klienci')
  then begin
    select fskrot, nazwa, kontofk,
        coalesce(ulica,'')||iif(coalesce(nrdomu,'')<>'',' '||nrdomu,'')
          ||iif(coalesce(nrlokalu,'')<>'', '/'||nrlokalu,'')
          ||', '||coalesce(kodp,'')||' '||coalesce(miasto,''),
        coalesce(kodp,'')||' '||coalesce(miasto,''),
        coalesce(ulica,'')||iif(coalesce(nrdomu,'')<>'',' '||nrdomu,'')
          ||iif(coalesce(nrlokalu,'')<>'','/'||nrlokalu,''), nip
      from klienci
        where ref = :slopoz
      into :kod, :nazwa, :kontofk,
        :adres,
        :miasto,
        :ulica, :nip;
  end
  else if (:typ = 'pklienci')
  then begin
    select skrot, nazwa, coalesce(kodp,'')||' '||coalesce(miasto,''),
        coalesce(ulica,'')||iif(coalesce(nrdomu,'')<>'',' '||nrdomu,'')
          ||iif(coalesce(nrlokalu,'')<>'','/'||nrlokalu,'')
        from pklienci
        where ref = :slopoz
      into :kod, :nazwa, :miasto,
        :ulica;
  end
  else if (:typ = 'dostawcy')
  then begin
    select id, nazwa, kontofk,
        coalesce(ulica,'')||iif(coalesce(nrdomu,'')<>'',' '||nrdomu,'')
          ||iif(coalesce(nrlokalu,'')<>'','/'||nrlokalu,'')
          ||', '||coalesce(kodp,'')||' '||coalesce(miasto,''),
        coalesce(kodp,'')||' '||coalesce(miasto,''),
        coalesce(ulica,'')||iif(coalesce(nrdomu,'')<>'',' '||nrdomu,'')
          ||iif(coalesce(nrlokalu,'')<>'','/'||nrlokalu,''), nip
      from dostawcy
      where ref = :slopoz
      into :kod, :nazwa, :kontofk,
        :adres,
        :miasto,
        :ulica, :nip;
  end
  else if (:typ = 'sprzedawcy')
  then begin
    select nazwa, nazwa, kontofk, coalesce(kodpoczt,'')||' '||coalesce(miasto,''),
        coalesce(adres,'')||iif(coalesce(nrdomu,'')<>'',' '||nrdomu,'')
          ||iif(coalesce(nrlokalu,'')<>'','/'||nrlokalu,'')
      from sprzedawcy
      where ref = :slopoz
      into :kod, :nazwa, :kontofk, :miasto,
        :ulica;
  end
  else if (:typ = 'odbiorcy')
  then begin
    select nazwa, nazwafirmy, coalesce(dkodp,'')||' '||coalesce(dmiasto,''),
        coalesce(dulica,'')||iif(coalesce(dnrdomu,'')<>'',' '||dnrdomu,'')
          ||iif(coalesce(dnrlokalu,'')<>'','/'||dnrlokalu,'')
      from odbiorcy
      where ref = :slopoz
      into :kod, :nazwa, :miasto,
        :ulica;
  end
  else if (:typ = 'esystem')
  then begin
    select kod, nazwa, kontoks
      from slopoz
      where slownik = :slodef
        and ref = :slopoz
      into :kod, :nazwa, :kontofk;
  end
  suspend;
  if (:nip > '') then
  begin
    nip = replace(:nip,' ','');
    nip = replace(:nip,'-','');
  end
end^
SET TERM ; ^
