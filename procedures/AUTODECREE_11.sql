--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_11(
      BKDOC integer)
   as
declare variable amount numeric(14,2);
declare variable account varchar(20);
declare variable descript varchar(100);
declare variable period varchar(6);
declare variable symbol varchar(20);
declare variable department varchar(10);
declare variable eprbill integer;
begin
  -- schemat dekretacji - umowy zlecenia

  select period from bkdocs where ref=:bkdoc
    into :period;

  for
    select PR.ref, E.personnames, C.department, E.symbol
      from epayrolls PR
        join emplcontracts C on (PR.emplcontract = C.ref)
        join employees E on (E.ref = C.employee)
      where PR.empltype = 2 and PR.tper = :period
      into :eprbill, :descript, :department, :symbol
  do begin

    -- koszt
    select sum(pvalue)
      from eprpos
      where ecolumn = 1080 and payroll = :eprbill
      into :amount;

    account = '431';
    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

    account = '490';
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

    account = '???';
    if (department = 'HURTOWNIA') then
      account = '502';
    else if (department = 'KSIĘGOWOŚĆ') then
      account = '550';
    else if (department = 'USŁUGI') then
      account = '501-000';

    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

    -- koszt
    select sum(pvalue)
      from eprpos
      where ecolumn in(8200, 8210, 8220) and payroll = :eprbill
      into :amount;

    account = '440-01';
    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

    select sum(pvalue)
      from eprpos
      where ecolumn in(8810, 8820) and payroll = :eprbill
      into :amount;

    account = '440-02';
    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

    select sum(pvalue)
      from eprpos
      where ecolumn in (8200, 8210, 8220, 8810, 8820) and payroll = :eprbill
      into :amount;

    account = '490';
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

    account = '???';
    if (department = 'HURTOWNIA') then
      account = '502';
    else if (department = 'KSIĘGOWOŚĆ') then
      account = '550';
    else if (department = 'USŁUGI') then
      account = '501-000';

    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

    select sum(pvalue)
      from eprpos
      where ecolumn = 7350 and payroll = :eprbill
      into :amount;

    account = '222-02';
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

    select sum(pvalue)
      from eprpos
      where ecolumn in (6100, 6110, 6120, 8200, 8210, 8220) and payroll = :eprbill
      into :amount;

    account = '227-51';
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

    select sum(pvalue)
      from eprpos
      where ecolumn = 7220 and payroll = :eprbill
      into :amount;

    account = '227-52';
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

    select sum(pvalue)
      from eprpos
      where ecolumn in (8810, 8820) and payroll = :eprbill
      into :amount;

    account = '227-53';
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

    select sum(pvalue)
      from eprpos
      where ecolumn = 7450 and payroll = :eprbill
      into :amount;

    account = '231-' || symbol;
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

  update bkdocs set otable = 'EPRBILLS', oref = :period where ref=:bkdoc;
  update epayrolls set status = 2 where tper = :period and empltype = 2;

  end
end^
SET TERM ; ^
