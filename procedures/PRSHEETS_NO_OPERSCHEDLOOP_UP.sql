--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHEETS_NO_OPERSCHEDLOOP_UP(
      OPER integer)
  returns (
      TEST integer)
   as
declare variable scheduled integer;
declare variable descript varchar(255);
declare variable operup integer;
begin
  operup = null;
  scheduled = null;
  select opermaster, scheduled, descript  from prshopers po where po.ref = :oper  into :operup, :scheduled, :descript;
  if(scheduled is not null and scheduled = 1) then exception prsheets_loop 'Operacja nadrzędna "'||substring(:descript from 1 for 20)||'" jest już harmonogramowana.';
  if(operup is not null and operup > 0) then execute procedure prsheets_no_operschedloop_up(operup) returning_values :test;
end^
SET TERM ; ^
