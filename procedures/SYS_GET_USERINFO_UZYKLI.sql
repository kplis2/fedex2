--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_GET_USERINFO_UZYKLI(
      USR varchar(40) CHARACTER SET UTF8                           ,
      PSW varchar(40) CHARACTER SET UTF8                           ,
      OLDPSW varchar(40) CHARACTER SET UTF8                           ,
      ID varchar(255) CHARACTER SET UTF8                           )
  returns (
      USERID varchar(255) CHARACTER SET UTF8                           ,
      USERLOGIN varchar(255) CHARACTER SET UTF8                           ,
      IMIE varchar(255) CHARACTER SET UTF8                           ,
      NAZWISKO varchar(255) CHARACTER SET UTF8                           ,
      UWAGI varchar(1024) CHARACTER SET UTF8                           ,
      SUPERVISOR smallint,
      SUPERVISEGROUP varchar(255) CHARACTER SET UTF8                           ,
      EMAIL varchar(255) CHARACTER SET UTF8                           ,
      GRUPA varchar(1024) CHARACTER SET UTF8                           ,
      SENTE smallint,
      MODULES varchar(1024) CHARACTER SET UTF8                           ,
      APPLICATIONS varchar(255) CHARACTER SET UTF8                           ,
      OTABLE varchar(40) CHARACTER SET UTF8                           ,
      OREF integer,
      MUST_CHANGE smallint,
      DAYS_TO_CHANGE integer,
      LAST_CHANGE timestamp,
      NAMESPACE varchar(40) CHARACTER SET UTF8                           ,
      USERUUID varchar(40) CHARACTER SET UTF8                           ,
      LICENCELIMIT SMALLINT_ID)
   as
declare variable CRYPTEDPSW varchar(255);
declare variable KLIENT integer;
declare variable ODDZIAL varchar(255);
declare variable COMPANY integer;
begin
  cryptedpsw = :psw;
  -- identyfikacja przez login
  select first 1 u.ref,u.klient,u.imie,u.nazwisko,u.email
  from uzykli u
  where id=:usr and (u.haslo=:cryptedpsw or u.haslo1=:cryptedpsw)
  into :oref,:klient,:imie,:nazwisko,:email;
  if(:klient is null) then begin
    -- identyfikacja przez email
    select first 1 u.ref,u.klient,u.imie,u.nazwisko,u.email
    from uzykli u
    where email=:usr and (u.haslo=:cryptedpsw or u.haslo1=:cryptedpsw)
    into :oref,:klient,:imie,:nazwisko,:email;
  end
  if(:klient is not null) then begin
    select k.oddzial
    from klienci k
    where k.ref=:klient
    into :oddzial;
    userid = :usr;
    userlogin = :usr;
    supervisor = 0;
    supervisegroup = '';
    grupa = '';
    sente = 0;
    modules = 'XXX'; --tutaj nalezy wstawic liste modulow dostepnych jak sie ktos zaloguje jako klient
    applications = ''; -- j.w.
    otable = 'UZYKLI';
    namespace = 'SENTE';
    useruuid = 'c22b93adbed54a93bd153cc2ad2afdcd'; -- tutaj UUID roli predefiniowanej w projekcie SENTE ktora jest rolą dla wszystkich zalogowanych klientów
    -- ustaw globale aby wiadomo bylo jakim klientem jestem
    execute procedure SET_GLOBAL_PARAM('UZYKLI',:oref);
    execute procedure SET_GLOBAL_PARAM('KLIENT',:klient);
  end else begin
    execute procedure SET_GLOBAL_PARAM('UZYKLI','');
    execute procedure SET_GLOBAL_PARAM('KLIENT','');
  end
  if(:id is null) then begin
      -- jesli operator nie ma przypisanego oddzialu to wybierz glowny oddzial
      if(:oddzial is null) then
        select first 1 o.oddzial
        from oddzialy o
        where o.glowny=1
        order by o.company
        into :oddzial;
      if(:oddzial is null) then
        exception universal 'Nie ustalono oddzialu domyslnego w trakcie logowania';
      select o.company
        from oddzialy o
        where o.oddzial = :oddzial
        into :company;
      -- jesli jestesmy w trakcie logowaia to ustaw parametry obowiazkowe
      execute procedure SET_GLOBAL_PARAM('AKTUODDZIAL',:oddzial);
      execute procedure SET_GLOBAL_PARAM('AKTUOPERATOR','0');
      execute procedure SET_GLOBAL_PARAM('AKTUOPERGRUPY','');
      execute procedure SET_GLOBAL_PARAM('CURRENTCOMPANY',:company);
      execute procedure SET_GLOBAL_PARAM('GLOBALAPPLICATIONIDENTS','');
  end
end^
SET TERM ; ^
