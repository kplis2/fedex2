--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_CALENDAR_CHECKSTARTTIME(
      PRDEPART varchar(10) CHARACTER SET UTF8                           ,
      ENDTIME timestamp,
      WORKTIME double precision)
  returns (
      STARTTIME timestamp)
   as
declare variable CURDATE timestamp;
declare variable ST time;
declare variable ET time;
declare variable CALENDAR integer;
declare variable DAYREF integer;
declare variable DAYKIND integer;
declare variable DAYWORKTIME double precision;
declare variable DAYSTART timestamp;
declare variable DAYEND timestamp;
begin
  curdate = cast (endtime as date);
  starttime = :endtime;
  select prdeparts.calendar from PRDEPARTs where SYMBOL = :PRDEPART into :calendar;
  if(:calendar is null) then
    exception UNIVERSAL 'Brak kalendarza dla wydzialu '||:prdepart;
  for select e.REF, Cdate, edk.daytype, WORKSTART, WORKEND, PRWORKTIME
    from ecaldays e
    join edaykinds edk on edk.ref = e.daykind
    where calendar = :calendar and cdate <= :curdate
    order by cdate desc
    into :dayref, :curdate,  :daykind, :st,  :et, :dayworktime
  do begin
    if(:daykind <> 1) then begin--dzien niepracujacy - pomijam
      starttime = :curdate;--przesuniecie na poczatek tego dnia
    end else begin
      daystart = cast(:curdate as date)||' '||:st;
      dayend = cast(:curdate as date)||' '||:et;
      if(:dayend = :curdate) then--zakonczenie dnia o polnocy
        dayend = :curdate + 1;
      if(:endtime < :dayend) then--zmniejsze ilosci dnia roboczego, bo opracja sie zaczyna w trakcie dnia
        dayworktime = (:dayend - :endtime);
      else
        endtime = :dayend;
      if(:dayworktime > 0) then begin-- moze byc mniejsze, jesli end byl po zakoczeniu dnia pracy
        if(:worktime < :dayworktime) then begin
          starttime = :endtime - :worktime;
          exit;
        end else begin
          starttime = :curdate;
          worktime = :worktime - :dayworktime;
        end
      end else
        starttime = :curdate;
    end
  end
end^
SET TERM ; ^
