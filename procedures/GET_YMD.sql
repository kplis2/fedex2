--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_YMD(
      FROMDATE date,
      TODATE date,
      VACOPTIM smallint = 0,
      VACINCDAYS integer = 0)
  returns (
      Y smallint,
      M smallint,
      D smallint)
   as
declare variable T_DATE date;
declare variable EDAYS smallint;
begin
/*DS: Obliczanie liczby dni, misiecy, lat w sytuacji gdy dodajemy ciagle okresy.
  MWr: Jezeli VACOPTIM = 1 to liczba miesiecy jest wyliczna jako suma pelnych
       miesiecy i pozostalych dni (opcjonalnie + VACINCDAYS) /30.             */

  y = 0;
  m = 0;
  d = 0;

  if (fromdate is not null and todate is not null and fromdate <= todate) then
  begin
    if (vacoptim = 1) then --BS45844
    begin
    --w EDAYS przechowujemy liczbe dni nieobejmujacych pelnych miesiecy
      edays = coalesce(vacincdays,0);

      select l from monthlastday(:todate) into :t_date;

      if (not(extract(month from todate) = extract(month from fromdate)
           and extract(year from todate) = extract(year from fromdate))
      ) then begin
      /*daty nie naleza do jednego okresu, 'rzutujemy' zakres dat do
        pelnych miesiecy, a 'obciete' dni to wspomniana wartosc EDAYS*/

        if (extract(day from todate) <> extract(day from t_date)) then
        begin
           t_date = todate;
           todate = cast(extract(year from todate) || '/' || extract(month from todate) || '/1' as date) - 1;
           edays = edays + t_date - todate;
        end
        
        if (extract(day from fromdate) <> 1) then
        begin
           select l from monthlastday(:fromdate) into :t_date;
           edays = edays + t_date - fromdate + 1;
           fromdate = t_date + 1;
        end
      end else
      begin
      /*jezeli daty naleza do jednego okresu i jednoczesnie stanowia pelny
        miesiac, to nie nalezy wyliczac EDAYS*/
        if (extract(day from todate) <> extract(day from t_date)
           or extract(day from fromdate) <> 1
       ) then
         edays = edays + todate + 1 - fromdate;
      end
    end

    t_date = fromdate;
    while (t_date <= todate + 1)
    do begin
      execute procedure date_add(fromdate, y + 1, 0, 0) returning_values t_date;
      if (t_date <= todate + 1) then
        y = y + 1;
    end
  
    t_date = fromdate;
    while (t_date <= todate + 1)
    do begin
      execute procedure date_add(fromdate, y, m + 1, 0) returning_values t_date;
      if (t_date <= todate + 1) then
        m = m + 1;
    end
  
    if (vacoptim = 1) then
    begin
      m = m + edays/30;
      d = mod(edays,30);
    end else
    begin
      execute procedure date_add(fromdate, y, m, 0)
        returning_values fromdate;
      d = todate + 1 - fromdate;
    end
  end
  suspend;
end^
SET TERM ; ^
