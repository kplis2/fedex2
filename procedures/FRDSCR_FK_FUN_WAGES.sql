--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRDSCR_FK_FUN_WAGES(
      KEY1 integer,
      KEY2 integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      TIMEELEM integer,
      ELEMENT integer)
  returns (
      RET numeric(14,2))
   as
declare variable p numeric(14,12);
  declare variable wage numeric(14,2);
  declare variable oddzial varchar(10);
  declare variable oddzialsymbol varchar(10);
  declare variable period char(6);
  declare variable iyear integer;
  declare variable imonth smallint;
  declare variable todate date;
  declare variable fromdate date;
  declare variable tmpdate date;
  declare variable etodate date;
  declare variable efromdate date;
  declare variable days numeric(14,12);
  declare variable edays integer;
  declare variable employee integer;
  declare variable ws integer;
  declare variable ews integer;
  declare variable department varchar(2);
  declare variable ph varchar(80);
  declare variable przedstawiciel integer;
begin
  ret = 0;
  if (:TIMEELEM is null) then
    select frdcells.timelem
      from frdcells
      where frdcells.ref = :key1
      into :timeelem;
  if (:element is null) then
    select frdcells.dimelem
      from frdcells
      where frdcells.ref = :key1
      into :element;

  select first 1 shortname
    from frdimelems
    where dimelem = :element
    into :oddzialsymbol;

  select first 1 name
    from dimelemdef
    where shortname = :oddzialsymbol
    into :ph;

  select ref
    from employees
    where personnames = :ph
    into :przedstawiciel;

  select oddzial
    from oddzialy
    where symbol = :oddzialsymbol
    into oddzial;

  select first 1 shortname
    from frdimelems
    where dimelem = :timeelem
    into :period;
  iyear = cast(substring(period from 1 for 4) as integer);
  imonth = cast(substring(period from 5 for 2) as smallint);
  if (imonth = 12) then
     todate = cast((iyear + 1) || '/1/1' as date) - 1;
  else
     todate = cast(iyear || '/' || (imonth+1) || '/1' as date) - 1;

  fromdate = cast(iyear || '/1/1' as date);

  for
    select coalesce(sum(P.pvalue),0), EM.fromdate, EM.todate, EM.employee
      from eprpos P
        join epayrolls PP on P.payroll = PP.ref
        join employment EM on EM.employee = P.employee
      where
        PP.cper = :period and EM.fromdate <= :todate and (EM.todate >= :fromdate or EM.todate is null)
        and P.ecolumn in (4000, 5100, 8200, 8210, 8220, 8810, 8820)
        and ((EM.branch = :oddzial and EM.department <> 'PH')
          or (:oddzial is null and :przedstawiciel is null)
          or (:oddzial is null and EM.employee = :przedstawiciel))
      group by PP.ref, EM.ref, EM.fromdate, EM.todate, EM.employee
      into :wage, :efromdate, :etodate, :employee
  do begin
    if (efromdate < fromdate) then
      efromdate = fromdate;
    if (etodate > todate or etodate is null) then
      etodate = todate;

-- W ramach BS36832 zwiekszono liczbe argumentow zwracanych przez ecal_work
--    execute procedure ecal_work(employee, fromdate, todate)
--     returning_values days, ws;
--    execute procedure ecal_work(employee, efromdate, etodate)
--      returning_values edays, ews;
    select wd, ws from ecal_work(:employee, :fromdate, :todate)  into days, ws;
    select wd, ws from ecal_work(:employee, :efromdate, :etodate) into edays, ews;


    if (edays <> days) then
    begin
      p = edays / days;
      wage = p * wage;
    end
    ret =  ret + wage;
  end
-- umowy cywilnoprawne
    select coalesce(sum(PP.pvalue),0)
    from epayrolls PR
      join eprpos PP on (PR.ref = PP.payroll)
      join emplcontracts C on (PR.emplcontract = C.ref)
    where PR.empltype = 2 and PR.cper = :period and PP.ecolumn = 1080
    and ((C.branch = :oddzial and C.department <> 'PH')
          or (:oddzial is null and :przedstawiciel is null)
          or (:oddzial is null and C.employee = :przedstawiciel))
    into :wage;
    ret = ret + wage;
suspend;
end^
SET TERM ; ^
