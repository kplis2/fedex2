--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_CENNIK_KONTROLA_Z_DOSTAWA(
      CENNIK integer,
      DOSTAWAID varchar(120) CHARACTER SET UTF8                           ,
      TABKURSID varchar(20) CHARACTER SET UTF8                           )
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      WERSJA integer,
      WERNAZWA varchar(40) CHARACTER SET UTF8                           ,
      MARZA numeric(14,4),
      KOSZTZAK numeric(14,2),
      CENACEN numeric(14,4),
      CENAMIN numeric(14,4))
   as
declare variable KURS numeric(14,4);
declare variable CENAWER numeric(14,4);
declare variable DOSTAWA integer;
declare variable TABKURS integer;
begin

  select ref from DOSTAWY where SYMBOL = :DOSTAWAID into :dostawa;
  if(:dostawa is null) then dostawa = 0;
  select ref from TABKURS where SYMBOL = :TABKURSID into :tabkurs;
  if(:tabkurs is null) then tabkurs = 0;
  for select CENNIK.KTM, CENNIK.WERSJA, TOWARY.NAZWA, WERSJE.NAZWA,
             WERSJE.marzamin,CENNIK.cenanet, WERSJE.cena_zakn,TABKURSP.sredni
  from CENNIK join TOWARY on ( CENNIK.KTM = TOWARY.KTM and CENNIK.CENNIK = :CENNIK)
              join WERSJE on( CENNIK.KTM = WERSJE.KTM and CENNIK.WERSJA = WERSJE.NRWERSJI)
              left join TABKURSP on (TABKURSP.TABKURS = :tabkurs AND TABKURSp.WALUTA = CENNIK.waluta)
    into :ktm, :wersja, :nazwa, :wernazwa, :marza,:cenacen, :cenawer, :kurs
 do begin
   if(:kurs is null) then kurs = 1;
   cenacen = :cenacen * :kurs;
   if(:dostawa = 0) then kosztzak = cenawer;
   else begin
      kosztzak = null;
     select AVG(DOKUMPOZ.CENASR) from DOKUMPOZ join DOKUMNAG on (DOKUMNAG.REF=DOKUMPOZ.DOKUMENT and DOKUMNAG.akcept = 1)
       join DEFDOKUM on (DOKUMNAG.typ = DEFDOKUM.symbol and DEFDOKUM.zewn = 1 and DEFDOKUM.wydania = 0)
     into :kosztzak;
     if(:kosztzak is null) then kosztzak = 0;

   end
   if(:marza is null) then marza = 0;
   cenamin = :kosztzak * ( 100+:marza)/100;
   if(:cenacen < :cenamin) then
     suspend;
 end
end^
SET TERM ; ^
