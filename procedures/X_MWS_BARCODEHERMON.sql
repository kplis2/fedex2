--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_BARCODEHERMON(
      KODKRESK_IN STRING100)
  returns (
      KODKRESK_O STRING100)
   as
declare variable position_temp integer_id;
declare variable position_temp2 integer_id;
declare variable len integer_id;
begin
--procedura sluzaca do odczytania specyficznych dla hermona kodow kreskowych ZG 133618
  position_temp = position('-' in :kodkresk_in);
  position_temp2 = position('-', kodkresk_in , position_temp + 1);
  if (position_temp <> 0 and position_temp2 = 0) then
  begin
    kodkresk_in = substring(kodkresk_in FROM position_temp + 1);
    len = CHAR_LENGTH(kodkresk_in);
    if (len = 7) then
    begin
      kodkresk_in = '5400000'|| substring(kodkresk_in FROM 3);
    end
  end
  kodkresk_o = kodkresk_in;
  suspend;
end^
SET TERM ; ^
