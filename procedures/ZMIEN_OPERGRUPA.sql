--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ZMIEN_OPERGRUPA(
      OPERREF integer,
      SYMBOLGR varchar(255) CHARACTER SET UTF8                           ,
      TRYB smallint)
  returns (
      NEWGRUPA varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable grupa varchar(1024);
begin
  symbolgr = ';' || :symbolgr || ';';
  newgrupa = '';
  select grupa from operator where ref = :operref into :grupa;
  if (:grupa is null or :grupa = '') then grupa = ';';

  if (:tryb = 1) then
    newgrupa = replace(:grupa,  :symbolgr,  ';');
  else if (:tryb = 0) then begin
    if (not exists(select first 1 1 from operator where grupa like '%'||:symbolgr||'%' and ref = :operref)) then
      newgrupa = :grupa || substring(:symbolgr from 2 for coalesce(char_length(:symbolgr),0)); -- [DG] XXX ZG119346
  end

  if (:newgrupa = '') then newgrupa = :grupa;
  if (:newgrupa <> :grupa) then
    update operator set grupa = :newgrupa where ref = :operref;
  suspend;
end^
SET TERM ; ^
