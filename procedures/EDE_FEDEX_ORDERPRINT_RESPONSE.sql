--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_ORDERPRINT_RESPONSE(
      EDEDOCSH_REF integer)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
   as
declare variable shippingdoc integer;
declare variable filepath varchar(80);
begin
  otable = 'LISTYWYS';

  select oref
    from ededocsh
    where ref = :ededocsh_ref
  into :shippingdoc;
  oref = :shippingdoc;

  select p.val
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.name = 'sciezkaPliku'
  into :filepath;

  update listywys l set l.opis = :filepath
    where l.ref = :shippingdoc;
  suspend;
end^
SET TERM ; ^
