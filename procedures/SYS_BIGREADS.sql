--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_BIGREADS(
      LIMIT integer = 1000)
  returns (
      ID integer,
      STATE smallint,
      PAGE_READS bigint,
      SQL_TEXT blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           ,
      TMSTAMP timestamp,
      ADDRESS varchar(253) CHARACTER SET UTF8                           ,
      PROCESS varchar(253) CHARACTER SET UTF8                           )
   as
begin
  for select s.mon$statement_id, s.mon$state, i.mon$page_reads, s.mon$sql_text,
    s.mon$timestamp, a.mon$remote_address, a.mon$remote_process
    from mon$statements s
    join mon$io_stats i on i.mon$stat_id=s.mon$stat_id
    left join mon$attachments a on a.mon$attachment_id=s.mon$attachment_id
    where i.mon$page_reads>:limit
    order by i.mon$page_reads desc
    into ID,STATE,PAGE_READS,SQL_TEXT,TMSTAMP,ADDRESS,PROCESS
    do begin
      suspend;
    end
end^
SET TERM ; ^
