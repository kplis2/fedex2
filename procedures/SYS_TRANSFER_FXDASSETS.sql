--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_TRANSFER_FXDASSETS as
declare variable S1 varchar(255); /* symbol */
declare variable S2 varchar(255); /* nazwa */
declare variable S3 varchar(255); /* grupa */
declare variable S4 varchar(255); /* oddzial */
declare variable S5 varchar(255); /* wydzial */
declare variable S6 varchar(255); /* data zakupu */
declare variable S7 varchar(255); /* data uzytkowania */
declare variable S8 varchar(255); /* nr seryjny */
declare variable S9 varchar(255); /* opis */
declare variable S10 varchar(255); /* konto amortyzacji podatkowej */
declare variable S11 varchar(255); /* konto amortyzacji finansowej */
declare variable S12 varchar(255); /* metoda amortyzacji podatkowej */
declare variable S13 varchar(255); /* metoda amortyzacji finansowej */
declare variable S14 varchar(255); /* stawka amortyzacji podatkowej */
declare variable S15 varchar(255); /* stawka amortyzacji finansowej */
declare variable S16 varchar(255); /* korekta amortyzacji podatkowej */
declare variable S17 varchar(255); /* korekta amortyzacji finansowej */
declare variable S18 varchar(255); /* wartosc podatkowa */
declare variable S19 varchar(255); /* wartosc finansowa */
declare variable S20 varchar(255); /* umorzenie podatkowe */
declare variable S21 varchar(255); /* umorzenie finansowe */
declare variable S22 varchar(255); /* osoba */
declare variable S23 varchar(255); /* cena zakupu */
declare variable SYMBOL varchar(20);
declare variable NAME varchar(80);
declare variable GRUPA varchar(10);
declare variable ODDZIAL varchar(10);
declare variable WYDZIAL varchar(10);
declare variable PURCHASEDT timestamp;
declare variable USINGDT timestamp;
declare variable SERIALNO varchar(20);
declare variable DESCRIPT varchar(255);
declare variable TBKSYMBOL BKSYMBOLS_ID;
declare variable FBKSYMBOL BKSYMBOLS_ID;
declare variable TAMETH varchar(3);
declare variable FAMETH varchar(3);
declare variable TARATE numeric(10,5);
declare variable FARATE numeric(10,5);
declare variable TACRATE numeric(10,5);
declare variable FACRATE numeric(10,5);
declare variable TVALUE numeric(15,2);
declare variable FVALUE numeric(15,2);
declare variable TREDEMPTION numeric(15,2);
declare variable FREDEMPTION numeric(15,2);
declare variable PERSON integer;
declare variable PVALUE numeric(15,2);
declare variable TMP integer;
begin

  select count(ref) from expimp where coalesce(char_length(s1),0)>20 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich symboli.';

  select count(ref) from expimp where coalesce(char_length(s2),0)>80 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich nazw ST.';

  select count(ref) from expimp where coalesce(char_length(s3),0)>10 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich grup ST.';

  select count(ref) from expimp where coalesce(char_length(s4),0)>10 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich oddzialow.';

  select count(ref) from expimp where coalesce(char_length(s5),0)>20 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich wydzialow.';

  select count(ref) from expimp where coalesce(char_length(s8),0)>20 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich nr seryjnych.';

  select count(ref) from expimp where coalesce(char_length(s10),0)>20 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich kont podatkowych.';

  select count(ref) from expimp where coalesce(char_length(s11),0)>20 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich kont finansowych.';


  for select s1, s2,  s3,  s4,  s5,  s6,  s7,  s8,  s9,  s10,
            s11, s12, s13, s14, s15, s16, s17, s18, s19, s20,
            s21, s22, s23
    from expimp 
  into :s1, :s2,  :s3,  :s4,  :s5,  :s6,  :s7,  :s8,  :s9,  :s10,
      :s11, :s12, :s13, :s14, :s15, :s16, :s17, :s18, :s19, :s20,
      :s21, :s22, :s23
  do begin

-- symbol ST
    symbol = substring(:s1 from  1 for  20);
    if (exists(select first 1 1 from fxdassets where symbol=:symbol)) then
      exception universal 'ST o symbolu: ' || :s1 || ' juz znajduje sie w bazie';


-- nazwa ST
    name = substring(:s2 from  1 for  80);

-- grupa ST
    grupa = substring(:s3 from  1 for  10);
    if (not exists(select first 1 1 from amortgrp where symbol=:grupa)) then
      exception universal 'Grupa ' || :grupa || ' nie istnieje.';

-- oddzial
    oddzial = substring(:s4 from  1 for  10);
    if (not exists(select first 1 1 from oddzialy where oddzial=:oddzial)) then
      exception universal 'Oddzial ' || :oddzial || ' nie istnieje.';

-- wydzial
    wydzial = substring(:s5 from  1 for  10);

    if (not exists(select first 1 1 from departments where symbol=:wydzial)) then
      exception universal 'Wydzial ' || :wydzial || ' nie istnieje.';

-- data zakupu
    if(s6 like '____-__-__') then
      purchasedt = cast(:s6 as date);
    else
      exception universal 'Bledny format daty zakupu przy ST: ' || :s1;

-- data uzytkowania
    if(s7 like '____-__-__') then
      usingdt = cast(:s7 as date);
    else
      exception universal 'Bledny format daty uzytkowania przy ST: ' || :s1;


-- nr seryjny
    serialno = substring(:s8 from  1 for  20);


-- opis
    descript = :s9;

-- konto podatkowe
    tbksymbol = substring(:s10 from  1 for  20);
    if (not exists (select first 1 1 from bksymbols where symbol=:tbksymbol and sgroup='EFK')) then
      insert into bksymbols(symbol, name, sgroup)
      values (:tbksymbol, '', 'EFK');

-- konto finansowe
    fbksymbol = substring(:s11 from  1 for  20);
    if (not exists (select first 1 1 from bksymbols where symbol=:fbksymbol and sgroup='EFK')) then
      insert into bksymbols(symbol, name, sgroup)
      values (:fbksymbol, '', 'EFK');

-- metoda amortyzacji podatkowej
    if(:s12='L') then
      tameth = 'LIP';
    else if (:s12='D') then
      tameth = 'DPR';
    else if (:s12='U') then
      tameth = 'UJP';
    else
      exception universal 'Metoda amortyzacji podatkowej srodka: ' || :s1 || ' jest bledna.';


-- metoda amortyzacji finansowej
    if(:s13='L') then
      fameth = 'LIF';
    else if (:s13 = 'D') then
      fameth = 'DFR';
    else if (:s13 = 'U') then
      fameth = 'UJF';
    else
      exception universal 'Metoda amortyzacji finansowej srodka: ' || :s1 || ' jest bledna.';


-- stawka amortyzacji podatkowej
    s14 = replace(:s14, ',', '.');
    tarate = cast(:s14 as numeric(10,5));
    if (tarate > 100 or tarate < 0) then
      exception universal 'Bledna stawka amortyzacji podatkowej srodka: '|| :s1;


-- stawka amortyzacji finansowej
    s15 = replace(:s15, ',', '.');
    farate = cast(:s15 as numeric(10,5));
    if (farate > 100.00 or farate < 0.0) then
      exception universal 'Bledna stawka amortyzacji finansowej srodka: '|| :s1;


-- korekta amortyzacji podatkowej
    s16 = replace(:s16, ',', '.');
    tacrate = cast(:s16 as numeric(10,5));


-- korekta amortyzacji finansowej
    s17 = replace(:s17, ',', '.');
    facrate = cast(:s17 as numeric(10,5));


-- wartosc podatkowa
    s18 = replace(:s18, ',', '.');
    tvalue = cast(:s18 as numeric(15,2));


-- wartosc finansowa
    s19 = replace(:s19, ',', '.');
    fvalue = cast(:s19 as numeric(15,2));


-- umorzenie podatkowe
    s20 = replace(:s20, ',', '.');
    tredemption = cast(:s20 as numeric(15,2));


-- umorzenie finansowe
    s21 = replace(:s21, ',', '.');
    fredemption = cast(:s21 as numeric(15,2));


-- osoba
    select ref from cpersons where symbol=cast(:s22 as varchar(20))
    into :person;
    if(person is null) then
      exception universal 'Nie ma w bazie osoby o koncie ksiegowym: ' || :s22 ;


-- cena zakupu
    s23 = replace(:s23, ',', '.');
    pvalue = cast(:s23 as numeric(15,2));



-- INSERT
  insert into fxdassets (symbol, name, amortgrp, oddzial, department, purchasedt, usingdt, serialno, descript, 
                         tbksymbol, fbksymbol, tameth, fameth, tarate, farate, tacrate, facrate,
                         tvalue, fvalue, tredemption, fredemption, person, pvalue)
              values (:symbol,  :name, :grupa, :oddzial, :wydzial, :purchasedt, :usingdt, :serialno, :descript, 
                      :tbksymbol, :fbksymbol, :tameth, :fameth, :tarate, :farate, :tacrate, :facrate,
                      :tvalue, :fvalue, :tredemption, :fredemption, :person, :pvalue);
  end
end^
SET TERM ; ^
