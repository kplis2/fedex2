--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_GEN_UPO(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
begin
  select trim(refid) from edeclarations where ref = :oref
  into :val;
  id = 0;
  parent = null;
  name = 'refId';
  params = null;
  suspend;
end^
SET TERM ; ^
