--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPERACJA(
      ZAM integer,
      OPER integer,
      LISTA integer)
  returns (
      STATUS integer)
   as
begin
  /* To ma być procedura wykonująca zdefiniowaną
     operacje magazynową */
  /* 1. Wykonujemy weryfikacje danych na zamówieniu. Lista weryfikacji
        w tabeli DEFOPERWER. Bieżemy te rekordy, w których:
        DEFOPERWER.OPERACJA = OPER,
        DEFOPERWER.PRZED = 1
        Weryfikacji poddajemy pole DEFOPERWER.POLE tabeli NAGZAM (jeżeli
        DEFOPERWER.ZAMPOZ=1) lub tabeli POZZAM (DEFOPERWER.ZAMPOZ=0)
        Reguly weryfikacji opisano w dokumencie wizji. Jeżeli weryfikacja
        sie nie powiedzie, to anulujemy transakcje i zwracamy komunikat,
        ze dane pola nie mają odpowiedniej wartosci.*/
  /* 2. Generujemy wpis o operacji w tabeli HISTZAM */
  /* 3. Jeżeli DEFOPERZAM.SLADNAG=1 to kopiujemy naglówek do tabeli
        NAGZAMH */
  /* 4. Jeżeli DEFOPERZAM.SLADPOZ=1 to kopiujemy wszystkie pozycje
        do tabeli POZZAMH */
  /* 5. Jeżeli DEFOPERZAM.PRZESUN=1 to podstawiamy NAGZAM.REJESTR =
        DEFOPERZAM.DOREJ */
  /* 6. Wykonujemy modyfikacje innych pól zgodnie z definicjami w tabeli
        DEFOPERMOD. Pole ZAMPOZ traktujemy analogicznie, jak w przypadku
        weryfikacji. Pole WARTOSC może być wyrażeniem SQL */
  /* 7. Ponownie wykonujemy liste weryfikacji, tym razem dla
        DEFOPERWER.PRZED=0
        Reguly weryfikacji opisano w dokumencie wizji. Jeżeli weryfikacja
        sie nie powiedzie, to anulujemy transakcje i zwracamy komunikat,
        ze dane pola nie mają odpowiedniej wartosci.*/
  /* 8. Zmieniamy stan zamówienia zgodnie z polem DEFOPERZAM.STAN i
        dokonujemy wszelkich związanych z tym operacji (rezerwacje,
        blokady) Opisane jest to przy tabeli NAGZAM */
  /* 9. Wykonujemy liste operacji magazynowych, czyli wystawiamy dokumenty.
        Jeden rekord DEFOPERMAG wystawia jeden dokument magazynowy.
        W magazynie DEFOPERMAG.MAGAZYN, typ DEFOPERMAG.TYP, pole MAG2 jest
        informacyjne w przypadku dokumentów przesuniec miedzymagazynowych.
        Pole DEFOPERMAG.AKCEPT mowi, czy dokument ma być już akceptowany
        podczas jego zakadania. Spowoduje to przeprowadzanie naliczeń na
        stanach magazynowych od razu podczas zakadania dokumentu.
        Pole DEFOPERMAG.BLOKADA przenosimy na nagówek dokumentu. Powoduje
        to zablokowanie lub udostepnienie dokumentu do modyfikacji.
        Nastpnie pozycje zamówenia są przenoszone jako pozycje dokumentu.
        Dokonujemy weryfikacji dostepnosci stanów magazynowych. Jeżeli
        DEFOPERMAG.CALK=1, to dopuszczamy jedynie cakowitą realizacje, bo
        w przeciwnym razie wycofujemy calą operacje i zwracamy blad.
        Jeżeli DEFOPERMAG.CALK=0, to w przypadku braku na stanach dokumenty
        wystawiamy na tyle, ile można. Jeżeli DEFOPERMAG.MODZAM = 1, to
        rzeczywiste ilosci z dokumentów magazynowych przenoszone są na
        pozycje zamówienia. Flaga DEFOPERMAG.UZUP natomiast wystawia dokument
        tylko w takich ilosciach, aby stany magazynowe w magazynach, których
        dotyczą pozycje zamówienia byly wystarczające do jego zrealizowania.
        Ma to znaczenie tylko dla dokumentów rozchodowych, których
        DEFOPERMAG.MAG2 = NAGZAM.MAGAZYN oraz przychodowych, których
        DEFOPERMAG.MAGAZYN = NAGZAM.MAGAZYN.  */
  /* 10. Jeżeli DEFOPERZAM.FAKTURA = 1, to wystawiana jest faktura zgodnie
        z pozycjami zamówienia. Wystawiany jest dokument typu
        DEFOPERZAM.TYPFAK w rejestrze faktur DEFOPERZAM.REJFAK.
        Symbol wystawionej faktury wpisujemy do pola  */
  /* 11. Jeżeli DEFOPERZAM.WYSYLKA = 1, to zamówienie dolączane jest
        do listy wysylkowej wskazanej uprzednio w programie i
        przekazanej parametrem LISTA. Ten obszar jeszcze jest w proszku. */

  suspend;
end^
SET TERM ; ^
