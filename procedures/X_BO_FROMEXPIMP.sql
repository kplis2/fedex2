--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_BO_FROMEXPIMP(
      FROMREF INTEGER_ID,
      OVER INTEGER_ID)
   as
  declare variable ref integer_id;
  declare variable x_kod_towaru_ses string100;
  declare variable stan numeric_14_4;
  declare variable wh string3;
  declare variable timestart timestamp_id;
  declare variable timestartdcl timestamp_id;
  declare variable timestopdcl timestamp_id;
  declare variable mwsaccessory integer_id;
  declare variable period period_id;
  declare variable branch string10;
  declare variable good string40;
  declare variable vers integer_id;
  declare variable whareap integer_id;
  declare variable maxnumber integer_id;
  declare variable realtime double precision;
  declare variable x_partia date_id;
  declare variable x_serial_no integer_id;
  declare variable x_slownik integer_id;
  declare variable ref_mwsord integer_id;
  declare variable ref_mwsact integer_id;
  declare variable stanyililosc numeric_14_4;
  declare variable refsc integer_id;
begin
  select oddzial from defmagaz where symbol = 'MWS' into branch;   --branch
  select okres from datatookres(current_date,0,0,0,0) into period; --period
  select ref from mwsaccessories where aktuoperator = 74 into :mwsaccessory;  --mwsaccessory
  execute procedure gen_ref('MWSORDS') returning_values ref_mwsord; --ref_mwsord
  insert into mwsords (ref, mwsordtype, stocktaking, doctype, operator, priority, description, wh, regtime, timestartdecl,
    timestopdecl, mwsaccessory, branch, period, cor, rec, bwharea, ewharea, status)
  values (:ref_mwsord, 9, 0, 'O', 74, 0, 'Bilans otwarcia', 'MWS', current_timestamp(0), :timestart,
           current_timestamp(0), :mwsaccessory, :branch, :period, 0, 0, null, null, 0);
  for
    select e.ref, cast(trim(e.s1) as numeric_14_4), trim(e.s2)
      from expimp e
      where e.ref >= :fromref
        and e.ref <= :fromref + :over
    into :ref, :stan, :x_kod_towaru_ses
  do begin
    select t.ktm, w.ref
      from towary t
      join wersje w on (w.ktm = t.ktm)
    where t.x_kod_towaru_ses = :x_kod_towaru_ses
      and w.nrwersji = 0
    into :good, :vers;
    if (not exists(select first 1 1 from towary t where t.x_kod_towaru_ses = :x_kod_towaru_ses)) then
      exception universal 'Towar nie istnieje w bazie '||x_kod_towaru_ses||' '||ref;
    if (stan is not null and stan > 0) then begin
      select max(number) from mwsacts where mwsord = :ref_mwsord into maxnumber;
      if (maxnumber is null) then maxnumber = 0;
      maxnumber = maxnumber + 1;
      execute procedure gen_ref('MWSACTS') returning_values ref_mwsact;  --ref_mwsact
      insert into mwsacts (ref, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, docid, docposid, doctype, settlmode,
              closepalloc, wh, wharea, whsec, regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, recdoc, plus, whareap, whareal, wharealogp,
              wharealogl, number, disttogo, mwspallocl, mwspallocp, mwsconstlocl)
      values (:ref_mwsact, :ref_mwsord, 0, 0, :good, :vers, :stan, 2, null, null, 'O', 0,
              0, 'MWS', :whareap, null, current_timestamp(0), :timestartdcl, :timestopdcl, :realtime, null, null, 1, 0, 1, null, null, null,
              null, :maxnumber, 0, 1, 1, 1);

-- auto commit mwsacts
    update mwsacts ma set ma.status = 1
      where ma.ref  = :ref_mwsact;
    update mwsacts ma set ma.status = 2, ma.quantityc = ma.quantity
      where ma.ref  = :ref_mwsact;

 /*   select s.ilosc
      from stanyil s
      where s.wersjaref = :vers
    into :stanyililosc;
    if (:stanyililosc is null) then stanyililosc = 0;

    update or insert into stanyil (magazyn, wersjaref, ilosc, wersja)
      values ('MWS', :vers,  :stanyililosc + :stan, 0)
      matching (magazyn, wersjaref); */

    execute procedure GEN_REF('STANYCEN') returning_values :refsc;
      insert into STANYCEN(REF,MAGAZYN,KTM,WERSJA,ILOSC,CENA,WARTOSC,DOSTAWA,DATA)
        values (:refsc,'MWS', :good, 0, 0, 0, 0, 0, current_timestamp(0));

    end
  end
-- auto commit mwsord
    if (exists(select first 1 1 from mwsacts ma where ma.mwsord = :ref_mwsord)) then begin
      update mwsords mo set mo.status = 1, mo.timestopdecl = :timestopdcl, mo.operator = null, mo.mwsaccessory = :mwsaccessory
        where mo.ref = :ref_mwsord
          and mo.status = 0;
      update mwsords mo set mo.status = 5, mo.operator = 74
        where mo.ref = :ref_mwsord
          and mo.status = 1;
    end
    else begin
-- kasowanie pustego mwsord
      delete from mwsords where ref = :ref_mwsord;
    end

end^
SET TERM ; ^
