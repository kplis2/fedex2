--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ANAL_MWS_REC_DOCS(
      SUPLIER integer,
      DOCID integer,
      BDATE date,
      EDATE date,
      DOCTYPE varchar(3) CHARACTER SET UTF8                           ,
      WH varchar(3) CHARACTER SET UTF8                           ,
      PALINCLUDE smallint,
      ONLYMISTAKES smallint)
  returns (
      SUPLIEROUT varchar(60) CHARACTER SET UTF8                           ,
      DOCIDOUT integer,
      DOCSYMBOL varchar(40) CHARACTER SET UTF8                           ,
      ORDREF integer,
      ORDSYMBOL varchar(40) CHARACTER SET UTF8                           ,
      ORDSTATUS smallint,
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      QUANTITYONPOS numeric(14,4),
      QUANTITYONPOSDETS numeric(14,4),
      QUANTITYONACTS numeric(14,4),
      QUANTITYONACTDETS numeric(14,4),
      QUANTITYONMINUS numeric(14,4),
      QUANTITYONPLUS numeric(14,4),
      BILANS numeric(14,4),
      STATUS smallint)
   as
begin
  for
    select distinct d.ref, d.symbol, o.ref, o.symbol, o.status, case when d.dostawca is null then d.magazyn else s.id end
      from dokumnag d
        left join mwsords o on (o.docid = d.ref and o.rec = 1 and o.status = 5)
        left join dostawcy s on (s.ref = d.dostawca)
        left join defdokum f on (d.typ = f.symbol)
        left join defmagaz m on (m.symbol = d.magazyn)
    where d.data >= :bdate and d.data <= :edate and d.akcept in (1,8)
      and f.wydania = 0 and f.koryg = 0 and f.zewn = 1
      and (d.ref = :docid or :docid is null)
      and (d.typ = :doctype or :doctype is null)
      and (d.dostawca = :suplier or :suplier is null)
      and (d.magazyn = :wh or :wh is null or :wh = '')
      and (o.ref is not null or coalesce(m.mws,0) = 0)
    into docidout, docsymbol, ordref, ordsymbol, ordstatus, suplierout
  do begin
    for
      select GOOD, VERS, quantityonpos, quantityonposdets, quantityonacts,
          quantityonactdets, quantityonminus, quantityonplus, bilans, status
        from ANAL_MWS_REC_DOCS_DETS(:docidout,:palinclude)
        into GOOD, VERS, quantityonpos, quantityonposdets, quantityonacts,
          quantityonactdets, quantityonminus, quantityonplus, bilans, status
    do begin
      if ((onlymistakes = 1 and status = 0) or onlymistakes = 0) then
        suspend;
    end
  end
end^
SET TERM ; ^
