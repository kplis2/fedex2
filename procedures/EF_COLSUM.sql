--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_COLSUM(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      COL1 integer,
      COL2 integer)
  returns (
      RET numeric(14,2))
   as
begin
  --DU: personel - funkcja liczy sume wartosci skladnikow z listy plac
  select sum(pvalue)
    from eprpos
    where employee = :employee and payroll = :payroll
      and ecolumn >= :col1 and ecolumn <= :col2
    into :ret;
  if (ret is null) then
    ret = 0;
  suspend;
end^
SET TERM ; ^
