--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PLANS_PR_SUB_CALC(
      PLANS integer,
      PLANSCOL integer,
      UZUPELNIANIE smallint,
      AKTUALIZACJA smallint,
      PLANSROW integer)
  returns (
      VAL numeric(14,4))
   as
declare variable czasprod smallint;
declare variable stanymag smallint;
DECLARE VARIABLE KEY1 VARCHAR(20);
DECLARE VARIABLE TEMPODNI NUMERIC(14,4);
DECLARE VARIABLE TEMPOOKR NUMERIC(14,4);
DECLARE VARIABLE DATAPOCZ TIMESTAMP;
DECLARE VARIABLE DATAKON TIMESTAMP;
DECLARE VARIABLE CNT INTEGER;
begin
  if (:AKTUALIZACJA is null) then AKTUALIZACJA = 0;
  if (:uzupelnianie is null) then uzupelnianie = 0;
  if (:czasprod is null) then czasprod = 0;
  if (:stanymag is null) then stanymag = 0;
  select planscol.bdata, planscol.ldata from planscol where planscol.ref = :PLANSCOL
    into :datapocz, :datakon;
  select plansrow.KEY1 from plansrow
    where plansrow.plans = :PLANS and PLANSrow.ref = :PLANSROW into :key1;
  execute procedure PLANS_PR_SUB_CALC_KTM(:key1,:datapocz,:datakon,:czasprod, :stanymag) returning_values(:val);
  if (:val is null) then val = -1;
  suspend;
end^
SET TERM ; ^
