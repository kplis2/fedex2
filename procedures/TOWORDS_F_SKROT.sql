--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TOWORDS_F_SKROT(
      WEJ varchar(1024) CHARACTER SET UTF8                           ,
      LANGUAGE integer)
  returns (
      RET varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable ind integer;
declare variable k integer;
declare variable wyj VARCHAR(1024);
declare variable tmp char;
begin
  ind = 1;
  wyj = '';
  if (language = 0) then begin
    tmp = 'A';
    while (tmp <> '' and tmp <> ',' and tmp <> '.')
    do begin
      if (tmp = '0') then wyj = :wyj || 'zero ';
      if (tmp = '1') then wyj = :wyj || 'jed ';
      if (tmp = '2') then wyj = :wyj || 'dwa ';
      if (tmp = '3') then wyj = :wyj || 'trzy ';
      if (tmp = '4') then wyj = :wyj || 'czte ';
      if (tmp = '5') then wyj = :wyj || 'pięć ';
      if (tmp = '6') then wyj = :wyj || 'sze ';
      if (tmp = '7') then wyj = :wyj || 'sied ';
      if (tmp = '8') then wyj = :wyj || 'osie ';
      if (tmp = '9') then wyj = :wyj || 'dzie ';

      tmp = substring(wej from 1 for 1);
      wej = substring(wej from 2);
      if (tmp = ',') then wyj = :wyj || '*';

    end
    ret = wyj;
  end else if (language = 1) then begin
    tmp = 'A';
    while (tmp <> '' and tmp <> ',')
    do begin
      if (tmp = '0') then wyj = :wyj || 'null ';
      if (tmp = '1') then wyj = :wyj || 'one ';
      if (tmp = '2') then wyj = :wyj || 'two ';
      if (tmp = '3') then wyj = :wyj || 'three ';
      if (tmp = '4') then wyj = :wyj || 'four ';
      if (tmp = '5') then wyj = :wyj || 'five ';
      if (tmp = '6') then wyj = :wyj || 'six ';
      if (tmp = '7') then wyj = :wyj || 'seven ';
      if (tmp = '8') then wyj = :wyj || 'eight ';
      if (tmp = '9') then wyj = :wyj || 'nine ';

      tmp = substring(wej from 1 for 1);
      wej = substring(wej from 2);
      if (tmp = ',') then wyj = :wyj || '*';

    end
    ret = wyj;
  end else if (language = 2) then begin
    tmp = 'A';
    while (tmp <> '' and tmp <> ',')
    do begin
      if (tmp = '0') then wyj = :wyj || 'null';
      if (tmp = '1') then wyj = :wyj || 'ein';
      if (tmp = '2') then wyj = :wyj || 'zwei';
      if (tmp = '3') then wyj = :wyj || 'drei';
      if (tmp = '4') then wyj = :wyj || 'vier';
      if (tmp = '5') then wyj = :wyj || 'funf';
      if (tmp = '6') then wyj = :wyj || 'sechs';
      if (tmp = '7') then wyj = :wyj || 'sieben';
      if (tmp = '8') then wyj = :wyj || 'acht';
      if (tmp = '9') then wyj = :wyj || 'neun';

      tmp = substring(wej from 1 for 1);
      wej = substring(wej from 2);
      if (tmp = ',') then wyj = :wyj || '*';

    end
    ret = wyj;
  end
  suspend;
end^
SET TERM ; ^
