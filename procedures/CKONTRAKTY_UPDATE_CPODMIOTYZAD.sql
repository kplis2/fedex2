--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CKONTRAKTY_UPDATE_CPODMIOTYZAD(
      CKONTRAKT integer)
   as
declare variable lista VARCHAR(255);
declare variable cpodm integer;
begin
  lista = ';';
  for
      select distinct ZADANIA.CPODMIOT
      from ZADANIA
      where (ZADANIA.CKONTRAKT=:ckontrakt)
      into :cpodm
  do begin
  lista = :lista || cast(:cpodm as varchar(10)) || ';';
  end
  update CKONTRAKTY set CPODMIOTYZAD = :lista where REF=:ckontrakt;
end^
SET TERM ; ^
