--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_SUPP_DIFF_POS(
      DOCID integer,
      MWSDISPOSITION smallint,
      MODE smallint)
  returns (
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      LOT integer,
      WH varchar(3) CHARACTER SET UTF8                           ,
      DOCILOSC numeric(14,4),
      ILOSC numeric(14,4))
   as
begin
  if (mwsdisposition is null) then mwsdisposition = 0;
  if (mode is null) then mode = 0;
  if (mwsdisposition = 0 and mode = 0) then
  begin
    for
      select distinct dr1.ktm, dp1.wersjaref, null, dr1.magazyn,
          (select sum(case when dr.iloscl is not null then dr.iloscl else 0 end)
             from dokumroz dr
               join dokumpoz dp on (dr.pozycja = dp.ref)
             where dp.dokument = :docid and dp.wersjaref = dp1.wersjaref),
           (select sum(case when md1.quantityc is not null then  md1.quantityc else 0 end)
              from mwsactdets md1
              where md1.vers = dp1.wersjaref
                and md1.docid = :docid and md1.status = 5
                and md1.mwspallocp is null)
        from dokumroz dr1
          join dokumpoz dp1 on (dr1.pozycja = dp1.ref)
        where dp1.dokument = :docid
        into good, vers, lot, wh,
          docilosc, ilosc
    do begin
      suspend;
    end
  end else if (mwsdisposition = 1 and mode = 0) then
  begin
    for
      select distinct dp1.ktm, dp1.wersjaref, null, dn1.magazyn,
          (select sum(dp.ilosc)
             from dokumpoz dp
             where dp.dokument = :docid and dp.wersjaref = dp1.wersjaref),
          (select sum(case when md1.quantityc is not null then  md1.quantityc else 0 end)
             from mwsactdets md1
             where md1.vers = dp1.wersjaref
               and md1.docid = :docid and md1.status = 5
               and md1.mwspallocp is null)
        from dokumpoz dp1
          left join dokumnag dn1 on (dn1.ref = dp1.dokument)
        where dp1.dokument = :docid
        into good, vers, lot, wh,
          docilosc, ilosc
    do begin
      suspend;
    end
  end else if (mwsdisposition = 0 and mode = 1) then
  begin
    for
      select distinct md.good, md.vers, d.magazyn,
          (select sum(case when md1.quantityc is not null then  md1.quantityc else 0 end)
             from mwsactdets md1
             where md1.vers = md.vers and md.mwsord = md1.mwsord),
          (select sum(case when dr.iloscl is not null then dr.iloscl else 0 end)
             from dokumroz dr
               join dokumpoz dp on (dr.pozycja = dp.ref)
             where dp.dokument = md.docid and dp.wersjaref = md.vers)
        from mwsactdets md
          left join dokumnag d on (d.ref = md.docid and md.doctype = 'M')
        where md.docid = :docid and md.doctype = 'M' and md.status = 5 and md.mwspallocp is null
        into good, vers, wh, ilosc, docilosc
    do begin
      suspend;
    end
  end else if (mwsdisposition = 1 and mode = 1) then
  begin
    for
      select distinct md.good, md.vers, md.wh,
          (select sum(case when md1.quantityc is not null then  md1.quantityc else 0 end)
             from mwsactdets md1
             where md1.vers = md.vers and md.mwsord = md1.mwsord),
          (select sum(dp.ilosc)
             from dokumpoz dp
             where dp.dokument = md.docid and dp.wersjaref = md.vers)
        from mwsactdets md
        where md.docid = :docid and md.doctype = 'M' and md.status = 5 and md.mwspallocp is null
        into good, vers, wh, ilosc, docilosc
    do begin
      suspend;
    end
  end
end^
SET TERM ; ^
