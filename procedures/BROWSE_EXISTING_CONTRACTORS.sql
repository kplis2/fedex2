--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BROWSE_EXISTING_CONTRACTORS(
      SKROT SHORTNAME_ID,
      NIP varchar(20) CHARACTER SET UTF8                           ,
      SLODEFIN integer = -1,
      SLOPOZIN integer = -1)
  returns (
      SLODEF integer,
      SLODEFNAME varchar(20) CHARACTER SET UTF8                           ,
      SKROT1 SHORTNAME_ID,
      NIP1 varchar(20) CHARACTER SET UTF8                           ,
      AKTYWNY smallint,
      CRM smallint,
      REF integer,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      BAZA varchar(50) CHARACTER SET UTF8                           ,
      COMPANY integer,
      ZAKLAD varchar(255) CHARACTER SET UTF8                           )
   as
declare variable SLODEFREF integer;
declare variable PTNIP varchar(40);
begin

/*  for select SKROT, NIP, AKTYWNY, REF, NAZWA,SLODEF
    from CPODMIOTY
    where (SKROT=:skrot and SKROT<>'') or (NIP=:nip and NIP<>'')
    into :skrot1, :nip1, :aktywny, :ref, :nazwa, :slodefref
  do begin
    select NAZWA from SLODEF where REF=:slodefref into :nazwaslodef;
    baza='CPODMIOTY';
    suspend;
  end*/
  
  if(coalesce(char_length(:nip),0)<3) then nip = 'xyxyxyxyxy'; -- [DG] XXX ZG119346
  if(coalesce(char_length(:skrot),0)<2) then skrot = 'xyxyxyxyxy'; -- [DG] XXX ZG119346

  ptnip = replace(:nip, '-', '');
  ptnip = replace(:ptnip, ' ', '');
  
  select REF from SLODEF where TYP='DOSTAWCY' into :slodef;
  slodefname='DOSTAWCY';
  for select ID, NIP, AKTYWNY, CRM, REF, NAZWA, COMPANY
    from DOSTAWCY
    where ((upper(ID) like '%%'||upper(:skrot)||'%%' and ID<>'') or
           (upper(NAZWA) like '%%'||upper(:skrot)||'%%' and NAZWA<>'') or
           ((PROSTYNIP like '%%'||:ptnip or :ptnip like '%%'||PROSTYNIP) and PROSTYNIP<>''))
           and (:slodefin <> 6 or ref <> :slopozin)
    order by ID
    into :skrot1, :nip1, :aktywny, :crm, :ref, :nazwa, :company
  do begin
    if(:company is not null) then
      select NAME from COMPANIES where REF=:company into :zaklad;
    else
      zaklad = '';
    suspend;
  end
/*  for select ID, NIP, AKTYWNY, CRM, REF, NAZWA, COMPANY
    from DOSTAWCY
    where ((upper(ID) like '%%'||upper(:skrot)||'%%' and ID<>'') or
           (upper(NAZWA) like '%%'||upper(:skrot)||'%%' and NAZWA<>'') or
           (PROSTYNIP=:ptnip and PROSTYNIP<>''))
    order by ID
    into :skrot1, :nip1, :aktywny, :crm, :ref, :nazwa, :company
  do begin
    if(:company is not null) then
      select NAME from COMPANIES where REF=:company into :zaklad;
    else
      zaklad = '';
    suspend;
  end*/

  select REF from SLODEF where TYP='KLIENCI' into :slodef;
  slodefname='KLIENCI';
  for select FSKROT, NIP, AKTYWNY, CRM, REF, NAZWA, COMPANY
    from KLIENCI
    where ((upper(FSKROT) like '%%'||upper(:skrot)||'%%' and FSKROT<>'') or
           (upper(NAZWA) like '%%'||upper(:skrot)||'%%' and NAZWA<>'') or
           ((PROSTYNIP like '%%'||:ptnip or :ptnip like '%%'||PROSTYNIP) and PROSTYNIP<>''))
           and (:slodefin <> 1 or ref <> :slopozin)
    order by FSKROT
    into :skrot1, :nip1, :aktywny, :crm, :ref, :nazwa, :company
  do begin
    if(:company is not null) then
      select NAME from COMPANIES where REF=:company into :zaklad;
    else
      zaklad = '';
    suspend;
  end

  select REF from SLODEF where TYP='PKLIENCI' into :slodef;
  slodefname='PKLIENCI';
  nip1='';
  aktywny=1;
  for select SKROT, REF, NAZWA, CRM
    from PKLIENCI
    where ((upper(SKROT) like '%%'||upper(:skrot)||'%%' and SKROT<>'') or
           (upper(NAZWA) like '%%'||upper(:skrot)||'%%' and NAZWA<>''))
           and (:slodefin <> 7 or ref <> :slopozin)
    order by SKROT
    into :skrot1, :ref, :nazwa, :crm
  do begin
    company = NULL;
    zaklad = '';
    suspend;
  end
/*
  select REF from SLODEF where TYP='SPRZEDAWCY' into :slodef;
  slodefname='SPRZEDAWCY';
  for select SKROT, NIP, AKT, CRM, REF, NAZWA, COMPANY
    from SPRZEDAWCY
    where ((upper(SKROT) like '%%'||upper(:skrot)||'%%' and SKROT<>'') or
           (upper(NAZWA) like '%%'||upper(:skrot)||'%%' and NAZWA<>'') or
           (NIP=:nip and NIP<>''))
    order by skrot
    into :skrot1, :nip1, :aktywny, :crm, :REF, :nazwa, :company
  do begin
    if(:company is not null) then
      select NAME from COMPANIES where REF=:company into :zaklad;
    else
      zaklad = '';
    suspend;
  end

  slodefname='SLOPOZ';
  nip1='';
  aktywny=1;
  for select SLOWNIK, KOD, REF, NAZWA, CRM
    from SLOPOZ
    where ((upper(KOD) like '%%'||upper(:skrot)||'%%' and KOD<>'') or
          (upper(NAZWA) like '%%'||upper(:skrot)||'%%' and NAZWA<>''))
    order by kod
    into :slodef, :skrot1, :ref, :nazwa, :crm
  do begin
    company = NULL;
    zaklad = '';
    suspend;
  end
*/
end^
SET TERM ; ^
