--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_WORKEDHOURS(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      ECOLUMN integer)
  returns (
      HOURS numeric(14,2))
   as
declare variable FROMDATE date;
declare variable TODATE date;
declare variable ACCOUNT_PERIOD smallint;
declare variable ACCOUNT_LENGTH smallint;
declare variable STARTDATE date;
declare variable ENDDATE date;
declare variable OVERTIME0 integer;
declare variable OVERTIME50 integer;
declare variable OVERTIME100 integer;
declare variable NIGHTHOURS integer;
declare variable ETYPE smallint;
begin
  --DS: wyliczanie liczby nadgodzin danego typu
  execute procedure get_config('EWORKEDHOURS_TYPE',2) returning_values :etype;
  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  if (etype = 0) then
  begin
    --nadgodziny 100% (przekroczenie normy sredniotygodniowej)
    if (ecolumn in (620,630)) then
    begin
      overtime50 = null;
      overtime100 = null;
      select first 1 c.account_period, c.account_length, c.startdate
        from ecalendars c
          join emplcalendar ec on c.ref = ec.calendar
        where ec.employee = :employee
          and ec.fromdate <= :fromdate
        order by ec.fromdate desc
        into :account_period, :account_length, :startdate;
    
      --jezeli okres jest rozny od miesiecznego
      if (account_period <> 1 or account_length <> 1) then
      begin
        --okres rozliczeniowy w tygodniach (mozliwy jest chyba tylko 4-tygodniowy przy pracy w ruchu ciaglym)
        if (account_period = 0) then
        begin
          while (startdate < fromdate and account_length <> 0) do  --MWr 20110620 dopisane account_length <> 0, przy 0 mamy petle nieskonczona
            startdate = startdate + account_length * 7;
          --jesli koniec okresu rozliczeniowego przypada w tym miesiacu
          if (startdate <= todate + 1) then
          begin
            enddate = startdate - 1;
            execute procedure date_add(startdate,0,0,-7 * account_length) returning_values startdate;
            select overtime_weektype50_min, overtime_weektype100_min
              from sp_eworkedhours(null,null,null,null,:startdate,:enddate,0,1,:employee)
              into :overtime50, :overtime100;
          end
        end else
        --w miesiacach
        begin
          while (startdate < fromdate) do
            execute procedure date_add(startdate,0,account_length,0) returning_values startdate;
          --jesli koniec okresu rozliczeniowego przypada w tym miesiacu
          if (startdate <= todate + 1) then
          begin
            enddate = startdate - 1;
            execute procedure date_add(startdate,0,-account_length,0) returning_values startdate;
            select overtime_weektype50_min, overtime_weektype100_min
              from sp_eworkedhours(null,null,null,null,:startdate,:enddate,0,1,:employee)
              into :overtime50, :overtime100;
          end
        end
      end
      if (ecolumn = 620) then
        hours = coalesce(overtime50,0) / 3600.00;
      else
        hours = coalesce(overtime100,0) / 3600.00;
    end
    else begin
      -- 59 -> godziny ponadwymiarowe
      -- 60 -> nadgodziny 50%
      -- 61 -> nadgodziny 100%
      -- 64 -> nocne
  
      --liczba nadgodzin w ujeciu miesiecznym
      --czyli przekroczenie dziennej normy pracownika
      select sum(ec.overtime0), sum(ec.overtime50), sum(ec.overtime100), sum(ec.nighthours)
        from emplcaldays ec
          join ecaldays c on c.ref = ec.ecalday
          join edaykinds edk on edk.ref = c.daykind
        where ec.employee = :employee
          and ec.cdate >= :fromdate
          and ec.cdate <= :todate
          and edk.daytype = 1
        into :overtime0, :overtime50, :overtime100, :nighthours;
      if (ecolumn = 590) then
        hours = overtime0;
      else if (ecolumn = 600) then
        hours = overtime50;
      else if (ecolumn = 610) then
        hours = overtime100;
      else if (ecolumn = 640) then
        hours = nighthours;
      hours = hours / 3600.00;
    end
  end
  --wyliczenie z okna przepracowanych godzin
  else begin
    select sum(amount)
      from eworkedhours H
      where hdate <= :todate and hdate >= :fromdate
        and ecolumn = :ecolumn and employee = :employee
      into :hours;
  end
  suspend;
end^
SET TERM ; ^
