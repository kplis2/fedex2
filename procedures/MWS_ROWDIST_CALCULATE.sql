--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_ROWDIST_CALCULATE(
      WHSECROW integer)
   as
declare variable coordxb numeric(14,2);
declare variable coordxe numeric(14,2);
declare variable coordyb numeric(14,2);
declare variable coordye numeric(14,2);
declare variable coordywhsecrow numeric(14,2);
declare variable wharearef integer;
declare variable number integer;
declare variable whsec integer;
begin
  -- okreslenie szerokosci obszaru
  select coordxb, coordxe, coordyb, whsec
    from whsecrows where ref = :whsecrow
    into coordxb, coordxe, coordywhsecrow, whsec;
  if (coordywhsecrow is null) then coordywhsecrow = 0;
  if (coordxb is null) then coordxb = 0;
  if (coordxe is null) then coordxe = 0;
  -- okreslenie dlugosci obszaru z wymiarow segmentu regalu
  for
    select ref, number from whareas where whsecrow = :whsecrow
      into wharearef, number
  do begin
    select sum(l) from whareas where whsecrow = :whsecrow and number < :number
      into :coordyb;
    if (:coordxb is null) then coordxb = 0;
    if (:coordyb is null) then coordyb = 0;
    update whareas set coordxb = :coordxb, coordxe = :coordxe, coordyb = :coordyb + :coordywhsecrow
      where ref = :wharearef;
  end
end^
SET TERM ; ^
