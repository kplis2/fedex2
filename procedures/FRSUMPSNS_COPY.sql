--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRSUMPSNS_COPY(
      OLD_FRHDRS_SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      NEW_FRHDRS_SYMBOL varchar(20) CHARACTER SET UTF8                           )
   as
declare variable old_frpsns_ref integer;
declare variable new_frpsns_ref integer;
declare variable new_frpsns_ref2 integer;
declare variable frpsns_number integer;
declare variable frpsns_symbol varchar(15);
declare variable frsumpsns_sumpsn integer;
declare variable frsumpsns_ratio smallint;
declare variable max_ver integer;
begin

  for select ref from frpsns
  where frhdr = :old_frhdrs_symbol
  into :old_frpsns_ref
  do begin

  select max(frversion) from frsumpsns
  where frpsn = :old_frpsns_ref
  into :max_ver;

  for select sumpsn, ratio from frsumpsns
  where frpsn = :old_frpsns_ref and frversion = :max_ver
  into :frsumpsns_sumpsn, :frsumpsns_ratio
  do begin
      select number, symbol from frpsns
      where ref = :old_frpsns_ref
      into :frpsns_number, :frpsns_symbol;

      select ref from frpsns
      where number = :frpsns_number and symbol = :frpsns_symbol and frhdr=:new_frhdrs_symbol
      into :new_frpsns_ref;

      select number, symbol from frpsns
      where ref = :frsumpsns_sumpsn
      into :frpsns_number, :frpsns_symbol;

      select ref from frpsns
      where number = :frpsns_number and symbol = :frpsns_symbol and frhdr=:new_frhdrs_symbol
      into :new_frpsns_ref2;

      insert into frsumpsns(frpsn, sumpsn, ratio, frversion)
      values(:new_frpsns_ref, :new_frpsns_ref2, :frsumpsns_ratio, 0);
    end
  end
end^
SET TERM ; ^
