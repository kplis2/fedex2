--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE Z06_HIRED(
      YEARID integer,
      ONLY_WOMAN smallint,
      FIRSTJOB smallint,
      EDUCATION integer,
      WORKED_HERE smallint,
      VACONLY smallint,
      COMPANY integer)
  returns (
      AMOUNT numeric(14,2))
   as
declare variable TMP integer;
declare variable YFROMDATE date;
declare variable YTODATE date;
begin
/* Funkcja wykorzystywana w sprawozdaniu GUS Z-06, analizuje liczb osob
   zatrudnionych w danym roku YEARID wg zadanych warunkow. Parametry:
     > WORKED_HERE = 1 - pracowal wczesniej
                   = 2 - przerwa krotsza niz 1 miesiac
     > ONLY_WOMEN  = 1 - analizuj tylko kobiety */


  yfromdate = yearid || '/1/1';
  ytodate = yearid || '/12/31';
  only_woman = coalesce(only_woman,0);
  if (firstjob = 0) then firstjob = null;
  if (worked_here = 0) then worked_here = null;
  if (vaconly = 0) then vaconly = null;

  if (vaconly is null) then
    select count(distinct e.person)
      from employees e
        join persons p on (p.ref = e.person)
        join employment c on (e.ref = c.employee)
        left join edictzuscodes z on (z.ref = p.education and z.dicttype = 6)
      where e.company = :company
        and c.workdim >= 1
        and extract(year from c.fromdate) = :yearid
        and c.fromdate = (select min(m.fromdate) from employment m
                             where m.employee = e.ref)
        and (:only_woman = 0 or p.sex = 0)
        and (:firstjob is null
          or (not(exists(select first 1 1 from eworkcertifs r  --BS58966
                            where r.employee = e.ref and r.fromdate < :yfromdate))
              and (z.code = :education or :education is null)))
        and (:worked_here is null
          or exists(select first 1 1 from eworkcertifs w
                      where w.employee = e.ref and w.todate <= c.fromdate
                        and (w.todate >= (c.fromdate - 30) or :worked_here = 1)
                        and w.seniorityflags like '%STW%'))
      into :amount;

  if (firstjob is null and worked_here is null) then
    select count(person) from get_persons_wych_or_bezpl(:yfromdate, :ytodate, :company, :only_woman)
      into :tmp;

  amount = coalesce(amount,0) + coalesce(tmp,0);
  suspend;
end^
SET TERM ; ^
