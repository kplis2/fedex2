--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NEWBKDOC_KOREKTA(
      KORYGOWANY BKDOCS_ID,
      KTYPE BKDOCTYPES_ID,
      KPERIOD OKRES_ID,
      KTRANSDATE TIMESTAMP_ID,
      KVATREG BKREGS_ID,
      SYMBOL DOCSYMBOL_ID,
      AUTOS smallint = 0)
  returns (
      KOREKTAREF BKDOCS_ID)
   as
declare variable CORRTYPE BKDOCTYPES_ID;
declare variable COMPANY COMPANIES_ID;
declare variable NSYMBOL DOCSYMBOL_ID;
declare variable KCOUNT INTEGER_ID;
declare variable nstatus integer;
declare variable DEF integer;
declare variable POS integer;
begin
  --exception universal''||korygowany;
  execute procedure get_global_param('CURRENTCOMPANY')
    returning_values :company;
  select SYMBOL, status, DICTDEF, DICTPOS
    from bkdocs b
    where b.ref = :korygowany
    into :nsymbol, :nstatus, :def, :pos;
  if (nstatus<2) then
    exception universal'Nie można wystawić korekty do dokumenty niezaksięgowanego';
  nsymbol = '';
  --Czy wstawiamy wsadowo - wtedy musimy jakos wygenerować symbol
  if (AUTOS = 1) then
  begin
    select count(ref)
      from bkdocs b
      where b.bkdocsvatcorrectionref = :korygowany
      into :kcount;
    nsymbol = nsymbol || ' korekta nr '||kcount;
  end else -- Jednak jak wprowadzamy recznie to musimy przepisać symbol z podpowiedzi
  begin
    nsymbol = SYMBOL;
  end
  insert into bkdocs (DOCTYPE, BKDOCSVATCORRECTIONREF,  VATPERIOD, PERIOD,
    VATREG, TRANSDATE, COMPANY, BKREG, SYMBOL, DICTDEF, DICTPOS)
    values(:KTYPE, :KORYGOWANY, :KPERIOD, :KPERIOD,
      :KVATREG, :KTRANSDATE, :COMPANY, 'ZLEDLUGI', :NSYMBOL, :DEF, :POS)
    returning ref into :KOREKTAREF;
end^
SET TERM ; ^
