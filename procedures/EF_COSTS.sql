--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_COSTS(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      COSTS numeric(14,2))
   as
declare variable fromdate date;
  declare variable todate date;
  declare variable infocosts smallint;
  declare variable pvalue numeric(14,2);
begin
  --DU: personel - liczy koszty uzyskania pracownika

  execute procedure efunc_prdates(payroll, 1)
    returning_values fromdate, todate;
  costs = 0;

  --jezeli ktorykolwiek ze skladnikow wynagrodzenia jest dodatni to licz KUP
  select first 1 p.pvalue
    from eprpos p
    where p.payroll = :payroll
      and p.employee = :employee
      and p.ecolumn >= 1000
      and p.ecolumn <= 3999
      and p.pvalue > 0
    into :pvalue;

  if (pvalue is null) then exit;

  select max(fromdate)
    from empltaxinfo
    where fromdate <= :todate and employee = :employee
    into :fromdate;

  if (fromdate is null) then
    exception NO_TAX_INFO;

  select max(costs)
    from empltaxinfo
    where fromdate = :fromdate and employee = :employee
    into infocosts;

  if (infocosts = 1) then
  begin
    execute procedure ef_pval(:employee, :payroll, :prefix, 'KOSZTYNORMALNE')
      returning_values costs;
  end else if (infocosts = 2) then
  begin
    execute procedure ef_pval(:employee, :payroll, :prefix, 'KOSZTYPODWYZSZONE')
      returning_values costs;
  end
  suspend;
end^
SET TERM ; ^
