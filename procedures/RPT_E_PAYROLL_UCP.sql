--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_E_PAYROLL_UCP(
      BILLFROMDATE date,
      BILLTODATE date,
      PAYDAY date,
      STATUS smallint,
      EMPLOYEE integer)
  returns (
      NUM1 integer,
      NAME1 varchar(22) CHARACTER SET UTF8                           ,
      VAL1 numeric(14,2),
      REPPAY1 integer,
      NUM2 integer,
      NAME2 varchar(22) CHARACTER SET UTF8                           ,
      VAL2 numeric(14,2),
      REPPAY2 integer,
      NUM3 integer,
      NAME3 varchar(22) CHARACTER SET UTF8                           ,
      VAL3 numeric(14,2),
      REPPAY3 integer,
      NUM4 integer,
      NAME4 varchar(22) CHARACTER SET UTF8                           ,
      VAL4 numeric(14,2),
      REPPAY4 integer,
      NUM5 integer,
      NAME5 varchar(22) CHARACTER SET UTF8                           ,
      VAL5 numeric(14,2),
      REPPAY5 integer)
   as
declare variable col1 smallint;
declare variable col2 smallint;
declare variable col3 smallint;
declare variable col4 smallint;
declare variable col5 smallint;
declare variable maxcol smallint;
begin
--MWr: Personel - Generowanie wydruku list plac dla rachunkow UCP

  status = coalesce(status,0);

  select count(distinct case  when p.ecolumn < 1000 then  p.ecolumn end),
         count(distinct case when p.ecolumn >= 1000 and p.ecolumn < 4000 then p.ecolumn end),
         count(distinct case when p.ecolumn > 4000 and p.ecolumn < 5900 then p.ecolumn end),
         count(distinct case when p.ecolumn > 5900 and p.ecolumn < 8000 then p.ecolumn end),
         count(distinct case when p.ecolumn > 8000 and p.ecolumn < 9050 then p.ecolumn end)
      from epayrolls r
        join eprpos p on (r.ref = p.payroll and r.empltype = 2)
        join ecolumns c on (c.number = p.ecolumn)
      where r.billdate >= :billfromdate and r.billdate <= :billtodate
        and (r.payday = :payday or :payday is null) and status >= :status
        and p.employee = :employee and c.reppay > 0
      into :col1, :col2, :col3, :col4, :col5;

  maxcol = col1;
  if (col2 > maxcol) then maxcol = col2;
  if (col3 > maxcol) then maxcol = col3;
  if (col4 > maxcol) then maxcol = col4;
  if (col5 > maxcol) then maxcol = col5;

  while (maxcol > 0) do
  begin
    if (col1 >= maxcol) then
      select first 1 skip (:col1 - :maxcol) c.number, c.name, c.reppay, sum(p.pvalue)
        from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype = 2)
          join ecolumns c on (c.number = p.ecolumn and c.reppay > 0)
        where r.billdate >= :billfromdate and r.billdate <= :billtodate
          and (r.payday = :payday or :payday is null) and status >= :status
          and p.employee = :employee
          and p.ecolumn < 1000
        group by c.number, c.name, c.reppay
        order by c.number
        into :num1, :name1, :reppay1, :val1;

    if (col2 >= maxcol) then
      select first 1 skip (:col2 - :maxcol) c.number, c.name, c.reppay, sum(p.pvalue)
        from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype = 2)
          join ecolumns c on (c.number = p.ecolumn and c.reppay > 0)
        where r.billdate >= :billfromdate and r.billdate <= :billtodate
          and (r.payday = :payday or :payday is null) and status >= :status
          and p.employee = :employee
          and p.ecolumn >= 1000 and p.ecolumn < 4000
        group by c.number, c.name, c.reppay
        order by c.number
        into :num2, :name2, :reppay2, :val2;

    if (col3 >= maxcol) then
      select first 1 skip (:col3 - :maxcol) c.number, c.name, c.reppay, sum(p.pvalue)
        from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype = 2)
          join ecolumns c on (c.number = p.ecolumn and c.reppay > 0)
        where r.billdate >= :billfromdate and r.billdate <= :billtodate
          and (r.payday = :payday or :payday is null) and status >= :status
          and p.employee = :employee
        and p.ecolumn > 4000 and p.ecolumn < 5900
        group by c.number, c.name, c.reppay
        order by c.number
        into :num3, :name3, :reppay3, :val3;

    if (col4 >= maxcol) then
      select first 1 skip (:col4 - :maxcol) c.number, c.name, c.reppay, sum(p.pvalue)
        from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype = 2)
          join ecolumns c on (c.number = p.ecolumn and c.reppay > 0)
        where r.billdate >= :billfromdate and r.billdate <= :billtodate
          and (r.payday = :payday or :payday is null) and status >= :status
          and p.employee = :employee
        and p.ecolumn > 5900 and p.ecolumn < 8000
        group by c.number, c.name, c.reppay
        order by c.number
        into :num4, :name4, :reppay4, :val4;

    if (col5 >= maxcol) then
      select first 1 skip (:col5 - :maxcol) c.number, c.name, c.reppay, sum(p.pvalue)
        from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype = 2)
          join ecolumns c on (c.number = p.ecolumn and c.reppay > 0)
        where r.billdate >= :billfromdate and r.billdate <= :billtodate
          and (r.payday = :payday or :payday is null) and status >= :status
          and p.employee = :employee
        and p.ecolumn > 8000 and p.ecolumn < 9050
        group by c.number, c.name, c.reppay
        order by c.number
        into :num5, :name5, :reppay5, :val5;

    suspend;
    maxcol = maxcol - 1;
  end
end^
SET TERM ; ^
