--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_UPS_SHIPMENT_REQUEST(
      OTABLE varchar(31) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable isShippingList smallint;
declare variable ShipToAdress string;
declare variable ShipToCity city_id;
declare variable ShipToPostCode postcode;
declare variable ShipToCountryCode country_id;
declare variable ShipToAttentionName string;
declare variable ShipToName string;
declare variable ShipToPhoneNumber phones_id;
declare variable ServiceCode varchar(2);
declare variable LabelImageFormatCode varchar(6);
declare variable PackageRef listywysdroz_opk_id;
declare variable PackageWeight waga_id;
declare variable PackageLength waga_id;
declare variable PackageHeight waga_id;
declare variable PackageWidth waga_id;
declare variable PackageDeclaredValue numeric(14,2);
declare variable ShipCOD numeric(14,2);
declare variable PackageUnitOfMeasurement varchar(3);
declare variable PackageType varchar(2);
declare variable PackageParent integer;
declare variable SubPackageParent integer;
declare variable ReferenceParent integer;
declare variable SubReferenceParent integer;
declare variable GrandParent integer;
declare variable ShipFromAdress string;
declare variable ShipFromCity string;
declare variable ShipFromPostalCode string;
declare variable ShipFromAttentionName string;
declare variable ShipFromName string;
declare variable ShipFromCountryCode string;
declare variable ShipperPhoneNumber string;
declare variable ShipperAttentionName string;
declare variable ShipperName string;
declare variable ShipperPostalCode string;
declare variable ShipperNumber string;
declare variable ShipperAdress string;
declare variable ShipperCity string;
declare variable ShipToNameTmp string;
declare variable UserName string;
declare variable UserPassword string;
declare variable AccesLicenseNumber string;
declare variable ShipmentDescription string1024;
declare variable branch oddzial_id;
declare variable dokmagref integer;
declare variable zewn smallint;
declare variable wydania smallint;
declare variable sposdost integer;
declare variable cnt integer;
declare variable ReferenceId varchar(30);
declare variable ReferenceVal varchar(255);
declare variable dtelefon varchar(255);
declare variable i integer;
declare variable petla smallint;
declare variable shipper spedytor_id;
declare variable filepath string255;
declare variable listywysd_ref listywysd_id;
declare variable klient_waluta waluta_id;
declare variable czypobranie smallint_id; --[PM] XXX PR121869
begin
  --Sprawdzanie czy istieje dokument spedycyjny
  select first 1 1
    from listywysd l
    where l.ref = :oref
  into :isShippingList;

  if(isShippingList is null) then
    exception ede_ups_listywysd_brak;

  val = 1;
  parent = null;
  params = null;
  id = 0;
  name = 'SendShipmentRequest';
  suspend;

  GrandParent = id;

  --Wybieranie danyh do wysyki
  --ML najpierw odbiorca, potem klient, potem listywysd
  select first 1
      l.ref, 
      iif(o.ref is not null, o.dulica, iif(k.ref is not null, k.dulica, l.adres)),
      iif(o.ref is not null, o.dmiasto, iif(k.ref is not null, k.dmiasto, l.miasto)),
      iif(o.ref is not null, o.dkodp, iif(k.ref is not null, k.dkodp, l.kodp)),
      iif(o.ref is not null, o.krajid, iif(k.ref is not null, k.krajid, l.krajid)),
      l.kontrahent,
      substring(iif(o.ref is not null, o.nazwa, k.nazwa) from 1 for 35),
      case when coalesce(o.nazwafirmy,'') = '' then k.nazwa else o.nazwafirmy end,
      iif(o.ref is not null, o.dtelefon, k.telefon),
      s.x_service_code,
      s.x_label_format, --XXX MSt: ZG60713
      sped.osoba, substring(sped.telefon from 1 for 15),
      substring(l.uwagisped from 1 for 50), l.pobranie,
      l.sposdost, l.oddzial, sp.spedytor,
      k.waluta,
      coalesce(p.pobranie,0) --[PM] XXX PR121869
    from listywysd l
      left join odbiorcy o on (l.odbiorcaid = o.ref)
      left join klienci k on (k.ref = o.klient)
      join sposdost s on s.ref = l.sposdost
      join spedytwys sp on sp.sposdost = s.ref
      join spedytorzy sped on (sp.spedytor = sped.symbol)
      left join platnosci p on l.sposzap = p.ref --[PM] XXX PR121869
    where l.ref = :oref
    into  :listywysd_ref,
      :ShipToAdress, :ShipToCIty, :ShipToPostCode, :ShipToCountryCode, :ShipToNameTmp,
      :ShipToAttentionName,
      :ShipToName,
      :dtelefon, :ServiceCode, :LabelImageFormatCode, --XXX MSt: ZG60713
      :ShipFromAttentionName, :ShipperPhoneNumber,
      :ShipmentDescription, :ShipCOD,
      :sposdost, :branch, :shipper,
      :klient_waluta,
      :czypobranie; --[PM] XXX PR121869

   --sprawdzanie czy istieje dokument spedycyjny
  if(listywysd_ref is null) then exception EDE_LISTYWYSD_BRAK;


  if (czypobranie = 1 and coalesce(shipcod,0) <= 0) then --[PM] XXX PR121869
    exception universal 'Pobranie BEZ KWOTY !!!'; --[PM] XXX PR121869

  select k.klucz, k.login, k.haslo, k.numerklienta, k.sciezkapliku
    from spedytkonta k
    where k.spedytor = :shipper
      --and k.oddzial = :branch
  into :AccesLicenseNumber, :username, :userpassword, :shippernumber, :filepath;

  val = UserName;
  name = 'UserName';
  id = id + 1;
  parent = GrandParent;
  suspend;

  val = UserPassword;
  name = 'UserPassword';
  id = id + 1;
  parent = GrandParent;
  suspend;

  val = AccesLicenseNumber;
  name = 'AccesLicenseNumber';
  id = id + 1;
  parent = GrandParent;
  suspend;

  val = filepath;
  name = 'FilePath';
  id = id + 1;
  parent = GrandParent;
  suspend;

  val = 'TEST'; --jesli chcesz testowac zmien na TEST inaczej PRODUCTION
  name = 'ServiceUrlType';
  id = id + 1;
  parent = GrandParent;
  suspend;

  val = 'nonvalidate';
  name = 'RequestOption';
  id = id + 1;
  parent = GrandParent;
  suspend;

  select first 1 dokref from listywysdpoz where dokument = :oref
  into :dokmagref;
  select zewn, wydania from dokumnag where ref = :dokmagref
  into :zewn, :wydania;

  if (:ShipToName is null or (:zewn = 0 and :wydania = 1)) then
    ShipToName = :ShipToNameTmp;

  if (:ShipToAttentionName is null) then
    ShipToAttentionName = :ShipToNameTmp;

  --XXX MSt: ZG60713
  if (:dtelefon is null) then
    dtelefon = '';
  if (coalesce(char_length(:dtelefon),0) > 15) then
  begin
    i = 1;
    petla = 1;
    while (i <= (coalesce(char_length(:dtelefon),0) - 2) and :petla = 1)
    do begin
      if (substring(:dtelefon from i for i) in ('0','1','2','3','4','5','6','7','8','9')) then
        petla = 0;
      else
        i = :i + 1;
    end
    if (:petla = 0 and i <= (coalesce(char_length(:dtelefon),0) - 2)) then
      ShipToPhoneNumber = substring(:dtelefon from  i for  i + 14); -- 1 + 15 - 1 = 15
    else
      ShipToPhoneNumber = substring(:dtelefon from 1 for 15);
  end
  else
    ShipToPhoneNumber = :dtelefon;
  --XXX MSt: Koniec

  if (coalesce(:ShipToPhoneNumber,'') = '') then
    ShipToPhoneNumber = '0000000000';
  if (coalesce(:ShipToCountryCode,'') = '' or lower(:ShipToCountryCode) = 'polska') then
    ShipToCountryCode = 'PL';

  execute procedure X_USUN_POLSKIE_ZNAKI(:ShipToPhoneNumber,1)
    returning_values :ShipToPhoneNumber;


  select dm.oddzial, dm.dulica, dm.dmiasto, dm.dkodp, dm.dtelefon
    from defmagaz dm where dm.symbol = 'MWS'    --na sztywno????
  into :branch, :ShipFromAdress, :ShipFromCity, :ShipFromPostalCode, :ShipperPhoneNumber;

/*  select k.wartosc
    from konfig k
    where k.akronim = 'INFO1'
*/
-- ML UWAGA tylko do testów
-- ShipFromAdress = 'ul. Testowa 1';
-- ShipFromCity = 'Wrocław';
-- ShipFromPostalCode = '54-234';
-- ShipFromCountryCode = 'PL';
--  ShipperPhoneNumber = '123456789'; -- ML UWAGA tylko do testów

  ShipFromCountryCode = 'PL';
  ShipFromName = 'LogBox';   -- tmyczasowo
  ShipperName = substring(ShipFromName from 1 for 35);
  ShipperAttentionName = substring(ShipFromAttentionName from 1 for 35);

  execute procedure x_usun_polskie_znaki(ShipFromAdress, 1) returning_values ShipFromAdress; --XXX ZG57986
  ShipperAdress =  ShipFromAdress;
  
  ShipperPostalCode = ShipFromPostalCode;
  execute procedure x_usun_polskie_znaki(ShipFromCity, 1) returning_values ShipFromCity;  --XXX ZG57986
  ShipperCity = ShipFromCity;

-- XXX Koniec KBI ZG111380

  --Adres magazynu
  val = ShipFromAdress;
  name = 'ShipFromAdress';
  params = null;
  parent = GrandParent;
  id = id + 1;
  suspend;

  --Miasto magazynu
  val = ShipFromCity;
  name = 'ShipFromCity';
  id = id + 1;
  suspend;

  --Kod pocztowy magazynu
  val = ShipFromPostalCode;
  name = 'ShipFromPostalCode';
  id = id + 1;
  suspend;

  --Kraj magazynu
  val = ShipFromCountryCode;
  name = 'ShipFromCountryCode';
  id = id + 1;
  suspend;

  --Kraj wysyającego
  val = ShipFromCountryCode;
  name = 'ShipperCountryCode';
  id = id + 1;
  suspend;

  --Nadawca
  val = ShipFromAttentionName;
  execute procedure x_usun_polskie_znaki(val, 1) returning_values val;        --XXX ZG57986
  val = 'Testname';

  name = 'ShipFromAttentionName';
  id = id + 1;
  suspend;

  --Podmiot
  val = ShipFromName;
  name = 'ShipFromName';
  id = id + 1;
  suspend;


  val = ShipperPhoneNumber;
  name = 'ShipperPhoneNumber';
  id = id + 1;
  suspend;

  val = ShipperNumber;
  name = 'ShipperNumber';
  id = id + 1;
  suspend;

  val = ShipmentDescription;
  name = 'ShipmentDescription';
  id = id + 1;
  suspend;

  val = ShipperName;
  name = 'ShipperName';
  id = id + 1;
  suspend;

  val = ShipperAttentionName;
  execute procedure x_usun_polskie_znaki(val, 1) returning_values val;        --XXX ZG57986

  val = 'Testname'; -- ML tylko do testów
  name = 'ShipperAttentionName';
  id = id + 1;
  suspend;

  val = ShipperAdress;
  execute procedure x_usun_polskie_znaki(val, 1) returning_values val;        --XXX ZG57986
  name = 'ShipperAdress';
  id = id + 1;
  suspend;

  val = ShipperPostalCode;
  name = 'ShipperPostalCode';
  id = id + 1;
  suspend;

  val = ShipperCity;
  execute procedure x_usun_polskie_znaki(val, 1) returning_values val;        --XXX ZG57986
  name = 'ShipperCity';
  id = id + 1;
  suspend;

  val = ShipperNumber;
  name = 'BillShipperAccountNumber';
  id = id + 1;
  suspend;

  val = '01';
  name = 'ShipmentChargeNumber' ;
  id = id + 1;
  suspend;

  --Adres dostawy
  val = substring(ShipToAdress from 1 for 35);
  execute procedure x_usun_polskie_znaki(val, 1) returning_values val;        --XXX ZG57986
  name = 'ShipToAdress1';
  id = id + 1;
  suspend;

  if (coalesce(char_length(:ShipToAdress),0) > 35) then
  begin
    val = substring(ShipToAdress from 36 for 70);
    execute procedure x_usun_polskie_znaki(val, 1) returning_values val;        --XXX ZG57986
    name = 'ShipToAdress2';
    id = id + 1;
    suspend;
  end
  else
  begin
    val = '';
    execute procedure x_usun_polskie_znaki(val, 1) returning_values val;        --XXX ZG57986
    name = 'ShipToAdress2';
    id = id + 1;
    suspend;
  end

  if (coalesce(char_length(:ShipToAdress),0) > 70) then
  begin
    val = substring(ShipToAdress from 71 for 100);
    execute procedure x_usun_polskie_znaki(val, 1) returning_values val;        --XXX ZG57986
    name = 'ShipToAdress3';
    id = id + 1;
    suspend;
  end
  else
  begin
    val = '';
    execute procedure x_usun_polskie_znaki(val, 1) returning_values val;        --XXX ZG57986
    name = 'ShipToAdress3';
    id = id + 1;
    suspend;
  end

  --Miasto dostawy
  val = ShipToCity;
  execute procedure x_usun_polskie_znaki(val, 1) returning_values val;        --XXX ZG57986
  id = id + 1;
  name = 'ShipToCity';
  suspend;

  --Kod pocztowy dostawy
  val = ShipToPostCode;
  id = id + 1;
  name = 'ShipToPostalCode';
  suspend;

  --Kod kraju dostawy
  val = ShipToCountryCode;
  id = id + 1;
  name = 'ShipToCountryCode';
  suspend;

  --Nazwa odbiorcy
  val = ShipToAttentionName;
  execute procedure x_usun_polskie_znaki(val, 1) returning_values val;        --XXX ZG57986
  id = id + 1;
  name = 'ShipToAttentionName';
  suspend;

  --Nazwa podmiotu odbiorcy
  val = substring(ShipToName from 1 for 35);
  execute procedure x_usun_polskie_znaki(val, 1) returning_values val;        --XXX ZG57986
  id = id + 1;
  name = 'ShipToName';
  suspend;

  --Telefon odbiorcy
  val = ShipToPhoneNumber;
  id = id + 1;
  name = 'ShipToPhoneNumber';
  suspend;

  --Typ usugi
  val = ServiceCode;
  id = id + 1;
  name = 'ServiceCode';
  suspend;

  --Format etykiety
  val = LabelImageFormatCode;
  id = id + 1;
  name = 'LabelImageFormatCode';
  suspend;

  if (coalesce(:ShipCOD,0) > 0) then
  begin
    val = '1';
    id = id + 1;
    name = 'CODFundsCode';
    suspend;

    val = :ShipCOD;
    id = id + 1;
    name = 'CODMonetaryValue';
    suspend;


    val = coalesce(upper(:klient_waluta), 'PLN'); --poprawić, musi być tez w euro
    id = id + 1;
    name = 'CODCurrencyCode';
    suspend;
  end

  --Referencje i MPK
  cnt = 0;
  ReferenceParent = GrandParent;

  val = 1;
  id = id +1;
  name = 'Reference';
  parent = ReferenceParent;
  SubReferenceParent = id;
  suspend;

  execute procedure get_symbol_from_number(1, 2) returning_values :val;
  id = :id + 1;
  name = 'ReferenceCode';
  parent = SubReferenceParent;
  suspend;

  val = :listywysd_ref;
  id = :id + 1;
  name = 'ReferenceValue';
  parent = SubReferenceParent;
  suspend;

  PackageParent = GrandParent;

  --Paczki
  for
    select lopk.waga, lopk.ref, 'KGS', lopk.typ,
        lopk.dlugosc, lopk.wysokosc, lopk.szerokosc, lopk.x_ubezpieczenie
      from listywysdroz_opk lopk
      where lopk.listwysd = :oref
      into :PackageWeight, :PackageRef, :PackageUnitOfMeasurement, :PackageType,
        :PackageLength, :PackageHeight, :PackageWidth, :PackageDeclaredValue
    do begin
      val = 1;
      id = id +1;
      name = 'Package';
      parent = PackageParent;
      SubPackageParent = id;
      suspend;

      val = PackageRef;
      id = id + 1;
      name = 'PackageRef';
      parent = SubPackageParent;
      suspend;

      val = PackageWeight;
      id = id + 1;
      name = 'PackageWeight';
      parent = SubPackageParent;
      suspend;

      -- XXX KBI Asysty 20161213 co nie UPS nie przyjmuje wymiany danych elektroncznych kiedy rozmiary pczek sa uzupelnienoe
      -- a dokadnie podawalimy tylko wysokosc. A że musimy podawać do do Schenkera tutaj na razie na sztywno przekazuje 0.0000
      -- jak bedzie potrzeba to sie zbada o co chodzi z UPSwm
      --val = PackageLength;
      val = '0.0000';
      id = id + 1;
      name = 'PackageDimensionsLenght';
      parent = SubPackageParent;
      suspend;

      --val = PackageHeight;
      val = '0.0000';
      id = id + 1;
      name = 'PackageDimensionsHeight';
      parent = SubPackageParent;
      suspend;

      --val = PackageWidth;
      val = '0.0000';
      id = id + 1;
      name = 'PackageDimensionsWidth';
      parent = SubPackageParent;
      suspend;

      val = 'CM';
      id = id + 1;
      name = 'PackageDimensionsUnitOfMeasurement';
      parent = SubPackageParent;
      suspend;

      val = PackageUnitOfMeasurement;
      id = id + 1;
      name = 'PackageUnitOfMeasurement';
      parent = SubPackageParent;
      suspend;

      if (:PackageType = 2) then val = '30';
      else val = '02';
      id = id + 1;
      name = 'PackageType';
      parent = SubPackageParent;
      suspend;

--      val = PackageUnitOfMeasurement;
--      id = id + 1;
--      name = 'PackageUnitOfMeasurement';
--      parent = SubPackageParent;
--      suspend;

      if (coalesce(:PackageDeclaredValue,0) > 0) then
      begin
        val = PackageDeclaredValue;
        id = id + 1;
        name = 'PackageDeclaredValueMonetaryValue';
        parent = SubPackageParent;
        suspend;

        val = '01';
        id = id + 1;
        name = 'PackageDeclaredValueType';
        parent = SubPackageParent;
        suspend;

        val = 'PLN';
        id = id + 1;
        name = 'PackageDeclaredValueCurrencyCode';
        parent = SubPackageParent;
        suspend;
      end
    end
    parent = GrandParent;
end^
SET TERM ; ^
