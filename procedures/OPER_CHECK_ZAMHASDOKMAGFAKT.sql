--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_CHECK_ZAMHASDOKMAGFAKT(
      ZAM integer,
      OPERATOR integer)
  returns (
      STATUS integer,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable cnt integer;
begin
  select count(*) from
    DOKUMNAG join NAGZAM on (DOKUMNAG.ZAMOWIENIE = NAGZAM.REF)
    where (NAGZAM.REF = :zam or (NAGZAM.ORG_REF = :zam)) and DOKUMNAG.FAKTURA > 0 into :cnt;
    if(:cnt > 0) then begin
      status = 0;
      msg = 'Zamówienie posiada dok. WZ zafakturowane. Wystawienie dok. sprzedaży poprzez "Fakturowanie dok. WZ"';
    end else
      status = 1;
   if(:status = 1) then begin
     cnt = 0;
     select count(*) from
      DOKUMNAG G join NAGZAM on (G.ZAMOWIENIE = NAGZAM.REF)
      join DOKUMNAG K on (G.ref= k.refk)
      where (NAGZAM.REF = :zam or (NAGZAM.ORG_REF = :zam)) and G.faktura is null
      into :cnt;
      if(:cnt > 0) then begin
        status = 0;
        msg = 'Zamówienie posiada dok. WZ niezafakturowane, które mają wystawione dok. korekty ilosciowej. Wystawienie dok. sprzedaży poprzez "Fakturowanie dok. WZ"';
      end
   end
end^
SET TERM ; ^
