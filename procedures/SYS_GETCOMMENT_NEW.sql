--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_GETCOMMENT_NEW(
      OBJECTNAME varchar(255) CHARACTER SET UTF8                           ,
      OBJECTTYPE integer)
  returns (
      DESCRIPT blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           )
   as
declare variable tablename varchar(100);
		begin
  if(position('.' in :objectname) > 0) then
	begin

		tablename = substring(:objectname from 1 for position('.' in :objectname)-1);
    objectname = substring(:objectname from position('.' in :objectname)+1);
  end
  if(objecttype = 1) then
		select first 1 rdb$description from rdb$fields where rdb$field_name = :objectname into :descript;
  else if(objecttype = 2) then
		select first 1 rdb$description from rdb$generators where rdb$generator_name = :objectname into :descript;
  else if(objecttype = 3) then
		select first 1 rdb$description from rdb$exceptions where rdb$exception_name = :objectname into :descript;
  else if(objecttype = 4) then
		select first 1 rdb$description from rdb$relations where rdb$relation_name = :objectname and(rdb$relation_type is NULL or rdb$relation_type in (0,4,5)) into :descript;
  else if(objecttype = 5) then
		select first 1 rdb$description from rdb$relation_fields where rdb$relation_name = :tablename
																													and rdb$field_name = :objectname and rdb$view_context is null into :descript;
  else if(objecttype = 6) then
		select first 1 rdb$description from rdb$procedures where rdb$procedure_name = :objectname into :descript;
  else if(objecttype = 7) then
		select first 1 rdb$description from rdb$relations where rdb$relation_name = :objectname and rdb$relation_type = 1 into :descript;
  else if(objecttype = 8) then
		select first 1 rdb$description from rdb$triggers where rdb$trigger_name = :objectname into :descript;
  else if(objecttype = 9) then
		select first 1 rdb$description from rdb$procedures where rdb$procedure_name = :objectname into :descript;
  else if(objecttype = 10 or objecttype = 11 or objecttype = 12 or objecttype = 13)  then
		select first 1 rdb$description from rdb$indices where rdb$index_name = :objectname and rdb$relation_name = :tablename into :descript;
    else if(objecttype = 14) then
		select first 1 val from s_appini where section = :objectname and ident = 'Signature' into :descript;
  else if(objecttype = 15) then
		select  first 1 val from s_appini where section = :objectname and ident = 'Signature' into :descript;
  else if(objecttype = 16) then
		select first 1 val from s_appini where section = :objectname and ident = 'Signature' into :descript;
  else if(objecttype = 17) then
		select first 1 val from s_appini where section = :objectname and ident = 'Signature' into :descript;
  else if(objecttype = 18) then
		select first 1val from s_appini where section = :objectname and ident = 'Signature' into :descript;
  suspend;
end^
SET TERM ; ^
