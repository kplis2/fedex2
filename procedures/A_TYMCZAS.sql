--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE A_TYMCZAS as
declare variable ref integer;
begin
  for
    select ref from rkdoknag
      where stanowisko in ('01', '02')
      into :ref
  do
    execute procedure rkdok_obl_war(ref);

  for
    select ref
      from rkrapkas 
      where stanowisko = '01' or stanowisko = '02'
      order by dataod, numer
      into :ref
  do begin
    execute procedure RKRAP_OBL_WAR(ref);
  end
end^
SET TERM ; ^
