--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR92121_TEST_SELECT(
      SLEEP integer NOT NULL)
  returns (
      REF integer,
      ITEM varchar(100) CHARACTER SET UTF8                           ,
      TIMEST timestamp)
   as
declare variable currtime  timestamp;
declare variable starttime timestamp;
BEGIN
  FOR
    select * from pr92121_test
    INTO :REF,
         :ITEM,
         :TIMEST
  DO
  BEGIN
    SUSPEND;

    starttime = cast('now' as timestamp);
    currtime=starttime;
    while(datediff(MILLISECOND from starttime to currtime)<sleep) do
    begin
      currtime = cast('now' as timestamp);
    end
  END
END^
SET TERM ; ^
