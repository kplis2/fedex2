--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_FUN_DG071(
      PERIOD varchar(6) CHARACTER SET UTF8                           )
  returns (
      AMOUNT numeric(15,2))
   as
DECLARE VARIABLE DATAZM TIMESTAMP;
DECLARE VARIABLE FDAY TIMESTAMP;
DECLARE VARIABLE LDAY TIMESTAMP;
DECLARE VARIABLE ilosc numeric(14,2);
declare VARIABLE NUM NUMERIC(14,2);
declare variable DEN NUMERIC(14,2);
begin

  execute procedure datatookres(:PERIOD,0,0,0,1)
    returning_values :PERIOD, :DATAZM, :FDAY, :LDAY;
  ILOSC = 0;
  amount = 0;
  for select c.dimnum, C.dimden
    from employees e
    join emplcontracts c on (c.employee = e.ref
    and ((c.fromdate <= :LDAY and c.enddate >= :LDAY)
      or (c.fromdate <= :LDAY and c.enddate is null)
      )
    )
    join ECONTRTYPES t on (t.contrtype = c.econtrtype)
    where t.empltype =1
  into :NUM, :DEN
  DO BEGIN
    if (num is null) then
      num = 1;
    if (den is null) then
      den = 1;
    amount = amount + (num/den);
  END

  for select c.dimnum, C.dimden
    from employees e
    join emplcontracts c on (c.employee = e.ref
    and ((c.fromdate <= :FDAY and c.enddate >= :FDAY)
      or (c.fromdate <= :FDAY and c.enddate is null)
      )
    )
    join ECONTRTYPES t on (t.contrtype = c.econtrtype)
    where t.empltype =1
  into :NUM, :DEN
  DO BEGIN
    if (num is null) then
      num = 1;
    if (den is null) then
      den = 1;
    ilosc = ilosc + (num/den);
  END
  AMOUNT = (AMOUNT + ilosc)/2;
  AMOUNT = cast(AMOUNT as integer);
  suspend;
end^
SET TERM ; ^
