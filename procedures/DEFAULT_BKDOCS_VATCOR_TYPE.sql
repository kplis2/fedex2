--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DEFAULT_BKDOCS_VATCOR_TYPE(
      BKDOC BKDOCS_ID)
  returns (
      RET BKDOCTYPES_ID)
   as
declare variable vvtype smallint;
begin
  --Procedura zwraca domylnych typów korekt na podstawie badanie czy bkdocs pochodzi
  -- z rejestru VAT sprzedaży czy zakupu i szuka odpowiedniego typu dokumentu korygującego VAT
  -- jeżeli co nie nie uda zwraca wartosc ujemną
  select v.vtype
    from vatregs v join bkdocs b on (b.vatreg = v.symbol and b.company = v.company)
    where b.ref = :bkdoc
    into :vvtype;
  if (vvtype = 0 or vvtype = 3 ) then
    select bt.ref
      from bkdoctypes bt
      where bt.isvatcorrection = 1 and bt.bkreg = 'ZLEDLUGI'
        and bt.kind = iif(:vvtype = 0,2,1)
      into :ret;
  else
    ret = -1;
  suspend;
end^
SET TERM ; ^
