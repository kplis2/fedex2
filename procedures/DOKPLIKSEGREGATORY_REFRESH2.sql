--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKPLIKSEGREGATORY_REFRESH2(
      REFSEGREGATOR integer)
   as
declare variable DOKPLIKREF integer;
begin
  for select distinct ds.dokplik from dokpliksegregatory ds
    where ds.segregator = :refsegregator
  into :dokplikref
  do begin
    execute procedure DOKPLIKSEGREGATORY_REFRESH (:dokplikref);
  end
end^
SET TERM ; ^
