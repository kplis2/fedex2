--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STKRTAB_POZINFO(
      TABELA integer,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      ODDATY timestamp,
      DODATY timestamp,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      MINDNIDOST integer,
      MINCYKLDOST integer,
      MINREZ numeric(14,2),
      MINREZTYP smallint,
      MINROZNICA numeric(14,4),
      TYLKOZEWNETRZNE smallint,
      PPILOSC numeric(14,4),
      PPWARTOSC numeric(14,2),
      PRILOSC numeric(14,4),
      PRWARTOSC numeric(14,2))
  returns (
      REF integer,
      PILOSC numeric(14,4),
      PWARTOSC numeric(14,2),
      RILOSC numeric(14,4),
      RWARTOSC numeric(14,2),
      DZIENNASPR numeric(14,4),
      DNICYKL integer,
      DNIDOST integer,
      MINTAB numeric(14,4),
      MAXTAB numeric(14,4),
      REZ numeric(14,4))
   as
declare variable ilosc numeric(14,4);
declare variable ilkrytyczna numeric(14,4);
declare variable wartosc numeric(14,2);
declare variable dokladnosc numeric(14,4);
declare variable dokladnoscm integer;
declare variable dostawcadnidost integer;
declare variable aktywny integer;
begin
  pilosc = 0;
  pwartosc = 0;
  rilosc = 0;
  rwartosc = 0;
  select MIARa.dokladnosc, TOWARY.akt
    from TOWARY join miara on (TOWARY.miara = miara.miara)
    where TOWARY.ktm = :ktm
    into :dokladnoscm, :aktywny;
  if(:dokladnoscm is null) then dokladnoscm = 2;
  dokladnosc = 1;
  while(:dokladnoscm > 0) do begin
    dokladnosc = :dokladnosc * 10;
    dokladnoscm = :dokladnoscm - 1;
  end
  /* jesli towar nieaktywny to nie zamawiamy */
  if(:aktywny=0) then begin
    dziennaspr = 0;
    mintab = 0;
    maxtab = 0;
    exit;
  end
  if(:ppilosc is not null) then begin
    pilosc = ppilosc;
    pwartosc = ppwartosc;
    rilosc = prilosc;
    rwartosc = prwartosc;
  end else begin
    execute procedure MAG_OBROTY(:magazyn,:ktm,:wersja,:oddaty,:dodaty)
      returning_values :ilosc,:wartosc,:pilosc,:pwartosc,:rilosc,:rwartosc,:ilosc,:wartosc,:ilosc,:wartosc;
  end
 /*obliczanie dnicykl i dostcykl i stanów*/
 select DOSTAWCY.dnicykl, DOSTAWCY.dnireal, DOSTCEN.dnireal
   from DOSTAWCy
   join DOSTCEN on (DOSTCEN.dostawca = dostawcy.ref)
   where DOSTCEN.ktm = :ktm and DOSTCEN.wersja = :wersja and DOSTCEN.gl = 1
   into :dnicykl, :dostawcadnidost, :dnidost;
-- if(:dnicykl is null) then dnicykl = :srdnicykl;
 if(:dnidost is null or :dnidost=0) then dnidost = :dostawcadnidost;
 if(:dnidost is null) then dnidost = :mindnidost;
 if(:dnicykl < :mincykldost) then dnicykl = :mincykldost;
 if(:dnidost < :mindnidost) then dnidost = :mindnidost;
 if((:rilosc > 0) and ((:dodaty - :oddaty + 1) > 0)) then
   dziennaspr = rilosc / (:dodaty - oddaty + 1);
 else
   dziennaspr = 0;
 /* obliczenie ilosci krytycznej */
 ilkrytyczna = 0;
 select min(ilkrytyczna) from stkrpoz where tabela = :tabela and ktm = :ktm and wersja = :wersja into :ilkrytyczna;
 if(ilkrytyczna is null) then ilkrytyczna = 0;
 /*okreslenie stanu maksymalnego*/
 maxtab = :dziennaspr * :dnicykl;
 if(:maxtab is null) then maxtab = 0;
 if(:minreztyp = 0 and :minrez < 100 and :minrez > 0) then begin/*procent*/
   rez = :maxtab / (1 - :minrez/100) - :maxtab;
   rez = cast((:rez * :dokladnosc) as integer) / :dokladnosc;
   if(:rez < 0) then rez = 0;
   if(:rez < :ilkrytyczna) then rez = :ilkrytyczna;
   maxtab = :maxtab + :rez;
 end else if(:minreztyp =1 and :dziennaspr > 0 and :minrez > 0)then begin
   rez = :minrez * :dziennaspr;
   rez = cast((:rez * :dokladnosc) as integer) / :dokladnosc;
   if(:rez < 0) then rez = 0;
   if(:rez < :ilkrytyczna) then rez = :ilkrytyczna;
   maxtab = :maxtab + :rez;
 end else begin
   rez = :ilkrytyczna;
   maxtab = :maxtab + :rez;
 end
 if(:maxtab is null) then maxtab = 0;
 /*okrelenie stanu minimalnego*/
 mintab = 0;
 if(:dnidost < :dnicykl) then
   mintab = :dziennaspr * :dnidost;
 if(:mintab < :rez) then
   mintab = :rez;
 if(:minroznica > 0 and :minroznica < 100) then begin
   if(:mintab > (:maxtab * (1-:minroznica /100))) then
     maxtab = :mintab / (1 - :minroznica /100);
 end
 /*obciecie jednostek magasynowych - ogonki*/
 mintab = cast((:mintab * :dokladnosc) as integer)/ :dokladnosc;
 maxtab = cast((:maxtab * :dokladnosc) as integer)/ :dokladnosc;
 if(:maxtab <= :mintab) then
   maxtab = :mintab + 1/:dokladnosc;
 ref = 1;
 suspend;
end^
SET TERM ; ^
