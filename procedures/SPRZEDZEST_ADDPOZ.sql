--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SPRZEDZEST_ADDPOZ(
      ZESTAWIENIE integer,
      SPRZEDAWCA integer,
      ZAM integer,
      FAK integer,
      TYP integer,
      DATA timestamp,
      WARTNET numeric(14,2),
      WARTBRU numeric(14,2),
      KOSZT numeric(14,2),
      KOREKTA integer,
      ODBRUTTO integer,
      PROCENTDEF numeric(14,2),
      WSPOLCZYNNIKKOR numeric(14,2),
      VAL1 numeric(14,2),
      VAL2 numeric(14,2),
      VAL3 numeric(14,2))
  returns (
      CHANGED integer)
   as
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE ZMIEN SMALLINT;
DECLARE VARIABLE OWARTNET NUMERIC(14,2);
DECLARE VARIABLE OWARTBRU NUMERIC(14,2);
DECLARE VARIABLE ODATA TIMESTAMP;
DECLARE VARIABLE PODSTAWAPRO NUMERIC(14,2);
DECLARE VARIABLE PROCENT NUMERIC(14,2);
DECLARE VARIABLE PROWIZJA NUMERIC(14,2);
DECLARE VARIABLE TYPPRO SMALLINT;
DECLARE VARIABLE TRYB INTEGER;
DECLARE VARIABLE OKOSZT NUMERIC(14,2);
DECLARE VARIABLE OPROCENTDEF NUMERIC(14,2);
DECLARE VARIABLE OWSPOLCZYNNIKKOR NUMERIC(14,2);
begin
  zmien = 0;
  changed = 0;
  if(:zam is null) then zam = 0;
  if(:fak is null) then fak = 0;
  if(:typ is null) then typ = 0;
  if(:koszt is null) then koszt = 0;
  if(:wspolczynnikkor is null) then wspolczynnikkor = 1;
  select SPRZEDAWCY.proprocent, tryb
    from SPRZEDAWCY
    where REF=:sprzedawca
    into :typpro, :tryb;
  if(:tryb is null) then tryb = 3;
  if(:typpro is null) then typpro = 0;
  select count(*) from SPRZEDZESTP
    where ZESTAWIENIE=:zestawienie AND
          ((ZAM=:zam) or (:zam = 0)) AND
          ((fak=:fak) or (:fak = 0))
          and TYP = :typ
    into :cnt;

  if(:cnt is null) then cnt = 0;






  if(:cnt = 0) then zmien = 1;
  else begin
    /* sprawdzenie, czy zaszly jakies zmiany*/
     select WARTNET, WARTBRU,KOSZT, PROCENT, WSPOLCZYNNIKKOR, DATA
     from SPRZEDZESTP where ZESTAWIENIE=:zestawienie AND
          ((ZAM=:zam) or (:zam = 0)) AND
          ((fak=:fak) or (:fak = 0)) and
          TYP =:typ
      into :owartnet, :owartbru, :okoszt, :oprocentdef, :owspolczynnikkor, :odata ;
      if((:wartnet <> :owartnet) or (:wartbru <> :owartbru)
         or (:data <> :odata) or (:okoszt <> :koszt)
         or (:wspolczynnikkor <> :owspolczynnikkor and :owspolczynnikkor is not null)
         or (:procentdef is not null and :procentdef <> :oprocentdef)
      )then zmien = 1;
      zmien = 1;
  end
  /* blokowanie naliczania korekt */
  if(:korekta = 1 and :tryb < 2) then zmien = -1;
  if(:zmien > 0) then begin
    /*obliczenie prowizji*/
    if(:tryb = 1 or (:tryb = 3))then
      podstawapro = :wartnet - :koszt;
    else begin
      podstawapro = :wartnet;
      if(:odbrutto >0) then
        podstawapro=:wartbru;
    end
    prowizja = 0;
    procent = 0;
    for select PROWIZJA, PROCENT
      from SPRZEDPROG
      where WART_MIN <= :podstawapro AND SPRZEDAWCA=:sprzedawca
      order by wart_min
      into :prowizja, :procent
    do
      prowizja=:prowizja;
    if(:PROCENTDEF > 0) then
      procent = :procentdef;
    if(:typpro = 0) then
       prowizja = (:podstawapro * :procent)/100;
    prowizja = :prowizja * :wspolczynnikkor;
    if(:cnt > 0) then
      update SPRZEDZESTP set DATA = :data, WARTNET = :wartnet, WARTBRU=:wartbru,
        KOSZT = :KOSZT, ZYSK = :WARTNET - :koszt, prowizja = :prowizja , CHECKED=1,
        PROCENT = :procent, WSPOLCZYNNIKKOR = :wspolczynnikkor, VAL1 = :val1, val2 = :val2, val3 = :val3
      where ZESTAWIENIE=:zestawienie AND
          ((ZAM=:zam) or (:zam = 0)) AND
          ((fak=:fak) or (:fak = 0)) and
          TYP=:typ;
    else   begin
      insert into SPRZEDZESTP(ZESTAWIENIE, ZAM, FAK, DATA, TYP, WARTNET, WARTBRU,KOSZT, ZYSK,PROWIZJA,CHECKED,
          PROCENT, WSPOLCZYNNIKKOR, val1, val2, val3)
        values(:zestawienie, :zam, :fak, :data, :typ, :wartnet, :wartbru, :koszt, :wartnet - :koszt, :prowizja,1,
          :procent, :wspolczynnikkor, :val1, :val2, :val3);
             end
    changed = 1;
  end else
   if(zmien <> -1) then
    update SPRZEDZESTP set CHECKED=1 where ZESTAWIENIE=:zestawienie AND
          ((ZAM=:zam) or (:zam = 0)) AND
          ((fak=:fak) or (:fak = 0))
          and TYP = :typ;
end^
SET TERM ; ^
