--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REZ_BLOK_DEREALIZE(
      DOKUMMAG integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      CENA numeric(14,4),
      DOSTAWA integer,
      NEWIL numeric(14,4))
   as
declare variable ilzreal numeric(14,4);
declare variable ilcofnij numeric(14,4);
declare variable cofnij numeric(14,4);
declare variable pozref integer;
declare variable ilosc numeric(14,4);
declare variable status char(1);
declare variable cnt integer;
declare variable newstatus varchar(1);
declare variable zam integer;
declare variable pozzamref integer ;
begin
  select max(ZAMOWIENIE),max(stanyrez.pozzam),sum(ilosc)
  from STANYREZ where DOKUMMAG=:DOKUMMAG and KTM=:KTM and WERSJA=:WERSJA and
    ((cena =:cena) or(cena is null) or(cena = 0)) and
    ((dostawa=:dostawa) or(dostawa = 0) or(dostawa is null) or(:dostawa = 0) or (:dostawa is null))
    and zreal = 1
    into :zam,:pozzamref, :ilzreal;
  if(:pozzamref is not null) then
    select stan from POZZAM where ref=:pozzamref into :newstatus;
  if((:newstatus is null or (:newstatus = ' ')) and :zam is not null) then
    select STAN from NAGZAM where REF=:zam into :newstatus;
  if(:ilzreal is null) then ilzreal = 0;
  if(:ilzreal > :newil) then begin
    ilcofnij = :ilzreal - :newil;
    if(:newstatus = '') then
       exception DEREALIZEBLOK_BRAKNEWSTATUS;
    for select ZAMOWIENIE, POZZAM,ILOSC, STATUS
       from STANYREZ
       where DOKUMMAG=:DOKUMMAG and KTM=:KTM and WERSJA=:WERSJA
         and ((cena =:cena) or(cena is null) or(cena = 0))
         and ((dostawa=:dostawa) or(dostawa = 0) or(dostawa is null) or(:dostawa = 0) or (:dostawa is null))
         and zreal = 1 order by STATUS /* najpierw B, potem Z */
       into :zam, :pozref, :ilosc, :STATUS
    do begin
     if(:ilcofnij > 0) then begin
       if(:ilcofnij > :ilosc) then cofnij = :ilosc;
       else cofnij = :ilcofnij;
       if(:cofnij > 0) then begin
         cnt = null;
         select count(*) from STANYREZ where ZAMOWIENIE = :zam and POZZAM=:pozref and ((DOKUMMAG is null) or (DOKUMMAG = 0))
           and STATUS=:newstatus and zreal = 0
           into  :cnt;
         if(:cnt is null) then cnt = 0;
         if(:cnt > 0) then
           update STANYREZ set ILOSC = ILOSC + :cofnij where ZAMOWIENIE = :zam and  POZZAM=:pozref and ((DOKUMMAG is null) or (DOKUMMAG = 0))
             and STATUS=:newstatus and zreal = 0;
         else if (:newstatus <> 'N') then begin
           insert into STANYREZ(POZZAM,STATUS,ZREAL,DOKUMMAG,MAGAZYN,KTM,WERSJA,ZAMOWIENIE,DATA,ILOSC,CENA,DOSTAWA,DATABL,PRIORYTET)
            select POZZAM,STATUS,0,null,MAGAZYN,KTM,WERSJA,ZAMOWIENIE,DATA,:cofnij,CENA,DOSTAWA,DATABL,PRIORYTET from STANYREZ
             where POZZAM=:pozref AND STATUS = :status AND ZREAL = 1 AND DOKUMMAG=:DOKUMMAG;
         end
         if(:cofnij = :ilosc) then
           delete from STANYREZ where ZAMOWIENIE = :zam and  POZZAM=:pozref and STATUS=:status and ZREAL=1 AND DOKUMMAG=:DOKUMMAG;
         else if(:cofnij > 0) then
           update STANYREZ set ILOSC = ILOSC-:cofnij where ZAMOWIENIE = :zam and  POZZAM=:pozref and STATUS=:status and ZREAL=1 AND DOKUMMAG=:DOKUMMAG;
         ilcofnij = :ilcofnij - :cofnij;
       end
     end
    end
  end
end^
SET TERM ; ^
