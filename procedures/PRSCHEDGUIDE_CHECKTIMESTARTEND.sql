--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDE_CHECKTIMESTARTEND(
      GUIDE integer)
   as
declare variable starttime timestamp;
declare variable endtime timestamp;
declare variable schedzam integer;
begin
  select min(starttime), max(endtime)
    from PRSCHEDOPERS
    where GUIDE = :guide
    into :starttime, :endtime;
  update PRSCHEDGUIDES set starttime = :starttime, endtime = :endtime
  where ref = :guide;
  select prschedguides.prschedzam from prschedguides where ref=:guide into :schedzam;
  execute procedure prschedzam_checktimestartend(:schedzam);
end^
SET TERM ; ^
