--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WFWORKFLOW_STATUS_SET(
      WFWORKFLOW integer)
   as
declare variable wfstatus smallint;
declare variable zadminstan smallint;
declare variable grupafazy smallint;
begin
--sprawdzanie czy obieg ma/moze miec faze archiwalna
  select min(c.grupa), min(z.stan)
    from wfworkflows w
    left join cfazy c on w.faza = c.ref
    left join zadania z on w.ref = z.wfworkflow
    where w.ref = :wfworkflow
  into :grupafazy, :zadminstan;
  if(grupafazy = 2) then begin
    if(zadminstan = 0) then exception universal 'Istnieją zadania niewykonane do obiegu. Zakończenie obiegu niedozwolone.';
    else wfstatus = 3;
  end else if(zadminstan = 0) then wfstatus = 1;
  else if(zadminstan = 1) then wfstatus = 2;
  else wfstatus = 0;
  update wfworkflows set status = :wfstatus where ref = :wfworkflow and status <> :wfstatus;
end^
SET TERM ; ^
