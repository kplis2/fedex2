--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_EMPLCONTRACT_ANNEX(
      ANNEX integer)
  returns (
      INFO1 varchar(1024) CHARACTER SET UTF8                           ,
      INFO2 varchar(1024) CHARACTER SET UTF8                           ,
      INFO3 varchar(1024) CHARACTER SET UTF8                           ,
      INFOREGON varchar(1024) CHARACTER SET UTF8                           ,
      INFONIP varchar(1024) CHARACTER SET UTF8                           ,
      PODTEL varchar(1024) CHARACTER SET UTF8                           ,
      PODFAX varchar(1024) CHARACTER SET UTF8                           ,
      PODMIEJ varchar(1024) CHARACTER SET UTF8                           ,
      PERSONNAMES varchar(120) CHARACTER SET UTF8                           ,
      ANNEXDATE date,
      CONTRDATE date,
      PESEL varchar(30) CHARACTER SET UTF8                           ,
      STPREF varchar(30) CHARACTER SET UTF8                           ,
      STREET varchar(30) CHARACTER SET UTF8                           ,
      HOUSENO varchar(30) CHARACTER SET UTF8                           ,
      LOCALNO varchar(30) CHARACTER SET UTF8                           ,
      POSTCODE varchar(30) CHARACTER SET UTF8                           ,
      CITY varchar(30) CHARACTER SET UTF8                           ,
      BANNEX varchar(30) CHARACTER SET UTF8                           )
   as
declare variable b_annex smallint;
begin
  select k.wartosc from konfig k where k.akronim = 'INFO1' into info1;
  select k.wartosc from konfig k where k.akronim = 'INFO2' into info2;
  select k.wartosc from konfig k where k.akronim = 'INFO3' into info3;

  select k.wartosc from konfig k where k.akronim = 'INFOREGON' into inforegon;
  select k.wartosc from konfig k where k.akronim = 'INFONIP' into infonip;

  select case when k.wartosc is null then '' else 'tel. ' || k.wartosc end from konfig k where k.akronim = 'PODTEL' into podtel;
  select case when k.wartosc is null then '' else 'fax. ' || k.wartosc end from konfig k where k.akronim = 'PODFAX' into podfax;

  select k.wartosc from konfig k where k.akronim = 'PODMIEJ' into podmiej;

  select first 1 e.personnames, cast(a.fromdate as date), cast(c.contrdate as date),
    p.pesel, ad.stpref, ad.street, ad.houseno, ad.localno, ad.postcode, ad.city
    from econtrannexes a
    join employees e on a.employee = e.ref
    join persons p on p.ref = e.person
    join emplcontracts c on c.ref = a.emplcontract
    left join epersaddr ad on (p.ref = ad.person and ad.addrtype = 0)
    where a.ref = :annex
    into :personnames, :annexdate, :contrdate, :pesel, :stpref, :street, :houseno, :localno, :postcode, :city;

    pesel = coalesce('','PESEL: '||:pesel);
    stpref = coalesce('',:stpref);
    street = coalesce('',:street);
    houseno = coalesce('',:houseno);
    localno = coalesce('','/'||:localno);
    postcode = coalesce('',:postcode);
    city = coalesce('',:city);

    select first 1 bannex from rpt_e_annex(:annex) into :b_annex;
    if(b_annex <> -1) then
      bannex = ' wraz z późniejszymi aneksami';
    else
      bannex = '';

  suspend;
end^
SET TERM ; ^
