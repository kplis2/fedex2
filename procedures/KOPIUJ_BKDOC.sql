--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KOPIUJ_BKDOC(
      BKDOC integer,
      TRYBK integer,
      INSYMBOL varchar(20) CHARACTER SET UTF8                           ,
      INREJESTR varchar(10) CHARACTER SET UTF8                           ,
      INOKRES varchar(6) CHARACTER SET UTF8                           ,
      INTYP integer)
  returns (
      BKDOCNEW integer)
   as
declare variable DESCRIPT varchar(80);
declare variable DOCDATE timestamp;
declare variable TRANSDATE timestamp;
declare variable VATDATE timestamp;
declare variable PAYDAY timestamp;
declare variable REGOPER integer;
declare variable VATREG varchar(10);
declare variable DICTDEF integer;
declare variable DICTPOS integer;
declare variable NIP varchar(15);
declare variable CONTRACTOR varchar(255);
declare variable CURRENCY varchar(3);
declare variable CURRATE numeric(14,4);
declare variable CORRSYMBOL varchar(20);
declare variable PAYMENTMETH integer;
declare variable COSTSALES numeric(14,2);
declare variable VATPERIOD varchar(6);
declare variable VNUMBER integer;
declare variable OTABLE varchar(40);
declare variable OREF integer;
declare variable AUTODOC smallint;
declare variable BRANCH varchar(10);
declare variable COMPANY integer;
declare variable CURVATRATE numeric(14,4);
declare variable DECACCOUNT ACCOUNT_ID;
declare variable DECNUMBER integer;
declare variable DECDESCRIPT varchar(80);
declare variable DECCURRENCY varchar(3);
declare variable DECRATE numeric(14,4);
declare variable DECSIDE smallint;
declare variable DECDEBIT numeric(15,2);
declare variable DECCREDIT numeric(15,2);
declare variable DECCURRDEBIT numeric(15,2);
declare variable DECCURRCREDIT numeric(15,2);
declare variable DECSETTLEMENT varchar(20);
declare variable DECOPENDATE timestamp;
declare variable DECPAYDAY timestamp;
declare variable DECSETTLCREATE smallint;
declare variable DECFSOPERTYPE integer;
declare variable DECREE integer;
declare variable BKNUMBER integer;
declare variable BKNETV numeric(14,2);
declare variable BKVATE numeric(14,2);
declare variable BKGROSSV numeric(14,2);
declare variable BKTAXGR varchar(10);
declare variable BKVATGR varchar(5);
declare variable BKVATREG varchar(10);
declare variable BKCURRNETV numeric(14,2);
declare variable BKVATVF numeric(14,2);
declare variable BKVATGRF varchar(5);
declare variable BKNET numeric(14,2);
declare variable BKVATPOS integer;
declare variable DECREENEW integer;
declare variable DISTKONTO ACCOUNT_ID;
declare variable DISTDEBIT numeric(15,2);
declare variable DISTCREDIT numeric(15,2);
declare variable DISTNUMBER integer;
declare variable OKRES varchar(6);
declare variable DISTTRANSDATE timestamp;
declare variable DISTDICTDEF integer;
declare variable DISTDICTPOS integer;
declare variable DISTCOMPANY integer;
declare variable SYMBOL1 SYMBOLDIST_ID;
declare variable SYMBOL2 SYMBOLDIST_ID;
declare variable SYMBOL3 SYMBOLDIST_ID;
declare variable SYMBOL4 SYMBOLDIST_ID;
declare variable SYMBOL5 SYMBOLDIST_ID;
declare variable SYMBOL6 SYMBOLDIST_ID;
declare variable SLOWNIK1 integer;
declare variable SLOWNIK2 integer;
declare variable SLOWNIK3 integer;
declare variable SLOWNIK4 integer;
declare variable SLOWNIK5 integer;
declare variable SLOWNIK6 integer;
declare variable DISTSTATUS smallint;
declare variable DISTDESCRIPT varchar(80);
declare variable TMPDEBIT integer;
declare variable COSTDISTRIBUTION integer;
declare variable CDTYPE smallint;
declare variable MULTI smallint;
declare variable BKVATV MONEY;
declare variable BKDEBITED SMALLINT_ID;
declare variable BKDEBITTYPE SMALLINT_ID;
declare variable BKDEBITPROCENT PROCENT;
declare variable DOCTYPEKIND smallint;
begin

  select  descript, docdate, transdate, vatdate, payday, regoper, vatreg, dictdef, dictpos, nip, contractor,
         currency, currate, corrsymbol, paymentmeth, costsales, vatperiod, vnumber, otable, oref, autodoc, branch,
         company, curvatrate, costdistribution, cdtype
    from bkdocs
    where ref = :bkdoc
    into  :descript, :docdate, :transdate, :vatdate, :payday, :regoper, :vatreg, :dictdef, :dictpos, :nip,
         :contractor, :currency, :currate, :corrsymbol, :paymentmeth, :costsales, :vatperiod, :vnumber, :otable, :oref,
         :autodoc, :branch, :company, :curvatrate, :costdistribution, :cdtype;

  execute procedure gen_ref('BKDOCS')
      returning_values :bkdocnew;

  -- w przypadku storno bkdocs jest dokumentem ĹşrĂłdlowym
  if((:trybk=1) or (:trybk=2)) then begin
    otable = 'BKDOCS';
    oref = :bkdoc;
  end

--< BS61553
  doctypekind = null;
  select b.kind from bkdoctypes b where b.ref = :intyp
    into :doctypekind;

  if(coalesce(doctypekind,0) = 0) then begin
    vatreg = null;
    vatdate = null;
    vatperiod = null;
    vnumber = null;
    curvatrate = null;
  end
-- >

  insert into bkdocs (ref, period, bkreg, doctype, symbol, descript, docdate, transdate, vatdate, payday,
                      regoper, vatreg, dictdef, dictpos, nip, contractor, currency, currate, corrsymbol, paymentmeth,
                      costsales, vatperiod, vnumber, otable, oref, autodoc, branch, company, curvatrate,
                      costdistribution, cdtype)
    values (:bkdocnew, :inokres, :inrejestr, :intyp, :insymbol, :descript, :docdate, :transdate, :vatdate,
            :payday, :regoper, :vatreg, :dictdef, :dictpos, :nip, :contractor, :currency, :currate, :corrsymbol,
            :paymentmeth, :costsales, :vatperiod, :vnumber, :otable, :oref, :autodoc, :branch, :company, :curvatrate,
            :costdistribution, :cdtype);

  for
    select ref, account, number, descript, currency, rate, side, debit, credit, currdebit, currcredit, settlement,
           opendate, payday, coalesce(settlcreate,0), fsopertype, dist1ddef, dist1symbol, dist2ddef, dist2symbol, dist3ddef,
           dist3symbol, dist4ddef, dist4symbol, dist5ddef, dist5symbol, dist6ddef, dist6symbol
      from decrees
      where bkdoc = :bkdoc
      order by number
      into :decree, :decaccount, :decnumber, :decdescript, :deccurrency, :decrate, :decside, :decdebit, :deccredit,
           :deccurrdebit, :deccurrcredit, :decsettlement, :decopendate, :decpayday, :decsettlcreate, :decfsopertype,
           :slownik1, :symbol1, :slownik2, :symbol2, :slownik3, :symbol3, :slownik4, :symbol4, :slownik5, :symbol5,
           :slownik6, :symbol6
  do begin

    execute procedure gen_ref('DECREES')
        returning_values :decreenew;

    if (trybk = 1) then begin
      tmpdebit = :decdebit;
      decdebit = :deccredit;
      deccredit = :tmpdebit;
      tmpdebit = :deccurrdebit;
      deccurrdebit = :deccurrcredit;
      deccurrcredit = :tmpdebit;
      decside = abs(:decside - 1);
    end
    else if (trybk = 2) then begin
      decdebit = -:decdebit;
      deccredit = -:deccredit;
      deccurrdebit = -:deccurrdebit;
      deccurrcredit = -:deccurrcredit;
    end
    else if (trybk is null or trybk not in (0, 1, 2)) then
      exception test_break 'Niepoprawny tryb kopiowania';

    insert into decrees (ref, account, number, descript, currency, rate, side, bkdoc, debit, credit, currdebit,
                         currcredit, settlement, opendate, payday, settlcreate, fsopertype, dist1ddef, dist1symbol,
                         dist2ddef, dist2symbol, dist3ddef, dist3symbol, dist4ddef, dist4symbol, dist5ddef, dist5symbol,
                         dist6ddef, dist6symbol)
      values (:decreenew, :decaccount, :decnumber, :decdescript, :deccurrency, :decrate, :decside, :bkdocnew, :decdebit,
              :deccredit, :deccurrdebit, :deccurrcredit,  (case when :decsettlcreate = 1 then '' else :insymbol end) , :decopendate, :decpayday,
              :decsettlcreate, :decfsopertype, :slownik1, :symbol1, :slownik2, :symbol2, :slownik3, :symbol3, :slownik4,
              :symbol4, :slownik5, :symbol5, :slownik6, :symbol6);

 --lgabryel+
     select count(d.ref)
     from distpos d join slodef s on (d.tempdictdef = s.ref or (d.tempdictdef2 = s.ref ) or (d.tempdictdef3 = s.ref )
       or (d.tempdictdef4 = s.ref ) or (d.tempdictdef5 = s.ref ) or (d.tempdictdef6 = s.ref )    )
     where d.decrees = :decree and s.multidist=1
     into :multi;

   if(:multi>0) then
   begin
     for
       select account, debit, credit, number, period, transdate, dictdef, dictpos, company, tempsymbol, tempsymbol2,
              tempsymbol3, tempsymbol4, tempsymbol5, tempdictdef, tempdictdef2, tempdictdef3, tempdictdef4, tempdictdef5,
              status, descript, tempsymbol6, tempdictdef6
         from distpos d
         where d.decrees = :decree
         order by number
         into :distkonto, :distdebit, :distcredit, :distnumber, :okres, :disttransdate, :distdictdef, :distdictpos,
              :distcompany, :symbol1, :symbol2, :symbol3, :symbol4, :symbol5, :slownik1, :slownik2, :slownik3, :slownik4,
              :slownik5, :diststatus, :distdescript, :symbol6, :slownik6
     do
       begin

       if (trybk = 1) then begin
         tmpdebit = :distdebit;
         distdebit = :distcredit;
         distcredit = :tmpdebit;
       end
       else if (trybk = 2) then begin
         distdebit = -:distdebit;
         distcredit = -:distcredit;
       end
       else if (trybk is null or trybk not in (0, 1, 2)) then
         exception test_break 'Niepoprawny tryb kopiowania';

       insert into distpos (decrees, bkdoc, account, debit, credit, number, period, transdate, dictdef, dictpos, company,
                           tempsymbol, tempsymbol2, tempsymbol3, tempsymbol4, tempsymbol5, tempdictdef, tempdictdef2,
                           tempdictdef3, tempdictdef4, tempdictdef5, status, descript, tempsymbol6, tempdictdef6)
         values (:decreenew, :bkdocnew, :distkonto, :distdebit, :distcredit, :distnumber, :okres, :disttransdate,
                 :distdictdef, :distdictpos, :distcompany, :symbol1, :symbol2, :symbol3, :symbol4, :symbol5, :slownik1,
                 :slownik2, :slownik3, :slownik4, :slownik5, :diststatus, :distdescript, :symbol6, :slownik6);
    end
   end
--lgabryel-
  end
  if(doctypekind <> 0) then begin --BS61663
    for
      select number, netv, vate, vatv, debited, debittype, debitprocent, grossv, taxgr, vatgr, vatreg, currnetv, vatvf, vatgrf,
        net
        from bkvatpos
        where bkdoc = :bkdoc
        order by number
        into :bknumber, :bknetv, :bkvate, :bkvatv, :bkdebited, :bkdebittype, :bkdebitprocent, :bkgrossv, :bktaxgr, :bkvatgr, :bkvatreg, :bkcurrnetv, :bkvatvf, :bkvatgrf,
             :bknet
    do begin
  
      if (trybk in (1, 2)) then begin
        bknetv = -:bknetv;
        bkvate = -:bkvate;
        bkgrossv = -:bkgrossv;
        bkcurrnetv = -:bkcurrnetv;
        bkvatvf = -:bkvatvf;
        bknet = -:bknet;
        bkvatv = -:bkvatv; 
      end
      execute procedure gen_ref('BKVATPOS')
          returning_values :bkvatpos;
      insert into bkvatpos (ref, bkdoc, number, netv, vate, vatv, debited, debittype, debitprocent, grossv, taxgr, vatgr, vatreg, currnetv, vatvf, vatgrf, net)
        values (:bkvatpos, :bkdocnew, :bknumber, :bknetv, :bkvate, :bkvatv, :bkdebited, :bkdebittype, :bkdebitprocent, :bkgrossv, :bktaxgr, :bkvatgr, :bkvatreg, :bkcurrnetv,
                :bkvatvf, :bkvatgrf, :bknet);
    end
  end
end^
SET TERM ; ^
