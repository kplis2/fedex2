--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_WMS_CHECK_LOT_AND_VERS_4_FLOW(
      MWSCONSTLOC_IN MWSCONSTLOCS_ID,
      VERS_IN WERSJE_ID,
      X_PARTIA DATE_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable flow_group integer_id;
declare variable VERS_act WERSJE_ID;
declare variable x_partia_act string255;
declare variable CHECKDOST SMALLINT_ID;
declare variable count_x_partia SMALLINT_ID;
declare variable constlocsymb string255;
begin
--exception universal mwsconstloc_in ||'  '||vers_in|| ' '||x_partia;
--msg = mwsconstloc_in ||'  '||vers_in|| ' '||x_partia;
--status = 0;
  if (MWSCONSTLOC_IN is null) then
  begin
    STATUS=0;
    msg='Brak podanej lokacji';
    suspend;
    exit;
  end else
    select symbol from mwsconstlocs where ref=:mwsconstloc_in into :constlocsymb;
  if (VERS_IN is null) then
  begin
    STATUS=0;
    msg='Brak podanej wersji';     
    suspend;
    exit;
  end
/*
  if (x_partia is null) then
  begin
    STATUS=0;
    msg='Brak podanej partii';
    suspend;
    exit;
  end
  */

  select  c.x_flow_group
    from mwsconstlocs c
    where c.ref = :mwsconstloc_in
    into  :flow_group;

  if (:flow_group is not null) then
  begin
    select min(s.vers), min(s.x_partia),
           count(distinct s.x_partia), min(w.x_mws_partie)
      from mwsstock s
        join mwsconstlocs c on (c.ref = s.mwsconstloc)
        join wersje w on (w.ref = s.vers)
      where c.x_flow_group=:flow_group
      into :VERS_act, :x_partia_act,
           :count_x_partia, :CHECKDOST;

    if (VERS_act is not null) then    --jest cos na tej lokacji przeplywowej
    begin

      if (vers_act <> vers_in) then
      begin
        STATUS=0;
        msg='Na lokacji jest inny wersja towaru.';
      end
      else begin
    
        if (CHECKDOST=0)then
        begin
          STATUS=1;
          msg='OK';
        end
        else if (count_x_partia>1)then
        begin
          STATUS=0;
          msg='Na lokacji znajduje sie towar z różnych partii, którego aktualnie nie można mieszać.';
        end
        else if (x_partia_act<>x_partia)then
        begin
          STATUS=0;
          msg='Na lokacji znajduje sie towar z innej partii, nie można mieszać. P:'||:constlocsymb||' P:'||x_partia_act||' Partia_in:'||:x_partia;
        end else
        begin
          STATUS=1;
          msg='OK';
        end

      end

    end else
    begin  --nic nie ma na lokacji
      STATUS=1;
      msg='Lokacja pusta';
    end
  end else     --lokacja nieprzeplywowa
  begin
    STATUS=1;
    msg='Lokacja nieprzeplywowa';
  end
  
  suspend;
end^
SET TERM ; ^
