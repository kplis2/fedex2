--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_CHECK_CLEARING_ACCOUNTS(
      ACCOUNT ACCOUNT_ID,
      FROMDATE timestamp,
      TODATE timestamp,
      COMPANY integer)
  returns (
      LP integer,
      BKREF integer,
      BKREG varchar(10) CHARACTER SET UTF8                           ,
      BKSYMBOL varchar(40) CHARACTER SET UTF8                           ,
      BKDATE timestamp,
      DEBIT numeric(14,2),
      CREDIT numeric(14,2),
      SALDO numeric(14,2),
      KONTRAHENT varchar(255) CHARACTER SET UTF8                           ,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      SALDONAR numeric(14,2))
   as
declare variable OTABLE varchar(40);
declare variable OREF integer;
declare variable DOKTABLE varchar(20);
declare variable DOKREF integer;
declare variable DOKSYMBOL varchar(40);
declare variable LCREDIT numeric(14,2);
declare variable BUF numeric(14,2);
declare variable NAGFAKREF integer;
declare variable NAGFAKDATE timestamp;
declare variable DOKUMNAGREF integer;
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable DOKUMNAGDATE timestamp;
declare variable CNT integer;
declare variable DOKUMNAGPWARTOSC numeric(14,2);
begin
  --symbol,dat,kontrahenta, warto nierzparowanego dokumentu
  SALDONAR = 0;
  SALDO = 0;
  LP = 1;
  for select distinct bkdocs.ref,bkdocs.bkreg,bkdocs.symbol,bkdocs.transdate,bkdocs.otable,bkdocs.oref, bkdocs.dictdef, bkdocs.dictpos
    from decrees
    left join bkdocs on (decrees.bkdoc=bkdocs.ref)
    where decrees.transdate>=:fromdate and decrees.transdate<=:todate
    and decrees.status>1
    and decrees.account like :account||'%'
    and decrees.company = :company
    order by bkdocs.ref
    into :bkref,:bkreg,:bksymbol,:bkdate,:otable,:oref, :slodef, :slopoz
    do begin
      descript = '';
      credit = 0;
      debit = 0;
      select sum(debit-credit)
        from decrees
        where bkdoc=:bkref and status>1 and account like :account||'%'
          and decrees.company = :company
        into :debit;
      if(:otable is null) then otable = '';
      if(:otable='') then begin
        descript = 'Dokument wystawiony recznie';
        SALDONAR = :SALDONAR + :debit - :credit;
        SALDO = :debit - :credit;
        if(:debit<0 or :credit<0) then begin buf=:credit; credit=-:debit; debit=-:buf; end
        execute procedure NAME_FROM_DICTPOSREF(slodef, slopoz)
          returning_values :kontrahent;
        suspend;
        lp = :lp + 1;
      end else if(:otable='NAGFAK') then begin
        select symbol, dostawca from nagfak where ref=:oref into :descript, :slopoz;
        slodef = 6;
        for select doktable,dokref,doksymbol from RPT_FK_DOKUMENTY(:oref, :company)
        into :doktable,:dokref,:doksymbol
        do begin
          lcredit = 0;
          select sum(decrees.credit-decrees.debit)
            from decrees
            join bkdocs on (bkdocs.ref=decrees.bkdoc)
            where bkdocs.otable=:doktable and bkdocs.oref=:dokref
            and decrees.status>1 and decrees.account like :account||'%'
            and bkdocs.transdate<=:todate
            and decrees.company = :company
            into :lcredit;
            if (:lcredit is null ) then lcredit = 0;
          if(:lcredit<>0) then begin
            credit = :credit + :lcredit;
            if(coalesce(char_length(:descript),0)<200) then descript = :descript||', '||:doksymbol; -- [DG] XXX ZG119346
          end
          doksymbol = '';
        end
        if(:credit<>:debit) then begin
          SALDONAR = :SALDONAR + :debit - :credit;
          SALDO = :debit - :credit;
          if(:debit<0 or :credit<0) then begin buf=:credit; credit=-:debit; debit=-:buf; end
          execute procedure NAME_FROM_DICTPOSREF(slodef, slopoz)
            returning_values :kontrahent;
          suspend;
          lp = :lp + 1;
        end
      end else if(:otable='DOKUMNAG') then begin
        nagfakref = null;
        nagfakdate = null;
        cnt = 0;
        select symbol,faktura, slodef, slopoz
        from dokumnag where ref=:oref
        into :descript,:nagfakref,:slodef,:slopoz;
        if(:nagfakref is not null) then begin
          select transdate from bkdocs
          where otable='NAGFAK' and oref=:nagfakref
          and status>1
          and company = :company
          into :nagfakdate;
          select count(*) from decrees
          join bkdocs on (bkdocs.ref=decrees.bkdoc)
          where bkdocs.otable='NAGFAK' and bkdocs.oref=:nagfakref
          and decrees.status>1 and decrees.account like :account||'%'
          and bkdocs.transdate<=:todate
          and decrees.company=:company
          into :cnt;
        end
        if(:nagfakref is null or :nagfakdate is null) then begin
          descript = :descript || ' - brak faktury lub faktura niezaksiegowana';
          SALDONAR = :SALDONAR + :debit - :credit;
          SALDO = :debit - :credit;
          if(:debit<0 or :credit<0) then begin buf=:credit; credit=-:debit; debit=-:buf; end
          execute procedure NAME_FROM_DICTPOSREF(slodef, slopoz)
            returning_values :kontrahent;
          suspend;
          lp = :lp + 1;
        end else if(:nagfakdate>:todate) then begin
          descript = :descript || ' - faktura poza badanym okresem';
          SALDONAR = :SALDONAR + :debit - :credit;
          SALDO = :debit - :credit;
          if(:debit<0 or :credit<0) then begin buf=:credit; credit=-:debit; debit=-:buf; end
          execute procedure NAME_FROM_DICTPOSREF(slodef, slopoz)
            returning_values :kontrahent;
          suspend;
          lp = :lp + 1;
        end else if(:cnt=0) then begin
          descript = :descript || ' - faktura nie dotyczy badanego konta';
          SALDONAR = :SALDONAR + :debit - :credit;
          SALDO = :debit - :credit;
          if(:debit<0 or :credit<0) then begin buf=:credit; credit=-:debit; debit=-:buf; end
          execute procedure NAME_FROM_DICTPOSREF(slodef, slopoz)
            returning_values :kontrahent;
          suspend;
          lp = :lp + 1;
        end
      end else if(:otable='DOKUMNOT') then begin
        dokumnagref = null;
        dokumnagdate = null;
        nagfakref = null;
        nagfakdate = null;
        dokumnagpwartosc = 0;
        cnt = 0;
        select symbol,dokumnagkor,faktura
        from dokumnot where ref=:oref
        into :descript,:dokumnagref,:nagfakref;
        if(dokumnagref is not null) then begin
          select transdate from bkdocs
          where otable='DOKUMNAG' and oref=:dokumnagref
          and status>1
          into :dokumnagdate;
          select pwartosc, slodef, slopoz from dokumnag
          where ref=:dokumnagref
          into :dokumnagpwartosc,:slodef,:slopoz;
          if(:nagfakref is null) then begin
            select faktura from dokumnag
            where ref=:dokumnagref
            into :nagfakref;
          end
          if(:nagfakref is not null) then begin
            select transdate from bkdocs
            where otable='NAGFAK' and oref=:nagfakref
            and status>1
            and company = :company
            into :nagfakdate;
            select count(*) from decrees
            join bkdocs on (bkdocs.ref=decrees.bkdoc)
            where bkdocs.otable='NAGFAK' and bkdocs.oref=:nagfakref
            and decrees.status>1 and decrees.account like :account||'%'
            and bkdocs.transdate<=:todate
            and decrees.company=:company
            into :cnt;
          end
        end
        if(:dokumnagref is null or (:dokumnagdate is null and :dokumnagpwartosc<>0)) then begin
          if(:debit<>0 or :credit<>0) then begin
            descript = :descript || ' - brak dok.mag. lub dok.mag. niezaksiegowany';
            SALDONAR = :SALDONAR + :debit - :credit;
            SALDO = :debit - :credit;
            if(:debit<0 or :credit<0) then begin buf=:credit; credit=-:debit; debit=-:buf; end
            execute procedure NAME_FROM_DICTPOSREF(slodef, slopoz)
              returning_values :kontrahent;
            suspend;
            lp = :lp + 1;
          end
        end else if(:nagfakref is null or :nagfakdate is null) then begin
          if(:debit<>0 or :credit<>0) then begin
            descript = :descript || ' - brak faktury lub faktura niezaksiegowana';
            SALDONAR = :SALDONAR + :debit - :credit;
            SALDO = :debit - :credit;
            if(:debit<0 or :credit<0) then begin buf=:credit; credit=-:debit; debit=-:buf; end
            execute procedure NAME_FROM_DICTPOSREF(slodef, slopoz)
              returning_values :kontrahent;
            suspend;
            lp = :lp + 1;
          end
        end else if(:dokumnagdate>:todate) then begin
          if(:debit<>0 or :credit<>0) then begin
            descript = :descript || ' - dok. mag. poza badanym okresem';
            SALDONAR = :SALDONAR + :debit - :credit;
            SALDO = :debit - :credit;
            if(:debit<0 or :credit<0) then begin buf=:credit; credit=-:debit; debit=-:buf; end
            execute procedure NAME_FROM_DICTPOSREF(slodef, slopoz)
              returning_values :kontrahent;
            suspend;
            lp = :lp + 1;
          end
        end else if(:nagfakdate>:todate) then begin
          if(:debit<>0 or :credit<>0) then begin
            descript = :descript || ' - faktura poza badanym okresem';
            SALDONAR = :SALDONAR + :debit - :credit;
            SALDO = :debit - :credit;
            if(:debit<0 or :credit<0) then begin buf=:credit; credit=-:debit; debit=-:buf; end
            execute procedure NAME_FROM_DICTPOSREF(slodef, slopoz)
              returning_values :kontrahent;
            suspend;
            lp = :lp + 1;
          end
        end else if(:cnt=0) then begin
          if(:debit<>0 or :credit<>0) then begin
            descript = :descript || ' - faktura nie dotyczy badanego konta';
            SALDONAR = :SALDONAR + :debit - :credit;
            SALDO = :debit - :credit;
            if(:debit<0 or :credit<0) then begin buf=:credit; credit=-:debit; debit=-:buf; end
            execute procedure NAME_FROM_DICTPOSREF(slodef, slopoz)
              returning_values :kontrahent;
            suspend;
            lp = :lp + 1;
          end
        end
      end
    end
end^
SET TERM ; ^
