--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FXA_VALDOCS_OT(
      FXDASSET INTEGER_ID,
      VALDOCREF INTEGER_ID)
  returns (
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      NAME varchar(80) CHARACTER SET UTF8                           ,
      ODDZIAL varchar(10) CHARACTER SET UTF8                           ,
      REF integer,
      USINGDT timestamp,
      SERIALNO varchar(20) CHARACTER SET UTF8                           ,
      AMORTGRP varchar(10) CHARACTER SET UTF8                           ,
      FVALUE numeric(18,2),
      TVALUE numeric(18,2),
      TREDEMPTION numeric(18,2),
      FREDEMPTION numeric(18,2),
      DOCDATE timestamp,
      FVALUE1 numeric(18,2),
      TVALUE1 numeric(18,2),
      SYMBOL1 varchar(20) CHARACTER SET UTF8                           ,
      NAME1 varchar(60) CHARACTER SET UTF8                           ,
      NAME2 varchar(80) CHARACTER SET UTF8                           ,
      AMCORRECTION smallint,
      CORPERIOD varchar(20) CHARACTER SET UTF8                           ,
      OPERIOD varchar(20) CHARACTER SET UTF8                           ,
      OPERATOR varchar(60) CHARACTER SET UTF8                           ,
      INVOICE varchar(40) CHARACTER SET UTF8                           ,
      OPERDATE timestamp)
   as
declare variable VTVALUE numeric(18,2);
declare variable VTREDEMPTION numeric(18,2);
declare variable VFVALUE numeric(18,2);
declare variable VFREDEMPTION numeric(18,2);
declare variable AMYEAR SMALLINT_ID;
declare variable AMMONTH SMALLINT_ID;
declare variable SUMTAMORT numeric(18,2);
declare variable SUMFAMORT numeric(18,2);
declare variable BKDOC BKDOCS_ID;
begin

  select F.symbol, F.name, F.oddzial, F.ref, F.usingdt, F.serialno, F.amortgrp, F.fvalue, F.tvalue,
         F.tredemption, F.fredemption, V.fxdasset, V.docdate, F.fvalue, F.tvalue, V.symbol, D.name,
         T.name, T.AMCORRECTION, amc.name, amp.name, o.nazwa, b.symbol, amp.amyear, amp.ammonth, v.operdate
    from valdocs V
      left join fxdassets F on (F.ref = V.fxdasset)
      left join departments D on (D.symbol = F.department)
      left join vdoctype T on (T.symbol = V.doctype)
      left join amperiods amc on (amc.ref = v.corperiod)
      left join amperiods amp on (amp.ref = v.operiod)
      left join operator O on (v.regoper = o.ref)
      left join bkdocs b on (b.ref = v.invoice)
    where (V.fxdasset = :FXDASSET
      and V.ref = :valdocref)
    into :symbol, :name, :oddzial, :ref, :usingdt, :serialno, :amortgrp, :fvalue, :tvalue,
         :tredemption, :fredemption, :fxdasset, :docdate, :fvalue1, :tvalue1, :symbol1, :name1,
         :name2, :amcorrection, :corperiod, :operiod, :operator, :invoice, :amyear, :ammonth, :operdate;
  select sum(tvalue + tvallim), sum(fvalue), sum(tredemption), sum(fredemption)
    from valdocs
    where fxdasset = :fxdasset
      and docdate < :docdate
    into :vtvalue, :vfvalue, :vtredemption, :vfredemption;

  if (Vtvalue is not null) then
    tvalue = tvalue + Vtvalue;
  if (Vfvalue is not null) then
    fvalue = fvalue + Vfvalue;
  if (Vtredemption is not null) then
    tredemption = tredemption + Vtredemption;
  if (Vfredemption is not null) then
    fredemption = fredemption + Vfredemption;

  select coalesce(sum(tamort), 0), coalesce(sum(famort), 0)
    from amortization AM
      left join amperiods a on (am.amperiod = a.ref)
    where AM.fxdasset = :fxdasset
      and (a.amyear < :amyear or (a.amyear = :amyear
      and a.ammonth < :ammonth))
    into :sumtamort, :sumfamort;

  if (sumtamort is not null) then
    tredemption = tredemption + sumtamort;
  if (sumfamort is not null) then
    fredemption = fredemption + sumfamort;
  suspend;
end^
SET TERM ; ^
