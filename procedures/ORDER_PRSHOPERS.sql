--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ORDER_PRSHOPERS(
      SHEET integer,
      OPERMASTER integer,
      OLDN integer,
      NEWN integer)
   as
declare variable ref integer;
  declare variable number integer;
begin
  --firts we find minor number
  if (oldn < newn) then
    newn = oldn;

  number = newn;
  if(:opermaster is null) then begin
    for
      select ref
        from PRSHOPERS
        where sheet = :sheet and opermaster is null and number >= :number
        order by number, ord
        into :ref
    do begin
      update PRSHOPERS set number = :number, ord = 2
        where ref = :ref;
      number = number + 1;
    end
    update PRSHOPERS set ord = 1
      where sheet = :sheet and opermaster is null and number >= :newn;
  end else begin
    for
      select ref
        from PRSHOPERS
        where sheet = :sheet and opermaster = :opermaster and number >= :number
        order by number, ord
        into :ref
    do begin
      update PRSHOPERS set number = :number, ord = 2
        where ref = :ref;
      number = number + 1;
    end
    update PRSHOPERS set ord = 1
      where sheet = :sheet and opermaster = :opermaster and number >= :newn;
  end
end^
SET TERM ; ^
