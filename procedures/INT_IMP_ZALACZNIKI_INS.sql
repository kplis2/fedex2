--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_ZALACZNIKI_INS(
      TABELAID type of column INT_IMP_ZALACZNIKI.TABELAID,
      OBIEKTID type of column INT_IMP_ZALACZNIKI.OBIEKTID,
      ZALACZNIKID type of column INT_IMP_ZALACZNIKI.ZALACZNIKID,
      TYP type of column INT_IMP_ZALACZNIKI.TYP,
      WARTOSC type of column INT_IMP_ZALACZNIKI.WARTOSC,
      SCIEZKA type of column INT_IMP_ZALACZNIKI.SCIEZKA,
      GLOWNY type of column INT_IMP_ZALACZNIKI.GLOWNY,
      NAZWAPLIKU type of column INT_IMP_ZALACZNIKI.NAZWAPLIKU,
      ROZSZERZENIE type of column INT_IMP_ZALACZNIKI.ROZSZERZENIE,
      NAZWA type of column INT_IMP_ZALACZNIKI.NAZWA,
      OPIS type of column INT_IMP_ZALACZNIKI.OPIS,
      SKADTABELA type of column INT_IMP_ZALACZNIKI.SKADTABELA,
      SKADREF type of column INT_IMP_ZALACZNIKI.SKADREF,
      DEL type of column INT_IMP_ZALACZNIKI.DEL,
      HASHVALUE type of column INT_IMP_ZALACZNIKI.HASH,
      REC type of column INT_IMP_ZALACZNIKI.REC,
      SESJA type of column INT_IMP_ZALACZNIKI.SESJA)
  returns (
      REF type of column INT_IMP_ZALACZNIKI.REF)
   as
begin
  insert into int_imp_zalaczniki (zalacznikid, tabelaid, obiektid, typ, sciezka, glowny,
      nazwapliku, rozszerzenie, nazwa, opis, skadtabela, skadref,
      "HASH", del, rec, sesja, wartosc)
  values (:zalacznikid, :tabelaid, :obiektid, :typ, :sciezka, :glowny,
    :nazwapliku, :rozszerzenie, :nazwa, :opis, :skadtabela, :skadref,
    :hashvalue, :del, :rec, :sesja, :wartosc)
    returning ref into :ref;
end^
SET TERM ; ^
