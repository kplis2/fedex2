--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XP_ILOSC(
      KEY1 integer,
      KEY2 integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      TYP varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(16,6))
   as
declare variable quantity numeric(16,6);
declare variable equantity varchar(1024);
declare variable parref integer;
declare variable parvalue numeric(16,6);
declare variable parmodified smallint;
declare variable paramname varchar(255);
declare variable fromprshmat integer;
declare variable fromprshoper integer;
declare variable fromprshtool integer;
begin
/*  TYP:
  (pusty) = B
  B - ilosc brutto z technologii
  N - ilosc netto z technologii
*/
  quantity = 0;
  paramname='ILOSC('||:typ||')';
  -- czy parametr jest uprzednio zapisany i zmodyfikowany
  parref = NULL;
  select fromprshmat, fromprshoper, fromprshtool from prcalccols where ref=:key2
    into :fromprshmat, :fromprshoper, :fromprshtool;
  select first 1 ref, cvalue, modified
    from prcalccols where parent=:key2 and expr=:paramname and calctype=1
    into :parref, :parvalue, :parmodified;
  if(:parref is not null and :parmodified=1) then begin
    ret = :parvalue;
    exit;
  end
  if(:typ='' or :typ is null) then typ = 'B';

  -- nalezy obliczyc na nowo
  if(:fromprshmat is not null) then begin
    -- ilosc z materialow
    if(:typ='B') then begin
      select EGROSSQUANTITY from PRSHMAT where PRSHMAT.REF=:fromprshmat
      into :equantity;
    end else begin
      select ENETQUANTITY from PRSHMAT where PRSHMAT.REF=:fromprshmat
      into :equantity;
    end

  end else if(:fromprshoper is not null) then begin
    -- ilosc z operacji

  end else if(:fromprshtool is not null) then begin
    -- ilosc z dodatkow
    select QUANTITY from PRSHTOOLS where ref=:fromprshtool
    into :quantity;

  end
  quantity = NULL;
  execute procedure STATEMENT_PARSE(:equantity,:key1,:key2,:prefix) returning_values :quantity;
  if(:quantity is null) then quantity = 0;
  ret = :quantity;

  if (ret is null) then ret = 0;
  -- nalezy zapisac wyliczony parametr
  if(:parref is not null) then begin
    update prcalccols set cvalue=:ret where ref=:parref;
  end else begin
    insert into prcalccols(PARENT,CVALUE,EXPR,DESCRIPT,CALCTYPE,PRCOLUMN)
    values(:key2,:ret,:paramname,'Ilosc',1,null);
  end
end^
SET TERM ; ^
