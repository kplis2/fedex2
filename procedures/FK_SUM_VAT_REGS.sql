--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_SUM_VAT_REGS(
      COMPANY integer,
      BKPERIOD varchar(6) CHARACTER SET UTF8                           ,
      VATPERIOD varchar(6) CHARACTER SET UTF8                           )
  returns (
      VTYPE smallint,
      VATREG varchar(10) CHARACTER SET UTF8                           ,
      NETTO22 numeric(14,2),
      NETTO05 numeric(14,2),
      VAT22 numeric(14,2),
      NETTO07 numeric(14,2),
      VAT07 numeric(14,2),
      VAT05 numeric(14,2),
      NETTO06 numeric(14,2),
      VAT06 numeric(14,2),
      NETTO03 numeric(14,2),
      VAT03 numeric(14,2),
      NETTO00 numeric(14,2),
      NETTOZW numeric(14,2),
      NETTONP numeric(14,2),
      NETTODEB numeric(14,2),
      VATDEB numeric(14,2),
      NETTO57 numeric(14,2),
      VAT57 numeric(14,2))
   as
declare variable vtype2 smallint;
declare variable vatreg2 varchar(10);
declare variable netto numeric(14,2);
declare variable vat numeric(14,2);
declare variable vatgr varchar(5);
declare variable debited smallint;
declare variable taxgr2 varchar(10);
declare variable sql varchar(1024);
begin
  netto22 = 0;
  netto07 = 0;
  netto06 = 0;
  netto05 = 0;
  netto03 = 0;
  netto00 = 0;
  nettozw = 0;
  nettonp = 0;
  nettodeb = 0;
  vat22 = 0;
  vat07 = 0;
  vat05 = 0;
  vat06 = 0;
  vat03 = 0;
  vatdeb = 0;
  NETTO57 = 0;
  VAT57 = 0;
  sql = 'select VR.vtype, BD.vatreg, VP.netv, VP.vatv, VP.vatgr, VP.debited, VP.taxgr ';
  sql = sql ||'from bkdocs BD join bkvatpos VP on (BD.ref=VP.bkdoc) ';
  sql = sql ||'join vatregs VR on (BD.vatreg=VR.symbol) where BD.status>0 ';
  sql = sql ||'and bd.company = '||:company||' and vp.company = '||:company||' and vr.company = '||:company;
  if(:bkperiod <> '') then sql = sql || 'and BD.period='''||:bkperiod||''' ';
  if(:vatperiod <> '') then sql = sql || 'and BD.vatperiod='''||:vatperiod||''' ';
  sql = sql || 'order by VR.vtype, BD.vatreg';
  for execute statement :sql
    into :vtype2, :vatreg2, :netto, :vat, :vatgr, :debited, :taxgr2
  do begin
    if (vtype is null) then
    begin
      vtype = vtype2;
      vatreg = vatreg2;
    end
    if (taxgr2 is null) then
      taxgr2 = '';

    if (vatreg<>vatreg2) then
    begin
      suspend;
      vtype = vtype2;
      vatreg = vatreg2;
      netto22 = 0;
      netto07 = 0;
      netto05 = 0;
      netto03 = 0;
      netto00 = 0;
      nettozw = 0;
      nettodeb = 0;
      vat22 = 0;
      vat07 = 0;
      vat05 = 0;
      vat03 = 0;
      vatdeb = 0;
      NETTO57 = 0;
      VAT57 = 0;
    end

    if (debited=0) then
    begin
      nettodeb = nettodeb + netto;
      vatdeb = vatdeb + vat;
      if (vtype2 > 2) then
      begin
        if (taxgr2 = 'P57') then
        begin
          NETTO57 = NETTO57 + netto;
          VAT57 = VAT57 + vat;
        end
      end
    end else
    if (vatgr='22') then
    begin
      netto22 = netto22 + netto;
      vat22 = vat22 + vat;
    end else
    if (vatgr='07') then
    begin
      netto07 = netto07 + netto;
      vat07 = vat07 + vat;
    end else
    if (vatgr='06') then
    begin
      netto06 = netto06 + netto;
      vat06 = vat06 + vat;
    end else
    if (vatgr='03') then
    begin
      netto03 = netto03 + netto;
      vat03 = vat03 + vat;
    end else
    if (vatgr='00') then
    begin
      netto00 = netto00 + netto;
    end else
    if (vatgr='ZW') then
    begin
      nettozw = nettozw + netto;
    end else
    if (vatgr='NP') then
    begin
      nettonp = nettonp + netto;
    end else
    if (vatgr='05') then
    begin
      netto05 = netto05 + netto;
      vat05 = vat05 + vat;
    end
    else
      exception FK_EXPT_VAT_GR vatgr;
  end
  suspend;
end^
SET TERM ; ^
