--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CHECK_MAGBREAK(
      MWSORD integer,
      VERS integer)
  returns (
      MAGBREAK smallint)
   as
  declare variable goodmagbreak smallint;
begin
  magbreak = null;
  if (exists(select first 1 1 from rdb$procedures r where r.rdb$procedure_name = 'xk_mws_check_magbreak')) then
    execute statement 'execute procedure xk_mws_check_magbreak (' || coalesce(:mwsord,0) || ',' || coalesce(:vers,0) || ') '
      into :magbreak;
  if (magbreak is null) then
  begin
    select first 1 d.magbreak
      from mwsords m
        left join defmagaz d on (m.wh = d.symbol)
      where m.ref = :mwsord
      into :magbreak;
    if(:magbreak in (0,1)) then
    begin
      select first 1 t.magbreak
        from wersje w
          left join towary t on (w.ktm = t.ktm)
        where w.ref = :vers
        into :goodmagbreak;
      if(:goodmagbreak > :magbreak) then
        magbreak = :goodmagbreak;
    end
  end
end^
SET TERM ; ^
