--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WERSJE_ATRYBUTY(
      WERSJAREF varchar(80) CHARACTER SET UTF8                           ,
      ATRYBUT varchar(255) CHARACTER SET UTF8                           ,
      WARTOSC varchar(255) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE NRWERSJI SMALLINT;
begin
  if (not exists(select wersjaref from ATRYBUTY where wersjaref = :wersjaref and CECHA = :atrybut)) then begin
    insert into ATRYBUTY (KTM, NRWERSJI, CECHA, WARTOSC, WERSJAREF)
      select w.ktm, w.nrwersji, :atrybut, :wartosc, :wersjaref
        from wersje w
        where ref = :wersjaref;
  end else
    update ATRYBUTY set WARTOSC = :wartosc where wersjaref = :wersjaref and CECHA = :ATRYBUT;
end^
SET TERM ; ^
