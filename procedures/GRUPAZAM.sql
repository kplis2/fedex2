--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GRUPAZAM
  returns (
      ID integer)
   as
begin
  id = gen_id(GEN_GRUPAZAM,1);
  execute procedure RP_CHECK_REF('GRUPAZAM', :id)
    returning_values :id;
  suspend;
end^
SET TERM ; ^
