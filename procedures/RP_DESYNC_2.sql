--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RP_DESYNC_2(
      TABELA varchar(15) CHARACTER SET UTF8                           ,
      KEY1 varchar(65) CHARACTER SET UTF8                           ,
      KEY2 varchar(65) CHARACTER SET UTF8                           ,
      KEY3 varchar(65) CHARACTER SET UTF8                           ,
      KEY4 varchar(65) CHARACTER SET UTF8                           ,
      KEY5 varchar(65) CHARACTER SET UTF8                           ,
      STATE integer)
   as
DECLARE VARIABLE IL INTEGER;
declare variable slocat varchar(255);
declare variable locationbit varchar(255) ;
begin
 if(:state is null or (:state = 0))then exit;
  execute procedure GETCONFIG('AKTULOCATBIT') returning_values :locationbit;
    if(:state = -1) then
      /*do lokalizacji centrali*/
      execute procedure GETCONFIG('AKTULOCATMBIT') returning_values :slocat;
    else if(:state = -2) then begin
      /*do wszystkich lokalizacji oprocz centrali*/
      execute procedure GETCONFIG('AKTULOCATNOTMBIT') returning_values :slocat;
    end else if(:state = 0) then
      /*do wszystkich lokalizacji*/
      execute procedure GETCONFIG('AKTULOCATSUMBIT') returning_values :slocat;
    if(:slocat <> '')then begin
      /*wyciecie z celu wlasnej lokalizacji*/
      state = cast(:slocat as integer);
      if((:state / cast(:locationbit as integer) / 2 * 2) <> (:state / cast(:locationbit as integer) )) then
        state = :state - cast(:locationbit as integer);
    end
 if(:state > 0) then begin
 if(:key1 <> '' and :key2 <> '' and :key3 <> '' and :key4 <> '' and :key5 <> '') then begin
   select count(*) from LOG_FILE where TABLE_NAME=:TABELA and KEY1=:KEY1 and KEY2=:KEY2 and KEY3=:KEY3 and KEY4=:KEY4 and KEY5=:KEY5 into :il;
   if(:il is null) then il = 0;
   if(:il = 0) then
      insert into LOG_FILE(TABLE_NAME,TIME_STAMP,KEY1,KEY2,KEY3,KEY4,KEY5,STATE) values (:TABELA,CURRENT_TIMESTAMP(0),:key1, :key2, :key3, :key4, :key5, :state);
   else
     update LOG_FILE set TIME_STAMP=CURRENT_TIMESTAMP(0), STATE = :state where TABLE_NAME=:tabela and KEY1=:key1 and KEY2=:key2 and KEY3=:key3 and KEY4=:key4;
 end else if(:key1 <> '' and :key2 <> '' and :key3 <> '' and :key4 <> '') then begin
   select count(*) from LOG_FILE where TABLE_NAME=:TABELA and KEY1=:KEY1 and KEY2=:KEY2 and KEY3=:KEY3 and KEY4=:KEY4 into :il;
   if(:il is null) then il = 0;
   if(:il = 0) then
      insert into LOG_FILE(TABLE_NAME,TIME_STAMP,KEY1,KEY2,KEY3,KEY4,STATE) values (:TABELA,CURRENT_TIMESTAMP(0),:key1, :key2, :key3, :key4, :state);
   else
     update LOG_FILE set TIME_STAMP=CURRENT_TIMESTAMP(0), STATE = :state where TABLE_NAME=:tabela and KEY1=:key1 and KEY2=:key2 and KEY3=:key3;
 end else if(:key1 <> '' and :key2 <> '' and :key3 <> '') then begin
   select count(*) from LOG_FILE where TABLE_NAME=:TABELA and KEY1=:KEY1 and KEY2=:KEY2 and KEY3=:KEY3 into :il;
   if(:il is null) then il = 0;
   if(:il = 0) then
      insert into LOG_FILE(TABLE_NAME,TIME_STAMP,KEY1,KEY2,KEY3,STATE) values (:TABELA,CURRENT_TIMESTAMP(0),:key1, :key2, :key3,:state);
   else
     update LOG_FILE set TIME_STAMP=CURRENT_TIMESTAMP(0), STATE = :state where TABLE_NAME=:tabela and KEY1=:key1 and KEY2=:key2 and KEY3=:key3;
 end else if(:key1 <> '' and :key2 <> '') then begin
   select count(*) from LOG_FILE where TABLE_NAME=:TABELA and KEY1=:KEY1 and KEY2=:KEY2 into :il;
   if(:il is null) then il = 0;
   if(:il = 0) then
      insert into LOG_FILE(TABLE_NAME,TIME_STAMP,KEY1,KEY2,STATE) values (:TABELA,CURRENT_TIMESTAMP(0),:key1, :key2,:state);
   else
     update LOG_FILE set TIME_STAMP=CURRENT_TIMESTAMP(0) , STATE = :state where TABLE_NAME=:tabela and KEY1=:key1 and KEY2=:key2;
 end else if(:key1 <> '') then begin
   select count(*) from LOG_FILE where TABLE_NAME=:TABELA and KEY1=:KEY1 into :il;
   if(:il is null) then il = 0;
   if(:il = 0) then
      insert into LOG_FILE(TABLE_NAME,TIME_STAMP,KEY1,state) values (:TABELA,CURRENT_TIMESTAMP(0),:key1,:state);
   else
     update LOG_FILE set TIME_STAMP=CURRENT_TIMESTAMP(0), STATE = :state  where TABLE_NAME=:tabela and KEY1=:key1;
 end else
   exception RP_DESYNC_WRONG_KEY;
 end
end^
SET TERM ; ^
