--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_TOWARY_SUPPLYCLASSES(
      MAGAZYN DEFMAGAZ_ID,
      ODDATY date,
      DODATY date,
      PROCENTGORA integer,
      PROCENTDOL integer)
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      MIARA varchar(10) CHARACTER SET UTF8                           ,
      IT numeric(14,4),
      ROI numeric(14,4),
      ITCLASS smallint,
      ROICLASS smallint)
   as
declare variable minit numeric(14,4);
declare variable maxit numeric(14,4);
declare variable minroi numeric(14,4);
declare variable maxroi numeric(14,4);
declare variable gorait numeric(14,4);
declare variable dolit numeric(14,4);
declare variable goraroi numeric(14,4);
declare variable dolroi numeric(14,4);
begin
  select min(IT),max(IT),min(ROI),max(ROI)
    from GET_TOWARY_SUPPLYVALUES(:magazyn,:oddaty,:dodaty)
    into :minit,:maxit,:minroi,:maxroi;
  gorait = :maxit - (:maxit-:minit)*:procentgora/100;
  dolit = :minit + (:maxit-:minit)*:procentdol/100;
  goraroi = :maxroi - (:maxroi-:minroi)*:procentgora/100;
  dolroi = :minroi + (:maxroi-:minroi)*:procentdol/100;
  for select KTM,WERSJAREF,NAZWA,MIARA,IT,ROI
    from GET_TOWARY_SUPPLYVALUES(:magazyn,:oddaty,:dodaty)
    into :ktm, :wersjaref, :nazwa, :miara, :it, :roi
  do begin
    if(:it>:gorait) then itclass = 3;
    else if(:it>dolit) then itclass = 2;
    else itclass = 1;
    if(:roi>:goraroi) then roiclass = 3;
    else if(:roi>dolroi) then roiclass = 2;
    else roiclass = 1;
    suspend;
  end
end^
SET TERM ; ^
