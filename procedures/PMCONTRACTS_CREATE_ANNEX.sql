--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PMCONTRACTS_CREATE_ANNEX(
      PMCONTRACT integer)
   as
declare variable nowyref integer;
    declare variable ins_pmcontract integer;
    declare variable ins_dictdef integer;
    declare variable ins_dictpos integer;
    declare variable ins_instype smallint;
    declare variable ins_currency varchar(3);
    declare variable ins_insval numeric(14,2);
    declare variable ins_currate numeric(14,4);
    declare variable ins_insvalpln numeric(14,2);
    declare variable ins_expdate timestamp;
    declare variable ins_cgetdate timestamp;
    declare variable ins_cexpdate timestamp;
    declare variable ins_reqdate timestamp;
    declare variable ins_deldate timestamp;
    declare variable ins_status smallint;
    declare variable ins_discrate numeric(14,2);
    declare variable ins_descript varchar(1024);
begin
   /* procedura kopiujaca dana umowe jako aneks do tej umowy */

   execute procedure GEN_REF('pmcontracts') returning_values :nowyref;

   /* kopiowanie rekordu z tabeli pmcontracts */
   insert into pmcontracts (REF, SYMBOL, TYPE, ORGREF, SOURCE, PMPLAN, PMCONTRTYPE, STARTDATE,
      DICTDEF, DICTPOS, SUBJECT, CVALUE, STATUS, ACTDEPARTMENT, WARRANTY, GRI, PAYER, DATEFROM,
      DATETO, PAYMENT, PARTIALPAYMENT, FINALPAYMENT, PAYMENTDESC, FLAGS, REGDEPARTMENT, DESCRIPT,
      ANNOTATION, PERCENT)
   select :nowyref, SYMBOL||'/A', 1, REF, SOURCE, PMPLAN, PMCONTRTYPE, current_date, DICTDEF, DICTPOS,
      SUBJECT, CVALUE, STATUS, ACTDEPARTMENT, WARRANTY, GRI, PAYER, current_date, DATETO, PAYMENT,
      PARTIALPAYMENT, FINALPAYMENT, PAYMENTDESC, FLAGS, REGDEPARTMENT, DESCRIPT, ANNOTATION, PERCENT
   from pmcontracts
   where (:pmcontract = REF);

   /* kopiowanie rekordow z tabeli pminsurances */
   for select PMCONTRACT, DICTDEF, DICTPOS, INSTYPE, CURRENCY, INSVAL, CURRATE, INSVALPLN,
               EXPDATE, CGETDATE, CEXPDATE, REQDATE, DELDATE, STATUS, DISCRATE, DESCRIPT
        from pminsurances
          where pmcontract = :pmcontract
          into :ins_pmcontract, :ins_dictdef, :ins_dictpos, :ins_instype, :ins_currency, :ins_insval,
               :ins_currate, :ins_insvalpln, :ins_expdate, :ins_cgetdate, :ins_cexpdate,:ins_reqdate,
               :ins_deldate, :ins_status, :ins_discrate, :ins_descript
   do begin
        insert into pminsurances (PMCONTRACT, DICTDEF, DICTPOS, INSTYPE, CURRENCY, INSVAL, CURRATE,
                                  INSVALPLN, EXPDATE, CGETDATE, CEXPDATE, REQDATE, DELDATE, STATUS,
                                  DISCRATE, DESCRIPT)
        values (:nowyref, :ins_dictdef, :ins_dictpos, :ins_instype, :ins_currency, :ins_insval,
                :ins_currate, :ins_insvalpln, :ins_expdate, :ins_cgetdate, :ins_cexpdate,:ins_reqdate,
                :ins_deldate, :ins_status, :ins_discrate, :ins_descript);
   end
end^
SET TERM ; ^
