--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDE_AMOUNTZREAL(
      PRSCHEDGUIDE integer)
   as
declare variable amountzreal numeric(14,4);
declare variable amountshortage numeric(14,4);
declare variable quantity numeric(14,4);
begin
  select sum(d.quantity)
    from prschedguidedets d
    where d.out = 0 and d.overlimit = 0 and d.shortage = 0 and d.byproduct = 0
      and d.prschedguide = :prschedguide and d.accept > 0
    into amountzreal;
  amountshortage = 0;
  for
    select max(coalesce(s.headergroupamount,0))
      from prschedguidedets d
        left join prshortages s on (s.ref = d.prshortage)
      where d.prschedguide = :prschedguide and d.out = 0 and d.overlimit = 0
        and d.shortage = 1 and d.byproduct = 0 and d.accept > 0
      group by coalesce(s.headergroup,0)
      into quantity
  do begin
    if (quantity is null) then quantity = 0;
    amountshortage = amountshortage + quantity;
  end
  update prschedguides set amountzreal = :amountzreal, amountshortage = :amountshortage
    where ref = :prschedguide;
end^
SET TERM ; ^
