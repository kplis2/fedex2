--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FR_CHECK_ACCOUNTS(
      FRHDR varchar(20) CHARACTER SET UTF8                           ,
      YEARID integer)
  returns (
      ACCOUNT ACCOUNT_ID,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      ALG varchar(30) CHARACTER SET UTF8                           ,
      CNT integer,
      BKTYPE smallint,
      BKTYPETXT varchar(20) CHARACTER SET UTF8                           ,
      FRHNAME varchar(255) CHARACTER SET UTF8                           ,
      FRPSNS varchar(255) CHARACTER SET UTF8                           )
   as
declare variable CHKTYPE smallint;
declare variable DOBAL integer;
declare variable DORES integer;
declare variable OPERATOR integer;
declare variable CURRENTCOMPANY integer;
declare variable COUNTRY_CURRENCY varchar(3);
declare variable FRPSN varchar(20);
declare variable BAL integer;
declare variable RES integer;
declare variable FRHDR2 varchar(20);
declare variable NULLONLY integer;
declare variable CPERIOD varchar(6);
begin
  execute procedure get_global_param ('AKTUOPERATOR')
    returning_values operator;
  execute procedure get_global_param ('CURRENTCOMPANY')
    returning_values currentcompany;
  execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
    returning_values country_currency;

  cperiod = :yearid || '01';
  cnt = 0;
  frpsns = '';
  dobal = 9;
  dores = 9;
  if (bal = 1) then dobal = 0;
  if (res = 1) then dores = 1;
  select f.chktype, f.name, c.balacc, c.resacc, c.nullonly from frhdrs f
    left join frchktypes c on (c.ref = f.chktype)
    where f.symbol = :frhdr
    into :chktype, :frhname, :bal, :res, :nullonly;

  if (bal = 1) then dobal = 0;
  if (res = 1) then dores = 1;

  for select symbol from frhdrs
        where chktype = :chktype
      into :frhdr2
    do begin
      delete from frvhdrs
        where frhdr = :frhdr2 and period = :cperiod
          and company = :currentcompany and frvtype = 1;
      insert into frvhdrs (frhdr, period, company, regdate, regoper, frvtype)
        values (:frhdr2, :cperiod, :currentcompany, current_date, :operator, 1);
    end


  for select A.account, A.bktype, A.descript from accounting a
          where a.yearid = :yearid
            and company = :currentcompany
            and currency = :country_currency
            and (a.bktype = :dobal or a.bktype = :dores)
          order by A.account
          into :account, :bktype, :descript
      do begin
        if (bktype = 0) then bktypetxt = 'bilansowe';
          else if (bktype = 1) then bktypetxt = 'wynikowe';
        for select c.proced, c.psymbol from frchkaccounts c
              join frhdrs f on (f.symbol = c.frhdr)
          where c.account = :account
            and c.period = :cperiod
            and f.chktype = :chktype
          into :alg, :frpsn
          do begin
            cnt = cnt + 1;
            frpsns = frpsns || :frpsn || '  ';
          end
        if (cnt = 0) then suspend;
          else if (nullonly <> 1) then suspend;
        cnt = 0;
        alg = '';
        frpsns = '';
      end
end^
SET TERM ; ^
