--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_CHECK_IS_INT(
      CHECKVAL varchar(40) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint)
   as
declare variable INTVAL integer;
begin
  status = 1;
  intval = cast(checkval as integer);
  when any
  do status = 0;
end^
SET TERM ; ^
