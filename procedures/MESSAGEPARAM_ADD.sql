--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MESSAGEPARAM_ADD(
      MESSAGEREF integer,
      PARAMNAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMVALUE varchar(255) CHARACTER SET UTF8                           ,
      ATTACHMENTTYPE smallint)
   as
begin
  insert into s_messageparams(s_message, paramname, paramvalue, attachmenttype)
    values (:messageref, :paramname, :paramvalue, :attachmenttype);
end^
SET TERM ; ^
