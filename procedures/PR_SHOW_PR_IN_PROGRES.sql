--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_SHOW_PR_IN_PROGRES(
      KTM varchar(80) CHARACTER SET UTF8                           )
  returns (
      FIRSTOPER integer,
      SHOPERNUM integer,
      KTMOUT varchar(80) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      WERSNUMBER integer,
      OPERTYPE varchar(255) CHARACTER SET UTF8                           ,
      OPERNAME varchar(255) CHARACTER SET UTF8                           ,
      GUIDESYMBOL varchar(255) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4),
      QUANTITYRES numeric(14,4),
      QUANTITYSHORTAGES numeric(14,4),
      OPERPARAM varchar(255) CHARACTER SET UTF8                           ,
      PRDEPART varchar(20) CHARACTER SET UTF8                           )
   as
declare variable prguide integer;
declare variable firstamountin numeric(14,4);
declare variable firstamountresult numeric(14,4);
declare variable firstamountshortages numeric(14,4);
declare variable nextoper integer;
declare variable nextamountin numeric(14,4);
declare variable nextamountresult numeric(14,4);
declare variable nextamountshortages numeric(14,4);
begin
  for
    select prg.ref, prg.symbol, prg.wersja, prg.prdepart
      from prschedguides prg
      where prg.status = 1 and prg.ktm = :ktm
      into prguide, guidesymbol, wersnumber, prdepart
  do begin
    select ref from wersje where ktm = :ktm and nrwersji = :wersnumber into wersjaref;
    ktmout = ktm;
    firstoper = null;
    select first 1 pro.ref, pro.amountin, pro.amountresult, pro.amountshortages, prsho.number, prsho.params3
      from prschedopers pro
        join prshopers prsho on (prsho.ref = pro.shoper)
      where pro.guide = :prguide and pro.activ = 1 and pro.amountin > 0
      order by pro.number
      into firstoper, firstamountin, firstamountresult, firstamountshortages, shopernum, operparam;
    opername = 'PRZED 1 OPER.';
    opertype = 'PRZED 1 OPER.';
    quantity = 0;
    quantityres = 0;
    quantityshortages = 0;
    if (firstoper is not null) then
    begin
      quantity = firstamountin;
      quantityres = firstamountin - firstamountresult - firstamountshortages;
      quantityshortages = 0;
    end
    firstoper = null;
    if (quantityres > 0) then
      suspend;
    for
      select pro.ref, pro.oper, prsho.descript, pro.amountin, pro.amountresult, pro.amountshortages, prsho.number, prsho.params3
        from prschedopers pro
          join prshopers prsho on (prsho.ref = pro.shoper)
        where pro.guide = :prguide and pro.activ = 1 and pro.amountin > 0
        order by pro.number
        into firstoper, opername, opertype, firstamountin, firstamountresult, firstamountshortages, shopernum, operparam
    do begin
      if (firstoper is not null) then
      begin
        if (firstamountresult = 0) then
        begin
          quantity = firstamountin;
          quantityres = firstamountresult;
          quantityshortages = firstamountshortages;
          if (quantityres > 0 or (quantity > 0 and quantityres = 0)) then
            suspend;
        end
        else
        begin
          select first 1 prso.ref, prso.amountin, prso.amountresult, prso.amountshortages
            from prschedoperdeps prod
              join prschedopers prso on (prso.ref = prod.depto and prod.depfrom = :firstoper and prso.activ = 1)
            into nextoper, nextamountin, nextamountresult, nextamountshortages;
          if (nextamountin is null) then nextamountin =0;
          if (nextamountresult is null) then nextamountresult =0;
          if (nextamountshortages is null) then nextamountshortages =0;
          quantity = firstamountin;
          quantityres = firstamountresult - nextamountresult - nextamountshortages;
          quantityshortages = firstamountshortages;
          if (quantityres > 0) then
            suspend;
        end
      end
    end
  end
  opername = 'OCZEKUJE NA PW';
  opertype = 'OCZEKUJE NA PW';
  for
    select prg.ref, prg.symbol, prg.wersja, prg.amount, prg.amountzreal
      from prschedguides prg
      where prg.status = 2 and prg.ktm = :ktm and prg.amount <> prg.amountzreal
      into prguide, guidesymbol, wersnumber, quantity, quantityres
  do begin
    quantityres = quantity - quantityres;
    firstoper = null;
    suspend;
  end
end^
SET TERM ; ^
