--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CONTACT_CONTROL(
      MEDIUM varchar(20) CHARACTER SET UTF8                           ,
      SIGNS varchar(255) CHARACTER SET UTF8                           ,
      TRYB smallint,
      STABLE varchar(20) CHARACTER SET UTF8                           ,
      SFIELD varchar(20) CHARACTER SET UTF8                           ,
      SKEY varchar(80) CHARACTER SET UTF8                           ,
      DESCRIPTION varchar(60) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE LEN SMALLINT;
DECLARE VARIABLE POS SMALLINT;
DECLARE VARIABLE POS_AKT SMALLINT;
DECLARE VARIABLE SIGNS_TMP VARCHAR(255);
DECLARE VARIABLE SIGNS_AKT VARCHAR(100);
DECLARE VARIABLE SIGN VARCHAR(1);
DECLARE VARIABLE SPOSITION SMALLINT;
DECLARE VARIABLE FULLNUMBER VARCHAR(30);
begin
  -- kasowanie z ksiazki telefonicznej gdy wykasowano telefony.
  if( (:signs is null or :signs = '') and :tryb < 1
      and (:medium = 'CELL' or :medium = 'PHONE' or :medium = 'FAX') ) then begin
    delete from cphones where STABLE = :stable and SFIELD = :sfield
        and SKEY = :SKEY;
    exit;
  end
  if( :medium <> 'EMAIL' and :medium <> 'CELL'
      and :medium <> 'PHONE' and :medium <> 'FAX') then
    exception CONTACT_EXPT 'Nie podano medium komunikacji';
  if(:signs is null or :signs = '') then exit;
  if( (:signs is null or :signs = '' or coalesce(char_length(:signs),0) < 6) and tryb < 1 ) then -- [DG] XXX ZG119346
    exception CONTACT_EXPT 'Bledna dlugosc '||:medium;
  if( :medium = 'EMAIL' ) then
  begin
    if( :signs not like '%_@_%._%'
         or :signs like 'www.%'
         or :signs like '@%'
         or :signs like '%@'
         or :signs not like '%@%'
    ) then
      exception CONTACT_EXPT 'Adres '||:signs||' wydaje sie byc bledny';
  end
  else if(:medium in ('CELL','PHONE','FAX') ) then
  begin
    len = coalesce(char_length(:signs),0); -- [DG] XXX ZG119346
    if(:len < 9 and :medium = 'CELL' and tryb < 1) then
      exception CONTACT_EXPT 'Za malo znaków dla tel. komorkowego';
    if(:len < 7 and (:medium = 'PHONE' or :medium = 'FAX') and tryb < 1) then
      exception CONTACT_EXPT 'Za malo znaków dla tel. stacjonarnego';
    pos = 1;
    sposition = 0;
    while (pos > 0)
    do begin
      pos = position(';' in :signs);
      if(:pos = 0 and coalesce(char_length(:signs),0) > 0) then -- [DG] XXX ZG119346
        signs_akt = substring(:signs from  1 for coalesce(char_length(:signs),0)); -- [DG] XXX ZG119346
      else if(:pos > 0 and coalesce(char_length(:signs),0) > 0) then -- [DG] XXX ZG119346
        signs_akt = substring(:signs from  1 for  :pos);
      signs = substring(:signs from  :pos + 1 for coalesce(char_length(:signs),0)); -- [DG] XXX ZG119346
      pos_akt = 0;
      signs_tmp = :signs_akt;
      len = coalesce(char_length(:signs_akt),0); -- [DG] XXX ZG119346
      if( :signs_akt <> ';' and :signs_akt <> '' and :signs_akt is not null) then
      begin
        sposition = :sposition + 1;
        fullnumber = '';
        while (:pos_akt < :len)
        do begin
          pos_akt = pos_akt + 1;
          sign = substring(:signs_tmp from 1 for 1);
          signs_tmp = substring(:signs_tmp from  2 for  :len - :pos_akt + 1);
          if( :sign not in ('1','2','3','4','5','6','7','8','9','0','-','+','(',')','/','\',' ',';')
          ) then
            exception CONTACT_EXPT 'numer '||:signs_akt||' posiada niedozwolony znak: '||:sign;
          else begin
            if(:sign not in ('-','+','(',')','/','\',';',' ')) then
              fullnumber = :fullnumber||:sign;
          end
        end
        -- obsuga ksiazki telefonicznej - wstawianie i aktualizacja rekordow
        if (not exists(select ref from CPHONES where STABLE = :stable
             and SFIELD = :sfield and SKEY = :SKEY and SPOSITION = :sposition)
        ) then

          insert into CPHONES (fullnumber, stable, sfield, skey, sposition, description)
                        values(:fullnumber, :stable, :sfield , :skey, :sposition, :description);
        else if( exists(select ref from CPHONES where STABLE = :stable
                 and SFIELD = :sfield and SKEY = :SKEY and SPOSITION = :sposition
                 and CPHONES.fullnumber <> :fullnumber)
        ) then
          update CPHONES set fullnumber = :fullnumber, description = :description
            where STABLE = :stable and SFIELD = :sfield and SKEY = :SKEY and SPOSITION = :sposition;
      end
    end
    -- obsluga ksiazki telefonicznej - kasowanie rekordow
    delete from cphones where STABLE = :stable and SFIELD = :sfield
        and SKEY = :SKEY and SPOSITION > :sposition;
  end
  --suspend;
end^
SET TERM ; ^
