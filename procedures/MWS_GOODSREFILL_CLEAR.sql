--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_GOODSREFILL_CLEAR(
      WHIN varchar(3) CHARACTER SET UTF8                           ,
      MWSORDTYPE integer,
      VERS integer,
      MAXLEVEL integer,
      DEEP integer,
      MAXCNT integer)
  returns (
      STATUS integer)
   as
declare variable VERSTMP integer;
declare variable GOOD varchar(40);
declare variable QUANTITY numeric(14,4);
declare variable MWSCONSTLOCP integer;
declare variable MWSCONSTLOCL integer;
declare variable QUANTITYSTOCK numeric(14,4);
declare variable QUANTITYREFILL numeric(14,4);
declare variable QUANTITYREFILLED numeric(14,4);
declare variable QREFILL numeric(14,4);
declare variable TOUPDATE numeric(14,4);
declare variable MWSGOODREFILL integer;
declare variable WHAREAB integer;
declare variable PRIORITY smallint;
declare variable BUFFORS varchar(100);
declare variable BUFFOR integer;
declare variable WHAREALOGL integer;
declare variable STOP smallint;
declare variable GOODPRIORITY smallint;
declare variable MWSPALLOCP integer;
declare variable WHAREAP integer;
declare variable WHAREALOGP integer;
declare variable REFMWSORDOUT integer;
declare variable MWSPALLOCL integer;
declare variable WHAREAL integer;
declare variable REFILL integer;
declare variable ORDVERS integer;
declare variable docposid integer;
begin
  status = 1;
  priority = 1;
  -- czy wolno brac towar z lokacji niedostepnych dla pickerow "0"
  execute procedure get_config('MWSTAKEFROMBUFFOR',2) returning_values buffors;
  if (buffors is null or buffors = '') then
    buffor = 0;
  else
    buffor = cast(buffors as smallint);
  -- nie sciagamy towaru, jezeli mozna go brac z gory
  if (buffor > 0) then
    exit;
  refmwsordout = null;
  for
    select vers, good, quantity, priority
      from XK_GOODS_REFILL_ORDER(:whin,6)
      into verstmp, good, quantity, goodpriority
  do begin
    mwsconstlocl = null;
    mwsconstlocp = null;
    whareab = null;
    refmwsordout = null;
    -- sprawdzenie czy poprzednie zlecenia nie zestawily juz tego towaru w mixie
    select sum(m.quantity - m.ordered)
      from mwsgoodsrefill m
        left join wersje v on (v.ref = m.vers)
        left join dokumnag d on (d.ref = m.docid)
        left join sposdost s on (s.ref = d.sposdost)
        left join xk_goods_relocate x on (x.vers = m.vers and x.wh = m.wh)
      where d.ref is not null and m.preparegoods = 1 and m.vers = :verstmp and m.wh = :whin
        and exists (select s.ref from mwsstock s join mwsconstlocs l on (l.ref = s.mwsconstloc and l.act > 0)
           where s.vers = m.vers and s.blocked = 0 and (s.x_blocked<>1))  --XXX Ldz ZG 126909)
        and not exists(select first 1 xk.mwsconstloc
          from xk_mws_take_full_pal_check(m.wh,d.grupasped,null,0) xk
          group by xk.mwsconstloc)
      into quantity;
    if (quantity is null) then quantity = 0;
     execute procedure XK_MWS_GET_BEST_SOURCE_3(:GOOD ,:VERSTMP ,null, null, :MWSORDTYPE, :WHIN, null, null, null, null, null, null, null, null, null, null, null, 1, :quantity)
      returning_values (mwsconstlocp, mwspallocp, whareap, wharealogp, refill);
    if (mwsconstlocp is not null and quantity > 0) then
    begin
      quantitystock = null;
      select first 1 s.mwsconstloc, s.good, s.quantity - s.blocked
        from mwsstock s
          left join mwsconstlocs mc on (mc.ref = s.mwsconstloc)
          left join whareas w on (w.ref = mc.wharea)
          left join whsecs ws on (ws.ref = w.whsec)
          left join dostawy d on (d.ref = s.lot)
        where s.mwsconstloc = :mwsconstlocp and s.vers = :verstmp
          and mc.act > 0 and mc.goodsav = 0 and mc.ref is not null and w.ref is not null
          and :quantity > 0 and (ws.deliveryarea = 2 or ws.deliveryarea = 6)
          and (s.x_blocked<>1)  --XXX Ldz ZG 126909
        into mwsconstlocp, good, quantitystock;
      if (quantitystock is null) then quantitystock = 0;
      if (quantitystock > 0) then
      begin
        quantityrefilled = 0;
        stop = 0;
        /* XXX KBI Ponieważ lokacji docelowej (planowanej) bede szuka dopiero podczas genrowania MG
        -- okreslamy lokacje koncowa
        execute procedure XK_MWS_GET_BEST_LOCATION_3(:GOOD ,:verstmp ,null, null, :MWSORDTYPE, :whin, null, null, 1, 1, null, null, null, null, null, null, null, 1, :quantitystock)
          returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
        if (mwsconstlocl is not null) then
        */
          execute procedure MWS_MWSCONSTLOC_REFILL(:mwsordtype,:mwsconstlocp,:mwsconstlocl,'',:goodpriority,:maxcnt,:verstmp)
            returning_values refmwsordout;
        if (refmwsordout is not null and refmwsordout > 0) then
        begin
          -- aktualizacja towarow ze zlecenia
          for
            select a.vers, a.quantity
              from mwsacts a
              where a.mwsord = :refmwsordout and a.quantity > 0
              into ordvers, quantityrefilled
          do begin
            for
              select f.docposid
                from mwsgoodsrefill f
                where f.preparegoods = 1 and f.reallater = 0 and f.vers = :ordvers
                into docposid
            do begin
              execute procedure mws_docpos_waiting_pow (whin,docposid,ordvers);
            end
          end
          if (not exists (select first 1 ref from mwsacts where mwsord = :refmwsordout)) then
          begin
            update mwsords set status = 0 where ref = :refmwsordout;
            delete from mwsords where ref = :refmwsordout;
          end
        end
      end
    end
  end
end^
SET TERM ; ^
