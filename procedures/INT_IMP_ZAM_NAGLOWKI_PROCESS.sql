--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_ZAM_NAGLOWKI_PROCESS(
      SESJAREF SESJE_ID,
      TABELA STRING35,
      REF INTEGER_ID,
      BLOKUJZALEZNE SMALLINT_ID = 0,
      TYLKONIEPRZETWORZONE SMALLINT_ID = 0,
      AKTUALIZUJSESJE SMALLINT_ID = 1)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable sesja sesje_id;
declare variable tmpstatus smallint_id;
declare variable tmpmsg string255;
declare variable sql memo;
begin
  --zmiana pustych wartosci na null-e
  if (:sesjaref = 0) then
    sesjaref = null;
  if (trim(:tabela) = '') then
    tabela = null;
  if (:ref = 0) then
    ref = null;
  if (:blokujzalezne is null) then
    blokujzalezne = 0;
  if (:tylkonieprzetworzone is null) then
    tylkonieprzetworzone = 0;
  if (:aktualizujsesje is null) then
    aktualizujsesje =1;

  status = 1;
  msg = '';
  tmpstatus = 1;
  tmpmsg = '';

  --sprawdzenie czy wszystkie potrzebne informacje sa
  if (:sesjaref is null and :ref is null) then
  begin
    status = -1;
    msg = 'Za mało parametrów. Brak numeru sesji oraz REF';
    exit; --EXIT
  end

  if (:sesjaref is null) then
  begin
    if (:tabela is null) then
    begin
      status = -1;
      msg = 'Za mało parametrów. Brak numeru sesji oraz nazwy tabeli';
      exit; --EXIT
    end

    sql = 'select sesja from '||:tabela||' where ref='||:ref;
    execute statement :sql into :sesja;

  end
  else
    sesja = :sesjaref;

  if (:sesja is null) then
  begin
    status = -1;
    msg = 'Nie znaleziono numeru sesji.';
    exit; --EXIT
  end

  if (exists(select first 1 1 from rdb$procedures p where p.rdb$procedure_name = 'X_INT_IMP_ZAM_NAGLOWKI_PROCESS')) then
  begin
    sql = 'execute procedure X_INT_IMP_ZAM_NAGLOWKI_PROCESS('||:sesjaref||','||coalesce(''''||:tabela||'''','null')||','||
        coalesce(:ref,'null')||','||:blokujzalezne||','||:tylkonieprzetworzone||','||:aktualizujsesje||')';
    execute statement :sql into :status, :msg;
  end
  else
  begin
    --TODO, standardowa procedura
  end

  --aktualizacja informacji o sesji
  if (:aktualizujsesje = 1) then
  begin
    select status, msg from int_sesje_aktualizuj(:sesja, 1) into :tmpstatus, :tmpmsg;
    if(coalesce(:status,-1) = 1) then
      status = :tmpstatus; --ML jezeli przed aktualizacjas sesji status byl zly, to nie zmieniamy na ok
    msg = substring((msg || coalesce(:tmpmsg,'')) from 1 for 255);
  end

  suspend;
end^
SET TERM ; ^
