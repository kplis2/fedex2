--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_CREATE_STAT(
      TYP varchar(3) CHARACTER SET UTF8                           ,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      NAGFA integer,
      DATA timestamp)
  returns (
      REF integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           )
   as
declare variable dokpozref integer;
declare variable cenacen numeric(14,4);
declare variable ilosc numeric(14,4);
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable exist smallint;
begin
  exist=0;--znacznik czy wystapil niedobor ilosciowy
  execute procedure GEN_REF('DOKUMNAG') returning_values :ref;
  if(:data is null) then data = current_date;
  insert into DOKUMNAG(REF,MAGAZYN, TYP, DATA) values (:ref,:magazyn, :typ, :data);

  --bierzemy tylko te pozycje pozfak'a gdzie bedzie niedobor ilosciowy na stanyil
  for select p.ilosc-si.ilosc,p.ktm,p.wersjaref,w.cena_zakn from pozfak p
    left join stanyil si on (p.wersjaref=si.wersjaref) left join wersje w on (p.wersjaref=w.ref)
    where p.dokument=:nagfa and (si.ilosc-p.ilosc)<0 and si.magazyn=:magazyn
    into :ilosc,:ktm,:wersja,:cenacen do
  begin
    execute procedure GEN_REF('DOKUMPOZ') returning_values :dokpozref;
    insert into dokumpoz(ref,dokument,ilosc,ktm,wersjaref,cena)
                 values (:dokpozref,:ref,:ilosc,:ktm,:wersja,:cenacen);
    exist=1;
  end

  --jezeli nie dodano zadnej pozycji usuwamy dokument
  if (exist=0) then delete from dokumnag where ref=:ref;
end^
SET TERM ; ^
