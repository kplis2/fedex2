--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DROP_ALL_PROCEDURES
  returns (
      PROCEDURERESULT varchar(4000) CHARACTER SET UTF8                           )
   as
declare variable procedureName varchar(31);
declare variable procedureImputs smallint;
declare variable procedureOutputs smallint;
declare variable parameterName varchar(31);
declare variable parameterType smallint;
declare variable parameterLength smallint;
declare variable parameterPrecision smallint;
declare variable parameterScale smallint;
declare variable parameterTypeName varchar(31);
declare variable parameterTypeNameDecoded varchar(50);
declare variable parameterInputCounter smallint;
declare variable parameterOutputCounter smallint;
declare variable triggerName varchar(31);
declare variable pom varchar(10);

begin
  exception universal 'rozwalanie_bazy';
  pom = '
  ';
  for
    select p.rdb$procedure_name, p.rdb$procedure_inputs, p.rdb$procedure_outputs
      from rdb$procedures p
      where rdb$procedure_name <> 'DROP_ALL_PROCEDURES'
      order by rdb$procedure_name
      into :procedureName, :procedureImputs, :procedureOutputs
  do begin
    procedureResult = '' || 'ALTER PROCEDURE ' || procedureName;
    parameterInputCounter = 0;
    parameterOutputCounter = 0;

    for
      select p.rdb$parameter_name, p.rdb$parameter_type, f.rdb$field_length, f.rdb$field_precision, f.rdb$field_scale, t.rdb$type_name
        from rdb$procedure_parameters p
          join rdb$fields f on f.rdb$field_name = p.rdb$field_source
          join rdb$types t on (t.rdb$field_name = 'RDB$FIELD_TYPE') and (t.rdb$type = f.rdb$field_type)
        where p.rdb$PROCEDURE_name = :procedureName
          order by p.rdb$parameter_type, rdb$PARAMETER_number
        into :parameterName, :parameterType, :parameterLength, :parameterPrecision, :parameterScale, :parameterTypeName
     do begin
       -- skladam pelna nazwe typu

       if (parameterTypeName = 'LONG                          ') then
         parameterTypeNameDecoded = 'INTEGER';
       else if (parameterTypeName = 'SHORT                          ') then
         parameterTypeNameDecoded = 'SMALLINT';
       else if (parameterTypeName = 'TEXT                           ') then
         parameterTypeNameDecoded = 'CHAR(' || parameterLength || ')' ;
       else if (parameterTypeName = 'TIMESTAMP                      ') then
         parameterTypeNameDecoded = 'TIMESTAMP';
       else if (parameterTypeName = 'DATE                           ') then
         parameterTypeNameDecoded = 'DATE';
       else if (parameterTypeName = 'VARYING                        ') then
         parameterTypeNameDecoded = 'VARCHAR(' || parameterLength || ')';
       else if (parameterTypeName = 'INT64                          ') then
       begin
         parameterScale = parameterScale * -1;
         parameterTypeNameDecoded = 'NUMERIC(' || parameterPrecision || ',' || parameterScale || ')';
       end
       else
         parameterTypeNameDecoded = parameterTypeName;

       if (parameterType = 0) then
       begin
         if (parameterInputCounter = 0) then
           procedureResult = procedureResult || ' (' || pom;

         parameterInputCounter = parameterInputCounter + 1;


         procedureResult = procedureResult || parameterName || ' ' || parameterTypeNameDecoded;

         if (parameterInputCounter < procedureImputs) then
           procedureResult = procedureResult || ',' || pom;
         else
           procedureResult = procedureResult || ')' ;


       end -- end if (parameterType = 0) then
       else if (parameterType = 1) then
       begin
           if (parameterOutputCounter = 0) then
           procedureResult = procedureResult || pom || 'RETURNS (' || pom;

         parameterOutputCounter = parameterOutputCounter + 1;

         procedureResult = procedureResult || parameterName || ' ' || parameterTypeNameDecoded;

          if (parameterOutputCounter < procedureOutputs) then
           procedureResult = procedureResult || ',' || pom;
         else
           procedureResult = procedureResult || ')';

       end -- end  else if (parameterType = 1) then
     end  -- end for select (2)

    procedureResult = procedureResult || pom || 'AS' || pom || 'begin' || pom || '  suspend;' || pom || 'end' || pom;

    execute statement procedureResult;
    --suspend;
  end -- end for select (1)

  -- usun wszytkie triggery
  for
    select t.rdb$trigger_name
      from rdb$triggers t
      where rdb$system_flag = 0 or rdb$system_flag is null
      order by t.rdb$trigger_name
     into :triggerName
  do begin
    execute statement 'DROP TRIGGER ' || triggerName;
  end

  -- usun wszystkie procedury
  for
    select p.rdb$procedure_name
      from rdb$procedures p
      where rdb$procedure_name <> 'DROP_ALL_PROCEDURES'
        order by rdb$procedure_name
    into :procedureName
  do begin
    execute statement 'DROP PROCEDURE ' || procedureName;
  end


  -- usun wszystkie constrainy
  for
    select rdb$relation_constraints.rdb$constraint_name,
        rdb$relation_constraints.rdb$relation_name
      from rdb$relation_constraints
      where rdb$relation_constraints.rdb$constraint_type in ('FOREIGN KEY')
    into :procedurename, :parameterName
  do begin
    execute statement 'ALTER TABLE ' || :parameterName || 'DROP CONSTRAINT ' || :procedurename;
  end

  for
    select rdb$relation_constraints.rdb$constraint_name,
        rdb$relation_constraints.rdb$relation_name
      from rdb$relation_constraints
      where rdb$relation_constraints.rdb$constraint_type in ('PRIMARY KEY')
    into :procedurename, :parameterName
  do begin
    execute statement 'ALTER TABLE ' || :parameterName || 'DROP CONSTRAINT ' || :procedurename;
  end

  for
    select rdb$relation_constraints.rdb$constraint_name,
        rdb$relation_constraints.rdb$relation_name
      from rdb$relation_constraints
      where rdb$relation_constraints.rdb$constraint_type in ('CHECK')
    into :procedurename, :parameterName
  do begin
    execute statement 'ALTER TABLE ' || :parameterName || 'DROP CONSTRAINT ' || :procedurename;
  end
  for
    select rdb$relation_constraints.rdb$constraint_name,
        rdb$relation_constraints.rdb$relation_name
      from rdb$relation_constraints
      where rdb$relation_constraints.rdb$constraint_type in ('UNIQUE')
    into :procedurename, :parameterName
  do begin
    execute statement 'ALTER TABLE ' || :parameterName || 'DROP CONSTRAINT ' || :procedurename;
  end
  -- usun wszystkie indexy

  for
    select RDB$INDICES.rdb$index_name
      from
      RDB$INDICES
      where RDB$INDICES.rdb$index_name not like 'IBE$%'
        and RDB$INDICES.rdb$system_flag is null
    into :procedurename
  do begin
    execute statement 'DROP INDEX ' || :procedureName;
  end
--  exception universal 'rozwalanie_bazy';
end^
SET TERM ; ^
