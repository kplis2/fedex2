--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRNAGZAM_GEN_OLDDEL(
      MODE smallint,
      NAGZAM integer,
      POZZAM integer,
      PRNAGZAM integer,
      PRPOZZAM integer,
      PRSHEETDOWN smallint)
   as
DECLARE VARIABLE CURREF INTEGER;
DECLARE VARIABLE REJESTR VARCHAR(3);
DECLARE VARIABLE PRREJZAMGEN VARCHAR(3);
DECLARE VARIABLE ID VARCHAR(30);
begin
  select substring(cvalue from 1 for 3) from get_config('PRREJZAMGEN',1) into :prrejzamgen;
  if(mode <> 3) then begin
    for select ref, rejestr, id from nagzam where kpopzam = :nagzam into :curref, :rejestr, :id
    do begin
      if(rejestr <> prrejzamgen) then exception prnagzam_gen 'Nie można usunąć zlecenia '||:id;
      else delete from nagzam where ref = :curref;
    end
  end else begin
    delete from pozzam where zamowienie = :prnagzam;
    if(prsheetdown is not null and prsheetdown = 1) then
    execute procedure prnagzam_gen_olddel_down(:prnagzam) returning_values :curref;
  end
end^
SET TERM ; ^
