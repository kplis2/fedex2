--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_KLIENT_INSERT(
      TELEFON varchar(40) CHARACTER SET UTF8                           ,
      AKTYWNY smallint,
      HASLO varchar(32) CHARACTER SET UTF8                           ,
      TELKOM varchar(40) CHARACTER SET UTF8                           ,
      WITRYNA smallint,
      ODDZIAL varchar(10) CHARACTER SET UTF8                           ,
      SPOSDOST integer,
      SPOSPLAT integer,
      COMPANY integer,
      GRUPA integer,
      DKODP varchar(10) CHARACTER SET UTF8                           ,
      DMIASTO varchar(60) CHARACTER SET UTF8                           ,
      DNAZWA varchar(255) CHARACTER SET UTF8                           ,
      EMAIL varchar(255) CHARACTER SET UTF8                           ,
      MIASTO varchar(60) CHARACTER SET UTF8                           ,
      KODP varchar(10) CHARACTER SET UTF8                           ,
      ULICA varchar(60) CHARACTER SET UTF8                           ,
      DULICA varchar(60) CHARACTER SET UTF8                           ,
      TYP integer,
      FIRMA smallint,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      SPRZEDAWCA_SKROT SHORTNAME_ID,
      FSKROT SHORTNAME_ID,
      NIP varchar(15) CHARACTER SET UTF8                           ,
      NAZWISKO varchar(255) CHARACTER SET UTF8                           ,
      IMIE varchar(255) CHARACTER SET UTF8                           ,
      ID varchar(40) CHARACTER SET UTF8                           )
  returns (
      STATUS integer,
      MSG varchar(100) CHARACTER SET UTF8                           )
   as
declare variable idklient integer;
declare variable sprzedawca integer;
declare variable licznosc integer;
declare variable prostynip varchar(10);
declare variable rola smallint;
begin

  status = 0;
  rola = 0;
  if(sprzedawca_skrot is not null and sprzedawca_skrot <> '') then
  begin
  -- podano sprzedawce
     select ref from sprzedawcy
     where LOWER(skrot)=LOWER(:sprzedawca_skrot)
     into :sprzedawca;
     if(sprzedawca is null) then
     begin
        status = 0;
        msg ='Sprzedawca o podanym ID nie istnieje!';
        suspend;
        EXIT;
     end
  end

  select Count(*) from uzykli where email = :email
  into :licznosc;
  if (licznosc > 0) then
    begin
      status = 0;
      msg ='Istnieje użytkownik o podanym adresie email';
      suspend;
      EXIT;
    end

  select Count(*) from uzykli where id = :id
  into :licznosc;
  if (licznosc > 0) then
    begin
      status = 0;
      msg ='Istnieje użytkownik o podanym loginie';
      suspend;
      EXIT;
    end

  idklient = null;
  if (nip is not null and nip <> '') then
  begin
    prostynip = replace(nip, '-', '');
    select k.ref
    from klienci k
    where prostynip = :prostynip and company = 1 and firma = 1
    into :idklient;
  end
    if (idklient is null) then
    begin
            -- klient nie istnieje
            select id from gen_ref('klienci') into :idklient;
            insert into klienci(REF,ID,FSKROT,SPRZEDAWCA,FIRMA,TYP,IMIE,NAZWISKO,NAZWA,NIP,ULICA,MIASTO,KODP,EMAIL,DNAZWA,DULICA,DMIASTO,DKODP,GRUPA,COMPANY,SPOSPLAT,SPOSDOST,ODDZIAL)
            values(:idklient,:id,:fskrot,:sprzedawca,:firma,:typ,:imie,:nazwisko,:nazwa,:nip,:ulica,:miasto,:kodp,:email,:dnazwa,:dulica,:dmiasto,:dkodp,:grupa,:company,:sposplat,:sposdost,:oddzial);
            rola = 1;
    end
    else
    begin
            -- klient istnieje
            rola = 0;
    end


    if(firma != 1) then
       begin
           -- klient indywidualny
           rola = 1;
        end

  insert into uzykli(KLIENT,ID,IMIE,NAZWISKO,NAZWA,TELKOM,TELEFON,WITRYNA,ROLA,HASLO,EMAIL,AKTYWNY)
  values(:idklient,:id,:imie,:nazwisko,:nazwa,:telkom,:telefon,:witryna,:rola,:haslo,:email, :aktywny);
  status = 1;
  msg = 'Użytkownik zosta dodany';

  suspend;
end^
SET TERM ; ^
