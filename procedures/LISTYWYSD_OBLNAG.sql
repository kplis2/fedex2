--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_OBLNAG(
      LISTWYSD integer)
  returns (
      WAGA numeric(15,4),
      ILOSCPACZEK integer,
      ILOSCPALET integer)
   as
begin
  select sum(lwo.waga), count(lwo1.ref) as iloscpaczek, count(lwo2.ref) as iloscpalet
    from listywysdroz_opk lwo
      left join listywysdroz_opk lwo1 on (lwo.ref = lwo1.ref and lwo1.typ = 0)
      left join listywysdroz_opk lwo2 on (lwo.ref = lwo2.ref and lwo2.typ = 1)
    where lwo.listwysd = :listwysd
      and coalesce(lwo.rodzic,0) = 0
  into :waga, :iloscpaczek, :iloscpalet;

  if (:waga is null) then waga = 0;
  if (:iloscpaczek is null) then iloscpaczek = 0;
  if (:iloscpalet is null) then iloscpalet = 0;
  suspend;
end^
SET TERM ; ^
