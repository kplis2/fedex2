--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ECAL_SETPATTERN(
      CALENDAR integer,
      CYEAR integer,
      FROMDATE date = null)
  returns (
      D timestamp)
   as
declare variable daykind smallint;
declare variable workstart time;
declare variable workend time;
declare variable workstart2 time;
declare variable workend2 time;
declare variable worktime integer;
declare variable descript varchar(30);
declare variable pattkind smallint;
declare variable periodb smallint;
declare variable dk timestamp;
declare variable pdate date;
declare variable stdcalendar integer;
declare variable add_holiday smallint;
declare variable ispatt4year smallint;
begin
/*Generuje dni kalednarza CALENDAR w zadanym roku CYEAR na podstawie
  zdefiniowanego wczesniej wzoraca. Jezeli wzorzec nie istnieje to zostanie utworzony.
  Generowanie w zakresie roku CYEAR mozna rozpoczac od zadanego dnia FROMDATE (PR30969)*/

  if (coalesce(cyear,0) < 0 or calendar is null) then
    exception bad_parameter;
  if (fromdate is null) then
    fromdate = (cast(cyear as varchar(4)) || '-01-01');
  else if (extract(year from fromdate) <> cyear) then
    exception universal '''Data od'' poza zakresem roku: ' || cyear ;

  --ustalam typ wzorca
  select pattkind, periodb, add_holiday
    from ecalendars
    where ref = :calendar
    into :pattkind, :periodb, :add_holiday;

  --sprawdzam czy istnieje wzorzec na dany rok
  select first 1 1 from ecalpattdays
    where calendar = :calendar and pyear = :cyear
    into :ispatt4year;
  ispatt4year = coalesce(ispatt4year,0);

  --ustawiam sie na pierwszym dniu, od którego bedziemy aktualizowac kalendarz
  d = fromdate;

--obsluga wzorca tygodniowego --------------------------------------------------
  if (pattkind = 0) then
  begin
    --gdy brak wzorca to go utworz
    if (ispatt4year = 0) then
      execute procedure epatt_default(calendar, cyear);

    --usuwam dane z tabeli tygodni (PR30368)
    execute procedure get_config('STDCALENDAR',2)
      returning_values stdcalendar;
    if (calendar = stdcalendar) then
      delete from weeks
        where weekyear = :cyear
          and firstday + 3 >= :fromdate;

    --przejscie przez kazdy dzien w roku (od fromdate) w porownaniu ze wzorcem
    while (extract(year from d) = cyear)
    do begin
      daykind = null;
    
      --wstawiam dni do tabeli tygodni (PR30368)
      if (extract(weekday from d) = 4 and calendar = stdcalendar) then
        insert into weeks (firstday) values (:d - 3);

      select daykind, workstart, workend, workstart2, workend2, worktime, descript
        from ecalpattdays
        where calendar = :calendar
          and pdate = :d
          and pattkind = 0  --cykl jednorazowy
        into :daykind, :workstart, :workend, :workstart2, :workend2, :worktime, :descript;
    
      if (daykind is null) then
      begin
        select daykind, workstart, workend, workstart2, workend2, worktime, descript
          from ecalpattdays
          where calendar = :calendar
            and pyear = :cyear
            and dayofweek = extract(weekday from :d)
            and pattkind = 1  --cykl tygodniowy
          into :daykind, :workstart, :workend, :workstart2, :workend2, :worktime, :descript;

        if (daykind is null) then
          exception niepelny_wzorzec;
      end

      --wstawiam znalezione wartosci do kalendarza
      if (exists(select first 1 1 from ecaldays where calendar = :calendar and cdate = :d)) then
        update ecaldays set daykind = :daykind, workstart = :workstart, workend = :workend,
          workstart2 = :workstart2, workend2 = :workend2, worktime = :worktime, descript = :descript
        where calendar = :calendar and cdate = :d;
      else
        insert into ecaldays (calendar, cdate, daykind, workstart, workend, workstart2, workend2, worktime, descript)
          values (:calendar, :d, :daykind, :workstart, :workend, :workstart2, :workend2, :worktime, :descript);
      d = d + 1;
    end
  end else
--obsluga wzorca wielobrygadowego ----------------------------------------------
  if (pattkind = 1) then
  begin
    --gdy brak wzorca to go utworz
    if (ispatt4year = 0) then
      execute procedure epatt_4bryg(calendar, cyear, periodb, add_holiday);

    dk = (cast(cyear as varchar(4)) || '-12-31');
    while(d <= dk) do
    begin
      for
        select daykind, workstart, workend, workstart2, workend2, worktime, descript
          from ecalpattdays
          where calendar = :calendar
            and pyear = :cyear
            and pattkind = 2
          into :daykind, :workstart, :workend, :workstart2, :workend2, :worktime, :descript
      do begin
        --wstawiam znalezione wartosci do kalendarza
        if (daykind is null) then
          exception niepelny_wzorzec;
        --sprawdzenie bo moze probowac dodac dni z nastepnego roku
        if (d <= dk) then begin
          if (exists(select first 1 1 from ecaldays where calendar = :calendar and cdate = :d)) then
            update ecaldays set daykind = :daykind, workstart = :workstart, workend = :workend,
              workstart2 = :workstart2, workend2 = :workend2, worktime = :worktime, descript = :descript
            where calendar = :calendar and cdate = :d;
          else
            insert into ecaldays (calendar, cdate, daykind, workstart, workend, workstart2, workend2, worktime, descript)
              values (:calendar, :d, :daykind, :workstart, :workend, :workstart2, :workend2, :worktime, :descript);
        end
        d = d + 1;
      end
    end
    --i uzupelnienie jednorazowych wydarzen
    for
      select daykind, workstart, workend, worktime, workstart2, workend2, descript, pdate
        from ecalpattdays
        where calendar = :calendar
          and pyear = :cyear
          and pdate >= :fromdate
          and pattkind = 0
        into :daykind, :workstart, :workend, :worktime, :workstart2, :workend2, :descript, :pdate
    do begin
      update ecaldays
        set daykind = :daykind, workstart = :workstart, workend = :workend,
          workstart2 = :workstart2, workend2 = :workend2,
          worktime = :worktime, descript = :descript
        where calendar = :calendar and cdate = :pdate;
    end
  end else
--obsluga kolejnych wzorcow ----------------------------------------------------
    exception nieznany_rodzaj_wzorca;

  suspend;
end^
SET TERM ; ^
