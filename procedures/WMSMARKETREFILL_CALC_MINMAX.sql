--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WMSMARKETREFILL_CALC_MINMAX(
      WH DEFMAGAZ_ID,
      DOCTYPES STRING40,
      INTERVAL INTEGER_ID)
   as
declare variable vers wersje_id;
declare variable quantity quantity_mws;
declare variable mwsconstloc mwsconstlocs_id;
declare variable whsec whsecs_id;
declare variable good ktm_id;
begin
  update wmsmarketrefill r set r.act = 0 where r.wh = :wh and r.act = 1;
  for
    select p.wersjaref, p.ktm, sum(p.ilosc)
      from dokumnag d
        left join dokumpoz p on p.dokument = d.ref
      where d.magazyn = :wh and position(';'||d.typ||';' in :doctypes) > 0
        and d.data >= current_date - :interval
      group by p.wersjaref, p.ktm
      into vers, good, quantity
  do begin
    mwsconstloc = null;
    whsec = null;
    select s.mwsconstloc
      from mwsconstlocsymbs s
      where s.wh = :wh and s.vers = :vers
      into mwsconstloc;
    select c.whsec
      from mwsconstlocs c where c.ref = :mwsconstloc
      into whsec;
    update or insert into wmsmarketrefill (wh, whsec, vers, good, maxq, minq, act)
      values (:wh, :whsec, :vers, :good, :quantity, cast(:quantity/2 as integer),1)
      matching (wh, whsec, vers);
  end
end^
SET TERM ; ^
