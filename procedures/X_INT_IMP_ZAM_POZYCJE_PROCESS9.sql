--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_INT_IMP_ZAM_POZYCJE_PROCESS9(
      SESJAREF SESJE_ID,
      TABELA STRING35,
      REF INTEGER_ID,
      BLOKUJZALEZNE SMALLINT_ID = 0,
      TYLKONIEPRZETWORZONE SMALLINT_ID = 0,
      AKTUALIZUJSESJE SMALLINT_ID = 1)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable stabela string35;
--declare variable tmptabela string35;
declare variable sprocedura string255;
declare variable szrodlo zrodla_id;
declare variable skierunek smallint_id;
declare variable sesja sesje_id;
declare variable sql memo;
--declare variable tmpsql memo;
declare variable tmpstatus smallint_id;
declare variable tmpmsg string255;
declare variable error_row smallint_id;
declare variable errormsg_row string255;
declare variable errormsg_all string255;
declare variable tmperror smallint_id;
declare variable statuspoz smallint_id;
declare variable nz_ref integer_id;
declare variable nz_wydania integer_id;
declare variable nz_zewnetrzny integer_id;
declare variable pz_ref integer_id;
begin
  status = 1;
  msg = '';
  errormsg_all = '';

  --zmiana pustych wartosci na null-e
  if (:sesjaref = 0) then
    sesjaref = null;
  if (trim(:tabela) = '') then
    tabela = null;
  if (:ref = 0) then
    ref = null;
  if (:blokujzalezne is null) then
    blokujzalezne = 0;
  if (:tylkonieprzetworzone is null) then
    tylkonieprzetworzone = 0;
  if (:aktualizujsesje is null) then
    aktualizujsesje =1;

  --sprawdzenie czy wszystkie potrzebne informacje sa
  if (:sesjaref is null and :ref is null) then
  begin
    status = -1;
    msg = 'Za mało parametrów. Brak numeru sesji oraz REF';
    exit; --EXIT
  end

  if (:sesjaref is null) then
  begin
    if (:tabela is null) then
    begin
      status = -1;
      msg = 'Za mało parametrów. Brak numeru sesji oraz nazwy tabeli';
      exit; --EXIT
    end

    sql = 'select sesja from '||:tabela||' where ref='||:ref;
    execute statement :sql into :sesja;

  end
  else
    sesja = :sesjaref;

  if (:sesja is null) then
  begin
    status = -1;
    msg = 'Nie znaleziono numeru sesji.';
    exit; --EXIT
  end

  --pobranie parametrow sesji
  select s.tabela, s.procedura, s.zrodlo, s.kierunek
    from int_sesje s
    where ref = :sesja
  into :stabela, :sprocedura, :szrodlo, :skierunek;

  if (:tabela is not null) then --jesli podano tabele na wejsciu traktuje ja nadrzednie
    stabela = :tabela;

  --wlasciwe przetworzenie
  execute procedure set_global_param ('AKTUOPERATOR',74); --#JOTOOD
  for
    select n.ref, n.wydania, n.zewnetrzny, p.ref
      from int_imp_zam_pozycje p
        left join int_imp_zam_naglowki n on (n.ref = p.skadref)
      where (p.sesja = :sesja or :sesja is null)
        and ((:ref is not null and p.ref = :ref and :tabela is null) or
             (:ref is not null and :tabela is not null and p.skadref = :ref and p.skadtabela = :tabela) or
             (:ref is null))
      order by p.ref
    into :nz_ref, :nz_wydania, :nz_zewnetrzny, :pz_ref
  do begin
    if (:nz_wydania = 0 and :nz_zewnetrzny = 1) then
      execute procedure x_int_imp_zamowienia_pozycje(:sesjaref, null, :pz_ref)
        returning_values :status, :msg;
    else
      execute procedure x_int_imp_dokumenty_pozycje(:sesjaref, null, :pz_ref)
        returning_values :status, :msg;
  end

  --aktualizacja informacji o sesji
  if (aktualizujsesje = 1) then
  begin
    select status, msg from int_sesje_aktualizuj(:sesja, 1) into :tmpstatus, :tmpmsg;
  end
  suspend;
end^
SET TERM ; ^
