--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_CALCULATE_SBASE(
      EMPLREF integer,
      FROMBASEPERIOD varchar(6) CHARACTER SET UTF8                           ,
      TOBASEPERIOD varchar(6) CHARACTER SET UTF8                           ,
      CURRENTCOMPANY integer)
   as
declare variable EMPLOYEE integer;
declare variable SBASE numeric(14,2);
declare variable SBASEGROSS numeric(14,2);
declare variable SWORKEDDAYS integer;
declare variable WORKDAYS integer;
declare variable FROMDATE date;
declare variable TODATE date;
declare variable EMPLSTART date;
declare variable TMP integer;
declare variable TAKEIT smallint;
declare variable HOURWAGE numeric(14,2);
declare variable ONDATE date;
declare variable FC numeric(14,2);
declare variable FEP numeric(14,2);
declare variable FRP numeric(14,2);
declare variable WSK numeric(14,6);
declare variable WSK_OLD numeric(14,6);
declare variable IPER varchar(6);
declare variable ISBASE numeric(14,2);
declare variable PERIOD varchar(6);
declare variable TMPSBASE numeric(14,2);
declare variable BASESDIFF numeric(14,2);
declare variable EMPLTYPE smallint;
declare variable STDCALENDAR integer;
begin

  for
    select distinct P.employee, PR.cper, pr.empltype
      from epayrolls PR
        join eprpos P on (P.payroll = PR.ref)
        join employees E on (E.ref = P.employee and (E.ref = :EMPLREF or :EMPLREF is NULL))
        left join emplcontracts c on pr.emplcontract = c.ref
       where PR.cper <= :TOBASEPERIOD
         and PR.cper >= :FROMBASEPERIOD
         and E.company = :currentcompany
         and (pr.empltype = 1 or c.iflags like '%FC;%')
         and coalesce(pr.tosbase,1) = 1 --PR30950
      into :employee, :period, :empltype
  do begin

    execute procedure efunc_datesfromperiod(period)
      returning_values fromdate, todate; 
    if (empltype = 1) then
      select wd from ecal_work(:employee, :fromdate, :todate)
        into workdays;
    else
    begin
      --jesli nie jest pracownikiem bierz ze standardowego kalendarza
      execute procedure GET_CONFIG('STDCALENDAR', 2)
        returning_values stdcalendar;
      if (not exists(select first 1 1 from ECALENDARS where ref = :stdcalendar)) then
        exception BRAK_KALENDARZA;
      select count(*)
        from ECALDAYS cd
        join edaykinds edk on edk.ref = cd.daykind
        where cd.CDATE >= :fromdate and cd.CDATE <= :todate
          and edk.daytype = 1 --dni robocze
          and cd.calendar = :stdcalendar
        into :workdays;
    end

    tmp = null;
    select sum(a.workdays)
      from eabsences a
        join ecolparams c on a.ecolumn = c.ecolumn
      where a.employee = :employee and a.fromdate >= :fromdate and a.todate <= :todate
        and c.param = 'ABSTYPE' and c.pval in (1,2)
        and a.correction in (0,2)
      into :tmp;
    tmp = coalesce(tmp, 0);

    sworkeddays = workdays - tmp;
    if (sworkeddays >= workdays/2.00) then
      takeit = 1;
    else
      takeit = 0;

    select min(fromdate)
      from employment
      where employee = :employee
      into :emplstart;

    if (emplstart > fromdate) then takeit = 0;
    sbase = 0;
    sbasegross = 0;
    WSK_old = 0;
    tmpsbase = 0;
    for
      select distinct(PR.iper)
        from epayrolls PR
          join eprpos P on (P.payroll = PR.ref)
          left join emplcontracts c on pr.emplcontract = c.ref
        where PR.cper = :period and P.employee = :employee
          and (pr.empltype = 1 or c.iflags like '%FC;%')
          and coalesce(pr.tosbase,1) = 1 --PR30950
        into :iper
    do begin
      isbase = null;
      select sum(P.pvalue)
        from epayrolls PR
          join eprpos P on (P.payroll = PR.ref)
          join ecolumns C on (C.number = P.ecolumn)
          left join emplcontracts co on pr.emplcontract = co.ref
        where PR.cper = :period and PR.iper = :iper and C.cflags containing ';CHR;' and P.employee = :employee
          and (pr.empltype = 1 or co.iflags like '%FC;%')
          and coalesce(pr.tosbase,1) = 1 --PR30950
      into :isbase;
      isbase = coalesce(isbase, 0);

      hourwage = null;
      select max(P.pvalue)
        from epayrolls PR
          join eprpos P on (P.payroll = PR.ref)
          left join emplcontracts co on pr.emplcontract = co.ref
        where PR.cper = :period and PR.iper = :iper and P.employee = :employee
          and P.ecolumn = 910
          and (pr.empltype = 1 or co.iflags like '%FC;%')
          and coalesce(pr.tosbase,1) = 1 --PR30950
        into :hourwage;

      if (hourwage is not null) then
      begin
        tmp = null;
        select sum(A.workdays)
          from eabsences A
            join epayrolls P on (A.epayroll = P.ref)
            join ecolparams c on a.ecolumn = c.ecolumn
          where P.cper = :period and P.iper = :iper and A.employee = :employee
            and A.fromdate >= :fromdate and A.todate <= :todate
            and c.param = 'ABSTYPE' and c.pval = 0
            and A.correction in (0,2)
            and coalesce(p.tosbase,1) = 1 --PR30950
          into :tmp;
        tmp = coalesce(tmp, 0);

        isbase = isbase + ((workdays - tmp) * 8 * hourwage);
      end

      basesdiff = 0;
      select sum(case when p.ecolumn = 6000 then P.pvalue else -p.pvalue end)
        from epayrolls PR
          join eprpos P on (P.payroll = PR.ref)
          join ecolumns C on (C.number = P.ecolumn)
          left join emplcontracts co on pr.emplcontract = co.ref
        where PR.cper = :period and PR.iper = :iper and P.employee = :employee
         and p.ecolumn in (5990,6000,6040,6050)
         and (pr.empltype = 1 or co.iflags like '%FC;%')
        into :basesdiff;

      --roznica miedzy podstawa chorobowa a emeryt.-rentowa? => przekroczenie limitu skladek
      if (coalesce(basesdiff,0) = 0) then
      begin
        ondate = cast(substring(iper from 1 for 4)|| '-' ||substring(iper from 5 for 2)|| '-01' as date);

        execute procedure get_pval(ondate, currentcompany, 'FC')
          returning_values FC;
        execute procedure get_pval(ondate, currentcompany, 'FEP')
          returning_values FEP;
        execute procedure get_pval(ondate, currentcompany, 'FRP')
          returning_values FRP;

        WSK = FEP + FRP + FC;
      end else
      begin
        select sum(case when p.ecolumn in (6100,6110,6120) then P.pvalue * 100.00 else 0 end) /    --skladki
               sum(case when p.ecolumn not in (6100,6110,6120) then P.pvalue * 100.00 else 0 end)  --skladniki ZUS
          from epayrolls PR
            join eprpos P on (P.payroll = PR.ref)
            join ecolumns C on (C.number = P.ecolumn)
            left join emplcontracts co on pr.emplcontract = co.ref
          where PR.cper = :period and PR.iper = :iper and P.employee = :employee
            and (C.number in (6100,6110,6120) or C.cflags containing ';ZUS;')
            and (pr.empltype = 1 or co.iflags like '%FC;%')
           into :WSK;

        WSK = WSK * 100;
      end

      WSK = 1 - (WSK) * 0.01;
      if (WSK_OLD = 0) then
        WSK_OLD = WSK;

      sbasegross = sbasegross + isbase;

      if (WSK <> WSK_OLD) then
      begin
        sbase = sbase + tmpsbase * WSK_OLD;
        tmpsbase = isbase;
        WSK_OLD = WSK;
      end else
        tmpsbase = tmpsbase + isbase;
    end
    sbase = sbase + tmpsbase * WSK;

    if ((exists(select employee from eabsemplbases
        where employee = :employee and period = :period))) then
    begin
      update eabsemplbases set sbase = :sbase, workdays = :workdays,
          sworkeddays = :sworkeddays, sbasegross = :sbasegross, takeit = :takeit 
        where period = :period and employee = :employee;
    end else if (sbase > 0) then
    begin
      insert into eabsemplbases (employee, period, sbase, workdays, sworkeddays, takeit, sbasegross)
        values (:employee, :period, :sbase, :workdays, :sworkeddays, :takeit, :sbasegross);
    end
  end
end^
SET TERM ; ^
