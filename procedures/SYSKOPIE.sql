--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYSKOPIE(
      N integer)
  returns (
      NUMER integer,
      KOPIA integer)
   as
begin
  numer = 0;
  kopia = 0;
  while(numer<=n) do
  begin
    suspend;
    numer = numer+1;
    kopia = 1;
  end
end^
SET TERM ; ^
