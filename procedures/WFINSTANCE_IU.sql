--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WFINSTANCE_IU(
      REF WFINSTANCE_ID,
      OBJECTUUID NEOS_ID,
      UUID NEOS_ID,
      WFACTIVE SMALLINT_ID,
      PRIMARYATTACHMENT UUID,
      CREATIONEVENTREF WFEVENTS_ID,
      DISPLAYNAME STRING,
      OBJECTLABEL STRING255,
      SIGNATURE STRING255)
  returns (
      REFOUT WFINSTANCE_ID)
   as
begin
  if (exists(select ref from wfinstance where (ref = :ref))) then
  begin
    update wfinstance
    set objectuuid = :objectuuid,
        uuid = :uuid,
        wfactive = :wfactive,
        primaryattachment = :primaryattachment,
        creationeventref = :creationeventref,
        displayname = :displayname,
        signature = :signature,
        objectlabel = :objectlabel
    where (ref = :ref);
    refout = ref;
  end
  else
    insert into wfinstance (
        objectuuid,
        uuid,
        wfactive,
        primaryattachment,
        creationeventref,
        displayname,
        signature,
        objectlabel)
    values (
        :objectuuid,
        :uuid,
        :wfactive,
        :primaryattachment,
        :creationeventref,
        :displayname,
        :signature,
        :objectlabel)
   returning ref into :refout;
   suspend;
end^
SET TERM ; ^
