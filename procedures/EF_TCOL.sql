--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_TCOL(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      COL integer)
  returns (
      RET numeric(14,2))
   as
begin
/*DU: Personel - funkcja podaje wartosc skladnika COL z innych list plac
                 tego samego okresu podatkowego*/

  execute procedure ef_ftcolsum(:employee, :payroll, :prefix, null, :col, :col)
    returning_values ret;

  suspend;
end^
SET TERM ; ^
