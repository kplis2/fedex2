--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_STANYK(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      DATA timestamp,
      TRYB integer)
  returns (
      RKTM varchar(40) CHARACTER SET UTF8                           ,
      RWERSJA integer,
      RNAZWA varchar(255) CHARACTER SET UTF8                           ,
      RNAZWAT varchar(255) CHARACTER SET UTF8                           ,
      RWERSJAREF integer,
      STAN numeric(14,4),
      WARTOSC numeric(14,2),
      STANINW numeric(14,4),
      WARTOSCINW numeric(14,2))
   as
declare variable rref integer;
declare variable wydania smallint;
declare variable cenanew numeric(14,2);
declare variable wartosck numeric(14,2);
declare variable inwp integer;
declare variable inwm integer;
begin
    inwp = -1;
    inwm = -1;
    rktm='';
    rnazwa='';
    rwersja=0;
    staninw = 0;
    wartoscinw = 0;
    if(:tryb is null) then tryb = 0;
    if(:tryb>0) then begin
      select inwp,inwm from inwenta where ref=:tryb into :inwp, :inwm;
      if(:inwp is null or :inwp=0) then inwp = -1;
      if(:inwm is null or :inwm=0) then inwm = -1;
    end
    if(:tryb=0) then begin
      for select dokumpoz.wersjaref,
             sum(dokumpoz.ilosc*(1-2*defdokum.wydania)),
             sum(dokumpoz.pwartosc*(1-2*defdokum.wydania))
        from dokumpoz
        join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.dokument)
        left join DEFDOKUM on ( DOKUMNAG.typ = DEFDOKUM.symbol)
        left join TOWARY on (TOWARY.KTM=DOKUMPOZ.KTM)
        where dokumnag.magazyn=:magazyn
          and dokumnag.akcept in (1,8)
          and dokumnag.data<=:data
          and TOWARY.usluga <> 1
        group by dokumpoz.wersjaref
        into :rwersjaref,:stan, :wartosc
        do begin
          if(:stan is null) then stan=0;
          if(:wartosc is null) then wartosc=0;
          if(:stan<>0 or :wartosc<>0) then begin
            select ktm,nazwa,nazwat,nrwersji from wersje where ref=:rwersjaref
            into :rktm,:rnazwa,:rnazwat,:rwersja;
            suspend;
          end
        end
      for select dokumnotp.wersjaref,
             sum(dokumnotp.wartosc*(1-2*defdokum.wydania))
        from dokumnotp
        join dokumnot on (dokumnot.ref=dokumnotp.dokument)
        left join defdokum on (dokumnot.typ = defdokum.symbol)
        left join TOWARY on (TOWARY.KTM=DOKUMNOTP.ktm)
        where dokumnot.magazyn=:magazyn
          and dokumnot.akcept in (1,8)
          and dokumnot.data<=:data
          and TOWARY.usluga <> 1
        group by dokumnotp.wersjaref
        into :rwersjaref,:wartosc
        do begin
          stan = 0;
          if(:wartosc is null) then wartosc = 0;
          if(:wartosc<>0) then begin
            select ktm,nazwa,nazwat,nrwersji from wersje where ref=:rwersjaref
            into :rktm,:rnazwa,:rnazwat,:rwersja;
            suspend;
          end
        end
    end else begin
      /* z pominieciem dokumentow inwentaryzacyjnych */
      for select dokumpoz.wersjaref,
             sum(dokumpoz.ilosc*(1-2*defdokum.wydania)),
             sum(dokumpoz.pwartosc*(1-2*defdokum.wydania))
        from dokumpoz
        join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.dokument)
        left join DEFDOKUM on ( DOKUMNAG.typ = DEFDOKUM.symbol)
        left join TOWARY on (TOWARY.KTM=DOKUMPOZ.KTM)
        where dokumnag.magazyn=:magazyn
          and dokumnag.akcept in (1,8)
          and dokumnag.data<=:data
          and (dokumnag.ref not in (select coalesce(inwm,0) from inwenta where magazyn=:magazyn and data=:data))
          and (dokumnag.ref not in (select coalesce(inwp,0) from inwenta where magazyn=:magazyn and data=:data))
          and TOWARY.usluga <> 1
        group by dokumpoz.wersjaref
        into :rwersjaref,:stan, :wartosc
        do begin
          if(:stan is null) then stan=0;
          if(:wartosc is null) then wartosc=0;
          if(:stan<>0 or :wartosc<>0) then begin
            select ktm,nazwat,nazwa,nrwersji from wersje where ref=:rwersjaref
            into :rktm,:rnazwat,:rnazwa,:rwersja;
            suspend;
          end
        end
      for select dokumnotp.wersjaref,
             sum(dokumnotp.wartosc*(1-2*defdokum.wydania))
        from dokumnotp
        join dokumnot on (dokumnot.ref=dokumnotp.dokument)
        left join defdokum on (dokumnot.typ = defdokum.symbol)
        left join TOWARY on (TOWARY.KTM=DOKUMNOTP.ktm)
        where dokumnot.magazyn=:magazyn
          and dokumnot.akcept in (1,8)
          and dokumnot.data<=:data
          and ((dokumnot.dokumnagkor not in (select coalesce(inwm,0) from inwenta where magazyn=:magazyn and data=:data)
                and dokumnot.dokumnagkor not in (select coalesce(inwp,0) from inwenta where magazyn=:magazyn and data=:data)
               ) or (dokumnot.dokumnagkor is null)
              )
          and TOWARY.USLUGA<>1
        group by dokumnotp.wersjaref
        into :rwersjaref,:wartosc
        do begin
          stan = 0;
          if(:wartosc is null) then wartosc = 0;
          if(:wartosc<>0) then begin
            select ktm,nazwat,nazwa,nrwersji from wersje where ref=:rwersjaref
            into :rktm,:rnazwat,:rnazwa,:rwersja;
            suspend;
          end
        end
      /* tylko dokumenty inwentaryzacyjne */
      stan = 0;
      wartosc = 0;
      for select dokumpoz.wersjaref,
             sum(dokumpoz.ilosc*(1-2*defdokum.wydania)),
             sum(dokumpoz.pwartosc*(1-2*defdokum.wydania))
        from dokumpoz
        join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.dokument)
        left join DEFDOKUM on ( DOKUMNAG.typ = DEFDOKUM.symbol)
        left join TOWARY on (TOWARY.KTM=DOKUMPOZ.ktm)
        where ((dokumnag.ref in (select coalesce(inwm,0) from inwenta where magazyn=:magazyn and data=:data)) or
               (dokumnag.ref in (select coalesce(inwp,0) from inwenta where magazyn=:magazyn and data=:data))
              ) and TOWARY.USLUGA<>1
        group by dokumpoz.wersjaref
        into :rwersjaref,:staninw, :wartoscinw
        do begin
          if(:staninw is null) then staninw=0;
          if(:wartoscinw is null) then wartoscinw=0;
          if(:staninw<>0 or :wartoscinw<>0) then begin
            select ktm,nazwat,nazwa,nrwersji from wersje where ref=:rwersjaref
            into :rktm,:rnazwat,:rnazwa,:rwersja;
            suspend;
          end
        end
      for select dokumnotp.wersjaref,
             sum(dokumnotp.wartosc*(1-2*defdokum.wydania))
        from dokumnotp
        join dokumnot on (dokumnot.ref=dokumnotp.dokument)
        left join defdokum on (dokumnot.typ = defdokum.symbol)
        left join TOWARY on (TOWARY.KTM=DOKUMNOTP.ktm)
        where (dokumnot.dokumnagkor in (select coalesce(inwm,0) from inwenta where magazyn=:magazyn and data=:data) or
               dokumnot.dokumnagkor in (select coalesce(inwp,0) from inwenta where magazyn=:magazyn and data=:data)
              )and dokumnot.dokumnagkor is not null and TOWARY.USLUGA<>1
        group by dokumnotp.wersjaref
        into :rwersjaref,:wartoscinw
        do begin
          staninw = 0;
          if(:wartoscinw is null) then wartoscinw = 0;
          if(:wartoscinw<>0) then begin
            select ktm,nazwat,nazwa,nrwersji from wersje where ref=:rwersjaref
            into :rktm,:rnazwat,:rnazwa,:rwersja;
            suspend;
          end
        end
    end
end^
SET TERM ; ^
