--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_CHECK_FIELDS_VALUES(
      OTABLE STRING255,
      FIELDS STRING1024,
      FIELDVALUES STRING1024)
  returns (
      RET SMALLINT_ID)
   as
declare variable field VARCHAR(1024);
declare variable val VARCHAR(1024);
declare variable stm string8191;
BEGIN
  stm = '';
  if(coalesce(otable,'') <> '') then
  begin
    if(exists(select first 1 1 from rdb$relations where rdb$relation_name = :otable)) then
    begin
      FOR select field.STRINGOUT, val.stringout
          from parsestring(:fields, ';') field
            left join parsestring(:fieldvalues, ';') val on (1=1 and field.lp = val.lp)
        INTO :field, :val
      DO BEGIN
        stm = stm || ' and ' || field || ' = ''' || val ||''' ';
    
      end
      if (stm <> '') then
      begin
          stm = 'select first 1 1 from'||:otable||'where 1 = 1 '||stm;
          execute statement stm
            into ret;
      end
      if (ret is null) then
        ret = 0;
      suspend;
    end else
      exception universal'Podana tabela nie istnieje w bazie danych';
  end else
    exception universal'Nie podano nazwy tabeli';
END^
SET TERM ; ^
