--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ECONPAYROLLSPOS_ADD(
      ECONPAYROLL integer,
      ECONPAYROLLNAG integer,
      EMPLOYEE integer,
      CHECKINCREASE smallint,
      EMITDATE smallint,
      ACCORD smallint)
   as
begin
  update econtrpayrollspos set econtrpayrollspos.countnag = 1
    where econtrpayrollspos.econtrpayrollnag = :econpayrollnag;
  delete from econtrpayrollspos where econtrpayrollspos.econtrpayrollnag = :econpayrollnag
    and econtrpayrollspos.epresencelistsposhelp is null;
  execute procedure ECONTRPAYROLLS_CALCULATE(:econpayrollnag);
  if (checkincrease is null) then
    checkincrease = 0;
  if (emitdate is null) then
    emitdate = 1;
  if (:accord = 0) then
  begin
    insert into econtrpayrollspos (epresencelistspos, countnag,econtrpayrolls, econtrpayrollnag, accord, stable, sref, ssymbol, points, topay,
        econtractsdef, econtractsposdef, sdate)
    select countwages.slistpos, 1, :ECONPAYROLL, :ECONPAYROLLNAG, countwages.accord, countwages.stable, countwages.sref, countwages.ssymbol, countwages.points, countwages.amount,
      countwages.act, countwages.actpos, countwages.sdate
      from countwages(:econpayrollnag,:EMPLOYEE,:emitdate,:checkincrease);
  end else if (:accord = 1) then
  begin
    insert into econtrpayrollspos (epresencelistspos, countnag,econtrpayrolls, econtrpayrollnag, accord, stable, sref, ssymbol, points, topay,
        econtractsdef, econtractsposdef, sdate)
    select countwages_accord.slistpos, 1, :ECONPAYROLL, :ECONPAYROLLNAG, countwages_accord.accord, countwages_accord.stable, countwages_accord.sref, countwages_accord.ssymbol, countwages_accord.points, countwages_accord.amount,
      countwages_accord.act, countwages_accord.actpos, countwages_accord.sdate
      from countwages_accord(:econpayrollnag,:EMPLOYEE,:emitdate,:checkincrease);
  end
  update econtrpayrollspos set econtrpayrollspos.countnag = 0
    where econtrpayrollspos.econtrpayrollnag = :econpayrollnag;
  execute procedure ECONTRPAYROLLS_CALCULATE(:econpayrollnag);
end^
SET TERM ; ^
