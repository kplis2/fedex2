--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDECL_REMOVEFROMGROUP(
      DECLREF integer)
  returns (
      LOGMSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable SCODE varchar(20);
declare variable DECLGROUP integer;
declare variable ISGROUP integer;
declare variable PERSMSG varchar(255) = '';
declare variable PERS varchar(200);
declare variable GROUPNAME varchar(40);
declare variable GROUPSTATE smallint;
begin

  select df.systemcode, coalesce(p.person,''), coalesce(d.declgroup,0)
       , df.isgroup, d.groupname
    from edeclarations d
      join edecldefs df on (df.ref = d.edecldef)
      left join persons p on (d.person = p.ref)
    where d.ref = :declref
    into scode, pers, declgroup, isgroup, :groupname;

  if (declgroup > 0) then
    select state, coalesce(groupname,ref) from edeclarations where ref = :declgroup
      into :groupstate, :groupname;

  if (pers <> '') then persmsg = pers;
  else if (groupname <> '') then persmsg = groupname;

  persmsg = coalesce(' ' || scode || ' : ' || persmsg || ' - ', '');

  if (isgroup = 1 or declgroup = 0) then
    logmsg = ascii_char(215) || persmsg || 'usunąć z grupy można tylko deklaracje zgrupowane';
  else if (isgroup = 0 and groupstate >= 1) then
    logmsg = ascii_char(164) || persmsg || 'nie można usunąć deklaracji z grupy, ponieważ deklaracja grupująca ''' || groupname || ''' jest już zatwierdzona';

  if (logmsg > '') then
    exit;
  else begin
    update edeclarations set declgroup = null where ref = :declref;
    logmsg = ascii_char(149) || persmsg || 'usunięto z grupy ''' || groupname || '''';
    suspend;
  end
  
  when any do
  begin
    logmsg = ascii_char(215) || persmsg || 'nie udało się usunąć deklaracji z grupy ' || coalesce(groupname,'');
    suspend;
  end
end^
SET TERM ; ^
