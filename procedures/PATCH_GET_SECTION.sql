--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PATCH_GET_SECTION(
      SECTION STRING)
  returns (
      APP_BLOCK STRING8191)
   as
declare variable IDENT STRING;
declare variable VAL STRING1024;
begin
  app_block = '['||:section||']
';
  for select ident, val from s_appini where section = :section
  order by ident
  into :ident, :val
  do begin
    app_block = app_block || ident || '=' || val || '
';
  end
  suspend;
end^
SET TERM ; ^
