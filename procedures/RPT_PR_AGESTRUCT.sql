--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PR_AGESTRUCT(
      DATA_PODANA date,
      CZYPEL smallint,
      COMPANY integer)
  returns (
      LICZNIK25 integer,
      LICZNIK30 integer,
      LICZNIK40 integer,
      LICZNIK50 integer,
      LICZNIKPONAD50 integer,
      LICZNIK25M integer,
      LICZNIK30M integer,
      LICZNIK40M integer,
      LICZNIK50M integer,
      LICZNIKPONAD50M integer,
      LICZNIK25K integer,
      LICZNIK30K integer,
      LICZNIK40K integer,
      LICZNIK50K integer,
      LICZNIKPONAD50K integer)
   as
declare variable wiek integer;
declare variable data_ur timestamp;
declare variable tmp_wiek integer;
declare variable rok_ur integer;
declare variable plec integer;
declare variable person integer;
begin
  licznik25 = 0;
  licznik30 = 0;
  licznik40 = 0;
  licznik50 = 0;
  licznikponad50 = 0;
  licznik25k = 0;
  licznik30k = 0;
  licznik40k = 0;
  licznik50k = 0;
  licznikponad50k = 0;
  licznik25m = 0;
  licznik30m = 0;
  licznik40m = 0;
  licznik50m = 0;
  licznikponad50m = 0;
  for
    select distinct p.ref, P.birthdate, P.sex
      from persons P
        join employees E on (E.person=P.ref)
        join employment Em on (Em.employee=E.ref)
      where (Em.workdim = 1 or :czypel = 0)
        and EM.fromdate <= :data_podana and (EM.todate >= :data_podana or EM.todate is null)
        and e.company = :company
      into :person, :data_ur, :plec
  do begin
    rok_ur = cast(extract(year from data_ur) as integer);
    -- Wiek pracownika -----------------------------------------------------------------
    tmp_wiek = cast((extract(year from :data_podana) - :rok_ur) as integer);
    if (extract(month from :data_ur) > extract(month from :data_podana)) then
      tmp_wiek = tmp_wiek - 1;
    else if (extract(month from :data_ur) = extract(month from :data_podana)) then
      if (extract(day from :data_ur) >= extract(day from :data_podana)) then
        tmp_wiek = tmp_wiek - 1;
    wiek = tmp_wiek;
    if (wiek < 25) then
    begin
      licznik25 = licznik25 + 1;
      if (plec = 0) then
        licznik25k = licznik25k + 1;
      else
        licznik25m = licznik25m + 1;
    end else if (wiek >= 25 and wiek < 30) then
    begin
      licznik30 = licznik30 + 1;
      if (plec = 0) then
        licznik30k = licznik30k + 1;
      else
        licznik30m = licznik30m + 1;
    end else if (wiek >= 30 and wiek < 40) then
    begin
      licznik40 = licznik40 + 1;
      if (plec = 0) then
        licznik40k = licznik40k + 1;
      else
        licznik40m = licznik40m + 1;
    end else if (wiek >= 40 and wiek < 50) then
    begin
      licznik50 = licznik50 + 1;
      if (plec = 0) then
        licznik50k = licznik50k + 1;
      else
        licznik50m = licznik50m + 1;
    end else if (wiek >= 50) then
    begin
      licznikponad50 = licznikponad50 + 1;
      if (plec = 0) then
        licznikponad50k = licznikponad50k + 1;
      else
        licznikponad50m = licznikponad50m + 1;
    end
  end
  suspend;
end^
SET TERM ; ^
