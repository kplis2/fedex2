--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_E_ABSENCE_RECORD(
      EMPLOYEE integer,
      YEARID char(4) CHARACTER SET UTF8                           ,
      MONTHID char(2) CHARACTER SET UTF8                           ,
      DKCOLOUR smallint = 2)
  returns (
      WORKTIME numeric(14,4),
      RWORKTIME numeric(14,4),
      MDAYKIND smallint,
      ABSENCETYPE integer,
      EXTRAHOURS numeric(14,4),
      FIFTYHOURS numeric(14,4),
      HUNDREDHOURS numeric(14,4),
      NIGHTHOURS numeric(14,4),
      URLOPWYPOCZYNKOWY numeric(14,4),
      URLOPNAZADANIE numeric(14,4),
      URLOPOKOLICZNOSCIOWY numeric(14,4),
      URLOPBEZPLATNY numeric(14,4),
      URLOPWYCHOWAWCZY numeric(14,4),
      URLOPMACIERZYNSKI numeric(14,4),
      OPIEKANADDZIECKIEM numeric(14,4),
      CHOROBOWE numeric(14,4),
      ZASILEKZUS numeric(14,4),
      NNIEUSPRAWIEDLIWIONA numeric(14,4),
      INNEPLATNE numeric(14,4),
      INNENIEPLATNE numeric(14,4))
   as
declare variable TMPSTDDATE timestamp;
declare variable PROPORTIONAL integer;
declare variable DIMNUM numeric(14,2);
declare variable DIMDEN numeric(14,2);
declare variable I integer = 0;
declare variable AFROMDATE timestamp;
declare variable ATODATE timestamp;
declare variable ABSHOURS numeric(14,4);
declare variable STDCALENDAR integer;
declare variable EDAYKIND smallint;
begin
/*MWr Personel: Procedura generuje dane do wydruku: Ewidencja obecnosci pracownika.
  Do kolorowania dni na wydruku sluzy parametr DKCOLOUR: =0 - brak kolorowania,
  =1 - wg kalendarza standardowego, =2 - wg kalednarza pracownika */

  dkcolour = coalesce(dkcolour,2);

  execute procedure get_config('STDCALENDAR',2)
    returning_values :stdcalendar;

  --przejscie przez kazdy dzien kalendarza standardowego z zadanego okresu
  for
    select cd.cdate, dk.daykind from ecaldays cd
      join edaykinds dk on dk.ref = cd.daykind
      where calendar = :stdcalendar and cyear = :yearid
        and (cmonth = :monthid or :monthid = '13')
      order by cdate
      into :tmpstddate, :mdaykind
  do begin
    if (dkcolour = 0) then mdaykind = 1;
    i = i + 1;
    extrahours = 0;
    nighthours = 0;
    fiftyhours = 0;
    hundredhours = 0;
    urlopwypoczynkowy = 0;
    urlopnazadanie = 0;
    urlopokolicznosciowy = 0;
    urlopbezplatny = 0;
    urlopwychowawczy = 0;
    urlopmacierzynski = 0;
    opiekanaddzieckiem = 0;
    chorobowe = 0;
    zasilekzus = 0;
    nnieusprawiedliwiona = 0;
    inneplatne = 0;
    innenieplatne = 0;
    absencetype = 0;
    worktime = null;
    rworktime = null;
    proportional = null;
    abshours = null;
    afromdate = null;

    select coalesce(proportional,0), dimnum, dimden from employment
      where employee = :employee
        and fromdate <= :tmpstddate
        and coalesce(todate, :tmpstddate) >= :tmpstddate
      into :proportional, :dimnum, :dimden;

    if (proportional is not null) then
    begin
    --pracownik pracowal w danym dniu, pobieramy czas pracy (w godz) z jego kalendarza
      select first 1 worktime/3600.0000, dk.daykind from emplcaldays ec
      join edaykinds dk on dk.ref = ec.daykind
        where employee = :employee and cdate = :tmpstddate
        into :worktime, :edaykind;

      if (dkcolour = 2) then
        mdaykind = edaykind;

      if (proportional = 1) then
        worktime = worktime * dimnum / dimden;

      select fromdate, todate, ecolumn, coalesce(worksecs,0)/3600.0000
          from eabsences
          where employee = :employee and correction in (0,2)
            and fromdate <= :tmpstddate and :tmpstddate <= todate
        into :afromdate, :atodate, :absencetype, :abshours;

      if (afromdate is not null) then
      begin
        if(not((atodate - afromdate) = -1 and edaykind = 1 and abshours < worktime)) then
          abshours = worktime; --gdy nieobecnosc nie trwa 1 dzien roboczy to jej czas trwania z kalendarza

        if (absencetype in (40,50,60)) then
          chorobowe = abshours;
        else if (absencetype in (90,100,110,120)) then
          zasilekzus = abshours;
        else if (absencetype = 180) then
          opiekanaddzieckiem = abshours;
        else if (absencetype = 220) then
          urlopwypoczynkowy = abshours;
        else if (absencetype = 230) then
          urlopnazadanie = abshours;
        else if (absencetype = 250) then
          urlopokolicznosciowy = abshours;
        else if (absencetype = 260) then
          urlopwychowawczy = abshours;
        else if (absencetype in (140,150,270,280,290,440,450)) then
          urlopmacierzynski = abshours;
        else if (absencetype = 10) then
          nnieusprawiedliwiona = abshours;
        else if (absencetype = 300) then
          urlopbezplatny = abshours;
        else if (absencetype in (130,190,330,390)) then  --chor oczekiwanie, opieka bezpl, sluzba wojskowa, nieob uspr bezplatna
          innenieplatne = abshours;
        else
          inneplatne = abshours;
      end

      select sum(case when ecolumn = 600 then amount else 0 end)
           , sum(case when ecolumn = 610 then amount else 0 end)
           , sum(case when ecolumn = 640 then amount else 0 end)
        from eworkedhours
        where employee = :employee and hdate = :tmpstddate and ecolumn in (600,610,640)
        into :fiftyhours, :hundredhours, :nighthours;

      fiftyhours = coalesce(fiftyhours,0);
      hundredhours = coalesce(hundredhours,0);
      nighthours = coalesce(nighthours,0);

      extrahours = fiftyhours + hundredhours;
      rworktime = worktime + extrahours - coalesce(abshours,0);
      if (worktime = 0) then worktime = null;
    end
    suspend;
  end --for (petla przechodzenia przez dni kalendarza glownego
end^
SET TERM ; ^
