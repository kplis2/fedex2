--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BLANK_ANEKS(
      REFUMOWY integer,
      FIELDNAME varchar(31) CHARACTER SET UTF8                           ,
      MODE integer = 0)
  returns (
      RET varchar(80) CHARACTER SET UTF8                           )
   as
declare variable isannex smallint;
begin
  --DS: procedura blankuje aneks, wyszukuje wartosc pola FIELDNAME najpierw na ostatnim aneksie
  --jak go nie ma to na umowie
  execute statement 'select first 1 '||fieldname||', 1 from econtrannexes
    where emplcontract='||refumowy||' and atype = 1 order by fromdate desc'
    into :ret, :isannex;

  if (isannex is null) then --PR61955 
    execute statement 'select '||fieldname||' from emplcontracts where ref='||refumowy
    into :ret;

    /**
   *   jezeli pole jest polem numerycznym z mozliwa czescia dziesietna zmien notacje z kropki na przecinek
   *   3 - char
   *   7 - smallint
   *   8 - integer
   *  10 - float
   *  12 - date
   *  13 - time
   *  16 - numeric
   *  27 - double precision
   *  35 - timestamp
   *  37 - varchar
   * 261 - blob
   */
  if(mode = 0) then     --PR61955
    if (exists(select rf.rdb$field_name
      from rdb$relation_fields rf
        join rdb$fields f on rf.rdb$field_source = f.rdb$field_name
      where rf.rdb$field_name = upper(:fieldname)
        and rf.rdb$relation_name = 'EMPLCONTRACTS'
        and f.rdb$field_type in (10,16,27))) then
      ret = replace(ret,'.',',');
  suspend;

  when any do
    exception universal 'Brak pola '||coalesce(fieldname,'')||' na umowie';
end^
SET TERM ; ^
