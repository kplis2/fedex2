--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_EMPLCONTR2ZUA_4PERSON(
      PERSON integer)
  returns (
      EMPLCONTRACT integer)
   as
begin
/*MWr Personel: Procedura zwraca ref aktualnej umowy o prace EMPLCONTRACT dla
      zadanej osoby PERSON. Wykorzystywane przy zmienie adresu osoby, gdzie po
      tej operacji nalezy wykonac eksport do Platnika ZUS ZUA*/

  select first 1 m.emplcontract from employment m
    join employees e on (e.ref = m.employee)
    where e.person = :person
      and m.fromdate <= current_date
    order by m.fromdate desc
    into :emplcontract;

  emplcontract = coalesce(emplcontract,0);
  suspend;
end^
SET TERM ; ^
