--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FKFA_GENERATE_INVENTORY(
      FAINVHDR integer,
      COMPANY integer)
   as
declare variable symbol varchar(20);
declare variable name varchar(80);
declare variable department varchar(10);
declare variable purchasedt timestamp;
declare variable fvalue numeric(15,2);
declare variable fnetval numeric(15,2);
declare variable fredemption numeric(15,2);
declare variable amgrp varchar(1);
declare variable invdate timestamp;
declare variable fvaldoc numeric(15,2);
declare variable fredemptdoc numeric(15,2);
declare variable ref integer;
declare variable famort numeric(15,2);
declare variable branch varchar(10);
declare variable comparment varchar(10);
declare variable fbranch varchar(10);
declare variable fdepartment varchar(10);
declare variable fcomparment varchar(10);
declare variable dodaj integer;
declare variable obranch varchar(10);
declare variable odepartment varchar(10);
declare variable ocomparment varchar(10);
begin
  select invdate, coalesce(branch,''), coalesce(department, ''),
         coalesce(comparment,'') from fainvhdr
    where ref=:fainvhdr
    into :invdate, :branch, :department, :comparment;

  delete from FAINVPOS where FAINVHDR=:FAINVHDR;


  for
    select f.ref, f.symbol, f.name, f.department, f.purchasedt, f.fvalue, f.fredemption, f.amgrp,
           f.oddzial, f.comparment
    from fxdassets f
    where (f.activ = 1 or exists(select first 1 ref from valdocs
                                   where liquidation = 1 and fxdasset = f.ref
                                     and operdate > :invdate))
      and f.company = :company
      and f.purchasedt <= :invdate
    into :ref, :symbol, :name, :fdepartment, :purchasedt, :fvalue, :fredemption, :amgrp,
         :fbranch, :fcomparment
  do begin
    dodaj = 1;
    /* sprawdzenie pierwszej zmiany oddzialu po dacie inwentaryzacji */
    if (branch <> '') then begin
      obranch = :fbranch;
      select first 1 fd.obranch from fareldocs fd
        where fd.fxdasset = :ref
          and fd.operdate > :invdate
          and fd.obranch <> fd.branch
        order by fd.operdate
        into :obranch;
      if (obranch <> :branch or :fbranch is null) then
        dodaj = 0;
    end
    /* sprawdzenie pierwszej zmiany wydzialu po dacie inwentaryzacji */
    if (dodaj = 1 and department <> '') then begin
      odepartment = :fdepartment;
      select first 1 fd.odepartment from fareldocs fd
        where fd.fxdasset = :ref
          and fd.operdate > :invdate
          and fd.odepartment <> fd.department
        order by fd.operdate
        into :odepartment;
      if (odepartment <> :department or :fdepartment is null) then
        dodaj = 0;
    end
    /* sprawdzenie pierwszej zmiany pomieszczenia po dacie inwentaryzacji */
    if (dodaj = 1 and comparment <> '') then begin
      ocomparment = :fcomparment;
      select first 1 fd.ocompartment from fareldocs fd
        where fd.fxdasset = :ref
          and fd.operdate > :invdate
          and fd.ocompartment <> fd.compartment
        order by fd.operdate
        into :ocomparment;
      if (ocomparment <> :comparment or fcomparment is null) then
        dodaj = 0;
    end
    if (dodaj = 1) then begin
      /* tu jeszcze liczenie z dokumentow i umorzenia */
      select sum(fvalue), sum(fredemption)
        from valdocs
        where fxdasset=:ref and regdate<=:invdate
        into :fvaldoc, :fredemptdoc;

      if (fvaldoc is not null) then
        fvalue = fvalue + fvaldoc;
      if (fredemptdoc is not null) then
        fredemption = fredemption + fredemptdoc;

      select sum(famort)
        from amortization
        where fxdasset = :ref and amyear*12+ammonth < extract(year from :invdate)*12+extract(month from :invdate)
        into :famort;

      if (famort is not null) then
        fredemption = fredemption + famort;

      fnetval = fvalue - fredemption;

      insert into fainvpos
        (fainvhdr, symbol, name, department, purchasedt, favalue, fnetval, amgrp, fxdasset, branch, comparment)
      values
        (:fainvhdr, :symbol, :name, :fdepartment, :purchasedt, :fvalue, :fnetval, :amgrp, :ref, :fbranch, :fcomparment);
    end
  end
end^
SET TERM ; ^
