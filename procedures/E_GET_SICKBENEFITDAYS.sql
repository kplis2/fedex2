--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_SICKBENEFITDAYS(
      EABSENCE integer,
      SICKECOL_LIST varchar(255) CHARACTER SET UTF8                           ,
      CHECKTOCURRENT smallint = 0,
      EABSCARD smallint = 0)
  returns (
      SICKBENEFITDAYS integer)
   as
declare variable FROMDATE_TEMP date;
declare variable TODATE_TEMP date;
declare variable LOOP_FROM smallint;
declare variable LOOP_TO smallint;
declare variable AFROMDATE date;
declare variable ATODATE date;
declare variable EMPLOYEE integer;
declare variable ECOLUMN integer;
declare variable SCODE integer;
declare variable SCODE_TEMP integer;
declare variable DAYS integer;
declare variable CORRECTION smallint;
begin
/* MWr Personel: Procedura zwraca liczbe dni okresu zasilkowego dla zadanej
   nieboecnosci (EABSENCE) i listy numerow ecolumns SICKECOL_LIST, ktore definiuja
   okres zasilkowy. Numery podajemy jako liste odzielona srednikami, koniecznie z
   srednikiem na poczatku i koncu listy np: ';nr1;nr2;nr3;'.
   EABSCARD - okresla, czy procedura ma zwracac wyniki dla kartoteki nieobecnosci
              uwzgledniajac correction = 1 (PR39696)
   CHECKTOCURRENT - sprawdzenie odbywa sie tylko do zadanej nieob. wlaczenie (BS44480) */

  sickbenefitdays = 0;
  CHECKTOCURRENT = coalesce(CHECKTOCURRENT,0);
  EABSCARD = coalesce(EABSCARD,0);

  select fromdate, todate, employee, ecolumn, scode, days, correction
    from eabsences
    where ref = :eabsence
    into :afromdate, :atodate, :employee, :ecolumn, :scode, :days, :correction;

  if ((correction in (0,2) or eabscard = 1) and sickecol_list like '%;' || ecolumn || ';%') then
  begin
    loop_from = 1;
    loop_to = iif(CHECKTOCURRENT = 1, 0, 1);
    sickbenefitdays = days;
    while (loop_from + loop_to > 0) do
    begin
      if (loop_from > 0) then
      begin
        fromdate_temp = null;
        scode_temp = null;
        select first 1 fromdate, scode, days
          from eabsences
          where :sickecol_list like '%;' || ecolumn || ';%'
            and employee = :employee
            and todate <= (:afromdate - 1)
            and todate >= (:afromdate - 61)
            and correction in (0,2)
            and (:correction <> 1 or corrected <> :eabsence)
          order by fromdate desc
          into :fromdate_temp, :scode_temp, :days;
        if (fromdate_temp is null or scode_temp <> scode ) then
          loop_from = 0;
        else begin
          afromdate = fromdate_temp;
          sickbenefitdays = sickbenefitdays + days;
        end
      end
      if (loop_to > 0) then
      begin
        todate_temp = null;
        scode_temp = null;
        select first 1 todate, scode, days
          from eabsences
          where :sickecol_list like '%;' || ecolumn || ';%'
            and employee = :employee
            and fromdate >= (:atodate + 1)
            and fromdate <= (:atodate + 61)
            and correction in (0,2)
            and (:correction <> 1 or corrected <> :eabsence)
          order by todate asc
          into :todate_temp, :scode_temp, :days;
        if (todate_temp is null or scode_temp <> scode) then
          loop_to = 0;
        else begin
          atodate = todate_temp;
          sickbenefitdays = sickbenefitdays + days;
        end
      end
    end
  end

  suspend;
end^
SET TERM ; ^
