--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KOPIUJ_WERSJE(
      SOURCE_REF integer,
      DEST_REF integer)
   as
declare variable KTM varchar(40);
declare variable NRWERSJI integer;
declare variable CECHA varchar(255);
declare variable WARTOSC varchar(255);
declare variable PWARTOSC varchar(255);
declare variable KOD varchar(20);
declare variable CNT integer;
declare variable WART_OLD varchar(255);
declare variable CENANET numeric(14,4);
declare variable CENABRU numeric(14,4);
declare variable CENAPNET numeric(14,4);
declare variable CENAPBRU numeric(14,4);
declare variable CENNIK integer;
declare variable STALACENA smallint;
begin
  select KTM, NRWERSJI from WERSJE where REF=:DEST_REF into :ktm, :nrwersji;
  if(:ktm is null or (:nrwersji is null))then exception OPER_ERROR;
/*  select count(*) from ATRYBUTY where WERSJAREF=:SOURCE_REF into :cnt;*/
 if(:source_ref = 0) then exception OPER_ERROR;
   for select CECHA, WARTOSC, PWARTOSC, KOD
    from ATRYBUTY where WERSJAREF=:SOURCE_REF
    into :cecha, :wartosc, :pwartosc, :kod
  do begin
    wart_old = null;
    select wartosc from ATRYBUTY where WERSJAREF=:dest_ref AND CECHA = :cecha AND kod = :kod into :wart_old;     --dodano KOD
    cnt = 0;
    select count(*) from ATRYBUTY where WERSJAREF=:dest_ref AND CECHA = :cecha AND kod = :kod into :cnt;         --dodano KOD
    if(:cnt > 0) then begin
      if(:wart_old is null or (:wart_old = '')) then
        update ATRYBUTY set WARTOSC = :wartosc where WERSJAREF=:dest_ref AND CECHA = :cecha AND kod = :kod;      --dodano KOD
    end else
     insert into ATRYBUTY(WERSJAREF, CECHA, WARTOSC, PWARTOSC, KOD)
       values (:dest_ref,:cecha, :wartosc, :pwartosc, :kod);
  end
  /* dodanie cenników  */
  for select CENNIK,CENANET, CENABRU, CENAPNET, CENAPBRU, STALACENA
  from CENNIK
  where WERSJAREF = :SOURCE_REF
  into :cennik, :cenanet, :cenabru, :cenapnet, :cenapbru, :stalacena
  do begin
    cnt = null;
    select count(*) from CENNIK where CENNIK=:cennik and WERSJAREF = :dest_ref
    into :cnt;
    if(:cnt is null) then cnt = 0;
    if(:cnt = 0) then
      insert into CENNIK(CENNIK,WERSJAREF, CENANET, CENABRU, CENAPNET, CENAPBRU, STALACENA)
      values(:cennik, :dest_ref, :cenanet, :cenabru, :cenapnet, :cenapbru, :stalacena);
  end
/*
  insert into cennik(CENNIK, KTM, WERSJAREF, CENANET, CENABRU,CENAPNET, CENAPBRU)
    select CENNIK, :ktm, :DEST_REF, CENANET, CENABRU, CENAPNET, CENAPBRU
      from cennik  ORGCENNIK
      where WERSJAREF=:SOURCE_REF AND
      (select count(*) from CENNIK where ORGCENNIK.WERSJAREF = :DEST_REF AND ORGCENNIK.CENNIK = CENNIK) = 0 ;
  */
end^
SET TERM ; ^
