--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDULE_CALC_VERIFY(
      PRSCHEDULE integer)
   as
declare variable curtime timestamp;
declare variable schednum integer;
declare variable isoper smallint;
declare variable status smallint;
declare variable tstart timestamp;
declare variable tdepend timestamp;
declare variable tend timestamp;
declare variable machine integer;
declare variable verifiednum integer;
declare variable schedoper integer;
declare variable shoper integer;
declare variable worktime float;
declare variable workplace varchar(20);
declare variable prdepart varchar(20);
declare variable losttime float;
declare variable schedzam integer;
declare variable matready timestamp;
declare variable matstatus smallint;
declare variable operendtime timestamp;
declare variable prganttref bigint;
begin
  --analiza po kolei operacji tak
  isoper = 1;
  verifiednum = 1;
  select prdepart from prschedules where ref = :prschedule into :prdepart;
  while (:isoper = 1)
  do begin
    --wybor operacji do analizy
    schedoper = null;
    select first 1 g.ref, A.REF, g.materialreadytime, g.statusmat,
      g.timefrom, g.timeto, g.status, g.prmachine, g.worktime, g.prshoper
    from prgantt g
      left join PRSCHEDOPERS A on (g.prschedoper = a.ref)
      left join prschedoperdeps d on (d.depto = A.ref)
      left join prgantt B on (d.depfrom = B.prschedoper and B.verified = 0)
      left join prgantt M on (m.prschedule = g.prschedule and m.prmachine = g.prmachine and m.timefrom < g.timefrom and m.verified = 0)
    where G.PRSCHEDULE = :prschedule and A.VERIFIED = 0 and g.timefrom is not null --tylko operacje, ktore wczeniewj juz byly zharmongoramowane
      and B.ref is null--operacje, ktore nie maja niezweryfikowanych poprzednikow
      and M.ref is null--opreacje, ktore nie maja niezwyryfikowanych operacji poprzedzajacych na maszynie
    order by A.starttime, A.schedzamnum, A.GUIDENUM
    into prganttref, schedoper, matready, matstatus,
     tstart, tend, status, machine, worktime, shoper;
    if(schedoper is null) then
    begin
      --zakonczenei oepracji
      update prgantt set verified = 1 where verified = 0;--bo nie wszystkie musialy podejsc pod werfikacje, co zostaly zazanczone
      exit;
    end
    workplace = null;
    select workplace from prgantt where ref=:prganttref
      into :workplace;
    --okreslenie minimalnego czasu rozpoczecia
    tdepend = null;
    curtime = current_time;
    --operacje poprzedzające w przewodniku
    select max(g.timeto)
      from prgantt g
      where g.depfrom = :schedoper
      into :tdepend;
    if(:tdepend is null) then tdepend = :curtime;
    if(:tstart is null or :tstart < :tdepend )then begin
      tstart = tdepend;
    end
    --operacje poprzedzajace na maszynie
    if(:workplace is not null) then begin
      execute procedure prschedule_find_machine(:prganttref,:tstart, 0) returning_values :tstart, :tend, :machine;
    end else begin
      --wyrównanie czasu startu
      execute procedure prschedule_checkoperstarttime(:prganttref, :tstart) returning_values :tstart;
      --sprawdzenie, czy start nie wypad w czasie przerwy pracy - jesli tak, to na poczatek kolejnego czasu
      execute procedure PRGANTT_CHECKREALWORKTIME(:prganttref, :machine, :tstart, :worktime) returning_values :worktime;
    end
    update prgantt set timefrom = :tstart, timeto = :tend, PRMACHINE = :machine,
    verified  = :verifiednum, WORKTIME = :worktime
      where ref=:prganttref;
    verifiednum = :verifiednum + 1;
  end
  update prgantt set verified = 1 where verified = 0;--bo nie wszystkie musialy podejsc pod werfikacje, co zostaly zazanczone
end^
SET TERM ; ^
