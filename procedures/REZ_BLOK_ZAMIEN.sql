--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REZ_BLOK_ZAMIEN(
      POZZAM integer,
      POZZAMREZ integer,
      OPERATOR integer)
  returns (
      ILOSCZMIEN numeric(14,4))
   as
DECLARE VARIABLE ILOSC NUMERIC(14,4);
DECLARE VARIABLE ILOSCREZ NUMERIC(14,4);
DECLARE VARIABLE KTM VARCHAR(80);
DECLARE VARIABLE KTMREZ VARCHAR(80);
DECLARE VARIABLE MAG VARCHAR(3);
DECLARE VARIABLE MAGREZ VARCHAR(3);
DECLARE VARIABLE OPER INTEGER;
DECLARE VARIABLE OPERREZ INTEGER;
DECLARE VARIABLE ADMINISTRATOR SMALLINT;
declare variable aktuoddzial varchar(20);
declare variable rezoddzial varchar(10);
declare variable nagzamrez integer;
declare variable nagzamrezid varchar(255);
declare variable nagzamrezrej varchar(3);
declare variable nagzam integer;
declare variable nagzamid varchar(255);
declare variable nagzamrej varchar(3);
begin
  if(:pozzam = :pozzamrez) then
    exception rez_wrong 'To samo zamowienie źrodlowe i docelowe';
  if(:pozzam is null or :pozzam = 0 or :pozzamrez is null or pozzamrez = 0) then
    exception rez_wrong 'Przenoszenie blokad dozwolone tylko z poziomu pozycji zamówień';
  select p.ktm, p.magazyn, n.operator, n.id, n.ref, n.rejestr  from pozzam p join nagzam n on (p.zamowienie = n.ref)
  where p.ref = :pozzam into :ktm, :mag, :oper, :nagzamid, :nagzam, :nagzamrej;
  select p.ktm, p.magazyn, n.operator, n.ref, n.rejestr, n.id  from pozzam p join nagzam n on (p.zamowienie = n.ref)
  where p.ref = :pozzamrez into :ktmrez, :magrez, :operrez,  :nagzamrez, :nagzamrezrej, :nagzamrezid;
  select o.administrator from operator o where o.ref = :operator into :administrator;
  select oddzial from operator where ref = :oper into aktuoddzial;
  select oddzial from operator where ref  = :operrez into rezoddzial;
  if(aktuoddzial is null) then aktuoddzial = 'aktuoddzial';
  if(rezoddzial is null) then aktuoddzial = 'rezoddzial';
  if((:administrator is null or :administrator = 0) and (aktuoddzial <> rezoddzial)) then
    exception rez_wrong 'Brak uprawnień';
  if(:mag is null or :magrez is null or :mag <> :magrez) then
    exception rez_wrong 'Niezgodne magazyny na pozycjach zamówień';
/*ilosci do zablokowania na zamówieniu docelowym*/
  select max(s.ilosc) from stanyrez s where s.pozzam = :pozzam and s.status = 'Z' and s.zreal = 0 into :ilosc;
  if(:ilosc is null or :ilosc = 0) then exception REZ_WRONG 'Docelowa pozycja zamówienia ma już naniesione blokady.';
/*ilosci do sciagniecia blokady z zamówienia zrodlowego*/
  select max(s.ilosc) from stanyrez s where s.pozzam = :pozzamrez and s.status = 'B' and s.zreal = 0 into :iloscrez;
  if(:iloscrez is null or :iloscrez = 0) then exception REZ_WRONG pozzamrez||'Wybrana rezerwacja do przeniesienia nie jest zablokowana.';
  if(:iloscrez < :ilosc) then ilosczmien = :iloscrez;
  else ilosczmien = :ilosc;
/*zwalnianie/nanoszenie blokad z zamówienia zrodlowego*/
  execute procedure rez_free_blokada(:pozzamrez,:ilosczmien);
  execute procedure rez_set_blokada(:pozzam, :ilosczmien);
  insert into HISTZAM(ZAMOWIENIE, OPERATOR, DATA, ORG_REJ, OPIS,  NOBACK)
  values (:nagzamrez, :operator,current_timestamp(0), :nagzamrezrej, 'Przeniesienie blokad na zamówienie '||:nagzamid||' z towaru '||:ktmrez||' w ilości '||:ilosczmien,1);
  insert into HISTZAM(ZAMOWIENIE, OPERATOR, DATA, ORG_REJ, OPIS,  NOBACK)
  values (:nagzam, :operator,current_timestamp(0), :nagzamrej, 'Przeniesienie blokad  z zamówienia '||:nagzamrezid||' na towar '||:ktm||' w ilości '||:ilosczmien,1);
end^
SET TERM ; ^
