--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OFERTY_OBLICZ(
      OFER integer)
   as
DECLARE VARIABLE SUMWARTZAK NUMERIC(14,2);
DECLARE VARIABLE SUMWARTNET NUMERIC(14,2);
DECLARE VARIABLE SUMWARTBRU NUMERIC(14,2);
DECLARE VARIABLE SUMMARZA NUMERIC(14,2);
begin
  if(OFER is not null) then
   begin
     select sum(WARTZAK),sum(WARTNET),sum(WARTBRU),sum(MARZA) from OFERPOZ where OFERTA=:OFER
     into :sumwartzak,:sumwartnet,:sumwartbru,:summarza;
     if(:sumwartnet<>0) then
       begin
         update OFERTY set SUMWARTZAK=:sumwartzak,SUMWARTNET=:sumwartnet,
             SUMWARTBRU=:sumwartbru,SUMMARZA=:summarza,
             MARZAPR=cast((:summarza/:sumwartnet)*100 as numeric(14,2)),
             MARZAKOSZT = :summarza - KOSZTDOST,
             MARZAKOSZTPR = cast(((:summarza - KOSZTDOST)/:sumwartnet)*100 as numeric(14,2))
         where REF=:OFER;
       end
     else
       begin
         update OFERTY set SUMWARTZAK=:sumwartzak,SUMWARTNET=:sumwartnet,
             SUMWARTBRU=:sumwartbru,SUMMARZA=:summarza,MARZAPR=0,
             MARZAKOSZT = :summarza - KOSZTDOST, MARZAKOSZTPR = 0
         where REF=:OFER;
       end
   end
end^
SET TERM ; ^
