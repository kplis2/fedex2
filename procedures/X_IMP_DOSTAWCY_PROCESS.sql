--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_IMP_DOSTAWCY_PROCESS(
      REF_IMP INTEGER_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING1024_UTF8)
   as
declare variable REF INTEGER_ID;
declare variable DOSTAWCAREF INTEGER_ID;
declare variable DOSTAWCAID INTEGER_ID;
declare variable NAZWA SHORTNAME_ID;
declare variable NAZWAPELNA STRING255;
declare variable FIRMA SMALLINT_ID;
declare variable NIP NIP_ID;
declare variable KRAJID STRING20;
declare variable ULICA STRING255;
declare variable NRDOMU STRING10;
declare variable NRLOKALU STRING10;
declare variable KODPOCZTOWY STRING10;
declare variable MIASTO STRING255;
declare variable TELEFON STRING255;
declare variable UWAGI STRING1024;
declare variable AKTYWNY SMALLINT_ID;
declare variable UWAGIDOKUMENT STRING511;
declare variable EMAIL STRING255;
declare variable dostawca_int_id string40;
begin
-- procedura pobiera dane o dostawcach z tabeli przejsciowej i tworzy nowego odbiorce
    status = 1;
    msg = '';
    select DOSTAWCAID, NAZWA, NAZWAPELNA, FIRMA, NIP, KRAJID, ULICA, NRDOMU, NRLOKALU, KODPOCZTOWY, MIASTO, TELEFON,
           UWAGI, AKTYWNY, UWAGIDOKUMENT, EMAIL
        from X_IMP_DOSTAWCY
        where ref = :ref_imp
        into :DOSTAWCAID, :NAZWA, :NAZWAPELNA, :FIRMA, :NIP, :KRAJID, :ULICA, :NRDOMU, :NRLOKALU, :KODPOCZTOWY, :MIASTO,
         :TELEFON, :UWAGI, :AKTYWNY, :UWAGIDOKUMENT, :EMAIL;
    dostawca_int_id = cast (dostawcaid as string40);

    --sprawdzenie poprawnosci krajid
    krajid = upper(krajid);
    if (coalesce(krajid,'') = '') then
    begin
        status = 8;
        msg = 'brak id kraju';
    end else
    if (not exists (select first 1 * from countries c where c.symbol = :krajid ))
    then
    begin
        status = 8;
        msg = 'brak Kraju o kodzie alfa-2: '||coalesce(:krajid,'<brak>');
    end
    if (coalesce (nazwapelna,'') = '' ) then
        nazwapelna = nazwa;

    nazwa = substring(nazwa from 1 for 40);

    select ref from dostawcy o where o.int_id = :dostawca_int_id
        into :dostawcaref;
    if (status = 1) then
    begin
        if ( coalesce(dostawcaref, 0) = 0) then
        begin
            insert into DOSTAWCY
                (ID, FIRMA, NAZWA, NIP, ULICA, MIASTO, KODP, KRAJID,
                 TELEFON, EMAIL, UWAGI, AKTYWNY, COMPANY,
                 NRDOMU, NRLOKALU, INT_ID, INT_DATAOSTPRZETW, x_uwagi_dokument,
                 x_imp_ref)
            values
                (:NAZWA, :FIRMA, :NAZWAPELNA, :NIP, :ULICA, :MIASTO, :KODPOCZTOWY, :KRAJID,
                :TELEFON, :EMAIL, :UWAGI, :AKTYWNY, 1,
                :NRDOMU, :NRLOKALU, :dostawca_int_id, current_timestamp, :uwagidokument,
                :ref_imp);
        end
        else begin
           update DOSTAWCY
               set ID = :NAZWA,
                   FIRMA = :FIRMA,
                   NAZWA = :NAZWAPELNA,
                   NIP = :NIP,
                   ULICA = :ULICA,
                   MIASTO = :MIASTO,
                   KODP = :KODPOCZTOWY,
                   KRAJID = :KRAJID,
                   TELEFON = :TELEFON,
                   EMAIL = :EMAIL,
                   UWAGI = :UWAGI,
                   AKTYWNY = coalesce(:AKTYWNY,1),
                   REGDATE = current_timestamp,
                   COMPANY = 1,
                   NRDOMU = :NRDOMU,
                   NRLOKALU = :NRLOKALU,
                   INT_ID = :dostawca_int_id,
                   INT_DATAOSTPRZETW = current_timestamp,
                   X_UWAGI_DOKUMENT = :uwagidokument,
                   x_imp_ref = :ref_imp
               where (REF = :dostawcaref);
        end
    end
  suspend;
end^
SET TERM ; ^
