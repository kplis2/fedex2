--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_NALICZ_PLANS_SPRZWART_ZTIF(
      PLANS integer,
      PLANSCOL integer,
      UZUPELNIANIE smallint,
      AKTUALIZACJA smallint,
      PLANSROW integer)
  returns (
      VAL numeric(14,4))
   as
DECLARE VARIABLE KEY1 VARCHAR(20);
DECLARE VARIABLE TEMPODNI NUMERIC(14,4);
DECLARE VARIABLE TEMPOOKR NUMERIC(14,4);
DECLARE VARIABLE DATAPOCZ TIMESTAMP;
DECLARE VARIABLE DATAKON TIMESTAMP;
DECLARE VARIABLE CNT INTEGER;
declare variable company varchar(20);
begin
  if (:AKTUALIZACJA is null) then AKTUALIZACJA = 0;
  if (:uzupelnianie is null) then uzupelnianie = 0;
  select plans.company from plans where plans.ref = :PLANS into :company;
  select planscol.bdata, planscol.ldata from planscol where planscol.ref = :PLANSCOL
    into :datapocz, :datakon;
  select plansrow.KEY1 from plansrow
    where plansrow.plans = :PLANS and PLANSrow.ref = :PLANSROW into :key1;
  select sum(p.wartnetzl)
    from pozfak p
    left join nagfak n on (p.dokument = n.ref)
    left join klienci k on (n.klient = k.ref)
    where p.ktm = :key1 and k.company = :company and n.datasprz >= :datapocz
      and n.datasprz <= :datakon
  into :val;
  if (:val is null) then val = 0;
  suspend;
END^
SET TERM ; ^
