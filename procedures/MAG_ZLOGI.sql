--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_ZLOGI(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      DATASTAN timestamp,
      ILDNI integer,
      KTMFOR varchar(40) CHARACTER SET UTF8                           ,
      VERFOR integer)
  returns (
      MAG varchar(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      CENA numeric(14,4),
      DOSTAWA integer,
      STAN numeric(14,4),
      STANWART numeric(14,4),
      ZLOGI numeric(14,4),
      ZLOGIWART numeric(14,4))
   as
declare variable PRZECTYP varchar(3);
declare variable DATAZLOG timestamp;
declare variable ILROZ numeric(14,4);
declare variable WARTROZ numeric(14,4);
declare variable SQL varchar(1024);
begin
  execute procedure GETCONFIG('MAGPRZECTYP') returning_values :przectyp;
  datazlog = :datastan - :ildni;
  if (verfor = 0) then VERFOR =null;
  for select symbol from DEFMAGAZ where SYMBOL=:magazyn or (:magazyn is null) or (:magazyn = '')
  into :mag
  do begin
    for select RKTM, RWERSJA, RCENA, RDOSTAWA, STAN, WARTOSC
        from MAG_STANY(:mag,:KTMFOR,:VERFOR,NULL,NULL,:datastan,0,0)
    into :ktm, :wersja, :cena, :dostawa,:stan, :stanwart
    do begin
      if(:stan > 0) then begin
        zlogi = null;
        execute procedure MAG_STAN(:mag, :ktm, :wersja, :cena, :dostawa, :datazlog) returning_values :zlogi, :zlogiwart;
        if(:zlogi > 0) then begin
          ilroz = null;
          wartroz = null;
          SQL = 'select sum(DOKUMROZ.ILOSC), sum(DOKUMROZ.WARTOSC)
            from DOKUMROZ join DOKUMPOZ on (DOKUMROZ.pozycja = DOKUMPOZ.REF';
            if (cena is not null) then SQL = SQL || ' and DOKUMROZ.CENA = '||:cena;
            if (dostawa is not null) then SQL = SQL || ' and DOKUMROZ.DOSTAWA = '||:dostawa;
            SQL = SQL || ') join DOKUMNAG on (DOKUMNAG.REF = DOKUMPOZ.DOKUMENT and DOKUMNAG.magazyn = '''||:mag||''' and DOKUMNAG.AKCEPT = 1 and DOKUMNAG.data > '''
              ||:datazlog||''' and DOKUMNAG.DATA <= '''||:DATASTAN||''')
                        join DEFDOKUM on (DOKUMNAG.TYP  = DEFDOKUM.SYMBOL and DEFDOKUM.WYDANIA=1)
          where DOKUMROZ.KTM =''' ||:KTM ||''' and DOKUMROZ.WERSJA = '||:wersja;
          execute statement :SQL
          into :ilroz, :wartroz;
          if(:ilroz > 0) then begin
            zlogi = :zlogi - :ilroz;
            zlogiwart = :zlogiwart - :wartroz;
          end
          if(coalesce(:przectyp,'') != '') then begin
            ilroz = null;
            wartroz = null;
            select sum(DOKUMROZ.ILOSC), sum(DOKUMROZ.WARTOSC)
            from DOKUMROZ join DOKUMPOZ on (DOKUMROZ.pozycja = DOKUMPOZ.REF and(DOKUMROZ.CENA = :cena or (:cena is null)) and (DOKUMROZ.DOSTAWA = :dostawa or (:dostawa is null)))
                        join DOKUMNAG on (DOKUMNAG.REF = DOKUMPOZ.DOKUMENT and DOKUMNAG.magazyn = :mag and DOKUMNAG.AKCEPT = 1 and DOKUMNAG.TYP = :przectyp and DOKUMNAG.data > :datazlog and DOKUMNAG.DATA <= :DATASTAN)
                        join DEFDOKUM on (DOKUMNAG.TYP  = DEFDOKUM.SYMBOL and DEFDOKUM.WYDANIA=0)
            where DOKUMROZ.KTM = :KTM and DOKUMROZ.WERSJA = :wersja
            into :ilroz, :wartroz;
            if(:ilroz > 0) then begin
              zlogi = :zlogi + :ilroz;
              zlogiwart = :zlogiwart + :wartroz;
            end
          end
          if(:zlogi > 0 and :stan >= :zlogi and :stanwart >= :zlogiwart) then
            suspend;
        end
      end
      stan = null;
    end
  end
end^
SET TERM ; ^
