--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_WYDHIST_DATA(
      MAGAZYNY varchar(255) CHARACTER SET UTF8                           ,
      MASKAKTM varchar(40) CHARACTER SET UTF8                           ,
      DATAOD date,
      DATADO date,
      ZEWN smallint)
  returns (
      DATA date,
      ILOSC numeric(14,4),
      ILOSCBRAK numeric(14,4),
      ILOSCINC numeric(14,4))
   as
declare variable prevdata date;
declare variable newdata date;
declare variable newilosc numeric(14,4);
declare variable newiloscbrak numeric(14,4);
declare variable newiloscinc numeric(14,4);
begin
  prevdata = :dataod - 1;
  for select g.data,
  sum(g.ilosc),sum(g.iloscbrak),sum(g.iloscinc)
  from get_wydhist(:magazyny,:maskaktm,:dataod,:datado,:zewn) g
  group by g.data
  order by g.data
  into :newdata, :newilosc, :newiloscbrak, :newiloscinc
  do begin
    while(:prevdata<:newdata-1) do begin
      prevdata = :prevdata + 1;
      data = :prevdata;
      ilosc = 0;
      iloscbrak =0;
      iloscinc = 0;
      suspend;
    end
    prevdata = :newdata;
    data = :prevdata;
    ilosc = :newilosc;
    iloscbrak = :newiloscbrak;
    iloscinc = :newiloscinc;
    suspend;
  end
  while(:prevdata<:datado) do begin
    prevdata = :prevdata + 1;
    data = :prevdata;
    ilosc = 0;
    iloscbrak =0;
    iloscinc = 0;
    suspend;
  end
end^
SET TERM ; ^
