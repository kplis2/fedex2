--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EPATT_DEFAULT(
      CALENDAR integer,
      PYEAR integer)
   as
declare variable d date;
declare variable workstart time;
declare variable workend time;
declare variable worktime integer;
declare variable daykind smallint;
declare variable day_of_week smallint;
declare variable patternyear integer;
begin
  --DS: dodawanie wzorca tygodniowego
  --jezeli istnieja juz zarejestrowane wzorce kopiuje z nich dane, inaczej wstawia prace w godz 8:00-16:00
  if (pyear is null or pyear<0) then
    exception BAD_PARAMETER;
  -- usuwam stary wzorzec
  delete from ECALPATTDAYS where calendar=:calendar and pyear=:pyear;

  --szukam pierwszego poniedzialku, nie liczac nowego roku
  d = (cast(pyear as varchar(4)) || '-01-02');
  while (extract(weekday from d)<>1) do
     d = d + 1;

  if (exists(select p.ref
    from ecalpattdays p
    where p.calendar = :calendar
      and p.pyear = :pyear - 1)) then
    patternyear = pyear - 1;
  --poki co z interfejsu nie mozna dodac lat wstecz, ale czesto klientom takie operacje sie robi,
  --aby mogli rejestrowac nieobecnosci archiwalne
  else if (exists(select p.ref
    from ecalpattdays p
    where p.calendar = :calendar
      and p.pyear = :pyear + 1)) then
    patternyear = pyear + 1;

  -- po wszystkich dniach tygodnia, oprócz niedzieli
  day_of_week = 1;
  while (day_of_week>0)
  do begin
    day_of_week = extract(weekday from d);
    if (patternyear is not null) then
    begin
      select p.workstart, p.workend, p.daykind
        from ecalpattdays p
        where p.calendar = :calendar
          and p.pyear = :patternyear
          and p.dayofweek = :day_of_week
          and p.pattkind = 1
        into :workstart, :workend, :daykind;
    end else
    begin
      --jezeli nie ma wczesniejszych kalendarzy, dodaj standard
      if (day_of_week=0) then
      begin
        DAYKIND = 0; -- Niedziela to swieto
        WORKSTART =  '00:00:00';
        WORKEND = '00:00:00';
        WORKTIME = 0;
      end else if (day_of_week=6) then
      begin
        DAYKIND = 2; -- sobota to dzien wolny
        WORKSTART =  '00:00:00';
        WORKEND = '00:00:00';
        WORKTIME = 0;
      end else
      begin
        DAYKIND = 1; -- pozostale dni sa robocze
        WORKSTART = '08:00:00';
        WORKEND = '16:00:00';
        WORKTIME = WORKEND - WORKSTART; /* ilosc sekund*/
      end
    end

    insert into ECALPATTDAYS (CALENDAR,PDATE , DAYKIND, WORKSTART, WORKEND, WORKTIME, DESCRIPT, PATTKIND)
      values (:calendar, :d, :DAYKIND, :WORKSTART, :WORKEND, :WORKTIME, '', 1);
    d = d + 1; -- kolejny dzień
  end
  --dodaj swieta
  execute procedure add_holidays(calendar, pyear);
end^
SET TERM ; ^
