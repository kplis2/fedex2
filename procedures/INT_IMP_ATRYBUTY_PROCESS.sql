--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_ATRYBUTY_PROCESS(
      SESJAREF SESJE_ID,
      TABELA TABLE_ID,
      REF INTEGER_ID,
      BLOKUJZALEZNE SMALLINT_ID = 0,
      TYLKONIEPRZETWORZONE SMALLINT_ID = 0,
      AKTUALIZUJSESJE SMALLINT_ID = 1)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable STABELA TABLE_ID;
declare variable TMPTABELA TABLE_ID;
declare variable SPROCEDURA STRING255;
declare variable SZRODLO ZRODLA_ID;
declare variable SKIERUNEK SMALLINT_ID;
declare variable SESJA SESJE_ID;
declare variable SQL MEMO;
declare variable SQL2 MEMO;
declare variable TMPSQL MEMO;
declare variable TMPSTATUS SMALLINT_ID;
declare variable TMPMSG STRING255;
declare variable ERROR SMALLINT_ID;
declare variable ERRORMSG STRING255;
declare variable ERROR2 SMALLINT_ID;
declare variable TMPERROR SMALLINT_ID;
declare variable ERRORMSG2 STRING255;
declare variable STATUSPOZ SMALLINT_ID;
declare variable A_REF INTEGER_ID;
declare variable A_TABELAID STRING120;
declare variable A_OBIEKTID STRING120;
declare variable A_ATRYBUT STRING255;
declare variable A_ATRYBUTID STRING120;
declare variable A_WARTOSC STRING255;
declare variable A_WARTOSCID STRING120;
declare variable A_HASH STRING255;
declare variable A_DEL INTEGER_ID;
declare variable A_REC STRING255;
declare variable A_SKADTABELA STRING40;
declare variable A_SKADREF INTEGER_ID;
declare variable J_TMP_PRZELICZ NUMERIC_14_4;
declare variable J_GLOWNASENTE SMALLINT_ID;
declare variable ODDZIAL ODDZIAL_ID;
declare variable COMPANY COMPANIES_ID;
declare variable DEFCECHY CECHA_ID;
declare variable DEFCECHW STRING255;
declare variable OTABLE TABLE_ID;
declare variable OREF INTEGER_ID;
declare variable ZALOZATRYBUT SMALLINT_ID;
declare variable SLOWNIKWYMUSZONY SMALLINT_ID;
declare variable TMP_ATRYBUT STRING255;
declare variable XATRYBUT INTEGER_ID;
declare variable WYDANIA INTEGER_ID;
declare variable DATAOSTPRZETW TIMESTAMP_ID;
declare variable X_ATRYBUT INTEGER_ID;
declare variable O_COMPANY COMPANIES_ID;
declare variable O_ODDZIAL ODDZIAL_ID;
declare variable O_OTABLE TABLE_ID;
declare variable O_OREF INTEGER_ID;
declare variable O_CECHA CECHA_ID;
declare variable O_REFDEFCECHW INTEGER_ID;
declare variable O_WARTOSC STRING;
declare variable O_I_WARTOSC INTEGER_ID;
declare variable O_D_WARTOSC DATE_ID;
declare variable O_N_WARTOSC NUMERIC_14_2;
declare variable O_REGDATE TIMESTAMP_ID;
declare variable O_REGOPER INTEGER_ID;
declare variable O_CHANGEDATE TIMESTAMP_ID;
declare variable O_CHANGEOPER OPERATOR_ID;
declare variable O_INT_ZRODLO ZRODLA_ID;
declare variable O_INT_ID STRING120;
declare variable O_KTM KTM_ID;
declare variable O_NAZWACECHY STRING40;
declare variable ktm ktm_id;
declare variable tmpktm ktm_id;
declare variable ktm_in_id string120;
declare variable wersjaref integer_id;
declare variable nagkpl kplnag_id;
declare variable kplwersja wersje_id;
declare variable kplktm ktm_id;
begin
  --zmiana pustych wartosci na null-e
  if (sesjaref = 0) then
    sesjaref = null;
  if (trim(tabela) = '') then
    tabela = null;
  if (ref = 0) then
    ref = null;
  if (blokujzalezne is null) then
    blokujzalezne = 0;
  if (tylkonieprzetworzone is null) then
    tylkonieprzetworzone = 0;
  if (aktualizujsesje is null) then
    aktualizujsesje =1;

  status = 1;

  --sprawdzenie czy wszystkie potrzebne informacje sa
  if (sesjaref is null and ref is null) then
    begin
      status = -1;
      msg = 'Za malo parametrow.';
      exit; --EXIT
    end

  if (sesjaref is null) then
  begin
    if (ref is null) then
      begin
        status = -1;
        msg = 'Jesli nie podales sesji to wypadaloby podac ref-a...';
        exit; --EXIT
      end
    if (tabela is null) then
      begin
        status = -1;
        msg = 'Wypadaloby podac nazwe tabeli...';
        exit; --EXIT
      end


    sql = 'select sesja from '||:tabela||' where ref='||:ref;
    execute statement sql into :sesjaref;

  end
  else
    sesja = sesjaref;

  if (sesjaref is null) then
    begin
      status = -1;
      msg = 'Nie znaleziono numeru sesji.';
      exit; --EXIT
    end

  --pobranie parametrow sesji
  select s.tabela, s.procedura, s.zrodlo, s.kierunek
    from int_sesje s
    where ref = :sesja
    into :stabela, :sprocedura, :szrodlo, :skierunek;

  if (tabela is not null) then --jesli podano tabele na wejsciu traktuje ja nadrzednie
    stabela = tabela;

  oddzial = null;
  company = null;
  select oddzial, company from int_zrodla where ref = :szrodlo
    into :oddzial, :company;
  if (oddzial is null) then
    oddzial = 'CENTRALA';
  if (company is null) then
    company = 1;

  -- [DG] XXX eksperymentalne

  sql = 'select ref, tabelaid, obiektid, atrybut, atrybutid, wartosc, wartoscid,
        skadtabela, skadref,
        --hash,
        del, rec
      from INT_IMP_ATRYBUTY k
      where 1 = 1';
    
  if (:sesja is not null ) then
  begin
    sql = sql || ' and k.sesja = ' || :sesja;
  end
  if (ref is not null and tabela is null) then
  begin
    sql = sql || ' and k.ref = ' || :ref;
  end
  else if (ref is not null and tabela is not null) then
  begin
    sql = sql || ' and k.skadref = ' || :ref || ' and k.skadtabela = ''' || :tabela||'''';
  end

  --wlasciwe przetworzenie
/*  for select ref, tabelaid, obiektid, atrybut, atrybutid, wartosc, wartoscid,
        skadtabela, skadref,
        --hash,
        del, rec
      from INT_IMP_ATRYBUTY k
      where (k.sesja = :sesja or :sesja is null)
        and ((:ref is not null and k.ref = :ref and :tabela is null)
        or (:ref is not null and :tabela is not null and k.skadref = :ref and k.skadtabela = :tabela)
        or (:ref is null))
      order by ref */
    for
      execute statement sql
      into :a_ref, :a_tabelaid, :a_obiektid, :a_atrybut, :a_atrybutid, :a_wartosc, :a_wartoscid,
        :a_skadtabela, :a_skadref,
        --:o_hash,
        :a_del, :a_rec
    do begin
      error = 0;
      errormsg = '';
      error2 = 0;
      errormsg2 = '';
      tmperror = null;

      defcechy = null;
      defcechw = null;
      otable = null;
      oref = null;
      slownikwymuszony = null;
      xatrybut = null;

      zalozatrybut = 0; --[PM] !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! JO

      if (coalesce(trim(a_skadtabela),'') = '' and a_tabelaid not in ('TOWARY','KLIENCI','DOSTAWCY','ODBIORCY')) then     -- XXX[KBI]
        begin
          error = 1;
          errormsg = errormsg || 'Brak tabeli zrodlowej.';
        end

      if (coalesce(trim(a_atrybut),'') = '') then
        begin
          error = 1;
          errormsg = errormsg || 'Brak wskazania atrybutu.';
        end

      if (a_skadtabela = 'INT_IMP_TOW_TOWARY' or a_tabelaid = 'TOWARY') then  -- XXX[KBI]  
        begin
          otable = 'WERSJE';
          if (coalesce(a_skadref,0) > 0) then
            begin
              select wersjaref from INT_IMP_TOW_TOWARY where ref = :a_skadref
                into :oref;
            end
          if (oref is null and coalesce(trim(a_obiektid),'') <> '') then
            begin
              select w.ref from towary t left join wersje w on t.ktm = w.ktm
                where t.int_zrodlo = :szrodlo and t.int_id = :a_obiektid
                into :oref;
            end
        end
      else if (a_skadtabela = 'INT_IMP_KLI_KLIENCI' or a_tabelaid = 'KLIENCI') then  -- XXX[KBI] 
        begin
          otable = 'KLIENCI';
          if (coalesce(a_skadref,0) > 0) then
            begin
              select klient from INT_IMP_KLI_KLIENCI where ref = :a_skadref
                into :oref;
            end
          if (oref is null and coalesce(trim(a_obiektid),'') <> '') then
            begin
              select ref from klienci k
                where k.int_zrodlo = :szrodlo and k.int_id = :a_obiektid
                into :oref;
            end
        end
      else if (a_skadtabela = 'INT_IMP_ZAM_NAGLOWKI') then
        begin
          if (coalesce(a_skadref,0) > 0) then
            begin
              select
                case wydania
                  when 0 then zamowienie
                  when 1 then dokument
                  else dokument
                end,
                case wydania
                  when 0 then 'NAGZAM'
                  when 1 then 'DOKUMNAG'
                  else 'DOKUMNAG'
                end
                from INT_IMP_ZAM_NAGLOWKI where ref = :a_skadref
                into :oref, :otable;
            end
          if (oref is null and coalesce(trim(a_obiektid),'') <> '') then
            begin
              select ref from nagzam n
                where n.int_zrodlo = :szrodlo and n.int_id = :a_obiektid
                into :oref;
            end
        end
      else if (a_skadtabela = 'INT_IMP_DOS_DOSTAWCY' or a_tabelaid = 'DOSTAWCY') then  -- XXX[KBI] 
        begin
          otable = 'DOSTAWCY';
          if (coalesce(a_skadref,0) > 0) then
            begin
              select dostawca from INT_IMP_DOS_DOSTAWCY where ref = :a_skadref
                into :oref;
            end
          if (oref is null and coalesce(trim(a_obiektid),'') <> '') then
            begin
              select ref from dostawcy d
                where d.int_zrodlo = :szrodlo and d.int_id = :a_obiektid
                into :oref;
            end
        end
      else if (a_skadtabela = 'INT_IMP_KLI_ODBIORCY' or a_tabelaid = 'ODBIORCY') then  -- XXX[KBI] 
        begin
          otable = 'ODBIORCY';
          if (coalesce(a_skadref,0) > 0) then
            begin
              select odbiorca from INT_IMP_KLI_ODBIORCY where ref = :a_skadref
                into :oref;
            end
          if (oref is null and coalesce(trim(a_obiektid),'') <> '') then
            begin
              select ref from odbiorcy o
                where o.int_zrodlo = :szrodlo and o.int_id = :a_obiektid
                into :oref;
            end
        end
      -- XXX[KBI]
      else if (a_skadtabela = 'INT_IMP_ZAM_POZYCJE' or a_tabelaid = 'DOKUMPOZ') then
        begin
          otable = 'DOKUMPOZ';
          if (coalesce(a_skadref,0) > 0) then
            begin
              select pozycjadok from INT_IMP_ZAM_POZYCJE where ref = :a_skadref
                into :oref;
            end
          if (oref is null and coalesce(trim(a_obiektid),'') <> '') then
            begin
              select ref from dokumpoz p
                where p.int_zrodlo = :szrodlo and p.int_id = :a_obiektid
                into :oref;
            end
          -- aktualizacja wpisów w pozycji dokumetu
          if (oref is not null) then begin
            -- Okreslenie iwestycji
            if (a_atrybut = 'INWESTYCJE') then begin
              -- sprawdzenie czy wartosc atrybutu jest intigerem
              execute procedure CHECK_STRING_TO_INTEGER(:a_wartosc) returning_values :tmpstatus;
              if (tmpstatus <> 1) then begin
                error = 1;
                errormsg = errormsg || 'Niepoprawny format atrybutu '||coalesce(:a_atrybut,'<null>')||' o wartości '||:a_wartosc;
              end else if (not exists(select first 1 1 from x_slowniki_ehrle xe where xe.ref = :a_wartosc and xe.typ = 3)) then begin
                error = 1;
                errormsg = errormsg || 'Nie znaleziono słownika inwestycji dla atrybutu '||coalesce(:a_atrybut,'<null>')||' o wartości '||:a_wartosc;
              end else begin
                -- aktualizacja dokum poza po numer inwestycji
                --<< XXX MKD
                select first 1 dp.ktm  from dokumpoz dp where dp.ref = :oref
                    into :tmpktm;
                select first 1 kn.wersjaref
                FROM MWSSTOCK
                    JOIN MWSPALLOCS AA1 ON ( MWSSTOCK.MWSPALLOC = AA1.REF )
                    LEFT JOIN MWSCONSTLOCS AA0 ON ( MWSSTOCK.MWSCONSTLOC = AA0.REF )
                    LEFT JOIN TOWARY AA2 ON ( MWSSTOCK.GOOD = AA2.KTM )
                    LEFT JOIN X_MWS_SERIE AA3 ON ( MWSSTOCK.X_SERIAL_NO = AA3.REF )
                    LEFT JOIN kplpoz kp ON (kp.wersjaref = MWSSTOCK.VERS)
                    LEFT JOIN kplnag kn ON (kp.nagkpl = kn.ref)
                    WHERE MWSSTOCK.WH = 'MWS' and MWSSTOCK.x_slownik = :a_wartosc
                            and mwsstock.quantity > 0 and  kn.ktm = :tmpktm
                into :kplwersja;

                if (kplwersja is not null) then begin
                    update dokumpoz dp set  dp.wersjaref = :kplwersja where dp.ref = :oref;
                    update dokumpoz dp set  havefake = 1 , dp.x_slownik = :a_wartosc where dp.ref = :oref;
                end
                else begin
                -->> XXX MKD
                   update dokumpoz dp set dp.x_slownik = :a_wartosc where dp.ref = :oref;
                --<< XXX MKD
                end
                -->> XXX MKD
              end
            end else if (lower(a_atrybut) = 'program') then begin
              -- w przypadku programów musimy na poczatku sprawdzić czy mamy polaczona wersje z podanym programem komuterowym
              -- i czy wersja nalezy do wlasciwego KTMu
              select dp.ktm from dokumpoz dp where dp.ref = :oref into :ktm;
              select t.int_id from towary t where t.ktm = :ktm into :ktm_in_id;

              select first 1 xe.oref
                from x_slowniki_ehrle xe
                where xe.id_ses = :a_wartosc
                  and xe.typ = 1
                  and xe.typ_id = :ktm_in_id
               into :wersjaref;

               if (wersjaref is null) then begin
                error = 1;
                errormsg = errormsg || 'Atrybut '||coalesce(:a_atrybut,'<null>')||' o wartości '||:a_wartosc||' nie jest powiązany ze słownikiem programów';
              end else if (not exists(select first 1 1 from wersje w where w.ref = :wersjaref and w.ktm = :ktm)) then begin
                -- sprawdzamy czy ktm i werrsja stanowia parę
                error = 1;
                errormsg = errormsg || 'Atrybut '||coalesce(:a_atrybut,'<null>')||' o wartości '||:a_wartosc||' nie jest powiązany towarem na pozycji';
              end else begin
                update dokumpoz dp set dp.wersjaref = :wersjaref where dp.ref = :oref;
              end
            --<< XXX MKD
            end else if (lower(a_atrybut) = 'fromprogram') then begin
              -- w przypadku programów musimy na poczatku sprawdzić czy mamy polaczona wersje z podanym programem komuterowym
              -- i czy wersja nalezy do wlasciwego KTMu
              select dp.ktm from dokumpoz dp where dp.ref = :oref into :ktm;
              select t.int_id from towary t where t.ktm = :ktm into :ktm_in_id;

              select first 1 xe.oref
                from x_slowniki_ehrle xe
                where xe.id_ses = :a_wartosc
                  and xe.typ = 1
                  and xe.typ_id = :ktm_in_id
               into :wersjaref;

               if (wersjaref is null) then begin
                error = 1;
                errormsg = errormsg || 'Atrybut '||coalesce(:a_atrybut,'<null>')||' o wartości '||:a_wartosc||' nie jest powiązany ze słownikiem programów';
              end else if (not exists(select first 1 1 from wersje w where w.ref = :wersjaref and w.ktm = :ktm)) then begin
                -- sprawdzamy czy ktm i wersja stanowia parę
                error = 1;
                errormsg = errormsg || 'Atrybut '||coalesce(:a_atrybut,'<null>')||' o wartości '||:a_wartosc||' nie jest powiązany towarem na pozycji';
              end else begin
                update dokumpoz dp set dp.x_fromprogram = :wersjaref where dp.ref = :oref;
              end
            -->> XXX MKD
            end else begin
              error = 1;
              errormsg = errormsg || 'Atrybut '||coalesce(:a_atrybut,'<null>')||' jest nie obsługiwany';
            end

          end
        end
        -- XXX Koniec KBI

      if (oref is null) then
        begin
          error = 1;
          errormsg = errormsg || 'Nie znaleziono REF-a z tabeli '||coalesce(:otable,'<null>');
        end

      if (coalesce(a_del,0) = 1 and error = 0) then --rekord do skasowania
        begin
          --select status, msg from INT_IMP_ATRYBUTY_DEL()
            --into :tmpstatus, :tmpmsg;
          tmpstatus = null;
        end
      else if (error = 0 and coalesce(a_del,0) = 0) then
      begin

        --nullowanie, zerowanie i blankowanie
        if (coalesce(trim(a_atrybut),'') = '') then
          a_atrybut = null;
        if (coalesce(trim(a_atrybutid),'') = '') then
          a_atrybutid = null;
        if (coalesce(trim(a_wartoscid),'') = '') then
          a_wartoscid = null;
    
        if (:zalozatrybut = 1) then
        begin
          --sprawdzam tabele DEFCECHY
          if (a_atrybut is not null and a_atrybutid is not null) then
          begin
            select dc.symbol, coalesce(slow,0)
              from defcechy dc where dc.symbol = :a_atrybut
                and dc.int_zrodlo = :szrodlo and dc.int_id = :a_atrybutid
              into :defcechy, :slownikwymuszony;
            if (defcechy is null) then
              select dc.symbol, coalesce(slow,0)
                from defcechy dc where dc.symbol = :a_atrybut
                  and dc.int_id = :a_atrybutid
                into :defcechy, :slownikwymuszony;
            if (defcechy is null) then
              select dc.symbol, coalesce(slow,0)
                from defcechy dc where dc.nazwa = :a_atrybut
                  and dc.int_zrodlo = :szrodlo and dc.int_id = :a_atrybutid
                into :defcechy, :slownikwymuszony;
            if (defcechy is null) then
              select dc.symbol, coalesce(slow,0)
                from defcechy dc where dc.nazwa = :a_atrybut
                  and dc.int_id = :a_atrybutid
                into :defcechy, :slownikwymuszony;
          end
          if (defcechy is null and a_atrybut is not null) then
          begin
            select dc.symbol, coalesce(slow,0)
              from defcechy dc where dc.symbol = :a_atrybut
                and dc.int_zrodlo = :szrodlo
              into :defcechy, :slownikwymuszony;
            if (defcechy is null) then
              select dc.symbol, coalesce(slow,0)
                from defcechy dc where dc.symbol = :a_atrybut
                into :defcechy, :slownikwymuszony;
            if (defcechy is null) then
              select dc.symbol, coalesce(slow,0)
                from defcechy dc where dc.nazwa = :a_atrybut
                  and dc.int_zrodlo = :szrodlo
                into :defcechy, :slownikwymuszony;
            if (defcechy is null) then
              select dc.symbol, coalesce(slow,0)
                from defcechy dc where dc.nazwa = :a_atrybut
                into :defcechy, :slownikwymuszony;
          end
          if (defcechy is null and a_atrybutid is not null) then
          begin
            select dc.symbol, coalesce(slow,0)
              from defcechy dc where dc.int_zrodlo = :szrodlo
                and dc.int_id = :a_atrybutid
              into :defcechy, :slownikwymuszony;
            if (defcechy is null) then
              select dc.symbol, coalesce(slow,0)
                from defcechy dc where dc.int_id = :a_atrybutid
                into :defcechy, :slownikwymuszony;
          end
    
          if (a_atrybut is not null and defcechy is null and zalozatrybut = 1) then --insert do defcechy
          begin
            /* JO - na razie zostawiam do przemyslenia bo teraz do symbolu leci
            tmp_atrybut = :a_atrybutid;
            if (char_length(tmp_atrybut) > 20) then
              tmp_atrybut = replace(tmp_atrybut, '', '');
            if (char_length(tmp_atrybut) > 20) then
              tmp_atrybut = substring(tmp_atrybut from 1 for 20);
            */
    
            insert into defcechy (symbol, nazwa, rodzaj, tabela, pole, wersje, typ, wyszuk, wymslow, slow, grupa, state, tworzyktm, filtr, redagowalne,
                wartpocz, wartpusta, warunekatr, dlugosc, ktmuseopis, lp, ord, flagi, nazwagrupy, redundantakr, token, autoatrybut,
                parsymbol, parskrot, parnazwa, partyp, parredzamspr, parredzamzak, parredfakspr, parredfakzak, parreddokwyd, parreddokprz, parwartdom,
                pardictbase, pardicttable, pardictfield, parkeyfields, pardictindex, pardictprefix, pardictfilter, tworzywersje, filteratr, tworzynazwe,
                towtypes, maska, znakidostepne, int_zrodlo, int_id)
              values (:a_atrybutid, :a_atrybut, 2, 'ATRYBUTY', 'WARTOSC', 0, 0, 0, 0, 0, null, 0, 0, '', 0,
                '', '', '', 0, 0, null, 1, '', null, '', 9, 0,
                '', '', '', 0, 0, 0, 0, 0, 0, 0, '',
                '', '', '', '', '', '', '', 0, '', 0,
                '', '', '', :szrodlo, :a_atrybutid)
              returning symbol, slow into :defcechy, :slownikwymuszony;
          end
    
          --sprawdzam tabele DEFCECHW
          if (slownikwymuszony = 1 and defcechy is not null) then
          begin
            if (a_wartosc is not null and a_wartoscid is not null) then
              begin
                select dc.id_defcechw
                  from defcechw dc where dc.cecha = :defcechy
                    and dc.wartstr = :a_wartosc
                    and dc.int_zrodlo = :szrodlo and dc.int_id = :a_wartoscid
                  into :defcechw;
                if (defcechw is null) then
                  select dc.id_defcechw
                    from defcechw dc where dc.cecha = :defcechy
                      and dc.wartstr = :a_wartosc
                      and dc.int_id = :a_wartoscid
                    into :defcechw;
                if (defcechw is null) then
                  select dc.id_defcechw
                    from defcechw dc where dc.cecha = :defcechy
                      and dc.opis = :a_wartosc
                      and dc.int_zrodlo = :szrodlo and dc.int_id = :a_wartoscid
                    into :defcechw;
                if (defcechw is null) then
                  select dc.id_defcechw
                    from defcechw dc where dc.cecha = :defcechy
                      and dc.opis = :a_wartosc
                      and dc.int_id = :a_wartoscid
                    into :defcechw;
              end
            if (defcechw is null and a_wartosc is not null) then
              begin
                select dc.id_defcechw
                  from defcechw dc where dc.cecha = :defcechy
                    and dc.wartstr = :a_wartosc
                    and dc.int_zrodlo = :szrodlo
                  into :defcechw;
                if (defcechw is null) then
                  select dc.id_defcechw
                    from defcechw dc where dc.cecha = :defcechy
                      and dc.wartstr = :a_wartosc
                    into :defcechw;
                if (defcechw is null) then
                  select dc.id_defcechw
                    from defcechw dc where dc.cecha = :defcechy
                      and dc.opis = :a_wartosc
                      and dc.int_zrodlo = :szrodlo
                    into :defcechw;
                if (defcechw is null) then
                  select dc.id_defcechw
                    from defcechw dc where dc.cecha = :defcechy
                      and dc.opis = :a_wartosc
                    into :defcechw;
              end
            if (defcechw is null and a_atrybutid is not null) then
              begin
                select dc.id_defcechw
                  from defcechw dc where dc.cecha = :defcechy
                    and dc.int_zrodlo = :szrodlo and dc.int_id = :a_wartoscid
                  into :defcechw;
                if (defcechw is null) then
                  select dc.id_defcechw
                    from defcechw dc where dc.cecha = :defcechy
                      and dc.int_id = :a_wartoscid
                    into :defcechw;
              end
    
            if (defcechw is null) then --and zalozatrybut = 1 --insert do defcechw
              begin
                INSERT INTO DEFCECHW (CECHA, WARTSTR, WARTNUM, WARTDATE, OPIS, KOD, PREFIKS, ZMIEN, OPISDONAZWY, INT_ZRODLO, INT_ID)
                  VALUES (:defcechy, :a_wartosc, NULL, NULL, :a_wartosc, null, NULL, 0, :a_wartosc, :szrodlo, :a_atrybutid)
                  returning id_defcechw into :defcechw;
              end
          end
        end
        -- Uzupełnianie lub aktualizacja tabeli x_atrybuty
        if (error = 0) then begin
          if (a_atrybutid is not null) then begin
            select ref, cecha, refdefcechw, wartosc,
                regoper, int_zrodlo, nazwacechy, int_id
              from x_atrybuty
              where company = :company
                and oddzial = :oddzial
                and otable = :otable
                and oref = :oref
                and int_id = :a_atrybutid
          
            into :xatrybut, :o_cecha, :o_refdefcechw, :o_wartosc,
                :o_regoper, :o_int_zrodlo, :o_nazwacechy, :o_int_id;
          end else begin
            select ref, cecha, refdefcechw, wartosc,
                regoper, int_zrodlo, nazwacechy, int_id
              from x_atrybuty
              where company = :company
                and oddzial = :oddzial
                and otable = :otable
                and oref = :oref
               -- and int_id = :a_atrybutid
          
            into :xatrybut, :o_cecha, :o_refdefcechw, :o_wartosc,
                :o_regoper, :o_int_zrodlo, :o_nazwacechy, :o_int_id;
          end
    
          -- aktualizacja znalezionego rekordu X_atrybuty
          if (xatrybut is not null) then begin
            if (o_cecha is distinct from DEFCECHY or
                o_refdefcechw is distinct from DEFCECHW or
               o_wartosc is distinct from coalesce(a_wartosc, a_wartoscid) or
                --o_regoper,
                o_int_zrodlo is distinct from szrodlo or
                o_nazwacechy is distinct from a_atrybut)
            then begin
              update x_atrybuty set cecha = :DEFCECHY,
                  refdefcechw = :DEFCECHW,
                  wartosc = coalesce(:a_wartosc, :a_wartoscid),
                  int_zrodlo = :szrodlo,
                  nazwacechy = :a_atrybut
                where company = :company and oddzial = :oddzial
                  and otable = :otable and oref = :oref and int_id = :a_atrybutid;
            end
          -- nie byo takiego rekrdu w x_atrybuty, zakadamy nowy
          end else begin
           insert into  X_ATRYBUTY (COMPANY, ODDZIAL, OTABLE, OREF, CECHA, REFDEFCECHW, WARTOSC,
                      REGOPER, INT_ZRODLO, INT_ID, NAZWACECHY)
              values (:COMPANY, :ODDZIAL, :OTABLE, :OREF, :DEFCECHY, :DEFCECHW, coalesce(:a_wartosc, :a_wartoscid),
                    null, :szrodlo, :a_atrybutid, :a_atrybut)
    
            returning REF into :xatrybut;
    
          end
          /*
          update or insert into X_ATRYBUTY (COMPANY, ODDZIAL, OTABLE, OREF, CECHA, REFDEFCECHW, WARTOSC,
                      REGOPER, INT_ZRODLO, INT_ID, NAZWACECHY)
            values (:COMPANY, :ODDZIAL, :OTABLE, :OREF, :DEFCECHY, :DEFCECHW, coalesce(:a_wartosc, :a_wartoscid),
                    null, :szrodlo, :a_atrybutid, :a_atrybut)
            matching(company, oddzial, otable, oref,  int_id) -- [DG] XXX poprawka zwiazana z wylaczeniem tworzenia defcechw
            returning REF into :xatrybut;
            */
          o_cecha = null; o_refdefcechw = null; o_wartosc = null;
          o_regoper = null; o_int_zrodlo = null; o_nazwacechy = null; o_int_id  = null;
        end

     select dataostprzetw, x_atrybut  from INT_IMP_ATRYBUTY where ref = :a_ref into :dataostprzetw, :x_atrybut;
          
     if (xatrybut is not null and (dataostprzetw is null or x_atrybut is null)) then
       update INT_IMP_ATRYBUTY
          set otable = :otable, oref = :ref
          where ref = :a_ref; --and (dataostprzetw is null or x_atrybut is null)
           -- and :xatrybut is not null;


      if (coalesce(error,0) <> 0 or coalesce(errormsg,'') <> '' ) then
        update INT_IMP_ATRYBUTY
          set error = :error, errormsg = :errormsg,
            dataostprzetw = current_timestamp(0)
          where ref = :a_ref;
       dataostprzetw = null; x_atrybut = null;
      /*
      if (coalesce(trim(o_skadtabela), '') <> ''
          and coalesce(o_skadref,0) > 0 ) then
        begin
          sql = 'update '|| :o_skadtabela ||' set odbiorca_sente = '|| :odbiorca ||
            ' where ref = '|| :o_skadref;
          execute statement sql;
        end
      */

    end
    if ( error = 1) then begin                                                                                    -- XXX[KBI] 
      status = -1;                                                                                                -- XXX[KBI]
      msg = substring(( coalesce(msg,'')||' '||coalesce(errormsg,' Nieokrelony błąd.')) from 1 for 255);        -- XXX[KBI]
    end                                                                                                           -- XXX[KBI]
  end

  --przetworzenie tabel zaleznych
  /*if (blokujzalezne = 0) then
  begin
    tmptabela = null;
    tmpstatus = null;
    tmpmsg = null;

    sql = 'select zalezna from int_tabele where tabela = '''||:stabela||
      ''' and zrodlo = '||:szrodlo||' and kierunek = '||:skierunek||
      ' order by kolejnosc';

    for execute statement :sql into :tmptabela
    do begin
      tmpsql = 'select status, msg from INT_IMP_TOW_PROCESS('
        ||:sesja||', '''||:tmptabela||''', null, 1, 1, 0)';
      execute statement :tmpsql into :tmpstatus, :tmpmsg;
    end

    if (:tmptabela is null) then
    begin
      sql = 'select zalezna from int_tabele where tabela = '''||:stabela||
        ''' and zrodlo is null and kierunek = '||:skierunek||
        ' order by kolejnosc';

      for execute statement :sql into :tmptabela
        do begin
        tmpsql = 'select status, msg from INT_IMP_TOW_PROCESS('
            ||:sesja||', '''||:tmptabela||''', null, 1, 1, 0)';
         execute statement :tmpsql into :tmpstatus, :tmpmsg;
        end
    end

  end */


  --aktualizacja informacji o sesji
  if (:aktualizujsesje = 1) then
  begin
    select status, msg from int_sesje_aktualizuj(:sesja, 1) into :tmpstatus, :tmpmsg;
  end

  suspend;
end^
SET TERM ; ^
