--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DIMELEMDEF_ADD_ONE(
      DIMHIERDEF integer,
      SLODEF integer,
      SLOPOZ integer)
  returns (
      STATUS smallint)
   as
declare variable NAME varchar(80);
declare variable SHORTNAME varchar(40);
declare variable ACT smallint;
declare variable ACC ANALYTIC_ID;
begin
  status = 0;

  if (:dimhierdef > 0 and :slodef > 0 and :slopoz > 0) then
  begin
    if (not exists (select ref from dimelemdef where dimelemdef.slodef = :slodef
       and dimelemdef.slopoz = :slopoz and dimelemdef.dimhier = :dimhierdef)
    ) then begin
      insert into dimelemdef  (shortname, dimhier, slodef, slopoz, name, act,acc)
         select slopoz.kod, :dimhierdef, :slodef, :slopoz, substring(slopoz.nazwa from 1 for 80),
             slopoz.akt, slopoz.kontoks
           from slopoz where slopoz.slownik = :slodef and slopoz.ref = :slopoz;
      status = 1;
    end else begin
      select substring(slopoz.nazwa from 1 for 80), slopoz.kod, slopoz.akt, slopoz.kontoks
        from slopoz
        where slopoz.ref = :slopoz and slopoz.slownik = :slodef
        into :name, shortname, :act, :acc;
      update dimelemdef set dimelemdef.name = :name, dimelemdef.shortname = :shortname,
        dimelemdef.act = :act, dimelemdef.acc = :acc where dimelemdef.slopoz = :slopoz
          and dimelemdef.slodef = :slodef and dimelemdef.dimhier = :dimhierdef;
      status = 2;
    end
  end
end^
SET TERM ; ^
