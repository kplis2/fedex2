--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_GUS_THREE(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      FIELD varchar(5) CHARACTER SET UTF8                           )
  returns (
      AMOUNT numeric(15,2))
   as
declare variable frvhdr integer;
declare variable frpsn integer;
declare variable frcol integer;
declare variable yearid integer;
declare variable monthid integer;
declare variable smonthid integer;
declare variable cacheparams varchar(255);
declare variable ddparams varchar(255);
declare variable company integer;
declare variable frversion integer;
declare variable firstday date;
declare variable lastday date;
declare variable sumworkdim float;
declare variable fworkdim float;
declare variable lworkdim float;
declare variable speriod varchar(6);
begin
  select frvhdr, frpsn, frcol
    from frvpsns
    where ref = :frvpsn
    into :frvhdr, :frpsn, :frcol;

  yearid = cast(substring(:period from 1 for 4) as integer);
  smonthid = cast(substring(:period from 5 for 6) as integer);
  if (smonthid <= 3) then smonthid = 3;
    else if (smonthid <= 6) then smonthid = 6;
      else smonthid = 9;
  speriod = cast(:yearid || '0' || :smonthid as varchar(6));
  select company, frversion
    from frvhdrs
    where ref = :frvhdr
    into :company, :frversion;
  cacheparams = field||';'||smonthid||';'||company;
  ddparams = '';

  if (not exists (select ref from frvdrilldown where functionname = 'GUS_THREE' and cacheparams = :cacheparams and frversion = :frversion)) then
  begin
/* PRZECIETNA LICZBA ZATRUDNIONYCH - OGOLEM */
    if (field = 'D1P2') then begin
      monthid = 1;
      sumworkdim = 0;
      while (monthid  <= smonthid) do
        begin
          firstday = cast(yearid || '/' || monthid || '/1' as date);
          if (monthid = 12) then
            lastday = cast(yearid || '/' || monthid || '/31' as date);
          else
            lastday = cast(yearid || '/' || (monthid + 1) || '/1' as date) - 1;
          select coalesce (sum(coalesce(e.workdim, 0)),0)
            from employment e
            join employees  p on (p.ref = e.employee)
            where e.fromdate <= :firstday and p.company = :company
              and (e.todate is null or e.todate >= :firstday)
            into :fworkdim;
          select coalesce (sum(coalesce(e.workdim, 0)),0)
            from employment e
            join employees p on (e.employee = p.ref)
            where e.fromdate <= :lastday
              and (e.todate is null or e.todate >= :lastday) and company = :company
            into :lworkdim;

          sumworkdim = sumworkdim + (fworkdim + lworkdim) / 2;
          monthid = monthid + 1;
        end
      amount = sumworkdim / smonthid;
/* PRACUJACY W OSOBACH */
    end else if (field = 'D1P3') then begin
        select count(*)
          from employees E
          join employment C on (E.ref = C.employee)
          where C.fromdate <= (cast(:yearid || '/' || (:smonthid + 1) || '/1' as date) - 1)
            and (C.todate is null or C.todate >= (cast(:yearid || '/' || (:smonthid + 1) || '/1' as date) - 1))
            and e.company = :company
          into :amount;
/* CZAS PRZEPRACOWANY */
    end else if (field = 'D1P4') then begin
        select sum(P.pvalue) / 1000
          from epayrolls PR
          join eprpos P on (P.payroll = PR.ref)
          where PR.cper starting with :yearid
            and PR.cper <= :speriod
            and P.ecolumn in (520,530)
            and pr.company = :company
          into :amount;
/* WYNAGRODZENIA OGOLEM */
    end else if (field = 'D1P5') then begin
        select sum(P.pvalue) / 1000
          from epayrolls PR
          join eprpos P on (P.payroll = PR.ref)
          where PR.cper starting with :yearid
            and PR.cper <= :speriod
            and P.ecolumn = 4000
            and pr.company = :company
          into :amount;
/* SKLADKI NA UBEZPIECZENIE EMERYTALNE, RENTOWE I CHOROBOWE */
    end else if (field = 'D1P8') then begin
        select sum(P.pvalue) / 1000
          from epayrolls PR
          join eprpos P on (P.payroll = PR.ref)
          where PR.cper starting with :yearid
            and PR.cper <= :speriod
            and P.ecolumn in (6100, 6110, 6120, 6130)
            and pr.company = :company
          into :amount;
/* PODATEK DOCHODOWY OD OSOB FIZYCZNYCH */
    end else if (field = 'D1P9') then begin
        select sum(P.pvalue) / 1000
          from epayrolls PR
          join eprpos P on (P.payroll = PR.ref)
          where PR.cper starting with :yearid
            and PR.cper <= :speriod
            and P.ecolumn = 7100
            and pr.company = :company
          into :amount;
    end

  end else
    select first 1 fvalue from frvdrilldown where cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr
      into :amount;
  if (amount is null) then amount = 0;
  insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
     values (:frvhdr, :frpsn, :frcol, 'GUS_THREE', :cacheparams, :ddparams, :amount, :frversion);
  suspend;
end^
SET TERM ; ^
