--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PM_ROZLICZENIE_POZ(
      PMPOSITION integer,
      DATAOD date,
      DATADO date)
  returns (
      REF integer,
      STYP varchar(2) CHARACTER SET UTF8                           ,
      KWOTA numeric(14,2),
      NUMERPOZ integer,
      NUMERDOK integer,
      SYMBOL varchar(255) CHARACTER SET UTF8                           ,
      ILOSC numeric(14,4),
      KTM varchar(40) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      DATA timestamp)
   as
begin
  for
  select dn.ref, 'M', dp.wartosc, dp.numer as typ, dn.numer,
         dn.symbol, dp.ilosc, dp.ktm, tw.nazwa, dn.data
    from dokumpoz dp
    left join dokumnag dn on (dn.ref = dp.dokument)
    left join defdokum dd on (dd.symbol = dn.typ)
    left join towary tw on (dp.ktm = tw.ktm)
    where dd.rozliczpo = 1 and dp.pmposition = :pmposition
          and dn.akcept = 1 and dd.wydania = 1
          and (:dataod <= dn.data or :dataod is null)
          and (:datado >= dn.data or :datado is null)

  union

  select dn.ref, 'M', -dp.wartosc, dp.numer as typ,
         dn.numer, dn.symbol, dp.ilosc, dp.ktm, tw.nazwa, dn.data
    from dokumpoz dp
    left join dokumnag dn on (dn.ref = dp.dokument)
    left join defdokum dd on (dd.symbol = dn.typ)
    left join towary tw on (dp.ktm = tw.ktm)
    where dd.rozliczpo = 1 and dp.pmposition = :pmposition
          and dn.akcept = 1 and dd.wydania = 0
          and (:dataod <= dn.data or :dataod is null)
          and (:datado >= dn.data or :datado is null)

  union

  select nf.ref, 'U', pfu.kwota, pf.numer, nf.numer, nf.symbol,
         pf.ilosc, pf.ktm, pf.nazwat, nf.dataotrz
    from pozfakusl pfu
    left join pozfak pf on (pf.ref = pfu.refpozfak)
    left join nagfak nf on (nf.ref = pf.dokument)
    left join typfak tf on (tf.symbol = nf.typ)
    where pfu.pmposition = :pmposition
          and nf.akceptacja = 1 and tf.rozliczpo = 1 and tf.zakup = 1
          and (:dataod <= nf.dataotrz or :dataod is null)
          and (:datado >= nf.dataotrz or :datado is null)

  union

  select rn.ref, 'S', rn.stawka * rn.dniwypoz, 1, 0, rz.nazwa,
         rn.dniwypoz, rz.ktm, rz.opis, rn.datado
    from reznag rn
    left join rezzasoby rz on (rn.rezzasob = rz.ref)
    where rn.pmposition = :pmposition
          and rn.status = 3
          and (:dataod <= rn.datado or :dataod is null)
          and (:datado >= rn.datado or :datado is null)

  union

  select rcp.ref, 'R', p.budprice * rcp.amount, 1, 0, e.personnames,
         rcp.amount, p.ktm, p.name, rcp.data
    from pmrcp rcp
    left join pmpositions p on (rcp.pmposition = p.ref)
    left join employees e on (e.ref=rcp.employee)
    where rcp.pmposition = :pmposition
          and (:dataod <= rcp.data or :dataod is null)
          and (:datado >= rcp.data or :datado is null)

  into :ref, :styp, :kwota, :numerpoz, :numerdok, :symbol, :ilosc, :ktm, :nazwa, :data
  do begin
    suspend;
  end
end^
SET TERM ; ^
