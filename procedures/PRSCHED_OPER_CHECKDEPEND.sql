--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_OPER_CHECKDEPEND(
      SCHEDOPER integer)
   as
declare variable operref integer;
declare variable opernum integer;
declare variable opermaster integer;
declare variable opermcomplex integer;
declare variable opermtype integer;
declare variable opersheet integer;
declare variable curoper integer;
declare variable prevopernum integer;
declare variable prevoperref integer;
begin
  /* Procedure Text */
  delete from prschedoperdeps where prschedoperdeps.depto = :schedoper;
  select prschedopers.shoper from prschedopers where ref=:schedoper into :curoper;

  while (:curoper is not null)
  do begin
    select A.number,a.sheet,a.opermaster, B.opertype
      from prshopers A
       left join prshopers B on (A.opermaster = B.REF)
    where A.ref = :curoper
    into :opernum, :opersheet, :opermaster, :opermtype;
    if(:opermaster > 0 and :opermtype = 0) then begin
      --operacja nadrzedna jest szeregowa - szukam, czy jest moj poprzednik
      prevopernum = null;
      select max(prshopers.number) from prshopers where opermaster = :opermaster and number < :opernum into :prevopernum;
      if(:prevopernum > 0) then begin
         select ref from prshopers where opermaster = :opermaster and number = :prevopernum into :prevopernum;
         execute procedure prsched_oper_savedepend(:schedoper,:prevopernum);
         exit;
      end
    end else if(:opermaster is null) then begin
      prevopernum = null;
      select max(prshopers.number) from prshopers where opermaster  is null and sheet = :opersheet and number < :opernum into :prevopernum;
      if(:prevopernum > 0) then begin
         select ref from prshopers where opermaster is null and sheet =:opersheet and number = :prevopernum into :prevoperref;
         execute procedure prsched_oper_savedepend(:schedoper,:prevoperref);
         if(not exists(select ref from prschedoperdeps where depto = :schedoper)) then begin
           opermaster = prevoperref;
         end else exit;
      end
    end
    curoper = opermaster;
  end
end^
SET TERM ; ^
