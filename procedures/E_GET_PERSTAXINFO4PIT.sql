--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_PERSTAXINFO4PIT(
      YEARID varchar(4) CHARACTER SET UTF8                           ,
      PERSON integer,
      INFOTYPE smallint = 0,
      COMPANY COMPANIES_ID = null)
  returns (
      COSTS smallint,
      TAXOFFICE varchar(200) CHARACTER SET UTF8                           ,
      USCODE varchar(4) CHARACTER SET UTF8                           ,
      TAXID varchar(15) CHARACTER SET UTF8                           ,
      BUSINESSACTIV smallint)
   as
declare variable YTODATE date;
declare variable yfromdate date;
declare variable cfromdate date;
declare variable EMPLIST varchar(50);
declare variable COSTS_NR smallint;
declare variable COSTS_tmp smallint;
declare variable employee integer;
declare variable ECOUNT integer;
begin
/*Perosnel - MWr: Procedura zwraca informacje podatkowe potrzebne do wypelnienia
  wybranych wydrukow deklaracji PIT za dany rok YEARID, dla danej osoby PERSON.
  Informacje te to: rodzaj kosztow uzyskania COSTS wg opisu na deklaracji; nazwa
  Urzedu Skarbowgo: TAXOFFICE; kod US do e-deklracji USCODE; info czy osoba prowadzi
  dzial. gosp. BUSINESSACTIV i odbowiadajacy jej indentyfikator podatkowy TAXID.

  Parametr INFOTYPE = 0 (domyslnie) procedura zwraca wszystkie informacje
                    = 1 procedura zwraca tylko informacje o kosztach
                    = 2 procedura zwraca wszystkie inne informacje oprocz kosztow */

  infotype = coalesce(infotype,0);
  company = coalesce(company,0);
  yfromdate = yearid || '-01-01';
  ytodate = yearid || '-12-31';

  if (company = 0) then
    execute procedure get_global_param('CURRENTCOMPANY')
      returning_values :company;

  if (infotype in (0,1)) then
  begin
    emplist = ';';
    ecount = 0;
    costs_nr = 0;
    costs = 0;
  
    for
      select distinct e.ref from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype = 1)
          join employees e on (e.ref = p.employee)
        where p.ecolumn = 5950
          and e.person = :person
          and e.company = :company
          and r.company = :company
          and r.tper starting with :yearid
      into :employee
    do begin
      ecount = ecount + 1;
      emplist = emplist || employee || ';';

      costs_tmp = null;
      select first 1 costs, fromdate from empltaxinfo
        where employee = :employee and fromdate < :ytodate
        order by fromdate desc
        into :costs_tmp, :cfromdate;

      if (extract(year from cfromdate) = yearid) then
      begin
        cfromdate = null;
        select first 1 fromdate from empltaxinfo
          where employee = :employee and fromdate <= :yfromdate
          order by fromdate desc
          into :cfromdate;

        cfromdate = coalesce(cfromdate,yfromdate);
        select max(costs) from empltaxinfo
          where employee = :employee and fromdate < :ytodate and fromdate >= :cfromdate
          into :costs_tmp;
      end

      if (costs < costs_tmp) then
        costs = costs_tmp;
    end

    if (ecount > 0) then
    begin
      costs_nr = 1;

      if (ecount > 1) then
      begin
      --kartotek pracow. wiecej niz jedna - sprawdzamy czy praca odbywala sie rownolegle
        ecount = 0;

        select count(e.ref) from employees e
          where :emplist like '%;' || e.ref || ';%'
            and exists (select first 1 1 from epayrolls r2
                          join eprpos p2 on (r2.ref = p2.payroll and r2.tper starting with :yearid and r2.empltype = 1 and p2.ecolumn = 6500)
                          join employees e2 on (e2.person = e.person and e.ref <> e2.ref and e2.ref = p2.employee and :emplist like '%;' ||e2.ref || ';%'))
          into :ecount;

        if (ecount > 1) then
          costs_nr = 2;  --wiecej niz jeden stosunek pracy
      end
    end
  
    if (costs = 0) then
      costs = null; --edeklaracje nie lubia zerowych kosztow
    else if (costs = 1 and costs_nr = 2) then
      costs = 2; --koszty z wiecej niz jednego stosunku pracy
    else if (costs = 2 and costs_nr = 1) then
      costs = 3; --koszty podwyzszone, z jednego stosunku pracy
    else if (costs = 2 and costs_nr = 2) then
      costs = 4; --koszty podwyzszone, z wiecej niz jednego stosunku pracy

  end

  if (infotype in (0,2)) then
  begin
  --pobranie Urzedu Skarbowego, identyfikatora podatkowego osoby, itp
    select first 1 coalesce(ir.name,'') || ', ' || coalesce(ir.address,'') || ', ' || coalesce(ir.postcode,'') || ' ' || coalesce(ir.city,''),
        ir.code, t.businessactiv, case when t.businessactiv = 0 then p.pesel else p.nip end
      from persons p
        join employees e on (e.person = p.ref)
        join empltaxinfo t on (t.employee = e.ref)
        left join einternalrevs ir on (t.taxoffice = ir.ref)
      where t.fromdate < :ytodate
        and p.ref = :person
        and e.company = :company
      order by t.fromdate desc
      into :taxoffice, :uscode, :businessactiv, :taxid;
  
    taxid = replace(taxid,  '-',  '');
  end

  suspend;
end^
SET TERM ; ^
