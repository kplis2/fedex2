--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRGENORDSPREPARE_NAGZAMSPR_MIN(
      REJESTR varchar(3) CHARACTER SET UTF8                           ,
      TYPZAM varchar(3) CHARACTER SET UTF8                           ,
      DATAOD varchar(10) CHARACTER SET UTF8                           ,
      DATADO varchar(10) CHARACTER SET UTF8                           ,
      KLIENT integer,
      MASKA varchar(60) CHARACTER SET UTF8                           ,
      PRREJESTR varchar(3) CHARACTER SET UTF8                           ,
      BYPOZZAM smallint,
      ILWMS smallint,
      SHOWEMPTY smallint,
      AUTOSCHED smallint,
      PRDEPART PRDEPARTS_ID = null)
   as
declare variable SQL varchar(4000);
declare variable WERSJAREF integer;
declare variable AMOUNT numeric(14,4);
declare variable DESCRIPT varchar(1024);
declare variable NAGZAM integer;
declare variable POZZAM integer;
declare variable PRSHEET integer;
declare variable AMOUNTINWH numeric(14,4);
declare variable AMOUNTINPRORD numeric(14,4);
declare variable AMOUNTINORD numeric(14,4);
declare variable AMOUNTMIN numeric(14,4);
declare variable AMOUNTMAX numeric(14,4);
declare variable TERMDOST timestamp;
declare variable actuprdepart prdeparts_id;
begin
  delete from PRGENORDSPREPARE;
  MASKA =  '%'||coalesce(:MASKA,'')||'%';
  if( not exists (select first 1 1 from prdeparts pd where pd.symbol = :prdepart))
    then prdepart = null;
  for
    select s.wersjaref, sum(s.ilosc), sum(s.stanmin), sum(s.stanmax)
      from stanyil s
      where s.ktm like :MASKA
      group by s.wersjaref
      into :wersjaref,:amountinwh, :amountmin, :amountmax
  do begin
    if(ilwms = 1) then
      select sum(s.quantity - s.blocked + s.ordered)
        from mwsstock s
        where s.vers = :wersjaref
        into :amountinwh;
    amount = coalesce(:amountmin, 0) - coalesce(:amountinwh,0);
    if (:amount > 0 or :showempty = 1) then
    begin
      prsheet = null;
      actuprdepart = null;
      select first 1 p.ref, p.prdepart
        from prsheets p
        where p.wersjaref = :wersjaref and (:PRDEPART is null or p.prdepart = :PRDEPART)
          and p.status = 2
        into :prsheet, :actuprdepart;
      if(:PRDEPART is null or :prsheet is not null) then
      insert into PRGENORDSPREPARE(MODE, PRSHEET, AMOUNT, REJESTR, NAGZAM, POZZAM, PRNAGZAM, PRPOZZAM,
          OTABLE, OREF, WERSJAREF, DESCRIPT, PRSCHEDZAMDATE, AMOUNTINPRORD, AMOUNTINWH, AMOUNTINORD, AUTOSCHED,
          amountmin,  amountmax, termdost, prdepart)
      values(2, :prsheet, :amount, :prrejestr, null, null, null, null,
          null, null, :wersjaref, null, current_date, null, :amountinwh, null, 0,
          :amountmin, :amountmax, null, :actuprdepart);
    end
  end
end^
SET TERM ; ^
