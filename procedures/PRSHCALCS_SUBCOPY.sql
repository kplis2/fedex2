--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHCALCS_SUBCOPY(
      FROMPRSHCALC integer,
      FROMPRCALCCOL integer,
      TOPRSHCALC integer,
      TOPRCALCCOL integer)
   as
declare variable toref integer;
declare variable fromref integer;
begin
  -- skopiuj biezaca rubryke kalkulacji
  execute procedure GEN_REF('PRCALCCOLS') returning_values :toref;
  insert into prcalccols(ref, parent, calc, prcolumn, cvalue, expr, modified, fromprcalccol,
                         key1, key2, prefix, descript, calctype, fromprshmat, fromprshoper, fromprshtool,
                         isalt, prshoperalt, excluded, color)
  select :toref, :toprcalccol, :toprshcalc, prcolumn, cvalue, expr, modified, fromprcalccol,
         key1, :toref, prefix, descript, calctype, fromprshmat, fromprshoper, fromprshtool,
         isalt, prshoperalt, excluded, color
  from prcalccols where ref=:fromprcalccol;

  -- przejdz sie po rubrykach podrzednych i wywolaj dla nich rekurencje
  for select ref from prcalccols where calc=:fromprshcalc and parent=:fromprcalccol
  into :fromref
  do begin
    execute procedure PRSHCALCS_SUBCOPY(:fromprshcalc, :fromref, :toprshcalc, :toref);
  end
end^
SET TERM ; ^
