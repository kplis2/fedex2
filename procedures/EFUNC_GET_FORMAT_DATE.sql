--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EFUNC_GET_FORMAT_DATE(
      DATEIN date,
      FORMAT varchar(10) CHARACTER SET UTF8                            = 'YYYY-MM-DD')
  returns (
      DATEOUT varchar(10) CHARACTER SET UTF8                           )
   as
declare variable DDAY varchar(2);
declare variable DMONTH varchar(2);
declare variable DYEAR varchar(4);
begin
/*MWr: Procedura pozwala uzyskac dla zadanej daty DATEIN wymagany format, zadany
       przez parametr FORMAT (zlozony z elementow: YYYY, MM, DD)*/

  if (datein is not null) then
  begin
    execute procedure get_symbol_from_number(extract(day from datein), 2)
      returning_values :dday;
  
    execute procedure get_symbol_from_number(extract(month from datein), 2)
      returning_values :dmonth;
  
    dyear = extract(year from datein);
  
    dateout = format;
    dateout = replace(dateout,  'YYYY',  dyear);
    dateout = replace(dateout,  'MM',  dmonth);
    dateout = replace(dateout,  'DD',  dday);
  end
  suspend;
end^
SET TERM ; ^
