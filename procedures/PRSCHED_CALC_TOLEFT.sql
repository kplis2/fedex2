--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_CALC_TOLEFT(
      SCHEDULE integer,
      LASTOPER integer,
      OPERCOUNT integer)
  returns (
      SCHEDOPER integer,
      CNT integer)
   as
declare variable curtime timestamp;
declare variable schednum integer;
declare variable isoper smallint;
declare variable status smallint;
declare variable tstart timestamp;
declare variable tdepend timestamp;
declare variable tend timestamp;
declare variable machine integer;
declare variable verifiednum integer;
declare variable shoper integer;
declare variable worktime float;
declare variable workplace varchar(20);
declare variable prdepart varchar(20);
declare variable matreadytime timestamp;
declare variable laststatus smallint;
declare variable workload double precision;
declare variable tprevend timestamp;
declare variable rstart timestamp;
declare variable rend timestamp;
declare variable rmachine integer;
declare variable rschedoper integer;
declare variable latenes double precision;
declare variable minlatenes double precision;
declare variable minspace integer;
declare variable "ROUND" integer;
declare variable minlength double precision;
declare variable rthoper integer;
declare variable tdependr timestamp;
declare variable rstartfwt timestamp;
declare variable tstartfwt timestamp;
declare variable machinecount integer;
declare variable altoper smallint;
declare variable brakmat smallint;
declare variable sql varchar(1024);
declare variable c integer;
begin
  c = 0;
  laststatus = 2;
  select PRSCHEDULES.mintimebetweenopers, prschedules.roundstarttime,  prschedules.fromdate, prschedules.prdepart
      from prschedules
      where prschedules.ref = :schedule
      into :minspace, :round, :curtime, :prdepart;
  minlength = 1440;
  minlength = 1/:minlength;
  if(:round > :minspace or (:minspace is null) ) then minspace = :round;
  minlength = :minlength * :minspace;
  if(:lastoper = 0) then begin
    verifiednum = 0;
    --ustawianei poczatkowa readytoharm
    update prschedopers set readytoharm = 0 where schedule = :schedule;
    for select a.ref
      from PRSCHEDOPERS A
      where A.SCHEDULE = :schedule and A.VERIFIED = 0 and a.activ = 1
        and not exists(select b.ref
          from prschedoperdeps dp
          left join prschedopers b on (dp.depfrom = b.ref)
          where dp.depto = a.ref and b.ref is not null and b.verified = 0 and b.status < 3 and b.activ = 1
        )
        and a.status < 3
      into :schedoper
    do update prschedopers set readytoharm = 1 where ref = :schedoper;

  end else
    select VERIFIED, STATUS from PRschedopers where ref=:lastoper into :verifiednum, :laststatus;
  cnt = 0;
  if(:curtime is null or (:curtime < current_timestamp(0)) ) then curtime = current_timestamp(0);
  while(:cnt < :opercount and :lastoper >= 0) do begin
    verifiednum = :verifiednum + 1;
    -- dla wszystkich operacji niezweryfikowanych, ktore nie posiadaja niezweryfikowanych operacji poprzedzajacych, okresl czas dostepnosci materialow
    for select first 1 a.ref
      from PRSCHEDOPERS A
      where A.SCHEDULE = :schedule and A.VERIFIED = 0 and a.statusmat is null
        and a.readytoharm = 1
        -- and number=(select first 1 number from prschedopers b where b.guide=a.guide and b.activ=1 order by number)
      order by
        A.schedule, A.verified, /*A.starttime,*/
        A.status desc,  -- najpierw w trakcei realizacji
        A.schedzamnum, A.GUIDENUM -- zgodnie z priorytetami
      into :schedoper
    do begin
      execute procedure PRSCHED_OPER_CHECKMATERIALSTATE(:schedoper);
    end
    schedoper = null;
    rschedoper = null;
    minlatenes = null;
    --dla kazdejoperacji okrel czas "opoxnienia" startu w stosunku do miejsca, w ktorym mozna posadzic operacje
    if(exists (select ref from prschedopers where SCHEDULE = :schedule and VERIFIED = 0 and readytoharm = 1 and materialreadytime is not null)) then
       brakmat = 1;
    else
       brakmat = 0;

    for select a.ref, a.Starttime, a.ENDTIME, a.STATUS, a.MACHINE, a.worktime, a.SHOPER, a.materialreadytime, a.workplace
      from PRSCHEDOPERS A
      where A.SCHEDULE = :schedule and A.VERIFIED = 0 and a.readytoharm = 1
            and (a.materialreadytime is not null or a.verified=:brakmat)
      order by A.schedule, A.verified, a.readytoharm, A.schedzamnum, A.GUIDENUM
      into :schedoper, :tstart,  :tend, :status, :machine, :worktime, :shoper, :matreadytime, :workplace
    do begin
      if(:minlatenes < :minlength) then
        break;  -- jesli ostatni odstep jest wystarczajaco maly, to nei szukam dalej
      tdepend = null;
      --okreslenie minimalnego czasu rozpoczecia
      select max(endtime)
        from prschedopers p
          join prschedoperdeps on (prschedoperdeps.depfrom = p.ref)
        where prschedoperdeps.depto = :schedoper
        into :tdepend;
      if(:tdepend is null) then tdepend = :curtime;
      if(:tstart is null or :tstart < :tdepend )then begin
        tstart = tdepend;
      end
      --sprawdzenie zaleznosci materialowej
      if(:matreadytime > :tstart) then
        tstart  = :matreadytime;
     --sprawdzenie, czy start nie wypad w czasie przerwy pracy - jesli tak, to na poczatek kolejnego czasu
      execute procedure PRSCHED_CALENDAR_FIRSTWORKTIME(:prdepart,:tstart) returning_values
         :tstartfwt;
      if(:workplace <>'') then begin
        execute procedure prsched_find_machine(:schedoper,:tstartfwt, 0) returning_values :tstart,  :tend, :machine;
        if(:machine is null) then begin

           execute procedure prsched_checkoperstarttime(:schedoper, :tstartfwt) returning_values :tstart;
           execute procedure pr_calendar_checkendtime(:prdepart, :tstart, :worktime) returning_values :tend;
        end
        --sprawdzenie opoznienia w stosunku do poprzedzajacej oepracji na danej maszynie
        tprevend = null;
        select max (prschedopers.endtime)
        from PRSCHEDOPERS
        where schedule = :schedule
          and machine = :machine
          and endtime <= :tstart
          and ref<> :schedoper
        into :tprevend;
        if(:tprevend < :curtime) then
           tprevend = null;--jesli poprzednie operacje już si zakonczyly/powinny byly zakonczyc, to tak, jakby ich nie bylo
        if(:tprevend is null) then latenes = 0;
        else latenes = :tstart - :tprevend;
        if(:minlatenes is null or (minlatenes > :latenes))then begin

          rschedoper = schedoper;
          rmachine = :machine;
          rstart = :tstart;
          rend = :tend;
          minlatenes = :latenes;
          rstartfwt = :tstartfwt;
        end
      end else begin
        machine = null;
        execute procedure prsched_checkoperstarttime(:schedoper, :tstart) returning_values :tstart;
        execute procedure pr_calendar_checkendtime(:prdepart, :tstart, :worktime) returning_values :tend;
        rschedoper = schedoper;
        rmachine = machine;
        rstart = :tstart;
        rstartfwt = :tstart;
        rend = :tend;
        minlatenes = 0;
      end
      c = :c + 1;

    end
    if(:rschedoper is null) then begin
      -- zakończenie harmonogramowania operacji
      schedoper = -1;
      exit;
    end
    update prschedopers set verified  = :verifiednum where ref=:rschedoper;

    update prschedopers set STARTTIME = :rstart, ENDTIME = :rend, MACHINE = :rmachine, verified  = :verifiednum where ref=:rschedoper;

    --ustawianie readytoharm na innych
    for select o.ref
      from prschedoperdeps d
      left join prschedopers o on (o.ref = d.depto)
      where d.depfrom = :rschedoper and o.activ = 1
      into :rthoper
    do begin
      execute procedure prsched_calc_readytoharm_check(:rthoper);
    end
    schedoper = rschedoper;
    cnt = :cnt + 1;
  end
end^
SET TERM ; ^
