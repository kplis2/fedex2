--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_OBLICZSYMBOLFAK(
      DOKUMSPED integer)
   as
DECLARE VARIABLE SYMBOL VARCHAR(1000);
DECLARE VARIABLE SSYMBOL VARCHAR(255);
BEGIN
  symbol = '';
  for
    select numer
      from nagfak
      where nagfak.listywysd = :dokumsped
      order by ref
      into :ssymbol
  do begin
    if(symbol <> '') then symbol = symbol ||';';
    symbol = symbol||ssymbol;
    symbol =  substring(:symbol from 1 for 60);
  end
  update LISTYWYSD set listywysd.symbfak = :symbol
   where ((symbfak <> :symbol) or (symbfak is null)) and REF=:dokumsped;
END^
SET TERM ; ^
