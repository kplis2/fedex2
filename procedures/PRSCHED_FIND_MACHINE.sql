--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_FIND_MACHINE(
      SCHEDOPER integer,
      TSTART timestamp,
      TRYB smallint)
  returns (
      STARTTIME timestamp,
      ENDTIME timestamp,
      MACHINE integer)
   as
declare variable defmachine integer;
declare variable workplace varchar(20);
declare variable schedule integer;
declare variable schedzamnum integer;
declare variable gapfound smallint;
declare variable localtime timestamp;
declare variable schedzam integer;
declare variable worktime double precision;
declare variable cnt integer;
declare variable minspace integer;
declare variable minlength double precision;
declare variable prdepart varchar(20);
declare variable tmpendtime timestamp;
declare variable s1 timestamp;
declare variable s2 timestamp;
declare variable s3 timestamp;
declare variable m1 integer;
begin
  select po.MACHINE, po.worktime, ph.workplace, p.mintimebetweenopers, p.prdepart
    from PRSCHEDOPERS po
      join prshopers  ph on (po.shoper = ph.ref)
      join prschedzam  pz on (pz.ref = po.schedzam)
      join prschedules p on (p.ref = pz.prschedule)
    where po.ref=:schedoper
    into :machine, :worktime, :workplace, :minspace, :prdepart;

  minlength = 1440;
  minlength = coalesce(minspace,0)/:minlength;
  --minlength = 1/:minlength;
  --minlength = :minlength * :minspace;

  starttime = :tstart;
  endtime = :tstart;
  defmachine = :machine;--sprawdzenie, czy maszyna juz jest przypisana

  /*if(:defmachine is null) then begin
    --sprawdzenie, czy dla tego stanowiska byla juz wyboerana maszyna w ramach zlecenia
    --  - jesli tak, to zachowanie tej samej maszyny
    select max(m.ref)
      from PRMACHINES m
        join PRSCHEDOPERS po on (po.machine = m.ref)
      where po.schedzam = :schedzam
        and m.prworkplace = :workplace
     into :defmachine;
  end*/

  for
    select p.rmachine, p.rstart, p.rend
      from prsched_free_machine(:workplace, :machine, :tryb) p
      where (cast(p.rend - :starttime as double precision) >= :worktime or p.rend is null)
      order by p.rstart
      into :m1, :s1, :s2
  do begin
    if (:s1 < :tstart) then
      s1 = :tstart;

    --przesuniecie poczatku na dzien pracujacy
    execute procedure PRSCHED_CALENDAR_FIRSTWORKTIME(:prdepart,cast(:s1 as timestamp))
      returning_values :s3;--:starttime;
    --zaokrąaglenie i odsuniecie od poprzedniej
    execute procedure prsched_checkoperstarttime(:schedoper,:s3)
      returning_values :s3;
    if (:s2 is null or cast(:s2 - :s3 as double precision) - :minlength >= :worktime) then--czas zakonczenia przerwy nie jest okreslony lub potencjalnei sie miesci
    begin
      execute procedure pr_calendar_checkendtime(:prdepart, :s3, :worktime)
        returning_values :tmpendtime;
      if (:tmpendtime <= :s2 or :s2 is null) then
      begin
        s2 = :tmpendtime;
        machine = :m1;
        starttime = :s3;
        endtime = :s2;
         suspend;
      end
    end 
  end
end^
SET TERM ; ^
