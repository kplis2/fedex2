--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CENNIK_AKTUREG(
      CENNIK integer,
      KTMMASKA varchar(40) CHARACTER SET UTF8                           ,
      ODKTM varchar(40) CHARACTER SET UTF8                           ,
      DOKTM varchar(40) CHARACTER SET UTF8                           ,
      GRUPA varchar(60) CHARACTER SET UTF8                           ,
      TRYB integer,
      ZRODLO integer,
      WARTOSC numeric(14,2),
      WERSJAREF integer,
      DODAJ integer,
      FORCECONSTS integer,
      TOLERANCJA numeric(14,2),
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      CENNIKNAD integer,
      PRMIES numeric(14,2))
  returns (
      STATUS integer,
      ILEPRZEL integer,
      ILEDOD integer)
   as
declare variable cnt integer;
declare variable nowacenanet numeric(14,2);
declare variable nowacenabru numeric(14,2);
declare variable nowamarza numeric(14,2);
declare variable nowynarzut numeric(14,2);
declare variable staracenanet numeric(14,2);
declare variable staramarza numeric(14,2);
declare variable starynarzut numeric(14,2);
declare variable ktm varchar(40);
declare variable jedn integer;
declare variable przelicz numeric(14,4);
declare variable zrodlomarzy integer;
declare variable kurs numeric(14,4);
declare variable defstalacena smallint;
declare variable staryprmies numeric(14,2);
begin
  ileprzel = 0;
  iledod = 0;
  if(odktm is null) then odktm = '';
  if(doktm is null) then doktm = '';
  if(ktmmaska is null) then ktmmaska = '';
  if(:cennik is not null) then begin
    select ZRODLO, KURS, DEFSTALACENA
      from DEFCENNIK
      where REF=:cennik
      into :zrodlomarzy, :kurs, :defstalacena;
    if(:kurs = 0 or (:kurs is null)) then kurs = 1;
    if(:wersjaref is not null and :wersjaref>0) then begin
      /* przeliczenie ceny konkretnego towaru */
      select count(*) from CENNIK where CENNIK=:cennik and WERSJAREF=:wersjaref into :cnt;
      if(:cnt=0) then begin
        if(:dodaj>0) then begin
          /* jesli nie ma go w cenniku, to dodajemy w jednostce magazynowej */
          select KTM from WERSJE where REF=:WERSJAREF into :ktm;
          execute procedure CENNIK_OBLICZREG(:ktm, :wersjaref, :tryb, :zrodlo, :wartosc, 1, :zrodlomarzy, :kurs, 0, 0,
                                             :tolerancja, 0, :magazyn, :cenniknad)
            returning_values :nowacenanet,:nowacenabru,:nowamarza,:nowynarzut;
          if(:nowacenanet<>0) then begin
            insert into CENNIK(CENNIK,WERSJAREF,CENANET,CENABRU,STALACENA,MARZA,NARZUT,PROCENTMIES)
              values (:cennik,:wersjaref,:nowacenanet,:nowacenabru, :defstalacena, :nowamarza,:nowynarzut,:prmies);
            iledod = :iledod+1;
          end
        end
      end else if(:dodaj<2) then begin
        /* jesli jest, to przeliczamy we wszystkich jednostkach */
        for select CENNIK.KTM, CENNIK.JEDN, TOWJEDN.przelicz, CENNIK.CENANET,CENNIK.MARZA,CENNIK.NARZUT
              from CENNIK join TOWJEDN on (CENNIK.JEDN = TOWJEDN.REF)
              where CENNIK = :CENNIK and (STALACENA=0 or (:forceconsts = 1))
                and (cennik.WERSJAREF =:wersjaref)
            into :ktm, :jedn, :przelicz, :staracenanet, :staramarza, :starynarzut
        do begin
          execute procedure CENNIK_OBLICZREG(:ktm, :wersjaref, :tryb, :zrodlo, :wartosc, :przelicz, :zrodlomarzy,
                                             :kurs, :STARACENANET, 0, :tolerancja, 1, :magazyn, :cenniknad)
            returning_values :nowacenanet,:nowacenabru,:nowamarza,:nowynarzut;
          if(:nowacenanet<>:staracenanet or (:nowamarza<>:staramarza) or (:nowynarzut<>:starynarzut)) then begin
            update cennik set marza = :nowamarza, narzut = :nowynarzut, cenanet = :nowacenanet, cenabru = :nowacenabru, CENAPNET = CENANET, CENAPBRU = CENABRU, PROCENTMIES=:prmies
              where CENNIK = :cennik
                and WERSJAREF = :wersjaref
                and JEDN=:jedn;
            ileprzel = :ileprzel+1;
          end
        end
      end
    end else begin
      if(:dodaj<2) then begin
        /* przeliczenie ceny dla wszystkich towarow obecnych w cenniku */
        for select CENNIK.KTM, CENNIK.WERSJAREF, CENNIK.JEDN, TOWJEDN.przelicz, CENNIK.CENANET, CENNIK.MARZA,
                   CENNIK.NARZUT, CENNIK.PROCENTMIES
              from CENNIK join TOWJEDN on (CENNIK.JEDN = TOWJEDN.REF)
                          join TOWARY on (TOWARY.KTM = CENNIK.KTM)
              where CENNIK = :CENNIK and (STALACENA=0 or (:forceconsts = 1))
                and (cennik.KTM >=:odktm or (:odktm = '') or (:odktm is null))
                and (cennik.KTM <= :doktm or (:doktm = '') or (:doktm is null))
                and (cennik.KTM like :ktmmaska or (:ktmmaska = '') or (:ktmmaska is null))
                and (towary.grupa = :grupa or (:grupa = '') or (:grupa is null))
            into :ktm, :wersjaref, :jedn, :przelicz, :staracenanet, :staramarza,
                 :starynarzut, :staryprmies
        do begin
          execute procedure CENNIK_OBLICZREG(:ktm, :wersjaref, :tryb, :zrodlo, :wartosc, :przelicz, :zrodlomarzy,
                                             :kurs, :STARACENANET, 0, :tolerancja, 1, :magazyn, :cenniknad)
            returning_values :nowacenanet,:nowacenabru,:nowamarza,:nowynarzut;
          if(:nowacenanet<>:staracenanet or (:nowamarza<>:staramarza) or (:nowynarzut<>:starynarzut) or (:prmies<>:staryprmies) ) then begin
            update cennik set marza = :nowamarza, narzut = :nowynarzut, cenanet = :nowacenanet, cenabru = :nowacenabru, CENAPNET = CENANET, CENAPBRU = CENABRU, PROCENTMIES=:prmies
              where CENNIK = :cennik
                and WERSJAREF = :wersjaref
                and JEDN=:jedn;
            ileprzel = :ileprzel+1;
          end
        end
      end
      if(:dodaj>0) then begin
        /* dodaj nowe pozycje do cennika */
        for select WERSJE.KTM,WERSJE.REF
              from WERSJE left join CENNIK on (CENNIK.wersjaref = WERSJE.REF and cennik.cennik = :CENNIK)
              where (cennik.cennik is null)
                and (WERSJE.KTM >=:odktm or (:odktm = '') or (:odktm is null))
                and (WERSJE.KTM <= :doktm or (:doktm = '') or (:doktm is null))
                and (WERSJE.KTM like :ktmmaska or (:ktmmaska = '') or (:ktmmaska is null))
                and (WERSJE.grupa = :grupa or (:grupa = '') or (:grupa is null))
                and WERSJE.AKT=1
            into :ktm, :wersjaref
        do begin
          execute procedure CENNIK_OBLICZREG(:ktm, :wersjaref, :tryb, :zrodlo, :wartosc, 1, :zrodlomarzy, :kurs, 0, 0,
                                             :tolerancja, 0, :magazyn, :cenniknad)
            returning_values :nowacenanet,:nowacenabru,:nowamarza,:nowynarzut;
          if(:nowacenanet<>0) then begin
            insert into CENNIK(CENNIK,WERSJAREF,CENANET,CENABRU,STALACENA,MARZA,NARZUT, PROCENTMIES)
              values (:cennik,:wersjaref,:nowacenanet,:nowacenabru,:defstalacena,:nowamarza,:nowynarzut,:prmies);
            iledod = :iledod+1;
          end
        end
      end
    end
  end
  STATUS = 1;
end^
SET TERM ; ^
