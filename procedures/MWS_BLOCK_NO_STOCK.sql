--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_BLOCK_NO_STOCK(
      LOT smallint,
      WH DEFMAGAZ_ID)
  returns (
      GOOD KTM_ID,
      VERS integer,
      QUANTITY ILOSCI_MAG,
      RESERVED ILOSCI_MAG,
      BLOCKED ILOSCI_MAG,
      ORDERED ILOSCI_MAG,
      QUANTITYSTOCK ILOSCI_MAG)
   as
declare variable altposmode smallint;
begin
  if (:lot is null) then lot = 0;
  if (:lot = 0) then
  begin
    for
      select s.ktm, s.wersjaref, s.ilosc, s.zarezerw, s.zablokow, s.zamowiono,
          coalesce(t.altposmode,0)
        from stanyil s
          join towary t on (s.ktm = t.ktm)
        where s.magazyn = :wh
      into :good, :vers, :quantity, :reserved, :blocked, :ordered, :altposmode
    do begin
      if (:altposmode = 0) then
        select sum(v.quantity)
          from VIEW_MWS_QUANTITY v
          where v.vers = :vers
            and v.wh = :wh
        into :quantitystock;
      else
        execute procedure MWS_CALC_KPL_QUANTITY(:wh, :vers, :lot)
          returning_values :quantitystock;
      if (:quantitystock is null) then quantitystock = 0;
      suspend;
      quantity = 0;
      blocked = 0;
      quantitystock = 0;
    end
  end
  else
  begin
    for
      select s.ktm, s.wersjaref, s.ilosc, s.zarezerw, s.zablokow, s.zamowiono,
          coalesce(t.altposmode,0)
        from stanycen s
          join towary t on (s.ktm = t.ktm)
        where s.magazyn = :wh
          and s.dostawa = :lot
      into :good, :vers, :quantity, :reserved, :blocked, :ordered, :altposmode
    do begin
      if (:altposmode = 0) then
        select sum(v.quantity)
          from VIEW_MWS_LOT_QUANTITY v
          where v.vers = :vers
            and v.wh = :wh
            and v.lot = :lot
        into :quantitystock;
      else
        execute procedure MWS_CALC_KPL_QUANTITY(:wh, :vers, :lot)
          returning_values :quantitystock;
      if (:quantitystock is null) then quantitystock = 0;
      suspend;
      quantity = 0;
      blocked = 0;
      quantitystock = 0;
    end
  end
end^
SET TERM ; ^
