--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_CHECK_MIEDZYWART(
      DOKUMENT integer)
  returns (
      STATUS integer)
   as
declare variable ref2 integer;
declare variable wartosc numeric(14,2);
declare variable wartosc2 numeric(14,2);
declare variable symbol varchar(40);
declare variable symbol2 varchar(40);
begin
  status = 1;
  select REF2,WARTOSC,WARTOSC2,SYMBOL,SYMBOL2
  from DOKUMNAG where REF=:dokument
  into :ref2,:wartosc,:wartosc2,:symbol,:symbol2;
  if(:ref2 is not null and :wartosc<>wartosc2) then begin
    status = 0;
    exception DOKUMNAG_MIEDZYWART 'Dokumenty '||:symbol||' i '||:symbol2||' mają różne wartosci.';
  end
end^
SET TERM ; ^
