--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ALLOW_COMPANY_PATTERN_OFF(
      COMPANY COMPANIES_ID)
  returns (
      ALLOW SMALLINT_ID)
   as
declare variable patt_ref bkyears_id;
begin
  -- Procedura sprawzdająca czy dane company moze przestać być wzorcem
  -- W tym celu należy sprawdzić, czy nie istnieją dowiązania pattern_ref
  -- na to company

  for
    select b.ref
      from bkyears b
      where b.company = :company
      into :patt_ref
  do begin
    if (exists (select first 1 1 from bkyears byr where byr.patternref = :patt_ref) ) then
    begin
      allow = 0;
      break;
    end
  end
  suspend;
end^
SET TERM ; ^
