--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LOGIN_USER(
      APPLICATION varchar(255) CHARACTER SET UTF8                           ,
      USERID integer,
      MAXNUM integer)
  returns (
      STATUS integer)
   as
declare variable cnt integer;
declare variable sente smallint;
begin
  -- procedura nie jest wykorzystywana w wyniku PR44559
  cnt = 0;
  status = 0;
  select sente from s_users where userid = :userid into :sente;
  if(:sente=1) then begin
    status = 1;
    exit;
  end
  if(:maxnum>=0) then begin
    select count(*) from s_sessions where application=:application and number=0 and connectionid<>current_connection into :cnt;
    if(:cnt>=:maxnum) then exit;
  end
  update or insert into s_sessions(application,userid,number,connectionid) values (:application,:userid,0,current_connection)
  matching (connectionid);
  status = 1;
end^
SET TERM ; ^
