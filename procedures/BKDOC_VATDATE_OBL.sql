--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BKDOC_VATDATE_OBL(
      OTABLE varchar(30) CHARACTER SET UTF8                           ,
      OREF integer,
      DATADOK timestamp,
      TERMPLAT timestamp,
      COMPANY integer,
      VATFUNCREF integer)
  returns (
      VATPERIOD PERIOD_ID,
      VATDATE TIMESTAMP_ID)
   as
declare variable PROCNAME PROCEDURE_NAME;
declare variable S varchar(1024);
begin
/*PL: Procedura okresla na podstawie VATFUNCREF jaka procedure wyznaczajaca
      date vat wykorzystac, nastepnie na jej podstawie okresla okres vat
      i zwraca obydwie wartosci.
      OREF jest REFem na tabele przekazana przez OTABLE
      OTABLE jest nazwa tabeli
      DATADOK oznacza date dokumentu(np. DATA z NAGFAK)
      TERMPLAT oznacza date platnosci(np. termplat z NAGFAK)
      VATFUNCREF jest REFem ze slownika DICTVATFUNCTIONS
      (PR57428)
*/

  vatdate = null;
  vatperiod = null;
  select procedurename
    from dictvatfunctions
    where ref = :vatfuncref
  into procname;
  if(procname is not null) then
  begin
    s = 'select vatdate from '||procname||' (';
    if(:otable is null) then s = s || 'NULL,';
    else s = s || '''' || :otable || ''',';
    if(:oref is null) then s = s || 'NULL,';
    else s = s || :oref || ',';
    if(:datadok is null) then s = s || 'NULL,';
    else s = s || '''' || cast(:datadok as date) || ''',';
    if(:termplat is null) then s = s || 'NULL)';
    else s = s || '''' || cast(:termplat as date) || ''')';
    execute statement s into vatdate;
    if(vatdate is not null) then
      select ID
        from BKPERIODS
        where company = :company
          and SDATE <= :vatdate
          and FDATE >=:vatdate
          and PTYPE=1
      into vatperiod;
  end
  suspend;
end^
SET TERM ; ^
