--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_GET_NAVIGATOR(
      NAMESPACE varchar(40) CHARACTER SET UTF8                           ,
      ITEMREF varchar(255) CHARACTER SET UTF8                            = null,
      ONLYIDENT varchar(255) CHARACTER SET UTF8                            = null)
  returns (
      REF varchar(255) CHARACTER SET UTF8                           ,
      PARENT varchar(255) CHARACTER SET UTF8                           ,
      PREV varchar(255) CHARACTER SET UTF8                           ,
      NUMBER smallint,
      NAME varchar(80) CHARACTER SET UTF8                           ,
      HINT varchar(255) CHARACTER SET UTF8                           ,
      KINDSTR varchar(20) CHARACTER SET UTF8                           ,
      ACTIONID varchar(60) CHARACTER SET UTF8                           ,
      APPLICATIONS varchar(60) CHARACTER SET UTF8                           ,
      FORMODULES varchar(60) CHARACTER SET UTF8                           ,
      RIGHTS varchar(255) CHARACTER SET UTF8                           ,
      RIGHTSGROUP varchar(255) CHARACTER SET UTF8                           ,
      ISNAVBAR smallint,
      ISTOOLBAR smallint,
      ISWINDOW smallint,
      SIGNATURE varchar(80) CHARACTER SET UTF8                           ,
      ITEMTYPE smallint,
      GROUPPROCEDURE varchar(40) CHARACTER SET UTF8                           ,
      RIBBONTAB varchar(40) CHARACTER SET UTF8                           ,
      RIBBONGROUP varchar(40) CHARACTER SET UTF8                           ,
      GLOBALVALUE varchar(40) CHARACTER SET UTF8                           ,
      POPUPMENU varchar(40) CHARACTER SET UTF8                           ,
      FORADMIN smallint,
      SHORTCUT varchar(20) CHARACTER SET UTF8                           ,
      GLOBALSHORTCUT smallint,
      GLOBALCONTEXT varchar(20) CHARACTER SET UTF8                           ,
      BEGINGROUP smallint,
      GLOBALSUFFIX varchar(40) CHARACTER SET UTF8                           ,
      CHANGEDFIELDS varchar(1024) CHARACTER SET UTF8                           ,
      ISEHRM smallint,
      EHRMLINK varchar(255) CHARACTER SET UTF8                           )
   as
declare variable CURRLAYER integer;
declare variable SECTION varchar(255);
declare variable IDENT varchar(255);
declare variable VAL varchar(1024);
declare variable OLDSECTION varchar(255);
declare variable LAYER integer;
declare variable COMPANY COMPANIES_ID;
declare variable PATTERN SMALLINT_ID;
begin
  --Ściągam currentcompany, żeby wiedzieć do jakiego company sie loguje
  execute procedure get_global_param('CURRENTCOMPANY')
    returning_values :company;
  --Szukam czy company nie jest wzorcowym, dla danej grupy company dzieci  PR60327
  select coalesce(c.pattern,0)
    from companies c
    where c.ref = :company
    into :pattern;

  if(:namespace is null or :namespace='') then namespace = 'SENTE';
  select layer from s_namespaces where namespace=:namespace into :currlayer;
  oldsection = '';
  changedfields = '';
  if(:itemref is null) then itemref = '';
  if(:onlyident is null) then onlyident = '';
  for select n.layer, s.section, s.ident, s.val
  from s_appini s
  left join s_namespaces n on (n.namespace=s.namespace)
  where s.section starting with 'Navigator:'||:itemref
  and n.layer<=:currlayer
  and (:onlyident = '' or :onlyident like '%'||s.ident||'%')
  order by s.section, n.layer desc
  into :layer, :section, :ident, :val
  do begin
    -- jesli jest nowy item to zwroc rekord wynikowy
    if(:section<>:oldsection) then begin
      -- jesli to kolejny item to zwroc biezacy wynik
      if(:parent='') then parent = NULL;
      --Sprawdzam, czy company wzorcowe - jeżeli tak, to laduje sama konfiguracje PR60327
      --Projektowane na potrzeby GNS

      --Na potrzeby GNS wyremowane, ale warunek musi wrócić !
      if(:oldsection<>'' /*and ((pattern = 1 and foradmin = 1) or (pattern = 0))*/) then suspend;
      oldsection = :section;
      changedfields = '';
      ref = NULL; parent = NULL; prev = NULL; number = NULL; name = NULL; hint = NULL; kindstr = NULL; actionid = NULL;
      applications = NULL; formodules = NULL; rights = NULL; rightsgroup = NULL;
      isnavbar = NULL; istoolbar = NULL; iswindow = NULL; signature = NULL;
      itemtype = NULL; groupprocedure = NULL; ribbontab = NULL; ribbongroup = NULL;
      globalvalue = NULL; popupmenu = NULL; foradmin = NULL; shortcut = NULL; globalshortcut = NULL; 
      globalcontext = NULL; begingroup = NULL; globalsuffix = NULL; isehrm = NULL; ehrmlink = NULL;
    end
    -- zbieraj kolejne wartosci pol
    if(:ident = 'REF' and :ref is NULL) then REF = :val;
    if(:ident = 'PARENT' and :parent is NULL) then PARENT = :val;
    if(:ident = 'PREV' and :prev is NULL) then PREV = :val;
    if(:ident = 'NUMBER' and :number is NULL and :val<>'') then NUMBER = :val;
    if(:ident = 'Signature' and :signature is NULL) then SIGNATURE = :val;
    if(:ident = 'NAME' and :name is NULL) then NAME = :val;
    if(:ident = 'HINT' and :hint is NULL) then HINT = :val;
    if(:ident = 'KINDSTR' and :kindstr is NULL) then KINDSTR = :val;
    if(:ident = 'ACTIONID' and :actionid is NULL) then ACTIONID = :val;
    if(:ident = 'APPLICATIONS' and :applications is NULL) then APPLICATIONS = :val;
    if(:ident = 'FORMODULES' and :formodules is NULL) then FORMODULES = :val;
    if(:ident = 'RIGHTS' and :rights is NULL) then RIGHTS = :val;
    if(:ident = 'RIGHTSGROUP' and :rightsgroup is NULL) then RIGHTSGROUP = :val;
    if(:ident = 'ISNAVBAR' and :isnavbar is NULL and :val<>'') then ISNAVBAR = :val;
    if(:ident = 'ISTOOLBAR' and :istoolbar is NULL and :val<>'') then ISTOOLBAR = :val;
    if(:ident = 'ISWINDOW' and :iswindow is NULL and :val<>'') then ISWINDOW = :val;
    if(:ident = 'ITEMTYPE' and :itemtype is NULL and :val<>'') then ITEMTYPE = :val;
    if(:ident = 'GROUPPROCEDURE' and :groupprocedure is NULL) then GROUPPROCEDURE = :val;
    if(:ident = 'RIBBONTAB' and :ribbontab is NULL) then RIBBONTAB = :val;
    if(:ident = 'RIBBONGROUP' and :ribbongroup is NULL) then RIBBONGROUP = :val;
    if(:ident = 'GLOBALVALUE' and :globalvalue is NULL) then GLOBALVALUE = :val;
    if(:ident = 'POPUPMENU' and :popupmenu is NULL) then POPUPMENU = :val;
    if(:ident = 'FORADMIN' and :foradmin is NULL and :val<>'') then FORADMIN = :val;
    if(:ident = 'SHORTCUT' and :shortcut is NULL) then SHORTCUT = :val;
    if(:ident = 'GLOBALSHORTCUT' and :globalshortcut is NULL and :val<>'') then GLOBALSHORTCUT = :val;
    if(:ident = 'GLOBALCONTEXT' and :globalcontext is NULL) then GLOBALCONTEXT = :val;
    if(:ident = 'BEGINGROUP' and :begingroup is NULL and :val<>'') then BEGINGROUP = :val;
    if(:ident = 'GLOBALSUFFIX' and :globalsuffix is NULL) then GLOBALSUFFIX = :val;
    if(:ident = 'ISEHRM' and :isehrm is NULL and :val<>'') then ISEHRM = :val;
    if(:ident = 'EHRMLINK' and :ehrmlink is NULL) then EHRMLINK = :val;

    -- ustal liste changedfields
    if(:layer=:currlayer and :currlayer>0) then begin
      if(:changedfields<>'') then changedfields = :changedfields || ';';
      changedfields = :changedfields || :ident;
    end
  end
  if(:parent='') then parent = NULL;
  --Sprawdzam, czy company wzorcowe - jeżeli tak, to laduje sama konfiguracje PR60327
  --Projektowane na potrzeby GNS

  --Na potrzeby GNS wyremowane, ale warunek musi wrócić !
  if(:oldsection<>''/* and ((pattern = 1 and foradmin = 1) or (pattern = 0))*/) then suspend;
end^
SET TERM ; ^
