--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_KARDEX_POS(
      SYMBOL STRING100)
  returns (
      KARDEX_OPT SMALLINT_ID)
   as
declare variable a numeric (14,4);
declare variable b numeric (14,4);
begin
-- Na podstawie symbolu K1-03-01 zwraca dostepne pozycje(glebokosc)
  select count(*)
    from mwsconstlocs m
    where m.symbol like :symbol||'%'
  into :kardex_opt;

  a = 8;
  b = 24;
  if (a >= b) then
    kardex_opt = 1;
end^
SET TERM ; ^
