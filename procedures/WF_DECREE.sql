--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WF_DECREE(
      WFWORKFLOW integer)
  returns (
      BKDOC integer)
   as
declare variable PERIOD varchar(6);
declare variable PAYDAY timestamp;
declare variable DICTDEF integer;
declare variable DICTPOS integer;
declare variable DOKPLIK integer;
declare variable SYMBOL varchar(20);
begin
  select (select x.okres from datatookres(substring(cast(d.data as varchar(255)) from 1 for 10),0,0,0,1) x), d.paramd1, c.slodef, c.slopoz, d.ref, substring(d.symbol from 1 for 20)
    from wfworkflows w
    join dokzlacz z on z.ztable = 'WFWORKFLOWS' and z.zdokum = w.ref
    join dokplik d on d.ref = z.dokument
    left join cpodmioty c on c.ref = d.cpodmiot
    where w.ref = :wfworkflow
  into :period, :payday, :dictdef, :dictpos, :dokplik, :symbol;
  select b.ref from bkdocs b where b.otable = 'WFWORKFLOWS' and b.oref = :wfworkflow into :bkdoc;
  if(bkdoc is null) then begin
    execute procedure GEN_REF('BKDOCS') returning_values :bkdoc;
    insert into bkdocs(ref,period,company,bkreg,doctype,payday,dictdef,dictpos,otable,oref,vatreg, transdate,symbol)
      values (:bkdoc, :period,1,'ZAK. KOSZ.',49, :payday, :dictdef, :dictpos, 'WFWORKFLOWS',:wfworkflow,'ZAK. KOSZ.',current_date, :symbol);
    insert into dokzlacz(ztable,zdokum,dokument) values('BKDOCS',:bkdoc,:dokplik);
  end
  suspend;
end^
SET TERM ; ^
