--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_CREATE_DOKMAG(
      FAK integer,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      TYPMAG varchar(3) CHARACTER SET UTF8                           ,
      TYPMAGK varchar(3) CHARACTER SET UTF8                           ,
      TYPMAGO varchar(3) CHARACTER SET UTF8                           ,
      TYPMAGKO varchar(3) CHARACTER SET UTF8                           ,
      DATA timestamp,
      AKCEPT smallint,
      HASDOK smallint,
      BLOCKMASTER integer)
  returns (
      STATUS integer,
      DOKSYM varchar(20) CHARACTER SET UTF8                           )
   as
declare variable NUMER integer;
declare variable KTM varchar(40);
declare variable WERSJA integer;
declare variable CENA numeric(14,4);
declare variable WYDANIA integer;
declare variable TYPDOK varchar(3);
declare variable ILOSC numeric(14,4);
declare variable ILOSCO numeric(14,4);
declare variable ILSTAN numeric(14,4);
declare variable DOSTAWA integer;
declare variable SYMBOL varchar(30);
declare variable KOREKTA integer;
declare variable ZEWN integer;
declare variable KORYG integer;
declare variable ZRODLO smallint;
declare variable AKCEPTACJA integer;
declare variable ZAKUP integer;
declare variable TYPMAGAZ char(1);
declare variable DOSTAWCA integer;
declare variable IDDOSTAWCY varchar(20);
declare variable CENAMAG numeric(14,4);
declare variable PCENAMAG numeric(14,4);
declare variable ILMAG numeric(14,4);
declare variable CENADOK numeric(14,4);
declare variable DOSTMAG integer;
declare variable ILMAGK numeric(14,4);
declare variable ILMAGO numeric(14,4);
declare variable ILMAGKO numeric(14,4);
declare variable ILPZNS numeric(14,4);
declare variable ILPZNSO numeric(14,4);
declare variable CENAPZNS numeric(14,4);
declare variable DOSTPZNS integer;
declare variable ILZPZNS numeric(14,4);
declare variable ILZPZNSO numeric(14,4);
declare variable CENAZPZNS numeric(14,4);
declare variable DOSTZPZNS integer;
declare variable MAGSTAN numeric(14,4);
declare variable PILOSC numeric(14,4);
declare variable PILOSCO numeric(14,4);
declare variable ILOSCFAKP numeric(14,4);
declare variable MAGWARTOSC numeric(14,2);
declare variable DOKMAG integer;
declare variable DOKMAGK integer;
declare variable DOKMAGO integer;
declare variable DOKMAGKO integer;
declare variable LDOK integer;
declare variable LDOKTYP varchar(3);
declare variable SERIALIZED smallint;
declare variable DOKPZNS integer;
declare variable DOKZPZNS integer;
declare variable DOKPZNSO integer;
declare variable DOKZPZNSO integer;
declare variable WZSER smallint;
declare variable ZZSER smallint;
declare variable WZBREAK smallint;
declare variable POZREF integer;
declare variable POZREFORG integer;
declare variable DOKPOZREF integer;
declare variable SERIALIL numeric(14,4);
declare variable TOSERIALIL numeric(14,4);
declare variable AUTOSERROZPISZ smallint;
declare variable I numeric(14,4);
declare variable WERSJAREF integer;
declare variable DOKMAGBLOK smallint;
declare variable DOSTNOTSER varchar(255);
declare variable NOTSER smallint;
declare variable JEDN integer;
declare variable PRZELICZ numeric(14,4);
declare variable FROMZAM integer;
declare variable OPK smallint;
declare variable ODDZIAL varchar(20);
declare variable AKTUODDZIAL varchar(20);
declare variable PCENACEN numeric(14,4);
declare variable PCENANET numeric(14,4);
declare variable PCENABRU numeric(14,4);
declare variable PWARTNET numeric(14,2);
declare variable PWARTBRU numeric(14,2);
declare variable CENACEN numeric(14,4);
declare variable CENANET numeric(14,4);
declare variable CENABRU numeric(14,4);
declare variable WARTNET numeric(14,2);
declare variable WARTBRU numeric(14,2);
declare variable WALCEN varchar(3);
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable KLIENT integer;
declare variable SPOSDOST integer;
declare variable TRASADOST integer;
declare variable TERMDOST timestamp;
declare variable LISTADOST integer;
declare variable LISTADKODP varchar(10);
declare variable FAKTRANS integer;
declare variable DOKTRANS integer;
declare variable POZTOKOR integer; /* ref poz. dok. mag, do korekty ilosciowej */
declare variable POZTONOT integer; /* ref. poz. dok. mag do noty magazynowej */
declare variable DOKTOKOR integer;
declare variable NOTDOK integer; /* naglowek noty magazynowej */
declare variable LISTADULICA varchar(255);
declare variable LISTADNRDOMU nrdomu_id;
declare variable LISTADNRLOKALU nrdomu_id;
declare variable LISTADMIASTO varchar(255);
declare variable ZAM integer;
declare variable RABAT numeric(14,4);
declare variable RABATNAG numeric(14,4);
declare variable RABATTAB numeric(14,4);
declare variable FROMDOKPOZ integer;
declare variable ANULOWANIE smallint;
declare variable TILOSC numeric(14,4);
declare variable TILOSCM numeric(14,4);
declare variable TILOSCO numeric(14,4);
declare variable BLOCKREF integer;
declare variable NUMBER_GEN varchar(50);
declare variable DATAWAZN timestamp;
declare variable LOCATION varchar(255);
declare variable AKTULOCAT varchar(255);
declare variable KOSZTDOSTPLAT integer;
declare variable KOSZTDOSTBEZ integer;
declare variable KOSZTDOSTROZ integer;
declare variable ODBIORCAID integer;
declare variable CNT integer;
declare variable SPOSZAP integer;
declare variable DNIZAP integer;
declare variable ZAMDEPEND integer;
declare variable GRUPADOK integer;
declare variable FROMPOZZAM integer;
declare variable FLAGI varchar(20);
declare variable NIENOTYKORZAK varchar(20);
declare variable NR integer;
declare variable SPRZEDAWCA integer;
declare variable SPRZEDAWCAZEW integer;
declare variable ILZAMDEPEND numeric(14,4);
declare variable KOSZTDOST numeric(14,2);
declare variable MAGWARTNET numeric(14,2);
declare variable TOWWARTNET numeric(14,2);
declare variable CKONTRAKTY integer;
declare variable KURS numeric(14,4);
declare variable TABKURS integer;
declare variable CENAWWAL numeric(14,4);
declare variable WALUTA varchar(3);
declare variable WARTOSCWAL numeric(14,2);
declare variable SYMBFAK varchar(80);
declare variable DATANOTY timestamp;
declare variable PARAMN1 numeric(14,4);
declare variable PARAMN2 numeric(14,4);
declare variable PARAMN3 numeric(14,4);
declare variable PARAMN4 numeric(14,4);
declare variable PARAMN5 numeric(14,4);
declare variable PARAMN6 numeric(14,4);
declare variable PARAMN7 numeric(14,4);
declare variable PARAMN8 numeric(14,4);
declare variable PARAMN9 numeric(14,4);
declare variable PARAMN10 numeric(14,4);
declare variable PARAMN11 numeric(14,4);
declare variable PARAMN12 numeric(14,4);
declare variable PARAMS1 varchar(255);
declare variable PARAMS2 varchar(255);
declare variable PARAMS3 varchar(255);
declare variable PARAMS4 varchar(255);
declare variable PARAMS5 varchar(255);
declare variable PARAMS6 varchar(255);
declare variable PARAMS7 varchar(255);
declare variable PARAMS8 varchar(255);
declare variable PARAMS9 varchar(255);
declare variable PARAMS10 varchar(255);
declare variable PARAMS11 varchar(255);
declare variable PARAMS12 varchar(255);
declare variable PARAMD1 timestamp;
declare variable PARAMD2 timestamp;
declare variable PARAMD3 timestamp;
declare variable PARAMD4 timestamp;
declare variable KRAJWYSYLKI varchar(20);
declare variable KRAJPOCHODZENIA varchar(20);
declare variable TEMPILOSC numeric(14,4);
declare variable OPERAKCEPT integer;
declare variable SPOSDOSTAUTO integer;
declare variable DOKUMPOZ_COUNT integer;
declare variable REFPARPRO integer;
declare variable CNTPARPRO integer;
declare variable SRVREQUEST integer;
declare variable PREC smallint;
declare variable GR_VAT varchar(5);
declare variable ALTTOPOZ POZZAM_ID;
declare variable FAKE smallint;
declare variable ALTTOPOZN DOKUMPOZ_ID;
begin
  /* TODO Kuba: uwzglednic pole POZFAK.CENACEN podczas generowania dok. mag.*/
  /* sprawdzenie typu dok. mag. z typem dok. sprzedazy */
  select TYPFAK.KOREKTA, NAGFAK.AKCEPTACJA, NAGFAK.ZAKUP, NAGFAK.KLIENT, NAGFAK.DOSTAWCA,
     NAGFAK.SPOSDOST, NAGFAK.SPOSDOSTAUTO, NAGFAK.TRASADOST, NAGFAK. TERMDOST, NAGFAK.wysylka,
     NAGFAK.DULICA, NAGFAK.DNRDOMU, NAGFAK.DNRLOKALU, NAGFAK.DMIASTO,
     NAGFAK.sposzap, nagfak.termin, nagfak.rabat,nagfak.kosztdost,
     NAGFAK.kosztdostbez, NAGFAK.kosztdostroz, NAGFAK.kosztdostplat, nagfak.odbiorcaid, nagfak.dkodp,
     TYPFAK.TRANSPORT, nAGFAK.anulowanie, NAGFAK.grupadok, NAGFAK.sprzedawca, NAGFAK.sprzedawcazew, NAGFAK.ckontrakty,
     NAGFAK.KURS, NAGFAK.WALUTA, nagfak.tabkurs, NAGFAK.sumwartnet- nagfak.psumwartnet,
     NAGFAK.SYMBFAK, NAGFAK.KRAJWYSYLKI,NAGFAK.operakcept,NAGFAK.srvrequest
  from TYPFAK join NAGFAK on ( typfak.SYMBOL = NAGFAK.TYP)
  where NAGFAK.REF =:fak
  into :korekta, :akceptacja, :zakup, :klient, :dostawca,
  :sposdost, :sposdostauto, :trasadost, :termdost, :listadost,
  :listadulica, :listadnrdomu, :listadnrlokalu, :listadmiasto,
  :sposzap, :dnizap, :rabatnag,:kosztdost,
  :kosztdostbez, :kosztdostroz, :kosztdostplat, :odbiorcaid, :listadkodp,
  :faktrans, :anulowanie, :grupadok, :sprzedawca, :sprzedawcazew, :ckontrakty,
  :kurs, :waluta, :tabkurs, wartoscwal,
  :symbfak, :krajwysylki, :operakcept, :srvrequest;
  /* przeliczenie udzialu kosztu zakupu na wskazany magazyn przed naniesieniem na PZ*/
  if(:zakup=1) then begin
    select sum(WARTNET-PWARTNET) from POZFAK
    where POZFAK.DOKUMENT = :FAK and POZFAK.USLUGA <>1 and POZFAK.MAGAZYN = :magazyn
    into :magwartnet;
    select sum(WARTNET-PWARTNET) from POZFAK
    where POZFAK.DOKUMENT = :FAK and POZFAK.USLUGA <>1
    into :towwartnet;
    if(:towwartnet<>0) then
      kosztdost = :kosztdost*(:magwartnet/:towwartnet);
    else
      kosztdost = 0;
  end else kosztdost = 0;

  if(:korekta = 1) then begin
    select ref from NAGZAM where FAKTURA = :grupadok and FAKDEPEND = 1 into :zamdepend;
  end
  if(:hasdok is null) then hasdok = 0;
  if(:anulowanie is null) then anulowanie = 0;
  if(:faktrans <> 1 or (:faktrans is null)) then begin
    sposdost = null;
    trasadost = null;
    listadost = null;
    termdost = null;
    sposdostauto = 0;
  end
  execute procedure GETCONFIG('DOSTNOTSER') returning_values :dostnotser;
  select ODDZIAL from DEFMAGAZ where symbol = :magazyn into :oddzial;
  select LOCATION from ODDZIALY where ODDZIAL=:oddzial into :location;
  execute procedure GETCONFIG('AKTUODDZIAL') returning_values :aktuoddzial;
  execute procedure GETCONFIG('AKTULOCAT') returning_values :aktulocat;
  execute procedure GETCONFIG('NIENOTYKORZAK') returning_values :nienotykorzak;
  if(:nienotykorzak is null) then nienotykorzak = '';
  if(:location <> :aktulocat)then exception NAGFAK_DOKMAG_WRONGLOCATION;
/*  if(:oddzial <> :aktuoddzial)then exception NAGFAK_DOKMAG_WRONGODDZIAL;*/
  if(:dostnotser <> '1') then notser = 0;
  else notser = 1;
  if(:akceptacja is null) then akceptacja = 0;
  if(:akceptacja = 0) then exception FAK_DOK_CREATE_NOACK;
  if(:akcept is null) then akcept = 0;
  if(:korekta is null) then korekta = 0;
  if(:akcept > 1) then zrodlo = 3;
  else zrodlo = 2;
  if(:klient > 0) then begin
    select REF from SLODEF where TYP = 'KLIENCI' into :slodef;
    slopoz = :klient;
  end else begin
    select REF from SLODEF where TYP = 'DOSTAWCY' into :slodef;
    slopoz = :dostawca;
  end
  select TYP from DEFMAGAZ where SYMBOL=:MAGAZYN into :typmagaz;
  nr = 1;
  for select POZFAK.KTM, POZFAK.WERSJA, POZFAK.NUMER,
    POZFAK.ILOSCm,POZFAK.PILOSCm, POZFAK.ILOSCO, POZFAK.PILOSCO, POZFAK.JEDNO, POZFAK.CENAMAG, POZFAK.PCENAMAG,
    POZFAK.CENACEN, POZFAK.cenanetzl, POZFAK.CENABRUZL, POZFAK.wartnetZL, POZFAK.wartbruzl,
    POZFAK.PCENACEN, POZFAK.PCENANET,POZFAK.pcenabruzl, POZFAK.pwartnetzl, POZFAK.pwartbruzl,
    POZFAK.SERIALIZED, POZFAk.REF, POZFAK.WERSJAREF, POZFAK.FROMZAM, POZFAK.OPK, POZFAK.walcen, POZFAK.RABAT,
    POZFAK.RABATTAB, POZFAK.fromdokumpoz, POZFAK.FROMZAM, POZFAK.DATAWAZN, POZFAK.flagi, POZFAK.CENANET,
    POZFAK.PARAMN1, POZFAK.PARAMN2, POZFAK.PARAMN3, POZFAK.PARAMN4,
    POZFAK.PARAMN5, POZFAK.PARAMN6, POZFAK.PARAMN7, POZFAK.PARAMN8,
    POZFAK.PARAMN9, POZFAK.PARAMN10, POZFAK.PARAMN11, POZFAK.PARAMN12,
    POZFAK.PARAMS1, POZFAK.PARAMS2, POZFAK.PARAMS3, POZFAK.PARAMS4,
    POZFAK.PARAMS5, POZFAK.PARAMS6, POZFAK.PARAMS7, POZFAK.PARAMS8,
    POZFAK.PARAMS9, POZFAK.PARAMS10, POZFAK.PARAMS11, POZFAK.PARAMS12,
    POZFAK.PARAMD1, POZFAK.PARAMD2, POZFAK.PARAMD3, POZFAK.PARAMD4,
    POZFAK.KRAJPOCHODZENIA, POZFAK.prec, POZFAK.gr_vat, POZFAK.fake, POZFAK.alttopoz
    from POZFAK
    where POZFAK.DOKUMENT = :FAK and (POZFAK.USLUGA <>1 or POZFAK.OPK>0) and POZFAK.MAGAZYN = :magazyn
    order by POZFAK.NUMER, POZFAK.alttopoz nulls first
    into :ktm, :wersja, :numer, :ilosc, :pilosc, :ilosco, :pilosco, :jedn, :cenamag, :pcenamag,
         :cenacen, :cenanet, :cenabru, :wartnet, :wartbru,
         :pcenacen, :pcenanet, :pcenabru, :pwartnet, :pwartbru,
         :serialized, :pozref, :wersjaref, :fromzam, :opk, :walcen, :rabat,
         :rabattab, :fromdokpoz, :zam, :datawazn, :flagi, :cenawwal,
         :paramn1, :paramn2, :paramn3, :paramn4,
         :paramn5, :paramn6, :paramn7, :paramn8,
         :paramn9, :paramn10, :paramn11, :paramn12,
         :params1, :params2, :params3, :params4,
         :params5, :params6, :params7, :params8,
         :params9, :params10, :params11, :params12,
         :paramd1, :paramd2, :paramd3, :paramd4,
         :krajpochodzenia, :prec, :gr_vat, :fake, :AltToPoz
  do begin
    if(:opk is null) then opk = 0;
    if(:serialized = 1) then begin
      select TOWARY.serautowyd from TOWARY where KTM = :ktm into :autoserrozpisz;
    end
    /*jesli anulowanie, to zamina kierunku obrotu*/
    if(:ANULOWANIE<>0) then begin
      tilosc = :ilosc;
      tilosco = :ilosco;
      ilosc = :pilosc;
      ilosco = :pilosco;
      pilosc = :tilosc;
      pilosco = :tilosco;
    end
    if(:zamdepend > 0 and :pilosc > :ilosc) then begin
      /*okrelenie, jaka iloć zostala rzeczywicie wydana, jesli mniejsza niz :ilosc, to :pilosc = :ilosc*/
      select POZZAM.ilzreal from POZZAM join POZFAK on (POZFAK.frompozzam = pozzam.ref)
      where POZFAK.DOKUMENT = :grupadok and POZFAK.numer = :numer into :ilzamdepend;
      if(:ilzamdepend < :pilosc) then begin
        if(:ilzamdepend > :ilosc) then
          pilosc = :ilzamdepend;
        else
          pilosc= :ilosc;
      end
    end
    -- odnalezienie ilosci z juz powiazanych dokumentow magazynowych
    -- i ewentualne wiazanie naglowkow
    iloscfakp = 0;
    if(:anulowanie=0) then begin
      for select ref,ilosc,iloscm from POZFAK_GET_DOKUMPOZ(:pozref) into :dokpozref,:tilosc,:tiloscm
      do begin
        if(:tilosc is null) then tilosc = 0;
        if(:tiloscm is null) then tiloscm = 0;
        iloscfakp = :iloscfakp + :tilosc - :tiloscm;
        select dokument from dokumpoz where ref=:dokpozref into :ldok;
        update dokumnag set faktura=:fak where ref=:ldok and faktura is null;
      end
    end
    if(:iloscfakp is null) then iloscfakp = 0;
    /*okrelenie typu operacji*/
    przelicz = null;
    select PRZELICZ from TOWJEDN where REF=:jedn into :przelicz;
    if(:przelicz is null)then przelicz = 0;
    if(:przelicz = 0) then przelicz = 1;
    pozreforg = null;
    poztokor = null;
    doktokor = null;
    ilmag = 0; ilmago = 0;cenadok = null;dostmag = null;
    ilmagk = 0; ilmagko = 0;
/*    ilpzns = 0; ilpznso = 0; cenapzns = null;dostpzns = null;
    ilzpzns = 0; ilzpznso = 0; cenazpzns = null; dostzpzns = null;*/
    wzser = :notser;
    zzser =:notser;
    wzbreak = 0;
/*    if((:zakup = 1) and :dostawa is null and :typmagaz = 'P') then execute procedure NAGFAK_CREATE_DOKMAG_DOSTAWA(:fak, :data,:magazyn) returning_values :dostawa;*/
    /* operacje ilosciowe*/
    if(:ilosc > :pilosc) then begin /*zwiekszenie kierunku obrotu*/
       ilmag = :ilosc - :pilosc;
       ilmago = :ilosco - :pilosco;
       /* odliczamy to co juz jest na podpietych dokumentach magazynowych*/
       if(:zakup = 1) then
         ilmag = :ilmag - :iloscfakp;
       else
         ilmag = :ilmag + :iloscfakp;
       /* i sprawdzamy czy nie podpieto zbyt duzo*/
       if(:iloscfakp<>0 and :ilmag<0) then exception MAGDOK_ILKORTOBIG 'Zbyt duża ilosc na skojarzonych dok. mag. dla '||:ktm;

       if(:zakup = 1) then  /*podanie ceny w przypadku dok. zakupu - inaczej cene nadaje sam magazyn zgodnei z zasada roachodu*/
         cenadok = :cenamag;
       dostmag = :dostawa;
    end else begin /* zmniejszenie kierunku obrotu*/
       ilmagk = :pilosc - :ilosc;
       ilmagko = :pilosco - :ilosco;
       /* odliczamy to co juz jest na podpietych dokumentach magazynowych*/
       if(:zakup = 1) then
         ilmagk = :ilmagk + :iloscfakp;
       else
         ilmagk = :ilmagk - :iloscfakp;
       /* i sprawdzamy czy nie podpieto zbyt duzo*/
       if(:iloscfakp<>0 and :ilmagk<0) then exception MAGDOK_ILKORTOBIG 'Zbyt duża ilosc na skojarzonych dok. mag. dla '||:ktm;
       if(:zakup = 0) then  /*podanie ceny w przypadku dok. sprzedazy
                            - inaczej cene nadaje sam magazyn zgodnei z zasada rozchodu
                            cena moze byc dobrana sama pozniej, jesli to bedzie korekta do konkretnego dokumentu*/
         cenadok = :pcenamag;
       dostmag = :dostawa;
    end
    cnt = 0;
    if(:ilmagk > 0 /*and zakup = 0*/) then begin -- Korekta ilociowa na -
       -- odszukanie pierwotnej pozycji (przed korekta)
       select pg.pozfak
         from pozfak_get_firstpoz(:pozref) pg
       left join POZFAK F on (pg.pozfak = F.REF)
       left join nagfak FAKPOWIAZANE on (FAKPOWIAZANE.ref = F.dokument)
       left join typfak t on (FAKPOWIAZANE.typ = t.symbol)
       where t.nieobrot = 0
       into :pozreforg;
       if(:pozreforg > 0) then begin
         cnt = 0; -- iloc pozycji dokumentow magazynowych ktore nalezy skorygowac
         cntparpro = 0; -- iloc pozycji dokumentow magazynowych ktore nalezy skorygowac (dla refparpro)
         select count(*) from DOKUMPOZ where FROMPOZFAK = :pozreforg and ILOSCL > 0 into :cnt;
         if(:cnt=1) then -- korygowana pozycja faktury skojarzona tylko z jedna pozycja jakiegos dokumentu magazynowego
           select max(ref) from DOKUMPOZ where FROMPOZFAK = :pozreforg and ILOSCL > 0 into :poztokor;
         if(:poztokor is null) then -- wiele pozycji, lub ich brak
           select FROMDOKUMPOZ from POZFAK where ref=:pozreforg into :poztokor;
         if(:poztokor is null) then begin --szukamy faktury pierwotnej (źródowej)
           --pobranie refparpro
           select p.refparpro from pozfak p where p.ref = :pozreforg into :refparpro;
           --powtorzenie bloku jak wyżej z nowym refem pozycji faktury
           select count(*) from DOKUMPOZ where FROMPOZFAK = :refparpro and ILOSCL > 0 into :cntparpro;
           if(:cntparpro=1) then -- korygowana pozycja faktury skojarzona tylko z jedna pozycja jakiegos dokumentu magazynowego
             select max(ref) from DOKUMPOZ where FROMPOZFAK = :refparpro and ILOSCL > 0 into :poztokor;
           if(:poztokor is null) then -- wiele pozycji, lub ich brak
             select FROMDOKUMPOZ from POZFAK where ref=:refparpro into :poztokor;
         end
         if(:poztokor is null and :cnt<>1 and :cntparpro <> 1) then -- wiele pozycji, lub ich brak
           select cena_zakn from wersje where ktm=:ktm and nrwersji=:wersja into :cenadok;
       end else
         exception NAGFAK_CRDOKMAG_BEZORG; -- Brak określonej pozycji pierwotnej dok. VAT.
       select DOKUMENT from DOKUMPOZ where ref=:poztokor into :doktokor;
    end
    /*okreslenie dok. magazynowego, do ktorehgo ma zostac dodana pozycja*/
    ldoktyp = '';
    if(:ilmag > 0) then begin
      if(:opk = 0) then
        ldoktyp = :typmag;
      else
        ldoktyp = :typmago;
    end else if(:ilmagk > 0) then begin
      if(:opk = 0) then
        ldoktyp = :typmagk;
      else
        ldoktyp = :typmagko;
      ilmag = :ilmagk;
      ilmago = :ilmagko;
    end

    if (exists(select first 1 1 from pozfak where dokument = :fak and fake = 1) and
        exists(select first 1 1 from defdokummag where magazyn = :magazyn and typ = :ldoktyp and coalesce(altallow,0) = 0)) then
      exception FAKE_NOT_ALLOW 'Pozycje aleternatywne nie dozwolone dla magazynu i typu dokumentu!';

    if(:ldoktyp <>'' and :hasdok = 0) then begin
      /*naglowek dokumentu magazynowego*/
      ldok = null;
      select DOKUMNAG.REF
      from DOKUMNAG where FAKTURA = :fak and  MAGAZYN = :MAGAZYN
          and (BLOKADA = 2 or BLOKADA = 0) and TYP = :ldoktyp and zrodlo > 1
          and ((:poztokor is null and coalesce(DOKUMNAG.refk,0) = 0) or (DOKUMNAG.refk = :doktokor))
      into :ldok;
      if(:ldok is null) then begin
        execute procedure GEN_REF('DOKUMNAG') returning_values :ldok;
        if(:blockmaster > 0) then begin
          select NUMBER_GEN from DEFMAGAZ where SYMBOL=:magazyn into :number_gen;
          select min(REF) from numberblock
          where NUMERATOR = :number_gen and NUMBERBLOCK.blockmaster = :blockmaster
          and DATA = :data and NUMTYPDOK = cast(:ldoktyp as char(3)) and NUMREJDOK = cast(:magazyn as char(3)) and numberblock.dokdokumnag is null
          into :blockref;
        end
        insert into DOKUMNAG(REF, MAGAZYN, TYP, DATA,  KLIENT, DOSTAWCA,SLODEF, SLOPOZ,FAKTURA, zrodlo, NOTSERIALIZED,MAGBREAK,
             SPOSDOST, SPOSDOSTAUTO, KOSZTDOST, TERMDOST, TRASADOST, WYSYLKA, DULICA, DNRDOMU, DNRLOKALU, DMIASTO, DKODP,
             refk, OPERATOR, ZAMOWIENIE, NUMBLOCK, KOSZTDOSTBEZ, KOSZTDOSTROZ, ODBIORCAID, KOSZTDOSTPLAT,
             ODBIORCA, ILOSCPACZEK, ILOSCPALET, ILOSCPALETBEZ, FLAGISPED, UWAGISPED, BN,
             SPOSZAP, DNIZAP, RABAT, SPRZEDAWCA, SPRZEDAWCAZEW, CINOUT, CKONTRAKTY, KURS, WALUTA, tabkurs, WARTOSCWAL, ZANULOWANIA, SYMBFAK, KRAJWYSYLKI, SRVREQUEST)
        select :ldok, :magazyn, :ldoktyp, :data,  KLIENT, DOSTAWCA,:slodef, :slopoz, ref, :zrodlo, :wzser, :wzbreak,
           :sposdost, :sposdostauto, :kosztdost, :termdost, :trasadost,  :listadost, :listadulica, :listadnrdomu, :listadnrlokalu, :listadmiasto, :listadkodp,
           :doktokor, case when (:anulowanie = 1) then null else operakcept end,:zam, :blockref, :kosztdostbez, :kosztdostroz, :ODBIORCAID, :KOSZTDOSTPLAT,
           ODBIORCA, ILOSCPACZEK, ILOSCPALET, ILOSCPALETBEZ, FLAGISPED, UWAGISPED, BN,
           :sposzap, :dnizap, :rabatnag, :sprzedawca, :sprzedawcazew, CINOUT, :CKONTRAKTY, :kurs, :waluta, :tabkurs, :wartoscwal, :anulowanie, :symbfak, :krajwysylki, :srvrequest
        from NAGFAK where REF=:FAK;
        nr = 1;
      end else begin
        /*aktualizacja numeru zamówienia na nagówku*/
        if(:zam > 0) then
          update DOKUMNAG set ZAMOWIENIE = :zam where ref=:ldok and (ZAMOWIENIE is null or (ZAMOWIENIE <> :zam));
      end

      /*pozycja dokumentu magazynowego*/
      if(:ilmagk > 0 and cnt>1) then begin
        /*jesli pozycja faktury powstala z wielu pozycji dok mag, to zakladaj wiele pozycji na dok mag kor*/
        for select ref,iloscl from DOKUMPOZ where FROMPOZFAK = :pozreforg and ILOSCL > 0
        into :poztokor,:tempilosc
        do begin
          if(:ilmag>0) then begin
            if(:tempilosc>:ilmag) then tempilosc = :ilmag;
            ilmag = :ilmag - :tempilosc;
            execute procedure GEN_REF('DOKUMPOZ') returning_values :dokpozref;
            insert into DOKUMPOZ(REF,DOKUMENT, NUMER, ORD, KTM, WERSJA, ILOSC, CENA, FROMZAM,
                           CENACEN, WALCEN, CENANET, CENABRU, RABATTAB, RABAT, FROMPOZFAK, KORTOPOZ, DATAWAZN, FLAGI, CENAWAL,
                           PARAMN1, PARAMN2, PARAMN3, PARAMN4,
                           PARAMN5, PARAMN6, PARAMN7, PARAMN8,
                           PARAMN9, PARAMN10, PARAMN11, PARAMN12,
                           PARAMS1, PARAMS2, PARAMS3, PARAMS4,
                           PARAMS5, PARAMS6, PARAMS7, PARAMS8,
                           PARAMS9, PARAMS10, PARAMS11, PARAMS12,
                           PARAMD1, PARAMD2, PARAMD3, PARAMD4,
                           KRAJPOCHODZENIA,OPK,PREC, GR_VAT)
              values(:dokpozref, :ldok, :nr, 1, :ktm, :wersja, :tempilosc, :cenadok, :FROMZAM,
                           :cenacen, :walcen, :cenanet, :cenabru, :rabat, :rabattab, :pozref, :poztokor, :datawazn, :flagi, :cenawwal,
                           :paramn1, :paramn2, :paramn3, :paramn4,
                           :paramn5, :paramn6, :paramn7, :paramn8,
                           :paramn9, :paramn10, :paramn11, :paramn12,
                           :params1, :params2, :params3, :params4,
                           :params5, :params6, :params7, :params8,
                           :params9, :params10, :params11, :params12,
                           :paramd1, :paramd2, :paramd3, :paramd4,
                           :krajpochodzenia,:opk,:prec, :gr_vat);
            nr = :nr + 1;
          end
        end
        if(:ilmag > 0) then exception MAGDOK_ILKORTOBIG;
      end else begin
        /*normalna pozycja dokumentu magazynowego*/
        execute procedure GEN_REF('DOKUMPOZ') returning_values :dokpozref;
        AltToPozN = null;
        if (AltToPoz is not null) then
        begin
          select ref
            from dokumpoz
            where frompozfak = :AltToPoz
            into :AltToPozN;
        end

        insert into DOKUMPOZ(REF,DOKUMENT, NUMER, ORD, KTM, WERSJA, ILOSC,ILOSCO, JEDNO, CENA, FROMZAM,
                           CENACEN, WALCEN, CENANET, CENABRU, WARTSNETTO, WARTSBRUTTO , RABATTAB, RABAT, FROMPOZFAK, KORTOPOZ, DATAWAZN, FLAGI, CENAWAL,
                           PARAMN1, PARAMN2, PARAMN3, PARAMN4,
                           PARAMN5, PARAMN6, PARAMN7, PARAMN8,
                           PARAMN9, PARAMN10, PARAMN11, PARAMN12,
                           PARAMS1, PARAMS2, PARAMS3, PARAMS4,
                           PARAMS5, PARAMS6, PARAMS7, PARAMS8,
                           PARAMS9, PARAMS10, PARAMS11, PARAMS12,
                           PARAMD1, PARAMD2, PARAMD3, PARAMD4,
                           KRAJPOCHODZENIA,OPK,PREC, GR_VAT, AltToPoz, fake)
          values(:dokpozref, :ldok, :nr, 1, :ktm, :wersja, :ilmag,:ilmago, :jedn, :cenadok, :FROMZAM,
                           :cenacen, :walcen, :cenanet, :cenabru, :wartnet, :wartbru, :rabattab, :rabat, :pozref, :poztokor, :datawazn, :flagi, :cenawwal,
                           :paramn1, :paramn2, :paramn3, :paramn4,
                           :paramn5, :paramn6, :paramn7, :paramn8,
                           :paramn9, :paramn10, :paramn11, :paramn12,
                           :params1, :params2, :params3, :params4,
                           :params5, :params6, :params7, :params8,
                           :params9, :params10, :params11, :params12,
                           :paramd1, :paramd2, :paramd3, :paramd4,
                           :krajpochodzenia,:opk,:prec,:gr_vat, :AltToPozN, :fake);
        nr = :nr + 1;
        if(:serialized = 1)  then begin
          /*sprawdzenie ilosci serlizowanych zgodnych z pozycj*/
          serialil = null;
          select sum(ilosc) from DOKUMSER where REFPOZ =:pozref and TYP='F' into :serialil;
          if(:serialil is null) then serialil = 0;
          toserialil = :ilosc - :pilosc;
          if(:toserialil < 0) then toserialil = - :toserialil;
          if(:toserialil > :serialil and :autoserrozpisz = 1 and :ilosc > :pilosc) then begin
            i = :toserialil - :serialil;
            execute procedure SERNR_AUTOROZPISZ(:pozref, 'F',:magazyn,:wersjaref,:i);
            serialil = null;
            select sum(ilosc) from DOKUMSER where REFPOZ =:pozref and TYP='F' into :serialil;
            if(:serialil is null) then serialil = 0;
          end
          if(:toserialil <> :serialil) then exception FAK_DOK_CREATE_ILSERWR 'Serializacja dla tow.: '||:ktm||' niekompletna';
        -- SERFIX1 --
          insert into DOKUMSER(REFPOZ,TYP,NUMER,ODSERIAL,DOSERIAL,ODSERIALNR,DOSERIALNR,ILOSC,ORD,ORGDOKUMSER)
              select :dokpozref, 'M',NUMER,ODSERIAL, DOSERIAL,ODSERIALNR,DOSERIALNR,ILOSC,1, coalesce(orgdokumser, ref)
              from DOKUMSER where REFPOZ = :pozref and TYP = 'F';
        -- SERFIX1 --
        end
      end
    end
  end
  if(:akcept =1 or (:akcept = 3)) then begin
   for select ref from DOKUMNAG where FAKTURA = :fak and zrodlo > 1 and MAGAZYN=:magazyn and akcept = 0
   into :dokmag
   do begin
     dokumpoz_count = 0;
     if (exists(select first 1 1 from dokumpoz where dokument = :dokmag)) then dokumpoz_count = 1;
       if (dokumpoz_count > 0) then
         update DOKUMNAG set AKCEPT = 1, BLOKADA = bin_and(BLOKADA,4) where ref = :dokmag;
       else
       begin
         update DOKUMNAG set BLOKADA = 0 where ref = :dokmag;
         delete from dokumnag where ref = :dokmag;
       end
   end
  end else begin
    /*zdjecie blokady do redagowania*/
   for select ref from DOKUMNAG where FAKTURA = :fak and zrodlo > 1 and MAGAZYN=:magazyn and akcept = 0
   into :dokmag
   do begin
     dokumpoz_count = 0;
     if (exists(select first 1 1 from dokumpoz where dokument = :dokmag)) then dokumpoz_count = 1;
       if (dokumpoz_count > 0) then
         update DOKUMNAG set BLOKADA = bin_and(BLOKADA,4) where ref = :dokmag;
       else
       begin
         update DOKUMNAG set BLOKADA = 0 where ref = :dokmag;
         delete from dokumnag where ref = :dokmag;
       end
   end
  end
  if(:zamdepend > 0) then begin
    for select POZZAM.ref from POZZAM join NAGZAM on (POZZAM.zamowienie = NAGZAM.REF)
    where NAGZAM.faktura = :grupadok and FAKDEPEND = 1
    into :frompozzam
    do begin
      execute procedure POZZAM_OBLILOSCFROMFAK(:frompozzam);
    end
  end
  doksym = :symbol;
  status = 1;
end^
SET TERM ; ^
