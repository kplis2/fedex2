--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_GET_FIRSTFAK_UP(
      KOREKTA integer)
  returns (
      NAGFAK integer)
   as
declare variable korektadown integer;
begin
  if(exists(select ref from nagfak where korekta = :korekta)) then begin
    for select ref
      from nagfak
      where korekta = :korekta
      into :korektadown
    do begin
      for select nagfak from nagfak_get_firstfak_up(:korektadown)
        into :nagfak
        do suspend;
    end
  end else begin
    nagfak = :korekta;
    suspend;
  end
end^
SET TERM ; ^
