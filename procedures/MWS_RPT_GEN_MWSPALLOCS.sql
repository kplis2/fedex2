--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_RPT_GEN_MWSPALLOCS(
      OPERREF OPERATOR_ID,
      SHOWMSG SMALLINT_ID = 0,
      LICZBA INTEGER_ID = 1)
  returns (
      MWSPALLOCREF MWSPALLOCS_ID,
      MWSPALLOCSYMB SYMBOL_ID,
      MSG STRING255)
   as
begin
  if (:liczba is null) then
    liczba = 1;
  if (:liczba > 1) then showmsg = 0;

  while (:liczba > 0)
  do begin
    execute procedure gen_ref('MWSPALLOCS') returning_values :mwspallocref;
    mwspallocsymb = 'P'||:mwspallocref;
    insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status,
        fromdocid, fromdocposid, fromdoctype)
      values (:mwspallocref, :mwspallocsymb, null, 2, -1,
        null, null, 'P');
    liczba = liczba - 1;

    if (:showmsg = 1) then
      msg = 'PALETA: '||:mwspallocref;
    suspend;
  end
end^
SET TERM ; ^
