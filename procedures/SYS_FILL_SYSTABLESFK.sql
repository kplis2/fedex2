--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_FILL_SYSTABLESFK as
begin
  delete from sys_tablesfk;
  delete from sys_tablesfkfields;

  insert into sys_tablesfk (table_name, fk_name, ontable)
/*    select rdb$relation_constraints.rdb$relation_name, rdb$relation_constraints.rdb$constraint_name, rdb$indices.rdb$relation_name
      from rdb$relation_constraints
        join rdb$ref_constraints on rdb$relation_constraints.rdb$constraint_name = rdb$ref_constraints.rdb$constraint_name
        join rdb$indices on rdb$ref_constraints.rdb$const_name_uq = rdb$indices.rdb$index_name
       where rdb$relation_constraints.rdb$constraint_type = 'FOREIGN KEY';*/
        select distinct

  re1.RDB$RELATION_NAME,
  re1.RDB$CONSTRAINT_NAME,
  re2.RDB$RELATION_NAME
 -- rind1.RDB$FIELD_NAME,
 -- rind2.RDB$FIELD_NAME
  from RDB$RELATION_CONSTRAINTS re1
join RDB$REF_CONSTRAINTS ref on (re1.RDB$CONSTRAINT_NAME = ref.RDB$CONSTRAINT_NAME)
join RDB$RELATION_CONSTRAINTS re2 on (ref.RDB$CONST_NAME_UQ = re2.RDB$CONSTRAINT_NAME)
join RDB$INDEX_SEGMENTS rind1 on (re1.RDB$INDEX_NAME = rind1.RDB$INDEX_NAME)
join RDB$INDEX_SEGMENTS rind2 on (re2.RDB$INDEX_NAME = rind2.RDB$INDEX_NAME)
where re1.RDB$CONSTRAINT_TYPE = 'FOREIGN KEY';


   insert into sys_tablesfkfields (fk_name, field_name, null_flag)
   select rdb$relation_constraints.rdb$constraint_name, rdb$index_segments.rdb$field_name, rdb$relation_fields.rdb$null_flag
      from rdb$relation_constraints
        join rdb$index_segments on rdb$index_segments.rdb$index_name = rdb$relation_constraints.rdb$index_name
         join rdb$relation_fields on rdb$relation_fields.rdb$field_name = rdb$index_segments.rdb$field_name and rdb$relation_fields.rdb$relation_name = rdb$relation_constraints.rdb$relation_name
      where rdb$relation_constraints.rdb$constraint_type = 'FOREIGN KEY';

--  update sys_tables set dorder = null;
--  update sys_tables set dorder = 0;

end^
SET TERM ; ^
