--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_ORDER_SCHEDULE(
      NAGZAM integer,
      POZZAM integer,
      PRSCHEDULE integer,
      GENID integer)
  returns (
      OUTGENID integer)
   as
declare variable wydania smallint;
declare variable quantity numeric(14,4);
declare variable prsheet integer;
declare variable prord integer;
declare variable nref integer;
declare variable pref integer;
declare variable grupagen integer;
declare variable grupazam integer;
declare variable rejestr varchar(3);
declare variable wersjaref integer;
declare variable ordtoschedule integer;
declare variable prordlist varchar(1024);
declare variable outstring varchar(40);
declare variable prschedguide integer;
begin
  grupagen = 0;
  grupazam = 0;
  prord = null;
  prordlist = '';
  if (genid is null or genid = 0) then
    select -schedview from nagzam where ref = :nagzam
      into genid;
  if (genid is null or genid = 0) then
    execute procedure gen_ref('SCHEDVIEW') returning_values genid;
  outgenid = genid;
  execute procedure get_config('PRREJZAMSCHED',2) returning_values rejestr;
  select wydania from nagzam n left join typzam t on (t.symbol = n.typzam) where n.ref = :nagzam 
    into wydania;
  -- zamówienia sprzedazy osobno bo tu trzeba wygenerować najpierw zlecenia produkcyjne
  ordtoschedule = 0;
  if (wydania = 1) then
  begin
    -- najpierw generujemy zlecenia produkcyjne
    for
      select s.ref, p.iloscm - p.ilzdysp, p.zamowienie, p.ref, p.wersjaref
        from nagzam n
          left join pozzam p on (p.zamowienie = n.ref)
          left join prsheets s on (s.wersjaref = p.wersjaref and p.prsheet = s.ref)
        where n.ref = :nagzam and p.out = 1 and p.iloscm - p.ilzdysp > 0
          and (:pozzam = 0 or p.ref = :pozzam)
        into prsheet, quantity, nref, pref, wersjaref
    do begin
      if (prsheet is null) then
        select s.ref
          from prsheets s
          where s.wersjaref = :wersjaref and s.status = 2
          into prsheet;
      if (prsheet is not null) then
      begin
        execute procedure pr_gen_prodords(:prord,0,:prsheet,:quantity,:rejestr,:nref,:pref,0,0,0,:grupazam,0,1,:grupagen,0,:genid)
          returning_values(:prord, :grupazam, :grupagen);
        if (ordtoschedule <> prord) then
          prordlist = prordlist||';'||:prord;
      end
      ordtoschedule = prord;
    end
    -- oznaczamy grupe generowania harmonogramu do wyswietlenia
    update nagzam set schedview = -:genid where ref = :nagzam and coalesce(schedview,0) = 0;
    if (prordlist <> '' and prordlist <> ';') then
    begin
      ordtoschedule = null;
      -- potem dodajemuy do harmonogramu zlecenia produkcyjne
      for
        select outstring
          from parse_lines(:prordlist,';')
          where outstring <> ''
          into outstring
      do begin
        prord = cast(outstring as integer);
        for
          select distinct nagzamref
            from PR_SHOW_POZZAM_CASCADE (:prord,null)
            into ordtoschedule
        do begin
          if (not exists (select first 1 1 from prschedzam where zamowienie = :ordtoschedule and prschedule = :prschedule)) then
            insert into prschedzam(zamowienie, prschedule) values (:ordtoschedule, :prschedule);
        end
      end
    end
  end
  -- zlecenia produkcyjne
  else if (wydania = 3) then
  begin
    -- po prostu dodajemy do harmonogramu
    if (not exists (select first 1 1 from prschedzam z where z.prschedule = :prschedule and z.zamowienie = :nagzam)) then
    begin
      if (prschedule > 0) then
        insert into prschedzam(zamowienie, prschedule) values (:nagzam, :prschedule);
      else
        exception prschedules_error 'Brak wybranego harmonogramu';
    end
    update nagzam set schedview = -:genid where ref = :nagzam and coalesce(schedview,0) = 0;
  end
  for
    select g.ref
      from prschedguides g
      where g.schedview = :genid
      into prschedguide
  do begin
    execute procedure PRSCHEDGUIDE_SCHEDULE_TOLEFT (:prschedguide);
    execute procedure PRSCHEDULE_UPDATE (:prschedguide);
    update prgantt set genid = :outgenid where prschedguide = :prschedguide;
  end
end^
SET TERM ; ^
