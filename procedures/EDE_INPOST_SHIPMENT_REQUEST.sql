--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_INPOST_SHIPMENT_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable USERLOGIN STRING40;
declare variable USERPASSWORD STRING40;
declare variable SHIPPINGDOC integer;
declare variable ROOTPARENT integer;
declare variable GRANDPARENT integer;
declare variable SUBPARENT integer;
declare variable SECONDSUBPARENT integer;
declare variable SHIPPINGCOMMENTS varchar(200);
declare variable DELIVERYTYPE integer;
declare variable SHIPPER SPEDYTOR_ID;
declare variable SHIPPFROMPLACE varchar(20);
declare variable SHIPPERFNAME varchar(60);
declare variable SHIPPERSNAME varchar(60);
declare variable SHIPPERCOMPANYNAME varchar(255);
declare variable SHIPPERPHONE varchar(255);
declare variable SHIPPEREMAIL varchar(255);
declare variable SHIPPERCITY varchar(255);
declare variable SHIPPERPOSTCODE POSTCODE;
declare variable SHIPPERCOUNTRY COUNTRY_ID;
declare variable SHIPPERSTREET STREET_ID;
declare variable SHIPPERHOUSENO NRDOMU_ID;
declare variable SHIPPERLOCALNO NRDOMU_ID;
declare variable RECIPIENTREF integer;
declare variable RECIPIENTNAME varchar(255);
declare variable RECIPIENTNAMETMP varchar(255);
declare variable RECIPIENTISCOMPANY smallint;
declare variable RECIPIENTTAXID NIP;
declare variable RECIPIENTFNAME varchar(60);
declare variable RECIPIENTFNAMETMP varchar(60);
declare variable RECIPIENTSNAME varchar(60);
declare variable RECIPIENTSNAMETMP varchar(60);
declare variable RECIPIENTCITY varchar(255);
declare variable RECIPIENTCITYTMP varchar(255);
declare variable RECIPIENTPOSTCODE POSTCODE;
declare variable RECIPIENTPOSTCODETMP POSTCODE;
declare variable RECIPIENTCOUNTRY COUNTRY_ID;
declare variable RECIPIENTCOUNTRYTMP COUNTRY_ID;
declare variable RECIPIENTSTREET STREET_ID;
declare variable RECIPIENTSTREETTMP STREET_ID;
declare variable RECIPIENTHOUSENO NRDOMU_ID;
declare variable RECIPIENTHOUSENOTMP NRDOMU_ID;
declare variable RECIPIENTLOCALNO NRDOMU_ID;
declare variable RECIPIENTLOCALNOTMP NRDOMU_ID;
declare variable RECIPIENTPHONE PHONES_ID;
declare variable RECIPIENTEMAIL EMAILWZOR_ID;
declare variable CODAMOUNT CENY;
declare variable INSURANCEAMOUNT CENY;
declare variable PACKAGEREF integer;
declare variable PACKAGETYPE varchar(10);
declare variable PACKAGEWEIGHT WAGA_ID;
declare variable PACKAGELENGTH WAGA_ID;
declare variable PACKAGEWIDTH WAGA_ID;
declare variable PACKAGEHEIGHT WAGA_ID;
declare variable PACKAGEINSURANCE CENY;
declare variable INPOSTPACKAGETYPE char(1);
declare variable INPOSTPACKAGEDIM WAGA_ID;
declare variable SHIPPINGPOINT STRING20;
declare variable czypobranie smallint_id; --[PM] XXX PR121869
begin
  select l.ref, substring(l.uwagisped from 1 for 200), l.sposdost, l.fromplace, l.odbiorcaid,
      trim(l.kontrahent), 0, '',
      trim(l.imie), trim(l.nazwisko),
      trim(l.miasto), trim(l.kodp), trim(l.krajid),
      trim(l.adres), trim(l.nrdomu), trim(l.nrlokalu),
      l.pobranie,
      trim(o.email), coalesce(trim(o.dtelefon),trim(l.telefon)),
      trim(l.sposdostpunktodbioru),
      coalesce(p.pobranie,0) --[PM] XXX PR121869
    from listywysd l
      left join odbiorcy o on (l.odbiorcaid = o.ref)
      left join platnosci p on l.sposzap = p.ref --[PM] XXX PR121869
    where l.ref = :oref
  into :shippingdoc, :shippingcomments, :deliverytype, :shippfromplace, :recipientref,
    :recipientname, :recipientiscompany, :recipienttaxid,
    :recipientfname, :recipientsname,
    :recipientcity, :recipientpostcode, :recipientcountry,
    :recipientstreet, :recipienthouseno, :recipientlocalno,
    :codamount,
    :recipientemail, :recipientphone,
    :shippingpoint,
    :czypobranie; --[PM] XXX PR121869

  --sprawdzanie czy istieje dokument spedycyjny
  if(shippingdoc is null) then exception ede_ups_listywysd_brak;


  if (czypobranie = 1 and coalesce(codamount,0) <= 0) then --[PM] XXX PR121869
    exception universal 'Pobranie BEZ KWOTY !!!'; --[PM] XXX PR121869

  select first 1 k.login, k.haslo
    from spedytwys w
      left join spedytkonta k on (k.spedytor = w.spedytor)
    where w.sposdost = :deliverytype
  into :userlogin, :userpassword;

  val = null;
  parent = null;
  params = null;
  id = 0;
  name = 'CreateDeliveryPacks';
  suspend;

  rootparent = :id;

  val = :userlogin;
  parent = :rootparent;
  params = null;
  id = id + 1;
  name = 'email';
  suspend;

  val = :userpassword;
  parent = :rootparent;
  params = null;
  id = id + 1;
  name = 'password';
  suspend;

  val = 'TEST'; --jesli chcesz testowac zmien na TEST inaczej PRODUCTION
  name = 'ServiceUrlType';
  id = id + 1;
  parent = rootparent;
  suspend;

  val = null;
  parent = :rootparent;
  name = 'paczkomaty';
  params = null;
  id = id + 1;
  suspend;

  grandparent = :id;

  --0 - wymaga dodatkowego generowania etykiet, 1 - przesylka gotowa i etykiety generowane automatycznie
  val = '1';
  parent = :grandparent;
  name = 'autoLabels';
  params = null;
  id = id + 1;
  suspend;

  --0 - nadawane w oddziale firmy, 1 - nadawane w paczkomacie
  val = '0';
  parent = :grandparent;
  name = 'selfSend';
  params = null;
  id = id + 1;
  suspend;

  select o.imie, o.nazwisko, d.opis, d.dtelefon, d.demail,  
      d.dulica, d.dnrdomu, d.dnrlokalu, d.dmiasto, d.dkodp
    from defmagaz d
      left join operator o on (d.dosoba = o.ref)
    where d.symbol = :shippfromplace
  into :shipperfname, :shippersname, :shippercompanyname, :shipperphone, :shipperemail,
    :shipperstreet, :shipperhouseno, :shipperlocalno, :shippercity, :shipperpostcode;

  for
    select o.ref, o.typopk, o.dlugosc, o.szerokosc, o.wysokosc,
        o.x_ubezpieczenie
      from listywysdroz_opk o
      where o.listwysd = :shippingdoc
    into :packageref, :packagetype, :packagelength, :packagewidth, :packageheight,
      :packageinsurance
  do begin
    val = null;
    parent = :grandparent;
    name = 'pack';
    params = null;
    id = id + 1;
    suspend;
  
    subparent = :id;

    val = packageref;
    parent = :subparent;
    name = 'id';
    params = null;
    id = id + 1;
    suspend;

    val = 'test03@paczkomaty.pl'; --:recipientemail;
    parent = :subparent;
    name = 'addresseeEmail';
    params = null;
    id = id + 1;
    suspend;

    val = 'test@testowy.pl'; --:shipperemail; todo
    parent = :subparent;
    name = 'senderEmail';
    params = null;
    id = id + 1;
    suspend;

    val = :recipientphone;
    parent = :subparent;
    name = 'phoneNum';
    params = null;
    id = id + 1;
    suspend;

    --paczkomat - to do
    val = :shippingpoint;
    parent = :subparent;
    name = 'boxMachineName';
    params = null;
    id = id + 1;
    suspend;

    --paczkomat - to do
--    val = 'ALTPACZKOMAT';
--    parent = :subparent;
--    name = 'alternativeBoxMachineName';
--    params = null;
--    id = id + 1;
--    suspend;

    --paczkomat - todo
--    val = 'SENDPACZKOMAT';
--    parent = :subparent;
--    name = 'senderBoxMachineName';
--    params = null;
--    id = id + 1;
--    suspend;

    /*$$IBEC$$ --rodzaj paczki
    inpostpackagedim = minvalue(:packagelength, :packagewidth, :packageheight);
    if (:inpostpackagedim is null) then inpostpackagedim = 0;
    if (:inpostpackagedim > 0) then
    begin
      if (:inpostpackagedim <= 8) then inpostpackagetype = 'A';
      else if (:inpostpackagedim > 8 and :inpostpackagedim <= 19) then inpostpackagetype = 'B';
      else if (:inpostpackagedim > 19 and :inpostpackagedim <= 41) then inpostpackagetype = 'C';
      else exception universal 'Przekroczony dopuszczalny rozmiar paczki';
    end $$IBEC$$*/

    if (:packagetype = 4) then inpostpackagetype = 'A';
    else if (:packagetype = 2) then inpostpackagetype = 'B';
    else if (:packagetype = 3) then inpostpackagetype = 'C';
    else exception universal 'Brak rodzaju paczki';

    val = :inpostpackagetype;
    parent = :subparent;
    name = 'packType';
    params = null;
    id = id + 1;
    suspend;

    val = :packageinsurance;
    parent = :subparent;
    name = 'insuranceAmount';
    params = null;
    id = id + 1;
    suspend;

    val = :codamount;
    parent = :subparent;
    name = 'onDeliveryAmount';
    params = null;
    id = id + 1;
    suspend;

    val = :shippingcomments;
    parent = :subparent;
    name = 'customerRef';
    params = null;
    id = id + 1;
    suspend;

    val = null;
    parent = :subparent;
    name = 'senderAddress';
    params = null;
    id = id + 1;
    suspend;

    secondsubparent = id;

    val = :shipperfname;
    parent = :secondsubparent;
    name = 'name';
    params = null;
    id = id + 1;
    suspend;

    val = :shippersname;
    parent = :secondsubparent;
    name = 'surName';
    params = null;
    id = id + 1;
    suspend;

    val = 'test@testowy.pl';  --:shipperemail; todo
    parent = :secondsubparent;
    name = 'email';
    params = null;
    id = id + 1;
    suspend;

    val = :shipperphone;
    parent = :secondsubparent;
    name = 'phoneNum';
    params = null;
    id = id + 1;
    suspend;

    val = :shipperstreet;
    parent = :secondsubparent;
    name = 'street';
    params = null;
    id = id + 1;
    suspend;

    val = :shipperhouseno;
    parent = :secondsubparent;
    name = 'buildingNo';
    params = null;
    id = id + 1;
    suspend;

    val = :shipperlocalno;
    parent = :secondsubparent;
    name = 'flatNo';
    params = null;
    id = id + 1;
    suspend;

    val = :shippercity;
    parent = :secondsubparent;
    name = 'town';
    params = null;
    id = id + 1;
    suspend;

    val = :shipperpostcode;
    parent = :secondsubparent;
    name = 'zipCode';
    params = null;
    id = id + 1;
    suspend;

    --wojewodztwo
/*
    val = :shipperpostcode;
    parent = :secondsubparent;
    name = 'province';
    params = null;
    id = id + 1;
    suspend;
*/
    --todo
    val = '';
    parent = :subparent;
    name = 'dispatchPointName';
    params = null;
    id = id + 1;
    suspend;

    --ustawic znacznik, ze transakcja jest z allegro
    --todo
    /*$$IBEC$$ val = null;
    parent = :subparent;
    name = 'allegro';
    params = null;
    id = id + 1;
    suspend;

    secondsubparent = id;

    val = 'allegro_user_id';
    parent = :secondsubparent;
    name = 'userId';
    params = null;
    id = id + 1;
    suspend;

    val = 'allegro_payu_id';
    parent = :secondsubparent;
    name = 'transactionId';
    params = null;
    id = id + 1;
    suspend; $$IBEC$$*/

    --ustawic znacznik, ze transakcja jest z allegro
    --todo
    /*$$IBEC$$ val = null;
    parent = :subparent;
    name = 'receiverAddress';
    params = null;
    id = id + 1;
    suspend;

    secondsubparent = id;

    val = 'imię_odbiorcy';
    parent = :secondsubparent;
    name = 'name';
    params = null;
    id = id + 1;
    suspend;

    val = 'nazwisko_odbiorcy';
    parent = :secondsubparent;
    name = 'surName';
    params = null;
    id = id + 1;
    suspend;

    val = 'nawza_firmy_odbiorcy';
    parent = :secondsubparent;
    name = 'companyName';
    params = null;
    id = id + 1;
    suspend;

    val = 'numer_telefonu_odbiorcy';
    parent = :secondsubparent;
    name = 'phoneNum';
    params = null;
    id = id + 1;
    suspend;

    val = 'nazwa_ulicy_odbiorcy';
    parent = :secondsubparent;
    name = 'street';
    params = null;
    id = id + 1;
    suspend;

    val = 'identyfikator_budynku_odbiorcy';
    parent = :secondsubparent;
    name = 'buildingNo';
    params = null;
    id = id + 1;
    suspend;

    val = 'indentyfikator_lokalu_odbiorcy';
    parent = :secondsubparent;
    name = 'flatNo';
    params = null;
    id = id + 1;
    suspend;

    val = 'miasto_ odbiorcy';
    parent = :secondsubparent;
    name = 'town';
    params = null;
    id = id + 1;
    suspend;

    val = 'kod_pocztowy_odbiorcy';
    parent = :secondsubparent;
    name = 'zipCode';
    params = null;
    id = id + 1;
    suspend;

    val = 'województwo_odbiorcy';
    parent = :secondsubparent;
    name = 'province';
    params = null;
    id = id + 1;
    suspend; $$IBEC$$*/
  end
end^
SET TERM ; ^
