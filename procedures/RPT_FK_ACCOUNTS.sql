--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_ACCOUNTS(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      CURRENCY varchar(3) CHARACTER SET UTF8                           ,
      ACCWOPEN smallint,
      ACCTYPE smallint,
      ACCOUNTMASK ACCOUNT_ID,
      FROMACCOUNT ACCOUNT_ID,
      TOACCOUNT ACCOUNT_ID,
      RANGE smallint,
      DOUBLEBAL smallint,
      COMPANY integer,
      ENDBOOKED smallint = 0)
  returns (
      SYMBOL varchar(3) CHARACTER SET UTF8                           ,
      DESCRIPT varchar(60) CHARACTER SET UTF8                           ,
      ODEBIT numeric(15,2),
      OCREDIT numeric(15,2),
      DEBIT numeric(15,2),
      CREDIT numeric(15,2),
      ADEBIT numeric(15,2),
      ACREDIT numeric(15,2),
      BDEBIT numeric(15,2),
      BCREDIT numeric(15,2),
      YEARNAME varchar(40) CHARACTER SET UTF8                           )
   as
declare variable YEARID integer;
declare variable BALANCETYPE smallint;
declare variable BKACCOUNT integer;
declare variable BO_DEBIT numeric(15,2);
declare variable BO_CREDIT numeric(15,2);
declare variable BM_DEBIT numeric(15,2);
declare variable BM_CREDIT numeric(15,2);
declare variable SUM_DEBIT numeric(15,2);
declare variable SUM_CREDIT numeric(15,2);
declare variable IFRANGE smallint;
declare variable PERIOD_TRUNCATED varchar(4);
declare variable EB smallint;
declare variable BB smallint;
begin

  accountmask = substring(accountmask from 1 for 3);
  execute procedure create_account_mask(accountmask)
    returning_values accountmask;
  if (accountmask = '%') then accountmask = null;

  select yearid from bkperiods where company = :company and id = :period into :yearid;
  select yearid from bkyears where company = :company and yearid = :yearid into :yearname;
  if (toaccount is not null and toaccount <> '') then
  begin
    select max(symbol)
      from bkaccounts
      where (symbol <= :toaccount or symbol like :toaccount || '%')
        and yearid = :yearid  and company = :company
      into :toaccount;
  end

  toaccount=substring(:toaccount from 1 for 3);
  fromaccount=substring(:fromaccount from 1 for 3);
  ifrange = 1;

  if(endbooked = 0) then begin
    eb = 0;
    bb = 1;
  end else begin
    eb = 1;
    bb = 0;
  end

  for
    select ref, symbol, descript, saldotype
      from bkaccounts
      where yearid = :yearid
        and (:accountmask is null or symbol like :accountmask) --maska
        and (:fromaccount = '' or symbol >= substring(:fromaccount from 1 for 3))  -- od konta
        and (:toaccount = '' or symbol <= substring(:toaccount from 1 for 3))      -- do konta
        and ((:acctype = 0 and bktype < 2) or (:acctype = 1 and bktype = 2) or :acctype = 2)
        and company = :company --company
      order by symbol
      into :bkaccount, :symbol, :descript, :balancetype
  do begin
    odebit = 0;
    ocredit = 0;
    debit = 0;
    credit = 0;
    adebit = 0;
    acredit = 0;
    bdebit = 0;
    bcredit = 0;

    if (range = 2) then -- sprawdzanie zakresu dla opcji 'z obrotami'
    begin
      if (exists(select accounting from turnovers where
            ((period > substring(:period from 1 for 4) || '00' and :ACCWOPEN = 0)
             or period >= substring(:period from 1 for 4) || '00' and :ACCWOPEN = 1)
            and period <= :period
            and account like :symbol || '%'
            and (debit <> 0 or credit <>0)
            and turnovers.company = :company)) then
        ifrange = 1;
      else ifrange = 0;
    end else
    if (range = 1) then -- sprawdzenie zakresu dla opcji 'otwarte'
    begin
      if (exists(select ref from accounting where yearid = :yearid
        and account like :symbol || '%' and accounting.company = :company)) then --company
        ifrange = 1;
      else ifrange = 0;
    end
    period_truncated=substring(:period from 1 for 4);
for
      select sum(case when T.period = :period_truncated || '00' then T.debit * :bb + T.ebdebit * :eb else 0 end),
           sum(case when T.period = :period_truncated || '00' then T.credit * :bb + T.ebcredit * :eb else 0 end),
           sum(case when T.period = :period then T.debit * :bb + T.ebdebit * :eb else 0 end),
           sum(case when T.period = :period then T.credit * :bb + T.ebcredit * :eb else 0 end),
           sum(case when T.period > :period_truncated || '00' then T.debit * :bb + T.ebdebit * :eb else 0 end),
           sum(case when T.period > :period_truncated || '00' then T.credit * :bb + T.ebcredit * :eb else 0 end)
         from accounting A
           left join turnovers T on (T.accounting = A.ref
             and T.period >= :period_truncated || '00'
             and T.period <= :period)
         where A.bkaccount = :bkaccount and A.currency = :currency and A.yearid = :yearid --nie trzeba warunku na company bo :bkaccount to ref uwzgledniajacy company
         group by A.account
         into :bo_debit, :bo_credit, :bm_debit, :bm_credit, :sum_debit, :sum_credit
    do begin
      if (accwopen = 1) then
      begin
        sum_debit = sum_debit + bo_debit;
        sum_credit = sum_credit + bo_credit;
      end
      odebit = odebit + bo_debit;
      ocredit = ocredit + bo_credit;
      debit = debit + bm_debit;
      credit = credit + bm_credit;
      adebit = adebit + sum_debit;
      acredit = acredit + sum_credit;

      if (accwopen = 1) then
      begin
        if (abs(sum_debit) > abs(sum_credit)) then
          bdebit = bdebit + sum_debit - sum_credit;
        else
          bcredit = bcredit + sum_credit - sum_debit;
      end else
      begin
        if (abs(sum_debit + bo_debit) > abs(sum_credit + bo_credit)) then
          bdebit = bdebit + sum_debit + bo_debit - sum_credit - bo_credit;
        else
          bcredit = bcredit + sum_credit + bo_credit - sum_debit - bo_debit;
      end
    end

    if (balancetype = 0) then
    begin
      bdebit = bdebit - bcredit;
      bcredit = 0;
    end else
    if (balancetype = 1) then
    begin
      if (doublebal = 0) then
      begin
        if (abs(bdebit) > abs(bcredit)) then
        begin
          bdebit = bdebit - bcredit;
          bcredit = 0;
        end else
        begin
          bcredit = bcredit - bdebit;
          bdebit = 0;
        end
      end
    end else
    if (balancetype = 2) then
    begin
      bcredit = bcredit- bdebit;
      bdebit = 0;
    end
    if (ifrange = 1) then suspend;
  end
end^
SET TERM ; ^
