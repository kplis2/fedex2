--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GETCECHA(
      KTM KTM_ID,
      CECHA CECHA_ID,
      WARTOSC STRING255)
  returns (
      IDDEFCECHW INTEGER_ID)
   as
declare variable POCZATEK INTEGER_ID;
declare variable KONIEC INTEGER_ID;
declare variable ZMIENNA STRING255;
declare variable ZMIENNA_DZ STRING255;
declare variable ZMIENNA_NEW STRING255;
declare variable TOWTYPE SMALLINT_ID;
declare variable POZKTM SMALLINT_ID;
declare variable DLUGOSC SMALLINT_ID;
declare variable TXT STRING255;
declare variable WYRAZENIE STRING255;
declare variable TYPPOLA SMALLINT_ID;
declare variable ZAPYTANIE MEMO;
declare variable POLE STRING30;
begin
    --wszystko bierze w leb, jesli mamy wartosci w defcechw na tej samej wartosci
    -- cechy a warunek jest na prefiks i "null" lub pusty i ktos wybierze wartosc
    --(ta sama na prefiksie i nullu) - problem nie do rozwiazania ;-(
    --stad: w przedostatniej linii "first 1 (...) order by prefiks" - najpierw bierze z prefiksem
    koniec=0;
    select usluga from towary where ktm=:ktm into :towtype;
    select filtr, typ from defcechy
        where symbol=:cecha and (towtypes is null or towtypes like '%;'||:towtype||';%')
    into :txt, :typpola;
    wyrazenie=txt;

    if (typpola in (0,1)) then pole='wartnum';
    else if (typpola in (2)) then pole='wartstr';
    else if (typpola in (3,4)) then pole='wartdate';
    else exception universal 'Nie ma takiego typu pola.';

    while ( poczatek is null or poczatek>0 ) do
    begin
      if (poczatek is null) then poczatek=1;
      else poczatek=koniec+1;
      poczatek=position('@', txt, poczatek);
      if (poczatek>0) then koniec=position('@', txt, poczatek+1);
      if (poczatek>0 and koniec>0) then
        begin
          zmienna_dz=substring(txt from  poczatek for  koniec);
          zmienna=replace(zmienna_dz, '@', '');
          select tworzyktm, dlugosc from defcechy where towtypes
            like '%;'||:towtype||';%' and upper(symbol)=upper(:zmienna) into :pozktm, :dlugosc;
          zmienna_new=substring(ktm from  pozktm for  pozktm+dlugosc-1);
          wyrazenie=replace(wyrazenie, zmienna_dz, zmienna_new);
        end
      else poczatek=0; -- jak ktos wpisze nieparzysta ilosc malp (@) zeby nie zajechalo bazy ;-P
    end

    wyrazenie=replace(wyrazenie, '%%', '%');
    if (wyrazenie is null) then wyrazenie='';
    if (wyrazenie<>'') then wyrazenie=' and '||:wyrazenie;


  zapytanie= 'select first 1 id_defcechw from defcechw where cecha = '''||:cecha||''' and '||:pole||' = '''||:wartosc||'''' ||wyrazenie||' order by prefiks desc';
  execute statement zapytanie into :iddefcechw;
end^
SET TERM ; ^
