--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_NOTA(
      REFNOTA integer,
      ODDATY timestamp,
      DODATY timestamp,
      WEZWTYP varchar(10) CHARACTER SET UTF8                           ,
      COMPANY integer)
  returns (
      REF integer,
      DOCREF integer,
      NUMER integer,
      SYMBOL varchar(30) CHARACTER SET UTF8                           ,
      DATA timestamp,
      KHKOD varchar(255) CHARACTER SET UTF8                           ,
      KHKODKS varchar(255) CHARACTER SET UTF8                           ,
      KHULICA varchar(255) CHARACTER SET UTF8                           ,
      KHMIASTO varchar(255) CHARACTER SET UTF8                           ,
      KHNAZWA varchar(255) CHARACTER SET UTF8                           ,
      KHADRES varchar(500) CHARACTER SET UTF8                           ,
      KHNIP varchar(15) CHARACTER SET UTF8                           ,
      SUMAMOUNT numeric(14,2),
      SUMINTEREST numeric(14,2),
      TEXT varchar(1024) CHARACTER SET UTF8                           ,
      CURRENCY WALUTA_ID)
   as
declare variable NOTATYP varchar(10);
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable NIC varchar(255);
declare variable SLODEFREF integer;
begin
  ref = 0;
  for select NOTYNAG.ref, NOTYNAG.data, NOTYNAG.notatyp, NOTYNAG.symbol,
    NOTYNAG.slokod, NOTYNAG.slokodks, notynag.slodef, notynag.slopoz,
    bdebit, notynag.interests, notynag.currency
  from NOTYNAG
  where ((:refnota is null ) or (:refnota = 0) or (:refnota = notynag.ref))
  and notynag.notakind = 1
  and ((:oddaty is null) or (notynag.data >= :oddaty))
  and ((:dodaty is null) or (notynag.data <= :dodaty))
  and ((:wezwtyp is null) or (:wezwtyp = '') or (:wezwtyp = notynag.notatyp))
  and notynag.company = :company
  order by notynag.data,notynag.slokodks
  into :docref, :data, :notatyp, :symbol, :khkod, :khkodks, :slodef, :slopoz,
       :sumamount,:suminterest, :currency
  do begin
    execute procedure SLO_DANE(:slodef, :slopoz) returning_values :khkod,:khnazwa,:nic;
    select min(ref) from slodef where slodef.typ = 'KLIENCI' into :slodefref;
    if(:slodef = :slodefref) then begin
      select KLIENCI.ULICA || iif(coalesce(KLIENCI.NRDOMU,'')<>'',' '||KLIENCI.NRDOMU,'')
        ||iif(coalesce(KLIENCI.NRLOKALU,'')<>'','/'||KLIENCI.NRLOKALU,'')
        ||iif(klienci.poczta<>'' and klienci.poczta<>klienci.miasto, ' ' || klienci.miasto,'')
        ||', '||KLIENCI.kodp||' '||iif(klienci.poczta<>'',klienci.poczta,klienci.miasto),
        KLIENCI.NIP, KLIENCI.ULICA, KLIENCI.miasto from KLIENCI where REF=:slopoz
      into :khadres, :khnip, :khulica, :khmiasto;
    end
    select NOTYTYP.degree, NOTYTYP.descript from NOTYTYP where NOTYTYP.TYP = :notatyp into :numer, :text;
    ref = :ref + 1;
    suspend;
  end
end^
SET TERM ; ^
