--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TYM_MWSSTOCK as
declare variable wh varchar(3);
declare variable vers integer;
begin
  for
    select s.wh, s.vers
      from mwsstock s
      group by s.wh, s.vers
      into wh, vers
  do begin
    execute procedure XK_MWS_RECALC_SELLAV_FOR_STOCK (vers,wh);
  end
end^
SET TERM ; ^
