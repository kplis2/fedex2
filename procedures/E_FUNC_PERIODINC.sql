--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_FUNC_PERIODINC(
      PERIODIN varchar(6) CHARACTER SET UTF8                           ,
      INCREMENT integer,
      DATEFORPER timestamp = null)
  returns (
      PERIODOUT varchar(6) CHARACTER SET UTF8                           )
   as
declare variable YEARP integer;
declare variable MONTHP integer;
declare variable I integer;
begin
/*MWr: Procedura pobiera okres oraz inkrementuje go lub dekrementuje o liczbe
       miesiecy reprezentowana przez zmienna INCREMENT; Jeżeli zostala podana
       data DATEFORPER to PERIODIN jest okreslany/nadpisywany na jej podstawie.
       Obsluguje PERIODIN zapisany zarowno jako RRRRM oraz RRRRMM*/

  if (dateforper is not null) then
    periodin = extract(year from dateforper) || extract(month from dateforper);

  periodin = coalesce(trim(periodin),'');

  if (periodin <> '') then
  begin
    yearp = cast(substring(periodin from 1 for 4) as integer);
    if (coalesce(char_length(periodin),0) > 4) then -- [DG] XXX ZG119346
      monthp = cast(substring(periodin from 5 for 2) as integer);
    if (monthp is null or monthp > 12 or monthp <= 0) then  --PR33972
      exception universal 'Nieprawidłowy okres!';

    periodout = periodin;
    increment = coalesce(increment,0);
    i = abs(increment);
    while (i > 0 or (increment = 0 and i >= 0)) do
    begin
      if (increment > 0 ) then
      begin
        if (monthp < 12) then
          monthp = monthp + 1;
        else begin
          yearp = yearp + 1;
          monthp = 1;
        end
      end else
      if (increment < 0 ) then
      begin
        if (monthp > 1) then
          monthp = monthp - 1;
        else begin
          yearp = yearp - 1;
          monthp = 12;
        end
      end

      if (monthp < 10) then
        periodout = yearp || '0' || monthp;
      else
        periodout = yearp || monthp;

      i = i - 1;
      periodin = periodout;
    end
  end
  suspend;
end^
SET TERM ; ^
