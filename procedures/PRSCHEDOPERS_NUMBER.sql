--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDOPERS_NUMBER(
      SCHOPERFROM integer)
   as
declare variable depto integer;
declare variable number integer;
begin
  for
    select d.depto from prschedoperdeps d
      where d.depfrom = :SCHOPERFROM
      into :depto
  do begin
    execute procedure PRSCHEDOPERS_NUMBER (:depto);
  end
  if(:depto is null) then
  begin
    update prschedopers o set o.number = null where o.ref = :SCHOPERFROM;
    execute procedure PRSCHEDOPERS_NUMBER_DOWN (:SCHOPERFROM, 1) returning_values :number, :schoperfrom;
    execute procedure PRSCHEDOPERS_NUMBER_UP (:SCHOPERFROM, 1);
  end
end^
SET TERM ; ^
