--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GEN_DOKLINKPLIK(
      DOKLINKREF integer,
      SYMBOL varchar(255) CHARACTER SET UTF8                           ,
      EXT varchar(20) CHARACTER SET UTF8                           )
  returns (
      DOKLINKPLIK varchar(255) CHARACTER SET UTF8                           ,
      FOLDER varchar(10) CHARACTER SET UTF8                           )
   as
declare variable CVALUE varchar(1024);
declare variable FORMAT smallint;
begin
   cvalue='';
   folder = '';

   execute procedure get_config('DOKLINKPLIKFORMAT', 0) returning_values :cvalue;
   if(Position('.' in :ext) = 0) then ext = '.' || :ext;

   if(cvalue = '1') then begin
     doklinkplik = :doklinkref || '\' || :symbol || :ext;
     folder = cast(:doklinkref as varchar(10));
   end
   else
     doklinkplik = :symbol || '_' || :doklinkref || :ext;
  suspend;
end^
SET TERM ; ^
