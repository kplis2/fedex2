--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_IS_SPEEDPAK(
      LISTWYSD LISTYWYSD_ID)
  returns (
      ODP smallint)
   as
begin
  select first 1 coalesce(ld.x_szybko, 0)
    from listywysd ld
    where ld.ref = :listwysd
  into :odp;
  suspend;
end^
SET TERM ; ^
