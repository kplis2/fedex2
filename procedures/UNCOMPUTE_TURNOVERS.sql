--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE UNCOMPUTE_TURNOVERS(
      DREF integer)
   as
declare variable DEBIT numeric(15,2);
declare variable CREDIT numeric(15,2);
declare variable CURR_DEBIT numeric(15,2);
declare variable CURR_CREDIT numeric(15,2);
declare variable ACCOUNT ACCOUNT_ID;
declare variable PERIOD varchar(6);
declare variable YEARID integer;
declare variable ACCOUNTING integer;
declare variable COUNTRY_CURRENCY varchar(3);
declare variable DECREE_CURRENCY varchar(3);
declare variable COMPANY integer;
declare variable TRNDATE timestamp;
declare variable TRNPERIOD varchar(6);
begin
  select D.account, D.debit, D.credit, B.period, P.yearid, D.currency, D.currdebit, D.currcredit, B.company, D.transdate, D.period
    from decrees D
      left join bkdocs B on (D.bkdoc = B.ref)
      left join bkperiods P on (P.id = B.period and p.company = d.company)
    where D.ref = :DREF
    into :account, :debit, :credit, :period, :yearid, :decree_currency, :curr_debit, :curr_credit, :company, :trndate, :trnperiod;

  execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2) returning_values country_currency;
  select ref
    from accounting
    where company = :company and account = :account and yearid=:yearid and currency = :country_currency
    into :accounting;

  update turnoversd set debit = debit - :debit, credit = credit - :credit
     where accounting = :accounting and period = :period  and trndate = :trndate;

  update turnovers set debit = debit - :debit, credit=credit-:credit
    where accounting = :accounting and period = :period;

  if ((decree_currency is not null) and decree_currency <> country_currency) then
  begin
    accounting = null;
    select ref
      from accounting
      where company = :company and account = :account and yearid = :yearid and currency = :decree_currency
      into :accounting;

   update turnoversd set debit = debit - :curr_debit, credit = credit - :curr_credit
     where accounting = :accounting and period = :period  and trndate = :trndate;

    update turnovers set debit = debit - :curr_debit, credit = credit - :curr_credit
      where accounting = :accounting and period = :period;
  end
end^
SET TERM ; ^
