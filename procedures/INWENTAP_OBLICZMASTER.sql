--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INWENTAP_OBLICZMASTER(
      DOKUM integer,
      WERSJAREF integer,
      CENA numeric(14,2),
      DOSTAWA integer,
      ILINW numeric(14,4))
   as
declare variable ilsum numeric(14,4);
declare variable zpartiami smallint;
declare variable cnt integer;
begin
  ilsum = 0;
  select zpartiami from INWENTA where ref=:dokum into :zpartiami;
  if(:zpartiami is null) then zpartiami = 0;
  if(:zpartiami = 0) then
    select sum(ILINW) from INWENTAP join INWENTA on (INWENTAP.inwenta = inwenta.ref)
    where INWENTA.inwentaparent = :dokum and INWENTAP.wersjaref = :wersjaref into :ilsum;
  else
    select sum(ILINW) from INWENTAP join INWENTA on (INWENTAP.inwenta = inwenta.ref)
    where INWENTA.inwentaparent = :dokum and INWENTAP.wersjaref = :wersjaref and cena = :cena and dostawa = :dostawa
    into :ilsum;
  if(:ilsum is null) then ilsum = 0;
  cnt = null;
  if(:zpartiami = 0) then begin
    select count(*) from INWENTAP where inwenta = :dokum and inwentap.wersjaref = :wersjaref into :cnt;
    if(:cnt is null) then cnt = 0;
    if(:cnt = 0) then
      insert into INWENTAP(INWENTA, WERSJAREF, CENA, DOSTAWA, ILINW)
      values (:dokum, :wersjaref, 0,NULL, :ilsum);
    else
      update INWENTAP set ILINW = :ilsum + ilkorekta where inwenta=:dokum and wersjaref = :wersjaref;
  end else begin
    select count(*) from INWENTAP where inwenta = :dokum and inwentap.wersjaref = :wersjaref and cena = :cena and dostawa = :dostawa into :cnt;
    if(:cnt is null) then cnt = 0;
    if(:cnt = 0) then
      insert into INWENTAP(INWENTA, WERSJAREF, CENA, DOSTAWA, ILINW)
      values (:dokum, :wersjaref, :cena,:dostawa, :ilsum);
    else
      update INWENTAP set ILINW = :ilsum + ilkorekta where inwenta=:dokum and wersjaref = :wersjaref and cena = :cena and dostawa = :dostawa;
  end
end^
SET TERM ; ^
