--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INWENTA_POLACZ(
      FROMINW integer,
      TOINW integer,
      TRYB smallint)
   as
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable cena numeric(14,2);
declare variable dostawa integer;
declare variable ilinw numeric(14,4);
declare variable cnt integer;
declare variable topoz integer;
declare variable zbiorczy integer;
begin
  select INWENTA.zbiorczy from INWENTA where REF=:toinw into :zbiorczy;
  if(:zbiorczy is null or (:zbiorczy <> 1)) then exception UNIVERSAL 'Arkusz, do którego nastepuje przeniesienie, nie jest arkuszem zbiorczym';
  for select KTM, WERSJA, cena, dostawa, ilinw
  from INWENTAP where INWENTA = :frominw and ilinw > 0
  into :ktm, :wersja, :cena, dostawa, :ilinw
  do begin
    cnt = null;
    topoz = null;
    if(:ilinw is null) then ilinw = 0;
    select REF from INWENTAP
       where INWENTA = :toinw and KTM = :ktm and WERSJA = :wersja
       and ((CENA = :cena) or (:cena is null)) and ((DOSTAWA = :dostawa) or (DOSTAWA is null and :dostawa is null))
       into :topoz;
    if(:topoz > 0) then begin
      if(:tryb = 3) then exception INWENTA_DOLACZ_CONFLICT;
      else if(:tryb = 2) then
        update INWENTAP set ILINW = :ilinw
          where INWENTA = :toinw and KTM = :ktm and WERSJA = :wersja
          and CENA = :cena and ((DOSTAWA = :dostawa) or (DOSTAWA is null and :dostawa is null));
      else if(:tryb = 0) then
        update INWENTAP set ILINW = ILINW + :ilinw
          where INWENTA = :toinw and KTM = :ktm and WERSJA = :wersja
          and CENA = :cena and ((DOSTAWA = :dostawa) or (DOSTAWA is null and :dostawa is null));
    end else
      insert into INWENTAP(INWENTA,KTM,WERSJA,CENA,DOSTAWA,ILINW)
        values(:toinw, :ktm, :wersja, :cena, :dostawa, :ilinw);
  end
  update INWENTA set INWENTAPARENT = :toinw where REF=:frominw;
end^
SET TERM ; ^
