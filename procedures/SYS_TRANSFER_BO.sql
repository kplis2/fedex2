--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_TRANSFER_BO(
      BKDOC integer)
   as
declare variable s1 varchar(255); /* konto ksiegowe */
declare variable s2 varchar(255); /* kwota Winien PLN */
declare variable s3 varchar(255); /* kwota Ma PLN */
declare variable s4 varchar(255); /* opis */
declare variable s5 varchar(255); /* waluta */
declare variable s6 varchar(255); /* kwota Winien */
declare variable s7 varchar(255); /* kwota Ma */
declare variable s8 varchar(255); /* kurs */
declare variable account varchar(20);
declare variable winienzl numeric(14,2);
declare variable mazl numeric(14,2);
declare variable descript varchar(255);
declare variable currency varchar(3);
declare variable winien numeric(14,2);
declare variable ma numeric(14,2);
declare variable kurs numeric(14,4);
declare variable tmp integer;
begin

  select count(ref) from expimp where coalesce(char_length(s1),0)>20 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich kont ksiegowych.';

  select count(ref) from expimp where coalesce(char_length(s5),0)>3 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich symboli waluty.';


  for select s1, s2,  s3,  s4,  s5,  s6,  s7
    from expimp 
  into :s1, :s2,  :s3,  :s4,  :s5,  :s6,  :s7
  do begin

-- konto ksiegowe
    account = substring(:s1 from  1 for  20);


-- kwota WN PLN
    s2 = replace(:s2, ',', '.');
    winienzl = cast(:s2 as numeric(14,2));


-- kwata MA PLN
    s3 = replace(:s3, ',', '.');
    mazl = cast(:s3 as numeric(14,2));


-- opis
    descript = :s4;


-- waluta
    currency = substring(:s5 from  1 for  3);


-- kwota WN
    s6 = replace(:s6, ',', '.');
    winien = cast(:s6 as numeric(14,2));


-- kwata MA
    s7 = replace(:s7, ',', '.');
    ma = cast(:s7 as numeric(14,2));


-- kurs
    s8 = replace(:s8, ',', '.');
    kurs = cast(:s8 as numeric(14,4));


-- INSERTY
-- PLN
    if(currency='PLN' or currency='' or currency is null) then
    begin
      if(winienzl > 0) then
        execute procedure insert_decree(:bkdoc, :account, 0, :winienzl, :descript, 0);
      if(mazl > 0) then
        execute procedure insert_decree(:bkdoc, :account, 1, :mazl, :descript, 0);
    end
-- walutay
    else
    begin
      if(winienzl > 0) then
        execute procedure insert_decree_currency(:bkdoc, :account, 0, :winienzl, :winien, :kurs,
                                 :currency, :descript, 0);
      if(mazl > 0) then
        execute procedure insert_decree_currency(:bkdoc, :account, 0, :mazl, :ma, :kurs, :currency,
                                    :descript, 0);
    end

  end
end^
SET TERM ; ^
