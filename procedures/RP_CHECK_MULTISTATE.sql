--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RP_CHECK_MULTISTATE(
      TABLENAME varchar(31) CHARACTER SET UTF8                           ,
      KEY1 varchar(65) CHARACTER SET UTF8                           ,
      KEY2 varchar(65) CHARACTER SET UTF8                           ,
      KEY3 varchar(65) CHARACTER SET UTF8                           ,
      KEY4 varchar(65) CHARACTER SET UTF8                           ,
      KEY5 varchar(65) CHARACTER SET UTF8                           ,
      TOKEN integer,
      CURSTATE integer)
  returns (
      NEWSTATE integer)
   as
declare variable locations varchar(255);
declare variable mlocations varchar(255);
declare variable location integer;
declare variable mlocation integer;
declare variable tolocat integer;
declare variable tolocat2 integer;
declare variable tryb varchar(255);
declare variable locatchange integer;
declare variable locnum integer;
declare variable logref integer;
begin
  if(:token <> 7) then
    newstate = 0;
  tolocat = null;

  if(:key2 is null) then key2 = '';
  if(:key3 is null) then key3 = '';
  if(:key4 is null) then key4 = '';
  if(:key5 is null) then key5 = '';
  if(:curstate = -2) then -- polecenie usunicia kopiowania i usuwania rekordu
  begin
    delete from LOG_FILE where TABLE_NAME = :tablename and KEY1=:key1 and KEY2 = :key2 and key3 = :key3 and KEY4 = :key4 and key5 = :key5;
    exit;
  end
  --nastepuje okreslenie
  locatchange = 0;
  if(:curstate < -2) then -- w STATE jest zakodowany numer lokalizacji dokonującej zmiany - nie nalezy do niej kopiowac
  begin
    locatchange = bin_and(:curstate,65535);
    curstate = 0;
  end
  execute procedure GET_CONFIG('AKTULOCAT',2)
    returning_values :locations;
  execute procedure GET_CONFIG('AKTULOCATM',2)
    returning_values :mlocations;
  execute procedure GET_CONFIG('AKTULOCATTRYB',2)
    returning_values tryb;
  if (:locations = '' or :locations is null ) then exit;
  location = locations;
  mlocation = mlocations;
  if(:curstate > 0) then
    tolocat = :curstate;
  else begin

      --okreslenie lokalizacji docelowych, gdzie ma byc kopiowany rekord
    select max(tolocat), max(tolocat2)
    from RP_SYNCSTATE S
    where s.tabela=:tablename and
          ( s.fromlocat=:location or s.fromlocat=0
            or (s.fromlocat=-1 and :tryb='M')
            or (s.fromlocat=-2 and :tryb='S')
          )
    into :tolocat, :tolocat2;
    if (tolocat2 is null) then
      tolocat2 = 0;
    if (tolocat < 0 and tolocat2 < 0) then
    begin
      if (tolocat = -3) then
        tolocat = tolocat2;
      else  if (tolocat <> tolocat2) then
          tolocat = 0;
    end

    if (tolocat = -3) then
      tolocat = null; --brak celu
  end
  if(:tolocat is null) then exit;
  --wylistowanie lokalizacji docelowych i insert do bazy LOG_FILE odpowiednich rekordów
  for select LOCNUM from rp_location_list
  into :locnum
  do begin
    if(
        ( (:tolocat = :locnum)
          or (:tolocat = 0)
          or (:tolocat2 = :locnum)
          or (:tolocat = -1 and :locnum = :mlocation)
          or (:tolocat = -2 and :locnum <> :mlocation)
        )
      and
        (:locnum <> :location) --wyciecie z celu wlasnej lokalacji
      and
        (:locatchange = 0 or :locnum <> :locatchange) -- wyciecie z celu lokacji dokonujacej zapisu
    ) then
    begin
      --zapisanie do LOG_FILE odpowiendiej wartoci
      logref = 0;
      select first 1 REF
        from LOG_FILE
        where TABLE_NAME = :tablename and KEY1 = :key1 and KEY2 = :key2
            and KEY3 = :key3 and KEY4 = :key4 and key5 = :key5
            and STATE = :locnum
            and  copy = 1
        order by ref
      into :logref;
      if(:logref > 0) then begin
        -- rekord juz istnieje - zmiana licznika
        update LOG_FILE set STATECOUNT = STATECOUNT + 1 where REF=:logref;
      end else begin
        insert into log_file (TABLE_NAME, KEY1, KEY2, KEY3, KEY4, KEY5, state,  COPY)
          values (:tablename, :key1, :key2, :key3, :key4, :key5,  :locnum,  1);
      end
    end
  end


end^
SET TERM ; ^
