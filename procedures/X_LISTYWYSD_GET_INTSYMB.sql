--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_LISTYWYSD_GET_INTSYMB(
      LISTYWYSD LISTYWYSD_ID)
  returns (
      INT_SYMBOL STRING1024)
   as
begin
  --procedura zwraca liste symboli WZ z HERMONA ktore wchodza w sklad podanego listu spedycyjnego.
  select list( DISTINCT dg.int_symbol)
    from listywysd ld
      join listywysdpoz lp on lp.dokument = ld.ref
      join dokumpoz dp on dp.ref = lp.dokpoz
      join dokumnag dg on dp.dokument = dg.ref
    where ld.ref = :listywysd
    into :int_symbol;
  suspend;
end^
SET TERM ; ^
