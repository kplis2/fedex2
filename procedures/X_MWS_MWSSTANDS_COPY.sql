--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_MWSSTANDS_COPY(
      WHSEC WHSECS_ID)
   as
declare variable sourcewhsecrow whsecrows_id;
declare variable destwhsecrow whsecrows_id;
declare variable destwhsecrownum integer_id;
begin
  for
    select r.ref, r.number
      from whsecrows r
      where r.whsec = :whsec
        and r.rowtype = 0
        and not exists(select first 1 1 from mwsstands s where s.whsecrow = r.ref)
      order by r.number
    into :destwhsecrow, :destwhsecrownum
  do begin
    select r.ref
      from whsecrows r
      where r.whsec = :whsec
        and r.rowtype = 0
        and exists(select first 1 1 from mwsstands s where s.whsecrow = r.ref)
        and r.number = :destwhsecrownum - 1
    into :sourcewhsecrow;

    insert into mwsstands (WH, WHSEC, SYMBOL, STORIESQUANT, LOADCAPACITY, MWSSTANDTYPE, L, W, MWSSTANDSEGQ, WHSECROW, NUMBER, ORD, WHSECDICT, WHSECROWDICT)
      select
          WH,
          WHSEC,
          SYMBOL,
          STORIESQUANT,
          LOADCAPACITY,
          MWSSTANDTYPE,
          L,
          W,
          MWSSTANDSEGQ,
          :destwhsecrow,
          NUMBER,
          ORD,
          WHSECDICT,
          WHSECROWDICT
      from mwsstands s
      where s.whsecrow = :sourcewhsecrow;
  end
end^
SET TERM ; ^
