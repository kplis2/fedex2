--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NEOSSSERVER_GETSTATE(
      REF INTEGER_ID)
  returns (
      STATE SMALLINT_ID,
      MESSAGE MEMO)
   as
begin
  select state,msg from neossserver_data where ref=:ref
  into state,message;

  suspend;
end^
SET TERM ; ^
