--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INSERT_DECREE_DISTS(
      BKDOC integer,
      ACCOUNT ACCOUNT_ID,
      SIDE smallint,
      AMOUNT numeric(15,2),
      DESCRIPT varchar(80) CHARACTER SET UTF8                           ,
      DIST1DDEF integer,
      DIST1SYMBOL SYMBOLDIST_ID,
      DIST2DDEF integer,
      DIST2SYMBOL SYMBOLDIST_ID,
      DIST3DDEF integer,
      DIST3SYMBOL SYMBOLDIST_ID,
      DIST4DDEF integer,
      DIST4SYMBOL SYMBOLDIST_ID,
      DIST5DDEF integer,
      DIST5SYMBOL SYMBOLDIST_ID,
      OPTIMALIZE smallint,
      OPTLAST smallint = 1,
      DIST6DDEF integer = null,
      DIST6SYMBOL SYMBOLDIST_ID = null,
      MATCHINGSYMBOL varchar(40) CHARACTER SET UTF8                            = null)
   as
declare variable DEBIT numeric(14,2);
declare variable CREDIT numeric(14,2);
declare variable DECREE integer;
declare variable DISTPOS integer;
declare variable NUMBER integer;
declare variable PERIOD varchar(6);
declare variable TRANSDATE timestamp;
declare variable COMPANY integer;
declare variable MULTIDIST smallint;
declare variable MULTIDIST1 smallint;
declare variable MULTIDIST2 smallint;
declare variable MULTIDIST3 smallint;
declare variable MULTIDIST4 smallint;
declare variable MULTIDIST5 smallint;
declare variable MULTIDIST6 smallint;
begin
  if (amount is not null and amount <> 0) then
  begin
    if (side = 0) then
    begin
      debit = amount;
      credit = 0;
    end else
    begin
     debit = 0;
     credit = amount;
    end
    if (account is null) then account = '';

    multidist1 =0;
    multidist2 =0;
    multidist3 =0;
    multidist4 =0;
    multidist5 =0;
    multidist6 =0;
    if (:dist1ddef is not null) then
      select coalesce(multidist, 0) from slodef
      where ref = :dist1ddef
      into :multidist1;
    if (:dist2ddef is not null) then
      select coalesce(multidist, 0) from slodef
      where ref = :dist2ddef
      into :multidist2;
    if (:dist3ddef is not null) then
      select coalesce(multidist, 0) from slodef
      where ref = :dist3ddef
      into :multidist3;
    if (:dist4ddef is not null) then
      select coalesce(multidist, 0) from slodef
      where ref = :dist4ddef
      into :multidist4;
    if (:dist5ddef is not null) then
      select coalesce(multidist, 0) from slodef
      where ref = :dist5ddef
      into :multidist5;
    if (:dist6ddef is not null) then
      select coalesce(multidist, 0) from slodef
      where ref = :dist6ddef
      into :multidist6;

    if (:multidist1>0 or :multidist2 >0 or :multidist3 >0 or :multidist4 >0 or :multidist5 >0 or :multidist6 >0)
    then multidist = 1;
    else multidist = 0;

    if (optimalize = 1) then
    begin
     if(multidist = 0) then begin
       decree = null;
      if (side = 0) then
      begin
        select max(ref) from decrees
          where bkdoc = :bkdoc and account = :account and debit <> 0
            and descript = :descript
            and (matchingsymbol = :matchingsymbol or :matchingsymbol is null)
            and ((dist1ddef is null and :dist1ddef is null)
              or (dist1ddef = :dist1ddef and dist1symbol = :dist1symbol))
            and ((dist2ddef is null and :dist2ddef is null)
              or (dist2ddef = :dist2ddef and dist2symbol = :dist2symbol))
            and ((dist3ddef is null and :dist3ddef is null)
              or (dist3ddef = :dist3ddef and dist3symbol = :dist3symbol))
            and ((dist4ddef is null and :dist4ddef is null)
              or (dist4ddef = :dist4ddef and dist4symbol = :dist4symbol))
            and ((dist5ddef is null and :dist5ddef is null)
              or (dist5ddef = :dist5ddef and dist5symbol = :dist5symbol))
            and ((dist6ddef is null and :dist6ddef is null)
              or (dist6ddef = :dist6ddef and dist6symbol = :dist6symbol))
          into :decree;
      end else
      begin
        select max(ref) from decrees
          where bkdoc = :bkdoc and account = :account and credit <> 0
            and descript = :descript
            and (matchingsymbol = :matchingsymbol or :matchingsymbol is null)
            and ((dist1ddef is null and :dist1ddef is null)
              or (dist1ddef = :dist1ddef and dist1symbol = :dist1symbol))
            and ((dist2ddef is null and :dist2ddef is null)
              or (dist2ddef = :dist2ddef and dist2symbol = :dist2symbol))
            and ((dist3ddef is null and :dist3ddef is null)
              or (dist3ddef = :dist3ddef and dist3symbol = :dist3symbol))
            and ((dist4ddef is null and :dist4ddef is null)
              or (dist4ddef = :dist4ddef and dist4symbol = :dist4symbol))
            and ((dist5ddef is null and :dist5ddef is null)
              or (dist5ddef = :dist5ddef and dist5symbol = :dist5symbol))
            and ((dist6ddef is null and :dist6ddef is null)
              or (dist6ddef = :dist6ddef and dist6symbol = :dist6symbol))
          into :decree;
      end
      if (decree is not null) then
      begin
        select max(number) from decrees where bkdoc = :bkdoc
          into :number;
        update decrees set debit = debit + :debit, credit = credit + :credit,
          number = iif(:optlast = 1, :number, number)
          where ref = :decree;
      end else
      begin
        insert into decrees (bkdoc, account, side, debit, credit, descript,
            dist1ddef, dist1symbol, dist2ddef, dist2symbol, dist3ddef,
            dist3symbol, dist4ddef, dist4symbol, dist5ddef, dist5symbol,
            dist6ddef, dist6symbol, matchingsymbol)
          values (:bkdoc, :account, :side, :debit, :credit, :descript,
            :dist1ddef, :dist1symbol, :dist2ddef, :dist2symbol, :dist3ddef,
            :dist3symbol, :dist4ddef, :dist4symbol, :dist5ddef, :dist5symbol,
            :dist6ddef, :dist6symbol, :matchingsymbol);
      end
     end
     else if(multidist > 0) then begin
      decree = null;
      if (side = 0) then
      begin
        select max(ref) from decrees
          where bkdoc = :bkdoc and account = :account and debit <> 0
            and descript = :descript
            and (matchingsymbol = :matchingsymbol or :matchingsymbol is null)
          into :decree;
      end else
      begin
        select max(ref) from decrees
          where bkdoc = :bkdoc and account = :account and credit <> 0
            and descript = :descript
            and (matchingsymbol = :matchingsymbol or :matchingsymbol is null)
          into :decree;
      end
      if (decree is not null) then
      begin
        select max(number) from decrees where bkdoc = :bkdoc
          into :number;
        update decrees set debit = debit + :debit, credit = credit + :credit,
          number = iif(:optlast = 1, :number, number), blokobldists = 1
          where ref = :decree;
      end else
      begin
        execute procedure gen_ref('DECREES') returning_values :decree;
        insert into decrees (ref, bkdoc, account, side, debit, credit, descript, matchingsymbol)
          values (:decree,:bkdoc, :account, :side, :debit, :credit, :descript, :matchingsymbol);
      end
      select period, transdate, company from decrees where ref = :decree
      into :period, :transdate, :company;
      if ((:dist1ddef is not null and coalesce(:dist1symbol, '') <> '')
       or (:dist2ddef is not null and coalesce(:dist2symbol, '') <> '')
       or (:dist3ddef is not null and coalesce(:dist3symbol, '') <> '')
       or (:dist4ddef is not null and coalesce(:dist4symbol, '') <> '')
       or (:dist5ddef is not null and coalesce(:dist5symbol, '') <> '')
       or (:dist6ddef is not null and coalesce(:dist6symbol, '') <> '')) then begin
       if (side = 0) then
       begin
          select max(ref) from distpos
          where bkdoc = :bkdoc and account = :account and debit <> 0
          and descript = :descript
          and ((tempdictdef is null and :dist1ddef is null)
              or (tempdictdef = :dist1ddef and tempsymbol = :dist1symbol))
            and ((tempdictdef2 is null and :dist2ddef is null)
              or (tempdictdef2 = :dist2ddef and tempsymbol2 = :dist2symbol))
            and ((tempdictdef3 is null and :dist3ddef is null)
              or (tempdictdef3 = :dist3ddef and tempsymbol3 = :dist3symbol))
            and ((tempdictdef4 is null and :dist4ddef is null)
              or (tempdictdef4 = :dist4ddef and tempsymbol4 = :dist4symbol))
            and ((tempdictdef5 is null and :dist5ddef is null)
              or (tempdictdef5 = :dist5ddef and tempsymbol5 = :dist5symbol))
            and ((tempdictdef6 is null and :dist6ddef is null)
              or (tempdictdef6 = :dist6ddef and tempsymbol6 = :dist6symbol))
          into :distpos;
       end else
       begin
          select max(ref) from distpos
          where bkdoc = :bkdoc and account = :account and credit <> 0
          and descript = :descript
          and ((tempdictdef is null and :dist1ddef is null)
              or (tempdictdef = :dist1ddef and tempsymbol = :dist1symbol))
            and ((tempdictdef2 is null and :dist2ddef is null)
              or (tempdictdef2 = :dist2ddef and tempsymbol2 = :dist2symbol))
            and ((tempdictdef3 is null and :dist3ddef is null)
              or (tempdictdef3 = :dist3ddef and tempsymbol3 = :dist3symbol))
            and ((tempdictdef4 is null and :dist4ddef is null)
              or (tempdictdef4 = :dist4ddef and tempsymbol4 = :dist4symbol))
            and ((tempdictdef5 is null and :dist5ddef is null)
              or (tempdictdef5 = :dist5ddef and tempsymbol5 = :dist5symbol))
            and ((tempdictdef6 is null and :dist6ddef is null)
              or (tempdictdef6 = :dist6ddef and tempsymbol6 = :dist6symbol))
          into :distpos;
       end
       if(:distpos is not null) then
        update distpos set debit = debit + :debit, credit = credit + :credit where ref=:distpos;
       else
        insert into distpos (tempdictdef, tempsymbol, tempdictdef2, tempsymbol2,tempdictdef3, tempsymbol3,
                          tempdictdef4, tempsymbol4, tempdictdef5, tempsymbol5, tempdictdef6, tempsymbol6,
                          decrees, bkdoc, period, account, debit, credit, transdate, company, descript)
        values (:dist1ddef, :dist1symbol,:dist2ddef, :dist2symbol, :dist3ddef, :dist3symbol,
               :dist4ddef, :dist4symbol, :dist5ddef, :dist5symbol, :dist6ddef, :dist6symbol,
               :decree, :bkdoc, :period, :account, :debit, :credit, :transdate, :company, :descript);
      end
     end
    end else
    begin
      if(multidist = 0) then begin
        insert into decrees (bkdoc, account, side, debit, credit, descript,
         dist1ddef, dist1symbol, dist2ddef, dist2symbol, dist3ddef,
          dist3symbol, dist4ddef, dist4symbol, dist5ddef, dist5symbol,
          dist6ddef, dist6symbol, matchingsymbol)
        values (:bkdoc, :account, :side, :debit, :credit, :descript,
          :dist1ddef, :dist1symbol, :dist2ddef, :dist2symbol, :dist3ddef,
          :dist3symbol, :dist4ddef, :dist4symbol, :dist5ddef, :dist5symbol,
          :dist6ddef, :dist6symbol, :matchingsymbol);
      end else begin
        execute procedure gen_ref('DECREES') returning_values :decree;
        insert into decrees (ref, bkdoc, account, side, debit, credit, descript, matchingsymbol)
          values (:decree,:bkdoc, :account, :side, :debit, :credit, :descript, :matchingsymbol);
        insert into distpos (tempdictdef, tempsymbol, tempdictdef2, tempsymbol2,tempdictdef3, tempsymbol3,
                          tempdictdef4, tempsymbol4, tempdictdef5, tempsymbol5, tempdictdef6, tempsymbol6,
                          decrees, bkdoc, account, debit, credit, descript)
        values (:dist1ddef, :dist1symbol,:dist2ddef, :dist2symbol, :dist3ddef, :dist3symbol,
               :dist4ddef, :dist4symbol, :dist5ddef, :dist5symbol, :dist6ddef, :dist6symbol,
               :decree, :bkdoc, :account, :debit, :credit, :descript);
      end
    end
  end
end^
SET TERM ; ^
