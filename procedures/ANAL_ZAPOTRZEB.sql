--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ANAL_ZAPOTRZEB(
      DOSTAWCA integer,
      ZAMOWIENIEIN integer)
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      MIARA varchar(5) CHARACTER SET UTF8                           ,
      IW numeric(14,4),
      SM numeric(14,4),
      ZAM numeric(14,4),
      POT numeric(14,4),
      POTPILNE numeric(14,4),
      ZAMOWIC numeric(14,4),
      BRAK numeric(14,4),
      BRAKP numeric(14,4),
      STANMIN numeric(14,4),
      STANMAX numeric(14,4),
      STATUSDOST smallint,
      REF integer,
      CZASDOSTAWY integer,
      DOSTAWCAOUT integer,
      DOSTAWCAGL SHORTNAME_ID,
      DOSTAWCAFIRST SHORTNAME_ID)
   as
declare variable PRSHET integer;
declare variable SQL varchar(1024);
declare variable ZREAL numeric(14,4);
begin
  ref = zamowieniein;
  dostawcaout = dostawca;
  if(dostawca = 0)then dostawca = null;
  if(zamowieniein = 0) then zamowieniein = null;
--asortyment z kartami technologicznymi
  for select max(w.ktm), w.ref, max(w.nazwat), max(w.miara), max(coalesce(d.gl,0)),max(coalesce(d.dnireal,0)),
      sum(case when nzp.termdost-16-coalesce(dost.dnireal,0)<=current_date then case when nzp.typzam = 'ZBI' then pzp.kilosc - pzp.ilzreal else pzp.ilosc -pzp.ilzreal end else 0 end),
      sum(case when nzp.typzam = 'ZBI' then pzp.kilosc - pzp.ilzreal else pzp.ilosc -pzp.ilzreal end),
      sum(case when nzp.termdost-coalesce(dost.dnireal,0)<current_date then case when nzp.typzam = 'ZBI' then pzp.kilosc - pzp.ilzreal else pzp.ilosc -pzp.ilzreal end else 0 end)
    from pozzam pzp
    join nagzam nzp on pzp.zamowienie = nzp.ref
    left join wersje w on (pzp.wersjaref = w.ref)
    left join dostcen d on d.wersjaref = w.ref and (:dostawca is not null and d.dostawca = :dostawca or :dostawca is null and d.gl=1)
    left join dostawcy dost on d.dostawca = dost.ref
    where nzp.typzam in ('PRO','ZBI') and pzp.out = 1 and (pzp.ilosc - pzp.ilzreal >0 or pzp.kilosc - pzp.ilzreal >0)
      and (:dostawca is null or d.dostawca is not null) and coalesce(pzp.paramn1,0) = 0
      and coalesce(pzp.anulowano,0) = 0 and coalesce(nzp.anulowano,0) = 0
      and coalesce(nzp.anulowano,0)=0
    group by w.ref
    into :ktm, :wersjaref, :nazwa, :miara, :statusdost, :czasdostawy,
         :iw,
         :pot,
         :potpilne
  do begin
    sm = 0;
    select sum(s.ilosc) from stanyil s where s.wersjaref = :wersjaref into :sm;
    if(sm is null)then sm = 0;
    zam = 0;
    select sum(p.iloscm - p.ilzrealm)
      from nagzam n
      join pozzam p on n.ref = p.zamowienie
      where n.rejestr in ('DWY','DOS','WOD') and p.wersjaref = :wersjaref
    into :zam;
    if(zam is null)then zam = 0;
    zamowic = iw - sm - zam;
    if(zamowic < 0)then zamowic = 0;
    brak = iw - sm;
    if(brak < 0)then brak = 0;
    brakp = potpilne - sm;
    if(brakp < 0)then brakp = 0;
    stanmin = 0;
    stanmax = 0;
    dostawcagl=null;
    dostawcafirst=null;
    select first 1 d.id
      from dostcen ds
      left join dostawcy d on ds.dostawca = d.ref
      where ds.wersjaref = :wersjaref and ds.gl = 1
    into :dostawcagl;
    select first 1 d.id
      from dostcen ds
      left join dostawcy d on ds.dostawca = d.ref
      where ds.wersjaref = :wersjaref and ds.gl = 0
    into :dostawcafirst;
    suspend;
  end
end^
SET TERM ; ^
