--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REZ_ADD_ZAM(
      MAGAZYN char(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      ILOSC numeric(14,4),
      ZAM integer)
   as
declare variable cnt integer;
declare variable wydania smallint;
begin
  select TYPZAM.WYDANIA from NAGZAM left join TYPZAM on (NAGZAM.typzam = TYPZAM.symbol) where NAGZAM.ref = :zam into :wydania;
  if(:wydania is null)then wydania = 0;
  if(:wydania = 0) then exit; /* liczymy ilosci zamowione tylko dla zamowien wydania */
  if(:ilosc is null) then ilosc = 0;
  select count(*) from STANYIL where MAGAZYN=:MAGAZYN AND KTM =:KTM AND WERSJA=:wersja into :cnt;
  if(:cnt > 0) then
   update STANYIL set ZAMOWIONO = ZAMOWIONO+:ILOSC where MAGAZYN=:MAGAZYN AND KTM = :KTM AND WERSJA=:wersja;
  else
   insert into STANYIL(MAGAZYN, KTM, WERSJA, ILOSC, CENA, ZAMOWIONO)
   values(:MAGAZYN,:KTM,:wersja,0,0,:ILOSC);
end^
SET TERM ; ^
