--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_DIVIDE(
      ORGREF integer)
  returns (
      STATUS integer)
   as
declare variable refnewdokumnag integer;
declare variable czyzmianailosci integer;
declare variable dokpoziloscdorozb Numeric(14,4);
declare variable dokpozilosc Numeric(14,4);
declare variable dokpozref integer;
begin
  status = 0;
  -- nie rozbijamy dokumentu ktory ma juz jakies noty
  -- gdyż ta metoda nie powoduje klonowania rozpisek, a wiec nie sklonuje ewentualnych DOKUMNOTP
  if(exists(select first 1 1
    from dokumpoz
    join dokumroz on (dokumroz.pozycja=dokumpoz.ref)
    join dokumnotp on (dokumnotp.dokumrozkor=dokumroz.ref)
    where dokumpoz.dokument=:orgref)) then exception DOKUMROZ_HASNOTKOR;
  -- rozbicie dokumentu
  select count(*) from dokumpoz where dokumpoz.dokument = :orgref and dokumpoz.ilosc <> dokumpoz.iloscdorozb into :czyzmianailosci;
  if (czyzmianailosci <> 0) then begin
    execute procedure DOKUMNAG_CREATE_COPY(:orgref) returning_values :refnewdokumnag;
    update dokumnag set dokumnag.akcept = 9, dokumnag.numreczna = 1 where dokumnag.ref = :orgref;
    for
      select dokumpoz.ref,  dokumpoz.iloscdorozb, dokumpoz.ilosc
      from dokumpoz
      where dokumpoz.dokument = :ORGREF
      into :dokpozref, :dokpoziloscdorozb, :dokpozilosc
    do begin
      if (:dokpoziloscdorozb > 0 and :dokpoziloscdorozb < :dokpozilosc) then
        update dokumpoz
          set dokumpoz.ilosc = dokumpoz.iloscdorozb
          where dokumpoz.ref = :dokpozref;
      else if (:dokpoziloscdorozb = 0) then
        delete from dokumpoz where dokumpoz.ref = :dokpozref;
    end
    update dokumnag set dokumnag.akcept = 1 where dokumnag.ref = :orgref;
    status = 1;
  end else status = 0;
  suspend;
 -- exception test_break 'TEST';
end^
SET TERM ; ^
