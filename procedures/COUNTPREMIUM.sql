--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COUNTPREMIUM(
      TOCOUNT integer,
      MPP numeric(14,2),
      ECONTRACTDEF integer,
      PENALTY integer)
  returns (
      COUNTED integer)
   as
DECLARE VARIABLE COUNTER INTEGER;
DECLARE VARIABLE ECONTRPAYROLLSNAG INTEGER;
DECLARE VARIABLE ECONTRPAYROLL INTEGER;
DECLARE VARIABLE POINTS NUMERIC(14,2);
DECLARE VARIABLE BONUSPERCENT NUMERIC(14,2);
DECLARE VARIABLE OPERATOR INTEGER;
DECLARE VARIABLE DOCUMENT INTEGER;
DECLARE VARIABLE BDATE DATE;
DECLARE VARIABLE EDATE DATE;
DECLARE VARIABLE SYMBOL VARCHAR(20);
DECLARE VARIABLE AM SMALLINT;
DECLARE VARIABLE SM SMALLINT;
DECLARE VARIABLE OM SMALLINT;
DECLARE VARIABLE BLAD VARCHAR(30);
DECLARE VARIABLE AO INTEGER;
DECLARE VARIABLE SO INTEGER;
DECLARE VARIABLE OMOA03 INTEGER;
declare variable procxx varchar(40);
begin
  counted = 0;
  if (PENALTY = 0) then
  begin
    --gdy premie
    counter = 0;
    for
      select ECONTRPAYROLLSNAG.ref, ECONTRPAYROLLSNAG.sumpoints,
          ECONTRPAYROLLSNAG.econtrpayroll
        from ECONTRPAYROLLSNAG
        where ECONTRPAYROLLSNAG.premcalc = 1
        order by ECONTRPAYROLLSNAG.sumpoints desc
        into :econtrpayrollsnag, :points,
            :econtrpayroll
    do begin
      counter = :counter + 1;
      if (:counter <= :tocount) then
      begin
        select econtractspremstep.bonuspercent
          from econtractspremstep
          where econtractspremstep.place = :counter
            and  econtractspremstep.econtractsdef = :econtractdef
        into :bonuspercent;
        if (:bonuspercent is not null and :bonuspercent <> 0) then
        begin
          counted = :counted + 1;
          insert into econtrpayrollspos (epresencelistspos, countnag,econtrpayrolls, econtrpayrollnag, accord, stable, sref, ssymbol,
              points, topay, econtractsdef, econtractsposdef, sdate)
          values (null, 0, :econtrpayroll, :econtrpayrollsnag, 2, 'PREMIUM', null, 'Premia za wynik '||:counter,
              :bonuspercent*:points/100,:bonuspercent*:points/100*:mpp,:econtractdef,null,current_date);

        end
      end
    end
  end
/*  else
  begin
    --gdy kary
    for
      select ECONTRPAYROLLSNAG.ref, ECONTRPAYROLLSNAG.econtrpayroll,
          ECONTRPAYROLLSNAG.operator, ECONTRPAYROLLSNAG.bdate, ECONTRPAYROLLSNAG.edate
        from ECONTRPAYROLLSNAG
        where ECONTRPAYROLLSNAG.premcalc = 1
        order by ECONTRPAYROLLSNAG.sumpoints desc
        into :econtrpayrollsnag, :econtrpayroll,
          :operator, :bdate, :edate
    do begin
      for select DOKUMNAG.ref, DOKUMNAG.symbol, DOKUMNAG. acceptmistake,
          DOKUMNAG.sellmistake, DOKUMNAG.othermistake,
          DOKUMNAG.acceptoper, DOKUMNAG.selloper, DOKUMNAG.omoa03
        from DOKUMNAG
        where (acceptoper = :operator and acceptmistake = 1) or
              (selloper = :operator and sellmistake = 1) or
              (othermistake = 1) and
              cast(DOKUMNAG.data as date) > :bdate and cast(DOKUMNAG.data as date) < :edate
        into :document, :symbol, :am,
              :sm, :om,
              :ao, :so, :omoa03
      do begin
        --tutaj liczenie punktow karnych za WZ
        select calcproc
          from econtractsdef
          where description = 'Kary'
          into :procxx;
        execute statement 'select points from '||:procxx||'('||:document||', '||:mpp||', '||:operator||')'
          into :points;
        if (:am = 1 and :ao = :operator) then
          blad = 'Błąd magazynu';
        if (:sm = 1 and :so = :operator) then
            blad = 'Błąd biura';
        if (:om = 1 and :omoa03 = :operator) then
        begin
          if (blad = '') then
            blad = 'Błąd inny';
          else
            blad = :blad||', inny';
        end
        if (:points is not null) then
        begin
          counted = :counted + 1;
          insert into econtrpayrollspos (epresencelistspos, countnag,econtrpayrolls, econtrpayrollnag, accord, stable, sref, ssymbol,
              points, topay, econtractsdef, econtractsposdef, sdate)
            values (null, 0, :econtrpayroll, :econtrpayrollsnag, 2, 'DOKUMNAGPENALTY', :document, :blad||': '||:symbol,
              :points,:points*:mpp,:econtractdef,null,current_date);
        end
      end
    end
  end*/
  update ECONTRPAYROLLSNAG set PREMCALC = 0 where PREMCALC = 1;
end^
SET TERM ; ^
