--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ADD_MSG(
      MSG_OLD varchar(4096) CHARACTER SET UTF8                           ,
      MSG_NEW varchar(4096) CHARACTER SET UTF8                           )
  returns (
      MSG varchar(4096) CHARACTER SET UTF8                           )
   as
declare variable eom string3;
begin
eom = '
';

  msg = '';
  if(coalesce(char_length(:msg_old),0) + coalesce(char_length(:msg_new),0) <= 4093) then msg = :msg_old || :msg_new; -- [DG] XXX ZG119346
  else msg = :msg_old || '...';

  suspend;
end^
SET TERM ; ^
