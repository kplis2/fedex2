--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CENNIK_OBLICZREG(
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      TRYB integer,
      ZRODLO integer,
      WARTOSC numeric(14,2),
      PRZELICZ float,
      ZRODLOMARZY integer,
      KURS numeric(14,4),
      STARACENANET numeric(14,4),
      STALACENA integer,
      TOLERANCJA numeric(14,2),
      REGULA smallint,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      CENNIKNAD integer)
  returns (
      CENANET numeric(14,4),
      CENABRU numeric(14,4),
      MARZA numeric(14,2),
      NARZUT numeric(14,2))
   as
declare variable cenabazowa numeric(14,4);
declare variable cenamarzy numeric(14,4);
declare variable vat numeric(14,2);
declare variable cena_zakn numeric(14,4);
declare variable cena_kosztn numeric(14,4);
declare variable cena_fabn numeric(14,4);
declare variable cena_mag numeric(14,4);
declare variable cena_cen numeric(14,4);
begin
  if(:PRZELICZ is null or (:PRZELICZ=0)) then PRZELICZ = 1;
  if(:WARTOSC is null) then WARTOSC = 0;
  if(:TRYB is null) then TRYB = 0;
  if(:ZRODLO is null) then ZRODLO = 0;
  if(:ZRODLOMARZY is null) then ZRODLOMARZY = 0;
  if(:kurs is null or (:kurs = 0)) then kurs = 1;

  /*okreslenie ceny dla marzy*/
  select CENA_ZAKN,CENA_KOSZTN,CENA_FABN
    from WERSJE
    where REF=:WERSJAREF
  into :cena_zakn,:cena_kosztn,:cena_fabn;

  if(:ZRODLOMARZY=0) then cenamarzy = :cena_zakn;
  else if(:ZRODLOMARZY=1) then cenamarzy = :cena_kosztn;
  else if(:ZRODLOMARZY=2) then cenamarzy = :cena_fabn;
  else if(:ZRODLOMARZY=3) then cenamarzy = :cena_mag;
  if(:TRYB=3) then begin
    CENANET = :WARTOSC;
    cenamarzy = :cenamarzy / :kurs;
  end
  else begin
    cenabazowa = 0;
    CENANET = 0;
    /*okreslenie ceny bazowej*/
    if(:ZRODLO=0) then cenabazowa = :cena_zakn;
    else if(:ZRODLO=1) then cenabazowa = :cena_kosztn;
    else if(:ZRODLO=2) then cenabazowa = :cena_fabn;
    else if(:ZRODLO=3) then begin
      cena_mag = 0;
      select max(s.cena) from stanycen s where s.magazyn = :magazyn and s.wersjaref = :wersjaref and s.ilosc > 0 into :cena_mag;
      if(:cena_mag is null or :cena_mag = 0) then cenabazowa = :staracenanet*(100-:WARTOSC)/100;
      else cenabazowa = :cena_mag;
    end
    else if (:ZRODLO=4) then begin
      cena_cen = 0;
      select c.cenanet
        from cennik c
        where c.wersjaref = :wersjaref
          and c.cennik = :cenniknad
      into :cena_cen;

      if(:cena_cen is null or :cena_cen = 0) then cenabazowa = :staracenanet*(100-:WARTOSC)/100;
      else cenabazowa = :cena_cen;

    end
    cenabazowa = :cenabazowa / :kurs;
    if(:STALACENA=0) then begin
      if(:TRYB=0) then CENANET = :cenabazowa*100/(100-:WARTOSC);
      else if(:TRYB=1) then CENANET = :cenabazowa*(100+:WARTOSC)/100;
      else if(:TRYB=2) then CENANET = :cenabazowa*(100-:WARTOSC)/100;
    end else CENANET = :STARACENANET;
  end
  select VAT.STAWKA from TOWARY join VAT on (TOWARY.vat=VAT.grupa) where TOWARY.KTM=:ktm into :vat;
  if(:vat is null) then vat = 0;
  vat = 1+:vat/100;
  if(:STALACENA=0) then CENANET = :CENANET * :przelicz;
  if(:STARACENANET is not null and :staracenanet > 0) then
  begin
    if( (:CENANET/:STARACENANET) < (1 + :TOLERANCJA/100)
       and (:CENANET/:STARACENANET) > (1 - :TOLERANCJA/100) and STALACENA = 0
       and :REGULA = 1 ) then
    begin
      CENANET = :STARACENANET;
    end
  end
  CENABRU = :CENANET * :vat;
  cenamarzy = :cenamarzy * :przelicz;
  if(:CENANET>0) then MARZA = (:CENANET - :cenamarzy) * 100 / :CENANET;
  else MARZA = 0;
  if(:cenamarzy>0) then NARZUT = (:CENANET - :cenamarzy) * 100 / :cenamarzy;
  else NARZUT = 100;
end^
SET TERM ; ^
