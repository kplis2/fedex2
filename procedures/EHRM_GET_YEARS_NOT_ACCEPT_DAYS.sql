--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EHRM_GET_YEARS_NOT_ACCEPT_DAYS(
      EMPLOYEEREF EMPLOYEES_ID)
  returns (
      YEARS SMALLINT_ID)
   as
begin
    for select distinct d.pyear from emplcaldays d
     join employees e on d.employee = e.ref
     left join EMPLSUPERIORS es on(es.employee = e.ref)   --PR55566
     where es.superior = :employeeref
     order by d.pyear desc
    into :years
     do begin
        suspend;
     end
end^
SET TERM ; ^
