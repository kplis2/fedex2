--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_OFERTY_OFERTA_ATRYBUTY(
      WERSJAREF integer)
  returns (
      ATRYBUTY varchar(2048) CHARACTER SET UTF8                           )
   as
declare variable wartosc varchar(255);
declare variable nazwa varchar(255);
begin
atrybuty = '';
  for select a.wartosc, c.nazwa
   from atrybuty a left join defcechy c on (a.cecha = c.symbol)
   where a.wersjaref = :wersjaref
   order by c.lp
   into :wartosc, :nazwa
  do begin
    if(:wartosc is null) then wartosc = '';
    if(:nazwa is null) then nazwa = '';
    if(coalesce(char_length(:atrybuty || :wartosc || :nazwa),0) < 2040) then begin -- [DG] XXX ZG119346
      nazwa = replace(:nazwa,'|',',');
      wartosc = replace(:wartosc,'|',',');
      atrybuty = :atrybuty ||:nazwa||': '||:wartosc||'; ';
    end
  end
  suspend;
end^
SET TERM ; ^
