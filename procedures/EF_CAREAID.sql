--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_CAREAID(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable fromdate timestamp;
  declare variable todate timestamp;
  declare variable invalidity smallint;
  declare variable invalidityfrom timestamp;
  declare variable invalidityto timestamp;
  declare variable careaid smallint;
  declare variable birthdate timestamp;
  declare variable yage smallint;
  declare variable mage smallint;
  declare variable dage smallint;
begin

--MW personel: dodatek rehabilitacyjny
  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  ret = 0;

--dodatek rehabilitacyjny przysugujący na czlonków rodziny

  for
    select careaid
      from emplfamily
      where careaid = 1 and employee = :employee
        and caidfrom <= :fromdate
        and caidto >= :todate
      into :careaid
  do begin
    ret = ret + 144;
  end

--dodatek rehabilitacyjny z tytulu niepelnosprawnosci pracownika

  select Z.invalidity, Z.invalidityfrom, Z.invalidityto
    from ezusdata Z join employees E on (Z.person = E.person)
    where E.ref = :employee and Z.invalidityfrom < :todate
      and (Z.invalidityto is null or invalidityto > :fromdate) and aid = 1
    into :invalidity, :invalidityfrom, :invalidityto;

--niepełnosprawnemu dziecku
  if (invalidity = 254) then
    ret = ret + 144;

--osobie niepełnosprawnej w wieku powyżej 16 roku życia, jeżeli legitymuje się
--orzeczeniem o znacznym stopniu niepełnosprawności
  if (invalidity = 86) then
    ret = ret + 144;

--osobie niepełnosprawnej w wieku powyżej 16 roku życia, jeśli została uznana
--za niepełnosprawną w stopniu umiarkowanym, a niepełnosprawność powstała w wieku
--do ukończenia 21 lat,
  if (invalidity = 85) then
    begin
      select P.birthdate
        from persons P join employees E on (E.person = P.ref)
        where E.ref = :employee
      into :birthdate;

      yage = cast (substring (invalidityfrom from 1 for 4) as integer) - cast (substring (birthdate from 1 for 4) as integer);
      mage = cast (substring (invalidityfrom from 6 for 2) as integer) - cast (substring (birthdate from 6 for 2) as integer);
      dage = cast (substring (invalidityfrom from 9 for 2) as integer) - cast (substring (birthdate from 9 for 2) as integer);
      if (yage < 21 or (yage = 21 and mage < 0) or (yage = 21 and mage = 0 and dage < 0))
        then ret = ret + 144;
    end

  suspend;
end^
SET TERM ; ^
