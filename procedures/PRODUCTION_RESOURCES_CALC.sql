--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRODUCTION_RESOURCES_CALC(
      VERS integer,
      PRSCHEDGUIDEPOS integer,
      PRPOZZAM integer)
   as
declare variable WH varchar(3);
declare variable good varchar(40);
declare variable versno integer;
begin
  delete from prscheduleresources;
  if (vers > 0) then
  begin
    select ktm, nrwersji from wersje where ref = :vers
      into good, versno;
    if (prschedguidepos > 0) then
      select p.warehouse from prschedguidespos p where p.ref = :prschedguidepos
        into wh;
    else if (prpozzam > 0) then
      select p.magazyn from pozzam p where p.ref = :prpozzam
        into wh;
    if (wh <> '' and wh is not null) then
    begin
      insert into prscheduleresources (ondate, symbol, plusq, minusq, quantityafter)
      select r.data, :good, r.ilplus, r.ilminus, r.ilafter
        from br_stanyrez(:wh, :good, :versno, 1) r;
    end
  end
end^
SET TERM ; ^
