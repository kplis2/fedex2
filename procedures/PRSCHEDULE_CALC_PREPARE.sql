--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDULE_CALC_PREPARE(
      PRSCHEDULE integer,
      PRSCHEDZAM integer,
      PRSCHEDGUIDE integer,
      PRSCHEDOPER integer,
      MODE integer,
      CLEAROPER smallint,
      ONLYWAITINGGUIDE smallint,
      TIMESTARTFROM timestamp = current_timestamp(0))
  returns (
      OPERCOUNT integer)
   as
declare variable curnum integer;
declare variable sql varchar(255);
declare variable schednum integer;
declare variable guidestatus integer;
begin
  if (timestartfrom is null or timestartfrom < current_timestamp(0)) then
    timestartfrom = current_timestamp(0);
  guidestatus = 10;
  if(:onlywaitingguide = 1) then
    guidestatus = 0;
  --przygotowanie do naliczania
  if(:mode < 3) then begin-- 3 oznacza kontynuacje poprzedniego
    update PRGANTT set verified = 1 where verified = 0 and gtype = 0;--zaznaczenie, ze wszystkieoepracje sa zweryfikowae
  end
  --jesli jakims przypadkiem operacje zrealizowane maja czasy w  rozpoczecia przyszlosci, to naprawienie tego - ustawieni im czasow null'owych
  update PRGANTT set timefrom = NULL where
    timefrom > current_timestamp(0) and status > 2 and timefrom >= :timestartfrom  and gtype = 0;
  update PRGANTT set timeto = current_timestamp(0)
    where timefrom < current_timestamp(0) and timeto > current_timestamp(0) and status > 2 and timefrom >= :timestartfrom  and gtype = 0;
  if(:mode < 4) then
  begin
    if(prschedoper > 0) then
    begin
      select schedule, schedzam, guide, number
        from PRSCHEDOPERS where  PRschedopers.ref=:prschedoper
        into :prschedule, :prschedzam, :prschedguide, :curnum;
      update PRSCHEDOPERS set VERIFIED = 0
        where guide = :prschedguide
        and status in (0,1,2)
        and guidestatus <= :guidestatus and schedblocked = 0
        and ((:mode = 0 and PRSCHEDOPERS.number = :curnum)
            or(:mode = 1 and PRSCHEDOPERS.number >= :curnum)
            or(:mode = 2 and PRSCHEDOPERS.number <= :curnum)
            or(:mode = 3)
            );
    end else if(prschedguide > 0) then begin
      select prschedule, prschedzam, numer from PRSCHEDGUIDES where  PRSCHEDGUIDES.ref=:prschedguide
        into :prschedule, :prschedzam, :curnum;
      update PRSCHEDOPERS set VERIFIED = 0, CALCDEPTIMES = 0, TIMECONFLICT = 0
        where schedule = :prschedule and schedzam = :prschedzam
        and (status = 0 or (status = 1) or (status = 2) )
        and guidestatus <= :guidestatus and schedblocked = 0
        and activ = 1
        and ( (:mode = 0 and PRSCHEDOPERS.guidenum = :curnum)
            or(:mode = 1 and PRSCHEDOPERS.guidenum >= :curnum)
            or(:mode = 2 and PRSCHEDOPERS.guidenum <= :curnum)
            or(:mode = 3)
            );
    end else if(prschedzam > 0) then begin
      select prschedule,  number from PRSCHEDZAM where  PRSCHEDZAM.ref=:prschedzam
        into :prschedule, :curnum;
      update PRSCHEDOPERS set VERIFIED = 0, CALCDEPTIMES = 0 , TIMECONFLICT = 0
        where schedule = :prschedule
        and (status = 0 or (status = 1) or (status = 2) )
        and activ = 1
        and guidestatus <= :guidestatus and schedblocked = 0
        and ( (:mode = 0 and PRSCHEDOPERS.schedzamnum = :curnum)
            or(:mode = 1 and PRSCHEDOPERS.schedzamnum >= :curnum)
            or(:mode = 2 and PRSCHEDOPERS.schedzamnum <= :curnum)
            or(:mode = 3)
            );
    end else
    if(prschedule > 0) then
      update prgantt set VERIFIED = 0 where prschedule = :prschedule  and gtype = 0;
  end else
  begin
    --kontunuacja poprzedniego - sprawdzenie, czy nie wchodzi na cudzy harmonogram
    if(:prschedule > 0) then
      update PRGANTT set verified = 1 where verified = 0 and prschedule <> :prschedule and gtype = 0;--zaznaczenie, ze wszystkieoepracje sa zweryfikowae
  end
  --wyczyszczenie danych
  if(:clearoper = 1)then
  begin
    update PRGANTT set TIMEFROM = null, TIMETO = null, PRMACHINE = null,
        MATERIALREADYTIME = null, STATUSMAT = null,
        status = (case when status < 2 then 0 else status end)--zmiana statusu
      where
        PRSCHEDULE = :prschedule and VERIFIED = 0 and gtype = 0;
  end
  --blokada wyliczania czasuow zakocznienai i rozpoczecia do przewodnikow
  select count(*) from PRGANTT where prschedule = :prschedule and VERIFIED = 0
    into :opercount;
end^
SET TERM ; ^
