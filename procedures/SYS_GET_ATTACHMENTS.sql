--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_GET_ATTACHMENTS(
      FORTRANSACTION integer = NULL)
  returns (
      REMOTEADRES varchar(255) CHARACTER SET UTF8                           ,
      APPLICATION varchar(255) CHARACTER SET UTF8                           ,
      OPERATOR varchar(255) CHARACTER SET UTF8                           ,
      SERVERPID integer,
      ATTACHMETSNAME varchar(255) CHARACTER SET UTF8                           ,
      ATTIMESTAMP timestamp,
      APPNAME varchar(255) CHARACTER SET UTF8                           ,
      ATTACHMENT integer,
      REMOTEPID integer,
      USERDB varchar(40) CHARACTER SET UTF8                           ,
      TRCNT integer,
      LONGTRCNT integer,
      OPERATORREF integer,
      MINTRANSACTIONTIME timestamp)
   as
begin
  if(:fortransaction=0) then fortransaction = NULL;
  for select a.mon$remote_address, ss.application, min(coalesce(o.nazwa, o2.nazwa)), a.mon$server_pid, a.mon$attachment_name, a.mon$timestamp, a.mon$remote_process,
    a.mon$attachment_id, a.mon$remote_pid, a.mon$user, count(t.mon$transaction_id),
    sum(iif(coalesce(t.mon$state,0) = 1 and cast(current_timestamp(0) - coalesce(t.mon$timestamp,current_timestamp(0)) as double precision)*60*24 > 2,1,0)),
    min(coalesce(o.ref,o2.ref)),
    min(t.mon$timestamp)
  from mon$attachments a
    left join s_sessions ss on (ss.connectionid = a.mon$attachment_id and ss.number = 0)
    left join operator o on (o.userid = ss.userid)
    left join globalparams g on (g.connectionid = a.mon$attachment_id and g.psymbol = 'AKTUOPERATOR')
    left join operator o2 on (o2.ref = g.pvalue)
    left join mon$transactions t on (t.mon$attachment_id = a.mon$attachment_id)
    where (t.mon$transaction_id=:fortransaction or :fortransaction is null)
  group by a.mon$remote_address, a.mon$server_pid, a.mon$attachment_name, a.mon$timestamp, a.mon$remote_process,
    a.mon$attachment_id, a.mon$remote_pid, a.mon$user, a.mon$attachment_id, ss.application
  into :REMOTEADRES, :APPLICATION, :OPERATOR, :SERVERPID, :ATTACHMETSNAME, :ATTIMESTAMP, :APPNAME, :ATTACHMENT,
       :REMOTEPID, :USERDB, :TRCNT, :LONGTRCNT, :OPERATORREF, :MINTRANSACTIONTIME
  do begin
    suspend;
  end
end^
SET TERM ; ^
