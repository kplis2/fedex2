--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAV_FRDHDRS
  returns (
      REF integer,
      PARENT integer,
      NUMBER integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      HINT varchar(255) CHARACTER SET UTF8                           ,
      KINDSTR varchar(20) CHARACTER SET UTF8                           ,
      ACTIONID varchar(60) CHARACTER SET UTF8                           ,
      APPLICATIONS varchar(60) CHARACTER SET UTF8                           ,
      FORMODULES varchar(60) CHARACTER SET UTF8                           ,
      RIGHTS varchar(255) CHARACTER SET UTF8                           ,
      RIGHTSGROUP varchar(255) CHARACTER SET UTF8                           ,
      ISNAVBAR smallint,
      ISTOOLBAR smallint,
      ISWINDOW smallint,
      ISMAINMENU smallint,
      ITEMTYPE smallint,
      GROUPPROCEDURE varchar(60) CHARACTER SET UTF8                           ,
      CHILDRENCOUNT smallint,
      RIBBONTAB varchar(40) CHARACTER SET UTF8                           ,
      RIBBONGROUP varchar(40) CHARACTER SET UTF8                           ,
      NATIVETABLE varchar(40) CHARACTER SET UTF8                           ,
      POPUPMENU varchar(40) CHARACTER SET UTF8                           ,
      NATIVEFIELD varchar(40) CHARACTER SET UTF8                           ,
      FORADMIN smallint)
   as
begin
  parent = NULL;
  hint = '';
  actionid = 'DisplayFRDPLANSFromGlobal';
  isnavbar = 0;
  istoolbar = 1;
  iswindow = 0;
  ismainmenu = 0;
  itemtype = 0;
  childrencount = 0;
  ribbontab = '';
  ribbongroup = '';
  nativetable = 'FRDHDRS';
  popupmenu = 'PopupFRDHDRS';
  groupprocedure = '';
  kindstr = 'MI_FKBOOKA';
  formodules = '';
  foradmin = 0;
  number = 1;
  for select D.REF, D.SHORTNAME, D.READRIGHTS, D.READRIGHTSGROUP, H.APPLICATION
  from FRDHDRS D
  JOIN FRHDRS H ON ( D.FRHDR = H.SYMBOL )
  order by SHORTNAME
  into :REF, :NAME, :RIGHTS, :RIGHTSGROUP, :applications
  do begin
    suspend;
    number = :number + 1;
  end
end^
SET TERM ; ^
