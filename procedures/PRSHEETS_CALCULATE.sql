--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHEETS_CALCULATE(
      SHEET integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      RODZAJ char(1) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE EGROSSQUANTITY VARCHAR(1024);
DECLARE VARIABLE ENETQUANTITY VARCHAR(1024);
DECLARE VARIABLE ECONDITION VARCHAR(1024);
DECLARE VARIABLE MATREF INTEGER;
DECLARE VARIABLE KTM VARCHAR(80);
DECLARE VARIABLE WERSJA INTEGER;
DECLARE VARIABLE MATTYPE INTEGER;
DECLARE VARIABLE CONDITION NUMERIC(14,4);
DECLARE VARIABLE NETQUANTITY NUMERIC(14,4);
DECLARE VARIABLE GROSSQUANTITY NUMERIC(14,4);
DECLARE VARIABLE EOPERFACTOR VARCHAR(1024);
DECLARE VARIABLE OPERFACTOR NUMERIC(14,4);
DECLARE VARIABLE EPLACETIME VARCHAR(1024);
DECLARE VARIABLE PLACETIME NUMERIC(14,4);
DECLARE VARIABLE EOPERTIME VARCHAR(1024);
DECLARE VARIABLE OPERTIME NUMERIC(14,2);
DECLARE VARIABLE EHOURFACTOR VARCHAR(1024);
DECLARE VARIABLE HOURFACTOR NUMERIC(14,4);
DECLARE VARIABLE ESHEETRATE VARCHAR(1024);
DECLARE VARIABLE SHEETRATE NUMERIC(14,4);
DECLARE VARIABLE STAT VARCHAR(1024);
DECLARE VARIABLE PSHEET INTEGER;
DECLARE VARIABLE RET SMALLINT;
DECLARE VARIABLE RESULT NUMERIC(14,6);
begin
/*
   RODZAJ:
   S - surowce,
   P - produkty uboczne,
   O - odpady,
*/

  --exception test_break coalesce(SHEET,'SH') ||'; '||coalesce(PREFIX,'PR')||'; ' ||coalesce(RODZAJ,'RD');
  mattype = -1;
  if(:rodzaj='S') then mattype = 0;
  if(:rodzaj='P') then mattype = 1;
  if(:rodzaj='O') then mattype = 2;
/*materialy*/
  stat = ' select REF, SHEET, ENETQUANTITY, EGROSSQUANTITY, KTM, WERSJA, ECONDITION from PRSHMAT ';
  if (sheet is not null and sheet > 0) then
    stat = stat || ' where PRSHMAT.SHEET=' || sheet || ' and PRSHMAT.MATTYPE=' || mattype;
  else
    stat = stat || ' where PRSHMAT.MATTYPE=' || mattype;
  for execute statement stat into :matref, :psheet, :enetquantity,:egrossquantity,:ktm,:wersja,:econdition
  do begin
    select ret,result from numericis(:econdition) into :ret, result;
    if(ret = 1) then econdition = result;
    if(:econdition='') then condition = 1;
    else execute procedure STATEMENT_PARSE(:econdition,:psheet,0,:prefix) returning_values :condition;
    if(:condition=1) then begin
      if(enetquantity = '') then netquantity = 1;
      else begin
        select ret,result from numericis(:enetquantity) into :ret, result;
        if(ret = 1) then netquantity = result;
        else execute procedure STATEMENT_PARSE(:enetquantity,:psheet,0,:prefix) returning_values :netquantity;
        if(:netquantity is null) then netquantity = 0;
      end
      if(egrossquantity = '') then grossquantity = 1;
      else begin
        select ret,result from numericis(:egrossquantity) into :ret, result;
        if(ret = 1) then grossquantity = result;
        else execute procedure STATEMENT_PARSE(:egrossquantity,:psheet,0,:prefix) returning_values :grossquantity;
        if(:grossquantity is null) then grossquantity = 0;
      end
      update prshmat pm set pm.condition = :condition, pm.netquantity = :netquantity, pm.grossquantity = :grossquantity where pm.ref = :matref;
    end
  end
/*operacje*/
  stat = ' select REF, SHEET, ECONDITION, EOPERFACTOR, EOPERTIME, EPLACETIME from PRSHOPERS ';
  if (:sheet is not null and :sheet > 0)then stat = stat || ' where PRSHOPERS.SHEET='||:sheet;
  for execute statement :stat into :matref, :psheet, :econdition, :eoperfactor, :eopertime, :eplacetime
  do begin
    select ret,result from numericis(:econdition) into :ret, result;
    if(ret = 1) then econdition = result;
    if(:econdition='') then condition = 1;
    else execute procedure STATEMENT_PARSE(:econdition,:psheet,0,:prefix) returning_values :condition;
    if(:condition=1) then begin
      if(eoperfactor = '')then operfactor = 1;
      else begin
        select ret,result from numericis(:eoperfactor) into :ret, result;
        if(ret = 1) then operfactor = result;
        else execute procedure STATEMENT_PARSE(:eoperfactor,:psheet,0,:prefix) returning_values :operfactor;
        if(:operfactor is null) then operfactor = 0;
      end
      if(eopertime = '')then opertime = 1;
      else begin
        select ret,result from numericis(:eopertime) into :ret, result;
        if(ret = 1) then opertime = result;
        else execute procedure STATEMENT_PARSE(:eopertime,:psheet,0,:prefix) returning_values :opertime;
        if(:opertime is null) then opertime = 0;
      end
      if(eplacetime = '') then placetime = 1;
      else begin
        select ret,result from numericis(:eplacetime) into :ret, result;
        if(ret = 1) then placetime = result;
        else execute procedure STATEMENT_PARSE(:eplacetime,:psheet,0,:prefix) returning_values :placetime;
        if(:placetime is null) then placetime = 0;
      end
      update prshopers po set po.condition = :condition, po.operfactor = :operfactor,
        po.opertime = :opertime, po.placetime = :PLACETIME where po.ref = :matref;
    end
  end
/*narzedzia*/
  stat = ' select REF, SHEET, ECONDITION, EHOURFACTOR, ESHEETRATE from PRSHTOOLS ';
  if (:sheet is not null and :sheet > 0)then stat = stat || ' where PRSHTOOLS.SHEET='||:sheet;
  for execute statement :stat into :matref, :psheet, :econdition, :ehourfactor, :esheetrate
  do begin
    select ret,result from numericis(:econdition) into :ret, result;
    if(ret = 1) then econdition = result;
    if(:econdition='') then condition = 1;
    else execute procedure STATEMENT_PARSE(:econdition,:psheet,0,:prefix) returning_values :condition;
    if(:condition=1) then begin
      if(ehourfactor = '')then hourfactor = 1;
      else begin
        select ret,result from numericis(:ehourfactor) into :ret, result;
        if(ret = 1) then hourfactor = result;
        else execute procedure STATEMENT_PARSE(:ehourfactor,:psheet,0,:prefix) returning_values :hourfactor;
        if(:hourfactor is null) then hourfactor = 0;
      end
      if(esheetrate = '')then sheetrate = 1;
      else begin
        select ret,result from numericis(:esheetrate) into :ret, result;
        if(ret = 1) then sheetrate = result;
        else execute procedure STATEMENT_PARSE(:sheetrate,:psheet,0,:prefix) returning_values :sheetrate;
        if(:sheetrate is null) then sheetrate = 0;
      end
      update prshtools pt set pt.condition = :condition, pt.hourfactor = :hourfactor,
        pt.sheetrate = :sheetrate where pt.ref = :matref;
    end
  end
end^
SET TERM ; ^
