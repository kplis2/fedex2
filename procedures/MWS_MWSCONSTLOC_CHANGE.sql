--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_MWSCONSTLOC_CHANGE(
      MWSORDTYPE integer,
      MWSCONSTLOCPS varchar(40) CHARACTER SET UTF8                           ,
      MWSCONSTLOCP integer,
      MWSCONSTLOCLS varchar(40) CHARACTER SET UTF8                           ,
      MWSCONSTLOCL integer,
      AUTOCOMMIT smallint,
      ACTUOPERATOR integer,
      MWSSTOCKCHECK smallint,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      PRIORITY smallint)
  returns (
      MWSORD integer)
   as
declare variable wh varchar(3);
declare variable operator integer;
declare variable operatordict varchar(80);
declare variable blocksymbol varchar(40);
declare variable timestart timestamp;
declare variable timestartdcl timestamp;
declare variable timestopdcl timestamp;
declare variable refmwsord integer;
declare variable mwsaccessory integer;
declare variable period varchar(6);
declare variable branch varchar(10);
declare variable mix smallint;
declare variable good varchar(40);
declare variable paltype varchar(40);
declare variable vers integer;
declare variable wharealogl integer;
declare variable wharealogp integer;
declare variable whareap integer;
declare variable whareal integer;
declare variable maxnumber integer;
declare variable quantity numeric(14,4);
declare variable realtime double precision;
declare variable mwspalloclp integer;
declare variable mwspallocl integer;
declare variable dist numeric(14,4);
declare variable stocktaking smallint;
declare variable rec smallint;
declare variable cor smallint;
declare variable mwspallocp integer;
declare variable mwsactref integer;
declare variable lot integer;
declare variable levelnump integer;
declare variable levelnuml integer;
declare variable operatortmp integer;
declare variable mixtakepal integer;
declare variable mixleavepal integer;
declare variable mixmwsconstlocl integer;
declare variable mixmwspallocl integer;
declare variable pall numeric(14,2);
declare variable palw numeric(14,2);
declare variable palh numeric(14,2);
declare variable mixrefill smallint;
declare variable actcnt integer;
declare variable deepl smallint;
declare variable refill smallint;
declare variable mwsconstloclsymb varchar(40);
declare variable x_partia date_id;              -- XXX KBI
declare variable x_serial_no integer_id;        -- XXX KBI
declare variable x_slownik integer_id;          -- XXX KBI
begin
  rec = 0;
  cor = 0;
  stocktaking = 0;
  if (mwsordtype = 0 or mwsordtype is null) then
  begin
    select first 1 m.ref from mwsordtypes m where m.mwsortypedets = 11
      into mwsordtype;
  end
  if (mwsstockcheck is null) then mwsstockcheck = 1;
  if (autocommit is null) then autocommit = 0;
  if (descript is null) then descript = '';
  select ref, deep from mwsconstlocs where symbol = :mwsconstlocls
    into mwsconstlocl, deepl;
  if (deepl is null) then deepl = 0;
  if (deepl = 1) then
  begin
    priority = 1;
    descript =  descript||' Uzupelnienie deep';
  end
  else
    descript = descript||' Przesuniecie palety';
  select ref from mwsconstlocs where symbol = :mwsconstlocps into mwsconstlocp;
  if (mwsordtype is null) then exception MWSORDTYPE_NOT_SET;
  if (mwsstockcheck = 1) then
  begin
    if (exists(select ref from mwsstock where mwsconstloc = :mwsconstlocl and ispal = 0)) then
      exception MWSORD_ALREADY_PLANED 'zaplanowano zlecenie dla lokacji koncowej '||mwsconstlocps||' '||mwsconstlocls;
    if (exists(select ref from mwsstock where mwsconstloc = :mwsconstlocp and ispal = 0 and blocked > 0)) then
      exception MWSORD_ALREADY_PLANED 'zaplanowano zlecenie dla lokacji poczatkowej '||mwsconstlocps||' '||mwsconstlocls;
  end
  select wh from mwsconstlocs where ref = :mwsconstlocp into wh;
  select oddzial from defmagaz where symbol = :wh into branch;
  execute procedure gen_ref('MWSORDS') returning_values refmwsord;
  select okres from datatookres(current_date,0,0,0,0) into period;
  insert into mwsords (ref, mwsordtype, stocktaking, doctype, operator, priority, description, wh,
      regtime, timestartdecl, timestopdecl, mwsaccessory, branch, period, cor, rec,
      bwharea, ewharea, status)
    values (:refmwsord, :mwsordtype, :stocktaking, 'P', :operator, :priority, :descript, :wh,
      current_timestamp(0), :timestart, current_timestamp(0), :mwsaccessory, :branch, :period, :cor, :rec,
      null, null, 0);
  -- POZYCJE ZLECENIA MAGAZYNOWEGO
  if (mwsconstlocl is null) then
  begin
    select count(ref) from mwsstock where mwsconstloc = :mwsconstlocp and quantity > 0
      into :mix;
    select first 1 m.good
      from mwsstock m
        left join towary t on (t.ktm = m.good)
      where t.paleta = 1 and m.mwsconstloc = :mwsconstlocp
      into paltype;
    select first 1 m.good, m.vers, mc.l, mc.w, mc.h
      from mwsstock m
        join mwsconstlocs mc on (mc.ref = m.mwsconstloc)
        left join towary t on (t.ktm = m.good)
      where t.paleta <> 1 and m.mwsconstloc = :mwsconstlocp
      into good, vers, pall, palw, palh;
    -- sprawdzamy czy przesuwamy mixa
    if (mix > 2) then mix = 1; else mix = 0;
    execute procedure XK_MWS_GET_BEST_LOCATION(:good, :vers,null,:refmwsord,:mwsordtype,
        :wh, null, null, 3, 1, 0, :mix,null,:paltype, :palw,:pall,:palh,0,null)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end
  else
  begin
    select wharea, wharealogfromstartarea from mwsconstlocs where ref = :mwsconstlocl
      into whareal, wharealogl;
  end
  select wharea, wharealogfromstartarea from mwsconstlocs where ref = :mwsconstlocp
    into whareap, wharealogp;
  maxnumber = 0;
  if (mwsconstlocl is not null) then
  begin
    select symbol from mwsconstlocs where ref = :mwsconstlocl into MWSCONSTLOCLSYMB;
    operatortmp = null;
    for
      select good, vers, quantity, mwspalloc,
          mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, mixrefill,
          x_partia, x_serial_no, x_slownik     -- XXX KBI
        from mwsstock
        where mwsconstloc = :mwsconstlocp and quantity > 0
        into good, vers, quantity, mwspallocp,
          mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, mixrefill,
          :x_partia, :x_serial_no, :x_slownik
    do begin
      select mwsstandlevelnumber
        from mwsconstlocs
        where ref = :mwsconstlocp
      into levelnump;
      select mwsstandlevelnumber
        from mwsconstlocs
        where ref = :mwsconstlocl
      into levelnuml;
      if (levelnump is null) then levelnump = 0;
      if (levelnuml is null) then levelnuml = 0;
      maxnumber = maxnumber + 1;
      execute procedure gen_ref('MWSACTS') returning_values mwsactref;
      insert into mwsacts (ref,mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
          mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
          regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, recdoc, plus,
          whareap, whareal, wharealogp, wharealogl, number, disttogo, mixedpallgroup, palgroup,
          mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, refilltry,
          x_partia, x_serial_no, x_slownik) -- XXX KBI
        values (:mwsactref,:refmwsord, 0, :stocktaking, :good, :vers, :quantity, :mwsconstlocp,  :mwspallocp,
            :mwsconstlocl, :mwspallocp, null, null, 'O', 0, 0, :wh, :whareap, null,
            current_timestamp(0), :timestartdcl, :timestopdcl, :realtime, null, null, 1, :rec, 1,
            null, :whareal, null, :wharealogl, :maxnumber, :dist, :mix, null,
            :mixtakepal, :mixleavepal, :mixmwsconstlocl, :mixmwspallocl, :mixrefill,
            :x_partia, :x_serial_no, :x_slownik); -- XXX KBI);
     update mwsacts set status = 1 where ref = :mwsactref;
      timestart = timestartdcl;
    end
  end
  -- na koniec jest komunikat ze nie ma wolnego miejsca na magazynie
  else
    exception XK_NO_PLACE_IN_WHSEC 'Brak okreslonego miejsca dla palety';
  select count(ref) from mwsacts where mwsord = :refmwsord into actcnt;
  if (actcnt is null) then actcnt = 0;
  if (actcnt > 0) then
  begin
    update mwsords set status = 1, timestopdecl = :timestopdcl, operator = null, mwsaccessory = :mwsaccessory
      where ref = :refmwsord and status = 0;
    update mwsords mo set mo.operator = null where mo.ref = :refmwsord;
    if (autocommit = 1) then
    begin
      update mwsacts ma set ma.status = 2, ma.quantityc = ma.quantity where ma.mwsord = :refmwsord;
      update mwsords mo set mo.status = 5, mo.operator = :actuoperator where mo.ref = :refmwsord;
    end
    mwsord = :refmwsord;
  end else
    delete from mwsords where ref = :refmwsord;
end^
SET TERM ; ^
