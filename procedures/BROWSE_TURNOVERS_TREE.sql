--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BROWSE_TURNOVERS_TREE(
      COMPANY integer,
      ACCOUNT ACCOUNT_ID,
      FROMPERIOD varchar(6) CHARACTER SET UTF8                           ,
      TOPERIOD varchar(6) CHARACTER SET UTF8                           ,
      ADDBO smallint,
      ENDBOOKED smallint = 0)
  returns (
      BACK ACCOUNT_ID,
      SYMBOL ACCOUNT_ID,
      DESCRIPT varchar(80) CHARACTER SET UTF8                           ,
      BO_DEBIT numeric(15,2),
      BO_CREDIT numeric(15,2),
      BO_SALDO numeric(15,2),
      BM_DEBIT numeric(15,2),
      BM_CREDIT numeric(15,2),
      BM_SALDO numeric(15,2),
      SUM_DEBIT numeric(15,2),
      SUM_CREDIT numeric(15,2),
      SUM_SALDO numeric(15,2),
      SALDO_DEBIT numeric(15,2),
      SALDO_CREDIT numeric(15,2),
      SALDO_WM numeric(15,2),
      POS integer,
      BKTYPE smallint)
   as
declare variable PERIOD_BO varchar(6);
declare variable PERIOD_BO_INC varchar(6);
declare variable YEARID integer;
declare variable TMP varchar(20);
declare variable BKACCOUNT integer;
declare variable I smallint;
declare variable J smallint;
declare variable K smallint;
declare variable LEN smallint;
declare variable LEN2 smallint;
declare variable DICTDEF integer;
declare variable DICTDEF2 integer;
declare variable SEP varchar(1);
declare variable REF integer;
declare variable TMP_BO_DEBIT numeric(14,2);
declare variable TMP_BO_CREDIT numeric(14,2);
declare variable TMP_BM_DEBIT numeric(14,2);
declare variable TMP_BM_CREDIT numeric(14,2);
declare variable TMP_SUM_DEBIT numeric(14,2);
declare variable TMP_SUM_CREDIT numeric(14,2);
declare variable TMP_SUM_DEBIT_SALDO numeric(14,2);
declare variable TMP_SUM_CREDIT_SALDO numeric(14,2);
declare variable DLEVEL smallint;
declare variable LLEVEL smallint;
declare variable COUNTRY_CURRENCY varchar(3);
declare variable BALANCETYPE smallint;
declare variable EB smallint;
declare variable BB smallint;
declare variable POPSUBACCOUNT varchar(20);
declare variable TMPSUBACCOUNT varchar(20);
begin
  execute procedure get_config('COUNTRY_CURRENCY', 2)
    returning_values country_currency;

  select yearid from bkperiods where company = :company and id = :fromperiod
    into :yearid;
  select min(id) from bkperiods where company = :company and yearid=:yearid
    into :period_bo;

  if (addbo = 1) then
    period_bo_inc = period_bo;
  else
    select min(id) from bkperiods
      where company = :company and yearid=:yearid and id > :period_bo
      into :period_bo_inc;

  back = null;
  if(endbooked = 0) then begin--wszystkie
    eb = 0;
    bb = 1;
  end else if (endbooked = 1) then begin-- tylko próbnie
    eb = -1;
    bb = 1;
  end else begin--tylko końcowo
    eb = 1;
    bb = 0;
  end


  if (account = '') then
  begin -- syntetyka
    for select symbol, descript, ref, bktype, saldotype
      from bkaccounts
      where company = :company and yearid = :yearid
      order by symbol
      into :symbol, :descript, :ref, :bktype, :balancetype
    do begin

      bo_credit = 0;
      bo_debit = 0;
      bm_credit = 0;
      bm_debit = 0;
      sum_credit = 0;
      sum_debit = 0;
      saldo_credit = 0;
      saldo_debit = 0;

      for
        select
            sum(case when T.period = :period_bo then T.debit * :bb + T.ebdebit * :eb else 0 end) as bo_debit,
            sum(case when T.period = :period_bo then T.credit * :bb + T.ebcredit * :eb else 0 end) as bo_credit,
            sum(case when T.period >= :fromperiod then T.debit * :bb + T.ebdebit * :eb else 0 end) as bm_debit,
            sum(case when T.period >= :fromperiod then T.credit * :bb + T.ebcredit * :eb else 0 end) as bm_credit,
            sum(case when T.period >= :period_bo_inc then T.debit * :bb + T.ebdebit * :eb else 0 end) as sum_debit,
            sum(case when T.period >= :period_bo_inc then T.credit * :bb + T.ebcredit * :eb else 0 end) as sum_credit,
            sum(case when T.period >= :period_bo then T.debit * :bb + T.ebdebit * :eb else 0 end) as sum_debit_saldo,
            sum(case when T.period >= :period_bo then T.credit * :bb + T.ebcredit * :eb else 0 end) as sum_credit_saldo
          from accounting A
            left join turnovers T on (T.accounting = A.ref and T.period >= :period_bo and T.period <= :toperiod)
          where A.yearid = :yearid and A.currency = 'PLN' and A.bkaccount = :ref
          group by A.ref
          into :tmp_bo_debit, :tmp_bo_credit, :tmp_bm_debit, :tmp_bm_credit, :tmp_sum_debit, :tmp_sum_credit
            ,:tmp_sum_debit_saldo, :tmp_sum_credit_saldo
      do begin
        bo_debit = bo_debit + tmp_bo_debit;
        bo_credit = bo_credit + tmp_bo_credit;

        bm_debit = bm_debit + tmp_bm_debit;
        bm_credit = bm_credit + tmp_bm_credit;

        sum_debit = sum_debit + coalesce(tmp_sum_debit,0);
        sum_credit = sum_credit + coalesce(tmp_sum_credit,0);

        if (:addbo = 0) then
        begin
          tmp_sum_debit = tmp_sum_debit_saldo;
          tmp_sum_credit = tmp_sum_credit_saldo;
        end

        if (abs(tmp_sum_debit) > abs(tmp_sum_credit)
            or (abs(tmp_sum_debit) = abs(tmp_sum_credit) and tmp_sum_debit > tmp_sum_credit))
        then
          saldo_debit = saldo_debit + tmp_sum_debit - tmp_sum_credit;

        if (abs(tmp_sum_credit) > abs(tmp_sum_debit)
            or (abs(tmp_sum_credit) = abs(tmp_sum_debit) and tmp_sum_credit > tmp_sum_debit))
        then
          saldo_credit = saldo_credit + tmp_sum_credit - tmp_sum_debit;

      end
      i = null;
      select count(*) from accstructure where bkaccount=:ref
        into :pos;
      -- sprawdzanie czy istnieja otwarte konta
      if (pos > 0) then
        select count(*) from accounting where bkaccount = :ref
          into :pos;

      if (balancetype = 0) then
      begin
        saldo_debit = saldo_debit - saldo_credit;
        saldo_credit = 0;
      end else if (balancetype = 2) then
      begin
        saldo_credit = saldo_credit- saldo_debit;
        saldo_debit = 0;
      end
      BO_SALDO = BO_DEBIT - BO_CREDIT;
      BM_SALDO = BM_DEBIT - BM_CREDIT;
      SUM_SALDO = SUM_DEBIT - SUM_CREDIT;
      SALDO_WM = SALDO_DEBIT - SALDO_CREDIT;
      suspend;
    end
  end else
  begin
     --analityka

    tmp = substring(account from 1 for 3);
    select ref, saldotype, bktype
      from bkaccounts where company = :company and symbol = :tmp and yearid = :yearid
      into :bkaccount, :balancetype, :bktype;

    dictdef = null;
    i = 3;
    k = 3;

    for
      select separator, dictdef, len, nlevel
        from accstructure
        where bkaccount = :bkaccount
        order by bkaccount, nlevel
        into :sep, :dictdef2, :len2, :llevel
    do begin
      if (sep <> ',') then
        i = i + 1;
      i = i + len2;
      if (coalesce(char_length(account),0) < i and dictdef is null) then -- [DG] XXX ZG119346
      begin
        dictdef = dictdef2;
        j = i;
        len = len2;
        dlevel = llevel;
      end
      if (dictdef is null) then
      begin
         k = len2;
         if (sep <> ',') then
           k = k + 1;
      end
    end

    back = substring(account from  1 for coalesce(char_length(account),0) - k); -- [DG] XXX ZG119346
    if (dlevel = llevel) then
    begin  --ostatni poziom analityczny
      pos = 0;
      for
        select A.account,
            sum(case when T.period = :period_bo then T.debit * :bb + T.ebdebit * :eb else 0 end) as bo_debit,
            sum(case when T.period = :period_bo then T.credit * :bb + T.ebcredit * :eb else 0 end) as bo_credit,
            sum(case when T.period >= :fromperiod then T.debit * :bb + T.ebdebit * :eb else 0 end) as bm_debit,
            sum(case when T.period >= :fromperiod then T.credit * :bb + T.ebcredit * :eb else 0 end) as bm_credit,
            sum(case when T.period >= :period_bo_inc then T.debit * :bb + T.ebdebit * :eb else 0 end) as sum_debit,
            sum(case when T.period >= :period_bo_inc then T.credit * :bb + T.ebcredit * :eb else 0 end) as sum_credit,
            sum(case when T.period >= :period_bo then T.debit * :bb + T.ebdebit * :eb else 0 end) as sum_debit_saldo,
            sum(case when T.period >= :period_bo then T.credit * :bb + T.ebcredit * :eb else 0 end) as sum_credit_saldo
          from accounting A
            left join turnovers T on (T.accounting = A.ref and T.period >= :period_bo and T.period <= :toperiod)
          where A.account like :account || '%' and A.company = :company
            and A.yearid = :yearid and A.currency = :country_currency
          group by A.account
          into :symbol, :bo_debit, :bo_credit, :bm_debit, :bm_credit, :sum_debit, :sum_credit,
            :tmp_sum_debit_saldo, :tmp_sum_credit_saldo
      do begin
        saldo_debit = 0;
        saldo_credit = 0;
        if (abs(tmp_sum_debit_saldo) > abs(tmp_sum_credit_saldo)
          or (abs(tmp_sum_debit_saldo) = abs(tmp_sum_credit_saldo) and tmp_sum_debit_saldo > tmp_sum_credit_saldo))
        then
          saldo_debit = tmp_sum_debit_saldo - tmp_sum_credit_saldo;
        if (abs(tmp_sum_credit_saldo) > abs(tmp_sum_debit_saldo)
            or (abs(tmp_sum_credit_saldo) = abs(tmp_sum_debit_saldo) and tmp_sum_credit_saldo > tmp_sum_debit_saldo))
        then
          saldo_credit = tmp_sum_credit_saldo - tmp_sum_debit_saldo;

        tmp = substring(symbol from  (j-len+1) for  j);
        execute procedure name_from_bksymbol(company, dictdef, tmp)
          returning_values descript, ref;

        if (balancetype = 0) then
        begin
          saldo_debit = saldo_debit - saldo_credit;
          saldo_credit = 0;
        end else
        if (balancetype = 2) then
        begin
          saldo_credit = saldo_credit- saldo_debit;
          saldo_debit = 0;
        end
        BO_SALDO = BO_DEBIT - BO_CREDIT;
        BM_SALDO = BM_DEBIT - BM_CREDIT;
        SUM_SALDO = SUM_DEBIT - SUM_CREDIT;
        SALDO_WM = SALDO_DEBIT - SALDO_CREDIT;
        suspend;
      end
    end else
    begin --posredni poziom analityczny
      pos = 1; --skoro mamy kwoty to powinien miec pozycje

      bo_credit = 0;
      bo_debit = 0;
      bm_credit = 0;
      bm_debit = 0;
      sum_credit = 0;
      sum_debit = 0;
      saldo_credit = 0;
      saldo_debit = 0;
      popsubaccount = '';
      for
        select substring(a.account from 1 for :j),
           sum(case when T.period = :period_bo then T.debit * :bb + T.ebdebit * :eb else 0 end) as bo_debit,
              sum(case when T.period = :period_bo then T.credit * :bb + T.ebcredit * :eb else 0 end) as bo_credit,
              sum(case when T.period >= :fromperiod then T.debit * :bb + T.ebdebit * :eb else 0 end) as bm_debit,
              sum(case when T.period >= :fromperiod then T.credit * :bb + T.ebcredit * :eb else 0 end) as bm_credit,
              sum(case when T.period >= :period_bo_inc then T.debit * :bb + T.ebdebit * :eb else 0 end) as sum_debit,
              sum(case when T.period >= :period_bo_inc then T.credit * :bb + T.ebcredit * :eb else 0 end) as sum_credit,
              sum(case when T.period >= :period_bo then T.debit * :bb + T.ebdebit * :eb else 0 end) as sum_debit_saldo,
              sum(case when T.period >= :period_bo then T.credit * :bb + T.ebcredit * :eb else 0 end) as sum_credit_saldo
          from accounting a
            left join turnovers T on (T.accounting = A.ref and T.period >= :period_bo and T.period <= :toperiod)
          where a.account like :account||'%' and a.company = :company
            and a.yearid = :yearid and a.currency = :country_currency
          group by A.ref, substring(a.account from 1 for :j)
          order by 1
          into :symbol, :tmp_bo_debit, :tmp_bo_credit, :tmp_bm_debit, :tmp_bm_credit, :tmp_sum_debit, :tmp_sum_credit,
            :tmp_sum_debit_saldo, :tmp_sum_credit_saldo
      do begin
        if (popsubaccount = '') then
          popsubaccount = :symbol;
        else if (:popsubaccount <> :symbol) then
        begin
          tmpsubaccount = :symbol;
          symbol = popsubaccount;
          if (balancetype = 0) then
          begin
            saldo_debit = saldo_debit - saldo_credit;
            saldo_credit = 0;
          end else
          if (balancetype = 2) then
          begin
            saldo_credit = saldo_credit- saldo_debit;
            saldo_debit = 0;
          end
  
          if (bo_debit<>0 or bo_credit<>0 or bm_debit<>0 or bm_credit<>0 or sum_debit<>0 or sum_credit<>0 or saldo_debit<>0 or saldo_credit<>0) then
          begin
            tmp = substring(symbol from  (j-len+1) for  j);
            execute procedure name_from_bksymbol(company, dictdef, tmp)
              returning_values descript, ref;
            BO_SALDO = BO_DEBIT - BO_CREDIT;
            BM_SALDO = BM_DEBIT - BM_CREDIT;
            SUM_SALDO = SUM_DEBIT - SUM_CREDIT;
            SALDO_WM = SALDO_DEBIT - SALDO_CREDIT;
            suspend;
          end
          popsubaccount = tmpsubaccount;
          symbol = tmpsubaccount;
          bo_credit = 0;
          bo_debit = 0;
          bm_credit = 0;
          bm_debit = 0;
          sum_credit = 0;
          sum_debit = 0;
          saldo_credit = 0;
          saldo_debit = 0;
        end

        bo_debit = bo_debit + tmp_bo_debit;
        bo_credit = bo_credit + tmp_bo_credit;

        bm_debit = bm_debit + tmp_bm_debit;
        bm_credit = bm_credit + tmp_bm_credit;

        sum_debit = sum_debit + coalesce(tmp_sum_debit,0);
        sum_credit = sum_credit + coalesce(tmp_sum_credit,0);

        if (:addbo = 0) then
        begin
          tmp_sum_debit = tmp_sum_debit_saldo;
          tmp_sum_credit = tmp_sum_credit_saldo;
        end

        if (abs(tmp_sum_debit) > abs(tmp_sum_credit)
            or (abs(tmp_sum_debit) = abs(tmp_sum_credit) and tmp_sum_debit > tmp_sum_credit))
        then
          saldo_debit = saldo_debit + tmp_sum_debit - tmp_sum_credit;
        if (abs(tmp_sum_credit) > abs(tmp_sum_debit)
            or (abs(tmp_sum_credit) = abs(tmp_sum_debit) and tmp_sum_credit > tmp_sum_debit))
        then
          saldo_credit = saldo_credit + tmp_sum_credit - tmp_sum_debit;
      end
      if (:symbol <> '') then
      begin
        if (balancetype = 0) then
        begin
          saldo_debit = saldo_debit - saldo_credit;
          saldo_credit = 0;
        end else
        if (balancetype = 2) then
        begin
          saldo_credit = saldo_credit- saldo_debit;
          saldo_debit = 0;
        end

        if (bo_debit<>0 or bo_credit<>0 or bm_debit<>0 or bm_credit<>0 or sum_debit<>0 or sum_credit<>0 or saldo_debit<>0 or saldo_credit<>0) then
        begin
          tmp = substring(symbol from  (j-len+1) for  j);
          execute procedure name_from_bksymbol(company, dictdef, tmp)
            returning_values descript, ref;
          BO_SALDO = BO_DEBIT - BO_CREDIT;
          BM_SALDO = BM_DEBIT - BM_CREDIT;
          SUM_SALDO = SUM_DEBIT - SUM_CREDIT;
          SALDO_WM = SALDO_DEBIT - SALDO_CREDIT;
          suspend;
        end
      end
    end
  end
end^
SET TERM ; ^
