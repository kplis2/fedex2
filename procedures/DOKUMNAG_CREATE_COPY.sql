--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_CREATE_COPY(
      REFDOKUMNAGDIVIDE integer,
      ACC integer = 1)
  returns (
      NEWDOKUMNAGREF integer)
   as
declare variable MAGAZYN varchar(3);
declare variable OKRES varchar(6);
declare variable TYP varchar(3);
declare variable NUMER integer;
declare variable NUMERROZBICIA integer;
declare variable SYMBOLORGINAL varchar(20);
declare variable NEWSYMBOL varchar(20);
declare variable ORGINALDOKNAGILOSC numeric(14,4);
declare variable DOKUMENT DOKUMNAG_ID;
declare variable KTM KTM_ID;
declare variable WERSJA NRWERSJI_ID;
declare variable JEDNO TOWJEDN;
declare variable ILOSC ILOSCI_MAG;
declare variable ILOSCROZ ILOSCI_MAG;
declare variable DOSTAWA DOSTAWA_ID;
declare variable DATAWAZN TIMESTAMP_ID;
declare variable CENA CENYMAG;
declare variable CENASR CENYMAG;
declare variable CENACEN CENYSPR;
declare variable WALCEN WALUTA_ID;
declare variable RABATTAB CENY;
declare variable RABAT CENY;
declare variable CENANET CENYSPR;
declare variable CENABRU CENYSPR;
declare variable WERSJAREF integer;
declare variable ROZ_BLOK SMALLINT_ID;
declare variable SERIALIZED SMALLINT_ID;
declare variable SERIALNR SERIAL_ID;
declare variable WARTREALZAM NAGZAM_ID;
declare variable WARTREALPOZ POZZAM_ID;
declare variable FROMZAM NAGZAM_ID;
declare variable FROMPOZZAM POZZAM_ID;
declare variable FROMPOZFAK POZFAK_ID;
declare variable OPK SMALLINT_ID;
declare variable MAGLOCAL MAGLOCAL_ID;
declare variable OBLICZCENZAK SMALLINT_ID;
declare variable OBLICZCENKOSZT SMALLINT_ID;
declare variable OBLICZCENFAB SMALLINT_ID;
declare variable NOWACENASPRNET CENYSPR;
declare variable NOWACENASPRBRU CENYSPR;
declare variable STANOPKROZ STANYOPK_DI;
declare variable ILOSCL ILOSCI_MAG;
declare variable WARTOSCL CENY;
declare variable PCENASR CENYMAG;
declare variable PWARTOSC CENY;
declare variable KORTOPOZ DOKUMPOZ_ID;
declare variable CENAWAL CENYWAL;
declare variable FOC SMALLINT_ID;
declare variable ILOSCLWYS ILOSCI_MAG;
declare variable WAGA WAGA_ID;
declare variable BKTM KTM_ID;
declare variable BWERSJA INTEGER_ID;
declare variable BILOSC ILOSCI_MAG;
declare variable BCENA CENYMAG;
declare variable BDOSTAWA DOSTAWA_ID;
declare variable BDATAWAZN TIMESTAMP_ID;
declare variable BLOKCENA SMALLINT_ID;
declare variable FLAGI STRING40;
declare variable UWAGI STRING;
declare variable CENA_KOSZT CENYMAG;
declare variable WART_KOSZT CENY;
declare variable BCENA_KOSZT CENYMAG;
declare variable OPISWYD STRING40;
declare variable ILOSCDOROZB ILOSCI_MAG;
declare variable PARAMD1 PARAMD;
declare variable PARAMD2 PARAMD;
declare variable PARAMD3 PARAMD;
declare variable PARAMD4 PARAMD;
declare variable PARAMN1 PARAMN;
declare variable PARAMN2 PARAMN;
declare variable PARAMN3 PARAMN;
declare variable PARAMN4 PARAMN;
declare variable PARAMN5 PARAMN;
declare variable PARAMN6 PARAMN;
declare variable PARAMN7 PARAMN;
declare variable PARAMN8 PARAMN;
declare variable PARAMN9 PARAMN;
declare variable PARAMN10 PARAMN;
declare variable PARAMN11 PARAMN;
declare variable PARAMN12 PARAMN;
declare variable PARAMS1 PARAMS;
declare variable PARAMS2 PARAMS;
declare variable PARAMS3 PARAMS;
declare variable PARAMS4 PARAMS;
declare variable PARAMS5 PARAMS;
declare variable PARAMS6 PARAMS;
declare variable PARAMS7 PARAMS;
declare variable PARAMS8 PARAMS;
declare variable PARAMS9 PARAMS;
declare variable PARAMS10 PARAMS;
declare variable PARAMS11 PARAMS;
declare variable PARAMS12 PARAMS;
declare variable PREC SMALLINT_ID;
declare variable GR_VAT VAT_ID;
declare variable ALTTOPOZ DOKUMPOZ_ID;
declare variable FAKE smallint;
declare variable ALTTOPOZN DOKUMPOZ_ID;
declare variable ORGDOKUMPOZ DOKUMPOZ_ID;
begin
  select magazyn, okres, typ, numer
    from dokumnag
    where ref = :refdokumnagdivide
    into :magazyn, :okres, :typ, :numer;
  symbolorginal = null;
  select dokumnag.symbolorg from dokumnag where dokumnag.ref = :refdokumnagdivide into :symbolorginal;
  if(:symbolorginal is null or :symbolorginal = '') then
    select dokumnag.symbol from dokumnag where dokumnag.ref = :refdokumnagdivide
    into :symbolorginal;
  select count(*) from dokumnag
    where dokumnag.magazyn = :magazyn and dokumnag.okres = :okres
      and dokumnag.typ = :typ and dokumnag.symbol like :symbolorginal||'%'
    into :numerrozbicia;
  newsymbol = :symbolorginal || '/' || :numerrozbicia;
  execute procedure GEN_REF('DOKUMNAG') returning_values :newdokumnagref;
  insert into dokumnag (ref, magazyn, mag2, okres, typ, data,
         dostawa, dostawca, klient, rabat, uwagi, blokada, operator, oddzial,
         odbiorcaid, dulica, dnrdomu, dnrlokalu, dmiasto, dkodp, termdost, sposdost, trasadost, wysylka,
         kosztdost, zlecref, zlecnazwa, symbfak, datafak, walutowy, waluta, kurs, tabkurs,
         termzap, sposzap, dnizap, kosztdostplat, odbiorcaidsped, flagi,
         flagisped, sprzedawca, sprzedawcazew, symbolfak, numreczna, symbolorg)
    select :newdokumnagref, d.magazyn, d.mag2, d.okres, d.typ, d.data,
           d.dostawa, d.dostawca, d.klient, d.rabat, d.uwagi, d.blokada, d.operator, d.oddzial,
           d.odbiorcaid, d.dulica, d.dnrdomu, d.dnrlokalu, d.dmiasto, d.dkodp, d.termdost, d.sposdost, d.trasadost, d.wysylka,
           d.kosztdost, d.zlecref, d.zlecnazwa, d.symbfak, d.datafak, d.walutowy, d.waluta, d.kurs, d.tabkurs,
           d.termzap, d.sposzap, d.dnizap, d.kosztdostplat, d.odbiorcaidsped, d.flagi,
           d.flagisped, d.sprzedawca, d.sprzedawcazew, d.symbolfak, 1, :symbolorginal
    from dokumnag d
    where d.ref = :refdokumnagdivide;

    for select :newdokumnagref , d.ktm, d.wersja, d.jedno, D.ilosc - D.iloscdorozb , d.iloscroz,
        d.dostawa, d.datawazn, d.cena, d.cenasr, d.cenacen, d.walcen, d.rabattab, d.rabat, d.cenanet,
        d.cenabru, d.wersjaref, d.roz_blok, d.serialized, d.serialnr,
        d.wartrealzam, d.wartrealpoz, d.fromzam, d.frompozzam, d.frompozfak, d.opk, d.maglocal, d.obliczcenzak,
        d.obliczcenkoszt, d.obliczcenfab, d.nowacenasprnet, d.nowacenasprbru, d.stanopkroz, d.iloscl, d.wartoscl,
        d.pcenasr, d.pwartosc, d.kortopoz, d.cenawal, d.foc, d.ilosclwys, d.waga, d.bktm, d.bwersja,
        d.bilosc, d.bcena, d.bdostawa, d.bdatawazn, d.blokcena, d.flagi, d.uwagi, d.cena_koszt, d.wart_koszt,
        d.bcena_koszt, d.opiswyd, 0,
        d.paramd1, d.paramd2, d.paramd3, d.paramd4,
        d.paramn1, d.paramn2, d.paramn3, d.paramn4,
        d.paramn5, d.paramn6, d.paramn7, d.paramn8,
        d.paramn9, d.paramn10, d.paramn11, d.paramn12,
        d.params1, d.params2, d.params3, d.params4,
        d.params5, d.params6, d.params7, d.params8,
        d.params9, d.params10, d.params11, d.params12, d.prec, d.gr_vat, d.fake, d.alttopoz, ref
        from dokumpoz d
      where d.dokument = :refdokumnagdivide and D.ilosc <> D.iloscdorozb
      order by d.alttopoz nulls first, d.numer
      into :dokument, :ktm, :wersja, :jedno,  :ilosc, :iloscroz,
        :dostawa, :datawazn, :cena, :cenasr, :cenacen, :walcen, :rabattab, :rabat, :cenanet,
        :cenabru, :wersjaref, :roz_blok, :serialized, :serialnr,
        :wartrealzam, :wartrealpoz, :fromzam, :frompozzam, :frompozfak, :opk, :maglocal, :obliczcenzak,
        :obliczcenkoszt, :obliczcenfab, :nowacenasprnet, :nowacenasprbru, :stanopkroz, :iloscl, :wartoscl,
        :pcenasr, :pwartosc, :kortopoz, :cenawal, :foc, :ilosclwys, :waga, :bktm, :bwersja,
        :bilosc, :bcena, :bdostawa, :bdatawazn, :blokcena, :flagi, :uwagi, :cena_koszt, :wart_koszt,
        :bcena_koszt, :opiswyd, :iloscdorozb,
        :paramd1, :paramd2, :paramd3, :paramd4,
        :paramn1, :paramn2, :paramn3, :paramn4,
        :paramn5, :paramn6, :paramn7, :paramn8,
        :paramn9, :paramn10, :paramn11, :paramn12,
        :params1, :params2, :params3, :params4,
        :params5, :params6, :params7, :params8,
        :params9, :params10, :params11, :params12, :prec, :gr_vat, :fake, :alttopoz, :orgdokumpoz
     do begin

       alttopozn = null;
       if (:alttopoz is not null) then
       begin
         select ref from dokumpoz 
       where dokument = :newdokumnagref 
       and orgdokumpoz = :alttopoz
           into :alttopozn;
       end 

       insert into dokumpoz (dokument, ktm, wersja, jedno,  ilosc, iloscroz,
         dostawa, datawazn, cena, cenasr, cenacen, walcen, rabattab, rabat, cenanet,
         cenabru, wersjaref, roz_blok, serialized, serialnr,
         wartrealzam, wartrealpoz, fromzam, frompozzam, frompozfak, opk, maglocal, obliczcenzak,
         obliczcenkoszt, obliczcenfab, nowacenasprnet, nowacenasprbru, stanopkroz, iloscl, wartoscl,
         pcenasr, pwartosc, kortopoz, cenawal, foc, ilosclwys, waga, bktm, bwersja,
         bilosc, bcena, bdostawa, bdatawazn, blokcena, flagi, uwagi, cena_koszt, wart_koszt,
         bcena_koszt, opiswyd, iloscdorozb,
         paramd1, paramd2, paramd3, paramd4,
         paramn1, paramn2, paramn3, paramn4,
         paramn5, paramn6, paramn7, paramn8,
         paramn9, paramn10, paramn11, paramn12,
         params1, params2, params3, params4,
         params5, params6, params7, params8,
         params9, params10, params11, params12, prec, gr_vat, orgdokumpoz, fake, alttopoz)
       values(:dokument, :ktm, :wersja, :jedno,  :ilosc, :iloscroz,
        :dostawa, :datawazn, :cena, :cenasr, :cenacen, :walcen, :rabattab, :rabat, :cenanet,
        :cenabru, :wersjaref, :roz_blok, :serialized, :serialnr,
        :wartrealzam, :wartrealpoz, :fromzam, :frompozzam, :frompozfak, :opk, :maglocal, :obliczcenzak,
        :obliczcenkoszt, :obliczcenfab, :nowacenasprnet, :nowacenasprbru, :stanopkroz, :iloscl, :wartoscl,
        :pcenasr, :pwartosc, :kortopoz, :cenawal, :foc, :ilosclwys, :waga, :bktm, :bwersja,
        :bilosc, :bcena, :bdostawa, :bdatawazn, :blokcena, :flagi, :uwagi, :cena_koszt, :wart_koszt,
        :bcena_koszt, :opiswyd, :iloscdorozb,
        :paramd1, :paramd2, :paramd3, :paramd4,
        :paramn1, :paramn2, :paramn3, :paramn4,
        :paramn5, :paramn6, :paramn7, :paramn8,
        :paramn9, :paramn10, :paramn11, :paramn12,
        :params1, :params2, :params3, :params4,
        :params5, :params6, :params7, :params8,
        :params9, :params10, :params11, :params12, :prec, :gr_vat, :orgdokumpoz, :fake, :alttopozn );
     end

    select sum(coalesce(p.ilosc,0) - coalesce(p.iloscdorozb,0))
      from dokumpoz p
      where p.dokument = :newdokumnagref
      into :orginaldoknagilosc;

    if (:orginaldoknagilosc <= 0) then
      exception DOKUMNAG_EXPT 'Nie mona przeniesc wszystkich pozycji na dokument rozbijany.';
    else
    begin --BS36993
      update dokumnag set dokumnag.akcept = 1 where dokumnag.ref = :newdokumnagref;
      update dokumnag set dokumnag.symbol = :newsymbol where dokumnag.ref = :newdokumnagref;
    end
  suspend;

end^
SET TERM ; ^
