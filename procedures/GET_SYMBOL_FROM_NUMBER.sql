--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_SYMBOL_FROM_NUMBER(
      NUMBER integer,
      LEN smallint)
  returns (
      SYMBOL varchar(30) CHARACTER SET UTF8                           )
   as
declare variable slength smallint;
begin
  symbol = '00000000000000000000' || cast(number as varchar(10));
  slength = coalesce(char_length(symbol),0); -- [DG] XXX ZG119346
  symbol = substring(symbol from  slength - len + 1 for  slength);
  suspend;
end^
SET TERM ; ^
