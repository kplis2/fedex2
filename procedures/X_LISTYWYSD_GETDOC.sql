--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_LISTYWYSD_GETDOC(
      DOCUMENT STRING30,
      PACKOPER OPERATOR_ID,
      AKTUMAG STRING3,
      MODE SMALLINT_ID = 0)
  returns (
      DOCREF DOKUMNAG_ID,
      DOCSYMB STRING30,
      SHIPPINGDOC LISTYWYSD_ID,
      PACKCONTROL SMALLINT_ID,
      STATUS SMALLINT_ID,
      MSG STRING1024)
   as
  declare variable cart integer_id;
  declare variable INTSTATUS smallint;
  declare variable DESCRIPT varchar(1024);
  declare variable WH DEFMAGAZ_ID;
  declare variable ACCEPT smallint;
  declare variable MWS smallint;
  declare variable EOL varchar(3);
  declare variable DOCWHREF integer;
  declare variable DOCSYMBS varchar(30);
  declare variable DOCWH varchar(3);
  declare variable POSREF integer;
  declare variable MWSACTREF integer;
  declare variable EMWSCONSTLOCS MEMO;
  declare variable MWSORD MWSORDS_ID;
  declare variable ACTOPER OPERATOR_ID;
  declare variable BLOKADAPAK SMALLINT_ID;
  declare variable DOCGROUP DOKUMNAG_ID;
  declare variable MAGAZYN STRING3;
  declare variable EMWSCONSTLOCSTMP MEMO; /* [PM] XXX */
  declare variable CURCOMPANY COMPANIES_ID;
  declare variable DOCCOMPANY COMPANIES_ID;
  declare variable DOCCOMPANYNAME STRING20;
  declare variable X_PACKOPER OPERATOR_ID;
  declare variable X_PACKOPERLOGIN STRING20;
  declare variable BRANCH ODDZIAL_ID;
  declare variable X_STARTPAKSTATUS SMALLINT_ID;
  declare variable X_LASTPACKDOK DOKUMNAG_ID;
  declare variable DOCMAGAZYN DEFMAGAZ_ID; /* [PM] XXX */
  declare variable MULTI SMALLINT_ID; --[PM] XXX #POJEDYNKI
  declare variable KLIENT STRING255;
  declare variable ODBIORCA STRING255;
  declare variable WZ_WAPRO STRING255;
begin
--procedura listywysd_getdoc przycieta i zmodyfikowana do potrzeb HERMONA
  /* STATUS
0 - otwarty dokument spedydyjny
1 - zamkniety dokument spedydyjny
*/
  EOL = '
';

  DOCREF = 0;
  DOCSYMB = '';
  SHIPPINGDOC = 0;
  PACKCONTROL = 0;
  STATUS = 1;
  MSG = '';
  WZ_WAPRO = '';-- KPI

  --blokowanie pobrania tego samego dokumentu przez kilku operatorow jednoczesnie
  execute procedure SET_GLOBAL_PARAM('MWSACTUPACKOPERATOR', :PACKOPER);
  execute procedure GET_GLOBAL_PARAM('CURRENTCOMPANY')
      returning_values CURCOMPANY;
  MAGAZYN = AKTUMAG;-- KBI wdrożenie magazynu celnego

  if (coalesce(MAGAZYN, '') = '') then
    exception UNIVERSAL 'Brak magazynu';

  select ODDZIAL
    from DEFMAGAZ
    where SYMBOL = :MAGAZYN -- [DG] XXX ZG97597
  into :BRANCH;

  --sprawdzam czy ref dokumentu czy symbol
  execute procedure CHECK_STRING_TO_INTEGER(:DOCUMENT)
      returning_values INTSTATUS;

  if (INTSTATUS is null) then
    INTSTATUS = 0;

  --<<<START znalezienie odpowiedniej grupy spedycyjnej i dokumentu magazynowego
  if (INTSTATUS = 1) then
    select D.REF, D.GRUPASPED, coalesce(D.MWSMAG, D.MAGAZYN)
    from DOKUMNAG D
    where D.REF = :DOCUMENT
    into :DOCWHREF, :DOCGROUP, :WH;
  else
  begin
    --sprawdzenie czy istnieje dokument o takim symbolu z sente lub z WAPRO
    select D.REF, D.GRUPASPED, coalesce(D.MWSMAG, D.MAGAZYN)
      from DOKUMNAG D
      where D.SYMBOL = :DOCUMENT or D.INT_SYMBOL = :DOCUMENT ---KPI ZG137148
    into :DOCWHREF, :DOCGROUP, :WH;
    --jezeli to nie symbol dokumentu to szukam wozka
    if (:DOCGROUP is null) then
    begin
      select C.REF
        from MWSCONSTLOCS C
        where C.SYMBOL = upper(:DOCUMENT)
      into :CART;

      if (:CART is null) then
      begin
        MSG = 'Brak wózka zdefiniowanego w systemie';
        STATUS = 0;
        in autonomous transaction
        do -- [DG] XXX ZG97597
          delete from X_STARTPAK;
        exit;
      end

      select first 1 D2.GRUPASPED, coalesce(D2.MWSMAG, D2.MAGAZYN)
        from MWSACTS A
          left join DOKUMNAG D1 on (A.DOCID = D1.REF)
          left join DOKUMNAG D2 on (D1.GRUPASPED = D2.GRUPASPED)
          left join DOKUMPOZ P2 on (P2.DOKUMENT = D2.REF) -- MSt: nizej
        where A.MWSCONSTLOC = :CART and
              A.STATUS = 5 and
              P2.ILOSCL > 0 -- MSt: Korekta calosciowa do pierwszego dokumentu w grupie powodowala zlapanie dokumentu ktorego nie wydajemy
        order by D2.ZEWN desc
      into :DOCGROUP, :WH;

      if (:DOCGROUP is null) then
      begin
        MSG = 'Brak zlecenia szykowanego na tym wózku ';
        STATUS = 0;
        in autonomous transaction
        do -- [DG] XXX ZG97597
          delete from X_STARTPAK;
        exit;
      end
    end
  end

  if (coalesce(DOCWHREF, 0) = 0 and
      coalesce(DOCGROUP, 0) = 0) then
  begin
    MSG = 'W systemie nie zdefiniowano takiego dokumentu.';
    STATUS = 0;
    in autonomous transaction
    do -- [DG] XXX ZG97597
      delete from X_STARTPAK;
    exit;
  end
  --STOP >>> znalezienie odpowiedniej grupy spedycyjnej i dokumentu magazynowego

  select first 1 D.COMPANY, D.MAGAZYN --[PM] XXX
    from DOKUMNAG D
    where D.GRUPASPED = :DOCGROUP and
          D.WYSYLKADONE = 0
  into :DOCCOMPANY, :DOCMAGAZYN;--[PM] XXX

  if (:DOCCOMPANY <> :CURCOMPANY) then
  begin
    select SYMBOL
      from COMPANIES
      where REF = :DOCCOMPANY
    into :DOCCOMPANYNAME;

    STATUS = 0;
    MSG = 'Dokument z ' || :DOCCOMPANYNAME || '. Zmień oddział, aby spakować';
    in autonomous transaction
    do -- [DG] XXX ZG97597
      delete from X_STARTPAK;
    exit;
  end

  if (:MAGAZYN <> :DOCMAGAZYN) then
  begin
    STATUS = 0;
    MSG = 'Dokument z ' || :DOCMAGAZYN || ' a pakujesz na ' || :MAGAZYN || ' !';
    in autonomous transaction
    do -- [DG] XXX ZG97597
      delete from X_STARTPAK;
    exit;
  end

  ACCEPT = 0;
  if (not exists(
              select first 1 1
                from DOKUMNAG
                where GRUPASPED = :DOCGROUP and WYSYLKADONE = 0)
      or (exists(
              select first 1 1
                 from LISTYWYSD
                 where X_GRUPASPED = :DOCGROUP and AKCEPT < 2)))  --[PM] XXX dokumenty ze statusem 1 gdzie wysypala sie komuniacja ze spedytorem
  then
  begin
    select first 1 L.REF, L.AKCEPT, L.X_PACKOPER
      from LISTYWYSDPOZ P
        join LISTYWYSD L on (P.DOKUMENT = L.REF)
      where P.DOKTYP = 'M' and P.DOKREF = :DOCGROUP
    into :SHIPPINGDOC, :ACCEPT, :X_PACKOPER;

    if (:PACKOPER is distinct from :X_PACKOPER) then -- jezeli operatorzy sa rozni, to dzialanie zalezne od trybu
    begin
      if (:MODE = 0) then -- jezeli tryb 0 (VCL), to status 0
      begin
        STATUS = 0;
        select LOGIN
          from OPERATOR
          where REF = :X_PACKOPER
        into :X_PACKOPERLOGIN;
        MSG = 'Dokument jest pakowany przez operatora ' || coalesce(:X_PACKOPERLOGIN, '(null)');
      end
      /*else
      if (:MODE = 1) then -- jezeli wywolanie z 1 (rekurencja ze startpak), to status -1
        STATUS = -1;
      in autonomous transaction
      do -- [DG] XXX ZG97597
        delete from X_STARTPAK; */
      exit;
    end
    -- [DG] XXX end

    if (:ACCEPT = 2) then
    begin
      STATUS = 0;
      MSG = 'Dokument spedycyjny został już zamknięty. Otwórz dokument!';
      in autonomous transaction
      do -- [DG] XXX ZG97597
        delete from X_STARTPAK;
      exit;
    end
  end

  if (exists(
            select first 1 1
              from DOKUMNAG
              where GRUPASPED = :DOCGROUP and
                   AKCEPT in (0, 9))) then
  begin
    STATUS = 0;
    MSG = 'Nie wszystkie dokumenty z grupy zostały zaakceptowane!';
    in autonomous transaction
      do -- [DG] XXX ZG97597
      delete from X_STARTPAK;
    exit;
  end


  --obsluga przypadku gdy cala grupa spedycyjna zostala skorygowana do zera
  if (not exists(
              select first 1 1
                from DOKUMNAG N
                  left join DOKUMPOZ P on (N.REF = P.DOKUMENT)
                where N.GRUPASPED = :DOCGROUP
                      and N.WYDANIA = 1
                      and P.ILOSCL > 0)) then
  begin
    STATUS = -2;--[PM] XXX #URUCHOMIENIE #ZWROTY status -2 dla zwrotow calosciowych
    MSG = 'Cała grupa wysyłkowa została skorygowana. Odstaw do ZWROTÓW!';

    select cast(list(distinct iif(A.MWSCONSTLOC is not null, L.SYMBOL || ' z ', '') || coalesce(C.MWSCONSTLOCSYMB, 'Brak miejsca odstawienia'), '; ') as MEMO)
      from DOKUMNAG D
        left join MWSACTS A on (A.DOCID = D.REF and A.DOCTYPE = 'M')
        left join MWSCONSTLOCS L on (A.MWSCONSTLOC = L.REF)
        left join MWSORDCARTCOLOURS C on (C.MWSORD = A.MWSORD and C.DOCID = D.REF and L.REF = C.CART)
      where D.GRUPASPED = :DOCGROUP and
          A.STATUS = 5 and
          A.RECDOC = 0
    into :EMWSCONSTLOCSTMP;--[PM] XXX
    --emwsconstlocstmp = cast (emwsconstlocstmp as string10240); --[PM] XXX start ZG105282
    if (coalesce(:EMWSCONSTLOCSTMP, '') <> '') then
    begin
      if (coalesce(char_length(:EMWSCONSTLOCSTMP), 0) > 255) then
      begin
        MSG = :MSG || 'Lista wózków nie miesci sie na ekranie. Pobierz liste z "Raportu do spakowania".';
      end
      else
      begin
        EMWSCONSTLOCS = EMWSCONSTLOCSTMP;
        EMWSCONSTLOCS = EMWSCONSTLOCSTMP;
        select first 1 K.FSKROT, O.NAZWA
          from DOKUMNAG DN
            left join KLIENCI K on (K.REF = DN.KLIENT)
            left join ODBIORCY O on (O.REF = DN.ODBIORCAID)
          where DN.GRUPASPED = :DOCGROUP
        into :KLIENT, :ODBIORCA;
        if (:KLIENT is not null) then
          MSG = :MSG || 'Klient ' || coalesce(:KLIENT, '');
        if (:ODBIORCA is not null) then
          MSG = :MSG || ' Odbiorca ' || coalesce(:ODBIORCA, '');
        MSG = :MSG || 'Pobierz koszyki:' || :EOL || :EMWSCONSTLOCS;
      end
    end --[PM] XXX koniec ZG105282

    for select distinct A.MWSORD, A.MWSCONSTLOC
          from DOKUMNAG D
            left join MWSACTS A on (A.DOCID = D.REF and A.DOCTYPE = 'M')
          where D.GRUPASPED = :DOCGROUP and
              A.STATUS = 5 and
              A.RECDOC = 0
        into :MWSORD, :CART
    do
    begin
      update or insert into X_MWSORDCARTCOR (MWSORD, CART, STATUS, OPERATOR, DOCGROUP)
      values (:MWSORD, :CART, 1, :PACKOPER, :DOCGROUP)
      matching (MWSORD, CART, STATUS);
    end

    update DOKUMNAG DN
      set DN.X_BLOKADAPAK = -1
      where DN.X_BLOKADAPAK = 999 and
          DN.GRUPASPED = :DOCGROUP;

    in autonomous transaction
      do -- [DG] XXX ZG97597
      delete from X_STARTPAK;
    exit;
  end

  select MWS
    from DEFMAGAZ
    where SYMBOL = :WH
  into :MWS;
  if (:MWS = 0) then
  begin
    select first 1 1
      from DOKUMNAG N
        left join DEFMAGAZ D on (N.MAGAZYN = D.SYMBOL)
        left join DEFMAGAZ D1 on (N.MWSMAG = D1.SYMBOL)
      where N.GRUPASPED = :DOCGROUP and
          (D.MWS = 1 or D1.MWS = 1)
    into :MWS;
  end
  --czy wszystko z grupy spedycyjnej zostalo naszykowane
  if (:MWS = 1) then
  begin
    BLOKADAPAK = 0;
    select first 1 P.REF, max(coalesce(N.X_BLOKADAPAK, 0))
      from DOKUMNAG N
        left join DOKUMPOZ P on (P.DOKUMENT = N.REF)
        left join TOWARY T on (T.KTM = P.KTM)
        left join DEFDOKUM DD on (DD.SYMBOL = N.TYP)
        left join DEFMAGAZ D on (N.MAGAZYN = D.SYMBOL)
        left join DEFMAGAZ D1 on (N.MWSMAG = D1.SYMBOL)
      where N.GRUPASPED = :DOCGROUP and
          (P.ILOSCL > P.ILOSCONMWSACTSC or coalesce(N.X_BLOKADAPAK, 0) = 1) and
          T.USLUGA <> 1 and
          DD.KORYG = 0 and
          N.WYDANIA = 1 and
          (D.MWS = 1 or D1.MWS = 1)
      group by P.REF
    into :POSREF, :BLOKADAPAK;

    if (:POSREF is not null) then
    begin
      STATUS = 0;
      if (:BLOKADAPAK = 0) then
      begin
        MSG = 'Nie wszystko z grupy wysyłkowej zostało naszykowane! ';
        ---KPI   ZG137132

        select LIST(DISTINCT N.INT_SYMBOL,',')
          from DOKUMNAG N
            left join DOKUMPOZ P on (P.DOKUMENT = N.REF)
            left join TOWARY T on (T.KTM = P.KTM)
            left join DEFDOKUM DD on (DD.SYMBOL = N.TYP)
            left join DEFMAGAZ D on (N.MAGAZYN = D.SYMBOL)
            left join DEFMAGAZ D1 on (N.MWSMAG = D1.SYMBOL)
          where N.GRUPASPED = :DOCGROUP and
              (P.ILOSCL > P.ILOSCONMWSACTSC or coalesce(N.X_BLOKADAPAK, 0) = 1) and
              T.USLUGA <> 1 and
              DD.KORYG = 0 and
              N.WYDANIA = 1 and
              (D.MWS = 1 or D1.MWS = 1)
        into :WZ_WAPRO;
            MSG = MSG || ' ' || WZ_WAPRO;
      end

      ---endKPI

      else
      if (:BLOKADAPAK = 1) then --[PM] XXX #URUCHOMIENIE
        MSG = 'Dokument zablokowany do pakowania, skontaktuj się z BOK!';
      in autonomous transaction
      do -- [DG] XXX ZG97597
        delete from X_STARTPAK;
      exit;
    end

    select first 1 A.REF
      from DOKUMNAG D
        left join MWSACTS A on (A.DOCID = D.REF and A.DOCTYPE = 'M')
        left join MWSORDS O on (O.REF = A.MWSORD)
      where D.GRUPASPED = :DOCGROUP and
          (O.STATUS <> 5) and
          A.STATUS = 5 and
          A.RECDOC = 0
    into :MWSACTREF;
    if (:MWSACTREF is not null) then
    begin
      STATUS = 0;
      MSG = 'Nie wszystkie pozycje zostały odstawione do pakowania!';
      in autonomous transaction
      do -- [DG] XXX ZG97597
        delete from X_STARTPAK;
      exit;
    end
  end

  MSG = 'Dokumenty z innych magazynów (nie MWS):' || :EOL;
  for select N.SYMBOL, N.MAGAZYN
      from DOKUMNAG N
      left join DEFMAGAZ D on (N.MAGAZYN = D.SYMBOL)
      where N.GRUPASPED = :DOCGROUP and
            N.REF <> :DOCWHREF and
            D.MWS = 0 and
            D.MAGMASTER is null
      into :DOCSYMBS, :DOCWH
  do
  begin
    MSG = :MSG || :DOCSYMBS || ' z mag. ' || :DOCWH || :EOL;
  end
  if (:DOCSYMBS is null) then
    MSG = '';

  if (:ACCEPT = 0) then
  begin
    select cast(list(distinct iif(A.MWSCONSTLOC is not null, L.SYMBOL || ' z ', '') || coalesce(C.MWSCONSTLOCSYMB, 'Brak miejsca odstawienia'), '; ') as MEMO)
      from DOKUMNAG D
      left join MWSACTS A on (A.DOCID = D.REF and A.DOCTYPE = 'M')
      left join MWSCONSTLOCS L on (A.MWSCONSTLOC = L.REF)
      left join MWSORDCARTCOLOURS C on (C.MWSORD = A.MWSORD and C.DOCID = D.REF and L.REF = C.CART)
    where D.GRUPASPED = :DOCGROUP and
          A.STATUS = 5 and
          A.RECDOC = 0
    into :EMWSCONSTLOCSTMP;--[PM] XXX
    --budowanie informacji w oknie pakowania wyswietlanej po otwarciu dokumentu
    if (coalesce(:EMWSCONSTLOCSTMP, '') <> '') then
    begin
      EMWSCONSTLOCS = EMWSCONSTLOCSTMP;
      select first 1 K.FSKROT, O.NAZWA
      from DOKUMNAG DN
      left join KLIENCI K on (K.REF = DN.KLIENT)
      left join ODBIORCY O on (O.REF = DN.ODBIORCAID)
      where DN.GRUPASPED = :DOCGROUP
      into :KLIENT, :ODBIORCA;
      if (:KLIENT is not null) then
        MSG = :MSG || 'Klient ' || coalesce(:KLIENT, '');
      if (:ODBIORCA is not null) then
        MSG = :MSG || ' Odbiorca ' || coalesce(:ODBIORCA, '');
      MSG = :MSG || ' Pobierz koszyki:' || :EOL || :EMWSCONSTLOCS;
    end
  end
  else if (:ACCEPT = 1) then
    MSG = :MSG || 'Dokument jest już spakowany i zaakceptowany.';
  else
    exception UNIVERSAL 'Błąd pobierania dokumentu, zgłoś to!';

  -- XXX WN Start uwagi wewn
  select D.REF, D.SYMBOL, D.MAGAZYN,
         substring(coalesce(D.UWAGI, '') || ' ' || coalesce(D.UWAGIWEWN, '') from 1 for 1024),
         coalesce(K.KONTROLAPAK, 0)
    from DOKUMNAG D
      left join KLIENCI K on (D.KLIENT = K.REF)
    where D.REF = :DOCGROUP
      into :DOCREF, :DOCSYMB, :WH, :DESCRIPT, :PACKCONTROL;
  -- XXX WN Stop

  --moze byc juz zalozony dokument spedycyjny, bo ktos zamknal okno
  if (coalesce(SHIPPINGDOC, 0) = 0) then
  begin
    select first 1 P.DOKUMENT
      from LISTYWYSDPOZ P
        join LISTYWYSD L on (P.DOKUMENT = L.REF)
    where P.DOKTYP = 'M' and
          P.DOKREF = :DOCGROUP and
          coalesce(L.AKCEPT, 0) = 0
    into :SHIPPINGDOC;
    if (SHIPPINGDOC is null) then
      SHIPPINGDOC = 0;
  end

  if (:PACKCONTROL = 0) then
    select coalesce(O.KONTROLAPAK, 0)
      from OPERMAG O
      where O.MAGAZYN = :WH and
          O.OPERATOR = :PACKOPER
    into :PACKCONTROL;

    if (coalesce(:DESCRIPT, '') <> '') then
      MSG = :MSG || :EOL || substring(:DESCRIPT from 1 for 255);

  if (coalesce(char_length(:MSG), 0) > 255) then
    MSG = substring(:MSG from 1 for 250) || '...';
  -- [DG] XXX startpak ZG97597
 /* if (SHIPPINGDOC = 0) then
  begin
    execute procedure LISTYWYSD_ADD_DOC(:DOCREF, 'M', null, :PACKCONTROL, :PACKOPER)
        returning_values :SHIPPINGDOC;
    STATUS = 2;
  end    */
  -- [DG] XXX end
end^
SET TERM ; ^
