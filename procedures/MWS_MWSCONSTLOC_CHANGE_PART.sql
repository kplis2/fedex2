--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_MWSCONSTLOC_CHANGE_PART(
      MWSORDIN integer,
      MWSORDTYPE integer,
      VERSIN integer,
      LOTIN integer,
      QUANTITYIN numeric(14,4),
      MWSCONSTLOCPS varchar(40) CHARACTER SET UTF8                           ,
      MWSCONSTLOCP integer,
      MWSCONSTLOCLS varchar(40) CHARACTER SET UTF8                           ,
      MWSCONSTLOCL integer,
      AUTOCOMMIT smallint,
      ACTUOPERATOR integer,
      MWSSTOCKCHECK smallint,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      PRIORITY smallint,
      UNITP integer = null,
      X_PARTIAS STRING20 = null,
      X_SERIAL_NO STRING = null,
      X_SLOWNIK STRING = null)
  returns (
      MWSORD integer)
   as
declare variable WH varchar(3);
declare variable OPERATOR integer;
declare variable OPERATORDICT varchar(80);
declare variable BLOCKSYMBOL varchar(40);
declare variable TIMESTART timestamp;
declare variable TIMESTARTDCL timestamp;
declare variable TIMESTOPDCL timestamp;
declare variable REFMWSORD integer;
declare variable MWSACCESSORY integer;
declare variable PERIOD varchar(6);
declare variable BRANCH varchar(10);
declare variable MIX smallint;
declare variable GOOD varchar(40);
declare variable PALTYPE varchar(40);
declare variable VERS integer;
declare variable WHAREALOGL integer;
declare variable WHAREALOGP integer;
declare variable WHAREAP integer;
declare variable WHAREAL integer;
declare variable MAXNUMBER integer;
declare variable QUANTITY numeric(14,4);
declare variable REALTIME double precision;
declare variable MWSPALLOCLP integer;
declare variable DIST numeric(14,4);
declare variable STOCKTAKING smallint;
declare variable REC smallint;
declare variable COR smallint;
declare variable MWSPALLOCP integer;
declare variable MWSACTREF integer;
declare variable LOT integer;
declare variable LEVELNUMP integer;
declare variable LEVELNUML integer;
declare variable OPERATORTMP integer;
declare variable MIXTAKEPAL integer;
declare variable MIXLEAVEPAL integer;
declare variable MIXMWSCONSTLOCL integer;
declare variable MIXMWSPALLOCL integer;
declare variable PALL numeric(14,2);
declare variable PALW numeric(14,2);
declare variable PALH numeric(14,2);
declare variable MIXREFILL smallint;
declare variable ACTCNT integer;
declare variable MWSPALLOCL integer;
declare variable DEEPL smallint;
declare variable REFILL smallint;
declare variable MWSCONSTLOCLSYMB varchar(40);
declare variable x_partia date_id;              -- XXX KBI
declare variable serial integer_id;
declare variable slownik integer_id;
begin
  rec = 0;
  cor = 0;
  if (coalesce(x_partias,'') <> '') then
    x_partia = x_partias;

  if (x_serial_no = '') then x_serial_no = null;
  if (x_slownik = '') then x_slownik = null;

  if (coalesce(x_serial_no,'') <> '') then
    select s.ref from X_MWS_SERIE s where s.serialno = :x_serial_no into :serial;

  if (coalesce(x_slownik,'') <> '') then
    select x.ref from X_SLOWNIKI_EHRLE x where x.wartosc = :x_slownik into :slownik;

  if (mwsordtype = 0 or mwsordtype is null) then
  begin
    select first 1 m.ref from mwsordtypes m where m.mwsortypedets = 11
      into mwsordtype;
  end
  refmwsord = mwsordin;
  if (refmwsord = 0) then refmwsord = null;
  stocktaking = 0;
  if (mwsstockcheck is null) then mwsstockcheck = 1;
  if (autocommit is null) then autocommit = 0;
  if (descript is null) then descript = '';

  if (coalesce(:mwsconstlocl,0) = 0) then
    select ref, deep from mwsconstlocs where symbol = :mwsconstlocls
    into mwsconstlocl, deepl;
  else
    select deep from mwsconstlocs where ref = :mwsconstlocl
    into deepl;

  if (deepl is null) then deepl = 0;
  if (deepl = 1) then
  begin
    priority = 1;
    descript =  descript||' Uzupelnienie deep';
  end
  else
    descript = descript||' Przesuniecie palety';
  select ref from mwsconstlocs where symbol = :mwsconstlocps into mwsconstlocp;
  if (mwsordtype is null) then exception MWSORDTYPE_NOT_SET;
  if (mwsstockcheck = 1) then
  begin
    if (exists(select ref from mwsstock where mwsconstloc = :mwsconstlocl and ispal = 0)) then
      exception MWSORD_ALREADY_PLANED 'zaplanowano zlecenie dla lokacji koncowej '||mwsconstlocps||' '||mwsconstlocls;
    if (exists(select ref from mwsstock where mwsconstloc = :mwsconstlocp and ispal = 0 and blocked > 0)) then
      exception MWSORD_ALREADY_PLANED 'zaplanowano zlecenie dla lokacji poczatkowej '||mwsconstlocps||' '||mwsconstlocls;
  end
  select wh from mwsconstlocs where ref = :mwsconstlocp into wh;
  select oddzial from defmagaz where symbol = :wh into branch;
  if (refmwsord is null) then
  begin
    execute procedure gen_ref('MWSORDS') returning_values refmwsord;
    select okres from datatookres(current_date,0,0,0,0) into period;
    insert into mwsords (ref, mwsordtype, stocktaking, doctype, operator, priority, description, wh,
        regtime, timestartdecl, timestopdecl, mwsaccessory, branch, period, cor, rec,
        bwharea, ewharea, status)
      values (:refmwsord, :mwsordtype, :stocktaking, 'P', :operator, :priority, :descript, :wh,
        current_timestamp(0), :timestart, current_timestamp(0), :mwsaccessory, :branch, :period, :cor, :rec,
        null, null, 0);
  end
  -- POZYCJE ZLECENIA MAGAZYNOWEGO
  if (coalesce(mwsconstlocl,0) = 0) then
  begin
    select count(ref) from mwsstock where mwsconstloc = :mwsconstlocp and quantity > 0
      into :mix;
    select first 1 m.good
      from mwsstock m
        left join towary t on (t.ktm = m.good)
      where t.paleta = 1 and m.mwsconstloc = :mwsconstlocp
      into paltype;
    select first 1 m.good, m.vers, mc.l, mc.w, mc.h
      from mwsstock m
        join mwsconstlocs mc on (mc.ref = m.mwsconstloc)
        left join towary t on (t.ktm = m.good)
      where t.paleta <> 1 and m.mwsconstloc = :mwsconstlocp
      into good, vers, pall, palw, palh;
    -- sprawdzamy czy przesuwamy mixa
    if (mix > 2) then mix = 1; else mix = 0;
    execute procedure XK_MWS_GET_BEST_LOCATION(:good, :vers,null,:refmwsord,:mwsordtype, :wh, null, null, 3, 1, 0, :mix,null,:paltype, :palw,:pall,:palh,0,:quantityin)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end
  else
  begin
    select wharea, wharealogfromstartarea from mwsconstlocs where ref = :mwsconstlocl
      into whareal, wharealogl;
  end
  select wharea, wharealogfromstartarea from mwsconstlocs where ref = :mwsconstlocp
    into whareap, wharealogp;
  maxnumber = 0;
  if (mwsconstlocl is not null) then
  begin
    select symbol from mwsconstlocs where ref = :mwsconstlocl into MWSCONSTLOCLSYMB;
    operatortmp = null;
    for
      select good, vers, quantity - blocked, mwspalloc, lot,
          mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, mixrefill,
          x_partia, x_serial_no, x_slownik -- XXX KBI
        from mwsstock
        where mwsconstloc = :mwsconstlocp and quantity - blocked > 0
          and (vers = :versin or :versin is null)
          and (lot = :lotin or :lotin is null or :lotin = 0)
          and (:x_partia is null or x_partia = :x_partia)
          and (:serial is null or x_serial_no = :serial)
          and (:slownik is null or x_slownik = :slownik)
        into good, vers, quantity, mwspallocp, lot,
          mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, mixrefill,
          :x_partia, :serial, :slownik -- XXX KBI
    do begin
      if (versin is not null) then
      begin
        if (quantity >= quantityin and quantityin > 0) then
          quantity = quantityin;
      end
      select mwsstandlevelnumber
        from mwsconstlocs
        where ref = :mwsconstlocp
      into levelnump;
      select mwsstandlevelnumber
        from mwsconstlocs
        where ref = :mwsconstlocl
      into levelnuml;
      if (levelnump is null) then levelnump = 0;
      if (levelnuml is null) then levelnuml = 0;
      select ref from mwsaccessories where aktuoperator = :operator
        into :mwsaccessory;
      if (:versin is null or :quantityin > 0) then
      begin
        maxnumber = maxnumber + 1;
        execute procedure gen_ref('MWSACTS') returning_values mwsactref;
        insert into mwsacts (ref,mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
            mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
            regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, recdoc, plus,
            whareap, whareal, wharealogp, wharealogl, number, disttogo, mixedpallgroup, palgroup,
            mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, refilltry, lot, unitp,
            x_partia, x_serial_no, x_slownik) -- XXX KBI)
          values (:mwsactref,:refmwsord, 0, :stocktaking, :good, :vers, :quantity, :mwsconstlocp,  :mwspallocp,
              :mwsconstlocl, :mwspallocp, null, null, 'O', 0, 0, :wh, :whareap, null,
              current_timestamp(0), :timestartdcl, :timestopdcl, :realtime, null, null, 1, :rec, 1,
              null, :whareal, null, :wharealogl, :maxnumber, :dist, :mix, null,
              :mixtakepal, :mixleavepal, :mixmwsconstlocl, :mixmwspallocl, :mixrefill, :lot, :unitp,
              :x_partia, :serial, :slownik); -- XXX KBI);
        update mwsacts set status = 1 where ref = :mwsactref;
        timestart = timestartdcl;
        if (:versin is not null) then
        begin
          quantityin = quantityin - quantity;
          if (quantityin <= 0) then
            break;
        end
      end
    end
    if (versin is not null and quantityin > 0) then
      exception MWSORD_ALREADY_PLANED 'Brak wystarczajacej ilosci towaru na palecie';
  end
  -- na koniec jest komunikat ze nie ma wolnego miejsca na magazynie
  else
    exception XK_NO_PLACE_IN_WHSEC 'Brak okreslonego miejsca dla palety';
  select count(ref) from mwsacts where mwsord = :refmwsord into actcnt;
  if (actcnt is null) then actcnt = 0;
  if (actcnt > 0) then
  begin
    update mwsords set status = case when :autocommit = -1 then 0 else 1 end, timestopdecl = :timestopdcl, operator = null, mwsaccessory = :mwsaccessory
      where ref = :refmwsord and status = 0;
    update mwsords mo set mo.operator = null where mo.ref = :refmwsord;
    if (autocommit = 1) then
    begin
      update mwsacts ma set ma.status = 2, ma.quantityc = ma.quantity where ma.mwsord = :refmwsord;
      update mwsords mo set mo.status = 5, mo.operator = :actuoperator where mo.ref = :refmwsord;
    end
    mwsord = :refmwsord;
  end else
    delete from mwsords where ref = :refmwsord;
end^
SET TERM ; ^
