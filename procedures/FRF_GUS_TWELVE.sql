--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_GUS_TWELVE(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      FIELD varchar(5) CHARACTER SET UTF8                           )
  returns (
      AMOUNT numeric(15,2))
   as
declare variable B1 integer;
declare variable B2 varchar(40);
declare variable B3 integer;
declare variable B4 integer;
declare variable B5 integer;
declare variable frvhdr integer;
declare variable frpsn integer;
declare variable frcol integer;
declare variable yearid integer;
declare variable cacheparams varchar(255);
declare variable ddparams varchar(255);
declare variable company integer;
declare variable frversion integer;
declare variable monthid integer;
declare variable lastday date;
declare variable symbol varchar(15);
declare variable i integer;
declare variable ref1 integer;
declare variable ref2 integer;
declare variable ref3 integer;
declare variable ref4 integer;
declare variable ref5 integer;
declare variable ref6 integer;
declare variable ref7 integer;
declare variable ref8 integer;
declare variable ref9 integer;
declare variable ref10 integer;
declare variable ref11 integer;
declare variable ref12 integer;
declare variable ref13 integer;
declare variable ref14 integer;
begin
  if (frvpsn = 0) then exit;
  select frvhdr, frpsn, frcol from frvpsns
    where ref = :frvpsn
    into :frvhdr, :frpsn, :frcol;

  select company, frversion from frvhdrs
    where ref = :frvhdr
    into :company, :frversion;

  yearid = cast(substring(period from 1 for 4) as integer);
  cacheparams = field||';'||yearid||';'||company;
  -- cacheparams = frpsn||';'||frcol||';'||yearid||';'||company;
  ddparams = '';

  if (not exists (select ref from frvdrilldown where functionname = 'GUS_TWELVE'
           and cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr)
  ) then begin

    select symbol from frpsns
      where ref = :frpsn
      into :symbol;

   for
     select first 14 em.ref as B1,ed.workpost as B2,substring(eg.code from 1 for 4) as B3,p.sex as B4, ez.code as B5
       from employees em
       join persons P on p.ref = em.person
       join edictworkposts ed on em.workpost = ed.ref
       join edictguscodes eg on ed.guscode = eg.ref
       join edictzuscodes ez on ez.ref = p.education
       order by B1,B2,B3,B4
    into :B1,:B2,:B3,:B4,:B5
    do begin
      i=1;
      if (i=1) then ref1 = B1;
      if (i=2) then ref2 = B1;
      if (i=3) then ref3 = B1;
      if (i=4) then ref4 = B1;
      if (i=5) then ref5 = B1;
      if (i=6) then ref6 = B1;
      if (i=7) then ref7 = B1;
      if (i=8) then ref8 = B1;
      if (i=9) then ref9 = B1;
      if (i=10) then ref10 = B1;
      if (i=11) then ref11 = B1;
      if (i=12) then ref12 = B1;
      if (i=13) then ref13 = B1;
      if (i=14) then ref14 = B1;
      i = i+1;
      if (field = 'B1,1') then amount = ref1;
      if (field = 'B1,2') then amount = ref2;
      if (field = 'B1,3') then amount = ref3;
      if (field = 'B1,4') then amount = ref4;
      if (field = 'B1,5') then amount = ref5;
      if (field = 'B1,6') then amount = ref6;
      if (field = 'B1,7') then amount = ref7;
      if (field = 'B1,8') then amount = ref8;
      if (field = 'B1,9') then amount = ref9;
      if (field = 'B1,10') then amount = ref10;
      if (field = 'B1,11') then amount = ref11;
      if (field = 'B1,12') then amount = ref12;
      if (field = 'B1,13') then amount = ref13;
      if (field = 'B1,14') then amount = ref14;
    end

  end else
    select first 1 fvalue from frvdrilldown where cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr
      into :amount;
  if (amount is null) then amount = 0;
  insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
     values (:frvhdr, :frpsn, :frcol, 'GUS_TWELVE', :cacheparams, :ddparams, :amount, :frversion);
end^
SET TERM ; ^
