--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_SIGNATURESEARCH(
      SIGNATURE STRING40)
  returns (
      OBJECTTYPE STRING40,
      OBJECTNAME STRING100)
   as
begin
 objecttype = 'procedure';
 for
   select o.rdb$procedure_name
     from rdb$procedures o
     where o.rdb$description containing :signature
     into :objectname
 do
   suspend;
 objecttype = 'transfer procedure';
 for
   select o.rdb$procedure_name
     from rdb$procedures o
     where o.rdb$procedure_name containing :signature
     into :objectname
 do
   suspend;
 objecttype = 'trigger';
 for
   select o.rdb$trigger_name
     from rdb$triggers o
     where o.rdb$description containing :signature
     into :objectname
 do
   suspend;
 objecttype = 'table';
 for
   select o.rdb$relation_name
     from rdb$relations o
     where o.rdb$description containing :signature
     into :objectname
 do
   suspend;
 objecttype = 'field';
 for
   select o.rdb$relation_name||'.'||o.rdb$field_name
     from rdb$relation_fields o
     where o.rdb$description containing :signature
     into :objectname
 do
   suspend;
 objecttype = 'index';
 for
   select o.rdb$index_name
     from rdb$indices o
     where o.rdb$description containing :signature
     into :objectname
 do
   suspend;
 objecttype = 'domain';
 for
   select o.rdb$field_name
     from rdb$fields o
     where o.rdb$description containing :signature
     into :objectname
 do
   suspend;
 objecttype = 's_appinin section';
 for
   select o.section
     from s_appini o
     where o.ident = 'Signature'
       and o.val containing :signature
     into :objectname
 do
   suspend;
 objecttype = 's_appinin item';
 for
   select o.section || '.' || o.ident
     from s_appini o
     where o.ident like '%_:Signature'
       and o.val containing :signature
     into :objectname
 do
   suspend;
end^
SET TERM ; ^
