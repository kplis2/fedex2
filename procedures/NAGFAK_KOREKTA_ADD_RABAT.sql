--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_KOREKTA_ADD_RABAT(
      KOREKTA integer,
      MASKA varchar(255) CHARACTER SET UTF8                           ,
      GRUPA varchar(255) CHARACTER SET UTF8                           ,
      RABATW numeric(14,4),
      RABATN numeric(14,4),
      RABATP numeric(14,4))
   as
declare variable SUMWARTBRU numeric(14,4);
declare variable SUMWARTNET numeric(14,4);
declare variable KOREKTARABAT numeric(14,4);
declare variable NAGFAKRABAT numeric(14,4);
declare variable NAGFAK integer;
declare variable RABATWRONG smallint;
begin
  korektarabat = 0;
  nagfakrabat = 0;
  if(grupa = '') then grupa = null;
  if(maska = '') then maska = null;
  rabatwrong = 0;
  if(coalesce(rabatw,0)>0) then rabatwrong = rabatwrong + 1;
  if(coalesce(rabatn,0)>0) then rabatwrong = rabatwrong + 1;
  if(coalesce(rabatp,0)>0) then rabatwrong = rabatwrong + 1;
  if(rabatwrong>1) then
    exception universal 'Niedozwolone jednoczesne korygowanie rabatem wartościowym i procentowym';
  if(rabatw is null) then rabatw = 0;
  if(rabatn is null) then rabatn = 0;
  if(rabatp is null) then rabatp = 0;
  if(rabatp>100) then
    exception universal 'Niedozwolona wartość rabatu procentowego';
  select sum(coalesce(p.wartbru,0)), sum(coalesce(p.wartnet,0))
    from nagfak n
    join pozfak p on p.dokument = n.ref
    left join towary t on p.ktm = t.ktm
    where n.korekta = :korekta --and n.wasdeakcept>=10
    and (:grupa is null or :grupa = t.grupa) and (:maska is null or p.ktm like :maska)
  into :sumwartbru, :sumwartnet;
  if(sumwartbru < rabatw or (sumwartbru = 0) or sumwartnet < rabatn or (sumwartnet = 0)) then
    exception universal 'Niedozwolona wartość rabatu wartościowego - rabat wyższy od wartości faktury';
  if(rabatw > 0) then rabatw = rabatw/sumwartbru;
  else rabatw = rabatn/sumwartnet;
  select rabat from nagfak where ref = :korekta into :korektarabat;
  execute procedure nagfak_obl_psum(:korekta);
  update pozfak p set p.cenacen = p.cenacen  -p.cenacen*:rabatw,
      p.rabat = (select p1.rabat from pozfak p1 where p1.ref = p.refk) + (:rabatp-:korektarabat+(select n.rabat from nagfak n where n.ref = (select np.dokument from pozfak np where np.ref = p.refk))),
      p.prabat = (select p1.rabat from pozfak p1 where p1.ref = p.refk) +(select n.rabat from nagfak n where n.ref = (select np.dokument from pozfak np where np.ref = p.refk))
    where p.dokument = :korekta and
      (:grupa is null or :grupa = (select t.grupa from towary t where t.ktm = p.ktm))
      and (:maska is null or p.ktm like :maska);
  update nagfak set wasdeakcept = wasdeakcept - 10 where korekta = :korekta and wasdeakcept >= 10;
end^
SET TERM ; ^
