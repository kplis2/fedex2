--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CREATE_ACCOUNT_MASK(
      ACCONTOLD ACCOUNT_ID)
  returns (
      ACCONTNEW ACCOUNT_ID)
   as
declare variable lengt smallint;
  declare variable tmp varchar(1);
  declare variable i smallint;
begin
  -- Procedure która na podstawie stringa wejsciowego generuje
  -- stringa wyjsciowego z odpowiednim kontem
  i = 0; -- czy od tylu pojawil sie znak rozny od ? lub _

  accontnew = '';
  lengt =  coalesce(char_length(accontold),0); -- [DG] XXX ZG119346
  while (lengt > 0) do
  begin
    tmp = substring(accontold from  lengt for  lengt);
    if (tmp = '?' or tmp = '_') then
    begin
      if (i > 0) then
        accontnew = '_' || accontnew;
    end else
    begin
      i = 1;
      accontnew = tmp || accontnew;
    end
    lengt = lengt - 1;
  end
  suspend;
end^
SET TERM ; ^
