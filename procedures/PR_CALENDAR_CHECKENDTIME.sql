--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_CALENDAR_CHECKENDTIME(
      PRDEPART varchar(10) CHARACTER SET UTF8                           ,
      STARTTIME timestamp,
      WORKTIME double precision)
  returns (
      ENDTIME timestamp)
   as
declare variable CURDATE timestamp;
declare variable ST time;
declare variable ET time;
declare variable CALENDAR integer;
declare variable DAYREF integer;
declare variable DAYKIND integer;
declare variable DAYWORKTIME double precision;
declare variable DAYSTART timestamp;
declare variable DAYEND timestamp;
begin
  curdate = cast (starttime as date);
  endtime = :starttime;
  select prdeparts.calendar from PRDEPARTs where SYMBOL = :PRDEPART into :calendar;
  if(:calendar is null) then
    exception UNIVERSAL 'Brak kalendarza dla wydzialu '||:prdepart;
  for select e.REF, Cdate, edk.daytype, WORKSTART, WORKEND, PRWORKTIME
    from ecaldays e
    join edaykinds edk on edk.ref = e.daykind
    where calendar = :calendar and cdate >= :curdate
    order by calendar, cdate
    into :dayref, :curdate,  :daykind, :st,  :et, :dayworktime
  do begin

    if(:daykind <> 1) then begin--dzien niepracujacy - pomijam
      endtime = :curdate + 1;--przesuniecie na początek dnia kolejnego
    end else begin
      daystart = cast(:curdate as date)||' '||:st;
      dayend = cast(:curdate as date)||' '||:et;
      if(:dayend = :curdate) then--zakonczenie dnia o polnocy
        dayend = :curdate + 1;
      if(:starttime > :daystart) then--zmniejsze ilosci dnia roboczego, bo opracja sie zaczyna w trakcie dnia
        dayworktime = (:dayend - :starttime);
      else
        starttime = :daystart;
      if(:dayworktime > 0) then begin-- moze byc mniejsze, jesli starttime byl po zakoczeniu dnia pracy
        if(:worktime < :dayworktime) then begin
          endtime = :starttime + :worktime;
          exit;
        end else begin
          endtime = :curdate + 1;
          worktime = :worktime - :dayworktime;
        end
      end else
        endtime = :curdate + 1;
    end
  end
end^
SET TERM ; ^
