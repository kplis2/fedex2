--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMPLOYEES_UPDATE(
      EMPLREF integer,
      CONTRACTDATE timestamp)
   as
declare variable MAXDATE timestamp;
declare variable BRANCH varchar(10);
declare variable DEPT varchar(10);
declare variable CWORKTYPE integer;
declare variable WORKPOST integer;
declare variable ECONTRTYPE smallint;
declare variable CALENDAR integer;
declare variable ORDERJOB smallint;
declare variable AUTOPRESENT smallint;
declare variable CEMPLGROUP integer;
declare variable CEPAYRULE integer;
begin
  --ustalam date aktualnej umowy
  select max(c.fromdate)
    from emplcontracts c
      left join econtrtypes t on (t.contrtype = c.econtrtype)
    where c.employee = :emplref and t.empltype = 1
    into :maxdate;

  if (maxdate is null) then
  begin
   --brak umowy o prace, szukam innych umow
    select max(fromdate) from emplcontracts
      where employee = :emplref
      into :maxdate;
  end

  -- pobieram informacje z emplcontracts
  select first 1 cbranch, cdepartment, econtrtype, cworkpost, cworktype,
         autopresent, cemplgroup, cepayrule
    from emplcontracts
    where employee = :emplref and fromdate = :maxdate
      and empltype > 0
    order by empltype  --BS48914
    into :branch, :dept, :econtrtype, :workpost, :cworktype,
         :autopresent, :cemplgroup, :cepayrule;

  select first 1 calendar
    from emplcalendar
    where employee = :emplref
    order by fromdate desc
    into :calendar;

  -- sprawdzam czy jest zleceniobiorca
  orderjob = 0;
  if (exists(select ref from emplcontracts c left join econtrtypes t on (t.contrtype = c.econtrtype)
       where c.employee = :emplref and t.empltype <> 1)) then
    orderjob = 1;

  -- zapisuje informacje do employees
  if (cworktype = 0) then
    cworktype = null;

  if (contractdate = maxdate or contractdate is null) then  --BS48914
  begin
    update employees set branch = :branch, department = :dept, worktype = :cworktype,
       econtrtype = :econtrtype, calendar = :calendar, workpost = :workpost,
       orderjob = :orderjob, autopresent = :autopresent, emplgroup = :cemplgroup,
       epayrule = :cepayrule
      where ref = :emplref;
  end
end^
SET TERM ; ^
