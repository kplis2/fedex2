--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_TOW_PROCESS(
      SESJAREF SESJE_ID,
      TABELA TABLE_ID,
      REF INTEGER_ID,
      BLOKUJZALEZNE SMALLINT_ID = 0,
      TYLKONIEPRZETWORZONE SMALLINT_ID = 0,
      AKTUALIZUJSESJE SMALLINT_ID = 1)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable stabela table_id;
declare variable tmptabela table_id;
declare variable sprocedura string255;
declare variable szrodlo zrodla_id;
declare variable skierunek smallint_id;
declare variable sesja sesje_id;
declare variable sql memo;
declare variable tmpsql memo;
declare variable tmpstatus smallint_id;
declare variable tmpmsg string255;
declare variable error smallint_id;
declare variable errormsg string255;
declare variable error2 smallint_id;
declare variable tmperror smallint_id;
declare variable errormsg2 string255;
declare variable statuspoz smallint_id;
--TOWAR
declare variable T_REF INTEGER_ID;
declare variable T_TOWARID STRING120;
declare variable T_SYMBOL STRING255;
declare variable T_NAZWA STRING255;
declare variable T_GRUPA STRING255;
declare variable T_GRUPAID STRING120;
declare variable T_RODZAJ STRING255;
declare variable T_RODZAJTMP STRING255;
declare variable T_RODZAJID STRING120;
declare variable T_AKTYWNY INTEGER_ID;
declare variable T_VAT STRING10;
declare variable T_OPIS blob_utf8;
declare variable T_CENAZAKUPUNET  KURS_ID;
declare variable T_CENAZAKUPUBRU  KURS_ID;
declare variable T_CHODLIWY INTEGER_ID;
declare variable T_WITRYNA INTEGER_ID;
declare variable T_HASH STRING255;
declare variable T_DEL INTEGER_ID;
declare variable T_REC STRING255;
declare variable T_SKADTABELA STRING40;
declare variable T_SKADREF INTEGER_ID;


--towar zmienne pomocnicze
declare variable T_TMP_TOWTYPE SMALLINT_ID; --nie zmieniac na domene towtype_id !!!!
declare variable T_TMP_NAZWA TOWARY_NAZWA;
--JEDNOSTKA
declare variable J_REF INTEGER_ID;
declare variable J_TOWARID STRING120;
declare variable J_JEDNOSTKA STRING60;
declare variable J_JEDNOSTKAID STRING120;
declare variable J_GLOWNA INTEGER_ID;
declare variable J_LICZNIK NUMERIC_14_4;
declare variable J_MIANOWNIK NUMERIC_14_4;
declare variable J_WAGA NUMERIC_14_4;
declare variable J_WYSOKOSC NUMERIC_14_4;
declare variable J_DLUGOSC NUMERIC_14_4;
declare variable J_SZEROKOSC NUMERIC_14_4;
declare variable J_HASH STRING255;
declare variable J_DEL INTEGER_ID;
declare variable J_REC STRING255;
declare variable J_SKADTABELA STRING40;
declare variable J_SKADREF INTEGER_ID;
--jednostka zmienne pomocnicze
declare variable J_TMP_PRZELICZ NUMERIC_14_4;
declare variable J_GLOWNASENTE SMALLINT_ID;
--KOD KRESKOWY
declare variable K_REF INTEGER_ID;
declare variable K_TOWARID STRING120;
declare variable K_JEDNOSTKA STRING60;
declare variable K_JEDNOSTKAID STRING120;
declare variable K_KODKRESKOWY STRING255;
declare variable K_GLOWNY INTEGER_ID;
declare variable K_HASH STRING255;
declare variable K_DEL INTEGER_ID;
declare variable K_REC STRING255;
declare variable K_SKADTABELA STRING40;
declare variable K_SKADREF INTEGER_ID;
--kod kreskowy zmienne pomocnicze

--zmienne ogolne
declare variable KTM KTM_ID;
declare variable WERSJAREF WERSJE_ID;
declare variable JEDN JEDN_MIARY;
declare variable TOWJEDN TOWJEDN;
declare variable TOWKODKRESK TOWKODKRESK_ID;
declare variable dataostprzetwtmp  timestamp_id;
declare variable ktmtmp ktm_id;
declare variable glowna_sentetmp smallint_id;
declare variable towjedntmp integer_id;

--Towar dane oryginalne
declare variable o_ktm ktm_id;
declare variable o_kodprod ktm_id;
declare variable o_nazwa towary_nazwa;
declare variable o_grupa string60;
declare variable o_akt smallint_id;
declare variable o_vat vat_id;
declare variable o_x_opis blob_utf8;
declare variable o_cena_zakn ceny;
declare variable o_cena_zakb ceny;
declare variable o_chodliwy smallint_id;
declare variable o_witryna smallint_id;
declare variable o_miara jedn_miary;
declare variable o_int_zrodlo zrodla_id;
declare variable o_int_id string120;
declare variable o_usluga smallint_id;
declare variable o_int_dataostprzetw timestamp;
declare variable o_int_sesjaostprzetw sesje_id;
declare variable o_x_int_grupa_id string60;

-- orginalna jednostak glowna

--declare variable  jedn,
declare variable o_glowna smallint_id;
declare variable o_przelicz numeric_14_4;
declare variable o_const smallint_id;
declare variable o_s smallint_id;
declare variable o_z smallint_id;
declare variable o_m smallint_id;
declare variable o_c smallint_id;
declare variable o_waga waga_id;
declare variable o_wys waga_id;
declare variable o_dlug waga_id;
declare variable o_szer waga_id;
--declare variable int_id,


begin
  --zmiana pustych wartosci na null-e
  if (sesjaref = 0) then
    sesjaref = null;
  if (trim(tabela) = '') then
    tabela = null;
  if (ref = 0) then
    ref = null;
  if (blokujzalezne is null) then
    blokujzalezne = 0;
  if (tylkonieprzetworzone is null) then
    tylkonieprzetworzone = 0;
  if (aktualizujsesje is null) then
    aktualizujsesje =1;

  status = 1;

  --sprawdzenie czy wszystkie potrzebne informacje sa
  if (sesjaref is null and ref is null) then
    begin
      status = -1;
      msg = 'Za malo parametrow.';
      exit; --EXIT
    end

  if (sesjaref is null) then
  begin
    if (ref is null) then
      begin
        status = -1;
        msg = 'Jesli nie podales sesji to wypadaloby podac ref-a...';
        exit; --EXIT
      end
    if (tabela is null) then
      begin
        status = -1;
        msg = 'Wypadaloby podac nazwe tabeli...';
        exit; --EXIT
      end


    sql = 'select sesja from '||:tabela||' where ref='||:ref;
    execute statement sql into :sesjaref;

  end
  else
    sesja = sesjaref;

  if (sesjaref is null) then
    begin
      status = -1;
      msg = 'Nie znaleziono numeru sesji.';
      exit; --EXIT
    end

  --pobranie parametrow sesji
  select s.tabela, s.procedura, s.zrodlo, s.kierunek
    from int_sesje s
    where ref = :sesja
    into :stabela, :sprocedura, :szrodlo, :skierunek;

  if (tabela is not null) then --jesli podano tabele na wejsciu traktuje ja nadrzednie
    stabela = tabela;

  --wlasciwe przetworzenie
  if (stabela = 'INT_IMP_TOW_TOWARY') then
  begin
    for
      select ref, towarid, symbol, nazwa,
            grupa, grupaid, aktywny, vat, opis,
            cenazakupunet, cenazakupubru, chodliwy, witryna,
            rodzaj, rodzajid,
            --"hash",
            del, rec
        from INT_IMP_TOW_TOWARY t
        where sesja = :sesja and (ref = :ref or :ref is null)
        order by ref
      into :t_ref, :t_towarid, :t_symbol, :t_nazwa,
          :t_grupa, :t_grupaid, :t_aktywny, :t_vat, :t_opis,
          :t_cenazakupunet, :t_cenazakupubru, :t_chodliwy, :t_witryna,
          :t_rodzaj, :t_rodzajid,
          --:t_hash,
          :t_del, :t_rec
    do begin
      error = 0;
      errormsg = '';
      error2 = 0;
      errormsg2 = '';
      tmperror = null;

      ktm = null;
      wersjaref = null;
      jedn = null;
      towjedn = null;

      if (coalesce(trim(t_vat),'') <> '') then -- [DG/PM] XXX
        t_vat = lpad(:t_vat,2,'0'); -- [DG] XXX dodany lpad

      if (coalesce(trim(t_towarid),'') = '') then
      begin
        error = 1;
        errormsg = errormsg || 'Brak TOWARID.';
      end

      if (coalesce(t_del,0) = 1 and error = 0) then --rekord do skasowania
      begin
        --select status, msg from INT_IMP_TOW_TOWARY_DEL(t_towarid)
          --into :tmpstatus, :tmpmsg;
        tmpstatus = null;
      end
      else if (error = 0 and coalesce(t_del,0) = 0) then
      begin
        j_jednostka = null;
        j_jednostkaid = null;
        j_glowna = null;
        j_licznik = null;
        j_mianownik = null;
        j_waga = null;
        j_wysokosc = null;
        j_dlugosc = null;
        j_szerokosc = null;
        j_hash = null;
        j_del = null;
        j_rec = null;
        
        --w ramach towaru konieczne jest przetwarzanie jednostek
        --poniewa na triggerach zakladana jest jednostka glowna
        select first 1 ref, jednostka, jednostkaid, glowna, licznik, mianownik,
            waga, wysokosc, dlugosc, szerokosc,
            --"hash",
            del, rec, glowna_sente
          from INT_IMP_TOW_JEDNOSTKI j
          where j.sesja = :sesja and j.skadtabela = :stabela and j.skadref = :t_ref
            --and coalesce(j.glowna,0) > 0 --w zaleznosci od potrzeb
            and coalesce(del,0) = 0
          order by coalesce(glowna,0) desc,
            case when coalesce(licznik,0) = 1 and coalesce(mianownik, 1) = 1 then 0 else 1 end,
            ref
        into :j_ref, :j_jednostka, :j_jednostkaid, :j_glowna, :j_licznik, :j_mianownik,
          :j_waga, :j_wysokosc, :j_dlugosc, :j_szerokosc,
          --:j_hash,
          :j_del, :j_rec, :j_glownasente;

        if (coalesce(trim(:j_jednostka),'') = '') then
          begin
            status = -1; -- XXX KBI;
            error = 1;
            errormsg = :errormsg || 'Brak jednostki glownej dla '||t_towarid;
          end

        j_tmp_przelicz = coalesce(:j_licznik,1) / coalesce(:j_mianownik,1);

        if (coalesce(trim(:j_tmp_przelicz),0) <> 1) then
        begin
          status = -1; -- XXX KBI;
          error = 1;
          error2 = 1;
          errormsg = :errormsg || 'Przelicznik na glownej jedn. musi byc 1. '||:t_towarid;
          errormsg2 = :errormsg2 || 'Przelicznik na glownej jedn. musi byc 1. '||:t_towarid;
        end

        if (coalesce(trim(:t_vat),'') = '') then
          select cvalue from get_config('VAT', 0, null) into :t_vat;
        else if (not exists (select first 1 1 from vat where grupa = :t_vat)) then
        begin
          status = -1; -- XXX KBI;
          error = 1;
          errormsg = :errormsg || 'Nie ma takiego VAT-u. Dla '||:t_towarid;
        end

        if(char_length(:t_grupa) > 60)then
        begin
          status = -1; -- XXX KBI;
          error = 1;
          errormsg = :errormsg || 'Zbyt dluga nazwa grupy ('||:t_grupa||') dla '||:t_towarid;
          --t_grupa = right(:t_grupa,60);
        end

        --blokujzalezne = 1; --!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if (  :blokujzalezne = 0) then --tabele zalezne BEFORE
        begin
          select status, msg from int_tabelezalezne_process(:sesja,
              :stabela, :ref, :szrodlo, :skierunek, 0)
            into :tmpstatus, :tmpmsg;
          if(coalesce(:tmpstatus,0) = -1 )then
          begin
            status = -1 ; --- XXX KBI
            error = 1;
            errormsg = substring(errormsg || ' ' || coalesce(:tmpmsg,'') from 1 for 255);
          end 
        end



        if (error = 0) then
        begin

          --jednostka miary start
          --1
          select first 1 m.miara
            from int_slownik s
            join miara m on (m.miara = s.okey)
            where s.zrodlo = :szrodlo
              and s.wartoscid is not distinct from :j_jednostkaid
              --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
              and s.otable = 'MIARA'
            into :jedn;
   /*       if (:jedn is null) then
            --2
            select first 1  m.miara from int_slownik s
            join miara m on m.miara = s.okey
            where s.zrodlo = :szrodlo
              and lower(s.wartosc) is not distinct from lower(:j_jednostka)
              --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
              and s.otable = 'MIARA'
            into :jedn;
          if (:jedn is null) then
            --3
            select first 1  m.miara from int_slownik s
            join miara m on m.miara = s.okey
            where s.zrodlo = :szrodlo
              and lower(trim('.' from s.wartosc)) is not distinct from lower(trim('.' from :j_jednostka))
              --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
              and s.otable = 'MIARA'
            into :jedn;
          if (:jedn is null) then
            --4
            select first 1  m.miara from int_slownik s
            join miara m on m.miara = s.okey
            where s.zrodlo = :szrodlo
              and s.wartosc is not distinct from lower(replace(:j_jednostka,'.',''))
              --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
              and s.otable = 'MIARA'
            into :jedn;
          if (:jedn is null) then
            --5
            select first 1  m.miara from int_slownik s
              join miara m on m.miara = s.okey
              where s.zrodlo = :szrodlo
                and lower(s.wartosc) is not distinct from lower(replace(:j_jednostka,'.',''))
                --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
                and s.otable = 'MIARA'
              into :jedn;
          if (:jedn is null) then
            --6
            select first 1  m.miara from int_slownik s
              join miara m on m.miara = s.okey
              where s.zrodlo = :szrodlo
                and trim(s.wartosc) is not distinct from trim(:j_jednostka)
                --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
                and s.otable = 'MIARA'
              into :jedn;
          if (:jedn is null) then
            --7
            select first 1  m.miara from int_slownik s
              join miara m on m.miara = s.okey
              where s.zrodlo = :szrodlo
                and trim(lower(s.wartosc)) is not distinct from trim(lower(:j_jednostka))
                --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
                and s.otable = 'MIARA'
              into :jedn;
          if (:jedn is null) then
            --8
            select first 1  m.miara from int_slownik s
              join miara m on m.miara = s.okey
              where s.zrodlo = :szrodlo
                and lower(s.wartosc) is not distinct from substring(:j_jednostka from  1 for 5)
                --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
                and s.otable = 'MIARA'
              into :jedn;
          if (:jedn is null) then
            --9
            select first 1  m.miara from int_slownik s
              join miara m on m.miara = s.okey
              where s.zrodlo = :szrodlo
                and lower(s.wartosc) is not distinct from lower(replace(trim(substring(:j_jednostka from  1 for 5)),' ', ''))
                --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
                and s.otable = 'MIARA'
              into :jedn;
          if (:jedn is null) then
            --10
            select first 1  m.miara from int_slownik s
              join miara m on m.miara = s.okey
              where s.zrodlo = :szrodlo
                and lower(s.wartosc) is not distinct from lower(replace(replace(trim(substring(:j_jednostka from  1 for 5)),' ', ''), '.', ''))
                --and (:j_jednostkaid is null or s.wartoscid is not distinct from :j_jednostkaid)
                and s.otable = 'MIARA'
              into :jedn;

                      */
          if (coalesce(trim(:jedn),'') = '') then
          begin
            status = -1 ; --- XXX KBI
            error = 1;
            errormsg = substring(errormsg || ' ' || 'Brak jednostki miary.' from 1 for 255);
          end

         /*

         select miara from miara where miara = :j_jednostka into :jedn;
          if (:jedn is null) then
            select miara from miara where lower(miara) = lower(:j_jednostka) into :jedn;
          if (:jedn is null) then
            select miara from miara where miara = replace(:j_jednostka,'.','') into :jedn;
          if (:jedn is null) then
            select miara from miara where lower(miara) = lower(replace(:j_jednostka,'.','')) into :jedn;
          if (:jedn is null) then
            select miara from miara where trim(miara) = trim(:j_jednostka) into :jedn;
          if (:jedn is null) then
            select miara from miara where trim(lower(miara)) = trim(lower(:j_jednostka)) into :jedn;
          if (:jedn is null) then
            select miara from miara where id_zewn = :j_jednostka into :jedn;
          if (:jedn is null) then
            select miara from miara where miara = substring(:j_jednostka from  1 for 5) into :jedn;
          if (:jedn is null) then
            select miara from miara where lower(miara) = lower(trim(substring(:j_jednostka from  1 for 5))) into :jedn;
          if (:jedn is null) then
            select miara from miara where lower(miara) = lower(replace(trim(substring(:j_jednostka from  1 for 5)),' ', '')) into :jedn;
          if (:jedn is null) then
            select miara from miara where lower(miara) = lower(replace(replace(trim(substring(:j_jednostka from  1 for 5)),' ', ''), '.', '')) into :jedn;
          */



          /*
          if (:jedn is null) then
            insert into miara (miara, opis, dokladnosc, id_zewn)
              values(substr(lower(trim(replace(:j_jednostka,' ',''))),1,5),
                :j_jednostka, 2, :j_jednostka)
              returning miara into :jedn; --JEDN

          */
          --jednostka miary koniec

          --nullowanie, zerowanie i blankowanie - towar
          if (coalesce(trim(:t_symbol),'') = '') then
            t_symbol = null;
          if (:t_nazwa is null) then
            t_nazwa = '';
          if (:t_grupa is null) then
            t_grupa = '';
          if (coalesce(trim(:t_grupaid),'') = '') then
            t_grupaid = null;
          if (:t_rodzaj is null) then
            t_rodzaj = '';
          if (coalesce(trim(:t_rodzajid),'') = '') then
            t_rodzajid = null;
          if (:t_aktywny is null) then
            t_aktywny = 1;
          if (coalesce(trim(:t_opis),'') = '') then
            t_opis = null;
          if (:t_cenazakupunet is null) then
            t_cenazakupunet = 0;
          if (:t_cenazakupubru is null) then
            t_cenazakupubru = 0;
          if (:t_chodliwy is null) then
            t_chodliwy = 0;
          if (:t_witryna is null) then
            t_witryna = 0;

          t_tmp_towtype = null;
          if (:t_rodzajid is not null) then
            select first 1  numer from towtypes where id_zewn = :t_rodzajid into :t_tmp_towtype;
          if (:t_tmp_towtype is null and :t_rodzaj <> '') then
            select first 1  numer from towtypes where id_zewn = :t_rodzaj into :t_tmp_towtype;
          if (:t_tmp_towtype is null and :t_rodzaj <> '') then
            select first 1  numer from towtypes where lower(nazwa) = lower(:t_rodzaj) into :t_tmp_towtype;
          --XXX JO: roznica polskich znakow
          if (:t_tmp_towtype is null and :t_rodzaj <> '') then
          begin
            execute procedure usun_znaki(lower(:t_rodzaj),0, null, null)
              returning_values :t_rodzajtmp;

            select first 1  numer from towtypes
              where lower(nazwa) = lower(:t_rodzajtmp)
                or lower(nazwapoj) = lower(:t_rodzajtmp)
            into :t_tmp_towtype;
          end
          --XXX JO: koniec
          if (:t_tmp_towtype is null) then --jesli nie znalazlem to uznaje za towar
            t_tmp_towtype = 0;

          --t_tmp_nazwa = substr(coalesce(:t_symbol||' - ', '')||:t_nazwa, 1 ,80);
          t_tmp_nazwa = substring(coalesce(:t_nazwa,'') from  1  for 80);
          -- [DG] XXX jezeli nazwa pusta to wpisuje symbol towaru
          if (coalesce(t_tmp_nazwa,'') = '') then
          begin
            t_tmp_nazwa = substring(coalesce(:t_symbol,'') from  1  for 80);
          end

          --najpierw wrzucamy towar, jednostka glowna zaklada sie na triggerach
          select ktm, kodprod, nazwa, grupa, akt, vat, x_opis, cena_zakn, cena_zakb,
              chodliwy, witryna, miara, int_zrodlo, int_id, usluga, x_int_grupa_id
            from towary
            where ktm = :t_towarid
          into  :ktm, :o_kodprod, :o_nazwa, :o_grupa, :o_akt, :o_vat, :o_x_opis, :o_cena_zakn, :o_cena_zakb,
              :o_chodliwy, :o_witryna, :o_miara, :o_int_zrodlo, :o_int_id, :o_usluga, :o_x_int_grupa_id;

          if (ktm is not null) then begin
            if (o_kodprod is distinct from substring(:t_symbol from 1 for 40) or
                o_nazwa is distinct from t_tmp_nazwa or
                o_grupa is distinct from t_grupa or
                -- o_akt is distinct from -- XXX kbi aktywnosc towaru przyp[isywana na sztywno ??
                o_vat  is distinct from  t_vat or
                o_x_opis is distinct from t_opis or
                o_cena_zakn is distinct from t_cenazakupunet or
                o_cena_zakb is distinct from t_cenazakupubru or
                o_chodliwy  is distinct from  t_chodliwy or
                o_witryna  is distinct from t_witryna or
                o_miara  is distinct from  jedn or
                o_int_zrodlo  is distinct from szrodlo or
                o_int_id  is distinct from t_towarid or
                o_usluga  is distinct from  t_tmp_towtype or
                o_x_int_grupa_id  is distinct from t_grupaid)
              then begin
                update towary set kodprod = substring(:t_symbol from 1 for 40), nazwa = :t_tmp_nazwa,
                    grupa = :t_grupa, vat = :t_vat, x_opis = :t_opis, cena_zakn = :t_cenazakupunet,
                    cena_zakb = :t_cenazakupubru, chodliwy = :t_chodliwy, witryna = :t_witryna,
                    miara = :jedn, int_zrodlo = :szrodlo, int_id = :t_towarid, usluga = :t_tmp_towtype,
                    x_int_grupa_id = :t_grupaid, int_dataostprzetw = current_timestamp(0), int_sesjaostprzetw = :sesja
                  where ktm = :ktm;
            end
          end else begin
            insert into towary (ktm, kodprod, nazwa,
              grupa, akt, vat,
              x_opis,
              cena_zakn, cena_zakb,
              chodliwy, witryna, miara, int_zrodlo, int_id, usluga,
              int_dataostprzetw, int_sesjaostprzetw,
              x_int_grupa_id)
            values(:t_towarid, substring(:t_symbol from 1 for 40), :t_tmp_nazwa,
              :t_grupa,
              1,
              :t_vat,
              :t_opis,
              :t_cenazakupunet, :t_cenazakupubru,
              :t_chodliwy, :t_witryna, :jedn, :szrodlo, :t_towarid, :t_tmp_towtype,
              current_timestamp(0), :sesja,
              :t_grupaid);
            --returning ktm into :ktm; --KTM
              ktm = t_towarid;

          end
          /*
          update or insert into towary (ktm, kodprod, nazwa,
              grupa, akt, vat,
              x_opis,
              cena_zakn, cena_zakb,
              chodliwy, witryna, miara, int_zrodlo, int_id, usluga,
              int_dataostprzetw, int_sesjaostprzetw,
              x_int_grupa_id)
            values(:t_towarid, substring(:t_symbol from 1 for 40), :t_tmp_nazwa,
              :t_grupa,
              1,
              :t_vat,
              :t_opis,
              :t_cenazakupunet, :t_cenazakupubru,
              :t_chodliwy, :t_witryna, :jedn, :szrodlo, :t_towarid, :t_tmp_towtype,
              current_timestamp(0), :sesja,
              :t_grupaid)
            matching (ktm)
            returning ktm into :ktm; --KTM
             */
          select first 1 ref from wersje where ktm = :ktm into :wersjaref; --WERSJAREF

          --nullowanie, zerowanie i blankowanie - jednostka
          if (:j_glowna is null or j_glowna > 1) then
            j_glowna = 1;
          if (:j_licznik is null) then
            j_licznik = 1;
          if (:j_mianownik is null) then
            j_mianownik = 1;
          if (:j_waga is null) then
            j_waga = 0;
          if (:j_wysokosc is null) then
            j_wysokosc = 0;
          if (:j_dlugosc is null) then
            j_dlugosc = 0;
          if (:j_szerokosc is null) then
            j_szerokosc = 0;

          j_glowna = 1; --!!!
          j_glownasente = 1;

          --XXX JO: wymiary sa w milimetrach, zmiana na cm
          j_wysokosc = :j_wysokosc / 10.0000;
          j_dlugosc = :j_dlugosc / 10.0000;
          j_szerokosc = :j_szerokosc / 10.0000;
          j_waga = :j_waga / 1000.0000; --zmiana na kg
          --XXX JO: Koniec

          --nastepnie aktualizowana jest jednostka glowna, ktora zostala zalozona na triggerach
          select ref, glowna, przelicz, const, s, z, m, c, waga, wys , dlug, szer
            from towjedn
            where ktm = :ktm
              and jedn = :jedn
          into :towjedn, :o_glowna, :o_przelicz, :o_const, :o_s, :o_z, :o_m, :o_c, :o_waga, :o_wys , :o_dlug, :o_szer;

          if (towjedn is not null) then begin
            if (o_glowna is distinct from j_glowna or
                o_przelicz is distinct from j_tmp_przelicz or
                --o_const,
                --o_s,
                --o_z,
                --o_m,
                --o_c,
                o_waga is distinct from j_waga or
                o_wys is distinct from j_wysokosc or
                o_dlug is distinct from j_dlugosc or
                o_szer is distinct from j_szerokosc)
            then begin
                  update towjedn set glowna = :j_glowna, przelicz = :j_tmp_przelicz, const = 1,
                      s = 2, z = 2, m = 2, c = 2,
                      waga = :j_waga,
                      wys = :j_wysokosc, dlug = :j_dlugosc, szer = :j_szerokosc,
                      int_zrodlo = :szrodlo, int_id = :j_jednostka,
                      int_dataostprzetw = current_timestamp(0), int_sesjaostprzetw = :sesja
                    where ref = :towjedn;

            end
          end else begin
            insert into towjedn (ktm, jedn, glowna, przelicz, const,
                s, z, m, c,
                waga,
                wys, dlug, szer,
                int_zrodlo, int_id,
                int_dataostprzetw, int_sesjaostprzetw)
              values(:ktm, :jedn, :j_glowna, :j_tmp_przelicz, 1,
                2, 2, 2, 2,
                :j_waga,
                :j_wysokosc, :j_dlugosc, :j_szerokosc,
                :szrodlo, :j_jednostka,
                current_timestamp(0), :sesja)

              returning ref into :towjedn; --TOWJEDN

          end
          /*
          update or insert into towjedn (ktm, jedn, glowna, przelicz, const,
              s, z, m, c,
              waga,
              wys, dlug, szer,
              int_zrodlo, int_id,
              int_dataostprzetw, int_sesjaostprzetw)
            values(:ktm, :jedn, :j_glowna, :j_tmp_przelicz, 1,
              2, 2, 2, 2,
              :j_waga,
              :j_wysokosc, :j_dlugosc, :j_szerokosc,
              :szrodlo, :j_jednostka,
              current_timestamp(0), :sesja)
            matching(ktm, jedn)
            returning ref into :towjedn; --TOWJEDN
            */
            o_glowna = null; o_przelicz = null; o_const = null; o_s = null; o_z = null;
            o_m = null; o_c = null; o_waga = null; o_wys = null; o_dlug = null; o_szer = null;
            o_kodprod = null; o_nazwa = null; o_grupa = null; o_akt = null; o_vat = null; o_x_opis = null; o_cena_zakn = null; o_cena_zakb = null;
            o_chodliwy = null; o_witryna = null; o_miara = null; o_int_zrodlo = null; o_int_id = null; o_usluga = null; o_x_int_grupa_id = null;
        end

        if (:errormsg = '') then
          errormsg = null;
        if (:errormsg2 = '') then
          errormsg2 = null;

        select dataostprzetw, ktm  from int_imp_tow_towary where ref = :t_ref into :dataostprzetwtmp, :ktmtmp;
        if (ktm is not null and (dataostprzetwtmp is null  or ktmtmp is null)) then
          update int_imp_tow_towary set
            ktm = :ktm, wersjaref = :wersjaref, jedn = :jedn, towjedn = :towjedn,
            ref_gljed = :j_ref
            where ref = :t_ref; -- and (dataostprzetw is null or ktm is null)
             -- and :ktm is not null;
        if (coalesce(error,0) <> 0 or coalesce(errormsg,'') <> '') then
          update int_imp_tow_towary set error = :error, errormsg = :errormsg,
            dataostprzetw = current_timestamp(0)
            where ref = :t_ref;
        dataostprzetwtmp = null; ktmtmp = null;

        select dataostprzetw, glowna_sente, towjedn
          from int_imp_tow_jednostki
          where ref = :j_ref
        into :dataostprzetwtmp, :glowna_sentetmp, towjedntmp;
        if (towjedn is not null and(  dataostprzetwtmp is null or towjedntmp is null or glowna_sentetmp is distinct from :j_glownasente)) then
          update int_imp_tow_jednostki set
            ktm = :ktm, wersjaref = :wersjaref, jedn = :jedn, towjedn = :towjedn,
            ref_towar = :t_ref, glowna_sente = :j_glownasente
            where ref = :j_ref; -- and (dataostprzetw is null or towjedn is null or glowna_sente is distinct from :j_glownasente)
              --and :towjedn is not null;

        if (coalesce(error2,0) <> 0 or coalesce(errormsg2,'') <> '') then
          update int_imp_tow_jednostki set error = :error2, errormsg = :errormsg2,
            dataostprzetw = current_timestamp(0)
            where ref = :j_ref;








      dataostprzetwtmp = null; ktmtmp = null; glowna_sentetmp = null; towjedntmp = null;
      end

      --blokujzalezne = 1; --!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if ( :blokujzalezne = 0) then --tabele zalezne AFTER
      begin
        select status, msg from int_tabelezalezne_process(:sesja,
            :stabela, :ref, :szrodlo, :skierunek, 1)
          into :tmpstatus, :tmpmsg;
        if(coalesce(:tmpstatus,0) = -1 )then
        begin
          status = -1 ; --- XXX KBI
          error = 1;
          errormsg = substring(coalesce(errormsg,'') || ' ' || coalesce(:tmpmsg,'') from 1 for 255);
        end 
      end
    end
  end
  else if (stabela = 'INT_IMP_TOW_JEDNOSTKI') then
  begin
    execute procedure int_imp_tow_jednostki_process (:sesjaref, :tabela, :ref, :blokujzalezne, :tylkonieprzetworzone, :aktualizujsesje)
      returning_values :tmpstatus, :tmpmsg;
  end
  else if (stabela = 'INT_IMP_TOW_KODYKRESKOWE') then
  begin
    execute procedure int_imp_tow_kodkresk_process (:sesjaref, :tabela, :ref, :blokujzalezne, :tylkonieprzetworzone, :aktualizujsesje)





      returning_values :tmpstatus, :tmpmsg;
  end

  --przetworzenie tabel zaleznych
  if (blokujzalezne = 0) then
  begin
    tmptabela = null;
    tmpstatus = null;
    tmpmsg = null;

    sql = 'select zalezna from int_tabele where tabela = '''||:stabela||
      ''' and zrodlo = '||:szrodlo||' and kierunek = '||:skierunek||
      ' order by kolejnosc';

    for execute statement :sql into :tmptabela
    do begin
      tmpsql = 'select status, msg from INT_IMP_TOW_PROCESS('
        ||:sesja||', '''||:tmptabela||''', null, 1, 1, 0)';
      execute statement :tmpsql into :tmpstatus, :tmpmsg;
      if(coalesce(:tmpstatus,0) = -1 )then
      begin
        status = -1 ; --- XXX KBI
        error = 1;
        errormsg = substring(coalesce(errormsg,'') || ' ' || coalesce(:tmpmsg,'') from 1 for 255);
      end 
    end

    if (:tmptabela is null) then
    begin
      sql = 'select zalezna from int_tabele where tabela = '''||:stabela||
        ''' and zrodlo is null and kierunek = '||:skierunek||
        ' order by kolejnosc';

      for execute statement :sql into :tmptabela
        do begin
        tmpsql = 'select status, msg from INT_IMP_TOW_PROCESS('
            ||:sesja||', '''||:tmptabela||''', null, 1, 1, 0)';
         execute statement :tmpsql into :tmpstatus, :tmpmsg;
         if(coalesce(:tmpstatus,0) = -1 )then
          begin
            status = -1 ; --- XXX KBI
            error = 1;
            errormsg = substring(coalesce(errormsg,'') || ' ' || coalesce(:tmpmsg,'') from 1 for 255);
          end 
        end
    end

  end

  -- XXX KBI
  if (status < 1 and errormsg is not null) then
    msg = errormsg;
  -- XXX KBI
  --aktualizacja informacji o sesji
  if (:aktualizujsesje = 1) then
  begin
    select status, msg from int_sesje_aktualizuj(:sesja, 1) into :tmpstatus, :tmpmsg;
  end

  suspend;
end^
SET TERM ; ^
