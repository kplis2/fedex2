--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_PVAL(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      PARAM varchar(20) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable FROMDATE timestamp;
declare variable TODATE timestamp;
declare variable PERIOD smallint;
declare variable COMPANY integer;
begin
--MW: Personel - Pobieranie aktualnych parametrow placowych

  select company from employees
    where ref = :employee
    into :company;

  select ptype from wscfgdef
    where symbol = :param and company = :company
    into :period;

  if (period is null) then
    exception universal 'Brak definicji parametru o akronimie: ' || param;

  execute procedure efunc_prdates(payroll, period)
    returning_values fromdate, todate;

--Obsluga regulaminow wynagrodzen tylko dla okresow kosztowych (PR60011)
  if (period > 0) then
    employee = 0;

  execute procedure get_pval(:todate, :company, :param, :employee)
    returning_values ret;

  suspend;
end^
SET TERM ; ^
