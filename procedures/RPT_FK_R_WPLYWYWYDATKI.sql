--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_R_WPLYWYWYDATKI(
      DATAOD timestamp,
      DATADO timestamp,
      COMPANY integer)
  returns (
      REF integer,
      ACCOUNT ACCOUNT_ID,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      SETTLEMENT varchar(30) CHARACTER SET UTF8                           ,
      OPENDAY timestamp,
      PAYDATE timestamp,
      DESCRIPT varchar(100) CHARACTER SET UTF8                           ,
      KHKOD varchar(30) CHARACTER SET UTF8                           ,
      DEBIT numeric(15,2),
      CREDIT numeric(15,2),
      BDEBIT numeric(15,2),
      BCREDIT numeric(15,2),
      ISROZRACH smallint,
      CURRENCY varchar(3) CHARACTER SET UTF8                           ,
      PLNRDEBIT numeric(15,2),
      PLNRCREDIT numeric(15,2))
   as
declare variable old_symbol varchar(3);
  declare variable i_yearid integer;
  declare variable old_account ACCOUNT_ID;
  declare variable s_account ACCOUNT_ID;
  declare variable n_debit numeric(15,2);
  declare variable n_credit numeric(15,2);
  declare variable i_ref int;
  declare variable s_sep varchar(1);
  declare variable i_dictdef int;
  declare variable i_len smallint;
  declare variable s_tmp ACCOUNT_ID;
  declare variable old_prefix ACCOUNT_ID;
  declare variable i_i smallint;
  declare variable i_j smallint;
  declare variable i_k smallint;
  declare variable dictref integer;
  declare variable r_account integer;
  declare variable typ char(1);
  declare variable slodef integer;
  declare variable slopoz integer;
begin
  --TOFIX!!! pousuwa niepotrzebne deklaracje zmiennych - ogolnie wyczyscic
  ref = 0;
  bdebit = 0;
  bcredit = 0;
  --na poczatek licz stan przeterminowanych na date od daty
  --  - otwarte na dat z dat ptnoci przed ta data
  for
    select kontofk, symbfak
      from ROZRACH
      where dataotw < :dataod and dataplat < :dataod
        and ((datazamk is null) or (datazamk >= :dataod))
        and rozrach.company = :company
      into :s_account, :settlement
  do begin
    debit = null;
    credit = null;
    select sum(roz.winienzl), sum(roz.mazl)
      from rozrachp roz where roz.kontofk = :s_account and roz.symbfak = :settlement
        and roz.data <:dataod and roz.company = :company
      into :debit, :credit;
    if (debit is null) then debit = 0;
    if (credit is null) then credit = 0;
    if (debit > credit) then
      bdebit = bdebit + (debit - credit);
    else
      bcredit = bcredit + (credit - debit);
  end

  -- przegladanie rozrachunkow otwartych w tym okresie, obliczanie ich stanu
  -- na poczatek i relizacja w dacie platnosci
  for
    select kontofk, symbfak, dataotw, dataplat, typ, slodef, slopoz, rozrach.waluta
      from ROZRACH
      where ((datazamk is null) or (datazamk >= :dataod)) and dataotw <= :datado
        and dataplat >= :dataod and dataplat <= :datado
        and rozrach.company = :company
      order by dataplat
      into :s_account, :settlement, :openday, :paydate, :typ, :slodef, :slopoz, currency
  do begin
    -- okreslenie stanu na dzien poczatkowy - nie moze byc rowny,
    -- bo inaczej bylby zamkniety w okresie
    debit = null;
    credit = null;

    select sum(rp.winien), sum(rp.ma), sum(rp.winienzl), sum(rp.mazl)
      from rozrachp rp where kontofk = :s_account and symbfak = :settlement
        and rp.company = :company
    into :debit, :credit, :plnrdebit, :plnrcredit ;

    if (debit is null) then
    begin
      debit = 0;
      plnrdebit = 0;
    end
    if (credit is null) then
    begin
      credit = 0;
      plnrcredit = 0;
    end
    if (debit > credit) then
    begin
      debit = debit - credit;
      plnrdebit = plnrdebit - plnrcredit;
      credit = 0;
      plnrcredit = 0;
    end else
    begin
      credit = credit - debit;
      plnrcredit = plnrcredit - plnrdebit;
      debit = 0;
      plnrdebit = 0;
    end
    execute procedure name_from_dictposref(slodef, slopoz) returning_values descript;
    execute procedure kodks_from_dictpos(slodef, slopoz) returning_values khkod;
    ref = ref + 1;
    if (debit <>0 or credit <> 0) then
      suspend;
  end
end^
SET TERM ; ^
