--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZZAM_ZLECPROD(
      REF integer)
  returns (
      ID varchar(30) CHARACTER SET UTF8                           )
   as
declare variable nagzamref integer;
begin
  for select n.ref, n.id from nagzam n
   where n.kpopref = :REF
   into :nagzamref, :id
  do begin
    suspend;
    for select p.ref from pozzam p
     where p.zamowienie = :nagzamref
     into :ref do execute procedure pozzam_zlecprod(:ref) returning_values :nagzamref;
  end
end^
SET TERM ; ^
