--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SET_GLOBAL_PARAM(
      PNAME varchar(30) CHARACTER SET UTF8                           ,
      PVALUE varchar(255) CHARACTER SET UTF8                           ,
      ID integer = null,
      NEOSID NEOS_ID = null)
   as
declare variable sid varchar(40);
begin
  --1. pobieranie informacji o connectionie oraz
  --w przypadku neosa o aliasie workingcontextu
  sid = null;
  if(:id is NULL or :id=0) then begin
    id = current_connection;
    -- sprawdz czy nie ustawic zmiennej jeszcze w watku serwera
    if(:pname<>'CONNECTIONID' and pname<>'NEOSID') then begin
      -- przeczytaj id working contextu ze zmiennych kontekstowych
      select rdb$get_context('USER_SESSION','CONNECTIONID') from rdb$database into :sid;
      if(:sid is null) then begin
        -- a jesli sie nie udalo, to po staremu z globalparamsow
        select first 1 pvalue from globalparams
        where connectionid=current_connection and psymbol='CONNECTIONID'
        into :sid;
      end
    end
  end

  if(neosid is null or neosid='') then begin
    -- przeczytaj neosid ze zmiennych kontekstowych
    select rdb$get_context('USER_SESSION','NEOSID') from rdb$database into :neosid;
  end
  if(neosid is null or neosid='') then begin
    -- a jesli sie nie udalo, to po staremu z globalparamsow
    select first 1 pvalue from globalparams
      where connectionid=:id and psymbol='NEOSID'
      into :neosid;
  end

  --2.czyszczenie starej wartosci zmiennej, gdy tresc jest pusta
  if(pvalue='') then
  begin
    delete from globalparams
      where connectionid=:id and psymbol=:pname;
    if(:sid is not null) then
    begin
      if(:neosid is not null) then
      begin
        delete from globalparams where connectionid=:sid and neosid=:neosid and psymbol=:pname;
      end
      else
      begin
        /*obsluga dotychczasowego trybu z podawaniem tylko aliasu na connection
          troche na wyrost, bo w koncu poprawka wyjdzie z nowa wersja serwera
          ktory bedzie podawal juz alias na connection i na neosid
        */
        delete from globalparams where connectionid=:sid and psymbol=:pname;
      end
    end
  end

  --3.ustawianie nowej wartosci zmiennej
  if(:pvalue<>'') then
  begin
    if(neosid is not null) then
    begin
      update or insert into globalparams (PSYMBOL, PVALUE, CONNECTIONID, NEOSID)
        values (:pname, :pvalue, :id, :neosid) matching (CONNECTIONID, PSYMBOL, NEOSID);
    end
    else
    begin
      update or insert into globalparams (PSYMBOL, PVALUE, CONNECTIONID)
        values (:pname, :pvalue, :id) matching (CONNECTIONID, PSYMBOL);
    end

    if(:sid is not null) then begin
      if(:neosid is not null) then
      begin
        update or insert into globalparams (PSYMBOL, PVALUE, CONNECTIONID, NEOSID)
          values (:pname, :pvalue, :sid, :neosid) matching (CONNECTIONID, PSYMBOL, NEOSID);
      end else
      begin
        /*obsluga dotychczasowego trybu z podawaniem tylko aliasu na connection
          troche na wyrost, bo w koncu poprawka wyjdzie z nowa wersja serwera
          ktory bedzie podawal juz alias na connection i na neosid
        */
        update or insert into globalparams (PSYMBOL, PVALUE, CONNECTIONID)
          values (:pname, :pvalue, :sid) matching (CONNECTIONID, PSYMBOL);
      end
    end
  end
end^
SET TERM ; ^
