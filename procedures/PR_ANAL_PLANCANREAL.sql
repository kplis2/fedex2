--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_ANAL_PLANCANREAL(
      DATEFROM timestamp,
      DATETO timestamp,
      PLANN integer,
      PRDEPART varchar(80) CHARACTER SET UTF8                           )
  returns (
      SYMBOL varchar(255) CHARACTER SET UTF8                           ,
      PLANVAL numeric(15,4),
      PLACETIME numeric(14,2),
      TYP smallint)
   as
begin
  if(:PLANN = 0) then plann = null;
  if(:prdepart = '') then prdepart = null;
--narzedzia
  typ = 0;
  for select max(pt.symbol), sum(po.opertime * pv.planval * pt.quantity) as czas--, sum( pr_anal_plancanreal_ktm.planval) as ilosc
    from prshtools pt
    join prshopers po on (pt.opermaster = po.ref)
    join prsheets ps on (ps.ref = po.sheet)
    join plansrow pr on (pr.key1 = ps.ktm)
    join planscol pc on (pc.plans=pr.plans and pc.akt=1)
    join plansval pv on (pv.planscol=pc.ref and pv.plansrow=pr.ref)
    join plans p on (p.ref = pr.plans)
    where (p.ref = :plann or :plann is null) and (p.prdepart = :prdepart or :prdepart is null)
      and pc.akt = 1 and p.PLANSTYPE like 'PR%%' and  po.complex = 0
      and PC.LDATA <= :dateto and PC.BDATA >= : datefrom
    group by pt.symbol
    into :symbol, :placetime --, :planval
  do begin
    if (:placetime is not null) then  suspend;
  end
--stanowiska
  typ = 1;
  for select max(pw.descript),sum(po.placetime * pv.planval) as czas--, sum( pr_anal_plancanreal_ktm.planval) as ilosc
    from prworkplaces pw
    join prshopers po on (pw.symbol = po.workplace)
    join prsheets ps on (ps.ref = po.sheet)
    join plansrow pr on (pr.key1 = ps.ktm)
    join planscol pc on (pc.plans=pr.plans and pc.akt=1)
    join plansval pv on (pv.planscol=pc.ref and pv.plansrow=pr.ref)
    join plans p on (p.ref = pr.plans)
    where (p.ref = :plann or :plann is null) and (p.prdepart = :prdepart or :prdepart is null)
      and pc.akt = 1 and p.PLANSTYPE like 'PR%%' and  po.complex = 0
      and PC.LDATA <= :dateto and PC.BDATA >= : datefrom
    group by pw.symbol
    into :symbol, :placetime --, :planval
  do begin
    if (:placetime is not null) then suspend;
  end
--roboczogodziny
  typ = 2;
  for select 'Roboczogodziny',sum(po.placetime * pv.planval) as czas--, sum( pr_anal_plancanreal_ktm.planval) as ilosc
    from prshopers po
    join prsheets ps on (ps.ref = po.sheet)
    join plansrow pr on (pr.key1 = ps.ktm)
    join planscol pc on (pc.plans=pr.plans and pc.akt=1)
    join plansval pv on (pv.planscol=pc.ref and pv.plansrow=pr.ref)
    join plans p on (p.ref = pr.plans)
    where (p.ref = :plann or :plann is null) and (p.prdepart = :prdepart or :prdepart is null)
      and pc.akt = 1 and p.PLANSTYPE like 'PR%%' and  po.complex = 0
      and PC.LDATA <= :dateto and PC.BDATA >= : datefrom
    into :symbol, :placetime --, :planval
  do begin
    if (:placetime is not null) then suspend;
  end
end^
SET TERM ; ^
