--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CRMTRBOOKP_ADD(
      ODDZIAL varchar(20) CHARACTER SET UTF8                           ,
      TYP smallint,
      TBDATE timestamp,
      CONTRACTORNAME varchar(255) CHARACTER SET UTF8                           ,
      CONTRACTORADDRESS varchar(1024) CHARACTER SET UTF8                           ,
      TBVALUE numeric(14,2),
      TBWEIGHT numeric(14,3),
      PAYMENT numeric(14,2),
      COMMENTS varchar(1024) CHARACTER SET UTF8                           ,
      SHIPMENTNUMBER varchar(80) CHARACTER SET UTF8                           ,
      CHARGE numeric(14,2),
      CREATEHEADER smallint,
      CRMTRBOOKPREFIN integer)
  returns (
      CRMTRBOOKPREF integer)
   as
begin/*
PL(PATCH): Procedura wykomentowana, bo ksiązki nadawczo-odbiorczej nie ma na release, a wycofywanie z patcha byłoby bardziej kłopotliwe.
declare variable CRMTRBOOKH integer;
begin
--exception test_break 'CRMTRBOOKPREFIN='||coalesce(:CRMTRBOOKPREFIN, 'null');
--ZG68001 pgawlak - dodanie castowania na date, poniewaz naglowek ksiazki nadawczo odbiorczej nie powinien zawierac godzin w dacie od do
  if(CRMTRBOOKPREFIN is null) then begin
    select h.ref from  CRMTRBOOKH h where h.typ = :typ and cast(:TBDATE as date) >= h.fromdate and cast(:tbdate as date) <= h.todate into :crmtrbookh;

    if(crmtrbookh is null and coalesce(createheader,0) = 1) then begin
      execute procedure gen_ref('CRMTRBOOKH') returning_values :crmtrbookh;
      INSERT INTO CRMTRBOOKH (REF, ODDZIAL, TYP, FROMDATE, TODATE, DESCRIPT)
                  VALUES (:crmtrbookh, :oddzial, :typ, cast(:TBDATE as date), cast(:TBDATE as date), '');
    end
  
    if(crmtrbookh is null) then exception universal 'Nie można ustalić nagłówka książki odbiorczo-nadawczej!';
  
    execute procedure gen_ref('CRMTRBOOKP') returning_values :CRMTRBOOKPREF;
    INSERT INTO CRMTRBOOKP (REF, TBDATE, CONTRACTORNAME, CONTRACTORADDRESS, TBVALUE, TBWEIGHT, PAYMENT, COMMENTS,
                              SHIPMENTNUMBER, CHARGE, CRMTRBOOKH)
      VALUES (:CRMTRBOOKPREF, :TBDATE, :CONTRACTORNAME, :CONTRACTORADDRESS, :TBVALUE, :TBWEIGHT, :PAYMENT, :COMMENTS,
              :SHIPMENTNUMBER, :CHARGE, :CRMTRBOOKH);
  end else begin
    update CRMTRBOOKP set
    TBDATE = :TBDATE, CONTRACTORNAME = :CONTRACTORNAME, CONTRACTORADDRESS = :CONTRACTORADDRESS, TBVALUE = :TBVALUE,
    TBWEIGHT = :TBWEIGHT, PAYMENT = :PAYMENT, COMMENTS = :COMMENTS,
                            SHIPMENTNUMBER = :SHIPMENTNUMBER, CHARGE = :CHARGE
    where REF = :CRMTRBOOKPREFIN;
    CRMTRBOOKPREF = :CRMTRBOOKPREFIN;
  end
  suspend;
end*/
end^
SET TERM ; ^
