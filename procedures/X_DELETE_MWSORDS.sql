--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_DELETE_MWSORDS(
      TYP INTEGER_ID)
  returns (
      MWSORD MWSORDS_ID)
   as
begin
  --mkd
  --exception universal 'Wiesz co robisz???!!!';
  --in autonomous transaction do
  for
    select ref from mwsords o
      where o.mwsordtype not in (10,11,12,13)--bez in+ i in- bi- bi+ rli LI
      --and o.mwsordtype = :typ
      into mwsord do
  begin
      suspend;
      in autonomous transaction do begin
        execute procedure set_global_param('AKTUOPERATOR','74');
      update mwsords o set o.status = 2 where o.status = 5 and ref = :mwsord;
      update mwsacts a set a.status = 2 where a.status = 5 and a.mwsord = :mwsord;
      update mwsacts a set a.status = 1 where a.status = 2 and a.mwsord = :mwsord;
      update mwsords o set o.status = 1 where o.status = 2 and ref = :mwsord;
      update mwsords o set o.status = 0 where o.ref = :mwsord;
      delete from mwsords where ref = :mwsord;
      when any do
      begin
      end
      end
  end
end^
SET TERM ; ^
