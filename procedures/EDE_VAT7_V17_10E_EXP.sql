--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_VAT7_V17_10E_EXP(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(8191) CHARACTER SET UTF8                           )
   as
--zmianna pomocnicza
declare variable isdeclaration integer;
declare variable def varchar(20);
declare variable edeclddef integer;
declare variable code varchar(10);
declare variable pfield varchar(10);
declare variable systemcode varchar(20);
declare variable obligationkind varchar(20);
declare variable schemaver varchar(10);
declare variable symbol varchar(10);
declare variable variant varchar(10);
declare variable pvalue varchar(255);
declare variable tmpval1 varchar(255);
declare variable tmpval2 varchar(255);
declare variable tmp smallint;
declare variable curr_p integer;
declare variable correction smallint;
declare variable enclosureowner integer;
declare variable zaltyp varchar(20);
declare variable iszal smallint;
declare variable parent_lvl_0 integer;
declare variable parent_lvl_1 integer;
declare variable parent_lvl_2 integer;
declare variable parent_lvl_3 integer;
begin
  --Sprawdzamy czy dana deklaracja istnieje do eksportu
  select first 1 1
    from edeclarations e
    where e.ref = :oref
  into :isDeclaration;

  if(isDeclaration is null) then
    exception universal'Niepoprawny identyfikator e-deklaracji';

  --Pobieranie definicji
  select e.edecldef, ed.code, ed.systemcode, ed.obligationkind, ed.schemaver, ed.symbol, ed.variant, ed.ref
    from edeclarations e
      join edecldefs ed on (ed.ref = e.edecldef)
    where e.ref = :oref
  into :def, :code, :systemcode, :obligationkind, :schemaver, :symbol, :variant, :edeclddef;

  --Generujemy naglowek pliku XML
  id = 0;
  name = 'Deklaracja';
  parent = null;
  params = 'xmlns="http://crd.gov.pl/wzor/2016/08/05/3412/" xmlns:etd="http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/"';
  val = null;
  suspend;

  correction = 0;
  parent_lvl_0 = 0;
  --Rozpoczynamy naglowek e-deklaracji
  id = :id + 1; --1
  name = 'Naglowek';
  parent = parent_lvl_0;
  params = null;
  val  = null;
  parent_lvl_1 = :id;
  suspend;
  --cialo naglowka

    --KodFormularza
    /* <KodFormularza
         kodSystemowy= "VAT-7 (17)"
         kodPodatku= "VAT"
         rodzajZobowiazania= "Z"
         wersjaSchemy= "1-0E">
           VAT-7
       </KodFormularza>
    */
    id = id+1;
    name ='KodFormularza';
    parent = parent_lvl_1;
    params ='';
    if(code is not null) then
    params = params || 'kodPodatku="' || code || '" ';
    if(systemcode is not null) then
    params = params || 'kodSystemowy="' || systemcode || '" ';
    if(obligationkind is not null) then
    params = params || 'rodzajZobowiazania="' || obligationkind || '" ';
    if(schemaver is not null) then
    params = params || 'wersjaSchemy="' || schemaver || '"';
    val = symbol;
    suspend;
    
    --WariantFormularza
    /* <WariantFormularza>
         2
       </WariantFormularza>
    */
    id = id+1;
    name= 'WariantFormularza';
    parent = parent_lvl_1;
    params = null;
    val = variant;
    suspend;
    
    --CelZlozenia
    /* <CelZlozenia
         poz= "P_7">
           1
       </CelZlozenia>
    */
    id = id+1;
    name= 'CelZlozenia';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.fieldsymbol = 'K7'
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params = 'poz= "P_7"';
      val = :pvalue;
      suspend;
    end

    if(:val = 2) then
      correction = 1;
    pvalue = null;

    --Rok
    /* <Rok>
         2017
       </Rok>
    */
    id = id+1;
    name= 'Rok';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.fieldsymbol = 'K5'
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params = null;
      val = :pvalue;
      suspend;
    end
    pvalue = null;

    --Miesiac
    /* <Miesiac>
         2
       </Miesiac>
    */
    id = id+1;
    name= 'Miesiac';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.fieldsymbol = 'K4'
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params = null;
      val = :pvalue;
      suspend;
    end
    pvalue = null;

    --KodUrzedu
    /* <KodUrzedu>
         0402
       </KodUrzedu>
    */
    id = id+1;
    name= 'KodUrzedu';
    select epos.pvalue
      from edeclpos epos
      where epos.edeclaration = :oref and epos.fieldsymbol = 'USP'
    into :pvalue;
    if (:pvalue is not null) then
    begin
      parent = parent_lvl_1;
      params = null;
      val = :pvalue;
      suspend;
    end
    --Koniec Naglowka
    
  --Podmiot
  /* <Podmiot1 rola= "Podatnik">
       <!--You have a CHOICE of the next 2 items at this level-->
       <etd:OsobaFizyczna>
         ...
       </etd:OsobaFizyczna>
       <etd:OsobaNiefizyczna>
         ...
       </etd:OsobaNiefizyczna>
     </Podmiot1>
  */
  id = id+1;
  name= 'Podmiot1';
  parent = parent_lvl_0;
  params = 'rola= "Podatnik"';
  val = null;
  parent_lvl_1 = :id;
  suspend;
    pvalue = null;
    select epos.pvalue
        from edeclpos epos
        where epos.edeclaration = :oref and epos.fieldsymbol = 'K8'
      into :pvalue;
  if(:pvalue = 2) then
  begin
    --OsobaFizyczna
    /* <etd:OsobaFizyczna>
         <etd:NIP>string</etd:NIP>
         <etd:ImiePierwsze>token</etd:ImiePierwsze>
         <etd:Nazwisko>token</etd:Nazwisko>
         <etd:DataUrodzenia>1900-01-01+01:00</etd:DataUrodzenia>
       </etd:OsobaFizyczna>
    */
    id = id+1;
    name= 'etd:OsobaFizyczna';
    parent = parent_lvl_1;
    params = null;
    val = null;
    parent_lvl_2 = :id;
    suspend;
      pvalue = null;
      --NIP
      id = id+1;
      name= 'etd:NIP';
      parent = parent_lvl_2;
      select REPLACE(epos.pvalue, '-', '')
        from edeclpos epos
        where epos.edeclaration = :oref and epos.fieldsymbol = 'K1'
      into :pvalue;
      params = null;
      val = :pvalue;
      suspend;
      pvalue = null;
      --ImiePierwsze && Nazwisko && DataUrodzenia
      tmp = 0;
      select epos.pvalue
      from edeclpos epos
        where epos.edeclaration = :oref and epos.fieldsymbol = 'K9'
      into :pvalue;
      for select outstring
        from parse_lines(:pvalue, ',')
      into :tmpval2
      do begin
      if(tmp = 0) then
      begin
        tmpval1 = :tmpval2;
      end
      if(tmp = 1) then
      begin
      --PelnaNazwa
        id = id+1;
        name= 'etd:ImiePierwsze';
        parent = parent_lvl_2;
        val = tmpval2;
        params = null;
        suspend;

        --Nazwisko
        id = id+1;
        name= 'etd:Nazwisko';
        parent = parent_lvl_2;
        val = tmpval1;
        params = null;
        suspend;
      end
      if(tmp = 2) then
      begin
        --DataUrodzenia
        id = id+1;
        name= 'etd:DataUrodzenia';
        parent = parent_lvl_2;
        val = tmpval2;
        params = null;
        suspend;
      end
      tmp = :tmp + 1;
      end
    end
  else if(:pvalue = 1) then
    begin
    --OsobaNiefizyczna
    /* <etd:OsobaNiefizyczna>
         <etd:NIP>string</etd:NIP>
         <etd:PelnaNazwa>token</etd:PelnaNazwa>
         <!--Optional:-->
         <etd:REGON>string</etd:REGON>
       </etd:OsobaNiefizyczna>
    */
    id = id+1;
    name= 'etd:OsobaNiefizyczna';
    parent = parent_lvl_1;
    params = null;
    val = null;
    parent_lvl_2 = :id;
    suspend;
      pvalue = null;
      --NIP
      id = id+1;
      name= 'etd:NIP';
      parent = parent_lvl_2;
      select REPLACE(epos.pvalue, '-', '')
        from edeclpos epos
        where epos.edeclaration = :oref and epos.fieldsymbol = 'K1'
      into :pvalue;
      params = null;
      val = :pvalue;
      suspend;
      pvalue = null;
      --PelnaNazwa && REGON
      tmp = 0;
      select epos.pvalue
      from edeclpos epos
        where epos.edeclaration = :oref and epos.fieldsymbol = 'K9'
      into :pvalue;
      for select outstring
        from parse_lines(:pvalue, ',')
      into :tmpval2
      do begin
      if(tmp = 0)then
      begin
        --PelnaNazwa
        id = id+1;
        name= 'etd:PelnaNazwa';
        parent = parent_lvl_2;
        val = tmpval2;
        params = null;
        suspend;
      end
      else if(tmp = 1) then
      begin
        --REGON
        id = id+1;
        name= 'etd:REGON';
        parent = parent_lvl_2;
        val = trim(tmpval2);  -- zbedne spacje
        params = null;
        suspend;
      end
      tmp = :tmp + 1;
    end
  end

    --PozycjeSzczegolowe
    id = id+1;
    name='PozycjeSzczegolowe';
    parent = parent_lvl_0;
    params =null;
    val = null;
    parent_lvl_1 = :id;
    suspend;
    pvalue = null;
      -- algorytm uzupelniajacy pozycje
      -- od 10 do  74
      -- dodanie pol obowiazkowych

      --rdb$set_context('USER_TRANSACTION', 'EDE_PROCESS_RESPOND_Run', '0');
      update edeclarations e set e.state = 0 where e.ref = :oref;
      for select epos.pvalue, epos.field
        from edeclpos epos
        where epos.edeclaration = :oref
          and ((epos.field >= 10 and epos.field <= 69)
          or epos.field in (73, 74))
        order by epos.field
        into :pvalue, :pfield
      do begin
        if( (:pvalue is not null and ((:pfield between 10 and 61 and :pvalue<>0) or (:pfield in (41,53,54,57)) )) ) then -- (41,53,54,57) - pola obowiazkowe
        begin
          id = id+1;
          name='P_'||:pfield;
          parent = parent_lvl_1;
          params =null;
          val = :pvalue;
          suspend;
        end
        if (:pvalue is not null and ((:pfield between 62 and 65 and :pvalue<>0) or (:pfield = 73 and :pvalue != '')  )) then
        begin
          id = id+1;
          name='P_'||:pfield;
          parent = parent_lvl_1;
          params =null;
          val = upper(:pvalue);
          suspend;  
        end

        if(:pvalue is not null and :pfield between 66 and 68) then
        begin
          id = id+1;
          name='P_'||:pfield;
          parent = parent_lvl_1;
          params =null;
          if(:pvalue = 0) then val = 2;
            else val = upper(:pvalue);
          suspend;
        end
        if(:pfield = 69 and :pvalue > 0) then
        begin
          id = id+1;
          name='P_'||:pfield;
          parent = parent_lvl_1;
          params =null;
          val = :pvalue;
          suspend;
        end
        if(:pfield = 74 and :pvalue is not null) then -- Data wypelnienia: dd-mm-rrrr
        begin
          id = id+1;
          name='P_'||:pfield;
          parent = parent_lvl_1;
          params =null;
          val = extract(year from cast(pvalue as date))/* substring(:pvalue from 7 for 4)*/ || '-' ||
            trim(iif(extract(month from cast(pvalue as date)) < 10, '0',''))||extract(month from cast(pvalue as date)) /* substring(:pvalue from 4 for 2)*/ || '-' ||
            trim(iif(extract(day from cast(pvalue as date)) < 10, '0',''))||extract(day from cast(pvalue as date)) /*substring(:pvalue from 1 for 2); */   ;  --BS101141
          suspend;
        end
      end

    --Pouczenie
    id = id+1;
    name='Pouczenia';
    parent = parent_lvl_0;
    params =null;
    val = '1'; --poważnie - 1, tak ma być
    suspend;

end^
SET TERM ; ^
