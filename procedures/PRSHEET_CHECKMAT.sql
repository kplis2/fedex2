--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHEET_CHECKMAT(
      REFOPER integer)
  returns (
      OPMSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable nr smallint;
declare variable ktm varchar(40);
declare variable wersjaref integer;
declare variable jedn integer;
declare variable mag varchar(3);
declare variable ilosc varchar(255); --egrossquantity
begin
  OPMSG = '';
  select m.number, m.ktm, m.wersjaref, m.jedn, m.warehouse, m.egrossquantity
    from prshmat m
    where m.ref = :refoper
    into :nr, :ktm, :wersjaref, :jedn, :mag, :ilosc;
  if (coalesce(:nr,0)=0) then opmsg = :opmsg || 'Numer;';
  if (coalesce(:ktm,'')='') then opmsg = :opmsg || 'KTM;';
  if (coalesce(:wersjaref,0)=0) then opmsg = :opmsg || 'Wersja;';
  if (coalesce(:jedn,0) = 0) then
    opmsg = :opmsg || 'Jedn;';
  else if (not exists (select 1 from towjedn j where j.ref = :jedn and j.ktm = :ktm)) then
    opmsg = :opmsg || 'blad jednostki magazynowej;';
  if (coalesce(:mag,'')='') then opmsg = :opmsg || 'Magazyn;';
  if (coalesce(:ilosc,'')='') then opmsg = :opmsg || 'IloscBrutto;';
  if (coalesce(char_length(:opmsg),0) > 0) then -- [DG] XXX ZG119346
    opmsg = 'W materiale nr ' || :nr || ' brak nast. danych:' || :opmsg;
  suspend;
end^
SET TERM ; ^
