--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_GET_LISTAWYS(
      SHIPPINGDOC integer)
  returns (
      TRANSPORTORDER integer)
   as
declare variable shippingtype integer;
declare variable shippersymb spedytor_id;
declare variable actoper integer;
begin
  select d.listawys, d.sposdost
    from listywysd d
    where d.ref = :shippingdoc
  into :transportorder, :shippingtype;
  if (:transportorder is not null) then exit;

  select s.spedytor
    from spedytwys s
    where s.sposdost = :shippingtype
  into :shippersymb;

  select first 1 l.ref
    from listywys l
    where l.spedytor = :shippersymb
      and l.stan = 'O'
  into :transportorder;
  if (:transportorder is not null) then exit;

  execute procedure get_global_param('AKTUOPERATOR') returning_values :actoper;
  execute procedure gen_ref('LISTYWYS') returning_values :transportorder;

  insert into listywys(ref,spedytor,trasa,kierowca,dataotw,dataplwys,typ,stan,
      opis,operator,uwagizlec)
    values(:transportorder,:shippersymb,null,null,current_date,current_date,1,'O',
      '',:actoper,'');
  suspend;
end^
SET TERM ; ^
