--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BKDOCS_CHECK_VAT(
      BKDOC integer)
   as
declare variable vat1 numeric(14,2);
declare variable vat2 numeric(14,2);
declare variable checkvat varchar(255);
declare variable diff numeric(14,2);
declare variable doctype smallint;
declare variable vatcorrection smallint;
begin
  execute procedure get_config('BKDOC_CHECK_VATSUM', 0)
    returning_values :checkvat;
  if (checkvat <> '') then
  begin
    checkvat = replace(checkvat, ',', '.');
    diff = cast(checkvat as numeric(14,2));
    select sum(P.netv * V.stawka / 100), sum(P.vate)
      from bkvatpos P
        join vat V on (P.vatgr = V.grupa)
      where bkdoc = :bkdoc
      into :vat1, :vat2;
    select first 1 1
      from bkdocs b
      where b.ref = :bkdoc and b.bkdocsvatcorrectionref is not null
      into :vatcorrection;
    vatcorrection = coalesce(vatcorrection, 0);
    select bkdoctypes.kind from bkdocs join bkdoctypes on (bkdoctypes.ref = bkdocs.doctype)
          where bkdocs.ref = :bkdoc  into :doctype;
    -- jesli nie jest to SAD (bkdoctypes.kind = 3  to jest SAD)
    -- jesli nie jest to korekta VAT (bkdocs.bkdocsvatcorrectionref is not null)
    if (doctype <> 3 and vatcorrection = 0) then
      if (abs(vat2 - vat1) > diff ) then
        exception BKDOC_VAT_EXPT 'Niezgodność VATu w dokumencie na kwotę:  ' || (vat2-vat1);
  end
end^
SET TERM ; ^
