--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PRSHEETSOPER(
      SHEET integer,
      OPER integer,
      LICZNIK integer,
      POZIOM integer,
      ZLOZONA integer,
      R1 bigint,
      R2 integer,
      R3 integer,
      R4 integer,
      R5 integer)
  returns (
      OSHEET integer,
      OOPER integer,
      OLICZNIK integer,
      OPOZIOM integer,
      ONUMER integer,
      TYP varchar(1) CHARACTER SET UTF8                           ,
      P0 varchar(80) CHARACTER SET UTF8                           ,
      P1 varchar(80) CHARACTER SET UTF8                           ,
      P2 varchar(80) CHARACTER SET UTF8                           ,
      P3 varchar(80) CHARACTER SET UTF8                           ,
      P4 varchar(80) CHARACTER SET UTF8                           ,
      P5 varchar(80) CHARACTER SET UTF8                           ,
      P6 varchar(80) CHARACTER SET UTF8                           ,
      P7 varchar(80) CHARACTER SET UTF8                           ,
      REF integer,
      OZLOZONA integer,
      OOSTATNIAWGRUPIE integer,
      OR1 bigint,
      OR2 integer,
      OR3 integer,
      OR4 integer,
      OR5 integer)
   as
DECLARE VARIABLE COMPLEX INTEGER;
DECLARE VARIABLE NUMER1 INTEGER;
DECLARE VARIABLE NUMER2 INTEGER;
DECLARE VARIABLE NAZWA1 VARCHAR(80);
DECLARE VARIABLE NAZWA2 VARCHAR(80);
DECLARE VARIABLE OPER1 INTEGER;
DECLARE VARIABLE OPER2 INTEGER;
DECLARE VARIABLE O1 INTEGER;
DECLARE VARIABLE O2 INTEGER;
begin
  or1 = r1;
  or2 = r2;
  or3 = r3;
  or4 = r4;
  or5 = r5;
  oostatniawgrupie = 0;
--  if (llevel=-1) then ollevel=-1;
  if (oper is null) then poziom=0;
  opoziom = poziom;
--  ollevel = llevel;
  --llevel = ollevel;
   if (opoziom =0 ) then begin or1=0; or2=0; or3=0; or4=0; or5=0; end
    else if (opoziom =1) then begin or2=0; or3=0; or4=0; or5=0; end
    else if (opoziom =2) then begin or3=0; or4=0; or5=0; end
    else if (opoziom =3) then begin or4=0; or5=0; end
  complex=0;
  osheet = sheet;
  select count(*) from prshopers pro
      where pro.sheet = :SHEET and (pro.opermaster = :oper or :OPER is null)
      into :numer1;
  if (licznik=-1) then licznik =: numer1;
  OLICZNIK=LICZNIK;
 if (oper is null ) then begin
 --
   select  count(*)
      from prshopers pro
      where pro.sheet = :SHEET and (pro.opermaster is null)
      into :oper1;
   o1 =0;
   for
    select  pro.complex, pro.ref, pro.descript, pro.workplace, pro.ref, pro.number,
            pro.placetime,  pro.opertime, pro.operfactor,'', ''
      from prshopers pro
      where pro.sheet = :SHEET and (pro.opermaster is null)
      order by pro.number
      into :complex, :OOPER, :P1, :P2, :ref, :onumer,
           :P3,  :P4, :P5, :P6, :P7
  do begin
    o1 = o1 + 1;
    if (oper1=o1) then oostatniawgrupie = 1;
    else oostatniawgrupie =0;
    ozlozona = complex;
    if (opoziom=0) then OR1=oostatniawgrupie ;
    if (opoziom=1) then OR2=oostatniawgrupie ;
    if (opoziom=2) then OR3=oostatniawgrupie ;
    if (opoziom=3) then OR4=oostatniawgrupie ;
    if (opoziom=4) then OR5=oostatniawgrupie ;
  --  if (:OPER is not null ) then p7=:REF;
--    p6=:complex;
    suspend;
    OLICZNIK=:OLICZNIK -1;
       if (complex = 1) then
      begin
        --OLLEVEL = POZIOM;
        poziom=poziom+1;
        for
          select OOPER, OLICZNIK, TYP, P0, P1, P2, P3, P4, P5, P6, P7, REF, OPOZIOM, ONUMER, OZLOZONA, OOSTATNIAWGRUPIE, OR1, OR2, OR3, OR4, OR5
            from RPT_PRSHEETSOPER(:OSHEET, :OOPER, :OLICZNIK,:POZIOM,0,:OR1+:OR1,:OR2+:OR2,:OR3+:OR3,:OR4+:OR4,:OR5+:OR5)
            into :OOPER, :LICZNIK, :TYP, :P0, :P1, :P2, :P3, :P4, :P5, :P6, :P7, :REF, :OPOZIOM, :ONUMER, :OZLOZONA, :OOSTATNIAWGRUPIE, :OR1, :OR2, :OR3, :OR4, :OR5
        do begin
          suspend;
          OLICZNIK= :OLICZNIK-1;
        end

        poziom=poziom-1;
        opoziom=poziom;
      end
    if (OLICZNIK =0) then
    begin
      exit;
      exception RPT_KONIEC 'koniec';
    end
  end
 end else begin
   select  count(*)
      from prshopers pro
      where pro.sheet = :SHEET and (pro.opermaster = :oper)
      into :oper2;
    o2 = 0;
    for
      select  pro.complex, pro.ref, pro.descript, pro.workplace, pro.ref, pro.number,
            pro.placetime,  pro.opertime, pro.operfactor, '',''
        from prshopers pro
        where pro.sheet = :SHEET and (pro.opermaster = :oper)
        order by pro.number
        into :complex, :OOPER, :P1, :P2, :ref, :onumer,
             :P3,  :P4, :P5, :P6, :P7
    do begin
    o2 = o2 + 1;
    if (oper2 = o2) then oostatniawgrupie = 1;
    else oostatniawgrupie = 0;
    ozlozona = complex;
    if (opoziom=0) then OR1=oostatniawgrupie ;
    if (opoziom=1 and or2=0) then OR2=oostatniawgrupie ;
    if (opoziom=2 and or3=0) then OR3=oostatniawgrupie ;
    if (opoziom=3 and or4=0) then OR4=oostatniawgrupie ;
    if (opoziom=4 and or5=0) then OR5=oostatniawgrupie ;
 --   if (:OPER is not null ) then p7=:REF;
 --   p6=:complex;
    suspend;
    OLICZNIK=:OLICZNIK -1;
      if (complex = 1) then
      begin
        --OLLEVEL = POZIOM;
        poziom=poziom+1;
        for
          select OOPER, OLICZNIK, TYP, P1, P2, P3, P4, P5, P6, P7, REF, OPOZIOM, ONUMER, OZLOZONA, OOSTATNIAWGRUPIE, OR1, OR2, OR3, OR4, OR5
            from RPT_PRSHEETSOPER(:OSHEET, :OOPER, :OLICZNIK,:POZIOM,0,:OR1+:OR1,:OR2+:OR2,:OR3+:OR3,:OR4+:OR4,:OR5+:OR5)
            order by RPT_PRSHEETSOPER.ONUMER
            into :OOPER, :LICZNIK, :TYP, :P1, :P2, :P3, :P4, :P5, :P6, :P7, :REF, :OPOZIOM, :ONUMER, :OZLOZONA, :OOSTATNIAWGRUPIE, :OR1, :OR2, :OR3, :OR4, :OR5
        do begin

          suspend;
          OLICZNIK= :OLICZNIK-1;
        end

        poziom=poziom-1;
        opoziom=poziom;
      end

  end
  end
  --LLEVEL = OLLEVEL;
end^
SET TERM ; ^
