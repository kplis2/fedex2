--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDULE_UPDATE(
      PRSCHEDGUIDE integer = 0)
   as
declare variable prschedoper integer;
declare variable cnt integer;
declare variable prmachine integer;
declare variable timefrom timestamp;
declare variable timeto timestamp;
declare variable status smallint;
begin
  for
    select g.prschedoper, min(g.prmachine), min(g.timefrom), max(g.timeto), count(g.ref)
      from prgantt g
      where :prschedguide = 0 or g.prschedguide = :prschedguide
      group by g.prschedoper
      into prschedoper, prmachine, timefrom, timeto, cnt
  do begin
    if (cnt = 1) then
    begin
      update prschedopers p set p.starttime = :timefrom, p.endtime = :timeto, p.machine = :prmachine
        where p.ref = :prschedoper;
    end else
    begin
      delete from prschedopertimes t where t.prschedoper = :prschedoper;
      prmachine = null;
      timefrom = null;
      timeto = null;
      for
        select g.prmachine, g.timefrom, g.timeto, g.status
          from prgantt g
          where g.prschedoper = :prschedoper
          into prmachine, timefrom, timeto, status
      do begin
        insert into prschedopertimes (prschedoper, starttime, endtime, prmachine, status)
          values (:prschedoper, :timefrom, :timeto, :prmachine, :status);
      end
    end
  end
end^
SET TERM ; ^
