--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZFAK_GET_DOKUMPOZ(
      POZREF integer)
  returns (
      REF integer,
      SYMBOL varchar(80) CHARACTER SET UTF8                           ,
      DATA timestamp,
      NR integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      ILOSC numeric(14,4),
      ILOSCM numeric(14,4),
      CENA numeric(14,4),
      PCENA numeric(14,4))
   as
declare variable fromdokumpoz integer;
declare variable cnt integer;
begin
  cnt = 0;
  for select dokumpoz.ref, dokumnag.symbol, dokumnag.data,
    dokumpoz.numer, dokumpoz.ktm, dokumpoz.wersja,
    (case when defdokum.wydania=0 then dokumpoz.ilosc else 0 end),
    (case when defdokum.wydania=1 then dokumpoz.ilosc else 0 end),
    dokumpoz.cena, dokumpoz.pcenasr
    from dokumpoz
    left join dokumnag on (dokumnag.ref=dokumpoz.dokument)
    left join defdokum on (defdokum.symbol=dokumnag.typ)
    where dokumpoz.frompozfak = :pozref --and DEFDOKUM.WYDANIA = 0
    into :ref,:symbol,:data,
         :nr,:ktm,:wersja,:ilosc,:iloscm,:cena,:pcena
  do begin
    cnt = cnt + 1;
    suspend;
  end
  if(cnt=0) then begin
    fromdokumpoz = null;
    select fromdokumpoz from pozfak where ref=:pozref into :fromdokumpoz;
    if(:fromdokumpoz is not null) then begin
      select dokumpoz.ref, dokumnag.symbol, dokumnag.data,
      dokumpoz.numer, dokumpoz.ktm, dokumpoz.wersja,
      (case when defdokum.wydania=0 then dokumpoz.ilosc else 0 end),
      (case when defdokum.wydania=1 then dokumpoz.ilosc else 0 end),
      dokumpoz.cena, dokumpoz.pcenasr
      from dokumpoz
      left join dokumnag on (dokumnag.ref=dokumpoz.dokument)
      left join defdokum on (defdokum.symbol=dokumnag.typ)
      where dokumpoz.ref = :fromdokumpoz --and DEFDOKUM.WYDANIA = 0
      into :ref,:symbol,:data,
           :nr,:ktm,:wersja,:ilosc,:iloscm,:cena,:pcena;
      suspend;
    end
  end
end^
SET TERM ; ^
