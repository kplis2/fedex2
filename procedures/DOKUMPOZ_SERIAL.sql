--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMPOZ_SERIAL(
      POZDOKUM integer,
      AL smallint)
  returns (
      SERIALNR varchar(40) CHARACTER SET UTF8                           ,
      ILOSC numeric(14,4),
      ORGDOKUMSER integer)
   as
DECLARE VARIABLE ISNUMBER SMALLINT;
DECLARE VARIABLE ILOSCROZ NUMERIC(14,4);
DECLARE VARIABLE NR INTEGER;
DECLARE VARIABLE ODSERIALNR INTEGER;
DECLARE VARIABLE DOSERIALNR INTEGER;
begin
 isnumber = 0;
  for select ODSERIAL, ODSERIALNR, DOSERIALNR, ILOSC, ILOSCROZ, coalesce(ORGDOKUMSER,ref) from DOKUMSER
   where (DOKUMSER.REFPOZ = :pozdokum and TYP = 'M')
   into :serialnr,:odserialnr, :doserialnr, :ilosc, :iloscroz, :ORGDOKUMSER
 do begin
   isnumber = 1;
   if(:odserialnr is not null and :doserialnr is not null) then begin
     if(:al = 0) then odserialnr = :odserialnr + :iloscroz;
     for select numer from SYSKOPIE(:doserialnr - :odserialnr) into :nr
     do begin
       serialnr = :odserialnr + :nr;
       ilosc = 1;
       suspend;
     end
   end else begin
     if(:al = 0) then
       ilosc = :ilosc - :iloscroz;
     if(:ilosc > 0) then
       suspend;
   end
 end
 if(:isnumber = 0) then
   suspend;
end^
SET TERM ; ^
