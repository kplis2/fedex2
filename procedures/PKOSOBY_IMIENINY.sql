--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PKOSOBY_IMIENINY(
      DZIENIM integer,
      MIESIM integer,
      PRZYPOMN integer)
  returns (
      NASTIM timestamp)
   as
begin
   nastim = cast(extract(year from current_date) || '-' || :miesim || '-' || :dzienim as timestamp);
   if(:nastim<=current_date+:przypomn) then nastim = cast((extract(year from current_date)+1) || '-' || :miesim || '-' || :dzienim as timestamp);
   nastim = :nastim - :przypomn;
  suspend;
end^
SET TERM ; ^
