--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_OBL_TEMPOSPRZ(
      KTM varchar(80) CHARACTER SET UTF8                           )
  returns (
      ILOSCSPRZED numeric(14,4),
      TEMPODNI numeric(14,4))
   as
DECLARE VARIABLE JEDNO INTEGER;
DECLARE VARIABLE ILDNI INTEGER;
DECLARE VARIABLE ILOSCWYD NUMERIC(14,4);
DECLARE VARIABLE ILOSCZWR NUMERIC(14,4);
begin
 -- execute procedure GETCONFIG('ILDNI') returning_values :ildni;
   ildni = 10;
  if (:ildni is null or :ildni = 0) then ildni = 60;
  select max(dokumpoz.jedno), sum(case when (defdokum.wydania = 1  and defdokum.koryg = 0) then dokumpoz.ilosc else 0 end),
    sum(case when (defdokum.wydania = 0  and defdokum.koryg = 1) then dokumpoz.ilosc else 0 end)
    from dokumpoz
    left join dokumnag on (dokumnag.ref = dokumpoz.dokument)
    left join defdokum on (dokumnag.typ = defdokum.symbol)
    where dokumpoz.ktm = :ktm  and defdokum.zewn = 1 and
    ((defdokum.wydania = 1  and defdokum.koryg = 0)
    or (defdokum.wydania = 0  and defdokum.koryg = 1)) and dokumnag.akcept = 1
    and dokumnag.data <= current_date and dokumnag.data >= current_timestamp(0) - :ildni
    into :jedno, :iloscwyd, :ilosczwr;
  if (:iloscwyd is null) then iloscwyd = 0;
  if (:ilosczwr is null) then ilosczwr = 0;
  iloscsprzed = :iloscwyd - :ilosczwr;
  if (:iloscsprzed < 0) then iloscsprzed = 0;
  tempodni = :iloscsprzed/:ildni;
  if (:tempodni is null) then tempodni = 0;
  suspend;
end^
SET TERM ; ^
