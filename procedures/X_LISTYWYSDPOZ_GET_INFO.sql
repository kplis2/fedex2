--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_LISTYWYSDPOZ_GET_INFO(
      LISTWYSDPOZ STRING2048)
  returns (
      INFO STRING2048)
   as
declare variable fps string20;
begin
  info = 'BRAK';

  --Jesli SUBSTRING(Twr_Kod,5,2)=”09” to wyswietlamy „Dodatkowa Folia!”
  if (
    exists(
      select first 1 1
        from listywysdpoz p
          left join wersje w on (p.wersjaref = w.ref)
        where p.ref = :listwysdpoz
          and substring(w.kodprod from 5 for 2) = '09'
    )
  ) then
    info = :info||'Dodatkowa Folia';

  if (:info <> '' and :info <> 'BRAK') then info = :info||'; ';
  else info = '';
  --Jesli na towarze jest atrybut „Velocity [FPS]” to wyswietlamy: „FPS: „+{wartosc z tego atrybutu}
  select a.wartosc
    from listywysdpoz p
      join wersje w on (p.wersjaref = w.ref)
      join towary t on (w.ktm = t.ktm)
      join x_atrybuty a on (a.otable = 'TOWARY' and a.oref = t.ref and lower(a.nazwacechy) = 'velocity [fps]')
    where p.ref = :listwysdpoz
  into :fps;

  if (coalesce(:fps,'') <> '') then
    info = :info||'FPS: '||:fps;
end^
SET TERM ; ^
