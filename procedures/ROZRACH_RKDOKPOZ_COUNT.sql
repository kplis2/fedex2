--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ROZRACH_RKDOKPOZ_COUNT(
      SLODEF integer,
      SLOPOZ integer,
      ACCOUNT ACCOUNT_ID,
      SETTLEMENT varchar(50) CHARACTER SET UTF8                           ,
      COMPANY integer)
   as
declare variable amount numeric(14,2) ;
begin
  select sum(p.kwota)
    from rkdokpoz p
      join rkdoknag n on (p.dokument = n.ref)
      join rkrapkas r on (r.ref = n.raport)
    where r.company = :company
      and n.slodef = :slodef
      and n.slopoz = :slopoz
      and p.konto = :account
      and p.rozrachunek = :settlement
      and coalesce(p.accept,0) = 0
      and coalesce(p.anulowany,0) = 0
    into :amount;

  update ROZRACH set ROZRACH.rkdokpozamount = coalesce(:amount,0)
    where SLODEF = :slodef and :slopoz = :slopoz and SYMBFAK = :settlement and KONTOFK = :account and company = :company;
end^
SET TERM ; ^
