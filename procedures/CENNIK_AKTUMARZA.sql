--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CENNIK_AKTUMARZA(
      WERSJAREF integer)
   as
declare variable cennik integer;
declare variable jedn integer;
declare variable cenanet numeric(14,2);
declare variable starynarzut numeric(14,2);
declare variable staramarza numeric(14,2);
declare variable zrodlo integer;
declare variable przelicz numeric(14,4);
declare variable cena_zakn numeric(14,2);
declare variable cena_kosztn numeric(14,2);
declare variable cena_fabn numeric(14,2);
declare variable cenamarzy numeric(14,2);
declare variable nowynarzut numeric(14,2);
declare variable nowamarza numeric(14,2);
declare variable aktuoddzial varchar(20);
begin
    execute procedure GETCONFIG('AKTUODDZIAL') returning_values :aktuoddzial;
    for
        select CENNIK.CENNIK,CENNIK.JEDN
        ,CENNIK.CENANET,CENNIK.NARZUT,CENNIK.MARZA
        ,DEFCENNIK.ZRODLO, TOWJEDN.przelicz
        from CENNIK
        left join DEFCENNIK on (DEFCENNIK.REF=CENNIK.CENNIK)
        left join TOWJEDN on (CENNIK.JEDN = TOWJEDN.REF)
        where CENNIK.wersjaref=:WERSJAREF and CENNIK.stalacena<>2
        and ((DEFCENNIK.ODDZIAL=:aktuoddzial) or (DEFCENNIK.ODDZIAL='') or (DEFCENNIK.ODDZIAL is NULL))
        into :cennik,:jedn
        ,:cenanet,:starynarzut,:staramarza
        ,:zrodlo, :przelicz
    do begin
      if((:przelicz is null) or (:przelicz = 0)) then przelicz = 1;
      if(:ZRODLO is null) then ZRODLO = 0;
      select CENA_ZAKN,CENA_KOSZTN,CENA_FABN from WERSJE where REF=:WERSJAREF into :cena_zakn,:cena_kosztn,:cena_fabn;
      if(:ZRODLO=0) then cenamarzy = :cena_zakn;
      else if(:ZRODLO=1) then cenamarzy = :cena_kosztn;
      else if(:ZRODLO=2) then cenamarzy = :cena_fabn;
      else if(:ZRODLO=3) then cenamarzy = :cena_zakn;
      cenamarzy = :cenamarzy * :przelicz;
      if(:CENANET>0) then NOWAMARZA = (:CENANET - :cenamarzy) * 100 / :CENANET;
      else NOWAMARZA = 0;
      if(:cenamarzy>0) then NOWYNARZUT = (:CENANET - :cenamarzy) * 100 / :cenamarzy;
      else NOWYNARZUT = 100;
      if((:nowynarzut<>:starynarzut) or (:nowamarza<>:staramarza)) then begin
        update cennik set marza = :nowamarza, narzut = :nowynarzut
        where CENNIK = :cennik and WERSJAREF = :wersjaref AND JEDN=:jedn;
      end
    end
end^
SET TERM ; ^
