--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDEDETS_ADD(
      PRGDPRNAGZAM integer,
      PRGDPRPOZZAM integer,
      PRGDPRSCHEDGUIDE integer,
      PRGDPRSCHEDGUIDEPOS integer,
      PRGDPRSCHEDOPER integer,
      PRGDPRSHMAT integer,
      PRGDKTM varchar(40) CHARACTER SET UTF8                           ,
      PRGDWERSJAREF integer,
      PRGDSSOURCE smallint,
      PRGDWH varchar(3) CHARACTER SET UTF8                           ,
      PRGDTYPDOK varchar(3) CHARACTER SET UTF8                           ,
      PRGDILOSC numeric(14,4),
      PRGDOUT smallint,
      PRGDOVERLIMIT smallint,
      PRGDAUTODOC smallint,
      PRGDPRSHORTAGE integer,
      PRGDSHORTAGE smallint,
      PRGDSTABLE varchar(40) CHARACTER SET UTF8                           ,
      PRGDSREF integer,
      PRGENGROUP integer)
  returns (
      NEWREF integer)
   as
begin
  if (prgengroup is null) then prgengroup = 0;
  if (prgengroup = 0) then
    execute procedure gen_ref ('PRGENGROUP') returning_values prgengroup;
  execute procedure gen_ref('PRSCHEDGUIDEDETS') returning_values newref;
  insert into prschedguidedets (ref,prnagzam, prpozzam, prschedguide, prschedguidepos, ktm, wersjaref,
      quantity, wh, doctype, out, overlimit, ssource, prschedoper, autodoc, stable, sref,
      prshortage, shortage, prgengroup)
    values (:newref,:prgdprnagzam, :prgdprpozzam, :prgdprschedguide, :prgdprschedguidepos, :prgdktm, :prgdwersjaref,
      :prgdilosc, :prgdwh, :prgdtypdok, :prgdout, :prgdoverlimit, :prgdssource,:prgdprschedoper, :prgdautodoc, :prgdstable, :prgdsref,
      :prgdprshortage, :prgdshortage, :prgengroup);
end^
SET TERM ; ^
