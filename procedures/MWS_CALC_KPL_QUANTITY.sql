--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CALC_KPL_QUANTITY(
      WH DEFMAGAZ_ID,
      VERS integer,
      LOTIN integer)
  returns (
      QUANTITY numeric(15,4))
   as
declare variable versquantity numeric(15,4);
declare variable kplposvers integer;
declare variable convertval numeric(15,4);
declare variable kplversq numeric(15,4);
declare variable constlocq numeric(15,4);
declare variable wgordsq numeric(15,4);
declare variable waitingq numeric(15,4);
declare variable sellordsq numeric(15,4);
begin
  if (:lotin is null) then lotin = 0;

  if (:lotin = 0) then
    select v.quantity from VIEW_MWS_QUANTITY v
      where v.vers = :vers
        and v.wh = :wh
    into :versquantity;
  else
    select v.quantity from VIEW_MWS_LOT_QUANTITY v
      where v.vers = :vers
        and v.wh = :wh
        and v.lot = :lotin
    into :versquantity;

  for
    select p.wersjaref, p.ilosc
      from kplnag n
        left join kplpoz p on (n.ref = p.nagkpl)
      where n.wersjaref = :vers
    into :kplposvers, :convertval
  do begin
    kplversq = 0;

    if (:lotin = 0) then
    begin
      select coalesce(sum(s.quantity - s.blocked + s.ordered),0)
        from mwsstock s
          left join mwsconstlocs c on (s.mwsconstloc = c.ref)
          left join defmagaz d on (d.symbol = c.wh)
        where c.act > 0
          and c.locdest in (1,2,3,4,5,9)
          and d.mws = 1
          and s.vers = :kplposvers
          and s.wh = :wh
      into :constlocq;
      if (:constlocq is null) then constlocq = 0;
  
      wgordsq = 0;
      select coalesce(sum(a.quantity),0)
        from mwsords o
          left join mwsacts a on (a.mwsord = o.ref)
        where o.mwsordtypedest = 9
          and o.status in (1,2)
          and a.status > 0 and a.status < 6
          and a.vers = :kplposvers
          and a.wh = :wh
      into :wgordsq;
      if (:wgordsq is null) then wgordsq = 0;
  
      waitingq = 0;
      select coalesce(sum(case when n.mwsdisposition = 1 then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0)),0)
        from dokumnag n
          left join dokumpoz p on (n.ref = p.dokument)
          left join defdokummag d on (d.typ = n.typ and d.magazyn = n.magazyn)
        where n.magazyn = :wh
          and n.mwsdone = 0
          and n.wydania = 1
          and n.mwsdoc = 1
          and coalesce(n.blockmwsords,0) = 0
          and n.frommwsord is null
          and ((n.akcept = 1 and p.iloscl > p.ilosconmwsacts) or
               (n.akcept = 0 and n.mwsdisposition = 1 and p.ilosc > p.ilosconmwsacts))
          and p.wersjaref = :kplposvers
          and p.genmwsordsafter = 1
          and coalesce(p.fake,0) = 0
          and coalesce(p.havefake,0) = 0
          and coalesce(d.afterackproc,'') <> ''
          and d.mwsordwaiting = 1
      into :waitingq;
      if (:waitingq is null) then waitingq = 0;
  
      sellordsq = 0;
      select s.zablokow as quantity
        from stanyil s
        where s.magazyn = :wh
          and s.wersjaref = :kplposvers
      into :sellordsq;
      if (:sellordsq is null) then sellordsq = 0;
    end

    if (:lotin > 0) then
    begin
      select coalesce(sum(s.quantity - s.blocked + s.ordered),0)
        from mwsstock s
          left join mwsconstlocs c on (s.mwsconstloc = c.ref)
          left join defmagaz d on (d.symbol = c.wh)
        where c.act > 0
          and c.locdest in (1,2,3,4,5,9)
          and d.mws = 1
          and s.vers = :kplposvers
          and s.wh = :wh
          and s.lot = :lotin
      into :constlocq;
      if (:constlocq is null) then constlocq = 0;
  
      wgordsq = 0;
      select coalesce(sum(a.quantity),0)
        from mwsords o
          left join mwsacts a on (a.mwsord = o.ref)
        where o.mwsordtypedest = 9
          and o.status in (1,2)
          and a.status > 0 and a.status < 6
          and a.vers = :kplposvers
          and a.wh = :wh
          and a.lot = :lotin
      into :wgordsq;
      if (:wgordsq is null) then wgordsq = 0;
  
      waitingq = 0;
      select coalesce(sum(case when n.mwsdisposition = 1 then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0)),0)
        from dokumnag n
          left join dokumpoz p on (n.ref = p.dokument)
          left join defdokummag d on (d.typ = n.typ and d.magazyn = n.magazyn)
        where n.magazyn = :wh
          and n.mwsdone = 0
          and n.wydania = 1
          and n.mwsdoc = 1
          and coalesce(n.blockmwsords,0) = 0
          and n.frommwsord is null
          and ((n.akcept = 1 and p.iloscl > p.ilosconmwsacts) or
               (n.akcept = 0 and n.mwsdisposition = 1 and p.ilosc > p.ilosconmwsacts))
          and p.wersjaref = :kplposvers
          and p.genmwsordsafter = 1
          and coalesce(p.fake,0) = 0
          and coalesce(p.havefake,0) = 0
          and coalesce(d.afterackproc,'') <> ''
          and d.mwsordwaiting = 1
          and p.dostawa = :lotin
      into :waitingq;
      if (:waitingq is null) then waitingq = 0;
  
      sellordsq = 0;
      select s.zablokow as quantity
        from stanycen s
        where s.magazyn = :wh
          and s.wersjaref = :kplposvers
          and s.dostawa = :lotin
      into :sellordsq;
      if (:sellordsq is null) then sellordsq = 0;
    end

    kplversq = :constlocq + :wgordsq - :waitingq - :sellordsq;
    kplversq = :kplversq / :convertval;

    if (:quantity is null or :quantity > :kplversq) then
      quantity = :kplversq;
  end
  quantity = :quantity + coalesce(:versquantity,0);
  suspend;
end^
SET TERM ; ^
