--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RP_CHECK_TOKEN(
      TABELA varchar(31) CHARACTER SET UTF8                           ,
      OLDTOKEN smallint)
  returns (
      TOKEN smallint)
   as
declare variable location varchar(255) ;
  declare variable locatupr varchar(255);
  declare variable tryb varchar(255);
begin
  if (oldtoken is null or oldtoken=0 or oldtoken=6
      or oldtoken=8 or oldtoken=9) then
  begin
    select LOCATUPR from RP_SYNCDEF where TABELA=:TABELA
      into :locatupr;

    if (locatupr<>'') then
    begin
      execute procedure GETCONFIG('AKTULOCAT')
        returning_values :location;
      execute procedure GETCONFIG('AKTULOCATTRYB')
        returning_values :tryb;
      if (locatupr='0') then
        token = 9;
      else if (locatupr='-1' and tryb='M') then
        token = 9;
      else if (locatupr='-2' and tryb='S') then
        token = 9;
      else if (locatupr=location) then
        token = 9;
      else if (oldtoken<>8 or oldtoken is null) then
        token = 0;
    end else
      if (oldtoken<>8 or oldtoken is null) then
        token = 9;
  end else
    token = oldtoken;

  if (token=0 and (oldtoken<>6 or oldtoken is null)) then
    exception RP_CHANGERECORDTOKENERR;

  --zdjecie uprawnien dla token = 8 - replikator
  if (token=8) then
    token = 0;

  --jesli brak ustawien replikacji to wszyscy maja prawo [DU]
  if (token is null) then
    token = 9;
end^
SET TERM ; ^
