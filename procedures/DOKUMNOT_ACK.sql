--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNOT_ACK(
      DOKUM integer,
      ACK smallint)
   as
declare variable LOOP smallint;
declare variable GRDOKUM integer;
declare variable DOKUMPREF integer;
declare variable FAKTURA integer;
begin
  if(:ack < 7) then begin
    /*wycofanie dokumentow pochodnych*/
    for select REF from DOKUMNOT where grupa = :dokum and ref<>:dokum
    order by ref desc
    into :grdokum
    do begin
      update DOKUMNOT set AKCEPT = 0 where ref = :grdokum and AKCEPT > 0;
      delete from DOKUMNOT where ref = :grdokum;
    end
  end
  /*akceptacja noty biezacej*/

  loop = 1;
  while(loop = 1) do begin
    dokumpref = null;
    select first 1 ref
      from dokumnotp
      where dokument = :dokum and akceptpoz<>:ack
      order by dokument, ref
      into :dokumpref;
    if(:dokumpref>0) then execute procedure DOKUMNOTP_ACK(:dokumpref, :ack);
    else loop = 0;
  end

  /*akceptacja pozostalych not z grupy wedlug ref'ow*/
  if(:ack < 7) then begin
    loop = :ack;
    while(loop = 1) do begin
      grdokum = null;
      select first 1 REF from DOKUMNOT    --MS optymalizacja
      where DOKUMNOT.grupa = :dokum and akcept = 0
      order by grupa,ref
      into :grdokum;
      if(:grdokum > 0) then
        if (not exists(select first 1 1 from dokumnotp where dokument = :grdokum)) then
          delete from dokumnot where ref = :grdokum;
        else
          update DOKUMNOT set AKCEPT = 1 where REF=:grdokum;
      else
        loop = 0;
    end
  end
  /*obiczenie wartosci noty magazynowej*/
  if(:ack = 1) then begin
    -- liczymy wartosc pozycji, ktore maja wskazanie na rozpise
    update DOKUMNOTP set WARTOSC =
      cast(DOKUMNOTP.CENANEW*(select DOKUMROZ.ILOSC from DOKUMROZ where DOKUMROZ.REF=DOKUMNOTP.dokumrozkor) as numeric(14,2))
      - cast(DOKUMNOTP.CENAOLD*(select DOKUMROZ.ILOSC from DOKUMROZ where DOKUMROZ.REF=DOKUMNOTP.dokumrozkor) as numeric(14,2))
      where DOKUMENT=:dokum and DOKUMROZKOR is not null;
    -- liczymy wartosc pozycji, ktore nie maja rozpiski (noty przeceniajace stan magazynowy)
    update DOKUMNOTP set WARTOSC =
      cast (((DOKUMNOTP.CENANEW - DOKUMNOTP.CENAOLD) * DOKUMNOTP.ILOSC) as numeric(14,2))
      where DOKUMENT=:dokum and DOKUMROZKOR is null;
    -- liczymy wartosc calej noty
    update DOKUMNOT set WARTOSC =
      (select sum(WARTOSC) from DOKUMNOTP where DOKUMNOTP.dokument = :dokum)
      where REF=:dokum;
  end else if(:ack < 7) then
    update DOKUMNOT set WARTOSC = 0 where ref=:dokum;
  /*zaktualizowanie znacznika rozliczenia magazynowego na powiazanej fakturze jesli to nota do zakupu*/
  select FAKTURA from DOKUMNOT where REF=:dokum into :faktura;
  if(:faktura is not null) then begin
    execute procedure NAGFAK_OBL_MAGROZLICZ(:faktura);
  end
end^
SET TERM ; ^
