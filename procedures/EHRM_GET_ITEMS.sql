--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EHRM_GET_ITEMS(
      EMPLOYEEREF EMPLOYEES_ID,
      FROMDATE TIMESTAMP_ID,
      TODATE TIMESTAMP_ID)
  returns (
      REF EPRESENCELISTSPOS_ID,
      ITEMDATE TIMESTAMP_ID,
      ORGTIMESTART STRING20,
      ORGTIMEEND STRING20,
      NEWTIMESTART STRING20,
      NEWTIMEEND STRING20,
      KIND smallint,
      ORGCOLOR varchar(10) CHARACTER SET UTF8                           ,
      NEWCOLOR varchar(10) CHARACTER SET UTF8                           ,
      VERCOLOR varchar(10) CHARACTER SET UTF8                           ,
      DESCRIPTION STRING5000,
      NOWCOLOR varchar(10) CHARACTER SET UTF8                           ,
      ACCEPTCOLOR varchar(10) CHARACTER SET UTF8                           ,
      STATUS smallint)
   as
begin
  orgcolor = '#D30617';
  newcolor = '#1458E0';
  vercolor = '#F7CF00';       --BS56958
  nowcolor = '#FF6C00';       --BS56958
  acceptcolor = '#006400';   --BS56958

  for select
    lp.ref,
    l.listsdate,
    coalesce(lp.timestartstr, (select first 1 p.timestopstr      --BS56958
            from epresencelistspos p
            where p.timestop < lp.timestop and p.epresencelists = lp.epresencelists and p.employee = lp.employee
            order by p.timestop desc),'00:00') as timestart,
    coalesce(lp.timestopstr, (select first 1 p.timestartstr          --BS56958
            from epresencelistspos p
            where p.timestart > lp.timestart and p.epresencelists = lp.epresencelists and p.employee = lp.employee
            order by p.timestart asc),'23:59') as timestop,
    lp.newstartat,
    lp.newstopat,
    case
        when (lp.timestart is null or lp.timestop is null)
         then '4'
         else lp.state
    end as state,
    lp.descapply,
    lp.state
    from epresencelistspos lp
    inner join epresencelists l on l.ref = lp.epresencelists
    where lp.employee = :employeeref and
          l.listsdate >= :fromdate and
          l.listsdate < :todate
    order by l.listsdate asc
   into :ref, :itemdate, :orgtimestart, :orgtimeend, :newtimestart, :newtimeend, :kind, :description, :STATUS
  do begin
     suspend;
  end
end^
SET TERM ; ^
