--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_PERSONS_WYCH_OR_BEZPL(
      FROMDATE date,
      TODATE date,
      COMPANY integer,
      ONLY_WOMAN smallint)
  returns (
      PERSON integer)
   as
declare variable absence integer;
  declare variable last_person integer;
  declare variable afromdate date;
  declare variable real_afromdate date;
  declare variable atodate date;
  declare variable y integer;
  declare variable m integer;
  declare variable d integer;
begin
  --DS: funkcja wykorzystywana w sprawozdaniu GUS Z-06,
  --wylicza osoby, ktore w zadanym okresie byly na wychowawczym badz bezplatnym co najmniej 3 miesiace
  last_person = 0;
  atodate =  '1899-12-30';
  for
    select a.ref, e.person, a.fromdate
      from persons p
        join employees E on p.ref = e.person
        join employment C on (E.ref = C.employee)
        join eabsences A on (A.employee = E.ref)
      where C.workdim >= 1
        and A.fromdate > :fromdate
        and A.fromdate <= :todate
        and C.fromdate < A.fromdate
        and (C.todate > A.fromdate or c.todate is null)
        and A.ecolumn in (260,300)
        and e.company = :company
        and (p.sex = 0 or :only_woman = 0)
      order by e.person, A.fromdate
    into :absence, :person, :afromdate
  do begin
    if (last_person <> person) then
      atodate = '1899-12-30';
    if (afromdate > atodate) then
    begin
      select afromdate, atodate
        from e_get_whole_eabsperiod(:absence,null,null,null,null,null,';260;300;')
        into :real_afromdate, :atodate;
      if (afromdate = real_afromdate) then
      begin
        execute procedure get_ymd(afromdate, atodate) returning_values y,m,d;
        --jesli urlop trwal co najmniej 3 miesiace
        if ((m >= 3 or y > 0) and person <> last_person) then
        suspend;
      end
    end
    last_person = person;

  end
end^
SET TERM ; ^
