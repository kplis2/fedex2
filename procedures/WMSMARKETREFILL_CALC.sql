--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WMSMARKETREFILL_CALC(
      WH DEFMAGAZ_ID,
      DOCTYPES STRING40,
      INTERVAL INTEGER_ID)
   as
declare variable vers wersje_id;
declare variable mwsconstloc mwsconstlocs_id;
declare variable mwsconstlocsymbol string40;
declare variable good ktm_id;
begin
  execute procedure WMSMARKETREFILL_CALC_PLACE(:WH);
  execute procedure WMSMARKETREFILL_CALC_MINMAX(:WH,:doctypes,:interval);
end^
SET TERM ; ^
