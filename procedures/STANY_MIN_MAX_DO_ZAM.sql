--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STANY_MIN_MAX_DO_ZAM(
      DOSTAWCA integer,
      TRYB smallint,
      MKTM varchar(80) CHARACTER SET UTF8                           ,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           )
  returns (
      KTM varchar(80) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      NAZWAT varchar(80) CHARACTER SET UTF8                           ,
      STAN numeric(14,4),
      ZAMOWIONO numeric(14,4),
      ZAREZERW numeric(14,4),
      ZABLOKOW numeric(14,4),
      PRODIL numeric(14,4),
      ILDOST numeric(14,4),
      STANMIN numeric(14,4),
      STANMAX numeric(14,4),
      ILSUBST numeric(14,4),
      SUBSTMIN numeric(14,4),
      SUBSTMAX numeric(14,4),
      DOSTGLOW integer,
      DOSTGLOWDICT varchar(60) CHARACTER SET UTF8                           ,
      POWIAZANIE varchar(20) CHARACTER SET UTF8                           )
   as
BEGIN
  EXECUTE PROCEDURE get_config('SUBSTYTUT',0) RETURNING_VALUES :POWIAZANIE;
  FOR
    SELECT DISTINCT stanyil.ktm, stanyil.wersjaref, stanyil.nazwat, stanyil.ilosc,
        stanyil.zarezerw, stanyil.zablokow, stanyil.zamowiono, stanyil.stanmin,
        stanyil.stanmax, stanyil.ilosc - stanyil.zarezerw - stanyil.zablokow + stanyil.zamowiono
      FROM stanyil
      LEFT JOIN dostcen ON (dostcen.wersjaref = stanyil.wersjaref and dostcen.akt = 1)
      WHERE stanyil.magazyn = :magazyn AND stanyil.stanmin IS NOT NULL AND stanyil.stanmax IS NOT NULL
          AND (:dostawca IS NULL OR (dostawca IS NOT NULL AND dostcen.dostawca = :dostawca))
          AND (:mktm IS NULL OR :mktm = '' OR (:mktm IS NOT NULL AND :mktm <> ''
          AND stanyil.ktm LIKE '%'||:mktm||'%'))
    INTO :ktm, :wersjaref, :nazwat, :stan,
        :zarezerw, :zablokow, :zamowiono, :stanmin,
        :stanmax, :ildost
  DO BEGIN
     SELECT SUM(stanyil.ilosc - stanyil.zablokow - stanyil.zarezerw + stanyil.zamowiono),
        SUM(stanyil.stanmin), SUM(stanyil.stanmax)
       FROM towakces
       JOIN stanyil ON (stanyil.wersjaref = towakces.awersjaref AND towakces.ktm=:ktm)
       WHERE stanyil.magazyn = :magazyn AND towakces.typ = :powiazanie
     INTO :ilsubst, :substmin, :substmax;

     select coalesce(sum (coalesce(n.kilosc,0) - coalesce(n.kilzreal,0)),0)
      from nagzam n
        join prschedguides p on (p.zamowienie = n.ref)
      where n.kwersjaref = :wersjaref
        and n.prsheet is not null
      into :PRODIL;

     ildost = ildost + :PRODIL; -- zwiekszam ildost
     /*SELECT dostcen.dostawca, dostawcy.id
       FROM dostcen
       JOIN dostawcy ON (dostcen.dostawca = dostawcy.ref
           AND dostcen.wersjaref = :wersjaref AND dostcen.gl = 1) */
     select dostcen.dostawca, dostawcy.id
      from wersje
      left JOIN dostcen ON (dostcen.wersjaref = wersje.ref AND dostcen.gl = 1)
      left join dostawcy on (dostawcy.ref = dostcen.dostawca)
     where wersje.ref = :wersjaref
     INTO :dostglow, :dostglowdict;

     if (:tryb = 0 and :ildost <= :stanmin ) then suspend;
     else if (:tryb = 1 and :ildost >= :stanmin and :ildost <=:stanmax) then suspend;
     else if (:tryb = 2 and :ildost >= :stanmax ) then suspend;
     else if (:tryb = 3) then suspend;
  END
END^
SET TERM ; ^
