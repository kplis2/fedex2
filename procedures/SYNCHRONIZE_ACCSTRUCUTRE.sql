--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYNCHRONIZE_ACCSTRUCUTRE(
      PATTERN ACCSTRUCTURE_ID,
      DEST ACCSTRUCTURE_ID,
      INTERNAL SMALLINT_ID default 0)
   as
declare variable vnlevel smallint;
declare variable vseparator char(1);
declare variable vdictdef slo_id;
declare variable vdictfilter string255;
declare variable vlen smallint;
declare variable vord smallint;
declare variable vdoopisu smallint_id;
begin
  --Procedura do synchronizacji dwóch poziomów analitycznych konta ksigowego. Jeżeli internal = 1 to update nie
  -- uruchomi mechanizmów synchronizacyjnych jezeli konto analityczne docelowe bedzie objete synchronizacja.
  --Domyslenie 0 bo jak ktos niecelowo cos zrobi zeby nie rozspójnic danych ze wzorcem
  select nlevel, separator, dictdef, dictfilter, len, ord, doopisu
    from accstructure
    where ref = :pattern
    into :vnlevel, :vseparator, :vdictdef, :vdictfilter, :vlen, :vord, :vdoopisu;
  update accstructure a
    set nlevel = :vnlevel, separator = :vseparator, dictdef = :vdictdef, dictfilter = :vdictfilter,
      len = :vlen, ord = :vord, doopisu = :vdoopisu, internalupdate = :internal
    where ref = :dest;
end^
SET TERM ; ^
