--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_ACK(
      DOKUMENT integer,
      ACK integer)
   as
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable cena numeric(14,4);
declare variable cena_koszt numeric(14,4);
declare variable ilosc numeric(15,5);
declare variable dostawa integer;
declare variable pozdokum integer;
declare variable fifo char(1);
declare variable typ char(1);
declare variable bktm varchar(40);
declare variable bwersja integer;
declare variable bcena numeric(15,4);
declare variable bcena_koszt numeric(14,4);
declare variable bilosc numeric(15,5);
declare variable bdostawa integer;
declare variable wydania smallint;
declare variable koryg smallint;
declare variable dokpozcenzero varchar(200);
declare variable miedzywart smallint;
declare variable stat smallint;
declare variable proddokum integer;
declare variable prodpozdokum integer;
declare variable prodwydania smallint;
declare variable prodpopraw smallint;
declare variable inwenta smallint;
declare variable usluga smallint;
declare variable slodef integer;
declare variable slopoz integer;
declare variable magazyn varchar(3);
declare variable typdok varchar(3);
declare variable cenasnetto numeric(14,4);
declare variable cenasbrutto numeric(14,4);
declare variable numer integer;
declare variable opktryb integer;
declare variable bn varchar(1);
declare variable back integer;
declare variable opkref integer;
declare variable opkstan numeric(18,4);
declare variable pozilosc numeric(18,4);
declare variable stanyujemne smallint;
declare variable zewn smallint;
declare variable pozref integer;
declare variable srvcontract integer;
declare variable data timestamp;
declare variable srvdaysinterval integer;
declare variable name varchar(255);
declare variable kod varchar(100);
declare variable kontofk varchar(30);
declare variable dnoddzial varchar(10);
declare variable prodtryb smallint;
declare variable altposmode smallint;
begin
  if((:ack = 8) or (:ack = 7)) then ack = 0;
  select d.magazyn, d.typ, d.inwentaafter, d.slodef, d.slopoz, d.data, d.oddzial,
      m.fifo, m.typ, m.stanycen,
      dd.wydania, dd.koryg, dd.miedzywart, dd.opak, dd.kaucjabn, dd.zewn,
      dm.prodtryb
    from dokumnag d
      join defmagaz m on (d.magazyn = m.symbol)
      join defdokum dd on (d.typ = dd.symbol)
      join defdokummag dm on (d.magazyn = dm.magazyn and d.typ = dm.typ)
    where ref=:dokument
  into :magazyn, :typdok, :inwenta, :slodef, :slopoz, :data, :dnoddzial,
    :fifo, :typ, :stanyujemne,
    :wydania, :koryg, :miedzywart, :opktryb, :bn, :zewn,
    :prodtryb;
 if(:ack = 0 and :FIFO = 'R'  and (:typ = 'C' or (:typ = 'P'))) then begin
   /* dokument magazynu wielu cen/parti reczny - odakceptwanie polega na cofnieciu akceptacji rozpisek */
   for select ref from DOKUMPOZ where dokument=:dokument into :pozdokum do begin
     update DOKUMROZ set ACK = 0 where POZYCJA=:pozdokum;
   end
 end else if(:ack = 9) then begin
   /*wycofanie do poprawy*/
   update DOKUMPOZ set BKTM = KTM, BWERSJA = wersja,  BILOSC = ILOSC, BCENA = cena, BCENA_KOSZT = cena_koszt, BDOSTAWA = dostawa, BLOKCENA = 1
   where DOKUMPOZ.dokument = :dokument;
 end else if(:ack = 5) then begin
   /*akceptacja po wycofaniu do poprawy*/
   if(:wydania=0) then execute procedure DOKUMNAG_OBL_CENAKOSZT(:dokument);
   for select DOKUMPOZ.REF,DOKUMPOZ.KTM,DOKUMPOZ.WERSJA,DOKUMPOZ.CENA,DOKUMPOZ.CENA_KOSZT,DOKUMPOZ.ILOSC,DOKUMPOZ.DOSTAWA,DOKUMPOZ.BKTM,DOKUMPOZ.BWERSJA,DOKUMPOZ.BCENA,DOKUMPOZ.BCENA_KOSZT,DOKUMPOZ.BILOSC,DOKUMPOZ.BDOSTAWA
   from DOKUMPOZ
     left join towary on (towary.ktm = dokumpoz.ktm)
   where dokumpoz.dokument=:dokument and towary.usluga<>1
     into :pozdokum,:ktm,:wersja,:cena,:cena_koszt,:ilosc,:dostawa, :bktm, :bwersja, :bcena, :bcena_koszt, :bilosc, :bdostawa
   do begin
     prodpopraw = 0;
     if( :ilosc = 0) then exception DOKUMNAG_ACKILOSC;
     execute procedure getconfig('DOKPOZCENZERO') returning_values :dokpozcenzero;
     if(:wydania = 0 and :koryg = 0 and :cena = 0 and :bcena <> 0 and :dokpozcenzero='1') then exception DOKUMNAG_ACKCENA;
     -- sprawdzenie czy mamy do czynienia z produkcja w tle lub automatycznym przesunieciem na mag. inwentaryzacji
     if (exists (select ref from dokumnag where dokumnag.grupaprod = :pozdokum)) then
       prodpopraw = 1;
     else
       prodpopraw = 0;
     /*sprawdzenie, czy trzeba wycofac w calosci stare wartosci*/
     if(((:KTM <> :bktm or :wersja <> :bwersja or cena <> :bcena
          --or cena_koszt <> :bcena_koszt
          or coalesce(:dostawa,0) <> coalesce(:bdostawa,0)
          --or (dostawa is not null and :BDOSTAWA is null) or (:dostawa is null and :bdostawa is not null)
          )
          and :bilosc > 0
         ) or :prodpopraw = 1)
     then begin
       bilosc = -:bilosc;
       -- usuniecie pozycji dokumentów przesuwajacych towar na magazyn inwentaryzacji
       if (:inwenta > 0 and :inwenta is not null and :prodpopraw = 1) then begin
         for
           select dokumnag.ref, defdokum.wydania
             from dokumpoz
             join dokumnag on (dokumnag.grupaprod = dokumpoz.ref)
             join defdokum on (defdokum.symbol = dokumnag.typ)
           where dokumpoz.ref = :pozdokum
           order by defdokum.wydania into :proddokum, :prodwydania
         do begin
           update dokumnag set dokumnag.akcept = 9 where dokumnag.ref = :proddokum;
           if(:prodwydania=0) then execute procedure DOKUMNAG_OBL_CENAKOSZT(:proddokum);
           for
             select REF
             from DOKUMPOZ where dokument=:proddokum
             into :prodpozdokum
           do begin
             delete from dokumpoz where dokumpoz.ref = :prodpozdokum;
           end
         end
       end
       update DOKUMROZ set BLOKOBLNAG = 1 where DOKUMROZ.pozycja = :pozdokum and BLOKOBLNAG = 0;
       update DOKUMROZ set ACK = 9 where DOKUMROZ.pozycja = :pozdokum and ACK = 1;
       exception universal'1';
       execute procedure DOKUMPOZ_CHANGE(:pozdokum,:bktm,:bwersja,:bcena,:bdostawa,:bilosc,0,1);
       update DOKUMROZ set BLOKOBLNAG = 0 where DOKUMROZ.pozycja = :pozdokum and BLOKOBLNAG = 1;
       update DOKUMROZ set ACK = 1 where DOKUMROZ.pozycja = :pozdokum and ACK = 9;
       bilosc = 0;
       -- wycofanie do poprawy dokumentow kompletacji i usuniecie ich pozycji dla kompletacji
       if ((:inwenta = 0 or :inwenta is null) and :prodpopraw = 1) then begin
         for
           select dokumnag.ref, defdokum.wydania
             from dokumpoz
             join dokumnag on (dokumnag.grupaprod = dokumpoz.ref)
             join defdokum on (defdokum.symbol = dokumnag.typ)
           where dokumpoz.ref = :pozdokum
           order by defdokum.wydania into :proddokum, :prodwydania
         do begin
           update dokumnag set dokumnag.akcept = 9 where dokumnag.ref = :proddokum;
           if(:prodwydania=0) then execute procedure DOKUMNAG_OBL_CENAKOSZT(:proddokum);
           for
             select REF
             from DOKUMPOZ where dokument=:proddokum
             into :prodpozdokum
           do begin
             delete from dokumpoz where dokumpoz.ref = :prodpozdokum;
           end
         end
       end
     end
     if(:BILOSC is null) then bilosc = 0;
     ilosc = :ilosc - :bilosc;
     if(:ilosc <> 0) then begin
       -- wstawienie pozycji kompletacji od nowa
       if ((:inwenta is null or :inwenta = 0) and :prodpopraw = 1) then
         execute procedure dokumnag_autocompletation(:dokument,:pozdokum,:ack,:inwenta);
       update DOKUMROZ set BLOKOBLNAG = 1 where DOKUMROZ.pozycja = :pozdokum and BLOKOBLNAG = 0;
       --exception universal'2';
       execute procedure DOKUMPOZ_CHANGE(:pozdokum,:ktm,:wersja,:cena,:dostawa,:ilosc,0,1);
       update DOKUMROZ set BLOKOBLNAG = 0 where DOKUMROZ.pozycja = :pozdokum and BLOKOBLNAG = 1;
       -- wystawienie pozycji dokumentow przesuwajacych towar na mag. inwentaryzacji
       if (:inwenta is not null and :inwenta > 0 and :prodpopraw = 1) then
         execute procedure dokumnag_autocompletation(:dokument,:pozdokum,:ack,:inwenta);
     end
     update DOKUMROZ set PCENA = null
        where DOKUMROZ.pozycja = :pozdokum;--wymuszenie odtworzenia ceny
     execute procedure DOKPOZ_OBL_FROMROZ(:pozdokum);
   end
   update DOKUMPOZ set BLOKCENA = 0 where dokument =:dokument;
 end else begin
   if(:wydania=0) then execute procedure DOKUMNAG_OBL_CENAKOSZT(:dokument);
   if(:ack = 1) then begin
     if(not exists(select ref from DOKUMNAG join DEFDOKUMMAG on (DEFDOKUMMAG.magazyn = DOKUMNAG.magazyn and DEFDOKUMMAG.typ = DOKUMNAG.typ) where DOKUMNAG.REF=:dokument)) then
       exception DOKUMNAG_EXPT 'Typ dok. mag. niedopuszczalny na wybranym magazynie';
   end
   for select DOKUMPOZ.REF,DOKUMPOZ.KTM,DOKUMPOZ.WERSJA,DOKUMPOZ.CENA,DOKUMPOZ.ILOSC,DOKUMPOZ.DOSTAWA, TOWARY.usluga,
          coalesce(TOWARY.altposmode,0)
   from DOKUMPOZ
     left join towary on (towary.ktm = dokumpoz.ktm)
   where dokumpoz.dokument=:dokument and towary.usluga<>1
     into :pozdokum,:ktm,:wersja,:cena,:ilosc,:dostawa, :usluga, :altposmode
   do begin
       prodpopraw = 0;
       -- sprawdzenie czy mamy do czynienia z produkcja w tle lub automatycznym przesunieciem na mag. inwentaryzacji
       if(:ack = 0) then begin
         if (exists (select ref from dokumnag where dokumnag.grupaprod = :pozdokum)) then
           prodpopraw = 1;
         else
           prodpopraw = 0;
       end
       -- dokonanie kompletacji w tle
       if(:ack = 1 and :altposmode = 0 and (:inwenta is null or :inwenta = 0) and
          ((:usluga = 2 and prodtryb > 0) or (exists(select first 1 1 ref from dokumpoz where dokument = :dokument and alttopoz = :pozdokum)))) then
         execute procedure dokumnag_autocompletation(:dokument,:pozdokum,:ack,:inwenta);
       -- wycofanie akceptacji dokumentow przesuniecia dla inwentaryzacji
       if (:ack = 0 and :inwenta > 0 and :inwenta is not null and :prodpopraw = 1 ) then
       begin
         for
           select dokumnag.ref
             from dokumpoz
               left join dokumnag on (dokumnag.grupaprod = dokumpoz.ref)
               left join defdokum on (defdokum.symbol = dokumnag.typ)
             where dokumpoz.ref = :pozdokum
             order by defdokum.wydania into :proddokum
         do begin
           update dokumnag set dokumnag.akcept = 0 where dokumnag.ref = :proddokum;
           delete from dokumnag where dokumnag.ref = :proddokum;
         end
       end
       if(:ack = 0) then ilosc= - ilosc;
       if(:ack = 1 and :ilosc = 0) then exception DOKUMNAG_ACKILOSC;
       execute procedure getconfig('DOKPOZCENZERO') returning_values :dokpozcenzero;
       if(:ack = 1 and :wydania = 0 and :koryg = 0 and :cena = 0 and :dokpozcenzero = '1') then exception DOKUMNAG_ACKCENA;
       update DOKUMROZ set BLOKOBLNAG = 1 where DOKUMROZ.pozycja = :pozdokum and BLOKOBLNAG = 0;
       execute procedure DOKUMPOZ_CHANGE(:pozdokum,:ktm,:wersja,:cena,:dostawa,:ilosc,0,1);
       update DOKUMROZ set BLOKOBLNAG = 0 where DOKUMROZ.pozycja = :pozdokum and BLOKOBLNAG = 1;
       -- dokonanie przesuniecia towarow na magazyn inwentaryzacji
       if(:ack = 1 and :inwenta > 0 and :inwenta is not null) then
         execute procedure dokumnag_autocompletation(:dokument,:pozdokum,:ack,:inwenta);
       -- wycofanie akceptacji dokumentow kompletacji i ich usuniecie dla produkcji
       if (:ack = 0 and (:inwenta = 0 or :inwenta is null) and :prodpopraw = 1) then
       begin
         for
           select dokumnag.ref
             from dokumpoz
               left join dokumnag on (dokumnag.grupaprod = dokumpoz.ref)
               left join defdokum on (defdokum.symbol = dokumnag.typ)
             where dokumpoz.ref = :pozdokum
             order by defdokum.wydania into :proddokum
         do begin
           update dokumnag set dokumnag.akcept = 0 where dokumnag.ref = :proddokum;
           delete from dokumnag where dokumnag.ref = :proddokum;
         end
       end
   end
 end
 -- naliczenie ILOSCL dla pozycji uslugowych
 for select DOKUMPOZ.REF,TOWARY.USLUGA
 from DOKUMPOZ
   left join towary on (towary.ktm = dokumpoz.ktm)
 where dokumpoz.dokument=:dokument and towary.usluga=1
   into :pozdokum,:usluga
 do begin
   select sum(ILOSC) from DOKUMPOZ where KORTOPOZ=:pozdokum into :ilosc;
   if(:ilosc is null) then ilosc = 0;
   update DOKUMPOZ set ILOSCL = ILOSC - :ilosc where REF=:pozdokum;
 end

 /*naliczanie i rozliczanie opakowan, ktore nie maja stanow na magazynie bo sa jako uslugi*/
 if(:ack=1 or :ack=5 or :ack=0 or :ack=9) then begin
   if(:ack=1 or :ack=5) then back = 0;
   else back = 1;
   if(:opktryb = 1) then
   begin
     for select DOKUMPOZ.REF,DOKUMPOZ.NUMER,DOKUMPOZ.KTM,DOKUMPOZ.WERSJA,
                DOKUMPOZ.ILOSC,DOKUMPOZ.cenanet,DOKUMPOZ.cenabru
       from DOKUMPOZ
         left join towary on (towary.ktm = dokumpoz.ktm)
       where dokumpoz.dokument=:dokument and towary.usluga=1 and dokumpoz.opk>0
       into :pozdokum,:numer,:ktm,:wersja,
            :ilosc,:cenasnetto,:cenasbrutto
     do begin
       if (:opktryb = 1)then
         if(:bn = 'B') then
           execute procedure OPK_ADD(:slodef,:slopoz,:ktm,:wersja,:ilosc,0,:cenasbrutto,:pozdokum,:back);
         else
           execute procedure OPK_ADD(:slodef,:slopoz,:ktm,:wersja,:ilosc,0,:cenasnetto,:pozdokum,:back);
     end
   end
   else if (:opktryb = 2 ) then begin
     for select dpoz.ref, stopk.stan, dpoz.ilosc, dpoz.ktm, dpoz.wersja, dpoz.cena,  dpoz.cenabru, dpoz.cenanet      --jedna pozycja bo rozliczny konkretny stan
       from DOKUMPOZ dpoz
         left join dokumpoz dpozkoryg on dpozkoryg.ref = dpoz.kortopoz
         left join stanyopk stopk on stopk.pozdokum = dpozkoryg.ref
         left join towary on (towary.ktm = dpoz.ktm)
       where dpoz.dokument=:dokument and towary.usluga=1 and dpoz.opk>0
       into :opkref, :opkstan, :pozilosc, :ktm, :wersja, :cena, :cenasbrutto, :cenasnetto
     do begin
       if (:opkstan < :pozilosc and :ack <> 0) then exception STANYOPK_ROZLICZONE 'Ilość na dokumencie ('||:pozilosc ||') jest większa od rozliczanego stanu ('||:opkstan || '). Akceptacja niemożliwa.';
       if (:opkref is not null) then
       begin
         if(:bn = 'B') then
           execute procedure OPK_DEL(:slodef, :slopoz, :ktm, :wersja, :pozilosc, :cena, :cenasbrutto, :opkref, NULL, :back);
         else
           execute procedure OPK_DEL(:slodef, :slopoz, :ktm, :wersja, :pozilosc, :cena, :cenasnetto, :opkref, NULL, :back);
       end
     end
   end
 end
 /* wydawanie sprzetu podlegajacego serwisowi */
 if(:ack=1 or :ack=5) then begin
   if(:zewn=1 and :wydania=1) then begin
     -- petla for select po pozycjach ale tylko tych serwisowanych
     for select DOKUMPOZ.ref, TOWARY.KTM, TOWARY.srvdaysinterval
       from DOKUMPOZ
       left join TOWARY on (DOKUMPOZ.KTM=TOWARY.KTM)
       where DOKUMPOZ.dokument=:dokument and TOWARY.srv=1
       into  :pozref, :ktm, :srvdaysinterval
     do begin
       -- czy istnieje umowa serwisowa do danego klienta
       srvcontract = null;
       select first 1 srvcontracts.ref
         from srvcontracts
         where srvcontracts.dictpos = :slopoz and srvcontracts.dictdef = :slodef and srvcontracts.aktywny=1 and srvcontracts.oddzial=:dnoddzial
         order by srvcontracts.fromdate desc
         into :srvcontract;
       if(:srvcontract is null) then begin
         -- jesli nie istnieje to zakladamy nowa umowe serwisowa
         execute procedure GEN_REF('SRVCONTRACTS') returning_values :srvcontract;
         execute procedure slo_dane(:slodef, :slopoz) returning_values :kod, :name, :kontofk;
         insert into srvcontracts(REF,NAME,DICTDEF,DICTPOS,FROMDATE,AKTYWNY,ODDZIAL)
           values (:srvcontract,substring(:name from 1 for 60),:slodef,:slopoz,:data,1,:dnoddzial);
       end
       -- zakladamy wpis o sprzecie do biezacej umowy serwisowej
       insert into srvcdevices(SRVCONTRACT,KTM,PLSRVDATE,LASTCOUNTER,STARTDATE,DOKUMPOZREF)
         values (:srvcontract,:ktm,:data+:srvdaysinterval,0,:data,:pozref);
     end
   end
 end
 if(:ack=0 or :ack=9) then begin
   -- usuwanie z tabeli srvcdevies
   for select DOKUMPOZ.ref
     from DOKUMPOZ
     left join TOWARY on (DOKUMPOZ.KTM=TOWARY.KTM)
     where DOKUMPOZ.dokument=:dokument and TOWARY.srv=1
     into  :pozref
   do begin
     delete from srvcdevices where srvcdevices.dokumpozref = :pozref;
   end
 end

 if((:ack = 5 or (:ack = 1)) and :wydania = 0) then begin
   execute procedure DOKUMNAG_CONNECTDOKUMROZ(:dokument);
   -- jesli magazyn dopuszcza stany ujemne to niech dokument przychodowy sprobuje je usunac
   if(:stanyujemne>0) then execute procedure DOKUMNAG_USUN_UJEMNE(:dokument) returning_values :stat;
 end
 execute procedure DOKMAG_OBL_WAR(:dokument,1);
 if((:ack = 5 or (:ack = 1)) and :miedzywart=1) then begin
   execute procedure DOKUMNAG_CHECK_MIEDZYWART(:dokument) returning_values :stat;
 end
end^
SET TERM ; ^
