--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDEDOC_INSERT_EDEDOCH_REPLYDOC(
      EDEDOCHREF integer)
  returns (
      EDEDOCHOUT integer)
   as
declare variable OREF integer;
declare variable OTABLE STRING20;
declare variable EDERULE EDERULES_ID;
declare variable EDEDOCHTMP integer;
begin
  -- Funkcja dodawania naglowka odpowiedzi z webserwisów po eksoprcie dokumentów
  -- do użycia w drajwerach fizycznych implementujących konkretne formaty

  --Pobranie oref, otable i ederule z dokumentu eksportu dla którego generowany jest dokument importu
  if (coalesce( ededochref, 0) <> 0 ) then begin
    select h.ref, h.oref,  h.otable, h.ederule
      from ededocsh h
      where h.ref = :ededochref
    into :ededochtmp, :oref, :otable, :ederule;

    if (ededochtmp is null) then
      exception universal 'Nie znaleziono dokumentu eksportu: '||:ededochref;

    execute procedure gen_ref('EDEDOCSH') returning_values :ededochout;
    insert into ededocsh (ref, OREF, OTABLE, EDERULE, DIRECTION, MANUALINIT, STATUS)
      values (:ededochout, :oref, :otable, :ederule, 0, 0, 3);
    update ededocsh set replyededoc = :ededochout where ref = :ededochref;
  end else begin
    exception universal 'nie przekazano referencji do dokumentu eksoprtu';
  end
end^
SET TERM ; ^
