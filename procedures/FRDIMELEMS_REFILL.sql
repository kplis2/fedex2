--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRDIMELEMS_REFILL as
declare variable frdimshier integer;
declare variable dimelem integer;
declare variable act smallint;
declare variable frhdr varchar(40);
begin
  for
    select fh.ref, d.ref, d.act, fd.symbol
      from dimhierdef h
        join frdimhier fh on (fh.dimhierdef = h.ref)
        join dimelemdef d on (h.ref = d.dimhier)
        left join frhdrs fd on (fh.frhdr = fd.symbol)
        left join frdimelems fe on (fe.frdimhier = fh.ref and fe.dimelem = d.ref)
      where fe.ref is null and fd.status = 1 and d.act > 0 and fd.status = 1
      into frdimshier, dimelem, act, frhdr
  do begin
    insert into frdimelems (frdimhier, dimelem, act)
      values (:frdimshier, :dimelem, :act);
    update frhdrs f set f.autorefilled = 1 where f.symbol = :frhdr and coalesce(f.autorefilled,0) = 0;
  end
end^
SET TERM ; ^
