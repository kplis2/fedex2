--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_MCOL(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      COL integer)
  returns (
      RET numeric(14,2))
   as
declare variable mper char(6);
  declare variable payday timestamp;
begin
  --DU: personel - funkcja podaje wartosc skladnika z innych list plac
  --z tego samego okresu
  select cper, payday
    from epayrolls
    where ref = :payroll
    into :mper, :payday;
 
  select sum(P.pvalue)
    from eprpos P
      join epayrolls R on (P.payroll = R.ref)
    where P.employee = :employee and P.payroll <> :payroll and R.cper = :mper
      and R.empltype = 1 and P.ecolumn = :col
      and (R.payday < :payday or (R.payday = :payday and R.ref < :payroll))
    into :ret;
  if (ret is null) then
    ret = 0;
  suspend;
end^
SET TERM ; ^
