--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NWW(
      A integer,
      B integer)
  returns (
      RET integer)
   as
begin
  --DS: obliczanie najmniejszej wspolnej wielokrotnosci
  select ret from nwd(:a,:b) into :ret;
  ret = (a * b)/ret;
  suspend;
end^
SET TERM ; ^
