--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMSER_ADD_REZ(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      NRSERYJNY varchar(40) CHARACTER SET UTF8                           ,
      ILOSC numeric(14,4))
   as
declare variable stancen integer;
declare variable stnr varchar(40);
declare variable stanil numeric(14,4);
declare variable zablil numeric(14,4);
declare variable ilblok numeric(14,4);
begin
  for select s.stancen, s.serialnr, s.ilosc, s.zablokow
    from stanyser s
    join stanycen c on(s.stancen = c.ref)
      where s.serialnr = :nrseryjny and c.magazyn = :magazyn
        and c.wersjaref = :wersjaref and s.ilosc>s.zablokow
      order by  s.data , s.stancen
    into :stancen, :stnr, :stanil, :zablil
  do begin
    if (stanil is null) then stanil = 0;
    if (zablil is null) then zablil = 0;
    if (ilosc is null) then ilosc = 0;
    ilblok = :stanil - :zablil;
    if(:ilosc<:ilblok) then ilblok = :ilosc;
    if(:ilblok>0) then begin
      update stanyser s
        set s.zablokow = s.zablokow + :ilblok
        where s.stancen = :stancen and s.serialnr = :stnr;
      ilosc = :ilosc - :ilblok;
    end
  end
  if (:ilosc>0) then
    exception STANYSER_MINUS 'Brak towaru z serii do rezerwacji w ilosci '||:ilosc;
end^
SET TERM ; ^
