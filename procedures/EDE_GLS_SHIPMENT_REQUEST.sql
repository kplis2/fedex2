--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_GLS_SHIPMENT_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable SHIPPINGDOC integer;
declare variable ROOTPARENT integer;
declare variable GRANDPARENT integer;
declare variable SUBPARENT integer;
declare variable SECONDSUBPARENT integer;
declare variable SHIPPINGCOMMENTS varchar(200);
declare variable DELIVERYTYPE integer;
declare variable SHIPPFROMPLACE varchar(20);
declare variable SHIPPERFNAME varchar(60);
declare variable SHIPPERSNAME varchar(60);
declare variable SHIPPERCOMPANYNAME varchar(255);
declare variable SHIPPERPHONE varchar(255);
declare variable SHIPPEREMAIL varchar(255);
declare variable RECIPIENTREF integer;
declare variable RECIPIENTNAME varchar(255);
declare variable RECIPIENTNAMETMP varchar(255);
declare variable RECIPIENTISCOMPANY smallint;
declare variable RECIPIENTTAXID NIP;
declare variable RECIPIENTFNAME varchar(60);
declare variable RECIPIENTFNAMETMP varchar(60);
declare variable RECIPIENTSNAME varchar(60);
declare variable RECIPIENTSNAMETMP varchar(60);
declare variable RECIPIENTCITY varchar(255);
declare variable RECIPIENTCITYTMP varchar(255);
declare variable RECIPIENTPOSTCODE POSTCODE;
declare variable RECIPIENTPOSTCODETMP POSTCODE;
declare variable RECIPIENTCOUNTRY COUNTRY_ID;
declare variable RECIPIENTCOUNTRYTMP COUNTRY_ID;
declare variable RECIPIENTSTREET STREET_ID;
declare variable RECIPIENTSTREETTMP STREET_ID;
declare variable RECIPIENTHOUSENO NRDOMU_ID;
declare variable RECIPIENTHOUSENOTMP NRDOMU_ID;
declare variable RECIPIENTLOCALNO NRDOMU_ID;
declare variable RECIPIENTLOCALNOTMP NRDOMU_ID;
declare variable RECIPIENTPHONE PHONES_ID;
declare variable RECIPIENTEMAIL EMAILWZOR_ID;
declare variable CODAMOUNT CENY;
declare variable INSURANCEAMOUNT CENY;
declare variable PACKAGEREF integer;
declare variable PACKAGETYPE varchar(10);
declare variable PACKAGEWEIGHT WAGA_ID;
declare variable PACKAGELENGTH WAGA_ID;
declare variable PACKAGEWIDTH WAGA_ID;
declare variable PACKAGEHEIGHT WAGA_ID;
declare variable USERNAME STRING40;
declare variable USERPASSWORD STRING40;
declare variable FILEPATH STRING255;
declare variable COURIERNO STRING30;
declare variable FEDEXNO STRING20;
declare variable SERVICETYPE STRING20;
declare variable REFERENCE STRING20;
declare variable PACKAGESCOUNT INTEGER_ID;
declare variable PACKAGESWEIGHT WAGA_ID;
declare variable SHIPPINGFLAGS STRING40;
declare variable czypobranie smallint_id; --[PM] XXX PR121869
begin
  --typ serwisu
  --jesli chcesz testowac ustaw na TEST, dla produkcyjnego ustaw PRODUCTION
  servicetype = 'TEST';

  select l.ref, substring(l.uwagisped from 1 for 200), l.sposdost, l.fromplace, l.odbiorcaid,
      trim(l.kontrahent), 0, '',
      trim(l.imie), trim(l.nazwisko),
      trim(l.miasto), trim(l.kodp), trim(l.krajid),
      trim(l.adres), trim(l.nrdomu), trim(l.nrlokalu),
      l.pobranie,
      l.flagisped,
      coalesce(p.pobranie,0) --[PM] XXX PR121869
    from listywysd l
      left join platnosci p on l.sposzap = p.ref --[PM] XXX PR121869
    where l.ref = :oref
  into :shippingdoc, :shippingcomments, :deliverytype, :shippfromplace, :recipientref,
    :recipientname, :recipientiscompany, :recipienttaxid,
    :recipientfname, :recipientsname,
    :recipientcity, :recipientpostcode, :recipientcountry,
    :recipientstreet, :recipienthouseno, :recipientlocalno,
    :codamount,
    :shippingflags,
    :czypobranie; --[PM] XXX PR121869

  --sprawdzanie czy istieje dokument spedycyjny
  if(shippingdoc is null) then exception EDE_LISTYWYSD_BRAK;


  if (czypobranie = 1 and coalesce(codamount,0) <= 0) then --[PM] XXX PR121869
    exception universal 'Pobranie BEZ KWOTY !!!'; --[PM] XXX PR121869

  val = null;
  parent = null;
  name = 'adePreparingBox_Insert';
  if (:servicetype = 'PRODUCTION') then
    params = 'xmlns="https://adeplus.gls-poland.com/adeplus/pm1/ade_webapi2.php?wsdl"';
  else
    params = 'xmlns="https://ade-test.gls-poland.com/adeplus/pm1/ade_webapi2.php?wsdl"';
  id = 0;
  suspend;

  rootparent = id;

  --dane autoryzacyjne
  select k.login, k.haslo, k.sciezkapliku
    from spedytwys w
      left join spedytkonta k on (w.spedytor = k.spedytor)
    where w.sposdost = :deliverytype
  into :username, :userpassword, :filepath;

  val = :username;
  name = 'username';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = :userpassword;
  name = 'password';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = :filepath;
  name = 'filepath';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = :servicetype;
  name = 'serviceType';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = 'roll_160x100_zebra_epl';
  name = 'labelName';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = 'EPL';
  name = 'labelType';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = null;
  name = 'consign_prep_data';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  grandparent = id;

  if (:recipientref is not null) then
  begin
    select o.nazwa, k.firma, k.nip,
        o.imie, o.nazwisko,
        o.dmiasto, o.dkodp, o.krajid,
        o.dulica, o.dnrdomu, o.dnrlokalu,
        coalesce(o.dtelefon, k.telefon, k.komorka), coalesce(o.email, k.email) --[PM] XXX coalesce
      from odbiorcy o
        join klienci k on (o.klient = k.ref)
      where o.ref = :recipientref
    into
      :recipientnametmp, :recipientiscompany, :recipienttaxid,
      :recipientfnametmp, :recipientsnametmp,
      :recipientcitytmp, :recipientpostcodetmp, :recipientcountrytmp,
      :recipientstreettmp, :recipienthousenotmp, :recipientlocalnotmp,
      :recipientphone, :recipientemail;
  end

  --nazwa odbiorcy
  if (coalesce(:recipientname,'') = '') then
    recipientname = :recipientnametmp;
  if (coalesce(:recipientname,'') = '') then
    exception EDE_LISTYWYSD_BRAK 'Brak nazwy odbiorcy';
  val = substring(:recipientname from  1 for  40);
  name = 'rname1';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = substring(:recipientname from  41 for  80);
  name = 'rname2';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = substring(:recipientname from  81 for  120);
  name = 'rname3';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  --kod kraju odbiorcy
  if (coalesce(:recipientcountry,'') = '') then
    recipientcountry = :recipientcountrytmp;
  if (coalesce(:recipientcountry,'') = '') then
    recipientcountry = 'PL';
--    exception ede_listywysd_brak 'Brak kraju odbiorcy';
  val = substring(:recipientcountry from 1 for 2);
  name = 'rcountry';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  --kod pocztowy odbiorcy
  if (coalesce(:recipientpostcode,'') = '') then
    recipientpostcode = :recipientpostcodetmp;
  if (coalesce(:recipientpostcode,'') = '') then
    exception ede_listywysd_brak 'Brak kodu pocztowego odbiorcy';
  val = :recipientpostcode;
  name = 'rzipcode';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  --miasto odbiorcy
  if (coalesce(:recipientcity,'') = '') then
    recipientcity = :recipientcitytmp;
  if (coalesce(:recipientcity,'') = '') then
    exception ede_listywysd_brak 'Brak miasta odbiorcy';
  val = substring(:recipientcity from 1 for 30);
  name = 'rcity';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  --ulica odbiorcy
  if (coalesce(:recipientstreet,'') = '') then
    recipientstreet = :recipientstreettmp;
  if (coalesce(:recipientstreet,'') = '') then
    exception ede_listywysd_brak 'Brak ulicy odbiorcy';

  if (coalesce(:recipienthouseno,'') = '') then
    recipienthouseno = coalesce(:recipienthousenotmp,'');
  if (coalesce(:recipientlocalno,'') = '') then
    recipientlocalno = coalesce(:recipientlocalnotmp,'');

  recipientstreet = :recipientstreet||' '||:recipienthouseno||iif(coalesce(:recipientlocalno,'') <> '','/'||:recipientlocalno, '');
  val = substring(:recipientstreet from 1 for 50);
  name = 'rstreet';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  --nr telefonu odbiorcy
  val = substring(:recipientphone from 1 for 20);
  name = 'rphone';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  --nr telefonu odbiorcy
  val = substring(:recipientemail from 1 for 40);
  name = 'rcontact';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  --referencje - opis zawartosci
  select first 1 substring(n.symbol from 1 for 25) from nagfak n where n.listywysd = :shippingdoc
  into :reference;
  if (:reference is null) then
    select first 1 n.symbol
      from listywysdpoz p
        left join dokumnag n on (p.dokref = n.ref)
      where p.dokument = :shippingdoc
      group by n.symbol
    into :reference;

  if (coalesce(:reference,'') <> '') then
  begin
    val = substring(:reference from 1 for 25);
    name = 'references';
    params = null;
    parent = grandparent;
    id = id + 1;
    suspend;
  end

  val = substring(:shippingcomments from 1 for 40);
  name = 'notes';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  select count(o.ref), sum(o.waga)
    from listywysdroz_opk o
    where o.listwysd = :shippingdoc
      and o.rodzic is null
  into :packagescount, :packagesweight;

  val = :packagescount;
  name = 'quantity';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = :packagesweight;
  name = 'weight';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = extract(year from current_date)||'-'||
        iif(extract(month from current_date) < 10, '0'||extract(month from current_date), extract(month from current_date))||'-'||
        iif(extract(day from current_date) < 10, '0'||extract(day from current_date), extract(day from current_date));
  name = 'date';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  /* wezel serwisow */
  val = null;
  name = 'srv_bool';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  subparent = :id;

  if (coalesce(:codamount,0) > 0) then
  begin
    val = '1';
    name = 'cod';
    params = null;
    parent = subparent;
    id = id + 1;
    suspend;

    val = :codamount;
    name = 'cod_amount';
    params = null;
    parent = subparent;
    id = id + 1;
    suspend;
  end

  /* flagi spedycyjne */
  /*$$IBEC$$ if (coalesce(:shippingflags,';') <> ';') then
  begin
    --dostawa do 10
    if (strmulticmp(';D;',:shippingflags) > 0) then --wybrac flage
    begin
      val = '1';
      name = 's10';
      params = null;
      parent = subparent;
      id = id + 1;
      suspend;
    end

    --dostawa do 12
    if (strmulticmp(';W;',:shippingflags) > 0) then --wybrac flage
    begin
      val = '1';
      name = 's10';
      params = null;
      parent = subparent;
      id = id + 1;
      suspend;
    end

    --dostawa w sobote
    if (strmulticmp(';S;',:shippingflags) > 0) then --wybrac flage
    begin
      val = '1';
      name = 'sat';
      params = null;
      parent = subparent;
      id = id + 1;
      suspend;
    end
  end $$IBEC$$*/

/* wezel opakowan */
  val = null;
  name = 'parcels';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  subparent = :id;

  /* wezly paczek, dla kazdej z paczki osobno */
  for
    select o.ref, t.symbol, o.waga, o.dlugosc, o.szerokosc, o.wysokosc
      from listywysdroz_opk o
        left join typyopk t on (o.typopk = t.ref)
      where o.listwysd = :shippingdoc
        and o.rodzic is null
    into :packageref, :packagetype, :packageweight, :packagelength, :packagewidth, :packageheight
  do begin
    val = null;
    name = 'items';
    params = null;
    parent = subparent;
    id = id + 1;
    suspend;
  
    secondsubparent = :id;

    --ref paczki
    val = :packageref;
    name = 'reference';
    params = null;
    parent = secondsubparent;
    id = id + 1;
    suspend;

    --waga paczki
    val = :packageweight;
    name = 'weight';
    params = null;
    parent = secondsubparent;
    id = id + 1;
    suspend;
  end
  /*koniec osobnych wezlow paczek */
/* koniec wezla opakowan */
end^
SET TERM ; ^
