--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SP_EMPLWORKEDHOURS(
      EMPLOYEE integer,
      CALENDAR integer,
      FROMPERIOD varchar(6) CHARACTER SET UTF8                           ,
      TOPERIOD varchar(6) CHARACTER SET UTF8                           ,
      FROMDATE_IN varchar(10) CHARACTER SET UTF8                           ,
      TODATE_IN varchar(10) CHARACTER SET UTF8                           )
  returns (
      CDATE date,
      DAYKIND smallint,
      CALENDAR_NAME varchar(20) CHARACTER SET UTF8                           ,
      NOMINAL_HOURS_MIN integer,
      NOMINAL_HOURS varchar(8) CHARACTER SET UTF8                           ,
      WORKED_HOURS_MIN integer,
      WORKED_HOURS varchar(8) CHARACTER SET UTF8                           ,
      SHORTAGE_HOURS_MIN integer,
      SHORTAGE_HOURS varchar(8) CHARACTER SET UTF8                           ,
      TAKE_AWAY_HOURS_MIN integer,
      TAKE_AWAY_HOURS varchar(8) CHARACTER SET UTF8                           ,
      COMPENSATED_TIME_MIN integer,
      COMPENSATED_TIME varchar(8) CHARACTER SET UTF8                           ,
      OVERTIME0_MIN integer,
      OVERTIME0_HOURS varchar(8) CHARACTER SET UTF8                           ,
      OVERTIME50_MIN integer,
      OVERTIME50_HOURS varchar(8) CHARACTER SET UTF8                           ,
      OVERTIME100_MIN integer,
      OVERTIME100_HOURS varchar(8) CHARACTER SET UTF8                           )
   as
declare variable FROMDATE date;
declare variable TODATE date;
begin
  --DS: wykazanie godzin do odbioru, odebranych, nadgodzin itp dla danego praownika
  if (calendar = 0) then calendar = null;
  if (coalesce(char_length(fromperiod),0) = 6) then -- [DG] XXX ZG119346
  begin
    select fromdate from period2dates(:fromperiod) into :fromdate;
    select todate from period2dates(:toperiod) into :todate;
  end else begin
    fromdate = cast(fromdate_in as date);
    todate = cast(todate_in as date);
  end

  for
    select ecd.cdate, edk.daykind, c.name, ecd.worktime, ecd.nomworktime,
        --braki
        case when ecd.compensatory_time - coalesce(ecd.compensated_time,0) < 0 then - ecd.compensatory_time + coalesce(ecd.compensated_time,0) else 0 end,
        --nadwyzka
        case when ecd.compensatory_time - coalesce(ecd.compensated_time,0) > 0 then ecd.compensatory_time - coalesce(ecd.compensated_time,0) else 0 end,
        ecd.overtime0, ecd.overtime50, ecd.overtime100, ecd.compensated_time
      from ecalendars c
        join ecaldays cd on cd.calendar = c.ref
        join emplcaldays ecd on ecd.ecalday = cd.ref
        join employees e on e.ref = ecd.employee
        join edaykinds edk on edk.ref = cd.daykind
      where ecd.cdate >= :fromdate
        and ecd.cdate <= :todate
        and (ecd.ecalendar = :calendar or :calendar is null)
        and e.ref = :employee
      order by ecd.cdate
      into :cdate, :daykind,  :calendar_name, :worked_hours_min, :nominal_hours_min,
        :shortage_hours_min, :take_away_hours_min,
        :overtime0_min, :overtime50_min, :overtime100_min, :compensated_time_min
  do begin
    execute procedure durationstr2(worked_hours_min) returning_values worked_hours;
    execute procedure durationstr2(nominal_hours_min) returning_values nominal_hours;
    execute procedure durationstr2(shortage_hours_min) returning_values shortage_hours;
    execute procedure durationstr2(take_away_hours_min) returning_values take_away_hours;
    execute procedure durationstr2(compensated_time_min) returning_values compensated_time;
    execute procedure durationstr2(overtime0_min) returning_values overtime0_hours;
    execute procedure durationstr2(overtime50_min) returning_values overtime50_hours;
    execute procedure durationstr2(overtime100_min) returning_values overtime100_hours;
    suspend;
  end
end^
SET TERM ; ^
