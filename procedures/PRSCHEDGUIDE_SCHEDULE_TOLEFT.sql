--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDE_SCHEDULE_TOLEFT(
      PRSCHEDGUIDE integer)
   as
declare variable curtime timestamp;
declare variable tstart timestamp;
declare variable tdepend timestamp;
declare variable tend timestamp;
declare variable machine integer;
declare variable prdepart varchar(20);
declare variable minspace integer;
declare variable round integer;
declare variable minlength double precision;
declare variable tstartfwt timestamp;
declare variable worktime double precision;
declare variable schedoper integer;
declare variable prschedule integer;
declare variable prganttref bigint;
declare variable rprganttref bigint;
declare variable lastganttref bigint;
declare variable opercount integer;
declare variable lastoper integer;
declare variable cnt integer;
declare variable verifiednum integer;
declare variable rschedoper integer;
declare variable depfrom integer;
declare variable minlatenes double precision;
declare variable brakmat smallint;
declare variable status smallint;
declare variable shoper integer;
declare variable matreadytime timestamp;
declare variable tprevend timestamp;
declare variable workplace varchar(20);
declare variable latenes double precision;
declare variable rstart timestamp;
declare variable rend timestamp;
declare variable rmachine integer;
declare variable rstartfwt timestamp;
declare variable c integer;
declare variable rthoper integer;
begin
  cnt = 0;
  opercount = 500;
  select g.prschedule
    from prschedguides g
    where g.ref = :prschedguide
    into prschedule;
  select PRSCHEDULES.mintimebetweenopers, prschedules.roundstarttime,  prschedules.fromdate, prschedules.prdepart
      from prschedules
      where prschedules.ref = :prschedule
      into :minspace, :round, :curtime, :prdepart;
  execute procedure PRGANTT_CALC (:prschedule, :prschedguide);
  update prgantt g set g.timefrom = null, g.timeto = null, g.prmachine = null,
      g.readytosched = 0, g.verified = 0
    where g.prschedguide = :prschedguide and g.timefrom >= current_timestamp(0);
  minlength = 1440;
  minlength = 1/:minlength;
  if(:round > :minspace or (:minspace is null) ) then minspace = coalesce(:round,0);
  minlength = :minlength * :minspace;
  verifiednum = 0;
  lastganttref = 0;
  for
    select a.ref
      from prgantt A
        left join prgantt B on (b.ref = a.depfrom and b.status < 3)
      where A.PRSCHEDULE = :prschedule and a.prschedguide = :prschedguide and A.VERIFIED = 0 and b.ref is null
      into :prganttref
  do begin
    update prgantt set readytosched = 1 where ref = :prganttref;
  end
  if(:curtime is null or (:curtime < current_timestamp(0)) ) then curtime = current_timestamp(0);
  while(cnt < opercount and lastganttref >= 0)
  do begin
    cnt = cnt + 1;
    verifiednum = :verifiednum + 1;
    lastganttref = - 1;
    prganttref = null;
    for
      select first 1 a.ref
        from prgantt A
        where A.PRSCHEDULE = :prschedule and a.prschedguide = :prschedguide and A.VERIFIED = 0
          and a.readytosched = 1
        order by coalesce(a.priority,0) desc,
          A.prschedule, A.verified,
          A.status desc
      into :prganttref
    do begin
      execute procedure PRSCHEDULE_CHECKMATERIALSTATE(:prganttref);
    end
    if (prganttref > 0) then
      lastganttref = prganttref;
    schedoper = null;
    rschedoper = null;
    minlatenes = null;
    --dla kazdejoperacji okrel czas "opoznienia" startu w stosunku do miejsca, w ktorym mozna posadzic operacje
    if(exists (select ref from prgantt where PRSCHEDULE = :prschedule and prschedguide = :prschedguide and VERIFIED = 0 and readytosched = 1 and materialreadytime is not null)) then
       brakmat = 1;
    else
       brakmat = 0;
    for
      select a.ref, a.depfrom, a.timefrom, a.timeto, a.STATUS, a.prMACHINE, a.prshoper, a.materialreadytime, a.workplace, a.worktime, a.prschedoper
        from PRGANTT A
      where A.PRSCHEDULE = :prschedule and a.prschedguide = :prschedguide and A.VERIFIED = 0 and a.readytosched = 1
            and (a.materialreadytime is not null or a.verified=:brakmat)
      order by A.prschedule, A.verified, a.readytosched, a.priority desc
      into :prganttref, depfrom, :tstart,  :tend, :status, :machine, :shoper, :matreadytime, :workplace, worktime, schedoper
    do begin
      if(:minlatenes < :minlength) then
        break;  -- jesli ostatni odstep jest wystarczajaco maly, to nei szukam dalej
      tdepend = null;
      --okreslenie minimalnego czasu rozpoczecia
      select max(f.timeto)
        from prgantt p
          left join prschedoperdeps d on (d.depto = p.prschedoper)
          left join prgantt f on (f.prschedoper = d.depfrom)
        where p.prschedoper = :schedoper
        into :tdepend;
      if(:tdepend is null) then tdepend = :curtime;
      if(:tstart is null or :tstart < :tdepend )then
        tstart = tdepend;
      --sprawdzenie zaleznosci materialowej
      if(:matreadytime > :tstart) then
        tstart  = :matreadytime;
      --sprawdzenie, czy start nie wypad w czasie przerwy pracy - jesli tak, to na poczatek kolejnego czasu
      execute procedure PRSCHED_CALENDAR_FIRSTWORKTIME(:prdepart,:tstart) returning_values
         :tstartfwt;
      if(:workplace <> '') then
      begin
        execute procedure prschedule_find_machine(:prganttref,:tstartfwt, 0) returning_values :tstart,  :tend, :machine;
        if(:machine is null) then
        begin
          execute procedure prschedule_checkoperstarttime(:prganttref, :tstartfwt) returning_values :tstart;
          execute procedure pr_calendar_checkendtime(:prdepart, :tstart, :worktime) returning_values :tend;
        end
        --sprawdzenie opoznienia w stosunku do poprzedzajacej oepracji na danej maszynie
        tprevend = null;
        select max (timeto)
        from prgantt
        where prschedule = :prschedule
          and prmachine = :machine
          and timeto <= :tstart
          and ref <> :prganttref
        into :tprevend;
        if(:tprevend < :curtime) then
           tprevend = null;--jesli poprzednie operacje już si zakonczyly/powinny byly zakonczyc, to tak, jakby ich nie bylo
        if(:tprevend is null) then latenes = 0;
        else latenes = :tstart - :tprevend;
        if(:minlatenes is null or (minlatenes > :latenes))then
        begin
          rschedoper = schedoper;
          rprganttref = prganttref;
          rmachine = :machine;
          rstart = :tstart;
          rend = :tend;
          minlatenes = :latenes;
          rstartfwt = :tstartfwt;
        end
      end else begin
        machine = null;
        execute procedure prschedule_checkoperstarttime(:prganttref, :tstart) returning_values :tstart;
        execute procedure pr_calendar_checkendtime(:prdepart, :tstart, :worktime) returning_values :tend;
        rschedoper = schedoper;
        rprganttref = prganttref;
        rmachine = machine;
        rstart = :tstart;
        rstartfwt = :tstart;
        rend = :tend;
        minlatenes = 0;
      end
      c = :c + 1;
    end
    if(:rschedoper is null) then
    begin
      -- zakończenie harmonogramowania operacji
      prganttref = -1;
      exit;
    end
    update prgantt set timefrom = :rstart, timeto = :rend, prMACHINE = :rmachine, verified  = :verifiednum
      where ref=:rprganttref;
    --ustawianie readytoharm na innych
    for
      select o.ref
        from prschedoperdeps d
          left join prschedopers o on (o.ref = d.depto)
        where d.depfrom = :rschedoper and o.activ = 1
        into :rthoper
    do begin
      execute procedure prschedule_readytosched_check(:rthoper);
    end
    schedoper = rschedoper;
  end
end^
SET TERM ; ^
