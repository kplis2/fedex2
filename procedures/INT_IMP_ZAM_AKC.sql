--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_ZAM_AKC(
      REF INTEGER_ID)
  returns (
      MSG STRING255,
      STATUS SMALLINT_ID)
   as
declare variable grupasped integer_id;
declare variable DOKPOZ DOKUMPOZ_ID;
declare variable DOKNAG DOKUMNAG_ID;
declare variable DOKNAGRW DOKUMNAG_ID;
declare variable DOKNAGPW DOKUMNAG_ID;
declare variable Z_NRWERSJI NRWERSJI_ID;
declare variable tmpktm ktm_id;
declare variable kplwersja wersje_id;
begin
 --EHRLE MKD automatyczna akceptacja wz podczas importu

        status = 1;
        msg = '';
        --sprawdzenie czy istnieje zamowienie na myjnie w kilku czesciach ale bez slownika investycji
        --jezeli tak zmiana pozycji na odpowiednia wersje i zaznaczenie havefake tak aby wygenerowaly sie z triggera pozycje kompletu
        for
            select p.ktm, p.ref
                from dokumpoz p
                where   p.dokument = :ref and
                        coalesce(p.havefake,0) = 0 and
                        coalesce(p.fake, 0) = 0 and
                        coalesce(p.x_slownik,0) = 0
        into :tmpktm, :dokpoz
        do begin
            select first 1 kn.wersjaref
            FROM MWSSTOCK
                JOIN MWSPALLOCS AA1 ON ( MWSSTOCK.MWSPALLOC = AA1.REF )
                LEFT JOIN MWSCONSTLOCS AA0 ON ( MWSSTOCK.MWSCONSTLOC = AA0.REF )
                LEFT JOIN TOWARY AA2 ON ( MWSSTOCK.GOOD = AA2.KTM )
                LEFT JOIN X_MWS_SERIE AA3 ON ( MWSSTOCK.X_SERIAL_NO = AA3.REF )
                JOIN kplpoz kp ON (kp.wersjaref = MWSSTOCK.VERS)
                JOIN kplnag kn ON (kp.nagkpl = kn.ref)
                WHERE MWSSTOCK.WH = 'MWS' and coalesce(MWSSTOCK.x_slownik,0) = 0
                        and mwsstock.quantity > 0 and  kn.ktm = :tmpktm
            into :kplwersja;
            
            if (kplwersja is not null) then begin
                update dokumpoz dp set  dp.wersjaref = :kplwersja, havefake = 1 where dp.ref = :dokpoz;
            end
            if (exists(
                        select kp.ref from kplpoz kp
                            join kplnag kn on (kp.nagkpl = kn.ref)
                            where kn.ktm = :tmpktm
                            )
                and kplwersja is null) then begin
                status = 0;
                msg = msg || 'Brak kąpletu nieprzypisanego do żadnego słownika.';
            end
        end
        --sprawdzam czy na dokumnagu sa jakies pozycje oznaczone do przeprogramowania
        -- jezeli tak to przeprogramowuje
        for
            select w.nrwersji ,p.ref
                from dokumpoz p
                join wersje w on (p.x_fromprogram = w.ref)
                where p.dokument = :ref and x_fromprogram is not null
        into :z_nrwersji, :dokpoz
        do begin
            --msg = 'nrwersji'||:z_nrwersji||'dokpoz'||:dokpoz;
            select docidrw, docidpw, msg, status from X_IMP_PRZEPROGRAMUJ(:dokpoz,:z_nrwersji,0)
                into :doknagrw,  :doknagpw, :msg, :status;
            --msg = msg ||' RW'|| coalesce(doknagrw,'-') ||' PW'|| coalesce(doknagpw,'-');
            if (doknagrw is null or doknagpw is null or status = 0) then
            begin
                msg = msg || ' Blad przeprogramowania:' || msg;
                status = 0;
                delete from dokumnag where ref = :ref;
                delete from dokumnag where ref = :doknagrw;
                delete from dokumnag where ref = :doknagpw;

            end
            dokpoz = null;
            z_nrwersji = null;
           --exception universal 'llll';

        end
        if (status = 0) then
        begin
            suspend;
            exit;
        end
        update dokumnag d set d.akcept =1 where d.ref = :ref;
        select first 1 grupasped from xk_dokumnag_grupasped_list(:ref) into :grupasped;
        if (coalesce(grupasped,0) > 0) then
            update dokumnag d set d.grupasped = :grupasped where d.ref = :ref;
        when any do
        begin
                status = 0;
               msg = msg || 'Blad przy zatwierdzaniu WZ';
               -- select substring(list(mcs.mon$object_name||' line '||mcs.mon$source_line,'-') from 1 for 255) from MON$CALL_STACK mcs into :msg;
               --delete from dokumnag where ref = :ref;
               --delete from dokumnag where ref = :doknagrw;
               --delete from dokumnag where ref = :doknagpw;
               suspend;
        end


end^
SET TERM ; ^
