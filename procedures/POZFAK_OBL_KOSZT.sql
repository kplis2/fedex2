--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZFAK_OBL_KOSZT(
      POZFAKREF integer)
   as
declare variable koszt numeric(14,2);
declare variable kosztm numeric(14,2);
declare variable pkoszt numeric(14,2);
declare variable pkosztm numeric(14,2);
declare variable wart_koszt numeric(14,2);
declare variable wart_kosztm numeric(14,2);
declare variable local integer;
declare variable fakref integer;
begin
  select dokument from POZFAK where REF = :pozfakref into :fakref;
  execute procedure CHECK_LOCAL('NAGFAK',:fakref) returning_values :local;
  if(:local = 1 and :fakref > 0) then begin
     select sum(DOKUMPOZ.wartosc),sum(DOKUMPOZ.pwartosc),sum(DOKUMPOZ.wart_koszt)
     from DOKUMPOZ left join DOKUMNAG on (DOKUMNAG.ref = DOKUMPOZ.DOKUMENT)
     left join DEFDOKUM on ( DOKUMNAG.TYp = DEFDOKUM.SYMBOL)
     where DOKUMPOZ.FROMPOZFAK = :pozfakref  and DEFDOKUM.wydania = 1
     into :koszt, :pkoszt, :wart_koszt;
     if(:koszt is null) then koszt = 0;
     if(:pkoszt is null) then pkoszt = 0;
     if(:wart_koszt is null) then wart_koszt = 0;
     select sum(DOKUMPOZ.wartosc),sum(DOKUMPOZ.pwartosc),sum(DOKUMPOZ.wart_koszt)
     from DOKUMPOZ left join DOKUMNAG on (DOKUMNAG.ref = DOKUMPOZ.DOKUMENT)
     left join DEFDOKUM on ( DOKUMNAG.TYp = DEFDOKUM.SYMBOL)
     where DOKUMPOZ.FROMPOZFAK = :pozfakref  and DEFDOKUM.wydania = 0 and DOKUMPOZ.OPK = 0
     into :kosztm, :pkosztm, :wart_kosztm;
     if(:kosztm is null) then kosztm = 0;
     if(:pkosztm is null) then pkosztm = 0;
     if(:wart_kosztm is null) then wart_kosztm = 0;
     koszt = :koszt - :kosztm;
     pkoszt = :pkoszt - :pkosztm;
     wart_koszt = :wart_koszt - :wart_kosztm;
     update POZFAK set KOSZTZAK = :koszt, PKOSZTZAK = :pkoszt, WART_KOSZT = :wart_koszt
     where REF=:pozfakref and ((KOSZTZAK <> :koszt) or (kosztZAK is null) or (PKOSZTZAK <> :pkoszt) or (PKOSZTZAK is null) or (WART_KOSZT<>:wart_koszt) or (WART_KOSZT is null));
  end
end^
SET TERM ; ^
