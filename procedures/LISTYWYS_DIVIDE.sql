--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYS_DIVIDE(
      ORGREF integer)
  returns (
      STATUS integer,
      LSYMBOL varchar(20) CHARACTER SET UTF8                           )
   as
declare variable newlistywysref integer;
declare variable newlistywysdref integer;
declare variable czyzmianailosci integer;
declare variable refld integer;
declare variable refldp integer;
declare variable dokum integer;
declare variable typdok varchar(1);
declare variable oldilosc integer;
declare variable oldilosco integer;
declare variable oldiloscdorozb integer;
declare variable oldref integer;
declare variable olddokpoz integer;
declare variable oldpozzam integer;
declare variable newilosc integer;
declare variable newilosco integer;
declare variable newiloscdorozb integer;
begin
  newlistywysref = 0;
  status = 0;
  for
    select ld.ref,ld.typ
    from listywysd ld
    where ld.listawys = :orgref
    into :refld, :typdok
  do begin
    newlistywysdref = null;
    for
      select ldp.ref,ldp.dokref,ldp.iloscdorozb      --case when (ldp.dokpoz is null) then ldp.pozzam else ldp.dokpoz end
      from listywysdpoz ldp
      where ldp.dokument = :refld
      into :refldp, :dokum,:czyzmianailosci
    do begin
      if (czyzmianailosci <> 0 and czyzmianailosci is not null) then begin
        -- zakladanie nowego dokumentu spedycyjnego ile wczesniej nie zostala zalozona
        if (:newlistywysref = 0) then begin
          execute procedure GEN_REF('LISTYWYS') returning_values :newlistywysref;
          insert into listywys (ref, dataotw, dataplwys, stan, typ, spedytor, trasa, sposzapsped)
          select :newlistywysref, l.dataotw, l.dataplwys, l.stan, l.typ, l.spedytor, l.trasa, l.sposzapsped
          from listywys l
          where l.ref = :orgref;
          select symbol
          from listywys
          where ref = :newlistywysref
          into :lsymbol;
        end
        --exception test_break :newlistywysref||' '||:dokum||' '||:typdok;
        -- dodawanie pozycji do zamowien
        select ldp.ilosc,ldp.ilosco,ldp.iloscdorozb,ldp.dokpoz,ldp.pozzam
        from listywysdpoz ldp where ldp.ref = :refldp
        into :oldilosc, :oldilosco, :oldiloscdorozb, :olddokpoz, :oldpozzam;
        newilosc = oldilosc - oldiloscdorozb;
        newilosco = oldilosco - oldiloscdorozb;
        newiloscdorozb = oldiloscdorozb;
        -- update ilosci na pozycjach oryginalnych bo LISTYWYS_ADDPOZ wylicza wartosci wg. zmian
        update listywysdpoz set ilosc = :newilosc, ilosco = :newilosco, iloscdorozb = null where listywysdpoz.ref = :refldp;

        -- jesli przeniesiono cala ilosc z pozycji oryginalnej to kasuje te pozycje
        execute procedure LISTYWYS_ADDPOZ(:newlistywysdref, :newlistywysref, :dokum, :typdok) returning_values :newlistywysdref;

        -- update ilosci na pozycjach skopiowanych bo LISTYWYS_ADDPOZ wstawia roznice miedzy iloscia na zamowieniu
        -- a iloscia na pozycji oryginalnej (nie zawsze zgadza sie z tym co ma byc)
        if(olddokpoz is null) then begin
          update listywysdpoz set ilosc = :newiloscdorozb, ilosco = :newiloscdorozb, iloscdorozb = null where dokument = :newlistywysdref and pozzam = :oldpozzam ;
        end
        if(oldpozzam is null) then begin
          update listywysdpoz set ilosc = :newiloscdorozb, ilosco = :newiloscdorozb, iloscdorozb = null where dokument = :newlistywysdref and dokpoz = :olddokpoz;
        end
        -- jesli przeniesiono cala ilosc z pozycji oryginalnej to kasuje te pozycje

        if(newilosc<=0)then
          delete from listywysdpoz where ref = :refldp;
        else
          update listywysdpoz set ilosc = :newilosc, ilosco = :newilosco,  iloscdorozb = null where ref = :refldp;
        status = 1;
        czyzmianailosci = null;
      end
    end
  end
  suspend;
end^
SET TERM ; ^
