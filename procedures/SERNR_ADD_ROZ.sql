--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SERNR_ADD_ROZ(
      POZYCJA integer,
      ILOSC numeric(14,4),
      SERIALNR varchar(40) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE REF INTEGER;
DECLARE VARIABLE ODSERIAL VARCHAR(40);
DECLARE VARIABLE ODSERIALNR INTEGER;
DECLARE VARIABLE DOSERIALNR INTEGER;
DECLARE VARIABLE ILPOZ NUMERIC(14,4);
DECLARE VARIABLE ILOSCROZ NUMERIC(14,4);
DECLARE VARIABLE SERIALNUM INTEGER;
DECLARE VARIABLE I INTEGER;
begin
  for select REF, ODSERIAL, ODSERIALNR, DOSERIALNR, ILOSC, ILOSCROZ from DOKUMSER
    where DOKUMSER.refpoz = :pozycja and typ = 'M'
    into :ref,:odserial, :odserialnr, :doserialnr, :ilpoz, :iloscroz
  do begin
    if(:ilosc > 0) then begin
      if(:odserialnr is not null and :doserialnr is not null) then begin
        if(:serialnum is null) then serialnum = cast(:serialnr as integer);
        if(:serialnum >= :odserialnr and :serialnum <= :doserialnr) then begin
          i = :ilpoz - :iloscroz;
          if(:i > :ilosc) then i = :ilosc;
          update DOKUMSER set ILOSCROZ = ILOSCROZ + :i where REF=:ref;
          ilosc = :ilosc - :i;
        end
      end else if(:odserial = :serialnr)then begin
         i = :ilpoz - :iloscroz;
         if(:i > :ilosc) then i = :ilosc;
         update DOKUMSER set ILOSCROZ = ILOSCROZ + :i where ref = :ref;
         ilosc = :ilosc - :i;
      end
    end
  end
end^
SET TERM ; ^
