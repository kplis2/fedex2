--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_STATEMENT_PARSE(
      SOURCE varchar(8191) CHARACTER SET UTF8                           ,
      KEY1 integer,
      KEY2 integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(16,6))
   as
declare variable decl varchar(1024);
declare variable body varchar(8191);
declare variable lastvar integer;
begin
  execute procedure STATEMENT_MULTIPARSE(:source, :prefix, '', '', 1, 1)
    returning_values :decl, :body, :lastvar;
  execute procedure E_STATEMENT_EXECUTE(:decl, :body, :key1, :key2, 0)
    returning_values :ret;
end^
SET TERM ; ^
