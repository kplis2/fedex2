--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NEOS_CLI_WAIT(
      SECONDS INTEGER_ID)
   as
declare variable start_timestamp timestamp_id;
begin
  start_timestamp = current_timestamp;
  while(datediff(second from :start_timestamp to cast('NOW' as timestamp)) < :seconds) do
  begin

  end
end^
SET TERM ; ^
