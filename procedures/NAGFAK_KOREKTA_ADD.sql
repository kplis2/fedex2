--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_KOREKTA_ADD(
      KOREKTA integer,
      NAGFAK integer,
      MASKA varchar(255) CHARACTER SET UTF8                           ,
      GRUPA varchar(255) CHARACTER SET UTF8                           ,
      TERMZAP timestamp)
   as
declare variable SAD smallint;
declare variable POZSAD integer;
declare variable NEWPOZSAD integer;
declare variable REFDOPAKIET integer;
declare variable POZREF integer;
declare variable BN varchar(1);
declare variable RABAT numeric(14,2);
declare variable KTM varchar(40);
declare variable NAZWAT TOWARY_NAZWA;
declare variable PKWIU PKWIU;
declare variable WERSJAREF integer;
declare variable ILOSC numeric(14,4);
declare variable ILOSCO numeric(14,4);
declare variable ILOSCM numeric(14,4);
declare variable DOSTAWA integer;
declare variable JEDN TOWJEDN;
declare variable JEDNO TOWJEDN;
declare variable CENACEN CENYWAL;
declare variable WALCEN WALUTA_ID;
declare variable KURSCEN KURS;
declare variable TABKURSCEN TABKURS_ID;
declare variable RABATTAB CENY;
declare variable CENANET CENYWAL;
declare variable CENABRU numeric(14,4);
declare variable WARTNET numeric(14,2);
declare variable WARTBRU numeric(14,2);
declare variable GR_VAT VAT_ID;
declare variable VAT CENY;
declare variable CENANETZL CENYSPR;
declare variable CENABRUZL CENYSPR;
declare variable WARTNETZL CENY;
declare variable wartbruzl CENY;
declare variable MAGAZYN DEFMAGAZ_ID;
declare variable OPK smallint;
declare variable OPIS STRING255;
declare variable PARAMN1 numeric(14,4);
declare variable PARAMN2 numeric(14,4);
declare variable PARAMN3 numeric(14,4);
declare variable PARAMN4 numeric(14,4);
declare variable PARAMN5 numeric(14,4);
declare variable PARAMN6 numeric(14,4);
declare variable PARAMN7 numeric(14,4);
declare variable PARAMN8 numeric(14,4);
declare variable PARAMN9 numeric(14,4);
declare variable PARAMN10 numeric(14,4);
declare variable PARAMN11 numeric(14,4);
declare variable PARAMN12 numeric(14,4);
declare variable PARAMS1 varchar(255);
declare variable PARAMS2 varchar(255);
declare variable PARAMS3 varchar(255);
declare variable PARAMS4 varchar(255);
declare variable PARAMS5 varchar(255);
declare variable PARAMS6 varchar(255);
declare variable PARAMS7 varchar(255);
declare variable PARAMS8 varchar(255);
declare variable PARAMS9 varchar(255);
declare variable PARAMS10 varchar(255);
declare variable PARAMS11 varchar(255);
declare variable PARAMS12 varchar(255);
declare variable PARAMD1 timestamp;
declare variable PARAMD2 timestamp;
declare variable PARAMD3 timestamp;
declare variable PARAMD4 timestamp;
declare variable FROMPROMOREF PROMOCJE_ID;
declare variable ILOSCZADOPAKIET ILOSCI_FAK;
declare variable PREC smallint;
declare variable ALTTOPOZ POZZAM_ID;
declare variable FAKE smallint;
declare variable ALTTOPOZN DOKUMPOZ_ID;
declare variable CENAMAG numeric(14,2);
declare variable REFK POZFAK_ID;
begin
  if(exists(select n.ref
            from nagfak n
            left join nagfak nk on (n.korekta = nk.ref)
             where n.ref = :nagfak and n.korekta is not null and nk.anulowanie = 0
  )) then
    exception universal 'Do wybranej faktury wystawiono już fakture korygującą';
  if(termzap < '1900-01-01') then termzap = null;
  if(not exists (select p.ref from pozfak p where p.dokument = :nagfak
    and (coalesce(:grupa,'')='' or :grupa = (select t.grupa from towary t where t.ktm = p.ktm))
    and (coalesce(:maska,'')='' or p.ktm like :maska))
  ) then exit;
  update nagfak set korekta = :korekta, wasdeakcept = coalesce(wasdeakcept,0)+10  where ref = :nagfak;
  select bn, rabat from nagfak where ref = :nagfak into :bn, :rabat;

  select TYPFAK.sad from TYPFAK right join NAGFAK on ( NAGFAK.TYP = TYPFAK.SYMBOL) where NAGFAk.REF = :korekta into :sad;
  if (:sad is null) then sad = 0;
  if (:sad = 0) then begin

    for
      select p.KTM,p.NAZWAT,p.PKWIU,p.WERSJAREF,p.DOSTAWA,
         p.ILOSC, p.ILOSCO, p.ILOSCM,
         p.JEDN, p.JEDNO,
         (case when :bn = 'N' then p.CENACEN else (p.cenanet*100/(100-coalesce(:rabat,0)-coalesce(p.rabat,0)))end)/*-CENACEN*:rabatw*/,
         p.WALCEN,p.KURSCEN,p.TABKURSCEN,
         p.RABAT, p.RABATTAB, p.CENAMAG,
         p.GR_VAT, p.VAT, p.CENANET, p.CENABRU, p.WARTNET, p.WARTBRU,
         p.CENANETZL, p.CENABRUZL,  p.WARTNETZL, p.WARTBRUZL,
         p.OPK, p.MAGAZYN, p.OPIS,
         p.PARAMN1, p.PARAMN2, p.PARAMN3, p.PARAMN4,
         p.PARAMN5, p.PARAMN6, p.PARAMN7, p.PARAMN8,
         p.PARAMN9, p.PARAMN10, p.PARAMN11, p.PARAMN12,
         p.PARAMS1, p.PARAMS2, p.PARAMS3, p.PARAMS4,
         p.PARAMS5, p.PARAMS6, p.PARAMS7, p.PARAMS8,
         p.PARAMS9, p.PARAMS10, p.PARAMS11, p.PARAMS12,
         p.PARAMD1, p.PARAMD2, p.PARAMD3, p.PARAMD4,
         (case when :termzap is not null then :termzap else p.termzap end),
         p.REF, p.frompromoref, p.ilosczadopakiet, p.prec,
         p.fake, p.alttopoz
      from POZFAK p
      where p.DOKUMENT = :nagfak
      order by p.NUMER, p.alttopoz nulls first
      into KTM,NAZWAT,PKWIU,WERSJAREF,DOSTAWA,
         ILOSC, ILOSCO, ILOSCM,
         JEDN, JEDNO,
         CENACEN, WALCEN, KURSCEN,TABKURSCEN,
         RABAT,RABATTAB, CENAMAG,
         GR_VAT, VAT,
         CENANET, CENABRU,
         WARTNET, WARTBRU,
         CENANETZL, CENABRUZL,
         WARTNETZL, WARTBRUZL,
         OPK, MAGAZYN,OPIS,
         PARAMN1, PARAMN2, PARAMN3, PARAMN4,
         PARAMN5, PARAMN6, PARAMN7, PARAMN8,
         PARAMN9, PARAMN10, PARAMN11, PARAMN12,
         PARAMS1, PARAMS2, PARAMS3, PARAMS4,
         PARAMS5, PARAMS6, PARAMS7, PARAMS8,
         PARAMS9, PARAMS10, PARAMS11, PARAMS12,
         PARAMD1, PARAMD2, PARAMD3, PARAMD4,
         TERMZAP, REFK, frompromoref, ilosczadopakiet, prec,
         FAKE, ALTTOPOZ
       do begin

          -- ustawienie odwoań w polu ALTTOPOZ
          AltToPozN = null;
          if (AltToPoz is not null) then
          begin
            select first 1 p.ref
            from pozfak p
            where p.dokument = :korekta
              and refk = :ALTTOPOZ
            into :AltToPozN;
          end

          insert into POZFAK(DOKUMENT,ORD,KTM,NAZWAT,PKWIU,WERSJAREF,DOSTAWA,
               ILOSC, ILOSCO, ILOSCM, PILOSC,PILOSCO, PILOSCM, PNAZWAT,
               JEDN, JEDNO,
               CENACEN, WALCEN,PCENACEN,PWALCEN,KURSCEN,TABKURSCEN,PKURSCEN,
               RABAT,PRABAT,RABATTAB, PRABATTAB,
               PCENAMAG,
               GR_VAT, VAT, PGR_VAT, PVAT,
               CENANET, PCENANET, CENABRU, PCENABRU,
               WARTNET, PWARTNET, WARTBRU, PWARTBRU,
               CENANETZL, PCENANETZL,CENABRUZL, PCENABRUZL,
               WARTNETZL, PWARTNETZL, WARTBRUZL, PWARTBRUZL, OPK, MAGAZYN,OPIS,
               PARAMN1, PARAMN2, PARAMN3, PARAMN4,
               PARAMN5, PARAMN6, PARAMN7, PARAMN8,
               PARAMN9, PARAMN10, PARAMN11, PARAMN12,
               PARAMS1, PARAMS2, PARAMS3, PARAMS4,
               PARAMS5, PARAMS6, PARAMS7, PARAMS8,
               PARAMS9, PARAMS10, PARAMS11, PARAMS12,
               PARAMD1, PARAMD2, PARAMD3, PARAMD4, TERMZAP, REFK, frompromoref, ilosczadopakiet, prec,
               FAKE, ALTTOPOZ)
          values( :korekta,0,:KTM,:NAZWAT,:PKWIU,:WERSJAREF,:DOSTAWA,
               :ILOSC, :ILOSCO, :ILOSCM, :ILOSC, :ILOSCO, :ILOSCM, :nazwat,
               :JEDN, :JEDNO,
               :CENACEN, :WALCEN, :CENACEN, :WALCEN, :KURSCEN, :TABKURSCEN, :kurscen,
               :RABAT, :RABAT, :RABATTAB, :RABATTAB,
               :CENAMAG,
               :GR_VAT, :VAT, :GR_VAT, :VAT,
                NULL, :CENANET, NULL, :CENABRU,
               :WARTNET, :WARTNET, :WARTBRU, :WARTBRU,
               :CENANETZL, :CENANETZL, :CENABRUZL, :CENABRUZL,
               :WARTNETZL, :WARTNETZL, :WARTBRUZL, :WARTBRUZL, :OPK, :MAGAZYN, :OPIS,
               :PARAMN1, :PARAMN2, :PARAMN3, :PARAMN4,
               :PARAMN5, :PARAMN6, :PARAMN7, :PARAMN8,
               :PARAMN9, :PARAMN10, :PARAMN11, :PARAMN12,
               :PARAMS1, :PARAMS2, :PARAMS3, :PARAMS4,
               :PARAMS5, :PARAMS6, :PARAMS7, :PARAMS8,
               :PARAMS9, :PARAMS10, :PARAMS11, :PARAMS12,
               :PARAMD1, :PARAMD2, :PARAMD3, :PARAMD4, :TERMZAP,
               :REFK, :frompromoref, :ilosczadopakiet, :prec,
               :fake, :alttopozn);
        end
  end else begin
    --kopiowanie pozsad i pozfaków
    for select p.ref
      from pozsad p
      where p.faktura = :nagfak
      order by p.numer
      into :pozsad
    do begin
      execute procedure GEN_REF('POZSAD') returning_values :newpozsad;
      insert into pozsad(ref, faktura, numer, opis, podstawa, grupacla, stawkacla, clo,
            wartnet, gr_vat, vat,  clovat,
            ppodstawa, pgrupacla, pstawkacla, pclo,
            pwartnet, pgr_vat, pvat,  pclovat,
            ord, refk)
        select :newpozsad, :korekta, numer, opis, podstawa, grupacla, stawkacla, clo,
            wartnet, gr_vat, vat,  clovat,
            podstawa, grupacla, stawkacla, clo,
            wartnet, gr_vat, vat,  clovat,
            1, ref
          from pozsad where ref = :pozsad;
      --kopiowane pozfaków powiązanych z pozsad
      insert into POZFAK(DOKUMENT,ORD,KTM,NAZWAT,PKWIU,WERSJAREF,DOSTAWA,
         ILOSC, ILOSCO, ILOSCM, PILOSC,PILOSCO, PILOSCM, PNAZWAT,
         JEDN, JEDNO,
         CENACEN, WALCEN,PCENACEN,PWALCEN,KURSCEN,PKURSCEN,TABKURSCEN,
         RABAT,PRABAT,RABATTAB, PRABATTAB,
         PCENAMAG,
         GR_VAT, VAT, PGR_VAT, PVAT,
         CENANET, PCENANET, CENABRU, PCENABRU,
         WARTNET, PWARTNET, WARTBRU, PWARTBRU,
         CENANETZL, PCENANETZL,CENABRUZL, PCENABRUZL,
         WARTNETZL, PWARTNETZL, WARTBRUZL, PWARTBRUZL, OPK, MAGAZYN,OPIS,
         PARAMN1, PARAMN2, PARAMN3, PARAMN4,
         PARAMN5, PARAMN6, PARAMN7, PARAMN8,
         PARAMN9, PARAMN10, PARAMN11, PARAMN12,
         PARAMS1, PARAMS2, PARAMS3, PARAMS4,
         PARAMS5, PARAMS6, PARAMS7, PARAMS8,
         PARAMS9, PARAMS10, PARAMS11, PARAMS12,
         PARAMD1, PARAMD2, PARAMD3, PARAMD4, TERMZAP, REFK, refpozsad, frompromoref, ilosczadopakiet, prec,
         FAKE, ALTTOPOZ)
    select :korekta,0,p.KTM,p.NAZWAT,p.PKWIU,p.WERSJAREF,p.DOSTAWA,
         p.ILOSC, p.ILOSCO, p.ILOSCM, p.ILOSC, p.ILOSCO, p.ILOSCM,p.nazwat,
         p.JEDN, p.JEDNO,
         (case when :bn = 'N' then CENACEN else (cenanet*100/(100-coalesce(:rabat,0)-coalesce(rabat,0)))end)/*-CENACEN*:rabatw*/, WALCEN,(case when :bn = 'N' then CENACEN else (cenanet*100/(100-coalesce(:rabat,0)-coalesce(rabat,0)))end),WALCEN,KURSCEN,PKURSCEN,TABKURSCEN,
         p.RABAT, p.RABAT, p.RABATTAB, p.RABATTAB,
         p.CENAMAG,
         p.GR_VAT, p.VAT, p.GR_VAT, p.VAT,
         NULL, p.CENANET, NULL, p.CENABRU,
         p.WARTNET, p.WARTNET, p.WARTBRU, p.WARTBRU,
         p.CENANETZL, p.CENANETZL, p.CENABRUZL, p.CENABRUZL,
         p.WARTNETZL, p.WARTNETZL, p.WARTBRUZL, p.WARTBRUZL, p.OPK, p.MAGAZYN,p.OPIS,
         p.PARAMN1, p.PARAMN2, p.PARAMN3, p.PARAMN4,
         p.PARAMN5, p.PARAMN6, p.PARAMN7, p.PARAMN8,
         p.PARAMN9, p.PARAMN10, p.PARAMN11, p.PARAMN12,
         p.PARAMS1, p.PARAMS2, p.PARAMS3, p.PARAMS4,
         p.PARAMS5, p.PARAMS6, p.PARAMS7, p.PARAMS8,
         p.PARAMS9, p.PARAMS10, p.PARAMS11, p.PARAMS12,
         p.PARAMD1, p.PARAMD2, p.PARAMD3, p.PARAMD4, (case when :termzap is not null then :termzap else p.termzap end),
         p.REF, :newpozsad, p.frompromoref, p.ilosczadopakiet, p.prec,
         p.fake, 0
     from POZFAK p
     where p.DOKUMENT = :nagfak and refpozsad = :pozsad
        and coalesce(fake,0) = 0
     order by p.NUMER;
    end
  end
  --odtwarzanie REFDOPAKIET korekty na podstawie faktury zródowej
  for select PK.REF, PKP.REF
    from POZFAK PF
    join POZFAK PFP on (PF.refdopakiet = PFP.ref)
    join POZFAK PK on (pk.refk = pf.ref and pk.dokument = :korekta)
    join POZFAK PKP on (PKP.refk = PFP.ref and pkp.dokument = :korekta)
    where PF.dokument = :nagfak and pF.refdopakiet > 0
    into :pozref, :refdopakiet
  do begin
    update POZFAK set REFDOPAKIET = :refdopakiet where ref=:pozref;
  end

  suspend;
end^
SET TERM ; ^
