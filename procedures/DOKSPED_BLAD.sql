--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKSPED_BLAD(
      DOKREF integer,
      TRYB smallint,
      KIERUNEK smallint)
  returns (
      KOD smallint)
   as
declare variable operator integer;
begin
  if (:kierunek is null) then kierunek = 0;
  kod = 0;
  if (:TRYB is null) then
  begin
    kod = 1; -- nie podano trybu
    exit;
  end
  if (:tryb = 0) then
  begin
    select listywysd.selloper from listywysd where listywysd.ref = :DOKREF
      into :operator;
    if (:operator is not null and :kierunek = 0) then
      update listywysd set listywysd.sellmistake = 1
        where listywysd.ref = :dokref;
    else if (:operator is not null and :kierunek = 1) then
      update listywysd set listywysd.sellmistake = 0
        where listywysd.ref = :dokref;
    else if (:operator is null) then
      kod = 2; -- nie ma na dokumencie spedycyjnym okrelonego sprzedawcy
  end
  else if (:tryb = 1) then
  begin
    select listywysd.checkoper from listywysd where listywysd.ref = :DOKREF
      into :operator;
    if (:operator is not null and :kierunek = 0) then
      update listywysd set listywysd.checkmistake = 1
        where listywysd.ref = :dokref;
    else if (:operator is not null and :kierunek = 1) then
      update listywysd set listywysd.checkmistake = 0
        where listywysd.ref = :dokref;
    else if (:operator is null) then
      kod = 3; -- nie ma na dokumencie spedycyjnym okrelonego szykujacego
  end
  else if (:tryb = 2) then
  begin
    select listywysd.acceptoper from listywysd where listywysd.ref = :DOKREF
      into :operator;
    if (:operator is not null and :kierunek = 0) then
      update listywysd set listywysd.acceptmistake = 1
        where listywysd.ref = :dokref;
    else if (:operator is not null and :kierunek = 1) then
      update listywysd set listywysd.acceptmistake = 0
        where listywysd.ref = :dokref;
    else if (:operator is null) then
      kod = 3; -- nie ma na dokumencie spedycyjnym okrelonego sprawdzajacego
  end
end^
SET TERM ; ^
