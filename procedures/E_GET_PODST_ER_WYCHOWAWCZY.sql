--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_PODST_ER_WYCHOWAWCZY(
      PERSON integer,
      IPERIOD varchar(6) CHARACTER SET UTF8                           ,
      COMPANY integer)
  returns (
      BASEVAL numeric(14,2))
   as
declare variable fromdate date;
declare variable todate date;
declare variable mfromdate date;
declare variable mtodate date;
declare variable pfromdate date;
declare variable ptodate date;
declare variable afromdate date;
declare variable aref integer;
declare variable percount integer;
declare variable period varchar(6);
declare variable eperiod varchar(6);
declare variable pvalue numeric(14,2);
declare variable sum_pvalue numeric(14,2);
declare variable workdays numeric(14,2);
declare variable eworkdays numeric(14,2);
declare variable employee integer;
declare variable tmp integer;
declare variable qualipercount integer;
begin
/*Personel: Procedura wylicza w zadanym okresie ubezpieczniowym IPERIOD, dla danej
            osoby PEROSN, podstawe wymiaru skladki na urlopie wychowawczym */

  execute procedure period2dates(iperiod)
   returning_values fromdate, todate;

  execute procedure get_pval(fromdate, company, 'PODST_ER_WYCHOWAWCZY')
    returning_values baseval;

  if (fromdate >= '2012-01-01') then
  begin

    select first 1 a.ref, a.employee from employees e
        join eabsences a on (a.employee = e.ref)
      where e.company = :company and e.person = :person
        and a.company = :company and a.ecolumn = 260
        and a.fromdate < :todate and a.correction in (0,2)
      order by a.fromdate desc
      into :aref, :employee;

    if (aref is not null) then
    begin
    --wyznaczenie okresu, od ktorego wlaczenie bedziemy badac 12 wynagrodzen wstecz
      select afromdate from e_get_whole_eabsperiod(:aref, null, :todate, null, null, null, null)
        into :afromdate;
  
      execute procedure e_func_periodinc(null, -1, afromdate)
        returning_values period;
  
    --wyznaczenie najstarszego okresu kwalifikowanego do badania
      select min(m.fromdate) from employment m
        join employees e on (e.ref = m.employee and e.ref = :employee)
        into :mfromdate;

      execute procedure e_func_periodinc(null, 0, mfromdate)
        returning_values eperiod;

     --ustawienie licznika okresow kwalifikowanych
      if (afromdate = mfromdate) then
      begin
      /*PERCOUNT = -2 oznacza, ze wychowawczy rozpoczal sie z zatrudnieniem i
        gwarantuje jedno przejscie ponizszej petli gdzie wyliczamy wynagrodzenie */
        qualipercount = -2;
        period = eperiod;
      end else
        qualipercount = 0;

    --okreslenie wartosci podstawy skladek
      percount = 1;
      sum_pvalue = 0;
      while ((period >= eperiod and 0 <= qualipercount and percount <= 12) or qualipercount = -2) do
      begin
        execute procedure period2dates(period)
          returning_values pfromdate, ptodate;

        select wd from ecal_work(:employee, :pfromdate, :ptodate)
          into :workdays;

        tmp = null;
        select sum(workdays) from eabsences
          where employee = :employee and correction in (0,2)
            and fromdate >= :pfromdate and todate <= :ptodate
            and ecolumn in (10, 40, 50, 60, 90, 100, 110, 120, 130, 140, 150, 160, 170, 190, 260, 270, 280, 290, 300, 330, 440, 450)
          into :tmp;
        tmp = coalesce(tmp,0);

        if ((workdays - tmp) >= workdays/2.00 or qualipercount = -2) then
        begin
        --okres jest kalifikowany, wiec pobieram wynagrodenie z umowy (proporcjonalnie jezeli ulegalo zmianom)
          for
            select salary + caddsalary + funcsalary, fromdate, todate
              from e_get_employmentchanges(:employee, :pfromdate, :ptodate, ';SALARY;CADDSALARY;FUNCSALARY;')
              into :pvalue, :mfromdate, :mtodate
          do begin
            if (mfromdate <> pfromdate or mtodate <> ptodate) then --prosty test aby uniknac niepotrzebnego uruchamiana ecal_work
              select wd from ecal_work(:employee, :mfromdate, :mtodate)
                into :eworkdays;
            else
              eworkdays = workdays;

            if (workdays > 0) then
              pvalue = pvalue * (eworkdays / workdays);

            sum_pvalue = sum_pvalue + pvalue;
          end

          pvalue = null;
          select sum(p.pvalue) from epayrolls r
            join eprpos p on (p.payroll = r.ref and p.employee = :employee and r.cper = :period)
            join ecolumns c on (c.number = p.ecolumn and c.cflags containing ';CHR;')
            where p.ecolumn > 1000
            into :pvalue;

          sum_pvalue = sum_pvalue + coalesce(pvalue,0);
          qualipercount = qualipercount + 1;
        end

        percount = percount + 1;

        execute procedure e_func_periodinc(period, -1)
          returning_values period;
      end

      if (qualipercount > 0) then
        sum_pvalue = sum_pvalue/qualipercount; --przecietne wynagr. za okres 12 m-cy poprzedzajacych urlop wychowawczy

      if (sum_pvalue < baseval) then
        baseval = sum_pvalue;
    end
  end

  suspend;
end^
SET TERM ; ^
