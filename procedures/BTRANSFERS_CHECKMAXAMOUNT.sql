--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BTRANSFERS_CHECKMAXAMOUNT(
      BTRPOS integer)
  returns (
      MAXAMOUNT numeric(14,2))
   as
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable ONOTHER numeric(14,2);
declare variable ACCOUNT ACCOUNT_ID;
declare variable SETTLEMENT varchar(20);
declare variable BTRANSFER integer;
declare variable COMPANY integer;
begin
  select BTRANSFER, ACCOUNT, SETTLEMENT
    from  BTRANSFERPOS where REF = :btrpos
    into :btransfer, :account, :settlement;

  select SLODEF, SLOPOZ, company from BTRANSFERS where ref = :btransfer
    into :slodef, :slopoz, :company;

  select MA - WINIEN from ROZRACH
    where SLODEF = :slodef
      and slopoz = :slopoz
      and KONTOFK = :account
      and SYMBFAK = :settlement
      and company = :company
    into :maxamount;

  if (maxamount is null) then maxamount = 0;

  select sum(BTRANSFERPOS.AMOUNT)
    from BTRANSFERPOS
      join BTRANSFERS on (BTRANSFERS.slodef = :slodef and BTRANSFERS.slopoz = :slopoz and BTRANSFERPOS.BTRANSFER = BTRANSFERS.REF)
      join BTRANTYPE on (BTRANTYPE.symbol = BTRANSFERS.btype)
    where BTRANSFERPOS.ACCOUNT = :account
      and BTRANSFERS.company = :company
      and BTRANSFERPOS.settlement = :settlement
      and ((BTRANSFERS.status < 3 and ((BTRANTYPE.genrozrach = 0) or (BTRANTYPE.genrozrach is null)))
         or (BTRANSFERS.status < 1 and BTRANTYPE.genrozrach > 0)
        )
     into :onother;

  if (onother is null) then
    onother = 0;
  maxamount = maxamount - onother;
end^
SET TERM ; ^
