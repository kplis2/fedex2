--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_RECONNECT(
      OPER OPERATOR_ID = null)
   as
declare variable app varchar(40);
begin
  post_event 'RECONNECT'||coalesce(:oper,'');
  for
    select distinct s.application
      from operator o
        join s_sessions s on (s.userid = o.userid)
      where o.ref = :OPER or :oper is null
      into :app
  do
    execute procedure REFRESH_USERS2(:app);
end^
SET TERM ; ^
