--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_REALIZACJA_JOINMAGTOZAM(
      HISTORIA integer,
      ZAM integer,
      DOKMAG integer)
   as
declare variable org_ref integer;
declare variable reffak integer;
declare variable tonagfak integer;
declare variable fakdepend integer;
declare variable dokpozref integer;
declare variable pozref integer;
declare variable pozfakref integer;
declare variable magrozlicz integer;
begin
  tonagfak = 0;
  select org_ref, faktura, fakdepend from NAGZAM where ref=:zam into :org_ref, :reffak, :fakdepend;
  if(:org_ref is null) then org_ref = :zam;
  if(:org_ref <> :zam) then
     select faktura, fakdepend from NAGZAM where ref=:org_ref into :reffak, :fakdepend;
  if(:reffak > 0 and :fakdepend > 0) then begin
    select magrozlicz from NAGFAK where REF=:reffak into :magrozlicz;
    if(:magrozlicz is null) then magrozlicz = 0;
    if(:magrozlicz = 0) then
      tonagfak = 1;
  end
  if(:tonagfak > 0) then begin
        /*zaznaczenie naglowka dokumentu*/
        update DOKUMNAG set FAKTURA = :reffak where REF=:dokmag;
        update HISTZAM set MAGTOFAKJOINED = 1 where REF=:historia;
        /*dla każdej pozycji dokumetntu okrel pozycj dok. VAT*/
        for select DOKUMPOZ.REF, DOKUMPOZ.frompozzam, POZFAK.ref
        from DOKUMPOZ left join POZFAK on (DOKUMPOZ.FROMPOZZAM = POZFAK.frompozzam and POZFAK.dokument = :reffak)
        where DOKUMPOZ.DOKUMENT = :dokmag
        into :dokpozref, :pozref, :pozfakref
        do begin
          if(:pozfakref is null) then begin
            select max(POZFAK.REF)
              from POZFAK  join POZZAM  POZORG on (POZFAK.FROMPOZZAM = POZORG.REF and POZORG.zamowienie = :org_ref)
                           join POZZAM  POZYC on (POZORG.REF = POZYC.POPREF)
            where POZYC.REF = :pozref
            into :pozfakref;
          end
          if(:pozfakref > 0) then begin
            update DOKUMPOZ set FROMPOZFAK = :pozfakref where DOKUMPOZ.REF = :dokpozref;
          end
        end
   end
end^
SET TERM ; ^
