--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_SCHEDULER_ADD_PROCESS(
      SCHEDULE_REF SYS_SCHEDULE_ID,
      SCHEDULER_SYMBOL SYS_SCHEDULER_SYMBOL_ID = null)
  returns (
      INSTANCE integer)
   as
begin
  if (SCHEDULER_SYMBOL = '') then
    SCHEDULER_SYMBOL = null;
  insert into sys_schedulehist (schedule, SCHEDULER_SYMBOL)
    values (
      :schedule_ref, :SCHEDULER_SYMBOL)
    returning ref into :instance;
  suspend;
end^
SET TERM ; ^
