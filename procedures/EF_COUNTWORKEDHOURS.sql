--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_COUNTWORKEDHOURS(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable FROMDATE date;
declare variable TODATE date;
declare variable HOURS numeric(14,2);
begin
  --DU: personel - funkcja podaje ilosc przepracowanych przez pracownika godzin
  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;
  select ws from ecal_work(:employee, :fromdate, :todate)
    into ret;
  ret = ret / 3600;

  select sum(worksecs)
    from eabsences
    where employee = :employee and correction = 0
      and fromdate >= :fromdate and todate <= :todate
    into :hours;
  hours = hours / 3600;

  if (hours is not null) then
    ret = ret - hours;
  suspend;
end^
SET TERM ; ^
