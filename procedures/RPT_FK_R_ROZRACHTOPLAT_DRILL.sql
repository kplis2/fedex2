--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_R_ROZRACHTOPLAT_DRILL(
      DATAPLAT timestamp,
      DATASTAN timestamp,
      ACCOUNTMASK ACCOUNT_ID,
      FORSLODEF integer,
      DEPARTMENT varchar(10) CHARACTER SET UTF8                           ,
      ZAKRES smallint,
      COMPANY integer,
      CONTRACTOR_DRILL varchar(80) CHARACTER SET UTF8                           )
  returns (
      REF integer,
      ACCOUNTKOD ANALYTIC_ID,
      SLODEF integer,
      SLOPOZ integer,
      CONTRACTOR varchar(80) CHARACTER SET UTF8                           ,
      PHONE varchar(255) CHARACTER SET UTF8                           ,
      SETTLEMENT varchar(30) CHARACTER SET UTF8                           ,
      OPENDATE timestamp,
      PAYDATE timestamp,
      AMOUNT numeric(15,2),
      PAYED numeric(15,2),
      AFTERPAY integer,
      NR integer,
      CURRENCY varchar(3) CHARACTER SET UTF8                           )
   as
declare variable old_symbol varchar(3);
declare variable i_yearid integer;
declare variable old_account ACCOUNT_ID;
declare variable s_account  ACCOUNT_ID;
declare variable n_debit numeric(15,2);
declare variable n_credit numeric(15,2);
declare variable i_ref integer;
declare variable s_sep varchar(1);
declare variable i_dictdef integer;
declare variable i_len smallint;
declare variable s_tmp ACCOUNT_ID;
declare variable old_prefix ACCOUNT_ID;
declare variable i_i smallint;
declare variable i_j smallint;
declare variable i_k smallint;
declare variable dictref integer;
declare variable r_account integer;
declare variable valpom numeric(14,2);
declare variable old_slodef integer;
declare variable old_slopoz integer;
declare variable cdate timestamp;
declare variable slodefref integer;
declare variable slodefref1 integer;
declare variable account ACCOUNT_ID;
begin
  ref = 0;
  old_slodef = 0;
  old_slopoz = 0;
  cdate = current_date;
  execute procedure create_account_mask(:accountmask) returning_values :accountmask;
  if(:accountmask = '%') then accountmask = null;
  for select slodef, slopoz, symbfak, dataotw, dataplat, waluta, kontofk
    from ROZRACH
    where ((:ACCOUNTMASK is null) or (:accountmask = '') or (kontofk like :accountmask))
      and dataplat <= :dataplat
      and (datazamk is null or datazamk > :dataplat)
      and SLODEF > 0 and SLOPOZ > 0
      and ((:forslodef is null) or (:forslodef = 0) or (:forslodef = slodef))
      --and ((:zakres > 1) or (:zakres = 0 and winien > ma) or (:zakres = 1 and ma > winien))
      and ((:department is null) or (:department = '') or (:department = ODDZIAL))
      and rozrach.company = :company
      and slopoz =: contractor_drill
    order by slodef, slopoz
    into :slodef, :slopoz, :settlement, :opendate, :paydate, :currency, :account
  do begin
    select sum(r.winien), sum(r.ma)
      from rozrachp r
      where r.kontofk = :account
        and r.symbfak = :settlement
        and r.slodef = :slodef
        and r.slopoz = :slopoz
        and r.data <= coalesce(:datastan,:dataplat)
      into :amount, :payed;
    if (:zakres > 1 or (:zakres = 0 and :amount > :payed) or (:zakres = 1 and :amount < :payed)) then
    begin
      if(:amount < :payed) then begin
        valpom = :amount;
        amount = :payed;
        payed = :valpom;
      end
      payed = :amount - :payed;
      if(:slodef <> :old_slodef or (:slopoz <> old_slopoz)) then begin
        contractor = null;
        nr = 0;
        execute procedure KODKS_FROM_DICTPOS(:slodef, :slopoz) returning_values :accountkod;
        execute procedure name_from_dictposref(:slodef, :slopoz) returning_values :contractor;
        select min(slodef.ref) from slodef where slodef.typ = 'KLIENCI' into :slodefref;
        if(:slodef = :slodefref) then
          select KLIENCI.telefon from KLIENCI where REF=:slopoz into :phone;
        select min(slodef.ref) from slodef where slodef.typ = 'DOSTAWCY' into :slodefref1;
        if(:slodef = :slodefref1) then
          select DOSTAWCY.telefon from DOSTAWCY where REF=:slopoz into :phone;
        if(:slodef <> :slodefref and :slodef <> :slodefref1) then phone = null;
      end
      nr = :nr + 1;
      if(:paydate < :cdate) then
        afterpay = :cdate - :paydate;
      else
        afterpay = null;
      ref = :ref + 1;
      suspend;
      old_slodef = :slodef;
      old_slopoz = :slopoz;
    end
  end
end^
SET TERM ; ^
