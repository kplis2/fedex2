--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDEL_COUNTDAY(
      EDELDAY integer,
      DAYCNT integer,
      INCOUNTRY smallint)
  returns (
      DLUMP numeric(14,2),
      ACCOM numeric(14,2),
      ALLOW numeric(14,2),
      DAYINCOUNTRY numeric(14,2))
   as
  declare variable allowbase numeric(4,2);
  declare variable allowmeals numeric(4,2);
  declare variable hours numeric(14,2);
  declare variable breakfast smallint;
  declare variable dinner smallint;
  declare variable supper smallint;
  declare variable citytransport smallint;
  declare variable toairport smallint;
  declare variable meals smallint;
  declare variable accommodation smallint;
  declare variable nottocount smallint;
  declare variable adayincountry smallint;
begin

  select e.hours, e.breakfast, e.dinner, e.supper, e.citytransport, e.toairport,
      e.meals, e.accommodation, e.nottocount, e.incountry
    from edeldays e
    where e.ref = :edelday
  into :hours, :breakfast, :dinner, :supper, :citytransport,  :toairport,
      :meals, :accommodation, :nottocount, :adayincountry;

  allow = 0;
  dlump = 0;
  accom = 0;

  if (nottocount <> 1) then
  begin
    -- wyliczenie diety za dzień   w % kwoty dniowej
      -- wyliczenie podstawy
    execute procedure edel_countdayvalue(:hours,  :daycnt,  :incountry)
      returning_values :allowbase;

      -- wyliczenie co odjac
    allowmeals = 0;

    if (incountry = 1) then
    begin
      if (breakfast = 1) then allowmeals = allowmeals + 25;
      if (dinner = 1) then allowmeals = allowmeals + 50;
      if (supper = 1) then allowmeals = allowmeals + 25;
    end else
    begin
      if (meals = 1) then allowmeals = 100;
    end

    if (allowbase > allowmeals) then
      allow = allowbase - allowmeals;
    else
      allow = 0;

    -- wyliczenie ryczatu za dojazdy za dzien
    dlump = 0;
    if (citytransport = 1) then dlump = dlump + 10;
    if (toairport = 1) then dlump = dlump + 100;

    -- wyliczenie ryczatu za noclegi   w % kwoty dniowej
    if (accommodation = 1) then
      accom = 150;
    else
      accom = 0;

    dayincountry = adayincountry;
  end

  suspend;
end^
SET TERM ; ^
