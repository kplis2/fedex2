--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_FUN_ROZRACHSALDO(
      COMPANY integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      ACCOUNTS ACCOUNT_ID,
      SIDE smallint)
  returns (
      AMOUNT numeric(15,2))
   as
declare variable lastdate timestamp;
  declare variable debit numeric(15,2);
  declare variable credit numeric(15,2);
  declare variable saldo_debit numeric(14,2);
  declare variable saldo_credit numeric(14,2);
begin
  select fdate from bkperiods where id = :period
    into :lastdate;
  accounts = replace(accounts,  '?',  '_') || '%';

  amount = 0;
  for
    select sum(winienzl), sum(mazl)
      from rozrachp
      where kontofk like :accounts
        and data <= :lastdate and company = :company
      group by kontofk, symbfak
      having sum(winienzl) <> sum(mazl)
      into :debit, :credit
  do begin
    saldo_debit = 0;
    saldo_credit = 0;

    if (abs(debit) > abs(credit)) then
      saldo_debit = debit - credit;
    else
      saldo_credit = credit - debit;

    if (side = 0) then
      amount = amount + saldo_debit;
    else
      amount = amount + saldo_credit;
  end
  suspend;
end^
SET TERM ; ^
