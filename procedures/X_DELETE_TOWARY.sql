--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_DELETE_TOWARY as
declare variable ktm ktm_id;
begin
  --mkd
  exception universal 'Wiesz co robisz???!!!';
  --in autonomous transaction do
  in autonomous transaction do
  for
    select ktm from towary o
      where 1=1
      into :ktm do
  begin
     delete from towpliki t where t.ktm = :ktm;
     delete from towary where ktm = :ktm;

  end
end^
SET TERM ; ^
