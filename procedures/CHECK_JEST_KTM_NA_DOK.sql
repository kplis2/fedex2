--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHECK_JEST_KTM_NA_DOK(
      DOKUMENT integer,
      POZYCJA integer,
      WERSJAREF integer,
      TYP char(1) CHARACTER SET UTF8                           ,
      DOSTAWA integer)
  returns (
      JEST smallint)
   as
declare variable cnt integer;
begin
  jest = 0;
  if(:dostawa=0) then dostawa = null;
  if(typ = 'F') then begin
     if(:pozycja > 0) then
       select count(*) from POZFAK where DOKUMENT = :DOKUMENT and REF<>:POZYCJA and WERSJAREF =:wersjaref and (DOSTAWA=:dostawa or (:dostawa is null)) into :cnt;
     else
       select count(*) from POZFAK where DOKUMENT = :DOKUMENT and WERSJAREF =:wersjaref and (DOSTAWA=:dostawa or (:dostawa is null)) into :cnt;
  end
  if(typ = 'Z') then begin
     if(:pozycja > 0) then
       select count(*) from POZZAM where ZAMOWIENIE = :DOKUMENT and REF<>:POZYCJA and WERSJAREF =:wersjaref and (DOSTAWAMAG=:dostawa or (:dostawa is null)) into :cnt;
     else
       select count(*) from POZZAM where ZAMOWIENIE = :DOKUMENT and WERSJAREF =:wersjaref and (DOSTAWAMAG=:dostawa or (:dostawa is null)) into :cnt;
  end

  if(typ = 'M') then begin

     if(:pozycja > 0) then
       select count(*) from DOKUMPOZ where DOKUMENT = :DOKUMENT and REF<>:POZYCJA and WERSJAREF =:wersjaref and (DOSTAWA=:dostawa or (:dostawa is null)) into :cnt;
     else
       select count(*) from DOKUMPOZ where DOKUMENT = :DOKUMENT and WERSJAREF =:wersjaref and (DOSTAWA=:dostawa or (:dostawa is null)) into :cnt;
  end

  if(:cnt > 0) then jest = 1;

  suspend;
end^
SET TERM ; ^
