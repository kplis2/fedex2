--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COUNT_OVERTIME(
      EMPLCALDAY integer,
      OVERTIME_TYPE smallint,
      EMPLWORKSTART time = null,
      EMPLWORKEND time = null,
      EMPLWORKSTART2 time = null,
      EMPLWORKEND2 time = null,
      DAYKIND smallint = 1)
  returns (
      OVERTIME0 integer,
      OVERTIME50 integer,
      OVERTIME100 integer,
      COMPENSATORY_TIME integer,
      NIGHTHOURS integer)
   as
declare variable EMPLWORKTIME integer;
declare variable NOMWORKTIME integer;
declare variable NORM_PER_DAY integer;
declare variable NIGHTHOURS_START time;
declare variable NIGHTHOURS_END time;
declare variable LIMIT100 integer;
declare variable COMPENSATED_TIME integer;
declare variable EIGHT_HOURS integer;
declare variable ECALENDAR integer;
begin
  --DS: funkcja wylicza godiny ponadwymiarowe, nadliczbowe, nocne i do odbioru
  --nie updatuje pol, wywolywany bezposrednio na formie edycji dnia kalendarza
  overtime0 = 0;
  overtime50 = 0;
  overtime100 = 0;
  nighthours = 0;
  --po jakim czasie nadgodziny 50 zmieniaja sie na nadgodziny 100
  limit100 = 24*60*60;
  eight_hours = 8 * 60 * 60;

  if (emplworkstart is null) then
  begin
    --wywola sie, gdzie godziny pracy sie nie zmienily a jedynie zmienia sie charakter godzin
    --np z godzin do odebrania na nadgodziny
    select ec.workstart, ec.workend, ec.workstart2, ec.workend2, edk.daytype
      from emplcaldays ec
        join edaykinds edk on edk.ref = ec.daykind
      where ec.ref = :emplcalday
      into :emplworkstart, :emplworkend, :emplworkstart2, :emplworkend2, :daykind;
  end

  --pobranie nominalnych wartosci dla danego dnia
  select c.nomworktime, c.compensated_time, cal.norm_per_day, cal.nighthours_start, cal.nighthours_end, cal.ref
    from emplcaldays c
      join ecalendars cal on cal.ref = c.ecalendar
    where c.ref = :emplcalday
    into :nomworktime, :compensated_time, :norm_per_day, :nighthours_start, :nighthours_end, :ecalendar;

  if (daykind <> 1) then nomworktime=0;

  compensatory_time = 0;  --BS52333 (wylaczenie ponizszego fragmentu)
/*
  --przy wyliczeniach uwzglednij godziny ktore juz zostaly odebrane
  --zeby nie naliczyc ponownie nadgodzin za godziny odebrane
  compensated_time = coalesce(compensated_time,0);
  compensatory_time = compensated_time;
  if (emplworkend2 is not null and emplworkend2 <> emplworkstart2) then
  begin
    if (emplworkend2 - emplworkstart2 > compensated_time) then
    begin
      emplworkend2 = emplworkend2 - compensated_time;
      compensated_time = 0;
    end
    else begin
      compensated_time = emplworkend2 - emplworkstart2;
      emplworkend2 = emplworkstart2; --??
    end
  end
  emplworkend = emplworkend - compensated_time;
*/

  --wyliczenie czasu pracy
  execute procedure count_worktime(emplworkstart,emplworkend,emplworkstart2,emplworkend2,daykind)
    returning_values emplworktime;

  --dla rownowaznego czasu pracy
  --jezeli pracownik ma w danym dniu do przepracowania wiecej niz 8 godzin,
  --to nadgodziny licza sie dopiero powyzej tego wymiaru
  --jezeli mniej, to powyzej 8 godziny (ewentualnie nadgodziny sredniotygodniowe)
  if (norm_per_day > eight_hours) then
  begin
    if (nomworktime < eight_hours) then
      norm_per_day = eight_hours;
    else norm_per_day = nomworktime;
  end
  --w sobote nadgodziny 100% powyzej normy dziennej
  --if (daykind = 2) then limit100 = norm_per_day;
  --w niedziele wszystkie sa setkami
  else if (daykind = 0) then limit100 = 0;
  --aby od razu liczyc nadgodziny
  if (daykind <> 1) then norm_per_day = 0;
  
  --sprawdzanie czy i jakie nadgodziny wystapily (lub ponadwymiarowe - overtime0)
  if (nomworktime < emplworktime) then
  begin
    if (emplworktime > norm_per_day) then
    begin
      --nie pamietam po co to tu bylo ale maci
      --overtime0 = norm_per_day - nomworktime;
      if (emplworktime > norm_per_day + limit100) then
      begin
        overtime50 = limit100;
        if (norm_per_day + overtime50 < emplworktime) then
          overtime100 = emplworktime - norm_per_day - overtime50;
      end
      else
        overtime50 = emplworktime - norm_per_day;
    end
    else
      overtime0 = emplworktime - nomworktime;
  end
  --gdy pracownik pracowal krocej niz powinien (odbiera godziny)
  else begin
    compensatory_time = compensatory_time + emplworktime - nomworktime;
  end

  execute procedure count_nighthours(:ecalendar,:emplworkstart,:emplworkend,:emplworkstart2,:emplworkend2)
    returning_values :nighthours;

  --jezeli nadgodziny 50% sa w porze nocnej, nalezy je przemianowac na setki
  if (emplworkstart >= nighthours_end and emplworkstart < nighthours_start
    and overtime50 > 0 and overtime100 < nighthours) then
  begin
    overtime50 = overtime50 - nighthours + overtime100;
    if (overtime50 < 0) then
    begin
      overtime100 = nighthours + overtime50;
      overtime50 = 0;
    end else
      overtime100 = nighthours;
  end

  --na koncu poniewaz najpierw nalezy przemianowac nadgodziny w nocy na 100%
  if (nomworktime < emplworktime) then
  begin
    --godziny do odbioru na wniosek pracodawcy
    if (overtime_type = 1) then
    begin
      compensatory_time = compensatory_time + overtime0 + 1.5 * overtime50 + 2 * overtime100;
      overtime0 = null; overtime50 = null; overtime100 = null;
    end
    --godziny do odbioru na wniosek pracownika
    else if (overtime_type = 2) then
    begin
      compensatory_time = compensatory_time + overtime0 + overtime50 + overtime100;
      overtime0 = null; overtime50 = null; overtime100 = null;
    end
  end
  suspend;
end^
SET TERM ; ^
