--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_PRINTLABEL_TEST(
      REF_MWSACT INTEGER_ID)
  returns (
      ZPL STRING1024)
   as
declare variable KTM KTM_ID;
declare variable PARTIA STRING10;
declare variable SERIAL STRING100;
declare variable ref_SLOWNIK INTEGER_ID;
declare variable ref_WERSJA INTEGER_ID;

begin
/*
  zpl = 'CT~~CD,~CC^~CT~
        ^XA~TA000~JSN^LT0^MNW^MTD^PON^PMN^LH0,0^JMA^PR6,6~SD15^JUS^LRN^CI0^XZ
        ^XA
        ^MMT
        ^PW799
        ^LL1199
        ^LS0
^FT328,82^AAR,63,25^FH\^FDSlownik:^FS
        ^PQ1,0,1,Y^XZ'   ;*/



  select ma.good, ma.x_partia, ma.x_serial_no, ma.x_slownik
    from mwsacts ma
    where ma.ref = :ref_mwsact
  into :ktm, :partia, :serial, :ref_slownik;

  select first 1 w.ref -- do poprawienia
    from wersje w
    where w.ktm = :ktm
  into :ref_wersja;
--exception universal 'jjbn';
  --execute procedure x_mws_printlabel(ref_wersja, ktm, partia, serial, ref_slownik)
 --   returning_values :ZPL;
  suspend;
end^
SET TERM ; ^
