--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SIODEMKA_EDEDOC(
      DOCREF INTEGER_ID,
      REQUEST SMALLINT_ID)
  returns (
      EDEDOCHREF INTEGER_ID)
   as
  declare variable operator operator_id;
  declare variable status smallint_id;
begin

  ededochref = 0;
  execute procedure get_global_param('AKTUOPERATOR') returning_values :operator;
  --select nazwa from operator where ref = :operator ;
  if (:request = 0) then begin
    select ref from ederules where symbol = 'SIODEMKA_LISTNADANIE' into ededochref;
    INSERT INTO EDEDOCSH
             (EDERULE, DIRECTION, REGDATE, REGOPER,  OTABLE, OREF, STATUS, CONFIRMPATH, MANUALINIT, REPLYEDEDOC, SIGNSTATUS)
      VALUES (:ededochref, 1, current_timestamp(0), :operator,  'LISTYWYSD', :docref, 0, 0, 0, NULL, 0);
     end
  if (:request = 1) then
    select ref from ederules where symbol = 'SIODEMKA_ETYKIETAEPL' into ededochref;
    INSERT INTO EDEDOCSH
             (EDERULE, DIRECTION, REGDATE, REGOPER,  OTABLE, OREF, STATUS, CONFIRMPATH, MANUALINIT, REPLYEDEDOC, SIGNSTATUS)
      VALUES (:ededochref, 1, current_timestamp(0), :operator,  'LISTYWYSD', :docref, 0, 0, 0, NULL, 0);
 -- if (ededochref > 0) then
    --execute procedure ededoc_process(:ededochref) returning_values :status;
end^
SET TERM ; ^
