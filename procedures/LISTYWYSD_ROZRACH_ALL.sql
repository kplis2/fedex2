--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_ROZRACH_ALL(
      DOKTYP char(1) CHARACTER SET UTF8                           ,
      SREF integer)
  returns (
      SLODEF integer,
      SLOPOZ integer,
      SYMBFAK varchar(20) CHARACTER SET UTF8                           ,
      KONTOFK KONTO_ID,
      WALUTA varchar(3) CHARACTER SET UTF8                           ,
      WINIEN numeric(14,2),
      MA numeric(14,2),
      DATAPLAT timestamp,
      LREF integer)
   as
begin
  for
    select ROZRACH.slodef, ROZRACH.slopoz, ROZRACH.symbfak, ROZRACH.kontofk, ROZRACH.waluta,
            0, NAGFAK.dozaplaty, rozrach.dataplat, :sref
      from ROZRACH join NAGFAK on (NAGFAK.ref=rozrach.faktura)
      left join PLATNOSCI on (PLATNOSCI.ref = NAGFAK.sposzap)
      where nagfak.listywysd = :sref and platnosci.naspedytora = 1
      into :slodef, :slopoz,  :symbfak, :kontofk, :waluta, :winien, :ma, :dataplat, :lref
  do begin
    suspend;
  end
  for
    select SLODEF, SLOPOZ, KONTOFK, SYMBFAK, WALUTA, WINIEN, MA, PAYDAY, :sref
      from rozrachroz
      where ROZRACHROZ.doktyp = :doktyp and ROZRACHROZ.dokref = :sref
      into :slodef, :slopoz, :kontofk, :symbfak, :waluta, :winien, :ma, :DATAPLAT, :lref
  do begin
    suspend;
  end
end^
SET TERM ; ^
