--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MCZ_MWSACTS2REALIZE(
      MWSORD MWSORDS_ID)
  returns (
      LOKACJA varchar(20) CHARACTER SET UTF8                           ,
      GOODNAME varchar(100) CHARACTER SET UTF8                           ,
      ILOSCCALK numeric(15,4),
      ILOSCZREAL numeric(15,4),
      MWSACT MWSACTS_ID,
      GOOD varchar(20) CHARACTER SET UTF8                           ,
      VERSNAME varchar(100) CHARACTER SET UTF8                           )
   as
begin
  for
    select ma.ref, ma.good, ma.quantity, ma.quantityc, m.symbol,
        w.nazwat, w.nazwa
      from mwsacts ma
        left join mwsconstlocs m on(m.ref = ma.mwsconstlocp)
        left join wersje w on (ma.vers = w.ref)
      where ma.mwsord = :mwsord
        and ma.status < 3 and ma.status > 0
      order by ma.number
      into :mwsact, :good, :ilosccalk, :ilosczreal, :lokacja, :goodname, :versname
  do
    suspend;
end^
SET TERM ; ^
