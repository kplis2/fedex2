--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_MWSORD_STATUS_FROMACTS(
      MWSACT integer,
      MWSORD integer,
      GROUPBY smallint,
      GROUPVAL varchar(40) CHARACTER SET UTF8                           ,
      STATUS smallint,
      PACKMETH varchar(60) CHARACTER SET UTF8                           )
  returns (
      POSQUANTITY integer)
   as
begin
  if (groupby = 0) then
    select count(ref)
      from mwsacts
      where mwsord = :mwsord and status <> :status and status <> 6
      into posquantity;
  else if (groupby = 1 and mwsord > 0 and mwsord is not null) then
    select count(ref)
      from mwsacts
      where mwsord = :mwsord and status <> :status and status <> 6
        and (good <> :groupval or (good = :groupval and packmeth = :packmeth))
      into posquantity;
  else if (groupby = 2 and mwsord > 0 and mwsord is not null) then
    select count(ref)
      from mwsacts
      where mwsord = :mwsord and status <> :status and status <> 6
        and (keya <> :groupval or (keya = :groupval and packmeth = :packmeth))
      into posquantity;
  else if (groupby = 3 and mwsord > 0 and mwsord is not null) then
    select count(ref)
      from mwsacts
      where mwsord = :mwsord and status <> :status and status <> 6
        and (keyb <> :groupval or (keyb = :groupval and packmeth = :packmeth))
      into posquantity;
  else if (groupby = 4 and mwsord > 0 and mwsord is not null) then
    select count(ref)
      from mwsacts
      where mwsord = :mwsord and status <> :status and status <> 6
        and (keyc <> :groupval or (keyc = :groupval and packmeth = :packmeth))
      into posquantity;
  if (posquantity is null) then
    posquantity = 0;
end^
SET TERM ; ^
