--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_CALC_POSITION(
      EMPLOYEE integer,
      PAYROLL integer,
      ALGTYPE integer,
      ECOLUMN integer)
   as
declare variable PVALUE numeric(16,2);
declare variable ALWAYSSHOW smallint;
declare variable BODY varchar(2048);
declare variable DECL varchar(1024);
declare variable ABLOCK varchar(4096);
declare variable COLTYPE varchar(3);
declare variable EALGID varchar(10);
begin

  select a.alwaysshow, a.decl, a.body, c.coltype
    from ealgorithms a
    join ecolumns c on (c.number = a.ecolumn)  --PR55498
    where a.ealgtype = :algtype and a.ecolumn = :ecolumn
    into :alwaysshow, :decl, :body, :coltype;

  ablock = 'execute block returns (ret numeric(14,2))
  as
    declare variable key1 integer;
    declare variable key2 integer;
  ' ||
    :decl ||
  'begin
    key1 = '||:employee||';
    key2 = '||:payroll||';
' || :body ||
  ' suspend;
  end';

  ealgid = algtype || '_' || ecolumn;

  execute procedure ealgorithm_run(ealgid, employee, payroll, ablock) --PR53533
    returning_values pvalue;

  if (pvalue is not null and coltype = 'NIE') then  --BS48457, PR55498
    alwaysshow = 1;
  else if (pvalue is null) then
    pvalue = 0;

  if (exists(select first 1 1 from eprpos
      where payroll = :payroll and employee = :employee and ecolumn = :ecolumn)
  ) then begin
    if (pvalue <> 0 or alwaysshow = 1) then
      update eprpos set pvalue = :pvalue, chgtimestamp = null, chgoperator = null
        where payroll = :payroll and employee = :employee and ecolumn = :ecolumn;
    else
      delete from eprpos
        where payroll = :payroll and employee = :employee and ecolumn = :ecolumn;
  end else begin
    if (pvalue <> 0 or alwaysshow = 1) then
      insert into eprpos (payroll, employee, ecolumn, pvalue /*, cauto*/) --PR53533
        values (:payroll, :employee, :ecolumn, :pvalue /*, 1*/);
  end
end^
SET TERM ; ^
