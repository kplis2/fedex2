--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_MAGTOZAM(
      ZAM integer)
  returns (
      REF integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      DATA timestamp,
      WARTOSCWZ numeric(10,4))
   as
begin
  for
    select D.REF, D.symbol, D.data, D.pwartosc
      from NAGZAM DOKZAM
      left join DOKUMNAG D on (D.ZAMOWIENIE = DOKZAM.REF)
      where (DOKZAM.REF = :zam) or (DOKZAM.org_ref = :zam)
      into :ref, :symbol, :data,
      :WARTOSCWZ
  do begin
    if(:ref is not null and :symbol is not null) then suspend;
  end
end^
SET TERM ; ^
