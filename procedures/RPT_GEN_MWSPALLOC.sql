--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_GEN_MWSPALLOC(
      LICZBA INTEGER_ID = 1)
  returns (
      ZPL STRING1024)
   as
declare variable MWSPALLOCSYMB STRING80;
declare variable MWSPALLOCREF INTEGER_ID;
begin
  if (:liczba is null) then
    liczba = 1;

  while (:liczba > 0)
  do begin
    execute procedure gen_ref('MWSPALLOCS') returning_values :mwspallocref;
    mwspallocsymb = 'P'||:mwspallocref;
    insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status,
        fromdocid, fromdocposid, fromdoctype)
      values (:mwspallocref, :mwspallocsymb, null, 2, -1,
        null, null, 'P');
    liczba = liczba - 1;
    ZPL = 'CT~~CD,~CC^~CT~
          ^XA~TA000~JSN^LT0^MNW^MTD^PON^PMN^LH0,0^JMA^PR5,5~SD15^JUS^LRN^CI0^XZ
          ^XA
          ^MMT
          ^PW639
          ^LL0400
          ^LS0
          ^BY4,3,145^FT493,147^BCI,,Y,N
          ^FD>:'||coalesce(mwspallocref, '')||'^FS
          ^PQ1,0,1,Y^XZ';

    suspend;
  end
end^
SET TERM ; ^
