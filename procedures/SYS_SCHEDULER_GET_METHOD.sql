--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_SCHEDULER_GET_METHOD(
      REF_IN SYS_SCHEDULE_ID)
  returns (
      METHOD INTEGER_ID,
      COMPILED SMALLINT_ID,
      EXEC_METHOD_BODY blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           ,
      ERROR_METHOD_BODY blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           ,
      REF SYS_SCHEDULE_ID,
      NAME STRING,
      ALIVE_TIMEOUT INTEGER_ID)
   as
declare variable NEXT_START TIMESTAMP_ID;
declare variable SCHEDULE_REF SYS_SCHEDULE_ID;
declare variable INTERVAL INTEGER_ID;
declare variable AND_OR SMALLINT_ID;
declare variable RUN SMALLINT_ID;
begin
  /*Procedura przegl..da scheduler w poszukiwaniu metod do uruchomienia*/
  for
    select S.REF, S.TIMING, S.NEXT_RUN, S.AND_OR, S.COMPILED, S.SOURCE_CODE,
           S.SOURCE_CODE_ERROR_HANDLING, S.NAME, S.alive_timeout
      from SYS_SCHEDULE S
      where S.REF = :REF_IN
      into :SCHEDULE_REF, :INTERVAL, :NEXT_START, :AND_OR, :COMPILED, :EXEC_METHOD_BODY,
           :ERROR_METHOD_BODY, :NAME, :ALIVE_TIMEOUT
  do begin
    REF = SCHEDULE_REF;

    if (not exists(select first 1 1
                     from SYS_SCHEDULE S
                     where S.REF = :SCHEDULE_REF
                       and S.lastinstance is not null)) then
    begin
      METHOD = SCHEDULE_REF;
      suspend;
    end
  end
end^
SET TERM ; ^
