--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_WYDHIST(
      MAGAZYNY varchar(255) CHARACTER SET UTF8                           ,
      MASKAKTM varchar(40) CHARACTER SET UTF8                           ,
      DATAOD date,
      DATADO date,
      ZEWN smallint)
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      WERSJAREF integer,
      DATA date,
      ILOSC numeric(14,4),
      ILOSCBRAK numeric(14,4),
      ILOSCINC numeric(14,4))
   as
begin
  if(:magazyny is null) then magazyny = '';
  if(:maskaktm is null) then maskaktm = '';
  iloscbrak = 0;
  iloscinc = 0;
  -- policz wydania magazynowe w okreslonym przedziale czasu
  for select p.ktm,p.wersja,p.wersjaref,p.data,
  p.ilosc*(2*p.wydania-1)    -- wydania na plus, zwroty na minus
  from dokumpoz p
  left join dokumnag n on (n.ref=p.dokument)
  where p.data>=:dataod and p.data<=:datado
  and (p.ktm like :maskaktm or :maskaktm='')
  and (:magazyny like '%;'||trim(n.magazyn)||';%')
  and n.akcept in (1,8)
  and (n.zewn=1 or :zewn=0)
  and ((n.wydania=1 and n.koryg=0) or (n.wydania=0 and n.koryg=1))
  into :ktm, :wersja, :wersjaref, :data, :ilosc
  do begin
    suspend;
  end
  -- dodaj w wyniku wszystkie ktmy, aby pokryc cala dziedzine
  ilosc = 0;
  data = :dataod;
  for select w.ktm, w.nrwersji, w.ref
  from wersje w
  where w.ktm like :maskaktm
  into :ktm, :wersja, :wersjaref
  do begin
    suspend;
  end
end^
SET TERM ; ^
