--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_MAKE_YEARCLOSING(
      COMPANY integer,
      FORYEAR integer)
   as
declare variable account ACCOUNT_ID;
  declare variable symbfak varchar(20);
  declare variable currency varchar(3);
  declare variable debit numeric(14,2);
  declare variable balance_debit numeric(14,2);
  declare variable currdebit numeric(14,2);
  declare variable balance_currdebit numeric(14,2);
  declare variable credit numeric(14,2);
  declare variable balance_credit numeric(14,2);
  declare variable currcredit numeric(14,2);
  declare variable balance_currcredit numeric(14,2);
  declare variable bo_debit numeric(14,2);
  declare variable bo_credit numeric(14,2);
  declare variable periodbo varchar(6);
  declare variable periodbz varchar(6);
  declare variable naccount ACCOUNT_ID;
  declare variable bkreg varchar(10);
  declare variable doctype integer;
  declare variable bkdoc integer;
  declare variable bkaccount integer;
  declare variable country_currency varchar(3);
  declare variable res smallint;
  declare variable issettlacc smallint;
  declare variable dist integer;
  declare variable decree integer;
  declare variable side smallint;
  declare variable autoaccept smallint;
  declare variable operator integer;
  declare variable bktype integer;
  declare variable balancetype smallint;
  declare variable sdate timestamp;
  declare variable multivaluedist smallint;
begin
  periodbo = cast(foryear + 1 as varchar(4)) || '00';
  update turnovers set debit = 0, credit = 0
    where period = :periodbo and company = :company;

  for
    select ref
      from decrees where period = :periodbo and company = :company and status > 1
      into :decree
  do
    execute procedure compute_turnovers(decree);

  select id from bkperiods where company = :company and yearid = :foryear and ptype = 2
    into :periodbz;

  execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
    returning_values country_currency;

  select min(symbol) from bkregs where company = :company and rtype = 0
    into :bkreg;
  if (bkreg is null) then
    exception FK_EXPT;

  select min(ref) from bkdoctypes where bkreg = :bkreg
    into :doctype;

  select sdate from bkperiods where company = :company and id = :periodbo
    into :sdate;

  execute procedure GEN_REF('BKDOCS') returning_values bkdoc;
  insert into bkdocs (ref, period, bkreg, number, transdate, docdate, symbol, doctype, descript, autobo, company, otable)
    values (:bkdoc, :periodbo, :bkreg, 1, :sdate, :sdate, 'BO/' || (:foryear + 1), :doctype, 'Automatyczny bilans otwarcia', 1, :company, :periodbz);

  for
    select A.account, A.currency, sum(T.debit), sum(T.credit), A.bkaccount, B.bktype, b.saldotype
      from accounting A
        join turnovers T on (T.accounting = A.ref)
        join bkaccounts B on (B.ref = A.bkaccount)
      where A.yearid = :foryear and A.company = :COMPANY
        and b.bktype <> 2 -- wylączone konta pozabilansowe jednoroczne
        and (A.currency = 'PLN' or B.multicurr = 1) -- wylączone przenoszenie sald walutowych z kont zlotowkowych
      group by A.account, A.currency, A.bkaccount, B.bktype, b.saldotype
      into :account, :currency, :debit, :credit, :bkaccount, :bktype, :balancetype
  do begin
    bo_debit = 0;
    bo_credit = 0;

    naccount = null;
    select naccount from bkaccontschng
      where oaccount = :account and yearid = :foryear
      and company = :company
      into :naccount;
    if (naccount is not null) then
      account = naccount;
    -- sprawdzenie czy saldo nie jest na koncie wynikowym
    if (debit - credit <> 0 and bktype = 1) then
      exception FK_EXPT 'Saldo na koncie wynikowym: ' || account || ' Zamknięcie roku niemożliwe!';

    if (abs(debit) > abs(credit)) then
    begin
      bo_debit = debit - credit;
      side = 0;
    end
    else begin
      bo_credit = credit - debit;
      side = 1;
    end

    if (balancetype = 0) then
    begin
      bo_debit = bo_debit - bo_credit;
      bo_credit = 0;
      side = 0;
    end else if (balancetype = 2) then
    begin
      bo_credit = bo_credit - bo_debit;
      bo_debit = 0;
      side = 1;
    end



    select B.settl
      from bkaccounts B
      where B.ref = :bkaccount
      into :issettlacc;

    if (issettlacc = 2) then
    begin
      for
        select symbfak, sum(winien), sum(ma), sum(winienzl), sum(mazl)
          from rozrachp
          where company = :company and kontofk = :account and waluta = :currency
           and data < cast((:foryear+1) as varchar(4)) || '-01-01'
          group by kontofk, symbfak, waluta
          having sum(winienzl) <> sum(mazl) or sum(winien) <> sum(ma)
          into :symbfak, :currdebit, :currcredit, :debit, :credit
      do begin
        balance_debit = 0;
        balance_credit = 0;

        if (abs(debit) > abs(credit)) then
        begin
          balance_debit = debit - credit;
          side = 0;
        end else
        begin
          balance_credit = credit - debit;
          side = 1;
        end

        if (abs(currdebit) > abs(currcredit)) then
        begin
          balance_currdebit = currdebit - currcredit;
          side = 0;
        end else
        begin
          balance_currcredit = currcredit - currdebit;
          side = 1;
        end

        if (currency = country_currency) then
          insert into decrees (bkdoc, account, side, debit, credit, settlement)
            values (:bkdoc, :account, :side, :balance_debit, :balance_credit, :symbfak);
        else
          insert into decrees (bkdoc, account, side, debit, credit, currdebit, currcredit, currency, settlement)
            values (:bkdoc, :account, :side, :balance_debit, :balance_credit, :balance_currdebit, :balance_currcredit, :currency, :symbfak);
      end
    end else
    begin

      if (bo_debit <> 0 or bo_credit <> 0) then
      begin
        execute procedure check_account(company, account, foryear + 1, 0)
          returning_values res, dist, dist, dist, dist, dist, dist, multivaluedist;
        if (currency = country_currency) then
          insert into decrees (bkdoc, account, side, debit, credit)
            values (:bkdoc, :account, :side, :bo_debit, :bo_credit);
        else
          insert into decrees (bkdoc, account, side, currdebit, currcredit, currency)
            values (:bkdoc, :account, :side, :bo_debit, :bo_credit, :currency);
      end
    end
  end

  -- automatyczna akceptacja po wystawieniu
  execute procedure get_global_param('AKTUOPERATOR') returning_values :operator;
  execute procedure GET_CONFIG('BOOK_WITH_ACCEPT', 2) returning_values :autoaccept;
  if (autoaccept = 1) then
    update bkdocs set status = 2, accoper = :operator, bookoper = :operator where ref = :bkdoc;
  else
    update bkdocs set status = 1, accoper = :operator where ref = :bkdoc;
end^
SET TERM ; ^
