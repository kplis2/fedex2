--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ADDBKACCOUNTSFROMPATTTODESTS(
      PATTERN_BKACCOUNTS varchar(4096) CHARACTER SET UTF8                           ,
      DEST_COMPANIES varchar(4096) CHARACTER SET UTF8                           )
   as
declare variable VAL_COMPANIES integer;
declare variable VAL_BKACCOUNT integer;
declare variable CURRENT_COMPANY COMPANIES_ID;
declare variable pattern_copany companies_id;
begin
  -- exception universal''||:pattern_bkaccounts||'  '||:dest_companies;
  -- ściągamy obecny zakład
  execute procedure get_global_param('CURRENTCOMPANY')
    returning_values :current_company;

  pattern_bkaccounts = substring( pattern_bkaccounts from 1 for char_length(pattern_bkaccounts));
  pattern_bkaccounts = substring( pattern_bkaccounts from 1 for char_length(pattern_bkaccounts));

  dest_companies = substring( dest_companies from 1 for char_length(dest_companies));
  dest_companies = substring( dest_companies from 1 for char_length(dest_companies));

  for
    select outstring
      from parse_lines(:dest_companies,';')
      into :val_companies
  do begin
    for
      select outstring
       from parse_lines(:pattern_bkaccounts,';')
        into :val_bkaccount
    do begin

      if (current_company = val_companies) then
      begin
        select company
          from bkaccounts
          where ref = :val_bkaccount
          into :pattern_copany;
      end else
        pattern_copany = current_company;
      -- exception universal''||:pattern_copany||'  '||:val_companies||' '||:val_bkaccount;
      execute procedure add_bkaccount_from_pattern(:pattern_copany,:val_companies,:val_bkaccount);
    end
  end
  suspend;
end^
SET TERM ; ^
