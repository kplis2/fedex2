--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE M_SETTLE_DECREEMATCHING(
      DECREEMATCHINGGROUP DECREEMATCHINGGROUPS_ID,
      MATCHINGMODE INTEGER_ID)
   as
declare variable sumcredit ct_amount;
declare variable sumdebit dt_amount;
declare variable coramount money;
declare variable tmpdecreematchinggroup decreematchinggroups_id;
declare variable mref decreematchings_id;
declare variable credit ct_amount;
declare variable debit dt_amount;
declare variable dref decrees_id;
begin
  -- weryfikacja czy można rozliczyc konta jest w kodzie
  -- jak nie odpalamy z kodu to trzeba wczesniej odpalic M_SETTLE_DECREEMATCHING_CHECK
  execute procedure insert_decreematchinggroup(:decreematchinggroup)
    returning_values(:decreematchinggroup);
  -- wszystkie rekordy do jednej grupy
  if (:matchingmode = 0) then
  begin
    update decreematchings m set m.decreematchinggroup = :decreematchinggroup, m.blockcalc = 1
      where m.matchinggroupid = :decreematchinggroup;
    execute procedure decreematchinggroup_calc(:decreematchinggroup);
  end
  -- wydzielamy tyle, żeby sie kompensowalo
  else if (:matchingmode = 1) then
  begin
    select sum(m.credit), sum(m.debit)
      from decreematchings m
      where m.matchinggroupid = :decreematchinggroup and m.decreematchinggroup is null
      into :sumcredit, :sumdebit;
    if (:sumcredit is null) then sumcredit = 0;
    if (:sumdebit is null) then sumdebit = 0;
    if (:sumcredit - :sumdebit > 0) then
    begin
      coramount = :sumcredit - :sumdebit;
      -- odpinamy najstarsze rekordy z grupy matchowania - potem do tego co zostanie ustawimy ref grupyrozliczeniowej
      for
        select m.ref, m.credit, m.decree
          from decreematchings m
            left join decrees d on (d.ref = m.decree)
          where m.matchinggroupid = :decreematchinggroup and m.credit > 0 and m.decreematchinggroup is null
          order by m.matchinggroupid desc, d.transdate desc
          into :mref, :credit, :dref
      do begin
        if (:credit <= :coramount) then
        begin
          update decreematchings m set m.matchinggroupid = null where m.ref = :mref;
          coramount = :coramount - :credit;
        end else if (:credit > :coramount) then
        begin
          update decreematchings m set m.credit = m.credit - :coramount where m.ref = :mref;
          execute procedure insert_decreematching(:dref,:coramount,0,1);
          coramount = 0;
        end
        if (:coramount <= 0) then
          break;
      end
    end else if (:sumdebit > :sumcredit) then
    begin
      coramount = :sumdebit - :sumcredit;
      -- odpinamy najstarsze rekordy z grupy matchowania - potem do tego co zostanie ustawimy ref grupyrozliczeniowej
      for
        select m.ref, m.debit, m.decree
          from decreematchings m
            left join decrees d on (d.ref = m.decree)
          where m.matchinggroupid = :decreematchinggroup and m.debit > 0 and m.decreematchinggroup is null
          order by m.matchinggroupid desc, d.transdate desc
          into :mref, :debit, :dref
      do begin
        if (:debit <= :coramount) then
        begin
          update decreematchings m set m.matchinggroupid = null where m.ref = :mref;
          coramount = :coramount - :debit;
        end else if (:debit > :coramount) then
        begin
          update decreematchings m set m.debit = m.debit - :coramount where m.ref = :mref;
          execute procedure insert_decreematching(:dref,0,:coramount,1);
          coramount = 0;
        end
        if (:coramount <= 0) then
          break;
      end
    end
    update decreematchings m set m.decreematchinggroup = :decreematchinggroup, m.blockcalc = 1
      where m.matchinggroupid = :decreematchinggroup and m.decreematchinggroup is null;
    execute procedure decreematchinggroup_calc(:decreematchinggroup);
  end
  -- dolaczamy do innej grupy rekordy nieprzypisane
  else if (:matchingmode = 2) then
  begin
    select first 1 m.decreematchinggroup
      from decreematchings m
      where m.matchinggroupid = :decreematchinggroup and m.decreematchinggroup is not null
      into :tmpdecreematchinggroup;
    if (:tmpdecreematchinggroup is not null) then
    begin
      update decreematchings m set m.decreematchinggroup = :tmpdecreematchinggroup, m.blockcalc = 1
        where m.decreematchinggroup is null and m.matchinggroupid = :decreematchinggroup;
      execute procedure decreematchinggroup_calc(:tmpdecreematchinggroup);
    end
  end
end^
SET TERM ; ^
