--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_CREATE_DOKUMNOT(
      FAK integer,
      DATA timestamp,
      AKCEPT smallint)
  returns (
      STATUS integer,
      DOKSYM varchar(255) CHARACTER SET UTF8                           )
   as
declare variable CENAMAG numeric(14,4);
declare variable ZAKUP integer;
declare variable POZREF integer;
declare variable KTM varchar(40);
declare variable WERSJA integer;
declare variable NUMER integer;
declare variable ILOSC numeric(14,4);
declare variable ILOSCFAK numeric(14,4);
declare variable PILOSCFAK numeric(14,4);
declare variable FROMDOKPOZ integer;
declare variable POZREFORG integer;
declare variable AKCEPTACJA integer;
declare variable NIENOTYKORZAK varchar(255);
declare variable ODDZIAL varchar(255);
declare variable AKTUODDZIAL varchar(255);
declare variable LOCATION integer;
declare variable AKTULOCAT integer;
declare variable POZTONOT integer;
declare variable NOTDOK integer;
declare variable DOKTOKOR integer;
declare variable DATANOTY timestamp;
declare variable OPERAKCEPT integer;
declare variable ILOSCMAG numeric(14,4);
declare variable DOKMAG integer;
declare variable DOKUMROZREF integer;
declare variable DOKUMPOZNEWREF integer;
declare variable DOKUMROZILOSC numeric(14,4);
declare variable SYMB varchar(40);
declare variable STATUS2 integer;
declare variable DOKUMROZCENA numeric(14,4);
declare variable DOKUMROZPCENA numeric(14,4);
declare variable MAGAZYN varchar(3);
declare variable KOREKTA smallint;
declare variable CENAPOKOR numeric(14,4);
declare variable SKAD integer;
declare variable REFK integer;
declare variable CENANOTZAKUPU numeric(14,4);
declare variable dataotrz timestamp;
declare variable company integer;
declare variable period varchar(40);
declare variable pstatus smallint;
begin
  status = 1;
  doksym = '';
  select NAGFAK.AKCEPTACJA, NAGFAK.ZAKUP,NAGFAK.operakcept, TYPFAK.korekta, NAGFAK.skad
  from TYPFAK join NAGFAK on ( typfak.SYMBOL = NAGFAK.TYP)
  where NAGFAK.REF =:fak
  into :akceptacja, :zakup, :operakcept, :korekta, :skad;

  if(:akceptacja is null) then akceptacja = 0;
  if(:akceptacja = 0) then exception FAK_DOK_CREATE_NOACK;
  if(:zakup = 0) then exit;
  execute procedure GETCONFIG('NIENOTYKORZAK') returning_values :nienotykorzak;
  if(:nienotykorzak is null) then nienotykorzak = '';
  if(:nienotykorzak = '1') then exit;
  execute procedure GETCONFIG('AKTUODDZIAL') returning_values :aktuoddzial;
  execute procedure GETCONFIG('AKTULOCAT') returning_values :aktulocat;
  -- wykonujemy tylko dla dokumentow zakupu
  for select POZFAK.REF, POZFAK.KTM, POZFAK.WERSJA, POZFAK.NUMER, POZFAK.MAGAZYN,
    POZFAK.ILOSCM,POZFAK.PILOSCM, POZFAK.CENAMAG, POZFAK.fromdokumpoz, POZFAK.REFK
    from POZFAK
    where POZFAK.DOKUMENT = :FAK and POZFAK.USLUGA <>1 --and POZFAK.MAGAZYN = :magazyn
    and POZFAK.OPK = 0
      and coalesce(POZFAK.FAKE,0) = 0
    order by POZFAK.NUMER
    into :pozref, :ktm, :wersja, :numer, :magazyn, :iloscfak, :piloscfak, :cenamag, :fromdokpoz, :refk
  do begin
    select ODDZIAL from DEFMAGAZ where symbol = :magazyn into :oddzial;
    select LOCATION,COMPANY from ODDZIALY where ODDZIAL=:oddzial into :location,:company;
    if(:location <> :aktulocat) then exception NAGFAK_DOKMAG_WRONGLOCATION;
    /*rózniece cen przed i po - na ilosci poprzednie nalezy wystawic note magazynowa*/
    if(:iloscfak > 0) then begin
       -- generowanie noty magazynowej do pozycji dok. magazynowego
       -- okreslenie pierwotnej pozycji faktury do korygowania (ma znaczenie jesli akceptujemy korekte zakupu)
       status = 0;
       for select pg.pozfak
         from pozfak_get_firstpoz_up(:pozref) pg
         left join POZFAK F on (pg.pozfak = F.REF)
         left join nagfak N on (N.ref = F.dokument)
         left join typfak t on (N.typ = t.symbol)
         where t.nieobrot = 0 and pg.pozfak>0
         into :pozreforg
       do begin
         status = 1;
         -- jesli to jest korekta to wez oryginalna ilosc z pozycji pierwszej faktury, aby nie klonowac pozycji PZ
         if(:pozreforg<>:pozref) then select iloscm from pozfak where ref=:pozreforg into :iloscfak;
         for select ref,ilosc
         from POZFAK_GET_DOKUMPOZ(:pozreforg)
         where ilosc>0
         into :poztonot,:iloscmag
         do begin
           /*okrelenie, czy jest już zalozona nota mgazynowa na dany dokument*/
           notdok = null;
           select max(DOKUMNOT.ref) from DOKUMNOT join DOKUMNAG on (DOKUMNAG.REF=DOKUMNOT.dokumnagkor)
           join DOKUMPOZ on (DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
           where DOKUMPOZ.REF = :poztonot and DOKUMNOT.FAKTURA = :fak and DOKUMNOT.akcept = 0
           into :notdok;
           if(:notdok is nulL) then notdok = 0;

           /*okreslenie rozpisek korygowanych*/
           if(:iloscfak<:iloscmag) then ilosc = :iloscfak;
           else ilosc = :iloscmag;

           if(:ilosc>0) then begin
             if(:ilosc<:iloscmag) then begin
               execute procedure DOKUMPOZ_CLONE(:poztonot,:ilosc) returning_values :dokumpoznewref;
             end
             --pozycja w calosci nadaje sie do skorygowania, bierzemy wszystkie rozpiski
             for select ref,ilosc,cena,pcena
             from dokumroz
             where pozycja=:poztonot
             into :dokumrozref,:dokumrozilosc,:dokumrozcena,:dokumrozpcena
             do begin
               cenanotzakupu = 0;
               select sum(p.cenanew - p.cenaold)
               from dokumnotp p
               left join dokumnot d on (d.ref=p.dokument)
               where p.dokumrozkor=:dokumrozref
               and d.rodzaj=0
               and d.ref=d.grupa
               and d.akcept=1
               into :cenanotzakupu;
               if(:cenanotzakupu is null) then cenanotzakupu = 0;

               -- nowa cena magazynowa wynika ze zwiekszenia biezacej ceny magazynowej (dokumrozcena)
               -- o biezaca cene z faktury minus cena pierwotna minus te noty ktore juz uwzgledniaja
               -- zmiany ceny z faktur i korekt zakupu, ale nie te ktore pochodza z faktur kosztowych
               cenapokor = :dokumrozcena + (:cenamag - :dokumrozpcena - :cenanotzakupu);
               -- jesli to faktura korygujaca zakup, to nie patrz na ostateczna cene magazynowa (cenamag)
               -- ale na roznice, bo mogly juz powstac koszty z innego tytulu (sad, faktury uslugowe)
/*               if(:skad in (0,1) and :refk is null) then
                 cenapokor = :cenamag;
               else
                 cenapokor = (:cenamag-:pcenamag)+:dokumrozcena;*/
               -- jesli ceny sa rozne to zrob pozycje noty
               if(:dokumrozcena<>:cenapokor) then begin
                 if(:notdok = 0) then begin
                   execute procedure GEN_REF('DOKUMNOT') returning_values :notdok;
                   select DOKUMENT from DOKUMPOZ where ref=:poztonot into :doktokor;
                   select DATA,MAGAZYN from DOKUMNAG where REF=:doktokor into :datanoty,:magazyn;
                   if(:data>:datanoty) then datanoty = :data;

                   -- jesli okres ksiegowy dla tej daty jest zamkniety to bierzmy do noty date otrzymania faktury
                   select dataotrz from nagfak where ref = :fak into :dataotrz;
                   execute procedure date2period(:datanoty,:company) returning_values :period;
                   select p.status from bkperiods p where p.id=:period and p.company=:company into :pstatus;
                   if (:pstatus=2 and :dataotrz is not null and :dataotrz > :datanoty) then datanoty = :dataotrz;

                   -- zakladamy note
                   insert into DOKUMNOT(ref, GRUPA, TYP,MAGAZYN, MAG2, DATA, DOKUMNAGKOR, FAKTURA, UWAGI, OPERATOR, RODZAJ)
                   select :notdok, :notdok, DOKUMNAG.TYP, DOKUMNAG.MAGAZYN, DOKUMNAG.MAG2, :datanoty, :doktokor, :fak, substring(DOKUMNAG.uwagi from 1 for 255), :operakcept, 0
                   from DOKUMNAG where dokumnag.ref = :doktokor;
                 end
                 insert into DOKUMNOTP(DOKUMENT, DOKUMROZKOR, CENANEW)
                 values (:notdok, :dokumrozref, :cenapokor); /*nowa cena na pozycji tow. dok. VAT*/
               end
               ilosc = :ilosc - :dokumrozilosc;
             end
           end
           if(:ilosc>0) then exception DOKUMNOT_AKC_EXPT 'Brak wolnej rozpiski do skorygowania na '||:ilosc||' z '||:ktm;
         end
       end
    end
  end

  doksym = '';
  if(:akcept =1 or (:akcept = 3)) then begin
    for select ref from DOKUMNOT where FAKTURA = :fak and GRUPA = REF
    into :dokmag
    do begin
      if(exists(select ref from dokumnotp where dokument=:dokmag)) then begin
        update DOKUMNOT set AKCEPT = 1, BLOKADA = bin_and(BLOKADA,4) where ref = :dokmag;
        select symbol from dokumnot where ref=:dokmag into :symb;
        if(:symb is null) then symb = '';
        if (coalesce(char_length(:doksym||'; '||:symb),0) > 255) then begin -- [DG] XXX ZG119346
          doksym = substring(:doksym||'...' from 1 for 255);
        end else begin
          if(:doksym<>'') then doksym = :doksym||'; ';
          doksym = :doksym||:symb;
        end
      end else begin
        delete from DOKUMNOT where ref = :dokmag;
      end
    end
  end else begin
    /*zdjecie blokady do redagowania*/
    for select ref from DOKUMNOT where FAKTURA = :fak and GRUPA = REF
    into :dokmag
    do begin
      update DOKUMNOT set BLOKADA = bin_and(BLOKADA,4) where ref = :dokmag;
    end
  end
end^
SET TERM ; ^
