--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZFAK_CLEAR_DOKUMPOZ(
      POZFAKREF integer,
      DOKUMPOZREF integer)
   as
declare variable cnt integer;
declare variable dokumnagref integer;
declare variable nagfakref integer;
begin
  select dokument from dokumpoz where ref=:dokumpozref into :dokumnagref;
  --odwiaz pozycje
  update dokumpoz set frompozfak = NULL where ref=:dokumpozref and frompozfak=:pozfakref;
  select count(*) from dokumpoz where frompozfak=:pozfakref into :cnt;
  if(:cnt=1) then
    update pozfak set fromdokumpoz = :dokumpozref where ref=:pozfakref;
  else
    update pozfak set fromdokumpoz = null where ref=:pozfakref;
  --odwiaz naglowki, poprzez skasowanie znacznika faktura, jesli juz nie ma powiazania przez pozycje
  select dokument from pozfak where ref=:pozfakref into :nagfakref;
  cnt = 0;
  select count(dokumpoz.ref) from dokumpoz
    join pozfak on (dokumpoz.frompozfak = pozfak.ref)
    where dokumpoz.dokument = :dokumnagref and pozfak.dokument = :nagfakref
    into :cnt;
  if(:cnt=0) then update dokumnag set faktura = null where ref=:dokumnagref and faktura=:nagfakref;
end^
SET TERM ; ^
