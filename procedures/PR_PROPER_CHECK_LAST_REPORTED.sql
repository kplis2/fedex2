--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_PROPER_CHECK_LAST_REPORTED(
      PRSCHEDOPER integer)
  returns (
      DEPTO smallint)
   as
declare variable oref integer;
declare variable reported smallint;
begin
  depto = 0;
  for
    select d.depto
      from prschedoperdeps d
        left join prschedopers o on (o.ref = d.depto)
      where d.depfrom = :prschedoper and o.activ > 0
      into oref
  do begin
    reported = null;
    select o.reported
      from prschedopers o
      where o.ref = :oref and o.activ > 0
      into reported;
    if (reported is null) then reported = 0;
    if (reported > 0) then
    begin
      depto = 1;
      exit;
    end else
    begin
      execute procedure pr_proper_check_last_reported(:oref)
        returning_values depto;
      if (depto = 1) then
        exit;
    end
  end
end^
SET TERM ; ^
