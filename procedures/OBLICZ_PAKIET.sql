--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OBLICZ_PAKIET(
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      KLIENT integer,
      ILOSC numeric(14,4),
      POZREF integer,
      POZTYP char(1) CHARACTER SET UTF8                           ,
      MAG varchar(3) CHARACTER SET UTF8                           )
  returns (
      PKTM varchar(40) CHARACTER SET UTF8                           ,
      PWERSJAREF integer,
      PNAZWA varchar(255) CHARACTER SET UTF8                           ,
      WNAZWA varchar(255) CHARACTER SET UTF8                           ,
      WOPIS varchar(255) CHARACTER SET UTF8                           ,
      ILOSCZA numeric(14,4),
      ILOSCDOST numeric(14,4),
      ILOSCDOK numeric(14,4),
      DOST smallint,
      GRATIS smallint,
      CENNIK integer,
      CENNIKNAZWA varchar(255) CHARACTER SET UTF8                           ,
      RABAT numeric(14,4),
      REFPROM integer)
   as
declare variable cnt integer;
declare variable typprom char(1);
declare variable cechaprom varchar(30);
declare variable cechatabela varchar(30);
declare variable cechapole varchar(30);
declare variable cecharodzaj integer;
declare variable wartcechyprom varchar(255);
declare variable getprom integer;
declare variable ktmmaska varchar(40);
declare variable odktm varchar(40);
declare variable doktm varchar(40);
declare variable curprior integer;
declare variable lastprior integer;
declare variable grupakli integer;
declare variable iloscdouzycia numeric(14,4);
begin
  lastprior = -1;
  if(:mag is null) then mag = '';
  if(:klient < 0) then begin
    grupakli = - :klient;
    klient = 0;
  end else begin
    select GRUPA from KLIENCI where REF=:KLIENT into :grupakli;
    if(:grupakli is null) then grupakli = 0;
  end

    -- przejrzyj liste regul, ktore sa promocjami pakietowymi
    for select PROMOCJE.REF, PROMOCJE.TYP, PROMOCJE.CECHA, PROMOCJE.WARTCECHY, PROMOCJE.KTMMASKA, PROMOCJE.ODKTM, PROMOCJE.DOKTM, PROMOCJE.PRIORYTET
      from PROMOCJE
        join PROMOKLI on (PROMOCJE.REF = PROMOKLI.PROMOCJA)
       where TABRAB=1 AND ((PROMOKLI.GRUPAKLI = :grupakli) or (PROMOKLI.KLIENT=:KLIENT))
        and (PROMOCJE.MAGAZYN is null or PROMOCJE.MAGAZYN = '' or PROMOCJE.MAGAZYN = :mag) and PROMOCJE.PROMTYP=4
        and coalesce(PROMOCJE.DATAOD,current_timestamp(0)-1) < current_timestamp(0)
        and coalesce(PROMOCJE.DATADO, current_timestamp(0)+1) > current_timestamp(0)-1
      order by PROMOCJE.PRIORYTET DESC
      into :refprom, :typprom, :cechaprom, :wartcechyprom, :ktmmaska, :odktm, :doktm, :curprior
    do begin
      getprom = 0;
      if(:curprior is null) then curprior = 0;
      if(:curprior>=:lastprior) then begin
        if(:typprom = 'T') then begin
          cnt = null;
          select count(*) from PROMOCT where PROMOCJA=:refprom AND (:KTM like KTM) into :cnt;
          if(:cnt is null) then cnt = 0;
          if(:cnt > 0) then begin
            getprom = 1;
          end
        end else if(:typprom = 'G') then begin
          cnt = null;
          select DEFCECHY.rodzaj, DEFCECHY.tabela, DEFCECHY.pole from DEFCECHY where DEFCECHY.symbol=:cechaprom into :cecharodzaj, :cechatabela, :cechapole;
          if(:cecharodzaj = 1) then begin
            select count(*) from ATRYBUTY where KTM=:KTM and NRWERSJI=:WERSJA AND CECHA=:cechaprom AND WARTOSC=:wartcechyprom into :cnt;
          end
          if(:cecharodzaj = 0 and :cechatabela='TOWARY' and :cechapole='GRUPA') then begin
            select count(*) from TOWARY where KTM=:KTM and GRUPA=:wartcechyprom into :cnt;
          end
          if(:cnt is null) then cnt = 0;
          if(:cnt > 0) then getprom = 1;
        end else if(:typprom = 'Z') then begin
          getprom = 1;
          if(:getprom=1 and :odktm is not null and :odktm<>'' and :KTM<:odktm) then getprom = 0;
          if(:getprom=1 and :doktm is not null and :doktm<>'' and :KTM>:doktm) then getprom = 0;
          if(:getprom=1 and :ktmmaska is not null and :ktmmaska<>'' and not(:KTM like :ktmmaska)) then getprom = 0;
        end else if(:typprom = 'U') then begin
          getprom = 1;
        end
      end
      -- jesli regula pasuje zarowno pod towar jak i klienta to zobacz jakie mamy promocje pakietowe
      if(:getprom > 0) then begin
        -- przejdz po liscie towarow w promocji pakietowej
        for
          select P.ktm, P.wersja, P.iloscza, P.gratis, P.cennik, P.rabat, W.nazwat, W.nazwa, W.opis, D.nazwa
          from promop P
          left join wersje W on (W.ref=p.wersja)
          left join defcennik D on (D.ref=p.cennik)
          where P.promocja = :refprom
          into :pktm, :pwersjaref, :iloscza, :gratis, :cennik, :rabat, :pnazwa, :wnazwa, :wopis, :cenniknazwa
        do begin
            iloscdouzycia = :ilosc;
            -- sprawdzamy ile juz przyznano na dokumencie
            iloscdok = 0;
            if(:poztyp='F') then begin
              select sum(pozfak.ilosc*pozfak.ilosczadopakiet)
                from POZFAK
                where REFDOPAKIET=:pozref
              into :iloscdok;
            end else if(:poztyp='M') then begin
              select sum(dokumpoz.ilosc*dokumpoz.ilosczadopakiet)
                from DOKUMPOZ
                where REFDOPAKIET=:pozref
              into :iloscdok;
            end else if(:poztyp='Z') then begin
              select sum(pozzam.ilosc*pozzam.ilosczadopakiet)
                from POZZAM
                where REFDOPAKIET=:pozref
              into :iloscdok;
            end
            if(:iloscdok is null) then iloscdok = 0;
            -- zmniejszenie ilosci o to ile juz wykorzystano na dokumencie
            iloscdouzycia = :iloscdouzycia - :iloscdok;
            -- obliczamy ilosc dostepna w promocji pakietowej
            if(:iloscza=0) then iloscdost = :iloscdouzycia;
            else iloscdost = cast(floor(:iloscdouzycia/:iloscza) as integer);
            -- obliczamy znacznik dostepnosci
            if(:iloscdost>0) then dost = 1;
            else dost = 0;
          suspend;
        end
      end
    end
end^
SET TERM ; ^
