--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_ADDCUSTOMER_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable shippingdoc integer;
declare variable grandparent integer;
declare variable subparent integer;
declare variable deliverytype integer;
declare variable accesscode string80;
declare variable cref integer;
declare variable companyname varchar(150);
declare variable iscompany char(1);
declare variable taxid nip;
declare variable fname varchar(50);
declare variable sname varchar(90);
declare variable city city_id;
declare variable postcodeval postcode;
declare variable countryid country_id;
declare variable street street_id;
declare variable houseno nrdomu_id;
declare variable localno nrdomu_id;
declare variable phone phones_id;
declare variable email emails_id;
declare variable fax phones_id;
declare variable mobile phones_id;
declare variable dropshippingsender dostawca_id;
declare variable branch oddzial_id;
begin
  select l.ref, l.sposdost, l.oddzial, 0 -- [XXX] DG  DROPS l.dropshippingsender
    from listywysd l
    where l.ref = :oref
  into :shippingdoc, :deliverytype, :branch, :dropshippingsender;

  if (:branch is null) then
    execute procedure getconfig('AKTUODDZIAL') returning_values :branch;
  if (:branch is null) then
    exception universal 'Nie ustawiono oddziału';

  --sprawdzanie czy istieje dokument spedycyjny
  if(shippingdoc is null) then exception ede_fedex 'Brak listy spedycyjnej.';

  val = null;
  parent = null;
  params = null;
  id = 0;
  name = 'dodajKlienta';
  suspend;

  grandparent = :id;

  --dane autoryzacyjne
  select k.klucz
    from spedytwys w
      left join spedytkonta k on (w.spedytor = k.spedytor)
    where w.sposdost = :deliverytype
      and k.oddzial = :branch
  into :accesscode;

  val = :accesscode;
  name = 'kodDostepu';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = 'PRODUCTION'; --jesli chcesz testowac zmien na TEST
  name = 'serviceUrlType';
  id = id + 1;
  parent = GrandParent;
  suspend;

  val = null;
  name = 'kontrahent';
  params = 'xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"';
  parent = grandparent;
  id = id + 1;
  suspend;

  subparent = id;

  if (coalesce(:dropshippingsender,0) = 0) then
  begin
    select k.ref, substr(k.nazwa, 1, 150), k.firma, k.nip, substr(k.imie, 1, 50), substr(k.nazwisko, 1, 90),
        substr(k.miasto, 1, 70), k.kodp, k.krajid, substr(k.ulica, 1, 70), k.nrdomu, k.nrlokalu,
        k.telefon, k.email, k.fax, k.komorka
      from klienci k
      where k.ref = :oref
    into :cref, :companyname, :iscompany, :taxid, :fname, :sname,
      :city, :postcodeval, :countryid, :street, :houseno, :localno,
      :phone, :email, :fax, :mobile;
  end
  else
  begin
    select d.ref, substr(d.nazwa, 1, 150), d.firma, d.nip,
        substr(d.miasto, 1, 70), d.kodp, d.krajid, substr(d.ulica, 1, 70), d.nrdomu, d.nrlokalu,
        d.telefon, d.email, d.fax
      from dostawcy d
      where d.ref = :dropshippingsender
    into :cref, :companyname, :iscompany, :taxid,
      :city, :postcodeval, :countryid, :street, :houseno, :localno,
      :phone, :email, :fax;
  end

  val = :cref;
  name = 'nrExt';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  val = :companyname;
  name = 'nazwa';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  val = :iscompany;
  name = 'czyFirma';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  val = :taxid;
  name = 'nip';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  val = :fname;
  name = 'imie';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  val = :sname;
  name = 'nazwisko';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  val = :city;
  name = 'miasto';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  val = :postcodeval;
  name = 'kod';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  val = :countryid;
  name = 'kodKraju';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  val = :street;
  name = 'ulica';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  val = :houseno;
  name = 'nrDom';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  val = :localno;
  name = 'nrLokal';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  val = :phone;
  name = 'telKontakt';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  val = :phone;
  name = 'telKontakt';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  val = :email;
  name = 'emailKontakt';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  val = :fax;
  name = 'fax';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  val = :mobile;
  name = 'telefonKom';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;

  val = '0'; --tylko 0
  name = 'czyNadawca';
  params = null;
  parent = subparent;
  id = id + 1;
  suspend;
end^
SET TERM ; ^
