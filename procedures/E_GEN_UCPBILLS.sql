--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GEN_UCPBILLS(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      FROMNAME varchar(60) CHARACTER SET UTF8                           ,
      TONAME varchar(60) CHARACTER SET UTF8                           ,
      CONTRTYPE integer,
      ISGENEMPTY smallint,
      GENNOTEXISTS smallint,
      COMPANY integer,
      CPER varchar(6) CHARACTER SET UTF8                           ,
      PERSON2BILL integer = null,
      BILLDATE date = null,
      PAYDAY date = null,
      LUMPTAX smallint = null,
      STATUS smallint = null)
  returns (
      PERSON integer,
      PERSONNAMES varchar(120) CHARACTER SET UTF8                           ,
      BILLSUMSALARY numeric(14,2),
      UCPSUMSALARY numeric(14,2),
      MSGLOGFORM varchar(200) CHARACTER SET UTF8                           ,
      CALCSTATUS smallint)
   as
declare variable FROMDATE timestamp;
declare variable TODATE timestamp;
declare variable CONTRFROMDATE date;
declare variable SNAME varchar(30);
declare variable FNAME varchar(60);
declare variable SYMBOL varchar(20);
declare variable COUNTER integer;
declare variable CONTRSUMSALARY numeric(14,2);
declare variable ALLBILLSUMSALARY numeric(14,2);
declare variable CONTRACT integer;
declare variable PAYROLL integer;
declare variable EMPLOYEE integer;
declare variable TPER varchar(6);
begin
/*
MWr: Personel - do generowanie rachunkow do umow cywilnoprawnych.

  Procedura obslugiwana jest z kodu zrodlowego i dziala w dwoch przebiegach.
  W pierwszym wywolaniu tworzona jest lista osob z umowami cywilnoprawnymi, opcjonalnie
  zadanego typu CONTRTYPE, w zdanym okresie PERIOD, z zadanym zakresem nazwisk FROMNAME-TONAME (PR54412)
  W drugim przebiegu wystawiane sa rachunki, gdzie:
    PERSON2BILL - ref osoby, dla ktorej wystawiamy rachunki (pobieramy z pierwszego przebiegu).
       Jezeli nie jest okreslone mamy do czynienia z pierwszym przebiegiem procedury;
    BILLDATE - data wystawienia rachunku;
    PAYDATE - data wyplaty;
    LUMPTAX -  jezeli UCPSUMSALARY <= 200 to pojawia sie pytanie czy liczymy podatek
       zryczaltowany, jezeli tak to LUMPTAX = 1, w przeciwnych przypadkach LUMPTAX = 0;
    STATUS - domyslny status dla wystawianych rachunkow: 1-zakceptowane, 0-niezakceptowane;
  W obu przebiegach parametr:
    ISGENEMPTY okresla czy uwzgledniamy umowy o zerowej kwocie wynagrodzenia i czy
       dopuszczamy generowanie rachunkow zerowych (jezeli tak to param. = 1).
    GENNOTEXISTS = 1 pozwala nie generowac rachuku do umowy, jezeli sa juz do niej
       wystawione jakies rachunki w okresie CPER.
    CPER okresla okres kosztowy dla wystaweianych rachunkow.

  Zmienne wyjsciowe:
    BILLSUMSALARY - kwota wystawionych rachunkow w zadanym okresie. Generujue pytanie,
       czy kontynuwac naliczenie rachunkow, jezeli juz jakies istnieja;
    UCPSUMSALARY - omowione przy LUMPTAX;
    MSGLOGFORM - tekst jaki pojawi sie w oknie raportu dla danej pozycji;
    CALCSTATUS - status dla operacji generowania rachunku: 1-0K, 0-wystapil wyjatek;
*/

  fromname = upper(trim('-' from trim(coalesce(fromname, ''))));
  toname = upper(trim('-' from trim(coalesce(toname, ''))));
  contrtype = coalesce(contrtype, 0);
  isgenempty = coalesce(isgenempty, 1);
  company = coalesce(company, 1);
  person2bill = coalesce(person2bill,0);
  lumptax = coalesce(lumptax, 0);
  status = coalesce(status, 0);
  gennotexists = coalesce(gennotexists, 0);
  if (billdate is null) then billdate = current_date;
  if (payday is null) then payday = current_date;
  counter = 0;

  execute procedure e_func_periodinc(period,0)returning_values period; --normalizacja i kontrola okresow
  execute procedure e_func_periodinc(cper,0)returning_values cper;
  execute procedure e_func_periodinc(null,0,payday) returning_values tper;
  execute procedure period2dates(period) returning_values fromdate, todate;

  if (person2bill = 0) then
  begin
  --Tworzenie listy zleceniobiorcow
    for
      select e.person, e.sname, e.fname, sum(coalesce(c.csalary,0))
        from employees e
        join emplcontracts c on (c.employee = e.ref)
        join empltypes t on (t.ref = c.empltype and t.browsetype = 2) --PR64361 
        left join epayrolls r on (r.emplcontract = c.ref and r.cper = :cper and :gennotexists = 1)
        where e.company = :company
          and (:fromname = '' or substring(upper(sname) from 1 for coalesce(char_length(:fromname),0)) >= :fromname COLLATE UNICODE) --bez collate wybor nazwisk od R uwzgledni np Ł  -- [DG] XXX ZG119346
          and (:toname = '' or substring(upper(sname) from 1 for coalesce(char_length(:toname),0)) <= :toname COLLATE UNICODE) -- [DG] XXX ZG119346
          and c.fromdate <= :todate
          and (c.todate >= :fromdate or c.todate is null)
          and (c.econtrtype = :contrtype or :contrtype = 0)
          and (c.csalary > 0 or :isgenempty = 1)
          and (r.ref is null or :gennotexists = 0)
        group by e.person, e.sname, e.fname
        order by e.sname, e.fname 
        into :person, :sname, :fname, :contrsumsalary
    do begin
      personnames = sname || ' ' || fname;

      billsumsalary = null;
      allbillsumsalary = null;
      select sum(case when r.tper = :tper then p.pvalue else 0 end),
             sum(case when :gennotexists = 0 and r.cper = :cper and (c.econtrtype = :contrtype or :contrtype = 0) then p.pvalue else 0 end)
        from epayrolls r
          join eprpos p on (p.payroll = r.ref)
          join eprempl e on (e.epayroll = p.payroll and e.employee = p.employee)
          join emplcontracts c on (c.ref = r.emplcontract)
          join empltypes t on (t.ref = c.empltype and t.browsetype = 2) --PR64361
        where e.person = :person 
          and p.ecolumn = 3000
          and (r.tper = :tper or r.cper = :cper)
        into :allbillsumsalary, :billsumsalary;
      billsumsalary = coalesce(billsumsalary,0); --badane do komunikatu o wystawionych umowach w okresie
      ucpsumsalary = coalesce(allbillsumsalary,0) + contrsumsalary; --badane do naliczenia pod. ryczaltowego

      counter = counter + 1;
      MsgLogForm = counter || ') Wystawiam rachunki dla: ' || :personnames;

      suspend;
    end
  end else
  begin
  --Wystawienie i obliczenie rachunkow dla danej osoby
    for
      select c.ref, c.employee, e.sname || ' ' || e.fname, coalesce(c.csalary,0), c.fromdate
        from employees e
        join emplcontracts c on (c.employee = e.ref)
        join empltypes t on (t.ref = c.empltype and t.browsetype = 2) --PR64361 
        left join epayrolls r on (r.emplcontract = c.ref and r.cper = :cper and :gennotexists = 1)
        where e.company = :company
          and c.fromdate <= :todate
          and (c.todate >= :fromdate or c.todate is null)
          and (c.econtrtype = :contrtype or :contrtype = 0)
          and (c.csalary > 0 or :isgenempty = 1)
          and (r.ref is null or :gennotexists = 0)
          and e.person = :person2bill
        order by e.personnames
        into :contract, :employee, :personnames, :contrsumsalary, :contrfromdate
    do begin
      CalcStatus = 1;

      execute procedure gen_ref('EPAYROLLS')
        returning_values payroll;

      insert into epayrolls (ref, prtype, cper, tper, iper, name, descript, payday,
           billdate, pvalue, emplcontract, stable, status, esymbol, advance, company, lumpsumtax)
        values (:payroll, 'UCP', :cper, :tper, :tper, '', '', :payday, :billdate,
           :contrsumsalary, :contract, '', 0, '', 0, :company, :lumptax);
    
      execute procedure e_calculate_payment(:employee,:payroll)
        returning_values :person;  --wykorzystanie nieistotnej (w danej chwili) zmiennej integer

      select trim(symbol) from epayrolls where ref = :payroll
        into :symbol;

      if (symbol = '') then
        symbol = null;

      if (status > 0) then
        update epayrolls set status = :status where ref = :payroll;

      MsgLogForm = 'utworzono ' || coalesce(symbol,'rachunek') || ' do umowy z ' || contrfromdate || ' na kwotę ' || contrsumsalary || ' zł';
      if (lumptax > 0) then MsgLogForm = MsgLogForm || ' (podat. ryczałt.)';
      suspend;

      when any do begin
        CalcStatus = 0;
        MsgLogForm = 'założenie rachunku nie powiodło się!';
      --exception; --odkomentowanie pozwoli na wglad w wyjatek
        suspend;
      end
    end
  end
end^
SET TERM ; ^
