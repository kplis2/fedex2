--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE VAT_BY_DOCDATE(
      OTABLE varchar(30) CHARACTER SET UTF8                           ,
      OREF integer,
      DATADOK timestamp,
      TERMPLAT timestamp)
  returns (
      VATDATE timestamp)
   as
begin
/*PL: Procedura zwraca date vat jako date dokumentu przekazana
      w parametrze datadok(PR57428)
*/
    vatdate = datadok;
  suspend;
end^
SET TERM ; ^
