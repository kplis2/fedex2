--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_BTRANSFERPOS(
      BTRANSFERREF integer)
  returns (
      REF integer,
      BTRANSFER integer,
      ACCOUNT ACCOUNT_ID,
      SETTLEMENT varchar(20) CHARACTER SET UTF8                           ,
      MAXAMOUNT numeric(14,2),
      AMOUNT numeric(14,2),
      COLOR integer)
   as
declare variable status integer;
begin
  select status from BTRANSFERS where REF = :btransferref into :status;
  for select REF,BTRANSFER, ACCOUNT, SETTLEMENT, MAXAMOUNT, AMOUNT
  from btransferpos
  where btransfer = :btransferref
  into :ref, :btransfer, :account, :settlement, :maxamount, :amount
  do begin
    if(:status < 2) then
      execute procedure btransfers_checkmaxamount(:ref) returning_values :maxamount;
    if(:maxamount >= :amount) then
      Color = 0;
    else
      Color = 1;
    suspend;
  end
end^
SET TERM ; ^
