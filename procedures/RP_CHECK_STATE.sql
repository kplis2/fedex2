--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RP_CHECK_STATE(
      TABELA varchar(31) CHARACTER SET UTF8                           ,
      PARAM1 varchar(10) CHARACTER SET UTF8                           ,
      PARAM2 varchar(10) CHARACTER SET UTF8                           ,
      KEY1 varchar(65) CHARACTER SET UTF8                           ,
      KEY2 varchar(65) CHARACTER SET UTF8                           ,
      KEY3 varchar(65) CHARACTER SET UTF8                           ,
      KEY4 varchar(65) CHARACTER SET UTF8                           ,
      KEY5 varchar(65) CHARACTER SET UTF8                           ,
      CURSTATE integer)
  returns (
      STATE integer)
   as
declare variable location varchar(255) ;
  declare variable locationbit varchar(255) ;
  declare variable locatupr varchar(255);
  declare variable slocat varchar(255);
  declare variable locat2 integer;
  declare variable tryb varchar(255);
  declare variable locatchange integer;
  declare variable locatchange2 integer;
  declare variable i integer;
  declare variable locat2bit integer;
begin
  locatchange = 0;

  if (curstate<-10) then
  begin
    locatchange = 0 - curstate - 10;
    curstate = 0;
  end

  if (curstate>65535) then
  begin
    locatchange2 = curstate / 65536;
    curstate = bin_and(curstate, 65535);
    locatchange = bin_or(locatchange, locatchange2);
  end

  if (curstate<0) then
    curstate = 0;

  execute procedure GETCONFIG('AKTULOCAT')
    returning_values location;
  execute procedure GETCONFIG('AKTULOCATBIT')
    returning_values locationbit;
  execute procedure GETCONFIG('AKTULOCATTRYB')
    returning_values tryb;

  if (location<>'') then
  begin
    if (tabela='PLATNOSCI' or tabela='DEFCENNIK'
        or tabela='SLODEF' or tabela = 'KLIENCI'
        or tabela='URZYKLI') then
    begin
      select max(tolocat), max(tolocat2) from RP_SYNCSTATE
        where tabela=:TABELA and (fromlocat=:location or fromlocat=0
          or (fromlocat=-1 and :tryb='M') or (fromlocat=-2 and :tryb='S'))
        into :state, :locat2;

      if (locat2 is null) then
        locat2 = 0;

      if (param1='1' and param2='0' and locat2 > 0) then
      begin
        i = 1;
        locat2bit = 1;
        while (locat2>i) do
        begin
          locat2bit = locat2bit * 2;
          i = i + 1;
        end
        execute procedure RP_DESYNC_2(:tabela, :KEY1, :KEY2, :key3, :key4,
          :key5, :locat2bit);

      end else
        if (param2='0') then
          locat2 = 0;
    end else if (tabela='DOKUMNAG' or tabela='NAGFAK' or tabela='DOKUMNOT') then
    begin
      select max(tolocat), max(tolocat2) from RP_SYNCSTATE
        where tabela=:TABELA and (fromlocat=:location or fromlocat=0
          or (fromlocat=-1 and :tryb='M') or (fromlocat=-2 and :tryb='S'))
        into :state, :locat2;
      if ((param1='1' or param1='8') and param2<>'1' and param2<>'8') then
        execute procedure RP_DESYNC_2(:tabela, :KEY1, :KEY2, :key3, :key4,
          :key5, :state);
      if (param2<>'1' and param2<>'8') then
      begin
        state = null;
        curstate = 0;
      end
      locat2 = 0;
    end else if (tabela='RKRAPKAS') then
    begin
      select max(tolocat), max(tolocat2) from RP_SYNCSTATE
        where tabela=:TABELA and (fromlocat=:location or fromlocat=0
          or (fromlocat=-1 and :tryb='M') or (fromlocat=-2 and :tryb='S'))
        into :state, :locat2;
      if (param1<>'0' and param2='0') then
        execute procedure RP_DESYNC_2(:tabela,:KEY1, :KEY2, :key3, :key4,
          :key5, :state);
      if (param2='0') then
      begin
        state = null;
        curstate = 0;
      end
      locat2 = 0;
    end else if (tabela='ROZRACH') then
    begin
      select max(tolocat), max(tolocat2) from RP_SYNCSTATE
        where tabela=:TABELA and (fromlocat=:location or fromlocat=0
          or (fromlocat=-1 and :tryb='M') or (fromlocat=-2 and :tryb='S'))
        into :state, :locat2;
      if (param1<>'1' and param1<>'6') then
        state = null;
      locat2 = 0;
    end else if (tabela='SLOPOZ') then
    begin
      select max(tolocat), max(tolocat2) from RP_SYNCSTATE
        where tabela=:TABELA and (fromlocat=:location or fromlocat=0
          or (fromlocat=-1 and :tryb='M') or (fromlocat=-2 and :tryb='S'))
        into :state, :locat2;
      if (param1<>'1') then
        state = null;
      locat2 = 0;
    end else
    begin
      select max(tolocat), max(tolocat2) from RP_SYNCSTATE
        where tabela=:TABELA and (fromlocat=:location or fromlocat=0
          or (fromlocat=-1 and :tryb='M') or (fromlocat=-2 and :tryb='S'))
        into :state, :locat2;
      if (locat2 is null) then
        locat2 = 0;
    end

    if (locat2<0 and state<0) then
    begin
      if (state=-3) then
        state = locat2;
      else
        if (locat2<>state) then
          state = 0;
    end

    if (state=-3) then
      state = null; --brak celu

    --przeksztalecenie odpowiednie kodu state
    if (state=-1) then --*do lokalizacji centrali
      execute procedure GETCONFIG('AKTULOCATMBIT')
        returning_values :slocat;
    else if (state=-2) then -- do wszystkich lokalizacji oprocz centrali
      execute procedure GETCONFIG('AKTULOCATNOTMBIT')
        returning_values :slocat;
    else if (state=0) then  --do wszystkich lokalizacji
      execute procedure GETCONFIG('AKTULOCATSUMBIT')
        returning_values :slocat;

    if (slocat<>'')then
    begin
      --wyciecie z celu wlasnej lokalizacji
      state = cast(slocat as integer);
      if ((state / cast(locationbit as integer) / 2 * 2) <> (state / cast(:locationbit as integer))) then
        state = state - cast(locationbit as integer);
    end

    --dodanie lokalizacji drugiej
    if (locat2>0) then
    begin
      i = 1;
      locat2bit = 1;
      while (locat2>i) do
      begin
        locat2bit = locat2bit * 2;
        i = i+1;
      end
      state = bin_or(state, locat2bit);
    end

    --wyciecie lokalizacji, kora naniosla zmiany
    if (state>0 and locatchange>0) then
    begin
      if (state/locatchange/2*2 <> state/locatchange) then
        state = state - locatchange;
    end
    --dodanie satrej wartosci state
    state = bin_or(state, curstate);
  end

  if (state is null or state<0) then
    state = 0;

end^
SET TERM ; ^
