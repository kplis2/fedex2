--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_REALOPER(
      FROMDOKUM integer,
      OPERACJA char(3) CHARACTER SET UTF8                           ,
      TYP char(1) CHARACTER SET UTF8                           )
   as
  declare variable nagzam type of nagzam_id;
  declare variable status smallint;
  declare variable histzam type of histzam_id;
  declare variable operator type of operator_id;
  declare variable skad smallint;
  declare variable ilpoz integer;
  declare variable dysppart integer;
  declare variable operdore integer;
  declare variable operdodp integer;
  declare variable operdodb integer;
  declare variable operdodk integer;
  declare variable operdodd integer;
  declare variable rerozpiszzawsze smallint;
  declare variable rerozpisz smallint;
  declare variable dprozpisz smallint;
  declare variable dbrozpisz smallint;
  declare variable dkrozpisz smallint;
  declare variable ddrozpisz smallint;
  declare variable wsadowo smallint;
  declare variable dore smallint;
  declare variable dodp smallint;
  declare variable dodb smallint;
  declare variable dodk smallint;
  declare variable dodd smallint;
  declare variable rezam integer;
  declare variable dpzam integer;
  declare variable dbzam integer;
  declare variable dkzam integer;
  declare variable ddzam integer;
  declare variable nagzamdatamag date;
  declare variable statusr integer;
  declare variable kodyzam varchar(300);
  declare variable symbolezam varchar(300);
  declare variable symboledysp varchar(255);
  declare variable pos smallint;
  declare variable reznotmoveonklon smallint;
  declare variable sql varchar (512);
  declare variable stan smallint;
  declare variable dorej varchar(255);
  declare variable kstan smallint;
  declare variable znak char(1);
  declare variable kznak char(1);
  declare variable tostan integer;
  declare variable tokstan integer;
  declare variable torej char(3);
  declare variable rerej char(3);
  declare variable dprej char(3);
  declare variable dbrej char(3);
  declare variable dkrej char(3);
  declare variable ddrej char(3);
  declare variable rodzaj smallint;
  declare variable lzam integer;
  declare variable rekstan smallint;
  declare variable restan smallint;
  declare variable dpstan smallint;
  declare variable dbstan smallint;
  declare variable dkstan smallint;
  declare variable ddstan smallint;
  declare variable rejestr char(3);
  declare variable dorejestr char(3);
  declare variable nstan varchar(1);
  declare variable nkstan varchar(1);
  declare variable opis varchar(400);
  declare variable id varchar(30);
  declare variable mainzam integer;
  declare variable oldzam integer;
  declare variable zamreffordysp varchar(255);
  declare variable rozpisz smallint;
  declare variable pref integer;
  declare variable popref integer;
begin
  select d.rerozpiszzawsze, d.rerozpisz, d.dprozpisz, d.dbrozpisz, d.dkrozpisz, d.ddrozpisz, d.wsadowo,
        d.realizacja, d.dyspp, d.dyspb, d.dyspk, d.dyspd, d.reznotmoveonklon, d.stan, d.dorej, d.kstan,
        d.rerej, d.dprej, d.dbrej, d.dkrej, d.ddrej, d.rekstan, d.restan, d.dpstan, d.dbstan,
        d.dkstan, d.ddstan, d.wsadowo, coalesce(d.rozpisz,0)
    from defoperzam d
    where d.symbol = :operacja
    into :rerozpiszzawsze, :rerozpisz, :dprozpisz, :dbrozpisz, :dkrozpisz , :ddrozpisz, :wsadowo,
        :dore, :dodp, :dodb, :dodk, :dodd, :reznotmoveonklon, :stan, :dorej, :kstan,
        :rerej, :dprej, :dbrej, :dkrej, :ddrej, :rekstan, :restan, :dpstan, :dbstan,
        :dkstan, :ddstan, :wsadowo, :rozpisz;

  if(:wsadowo = 0) then
    exception universal 'Realizacja czŕťciowa niedozwolona w trybie wsadowym';
  if(:rozpisz = 0) then
    exception universal 'Uzupe-nij warunek czŕťciowego rozpisywania zamˇwie˝ na definicji operacji.';

  execute procedure get_global_param('AKTUOPERATOR')
    returning_values :operator;
  nagzamdatamag = current_date;

  opis = '';
  if(:typ = 'M') then       -- zamˇwienia na podstawie DOKUMPOZˇw
  begin
    sql = ' select distinct n.skad, n.rejestr, n.stan, n.kstan, n.ref, n.id ';
    sql = sql || ' from dokumpoz dp ';
    sql = sql || '   left join pozzam pz on (pz.ref = dp.frompozzam)';
    sql = sql || '   left join nagzam n on (pz.zamowienie = n.ref) ';
    sql = sql || ' where dp.dokument = '||coalesce(:fromdokum,0);
    sql = sql || '   and dp.frompozzam is not null ';
  end
  else if(:typ = 'F') then  -- zamˇwienia na podstawie POZFAKˇw
  begin
    sql = ' select distinct n.skad, n.rejestr, n.stan, n.kstan, n.ref, n.id ';
    sql = sql || ' from pozfak p ';
    sql = sql || '   left join pozzam pz n on (pz.ref = p.frompozzam) ';
    sql = sql || '   left join nagzam n on (pz.zamowienie = n.ref) ';    
    sql = sql || ' where p.dokument = '||coalesce(:fromdokum,0);
    sql = sql || '   and p.frompozzam is not null ';
  end
  for execute statement sql
    into :skad, :rejestr, :nstan, :nkstan, :nagzam, :id
  do begin
    mainzam = :nagzam;
    oldzam = :nagzam;
    histzam = 0;
    rezam = 0;
    dpzam = 0;
    dbzam = 0;
    dkzam = 0;
    ddzam = 0;

    -- 1. Wpis do historii
    execute procedure oper_create_hist_zam(:nagzam, :operacja, :operator, :skad, :fromdokum, 0, 0)
      returning_values :status, :histzam;
    if(status <> 1) then
      exception universal 'Rejestracja historii operacji zamˇwienia ' || :id || ' zako˝czona niepowodzeniem';

    /* Metoda TOperZam::Dyspozycja() - poczatek wywolania */

    -- 2. Ustalenie ilosci na podstawie dokumentu
    execute procedure oper_iloscifromdok (:nagzam, :fromdokum, :typ, :histzam, :operator)
      returning_values :ilpoz;
    if(ilpoz = 0) then
    begin
      delete from histzam where ref = :histzam;
      histzam = 0;
      break;
      /* UWAGA!! Czy omijamy te pozycje czy wyrzucamy exception dla calego dokumentu ??!!! */
    end

    -- 3. Sprawdzenie, czy rzeczywiscie jest operacja wykonywana
    operdore = 0;
    operdodp = 0;
    operdodb = 0;
    operdodk = 0;
    operdodd = 0;

    execute procedure oper_dysp_checkreal(:nagzam, :operacja)
      returning_values :dysppart, :operdore, :operdodp, :operdodb, :operdodk, :operdodd;
    if(((:operdore = 1 and :rerozpiszzawsze = 1) or :operdodp = 1 or :operdodd = 1) and :dysppart = 1) then
    begin
      opis = :opis || 'Czŕťciowa realizacja/dyspozycja wybrana automatycznie.';
    end
    else if(:dysppart = 1 and
        ((:operdore = 0 or :rerozpisz = 1)
          and (:operdodp = 0 or :dprozpisz = 1)
          and (:operdodb = 0 or :dbrozpisz = 1)
          and (:operdodk = 0 or :dkrozpisz = 1)
          and (:operdodd = 0 or :ddrozpisz = 1))) then
    begin
      if(:rozpisz = 2) then   -- zgodnosc z kodem w c++: 1- pozostaw do pˇčniejszej realizacji, 2-realizacja zamowienia w calosci, 3-anuluj
        dysppart = 0;
      else if(:rozpisz = 3) then
        exception universal 'Dla podanej operacji realizacja czŕťciowa niedozwolona.';
    end
    else
    begin
      dysppart = 0;
    end
    -- 4. Uruchomienie realizacji czesciowej
    opis = :opis || 'Realizacja zamˇwienia ' || :id || ' w trybie wsadowym';
    if(operdore = 0) then
    begin
      dore = 0;
      rerozpisz = 0;
    end
    if(operdodp = 0) then
    begin
      dodp = 0;
      dprozpisz = 0;
    end
    if(operdodb = 0) then
    begin
      dodb = 0;
      dbrozpisz = 0;
    end
    if(operdodk = 0) then
    begin
      dodk = 0;
      dkrozpisz = 0;
    end
    if(operdodd = 0) then
    begin
      dodd = 0;
      ddrozpisz = 0;
    end
    if(dore = 1 and rerozpisz = 0) then
      rezam = :oldzam;
    if(dodp = 1 and dprozpisz = 0) then
      dpzam = :oldzam;
    if(dodb = 1 and dbrozpisz = 0) then
      dbzam = :oldzam;
    if(dodk = 1 and dkrozpisz = 0) then
      dkzam = :oldzam;
    if(dodd = 1 and ddrozpisz = 0) then
      ddzam = :oldzam;
    if((:rezam + :dpzam + :dbzam + :dkzam + :ddzam) > :oldzam) then
      exception universal 'Brak mo¬liwoťci odpowiedniego rozpisania zamˇwienia ' || :id || ' na zamˇwienia czŕťciowe';
    if(:dysppart = 0) then
    begin
      if(:dodd = 1 and :ddzam = 0) then
        ddzam = :oldzam;
      else if(:dodk = 1 and :dkzam = 0) then
        dkzam = :oldzam;
      else if(:dodb = 1 and :dbzam = 0) then
        dbzam = :oldzam;
      else if(:dodp = 1 and :dpzam = 0) then
        dpzam = :oldzam;
      else if(:dore = 1 and :rezam = 0) then
        rezam = :oldzam;
    end
    if(:dore = 0) then
      rezam = -1;
    if(:dodp = 0) then
      dpzam = -1;
    if(:dodb = 0) then
      dbzam = -1;
    if(:dodk = 0) then
      dkzam = -1;
    if(:dodd = 0) then
      ddzam = -1;
    execute procedure oper_realizacja(:oldzam, :histzam, :operacja, :rezam, :dpzam, :dbzam, :dkzam, :ddzam, 0, :nagzamdatamag)
      returning_values :statusr, :kodyzam, :symbolezam, :symboledysp, :zamreffordysp;
    if(:statusr = -1) then
      exception universal 'Nie powiod-o siŕ za-o¬enie realizacji czŕťciowej do zamˇwienia ' || :id;

    -- 5. Podlaczenie dokumentu magazynowego
    else if(:statusr > 0) then
    begin
      pos = position(';' in :kodyzam);
      rezam = cast((substring(:kodyzam from 1 for pos-1)) as integer);
      kodyzam = substring(:kodyzam from  :pos + 1 for coalesce(char_length(:kodyzam),0)); -- [DG] XXX ZG119346
      pos = position(';' in :kodyzam);
      dpzam = cast((substring(:kodyzam from 1 for pos-1)) as integer);
      kodyzam = substring(:kodyzam from  :pos + 1 for coalesce(char_length(:kodyzam) , 0)); -- [DG] XXX ZG119346
      pos = position(';' in :kodyzam);
      dbzam = cast((substring(:kodyzam from 1 for pos-1)) as integer);
      kodyzam = substring(:kodyzam from  :pos + 1 for coalesce(char_length(:kodyzam) , 0)); -- [DG] XXX ZG119346
      pos = position(';' in :kodyzam);
      dkzam = cast((substring(:kodyzam from 1 for pos-1)) as integer);
      kodyzam = substring(:kodyzam from  :pos + 1 for coalesce(char_length(:kodyzam) , 0)); -- [DG] XXX ZG119346
      ddzam = cast((substring(:kodyzam from 1 for coalesce(char_length(:kodyzam) , 0))) as integer); -- [DG] XXX ZG119346
      if(:mainzam <> :rezam and :rezam > 0) then
        mainzam = :rezam;
      if(:statusr = 2) then
        opis = opis || 'Wystawiono realizacje/dyspozycje czŕťciowe o symbolu: ' || :symbolezam;
      else if(:rezam > 0) then
        opis = opis || 'Zamˇwienie zosta-o zaznaczone jako zrealizowane.';
      if(:symboledysp <> '') then
        opis = opis || 'Wystawiono nastŕpuj¦ce dyspozycje: ' || :symboledysp;

      -- 6. Podl¦czenie dokumentu magazynowego
      if(:fromdokum > 0 and :typ = 'M' and :rezam > 0) then       -- M.S czy ten warunek powinien byc taki skoro mamy dokument maagzynowy
        execute procedure oper_realizacja_joinmagtozam(:histzam, :mainzam, :fromdokum);

      if(:mainzam <> :oldzam and :fromdokum > 0 and :reznotmoveonklon = 0) then
      begin
        if(:typ = 'M') then
        begin
          update dokumpoz set fromzam = :mainzam where dokument = :fromdokum and fromzam = :oldzam;
        end
        else
          update pozfak set fromzam = :mainzam where dokument = :fromdokum and fromzam = :oldzam;
      end

    /* Metoda TOperZam::Dyspozycja() - koniec wywolania */

      -- 7. Ustawienie stanu zamˇwienia
--      if(:mainzam <> :oldzam) then
      begin
        if (((:stan > 0) or (:dorej <> '') or (:kstan > 0))
          and (:oldzam <> :rezam and :oldzam <> :dpzam and :oldzam <> :dbzam and :oldzam <> :oldzam and :oldzam <> :ddzam) ) then
        begin
          znak = '';
          znak =
            case
              when :stan = 1 then 'R'
              when :stan = 2 then 'B'
              when :stan = 3 then 'D'
              when :stan = 4 then 'N'
            end;
          if (:znak <> '') then
            update nagzam set stan = :znak where ref = case when coalesce(:mainzam,0) <> :nagzam and coalesce(:mainzam,0) > 0 then :mainzam else :nagzam end;
          znak = '';
          znak =
            case
              when :kstan = 1 then 'R'
              when :kstan = 2 then 'B'
              when :kstan = 3 then 'D'
              when :kstan = 4 then 'N'
            end;
          if(:znak <> '') then
            update nagzam set kstan = :znak where ref = case when coalesce(:mainzam,0) <> :nagzam and coalesce(:mainzam,0) > 0 then :mainzam else :nagzam end;
          if(:dorej <> '') then
          begin
            update nagzam set rejestr = :dorej where ref = case when coalesce(:mainzam,0) <> :nagzam and coalesce(:mainzam,0) > 0 then :mainzam else :nagzam end;
            opis = :opis || 'Zamˇwienie przeniesiono do rejestru: ' || :dorej;
          end
        end
        rodzaj = 0;
        tostan = 0;
        tokstan = 0;
        while (:rodzaj < 5)
        do begin
          if(:rodzaj = 0) then
          begin
            tostan = :restan;
            torej = :rerej;
            tokstan = :rekstan;
            lzam = :rezam;
          end
          if(:rodzaj = 1) then
          begin
            tostan = :dpstan;
            torej = :dprej;
            lzam = :dpzam;
          end
          if(:rodzaj = 2) then
          begin
            tostan = :dbstan;
            torej = :dbrej;
            lzam = :dbzam;
          end
          if(:rodzaj = 3) then
          begin
            tostan = :dkstan;
            torej = :dkrej;
            lzam = :dkzam;
          end
          if(:rodzaj = 4) then
          begin
            tostan = :ddstan;
            torej = :ddrej;
            lzam = :ddzam;
          end
          if(:lzam > 0) then
          begin
            if(:tostan > 0 and :torej <> '' and :tokstan > 0) then
            begin
              if(:rejestr = :torej) then
                dorejestr = '';
              else dorejestr = :torej;
              znak =
                case
                  when :tostan = 1 then 'R'
                  when :tostan = 2 then 'B'
                  when :tostan = 3 then 'D'
                  when :tostan = 4 then 'N'
                end;
              if(:znak = :nstan) then
                znak = '';
              kznak =
                case
                  when :tokstan = 1 then 'R'
                  when :tokstan = 2 then 'B'
                  when :tokstan = 3 then 'D'
                  when :tokstan = 4 then 'N'
                end;
              if(:kznak = :nkstan) then
                kznak = '';
              if(:dorejestr <> '' or :znak <> '' or :kznak <> '') then
              begin
                if(:znak <> '') then
                  update nagzam set stan = :znak where ref = case when coalesce(:mainzam,0) <> :nagzam and coalesce(:mainzam,0) > 0 then :mainzam else :nagzam end;
                if(:kznak <> '') then
                  update nagzam set kstan = :kznak where ref = case when coalesce(:mainzam,0) <> :nagzam and coalesce(:mainzam,0) > 0 then :mainzam else :nagzam end;
                if(:dorejestr <> '') then
                begin
                  update nagzam set rejestr = :torej where ref = case when coalesce(:mainzam,0) <> :nagzam and coalesce(:mainzam,0) > 0 then :mainzam else :nagzam end;
                  opis = :opis || 'Zamˇwienie przeniesiono do rejestru: ' || :dorejestr;
                end
              end
            end
          end
          rodzaj = rodzaj + 1;
        end
      end
    end

    -- 8. Uzupelnienie opisu historii
    if(:histzam > 0) then
      update histzam set opis = :opis where ref = :histzam;
  end
end^
SET TERM ; ^
