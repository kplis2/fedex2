--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MSGORD_CREATE(
      TYP varchar(20) CHARACTER SET UTF8                           ,
      TYPDOK varchar(1) CHARACTER SET UTF8                           ,
      REFDOK integer,
      OPIS varchar(5000) CHARACTER SET UTF8                           ,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      EMAIL varchar(255) CHARACTER SET UTF8                           ,
      OPERATOR integer)
  returns (
      REF integer)
   as
begin
  execute procedure GEN_REF('MSGORDS') returning_values :ref;
  insert into MSGORDS(REF, TYP, OPIS, TYPDOK, REFDOK, STATUS, DATA,TOEMAIL,SYMBOL,OPERATOR)
  values (:ref, :typ, :opis, :typdok, :refdok, 0, current_date,:EMAIL,:SYMBOL,:OPERATOR);
end^
SET TERM ; ^
