--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_RECALC_RELATIONS(
      STABLE varchar(40) CHARACTER SET UTF8                           ,
      SREF integer,
      QUANTITY numeric(14,4),
      RELATION integer,
      MODE smallint)
   as
declare variable rref integer;
declare variable pref integer;
declare variable relationdef integer;
declare variable quantityfromtmp numeric(14,4);
declare variable quantitytotmp numeric(14,4);
declare variable quantityfrom numeric(14,4);
declare variable quantityto numeric(14,4);
declare variable quantitytoreal numeric(14,4);
declare variable kilkalk numeric(14,4);
declare variable kilosc numeric(14,4);
declare variable shortages numeric(14,4);
declare variable prshmat integer;
begin
  if (relation = 0) then relation = null;
  if (stable = 'POZZAM' and quantity <> 0 and mode = 0) then
  begin
    if (relation is not null) then
    begin
      update relations set quantityfrom = quantityfrom + :quantity where ref = :relation;
    end else if (relation is null and quantity < 0) then
    begin
      -- odejmuje z pierwszego lepszego pozzama
      for
        select r.ref, r.relationdef, r.quantityfrom, r.quantityto
          from relations r
          where r.stablefrom = :stable and r.sreffrom = :sref and r.act > 0
          into rref, relationdef, quantityfrom, quantityto
      do begin
        if (abs(quantity) > quantityfrom) then
          quantityfromtmp = -quantityfrom;
        else
          quantityfromtmp = quantity;
        quantitytotmp = quantityfromtmp;
        if (relationdef = 2) then
        begin
          select coalesce(p.kilorg,1), coalesce(p.kilcalcorg,1), coalesce(p.shortages,1)
            from pozzam p
            where p.ref = :sref
            into kilosc, kilkalk, shortages;
          quantitytotmp = quantitytotmp * kilkalk;
          quantitytotmp = quantitytotmp / shortages;
          quantitytotmp = quantitytotmp / kilosc;
        end
        update relations set quantityfrom = quantityfrom + :quantityfromtmp,
            quantityto = quantityto + :quantitytotmp
          where ref = :rref;
        quantity = quantity - quantityfromtmp;
        if (quantity = 0) then
          break;
      end
    end else if (relation is null and quantity > 0) then
    begin
      -- moge dodawac tylko do relacji niespelnionych pozzamow
      for
        select r.ref, r.relationdef, r.srefto, p.prshmat,
            coalesce(p.kilorg,1), coalesce(p.kilcalcorg,1), coalesce(p.shortages,1)
          from relations r
            left join pozzam p on (p.ref = r.sreffrom and r.stablefrom = 'POZZAM')
          where r.stablefrom = :stable and r.sreffrom = :sref and r.stableto = 'POZZAM' and r.act > 0
          into rref, relationdef , pref, prshmat,
            kilosc, kilkalk, shortages
      do begin
        quantityfromtmp = 0;
        select p.ilosc - sum(r.quantityto)
          from pozzam p
            left join relations r on (r.srefto = p.ref and r.stableto = 'POZZAM')
            left join pozzam pm on (pm.ref = r.sreffrom and r.stablefrom = 'POZZAM')
          where p.ref = :pref and r.relationdef = :relationdef and r.act > 0
            and ((pm.prshmat = :prshmat and r.relationdef = 2) or r.relationdef = 1)
          group by p.ref, p.ilosc
          into quantitytotmp;
        if (quantitytotmp > 0) then
        begin
          quantityfromtmp = quantitytotmp;
          if (relationdef = 2) then
          begin
            quantityfromtmp = quantityfromtmp * kilosc;
            quantityfromtmp = quantityfromtmp * shortages;
            quantityfromtmp = quantityfromtmp / kilkalk;
          end
          if (quantityfromtmp > quantity) then
          begin
            quantityfromtmp = quantity;
            quantitytotmp = quantityfromtmp;
            if (relationdef = 2) then
            begin
              quantitytotmp = quantitytotmp * kilkalk;
              quantitytotmp = quantitytotmp / shortages;
              quantitytotmp = quantitytotmp / kilosc;
            end
          end
          update relations set quantityfrom = quantityfrom + :quantityfromtmp,
              quantityto = quantityto + :quantitytotmp
            where ref = :rref;
          quantity = quantity - quantityfromtmp;
          if (quantity <= 0) then
            break;
        end
      end
    end
  end else if (stable = 'POZZAM' and quantity <> 0 and mode = 1) then
  begin
    if (relation is not null) then
    begin
      update relations set quantitytoreal = quantitytoreal + :quantity where ref = :relation;
    end else if (relation is null and quantity < 0) then
    begin
      -- odejmuje z pierwszego pozzama z najnizszym priorytetem
      for
        select r.ref, r.relationdef, r.quantitytoreal
          from relations r
          where r.stablefrom = :stable and r.sreffrom = :sref and r.act > 0
            and r.quantitytoreal > 0 and r.relationdef = 1
          order by r.realpriority desc
          into rref, relationdef, quantitytoreal
      do begin
        if (abs(quantity) > quantitytoreal) then
          quantitytotmp = -quantitytoreal;
        else
          quantitytotmp = quantity;
        update relations set quantitytoreal = quantitytoreal + :quantitytotmp
          where ref = :rref;
        quantity = quantity - quantitytotmp;
        if (quantity = 0) then
          break;
      end
    end else if (relation is null and quantity > 0) then
    begin
      for
        select r.ref, r.relationdef, r.quantityto - r.quantitytoreal
          from relations r
          where r.stablefrom = :stable and r.sreffrom = :sref and r.act > 0
            and r.quantityto > 0 and r.relationdef = 1 and r.quantityto - r.quantitytoreal > 0
          order by r.realpriority
          into rref, relationdef, quantitytoreal
      do begin
        if (quantity > quantitytoreal) then
          quantitytotmp = quantitytoreal;
        else
          quantitytotmp = quantity;
        update relations set quantitytoreal = quantitytoreal + :quantitytotmp
          where ref = :rref;
        quantity = quantity - quantitytotmp;
        if (quantity = 0) then
          break;
      end
    end
  end
end^
SET TERM ; ^
