--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EQUICKMASSEDITEPRPOS(
      PVALUE numeric(14,2),
      EMPLOYEE integer,
      ECOLUMN integer,
      PAYROLL integer)
  returns (
      LOGMSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable STATUS integer;
declare variable PVALUE_OLD numeric(14,2);
begin

  select ' ' || personnames || ' - ' from employees
    where ref = :employee
    into :logmsg;

  select status, pvalue
    from quickediteprpos
    where employee = :employee
      and ecolumn = :ecolumn
      and epayroll = :payroll
    into :status, :pvalue_old;

  if (pvalue = 0) then
    pvalue = null;

  if (status > 0) then
    logmsg = ascii_char(164) || logmsg || 'redakcja anulowana (rozliczenie zaakceptowane)';
  else if (pvalue_old is not distinct from pvalue) then
    logmsg = ascii_char(164) || logmsg || 'brak zmian w ustawieniu wartości składnika';
  else begin

    update quickediteprpos
      set pvalue = coalesce(:pvalue,0)
      where employee = :employee
        and ecolumn = :ecolumn
        and epayroll = :payroll;

    logmsg = ascii_char(149) || logmsg;
    if (pvalue_old is null and pvalue is not null) then
      logmsg = logmsg || 'dodano nowy składnik';
    else if (pvalue is null and pvalue_old is not null) then
      logmsg = logmsg || 'usunięto składnik płacowy';
    else
      logmsg = logmsg || 'zaktualizowano wartość składnika';
  end

  suspend;

  when any do
  begin
    logmsg = ascii_char(215) || logmsg || 'nie udało się ustawić składnika';
    suspend;
  end
end^
SET TERM ; ^
