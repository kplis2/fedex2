--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_MINMAX(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      KTMMASKA varchar(40) CHARACTER SET UTF8                           ,
      DOSTAWCA integer,
      ZAKRES smallint,
      ZAMOWIENIE integer)
  returns (
      LP integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      WERSJAREF integer,
      NAZWAT varchar(100) CHARACTER SET UTF8                           ,
      MIARA varchar(5) CHARACTER SET UTF8                           ,
      ILOSCWOPAK varchar(20) CHARACTER SET UTF8                           ,
      STAN numeric(14,4),
      CENA numeric(14,4),
      WARTOSC numeric(14,2),
      ZAREZERW numeric(14,4),
      ZABLOKOW numeric(14,4),
      ZAMOWIONO numeric(14,4),
      WOLNE numeric(14,4),
      NAGZAMREF integer,
      POZZAMREF integer,
      ILOSCNAZAM numeric(14,4),
      STANMIN numeric(14,4),
      STANMAX numeric(14,4),
      MINMAXSTAN smallint,
      KOLOR smallint)
   as
begin
  lp = 0;
  iloscnazam = NULL;
  nagzamref = :zamowienie;
  if(:dostawca is null) then dostawca = 0;
  if(:zakres is null) then zakres = 0;
  for select STANYIL.ktm, STANYIL.wersja, STANYIL.wersjaref,
   STANYIL.ilosc, STANYIL.cena, STANYIL.wartosc,
   STANYIL.nazwat, STANYIL.miara, TOWARY.iloscwopak,
   STANYIL.zarezerw, STANYIL.zablokow, STANYIL.zamowiono,
   STANYIL.ilosc-STANYIL.zarezerw-STANYIL.zablokow+STANYIL.zamowiono,
   STANYIL.stanmin, STANYIL.stanmax, STANYIL.minmaxstan, STANYIL.kolor
  from STANYIL
  left join DOSTCEN on (DOSTCEN.wersjaref = STANYIL.wersjaref)
  left join TOWARY on (TOWARY.ktm=STANYIL.ktm)
  where (STANYIL.MAGAZYN = :magazyn)
   and ((:ktmmaska is null) or (:ktmmaska = '') or (STANYIL.KTM like :ktmmaska))
   and ((:dostawca = 0 and (dostcen.gl=1 or dostcen.gl is null)) or (:dostawca = DOSTCEN.dostawca and DOSTCEN.akt=1))
   and ((:zakres=0) or (:zakres = STANYIL.minmaxstan) or(stanyil.zarezerw+stanyil.zablokow-stanyil.zamowiono>stanyil.ilosc))
  order by STANYIL.ktm,STANYIL.WERSJA
  into :ktm, :wersja, :wersjaref,
   :stan, :cena, :wartosc,
   :nazwat, :miara, :iloscwopak,
   :zarezerw, :zablokow, :zamowiono,
   :wolne,
   :stanmin, :stanmax, :minmaxstan, :kolor
  do begin
    pozzamref = NULL;
    iloscnazam = NULL;
    if(:stanmin is null) then stanmin = 0;
    if(:stanmax is null) then stanmax = 0;
    if(:zamowienie is not null and :zamowienie<>0) then begin
      select min(POZZAM.REF), sum(POZZAM.ILOSC) from POZZAM
      where (POZZAM.ZAMOWIENIE=:zamowienie) and (POZZAM.MAGAZYN=:magazyn) and
            (POZZAM.WERSJAREF=:wersjaref)
      into :pozzamref, :iloscnazam;
    end
    lp = :lp + 1;
    suspend;
  end
end^
SET TERM ; ^
