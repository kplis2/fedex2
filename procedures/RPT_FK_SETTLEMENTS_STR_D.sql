--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_SETTLEMENTS_STR_D(
      PACCOUNT varchar(256) CHARACTER SET UTF8                           ,
      P1 numeric(14,2),
      P2 numeric(14,2),
      P3 numeric(14,2),
      D1 timestamp,
      C smallint)
  returns (
      ACCOUNT varchar(3) CHARACTER SET UTF8                           ,
      VAL1 numeric(14,2),
      VAL2 numeric(14,2),
      VAL3 numeric(14,2),
      VAL4 numeric(14,2),
      VAL5 numeric(14,2),
      SUMVAL numeric(14,2),
      SUMP numeric(14,2),
      PAYDATE timestamp,
      SYMBFAK varchar(255) CHARACTER SET UTF8                           )
   as
declare variable valw numeric(14,2);
declare variable valm numeric(14,2);
declare variable val numeric(14,2);
declare variable d timestamp;
declare variable dictdef integer;
declare variable dictpos integer;
begin
    d=cast(d1 as timestamp);
  for
  select cast(substring(R.kontofk from 1 for 3) as varchar(3)), sum(P.winienzl),sum(P.mazl), R.dataplat, R.slodef, R.slopoz, R.symbfak
    from rozrachp P
    join rozrach R on (R.slodef = P.slodef and R.slopoz = P.slopoz
          and R.kontofk = P.kontofk and R.symbfak = P.symbfak
          and R.company = P.company)
           where
             ((:paccount is null) or (:paccount =';') or (position(';'||cast(substring(R.kontofk from 1 for 3) as varchar(3))||';' in :paccount)>0))
    and P.data<=:D  and R.company = :c
    group by R.kontofk , R.dataplat, R.slodef, R.slopoz, R.symbfak
    having  sum(P.winienzl)> sum(P.mazl)
    into :account, :valw,:valm ,:paydate, :dictdef, :dictpos, :symbfak
  do begin
  val1 = 0;
  val2 = 0;
  val3 = 0;
  val4 = 0;
  val5 = 0;
  sumval = 0;
  if (:D-paydate <=0) then
    val1 = valw - valm;
  else if (D-paydate>0 and (D-paydate<=p1 or p1=0) ) then
    val2 = valw - valm;
  else if (D-paydate>p1 and (D-paydate<=p2 or p2=0)) then
    val3 = valw - valm;
  else if (D-paydate>p2 and (D-paydate<=p3 or p3=0)) then
    val4 = valw - valm;
  else if (D-paydate>p3)  then
  val5 = valw - valm;
  sumval = valw - valm;
  if (p3 =0) then sump = val2+val3+val4;
  else   sump = val2+val3+val4+val5;
  suspend;
  end
end^
SET TERM ; ^
