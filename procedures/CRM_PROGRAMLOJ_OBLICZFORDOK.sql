--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CRM_PROGRAMLOJ_OBLICZFORDOK(
      STABLE varchar(31) CHARACTER SET UTF8                           ,
      SREF integer,
      AUTOOBLICZ smallint,
      SPOSOBURUCH varchar(1) CHARACTER SET UTF8                           )
   as
declare variable CPLREF integer;
declare variable CPLUCZESTREF integer;
declare variable TYPPKT integer;
declare variable REGULA integer;
declare variable PROCSQL varchar(255);
declare variable PROCED varchar(255);
declare variable SUMAPKT integer;
declare variable ZAKRESFROM varchar(255);
declare variable ZAKRESWHERE varchar(255);
declare variable CNT integer;
declare variable CPLDEFOPERREF integer;
declare variable CPLDEFOPERREFMIN integer;
declare variable CPLDEFOPERREFPLU integer;
declare variable SCPODMIOT integer;
declare variable SCPLUCZEST integer;
declare variable KLIENT integer;
declare variable DOSTAWCA integer;
declare variable ZEW integer;
declare variable WYD integer;
declare variable KOR integer;
declare variable ZAKUP integer;
declare variable OPISTRAN varchar(255);
declare variable DOKDATA timestamp;
declare variable DOKDATAACK timestamp;
declare variable DOKKONTRAH varchar(255);
declare variable DOKODDZIAL varchar(10);
declare variable DOKSYMBOL varchar(20);
declare variable NRKARTY integer;
declare variable MNOZNIK numeric(14,4);
declare variable AKTUODDZIAL varchar(10);
declare variable MSGORD integer;
declare variable KOMUNIKAT varchar(500);
declare variable SLODEFREF integer;
begin
  /*okreslenie podmiotu i/lub uczestnika na podstawie danych dokumentu*/
  if(:stable = 'DOKUMNAG') then begin
    delete from cploper where typdok = 'DOKUMNAG' and refdok = :sref;
    select defdokum.zewn, defdokum.wydania, defdokum.koryg,
        dokumnag.klient, dokumnag.dostawca,
        dokumnag.data, dokumnag.dataakc, dokumnag.symbol, dokumnag.oddzial
      from defdokum join DOKUMNAG on (DOKUMNAG.typ = DEFDOKUM.SYMBOL)
      where dokumnag.ref = :sref
      into :zew, :wyd, :kor, :klient, :dostawca,
        :dokdata, :dokdataack, :doksymbol, :dokoddzial;
    if (:zew is null) then zew = 1;
    if (:wyd is null) then wyd = 1;
    if (:kor is null) then kor = 1;
    if(:zew = 1) then begin
      if ((:wyd = 1 and :kor = 0) or (:wyd = 0  and :kor = 1)) then begin
        select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'KLIENCI' into :slodefref;
        select ref from CPODMIOTY where SLODEF= :slodefref and SLOPOZ = :klient into :scpodmiot;
      end
      if ((:wyd = 0 and kor = 0) or (:wyd = 1 and :KOR = 1)) then begin
        select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'DOSTAWCY' into :slodefref;
        select ref from CPODMIOTY where SLODEF = :slodefref and SLOPOZ = :dostawca into :scpodmiot;
      end
    end
  end else if(:stable = 'NAGFAK') then begin
    delete from cploper where typdok = 'NAGFAK' and refdok = :sref;
    select NAGFAK.zakup, NAGFAK.klient, nagfak.dostawca, cpluczestid, cpluczestkartid,
           NAGFAK.SYMBOL, NAGFAK.DATA, NAGFAK.dataakc, NAGFAK.ODDZIAL
      from NAGFAK
      where ref=:sref
      into :zakup, :klient, :dostawca, :scpluczest, :nrkarty,
            :doksymbol, :dokdata, :dokdataack, :dokoddzial;
    if(:scpluczest > 0) then
      select CPODMIOT from CPLUCZEST where ref=:scpluczest into :scpodmiot;
    else if(:zakup = 0 and :klient > 0) then begin
      select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'KLIENCI' into :slodefref;
      select REF from CPODMIOTY where SLODEF = :slodefref and SLOPOZ = :klient into :scpodmiot;
    end else if(:zakup = 1 and :dostawca > 0) then begin
      select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'DOSTAWCY' into :slodefref;
      select ref from CPODMIOTY where SLODEF = :slodefref and SLOPOZ = :dostawca into :scpodmiot;
    end
    if(:zakup = 0) then
      select NAZWA from KLIENCI where REF=:klient into :dokkontrah;
    else
      select NAZWA from DOSTAWCY where REF=:dostawca into :dokkontrah;
  end
  if(:scpodmiot is null) then exit;
  /*dla kazdego programu lojalnosciowego podmiotu, na którym dany dokument nie dokonal zapisu*/
  for select cpluczest.ref, cpluczest.cpl, cpluczest.mnoznik
  from cpluczest
  left join cploper on (cploper.cpluczest = cpluczest.ref and cploper.typdok=:stable and cploper.refdok=:sref)
  where cpluczest.cpodmiot = :scpodmiot
    and cploper.ref is null
    and ((:scpluczest is null) or (cpluczest.ref = :scpluczest))
    and (cpluczest.datadeaktyw is null or cpluczest.datadeaktyw>=:dokdata)
    and (cpluczest.dataotw is null or cpluczest.dataotw<=:dokdata)
  into :cpluczestref, :cplref, :mnoznik
  do begin
     execute procedure getconfig('AKTUODDZIAL') returning_values :aktuoddzial;
     /*dla kazdej reguly, ktora podlega pod dany program i jest dla danego typu dokumentu*/
     for select cplreguly.ref, cplreguly.procsql, cplreguly.typpunkt, ZAKRESFROM, ZAKRESWHERE, OPERACJAPLUS, OPERACJAMINUS
     from cplreguly
     where cplreguly.progloj = :cplref
       and cplreguly.typdok = :stable
       and cplreguly.autooblicz = :autooblicz
     into  :regula, :procsql, :typpkt, :zakresfrom, :zakreswhere, :cpldefoperrefplu, :cpldefoperrefmin
     do begin
       /*sprawdzenie, czy rekord pasuje do zadanej dziedziny*/
       if(:zakresfrom<>'') then begin
         proced = 'select count(*) from '||:zakresfrom||' where REF='||:sref;
         if(:zakreswhere <> '') then begin
           proced = :proced||' and ('||:zakreswhere||')';
         end
         cnt = 0;
         if(:proced is not null) then
            execute statement :proced into :cnt;
       end else begin
         cnt = 1;
       end
       if(:cnt > 0) then begin
         /*wykonanie liczenia punktów*/
         if(:procsql='') then exception CRM_PROGLOJ_EXPT 'Nie wskazano procedury SQL do naliczania punktów w regule prog. loj.';
         sumapkt = null;
         proced = 'select PKT,OPIS,KOMUNIKAT from '||:procsql||'('||:regula ||','''||:stable||''','||:sref||',0)';
         execute statement :proced into :sumapkt, :opistran,:komunikat;
         /*jesli nie ma bledów - wpisz do bazy */
         if(:sumapkt is not null) then begin
           if(:mnoznik is not null) then
             sumapkt = :sumapkt * :mnoznik;
           if(:sumapkt < 0) then begin
             cpldefoperref = :cpldefoperrefmin;
             sumapkt = - :sumapkt;
           end else cpldefoperref = :cpldefoperrefplu;
           insert into cploper(CPLUCZEST, CPL, DATA,operacja, typpkt, opis, typdok, refdok, ilpkt, oddzial,nrkarty, cplregula,
             DOKDATA, DOKDATAACK, DOKSYMBOL, DOKODDZIAL, DOKKONTRAH)
           values (:cpluczestref, :cplref, current_date, :cpldefoperref, :typpkt, :opistran, :stable, :sref, :sumapkt, :aktuoddzial,:nrkarty,:regula,
             :dokdata, :dokdataack, :doksymbol, :dokoddzial, :dokkontrah);
         end
         -- jesli sa bledy i uruchomienie automatyczne zapisz komunikat - uruchomienie reczne message na pulpit
         else if(:komunikat is not null ) then begin
           if(:sposoburuch = 'A') then begin
             execute procedure msgord_create('PROGLOJWARNING','F',:sref,:komunikat,null,'',null) returning_values :msgord;
             execute procedure msgord_close(:msgord);
           end else begin
             exception CRM_PROGLOJ_EXPT substring(:komunikat from 1 for 20);
           end
         end
       end
     end
  end
end^
SET TERM ; ^
