--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_FOREGIN_KEY_CHECK
  returns (
      DOMENA varchar(31) CHARACTER SET UTF8                           ,
      POLE varchar(31) CHARACTER SET UTF8                           ,
      GLOWNATABELA varchar(31) CHARACTER SET UTF8                           ,
      POLEZALEZNE varchar(31) CHARACTER SET UTF8                           ,
      TABELAZALEZNA varchar(31) CHARACTER SET UTF8                           )
   as
--declare variable a VARCHAR(31);
--declare variable b VARCHAR(31);
--declare variable c VARCHAR(31);
begin
  for select RDB$FIELDS.RDB$FIELD_NAME,RDB$RELATION_FIELDS.rdb$field_name,RDB$RELATION_FIELDS.rdb$relation_name
    from RDB$FIELDS
    join RDB$RELATION_FIELDS on
    (RDB$RELATION_FIELDS.rdb$field_source=RDB$FIELDS.RDB$FIELD_NAME
    and RDB$RELATION_FIELDS.RDB$FIELD_POSITION=0
    )
    left join RDB$RELATION_CONSTRAINTS on
    (RDB$RELATION_CONSTRAINTS.rdb$relation_name=RDB$RELATION_FIELDS.rdb$relation_name
--    and RDB$RELATION_CONSTRAINTS.RDB$INDEX_NAME is not null
    and RDB$RELATION_CONSTRAINTS.RDB$CONSTRAINT_TYPE not like 'PRIMARY%'
    )
    left join RDB$INDICES rdb on (RDB$RELATION_CONSTRAINTS.RDB$INDEX_NAME = RDB.RDB$INDEX_NAME and RDB.RDB$RELATION_NAME is not null)
    where RDB$FIELDS.RDB$FIELD_NAME not like 'RDB$%'
      and RDB$RELATION_CONSTRAINTS.rdb$constraint_name is not null
    group by RDB$FIELDS.RDB$FIELD_NAME,RDB$RELATION_FIELDS.rdb$field_name,RDB$RELATION_FIELDS.rdb$relation_name
    order by RDB$FIELDS.RDB$FIELD_NAME
    into :domena,:pole,:glownatabela
  do begin
    for select RDB$FIELDS.RDB$FIELD_NAME,RDB$RELATION_FIELDS.rdb$field_name,RDB$RELATION_FIELDS.rdb$relation_name
      from RDB$FIELDS
      join RDB$RELATION_FIELDS on
      (RDB$RELATION_FIELDS.rdb$field_source=RDB$FIELDS.RDB$FIELD_NAME
--      and RDB$RELATION_FIELDS.RDB$FIELD_POSITION=0
      )
      left join RDB$RELATION_CONSTRAINTS on
      (RDB$RELATION_CONSTRAINTS.rdb$relation_name=RDB$RELATION_FIELDS.rdb$relation_name
       and RDB$RELATION_CONSTRAINTS.RDB$INDEX_NAME is not null
       and RDB$RELATION_CONSTRAINTS.RDB$CONSTRAINT_TYPE not like 'PRIMARY%'
       and RDB$RELATION_CONSTRAINTS.rdb$relation_name=:glownatabela)
      where RDB$FIELDS.RDB$FIELD_NAME =:domena
      and RDB$RELATION_CONSTRAINTS.rdb$constraint_type is null
      into :domena,:polezalezne,:tabelazalezna
    do begin
/*      if (not exists(select RDB$FIELDS.RDB$FIELD_NAME,RDB$RELATION_FIELDS.rdb$field_name,RDB$RELATION_FIELDS.rdb$relation_name,C1.RDB$INDEX_NAME,C1.RDB$CONSTRAINT_TYPE,C2.RDB$INDEX_NAME,C2.RDB$CONSTRAINT_TYPE
        from RDB$FIELDS
        join RDB$RELATION_FIELDS on
        (RDB$RELATION_FIELDS.rdb$field_source=RDB$FIELDS.RDB$FIELD_NAME
--        and RDB$RELATION_FIELDS.RDB$FIELD_POSITION=0
        )
        left join RDB$RELATION_CONSTRAINTS  C1 on
        (RDB$RELATION_CONSTRAINTS.rdb$relation_name=RDB$RELATION_FIELDS.rdb$relation_name
        and RDB$RELATION_CONSTRAINTS.RDB$INDEX_NAME is not null
        and RDB$RELATION_CONSTRAINTS.RDB$CONSTRAINT_TYPE not like 'PRIMARY%'
        and RDB$RELATION_CONSTRAINTS.rdb$relation_name=:tabelazalezna)--:glownatabela)
        left join RDB$RELATION_CONSTRAINTS  C2 on
        (RDB$RELATION_CONSTRAINTS.rdb$relation_name=RDB$RELATION_FIELDS.rdb$relation_name
        and RDB$RELATION_CONSTRAINTS.RDB$CONSTRAINT_TYPE not like 'NOT%' )
        where RDB$FIELDS.RDB$FIELD_NAME =:domena
        and C1.rdb$constraint_type is null
        and C2.RDB$CONSTRAINT_TYPE = 'FOREIGN KEY'
        and RDB$RELATION_FIELDS.rdb$field_name=:polezalezne
        )) then*/
        if (tabelazalezna<>glownatabela) then
          suspend;
    end
  end
end^
SET TERM ; ^
