--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CD_DECREE(
      BKDOC integer)
   as
declare variable fromaccount varchar(20);
declare variable account varchar(20);
declare variable cvalue numeric(14,2);
declare variable decree integer;
declare variable bkdoccostdistr integer;
declare variable cdtype integer;
declare variable old_fromaccount varchar(20);
declare variable fromvalue numeric(14,2);
declare variable dist1ddef integer;
declare variable dist1symbol varchar(20);
declare variable dist2ddef integer;
declare variable dist2symbol varchar(20);
declare variable dist3ddef integer;
declare variable dist3symbol varchar(20);
declare variable dist4ddef integer;
declare variable dist4symbol varchar(20);
declare variable dist5ddef integer;
declare variable dist5symbol varchar(20);
declare variable dist6ddef integer;
declare variable dist6symbol varchar(20);
declare variable cdname varchar(80);
declare variable costsource smallint;
declare variable "REVERSE" smallint;
declare variable cd integer;
declare variable cdsourceacc integer;
declare variable pm char(1);
declare variable side smallint;
declare variable period varchar(6);
declare variable company integer;
declare variable descript varchar(255);
declare variable opis smallint;
begin

/*$$IBEC$$   select cdtype, costdistribution from bkdocs where ref = :bkdoc into :cdtype, :cd;

  select company, period from bkdocs where ref = :bkdoc into :company, :period;

 for select b.cdsourceacc, sum(b.cvalue), max(c.name), max(c.costsource), max(reverse), max(c.descripttype)
   from bkdoccostdistr b
   left join costdistribution c on b.costdistribution = c.ref
   where b.bkdoc = :bkdoc
   group by (b.cdsourceacc)
 into :cdsourceacc, :cvalue, :cdname, :costsource, :reverse, :opis
 do begin
    select c.account, b.dist1, b.dist2, b.dist3, b.dist4, b.dist5, b.dist6,
               c.todist1symbol, c.todist2symbol, c.todist3symbol, c.todist4symbol, c.todist5symbol, c.todist6symbol
           from cdsourceacc c
           join bkaccounts b on (substring(c.account from 1 for 3) = b.symbol)
           where c.ref = :cdsourceacc  and b.yearid = cast(substring(:period from  1 for 4) as integer)
           and b.company = :company
           into :account, :dist1ddef, :dist2ddef, :dist3ddef, :dist4ddef, :dist5ddef, :dist6ddef,
                :dist1symbol, :dist2symbol, :dist3symbol, :dist4symbol, :dist5symbol, :dist6symbol;

     if (coalesce(opis,0) = 0) then
      descript = :cdname;
    else if (opis = 1) then
      execute procedure FK_GET_ACCOUNT_DESCRIPT(:company, :period, :account) returning_values :descript;
    else
      select bk.descript from bkdocs bk where bk.ref = :bkdoc into :descript;

     if(:reverse = 0) then begin
     side = 1;
     end
     if(:reverse = 1) then begin
     side = 0;
     cvalue = cvalue*(-1);
     end
     if(:reverse = 2) then begin
     side = 0;
     end

     $$IBEC$$*//*execute procedure insert_decree_dists(bkdoc, account, 1-:reverse, (1-2*:reverse)*cvalue, substring(:descript from 1 for 80),
      :dist1ddef, :dist1symbol, :dist2ddef, :dist2symbol, :dist3ddef, :dist3symbol,
      :dist4ddef, :dist4symbol, :dist5ddef, :dist5symbol, 0); *//*$$IBEC$$ 
       if(:reverse <> 3) then execute procedure insert_decree_dists(bkdoc, account, :side, :cvalue, substring(:descript from 1 for 80),
      :dist1ddef, :dist1symbol, :dist2ddef, :dist2symbol, :dist3ddef, :dist3symbol,
      :dist4ddef, :dist4symbol, :dist5ddef, :dist5symbol, 1, 1, :dist6ddef, :dist6symbol);

    for select account, side, pm from CDADDONACC where cdsourceacc = :cdsourceacc
    into :account, :side, :pm
    do begin
      if (:pm = '-') then cvalue = (-1)*:cvalue;
       if (coalesce(opis,0) = 0) then
      descript = :cdname;
      else if (opis = 1) then
        execute procedure FK_GET_ACCOUNT_DESCRIPT(:company, :period, :account) returning_values :descript;
      else
        select bk.descript from bkdocs bk where bk.ref = :bkdoc into :descript;
      execute procedure insert_decree(:bkdoc, :account, :side, cvalue, substring(:descript from 1 for 80), 0);
    end
  end

  dist1ddef = 0;
  dist2ddef = 0;
  dist3ddef = 0;
  dist4ddef = 0;
  dist5ddef = 0;
  dist6ddef = 0;
  dist1symbol = '';
  dist2symbol = '';
  dist3symbol = '';
  dist4symbol = '';
  dist5symbol = '';
  dist6symbol = '';

  for
    select b.ref, b.account, b.cvalue, b.fromaccount, b.fromvalue, b.dist1ddef, b.dist1symbol, b.dist2ddef, b.dist2symbol,
    b.dist3ddef, b.dist3symbol, b.dist4ddef, b.dist4symbol, b.dist5ddef, b.dist5symbol, b.dist6ddef, b.dist6symbol, c.name, c.reverse, c.descripttype
      from bkdoccostdistr b
      left join costdistribution c on b.costdistribution = c.ref
      where b.bkdoc = :bkdoc
      into :bkdoccostdistr, :account, :cvalue, :fromaccount, :fromvalue, :dist1ddef, :dist1symbol,
      :dist2ddef, :dist2symbol, :dist3ddef, :dist3symbol, :dist4ddef, :dist4symbol, :dist5ddef, :dist5symbol, :dist6ddef, :dist6symbol, :cdname, :reverse, :opis
  do begin
    $$IBEC$$*//* if (cdtype =0) then
    begin
      if (old_fromaccount is null or (fromaccount <> old_fromaccount)) then
      begin
        if (opis = 0 or opis is null) then
          descript = :cdname;
        else
         execute procedure FK_GET_ACCOUNT_DESCRIPT(:company, :period, :account) returning_values :descript;
        execute procedure insert_decree(bkdoc, fromaccount, 1-:reverse, (1-2*:reverse)*fromvalue, substring(:descript from 1 for 80), 0);
        old_fromaccount = fromaccount;
      end
    end  *//*$$IBEC$$ 
     if (coalesce(opis,0) = 0) then
      descript = :cdname;
     else if (opis = 1) then
      execute procedure FK_GET_ACCOUNT_DESCRIPT(:company, :period, :account) returning_values :descript;
     else
      select bk.descript from bkdocs bk where bk.ref = :bkdoc into :descript;
    execute procedure insert_decree_dists(bkdoc, account, 0, cvalue, substring(:descript from 1 for 80),
      dist1ddef, dist1symbol, dist2ddef, dist2symbol, dist3ddef, dist3symbol,
      dist4ddef, dist4symbol, dist5ddef, dist5symbol, 1, 1, :dist6ddef, :dist6symbol);
    select max(ref) from decrees where bkdoc = :bkdoc and account = :account and debit = :cvalue
      into :decree;
    update bkdoccostdistr set decree = :decree where ref = :bkdoccostdistr;
  end
 $$IBEC$$*/
end^
SET TERM ; ^
