--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_REALIZACJA(
      ZAM integer,
      HISTZAM integer,
      DEFOPER varchar(3) CHARACTER SET UTF8                           ,
      REZAM integer,
      DPZAM integer,
      DBZAM integer,
      DKZAM integer,
      DDZAM integer,
      GRUPAZAM integer,
      NDATAMAG date)
  returns (
      STATUS integer,
      KODYZAM varchar(300) CHARACTER SET UTF8                           ,
      SYMBOLEZAM varchar(300) CHARACTER SET UTF8                           ,
      SYMBOLEDYSP varchar(255) CHARACTER SET UTF8                           ,
      ZAMREFFORDYSP varchar(255) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE TOREJ VARCHAR(3);
DECLARE VARIABLE TOSTAN INTEGER;
DECLARE VARIABLE KLONSYMBOL VARCHAR(30);
DECLARE VARIABLE DKNEWTYP VARCHAR(3);
DECLARE VARIABLE DKNEWREJ VARCHAR(3);
DECLARE VARIABLE DKNEWSTAN SMALLINT;
DECLARE VARIABLE OPERACJA VARCHAR(3);
DECLARE VARIABLE OPERATOR INTEGER;
DECLARE VARIABLE NEWHISTZAM INTEGER;
DECLARE VARIABLE POZZAMREF INTEGER;
DECLARE VARIABLE POCHZAM INTEGER;
DECLARE VARIABLE STAT INTEGER;
DECLARE VARIABLE DKNEWSTANC CHAR(1);
DECLARE VARIABLE ODDZIAL VARCHAR(20);
DECLARE VARIABLE KTM VARCHAR(40);
DECLARE VARIABLE WERSJAREF INTEGER;
DECLARE VARIABLE MAG VARCHAR(3);
DECLARE VARIABLE ILOSCM NUMERIC(14,4);
DECLARE VARIABLE KPLNAG INTEGER;
DECLARE VARIABLE KLIENT INTEGER;
DECLARE VARIABLE DOSTAWCA INTEGER;
DECLARE VARIABLE SPOSDOST INTEGER;
DECLARE VARIABLE MAINTOREJ VARCHAR(3);
DECLARE VARIABLE MAINTOSTAN INTEGER;
DECLARE VARIABLE ISMAINZAM INTEGER;
DECLARE VARIABLE DODKISREAL SMALLINT;
DECLARE VARIABLE KILOSC NUMERIC(14,4);
DECLARE VARIABLE KILOSCSUM NUMERIC(14,4);
DECLARE VARIABLE DKPOJEDYNCZO SMALLINT;
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE POCHZAMISREAL SMALLINT;
DECLARE VARIABLE TERMDOST TIMESTAMP;
DECLARE VARIABLE OPERATORORG INTEGER;
DECLARE VARIABLE UWAGI VARCHAR(1024);
DECLARE VARIABLE ODBIORCA VARCHAR(255);
DECLARE VARIABLE DULICA VARCHAR(255);
DECLARE VARIABLE DNRDOMU NRDOMU_ID;
DECLARE VARIABLE DNRLOKALU NRDOMU_ID;
DECLARE VARIABLE DMIASTO VARCHAR(255);
DECLARE VARIABLE DKODP VARCHAR(10);
DECLARE VARIABLE ODBIORCAID INTEGER;
DECLARE VARIABLE LSYMBOLDYSP VARCHAR(20);
DECLARE VARIABLE LDOSTZAM INTEGER;
DECLARE VARIABLE NOTBLOKKLON SMALLINT;
DECLARE VARIABLE WERSJA INTEGER;
DECLARE VARIABLE CENAMAG NUMERIC(14,4);
DECLARE VARIABLE DOSTAWAMAG INTEGER;
DECLARE VARIABLE OLDILOSC NUMERIC(14,4);
DECLARE VARIABLE NEWILOSC NUMERIC(14,4);
DECLARE VARIABLE REBLOKWIECEJ SMALLINT;
DECLARE VARIABLE MAGAZYN VARCHAR(5);
DECLARE VARIABLE GRUPOWO INTEGER;
declare variable prdepart varchar(20);
declare variable reonlynag smallint;
declare variable bn char(1);
begin
  ZAMREFFORDYSP = '';
  select KLIENT,DOSTAWCA,SPOSDOST,TERMDOST,OPERATOR,UWAGI,
    ODBIORCA,ODBIORCAID,DULICA,DNRDOMU,DNRLOKALU,DMIASTO,DKODP,BN
    from NAGZAM where REF=:zam
  into :klient,:dostawca,:sposdost,:termdost,:operatororg,:uwagi,
    :odbiorca,:odbiorcaid,:dulica,:dnrdomu,:dnrlokalu,:dmiasto,:dkodp,:bn;
  select reblokwiecej, reonlynag from DEFOPERZAM where SYMBOL = :defoper
    into :reblokwiecej, :reonlynag;
  /*dla kazdego sposobu realizacji, sprawdzenie, czy n aprawd on wystpuje, i jeli tak,
    czy wymaga podkadu. Jak wymaga, to klonowanie podkadu i przypisanie odpowiedniego ref'a podkladu*/
  if(:rezam > 0) then
    select rerej, restan, reblokwiecej, reonlynag from DEFOPERZAM where SYMBOL = :defoper
    into :maintorej, :maintostan, :reblokwiecej, :reonlynag;
  else if(:dpzam > 0) then
    select dprej, dpstan from DEFOPERZAM where SYMBOL = :defoper into :maintorej, :maintostan;
  else if(:dbzam > 0) then
    select dbrej, dbstan from DEFOPERZAM where SYMBOL = :defoper into :maintorej, :maintostan;
  else if(:dkzam > 0) then
    select dkrej, dkstan from DEFOPERZAM where SYMBOL = :defoper into :maintorej, :maintostan;
  else if(:ddzam > 0) then
    select ddrej, ddstan, grupa from DEFOPERZAM where SYMBOL = :defoper into :maintorej, :maintostan, :grupowo;
  if(:maintorej is null) then maintorej = '';
  if(:maintostan is null) then maintostan = 0;
  ismainzam  = :rezam + :dpzam + :dbzam + :dkzam + :ddzam;
  klonsymbol = '';
  symboledysp = '';
  status = 0;
  if(:reonlynag = 1) then begin
        update POZZAM set ILREAL = 0, ILREALM = 0,  ILREALO = 0  where zamowienie = :zam;
  end
  if(:rezam = 0) then begin
    select REREJ, RESTAN, REZNOTMOVEONKLON from DEFOPERZAM where SYMBOL=:defoper into :torej, tostan, :notblokklon;
    if((:torej <> '' and(:maintorej <> :torej)) or (:tostan > 0 and (:maintostan <> :tostan))) then begin
     execute procedure OPER_REALIZACJA_CREATE_COPY(:ZAM,:histzam,:torej,:tostan,0,0, :notblokklon) returning_values :rezam,:klonsymbol;
      update HISTZAM set REALOUT = 1 where REF=:histzam;
      status = 2;
    end else
      rezam = :zam;
  end
  kodyzam = cast(:rezam as varchar(30));
  symbolezam = :klonsymbol;

  klonsymbol = '';
  if(:dpzam = 0) then begin
    select dpREJ, dpSTAN, 0 from DEFOPERZAM where SYMBOL=:defoper into :torej, tostan, :notblokklon;
    if(:torej <> '' or (:tostan > 0)) then begin
      execute procedure OPER_REALIZACJA_CREATE_COPY(:ZAM,:histzam,:torej,:tostan,1,0, :notblokklon) returning_values :dpzam,:klonsymbol;
      update HISTZAM set DPOUT = 1 where REF=:histzam;
      status = 2;
    end else
      dpzam = :zam;
  end
  kodyzam = :kodyzam ||';'|| cast(:dpzam as varchar(30));
  symbolezam = :symbolezam ||';'||:klonsymbol;
  if(:dpzam > 0) then begin
     for select distinct POZZAMDYSP.MAGAZYN
     from POZZAMDYSP join pozzam on (POZZAM.ref = POZZAMDYSP.pozzam)
     where POZZAM.zamowienie = :dpzam  and pozzamdysp.TYP = 1
       and POZZAMDYSP.magazyn is not null and POZZAMDYSP.magazyn <> ''
--       and POZZAMDYSP.magazyn <> POZZAM.magazyn
     into :magazyn
     do begin
       execute procedure OPER_CREATEZAMDYSPP(:dpzam, :histzam, :defoper, :magazyn, :ndatamag )
       returning_values :ldostzam, :lsymboldysp;
       if(:lsymboldysp<>'') then begin
         symboledysp = :symboledysp||';'||:lsymboldysp;
         if(:zamreffordysp <> '') then zamreffordysp = :zamreffordysp||',';
         zamreffordysp = :zamreffordysp || cast(:dpzam as varchar(11));
       end
     end
  end

  klonsymbol = '';
  if(:dbzam = 0) then begin
    select dbREJ, dbSTAN, 0 from DEFOPERZAM where SYMBOL=:defoper into :torej, tostan, :notblokklon;
    if(:torej <> '' or (:tostan > 0)) then begin
      execute procedure OPER_REALIZACJA_CREATE_COPY(:ZAM,:histzam,:torej,:tostan,2,0, :notblokklon) returning_values :dbzam,:klonsymbol;
      update HISTZAM set DBOUT = 1 where REF=:histzam;
      status = 2;
    end else
      dbzam = :zam;
  end
  kodyzam = :kodyzam ||';'|| cast(:dbzam as varchar(30));
  symbolezam = :symbolezam ||';'||:klonsymbol;
  if(:dbzam > 0) then begin
     for select distinct POZZAMDYSP.MAGAZYN
     from POZZAMDYSP join pozzam on (POZZAM.ref = POZZAMDYSP.pozzam)
     where POZZAM.zamowienie = :dbzam  and pozzamdysp.TYP = 2
       and POZZAMDYSP.magazyn is not null and POZZAMDYSP.magazyn <> ''
       and POZZAMDYSP.magazyn <> POZZAM.magazyn
     into :magazyn
     do begin
       execute procedure OPER_CREATEZAMDYSPB(:dbzam, :histzam, :defoper, :magazyn, :ndatamag )
       returning_values :ldostzam, :lsymboldysp;
       if(:lsymboldysp<>'') then begin
         symboledysp = :symboledysp||';'||:lsymboldysp;
         if(:zamreffordysp <> '') then zamreffordysp = :zamreffordysp||',';
         zamreffordysp = :zamreffordysp || cast(:dbzam as varchar(11));
       end
     end
  end

  klonsymbol = '';
  if(:dkzam = 0) then begin
    select dkREJ, dkSTAN, DKISREAL from DEFOPERZAM where SYMBOL=:defoper into :torej, tostan, :dodkisreal;
    if((:torej <> '' and(:maintorej <> :torej)) or (:tostan > 0 and (:maintostan <> :tostan))) then begin
      execute procedure OPER_REALIZACJA_CREATE_COPY(:ZAM,:histzam,:torej,:tostan, 3,:dodkisreal, :notblokklon) returning_values :dkzam,:klonsymbol;
      update HISTZAM set DKOUT = 1 where REF=:histzam;
      status = 2;
    end else
      dkzam = :zam;
  end
  if(:dkzam > 0) then begin
    /*wykonanie operacji dla kazdego zamówienia pochodznego zgodnie z definicją*/
    select DKNEWTYP, DKNEWREJ, DKNEWSTAN, DKISREAL, REZNOTMOVEONKLON from DEFOPERZAM where SYMBOL=:defoper into :dknewtyp, :dknewrej, :dknewstan, :dodkisreal, :notblokklon;
    if(:dknewrej <>'' or (:dknewtyp <>'')or (:dknewstan > 0)) then begin
      for select REF from POZZAM where ZAMOWIENIE=:dkzam into :pozzamref
      do begin
        kiloscsum = 0;
        for select ref, KILOSC from NAGZAM where KPOPREF = :pozzamref into :pochzam, :kilosc
        do begin

          execute procedure OPER_CREATE_HIST_ZAM(:pochzam, :operacja, :operator,0,0,0,0) returning_values :stat, :newhistzam;
          update HISTZAM set MAINHISTZAM =:histzam, MAINZAM = :dkzam where REF=:newhistzam;
          if(:dknewrej <>'') then
            update NAGZAM set REJESTR = :dknewrej where REF=:pochzam and REJESTR <> :dknewrej;
          if(:dknewtyp <> '') then
            update NAGZAM set TYPZAM = :dknewtyp where REF=:pochzam and TYPZAM <> :dknewtyp;
          if(:dknewstan > 0) then begin
            if(:dknewstan = 1) then dknewstanc = 'R';
            else if(:dknewstan = 2) then dknewstanc= 'B';
            else if(:dknewstan = 3) then dknewstanc= 'D';
            else if(:dknewstan = 4) then dknewstanc= 'N';
            select ODDZIAL from NAGZAM where REF=:pochzam into :oddzial;
            update NAGZAM set STAN=:dknewstanc where REF=:pochzam and STAN <> :dknewstanc;
          end
          if(:dodkisreal = 1) then begin
            select KISREAL from NAGZAM where ref = :pochzam into :pochzamisreal;
            if((:pochzamisreal is null) or (:pochzamisreal = 0))then begin
              update NAGZAm set KISREAL = :dodkisreal where REF=:pochzam;
              update HISTZAM set KISREAL = 1 where ref=:newhistzam;
              kiloscsum = :kiloscsum + :kilosc;
            end
          end
        end
        if(:kiloscsum > 0 and :dodkisreal > 0) then begin
          update POZZAM set ILREAL = ILREAL + :kiloscsum where REF=:pozzamref;
          execute procedure POZZAM_OBL(:pozzamref);
        end
      end
    end
    select dkPojedynczo from DEFOPERZAM where SYMBOL=:defoper into :dkpojedynczo;
    /*generowanie zam. montazu zgodnie z dyspozycjami pozakadanymi*/
    for select POZZAM.REF, POZZAM.KTM, POZZAM.WERSJAREF, POZZAMDYSP.ILOSCM, POZZAM.MAGAZYN
    from POZZAM join POZZAMDYSP on (POZZAMDYSP.POZZAM = POZZAM.REF)
    where POZZAM.zamowienie = :dkzam and POZZAMDYSP.typ = 3
    into :pozzamref, :ktm, :wersjaref, :iloscm, :mag
    do begin
      cnt = 1;
      /*jesli pojedynczo*/
      if(:dkpojedynczo=1) then begin
        cnt = :iloscm;
        iloscm = 1;
      end
      while(:cnt>0) do begin
        cnt = :cnt - 1;
        /*zalozenie zamówienia*/
        execute procedure GEN_REF('NAGZAM') returning_values :pochzam;
        insert into NAGZAM(REF,TYPZAM,REJESTR,MAGAZYN, STAN,KPOPREF,KILOSC, KISREAL, ORG_OPER, TYP,KLIENT,SPOSDOST,TERMDOST,OPERATOR,UWAGI,
        ODBIORCA,ODBIORCAID,DULICA,DNRDOMU,DNRLOKALU,DMIASTO,DKODP,DATAWE,PRDEPART,BN)
        values (:pochzam, :dknewtyp, :dknewrej, :mag, 'N',:pozzamref, :iloscm, :dodkisreal, :histzam, 0,:klient,:sposdost,:termdost,:operatororg,:uwagi,
        :ODBIORCA,:ODBIORCAID,:DULICA,:DNRDOMU,:DNRLOKALU,:DMIASTO,:DKODP, current_timestamp(0),:PRDEPART,:bn);
        /*zalozenie pozycji - receptury*/
        select max(REF) from KPLNAG where WERSJAREF = :wersjaref and AKT = 1 and GLOWNA = 1 into :kplnag;
        if(:kplnag > 0) then begin
          execute procedure KOMPLET_GENZAM(:pochzam, :kplnag);
        end
        /*zmiana stanu*/
        if(:dknewstan > 0) then begin
           if(:dknewstan = 1) then dknewstanc = 'R';
           else if(:dknewstan = 2) then dknewstanc= 'B';
           else if(:dknewstan = 3) then dknewstanc= 'D';
           else if(:dknewstan = 4) then dknewstanc= 'N';
           update NAGZAM set STAN=:dknewstanc where REF=:pochzam and STAN <> :dknewstanc;
        end
        if(:dodkisreal > 0) then begin
            update POZZAM set ILREAL = ILREAL + :iloscm where REF=:pozzamref;
            execute procedure POZZAM_OBL(:pozzamref);
        end
      end
    end
  end
  kodyzam = :kodyzam ||';'|| cast(:dkzam as varchar(30));
  symbolezam = :symbolezam ||';'||:klonsymbol;

  klonsymbol = '';
  if(:ddzam = 0) then begin
    select ddREJ, ddSTAN from DEFOPERZAM where SYMBOL=:defoper into :torej, tostan;
    if(:torej <> '' or (:tostan > 0)) then begin
      execute procedure OPER_REALIZACJA_CREATE_COPY(:ZAM,:histzam,:torej,:tostan,4,0, :notblokklon) returning_values :ddzam,:klonsymbol;
      update HISTZAM set DDOUT = 1 where REF=:histzam;
      status = 2;
    end else
      ddzam = :zam;
  end
  kodyzam = :kodyzam ||';'|| cast(:ddzam as varchar(30));
  symbolezam = :symbolezam ||';'||:klonsymbol;
  if(:ddzam > 0) then begin
     if(:grupowo=0 or :grupowo is null) then grupazam=0;
     for select distinct POZZAMDYSP.dostawca
     from POZZAMDYSP join pozzam on (POZZAM.ref = POZZAMDYSP.pozzam)
     where POZZAM.zamowienie = :ddzam  and pozzamdysp.TYP = 4 and POZZAMDYSP.dostawca is not null
     into :dostawca
     do begin
       execute procedure OPER_CREATEZAMDOST(:ddzam, :histzam, :defoper, :dostawca, :grupazam) returning_values :ldostzam, :lsymboldysp;
       symboledysp = :symboledysp||';'||:lsymboldysp;
       if(:zamreffordysp <> '') then zamreffordysp = :zamreffordysp||',';
       zamreffordysp = :zamreffordysp || cast(:ddzam as varchar(11));
     end
  end

  if((:rezam > 0 and :rezam = :zam) or
     (:dkzam > 0 and :dkzam = :zam and :dodkisreal = 1)) then
  begin
    /* zapewnij blokady jesli realizujemy wiecej*/
    if(:reblokwiecej=1) then begin
      for select magazyn,ktm,wersja,cenamag,dostawamag,iloscm-ilonklonm,ilrealm
        from pozzam where zamowienie=:zam and ilrealm>iloscm-ilonklonm
        into :mag,:ktm,:wersja,:cenamag,:dostawamag,:oldilosc,:newilosc
      do begin
        execute procedure REZ_BLOK_PRZESUN(:mag,:ktm,:wersja,:cenamag,:dostawamag,:newilosc-:oldilosc);
      end
    end
    /*zamowienie oryginalne staje sie swoją realizacją */
    update NAGZAM set TYP = 3,ORG_REF = REF where REF=:zam;
  end
  if(:rezam > 0) then
    execute procedure NAGZAM_OBL(:zam);
  if(:status <> 2) then
    STATUS = 1;
end^
SET TERM ; ^
