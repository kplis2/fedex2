--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXP2PFRON_INF1(
      FIELD varchar(6) CHARACTER SET UTF8                           ,
      COMPANY integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           )
  returns (
      FIELDVALUE varchar(80) CHARACTER SET UTF8                           )
   as
begin
  if (:field = 'A1') then
    execute procedure get_config('INFOPFRON',1) returning_values  :fieldvalue;
  else if (:field = 'A2') then
  begin
    execute procedure get_config('INFONIP',1) returning_values  :fieldvalue;
    fieldvalue = replace(fieldvalue, '-', '');
    if (coalesce(char_length(fieldvalue),0) <> 10) then -- [DG] XXX ZG119346
      exception universal 'NIP musi mieć 10 cyfr';
  end
  else if (:field = 'A3') then
  begin
    execute procedure get_config('INFOREGON',1) returning_values  :fieldvalue;
    fieldvalue = replace(fieldvalue, '-', '');
    if (coalesce(char_length(fieldvalue),0) = 9) then -- [DG] XXX ZG119346
      fieldvalue = fieldvalue || '00000';
    else if (coalesce(char_length(fieldvalue),0) <> 14) then -- [DG] XXX ZG119346
      exception universal 'REGON musi mieć 9 lub 14 cyfr';
  end
  else if (:field = 'A4') then
    execute procedure get_config('INFO1',1) returning_values  :fieldvalue;
  else if (:field = 'A5') then
    execute procedure get_config('PODULICA',1) returning_values  :fieldvalue;
  else if (:field = 'A6') then
    execute procedure get_config('PODNRDOMU',1) returning_values  :fieldvalue;
  else if (:field = 'A7') then
    execute procedure get_config('PODNRLOKALU',1) returning_values  :fieldvalue;
  else if (:field = 'A8') then
    execute procedure get_config('PODMIEJ',1) returning_values  :fieldvalue;
  else if (:field = 'A9') then
    execute procedure get_config('PODKODP',1) returning_values  :fieldvalue;
  else if (:field = 'A10') then
    execute procedure get_config('PODPOCZTA',1) returning_values  :fieldvalue;
  else if (:field = 'A11') then
    execute procedure get_config('PODTEL',1) returning_values :fieldvalue;
  else if (:field = 'A12') then
    execute procedure get_config('PODFAX',1) returning_values :fieldvalue;
  else if (:field = 'A13') then
    execute procedure get_config('INFOMAIL',1) returning_values :fieldvalue;
  --pola typu B podawane sa recznie
  else if (:field = 'C17') then
  begin
    execute procedure EXP2PFRON_INF1_SUB_AVG_EMPL(:period,:company,null,0) returning_values :fieldvalue;
    if (:fieldvalue is null) then
      fieldvalue = '0';
  end
  --obliczanie pol c17 i c19 czesciowo odbywa sie w kodzie c++ (c16 i c18 calkowicie)
  else if (:field = 'C20') then
  begin
    execute procedure EXP2PFRON_INF1_SUB_AVG_EMPL(:period,:company,3,0) returning_values :fieldvalue;
    if (:fieldvalue is null) then fieldvalue = '0';
  end
  else if (:field = 'C22') then
  begin
    execute procedure EXP2PFRON_INF1_SUB_AVG_EMPL(:period,:company,2,0) returning_values :fieldvalue;
    if (:fieldvalue is null) then fieldvalue = '0';
  end
  else if (:field = 'C23') then
  begin
    execute procedure EXP2PFRON_INF1_SUB_AVG_EMPL(:period,:company,1,0) returning_values :fieldvalue;
    if (:fieldvalue is null) then fieldvalue = '0';
  end
  else if (:field = 'C31') then
  begin
    execute procedure EXP2PFRON_INF1_SUB_AVG_EMPL(:period,:company,null,1) returning_values :fieldvalue;
    if (:fieldvalue is null) then fieldvalue = '0';
  end
  else if (:field = 'C34') then
  begin
    execute procedure EXP2PFRON_INF1_SUB_AVG_EMPL(:period,:company,3,1) returning_values :fieldvalue;
    if (:fieldvalue is null) then fieldvalue = '0';
  end
  else if (:field = 'C36') then
  begin
    execute procedure EXP2PFRON_INF1_SUB_AVG_EMPL(:period,:company,2,1) returning_values :fieldvalue;
    if (:fieldvalue is null) then fieldvalue = '0';
  end
  else if (:field = 'C37') then
  begin
    execute procedure EXP2PFRON_INF1_SUB_AVG_EMPL(:period,:company,1,1) returning_values :fieldvalue;
    if (:fieldvalue is null) then fieldvalue = '0';
  end
  else fieldvalue = '';
  suspend;
end^
SET TERM ; ^
