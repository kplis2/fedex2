--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_ADD_PRORD(
      PRSCHED integer,
      PRORD integer)
   as
declare variable prdepart varchar(20);
declare variable prschedule integer;
declare variable prorddep integer;
begin
  -- okreslenie do ktorego harmonogramu ma trafic zlecenie produkcyjne
  for
    select prordout
      from PR_SEL_PRORD_DEPEND(:prord)
    group by prordout
    into prorddep
  do begin
     select n.prdepart
      from nagzam n
      where n.ref = :prorddep
      into prdepart;
    if (prdepart is null) then
      exception prschedules_error 'Nie ustalony wydział produkcyjny';
    prschedule = null;
    if (prsched is null or prsched = 0) then
    begin
      select p.ref
        from prschedules p where p.prdepart = :prdepart and p.main = 1 and p.status in (1,2)
        into prschedule;
    end
    else
      prschedule = prsched;
    if (prschedule is not null) then
    begin
      insert into prschedzam(zamowienie, prschedule) values(:prorddep, :prschedule);
    end else
      exception prschedules_error 'Brak harmonogramu głównego dla wydziału: '||prdepart;
    update nagzam set autoscheduled = 1 where ref = :prorddep;
  end
end^
SET TERM ; ^
