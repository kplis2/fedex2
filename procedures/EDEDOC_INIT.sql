--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDEDOC_INIT(
      EDERULE integer,
      OTABLE varchar(255) CHARACTER SET UTF8                           ,
      OREF integer,
      EXTFILENAME varchar(255) CHARACTER SET UTF8                           ,
      EXTSYMBOLDOK varchar(255) CHARACTER SET UTF8                           ,
      EXTPATH varchar(255) CHARACTER SET UTF8                           )
  returns (
      EDOCREF integer,
      ISNEWDOC integer)
   as
declare variable GENPROCEDURE varchar(255);
declare variable VERIFYPROCEDURE varchar(255);
declare variable sql varchar(255);
declare variable AUTOPROCESS smallint;
declare variable AUTOEXCHANGE smallint;
declare variable DIRECTION smallint;
declare variable FILEPATH varchar(255);
declare variable FILENAME varchar(255);
declare variable AKTUOPERATOR integer;
declare variable VERIFSTATE smallint;
declare variable needsign smallint;
declare variable symbol varchar(60);
declare variable status smallint;
begin
  verifstate = 1;
  isnewdoc = 0;
  filepath = '';
  filename = '';
  symbol = '';
  edocref = null;
  select pvalue from GET_GLOBAL_PARAM('AKTUOPERATOR') into :aktuoperator;

  select er.genprocedure, er.verifyprocedure, er.autoprocess, er.autoexchange, er.impexp, er.needsign
    from ederules er where ref=:ederule
  into  :genprocedure, :verifyprocedure, :autoprocess, :autoexchange, :direction, :needsign;
  ---najpierw weryfikacja, czy dokument powinien być stworzony

  if(:verifyprocedure<>'') then begin
    sql = 'select status, filepath, filename, symbol from '||:verifyprocedure||'('|| :ederule||','''||:otable||''','||:oref||')';
    execute statement :sql
      into :verifstate, :filepath, :filename, :symbol;

  end
  if(extfilename <> '') then
    filename  = extfilename;

  if(extsymboldok <> '') then
    symbol = extsymboldok;

  if(extpath <> '') then
    filepath = extpath;

  if(:verifstate =1) then begin
    --teraz weryfikacja, czy dokument już nie jest przypadkiem stworzony
    select REF from ededocsh where
      STATUS = 0
      and EDERULE = :ederule
      and DIRECTION = :direction
      and OTABLE = :otable
      and OREF = :oref
      and PATH = :filepath
      and FILENAME = :filename
      and SYMBOLDOK = :symbol
    into
      :edocref;
    if(edocref is null) then begin
      -- założenie nagłowka dokumentu (EDOCUMENTSH) -> returning_values :edocref
      execute procedure gen_ref('EDEDOCSH') returning_values :edocref;

      insert into EDEDOCSH (ref, OTABLE,OREF,STATUS,DIRECTION,PATH,FILENAME,SYMBOLDOK, EDERULE, SIGNSTATUS )
        values (:edocref, :otable, :oref, 0, :direction, :filepath, :filename, :symbol, :ederule, :needsign);
      isnewdoc = 1;
    end
    --automatyczne procedowanie przy eksporcie
    if(:direction = 1 and :autoprocess = 1) then begin
       execute procedure ededoc_process(:edocref) returning_values status;
    end
    --automatyczne zaznaczanie do importu przy imporcie
    if(:direction = 0 and :autoexchange = 1) then
      update ededocsh set STATUS = 1 where REF=:edocref and STATUS = 0;
  end
end^
SET TERM ; ^
