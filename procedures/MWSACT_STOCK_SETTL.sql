--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWSACT_STOCK_SETTL(
      DOCID integer,
      DOCGROUP integer,
      DOCPOSID integer,
      MWSORD integer,
      MWSACT integer,
      RECALC smallint,
      STOCKMWSORDTYPE integer,
      WH varchar(3) CHARACTER SET UTF8                           )
   as
declare variable procname varchar(1024);
declare variable refmwsord integer;
begin
  -- najpierw szukamy nazwy procedury na przypisaniu zleceń do magazynów
  select procgenmwsacts from mwsordtypes where ref = :stockmwsordtype
    into procname;
  if (procname is null or procname = '') then
    exit;
    --exception PROCGENMWSACTS_NOT_SET;
  else
  begin
    -- generujemy zlecenie
    procname = 'select refmwsord from '||procname||'('||:docid||','||:docgroup||','||:docposid||','''||'R'||''','||:mwsord||','||:mwsact||','||:recalc||','||:stockmwsordtype||')';
    execute statement procname into refmwsord;
  end
end^
SET TERM ; ^
