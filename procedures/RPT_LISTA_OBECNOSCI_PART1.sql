--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_LISTA_OBECNOSCI_PART1(
      DATAOD timestamp,
      DATADO timestamp)
  returns (
      OPER1 integer,
      OPER2 integer,
      OPER3 integer,
      OPER4 integer,
      OPER5 integer)
   as
declare variable operat integer;
declare variable i integer;
begin
    i = 1;
    for
      select distinct n.operator
        from epresencelistnag n
          left join epresencelists l on (n.epresencelist = l.ref)
        where l.listsdate >= :DATAOD
          and l.listsdate <= :DATADO
        into :operat
    do begin
      if (i = 1) then
      begin
        OPER1 = null;
        OPER2 = null;
        OPER3 = null;
        OPER4 = null;
        OPER5 = null;
        oper1 = operat;
      end
      else if (i = 2 ) then oper2 = operat;
      else if (i = 3 ) then oper3 = operat;
      else if (i = 4 ) then oper4 = operat;
      else if (i = 5 ) then
        begin
          oper5 = operat;
          i = 0;
          suspend;
        end
      i = i +1;
    end
  if (i > 1) then suspend;
end^
SET TERM ; ^
