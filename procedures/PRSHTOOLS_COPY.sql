--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHTOOLS_COPY(
      NEWTOOLMASTER integer,
      NEWPRSHOPER integer,
      NEWPRSHEET integer,
      OLDPRSHOPER integer,
      OLDTOOLMASTER integer)
   as
declare variable ref integer ;
declare variable vnewtoolref integer;
begin
  for
    select p.ref
      from prshtools p
      where p.opermaster = :oldprshoper
        and ((:oldtoolmaster is null and p.prshtoolmaster is null)
          or p.prshtoolmaster = :oldtoolmaster)
      into :ref
  do begin
    execute procedure GEN_REF('PRSHTOOLS') returning_values :vnewtoolref;
    INSERT INTO PRSHTOOLS (REF, SHEET, OPERMASTER, SYMBOL, TOOLTYPE, ECONDITION,
        CONDITION, QUANTITY, EHOURFACTOR, HOURFACTOR, ESHEETRATE, SHEETRATE,
        DESCRIPT, LONGDESCRIPT, PARAMS1, PARAMS2, PARAMS3, PARAMS4, PARAMS5,
        PARAMS6, PRTOOLSTYPENAME, PRSHTOOLMASTER) 
        select :vnewtoolref, :newprsheet, :newprshoper, SYMBOL, TOOLTYPE, ECONDITION,
            CONDITION, QUANTITY, EHOURFACTOR, HOURFACTOR, ESHEETRATE, SHEETRATE,
            DESCRIPT, LONGDESCRIPT, PARAMS1, PARAMS2, PARAMS3, PARAMS4, PARAMS5,
            PARAMS6, PRTOOLSTYPENAME, :newtoolmaster
          from prshtools
          where ref = :ref;
    if (exists (select first 1 1 from prshtools pp where pp.prshtoolmaster = :ref)) then
      execute procedure PRSHTOOLS_COPY(:vnewtoolref, :newprshoper, :newprsheet, :oldprshoper, :ref);
  end
end^
SET TERM ; ^
