--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PX_CREATEDOCUMENTHEADER(
      REFCUSTOMER integer,
      REFDELIVER integer,
      DOCTYPE varchar(3) CHARACTER SET UTF8                           ,
      MAG varchar(3) CHARACTER SET UTF8                           ,
      OKRES varchar(6) CHARACTER SET UTF8                           ,
      OPER varchar(60) CHARACTER SET UTF8                           )
  returns (
      REFDOC integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           )
   as
declare variable currdatestamp timestamp;
declare variable currentoper integer;
begin

   currdatestamp=CURRENT_TIMESTAMP(0);

    select o.ref from operator  o
    where o.nazwa=:oper
    into :currentoper;


   if(refdeliver is null) then
    begin

          insert into dokumnag (magazyn,typ,okres, klient, operator, data)
           values(:mag, :doctype, :okres, :refCustomer, :currentoper, :currdatestamp);


    end
   else
    begin

     insert into dokumnag (magazyn,typ,okres,  operator, data, dostawca)
        values(:mag, :doctype, :okres,  :currentoper, :currdatestamp, :refdeliver);



    end

    
      select first 1 d.ref, d.symbol from dokumnag d
       order by ref desc
       into :refdoc, :symbol;


  suspend;
end^
SET TERM ; ^
