--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_EMPLCONTRACT(
      EMPLCONTRACT integer)
  returns (
      INFO1 varchar(1024) CHARACTER SET UTF8                           ,
      INFO2 varchar(1024) CHARACTER SET UTF8                           ,
      INFO3 varchar(1024) CHARACTER SET UTF8                           ,
      INFOREGON varchar(1024) CHARACTER SET UTF8                           ,
      INFONIP varchar(1024) CHARACTER SET UTF8                           ,
      PODTEL varchar(1024) CHARACTER SET UTF8                           ,
      PODFAX varchar(1024) CHARACTER SET UTF8                           ,
      PODMIEJ varchar(1024) CHARACTER SET UTF8                           ,
      CONTRDATE date,
      TITLE varchar(40) CHARACTER SET UTF8                           ,
      REPREZ varchar(255) CHARACTER SET UTF8                           ,
      S1 varchar(15) CHARACTER SET UTF8                           ,
      S2 varchar(15) CHARACTER SET UTF8                           ,
      S3 varchar(15) CHARACTER SET UTF8                           ,
      S4 varchar(15) CHARACTER SET UTF8                           ,
      PERSONNAMES varchar(120) CHARACTER SET UTF8                           ,
      PESEL varchar(11) CHARACTER SET UTF8                           ,
      NIP varchar(15) CHARACTER SET UTF8                           ,
      STPREF varchar(30) CHARACTER SET UTF8                           ,
      STREET varchar(30) CHARACTER SET UTF8                           ,
      HOUSENO varchar(30) CHARACTER SET UTF8                           ,
      LOCALNO varchar(30) CHARACTER SET UTF8                           ,
      POSTCODE varchar(30) CHARACTER SET UTF8                           ,
      CITY varchar(30) CHARACTER SET UTF8                           ,
      TNAME varchar(30) CHARACTER SET UTF8                           ,
      FROMDATE date,
      TODATESTR varchar(30) CHARACTER SET UTF8                           ,
      WORKPOST varchar(60) CHARACTER SET UTF8                           ,
      WORKLOCAL varchar(1024) CHARACTER SET UTF8                           ,
      WORKDIMEN varchar(60) CHARACTER SET UTF8                           ,
      PAYMENTTYPESTR varchar(80) CHARACTER SET UTF8                           ,
      SALARY numeric(14,2),
      ADDCONDIT varchar(1024) CHARACTER SET UTF8                           ,
      CONTRNO varchar(20) CHARACTER SET UTF8                           )
   as
declare variable sex smallint;
declare variable todate date;
declare variable dimden smallint;
declare variable dimnum smallint;
declare variable paymenttype smallint;
declare variable econtrtype integer;
begin
  select k.wartosc from konfig k where k.akronim = 'INFO1' into info1;
  select k.wartosc from konfig k where k.akronim = 'INFO2' into info2;
  select k.wartosc from konfig k where k.akronim = 'INFO3' into info3;

  select k.wartosc from konfig k where k.akronim = 'INFOREGON' into inforegon;
  select k.wartosc from konfig k where k.akronim = 'INFONIP' into infonip;

  select case when k.wartosc is null then '' else 'tel. ' || k.wartosc end from konfig k where k.akronim = 'PODTEL' into podtel;
  select case when k.wartosc is null then '' else 'fax. ' || k.wartosc end from konfig k where k.akronim = 'PODFAX' into podfax;

  select k.wartosc from konfig k where k.akronim = 'PODMIEJ' into podmiej;

  select first 1 t.title, cast(c.contrdate as date), e.personnames, p.pesel, p.nip,
    p.sex, a.stpref, a.street, a.houseno, a.localno, a.postcode, a.city,
    t.tname, cast(c.fromdate as date), cast(c.todate as date), w.workpost,
    c.local, c.dimden, c.dimnum, c.paymenttype, c.salary, c.addcondit, c.contrno, c.econtrtype
    from EMPLCONTRACTS C
    left join EDICTWORKPOSTS W on W.REF = C.workpost
    left join EMPLOYEES E on E.REF = C.employee
    left join ECONTRTYPES T on T.CONTRTYPE = C.econtrtype
    left join EPERSADDR A on E.PERSON = A.PERSON and A.ADDRTYPE=0
    left join PERSONS P on P.ref = E.person
    where C.REF = :emplcontract
    into :title, :contrdate, :personnames, :pesel, :nip,
         :sex, :stpref, :street, :houseno, :localno, :postcode, :city,
         :tname, :fromdate, :todate, :workpost,
         :worklocal, :dimden, :dimnum, :paymenttype, :salary, :addcondit, :contrno, :econtrtype;

  pesel = coalesce(:pesel, '');
  salary = coalesce(:salary, 0);

  if(contrno is not null) then
    contrno = 'NR '||:contrno;
  else
    contrno = '';

  if(econtrtype = 5) then
  begin
    if(paymenttype = 0) then
      paymenttypestr = 'miesięcznie';
    else
      paymenttypestr = '';
  end
  else
  begin
    if(paymenttype <> 1) then
      paymenttypestr = 'stawka płacy zasadniczej - wynagrodzenie - uposażenie';
    else
      paymenttypestr = 'stawka płacy godzinowej';
   end

  if(dimden = dimnum) then
    workdimen = ' pełny wymiar czasu pracy ';
  else
    workdimen = 'w wymiarze ' || :dimnum || '/' || :dimden || ' etatu';

  if(worklocal = '' or worklocal is null) then
    worklocal = :info1 || ', ' || :info2 || ', ' || :info3;

  if(workpost is not null) then
    workpost = ' na stanowisku ' || :workpost;
  else
    workpost = '';

  if(todate is not null) then
    todatestr = ' do dnia ' || :todate;
  else
    todatestr = '';

  stpref = coalesce('',:stpref);
  street = coalesce('',:street);
  houseno = coalesce('',:houseno);
  postcode = coalesce('',:postcode);
  city = coalesce('',:city);

  if(localno is not null) then
    localno = '/' || :localno;
  else
    localno = '';

  select wartosc from konfig where akronim = 'REPREZ' into :reprez;

  if(sex = 1) then
  begin
    s1 = 'Panem';
    s2 = 'zamieszkałym';
    s3 = 'zwanym';
    s4 = 'Pana';
  end
  else
  begin
    s1 = 'Panią';
    s2 = 'zamieszkałą';
    s3 = 'zwaną';
    s4 = 'Panią';
  end
  suspend;
end^
SET TERM ; ^
