--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BANK_COPY(
      BANKISYMBOL varchar(64) CHARACTER SET UTF8                           )
   as
declare variable nazwa varchar(255);
declare variable ulica varchar(255);
declare variable miasto varchar(255);
declare variable kodp varchar(10);
declare variable konto varchar(40);
declare variable tabkurs smallint;
begin
  if (bankisymbol is not null) then begin
    select banki.nazwa, banki.ulica, banki.miasto, banki.kodp,
      banki.konto, banki.tabkurs
    from banki
    where banki.symbol = :bankisymbol
    into :nazwa, :ulica, :miasto, :kodp,
      :konto, :tabkurs;
    insert into banki
      (banki.symbol, banki.nazwa, banki.ulica, banki.miasto, banki.kodp,
      banki.konto, banki.tabkurs)
    values
      (:bankisymbol||'-K', :nazwa, :ulica, :miasto, :kodp,
      :konto, :tabkurs);
  end
end^
SET TERM ; ^
