--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_OBROTY_LISTA(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      ODDATY timestamp,
      DODATY timestamp)
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      ILPRZY numeric(14,4),
      WARTPRZY numeric(14,2),
      ILROZ numeric(14,4),
      WARTROZ numeric(14,2))
   as
declare variable datapocz timestamp;
declare variable ilkorm numeric(14,4);
declare variable wartkorm numeric(14,2);
declare variable stanpm numeric(14,4);
declare variable wartpm numeric(14,2);
begin
    for select dokumpoz.ktm, dokumpoz.wersja
         , sum(DOKUMPOZ.ilosc), sum(DOKUMPOZ.wartosc)
         , sum(DOKUMPOZ.ilosc * DEFDOKUM.WYDANIA)
         , sum(dokumpoz.wartosc * Defdokum.wydania)
         from DOKUMNAG
         join DOKUMPOZ on (DOKUMPOZ.dokument = DOKUMNAG.REF)
         left join DEFDOKUM on (DOKUMNAG.typ = DEFDOKUM.symbol)
         where position(';'||DOKUMNAG.akcept||';' in ';1;8;')>0
         and DOKUMNAG.MAGAZYN=:MAGAZYN
         AND DOKUMNAG.DATA >= :ODDATY
         AND DOKUMNAG.DATA <= :DODATY
    group by dokumpoz.ktm,dokumpoz.wersja
    into :ktm, :wersja, :ilprzy, :wartprzy
        , :ilroz, :wartroz
    do begin
      if(:ilprzy is null) then ilprzy = 0;
      if(:wartprzy is null) then wartprzy = 0;
      if(:ilroz is null) then ilroz = 0;
      if(:wartroz is null) then wartroz = 0;
      ilprzy = :ilprzy - :ilroz;
      wartprzy = :wartprzy - :wartroz;
      suspend;
    end
end^
SET TERM ; ^
