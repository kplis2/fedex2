--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_ZAMTOFAK(
      FAK integer)
  returns (
      REF integer,
      ID varchar(20) CHARACTER SET UTF8                           ,
      REJESTR varchar(3) CHARACTER SET UTF8                           )
   as
begin
  for select nz.ref, nz.id, nz.rejestr
        from pozfak pf
        join pozzam pz on (pz.ref = pf.frompozzam)
        join nagzam nz on (nz.ref = pz.zamowienie)
        where pf.dokument = :fak
  into :ref, :id, :rejestr
  do begin
    suspend;
  end

  for
    select nz.ref, nz.id, nz.rejestr
      from nagzam nz
      join nagfak nf on (nz.ref = nf.fromnagzam)
      where nf.ref = :fak
      into :ref, :id, :rejestr
  do begin
    suspend;
  end
end^
SET TERM ; ^
