--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_BACK_OPER(
      ZAM integer,
      HISTZAM integer,
      MAINOPER integer,
      BACKWITHREAL integer,
      BLOCKNUM smallint)
  returns (
      STATUS integer,
      MSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable org_ref integer;
declare variable old_rej char(3);
declare variable akt_rej char(3);
declare variable akt_blokada smallint;
declare variable old_blokada smallint;
declare variable old_datazm date;
declare variable old_org_ref integer;
declare variable d_ref integer;
declare variable d_symb varchar(30);
declare variable realout integer;
declare variable old_stan char(1);
declare variable old_kstan char(1);
declare variable refzrodl integer;
declare variable mainhist integer;
declare variable zampoch integer;
declare variable newstat integer;
declare variable newmsg varchar(400);
declare variable tmpzam integer;
declare variable tmpsymb varchar(30);
declare variable cnt integer;
declare variable dpout smallint;
declare variable dbout smallint;
declare variable dkout smallint;
declare variable ddout smallint;
declare variable oldtyp smallint;
declare variable old_wysylka integer;
declare variable old_cinout integer;
declare variable newtyp smallint;
declare variable washist smallint;
declare variable fromdokum integer;
declare variable skad integer;
declare variable dysp_org_ref integer;
declare variable mainzam integer;
declare variable kisreal smallint;
declare variable krealwasdone smallint;
declare variable oldfaktura integer;
declare variable nagfakakcept integer;
declare variable symbfak varchar(20);
declare variable refromdokref integer;
declare variable magtofakjoined integer;
declare variable old_status smallint;
declare variable noback smallint;
declare variable propersrap integer;
declare variable prshortage integer;
declare variable pozzam_ref integer;
declare variable pozzam_orgref integer;
begin
  MSG = '';
  STATUS=-1;
  washist = 0;
  noback = 0;
  /*okreslenie danych do których należy sie cofnac*/
  if(:backwithreal is null) then backwithreal = 0;
  if(:histzam > 0) then begin
    select HISTZAM.zamowienie, HISTZAM.noback, HISTZAM.propersrap, HISTZAM.prshortage
      from HISTZAM
      where ref = :histzam
      into :zam, :noback, :propersrap, :prshortage;
    if(noback = 1) then exception OPER_BACK_NO_HISTZAM 'Typ wybranego wpisu w historii operacji uniemożliwia jego usunięcie';
    washist = 1;
  end
  select org_ref,refzrodl from NAGZAM where REF=:zam into :org_ref,:refzrodl;
  if(:org_ref is null) then org_ref = 0;
  if(:histzam is null) then histzam = 0;
  if(:histzam = 0) then
    select max(ref) from HISTZAM where ZAMOWIENIE=:zam and noback = 0  into :histzam;
  else begin
    /*sprawdzenie, czy operacja wypuscila realizacje - wowczas wycofywanie realizacji*/
    select ZAMOWIENIE, REALOUT, DPOUT, DBOUT,DKOUT,DDOUT from HISTZAM where REF=:histzam  and noback = 0 into :zam, :realout, :DPOUT, :DBOUT, :DKOUT,:DDOUT;
  end
  if(:mainoper > 0) then begin
    select max(ref) from HISTZAM where ZAMOWIENIE = :zam  and noback = 0 /*and MAINHISTZAM = :mainoper*/ into :mainhist;
    if(:mainhist <> :histzam and coalesce(:propersrap,0) = 0 and coalesce(:prshortage,0) = 0) then begin
      exception OPER_BACK_SA_OPPOCH_LATER;
    end
  end
  mainzam = :zam;
  if((:histzam = 0) or (:histzam is null)) then begin
    /*brak zapisów w historii - sprawdzenie, czy to nie realizacja */
    if(:org_ref > 0 and :org_ref <> :zam and (:refzrodl is null or (:refzrodl = 0))) then begin
      select org_oper from NAGZAM where ref=:zam into :histzam;
      select ZAMOWIENIE, REALOUT, DPOUT, DBOUT,DKOUT,DDOUT from HISTZAM where REF=:histzam into :zam, :realout, :DPOUT, :DBOUT, :DKOUT,:DDOUT;
      /*sprawdzenie, czy na zamówieniu oryginalnym byly juz wykonywane jakie pozniejsze operacje - inaczej nie mozna wycofac*/
      cnt = null;
      select count(*) from HISTZAM where ZAMOWIENIE = :zam  and noback = 0 and ref > :histzam into :cnt;
      if(:cnt > 0) then
        exception OPER_ZAMORGHASOPRER;
    end else
      histzam = 0;
  end else if(:washist = 0)then begin
    select REALOUT +DKOUT +DPOUT + DBOUT + DDOUT from HISTZAM where ref=:histzam  and noback = 0 into :realout;
    if(:realout > 0 and BACKWITHREAL = 0) then
      exception OPER_BACK_REALIZACJA;
  end else if(:washist = 1) then begin
    -- sprawdzenie dla realizacji zadanej generujacej klony czy zamowienie oryginalne, posiada pozniejsze operacje
    -- nie generujace klonow BS39217
      cnt = null;
      select count(*) from HISTZAM where ZAMOWIENIE = :zam  and noback = 0
          and REALOUT +DKOUT +DPOUT + DBOUT + DDOUT  = 0 -- brak klonowania w danej operacji
          and ref > :histzam
        into :cnt;
      if(:cnt > 0) then
        exception OPER_ZAMORGHASNOCLONEOPER;
  end
  if(:histzam = 0 or (:histzam is null)) then  begin
     MSG = 'Brak rekordu historii do wycofania';
     exception OPER_BACK_NO_HISTZAM;
  end
  /* okreslenie wartoci do replace*/
  if(:histzam > 0) then
    select org_rej, datazm, org_blokada, org_id, org_stan, org_kstan, wysylka, cinout,
      DOKUMENTMAIN, SKAD, KISREAL, OLDFAKTURA, refromdokref, magtofakjoined, status
    from HISTZAM where REF=:histzam
      into :old_rej, :old_datazm, :old_blokada, :old_org_ref, :old_stan, :old_kstan, :old_wysylka, :old_cinout,
      :fromdokum, :skad, :krealwasdone, :oldfaktura, :refromdokref, :magtofakjoined, old_status;
  if(:skad > 0 and :washist = 0)then
    exception OPER_BACK_FROMDOKUM;
  /*przywrocenie blokad na zamowieniu i zamowieniach pochodnych, jakie byly przed operacja*/
  for select REF from NAGZAM where org_oper=:histzam and REF<>:zam into :tmpzam
  do begin
    update NAGZAM set STAN = :old_stan where REF=:tmpzam and STAN <> :old_stan;
    update NAGZAM set KSTAN = :old_kstan where REF=:tmpzam and KSTAN <> :old_kstan;
  end

  /* wycofanie faktur */
  for select ref, SYMBOL, akceptacja from NAGFAK where HISTORIA = :histzam into :d_ref, :d_symb, :nagfakakcept
  do begin
     if(:nagfakakcept = 9) then begin
       MSG = :MSG || 'Faktura do wycofania jest w trakcie poprawiania. Wycofanie niemożiwe.';
       exception OPER_FAKISPOPRAWIANA;
     end
     if(:refromdokref = :d_ref) then begin
       update NAGFAK set NAGFAK.FROMNAGZAM = null where ref=:d_ref;
       MSG = :MSG || 'Odlaczono fakture '||:d_symb||'.|';
     end else begin
       if(:blocknum > 0) then
         update NAGFAK set NUMBLOCKGET = :blocknum where ref=:d_ref;
       if(:NAGFAKAKCEPT = 1) then
         update NAGFAK set AKCEPTACJA = 0 where ref=:d_ref;
       update nagfak set skad = -1 where ref = :d_ref;
       delete from NAGFAK where REF=:d_ref;
       MSG = :MSG || 'Wycofano fakture '||:d_symb||'.|';
     end
  end
  /* wycofanie dokumentów */
  for select ref, symbol from DOKUMNAG where HISTORIA=:histzam and ZAMOWIENIE is not null and ZAMOWIENIE>0 into :d_ref, :d_symb do begin
    if(:refromdokref = :d_ref) then begin
      update DOKUMPOZ set FROMZAM = null where dokument=:d_ref;
      MSG = :MSG || 'Odlaczono dokument magazynowy '||:d_symb||'.|';
    end else begin
     update DOKUMNAG set BLOKADA = 0 where BLOKADA > 0 and ref = :d_ref;
     if(:blocknum > 0) then
       update DOKUMNAG set NUMBLOCKGET = :blocknum where ref=:d_ref;
     update DOKUMNAG set AKCEPT = 0 where REF = :d_ref;
     delete from DOKUMNAG where REF=:d_ref;
     MSG = :MSG || 'Wycofano dokument magazynowy '||:d_symb||'.|';
    end
  end
  /*wycofanie operacji zamówień zależnych*/
  for select ZAMOWIENIE from HISTZAM where MAINHISTZAM = :histzam and MAINZAM = :mainzam  and noback = 0 into :zampoch
  do begin
    execute procedure OPER_BACK_OPER(:zampoch, 0,:histzam, 0, :blocknum) returning_values :newstat, :newmsg;
    MSG=:MSG||:newmsg;

  end
  /*sprawdzenie, czy mozna usunac klony*/
  for select REF, ID from NAGZAM where org_oper=:histzam and REF<>:zam into :tmpzam,:tmpsymb
  do begin
   cnt = null;
   select count(*) from HISTZAm where ZAMOWIENIE=:tmpzam  and noback = 0 into :cnt;

   if(:cnt >0 )then begin
     MSG=:MSG||'Dyspozycja powiazana '||:tmpsymb||' ma pozniejsze operacje. Wycofaj te operacje'||'.|';
     exception OPER_BACK_DYSP_OPER;
   end
  end
  cnt = 0;
  for select REF, ID, ORG_REF from NAGZAM where org_oper=:histzam and REF<>:zam into :tmpzam,:tmpsymb, :dysp_org_ref
  do begin
     cnt = :cnt + 1;
     if(:dysp_org_ref is null or :dysp_org_ref = 0) then dysp_org_ref = :org_ref;
     MSG=MSG||'Usunieto realizcje/dyspozycje zamowienia'||:tmpsymb||'.|';
    /* przesuniecie blokad i rezerwacji zpowrotem*/
/*    execute procedure REZ_BLOK_ZAM_BACK(:org_ref, :tmpzam);*/
    /*najpierw wycofanie blokad z realizacji, z zaznaczeniem, by nie rozpisyway tego, co zwalniają, lecz pozostawily dla zam. glownego */
    update NAGZAM set NAGZAM.stan = 'W' where REF=:tmpzam and STAN <> 'N';
    /*kasowanie realizaji, a tym samym przywrócenie iloci zrealizowaneych na zamowieniu macierzystym*/
    update DOKUMPOZ set FROMZAM = null where FROMZAM=:tmpzam;
    /*Przepiecie numerow seryjnych*/
    for select ref, popref from pozzam
      where zamowienie = :tmpzam and coalesce(serialized, 0) = 1
    into :pozzam_ref, :pozzam_orgref
    do begin
      execute procedure COPY_DOKUMSER(:pozzam_ref, :pozzam_orgref, null, 'Z', 'Z', 2);
    end

    delete from NAGZAM where ref=:tmpzam;
    /*przepiecie dok. powiazanych z kasowanym zamowieniem*/
    if(:skad = 1 and :fromdokum > 0) then
      update DOKUMPOZ set FROMZAM = :DYSP_org_ref where DOKUMENT = :fromdokum and FROMZAM = :tmpzam;
    if(:skad = 2 and :fromdokum > 0) then begin
      update POZFAK set FROMZAM = :dysp_org_ref where DOKUMENT = :fromdokum and FROMZAM = :tmpzam;
      update DOKUMPOZ set FROMZAM = :dysp_org_ref where FROMZAM = :tmpzam;
    end

  end
  if(:cnt > 0) then begin
    execute procedure OPER_BACK_BLOK_RESTORE(:org_ref);
    select TYP from NAGZAM where REF=:org_ref into :oldtyp;
    execute procedure ZAM_SET_TYP(:org_ref);
    select TYP from NAGZAM where REF=:org_ref into :newtyp;
    if(:oldtyp > 1 and :newtyp < 2) then
       update POZZAM set ILREAL = 0 where ZAMOWIENIE=:org_ref;
  end
    /* update danych  */
  select REJESTR, BLOKADA from NAGZAM where ref=:zam into :akt_rej, :akt_blokada;
  if(:akt_rej <> old_rej) then begin
    if(:old_org_ref <> :org_ref) then
      MSG=:MSG||'Wycofanie realizacji i powrót do rejestru '||:old_rej||'.|';
    else
      MSG=:MSG||'Powrót do rejestru '||:old_rej||'.|';
  end if(:old_org_ref <> :org_ref) then
    MSG=:MSG||'Wycofanie realizacji |';
  if(:old_blokada <> :akt_blokada) then begin
      MSG = :MSG || 'Zmieniono blokade zamowienia na ';
      if(:akt_blokada = 'N') then MSG = :MSG||'zamowiono';
      else if(:akt_blokada = 'R')then MSG=:MSG||'zarezerwowano';
      else if(:akt_blokada = 'B')then MSG=:MSG||'zablok=owano';
      else if(:akt_blokada = 'Z')then MSG=:MSG||'zdjete';
      MSG = :MSG || ' .|';
  end
  if(:refromdokref > 0) then begin
    update DOKUMNAG set ZAMOWIENIE = null where ref=:refromdokref;
    if(:skad = 0) then
      update DOKUMPOZ set FROMPOZZAM = null where dokument = :refromdokref;
    if(:magtofakjoined > 0) then begin
      update DOKUMNAG set FAKTURA = null where ref=:refromdokref;
      update DOKUMPOZ set FROMPOZfak = null where dokument = :refromdokref;
    end
  end
  update NAGZAm
    set REJESTR=:old_rej, DATAZM = :old_datazm, BLOKADA = :old_blokada,
      ORG_REF=:old_org_ref, WYSYLKA = :old_wysylka, CINOUT = :old_cinout, STATUS = :old_status
    where REF=:zam;
  select TYP from NAGZAM where REF=:org_ref into :oldtyp;
  execute procedure ZAM_SET_TYP(:zam);
  execute procedure NAGZAM_CHECK_ZREAL(:zam);
  if(:krealwasdone = 1) then begin
      update NAGZAM set KISREAL = 0 where KISREAL > 0 and REF=:zam;
  end
  if(:oldfaktura > 0) then begin
      select SYMBOL from NAGFAK where REF=:oldfaktura into :symbfak;
      update NAGZAM set FAKTURA = :oldfaktura, SYMBFAK = :symbfak where FAKTURA is null and REF=:zam;
  end
  select TYP from NAGZAM where REF=:org_ref into :newtyp;
  if(:oldtyp > 1 and :newtyp < 2) then
     update POZZAM set ILREAL = 0 where ZAMOWIENIE=:org_ref;
  update NAGZAM set STAN = :old_stan, KSTAN = :old_kstan where REF=:zam;
  execute procedure NAGZAM_OBL(:zam);
  if(:histzam > 0) then
    delete from HISTZAM where ref=:histzam;
  status = 1;
end^
SET TERM ; ^
