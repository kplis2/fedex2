--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_PRZELICZ_JEDNOSTKI(
      REF integer)
  returns (
      ILOSC1 numeric(14,2),
      JEDNO integer,
      WAGA numeric(15,4),
      KTM varchar(80) CHARACTER SET UTF8                           ,
      SPOSPAK varchar(15) CHARACTER SET UTF8                           ,
      SZTUK numeric(14,2),
      OPAKOWAN integer,
      KARTONOW integer,
      OBJETOSC numeric(14,4),
      PRZEL numeric(14,2),
      POM varchar(7) CHARACTER SET UTF8                           ,
      TJEDEN varchar(5) CHARACTER SET UTF8                           ,
      TPRZELICZ numeric(14,4),
      NUMER integer,
      TOW_NAZWA varchar(80) CHARACTER SET UTF8                           ,
      WER_NAZWA varchar(50) CHARACTER SET UTF8                           ,
      TMIARA varchar(5) CHARACTER SET UTF8                           ,
      CENASR numeric(14,2),
      CENA numeric(14,2),
      WARTOSC numeric(14,2),
      CENANET numeric(14,2),
      CENABRU numeric(14,2),
      WARTOSNETTO numeric(14,2),
      WARTOSCBRUTTO numeric(14,2),
      CENAWAL numeric(14,4),
      WARTOSCWAL numeric(14,4),
      SWW varchar(16) CHARACTER SET UTF8                           ,
      MASTER integer,
      FLAGI varchar(20) CHARACTER SET UTF8                           ,
      ILNUM numeric(14,2),
      UWAGI varchar(255) CHARACTER SET UTF8                           ,
      KTMNADRZEDNY varchar(20) CHARACTER SET UTF8                           ,
      POZKPL smallint)
   as
DECLARE VARIABLE ILOP NUMERIC(14,2);
DECLARE VARIABLE ILKART NUMERIC(14,2);
DECLARE VARIABLE KARTONOWNUM NUMERIC(14,2);
DECLARE VARIABLE OPAKNUM NUMERIC(14,2);
DECLARE VARIABLE REFD INTEGER;
DECLARE VARIABLE ILOSC NUMERIC(14,2);
begin
  ilnum = 1;
  for select dokumpoz.ilosc, dokumpoz.jedno, dokumpoz.waga, dokumpoz.ktm, dokumpoz.ref from dokumpoz
    where dokumpoz.dokument = :ref
    into :ilosc1, :jedno, :waga, :ktm, :refd
  do begin
    ilosc=:ilosc1;
    select TOWJEDN.JEDN,DOKUMPOZ.NUMER,TOWARY.NAZWA,TOWJEDN.przelicz, 
           TOWARY.MIARA, DOKUMPOZ.CENASR,DOKUMPOZ.CENA,
           DOKUMPOZ.WARTOSC,DOKUMPOZ.CENANET, DOKUMPOZ.CENABRU, DOKUMPOZ.WARTSNETTO,
           DOKUMPOZ.WARTSBRUTTO,DOKUMPOZ.CENAWAL,DOKUMPOZ.WARTOSCWAL, towary.sww
    from   dokumpoz
    JOIN   towary on (dokumpoz.ktm = towary.ktm)
    join   towjedn  on (towjedn.ktm = towary.ktm)
    where  DOKUMPOZ.DOKUMENT=:ref and dokumpoz.ref = :refd and towjedn.glowna = 1
    order by DOKUMPOZ.NUMER
    into   :TJEDEN, :NUMER, :TOW_NAZWA,:tprzelicz,  :TMIARA, :CENASR, :CENA,
           :WARTOSC, :CENANET, :CENABRU, :WARTOSNETTO, :WARTOSCBRUTTO, :CENAWAL, :WARTOSCWAL, :SWW;

    if (:ilosc is null) then ilosc = 0;
    select towjedn.przelicz from towjedn where ktm = :ktm and towjedn.jedn = 'm3' into :objetosc;
    if (:objetosc is null) then objetosc = 0;
    objetosc = :objetosc * :ilosc;
    select towjedn.przelicz from towjedn where ktm = :ktm and towjedn.jedn = 'op.' into :ilop;
    if (:ilop is null) then ilop = 0;
    select towjedn.przelicz from towjedn where ktm = :ktm and towjedn.jedn = 'kart' into :ilkart;
    if (:ilkart is null) then ilkart = 0;
       spospak = '('||:ilop||'/'||:ilkart||')';
    if (ilkart<>0 and ilop <> 0) then begin
      if (:ilosc > :ilkart and :ILKART > 0) then begin
        kartonownum = :ilosc/:ilkart;
        kartonow = cast(:kartonownum as integer);
        if (:kartonow > :kartonownum) then kartonow = :kartonow - 1;
        ilosc = :ilosc - :kartonow * :ilkart;
      end
      if (:ilosc > :ilop and :ilop > 0) then begin
        opaknum = :ilosc/:ilop;
        opakowan = cast(:opaknum as integer);
        if (:opakowan > :opaknum) then opakowan = :opakowan - 1;
        ilosc = :ilosc - :opakowan * :ilop;
      end
    end
    select towjedn.jedn from towjedn where towjedn.ref = :jedno into :pom;
    if ('op.' = pom ) then przel = :ilop * :ilosc ;
    if ('kart' = pom ) then przel = :ilkart * :ilosc ;
    if ((pom <> 'op.') or (pom<>'kat') ) then przel = 1;
    sztuk = :ilosc;
  suspend;
  end
end^
SET TERM ; ^
