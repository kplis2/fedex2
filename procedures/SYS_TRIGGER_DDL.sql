--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_TRIGGER_DDL(
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      CREATE_OR_ALTER smallint)
  returns (
      DDL blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           )
   as
declare variable coa      varchar(80);
declare variable source   blob sub_type 1        segment size 80;
declare variable params   blob sub_type 1        segment size 80;
declare variable descript blob sub_type 1        segment size 80;
declare variable qnazwa    varchar(255);
declare variable eol      varchar(2);
begin
eol = '
';
  coa = 'SET TERM ^;'||:eol;
  qnazwa=(select outname from sys_quote_reserved(:nazwa));
  coa = :coa || 'CREATE OR ALTER TRIGGER '||:qnazwa;
  select rdb$trigger_source, rdb$description from rdb$triggers
      where rdb$trigger_name = :nazwa
  into source, descript;
  if(create_or_alter = 1) then
  begin
    source = :source || 'begin'||:eol||'  suspend;'||:eol||'end^'||:eol||'SET TERM ; ^';
    suspend;
  end
  execute procedure SYS_TRIGGER_PARAMETERS(:nazwa) returning_values :params;
  ddl = :coa||' '||:params||:eol||:source;
  ddl = trim(trailing
'
' from :ddl) || '^'||:eol||'SET TERM ; ^';
  suspend;
end^
SET TERM ; ^
