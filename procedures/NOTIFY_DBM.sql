--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NOTIFY_DBM
  returns (
      ID varchar(20) CHARACTER SET UTF8                           ,
      PARENTID varchar(20) CHARACTER SET UTF8                           ,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           ,
      NUMVAL numeric(14,2),
      WATCHBELOW smallint,
      COLORINDEX integer,
      PRIORITY integer,
      ICON varchar(255) CHARACTER SET UTF8                           ,
      MODULE varchar(255) CHARACTER SET UTF8                           ,
      ACTIONID varchar(255) CHARACTER SET UTF8                           ,
      ACTIONPARAMS varchar(255) CHARACTER SET UTF8                           ,
      ACTIONNAME varchar(255) CHARACTER SET UTF8                           ,
      RIGHTSGROUP varchar(255) CHARACTER SET UTF8                           ,
      RIGHTS varchar(255) CHARACTER SET UTF8                           ,
      COMPANY integer)
   as
declare variable magazyn varchar(3);
declare variable sumval numeric(14,2);
declare variable kolor integer;
declare variable ikona varchar(40);
begin
  -- towary do zamowienia
  watchbelow = 0;
  colorindex = 2;
  priority = 1;
  icon = 'MI_TOWARY';
  module = 'SID';
  actionid = 'Browse_STANYZAM';
  actionparams = '';
  actionname = 'Towary do zamówienia';
  rightsgroup = '';
  rights = '';
  sumval = 0;
  for select count(*),s.magazyn,max(o.company)
    from stanyzam s
    left join defmagaz d on (d.symbol=s.magazyn)
    left join oddzialy o on (o.oddzial=d.oddzial)
    where s.iloscsugerowana>0
    group by s.magazyn
    into :numval, :magazyn, :company
  do begin
    id = 'STANYZAM_'||:magazyn;
    parentid = '';
    val = cast(:numval as integer);
    descript = 'Towary do zamówienia ('||:magazyn||')';
    icon = 'MI_TOWARY';
    suspend;

    for select count(*),l.kolor
      from stanyzam s
      join stanyil l on (l.magazyn=s.magazyn and l.wersjaref=s.wersjaref)
      where l.magazyn=:magazyn
      and l.kolor is not null
      group by l.kolor
      into :numval, :kolor
    do begin
      id = 'STANYZAM_'||:magazyn||'_'||:kolor;
      parentid = 'STANYZAM_'||:magazyn;
      val = cast(:numval as integer);
      if(:kolor=0) then begin
        descript = 'w tym czarne';
        icon = 'MI_PROMOCJA';
      end else if(:kolor=1) then begin
        descript = 'w tym czerwone';
        icon = 'MI_PRIORITY';
      end else if(:kolor=2) then begin
        descript = 'w tym żólte';
        icon = 'MI_MYSLNIK';
      end else if(:kolor=3) then begin
        descript = 'w tym zielone';
        icon = 'MI_MYSLNIKZIEL';
      end
      suspend;
    end
  end

  -- bufory do zmiany poziomu
  watchbelow = 0;
  colorindex = 2;
  priority = 1;
  icon = 'MI_DOSTAWA';
  module = 'SID';
  actionid = 'Browse_STANYBUF';
  actionparams = '';
  actionname = 'Zarządzanie poziomem buforów';
  rightsgroup = '';
  rights = '';
  sumval = 0;
  for select count(*),s.magazyn,max(o.company)
    from stanybuf s
    left join defmagaz d on (d.symbol=s.magazyn)
    left join oddzialy o on (o.oddzial=d.oddzial)
    where s.ikona<>''
    group by s.magazyn
    into :numval, :magazyn,:company
  do begin
    id = 'STANYBUF_'||:magazyn;
    parentid = '';
    val = cast(:numval as integer);
    descript = 'Bufory do zmiany ('||:magazyn||')';
    icon = 'MI_DOSTAWA';
    suspend;

    for select count(*),s.ikona
      from stanybuf s
      where s.magazyn=:magazyn and s.ikona<>''
      group by s.ikona
      into :numval, :ikona
    do begin
      id = 'STANYBUF_'||:magazyn||'_'||substring(:ikona from 1 for 5);
      parentid = 'STANYBUF_'||:magazyn;
      val = cast(:numval as integer);
      if(:ikona='MI_DOWN') then descript = 'w tym zmniejszenia';
      else if(:ikona='MI_UP') then descript = 'w tym zwiekszenia';
      else if(:ikona='MI_INFORMATION') then descript = 'w tym nowe';
      icon = :ikona;
      suspend;
    end
  end
end^
SET TERM ; ^
