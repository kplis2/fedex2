--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMPOZ_CHANGE_DO_CORR_ALL(
      POZDOKUM integer,
      ILOSC numeric(14,5),
      BLOKOBLNAG smallint)
  returns (
      RILOSC numeric(14,5))
   as
declare variable rozref integer;
declare variable j numeric(14,5);
declare variable cenamag numeric(14,4);
declare variable dostawa integer;
declare variable cenasnet numeric(14,4);
declare variable cenasbru numeric(14,4);
declare variable cena_koszt numeric(14,4);
declare variable grupadok integer;
declare variable numer integer;
declare variable frompozfak integer;
begin
/*sprawdza po kolei wszystkie rozpiski wydania do dokumentów od najstarszej we wszystkich dokumentach dla grupy faktur*/
  -- znajdz biezaca grupe faktur
  select nf.grupadok,dp.frompozfak from nagfak nf
  left join dokumnag dn on (dn.faktura = nf.ref)
  left join dokumpoz dp on (dp.dokument = dn.ref)
  where dp.ref = :pozdokum into :grupadok,:frompozfak;

  while(:ilosc>0 and :grupadok is not null) do begin
    -- szukamy rozpisek i korygujemy
    for select DR.ref, DR.iloscl, dr.cena, dr.dostawa, dr.cenasnetto, dr.cenasbrutto, dr.cena_koszt
    from nagfak nf
    left join dokumnag dn on (dn.faktura = nf.ref)
    left join dokumpoz dp on (dp.dokument = dn.ref)
    left join dokumroz dr on (dr.pozycja = dp.ref)
    where dr.iloscl > 0 and nf.grupadok = :grupadok and dr.wydania = 1
    order by nf.ref asc, dr.numer desc
    into :rozref, :j, :cenamag, :dostawa, :cenasnet, :cenasbru, :cena_koszt
    do begin
      if(:j > :ilosc) then j = :ilosc;
      if(:ilosc > 0) then begin
        numer = null;
        select NUMER from DOKUMROZ where KORTOROZ = :rozref and pozycja = :pozdokum into :numer;
        if(:numer is null) then begin
          select max(numer) from DOKUMROZ where pozycja = :pozdokum into :numer;
          if(:numer is null or (:numer = 0))then numer = 0;
          numer = numer +  1;
          insert into DOKUMROZ(POZYCJA, NUMER,ILOSC, CENA, DOSTAWA, SERIALNR, CENASNETTO, CENASBRUTTO, KORTOROZ, BLOKOBLNAG, CENA_KOSZT)
          values (:pozdokum, :numer, :j, :cenamag, :dostawa, '', :cenasnet, :cenasbru, :rozref, :blokoblnag, :cena_koszt);
        end
        else
          update DOKUMROZ set ILOSC = ILOSC + :j, PCENA = null where pozycja = :pozdokum and numer = :numer;
        ilosc = :ilosc - :j;
      end
    end

    -- jesli nie skorygowalismy wszystkiego to szukamy innej grupy faktur
    -- byc moze jest taka w wyniku tworzenia korekt samodzielnych
    grupadok = NULL;
    if(:ilosc>0 and :frompozfak is not null) then begin
      -- lapiemy poprzedni pozfak przez REFK
      select refk from pozfak where ref=:frompozfak into :frompozfak;
      if(:frompozfak is not null) then begin
        -- szukamy grupy
        select nagfak.grupadok
        from nagfak
        join pozfak on nagfak.ref=pozfak.dokument
        where pozfak.ref=:frompozfak
        into :grupadok;
      end
    end
  end
  rilosc = :ilosc;
end^
SET TERM ; ^
