--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_LISTA_OBECNOSCI_PART2(
      DATEFROM timestamp,
      DATETO timestamp,
      OPERATOR integer)
  returns (
      DAT timestamp,
      OP1 varchar(6) CHARACTER SET UTF8                           ,
      OPNAZ1 varchar(60) CHARACTER SET UTF8                           ,
      START1 varchar(20) CHARACTER SET UTF8                           ,
      END1 varchar(20) CHARACTER SET UTF8                           ,
      TIME1 numeric(14,2),
      OP2 integer,
      OPNAZ2 varchar(60) CHARACTER SET UTF8                           ,
      START2 varchar(20) CHARACTER SET UTF8                           ,
      END2 varchar(20) CHARACTER SET UTF8                           ,
      TIME2 numeric(14,2),
      OP3 integer,
      OPNAZ3 varchar(60) CHARACTER SET UTF8                           ,
      START3 varchar(20) CHARACTER SET UTF8                           ,
      END3 varchar(20) CHARACTER SET UTF8                           ,
      TIME3 numeric(14,2),
      OP4 integer,
      OPNAZ4 varchar(60) CHARACTER SET UTF8                           ,
      START4 varchar(20) CHARACTER SET UTF8                           ,
      END4 varchar(20) CHARACTER SET UTF8                           ,
      TIME4 numeric(14,2),
      OP5 integer,
      OPNAZ5 varchar(60) CHARACTER SET UTF8                           ,
      START5 varchar(20) CHARACTER SET UTF8                           ,
      END5 varchar(20) CHARACTER SET UTF8                           ,
      TIME5 numeric(14,2),
      WAS1 smallint,
      WAS2 smallint,
      WAS3 smallint,
      WAS4 smallint,
      WAS5 smallint)
   as
declare variable operat1 integer;
declare variable operat2 integer;
declare variable operat3 integer;
declare variable operat4 integer;
declare variable operat5 integer;
declare variable datefrompom timestamp;
begin
  datefrompom = :datefrom;
  if (:operator = 0) then
    for select OPER1,OPER2,OPER3,OPER4,OPER5
      from RPT_LISTA_OBECNOSCI_PART1(:datefrom,:dateto)
    into :op1, :op2, :op3, :op4, op5
    do begin
      OPNAZ1 = null;
      OPNAZ2 = null;
      OPNAZ3 = null;
      OPNAZ4 = null;
      OPNAZ5 = null;
      datefrom = :datefrompom;
      select operator.nazwa from operator where operator.ref = :op1
        into opnaz1;
      select operator.nazwa from operator where operator.ref = :op2
        into opnaz2;
      select operator.nazwa from operator where operator.ref = :op3
        into opnaz3;
      select operator.nazwa from operator where operator.ref = :op4
        into opnaz4;
      select operator.nazwa from operator where operator.ref = :op5
        into opnaz5;
      for select distinct epresencelists.listsdate from epresencelists
      where epresencelists.listsdate <= :dateto and epresencelists.listsdate >= :datefrom
      into :dat
      do begin
        START1 = null;
        END1 = null;
        TIME1 = null;
        was1 = null;
        START2 = null;
        END2 = null;
        TIME2 = null;
        was2 = null;
        START3 = null;
        END3 = null;
        TIME3 = null;
        was3 = null;
        START4 = null;
        END4 = null;
        TIME4 = null;
        was4 = null;
        START5 = null;
        END5 = null;
        TIME5 = null;
        was5 = null;
        if (op1 is not null) then
        begin
          --exception test_break :dat;
          select distinct epresencelistnag.timestart,epresencelistnag.timestop,
          epresencelistnag.jobtimeh,epresencelistnag.wastoday
          from epresencelistnag
            left join epresencelists on (epresencelistnag.epresencelist = epresencelists.ref)
          where epresencelists.listsdate = :dat and epresencelistnag.operator = :op1
          into :start1,:end1,:time1,:was1;
        end
          select distinct epresencelistnag.timestart,epresencelistnag.timestop,
          epresencelistnag.jobtimeh,epresencelistnag.wastoday
          from epresencelistnag
            left join epresencelists on (epresencelistnag.epresencelist = epresencelists.ref)
          where epresencelists.listsdate = :datefrom and epresencelistnag.operator = :op2
          into :start2,:end2,:time2,:was2;

        if (op3 is not null) then
        begin
          select distinct epresencelistnag.timestart,epresencelistnag.timestop,
          epresencelistnag.jobtimeh,epresencelistnag.wastoday
          from epresencelistnag
            left join epresencelists on (epresencelistnag.epresencelist = epresencelists.ref)
          where epresencelists.listsdate = :dat and epresencelistnag.operator = :op3
          into :start3,:end3,:time3,:was3;
        end

        if (op4 is not null) then
        begin
          select distinct epresencelistnag.timestart,epresencelistnag.timestop,
          epresencelistnag.jobtimeh,epresencelistnag.wastoday
          from epresencelistnag
            join epresencelists on (epresencelistnag.epresencelist = epresencelists.ref)
          where epresencelists.listsdate = :dat and epresencelistnag.operator = :op4
          into :start4,:end4,:time4,:was4;
        end

        if (op5 is not null) then
        begin
          select distinct epresencelistnag.timestart,epresencelistnag.timestop,
          epresencelistnag.jobtimeh,epresencelistnag.wastoday
          from epresencelistnag
            left join epresencelists on (epresencelistnag.epresencelist = epresencelists.ref)
          where epresencelists.listsdate = :dat and epresencelistnag.operator = :op5
          into :start5,:end5,:time5,:was5;
        end
      if ((:start1 is null and :op1 is not null) or :was1 = 0) then
        begin
         start1 = 'nieobecny';
         end1 = 'nieobecny';
         time1 = 0;
        end
      if ((:start2 is null and :op2 is not null) or :was2 = 0) then
        begin
          start2 = 'nieobecny';
          end2 = 'nieobecny';
          time2 = 0;
        end
      if ((:start3 is null and :op3 is not null) or :was3 = 0) then
        begin
          start3 = 'nieobecny';
          end3 = 'nieobecny';
          time3 = 0;
        end
      if ((:start4 is null and :op4 is not null) or :was4 = 0) then
        begin
          start4 = 'nieobecny';
          end4 = 'nieobecny';
          time4 = 0;
        end
      if ((:start5 is null and :op5 is not null) or :was5 = 0) then
        begin
          start5 = 'nieobecny';
          end5 = 'nieobecny';
          time5 = 0;
        end
      suspend;
      datefrom = :datefrom + 1;
    end
  end
  else begin
    OPNAZ1 = null;
    op1 = :operator;
    select operator.nazwa from operator where operator.ref = :operator
        into :opnaz1;
    for select distinct epresencelists.listsdate from epresencelists
      where epresencelists.listsdate <= :dateto and epresencelists.listsdate >= :datefrom
      into :dat
      do begin
        START1 = null;
        END1 = null;
        TIME1 = null;
        was1 = null ;
        select distinct epresencelistnag.timestart,epresencelistnag.timestop,
          epresencelistnag.jobtimeh,epresencelistnag.wastoday
          from epresencelistnag
            left join epresencelists on (epresencelistnag.epresencelist = epresencelists.ref)
          where epresencelists.listsdate = :dat and epresencelistnag.operator = :op1
          into :start1,:end1,:time1,:was1;
        if ((:start1 is null and :op1 is not null) or :was1 = 0) then
        begin
         start1 = 'nieobecny';
         end1 = 'nieobecny';
         time1 = 0;
        end
        suspend;
      datefrom = :datefrom + 1;
    end
  end
end^
SET TERM ; ^
