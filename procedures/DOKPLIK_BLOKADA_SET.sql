--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKPLIK_BLOKADA_SET(
      DOKPLIK integer,
      BLOKADA smallint,
      CHECKB smallint)
   as
declare variable CURRBLOKADA smallint;
declare variable CURROPERATOR integer;
declare variable CURROPERBLOK integer;
declare variable curroperblockname varchar(60);
begin
  --jesli checkb = 1 to tylko sprawdzenie czy mozna
  execute procedure get_global_param('AKTUOPERATOR') returning_values :curroperator;
  if(:curroperator is null) then exception DOKPLIK_ERROR 'Nie można zidentyfikować bieżącego operatora';

  select blokada, operblok from dokplik where ref = :dokplik
  into :currblokada, :CURROPERBLOK;

  if(currblokada = blokada) then
  begin
    if(curroperator is distinct from CURROPERBLOK) then
    begin
        select nazwa from operator where ref=:CURROPERBLOK
        into curroperblockname;
        if (currblokada = 1) then exception dokplik_error 'Dokument jest już pobrany'||coalesce(' przez '||curroperblockname,'');
        else if(currblokada = 2)then exception dokplik_error 'Dokument jest już w trakcie pobierania'||coalesce(' przez '||curroperblockname,'');
        else if(currblokada = 3)then exception dokplik_error 'Dokument jest już w trakcie zwracania'||coalesce(' przez '||curroperblockname,'');
        else exception dokplik_error 'Dokument jest już zwrócony'||coalesce(' przez '||curroperblockname,'');
    end
    exit;
  end

  if(blokada = 0 and coalesce(curroperblok,0)<>0 and coalesce(curroperblok,0) <> coalesce(curroperator,0))
    then exception dokplik_error 'Tylko operator, który pobrał dokument może go zwrócić';

  if(currblokada=2 and blokada=3)
    then exception dokplik_error 'Nie można zmienić statusu dokumentu z `w trakcie pobierania` na `w trakcie zwracania`';

  if(currblokada=3 and blokada=2)
    then exception dokplik_error 'Nie można zmienić statusu dokumentu z `w trakcie zwracania` na `w trakcie pobierania`';

  if(checkb = 1) then exit;

  if(blokada = 1) then update dokplik set blokada = 1, operblok = :curroperator where ref = :dokplik;
  else
  if(blokada = 0) then
  begin
    update doklink set localfile = null, modifieddate=null where dokplik = :dokplik;
    update dokplik set blokada = 0, operblok = null where ref = :dokplik;
  end
  else
  if(blokada = 2) then
    update dokplik set blokada = :blokada, operblok = :curroperator where ref=:dokplik;
  else
    update dokplik set blokada = :blokada where ref=:dokplik;
end^
SET TERM ; ^
