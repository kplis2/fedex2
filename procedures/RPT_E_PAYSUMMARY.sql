--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_E_PAYSUMMARY(
      FROMCPER char(6) CHARACTER SET UTF8                           ,
      TOCPER char(6) CHARACTER SET UTF8                           ,
      FROMTPER char(6) CHARACTER SET UTF8                           ,
      TOTPER char(6) CHARACTER SET UTF8                           ,
      FROMIPER char(6) CHARACTER SET UTF8                           ,
      TOIPER char(6) CHARACTER SET UTF8                           ,
      BRANCH varchar(10) CHARACTER SET UTF8                           ,
      DEPT varchar(10) CHARACTER SET UTF8                           ,
      EMPLTYPE smallint,
      CURRENTCOMPANY integer)
  returns (
      NUM1 integer,
      NAME1 varchar(22) CHARACTER SET UTF8                           ,
      VAL1 numeric(14,2),
      REPSUM1 integer,
      NUM2 integer,
      NAME2 varchar(22) CHARACTER SET UTF8                           ,
      VAL2 numeric(14,2),
      REPSUM2 integer,
      NUM3 integer,
      NAME3 varchar(22) CHARACTER SET UTF8                           ,
      VAL3 numeric(14,2),
      REPSUM3 integer,
      NUM4 integer,
      NAME4 varchar(22) CHARACTER SET UTF8                           ,
      VAL4 numeric(14,2),
      REPSUM4 integer,
      NUM5 integer,
      NAME5 varchar(22) CHARACTER SET UTF8                           ,
      VAL5 numeric(14,2),
      REPSUM5 integer)
   as
declare variable col1 smallint = 0;
declare variable col2 smallint = 0;
declare variable col3 smallint = 0;
declare variable col4 smallint = 0;
declare variable col5 smallint = 0;
declare variable maxcol smallint = 0;
declare variable num integer;
declare variable name varchar(22);
declare variable val numeric(14,2);
declare variable repsum integer;
declare variable print_no integer;
begin
  if (EmplType < 1) then EmplType = null;
  if (branch = '') then branch = null;
  if (dept = '') then dept = null;

  if (fromcper = '') then
    fromcper = null;
  if (tocper = '') then
    tocper = null;
  if (fromtper = '') then
    fromtper = null;
  if (totper = '') then
    totper = null;
  if (fromiper = '') then
    fromiper = null;
  if (toiper = '') then
    toiper = null;

  print_no = gen_id(gen_rpt_paysummary_table,1);
  for
    select C.number, C.name, C.repsum, SUM(P.pvalue)
      from epayrolls ep
        join eprpos p on ep.ref = p.payroll
        join employees e on e.ref = p.employee
        join ecolumns c on c.number = p.ecolumn
        join eprempl r on ep.ref = r.epayroll and e.ref = r.employee
      where C.repsum > 0
        and ep.empltype = coalesce(:empltype,ep.empltype)
        and (r.branch = :branch or :branch is null)
        and (r.department = :dept or :dept is null)
        and e.company = :currentcompany
        and (ep.cper >= :fromcper or :fromcper is null)
        and (ep.cper <= :tocper or :tocper is null)
        and (ep.iper >= :fromiper or :fromiper is null)
        and (ep.iper <= :toiper or :toiper is null)
        and (ep.tper >= :fromtper or :fromtper is null)
        and (ep.tper <= :totper or :totper is null)
      group by C.number, C.name, C.repsum
      into :num, :name, :repsum, :val
  do
    insert into rpt_paysummary_table values (:print_no, :num, :name, :repsum, :val);

  select count(distinct case when P.ecolumn < 1000 then p.ecolumn end),
      count(distinct case when P.ecolumn >= 1000 and p.ecolumn < 4000 then p.ecolumn end),
      count(distinct case when P.ecolumn > 4000 and p.ecolumn < 5900 then p.ecolumn end),
      count(distinct case when P.ecolumn > 5900 and p.ecolumn < 8000 then p.ecolumn end),
      count(distinct case when P.ecolumn > 8000 and p.ecolumn < 9050 then p.ecolumn end)
    from rpt_paysummary_table P
    where p.print_no = :print_no
    into :col1, :col2, :col3, :col4, :col5;

  if (col1 > maxcol) then
    maxcol = col1;
  if (col2 > maxcol) then
    maxcol = col2;
  if (col3 > maxcol) then
    maxcol = col3;
  if (col4 > maxcol) then
    maxcol = col4;
  if (col5 > maxcol) then
    maxcol = col5;

  while (maxcol > 0) do
  begin
    if (col1 >= maxcol) then
      select first 1 skip (:col1 - :maxcol) p.ecolumn, p.name, p.repsum, sum(p.pvalue)
        from  rpt_paysummary_table P
        where P.ecolumn < 1000
          and p.print_no = :print_no
        group by p.ecolumn, p.name, p.repsum
        into :num1, :name1, :repsum1, val1;

    if (col2 >= maxcol) then
      select first 1 skip (:col2 - :maxcol) p.ecolumn, p.name, p.repsum, sum(p.pvalue)
        from  rpt_paysummary_table P
        where p.ecolumn >= 1000 and p.ecolumn < 4000
          and p.print_no = :print_no
        group by p.ecolumn, p.name, p.repsum
        into :num2, :name2, :repsum2, val2;

    if (col3 >= maxcol) then
      select first 1 skip (:col3 - :maxcol) p.ecolumn, p.name, p.repsum, sum(p.pvalue)
        from  rpt_paysummary_table P
        where p.ecolumn > 4000 and p.ecolumn < 5900
          and p.print_no = :print_no
        group by p.ecolumn, p.name, p.repsum
        into :num3, :name3, :repsum3, val3;

    if (col4 >= maxcol) then
      select first 1 skip (:col4 - :maxcol) p.ecolumn, p.name, p.repsum, sum(p.pvalue)
        from  rpt_paysummary_table P
        where p.ecolumn > 5900 and p.ecolumn < 8000
          and p.print_no = :print_no
        group by p.ecolumn, p.name, p.repsum
        into :num4, :name4, :repsum4, val4;

    if (col5 >= maxcol) then
      select first 1 skip (:col5 - :maxcol) p.ecolumn, p.name, p.repsum, sum(p.pvalue)
        from  rpt_paysummary_table P
        where p.ecolumn > 8000 and p.ecolumn < 9050
          and p.print_no = :print_no
        group by p.ecolumn, p.name, p.repsum
        into :num5, :name5, :repsum5, val5;

    suspend;
    maxcol = maxcol - 1;
  end
  delete from rpt_paysummary_table where print_no = :print_no;
end^
SET TERM ; ^
