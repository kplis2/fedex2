--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAKZAL_OBLONZAM(
      DOKUMENT integer)
   as
declare variable zam integer;
declare variable zaliczkowy integer;
declare variable kwotazal numeric(14,2);
declare variable kwotazalnieroz numeric(14,2);
declare variable kwotazalgrup numeric(14,2);
declare variable kwotazalnierozgrup numeric(14,2);
declare variable grfak integer;
declare variable reffak integer;
declare variable EWARTNET NUMERIC(14,2);
declare variable EWARTBRU NUMERIC(14,2);
declare variable PEWARTNET NUMERIC(14,2);
declare variable PEWARTBRU NUMERIC(14,2);
declare variable WARTNET NUMERIC(14,2);
declare variable WARTBRU NUMERIC(14,2);
declare variable vat varchar(5);
begin
   select GRUPADOK from NAGFAK where ref=:dokument into :grfak;
   select min(REF) from NAGFAK where ZALICZKOWY > 0 and FROMNAGZAM > 0 and GRUPADOK = :grfak into :dokument;
   select FROMNAGZAM from NAGFAK where REF=:dokument and ZALICZKOWY > 0 and ANULOWANIE = 0 into :zam;
   kwotazal = 0;
   kwotazalnieroz = 0;
   if(:zam > 0) then begin
     for select grupadok
     from NAGFAK where FROMNAGZAM = :zam and ZALICZKOWY > 0 and ANULOWANIE = 0 and AKCEPTACJA in (1,8)
     into :grfak
     do begin
        for select  NAGFAK.REF, rozfak.vat, rozfak.esumwartbru , rozfak.pesumwartbru
          from ROZFAK join NAGFAK on (NAGFAK.REF=ROZFAK.dokument)
          where NAGFAK.grupadok = :grfak  and ZALICZKOWY > 0 and ANULOWANIE = 0 and AKCEPTACJA in (1,8)
          order by nagfak.ref
          into :reffak, :vat, :EWARTbru, :pEWARTBRU
        do begin
           if(:EWARTBRU is null) then ewartbru = 0;
           kwotazal = :kwotazal + :ewartbru-:pewartbru;
           select sum(nagfakzal.wartnet),sum(nagfakzal.wartbru)
             from nagfakzal
             left join nagfak on (nagfak.ref=nagfakzal.faktura)
             where nagfakzal.fakturazal=:reffak and nagfakzal.vat=:vat and nagfak.anulowanie=0 and NAGFAK.akceptacja in (1,8)
           into :pewartnet, :pewartbru;
           if(:pewartnet is null) then pewartnet = 0;
           if(:pewartbru is null) then pewartbru = 0;
           kwotazalnieroz = :kwotazalnieroz + (:ewartbru - :pewartbru);
        end
     end
     update NAGZAM set KWOTAFAKZAL = :kwotazal, KWOTAFAKZALNIEROZ = :kwotazalnieroz where ref=:zam;
   end
end^
SET TERM ; ^
