--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMPL_VAC_SENIORITY(
      EMPLREF integer,
      ONDATE date)
  returns (
      SENIORITY_DATE date)
   as
declare variable FROMD date;
declare variable TOD date;
declare variable EDUCATION_YEARS integer;
declare variable ED_FROMDATE date;
declare variable ED_TODATE date;
declare variable Y_TEMP smallint;
declare variable M_TEMP smallint;
declare variable D_TEMP smallint;
declare variable Y smallint;
declare variable M smallint;
declare variable D smallint;
declare variable WORKTIME integer;
declare variable ATODATE date;
declare variable AFROMDATE date;
declare variable NIESKL_REF integer;
declare variable CTXTODATESTR varchar(20);
declare variable CTXTODATE timestamp;
begin
  --DS: wyliczenie daty uzyskania uprawnień do 26 dni urlopu rocznie
  --staz pracy na podstawie ukonczonych szkol
  select first 1 Z.addinfo
    from eperschools E
      join edictzuscodes Z on (E.education = Z.ref)
      join employees EM on (EM.person = E.person)
    where EM.ref = :emplref and E.isvacbase = 1 and E.todate <= :ondate
    order by E.fromdate desc
    into  :education_years;

  y=coalesce(education_years,0);
  m=0;
  d=0;

  --staz pracy na podstawie swiadectw pracy
  for
    select fromdate, todate
      from eworkcertifs
      where employee = :EmplRef
        and seniorityflags like '%;STU;%'
      into :fromd, :tod
  do begin
    execute procedure get_ymd(:fromd, :tod) returning_values y_temp,m_temp,d_temp;
    y = y + y_temp;
    m = m + m_temp;
    d = d + d_temp;
    --od swiadectw odejmujemy okresy szkol pokrywajace sie
    --prawidlowo powinna byc tylko jedna szkola brana do urlopu
    for
      select s.fromdate, s.todate
        from eperschools s
          join employees e on (e.person = s.person)
         where e.ref = :emplref and s.isvacbase = 1 and s.todate <= :ondate
           and s.fromdate <= :tod and s.todate >= :fromd
        into :ed_fromdate, :ed_todate
    do begin
      if (ed_fromdate < fromd) then ed_fromdate = fromd;
      if (ed_todate > tod) then ed_todate = tod;
      execute procedure get_ymd(:ed_fromdate, :ed_todate) returning_values y_temp,m_temp,d_temp;
      y = y - y_temp;
      m = m - m_temp;
      d = d - d_temp;
    end
  end

  ctxtodatestr = rdb$get_context('USER_TRANSACTION', 'RPT_WORKREFERENCE_TODATE');  --PR54992
  if (ctxtodatestr <> '') then ctxtodate = cast(ctxtodatestr as timestamp);

  select min(fromdate), max(coalesce(cast(todate as date), :ondate))
    from employment
    where employee = :EmplRef
      and (fromdate <= :ctxtodate or :ctxtodate is null) --PR54992
    into :fromd, :tod;

  if (tod >= ondate) then
    tod = ondate - 1;

  --odejmujemy szkole
  for
    select s.fromdate, s.todate
      from eperschools s
        join employees e on (e.person = s.person)
       where e.ref = :emplref and s.isvacbase = 1 and s.todate <= :ondate
         and s.fromdate <= :tod and s.todate >= :fromd
      into :ed_fromdate, :ed_todate
  do begin
    if (ed_fromdate < fromd) then ed_fromdate = fromd;
    if (ed_todate > tod) then ed_todate = tod;
    execute procedure get_ymd(ed_fromdate, ed_todate) returning_values y_temp,m_temp,d_temp;
    y = y - y_temp;
    m = m - m_temp;
    d = d - d_temp;
  end

  --jezeli w okresie miedzy zatrudnieniem a rozpoczeciem szkoly byly okresy nieskladkowe
  --watpliwe zeby kiedykolwiek wykonalo sie wnetrze funkcji
  if (ed_fromdate > fromd) then
  begin
    atodate = fromd-1;
    while (1=1) do
    begin
      nieskl_ref = null;
      select first 1 ref
        from eabsences
        where employee = :emplref and ecolumn in (10,300,330)
          and fromdate < :ed_fromdate and fromdate > :atodate
          and todate <= :ondate
          and correction in (0,2)
        order by fromdate
        into :nieskl_ref;
      if (nieskl_ref is null) then break;
      atodate = null;
      select afromdate, atodate from e_get_whole_eabsperiod(:nieskl_ref,null,null,null,null,null, ';10;300;330;')
        into :afromdate, :atodate;
      if (atodate >= ed_fromdate) then
        atodate = ed_fromdate - 1;
      execute procedure get_ymd(afromdate, atodate)
         returning_values y_temp,m_temp,d_temp;
      y = y - y_temp;
      m = m - m_temp;
      d = d - d_temp;
    end
  end

  --odjecie pozostalych okresow nieskladkowych
  atodate = coalesce(ed_todate,fromd-1);
  while (1=1) do
  begin
    nieskl_ref = null;
    select first 1 ref
      from eabsences
      where employee = :emplref and ecolumn in (10,300,330)
        and todate > :atodate and todate <= :ondate
        and correction in (0,2)
      order by fromdate
      into :nieskl_ref;
    if (nieskl_ref is null) then break;
    atodate = null;
    select afromdate, atodate from e_get_whole_eabsperiod(:nieskl_ref,null, null,null,null,null, ';10;300;330;')
      into :afromdate, :atodate;
    --jezeli okres pokrywa sie np ze szkola brana do stazu
    if (afromdate < coalesce(ed_todate+1,fromd)) then
      afromdate = coalesce(ed_todate+1,fromd);
    execute procedure get_ymd(afromdate, atodate)
       returning_values y_temp,m_temp,d_temp;
    y = y - y_temp;
    m = m - m_temp;
    d = d - d_temp;
  end

  --sprawdzenie limitu lat
  select first 1 e.worktime
    from evaclimdefs e
    where fromdate <= :ondate
    order by e.fromdate desc, e.worktime desc
    into :worktime;

  execute procedure normalize_ymd(y, m, d) returning_values y,m,d;
  --odejmujemy jeden poniewaz przykladowo 2010-01-01 plus jeden rok na gruncie prawa pracy
  --da nam nie 2011-01-01 lecz 2010-12-31
  execute procedure date_add(fromd,worktime - y,-m, -d - 1) returning_values seniority_date;
  suspend;
end^
SET TERM ; ^
