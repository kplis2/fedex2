--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_FISKALIZUJ_REMOTE(
      HOST varchar(30) CHARACTER SET UTF8                           ,
      DOKUMENT integer)
   as
declare variable msg varchar(100);
begin
  if(:host is null or (:host = '')) then exit;
  if(:dokument is null or (:dokument <= 0))then exit;
  msg = :host||';'||cast(:dokument as varchar(50));
  POST_EVENT :msg;
end^
SET TERM ; ^
