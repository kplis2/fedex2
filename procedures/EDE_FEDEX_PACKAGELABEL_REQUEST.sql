--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_PACKAGELABEL_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer,
      REF integer = null)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable shippingdoc integer;
declare variable packageref integer;
declare variable packageno string30;
declare variable grandparent integer;
declare variable subparent integer;
declare variable deliverytype integer;
declare variable accesscode string80;
declare variable filepath string255;
declare variable format string10;
begin
  select l.ref, l.sposdost
    from listywysd l
    where l.ref = :oref
  into :shippingdoc, :deliverytype;

  --sprawdzanie czy istieje dokument spedycyjny
  if (shippingdoc is null) then
    exception ede_fedex 'Brak listy spedycyjnej.';

  val = null;
  name = 'wydrukujEtykietePaczki';
  parent = null;
  params = null;
  id = 0;
  suspend;

  grandparent = id;

  --dane autoryzacyjne
  select k.klucz, k.sciezkapliku
    from spedytwys w
      left join spedytkonta k on (w.spedytor = k.spedytor)
    where w.sposdost = :deliverytype
  into :accesscode, :filepath;

  val = :accesscode;
  name = 'kodDostepu';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = :filepath;
  name = 'sciezkaPliku';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = 'PRODUCTION'; --jesli chcesz testowac zmien na TEST
  name = 'serviceUrlType';
  id = id + 1;
  parent = GrandParent;
  suspend;

  select symboldok from ededocsh where ref = :ref
  into :format;
  if (coalesce(:format,'') = '') then format = 'EPL';
  val = :format;
  name = 'format';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  for
    select o.ref, o.nrpaczkistr
      from listywysdroz_opk o
      where o.listwysd = :shippingdoc
        and o.rodzic is null
        and o.nrpaczkistr <> ''
    into :packageref, :packageno
  do begin
    val = '';
    name = 'paczka';
    params = null;
    parent = grandparent;
    id = id + 1;
    suspend;
  
    subparent = :id;

    val = :packageref;
    name = 'refPaczki';
    params = null;
    parent = subparent;
    id = id + 1;
    suspend;

    val = :packageno;
    name = 'numerPaczki';
    params = null;
    parent = subparent;
    id = id + 1;
    suspend;
  end
end^
SET TERM ; ^
