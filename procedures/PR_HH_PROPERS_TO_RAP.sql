--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_HH_PROPERS_TO_RAP(
      PRDEPART varchar(40) CHARACTER SET UTF8                           )
  returns (
      REF integer,
      SYMBOL varchar(40) CHARACTER SET UTF8                           ,
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4),
      QUANTITYR numeric(14,4),
      QUANTITYTORAP numeric(14,4))
   as
begin
  for
    select p.ref, p.oper, g.ktm, p.amountin, p.amountresult, p.amountin - p.amountresult
      from prschedopers p
        left join prschedguides g on (g.ref = p.guide)
      where p.prdepart = :prdepart and p.status in (1,2)
      into ref, symbol, good, quantity, quantityr, quantitytorap
  do begin
    suspend;
  end
end^
SET TERM ; ^
