--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PIT8C_V5(
      PERSON_INPUT integer,
      YEARID smallint,
      CURRENTCOMPANY integer)
  returns (
      P34 varchar(255) CHARACTER SET UTF8                           ,
      P35 numeric(14,2),
      P36 varchar(255) CHARACTER SET UTF8                           ,
      P37 numeric(14,2),
      P38 varchar(255) CHARACTER SET UTF8                           ,
      P39 numeric(14,2),
      P40 numeric(14,2),
      P41 numeric(14,2),
      P42 numeric(14,2),
      P43 numeric(14,2),
      P44 numeric(14,2),
      P45 numeric(14,2),
      P46 numeric(14,2),
      P47 numeric(14,2),
      P48 numeric(14,2),
      P49 numeric(14,2),
      P50 numeric(14,2),
      P51 numeric(14,2),
      P52 numeric(14,2),
      P53 numeric(14,2),
      P54 numeric(14,2),
      P55 numeric(14,2),
      P56 numeric(14,2),
      P57 numeric(14,2),
      P58 numeric(14,2),
      P59 numeric(14,2),
      BUSINESSACTIV smallint,
      INFONIP varchar(13) CHARACTER SET UTF8                           ,
      PERSON integer,
      TAXOFFICE varchar(200) CHARACTER SET UTF8                           ,
      BTYPE varchar(50) CHARACTER SET UTF8                           ,
      BVALUE numeric(14,2))
   as
declare variable I integer;
declare variable ZAS_CHOR numeric(14,2);
declare variable ZAS_MAC numeric(14,2);
declare variable ZAS_OPK numeric(14,2);
declare variable SWI_REH numeric(14,2);
declare variable ZAS_WYP numeric(14,2);
begin
--Procedura generuje dane do PITu-8C w wersji 5-tej

  execute procedure getconfig('INFONIP') returning_values :infonip;
  execute procedure efunc_get_format_nip(:infonip) returning_values :infonip;
  person_input = coalesce(person_input,0);

/*Pola przygotowane pod przyszla obsluge, drukowane na szablonie:
  P41 = 0;   P42 = 0;   P43 = 0;   P44 = 0;   P45 = 0;   P46 = 0;   P47 = 0;
  P48 = 0;   P49 = 0;   P50 = 0;   P51 = 0;   P52 = 0;   P53 = 0;   P54 = 0;
  P55 = 0;   P56 = 0;   P57 = 0;   P58 = 0;   P59 = 0;  */

  if (currentcompany > 0) then
  begin
    for
      select e.person from epayrolls r
          join eprpos p on (r.ref = p.payroll)
          join employees e on (p.employee = e.ref)
        where e.company = :currentcompany
          and r.tper starting with :yearid
          and ((p.ecolumn in (4100, 4150, 4200, 4350, 4450, 4500)
              and :person_input = 0
              and r.empltype = 2)
            or e.person = :person_input)
        group by e.person, e.personnames
        order by e.personnames
        into :person
    do begin
  
     --pobranie informacji podatkowych (dzialanosc gospodarcza + US)
      select businessactiv, taxoffice from e_get_perstaxinfo4pit(:yearid, :person)
        into :businessactiv, :taxoffice;
  
      P34 = '';
      P35 = null;
      P36 = '';
      P37 = null;
      P38 = '';
      P39 = null;
  
      i = 0;
      for
        select btype, bvalue
          from rpt_pit8c_v5(:person, :yearid, -1)
          into :btype, :bvalue
      do begin
  
        if (i = 0) then begin
          P34 = btype;
          P35 = bvalue;
        end else
        if (i = 1) then begin
          P36 = btype;
          P37 = bvalue;
        end else
        if (i = 2) then begin
          P38 = btype;
          P39 = bvalue;
        end else
        if (i >= 3) then begin
          P38 = P38 || '; ' || btype;
          P39 = P39 + bvalue;
        end
        i = i + 1;
      end
  
      P40 = coalesce(P35,0) + coalesce(P37,0) + coalesce(P39,0);
      if (P40 = 0) then P40 = null;

/*    Pola przygotowane pod przyszla obsluge, drukowane na szablonie:

      P55 = coalesce(P45,0) + coalesce(P47,0) + coalesce(P49,0) + coalesce(P51,0) + coalesce(P53, 0);
      P56 = coalesce(P46,0) + coalesce(P48,0) + coalesce(P50,0) + coalesce(P52,0) + coalesce(P54, 0);
      P57 = coalesce(P55,0) - coalesce(P56,0);
      P58 = coalesce(P56,0) - coalesce(P55,0);
      P43 = coalesce(P41,0) - coalesce(P42,0);
      if (P41 = 0) then P41 = null;
      if (P42 = 0) then P42 = null;
      if (P43 = 0) then P43 = null;
      if (P44 = 0) then P44 = null;
      if (P45 = 0) then P45 = null;
      if (P46 = 0) then P46 = null;
      if (P47 = 0) then P47 = null;
      if (P48 = 0) then P48 = null;
      if (P49 = 0) then P49 = null;
      if (P50 = 0) then P50 = null;
      if (P51 = 0) then P51 = null;
      if (P52 = 0) then P52 = null;
      if (P53 = 0) then P53 = null;
      if (P54 = 0) then P54 = null;
      if (P55 = 0) then P55 = null;
      if (P56 = 0) then P56 = null;
      if (P57 = 0) then P57 = null;
      if (P58 = 0) then P58 = null;
      if (P59 = 0) then P59 = null; */

      suspend;
    end
  end else

--Naliczanie wartosci do wypelnienia czesc 'D' PITa ----------------------------
  if (currentcompany = -1) then
  begin
    select sum(case when p.ecolumn in (4100,4200) then p.pvalue else 0 end),
           sum(case when p.ecolumn = 4150 then p.pvalue else 0 end),
           sum(case when p.ecolumn = 4350 then p.pvalue else 0 end),
           sum(case when p.ecolumn = 4450 then p.pvalue else 0 end),
           sum(case when p.ecolumn = 4500 then p.pvalue else 0 end)
      from epayrolls r
        join eprpos p on (r.ref = p.payroll)
        join employees e on (e.ref = p.employee)
      where p.ecolumn in (4100, 4150, 4200, 4350, 4450, 4500)
        and r.tper starting with :yearid
        and e.person = :person_input
        and r.empltype = 2
      into :zas_chor, :zas_wyp, :zas_mac, :zas_opk, :swi_reh;

    if (zas_chor is not null) then
    begin
      if (zas_chor <> 0) then begin
         btype = 'Zasiłek chorobowy';
         bvalue = zas_chor;
         suspend;
      end 
      if (zas_wyp <> 0) then begin
         btype = 'Zasiłek wypadkowy';
         bvalue = zas_wyp;
         suspend;
      end 
      if (zas_mac <> 0) then begin
         btype = 'Zasiłek macierzyński';
         bvalue = zas_mac;
         suspend;
      end
      if (zas_opk <> 0) then begin
        btype = 'Zasiłek opiekuńczy';
        bvalue = zas_opk;
        suspend;
      end 
      if (swi_reh <> 0) then begin
        btype = 'Świadczenie rehabilitacyjne';
        bvalue = swi_reh;
        suspend;
      end

    end
  end
end^
SET TERM ; ^
