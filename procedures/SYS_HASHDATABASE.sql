--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_HASHDATABASE as
declare variable ktm varchar(40);
declare variable ref integer;
declare variable imie varchar(255);
declare variable nazwisko varchar(255);
declare variable nazwa varchar(255);
declare variable skrot varchar(255);
declare variable ulica varchar(255);
declare variable kodp varchar(255);
declare variable miasto varchar(255);
declare variable cpwoj16m integer;
declare variable firma integer;
declare variable done integer;
declare variable nip varchar(20);
begin
  exception universal 'Czy Ty wiesz co ty robisz???';
  -- hashowanie towarow
  for select ktm from towary
  where usluga<>1
  order by ktm
  into :ktm
  do begin
    select nazwa from sys_hashtowar into :nazwa;
    update towary set nazwa=:nazwa where ktm=:ktm;
  end

  --hashowanie klientow
  delete from kliencihist;
  for select ref,firma from klienci
  order by ref
  into :ref,:firma
  do begin
    done = 0;
    while(done=0) do begin
      select imie,nazwisko,nazwa,skrot,ulica,kodp,miasto,cpwoj16m,nip
        from sys_hashaddress(:firma)
        into :imie,:nazwisko,:nazwa,:skrot,:ulica,:kodp,:miasto,:cpwoj16m,:nip;
      skrot = substring(:skrot from 1 for 35)||cast(rand()*9 as integer)||cast(rand()*9 as integer);
      if(not exists(select first 1 1 from klienci where fskrot=:skrot)) then begin
        update klienci set fskrot=:skrot, nazwa=:nazwa, imie=:imie, nazwisko=:nazwisko,
          ulica=:ulica, kodp=:kodp, miasto=:miasto, poczta=:miasto,  cpwoj16m=:cpwoj16m, nip = :nip,
          historia=0
          where ref=:ref;
        done = 1;
      end
    end
  end

  --hashowanie dostawcow
  for select ref,firma from dostawcy
  order by ref
  into :ref,:firma
  do begin
    done = 0;
    while(done=0) do begin
      select imie,nazwisko,nazwa,skrot,ulica,kodp,miasto,cpwoj16m,nip
        from sys_hashaddress(:firma)
        into :imie,:nazwisko,:nazwa,:skrot,:ulica,:kodp,:miasto,:cpwoj16m,:nip;
      skrot = substring(:skrot from 1 for 35)||cast(rand()*9 as integer)||cast(rand()*9 as integer);
      if(not exists(select first 1 1 from dostawcy where id=:skrot)) then begin
        update dostawcy set id=:skrot, nazwa=:nazwa,
          ulica=:ulica, kodp=:kodp, miasto=:miasto, poczta=:miasto, cpwoj16m=:cpwoj16m, nip = :nip
          where ref=:ref;
        done = 1;
      end
    end
  end

  --hashowanie sprzedawcow
  for select ref from sprzedawcy
  order by ref
  into :ref
  do begin
      select imie,nazwisko,nazwa,skrot,ulica,kodp,miasto,cpwoj16m
        from sys_hashaddress(0)
        into :imie,:nazwisko,:nazwa,:skrot,:ulica,:kodp,:miasto,:cpwoj16m;
      update sprzedawcy set skrot=:skrot, nazwa=:nazwa, imie = :imie, nazwisko = :nazwisko,
          adres='', kodpoczt='', miasto='', poczta=''
          where ref=:ref;
  end

  --hashowanie operatorow
  for select ref from operator
  order by ref
  into :ref
  do begin
      select imie,nazwisko,nazwa,skrot,ulica,kodp,miasto,cpwoj16m
        from sys_hashaddress(0)
        into :imie,:nazwisko,:nazwa,:skrot,:ulica,:kodp,:miasto,:cpwoj16m;
      update operator set nazwa=:nazwa, imie = :imie, nazwisko = :nazwisko,
          email = '', telefon = ''
          where ref=:ref;
  end

  --konfigi
UPDATE KONFIG SET 
    WARTOSC = 'ABC sp z o.o.'
WHERE (AKRONIM = 'INFO1');

UPDATE KONFIG SET 
    WARTOSC = 'Ul. Informatyków 98'
WHERE (AKRONIM = 'INFO2');

UPDATE KONFIG SET 
    WARTOSC = '51-555 Wrocław'
WHERE (AKRONIM = 'INFO3');

UPDATE KONFIG SET 
    WARTOSC = 'B P H  P B K  SA   I/O Wrocław'
WHERE (AKRONIM = 'INFOBANK');

UPDATE KONFIG SET 
    WARTOSC = 'FIRMA@firma.pl'
WHERE (AKRONIM = 'INFOMAIL');

UPDATE KONFIG SET 
    WARTOSC = '4231231210'
WHERE (AKRONIM = 'INFONIP');

UPDATE KONFIG SET 
    WARTOSC = '12-1111-1111-1111-1111-1111-2222'
WHERE (AKRONIM = 'INFORACH');

UPDATE KONFIG SET 
    WARTOSC = '005245775'
WHERE (AKRONIM = 'INFOREGON');

UPDATE KONFIG SET 
    WARTOSC = 'B P H  P B K  SA   I/O Wrocław'
WHERE (AKRONIM = 'INFOBANK2');

UPDATE KONFIG SET 
    WARTOSC = '12-1111-1111-1111-1111-1111-2222'
WHERE (AKRONIM = 'INFORACH2');

UPDATE KONFIG SET 
    WARTOSC = ''
WHERE (AKRONIM = 'INFO4');

UPDATE KONFIG SET 
    WARTOSC = 'PL4092197426'
WHERE (AKRONIM = 'INFONIPUE');

UPDATE KONFIG SET 
    WARTOSC = '14X2048E9'
WHERE (AKRONIM = 'INFOPFRON');

UPDATE KONFIG SET 
    WARTOSC = 'ABC SP. Z O.O.'
WHERE (AKRONIM = 'INFOSHORTNAME');

UPDATE KONFIG SET 
    WARTOSC = 'Ewa Zielińska'
WHERE (AKRONIM = 'INFO_WINDYKACJA');

UPDATE KONFIG SET 
    WARTOSC = '1312'
WHERE (AKRONIM = 'INFONUSP');

UPDATE KONFIG SET 
    WARTOSC = 'Sąd Rejonowy dla Wrocławia - Fabrycznej, VI Wydział Gospodarczy Krajowego Rejestru Sądowego'
WHERE (AKRONIM = 'INFOREJ1');

UPDATE KONFIG SET 
    WARTOSC = '123456789'
WHERE (AKRONIM = 'INFOREJ2');

UPDATE KONFIG SET 
    WARTOSC = '1 000 000,00 opłacony w całości'
WHERE (AKRONIM = 'INFOREJ3');

UPDATE KONFIG SET 
    WARTOSC = 'Wrocław'
WHERE (AKRONIM = 'INFOZUS');

UPDATE KONFIG SET 
    WARTOSC = '1234565432'
WHERE (AKRONIM = 'INFOGLN');

UPDATE KONFIG SET 
    WARTOSC = '5811'
WHERE (AKRONIM = 'INFOPKD');



end^
SET TERM ; ^
