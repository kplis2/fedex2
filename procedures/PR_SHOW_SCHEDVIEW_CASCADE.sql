--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_SHOW_SCHEDVIEW_CASCADE(
      REFIN integer,
      PARENTIN integer,
      POZZAMREFIN integer,
      SCHEDVIEWIN integer)
  returns (
      REF integer,
      PARENT integer,
      POZZAMREF integer,
      NAGZAMREF integer,
      NAGZAMID varchar(40) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      PRSCHEDGUIDE integer,
      PRSCHEDGUIDEPOS integer,
      PRSCHEDOPER integer,
      QUANTITY numeric(14,4),
      QUANTITYR numeric(14,4),
      PRSHOPER integer,
      PROPER varchar(20) CHARACTER SET UTF8                           ,
      PRMACHINE integer,
      DESCRIPT varchar(1024) CHARACTER SET UTF8                           ,
      TERMDOST timestamp)
   as
declare variable wydania smallint;
declare variable tmp1 integer;
declare variable tmp2 integer;
declare variable tmp3 integer;
declare variable refout integer;
declare variable tmpref integer;
begin
  if (ref is null) then ref = 0;
  if (refin > 0) then ref = refin;
  if (parentin > 0) then
    parent = parentin;
  if (pozzamrefin = 0) then pozzamrefin = null;
  -- petla po pozycjach
  tmp3 = parent;
  for
    select n.id, n.ref, p.ref, p.ktm, p.wersjaref, p.ilosc, p.ilzreal, t.wydania
      from nagzam n
        left join pozzam p on (n.ref = p.zamowienie)
        left join typzam t on (t.symbol = n.typzam)
      where n.schedview = case when :parent is null then -1 else 1 end * :schedviewin and ((p.out = 0 and t.wydania = 3) or (p.out = 1 and t.wydania = 1))
        and (p.ref = :pozzamrefin or :pozzamrefin is null)
      into nagzamid, nagzamref, pozzamref, ktm, wersjaref, quantity, quantityr, wydania
  do begin
    ref = ref + 1;
    descript = ktm;
    prschedguidepos = null;
    prschedguide = null;
  --  if (wydania = 3 or (wydania = 1 and not exists (select first 1 1 from relations r where r.stableto = 'POZZAM' and r.srefto = :pozzamref))) then
      suspend;
    parent = ref;
    tmp1 = parent;
    if (wydania = 3) then
    begin
      for
        select g.ref, g.amount, g.amountzreal, o.shoper, o.oper, o.machine, o.ref, o.endtime
          from prschedguides g
            left join prschedopers o on (o.guide = g.ref)
          where g.pozzam = :pozzamref and o.activ = 1
          into prschedguide, quantity, quantityr, prshoper, proper, prmachine, prschedoper, termdost
      do begin
        ref = ref + 1;
        ktm = '';
        wersjaref = null;
        descript = proper;
        suspend;
        tmp3 = parent;
        parent = ref;
        tmp2 = parent;
        for
          select p.ref, p.ktm, p.wersjaref, p.amount, p.pozzamref
            from prschedguidespos p
            where p.prschedguide = :prschedguide and p.prschedoper = :prschedoper and p.activ = 1 and p.out = 1
            into prschedguidepos, ktm, wersjaref, quantity, pozzamref
        do begin
          if (exists (select first 1 1 from relations r where r.stableto = 'POZZAM' and r.srefto = :pozzamref and r.stablefrom = 'POZZAM')) then
          begin
            prschedguidepos = null;
            for
              select r.sreffrom
                from relations r
                where r.stableto = 'POZZAM' and r.srefto = :pozzamref and r.stablefrom = 'POZZAM'
                into tmpref
            do begin
              for
                select s.ref, s.parent, s.pozzamref, s.nagzamref, s.nagzamid,
                    s.ktm, s.wersjaref, s.prschedguide, s.prschedguidepos, s.prschedoper,
                    s.quantity, s.quantityr, s.prshoper, s.proper, s.prmachine, s.descript, s.termdost
                  from pr_show_schedview_cascade(:ref,:parent,:tmpref,:schedviewin) s
                  into ref, parent, pozzamref, nagzamref, nagzamid,
                    ktm, wersjaref, prschedguide, prschedguidepos, prschedoper,
                    quantity, quantityr, prshoper, proper, prmachine, descript, termdost
              do begin
                suspend;
              end
            end
          end else
          begin
            ref = ref + 1;
            descript = ktm;
            prschedoper = null;
            proper = '';
            prshoper = null;
            prmachine = null;
            suspend;
            prschedguidepos = null;
          end
        end
        prschedguidepos = null;
        parent = tmp2;
      end
      parent = tmp3;
    end else if (wydania = 1) then
    begin
      for
        select r.sreffrom
          from relations r
          where r.stableto = 'POZZAM' and r.srefto = :pozzamref and r.stablefrom = 'POZZAM'
          into tmpref
      do begin
        for
          select s.ref, s.parent, s.pozzamref, s.nagzamref, s.nagzamid,
              s.ktm, s.wersjaref, s.prschedguide, s.prschedguidepos, s.prschedoper,
              s.quantity, s.quantityr, s.prshoper, s.proper, s.prmachine, s.descript, s.termdost
            from pr_show_schedview_cascade(:ref,:parent,:tmpref,:schedviewin) s
            into ref, parent, pozzamref, nagzamref, nagzamid,
                ktm, wersjaref, prschedguide, prschedguidepos, prschedoper,
                quantity, quantityr, prshoper, proper, prmachine, descript, termdost
        do begin
          suspend;
        end
      end
      parent = tmp3;
    end
  end
end^
SET TERM ; ^
