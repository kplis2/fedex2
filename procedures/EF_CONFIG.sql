--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_CONFIG(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      AKRONIM varchar(20) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable wartosc varchar(20);
begin

  execute procedure GET_CONFIG(AKRONIM, 2) returning_values ret;

  ret = replace (ret, ',', '.');

  suspend;
end^
SET TERM ; ^
