--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XP_CZAS(
      KEY1 integer,
      KEY2 integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      TYP varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(16,6))
   as
declare variable opertime numeric(16,6);
declare variable eopertime varchar(1024);
declare variable parref integer;
declare variable parvalue numeric(16,6);
declare variable parmodified smallint;
declare variable paramname varchar(255);
declare variable fromprshmat integer;
declare variable fromprshoper integer;
declare variable fromprshtool integer;
begin
/* TYP:
  (pusty) - czas jednostkowy
  J - czas jednostkowy
  P - czas przygotowania
*/
  opertime = 0;
  paramname='CZAS('||:typ||')';
  -- czy parametr jest uprzednio zapisany i zmodyfikowany
  parref = NULL;
  select fromprshmat, fromprshoper, fromprshtool from prcalccols where ref=:key2
    into :fromprshmat, :fromprshoper, :fromprshtool;
  select first 1 ref, cvalue, modified
    from prcalccols where parent=:key2 and expr=:paramname and calctype=1
    into :parref, :parvalue, :parmodified;
  if(:parref is not null and :parmodified=1) then begin
    ret = :parvalue;
    exit;
  end
  if(:typ is null or :typ='') then typ = 'J';

  -- nalezy obliczyc na nowo
  if(:fromprshoper is not null) then begin
    if(:typ='J') then begin
      select EOPERTIME from PRSHOPERS where PRSHOPERS.REF=:fromprshoper
      into :eopertime;
    end else begin
      select OPERTIMEPF from PRSHOPERS where PRSHOPERS.REF=:fromprshoper
      into :eopertime;
    end
  end
  opertime = NULL;
  execute procedure STATEMENT_PARSE(:eopertime,:key1,:key2,:prefix) returning_values :opertime;
  if(:opertime is null) then opertime = 0;
  ret = :opertime;

  if (ret is null) then ret = 0;
  -- nalezy zapisac wyliczony parametr
  if(:parref is not null) then begin
    update prcalccols set cvalue=:ret where ref=:parref;
  end else begin
    insert into prcalccols(PARENT,CVALUE,EXPR,DESCRIPT,CALCTYPE,PRCOLUMN)
    values(:key2,:ret,:paramname,'Czas',1,null);
  end
end^
SET TERM ; ^
