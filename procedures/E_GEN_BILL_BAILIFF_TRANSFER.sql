--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GEN_BILL_BAILIFF_TRANSFER(
      FROMDATE date,
      TODATE date,
      BANKACC varchar(10) CHARACTER SET UTF8                           ,
      CURRENTCOMPANY integer,
      TDATE date = null)
  returns (
      COUNTER integer)
   as
declare variable toacc varchar(60);
declare variable amount numeric(14,2);
declare variable descript varchar(255);
declare variable toadress varchar(255);
declare variable address varchar(255);
declare variable postcode varchar(6);
declare variable city varchar(255);
declare variable towho varchar(255);
declare variable komornik integer;
declare variable varcourtcase varchar(80);
declare variable varsignature varchar(80);
declare variable varperson varchar(81);
declare variable btransfer integer;
declare variable payroll integer;
declare variable ecolldedinstal integer;
declare variable przelewtype varchar(10);
declare variable payday timestamp;
declare variable isreqtdate smallint = 0;
begin
--Generowanie przelewow komorniczych do rachunkow UCP

  counter = 0;
  if (coalesce(tdate,cast('1899-12-30' as date)) <> cast('1899-12-30' as date)) then
  begin
    isreqtdate = 1;
    if (tdate < current_timestamp(0)) then tdate = current_date;
  end
  execute procedure getconfig('PRZELEWKOM') returning_values :przelewtype;

  for
   select d.bailiff, trim(coalesce(d.courtcase,'')), trim(coalesce(d.signature,''))
        , p.person, i.insvalue, r.payday, r.ref, i.ref
      from ecolldeductions d
        join ecolldedinstals i on (d.ref = i.colldeduction)
        join epayrolls r on (r.ref = i.payroll)
        join persons p on (p.ref = d.person)
      where r.empltype = 2
        and r.bailiffbtr is null and i.btransfer is null
        and r.payday >= :fromdate and r.payday <= :todate
        and r.company = :currentcompany
      into :komornik, :varcourtcase,  :varsignature, :varperson, :amount, :payday, :payroll, :ecolldedinstal
  do begin

    if (isreqtdate = 0) then
    begin
      tdate = payday;
      if (tdate < current_timestamp(0)) then tdate = current_date;
    end

    if (komornik is not null) then
    begin
      select account, bailiffname, street, city, postcode
        from ebailiff
        where ref = :komornik
        into :toacc, :towho, :address, :city, :postcode;

      toadress = coalesce(address,'') || '; ' || coalesce(postcode,'') || ' ' || coalesce(city,'');
    end else
      toadress = '';

    descript = 'Przelew komorniczy ' || varperson;
    if (varcourtcase <> '') then descript = descript || ', Sprawa: ' || varcourtcase;
    if (varsignature <> '') then descript = descript || ', Sygnatura: ' || varsignature;

    execute procedure gen_ref('BTRANSFERS')
      returning_values btransfer;

    insert into btransfers (ref, bankacc, typ, btype, data, towho, toacc,
        toadress, todescript, curr, amount, autobtransfer, company)
      values (:btransfer, :bankacc, 1, :przelewtype, :tdate, :towho, :toacc,
        :toadress, :descript, 'PLN', :amount, 1, :currentcompany);

    update epayrolls set bailiffbtr = :btransfer where ref = :payroll;
    update ecolldedinstals set btransfer = :btransfer where ref = :ecolldedinstal;

    counter = counter + 1;
  end
  suspend;
end^
SET TERM ; ^
