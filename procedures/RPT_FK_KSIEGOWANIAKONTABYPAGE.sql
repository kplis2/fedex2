--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_KSIEGOWANIAKONTABYPAGE(
      FROMPERIOD varchar(6) CHARACTER SET UTF8                           ,
      TOPERIOD varchar(6) CHARACTER SET UTF8                           ,
      BKREG varchar(10) CHARACTER SET UTF8                           ,
      ACCTYP smallint,
      ACCOUNTMASK varchar(25) CHARACTER SET UTF8                           ,
      WITHMIDDLE smallint,
      COMPANY integer,
      MODE smallint = 0,
      FROMPAGE integer = 1,
      DELTA integer = 0)
  returns (
      REF NUMERIC_14_2,
      DOCREF integer,
      DOCSYMBOL varchar(30) CHARACTER SET UTF8                           ,
      DOCDATE timestamp,
      TRANSDATE timestamp,
      DOCREG varchar(10) CHARACTER SET UTF8                           ,
      NRLEDGER integer,
      NRREGISTER integer,
      LP integer,
      ACCOUNT varchar(20) CHARACTER SET UTF8                           ,
      DESCRIPT STRING1024,
      DEBIT numeric(15,2),
      CREDIT numeric(15,2),
      SDEBIT numeric(15,2),
      SCREDIT numeric(15,2),
      PDEBIT numeric(15,2),
      PCREDIT numeric(15,2),
      SETTLEMENT varchar(30) CHARACTER SET UTF8                           ,
      STLOPENDATE timestamp,
      STLPAYDATE timestamp,
      TOSUMMARY smallint,
      ACCHEAD smallint,
      NOPAGE integer,
      LASTPAGE integer,
      BALANCETYPE SMALLINT_ID)
   as
  declare variable pagesize numeric_14_2;
  declare variable linecount integer_id;
  declare variable pagesumdebit numeric_14_2;
  declare variable pagesumcredit numeric_14_2;
  declare variable ppagesumdebit numeric_14_2;
  declare variable ppagesumcredit numeric_14_2;
  declare variable currentpage integer_id;
  declare variable tmpcurrentpage integer_id;
  declare variable printsum smallint_id;
  declare variable tmp_TOSUMMARY smallint_id;
begin
  pagesize = 34;
  printsum = 0;
  REF = 1;
  pagesumdebit = 0;
  pagesumcredit = 0;
  ppagesumdebit = 0;
  ppagesumcredit = 0;
  insert into expimp ( S1, S2, S3)
  values (:frompage, :delta,'WYDRUK');
  -- wyliczenie ilosci stron
  select count(1)
    from RPT_FK_KSIEGOWANIAKONTA (:FROMPERIOD ,:TOPERIOD,:BKREG,:ACCTYP,:ACCOUNTMASK,:WITHMIDDLE,:COMPANY,:MODE)
  into linecount;
  LASTPAGE = ceiling(coalesce(:linecount,0) / pagesize);
  -- czytam kolejne zapisy i podsumowania dla kont
  for
    select REF,DOCREF,DOCSYMBOL,DOCDATE,TRANSDATE,DOCREG,NRLEDGER,NRREGISTER, LP,ACCOUNT,
      DESCRIPT,DEBIT,CREDIT,SDEBIT,SCREDIT,SETTLEMENT,STLOPENDATE,STLPAYDATE,TOSUMMARY,ACCHEAD, balancetype
    from RPT_FK_KSIEGOWANIAKONTA (:FROMPERIOD ,:TOPERIOD,:BKREG,:ACCTYP,:ACCOUNTMASK,:WITHMIDDLE,:COMPANY,:MODE)
    order by ref
      into REF,DOCREF,DOCSYMBOL,DOCDATE,TRANSDATE,DOCREG,NRLEDGER,NRREGISTER, LP,ACCOUNT,
        DESCRIPT,DEBIT,CREDIT,SDEBIT,SCREDIT,SETTLEMENT,STLOPENDATE,STLPAYDATE,TOSUMMARY,ACCHEAD, balancetype
  do begin
    -- wyliczenie numeru strony
    tmp_TOSUMMARY = :TOSUMMARY;
    NOPAGE = floor((:ref-1.00)/:pagesize) + 1;
    if(:FROMPAGE <= :NOPAGE and (:FROMPAGE + :DELTA) > :NOPAGE) then begin
      printsum = 1;
      suspend;
      -- jesli skonczyl zakres wychodzi
    end
    -- inicjuje numer pierwszej strony wydruku
    if(currentpage is null) then currentpage = :NOPAGE;
    -- wyliczanie podsumowań strony
    if(:nopage > :currentpage and mode = 1) then begin
      tmpcurrentpage = :nopage;
      nopage = :currentpage;
      tosummary = -1;
      acchead = 2;
      LP = 0;
      DESCRIPT = 'Razem: ';
      PDEBIT = pagesumdebit + ppagesumdebit;
      PCREDIT = pagesumcredit + ppagesumcredit;
      if(:FROMPAGE <= :NOPAGE and (:FROMPAGE + :DELTA) > :NOPAGE) then begin
        ref = ref - 0.1;
        suspend;
      end
      DESCRIPT = 'Z przeniesienia';
      PDEBIT = ppagesumdebit;
      PCREDIT = ppagesumcredit;
      if(:FROMPAGE <= :NOPAGE and (:FROMPAGE + :DELTA) > :NOPAGE) then begin
        ref = ref - 0.1;
        suspend;
      end
      DESCRIPT = 'Suma strony: ';
      PDEBIT = pagesumdebit;
      PCREDIT = pagesumcredit;
      if(:FROMPAGE <= :NOPAGE and (:FROMPAGE + :DELTA) > :NOPAGE) then begin
        ref = ref - 0.1;
        suspend;
      end
      -- przeniesienie
      ppagesumdebit = coalesce(ppagesumdebit,0) + pagesumdebit;
      ppagesumcredit = coalesce(ppagesumcredit,0) +  pagesumcredit;
      -- zerowanie sumowań strony
      pagesumdebit = 0;
      pagesumcredit = 0;
      PDEBIT = 0;
      PCREDIT = 0;
      nopage = :tmpcurrentpage;
      currentpage = :tmpcurrentpage;
    end
    if( :tmp_TOSUMMARY = 1) then begin
      pagesumdebit = :pagesumdebit + coalesce(:debit,0);
      pagesumcredit = :pagesumcredit + coalesce(:credit,0);
  end

  end
  -- podsumowanie ostatniej strony
  if (printsum = 1 and (:FROMPAGE + :DELTA) > :NOPAGE-1) then begin
    tosummary = -1;
    acchead = 2;
    LP = 0;
    DESCRIPT = 'Suma strony: ';
    PDEBIT = pagesumdebit;
    PCREDIT = pagesumcredit;
    ref = ref + 1;
    suspend;
    DESCRIPT = 'Z przeniesienia';
    PDEBIT = ppagesumdebit;
    PCREDIT = ppagesumcredit;
    ref = ref +1;
    suspend;
    DESCRIPT = 'Razem: ';
    PDEBIT = pagesumdebit + ppagesumdebit;
    PCREDIT = pagesumcredit + ppagesumcredit;
    ref = ref + 1;
    suspend;
  end
end^
SET TERM ; ^
