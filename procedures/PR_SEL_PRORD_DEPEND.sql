--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_SEL_PRORD_DEPEND(
      PRORD integer)
  returns (
      PRORDOUT integer)
   as
declare variable prpozzam integer;
begin
  for
    select p.ref
      from pozzam p
      where p.zamowienie = :prord and p.out = 0
      into prpozzam
  do begin
    for
      select prnagzamout
        from PR_POZZAM_CASCADE (:prpozzam,0,0,0,1)
        where postypeout = 0
        into prordout
    do begin
      suspend;
    end
  end
end^
SET TERM ; ^
