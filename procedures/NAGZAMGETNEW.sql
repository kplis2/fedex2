--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGZAMGETNEW(
      KLIENT integer)
  returns (
      NAGZAMREF NAGZAM_ID)
   as
declare variable ODDZIAL varchar(10); /* z klienta; */
declare variable MAGAZYN char(3);
declare variable DULICA STREET_ID;
declare variable DNRDOMU NRDOMU_ID;
declare variable DNRLOKALU NRDOMU_ID;
declare variable DMIASTO CITY_ID;
declare variable DKODP char(6);
declare variable ODBIORCA ODBIORCY_ID;
declare variable BN char(1);
declare variable typzam varchar(10);
declare variable rejzam varchar(10);
begin
  --znalezienie ddzialu i magazynu
  select first 1 k.oddzial, o.magazyn, k.dulica, k.dnrdomu, k.dnrlokalu, k.dmiasto, k.dkodp, odb.ref,
                case when k.firma = 1 then 'N' else 'B' end
    from klienci k
      left join oddzialy o on k.oddzial=o.oddzial
      left join odbiorcy odb on k.ref=odb.klient
    where k.ref = :klient
    into :oddzial, :magazyn, :dulica, :dnrdomu, :dnrlokalu, :dmiasto, :dkodp, :odbiorca, :bn;

  execute procedure get_config('NEWZAMTYPSPR',2) returning_values :typzam;
  execute procedure get_config('NEWZAMREJSPR',2) returning_values :rejzam;
  if(coalesce(:magazyn,'')='') then
    execute procedure get_config('MAGSPR',2) returning_values :magazyn;
  -- sprawdzenie czy nagzam istnieje
  select first 1 n.ref
    from nagzam n
    where n.klient = :klient
      and n.rejestr = :rejzam
      and n.typzam = :typzam
      and (n.magazyn = :magazyn or :magazyn is null)
      and n.datawe <= current_date
      and n.datazm >= current_date
    order by ref
    into :nagzamref;
  -- utworzenie nowego nagzama w przypadku braku
  if(nagzamref is null or nagzamref = 0) then
    insert into nagzam(klient, rejestr, typzam, typ, oddzial, magazyn, dmiasto,  dulica, dnrdomu, dnrlokalu, dkodp, odbiorcaid, bn)
      values(:klient, :rejzam, :typzam, 0, :oddzial, :magazyn, :dmiasto, :dulica, :dnrdomu, :dnrlokalu, :dkodp, :odbiorca, :bn)
      returning ref into :nagzamref;
  suspend;
end^
SET TERM ; ^
