--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COPY_PRDVERSION(
      PRDV integer,
      OKRES varchar(6) CHARACTER SET UTF8                           )
   as
declare variable PRD integer;
declare variable NEWPRDV integer;
declare variable NEWPA integer;
declare variable DEDUCTACCOUNT ACCOUNT_ID;
declare variable DEDUCTAMOUNT numeric(14,2);
declare variable FIRSTDEDUCTAMOUNT numeric(14,2);
declare variable DEDUCTEDAMOUNT numeric(14,2);
declare variable DIST1DDEF integer;
declare variable DIST1SYMBOL SYMBOLDIST_ID;
declare variable DIST2DDEF integer;
declare variable DIST2SYMBOL SYMBOLDIST_ID;
declare variable DIST3DDEF integer;
declare variable DIST3SYMBOL SYMBOLDIST_ID;
declare variable DIST4DDEF integer;
declare variable DIST4SYMBOL SYMBOLDIST_ID;
declare variable DIST5DDEF integer;
declare variable DIST5SYMBOL SYMBOLDIST_ID;
declare variable DIST6DDEF integer;
declare variable DIST6SYMBOL SYMBOLDIST_ID;
declare variable DESCRIPT varchar(255);
declare variable DIVRATE numeric(14,6);
begin
  select pd.prdaccounting from prdversions pd where pd.ref = :prdv into :prd;
  ----generowanie nowej wersji
  execute procedure GEN_REF('PRDVERSIONS') returning_values :newprdv;
  insert into prdversions(ref, prdaccounting, fromperiod) values (:newprdv, :prd, :okres);

  for select pa.deductaccount, pa.deductamount, pa.firstdeductamount, pa.dist1ddef, pa.dist1symbol,
        pa.dist2ddef, pa.dist2symbol, pa.dist3ddef, pa.dist3symbol, pa.dist4ddef, pa.dist4symbol, pa.dist5ddef, pa.dist5symbol,
        pa.dist6ddef, pa.dist6symbol, pa.descript, pa.divrate
  from prdaccountingpos pa where pa.prdversion = :prdv
  into :deductaccount, :deductamount, :firstdeductamount, :dist1ddef, :dist1symbol, :dist2ddef, :dist2symbol,
       :dist3ddef, :dist3symbol, :dist4ddef, :dist4symbol, :dist5ddef, :dist5symbol, :dist6ddef, :dist6symbol, :descript,
       :divrate
  do begin
     execute procedure GEN_REF('PRDACCOUNTINGPOS') returning_values :newpa;
     insert into prdaccountingpos(REF, prdaccounting, PRDVERSION, deductaccount, deductamount, firstdeductamount,
                     dist1ddef, dist1symbol, dist2ddef, dist2symbol, dist3ddef, dist3symbol, dist4ddef,
                     dist4symbol, dist5ddef, dist5symbol, dist6ddef, dist6symbol, descript, divrate)
                 values(:newpa, :prd, :newprdv, :deductaccount, :deductamount, :firstdeductamount,
                     :dist1ddef, :dist1symbol, :dist2ddef, :dist2symbol, :dist3ddef, :dist3symbol, :dist4ddef,
                     :dist4symbol, :dist5ddef, :dist5symbol, :dist6ddef, :dist6symbol, :descript, :divrate);
  end
end^
SET TERM ; ^
