--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHORTAGES_DEFAULT(
      PRSCHEDOPER integer,
      PRSCHEDGUIDESPOSIN integer,
      PRNAGZAMIN integer)
  returns (
      AMOUNT numeric(14,4),
      KTM varchar(80) CHARACTER SET UTF8                           ,
      PRSCHEDGUIDESPOSOUT integer,
      PRPOZZAMOUT integer)
   as
declare variable amountcur numeric(14,4);
declare variable kamountnorm numeric(14,4);
declare variable cnt integer;
declare variable guide integer;
begin
  ktm = null;
  prschedguidesposout = null;
  prpozzamout = null;
--default ktm
  if(prschedoper > 0) then begin
    select guide from prschedopers where ref =:prschedoper into :guide;
    select count(*) from PRSCHEDGUIDESPOS where PRSCHEDGUIDE = :guide into :cnt;
    if(cnt = 1) then begin
      select max(ref) from prschedguidespos where PRSCHEDGUIDE = :guide into :prschedguidesposout;
      select ktm from prschedguidespos where ref = :prschedguidesposout into :ktm;
      prschedguidesposin = prschedguidesposout;
    end
  end else if(prnagzamin > 0) then begin
    select count(*) from POZZAM where ZAMOWIENIE = :prnagzamin into :cnt;
    if(cnt = 1) then begin
      select max(ref) from pozzam where zamowienie = :prnagzamin into :prpozzamout;
      select ktm from pozzam where ref = :prpozzamout into :ktm;
    end
  end
--podpowiedz ilosci
  amount = 0;
  if(prschedoper is not null and prschedoper > 0) then begin
    if(prschedguidesposin is not null and prschedguidesposin > 0) then begin
      select amountin * (case when psho.reportedfactor is not null and psho.reportedfactor > 0 then psho.reported else 1 end)
        - amountresult
        from prschedopers pscho
        left join prshopers psho on (pscho.shoper = psho.ref)
        where pscho.ref = :prschedoper
      into :amount;
      select sum(psh.amount)
        from prshortages psh
        left join prshortagestypes psht on (psh.prshortagestype = psht.symbol)
        where psh.prschedoper = :prschedoper and psh.prschedguidespos = :prschedguidesposin
          and psht.noamountinfluence = 0
      into :amountcur;
      if(amountcur is null) then amountcur = 0;
      select kamountnorm from prschedguidespos where ref = :prschedguidesposin into :kamountnorm;
      amount = amount * kamountnorm - amountcur;
    end
  end
  if(amount is null or amount < 0) then amount = 0;
  if(PRSCHEDGUIDESPOSOUT is null) then PRSCHEDGUIDESPOSOUT = 0;
  if(PRPOZZAMOUT is null) then PRPOZZAMOUT = 0;
  suspend;
end^
SET TERM ; ^
