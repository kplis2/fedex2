--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_FUN_DG072(
      PERIOD varchar(6) CHARACTER SET UTF8                           )
  returns (
      AMOUNT numeric(15,2))
   as
DECLARE VARIABLE okres varchar(6);
DECLARE VARIABLE wynik numeric(14,2);
DECLARE VARIABLE ile integer;
DECLARE VARIABLE miesiac integer;
begin
  okres = substring(period from 1 for 4)||'01';
  miesiac = 1;
  wynik = 0;
  ile = 0;
  AMOUNT = 0;
  while (okres<=PERIOD) do
  begin
    ile = ile + 1;
    miesiac = miesiac +1;
    --exception  test_break;
    execute procedure FK_FUN_DG071(:okres)
      returning_values :wynik;
    if (wynik is null) then
      wynik = 0;
    AMOUNT = AMOUNT + wynik;

    if (miesiac > 9) then
      okres = substring(period from 1 for 4)||miesiac;
    else
      okres = substring(period from 1 for 4)||'0'||miesiac;
  end
  AMOUNT = AMOUNT/ile;
--  AMOUNT = AMOUNT + 0.5;
  AMOUNT = cast(AMOUNT as integer);
  suspend;
end^
SET TERM ; ^
