--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EPRESENCELISTS_AUTOFILL(
      BDATE date,
      EDATE date,
      SOURCE integer)
   as
declare variable operator integer;
declare variable employee integer;
declare variable poref integer;
declare variable accord smallint;
declare variable startat timestamp;
declare variable stopat timestamp;
declare variable econtractsdef integer;
declare variable econtractsdefpos integer;
declare variable sdate date;
declare variable eprl integer;
declare variable eprln integer;
declare variable workend varchar(10);
declare variable workstart varchar(10);
declare variable shiftend varchar(10);
declare variable shiftstart varchar(10);
declare variable shift2end varchar(100);
declare variable shift2start varchar(100);
declare variable shift3end varchar(100);
declare variable shift3start varchar(100);
declare variable timediff integer;
declare variable shift smallint;
declare variable autopresent smallint;
declare variable calendar integer;
declare variable timeend time;
declare variable timestart time;
begin
  if (source is null) then
    source = 0;
  -- uzupelnienie z listy plac
  if (source = 0) then
  begin
    for
      select distinct ep.operator, ep.employee, 1, '08:00', '08:30',
          eppos.econtractsposdef, eppos.econtractsdef, cast(eppos.sdate as date)
        from econtrpayrollspos eppos
          left join econtrpayrolls ep on (ep.ref = eppos.econtrpayrolls)
        where cast(eppos.sdate as date) <= :edate and cast(eppos.sdate as date) >= :bdate
          and eppos.accord = 1
        into operator, employee, accord, workstart, workend,
             econtractsdefpos, econtractsdef, sdate
    do begin
      if (not exists (select ref from epresencelists where cast(listsdate as date) = :sdate)) then
        insert into epresencelists (listsdate)
        values (:sdate);
      select max(ref) from epresencelists
        where cast(listsdate as date) = :sdate
        into :eprl;
      if (:eprl is not null) then
      begin
        if (not exists (select ref from epresencelistnag
          where operator = :operator and employee = :employee
            and epresencelist = :eprl)) then
        begin
          insert into epresencelistnag (operator,timestart,timestop,wastoday,epresencelist,employee)
         values (:operator,:workstart, :workend,0,:eprl,:employee);
        end
        select ref from epresencelistnag
          where operator = :operator and employee = :employee
            and epresencelist = :eprl
          into :eprln;
        if (:eprln is not null) then
        begin
          if (not exists (select ref from epresencelistspos where epresencelistnag = :eprln
              and econtractsdef = :econtractsdef
              and econtractposdef = :econtractsdefpos)) then
          begin
            insert into epresencelistspos (epresencelists, econtractsdef, operator, accord,
                timestartstr,timestopstr,epresencelistnag,pointssource,econtractposdef,employee)
              values(:eprl, :econtractsdef, :operator, 1,
                  :workstart, :workend, :eprln, 0,:econtractsdefpos,:employee);
          end
        end
      end
    end
  -- naliczanie z operacji produkcyjnych
  end else if (source = 1) then
  begin
    execute procedure get_config('SHIFT2START',2) returning_values shift2start;
    execute procedure get_config('SHIFT2END',2) returning_values shift2end;
    execute procedure get_config('SHIFT3START',2) returning_values shift3start;
    execute procedure get_config('SHIFT3END',2) returning_values shift3end;
    for
      select o.ref, p.employee, 0, p.maketimefrom, p.maketimeto,
          p.econtractsposdef, p.econtractsdef, cast(p.maketimefrom as date),
          cast(p.maketimeto as date) - cast(p.maketimefrom as date),
          substring(cast(cast(p.maketimefrom as time) as varchar(100)) from 1 for 5),
          substring(cast(cast(p.maketimeto as time) as varchar(100)) from 1 for 5),
          e.autopresent - 1, e.calendar, p.ref
        from propersraps p
          left join operator o on (o.employee = p.employee)
          left join employees e on (e.ref = p.employee)
        where cast(p.maketimefrom as date) <= :edate
          and cast(p.maketimefrom as date) >= :bdate
          and p.employee is not null and p.maketimefrom is not null
          and e.autopresent > 0
        into operator, employee, accord, startat, stopat,
             econtractsdefpos, econtractsdef, sdate,
             timediff,
             workstart,
             workend,
             autopresent, calendar, poref
    do begin
      eprl = null;
      eprln = null;
      if (not exists (select ref from epresencelists where cast(listsdate as date) = :sdate)) then
        insert into epresencelists (listsdate)
        values (:sdate);
      select max(ref) from epresencelists
        where cast(listsdate as date) = :sdate
        into :eprl;
      if (:eprl is not null) then
      begin
        -- okreslenie zmiany
        shift = 2;
        if (cast(workstart as time) < cast(shift2end as time)
            and cast(workstart as time) >= cast(shift2start as time)
            and timediff = 0
        ) then
          shift = 1;
        else if (cast(workstart as time) < cast(shift2start as time)
            and cast(workstart as time) >= cast(shift3end as time)
            and timediff = 0
        ) then
          shift = 0;
        -- sprawdzenie czy nie ma juz naglowka listy obecnosci
        execute procedure EWORKTIME_CALCULATE(employee, calendar, shift, eprl,null)
          returning_values (timestart, timeend);
        shiftstart = substring(cast(timestart as varchar(20)) from 1 for 5);
        shiftend = substring(cast(timeend as varchar(20)) from 1 for 5);
        if (not exists (select ref from epresencelistnag
          where employee = :employee
            and epresencelist = :eprl)) then
        begin
          insert into epresencelistnag (operator,timestart,timestop,wastoday,epresencelist,employee, shift)
            values (:operator,:shiftstart, :shiftend,:autopresent,:eprl,:employee, :shift);
        end
        else
        begin
          update epresencelistnag set timestart = :shiftstart, timestop = :shiftend,
              wastoday = 1, shift = :shift
            where employee = :employee and epresencelist = :eprl;
        end
        select ref from epresencelistnag
          where employee = :employee and shift = :shift
            and epresencelist = :eprl
          into :eprln;
        if (:eprln is not null) then
        begin
          workstart = substring(workstart from 1 for 5);
          workend = substring(workend from 1 for 5);
          if (not exists(select ref from epresencelistspos
             where doctype = 'PROPERSRAPS' and docref = :poref and epresencelistnag = :eprln)) then
            insert into epresencelistspos (epresencelists, econtractsdef, operator, accord,
                timestartstr,timestopstr,epresencelistnag,
                pointssource,econtractposdef,employee, docref, doctype)
              values(:eprl, :econtractsdef, :operator, 0,
                  :workstart, :workend, :eprln, 0,:econtractsdefpos,:employee, :poref, 'PROPERSRAPS');
        end
      end
    end
  end
end^
SET TERM ; ^
