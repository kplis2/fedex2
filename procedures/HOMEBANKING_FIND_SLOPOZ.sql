--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE HOMEBANKING_FIND_SLOPOZ(
      TABELA varchar(40) CHARACTER SET UTF8                           ,
      NAZWA varchar(1025) CHARACTER SET UTF8                           )
  returns (
      SLODEF integer,
      SLOPOZ integer)
   as
begin
    select ref  from slodef where slodef.typ = :tabela into :slodef;
    if (tabela = 'KLIENCI') then begin
      select min(klienci.ref) from klienci where klienci.nazwa like '%'||:nazwa||'%' into :slopoz;
      if (:slopoz is null) then select min(klienci.ref) from klienci into :slopoz;
    end else if (tabela ='DOSTAWCY') then begin
      select min(dostawcy.ref) from dostawcy where dostawcy.nazwa like '%'||:nazwa||'%' into :slopoz;
      if (:slopoz is null) then select min(dostawcy.ref) from dostawcy into :slopoz;
    end
  suspend;
end^
SET TERM ; ^
