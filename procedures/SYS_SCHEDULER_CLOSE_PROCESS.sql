--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_SCHEDULER_CLOSE_PROCESS(
      NEOS_NOW TIMESTAMP_ID,
      INSTANCE INTEGER_ID = null,
      ACTIVITY_SCHEDULE SYS_SCHEDULE_ID = null,
      SCHEDULER_SYMBOL SYS_SCHEDULER_SYMBOL_ID = null)
  returns (
      RET smallint)
   as
begin
  --Zamyka wybrany proces schedulingu lub wszystkie
  ret = 1;
  --exception universal'  '||:instance;
  update sys_schedulehist sh set stop = :neos_now
    where sh.schedule = coalesce(:activity_schedule, sh.schedule)
      and sh.ref = coalesce(:instance, sh.ref)
      and sh.stop is null
      and coalesce(sh.scheduler_symbol, '') = coalesce(:scheduler_symbol, '');

  update sys_schedule s set lastinstance = null
    where s.ref = coalesce(:activity_schedule, s.ref)
      and coalesce(s.scheduler_symbol, '') = coalesce(:scheduler_symbol, '');

  if ( exists(
    select first 1 1
    from sys_schedule s
    where s.ref = :activity_schedule
          and s.lastinstance is not null
          and coalesce(s.scheduler_symbol, '') = coalesce(:scheduler_symbol, '')
    )) then
      ret = 0;
  suspend;
end^
SET TERM ; ^
