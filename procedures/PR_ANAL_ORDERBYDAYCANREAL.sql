--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_ANAL_ORDERBYDAYCANREAL(
      PRSCHEDULE integer,
      PRDEPART varchar(255) CHARACTER SET UTF8                           )
  returns (
      DATEP timestamp,
      SYMBOL varchar(255) CHARACTER SET UTF8                           ,
      PLACETIME numeric(14,2),
      TYP smallint,
      PLACEHAVE numeric(14,2),
      DIF numeric(14,2))
   as
declare variable fromdate timestamp;
declare variable todate timestamp;
begin
  if(:prschedule is null) then prschedule = 0;
  if(:prdepart is null) then prdepart = '';
  select p.fromdate, p.todate from prschedules p where p.ref = :prschedule into :fromdate, :todate;
  if(:fromdate is null or :todate is null) then begin
    fromdate = current_date;
    todate = :fromdate + 100;
  end
  datep = :fromdate;
  while(:datep <= :todate) do begin
  --narzedzia
    typ = 0;
    placetime = 0;
    for select max(pt.symbol), sum(po.opertime * n.kilosc * pt.quantity) as czas, max(prt.techjednquantity) as stan--, sum( pr_anal_plancanreal_ktm.planval) as ilosc
      from prshtools pt
      join prtools prt on (prt.symbol=pt.symbol)
      join prshopers po on (pt.opermaster = po.ref)
      join prsheets ps on (ps.ref = po.sheet)
      join nagzam n on (n.prsheet = ps.ref)
      left join prschedules pr on (pr.ref = n.prschedule)
      left join plans pl on (pl.ref = pr.plans)
      where po.complex = 0 and n.termdost = :datep
        and (n.prschedule = :prschedule or :prschedule = 0)
        and (pl.prdepart = :prdepart or :prdepart = '')
    group by pt.symbol
    into :symbol, :placetime, :placehave --, :planval
    do begin
    if(:placehave is null) then placehave=0;
    DIF = placehave - placetime;
    if(:placetime <> 0) then suspend;
    end
    --stanowiska
    typ = 1;
    placetime = 0;
    for select max(pw.descript),sum(po.placetime * n.kilosc) as czas, max(pw.techjednquantity) as stan--, sum( pr_anal_plancanreal_ktm.planval) as ilosc
      from prworkplaces pw
      join prshopers po on (pw.symbol = po.workplace)
      join prsheets ps on (ps.ref = po.sheet)
      join nagzam n on (n.prsheet = ps.ref)
      left join prschedules pr on (pr.ref = n.prschedule)
      left join plans pl on (pl.ref = pr.plans)
      where po.complex = 0 and n.termdost = :datep
        and (n.prschedule = :prschedule or :prschedule = 0)
        and (pl.prdepart = :prdepart or :prdepart = '')
      group by pw.symbol
    into :symbol, :placetime, :placehave --, :planval
    do begin
    if(:placehave is null) then placehave=0;
    DIF = placehave - placetime;
    if(:placetime <> 0) then suspend;
    end
    typ = 2;
    placetime = 0;
    for select 'Roboczogodziny',sum(po.opertime * n.kilosc) as czas, max(prdep.techjednquantity) as stan--, sum( pr_anal_plancanreal_ktm.planval) as ilosc
      from prshopers po
      join prsheets ps on (ps.ref = po.sheet)
      join nagzam n on (n.prsheet = ps.ref)
      left join prschedules pr on (pr.ref = n.prschedule)
      left join plans pl on (pl.ref = pr.plans)
      join prdeparts  prdep on (pl.prdepart=prdep.symbol)

      where po.complex = 0 and n.termdost = :datep
        and (n.prschedule = :prschedule or :prschedule = 0)
        and (pl.prdepart = :prdepart or :prdepart = '')
      into :symbol, :placetime, :placehave --, :planval
    do begin
    if(:placehave is null) then placehave=0;
    DIF = placehave - placetime;
    if(:placetime <> 0) then suspend;
    end
    datep = :datep + 1;
  end
end^
SET TERM ; ^
