--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_CHECK_BARCODE_MWSACTS(
      MWSACTREF INTEGER_ID,
      BARCODE KODKRESKOWY)
  returns (
      STATUS smallint,
      MSG STRING)
   as
declare variable tktm varchar(40);
declare variable unit varchar(5);
declare variable przelicz numeric(14,4);
declare variable mwsbarcode varchar(40);
declare variable goodbarcode varchar(40);
declare variable tymprzelicz numeric(14,4);
declare variable versbarcode integer;
declare variable unitid integer;
declare variable good ktm_id;
declare variable vers integer;
declare variable myslnik smallint_id;
begin
  status = 1;
  msg = '';
  select ma.good, ma.vers
    from mwsacts ma
    where ma.ref = :mwsactref
  into :good, :vers;

  execute procedure xk_mws_barcode_validation(:barcode)
    returning_values :goodbarcode, :versbarcode, :przelicz, :status, :unit, :unitid;
  if (status = 0) then begin
    msg = 'Niepoprawny kod kreskowy';
  end else begin
    if (goodbarcode <> good or versbarcode <> vers) then
    begin
      status = 0;
      msg = 'Niepoprawny kod kreskowy';
    end else begin
      status = 1;
    end
  end
end^
SET TERM ; ^
