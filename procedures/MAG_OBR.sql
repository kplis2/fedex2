--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_OBR(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      DTOD timestamp,
      DTDO timestamp,
      AKT smallint,
      NIEOBROT smallint,
      KOREKTA smallint,
      DOSTAWA integer)
  returns (
      NR integer,
      RROZ integer,
      RPOZ integer,
      RDOK integer,
      RMAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      RKTM varchar(40) CHARACTER SET UTF8                           ,
      RWERSJA integer,
      RNAZWAT varchar(255) CHARACTER SET UTF8                           ,
      RNAZWAW varchar(255) CHARACTER SET UTF8                           ,
      ROKRES varchar(6) CHARACTER SET UTF8                           ,
      RTYP varchar(3) CHARACTER SET UTF8                           ,
      RNUMER integer,
      RSYMBOL varchar(30) CHARACTER SET UTF8                           ,
      RDATA timestamp,
      RAKT smallint,
      ILPLUS numeric(14,4),
      ILMINUS numeric(14,4),
      CENASR numeric(14,4),
      WARTOSC numeric(14,2),
      ILAFTER numeric(14,4),
      WARTAFTER numeric(14,2),
      RKONTRAH varchar(40) CHARACTER SET UTF8                           ,
      RMAG2 varchar(3) CHARACTER SET UTF8                           ,
      RCENANET numeric(14,4),
      RCENABRU numeric(14,4),
      DATAWAZN varchar(255) CHARACTER SET UTF8                           ,
      MARZA numeric(14,2),
      MARZAPR numeric(14,2),
      RDOSTAWA integer,
      DOKUMROZ varchar(255) CHARACTER SET UTF8                           ,
      ILBEFORE numeric(14,4),
      WARTBEFORE numeric(14,2))
   as
declare variable DTODST date;
declare variable WYDANIA smallint;
declare variable KLIENT varchar(255);
declare variable DOSTAWCA varchar(255);
declare variable WARTSNETTO numeric(14,4);
declare variable ZEWN smallint;
declare variable KORYG smallint;
declare variable RCENA numeric(14,2);
declare variable ZNAK smallint;
begin
  if(:dostawa is null or (:dostawa = 0) ) then
    begin
      dtodst = :dtod - 1;
      nr = 0;
      if(nieobrot is null) then nieobrot = 0;
      -- pobierz szybko stan na dzień poczatkowy minus 1
      execute procedure MAG_STANQ(:MAGAZYN,:KTM,:WERSJA,:dtodst,NULL) returning_values :ilafter, :wartafter;
      if(:ilafter is null) then ilafter = 0;
      if(:wartafter is null) then wartafter = 0;
      ilbefore = ilafter;
      wartbefore = wartafter;
      -- pokaz kolejne obroty w zadanym przedziale dat
      for select DOKUMPOZ.REF, DOKUMNAG.REF,DOKUMNAG.magazyn, DOKUMNAG.okres, DOKUMNAG.typ,DOKUMNAG.NUMER, DOKUMNAG.akcept,DOKUMNAG.symbol,
            DOKUMPOZ.KTM, DOKUMPOZ.WERSJA ,DOKUMPOZ.ILOSC,  DOKUMPOZ.cenasr,DOKUMPOZ.wartosc,
            DOKUMPOZ.CENANET, DOKUMPOZ.CENABRU, DOKUMPOZ.WARTSNETTO,
            WERSJE.NAZWAT, WERSJE.NAZWA, DEFDOKUM.WYDANIA, DEFDOKUM.KORYG, DEFDOKUM.ZEWN, DOKUMNAG.DATA,
            KLIENCI.fskrot, DOSTAWCY.id, DOKUMNAG.MAG2

            from DOKUMPOZ
            left join DOKUMNAG on(DOKUMPOZ.DOKUMENT = DOKUMNAG.ref)
            left join WERSJE on (DOKUMPOZ.WERSJAREF = WERSJE.REF)
            left join DEFDOKUM on (DOKUMNAG.typ = DEFDOKUM.symbol)
            left join KLIENCI on (DOKUMNAG.KLIENT = KLIENCI.REF)
            left join DOSTAWCY on (DOKUMNAG.DOSTAWCA = DOSTAWCY.REF)
            where (DOKUMNAG.MAGAZYN = :MAGAZYN)
               and (:akt = 0 or (DOKUMNAG.AKCEPT = 1) or (DOKUMNAG.AKCEPT = 8))
               and (DOKUMNAG.DATA >= :dtod)
               and (DOKUMNAG.DATA <= :dtdo)
               and (DOKUMPOZ.KTM = :ktm)
               and (DOKUMPOZ.WERSJA = :wersja)
               and (DEFDOKUM.NIEOBROT <= :nieobrot)
               and coalesce(DOKUMPOZ.fake,0) = 0
            order by DOKUMNAG.DATA, DOKUMNAG.DATAAKC, DOKUMNAG.REF
            into :RPOZ, :RDOK, :RMAGAZYN, :ROKRES, :RTYP, :RNUMER, :RAKT, :RSYMBOL,
              :RKTM, :RWERSJA, :ILPLUS, :CENASR,:wartosc,
              :RCENANET, :RCENABRU, :WARTSNETTO,
              :RNAZWAT, :RNAZWAW,:wydania, :koryg, :zewn, :RDATA,
              :klient, :dostawca, :RMAG2
            do begin
              ilbefore = ilafter;
              wartbefore = wartafter;

              znak = 1;
                if(korekta = 1 and koryg <> 0) then begin
                  if(wydania = 1) then wydania = 0;
                  else wydania = 1;
                  ilplus = -ilplus;
                  znak = -1;
                end
                if(:wydania = 0) then begin
                  ilminus  = NULL;
                  ilafter = :ilafter + ilplus;
                  wartafter = :wartafter + znak * :wartosc;
                end else begin
                  ilminus = :ilplus;
                  ilplus = NULL;
                  ilafter = :ilafter - :ilminus;
                  wartafter = :wartafter - znak * :wartosc;
                end
              if(:rdata>=:dtod) then begin
                execute procedure GET_DATA_FOR_DOKUMPOZ(:rpoz) returning_values :datawazn, :dokumroz;
                rkontrah = null;
                if(:klient is not null) then rkontrah = :klient;
                if(:dostawca is not null) then rkontrah = :dostawca;
                nr = :nr + 1;
                if(zewn = 1 and wydania = 1 and koryg = 0) then begin
                  marza = wartsnetto - wartosc;
                  if(wartsnetto <> 0) then marzapr = (wartsnetto - wartosc) / wartsnetto*100;
                end else if(zewn = 1 and wydania = 0 and koryg = 1) then begin
                  marza = wartosc - wartsnetto;
                  if(wartsnetto <> 0) then marzapr = (wartosc - wartsnetto) / wartsnetto*100;
                end else begin
                  marza = null;
                  marzapr = null;
                end
               suspend;
              end
            end
  end else begin
      dtodst = :dtod - 1;
      nr = 0;
      if(nieobrot is null) then nieobrot = 0;
      -- pobierz szybko stan na dzień poczatkowy minus 1
      execute procedure MAG_STANQ(:MAGAZYN,:KTM,:WERSJA,:dtodst,:dostawa) returning_values :ilafter, :wartafter;
      if(:ilafter is null) then ilafter = 0;
      if(:wartafter is null) then wartafter = 0;
      ilbefore = ilafter;
      wartbefore = wartafter;
      -- pokaz kolejne obroty w zadanym przedziale dat
      for select DOKUMROZ.ref, DOKUMPOZ.REF, DOKUMNAG.ref, DOKUMNAG.MAGAZYN, DOKUMNAG.okres, DOKUMNAG.typ,
            DOKUMNAG.NUMER, DOKUMNAG.akcept, DOKUMNAG.SYMBOL, DOKUMROZ.KTM, DOKUMROZ.wersja, DOKUMROZ.ILOSC, DOKUMROZ.CENA,
            DOKUMROZ.WARTOSC, DOKUMPOZ.CENANET, DOKUMPOZ.CENABRU, DOKUMROZ.wartsnetto, WERSJE.nazwat, WERSJE.nazwa,
            DEFDOKUM.WYDANIA, DEFDOKUM.koryg, DEFDOKUM.ZEWN, DOKUMNAG.DATA, KLIENCI.FSKROT, DOSTAWCY.ID, DOKUMNAG.MAG2
            from DOKUMROZ
            left join DOKUMPOZ on (DOKUMROZ.POZYCJA = DOKUMPOZ.REF)
            left join DOKUMNAG on(DOKUMPOZ.DOKUMENT = DOKUMNAG.ref)
            left join WERSJE on (DOKUMPOZ.WERSJAREF = WERSJE.REF)
            left join DEFDOKUM on (DOKUMNAG.typ = DEFDOKUM.symbol)
            left join KLIENCI on (DOKUMNAG.KLIENT = KLIENCI.REF)
            left join DOSTAWCY on (DOKUMNAG.DOSTAWCA = DOSTAWCY.REF)
            where (DOKUMROZ.dostawa = :dostawa)
               and (DOKUMNAG.MAGAZYN = :MAGAZYN)
               and (:akt = 0 or (DOKUMNAG.AKCEPT = 1) or (DOKUMNAG.AKCEPT = 8))
               and (DOKUMNAG.DATA >= :dtod)
               and (DOKUMNAG.DATA <= :dtdo)
               and (DOKUMPOZ.KTM = :ktm)
               and (DOKUMPOZ.WERSJA = :wersja)
               and (DEFDOKUM.NIEOBROT <= :nieobrot)
               and coalesce(DOKUMPOZ.fake,0) = 0
            order by DOKUMNAG.DATA, DOKUMNAG.DATAAKC, DOKUMNAG.REF
            into :RROZ, :RPOZ, :RDOK, :RMAGAZYN, :ROKRES, :RTYP, :RNUMER, :RAKT, :RSYMBOL,
              :RKTM, :RWERSJA, :ILPLUS, :CENASR,:wartosc,
              :RCENANET, :RCENABRU, :WARTSNETTO,
              :RNAZWAT, :RNAZWAW,:wydania, :koryg, :zewn, :RDATA,
              :klient, :dostawca, :RMAG2
            do begin
              ilbefore = ilafter;
              wartbefore = wartafter;

              znak = 1;
                if(korekta = 1 and koryg <> 0) then begin
                  if(wydania = 1) then wydania = 0;
                  else wydania = 1;
                  ilplus = -ilplus;
                  znak = -1;

                end

              if(:wydania = 0) then begin
                ilminus  = NULL;
                ilafter = :ilafter + ilplus;
                wartafter = :wartafter + znak*:wartosc;
              end else begin
                ilminus = :ilplus;
                ilplus = NULL;
                ilafter = :ilafter - :ilminus;
                wartafter = :wartafter - znak*:wartosc;
              end
              if(:rdata>=:dtod) then begin
                execute procedure GET_DATA_FOR_DOKUMPOZ(:rpoz) returning_values :datawazn, :dokumroz;
                rkontrah = null;
                if(:klient is not null) then rkontrah = :klient;
                if(:dostawca is not null) then rkontrah = :dostawca;
                nr = :nr + 1;
                if(zewn = 1 and wydania = 1 and koryg = 0) then begin
                  marza = wartsnetto - wartosc;
                  if(wartsnetto <> 0) then marzapr = (wartsnetto - wartosc) / wartsnetto*100;
              end else if(zewn = 1 and wydania = 0 and koryg = 1) then begin
                  marza = wartosc - wartsnetto;
                  if(wartsnetto <> 0) then marzapr = (wartosc - wartsnetto) / wartsnetto*100;
              end else begin
                  marza = null;
                  marzapr = null;
              end
            suspend;
          end
        end
  end
  if (nr=0) then begin
    NR = NULL;
    suspend;
  end
end^
SET TERM ; ^
