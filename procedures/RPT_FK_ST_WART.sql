--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_ST_WART(
      AMPERIOD integer,
      COMPANY integer)
  returns (
      REF integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      NAME varchar(80) CHARACTER SET UTF8                           ,
      AMORTGRP varchar(10) CHARACTER SET UTF8                           ,
      ODDZIAL varchar(10) CHARACTER SET UTF8                           ,
      DEPARTMENT varchar(10) CHARACTER SET UTF8                           ,
      PURCHASEDT timestamp,
      USINGDT timestamp,
      SERIALNO varchar(20) CHARACTER SET UTF8                           ,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      TBKSYMBOL BKSYMBOLS_ID,
      TAMETH varchar(3) CHARACTER SET UTF8                           ,
      TARATE numeric(10,5),
      TACRATE numeric(10,5),
      TACHDATE timestamp,
      FAMETH varchar(3) CHARACTER SET UTF8                           ,
      FARATE numeric(10,5),
      FACRATE numeric(10,5),
      FACHDATE timestamp,
      ACTIV smallint,
      TVALUE numeric(15,2),
      TVALLIM numeric(15,2),
      TALLOWENCE numeric(15,2),
      TREDEMPTION numeric(15,2),
      FVALUE numeric(15,2),
      FREDEMPTION numeric(15,2),
      FREST numeric(15,2),
      PERSON integer,
      COMPARMENT varchar(10) CHARACTER SET UTF8                           ,
      FXDAMINOUT integer,
      AMGRP varchar(1) CHARACTER SET UTF8                           ,
      FBKSYMBOL BKSYMBOLS_ID,
      PVALUE numeric(15,2),
      LIQUIDATIONDATE timestamp,
      LIQUIDATIONPERIOD integer)
   as
declare variable ta numeric(14,2);
declare variable fa numeric(14,2);
declare variable vtvalue numeric(15,2);
declare variable vfvalue numeric(15,2);
declare variable vtredemption numeric(15,2);
declare variable vfredemption numeric(15,2);
begin
  for
    select REF, SYMBOL, NAME, AMORTGRP, ODDZIAL, DEPARTMENT, PURCHASEDT, USINGDT,
      SERIALNO, DESCRIPT, TBKSYMBOL, TAMETH, TARATE, TACRATE, TACHDATE, FAMETH,
      FARATE, FACRATE, FACHDATE, ACTIV, TVALUE, TVALLIM, TALLOWENCE, TREDEMPTION,
      FVALUE, FREDEMPTION, FREST, PERSON, COMPARMENT, FXDAMINOUT, AMGRP, FBKSYMBOL,
      PVALUE, LIQUIDATIONDATE, LIQUIDATIONPERIOD
    from FXDASSETS where company = :company order by symbol
    into :REF, :SYMBOL, :NAME, :AMORTGRP, :ODDZIAL, :DEPARTMENT, :PURCHASEDT, :USINGDT,
    :SERIALNO, :DESCRIPT, :TBKSYMBOL, :TAMETH, :TARATE, :TACRATE, :TACHDATE, :FAMETH,
    :FARATE, :FACRATE, :FACHDATE, :ACTIV, :TVALUE, :TVALLIM, :TALLOWENCE, :TREDEMPTION,
    :FVALUE, :FREDEMPTION, :FREST, :PERSON, :COMPARMENT, :FXDAMINOUT, :AMGRP, :FBKSYMBOL,
    :PVALUE, :LIQUIDATIONDATE, :LIQUIDATIONPERIOD
  do begin
    -- korekta o naliczone amortyzacje
    select coalesce(sum(A.tamort),0), coalesce(sum(A.famort),0)
      from amortization A
      where A.fxdasset = :ref and A.amperiod <= :amperiod
    into :ta, :fa;
    tredemption = tredemption + ta;
    fredemption = fredemption + fa;
-- uwzglednienie dokumentów wartosciowych
    select sum(tvalue+tvallim), sum(fvalue), sum(tredemption), sum(fredemption)
      from valdocs
      where fxdasset=:ref and amperiod<=:amperiod
        and COALESCE(valdocs.corperiod, 0) = 0
      into :Vtvalue, :Vfvalue, :Vtredemption ,:Vfredemption;
    if (Vtvalue is not null) then
      tvalue=tvalue+Vtvalue;
    if (Vfvalue is not null) then
      fvalue=fvalue+Vfvalue;
    if (Vtredemption is not null) then
      tredemption=tredemption+Vtredemption;
    if (Vfredemption is not null) then
      fredemption=fredemption+Vfredemption;

    suspend;            
  end
end^
SET TERM ; ^
