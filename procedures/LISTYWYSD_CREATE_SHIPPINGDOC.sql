--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_CREATE_SHIPPINGDOC(
      DOCREF integer)
  returns (
      SHIPPINGDOC integer,
      STATUS smallint)
   as
begin
/* STATUS
0 - otwarty dokument spedydyjny
1 - zamkniety dokument spedydyjny
*/
  status = 0;
  select dokument, l.akcept
    from listywysdpoz lp
      join listywysd l on (lp.dokument = l.ref)
    where dokref = :docref
  into :shippingdoc, :status;
  if (:shippingdoc is not null) then
  begin
    if (:status > 0) then status = 1;
    exit;
  end

  if (exists(select first 1 1 from rdb$procedures p where p.rdb$procedure_name = 'X_LISTYWYSD_CREATE_SHIPPINGDOC')) then
  begin
    execute statement 'execute procedure X_LISTYWYSD_CREATE_SHIPPINGDOC('||:docref||')'
    into :shippingdoc;
    exit;
  end
  else
  begin
    execute procedure listywys_addpoz(null, null, :docref,  'M')
      returning_values :shippingdoc;
  end
end^
SET TERM ; ^
