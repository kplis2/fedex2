--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CALCULATE_ACC_AMORTIZATION(
      FXDASSET integer,
      AMPERIOD integer)
  returns (
      TAX_AMORT numeric(15,2),
      FIN_AMORT numeric(15,2),
      TTYPE smallint,
      FTYPE smallint,
      TARATE numeric(10,5),
      FARATE numeric(10,5))
   as
declare variable USING_DATE timestamp;
declare variable USING_DATE_YEAR integer;
declare variable USING_DATE_MONTH integer;
declare variable A_YEAR integer; /* end year */
declare variable A_MONTH integer; /* end month */
declare variable PERIOD_REF integer;
declare variable PERIOD_YEAR integer;
declare variable PERIOD_MONTH integer;
declare variable TAX_VALUE numeric(15,2);
declare variable TAX_REDEMPTION numeric(15,2);
declare variable ACC_TAX_REDEMPTION numeric(15,2);
declare variable TAX_VALUE_DOC numeric(15,2);
declare variable TAX_REDEMPTION_DOC numeric(15,2);
declare variable FIN_VALUE numeric(15,2);
declare variable FIN_REDEMPTION numeric(15,2);
declare variable ACC_FIN_REDEMPTION numeric(15,2);
declare variable FIN_VALUE_DOC numeric(15,2);
declare variable FIN_REDEMPTION_DOC numeric(15,2);
declare variable TAX_INITIAL_VALUE numeric(15,2);
declare variable FIN_INITIAL_VALUE numeric(15,2);
declare variable TAX_AMORT_RATE numeric(15,2);
declare variable FIN_AMORT_RATE numeric(15,2);
declare variable TAX_AMORT_CRATE numeric(15,2);
declare variable FIN_AMORT_CRATE numeric(15,2);
declare variable TAX_AMORT_CHANGE_DATE timestamp;
declare variable FIN_AMORT_CHANGE_DATE timestamp;
declare variable TAX_AMORT_METH_TYPE integer; /* 0 - L, 1 - D, 2 - J */
declare variable TAX_AMORT_METH_MONTH integer; /* 0 - nastepny, 1-bieżący */
declare variable FIN_AMORT_METH_TYPE integer; /* 0 - L, 1 - D, 2 - J */
declare variable FIN_AMORT_METH_MONTH integer; /* 0 - nastepny, 1-bieżący */
declare variable REF integer;
declare variable COMMMPANY integer;
begin
  acc_tax_redemption = 0;
  acc_fin_redemption = 0;

  -- znaduje rok, okres w AMPERIOD
  select AMYEAR, AMMONTH
    from AMPERIODS where REF=:AMPERIOD
    into :a_year, :a_month;


  -- odczytuje poczatkowa wartosc srodka trwalego i stope amortyzacji
  -- odczytuje date wprowadzenia do exploatacji z FXDASSETS
  SELECT FA.TVALUE, FA.TREDEMPTION, FA.TARATE, FA.TACRATE, AM1.typ, AM1.ammonth,
         FA.FVALUE, FA.FREDEMPTION, FA.FARATE, FA.FACRATE, AM2.typ, AM2.ammonth,
         FA.USINGDT, extract(year from FA.USINGDT), extract(month from FA.USINGDT),
         FA.company
    from FXDASSETS FA
      left join amortmeth AM1 on (AM1.symbol=FA.tameth)
      left join amortmeth AM2 on (AM2.symbol=FA.fameth)
    where ref = :FXDASSET
    into :tax_value, :tax_redemption, :tax_amort_rate, :tax_amort_crate, :tax_amort_meth_type, :tax_amort_meth_month,
         :fin_value, :fin_redemption, :fin_amort_rate, :fin_amort_crate, :fin_amort_meth_type, :fin_amort_meth_month,
         :using_date, using_date_year, using_date_month, :commmpany;

  tax_amort_meth_month = 1 - tax_amort_meth_month;
  fin_amort_meth_month = 1 - fin_amort_meth_month;

  -- przepisanie podstawy do amortyzacji liniowej i jednorazowej
  tax_initial_value = :tax_value;
  fin_initial_value = :fin_value;

  using_date_year = extract(year from using_date);
  using_date_month = extract(month from using_date);


  -- USTALAM KWOTE DOTYCHCZASOWEJ
  tax_amort = 0;
  fin_amort = 0;

  FOR
    SELECT REF, AMYEAR, AMMONTH from AMPERIODS
    where
      -- wiekszy niz UD
      (AMYEAR>:using_date_year or (AMYEAR=:using_date_year and AMMONTH>=:using_date_month)) and
      -- mniejszy LUB ROWNY niz okres z parametrow
      (AMYEAR<:a_year  or (AMYEAR=:a_year and AMMONTH<=:a_month))
       and company = :commmpany
    order by amyear, ammonth
    into period_ref, :period_year, :period_month
  DO BEGIN
    select sum(tvalue), sum(tredemption), sum(fvalue), sum(fredemption)
      from valdocs where fxdasset=:fxdasset and operiod=:period_ref
      into tax_value_doc, tax_redemption_doc, fin_value_doc, fin_redemption_doc;

    if (tax_value_doc is not null) then
      tax_value = tax_value + tax_value_doc;
    if (tax_redemption_doc is not null) then
      tax_redemption = tax_redemption + tax_redemption_doc;
    if (fin_value_doc is not null) then
      fin_value = fin_value + fin_value_doc;
    if (fin_redemption_doc is not null) then
      fin_redemption = fin_redemption + fin_redemption_doc;

    select max(ref)
      from amchngmeth
      where operiod<=:period_ref and amperiod<=:amperiod and fxdasset=:FXDASSET
      into :ref;
    if (:ref is not null) then
    begin
      select AM1.typ, AC.tarate, AC.tacrate, AC.tachdate,
             AM2.typ, AC.farate, AC.facrate, AC.fachdate
        from amchngmeth AC
        left join amortmeth AM1 on (AM1.symbol=AC.tameth)
        left join amortmeth AM2 on (AM2.symbol=AC.fameth)
        where ref=:ref
        into :tax_amort_meth_type, :tax_amort_rate, :tax_amort_crate, :tax_amort_change_date,
             :fin_amort_meth_type, :fin_amort_rate, :fin_amort_crate, :fin_amort_change_date;

    end

    if (period_month=1) then
    begin
      acc_tax_redemption = tax_redemption + tax_amort;
      acc_fin_redemption = fin_redemption + fin_amort;
    end

    --*********** AMORTYZACJA PODATKOWA **************
    if (tax_amort_meth_type=0) then
    -- LINIOWA
    begin
      if ((using_date_year*12+using_date_month)-(period_year*12+period_month)<=-tax_amort_meth_month) then
        tax_amort = tax_amort + (tax_value * tax_amort_rate * tax_amort_crate)/1200;
      ttype = 0;
      if (tax_amort>tax_value-tax_redemption) then
        tax_amort = tax_value-tax_redemption;
    end else
    if (tax_amort_meth_type=1) then
    -- DEGRESYWNA
    begin
      if ((using_date_year*12+using_date_month)-(period_year*12+period_month)<=-tax_amort_meth_month) then
      begin
        if (((tax_value-acc_tax_redemption) * tax_amort_rate * tax_amort_crate) > (tax_value * tax_amort_rate)) then
        begin
          tax_amort = tax_amort + ((tax_value-acc_tax_redemption) * tax_amort_rate * tax_amort_crate)/1200;
          ttype = 1;
        end else
        begin
          tax_amort = tax_amort + (tax_value * tax_amort_rate)/1200;
          ttype = 0;
        end
      end
      if (tax_amort>tax_value-tax_redemption) then
        tax_amort = tax_value-tax_redemption;
    end else
    if (tax_amort_meth_type=2) then
    -- JEDNORAZOWA
    begin
--      if ((using_date_year*12+using_date_month)-(period_year*12+period_month)+1<=tax_amort_meth_month) then
      if ((using_date_year*12+using_date_month)-(period_year*12+period_month)<=-tax_amort_meth_month) then
        tax_amort = tax_value-tax_redemption;
      ttype = 2;
    end


    --*********** AMORTYZACJA FINANSOWA ***************
    if (fin_amort_meth_type=0) then
    -- LINIOWA
    begin
      if ((using_date_year*12+using_date_month)-(period_year*12+period_month)<=-fin_amort_meth_month) then
        fin_amort = fin_amort + (fin_value * fin_amort_rate * fin_amort_crate)/1200;
      ftype = 0;
      if (fin_amort>fin_value-fin_redemption) then
        fin_amort = fin_value-fin_redemption;
    end else
    if (fin_amort_meth_type=1) then
    -- DEGRESYWNA
    begin
      if ((using_date_year*12+using_date_month)-(period_year*12+period_month)<=-fin_amort_meth_month) then
      begin
        if (((fin_value - acc_fin_redemption) * fin_amort_rate * fin_amort_crate) > (fin_value * fin_amort_rate)) then
        begin
          fin_amort = fin_amort + ((fin_value - acc_fin_redemption) * fin_amort_rate * fin_amort_crate)/1200;
          ftype = 1;
        end else
        begin
          fin_amort = fin_amort + (fin_value * fin_amort_rate)/1200;
          ftype = 0;
        end
      end
      if (fin_amort>fin_value-fin_redemption) then
        fin_amort = fin_value-fin_redemption;
    end else
    if (fin_amort_meth_type=2) then
    -- JEDNORAZOWA
    begin
--      if ((using_date_year*12+using_date_month)-(period_year*12+period_month)+1=fin_amort_meth_month) then
      if ((using_date_year*12+using_date_month)-(period_year*12+period_month)<=-fin_amort_meth_month) then
        fin_amort = fin_value-fin_redemption;
      ftype = 2;
    end
  END

  -- sprawdzam czy zostalo cos do umorzenia
  if (tax_amort>tax_value-tax_redemption) then tax_amort = tax_value-tax_redemption;
  if (fin_amort>fin_value-fin_redemption) then fin_amort = fin_value-fin_redemption;

  tarate = tax_amort_rate;
  farate = fin_amort_rate;
  suspend;
end^
SET TERM ; ^
