--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_ST_AM_PLANP(
      SYEAR integer,
      FYEAR integer,
      FX integer,
      TYP integer,
      COMPANY integer)
  returns (
      TMONTH integer,
      LYEAR integer,
      YAMOUNT numeric(10,2),
      TAMOUNT numeric(10,2),
      TAMOUNT1 numeric(10,2),
      TAMOUNT2 numeric(10,2),
      TAMOUNT3 numeric(10,2),
      TAMOUNT4 numeric(10,2),
      TAMOUNT5 numeric(10,2),
      TAMOUNT6 numeric(10,2),
      TAMOUNT7 numeric(10,2),
      TAMOUNT8 numeric(10,2),
      TAMOUNT9 numeric(10,2),
      TAMOUNT10 numeric(10,2),
      TAMOUNT11 numeric(10,2),
      TAMOUNT12 numeric(10,2))
   as
declare variable PYEAR integer;
    declare variable wys integer;
-- nowe
    declare variable N_AMPERIOD INTEGER;
    declare variable N_TAX_AMORT NUMERIC(15,2);
    declare variable N_FIN_AMORT NUMERIC(15,2);
    declare variable N_acc_tax_amort NUMERIC(15,2);
    declare variable N_acc_fin_amort NUMERIC(15,2);
    declare variable N_sum_tax_amort NUMERIC(15,2);
    declare variable N_sum_fin_amort NUMERIC(15,2);
-- koniec nowe
    declare variable varamperiods integer;

begin
    TAMOUNT1 = 0;
    TAMOUNT2 = 0;
    TAMOUNT3 = 0;
    TAMOUNT4 = 0;
    TAMOUNT5 = 0;
    TAMOUNT6 = 0;
    TAMOUNT7 = 0;
    TAMOUNT8 = 0;
    TAMOUNT9 = 0;
    TAMOUNT10 = 0;
    TAMOUNT11 = 0;
    TAMOUNT12 = 0;
    LYEAR = 0;
    wys = 0;
    N_sum_tax_amort = 0;
    N_sum_fin_amort = 0;
    if (TYP = 0) then BEGIN
--      FOR
--        SELECT C.tax_amort, C.ammonth, C.amyear
--          FROM calculate_amortplan(:FX, :company) C
--          WHERE C.amyear >= :SYEAR
--          AND C.amyear <= :FYEAR
--          into :TAMOUNT, :TMONTH, :PYEAR
--      DO BEGIN

      for
        select ref, amyear, ammonth
          from amperiods
          where company = :company and amyear <= :FYEAR and amyear >= :SYEAR
            order by amyear, ammonth
        into :N_AMPERIOD, PYEAR, TMONTH
      do begin
        if(:PYEAR = :syear and tmonth = 1)  then
        begin
          select am.ref from amperiods am
            where am.amyear = :syear - 1 and am.ammonth = 12 and am.company = :company
          into varamperiods;

          select coalesce(TAX_AMORT,0) from calculate_acc_amortization(:FX, :varamperiods)
          into :N_sum_tax_amort;
        end

        select coalesce(TAX_AMORT,0) from calculate_acc_amortization(:FX, :n_amperiod)
          into :N_acc_tax_amort;
        TAMOUNT = N_acc_tax_amort - N_sum_tax_amort;
        N_sum_tax_amort = N_acc_tax_amort;

        if (:LYEAR = 0) then
          LYEAR = PYEAR;
        if (WYS = 1) then
          WYS = 0;
        if (LYEAR <> PYEAR) THEN BEGIN
          YAMOUNT = TAMOUNT1 + TAMOUNT2 + TAMOUNT3 + TAMOUNT4 + TAMOUNT5 + TAMOUNT6
          + TAMOUNT7 + TAMOUNT8 + TAMOUNT9 + TAMOUNT10 + TAMOUNT11 + TAMOUNT12;
          if (LYEAR is null) then
            LYEAR = SYEAR;
          suspend;
          wys = 1;
          LYEAR = PYEAR;
          TAMOUNT1 = 0;
          TAMOUNT2 = 0;
          TAMOUNT3 = 0;
          TAMOUNT4 = 0;
          TAMOUNT5 = 0;
          TAMOUNT6 = 0;
          TAMOUNT7 = 0;
          TAMOUNT8 = 0;
          TAMOUNT9 = 0;
          TAMOUNT10 = 0;
          TAMOUNT11 = 0;
          TAMOUNT12 = 0;
        END
        if (TMONTH = 1) then
          TAMOUNT1 = TAMOUNT;
        else if (TMONTH = 2) then
          TAMOUNT2 = TAMOUNT;
        else if (TMONTH = 3) then
          TAMOUNT3 = TAMOUNT;
        else if (TMONTH = 4) then
          TAMOUNT4 = TAMOUNT;
        else if (TMONTH = 5) then
          TAMOUNT5 = TAMOUNT;
        else if (TMONTH = 6) then
          TAMOUNT6 = TAMOUNT;
        else if (TMONTH = 7) then
          TAMOUNT7 = TAMOUNT;
        else if (TMONTH = 8) then
          TAMOUNT8 = TAMOUNT;
        else if (TMONTH = 9) then
          TAMOUNT9 = TAMOUNT;
        else if (TMONTH = 10) then
          TAMOUNT10 = TAMOUNT;
        else if (TMONTH = 11) then
          TAMOUNT11 = TAMOUNT;
        else if (TMONTH = 12) then
          TAMOUNT12 = TAMOUNT;
      END
      LYEAR = PYEAR;
      if (wys = 0) then BEGIN
        YAMOUNT = TAMOUNT1 + TAMOUNT2 + TAMOUNT3 + TAMOUNT4 + TAMOUNT5 + TAMOUNT6
          + TAMOUNT7 + TAMOUNT8 + TAMOUNT9 + TAMOUNT10 + TAMOUNT11 + TAMOUNT12;
        if (LYEAR is null) then
          LYEAR = SYEAR;
        suspend;
    END
    end
    else if (TYP = 1) then BEGIN
      for
        select ref, amyear, ammonth
          from amperiods
          where company = :company and amyear <= :FYEAR and amyear >= :SYEAR
            order by amyear, ammonth
        into :N_AMPERIOD, PYEAR, TMONTH
      do begin
        if(:PYEAR = :syear and tmonth = 1)  then
        begin
          select am.ref from amperiods am
            where am.amyear = :syear - 1 and am.ammonth = 12 and am.company = :company
          into varamperiods;

          select coalesce(FIN_AMORT,0) from calculate_acc_amortization(:FX, :varamperiods)
          into :n_sum_fin_amort;
        end

        select coalesce(FIN_AMORT,0) from calculate_acc_amortization(:FX, :n_amperiod)
          into :N_acc_FIN_amort;
        TAMOUNT = N_acc_FIN_amort - N_sum_FIN_amort;
        N_sum_FIN_amort = N_acc_FIN_amort;

        if (:LYEAR = 0) then
          LYEAR = PYEAR;
        if (WYS =1) then
          WYS = 0;
        if (LYEAR <> PYEAR) THEN BEGIN
          YAMOUNT = TAMOUNT1 + TAMOUNT2 + TAMOUNT3 + TAMOUNT4 + TAMOUNT5 + TAMOUNT6
          + TAMOUNT7 + TAMOUNT8 + TAMOUNT9 + TAMOUNT10 + TAMOUNT11 + TAMOUNT12;
          if (LYEAR is null) then
            LYEAR = SYEAR;
          suspend;
          wys = 1;
          LYEAR = PYEAR;
          TAMOUNT1 = 0;
          TAMOUNT2 = 0;
          TAMOUNT3 = 0;
          TAMOUNT4 = 0;
          TAMOUNT5 = 0;
          TAMOUNT6 = 0;
          TAMOUNT7 = 0;
          TAMOUNT8 = 0;
          TAMOUNT9 = 0;
          TAMOUNT10 = 0;
          TAMOUNT11 = 0;
          TAMOUNT12 = 0;
        END
        if (TMONTH = 1) then
          TAMOUNT1 = TAMOUNT;
        else if (TMONTH = 2) then
          TAMOUNT2 = TAMOUNT;
        else if (TMONTH = 3) then
          TAMOUNT3 = TAMOUNT;
        else if (TMONTH = 4) then
          TAMOUNT4 = TAMOUNT;
        else if (TMONTH = 5) then
          TAMOUNT5 = TAMOUNT;
        else if (TMONTH = 6) then
          TAMOUNT6 = TAMOUNT;
        else if (TMONTH = 7) then
          TAMOUNT7 = TAMOUNT;
        else if (TMONTH = 8) then
          TAMOUNT8 = TAMOUNT;
        else if (TMONTH = 9) then
          TAMOUNT9 = TAMOUNT;
        else if (TMONTH = 10) then
          TAMOUNT10 = TAMOUNT;
        else if (TMONTH = 11) then
          TAMOUNT11 = TAMOUNT;
        else if (TMONTH = 12) then
          TAMOUNT12 = TAMOUNT;
      END
    LYEAR = PYEAR;
    if (wys = 0) then BEGIN
      YAMOUNT = TAMOUNT1 + TAMOUNT2 + TAMOUNT3 + TAMOUNT4 + TAMOUNT5 + TAMOUNT6
        + TAMOUNT7 + TAMOUNT8 + TAMOUNT9 + TAMOUNT10 + TAMOUNT11 + TAMOUNT12;
        if (LYEAR is null) then
          LYEAR = SYEAR;
      suspend;
    END
  end
end^
SET TERM ; ^
