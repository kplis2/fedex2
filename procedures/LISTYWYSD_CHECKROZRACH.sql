--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_CHECKROZRACH(
      DOKUMSPED integer)
  returns (
      SALDOWNMA numeric(14,2))
   as
declare variable klient integer;
declare variable slodefref integer;
begin
  saldownma = 0;
  select ODBIORCY.klient from LISTYWYSD left join ODBIORCY on (LISTYWYSD.odbiorcaid = ODBIORCY.ref)
  where LISTYWYSD.REF=:dokumsped
  into :klient;
  if(:klient > 0) then begin
    select min(ref) from slodef where slodef.typ = 'KLIENCI' into :slodefref;
    select sum(rozrach.saldowm)
    from ROZRACH left join NAGFAK on (NAGFAK.REF = rozrach.faktura)
    where ROZRACH.SLODEF = :slodefref and ROZRACH.SLOPOZ = :klient
      and (nagfak.listywysd <> :dokumsped or (nagfak.listywysd is null))
    into :saldownma;
  end
  if(:saldownma is null) then saldownma = 0;
end^
SET TERM ; ^
