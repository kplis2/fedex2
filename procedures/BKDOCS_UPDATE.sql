--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BKDOCS_UPDATE(
      BKDOC integer)
   as
declare variable sumcredit numeric(14,2);
declare variable currency numeric(14,2);
declare variable sumdebit numeric(14,2);
declare variable balcredit numeric(14,2);
declare variable baldebit numeric(14,2);
begin
  select sum(D.CREDIT), sum(D.DEBIT)
    from DECREES D
    join bkaccounts B on (B.ref=D.accref)
    where D.BKDOC=:bkdoc and B.bktype<2
    into :sumcredit, :sumdebit;

  if (sumcredit is null) then
    sumcredit = 0;
  if (sumdebit is null) then
    sumdebit = 0;

  balcredit = sumcredit - sumdebit;
  baldebit = sumdebit - sumcredit;

  if (balcredit < 0) then
    balcredit = 0;
  if (baldebit < 0) then
    baldebit = 0;

  update BKDOCS
    set SUMCREDIT = :sumcredit, SUMDEBIT = :sumdebit, BALCREDIT = :balcredit, BALDEBIT = :baldebit
    where REF = :bkdoc;
  suspend;
end^
SET TERM ; ^
