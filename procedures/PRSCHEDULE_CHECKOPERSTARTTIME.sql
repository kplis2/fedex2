--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDULE_CHECKOPERSTARTTIME(
      PRGANTTREF integer,
      PSTARTTIME timestamp)
  returns (
      STARTTIME timestamp)
   as
declare variable minspace integer;
declare variable "ROUND" integer;
declare variable minlength double precision;
declare variable roundtime double precision;
declare variable daytime double precision;
declare variable startdate date;
declare variable daytimepers integer;
declare variable dpndtime timestamp;
declare variable schedoper integer;
begin
  --sprawdzenie czasu zaknczenia ostatniej oepracji poprzedzajacej
  select max(d.timeto)
    from prgantt g
      left join prgantt d on (d.prschedoper = g.depfrom)
    where g.ref = :prganttref
  into :dpndtime;
  starttime = :pstarttime;
  if(:starttime < :dpndtime and dpndtime is not null) then
    starttime = :dpndtime;
  select s.mintimebetweenopers, s.roundstarttime
    from prgantt g
      left join prschedules s on (s.ref = g.prschedule)
    where g.ref = :prganttref
    into :minspace, :round;
  minlength = 1440;
  minlength = 1/:minlength;
  if(:starttime < (:dpndtime + (minspace * minlength)))then
    starttime = :dpndtime + (minspace * minlength);
  if(:round > 0) then begin
    roundtime = :minlength * :round;
    startdate = cast(:starttime as date);-- sama data, czyli godzina 00:00
    daytime = :starttime - cast(:startdate as timestamp);--czas w danym dniu liczony nie w sekundach, ale w dniach
    daytimepers = :daytime /:roundtime;--ilosc okresow wyrownania w ciagu dnia
    if(:daytimepers < :daytime/:roundtime) then--zaokraglenei w gore
      daytimepers = :daytimepers + 1;
    starttime = cast(startdate as timestamp) + (:daytimepers * :roundtime);
    if(:starttime < :pstarttime) then
      starttime = :pstarttime;--przesuniecie na skutek zaokrąglen
  end
end^
SET TERM ; ^
