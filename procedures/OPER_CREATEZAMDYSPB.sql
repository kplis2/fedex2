--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_CREATEZAMDYSPB(
      ZAM integer,
      HISTZAM integer,
      DEFOPER varchar(5) CHARACTER SET UTF8                           ,
      MAGAZYN varchar(5) CHARACTER SET UTF8                           ,
      NDATAMAG date)
  returns (
      DOSTZAM integer,
      SYMBOLZAM varchar(20) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE CENANET NUMERIC(14,4);
DECLARE VARIABLE JEDN INTEGER;
DECLARE VARIABLE KTM VARCHAR(30);
DECLARE VARIABLE PRZELICZ NUMERIC(14,4);
DECLARE VARIABLE POZZAMDREF INTEGER;
declare variable datamag timestamp;
declare variable dbnewtyp char(3);
declare variable dbnewrej char(3);
begin
  symbolzam = '';
  select DEFOPERZAM.dbnewtyp, DEFOPERZAM.dbnewrej
  from DEFOPERZAM where SYMBOL = :defoper
  into :dbnewtyp,:dbnewrej;
  if(coalesce(:dbnewtyp,'')='' or coalesce(:dbnewrej,'')='') then exit;
  if(:ndatamag is null) then
    datamag = current_timestamp(0);

  /*zalozenie zamówienia*/
  execute procedure GEN_REF('NAGZAM') returning_values :dostzam;
  insert into NAGZAM(REF, TYPZAM, REJESTR, MAGAZYN, MAG2, STAN, TYP, REFZRODL, symbzrodl,
        TERMDOST, OPERATOR, UWAGI, DOSTAWCA, KLIENT,
        ODBIORCA,ODBIORCAID,DULICA,DNRDOMU,DNRLOKALU,DMIASTO,DKODP,DATAWE,PRDEPART,BN,
        uzykli, platnik, sposdost, sposdostauto, trasadost, wysylka, wysylkadod
        , uwagiwewn, uwagisped, kosztdostbez, kosztdost, kosztdostroz, kosztdostplat,
        flagi, flagisped, KURS, WALUTA, tabkurs, zlecnazwa, znakzewn)
  select :dostzam, :dbnewtyp, :dbnewrej, :magazyn, MAGAZYN, 'N', 3, NAGZAM.REF, NAGZAM.id,
        TERMDOST, OPERATOR, UWAGI, DOSTAWCA, KLIENT,
        ODBIORCA,ODBIORCAID,DULICA,DNRDOMU,DNRLOKALU,DMIASTO,DKODP, :datamag ,PRDEPART, BN, uzykli, platnik,
        sposdost, sposdostauto, trasadost, nagzam.wysylka, wysylkadod
        , uwagiwewn, uwagisped,kosztdostbez, kosztdost, kosztdostroz, kosztdostplat,
        flagi, flagisped,KURS, WALUTA, tabkurs, zlecnazwa, znakzewn
  from NAGZAM
  where NAGZAM.REF = :zam;
  /*zaloznie pozycji zamówienia*/
  for select POZZAMDYSP.REF, POZZAM.JEDN, POZZAM.KTM
  from POZZAM join POZZAMDYSP on (POZZAMDYSP.pozzam = POZZAM.REF)
  where POZZAM.zamowienie = :zam and POZZAMDYSP.magazyn = :magazyn
  order by POZZAM.NUMER
  into :pozzamdref, :jedn, :ktm
  do begin
    insert into POZZAM(ZAMOWIENIE, KTM, WERSJAREF, MAGAZYN, mag2, JEDNO, JEDN,
      ILOSCO, ILOSC, ILOSCM, GENPOZREF,
      ILREALO, ILREAL, ILREALM,
      ILZREAL, ILZREALM,
      cenacen, CENANET, cenabru, rabat, opk, walcen, prec)
    select :dostzam,  POZZAM.KTM, POZZAM.wersjaref, :magazyn, POZZAM.magazyn, POZZAM.jedno, POZZAM.JEDN,
      POZzamdysp.ilosco, pozzamdysp.ilosc, pozzamdysp.iloscm, POZZAM.REF,
      POZzamdysp.ilosco, pozzamdysp.ilosc, pozzamdysp.iloscm,
      pozzamdysp.ilosc, pozzamdysp.iloscm,
      POZZAM.CENACEN, POZZAM.CENANET, POZZAM.CENABRU, pozzam.rabat, pozzam.opk, pozzam.walcen, POZZAM.prec
    from POZZAMDYSP join POZZAM on (POZZAMDYSP.pozzam = POZZAM.REF)
      where POZZAMDYSP.REF = :pozzamdref;
  end
end^
SET TERM ; ^
