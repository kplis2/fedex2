--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_INPOST_GETSTICKER_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable userlogin string40;
declare variable userpassword string40;
declare variable shippingdoc integer;
declare variable deliverytype integer;
declare variable rootparent integer;
declare variable package_node integer_id;
declare variable packref integer;
declare variable packcode string40;
declare variable filepath string255;
begin
  select l.ref, l.sposdost
    from listywysd l
    where l.ref = :oref
  into :shippingdoc, :deliverytype;

  --sprawdzanie czy istieje dokument spedycyjny
  if(shippingdoc is null) then exception ede_ups_listywysd_brak;

  select first 1 k.login, k.haslo, k.sciezkapliku
    from spedytwys w
      left join spedytkonta k on (k.spedytor = w.spedytor)
    where w.sposdost = :deliverytype
  into :userlogin, :userpassword, :filepath;

  val = null;
  parent = null;
  name = 'GetSticker';
  params = null;
  id = 0;
  suspend;

  rootparent = :id;

  val = :userlogin;
  parent = :rootparent;
  name = 'email';
  params = null;
  id = id + 1;
  suspend;

  val = :userpassword;
  parent = :rootparent;
  name = 'password';
  params = null;
  id = id + 1;
  suspend;

  val = 'TEST'; --jesli chcesz testowac zmien na TEST inaczej PRODUCTION
  name = 'ServiceUrlType';
  id = id + 1;
  parent = :rootparent;
  suspend;

  --uzywane w przypadku PDF-ow
  val = :filepath;
  parent = :rootparent;
  name = 'path';
  params = null;
  id = id + 1;
  suspend;

  for
    select o.ref, substring(o.symbolsped from 1 for 40)
      from listywysdroz_opk o
      where o.listwysd = :shippingdoc
        and o.rodzic is null
    into :packref, :packcode
  do begin
    val = null;
    parent = :rootparent;
    name = 'package';
    params = null;
    id = id + 1;
    suspend;

    package_node = id;

    --val = 'Epl2';
    val = 'Pdf';
    parent = :package_node;
    name = 'labelFormat';
    params = null;
    id = id + 1;
    suspend;

    val = 'A6P';
    parent = :package_node;
    name = 'labelType';
    params = null;
    id = id + 1;
    suspend;

    val = :packref;
    parent = :package_node;
    name = 'packref';
    params = null;
    id = id + 1;
    suspend;

    val = :packcode;
    parent = :package_node;
    name = 'packcode';
    params = null;
    id = id + 1;
    suspend;
  end
end^
SET TERM ; ^
