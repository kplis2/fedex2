--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_PFCW(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable fromdate date;
  declare variable todate date;
  declare variable person integer;
  declare variable icode varchar(20);
begin
  --DU: personel - wylicza podstawe ubezpieczenia chorobowego i wypadkowego
  execute procedure efunc_prdates(payroll, 2)
    returning_values fromdate, todate;

  select person from employees where ref = :employee
    into :person;

  select c.code
    from ezusdata z
      join edictzuscodes c on (z.ascode = c.ref)
    where z.person = :person
      and z.fromdate = (select max(fromdate) from ezusdata
        where person = :person and fromdate <= :todate)
    into :icode;

  if (substring(icode from 1 for 2) <> '11') then
  begin
    execute procedure ef_fcolsum(employee, payroll, 'EF_', 'ZUS')
      returning_values ret;
  end
  suspend;
end^
SET TERM ; ^
