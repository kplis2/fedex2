--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWIAD_SET(
      ID STRING255,
      MAILBOX integer,
      TYTUL STRING2048_UTF8,
      FULLHTMLBODY BLOB_UTF8)
   as
begin
  update emailwiad set tytul=:tytul, fullhtmlbody = :fullhtmlbody
  where id=:id and mailbox=:mailbox;
end^
SET TERM ; ^
