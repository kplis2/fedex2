--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_PIT8C_V7_10E_EXP(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable ISDECLARATION integer;
declare variable DEF varchar(20);
declare variable CODE varchar(10);
declare variable SYSTEMCODE varchar(20);
declare variable OBLIGATIONKIND varchar(20);
declare variable SCHEMAVER varchar(10);
declare variable SYMBOL varchar(10);
declare variable VARIANT varchar(10);
declare variable PVALUE varchar(255);
declare variable TMPVAL1 varchar(255);
declare variable TMPVAL2 varchar(255);
declare variable TMP smallint;
declare variable CURR_P integer;
declare variable CORRECTION smallint;
declare variable ENCLOSURE integer;
declare variable ZALTYP integer;
declare variable ISZAL smallint;
declare variable PARENT_LVL_0 integer;
declare variable PARENT_LVL_1 integer;
declare variable PARENT_LVL_2 integer;
declare variable PARENT_LVL_3 integer;
declare variable FIELD integer;
begin
  -- Sprawdzamy czy dana deklaracja istnieje do eksportu
  select first 1 1
    from edeclarations
    where ref = :oref
    into :isDeclaration;

  if (isDeclaration is null) then
    exception universal 'Niepoprawny identyfikator e-deklaracji';

  -- Pobieranie definicji
  select e.edecldef, ed.code, ed.systemcode, ed.obligationkind, ed.schemaver, ed.symbol, ed.variant
    from edeclarations e join edecldefs ed on (ed.ref = e.edecldef)
    where e.ref = :oref
    into :def, :code, :systemcode, :obligationkind, :schemaver, :symbol, :variant;

  -- Generujemy naglowek pliku XML
  id = 0;
  name = 'Deklaracja';
  parent = null;
  params = 'xmlns="http://crd.gov.pl/wzor/2014/12/05/1880/" xmlns:etd="http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2011/06/21/eD/DefinicjeTypy/" xmlns:zzu="http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2011/10/07/eD/ORDZU/"';
  val = null;
  suspend;

  correction = 0;
  parent_lvl_0 = 0;
  -- Rozpoczynamy naglowek e-deklaracji
  id = id + 1;
  name = 'Naglowek';
  parent = parent_lvl_0;
  params = null;
  val = null;
  parent_lvl_1 = :id;
  suspend;
  ----------------------------
    id = id + 1;
    name = 'KodFormularza';
    parent = parent_lvl_1;
    params = '';
    if(code is not null) then
      params = params || 'kodPodatku="' || code || '" ';
    if(systemcode is not null) then
      params = params || 'kodSystemowy="' || systemcode || '" ';
    if(obligationkind is not null) then
      params = params || 'rodzajZobowiazania="' || obligationkind || '" ';
    if(schemaver is not null) then
      params = params || 'wersjaSchemy="' || schemaver || '"';
    val = symbol;
    suspend;
    
    -- Wariant formularza
    id = id + 1;
    name = 'WariantFormularza';
    parent = parent_lvl_1;
    params = null;
    val = variant;
    suspend;
    --  A  ---------------------------------->
    -- Miejsce i cel skladania formularza

    val = null;
    select field, pvalue
      from edeclpos
      where edeclaration = :oref
        and field = 6
        and pvalue is not null
      order by field
      into :field, :val;
    if (field = 6) then
    begin
      id = id + 1;
      params = 'poz="P_6"';
      name = 'CelZlozenia';
      suspend;
    end
    correction = val;
    params = null;

    for
      select field, pvalue
        from edeclpos
        where edeclaration = :oref
          and field in (4,5)
          and pvalue is not null
        order by field
        into :field, :val
    do begin
     id = id + 1;
     if (field = 4) then name = 'Rok';
     else if (field = 5) then name = 'KodUrzedu';
     suspend;
    end

  id = id + 1;
  name = 'Podmiot1';
  parent = parent_lvl_0;
  params = 'rola="Składający"';
  val = null;
  parent_lvl_1 = :id;
  suspend;
    --  B  -------------------------------------------------------->
    -- Dane identyfikacyjne skladajacego
    select pvalue
      from edeclpos
      where edeclaration = :oref
        and field = 7
        and pvalue is not null
      into :pvalue;

    id = id + 1;
    if (pvalue = 1) then name = 'etd:OsobaNiefizyczna';
    else if (pvalue = 2) then name = 'etd:OsobaFizyczna';
    params = null;
    parent = parent_lvl_1;
    parent_lvl_2 = :id;
    suspend;
    --  B  -------------------------------------------------------->
      val = null;
      params = null;
      parent = parent_lvl_2;
      for select field, pvalue
        from edeclpos
        where edeclaration = :oref
          and field in (1,8,9)
          and pvalue is not null
        order by field
        into :field, :val
      do begin
        if (field = 1) then
        begin
          name = 'etd:NIP';
          id = id + 1;
          suspend;
        end else
        if (field = 8) then
        begin
          for select upper(stringout), lp
            from parsestring(:val, ', ')
            order by lp
            into :tmpval2, tmp
          do begin
            id = id + 1;
            val = tmpval2;
            if (tmp = 1) then name = 'etd:PelnaNazwa';
            else if (tmp = 2) then name = 'etd:REGON';
            suspend;
          end
        end
        else if (field = 9) then
        begin
          for select upper(stringout), lp
            from parsestring(:val, ', ')
            order by lp
            into :tmpval2, tmp
          do begin
            if (tmp = 1) then tmpval1 = :tmpval2; --name = 'etd:Nazwisko';
            else if (tmp = 2) then
            begin
              id = id + 1;
              name = 'etd:ImiePierwsze';
              val = tmpval2;             
              suspend;
              id = id + 1;
              name = 'etd:Nazwisko';
              val = tmpval1;
              suspend;
            end
            else if (tmp = 3) then
            begin
              id = id + 1;
              val = tmpval2;
              name = 'etd:DataUrodzenia';
              suspend;
            end
          end
        end
      end

  --  C  ---------------------------------------->
  id = id + 1;
  name = 'Podmiot2';
  parent = parent_lvl_0;
  params = 'rola="Podatnik"';
  val = null;
  parent_lvl_1 = :id;
  suspend;
  -- Dane podatnika
    id = id + 1;
    name = 'etd:OsobaFizyczna';
    parent = parent_lvl_1;
    params = null;
    val = null;
    parent_lvl_2 = :id;
    suspend;
    --  C1 Dane identyfikacyjne -------------------------
      parent = parent_lvl_2;
      for select field, trim(upper(pvalue))
          from edeclpos e
          where edeclaration = :oref
            and pvalue is not null
            and field in (10,12,13)
          order by field
          into :field, :val
      do begin
        id = id + 1;
        if (field = 10) then
        begin
          if (char_length(:val) <> 11) then name = 'etd:NIP';
          else name = 'etd:PESEL';
        end else
        if (field = 12) then  -- bramka wymaga najpierw podania imienia, nastepnie nazwiska
        begin
          name = 'etd:ImiePierwsze';
          suspend;
          id = id + 1;
          name = 'etd:Nazwisko';
          select trim(upper(pvalue))
            from edeclpos
            where edeclaration = :oref
              and pvalue is not null
              and field = 11
            into :val;
        end else
        if (field = 13) then
          name = 'etd:DataUrodzenia';
        suspend;
      end
    --  C2 Adres zamieszkania -------------------------
    id = id + 1;
    name = 'etd:AdresZamieszkania';
    parent = parent_lvl_1;
    params = 'rodzajAdresu="RAD"';
    val = null;
    parent_lvl_2 = :id;
    suspend;
    ------------------------------------------->
      id = id+1;
      name = 'etd:AdresPol';
      parent = parent_lvl_2;
      params = null;
      --val = null;  jest juz nullem
      parent_lvl_3 = :id;
      suspend;
      --------------------------------------------->
        parent = parent_lvl_3;
        for select field, trim(upper(pvalue))
            from edeclpos
            where edeclaration = :oref
              and pvalue is not null
              and field in (14,15,16,17,18,19,20,21,22,23)
            order by field
            into :field, :val
        do begin
          id = id + 1;
          if (field = 14) then name = 'etd:KodKraju';
          else if (field = 15) then name = 'etd:Wojewodztwo';
          else if (field = 16) then name = 'etd:Powiat';
          else if (field = 17) then name = 'etd:Gmina';
          else if (field = 18) then name = 'etd:Ulica';
          else if (field = 19) then name = 'etd:NrDomu';
          else if (field = 20) then name = 'etd:NrLokalu';
          else if (field = 21) then name = 'etd:Miejscowosc';
          else if (field = 22) then name = 'etd:KodPocztowy';
          else if (field = 23) then name = 'etd:Poczta';
          suspend;
        end

    id = id + 1;
    name = 'PozycjeSzczegolowe';
    parent = parent_lvl_0;
    params = null;
    val = null;
    parent_lvl_1 = :id;
    suspend;
      --  D,E,F,G,H  ------------------------------->
      parent = parent_lvl_1;
        for select field, upper(pvalue)
          from edeclpos
          where edeclaration = :oref
            and pvalue is not null
            and field >= 24
            and field <= 49
          order by field
          into :field, :val
        do begin
          id = id + 1;
          name = 'P_'||field;
          suspend;
        end

    id = id + 1;
    name = 'Pouczenie1';
    parent = parent_lvl_0;
    val = 'Za uchybienie obowiązkom płatnika, a także za złożenie informacji nieprawdziwej grozi odpowiedzialność przewidziana w Kodeksie karnym skarbowym.';
    suspend;

    id = id + 1;
    name = 'Pouczenie2';
    parent = parent_lvl_0;
    val = 'Informacji nie sporządzają płatnicy stypendiów, którym podatnik (stypendysta) w terminie do dnia 10 stycznia roku następującego po roku podatkowym złożył oświadczenie, o którym mowa w art. 37 ust. 1 ustawy.';
    suspend;

    if (correction = 2) then --=================================================
    begin
      select ref from edeclarations
        where enclosureowner = :oref
        into :enclosure;

      if (oref is not null) then
      begin
        select edecldef from edeclarations
          where enclosureowner = :oref
          into :zaltyp;

        if(zaltyp is not null) then
    begin
          --Zalacznik_ORD-ZU
            id = id + 1;
            name = 'Zalaczniki';
            parent = parent_lvl_0;
            params = '';
            val = null;
            parent_lvl_1 = :id;
            suspend;
            id = id + 1;
            name = 'zzu:Zalacznik_ORD-ZU';
            parent = parent_lvl_1;
            params = '';
            val = null;
            parent_lvl_2 = :id;
            suspend;

              --Pobieranie definicji do ORD_ZU
              select code, systemcode, schemaver, symbol, variant
                from edecldefs
                where ref = :zaltyp
                into :code, :systemcode, :schemaver, :symbol, :variant;

              --Rozpoczynamy naglowek e-deklaracji ORD_ZU
            id = id + 1;
            name = 'zzu:Naglowek';
            parent = parent_lvl_2;
            params = null;
            val = null;
            parent_lvl_3 = :id;
            suspend;
            --cialo naglowka
            
               --KodFormularza
                id = id + 1;
                name = 'zzu:KodFormularza';
                parent = parent_lvl_3;
                params = '';
                if(systemcode is not null) then
                  params = params || 'kodSystemowy = "' || systemcode || '" ';
                if(schemaver is not null) then
                  params = params || 'wersjaSchemy = "' || schemaver || '"';
                val = symbol;
                suspend;
                
                --WariantFormularza
                id = id + 1;
                name = 'zzu:WariantFormularza';
                parent = parent_lvl_3;
                params = null;
                val = variant;
                suspend;

              --PozycjeSzczegolowe
              id = id + 1;
              name = 'zzu:PozycjeSzczegolowe';
              parent = parent_lvl_2;
              params = null;
              val  = null;
              parent_lvl_3 = id;
              suspend;

              --Pozycje od first do last -  35 - 82
              curr_p = 13;
              for
                select pvalue from edeclpos
                  where edeclaration = :enclosure
                    and field > 12
                    and field < 14
                  into :pvalue
              do begin
                if (pvalue is not null) then
                begin
                  id = id + 1;
                  name = 'zzu:P_'||curr_p;
                  parent = parent_lvl_3;
                  params = null;
                  val = upper(pvalue);
                  suspend;
                end
                curr_p = curr_p +1;
              end
          end
        end 
      end
end^
SET TERM ; ^
