--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_LISTYWYSD_POZ_2(
      MWSORD MWSORDS_ID,
      LASTMWSACT MWSACTS_ID)
  returns (
      MWSACT MWSACTS_ID,
      NUMER INTEGER_ID,
      KTM KTM_ID,
      NRWRSJI NRWERSJI_ID,
      WERSJAREF WERSJE_ID,
      NAZWA TOWARY_NAZWA,
      ILOSC ILOSCI_MAG,
      ILOSCSPK ILOSCI_MAG,
      SPAKOWANO SMALLINT_ID,
      LISTWYSD LISTYWYSD_ID,
      LISTWYSDPOZ LISTYWYSDPOZ_ID,
      AKCEPT SMALLINT_ID,
      KOLOR SMALLINT_ID)
   as
declare variable multi smallint_id;
begin
  --[PM] XXX wyswietlenie listy pozycji na pakowaniu dla pojedynek
  -- #POJEDYNKI
  -- kolor: 0 - bialy, 1 - zielony, 2 - zolty, 3 - czerwony

  if (lastmwsact = 0) then
    lastmwsact = null;

  exception universal 'test break msword: ' || coalesce(:MWSORD,'<pusty>') || ' lastmwsact: ' || coalesce(:LASTMWSACT,'<pusty>');

  select multi from mwsords where ref = :mwsord into :multi;
  --if (coalesce(multi,0) <> 2) then
    --exception universal 'Co Ty robisz?'; --tu maja byc tylko pojedynki

  for select ma.ref, ma.number, w.ktm, w.nrwersji, ma.vers, w.nazwat,
          ma.quantityc, coalesce(lp.iloscspk,0), min(lro.x_spakowano),
          ld.ref, lp.ref, ld.akcept
        from mwsacts ma left join wersje w on ma.vers = w.ref
          left join listywysdpoz lp on ma.docid = lp.dokpoz and ma.doctype = lp.doktyp
          left join listywysd ld on lp.dokument = ld.ref
          left join listywysdroz_opk lro on lro.listwysd = ld.ref
        where ma.mwsord = :mwsord and ma.status = 5
        group by 1,2,3,4,5,6,7,8,10,11,12
        order by 2,1
        into :mwsact, :numer, :ktm, :nrwrsji, :wersjaref, :nazwa,
          :ilosc, :iloscspk, :spakowano,
          :listwysd, :listwysdpoz, :akcept
     do begin
       if (mwsact is not distinct from lastmwsact) then
         kolor = 2;
       else if (akcept = 2) then
         kolor = 1;
       else if (akcept is not null and akcept in (0,1)) then
         kolor = 3;
       else
         kolor = 0;
       suspend;
     end
end^
SET TERM ; ^
