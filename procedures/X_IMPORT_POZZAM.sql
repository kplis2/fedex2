--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_IMPORT_POZZAM(
      KEY0 varchar(255) CHARACTER SET UTF8                           ,
      KEY1 varchar(255) CHARACTER SET UTF8                           ,
      KEY2 varchar(255) CHARACTER SET UTF8                           ,
      KEY3 varchar(255) CHARACTER SET UTF8                           ,
      KEY4 varchar(255) CHARACTER SET UTF8                           ,
      COL0 varchar(255) CHARACTER SET UTF8                           ,
      COL1 varchar(255) CHARACTER SET UTF8                           )
  returns (
      STATUS integer,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable c0 numeric(14,4);
declare variable c1 numeric(14,4);
declare variable zam integer;
declare variable poz integer;
begin
  -- przyklad importu z excela
  -- key0 POZZAM.ZAMOWIENIE
  -- key1 POZZAM.REF
  -- key2 POZZAM.MAGAZYN
  -- key3 POZZAM.KTM
  -- key4 POZZAM.WERSJA

  -- sprawdz czy wiersz nie jest pusty, jesli tak to go omijamy ale nie zglaszamy bledu
  if(:key0 = '' or :key2 = '' or :key3 = '') then begin
    status = 1;
    suspend;
  end

  -- inicjuj niewypelnione pola i konwertuj dane numeryczne
  if(:key0='') then key0 = 0;
  zam = cast(:key0 as integer);
  if(:key1='') then key1 = 0;
  poz = cast(:key1 as integer);
  if(:key4='') then key4 = 0;
  if(:col0='' or :col0 is null) then col0 = '0';
  c0 = cast(replace(col0,  ', ', '.') as numeric(14,4));
  if(:col1='' or :col1 is null) then col1 = '0';
  c1 = cast(replace(col1,  ', ', '.') as numeric(14,4));

  -- sprawdz czy rekord istnieje, popraw lub dodaj
  if(:poz<>0 and exists(select first 1 1 from pozzam where ref=:poz)) then begin
    update pozzam set magazyn=:key2, KTM=:key3, WERSJA=:key4, ilosc = :c0, paramn1 = :c1
    where ref=:poz;
  end else if(exists(select first 1 1 from pozzam where zamowienie=:zam and magazyn=:key2 and ktm=:key3 and wersja=:key4)) then begin
    update pozzam set ilosc = :c0, paramn1 = :c1
    where zamowienie=:zam and magazyn=:key2 and ktm=:key3 and wersja=:key4;
  end else begin
    insert into pozzam(zamowienie, magazyn, ktm, wersja, ilosc, paramn1)
    values(:zam, :key2, :key3, :key4, :c0, :c1);
  end

  -- wychodzimy
  status = 1;
  msg = 'ok';
  suspend;
end^
SET TERM ; ^
