--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_IMPORT_POPULARITY as
declare variable IDDOSTAWCY STRING120;
declare variable ILOSC INTEGER_ID;
begin
    for
        select e.s1, e.s2 from expimp e
        into :iddostawcy, :ilosc
    do begin
        update dostawcy d set d.x_popularity = :ilosc where d.int_id = :iddostawcy;
    end

end^
SET TERM ; ^
