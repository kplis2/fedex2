--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PERSON_CHECK_COMPANY(
      PERSON integer,
      COMPANY integer)
  returns (
      SINGLEPERSON smallint,
      CNT smallint,
      STATUS smallint,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable COMPANYNAME varchar(80);
declare variable EXIST smallint = 0;
begin
  status = 1;

  execute procedure getconfig('SINGLEPERSON') returning_values :singleperson;
  if (singleperson is null) then singleperson = 0;

  select coalesce(name,symbol) from companies
    where ref = :company
    into :companyname;

  select count(person) from eperscompany
    where person = :person
    into :cnt;

  select count(person) from eperscompany
    where person = :person and company = :company
    into :exist;

  if (exist = 0) then
  begin
    if (singleperson = 1) then
      msg = 'Czy powiązać, już istniejącą, wybraną kartotekę osobową z bieżącym zakładem?';
    else
      msg = 'Czy skopiować dane osobowe z wybranej kartoteki osobowej';
  end else
  begin
    msg = 'Wybrana osoba jest już przypisana do zakładu: ' || companyname || '!';
    if (singleperson = 0) then
      msg = msg || ' Czy skopiować dane osobowe?';
    status = 0;
  end
end^
SET TERM ; ^
