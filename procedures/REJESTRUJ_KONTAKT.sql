--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REJESTRUJ_KONTAKT(
      EMADRREF integer,
      EMREF integer,
      EMCPODMIOT integer,
      EMPKOSOBA integer)
   as
declare variable TYPKON integer;
declare variable INOUT smallint;
declare variable OPIS type of column kontakty.opis;
declare variable FOLD integer;
declare variable EMSTAN integer;
declare variable EMTYTUL type of column emailwiad.tytul;
declare variable EMDATA timestamp;
declare variable EMOPER integer;
begin

  select FOLDER,STAN,TYTUL,DATA,OPERATOR from EMAILWIAD where REF=:emref into :fold,:emstan,:emtytul,:emdata,:emoper;
  select TYPKONTAKTU from DEFFOLD where REF=:fold into :typkon;
  if(:emstan=1) then begin
    inout = 1;
    opis = 'Odebranie wiadomosci: ';
  end else if(:emstan=3) then begin
    inout = 0;
    opis = 'Wyslanie wiadomosci: ';
  end else typkon = NULL;
  opis = opis || :emtytul;
  if((:typkon is not null) and (:emcpodmiot is not NULL)) then begin
    insert into KONTAKTY(DATA,INOUT,RODZAJ,OPIS,OPERATOR,CPODMIOT,PKOSOBA,ZALEZNY,EMAILWIAD,EMAILADR)
    values (:emdata,:inout,:typkon,:opis,:emoper,:emcpodmiot,:empkosoba,1,:emref, :emadrref);
  end
  suspend;
end^
SET TERM ; ^
