--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_7(
      BKDOC integer)
   as
declare variable account varchar(20);
declare variable amount numeric(14,2);
declare variable settlement varchar(20);
declare variable descript varchar(255);
declare variable n_ref integer;
declare variable n_kosztdost numeric(14,2);
declare variable n_odchylkor numeric(14,2);
declare variable p_clo numeric(14,2);
declare variable p_vat numeric(14,2);
declare variable sum_val numeric(14,2);
declare variable dictdef integer;
declare variable dictpos integer;
declare variable tmp varchar(255);
declare variable bankaccount integer;
begin

  -- schemat dekretowania - dokumenty SAD

  select B.symbol, B.descript, N.ref, N.kosztdost, N.odchylkor, B.dictdef, B.dictpos, B.bankaccount
    from bkdocs B
    join nagfak N on (N.ref = B.oref)
    where B.otable='NAGFAK' and B.ref=:bkdoc
    into :settlement, :descript, :n_ref, :n_kosztdost, :n_odchylkor, :dictdef, :dictpos, :bankaccount;

  sum_val = n_kosztdost;

  account = '300';
  amount = n_kosztdost;
  execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);
  
  for select clo, vat
    from pozsad
    where faktura = :n_ref
    into :p_clo, :p_vat
  do begin
    if (p_clo <> 0) then
      sum_val = sum_val + p_clo;

    account = '300';
    amount = p_clo;
    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

    if (p_vat <> 0) then
      sum_val = sum_val + p_vat;

    account = '225';
    amount = p_vat;
    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);
  end

  execute procedure KODKS_FROM_DICTPOS(dictdef, dictpos)
    returning_values :tmp;

  account = '228';
  amount = sum_val;
  execute procedure insert_decree_settlement(bkdoc, account || tmp, 1, amount, descript, settlement, null, 3, null, null, null, null, null, null, :bankaccount, 0);

  if (n_odchylkor < 0) then
  begin
    amount = -n_odchylkor;
    account = '300';
    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

    account = '760-04';
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);
    
  end else
  if (n_odchylkor > 0) then
  begin
    amount = n_odchylkor;
    account = '761';
    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

    account = '300';
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);
  end
end^
SET TERM ; ^
