--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TIMEDIFF2STR(
      TIMEDIF float)
  returns (
      STR varchar(20) CHARACTER SET UTF8                           )
   as
declare variable secv float;
declare variable minv float;
declare variable hourv float;
declare variable dayv float;
declare variable v integer;
declare variable d integer;
begin
  timedif = cast(timedif *  86400 as integer);
  dayv = 1;
  hourv = :dayv/24;
  minv = :hourv / 60;
  secv = :minv / 60;
  d = 0;
  str = '';
  --sekundy
  v = timedif - cast(cast(timedif as integer) / 60 as integer)*60;
  str = v;
  if(v < 10)then
    str = '0'||:str;
  str = ':'||str;
  timedif = cast(cast(timedif as integer) / 60 as integer)-:d;
  d = 0;
  --minuty
  v = timedif - cast(cast(timedif as integer) / 60 as integer)*60;
  str = v||str;
  if(v < 10)then
    str = '0'||:str;
  str = ':'||str;
  timedif = cast(cast(timedif as integer) / 60 as integer) -:d;
  --godziny
  d = cast(cast(timedif as integer) / 24 as integer);
  v = cast(timedif as integer) - 24 * :d;
  if(d>0)then
    str = d||'d '||v||str;
  else
    str = v||str;
end^
SET TERM ; ^
