--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_DATA_FOR_DOKUMPOZ(
      REF integer)
  returns (
      DATA varchar(255) CHARACTER SET UTF8                           ,
      DOKUMROZ varchar(255) CHARACTER SET UTF8                           )
   as
declare variable d varchar(40);
declare variable rcena numeric(14,4);
declare variable rilosc numeric(14,4);
begin
 data = '';
 dokumroz = '';
 for select cast(dostawy.datawazn as varchar(40))
 from dokumpoz join  dokumroz on (dokumpoz.ref = dokumroz.pozycja)
      join dostawy on (dokumroz.dostawa = dostawy.ref)
 where dokumpoz.ref = :ref into :d
 do begin
   if (:data='') then data = substring(:d from 1 for 10);
   else data = :data ||'; '|| substring(:d from 1 for 10);
 end
 for select cena, ilosc
   from dokumroz r
   where r.pozycja = :ref
   into :rcena, :rilosc
 do begin
   if(coalesce(char_length(dokumroz),0)<220) then begin -- [DG] XXX ZG119346
     if (dokumroz='') then dokumroz = rilosc||' - '||rcena;
     else dokumroz = dokumroz||'; '||rilosc||' - '||rcena;
   end
 end
 suspend;
end^
SET TERM ; ^
