--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_GUS_SIX(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      FIELD varchar(5) CHARACTER SET UTF8                           )
  returns (
      AMOUNT numeric(15,2))
   as
declare variable frvhdr integer;
declare variable frpsn integer;
declare variable frcol integer;
declare variable yearid integer;
declare variable cacheparams varchar(255);
declare variable ddparams varchar(255);
declare variable company integer;
declare variable frversion integer;
declare variable lastday date;
declare variable absences integer;
begin
  select F.frvhdr, F.frpsn, F.frcol
    from frvpsns F
    where F.ref = :frvpsn
    into :frvhdr, :frpsn, :frcol;

  yearid = cast(substring(:period from 1 for 4) as integer);
  select F.company, F.frversion
    from frvhdrs F
    where F.ref = :frvhdr
    into :company, :frversion;
  cacheparams = field||';'||yearid||';'||company;
  ddparams = '';

  lastday = yearid||'/12/31';

  if (not exists (select ref from frvdrilldown where functionname = 'GUS_SIX'
           and cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr)) then
  begin
    -- PRZECIETNA LICZBA ZATRUDNIONYCH - OGOLEM
    if (field = 'D1P1') then begin
     select amount from avg_empl_in_person(:yearid,null,:company)
        into :amount;
    end else if (field = 'D1W1') then
    begin
      --WYNAGRODZENIA BRUTTO - OGOLEM
      select sum(P.pvalue) / 1000
        from epayrolls PR
          join eprpos P on (P.payroll = PR.ref)
        where PR.cper starting with :yearid and P.ecolumn = 4000
          and pr.company = :company and pr.empltype = 1
        into :amount;
      amount = cast(amount as numeric(14,1));
    end else if (field = 'D1W4') then
    begin
      -- Składki na ubezpieczenia emerytalne, rentowe i chorobowe opłacane przez ubezpieczonych
      select sum(P.pvalue) / 1000
        from epayrolls PR
          join eprpos P on (P.payroll = PR.ref)
        where PR.cper starting with :yearid and P.ecolumn in (6100, 6110, 6120, 6130)
          and pr.company = :company and pr.empltype in (1,2)
        into :amount;
      amount = cast(amount as numeric(14,1));
    end else if (field = 'D1P6') then
    begin
      -- liczba pracownikow wg stanu na 31.12 ktorzy nie przekroczyli min. krajowej

    end else if (field = 'D1W6') then
    begin
      -- liczba pracownikow wg stanu na 31.12 ktorzy nie przekroczyli min. krajowej   - w tym 1020.80 - 1275.99 (na rok 2009)

    end else if (field = 'D2W1') then
    begin
      -- WYNAGRODZENIA OSOBOWE
      select sum(P.pvalue) / 1000
        from epayrolls PR
          join eprpos P on (P.payroll = PR.ref)
        where PR.cper starting with :yearid and P.ecolumn = 4000
          and pr.company = :company and pr.empltype = 1
        into :amount;
      amount = cast(amount as numeric(14,1));
    end else if (field = 'D2W2') then
    begin
      -- w tym w godzinach nadliczbowych
      select sum(P.pvalue) / 1000
        from epayrolls PR
          join eprpos P on (P.payroll = PR.ref)
        where PR.cper starting with :yearid and P.ecolumn in (1200, 1210)
           and pr.company = :company and pr.empltype = 1
        into :amount;
      amount = cast(amount as numeric(14,1));
    end else if (field = 'D2W6') then
    begin
      -- WYNAGRODZENIA Z TYTULU UMOWY-ZLECENIA LUB UMOWY O DZIELO
      select sum(P.pvalue) / 1000
        from epayrolls PR
          join eprpos P on (P.payroll = PR.ref)
        where PR.cper starting with :yearid and P.ecolumn = 3000
          and pr.company = :company and pr.empltype = 2
        into :amount;
      amount = cast(amount as numeric(14,1));
    end else if (field = 'D3O1') then
    begin
      -- CZAS FAKTYCZNIE PRZEPRACOWANY
      select sum(P.pvalue) / 1000
        from epayrolls PR
          join eprpos P on (P.payroll = PR.ref)
        where PR.cper starting with :yearid and P.ecolumn in (520,600,610)
          and pr.company = :company and pr.empltype = 1
        into :amount;
      amount = cast(amount as numeric(14,1));
    end else if (field = 'D3O2') then
    begin
      -- w tym w godzinach nadliczbowych
      select sum(P.pvalue) / 1000
        from epayrolls PR
          join eprpos P on (P.payroll = PR.ref)
        where PR.cper starting with :yearid and P.ecolumn in (600,610)
          and pr.company = :company and pr.empltype = 1
        into :amount;
      amount = cast(amount as numeric(14,1));
    end else if (field = 'D3O3') then
    begin
      -- CZAS NIEPRZEPRACOWANY
      select cast(sum(A.worksecs) / 3600000.00  as numeric(14,1))
        from eabsences A where A.ayear = :yearid
          and a.company = :company
          and A.correction in (0,2)
          and a.ecolumn not in (330,260)
        into :amount;
    end else if (field = 'D3O4') then
    begin
      -- W TYM OPLACONY
      select cast(sum(A.worksecs) / 3600000.00  as numeric(14,1))
        from eabsences A where A.ayear = :yearid
          and A.ecolumn in (40, 50, 60, 180, 220, 230, 250)
          and a.company = :company and A.correction in (0,2)
        into :amount;
    end else if (field = 'D3O5') then
    begin
      -- w tym z powodu urlopow
      select cast(sum(A.worksecs) / 3600000.00  as numeric(14,1))
        from eabsences A where A.ayear =  :yearid
          and A.ecolumn in (220, 230)
          and a.company = :company and A.correction in (0,2)
        into :amount;
    end else if (field = 'D3O6') then
    begin
      -- W TYM z powodu chorób
      select cast(sum(A.worksecs) / 3600000.00  as numeric(14,1))
        from eabsences A where A.ayear = :yearid
          and A.ecolumn in (40, 50, 60)
          and a.company = :company and A.correction in (0,2)
        into :amount;
    end else if (field = 'D4O1') then
    begin
      -- PRACUJACY OGOLEM
      select count(distinct e.person)
        from employees E
          join employment C on (E.ref = C.employee)
        where C.fromdate <= :lastday
          and (C.todate is null or C.todate >= :lastday)
          and e.company = :company
        into :amount;

      select count(distinct e.person)
        from eabsences a
          join employees e on e.ref = a.employee
        where a.ecolumn in (260,330,300)
          and a.fromdate <= :lastday and a.todate >= :lastday
          and e.company = :company
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end else if (field = 'D4K1') then
    begin
      -- W TYM KOBIETY
      select count(distinct e.person)
        from employees E
          join persons P on (P.ref = E.person)
          join employment C on (E.ref = C.employee)
        where C.fromdate <= :lastday
           and (C.todate is null or C.todate >= :lastday)
           and P.sex = 0  and e.company = :company
        into :amount;

      select count(distinct e.person)
        from eabsences a
          join employees e on e.ref = a.employee
          join persons p on p.ref = e.person
        where a.ecolumn in (260,330,300)
          and a.fromdate <= :lastday and a.todate >= :lastday
          and e.company = :company
          and p.sex = 0
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end else if (field = 'D4O3') then
    begin
      -- PELNOZATRUDNIENI
      select count(*) from (
        select e.person
          from employees E
            join employment C on (E.ref = C.employee)
          where C.fromdate <= :lastday
            and (C.todate is null or C.todate >= :lastday)
            and e.company = :company
          group by e.person
          having sum(c.workdim) >= 1)
        into :amount;

      select count(*) from (
        select e.person
          from eabsences a
            join employees e on e.ref = a.employee
            join employment m on e.ref = m.employee
          where a.fromdate <= :lastday and a.todate >= :lastday
            and e.company = :company
            and a.ecolumn in (260,330,300)
          group by e.person
          having sum(m.workdim) >= 1)
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end else if (field = 'D4K3') then
    begin
      -- W TYM KOBIETY
      select count(*) from (
        select e.person
          from employees E
            join employment C on (E.ref = C.employee)
            join persons p on p.ref = e.person
          where C.fromdate <= :lastday
            and (C.todate is null or C.todate >= :lastday)
            and e.company = :company
            and p.sex = 0
          group by e.person
          having sum(c.workdim) >= 1)
        into :amount;

      select count(*) from (
        select e.person
          from eabsences a
            join employees e on e.ref = a.employee
            join employment m on e.ref = m.employee
            join persons p on p.ref = e.person
          where a.fromdate <= :lastday and a.todate >= :lastday
            and e.company = :company
            and m.workdim >= 1
            and p.sex = 0
            and a.ecolumn in (260,330,300)
          group by e.person
          having sum(m.workdim) >= 1)
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end else if (field = 'D4O4') then
    begin
      -- w tym zatrudnieni na umowy okresowe
      select count(*) from (
        select e.person
          from employees E
            join employment C on (E.ref = C.employee)
            join emplcontracts ec on ec.ref = c.emplcontract
          where C.fromdate <= :lastday
            and (C.todate is null or C.todate >= :lastday)
            and e.company = :company
            and ec.econtrtype in (2,3)
          group by e.person
          having sum(c.workdim) >= 1)
        into :amount;

      select count(*) from (
        select e.person
          from eabsences a
            join employees e on e.ref = a.employee
            join employment m on e.ref = m.employee
          where a.fromdate <= :lastday and a.todate >= :lastday
            and e.company = :company
            and a.ecolumn in (260,330,300)
          group by e.person
          having sum(m.workdim) >= 1)
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end else if (field = 'D4K4') then
    begin
      -- w tym zatrudnieni na umowy okresowe - w tym kobiety
      select count(*) from (
        select e.person
          from employees E
            join employment C on (E.ref = C.employee)
            join persons p on p.ref = e.person
            join emplcontracts ec on ec.ref = c.emplcontract
          where C.fromdate <= :lastday
            and (C.todate is null or C.todate >= :lastday)
            and e.company = :company
            and p.sex = 0
            and ec.econtrtype in (2,3)
          group by e.person
          having sum(c.workdim) >= 1)
        into :amount;

      select count(*) from (
        select e.person
          from eabsences a
            join employees e on e.ref = a.employee
            join employment m on e.ref = m.employee
            join persons p on p.ref = e.person
          where a.fromdate <= :lastday and a.todate >= :lastday
            and e.company = :company
            and m.workdim >= 1
            and p.sex = 0
            and a.ecolumn in (260,330,300)
          group by e.person
          having sum(m.workdim) >= 1)
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end else if (field = 'D4O5') then
    begin
      -- NIEPELNOZATRUDNIENI
      select count(*) from (
        select e.person
          from employees E
            join employment C on (E.ref = C.employee)
          where C.fromdate <= :lastday
            and (C.todate is null or C.todate >= :lastday)
            and e.company = :company
          group by e.person
          having sum(c.workdim) < 1)
        into :amount;

      select count(*) from (
        select e.person
          from eabsences a
            join employees e on e.ref = a.employee
            join employment m on e.ref = m.employee
          where a.fromdate <= :lastday and a.todate >= :lastday
            and e.company = :company
            and a.ecolumn in (260,330,300)
          group by e.person
          having sum(m.workdim) < 1)
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end else if (field = 'D4K5') then
    begin
      -- NIEPELNOZATRUDNIENI - w tym kobiety
      select count(*) from (
        select e.person
          from employees E
            join employment C on (E.ref = C.employee)
            join persons p on p.ref = e.person
          where C.fromdate <= :lastday
            and (C.todate is null or C.todate >= :lastday)
            and e.company = :company
            and p.sex = 0
          group by e.person
          having sum(c.workdim) < 1)
        into :amount;

      select count(*) from (
        select e.person
          from eabsences a
            join employees e on e.ref = a.employee
            join employment m on e.ref = m.employee
            join persons p on p.ref = e.person
          where a.fromdate <= :lastday and a.todate >= :lastday
            and e.company = :company
            and m.workdim < 1
            and p.sex = 0
            and a.ecolumn in (260,330,300)
          group by e.person
          having sum(m.workdim) < 1)
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end else if (field = 'D4O6') then
    begin
      -- NIEPELNOZATRUDNIENI - w tym zatr na umowy okresowe
      select count(*) from (
        select e.person
          from employees E
            join employment C on (E.ref = C.employee)
            join emplcontracts ec on ec.ref = c.emplcontract
          where C.fromdate <= :lastday
            and (C.todate is null or C.todate >= :lastday)
            and e.company = :company
            and ec.econtrtype in (2,3)
          group by e.person
          having sum(c.workdim) < 1)
        into :amount;

      select count(*) from (
        select e.person
          from eabsences a
            join employees e on e.ref = a.employee
            join employment m on e.ref = m.employee
          where a.fromdate <= :lastday and a.todate >= :lastday
            and e.company = :company
            and a.ecolumn in (260,330,300)
          group by e.person
          having sum(m.workdim) < 1)
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end else if (field = 'D4K6') then
    begin
      -- NIEPELNOZATRUDNIENI - w tym zatr na umowy okresowe - w tym kobiety
      select count(*) from (
        select e.person
          from employees E
            join employment C on (E.ref = C.employee)
            join persons p on p.ref = e.person
            join emplcontracts ec on ec.ref = c.emplcontract
          where C.fromdate <= :lastday
            and (C.todate is null or C.todate >= :lastday)
            and e.company = :company
            and p.sex = 0
            and ec.econtrtype in (2,3)
          group by e.person
          having sum(c.workdim) < 1)
        into :amount;

      select count(*) from (
        select e.person
          from eabsences a
            join employees e on e.ref = a.employee
            join employment m on e.ref = m.employee
            join persons p on p.ref = e.person
          where a.fromdate <= :lastday and a.todate >= :lastday
            and e.company = :company
            and m.workdim < 1
            and p.sex = 0
            and a.ecolumn in (260,330,300)
          group by e.person
          having sum(m.workdim) < 1)
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end else if (field = 'D4O12') then
    begin
      -- NIEPELNOSPRAWNI
      select count(distinct e.person)
        from employees E
          join employment C on (E.ref = C.employee)
          join ezusdata Z on (Z.person = e.person)
          join edictzuscodes d on d.ref = z.invalidity
        where C.fromdate <= :lastday
          and (C.todate is null or C.todate >= :lastday)
          and d.dicttype = 2 and d.code <> 0
          and e.company = :company
        into :amount;

      select count(distinct e.person)
        from eabsences a
          join employees e on e.ref = a.employee
          join ezusdata Z on (Z.person = e.person)
          join edictzuscodes d on d.ref = z.invalidity
        where a.fromdate <= :lastday and a.todate >= :lastday
          and e.company = :company
          and d.dicttype = 2 and d.code <> 0
          and a.ecolumn in (260,330,300)
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end else if (field = 'D4K12') then
    begin
      -- NIEPELNOSPRAWNI - w tym kobiety
      select count(distinct e.person)
        from employees E
          join employment C on (E.ref = C.employee)
          join ezusdata Z on (Z.person = e.person)
          join edictzuscodes d on d.ref = z.invalidity
          join persons p on p.ref = e.person
        where C.fromdate <= :lastday
          and (C.todate is null or C.todate >= :lastday)
          and d.dicttype = 2 and d.code <> 0
          and e.company = :company
          and p.sex = 0
        into :amount;

      select count(distinct e.person)
        from eabsences a
          join employees e on e.ref = a.employee
          join ezusdata Z on (Z.person = e.person)
          join edictzuscodes d on d.ref = z.invalidity
          join persons p on p.ref = e.person
        where a.fromdate <= :lastday and a.todate >= :lastday
          and e.company = :company
          and d.dicttype = 2 and d.code <> 0
          and a.ecolumn in (260,330,300)
          and p.sex = 0
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end else if (field = 'D4O13') then
    begin
      -- CUDZOZIEMCY
      select count(distinct e.person)
        from employees E
          join employment C on (E.ref = C.employee)
          join persons P on (P.ref = E.person)
        where C.fromdate <= :lastday
          and (C.todate is null or C.todate >= :lastday)
          and P.foreigner = 1
          and e.company = :company
        into :amount;

      select count(distinct e.person)
        from eabsences a
          join employees e on e.ref = a.employee
          join persons p on p.ref = e.person
        where a.fromdate <= :lastday and a.todate >= :lastday
          and e.company = :company
          and a.ecolumn in (260,330,300)
          and p.foreigner = 1
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end else if (field = 'D4K13') then
    begin
      -- CUDZOZIEMCY - w tym kobiety
      select count(distinct e.person)
        from employees E
          join employment C on (E.ref = C.employee)
          join persons P on (P.ref = E.person)
        where C.fromdate <= :lastday
          and (C.todate is null or C.todate >= :lastday)
          and P.foreigner = 1 and p.sex = 0
          and e.company = :company
        into :amount;

      select count(distinct e.person)
        from eabsences a
          join employees e on e.ref = a.employee
          join persons p on p.ref = e.person
        where a.fromdate <= :lastday and a.todate >= :lastday
          and e.company = :company
          and a.ecolumn in (260,330,300)
          and p.foreigner = 1 and p.sex = 0
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end else if (field = 'D4O14') then
    begin
      -- EMERYCI I RENCISCI
      select count(distinct e.person)
        from employees E
          join employment C on (E.ref = C.employee)
          join ezusdata Z on (Z.person = e.person)
          join edictzuscodes d on d.ref = z.invalidity
        where C.fromdate <= :lastday
          and (C.todate is null or C.todate >= :lastday)
          and d.dicttype = 1 and d.code <> 0
          and e.company = :company
        into :amount;

      select count(distinct e.person)
        from eabsences a
          join employees e on e.ref = a.employee
          join ezusdata Z on (Z.person = e.person)
          join edictzuscodes d on d.ref = z.invalidity
        where a.fromdate <= :lastday and a.todate >= :lastday
          and e.company = :company
          and d.dicttype = 1 and d.code <> 0
          and a.ecolumn in (260,330,300)
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end else if (field = 'D4K14') then
    begin
      -- EMERYCI I RENCISCI - w tym kobiety
      select count(distinct e.person)
        from employees E
          join employment C on (E.ref = C.employee)
          join ezusdata Z on (Z.person = e.person)
          join edictzuscodes d on d.ref = z.invalidity
          join persons p on p.ref = e.person
        where C.fromdate <= :lastday
          and (C.todate is null or C.todate >= :lastday)
          and d.dicttype = 1 and d.code <> 0
          and e.company = :company
          and p.sex = 0
        into :amount;

      select count(distinct e.person)
        from eabsences a
          join employees e on e.ref = a.employee
          join ezusdata Z on (Z.person = e.person)
          join edictzuscodes d on d.ref = z.invalidity
          join persons p on p.ref = e.person
        where a.fromdate <= :lastday and a.todate >= :lastday
          and e.company = :company
          and d.dicttype = 1 and d.code <> 0
          and a.ecolumn in (260,330,300)
          and p.sex = 0
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end
  end else
    select first 1 fvalue from frvdrilldown
      where cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr
      into :amount;
  if (amount is null) then amount = 0;
  insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
     values (:frvhdr, :frpsn, :frcol, 'GUS_SIX', :cacheparams, :ddparams, :amount, :frversion);
  suspend;
end^
SET TERM ; ^
