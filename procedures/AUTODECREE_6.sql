--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_6(
      BKDOC integer)
   as
declare variable account varchar(20);
  declare variable amount numeric(14,2);
  declare variable curramount numeric(14,2);
  declare variable descript varchar(255);
  declare variable dictpos int;
  declare variable dictdef int;
  declare variable oddzial varchar(255);
  declare variable magazyn varchar(255);
  declare variable symbol varchar(20);
  declare variable dostawca varchar(20);
  declare variable waluta varchar(3);
  declare variable currency varchar(3);
  declare variable kurs numeric(14,4);
  declare variable rate numeric(14,4);
  declare variable settlement varchar(20);
  declare variable opendate timestamp;
  declare variable payday timestamp;
  declare variable termzap timestamp;
  declare variable wartosczl numeric(14,2);
  declare variable wartosc numeric(14,2);
  declare variable odchylenie numeric(14,2);
  declare variable freeofcharge numeric(14,2);
  declare variable sumprocentplat numeric(14,2);
  declare variable oref integer;
  declare variable otable varchar(40);
  declare variable sumgrossv numeric(14,2);
  declare variable docdate timestamp;
  declare variable crnote smallint;
  declare variable fsopertype smallint;

begin
  -- schemat dekretowania - PZI

  descript = '';

  select D.symbfak, B.descript, D.pwartosc, D.oddzial, D.magazyn,
    D.slodef, D.slopoz, D.kurs, D.waluta, D.wartoscwal, D.symbfak, D.data,
    D.wartosczl, D.odchylenie, D.sumfoczl
    from bkdocs B
    join dokumnag D on (D.ref = B.oref and B.ref=:bkdoc)
    into :symbol, :descript, :wartosc, :oddzial, :magazyn,
    :dictdef, :dictpos, :kurs, :waluta, :curramount, :settlement, :opendate,
    :wartosczl, :odchylenie, :freeofcharge;

    select  B.sumgrossv, B.symbol, B.docdate, B.payday,
      B.descript, B.dictdef, B.dictpos, B.oref,B.otable ,T.creditnote, O.symbol
    from bkdocs B
      left join bkdoctypes T on (B.doctype = T.ref)
      left join nagfak N on (B.oref = N.ref)
      left join oddzialy O on (N.oddzial = O.oddzial)
      left join vatregs VR on (B.vatreg = VR.symbol and B.company = VR.company)
    where B.ref = :bkdoc
    into :sumgrossv, :settlement, :docdate, :payday,
      :descript, :dictdef, :dictpos, :oref,otable, :crnote, :oddzial;

    if (crnote = 1) then
      fsopertype = 2;
    else
      fsopertype = 1;
    amount = wartosczl;
    execute procedure kodks_from_dictpos(dictdef, dictpos)
      returning_values dostawca;
    account = '204-' || dostawca;
    currency = waluta; rate = kurs;
    payday = opendate;

    execute procedure fk_autodecree_termplat(bkdoc,account, amount, curramount, rate, currency,  1 , settlement,
                                          oref, otable,fsopertype,docdate,
                                          payday,descript)
      returning_values :sumprocentplat;

    amount = wartosczl;
    account = '300';
    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

    amount = wartosc;
    account = '300';
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

    amount = wartosc;
    account = '330-' || magazyn;
    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

    amount = freeofcharge;
    account = '300';
    execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

    amount = freeofcharge;
    account = '760-04';
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

    if (odchylenie > 0) then begin
      amount = odchylenie;
      account = '300';
      execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);

      account = '760-04';
      execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);
    end

    if (odchylenie < 0) then begin
      amount = -odchylenie;
      account = '300';
      execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);

      account = '761';
      execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);
    end

end^
SET TERM ; ^
