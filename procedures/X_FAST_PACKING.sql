--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_FAST_PACKING(
      LISTYWYSD INTEGER_ID,
      OPERATOR INTEGER_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
  declare variable symbol string20;
  declare variable wersjaref integer_id;
  declare variable listywysdpozref integer_id;
  declare variable ilosc numeric_14_4;
  declare variable ktm ktm_id;
begin
  status = 0;
  msg = '';

  select m.symbol
    from listywysd m
    where m.ref = :listywysd
  into :symbol;

  if (coalesce(symbol, '')<>'') then begin
    for
      select l.ref, l.wersjaref, l.iloscmax
        from listywysdpoz l
        where l.dokument = :listywysd
      into :listywysdpozref, :wersjaref, :ilosc
    do begin
      select t.ktm
        from towary t
          join wersje w on w.ktm = t.ktm
        where w.ref = :wersjaref
      into :ktm;
      insert into listywysdroz(listywysd, listywysdpoz, iloscmag, pakowal, x_data_pak)
                    values(:listywysd, :listywysdpozref, :ilosc, :operator, current_timestamp(0));
    end
    update listywysd l
      set l.x_szybko = 1
      where l.ref = :listywysd;
    update listywysdpoz l
      set l.iloscspk = l.iloscmax
      where l.dokument = :listywysd;
    status = 1;
    msg = 'Spakowano wszystkie produkty '||:symbol;
    suspend;
    exit;
  end else begin
    status =0;
    msg = 'Brak wymaganego dokumentu';
    suspend;
    exit;
  end

end^
SET TERM ; ^
