--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ROZRACHROZ_WYROWNAJ(
      DOKUMENT integer,
      TYP varchar(3) CHARACTER SET UTF8                           )
   as
declare variable rozref integer;
declare variable slodef integer;
declare variable slopoz integer;
declare variable symbfak varchar(30);
declare variable kontofk KONTO_ID;
declare variable winien numeric(14,2);
declare variable ma numeric(14,2);
declare variable rwinien numeric(14,2);
declare variable rma numeric(14,2);
declare variable rowinien numeric(14,2);
declare variable roma numeric(14,2);
begin
  for select REF, SLODEF, SLOPOZ, KONTOFK, SYMBFAK, WINIEN, MA
  from ROZRACHROZ
  where rozrachroz.doktyp = :typ and rozrachroz.dokref = :dokument
  into :rozref, :slodef, :slopoz, :kontofk, :symbfak, :winien, :ma
  do begin
    rwinien = 0;
    rma = 0;
    rowinien = 0;
    roma = 0;
    select WINIEN, MA
      from ROZRACH where SLODEF = :Slodef and SLOPOZ = :slopoz and SYMBFAK = :symbfak and KONTOFK = :kontofk
    into :rwinien, :rma;
    select sum(WINIEN), sum(MA)
      from ROZRACHROZ where SLODEF = :Slodef and SLOPOZ = :slopoz and
        SYMBFAK = :symbfak and KONTOFK = :kontofk and ref <> :rozref
      into :rowinien, :roma;
    if(:rwinien is null) then rwinien = 0;
    if(:rma is null) then rma = 0;
    if(:rowinien is null) then rowinien = 0;
    if(:roma is null) then roma = 0;
    rwinien = rwinien + rowinien;
    rma = rma + roma;
    if(:rma > :rwinien) then begin
      rma = :rma - :rwinien;
      rwinien = 0;
    end else begin
      rwinien = :rwinien - :rma;
      rma = 0;
    end
    if(:ma <> :rwinien) then
      update rozrachroz set MA = :RWINIEN where ref=:rozref;
    if(:winien <> :rma) then
      update ROZRACHROZ set WINIEN=:RMA where ref=:rozref;


  end
end^
SET TERM ; ^
