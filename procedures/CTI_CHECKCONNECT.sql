--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CTI_CHECKCONNECT(
      MPHONE varchar(60) CHARACTER SET UTF8                           ,
      TRYB smallint)
  returns (
      RECCOUNT integer,
      MAXLINES integer,
      MAXLINESSMALL integer)
   as
begin
  select count(*), max(cti_showconnect.lines), max(cti_showconnect.linessmall)
    from cti_showconnect(:mphone, :tryb)
    into :reccount, :maxlines, :maxlines;

  if(:maxlines is null) then maxlines = 1;
  if(:maxlinessmall is null) then maxlinessmall = 1;

  suspend;
end^
SET TERM ; ^
