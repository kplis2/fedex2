--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_21(
      BKDOC integer)
   as
declare variable vdoctype varchar(2);
declare variable isintasset smallint;
declare variable tvalue numeric(15,2);
declare variable tredemption numeric(15,2);
declare variable vsymbol varchar(80);
declare variable fabksymbol varchar(20);
begin
  select v.doctype, f.isintasset, v.tvalue, v.tredemption, v.symbol, a.fabksymbol
    from bkdocs b
    left join valdocs v on b.otable = 'VALDOCS' and b.oref = v.ref
    left join fxdassets f on v.fxdasset = f.ref
    left join amortgrp a on f.amortgrp = a.symbol
    where b.ref = :bkdoc
  into :vdoctype, :isintasset, :tvalue, :tredemption, :vsymbol, :fabksymbol;

  if (vdoctype in ('OT','MS','K-', 'K+')) then begin
    if (:isintasset = 0) then begin
      execute procedure insert_decree(bkdoc, '010-'||:fabksymbol , 0, tvalue, 'Dokument '||vsymbol, 0);
      execute procedure insert_decree(bkdoc, '080-01', 1, tvalue, 'Dokument '||vsymbol, 0);
    end else begin
      execute procedure insert_decree(bkdoc, '011-'||:fabksymbol, 0,tvalue, 'Dokument '||vsymbol, 0);
      execute procedure insert_decree(bkdoc, '080-03', 1, tvalue, 'Dokument '||vsymbol, 0);
    end
  end else if (vdoctype in ('LT','LC','ST')) then begin
    if (:isintasset = 0) then begin
      execute procedure insert_decree(bkdoc, '070-'||:fabksymbol, 0, tredemption * (-1), 'Dokument '||vsymbol, 0);
      execute procedure insert_decree(bkdoc, '010-'||:fabksymbol, 1, tvalue * (-1), 'Dokument '||vsymbol, 0);
    end else begin
      execute procedure insert_decree(bkdoc, '071-'||:fabksymbol, 0,tvalue * (-1), 'Dokument '||vsymbol, 0);
      execute procedure insert_decree(bkdoc, '021-'||:fabksymbol, 1, tvalue * (-1), 'Dokument '||vsymbol, 0);
    end
    execute procedure insert_decree(bkdoc, '761-03', 0, tredemption - tvalue, 'Dokument '||vsymbol, 0);
  end
end^
SET TERM ; ^
