--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OBLICZ_CENE(
      BN char(1) CHARACTER SET UTF8                           ,
      ILOSC numeric(14,4),
      CENACEN numeric(14,4),
      WALCEN varchar(3) CHARACTER SET UTF8                           ,
      RABAT numeric(14,4),
      VAT numeric(14,2),
      PREC smallint)
  returns (
      RCENANET numeric(14,4),
      RCENABRU numeric(14,4),
      RWARTNET numeric(14,2),
      RWARTBRU numeric(14,2))
   as
begin
  if(:bn is null or :bn=' ') then bn = 'N';
  if(:prec is null) then select -F.rdb$field_scale from rdb$fields F where F.rdb$field_name = 'CENYSPR' into :prec;
  -- przelicz z waluty cennika na walute dokumentu



  -- uwzglednij rabat
  if(:bn='N') then begin
    if(:prec=4) then
      rcenanet = cast(:cenacen * (100-:rabat)/100 as numeric(14,4));
    else
      rcenanet = cast(:cenacen * (100-:rabat)/100 as numeric(14,2));
  end else begin
    if(:prec=4) then
      rcenabru = cast(:cenacen * (100-:rabat)/100 as numeric(14,4));
    else
      rcenabru = cast(:cenacen * (100-:rabat)/100 as numeric(14,2));
  end
  if(:rcenanet is null) then rcenanet = 0;
  if(:rcenabru is null) then rcenabru = 0;

  -- oblicz wartosci netto i brutto
  if(:bn = 'B') then begin
     rwartbru = :ilosc * :rcenabru;
     rwartnet = cast((:rwartbru / (1+:vat / 100)) as DECIMAL(14,2));
     if(:prec=4) then
       rcenanet = cast((:rcenabru / (1+(:vat/100))) as DECIMAL(14,4));
     else
       rcenanet = cast((:rcenabru / (1+(:vat/100))) as DECIMAL(14,2));
  end else begin
     rwartnet = :ilosc * :rcenanet;
     rwartbru = :rwartnet + cast((:rwartnet * (:vat / 100)) as DECIMAL(14,2));
     if(:prec=4) then
       rcenabru = :rcenanet + cast((:rcenanet * (:vat / 100)) as DECIMAL(14,4));
     else
       rcenabru = :rcenanet + cast((:rcenanet * (:vat / 100)) as DECIMAL(14,2));
  end
end^
SET TERM ; ^
