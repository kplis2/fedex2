--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_FUN_ILZATR(
      PERIOD varchar(6) CHARACTER SET UTF8                           )
  returns (
      AMOUNT numeric(15,2))
   as
DECLARE VARIABLE DATAZM TIMESTAMP;
DECLARE VARIABLE FDAY TIMESTAMP;
DECLARE VARIABLE LDAY TIMESTAMP;
begin

  execute procedure datatookres(:PERIOD,0,0,0,1)
    returning_values :PERIOD, :DATAZM, :FDAY, :LDAY;
  select count(*)
    from employees e
    join emplcontracts c on (c.employee = e.ref
    and ((c.fromdate <= :LDAY and c.enddate >= :LDAY)
      or (c.fromdate <= :LDAY and c.enddate is null)
      )
    )
    join ECONTRTYPES t on (t.contrtype = c.econtrtype)
    where t.empltype =1
  into :AMOUNT;
  suspend;
end^
SET TERM ; ^
