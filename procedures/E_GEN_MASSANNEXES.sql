--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GEN_MASSANNEXES(
      ANNEXEPARAMS varchar(4096) CHARACTER SET UTF8                           ,
      EMPLCONTRACT integer)
  returns (
      LOGMSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable CHNG smallint;
declare variable PERSONNAMES varchar(255);
declare variable CONTRDATE date;
declare variable ENDDATE date;
declare variable TMPDATE date;
declare variable FROMDATE date;
declare variable VAR varchar(255);
declare variable VAR2 varchar(80);
declare variable COLS varchar(255);
declare variable VALS varchar(255);
declare variable WART varchar(80);
declare variable KLUCZ varchar(40);
declare variable TREAT varchar(1);
declare variable ALLOWANNEXES integer;
declare variable COMPANY integer;
declare variable TMPSAL varchar(80);
declare variable PT integer;
declare variable RATIOSAL numeric(14,2);
declare variable REGOPER integer;
declare variable EMPLCONTRNAME varchar(200);
declare variable NOANNEXINTRO varchar(250);
begin
/*Procedura wystawia aneks, dla podanej umowy na podstawie parametrow przekazanych w zmiennej ANNEXEPARAMS
  Tekst wpisywany do LOGMSG i zwracany przez procedurę jest potrzebny aby wpisywać do logforma w aplikacji.
  Procedure należy odpalać poprzez akcję masowego generowania aneksów.
  Jeżeli w jakimś parametrze dostaniemy wartość BLANK, np SALARY=BLANK, znaczy to, że na oknie parametrów
  ta wartość nie została wybrana do zmiany, więc należy pobrać z ostatniego aneksu lub umowy (PR61955)
*/

  chng = 0;
  cols = 'EMPLCONTRACT';
  vals = ''||:emplcontract||',';
  annexeparams = replace(:annexeparams, '"', '');

  select e.personnames, c.contrdate, coalesce(c.todate,c.enddate), t.allowannexes, e.company
    from employees e
      join emplcontracts c on c.employee = e.ref
      left join econtrtypes t on t.contrtype = c.econtrtype
    where c.ref = :emplcontract
    into personnames, contrdate, enddate, allowannexes, company;

  emplcontrname = ' ' || personnames || ' (umowa z ' || cast(contrdate as date) || ') - ' ;
  noannexintro = ascii_char(164) || emplcontrname || 'nie wystawiono aneksu';

  if(allowannexes = 0) then
  begin
    logmsg = noannexintro || ', gdyż rodzaj umowy nie pozwala na wystawianie aneksów';
    exit;
  end

  for
    select stringout from parsestring(:annexeparams,';')
      into :var2
  do begin
    if(position('=' in :var2) > 0) then
    begin
      wart = substring(var2 from position('=' in var2)+1);
      klucz = substring(var2 from 1 for position('=' in var2)-1);
    end else
    begin
      logmsg = ascii_char(215) || emplcontrname || 'nie wystawiono aneksu, gdy wystąpił błąd parametrametryzacji';
      exit;
    end
  -------
    if (klucz = 'ATYPE') then
    begin
      cols = cols || ',' || klucz;
      vals = vals || '''' || wart|| '''';
    end else
  -------
    if (klucz = 'FROMDATE') then
    begin
      fromdate = cast(wart as date);

      select first 1 fromdate from econtrannexes
        where fromdate >= :fromdate and emplcontract = :emplcontract
        order by fromdate desc
        into :tmpdate;

      if (tmpdate is not null or enddate < fromdate) then
      begin
        if(enddate < fromdate) then
          logmsg = noannexintro || ', gdyż data aneksu wykracza poza zakres umowy';
        else if (tmpdate = fromdate) then
          logmsg = noannexintro || ', gdyż istnieje już aneks wystawiony na dany dzień';
        else
          logmsg = noannexintro || ', gdyż istnieje aneks późniejszy';
        exit;
      end
      cols = cols || ',' || klucz;
      vals = vals || ',''' || wart|| '''';
      tmpdate = null;
    end else
  -------
    if (klucz in ('SALARY','CADDSALARY','FUNCSALARY')) then
    begin
      if(wart = 'BLANK') then
        execute procedure blank_aneks(:emplcontract,:klucz,1) returning_values :wart;
      else begin
        treat = substring(:wart from position(':' in :wart)+1);
        wart = substring(:wart from 1 for position(':' in :wart)-1);
        wart = replace(wart,',','.');
        execute procedure blank_aneks(:emplcontract,:klucz,1) returning_values :var;
        if(treat = 'V') then --zmiana wartosciowa
          wart = cast(wart as money) + cast(var as money);
        else if(treat = 'P') then --zmiana procentowa
          wart = cast(var as money) + cast(wart as money)*cast(var as money)/100.00;
        else if(treat <> 'N') then --nowa wartosc
          exception universal 'Nieznany parametr "' || treat || '" dla klucza: ' || klucz;

        if(wart < 0) then
        begin
          logmsg = noannexintro || ', gdyż zmiana spowodowałaby ujemną wartość kwoty';
          exit;
        end else
        if(cast(wart as money) <> cast(:var as money)) then
          chng = 1;
      end
      cols = cols || ',' || klucz;
      vals = vals || ',''' || cast(wart as money) || '''';
    end else
  -------
    if (klucz in ('DEPARTMENT','WORKPOST','DIMNUM','DIMDEN','EPAYRULE','DICTPOS','DICTDEF','PAYMENTTYPE')) then
    begin
      execute procedure blank_aneks(:emplcontract,:klucz,1) returning_values :var;
      if(wart = 'BLANK') then
        wart = var;
      if(coalesce(var,'') <> coalesce(wart,'') or (klucz = 'PAYMENTTYPE' and wart = '4')) then
        chng = 1;

      cols = cols || ',' || klucz;
      if(coalesce(wart,'') <> '') then
        vals = vals || ',''' || wart || '''';
      else
        vals = vals || ',null';
    end else
  -------
    if (klucz = 'RATIO') then
    begin
      cols = cols || ',SALARY,RATIO';
      execute procedure blank_aneks(:emplcontract,'SALARY',1) returning_values :tmpsal;
      execute procedure calc_salary(cast(replace(:wart,',','.') as numeric(14,3)), :fromdate, :company) returning_values :ratiosal;
      var = cast(ratiosal as varchar(255));

      if(var is distinct from tmpsal) then
        chng = 1;

      if(var is not null) then
        vals = vals || ',''' || var || ''',' ||replace(wart,',','.');
      else
        vals = vals || ',null,' || replace(:wart,',','.');
    end else
  -------
    if (klucz = 'TODATE') then
    begin
      if(allowannexes = 1) then
      begin
        logmsg = noannexintro || ', gdyż rodzaj umowy nie pozwala na wystawianie aneksów na czas określony';
        exit;
      end
      cols = cols || ',' || klucz;
      vals = vals || ',''' || wart || '''';
    end
  end

  if(annexeparams not containing ';SALARY=BLANK' and annexeparams containing 'PAYMENTTYPE=BLANK') then
  begin
    execute procedure blank_aneks(:emplcontract,'PAYMENTTYPE',1) returning_values :pt;
    if(pt = 4) then
    begin
      logmsg = noannexintro || ', gdyż typ wynagrodzenia jest mnożnikowy';
      exit;
    end
  end

  if(chng = 1) then
  begin
    for
      select stringout from parsestring('BRANCH,LOCAL,BKSYMBOL,SGROUP,WORKCAT,WORKSYSTEM,PROPORTIONAL,BANKACCOUNT,EMPLGROUP',',')
        into :var2
    do begin
      cols = cols || ',' || var2;
      execute procedure blank_aneks(:emplcontract,:var2,1) returning_values :wart;
      if(wart is not null) then
        vals = vals || ',''' || wart || '''';
      else
        vals = vals || ',null';
    end

    execute procedure get_global_param('AKTUOPERATOR') returning_values :regoper;
    cols = cols || ',REGOPERATOR';
    if(regoper is not null)then
      vals = vals || ',' || regoper;
    else
      vals = vals || ',null';

    execute statement 'insert into econtrannexes('||cols||') values('||vals||')';
    logmsg = ascii_char(149) || emplcontrname || 'wystawiono aneks na dzień ' || fromdate;
  end else
    logmsg = noannexintro || ', gdyż nie wprowadziłby on żadnej zmiany';

  suspend;

  when any do
  begin
   logmsg = ascii_char(215) || emplcontrname || 'wystawienie aneksu nie powiodło się';
   suspend;
  end
end^
SET TERM ; ^
