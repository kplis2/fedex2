--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_101(
      BKDOC integer)
   as
declare variable PERIOD varchar(20);
declare variable ACCOUNT ACCOUNT_ID;
declare variable PRDSYMBOL varchar(20);
declare variable SIDE integer;
declare variable AMOUNT numeric(14,2);
declare variable FIRSTPERIOD varchar(20);
declare variable FREQUENCY smallint;
declare variable DEDUCTACCOUNT ACCOUNT_ID;
declare variable DEDUCTAMOUNT numeric(14,2);
declare variable PDEDUCTAMOUNT numeric(14,2);
declare variable POSAMOUNT numeric(14,2);
declare variable FIRSTDEDUCTAMOUNT numeric(14,2);
declare variable DEDUCTEDAMOUNT numeric(14,2);
declare variable SUMDEDUCTAMOUNT numeric(14,2);
declare variable DESCRIPT varchar(80);
declare variable PASSEDPERIODS smallint;
declare variable COMPANY integer;
declare variable DIST1DDEF integer;
declare variable DIST1SYMBOL SYMBOLDIST_ID;
declare variable DIST2DDEF integer;
declare variable DIST2SYMBOL SYMBOLDIST_ID;
declare variable DIST3DDEF integer;
declare variable DIST3SYMBOL SYMBOLDIST_ID;
declare variable DIST4DDEF integer;
declare variable DIST4SYMBOL SYMBOLDIST_ID;
declare variable DIST5DDEF integer;
declare variable DIST5SYMBOL SYMBOLDIST_ID;
declare variable DIST6DDEF integer;
declare variable DIST6SYMBOL SYMBOLDIST_ID;
declare variable PRDDIST1DDEF integer;
declare variable PRDDIST1SYMBOL SYMBOLDIST_ID;
declare variable PRDDIST2DDEF integer;
declare variable PRDDIST2SYMBOL SYMBOLDIST_ID;
declare variable PRDDIST3DDEF integer;
declare variable PRDDIST3SYMBOL SYMBOLDIST_ID;
declare variable PRDDIST4DDEF integer;
declare variable PRDDIST4SYMBOL SYMBOLDIST_ID;
declare variable PRDDIST5DDEF integer;
declare variable PRDDIST5SYMBOL SYMBOLDIST_ID;
declare variable PRDDIST6DDEF integer;
declare variable PRDDIST6SYMBOL SYMBOLDIST_ID;
declare variable FROMSDATE smallint;
declare variable SDATE date;
declare variable LICZNIK integer;
declare variable FDAY integer;
declare variable LDAY integer;
declare variable LDAYDATA timestamp;
declare variable FDAYDATA timestamp;
declare variable PRDREF integer;
declare variable PERIODCOUNT smallint;
declare variable PRDVERSION integer;
declare variable PRDACCOUNTINGPOS integer;
declare variable DIVRATE numeric(14,6);
begin

   -- schemat dekretowania - rozliczenia midzyokresowe
  sumdeductamount = 0;
  select company, period from bkdocs where ref = :bkdoc
    into :company, :period;
  licznik=0;
  for
    select p.ref, p.account, p.prdsymbol, p.side, p.amount, p.firstperiod, p.frequency, p.descript, p.sdate,
           p.periodcount, p.fromsdate, d.dist1ddef, d.dist1symbol, d.dist2ddef,
           d.dist2symbol, d.dist3ddef, d.dist3symbol, d.dist4ddef, d.dist4symbol, d.dist5ddef, d.dist5symbol,
           d.dist6ddef, d.dist6symbol, p.deductedamount, p.deductamount
      from prdaccounting p join decrees d on (d.ref = p.decree)
      where p.firstperiod <= :period and (p.amount - p.deductedamount) > 0 and p.status = 1 and p.company = :company
      order by p.account, p.prdsymbol
      into :prdref, :account, :prdsymbol, :side, :amount, :firstperiod, :frequency, :descript, :sdate,
           :periodcount, :fromsdate, :prddist1ddef, :prddist1symbol, :prddist2ddef, :prddist2symbol,
           :prddist3ddef, :prddist3symbol, :prddist4ddef, :prddist4symbol, :prddist5ddef, :prddist5symbol,
           :prddist6ddef, :prddist6symbol, :deductedamount, :pdeductamount
  do begin
    select first 1 v.ref from prdversions v where v.prdaccounting = :prdref and v.fromperiod <= :period
      order by v.fromperiod desc into :prdversion;
    sumdeductamount = 0;
    select count(*) from bkperiods
      where company = :company and id > :firstperiod and id <= :period and ptype = 1
      into :passedperiods;

    if (:period = :firstperiod or  mod(passedperiods, frequency) = 0 ) then
    begin
    -- zapis na konto odpisu okresowego
      for select ref, deductaccount, deductamount, firstdeductamount,
                 (:periodcount-1)*deductamount + firstdeductamount, dist1ddef, dist1symbol, dist2ddef,
                 dist2symbol, dist3ddef, dist3symbol, dist4ddef, dist4symbol, dist5ddef, dist5symbol,
                 dist6ddef, dist6symbol, divrate
      from prdaccountingpos where prdversion = :prdversion
      into :prdaccountingpos, :deductaccount, :deductamount, :firstdeductamount, :posamount, :dist1ddef,
           :dist1symbol, :dist2ddef, :dist2symbol, :dist3ddef, :dist3symbol, :dist4ddef, :dist4symbol,
           :dist5ddef, :dist5symbol, :dist6ddef, :dist6symbol, :divrate
      do begin
        if (period = firstperiod) then deductamount = :firstdeductamount;

        if (fromsdate <> 0) then begin
          if (period = firstperiod) then begin

            select cast(datatookres.lday as date)
              from datatookres(cast(:sdate as varchar(10)),null,null,null,1)
              into :ldaydata;
            lday = extract(day from ldaydata);
            deductamount = deductamount*(lday-extract(day from sdate))/lday;
          end else begin
            if (deductamount > (amount - deductedamount)*divrate/100 and (amount - deductedamount)<>:pdeductamount) then
              deductamount = (amount - deductedamount)*divrate/100;
        end
        end else begin
          if (deductamount > (amount - deductedamount)*divrate/100 and (amount - deductedamount)<>:pdeductamount) then
            deductamount = (amount - deductedamount)*divrate/100;
        end
        execute procedure insert_decree_prd(bkdoc, deductaccount, side, deductamount, descript, prdsymbol,
         :dist1ddef, :dist1symbol, :dist2ddef, :dist2symbol, :dist3ddef, :dist3symbol,
          :dist4ddef, :dist4symbol, :dist5ddef, :dist5symbol,0, 1, :dist6ddef, :dist6symbol, :prdref, :prdaccountingpos);

         sumdeductamount = :sumdeductamount + deductamount;
      end

    -- zapis na konto rozliczen miedzyokresowych
        side = 1 - side;
        execute procedure insert_decree_prd(bkdoc, account, side, sumdeductamount, descript, prdsymbol,
          :prddist1ddef, :prddist1symbol, :prddist2ddef, :prddist2symbol, :prddist3ddef, :prddist3symbol,
          :prddist4ddef, :prddist4symbol, :prddist5ddef, :prddist5symbol,0, 1, :prddist6ddef, :prddist6symbol, :prdref, null);

    end
  end
end^
SET TERM ; ^
