--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE HB_RKDOKPOZ_SUM(
      RKDOKNAG_REF integer,
      RKDOKPOZ_REF integer)
  returns (
      RKDOKPOZ_SUM numeric(14,2))
   as
begin
  select sum(case when rg.pm <> rp.pm then rp.kwota * (-1) else rp.kwota end) as rkdokpoz_sum
    from rkdoknag rg
      join rkdokpoz rp on (rg.ref = rp.dokument)
  where rp.dokument = :rkdoknag_ref
    and rp.ref <> :rkdokpoz_ref
  into :rkdokpoz_sum;
  rkdokpoz_sum = coalesce(:rkdokpoz_sum,0);
  suspend;
end^
SET TERM ; ^
