--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_PRZEPISZ_POZ_INW(
      Z integer,
      DOINW integer)
  returns (
      ILE integer)
   as
declare variable ilnwold numeric(14,4);
declare variable ktm varchar(30); 
declare variable wersjaref integer;
declare variable cnt integer;
begin
  cnt = 0;
  for select ip.ilinw , ip.ktm, ip.wersjaref from inwentap ip
    where IP.inwenta = :z
    into :ilnwold , :ktm, :wersjaref
  do begin
   update inwentap set ilinw = :ilnwold where inwentap.ktm = :ktm
     and inwentap.wersjaref = :wersjaref and inwentap.inwenta = :doinw;
   cnt = :cnt + 1;
  end
  ile = cnt;
  suspend;
end^
SET TERM ; ^
