--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_LISTYWYSDPOZ_ADDROZ(
      KODKRESK STRING40,
      LISTWYSD LISTYWYSD_ID,
      AKTOPK INTEGER_ID,
      MNOZNIK INTEGER_ID = 1,
      X_OPOZNIONEPAKOWANIE SMALLINT_ID = null)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable ktm ktm_id;
declare variable wersjaref wersje_id;
declare variable ilosc ilosci_mag;
declare variable jedn jedn_miary;
declare variable towjednref towjedn;
declare variable pozycja integer_id;
declare variable ilosctmp ilosci_mag;  --[PM] XXX
declare variable tmpstatus smallint_id; --[PM] XXX #OPOZNIONEPAKOWANIE
declare variable listwysdroz_opk listywysdroz_opk_id ; --[PM] XXX #OPOZNIONEPAKOWANIE
declare variable tmplistwysd listywysd_id; --[PM] XXX #OPOZNIONEPAKOWANIE
declare variable x_spakowano smallint_id; --[PM] XXX #OPOZNIONEPAKOWANIE
begin
  status = 0;
  msg = '';
  kodkresk = upper(:kodkresk);

  --ustawienie, ze dokument byl kontrolowany
  update listywysd set kontrolapak = 1
    where ref = :listwysd and (kontrolapak is null or kontrolapak = 0);
  --koniec ustawienia kontroli

  --[PM] XXX start #OPOZNIONEPAKOWANIE
  if (x_opoznionepakowanie is null) then --jesli nie ma tego parametru to go sciagam
    select x_opoznionepakowanie from listywysd where ref = :listwysd
      into :x_opoznionepakowanie;
  if (x_opoznionepakowanie is null) then
    x_opoznionepakowanie = 0;
  --[PM] XXX koniec

  if (x_opoznionepakowanie <> 1) then --[PM] XXX OPOZNIONEPAKOWANIE , normalne pakowanie
    begin --[PM] XXX OPOZNIONEPAKOWANIE
      --sprawdzenie opakowania
      if (exists(select nropk from listywysdroz_opk where listwysd = :listwysd and ref = :aktopk and stan = 1) or
          coalesce(:aktopk, 0) = 0) then
      begin
        msg = 'Nie można dodawać towaru do zamkniętego opakowania lub brak wskazania opakowania.';
        status = 0;
        suspend;
        exit;
      end
    
      execute procedure XK_MWS_BARCODE_VALIDATION(:kodkresk)
        returning_values :ktm, :wersjaref, :ilosc, :status, :jedn, :towjednref;
    
      if (:mnoznik < 0) then mnoznik = 1;
      ilosc = :ilosc * :mnoznik;
      ilosctmp = ilosc;
    
      if (:status = 1) then
      begin
        for
          select rpozycja, rilosc
            from XK_LISTYWYSDROZ_GETPOZ(:listwysd, :wersjaref, :ilosc)
          into :pozycja, :ilosc
        do begin
          --[PM] XXX start , jesli jest mniej niz jeden to dobijam roznice
          if (:pozycja = -1 and mnoznik = 1 and ilosc > -1.00 and ilosc < 0) then
            begin
              ilosc = ilosctmp + ilosc;
              select rpozycja, rilosc
                from XK_LISTYWYSDROZ_GETPOZ(:listwysd, :wersjaref, :ilosc)
                into :pozycja, :ilosc;
            end
          --[PM] XXX koniec
              
          if (:pozycja = -1) then
            begin
              wersjaref = 0;
              status = 0;
              pozycja = 0;
              if (:ilosc is null) then
                msg = 'Brak towaru na dokumencie magazynowym!';
              else if(:ilosc < 0) then
                msg = 'Przekroczono ilość do spakowania o '||cast(abs(:ilosc) as numeric(14,2))||' sztuk!';
              else msg = 'Brak towaru na dokumencie lub przekroczono ilość do spakowania!';
              ilosc = 1;
              suspend;
              exit;
            end
     
          insert into listywysdroz(listywysd, listywysdpoz, iloscmag, opk)
            values(:listwysd, :pozycja, :ilosc, :aktopk);
    
          if (not exists(select first 1 1 from listywysdpoz where dokument = :listwysd and x_spakowano = 0)) then
          begin
            msg = 'Spakowano wszystkie pozycje z dokumentu';
            status = 2;
          end
          else
            status = 1;
        end
      end
      else
        msg = 'Brak towaru o podanym kodzie kreskowym';
  end --[PM] XXX OPOZNIONEPAKOWANIE start , odstrzelenie spakowanych kartonow
    else -- x_opoznionepakowanie = 1
      begin
        tmpstatus = null;
        select status from check_string_to_integer(:kodkresk) into :tmpstatus;
        if (coalesce(tmpstatus,0) = 0) then
          begin
            msg = 'Podaj numer opakowania';
            status = 0;
            suspend;
            exit;
          end
        listwysdroz_opk = cast(kodkresk as listywysdroz_opk_id);
        tmplistwysd = null;
        x_spakowano = null;
        select listwysd, x_spakowano
          from listywysdroz_opk
          where ref = :listwysdroz_opk 
          into :tmplistwysd, :x_spakowano;
        if (tmplistwysd is null) then
          begin
            msg = 'Nie znaleziono takiego opakowania.';
            status = 0;
            suspend;
            exit;
          end
        if (tmplistwysd is distinct from listwysd) then
          begin
            msg = 'To opakowanie nie nalezy do wybranego listu spedycyjnego.';
            status = 0;
            suspend;
            exit;
          end
        if (coalesce(x_spakowano,0) = 1) then
          begin
            msg = 'To opakowanie jest juz spakowane.';
            status = 0;
            suspend;
            exit;
          end

        update listywysdroz_opk set x_spakowano = 1 where ref = :listwysdroz_opk;

        if (not exists(select first 1 1 from listywysdroz_opk where listwysd = :listwysd and x_spakowano = 0)) then
          begin
            msg = 'Skompletowano wszystkie paczki.';
            status = 2;
          end
        else
          status = 1;

      end
  --[PM] XXX OPOZNIONEPAKOWANIE koniec
end^
SET TERM ; ^
