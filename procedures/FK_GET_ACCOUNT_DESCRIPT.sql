--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_GET_ACCOUNT_DESCRIPT(
      COMPANY integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      ACCOUNT ACCOUNT_ID)
  returns (
      ACCOUNTDESCRIPT varchar(2048) CHARACTER SET UTF8                           )
   as
declare variable K smallint;
declare variable SEP varchar(1);
declare variable LEN smallint;
declare variable TMP ACCOUNT_ID;
declare variable YEARID integer;
declare variable BKACCOUNT integer;
declare variable DESCRIPT varchar(2048);
declare variable DICTDEF integer;
declare variable DICTPOS integer;
begin
  select P.yearid, Y.synlen
    from bkperiods P
      join bkyears Y on (P.yearid = Y.yearid and P.company = Y.company)
    where P.id = :period and P.company = :company
    into :yearid, :k;

  select ref, descript
    from bkaccounts
    where yearid = :yearid and symbol = substring(:account from  1 for  :k) and company = :company
    into :bkaccount, :accountdescript;

  for
    select separator, dictdef, len
      from accstructure
      where bkaccount = :bkaccount
      order by nlevel
      into :sep, :dictdef, :len
  do begin
    if (sep <> ',') then
      k = k + 1;
    tmp = substring(account from  k + 1 for  k + len);

    execute procedure name_from_bksymbol(company, dictdef, tmp) returning_values descript, dictpos;
    k = k + len;
    if (coalesce(char_length(accountdescript),0) < 255 - 83) then -- [DG] XXX ZG119346
      accountdescript = accountdescript || '; ' || descript;
  end
  suspend;
end^
SET TERM ; ^
