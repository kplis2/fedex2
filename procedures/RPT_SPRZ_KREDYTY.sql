--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_SPRZ_KREDYTY(
      KLIENT integer,
      DATA timestamp,
      ZEROWE integer,
      PRZEKROCZONE integer)
  returns (
      LIMIT numeric(14,2),
      STROZRACH numeric(14,2),
      BEZROZRACH numeric(14,2),
      WYKORZYSTANY numeric(14,2),
      PRZEKROCZONY numeric(14,2))
   as
declare variable slodef integer;
begin
  BEZROZRACH = 0;
  WYKORZYSTANY = 0;
  PRZEKROCZONY = 0;
  if((:klient is null) or (:klient = 0)) then exit;
  select KLIENCI.limitkr from KLIENCI where REF=:KLIENT into :limit;
  select REF from SLODEF where TYP='KLIENCI' into :slodef;
  if(:slodef is null)  then exception slodef_bezpklienci;
  if(:limit is null) then limit = 0;
  if(:limit = 0 and :zerowe = 0) then exit;
  select sum(ROZRACHP.winienzl- ROZRACHP.mazl)
    from ROZRACH join ROZRACHP on (rozrachp.SLODEF = ROZRACH.SLODEF and rozrachp.SLOPOZ = rozrach.SLOPOZ )
    where ROZRACH.SLOPOZ = :klient AND ROZRACH.SLODEF = :slodef
    and ROZRACH.DATAOTW <= :DATA
    and rozrachp.data <=:DATA
/*    group by ROZRACHP.klient, ROZRACHp.symbfak*/
 into :strozrach;
 if(:strozrach is null) then strozrach = 0;
 select sum(NAGFAK.dozaplaty*NAGFAK.KURS) from NAGFAK left join ROZRACH on(ROZRACH.FAKTURA = NAGFAK.REF)
   where NAGFAK.klient =:klient and NAGFAK.ZAKUP = 0 and ROZRACH.FAKTURA is null and
         NAGFAK.DATA <=:DATA
 into :bezrozrach;
 if(:bezrozrach is null) then bezrozrach = 0;
 wykorzystany = :bezrozrach + :strozrach;
 przekroczony = :wykorzystany - :limit;
 if(:przekroczony < 0) then przekroczony = 0;
 if(:przekroczony = 0 and :przekroczone = 1) then exit;
 suspend;
end^
SET TERM ; ^
