--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CALCULATE_ACCORDPOINTS(
      EMPLOYEE integer,
      BDATE timestamp,
      EDATE timestamp)
  returns (
      POINTSPERH numeric(14,2))
   as
declare variable worktime numeric(14,2);
declare variable sumpoints numeric(14,2);
begin
  select sum(epresencelistnag.jobtimeq)
    from epresencelistnag
    where epresencelistnag.employee = :employee
      and cast(epresencelistnag.startat as date) >= cast(:bdate as date)
      and cast(epresencelistnag.stopat as date) <= cast(:edate as date)
    into :worktime;
  select sum(econtrpayrollspos.points)
    from econtrpayrollspos
      join econtrpayrollsnag on (econtrpayrollspos.econtrpayrollnag = econtrpayrollsnag.ref)
    where econtrpayrollsnag.employee = :employee
      and cast(econtrpayrollspos.sdate as date) >= cast(:bdate as date)
      and cast(econtrpayrollspos.sdate as date) <= cast(:edate as date)
    into :sumpoints;
  if (:sumpoints is null) then
    sumpoints = 0;
  if (:worktime is not null and :worktime <> 0) then
    pointsperh = :sumpoints/:worktime;
  else
    pointsperh = 0;
  update econtractsassign set econtractsassign.accordpointsperh = :pointsperh
    where econtractsassign.employees = :employee;
end^
SET TERM ; ^
