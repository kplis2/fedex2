--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_WHSECROWSDIST_CALCULATE(
      WHSEC integer,
      W numeric(14,2),
      NUMBER integer)
  returns (
      COORDXB numeric(14,2),
      COORDXE numeric(14,2))
   as
declare variable mwseccoordxb numeric(14,2);
begin
  if (number is not null and number = 1) then
  begin
    select coordxb from whsecs where ref = :whsec into mwseccoordxb;
    coordxb = mwseccoordxb;
    coordxe = mwseccoordxb + w;
  end else if (number is not null and number > 1) then
  begin
    select coordxb from whsecs where ref = :whsec into mwseccoordxb;
    select sum(w) from whsecrows where number < :number and whsec = :whsec
      into coordxb;
    if (coordxb is null) then coordxb = 0;
    coordxb = coordxb + mwseccoordxb;
    coordxe = coordxb + w;
  end else begin
    select coordxb from whsecs where ref = :whsec into mwseccoordxb;
    coordxb = mwseccoordxb;
    coordxe = mwseccoordxb;
  end
end^
SET TERM ; ^
