--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ERECCANDCOMPRATING(
      RECCAND_REF integer)
   as
DECLARE VARIABLE CAND_REF INTEGER;
DECLARE VARIABLE RECPOST_REF INTEGER;
DECLARE VARIABLE ATTR_REF INTEGER;
DECLARE VARIABLE ATTR_WEIGHT FLOAT;
DECLARE VARIABLE ATTR_MIN FLOAT;
DECLARE VARIABLE ATTR_OPT FLOAT;
DECLARE VARIABLE ATTR_DEF FLOAT;
DECLARE VARIABLE ATTR_MAX FLOAT;
DECLARE VARIABLE RATING_SUM FLOAT;
DECLARE VARIABLE RATING FLOAT;
begin
  rating_sum = 0;

  select RECPOST, CANDIDATE from ereccands
  where ref = :reccand_ref
  into :recpost_ref, cand_ref;

  for select ATTRIBUTE, WEIGHT from erecattr
      where RECPOST = :recpost_ref
      into :attr_ref, :attr_weight do
  begin
    select MINIM, OPTIM, MAXIM, DEFAU from edictrecattr
    where REF =  :attr_ref
    into attr_min, attr_opt, attr_max, attr_def;

    rating = null;
    select RATING from ecandidattr
    where ATTRIBUTE = :attr_ref and CANDIDATE = :cand_ref
    into :rating;

    if( :rating is null ) then rating = attr_def;

    if ( :rating > 0 and rating>=attr_min and rating<=attr_max) then
    begin
      if (rating>attr_opt) then
        rating_sum = rating_sum + (rating-attr_max)/(attr_opt-attr_max)*:attr_weight;
      else /* (rating<=attr_opt) */
        if( attr_opt = attr_min ) then
          rating_sum = rating_sum + :attr_weight;
        else
          rating_sum = rating_sum + ((rating-attr_min)/(attr_opt-attr_min))*:attr_weight;
      end
    else
    begin
      if ((rating<attr_min and attr_opt=attr_min) or (rating>attr_max and attr_opt=attr_max)) then
        rating_sum = rating_sum + :attr_weight;
    end
  end
  /* zapisuje laczna ocene */
  update ereccands set RATING = :rating_sum where REF = :reccand_ref;

end^
SET TERM ; ^
