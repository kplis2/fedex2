--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_REFRESH_TEMPTABLE(
      TABLENAME varchar(255) CHARACTER SET UTF8                           ,
      PROCEDURESQL varchar(1024) CHARACTER SET UTF8                           ,
      FIELDS varchar(1024) CHARACTER SET UTF8                            = null)
   as
declare variable triggername varchar(255);
declare variable procname varchar(255);
declare variable f varchar(255);
declare variable sql varchar(2048);
declare variable p1 integer;
declare variable initfield smallint;
begin
  if(:fields is null or :fields='') then begin
    -- jesli nie okreslono listy pol to okreslamy automatycznie
    -- jako wsystkie pola zwracane przez procedure
    p1 = position('(' in :proceduresql)-1;
    if(:p1>0) then procname = upper(substring(:proceduresql from 1 for :p1));
    else procname = upper(:proceduresql);
    fields = '';
    for select trim(cast(rdb$parameter_name as varchar(255)))
    from rdb$procedure_parameters
    where rdb$procedure_name=:procname and rdb$parameter_type=1
    order by rdb$parameter_number
    into :f
    do begin
      if(:fields<>'') then fields = :fields || ', ';
      fields = :fields || :f;
    end
    if(:fields='') then exception universal 'Nie znaleziono listy pol w wyrazeniu wypelniajacym';
  end
  fields = upper(:fields);
  -- sprawdzamy czy w tabeli tymczasowej jest specjalne pole INITIALIZING
  if(exists(select first 1 1 from rdb$relation_fields where rdb$relation_name = :tablename and rdb$field_name='INITIALIZING')) then initfield = 1;
  else initfield = 0;
  -- rozpoczecie inicjalizacji, ustawiamy pole INITIALIZING na 1
  if(:initfield=1) then begin
    sql = 'update '||:tablename||' set INITIALIZING = 1 where INITIALIZING = 0 or INITIALIZING is null';
    execute statement :sql;
  end

  -- czyszczenie tabeli tymczasowej
  sql = 'delete from '||:tablename;
  execute statement :sql;

  -- uzupelnianie tabeli z procedury
  sql = 'insert into '||:tablename||' ('||:fields||') select '||:fields||' from '||:proceduresql;
  execute statement :sql;

  -- zakonczenie inicjalizacji
  if(:initfield=1) then begin
    sql = 'update '||:tablename||' set INITIALIZING = 0 where INITIALIZING = 1';
    execute statement :sql;
  end
end^
SET TERM ; ^
