--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMPLCONTRACTS_CURRENTS_UPDATE(
      EMPLCONTRREF integer)
   as
declare variable CDATE timestamp;
declare variable CBRANCH varchar(10);
declare variable CDEPARTMENT varchar(10);
declare variable CWORKPOST integer;
declare variable CWORKDIM float;
declare variable CSALARY numeric(14,2);
declare variable CCADDSALARY numeric(14,2);
declare variable CFUNCSALARY numeric(14,2);
declare variable CWORKTYPE integer;
declare variable CEMPLGROUP integer;
declare variable CWORKSYSTEM integer;
declare variable CEPAYRULE integer;
begin
/*MWr: Procedura dla danej umowy EMPLCONTRREF ustawia obowiazujace dla niej, na dzien
  biezacy, wartosci. Pierwotne wartosci umowy mogly zostac zmienione przez wystawione
  aneksy do umowy. Procedura zastepuje wczesniej istniejaca EMPLCONTRACTS_UPDATE (PR24342)*/

  select max(fromdate) from econtrannexes
    where emplcontract = :emplcontrref
      and fromdate <= current_date
      and (todate >= current_date or todate is null)
    into :cdate;

  if (cdate is null) then
    select branch, department, workpost, workdim, salary, caddsalary,
        funcsalary, emplgroup, worksystem, epayrule
      from emplcontracts
      where ref = :emplcontrref
      into :cbranch, :cdepartment, :cworkpost, :cworkdim, :csalary, :ccaddsalary,
        :cfuncsalary, :cemplgroup, :cworksystem, :cepayrule;
  else
    select branch, department, workpost, workdim, salary, caddsalary,
        funcsalary, emplgroup, worksystem, epayrule
      from econtrannexes
      where emplcontract = :emplcontrref and fromdate = :cdate
      into :cbranch, :cdepartment, :cworkpost, :cworkdim, :csalary, :ccaddsalary,
        :cfuncsalary, :cemplgroup, :cworksystem, :cepayrule;

  select worktype from edictworkposts
    where ref = :cworkpost
    into :cworktype;

  update emplcontracts
    set cbranch = :cbranch, cdepartment = :cdepartment, cworkpost = :cworkpost,
      cworkdim = :cworkdim, csalary = :csalary, ccaddsalary = :ccaddsalary,
      cfuncsalary = :cfuncsalary, cworktype = :cworktype, cemplgroup = :cemplgroup,
      cworksystem = :cworksystem, cepayrule = :cepayrule
    where ref = :emplcontrref
      and (:cbranch is distinct from cbranch or :cdepartment is distinct from cdepartment
        or :cworkpost is distinct from cworkpost or :cworkdim is distinct from cworkdim
        or :csalary is distinct from csalary or :ccaddsalary is distinct from ccaddsalary
        or :cfuncsalary is distinct from cfuncsalary or :cworktype is distinct from cworktype
        or :cemplgroup is distinct from cemplgroup or :cworksystem is distinct from cworksystem
        or :cepayrule is distinct from cepayrule
    );
end^
SET TERM ; ^
