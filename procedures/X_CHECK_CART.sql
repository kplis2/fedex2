--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_CHECK_CART(
      LISTWYSD LISTYWYSD_ID,
      CART STRING20)
  returns (
      IS_ALL_CHECK SMALLINT_ID,
      STATUS SMALLINT_ID,
      MSG STRING1024)
   as
  declare variable cartref mwsconstlocs_id;
  declare variable gooddoc smallint_id;
begin
  --procedura sluzy do zanotowania w tabeli MWSORDCARTCOLOURS ze pakowacz zeskanowal wozek
  -- zwraca tez informacje czy zostaly jeszcze jakies wozki do zeskanowania
  --arguments:
  --listwysd ref dokumentu spedycyjnego z tabeli listywysd
  --cart symbol wozka
  --returns:
  --is_all_check 1 - wszystkie wozki zeskanowane 0 - jeszcze nie wszystkie wozki zeskanowane
  --status  1 - bez bledu 0 - blad
  --msg komunikat bledu
  is_all_check = 0;
  status = 1;
  msg = '';
  --sprawdzenie poprawnosci danych
  if(listwysd is null ) then
  begin
    status = 0;
    msg = 'Nie podano numeru listu spedycyjnego.';
    exit;
  end

  if(coalesce(cart,'') = '' ) then
  begin
    status = 0;
    msg = 'Nie podano numeru symbolu wózka.';
    exit;
  end
  
  --pobranie ref wozka
  select ref
    from mwsconstlocs ms
    where ms.symbol = :cart
  into :cartref;
  
  if (cartref is null) then
  begin
    status = 0;
    msg = 'Nie ma takiego wózka w systemie.';
    exit;  
  end

  -- czy wozek pobrany do dobrego dokumentu
  select first 1 1
    from listywysd ld
      join dokumnag dg on ld.refdok = dg.grupasped
      join mwsordcartcolours mc on dg.ref = mc.docid
     where ld.ref = :listwysd
       and mc.cart = :cartref
  into gooddoc;

  if (gooddoc is null) then
  begin
    status = 0;
    msg = 'Wózek '|| :cart || ' nie jest przypisany do tego dokumentu.';
    exit;
  end
  --zaznaczenie zeskanowania wozka
  update mwsordcartcolours mc
    set mc.x_check_on_pack = 1
    where mc.cart = :cartref
      and mc.status < 2
       and mc.docid in ( select dg.ref
                          from dokumnag dg
                            join listywysd ld on ld.refdok = dg.grupasped
                          where ld.ref = :listwysd ) ;
  --sprawdzenie czy istnieja niezeskanowane wozki
  if (exists(
        select 1
          from mwsordcartcolours mc
            join dokumnag dg on dg.ref = mc.docid
            join listywysd ld on ld.refdok = dg.grupasped
            where ld.ref = :listwysd
              and coalesce(mc.x_check_on_pack, 0) = 0
              and mc.status < 2)) then
  begin
    is_all_check = 0;
  end
  else
    is_all_check = 1;
end^
SET TERM ; ^
