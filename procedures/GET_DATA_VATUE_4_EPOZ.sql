--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_DATA_VATUE_4_EPOZ(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      COMPANY COMPANIES_ID,
      OLD_EDECLDEFREF EDECLARATIONS_ID = 0,
      POZROWMIN SMALLINT_ID = 0,
      TESTPOZROWCOUNT SMALLINT_ID = 0)
  returns (
      NUMER INTEGER_ID,
      A_OLD STRING2,
      B_OLD varchar(15) CHARACTER SET UTF8                           ,
      C_OLD INTEGER_ID,
      D_OLD SMALLINT_ID,
      A STRING2,
      B varchar(15) CHARACTER SET UTF8                           ,
      C INTEGER_ID,
      D SMALLINT_ID)
   as
begin
/*TS: FK - pobiera dane dla wydruku.

  > POZROWMIN - procedura zwroci nie mniej niz dana liczbe wierszy.
  > TESTPOZROWCOUNT - liczba wierszy z testowymi danymi, ktore zostana
    wygenerowane dla wskazanej sekcji dokumentu i zwrocone razem z
    danymi zwroconymi z zapytania.
*/

  numer = 0;

  if (old_edecldefref = 0) then
  begin
    for
      select
          k.krajid, trim(replace(replace(k.nip,' ',''),'-','')), round(sum(bv.netv)), null
        from bkdocs b
        join vatregs v on (v.symbol = b.vatreg and v.company = b.company)
        left join klienci k on (k.ref = b.dictpos)
        join bkvatpos bv on (bv.bkdoc = b.ref)
        where v.vtype = 1 and bv.vatgr = 'NP'
          and b.status > 0
          and b.vatperiod = :period
          and b.company = :company
        group by k.krajid, k.nip
        into :a, :b, :c, :d
    do begin
       if (:b starting with :a) then
       begin
         b = replace(:b, :a, '');
       end
      numer = numer + 1;
      suspend;
    end
   end else if (exists(select first 1 1 from edeclarations e
                         join edecldefs ed on (e.edecldef = ed.ref)
                         where e.ref = :old_edecldefref
                           and ed.variant = 3
                           and ed.systemcode starting with 'VAT-UE')) then
   begin
    for
     select coalesce(epa.pvalue, jest.a), coalesce(epb.pvalue, jest.b),
         coalesce(epc.pvalue, 0), coalesce(epd.pvalue, 1),
         jest.a, jest.b, round(jest.c), jest.d
       from edeclpos epb
       join edeclpos epc on (epb.ref = epc.ref - 1 and epc.edeclaration = :old_edecldefref)
       join edeclpos epd on (epb.ref = epd.ref - 2 and epd.edeclaration = :old_edecldefref)
       join edeclpos epa on (epb.ref = epa.ref + 1 and epa.edeclaration = :old_edecldefref)
       right join (
         select
             k.krajid as a, 
               trim(replace(replace(iif(k.nip starting with k.krajid, replace(k.nip,k.krajid,''),k.nip),' ',''),'-','')) as b,
               sum(bv.netv) as c, 1 as d
          from bkdocs b
          join vatregs v on (v.symbol = b.vatreg and v.company = b.company)
          left join klienci k on (k.ref = b.dictpos)
          join bkvatpos bv on (bv.bkdoc = b.ref)
          where v.vtype = 1 and bv.vatgr = 'NP'
            and b.status > 0
            and b.vatperiod = :period
            and b.company = :company
          group by k.krajid, k.nip
       ) jest on (epb.pvalue = jest.b and epb.edeclaration = :old_edecldefref)
         into :a_old, :b_old, :c_old, :d_old,
           :a, :b, :c, :d
     do begin
       if (:c_old <> :c or :d_old <> :d) then
       begin
         numer = numer + 1;
         suspend;
       end
     end
   end else
   begin
    for
     select coalesce(bylo.a, jest.a), coalesce(bylo.b, jest.b), 
         coalesce(bylo.c, 0), coalesce(bylo.d, 1),
         jest.a, jest.b, round(jest.c), jest.d
       from ede_vatue_4_poz(:old_edecldefref, 'D') bylo
       right join (
         select
             k.krajid as a, 
               trim(replace(replace(iif(k.nip starting with k.krajid, replace(k.nip,k.krajid,''),k.nip),' ',''),'-','')) as b,
               sum(bv.netv) as c, 1 as d
          from bkdocs b
          join vatregs v on (v.symbol = b.vatreg and v.company = b.company)
          left join klienci k on (k.ref = b.dictpos)
          join bkvatpos bv on (bv.bkdoc = b.ref)
          where v.vtype = 1 and bv.vatgr = 'NP'
            and b.status > 0
            and b.vatperiod = :period
            and b.company = :company
          group by k.krajid, k.nip
       ) jest on (bylo.a = jest.a
         and bylo.b = jest.b)
         into :a_old, :b_old, :c_old, :d_old,
           :a, :b, :c, :d
     do begin
       if (:c_old <> :c or :d_old <> :d) then
       begin
         numer = numer + 1;
         suspend;
       end
     end
   end

  --/*
  --Dane testowe
  while(numer < :testpozrowcount) do
  begin
    if (old_edecldefref = 0) then
    begin
      a_old = 'FR';
      b_old = 31111111 + :numer;
      c_old = 300 * :numer;
      d_old = mod(rand(),2) + 1;
      a = a_old;
      b = b_old;
      c = c_old + 1;
      d = d_old;
      numer = numer + 1;
      suspend;
    end else
    begin
      for
        select first (:testpozrowcount - :numer) skip (:numer)
           bylo.a, bylo.b, bylo.c, bylo.d
         from ede_vatue_4_poz(:old_edecldefref, 'E') bylo
         into :a_old, :b_old, :c_old, :d_old
      do begin
        a = a_old;
        b = b_old;
        c = c_old + 1;
        d = d_old;
        numer = numer + 1;
        suspend;
      end
      numer = :testpozrowcount;
    end
  end
  --*/

  a_old = '';
  b_old = '';
  c_old = 0;
  d_old = null;
  a = '';
  b = '';
  c = 0;
  d = null;

  while(numer < :pozrowmin) do
  begin
    numer = numer + 1;
    suspend;
  end
end^
SET TERM ; ^
