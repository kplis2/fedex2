--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CQUEUES_SYNC(
      QUEUEID STRING,
      NAME STRING)
   as
begin
  update or insert into CQUEUES(queueid,name) values (:queueid,:name)
  matching (queueid);
end^
SET TERM ; ^
