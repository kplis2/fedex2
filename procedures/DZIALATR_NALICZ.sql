--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DZIALATR_NALICZ(
      ARTYKUL integer,
      USUN integer)
   as
declare variable klasa integer;
declare variable dzial integer;
declare variable cnt2 integer;
declare variable dref integer;
declare variable dnazwa varchar(40);
declare variable dwartosc varchar(4096);
declare variable dnumer integer;
begin
  select klasa,dzial from artykuly where ref=:artykul into :klasa, :dzial;
  if(:klasa is null) then select klasaa from dzialy where ref = :dzial into :klasa;
  if(:usun=1) then delete from dzialatr where artykul = :artykul;
  if(:klasa is not null) then begin
    for select REF,NAZWA,WARTOSC, numer from DEFDZIALATR
      where DEFDZIALATR.KLASA=:klasa AND DEFDZIALATR.NAGART=0
      into :dref, :dnazwa, :dwartosc, :dnumer
    do begin
      select count(*) from dzialatr where artykul=:artykul and nazwa=:dnazwa into :cnt2;
      if(:cnt2=0) then insert into dzialatr(artykul,nazwa,wartosc,numer,defdzialatr) values(:artykul,:dnazwa,:dwartosc,:dnumer,:dref);
    end
  end
  for select REF,NAZWA,WARTOSC, numer from DEFDZIALATR
    where DEFDZIALATR.DZIAL=:dzial AND DEFDZIALATR.NAGART=0
    into :dref, :dnazwa, :dwartosc, :dnumer
  do begin
    select count(*) from dzialatr where artykul=:artykul and nazwa=:dnazwa into :cnt2;
    if(:cnt2=0) then insert into dzialatr(artykul,nazwa,wartosc,numer,defdzialatr) values(:artykul,:dnazwa,:dwartosc,:dnumer,:dref);
  end
end^
SET TERM ; ^
