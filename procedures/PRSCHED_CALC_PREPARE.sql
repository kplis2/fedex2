--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_CALC_PREPARE(
      SCHEDULE integer,
      SCHEDZAM integer,
      GUIDE integer,
      SCHEDOPER integer,
      ZAKRES integer,
      CLEAROPER smallint,
      ONLYWAITINGGUIDE smallint)
  returns (
      OPERCOUNT integer)
   as
declare variable curnum integer;
declare variable sql varchar(255);
declare variable schednum integer;
declare variable guidestatus integer;
begin
  guidestatus = 10;
  if(:onlywaitingguide = 1) then
    guidestatus = 0;
  --przygotowanie do naliczania
  if(:zakres < 3) then begin-- 3 oznacza kontynuacje poprzedniego
    update PRSCHEDOPERS set prschedopers.verified = 1 where  verified = 0;--zaznaczenie, ze wszystkieoepracje sa zweryfikowae
  end
  --jesli jakims przypadkiem operacje zrealizowane maja czasy w  rozpoczecia przyszlosci, to naprawienie tego - ustawieni im czasow null'owych
  update PRSCHEDOPERS set  STARTTIME = NULL where
    starttime > current_timestamp(0) and status > 2;
  update PRSCHEDOPERS set  ENDTIME = current_timestamp(0)
    where
    starttime < current_timestamp(0) and
    endtime > current_timestamp(0) and
    status > 2;
  if(:zakres < 4) then begin
  if(:schedoper > 0) then begin
    select schedule, schedzam, guide, number from PRSCHEDOPERS where  PRschedopers.ref=:schedoper into :schedule, :schedzam, :guide, :curnum;
    update PRSCHEDOPERS set VERIFIED = 0, CALCDEPTIMES = 0, TIMECONFLICT = 0
      where schedzam = :schedzam and guide = :guide
      and (status = 0 or (status = 1) or (status = 2 ) )
      and guidestatus <= :guidestatus and schedblocked = 0
      and activ = 1
      and ((:zakres = 0 and PRSCHEDOPERS.number = :curnum)
          or(:zakres = 1 and PRSCHEDOPERS.number >= :curnum)
          or(:zakres = 2 and PRSCHEDOPERS.number <= :curnum)
          or(:zakres = 3)
          );
  end else if(:guide > 0) then begin
    select prschedule, prschedzam, numer from PRSCHEDGUIDES where  PRSCHEDGUIDES.ref=:guide into :schedule, :schedzam, :curnum;
    update PRSCHEDOPERS set VERIFIED = 0, CALCDEPTIMES = 0, TIMECONFLICT = 0
      where schedule = :schedule and schedzam = :schedzam
      and (status = 0 or (status = 1) or (status = 2) )
      and guidestatus <= :guidestatus and schedblocked = 0
      and activ = 1
      and ( (:zakres = 0 and PRSCHEDOPERS.guidenum = :curnum)
          or(:zakres = 1 and PRSCHEDOPERS.guidenum >= :curnum)
          or(:zakres = 2 and PRSCHEDOPERS.guidenum <= :curnum)
          or(:zakres = 3)
          );
  end else if(:schedzam > 0) then begin
    select prschedule,  number from PRSCHEDZAM where  PRSCHEDZAM.ref=:schedzam into :schedule, :curnum;
    update PRSCHEDOPERS set VERIFIED = 0, CALCDEPTIMES = 0 , TIMECONFLICT = 0
      where schedule = :schedule
      and (status = 0 or (status = 1) or (status = 2) )
      and activ = 1
      and guidestatus <= :guidestatus and schedblocked = 0
      and ( (:zakres = 0 and PRSCHEDOPERS.schedzamnum = :curnum)
          or(:zakres = 1 and PRSCHEDOPERS.schedzamnum >= :curnum)
          or(:zakres = 2 and PRSCHEDOPERS.schedzamnum <= :curnum)
          or(:zakres = 3)
          );
  end else if(:schedule > 0) then begin
  end
  end else begin
    --kontunuacja poprzedniego - sprawdzenie, czy nie wchodzi na cudzy harmonogram
    if(:schedule > 0) then
      update PRSCHEDOPERS set prschedopers.verified = 1 where  verified = 0 and schedule <> :schedule;--zaznaczenie, ze wszystkieoepracje sa zweryfikowae

  end
  --operacje w trakcie realizacji mają planowane zakonczenie najwczesniej na moment bieżacy (wczesniej niz TERAZ sie  nei zakoncza)
  --dodaje dodatkowe 10 minut, by nie bylo aktualizacji co kazde naliczenie - to trwa
--  update PRSCHEDOPERS set ENDTIME = (current_timestamp(0) + cast(1 as double precision)/24/6)  where schedule = :schedule and status = 2 and verified > 0
--    and (endtime < current_timestamp(0) or (endtime is null));
/*  for select REF from PRSCHEDZAM
    where PRSCHEDZAM.prschedule = :schedule
      and ( (:zakres = 0 and prschedzam.number = :curnum)
          or(:zakres = 1 and prschedzam.number >= :curnum)
          or(:zakres = 2)
          )
    into :schedzam
  do begin
    UPDATE prschedopers set VERIFIED = 0, CALCDEPTIMES = 0
      where SCHEDZAM = :schedzam
      and (:guide = 0 or (prschedopers.guide = :guide))
      and (status = 0 or (status = 1) or (status = 2 and starttime is null) )
      and prschedopers.number > :schednum;
  end*/
  --wyczyszczenie danych
  if(:clearoper = 1)then begin
      update PRSCHEDOPERS set STARTTIME = null, ENDTIME = null,
        --MACHINE = (case when status < 2 then null else machine end),--jesli w trakcie realizacji, to juz nei kasuje maszyny
        MACHINE = null,
        MATERIALREADYTIME = null, STATUSMAT = null,
        status = (case when status < 2 then 0 else status end)--zmiana statusu
--        schedzamnum = (case when status = 2 then 0 else schedzamnum end)--zmiana numeru na 0 dla "w trakcie realizacji" harmonogramowane z pierwszym priorytetem
      where
        SCHEDULE = :schedule and VERIFIED = 0;
  end
  --blokada wyliczania czasuow zakocznienai i rozpoczecia do przewodnikow
  select count(*) from PRSCHEDOPERS where schedule = :schedule and VERIFIED = 0 into :opercount;
end^
SET TERM ; ^
