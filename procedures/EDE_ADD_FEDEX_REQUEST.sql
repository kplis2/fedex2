--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_ADD_FEDEX_REQUEST(
      SHIPPINGDOC integer,
      EDERULESYMB varchar(40) CHARACTER SET UTF8                           ,
      ADDPARAM STRING20 = null)
  returns (
      EDEDOCHREF integer)
   as
declare variable EDERULE integer;
declare variable SPEDRESPONSE varchar(255);
declare variable OTABLE varchar(31);
declare variable OREF integer;
declare variable SHIPPINGDOCNO varchar(20);
begin
  if (:ederulesymb like 'EDE_FEDEX_ORDER%') then
  begin
    select ref from listywys where ref = :shippingdoc
    into :oref;

    if (:oref is null) then
      exception ede_fedex 'Brak listy spedycyjnej.';

    otable = 'LISTYWYS';
  end
  else
  begin
    select l.ref, substr(l.symbolsped,1,20), l.spedresponse from listywysd l where l.ref = :shippingdoc
    into :oref, :shippingdocno, :spedresponse;
    if (:oref is null) then
      exception ede_fedex 'Brak listy spedycyjnej.';
    if (:oref is not null and coalesce(:shippingdocno,'') = '' and :ederulesymb not in ('EDE_FEDEX_SHIPMENT', 'EDE_FEDEX_ADDCUSTOMER')) then
      exception ede_fedex 'Dokument nie został jeszcze nadany';
    if (coalesce(:spedresponse,'') = '1' and :ederulesymb = 'EDE_FEDEX_SHIPMENT') then
      exception universal 'Dokument został już wysłany. Anuluj wysyłkę i nadaj ją jeszcze raz.';

    otable = 'LISTYWYSD';
  end

  select es.ref
    from ederules es
    where es.symbol = :ederulesymb
    into :ederule;

  insert into ededocsh( ederule, direction, createdate, otable, oref, status, filename, manualinit, symboldok)
    values(:ederule, 1, current_timestamp, :otable, :oref, 0, :shippingdoc||'exp', 0, :addparam)
    returning ref into :ededochref;

  suspend;
end^
SET TERM ; ^
