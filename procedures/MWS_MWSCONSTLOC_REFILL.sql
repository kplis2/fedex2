--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_MWSCONSTLOC_REFILL(
      MWSORDTYPE integer,
      MWSCONSTLOCP integer,
      MWSCONSTLOCL integer,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      PRIORITY smallint,
      MAXCNT integer,
      VERSIN integer = 0)
  returns (
      MWSORD integer)
   as
declare variable WH varchar(3);
declare variable OPERATOR integer;
declare variable OPERATORDICT varchar(80);
declare variable BLOCKSYMBOL varchar(40);
declare variable TIMESTART timestamp;
declare variable TIMESTARTDCL timestamp;
declare variable TIMESTOPDCL timestamp;
declare variable REFMWSORD integer;
declare variable MWSACCESSORY integer;
declare variable PERIOD varchar(6);
declare variable BRANCH varchar(10);
declare variable MIX smallint;
declare variable GOOD varchar(40);
declare variable PALTYPE varchar(40);
declare variable VERS integer;
declare variable WHAREALOGL integer;
declare variable WHAREALOGP integer;
declare variable WHAREAP integer;
declare variable WHAREAL integer;
declare variable MAXNUMBER integer;
declare variable QUANTITY numeric(14,4);
declare variable REALTIME double precision;
declare variable MWSPALLOCLP integer;
declare variable MWSPALLOCL integer;
declare variable DIST numeric(14,4);
declare variable STOCKTAKING smallint;
declare variable REC smallint;
declare variable COR smallint;
declare variable MWSPALLOCP integer;
declare variable MWSACTREF integer;
declare variable LOT integer;
declare variable LEVELNUMP integer;
declare variable LEVELNUML integer;
declare variable OPERATORTMP integer;
declare variable MIXTAKEPAL integer;
declare variable MIXLEAVEPAL integer;
declare variable MIXMWSCONSTLOCL integer;
declare variable MIXMWSPALLOCL integer;
declare variable PALL numeric(14,2);
declare variable PALW numeric(14,2);
declare variable PALH numeric(14,2);
declare variable MIXREFILL smallint;
declare variable ACTCNT integer;
declare variable DEEPL smallint;
declare variable REFILL smallint;
declare variable MWSCONSTLOCLSYMB varchar(40);
declare variable cnt integer;
declare variable tmpmwsconstlocl integer;
declare variable x_partia date_id;              -- XXX KBI
declare variable x_serial_no integer_id;        -- XXX KBI
declare variable x_slownik integer_id;          -- XXX KBI
declare variable genfullconstloc smallint_id;  -- XXX KBI
begin
  rec = 0;
  cor = 0;
  stocktaking = 0;
  select wh from mwsconstlocs where ref = :mwsconstlocp into wh;
  select first 1 o.ref, count(a.ref)
    from mwsords o
      left join mwsacts a on (a.mwsord = o.ref)
    where o.mwsordtype = :mwsordtype and o.status = 1 and o.wh = :wh
      and a.mwsconstlocp =  :mwsconstlocp -- xxx kbi
    group by o.ref
    having count(a.ref) < :maxcnt
    order by count(a.ref)
    into refmwsord, cnt;
  if (cnt is null) then cnt = 0;
  if (refmwsord is null) then
  begin
    execute procedure gen_ref('MWSORDS') returning_values refmwsord;
    select oddzial from defmagaz where symbol = :wh into branch;
    select okres from datatookres(current_date,0,0,0,0) into period;
    insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator, priority,
        description, wh,  regtime, timestartdecl, branch, period,
        flags, docsymbs, cor, rec, status, shippingarea, SHIPPINGTYPE, takefullpal,
        slodef, slopoz, slokod, palquantity, repal)
      values (:refmwsord, :mwsordtype, 0, 'U', null, null, null, 1,
          '', :wh, current_timestamp(0), null, :branch, :period,
          '', '', 0, 0, 0, null, null, 0,
          null, null, 'MWS_MWSCONSTLOC_REFILL', null, 0);
  end
  -- POZYCJE ZLECENIA MAGAZYNOWEGO
  /* XXX KBI Ponieważ lokacji docelowej (planowanej) bede szuka dopiero podczas genrowania MG
  if (mwsconstlocl is null) then
  begin
    select count(ref) from mwsstock where mwsconstloc = :mwsconstlocp and quantity > 0
      into :mix;
    select first 1 m.good
      from mwsstock m
        left join towary t on (t.ktm = m.good)
      where t.paleta = 1 and m.mwsconstloc = :mwsconstlocp
      into paltype;
    select first 1 m.good, m.vers, mc.l, mc.w, mc.h
      from mwsstock m
        join mwsconstlocs mc on (mc.ref = m.mwsconstloc)
        left join towary t on (t.ktm = m.good)
      where t.paleta <> 1 and m.mwsconstloc = :mwsconstlocp
      into good, vers, pall, palw, palh;
    -- sprawdzamy czy przesuwamy mixa
    if (mix > 2) then mix = 1; else mix = 0;
    execute procedure XK_MWS_GET_BEST_LOCATION_3(:GOOD ,:VERS ,null, :REFMWSORD, :MWSORDTYPE,
        :WH, null, null, 6, 1, 0, :MIX, null, :PALTYPE, :PALW, :PALL, :PALH, null, 0)
      returning_values (mwsconstlocl, mwspallocl, whareal, wharealogl, refill);
  end
  else
  begin
    select wharea, wharealogfromstartarea from mwsconstlocs where ref = :mwsconstlocl
      into whareal, wharealogl;
  end
  */
  select wharea, wharealogfromstartarea from mwsconstlocs where ref = :mwsconstlocp
    into whareap, wharealogp;
  maxnumber = 0;
--  if (mwsconstlocl is not null) then    -- XXX KBI
--  begin                                 -- XXX KBI
    select first 1 c.ref
      from mwsconstlocs c
        left join mwsconstloctypes t on (t.ref = c.mwsconstloctype)
      where c.wh = :wh and t.locdest = 4
      into tmpmwsconstlocl;
    select symbol from mwsconstlocs where ref = :mwsconstlocl into MWSCONSTLOCLSYMB;
    operatortmp = null;

    -- XXX kbi ZG127278 czy na WG generować caly stan lokacji czy tylko wybrany towar
    execute procedure get_config('X_MWS_WG_FULL_CONSTL',2) returning_values genfullconstloc;
     if (genfullconstloc is null) then genfullconstloc = 0;
     if (genfullconstloc = 1) then
      vers = null;
     -- XXX koniec kbi ZG127278
    for
      select m.good, m.vers, m.quantity, m.mwspalloc,
          mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, mixrefill,
          x_partia, x_serial_no, x_slownik    -- XXX KBI
        from mwsstock m
        where mwsconstloc = :mwsconstlocp and quantity > 0 and (vers = :versin or coalesce(:versin,0) = 0) and (m.x_blocked<>1) --XXX Ldz ZG 126909
        into good, vers, quantity, mwspallocp,
          mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, mixrefill,
          :x_partia, :x_serial_no, :x_slownik -- XXX KBI
    do begin
      select mwsstandlevelnumber
        from mwsconstlocs
        where ref = :mwsconstlocp
      into levelnump;
      select mwsstandlevelnumber
        from mwsconstlocs
        where ref = :mwsconstlocl
      into levelnuml;
      if (levelnump is null) then levelnump = 0;
      if (levelnuml is null) then levelnuml = 0;
      maxnumber = maxnumber + 1;
      execute procedure gen_ref('MWSACTS') returning_values mwsactref;
      insert into mwsacts (ref,mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
          planmwsconstlocl,mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
          regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, recdoc, plus,
          whareap, whareal, wharealogp, wharealogl, number, disttogo, mixedpallgroup, palgroup,
          mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, refilltry,
          x_partia, x_serial_no, x_slownik) -- XXX KBI
        values (:mwsactref,:refmwsord, 0, :stocktaking, :good, :vers, :quantity, :mwsconstlocp,  :mwspallocp,
            :mwsconstlocl,:tmpmwsconstlocl, :mwspallocp, null, null, 'O', 0, 0, :wh, :whareap, null,
            current_timestamp(0), :timestartdcl, :timestopdcl, :realtime, null, null, 1, :rec, 1,
            null, :whareal, null, :wharealogl, :maxnumber, :dist, :mix, null,
            :mixtakepal, :mixleavepal, :mixmwsconstlocl, :mixmwspallocl, :mixrefill,
            :x_partia, :x_serial_no, :x_slownik); -- XXX KBI);
     update mwsacts set status = 1 where ref = :mwsactref;
      timestart = timestartdcl;
    end
--  end                                                                     --- XXX KBI
  -- na koniec jest komunikat ze nie ma wolnego miejsca na magazynie
--  else                                                                    --- XXX KBI
--    exception XK_NO_PLACE_IN_WHSEC 'Brak okreslonego miejsca dla palety'; --- XXX KBI
  select count(ref) from mwsacts where mwsord = :refmwsord into actcnt;
  if (actcnt is null) then actcnt = 0;
  if (actcnt > 0) then
  begin
    update mwsords set status = 1, operator = null, mwsaccessory = :mwsaccessory
      where ref = :refmwsord and status = 0;
    update mwsords mo set mo.operator = null where mo.ref = :refmwsord;
    mwsord = :refmwsord;
    execute procedure xk_mws_mwsord_route_opt(:refmwsord);
  end else
    delete from mwsords where ref = :refmwsord;
end^
SET TERM ; ^
