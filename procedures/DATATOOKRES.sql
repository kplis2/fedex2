--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DATATOOKRES(
      ZMIENNA varchar(15) CHARACTER SET UTF8                           ,
      ZMROK integer,
      ZMMIES numeric(14,4),
      ZMDZIEN integer,
      PAR integer)
  returns (
      OKRES varchar(6) CHARACTER SET UTF8                           ,
      DATAZM timestamp,
      FDAY timestamp,
      LDAY timestamp)
   as
declare variable l integer;
declare variable m integer;
declare variable r integer;
declare variable d integer;
declare variable mo integer;
declare variable y integer;
declare variable da integer;
declare variable zr integer;
declare variable reszta integer;
declare variable wiel integer;
declare variable data timestamp;
declare variable dataodjem date;
declare variable dzien varchar(2);
declare variable mies varchar(2);
declare variable parametr integer;
begin
  m = 0;
  d = 0;
  y = 0;
  if (zmrok is null) then zmrok = 0;
  if (zmmies is null) then zmmies = 0;
  if (zmdzien is null) then zmdzien = 0;
  if (par is null) then par = 0;
  if (zmienna like '__-__-__') then
    zmienna =  '20'||zmienna; --extract(year from current_date)||substring(zmienna from 3 for 6);
  else if (zmienna like '__-__-____') then
    zmienna = substring(zmienna from 7 for 4)||substring(zmienna from 3 for 4)||substring(zmienna from 1 for 2);
  l = coalesce(char_length(:zmienna),0); -- [DG] XXX ZG119346
  if (l <> 6 and l <> 10) then exception DATATOOKRES_EXPT ;
  else begin
    if (l = 10) then begin
      data = cast(:zmienna as TIMESTAMP);
      if (:zmienna is null) then data = '0001-01-01';
      y = extract(year from :data);
      mo = extract(month from :data);
      da = extract(day from :data);
      m = cast(:mo as integer);
      d = cast(:da as integer);
      r = cast(:y as integer);
    end
    else begin
      if (:zmienna is null) then zmienna = '000101';
      r = cast(substring(:zmienna from 1 for 4) as integer);
      mo = cast(substring(:zmienna from 5 for 2) as integer);
    end
    if (zmrok <> 0) then
      r = :r + :zmrok;
    if (zmmies <> 0) then begin
      if (zmmies < 0) then parametr = 2;
      else parametr = 0;
      execute procedure round_downup(:zmmies,12,:parametr) returning_values(:zr, :reszta, :wiel);
      r = :r + :wiel;
      m = :mo + (:zmmies - :wiel*12);
      if (:m <= 0) then begin
        m = 12 + :m;
        r = :r - 1;
      end
      else begin
        if (m > 12) then begin
          r = r + 1;
          m = m - 12;
        end
      end
    end
    else m = :mo;
    if (l = 10) then begin
      if (m = 2 and da > 28 and mod(r,4) > 0) then da = 28;
      if (m = 2 and da > 29 and mod(r,4) = 0) then da = 29;
      if (m in (4, 6, 9, 11) and da > 30) then da = 30;
      if (:da < 10) then dzien = '0'||:da;
      else
        dzien = cast(:da as varchar(2));
      if (:m < 10) then mies = '0'||:m;
      else
        mies = cast(:m as varchar(2));
      dataodjem = cast(:r||'-'||:mies||'-'||:dzien as TIMESTAMP);
      datazm = :dataodjem + :zmdzien;
      r = extract(year from :datazm);
      m = extract(month from :datazm);
    end
    if (:m < 10) then mies = '0'||:m;
    else
      mies = cast(:m as varchar(2));
    okres = cast (:r as varchar(4))||cast (:mies as varchar(2));
    if (par <> 0) then begin
      fday = cast(:r||'-'||:mies||'-01' as timestamp);
      m = :m + 1;
      if (m > 12) then begin
        m = 1;
        r = r + 1;
      end
      if (:m < 10) then mies = '0'||:m;
      else
        mies = cast(:m as varchar(2));
      lday = cast(:r||'-'||:mies||'-01' as timestamp) - 1;
    end
  end
  suspend;
end^
SET TERM ; ^
