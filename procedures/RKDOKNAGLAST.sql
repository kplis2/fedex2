--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RKDOKNAGLAST(
      STANOWISKO varchar(2) CHARACTER SET UTF8                           ,
      TYP varchar(3) CHARACTER SET UTF8                           ,
      NUMER integer,
      DATA varchar(15) CHARACTER SET UTF8                           )
  returns (
      WYNIK smallint)
   as
declare variable lday timestamp;
declare variable fday timestamp;
declare variable ffday timestamp;
begin
  wynik = 1;
  select lday, fday
    from DATATOOKRES(:DATA,null,null,null,1)
  into lday, fday;

  select fday
    from DATATOOKRES(:DATA,null,1,null,1)
  into ffday;

  if (exists(select ref from rkdoknag R
        where R.typ = :TYP
          and R.stanowisko = :STANOWISKO
          and R.numer > :numer
          and R.data >= :fday
          and R.data <= :lday))
  then
    wynik = 0;

  if (exists(select ref from rkdoknag R
        where R.typ = :TYP
          and R.stanowisko = :STANOWISKO
          and R.data >= :ffday))
  then
    wynik = 0;
end^
SET TERM ; ^
