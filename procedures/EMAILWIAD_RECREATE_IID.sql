--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWIAD_RECREATE_IID as
declare variable pfolder integer;
declare variable folder integer;
declare variable i integer;
declare variable reff integer;
begin
 pfolder = -1;
 for select REF, FOLDER
 from emailwiad
 where UID > 0
 and folder = 1124
 order by FOLDER, UID
 into :reff, :folder
 do begin
   if(:pfolder <>:folder) then
     i = 1;
   pfolder = :folder;
   update emailwiad set IID = :i where REF=:reff;
   i = i + 1;
 end

end^
SET TERM ; ^
