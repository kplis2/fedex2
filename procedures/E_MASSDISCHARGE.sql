--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_MASSDISCHARGE(
      DDATE timestamp,
      REASON integer,
      EMPLCONTRACT integer)
  returns (
      LOGMSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable PERSONNAMES varchar(255);
declare variable CONTRDATE date;
declare variable ENDDATE date;
declare variable etermination integer;
begin
/* Personel - procedura konczy umowe EMPLCONTRACT, gdzie DDATE <= ENDDATE
     lub ENDDATE jest nullem oraz pole ETERMINATION jest nullem (PR61955)  */

  select e.personnames, c.contrdate, c.enddate, c.etermination
    from employees e
      join emplcontracts c on (c.employee = e.ref)
    where c.ref = :emplcontract
    into :personnames, :contrdate, :enddate, :etermination;

  logmsg = ' ' || personnames || '  -  umowa z dnia ' || cast(contrdate as date) ;
  if(etermination is null and (ddate <= enddate or enddate is null)) then
  begin

    update emplcontracts
      set enddate = :ddate,
          etermination = :reason
      where ref = :emplcontract;

    logmsg = ascii_char(149) || logmsg || ' została zakończona';
  end else
    logmsg = ascii_char(164) || logmsg || ' była już zakończona';

  suspend;

  when any do
  begin
    logmsg = ascii_char(215) ||  ' ' || personnames || ' - zakończenie umowy z dnia ' || cast(contrdate as date) || ' nie powiodło się';
  --exception;
    suspend;
  end
end^
SET TERM ; ^
