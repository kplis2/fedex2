--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDULE_READYTOSCHED_CHECK(
      PRSCHEDOPER integer)
   as
begin
  if (not exists (
      select o.ref
        from  prschedoperdeps d
          left join prschedopers o on (d.depfrom = o.ref)
          left join prgantt g on (g.prschedoper = o.ref)
        where d.depto = :prschedoper and g.verified = 0 and g.status < 3)
  ) then
     update prgantt set readytosched = 1 where prschedoper = :prschedoper;
end^
SET TERM ; ^
