--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_GET_APPINI(
      NAMESPACE varchar(40) CHARACTER SET UTF8                           ,
      SECTION varchar(255) CHARACTER SET UTF8                           ,
      IDENT varchar(255) CHARACTER SET UTF8                           )
  returns (
      VAL varchar(1024) CHARACTER SET UTF8                           ,
      CHANGED smallint)
   as
declare variable currlayer integer;
declare variable layer integer;
begin
  if(:namespace is null or :namespace='') then namespace = 'SENTE';
  select layer from s_namespaces where namespace=:namespace into :currlayer;
  changed = 0;
  -- pobierz wartosc z warstwy biezacej lub pierwotnych
  select first 1 s.val, n.layer
    from s_appini s join s_namespaces n on (n.namespace=s.namespace)
    where s.section=:section and s.ident=:ident
    and ((n.layer=:currlayer and s.namespace=:namespace) or n.layer<:currlayer)
    order by n.layer desc
    into :val, :layer;
  if(:layer=:currlayer and :currlayer>0) then changed = 1;
  if(:val is null) then val = '';
end^
SET TERM ; ^
