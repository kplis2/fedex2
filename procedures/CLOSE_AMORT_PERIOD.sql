--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CLOSE_AMORT_PERIOD(
      AMPERIOD integer,
      COMPANY integer)
   as
begin
  execute PROCEDURE CALCULATE_AMORTIZATION_ALL(:amperiod,:company);
  update amperiods set status=1 where ref=:amperiod;
  suspend;
end^
SET TERM ; ^
