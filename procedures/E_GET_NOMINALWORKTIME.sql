--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_NOMINALWORKTIME(
      EMPLOYEE EMPLOYEES_ID,
      FROMDATE date,
      TODATE date,
      PERIOD varchar(6) CHARACTER SET UTF8                            = null)
  returns (
      WD integer,
      WS integer)
   as
declare variable WD_TMP integer;
declare variable WS_TMP integer;
declare variable ECALENDAR ecalendars_id;
declare variable TMPDATE date;
begin
--MWr: Personel - wyliczenie nominalnego czasu pracy dla danego pracownika i okresu

  if (coalesce(period,'') <> '') then
    execute procedure period2dates(:period)
      returning_values :fromdate, :todate;

  execute procedure emplcaldays_store('SUM', :employee, :fromdate, :todate) returning_values :ws;
  execute procedure emplcaldays_store('COUNT', :employee, :fromdate, :todate) returning_values :wd;

  --czy zatrudnienie nie konczy sie przed koncem miesiaca? jezeli tak to uzupelnij nominalne dni robocze
  select first 1 calendar, todate from emplcalendar
    where employee = :employee
    order by fromdate desc
    into :ecalendar, :tmpdate;

  if (tmpdate < todate) then
  begin
    execute procedure ecaldays_store('SUM', :ecalendar, :tmpdate + 1, :todate) returning_values :ws_tmp;
    execute procedure ecaldays_store('COUNT', :ecalendar, :tmpdate + 1, :todate) returning_values :wd_tmp;
    wd = wd + wd_tmp;
    ws = ws + ws_tmp;
  end

  --czy zatrudnienie nie zaczyna sie po pierwszym dniu miesiaca
  tmpdate = null;
  select first 1 calendar, fromdate from emplcalendar
    where employee = :employee
    order by fromdate
    into :ecalendar, :tmpdate;

  if (tmpdate > fromdate) then
  begin
    execute procedure ecaldays_store('SUM', :ecalendar, :fromdate, :tmpdate - 1) returning_values :ws_tmp;
    execute procedure ecaldays_store('COUNT', :ecalendar, :fromdate, :tmpdate - 1) returning_values :wd_tmp;
    wd = wd + wd_tmp;
    ws = ws + ws_tmp;
  end

  suspend;
end^
SET TERM ; ^
