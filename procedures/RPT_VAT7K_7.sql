--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_VAT7K_7(
      PERIOD varchar(1) CHARACTER SET UTF8                           ,
      YEARID varchar(4) CHARACTER SET UTF8                           ,
      PAR_K47 numeric(14,2),
      PAR_K62 numeric(14,2),
      PAR_K63 numeric(14,2),
      PAR_K64 numeric(14,2),
      COMPANY integer)
  returns (
      WSP45 numeric(14,2),
      K20 numeric(14,2),
      K21 numeric(14,2),
      K22 numeric(14,2),
      K23 numeric(14,2),
      K24 numeric(14,2),
      K25 numeric(14,2),
      K26 numeric(14,2),
      K27 numeric(14,2),
      K28 numeric(14,2),
      K29 numeric(14,2),
      K30 numeric(14,2),
      K31 numeric(14,2),
      K32 numeric(14,2),
      K33 numeric(14,2),
      K34 numeric(14,2),
      K35 numeric(14,2),
      K36 numeric(14,2),
      K37 numeric(14,2),
      K38 numeric(14,2),
      K39 numeric(14,2),
      K40 numeric(14,2),
      K41 numeric(14,2),
      K42 numeric(14,2),
      K43 numeric(14,2),
      K44 numeric(14,2),
      K45 numeric(14,2),
      K46 numeric(14,2),
      K47 numeric(14,2),
      K48 numeric(14,2),
      K49 numeric(14,2),
      K50 numeric(14,2),
      K51 numeric(14,2),
      K52 numeric(14,2),
      K53 numeric(14,2),
      K54 numeric(14,2),
      K55 numeric(14,2),
      K56 numeric(14,2),
      K57 numeric(14,2),
      K58 numeric(14,2),
      K59 numeric(14,2),
      K60 numeric(14,2),
      K61 numeric(14,2),
      K62 numeric(14,2),
      K63 numeric(14,2),
      K64 numeric(14,2),
      K65 numeric(14,2))
   as
declare variable vatgr varchar(6);
declare variable netv numeric(14,2);
declare variable vatv numeric(14,2);
declare variable taxgr varchar(10);
declare variable bkreg varchar(10);
declare variable wrongdoc varchar(20);
declare variable bkdocnumber integer;
declare variable vat_nalezny varchar(255);
declare variable vat_naliczony varchar(255);
declare variable sdate date;
declare variable fdate date;
declare variable np numeric(14,2);
declare variable odokresu varchar(6);
declare variable dookresu varchar(6);
declare variable k25_3 numeric(14,2);
declare variable k25_5 numeric(14,2);
declare variable k27_7 numeric(14,2);
declare variable k27_8 numeric(14,2);
declare variable k29_22 numeric(14,2);
declare variable k29_23 numeric(14,2);
begin

  -- paramert Konfiguracji
  execute procedure get_config('VAT7_NALEZNY', 0) -- naliczanie wg rejestrów
    returning_values :vat_nalezny;
  execute procedure get_config('VAT7_NALICZONY', 0) -- naliczanie staweK <> 0
    returning_values :vat_naliczony;

  if (PAR_K47 is null) then
    PAR_K47 = 0;
  if (PAR_K62 is null) then
    PAR_K62 = 0;
  if (PAR_K63 is null) then
    PAR_K63 = 0;
  if (PAR_K64 is null) then
    PAR_K64 = 0;
  WSP45 = 100; --domyslnie caly mozna odliczyc
  K20 = 0;
  K21 = 0;
  K22 = 0;
  K23 = 0;
  K24 = 0;
  K25 = 0;
  K25_3 = 0;
  K25_5 = 0;
  K26 = 0;
  K27 = 0;
  K27_7 = 0;
  K27_8 = 0;
  K28 = 0;
  K29 = 0;
  K29_22 = 0;
  K29_23 = 0;
  K30 = 0;
  K31 = 0;
  K32 = 0;
  K33 = 0;
  K34 = 0;
  K35 = 0;
  K36 = 0;
  K37 = 0;
  K38 = 0;
  K39 = 0;
  K40 = 0;
  K41 = 0;
  K42 = 0;
  K43 = 0;
  K44 = 0;
  K45 = 0;
  K46 = 0;
  K47 = PAR_K47;
  K48 = 0;
  K49 = 0;
  K50 = 0;
  K51 = 0;
  K52 = 0;
  K53 = 0;
  K54 = 0;
  K55 = 0;
  K56 = 0;
  K57 = 0;
  K58 = 0;
  K59 = 0;
  K60 = 0;
  K61 = 0;
  K62 = PAR_K62;
  K63 = PAR_K63;
  K64 = PAR_K64;
  K65 = 0;
  -- sprzedaz Krajowa
  -- vtype = 1 Korygowana jest deKlaracja w Ktorym zostala wystawiony doKument WDT (oKres zrodlowy)
  -- vtype = 2 KoreKta jest w biezacej deKlaracji zgodnie z data otrzymania potwierdzenia EXPORT (oKres biezacy otrzymania KoreKty)

  if (period = 0) then
  begin
    odokresu = :yearid||'01';
    dookresu = :yearid||'03';
  end
  if (period = 1) then
  begin
    odokresu = :yearid||'04';
    dookresu = :yearid||'06';
  end
  if (period = 2) then
  begin
    odokresu = :yearid||'07';
    dookresu = :yearid||'09';
  end
  if (period = 3) then
  begin
    odokresu = :yearid||'10';
    dookresu = :yearid||'12';
  end



  for select bKvatpos.vatgr, sum(bKvatpos.netv), sum(bKvatpos.vatv)
      from bKdocs
      join vatregs on (vatregs.symbol = bKdocs.vatreg)
      join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
      where bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
        and vatregs.vtype = 0 and bKdocs.status >= 0 and bKdocs.company= vatregs.company
        and bKdocs.company = :company 
        and bkvatpos.taxgr <> 'C41'
    group by bKvatpos.vatgr, bKvatpos.taxgr
    into :vatgr, :netv, :vatv
  do begin
    if (vatgr = '22') then
    begin
      K29 = K29 + netv;
      K29_22 = K29_22 + netv;
      K30 = K30 + vatv;
    end
    else if (vatgr = '23') then
    begin
      K29 = K29 + netv;
      K29_23 = K29_23 + netv;
      K30 = K30 + vatv;
    end
    else if (vatgr = '07') then
    begin
      K27 = K27 + netv;
      K27_7 = K27_7 + netv;
      K28 = K28 + vatv;
    end
    else if (vatgr = '08') then
    begin
      K27 = K27 + netv;
      K27_8 = K27_8 + netv;
      K28 = K28 + vatv;
    end
    else if (vatgr = '03') then
    begin
      K25 = K25 + netv;
      K25_3 = k25_3 + netv;
      K26 = K26 + vatv;
    end else if (vatgr = '05') then
    begin
      K25 = K25 + netv;
      K25_5 = k25_5 + netv;
      K26 = K26 + vatv;
    end else if (vatgr = 'ZW') then
    begin
      K20 = K20 + netv;
    end
    else if (vatgr = '00') then
    begin
      K23 = K23 + netv;
      if (taxgr = 'P23') then
      begin
        K24 = K24 + netv;
      end
    end
    --else
      --exception test_breaK 'Nieznana stawKa podatKowa ' || vatgr;
  end

/*
  EKSPORT TOWARÓW (p.32)
  cały eksport z wyj. grup C21 i C22(11)
  vatregs.vtype = 2 and bKvatpos.taxgr <> 'C21' and bKvatpos.taxgr <> 'C22(11)'
*/
  K32 = 0;
  select sum(bKvatpos.netv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    where ((bKvatpos.taxgr <> 'C21' and bKvatpos.taxgr <> 'C22(11)') or bKvatpos.taxgr is null)
      and vatregs.vtype = 2 and bKdocs.status > 0 and bKdocs.company= vatregs.company
      and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and bKdocs.company = :company
    into :K32;

/*
  DOSTAWA TOWARÓW ORAZ ŚWIADCZENIE USŁUG POZA TERYTORIUM KRAJU (p. 21)
  tylko eksport z grupą C21 lub C22(11)
  vatregs.vtype = 2 and (bKvatpos.taxgr = 'C21' or bKvatpos.taxgr = 'C22(11)')
*/
  select sum(bKvatpos.netv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    where (bKvatpos.taxgr = 'C21' or bKvatpos.taxgr = 'C22(11)') and bKdocs.company = :company
    and vatregs.vtype = 2 and bKdocs.status > 0 and bKdocs.company= vatregs.company
    and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
    into :K21;

/*
  DOSTAWA TOWARÓW ORAZ ŚWIADCZENIE USŁUG POZA TERYTORIUM KRAJU art. 100 (p. 22)
  tylko eksport z grupą C22(11)
  vatregs.vtype = 2 and bKvatpos.taxgr = 'C22(11)'
*/
  select sum(bKvatpos.netv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    where bKvatpos.taxgr = 'C22(11)' and bKdocs.company = :company
    and vatregs.vtype = 2 and bKdocs.status > 0 and bKdocs.company= vatregs.company
    and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
  into :K22;

/*
  DOSTAWA TOWARÓW ORAZ ŚWIADCZENIE USŁUG POZA TERYTORIUM KRAJU (p. 21)
  tylko WDT ze stawką NP
  vatregs.vtype = 1 and bKvatpos.vatgr = 'NP'  
*/
  select sum(bKvatpos.netv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    where bKvatpos.vatgr = 'NP' and bKdocs.company = :company
    and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
    and vatregs.vtype = 1 and bKdocs.status > 0 and bKdocs.company= vatregs.company
    into :np;

  K21 = coalesce(K21, 0) + coalesce(np, 0);

/*
  WEWNĄTRZWSPÓLNOTOWA DOSTAWA TOWARÓW (p. 31)
  cała WDT z wyj. stawki NP
  vatregs.vtype = 1 and bKvatpos.vatgr <> 'NP'
*/
  select sum(bKvatpos.netv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    where bKdocs.company = :company
    and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and vatregs.vtype = 1 and bKdocs.status > 0 and bKdocs.company= vatregs.company
      and bKvatpos.vatgr <> 'NP'
  into :K31;

/*
  WEWNĄTRZWSPÓLNOTOWE NABYCIE TOWARU (p. 33, 34)
  całe WNT z wyj. grup C36, C41 i C42(11)
  vatregs.vtype = 5 and (bKvatpos.taxgr <> 'C36' and bKvatpos.taxgr <> 'C42(11)' and bKvatpos.taxgr <> 'C41')
*/
    select sum(bKvatpos.netv),
      sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    where ((bKvatpos.taxgr <> 'C36' and bKvatpos.taxgr <> 'C42(11)' and bKvatpos.taxgr <> 'C41') or bKvatpos.taxgr is null)
      and vatregs.vtype = 5 and bKdocs.status > 0 and bKdocs.company= vatregs.company
      and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and bKdocs.company = :company
    into :K33, :K34;

/*
  DOSTAWA TOWARÓW ORAZ ŚWIADCZENIE USŁUG, DLA KTÓRYCH PODATNIKIEM JEST NABYWCA (p. 41, 42)
  wszystkie dokumenty z grupą C36 lub C41 lub C42(11)
  bKvatpos.taxgr = 'C36' or bKvatpos.taxgr = 'C42(11)' or bKvatpos.taxgr = 'C41'
*/
  select sum(bKvatpos.netv),
      sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    where (bKvatpos.taxgr = 'C36' or bKvatpos.taxgr = 'C42(11)' or bKvatpos.taxgr = 'C41')
      and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and bKdocs.status > 0 and bKdocs.company= vatregs.company
      and bKdocs.company = :company
    into :K41, :K42;

/*
  KWOTA PODATKU NALEŻNEGO OD TOWARÓW I USŁUG OBJĘTYCH SPISEM Z NATURY (p. 43)
  sprzedaż krajowa z grupą P40 lub C43(11)
  vatregs.vtype = 0 and (bKvatpos.taxgr = 'P40' or bKvatpos.taxgr = 'C43(11)')
*/
    select sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    where (bKvatpos.taxgr = 'P40' or bKvatpos.taxgr = 'C43(11)' or bKvatpos.taxgr is null)
      and vatregs.vtype = 0 and bKdocs.status > 0 and bKdocs.company= vatregs.company
      and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and bKdocs.company = :company
    into :K43;

/* 
  KWOTA PODATKU NALICZONEGO WYNIKAJĄCEGO ZE SPISU Z NATURY (p. 48)
  zakupy krajowe z grupą P45 lub D48(11)
  vatregs.vtype = 3 and (bKvatpos.taxgr = 'P45' or bKvatpos.taxgr = 'D48(11)')
*/
    select sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    where (bKvatpos.taxgr = 'P45' or bKvatpos.taxgr = 'D48(11)' or bKvatpos.taxgr is null)
      and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and vatregs.vtype = 3 and bKdocs.status > 0 and bKdocs.company= vatregs.company
      and bKdocs.company = :company
    into :K48;

/*
  WNT - NABYCIE ŚRODKÓW TRANSPORTU (p. 44)
  tylko WNT z grupą P39 lub C44(11)
  vatregs.vtype = 5 and (bKvatpos.taxgr = 'P39' or bKvatpos.taxgr = 'C44(11)')
*/
    select sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    where (bKvatpos.taxgr = 'P39' or bKvatpos.taxgr = 'C44(11)' or bKvatpos.taxgr is null)
      and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and vatregs.vtype = 5 and bKdocs.status > 0 and bKdocs.company= vatregs.company
      and bKdocs.company = :company
    into :K44;

/*
  IMPORT USŁUG (p. 37, 38)
  dokumenty z rejestru VAT typu Import usług
  vatregs.vtype = 6
*/
    select sum(bKvatpos.netv),
      sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    where bKdocs.company = :company
      and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and vatregs.vtype = 6 and bKdocs.status > 0 and bKdocs.company= vatregs.company
    into :K37, :K38;

/*
  IMPORT USŁUG art. 28b (p. 39, 40)
  dokumenty z rejestru VAT typu Import usług z grupą C39(11)
  vatregs.vtype = 6 and bKvatpos.taxgr = 'C39(11)'
*/
    select sum(bKvatpos.netv),
      sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    where bKdocs.company = :company
      and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and vatregs.vtype = 6 and bKdocs.status > 0 and bKdocs.company= vatregs.company
      and bKvatpos.taxgr = 'C39(11)'
    into :K39, :K40;


  if (odokresu <= '200512') then
  begin
    K20 = cast(K20 * 100 as integer) / 100;
    K21 = cast(K21 * 100 as integer) / 100;
    K22 = cast(K22 * 100 as integer) / 100;
    K23 = cast(K23 * 100 as integer) / 100;
    K24 = cast(K24 * 100 as integer) / 100;
    K25 = cast(K25 * 100 as integer) / 100;
    K26 = cast(K26 * 100 as integer) / 100;
    K27 = cast(K27 * 100 as integer) / 100;
    K28 = cast(K28 * 100 as integer) / 100;
    K29 = cast(K29 * 100 as integer) / 100;
    K30 = cast(K30 * 100 as integer) / 100;
    K31 = cast(K31 * 100 as integer) / 100;
    K32 = cast(K32 * 100 as integer) / 100;
    K33 = cast(K33 * 100 as integer) / 100;
    K34 = cast(K34 * 100 as integer) / 100;
    K35 = cast(K35 * 100 as integer) / 100;
    K36 = cast(K36 * 100 as integer) / 100;
    K37 = cast(K37 * 100 as integer) / 100;
    K38 = cast(K38 * 100 as integer) / 100;
    K39 = cast(K39 * 100 as integer) / 100;
    K41 = cast(K41 * 100 as integer) / 100;
  end else
  begin
    K20 = round(K20);
    K21 = round(K21);
    K22 = round(K22);
    K23 = round(K23);
    K24 = round(K24);
    K25 = round(K25);
    if (vat_nalezny = '1') then
    begin
      K26 = round(0.03 * K25_3) + round(0.05 * K25_5);
      K28 = round(0.07 * K27_7) + round(0.08 * K27_8);
      K30 = round(0.22 * K29_22) + round(0.23 * K29_23);
    end else
    begin
      K26 = round(K26);
      K28 = round(K28);
      K30 = round(K30);
    end
    K27 = round(K27);
    K29 = round(K29);
    K31 = round(K31);
    K32 = round(K32);
    K33 = round(K33);
    K34 = round(K34);
    K35 = round(K35);
    K36 = round(K36);
    K37 = round(K37);
    K38 = round(K38);
    K39 = round(K39);
    K40 = round(K40);
    K41 = round(K41);
    K42 = round(K42);
  end

  -- zeby liczyl odpowiednio
  if (K20 is null) then K20 = 0;
  if (K21 is null) then K21 = 0;
  if (K22 is null) then K22 = 0;
  if (K23 is null) then K23 = 0;
  if (K24 is null) then K24 = 0;
  if (K25 is null) then K25 = 0;
  if (K26 is null) then K26 = 0;
  if (K27 is null) then K27 = 0;
  if (K28 is null) then K28 = 0;
  if (K29 is null) then K29 = 0;
  if (K30 is null) then K30 = 0;
  if (K31 is null) then K31 = 0;
  if (K32 is null) then K32 = 0;
  if (K33 is null) then K33 = 0;
  if (K34 is null) then K34 = 0;
  if (K35 is null) then K35 = 0;
  if (K36 is null) then K36 = 0;
  if (K37 is null) then K37 = 0;
  if (K38 is null) then K38 = 0;
  if (K39 is null) then K39 = 0;
  if (K40 is null) then K40 = 0;
  if (K41 is null) then K41 = 0;
  if (K42 is null) then K42 = 0;
  if (K43 is null) then K43 = 0;
  if (K44 is null) then K44 = 0;
  if (K45 is null) then K45 = 0;
  if (K46 is null) then K46 = 0;
  if (K47 is null) then K47 = 0;
  if (K48 is null) then K48 = 0;

  K45 = K20 + K21 + K23 + K25 + K27 + K29 + K31 + K32 + K33
    + K35 + K37 + K41;
  K46 = K26 + K28 + K30 + K34 + K36 + K38 + K42 + K43 - K44;
  -- zaKupy z tego miesiaca
  for
    select  bKvatpos.taxgr, sum(bKvatpos.netv), sum(bKvatpos.vatv)
      from bKdocs
        join vatregs on (vatregs.symbol = bKdocs.vatreg)
        join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref and bKvatpos.debited = 1)
      where bKdocs.company = :company and bKvatpos.taxgr<>'P45'
        and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
        and vatregs.vtype > 2 and bKdocs.status > 0 and bKdocs.company= vatregs.company
      group by bKvatpos.taxgr
      into :taxgr, :netv, :vatv
  do begin
    if (taxgr in ('I11','I21','INW')) then
    begin
      if (vat_naliczony = '1') then
        if (vatv = 0) then netv = 0;
      K49 = K49 + netv;
      K50 = K50 + vatv;
    end else if (taxgr in ('P11','P21','POZ','C36','P39','C42(11)','C39(11)')) then
    begin
      if (vat_naliczony <> '' and vat_naliczony = '1') then
        if (vatv = 0) then netv = 0;
      K51 = K51 + netv;
      K52 = K52 + vatv;
    end
    else begin
      select first 1 bKdocs.symbol, bKdocs.bKreg, bKdocs.number
        from bKdocs
          join vatregs on (vatregs.symbol = bKdocs.vatreg)
          join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
          where bKdocs.company = :company and (bKvatpos.taxgr = :taxgr or bKvatpos.taxgr is null)
          and vatregs.vtype > 2 and bKdocs.status > 0 and bKdocs.company= vatregs.company
          and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
          and bKvatpos.debited = 1
        into :wrongdoc, :bKreg, :bKdocnumber;

--      exception universal 'Wystąpiła grupa podatKowa nieuwzględniona w deKlaracji!';
      exception universal 'Błędna gr. podatK. na doKumen. ' || :wrongdoc || ' ('|| :bKreg ||','|| :bKdocnumber ||')';
      -- exception fK_vat7_unKnown_taxgr;
    end
  end

 -- zsumowanie calego do odliczenia
  if (K50 is null) then
    K50 = 0;
  if (K51 is null) then
    K51 = 0;
  if (odokresu <= '200512') then
  begin       
    K44 = cast(K44 * 100 as integer) / 100;
    K49 = cast(K49 * 100 as integer) / 100;
    K50 = cast(K50 * 100 as integer) / 100;
    K51 = cast(K51 * 100 as integer) / 100;
    K52 = cast(K52 * 100 as integer) / 100;
  end else
  begin
    K44 = round(K44);
    K48 = round(K48);
    K49 = round(K49);
    K50 = round(K50);
    K51 = round(K51);
    K52 = round(K52);
  end

  K53 = (K50) * ((WSP45 - 100) / 100); --wyliczenie KoreKt
  K54 = (K52) * ((WSP45 - 100) / 100);

  if (odokresu <= '200512') then
  begin
    K53 = cast(K53 * 100 as integer) / 100;
    K54 = cast(K54 * 100 as integer) / 100;
  end else
  begin
    K53 = round(K53);
    K54 = round(K54);
  end

  K55 = K47 + K48 + K50 + K52 + K53 + K54; -- caly odliczony

  if (K46 > K55) then
    K58 = K46 - K55;
  else
    K58 = 0;

  if (K55 > K46) then
    K60 = K55 - K46;
  else
    K60 = 0;

  K61 = K62 + K63 + K64;
  K65 = K59 + K60 - K61;

  if (K20 = 0) then K20 = null;
  if (K21 = 0) then K21 = null;
  if (K22 = 0) then K22 = null;
  if (K23 = 0) then K23 = null;
  if (K24 = 0) then K24 = null;
  if (K25 = 0) then K25 = null;
  if (K26 = 0) then K26 = null;
  if (K27 = 0) then K27 = null;
  if (K28 = 0) then K28 = null;
  if (K29 = 0) then K29 = null;
  if (K30 = 0) then K30 = null;
  if (K31 = 0) then K31 = null;
  if (K32 = 0) then K32 = null;
  if (K33 = 0) then K33 = null;
  if (K34 = 0) then K34 = null;
  if (K35 = 0) then K35 = null;
  if (K36 = 0) then K36 = null;
  if (K37 = 0) then K37 = null;
  if (K38 = 0) then K38 = null;
  if (K39 = 0) then K39 = null;
  if (K40 = 0) then K40 = null;
  if (K41 = 0) then K41 = null;
  if (K42 = 0) then K42 = null;
  if (K43 = 0) then K43 = null;
  if (K44 = 0) then K44 = null;
  if (K45 = 0) then K45 = null;
  if (K46 = 0) then K46 = null;
  if (K47 = 0) then K47 = null;
  if (K48 = 0) then K48 = null;
  if (K49 = 0) then K49 = null;
  if (K50 = 0) then K50 = null;
  if (K51 = 0) then K51 = null;
  if (K52 = 0) then K52 = null;
  if (K53 = 0) then K53 = null;
  if (K54 = 0) then K54 = null;
  if (K55 = 0) then K55 = null;
  if (K56 = 0) then K56 = null;
  if (K57 = 0) then K57 = null;
  if (K58 = 0) then K58 = null;
  if (K59 = 0) then K59 = null;
  if (K60 = 0) then K60 = null;
  if (K61 = 0) then K61 = null;
  if (K62 = 0) then K62 = null;
  if (K63 = 0) then K63 = null;
  if (K64 = 0) then K64 = null;
  if (K65 = 0) then K65 = null;

  suspend;
end^
SET TERM ; ^
