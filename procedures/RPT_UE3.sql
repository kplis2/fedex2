--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_UE3(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      YEARID varchar(4) CHARACTER SET UTF8                           ,
      COMPANY integer,
      RODZAJ integer)
  returns (
      ALLPAGESA integer,
      ALLPAGESB integer,
      ALLPAGESC integer)
   as
declare variable sql varchar(4096);
declare variable odokresu varchar(2);
declare variable dookresu varchar(2);
declare variable temp float;
begin


if (rodzaj = 0) then
  begin
    if (period = 0) then
    begin
      odokresu = '01';
      dookresu = '03';
    end
    if (period = 1) then
    begin
      odokresu = '04';
      dookresu = '06';
    end
    if (period = 2) then
    begin
      odokresu = '07';
      dookresu = '09';
    end
    if (period = 3) then
    begin
      odokresu = '10';
      dookresu = '12';
    end
  end
--Licze ile jest pozycji do sekcji C, czyli na wydruk A
  sql ='select count(*) from (select K.nip
        from bkdocs B
        join vatregs V on (V.symbol = B.vatreg and V.company = B.company)
        left join klienci K on (K.ref = B.dictpos)
        join bkvatpos BV on (BV.bkdoc = B.ref)
          where v.vtype = 1 and BV.vatgr <> ''NP''
          and B.status > 0';
 if (rodzaj = 1) then sql = sql||' and b.vatperiod ='''||:period||'''';
  else sql = sql||' and substring(b.vatperiod from 1 for 4) = '''||:yearid||'''
          and substring(b.vatperiod from 5 for 2) >= '''||:odokresu||'''
          and substring(b.vatperiod from 5 for 2) <= '''||:dookresu||'''';

  sql = sql||'  and B.company = '||:company||'
          group by K.nip)';
  execute statement sql
        into :temp;
  temp = temp -12;
  if (temp > 0) then
  begin
  temp = temp/59;
  allpagesa = cast(temp as integer);
  if (allpagesa != temp) then allpagesa = allpagesa + 1;
  end
  else allpagesa = 0;

--Licze ile jest pozycji do sekcji D, czyli na wydruk B
  sql ='select count(*) from (select D.nip
        from bkdocs B
        join vatregs V on (V.symbol = B.vatreg and V.company = B.company)
        left join dostawcy D on (D.ref = B.dictpos)
          where v.vtype = 5
          and B.status > 1';
  if (rodzaj = 1) then sql = sql||' and b.vatperiod ='''||:period||'''';
  else sql = sql||' and substring(b.vatperiod from 1 for 4) = '''||:yearid||'''
          and substring(b.vatperiod from 5 for 2) >= '''||:odokresu||'''
          and substring(b.vatperiod from 5 for 2) <= '''||:dookresu||'''';

  sql = sql||'  and B.company = '||:company||'
          group by D.nip)';
  execute statement sql
        into :temp;
  temp = temp -12;
  if (temp > 0) then
  begin
  temp = temp/59;
  allpagesb = cast(temp as integer);
  if (allpagesb != temp) then allpagesb = allpagesb + 1;
  end
  else allpagesb = 0;
--Licze ile jest pozycji do sekcji E, czyli na wydruk C
  sql ='select count(*) from (select K.nip
        from bkdocs B
        join vatregs V on (V.symbol = B.vatreg and V.company = B.company)
        left join klienci K on (K.ref = B.dictpos)
        join bkvatpos BV on (BV.bkdoc = B.ref)
          where v.vtype = 1 and BV.vatgr = ''NP''
          and B.status > 0';
  if (rodzaj = 1) then sql = sql||' and b.vatperiod ='''||:period||'''';
  else sql = sql||' and substring(b.vatperiod from 1 for 4) = '''||:yearid||'''
          and substring(b.vatperiod from 5 for 2) >= '''||:odokresu||'''
          and substring(b.vatperiod from 5 for 2) <= '''||:dookresu||'''';

  sql = sql||'  and B.company = '||:company||'
          group by K.nip)';
  execute statement sql
        into :temp;
  temp = temp -12;
  if (temp > 0) then
  begin
  temp = temp/59;
  allpagesc = cast(temp as integer);
  if (allpagesc != temp) then allpagesc = allpagesc + 1;
  end
  else allpagesc = 0;

  suspend;
end^
SET TERM ; ^
