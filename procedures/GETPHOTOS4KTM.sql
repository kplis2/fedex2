--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GETPHOTOS4KTM(
      KTM KTM_ID)
  returns (
      REF INTEGER_ID,
      NRWERSJI INTEGER_ID,
      NUMER INTEGER_ID,
      TYTUL STRING60,
      OPIS STRING255,
      SCIEZKA STRING1024,
      X_ZDJECIE FOTO_BINARY)
   as
begin
  -- [DG] XXX DO TESTÓW
  --ktm = '19812';
  for
    select tp.ref, tp.wersja, tp.numer, tp.tytul, tp.opis, tp.plik, tp.x_zdjecie
        from towpliki tp
        where tp.ktm = :ktm
        order by tp.wersja, tp.numer
   /* select zz.ref, 0, zz.nazwa, zz.opis, replace(zz.sciezka,'\\10.1.30.7\Share01\Photos\','')
      from towary t
        join zalaczniki_zew zz on (zz.otable = 'TOWARY' and zz.oref = t.ref)
      where t.ktm = :ktm
        and zz.typ = 1
      order by zz.glowny desc    */
      into :ref, :nrwersji, :numer, :tytul, :opis, :sciezka, :x_zdjecie
  do begin
    suspend;
  end
end^
SET TERM ; ^
