--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_DELETE_DOKUMNAG
  returns (
      DOKUMNAG DOKUMNAG_ID)
   as
begin
  --mkd
  exception universal 'Wiesz co robisz???!!!';
  --in autonomous transaction do
  for
    select ref from dokumnag o
      where 1=1
      order by case o.typ when 'ZZ' then 1 when 'WZ' then 2 else 3 end
      --and o.mwsordtype = :typ
      into :dokumnag do
  begin
      suspend;
      in autonomous transaction do begin
        execute procedure set_global_param('AKTUOPERATOR','74');
        update dokumnag o set o.akcept = 0 where ref = :dokumnag;
        delete from dokumnag where ref = :dokumnag;
        when any do
        begin
        end
      end
  end
end^
SET TERM ; ^
