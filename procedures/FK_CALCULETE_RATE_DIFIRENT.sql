--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_CALCULETE_RATE_DIFIRENT(
      REFD integer)
   as
declare variable account ACCOUNT_ID;
declare variable period varchar(6);
declare variable iyear smallint;
declare variable imonth smallint;
declare variable side integer;
declare variable debit numeric(14,2);
declare variable credit numeric(14,2);
declare variable amount numeric(14,2);
declare variable settlement varchar(20);
declare variable curr_dt numeric(14,2);
declare variable curr_ct numeric(14,2);
declare variable dt numeric(14,2);
declare variable ct numeric(14,2);
declare variable winien numeric(14,2);
declare variable ma numeric(14,2);
declare variable winienzl numeric(14,2);
declare variable mazl numeric(14,2);
declare variable descript varchar(80);
declare variable kontofk KONTO_ID;
declare variable symbfak varchar(20);
declare variable currency varchar(3);
declare variable tmp varchar(80);
declare variable todo_currdt numeric(14,2);
declare variable todo_currct numeric(14,2);
declare variable todo_dtrate numeric(14,4);
declare variable todo_ctrate numeric(14,4);
declare variable done_currdt numeric(14,2);
declare variable done_currct numeric(14,2);
declare variable acc_currdt numeric(14,2);
declare variable acc_currct numeric(14,2);
declare variable rp_kurs numeric(14,4);
declare variable rp_ref integer;
declare variable ref_dt integer;
declare variable ref_ct integer;
declare variable dt_symbol varchar(20);
declare variable ct_symbol varchar(20);
declare variable i smallint;
declare variable r_winien numeric(14,2);
declare variable r_ma numeric(14,2);
declare variable tmp_descript varchar(80);
declare variable done smallint;
declare variable bkdocref integer;
declare variable konto ACCOUNT_ID;
declare variable rozrachunek varchar(80);
declare variable slodefp integer;
declare variable slopozp integer;
declare variable curdebit numeric(14,2);
declare variable curcredit numeric(14,2);
declare variable rate numeric(14,4);
declare variable strona smallint;
declare variable company integer;
begin
  /* BOBREK */
  /* schemat dekretowania - ksiegowanie różnic kursowych */
  select d.account, d.settlement, d.bkdoc, d.currdebit, d.currcredit, d.rate, d.side, d.company
    from decrees d
    where d.ref=:REFD
    into :konto, :rozrachunek, :bkdocref,:curdebit, :curcredit,:rate, :strona, :company ;

  if(rozrachunek is null) then
    exception decree_settle;
  execute procedure reg_settlement(refd);

  select B.descript, B.period, B.ref
    from bkdocs B
    where B.ref=:bkdocref
    into :tmp_descript, :period, :bkdocref;

  iyear = cast(substring(period from 1 for 4) as smallint);
  imonth = cast(substring(period from 5 for 2) as smallint);
  if (imonth=12) then
  begin
    iyear = iyear + 1;
    imonth = 1;
  end else
    imonth = imonth + 1;



  select MAX(r.slodef), max(r.slopoz)
    from rozrach r
    where r.kontofk = :konto
      and r.symbfak = :rozrachunek
      and r.company = :company
    into :slodefp, :slopozp;

  for select R.kontofk, R.symbfak, R.winien, R.ma, R.waluta
    from rozrach R
    where R.walutowy=1
      and R.kontofk = :konto
      and r.slodef = :slodefp
      and r.slopoz = :slopozp
      and r.symbfak = :rozrachunek
    into :kontofk, :symbfak, :r_winien, :r_ma, :currency
  do begin
    descript = tmp_descript;

    dt = 0; ct =0; curr_dt = 0; curr_ct = 0;
    done_currdt = 0; done_currct = 0;

    todo_currdt = 1; todo_currct = 1; /* zeby warunek petli przy pierwszym przebiegu byl spelniony */
    while (todo_currdt<>0 and todo_currct<>0) do
    begin
      todo_currdt = 0; todo_currct = 0;
      acc_currdt = 0; acc_currct = 0;
      done = 0;
      for select BD.symbol, RP.winien, RP.ma, RP.winienzl, RP.mazl, RP.ref, RP.kurs
        from rozrachp RP
        left join bkdocs BD on (RP.bkdoc=BD.ref)
        where RP.kontofk=:kontofk and RP.symbfak=:symbfak and RP.data<cast(:iyear||'/'||:imonth||'/1' as timestamp)
        order by data
        into :tmp, :winien, :ma, :winienzl, :mazl, :rp_ref, :rp_kurs
      do begin
       /* if (:WINIEN = 0) then
          WINIEN = curdebit;
        if(:MA = 0) then
          ma =curcredit;  */ --poprawione MW
        acc_currdt = acc_currdt + winien;
        acc_currct = acc_currct + ma;

        if (winien>0 and todo_currdt=0 and acc_currdt>done_currdt) then
        begin
          todo_currdt = acc_currdt - done_currdt;
          ref_dt = rp_ref;
          todo_dtrate = rp_kurs;  --
          dt_symbol = tmp;
        end

        if (ma>0 and todo_currct=0 and acc_currct>done_currct) then
        begin
          todo_currct = acc_currct - done_currct;
          ref_ct = rp_ref;
          todo_ctrate = rp_kurs;  --
          ct_symbol = tmp;
        end
        if (strona = 0) then
          todo_dtrate = rate;
        if (strona = 1) then
          todo_ctrate = rate;
        if (todo_currdt>0 and todo_currct>0 and done=0) then
        begin
          done = 1;
          /* sprawdzam czy juz czasem nie bylo rozliczone */
          select count(*) from rozrachp where dtratediff=:ref_dt and ctratediff=:ref_ct
            into :i;
          if (i=0) then
          begin
            /* rozliczaj */
            if (dt_symbol is null) then
              dt_symbol = '';
            if (ct_symbol is null) then
              ct_symbol = '';

            descript = substring (dt_symbol || ' ' || ct_symbol || ' :  ' from 1 for 80);

            if (todo_currdt>todo_currct) then
            begin
              done_currct = done_currct + todo_currct;
              done_currdt = done_currdt + todo_currct;
              amount = todo_currct * (todo_ctrate-todo_dtrate);
              descript = substring(descript || cast(todo_currct as varchar(25)) || ' * (' || cast(todo_ctrate as varchar(25)) || '-' || cast(todo_dtrate as varchar(25)) || ')' from 1 for 80);
            end else
            begin
              done_currct = done_currct + todo_currdt;
              done_currdt = done_currdt + todo_currdt;
              amount = todo_currdt * (todo_ctrate-todo_dtrate);
              descript = substring(descript || cast(todo_currdt as varchar(25)) || ' * (' || cast(todo_ctrate as varchar(25)) || '-' || cast(todo_dtrate as varchar(25)) || ')' from 1 for 80);
            end
            descript = substring(descript || tmp_descript from 1 for 80);

            if (amount>0) then
            begin
              side = 0;
              account = kontofk;
              settlement = symbfak;
              /* końcowe operacje - wstawianie dekretu do bazy */
              if (side = 0) then begin debit = amount; credit = 0; end else begin debit = 0; credit = amount; end
              if (account is null) then account = '';
              if (amount is not null and amount <>0) then
                insert into decrees (bkdoc, account, side, debit, credit, descript, settlement, currency, dtratediff, ctratediff)
                values (:bkdocref, :account, :side, :debit, :credit, :descript, :settlement, :currency, :ref_dt, :ref_ct);
              /*  KONIEC DEKRETU */

              side = 1;
              account = '750-01-B';
              /* końcowe operacje - wstawianie dekretu do bazy */
              if (side = 0) then begin debit = amount; credit = 0; end else begin debit = 0; credit = amount; end
              if (account is null) then account = '';
              if (amount is not null and amount <>0) then
                insert into decrees (bkdoc, account, side, debit, credit, descript)
                values (:bkdocref, :account, :side, :debit, :credit, :descript);
              /*  KONIEC DEKRETU */

            end else
            begin
              amount = -1 * amount;
              side = 1;
              account = kontofk;
              settlement = symbfak;
              /* końcowe operacje - wstawianie dekretu do bazy */
              if (side = 0) then begin debit = amount; credit = 0; end else begin debit = 0; credit = amount; end
              if (account is null) then account = '';
              if (amount is not null and amount <>0) then
                insert into decrees (bkdoc, account, side, debit, credit, descript, settlement, currency, dtratediff, ctratediff)
                 values (:bkdocref, :account, :side, :debit, :credit, :descript, :settlement, :currency, :ref_dt, :ref_ct);
              /*  KONIEC DEKRETU */


              side = 0;
              account = '751-01-B';
              /* końcowe operacje - wstawianie dekretu do bazy */
              if (side = 0) then begin debit = amount; credit = 0; end else begin debit = 0; credit = amount; end
              if (account is null) then account = '';
              if (amount is not null and amount <>0) then
                insert into decrees (bkdoc, account, side, debit, credit, descript) values
                (:bkdocref, :account, :side, :debit, :credit, :descript);
              /*  KONIEC DEKRETU */
            end
          end else
          begin
           if (todo_currdt>todo_currct) then
            begin
              done_currct = done_currct + todo_currct;
              done_currdt = done_currdt + todo_currct;
            end else
            begin
              done_currct = done_currct + todo_currdt;
              done_currdt = done_currdt + todo_currdt;
            end
          end
        end
      end
    end
  end
end^
SET TERM ; ^
