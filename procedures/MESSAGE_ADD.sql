--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MESSAGE_ADD(
      FROMOPER integer,
      TOOPER integer,
      TOADRESS varchar(255) CHARACTER SET UTF8                           ,
      MSGSUBJECT varchar(255) CHARACTER SET UTF8                           ,
      MSGTEXT TEXT_BLOB,
      SYMBOL varchar(255) CHARACTER SET UTF8                           ,
      ACTIONID varchar(255) CHARACTER SET UTF8                           ,
      ACTIONPARAMS varchar(255) CHARACTER SET UTF8                           ,
      ACTIONNAME varchar(255) CHARACTER SET UTF8                           ,
      SENDDATE varchar(255) CHARACTER SET UTF8                           ,
      DELIVERDATE varchar(255) CHARACTER SET UTF8                           ,
      NOTIFYDATE varchar(255) CHARACTER SET UTF8                           ,
      STATUS integer,
      PRIORITY integer,
      DEFMEDIUM integer,
      COMPANY integer = null)
  returns (
      REFID integer)
   as
begin
  if (:msgsubject is null) then msgsubject = '';
  REFID = 0;
  if(:senddate = '' or :senddate = '00:00:00') then senddate = null;
  if(:deliverdate = '' or :deliverdate = '00:00:00') then deliverdate = null;
  if(:notifydate = '' or :notifydate = '00:00:00') then notifydate = null;
  if(:company=0) then company = NULL;
  if(:symbol <> '') then begin
    delete from S_MESSAGES where SYMBOL=:symbol and TOOPER=:tooper and (STATUS=0 or STATUS=2);
  end

  execute procedure gen_ref('S_MESSAGES') returning_values :refid;
  insert into S_MESSAGES(REF,FROMOPER, TOOPER, TOADRESS, msgsubject, MSGTEXT, ACTIONID, ACTIONPARAMS,
        ACTIONNAME, SENDDATE, DELIVERDATE, NOTIFYDATE, STATUS, PRIORITY,
        DEFMEDIUM, SYMBOL, COMPANY, MSGTEXTLONG)
    values (:REFID,:FROMOPER, :TOOPER, :TOADRESS, :msgsubject, iif(CHAR_LENGTH(:MSGTEXT)> 4098,'',cast(:MSGTEXT as varchar(4096))), :ACTIONID, :ACTIONPARAMS,
        :ACTIONNAME, :SENDDATE, :DELIVERDATE, :NOTIFYDATE, :STATUS, :PRIORITY,
        :DEFMEDIUM, :SYMBOL, :COMPANY, iif(CHAR_LENGTH(:MSGTEXT)> 4098, :msgtext,''));
  suspend;
end^
SET TERM ; ^
