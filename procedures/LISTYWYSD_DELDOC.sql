--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_DELDOC(
      SHIPPINGDOC LISTYWYSD_ID)
  returns (
      STATUS STATUS_ID,
      MSG STRING255)
   as
declare variable accept status_id;
begin
  status = 1;

  select akcept from listywysd where ref = :shippingdoc
  into :accept;

  if (:accept > 0) then
  begin
    status = 0;
    msg = 'Dokument jest już zaakceptowany. Usunięcie niemożliwe';
  end

  if (:status = 1) then
  begin
    update listywysdroz_opk o set o.stan = 0 where o.listwysd = :shippingdoc;
    delete from listywysdroz r where r.listywysd = :shippingdoc;
    delete from listywysd where ref = :shippingdoc;
  end
end^
SET TERM ; ^
