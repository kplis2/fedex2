--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RABATUJ_ZBIORCZO(
      POZFAKREF integer,
      ZAKUP smallint)
  returns (
      WARTOSC numeric(14,2),
      ODDZIAL varchar(20) CHARACTER SET UTF8                           ,
      DOKSYM varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable refpozfakusl integer;
declare variable kwota numeric(14,2);
declare variable dataod timestamp;
declare variable datado timestamp;
declare variable ktmod varchar(40);
declare variable ktmdo varchar(40);
declare variable ktmmaska varchar(40);
declare variable cenaod numeric(14,4);
declare variable cenado numeric(14,4);
declare variable faktura integer;
declare variable dostawca integer;
declare variable magwartosc numeric(14,2);
declare variable kwotarozliczona numeric(14,2);
declare variable refdokumpoz integer;
declare variable pozcena numeric(14,4);
declare variable pozilosc numeric(14,4);
declare variable sql varchar(1024);
declare variable notdok integer;
declare variable refdokumnag integer;
declare variable datadokumnag timestamp;
declare variable datanoty timestamp;
declare variable datafaktury timestamp;
declare variable operakcept integer;
declare variable refdokumroz integer;
declare variable refdokumnotp integer;
declare variable cenamag numeric(14,4);
declare variable zmianawart numeric(14,2);
declare variable magazyn varchar(3);
declare variable dokmag integer;
declare variable symb varchar(255);
declare variable symbolefak varchar(2048);
begin
  select dokument,magazyn from pozfak where ref=:pozfakref into :faktura,:magazyn;
  select dostawca,data,operakcept from nagfak where ref=:faktura into :dostawca,:datafaktury,:operakcept;
  wartosc = 0;
  if(:zakup=1) then begin --korygowanie dokumentow PZ poprzez noty magazynowe
    -- przejdz sie po POZFAKUSL aby wylapac na co i jakie kwoty chcemy rozbic
    for select POZFAKUSL.REF,POZFAKUSL.KWOTA,POZFAKUSL.DATAOD, POZFAKUSL.DATADO, POZFAKUSL.KTMOD,
      POZFAKUSL.KTMDO, POZFAKUSL.KTMMASKA, POZFAKUSL.CENAOD, POZFAKUSL.CENADO, POZFAKUSL.symbolefak
       from POZFAKUSL
       where POZFAKUSL.REFPOZFAK = :pozfakref
         and POZFAKUSL.ROZLICZONO=0
         and POZFAKUSL.TRYB=1
       into :refpozfakusl,:kwota,:dataod,:datado,:ktmod,:ktmdo,:ktmmaska,:cenaod,:cenado, :symbolefak
    do begin
      if(:symbolefak='') then symbolefak = null;
      wartosc = :wartosc + :kwota;
      -- znormalizuj wartosci parametrow
      if(:dataod is null) then dataod = '1900-01-01';
      if(:datado is null) then datado = '2999-12-31';
      if(:ktmod is null or :ktmod='') then begin
        select min(ktm) from towary into :ktmod;
      end
      if(:ktmdo is null or :ktmdo='') then begin
        select max(ktm) from towary into :ktmdo;
      end
      if(:ktmmaska is null) then ktmmaska = '';
      if(:cenaod is null) then cenaod = -999999999;
      if(:cenado is null or :cenado=0) then cenado = 999999999;
      if(:kwota is null) then kwota = 0;
      -- zlap sumaryczna kwote dokumentow magazynowych podlegajacych korygowaniu
      select sum(p.wartosc)
        from dokumnag d
        join dokumpoz p on (p.dokument=d.ref)
        left join defdokum on (defdokum.symbol=d.typ)
        left join pozfak pf on (pf.ref=p.frompozfak)
        left join nagfak nf on (pf.dokument=nf.ref)
      where defdokum.wydania=0 and defdokum.koryg=0 and defdokum.zewn=1
--        and d.magazyn = :magazyn
        and d.akcept in (1,8) and d.dostawca=:dostawca
        and d.data>=:dataod
        and d.data<=:datado
        and p.ktm>=:ktmod
        and p.ktm<=:ktmdo
        and p.ktm like '%'||:ktmmaska||'%'
        and p.cena>=:cenaod
        and p.cena<=:cenado
        and ((coalesce(:symbolefak,'')='') or
             (nf.symbfak is not null and :symbolefak is not null and :symbolefak like '%'||nf.symbfak||'%')
            )
      into :magwartosc;
      if(:magwartosc is null) then magwartosc = 0;

      if(:kwota<0 and :magwartosc<=-:kwota) then exception POZFAKUSL_ZLAKWOTA 'Wartosc magazynu zbyt mala do korygowania ('||:magwartosc||')';
      kwotarozliczona = 0;

      -- przejdz kolejno po wszystkich dokumentach magazynowych podlegajacych korygowaniu i wystawiaj noty
      for select p.ref,p.cena,p.ilosc,d.ref,d.data
        from dokumnag d
        join dokumpoz p on (p.dokument=d.ref)
        left join defdokum on (defdokum.symbol=d.typ)
        left join pozfak pf on (pf.ref=p.frompozfak)
        left join nagfak nf on (pf.dokument=nf.ref)
      where defdokum.wydania=0 and defdokum.koryg=0 and defdokum.zewn=1
--        and d.magazyn = :magazyn
        and d.akcept in (1,8) and d.dostawca=:dostawca
        and d.data>=:dataod
        and d.data<=:datado
        and p.ktm>=:ktmod
        and p.ktm<=:ktmdo
        and p.ktm like '%'||:ktmmaska||'%'
        and p.cena>=:cenaod
        and p.cena<=:cenado
        and ((coalesce(:symbolefak,'')='') or
             (nf.symbfak is not null and :symbolefak is not null and :symbolefak like '%'||nf.symbfak||'%')
            )
      into :refdokumpoz,:pozcena,:pozilosc,:refdokumnag,:datadokumnag
      do begin
             /* wyliczenie zmiany w cenie*/
             cenamag = :kwota*:pozcena/:magwartosc;
             zmianawart = cast(:pozilosc*(:pozcena+:cenamag) as numeric(14,2)) - cast(:pozilosc*:pozcena as numeric(14,2));
             kwotarozliczona = :kwotarozliczona + :zmianawart;
             /*okrelenie, czy jest już zalozona nota mgazynowa na dany dokument*/
             notdok = null;
             select max(DOKUMNOT.ref) from DOKUMNOT join DOKUMNAG on (DOKUMNAG.REF=DOKUMNOT.dokumnagkor)
             join DOKUMPOZ on (DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
             where DOKUMPOZ.REF = :refdokumpoz and DOKUMNOT.FAKTURA = :faktura and DOKUMNOT.akcept = 0
             into :notdok;
             if(:notdok is null) then notdok = 0;
             if(:notdok = 0) then begin
               execute procedure GEN_REF('DOKUMNOT') returning_values :notdok;
               datanoty = :datadokumnag;
               if(:datafaktury>:datanoty) then datanoty = :datafaktury;
               insert into DOKUMNOT(ref, GRUPA, TYP,MAGAZYN, MAG2, DATA, DOKUMNAGKOR, FAKTURA, UWAGI,OPERATOR, RODZAJ)
               select :notdok, :notdok, DOKUMNAG.TYP, DOKUMNAG.MAGAZYN, DOKUMNAG.MAG2, :datanoty, :refdokumnag, :faktura, DOKUMNAG.uwagi, :operakcept, 10
               from DOKUMNAG where dokumnag.ref = :refdokumnag;
             end
             /*poprawa rozpisek tej pozycji dokumentu mag.*/
             for select DOKUMROZ.REF, DOKUMNOTP.REF
             from DOKUMROZ left join DOKUMNOTP on (DOKUMNOTP.dokument = :notdok and DOKUMNOTP.DOKUMROZKOR = DOKUMROZ.REF)
             where DOKUMROZ.POZYCJA = :refdokumpoz
             into :refdokumroz, :refdokumnotp
             do begin
               if(:refdokumnotp is null) then
                 insert into DOKUMNOTP(DOKUMENT, DOKUMROZKOR, CENANEW)
                 select :notdok, ref, DOKUMROZ.CENA + :cenamag
                 from DOKUMROZ where POZYCJA = :refdokumpoz;
               else
                 update DOKUMNOTP set CENANEW = CENANEW + :cenamag
                 where REF=:refdokumnotp;
             end
      end
      update POZFAKUSL set ROZLICZONO = 2 where REF = :refpozfakusl;
    end
    doksym = '';
    for select ref from DOKUMNOT where FAKTURA = :faktura and GRUPA = REF
    into :dokmag
    do begin
      if(exists(select ref from dokumnotp where dokument=:dokmag)) then begin
        update DOKUMNOT set AKCEPT = 1, BLOKADA = bin_and(BLOKADA,4) where ref = :dokmag;
        select symbol from dokumnot where ref=:dokmag into :symb;
        if(:symb is null) then symb = '';
        if(coalesce(char_length(:doksym),0)<1000) then begin -- [DG] XXX ZG119346
          if(:doksym<>'') then doksym = :doksym||'; ';
          doksym = :doksym||:symb;
        end
      end else begin
        delete from DOKUMNOT where ref = :dokmag;
      end
    end
  end else begin --zebranie kosztow z WZ i ich podzial wg oddzialow
    for select POZFAKUSL.REF,POZFAKUSL.KWOTA,POZFAKUSL.DATAOD, POZFAKUSL.DATADO, POZFAKUSL.KTMOD,
      POZFAKUSL.KTMDO, POZFAKUSL.KTMMASKA, POZFAKUSL.CENAOD, POZFAKUSL.CENADO
       from POZFAKUSL
       where POZFAKUSL.REFPOZFAK = :pozfakref
         and POZFAKUSL.ROZLICZONO=0
         and POZFAKUSL.TRYB=1
       into :refpozfakusl,:kwota,:dataod,:datado,:ktmod,:ktmdo,:ktmmaska,:cenaod,:cenado
    do begin
      -- znormalizuj wartosci parametrow
      if(:dataod is null) then dataod = '1900-01-01';
      if(:datado is null) then datado = '2999-12-31';
      if(:ktmod is null or :ktmod='') then begin
        select min(ktm) from towary into :ktmod;
      end
      if(:ktmdo is null or :ktmdo='') then begin
        select max(ktm) from towary into :ktmdo;
      end
      if(:ktmmaska is null) then ktmmaska = '';
      if(:cenaod is null) then cenaod = 0;
      if(:cenado is null or :cenado=0) then cenado = 999999999;
      if(:kwota is null) then kwota = 0;
      -- zlap sumaryczna kwote dokumentow magazynowych wydania dotyczacych dostaw danego dostawcy
      select sum(r.wartosc)
        from dokumnag d
        join dokumpoz p on (p.dokument=d.ref)
        join dokumroz r on (r.pozycja=p.ref)
        join dostawy on (dostawy.ref=r.dostawa)
        left join defdokum on (defdokum.symbol=d.typ)
        left join defmagaz on (defmagaz.symbol=d.magazyn)
      where defdokum.wydania=1 and defdokum.koryg=0 and defdokum.zewn=1
--        and d.magazyn = :magazyn
        and d.akcept in (1,8) and dostawy.dostawca=:dostawca
        and d.data>=:dataod
        and d.data<=:datado
        and p.ktm>=:ktmod
        and p.ktm<=:ktmdo
        and p.ktm like '%'||:ktmmaska||'%'
        and r.cena>=:cenaod
        and r.cena<=:cenado
      into :magwartosc;
      if(:magwartosc is null) then magwartosc = 0;
      -- zlap kwote dokumentow magazynowych wydania dotyczacych dostaw danego dostawcy
      -- w podziale na oddzialy
      for select defmagaz.oddzial,sum(r.wartosc)
        from dokumnag d
        join dokumpoz p on (p.dokument=d.ref)
        join dokumroz r on (r.pozycja=p.ref)
        join dostawy on (dostawy.ref=r.dostawa)
        left join defdokum on (defdokum.symbol=d.typ)
        left join defmagaz on (defmagaz.symbol=d.magazyn)
      where defdokum.wydania=1 and defdokum.koryg=0 and defdokum.zewn=1
--        and d.magazyn = :magazyn
        and d.akcept in (1,8) and dostawy.dostawca=:dostawca
        and d.data>=:dataod
        and d.data<=:datado
        and p.ktm>=:ktmod
        and p.ktm<=:ktmdo
        and p.ktm like '%'||:ktmmaska||'%'
        and r.cena>=:cenaod
        and r.cena<=:cenado
      group by defmagaz.oddzial
      into :oddzial,:wartosc
      do begin
        if(:wartosc is null) then wartosc = 0;
        wartosc = :wartosc*:kwota/:magwartosc;
        suspend;
      end
    end
  end
end^
SET TERM ; ^
