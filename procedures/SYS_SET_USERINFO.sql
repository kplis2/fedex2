--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_SET_USERINFO(
      USR varchar(40) CHARACTER SET UTF8                           ,
      PSW varchar(40) CHARACTER SET UTF8                           ,
      USERID varchar(255) CHARACTER SET UTF8                           ,
      USERLOGIN varchar(255) CHARACTER SET UTF8                           ,
      USERPASS varchar(255) CHARACTER SET UTF8                           ,
      IMIE varchar(255) CHARACTER SET UTF8                           ,
      NAZWISKO varchar(255) CHARACTER SET UTF8                           ,
      UWAGI varchar(1024) CHARACTER SET UTF8                           ,
      SUPERVISOR smallint,
      SUPERVISEGROUP varchar(255) CHARACTER SET UTF8                           ,
      EMAIL varchar(255) CHARACTER SET UTF8                           ,
      GRUPA varchar(1024) CHARACTER SET UTF8                           ,
      SENTE smallint,
      MODULES varchar(1024) CHARACTER SET UTF8                           ,
      APPLICATIONS varchar(255) CHARACTER SET UTF8                           ,
      OTABLE varchar(40) CHARACTER SET UTF8                           ,
      OREF integer,
      MUST_CHANGE smallint,
      DAYS_TO_CHANGE integer,
      USERUUID varchar(40) CHARACTER SET UTF8                            = null,
      LICENCELIMIT smallint = 0)
  returns (
      STATUS smallint,
      NEWUSERID integer,
      OLDUSERLOGIN varchar(40) CHARACTER SET UTF8                           ,
      MSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable CURRCRYPTEDPSW varchar(255);
declare variable CURRSUPERVISOR smallint;
declare variable CURRSUPERVISEGROUP varchar(255);
declare variable CURRSENTE smallint;
declare variable CURRUSERID varchar(255);
declare variable CRYPTEDUSERPASS varchar(255);
declare variable OLDUSERPASS varchar(255);
declare variable OLDIMIE varchar(255);
declare variable OLDNAZWISKO varchar(255);
declare variable OLDUWAGI varchar(255);
declare variable OLDSUPERVISOR smallint;
declare variable OLDSUPERVISEGROUP varchar(255);
declare variable OLDEMAIL varchar(255);
declare variable OLDGRUPA varchar(1024);
declare variable OLDSENTE smallint;
declare variable OLDMODULES varchar(1024);
declare variable OLDAPPLICATIONS varchar(255);
declare variable OLDOTABLE varchar(255);
declare variable OLDOREF integer;
declare variable OLDMUST_CHANGE integer;
declare variable OLDDAYS_TO_CHANGE integer;
declare variable OLDLICENCELIMIT smallint;
declare variable currapplications varchar(255);
begin

  currcryptedpsw = shash_hmac('hex','sha1',:psw,:usr);
  -- zautoryzuj usera ktory wywoluje procedure
  curruserid = NULL;
  select first 1 u.supervisor, u.supervisegroup, u.sente, u.userid, u.applications
  from s_users u
  where u.userlogin = :usr and u.userpass = :currcryptedpsw
  into :currsupervisor, :currsupervisegroup, :currsente, :curruserid, :currapplications;
  if(:curruserid is null) then begin
    status = 0;
    msg = 'Brak autoryzacji do zarządzania użytkownikami!';
    suspend;
    exit;
  end
  -- identyfikacja przez userid lub otable i oref
  if((coalesce(:userid,'')='') and coalesce(otable,'')<>'' and coalesce(oref,0)<>0) then begin
    select first 1 userid from s_users where otable=:otable and oref=:oref into :userid;
  end

  -- zweryfikuj czy nowy login nie jest juz uzyty u kogos innego
  if(exists(select first 1 1 from s_users u where u.userlogin=:userlogin and u.userid<>:userid)) then begin
    status = 0;
    msg = 'Login '||:userlogin||' jest w użyciu przez innego użytkownika!';
    suspend;
    exit;
  end

  -- czy poprawiamy istniejacego usera czy dodajemy nowego
  if(:userid<>'' and exists(select first 1 1 from s_users u where u.userid=:userid)) then begin
     -- poprawiamy istniejacego usera
     newuserid = :userid;
     -- nie da sie podwyzszyc stopnia bardziej niz sie jest samemu
     if(:supervisor is not null and :supervisor>:currsupervisor) then supervisor = :currsupervisor;
     -- uzytkownik nie bedacy sente nie moze zakladac uzytkownikow sente
     if(:sente is not null and :sente>:currsente) then sente = :currsente;

     -- bierzemy jego stare dane
     select first 1 u.userlogin, u.userpass, u.imie, u.nazwisko, u.uwagi, u.supervisor, u.supervisegroup,
       u.email, u.grupa, u.sente, u.modules, u.applications, u.otable, u.oref,
       u.must_change, u.days_to_change, U.LICENCELIMIT
       from s_users u
       where u.userid=:userid
      into :olduserlogin, :olduserpass, :oldimie, :oldnazwisko, :olduwagi, :oldsupervisor, :oldsupervisegroup,
        :oldemail, :oldgrupa, :oldsente, :oldmodules, :oldapplications, :oldotable, :oldoref,
        :oldmust_change, :olddays_to_change, :OLDLICENCELIMIT;

     if(:oldsupervisegroup is null) then oldsupervisegroup = '';

     -- pola ktore sa NULL zamien na stare wartosci
     if(:userlogin is not null and :userlogin<>:olduserlogin and :userpass is null) then begin
       status = 0;
       msg = 'Zmieniając login należy nadać nowe haslo!';
       suspend;
       exit;
     end
     userlogin = coalesce(:userlogin,:olduserlogin);
     if(:userpass is not null) then
       crypteduserpass = shash_hmac('hex','sha1',:userpass,:userlogin);
     else
       crypteduserpass = :olduserpass;
     imie = coalesce(:imie,:oldimie);
     nazwisko = coalesce(:nazwisko,:oldnazwisko);
     uwagi = coalesce(:uwagi,:olduwagi);
     if(:oldsupervisor = 2) then supervisor = 2;
     supervisor = coalesce(:supervisor,:oldsupervisor);
     supervisegroup = coalesce(:supervisegroup,:oldsupervisegroup);
     email = coalesce(:email,:oldemail);
     grupa = coalesce(:grupa,:oldgrupa);
     sente = coalesce(:sente,:oldsente);
     modules = coalesce(:modules,:oldmodules);
     applications = coalesce(:applications,:oldapplications);
     otable = coalesce(:otable,:oldotable);
     oref = coalesce(:oref,:oldoref);
     must_change = coalesce(:must_change,:oldmust_change);
     days_to_change = coalesce(:days_to_change,:olddays_to_change);
     LICENCELIMIT = COALESCE(:LICENCELIMIT,:OLDLICENCELIMIT);
     -- czy biezacy user ma w ogole prawo go poprawic
     if(:userid = :curruserid or  -- kazdy moze poprawic samego siebie
        (:currsupervisor=1 and coalesce(:oldsupervisegroup,'') like :currsupervisegroup||'%' and coalesce(:supervisegroup,'') like :currsupervisegroup||'%') or  -- admin moze poprawic wszystkich w swojej supervisegroup
        (:currsupervisor>1) or   -- developer moze poprawic wszystkich
        (:currsente=1) or  -- sente moze poprawic wszystkich
        (:currsupervisor=0 and coalesce(:currapplications,'') <> '' and strmulticmp(';EP;',:currapplications) > 0 and :otable = 'PERSONS') --modyfikacja na potrzeby eHRM
       ) then begin

       -- popraw usera
       update s_users u set
         u.userlogin = :userlogin,
         u.userpass = :crypteduserpass,
         u.imie = :imie,
         u.nazwisko = :nazwisko,
         u.uwagi = :uwagi,
         u.email = :email,
         u.must_change = :must_change,
         U.LICENCELIMIT = :LICENCELIMIT
       where u.userid=:userid;

       -- poprawa danych wrazliwych
       if((:currsupervisor=1 and coalesce(:oldsupervisegroup,'') like :currsupervisegroup||'%' and coalesce(:supervisegroup,'') like :currsupervisegroup||'%') or  -- admin moze poprawic wszystkich w swojej supervisegroup
          (:currsupervisor>1) or   -- developer moze poprawic wszystkich
          (:currsente=1)  -- sente moze poprawic wszystkich
         ) then begin

         update s_users u set
           u.supervisor = :supervisor,
           u.supervisegroup = :supervisegroup,
           u.grupa = :grupa,
           u.sente = :sente,
           u.modules = :modules,
           u.applications = :applications,
           u.otable = :otable,
           u.oref = :oref,
           u.days_to_change = :days_to_change,
           U.LICENCELIMIT = :LICENCELIMIT
         where u.userid=:userid;
       end
       status = 1;
       msg = 'Zaktualizowano dane użytkownika.';
       suspend;
       exit;
     end else begin
       status = 0;
       msg = 'Brak uprawnień do poprawienia danych użytkownika';
       suspend;
       exit;
     end

  end else begin
     -- dodajemy nowego usera
     userlogin = coalesce(:userlogin,'');
     userpass = coalesce(:userpass, '');
     crypteduserpass = shash_hmac('hex','sha1',:userpass,:userlogin);
     imie = coalesce(:imie,'');
     nazwisko = coalesce(:nazwisko,'');
     uwagi = coalesce(:uwagi,'');
     supervisor = coalesce(:supervisor,0);
     supervisegroup = coalesce(:supervisegroup,'');
     email = coalesce(:email,'');
     grupa = coalesce(:grupa,'');
     sente = coalesce(:sente,0);
     modules = coalesce(:modules,'');
     applications = coalesce(:applications,'');
     otable = coalesce(:otable,'OPERATOR');
     oref = coalesce(:oref,0);
     must_change = coalesce(:must_change,0);
     days_to_change = coalesce(:days_to_change,0);
     LICENCELIMIT = COALESCE(:LICENCELIMIT,0);
     -- zweryfikuj czy wprowadzono wymagane dane
     if(:userlogin='') then begin
       status = 0;
       msg = 'Nie wypełniono pola: Login!';
       suspend;
       exit;
     end
     if(:userpass='') then begin
       status = 0;
       msg = 'Nie wypełniono pola: Hasło!';
       suspend;
       exit;
     end

     -- czy dodajemy go w obrebie uprawnien biezacego usera
     if((:currsupervisor=1 and coalesce(:supervisegroup,'') like :currsupervisegroup||'%') or  -- admin moze dodac w swojej supervisegroup
        (:currsupervisor>1) or   -- developer moze dodac kazdego
        (:currsente=1) or  -- sente moze dodac kazdego
        (:currsupervisor=0 and coalesce(:currapplications,'') <> '' and strmulticmp(';EP;',:currapplications) > 0 and :otable = 'PERSONS') --modyfikacja na potrzeby eHRM
       ) then begin
       -- nie da sie zalozyc stopnia wyzszego niz sie jest samemu
       if(:supervisor>:currsupervisor) then supervisor = :currsupervisor;
       -- uzytkownik nie bedacy sente nie moze zakladac uzytkownikow sente
       if(:sente>:currsente) then sente = :currsente;
       -- zaloz usera
       if(:userid is null or (:userid = '0')) then newuserid = cast(gen_id(GEN_USERS,1) as varchar(30));
       else newuserid = :userid;

       insert into s_users (userid,userlogin,userpass,imie,nazwisko,uwagi,email,
           supervisor,supervisegroup,grupa,sente,modules,applications,
           otable,oref,must_change,days_to_change,LICENCELIMIT)
       values (:newuserid,:userlogin,:crypteduserpass,:imie,:nazwisko,:uwagi,:email,
           :supervisor,:supervisegroup,:grupa,:sente,:modules,:applications,
           :otable,:oref,:must_change,:days_to_change,:LICENCELIMIT);

       -- skopiuj userlogins
       execute procedure COPY_USERLOGINS(:usr, :psw, :curruserid, :newuserid) returning_values :status;

       status = 2;
       msg = 'Dodano nowego użytkownika.';
       suspend;
       exit;
     end else begin
       status = 0;
       msg = 'Brak uprawnień do dodania nowego użytkownika.';
       suspend;
       exit;
     end
  end
end^
SET TERM ; ^
