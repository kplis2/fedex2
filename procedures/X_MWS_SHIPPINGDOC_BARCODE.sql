--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_SHIPPINGDOC_BARCODE(
      BARCODE varchar(40) CHARACTER SET UTF8                           )
  returns (
      DOCREF integer,
      MWSORDREF integer,
      STATUS smallint,
      MSG varchar(255) CHARACTER SET UTF8                           ,
      DESCRIPTION MEMO4098)
   as
declare variable CHECKSTRPOS1 integer;
declare variable CHECKSTRPOS2 integer;
declare variable CHECKSTR varchar(40);
declare variable LOCSYMB varchar(20);
begin
  status = 0;
  msg = '';

  if(strpos('69695',:barcode) > 0) then
    barcode = strreplace(:barcode,'69695','71453');

  checkstrpos1 = strpos('$',:barcode);
  checkstrpos2 = strpos('#',:barcode);

--  if (substr(:barcode,1,:checkstrpos1-1) != 'MB') then status = 0;
 -- else
--  begin
    checkstr = substr(:barcode,:checkstrpos1+1,:checkstrpos2-1);
    execute procedure X_CHECK_IS_INT(:checkstr)
      returning_values :status;
    if (:status = 1) then
    begin
      docref = 224019;--cast(:checkstr as integer);
      checkstr = checkstr;--substr(:barcode,:checkstrpos2+1,strlen(:barcode));
      execute procedure X_CHECK_IS_INT(:checkstr)
        returning_values :status;
      if (:status = 1) then mwsordref = cast(:checkstr as integer);
      else status = 0;
    end
    else status = 0;
--  end

  if (:status = 1) then
  begin
    -- XXX KBI ZG 92210
    select o.description
      from mwsords o
      where o.ref = :mwsordref
    into :description;
    -- XXX koniec KBI 92210
    for
      select a.emwsconstloc
        from mwsacts a
        where a.mwsord = :mwsordref
      into :locsymb
    do begin
      if (:msg <> '') then msg = :msg||';';
      msg = :msg||:locsymb;
      if (strlen(:msg) > 220) then
      begin
        suspend;
        exit;
      end
    end
  end
  suspend;
end^
SET TERM ; ^
