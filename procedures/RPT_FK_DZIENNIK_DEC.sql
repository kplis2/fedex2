--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_DZIENNIK_DEC(
      COMPANY integer,
      ODOKRESU varchar(6) CHARACTER SET UTF8                           ,
      DOOKRESU varchar(6) CHARACTER SET UTF8                           ,
      REJESTR varchar(10) CHARACTER SET UTF8                           )
  returns (
      KB_OOP_WINIEN numeric(14,2),
      KB_OOP_MA numeric(14,2),
      KB_OBO_WINIEN numeric(14,2),
      KB_OBO_MA numeric(14,2),
      KB_OPR_WINIEN numeric(14,2),
      KB_OPR_MA numeric(14,2),
      KPB_OOP_WINIEN numeric(14,2),
      KPB_OOP_MA numeric(14,2),
      KPB_OBO_WINIEN numeric(14,2),
      KPB_OBO_MA numeric(14,2),
      KPB_OPR_WINIEN numeric(14,2),
      KPB_OPR_MA numeric(14,2))
   as
begin
  /* dla okresu biezacego */
  select sum(case when a.bktype < 2 then d.debit else 0 end),
      sum(case when a.bktype < 2 then d.credit else 0 end),
      sum(case when a.bktype = 2 then d.debit else 0 end),
      sum(case when a.bktype = 2 then d.credit else 0 end)
    from bkdocs b
      join decrees d on (d.bkdoc = b.ref)
      join bkaccounts a on (a.ref = d.accref)
    where (d.bkreg = :rejestr or :rejestr = '')
      and b.company = :company
      and b.period >= :odokresu
      and b.period <= :dookresu
    and b.status = 3
    into :kb_obo_winien, :kb_obo_ma, :kpb_obo_winien, :kpb_obo_ma;

  if (:kb_obo_winien is null) then kb_obo_winien = 0;
  if (:kb_obo_ma is null) then kb_obo_ma = 0;
  if (:kpb_obo_winien is null) then kpb_obo_winien = 0;
  if (:kpb_obo_ma is null) then kpb_obo_ma = 0;

    /* dla okresu poprzedniego / okresów poprzednich od poczatku roku */
  select sum(case when a.bktype < 2 then d.debit else 0 end),
      sum(case when a.bktype < 2 then d.credit else 0 end),
      sum(case when a.bktype = 2 then d.debit else 0 end),
      sum(case when a.bktype = 2 then d.credit else 0 end)
    from bkdocs b
      join decrees d on (d.bkdoc = b.ref)
      join bkaccounts a on (a.ref = d.accref)
    where (d.bkreg = :rejestr or :rejestr = '')
      and b.company = :company
      and b.period > substring(:odokresu from 1 for 4)||'00'
      and b.period < :odokresu -- dla wszystkich okresów od poczatku roku
    and b.status = 3
      --and b.period = (select okres from datatookres(:odokresu,0,-1,0,1)) -- dla jednego poprzedniego okresu
    into :kb_oop_winien, :kb_oop_ma, :kpb_oop_winien, :kpb_oop_ma;

  if (:kb_oop_winien is null) then kb_oop_winien = 0;
  if (:kb_oop_ma is null) then kb_oop_ma = 0;
  if (:kpb_oop_winien is null) then kpb_oop_winien = 0;
  if (:kpb_oop_ma is null) then kpb_oop_ma = 0;

  /* obroty od poczatku roku */
  kb_opr_winien = kb_obo_winien + kb_oop_winien;
  kb_opr_ma = kb_obo_ma + kb_oop_ma;
  kpb_opr_winien = kpb_obo_winien + kpb_oop_winien;
  kpb_opr_ma = kpb_obo_ma + kpb_oop_ma;

  suspend;
end^
SET TERM ; ^
