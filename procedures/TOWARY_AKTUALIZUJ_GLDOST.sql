--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TOWARY_AKTUALIZUJ_GLDOST(
      TYP varchar(1) CHARACTER SET UTF8                           ,
      DATA timestamp)
   as
declare variable wersjaref integer;
declare variable zapyt varchar(255);
declare variable cnt varchar(255);
declare variable dostawca integer;
declare variable mincena numeric(14,4);
declare variable cena_fab numeric(14,4);
declare variable cena_koszt numeric(14,4);
declare variable gl smallint;
declare variable cenazakgl smallint;
declare variable ktm varchar(40);
declare variable stawka numeric(14,4);
declare variable przelicz numeric(14,4);
declare variable jedn integer;
declare variable symboludost varchar(20);
begin
  if(typ is null) then typ='';
  for select wersje.ref, towary.ktm from wersje join towary on (wersje.ktm = towary.ktm)
  where wersje.akt = 1 and towary.akt = 1 and towary.lastcenzmiana = :data
  into :wersjaref, :ktm
   do begin
     mincena = -1;
     gl = 0;
     cenazakgl = 0;
     stawka = 0;
     przelicz = 0;
     jedn = 0;
     if(:typ='F') then zapyt = 'select min(dostcen.cena_fab) ';
     else zapyt = 'select min(dostcen.cenanet) ';
     zapyt = :zapyt || 'from dostcen join dostawcy on (dostawcy.ref = dostcen.dostawca) ';
     zapyt = :zapyt || 'where dostawcy.aktywny = 1 and dostcen.akt = 1 and wersjaref = '||:wersjaref;
     execute statement :zapyt into :mincena;
     if (:mincena>-1) then begin
       zapyt = 'select max(dostawcy.ref) from dostcen join dostawcy on (dostawcy.ref=dostcen.dostawca) where dostawcy.aktywny = 1 and dostcen.akt = 1 and wersjaref = '||:wersjaref||' and '||:mincena||' = ';
       if(:typ='F') then zapyt = :zapyt || ' dostcen.cena_fab';
       else zapyt = :zapyt || ' dostcen.cenanet ';
       execute statement :zapyt into :dostawca;
       select gl, cenazakgl, jedn, cenanet, cena_fab, cena_koszt, symbol_dost
       from dostcen where dostawca = :dostawca and wersjaref = :wersjaref and akt = 1
       into :gl, cenazakgl, :jedn, :mincena, :cena_fab, :cena_koszt, :symboludost;
       if (gl = 0) then
         update DOSTCEN set GL = 1 where WERSJAREF = :wersjaref and DOSTAWCA <> :dostawca;
       if(:cenazakgl = 1) then begin
         update DOSTCEN set CENAZAKGL = 0 where WERSJAREF = :wersjaref and DOSTAWCA <> :dostawca;
         select vat.STAWKA from TOWARY join VAT on (TOWARY.VAT = VAT.GRUPA) where TOWARY.KTM = :ktm into :stawka;
         stawka = 1 + :stawka/100;
         if(jedn > 0) then
           select PRZELICZ from TOWJEDN where REF=:jedn into :przelicz;
         else
          przelicz = 1;
         update WERSJE set CENA_ZAKN = :mincena/ :przelicz, CENA_ZAKB = :mincena * :stawka / :przelicz,
                       CENA_FABN = :cena_fab / :przelicz, cena_FABB = :cena_FAB * :stawka / :przelicz,
                       CENA_KOSZTN = :cena_koszt / :przelicz, cena_KOSZTB = :cena_koszt * :stawka / :przelicz,
                       SYMBOL_DOST = :symboludost
         where ref = :wersjaref;
       end
     end
   end
end^
SET TERM ; ^
