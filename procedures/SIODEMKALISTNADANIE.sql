--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SIODEMKALISTNADANIE(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable pvalue varchar(255);
declare variable parent_lvl_0 integer;
declare variable parent_lvl_1 integer;
declare variable parent_lvl_2 integer;
declare variable parent_lvl_3 integer;
declare variable symbol varchar(255);
declare variable toplace varchar(20);
declare variable kontrahent varchar(255);
declare variable adres varchar(255);
declare variable miasto varchar(255);
declare variable kodp varchar(6);
declare variable slodef integer;
declare variable slopoz integer;
declare variable uwagi varchar(255);
declare variable uwagisped varchar(255);
declare variable waga numeric(5,1);
declare variable odbiorcaid integer;
declare variable flagisped varchar(40);
declare variable firma smallint;
declare variable typ slotyp_id;
declare variable telefon phones_id;
declare variable email emails_id;
declare variable nrklienta integer;
declare variable wartdeklar numeric(8,2);
declare variable pobranie numeric(8,2);
declare variable kluczapi varchar(60);
declare variable stm string1024;
declare variable operator operator_id;
declare variable termdost timestamp_id;
begin
  nrklienta = '43892635';
  kluczapi = 'E85BF1498DF3D6C77CA955E2A9621557';
  firma = 1;
  stm = 'select symbol, toplace, kontrahent, adres, miasto,
    kodp, slodef,  slopoz, uwagi, uwagisped, waga, odbiorcaid, flagisped, wartdeklar,
    pobranie, TERMDOST
    from '||:otable||' where ref = '||:oref ;
  execute statement stm
    into :symbol, :toplace, :kontrahent, :adres, :miasto,
       :kodp, slodef, slopoz,uwagi,  :uwagisped, :waga, :odbiorcaid, :flagisped, :wartdeklar,
       :pobranie, termdost;
  uwagisped = uwagi ||' '||uwagisped;
  execute procedure get_global_param('AKTUOPERATOR') returning_values :operator;
  if (:slodef in (1,2)) then
  begin
    select typ from slodef where ref=:slodef into :typ;
    execute statement 'select firma, telefon, email from '||:typ||' where REF = '||:slopoz into
      :firma, :telefon, :email;
  end
  --Generujemy naglowek pliku XML
  id = 0;
  name = 'ListNadanie';
  parent = null;
  params = 'xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"  xmlns:ns1="http://app.siodemka.com/mm7ws/type/"';
  val = null;
  suspend;

  parent_lvl_0 = 0;
  --Rozpoczynamy naglowek
  id = :id + 1; --1
  name = 'listNadanieElement';
  parent = parent_lvl_0;
  params = null;
  val  = null;
  parent_lvl_0 = :id;
  suspend;
    --
    id = :id + 1;
    name = 'przesylka';
    parent = parent_lvl_0;
    params = null;
    val  = null;
    parent_lvl_1 = :id;
    suspend;
          --
      id = :id + 1;
      name = 'rodzajPrzesylki';
      parent = parent_lvl_1;
      params = null;
      val  = 'K';
      suspend;
          --
      id = :id + 1;
      name = 'placi';
      parent = parent_lvl_1;
      params = null;
      if (:flagisped containing ';P;') then
        val = '2';
      else
        val  = '1';
      suspend;
      --
      id = :id + 1;
      name = 'formaPlatnosci';
      parent = parent_lvl_1;
      params = null;
      if (:flagisped containing ';P;') then
        val = 'G';
      else
        val = 'G'; -- dopuki nie dostana pozwolenia na platnosc przelewek to bedzie gotówka
      suspend;
      --
      id = :id + 1;
      name = 'nadawca';
      parent = parent_lvl_1;
      params = null;
      val  = null;
      parent_lvl_2 = :id;
      suspend;
      --
        id = :id + 1;
        name = 'numer';
        parent = parent_lvl_2;
        params = null;
        val  = :nrklienta; -- numer klienta (nadawcy)
        suspend;
        --

        id = :id + 1;
        name = 'emailKontaktNadawca';
        parent = parent_lvl_2;                        --email nadawcy
        params = null;
        val  = '';  -- email kontaktowy nadawcy
        suspend;
        --
        id = :id + 1;
        name = 'telKontaktNadawca';                   --telefon nadawcy
        parent = parent_lvl_2;
        params = null;
        val  = '';  -- telefon kontaktowy nadawcy
        suspend;
      --
      id = :id + 1;
      name = 'odbiorca';
      parent = parent_lvl_1;
      params = null;
      val  = null;
      parent_lvl_2 = :id;
      suspend;
        --
        id = :id + 1;
        name = 'czyFirma';
        parent = parent_lvl_2;
        params = null;
        val  = :firma;
        suspend;
        --
        id = :id + 1;
        name = 'nazwa';
        parent = parent_lvl_2;
        params = null;
        val  = :kontrahent;  -- nazwa odbiorcy
        suspend;
        --
        id = :id + 1;
        name = 'kodKraju';
        parent = parent_lvl_2;
        params = null;
        val  = 'PL';  --
        suspend;
        --
        id = :id + 1;
        name = 'kod';
        parent = parent_lvl_2;
        params = null;
        val  = :kodp;  -- ZipCode
        suspend;
        --
        id = :id + 1;
        name = 'miasto';
        parent = parent_lvl_2;
        params = null;
        val  = :miasto;  --  miasto
        suspend;
        --
        id = :id + 1;
        name = 'ulica';
        parent = parent_lvl_2;
        params = null;
        val  = :adres;  -- ulica
        suspend;
        --
        id = :id + 1;
        name = 'nrDom';
        parent = parent_lvl_2;
        params = null;
        val  = '.';  -- numer domu
        suspend;
        --
        id = :id + 1;
        name = 'telKontakt';
        parent = parent_lvl_2;
        params = null;
        val  = substring(coalesce(:telefon,'') from 1 for 100);  -- telefon kontaktowy (odbiorca)
        suspend;
        --
        id = :id + 1;
        name = 'emailKontakt';
        parent = parent_lvl_2;
        params = null;
        val  = substring(coalesce(:email,'') from 1 for 100);  -- email kontaktowy (odbiorca)
        suspend;
        --
      id = :id + 1;
      name = 'platnik';
      parent = parent_lvl_1;
      params = null;
      val  = null;
      parent_lvl_2 = :id;
      suspend;
        --
        id = :id + 1;
        name = 'numerPlatnika';
        parent = parent_lvl_2;
        params = null;
        if (flagisped containing ';P;') then
          val = '';
        else
          val  = :nrklienta;  -- numer platnika
        suspend;
      --
      id = :id + 1;
      name = 'uslugi';
      parent = parent_lvl_1;
      params = null;
      val  = null;
      parent_lvl_2 = :id;
      suspend;
        --
        id = :id + 1;
        name = 'zkld';
        parent = parent_lvl_2;
        params = null;
        if (:flagisped containing ';A;') then
          val = '1';
        else
          val = '0';
        suspend;
        --
        id = :id + 1;
        name = 'zd';
        parent = parent_lvl_2;
        params = null;
        if (:flagisped containing ';B;') then
          val = '1';
        else
          val = '0';
        suspend;
        --
        id = :id + 1;
        name = 'ubezpieczenie';
        parent = parent_lvl_2;
        params = null;
        val  = null;  --
        parent_lvl_3 = :id;
        suspend;
          --
          id = :id + 1;
          name = 'kwotaUbezpieczenia';
          parent = parent_lvl_3;
          params = null;
          if (:flagisped containing ';C;') then
            val = :wartdeklar;
          else
            val = null;
          suspend;
                --
          id = :id + 1;
          name = 'opisZawartosci';
          parent = parent_lvl_3;
          params = null;
          if (:flagisped containing ';C;') then begin
            select list(opis,', ')from listywysdpoz where dokument = :oref into :val;
            --val  = ' ';  -- dorobic mechanizm opisu zawartosci przesylki
          end
          else
            val = null;
          parent_lvl_3 = :id;
          suspend;
        --
        id = :id + 1;
        name = 'pobranie';
        parent = parent_lvl_2;
        params = null;
        val  = null;  --
        parent_lvl_3 = :id;
        suspend;
          --
            id = :id + 1;
            name = 'kwotaPobrania';
            parent = parent_lvl_3;
            params = null;
            if (flagisped containing ';P;') then
              val = :pobranie;
            else
              val = '';
            suspend;
            --
            id = :id + 1;
            name = 'formaPobrania';
            parent = parent_lvl_3;
            params = null;
            if (flagisped containing ';P;') then
              val = 'P'; -- forma zwrotu pobrania P przekaz pocztowy
            else
              val = '';
            suspend;
            --
            id = :id + 1;
            name = 'nrKonta';
            parent = parent_lvl_3;             -- niewymagane dla przekazu pocztowego
            params = null;
            val  = '';
            parent_lvl_3 = :id;
            suspend;
          --
        id = :id + 1;
        name = 'awizacjaTelefoniczna';
        parent = parent_lvl_2;
        params = null;
        val  = '0';  --
        suspend; 
        --
        id = :id + 1;
        name = 'potwNadEmail';
        parent = parent_lvl_2;
        params = null;
        val  = '0';  --
        suspend;
        --
        id = :id + 1;
        name = 'potwDostEmail';
        parent = parent_lvl_2;
        params = null;
        val  = '0';  --
        suspend;
        --
        id = :id + 1;
        name = 'potwDostSMS';
        parent = parent_lvl_2;
        params = null;
        val  = '0';  --
        suspend;
        --
        id = :id + 1;
        name = 'skladowanie';
        parent = parent_lvl_2;
        params = null;
        val  = '0';  --
        suspend; 
        --
        id = :id + 1;
        name = 'nadOdbPKP';
        parent = parent_lvl_2;
        params = null;
        val  = '0';  --
        suspend;
        --
        id = :id + 1;
        name = 'odbNadgodziny';
        parent = parent_lvl_2;
        params = null;
        val  = '0';  --
        suspend; 
        --
        id = :id + 1;
        name = 'odbWlas';
        parent = parent_lvl_2;
        params = null;
        val  = '0';  --
        suspend; 
        --
        id = :id + 1;
        name = 'palNextDay';
        parent = parent_lvl_2;
        params = null;
        val  = '0';  --
        suspend; 
        --
        id = :id + 1;
        name = 'osobaFiz';
        parent = parent_lvl_2;
        params = null;
        val  = '0';  --
        suspend;   
        --
        id = :id + 1;
        name = 'market';
        parent = parent_lvl_2;
        params = null;
        val  = '0';  --
        suspend;  
        --
        id = :id + 1;
        name = 'zastrzDorNaGodz';
        parent = parent_lvl_2;
        params = null;
        val  = '00:00';  --
        suspend; 
        --
        id = :id + 1;
        name = 'zastrzDorNaDzien';
        parent = parent_lvl_2;
        params = null;
        val  = 'B';  --
        suspend; 
      --
      id = :id + 1;
      name = 'paczki';
      parent = parent_lvl_1;
      params = null;
      val  = null;  --
      parent_lvl_2 = :id;
      suspend;
      -- dla kazdej paczki dodajemy wpis
      for select waga from listywysdroz_opk where listwysd = :oref
        into :waga
      do begin
        --
        id = :id + 1;
        name = 'paczka';
        parent = parent_lvl_2;
        params = null;
        val  = null;  --
        parent_lvl_3 = :id;
        suspend;
          --
          id = :id + 1;
          name = 'typ';
          parent = parent_lvl_3;
          params = null;
          val  = 'PC';  --  PC - paczka; PL - paleta; KP - koperta;
          suspend;
          --
          id = :id + 1;
          name = 'waga';
          parent = parent_lvl_3;
          params = null;
          val  = :waga;  --
          suspend;
          --
          id = :id + 1;
          name = 'gab1';
          parent = parent_lvl_3;
          params = null;
          val  = '20';  --  dlugosc   paczki
          suspend;
          --
          id = :id + 1;
          name = 'gab2';
          parent = parent_lvl_3;
          params = null;
          val  = '20';  --  szerokosc  paczki
          suspend;
          --
          id = :id + 1;
          name = 'gab3';
          parent = parent_lvl_3;
          params = null;
          val  = '20';  --  wysokosc  paczki
          suspend;
          --
          id = :id + 1;
          name = 'ksztalt';
          parent = parent_lvl_3;
          params = null;
          val  = '1';  -- czy paczka posiada wystajace elementy 1 tak 0 nie
          suspend;
      end
      --
      id = :id + 1;
      name = 'potwierdzenieNadania';
      parent = parent_lvl_1;
      params = null;
      val  = null;  --
      parent_lvl_2 = :id;
      suspend;
        --
        id = :id + 1;
        name = 'dataNadania';             --data nadania przesyłki w formacie YYYY-MM-DD HH24:MI np. 2009-08-25 17:00 Data nie może być mniejsza niż obecna.
        parent = parent_lvl_2;
        params = null;
        val  = current_timestamp(0) + 1;
        parent_lvl_3 = :id;
        suspend;
        --
        id = :id + 1;
        name = 'numerKuriera';            -- numer kuriera podejmującego przesyłkę
        parent = parent_lvl_2;
        params = null;
        val  = 001;-- null;  --
        parent_lvl_3 = :id;
        suspend;
        --
        id = :id + 1;
        name = 'podpisNadawcy';           -- podpis nadawcy (np. nazwisko osoby nadającej przesyłkę)
        parent = parent_lvl_2;
        params = null;
        val = :operator;
        parent_lvl_3 = :id;
        suspend;
      --
      id = :id + 1;
      name = 'uwagi';
      parent = parent_lvl_1;
      params = null;
      val  = :uwagi;  --
      parent_lvl_2 = :id;
      suspend;
    --
    id = :id + 1;
    name = 'klucz';
    parent = parent_lvl_0;
    params = null;
    val  = :kluczapi;  -- klucz API
    parent_lvl_1 = :id;
    suspend;
end^
SET TERM ; ^
