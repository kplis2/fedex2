--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDEPOS_AMOUNTZREAL(
      PRSCHEDGUIDEPOS integer)
   as
declare variable amountzreal numeric(14,4);
declare variable amountoverlimit numeric(14,4);
begin
  select sum(d.quantity)
    from prschedguidedets d
    where d.out > 0 and d.overlimit = 0 and d.prschedguidepos = :prschedguidepos
    into amountzreal;
  select sum(d.quantity)
    from prschedguidedets d
    where d.out > 0 and d.overlimit = 1 and d.prschedguidepos = :prschedguidepos
    into amountoverlimit;
  update prschedguidespos set amountzreal = :amountzreal, amountoverlimit = :amountoverlimit
    where ref = :prschedguidepos;
end^
SET TERM ; ^
