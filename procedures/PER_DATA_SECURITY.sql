--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PER_DATA_SECURITY(
      PERSON integer,
      OPERATOR integer,
      KOD smallint,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      DATA timestamp)
   as
begin
  if (not exists (select ref from epersdatasecur
                    where person = :person and kod = :kod and descript = :descript
                      and operator = :operator and regdate = :data)) then
    insert into epersdatasecur (person, kod, descript, operator, regdate)
      values (:person, :kod, :descript, :operator, :data);
end^
SET TERM ; ^
