--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_VAT7_UE(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      DK39UE numeric(14,2))
  returns (
      WSP45 numeric(14,2),
      K20UE numeric(14,2),
      K21UE numeric(14,2),
      K22UE numeric(14,2),
      K23UE numeric(14,2),
      K24 varchar(6) CHARACTER SET UTF8                           ,
      K25 numeric(14,2),
      K26 numeric(14,2),
      K27 varchar(6) CHARACTER SET UTF8                           ,
      K28 numeric(14,2),
      K28UE numeric(14,2),
      K29 numeric(14,2),
      K29UE numeric(14,2),
      K24UE numeric(14,2),
      K30UE numeric(14,2),
      K25UE numeric(14,2),
      K31UE numeric(14,2),
      K26UE numeric(14,2),
      K32UE numeric(14,2),
      K27UE numeric(14,2),
      K33UE numeric(14,2),
      K37 numeric(14,2),
      K38 numeric(14,2),
      K39UE numeric(14,2),
      K40 numeric(14,2),
      K42 numeric(14,2),
      K44 numeric(14,2),
      K38UE numeric(14,2),
      K41UE numeric(14,2),
      K42UE numeric(14,2),
      K43UE numeric(14,2),
      K44UE numeric(14,2),
      K45UE numeric(14,2),
      K46UE numeric(14,2),
      K46 numeric(14,2),
      K47UE numeric(14,2),
      K48 numeric(14,2),
      K49 numeric(14,2),
      K50 numeric(14,2),
      K51 numeric(14,2),
      K52 numeric(14,2),
      K53 numeric(14,2),
      K54 numeric(14,2),
      K55 numeric(14,2),
      K56 numeric(14,2),
      K57 numeric(14,2),
      K57UE numeric(14,2),
      K58 numeric(14,2),
      K50UE numeric(14,2),
      K48UE numeric(14,2),
      K49UE numeric(14,2),
      K63 numeric(14,2),
      K52UE numeric(14,2),
      K51UE numeric(14,2),
      K66 numeric(14,2),
      K53UE numeric(14,2),
      K200A numeric(14,2),
      K58UE numeric(14,2),
      K55UE numeric(14,2))
   as
declare variable vatgr varchar(6);
  declare variable netv numeric(14,2);
  declare variable vatv numeric(14,2);
  declare variable taxgr varchar(10);
begin
  WSP45 = 100; /*domyslnie caly mozna odliczyc*/
  k20UE = 0;
  K28UE = 0;
  K29UE = 0;
  K30UE = 0;
  K31UE = 0;
  K32UE = 0;
  K33UE = 0;
  k37 = 0;
  k38 = 0;
  k39UE = :dK39UE;
  k40 = 0;
  k42 = 0;
  k44 = 0;
  K38UE = 0;
  K41UE = 0;
  K42UE = 0;
  K43UE = 0;
  K44UE = 0;
  K45UE = 0;
  K46UE = 0;
  k46 = 0;
  k48 = 0;
  k50 = 0;
  k52 = 0;
  k54 = 0;
  k56 = 0;
  k57 = 0;
  k57UE = 0;
  k58 = 0;
  k48UE = 0;
  k49UE = 0;
  k52UE = 0;
  k51UE = 0;
  k53UE = 0;
  K200A =0;

  if (k39UE is null) then
    k39UE = 0;

  /* sprzedaz */
  for
    select bkvatpos.vatgr, cast(sum(bkvatpos.netv)*100 as integer)/100, cast(sum(bkvatpos.vatv)*100 as integer)/100
    from bkdocs
    join vatregs on (bkdocs.vatperiod=:period and vatregs.symbol=bkdocs.vatreg and vatregs.vtype=0 and bkdocs.status>0)
    join bkvatpos on (bkvatpos.bkdoc=bkdocs.ref)
    group by bkvatpos.vatgr, bkvatpos.taxgr
    into :vatgr, :netv, :vatv
  do begin
    if (:vatgr='22') then
    begin
      k26UE = :netv;
      k27UE = :vatv;
    end
    else if (:vatgr='07') then
    begin
      k24UE = :netv;
      k25UE = :vatv;
    end
    else if (:vatgr='03') then
    begin
      k22UE = :netv;
      k23UE = :vatv;
    end
    else if (:vatgr='ZW') then
    begin
      k20UE = :netv;
    end
    else if (:vatgr='00') then
    begin
      if (taxgr='E21') then
      begin
        k200a = netv;
      end else
        k21UE = :netv;
    end
    else if (:k27 is null) then
    begin
      k27 = :vatgr;
      k28 = :netv;
      k29 = :vatv;
    end
    else if (:k24 is null) then
    begin
      k24 = :vatgr;
      k25 = :netv;
      k26 = :vatv;
    end
    else
      exception test_break;
    k37 = :k37 + :netv;
    k38 = :k38 + :vatv;
  end

  /* jeszcze eksport*/
  select cast(sum(bkvatpos.netv)*100 as integer)/100
    from bkdocs
    join vatregs on (bkdocs.vatperiod=:period and vatregs.symbol=bkdocs.vatreg and vatregs.vtype=2 and bkdocs.status>0)
    join bkvatpos on (bkvatpos.bkdoc=bkdocs.ref)
    into :k29UE;

  if (k29UE is null) then
    k29UE = 0;
  k29UE=k29UE+K200A;

  /* dostawa wewnatrzwspolnotowa */
    select cast(sum(bkvatpos.netv)*100 as integer)/100
    from bkdocs
    join vatregs on (bkdocs.vatperiod=:period and vatregs.symbol=bkdocs.vatreg and vatregs.vtype=1 and bkdocs.status>0)
    join bkvatpos on (bkvatpos.bkdoc=bkdocs.ref)
    into :k28UE;

  /* nabycie wewnatrzwspolnotowe */
    select cast(sum(bkvatpos.netv)*100 as integer)/100, cast(sum(bkvatpos.vatv)*100 as integer)/100
    from bkdocs
    join vatregs on (bkdocs.vatperiod=:period and vatregs.symbol=bkdocs.vatreg and vatregs.vtype=5 and bkdocs.status>0)
    join bkvatpos on (bkvatpos.bkdoc=bkdocs.ref)
    into :k30UE, :k31UE;

  /* import uslug */
    select cast(sum(bkvatpos.netv)*100 as integer)/100, cast(sum(bkvatpos.vatv)*100 as integer)/100
    from bkdocs
    join vatregs on (bkdocs.vatperiod=:period and vatregs.symbol=bkdocs.vatreg and vatregs.vtype=6 and bkdocs.status>0)
    join bkvatpos on (bkvatpos.bkdoc=bkdocs.ref)
    into :k32UE, :k33UE;

    if (k30ue is null) then  /* zeby liczyl odpowiednio */
        k30ue = 0;
    if (k31ue is null) then
        k31ue = 0;
    if (k32ue is null) then
        k32ue = 0;
    if (k33ue is null) then
        k33ue = 0;
    if (k28ue is null) then
        k28ue = 0;
    if (k29ue is null) then
        k29ue = 0;
    if (k23ue is null) then
        k23ue = 0;
    if (k27ue is null) then
        k27ue = 0;
    if (k25ue is null) then
        k25ue = 0;

  K38UE= K23UE + K25UE + K27UE + K31ue + K33ue;

    /*tylko export */
  /*if (k200 is not null) then
    k200 = k200 + k200a;
  else
    k200 = k200a;*/



  /* zakupy z tego miesiaca*/
  for
    select  bkvatpos.taxgr, sum(bkvatpos.netv), sum(bkvatpos.vatv)
    from bkdocs
    join vatregs on (bkdocs.vatperiod=:period and vatregs.symbol=bkdocs.vatreg and vatregs.vtype>2 /*and vatregs.vtype<>4 */and bkdocs.status>0)
    join bkvatpos on (bkvatpos.bkdoc=bkdocs.ref and bkvatpos.debited=1)
    group by bkvatpos.taxgr
    into :taxgr, :netv, :vatv
  do begin
    if (taxgr = 'I11') then
    begin
      k49 = netv;
      k50 = vatv;
    end
    else if (:taxgr = 'I21' or :taxgr = 'INW') then
    begin
      k51 = :netv;
      k52 = :vatv;
    end
    else if (:taxgr = 'P11') then
    begin
      k53 = :netv;
      k54 = :vatv;
    end
    else if (:taxgr = 'P21' or :taxgr = 'POZ' ) then
    begin
      k55 = :netv;
      k56 = :vatv;
    end
    /*else if (:taxgr = 'P57') then
    begin
      k57UE = :vatv;
    end     */
    else
      exception fk_vat7_unknown_taxgr ;
  end

  for /*paliwo znacznik ze nie do odliczenia*/
    select  bkvatpos.taxgr, cast(sum(bkvatpos.netv)*100 as integer)/100, cast(sum(bkvatpos.vatv)*100 as integer)/100
    from bkdocs
    join vatregs on (bkdocs.vatperiod=:period and vatregs.symbol=bkdocs.vatreg and vatregs.vtype>2 /*and vatregs.vtype<>4 */and bkdocs.status>0)
    join bkvatpos on (bkvatpos.bkdoc=bkdocs.ref and bkvatpos.debited=0)
    group by bkvatpos.taxgr
    into :taxgr, :netv, :vatv
  do begin
    if (:taxgr = 'P57') then
    begin
      k57UE = :vatv;
    end
    if (:taxgr = 'P58') then
    begin
      k58UE = :vatv;
    end
  end


  if (k20UE>0) then
  begin
    if (k55 is null) then k55 = 0;
    if (k53 is not null and k53<>0) then
      k55 = k55 + k53;
    if (k56 is null) then k56 = 0;
    if (k54 is not null and k54<>0) then
      k56 = k56 + k54;
    k53 = 0;
    k54 = 0;
  end else
  begin
    if (k53 is null) then k53 = 0;
    if (k55 is not null and k55<>0) then
      k53 = k53 + k55;
    if (k54 is null) then k54 = 0;
    if (k56 is not null and k56<>0) then
      k54 = k54 + k56;
    k55 = 0;
    k56 = 0;
  end

  if (:k20UE<0) then
    k58 = :k48 + :k56;
  else
    if (:k37 <> 0) then
      k58 = (:k48 + :k56)*(:k37 - :k20UE)/:k37;
 /* zsumowanie calego do odliczenia */
 /* k59 = :k39UE + :k42 + :k44 + :k50 + :k52 + :k46 + :k48 + :k54 + :k56;*/

  if (k31ue is null) then  /* zeby liczyl odpowiednio */
    k31ue = 0;
  if (k45ue is null) then
    k45ue = 0;
  if (k46ue is null) then
    k46ue = 0;
  if (k49 is null) then
    k49 = 0;
  if (k51 is null) then
    k51 = 0;

  k41UE=  :k49 + :k51;
  k41UE = cast(k41UE*100 as integer)/100;
  K42UE=  :k42 + :k44 + :k50 + :k52;
  k42UE = cast(k42UE*100 as integer)/100;
  k43UE=  :k53 + :k55;
  k43UE = cast(k43UE*100 as integer)/100;
  k44UE=  :k46 + :k48 + :k54 + :k56;
  k44UE = cast(k44UE*100 as integer)/100;
  K45UE = (:k42 + :k44 + :k50 + :k52)*((WSP45 - 100)/100); /*wyliczenie korekt*/
  K46UE = (:k46 + :k48 + :k54 + :k56)*((WSP45 - 100)/100);

  if (k45ue is null) then
    k45ue = 0;
  if (k46ue is null) then
    k46ue = 0;

  k45UE = cast(k45UE*100 as integer)/100;
  k46UE = cast(k46UE*100 as integer)/100;
  k39UE = cast(k39UE*100 as integer)/100;
  k42 = cast(k42*100 as integer)/100;
  k44 = cast(k44*100 as integer)/100;
  k46 = cast(k46*100 as integer)/100;
  k48 = cast(k48*100 as integer)/100;
  k50 = cast(k50*100 as integer)/100;
  k52 = cast(k52*100 as integer)/100;
  k54 = cast(k54*100 as integer)/100;
  k56 = cast(k56*100 as integer)/100;
  K47UE = K39UE + K42 + K44 + K46 + K48 + K50 + K52 + K54 + K56 + K45UE + K46UE;/* caly odliczony */

  if ((:k38UE)>(:k47UE)) then
    k50UE = k38ue-k47ue;
  else
    k50UE = 0;

  k63 = :k50UE - :k48UE - :k49UE;

  if ((:k47ue)>(:k38ue)) then
    k52UE = :k47ue-k38ue;
  else
    k52UE = 0;

  k66 = :k52UE + :k51UE;

  k55UE = :k66 - :k53UE;

  if (:k20UE = 0) then
    k20UE = null;
  if (:k21UE = 0) then
    k21UE = null;
  if (:k22UE = 0) then
    k22UE = null;
  if (:k23UE = 0) then
    k23UE = null;
  if (:k24UE = 0) then
    k24UE = null;
  if (:k25UE = 0) then
    k25UE = null;
  if (:k26UE = 0) then
    k26UE = null;
  if (:k27UE = 0) then
    k27UE = null;
  if (:k28UE = 0) then
    k28UE = null;
  if (:k29UE = 0) then
    k29UE = null;
  if (:k24UE = 0) then
    k24UE = null;
  if (:k25UE = 0) then
    k25UE = null;
  if (:k26UE = 0) then
    k26UE = null;
  if (:k27UE = 0) then
    k27UE = null;
  if (:k39UE = 0) then
    k39UE = null;
  if (:k40 = 0) then
    k40 = null;
  if (:k42UE = 0) then
    k42UE = null;
  if (:k45UE = 0) then
    k45UE = null;
  if (:k46UE = 0) then
    k46UE = null;
  if (:k46 = 0) then
    k46 = null;
  if (:k48 = 0) then
    k48 = null;
  if (:k50 = 0) then
    k50 = null;
  if (:k52UE = 0) then
    k52UE = null;
  if (:k55UE = 0) then
    k55UE = null;
  if (:k56 = 0) then
    k56 = null;
  if (:k57 = 0) then
    k57 = null;
  if (:k57UE = 0) then
    k57UE = null;
  if (:k58 = 0) then
    k58 = null;
  if (:k48UE = 0) then
    k48UE = null;
  if (:k49UE = 0) then
    k49UE = null;
  if (:k51UE = 0) then
    k51UE = null;
  if (:k53UE = 0) then
    k53UE = null;
  if (:k39UE = 0) then
    k39UE = null;
  if (:k58UE = 0) then
    k58UE = null;

  suspend;
end^
SET TERM ; ^
