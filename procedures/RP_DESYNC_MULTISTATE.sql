--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RP_DESYNC_MULTISTATE(
      TABLENAME varchar(31) CHARACTER SET UTF8                           ,
      KEY1 varchar(65) CHARACTER SET UTF8                           ,
      KEY2 varchar(65) CHARACTER SET UTF8                           ,
      KEY3 varchar(65) CHARACTER SET UTF8                           ,
      KEY4 varchar(65) CHARACTER SET UTF8                           ,
      KEY5 varchar(65) CHARACTER SET UTF8                           ,
      CURSTATE integer)
   as
declare variable locations varchar(255);
declare variable mlocations varchar(255);
declare variable location integer;
declare variable mlocation integer;
declare variable locatchange integer;
declare variable locatchange2 integer;
declare variable tolocat integer;
declare variable tolocat2 integer;
declare variable tryb varchar(255);
declare variable locnum integer;
begin
  locatchange = 0;
  locatchange2 = 0;
  if(:key2 is null) then key2 = '';
  if(:key3 is null) then key3 = '';
  if(:key4 is null) then key4 = '';
  if(:key5 is null) then key5 = '';
  if(:curstate < -2) then -- w STATE jest zakodowany numer lokalizacji dokonującej zmiany - nie nalezy do niej kopiowac
  begin
    locatchange = bin_and(:curstate,65535);
    --wyzerowanie modszych 16 bitów
    curstate = curstate / 65536;
    curstate = curstate * 65536;
  end
  if (curstate > 65535) then
  begin
    --na starszej czesci bitu jest zakoowana druga lokalizacja,
    -- gdzie nie należy replikować usunicia - chrona lokalizacji natywnej dokumentów
    --przesuniecie 16 bitow w prawo
    locatchange2 = curstate / 65536;
    if(:locatchange2 > 32767) then --najstarszy bit jest ustawiony - za duzy numer lokalizacji, zdaza sie, gdy byly ustawione dwie lokaizacje do pominiecia
      locatchange2 = bin_and(:locatchange2,32767);
    --tylko mlodsza czesc state - czyli wartosc rzeczywista, jesli jeszcze jest ustawiona
    curstate = bin_and(curstate, 65535);
  end
  execute procedure GETCONFIG('AKTULOCAT')
    returning_values :locations;
  execute procedure GETCONFIG('AKTULOCATM')
    returning_values :mlocations;
  execute procedure GETCONFIG('AKTULOCATTRYB')
    returning_values tryb;
  if (:locations = '' or :locations is null ) then exit;
  location = locations;
  mlocation = mlocations;
  --okreslenie celu z ktorego nalezy usunac rekord
  if(:curstate > 0) then
    tolocat = :curstate;
  else begin

      --okreslenie lokalizacji docelowych, gdzie ma byc kopiowany rekord
    select max(tolocat), max(tolocat2)
    from RP_SYNCSTATE S
    where s.tabela=:tablename and
          ( s.fromlocat=:location or s.fromlocat=0
            or (s.fromlocat=-1 and :tryb='M')
            or (s.fromlocat=-2 and :tryb='S')
          )
    into :tolocat, :tolocat2;
    if (tolocat2 is null) then
      tolocat2 = 0;
    if (tolocat < 0 and tolocat2 < 0) then
    begin
      if (tolocat = -3) then
        tolocat = tolocat2;
      else  if (tolocat <> tolocat2) then
          tolocat = 0;
    end

    if (tolocat = -3) then
      tolocat = null; --brak celu
  end
  --usuniecie innych polecen LOG_FILE dotyczacych kasowanego rekordu
  delete from LOG_FILE where TABLE_NAME = :tablename and KEY1 = :key1 and KEY2 = :key2 and KEY3 = :key3 and KEY4 = :key4 and key5 = :key5;
 --wylistowanie lokalizacji docelowych i insert do bazy LOG_FILE odpowiednich rekordów
  for select LOCNUM from rp_location_list
  into :locnum
  do begin
    if(
        ( (:tolocat = :locnum)
          or (:tolocat = 0)
          or (:tolocat2 = :locnum)
          or (:tolocat = -1 and :locnum = :mlocation)
          or (:tolocat = -2 and :locnum <> :mlocation)
        )
      and
        (:locnum <> :location) --wyciecie z celu wlasnej lokalacji
      and
        (:locatchange = 0 or :locnum <> :locatchange) -- wyciecie z celu lokacji dokonujacej zapisu
      and
        (:locatchange2 = 0 or :locnum <> :locatchange2) -- wyciecie z celu lokacji dokonujacej zapisu
    ) then
    begin
        insert into log_file (TABLE_NAME, KEY1, KEY2, KEY3, KEY4, key5,  state,  COPY)
          values (:tablename, :key1, :key2, :key3, :key4, :key5, :locnum,  0);
    end
  end

end^
SET TERM ; ^
