--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_DZIENNIK_LINES(
      COMPANY integer,
      ODOKRESU varchar(6) CHARACTER SET UTF8                           ,
      DOOKRESU varchar(6) CHARACTER SET UTF8                           ,
      REJESTR varchar(10) CHARACTER SET UTF8                           )
  returns (
      BKREG varchar(10) CHARACTER SET UTF8                           ,
      NLNUMBER integer,
      NUMBER integer,
      TRANSDATE timestamp,
      DOCDATE timestamp,
      REF integer,
      NAZWA varchar(60) CHARACTER SET UTF8                           ,
      NAME varchar(60) CHARACTER SET UTF8                           ,
      ODDZIAL varchar(10) CHARACTER SET UTF8                           ,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      SUMDEBIT numeric(14,2),
      SUMCREDIT numeric(14,2),
      SUMDEBITS numeric(14,2),
      SUMCREDITS numeric(14,2),
      SUMDEBITC numeric(14,2),
      SUMCREDITC numeric(14,2),
      ZPRZEN_DEBIT numeric(14,2),
      ZPRZEN_CREDIT numeric(14,2),
      NAWASTRONA smallint,
      DESCRIPT varchar(80) CHARACTER SET UTF8                           ,
      DEBIT numeric(14,2),
      CREDIT numeric(14,2),
      DNUMBER smallint,
      ACCOUNT ACCOUNT_ID,
      PODSUMSTR smallint,
      PODSUMDEK smallint,
      STRONA integer,
      NRLINII integer,
      LINIA integer)
   as
declare variable LINI smallint;
declare variable LMAX smallint;
declare variable NLNUMBERTMP integer;
declare variable NUMBERTMP integer;
declare variable DNUMBERTMP smallint;
declare variable SQL varchar(6000);
declare variable PREJESTR varchar(200);
begin
  lmax = 78;
  nawastrona = 0;
  lini = 1;
  strona = 1;
  ref = null;
  podsumdek=0;
  podsumstr = 0;
  sumdebits = 0;
  sumcredits = 0;
  sumdebitc = 0;
  sumcreditc = 0;
  zprzen_debit = 0;
  zprzen_credit = 0;
  nrlinii = 0;
  prejestr = '';
  linia = 1;

  if (coalesce(rejestr,'') <> '') then
    prejestr = '  and b.bkreg = '''||:rejestr||'''';


    sql = 'select b.bkreg, b.nlnumber, b.number, b.transdate, b.docdate, o.nazwa,';
    sql = sql||' b.branch, b.sumdebit, b.sumcredit, b.ref, t.name, b.symbol';
    sql = sql||' from bkdocs b';
    sql = sql||' left join bkdoctypes t on (t.ref = b.doctype)';
    sql = sql||' left join operator o on (o.ref = b.bookoper)';
    sql = sql||' where b.status = 3';
    sql = sql||' and b.company = '||:company;
    sql = sql||' and b.period >= '''||:odokresu||'''';
    sql = sql||'and b.period <='''||:dookresu||''''||:prejestr;
    sql = sql||' order by B.bkreg, B.nlnumber';

  for EXECUTE STATEMENT :sql
       into :bkreg, :nlnumber, :number, :transdate, :docdate, :nazwa,
        :oddzial, :sumdebit, :sumcredit, :ref, :name, :symbol
  do begin
    dnumber = 1;
    nrlinii = :nrlinii + 1;
    if(not exists(select first 1 1
        from decrees d
          join bkaccounts b on (d.accref = b.ref)
        where d.bkdoc = :ref and b.bktype <> 2 and b.bktype <> 3)
    ) then begin
      descript='Brak zapisów';
      debit = 0;
      credit = 0;
      account = '';
      if (lini >= lmax) then
      begin
        podsumstr = 1;
        nlnumberTMP = nlnumber;
        numberTMP = number;
        dnumberTMP = dnumber;
        nlnumber = null;
        number = null;
        dnumber = null;
        suspend;
        linia = linia + 1;

        zprzen_debit = sumdebitc;
        zprzen_credit = sumcreditc;
        sumdebits = 0;
        sumcredits = 0;
        nlnumber = nlnumberTMP;
        number = numberTMP;
        dnumber = dnumberTMP;
        podsumstr = 0;
        lini = 1;
        strona = strona + 1;
      end
      suspend;
      linia = linia + 1;
      lini = lini +1;
      dnumber = dnumber + 1;
    end
    for
      select d.descript, d.debit, d.credit, d.account
        from decrees d
          join bkaccounts b on (d.accref = b.ref)
        where d.bkdoc = :ref and b.bktype <> 2 and b.bktype <> 3
        into :descript, :debit, :credit, :account
    do begin
      if (lini >= lmax) then
      begin
        podsumstr = 1;
        nlnumberTMP = nlnumber;
        numberTMP = number;
        dnumberTMP = dnumber;
        nlnumber = null;
        number = null;
        dnumber = null;
        suspend;
        linia = linia + 1;

        zprzen_debit = sumdebitc;
        zprzen_credit = sumcreditc;
        sumdebits = 0;
        sumcredits = 0;
        nlnumber = nlnumberTMP;
        number = numberTMP;
        dnumber = dnumberTMP;
        podsumstr = 0;
        lini = 1;
        strona = strona + 1;
      end
      suspend;
      linia = linia + 1;

      sumdebits = sumdebits + debit;
      sumcredits = sumcredits + credit;
      sumdebitc = sumdebitc + debit;
      sumcreditc = sumcreditc + credit;
      lini = lini +1;

      dnumber = dnumber + 1;
    end
    podsumdek = 1;
    nlnumber = null;
    number = null;
    dnumber = null;
    suspend;
    linia = linia + 1;

    podsumdek = 0;
    lini = lini +1;
  end
  PODSUMSTR = 1;
  nlnumber = null;
  number = null;
  dnumber = null;
  suspend;
end^
SET TERM ; ^
