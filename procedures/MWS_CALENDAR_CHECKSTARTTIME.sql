--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CALENDAR_CHECKSTARTTIME(
      WH varchar(3) CHARACTER SET UTF8                           ,
      STARTTIMEDCL timestamp)
  returns (
      STARTTIME timestamp)
   as
declare variable ecalendar integer;
declare variable curdate date;
declare variable st time;
declare variable daykind integer;
declare variable daystart timestamp;
declare variable dayend timestamp;
begin
  curdate = cast(STARTTIMEDCL as date);
  select ecalendar from defmagaz where symbol = :wh into :ecalendar;
  if (:ecalendar is null) then
    exception universal 'Brak okreslonego kalendarza na mag.: '||:wh;
  -- szukam pierwszego dnia pracujacego na magazynie, ktory konczy sie pozniej niz ma sie zaczac operacja
  select first 1 cast(CDATE as date)||' '||WORKSTART
    from ecaldays
    where calendar = :ecalendar and cast(cdate as date) >= :curdate
      and DAYKIND = 1 and :STARTTIMEDCL < cast(cast(CDATE as date)||' '||workend as timestamp)
    order by calendar, cdate
    into :STARTTIME;
  if (cast(STARTTIME as date) = cast(STARTTIMEDCL as date)
      and starttime < starttimedcl) then
    starttime = starttimedcl;
end^
SET TERM ; ^
