--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOST_GENERATE_FROM_ANAL(
      NAGZAMREF integer,
      ANALDOST integer,
      TRYB integer,
      VAL integer,
      ILDNI integer,
      ODKTM varchar(40) CHARACTER SET UTF8                           ,
      DOKTM varchar(40) CHARACTER SET UTF8                           )
  returns (
      STATUS integer,
      NUMCHANGE integer)
   as
declare variable ildnistat numeric(14,4);
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable srednia numeric(14,4);
declare variable potrzeba numeric(14,4);
declare variable vall numeric(14,4);
declare variable magazyn char(3);
declare variable cnt integer;
declare variable cenanet numeric(14,4);
declare variable dostawca integer;
declare variable waluta varchar(3);
declare variable dcwaluta varchar(3);
declare variable kurs numeric(14,4);
declare variable pozref integer;
declare variable wersjaref integer;
declare variable oddzial varchar(20);
declare variable prec smallint;
begin
  STATUS = -1;
  NUMCHANGE = 0;
  /* ustalenie magazynu */
  /*zgodny z nagówkiem dostay*/
  select magazyn, DOSTAWCA, KURS, WALUTA, ODDZIAL
    from NAGZAM
    where REF=:NAGZAMREF
    into  :magazyn, :dostawca, :kurs, :waluta, :oddzial;
  if(:kurs is null) then kurs = 1;
  if(:analdost is null) then exception DOST_GEN_ANAL_NOT_DEFINED;
  if(:magazyn is null) then  exception DOST_GEN_MAGAZYN_NOT_DEFINED;
  select STATYSTYKA from ANALDOST where REF=:analdost into :ildnistat;
  if(:ildnistat is null) then ildnistat = 0;
  if(:ildnistat = 0) then ildnistat = 1;
  else if (:ildnistat = 1) then ildnistat = 7;
  else if (:ildnistat = 2) then ildnistat = 30;
  else ildnistat = 365;
  if(:magazyn is null) then begin
    select MAGAZYN from ANALDOST where REF=:ANALDOST into :magazyn;
  end
  for select KTM,WERSJA, SREDNIAW, POTRZEBA from ANALDOSTP where ANALIZA = :analdost AND (KTM >=:ODKTM or (:ODKTM='')) AND (KTM <= :DOKTM or (:DOKTM=''))
     into :ktm, :wersja, :srednia, :potrzeba
  do begin
    if(:val = 0) then
      vall = :potrzeba;
    else
      vall = :srednia * :ildni / :ildnistat;
    cnt = null;
    select count(*) from POZZAM where ZAMOWIENIE = :NAGZAMREF AND KTM = :KTM AND WERSJA = :WERSJA into :cnt;
    if(:cnt is null) then cnt = 0;
    if(:cnt > 1) then exception DOST_GEN_ANAL_MULTIPLE_KTMS;
    if(:cnt > 0) then begin
      pozref = 0;
      select max(REF) from POZZAM where ZAMOWIENIE = :NAGZAMREF AND KTM = :KTM AND WERSJA = :WERSJA AND ILOSC<>:vall into :pozref;
      if(:pozref > 0) then begin
        update POZZAM set ILOSC=:vall where REF=:pozref;
        NUMCHANGE = :numchange + 1;
      end
    end else if(:tryb = 0) then begin
      cenanet = 0;
      dcwaluta = :waluta;
      if(:dostawca is not null) then begin
        select REF from WERSJE where KTM=:ktm and NRWERSJI=:wersja into :wersjaref;
        select CENANET, WALUTA, PREC from GET_DOSTCEN(:wersjaref,:dostawca,:waluta,:oddzial,1) into :cenanet, :dcwaluta, :prec;
      end
      if(:cenanet is null) then cenanet = 0;
      if(:vall is null) then vall = 0;
      insert into POZZAM(ZAMOWIENIE, MAGAZYN, KTM, WERSJA,ILOSC,CENANET,WALCEN,OPK,PREC)
       values(:NAGZAMREF, :MAGAZYN, :KTM, :WERSJA, :vall,:cenanet, :dcwaluta,0,:prec);
       NUMCHANGE = :numchange + 1;
    end

  end
  status = 1;
end^
SET TERM ; ^
