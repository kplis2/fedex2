--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDEDET_CHANGE(
      MODE smallint,
      STABLE varchar(31) CHARACTER SET UTF8                           ,
      SREF integer,
      PRGDPRSCHEDGUIDEPOS integer)
   as
declare variable PRGDPRNAGZAM integer;
declare variable PRGDPRPOZZAM integer;
declare variable PRGDPRSCHEDGUIDE integer;
declare variable PRGDPRSCHEDOPER integer;
declare variable PRGDPRSHMAT integer;
declare variable PRGDWERSJAREF integer;
declare variable NEWREF integer;
declare variable PRGDSSOURCE smallint;
declare variable PRGDKTM varchar(40);
declare variable PRGDWH varchar(3);
declare variable PRGDTYPDOK varchar(3);
declare variable PRGDILOSC numeric(14,4);
declare variable PRGDOUT smallint;
declare variable PRGDOVERLIMIT smallint;
declare variable PRGDAUTODOC smallint;
declare variable PRGDGRUPAGEN integer;
declare variable PRGDVERSTO integer;
begin
  PRGDPRSCHEDOPER = null;
  PRGDOUT = 1;
  PRGDTYPDOK = '';
  PRGDOVERLIMIT = 0;
  PRGDAUTODOC = 0;
  if (stable = 'POZZAM' and sref is not null) then
  begin
    if (mode <> 1) then
      select p.prschedguidepos, p.magazyn, p.wersjaref, p.ktm, p.prshmat, case when n.prpreprod = 0 then p.ilosc else p.ilosc * p.kilosc end
        from pozzam p
          left join nagzam n on (n.ref = p.zamowienie)
        where p.ref = :sref
        into PRGDPRSCHEDGUIDEPOS, PRGDWH, PRGDWERSJAREF, PRGDKTM, PRGDPRSHMAT, PRGDILOSC;
    PRGDSSOURCE = 2;
    if (PRGDPRSCHEDGUIDEPOS is not null and mode in (1,2)) then
    begin
      update prschedguidedets d set d.accept = 0
        where d.prschedguidepos = :PRGDPRSCHEDGUIDEPOS and d.ssource = :prgdssource and d.sref = :sref;
      delete from prschedguidedets d
        where d.prschedguidepos = :PRGDPRSCHEDGUIDEPOS and d.ssource = :prgdssource and d.sref = :sref;
    end
    if (PRGDPRSCHEDGUIDEPOS is not null and mode in (0,2)) then
    begin
      select p.prschedguide, g.zamowienie, p.pozzamref
        from prschedguidespos p
          left join prschedguides g on (g.ref = p.prschedguide)
        where p.ref = :PRGDPRSCHEDGUIDEPOS
        into PRGDPRSCHEDGUIDE, PRGDPRNAGZAM, PRGDPRPOZZAM;
      execute procedure PRSCHEDGUIDEDETS_ADD(PRGDPRNAGZAM,PRGDPRPOZZAM,PRGDPRSCHEDGUIDE,
          PRGDPRSCHEDGUIDEPOS,PRGDPRSCHEDOPER,PRGDPRSHMAT,PRGDKTM,PRGDWERSJAREF,PRGDSSOURCE,
          PRGDWH,PRGDTYPDOK,PRGDILOSC,PRGDOUT,PRGDOVERLIMIT,PRGDAUTODOC,null,0,stable,sref,null)
        returning_values newref;
      update prschedguidedets d set d.accept = 2
        where d.prschedguidepos = :PRGDPRSCHEDGUIDEPOS and d.ssource = :prgdssource and d.sref = :sref;
    end
  end
  if (stable = 'PRSCHEDGUIDEDETS' and sref is not null) then
  begin
    PRGDSSOURCE = 1;
    if (mode <> 1) then
    begin
      select p.wersjaref, p.quantity
        from prschedguidedets p
        where p.ref = :sref
        into PRGDWERSJAREF, PRGDILOSC;
      select d.overlimit, d.prgengroup, d.wersjaref
        from prschedguidedets d
        where d.sref = :sref
        into PRGDOVERLIMIT, PRGDGRUPAGEN, PRGDVERSTO;
    end
    if (PRGDWERSJAREF <> PRGDVERSTO) then
      exception prschedguidedets_error 'Zmiana towaru niemożliwa przy przekazaniu z przewodnika na przewodnik.';
    if (mode in (0)) then
    begin
      select p.prschedguide, g.zamowienie, p.pozzamref
        from prschedguidespos p
          left join prschedguides g on (g.ref = p.prschedguide)
        where p.ref = :PRGDPRSCHEDGUIDEPOS
        into PRGDPRSCHEDGUIDE, PRGDPRNAGZAM, PRGDPRPOZZAM;
      execute procedure PRSCHEDGUIDEDETS_ADD(PRGDPRNAGZAM,PRGDPRPOZZAM,PRGDPRSCHEDGUIDE,
          PRGDPRSCHEDGUIDEPOS,PRGDPRSCHEDOPER,PRGDPRSHMAT,PRGDKTM,PRGDWERSJAREF,PRGDSSOURCE,
          PRGDWH,PRGDTYPDOK,PRGDILOSC,PRGDOUT,PRGDOVERLIMIT,PRGDAUTODOC,null,0,stable,sref,PRGDGRUPAGEN)
        returning_values newref;
      update prschedguidedets d set d.accept = 1
        where d.prschedguidepos = :PRGDPRSCHEDGUIDEPOS and d.ssource = :prgdssource and d.sref = :sref;
    end
    else if (mode in (1)) then
    begin
      update prschedguidedets d set d.accept = 0
        where d.ssource = :prgdssource and d.sref = :sref;
      delete from prschedguidedets d
        where d.ssource = :prgdssource and d.sref = :sref;
    end
    else if (mode = 2) then
    begin
      update prschedguidedets d set d.quantity = :PRGDILOSC
        where d.ssource = :prgdssource and d.sref = :sref;
    end
  end
end^
SET TERM ; ^
