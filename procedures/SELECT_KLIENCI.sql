--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SELECT_KLIENCI(
      GRUPA integer,
      NIEKUPIL integer,
      KUPILWIECEJ numeric(14,2),
      KUPILMNIEJ numeric(14,2),
      REGRAB integer,
      DATAOD date,
      DATADO date,
      OGRANICZ numeric(14,2),
      ILUB smallint,
      KTM varchar(80) CHARACTER SET UTF8                           ,
      KTMKWOTA numeric(14,2))
  returns (
      KREF integer,
      FSKROT SHORTNAME_ID,
      GRUPADICT varchar(20) CHARACTER SET UTF8                           ,
      NUMER varchar(30) CHARACTER SET UTF8                           ,
      EMAIL varchar(100) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE NIEKUPILBOOL SMALLINT;
DECLARE VARIABLE KUPILWIECEJBOOL SMALLINT;
DECLARE VARIABLE KUPILMNIEJBOOL SMALLINT;
DECLARE VARIABLE REGRABBOOL SMALLINT;
DECLARE VARIABLE OGRANICZBOOL SMALLINT;
DECLARE VARIABLE KTMBOOL SMALLINT;
DECLARE VARIABLE KUPILKWOTA NUMERIC(14,2);
DECLARE VARIABLE AKTUGRUPA INTEGER;
DECLARE VARIABLE ILFAK INTEGER;
DECLARE VARIABLE KUPILKWOTA1 NUMERIC(14,2);
DECLARE VARIABLE KUPILKWOTA2 NUMERIC(14,2);
DECLARE VARIABLE KUPILKWOTA3 NUMERIC(14,2);
DECLARE VARIABLE KTMKUPILKWOTA NUMERIC(14,2);
DECLARE VARIABLE OKRES1 VARCHAR(6);
DECLARE VARIABLE OKRES2 VARCHAR(6);
DECLARE VARIABLE OKRES3 VARCHAR(6);
DECLARE VARIABLE SPELNIA SMALLINT;
DECLARE VARIABLE NIEUSTAWIONO SMALLINT;
begin
  if (:grupa is null) then grupa = 0;
  if (:niekupil is null) then niekupil = 0;
  if (:kupilwiecej is null) then kupilwiecej = 0;
  if (:kupilmniej is null) then kupilmniej = 0;
  if (:ogranicz is null) then ogranicz = 0;
  if (:ilub is null) then ilub = 0;
  if (:ktmkwota is null) then ktmkwota = 0;
  for
    select klienci.ref, KLIENCI.fskrot, klienci.grupa, grupykli.opis, cphones.fullnumber, klienci.email
      from klienci
      join grupykli on (klienci.grupa = grupykli.ref)
      join cphones on (cphones.skey = klienci.ref and cphones.sfield = 'KOMORKA'
          and cphones.stable = 'KLIENCI' and cphones.sposition = 1)
      where ((klienci.grupa = :grupa and :grupa > 0) or :grupa = 0)
    into :kref, :fskrot, :aktugrupa, :grupadict, :numer, :email
  do begin
    -- "zerowanie" zmiennych
    niekupilbool = 0;
    kupilwiecejbool = 0;
    kupilmniejbool = 0;
    regrabbool = 0;
    ograniczbool = 0;
    ktmbool = 0;

    -- kupil wiecej / mniej niz podana kwota
    if (:kupilwiecej > 0 or :kupilmniej > 0) then begin
      select sum(NAGFAK.SUMWARTNET- NAGFAK.PSUMWARTNET) as SPRZNET
        from NAGFAK
        where (NAGFAK.ZAKUP = 0) and (NAGFAK.AKCEPTACJA = 1 or NAGFAK.AKCEPTACJA = 8)
        and (NAGFAK.ANULOWANIE = 0) and (NAGFAK.NIEOBROT < 2) and nagfak.klient = :kref
        and ((:dataod is not null and nagfak.data >= :dataod) or :dataod is null)
        and ((:datado is not null and nagfak.data <= :datado) or :datado is null)
      into :kupilkwota;
    end
    if (:kupilwiecej > 0) then begin
      if ( :kupilwiecej <= :kupilkwota ) then
        kupilwiecejbool = 1;
      else if (:kupilwiecej > :kupilkwota) then
        kupilwiecejbool = 0;
    end else kupilwiecejbool = 2;
    if (:kupilmniej > 0) then begin
      if ( :kupilmniej >= :kupilkwota ) then
        kupilmniejbool = 1;
      else if (:kupilmniej < :kupilkwota) then
        kupilmniejbool = 0;
    end else kupilmniejbool = 2;

    -- przypisany do podanej reguly rabatowej
    if (:regrab is null) then regrabbool = 2;
    else begin
      if ( exists(select ref from promokli where promokli.promocja = :regrab
           and (promokli.klient = :kref or promokli.grupakli = :aktugrupa))
      ) then
        regrabbool = 1;
      else
        regrabbool = 0;
    end

    -- nie kupil w podanycm okresie
    if(:niekupil > 0) then begin
      select count(*)
        from NAGFAK
        where (NAGFAK.ZAKUP = 0) and (NAGFAK.AKCEPTACJA = 1 or NAGFAK.AKCEPTACJA = 8)
        and (NAGFAK.ANULOWANIE = 0) and (NAGFAK.NIEOBROT < 2) and nagfak.klient = :kref
        and ((:dataod is not null and nagfak.data >= :dataod) or :dataod is null)
        and ((:datado is not null and nagfak.data <= :datado) or :datado is null)
      into :ilfak;
      if (:ilfak > 0) then niekupilbool = 0;
      else if (:ilfak = 0) then niekupilbool = 1;
    end else niekupilbool = 2;

    -- czy ograniczyl zakupy w ostatnich 3 okresach
    if (:ogranicz > 0) then begin
      select datatookres.okres from datatookres(current_date,0,-1,0,0)
      into :okres1;
      select sum(NAGFAK.SUMWARTNET- NAGFAK.PSUMWARTNET) as SPRZNET
        from NAGFAK
        where (NAGFAK.ZAKUP = 0) and (NAGFAK.AKCEPTACJA = 1 or NAGFAK.AKCEPTACJA = 8)
        and (NAGFAK.ANULOWANIE = 0) and (NAGFAK.NIEOBROT < 2) and nagfak.klient = :kref
        and nagfak.okres = :okres1
      into :kupilkwota1;
      select datatookres.okres from datatookres(current_date,0,-2,0,0)
      into :okres2;
      select sum(NAGFAK.SUMWARTNET- NAGFAK.PSUMWARTNET) as SPRZNET
        from NAGFAK
        where (NAGFAK.ZAKUP = 0) and (NAGFAK.AKCEPTACJA = 1 or NAGFAK.AKCEPTACJA = 8)
        and (NAGFAK.ANULOWANIE = 0) and (NAGFAK.NIEOBROT < 2) and nagfak.klient = :kref
        and nagfak.okres = :okres2
      into :kupilkwota2;
      select datatookres.okres from datatookres(current_date,0,-3,0,0)
      into :okres3;
      select sum(NAGFAK.SUMWARTNET- NAGFAK.PSUMWARTNET) as SPRZNET
        from NAGFAK
        where (NAGFAK.ZAKUP = 0) and (NAGFAK.AKCEPTACJA = 1 or NAGFAK.AKCEPTACJA = 8)
        and (NAGFAK.ANULOWANIE = 0) and (NAGFAK.NIEOBROT < 2) and nagfak.klient = :kref
        and nagfak.okres = :okres3
      into :kupilkwota3;
      if (:kupilkwota3 > 0) then begin
        if ((:kupilkwota2/:kupilkwota3) < (1 - :ogranicz/100)) then begin
          ograniczbool = 1;
        end else if ((:kupilkwota1/:kupilkwota3) < (1 - :ogranicz/100)) then begin
          ograniczbool = 1;
        end
      end
      if (:kupilkwota2 > 0 and (:kupilkwota1/:kupilkwota2) < (1 - :ogranicz/100)) then begin
        ograniczbool = 1;
      end
      else if ( ((:kupilkwota3 > 0 and :kupilkwota2 = 0) or (:kupilkwota3 > 0 and :kupilkwota2 = 0)
                  or (:kupilkwota2 > 0 and :kupilkwota1 = 0))  and :ograniczbool = 0
      ) then begin
        ograniczbool = 1;
      end
    end else if (:ogranicz = 0) then
      ograniczbool = 2;

    -- czy kupil zadany towar
    if (:ktm is null) then ktmbool = 2;
    else begin
      select sum(pozfak.wartnet - pozfak.pwartnet)
        from NAGFAK
        join POZFAK on (pozfak.dokument = nagfak.ref)
        where (NAGFAK.ZAKUP = 0) and (NAGFAK.AKCEPTACJA = 1 or NAGFAK.AKCEPTACJA = 8)
        and (NAGFAK.ANULOWANIE = 0) and (NAGFAK.NIEOBROT < 2) and nagfak.klient = :kref
        and ((:dataod is not null and nagfak.data >= :dataod) or :dataod is null)
        and ((:datado is not null and nagfak.data <= :datado) or :datado is null)
        and pozfak.ktm like '%'||:ktm||'%'
      into :ktmkupilkwota;
      if (:ktmkupilkwota >= :ktmkwota) then ktmbool = 0;
      else if (:ktmkupilkwota < :ktmkwota) then ktmbool = 1;
    end

    -- kryteria wyswietlania
    spelnia = 0;
    nieustawiono =0;
    if (:niekupilbool = 1) then spelnia = :spelnia + 1;
    else if (:niekupilbool = 2) then nieustawiono = :nieustawiono + 1;
    if (:kupilwiecejbool = 1) then spelnia = :spelnia + 1;
    else if (:kupilwiecejbool = 2) then nieustawiono = :nieustawiono + 1;
    if (:kupilmniejbool = 1) then spelnia = :spelnia + 1;
    else if (:kupilmniejbool = 2) then nieustawiono = :nieustawiono + 1;
    if (:regrabbool = 1) then spelnia = :spelnia + 1;
    else if (:regrabbool = 2) then nieustawiono = :nieustawiono + 1;
    if (:ograniczbool = 1) then spelnia = :spelnia + 1;
    else if (:ograniczbool = 2) then nieustawiono = :nieustawiono + 1;
    if (:ktmbool = 1) then spelnia = :spelnia + 1;
    else if (:ktmbool = 2) then nieustawiono = :nieustawiono + 1;

    -- wyswietlenie
    if (:ilub = 1 and :spelnia + :nieustawiono = 6) then
      suspend;
    else if (:ilub = 0 and :spelnia > 0) then
      suspend;
  end
end^
SET TERM ; ^
