--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRTOOLTYPESTODS_ADD(
      PRDSDEF varchar(20) CHARACTER SET UTF8                           ,
      PRTOOLSTYPES varchar(4096) CHARACTER SET UTF8                           ,
      PRTOOLSTYPESGROUP varchar(20) CHARACTER SET UTF8                           )
   as
declare variable prtoolstype varchar(20);
begin
  if (coalesce(PRTOOLSTYPES,'') <> '') then
  begin
    for
      select STRINGOUT
        from parsestring(:prtoolstypes,';')
        into :prtoolstype
    do begin
      if (not exists (select first 1 1 from prtoolstypestods p
        where p.prdsdef = :prdsdef and p.prtoolstypes = :prtoolstypes)
      ) then
        insert into prtoolstypestods (prdsdef, prtoolstypes)
          values (:prdsdef, :prtoolstype);
    end
  end else begin
    insert into prtoolstypestods (prdsdef, prtoolstypes)
      select :prdsdef, t.symbol
        from prtoolstypes t
        where t.prtoolstypegroup = :prtoolstypesgroup
          and not exists(select first 1 1 from prtoolstypestods tt
            where tt.prdsdef = :prdsdef and tt.prtoolstypes = t.symbol);
  end
end^
SET TERM ; ^
