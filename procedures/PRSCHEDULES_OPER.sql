--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDULES_OPER(
      PRSCHEDULE integer,
      PRSCHSTAT integer,
      REJZAMTO varchar(3) CHARACTER SET UTF8                           ,
      REJZAMFROM varchar(3) CHARACTER SET UTF8                           )
  returns (
      STATUS integer,
      TRESC varchar(255) CHARACTER SET UTF8                           )
   as
declare variable zam integer;
declare variable rejzamwaiting varchar(20);
declare variable rejzamprod varchar(20);
declare variable rejzamarch varchar(20);
declare variable oldstatus integer;
begin
  status = 0;
  tresc = '';
  select prschedules.status, prdeparts.prrejzamwaiting, prdeparts.prrejzamprod, prdeparts.prrejzamarch
    from prdeparts left join prschedules on (prdepart = prdeparts.symbol)
    where prschedules.ref = :prschedule
    into :oldstatus,:rejzamwaiting, :rejzamprod,  :rejzamarch;
  if(:rejzamwaiting is null or (:rejzamwaiting = '')) then
    execute procedure get_config('PRREJZAMWAITING',0) returning_values :rejzamwaiting;
  if(:rejzamprod is null or (:rejzamprod = '')) then
    execute procedure get_config('PRREJZAMPROD',0) returning_values :rejzamprod;
  if(:rejzamarch is null or (:rejzamarch = '')) then
    execute procedure get_config('PRREJZAMARCH',0) returning_values :rejzamarch;
  if(:rejzamto = 'XXX') then begin
    if(:status = 0 or (status  = 1) ) then
      rejzamto = :rejzamwaiting;
    else if(:status = 2) then
      rejzamto = :rejzamprod;
    else if(:status = 3) then
      rejzamto = :rejzamarch;
  end
  if(:rejzamfrom = 'XXX') then begin
    if(:oldstatus = 0 or (:oldstatus  = 1) ) then
      rejzamfrom = :rejzamwaiting;
    else if(:oldstatus = 2) then
      rejzamfrom = :rejzamprod;
    else if(:oldstatus = 3) then
      rejzamfrom = :rejzamarch;
  end
  update prschedules s set s.status = :prschstat where s.ref = :prschedule;
  if(:rejzamto <> '')then begin
    for select prschedzam.zamowienie from prschedzam where prschedzam.prschedule = :prschedule
    into :zam
    do begin
      update nagzam n set n.rejestr = :rejzamto where ref=:zam and (:rejzamfrom = '' or n.rejestr = :rejzamfrom);
    end
  end
  status = 1;
  tresc = 'Operacja zakończona powodzeniem.';
  suspend;
end^
SET TERM ; ^
