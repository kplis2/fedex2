--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_MONTH_NAME(
      MT smallint)
  returns (
      NAME varchar(15) CHARACTER SET UTF8                           )
   as
begin
  if (mt = 1) then
    name = 'styczeń';
  else if (mt = 2) then
    name = 'luty';
  else if (mt = 3) then
    name = 'marzec';
  else if (mt = 4) then
    name = 'kwiecień';
  else if (mt = 5) then
    name = 'maj';
  else if (mt = 6) then
    name = 'czerwiec';
  else if (mt = 7) then
    name = 'lipiec';
  else if (mt = 8) then
    name = 'sierpień';
  else if (mt = 9) then
    name = 'wrzesień';
  else if (mt = 10) then
    name = 'październik';
  else if (mt = 11) then
    name = 'listopad';
  else if (mt = 12) then
    name = 'grudzień';
  suspend;
end^
SET TERM ; ^
