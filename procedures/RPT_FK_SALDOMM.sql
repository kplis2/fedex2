--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_SALDOMM(
      ACCOUNT ACCOUNT_ID,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      FROMDATE timestamp,
      TODATE timestamp,
      COMPANY integer)
  returns (
      LP integer,
      BKREF integer,
      BKREG varchar(10) CHARACTER SET UTF8                           ,
      BKSYMBOL varchar(40) CHARACTER SET UTF8                           ,
      BKDATE timestamp,
      DEBIT numeric(14,2),
      CREDIT numeric(14,2),
      SALDO numeric(14,2),
      DESCRIPT varchar(255) CHARACTER SET UTF8                           )
   as
declare variable otable varchar(20);
declare variable oref integer;
declare variable dokmagdate timestamp;
declare variable dokmagdate2 timestamp;
declare variable cnt integer;
declare variable mag varchar(3);
declare variable mag2 varchar(3);
declare variable ref2 integer;
declare variable symbol2 varchar(40);
declare variable bkref2 integer;
declare variable bkdate2 timestamp;
declare variable buf numeric(14,2);
begin
  saldo = 0;
  lp = 1;
  for select distinct bkdocs.ref,bkdocs.bkreg,bkdocs.symbol,bkdocs.transdate,bkdocs.otable,bkdocs.oref
    from decrees
    left join bkdocs on (decrees.bkdoc=bkdocs.ref)
    where decrees.transdate>=:fromdate and decrees.transdate<=:todate
    and decrees.status>1
    and decrees.account like :account||'%'
    and decrees.company = :company
    order by bkdocs.transdate,bkdocs.ref
    into :bkref,:bkreg,:bksymbol,:bkdate,:otable,:oref
    do begin
      /* przejdz przez wszystkie bkdocs z tym kontem*/
      descript = '';
      credit = 0;
      debit = 0;
      select sum(credit-debit)
        from decrees
        where bkdoc=:bkref and status>1 and account like :account||'%'
          and company = :company
        into :credit;
      if(:credit is null) then credit = 0;
      if(:otable is null) then otable = '';
      if(:otable<>'DOKUMNAG' and :otable<>'DOKUMNOT' and :credit<>0) then begin
        /* BKDOCS wystawiony recznie */
        debit = 0;
        descript = 'Dokument wystawiony recznie';
        saldo = :saldo + :debit - :credit;
        if(:debit<0 or :credit<0) then begin buf=:credit; credit=-:debit; debit=-:buf; end
        suspend;
        lp = :lp + 1;
      end else if(:otable='DOKUMNAG') then begin
        /* znajdz DOKUMNAG na wskazanym magazynie*/
        debit = 0;
        select dokumnag.symbol,dokumnag.data,dokumnag.magazyn,
          dokumnag.ref2
        from dokumnag
        where dokumnag.ref=:oref
          and dokumnag.company = :company
        into :descript,:dokmagdate,:mag,:ref2;
        if(:mag=:magazyn) then begin
          symbol2 = null;
          dokmagdate2 = null;
          bkref2 = null;
          bkdate2 = null;
          mag2 = null;
          if(:ref2 is not null) then begin
            select dokumnag.symbol,dokumnag.data,dokumnag.magazyn
            from dokumnag
            where dokumnag.ref=:ref2
              and dokumnag.company = :company
            into :symbol2,:dokmagdate2,:mag2;
            select ref,transdate from bkdocs where otable='DOKUMNAG' and oref=:ref2
              and company = :company
            into :bkref2,:bkdate2;
          end
          if(:ref2 is null or :symbol2 is null) then begin
            debit = 0;
            descript = :descript || ' - brak dokumentu przeciwnego';
            saldo = :saldo + :debit - :credit;
            if(:debit<0 or :credit<0) then begin buf=:credit; credit=-:debit; debit=-:buf; end
            suspend;
            lp = :lp + 1;
          end else begin
            select sum(debit-credit)
            from decrees
            where bkdoc=:bkref2 and status>1 and account like :account||'%'
              and company = :company
            into :debit;
            if(:debit is null) then debit = 0;
            if(:bkdate2>:todate) then begin
              debit = 0;
              descript = :descript || ' - dokument '||:symbol2||' zadekretowany '||:bkdate2;
              saldo = :saldo + :debit - :credit;
              if(:debit<0 or :credit<0) then begin buf=:credit; credit=-:debit; debit=-:buf; end
              suspend;
              lp = :lp + 1;
            end else if(:credit<>:debit) then begin
              descript = :descript || ' - dokument '||:symbol2||' ma inn wartos';
              saldo = :saldo + :debit - :credit;
              if(:debit<0 or :credit<0) then begin buf=:credit; credit=-:debit; debit=-:buf; end
              suspend;
              lp = :lp + 1;
            end
          end
        end
      end else if(:otable='DOKUMNOT') then begin
        /* nota nie ma dokumentu przeciwnego */
        debit = 0;
        descript = 'Nota magazynowa';
        saldo = :saldo + :debit - :credit;
        if(:debit<0 or :credit<0) then begin buf=:credit; credit=-:debit; debit=-:buf; end
        suspend;
        lp = :lp + 1;
      end
    end
end^
SET TERM ; ^
