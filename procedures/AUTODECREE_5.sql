--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_5(
      BKDOC integer)
   as
declare variable account ACCOUNT_ID;
declare variable amount numeric(14,2);
declare variable vate numeric(14,2);
declare variable tmp varchar(255);
declare variable settlement varchar(20);
declare variable docdate timestamp;
declare variable payday timestamp;
declare variable descript varchar(255);
declare variable dictpos integer;
declare variable dictdef integer;
declare variable oref integer;
declare variable debited smallint;
declare variable crnote smallint;
declare variable fsopertype smallint;
declare variable branch_symbol varchar(20);
declare variable period varchar(6);
declare variable vatperiod varchar(6);
declare variable vperiod varchar(1);
declare variable vatgr varchar(10);
declare variable usluga smallint;
declare variable accountvatperiod ACCOUNT_ID;
declare variable otable varchar(31);
declare variable bksymbol KONTO_ID;
declare variable taxgr varchar(20);
declare variable bankaccount integer;
declare variable sumprocentplat numeric(14,2);
begin
  -- DU: standardowy schemat dekretowania - dokumenty zakupu towarów

  select B.sumgrossv, B.symbol, B.docdate, B.payday, B.descript, B.dictdef,
      B.dictpos, B.oref,  T.creditnote, O.symbol, B.period, B.vatperiod,B.otable, B.bankaccount
    from bkdocs B
      left join bkdoctypes T on (B.doctype = T.ref)
      left join nagfak N on (B.oref = N.ref)
      left join oddzialy O on (B.branch = O.oddzial)
    where B.ref = :bkdoc
    into :amount, :settlement, :docdate, :payday, :descript, :dictdef, :dictpos,
       :oref, :crnote, :branch_symbol, :period, :vatperiod,:otable, :bankaccount;



  execute procedure KODKS_FROM_DICTPOS(dictdef, dictpos)
    returning_values tmp;

  account = '201-' || tmp;



  if (crnote = 1) then
    fsopertype = 4;
  else
    fsopertype = 3;

  execute procedure fk_autodecree_termplat(bkdoc,account, amount,0,0,'', 1 , settlement,
                                          oref, otable,fsopertype,docdate,
                                          payday,descript,0,bankaccount)
    returning_values :sumprocentplat;

  for
    select netv, vate, debited, vatgr, taxgr
      from bkvatpos
      where bkdoc=:bkdoc
      into :amount, :vate, :debited, :vatgr, :taxgr
   do begin
     if (coalesce(vatperiod,'') = period) then
       accountvatperiod = '221-0-'||:taxgr||'-'||:vatgr;
     else
       accountvatperiod = '221-1-'||:taxgr||'-'||:vatgr;

     execute procedure insert_decree(bkdoc, accountvatperiod, 0, vate, descript, 1);
   end

   for select sum(R.sumwartnetzl - R.psumwartnetzl)
    from rozfak R
      where R.dokument = :oref
      into :amount
   do begin

       account = '300-01';


        execute procedure insert_decree(bkdoc, account, 0, amount, descript, 1);
  end

end^
SET TERM ; ^
