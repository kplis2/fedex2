--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STRING_MATCH_DELIM_COUNT(
      TOPARSE STRING,
      DELIM varchar(1) CHARACTER SET UTF8                           ,
      DELIMALLOWCOUNT smallint)
  returns (
      PARSEDSTRING STRING)
   as
declare variable delimcount smallint = 0;
declare variable deletedelimprob numeric(10,9);
declare variable delimpos smallint;
declare variable delimstodel smallint = 0;
begin

  /* TS: Usuwa badź uzupełnia nadmiarowe znaki
  *  w TOPARSE (będące separatorami DELIM) do DELIMALLOWCOUNT.
  *  Jeśli separatorów w TOPARSE jest więcej niż DELIMALLOWCOUNT,
  *  nadmiarowe są usuwane na podstawie prawdopodobieństwa - tym większe,
  *  im więcej należy usunąć znaków. Jeśli nie zostały usunięte wszystkie,
  *  reszta separatorów jest usuwana od początku TOPARSE.
  */

  parsedstring = :toparse;
  delimpos = position(:delim, :parsedstring);
  while (:delimpos > 0) do
  begin
    delimcount = :delimcount + 1;
    delimpos = position(:delim, :parsedstring, :delimpos + 1);
  end
  
  -- Usuwaj na podstawie prawdopodobienstwa
  delimstodel = :delimcount - :delimallowcount;
  if (:delimstodel > 0) then
  begin
    deletedelimprob = 1 - cast(:delimallowcount as numeric(10,9)) / :delimcount;
    delimpos = 0;
    while (:delimcount > 0) do
    begin
      delimpos = position(:delim, :parsedstring, :delimpos + 1);
      if (:delimstodel > 0
        and (:delimpos + 1 = position(:delim, :parsedstring, :delimpos + 1)  -- jesli separatory wystepuja po sobie
          or rand() < :deletedelimprob)) then                                --lub zgodnie z rozkladem
      begin
        parsedstring = substring(:parsedstring from 1 for :delimpos - 1)
          ||substring(:parsedstring from :delimpos + 1);
        delimstodel = :delimstodel - 1;
      end
      delimcount = :delimcount - 1;
    end
  
    -- Jesli nie usunieto wystarczajaco duzo, usuwaj od poczatku.
    delimpos = 0;
    while (:delimstodel > 0) do
    begin
      delimpos = position(:delim, :parsedstring, :delimpos + 1);
      parsedstring = substring(:parsedstring from 1 for :delimpos - 1)
        ||substring(:parsedstring from :delimpos + 1);
      delimstodel = :delimstodel - 1;
    end
  end else begin
     while (:delimcount < 0) do begin
       parsedstring = :parsedstring||:delim;
       delimstodel = :delimstodel + 1;
     end
  end
  suspend;
end^
SET TERM ; ^
