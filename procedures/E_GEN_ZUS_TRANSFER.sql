--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GEN_ZUS_TRANSFER(
      IPER char(6) CHARACTER SET UTF8                           ,
      TDATE date,
      BANKACC varchar(10) CHARACTER SET UTF8                           ,
      CURRENTCOMPANY integer)
  returns (
      COUNTER integer)
   as
declare variable dictdef integer;
declare variable dictpos integer;
declare variable toacc varchar(60);
declare variable amount numeric(14,2);
declare variable descript varchar(255);
declare variable descript1 varchar(10);
declare variable przelewtype varchar(10);
begin
  counter = 0;
  select ref
    from slodef
    where nazwa = 'Rozrachunki z ZUS'
    into :dictdef;  

--należności z tytułu składek na ubezpieczenia społeczne (83101010230000261395100000)

  descript = 'należności z tytułu składek na ubezpieczenia społeczne za okres ' ||
    substring(iper from 5 for 2) || '/' || substring(iper from 1 for 4);
  descript1 = 'S'||iper||'01';--dwie ostatnie cyfry to numer deklaracji

  execute procedure getconfig('PRZELEWZUS') returning_values :przelewtype;

  select ref
    from slopoz
    where kod = 'ZUS' and slownik = :dictdef
    into :dictpos;

  select account
    from bankaccounts
    where dictdef = :dictdef and dictpos = :dictpos and gl=1
    into :toacc;

  select sum(P.pvalue)
    from epayrolls EP
      join eprpos P on (EP.ref = P.payroll)
    where EP.iper = :iper and P.ecolumn in (6100, 6110, 6120, 8200, 8210, 8220)
      and EP.company = :currentcompany
    into :amount;

  if (tdate < current_timestamp(0)) then
    tdate = current_date;
  if (toacc is not null and amount > 0) then
  begin
    insert into BTRANSFERS (BANKACC, typ, BTYPE, slodef, SLOPOZ, data,
      towho, toacc, todescript, todescript1, curr, amount, autobtransfer, company)
    values (:bankacc, 1, :przelewtype, :dictdef, :dictpos, :tdate,
      'ZUS', :toacc, :descript, :descript1, 'PLN', :amount, 1, :currentcompany);
    counter = counter + 1;
  end
--należności z tytułu składek na ubezpieczenia zdrowotne (78101010230000261395200000)
  amount = 0;

  descript = 'należności z tytułu składek na ubezpieczenia zdrowotne za okres ' ||
    substring(iper from 5 for 2) || '/' || substring(iper from 1 for 4);

  dictpos = null;
  toacc = null;
  select ref
    from slopoz
    where kod = 'NFZ' and slownik = :dictdef
    into :dictpos;

  select account
    from bankaccounts
    where dictdef = :dictdef and dictpos = :dictpos and gl=1
    into :toacc;

  select sum(P.pvalue)
    from epayrolls EP
      join eprpos P on (EP.ref = P.payroll)
    where EP.iper = :iper and P.ecolumn = 7220 and EP.company = :currentcompany
    into :amount;

  if (toacc is not null and amount > 0) then
  begin
    insert into BTRANSFERS (BANKACC, typ, BTYPE, slodef, SLOPOZ, data,
      towho, toacc, todescript, todescript1, curr, amount, autobtransfer, company)
    values (:bankacc, 1, :przelewtype, :dictdef, :dictpos, :tdate,
      'ZUS', :toacc, :descript, :descript1, 'PLN', :amount, 1, :currentcompany);
    counter = counter + 1;
  end

--należności z tytułu składek na FP i FGŚP (73101010230000261395300000)
  amount = 0;

  descript = 'należności z tytułu składek na FP i FGŚP ' ||
    substring(iper from 5 for 2) || '/' || substring(iper from 1 for 4);

  dictpos = null;
  toacc = null;
  select ref
    from slopoz
    where kod = 'FP' and slownik = :dictdef
    into :dictpos;

  select account
    from bankaccounts
    where dictdef = :dictdef and dictpos = :dictpos and gl=1
    into :toacc;

  select sum(P.pvalue)
    from epayrolls EP
      join eprpos P on (EP.ref = P.payroll)
    where EP.iper = :iper and P.ecolumn in (8810, 8820)
    into :amount;

  if (toacc is not null and amount > 0) then
  begin
    insert into BTRANSFERS (BANKACC, typ, BTYPE, slodef, SLOPOZ, data,
      towho, toacc, todescript, todescript1, curr, amount, autobtransfer, company)
    values (:bankacc, 1, :przelewtype, :dictdef, :dictpos, :tdate,
      'ZUS', :toacc, :descript, :descript1, 'PLN', :amount, 1, :currentcompany);
    counter = counter + 1;
  end
  suspend;
end^
SET TERM ; ^
