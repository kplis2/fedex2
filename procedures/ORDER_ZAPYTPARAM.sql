--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ORDER_ZAPYTPARAM(
      ZAPYTANIE integer,
      OLDN integer,
      NEWN integer)
   as
declare variable parametr VARCHAR(20);
  declare variable number integer;
begin
  --firts we find minor number
  if (oldn < newn) then
    newn = oldn;

  number = newn;
  for
    select parametr
      from ZAPYTPARAM
      where zapytanie = :zapytanie and numer >= :number
      order by numer, ord
      into :parametr
  do begin
    update ZAPYTPARAM set numer = :number, ord = 2
      where zapytanie = :zapytanie and parametr = :parametr;
    number = number + 1;
  end

  update ZAPYTPARAM set ord = 1
    where zapytanie = :zapytanie and numer >= :newn;
end^
SET TERM ; ^
