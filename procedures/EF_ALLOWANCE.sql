--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_ALLOWANCE(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      ALLOWANCE numeric(14,2))
   as
declare variable fromdate timestamp;
declare variable todate timestamp;
declare variable infoallowance smallint;
declare variable income numeric(14,2);
declare variable pallowance numeric(14,2);
declare variable firsttaxlevel numeric(14,2);
declare variable company companies_id;
declare variable taxyear years_id;
declare variable tper period_id;
declare variable person persons_id;
declare variable payday timestamp;
declare variable extdate timestamp;
declare variable extamount numeric(14,2);
declare variable ablockdate date;
begin
--DU: personel - liczy koszty uzyskania pracownika

  allowance = 0;

  select tper, payday from epayrolls
    where ref = :payroll
    into :tper, :payday;

  execute procedure period2dates(:tper)
    returning_values :fromdate, :todate;

  taxyear = extract(year from todate);

  if (payday < todate) then todate = payday;

  select first 1 allowance, alloblockdate, extdate, extamount
    from empltaxinfo
    where employee = :employee and fromdate <= :todate
    order by fromdate desc
    into :infoallowance, :ablockdate,:extdate, :extamount;

  if (infoallowance > 0) then
  begin
    if (taxyear >= 2017) then  --BS99051
    begin
      if (payday > ablockdate
        and extract(year from ablockdate) = extract(year from payday)
        and extract(month from ablockdate) <> extract(month from payday)
      ) then
        infoallowance = 0;
      else begin

        select company, person from employees
          where ref = :employee
          into :company, :person;

        select sum(case when p.ecolumn = 7000 then p.pvalue else -p.pvalue end)
          from epayrolls r
            join eprpos p on (p.payroll = r.ref and 0+p.ecolumn in (7000,7010))
            join employees e on (e.ref = p.employee and e.company = r.company)
          where e.person = :person and e.company = :company and r.empltype in (1,3)
            and r.tper <= :tper and r.tper starting with cast(:taxyear as varchar(4))
            and (r.payday < :payday or (r.payday = :payday and r.ref <= :payroll))
          into :income;

        if (extract(year from extdate) = taxyear and extdate <= todate) then
          income = coalesce(income,0) + coalesce(extamount,0);

        if (income > 0) then
        begin
          select first 1 amount from etaxlevels
            where taxyear = :taxyear and amount > 0
            order by taxscale
            into :firsttaxlevel;
      
          if (infoallowance = 2) then
            income = income / 2.00;
          if (income > firsttaxlevel) then
            infoallowance = 0;
        end
      end
    end

    if (infoallowance in (1,2)) then
    begin
      execute procedure ef_pval(:employee, :payroll, :prefix, 'ULGAPODATKOWA')
        returning_values pallowance;

      allowance = pallowance * infoallowance;
    end
  end else
    if (infoallowance is null) then
      exception no_tax_info;

  suspend;
end^
SET TERM ; ^
