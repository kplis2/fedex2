--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE A_COMPUTE_ROZRACH as
declare variable slodef integer;
  declare variable slopoz integer;
  declare variable kontofk konto_id;
  declare variable symbfak varchar(20);
  declare variable company integer;
begin
  for
    select R.slodef, R.slopoz, R.kontofk, R.symbfak, R.company
      from rozrach R
        join rozrachp P on (R.slodef = P.slodef and R.slopoz = P.slopoz
          and R.kontofk = P.kontofk and R.symbfak = P.symbfak
          and R.company = P.company)
      group by R.slodef, R.slopoz, R.kontofk, R.symbfak, R.company
      having sum(P.winien) <> max(R.winien) or sum(P.ma) <> max(R.ma)
      into :slodef, :slopoz, :kontofk, :symbfak, :company
  do
    execute procedure ROZRACH_OBL_NAG(kontofk, slodef, slopoz, symbfak, :company);
  --suspend; --nie ma parametrów do zwrocenia SB
end^
SET TERM ; ^
