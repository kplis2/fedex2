--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_CALCULATE_CHECKWORKTIME(
      PRDEPART varchar(20) CHARACTER SET UTF8                           ,
      CHECKTIME timestamp,
      STARTTIME timestamp)
  returns (
      RETTIME timestamp,
      TIMELOST float)
   as
declare variable CALENDAR integer;
declare variable Y integer;
declare variable M integer;
declare variable D integer;
declare variable MOMENT time;
declare variable REFDAY integer;
declare variable REFNEXTDAY integer;
declare variable KIND integer;
declare variable DSTART time;
declare variable DEND time;
declare variable DAYSTART timestamp;
declare variable DAYNEXTSTART timestamp;
BEGIN
  rettime = :checktime;
  timelost = 0;
  SELECT calendar FROM prdeparts WHERE symbol = :prdepart INTO :calendar;
  IF(:calendar IS NULL) THEN EXIT;
  SELECT EXTRACT( DAY FROM :checktime) FROM RDB$database INTO :d;
  SELECT EXTRACT( MONTH FROM :checktime) FROM RDB$database INTO :m;
  SELECT EXTRACT( YEAR FROM :checktime) FROM RDB$database INTO :y;
  moment = CAST(:checktime AS TIME);
  SELECT REF, DAYKIND, WORKSTART, WORKEND, CDATE
    FROM ecaldays
    WHERE calendar = :calendar AND  cyear = :y AND cmonth = :m AND cday = :d
    INTO :refday, :kind, :dstart, :dend, :daystart;
  IF(:refday IS NULL)
    THEN EXCEPTION UNIVERSAL 'Brak kalendarza dla '||:checktime;
  --jesli jest w czasie pracy, to koniec
  if(:moment >= :dstart and :moment <= :dend) then
    exit;--jest w czasie pracy, wiec exit

  if(:starttime is not null and :starttime < :checktime) then begin
    --zliczanie czasu przerw od momentu starttime do checktime
  end
  --poszukiwanie dni do przeodu
  for select e.ref, edk.daytype, cdate, workstart, workend
  from ecaldays e
  join edaykinds edk on edk.ref = e.daykind
  where calendar = :calendar and cdate  >= :daystart
  into :refnextday, :kind, :daynextstart, :dstart, :dend
  do begin

    if(:kind = 1) then begin
      if((:refday = : refnextday and :moment < :dstart) or
         (:refday <> :refnextday))
      then begin
        rettime = cast(:daynextstart as date) ||' '|| cast(:dstart as time);
        exit;
      end
    end
  end
END^
SET TERM ; ^
