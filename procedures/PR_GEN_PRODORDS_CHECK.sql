--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_GEN_PRODORDS_CHECK(
      PRORD integer,
      MODE smallint,
      PRSHEET integer,
      AMOUNT numeric(14,4),
      REJESTR varchar(10) CHARACTER SET UTF8                           ,
      NAGZAM integer,
      POZZAM integer,
      PRNAGZAM integer,
      PRPOZZAM integer,
      STOPCASCADEIN smallint,
      GRUPAZAM integer = 0,
      ACCUMULATIONMODE smallint = 0)
  returns (
      STATUS smallint)
   as
begin
  status = 1;
  if (mode = 3) then
  begin
    if (exists(select first 1 1 from relations where srefto = :prpozzam
        and stableto = 'POZZAM' and relationdef = 2)
    ) then
      exception prnagzam_error 'Istnieją pozycje surowcowe do wyrobów gotowych';
  end
end^
SET TERM ; ^
