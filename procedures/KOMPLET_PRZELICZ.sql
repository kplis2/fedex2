--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KOMPLET_PRZELICZ(
      KOMPLET integer,
      TYPMAG integer,
      CENNIK integer)
   as
begin
  update KPLNAG set TYPMAG = :typmag, CENNIK = :cennik, KPLNAG.datalast = current_date where REF=:komplet;
  update KPLPOZ set CENAMAG = null, CENASPR = null where NAGKPL = :komplet;
end^
SET TERM ; ^
