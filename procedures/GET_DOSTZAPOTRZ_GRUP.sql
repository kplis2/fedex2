--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_DOSTZAPOTRZ_GRUP(
      DOSTAWCA integer,
      MAGAZYNY varchar(255) CHARACTER SET UTF8                           ,
      MASKAKTM varchar(40) CHARACTER SET UTF8                           ,
      DATAOD date,
      DATADO date,
      DNIZAPASU integer,
      ZEWN smallint,
      AGREGACJA smallint,
      TYPMIARY smallint,
      ZAMOWIENIE integer)
  returns (
      REF integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      KTMPREFIX varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      WERSJAREF integer,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      WNAZWA varchar(255) CHARACTER SET UTF8                           ,
      RMIARA varchar(20) CHARACTER SET UTF8                           ,
      GRUPA varchar(255) CHARACTER SET UTF8                           ,
      TYPTOWARU varchar(255) CHARACTER SET UTF8                           ,
      RDOSTAWCA varchar(255) CHARACTER SET UTF8                           ,
      SZUZYCIE numeric(14,4),
      SZUZYCIED numeric(14,4),
      STAN numeric(14,4),
      ZABLOKOW numeric(14,4),
      ZAREZERW numeric(14,4),
      ZAMOWIONO numeric(14,4),
      STANMIN numeric(14,4),
      STANMAX numeric(14,4),
      PROGNOZASPRZ numeric(14,4),
      ZAPOTRZ numeric(14,4),
      ZAMOWIC numeric(14,4),
      ILOSCNAZAM numeric(14,4),
      NAGZAMREF integer,
      POZZAMREF integer,
      STATUS smallint)
   as
begin
  pozzamref = NULL;
  nagzamref = NULL;
  iloscnazam = NULL;
  if(:magazyny<>'') then magazyny = ';'||:magazyny||';';
  if(:maskaktm is null or :maskaktm='') then maskaktm = '%';
  if(:zamowienie is null) then zamowienie = 0;
  if(:dostawca is null) then dostawca = 0;
  if(:agregacja is null) then agregacja=0;
  if(:datado is null) then datado = current_date;
  if(:dataod is null) then dataod = '2008-01-01';
  if(:datado<:dataod) then datado = :dataod;
  status = 0;
  -- pobierz dane bez grupowania
  for select KTM, WERSJA, WERSJAREF, NAZWA, WNAZWA, RMIARA, GRUPA, TYPTOWARU, RDOSTAWCA,
         SZUZYCIE, SZUZYCIED, STAN, ZABLOKOW, ZAREZERW, ZAMOWIONO, STANMIN, STANMAX,
         PROGNOZASPRZ, ZAPOTRZ, ZAMOWIC,
         ILOSCNAZAM, NAGZAMREF, POZZAMREF, STATUS
  from GET_DOSTZAPOTRZ(:DOSTAWCA, :MAGAZYNY, :MASKAKTM, :DATAOD, :DATADO, :DNIZAPASU, :ZEWN, :TYPMIARY, :ZAMOWIENIE)
  into :KTM, :WERSJA, :WERSJAREF, :NAZWA, :WNAZWA, :RMIARA, :GRUPA, :TYPTOWARU, :RDOSTAWCA,
         :SZUZYCIE, :SZUZYCIED, :STAN, :ZABLOKOW, :ZAREZERW, :ZAMOWIONO, :STANMIN, :STANMAX,
         :PROGNOZASPRZ, :ZAPOTRZ, :ZAMOWIC,
         :ILOSCNAZAM, :NAGZAMREF, :POZZAMREF, :STATUS
  do begin
    if(:agregacja>0) then begin
      ktmprefix = substring(:ktm from 1 for :agregacja);
    end else begin
      ktmprefix = NULL;
    end
    ref = :wersjaref;
    suspend;
  end
  -- pobierz dane zgrupowane
  ktmprefix = NULL;
  ref = -1;
  if(:agregacja>0) then begin
    for select substring(KTM from 1 for :agregacja), NULL, NULL, min(NAZWA), NULL, min(RMIARA), min(GRUPA), min(TYPTOWARU), min(RDOSTAWCA),
         sum(SZUZYCIE), sum(SZUZYCIED), sum(STAN), sum(ZABLOKOW), sum(ZAREZERW), sum(ZAMOWIONO), sum(STANMIN), sum(STANMAX),
         sum(PROGNOZASPRZ), sum(ZAPOTRZ), sum(ZAMOWIC),
         NULL, NULL, NULL, max(STATUS)
    from GET_DOSTZAPOTRZ(:DOSTAWCA, :MAGAZYNY, :MASKAKTM, :DATAOD, :DATADO, :DNIZAPASU, :ZEWN, :TYPMIARY, NULL)
    group by 1
    into :KTM, :WERSJA, :WERSJAREF, :NAZWA, :WNAZWA, :RMIARA, :GRUPA, :TYPTOWARU, :RDOSTAWCA,
         :SZUZYCIE, :SZUZYCIED, :STAN, :ZABLOKOW, :ZAREZERW, :ZAMOWIONO, :STANMIN, :STANMAX,
         :PROGNOZASPRZ, :ZAPOTRZ, :ZAMOWIC,
         :ILOSCNAZAM, :NAGZAMREF, :POZZAMREF, :STATUS
    do begin
      status = :status + 2;
      ref = :ref - 1;
      suspend;
    end
  end

end^
SET TERM ; ^
