--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_DOCLABEL_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer,
      REF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable shippingdoc integer;
declare variable shippingdocno string30;
declare variable grandparent integer;
declare variable deliverytype integer;
declare variable accesscode string80;
declare variable filepath string255;
declare variable format string10;
begin
  select l.ref, l.symbolsped, l.sposdost
    from listywysd l
    where l.ref = :oref
  into :shippingdoc, :shippingdocno, :deliverytype;

  --sprawdzanie czy istieje dokument spedycyjny
  if (shippingdoc is null) then exception ede_fedex 'Brak listy spedycyjnej.';

  val = null;
  name = 'wydrukujList';
  parent = null;
  params = null;
  id = 0;
  suspend;

  grandparent = id;

  --dane autoryzacyjne
  select k.klucz, k.sciezkapliku
    from spedytwys w
      left join spedytkonta k on (w.spedytor = k.spedytor)
    where w.sposdost = :deliverytype
  into :accesscode, :filepath;

  val = :accesscode;
  name = 'kodDostepu';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = :filepath;
  name = 'sciezkaPliku';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = 'PRODUCTION'; --jesli chcesz testowac zmien na TEST
  name = 'serviceUrlType';
  id = id + 1;
  parent = GrandParent;
  suspend;

  select symboldok from ededocsh where ref = :ref
  into :format;
  if (coalesce(:format,'') = '') then format = 'PDF';
  val = :format;
  name = 'format';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = :shippingdocno;
  name = 'numerPrzesylki';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;
end^
SET TERM ; ^
