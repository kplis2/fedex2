--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OBLICZ_NAGZAM(
      ZAMOWIENIE integer)
   as
declare variable SUMA_NETTO decimal(14,2);
declare variable SUMA_BRUTTO decimal(14,2);
declare variable SUMA_RNETTO decimal(14,2);
declare variable SUMA_RBRUTTO decimal(14,2);
declare variable SUMA_NETTOZL decimal(14,2);
declare variable SUMA_BRUTTOZL decimal(14,2);
declare variable SUMA_RNETTOZL decimal(14,2);
declare variable SUMA_RBRUTTOZL decimal(14,2);
declare variable VAT_NETTO decimal(14,2);
declare variable VAT_BRUTTO decimal(14,2);
declare variable VAT_RNETTO decimal(14,2);
declare variable VAT_RBRUTTO decimal(14,2);
declare variable VAT_NETTOZL decimal(14,2);
declare variable VAT_BRUTTOZL decimal(14,2);
declare variable VAT_RNETTOZL decimal(14,2);
declare variable VAT_RBRUTTOZL decimal(14,2);
declare variable SUMA_WARTMAG decimal(14,2);
declare variable SUMA_WARTRMAG decimal(14,2);
declare variable SUMA_KWARTMAG decimal(14,2);
declare variable SUMA_KBRUTTO decimal(14,2);
declare variable SUMA_KNETTO decimal(14,2);
declare variable KOSZTDOST decimal(14,2);
declare variable RABAT decimal(14,2);
declare variable DOZAP decimal(14,2);
declare variable BN varchar(100);
declare variable VAT_ST numeric(14,4);
declare variable WAGA numeric(14,3);
declare variable OBJETOSC numeric(15,4);
declare variable WALUTOWE smallint;
begin
  suma_netto = 0;
  suma_brutto = 0;
  suma_rnetto = 0;
  suma_rbrutto = 0;
  suma_nettozl = 0;
  suma_bruttozl = 0;
  suma_rnettozl  = 0;
  suma_rbruttozl = 0;
  select BN from NAGZAM where REF = :zamowienie into :bn;
  if (bn is null) then
    execute procedure GETCONFIG('WGCEN') returning_values :bn;
  select sum(WARTMAG), sUM(WARTRMAG),
         sum(KWARTMAG), sum(KWARTNET), sum(KWARTBRU), sum(WAGA), sum(OBJETOSC)
          from POZZAM
     where  zamowienie = :zamowienie
       and coalesce(fake,0) = 0
     into  :suma_wartmag, :suma_wartrmag,
           :suma_kwartmag, :suma_knetto, :suma_kbrutto, :waga, :objetosc;
   
  for select sum(POZZAM.wartnet), sum(POZZAM.WARTBRU), sum(POZZAM.WARTRNET), sum(POZZAM.WARTRBRU),
           sum(POZZAM.wartnetzl), sum(POZZAM.WARTBRUZL), sum(POZZAM.WARTRNETZL), sum(POZZAM.wartrbruzl),
           VAT.stawka
           from POZZAM
           join VAT on (POZZAM.GR_VAT = VAT.grupa)
     where  pozzam.zamowienie = :zamowienie
       and coalesce(fake,0) = 0
  group by VAT.stawka
  into :vat_netto, :vat_brutto, :vat_rnetto, :vat_rbrutto,
           :vat_nettozl, :vat_bruttozl, :vat_rnettozl, :vat_rbruttozl,
           :vat_st
  do begin
    if(:bn = 'N') then begin
      suma_netto = :suma_netto + :vat_netto;
      suma_rnetto = :suma_rnetto + :vat_rnetto;
      suma_nettozl = :suma_nettozl + :vat_nettozl;
      suma_rnettozl = :suma_rnettozl + :vat_rnettozl;

      suma_brutto = :suma_brutto + cast((:vat_netto *  (1+:vat_st/100)) as numeric(14,2));
      suma_rbrutto = :suma_rbrutto + cast((:vat_rnetto *  (1+:vat_st/100)) as numeric(14,2));
      suma_bruttozl = :suma_bruttozl + cast((:vat_nettozl *  (1+:vat_st/100)) as numeric(14,2));
      suma_rbruttozl = :suma_rbruttozl + cast((:vat_rnettozl *  (1+:vat_st/100)) as numeric(14,2));
    end
    else begin
      suma_brutto = :suma_brutto + :vat_brutto;
      suma_rbrutto = :suma_rbrutto + :vat_rbrutto;
      suma_bruttozl = :suma_bruttozl + :vat_bruttozl;
      suma_rbruttozl = :suma_rbruttozl + :vat_rbruttozl;

      suma_netto = :suma_netto + cast((:vat_brutto / (1+:vat_st/100)) as numeric(14,2));
      suma_rnetto = :suma_rnetto + cast((:vat_rbrutto / (1+:vat_st/100)) as numeric(14,2));
      suma_nettozl = :suma_nettozl + cast((:vat_bruttozl / (1+:vat_st/100)) as numeric(14,2));
      suma_rnettozl = :suma_rnettozl + cast((:vat_rbruttozl / (1+:vat_st/100)) as numeric(14,2));

    end
  end

  update NAGZAM set SUMWARTNET=:suma_netto, SUMWARTBRU=:suma_brutto,
      SUMWARTRNET =:suma_rnetto, SUMWARTRBRU=:suma_rbrutto,
      SUMWARTNETZL =:suma_nettozl, SUMWARTBRUZL=:suma_bruttozl,
      SUMWARTRNETZL =:suma_rnettozl, SUMWARTRBRUZL=:suma_rbruttozl,
      SUMWARTMAG = :suma_wartmag, SUMWARTRMAG = :suma_wartrmag,
      KSUMWARTMAG = :suma_kwartmag, KSUMWARTBRU = :suma_kbrutto,
      KSUMWARTNET = :suma_knetto, WAGA = :waga, OBJETOSC = :objetosc
      where REF=:zamowienie;

end^
SET TERM ; ^
