--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_E_PAYROLL_SUMMARY_UCP(
      BILLFROMDATE date,
      BILLTODATE date,
      PAYDAY date,
      STATUS smallint,
      COMPANY integer)
  returns (
      NUM1 integer,
      NAME1 varchar(22) CHARACTER SET UTF8                           ,
      VAL1 numeric(14,2),
      REPSUM1 integer,
      NUM2 integer,
      NAME2 varchar(22) CHARACTER SET UTF8                           ,
      VAL2 numeric(14,2),
      REPSUM2 integer,
      NUM3 integer,
      NAME3 varchar(22) CHARACTER SET UTF8                           ,
      VAL3 numeric(14,2),
      REPSUM3 integer,
      NUM4 integer,
      NAME4 varchar(22) CHARACTER SET UTF8                           ,
      VAL4 numeric(14,2),
      REPSUM4 integer,
      NUM5 integer,
      NAME5 varchar(22) CHARACTER SET UTF8                           ,
      VAL5 numeric(14,2),
      REPSUM5 integer)
   as
declare variable col1 smallint = 0;
declare variable col2 smallint = 0;
declare variable col3 smallint = 0;
declare variable col4 smallint = 0;
declare variable col5 smallint = 0;
declare variable maxcol smallint = 0;
begin
--MWr: Personel - Generowanie wydruku list plac dla rachunkow UCP

  status = coalesce(status,0);
  company = coalesce(company,1);

  select count(distinct case when p.ecolumn < 1000 then p.ecolumn end),
         count(distinct case when p.ecolumn >= 1000 and p.ecolumn < 4000 then p.ecolumn end),
         count(distinct case when p.ecolumn > 4000 and p.ecolumn < 5900 then p.ecolumn end),
         count(distinct case when p.ecolumn > 5900 and p.ecolumn < 8000 then p.ecolumn end),
         count(distinct case when p.ecolumn > 8000 and p.ecolumn < 9050 then p.ecolumn end)
    from epayrolls r
      join eprpos p on (r.ref = p.payroll and r.empltype = 2 and r.company = :company)
      join ecolumns c on (c.number = p.ecolumn and c.repsum > 0)
    where r.billdate >= :billfromdate and r.billdate <= :billtodate
      and (r.payday = :payday or :payday is null) and status >= :status
    into :col1, :col2, :col3, :col4, :col5;

  maxcol = col1;
  if (col2 > maxcol) then maxcol = col2;
  if (col3 > maxcol) then maxcol = col3;
  if (col4 > maxcol) then maxcol = col4;
  if (col5 > maxcol) then maxcol = col5;

  while (maxcol > 0) do
  begin
    if (col1 >= maxcol) then
      select first 1 skip (:col1 - :maxcol) c.number, c.name, sum(p.pvalue), c.repsum
        from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype = 2 and r.company = :company)
          join ecolumns c on (c.number = p.ecolumn and c.repsum > 0)
        where r.billdate >= :billfromdate and r.billdate <= :billtodate
          and (r.payday = :payday or :payday is null) and status >= :status
          and p.ecolumn < 1000 and repsum > 0
        group by c.number, c.name, c.repsum
        into :num1, :name1, val1, :repsum1;

    if (col2 >= maxcol) then
      select first 1 skip (:col2 - :maxcol) c.number, c.name, sum(p.pvalue), c.repsum
        from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype = 2 and r.company = :company)
          join ecolumns c on (c.number = p.ecolumn and c.repsum > 0)
        where r.billdate >= :billfromdate and r.billdate <= :billtodate
          and (r.payday = :payday or :payday is null) and status >= :status
          and p.ecolumn >= 1000 and p.ecolumn < 4000
        group by c.number, c.name, c.repsum
        into :num2, :name2, val2, :repsum2;

    if (col3 >= maxcol) then
      select first 1 skip (:col3 - :maxcol) c.number, c.name, sum(p.pvalue), c.repsum
        from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype = 2 and r.company = :company)
          join ecolumns c on (c.number = p.ecolumn and c.repsum > 0)
        where r.billdate >= :billfromdate and r.billdate <= :billtodate
          and (r.payday = :payday or :payday is null) and status >= :status
          and p.ecolumn > 4000 and p.ecolumn < 5900
        group by c.number, c.name, c.repsum
        into :num3, :name3, val3, :repsum3;

    if (col4 >= maxcol) then
      select first 1 skip (:col4 - :maxcol) c.number, c.name, sum(p.pvalue), c.repsum
        from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype = 2 and r.company = :company)
          join ecolumns c on (c.number = p.ecolumn and c.repsum > 0)
        where r.billdate >= :billfromdate and r.billdate <= :billtodate
          and (r.payday = :payday or :payday is null) and status >= :status
          and ecolumn > 5900 and ecolumn < 8000
        group by c.number, c.name, c.repsum
        into :num4, :name4, :val4, :repsum4;

    if (col5 >= maxcol) then
      select first 1 skip (:col5 - :maxcol) c.number, c.name, sum(p.pvalue), c.repsum
        from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype = 2 and r.company = :company)
          join ecolumns c on (c.number = p.ecolumn and c.repsum > 0)
        where r.billdate >= :billfromdate and r.billdate <= :billtodate
          and (r.payday = :payday or :payday is null) and status >= :status
          and ecolumn > 8000 and ecolumn < 9050
        group by c.number, c.name, c.repsum
        into :num5, :name5, :val5, :repsum5;

    suspend;
    maxcol = maxcol - 1;
  end
end^
SET TERM ; ^
