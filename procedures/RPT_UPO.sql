--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_UPO(
      EDECLREF integer)
  returns (
      PODMIOT varchar(80) CHARACTER SET UTF8                           ,
      IDDOK varchar(80) CHARACTER SET UTF8                           ,
      CZAS varchar(80) CHARACTER SET UTF8                           ,
      SKROTZL varchar(80) CHARACTER SET UTF8                           ,
      SKROTSYS varchar(80) CHARACTER SET UTF8                           ,
      STRUKTURA varchar(255) CHARACTER SET UTF8                           ,
      NIP varchar(13) CHARACTER SET UTF8                           ,
      ID2 varchar(13) CHARACTER SET UTF8                           ,
      US varchar(255) CHARACTER SET UTF8                           ,
      STEMPELCZAS varchar(80) CHARACTER SET UTF8                           )
   as
declare variable upoxml varchar(8000);
declare variable uscode varchar(4);
declare variable eyear integer;
declare variable person integer;
begin
/*MWr Personel: Procedura zwraca dane do wydruku Urzedowego Potwierdzenia
                Odbioru w module e-deklacji */

  if (edeclref > 0) then
  begin
    select upoxml, refid, eyear, person
      from edeclarations
      where ref = :edeclref
      into :upoxml, :iddok, :eyear, :person;
  
    execute procedure get_xmlitemval('NazwaPodmiotuPrzyjmujacego', upoxml)
      returning_values podmiot;

    podmiot = replace(podmiot,  '&3xF3;',  'ó');
  
    execute procedure get_xmlitemval('DataWplyniecia', upoxml)
      returning_values czas;
  
    execute procedure get_xmlitemval('SkrotZlozonejStruktury', upoxml)
      returning_values skrotzl;
  
    execute procedure get_xmlitemval('SkrotDokumentu', upoxml)
      returning_values skrotsys;
  
    if (skrotsys <> '') then
      skrotsys = substring(skrotsys from position('[' in skrotsys)-1 for coalesce(char_length(skrotsys),0)); -- [DG] XXX ZG119346
  
    execute procedure get_xmlitemval('NazwaStrukturyLogicznej', upoxml)
      returning_values struktura;
  
    execute procedure get_xmlitemval('NIP1', upoxml)
      returning_values nip;
  
    if (coalesce(nip,'') = '') then
      execute procedure get_xmlitemval('PESEL1', upoxml)
        returning_values nip;
  
    execute procedure get_xmlitemval('PESEL2', upoxml)
      returning_values id2;
  
    if (coalesce(id2,'') = '') then
      execute procedure get_xmlitemval('NIP2', upoxml)
        returning_values id2;
  
    execute procedure get_xmlitemval('StempelCzasu', upoxml)
      returning_values stempelczas;
  
    execute procedure get_xmlitemval('KodUrzedu', upoxml)
      returning_values uscode;
  
    select coalesce(name,'') || ', ' || coalesce(address,'') || ', ' || coalesce(postcode,'') || ' ' || coalesce(city,'')
      from einternalrevs
      where code = :uscode
      into :us;
  
    suspend;
  
    when any do
    begin
      select taxoffice from e_get_perstaxinfo4pit(:eyear,:person,2)
       where uscode = :uscode
       into :us;
      suspend;
    end
  end
end^
SET TERM ; ^
