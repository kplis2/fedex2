--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_CELL(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      ROW integer,
      COL integer)
  returns (
      AMOUNT numeric(15,2))
   as
declare variable PSNREF integer;
declare variable FRVHDR integer;
declare variable FRPSN integer;
declare variable FRCOL integer;
declare variable CACHEPARAMS varchar(255);
declare variable DDPARAMS varchar(255);
declare variable FRVERSION integer;
begin
  if (frvpsn = 0) then exit;

  select frvhdr, frpsn, frcol
    from frvpsns
    where ref = :frvpsn
    into :frvhdr, :frpsn, :frcol;

  select frversion
    from frvhdrs
    where ref = :frvhdr
    into :frversion;
  cacheparams = period||';'||row||';'||col;
  ddparams = row||';'||col;

  if (not exists (select ref from frvdrilldown where functionname = 'CELL' and cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr)) then
  begin

    select FVP.ref
      from frvpsns FVP
      join frpsns FP on (FVP.frpsn = FP.ref)
      where FP.number = :row and FVP.frvhdr = :frvhdr
    into :psnref;

    select FP.amount
      from frvpsns FP
      where FP.frvhdr = :frvhdr and FP.ref = :psnref and FP.col = :col
      into :amount;
  end else
    select first 1 fvalue from frvdrilldown where cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr
      into :amount;

  amount = coalesce(amount,0);
  insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
     values (:frvhdr, :frpsn, :frcol, 'CELL', :cacheparams, :ddparams, :amount, :frversion);
  suspend;
end^
SET TERM ; ^
