--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_DOS_DOSTAWCY_INS(
      DOSTAWCAID type of column INT_IMP_DOS_DOSTAWCY.DOSTAWCAID,
      DOSTAWA type of column INT_IMP_DOS_DOSTAWCY.DOSTAWA,
      DOSTAWAID type of column INT_IMP_DOS_DOSTAWCY.DOSTAWAID,
      ID type of column INT_IMP_DOS_DOSTAWCY.ID,
      NAZWA type of column INT_IMP_DOS_DOSTAWCY.NAZWA,
      NIP type of column INT_IMP_DOS_DOSTAWCY.NIP,
      REGON type of column INT_IMP_DOS_DOSTAWCY.REGON,
      KRAJ type of column INT_IMP_DOS_DOSTAWCY.KRAJ,
      KRAJID type of column INT_IMP_DOS_DOSTAWCY.KRAJID,
      ULICA type of column INT_IMP_DOS_DOSTAWCY.ULICA,
      NRDOMU type of column INT_IMP_DOS_DOSTAWCY.NRDOMU,
      NRLOKALU type of column INT_IMP_DOS_DOSTAWCY.NRLOKALU,
      KODPOCZTOWY type of column INT_IMP_DOS_DOSTAWCY.KODPOCZTOWY,
      MIASTO type of column INT_IMP_DOS_DOSTAWCY.MIASTO,
      TELEFON type of column INT_IMP_DOS_DOSTAWCY.TELEFON,
      KOMORKA type of column INT_IMP_DOS_DOSTAWCY.KOMORKA,
      FAX type of column INT_IMP_DOS_DOSTAWCY.FAX,
      EMAIL type of column INT_IMP_DOS_DOSTAWCY.EMAIL,
      WWW type of column INT_IMP_DOS_DOSTAWCY.WWW,
      UWAGI type of column INT_IMP_DOS_DOSTAWCY.UWAGI,
      AKTYWNY type of column INT_IMP_DOS_DOSTAWCY.AKTYWNY,
      DNIREAL type of column INT_IMP_DOS_DOSTAWCY.DNIREAL,
      LIMITKREDYTOWY type of column INT_IMP_DOS_DOSTAWCY.LIMITKREDYTOWY,
      TERMINDNI type of column INT_IMP_DOS_DOSTAWCY.TERMINDNI,
      WALUTA type of column INT_IMP_DOS_DOSTAWCY.WALUTA,
      RABAT type of column INT_IMP_DOS_DOSTAWCY.RABAT,
      TYPDOKMAG type of column INT_IMP_DOS_DOSTAWCY.TYPDOKMAG,
      TYPDOKFAK type of column INT_IMP_DOS_DOSTAWCY.TYPDOKFAK,
      BANK type of column INT_IMP_DOS_DOSTAWCY.BANK,
      RACHUNEK type of column INT_IMP_DOS_DOSTAWCY.RACHUNEK,
      ZAPLATA type of column INT_IMP_DOS_DOSTAWCY.ZAPLATA,
      ZAPLATAID type of column INT_IMP_DOS_DOSTAWCY.ZAPLATAID,
      SKADTABELA type of column INT_IMP_DOS_DOSTAWCY.SKADTABELA,
      SKADREF type of column INT_IMP_DOS_DOSTAWCY.SKADREF,
      DEL type of column INT_IMP_DOS_DOSTAWCY.DEL,
      HASHVALUE type of column INT_IMP_DOS_DOSTAWCY.HASH,
      REC type of column INT_IMP_DOS_DOSTAWCY.REC,
      SESJA type of column INT_IMP_DOS_DOSTAWCY.SESJA)
  returns (
      REF DOSTAWCA_ID)
   as
begin
  insert into int_imp_dos_dostawcy(dostawcaid, id, nazwa, nip, regon, kraj, krajid,
      ulica, nrdomu, nrlokalu, kodpocztowy, miasto, telefon, komorka, fax, email, www, uwagi,
      aktywny, dnireal, limitkredytowy, termindni, waluta, rabat, typdokmag, bank, rachunek,
      dostawa, dostawaid, zaplata, zaplataid,
      "HASH", del, rec, skadtabela, skadref, sesja)
    values(:dostawcaid, substring(coalesce(:id,:nazwa) from 1 for 40) , :nazwa, :nip, :regon, :kraj, :krajid,
      :ulica, :nrdomu, :nrlokalu, :kodpocztowy, :miasto, :telefon, :komorka, :fax, :email, :www, :uwagi,
      :aktywny, :dnireal, :limitkredytowy, :termindni, :waluta, :rabat, :typdokmag, :bank, :rachunek,
      :dostawa, :dostawaid, :zaplata, :zaplataid,
      :hashvalue, :del, :rec, :skadtabela, :skadref, :sesja)
     returning ref into :ref;
  suspend;
end^
SET TERM ; ^
