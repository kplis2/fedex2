--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_CALC_AIB1_SETALT(
      PRSCHEDOPERIN integer,
      STARTTIME timestamp,
      ENDTIME timestamp,
      MACHINE integer,
      RSTARTFWT timestamp,
      VERIFIEDNUM integer)
  returns (
      PRSCHEDOPEROUT integer)
   as
declare variable prschedoperalt integer;
declare variable prschedoperalt1 integer;
declare variable mindif double precision;
declare variable startalt timestamp;
declare variable startalt1 timestamp;
declare variable prshopermaster integer;
declare variable prshoperaltmaster integer;
declare variable guide integer;
declare variable endalt timestamp;
declare variable endalt1 timestamp;
declare variable starttimeout timestamp;
declare variable endtimeout timestamp;
declare variable machineout integer;
declare variable machine1 integer;
declare variable frst smallint;
begin
  if(prschedoperin is null or starttime is null or endtime is null or rstartfwt is null
    or verifiednum is null) then exception test_break '2';--rstartfwt||' setalt';
--    exception test_break prschedoperin||' '||STARTTIME||' '||ENDTIME||' '||MACHINE||' '||RSTARTFWT;
  mindif = 1440;
  mindif = 1/:mindif;
  prschedoperout = :prschedoperin;
  starttimeout = :starttime;
  endtimeout = :endtime;
  machineout = :machine;
  --wyszukanie czy jest lepsza operacja alternatywna
  for select p.ref, p.prshopermaster, p.prshoperaltmaster, p.guide
    from prschedopers i
    left join prschedopers p on (p.guide = i.guide and p.prshoperaltmaster = i.prshoperaltmaster)
    left join prshopers mi on (i.prshopermaster = mi.ref)
    left join prshopers mp on (p.prshopermaster = mp.ref)
    left join prshopers psh on (p.shoper = psh.ref)
    left join prshopers ish on (i.shoper = ish.ref)
    where p.ref is not null and i.ref <> p.ref and i.ref = :prschedoperin
      and p.verified = 0
      and i.status = 0 and i.workplace is not null
      and i.prshopermaster is not null and i.prshoperaltmaster is not null
      and p.workplace is not null
      and (mi.opertype = 1 or ish.number = (select min(number) from prshopers where opermaster = mi.ref))
      and (mp.opertype = 1 or psh.number = (select min(number) from prshopers where opermaster = mp.ref))
    into :prschedoperalt, :prshopermaster, :prshoperaltmaster, :guide
  do begin
--    if(prshopermaster = prshoperaltmaster or not exists(select ref from prschedopers where guide = :guide and prshopermaster = :prshopermaster and ref < :prschedoperalt)) then begin
        --operacje rownolegle
      frst=1;
      for select ref from prschedopers where prshopermaster=:prshopermaster and guide=:guide
         into :prschedoperalt
      do begin
         execute procedure prsched_find_machine(:prschedoperalt,:rstartfwt, 1) returning_values :startalt,  :endalt, :machine;
         rstartfwt = :endalt;
         if (:frst=1) then begin
            frst=0;
            prschedoperalt1 = :prschedoperalt;
            startalt1 = :startalt;
            endalt1 = :endalt;
            machine1 = :machine;
         end
      end
      if (endalt + mindif <= endtimeout) then begin
        prschedoperout = :prschedoperalt1;
        starttimeout = :startalt1;
        endtimeout = :endalt1;
        machineout= :machine1;
        update prschedopers set activ = 1 where ref = :prschedoperalt1;
      end
--    end
  end
--    if(prschedoperout = 62312) then exception test_break rstartfwt||' '||PRSCHEDOPERIN;
  update prschedopers set STARTTIME = :starttimeout, ENDTIME = :endtimeout, MACHINE = :machineout, verified  = :verifiednum where ref=:prschedoperout;
end^
SET TERM ; ^
