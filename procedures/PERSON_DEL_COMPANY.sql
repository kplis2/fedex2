--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PERSON_DEL_COMPANY(
      PERSON integer,
      COMPANY integer,
      DELIFLAST smallint = 0)
   as
declare perscnt integer;
begin

  delete from eperscompany where person = :person and company = :company;

  if (deliflast = 1) then
  begin
  --usun person'a, jezeli nie ma juz powiazn do niego

    select count(*) from eperscompany
      where person = :person
        and company <> :company
      into: perscnt;

    if (perscnt = 0) then
      delete from persons where ref = :person;
  end
end^
SET TERM ; ^
