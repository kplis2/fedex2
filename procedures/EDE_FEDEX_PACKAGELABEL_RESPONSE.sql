--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_PACKAGELABEL_RESPONSE(
      EDEDOCSH_REF integer)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
   as
declare variable shippingdoc integer;
declare variable shippinglabel blob_utf8;
declare variable format string20;
declare variable packageref integer;
declare variable parent integer;
begin
  otable = 'LISTYWYSD';

  select oref
    from ededocsh
    where ref = :ededocsh_ref
  into :shippingdoc;
  oref = :shippingdoc;

  select p.val
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.name = 'format'
  into :format;

  for
    select p.id
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.name = 'paczka'
    into :parent
  do begin
    select p.val
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.name = 'refPaczki'
    and p.parent = :parent
    into :packageref;

    select p.val
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.name = 'etykieta'
    and p.parent = :parent
    into :shippinglabel;

    update or insert into listywysdopkrpt(doksped, opk, rpt, format)
      values(:shippingdoc,:packageref,:shippinglabel,:format)
      matching (doksped, opk, format);
  end

  suspend;
end^
SET TERM ; ^
