--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ANALIZA_DOST_OBL_SPRZ(
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      DOSTAWCA integer,
      MAG varchar(3) CHARACTER SET UTF8                           ,
      DATAOD timestamp,
      DATADO timestamp,
      SPRZEDAZ smallint)
  returns (
      VALSPR numeric(14,4),
      STATUS smallint)
   as
DECLARE VARIABLE CNT INTEGER;
begin
  VALSPR = 0;

  select sum(DOKUMROZ.ILOSC),count(*)
       from DOKUMROZ join DOKUMPOZ on ( DOKUMROZ.POZYCJA=DOKUMPOZ.REF )
                     join DOKUMNAG on ( DOKUMPOZ.DOKUMENT = DOKUMNAG.REF AND DOKUMPOZ.KTM = :KTM AND DOKUMPOZ.WERSJA=:WERSJA)
                     join defdokum on ( DEFDOKUM.SYMBOL = DOKUMNAG.TYP AND DEFDOKUM.WYDANIA=1 AND DEFDOKUM.zewn = 1)
                     left join DOSTAWY on( DOSTAWY.REF = DOKUMROZ.DOSTAWA)
       where
            (DOKUMNAG.MAGAZYN=:MAG or (:mag = '') or (:mag is null))
        and (DOKUMNAG.data >= :dataod and DOKUMNAG.data <=:datado)
        and ( DOSTAWY.DOSTAWCA = :DOSTAWCA or (:sprzedaz = 0))
  into :VALSPR,:cnt;
  if(:VALSPR is null) then valspr = 0;
  STATUS = -1;
  STATUS = 1;
end^
SET TERM ; ^
