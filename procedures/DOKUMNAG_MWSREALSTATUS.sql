--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_MWSREALSTATUS(
      DOCID integer,
      MODE smallint)
   as
declare variable SPEDYCJA smallint;
declare variable GRUPASPED integer;
declare variable STAN varchar(10);
declare variable AKCEPT smallint;
declare variable STATUS smallint; /* 0 niewygenerowane, 1 - wygenerowane, 2 - w trakcie szykowania, 3 - naszykowane, 4 - w trakcie pakowania, 5 - spakowane, 6 - wyslane */
declare variable DPREF integer;
declare variable POS integer;
declare variable POSC integer;
declare variable PROGRESS varchar(255);
declare variable STM varchar(1024);
declare variable MWS smallint;
declare variable WYDANIA smallint;
declare variable DOC smallint;
declare variable ORD smallint;
declare variable OK smallint;
declare variable OSTATUS smallint;
declare variable ADDGOODS integer;
declare variable MWSMAG varchar(3);
begin
  select d.grupasped, coalesce(d.mwsdoc,0), coalesce(s.listywys,0), coalesce(d.wydania,0), coalesce(d.mwsmag, '')
    from dokumnag d
      left join sposdost s on (s.ref = d.sposdost)
    where d.ref = :docid
    into grupasped, mws, spedycja, wydania, mwsmag;
  -- statusy sprawdzamy od konca
  if (mwsmag <> '') then mws = 1;
  status = null;
  if (wydania = 1) then
  begin
    if (mode <> 0) then
    begin
      select first 1 t.stan, l.akcept
        from dokumnag d
          left join listywysdpoz p on (p.doktyp = 'M' and p.dokref = d.ref)
          left join listywysd l on (l.ref = p.dokument)
          left join listywys t on (t.ref = l.listawys)
        where d.grupasped = :grupasped
        order by d.koryg
        into stan, akcept;
      if (akcept = 2) then
      begin
        if (stan is not null and stan <> '') then
          status = 6;
        else
          status = 5;
      end
      else if (akcept in (0,1)) then
      begin
        status = 4;
      end else if (akcept is null) then
      begin
        dpref = null;
        select first 1 p.ref
          from dokumnag d
            left join dokumpoz p on (p.dokument = d.ref)
          where d.grupasped = :grupasped and p.ilosconmwsactsc <> case when d.akcept = 1 then p.iloscl else p.ilosc end
            and coalesce(p.havefake,0) = 0 and coalesce(p.fake,0) = 0 and d.wydania = 1
          into dpref;
        if (dpref is null) then
          status = 3;
      end
      if (status is null and mws > 0) then
      begin
        dpref = null;
        select first 1 a.ref
          from dokumnag d
            left join mwsacts a on (a.docid = d.ref)
          where d.grupasped = :grupasped and a.status in (2,5)
          into dpref;
        if (dpref is not null) then
          status = 2;
        if (dpref is null) then
          select first 1 o.ref
            from dokumnag d
              left join mwsacts a on (a.docid = d.ref)
              left join mwsords o on (o.ref = a.mwsord)
            where d.grupasped = :grupasped and o.status in (2)
            into dpref;
      end
      if (status is null and mws > 0) then
      begin
        select first 1 a.ref
          from dokumnag d
            left join mwsacts a on (a.docid = d.ref)
          where d.grupasped = :grupasped and a.status not in (0,6)
          into dpref;
      end
      if (status is null and mws > 0) then
        status = 1;
    end
    if (status is null) then status = 0;
    select count(p.ref)
      from dokumnag d
        left join dokumpoz p on (p.dokument = d.ref)
        left join towary t on (t.ktm = p.ktm)
      where d.grupasped = :grupasped and (d.mwsdoc = 1 or coalesce(d.mwsmag,'') <> '')
        and (coalesce(p.havefake,0) = 0 or coalesce(p.fake,0) = 1)
        and t.usluga <> 1 and d.wydania = 1
      into pos;
    if (pos is null) then pos = 0;
    select count(p.ref)
      from dokumnag d
        left join dokumpoz p on (p.dokument = d.ref)
        left join towary t on (t.ktm = p.ktm)
      where d.grupasped = :grupasped and d.wydania = 1 and (d.mwsdoc = 1 or coalesce(d.mwsmag,'') <> '')
        and (coalesce(p.havefake,0) = 0 or coalesce(p.fake,0) = 1)
        and t.usluga <> 1
        and (case when (d.akcept = 1 and coalesce(p.fake,0) = 0) then p.iloscl else p.ilosc end) = p.ilosconmwsactsc
      into posc;
    if (posc is null) then posc = 0;
    progress = posc||' / '||pos;
  end
  else if (wydania = 0 and mws > 0) then
  begin
    pos = 0;
    posc = 0;
    addgoods = 0;
    ostatus = 0;
    select first 1 o.status
      from mwsords o where o.docid = :docid
      order by o.status
    into ostatus;
    for
      select doc, ord, case when docq = ordq then 1 else 0 end
        from MWS_REC_DOC_ALL_GOODS_DETS (:docid,0)
        into doc, ord, ok
    do begin
      if (doc = 1) then
        pos = pos + 1;
      if (doc = 1 and ord = 1 and ok = 1) then
        posc = posc + 1;
      if (pos = 0 and ord = 0) then
        addgoods = addgoods + 1;
    end
    if (ostatus = 0) then
      status = 0;
    else if (ostatus = 1) then
      status = 1;
    else if (ostatus = 2) then
      status = 2;
    else if (ostatus = 5) then
      status = 3;
    else
      status = 2;
    if (status = 3) then
      progress = pos||' / '||pos;
    else if (addgoods = 0) then
      progress = posc||' / '||pos;
    else if (addgoods > 0) then
      progress = posc||' / '||pos||' (+'||addgoods||')';
  end
  if (:progress is null) then progress = '';
  if (:status is null) then status = 0;
  if (mode > 0) then
    update dokumnag d set d.mwsrealprogress = :progress, d.mwsstatus = :status
      where grupasped = :grupasped and (coalesce(d.mwsrealprogress,'') <> coalesce(:progress,'') or coalesce(d.mwsstatus,0) <> coalesce(:status,0));
  else
    update dokumnag d set d.mwsrealprogress = :progress
       where grupasped = :grupasped and coalesce(d.mwsrealprogress,'') <> coalesce(:progress,'');
end^
SET TERM ; ^
