--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_FP_FGSP(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable ONDATE date;
declare variable FROMDATE date;
declare variable OUTOFWORK smallint;
declare variable SEX smallint;
declare variable BIRTHDATE date;
declare variable STARTDATE date;
declare variable AGE smallint;
declare variable EMPLTYPE smallint;
declare variable FIRSTPERIOD varchar(6);
declare variable LASTPERIOD varchar(6);
declare variable IPER varchar(6);
declare variable APERIOD varchar(6);
declare variable TOAPERIOD varchar(6);
declare variable ATODATE date;
declare variable TMP_FROMDATE date;
declare variable TMP_TODATE date;
declare variable MONTHS smallint;
declare variable AREF integer;
declare variable AECOLUMN integer;
declare variable NAME varchar(120);
declare variable BREAK_MONTHS smallint = 0;
begin
/*MW: Personel - procedura zwraca czy za pracownika ma być odprowadzana skadka
                 na FP i FGSP. Przy modyfikacji uwzglednic porc. EF_FP_FGSP_UCP*/
  ret = 1;

/*------------------------------------------------------------------------------
  1) Kobiety powyzej 55 roku zycia i mezczyzni powyzej 60 roku zycia sa zwolnieni
    sa z obowiazku skladek na FP i FGSP. Data liczona na ostatni dzien miesiaca
    poprzedzajacego okres ubezpieczeniowy */

  select fromdate - 1 from efunc_prdates(:payroll, 2)
    into :ondate;

  select e.outofwork30, p.sex, p.birthdate, e.personnames, fromdate
    from employees e
      join persons p on p.ref = e.person
    where e.ref = :employee
    into :outofwork, :sex, :birthdate, :name, :startdate;

  birthdate = coalesce(birthdate,ondate);
  
  if (birthdate > ondate) then
    exception universal 'Niepoprawna data urodzenia dla osoby ' || name;
  
  select y from get_ymd(:birthdate, :ondate)
    into :age;
  
  if ((age >= 60 and sex = 1) or (age >= 55 and sex = 0)) then
    ret = 0;
  else begin
/*------------------------------------------------------------------------------
 2) Jezeli pracownik ukonczyl 50 rok zycia i byl w rejestrze bezrobotnych przez
    dluzej niz 30 dni to przez rok od podjecia pracy nie musi oplacac FP i FGSP */

    select iper, empltype from epayrolls
      where ref = :payroll
      into :iper, :empltype;

    if (empltype = 1 and outofwork = 1 and age >= 50 and startdate is not null) then   
    begin
    --zwolnienie od nastpnego miesiaca po zatrudnieniu na okres roku
      select okres from datatookres(:startdate,0,1,0,0) into :firstperiod; --miesiac po zatrudnieniu
      select okres from datatookres(:startdate,1,0,0,0) into :lastperiod;  --rok po zatrudnieniu
  
      if (iper >= firstperiod and iper <= lastperiod) then
        ret = 0;
    end

    if (ret = 1) then begin
/*------------------------------------------------------------------------------
 3) Jeżeli pracownik wroci do pracy po urlopie macierzynskim lub wychowawczym
    pracodawca nie musi placic skladki przez 36 miesiecy od daty powrotu.
    Jezeli np. pracownik wraca z urlopu 2-go (lub pozniej) lutego i wyplata
    jest w lutym to skladke liczymy; jezeli wyplata jest w marcu to skl. nie liczymy */

      select fromdate from efunc_prdates(:payroll, 0)
        into :fromdate;

      ondate = cast((extract(year from fromdate) - 3) || '-' || extract(month from fromdate) || '-01' as date);

      if (ondate < '2009-01-01') then
        ondate = '2009-01-01';
    
      select first 1 ref, todate, ecolumn from eabsences
        where employee = :employee and correction in (0,2)
          and todate >= :ondate and todate < :fromdate
          and ecolumn in (140,150,260,270,280,440,450)
        order by fromdate desc
        into :aref, :atodate, :aecolumn;
  
      if (aref is not null) then
      begin
        if (aecolumn = 260) then
        begin
        /*jezeli ostatnia nieobecnosc to url. wychowawczy, to musimy sprawdzic,
          czy nastepuje on bezposrednio po macierzynskim, a jezeli nie, to
          odpowiednio wydluzyc przerwe 36 miesiecy (o liczbe m-cy :break_months)*/

          select afromdate from e_get_whole_eabsperiod(:aref,null,null,null,null,null,null)
            into :tmp_fromdate;  --poczatek wychowawczego

          aref = null;
          select first 1 ref, todate from eabsences
            where employee = :employee and correction in (0,2)
              and todate >= '2009-01-01' and todate < :atodate
              and ecolumn in (140,150,270,280,440,450)
            order by fromdate desc
            into :aref, :tmp_todate;
  
          if (aref is not null and tmp_fromdate - 1 <> tmp_todate) then
          begin
          /*miedzy macierzynskim a wychowawczym jest przerwa. Jezeli wychowawczy
            nie zaczal sie pierwszego dnia danego miesiaca, to ten m-c sie nie liczy */

            atodate = tmp_todate;
            for
              select distinct g.afromdate, g.atodate
                from eabsences a
                left join e_get_whole_eabsperiod(a.ref, null, (:fromdate - 1), null, null, null, null) g on (1 = 1)
                where a.employee = :employee
                  and a.correction in (0,2)
                  and a.todate > :atodate and a.todate < :fromdate
                  and a.ecolumn = :aecolumn
                order by fromdate desc
                into :tmp_fromdate, tmp_todate --czas trwania url. wychowawczego
            do begin
              execute procedure e_func_periodinc(null, 0, tmp_fromdate - 1) returning_values :aperiod ;
              execute procedure e_func_periodinc(null, 0, tmp_todate) returning_values :toaperiod;
    
              months = 12 * (cast(substring(toaperiod from 1 for 4) as integer) - cast(substring(aperiod from 1 for 4) as integer))
                          +  cast(substring(toaperiod from 5 for 2) as integer) - cast(substring(aperiod from 5 for 2) as integer);

              if (months > 0) then
                break_months = break_months + months;
            end
          end
        end else begin
        --pobranie pelnego zakresu trwania nieobecnosci url. macierzynskiego
         select atodate from e_get_whole_eabsperiod(:aref,null,null,null,null,null,';140;150;270;280;440;450;')
           into :atodate;
        end

        execute procedure e_func_periodinc(null, 1, atodate)
          returning_values :aperiod;
        execute procedure e_func_periodinc(aperiod, 35 + :break_months)
          returning_values :toaperiod;
    
        if (aperiod <= iper and iper <= toaperiod) then
          ret = 0;
      end
    end
  end

  suspend;
end^
SET TERM ; ^
