--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STATEMENT_PARSE(
      SOURCE varchar(8191) CHARACTER SET UTF8                           ,
      KEY1 integer,
      KEY2 integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,4))
   as
DECLARE VARIABLE DECL VARCHAR(8191);
DECLARE VARIABLE BODY VARCHAR(8191);
DECLARE VARIABLE LASTVAR INTEGER;
begin
  execute procedure STATEMENT_MULTIPARSE(:source, :prefix, '', '', 1, 1)
    returning_values :decl, :body, :lastvar;
  execute procedure STATEMENT_EXECUTE(:decl, :body, :key1, :key2, 0)
    returning_values :ret;
end^
SET TERM ; ^
