--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHCALCS_COPY(
      FROMPRSHCALC integer,
      NEWNAME varchar(255) CHARACTER SET UTF8                           )
  returns (
      STATUS integer)
   as
declare variable toprshcalc integer;
declare variable ref integer;
begin
  -- procedura kopiuje naglowek kalkulacj i pozycje

  -- skopiuj naglowek kalkulacji
  execute procedure GEN_REF('PRSHCALCS') returning_values :toprshcalc;
  insert into prshcalcs(ref, sheet, method, descript, status)
  select :toprshcalc, sheet, method, :newname, 0
  from prshcalcs where ref=:fromprshcalc;


  -- przejdz sie po rubrykach na pierwszym poziomie i wywolaj dla nich rekurencje
  for select ref from prcalccols where calc=:fromprshcalc and parent is null
  into :ref
  do begin
    execute procedure PRSHCALCS_SUBCOPY(:fromprshcalc,:ref,:toprshcalc,NULL);
  end

  status = 1;
end^
SET TERM ; ^
