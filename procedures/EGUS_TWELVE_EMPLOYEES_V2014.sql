--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EGUS_TWELVE_EMPLOYEES_V2014(
      GYEAR varchar(4) CHARACTER SET UTF8                           ,
      COMPANY integer,
      NOTRANDOMLY smallint = 0)
  returns (
      GEXPKEY varchar(40) CHARACTER SET UTF8                           )
   as
declare variable EMPLOYEE integer;
declare variable PERSON integer;
declare variable FROMDATE date;
declare variable TODATE date;
declare variable RETIREMENT smallint;
declare variable WORKAWDAYS smallint;
declare variable FREEDAYS smallint;
declare variable rehabwdays smallint;
declare variable INSCHOOL smallint;
declare variable SEX smallint;
declare variable EMPLCOUNT integer;
declare variable DIVISOR integer;
declare variable PROFESSCODE varchar(20);
declare variable GFNAME varchar(30);
declare variable GSNAME varchar(60);
declare variable GID varchar(10);
declare variable CHILDCAREDAYS integer;
declare variable notwdays_notworks numeric(14,2);
declare variable notworkeddays numeric(14,2);
declare variable notwdays_works numeric(14,2);

begin
/*MatJ Personel: procedura generuje liste zatrudnionych w jednosce sprawozdawczej
  na potrzeby sprawozdania GUS Z-12 (na rok 2014). Sposob wyboru/losowania pracownikow
  definuje dokumentacja (pozycja 'A3'). Stan zatrudn. badamy na pazdziernik zadanego
  roku GYEAR. Wyniki losowania znajduja w tab. EXPIMP dla S1 = :GEXPKEY.*/

  company = coalesce(company,1);
  emplcount = 0;
  fromdate = cast(gyear || '/10/01' as date);
  todate = cast(gyear || '/10/31' as date);

  for
    select distinct e.ref, e.person, e.sname, e.fname, e.ref,
          abs(coalesce(p.sex,0) - 2), coalesce(g.code,'')
      from persons p
        join employees e on (p.ref = e.person)
        join employment m on (m.employee = e.ref)
        left join edictworkposts w on (w.ref = m.workpost)
        left join edictguscodes g on (g.ref = w.guscode)
      where e.company = :company
        and m.fromdate <= :todate and (m.todate is null or m.todate >= :fromdate)
        and m.fromdate = (select max(m2.fromdate) from employment m2 where m2.employee = m.employee  --gwarancja jednego rekordu dla pracownika
                             and m2.fromdate <= :todate and (m2.todate is null or m2.todate >= :fromdate))
      order by coalesce(g.code,''), abs(coalesce(p.sex,0) - 2), e.personnames
      into :employee, :person, :gsname, :gfname, :gid, :sex, :professcode
  do begin
  /*nalezy przetestowac wykluczenia pracownika wg podanych w dokumentacji warunkow,
    na poczatku testy 1,2,4 (przebywanie na zwoln. chorobowym i zasilku ZUS)*/

  --BS108630:
    notworkeddays = 0; -- czas nieprzepracowany ogolem,
    notwdays_works = 0; -- czas nieprzepracowany oplacony tylko przez zaklad pracy
    notwdays_notworks = 0; -- czas nieprzepracowany nie oplacony przez zaklad pracy

    select notworkeddays, notwdays_works
      from egus_twelve_worktime(2, :employee, :fromdate, :todate)
      into notworkeddays, :notwdays_works;

    notwdays_notworks = notworkeddays - notwdays_works;

    if (not(notwdays_works > 10 or notwdays_notworks > 2)) then
    begin
      workawdays = 0;
      rehabwdays = 0;
      freedays = 0;
      childcaredays = 0;
      select sum(case when ecolumn in (40,50,60,120,130,220,230) then workdays else 0 end),
             sum(case when ecolumn in (350,360,370) then workdays else 0 end), --sw rehabilitacyjne
             sum(case when ecolumn = 300 then days else 0 end), --bezplatny urlop
             sum(case when ecolumn = 260 then days else 0 end)  --url wychowawczy
        from eabsences
        where employee = :employee and correction in (0,2)
          and fromdate >= :fromdate and todate <= :todate
        into :workawdays, :rehabwdays, :freedays, :childcaredays;
  
      if (freedays > 0) then
      begin
        select first 1 1 from eabsences
            left join e_get_whole_eabsperiod(ref, null , null, null, null, null, ';300;') on (1 = 1)
          where ecolumn = 300 and correction in (0,2) and employee = :employee
            and fromdate >= :fromdate and todate <= :todate and (atodate - afromdate) > 90
        into :freedays;
      end
  
      if (childcaredays > 0) then
      begin
        select first 1 1 from eabsences
            left join e_get_whole_eabsperiod(ref, null , null, null, null, null, ';260;') on (1 = 1)
          where ecolumn = 260 and correction in (0,2) and employee = :employee
            and fromdate >= :fromdate and todate <= :todate and (atodate - afromdate) > 90
        into :childcaredays;
      end

      if (coalesce(rehabwdays,0) = 0 and coalesce(freedays,0) = 0 and coalesce(childcaredays,0) = 0) then
      begin
      --test nr 5: pracownik nie moze jeednoczesnie pracowac i byc na emeryturze
        retirement = 0;
        select first 1 1 from eperpension
          where person = :person and fromdate <= :todate and typepens = 1
          order by fromdate desc
          into :retirement;
  
        if (coalesce(retirement,0) = 0) then
          select first 1 case when c.code = 1 then 1 else 0 end from ezusdata z
              join edictzuscodes c on (c.ref = z.retired)
            where z.person = :person and z.fromdate <= :todate
            order by z.fromdate desc
            into :retirement;
  
        if (coalesce(retirement,0) = 0) then
        begin
        --test nr 12: pracownik nie moze byc w szkole, na studiach i odbywac praktyk
          inschool = 0;
          select first 1 1 from eperschools
            where person = :person
             and (fromdate is null or fromdate <= :todate)
             and (todate is null or todate >= :fromdate)
            into :inschool;
  
          if (coalesce(inschool,0) = 0) then
          begin
          --pracownik zakwalifikowany jako zatrudniony w spraw. Z-12
  
            emplcount = emplcount + 1;
            if (gexpkey is null) then
            begin
            --gener. unikalnego klucza wskazujacego w EXPIMP biezace wywolanie analizy
              execute procedure gen_ref('EXPIMP') returning_values :gexpkey;
              gexpkey = company || 'Z12' ||  '/' || coalesce(gexpkey,'0');
            end
            insert into expimp (s1, s2, s3, s5, s6, s7, s8, s9) --istotne tylko s1,s2,s3
              values (:gexpkey, :emplcount, :employee, :professcode, :sex, :gfname, :gsname, :gid);
          end
        end
      end
    end
  end

  if (emplcount > 40 and coalesce(notrandomly,0) = 0) then  --BS108630
  begin
  --losowanie pracownikow - 'wylaczeni' maja nullowany nr(S1) i ref(S2) w EXPIMP
    if (emplcount > 40 and emplcount <= 176) then divisor = 2;
    else if (emplcount > 176 and emplcount <= 396) then divisor = 3;
    else if (emplcount > 396 and emplcount <= 704) then divisor = 4;
    else if (emplcount > 704 and emplcount <= 1100) then divisor = 5;
    else if (emplcount > 1100 and emplcount <= 1584) then divisor = 6;
    else if (emplcount > 1584 and emplcount <= 2156) then divisor = 7;
    else if (emplcount > 2156 and emplcount <= 2816) then divisor = 8;
    else if (emplcount > 2816 and emplcount <= 3564) then divisor = 9;
    else if (emplcount > 3564 and emplcount <= 4400) then divisor = 10;
    else if (emplcount > 4400 and emplcount <= 5324) then divisor = 11;
    else if (emplcount > 5324 and emplcount <= 6336) then divisor = 12;
    else if (emplcount > 6336 and emplcount <= 7436) then divisor = 13;
    else divisor = 14;
  
    update expimp set s2 = null, s3 = null
      where s1 = :gexpkey and s2 > '' and mod(s2,:divisor) <> 1;
  end

  suspend;
end^
SET TERM ; ^
