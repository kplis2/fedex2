--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AVALIBLEBKACCOUNTSFROMPATTERN(
      DCOMPANY COMPANIES_ID,
      DYEAR YEARS_ID)
  returns (
      BREF BKACCOUNTS_ID,
      BSYMBOL BKSYMBOL_ID,
      BDESCRIPT STRING60)
   as
declare variable pcompany companies_id;
declare variable pyear years_id;
declare variable pattern bkyears_id;
begin
  select ba.patternref
    from bkyears ba
    where ba.company= :dcompany and ba.yearid = :dyear
    into :pattern;

  select ba.company, ba.yearid
    from bkyears ba
    where ba.ref = :pattern
    into :pcompany, :pyear;

  for
    select b.ref, b.symbol, b.descript
      from bkaccounts b
      where b.yearid = :pyear and b.company = :pcompany
        and b.ref not in (select pattern_ref
                            from bkaccounts
                            where company = :dcompany
                              and yearid =:dyear)
    into :bref, :bsymbol, :bdescript
  do begin
    suspend;
  end
end^
SET TERM ; ^
