--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRMACHINESWPLACE(
      MACHINE integer,
      FORDAY timestamp)
  returns (
      WPLACE varchar(20) CHARACTER SET UTF8                           )
   as
begin
  FORDAY = COALESCE(FORDAY, current_timestamp(0));
  select PRWORKPLACE from PRMACHINESWPLACES
    where PRMACHINE=:machine and FROMDATE<=:FORDAY and (TODATE>=:FORDAY or TODATE is null)
    into wplace;
  suspend;
end^
SET TERM ; ^
