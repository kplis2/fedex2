--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRDBOTTOMUP_DEPGEN(
      FRDPERSPECT integer,
      ELEMHIER varchar(80) CHARACTER SET UTF8                           ,
      FRDHDRVER integer,
      FRDCELL integer,
      TIMEELEM integer,
      FRDPERSDIMHIER integer,
      FRDPERSDIMVAL integer)
   as
declare variable calcmeth smallint;
declare variable hierorder integer;
declare variable frpsn integer;
declare variable element varchar(40);
declare variable elem integer;
declare variable cut smallint;
declare variable cellup integer;
declare variable mainperspect smallint;
declare variable plancalcmeth smallint;
declare variable realcalcmeth smallint;
declare variable val1calcmeth smallint;
declare variable val2calcmeth smallint;
declare variable val3calcmeth smallint;
declare variable val4calcmeth smallint;
declare variable val5calcmeth smallint;
declare variable plancalcfroms varchar(40);
declare variable realcalcfroms varchar(40);
declare variable val1calcfroms varchar(40);
declare variable val2calcfroms varchar(40);
declare variable val3calcfroms varchar(40);
declare variable val4calcfroms varchar(40);
declare variable val5calcfroms varchar(40);
begin
 --oreslenie czy jestem w glownej perspektywie
  select frdperspects.mainperspect
    from frdperspects
    where frdperspects.ref = :frdperspect
    into :mainperspect;
 -- nie liczymy zaleznosci dla komorek perspektywy glownej
  if (:mainperspect = 1) then
    exit;
  select frdpersdimvals.frpsn
    from frdpersdimvals
    where frdpersdimvals.ref = :frdpersdimval
    into :frpsn;
  select frdpersdimhier.hierorder,
      frdpersdimhier.plancalcmeth, frdpersdimhier.plancalcfroms,
      frdpersdimhier.realcalcmeth, frdpersdimhier.realcalcfroms,
      frdpersdimhier.val1calcmeth, frdpersdimhier.val1calcfroms,
      frdpersdimhier.val2calcmeth, frdpersdimhier.val2calcfroms,
      frdpersdimhier.val3calcmeth, frdpersdimhier.val3calcfroms,
      frdpersdimhier.val4calcmeth, frdpersdimhier.val4calcfroms,
      frdpersdimhier.val5calcmeth, frdpersdimhier.val5calcfroms
    from frdpersdimvals
      join frdpersdimhier on (frdpersdimvals.frdpersdimshier = frdpersdimhier.ref)
    where frdpersdimvals.ref = :frdpersdimval
    into :hierorder,
      :plancalcmeth, :plancalcfroms,
      :realcalcmeth, :realcalcfroms,
      :val1calcmeth, :val1calcfroms,
      :val2calcmeth, :val2calcfroms,
      :val3calcmeth, :val3calcfroms,
      :val4calcmeth, :val4calcfroms,
      :val5calcmeth, :val5calcfroms;
 -- sumowanie do glównej perspektywy -> korzen drzewa
  if (:hierorder = 0) then
  begin
    execute procedure string_reverse(:elemhier) returning_values :elemhier;
    cut = position(',' in :elemhier);
    if (:cut > 0) then begin
      elemhier = substring(:elemhier from  :cut + 1 for coalesce(char_length(:elemhier),0)); -- [DG] XXX ZG119346
      element = :elemhier;
      cut = position(',' in :elemhier);
      if (:cut = 0) then begin
        execute procedure string_reverse(:element) returning_values :element;
        elem = cast(:element as integer);
        execute procedure string_reverse(:elemhier) returning_values :elemhier;
      end else
        exit;
    end else
      exit;
    select frdpersdimvals.ref
      from frdpersdimvals
        join frdpersdimhier on (frdpersdimhier.ref = frdpersdimvals.frdpersdimshier
          and frdpersdimvals.frpsn = :frpsn)
        join frdperspects on (frdpersdimhier.frdperspect = frdperspects.ref)
      where frdperspects.mainperspect = 1
        and frdpersdimhier.hierorder = :hierorder
      into :frdpersdimval;
    select frdcells.ref
      from frdcells
      where frdcells.frdhdrver = :frdhdrver and frdcells.frdpersdimval = :frdpersdimval
        and frdcells.dimelem = :elem and frdcells.timelem = :timeelem and frdcells.elemhier = :elemhier
      into :cellup;
  end
  -- sumowanie wewnatrz perspektywy -> w ramach galezi drzewa
  else
  begin
    execute procedure string_reverse(:elemhier) returning_values :elemhier;
    cut = position(',' in :elemhier);
    if (:cut > 0) then begin
      elemhier = substring(:elemhier from  :cut + 1 for coalesce(char_length(:elemhier),0)); -- [DG] XXX ZG119346
      cut = position(',' in :elemhier);
      if (:cut > 0) then begin
        element = substring(:elemhier from  1 for  :cut - 1);
        execute procedure string_reverse(:element) returning_values :element;
        elem = cast(:element as integer);
        execute procedure string_reverse(:elemhier) returning_values :elemhier;
      end else
        exit;
    end else
      exit;
    select frdpersdimvals.ref
      from frdpersdimvals
        join frdpersdimhier on (frdpersdimhier.ref = frdpersdimvals.frdpersdimshier
          and frdpersdimvals.frpsn = :frpsn)
      where frdpersdimhier.frdperspect = :frdperspect
        and frdpersdimhier.hierorder = :hierorder - 1
      into :frdpersdimval;
    select frdcells.ref
      from frdcells
      where frdcells.frdhdrver = :frdhdrver and frdcells.frdpersdimval = :frdpersdimval
        and frdcells.dimelem = :elem and frdcells.timelem = :timeelem and frdcells.elemhier = :elemhier
      into :cellup;
  end
--    exception test_break :frdcell||' '||:cellup||' '||:plancalcmeth||' '||:plancalcfroms;
  if (:cellup is not null) then begin
    if (:plancalcmeth > 0 and :plancalcfroms is not null and :plancalcfroms <> '') then
      if (not exists(select ref from frdcellsbottomup where frdcellsbottomup.frdcell = :cellup
          and frdcellsbottomup.field = :plancalcfroms and frdcellsbottomup.frdcelladd = :frdcell
          and frdcellsbottomup.addfield = 'PLANAMOUNT' and frdcellsbottomup.bottomup = 1)) then
        insert into frdcellsbottomup (frdcell, field, frdcelladd, addfield, bottomup)
          values(:cellup, :plancalcfroms,:frdcell,'PLANAMOUNT', 1);
    if (:realcalcmeth > 0 and :realcalcfroms is not null and :realcalcfroms <> '') then
      if (not exists(select ref from frdcellsbottomup where frdcellsbottomup.frdcell = :cellup
          and frdcellsbottomup.field = :realcalcfroms and frdcellsbottomup.frdcelladd = :frdcell
          and frdcellsbottomup.addfield = 'REALAMOUNT' and frdcellsbottomup.bottomup = 1)) then
        insert into frdcellsbottomup (frdcell, field, frdcelladd, addfield, bottomup)
          values(:cellup, :realcalcfroms,:frdcell,'REALAMOUNT', 1);
    if (:val1calcmeth > 0 and :val1calcfroms is not null and :val1calcfroms <> '') then
      if (not exists(select ref from frdcellsbottomup where frdcellsbottomup.frdcell = :cellup
          and frdcellsbottomup.field = :val1calcfroms and frdcellsbottomup.frdcelladd = :frdcell
          and frdcellsbottomup.addfield = 'VAL1' and frdcellsbottomup.bottomup = 1)) then
        insert into frdcellsbottomup (frdcell, field, frdcelladd, addfield, bottomup)
          values(:cellup, :val1calcfroms,:frdcell,'VAL1', 1);
    if (:val2calcmeth > 0 and :val2calcfroms is not null and :val2calcfroms <> '') then
      if (not exists(select ref from frdcellsbottomup where frdcellsbottomup.frdcell = :cellup
          and frdcellsbottomup.field = :val2calcfroms and frdcellsbottomup.frdcelladd = :frdcell
          and frdcellsbottomup.addfield = 'VAL2' and frdcellsbottomup.bottomup = 1)) then
        insert into frdcellsbottomup (frdcell, field, frdcelladd, addfield, bottomup)
          values(:cellup, :val2calcfroms,:frdcell,'VAL2', 1);
    if (:val3calcmeth > 0 and :val3calcfroms is not null and :val3calcfroms <> '') then
      if (not exists(select ref from frdcellsbottomup where frdcellsbottomup.frdcell = :cellup
          and frdcellsbottomup.field = :val3calcfroms and frdcellsbottomup.frdcelladd = :frdcell
          and frdcellsbottomup.addfield = 'VAL3' and frdcellsbottomup.bottomup = 1)) then
        insert into frdcellsbottomup (frdcell, field, frdcelladd, addfield, bottomup)
          values(:cellup, :val3calcfroms,:frdcell,'VAL3', 1);
    if (:val4calcmeth > 0 and :val4calcfroms is not null and :val4calcfroms <> '') then
      if (not exists(select ref from frdcellsbottomup where frdcellsbottomup.frdcell = :cellup
          and frdcellsbottomup.field = :val4calcfroms and frdcellsbottomup.frdcelladd = :frdcell
          and frdcellsbottomup.addfield = 'VAL4' and frdcellsbottomup.bottomup = 1)) then
        insert into frdcellsbottomup (frdcell, field, frdcelladd, addfield, bottomup)
          values(:cellup, :val4calcfroms,:frdcell,'VAL4', 1);
    if (:val5calcmeth > 0 and :val5calcfroms is not null and :val5calcfroms <> '') then
      if (not exists(select ref from frdcellsbottomup where frdcellsbottomup.frdcell = :cellup
          and frdcellsbottomup.field = :val5calcfroms and frdcellsbottomup.frdcelladd = :frdcell
          and frdcellsbottomup.addfield = 'VAL5' and frdcellsbottomup.bottomup = 1)) then
        insert into frdcellsbottomup (frdcell, field, frdcelladd, addfield, bottomup)
          values(:cellup, :val5calcfroms,:frdcell,'VAL5', 1);
  end
end^
SET TERM ; ^
