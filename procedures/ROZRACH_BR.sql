--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ROZRACH_BR(
      TRYB integer,
      RODZAJ smallint,
      OGRSLODEF integer,
      OGRSLOPOZ integer,
      ZAKRES smallint,
      STRONA smallint,
      ODDATY timestamp,
      DODATY timestamp,
      SODDZIAL varchar(20) CHARACTER SET UTF8                           ,
      TERMIN timestamp,
      COMPANYIN smallint)
  returns (
      SLODEF integer,
      SLOPOZ integer,
      KONTOFK KONTO_ID,
      SYMBFAK varchar(20) CHARACTER SET UTF8                           ,
      WINIEN numeric(14,2),
      MA numeric(14,2),
      WINIENZL numeric(14,2),
      MAZL numeric(14,2),
      WALUTA varchar(3) CHARACTER SET UTF8                           ,
      WALUTOWY smallint,
      SALDO numeric(14,2),
      DATAOTW timestamp,
      DATAPLAT timestamp,
      SLODEFDICT varchar(20) CHARACTER SET UTF8                           ,
      SLOKOD varchar(100) CHARACTER SET UTF8                           ,
      SLONAZWA varchar(255) CHARACTER SET UTF8                           ,
      SLOKODKS ANALYTIC_ID,
      TYP char(1) CHARACTER SET UTF8                           ,
      KOLOR smallint,
      ILDNIMAX integer,
      ILDOK integer,
      SLOBANKACC integer,
      SLOBANKACCDICT varchar(255) CHARACTER SET UTF8                           ,
      RIGHTS varchar(40) CHARACTER SET UTF8                           ,
      RIGHTSGROUP varchar(40) CHARACTER SET UTF8                           ,
      BTRANAMOUNT numeric(14,2),
      BTRANAMOUNTAKC numeric(14,2),
      DICTGRP integer,
      ODDZIAL varchar(20) CHARACTER SET UTF8                           ,
      BTRANAMOUNTBANK numeric(14,2),
      SLOSPRZEDAWCA integer,
      REF integer,
      COMPANY integer)
   as
declare variable kont ACCOUNT_ID;
declare variable cnt integer;
declare variable data timestamp;
declare variable klislodef integer;
declare variable dostawca integer;
declare variable ogrslodef2 integer;
declare variable ogrslopoz2 integer;
begin
  if(:ogrslodef=0) then ogrslodef = null;
  if(:ogrslopoz=0) then ogrslopoz = null;
  select ref from slodef where typ='KLIENCI' into :klislodef;
  dostawca = null;
  ogrslodef2 = null;
  ogrslopoz2 = null;
  slosprzedawca = null;
  if(:ogrslodef=:klislodef and :ogrslopoz is not null) then begin
    select dostawca from klienci where ref=:ogrslopoz into :dostawca;
    if(:dostawca is not null) then begin
      select ref from slodef where typ='DOSTAWCY' into :ogrslodef2;
      if(:ogrslodef2 is not null) then ogrslopoz2 = :dostawca;
    end
  end
  data = current_date;
  if(:rodzaj = 0) then begin
    for select ROZRACH.REF, SLODEF, SLOPOZ, KONTOFK, SYMBFAK, WINIEN, MA, WINIENZL, MAZL, waluta, WALUTOWY,
      (winien - ma ), DATAOTW, DATAPLAT, SLODEF.NAZWA,ROZRACH.TYP, ROZRACH.bankaccount, ROZRACh.rights, ROZRACH.rightsgroup,
      ROZRACH.btranamount, ROZRACH.btranamountakc,
      ROZRACH.oddzial, ROZRACH.dictgrp,rozrach.btranamountbank,
      rozrach.company
     from ROZRACH left join SLODEF on ( SLODEF.REF = ROZRACH.SLODEF)
     where (
         (
              ((:OGRSLODEF is null or :ogrslodef = ROZRACH.SLODEF) and (:OGRSLOPOZ is null or :OGRSLOPOZ = ROZRACH.slopoz))
           or (:ogrslodef2 is not null and :ogrslodef2 = ROZRACH.SLODEF and :ogrslopoz2 is not null and :OGRSLOPOZ2 = ROZRACH.slopoz)
         )
         and ((:strona = 0) or (:strona = 1 and (ROZRACH.TYP = 'N' or (ROZRACH.WINIEN  - ROZRACH.MA >0)))
                            or (:strona = 2 and (ROZRACH.TYP = 'Z' or (ROZRACH.MA - ROZRACH.WINIEN > 0)))
             )
         and ((:zakres = 0) or
              (:zakres = 1 and ROZRACH.WINIEN <> ROZRACH.MA) or
              (:zakres = 2 and ROZRACH.WINIEN <> ROZRACH.MA and ROZRACH.DATAPLAT < current_date)
             )
         and (:oddaty is null or (rozrach.datazamk is null) or (rozrach.datazamk >= :oddaty))
         and (:dodaty is null or (rozrach.dataotw <= :dodaty))
         and ((:soddzial is null) or (:soddzial = '')or (ROZRACH.ODDZIAL = :soddzial))
         and ((:termin is null) or (ROZRACH.dataplat <= :termin))
         and (:company is null or (ROZRACH.company = :company))
     )
     into :ref, :slodef, :slopoz, :kontofk, :symbfak, :winien, :ma, :winienzl, :mazl, :waluta , :walutowy,
      :saldo, :dataotw, :dataplat, :slodefdict,:typ, :slobankacc, :rights, :rightsgroup, :btranamount, :btranamountakc, :oddzial, :dictgrp,:BTRANAMOUNTBANK,
      :company
    do begin
      if(:kontofk is null) then kontofk = '';
      if(:saldo <> 0 and :dataplat < current_date)then kolor = 2;
      else if(:saldo <> 0 and :dataplat < current_date) then kolor = 1;
      else kolor = 0;
      if(:tryb = 0) then begin
         execute procedure SLO_DANEKS(:slodef, :slopoz) returning_values :slokod, :slonazwa, :kont, :slokodks;
         select sprzedawca
         from SLO_DANEDOD(:slodef, :slopoz)
         into :slosprzedawca;
         slobankaccdict = '';
         if(:slobankacc > 0) then
            select BANK from BANKACCOUNTS where REF=:slobankacc into :slobankaccdict;
         suspend;
      end else begin
        cnt = null;
        if(:tryb = 2) then
          select count(*) from ROZRACH_KOMP(:slodef, :slopoz, :kontofk, :waluta, :winien - :ma,NULL,NULL,1) into :cnt;
        else
          select count(*) from ROZRACH_KOMP(:slodef, :slopoz, NULL, :waluta, :winien - :ma,NULL,NULL,1) into :cnt;
        if(:cnt is null) then cnt = 0;
        if(:kontofk is null) then kontofk = '';
        if(:saldo <> 0 and :dataplat < current_date)then kolor = 2;
        else if(:saldo <> 0 and :dataplat < current_date) then kolor = 1;
        else kolor = 0;
        if(:cnt > 0) then begin
          execute procedure SLO_DANEKS(:slodef, :slopoz) returning_values :slokod, :slonazwa, :kont, :slokodks;
          select sprzedawca
          from SLO_DANEDOD(:slodef, :slopoz)
          into :slosprzedawca;
          slobankaccdict = '';
          if(:slobankacc > 0) then
             select BANK from BANKACCOUNTS where REF=:slobankacc into :slobankaccdict;
          suspend;
        end
      end
    end
  end else begin
    symbfak = '';
    kontofk = '';
    rights = '';
    rightsgroup = '';
    for select max(rozrach.ref),  SLODEF, SLOPOZ,count(*), sum(WINIEN) , sum(MA), sum(WINIENZL), sum(MAZL), waluta, max(WALUTOWY),
      sum (winien - ma ), min(DATAOTW), min(DATAPLAT), max(SLODEF.NAZWA),
      max(rozrach.company)
     from ROZRACH join SLODEF on ( SLODEF.REF = ROZRACH.SLODEF
         and (
              ((:OGRSLODEF is null or :ogrslodef = ROZRACH.SLODEF) and (:OGRSLOPOZ is null or :OGRSLOPOZ = ROZRACH.slopoz))
           or (:ogrslodef2 is not null and :ogrslodef2 = ROZRACH.SLODEF and :ogrslopoz2 is not null and :OGRSLOPOZ2 = ROZRACH.slopoz)
         )
         and ((:OGRSLODEF is null) or (:ogrslodef = 0)or (:ogrslodef = ROZRACH.SLODEF))
         and ((:OGRSLOPOZ is null) or (:OGRSLOPOZ = 0)or (:OGRSLOPOZ = ROZRACH.slopoz))
         and ((:strona = 0) or (:strona = 1 and (ROZRACH.TYP = 'N' or (ROZRACH.WINIEN  - ROZRACH.MA >0)))
                            or (:strona = 2 and (ROZRACH.TYP = 'Z' or (ROZRACH.MA - ROZRACH.WINIEN > 0)))
             )
         and ((:zakres = 0) or
              (:zakres = 1 and ROZRACH.WINIEN <> ROZRACH.MA) or
              (:zakres = 2 and ROZRACH.WINIEN <> ROZRACH.MA and ROZRACH.DATAPLAT < current_date)
             )
         and (:oddaty is null or (rozrach.datazamk is null) or (rozrach.datazamk >= :oddaty))
         and (:dodaty is null or (rozrach.dataotw <= :dodaty))
         and ((:soddzial is null) or (:soddzial = '')or (ROZRACH.ODDZIAL = :soddzial))
         and ((:termin is null) or (ROZRACH.dataplat <= :termin))
         and (:company is null or (ROZRACH.company = :company))
     )
     group by SLODEF, SLOPOZ,WALUTA
     into :ref,  :slodef, :slopoz, :ildok,:winien, :ma, :winienzl, :mazl, :waluta , :walutowy,
      :saldo, :dataotw, :dataplat, :slodefdict,
      :company
    do begin
      execute procedure SLO_DANEKS(:slodef, :slopoz) returning_values :slokod, :slonazwa, :kont, :slokodks;
      select sprzedawca
      from SLO_DANEDOD(:slodef, :slopoz)
      into :slosprzedawca;
      ildnimax = :data - :dataplat;
      if(:saldo <> 0 and :dataplat < current_date)then kolor = 2;
      else if(:saldo <> 0 and :dataplat < current_date) then kolor = 1;
      else kolor = 0;
      suspend;
    end
  end
end^
SET TERM ; ^
