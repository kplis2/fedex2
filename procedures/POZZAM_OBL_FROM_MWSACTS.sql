--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZZAM_OBL_FROM_MWSACTS(
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      DOCID integer,
      DOCPOSID integer,
      STOCKTAKING smallint,
      REC smallint,
      QUANTITYCHANGE smallint,
      QUANTITYCCHANGE smallint)
   as
declare variable quantity numeric(14,4);
declare variable mwsactsquantity numeric(14,4);
declare variable quantityc numeric(14,4);
declare variable mwsactsquantityc numeric(14,4);
begin
  if (docposid is null) then
    exit;
  quantity = 0;
  quantityc = 0;
  mwsactsquantity = 0;
  mwsactsquantityc = 0;
  -- okreslam ilosc przychodowana na kazda lokacje paletowa zwiazana z pozycja dokumentu magazynowego
  if (doctype = 'Z') then
  begin
    select sum(quantity), sum(quantityc)
      from mwsacts
      where doctype = :doctype
        and docid = :docid and docposid = :docposid
        and ((recdoc = 0 and autogen = 1) or (recdoc = 1 and mwsconstlocp is null))
        and stocktaking = 0
        and status <> 6
      into mwsactsquantity, mwsactsquantityc;
    if (mwsactsquantity is null) then mwsactsquantity = 0;
    if (mwsactsquantityc is null) then mwsactsquantityc = 0;
    quantity = quantity + mwsactsquantity;
    quantityc = quantityc + mwsactsquantityc;
    mwsactsquantity = 0;
    update pozzam set ilosconmwsacts = :quantity, ilosconmwsactsc = :quantityc
      where ref = :docposid;
  end
end^
SET TERM ; ^
