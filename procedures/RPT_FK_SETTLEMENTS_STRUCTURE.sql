--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_SETTLEMENTS_STRUCTURE(
      PACCOUNT ACCOUNT_ID,
      P1 numeric(14,2),
      P2 numeric(14,2),
      P3 numeric(14,2),
      P4 numeric(14,2),
      DATA date,
      COMPANY integer)
  returns (
      ACCOUNT ACCOUNT_ID,
      NAME varchar(80) CHARACTER SET UTF8                           ,
      VAL1 numeric(14,2),
      VAL2 numeric(14,2),
      VAL3 numeric(14,2),
      VAL4 numeric(14,2),
      VAL5 numeric(14,2),
      VAL6 numeric(14,2),
      SUMVAL numeric(14,2))
   as
declare variable val numeric(14,2);
declare variable paydate timestamp;
declare variable dictdef integer;
declare variable dictpos integer;
begin
  if (data is null) then
  begin
    for
      select kontofk, winienzl-mazl, dataplat, slodef, slopoz
        from rozrach
        where kontofk like :paccount || '%'
          and company = :company
        order by kontofk
        into :account, :val, :paydate, :dictdef, :dictpos
    do begin
      val1 = 0;
      val2 = 0;
      val3 = 0;
      val4 = 0;
      val5 = 0;
      val6 = 0;
      sumval = 0;
      name = '';
      if (dictdef is not null and dictpos is not null) then
        execute procedure name_from_dictposref(dictdef, dictpos) returning_values name;
      if (current_date - cast(paydate as date) <=0) then
        val1 = val;
      else if (current_date - cast(paydate as date) > 0 and current_date - cast(paydate as date) <= p1) then
        val2 = val;
      else if (current_date - cast(paydate as date) > p1 and current_date - cast(paydate as date) <= p2) then
        val3 = val;
      else if (current_date - cast(paydate as date) > p2 and current_date - cast(paydate as date) <= p3) then
        val4 = val;
      else if (current_date - cast(paydate as date) > p3 and current_date - cast(paydate as date) <= p4) then
        val5 = val;
      else if (current_date - cast(paydate as date) > p4) then
        val6 = val;
      sumval = val;
      suspend;
    end
  end else begin
    for
      select p.kontofk, p.winienzl-p.mazl, dataplat, p.slodef, p.slopoz
        from rozrach r
          join rozrachp p on (p.kontofk = r.kontofk and r.slodef = p.slodef and r.slopoz = p.slopoz and r.symbfak = p.symbfak and r.company = p.company)
        where p.kontofk like :paccount || '%'
          and p.data <= cast(:data as timestamp) and r.company = :company
        order by p.kontofk
        into :account, :val, :paydate, :dictdef, :dictpos
    do begin
      if (val is null) then
        val = 0;
      val1 = 0;
      val2 = 0;
      val3 = 0;
      val4 = 0;
      val5 = 0;
      val6 = 0;
      sumval = 0;
      name = '';
      if (dictdef is not null and dictpos is not null) then
        execute procedure name_from_dictposref(dictdef, dictpos) returning_values name;
      if (cast(data as date) - cast(paydate as date) <= 0) then
        val1 = val;
      else if (cast(data as date) - cast(paydate as date) > 0 and cast(data as date) - cast(paydate as date) <= p1) then
        val2 = val;
      else if (cast(data as date) - cast(paydate as date) > p1 and cast(data as date) - cast(paydate as date) <=p2) then
        val3 = val;
      else if (cast(data as date) - cast(paydate as date) > p2 and cast(data as date) - cast(paydate as date) <=p3) then
        val4 = val;
      else if (cast(data as date) - cast(paydate as date) > p3 and cast(data as date) - cast(paydate as date) <=p4) then
        val5 = val;
      else if (cast(data as date) - cast(paydate as date) > p4) then
        val6 = val;
      sumval = val;
      suspend;
    end
  end
end^
SET TERM ; ^
