--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_GET_NAVIGATOR_EHRM(
      PERSONREF integer,
      EMPLREF integer)
  returns (
      REF varchar(255) CHARACTER SET UTF8                           ,
      PARENT varchar(255) CHARACTER SET UTF8                           ,
      PREV varchar(255) CHARACTER SET UTF8                           ,
      NUMBER smallint,
      NAME varchar(80) CHARACTER SET UTF8                           ,
      HINT varchar(255) CHARACTER SET UTF8                           ,
      KINDSTR varchar(20) CHARACTER SET UTF8                           ,
      EHRMLINK varchar(255) CHARACTER SET UTF8                           ,
      RIGHTS varchar(255) CHARACTER SET UTF8                           ,
      RIGHTSGROUP varchar(255) CHARACTER SET UTF8                           )
   as
declare variable NAMESPACE varchar(40);
declare variable ISEHRM smallint;
declare variable CURRLAYER integer;
declare variable SECTION varchar(255);
declare variable IDENT varchar(255);
declare variable VAL varchar(1024);
declare variable OLDSECTION varchar(255);
declare variable GRUPA varchar(1024);
begin
  select 'USER_'||s.userlogin
    from persons p
      left join s_users s on (p.userid = s.userid)
    where p.ref = :personref
  into :namespace;

  select ehrmgroups from employees where ref = :emplref
  into :grupa;

  if(:namespace is null or :namespace='' or :namespace = 'USER_') then namespace = 'SENTE';
  select layer from s_namespaces where namespace=:namespace into :currlayer;
  oldsection = '';
  for
    select a.section, a.ident, a.val
      from s_appini s
        left join s_namespaces n on (n.namespace=s.namespace)
        left join s_appini a on (s.section = a.section)
      where s.section starting with 'Navigator:'
        and s.ident = 'ISEHRM' and s.val = 1
        and n.layer<=:currlayer
      order by s.section, n.layer desc
    into :section, :ident, :val
  do begin
    -- jesli jest nowy item to zwroc rekord wynikowy
    if(:section<>:oldsection) then begin
      -- jesli to kolejny item to zwroc biezacy wynik
      if(:parent='') then parent = NULL;

      if(:oldsection<>'') then
      begin
        if (coalesce(:rightsgroup,'') = '') then suspend;

        if (coalesce(:rightsgroup,'') <> '' and coalesce(:grupa,'') <> '' and
          strmulticmp(:rightsgroup,:grupa) > 0) then
        begin
          suspend;
        end
      end

      oldsection = :section;
      ref = NULL; parent = NULL; prev = NULL; number = NULL; name = NULL; hint = NULL; kindstr = NULL;
      rights = NULL; rightsgroup = NULL; ehrmlink = NULL;
    end
    -- zbieraj kolejne wartosci pol
    if(:ident = 'REF' and :ref is NULL) then REF = :val;
    if(:ident = 'PARENT' and :parent is NULL) then PARENT = :val;
    if(:ident = 'PREV' and :prev is NULL) then PREV = :val;
    if(:ident = 'NUMBER' and :number is NULL and :val<>'') then NUMBER = :val;
    if(:ident = 'NAME' and :name is NULL) then NAME = :val;
    if(:ident = 'HINT' and :hint is NULL) then HINT = :val;
    if(:ident = 'KINDSTR' and :kindstr is NULL) then
      KINDSTR = case when :val = 'EMPTY' then '' else :val end;
    if(:ident = 'RIGHTS' and :rights is NULL) then RIGHTS = :val;
    if(:ident = 'RIGHTSGROUP' and :rightsgroup is NULL) then RIGHTSGROUP = :val;
    if(:ident = 'EHRMLINK' and :ehrmlink is NULL) then EHRMLINK = :val;
  end
  if(:parent='') then parent = NULL;

  if(:oldsection<>'') then
  begin
    if (coalesce(:rightsgroup,'') = '') then suspend;

    if (coalesce(:rightsgroup,'') <> '' and coalesce(:grupa,'') <> '' and
      strmulticmp(:rightsgroup,:grupa) > 0) then
    begin
      suspend;
    end
  end
end^
SET TERM ; ^
