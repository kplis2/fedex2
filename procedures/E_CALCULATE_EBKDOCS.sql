--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_CALCULATE_EBKDOCS(
      EBKDOC integer)
  returns (
      X integer)
   as
declare variable ecolumn integer;
declare variable payroll integer;
declare variable prtype varchar(10);
declare variable edecreeschema integer;
declare variable debit integer;
declare variable credit integer;
declare variable employee integer;
declare variable edecreeschemastype integer;
declare variable ratio integer;
declare variable ecost integer;
declare variable cref integer;
declare variable personnames varchar(255);
begin
 -- MW: Personel - funkcja tworzy dekrety do list plac
 -- DS: modyfikacje aby uwglednic podzial kosztow
  select edecreeschemastype
    from ebkdocs
    where ref = :ebkdoc
    into :edecreeschemastype;

  delete from ebkdocpos where ebkdoc = :ebkdoc;

  for
    select B.payroll, P.prtype, E.ref, max(C.ref), E.personnames
    from eprollbkdoc B join epayrolls P on (B.payroll = P.ref)
      join eprempl EP on (P.ref = EP.epayroll)
      join employees E on (EP.employee = E.ref)
      left join ecosts C on (E.ref = C.employee)
    where ebkdoc = :ebkdoc --and E.ref >= 500 and E.ref < 1000
    group by B.payroll, P.prtype, E.ref, E.personnames
    into :payroll, :prtype, :employee, :cref, :personnames
  do begin
    if (cref is null) then
      exception universal 'Brakuje konta ksiegowego dla: '||:personnames;
    select ref
      from edecreeschemas
      where prolltype = :prtype and edecreeschemastype = :edecreeschemastype
      into :edecreeschema;

    for
      select ecolumn, debit, credit, ratio, ecost
       from ecoltodecree
       where decreeschema = :edecreeschema --and ecolumn in (400, 491)
       order by ecolumn
       into :ecolumn, :debit, :credit, :ratio, :ecost
    do
      execute procedure e_calc_ebkdoc_position(employee, payroll, ebkdoc, ecolumn, debit, credit, ratio, ecost);
  end

  update ebkdocs set lastcalc = CURRENT_TIMESTAMP(0) where ref = :ebkdoc;
  suspend;
end^
SET TERM ; ^
