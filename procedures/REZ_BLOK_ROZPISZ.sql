--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REZ_BLOK_ROZPISZ(
      MAGAZYN char(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer)
   as
DECLARE VARIABLE REFPOZ INTEGER;
DECLARE VARIABLE CENA NUMERIC(14,4);
DECLARE VARIABLE DOSTAWA INTEGER;
DECLARE VARIABLE ILOSC NUMERIC(14,4);
DECLARE VARIABLE FREEIL NUMERIC(14,4);
DECLARE VARIABLE ILROZ NUMERIC(14,4);
DECLARE VARIABLE CNT INTEGER;
begin
  /*dla kazdej rezerwacji do zablokowania na danej kartotece wedlug daty realizacji blokady*/
  for select POZZAM, ILOSC,CENA, DOSTAWA
  from STANYREZ where MAGAZYN=:MAGAZYN and KTM=:KTM and WERSJA=:wersja
   and STATUS='Z' and zreal=0
   order by DOSTAWA, PRIORYTET, DATABL
   into :refpoz, :ilosc, :cena, :dostawa
 do begin
   execute procedure REZ_GET_FREE_STAN(:magazyn, :ktm, :wersja, :cena,:dostawa) returning_values :freeil;
   if(:freeil > :ilosc) then ilroz = :ilosc;
   else ilroz = :freeil;
   if(:ilroz > 0) then begin
      /*naożenie blokady*/
      cnt = null;
      select count(*) from STANYREZ where POZZAM=:refpoz AND STATUS='B' and zreal = 0 into :cnt;
      if(:cnt is null) then cnt = 0;
      if(:cnt > 0) then
        update STANYREZ set ILOSC = ilosc + :ilroz where POZZAM=:refpoz AND STATUS='B' and zreal = 0;
      else
        insert into STANYREZ(POZZAM,STATUS,ZREAL,DOKUMMAG,MAGAZYN,KTM,WERSJA,ZAMOWIENIE,DATA,ILOSC,CENA,DOSTAWA,DATABL,PRIORYTET)
           select POZZAM,'B',0,NULL,MAGAZYN,KTM,WERSJA,ZAMOWIENIE,DATA,:ilroz,CENA,DOSTAWA,DATABL,PRIORYTET from STANYREZ
            where POZZAM=:refpoz AND STATUS = 'Z' AND ZREAL = 0;
      /*zdjecie do blokady*/
      if(:ilroz = :ilosc) then
        delete from STANYREZ where POZZAM=:refpoz AND STATUS='Z' and zreal = 0;
      else
        update STANYREZ set ilosc = ilosc - :ilroz where POZZAM=:refpoz AND STATUS='Z' and zreal = 0;
   end
 end
end^
SET TERM ; ^
