--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_PAYROLLPERIODS(
      TYP varchar(4) CHARACTER SET UTF8                           ,
      COMPANY integer)
  returns (
      PERIOD varchar(6) CHARACTER SET UTF8                           )
   as
begin
/* PL: Personel - Procedura zwraca unikalne okresy PERIOD o zadanym TYP'ie (cper/iper/tper)
                  z tabeli EPAYROLLS dla zadanego zakladu COMPANY (BS57240)*/

  typ = coalesce(upper(typ), 'CPER');
  for
    select distinct case :typ
             when 'CPER' then cper
             when 'IPER' then iper
             when 'TPER' then tper end
      from epayrolls
      where company = :company
      into :period
  do
    suspend;

end^
SET TERM ; ^
