--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE Z06_FIRED(
      YEARID integer,
      REASONTYPE integer,
      ONLY_WOMAN smallint,
      COMPANY integer)
  returns (
      AMOUNT numeric(14,2))
   as
declare variable yearid_firstday date;
  declare variable yearid_lastday date;
begin
/* Funkcja wykorzystywana w sprawozdaniu GUS Z-06, analizuje liczbe osob
   zwolnionych w danym roku YEARID wg zadanych warunkow*/

  yearid_firstday = yearid || '/1/1';
  yearid_lastday = yearid || '/12/31';
  if (only_woman = 0) then only_woman = null;

  select count(*) from (
    select m.person
      from emplcontracts E
        join employees m on m.ref = e.employee
        join persons p on p.ref = m.person
        join eterminations t on t.ref = e.etermination
      where E.enddate >= :yearid_firstday - 1 and E.enddate < :yearid_lastday --BS35940
        and (t.reasontype = :reasontype or :reasontype is null)
        and E.company = :company
        and E.workdim >= 1
        and (not exists(select ref from employment EM
                          where EM.employee = m.ref
                            and EM.fromdate = e.todate + 1))
        and m.person not in (select person from get_persons_wych_or_bezpl(:yearid_firstday,:yearid_lastday,:company,1))
        and (P.sex = 0 or :only_woman is null)
      group by m.person, e.enddate)
    into :amount;
  suspend;
end^
SET TERM ; ^
