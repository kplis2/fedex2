--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_STANQ(
      MAGAZYN varchar(255) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      DATA timestamp,
      DOSTAWA integer)
  returns (
      STAN numeric(14,4),
      WARTOSC numeric(14,2))
   as
declare variable DATAF TIMESTAMP;
begin
  stan = NULL;
  wartosc = NULL;
  select sum(STAN), sum(WARTOSC)
    from browse_stanyarch(:ktm, :wersja, :dostawa, :magazyn, :data)
    into :stan, :wartosc;
  if (stan is null) then stan = 0;
  if (wartosc is null) then wartosc = 0;
end^
SET TERM ; ^
