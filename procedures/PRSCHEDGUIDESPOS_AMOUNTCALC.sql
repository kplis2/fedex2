--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDESPOS_AMOUNTCALC(
      PRPOZZAM integer,
      PRSCHEDGUIDESPOS integer)
   as
declare variable pozzamamount numeric(14,4);
declare variable prschedzam integer;
declare variable prgposamount numeric(14,4);
declare variable amountdiff numeric(14,4);
declare variable prgposcurref integer;
declare variable amountonprgpos numeric(14,4);
declare variable nagzamsymbol varchar(40);
begin
  prschedzam = null;
  if(prschedguidespos is not null) then begin
    select pp.pozzamref, p.prschedzam
      from prschedguidespos pp
      left join prschedguides p on (pp.prschedguide = p.ref)
      where pp.ref = :prschedguidespos
    into :prpozzam, :prschedzam;
  end else if (prpozzam is not null) then
    select first 1 g.prschedzam
      from pozzam p
        left join prschedguidespos pos on (p.ref = pos.pozzamref)
        left join prschedguides g on (g.ref = pos.prschedguide)
      where p.ref = :prpozzam
      into :prschedzam;
  select ilosc from pozzam where ref = :prpozzam into :pozzamamount;
  for
    select psz.ref, n.id
      from prschedzam psz
        left join nagzam n on (n.ref = psz.zamowienie)
      where (/*:prschedzam is null and exists
        (select pp.ref
          from prschedguidespos pp
          left join prschedguides p on (p.ref = pp.prschedguide)
         where pp.pozzamref = :prpozzam and psz.ref = p.prschedzam
        )
        or */psz.ref = :prschedzam)
    into :prschedzam, :nagzamsymbol
  do begin
    select sum(pp.amount)
    from prschedguidespos pp
    left join prschedguides p on (p.ref = pp.prschedguide)
    where p.prschedzam = :prschedzam and pp.pozzamref = :prpozzam
    into :prgposamount;
    amountdiff = pozzamamount - prgposamount;
    update prschedguidespos pp set pp.pggamountcalc = 1 where pp.pozzamref = :prpozzam
      and (select p.prschedzam from prschedguides p where p.ref = pp.prschedguide) = :prschedzam;
    if(amountdiff < 0) then begin
      amountdiff = - amountdiff;
 --obniżka pozycji przewodników
      while(amountdiff >= 1) do begin
        prgposcurref = null;
        select first 1 pp.ref, pp.amount - pp.amountzreal
          from prschedguidespos pp
          left join prschedguides p on (p.ref = pp.prschedguide)
          where p.prschedzam = :prschedzam and pp.pozzamref = :prpozzam
            and pp.amount - pp.amountzreal > 0 and p.statusmat < 3
          order by p.status, p.starttime nulls first, pp.amount - pp.amountzreal desc
        into :prgposcurref, :amountonprgpos;
        if(prgposcurref is null) then exception prschedguides_error substring('Nie można zmniejszyć o '||:amountdiff||' pozycji przew. do zlec. '||:nagzamsymbol from 1 for 70);
        if(:amountdiff > amountonprgpos) then begin
            amountdiff = amountdiff - amountonprgpos;
        end else begin
          amountonprgpos = :amountdiff;
          amountdiff = 0;
        end
        update prschedguidespos set amount = amount - :amountonprgpos where ref = :prgposcurref;
      end
    end else if(amountdiff > 0) then begin
 --podbicie pozycji przewodników
     prgposcurref = 0;
      while(amountdiff > 0 and prgposcurref is not null) do begin
        prgposcurref = null;
        select first 1 pp.ref, p.amount * pp.kamountshortage * pp.kamountnorm - pp.amountzreal
          from prschedguidespos pp
          left join prschedguides p on (p.ref = pp.prschedguide)
          where p.prschedzam = :prschedzam and pp.pozzamref = :prpozzam
            and pp.amount < p.amount * pp.kamountshortage * pp.kamountnorm and p.statusmat < 3
          order by p.status desc, p.starttime desc, pp.amount - pp.amountzreal asc
        into :prgposcurref, :amountonprgpos;
        if(:prgposcurref is not null) then begin
          if(:amountdiff > amountonprgpos) then begin
              amountdiff = amountdiff - amountonprgpos;
          end else begin
            amountonprgpos = :amountdiff;
            amountdiff = 0;
          end
          update prschedguidespos set amount = amount + :amountonprgpos where ref = :prgposcurref;
        end
      end
    end
    update prschedguidespos pp set pp.pggamountcalc = 0 where pp.pozzamref = :prpozzam
      and (select p.prschedzam from prschedguides p where p.ref = pp.prschedguide) = :prschedzam;
  end
end^
SET TERM ; ^
