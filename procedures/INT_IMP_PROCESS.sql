--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_PROCESS(
      SESJAREF SESJE_ID,
      TABELA TABLE_ID,
      REF INTEGER_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable stabela table_id;
declare variable sprocedura string255;
declare variable szrodlo zrodla_id;
declare variable skierunek smallint_id;
declare variable sesja sesje_id;
declare variable sql memo;
declare variable tmpstatus smallint_id;
declare variable tmpmsg string255;
begin
 -- exception universal coalesce(sesjaref, 'BRAK');
  tmpstatus = 1;
  tmpmsg = '';
  status = 1;
  msg = '';
  if (ref = 0) then ref = null;

  if (coalesce(sesjaref,0) > 0  --po numerze sesji
      and coalesce(tabela,'') = '' and coalesce(ref,0) = 0
     ) then
    begin

      select s.tabela, s.procedura, s.zrodlo, s.kierunek
        from int_sesje s
        where ref = :sesjaref
        into :stabela, :sprocedura, :szrodlo, :skierunek;

      sprocedura = ''; --tymczasowo!!!!!!!!!!!!!!!!!!!!

      if (coalesce(trim(sprocedura),'') = '') then
        begin
          if (coalesce(trim(stabela),'') = '') then
            exception universal 'Tabela nie może być pusta!';

          select first 1 procedura from int_procedury p
            where p.tabela = :stabela
              and p.kierunek = :skierunek
              and p.zrodlo = :szrodlo     -- Ldz to jest ref z tabeli int_zrodla..., sprawdzapo tym czy w tabeli int_procedury sa uprawnienia ?
            into :sprocedura;

          if (coalesce(trim(stabela),'') = '') then
            select first 1 procedura from int_procedury p
              where p.tabela = :stabela
                and p.kierunek = :skierunek
                and p.zrodlo is null
              into :sprocedura;
        end

      if (coalesce(trim(sprocedura),'') = '')  then
        exception universal 'Nie znaleziono procedury!';

      sql = 'select status, msg from  '||:sprocedura||' ('||coalesce(:sesjaref, 'null')
        ||', '||coalesce(''''||:stabela||'''', 'null')||', '||coalesce(:ref, 'null')||', 0, 0, 0)';
      execute statement sql into :tmpstatus, :tmpmsg;

      if(:tmpstatus is distinct from 1)then
      begin
        status = -1;
        msg = substring((msg || ' ' ||coalesce(:tmpmsg,'')) from 1 for 255);
      end

      select status, msg from int_sesje_aktualizuj(:sesjaref, 1) into :tmpstatus, :tmpmsg;

      if(:tmpstatus is distinct from 1)then
      begin
        status = -1;
        msg = substring((msg || ' ' ||coalesce(:tmpmsg,'')) from 1 for 255);
      end

    end
  else if (coalesce(sesjaref,0) > 0  --po numerze sesji i tabeli
      and coalesce(tabela,'') <> '' and coalesce(ref,0) = 0
     ) then
    begin
      exception universal 'Przypadek niebsłużony: numer sesji + tabela';
    end
  else if (coalesce(tabela,'') <> '' and coalesce(ref,0) > 0 --po tabeli i rekordzie
     ) then
    begin
      exception universal 'Przypadek niebsłużony: tabela + rekord';
    end
  else
    exception universal 'Przypadek niebsłużony.';

  suspend;
end^
SET TERM ; ^
