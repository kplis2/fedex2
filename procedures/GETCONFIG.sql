--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GETCONFIG(
      AKRONIM varchar(255) CHARACTER SET UTF8                           ,
      FROMDATE varchar(20) CHARACTER SET UTF8                            default null)
  returns (
      WARTOSC varchar(1024) CHARACTER SET UTF8                           )
   as
begin
  execute procedure get_config(:akronim,0,:fromdate) returning_values :wartosc;
  suspend;
end^
SET TERM ; ^
