--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRDSCR_FK_FUN_MAG_STANY(
      KEY1 integer,
      KEY2 integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      TIMEELEM integer,
      ELEMENT integer,
      ELEMHIER varchar(40) CHARACTER SET UTF8                           ,
      KTMLEVEL integer)
  returns (
      RET numeric(14,2))
   as
declare variable winien numeric(14,2);
  declare variable ma numeric(14,2);
  declare variable saldo numeric(14,2);
  declare variable oddzial varchar(10);
  declare variable oddzialsymbol varchar(10);
  declare variable period char(6);
  declare variable iyear integer;
  declare variable imonth smallint;
  declare variable todate date;
  declare variable ktm varchar(2);
  declare variable cut integer;
  declare variable elem_act varchar(5);
  declare variable wartosc numeric(14,2);
  declare variable wartmin numeric(14,2);
begin
  wartosc = 0;
  wartmin = 0;
  ret = 0;
  if (:TIMEELEM is null) then
    select frdcells.timelem
      from frdcells
      where frdcells.ref = :key1
      into :timeelem;
  if (:element is null) then
    select frdcells.dimelem
      from frdcells
      where frdcells.ref = :key1
      into :element;

  select first 1 shortname
    from frdimelems
    where dimelem = :element
    into :oddzialsymbol;

  select oddzial
    from oddzialy
    where symbol = :oddzialsymbol
    into oddzial;

  select first 1 shortname
    from frdimelems
    where dimelem = :timeelem
    into :period;

  iyear = cast(substring(period from 1 for 4) as integer);
  imonth = cast(substring(period from 5 for 2) as smallint);
  if (imonth = 12) then
     todate = cast((iyear + 1) || '/1/1' as date) - 1;
  else
     todate = cast(iyear || '/' || (imonth+1) || '/1' as date) - 1;

  if (:elemhier is null) then
    select frdcells.elemhier
      from frdcells
      where frdcells.ref = :key1
      into :elemhier;

  if (ktmlevel is not null) then
    while (ktmlevel > 0)
    do begin
      ktmlevel = ktmlevel - 1;
      cut = position(',' in :elemhier);
      elem_act = substring(:elemhier from 1 for :cut);
      elemhier = substring(:elemhier from :cut + 1 for coalesce(char_length(:elemhier) , 0)); -- [DG] XXX ZG119346
    end

  select shortname
    from frdimelems
    where dimelem = :elem_act
    into :ktm;

  for
    select sum(DOKUMROZ.WARTOSC), coalesce(sum(DOKUMROZ.WARTOSC * DEFDOKUM.WYDANIA),0)
    from DOKUMROZ join DOKUMPOZ on( DOKUMROZ.pozycja = DOKUMPOZ.ref)
      join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.dokument)
      join DEFDOKUM on ( DOKUMNAG.typ = DEFDOKUM.symbol)
    where ((:oddzial is null and DOKUMNAG.ODDZIAL <> 'POZABILANS') or DOKUMNAG.ODDZIAL = :oddzial) and (:ktm is null or DOKUMPOZ.KTM starting with :KTM or DOKUMPOZ.KTM is null)
--    where (DOKUMNAG.ODDZIAL in ('LEGNICA','BOLESLAWIE', 'GLOGOW', 'LEGNICA', 'LWOWEK', ')) and (:ktm is null or DOKUMPOZ.KTM starting with :KTM or DOKUMPOZ.KTM is null)
      and (DOKUMNAG.AKCEPT = 1 or DOKUMNAG.akcept = 8) AND DOKUMNAG.data <= :todate
--    group by DOKUMPOZ.KTM, DOKUMPOZ.WERSJA, DOKUMROZ.CENA, DOKUMROZ.DOSTAWA
    into :wartosc, :wartmin
  do begin
    if(:wartosc is null) then wartosc = 0;
    if(:wartmin is null) then wartmin = 0;
    ret = ret + :wartosc - 2 * :wartmin;
  end
suspend;
end^
SET TERM ; ^
