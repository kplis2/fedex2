--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDEL_COUNT_ALL(
      DELEGATION integer)
   as
declare variable daycnt integer;
declare variable edelday integer;
declare variable incountry smallint;
declare variable currency smallint;
declare variable country varchar(20);
declare variable dlumpsum numeric(14,2);
declare variable accomsum numeric(14,2);
declare variable allowsum numeric(14,2);
declare variable dlumpsumc numeric(14,2);
declare variable accomsumc numeric(14,2);
declare variable allowsumc numeric(14,2);
declare variable dlump numeric(14,2);
declare variable accom numeric(14,2);
declare variable allow numeric(14,2);
declare variable drivesum numeric(14,2);
declare variable exchangerate numeric(14,2);
declare variable dayincountry numeric(14,2);
declare variable dayallow numeric(14,2);
declare variable dayallowcurr numeric(14,2);
declare variable advancescountryzl numeric(14,2);
declare variable advanceget numeric(14,2);
declare variable fromdate date;
declare variable calculation numeric(14,2);
declare variable calculationzl numeric(14,2);
declare variable accommbill numeric(14,2);
declare variable othercosts numeric(14,2);
declare variable accomscountryzl numeric(14,2);
declare variable accomscurr numeric(14,2);
declare variable rating numeric(14,2);
declare variable othercostbillcurr numeric(14,2);
declare variable accommbillcurr numeric(14,2);
begin

  dlumpsum = 0;
  accomsum = 0;
  allowsum = 0;
  dlumpsumc = 0;
  accomsumc = 0;
  allowsumc = 0;
  calculation = 0;
  accommbill = 0;
  othercosts = 0;
  accomscountryzl = 0;
  accomscurr = 0;
  rating = 0;
  dayallow = 0;
  accommbill = 0;
  advanceget  =0;
  drivesum = 0;
  dayallow = 0;
  OTHERCOSTBILLCURR = 0;
  ACCOMMBILLCURR = 0;
  advancescountryzl = 0;
  dlumpsumc = 0;

  select (1 - typ), rating, dayallow,
      dayallowcurr, currency, country, advanceget, fromdate
    from edelegations where ref = :delegation
    into :incountry, :rating, :dayallow,
      :dayallowcurr, :currency, :country, :advanceget, :fromdate;

  -- zliczanie po dniach
  select count(daycounter) from edeldays where delegation = :delegation
    into daycnt;

  for
    select ref
      from edeldays where delegation = :delegation
      into :edelday
  do begin
    execute procedure edel_countday(edelday, daycnt, incountry)
      returning_values dlump, accom, allow, dayincountry;

    if (dayincountry = 1) then begin
      dlumpsum = dlumpsum + dlump;
      accomsum = accomsum + accom;
      allowsum = allowsum + allow;
    end else
    begin
      dlumpsumc = dlumpsumc + dlump;
      accomsumc = accomsumc + accom;
      allowsumc = allowsumc + allow;
    end
  end

  -- zliczanie po przejazdach
  select sum(e.costs)
    from edelpos e
    where e.delegation = :delegation
    into drivesum;

  -- zliczanie w walutach
  if (currency = 1) then
  begin
    advanceget = 0;
    select sum(exchangerate * allowincurr)
      from edeladvances
      where delegation = delegation and currency <> 'PLN'
      into :advancescountryzl;

    select sum(:rating * allowincurr)
      from edeladvances
      where delegation = delegation and currency = 'PLN'
      into :advanceget;
  end


  select first 1 allowincurrency
    from edelcountryallow
    where country = 'PL' and startdate <= :fromdate
    order by startdate desc
    into :dayallow;

  select first 1 allowincurrency
    from edelcountryallow
    where country = :country --(select c.currency from countries c where c.symbol = :country)
      and startdate <= :fromdate
    order by startdate desc
    into :dayallowcurr;

  if (rating is null or rating = 0) then rating = 1;
  if (dayallow = 0) then dayallow =1;
  --dodanie
  update edelegations set
    drivelump = :dlumpsum * :dayallow / 100, --ryczalt za dojazdy
    drivecost = :drivesum,--razem dojazdy przejazdy
    allowances = :allowsum * :dayallow / 100,--diety
    accomms = :accomsum * :dayallow / 100 ,--noclegi ryczalt
    drivelumpcurr = :dlumpsumc * :dayallowcurr / 100,  --zagr ryczalt za dojazdy
    allowancescurr = :allowsumc * :dayallowcurr / 100, -- zagr diety
    accomscurr = :accomsumc * :dayallowcurr/ 100, -- zagr noclegi ryczalt
    drivelumpcountryzl = :dlumpsumc * :dayallowcurr * rating / 100, --zagr ryczalt za dojazdy suma
    allowancescountryzl = :allowsumc * :dayallowcurr * rating / 100,      -- zagr diety suma
    accomscountryzl = :accomsumc * :dayallowcurr * rating / 100,          -- zagr noclegi ryczalt
    advancescountryzl = :advancescountryzl,
    advanceget = :advanceget,
    dayallow=:dayallow,
    calculationzl =  (:allowsum * :dayallow / 100 +  (:accomsum * :dayallow / 100) + :drivesum) - (:advanceget - accommbill - othercosts) ,  --ogolem
    calculation = ((:allowsum * :dayallow / 100 +  (:accomsum * :dayallow / 100) + :drivesum) - (coalesce(:advanceget,0) - coalesce(accommbill,0) - coalesce(othercosts,0))) +  --do wyplaty  czesc polska    */
                  ((:accomsumc * :dayallowcurr * rating / 100) + (:allowsumc * :dayallowcurr * rating / 100) + (:dlumpsumc * :dayallowcurr * rating / 100))
                   - ( :advancescountryzl - coalesce(OTHERCOSTBILLCURR,0)  - coalesce(ACCOMMBILLCURR,0)) --do wyplaty  czesc zagr


  where ref = :delegation;

  --naliczenie kosztów
  --execute procedure edelcalculation(:delegation);

end^
SET TERM ; ^
