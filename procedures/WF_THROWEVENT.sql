--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WF_THROWEVENT(
      SYMBOL STRING40,
      OBJECTNAME STRING40,
      KEY STRING40 = null,
      DATA STRING255 = null,
      REMOVE smallint = 0,
      PING smallint = 0)
   as
begin
  if(coalesce(ping,0) = 0) then
  begin
    remove = coalesce(remove,0);

    insert into wfevents (
      objectname,
      symbol,
      data,
      "KEY",
      REMOVE)
    values (
      :OBJECTNAME,
      :SYMBOL,
      :data,
      :"KEY",
      :REMOVE);
  end

  post_event 'NEOS.WORKFLOW.EVENT';
end^
SET TERM ; ^
