--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_OBLILREALONZAM(
      DOKMAG integer)
   as
declare variable pozzamref integer;
declare variable iloscm numeric(14,4);
declare variable ilosc numeric(14,4);
declare variable ilosco numeric(14,4);
declare variable jednm integer;
declare variable jedn integer;
declare variable jedno integer;
declare variable przelicz numeric(14,4);
begin
  for select DOKUMPOZ.wartrealpoz,min(POZZAM.jedno), min(pozzam.jedn), min(TOWARY.dm),
          sum(DOKUMPOZ.ilosc)
  from DOKUMPOZ
   left join POZZAM on (POZZAM.REF = DOKUMPOZ.wartrealpoz)
   left join TOWARY on (POZZAM.KTM = TOWARY.KTM)
   left join NAGZAM on (POZZAM.zamowienie = NAGZAM.ref)
  where DOKUMPOZ.DOKUMENT = :dokmag and DOKUMPOZ.wartrealpoz is not null
   and POZZAM.REF is not null
--   and NAGZAM.typ > 1
  group by DOKUMPOZ.wartrealpoz
  into :pozzamref, :jedno, :jedn, :jednm, :iloscm
  do begin
    if(:jedn is null) then jedn = :jednm;
    if(:jedno is null) then jedno = :jednm;
    if(:jedn <> :jednm) then begin
      przelicz = null;
      select PRZELICZ from TOWJEDN where REF=:jedn into :przelicz;
      if(:przelicz is null or :przelicz = 0) then przelicz = 1;
      ilosc = :iloscm / :przelicz;
    end else
     ilosc = :iloscm;
    if(:jedno <> :jednm) then begin
      przelicz = null;
      select PRZELICZ from TOWJEDN where REF=:jedno into :przelicz;
      if(:przelicz is null or :przelicz = 0) then przelicz = 1;
      ilosco = :iloscm / :przelicz;
    end else
      ilosco = :iloscm;
    update POZZAM set ilreal = :ilosc, ilrealm = :iloscm, ilrealo = :ilosco
    where REF = :pozzamref and (ILREALM <> :iloscm);
    execute procedure POZZAM_OBL(:pozzamref);
  end
end^
SET TERM ; ^
