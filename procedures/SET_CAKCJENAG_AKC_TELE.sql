--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SET_CAKCJENAG_AKC_TELE(
      CAKCJA integer)
   as
  declare variable lpozycji Integer;
  declare variable lpozycjiwyk integer;
  declare variable procentrealizacji numeric(14,2);

begin
  /*Obliczanie wszystkich ilosci pozycji*/
  select count(*) from cakcjepoz
      where cakcjepoz.cakcja = :cakcja into :lpozycji;

  /*Obliczanie ilosci pozycji aktywnych*/
  if(:lpozycji > 0) then begin
    select count(*) from cakcjepoz
      where cakcjepoz.cakcja = :cakcja
        and cakcjepoz.wykonano = 1 into :lpozycjiwyk;
    
    procentrealizacji = cast(:lpozycjiwyk as numeric(14,2)) / cast(:lpozycji as numeric(14,2))*100;
  end
  else begin
    lpozycjiwyk = 0;
    procentrealizacji = 0;
  end

  /*Update pol iloscpoz, iloscpozwyk, procent*/
  update cakcjenag
    set cakcjenag.iloscpozwyk = :lpozycjiwyk,
        cakcjenag.iloscpoz = :lpozycji,
        cakcjenag.procentreal = :procentrealizacji
      where cakcjenag.ref = :cakcja;
end^
SET TERM ; ^
