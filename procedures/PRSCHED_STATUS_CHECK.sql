--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_STATUS_CHECK(
      SCHEDULE integer,
      SCHEDZAM integer,
      GUIDE integer,
      SCHEDOPER integer,
      ZAKRES smallint,
      MATREADY smallint,
      TIMECONFLICT smallint,
      STATUS smallint)
  returns (
      CNT integer)
   as
declare variable doloop smallint;
begin
  --zaznaczanie do przeliczenia
  execute procedure prsched_calc_prepare(:schedule, schedzam, :guide, :schedoper, :zakres, 0, 0) returning_values :cnt;
  --odtwarzanie statusow materialowych
  if(:matready = 1) then begin
    doloop = 1;
    update PRSCHEDOPERS set MATERIALREADYTIME = null, STATUSMAT = null where VERIFIED = 0;
    while(:doloop = 1) do begin
      -- dla wszystkich operacji niezweryfikowanych, ktore nie posiadaja niezweryfikowanych operacji porzedzajacych, okresl czas dostepnosci materialow
      doloop = 0;
     for select a.ref
       from PRSCHEDOPERS A
         left join prschedoperdeps on (prschedoperdeps.depto = A.ref)
         left join prschedopers B on (prschedoperdeps.depfrom = B.REF and B.verified = 0)
       where A.VERIFIED = 0
        and a.statusmat is null
        and B.ref is null--operacje, ktore nie maja niezweryfikowanych poprzednikow
      into :schedoper
     do begin
       execute procedure PRSCHED_OPER_CHECKMATERIALSTATE(:schedoper);
       doloop = 1;
     end
    end
    for select distinct prschedopers.guide
      from prschedopers where verified = 0
      into :guide
    do begin
      execute procedure PRSCHEDGUIDES_STATUSMATCHECK(:guide);
    end
  end
  update PRSCHEDOPERS set VERIFIED = 1 where verified = 0;
end^
SET TERM ; ^
