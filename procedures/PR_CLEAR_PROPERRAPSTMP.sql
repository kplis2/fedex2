--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_CLEAR_PROPERRAPSTMP(
      PRDEPART varchar(20) CHARACTER SET UTF8                           ,
      EMPLOYEE integer,
      PRSCHEDOPER integer,
      PRSCHEDGUIDE integer,
      PRNAGZAM integer,
      PRPOZZAM integer,
      PROPER varchar(20) CHARACTER SET UTF8                           ,
      PRSHORTAGETYPE varchar(20) CHARACTER SET UTF8                           )
   as
declare variable employeetmp integer;
declare variable prschedopertmp integer;
declare variable amounttmp numeric(14,4);
declare variable prshortagetypetmp varchar(20);
declare variable maketimefromtmp timestamp;
declare variable maketimetotmp timestamp;
declare variable regtimetmp timestamp;
declare variable ktmtmp varchar(40);
declare variable prschedguidetmp integer;
declare variable wersjareftmp integer;
declare variable prnagzamtmp integer;
declare variable prpozzamtmp integer;
declare variable tref integer;
begin
  for
    select t.ref
      from propersrapstmp t
      where t.prdepart = :prdepart
        and (t.prschedoper = :prschedoper or :prschedoper is null or :prschedoper = 0)
        and (t.proper = :proper or :proper is null or :proper = '')
        and (t.prschedguide = :prschedguide or :prschedguide is null or :prschedguide = 0)
        and (t.prnagzam = :prnagzam or :prnagzam is null or :prnagzam = 0)
        and (t.prpozzam = :prpozzam or :prpozzam is null or :prpozzam = 0)
        and (t.employee = :employee or :employee is null or :employee = 0)
        and t.connection = current_connection
      into tref
  do begin
    delete from propersrapstmp where ref = :tref;
  end
end^
SET TERM ; ^
