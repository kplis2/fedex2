--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_EDEFPCARDS(
      EMPLS varchar(1024) CHARACTER SET UTF8                           ,
      FROM_PER varchar(6) CHARACTER SET UTF8                           ,
      TO_PER varchar(6) CHARACTER SET UTF8                           ,
      ISNAGLOWEK smallint,
      NR_RAPORT integer,
      PAYROLLTYPES varchar(100) CHARACTER SET UTF8                           ,
      EMPLGR integer)
  returns (
      NAZWA varchar(100) CHARACTER SET UTF8                           ,
      M1 numeric(14,2),
      M2 numeric(14,2),
      M3 numeric(14,2),
      M4 numeric(14,2),
      M5 numeric(14,2),
      M6 numeric(14,2),
      M7 numeric(14,2),
      M8 numeric(14,2),
      M9 numeric(14,2),
      M10 numeric(14,2),
      M11 numeric(14,2),
      M12 numeric(14,2),
      RAZEM numeric(14,2),
      PER01 varchar(6) CHARACTER SET UTF8                           ,
      PER02 varchar(6) CHARACTER SET UTF8                           ,
      PER03 varchar(6) CHARACTER SET UTF8                           ,
      PER04 varchar(6) CHARACTER SET UTF8                           ,
      PER05 varchar(6) CHARACTER SET UTF8                           ,
      PER06 varchar(6) CHARACTER SET UTF8                           ,
      PER07 varchar(6) CHARACTER SET UTF8                           ,
      PER08 varchar(6) CHARACTER SET UTF8                           ,
      PER09 varchar(6) CHARACTER SET UTF8                           ,
      PER10 varchar(6) CHARACTER SET UTF8                           ,
      PER11 varchar(6) CHARACTER SET UTF8                           ,
      PER12 varchar(6) CHARACTER SET UTF8                           ,
      CZY_PRZERWA smallint,
      CZY_PODSUMOWANIE smallint,
      CZY_DODATKOWEINFO smallint,
      CZY_SUMA_GRUP smallint,
      STOPKA varchar(200) CHARACTER SET UTF8                           )
   as
declare variable PERTYPE smallint;
declare variable REF_GRUPY integer;
declare variable NAZWA_GRUPY varchar(22);
declare variable CZY_SUMOWAC smallint;
declare variable SKLADNIK integer;
declare variable NR_SKLADNIKA integer;
declare variable SUM_M1 numeric(14,2);
declare variable SUM_M2 numeric(14,2);
declare variable SUM_M3 numeric(14,2);
declare variable SUM_M4 numeric(14,2);
declare variable SUM_M5 numeric(14,2);
declare variable SUM_M6 numeric(14,2);
declare variable SUM_M7 numeric(14,2);
declare variable SUM_M8 numeric(14,2);
declare variable SUM_M9 numeric(14,2);
declare variable SUM_M10 numeric(14,2);
declare variable SUM_M11 numeric(14,2);
declare variable SUM_M12 numeric(14,2);
declare variable SUM_RAZEM numeric(14,2);
declare variable PRZERWA_TMP smallint;
declare variable PER_TMP varchar(6);
declare variable I smallint;
declare variable DEFNAME varchar(30);
declare variable PRTYPE varchar(10);
declare variable "SIGN" smallint;
declare variable EMPLGR_NAME varchar(80);
begin
/*MWr: Procedura uzywana przy generowaniu kartotek definiowalnych. Wylicza
  wartosci wg zadanego schematu dla okresow od FROM_PER do TO_PER przy zalozeniu,
  ze maksymalna liczba okresow to 12. Obsluga multiselect'u dla pracownika EMPLS
  oraz typu list plac PAYROLLTYPES (PR31747) */

  isnaglowek = coalesce(isnaglowek, 0);
  empls = coalesce(empls,'');
  payrolltypes = coalesce(payrolltypes,'');
  if (empls <> '' and position(empls in '"') = 0) then empls = '"' || empls || '"';
  if (payrolltypes <> '' and position(payrolltypes in '"') = 0) then payrolltypes = '"' || payrolltypes || '"';

  --Uzupelninie zmiennych okresowych
  i = 1;
  PER_tmp =  from_per;
  while (i <= 12 and PER_tmp <= to_per) do
  begin
    if (i = 1) then PER01 = PER_tmp;
    else if (i = 2) then  PER02 = PER_tmp;
    else if (i = 3) then  PER03 = PER_tmp;
    else if (i = 4) then  PER04 = PER_tmp;
    else if (i = 5) then  PER05 = PER_tmp;
    else if (i = 6) then  PER06 = PER_tmp;
    else if (i = 7) then  PER07 = PER_tmp;
    else if (i = 8) then  PER08 = PER_tmp;
    else if (i = 9) then  PER09 = PER_tmp;
    else if (i = 10) then  PER10 = PER_tmp;
    else if (i = 11) then  PER11 = PER_tmp;
    else if (i = 12) then  PER12 = PER_tmp;

    execute procedure e_func_periodinc(:PER_tmp,1)
      returning_values :PER_tmp;
    i = i + 1;
  end

  --Pobranie podstawowych informacji o wydruku
  select pertype, rptname, emplinfo from edefpcards
    where ref = :nr_raport
    into :pertype, :nazwa, :czy_dodatkoweinfo;

  if (isNaglowek = 0) then
  begin
    for
      select ref, grname, issummary, isvoid, isgroupsum
        from edefpcardsgroup
        where defpcard = :nr_raport
        order by grnumber
        into :ref_grupy, :nazwa_grupy, :czy_sumowac, :przerwa_tmp, :czy_suma_grup
    do begin
      nazwa_grupy = trim(coalesce(nazwa_grupy,''));
      czy_przerwa = 0;
      czy_podsumowanie = 0;
      Sum_M1 = 0;
      Sum_M2 = 0;
      Sum_M3 = 0;
      Sum_M4 = 0;
      Sum_M5 = 0;
      Sum_M6 = 0;
      Sum_M7 = 0;
      Sum_M8 = 0;
      Sum_M9 = 0;
      Sum_M10 = 0;
      Sum_M11 = 0;
      Sum_M12 = 0;
      Sum_Razem = 0;
      i = 0;
      for
        select ecolumn, case when p.sign <> s.sign then -1 else 1 end from edefpcardsgrpos p
            join rpt_edefpcards_get_subgroups(:ref_grupy) s on (s.ref = defpcardgr)
          where ecolumn is not null
          order by ecolpos, ecolumn
          into :skladnik, :sign
      do begin
        for
          select EC.number, case when :sign < 0 then EC.name || ' (-)' else EC.name end
            , sum(case when ((PR.cper = :per01 and :PERTYPE = 0) or (PR.tper = :per01 and :PERTYPE = 1) or (PR.iper = :per01 and :PERTYPE = 2)) then :sign * EP.pvalue else case when :per01>=:from_per and :per01<=:to_per then 0 end end) as m1
            , sum(case when ((PR.cper = :per02 and :PERTYPE = 0) or (PR.tper = :per02 and :PERTYPE = 1) or (PR.iper = :per02 and :PERTYPE = 2)) then :sign * EP.pvalue else case when :per02>=:from_per and :per02<=:to_per then 0 end end) as m2
            , sum(case when ((PR.cper = :per03 and :PERTYPE = 0) or (PR.tper = :per03 and :PERTYPE = 1) or (PR.iper = :per03 and :PERTYPE = 2)) then :sign * EP.pvalue else case when :per03>=:from_per and :per03<=:to_per then 0 end end) as m3
            , sum(case when ((PR.cper = :per04 and :PERTYPE = 0) or (PR.tper = :per04 and :PERTYPE = 1) or (PR.iper = :per04 and :PERTYPE = 2)) then :sign * EP.pvalue else case when :per04>=:from_per and :per04<=:to_per then 0 end end) as m4
            , sum(case when ((PR.cper = :per05 and :PERTYPE = 0) or (PR.tper = :per05 and :PERTYPE = 1) or (PR.iper = :per05 and :PERTYPE = 2)) then :sign * EP.pvalue else case when :per05>=:from_per and :per05<=:to_per then 0 end end) as m5
            , sum(case when ((PR.cper = :per06 and :PERTYPE = 0) or (PR.tper = :per06 and :PERTYPE = 1) or (PR.iper = :per06 and :PERTYPE = 2)) then :sign * EP.pvalue else case when :per06>=:from_per and :per06<=:to_per then 0 end end) as m6
            , sum(case when ((PR.cper = :per07 and :PERTYPE = 0) or (PR.tper = :per07 and :PERTYPE = 1) or (PR.iper = :per07 and :PERTYPE = 2)) then :sign * EP.pvalue else case when :per07>=:from_per and :per07<=:to_per then 0 end end) as m7
            , sum(case when ((PR.cper = :per08 and :PERTYPE = 0) or (PR.tper = :per08 and :PERTYPE = 1) or (PR.iper = :per08 and :PERTYPE = 2)) then :sign * EP.pvalue else case when :per08>=:from_per and :per08<=:to_per then 0 end end) as m8
            , sum(case when ((PR.cper = :per09 and :PERTYPE = 0) or (PR.tper = :per09 and :PERTYPE = 1) or (PR.iper = :per09 and :PERTYPE = 2)) then :sign * EP.pvalue else case when :per09>=:from_per and :per09<=:to_per then 0 end end) as m9
            , sum(case when ((PR.cper = :per10 and :PERTYPE = 0) or (PR.tper = :per10 and :PERTYPE = 1) or (PR.iper = :per10 and :PERTYPE = 2)) then :sign * EP.pvalue else case when :per10>=:from_per and :per10<=:to_per then 0 end end) as m10
            , sum(case when ((PR.cper = :per11 and :PERTYPE = 0) or (PR.tper = :per11 and :PERTYPE = 1) or (PR.iper = :per11 and :PERTYPE = 2)) then :sign * EP.pvalue else case when :per11>=:from_per and :per11<=:to_per then 0 end end) as m11
            , sum(case when ((PR.cper = :per12 and :PERTYPE = 0) or (PR.tper = :per12 and :PERTYPE = 1) or (PR.iper = :per12 and :PERTYPE = 2)) then :sign * EP.pvalue else case when :per12>=:from_per and :per12<=:to_per then 0 end end) as m12
            , sum(case when EP.pvalue is not null then :sign * EP.pvalue else 0 end) as RAZEM
          from ecolumns EC
            join eprpos EP on (EP.ecolumn = EC.number
                             and (:empls = '' or :empls containing '"' || ep.employee || '"')
                             and (:emplgr is null or EP.employee in (select ref from employees E where e.emplgroup = :emplgr)))
            join epayrolls PR on (PR.ref = EP.payroll
                             and (:payrolltypes = '' or :payrolltypes containing '"' || PR.prtype || '"'))
          where EC.number = :skladnik
            and ((PR.cper >= :from_per and PR.cper <= :to_per and :PERTYPE = 0)
                or (PR.tper >= :from_per and PR.tper <= :to_per and :PERTYPE = 1)
                or (PR.iper >= :from_per and PR.iper <= :to_per and :PERTYPE = 2))
          group by EC.number, EC.name
          order by EC.number
          into :nr_skladnika, :nazwa, :m1, :m2, :m3, :m4, :m5, :m6, :m7, :m8, :m9, :m10, m11, :m12, :razem
        do
          if (razem <> 0) then
          begin
            if (czy_suma_grup = 0) then
            begin
              select (symbol || ' ' || :nazwa)
                from get_symbol_from_number(:nr_skladnika, 4)
                into :nazwa;
               suspend;
            end
            i = i + 1 ;
            --w pierwszym przebiegu ustalone zostanie czy zmienne agrugujace sa nulowe
            if (Sum_M1 is not null) then Sum_M1 = Sum_M1 + M1;
            if (Sum_M2 is not null) then Sum_M2 = Sum_M2 + M2;
            if (Sum_M3 is not null) then Sum_M3 = Sum_M3 + M3;
            if (Sum_M4 is not null) then Sum_M4 = Sum_M4 + M4;
            if (Sum_M5 is not null) then Sum_M5 = Sum_M5 + M5;
            if (Sum_M6 is not null) then Sum_M6 = Sum_M6 + M6;
            if (Sum_M7 is not null) then Sum_M7 = Sum_M7 + M7;
            if (Sum_M8 is not null) then Sum_M8 = Sum_M8 + M8;
            if (Sum_M9 is not null) then Sum_M9 = Sum_M9 + M9;
            if (Sum_M10 is not null) then Sum_M10 = Sum_M10 + M10;
            if (Sum_M11 is not null) then Sum_M11 = Sum_M11 + M11;
            if (Sum_M12 is not null) then Sum_M12 = Sum_M12 + M12;
            Sum_Razem = Sum_Razem + coalesce(razem,0);
          end
      end

      if (i > 0) then --drukujemy grupe jezeli byl wynik
      begin
        if (czy_sumowac = 1) then
        begin
          czy_podsumowanie = 1;
          if (nazwa = '') then nazwa = ' RAZEM:';
          else nazwa = nazwa_grupy || ':';
          razem = Sum_Razem;
          M1 = Sum_M1;
          M2 = Sum_M2;
          M3 = Sum_M3;
          M4 = Sum_M4;
          M5 = Sum_M5;
          M6 = Sum_M6;
          M7 = Sum_M7;
          M8 = Sum_M8;
          M9 = Sum_M9;
          M10 = Sum_M10;
          M11 = Sum_M11;
          M12 = Sum_M12;
          suspend;
        end
        if (przerwa_tmp = 1) then
        begin
          czy_podsumowanie = 0;
          czy_przerwa = 1;
          nazwa = '';
          M1 = NULL;
          M2 = NULL;
          M3 = NULL;
          M4 = NULL;
          M5 = NULL;
          M6 = NULL;
          M7 = NULL;
          M8 = NULL;
          M9 = NULL;
          M10 = NULL;
          M11 = NULL;
          M12 = NULL;
          Sum_Razem = NULL;
          razem = null;
          suspend;
        end
      end
    end
  end else
  begin
  --Ustawienie tekstu stopki na wydruku
    if (pertype = 0) then stopka = nazwa || ' (w ukł. kosztowym)';
    else if (pertype = 1) then stopka = nazwa || ' (w ukł. podatkowym)';
    else if (pertype = 2) then stopka = nazwa || ' (w ukł. ubezpieczeniowym)';

    if (emplgr is not null) then
    begin
      select name from edictemplgroup where ref = :emplgr
        into: emplgr_name;
      stopka = stopka || ';  gr. zawodowa: ' || emplgr_name;
    end

    if (payrolltypes <> '') then
    begin
      stopka = stopka || ';  typ listy: ';
      i = 0;
      for
        select prtype, defname from eprollstypes
          where :payrolltypes containing '"' || prtype || '"'
          into :prtype, :defname
      do begin
        if (i > 0) then  stopka = stopka || ', ';
        stopka = stopka || prtype;
        i = i + 1;
      end
      if (i = 1) then stopka = stopka || ' - ' || defname;
    end

  --Ustawienie tytulu wydruku
    nazwa = nazwa || ' - zestawienie za okres od ' || per01 || ' do ' || to_per;

    suspend;
  end
end^
SET TERM ; ^
