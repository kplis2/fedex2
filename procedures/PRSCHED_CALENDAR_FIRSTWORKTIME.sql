--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_CALENDAR_FIRSTWORKTIME(
      PRDEPART varchar(10) CHARACTER SET UTF8                           ,
      CHECKTIME timestamp)
  returns (
      RETTIME timestamp)
   as
declare variable CALENDAR integer;
declare variable Y integer;
declare variable M integer;
declare variable D integer;
declare variable MOMENT time;
declare variable REFDAY integer;
declare variable REFNEXTDAY integer;
declare variable KIND integer;
declare variable DSTART time;
declare variable DEND time;
declare variable DAYSTART timestamp;
declare variable DAYNEXTSTART timestamp;
BEGIN
  rettime = :checktime;
  SELECT calendar FROM prdeparts WHERE symbol = :prdepart INTO :calendar;
  IF(:calendar IS NULL) THEN EXIT;
  moment = CAST(:checktime AS TIME);
  daystart = cast(:checktime as DATE);

  SELECT e.REF, dk.daytype, e.WORKSTART, e.WORKEND
    FROM ecaldays e
    JOIN edaykinds dk on dk.ref = e.daykind
    WHERE e.calendar = :calendar
      AND cast(e.CDATE as date) = cast(:daystart as date)
    INTO :refday, :kind, :dstart, :dend;

  IF(:refday IS NULL)
    THEN EXCEPTION universal 'Brak dnia w kalendarzu ('||:prdepart||')dla '||:checktime;
  --jesli jest w czasie pracy, to koniec
  if(:kind = 1 and :moment >= :dstart and (:moment <= :dend or (:dend containing '00:00:00')) ) then
    exit;--jest w czasie pracy, wiec exit
  else if (:moment < :dstart) then begin
    rettime = cast(cast(:checktime as date) ||' '|| cast(:dstart as time) as timestamp);
    exit;
  end
  --poszukiwanie dni do przeodu
  select first 1 e.ref,  e.cdate, e.workstart
    from ecaldays e
    join edaykinds dk on dk.ref = e.daykind
    where e.calendar = :calendar
      and dk.daytype = 1
      and e.cdate  > :daystart
    order by e.calendar, e.daykind, e.cdate
    into :refnextday, :daynextstart, :dstart;
  rettime = cast(cast(:daynextstart as date) ||' '|| cast(:dstart as time) as timestamp);
END^
SET TERM ; ^
