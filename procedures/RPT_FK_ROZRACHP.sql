--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_ROZRACHP(
      DATA_IN timestamp,
      ACCOUNT ACCOUNT_ID,
      SYMBFAK varchar(20) CHARACTER SET UTF8                           ,
      COMPANY integer,
      SLODEF integer,
      SLOPOZ integer)
  returns (
      WINIEN numeric(14,4),
      MA numeric(14,4),
      WINIENPLN numeric(14,4),
      MAPLN numeric(14,4),
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      WALUTA varchar(3) CHARACTER SET UTF8                           ,
      KURS numeric(14,4),
      OPERACJA varchar(20) CHARACTER SET UTF8                           ,
      DATA timestamp)
   as
begin
  for select P.winien, P.ma, P.winienzl, P.mazl, B.symbol, P.waluta, P.kurs, P.operacja, P.data from rozrachp P
    left join BKDOCS B on (P.bkdoc = b.ref)
  where
  P.slodef = :slodef and P.slopoz = :slopoz and P.kontofk = :account and P.data <= :data_in
  and P.symbfak = :symbfak and P.company = :company
  order by P.data
  into :winien, :ma, :winienpln, :mapln, :symbol, :waluta, :kurs, :operacja, :data
  do begin
    suspend;
  end
end^
SET TERM ; ^
