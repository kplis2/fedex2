--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BTRANSFER_CHECKAMOUNT(
      BTRANSFER integer)
   as
declare variable AMOUNT numeric(14,2);
declare variable OAMOUNT numeric(14,2);
declare variable MAXAMOUNT numeric(14,2);
declare variable SETTLEMENT varchar(30);
declare variable ACCOUNT ACCOUNT_ID;
begin
  for select account, settlement, maxamount, amount
  from btransferpos
  where BTRANSFER = :btransfer
  into :account, :settlement, :maxamount, :amount
  do begin
    oamount = null;
    select sum(BTRANSFERPOS.AMOUNT)
      from BTRANSFERPOS
      join BTRANSFERS on (BTRANSFERS.REF = BTRANSFERPOS.BTRANSFER)
      where BTRANSFERPOS.SETTLEMENT = :settlement
          and BTRANSFERPOS.ACCOUNT = :account
          and BTRANSFERPOS.btransfer <> :btransfer
          and ((BTRANSFERS.status = 1) or (BTRANSFERS.status = 2))
    into :oamount;
    if(:oamount > 0) then begin
      maxamount = :maxamount - :oamount;
      if(:maxamount < 0) then maxamount = 0;
      if(:amount > :maxamount) then
        update BTRANSFERPOS set AMOUNT = :maxamount where BTRANSFER = :btransfer and SETTLEMENT = :settlement and ACCOUNT = :account;
    end
  end
end^
SET TERM ; ^
