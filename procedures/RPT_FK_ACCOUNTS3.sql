--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_ACCOUNTS3(
      DATA date,
      ACCOUNTMASK ACCOUNT_ID,
      RANGE smallint,
      WALUTOWY smallint,
      COMPANY integer)
  returns (
      ACCOUNT ACCOUNT_ID,
      ACCOUNTDESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      WINIEN_SUM numeric(14,4),
      MA_SUM numeric(14,4),
      WINIENPLN_SUM numeric(14,4),
      MAPLN_SUM numeric(14,4),
      SALDO_WINIEN_SUM numeric(14,4),
      SALDO_MA_SUM numeric(14,4),
      WALUTA varchar(3) CHARACTER SET UTF8                           )
   as
declare variable ACC_YEAR varchar(4);
declare variable ACC_MONTH_INT integer;
declare variable ACC_MONTH varchar(2);
begin
  for select distinct A.account kontofk from accounting A
    left join bkaccounts B on A.bkaccount = B.ref
    where B.settl > 0 and A.account like :accountmask||'%' and A.company = :company and A.yearid <= extract(year from :data)
    into :account
  do begin
    acc_year = cast(extract(year from :data) as varchar(4));
    acc_month_int = extract(month from :data);
    if(acc_month_int < 10) then
      acc_month = '0' || cast(acc_month_int as varchar(1));
    else
      acc_month = cast(acc_month_int as varchar(2));
    execute procedure fk_get_account_descript(:company, :acc_year||:acc_month, :account)
      returning_values :accountdescript;
    if(exists(select first 1 1 from rpt_fk_rozrach(:data, :account, :company, :walutowy))) then
    begin

      if(:range = 0) then
        for select WALUTA,sum(winien), sum(ma), sum(winienpln), sum(mapln) from rpt_fk_rozrach(:data, :account, :company, :walutowy)
          group by waluta
          into :waluta,:winien_sum, :ma_sum, :winienpln_sum, :mapln_sum
        do
          suspend;
      else if (:range = 1) then
        select sum(winien), sum(ma), sum(winienpln), sum(mapln) from rpt_fk_rozrach(:data, :account, :company, :walutowy)
        where SALDO_WINIEN <> 0 or SALDO_MA <> 0
        into :winien_sum, :ma_sum, :winienpln_sum, :mapln_sum;
      else if (:range = 2) then
        select sum(winien), sum(ma), sum(winienpln), sum(mapln) from rpt_fk_rozrach(:data, :account, :company, :walutowy)
        where SALDO_WINIEN = 0 and SALDO_MA = 0
        into :winien_sum, :ma_sum, :winienpln_sum, :mapln_sum;

      saldo_winien_sum = :winien_sum - :ma_sum;
      saldo_ma_sum = :ma_sum - :winien_sum;
      if(saldo_winien_sum < 0) then
        saldo_winien_sum = 0;
      if(saldo_ma_sum < 0) then
        saldo_ma_sum = 0;
      --if(:range = 0) then
        --suspend;
      else if(:range = 1 and :winien_sum <> :ma_sum) then begin
        for select waluta,sum(winien), sum(ma), sum(winienpln), sum(mapln) from rpt_fk_rozrach(:data, :account, :company, :walutowy)
          where SALDO_WINIEN <> 0 or SALDO_MA <> 0
          group by waluta
          into :waluta,:winien_sum, :ma_sum, :winienpln_sum, :mapln_sum
        do
          suspend;
      end
      else if(:range = 2 and :winien_sum = :ma_sum) then begin
        for select waluta,sum(winien), sum(ma), sum(winienpln), sum(mapln) from rpt_fk_rozrach(:data, :account, :company, :walutowy)
          where SALDO_WINIEN = 0 and SALDO_MA = 0
          group by waluta
          into :waluta,:winien_sum, :ma_sum, :winienpln_sum, :mapln_sum
        do
          suspend;
      end
    end
  end
end^
SET TERM ; ^
