--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ECALDAYS_STORE(
      STYPE varchar(10) CHARACTER SET UTF8                           ,
      CALENDAR integer,
      FROMDATE date,
      TODATE date,
      DAYKIND smallint = 1)
  returns (
      RET integer)
   as
declare variable SQUERY varchar(40);
declare variable SPARAMS varchar(100);
declare variable TMPREF integer;
declare variable RET_SUM integer;
declare variable RET_CNT integer;
begin

  if (stype in ('SUM', 'COUNT')) then
    squery = 'ECALDAYS_' || stype;
  else
    exception universal 'Nieznazny parametr!';

  sparams = 'CDATE>=''' || fromdate || ''' and CDATE<=''' || todate || ''' and CALENDAR=' || calendar;
  if (daykind is not null) then sparams = sparams || ' and DAYKIND=' || daykind;

  select ref, pvalue from epayrollparams_tmp
    where squery = :squery and sparams = :sparams
    into :tmpref, :ret;

  if (tmpref is null) then
  begin
    if (daykind is not null) then
      select coalesce(sum(d.worktime),0), count(*)
        from ecaldays d
        join edaykinds k on (k.ref = d.daykind)
        where d.cdate >= :fromdate and d.cdate <= :todate
          and d.calendar = :calendar
          and k.daykind = :daykind
        into :ret_sum, :ret_cnt;
    else
      select coalesce(sum(worktime),0), count(*)
        from ecaldays
        where cdate >= :fromdate and cdate <= :todate
          and calendar = :calendar
        into :ret_sum, :ret_cnt;

    if (stype = 'SUM') then
      ret = ret_sum;
    else
      ret = ret_cnt;

    insert into epayrollparams_tmp(squery, sparams, pvalue)
      values ('ECALDAYS_SUM', :sparams, :ret_sum);

    insert into epayrollparams_tmp(squery, sparams, pvalue)
      values ('ECALDAYS_COUNT', :sparams, :ret_cnt);
  end

  suspend;
end^
SET TERM ; ^
