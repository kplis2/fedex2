--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KLIENCI_KOPIUJODBIORCE(
      KLIENT integer,
      ODBIORCA integer)
  returns (
      NEWODBIORCA integer)
   as
begin
  execute procedure GEN_REF('ODBIORCY') returning_values :newodbiorca;
  insert into ODBIORCY(REF, KLIENT, NAZWA, NAZWAFIRMY, DULICA, DMIASTO, DKODP, DPOCZTA, DTELEFON, KATEGORIA, ODBIORCASPEDYC)
  select :newodbiorca, :klient, NAZWA, NAZWAFIRMY, DULICA, DMIASTO, DKODP, DPOCZTA, DTELEFON, KATEGORIA, ref
  from ODBIORCY where ref=:odbiorca;
end^
SET TERM ; ^
