--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHCALCS_CREATE(
      PRSHCALC integer)
  returns (
      STATUS integer)
   as
declare variable method integer;
declare variable ref integer;
declare variable sheet integer;
declare variable oldtkwref integer;
declare variable newtkwref integer;
begin
  -- procedura zaklada kalkulacje od nowa na podstawie zadanej metody

  -- zapamietaj ref starej rubryki do TKW
  oldtkwref = null;
  newtkwref = null;
  select first 1 ref from prcalccols where calc=:prshcalc and parent is null into :oldtkwref;
  -- wyczysc stare rubryki
  delete from prcalccols where calc=:prshcalc;

  -- przejdz sie po definicjach rubryk na pierwszym poziomie i wywolaj dla nich rekurencje
  select method,sheet from prshcalcs where ref=:prshcalc into :method, :sheet;
  execute procedure PRSHCALCS_SUBCREATE(:sheet,:prshcalc,NULL,:method,NULL);

  -- zaktualizuj znacznik excluded na podstawie wybranych alternatyw
  for select ref from prcalccols where calc=:prshcalc and isalt=1 and calctype=0
  into :ref
  do begin
    execute procedure PRSHCALC_UPDATE_EXCLUDED(:ref);
  end

  -- odczytaj nowy ref rubryki do TKW i podmien w kalkulacjach zaleznych
  select first 1 ref from prcalccols where calc=:prshcalc and parent is null into :newtkwref;
  if(:oldtkwref is not null and (newtkwref<>oldtkwref or newtkwref is null)) then
    update prcalccols set fromprcalccol=:newtkwref where fromprcalccol=:oldtkwref;

  status = 1;
end^
SET TERM ; ^
