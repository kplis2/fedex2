--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_REQMDLIMITS(
      EMPL integer,
      ONDATE date)
  returns (
      REQLIMIT smallint,
      MDLIMIT smallint,
      MSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable mdl smallint;
declare variable bdate date;
declare variable tvyear integer;
begin
  msg = '';
  tvyear = extract(year from :ondate);
  -- zalozenie ze mozna zlozyc wiecej niz jden wniosek w roku
  execute procedure get_config('VACREQLIMIT', 2) returning_values :reqlimit;
    select first 1 f.kind
      from empladdfiles f
        join empladdfiletypes t on (t.ref = f.filetype and t.symbol = 'STD_OPKKOD')
      where f.employee = :empl
        and f.kind in (1,2) -- 1 = 1 dzien opieki, 2 = 2 dni opieki, 0 i 3 = nie zlozyl i nie zamierza
        and f.fromdate <= :ondate
        and coalesce(cast(f.todate as date),:ondate) >= :ondate
      order by f.fromdate DESC
      into :mdl;
    mdl = coalesce(mdl,0);
  if(mdl > 0) then
  begin
    select max(f.birthdate)
      from emplfamily f
        join edictzuscodes z on z.ref = f.conecttype
      where f.employee = :empl
        and z.code in (11,21)
        and z.dicttype = 4
      into :bdate;
    if (:bdate is null) then
    begin
      mdlimit = 0;
      msg = 'Pracownik nie posiada dziecka w wieku do 14 lat';
    end
    else if(dateadd(14 year to bdate) < :ondate) then
    begin
      mdlimit = 0;
      msg = 'Dziecko pracownika skończyło 14 lat przed datą: '||:ondate;
    end
    else
      mdlimit = mdl;
  end
  else
    mdlimit = 0;
  suspend;
end^
SET TERM ; ^
