--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EGUS_TWELVE_WORKTIME(
      INFOMODE smallint,
      EMPLOYEE integer,
      FROMDATE date,
      TODATE date)
  returns (
      WDIMDAYS numeric(14,2),
      NOMWORKDAYS integer,
      STDEMPLCAL integer,
      NOTWORKEDHOURS numeric(14,2),
      NOTWHOURS_WORKS numeric(14,2),
      WORKEDHOURS numeric(14,2),
      EXTRAHOURS numeric(14,2),
      NOTWORKEDDAYS numeric(14,2),
      NOTWDAYS_WORKS numeric(14,2))
   as
declare variable MFROMDATE date;
declare variable MTODATE date;
declare variable WORKDIM float;
declare variable WORKDAYS integer;
declare variable ETYPE smallint;
declare variable PROPORTIONAL smallint;
declare variable TMP_WORKEDHOURS numeric(14,2);
begin
/*MWr Personel: procedura generuje dane do sprawozdania GUS Z-12 (na rok 2010)
  dla zadanego pracownika EMPLOYEE i zadanego okresu FROMDATE - TODATE. Dla
   - INFOMODE = 1: dane dotycza wskaznika wymiaru czasu pracy (wg przykladu
     liczenia podanego w dokumentacji dla D1) oraz nominalna liczbe dni roboczych
   - INFOMODE = 2: dane dotycza czasu pracy dla zadanego okresu */

  if (infomode = 1) then
  begin
    wdimdays = 0;
    for
      select fromdate, todate, workdim
        from e_get_employmentchanges(:employee, :fromdate, :todate, ';WORKDIM;')
        into :mfromdate, :mtodate, :workdim
    do begin
      workdays = null;
      select count(*) from emplcaldays cd
      join edaykinds edk on edk.ref = cd.daykind
        where cd.cdate >= :mfromdate and cd.cdate <= :mtodate
          and cd.employee = :employee and edk.daytype = 1
        into :workdays;
  
      wdimdays = wdimdays + workdim * (1.0 * coalesce(workdays,0));
    end
  
    --pobranie kalendarze wg, ktorego bedzie rozliczany pracownik
    select first 1 calendar from emplcalendar
      where employee = :employee and fromdate <= :todate
      order by fromdate desc
      into :stdemplcal;
  
    --pracownik moze nie miec kalendarza w danym okresie, wiec wezniemy 'najblizszy'
    if (stdemplcal is null) then
      select first 1 calendar from emplcalendar
        where employee = :employee and fromdate >=:fromdate
        order by fromdate
        into :stdemplcal;
  
    --ostatecznie mozemy posluzyc sie kalendarzem standardowym
    if (stdemplcal is null) then
      execute procedure get_config('STDCALENDAR',2) returning_values :stdemplcal;
  
    --wyliczenie nominalej liczby dni roboczych w danym okresie do przepracownia
    select count(*) from ecaldays cd
    join edaykinds edk on edk.ref = cd.daykind
      where cd.calendar = :stdemplcal and edk.daytype = 1
        and cd.cdate >= :fromdate and cd.cdate <= :todate
      into :nomworkdays;

    if (coalesce(nomworkdays,0) = 0) then
    begin
      execute procedure get_config('STDCALENDAR',2) returning_values :stdemplcal;

      select count(*) from ecaldays cd
      join edaykinds edk on edk.ref = cd.daykind
        where cd.calendar = :stdemplcal and edk.daytype = 1
          and cd.cdate >= :fromdate and cd.cdate <= :todate
        into :nomworkdays;
    end
  end else
--------------------------------------------------------------------------------
  if (infomode = 2) then
  begin
  /*czas nieprzepracowany ogolem oraz
    czas nieprzepracowany oplacony tylko przez zaklad pracy (sum1 >= sum2)*/

    select sum(a.worksecs) /3600.00, sum(case when strmulticmp(';'||coalesce(c.cflags,'')||';', ';ZAS;') = 1 or c.number in (10,190,260,300,330,390) then worksecs else 0 end) /3600.00
         , sum(a.workdays), sum(case when strmulticmp(';'||coalesce(c.cflags,'')||';', ';ZAS;') = 1 or c.number in (10,190,260,300,330,390) then workdays else 0 end)  --BS108630
      from eabsences a
      join ecolumns c on (c.number = a.ecolumn)
      where a.employee = :employee and a.correction in (0,2)
        and a.fromdate >= :fromdate and a.todate <= :todate
      into :notworkedhours, :notwhours_works
         , :notworkeddays, :notwdays_works;

    notworkeddays = coalesce(notworkeddays,0);
    notworkedhours = coalesce(notworkedhours,0);
    notwdays_works = coalesce(notwdays_works,0);
    notwhours_works = coalesce(notwhours_works,0);

  --czas faktycznie przepracowny w godz nominalnych
    workedhours = 0;
    for
      select fromdate,  todate, workdim, proportional
        from e_get_employmentchanges(:employee, :fromdate, :todate, ';PROPORTIONAL;')
        into: mfromdate, :mtodate, :workdim, :proportional
    do begin
      tmp_workedhours = null;
      select sum(worktime) /3600.00 from emplcaldays
        where employee = :employee
          and cdate >= :mfromdate and cdate <= :mtodate
        into :tmp_workedhours;
      tmp_workedhours = coalesce(tmp_workedhours,0);
  
      if (workdim <> 1 and proportional = 1) then
        tmp_workedhours = tmp_workedhours * workdim;

      workedhours = workedhours + tmp_workedhours;
    end
    workedhours = workedhours - notworkedhours;

  --czas faktycznie przepracowny w godz nadliczbowych
    execute procedure get_config('EWORKEDHOURS_TYPE',2)
      returning_values :etype; --(PR33972)
  
    if (etype = 0) then
      select sum(coalesce(overtime50,0) + coalesce(overtime100,0)) /3600.00 from emplcaldays
        where employee = :employee
          and cdate >= :fromdate and cdate <= :todate
        into :extrahours;
    else
      select sum(amount) from eworkedhours
        where employee = :employee
          and hdate >= :fromdate and hdate <= :todate
          and ecolumn in (600,610)
        into :extrahours;
    extrahours = coalesce(extrahours,0);
  end
  suspend;
end^
SET TERM ; ^
