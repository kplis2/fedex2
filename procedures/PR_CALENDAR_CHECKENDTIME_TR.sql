--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_CALENDAR_CHECKENDTIME_TR(
      PRDEPART varchar(10) CHARACTER SET UTF8                           ,
      ENDTIME timestamp,
      WORKTIME double precision)
  returns (
      STARTTIME timestamp)
   as
declare variable CURDATE timestamp;
declare variable TMPTIMESTAMP timestamp;
declare variable ST time;
declare variable ET time;
declare variable CALENDAR integer;
declare variable DAYKIND integer;
declare variable DAYWORKTIME double precision;
declare variable DAYNOTWORKTIME double precision;
declare variable PASSTIME double precision;
begin
  curdate = :endtime;
  starttime = :endtime;
  tmptimestamp = endtime;
  select prdeparts.calendar from PRDEPARTs where SYMBOL = :PRDEPART into :calendar;
  if(:calendar is null) then
    exception UNIVERSAL 'Brak kalendarza dla wydzialu '||:prdepart;

  select WORKSTART, WORKEND,
      cast(
        cast(cast(extract(hour from :endtime)||':'|| extract(minute from :endtime) ||':'|| extract(second from :endtime) as time)
          - WORKSTART as double precision)
          / cast (3600 as double precision)
          / cast(24 as double precision) as double precision),
      cast(
        cast(cast(extract(hour from :endtime)||':'|| extract(minute from :endtime) ||':'|| extract(second from :endtime) as time)
          - WORKEND as double precision)
          / cast (3600 as double precision)
          / cast(24 as double precision) as double precision)
    from ecaldays
    where calendar = :calendar
        and cdate = cast(:endtime as date)
    into :st,  :et, :dayworktime, :daynotworktime;
  if (daynotworktime > 0) then
    dayworktime = dayworktime - daynotworktime;
  if(:dayworktime > :worktime) then begin
    starttime = :endtime - :worktime;
    exit;
  end else begin
    worktime = worktime - dayworktime;
    starttime = cast(endtime as date);
  end

  for
    select cdate, edk.daytype, WORKSTART, WORKEND, PRWORKTIME
      from ecaldays cd
      join edaykinds edk on edk.ref = cd.daykind
      where cd.calendar = :calendar
        and cd.cdate < cast(:endtime as date)
      order by cd.calendar, cd.cdate desc
      into :curdate, :daykind, :st, :et, :dayworktime
  do begin
    if(daykind <> 1) then begin
      starttime = :starttime - 1; --jezeli dany dzien jest wolny, wowczas cofamy sie o cala dobe
    end else begin
      if(:dayworktime > 0)then
      begin
     --   starttime = :starttime - cast(:curdate + 1 - cast(cast(:curdate as date) || ' ' || :et as timestamp)as double precision);
        starttime = cast(cast(:curdate as date) || ' ' || :et as timestamp);
        if(:worktime < :dayworktime) then begin
          starttime = :starttime - :worktime;
          exit;
        end else begin
          --starttime = :starttime - :dayworktime - (cast(cast(:curdate as date) || ' ' || :st as timestamp) - cast(:curdate as date));
          starttime = cast(curdate as date);
          worktime = :worktime - :dayworktime;
        end
      end
    end
  end
end^
SET TERM ; ^
