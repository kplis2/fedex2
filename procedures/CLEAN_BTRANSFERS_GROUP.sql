--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CLEAN_BTRANSFERS_GROUP(
      BGROUP INTEGER_ID)
   as
declare variable btransfer btransfer_id;
begin
  --Procedura sprząta btransfery po masowej generacji
  select first 1 b.ref                                                      
    from btransfers b join btransferpos bp on ( bp.btransfer = b.ref )
    where b.gengroup = :bgroup
    group by  b.ref
    having count(bp.ref) = 0
    into :btransfer;
  while(btransfer is not null) do
  begin
    delete from btransfers b where b.ref = :btransfer;

    btransfer = null;

    select first 1 b.ref
      from btransfers b join btransferpos bp on ( bp.btransfer = b.ref )
      where b.gengroup = :bgroup
      group by  b.ref
      having count(bp.ref) = 0
      into :btransfer;
  end
end^
SET TERM ; ^
