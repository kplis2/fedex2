--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_GLS_SHIPPINGLABEL_REQUEST(
      OTABLE STRING20,
      OREF INTEGER_ID)
  returns (
      ID INTEGER_ID,
      PARENT INTEGER_ID,
      NAME STRING255,
      PARAMS STRING255,
      VAL STRING255)
   as
declare variable SHIPPINGDOC LISTYWYSD_ID;
declare variable ROOTPARENT INTEGER_ID;
declare variable GRANDPARENT INTEGER_ID;
declare variable SUBPARENT INTEGER_ID;
declare variable SECONDSUBPARENT INTEGER_ID;
declare variable SHIPPINGID STRING40;
declare variable SERVICETYPE STRING20;
declare variable USERNAME STRING40;
declare variable USERPASSWORD STRING40;
declare variable FILEPATH STRING255;
declare variable DELIVERYTYPE SPOSDOST_ID;
begin
  /* symbolspedid */
  servicetype = 'TEST';

  select l.ref, l.symbolspedid, l.sposdost
    from listywysd l
    where l.ref = :oref
  into :shippingdoc, :shippingid, :deliverytype;

  --sprawdzanie czy istieje dokument spedycyjny
  if(:shippingdoc is null) then exception EDE_LISTYWYSD_BRAK;

  val = null;
  parent = null;
  name = 'adePreparingBox_GetConsignLabels';
  if (:servicetype = 'PRODUCTION') then
    params = 'xmlns="https://adeplus.gls-poland.com/adeplus/pm1/ade_webapi2.php?wsdl"';
  else
    params = 'xmlns="https://ade-test.gls-poland.com/adeplus/pm1/ade_webapi2.php?wsdl"';
  id = 0;
  suspend;

  rootparent = id;

  --dane autoryzacyjne
  select k.login, k.haslo, k.sciezkapliku
    from spedytwys w
      left join spedytkonta k on (w.spedytor = k.spedytor)
    where w.sposdost = :deliverytype
  into :username, :userpassword, :filepath;

  val = :username;
  name = 'username';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = :userpassword;
  name = 'password';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = :filepath;
  name = 'filepath';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = :servicetype;
  name = 'serviceType';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = 'roll_160x100_zebra_epl';
  name = 'labelName';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = 'EPL';
  name = 'labelType';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = :shippingid;
  name = 'shippingid';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;
end^
SET TERM ; ^
