--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NOTYPOZ_OBL(
      POZREF integer,
      DOKUMENT integer)
   as
declare variable KURSNAG numeric(14,2);
declare variable WALUTANAG varchar(3);
begin
  --pobranie waluty oraz kursu z notypnag
  select ng.rateavg, currency
    from notynag ng
    where ng.ref = :dokument
    into :kursnag, :walutanag;
  --update pool
  update notypoz np
    set np.currency = :walutanag,
      np.bdebitzl = np.bdebit * :kursnag ,
      np.bcreditzl = np.bcredit * :kursnag,
      np.ammountzl = np.ammount * :kursnag,
      np.ammountpaidzl = np.ammountpaid * :kursnag,
      np.ammountleftzl = np.ammountleft * :kursnag
    where np.ref = :pozref;
end^
SET TERM ; ^
