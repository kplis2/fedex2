--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRDSCR_CELL(
      KEY1 integer,
      KEY2 integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      FRHDR varchar(10) CHARACTER SET UTF8                           ,
      FRDHDR varchar(20) CHARACTER SET UTF8                           ,
      FRDHDERVER varchar(20) CHARACTER SET UTF8                           ,
      FRDPERSPECT varchar(20) CHARACTER SET UTF8                           ,
      FRDPERSDIMHIER varchar(20) CHARACTER SET UTF8                           ,
      FRPSN varchar(20) CHARACTER SET UTF8                           ,
      VALTYPE varchar(20) CHARACTER SET UTF8                           ,
      ELEMHIER varchar(40) CHARACTER SET UTF8                           ,
      ELEMENT integer,
      TIMEELEM integer,
      ADDELEMHIER smallint)
  returns (
      RET numeric(14,2))
   as
declare variable adr integer;
declare variable frdhdrverint integer;
declare variable valfromtype varchar(20);
declare variable frdperspectint integer;
declare variable frdpersdimhierint integer;
declare variable frdpersdimvalint integer;
declare variable ddparams varchar(255);
declare variable period varchar(6);
begin
  if (:frhdr is null) then
    select frdperspects.frhdr
      from frdcells 
        join frdperspects on (frdperspects.ref = frdcells.frdperspects)
      where frdcells.ref = :key1
      into :frhdr;
  if (:frdhdr is null) then
    select frdhdrs.shortname
      from frdcells 
        join frdhdrsvers on (frdhdrsvers.ref = frdcells.frdhdrver)
        join frdhdrs on (frdhdrsvers.frdhdr = frdhdrs.ref)
      where frdcells.ref = :key1
      into :frdhdr;
  if (:frdhderver is null) then
    select frdhdrsvers.shortname
      from frdhdrs
        join frdhdrsvers on (frdhdrsvers.frdhdr = frdhdrs.ref)
      where frdhdrs.shortname = :frdhdr and frdhdrsvers.main = 1
      into :frdhderver;
  if (:frdperspect is null) then
    select frdperspects.shortname
      from frdcells 
        join frdperspects on (frdperspects.ref = frdcells.frdperspects)
      where frdcells.ref = :key1
      into :frdperspect;
  if (:frdpersdimhier is null) then
    select frdpersdimhier.shortname
      from frdcells
      join frdpersdimhier on (frdcells.frdpersdimhier = frdpersdimhier.ref)
      where frdcells.ref = :key1
      into :frdpersdimhier;
  if (:frpsn is null) then
    select frpsns.symbol
      from frdcells
        join frdpersdimvals on (frdpersdimvals.ref = frdcells.frdpersdimval)
        join frpsns on (frdpersdimvals.frpsn = frpsns.ref)
      where frdcells.ref = :key1
      into :frpsn;
  if (:valtype is null) then
    valtype = 'PLANAMOUNT';
  if (:elemhier is null) then
    select frdcells.elemhier
      from frdcells
      where frdcells.ref = :key1
      into :elemhier;
  if (:element is null) then
    select frdcells.dimelem
      from frdcells
      where frdcells.ref = :key1
      into :element;
  if (:timeelem is null) then
    select frdcells.timelem
      from frdcells
      where frdcells.ref = :key1
      into :timeelem;
  select frdhdrsvers.ref
    from frdhdrsvers
      join frdhdrs on (frdhdrs.ref = frdhdrsvers.frdhdr)
    where frdhdrs.shortname = :frdhdr and frdhdrsvers.shortname = :frdhderver
    into :frdhdrverint;
  select frdperspects.ref
    from frdperspects
    where frdperspects.shortname = :frdperspect
    into :frdperspectint;
  select frdpersdimhier.ref
    from frdpersdimhier
    where frdpersdimhier.frdperspect = :frdperspectint
      and frdpersdimhier.shortname = :frdpersdimhier
    into :frdpersdimhierint;
  select frdpersdimvals.ref
    from frdpersdimvals
      join frpsns on (frdpersdimvals.frpsn = frpsns.ref)
    where frpsns.frhdr = :frhdr and frpsns.symbol = :frpsn
      and frdpersdimvals.frdpersdimshier = :frdpersdimhierint
    into :frdpersdimvalint;
  if (:ADDELEMHIER is not null and :ADDELEMHIER =1) then
    elemhier = :elemhier||','||:element;
  select frdcells.ref
    from frdcells
    where frdcells.frdhdrver = :frdhdrverint and frdcells.frdpersdimval = :frdpersdimvalint
    and frdcells.dimelem = :element and frdcells.timelem = :timeelem
    and frdcells.elemhier = :elemhier
    into :adr;
  if (:valtype = 'PLANAMOUNT') then
    select frdcells.planamount
      from frdcells
      where frdcells.frdhdrver = :frdhdrverint and frdcells.frdpersdimval = :frdpersdimvalint
      and frdcells.dimelem = :element and frdcells.timelem = :timeelem
      and frdcells.elemhier = :elemhier
      into :ret;
  if (:valtype = 'REALAMOUNT') then
    select frdcells.realamount
      from frdcells
      where frdcells.frdhdrver = :frdhdrverint and frdcells.frdpersdimval = :frdpersdimvalint
      and frdcells.dimelem = :element and frdcells.timelem = :timeelem
      and frdcells.elemhier = :elemhier
      into :ret;
  if (:valtype = 'VAL1') then
    select frdcells.val1
      from frdcells
      where frdcells.frdhdrver = :frdhdrverint and frdcells.frdpersdimval = :frdpersdimvalint
      and frdcells.dimelem = :element and frdcells.timelem = :timeelem
      and frdcells.elemhier = :elemhier
      into :ret;
  if (:valtype = 'VAL2') then
    select frdcells.val2
      from frdcells
      where frdcells.frdhdrver = :frdhdrverint and frdcells.frdpersdimval = :frdpersdimvalint
      and frdcells.dimelem = :element and frdcells.timelem = :timeelem
      and frdcells.elemhier = :elemhier
      into :ret;
  if (:valtype = 'VAL3') then
    select frdcells.val3
      from frdcells
      where frdcells.frdhdrver = :frdhdrverint and frdcells.frdpersdimval = :frdpersdimvalint
      and frdcells.dimelem = :element and frdcells.timelem = :timeelem
      and frdcells.elemhier = :elemhier
      into :ret;
  if (:valtype = 'VAL4') then
    select frdcells.val4
      from frdcells
      where frdcells.frdhdrver = :frdhdrverint and frdcells.frdpersdimval = :frdpersdimvalint
      and frdcells.dimelem = :element and frdcells.timelem = :timeelem
      and frdcells.elemhier = :elemhier
      into :ret;
  if (:valtype = 'VAL5') then
    select frdcells.val5
      from frdcells
      where frdcells.frdhdrver = :frdhdrverint and frdcells.frdpersdimval = :frdpersdimvalint
      and frdcells.dimelem = :element and frdcells.timelem = :timeelem
      and frdcells.elemhier = :elemhier
      into :ret;
  if (:key2 = 0) then
    VALFROMTYPE = 'PLANAMOUNT';
  else if (:key2 = 1) then
    VALFROMTYPE = 'REALAMOUNT';
  else if (:key2 = 2) then
    VALFROMTYPE = 'VAL1';
  else if (:key2 = 3) then
    VALFROMTYPE = 'VAL2';
  else if (:key2 = 4) then
    VALFROMTYPE = 'VAL3';
  else if (:key2 = 5) then
    VALFROMTYPE = 'VAL4';
  else if (:key2 = 6) then
    VALFROMTYPE = 'VAL5';
  if (not exists (select ref from frdcellsbottomup where frdcellsbottomup.bottomup = 2
      and frdcellsbottomup.frdcell = :key1 and frdcellsbottomup.frdcelladd = :adr
      and frdcellsbottomup.func = 'CELL' and frdcellsbottomup.field = :valtype
      and frdcellsbottomup.addfield = :valtype)) then
  begin
    insert into frdcellsbottomup (frdcell, frdcelladd, field, addfield, isactual, func, bottomup)
      values (:key1,:adr,:VALFROMTYPE, :valtype,1,'CELL', 2);
  end else
  begin
    update frdcellsbottomup set frdcellsbottomup.isactual = 1
      where frdcellsbottomup.frdcell = :key1 and frdcellsbottomup.frdcelladd = :adr
        and frdcellsbottomup.bottomup = 2 and frdcellsbottomup.func = 'CELL'
        and frdcellsbottomup.field = :VALFROMTYPE and frdcellsbottomup.addfield = :valtype;
  end
  if (ret is null) then
    ret = 0;

  select substring(dimelemdef.shortname from 1 for 6)
    from dimelemdef
    where dimelemdef.ref = :timeelem
    into :period;

    ddparams = 'Okres='||:period||'; Wiersz='||:frpsn||'; Sprawozdanie='||:frhdr||'; Wartosc='||:valtype;
    insert into frddrilldown (frdcell, functionname, fvalue, ddparams, valtype, descript)
       values (:key1, 'CELL', :ret, :ddparams, :key2, 'Cell');
  suspend;
end^
SET TERM ; ^
