--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXP2PLATNIK_ZZA(
      EMPLCONTR_REF integer,
      CONTR_FROMDATE date,
      CONTR_TODATE date,
      COMPANY integer)
  returns (
      PERSON integer,
      PESEL varchar(11) CHARACTER SET UTF8                           ,
      NIP varchar(15) CHARACTER SET UTF8                           ,
      FNAME varchar(60) CHARACTER SET UTF8                           ,
      SNAME varchar(30) CHARACTER SET UTF8                           ,
      MIDDLENAME varchar(30) CHARACTER SET UTF8                           ,
      MNAME varchar(30) CHARACTER SET UTF8                           ,
      BIRTHDATE date,
      CITIZENSHIP varchar(20) CHARACTER SET UTF8                           ,
      FROMDATE date,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      DOCNO varchar(11) CHARACTER SET UTF8                           ,
      SEX varchar(1) CHARACTER SET UTF8                           ,
      POSTCODE varchar(10) CHARACTER SET UTF8                           ,
      CITY varchar(255) CHARACTER SET UTF8                           ,
      COMMUNITY varchar(255) CHARACTER SET UTF8                           ,
      STREET varchar(255) CHARACTER SET UTF8                           ,
      HOUSENO varchar(20) CHARACTER SET UTF8                           ,
      LOCALNO varchar(20) CHARACTER SET UTF8                           ,
      PHONE varchar(255) CHARACTER SET UTF8                           ,
      POSTCODE1 varchar(10) CHARACTER SET UTF8                           ,
      CITY1 varchar(255) CHARACTER SET UTF8                           ,
      COMMUNITY1 varchar(255) CHARACTER SET UTF8                           ,
      STREET1 varchar(255) CHARACTER SET UTF8                           ,
      HOUSENO1 varchar(20) CHARACTER SET UTF8                           ,
      LOCALNO1 varchar(20) CHARACTER SET UTF8                           ,
      PHONE1 varchar(255) CHARACTER SET UTF8                           ,
      POSTCODE2 varchar(10) CHARACTER SET UTF8                           ,
      CITY2 varchar(255) CHARACTER SET UTF8                           ,
      COMMUNITY2 varchar(255) CHARACTER SET UTF8                           ,
      STREET2 varchar(255) CHARACTER SET UTF8                           ,
      HOUSENO2 varchar(20) CHARACTER SET UTF8                           ,
      LOCALNO2 varchar(20) CHARACTER SET UTF8                           ,
      PHONE2 varchar(255) CHARACTER SET UTF8                           ,
      EMAIL2 varchar(255) CHARACTER SET UTF8                           ,
      EDUCATION varchar(20) CHARACTER SET UTF8                           ,
      INSURDATE date,
      ASCODE varchar(20) CHARACTER SET UTF8                           ,
      INVALIDITY varchar(20) CHARACTER SET UTF8                           ,
      RETIRED varchar(20) CHARACTER SET UTF8                           ,
      NFZ varchar(20) CHARACTER SET UTF8                           ,
      DIMNUM integer,
      DIMDEN integer,
      DISABILITY varchar(20) CHARACTER SET UTF8                           ,
      NFZNAME varchar(1024) CHARACTER SET UTF8                           ,
      SPECCONDITION varchar(20) CHARACTER SET UTF8                           ,
      SPECFROM date,
      SPECTO date,
      WPCODE varchar(20) CHARACTER SET UTF8                           )
   as
begin
  --MWr: Personel - Zgłoszenie do ubezpieczenia zdrowotnego / zgłoszenie zmiany danych ZUS ZZA

  nip = ''; --PR57778
  emplcontr_ref = coalesce(emplcontr_ref,0);

  for
    select p.ref,
        case when (ez.ispesel = 1) then p.pesel else '' end as pesel,
        upper(p.fname), upper(p.sname), upper(p.middlename), upper(p.mname), p.birthdate, upper(p.citizenship), c.fromdate,
        case when (ez.isother = 1 and p.evidenceno > '') then '1' when (ez.isother = 2 and p.passportno > '') then '2' else '' end as doctype,
        case when (ez.isother = 1 and p.evidenceno > '') then p.evidenceno when (ez.isother = 2 and p.passportno > '') then p.passportno else '' end as docno,
        case when (p.sex = 0) then 'K' else 'M' end as sex,
        a0.postcode, upper(a0.city), upper(g0.descript), upper(a0.street), upper(a0.houseno), upper(a0.localno), a0.phone,
        a1.postcode, upper(a1.city), upper(g1.descript), upper(a1.street), upper(a1.houseno), upper(a1.localno), a1.phone,
        a2.postcode, upper(a2.city), upper(g2.descript), upper(a2.street), upper(a2.houseno), upper(a2.localno), a2.phone, upper(p.email),
        case when c.empltype = 1 then ez.insurdate else c.fromdate end as insurdate,
        case when c.empltype = 1 then z1.code else z8.code end as ascode,
        z.code as education, z2.code as invalidity, z3.code as retired, z4.code as nfz, c.dimnum, c.dimden,
        z6.code as disability, z4.descript as nfzname, z7.code as speccondition, ez.specfrom, ez.specto, g2.code as wpcode
      from employees e
        join persons p on (p.ref = e.person)
        join emplcontracts c on (e.ref = c.employee)
        join econtrtypes t on (c.econtrtype = t.contrtype)
        left join epersaddr a0 on (a0.person = p.ref and a0.addrtype = 0)
        left join edictguscodes g0 on (a0.communityid = g0.ref)
        left join epersaddr a1 on (a1.person = p.ref and a1.addrtype = 1)
        left join edictguscodes g1 on (a1.communityid = g1.ref)
        left join epersaddr a2 on (a2.person = p.ref and a2.addrtype = 2)
        left join edictworkposts w on (e.workpost = w.ref)
        left join edictguscodes g2 on (w.guscode = g2.ref)
        left join edictzuscodes z on (p.education = z.ref)
        left join ezusdata ez on (ez.person = p.ref)
        left join edictzuscodes z1 on (z1.ref = ez.ascode)
        left join edictzuscodes z2 on (z2.ref = ez.invalidity)
        left join edictzuscodes z3 on (z3.ref = ez.retired)
        left join edictzuscodes z4 on (z4.ref = ez.nfz)
        left join edictzuscodes z6 on (z6.ref = ez.disability)
        left join edictzuscodes z7 on (z7.ref = ez.speccondition)
        left join edictzuscodes z8 on (z8.ref = c.ascode)
      where t.empltype > 1
        and c.iflags not like '%%;FC;%%'
        and c.iflags not like '%%;FE;%%'
        and c.iflags not like '%%;FR;%%'
        and c.iflags like '%%;NFZ;%%'
        and e.company = :company
        and ((:emplcontr_ref > 0 and c.ref = :emplcontr_ref)
          or (:emplcontr_ref <= 0
            and c.fromdate <= :contr_todate and c.fromdate >= :contr_fromdate
            and not exists (select ec2.ref
                              from emplcontracts ec2
                              where ec2.fromdate < c.fromdate and c.employee = ec2.employee and ec2.etermination is null
                              and ec2.ascode = c.ascode)))
      into :person, :pesel, :fname, :sname, :middlename, :mname, :birthdate, :citizenship, :fromdate, :doctype, :docno, :sex,
        :postcode,  :city,  :community,  :street,  :houseno,  :localno,  :phone,
        :postcode1, :city1, :community1, :street1, :houseno1, :localno1, :phone1,
        :postcode2, :city2, :community2, :street2, :houseno2, :localno2, :phone2,
        :email2, :insurdate, :ascode, :education, :invalidity, :retired, :nfz,
        :dimnum, :dimden, :disability, :nfzname, :speccondition, :specfrom, :specto, :wpcode
  do
    suspend;

end^
SET TERM ; ^
