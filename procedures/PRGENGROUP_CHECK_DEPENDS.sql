--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRGENGROUP_CHECK_DEPENDS(
      PRSCHEDGUIDEDET integer,
      PRGENGROUP integer)
  returns (
      STATUS smallint)
   as
begin
  status = 0;
  if (prschedguidedet = 0) then prschedguidedet = null;
  if (exists (
    select first 1 1
      from prschedguidedets d
      where d.ssource = 1 and d.prgengroup = :prgengroup and d.out = 0 and d.accept < 2
        and (d.ref = :prschedguidedet or :prschedguidedet is null))
  ) then
    status = 1;
  else if (exists (
    select first 1 1
      from prschedguidedets d
      where d.ssource = 1 and d.prgengroup = :prgengroup and d.out = 1 and d.accept < 2
        and (d.ref = :prschedguidedet or :prschedguidedet is null))
  ) then
    status = 2;
end^
SET TERM ; ^
