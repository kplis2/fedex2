--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_PODSUM_VAT_GET(
      PERIODID varchar(6) CHARACTER SET UTF8                           ,
      BKPERIOD varchar(6) CHARACTER SET UTF8                           ,
      VREG varchar(10) CHARACTER SET UTF8                           ,
      ZAKRES smallint,
      ZPOPMIES smallint,
      TYP smallint,
      COMPANY integer,
      ZAKUP integer)
  returns (
      VATREG varchar(10) CHARACTER SET UTF8                           ,
      WARTBRU numeric(14,2),
      WARTNET numeric(14,2),
      WARTVAT numeric(14,2),
      WARTVATDEB numeric(14,2),
      WARTPOZ numeric(14,2),
      VATGR varchar(5) CHARACTER SET UTF8                           ,
      REGNAME varchar(60) CHARACTER SET UTF8                           ,
      SUMWARTBRU numeric(14,2),
      SUMWARTNET numeric(14,2),
      SUMWARTVAT numeric(14,2),
      SUMWARTVATDEB numeric(14,2),
      TAXGR varchar(10) CHARACTER SET UTF8                           ,
      DEBITED smallint)
   as
declare variable vatreg_pom varchar(10);
declare variable vatgr_pom varchar(5);
declare variable regname_pom varchar(60);
declare variable taxgr_pom varchar(10);
declare variable debited_pom smallint;
begin
if ((PERIODID = '' and bkperiod = '') or (PERIODID is null and bkperiod is null)) then exception period_not_defined;
VATREG_pom = '';
vatgr_pom = '';
regname_pom = '';
FOR SELECT VATGR,WARTBRU,WARTNET,WARTVAT,WARTVATDEB,WARTPOZ,REGNAME,VATREG, TAXGR
FROM RPT_FK_PODSUM_VAT(:PERIODID,:BKPERIOD,:VREG,:ZAKRES,:ZPOPMIES,:TYP,:COMPANY,:ZAKUP)
order by VATREG, vatgr, debited
INTO :VATGR_POM,:WARTBRU,:WARTNET,:WARTVAT,:WARTVATDEB, :WARTPOZ,:REGNAME_POM,:VATREG_POM, :TAXGR_POM
  do begin
  if(:VATREG is null and vatgr is null) then
  begin
    VATREG = VATREG_pom;
    vatgr = vatgr_pom;
    regname = regname_pom;
    taxgr = taxgr_pom;
    debited = :debited_pom;
    SUMWARTBRU = 0;
    SUMWARTNET = 0;
    SUMWARTVAT = 0;
    SUMWARTVATDEB = 0;
  end
  IF (:VATREG_POM <> :VATREG OR :TAXGR_POM <> :TAXGR) THEN
    begin
      suspend;
      VATREG = :VATREG_pom;
      vatgr = :vatgr_pom;
      regname= :regname_pom;
      taxgr = :taxgr_pom;
      debited = :debited_pom;
      SUMWARTBRU = 0;
      SUMWARTNET = 0;
      SUMWARTVAT = 0;
      SUMWARTVATDEB = 0;
    end else
    if (:vatgr_pom <> :vatgr ) then
      begin
        suspend;
        VATREG = :VATREG_pom;
        vatgr = :vatgr_pom;
        regname= regname_pom;
        taxgr = taxgr_pom;
        debited = :debited_pom;
        SUMWARTBRU = 0;
        SUMWARTNET = 0;
        SUMWARTVAT = 0;
        SUMWARTVATDEB = 0;
      end
      begin
        SUMWARTBRU = :SUMWARTBRU + WARTBRU;
        SUMWARTNET = :SUMWARTNET + WARTNET;
        SUMWARTVAT = :SUMWARTVAT + WARTVAT;
        SUMWARTVATDEB = :SUMWARTVATDEB + WARTVATDEB;
      end
  end
  suspend;
end^
SET TERM ; ^
