--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_TIMEREADCOMMENTS_INSERT(
      OPERATOR INTEGER_ID,
      DOKUMENT INTEGER_ID,
      POZYCJA INTEGER_ID,
      TYP SMALLINT_ID,
      SYMBOL STRING20,
      REF_IN INTEGER_ID = 0)
  returns (
      REF_OUT INTEGER_ID)
   as
begin
  if (coalesce(ref_in, 0) <> 0) then begin
    update X_MWS_TIMEREADCOMMENTS x
      set x.time_stop = current_timestamp(0)
      where x.ref = :ref_in;
    ref_out = :ref_in;
    suspend;
    exit;
  end else begin
    insert into X_MWS_TIMEREADCOMMENTS (DOKUMENT, TIME_START, OPERATOR, SYMBOL, POZYCJA, TYP)
    values (:DOKUMENT, current_timestamp(0), :OPERATOR, :SYMBOL, :POZYCJA, :TYP)
    returning REF
    into :ref_out;
    suspend;
    exit;
  end
end^
SET TERM ; ^
