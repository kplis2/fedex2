--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_DATES(
      DATAOD date,
      DATADO date)
  returns (
      DATA date,
      ROK varchar(4) CHARACTER SET UTF8                           ,
      OKRES varchar(6) CHARACTER SET UTF8                           ,
      TYDZIEN varchar(7) CHARACTER SET UTF8                           )
   as
declare variable r integer;
declare variable m integer;
declare variable w integer;
declare variable mies varchar(2);
declare variable tydz varchar(2);
begin
  if(:datado is null) then datado = current_date;
  if(:dataod is null) then dataod = '2008-01-01';
  data = :dataod;
  while(data<=:datado) do begin
    r = extract(year from :data);
    m = extract(month from :data);
    w = extract(week from :data);
    rok = cast (:r as varchar(4));
    if (:m < 10) then mies = '0'||:m;
    else mies = cast(:m as varchar(2));
    okres = :rok||:mies;
    if (:w < 10) then tydz = '0'||:w;
    else tydz = cast(:w as varchar(2));
    tydzien = :rok||'/'||:tydz;
    suspend;
    data = :data + 1;
  end
end^
SET TERM ; ^
