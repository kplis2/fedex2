--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PARSE_LINES(
      INSTRING varchar(255) CHARACTER SET UTF8                           ,
      DELIMITER char(10) CHARACTER SET UTF8                           )
  returns (
      OUTSTRING varchar(255) CHARACTER SET UTF8                           )
   as
declare variable i integer;
declare variable len integer;
declare variable buf varchar(255);
declare variable ch char(1);
begin
  buf = instring;
  len = coalesce(char_length(instring),0); -- [DG] XXX ZG119346
  outstring = '';
  while(:len > 0) do
  begin
    ch = substring(:buf from 1 for 1);
    if(:len > 1) then

      buf = substring(:buf from 2 for :len);
--    suspend;
    if(:ch = :delimiter) then begin

      suspend;
      outstring = '';
    end else
      outstring = :outstring || :ch;
    len = len - 1;
  end
  suspend;
end^
SET TERM ; ^
