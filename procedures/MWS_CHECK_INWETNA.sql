--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CHECK_INWETNA(
      WH varchar(3) CHARACTER SET UTF8                           ,
      AKTUOPERATOR integer)
  returns (
      INWENTA integer)
   as
begin
  select first 1 i.ref
    from inwenta i
    where i.magazyn = :wh and i.zamk = 0
    into inwenta;
  if (inwenta is null) then
    inwenta = 0;
end^
SET TERM ; ^
