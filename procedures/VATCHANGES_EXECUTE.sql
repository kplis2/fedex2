--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE VATCHANGES_EXECUTE(
      REF integer)
  returns (
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable n integer;
declare variable maskaktm varchar(255);
declare variable maskawersji varchar(255);
declare variable oldvat varchar(2);
declare variable newvat varchar(2);
declare variable filtr varchar(255);
declare variable ktm varchar(40);
declare variable expr varchar(1024);
begin
  n = 0;
  msg = 'Wystąpił błąd. Operacja przerwana.';
  select maskaktm,maskawersji,oldvat,newvat,filtr from vatchanges where ref=:ref
  into :maskaktm, :maskawersji, :oldvat, :newvat, :filtr;
  if(:oldvat is null or :oldvat='') then exception universal 'Nie podano starej stawki VAT!';
  if(:newvat is null or :newvat='') then exception universal 'Nie podano nowej stawki VAT!';
  expr = 'select ktm from towary t where (t.vatchanged is null) and (t.VAT='''||:oldvat||''') ';
  if(:maskaktm<>'') then expr = :expr || ' and (t.KTM like '''||:maskaktm||''') ';
  if(:filtr<>'') then expr = :expr || ' and ('||:filtr||')';
  for execute statement :expr into :ktm
  do begin
    update towary set vatchanged=:newvat where ktm=:ktm;
    n = :n + 1;
  end
  msg = 'Zaktualizowano '||:n||' towarów.';
  update vatchanges set status=1, applydate=current_timestamp(0) where ref=:ref;
end^
SET TERM ; ^
