--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SPRZEDGETPAR(
      REFDEF integer,
      REFSP integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           )
  returns (
      WARTOSC varchar(40) CHARACTER SET UTF8                           )
   as
begin
  WARTOSC=null;
  select WARTOSC from SPRZEDPAR
  where SPRZEDPAR.sprzedawca=:REFSP and SPRZEDPAR.sprzeddef=:REFDEF and
  SPRZEDPAR.symbol=:SYMBOL
  into :WARTOSC;
  if (WARTOSC is null ) then
      begin
         select WARTOSC from SPRZEDDEFPAR
         where  SPRZEDDEFPAR.sprzeddef=:REFDEF and SPRZEDDEFPAR.symbol=:SYMBOL
         into :WARTOSC;
      end

end^
SET TERM ; ^
