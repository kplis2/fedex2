--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XP_STAWKA(
      KEY1 integer,
      KEY2 integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      TYP varchar(1) CHARACTER SET UTF8                           )
  returns (
      RET numeric(16,6))
   as
DECLARE VARIABLE RATE NUMERIC(16,6);
DECLARE VARIABLE TIMERATE SMALLINT;
DECLARE VARIABLE PARREF INTEGER;
DECLARE VARIABLE PARVALUE NUMERIC(16,6);
DECLARE VARIABLE PARMODIFIED SMALLINT;
DECLARE VARIABLE PARAMNAME VARCHAR(255);
DECLARE VARIABLE EOPERTIME VARCHAR(255);
DECLARE VARIABLE SYMBOL VARCHAR(40);
DECLARE VARIABLE FROMPRSHMAT INTEGER;
DECLARE VARIABLE FROMPRSHOPER INTEGER;
DECLARE VARIABLE FROMPRSHTOOL INTEGER;
DECLARE VARIABLE DESCRIPT VARCHAR(255) ;
BEGIN
  -- TYP - O - stawka z operacji produkcyjnej
  --     - S - stawka stanowiska produkcyjnego
  RATE = 0;
  IF(:TYP IS NULL) THEN TYP = '';
  PARAMNAME='STAWKA'||:TYP;
  -- czy parametr jest uprzednio zapisany i zmodyfikowany
  PARREF = NULL;
  SELECT FROMPRSHMAT, FROMPRSHOPER, FROMPRSHTOOL FROM PRCALCCOLS WHERE REF=:KEY2
    INTO :FROMPRSHMAT, :FROMPRSHOPER, :FROMPRSHTOOL;
  SELECT FIRST 1 REF, CVALUE, MODIFIED
    FROM PRCALCCOLS WHERE PARENT=:KEY2 AND EXPR=:PARAMNAME AND CALCTYPE=1
    INTO :PARREF, :PARVALUE, :PARMODIFIED;
  IF(:PARREF IS NOT NULL AND :PARMODIFIED=1) THEN BEGIN
    RET = :PARVALUE;
    EXIT;
  END

  -- nalezy obliczyc na nowo
  IF(:FROMPRSHOPER IS NOT NULL) THEN
  BEGIN
    IF (:TYP = 'O') THEN
    BEGIN
      SELECT OPER, HOURRATE
        FROM PRSHOPERS
        WHERE PRSHOPERS.REF=:KEY1
        INTO :SYMBOL, :RATE;
      IF(:RATE IS NULL) THEN RATE = 0;
      IF(:SYMBOL IS NULL) THEN SYMBOL = '';
      IF(:RATE=0 AND :SYMBOL<>'') THEN
        SELECT HOURRATE, DESCRIPT FROM PROPERS WHERE SYMBOL=:SYMBOL INTO :RATE, :DESCRIPT;
      ELSE  
        SELECT DESCRIPT FROM PROPERS WHERE SYMBOL=:SYMBOL INTO :DESCRIPT;
      DESCRIPT = SUBSTRing('Stawka operacji "'||COALESCE(:DESCRIPT,:SYMBOL)||'"' from 1 for 255);
    END ELSE IF (:TYP = 'S') THEN
    BEGIN
      SELECT P.WORKPLACE, W.HOURRATE, W.DESCRIPT
        FROM PRSHOPERS P
          JOIN PRWORKPLACES W ON (P.WORKPLACE = W.SYMBOL)
        WHERE REF = :KEY1
        INTO :SYMBOL, :RATE, :DESCRIPT;
      IF (:RATE IS NULL) THEN RATE = 0;
      IF (:SYMBOL IS NULL) THEN SYMBOL = '';
      DESCRIPT = SUBSTRing('Stawka stanowiska "'||COALESCE(:DESCRIPT,:SYMBOL)||'"' from 1 for 255);
    END
  END ELSE IF(:FROMPRSHTOOL IS NOT NULL) THEN BEGIN
    SELECT PRSHTOOLS.SYMBOL, PRSHOPERS.EOPERTIME
      FROM PRSHTOOLS LEFT JOIN PRSHOPERS ON (PRSHTOOLS.OPERMASTER = PRSHOPERS.REF)
      WHERE PRSHTOOLS.REF=:FROMPRSHTOOL
    INTO :SYMBOL, :EOPERTIME;
    IF(:SYMBOL IS NOT NULL) THEN BEGIN
      SELECT PRTOOLS.PRICE, PRTOOLSTYPES.TIMERATE
        FROM PRTOOLS LEFT JOIN PRTOOLSTYPES ON (PRTOOLSTYPES.SYMBOL = PRTOOLS.TOOLTYPE)
        WHERE PRTOOLS.SYMBOL=:SYMBOL INTO :RATE, :TIMERATE;
    END
  END
  IF(:RATE IS NULL) THEN RATE = 0;

  RET = :RATE;
  IF (RET IS NULL) THEN RET = 0;
  -- nalezy zapisac wyliczony parametr
  IF(:PARREF IS NOT NULL) THEN BEGIN
    UPDATE PRCALCCOLS SET CVALUE=:RET WHERE REF=:PARREF;
  END ELSE BEGIN
    INSERT INTO PRCALCCOLS(PARENT,CVALUE,EXPR,DESCRIPT,CALCTYPE,PRCOLUMN)
    VALUES(:KEY2,:RET,:PARAMNAME,:DESCRIPT,1,NULL);
  END
END^
SET TERM ; ^
