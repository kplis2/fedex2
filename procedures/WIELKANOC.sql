--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WIELKANOC(
      Y integer)
  returns (
      WIELKANOC timestamp)
   as
declare variable a integer;
  declare variable b integer;
  declare variable c integer;
  declare variable d integer;
  declare variable e integer;
  declare variable f integer;
begin
  -- a = rok MOD 19
  a = y / 19;
  a = y - a * 19;
  -- b = rok MOD 4
  b = y / 4;
  b = y - b * 4;
  -- c = rok MOD 7
  c = y / 7;
  c = y - c*7;
  -- d = (a*19+AA) MOD 30
  d = (a * 19 + 24) / 30;
  d = (a * 19 + 24) - d * 30;
  -- e = (2*b+4*c+6*d+BB) MOD 7
  e = (2 * b + 4 * c + 6 * d + 5) / 7;
  e = (2 * b + 4 * c + 6 * d + 5) - e * 7;
  -- f = (11*AA+11) MOD 30
  f = (11 * 24 + 11) / 30;
  f = (11 * 24 + 11) - f * 30;
  if (d = 29 and e = 6 ) then
    wielkanoc = cast(y as varchar(4)) || '/04/26';
  else if (d = 28 and e = 6 and f < 19) then
    wielkanoc = cast(y as varchar(4)) || '/04/18';
  else
  begin
    wielkanoc = cast(y as varchar(4)) || '/03/22';
    wielkanoc = wielkanoc + (d + e);
  end
  suspend;
end^
SET TERM ; ^
