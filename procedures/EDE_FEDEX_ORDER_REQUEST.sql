--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_ORDER_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer,
      REF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable transportorder integer;
declare variable transportordersymb varchar(20);
declare variable shippingdocs blob_utf8;
declare variable grandparent integer;
declare variable courierno string30;
declare variable shipper spedytor_id;
declare variable accesscode string80;
declare variable filepath string255;
declare variable format string10;
begin
  select l.ref, l.symbol, l.spedytor
    from listywys l
    where l.ref = :oref
  into :transportorder, :transportordersymb, :shipper;

  --sprawdzanie czy istieje dokument spedycyjny
  if(:transportorder is null) then exception ede_fedex 'Brak listy spedycyjnej.';

  val = null;
  parent = null;
  params = null;
  id = 0;
  name = 'zapiszDokumentWydania';
  suspend;

  grandparent = :id;

  --dane autoryzacyjne
  select k.klucz, k.sciezkapliku, k.kurier
    from spedytkonta k
    where k.spedytor = :shipper
  into :accesscode, :filepath, :courierno;

  val = :accesscode;
  name = 'kodDostepu';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = :filepath;
  name = 'sciezkaPliku';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = 'PRODUCTION'; --jesli chcesz testowac zmien na TEST
  name = 'serviceUrlType';
  id = id + 1;
  parent = GrandParent;
  suspend;

  select symboldok from ededocsh where ref = :ref
  into :format;
  if (coalesce(:format,'') = '') then format = 'PDF';
  val = :format;
  name = 'format';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = :transportorder;
  name = 'zlecTransportowe';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = :transportordersymb;
  name = 'zlecTransportoweSymb';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  select list(substr(d.symbolsped,1,20), ';')
    from listywysd d
    where d.listawys = :transportorder
  into :shippingdocs;
  shippingdocs = :shippingdocs||';';

  val = :shippingdocs;
  name = 'numerPrzesylki';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = ';';
  name = 'separator';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = :courierno;
  name = 'kurierNumer';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  --jesli nie chcemy zapisywac do pliku to nalezy usunac to sekcje
  val = '1';
  name = 'zapiszDoPliku';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;
end^
SET TERM ; ^
