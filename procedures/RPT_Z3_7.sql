--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_Z3_7(
      EMPLOYEE integer,
      ONDATE date = null)
  returns (
      FROMDATE date,
      TODATE date,
      DAYS integer,
      CODE varchar(20) CHARACTER SET UTF8                           )
   as
declare variable TDATE date;
declare variable NDAYS integer;
declare variable NTODATE date;
begin

  tdate = cast ('1900-01-01' as date);
  ondate = coalesce(ondate, current_date);
  days = 0;
  for
    select E.fromdate, E.todate, Z.code, case when (E.todate = E.fromdate) then 1 else (E.todate - E.fromdate) end
      from eabsences E
      left join edictzuscodes Z on (Z.ref = E.scode)
      where E.employee = :employee
        and E.ecolumn in (90,100,110,120,140,150,270,280,290,350,360,370,440,450)
        and E.fbase = (select max(E1.fbase) from eabsences E1
                         where E1.ecolumn in (90,100,110,120,140,150,270,280,290,350,360,370,440,450)
                           and E1.employee = :employee
                           and E1.fromdate < :ondate
                           and E1.correction in (0,2))
        and E.correction in (0,2)
      order by E.fromdate
      into :fromdate, :todate, :code, :days
  do begin
    if (fromdate > :tdate) then
    begin
      while (exists (select E.fromdate, E.todate, Z.code
                from eabsences E
                left join edictzuscodes Z on (Z.ref = E.scode)
                where E.employee = :employee
                  and E.ecolumn in (90,100,110,120,140,150,270,280,290,350,360,370,440,450)
                  and E.fbase = (select max(E1.fbase) from eabsences E1
                                    where E1.ecolumn in (90,100,110,120,140,150,270,280,290,350,360,370,440,450)
                                      and E1.employee = :employee
                                      and E1.fromdate < :ondate
                                      and E1.correction in (0,2))
                  and E.fromdate = :todate + 1
                  and E.correction in (0,2)))
      do begin
        select E.todate, case when (E.todate = E.fromdate) then 1 else (E.todate - E.fromdate) end
          from eabsences E
          left join edictzuscodes Z on (Z.ref = E.scode)
          where E.employee = :employee
            and E.ecolumn in (90,100,110,120,140,150,270,280,290,350,360,370,440,450)
            and E.fbase = (select max(E1.fbase) from eabsences E1
                             where E1.ecolumn in (90,100,110,120,140,150,270,280,290,350,360,370,440,450)
                               and E1.employee = :employee
                               and E1.fromdate < :ondate
                               and E1.correction in (0,2))
            and E.correction in (0,2)
            and E.fromdate = :todate + 1
          into :ntodate, :ndays;
        days = days + ndays;
        todate = ntodate;
      end
      suspend;
      days = 0;
      tdate = todate;
    end
  end
end^
SET TERM ; ^
