--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_E_PAYSUMMARY_SUM(
      FROMCPER char(6) CHARACTER SET UTF8                           ,
      TOCPER char(6) CHARACTER SET UTF8                           ,
      FROMTPER char(6) CHARACTER SET UTF8                           ,
      TOTPER char(6) CHARACTER SET UTF8                           ,
      FROMIPER char(6) CHARACTER SET UTF8                           ,
      TOIPER char(6) CHARACTER SET UTF8                           ,
      BRANCH varchar(10) CHARACTER SET UTF8                           ,
      DEPT varchar(10) CHARACTER SET UTF8                           ,
      EMPLTYPE smallint,
      CURRENTCOMPANY integer)
  returns (
      NUM1 integer,
      NAME1 varchar(22) CHARACTER SET UTF8                           ,
      VAL1 numeric(14,2),
      REPSUM1 integer,
      NUM2 integer,
      NAME2 varchar(22) CHARACTER SET UTF8                           ,
      VAL2 numeric(14,2),
      REPSUM2 integer,
      NUM3 integer,
      NAME3 varchar(22) CHARACTER SET UTF8                           ,
      VAL3 numeric(14,2),
      REPSUM3 integer,
      NUM4 integer,
      NAME4 varchar(22) CHARACTER SET UTF8                           ,
      VAL4 numeric(14,2),
      REPSUM4 integer,
      NUM5 integer,
      NAME5 varchar(22) CHARACTER SET UTF8                           ,
      VAL5 numeric(14,2),
      REPSUM5 integer)
   as
begin
  if(:EmplType < 1) then EmplType = null;
  if(:branch = '') then branch = null;
  if(:dept = '') then dept = null;

  name1 = null;
  val1 = null;
  NUM1 = null;
  repsum1 = null;
  if (fromcper = '') then
    fromcper = null;
  if (tocper = '') then
    tocper = null;
  if (fromtper = '') then
    fromtper = null;
  if (totper = '') then
    totper = null;
  if (fromiper = '') then
    fromiper = null;
  if (toiper = '') then
    toiper = null;


  val2 = 0;
  select COALESCE(SUM(P.pvalue),0)
    from epayrolls ep
      join eprpos p on ep.ref = p.payroll
      join employees e on e.ref = p.employee
      join ecolumns c on c.number = p.ecolumn
      join eprempl r on ep.ref = r.epayroll and e.ref = r.employee
    where C.repsum > 0
      and ep.empltype = coalesce(:empltype,ep.empltype)
      and (r.branch = :branch or :branch is null)
      and (r.department = :dept or :dept is null)
      and e.company = :currentcompany
      and (ep.cper >= :fromcper or :fromcper is null)
      and (ep.cper <= :tocper or :tocper is null)
      and (ep.iper >= :fromiper or :fromiper is null)
      and (ep.iper <= :toiper or :toiper is null)
      and (ep.tper >= :fromtper or :fromtper is null)
      and (ep.tper <= :totper or :totper is null)
      and P.ecolumn = 4000
    group by C.number
    into val2;

  select C.number, C.name, C.repsum
    from ecolumns C
    where C.number = 4000
    into :num2, :name2, :repsum2;

  val3 = 0;
  select COALESCE(SUM(P.pvalue),0)
    from epayrolls ep
      join eprpos p on ep.ref = p.payroll
      join employees e on e.ref = p.employee
      join ecolumns c on c.number = p.ecolumn
      join eprempl r on ep.ref = r.epayroll and e.ref = r.employee
    where C.repsum > 0
      and ep.empltype = coalesce(:empltype,ep.empltype)
      and (r.branch = :branch or :branch is null)
      and (r.department = :dept or :dept is null)
      and e.company = :currentcompany
      and (ep.cper >= :fromcper or :fromcper is null)
      and (ep.cper <= :tocper or :tocper is null)
      and (ep.iper >= :fromiper or :fromiper is null)
      and (ep.iper <= :toiper or :toiper is null)
      and (ep.tper >= :fromtper or :fromtper is null)
      and (ep.tper <= :totper or :totper is null)
      and P.ecolumn = 5900
    group by C.number
    into val3;

  select C.number, C.name, C.repsum
    from ecolumns C
    where C.number = 5900
    into :num3, :name3, :repsum3;

  val4 = 0;
  select COALESCE(SUM(P.pvalue),0)
    from epayrolls ep
      join eprpos p on ep.ref = p.payroll
      join employees e on e.ref = p.employee
      join ecolumns c on c.number = p.ecolumn
      join eprempl r on ep.ref = r.epayroll and e.ref = r.employee
    where C.repsum > 0
      and ep.empltype = coalesce(:empltype,ep.empltype)
      and (r.branch = :branch or :branch is null)
      and (r.department = :dept or :dept is null)
      and e.company = :currentcompany
      and (ep.cper >= :fromcper or :fromcper is null)
      and (ep.cper <= :tocper or :tocper is null)
      and (ep.iper >= :fromiper or :fromiper is null)
      and (ep.iper <= :toiper or :toiper is null)
      and (ep.tper >= :fromtper or :fromtper is null)
      and (ep.tper <= :totper or :totper is null)
      and P.ecolumn = 8000
    group by C.number
    into val4;

  select C.number, C.name, C.repsum
    from ecolumns C
    where C.number = 8000
    into :num4, :name4, :repsum4;

  val5 = 0;
  select COALESCE(SUM(P.pvalue),0)
    from epayrolls ep
      join eprpos p on ep.ref = p.payroll
      join employees e on e.ref = p.employee
      join ecolumns c on c.number = p.ecolumn
      join eprempl r on ep.ref = r.epayroll and e.ref = r.employee
    where C.repsum > 0
      and ep.empltype = coalesce(:empltype,ep.empltype)
      and (r.branch = :branch or :branch is null)
      and (r.department = :dept or :dept is null)
      and e.company = :currentcompany
      and (ep.cper >= :fromcper or :fromcper is null)
      and (ep.cper <= :tocper or :tocper is null)
      and (ep.iper >= :fromiper or :fromiper is null)
      and (ep.iper <= :toiper or :toiper is null)
      and (ep.tper >= :fromtper or :fromtper is null)
      and (ep.tper <= :totper or :totper is null)
      and P.ecolumn = 9050
    group by C.number
    into val5;

  select C.number, C.name, C.repsum
    from ecolumns C
    where C.number = 9050
    into :num5, :name5, :repsum5;

  suspend;
end^
SET TERM ; ^
