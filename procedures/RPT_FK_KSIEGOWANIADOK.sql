--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_KSIEGOWANIADOK(
      ODDATY timestamp,
      DODATY timestamp,
      BKREG varchar(15) CHARACTER SET UTF8                           ,
      SUMRODZAJ smallint,
      ACCOUNTMASK ACCOUNT_ID,
      ALLPOZ smallint,
      COMPANY integer)
  returns (
      REF integer,
      DOCREF integer,
      DOCSYMBOL varchar(30) CHARACTER SET UTF8                           ,
      TRANSDATE timestamp,
      DOCREG varchar(10) CHARACTER SET UTF8                           ,
      NRLEDGER integer,
      NRREGISTER integer,
      LP integer,
      ACCOUNT ACCOUNT_ID,
      DEBIT numeric(15,2),
      CREDIT numeric(15,2),
      SETTLEMENT varchar(30) CHARACTER SET UTF8                           ,
      STLOPENDATE timestamp,
      STLPAYDATE timestamp,
      TOSUMMARY smallint)
   as
declare variable OLD_SYMBOL varchar(3);
declare variable ACCTYPE smallint;
declare variable I_YEARID integer;
declare variable OLD_ACCOUNT ACCOUNT_ID;
declare variable S_ACCOUNT ACCOUNT_ID;
declare variable N_DEBIT numeric(15,2);
declare variable N_CREDIT numeric(15,2);
declare variable I_REF integer;
declare variable S_SEP varchar(1);
declare variable I_DICTDEF integer;
declare variable I_LEN smallint;
declare variable S_TMP varchar(20);
declare variable OLD_PREFIX ACCOUNT_ID;
declare variable I_I smallint;
declare variable I_J smallint;
declare variable I_K smallint;
declare variable DICTREF integer;
declare variable R_ACCOUNT integer;
begin
  --TOFIX!!! styl - jaki to wydruk?
  ref = 0;
  old_symbol = '';
  old_account = '';
  old_prefix = '';
  nrledger = 0;
  nrregister = 0;
  execute procedure create_account_mask(:accountmask) returning_values :accountmask;
  if(:accountmask = '%') then accountmask = null;
  for select B.REF, b.TRANSDATE, max(b.symbol),max(b.number), max(b.bkreg)
    from BKDOCS B
    join DECREES D on (b.ref = d.bkdoc and b.status > 1)
    where
       ((:ACCOUNTMASK is null) or (:accountmask = '') or (D.account like :accountmask))
       and ((:BKREG is null) or (:bkreg = '') or (B.bkreg = :bkreg))
       and b.transdate >= :oddaty and b.transdate <= :dodaty
       and B.company = :company
    group by B.REF, B.transdate
    order by B.transdate, B.ref
    into :DOCREF, :transdate, :docsymbol, :nrregister,:docreg
  do begin
    for select d.number, D.account, d.debit, d.credit, d.settlement, d.opendate, d.payday, b.bktype
    from DECREES d
    join BKACCOUNTS b on (B.ref = d.accref)
    where d.bkdoc = :docref
       and ((:allpoz = 0) or (:ACCOUNTMASK is null) or (:accountmask = '') or (D.account like :accountmask))
    order by NUMBER
    into :lp,:account,:debit, :credit, :settlement, :stlopendate, :stlpaydate, :acctype
    do begin
      tosummary = 0;
      if(:sumrodzaj = 1 or (:acctype < 2))then tosummary = 1;
      ref =:ref+1;
      suspend;
    end
  end
end^
SET TERM ; ^
