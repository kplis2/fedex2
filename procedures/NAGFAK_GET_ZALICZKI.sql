--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_GET_ZALICZKI(
      REFNAGFAK integer)
  returns (
      REFK integer,
      SYMBFAK varchar(255) CHARACTER SET UTF8                           ,
      DATEFAK timestamp,
      VAT varchar(5) CHARACTER SET UTF8                           ,
      KWOTANET numeric(14,2),
      KWOTABRU numeric(14,2),
      KWOTANETZL numeric(14,2),
      KWOTABRUZL numeric(14,2))
   as
declare variable zaliczkowy integer;
begin
  for select fakturazal,symbfakzal,vat,wartnet,wartbru,wartnetzl,wartbruzl
    from nagfakzal
    where faktura=:refnagfak
    into :refk,:symbfak,:vat,:kwotanet,:kwotabru,:kwotanetzl,:kwotabruzl
  do begin
    select data from nagfak where ref=:refk into :datefak;
    suspend;
  end
end^
SET TERM ; ^
