--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_INWENTA_DIFF(
      INWENTA integer,
      WERSJAREF integer,
      DOSTAWA integer)
   as
declare variable zpartiami smallint;
declare variable ilinwroz numeric(14,4);
declare variable magazyn varchar(3);
declare variable tmpil numeric(14,4);
declare variable pilosc numeric(14,4);
declare variable pref integer;
declare variable cena numeric(14,4);
declare variable ktm varchar(40);
begin
  ilinwroz = 0;
  select coalesce(i.zpartiami,0), i.magazyn
    from inwenta i
    where i.ref = :inwenta
    into zpartiami, magazyn;
  if (zpartiami = 1) then
  begin
    update inwentap p set p.mwsilroz = 0 where p.inwenta = :inwenta and p.wersjaref = :wersjaref and p.dostawa = :dostawa;
    select -coalesce(sum(s.quantity),0)
      from mwsconstlocs c
        left join mwsstock s on (s.mwsconstloc = c.ref)
      where c.wh = :magazyn and c.locdest = 6 and s.vers = :wersjaref and s.lot = :dostawa
      into ilinwroz;
    if (ilinwroz <> 0) then
    begin
      for
        select p.ref, p.ktm, p.ilstan, p.cena
          from inwentap p
          where p.inwenta = :inwenta and p.wersjaref = :wersjaref and p.dostawa = :dostawa and p.cena is not null
          order by p.cena desc
          into pref, ktm, pilosc, cena
      do begin
        if (ilinwroz > 0) then
        begin
          update inwentap p set p.mwsilroz = :ilinwroz where p.ref = :pref and p.dostawa = :dostawa and p.cena = :cena;
          ilinwroz = 0;
        end else if (ilinwroz < 0) then
        begin
          if (pilosc <= abs(ilinwroz)) then
            tmpil = pilosc;
          else
            tmpil = abs(ilinwroz);
          update inwentap p set p.mwsilroz = -:tmpil where p.ref = :pref and p.dostawa = :dostawa and p.cena = :cena;
          ilinwroz = ilinwroz + tmpil;
        end
        if (ilinwroz = 0) then
          break;
        if (ilinwroz < 0) then
          exception mws_inwenta_error 'Brak pokrycia różnicy w stanie magazynowym: '||ktm;
      end
    end
  end else
  begin
    update inwentap p set p.mwsilroz = 0 where p.inwenta = :inwenta and p.wersjaref = :wersjaref;
    select -coalesce(sum(s.quantity),0)
      from mwsconstlocs c
        left join mwsstock s on (s.mwsconstloc = c.ref)
      where c.wh = :magazyn and c.locdest = 6 and s.vers = :wersjaref
      into ilinwroz;
    if (ilinwroz <> 0) then
    begin
      for
        select p.ref, p.ktm, p.ilstan
          from inwentap p
          where p.inwenta = :inwenta and p.wersjaref = :wersjaref
          into pref, ktm, pilosc
      do begin
        if (ilinwroz > 0) then
        begin
          update inwentap p set p.mwsilroz = :ilinwroz where p.ref = :pref;
          ilinwroz = 0;
        end else if (ilinwroz < 0) then
        begin
          if (pilosc <= abs(ilinwroz)) then
            tmpil = pilosc;
          else
            tmpil = abs(ilinwroz);
          update inwentap p set p.mwsilroz = -:tmpil where p.ref = :pref;
          ilinwroz = ilinwroz + tmpil;
        end
        if (ilinwroz = 0) then
          break;
        if (ilinwroz < 0) then
          exception mws_inwenta_error 'Brak pokrycia różnicy w stanie magazynowym: '||ktm;
      end
    end
  end
end^
SET TERM ; ^
