--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_SLOPOZ(
      SLODEF integer,
      MAXCOUNT integer)
  returns (
      KOD varchar(60) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      KODKS ANALYTIC_ID)
   as
DECLARE VARIABLE TYP VARCHAR(20);
DECLARE VARIABLE NUMER INTEGER;
begin
  if (maxcount = 0) then maxcount = null;
  numer = 0;
  kodks = '';
  select TYP from SLODEF where ref = :slodef into :typ;
  if (typ is null) then
    exit;

  if (typ = 'KLIENCI') then
    for
      select FSKROT, NAZWA, KONTOFK
        from KLIENCI
        order by kontofk
        into :kod,:nazwa,:kodks
    do begin
      numer = numer + 1;
      if (numer > maxcount) then
      begin
        kod = '___';
        nazwa = '...';
        kodks = '___';
        suspend;
        exit;
      end
      suspend;
    end
  else if (typ = 'DOSTAWCY') then
    for
      select ID, NAZWA, KONTOFK
        from DOSTAWCY
        order by kontofk
        into :kod,:nazwa,:kodks
    do begin
      numer = numer + 1;
      if (numer > maxcount) then
      begin
        kod = '...';
        nazwa = '...';
        kodks = '___';
        suspend;
        exit;
      end
      suspend;
    end
  else if (typ = 'SPRZEDAWCY') then
    for
      select SKROT,NAZWA,KONTOFK
        from SPRZEDAWCY
        order by kontofk
        into :kod,:nazwa,:kodks
    do begin
      numer = numer + 1;
      if (numer > maxcount) then
      begin
        kod = '...';
        nazwa = '...';
        kodks = '___';
        suspend;
        exit;
      end
      suspend;
    end
  else if (typ = 'VAT') then
    for
      select GRUPA, STAWKA
        from VAT
        order by grupa
        into :kod, :nazwa
    do begin
      kodks = kod;
      numer = numer + 1;
      if (numer > maxcount) then
      begin
        kod = '...';
        nazwa = '...';
        kodks = '___';
        suspend;
        exit;
      end
      suspend;
    end
  else if (typ = 'GRVAT') then
    for
      select symbol, descript
        from GRVAT
        order by symbol
        into :kod, :nazwa
    do begin
      kodks = kod;
      numer = numer + 1;
      if (numer > maxcount) then
      begin
        kod = '...';
        nazwa = '...';
        kodks = '___';
        suspend;
        exit;
      end
      suspend;
    end
  else if (typ = 'PKLIENCI') then
    for
      select SKROT, NAZWA
        from PKLIENCI
        order by skrot
        into :kod, :nazwa
    do begin
      kodks = kod;
      numer = numer + 1;
      if (numer > maxcount) then
      begin
        kod = '...';
        nazwa = '...';
        kodks = '___';
        suspend;
        exit;
      end
      suspend;
    end
  else if (typ = 'ODDZIALY') then
    for
      select ODDZIAL, NAZWA, SYMBOL
        from ODDZIALY
        order by ODDZIAL
        into :kod, :nazwa, :kodks
    do begin
      numer = numer + 1;
      if (numer > maxcount) then
      begin
        kod = '...';
        nazwa = '...';
        kodks = '___';
        suspend;
        exit;
      end
      suspend;
    end
  else if (typ = 'DEFMAGAZ') then
    for
      select SYMBOL, OPIS, BKSYMBOL
        from DEFMAGAZ
        order by ODDZIAL
        into :kod, :nazwa, :kodks
    do begin
      numer = numer + 1;
      if (numer > maxcount) then
      begin
        kod = '...';
        nazwa = '...';
        kodks = '___';
        suspend;
        exit;
      end
      suspend;
    end
  else if (typ = 'WALUTY') then
    for
      select SYMBOL, NAZWA from WALUTY
        order by symbol
        into :kod, :nazwa
    do begin
      kodks = kod;
      numer = numer + 1;
      if (:numer > :maxcount) then begin
        kod = '___';
        nazwa = '...';
        kodks = '___';
        suspend;
        exit;
      end
      suspend;
    end
  else
    for
      select KOD, NAZWA, KONTOKS
        from SLOPOZ
        where slownik = :slodef
        order by kontoks
        into :kod, :nazwa,:kodks
    do begin
      numer = numer + 1;
      if (numer > maxcount) then
      begin
        kod = '___';
        nazwa = '...';
        kodks = '___';
        suspend;
        exit;
      end
      suspend;
    end
end^
SET TERM ; ^
