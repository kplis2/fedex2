--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_GLS_PICKUPCREATE_RESPONSE(
      EDEDOCSH_REF INTEGER_ID)
  returns (
      OTABLE STRING20,
      OREF INTEGER_ID)
   as
declare variable SHIPPINGDOC INTEGER_ID;  
declare variable LABELFORMAT STRING10;
declare variable PACKAGEREF INTEGER_ID;  
declare variable symbolsped string40;
declare variable root_node INTEGER_ID;
declare variable PARENT INTEGER_ID;
begin
  otable = 'LISTYWYS';
  select oref
    from ededocsh
    where ref = :ededocsh_ref
    into :shippingdoc;

  oref = :shippingdoc;

  select p.id
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.name = 'adePickup_Create'
  into :root_node;

  select trim(p.val)
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.parent = :root_node
      and p.name = 'documentid'
  into :symbolsped;

  update  listywys lw
  set  lw.symbolsped = :symbolsped, lw.statussped = 1
    where lw.ref is not distinct from :shippingdoc;

  suspend;
end^
SET TERM ; ^
