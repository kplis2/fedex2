--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRODUCTION_CAPACITY_CALC(
      MODE smallint,
      PERIODS integer,
      PRMACHINE integer)
   as
declare variable timetmp timestamp;
declare variable mintime timestamp;
declare variable maxtime timestamp;
declare variable interval double precision;
declare variable i integer;
declare variable timestart timestamp;
declare variable timestop timestamp;
declare variable avaible numeric(14,4);
declare variable used numeric(14,4);
declare variable minused numeric(14,4);
declare variable maxused numeric(14,4);
declare variable percentuse numeric(14,4);
begin
  if (prmachine is null) then prmachine = 0;
  if (exists (select first 1 1 from rdb$procedures where RDB$PROCEDURE_NAME ='XK_PRODUCTION_CAPACITY_CALC')) then
    execute statement 'execute procedure XK_PRODUCTION_CAPACITY_CALC('||:mode||','||:periods||','||:prmachine||')';
  else
  begin
    if (prmachine = 0) then
      delete from prgantttimes where mode = 1;
    else
      delete from prgantttimes where mode = 2 and prmachine = :prmachine;
    if (periods is null or periods = 0) then
      periods = 100;
    select count(distinct g.prmachine)
      from prgantt g
      where g.prmachine is not null
        and (:prmachine = 0 or g.prmachine = :prmachine)
      into avaible;
    timestart = null;
    timestop = null;
    for
      select t.prgantttime
        from PRGANTT_TIMES (:prmachine) t
        group by t.prgantttime
        order by t.prgantttime
        into timetmp
    do begin
      if (timestart is null and timestop is null) then
        timestart = timetmp;
      else if (timestop is null and timestart is not null) then
        timestop = timetmp;
      else
      begin
        timestart = timestop;
        timestop = timetmp;
      end
      if (timestart is not null and timestop is not null and timestart <> timestop) then
        insert into prgantttimes (timefrom, timeto, avaible, used , percentuse, mode, prmachine)
          values (:timestart, :timestop, :avaible, :used, :percentuse, 0, :prmachine);
      used = 0;
      percentuse = 0;
      select count(distinct g.prmachine)
        from prgantt g
        where g.timefrom <= :timetmp and g.timeto >= :timetmp
          and (:prmachine = 0 or g.prmachine = :prmachine)
        into used;
      if (avaible > 0) then
        percentuse = used/avaible;
      else
        percentuse = 1;
      percentuse = percentuse * 100;
    end
    if (mode in (1,2)) then
    begin
      select min(p.timefrom), max(p.timeto)
        from prgantt p
        where (:prmachine = 0 or p.prmachine = :prmachine)
        into mintime, maxtime;
      interval = (maxtime - mintime)/cast(periods as double precision);
      i = 1;
      while (i < periods)
      do begin
        maxtime = mintime + interval;
        select max(t.used), max(t.percentuse)
          from prgantttimes t
          where t.mode = 0 and t.prmachine = :prmachine and
            ((t.timefrom <= :mintime and t.timeto >= :maxtime)
              or (t.timefrom <= :mintime and t.timeto >= :mintime)
              or (t.timefrom <= :maxtime and t.timeto >= :maxtime)
            )
          into used, percentuse;
        insert into prgantttimes (timefrom, timeto, avaible, used , percentuse, mode, prmachine)
          values (:mintime, :maxtime, :avaible, :used, :percentuse, :mode, :prmachine);
        i = i + 1;
        mintime = maxtime;
      end
    end
  end
end^
SET TERM ; ^
