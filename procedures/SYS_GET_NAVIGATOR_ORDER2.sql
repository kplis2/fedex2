--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_GET_NAVIGATOR_ORDER2(
      NAMESPACE varchar(40) CHARACTER SET UTF8                           ,
      ITEMREF varchar(255) CHARACTER SET UTF8                            = null,
      ONLYIDENT varchar(255) CHARACTER SET UTF8                            = null)
  returns (
      REF varchar(255) CHARACTER SET UTF8                           ,
      PARENT varchar(255) CHARACTER SET UTF8                           ,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      SIGNATURE varchar(8) CHARACTER SET UTF8                           ,
      NUMBER smallint)
   as
begin

  -- pobranie rodziców
  for select DISTINCT
    s.PARENT as "REF",
    null as "PARENT",
    null as "NAME",
    null as "SIGNATURE",
    null as "NUMBER2"
  from SYS_GET_NAVIGATOR_ORDER(:namespace,:itemref,:onlyident) s
  group by s.Parent
  into :ref, :parent, :name, :signature, :number
  do begin
    name = :ref;

    select s.val from s_appini s
    where s.section = 'NavigatorTab:'||:name and
          s.ident = 'SIGNATURE'
    into :signature;

    select s.val from s_appini s
    where s.section = 'NavigatorTab:'||:name and
          s.ident = 'NUMBER2'
    into :number;

    suspend;
  end

  --pobranie dzieci
  for select DISTINCT
    s.ref,
    s.parent,
    null as "NAME",
    null as "SIGNATURE",
    null as "NUMBER2"
  from SYS_GET_NAVIGATOR_ORDER(:namespace,:itemref,:onlyident) s
  into :ref, :parent, :name, :signature, :number
  do begin
    name = replace(:ref, 'child_','');

    select s.val from s_appini s
    where s.section = 'NavigatorGroup:'||:parent||'_'||:name and
          s.ident = 'SIGNATURE'
    into :signature;

    select s.val from s_appini s
    where s.section = 'NavigatorGroup:'||:parent||'_'||:name and
          s.ident = 'NUMBER2'
    into :number;

    suspend;
  end
end^
SET TERM ; ^
