--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SET_STATISTICS_INDEX as
declare variable iname varchar(255);
begin
  -- DU (06/09/2004) - procedura nalicza selectivity
  -- dla wszystkich indeksów
  for
    select RDB$INDEX_NAME
      from RDB$INDICES
      into :iname
  do begin
    execute statement 'set statistics index ' || iname;
  end
end^
SET TERM ; ^
