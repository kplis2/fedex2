--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_CLOSE_PERIOD(
      COMPANY integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           )
   as
declare variable i integer;
declare variable fdate timestamp;
declare variable id varchar(6);
declare variable wartosc smallint;
declare variable ref integer;
declare variable vstatus smallint;
begin
  if (exists(select ref from bkdocs where period = :period and status < 3 and company = :company)) then
    exception FK_EXPT 'Niemożliwe zamknięcie miesiąca '|| + period || '.\nIstnieją nie zaksięgowane dokumenty!';

  select FDATE from BKPERIODS where company = :company and  id=:period
    into :fdate;

  select k.wartosc from konfig k where k.akronim = 'ROZRACHZAMK'
    into :wartosc;
 -- wartosc = 0 zamykanie okresu do datazamk, 1 - zamykanie okresu gdy saldo=0

  if (coalesce(:wartosc,'0') = '0') then
      update ROZRACH set ZAMK = 1 where DATAZAMK <= :fdate and company = :company  and coalesce(zamk,0) = 0;

  update BKPERIODS set STATUS = 2 where company = :company and  id = :period;

  for
    select first 3 id
      from bkperiods
      where company = :company
        and id > :period
      order by id
      into :id
    do begin
        update bkperiods set status = 1 where company = :company and id = :id and status = 0;
    end
end^
SET TERM ; ^
