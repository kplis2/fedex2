--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_ABSENCE_PERIODS_BASES(
      EMPLOYEE integer,
      ECOLUMN integer,
      FORDATE date,
      EABSREF integer = 0,
      CORRECTEDEABS integer = 0)
  returns (
      FBASE varchar(6) CHARACTER SET UTF8                           ,
      LBASE varchar(6) CHARACTER SET UTF8                           ,
      BASEMONTHS smallint,
      BASEABSENCE integer,
      FIRSTABSENCE integer)
   as
declare variable TODATE date;
declare variable ABSENCE integer;
declare variable BASETYPE varchar(1);
declare variable CHPERIOD varchar(6);
declare variable EFROMDATE date;
declare variable MINPERIOD varchar(6);
declare variable I integer;
declare variable J integer;
declare variable VWORKEDHOURS numeric(14,2);
declare variable RECOUNTBASEMONTH integer;
declare variable CORRECTION smallint;
declare variable CALCPAYBASE smallint;
declare variable ISRECORD smallint;
declare variable WORKEDTIME integer;
declare variable NOMINALTIME integer;
declare variable PFROMDATE date;
declare variable PTODATE date;
declare variable STDCALENDAR integer;
begin
/*MWr: Personel - procedura okresla zakres podstaw dla niebecnosci N zdefiniowanej
    poprzez: jej rodzaj ECOLUMN, przynaleznosc do pracownika EMPLOYEE, jej
    poczatek w dniu FORDATE.

  > Parametr EABSREF okresla Ref nieobecnosci, ktora nie bedzie brana pod uwage
    przy badaniu ciaglosci. W kodzie C++ jest to Ref modyfikowanej nieobecnosci.
    Pozwala obsluzyc przypadek, gdy na modyfikowanej nieobecnosci zwiekszeniu
    ulega FromDate. Bez wykluczenia EABSREF pobrana by byla zawsze informacja
    o ciaglosci z nieobecnosci, ktora modyfikujemy, czyli z EABSREF.
  > Jezeli N jest korekta to podanie CORRECTEDEABS(odbywa sie z poziomu kodu)
    pozwala pobrac informacje o ciaglosci z nieobecnoci skorygowanej*/

  recountbasemonth = 1; --czy przeliczyc liczbe miesiecy dla wyliczonych F-Lbase?
  eabsref = coalesce(eabsref,0);
  correctedeabs = coalesce(correctedeabs,0);

--okreslenie typu i liczby miesiecy do podstaway na podstawie parametrow skl.
  select cp1.pval, cp2.pval
    from ecolumns ec
      left join ecolparams cp1 on (cp1.ecolumn = ec.number and cp1.param = 'BASECOUNT')
      left join ecolparams cp2 on (cp2.ecolumn = ec.number and cp2.param = 'BASETYPE')
    where ec.number = :ecolumn
   into :basemonths, :basetype;

--przypisanie domyslne typu podstawy w przypadku braku konfiguracji
  if (basetype is null) then
    if (ecolumn in (40, 50, 60, 90, 100, 110, 120, 140, 150, 160, 170, 270, 280, 290, 350, 360, 370, 440, 450)) then
      basetype = 'S';
    else if (ecolumn in (220, 230, 310)) then
      basetype = 'V';

--przypisanie domyslne liczby miesiecy do podstawy dla braku konfiguracji
  basemonths = coalesce(basemonths,0);
  if (basemonths = 0 and basetype is not null) then
    if (basetype = 'S') then
      basemonths = 12;
    else if (ecolumn <> 310) then   --31: nieobecnosc usprawiedliwiona platna
      basemonths = 3;

  if (basetype > '') then
  begin
    --zmiana warunkow umowy (etatu)
    select first 1 fromdate
      from e_get_employmentchanges(:employee, null, :fordate - 1, ';WORKDIM;')
      order by fromdate desc
      into :efromdate;
  
    if (efromdate is not null) then
      execute procedure e_func_periodinc(null, 0, :efromdate)
        returning_values :chperiod;
  
  --okreslenie ciaglosci nieobecnosci chorobowej Czesc_1/2 ---------------------
    if (basetype = 'S') then
    begin
      select first 1 a.ref, a.todate, a.correction, a.corrected, a.calcpaybase,
          fbase, lbase, baseabsence, firstabsence
        from eabsences a
          join ecolparams c on (c.ecolumn = a.ecolumn)
        where a.employee = :employee and a.todate < :fordate
          and a.todate >= :efromdate
          and c.param = 'ABSTYPE' and c.pval = 1
          and a.correction in (0,2)
          and a.ref <> :eabsref
        order by a.todate desc
        into :absence, :todate, :correction, :correctedeabs, :calcpaybase,
          :fbase, :lbase, :baseabsence, :firstabsence;
    end else
  
  --okreslenie ciaglosci nieobecnosci urlopowej Czesc_1/2 ------------------------
    if (basetype = 'V') then
    begin
      select first 1 ref, correction, corrected, calcpaybase,
          fbase, lbase, baseabsence, firstabsence
        from eabsences
        where employee = :employee and todate = (:fordate - 1)
          and lbase <> '' --BS55666
          and ecolumn in (220, 230)
          and correction in (0,2)
          and ref <> :eabsref
        order by todate desc
        into :absence, :correction, :correctedeabs, :calcpaybase,
          :fbase, :lbase, :baseabsence, :firstabsence;
     end

  --jezeli mamy do czynienia z korektami, trzeba sprawdzic odwolanie do oryginalnych nieobecnosci (BS20435)
  --jezeli mamy przeliczenie podstaw, to nie odwoluj sie do n. skorygowanej
    if (absence is null and coalesce(calcpaybase,0) = 0 and
       (correction = 2 or correctedeabs > 0)) then
    begin
    --nie znaleziono nieobecnosci wczesniejszej, a biezaca nieobenosc stanowi korekte
    --dane pobierzemy wiec z nieobecnosci skorygowanej, o ile jest tego samego typu
      select a.ref, a.todate, fbase, lbase, baseabsence, firstabsence
        from eabsences a
          join ecolparams c on (c.ecolumn = a.ecolumn and c.param = 'BASETYPE' and c.pval = :basetype) --BS55666
        where a.ref = :correctedeabs
        into :absence, :todate, :fbase, :lbase, :baseabsence, :firstabsence;
    end

  --prosty test na niepusty/poprawny zakres podstaw fbase-lbase z absence (BS57024)
    if (absence is not null and coalesce(char_length(trim(lbase) || trim(fbase)),0) <> 12) then -- [DG] XXX ZG119346
      absence = null;
  
  --okreslenie ciaglosci nieobecnosci chorobowej Czesc_2/2 -----------------------
    if (:basetype = 'S') then
    begin
      if (absence is null or ((extract(year from fordate) * 12 + extract(month from fordate) - 3
        > extract(year from todate) * 12 + extract(month from todate)))) then
      begin
        if (absence is not null) then
          absence = null;  --oznaczenie, ze brak ciaglosci
  
        execute procedure e_func_periodinc(null, -1, :fordate)
          returning_values lbase;
  
        if (lbase < chperiod) then
          lbase = chperiod;
  
        execute procedure e_func_periodinc(:lbase, 1 - :basemonths)
          returning_values fbase;
  
        if (fbase < chperiod) then
          fbase = chperiod;
  
        --gdy choroba wystepuje niedlugo po zatrudnieniu (BS33364)
        if (fbase = lbase and extract(day from efromdate) <> 1
          and extract(month from efromdate) >= cast(substring(fbase from 5 for 6) as integer)
        ) then begin

          execute procedure period2dates(fbase)
            returning_values pfromdate, ptodate;

          execute procedure get_config('STDCALENDAR',2)
            returning_values :stdcalendar;

          execute procedure emplcaldays_store('SUM', :employee, maxvalue(coalesce(:efromdate,:pfromdate), :pfromdate), :todate)
            returning_values :workedtime;

          execute procedure ecaldays_store('SUM', :stdcalendar, :pfromdate, :ptodate)
            returning_values :nominaltime;

          if (workedtime is distinct from nominaltime) then  --BS102954
          begin
            execute procedure e_func_periodinc(null, 0, :fordate)
              returning_values fbase;
            lbase = fbase;
          end
        end
      end
    end else
  
  --okreslenie ciaglosci nieobecnosci urlopowej Czesc_2/2 ------------------------
    if (:basetype = 'V') then
    begin
      if (absence is null) then
      begin
        select min(period)
          from eabsemplbases
          where employee = :employee
          into :minperiod;
  
        execute procedure e_func_periodinc(null, -1, :fordate)
          returning_values lbase;
  
        if (lbase < chperiod) then
          lbase = chperiod;
  
        isrecord = 0; --do obslugi przypadku braku w tab.podstaw ostatniego miesiaca do podstawy (BS28786)
        j = 1;  --po wyjsciu z petli (:j-1) przedstawia wyliczona wartosc BaseMonths
        i = 0;
        fbase = lbase;
        while (i < :basemonths and fbase > :minperiod) do
        begin
          execute procedure e_func_periodinc(:lbase, 1 - :j)
            returning_values fbase;
  
          vworkedhours = null;
          select 1, vworkedhours
            from eabsemplbases
            where employee = :employee
              and period = :fbase
            into :isrecord, :vworkedhours;
  
          if (vworkedhours > 0 or (vworkedhours is null and isrecord = 0)) then
            i = i + 1;
  
          j = j + 1;
        end
  
        if (j - 1 < basemonths ) then --BS25752
        begin
        --jezeli zakres podstaw jest mnniejszy niz domyslny to ustaw domyslny
          recountbasemonth = 0;
          execute procedure e_func_periodinc(:lbase, 1 - :basemonths)
            returning_values fbase;
        end
      end
    end
  end

--gdy nie stwierdzono ciaglosci ------------------------------------------------
  if (absence is null) then
  begin
    baseabsence = 0;
    firstabsence = 0;
  end

--automatyczne przeliczenie liczby miesiecy-------------------------------------
  if (recountbasemonth = 1) then
  begin
    basemonths = 12 * cast(substring(lbase from 1 for 4) as integer)  +  cast(substring(lbase from 5 for 2) as integer)
               - 12 * cast(substring(fbase from 1 for 4) as integer)  -  cast(substring(fbase from 5 for 2) as integer) + 1;
  end

  suspend;
end^
SET TERM ; ^
