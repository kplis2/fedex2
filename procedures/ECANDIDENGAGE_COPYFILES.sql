--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ECANDIDENGAGE_COPYFILES(
      CANDIDATE integer,
      PERSON integer)
   as
begin
/*MWr Personel: Procedura wywolywana jest z modulu rekrutacji, przy zatrudnieniu
  kandydata CANDIDATE, gdy kandytat zostal juz skojarzony z karta osobowa PERSON.
  Kopiuje dane o zalacznikach elektronicznych z rekrutacji do kartoteki osobowej*/

  insert into personfiles (person, ftype, filename, name, descript)
    select :person, f.ftype, f.filename, f.name, f.descript
      from ecandidfiles f
      left join personfiles p on (p.person = :person
                                and coalesce(p.ftype,0) =  coalesce(f.ftype,0)
                                and coalesce(p.filename,'') = coalesce(f.filename,'')
                                and coalesce(p.name,'') = coalesce(f.name,'')
                                and coalesce(p.descript,'') = coalesce(f.descript,''))
    where f.ecandidate = :candidate
      and p.ref is null; --zabezpieczenie przed dublowaniem wpisow, gdy operacje zatrudnienia sie powtarza
end^
SET TERM ; ^
