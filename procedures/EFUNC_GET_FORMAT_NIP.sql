--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EFUNC_GET_FORMAT_NIP(
      NIPIN varchar(30) CHARACTER SET UTF8                           )
  returns (
      NIP varchar(15) CHARACTER SET UTF8                           )
   as
declare variable I smallint;
declare variable TSIGN varchar(1);
declare variable NIP_TMP varchar(30);
begin
/*MWr - Personel: procedura pobiera zadany nip NIPIN i formatuje go do wymaganego
        przez system SENTE Personel schamtu: 13 cyfr w formacie nnn-nnn-nn-nn*/

  nip = '';
  nipin = coalesce(nipin, '');
  if (nipin <> '') then
  begin
    nipin = trim(nipin);
    nip_tmp = '';
    i = 1;
    while (i <= 15) do
    begin
      tsign = substring(nipin from i for 1)  ;
      if (tsign <> '' and tsign <> '-' ) then
        nip_tmp = nip_tmp || tsign;
      if (coalesce(char_length(nip_tmp),0) in (3, 7, 10) and substring(nip_tmp from (coalesce(char_length(nip_tmp),0)) for 1) <> '-') then -- [DG] XXX ZG119346
        nip_tmp = nip_tmp || '-';
      i = i + 1;
    end
    nip = substring(nip_tmp from  1 for  15);
  end
  suspend;
end^
SET TERM ; ^
