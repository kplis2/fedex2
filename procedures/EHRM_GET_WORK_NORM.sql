--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EHRM_GET_WORK_NORM(
      FROMDATE TIMESTAMP_ID,
      TODATE TIMESTAMP_ID,
      EMPLOYEEREF EMPLOYEES_ID)
  returns (
      NORM INTEGER_ID)
   as
declare variable days type of integer_id;
declare variable seconds type of integer_id;
begin
    select WD, WS from ECAL_WORK(:employeeref,:fromdate,:todate)
    into :days, :seconds;

    if(:days = 0 or :seconds = 0) then
        norm = 0;
    else
        norm = (:seconds / 3600) / :days;
    suspend;
end^
SET TERM ; ^
