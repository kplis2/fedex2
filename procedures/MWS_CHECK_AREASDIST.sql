--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CHECK_AREASDIST(
      WHAREALOGB integer,
      WHAREALOGE integer)
  returns (
      DIST numeric(14,4))
   as
begin
  if (wharealogb is not null and wharealoge is not null
      and wharealogb <= wharealoge) then
  begin
    select distance
      from whareasdists
      where whareab = :wharealogb and whareae = :wharealoge
      into dist;
  end else if (wharealogb is not null and wharealoge is not null
      and wharealogb >= wharealoge) then
  begin
    select distance
      from whareasdists
      where whareab = :wharealoge and whareae = :wharealogb
      into dist;
  end
end^
SET TERM ; ^
