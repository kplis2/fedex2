--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FIX_SET_ALL_GRANTS as
begin
  update rdb$procedures set rdb$procedure_name = upper(rdb$procedure_name) where rdb$procedure_name similar to '%[a-z]%';
  update rdb$procedure_parameters set rdb$procedure_name = upper(rdb$procedure_name) where rdb$procedure_name similar to '%[a-z]%';
  update RDB$TRIGGERS set rdb$trigger_name = upper(rdb$trigger_name) where rdb$trigger_name similar to '%[a-z]%';
  update rdb$relations set rdb$relation_name = upper(rdb$relation_name) where rdb$relation_name similar to '%[a-z]%';
  update rdb$relation_fields set rdb$relation_name = upper(rdb$relation_name) where rdb$relation_name similar to '%[a-z]%';
end^
SET TERM ; ^
