--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RFISKAL(
      REF integer)
  returns (
      STATUS integer,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable akc smallint;
declare variable fiskal smallint;
begin
  status = 1;
  select akceptacja, fiskalny from nagfak left join typfak on (typfak.symbol = nagfak.typ)
  where nagfak.ref = :ref
  into :akc, :fiskal;
  if (akc <> 1) then
  begin
    status = 0;
    msg = 'Dokument niezaakceptowany fiskalizacja niemożliwa!';
  end
  if (fiskal <> 1) then
  begin
    status = 0;
    msg = 'Dokument nie podlega fiskalizacji!';
  end
  suspend;
end^
SET TERM ; ^
