--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_SETTLEMENTS_STRUCTURE_D(
      PACCOUNT ACCOUNT_ID,
      P1 numeric(14,2),
      P2 numeric(14,2),
      P3 numeric(14,2),
      P4 numeric(14,2),
      D1 varchar(12) CHARACTER SET UTF8                           )
  returns (
      ACCOUNT ACCOUNT_ID,
      NAME varchar(80) CHARACTER SET UTF8                           ,
      VAL1 numeric(14,2),
      VAL2 numeric(14,2),
      VAL3 numeric(14,2),
      VAL4 numeric(14,2),
      VAL5 numeric(14,2),
      VAL6 numeric(14,2),
      SUMVAL numeric(14,2),
      D timestamp)
   as
declare variable val numeric(14,2);
  declare variable paydate timestamp;
  declare variable dictdef integer;
  declare variable dictpos integer;
begin
    d=cast(d1 as timestamp);
  for
    select kontofk, winienzl-mazl, dataplat, slodef, slopoz
    from rozrach
    where winienzl-mazl<>0 and kontofk like :paccount || '%'
    and rozrach.dataotw<=:D
    order by kontofk
    into :account, :val, :paydate, :dictdef, :dictpos
  do begin
  val1 = 0;
  val2 = 0;
  val3 = 0;
  val4 = 0;
  val5 = 0;
  val6 = 0;
  sumval = 0;
  name = '';
  if (dictdef is not null and dictpos is not null) then
    execute procedure name_from_dictposref(dictdef, dictpos)
      returning_values name;
  if (current_timestamp(0)-paydate <=0) then
    val1 = val;
  else if (D-paydate>0 and D-paydate<=p1) then
    val2 = val;
  else if (D-paydate>p1 and D-paydate<=p2) then
    val3 = val;
  else if (D-paydate>p2 and D-paydate<=p3) then
    val4 = val;
  else if (D-paydate>p3 and D-paydate<=p4) then
    val5 = val;
  else if (D-paydate>p4) then
    val6 = val;
  sumval = val;
  suspend;
  end
end^
SET TERM ; ^
