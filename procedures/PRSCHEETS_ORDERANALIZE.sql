--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEETS_ORDERANALIZE(
      PROPER integer,
      NUM integer)
  returns (
      NEXTNUM integer)
   as
declare variable suboper integer;
begin
  for select ref from prshopers where OPERMASTER = :PROPER order by number
  into :suboper
  do begin
    execute procedure prscheets_orderanalize(:suboper,:num) returning_values :num;
  end
  update prshopers set prshopers.ordanalize = :num where ref=:proper;
  nextnum = num + 1;
end^
SET TERM ; ^
