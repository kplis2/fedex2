--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDULE_FIND_MACHINE_TORIGHT(
      PRGANTT integer,
      TEND timestamp,
      TRYB smallint)
  returns (
      STARTTIME timestamp,
      ENDTIME timestamp,
      MACHINE integer)
   as
declare variable defmachine integer;
declare variable workplace varchar(20);
declare variable schedule integer;
declare variable schedzamnum integer;
declare variable gapfound smallint;
declare variable localtime timestamp;
declare variable schedzam integer;
declare variable worktime double precision;
declare variable cnt integer;
declare variable minspace integer;
declare variable minlength double precision;
declare variable prdepart varchar(20);
declare variable tmpendtime timestamp;
declare variable s1 timestamp;
declare variable s2 timestamp;
declare variable s3 timestamp;
declare variable m1 integer;
begin
  select g.prmachine, g.worktime, g.workplace, p.mintimebetweenopers, p.prdepart
    from prgantt g
      left join prschedules p on (p.ref = g.prschedule)
    where g.ref = :prgantt
    into :machine, :worktime, :workplace, :minspace, :prdepart;

  minlength = 1440;
  minlength = coalesce(minspace,0)/:minlength;
  --minlength = 1/:minlength;
  --minlength = :minlength * :minspace;

  starttime = :tend;
  endtime = :tend;
  defmachine = :machine;
  --sprawdzenie, czy maszyna juz jest przypisana

  for
    select p.rmachine, p.rstart, p.rend
      from prschedule_free_machine_tr(:workplace, :machine, :tryb) p
      where p.worktime >= :worktime and p.rstart <= :tend - :worktime
      order by coalesce(p.rend,cast(current_date + 365 as timestamp)) desc
      into :m1, :s1, :s2
  do begin
    if (s2 is null) then
    begin
      if (s1 + worktime <= tend) then
      begin
        s2 = tend;
        s1 = s2 - worktime;
      end
    end else if (s2 > tend) then
      s2 = tend;
    --przesuniecie konca na dzien pracujacy
    execute procedure prsched_calendar_fw_tr(:prdepart,cast(:s2 as timestamp), :prgantt)
      returning_values :s3;

    if (cast(:s3 - :s1 as double precision) - :minlength >= :worktime) then
    begin
      -- przesuniecie poczatku w zwiazku z wolnym przypadajcym w czasie produkcji
      execute procedure pr_calendar_checkendtime_tr(:prdepart, :s3, :worktime)
        returning_values :tmpendtime;
      if (:tmpendtime <= :s3 ) then
      begin
        s1 = :tmpendtime;
      end
    end
    endtime = s3;
    starttime = s1;
    machine = m1;
    suspend;
  end
end^
SET TERM ; ^
