--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ERECRUITSAPP_FILEINS(
      REF INTEGER_ID,
      ECANDIDATE ECANDIDATES_ID,
      FILENAME BITMAPA,
      NAME STRING60,
      USERFILENAME BITMAPA = null,
      USERFILEEXT STRING20 = null,
      ERECCANDREF ERECCANDS_ID = null)
  returns (
      REFOUT ECANDIDFILES_ID)
   as
begin
  if (REF is null) then
  begin
    REFOUT = next value for GEN_ECANDIDFILES;
  end else
  begin
    insert into ECANDIDFILES (REF, ECANDIDATE, FILENAME, NAME, ERECCAND)
      values (:REF, :ECANDIDATE, :FILENAME, :NAME, :ERECCANDREF)
      returning REF into :REFOUT;
    insert into S_BINARY (LASTTIME, FILEDBASE, FILETABLE, FILEFIELD, FILEREF, EXT, FILENAME)
      values (current_timestamp(0), 'esystem', 'ECANDIDFILES', 'FILENAME', :REF, :USERFILEEXT, :USERFILENAME);
  end
  suspend;
end^
SET TERM ; ^
