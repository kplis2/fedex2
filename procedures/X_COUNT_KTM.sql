--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_COUNT_KTM
  returns (
      LAST_KTM INTEGER_ID)
   as
        declare variable ktm integer_id;

        begin
          last_ktm = 0;
          for select t.ktm from towary t order by cast(ktm as integer_id) asc
            into :ktm
          do begin
            while(:ktm - :last_ktm > 1)
            do begin
              suspend;
              last_ktm = last_ktm + 1;
            end
            last_ktm = ktm;
          end
        end^
SET TERM ; ^
