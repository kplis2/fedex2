--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWIAD_ADDDOKPLIKTOEMAILZAL(
      EMAILWIADREF integer,
      DOKPLIKREF integer)
   as
declare variable path varchar(255);
declare variable name varchar(255);
begin
  for
    select d.localfile, d.plik
      from doklink d
        where d.dokplik = :dokplikref
    into :path, :name
  do begin
    update or insert into emailzal(emailwiad, dokplik, typ, path, name)
      values(:emailwiadref, :dokplikref, 3, :path, :name)
      matching(emailwiad, dokplik, name);
  end
end^
SET TERM ; ^
