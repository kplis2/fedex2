--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_CRM_ACTIVITY(
      CPODMIOT integer = null,
      CKONTRAKT integer = null,
      PKOSOBA integer = null,
      ZADANIE integer = null)
  returns (
      TRESC MEMO,
      DATA timestamp,
      ZALOZYCIEL varchar(60) CHARACTER SET UTF8                           ,
      RODZAJ varchar(60) CHARACTER SET UTF8                           ,
      IKONA varchar(16) CHARACTER SET UTF8                           ,
      OTABLE varchar(40) CHARACTER SET UTF8                           ,
      OREF integer)
   as
begin
  if(:cpodmiot=0) then cpodmiot = null;
  if(:pkosoba=0) then pkosoba = null;
  if(:ckontrakt=0) then ckontrakt = null;
  if(:zadanie=0) then zadanie = null;
  if(:zadanie is not null) then begin
    cpodmiot = null;
    pkosoba = null;
    ckontrakt = null;
  end
  if(cpodmiot is null and pkosoba is null and ckontrakt is null and zadanie is null) then exit;

  --lista notatek
  otable = 'CNOTATKI';
  for select cn.ref, cn.tresc, cn.data, o.nazwa, 'Notatka', 'MI_NOTATKA'
    from cnotatki cn
    left join operator o on o.ref = cn.operator
    where (:cpodmiot is null or cn.cpodmiot = :cpodmiot) and
          (:ckontrakt is null or cn.ckontrakt = :ckontrakt) and
          (:pkosoba is null or cn.pkosoba = :pkosoba ) and
          (:zadanie is null or cn.zadanie = :zadanie )
    into :oref, :tresc, :data, :zalozyciel, :rodzaj, :ikona
   do begin
    suspend;
  end

  otable = 'KONTAKTY';
  --lista kontaktów
  for select k.ref, k.opis, k.data, o.nazwa, t.nazwa,
        case when k.inout = 0 then 'MI_OUTSIDE' else 'MI_INSIDE' end
    from kontakty k
    left join operator o on o.ref = k.operator
    left join ctypykon t on t.ref = k.rodzaj
    where (:cpodmiot is null or k.cpodmiot = :cpodmiot) and
          (:ckontrakt is null or k.ckontrakt = :ckontrakt) and
          (:pkosoba is null or k.pkosoba = :pkosoba ) and
          (:zadanie is null or k.zadanie = :zadanie )
    into :oref, :tresc, :data, :zalozyciel, :rodzaj, :ikona
   do begin
    suspend;
  end
end^
SET TERM ; ^
