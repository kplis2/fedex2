--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_PROCEDURE_PARAMETERS(
      NAZWA varchar(80) CHARACTER SET UTF8                           )
  returns (
      PARAMETERS blob sub_type 0 SEGMENT SIZE 80)
   as
declare variable PARAMETER_NAME varchar(80);
declare variable PARAMETER_DEFAULT varchar(80);
declare variable PARAMETER_TYPE smallint; /* 0-input, 1-output */
declare variable PARAMETER_NUMBER smallint;
declare variable PARAMETER_NULL_FLAG smallint;
declare variable PARAMETER_TYPE_NAME varchar(80); /* systemowa nazwa typu, np. DOUBLE, INT64 */
declare variable PARAMETER_LENGTH integer;
declare variable PARAMETER_CHARACTER_SET_NAME varchar(80);
declare variable PARAMETER_PRECISION integer;
declare variable PARAMETER_FIELD_SUB_TYPE integer;
declare variable PARAMETER_SCALE integer;
declare variable MAX0 integer; /* maksymalna pozycja parametru wejsciowego (licząc od 0) */
declare variable MAX1 integer; /* maksymalna pozycja parametru wyjsciowego (licząc od 0) */
declare variable P integer; /* flaga pozycji parametrow 1-pierwszy parametr in, 2-pierwszy parametr out */
declare variable EOL varchar(2);
declare variable COUNTIN smallint; /* liczba parametrów wejsciowych */
declare variable COUNTOUT smallint; /* liczba parametrow wyjsciowych */
declare variable FIELDS_FIELD_NAME varchar(31); /* nazwa typu użyta w definicji parametru (np.INTEGER_ID, jeżeli RDB$... to typ wbudowany) */
declare variable PARAMETER_MECHANISM smallint; /* 1-type of column */
declare variable PARAMETER_SEGMENT_LENGTH smallint;
declare variable PARAMETER_RELATION_NAME varchar(31);
declare variable PARAMETER_FIELD_NAME varchar(31);
begin
  parameters = '';
  eol = '
  ';
  p = 0;

  if(exists (select first 1 1 from rdb$procedure_parameters where rdb$procedure_name = :nazwa)) then
  begin
    select coalesce(r.rdb$procedure_inputs,0), coalesce(r.rdb$procedure_outputs,0)
      from rdb$procedures r
      where r.rdb$procedure_name = :nazwa
    into :countin, :countout;

    max0 = :countin - 1;
    max1 = :countout - 1;

    for
      select p.rdb$parameter_name, p.rdb$parameter_type, p.rdb$parameter_number, f.rdb$field_name, coalesce(p.rdb$parameter_mechanism,0),
        t.rdb$type_name, f.rdb$field_length, f.rdb$field_precision, f.rdb$field_scale,
        case when f.rdb$field_name like 'RDB$%' then substring(f.rdb$default_source from 1 for 80) else substring(p.rdb$default_source from 1 for 80) end,
        ch.rdb$character_set_name,  f.rdb$field_sub_type, f.rdb$segment_length, p.rdb$null_flag, p.rdb$relation_name, p.rdb$field_name
        from rdb$procedure_parameters p
          left join rdb$fields f on (p.rdb$field_source = f.rdb$field_name)
          left join rdb$types t on (f.rdb$field_type = t.rdb$type)
          left join rdb$character_sets ch on (ch.rdb$character_set_id = f.rdb$character_set_id)
        where p.rdb$procedure_name = :nazwa
          and t.rdb$field_name = 'RDB$FIELD_TYPE'                 -- ograniczamy wynik dla typów danych
        order by p.rdb$parameter_type, p.rdb$parameter_number     -- kolejnosc parametrow: in przed out oraz wg nr pozycji
      into :parameter_name, :parameter_type, :parameter_number, :fields_field_name, :parameter_mechanism,
        :parameter_type_name, :parameter_length, :parameter_precision, :parameter_scale,
        :parameter_default,:parameter_character_set_name, :parameter_field_sub_type, :parameter_segment_length, :parameter_null_flag, :parameter_relation_name, :parameter_field_name
    do begin
      if(:p = 0) then
      begin
        if(:parameter_type = 0) then
          parameters = '(' || :eol;
        if(:countin = 0) then
          parameters = :parameters || :eol;
        p = 1;
      end

      if(:p = 1 and :parameter_type = 1) then
      begin
        parameters = :parameters || 'returns (' || :eol;
        p = 2;
      end

      parameter_name = trim(:parameter_name);
      parameter_name = (select outname from sys_quote_reserved(:parameter_name));
      parameter_field_name = trim(:parameter_field_name);
      parameter_field_name = (select outname from sys_quote_reserved(:parameter_field_name));

      if(parameter_character_set_name is not null and
        parameter_character_set_name='UTF8') then
        parameter_length = parameter_length / 4;
      if(parameter_character_set_name is not null and
        parameter_character_set_name='UNICODE_FSS') then
        parameter_length = parameter_length / 3;

      if(:parameter_mechanism = 1 and :parameter_relation_name is not NULL) then
      begin
        parameters = :parameters || '    ' || :parameter_name || ' type of column ' || trim(:parameter_relation_name) || '.' || trim(:parameter_field_name);
        if(parameter_null_flag = 1) then
          parameters = :parameters || ' NOT NULL';
      end
      else if(:fields_field_name like 'RDB$%') then -- typy wbudowane
      begin
        if(parameter_type_name = 'DATE') then
          parameters = :parameters || '    ' || :parameter_name || ' date';
        else if(parameter_type_name = 'DOUBLE') then
          parameters = :parameters || '    ' || :parameter_name || ' double precision';
        else if(parameter_type_name = 'FLOAT') then
          parameters = :parameters || '    ' || :parameter_name || ' float';
        else if(parameter_type_name = 'INT64' and :parameter_field_sub_type = 0) then
          parameters = :parameters || '    ' || :parameter_name || ' bigint';
        else if(parameter_type_name = 'INT64' and :parameter_field_sub_type = 1) then
          parameters = :parameters || '    ' || :parameter_name || ' numeric(' || :parameter_precision || ',' || (-parameter_scale) || ')';
        else if(parameter_type_name = 'INT64' and :parameter_field_sub_type = 2) then
          parameters = :parameters || '    ' || :parameter_name || ' decimal(' || :parameter_precision || ',' || (-parameter_scale) || ')';
        else if(parameter_type_name = 'LONG' and :parameter_field_sub_type = 0) then
          parameters = :parameters || '    ' || :parameter_name || ' integer';
        else if(parameter_type_name = 'LONG' and :parameter_field_sub_type = 1) then
          parameters = :parameters || '    ' || :parameter_name || ' numeric(' || :parameter_precision || ',' || (-parameter_scale) || ')';
        else if(parameter_type_name = 'LONG' and :parameter_field_sub_type = 2) then
          parameters = :parameters || '    ' || :parameter_name || ' decimal(' || :parameter_precision || ',' || (-parameter_scale) || ')';
        else if(parameter_type_name = 'SHORT' and :parameter_precision = 0) then
          parameters = :parameters || '    ' || :parameter_name || ' smallint';
        else if(parameter_type_name = 'SHORT' and :parameter_precision > 0) then
          parameters = :parameters || '    ' || :parameter_name || ' numeric(' || :parameter_precision || ',' || (-parameter_scale) || ')';
        else if(parameter_type_name = 'TEXT') then
          parameters = :parameters || '    ' || :parameter_name || ' char(' || :parameter_length || ')';
        else if(parameter_type_name = 'TIMESTAMP') then
          parameters = :parameters || '    ' || :parameter_name || ' timestamp';
        else if(parameter_type_name = 'VARYING') then
          parameters = :parameters || '    ' || :parameter_name || ' varchar(' || :parameter_length || ')';
        else if(parameter_type_name = 'TIME') then
          parameters = :parameters || '    ' || :parameter_name || ' time';
        else if(parameter_type_name = 'BLOB') then
          begin
            parameters = :parameters || '    ' || :parameter_name || ' blob sub_type ' || :parameter_field_sub_type;
            if(parameter_segment_length is not null) then
            parameters = :parameters || ' SEGMENT SIZE ' || :parameter_segment_length;
          end
        if(parameter_character_set_name is not null and parameter_character_set_name <> 'WIN1250') then
            parameters = :parameters || ' CHARACTER SET ' || :parameter_character_set_name;
        if(parameter_null_flag = 1) then
            parameters = :parameters || ' NOT NULL';
      end else                -- typy definiowane przez użytkownika
      begin
        fields_field_name = substring(:fields_field_name from 1 for position(' ', :fields_field_name) - 1);
        parameters = :parameters || '    ' || :parameter_name || ' ';
        if(:parameter_mechanism = 1 and :parameter_relation_name is not NULL) then
          parameters = :parameters || ' type of column ' || trim(:parameter_relation_name) || '.' || trim(:parameter_field_name);
        parameters = :parameters || :fields_field_name;
        if(parameter_null_flag = 1) then
          parameters = :parameters || ' NOT NULL';
      end

      if(:parameter_default is not NULL) then
        parameter_default = ' ' || :parameter_default;
      else
        parameter_default = '';

      if((:parameter_type = 0 and :parameter_number = :max0) or (:parameter_type = 1 and :parameter_number = :max1)) then
        parameters = :parameters || :parameter_default || ')' || :eol;
      else
        parameters = :parameters || :parameter_default || ',' || :eol;
    end
  end
  suspend;
end^
SET TERM ; ^
