--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PIT11_V19(
      CURRENTCOMPANY integer,
      YEARID char(4) CHARACTER SET UTF8                           ,
      PERSON_INPUT integer,
      TYP smallint,
      DATA date)
  returns (
      P36 numeric(14,2),
      P37 numeric(14,2),
      P38 numeric(14,2),
      P40 numeric(14,2),
      P52 numeric(14,2),
      P53 numeric(14,2),
      P54 numeric(14,2),
      P55 numeric(14,2),
      P56R numeric(14,2),
      P57R numeric(14,2),
      P58R numeric(14,2),
      P59R numeric(14,2),
      P64 numeric(14,2),
      P65 numeric(14,2),
      P66 numeric(14,2),
      P67 numeric(14,2),
      P76 numeric(14,2),
      P78 numeric(14,2),
      P56 numeric(14,2),
      P57 numeric(14,2),
      P58 numeric(14,2),
      P59 numeric(14,2),
      PERSON integer,
      TAXYEAR char(4) CHARACTER SET UTF8                           ,
      TAXOFFICE varchar(200) CHARACTER SET UTF8                           ,
      USCODE varchar(4) CHARACTER SET UTF8                           ,
      COSTS smallint,
      TAXID varchar(15) CHARACTER SET UTF8                           ,
      BUSINESSACTIV smallint,
      INFONIP varchar(13) CHARACTER SET UTF8                           )
   as
declare variable AKTUOPERATOR integer;
declare variable DESCRIPT varchar(255);
begin
--Proc. generuje dane do PITu-11 w wersji 19-stej

  execute procedure getconfig('INFONIP') returning_values :infonip;
  execute procedure efunc_get_format_nip(:infonip) returning_values :infonip;

  if (person_INPUT = 0) then person_INPUT = null;
  taxyear = yearid;

  for
    select e.person
      from epayrolls pr 
        join eprpos pp on (pr.ref = pp.payroll)
        join employees e on (pp.employee = e.ref)
      where pp.pvalue <> 0 and pp.ecolumn in (3000, 5950)
        and pr.tper starting with :yearid
        and (e.person = :person_input or :person_input is null)
        and pr.company = :currentcompany
        and pr.lumpsumtax = 0
      group by e.person, e.personnames
      order by e.personnames
      into :person
  do begin

    p36 = null;
    p37 = null;
    p38 = null;
    p40 = null;
    p52 = null;
    p53 = null;
    p54 = null;
    p55 = null;
    p56r = null;
    p57r = null;
    p58r = null;
    p59r = null;
    p64 = null;
    p65 = null;
    p66 = null;
    p67 = null;
    p76 = null;
    p78 = null;
    p56 = null;
    p57 = null;
    p58 = null;
    p59 = null;
    costs = null;
    taxoffice = null;
    uscode = null;

-- przychod pracownika =========================================================
    select sum(pp.pvalue)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
        join employees e on (e.ref = pp.employee)
        join ecolumns c on (pp.ecolumn = c.number)
      where pr.tper starting with :yearid
        and e.person = :person
        and ((c.number = 5950 and pr.prtype = 'SOC') --przychod z list socjalnych (BS36243)
          or (c.cflags like '%;POD;%' and pr.prtype <> 'SOC' and c.number <> 5950)) --przychod z list innych niz socjalne
      into :p36;
    p36 = coalesce(p36,0);

-- koszty uzyskania pracownika
    select sum(pp.pvalue)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn = 6500
        and pr.company = :currentcompany --BS63191
        and pr.tper starting with :yearid
        and e.person = :person
      into :p37;
    p37 = coalesce(p37,0);

-- dochod pracownika
    p38 = :p36 - :p37;

-- zaliczka na podatek dochodowy (razem z wyrownaniem) pracownika
    select sum(pp.pvalue)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn in (7350, 7370)
        and pr.company = :currentcompany --BS63191
        and pr.tper starting with :yearid
        and e.person = :person
      into :p40;
    p40 = coalesce(p40,0);

-- przychod czlonka rady naddzorczej ===========================================
    select sum(pp.pvalue)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 3)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn = 5950
        and pr.tper starting with :yearid
        and e.person = :person
        and pr.lumpsumtax = 0
      into :p56r;
    p56r = coalesce(p56r,0);

-- koszty uzyskania czlonka rady naddzorczej
    select sum(pp.pvalue)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 3)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn = 6500
        and pr.tper starting with :yearid
        and e.person = :person
        and pr.lumpsumtax = 0
      into :p57r;
    p57r = coalesce(p57r,0);

-- dochod czlonka rady naddzorczej
    p58r = :p56r - :p57r;

-- zaliczka na podatek dochodowy (razem z wyrownaniem) rady naddzorczej
    select sum(pp.pvalue)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 3)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn in (7350, 7370)
        and pr.tper starting with :yearid
        and e.person = :person
        and pr.lumpsumtax = 0
      into :p59r;
    p59r = coalesce(p59r,0);

-- przychod z umowy UCP i z umowy prawa autorskie ==============================
    select sum(case when coalesce(c.prcosts,0) <> 50 then coalesce(pp.pvalue,0) else 0 end),
           sum(case when coalesce(c.prcosts,0) = 50 then coalesce(pp.pvalue,0) else null end)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 2)
        join emplcontracts c on (c.ref = pr.emplcontract)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn = 3000
        and pr.tper starting with :yearid
        and e.person = :person
        and pr.lumpsumtax = 0
      into :p56, :p64;
    p56 = coalesce(p56,0);

-- koszty uzyskania z umowy UCP i z umowy prawa autorskie
    select sum(case when coalesce(c.prcosts,0) <> 50 then coalesce(pp.pvalue,0) else 0 end),
           sum(case when coalesce(c.prcosts,0) = 50 then coalesce(pp.pvalue,0) else null end)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 2)
        join emplcontracts c on (c.ref = pr.emplcontract)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn = 6500
        and pr.tper starting with :yearid
        and e.person = :person
        and pr.lumpsumtax = 0
      into :p57, :p65;
    p57 = coalesce(p57,0);

-- dochod z umowy UCP
    p58 = p56 - p57;

-- dochod z umowy prawa autorskie
    if (p64 is not null or p65 is not null) then
      p66 = coalesce(p64,0) - coalesce(p65,0);

-- zaliczka na podatek doch. (z wyrownaniem) z um. UCP i z um. prawa autorskie
    select sum(case when coalesce(c.prcosts,0) <> 50 then coalesce(pp.pvalue,0) else 0 end),
           sum(case when coalesce(c.prcosts,0) = 50 then coalesce(pp.pvalue,0) else null end)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 2)
        join emplcontracts c on (c.ref = pr.emplcontract)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn in (7350, 7370)
        and pr.tper starting with :yearid
        and e.person = :person
        and pr.lumpsumtax = 0
      into :p59, :p67;
    p59 = coalesce(p59,0);

-- suma wartosci na pit11(V17)
    p56 = p56r + p56; --przychod RN i UCP
    p57 = p57r + p57; --koszty uzyskania RN i UCP
    p58 = p58r + p58; --dochod RN i UCP
    p59 = p59r + p59; --zaliczka na podatek RN i UCP

-- skladki na ubezpieczenie spoleczne ==========================================
    select sum(pp.pvalue)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn in (6100, 6110, 6120, 6130)
        and pr.tper starting with :yearid
        and e.person = :person
        and pr.lumpsumtax = 0
      into :p76;
    p76 = coalesce(p76,0);

-- skladki na ubezpieczenie zdrowotne
    select sum(pp.pvalue)
      from epayrolls pr
        join eprpos pp on (pr.ref = pp.payroll)
        join employees e on (e.ref = pp.employee)
      where pp.ecolumn = 7210
        and pr.tper starting with :yearid
        and e.person = :person
        and pr.lumpsumtax = 0
      into :p78;
    p78 = coalesce(p78,0);

--pobranie informacji podatkowych (US, koszty uzyskania, itp.)
    select costs, taxoffice, uscode, businessactiv, taxid
      from e_get_perstaxinfo4pit(:yearid, :person) --BS44508
      into :costs, :taxoffice, :uscode, :businessactiv, :taxid;

    if (p36 = 0) then
    begin
      p36 = null;
      if (p37 = 0) then p37 = null;
      if (p38 = 0) then p38 = null;
      if (p40 = 0) then p40 = null;
    end

    if (p52 = 0) then
    begin
      p52 = null;
      if (p53 = 0) then p53 = null;
      if (p54 = 0) then p54 = null;
      if (p55 = 0) then p55 = null;
    end

    if (p56 = 0) then
    begin
      p56 = null;
      if (p57 = 0) then p57 = null;
      if (p58 = 0) then p58 = null;
      if (p59 = 0) then p59 = null;
    end

    if (p64 = 0) then
    begin
      p64 = null;
      if (p65 = 0) then p65 = null;
      if (p66 = 0) then p66 = null;
      if (p67 = 0) then p67 = null;
    end

    if (p76 = 0) then p76 = null;
    if (p78 = 0) then p78 = null;

    suspend;

    if (typ = 1) then
    begin
      execute procedure get_global_param ('AKTUOPERATOR')
        returning_values aktuoperator;
      descript = 'Przekazanie danych na formularzu PIT-11 do ' || coalesce(taxoffice, 'Urzędu Skarbowego');
      if (not exists (select first 1 1 from epersdatasecur
                        where person = :person and descript = :descript and regdate = :data
                          and coalesce(operator,0) = coalesce(:aktuoperator,0) and kod = 2)
      ) then
        insert into epersdatasecur (person, kod, descript, operator, regdate)
          values (:person, 2, :descript, :aktuoperator, :data);
    end
  end
end^
SET TERM ; ^
