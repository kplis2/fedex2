--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_WZ_SEND(
      DOKUMNAGREFS STRING20)
  returns (
      DN_REF DOKUMNAG_ID,
      DN_NUMER INTEGER_ID,
      DN_MAGZRDL DEFMAGAZ_ID,
      DN_MAGDOC DEFMAGAZ_ID,
      SYMBOL SYMBOL_ID,
      DOKTYP DEFDOKUM_ID,
      DN_ODBIORCA_INT_ID INTEGER_ID,
      DP_REF INTEGER_ID,
      T_INT_ID STRING120,
      ILOSCL ILOSCI_MAG,
      DN_INT_ID STRING120,
      DP_INT_ID STRING120,
      DP_SLOWNIKID STRING40,
      DP_UWAGIPOZ STRING255,
      DP_PROGRAM_ID STRING255,
      SERIALNO STRING5000)
   as
declare variable ilosc integer_id;
declare variable licznik integer_id;
declare variable dokumnagref integer_id;
declare variable ktm ktm_id;
declare variable mwsord integer_id;
declare variable serialnopoz string255;
begin

    dokumnagref = cast( dokumnagrefs as integer_id);
    for
      select dn.ref, dn.numer, dn.magazyn, dn.mag2, dn.symbol,
            dn.typ, od.int_id, dp.ref, t.int_id, dp.iloscl,
            dn.int_id, dp.int_id, t.ktm, mo.ref
          from dokumnag dn
              join dokumpoz dp on (dp.dokument = dn.ref)
              join towary t on (t.ktm = dp.ktm)
              left join mwsacts a on (dp.frommwsact = a.ref)
              join odbiorcy od on (dn.odbiorcaid = od.ref)
              join mwsords mo on (dn.ref = mo.docid)
            where coalesce(dp.fake,0) <> 1 and dn.ref = :dokumnagref
            order by t.ktm
    into    :dn_ref, :dn_numer, :dn_magzrdl, :dn_magdoc, :symbol,
            :doktyp, :dn_odbiorca_int_id, :dp_ref, :t_int_id, :iloscl,
            :dn_int_id, :dp_int_id, :ktm, :mwsord
    do begin
        select count(ref) from x_mws_serie xs
            where :mwsord in (xs.mwsordin, xs. mwsordout)
                and :ktm = xs.ktm
            into :ilosc;
        if (coalesce(ilosc,0) >0 ) then
        begin
            serialno='';
            licznik =0;
            for
                select serialno from x_mws_serie xs
                    where :mwsord in (xs.mwsordin, xs. mwsordout)
                        and :ktm = xs.ktm order by ref
            into :serialnopoz do
            begin
                if(licznik > 0) then
                     serialno = serialno||',';
                if (licznik < iloscl) then
                    serialno = serialno || serialnopoz ;

                licznik = licznik +1;
            end
        end
        suspend;
        serialno = null;
    end
end^
SET TERM ; ^
