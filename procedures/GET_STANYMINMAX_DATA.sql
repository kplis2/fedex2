--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_STANYMINMAX_DATA(
      MAGAZYNY varchar(255) CHARACTER SET UTF8                           ,
      MASKAKTM varchar(40) CHARACTER SET UTF8                           ,
      DATAOD date,
      DATADO date)
  returns (
      DATA date,
      STANMIN numeric(14,4),
      STANMAX numeric(14,4))
   as
declare variable prevdata date;
declare variable newdata date;
declare variable newstanmin numeric(14,4);
declare variable newstanmax numeric(14,4);
begin
  prevdata = :dataod - 1;
  stanmin = 0;
  stanmax =0;
  for select s.data, s.stanmin, s.stanmax
  from stanyminmax s
  left join wersje w on (w.ref=s.wersjaref)
  where s.data<=:datado
    and (w.ktm like :maskaktm or :maskaktm='')
    and (:magazyny like '%;'||trim(s.magazyn)||';%')
  order by s.data
  into :newdata,:newstanmin,:newstanmax
  do begin
    while(:prevdata<:newdata-1) do begin
      prevdata = :prevdata + 1;
      data = :prevdata;
      if(:data>=:dataod) then suspend;
    end
    prevdata = :newdata;
    data = :prevdata;
    stanmin = :newstanmin;
    stanmax = :newstanmax;
    if(:data>=:dataod) then suspend;
  end
  while(:prevdata<:datado) do begin
    prevdata = :prevdata + 1;
    data = :prevdata;
    if(:data>=:dataod) then suspend;
  end
end^
SET TERM ; ^
