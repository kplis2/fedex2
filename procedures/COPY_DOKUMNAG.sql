--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COPY_DOKUMNAG(
      REF integer,
      REFKOP integer,
      KUMULACJA integer = 0)
   as
declare variable REFDOKUMPOZ integer;
declare variable KTM varchar(40);
declare variable WERSJA integer;
declare variable OPK integer;
declare variable JEDNO integer;
declare variable ILOSC numeric(14,4);
declare variable WAGA numeric(14,4);
declare variable FLAGI varchar(40);
declare variable PARAMN1 numeric(14,4);
declare variable PARAMN2 numeric(14,4);
declare variable PARAMN3 numeric(14,4);
declare variable PARAMN4 numeric(14,4);
declare variable PARAMN5 numeric(14,4);
declare variable PARAMN6 numeric(14,4);
declare variable PARAMN7 numeric(14,4);
declare variable PARAMN8 numeric(14,4);
declare variable PARAMN9 numeric(14,4);
declare variable PARAMN10 numeric(14,4);
declare variable PARAMN11 numeric(14,4);
declare variable PARAMN12 numeric(14,4);
declare variable PARAMS1 varchar(255);
declare variable PARAMS2 varchar(255);
declare variable PARAMS3 varchar(255);
declare variable PARAMS4 varchar(255);
declare variable PARAMS5 varchar(255);
declare variable PARAMS6 varchar(255);
declare variable PARAMS7 varchar(255);
declare variable PARAMS8 varchar(255);
declare variable PARAMS9 varchar(255);
declare variable PARAMS10 varchar(255);
declare variable PARAMS11 varchar(255);
declare variable PARAMS12 varchar(255);
declare variable PARAMD1 timestamp;
declare variable PARAMD2 timestamp;
declare variable PARAMD3 timestamp;
declare variable PARAMD4 timestamp;
declare variable GR_VAT varchar(5);
declare variable DOSTAWA integer;
declare variable CENA numeric(14,2);
declare variable ALTTOPOZ DOKUMPOZ_ID;
declare variable FAKE smallint;
declare variable HAVEFAKE smallint;
declare variable ORGDOKUMPOZ DOKUMPOZ_ID;
declare variable ALTTOPOZN DOKUMPOZ_ID;
declare variable CENACEN NUMERIC_14_4;
begin
  kumulacja = coalesce(kumulacja,0);
  if (kumulacja = 0) then
  begin
    for
      select ktm, wersja, jedno, ilosc, waga, flagi,
          paramd1, paramd2, paramd3, paramd4,
          paramn1, paramn2, paramn3, paramn4,
          paramn5, paramn6, paramn7, paramn8,
          paramn9, paramn10, paramn11, paramn12,
          params1, params2, params3, params4,
          params5, params6, params7, params8,
          params9, params10, params11, params12, gr_vat,
          alttopoz, fake, havefake, ref, cena, cenacen, dostawa
        from dokumpoz
        where dokument = :refkop
        order by dokumpoz.alttopoz nulls first, dokumpoz.numer
      into :ktm, :wersja, :jedno, :ilosc, :waga, :flagi,
        :paramd1, :paramd2, :paramd3, :paramd4,
        :paramn1, :paramn2, :paramn3, :paramn4,
        :paramn5, :paramn6, :paramn7, :paramn8,
        :paramn9, :paramn10, :paramn11, :paramn12,
        :params1, :params2, :params3, :params4,
        :params5, :params6, :params7, :params8,
        :params9, :params10, :params11, :params12, :gr_vat,
        :alttopoz, :fake,  :havefake, :orgdokumpoz, :cena, :cenacen, :dostawa
    do begin
      if (:alttopoz is not null) then
        select ref from dokumpoz
          where dokument = :ref and orgdokumpoz = :alttopoz
        into :alttopozn;
  
      insert into dokumpoz (dokument, KTM, wersja, jedno, ilosc, waga, flagi,
          paramd1, paramd2, paramd3, paramd4,
          paramn1, paramn2, paramn3, paramn4,
          paramn5, paramn6, paramn7, paramn8,
          paramn9, paramn10, paramn11, paramn12,
          params1, params2, params3, params4,
          params5, params6, params7, params8,
          params9, params10, params11, params12, gr_vat,
          alttopoz, fake, havefake, orgdokumpoz, cena, cenacen, dostawa)
        values (:ref, :ktm, :wersja, :jedno, :ilosc, :waga, :flagi,
          :paramd1, :paramd2, :paramd3, :paramd4,
          :paramn1, :paramn2, :paramn3, :paramn4,
          :paramn5, :paramn6, :paramn7, :paramn8,
          :paramn9, :paramn10, :paramn11, :paramn12,
          :params1, :params2, :params3, :params4,
          :params5, :params6, :params7, :params8,
          :params9, :params10, :params11, :params12, :gr_vat,
          :alttopozn, :fake, :havefake, :orgdokumpoz, :cena, :cenacen, :dostawa);
    end
  end
  else if (kumulacja > 0) then
  begin
    for
      select ktm, wersja, OPK, jedno, ilosc, waga, flagi,
          paramd1, paramd2, paramd3, paramd4,
          paramn1, paramn2, paramn3, paramn4,
          paramn5, paramn6, paramn7, paramn8,
          paramn9, paramn10, paramn11, paramn12,
          params1, params2, params3, params4,
          params5, params6, params7, params8,
          params9, params10, params11, params12, gr_vat, dostawa, cena, cenacen,
          alttopoz, fake, havefake, ref
        from dokumpoz
        where dokument = :refkop
        order by alttopoz nulls first
        into :ktm, :wersja, :OPK, :jedno, :ilosc, :waga, :flagi,
          :paramd1, :paramd2, :paramd3, :paramd4,
          :paramn1, :paramn2, :paramn3, :paramn4,
          :paramn5, :paramn6, :paramn7, :paramn8,
          :paramn9, :paramn10, :paramn11, :paramn12,
          :params1, :params2, :params3, :params4,
          :params5, :params6, :params7, :params8,
          :params9, :params10, :params11, :params12, :gr_vat, :dostawa, :cena, :cenacen,
          :AltToPozN, :fake,  :HaveFake, :OrgDokumPoz
      do begin
        refdokumpoz = null;
        if (coalesce(fake,0) = 0 and coalesce(havefake,0) = 0) then
        begin
          if(:kumulacja = 1) then --kumulacja po KTM-wersja
            select first 1 dp.ref
              from DOKUMPOZ dp
              where dp.dokument = :ref
                and dp.ktm = :ktm
                and dp.wersja = :wersja
                and dp.opk = :opk
              into :refdokumpoz;
          else begin --kumulacja po partii
            select first 1 ref from DOKUMPOZ
              where dokument = :ref and ktm = :ktm and wersja = :wersja
                and opk = :opk and cena = :cena and dostawa = :dostawa and cenacen = :cenacen
                and paramn1 is not distinct from :paramn1 and paramn2 is not distinct from :paramn2
                and paramn3 is not distinct from :paramn3 and paramn4 is not distinct from :paramn4
                and paramn5 is not distinct from :paramn5 and paramn6 is not distinct from :paramn6
                and paramn7 is not distinct from :paramn7 and paramn8 is not distinct from :paramn8
                and paramn9 is not distinct from :paramn9 and paramn10 is not distinct from :paramn10
                and paramn11 is not distinct from :paramn11 and paramn12 is not distinct from :paramn12
                and params1 is not distinct from :params1 and params2 is not distinct from :params2
                and params3 is not distinct from :params3 and params4 is not distinct from :params4
                and params5 is not distinct from :params5 and params6 is not distinct from :params6
                and params7 is not distinct from :params7 and params8 is not distinct from :params8
                and params9 is not distinct from :params9 and params10 is not distinct from :params10
                and params11 is not distinct from :params11 and params12 is not distinct from :params12
                and paramd1 is not distinct from :paramd1 and paramd2 is not distinct from :paramd2
                and paramd3 is not distinct from :paramd3 and paramd4 is not distinct from :paramd4
              into :refdokumpoz;
          end
        end
        if(refdokumpoz is null) then
        begin
          if (:alttopoz is not null) then
          begin
            select ref
              from dokumpoz
               where dokument = :ref
                  and orgdokumpoz = :alttopoz
               into :alttopozn;
          end
          insert into dokumpoz (dokument, KTM, wersja, jedno,OPK, ilosc, waga, flagi,
                paramd1, paramd2, paramd3, paramd4,
                paramn1, paramn2, paramn3, paramn4,
                paramn5, paramn6, paramn7, paramn8,
                paramn9, paramn10, paramn11, paramn12,
                params1, params2, params3, params4,
                params5, params6, params7, params8,
                params9, params10, params11, params12, gr_vat,
                alttopoz,fake, havefake, orgdokumpoz, cena, cenacen, dostawa)
            values (:ref, :ktm, :wersja, :jedno, :OPK, :ilosc, :waga, :flagi,
                :paramd1, :paramd2, :paramd3, :paramd4,
                :paramn1, :paramn2, :paramn3, :paramn4,
                :paramn5, :paramn6, :paramn7, :paramn8,
                :paramn9, :paramn10, :paramn11, :paramn12,
                :params1, :params2, :params3, :params4,
                :params5, :params6, :params7, :params8,
                :params9, :params10, :params11, :params12, :gr_vat,
                :alttopozn, :fake,  :havefake, :orgdokumpoz,
                (case when :kumulacja = 2 then :cena else null end), (case when :kumulacja = 2 then :cenacen else null end),
                (case when :kumulacja = 2 then :dostawa else null end));
        end
        else
        begin
          update dokumpoz set ilosc=ilosc+:ilosc where ref=:refdokumpoz;
        end
    end
  end
end^
SET TERM ; ^
