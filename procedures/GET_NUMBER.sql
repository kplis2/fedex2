--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_NUMBER(
      NAZWA varchar(40) CHARACTER SET UTF8                           ,
      REJESTR varchar(20) CHARACTER SET UTF8                           ,
      DOKUMENT varchar(20) CHARACTER SET UTF8                           ,
      DATA date,
      BLOCKREF integer,
      DOKREF integer)
  returns (
      NUMBER integer,
      SYMBOL varchar(40) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE P_REJ SMALLINT;
DECLARE VARIABLE P_DOK SMALLINT;
DECLARE VARIABLE P_OKRES SMALLINT;
DECLARE VARIABLE KEY_VAL VARCHAR(50);
DECLARE VARIABLE IS_REJ SMALLINT;
DECLARE VARIABLE IS_DOK SMALLINT;
DECLARE VARIABLE TYP_OKRES SMALLINT;
DECLARE VARIABLE OLD_NUMBER INTEGER;
DECLARE VARIABLE DELIM VARCHAR(1);
DECLARE VARIABLE OKRES VARCHAR(8);
DECLARE VARIABLE LOKRES VARCHAR(8);
DECLARE VARIABLE SUFFIX VARCHAR(20);
DECLARE VARIABLE CHRONO SMALLINT;
DECLARE VARIABLE YER INTEGER;
DECLARE VARIABLE LASTDATA TIMESTAMP;
DECLARE VARIABLE LASTDATA_NEXT TIMESTAMP;
DECLARE VARIABLE P_ODDZIAL SMALLINT;
DECLARE VARIABLE ODDZIAL VARCHAR(40);
declare variable ODDZIALSYM varchar(10);
DECLARE VARIABLE IS_ODDZIAL SMALLINT;
DECLARE VARIABLE NUMREF INTEGER;
DECLARE VARIABLE DATAOLD TIMESTAMP;
DECLARE VARIABLE S_ORDER SMALLINT;
DECLARE VARIABLE PROC VARCHAR(60);
DECLARE VARIABLE PROCED VARCHAR(255);
declare variable ISF_COMPANY smallint;
declare variable CURRENT_COMPANY integer;
begin
--exception universal coalesce(nazwa,'nazwa') ||' '|| coalesce(rejestr,'rejestr')||' '||dokument||' '||coalesce(data, 'data')||' '||coalesce(blockref,'blockref')||' '||coalesce(dokref,'dokref');
  if(:nazwa is null or (:nazwa = '')) then
     exception GENERATOR_NOT_DEFINED 'Numerator dla '||coalesce(:rejestr,'')||'/'||coalesce(:dokument,'')||' nie jest zdefiniowany!';
  execute procedure GETCONFIG('AKTUODDZIAL') returning_values :oddzial;
  select symbol from ODDZIALY where oddzial=:oddzial into :oddzialsym;
  select rejestr, dokument, oddzial, okres, s_rejestr, s_dokument, s_okres, delimiter, suffix, chrono, s_oddzial,s_order, procedura, company
    from NUMBERGEN where NAZWA = :nazwa
    into :is_rej, :is_dok, :is_oddzial, :typ_okres, :p_rej, :p_dok, :p_okres, :delim, :suffix, :chrono, :p_oddzial, :s_order, :proc, :ISF_company;
  if(:is_oddzial=0) then oddzial='XXX'; /* numeracja wspolna dla oddzialow prowadzonych w jednej bazie */
  if (:isf_company is null) then isf_company = 0;
  if (isf_company = 1) then
    execute procedure get_global_param('CURRENTCOMPANY')
      returning_values :current_company;

  /* kontrukcja klucza generatora*/
  key_val = '';
  if(:delim=' ') then delim='';
  if(:rejestr is null) then rejestr = '';
  if(:dokument is null) then dokument = '';
  if(:proc is null) then proc='';
  if(:is_rej = 1) then key_val = :key_val || :rejestr;
  if(:is_dok = 1) then key_val = :key_val || :dokument;
  okres = cast( extract( year from :data) as char(4));
  if(:typ_okres = 1) then begin
     /* data pelna*/
     if(extract(month from :data) < 10) then
       okres = :okres ||'0' ||cast(extract(month from :data) as char(1));
     else
       okres = :okres ||cast(extract(month from :data) as char(2));
  end
  if(:okres is null) then okres = '';
  key_val = :key_val || :okres;
  /*okreslenie kolejnego numeru */
  if(:blockref is not null) then begin
    select NUMBER from NUMBERBLOCK where REF=:blockref and NUMERATOR = :nazwa into :numref;
    if(:numref > 0) then begin
      select number, lastdata from NUMBERFREE where ref=:numref into :old_number, :dataold;
      if(:dataold <> :data) then exception NUMBERGET_DATABLEDNA;
    end
  end
  if(:old_number is null) then begin
    select min(number) from NUMBERFREE where oddzial = :oddzial and nazwa=:nazwa and key_val = :key_val and typ = 1 and numblock is null and (:ISF_company != 1 or company = :current_company) into :old_number;

  end
  if(:old_number is not null) then begin
    /* znaleziono stary numer */
    number = old_number;
    if(:chrono> 0 and :blockref is null)then begin
      select lastdata from NUMBERFREE where oddzial = :oddzial and nazwa=:nazwa and key_val = :key_val and typ = 1  and number = :old_number and (:ISF_company != 1 or company = :current_company)
      into :lastdata;
      if(:lastdata < :data) then begin
        select min(lastdata) from NUMBERFREE where oddzial = :oddzial and nazwa=:nazwa and key_val = :key_val and typ = 2  and number > :old_number and (:ISF_company != 1 or company = :current_company)
        into :lastdata_next;
        if(:lastdata_next < :data) then
          exception generator_chronofreenumber;
      end else if(:lastdata > :data)then begin
        select max(lastdata) from NUMBERFREE where oddzial = :oddzial and nazwa=:nazwa and key_val = :key_val and typ = 2  and number < :old_number and (:ISF_company != 1 or company = :current_company)
        into :lastdata_next;
        if(:lastdata_next > :data) then
          exception GENERATOR_CHRONOSAAFTER;
      end
    end
    delete from NUMBERFREE where nazwa = :nazwa and oddzial = :oddzial and key_val = :key_val and typ = 1 and number = :old_number and (:ISF_company != 1 or company = :current_company);
  end
  else
  begin
    /* odczytanie nowego numeru */
    select min(number), min(LASTDATA) from NUMBERFREE where oddzial = :oddzial and nazwa=:nazwa and key_val = :key_val and typ = 0 and (:ISF_company != 1 or company = :current_company) into :number, :lastdata;


    if(:number is null) then begin
        /* zalozenie pierwszego rekordu w danej dziedzinie numeratora */
       if (ISF_company = 1) then
         insert into NUMBERFREE( ODDZIAL, NAZWA, KEY_VAL,NUMBER,TYP, LASTDATA, company) values (:oddzial, :nazwa , :key_val, 0,0 , :data, :current_company);
       else
       insert into NUMBERFREE( ODDZIAL, NAZWA, KEY_VAL,NUMBER,TYP, LASTDATA) values(:oddzial, :nazwa,:key_val,0,0,:data);
      number = 0;
    end
    if(:chrono > 0) then
      if(:lastdata > :data) then
        exception GENERATOR_CHRONOSAAFTER;
    /*numer kolejny */
    number = number + 1;
    /* zapis ostatniego użytego numeru */
    if (ISF_company = 1) then
      update NUMBERFREE set NUMBER=:number, LASTDATA = :data where oddzial = :oddzial and nazwa=:nazwa and key_val =:key_val and typ = 0 and company = :current_company;
    else
    update NUMBERFREE set NUMBER=:number, LASTDATA = :data where oddzial = :oddzial and nazwa=:nazwa and key_val =:key_val and typ = 0;
  end
  if(:chrono > 0) then
    if (ISF_company = 1) then
      insert into NUMBERFREE( ODDZIAL, NAZWA, KEY_VAL,NUMBER,TYP, LASTDATA, company) values(:oddzial, :nazwa,:key_val,:number,2,:data, :current_company);
    else
    insert into NUMBERFREE( ODDZIAL, NAZWA, KEY_VAL,NUMBER,TYP, LASTDATA) values(:oddzial, :nazwa,:key_val,:number,2,:data);
  if(number is not null) then begin
    symbol = '';
    if(substring(:rejestr from 3 for 1)=' ') then REJESTR = substring(:rejestr from 1 for 2);
    if(:p_rej = 1) then symbol = symbol || REJESTR;
    if(:p_dok = 1 )then begin
      if(:symbol <> '')then symbol = symbol || delim;
      if(substring(:dokument from 3 for 1)=' ') then dokument = substring(:dokument from 1 for 2);
      symbol = symbol || :dokument;
    end
    if(s_order = 0)then begin
      if(:symbol <> '')then symbol = symbol || delim;
      symbol = symbol || cast(:number as varchar(15));
    end
    if(:p_oddzial = 1)then begin
      if(:symbol <> '')then symbol = symbol || delim;
      symbol = symbol || :oddzialsym;
    end
    if(:suffix <> '') then begin
      if(:symbol <> '')then symbol = symbol || delim;
      symbol = symbol || :suffix;
    end
    if(:p_okres > 0 )then begin
        lokres = cast( extract( year from :data) as char(4));
        if((:p_okres = 6) or (:p_okres = 4) or (:p_okres = 2) or (:p_okres = 8)) then begin
          lokres = substring(:lokres from 3 for 2);
        end
        if((:p_okres = 7) or (:p_okres = 8)) then begin
          lokres = '/'||:lokres;
        end
        if((:p_okres = 1) or (:p_okres = 2)) then
          if(extract(month from :data) < 10) then
            lokres = :lokres ||'0' ||cast(extract(month from :data) as char(1));
          else
            lokres = :lokres || cast(extract(month from :data) as char(2));
        if((:p_okres = 5) or (:p_okres = 6) or (:p_okres = 7) or (:p_okres = 8)) then
          if(extract(month from :data) < 10) then
            lokres = '0' ||cast(extract(month from :data) as char(1)) || :lokres;
          else
            lokres = cast(extract(month from :data) as char(2)) || :lokres;
        if(:symbol <> '')then symbol = symbol || delim;
        symbol = symbol || lokres;
    end
  end
  if(s_order = 1)then begin
    if(:symbol <> '')then symbol = symbol || delim;
    symbol = symbol || cast(:number as varchar(15));
  end
  if(:proc<>'') then begin
    proced = 'select rsymbol from '||:proc||'('||:dokref||','''||:nazwa||''','''||:rejestr||''','''||:dokument||''','''||:data||''','||:number||','''||:symbol||''')';
    if (:proced is not null) then execute statement :proced into :symbol;
  end
end^
SET TERM ; ^
