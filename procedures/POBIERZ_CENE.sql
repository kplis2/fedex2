--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POBIERZ_CENE(
      NRCENNIKA integer,
      WERSJAREF integer,
      JEDN integer,
      BN char(1) CHARACTER SET UTF8                           ,
      CENACEN numeric(14,4),
      WALCEN varchar(3) CHARACTER SET UTF8                           ,
      PREC smallint,
      CENAMAG numeric(14,4),
      DOSTAWA integer,
      KLIENT integer,
      TYP varchar(10) CHARACTER SET UTF8                           ,
      REF integer)
  returns (
      RCENACEN numeric(14,4),
      RWALCEN varchar(3) CHARACTER SET UTF8                           ,
      RJEDN integer,
      RSTATUS smallint,
      RPREC smallint)
   as
declare variable PROCEDURA varchar(100);
declare variable STAT varchar(1024);
declare variable ODDZIAL varchar(20);
declare variable KTM varchar(40);
begin

    NRCENNIKA = coalesce(NRCENNIKA,0);
    WERSJAREF = coalesce(WERSJAREF,0);
    JEDN = coalesce(JEDN,0);
    BN = coalesce(BN,'');
    CENACEN = coalesce(CENACEN,0);
    WALCEN = coalesce(WALCEN,'');
    PREC = coalesce(PREC,0);
    CENAMAG = coalesce(CENAMAG,0);
    DOSTAWA = coalesce(DOSTAWA,0);
    KLIENT = coalesce(KLIENT,0);
    TYP = coalesce(TYP,'');
    REF =  coalesce(REF,0);

  rstatus = 0;
  rcenacen = NULL;
  rwalcen = NULL;
  rjedn = NULL;
  rprec = NULL;
  if(:bn is null or :bn=' ') then bn = 'N';
  execute procedure GET_CONFIG('AKTUODDZIAL',0) returning_values :oddzial;

    -- zakladamy ze cennik jest ustalony
    select procedura, prec
      from defcennik
      where ref=:nrcennika
      into :procedura, :rprec;
    -- pobierz cene z procedury
    if(:procedura<>'') then begin
      stat = 'select * from ' || :procedura || '(';
      stat = stat || cast(:nrcennika as varchar(10))||', ';
      stat = stat || cast(:wersjaref as varchar(10))||', ';
      stat = stat || cast(:jedn as varchar(10))||', ';
      stat = stat || '''' || :bn ||''', ';
      stat = stat || cast(:cenamag as varchar(10))||', ';
      stat = stat || cast(:dostawa as varchar(10))||', ';
      stat = stat || cast(:klient as varchar(10))||', ';
      stat = stat || '''' || :oddzial ||''', ';
      stat = stat || '''' || :typ ||''', ';
      stat = stat || cast(:ref as varchar(10))||') ';
      execute statement :stat into :rstatus, :rcenacen, :rwalcen, :rjedn;
      if(:rprec is null) then rprec = :prec;
      if(:rstatus=1) then exit;
    end
    rcenacen = NULL;
    rwalcen = NULL;
    rjedn = NULL;
    -- pobierz cene z cennika dla zadanej jednostki
    select first 1 (case when :bn='B' then CENABRU else CENANET end), WALUTA, JEDN, PREC
      from CENNIK
      where CENNIK=:nrcennika and WERSJAREF=:wersjaref and JEDN = :jedn
      into :rcenacen, :rwalcen, :rjedn, :rprec;
    -- pobierz cene z cennika dla dowolnej jednostki
    if(:rcenacen is null) then begin
      select first 1 (case when :bn='B' then CENABRU else CENANET end), WALUTA, JEDN, PREC
        from CENNIK
        where CENNIK=:nrcennika and WERSJAREF=:wersjaref
        into :rcenacen, :rwalcen, :rjedn, :rprec;
    end
    -- pobierz cene z cennika dla wersji zerowej
    if(:rcenacen is null) then begin
      select KTM from WERSJE where ref=:wersjaref into :ktm;
      select first 1 (case when :bn='B' then CENABRU else CENANET end), WALUTA, JEDN, PREC
        from CENNIK
        where CENNIK=:nrcennika and KTM=:ktm and WERSJA=0
        into :rcenacen, :rwalcen, :rjedn, :rprec;
    end
    if(:rcenacen is not null) then rstatus = 1;
    if(:rcenacen is null) then begin
      rcenacen = :cenacen;
      rwalcen = :walcen;
      rjedn = :jedn;
      rprec = :prec;
    end
    if(:rprec is null) then rprec = :prec;
end^
SET TERM ; ^
