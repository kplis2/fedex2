--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDULE_CALC(
      PRSCHEDULE integer,
      PRSCHEDMETH varchar(40) CHARACTER SET UTF8                           ,
      PRSCHEDZAM integer,
      PRSCHEDGUIDE integer,
      PRSCHEDOPER integer,
      MODE integer,
      CLEAROPER smallint,
      ONLYWAITINGGUIDE smallint,
      TIMESTARTFROM timestamp = current_timestamp)
  returns (
      OPERCOUNT integer)
   as
declare variable sql varchar(1024);
begin
  select m.prschedulesql
    from prschmethods m
    where m.symbol = :prschedmeth
    into sql;
  if (coalesce(sql,'') <> '') then
  begin
    execute procedure prschedule_calc_prepare(:prschedule, :prschedzam, :prschedguide, :prschedoper,:mode, :clearoper, :onlywaitingguide)
      returning_values opercount;
    sql = 'execute procedure '||sql||'('||:prschedule||',0,0'||:opercount||')';
    execute statement sql;
  end
end^
SET TERM ; ^
