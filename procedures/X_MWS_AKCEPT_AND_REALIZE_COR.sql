--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_AKCEPT_AND_REALIZE_COR(
      DOCID DOC_ID)
  returns (
      REFMWSORD MWSORDS_ID)
   as
  declare variable palloc mwspallocs_id;
  declare variable mgref mwsords_id;
  declare variable actref mwsords_id;
begin
  -- MKD ZG 133728
  -- procedura suży do wygenerowania i zaakceptowania zlecenia PT na podstawie zaakceptowanej ZZ
  execute procedure xk_mws_gen_mwsord_outcor (docid,null,null,null,null,null,null, null)
    returning_values :refmwsord;
  if (refmwsord is not null ) then begin
    --zaakceptowanie zlecenia PT
    update mwsacts a set a.status = 1 where a.mwsord = :refmwsord;
    update mwsacts a set a.status = 2 where a.mwsord = :refmwsord;
    select MWSPALLOCREF from MWS_RPT_GEN_MWSPALLOCS (74)
      into  :palloc;
  
    update mwsacts a set a.mwspallocl = :palloc,
                         a.mwsconstlocl = 3, --MKD przyjecie na PZK1
                         a.quantityc = a.quantity,
                         a.status = 5
          where a.mwsord = :refmwsord;
    update mwsords o set o.status = 5, o.description = 'automatyczne przyjecie po korekcie WZ' where o.ref = :refmwsord;
    -- znalezienie i zaakceptowanie zlecenia MG
    for
      select ref from mwsacts s where s.mwsord = :refmwsord
      into :actref do
    begin
      mgref = null;
      select ref
        from mwsords o
          where o.frommwsact = :actref
              and o.status <> 5
      into :mgref;
      if (mgref is not null) then
      begin
        update mwsords o set o.status = 0 where o.ref = :mgref;
        delete from mwsords where ref= :mgref;
        /*update mwsacts a set a.status = 1 where a.mwsord = :mgref;
        update mwsacts a set a.status = 2 where a.mwsord = :mgref;
        update mwsacts a set --a.mwsconstlocp = 3,
                              a.mwsconstlocl = 3, --MKD przyjecie na PZK1
                           --a.mwspallocp = :palloc,
                           a.mwspallocl = :palloc, 
                           a.quantityc = a.quantityl,
                           a.status = 5
            where a.mwsord = :mgref;
        update mwsords o set o.status = 5, o.description = 'automatyczne przesuniecie po korekcie WZ' where o.ref = :mgref;*/
      end
    end
  end

  suspend;
end^
SET TERM ; ^
