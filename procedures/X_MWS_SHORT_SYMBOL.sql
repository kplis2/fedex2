--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_SHORT_SYMBOL(
      SHORT_SYMBOL STRING100)
  returns (
      X_ROW_TYPE SMALLINT_ID,
      MWSSTANDSEG INTEGER_ID,
      ISFULL INTEGER_ID)
   as
begin
  isfull = 0;
  x_row_type = 0;
  mwsstandseg = 0;
  isfull = 0;
--szukamy czy pelny symbol wystepuje w magazynie
  select first 1 1
    from mwsconstlocs m
    where m.symbol = :short_symbol
  into :isfull;
--szukamy typu lokacji i grupy
--grupy uzywam do slownikowania reszty kodu
  if (isfull = 0) then
    select first 1 m.x_row_type, m.mwsstandseg
      from mwsconstlocs m
      where m.symbol like :short_symbol||'___'   -- szukamy tylko bez 3 znaków '-00' -> '-99'
    into :x_row_type, :mwsstandseg;
  else
    select first 1 m.x_row_type    -- jesli mamy pelny symbol, a potrzebujemy typu regalu np. HH realizacja WG
      from mwsconstlocs m
      where m.symbol = :short_symbol
    into :x_row_type;
  suspend;
end^
SET TERM ; ^
