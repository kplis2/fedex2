--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_TOW_JEDNOSTKI_INS(
      TOWARID type of column INT_IMP_TOW_JEDNOSTKI.TOWARID,
      JEDNOSTKA type of column INT_IMP_TOW_JEDNOSTKI.JEDNOSTKA,
      JEDNOSTKAID type of column INT_IMP_TOW_JEDNOSTKI.JEDNOSTKAID,
      GLOWNA type of column INT_IMP_TOW_JEDNOSTKI.GLOWNA,
      LICZNIK type of column INT_IMP_TOW_JEDNOSTKI.LICZNIK,
      MIANOWNIK type of column INT_IMP_TOW_JEDNOSTKI.MIANOWNIK,
      WAGA type of column INT_IMP_TOW_JEDNOSTKI.WAGA,
      WYSOKOSC type of column INT_IMP_TOW_JEDNOSTKI.WYSOKOSC,
      DLUGOSC type of column INT_IMP_TOW_JEDNOSTKI.DLUGOSC,
      SZEROKOSC type of column INT_IMP_TOW_JEDNOSTKI.SZEROKOSC,
      SKADTABELA type of column INT_IMP_TOW_JEDNOSTKI.SKADTABELA,
      SKADREF type of column INT_IMP_TOW_JEDNOSTKI.SKADREF,
      DEL type of column INT_IMP_TOW_JEDNOSTKI.DEL,
      HASHVALUE type of column INT_IMP_TOW_JEDNOSTKI.HASH,
      REC type of column INT_IMP_TOW_JEDNOSTKI.REC,
      SESJA type of column INT_IMP_TOW_JEDNOSTKI.SESJA)
  returns (
      REF type of column INT_IMP_TOW_JEDNOSTKI.REF)
   as
begin
  insert into int_imp_tow_jednostki (towarid, jednostka, jednostkaid, glowna, licznik, mianownik,
      waga, wysokosc, dlugosc, szerokosc, "HASH", del, rec,
      skadtabela, skadref, sesja)
    values (:towarid, :jednostka, :jednostkaid, :glowna, :licznik, :mianownik,
      :waga, :wysokosc, :dlugosc, :szerokosc, :hashvalue, :del, :rec,
      :skadtabela, :skadref, :sesja)
    returning ref into :ref ;
end^
SET TERM ; ^
