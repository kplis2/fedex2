--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_MAGTOFAK(
      FAK integer)
  returns (
      REF integer,
      AKCEPTACJA smallint,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      WARTOSC numeric(14,2),
      KINDSTR varchar(20) CHARACTER SET UTF8                           )
   as
declare variable pozfakref integer;
begin
  for select dn.ref, dn.akcept, dn.symbol, dn.wartosc
        from pozfak pf
        join dokumpoz dp on (pf.ref = dp.frompozfak)
        join dokumnag dn on (dn.ref = dp.dokument)
        where pf.dokument = :fak
  into :ref, :akceptacja, :symbol, :wartosc
  do begin
    if(:akceptacja=0) then kindstr = 'MI_REDX';
    else if(:akceptacja=1 or :akceptacja=8) then kindstr = '14';
    else if(:akceptacja=9 or :akceptacja=10) then kindstr = 'MI_PADLOCK';
    suspend;
  end

  for select distinct dn.ref, dn.akcept, dn.symbol, dn.wartosc
        from dokumnag dn
        where dn.faktura = :fak
  into :ref, :akceptacja, :symbol, :wartosc
  do begin
    if(:akceptacja=0) then kindstr = 'MI_REDX';
    else if(:akceptacja=1 or :akceptacja=8) then kindstr = '14';
    else if(:akceptacja=9 or :akceptacja=10) then kindstr = 'MI_PADLOCK';
    suspend;
  end

end^
SET TERM ; ^
