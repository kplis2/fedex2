--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDE_CALC_TOLEFT(
      PRSCHEDGUIDE integer)
   as
declare variable curtime timestamp;
declare variable tstart timestamp;
declare variable tdepend timestamp;
declare variable tend timestamp;
declare variable machine integer;
declare variable prdepart varchar(20);
declare variable minspace integer;
declare variable round integer;
declare variable minlength double precision;
declare variable tstartfwt timestamp;
declare variable prschdate timestamp;
declare variable prschmachine smallint;
declare variable worktime double precision;
declare variable schedoper integer;
begin
  --exception test_break;
  select a.mintimebetweenopers, a.roundstarttime, a.fromdate, a.prdepart
    from prschedules a
      join prschedzam b on (a.prdepart = b.prdepart)
      join prschedguides g on (g.prschedzam = b.ref)
    where g.ref = :PRSCHEDGUIDE
    into :minspace, :round, :curtime, :prdepart;

--  if(:curtime is null /*or (:curtime < current_timestamp(0)) */)then
  --  curtime = current_timestamp(0);

  minlength = 1440;
  minlength = 1/:minlength;
  if(:round > :minspace or (:minspace is null) ) then minspace = coalesce(:round,0);
  minlength = :minlength * :minspace;

  update prschedopers p set p.verified = 0, p.starttime = null, p.endtime = null, p.calcdeptimes = 0
    where p.guide = :prschedguide;

  for select a.ref, a.MACHINE, a.worktime, g.prschdate, g.prschmachine
    from PRSCHEDOPERS a
      join prschedguides g on (g.ref = a.guide)
    where g.ref = :prschedguide
      and coalesce(a.workplace,'') <> ''
      and a.worktime > 0
      and a.activ = 1
      and coalesce(a.prschedcalcblock,0) = 0
    order by a.number
    into :schedoper, :machine, :worktime, :prschdate, :prschmachine
  do begin
    if (:prschdate is null) then
      prschdate = current_timestamp(0);

    --okreslenie minimalnego czasu rozpoczecia
    tdepend = null;
    select max(o.endtime)
      from prsched_calc_next_oper(:schedoper,null,1) p
        join prschedopers o on (o.ref = p.rprschedoper)
      where o.activ = 1
      into :tdepend;

    tstart = :prschdate;

    if(:tdepend is null) then tdepend = :prschdate;
    if(:tstart is null or :tstart < :tdepend )then
        tstart = :tdepend + :minlength;

    --sprawdzenie, czy start nie wypad w czasie przerwy pracy - jesli tak, to na poczatek kolejnego czasu
    tstartfwt = null;
    execute procedure PRSCHED_CALENDAR_FIRSTWORKTIME(:prdepart,:tstart)
      returning_values :tstartfwt;

    tstart = null;
    tend = null;
    machine = null;

    --wywolac jezeli ma uwzgledniac maszyny, jezeli nie to tstart = tstartfwt; tend = tstart + worktime;
    if (:prschmachine = 1) then begin
      select first 1 starttime, endtime, machine  --pobranie pierwszego wolnegi i pasujacego terminu
        from prsched_find_machine(:schedoper, :tstartfwt, 0)
        order by starttime
        into :tstart,  :tend, :machine;
    end else begin
      tstart = :tstartfwt;
      tend = :tstart + :worktime;
      machine = null;
    end

    update prschedopers set STARTTIME = :tstart, ENDTIME = :tend, MACHINE = :machine, verified = 1, calcdeptimes = 1
      where ref = :schedoper;
  end
end^
SET TERM ; ^
