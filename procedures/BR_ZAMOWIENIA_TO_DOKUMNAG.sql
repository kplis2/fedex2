--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_ZAMOWIENIA_TO_DOKUMNAG(
      REFDOK integer)
  returns (
      REF integer,
      SYMBOL varchar(40) CHARACTER SET UTF8                           ,
      REJESTR varchar(3) CHARACTER SET UTF8                           )
   as
declare variable org_ref integer;
begin
  for select P.FROMZAM,Z.ID,Z.REJESTR,Z.ORG_REF
    from DOKUMPOZ P
    join NAGZAM Z on (Z.REF=P.FROMZAM)
    where P.DOKUMENT=:refdok
    into :ref, :symbol, :rejestr, :org_ref
  do begin
    if(:org_ref <> :ref) then begin
      select REF, ID, REJESTR from NAGZAM where ref=:org_ref into :ref, :symbol, :rejestr;
    end
    suspend;
  end
end^
SET TERM ; ^
