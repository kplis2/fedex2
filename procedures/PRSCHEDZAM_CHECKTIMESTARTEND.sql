--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDZAM_CHECKTIMESTARTEND(
      SCHEDZAM integer)
   as
declare variable starttime timestamp;
declare variable endtime timestamp;
declare variable zam integer;
begin
  select min(starttime), max(endtime)
    from PRSCHEDguides
    where prschedzam  = :schedzam
    into :starttime, :endtime;
  update PRSCHEDzam set starttime = :starttime, endtime = :endtime
  where ref = :schedzam;
  select prschedzam.zamowienie from prschedzam where REF=:schedzam into :zam;
  execute procedure nagzam_termstartdost(:zam);
end^
SET TERM ; ^
