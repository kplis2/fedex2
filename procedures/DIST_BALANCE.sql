--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DIST_BALANCE(
      DREF integer)
  returns (
      AMOUNT numeric(14,2))
   as
declare variable distamount numeric(14,2);
declare variable decreeamount numeric(14,2);
begin
  select coalesce(sum(debit+credit),0) from distpos where decrees = :dref into :distamount;
  select coalesce(debit+credit,0) from decrees where ref=:dref into :decreeamount;
  amount = decreeamount - distamount;
  if(amount < 0) then amount = 0;
  suspend;
end^
SET TERM ; ^
