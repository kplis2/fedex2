--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INSERT_RELATION(
      RELATIONDEF integer,
      STABLEFROM varchar(40) CHARACTER SET UTF8                           ,
      SREFFROM integer,
      STABLETO varchar(40) CHARACTER SET UTF8                           ,
      SREFTO integer,
      QUANTITYFROM numeric(15,4),
      QUANTITYTO numeric(14,4))
   as
declare variable rref integer;
begin
  if (srefto is null or sreffrom is null) then
    exit;
  select first 1 ref
    from relations
    where relationdef = :relationdef and sreffrom = :sreffrom and stablefrom = :stablefrom
      and srefto = :srefto and stableto = :stableto and act = 1
    into rref;
  if (rref is null) then begin
    insert into relations (relationdef, stablefrom, sreffrom, stableto, srefto, quantityfrom, quantityto, act)
      values (:relationdef, :stablefrom, :sreffrom, :stableto, :srefto, :quantityfrom, :quantityto, 1);
  end
  else if (quantityfrom is not null and quantityto is not null) then
  begin
    update relations set quantityfrom = quantityfrom + :quantityfrom, quantityto = quantityto + :quantityto
      where ref = :rref;
  end else if (quantityfrom is not null and quantityto is null) then
  begin
    update relations set quantityfrom = quantityfrom + :quantityfrom where ref = :rref;
  end else if (quantityfrom is null and quantityto is not null) then
  begin
    update relations set quantityto = quantityto + :quantityto where ref = :rref;
  end   
end^
SET TERM ; ^
