--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BROWSE_DECREES(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      ACCOUNT ACCOUNT_ID)
  returns (
      DESCTYPE varchar(40) CHARACTER SET UTF8                           ,
      DEBIT numeric(14,2),
      CREDIT numeric(14,2))
   as
declare variable NEWDEBIT numeric(14,2);
declare variable NEWCREDIT numeric(14,2);
declare variable NEWTYPE varchar(40);
begin
  desctype = '';
  debit = 0;
  credit = 0;
  for select debit,credit,substring(descript from 1 for 3) from DECREES
    where PERIOD=:period
    and ACCOUNT=:account and STATUS>1
    order by descript,transdate
    into :newdebit,:newcredit,:newtype
    do begin
      if(:newtype<>:desctype) then begin
        if (desctype<>'') then suspend;
        desctype =:newtype;
        debit = 0;
        credit = 0;
      end

      if(:desctype='' ) then begin
                              desctype = :newtype;
                              debit=0;
                              credit=0;
                              end
      debit = debit + newdebit;
      credit = credit + newcredit;
    end
    suspend;
end^
SET TERM ; ^
