--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOSTAWCA_TOWARY(
      REFD integer)
  returns (
      KTMDOST varchar(40) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      JEDNOSTKA varchar(100) CHARACTER SET UTF8                           ,
      CENAWAL numeric(14,4),
      WALUTA varchar(3) CHARACTER SET UTF8                           ,
      KTM varchar(80) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE REF INTEGER;
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE DATA DATE;
DECLARE VARIABLE GRUPA VARCHAR(255);
DECLARE VARIABLE GRUPACENZEW INTEGER;
DECLARE VARIABLE DOSTAWCA VARCHAR(255);
DECLARE VARIABLE SLODEF INTEGER;
DECLARE VARIABLE SLOPOZ INTEGER;
DECLARE VARIABLE POTENCJALNY SMALLINT;
DECLARE VARIABLE SLODEFREF INTEGER;
begin
  FOR SELECT TOWARY.KTM FROM Towary
  order by TOWARY.KTM INTO :KTM
  DO BEGIN
    select max(datawe) from nagzam
      join pozzam on (pozzam.zamowienie = nagzam.ref)
      where nagzam.typzam = 'DST' and pozzam.ktm = :ktm AND NAGZAM.dostawca = :REFD
      into :data;
    select max(pozzam.ref) from pozzam
      join nagzam on (pozzam.zamowienie = nagzam.ref)
      where nagzam.datawe = :data and pozzam.ktm = :ktm  AND NAGZAM.dostawca = :REFD
      into :ref;
    select DOSTAWCY.id, towary.nazwa, towjedn.jedn, pozzam.cenanet, nagzam.waluta
      from pozzam
      join towary on (pozzam.ktm = towary.ktm)
      join towjedn on (towjedn.ref = pozzam.jedn)
      join nagzam on (pozzam.zamowienie = nagzam.ref)
      JOIN dostawcy ON (NAGZAM.dostawca = DOSTAWCY.ref)
      where pozzam.ref = :ref AND NAGZAM.dostawca = :REFD
      into :DOSTAWCA, :nazwa, :jednostka, :cenawal, :waluta;
    select count(*) from pozzam join nagzam on (pozzam.zamowienie = nagzam.ref)
      where pozzam.ktm = :KTM and nagzam.dostawca = :refd into :cnt;
    if (:cnt > 0) then
    begin
      potencjalny = 0;
      KTMDOST = :KTM;
      suspend;
    end
  END
  select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'DOSTAWCY' into :slodefref;
  for
    select cennikizewn.grupa, cennikizewn.ktm, cennikizewn.cena_zakn,
        cennikizewn.waluta, towary.nazwa, cennikizewn.miara, cennikizewn.slodef, cennikizewn.slopoz
      from cennikizewn
      left join towary on (cennikizewn.ktm = towary.ktm)
      where (cennikizewn.slodef = :slodefref or cennikizewn.slodef = 2032) and cennikizewn.slopoz = :refd
      and cennikizewn.ktm is not null
      into :GRUPACENZEW, :KTMDOST, :CENAWAL, :WALUTA, :NAZWA, :JEDNOSTKA, :SLODEF, :SLOPOZ
  do begin
    select slopoz.nazwa from slopoz where slopoz.slownik = :slodef and slopoz.ref = :SLODEF 
      into :DOSTAWCA;
    POTENCJALNY = 1;
    suspend;
  end
  select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'DOSTAWCY' into :slodefref;
  for
    select cennikizewn.grupa, cennikizewn.ktm, cennikizewn.cena_zakn,
        cennikizewn.waluta, cennikizewn.miara, cennikizewn.slodef, cennikizewn.slopoz
      from cennikizewn
      where (cennikizewn.slodef = :slodefref or cennikizewn.slodef = 2032) and cennikizewn.slopoz = :refd
      and cennikizewn.grupa is not null
      into :GRUPACENZEW, :KTMDOST, :CENAWAL, :WALUTA, :JEDNOSTKA, :SLODEF, :SLOPOZ
  do begin
    select slopoz.nazwa from slopoz where slopoz.slownik = :slodef and slopoz.ref = :SLODEF 
      into :DOSTAWCA;
    POTENCJALNY = 1;
    select defcechw.wartstr from defcechw where defcechw.id_defcechw = :grupacenzew
    into :grupa;
      for select towary.ktm from towary where towary.grupa = :grupa
      order by towary.ktm
      into :KTMDOST
      do begin
        suspend;
      end
  end
end^
SET TERM ; ^
