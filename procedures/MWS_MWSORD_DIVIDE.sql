--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_MWSORD_DIVIDE(
      MWSORD integer,
      MWSACTSQ integer)
  returns (
      MWSORDSCNT integer)
   as
declare variable actscnt integer;
declare variable refmwsord integer;
declare variable period varchar(6);
declare variable number integer;
declare variable mwsactref integer;
begin
  if (mwsactsq is null) then mwsactsq = 0;
  if (mwsactsq = 0) then
    exception MWSORD_CANT_BE_DIVIDED 'Podano ilosc operacji 0!';
  select count(ref) from mwsacts where mwsord = :mwsord and status < 5
    into actscnt;
  if (actscnt is null) then actscnt = 0;
  if (actscnt = 0 or actscnt < 2 * mwsactsq) then
  begin
    mwsordscnt = 1;
    exit;
  end
  mwsordscnt = 1;
  select okres from datatookres(current_date,0,0,0,0) into period;
  while (actscnt > 0)
  do begin
    if (actscnt < 2 * mwsactsq) then
      actscnt = 0;
    if (actscnt > 0) then
    begin
      execute procedure gen_ref('MWSORDS') returning_values refmwsord;
      insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator, priority,
          description, wh,  regtime, timestartdecl, mwsaccessory, branch, period,
          flags, docsymbs, cor, rec, bwharea, ewharea, status, shippingarea, SHIPPINGTYPE, takefullpal,
          slodef, slopoz, slokod, palquantity, repal)
        select :refmwsord, mwsordtype, stocktaking, doctype, docgroup, docid, operator, priority,
            'Podzial zlecenia', wh,  regtime, timestartdecl, mwsaccessory, branch, :period,
            flags, docsymbs, cor, rec, bwharea, ewharea, 3, shippingarea, SHIPPINGTYPE, takefullpal,
            slodef, slopoz, slokod, palquantity, repal
          from mwsords
          where ref = :mwsord;
      number = 0;
      mwsordscnt = mwsordscnt + 1;
      for
        select ref
          from mwsacts where mwsord = :mwsord and status < 5
          order by number desc
          into mwsactref
      do begin
        number = number + 1;
        update mwsacts set mwsord = :refmwsord, number = :number where ref = :mwsactref;
        if (number >= mwsactsq) then
          break;
      end
      update mwsords set status = 3 where ref = :refmwsord;
      actscnt = actscnt - number;
    end
  end
end^
SET TERM ; ^
