--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MASTERLINK(
      SPOSD integer,
      SPOSZ integer,
      WAGA numeric(14,2),
      KODPOCZ varchar(10) CHARACTER SET UTF8                           ,
      SUMWARTNET numeric(14,2),
      SUMWARTBRU numeric(14,2),
      FLAGI varchar(255) CHARACTER SET UTF8                           ,
      SPEDYTOR varchar(80) CHARACTER SET UTF8                           )
  returns (
      KOSZTC numeric(14,2),
      KOD varchar(10) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE KOSZTM NUMERIC(14,2);
DECLARE VARIABLE ILPACZEKNUM NUMERIC(14,2);
DECLARE VARIABLE ILPACZEK INTEGER;
DECLARE VARIABLE SOBOTA INTEGER;
DECLARE VARIABLE NIEDZIELA INTEGER;
DECLARE VARIABLE DODZIEWIATEJ INTEGER;
DECLARE VARIABLE DODWUNASTEJ INTEGER;
begin
  if (sobota is null) then sobota=0;
  if (niedziela is null) then niedziela=0;
  if (dodziewiatej is null) then dodziewiatej=0;
  if (dodwunastej is null) then dodwunastej=0;

  select spedodleg.kodrejonu from spedodleg
    where spedodleg.kodp = :kodpocz and spedodleg.spedytor = :sposd into :kod;
  kosztm = 12;
  ilpaczeknum = (waga/30);
  ilpaczek = cast(:ilpaczeknum as integer);
  if (ilpaczeknum>ilpaczek) then ilpaczek = ilpaczek + 1;
  kosztc = (kosztm * ilpaczek) * (1 + sobota*0.5 + niedziela*1 + dodziewiatej*0.3 + dodwunastej*1);
  suspend;
end^
SET TERM ; ^
