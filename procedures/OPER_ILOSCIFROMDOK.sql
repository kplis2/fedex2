--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_ILOSCIFROMDOK(
      ZAM integer,
      DOKUM integer,
      TYP char(1) CHARACTER SET UTF8                           ,
      HISTZAM integer,
      OPER integer)
  returns (
      ILPOZ integer)
   as
declare variable pozref integer;
declare variable ktm varchar(40);
declare variable wersjaref integer;
declare variable wersja integer;
declare variable oldilreal numeric(14,4);
declare variable ilosc numeric(14,4);
declare variable jedn integer;
declare variable ilreal numeric(14,4);
declare variable ilrealr numeric(14,4);
declare variable przelicz numeric(14,4);
declare variable pozdokref integer;
declare variable iloscadd numeric(14,4);
declare variable korekta integer;
declare variable redokladnie integer;
declare variable zamzrodl integer;
declare variable pozzamref integer;
declare variable ktmguidepos varchar(80);
declare variable ktmpozzam varchar(80);
declare variable pozzamrefold integer;
declare variable guidespos integer;
declare variable pozzamnewref integer;
declare variable iloscold numeric(14,4);
declare variable stan char(1);
begin
  ILPOZ = 0;
  if(:typ = 'G') then begin
    select amount - amountzreal from PRSCHEDGUIDES where REF = :dokum into :ilreal;
    update nagzam set KILREAL = :ilreal where ref = :zam;
    update POZZAM set ILREAL = 0, ILREALM = 0,  ILREALO = 0  where zamowienie = :zam;
    ilpoz =1;
  end else if(:typ = 'P') then begin
    for
      select pgp.ref, pgp.amount - pgp.amountzreal, pgp.pozzamref, pgp.ktm,
          p.ktm, pgp.pozzamrefold, pgp.pozzamamountold
        from PRSCHEDGUIDESPOS pgp
          left join pozzam p on (pgp.pozzamref = p.ref)
        where pgp.PRSCHEDGUIDE = :dokum and (coalesce(:oper,0) = 0 or :oper = pgp.prschedoper)
        into :guidespos, :ilosc, :pozzamref, :ktmguidepos,
          :ktmpozzam, :pozzamrefold, :iloscold
    do begin
      ilpoz = ilpoz + 1;
      --realizacja ktmu wg przewodnika
      if(ktmguidepos = ktmpozzam and pozzamrefold is null) then
        update pozzam set ilreal = :ilosc  where ref = :pozzamref;
      else begin
        if(iloscold is null) then iloscold = 0;
      --jesli powrot do pierwotnego ktmu
        if(pozzamrefold is not null) then
          select ktm from pozzam where ref = :pozzamrefold into :ktmpozzam;
        if(ktmguidepos = ktmpozzam) then begin
          update pozzam set ilreal = :ilosc, ilosc = ilosc + :iloscold where ref = :pozzamrefold;
          update prschedguidespos set pozzamref = :pozzamrefold, pozzamrefold = null, pozzamamountold = 0  where ref = :guidespos;
          if(pozzamrefold is not null) then delete from pozzam where ref = :pozzamref;
        end else begin
       --zmiana ktmu
          if(pozzamrefold is not null) then delete from pozzam where ref = :pozzamref;
          if(pozzamrefold is null) then pozzamrefold = pozzamref;
          update pozzam set ilosc = ilosc - :ilosc + :iloscold where ref = :pozzamrefold;
          execute procedure GEN_REF('POZZAM') returning_values :pozzamnewref;
          insert into pozzam(ref, zamowienie, magazyn, ktm, kilosc, ilosc, ilreal, popref, out, wersja, prshmat)
          select :pozzamnewref, zamowienie, magazyn, :ktmguidepos, kilosc, :ilosc, :ilosc, popref, out, wersja, prshmat
            from pozzam where ref = :pozzamrefold;
          select stan from pozzam where ref = :pozzamnewref into :stan;
          update pozzam set stan = :stan where ref = :pozzamnewref;
          update prschedguidespos set pozzamref = :pozzamnewref, pozzamrefold = :pozzamrefold, pozzamamountold = :ilosc where ref = :guidespos;
        end
      end
      pozzamrefold = null;
      iloscold = 0;
    end
  end else
  for select ref, ktm, wersja, wersjaref, ilrealm,iloscm - ilzrealm,jedn
  from POZZAM where zamowienie = :zam
     into :pozref,:ktm, :wersja, :wersjaref, :oldilreal,:ilosc,:jedn
  do begin
     if(:typ = 'M') then begin
       select defdokum.koryg from DEFDOKUM join DOKUMNAG on (DOKUMNAG.TYP = DEFDOKUM.symbol) where DOKUMNAG.REF=:dokum into :korekta;
       select sum(ILOSC) from DOKUMPOZ where DOKUMENT = :dokum
         and WERSJAREF = :WERSJAREF and FROMZAM = :ZAM
         and (FROMPOZZAM = :pozref)
         into :ilreal;
       if(:ilreal is null) then ilreal = 0;
       if(:ilreal < :ilosc) then begin
         for select REF, ILOSC
           from DOKUMPOZ
           where DOKUMENT = :dokum and WERSJAREF = :wersjaref
             and (FROMZAM is null and FROMPOZZAM = :pozref)
           into :pozdokref, :iloscadd
         do begin
           if(:ilreal < :ilosc) then begin
             if(:iloscadd > 0) then begin
               if(:korekta = 1) then
                 iloscadd = :iloscadd * -1;
               if(:iloscadd > (:ilosc - :ilreal)) then
                 iloscadd = :ilosc - :ilreal;
               ilreal = :ilreal + :iloscadd;
               update DOKUMPOZ set FROMZAM = :zam, FROMPOZZAM = :pozref where ref = :pozdokref;
             end
           end
         end
         if(:ilreal < :ilosc) then begin
           for select REF, ILOSC
             from DOKUMPOZ
             where DOKUMENT = :dokum and WERSJAREF = :wersjaref
               and (
                 ((FROMZAM is null and FROMPOZZAM is null) or (FROMZAM = 0) or (FROMZAM = :zam and frompozzam is null))
                   or (FROMZAM in (select ref from NAGZAM where REFZRODL = :zam ))
                 )
             into :pozdokref, :iloscadd
           do begin
             if(:ilreal < :ilosc) then begin
               if(:iloscadd > 0) then begin
                 if(:korekta = 1) then
                   iloscadd = :iloscadd * -1;
                 if(:iloscadd > (:ilosc - :ilreal)) then
                   iloscadd = :ilosc - :ilreal;
                 ilreal = :ilreal + :iloscadd;
                 update DOKUMPOZ set FROMZAM = :zam, FROMPOZZAM = :pozref where ref = :pozdokref;
               end
             end
           end
         end
       end
     end else begin
       select sum(ILOSCM - PILOSCM) from POZFAK where DOKUMENT = :dokum
         and WERSJAREF = :WERSJAREF and FROMZAM = :ZAM
         and (FROMPOZZAM = :pozref or FROMPOZZAM is null)
         into :ilreal;
       if(:ilreal is null) then ilreal = 0;
       if(:ilreal < :ilosc) then begin
         for select REF, ILOSCM - PILOSCM
           from POZFAK
           where DOKUMENT = :dokum and WERSJAREF = :wersjaref
             and ((FROMZAM is null) or (FROMZAM = 0))
           into :pozdokref, :iloscadd
         do begin
           if(:ilreal < :ilosc) then begin
             if(:iloscadd > 0) then begin
               if(:iloscadd > (:ilosc - :ilreal)) then
                 iloscadd = :ilosc - :ilreal;
               ilreal = :ilreal + :iloscadd;
               update POZFAK set FROMZAM = :zam where ref = :pozdokref;
             end
           end
         end
       end
     end
     if(:ilreal>0 and :ilreal < :ilosc) then begin
         --zamiana magazynowych na rozliczeniowe
         przelicz = null;
         select PRZELICZ from TOWJEDN where ref =:jedn into :przelicz;
         if(:przelicz > 0) then
           ilrealr = :ilreal / :przelicz;
         else
           ilrealr = :ilreal;
         update POZZAM set ILREAL = :ilrealr,ILREALM = 0, ILREALO = 0
           where ref = :pozref and ILREAL<>:ilrealr;
         if(:ilrealr <> 0) then
           ilpoz = :ilpoz + 1;
     end else if(:ilreal>0) then begin
         update POZZAM set ILREAL = ILOSC - ILZREAL,ILREALM = 0, ILREALO = 0
           where ref = :pozref and ILREAL<>ILOSC-ILZREAL;
         ilpoz = :ilpoz + 1;
     end
       --usuniecie rekordów dyspozycji, jakie mogly pozostac z poprzedniej operacji
     delete from POZZAMDYSP where POZZAM = :pozref;
  end
  --powiązanie dok magazynowego z realizowanym zamówieniem
  if(:typ = 'M') then begin
    if(:ilpoz>0 and not exists(select * from DOKUMNAG where REF=:dokum and ZAMOWIENIE=:zam)) then begin
      update DOKUMNAG set ZAMOWIENIE = :zam, HISTORIA = :histzam where ref=:DOKUM and ZAMOWIENIE is null;
      update HISTZAM set histzam.refromdokref = :dokum where ref=:histzam ;
    end
    select defoperzam.refromdokumdokladny from defoperzam join histzam on (histzam.operacja = defoperzam.symbol)
      where histzam.ref=:histzam into :redokladnie;
    if(:redokladnie = 1) then begin
      ktm = '';
      select first 1 DOKUMPOZ.KTM
        from DOKUMPOZ
        where DOKUMENT = :dokum
          and (FROMPOZZAM is null or (FROMZAM <> :zam))
      into :ktm;
      if(:ktm <> '') then
        exception OPER_ILOSCIFROMDOK_EXPT 'Pozycja dok. '||:ktm||' nie posiada pozycji na zamowieniu.';
      select first 1 max(POZZAM.KTM)
        from POZZAM join DOKUMPOZ on (DOKUMPOZ.DOKUMENT = :dokum and DOKUMPOZ.FROMPOZZAM = POZZAM.REF)
        where POZZAM.ZAMOWIENIE = :zam
        group by pozzam.ref
        having max(abs(pozzam.ilrealm)) <> sum(dokumpoz.ilosc)
      into :ktm;
      if(:ktm <> '') then
        exception OPER_ILOSCIFROMDOK_EXPT 'Pozycja zam. '||:ktm||' jest realizaowana za duzo.';
      select first 1 POZZAM.KTM
        from POZZAM
        where ZAMOWIENIE = :zam
          and (ILZREAL + ILREAL < 0)
      into :ktm;
      if(:ktm <> '') then
        exception OPER_ILOSCIFROMDOK_EXPT 'Pozycja zam. '||:ktm||' jest realizowana ujemnie.';
    end
  end else if(:typ = 'G') then begin
    ilpoz = 1;
  end
end^
SET TERM ; ^
