--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHECK_MULTISELECT(
      CLIST STRING8191,
      CTYPE smallint)
  returns (
      RET smallint)
   as
declare variable NUMBEROFTYPES integer;
declare variable MAXEMPLTYPE integer;
begin
/*MWr: Personel - Procedura ma pomoc weryfikowac elementy multiselekcji przed
       wykonaniem jakies operacji na nich. Parametry:
        > CTYPE - typ sprawdzenia
        > CLIST - lista elementoww w postaci '|Ref1|Ref2|Ref3|...|RefN|', gdzie
                  symbol "|" musi być na poczatku i koncu listy   */
  ret = 1;

--sprawdzenie unikalnosci szablonu wydruku dla wybranych umow o prace
  if (ctype = 0) then
  begin
    select count(distinct t.printerpattern), max(empltype)
      from econtrtypes t
      where :clist like '%|' || t.contrtype || '|%'
      into :numberoftypes, :maxempltype;

    if (numberoftypes > 1) then
      ret = 0;
    else if (maxempltype > 1 and position('|' in trim('|' from clist)) > 1) then
      exception universal 'Wykonanie zbiorczego wydruku możliwe jest tylko dla umów o pracę.';
  end

--sprawdzenie unikalnosci szablonu wydruku dla wybranych aneksow
  else if (ctype = 1) then
  begin
    select count(distinct a.printerpattern)
      from econtrannexestypes a
      where :clist like '%|' || cast(a.ref as varchar(10)) || '|%'
      into :numberoftypes;

    if (numberoftypes > 1) then
      ret = 0;
  end

--statusy rachunkow UCP (PR55960)
  else if (ctype = 2) then
  begin
    numberoftypes = position( '0' in :clist);

    if (numberoftypes <> 0) then
      ret = 0;
  end

  suspend;
end^
SET TERM ; ^
