--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_LISTYWYSDTOFAK(
      FAK integer)
  returns (
      REF integer,
      AKCEPTACJA smallint,
      SYMBOL varchar(255) CHARACTER SET UTF8                           ,
      WARTOSC numeric(14,2),
      KINDSTR varchar(20) CHARACTER SET UTF8                           )
   as
declare variable POZFAK_REF integer;
begin
  -- zwiazane przez dokumenty magazynowe
  for select l.ref, l.akcept, substring(l.symbol||' ('||l.symbolsped||' - '||coalesce(s.opis,'')||')' from 1 for 255)
       from listywysdpoz p
         join dokumpoz dp on (dp.ref = p.dokpoz)
         join pozfak pf on (dp.frompozfak = pf.ref)
         left join listywysd l on (l.ref = p.dokument)
         left join sposdost s on (s.ref = l.sposdost)
         where pf.dokument = :fak
         group by l.ref, l.akcept, l.symbol||' ('||l.symbolsped||' - '||coalesce(s.opis,'')||')'
  into :ref, :akceptacja, :symbol
  do begin
    if(:akceptacja=0) then kindstr = 'MI_REDX';
    else if(:akceptacja=1) then kindstr = '14';
    else if(:akceptacja=2) then kindstr = 'MI_PADLOCK';
    suspend;
  end
end^
SET TERM ; ^
