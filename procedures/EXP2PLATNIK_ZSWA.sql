--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXP2PLATNIK_ZSWA(
      EMPLOYEE_REF integer,
      T_FROMDATE date,
      T_TODATE date,
      COMPANY integer)
  returns (
      ROWNUMBER integer,
      PERSON integer,
      SNAME varchar(30) CHARACTER SET UTF8                           ,
      FNAME varchar(60) CHARACTER SET UTF8                           ,
      FROMDATE date,
      TODATE date,
      CODE varchar(20) CHARACTER SET UTF8                           ,
      DIMNUM smallint,
      DIMDEN smallint,
      ICODE varchar(20) CHARACTER SET UTF8                           ,
      PESEL varchar(11) CHARACTER SET UTF8                           ,
      NIP varchar(15) CHARACTER SET UTF8                           ,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      DOCNO varchar(11) CHARACTER SET UTF8                           ,
      BIRTHDATE date,
      ISNEWSECTION smallint)
   as
declare variable SFROMDATE date;
declare variable STODATE date;
declare variable ZFROMDATE date;
declare variable ZTODATE date;
declare variable PERSON_TEMP integer;
declare variable PERSON_REF integer;
declare variable ICODE_TEMP integer;
declare variable SFROMDATE_TEMP date;
declare variable STODATE_TEMP date;
declare variable SECTIONROWS smallint;
begin
/*MWr: Personel - Eksport do Platnika ZUS ZSWA (zgloszenie danych o pracy
                  w szczegolnych warunkach lub o szczegolnym charkterze)

  EMPLOYEE_REF - nr pracownika do okreslenia osoby, dla ktorej gener. deklaracje
  T_FROMDATE,T_TODATE - ramy czasowe okresu, za ktory skladamy deklaracje
  ISNEWSECTION - parametr wyjsciowy, jezeli = 0 oznacza, ze wygenerowy wiersz
                 dotyczy, jak wygenerowany wiersz wyzej, tej samej osoby, o tym
                 samym kodzie ubezpieczeniowym (o ile nie jest to 5 wiersz z kolei)*/

  nip = ''; --PR57778
  rownumber = 0;
  person_temp = 0;
  employee_ref = coalesce(employee_ref,0);
  if (employee_ref > 0) then
  begin
    select person from employees
      where ref = :employee_ref
      into :person_ref;
  end

  for
    select person, sfromdate, stodate, code, dimnum, dimden
      from get_especconditwork(:person_ref, :t_fromdate, :t_todate, :company, 0, 1) --BS70985
      order by personnames, person, employee, sfromdate
    into :person, :sfromdate, :stodate, :code, :dimnum, :dimden
  do begin

    --ograniczenie okresow pracy w szcz. warunkach do okresu deklaracji
    if (sfromdate < t_fromdate) then
      sfromdate = t_fromdate;
    if (stodate > t_todate or stodate is null) then
      stodate = t_todate;

    --grupowanie danych wg kodu ubezpieczenia
    sfromdate_temp = sfromdate;
    stodate_temp = stodate;
    for
      select d.fromdate, d.todate, z1.code || z2.code || z3.code, p.pesel, p.fname, p.sname, p.birthdate,
          case when (d.isother = 1 and p.evidenceno > '') then '1' when (d.isother = 2 and p.passportno > '') then '2' else '' end as doctype,
          case when (d.isother = 1 and p.evidenceno > '') then p.evidenceno when (d.isother = 2 and p.passportno > '') then p.passportno else '' end as docno
        from ezusdata d
          join persons p on (d.person = p.ref)
          left join edictzuscodes z1 on (z1.ref = d.ascode)
          left join edictzuscodes z2 on (z2.ref = d.retired)
          left join edictzuscodes z3 on (z3.ref = d.invalidity)
        where d.person = :person
          and d.fromdate <= :stodate
          and (d.todate >= :sfromdate or d.todate is null)
        order by d.fromdate
        into zfromdate, :ztodate, :icode, :pesel, :fname, :sname, :birthdate, :doctype, :docno
    do begin

      --ograniczenie okresu obowiazywania kodu ubezp. do okresu deklaracji
      if (zfromdate < t_fromdate) then
        zfromdate = t_fromdate;
      if (ztodate > t_todate or ztodate is null) then
        ztodate = t_todate;

      --koniunkcja okresow ubezpieczenia i pracy w szczegol. warunkach
      if (sfromdate_temp >= zfromdate and stodate_temp <= ztodate) then
      begin
         fromdate = sfromdate_temp;
         todate = stodate_temp;
      end else
      if (sfromdate_temp >= zfromdate and stodate_temp > ztodate) then
      begin
        fromdate = sfromdate_temp;
        sfromdate_temp = ztodate + 1;
        todate = ztodate;
      end else
      if (sfromdate_temp < zfromdate and stodate_temp <= ztodate) then
      begin
        fromdate = zfromdate;
        todate = stodate_temp;
      end else
      begin
        fromdate = zfromdate;
        todate = ztodate;
      end

     /*nowa sekcje (pkt od III do VI deklaracji) definuje: zmiana ubezpieczonego,
      zmiana kodu ubezp, wiecej niz 4 wpisy o war. szcz. pracy dla tej samej osoby*/
      if (person_temp <> person or icode <> icode_temp or sectionrows = 4) then
      begin
        isnewsection = 1;
        sectionrows = 1;
      end else
      begin
        isnewsection = 0;
        sectionrows = sectionrows + 1;
      end
      person_temp = person;
      icode_temp = icode;

      rownumber =  rownumber + 1;
      suspend;
    end
  end
end^
SET TERM ; ^
