--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FIND_LISTYWYSD_PODMIOT(
      REFLISTYWYSD integer)
  returns (
      CPODMIOT integer)
   as
DECLARE VARIABLE SLODEF INTEGER;
DECLARE VARIABLE SLOPOZ INTEGER;
begin
  select listywysd.slodef, listywysd.slopoz from listywysd where listywysd.ref = :REFLISTYWYSD
    into :slodef, :slopoz;
  select cpodmioty.ref from cpodmioty where cpodmioty.slodef = :slodef and cpodmioty.slopoz = :slopoz
    into :cpodmiot;
  if (:cpodmiot is null) then
    cpodmiot = 0;
  suspend;
end^
SET TERM ; ^
