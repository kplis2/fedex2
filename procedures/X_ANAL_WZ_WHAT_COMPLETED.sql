--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_ANAL_WZ_WHAT_COMPLETED(
      GRUPASPED DOKUMNAG_ID)
  returns (
      REFPOZ DOKUMPOZ_ID,
      DOKUMENT DOKUMNAG_ID,
      SYMBOLDOK SYMBOL_ID,
      KTM KTM_ID,
      NAZWA TOWARY_NAZWA,
      ILOSC ILOSCI_MAG,
      ILOSCL ILOSCI_MAG,
      ILOSCONMWSACTS ILOSCI_MAG,
      ILOSCONMWSACTSC ILOSCI_MAG,
      ILOSCN ILOSCI_MAG,
      POBRANIE CENA_ID,
      SPOSZAP integer)
   as
declare variable MODE SMALLINT_ID;
begin
  -- analiza pokazująca stan zlozenia danej grupy spedycyjnej lub wszystkich niezlozonych lub niewygenerowanych pozycji
  --ZG 133732
  -- mode 1 pokazuje wszystkie WZ
  -- mode 2 pokazuje zadana grupe spedycyjna
  if (GRUPASPED is null) then
    MODE = 1;
  else
    MODE = 2;
  --mode 1
  if (MODE = 1) then
  begin
    for select DN.SYMBOL, DN.REF, DP.REF, DP.KTM, T.NAZWA, DP.ILOSCL - DP.ILOSCONMWSACTSC, DP.ILOSCL, DP.ILOSCONMWSACTS,
               DP.ILOSCONMWSACTSC, DP.ILOSC, DN.x_pobranie, dn.sposzap --kpi
        from DOKUMNAG DN
        join DOKUMPOZ DP on (DP.DOKUMENT = DN.REF)
        join TOWARY T on (T.KTM = DP.KTM)
        where (DP.ILOSCL <> DP.ILOSCONMWSACTSC) and
              DN.TYP = 'WZ' and
              DN.AKCEPT = 1
        order by DN.REF, DN.GRUPASPED
        into :SYMBOLDOK, :DOKUMENT, :REFPOZ, :KTM, :NAZWA, :ILOSCN, :ILOSCL, :ILOSCONMWSACTS, :ILOSCONMWSACTSC, :ILOSC,
             :pobranie, :sposzap --kpi
    do
    begin
      suspend;
    end
  end

  -- mode 2
  else
  if (MODE = 2) then
  begin
    for select DN.SYMBOL, DN.REF, DP.REF, DP.KTM, T.NAZWA, DP.ILOSCL - DP.ILOSCONMWSACTSC, DP.ILOSCL, DP.ILOSCONMWSACTS,
               DP.ILOSCONMWSACTSC, DP.ILOSC, DN.x_pobranie, dn.sposzap --kpi
        from DOKUMNAG DN
        join DOKUMPOZ DP on (DP.DOKUMENT = DN.REF)
        join TOWARY T on (T.KTM = DP.KTM)
        where DN.GRUPASPED = :GRUPASPED and
              (DP.ILOSCL <> DP.ILOSCONMWSACTSC) and
              DN.TYP = 'WZ' and
              DN.AKCEPT = 1
        into :SYMBOLDOK, :DOKUMENT, :REFPOZ, :KTM, :NAZWA, :ILOSCN, :ILOSCL, :ILOSCONMWSACTS, :ILOSCONMWSACTSC, :ILOSC, :pobranie, :sposzap --kpi
    do
    begin
      suspend;
    end
  end
end^
SET TERM ; ^
