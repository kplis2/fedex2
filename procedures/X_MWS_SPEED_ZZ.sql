--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_SPEED_ZZ(
      REFDOK DOKUMNAG_ID,
      REFPOZ DOKUMPOZ_ID,
      TRYB SMALLINT_ID)
   as
declare variable data DATE_ID;
declare variable grupasped dokumnag_id;
declare variable poz dokumpoz_id;
declare variable STATUS SMALLINT_ID;
declare variable NEWDOKSYM SYMBOL_ID;
declare variable ktm ktm_id;
declare variable wersjaref wersje_id;
declare variable iloscl ilosci_mag;
declare variable brak smallint_id;
declare variable iloscbrak ilosci_mag;
declare variable iloscinsert ilosci_mag;
declare variable doktyp defdokum_id;
declare variable CNT INTEGER_ID;
declare variable NEWDOK DOKUMNAG_ID;
begin
  -- wystawienie szybkiej korekty do WZ ZG133732
  --Jest to troche przerobiona procedura  MWS_DOK_BRAKTOWARU_KOREKTA z PR121822
  --tryb: 1 - korygujemy o ilosc brakujaca(pozycje), 2 - zerujemy pozycje jezeli czegokolwiek brakuje, 3 - zerujemy cala grupe spedycyjna (anulowany),
  -- 4 - korygujemy wszystkie pozycje dla grupy spedycyjnej o ilosc jakiej brakuje
  cnt = 0;
  --exception universal 'dok'||coalesce(refdok, 0)||' poz'||coalesce(refpoz, 0)||'tryb' || coalesce(tryb,-1);
  if (tryb is null) then
    tryb = 0;
  if (data is null) then
    data = current_date;
  if (tryb not in (1,2,3,4)) then
    exception universal 'Nieprawidlowy tryb.';
  --if (tryb = 3 and refpoz is not null) then
    --exception universal 'Dla trybu 3 nie podajemy pozycji.';
  if (refpoz = 0) then
    refpoz = null;
  if (refdok = 0) then
    exception universal 'Trzeba podac korygowany dokument';
  if (refdok is null and refpoz is null) then
    exception universal 'Wypadaloby podac, co sie koryguje.';

  select grupasped from dokumnag where ref = :refdok into :grupasped;
  select typ from dokumnag where ref = :refdok into :doktyp;

  if (doktyp <> 'WZ') then
    exception universal 'Nie korygujemy tego typu. Jesli musisz to zrobic skontaktuj sie z Sente.';
  --obsluga trybow zwiazanych z jedna pozycja
  if (tryb in (1,2)) then
  begin
    execute procedure MAG_CREATE_DOK_KORYG (:refdok, :data, 0)
      returning_values(:status, :newdok, :newdoksym);
    for
        select ref, dp.ktm, dp.wersjaref, dp.iloscl, dp.iloscl - dp.ilosconmwsactsc
          from dokumpoz dp
          where dp.dokument = :refdok and (:refpoz is null or dp.ref = :refpoz)
            and dp.iloscl > 0
          into :poz, :ktm, :wersjaref, :iloscl, :iloscbrak
    do begin
      iloscinsert = 0;
      if (tryb = 1 and :iloscbrak > 0) then
        begin
          iloscinsert = :iloscbrak;
        end
      if (tryb = 2 and :iloscbrak > 0 ) then
        begin
          iloscinsert = iloscl;
        end
      if (iloscinsert > 0) then
        begin
          insert into dokumpoz(dokument, ktm, wersjaref, ilosc, kortopoz)
            values(:newdok, :ktm, :wersjaref, :iloscinsert, :poz);
          cnt = cnt + 1;
        end
    end

    if (cnt = 0) then
    begin
      delete from dokumnag where ref = :newdok;
      newdok = null;
    end
    else
       update dokumnag set akcept = 1 where ref = :newdok;
  end
  --obsluga trybow dla calej grupy spedycyjnej
  else if (tryb in(3,4)) then
  begin
    refdok = null;
    for
      select dn.ref
          from dokumnag dn
          where dn.grupasped = :grupasped
              and dn.typ = 'WZ'
          into :refdok
    do begin
        cnt = 0;
        status = null;
        newdok = null;
        newdoksym = null;
        execute procedure MAG_CREATE_DOK_KORYG (:refdok, :data, 0)
          returning_values(:status, :newdok, :newdoksym);

        for select ref, dp.ktm, dp.wersjaref, dp.iloscl, dp.iloscl - dp.ilosconmwsactsc
            from dokumpoz dp
            where dp.dokument = :refdok
              and dp.iloscl > 0
            into :poz, :ktm, :wersjaref, :iloscl, :iloscbrak
        do begin
          iloscinsert = 0;
          if (tryb = 3) then
            begin
              iloscinsert = iloscl;
            end 
          if (tryb = 4) then
            begin
              iloscinsert = :iloscbrak;
            end
          if (iloscinsert > 0) then
            begin
              insert into dokumpoz(dokument, ktm, wersjaref, ilosc, kortopoz)
                values(:newdok, :ktm, :wersjaref, :iloscinsert, :poz);
              cnt = cnt + 1;
            end
        end

        if (cnt = 0) then
        begin
          delete from dokumnag where ref = :newdok;
          newdok = null;
        end
        else
           update dokumnag set akcept = 1 where ref = :newdok;

    end
  end
end^
SET TERM ; ^
