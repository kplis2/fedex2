--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE HOURS2MINUTES(
      HOURS varchar(20) CHARACTER SET UTF8                           )
  returns (
      MINUTES integer)
   as
declare variable colon_position integer;
  declare variable minutes_no_hours integer;
begin
  --DS: funkcja uzywana na kartach urlopowych do wyliczania minut na podstawie wpisanych godzin w formacie HH:MM
  if (position('.' in :hours) > 0 or position(',' in :hours) > 0) then
    exception invalid_hour_format;

  colon_position = position(':' in  hours);
  minutes_no_hours = 0;
  if (colon_position <> 0) then
  begin
    if(colon_position >= coalesce(char_length(hours),0) or colon_position = 1) then -- [DG] XXX ZG119346
      exception invalid_hour_format;
    minutes_no_hours = substring(hours from colon_position + 1 for coalesce(char_length(hours),0) - colon_position); -- [DG] XXX ZG119346
    hours = substring(hours from 1 for colon_position - 1);
  end

  minutes = minutes_no_hours + cast(hours as integer) * 60;
  suspend;
end^
SET TERM ; ^
