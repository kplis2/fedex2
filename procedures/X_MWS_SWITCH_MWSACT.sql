--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_SWITCH_MWSACT(
      MWSACT_1 MWSACTS_ID,
      MWSACT_2 MWSACTS_ID)
  returns (
      STATUS smallint,
      MSG STRING)
   as
declare variable status_1 smallint_id;
declare variable x_serial_no_1 integer_id;
declare variable x_partia_1 date_id;
declare variable x_slownik_1 integer_id;
declare variable mwspallocp_1 mwspallocs_id;
declare variable mwspallocl_1 mwspallocs_id;
declare variable lot_1 dostawa_id; 
declare variable status_2 smallint_id;
declare variable x_serial_no_2 integer_id;
declare variable mwspallocp_2 mwspallocs_id;
declare variable mwspallocl_2 mwspallocs_id;
declare variable lot_2 dostawa_id;   
declare variable x_partia_2 date_id;
declare variable x_slownik_2 integer_id;
begin
  status = 1;
  msg = '';

  select ma.status, ma.x_serial_no, ma.mwspallocp, ma.mwspallocl, ma.lot,
         ma.x_partia, ma.x_slownik
    from mwsacts ma
    where ma.ref = :MWSACT_1
  into :status_1, :x_serial_no_1, :mwspallocp_1, :mwspallocl_1, :lot_1,
       :x_partia_1, :x_slownik_1;

  select ma.status, ma.x_serial_no, ma.mwspallocp, ma.mwspallocl, ma.lot,
         ma.x_partia, ma.x_slownik
    from mwsacts ma
    where ma.ref = :MWSACT_2
  into :status_2, :x_serial_no_2, :mwspallocp_2, :mwspallocl_2, :lot_2,
       :x_partia_2, :x_slownik_2;

  if (status_1=1) then
  begin
    update mwsacts a set a.status=0 where a.ref = :MWSACT_1;
  end
  else if (status_1 in (2,3)) then
  begin
    update mwsacts a set a.status=1 where a.ref = :MWSACT_1;
    update mwsacts a set a.status=0 where a.ref = :MWSACT_1;
  end
  else
  begin
    status = 0;
    msg = 'Blad zamian pozycji pierwotnej zlecenia towar juz pobrany.';
  end

  if (status_2=1) then
  begin
    update mwsacts a set a.status=0 where a.ref = :MWSACT_2;
  end
  else if (status_2 in (2,3)) then
  begin
    update mwsacts a set a.status=1 where a.ref = :MWSACT_2;
    update mwsacts a set a.status=0 where a.ref = :MWSACT_2;
  end
  else
  begin
    status = 0;
    msg = 'Blad zamian pozycji wtornej zlecenia towar juz pobrany.';
  end

  update mwsacts a
    set a.x_serial_no=:x_serial_no_2,
        a.x_partia=:x_partia_2,
        a.x_slownik=:x_slownik_2,
        a.mwspallocp=:mwspallocp_2,
        a.mwspallocl=:mwspallocl_2,
        a.lot=:lot_2
    where a.ref = :MWSACT_1;

  update mwsacts a
    set a.x_serial_no=:x_serial_no_1,
        a.x_partia=:x_partia_1,
        a.x_slownik=:x_slownik_1,
        a.mwspallocp=:mwspallocp_1,
        a.mwspallocl=:mwspallocl_1,
        a.lot=:lot_1
    where a.ref = :MWSACT_2;

  if (status_1=1) then
  begin
    update mwsacts a set a.status=1 where a.ref = :MWSACT_1;
  end
  else if (status_1 in (2,3)) then
  begin
    update mwsacts a set a.status=1 where a.ref = :MWSACT_1;
    update mwsacts a set a.status=:status_1 where a.ref = :MWSACT_1;
  end

  if (status_2=1) then
  begin
    update mwsacts a set a.status=1 where a.ref = :MWSACT_2;
  end
  else if (status_2 in (2,3)) then
  begin
    update mwsacts a set a.status=1 where a.ref = :MWSACT_2;
    update mwsacts a set a.status=:status_2 where a.ref = :MWSACT_2;
  end


end^
SET TERM ; ^
