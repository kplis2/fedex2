--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PLANS_KTM_AMOUNT(
      KTM varchar(80) CHARACTER SET UTF8                           ,
      PLANSCOLSREF integer)
  returns (
      INCOME numeric(14,2),
      EXPEND numeric(14,2))
   as
declare variable planval numeric(14,2);
declare variable planstype varchar(20);
declare variable prdepart varchar(20);
declare variable plansquantity numeric(14,4);
declare variable mainktm varchar(80);
declare variable datefrom timestamp;
declare variable dateto timestamp;
declare variable plansincome numeric(14,2);
declare variable plansexpend numeric(14,2);
declare variable grossquantity numeric(14,4);
begin
  income = 0;
  expend = 0;
  select pv.planval, p.planstype, p.prdepart, pc.bdata, pc.ldata
   from planscol pc
   join plans p on (p.ref = pc.plans)
   join plansrow pr on (p.ref = pr.plans and pr.key1 = :ktm)
   join plansval pv on (p.ref = pv.plans and pv.planscol = pc.ref and pv.plansrow = pr.ref)
   where pc.ref = :planscolsref
  into :planval, :planstype, :prdepart, :datefrom, :dateto;
  if(:planval is null) then planval = 0;
  if(:planstype = 'SPPLAN') then begin
    expend = :planval;
    income = 0;
  end else if (:planstype = 'ZAPLAN') then begin
    income = :planval;
    expend = 0;
  end else if (:planstype = 'PRPLAN') then begin
    income = :planval;
    expend =  0;
  end else begin
    income = :planval;
    for select p.quantity, p.ktm, pm.grossquantity from prsheets p join prshmat pm on (pm.sheet = p.ref)
     where pm.ktm = :ktm and p.status = 2 into :plansquantity, :mainktm, :grossquantity
    do begin
        execute procedure plans_ktm_from_to(:mainktm, :datefrom, :dateto) returning_values :plansincome, :plansexpend;
        if (:plansquantity is null or :plansquantity <= 0) then plansquantity = 1;
        expend = :expend + :plansincome *grossquantity/:plansquantity;
        if(:expend<0) then expend = 0;
        if(:income<0) then income = 0;
    end
  end
  suspend;
end^
SET TERM ; ^
