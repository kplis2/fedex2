--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_PIT(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      COL smallint)
  returns (
      AMOUNT numeric(14,2))
   as
declare variable frvhdr integer;
  declare variable frpsn integer;
  declare variable frcol integer;
  declare variable cacheparams varchar(255);
  declare variable ddparams varchar(255);
  declare variable iyear smallint;
  declare variable imonth smallint;
  declare variable company smallint;
  declare variable frversion integer;
begin
--Podatek dochodowy pracowników przed odliczeniem skadki na ubezpieczenie zdrowotne(DG-1)
  select frvhdr, frpsn, frcol
    from frvpsns
    where ref = :frvpsn
    into :frvhdr, :frpsn, :frcol;

  select company, frversion
    from frvhdrs
    where ref = :frvhdr
    into :company, :frversion;
  cacheparams = period||';'||company||';'||col;
  ddparams = '';

  if (not exists (select ref from frvdrilldown where functionname = 'PIT' and cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr)) then
  begin
    iyear = cast(substring(period from 1 for 4) as integer);
    imonth = cast(substring(period from 5 for 2) as smallint);

    if (col = 1) then
    select sum(EP.pvalue)
      from employees E
        join eprpos EP on (E.ref = EP.employee)
        join epayrolls PR on (EP.payroll = PR.ref)
      where E.econtrtype <> 9 and EP.ecolumn = 7100 and PR.cper starting with :iyear
        and PR.cper = :period and PR.company = :company
      into :amount;
    else
      select sum(EP.pvalue)
        from employees E
          join eprpos EP on (E.ref = EP.employee)
          join epayrolls PR on (EP.payroll = PR.ref)
        where E.econtrtype <> 9 and EP.ecolumn = 7100 and PR.cper starting with :iyear
          and PR.cper <= :period and PR.company = :company
        into :amount;
  end else
    select first 1 fvalue from frvdrilldown where cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr
      into :amount;

  amount = coalesce(amount,0);
  insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
     values (:frvhdr, :frpsn, :frcol, 'PIT', :cacheparams, :ddparams, :amount, :frversion);

suspend;
end^
SET TERM ; ^
