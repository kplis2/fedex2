--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_TRANSFER_DOSTAWCY as
declare variable s1 varchar(255); /* kod ksiegowy */
declare variable s2 varchar(255); /* nazwa */
declare variable s3 varchar(255); /* nip */
declare variable s4 varchar(255); /* kraj */
declare variable s5 varchar(255); /* czy dostawca jest aktywny ? */
declare variable s6 varchar(255); /* skrot */
declare variable s7 varchar(255); /* czy dostawca zagraniczny */
declare variable s8 varchar(255); /* ulica */
declare variable s9 varchar(255); /* miasto */
declare variable s10 varchar(255); /* wojewodztwo */
declare variable s11 varchar(255); /* telefon */
declare variable s12 varchar(255); /* komorka */
declare variable s13 varchar(255); /* fax */
declare variable s14 varchar(255); /* regon */
declare variable s15 varchar(255); /* zaklad */
declare variable s16 varchar(255); /* rachunek bankowy */
declare variable s17 varchar(255); /* email */
declare variable s18 varchar(255); /* uwagi */
declare variable s19 varchar(255); /* kod zewnetrzny */
declare variable s20 varchar(255); /* sposob platnosci */
declare variable s21 varchar(255); /* liczba dni zwloki */
declare variable s22 varchar(255); /* ilosc dni co ile jest dostawa od dostawcy */
declare variable kontofk varchar(20);
declare variable nazwa varchar(255);
declare variable nip varchar(15);
declare variable kraj varchar(40);
declare variable krajid varchar(20);
declare variable aktywny smallint;
declare variable fskrot varchar(40);
declare variable zagraniczny smallint;
declare variable ulica varchar(60);
declare variable miasto varchar(60);
declare variable cpwoj16m integer; /* ref na cpwoj16m */
declare variable telefon varchar(255);
declare variable komorka varchar(255);
declare variable fax varchar(255);
declare variable regon varchar(20);
declare variable company integer; /* ref na companies */
declare variable rachunek varchar(40);
declare variable email varchar(255);
declare variable uwagi varchar(1024);
declare variable kodzewn varchar(40);
declare variable sposplat integer; /* ref na platnosci */
declare variable dnizap integer;
declare variable dnicykl integer;
declare variable tmp integer;
begin

-- sprawdzanie dlugoscie danych w tabeli expimp
  select count(ref) from expimp where coalesce(char_length(s1),0)>20 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich kont ksiegowych.';

  select count(ref) from expimp where coalesce(char_length(s2),0)>80 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich nazw.';

  select count(ref) from expimp where coalesce(char_length(s3),0)>15 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich NIP.';

  select count(ref) from expimp where coalesce(char_length(s4),0)>40 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich nazw krajow.';

  select count(ref) from expimp where cast(:s5 as smallint)<>0 and cast(:s5 as smallint)<>1
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba zlych wartosci w kolumnie AKTYWNY.';

  select count(ref) from expimp where coalesce(char_length(s6),0)>40 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich skrotow.';

  select count(ref) from expimp where cast(:s7 as smallint)<>0 and cast(:s7 as smallint)<>1
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba zlych wartosci w kolumnie AKTYWNY.';

  select count(ref) from expimp where coalesce(char_length(s8),0)>60 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich ulic.';

  select count(ref) from expimp where coalesce(char_length(s9),0)>60 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich miast.';

  select count(ref) from expimp where coalesce(char_length(s10),0)>40 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich wojewodztw.';

  select count(ref) from expimp where coalesce(char_length(s14),0)>20 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich regonow.';

  select count(ref) from expimp where coalesce(char_length(s15),0)>10 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich nazw zakladow.';

  select count(ref) from expimp where coalesce(char_length(s16),0)>40 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich rachunkow bankowych.';

  select count(ref) from expimp where coalesce(char_length(s19),0)>40 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich kodow zewnetrznych.';


-- import

  for select s1, s2,  s3,  s4,  s5,  s6,  s7,  s8,  s9,  s10,
            s11, s12, s13, s14, s15, s16, s17, s18, s19, s20,
            s21, s22
    from expimp 
  into :s1, :s2,  :s3,  :s4,  :s5,  :s6,  :s7,  :s8,  :s9,  :s10,
      :s11, :s12, :s13, :s14, :s15, :s16, :s17, :s18, :s19, :s20,
      :s21, :s22
  do begin

/* kod ksiegowy */
     kontofk = substring(:s1 from  1 for  20);

/* nazwa */
     nazwa = substring(:s2 from  1 for  80);

/* nip */
     nip = substring(:s3 from  1 for  15);

/* kraj */
     kraj = substring(:s4 from  1 for  40);
     select symbol from countries where name=:kraj
     into :krajid;
     if (:krajid is null) then
       exception universal 'W tabeli COUNTRIES nie ma kraju: ' || :kraj;

/* czy dostawca jest aktywny ? */
    aktywny = cast(:s5 as smallint);

/* skrot */
    fskrot = substring(:s6 from 1 for 40);

/* czy dostawca zagraniczny */
    zagraniczny = cast(:s7 as smallint);

/* ulica */
    ulica = substring(:s8 from  1 for  60);

/* miasto */
    miasto = substring(:s9 from  1 for  60);

/* wojewodztwo */
    select ref from cpwoj16m
      where opis = upper(substring(:s10 from 1 for 40))
    into :cpwoj16m;
    if (cpwoj16m is null) then
      exception universal 'Wojewodztwo: ' || substring(:s10 from 1 for 40) || ' nie znajduje sie w  bazie.';

/* telefon */
    telefon = :s11;

/* komorka */
    komorka = :s12;

/* fax */
    fax = :s13;

/* regon */
    regon = :s14;

/* zaklad */
    select ref from companies
      where symbol = substring(:s15 from 1 for 10)
    into :company;
    if (company is null) then
      exception universal 'Zaklad: ' || substring(:s15 from 1 for 10) || ' nie znajduje sie w  bazie.';

/* rachunek bankowy */
    rachunek = substring(:s16 from  1 for  40);

/* email */
    email = :s17;

/* uwagi */
    uwagi = :s18;

/* kod zewnetrzny */
    kodzewn = :s19;

/* sposob platnosci */
    select ref from platnosci
      where opis = substring(:s20 from 1 for 40)
    into :sposplat;
    if (sposplat is null) then
      exception universal 'Sposob platnosci: ' || substring(:s20 from 1 for 40) || ' nie znajduje sie w  bazie.';


/* liczba dni zwloki */
--???

/* ilosc dni co ile jest dostawa od dostawcy */
    dnicykl = cast(:s22 as integer);

-- INSERT
-- xxxx do uzupelnienie liczba dni zwloki oraz komorka
    insert into dostawcy (kontofk,  nazwa, nip, kraj, krajid, aktywny, id, zagraniczny, ulica,
                          miasto, cpwoj16m, telefon, fax, regon, company, rachunek, email,
                          uwagi, kodzewn, sposplat,  dnicykl)
                values (:kontofk,  :nazwa, :nip, :kraj, :krajid, :aktywny, :fskrot,  :zagraniczny, :ulica,
                          :miasto, :cpwoj16m, :telefon, :fax, :regon, :company, :rachunek, :email,
                          :uwagi, :kodzewn, :sposplat,  :dnicykl);

  end
end^
SET TERM ; ^
