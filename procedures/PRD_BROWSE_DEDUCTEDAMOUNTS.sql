--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRD_BROWSE_DEDUCTEDAMOUNTS(
      PRDACCOUNTING integer)
  returns (
      PRDACCOUNTINGPOS integer,
      DEDUCTACCOUNT ACCOUNT_ID,
      DEDUCTAMOUNT numeric(14,2),
      FIRSTDEDUCTAMOUNT numeric(14,2),
      DEDUCTEDAMOUNT numeric(14,2),
      DIST1DDEF integer,
      DIST1DDEFDESC varchar(255) CHARACTER SET UTF8                           ,
      DIST1SYMBOL SYMBOLDIST_ID,
      DIST1SYMBOLDESC varchar(255) CHARACTER SET UTF8                           ,
      DIST2DDEF integer,
      DIST2DDEFDESC varchar(255) CHARACTER SET UTF8                           ,
      DIST2SYMBOL SYMBOLDIST_ID,
      DIST2SYMBOLDESC varchar(255) CHARACTER SET UTF8                           ,
      DIST3DDEF integer,
      DIST3DDEFDESC varchar(255) CHARACTER SET UTF8                           ,
      DIST3SYMBOL SYMBOLDIST_ID,
      DIST3SYMBOLDESC varchar(255) CHARACTER SET UTF8                           ,
      DIST4DDEF integer,
      DIST4DDEFDESC varchar(255) CHARACTER SET UTF8                           ,
      DIST4SYMBOL SYMBOLDIST_ID,
      DIST4SYMBOLDESC varchar(255) CHARACTER SET UTF8                           ,
      DIST5DDEF integer,
      DIST5DDEFDESC varchar(255) CHARACTER SET UTF8                           ,
      DIST5SYMBOL SYMBOLDIST_ID,
      DIST5SYMBOLDESC varchar(255) CHARACTER SET UTF8                           ,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      DIVRATE numeric(14,6),
      DIST6DDEF integer,
      DIST6DDEFDESC varchar(255) CHARACTER SET UTF8                           ,
      DIST6SYMBOL SYMBOLDIST_ID,
      DIST6SYMBOLDESC varchar(255) CHARACTER SET UTF8                           ,
      BKDOC integer,
      DECREE integer,
      PERIOD integer)
   as
declare variable prdside smallint;
begin
  for select d.ref, d.bkdoc, d.period, d.prdaccountingpos, s.deductaccount, s.deductamount, s.firstdeductamount,
   case when d.side = 0 then d.debit else d.credit end,  s.divrate, s.dist1ddef, s.dist1symbol, s.dist2ddef, s.dist2symbol,
     s.dist3ddef, s.dist3symbol, s.dist4ddef, s.dist4symbol, s.dist5ddef, s.dist5symbol, s.dist6ddef, s.dist6symbol
  from decrees d left join prdaccountingpos s on (s.ref = d.prdaccountingpos)
  where d.prdaccountingpos in (select p.ref from prdaccountingpos p where p.prdaccounting = :prdaccounting) and d.status > 1
  and d.prdaccountingpos is not null
  into :decree, :bkdoc, :period, :prdaccountingpos, :deductaccount, :deductamount, :firstdeductamount,
        :deductedamount, :divrate, :dist1ddef, dist1symbol, :dist2ddef, dist2symbol,: dist3ddef, dist3symbol,
          :dist4ddef, dist4symbol, :dist5ddef, dist5symbol, :dist6ddef, dist6symbol
  do begin
    /*Nazwa 1 slownika i pozycji wyroznika*/
    select sd.nazwa from slodef sd where sd.ref = :dist1ddef into :dist1ddefdesc;
    select sp.nazwa from slopoz sp where sp.kontoks = :dist1symbol and sp.slownik = :dist1ddef into :dist1symboldesc;
    /*Nazwa 2 slownika i pozycji wyroznika*/
    select sd.nazwa from slodef sd where sd.ref = :dist3ddef into :dist2ddefdesc;
    select sp.nazwa from slopoz sp where sp.kontoks = :dist2symbol and sp.slownik = :dist2ddef into :dist2symboldesc;
    /*Nazwa 3 slownika i pozycji wyroznika*/
    select sd.nazwa from slodef sd where sd.ref = :dist3ddef into :dist3ddefdesc;
    select sp.nazwa from slopoz sp where sp.kontoks = :dist3symbol and sp.slownik = :dist3ddef into :dist3symboldesc;
    /*Nazwa 4 slownika i pozycji wyroznika*/
    select sd.nazwa from slodef sd where sd.ref = :dist4ddef into :dist4ddefdesc;
    select sp.nazwa from slopoz sp where sp.kontoks = :dist4symbol and sp.slownik = :dist4ddef into :dist4symboldesc;
    /*Nazwa 5 slownika i pozycji wyroznika*/
    select sd.nazwa from slodef sd where sd.ref = :dist5ddef into :dist5ddefdesc;
    select sp.nazwa from slopoz sp where sp.kontoks = :dist5symbol and sp.slownik = :dist5ddef into :dist5symboldesc;
    /*Nazwa 6 slownika i pozycji wyroznika*/
    select sd.nazwa from slodef sd where sd.ref = :dist6ddef into :dist6ddefdesc;
    select sp.nazwa from slopoz sp where sp.kontoks = :dist6symbol and sp.slownik = :dist6ddef into :dist6symboldesc;
    suspend;
  end
end^
SET TERM ; ^
