--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_PODSUM_VAT_PODSUM(
      PERIODID varchar(6) CHARACTER SET UTF8                           ,
      BKPERIOD varchar(6) CHARACTER SET UTF8                           ,
      VREG varchar(10) CHARACTER SET UTF8                           ,
      ZAKRES smallint,
      ZPOPMIES smallint,
      TYP smallint,
      COMPANY integer,
      ZAKUP integer)
  returns (
      SUMWARTBRU numeric(14,2),
      SUMWARTNET numeric(14,2),
      SUMWARTVAT numeric(14,2),
      SUMWARTVATDEB numeric(14,2),
      VATGR varchar(5) CHARACTER SET UTF8                           ,
      TAXGR varchar(10) CHARACTER SET UTF8                           ,
      DEBITED smallint,
      VTYPE smallint,
      DEBITET smallint)
   as
declare variable sql varchar(5048);
declare variable nettodeb numeric(14,2);
declare variable vatdeb numeric(14,2);
declare variable netto57 numeric(14,2);
declare variable vat57 numeric(14,2);
begin
debitet = 1;
if ((PERIODID = '' and bkperiod = '') or (PERIODID is null and bkperiod is null)) then exception period_not_defined;
  nettodeb = 0;
  vatdeb = 0;
  NETTO57 = 0;
  VAT57 = 0;
  if (typ = -1) then
    typ = 0; -- jak ktos wykasuje wpis w combo
  if(:zakres is null) then zakres = 0;
  if(:zpopmies is null) then zpopmies = 0;
    SQL = 'select coalesce(va.grupa,''''), coalesce(sum(NETV),0), coalesce(sum(VATE),0), BVP.taxgr, ';
    SQL = SQL ||' coalesce(sum(VATV),0), ';
    sql = sql ||' max(v.vtype)';
    SQL = SQL ||' from BKVATPOS BVP left join bkdocs B on BVP.BKDOC = B.REF ';
    if(:bkperiod <> '') then sql = sql || 'and B.period='''||:bkperiod||''' ';
    SQL = SQL ||' join VATREGS V on (B.vatreg = V.symbol and B.company = V.company) join VAT va on va.grupa = BVP.vatgr ';
    SQL = SQL ||' join grvat G on G.symbol = BVP.taxgr ';
    sql = sql ||' where ';
    if(:periodid <> '') then  sql = sql ||' B.vatperiod = '''||:periodid||''' and ';
    sql = sql ||'b.status > 0';

    --<<PR73894: PL
    if(coalesce(zakup,0)>0)then
    begin
      sql = sql ||'  and (v.vatregtype in (1,2)
    or (V.VATREGTYPE = 3
         and exists(select first 1 1 from bkvatpos join grvat on (grvat.symbol = bkvatpos.taxgr) where bkvatpos.bkdoc = B.ref and grvat.vatregtype in (1,2))))';
    end
    else
    begin
      sql = sql ||'  and (v.vatregtype in (0,2)
    or (V.VATREGTYPE = 3
         and exists(select first 1 1 from bkvatpos join grvat on (grvat.symbol = bkvatpos.taxgr) where bkvatpos.bkdoc = B.ref and grvat.vatregtype in (0,2))))';
    end
    -->>
    if(coalesce(zakres,0)=1)then sql = sql ||' and t.creditnote = 0';
    else if(coalesce(zakres,0)=2)then sql = sql ||' and t.creditnote = 1';
    if(coalesce(zpopmies,0)>0)then sql = sql ||' and (b.vatperiod <> b.period)';
    if(coalesce(vreg,'')<>'')then sql = sql ||' and b.vatreg = '''||:vreg||'''';
    sql = sql ||' and B.company ='||:company||' group by va.grupa, ';
    if(coalesce(zakup,0)>0)then sql = sql ||' BVP.debited, ';
    sql = sql ||' BVP.taxgr order by va.grupa';
    for execute statement :sql
        INTO VATGR, SUMWARTNET, SUMWARTVAT, TAXGR, SUMWARTVATDEB, VTYPE
  do begin
      SUMWARTBRU = SUMWARTNET + SUMWARTVAT;
      if(:vatgr not in ('ZW', 'NP', 'OO')) then  vatgr = :vatgr || '%';  --<<PR73894: PL>>
      if (debited=0 and coalesce(zakup,0)>0) then
      begin
        nettodeb = nettodeb + SUMWARTNET;
        vatdeb = vatdeb + SUMWARTVAT;
        if (vtype > 2) then
        begin
          if (taxgr = 'P57') then
          begin
            NETTO57 = NETTO57 + SUMWARTNET;
            VAT57 = VAT57 + SUMWARTVAT;
          end
        end
      end else
      suspend;
    end
    SUMWARTNET = nettodeb;
    SUMWARTVAT = vatdeb;
    SUMWARTBRU = nettodeb + vatdeb;
    VATGR = 'DEB';
    TAXGR = 'DEB';
    debitet = 0;
    suspend;
    SUMWARTNET = NETTO57;
    SUMWARTVAT = VAT57;
    SUMWARTBRU = NETTO57 + VAT57;
    VATGR = 'P57';
    TAXGR = 'P57';
    debitet = 0;
  --  suspend;
end^
SET TERM ; ^
