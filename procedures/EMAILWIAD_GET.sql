--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWIAD_GET(
      FORID varchar(1024) CHARACTER SET UTF8                            = null,
      TAKEFULLBODY smallint = 1,
      FORMAILBOX integer = null,
      FORFOLDER integer = null,
      FORSEARCHSTRING varchar(255) CHARACTER SET UTF8                            = null,
      FORTHREADID integer = null,
      MAXN integer = null)
  returns (
      REF integer,
      TYTUL STRING2048_UTF8,
      DATA timestamp,
      OD STRING1024_UTF8,
      DOKOGO STRING2048_UTF8,
      CC STRING2048_UTF8,
      DELIVEREDTO varchar(255) CHARACTER SET UTF8                           ,
      IDWATKU integer,
      STAN smallint,
      OPERATOR integer,
      MAILBOX integer,
      FOLDER integer,
      ID varchar(1024) CHARACTER SET UTF8                           ,
      UID integer,
      IID integer,
      TRYB integer,
      TRESC MEMO5000_UTF8,
      FULLHTMLBODY BLOB_UTF8,
      FORMATFIELD varchar(64) CHARACTER SET UTF8                           ,
      HASATTACHMENTS smallint,
      BCC STRING2048_UTF8,
      PREVIOUSMAIL integer)
   as
declare variable OK smallint;
declare variable POS integer;
declare variable SECONDDB varchar(10);
declare variable FULLYLOADED smallint;
declare variable n integer;
begin
  -- TODO obsluzyc takefullbody
  if(:forid is not null and :forid<>'') then begin
    -- zwróć podjedynczego maila
    select e.REF, e.TYTUL, e.DATA, e.OD, e.DOKOGO, e.CC, e.DELIVEREDTO, e.IDWATKU, e.STAN, e.OPERATOR, e.MAILBOX, e.folder,
    e.ID, e.UID, e.IID, e.TRYB, e.tresc, iif(:takefullbody=1,e.fullhtmlbody,null), e.fullyloaded, e.hasattachments, e.bcc, e.previousmail
    from emailwiad e
    where e.id=:forid and (:formailbox is null or (:formailbox is not null and e.mailbox=:formailbox)) and e.folder = :forfolder
    into :ref, :tytul, :data, :od, :dokogo, :cc, :deliveredto, :idwatku, :stan, :operator, :mailbox, :folder,
         :id, :uid, :iid, :tryb, :tresc, :fullhtmlbody,:fullyloaded, :hasattachments, :bcc, :previousmail;
    formatfield = '';
    if(:stan=0) then formatfield = :formatfield || '%B';
    if(:fullyloaded=0) then formatfield = :formatfield || '%TclGray';
    if(formatfield<>'') then formatfield = '*=*='||:formatfield||'*';
    suspend;
  end else if(:forthreadid is not null and :forthreadid<>0) then begin
    -- zwróć maile danego wÄ…tku
    for select e.REF, e.TYTUL, e.DATA, e.OD, e.DOKOGO, e.CC, e.DELIVEREDTO, e.IDWATKU, e.STAN, e.OPERATOR, e.MAILBOX, e.folder,
    e.ID, e.UID, e.IID, e.TRYB, e.tresc, iif(:takefullbody=1,e.fullhtmlbody,null), e.fullyloaded, e.hasattachments, e.bcc, e.previousmail
    from emailwiad e
    where (e.ref=:forthreadid or e.idwatku=:forthreadid) and (:formailbox is null or (:formailbox is not null and e.mailbox=:formailbox))
    into :ref, :tytul, :data, :od, :dokogo, :cc, :deliveredto, :idwatku, :stan, :operator, :mailbox, :folder,
         :id, :uid, :iid, :tryb, :tresc, :fullhtmlbody,:fullyloaded, :hasattachments, :bcc, :previousmail
    do begin
      formatfield = '';
      if(:stan=0) then formatfield = :formatfield || '%B';
      if(:fullyloaded=0) then formatfield = :formatfield || '%TclGray';
      if(formatfield<>'') then formatfield = '*=*='||:formatfield||'*';
      suspend;
    end
  end else begin
    if(:formailbox=0 or :forfolder=0) then exit;
    if(:maxn is null or :maxn=0) then maxn = 1000;
    n = 0;
    if(:forsearchstring<>'') then
      forsearchstring = '%'||replace(lower(:forsearchstring), ' ', '%')||'%';
    -- zwróć wszystkie maile w folderze
    for select e.REF, e.TYTUL, e.DATA, e.OD, e.DOKOGO, e.CC, e.DELIVEREDTO, e.IDWATKU, e.STAN, e.OPERATOR, e.MAILBOX, e.folder,
         e.ID, e.UID, e.IID, e.TRYB, e.tresc, iif(:takefullbody=1,e.fullhtmlbody,null), e.fullyloaded, e.hasattachments, e.bcc, e.previousmail
    from emailwiad e
    where e.mailbox=:formailbox and e.folder = :forfolder and e.stan<>5
    order by e.mailbox desc,e.folder desc, e.data desc
    into :ref, :tytul, :data, :od, :dokogo, :cc, :deliveredto, :idwatku, :stan, :operator, :mailbox, :folder,
         :id, :uid, :iid, :tryb, :tresc, :fullhtmlbody,:fullyloaded, :hasattachments, :bcc, :previousmail
    do begin
      ok = 1;
      if(:forsearchstring<>'') then begin
        if(lower(:tytul||' '||:od||' '||:dokogo||' '||:cc||' '||:tresc) not like :forsearchstring) then ok = 0;
      end
      if(ok=1) then begin
        pos = position ('<' in :od);
        if(:pos>0) then od = substring(:od from 1 for :pos-1);
        formatfield = '';
        if(:stan=0) then formatfield = :formatfield || '%B';
        if(:fullyloaded=0) then formatfield = :formatfield || '%TclGray';
        if(formatfield<>'') then formatfield = '*=*='||:formatfield||'*';
        n = n + 1;
        suspend;
        if(n>=:maxn) then exit;
      end
    end
  end
end^
SET TERM ; ^
