--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EVERYDAY as
declare variable TODAY varchar(20);
declare variable STATEMENTTXT varchar(50);
declare variable CREF integer;
declare variable EMPLOYEE integer;
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable COMPANY integer;
declare variable KONTOFK KONTO_ID;
declare variable WALUTA varchar(3);
declare variable WINIEN numeric(14,2);
declare variable MA numeric(14,2);
declare variable WINIENZL numeric(14,2);
declare variable MAZL numeric(14,2);
declare variable ALLYEAR smallint;
declare variable ESTATUS smallint;
declare variable METHOD integer;
declare variable CALC integer;
declare variable PARSYMBOL varchar(20);
declare variable PRVALUE numeric(14,2);
declare variable FROMDATE date;
declare variable DTODAY timestamp;
declare variable evacemplcontr integer;
declare variable evacemployee integer;
begin
  select wartosc from konfig where akronim = 'TODAY'
    into :today;
  -- usuwanie blokad, ktore nie maja aktywnych polaczen do bazy
  execute procedure sys_clearsyslocks;
  if (today is not null and today <> cast(current_date as varchar(10))) then
  begin
    --i tu funkcje wykonywane raz dziennie
    ----------------------------------------------------------------------------

    --uzupelnienie pola dni na rozrachunkach
    update rozrach set dni= current_date-cast(dataplat as date) where datazamk is null;
    --uzupelnienie pola zatrudniony na pracownikach
    update employees set emplstatus = 0;
    update employees e set emplstatus = 1
      where exists (
        select first 1 1
          from emplcontracts c
          where c.employee = e.ref
            and c.empltype = 1
            and c.fromdate <= current_date
            and ((c.enddate is not null and c.enddate >= current_date)
              or (c.enddate is null and (c.todate is null or c.todate >= current_date))));

  /*Aktualizacja wartosci biezacych na umowie o prace; porownanie czy dokumenty
    generujace te wartosci nie ulegly zmienie miedzy dniem ostatniego uruchomienia
    programu, a dniem dzisiejszym (PR24342)*/
    dtoday = cast(today as date);
    for
      select m1.emplcontract from employment m1
        join employment m2 on (m1.employee = m2.employee)
        where m1.fromdate <= current_date and (m1.todate is null or m1.todate >= current_date)
          and m2.fromdate <= :dtoday and (m2.todate is null or m2.todate >= :dtoday)
          and (m1.emplcontract <> m2.emplcontract or coalesce(m1.annexe,0) <> coalesce(m2.annexe,0))
      into :cref
    do
      execute procedure emplcontracts_currents_update(:cref);

  --aktualizacja kart urlopowych dla pierwszej pracy
    execute procedure get_config('ACTLIMIT_FOR_ALLYEAR', 2) returning_values allyear;
    if (allyear = 0) then
    begin
      for
        select employee from evaclimits
          where vyear = extract(year from current_date) and firstjob = 1
          into :employee
      do
        execute procedure e_vac_card(current_date, :employee);
    end

  --aktualizacja kart urlopowych dla nieetatowcow, jezeli aneks zaczal obowiazywac(PR60323)
  --rowniez jezeli data na umowie zaczela obowiazywac miedzy dniem dzisiejszym a ostatnim
  --uruchomieniem programu
  for
     select distinct c.ref, c.employee
      from econtrannexes a
        join emplcontracts c on a.emplcontract = c.ref
        left join emplcontrcondit w on w.annexe = a.ref
        join edictcontrcondit d on d.ref = w.contrcondit
      where (d.symbol = 'DNI_WOLNE_PLATNE' or d.symbol = 'DNI_WOLNE_NIEPLATNE')
        and c.empltype > 1
        and a.fromdate <= current_date
        and a.fromdate >= :dtoday
      into evacemplcontr, evacemployee
  do begin
    execute procedure e_vac_card_ucp(current_date, :evacemployee, :evacemplcontr);
  end

  for
    select distinct c.ref, c.employee
      from emplcontracts c
        left join emplcontrcondit w on w.emplcontract = c.ref
        join edictcontrcondit d on d.ref = w.contrcondit
      where (d.symbol = 'DNI_WOLNE_PLATNE' or d.symbol = 'DNI_WOLNE_NIEPLATNE')
        and c.empltype > 1
        and c.fromdate <= current_date
        and c.fromdate >= :dtoday
      into evacemplcontr, evacemployee
  do begin
    execute procedure e_vac_card_ucp(current_date, :evacemployee, :evacemplcontr);
  end
/*  Wykomentowane z realizacja BS51114:

    Aktualizacja kart urlopowych dla urlopu uzupelniajacego; jezeli i data i
    limit uzupelnienia okreslono recznie, to nie ma co zmienic (BS51114)
    for
      select employee from evaclimits
        where vyear = :vyear
          and addvdate is not null and (addlimit_cust + addvdate_cust) <> 2
          and addvdate >= :today and addvdate <= current_date
        into :employee
    do
      execute procedure E_VAC_CARD(currendate, :employee)


  --Aktualizacja kart urlopowych dla urlopu rehabilitacyjnego
    for
      select employee from evaclimits
        where vyear = :vyear
        into :employee
    do begin
      --jesli od ostatniego uruchomienia programu komus sie rozpoczal albo skonczyl okres
      --kiedy moze wziac urlop rehab. to przelicz karty
      if (exists(select first 1 1  from get_disable_periods(:employee,0)
            where (fromdate >= :today and fromdate <= current_date)
               or (todate is null or (todate >= :today and todate <= current_date)))
      ) then
          execute procedure e_vac_card(current_date, :employee)
    end
*/

    --naliczenie przeterminowany sald na ROZRACHSTAN
    for
      select slodef,  slopoz, company, kontofk, waluta, sum(winien), sum(ma), sum(winienzl), sum(mazl)
        from rozrach
        where (saldowm<0 or saldowm>0) and dataplat < current_timestamp(0)
        group by slodef,  slopoz, company, kontofk, waluta
        into :slodef, :slopoz, :company, :kontofk, :waluta, :winien, :ma, :winienzl, :mazl
    do begin
      if (exists(select 1 from rozrachstan
                   where slodef = :slodef and slopoz = :slopoz and company = :company
                      and kontofk = :kontofk and waluta = :waluta)) then
      begin
        update rozrachstan set winienprz = :winien,  maprz = :ma, winienprzzl = :winienzl, maprzzl = :mazl
          where slodef = :slodef and slopoz = :slopoz and company = :company
          and kontofk = :kontofk and waluta = :waluta;
      end
    end

    -- generowanie zadań cyklicznych
    execute procedure XK_GENEROWANIE_ZADAN_AKCJIMARK(current_date) returning_values :estatus;

    --aktualizacja kalkulacji produkcyjnych -> parametry globalne
    for
      select p.method, p.prmethodpar, p.pvalue, p.fromdate
        from prmethodparvals p
        where cast(p.fromdate as date) <= current_date
          and p.calcineveryday = 1
        order by p.fromdate
        into :method, :parsymbol, :prvalue, :fromdate
    do begin
      for
        select c.ref
          from prshcalcs c
            join prcalcpars p on (p.calc = c.ref and p.symbol = :parsymbol and p.partype = 'G')
          where c.method = :method
          into :calc
      do
        update prcalcpars p set p.pvalue = :prvalue
          where p.symbol = :parsymbol and p.partype = 'G' and p.calc = :calc;
      update prmethodparvals p set p.calcineveryday = 2
        where p.method = :method and p.prmethodpar = :parsymbol and p.fromdate = :fromdate;
    end

    -- ustawienie kont na nieaktywne gdy minela data ich waznosci PR08251
    update bankaccounts
      set gl=0,act = 0
       where offdate <= current_date;

    delete from s_sessions where number=1 and lastdate<current_timestamp(0)-1;
    delete from s_logfile where logtime<current_timestamp(0)-90;
    delete from s_loghistory where logtime<current_timestamp(0)-90;
    delete from s_messages where senddate<current_timestamp(0)-90 and status=1;
    delete from globalparams where regdate<current_timestamp(0)-90;
    -- wywolanie x_everyday
    statementtxt = 'execute procedure x_everyday';
    if (exists(select RDB$PROCEDURE_NAME from RDB$PROCEDURES where upper(RDB$PROCEDURE_NAME)='X_EVERYDAY')) then
    begin
       execute statement statementtxt;
    end
    update konfig set wartosc = cast(current_date as varchar(10))
      where akronim = 'TODAY';
  end


end^
SET TERM ; ^
