--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_REC_DOC_ALL_GOODS(
      DOCID integer,
      PALINCLUDE smallint)
  returns (
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer)
   as
declare variable mwsord integer;
declare variable pal smallint;
declare variable mws smallint;
begin
  select coalesce(m.mws,0)
    from dokumnag d
      left join defmagaz m on (m.symbol = d.magazyn)
    where d.ref = :docid
    into mws;
  if (palinclude is null) then palinclude = 0;
  -- najpierw z dokumentu magazynowego
  for
    select p.ktm, p.wersjaref, coalesce(t.paleta,0)
      from dokumpoz p
        left join towary t on (t.ktm = p.ktm)
      where p.dokument = :docid
      group by p.ktm, p.wersjaref, coalesce(t.paleta,0)
      into good, vers, pal
  do begin
    if (palinclude = 1 and pal >=0) then
      suspend;
    else if (palinclude = 0 and pal = 0) then
      suspend;
  end
  if (mws > 0) then
  begin
    -- potem ze zlecenia
    for
      select a.good, a.vers, a.mwsord
        from mwsacts a
          join mwsords o on (o.ref = a.mwsord)
          left join towary t on (t.ktm = a.good)
          left join dokumpoz p on (p.ref = a.docposid)
        where p.wersjaref is null and o.docid = :docid
        group by a.good, a.vers, a.mwsord
        into good, vers, mwsord
    do begin
      select coalesce(paleta,0) from towary where ktm = :good into pal;
      if (palinclude = 1 and pal >=0) then
        suspend;
      else if (palinclude = 0 and pal = 0) then
        suspend;
    end
    -- potem z dokumentów pochodnych ze zlecenia
    for
      select p.ktm, p.wersjaref
        from dokumnag d
          left join dokumpoz p on (d.ref = p.dokument)
          left join towary t on (t.ktm = p.ktm)
        where d.frommwsord = :mwsord
          and not exists (select p1.ref from dokumpoz p1 where p1.wersjaref = p.wersjaref and p1.dokument = :docid)
          and not exists (select a.ref from mwsacts a where a.mwsord = :mwsord and a.vers = p.wersjaref)
        group by p.ktm, p.wersjaref
        into good, vers
    do begin
      select coalesce(paleta,0) from towary where ktm = :good into pal;
      if (palinclude = 1 and pal >=0) then
        suspend;
      else if (palinclude = 0 and pal = 0) then
        suspend;
    end
  end
  if (mws = 0) then
  begin
    -- z dokumentów róznicowych na magazynach nie WMS
    for
      select p.ktm, p.wersjaref
        from dokumnag d
          left join dokumpoz p on (p.dokument = d.ref)
        where d.grupaimp = :docid
          and not exists (select p1.ref from dokumpoz p1 where p1.wersjaref = p.wersjaref and p1.dokument = :docid)
        group by p.ktm, p.wersjaref
        into good, vers
    do begin
      select coalesce(paleta,0) from towary where ktm = :good into pal;
      if (palinclude = 1 and pal >=0) then
        suspend;
      else if (palinclude = 0 and pal = 0) then
        suspend;
    end
  end
end^
SET TERM ; ^
