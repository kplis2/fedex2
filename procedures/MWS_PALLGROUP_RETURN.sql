--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_PALLGROUP_RETURN(
      PALLGROUP integer)
   as
declare variable status smallint;
begin
  if (pallgroup is not null and pallgroup > 0) then
  begin
    select status from mwsords where frommwsact = :pallgroup
      into status;
    if (status = 5) then
      exception MWS_MWSORD_COMMITED 'Zlecenie przesuniecia zostalo już potwierdzone';
    update mwsords set status = 0 where frommwsact = :pallgroup;
    delete from mwsords where frommwsact = :pallgroup;
    select first 1 m.status
      from mwsacts a
        left join mwsords m on (m.ref = a.mwsord)
      where a.palgroup = :pallgroup
      and m.mwsordtype = 10
      into status;
    if (status = 5) then
      exception MWS_MWSORD_COMMITED 'Zlecenie rozladunku zostalo już potwierdzone';
    update mwsacts set status = 1 where palgroup = :pallgroup and status = 5;
  end
end^
SET TERM ; ^
