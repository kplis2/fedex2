--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TYM_STANYARCH as
declare variable wydania smallint;
declare variable data date;
declare variable datan date;
declare variable ilosc numeric(14,4);
declare variable cena numeric(14,4);
declare variable pwartosc numeric(14,4);
declare variable ktm varchar(20);
declare variable wersja integer;
declare variable magazyn varchar(3);
declare variable docref integer;
declare variable docdate date;
declare variable kortoroz integer;
declare variable dnpwartosc numeric(14,4);
begin
/*  for
    select ref, data
      from dokumnag
      into docref, docdate
  do begin
    update dokumpoz p set p.data = :docdate where p.dokument = :docref
      and not exists(select t.ktm from towary t where t.ktm = p.ktm and usluga = 1);
  end
  delete from stanyarch;
  for
    select df.wydania, dn.data, dp.ktm, dp.wersja, dn.magazyn,
        dr.ilosc, dr.pwartosc, dr.ref
      from dokumpoz dp
        join dokumroz dr on (dr.pozycja = dp.ref)
        join dokumnag dn on (dn.ref = dp.dokument)
        join defdokum df on (df.symbol = dn.typ)
        left join towary t on (t.ktm = dp.ktm)
      where dn.akcept = 1 and t.usluga <> 1
      into wydania, data, ktm, wersja, magazyn, ilosc, pwartosc, kortoroz
  do begin
    if (wydania = 0) then
      execute procedure STANYARCH_CHANGE('M',:magazyn,:ktm,:wersja,null,:data,:ilosc,:pwartosc);
    else
      execute procedure STANYARCH_CHANGE('M',:magazyn,:ktm,:wersja,null,:data,-:ilosc,-:pwartosc);
    for
      select dnn.data, dnp.ilosc, dnp.cenanew - dnp.cenaold, dnp.wartosc
        from dokumnotp dnp
          join dokumnot dnn on (dnp.dokument = dnn.ref)
        where dnp.dokumrozkor = :kortoroz and dnp.wartosc <> 0 and dnn.akcept = 1
        into datan, ilosc, cena, dnpwartosc
    do begin
      if (ilosc is null and cena is not null and cena <> 0) then
        ilosc = dnpwartosc/cena;
      else if (ilosc is null) then
        exception universal 'blad dla rozpiski '||:kortoroz;
      if (wydania = 0) then
        execute procedure STANYARCH_CHANGE('N',:magazyn,:ktm,:wersja,:kortoroz,datan,:ilosc,:dnpwartosc);
      else
        execute procedure STANYARCH_CHANGE('N',:magazyn,:ktm,:wersja,:kortoroz,datan,-:ilosc,-:dnpwartosc);
    end
  end    */
end^
SET TERM ; ^
