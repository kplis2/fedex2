--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_FIND_BEST_LOC(
      REFMWSACT INTEGER_ID,
      GOOD KTM_ID = '',
      VERS INTEGER_ID = null,
      WH DEFMAGAZ_ID = 'MWS',
      QUANTITY numeric(14,4) = 0,
      X_PRTIA STRING20 = '',
      MWSORDTYPE INTEGER_ID = null,
      REFILLTRY SMALLINT_ID = 0)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255,
      OUT_CONSTLOCS STRING20)
   as
declare variable constlocref integer_id;
declare variable cnt integer_id;
begin
  status = 0;
  msg = '';


    execute procedure XK_MWS_GET_BEST_LOCATION_3(:good, :vers, :refmwsact, null, :mwsordtype, :wh, null, null, null,
      null, null, null, null, null, null,  null, null,:refilltry, :quantity, :x_prtia )
    returning_values :constlocref, :cnt, :cnt, :cnt, :cnt;

  if (constlocref is not null) then begin
    select symbol from mwsconstlocs where ref = :constlocref into out_constlocs;
    status = 1;
  end else begin
    status = 0;
    msg = 'Nie znaleziono wolnej lokacji';
  end


end^
SET TERM ; ^
