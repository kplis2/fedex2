--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_UPDATEGENERAROR(
      TABLENAME varchar(255) CHARACTER SET UTF8                           )
   as
declare variable TMP INTEGER ;
begin
  TMP=1;
  EXECUTE STATEMENT 'SELECT MAX(ref) FROM ' || TableName INTO :TMP;
  EXECUTE STATEMENT 'SET GENERATOR GEN_' || TableName || ' TO ' || TMP;
  suspend;
end^
SET TERM ; ^
