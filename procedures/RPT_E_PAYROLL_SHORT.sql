--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_E_PAYROLL_SHORT(
      PAYROLL integer,
      EMPLOYEE integer)
  returns (
      NUM integer,
      NAME varchar(22) CHARACTER SET UTF8                           ,
      VAL numeric(14,2),
      REPPAY integer)
   as
declare variable CNT integer;
begin
  cnt = 0;
  for
    select c.number, c.name, p.pvalue, c.reppay
      from eprpos p
      join ecolumns c on (c.number=p.ecolumn)
      where payroll = :payroll
        and employee = :employee
        and reppay > 0
      order by p.ecolumn
    into :num, :name, :val, :reppay
  do begin
    cnt = cnt + 1;
    suspend;
  end

  name = '';
  val = null;
  while (cnt < 49) do
  begin
    cnt = cnt + 1;
    suspend;
  end
end^
SET TERM ; ^
