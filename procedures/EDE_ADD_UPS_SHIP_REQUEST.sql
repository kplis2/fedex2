--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_ADD_UPS_SHIP_REQUEST(
      LISTA integer)
  returns (
      REFDOK integer)
   as
declare variable ederule integer;
declare variable spedresponse varchar(255);
declare variable anulowany smallint_id;
declare variable listywysd_akcept smallint_id;
begin
  select spedresponse, x_anulowany, akcept from listywysd where ref = :lista
  into :spedresponse, :anulowany, :listywysd_akcept;
  if (coalesce(:spedresponse,'') = '1' and coalesce(:anulowany,0) = 0) then
    exception universal 'Dokument został już wysłany. Anuluj wysyłkę i nadaj ją jeszcze raz.';
  else if(coalesce(:listywysd_akcept,0) != 2)then
    exception universal 'Nie można nadać niezamkniętego dokumentu';

  select es.ref
    from ederules es
    where es.symbol = 'EDE_UPS_EXP'
    into : ederule;

  insert into ededocsh( ederule, direction, createdate, otable, oref, status, filename, manualinit)
    values(:ederule, 1, current_timestamp,'LISTYWYSD', :lista,0,:lista||'exp',0)
    returning ref into :refdok;

  suspend;
end^
SET TERM ; ^
