--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_SCHEDULER_GET_TIMEOUTED_SH(
      NEOS_NOW TIMESTAMP_ID)
  returns (
      INSTANCE SYS_SCHEDULEHIST_ID)
   as
begin
  /* Procedure Text */
  for
    select ss.ref
      from sys_schedulehist sh
        join sys_schedule ss on sh.schedule = ss.ref
      where ss.lastinstance is not null
        and (sh.startt is not null
        and sh.stop is null)
        and (sh.startt < dateadd(-ss.timeout minute to :neos_now))
      into instance
  do begin
    suspend;
  end
end^
SET TERM ; ^
