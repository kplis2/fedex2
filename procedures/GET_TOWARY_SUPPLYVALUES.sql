--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_TOWARY_SUPPLYVALUES(
      MAGAZYN DEFMAGAZ_ID,
      ODDATY date,
      DODATY date)
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      MIARA varchar(10) CHARACTER SET UTF8                           ,
      IT numeric(14,4),
      ROI numeric(14,4))
   as
declare variable stanmax numeric(14,4);
declare variable wielkosc_bufora numeric(14,4);
declare variable koszt_bufora numeric(14,4);
declare variable ilosc_sprzedana numeric(14,4);
declare variable sprznet numeric(14,4);
declare variable koszt_sprzedazy numeric(14,4);
declare variable zysk numeric(14,4);
declare variable cena_zakupu numeric(14,4);
begin
  for select
    max(POZFAK.KTM) as KTM,
    (POZFAK.WERSJAREF) as WERSJAREF,
    max(TOWARY.NAZWA) as NAZWA,
    max(TOWARY.MIARA) as MIARA,
    max(coalesce(STANYIL.STANMAX,0)) as STANMAX,
    sum(POZFAK.ILOSCM - POZFAK.PILOSCM) as ILOSC,
    sum(POZFAK.WARTNETZL - POZFAK.PWARTNETZL) as SPRZNET,
    sum(POZFAK.KOSZTZAK) as KOSZT,
    sum(POZFAK.WARTNETZL - POZFAK.PWARTNETZL)-sum(POZFAK.KOSZTZAK) as MARZA
  from POZFAK
    join NAGFAK on (NAGFAK.REF=POZFAK.DOKUMENT)
    join DOKUMPOZ on (DOKUMPOZ.FROMPOZFAK=POZFAK.REF)
    left join DOKUMNAG on (DOKUMNAG.REF=DOKUMPOZ.DOKUMENT)
    left join TOWARY on (POZFAK.KTM = TOWARY.KTM)
    left join STANYIL on (STANYIL.MAGAZYN=DOKUMNAG.MAGAZYN and STANYIL.WERSJAREF=POZFAK.WERSJAREF)
  where (NAGFAK.ZAKUP = 0)
    and (strmulticmp(';'||NAGFAK.AKCEPTACJA||';',';1;8;')>0)
    and (NAGFAK.ANULOWANIE = 0)
    and (NAGFAK.NIEOBROT < 2)
    and (TOWARY.USLUGA<>1)
    and (NAGFAK.DATA >= :oddaty)
    and (NAGFAK.DATA <= :dodaty)
    and DOKUMNAG.MAGAZYN = :magazyn
  group by POZFAK.WERSJAREF
  having sum(POZFAK.ILOSCM - POZFAK.PILOSCM)>0
  order by POZFAK.WERSJAREF
    into :ktm,:wersjaref,:nazwa,:miara,:stanmax,:ilosc_sprzedana,:sprznet,:koszt_sprzedazy,:zysk
  do begin
    --
    cena_zakupu = :koszt_sprzedazy / :ilosc_sprzedana;
    wielkosc_bufora = :stanmax; --powinna byc wielkosc usredniona w czasie
    koszt_bufora = :wielkosc_bufora * :cena_zakupu;
    if(:koszt_bufora<>0) then begin
      -- wylicz IT class
      it = :koszt_sprzedazy/:koszt_bufora;
      -- wylicz ROI class
      roi = :zysk/:koszt_bufora;
    end else begin
      it = 0;
      roi = 0;
    end
    suspend;
  end
end^
SET TERM ; ^
