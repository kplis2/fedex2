--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_VAC_CARD_UCP(
      ONDATE timestamp,
      EMPL integer = null,
      EMPLCONTRACT integer = null,
      COMPANY integer = null)
   as
declare variable VYEAR integer;
declare variable VACCARDYEAR integer;
declare variable dwp smallint;
declare variable dwnp smallint;
declare variable dwpused integer;
declare variable dwnpused integer;
declare variable paylimit integer;
declare variable nopaylimit integer;
declare variable contrref integer;
declare variable eref integer;
declare variable annexe integer;
begin
--MW: Personel - zakladanie kart urlopowych
  vyear = cast(extract(year from ondate) as integer);
--Moze jakis konfig
  execute procedure get_config('VACUCPPAYCOLUMN', 2) returning_values dwp;
  execute procedure get_config('VACUCPNOPAYCOLUMN', 2) returning_values dwnp;
  execute procedure get_config('VACCARDYEAR', 2) returning_values vaccardyear;
  if (vyear < vaccardyear) then
    if (empl < 0) then vyear = vaccardyear;
    else exception universal 'Nie można naliczać kart dla tego roku!!!';

  if (coalesce(company,0) = 0) then
  begin
    execute procedure get_global_param('CURRENTCOMPANY') returning_values :company;
    company = coalesce(company,0);
  end

  empl = abs(coalesce(empl,0));
  emplcontract = coalesce(emplcontract,0);

  if (emplcontract = 0) then
  begin
    if (empl = 0) then
    begin
      if(company = 0) then
        exception universal 'Błąd parametrów wejściowych';
    end
  end
  for
    select distinct c.emplcontract, c.employee
      from emplcontrhist c
      where (c.employee = :empl or :empl = 0)
        and c.fromdate <= :ondate
        and (c.todate is null or c.todate >= cast(:vyear || '/1/1' as date))
        and c.empltype > 1
        and (c.emplcontract = :emplcontract or :emplcontract = 0)
    into contrref, eref
  do begin

    select nopaylimit, paylimit
      from e_calculate_ucpvaclimits(:contrref, :ondate)
    into :nopaylimit, :paylimit;
    nopaylimit = coalesce(:nopaylimit,0);
    paylimit = coalesce(:paylimit,0);

    select sum(case when ecolumn = :dwp then workdays else 0 end),
           sum(case when ecolumn = :dwnp then workdays else 0 end)
      from eabsences
      where ayear = :vyear
        and correction in (0,2)
        and emplcontract = :contrref
      into :dwpused,:dwnpused;

    if(:nopaylimit > 0 or :paylimit > 0 ) then
    begin

      if (not exists(select first 1 1 from evaclimitsucp
                       where vyear = :vyear and emplcontract = :contrref)
      ) then
        insert into evaclimitsucp (employee, vyear, emplcontract, paylimit, nopaylimit, company,
          paylimitcons, nopaylimitcons
        ) values (:eref, :vyear, :contrref, :paylimit, :nopaylimit, :company,
          :dwpused, :dwnpused
        );
      else
        update evaclimitsucp set paylimit = :paylimit, nopaylimit = :nopaylimit,
          paylimitcons = :dwpused, nopaylimitcons = :dwnpused
        where vyear = :vyear and emplcontract = :contrref;
    end
    else
      update evaclimitsucp set paylimit = 0, nopaylimit = 0, paylimitcons = :dwpused,
        nopaylimitcons = :dwnpused
         where vyear = :vyear and emplcontract = :contrref;
  end
end^
SET TERM ; ^
