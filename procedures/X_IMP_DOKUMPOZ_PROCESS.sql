--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_IMP_DOKUMPOZ_PROCESS(
      REF_IMP INTEGER_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING1024)
   as
declare variable NAGID_IMP STRING120;
declare variable POZID_IMP STRING120;
declare variable NUMER_IMP INTEGER_ID;
declare variable IDARTYKULU_IMP STRING120;
declare variable JEDNOSTKA_IMP STRING100;
declare variable VAT_IMP VAT_ID;
declare variable MAGAZYN_IMP STRING6;
declare variable MAG2_IMP STRING6;
declare variable ILOSC_IMP QUANTITY_MWS;
declare variable CENANETTO_IMP CENA_ID;
declare variable CENABRUTTO_IMP CENA_ID;
declare variable WARTOSCNETTO_IMP CENA_ID;
declare variable WARTOSCBRUTTO_IMP CENA_ID;
declare variable UWAGI_IMP STRING255;
declare variable IDPOZKORYGOWANEJ_IMP INTEGER_ID;
declare variable WERSJAREF_LOC INTEGER_ID;
declare variable REF_NAG_LOC INTEGER_ID;
declare variable KTM KTM_ID;
declare variable MIARA_LOC STRING10;
declare variable JEDNO TOWJEDN;
declare variable TYP STRING3;
declare variable ILOSC_KORYGOWANEGO QUANTITY_MWS;
declare variable CZY_WSTAWIC_POZYCJE SMALLINT_ID;
declare variable POZREFKORY INTEGER_ID;
begin
  status = 8;
  msg = '';
  czy_wstawic_pozycje =1;
  if (not exists(select first 1 1 from x_imp_dokumpoz x where x.nagimpref = :ref_imp)) then begin
    msg = 'Brak pozycji do dokumentu :( '||coalesce(:ref_imp, 0);
    status = 8;
    suspend;
    exit;
  end

-- pobranie NagId
  select x.nagid, x.typ
    from x_imp_dokumnag x
      where x.ref = :ref_imp
  into :nagid_imp, :typ;

-- pobranie ref dokumnaga
  select d.ref
    from dokumnag d
      where d.int_id = :nagid_imp
  into :ref_nag_loc;

-- dla kazdej pozycji dokumentu
  for
    select POZID, NUMER, IDARTYKULU, JEDNOSTKA, VAT, MAGAZYN, MAG2, ILOSC, CENANETTO, CENABRUTTO,
             WARTOSCNETTO, WARTOSCBRUTTO, UWAGI, IDPOZKORYGOWANEJ
      from X_IMP_DOKUMPOZ
      where nagimpref = :ref_imp
      into :pozid_imp, :NUMER_imp, :IDARTYKULU_imp, :JEDNOSTKA_imp, :VAT_imp, :MAGAZYN_imp, :MAG2_imp, :ILOSC_imp, :CENANETTO_imp, :CENABRUTTO_imp,
           :WARTOSCNETTO_imp, :WARTOSCBRUTTO_imp, :UWAGI_imp, :IDPOZKORYGOWANEJ_imp
  do begin
      wersjaref_loc = null;
      czy_wstawic_pozycje =1;
      miara_loc = null;
      jedno = null;

      -- pobranie wersji ->WERSJE
      select first 1 w.ref, t.ktm
        from wersje w
        join towary t on (t.ktm = w.ktm)
          where t.int_id = :idartykulu_imp
            and w.akt = 1
      into :wersjaref_loc, :ktm;
    
      if (wersjaref_loc is null) then begin
        msg = 'Brak takiego towaru lub towar jest nieaktywny:' ||coalesce(:idartykulu_imp, 0);
        status = 8;
        suspend;
        exit;
      end
    
      -- weryfikacja i znalezienie jednostki miary w towjedn
      -- Wapro nie ma tabeli jednostki, dostajemy wrecz opisowe jednostki, dlatego podczas importu towaru z nowa jednostka  wsadzamy ich symbol do naszego opisu jednostek
      select first 1 miara
        from miara m
        where upper(m.opis) = upper(trim(:jednostka_imp))
      into :miara_loc;
    
      -- nie znalezlimy jednostki po opisie, przymuj za tym ze dostaem wlasciwy symbol jednostki
      if (:miara_loc is null) then
        miara_loc = substring(jednostka_imp from 1 for 10);

      select first 1 t.ref
        from towjedn t
        where t.ktm = :ktm and t.jedn = :miara_loc
      into :jedno;
    
      if (jedno is null) then begin
        msg = 'Brak jednostki (lub bledna) na pozycji. ' ||coalesce(:jednostka_imp, 0);
        status = 8;
        suspend;
        exit;
      end
      -- jezeli typ to WZk to nalezy pobrac dokument korygowany i policzyc roznice ilosci
      --poniewaz w WAPRO korekta to po prostu nowy dokument z nowymi ilosciami
      if (coalesce(typ,'') in ('WZk','PZk') ) then
      begin

        select dp.ref, dp.ilosc  from dokumpoz dp
            where dp.int_id = cast( coalesce(:idpozkorygowanej_imp,'0') as string40)
        into :pozrefkory, :ilosc_korygowanego;
        ilosc_imp = ilosc_korygowanego - ilosc_imp ;
        --jezeli roznica jest zerowa to nie wystawiamy korekty na ta pozycje
        if (ilosc_imp <= 0) then begin
            czy_wstawic_pozycje = 0;
        end
      end
      else begin
        idpozkorygowanej_imp = null;
      end
    -- wstawianie pozycji
    if( czy_wstawic_pozycje = 1) then
    begin
      insert into DOKUMPOZ (DOKUMENT, NUMER, KTM, JEDNO,
      ILOSC, CENANET, CENABRU, WARTSNETTO,
              WARTSBRUTTO, WERSJAREF, OPIS, GR_VAT,  INT_ID, INT_DATAOSTPRZETW,
              KORTOPOZ)
      values (:ref_nag_loc, :NUMER_imp, :ktm, :jedno,
       :ilosc_imp, :cenanetto_imp,
              :cenabrutto_imp, :wartoscnetto_imp, :wartoscbrutto_imp, :wersjaref_loc, :uwagi_imp, :vat_imp,  :pozid_imp, current_timestamp(0),
              :pozrefkory);
    end
  end
-- update -> przetworzony
  status = 0;
  msg = 'ok :)';
  suspend;
end^
SET TERM ; ^
