--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ROZRACH_TO_BALANCE_COMP(
      FSBMODE smallint,
      TRANSFERINC smallint,
      CURRENTCOMPANY COMPANIES_ID)
  returns (
      ROZREF integer,
      FSBREF integer,
      ACCOUNT ACCOUNT_ID,
      DICTDEF SLO_ID,
      DICTPOS SLOPOZ_ID,
      DICTGRP DICTGRP_ID,
      DEBIT DT_AMOUNT,
      CREDIT CT_AMOUNT,
      CURRENCY WALUTA_ID,
      GRPDEBIT DT_AMOUNT,
      GRPCREDIT CT_AMOUNT,
      COMPANY COMPANIES_ID,
      FSBKONTOFK STRING22,
      GRPBTRANAMOUNTBANK CENY,
      BTRANAMOUNTBANK CENY,
      SLODEF SLO_ID,
      SLOPOZ SLOPOZ_ID,
      SYMBFAK SYMBOL_ID,
      KONTOFK KONTO_ID,
      WALUTA WALUTA_ID,
      SLOKOD STRING80,
      SLONAZWA STRING255,
      DATAOTW timestamp,
      DATAPLAT timestamp,
      SALDOWINIEN CENY,
      SALDOMA CENY)
   as
begin
  for
    select R.REF,FSB.REF,FSB.ACCOUNT,FSB.DICTDEF,FSB.DICTPOS,FSB.DICTGRP,FSB.DEBIT,
           FSB.CREDIT,FSB.CURRENCY,FSB.GRPDEBIT,FSB.GRPCREDIT,FSB.COMPANY,FSB.KONTOFK,
           FSB.GRPBTRANAMOUNTBANK,FSB.BTRANAMOUNTBANK,R.SLODEF, R.SLOPOZ, R.SYMBFAK,
           R.KONTOFK as ROZKONTOFK, R.WALUTA,R.SLOKOD, R.SLONAZWA, R.DATAOTW, R.DATAPLAT,
      case when R.saldowm > 0 then R.saldowm else 0 end as SALDOWINIEN,
      case when -R.saldowm - R.btranamountbank*:transferinc > 0 then -R.saldowm + R.btranamountbank*:transferinc else 0 end as SALDOMA
      from fsbalances FSB
        left join rozrach R on (R.kontofk=FSB.account
          and R.slodef=FSB.dictdef
          and R.slopoz=FSB.dictpos
          and R.company=FSB.company
          and R.waluta = FSB.currency
         )
        where R.saldo > 0
          and R.company = :currentcompany
          and (
            (:fsbmode = 0 and FSB.debit > 0 and FSB.credit > 0)
            or (
              :fsbmode = 1
              and (FSB.grpdebit > 0 and (FSB.grpcredit - FSB.grpbtranamountbank*:transferinc) > 0)
              and (
                (FSB.debit <> FSB.grpdebit)
                or ((FSB.credit - FSB.btranamountbank*:transferinc) <> (FSB.grpcredit - FSB.grpbtranamountbank*:transferinc))
              )
            )
          )
        order by FSB.account
    into :ROZREF,:FSBREF,:ACCOUNT,:DICTDEF,:DICTPOS,:DICTGRP,:DEBIT,:CREDIT,:CURRENCY,
         :GRPDEBIT,:GRPCREDIT,:COMPANY,:FSBKONTOFK,:GRPBTRANAMOUNTBANK,:BTRANAMOUNTBANK,
         :SLODEF,:SLOPOZ,:SYMBFAK,:KONTOFK,:WALUTA,:SLOKOD,:SLONAZWA,:DATAOTW,:DATAPLAT,
         :SALDOWINIEN,:SALDOMA
  do begin
    suspend;
  end
end^
SET TERM ; ^
