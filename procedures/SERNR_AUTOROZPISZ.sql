--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SERNR_AUTOROZPISZ(
      POZYCJA integer,
      TYP char(1) CHARACTER SET UTF8                           ,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      ILOSC numeric(14,4))
   as
declare variable serialnr varchar(40);
declare variable serialil numeric(14,4);
declare variable numer integer;
begin
  select max(NUMER) from DOKUMSER where REFPOZ = :pozycja and TYP = :typ into :numer;
  if(:numer is null) then numer = 0;
  for select STANYSER.ILOSC, STANYSER.SERIALNR
  from STANYCEN
  left join STANYSER on (STANYSER.STANCEN = STANYCEN.REF)
  where STANYCEN.WERSJAREF = :wersjaref and STANYCEN.MAGAZYN = MAGAZYN and STANYSER.ILOSC > 0
  and STANYSER.SERIALNR not in (select DOKUMSER.ODSERIAL from DOKUMSER where DOKUMSER.REFPOZ = :pozycja and DOKUMSER.TYP = :typ)
  order by STANYSER.DATA, STANYSER.STANCEN
  into :serialil, :serialnr
  do begin
   if(:ilosc > 0) then begin
     if(:serialil > :ilosc ) then serialil = :ilosc;
     if(:serialil > 0) then begin
       numer = :numer + 1;
       insert into DOKUMSER(REFPOZ,TYP, NUMER, ORD, ODSERIAL, ILOSC)
         values(:pozycja, :typ, :numer, 1, :serialnr, :serialil);
       ilosc = :ilosc - :serialil;
     end
   end
  end
end^
SET TERM ; ^
