--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BLANK_RKDOKNAG_DOMPOZOPER(
      OPER RKDEFOPER_ID)
  returns (
      DOMPOZOPER RKPOZOPER_ID)
   as
begin
/*ts: procedura wywolywana poprzez blank: bl_rkdoknag_dompozoper; odpowiada
  za ustawienie domyslnej transakcji przy dodawaniu dokumentu
  do wyciągu bankowego. nie robimy tego qfindem, bo nie chcemy
  zmieniac kontekstu w innich miejscach aplikacji. (bs97859) */

  select dompozoper from rkdefoper
    where symbol = :oper
    into :dompozoper;
  suspend;
end^
SET TERM ; ^
