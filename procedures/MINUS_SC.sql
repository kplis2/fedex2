--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MINUS_SC(
      MAGAZYN char(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      ILOSC numeric(14,4),
      CENA numeric(14,4),
      DOSTAWA integer,
      SERIALNR varchar(40) CHARACTER SET UTF8                           ,
      DATA timestamp,
      CENA_KOSZT numeric(14,4),
      ORGDOKUMSER integer,
      DATADOK timestamp)
   as
declare variable typ_mag char(1);
declare variable fifo char(1);
declare variable delzero integer;
declare variable i decimal(14,4);
declare variable stminus smallint;
declare variable stan decimal(14,4);
declare variable censtan decimal(14,4);
declare variable refsc integer;
declare variable ilser numeric(14,4);
declare variable refser integer;
begin

  /* sprawdzenie czy nie bylo inwentaryzacji zamknietych z pozniejsza data*/
  if(exists (select first 1 1
               from inwentap inwp
               join inwenta inw on inwp.inwenta = inw.ref
               where inwp.ktm = :ktm and inw.data > :data and inw.zamk = 2 and inw.magazyn=:magazyn)) then
    exception INWENTA_ZAAKCEPTOWANE 'Inwentaryzacja z pozycją '||:ktm||' zamknięta z datą późniejszą niż data dokumentu.';

  select TYP,FIFO,USUNZERA, STANYCEN from DEFMAGAZ where SYMBOL=:magazyn into :typ_mag,:fifo,:delzero, :stminus;
  if(:ilosc is null) then ilosc = 0;
  if(:cena is null) then cena = 0;
  if(:stminus is null) then stminus = 0;
  i = - ilosc;
  if(:typ_mag = 'E') then begin
    select max(ref), sum(ilosc) from STANYCEN where MAGAZYN = :magazyn and KTM =:ktm and WERSJA=:wersja into :refsc, :stan;
    if(:refsc is null) then refsc = 0;
    select cena from STANYIL where MAGAZYN=:magazyn and KTM=:ktm and WERSJA = :wersja into :censtan;
    if(:censtan is null) then censtan =0;
    if(:stan <> 0 and :cena <> :censtan) then exception MAGDOK_WRONG_PRICE_E;
    if(:stminus = 0) then begin
      if(:stan - :ilosc < 0) then exception STCEN_MINUS;
    end
    if(:refsc > 0) then
      update STANYCEN set ILOSC = ILOSC - :ilosc, cena = :cena where MAGAZYN=:magazyn and KTM = :ktm and WERSJA = :wersja and DOSTAWA = 0;
    else begin
      execute procedure GEN_REF('STANYCEN') returning_values :refsc;
      insert into STANYCEN(REF,MAGAZYN,KTM,WERSJA,ILOSC,CENA,DOSTAWA,DATA)
        values (:refsc,:magazyn,:ktm,:wersja,:i,:cena,0,:data);
    end
  end else
  if(:typ_mag = 'S') then begin
    select max(ref), sum(ilosc) from STANYCEN where MAGAZYN = :magazyn and KTM =:ktm and WERSJA=:wersja and DOSTAWA=0 into :refsc, :stan;
    if(:refsc is null) then refsc = 0;
    if(:stminus = 0) then begin
      if(:stan - :ilosc < 0) then exception STCEN_MINUS;
    end
    if(:refsc > 0) then begin
      update STANYCEN set WARTOSC = WARTOSC-:ilosc*:cena, ILOSC = ILOSC - :ilosc where MAGAZYN=:magazyn and KTM = :ktm and WERSJA = :wersja and DOSTAWA = 0;
    end else begin
      execute procedure GEN_REF('STANYCEN') returning_values :refsc;
      insert into STANYCEN(REF,MAGAZYN,KTM,WERSJA,ILOSC,CENA,WARTOSC,DOSTAWA,DATA)
        values (:refsc,:magazyn,:ktm,:wersja,:i,:cena,:i*:cena, 0,:data);
    end
  end else
  if(:typ_mag = 'C' ) then begin
    select max(ref),sum(ilosc) from STANYCEN where MAGAZYN = :magazyn and KTM =:ktm and WERSJA=:wersja and cena = :cena into :refsc, :stan;
    if(:stminus = 0) then begin
      if(:stan - :ilosc < 0) then exception STCEN_MINUS;
    end
    if(:refsc > 0) then
      update STANYCEN set ILOSC = ILOSC - :ilosc where MAGAZYN=:magazyn and KTM = :ktm and WERSJA = :wersja and CENA = :cena and DOSTAWA = 0;
    else begin
      execute procedure GEN_REF('STANYCEN') returning_values :refsc;
      insert into STANYCEN(REF,MAGAZYN,KTM,WERSJA,ILOSC,CENA,DOSTAWA,DATA,CENA_KOSZT)
        values (:refsc,:magazyn,:ktm,:wersja,:i,:cena,0,:data,:cena_koszt);
    end
  end else
  if(:typ_mag = 'P')then begin
    if((:dostawa is null) or (:dostawa = 0))then exception STCEN_DOSTAWA_NULL;
    select max(ref),sum(ilosc) from STANYCEN where MAGAZYN = :magazyn and KTM =:ktm and WERSJA=:wersja and cena = :cena and DOSTAWA = :dostawa into :refsc,:stan;
    if(:stminus = 0) then begin
      if(:stan - :ilosc < 0) then exception STCEN_MINUS;
    end
    if(:refsc > 0) then
      update STANYCEN set ILOSC = ILOSC - :ilosc where MAGAZYN=:magazyn and KTM = :ktm and WERSJA = :wersja and CENA = :cena and DOSTAWA = :dostawa;
    else begin
      execute procedure GEN_REF('STANYCEN') returning_values :refsc;
      insert into STANYCEN(REF,MAGAZYN,KTM,WERSJA,ILOSC,CENA,DOSTAWA,DATA,CENA_KOSZT)
        values (:refsc,:magazyn,:ktm,:wersja,:i,:cena,:dostawa,:data,:cena_koszt);
    end
 end
 if(:serialnr <>'' and :refsc > 0) then begin
   select count(*),sum(ilosc) from STANYSER where STANCEN = :refsc and serialnr = :serialnr into :refser,:ilser;
   if(:ilser is null) then ilser = 0;
   if(:ilser < :ilosc) then exception STCEN_BRAKSER;
   update dokumser set stan = 'W' where ref = :orgdokumser and stan='B';
   if(:refser > 0) then
     update STANYSER set ILOSC = ILOSC-:ILOSC where STANCEN = :refsc and serialnr = :serialnr ;
 end
 execute procedure STANYARCH_CHANGE('M',:magazyn,:ktm,:wersja,null,cast(:datadok as timestamp),-:ilosc,cast(-:ilosc * :cena as numeric(14,2)), :dostawa);
 if(:delzero = 1) then
   delete from STANYCEN where ILOSC = 0 and MAGAZYN=:magazyn and KTM = :ktm and WERSJA=:wersja and zarezerw = 0 and zablokow = 0 and zamowiono = 0 and wartosc = 0;
end^
SET TERM ; ^
