--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_4(
      BKDOC integer)
   as
DECLARE VARIABLE ACCOUNT VARCHAR(20);
  DECLARE VARIABLE SIDE INTEGER;
  DECLARE VARIABLE AMOUNT NUMERIC(14,2);
  DECLARE VARIABLE TYP VARCHAR(3);
  DECLARE VARIABLE DESCRIPT VARCHAR(255);
  DECLARE VARIABLE DICTPOS INTEGER;
  DECLARE VARIABLE DICTDEF INTEGER;
  DECLARE VARIABLE ODDZIAL VARCHAR(255);
  DECLARE VARIABLE ODDZIAL2 VARCHAR(255);
  DECLARE VARIABLE SYMBOL VARCHAR(20);
  DECLARE VARIABLE MAGAZYN VARCHAR(255);
  DECLARE VARIABLE MAGAZYN2 VARCHAR(255);
  DECLARE VARIABLE NOTA SMALLINT;
  DECLARE VARIABLE wydania SMALLINT;
begin
  -- schemat dekretowania - dokumenty magazynowe

  nota = 0;
  select B.symbol, B.descript, D.slodef, D.slopoz, D.pwartosc, D.typ, O1.symbol, O2.symbol,
      D.magazyn, D.mag2, DD.wydania
    from bkdocs B
      join dokumnag D on (D.ref = B.oref and B.otable = 'DOKUMNAG')
      join defdokum DD on (DD.symbol = D.typ)
      join oddzialy O1 on (D.oddzial = O1.oddzial)
      left join oddzialy O2 on (D.oddzial2 = O2.oddzial)
    where B.ref = :bkdoc
    into :symbol, :descript, :dictdef, :dictpos, :amount, :typ, :oddzial, :oddzial2,
      :magazyn, :magazyn2, :wydania;

  if (symbol is null) then
  begin
    nota = 1;
    select B.symbol, 'korekta do ' || DM.symbol, D.wartosc, D.typ, O1.symbol, O2.symbol,
        D.magazyn, D.mag2, DD.wydania
      from bkdocs B
        join dokumnot D on (D.ref = B.oref and B.otable = 'DOKUMNOT')
        join defdokum DD on (DD.symbol = D.typ)
        join oddzialy O1 on (D.oddzial = O1.oddzial)
        left join defmagaz M2 on (D.mag2 = M2.symbol)
        left join oddzialy O2 on (M2.oddzial = O2.oddzial)
        left join dokumnag DM on (D.dokumnagkor = DM.ref)
      where B.ref = :bkdoc
      into :symbol, :descript, :amount, :typ, :oddzial, :oddzial2,
        :magazyn, :magazyn2, :wydania;
   if (descript is null) then
     descript = symbol;
  end

  account = '330-' || magazyn;
  side = wydania;

  execute procedure insert_decree(bkdoc, account, side, amount, descript, 0);

  if (typ = 'PZ' or typ = 'PZF') then
  begin
    account = '300' ;
    execute procedure insert_decree(bkdoc, account, 1 - side, amount, descript, 0);
  end else if (typ = 'PZK') then
  begin
    account = '300' ;
    execute procedure insert_decree(bkdoc, account, 1 - side, amount, descript, 0);
  end else if (typ = 'PZI') then
  begin
    account = '300';
    execute procedure insert_decree(bkdoc, account, 1 - side, amount, descript, 0);
  end else if (typ = 'PZU') then
  begin
    account = '300';
    execute procedure insert_decree(bkdoc, account, 1 - side, amount, descript, 0);
  end else if (typ = 'WZ') then
  begin
    account = '731-01';
    execute procedure insert_decree(bkdoc, account, 1 - side, amount, descript, 0);
  end else if (typ = 'ZZ') then
  begin
    account = '731-01';
    execute procedure insert_decree(bkdoc, account, 1 - side, amount, descript, 0);
  end else if (typ = 'MM-') then
  begin
    account = '337-' || magazyn2;
    execute procedure insert_decree(bkdoc, account, 1 - side, amount, descript, 0);
  end else if (typ = 'MM+') then
  begin
    account = '337-' || magazyn;
    execute procedure insert_decree(bkdoc, account, 1 - side, amount, descript, 0);
  end else if (typ = 'IN+')  then
  begin
    account = '264-01';
    execute procedure insert_decree(bkdoc, account, 1 - side, amount, descript, 0);
  end else if (typ = 'IN-')  then
  begin
    account = '264-01';
    execute procedure insert_decree(bkdoc, account, 1 - side, amount, descript, 0);
  end
end^
SET TERM ; ^
