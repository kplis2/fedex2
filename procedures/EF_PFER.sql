--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_PFER(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      RNWAGE numeric(14,2) = 0)
  returns (
      RET numeric(14,2))
   as
declare variable FROMDATE date;
declare variable TODATE date;
declare variable PERSON integer;
declare variable BLOCKDATE timestamp;
declare variable IPER char(6);
declare variable PVALUE numeric(14,2);
declare variable BASEINC numeric(14,2);
declare variable ZUS numeric(14,2);
declare variable ICODE varchar(20);
declare variable BLOCKCODE varchar(20);
declare variable BYEAR integer;
declare variable BDAY integer;
declare variable ZUSDATA integer;
begin
  --DU: personel - wylicza podstawe ubezpieczenia emerytalnego i rentowego

  execute procedure efunc_prdates(payroll, 2)
    returning_values fromdate, todate;

  select iper from epayrolls
    where ref = :payroll
    into :iper;

  select person from employees
    where ref = :employee
    into :person;

  select z.blockdate,
      case when z.baseincyear = extract(year from :fromdate) then z.baseinc else 0 end,
      c.code, c2.code, z.baseincyear, z.ref
    from ezusdata z
      join edictzuscodes c on (z.ascode = c.ref)
      left join edictzuscodes c2 on (z.blockade = c2.ref)
    where z.person = :person
      and z.fromdate = (select max(fromdate) from ezusdata
        where person = :person and fromdate <= :todate)
    into blockdate, :baseinc, :icode, :blockcode, :byear, :zusdata;

  baseinc = coalesce(baseinc,0);

  if (substring(icode from 1 for 2) <> '11') then
  begin
    select sum(p.pvalue) from epayrolls pr
        join eprpos p on (p.payroll = pr.ref and p.ecolumn = 6000)
        join employees e on (e.ref = p.employee and e.person = :person)  --BS35514
      where pr.iper starting with extract(year from :fromdate)
        and pr.iper <= :iper and pr.ref <> :payroll
      into :pvalue;

    if (pvalue is null) then pvalue = 0;

    execute procedure ef_fcolsum(employee, payroll, 'EF_', 'ZUS')
      returning_values ret;
    ret = ret + rnwage;

    execute procedure ef_pval(:employee, :payroll, :prefix, 'ZUS_PODST_FER')
      returning_values zus;

    if (blockcode is not null and blockdate <= todate and byear = extract(year from todate)) then
    begin
      --na wniosek ubezpieczonego
      if (blockcode = '1') then
      begin
        if (ret + pvalue + baseinc > zus) then
          ret = zus - pvalue - baseinc;
      end else
      --na wniosek platnika, program sam kontroluje i sam uzupelnia dane
      if (blockcode = '2') then
      begin
        --jesli blokada jest nieslusznie nalozona
        if (ret + pvalue + baseinc <= zus) then
          update ezusdata z set z.blockade = null, z.baseinc = null, z.blockdate = null where z.ref = :zusdata;
        else
          ret = zus - pvalue - baseinc;
      end else
      --na zadanie ZUS, nie interesuje nas kwota zwiekszenia podstawy
      if (blockcode = '3') then
        ret = 0;
    end else
    if (ret + pvalue + baseinc > zus) then
    begin
      bday = (todate - fromdate + 1) * (zus - pvalue) / (ret + baseinc) - 0.50;
      blockdate = fromdate + bday - 1;

      select ref from edictzuscodes
        where dicttype = 7 and code = '2'
        into :blockcode;

      if (byear < extract(year from blockdate)) then
      begin
        insert into ezusdata (person, fromdate, nfz, insurdate, ascode, invalidity, invalidityfrom,
            invalidityto, disability, retired, nfzfund, workfund, fgsp, aid, speccondition, specfrom,
            specto, ninotification, ispesel, isnip, isother, fulldim, blockade, blockdate)
        select person, :blockdate, nfz, insurdate, ascode, invalidity, invalidityfrom,
            invalidityto, disability, retired, nfzfund, workfund, fgsp, aid, speccondition, specfrom,
            specto, ninotification, ispesel, isnip, isother, fulldim, :blockcode, :blockdate
          from ezusdata
          where ref = :zusdata;
      end else
        update ezusdata z set z.blockdate = :blockdate, z.blockade = :blockcode where z.ref = :zusdata;

      ret = zus - pvalue - baseinc;
    end
  end
  if (ret < 0) then
  begin
    execute procedure EF_ICOL(employee, payroll, prefix, 6000) returning_values :pvalue;
    if (pvalue < 0) then ret = 0;
    else
    if (abs(ret) > pvalue) then ret = -pvalue;
  end
  suspend;
end^
SET TERM ; ^
