--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRPSNS_COUNT_MIN_ORD(
      FRPSNS_REF FRPSNS_ID,
      CURR_ORD SMALLINT_ID = 1,
      MAX_ORD SMALLINT_ID = 1)
  returns (
      MINORD SMALLINT_ID)
   as
declare variable frpsns_parent type of column frpsns.ref;
begin

  /* TS BS106559
     Dla podanego wiersza sprawozdania finansowego zwraca jego minimalna wartosc
     kolejnosci oblizania tj. taka, aby obliczyly sie najpierw wszystkie wartosci
     wierszy (ktore rowniez moga zalezec od innych wierszy), od ktorych podany wiersz zalezy.

     Procedura rekurencyjnie schodzi od wskazanego wiersza poprzez jego zaleznosci bedace sumami,
     az nie natrafi na wiersz, ktory albo nie ma juz zaleznosci, albo zalezy od wierszy,
     ktore same zaleznosci nie moga posiadac (nie sa wierszami sumujacymi).

     Glebokosc rekurencji wyznacza nam minimalna wartosc kolejnosci obliczania
     wskazanego wiersza (maksymalna liczba sum, jakie po drodze nalezy obliczyc
     + maksymalna kolejnosc obliczania zaleznosci ostatniego elementu na tej sciezce).
  */

  if (:curr_ord > :max_ord) then
  begin
    max_ord = :curr_ord;
  end

  if (exists(select first 1 1 from frsumpsns s
          join frpsns f on (f.ref = s.sumpsn)
        where s.frpsn = :frpsns_ref
          and f.algorithm = 2
          and s.frversion = 0)) then
  begin
    for -- Wchodz w rekurencje tylko dla zaleznosci bedacych sumami
      select f.ref
        from frsumpsns s
          join frpsns f on (f.ref = s.sumpsn)
        where s.frpsn = :frpsns_ref
          and f.algorithm = 2
          and s.frversion = 0
        into :frpsns_parent
    do begin
      execute procedure frpsns_count_min_ord(:frpsns_parent, :curr_ord + 1, :max_ord) returning_values :max_ord;
    end
  end else
  begin
    -- Jesli nie mozemy zejsc juz nizej, dodaj kolejnosc obliczania ostatniego wezla.
    select max(f.countord) + :curr_ord
      from frsumpsns s
        join frpsns f on (f.ref = s.sumpsn)
      where s.frpsn = :frpsns_ref
        and f.algorithm <> 2
        and s.frversion = 0
      into :max_ord;
  end

  minord = :max_ord;
  suspend;
end^
SET TERM ; ^
