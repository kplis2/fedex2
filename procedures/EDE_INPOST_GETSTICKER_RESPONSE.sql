--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_INPOST_GETSTICKER_RESPONSE(
      EDEDOCSH_REF EDEDOCH_ID)
  returns (
      OTABLE TABLE_NAME,
      OREF INTEGER_ID)
   as
declare variable shippingdoc listywysd_id;
declare variable shippingdocno string40;
declare variable parent integer_id;
declare variable packageref listywysdroz_opk_id;
declare variable packageno string10;
declare variable labelformat string10;
declare variable label blob_utf8;
declare variable cnt integer_id;
begin
  select oref
    from ededocsh
    where ref = :ededocsh_ref
  into :shippingdoc;

  otable = 'LISTYWYSD';
  oref = :shippingdoc;

  for
    select p.id
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.name = 'pack'
    into :parent
  do begin
    select p.val
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'packref'
    into :packageref;

    select p.val
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'format'
    into :labelformat;

    select p.val
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'label'
    into :label;

    update or insert into listywysdopkrpt(doksped, opk, format, rpt)
      values(:shippingdoc, :packageref, :labelformat, :label)
      matching (doksped, opk);
  end

  suspend;
end^
SET TERM ; ^
