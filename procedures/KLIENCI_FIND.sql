--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KLIENCI_FIND(
      INCOMPANY integer,
      INSEARCH varchar(255) CHARACTER SET UTF8                           )
  returns (
      OUTREF integer)
   as
begin
  outref = null;
  -- szukanie klienta po po fskrot po pelnej nazwie
   select first 1 ref
    from klienci
    where company = :incompany
      and fskrot = :insearch
      and aktywny = 1
    into :outref;

  -- szukanie klienta po fskrot wsrod aktywnych
  if(:outref is null) then begin
  select first 1 ref
    from klienci
    where company = :incompany
      and fskrot starting with :insearch
      and aktywny = 1
    into :outref;
  end

  -- szukanie klienta po fskrot
  if(:outref is null) then begin
    select first 1 ref
      from klienci
      where company = :incompany
        and fskrot starting with :insearch
      into :outref;
  end
  -- szukanie klienta po nipie wsrod aktywnych
  if(:outref is null) then begin
    select first 1 ref
      from klienci
      where company = :incompany
        and prostynip starting with :insearch
        and aktywny = 1
      into :outref;
  end

  -- szukanie ref klienta po nipie
  if(:outref is null) then begin
    select first 1 ref
      from klienci
      where company = :incompany
        and prostynip starting with :insearch
      into :outref;
  end

  -- jeżeli nie znalazl zadnego zwroc -1 (zeby obsluzyc w kodzie)
  if(:outref is null) then
    outref = -1;
end^
SET TERM ; ^
