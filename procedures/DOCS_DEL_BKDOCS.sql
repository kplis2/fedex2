--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOCS_DEL_BKDOCS(
      TYP smallint,
      DOCUMENT_REF integer,
      CURRENT_COMPANY integer =NULL)
   as
begin
  execute procedure DOCUMENTS_DELFROM_BKDOCS(TYP, DOCUMENT_REF);
  if (current_company is null) then
  begin
    execute procedure get_global_param('CURRENTCOMPANY')
    returning_values :current_company;
  end
  if (typ = 0) then
      update NAGFAK set blokada = 0 WHERE REF = :DOCUMENT_REF;
  else if (typ = 1) then
      update DOKUMNAG set BLOKADA = bin_and(BLOKADA, 3) where bin_and(BLOKADA, 4) > 0 and ref = :document_ref;
  else if (typ = 2) then
    update RKRAPKAS set STATUS = 1 where ref = :document_ref and status > 1;
  else if (typ = 3) then
      update DOKUMNOT set BLOKADA = bin_and(BLOKADA, 3) where bin_and(BLOKADA, 4) > 0 and ref = :document_ref;
  else if (typ = 4) then
      update fsclracch set STATUS = 1 where ref = :document_ref and status > 1;
  else if (typ = 5) then
      update epayrolls set STATUS = 1 where cper = :document_ref and status > 1 and empltype = 1 and company = :current_company;
  else if (typ = 6) then
      update epayrolls set STATUS = 1 where cper = :document_ref and status > 1 and empltype > 1 and company = :current_company;
  else if (typ = 7) then
      update valdocs set status = 0 where ref = :document_ref and status = 1;
end^
SET TERM ; ^
