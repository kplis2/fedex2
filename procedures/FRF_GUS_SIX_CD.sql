--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_GUS_SIX_CD(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      FIELD varchar(5) CHARACTER SET UTF8                           )
  returns (
      AMOUNT numeric(15,2))
   as
declare variable frvhdr integer;
declare variable frpsn integer;
declare variable frcol integer;
declare variable yearid integer;
declare variable cacheparams varchar(255);
declare variable ddparams varchar(255);
declare variable company integer;
declare variable frversion integer;
declare variable tmp numeric(15,2);
declare variable yearid_firstday date;
declare variable yearid_lastday date;
declare variable absences integer;
begin
  select frvhdr, frpsn, frcol
    from frvpsns
    where ref = :frvpsn
    into :frvhdr, :frpsn, :frcol;

  yearid = cast(substring(:period from 1 for 4) as integer);
  yearid_firstday = yearid || '/1/1';
  yearid_lastday = yearid || '/12/31';
  select company, frversion
    from frvhdrs
    where ref = :frvhdr
    into :company, :frversion;
  cacheparams = field||';'||yearid||';'||company;
  ddparams = '';

  if (not exists (select ref from frvdrilldown where functionname = 'GUS_SIX_CD' and cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr)) then
  begin
    if (field = 'D5O1') then
    begin
      -- PELNOZATRUDNIENI - stan na 31.12 roku ubieglego
      yearid = yearid - 1;
      yearid_lastday = yearid || '/12/31';
      select count(*) from (
        select e.person
          from employees E
            join employment C on (E.ref = C.employee)
          where C.fromdate <= :yearid_lastday
            and (C.todate is null or C.todate >= :yearid_lastday)
            and e.company = :company
          group by e.person
          having sum(c.workdim) >= 1)
        into :amount;

      select count(distinct e.person)
        from eabsences a
          join employees e on e.ref = a.employee
          join employment m on e.ref = m.employee
        where a.fromdate <= :yearid_lastday and a.todate >= :yearid_lastday
          and e.company = :company
          and m.workdim >= 1
          and a.ecolumn in (260,330,300)
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end else if (field = 'D5K1') then
    begin
      -- W TYM KOBIETY - stan na 31.12 roku ubieglego
      yearid = yearid - 1;
      yearid_lastday = yearid || '/12/31';

      select count(distinct e.person)
        from employees E
          join employment C on (E.ref = C.employee)
          join persons p on p.ref = e.person
        where C.fromdate <= :yearid_lastday
          and (C.todate is null or C.todate >= :yearid_lastday)
          and C.workdim >= 1
          and e.company = :company
          and p.sex = 0
        into :amount;

      select count(distinct e.person)
        from eabsences a
          join employees e on e.ref = a.employee
          join employment m on e.ref = m.employee
          join persons p on p.ref = e.person
        where a.fromdate <= :yearid_lastday and a.todate >= :yearid_lastday
          and e.company = :company
          and m.workdim >= 1
          and p.sex = 0
          and a.ecolumn in (260,330,300)
        into :absences;

      amount = coalesce(:amount,0) - coalesce(:absences,0);
    end else if (field = 'D5O2') then
    begin
      -- OSOBY PRZYJETE DO PRACY
      execute procedure z06_hired(yearid, 0,0,null,0,0,company) returning_values amount;
      --jeszcze wojskowi...
      select count(distinct e.person)
        from employees E
          join persons P on (P.ref = E.person)
          join employment C on (E.ref = C.employee)
          join eabsences A on (A.employee = E.ref)
        where C.workdim >= 1
          and A.todate >= :yearid_firstday
          and A.todate <= :yearid_lastday
          and C.fromdate < A.todate and C.todate > A.todate
          and a.ecolumn = 330
          and e.company = :company
        into :tmp;

      amount = coalesce(amount,0) + coalesce(tmp,0);
    end else if (field = 'D5K2') then
    begin
      -- OSOBY PRZYJETE DO PRACY - w tym kobiety
      execute procedure z06_hired(yearid, 1,0,null,0,0,company) returning_values amount;
       --...i wojskowe..
       select count(distinct e.person)
        from employees E
          join persons P on (P.ref = E.person)
          join employment C on (E.ref = C.employee)
          join eabsences A on (A.employee = E.ref)
        where C.workdim >= 1
          and A.todate >= :yearid_firstday
          and A.todate <= :yearid_lastday
          and C.fromdate < A.todate and C.todate > A.todate
          and a.ecolumn = 330
          and p.sex = 0
          and e.company = :company
        into :tmp;

      amount = coalesce(amount,0) + coalesce(tmp,0);
    end else if (field = 'D5O3') then
    begin
      --OSOBY PODEJMUJACE PRACE PO RAZ PIERWSZY
      execute procedure z06_hired(yearid,0,1,null,0,0,company) returning_values amount;
    end else if (field = 'D5K3') then
    begin
      -- OSOBY PODEJMUJACE PRACE PO RAZ PIERWSZY - w tym kobiety
      execute procedure z06_hired(yearid, 1,1,null,0,0,company) returning_values amount;
    end else if (field = 'D5O4') then
    begin
      -- OSOBY PODEJMUJACE PRACE PO RAZ PIERWSZY - wyksztacenie wyższe
      execute procedure z06_hired(yearid, 0,1,50,0,0,company) returning_values amount;
    end else if (field = 'D5K4') then
    begin
      -- OSOBY PODEJMUJACE PRACE PO RAZ PIERWSZY - wyksztacenie wyższe - w tym kobiety
      execute procedure z06_hired(yearid, 1,1,50,0,0,company) returning_values amount;
    end else if (field = 'D5O5') then
    begin
      -- OSOBY PODEJMUJACE PRACE PO RAZ PIERWSZY - szkoy policealne i srednie zawodowe
      execute procedure z06_hired(yearid, 0,1,40,0,0,company) returning_values tmp;
      execute procedure z06_hired(yearid, 0,1,31,0,0,company) returning_values amount;
      amount = amount + tmp;
    end else if (field = 'D5K5') then
    begin
      -- OSOBY PODEJMUJACE PRACE PO RAZ PIERWSZY - szkoy policealne i srednie zawodowe - w tym kobiety
      execute procedure z06_hired(yearid, 1,1,40,0,0,company) returning_values tmp;
      execute procedure z06_hired(yearid, 1,1,31,0,0,company) returning_values amount;
      amount = amount + tmp;
    end else if (field = 'D5O6') then
    begin
      -- OSOBY PODEJMUJACE PRACE PO RAZ PIERWSZY - liceum ogolnoksztalcace
      execute procedure z06_hired(yearid, 0,1,32,0,0,company) returning_values amount;
    end else if (field = 'D5K6') then
    begin
      -- OSOBY PODEJMUJACE PRACE PO RAZ PIERWSZY - liceum ogolnoksztalcace - w tym kobiety
      execute procedure z06_hired(yearid, 1,1,32,0,0,company) returning_values amount;
    end else if (field = 'D5O7') then
    begin
      -- OSOBY PODEJMUJACE PRACE PO RAZ PIERWSZY - wyksztalcenie zasadnicze
      execute procedure z06_hired(yearid, 0,1,20,0,0,company) returning_values amount;
    end else if (field = 'D5K7') then
    begin
      -- OSOBY PODEJMUJACE PRACE PO RAZ PIERWSZY - wyksztalcenie zasadnicze - w tym kobiety
      execute procedure z06_hired(yearid, 1,1,32,0,0,company) returning_values amount;
    end else if (field = 'D5O8') then
    begin
      -- OSOBY POPRZEDNIO PRACUJACE
      execute procedure z06_hired(yearid, 0,0,null,1,0,company) returning_values amount;
    end else if (field = 'D5K8') then
    begin
      -- OSOBY POPRZEDNIO PRACUJACE - w tym kobiety
      execute procedure z06_hired(yearid, 1,0,null,1,0,company) returning_values amount;
    end else if (field = 'D5O9') then
    begin
      -- OSOBY POPRZEDNIO PRACUJACE, W TYM PODEJMUJACE PRACE PO PRZERWIE NIE DLUZSZEJ NIZ 1 MIESIAC
      execute procedure z06_hired(yearid, 0,0,null,2,0,company) returning_values amount;
    end else if (field = 'D5K9') then
    begin
      -- OSOBY POPRZEDNIO PRACUJACE, W TYM PODEJMUJACE PRACE PO PRZERWIE NIE DLUZSZEJ NIZ 1 MIESIAC - w tym kobiety
      execute procedure z06_hired(yearid, 1,0,null,2,0,company) returning_values amount;
    end else if (field = 'D5O10') then
    begin
      -- OSOBY POWRACAJACE Z URLOPU BEZPL LUB WYCHOWAWCZEGO
      execute procedure z06_hired(yearid, 0,0,null,0,1,company) returning_values amount;
    end else if (field = 'D5K10') then
    begin
      -- OSOBY POWRACAJACE Z URLOPU BEZPL LUB WYCHOWAWCZEGO - w tym kobiety
      execute procedure z06_hired(yearid, 1,0,null,0,1,company) returning_values amount;
    end else if (field = 'D5O13') then
    begin
      -- OSOBY ZWOLNIONE Z PRACY
      execute procedure z06_fired(yearid,null,0,company) returning_values amount;
      select count(person)
        from get_persons_wych_or_bezpl(:yearid_firstday,:yearid_lastday,:company,0)
        into :tmp;
      amount = amount + tmp;
    end else if (field = 'D5K13') then
    begin
      -- OSOBY ZWOLNIONE Z PRACY - w tym kobiety
      execute procedure z06_fired(yearid,null,1,company) returning_values amount;
      select count(person)
        from get_persons_wych_or_bezpl(:yearid_firstday,:yearid_lastday,:company,0)
        into :tmp;
      amount = amount + tmp;
    end else if (field = 'D5O14') then
    begin
      -- OSOBY ZWOLNIONE Z PRACY - w drodze wypowiedzenia przez pracodawce
      execute procedure z06_fired(yearid,1,0,company) returning_values amount;
    end else if (field = 'D5K14') then
    begin
      -- OSOBY ZWOLNIONE Z PRACY - w drodze wypowiedzenia przez pracodawce - w tym kobiety
      execute procedure z06_fired(yearid,1,1,company) returning_values amount;
    end else if (field = 'D5K15') then
    begin
      -- OSOBY ZWOLNIONE Z PRACY - w drodze wypowiedzenia przez pracodawce,
      -- w tym z przyczyn niedotyczacych pracowniknikow
      execute procedure z06_fired(yearid,7,0,company) returning_values amount;
    end else if (field = 'D5K15') then
    begin
      -- OSOBY ZWOLNIONE Z PRACY - w drodze wypowiedzenia przez pracodawce,
      -- w tym z przyczyn niedotyczacych pracowniknikow - w tym kobiety
      execute procedure z06_fired(yearid,7,1,company) returning_values amount;
    end else if (field = 'D5O16') then
    begin
      -- OSOBY ZWOLNIONE Z PRACY - w drodze wypowiedzenia przez pracownika
      execute procedure z06_fired(yearid,2,0,company) returning_values amount;
    end else if (field = 'D5K16') then
    begin
      -- OSOBY ZWOLNIONE Z PRACY - w drodze wypowiedzenia przez pracownika - w tym kobiety
      execute procedure z06_fired(yearid,2,1,company) returning_values amount;
    end else if (field = 'D5O17') then
    begin
      -- OSOBY ZWOLNIONE Z PRACY - z powodu niezdolnosci do pracy
      execute procedure z06_fired(yearid,3,0,company) returning_values amount;
    end else if (field = 'D5K17') then
    begin
      -- OSOBY ZWOLNIONE Z PRACY - z powodu niezdolnosci do pracy - w tym kobiety
      execute procedure z06_fired(yearid,3,1,company) returning_values amount;
    end else if (field = 'D5O18') then
    begin
      -- OSOBY ZWOLNIONE Z PRACY - przeniesieni na emeryture
      execute procedure z06_fired(yearid,4,0,company) returning_values amount;
    end else if (field = 'D5K18') then
    begin
      -- OSOBY ZWOLNIONE Z PRACY - przeniesieni na emeryture - w tym kobiety
      execute procedure z06_fired(yearid,4,1,company) returning_values amount;
    end else if (field = 'D5O19') then
    begin
      -- OSOBY ZWOLNIONE Z PRACY - na mocy porozumienia stron
      execute procedure z06_fired(yearid,8,0,company) returning_values amount;
    end else if (field = 'D5K19') then
    begin
      -- OSOBY ZWOLNIONE Z PRACY - na mocy porozumienia stron - w tym kobiety
      execute procedure z06_fired(yearid,8,1,company) returning_values amount;
    end else if (field = 'D5O20') then
    begin
      -- OSOBY KTORE OTRZYMALY URLOP BEZPL LUB WYCHOWAWCZY
      select count(person)
        from get_persons_wych_or_bezpl(:yearid_firstday,:yearid_lastday,:company,0)
        into :amount;
    end else if (field = 'D5K20') then
    begin
      -- OSOBY KTORE OTRZYMALY URLOP BEZPL LUB WYCHOWAWCZY - w tym kobiety
      select count(person)
        from get_persons_wych_or_bezpl(:yearid_firstday,:yearid_lastday,:company,1)
        into :amount;
    end else if (field = 'D5O21') then
    begin
      -- OSOBY ZWOLNIONE Z PRACY - z uplywem czasu na ktory zostali zatrudnieni
      execute procedure z06_fired(yearid,9,0,company) returning_values amount;
    end else if (field = 'D5K21') then
    begin
      -- OSOBY ZWOLNIONE Z PRACY - z uplywem czasu na ktory zostali zatrudnieni - w tym kobiety
      execute procedure z06_fired(yearid,9,1,company) returning_values amount;
    end
  end else
    select first 1 fvalue from frvdrilldown where cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr
      into :amount;
  if (amount is null) then amount = 0;
  insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
     values (:frvhdr, :frpsn, :frcol, 'GUS_SIX_CD', :cacheparams, :ddparams, :amount, :frversion);
  suspend;
end^
SET TERM ; ^
