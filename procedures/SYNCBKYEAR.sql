--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYNCBKYEAR(
      PATTERN BKYEARS_ID,
      DEST BKYEARS_ID)
  returns (
      RET smallint)
   as
declare variable ispattern smallint_id;
begin
  -- Procedura dowiązująca wzorzec do roku ksigowego.
  select c.pattern
    from bkyears b join companies c on (b.company = c.ref)
    where b.ref = :dest
    into :ispattern;
  if (coalesce(ispattern,0)>0) then
    exception universal'Konto wzorcowe nie może zostać synchronizowane';
  ret = 0;
  update bkyears b
    set b.patternref = :pattern
    where b.ref = :dest;
  ret = 1;
  suspend;
 -- when any
 --   do begin
  --    suspend;
  --  end
end^
SET TERM ; ^
