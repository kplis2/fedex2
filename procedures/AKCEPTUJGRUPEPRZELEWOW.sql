--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AKCEPTUJGRUPEPRZELEWOW(
      GRUPA integer)
   as
declare variable iloscstatusow integer;
declare variable aktuoperat operator_id;
begin
  select count(distinct b.status )
    from btransfers b
    where b.gengroup = :grupa
    into :iloscstatusow;
  if (iloscstatusow > 1) then
    exception universal'Przelewy w rożnych statusach';
  if (exists (select first 1 1
               from btransfers b
               where b.gengroup = :grupa and status > 0)) then
  begin
    exception universal'Conajmniej jeden przelew zaakcetowany';
  end
  execute procedure get_global_param('AKTUOPERATOR')
    returning_values :aktuoperat;
  update btransfers b
    set b.status = b.status + 1, B.operack = :aktuoperat
    where b.gengroup = :grupa;
  suspend;
end^
SET TERM ; ^
