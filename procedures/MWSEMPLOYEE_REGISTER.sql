--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWSEMPLOYEE_REGISTER(
      EMPLOYEE integer,
      MWSORD integer,
      EMPSYMBOL varchar(40) CHARACTER SET UTF8                           ,
      SLOPOZ integer = 0)
  returns (
      STATUS smallint)
   as
declare variable slodef integer;
declare variable symbol varchar(40);
begin
  if (coalesce(slopoz,0) > 0) then
    select s.slownik from slopoz s where s.ref = :slopoz into :slodef;
  --STATUS = 2 - WYREJESTROWANO PRACOWNIKA
  --STATUS = 1 - ZAREJESTROWANO PRACOWNIKA
  if (exists(select p.sref
               from employees e
                 join persons p on(e.person = p.ref)
                 join operator o on(o.ref = p.sref)
               where e.ref = :employee
                 and o.aktywny = 0)) then
    exception universal 'Pracownik nieaktywny. Brak możliwoci zalogowania';
  if (exists(select em.ref
                from mwsordsemployees em
                where em.employee = :employee
                  and em.timestart is not null
                  and em.timestop is null
                  and em.mwsord = :mwsord)) then
  begin
    update mwsordsemployees e set e.timestop = current_timestamp(0) where e.mwsord = :mwsord and e.employee = :employee;
    status = 2;
  end
  else
  begin
    select first 1 mo.symbol
      from mwsordsemployees em
        left join mwsords mo on(mo.ref = em.mwsord)
      where em.employee = :employee
        and em.timestart is not null
        and em.timestop is null
        and em.mwsord <> :mwsord
      into :symbol;
    if (symbol is not null) then
      exception universal 'Pracownik zrejestrowany jest już do pracy przy zleceniu '||coalesce(:symbol,'');
    insert into mwsordsemployees(employee, mwsord, timestart,canceled, slopoz, slodef)
      values (:employee,:mwsord,current_timestamp(0),0,:slopoz,:slodef);
    status = 1;
  end
  suspend;
end^
SET TERM ; ^
