--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_ANAL_PRORDS_REAL_STATUS_CALC(
      PRREGIN varchar(40) CHARACTER SET UTF8                           ,
      NREFIN integer = 0,
      BDATE date = '2010-01-01',
      EDATE date = '2099-12-31')
   as
DECLARE VARIABLE STM VARCHAR(4096);
DECLARE VARIABLE SYMBOL VARCHAR(80);
DECLARE VARIABLE NAZWA VARCHAR(80);
DECLARE VARIABLE NREF INTEGER;
DECLARE VARIABLE PREF INTEGER;
DECLARE VARIABLE VERS INTEGER;
DECLARE VARIABLE ONDATE DATE;
DECLARE VARIABLE REF INTEGER;
DECLARE VARIABLE STATUS SMALLINT;
DECLARE VARIABLE ORDPARENT INTEGER;
DECLARE VARIABLE PARENT INTEGER;
DECLARE VARIABLE QUANTITY NUMERIC(14,4);
DECLARE VARIABLE QUANTITYREAL NUMERIC(14,4);
DECLARE VARIABLE PERCENTREAL NUMERIC(14,4);
DECLARE VARIABLE KTM VARCHAR(40);
DECLARE VARIABLE WERSJA INTEGER;
DECLARE VARIABLE SHEET INTEGER;
DECLARE VARIABLE COL INTEGER;
BEGIN
  DELETE FROM PRORDANALREAL;
  REF = 0;
  IF (NREFIN = 0) THEN
    STM = 'select n.id, n.ref, p.ref, p.wersjaref, coalesce(p.termdost,n.termdost), p.ilosc, p.ilzreal, t.nazwa, p.ktm, p.wersja, p.prsheet
     from nagzam n
       left join pozzam p on (p.zamowienie = n.ref)
       left join towary t on (t.ktm = p.ktm)
       left join typzam z on (z.symbol = n.typzam)
     where position('';''||n.rejestr||'';'' in '''||:PRREGIN||''') > 0 and p.out = 0 and z.wydania = 3
       and coalesce(p.termdost,n.termdost) >= '''||:BDATE||'''
       and coalesce(p.termdost,n.termdost) <= '''||:EDATE||'''
     order by coalesce(p.termdost,n.termdost)';
  ELSE
    STM = 'select n.id, n.ref, p.ref, p.wersjaref, coalesce(p.termdost,n.termdost), p.ilosc, p.ilzreal, t.nazwa, p.ktm, p.wersja, p.prsheet
     from nagzam n
       left join pozzam p on (p.zamowienie = n.ref)
       left join towary t on (t.ktm = p.ktm)
       left join typzam z on (z.symbol = n.typzam)
     where n.ref = '||:NREFIN||' and p.out = 0 and z.wydania = 3
     order by coalesce(p.termdost,n.termdost)';
  FOR
    EXECUTE STATEMENT STM
      INTO SYMBOL, NREF, PREF, VERS, ONDATE, QUANTITY, QUANTITYREAL, NAZWA, KTM, WERSJA, SHEET
  DO BEGIN
    ORDPARENT = NULL;
    SELECT FIRST 1 REF FROM PRORDANALREAL R WHERE R.SYMBOL = :SYMBOL AND NREF = :NREF
      INTO ORDPARENT;
    IF (ORDPARENT IS NULL) THEN
    BEGIN
      REF = REF + 1;
      ORDPARENT = REF;
      INSERT INTO PRORDANALREAL (REF, PARENT, SYMBOL, NREF,COLORINDEX,STATUS)
        VALUES (:REF, NULL, :SYMBOL, :NREF,0,0);
    END
    REF = REF + 1;
    PERCENTREAL = 0;
    IF (QUANTITY > 0) THEN
      PERCENTREAL = COALESCE(QUANTITYREAL,0) / COALESCE(QUANTITY,0);
    ELSE
      PERCENTREAL = 0;
    INSERT INTO PRORDANALREAL (REF, PARENT, SYMBOL, NREF, PREF, VERS, ONDATE, QUANTITY, QUANTITYREAL, PERCENTREAL,COLORINDEX,STATUS,ID)
        VALUES (:REF, :ORDPARENT, :NAZWA, :NREF, :PREF, :VERS, :ONDATE, :QUANTITY, :QUANTITYREAL, 100 * :PERCENTREAL,1,0,:REF);
    EXECUTE PROCEDURE PR_ANAL_PRORDS_REAL_STATUS_DETS(:REF,:NREF,:PREF,:REF) RETURNING_VALUES :REF;
  END
  -- naliczenie statusów dla galezi w drzewie
  -- statusy
  FOR
    SELECT R.REF
      FROM PRORDANALREAL R
      WHERE R.PARENT IS NULL
      INTO REF
  DO BEGIN
    EXECUTE PROCEDURE PR_ANAL_PRORDS_REAL_COSTS(:REF);
  END
--  update prordanalreal r set r.tkw = (select sum(coalesce(rp.tkw,0)) from prordanalreal rp where rp.parent = r.ref) where r.parent is null;
  UPDATE PRORDANALREAL R SET R.WORKCOSTS = NULL WHERE R.WORKCOSTS = 0;
  UPDATE PRORDANALREAL R SET R.MATCOSTS = NULL WHERE R.MATCOSTS = 0;
  UPDATE PRORDANALREAL R SET R.TKW = NULL WHERE R.TKW = 0;
END^
SET TERM ; ^
