--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CENNIK_AKTU_WERSJE(
      DEFCENNIK integer,
      WERSJAREF integer)
   as
declare variable defcenpod integer;
declare variable status integer;
declare variable ileprzel integer;
declare variable iledod integer;
begin
  for select dr.defcennik
        from defcenreg dr join defcennik dc on dr.defcennik = dc.ref
        where dr.cenniknad = :defcennik
        order by dc.kolejnosc, dc.ref
      into :defcenpod
  do begin
    execute procedure CENNIK_AKTUALIZUJ(:defcenpod, :wersjaref, 1, 0) returning_values :status,:ileprzel,:iledod;
  end
end^
SET TERM ; ^
