--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDES_JOIN(
      PRSCHEDGUIDEIN integer,
      PRSCHEDGUIDEOUT integer,
      AMOUNT numeric(14,2))
   as
declare variable prschedguide integer;
declare variable ktm varchar(80);
declare variable amountcurr numeric(14,2);
declare variable nagzamin integer;
declare variable nagzamout integer;
begin
  select ktm, zamowienie from prschedguides where ref = :prschedguidein into :ktm, :nagzamin;
  if(ktm is null) then exception prschedguides_error 'Nieokreślony przewodnik pierwotny';
  for select ref, amount, zamowienie
    from prschedguides
    where status = 0 and ktm = :ktm and ref <> :prschedguidein and
      (:prschedguideout = -1 or ref = :prschedguideout)
    order by ref
    into :prschedguide, :amountcurr, :nagzamout
  do begin
    if(prschedguideout > 0) then begin
      if(:amount is null or :amount <= 0 or :amount > :amountcurr) then
        exception prschedguides_error 'Niedozwolona ilość określona do przeniesienia';
      amountcurr = amount;
    end
    update prschedguides set pggamountcalc = 1, amount = amount + :amountcurr where ref = :prschedguidein;
    update prschedguides set pggamountcalc = 0 where ref = :prschedguidein;
    update nagzam set kilosc = kilosc + :amountcurr where ref = :nagzamin;

    update prschedguides set pggamountcalc = 1, amount = amount - :amountcurr where ref = :prschedguide;
    delete from prschedguides where ref = :prschedguide and amount = 0;
    update prschedguides set pggamountcalc = 0 where ref = :prschedguide;
    update nagzam set kilosc = kilosc - :amountcurr where ref = :nagzamout;
  end
end^
SET TERM ; ^
