--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DECREE_MATCHING_PARAMS(
      DECREE DECREES_ID)
  returns (
      COMPANY COMPANIES_ID,
      BKACCOUNT BKACCOUNTS_ID,
      MATCHINGSLODEF SLO_ID,
      MATCHINGSLOPOZ SLOPOZ_ID,
      MATCHINGLONAZWA STRING,
      MATCHINGSLOKOD STRING40,
      MATCHINGSYMBOL STRING40)
   as
declare variable BSLODEF SLO_ID;
declare variable BSLOPOZ SLOPOZ_ID;
declare variable ASLODEF SLO_ID;
declare variable ISDIST SMALLINT_ID;
declare variable STM MEMO;
declare variable DISTSYMBOL SYMBOLDIST_ID;
declare variable TMPKONTOFK ACCOUNT_ID;
declare variable TMPKODKS ACCOUNT_ID;
begin
  select b.company, a.bkaccount, b.dictdef, b.dictpos, c.matchingslodef, d.matchingsymbol
    from decrees d
      left join bkdocs b on (b.ref = d.bkdoc)
      left join bkperiods p on (p.id = b.period and p.company = b.company)
      left join accounting a on (a.account = d.account and a.company = b.company and a.yearid = p.yearid and a.currency = d.currency)
      left join bkaccounts c on (c.ref = a.bkaccount)
    where d.ref = :decree
    into :company, :bkaccount, :bslodef, :bslopoz, :aslodef, :matchingsymbol;
  -- jak nie ma slownika rozliczeniowego na koncie to wychodzimy
  if (:aslodef is null) then
    exit;
  -- slodefa i slopoza szukamy w nastepujacej kolejnosci
  -- najpierw idziemy po wyroznikach zgodnych z definicja na bkaacount
  select isdist
    from slodef d
    where d.ref = :aslodef
    into isdist;
  if (:isdist > 0) then
  begin
    stm = 'select symbol'||case when :isdist > 1 then cast(:isdist as varchar(10)) else '' end||' from distpos where decree = '||:decree;
    execute statement :stm into distsymbol;
    if (coalesce(:distsymbol,'') <> '') then
    begin
      execute procedure NAME_FROM_BKSYMBOL(:company,:aslodef, :distsymbol)
        returning_values(:matchinglonazwa,:matchingslopoz);
      matchingslokod = :distsymbol;
    end
  end else if (:bslodef = :aslodef) then
  -- potem bierzemy z BKDOCS slodef i slopoz jeżeli sie zgadza.
  begin
    matchingslodef = :bslodef;
    if (:bslopoz is not null) then
    begin
      matchingslopoz = :bslopoz;
      execute procedure slo_daneks(:bslodef, :bslopoz)
        returning_values(:matchingslokod, :matchinglonazwa, :tmpkontofk, :tmpkodks);
    end
  end 
end^
SET TERM ; ^
