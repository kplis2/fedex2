--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_CALC_FINISH as
declare variable guide integer;
declare variable schedzam integer;
begin
  for select distinct PRSCHEDOPERS.guide
    from PRSCHEDOPERS
    where PRSCHEDOPERS.calcdeptimes = 0
    into :guide
  do begin
    execute procedure PRSCHEDGUIDE_CHECKTIMESTARTEND(:guide);
    execute procedure prschedguides_statusmatcheck(:guide);
  end
  for select distinct PRSCHEDOPERS.schedzam
    from PRSCHEDOPERS
    where PRSCHEDOPERS.calcdeptimes = 0
    into :schedzam
  do begin
    execute procedure prschedzam_checktimestartend(:schedzam);
    execute procedure prschedzam_obl(:schedzam);
  end
--waczenie przeliczania samoczynnego statusuów
  update PRSCHEDOPERS set prschedopers.calcdeptimes = 1, prschedopers.lastchangetime = current_time where calcdeptimes = 0;

end^
SET TERM ; ^
