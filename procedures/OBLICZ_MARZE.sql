--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OBLICZ_MARZE(
      DOK_REF integer,
      POZ_REF integer,
      TYP varchar(1) CHARACTER SET UTF8                           ,
      AKCEPT smallint,
      TRYB smallint)
  returns (
      WARTMAG numeric(14,4),
      WARTFAK numeric(14,4),
      MARZA numeric(14,4),
      MARZAPROC numeric(14,4),
      WARTMAGPOZ numeric(14,4),
      WARTFAKPOZ numeric(14,4),
      MARZAPOZ numeric(14,4),
      MARZAPOZPROC numeric(14,4),
      MARZAPOZMIN numeric(14,4))
   as
declare variable MAGROZLICZ smallint;
declare variable PSUMWARTNETZL numeric(14,4);
declare variable DOKUMPOZWART numeric(14,4);
begin
/*Parametry wejsciowe
DOK_REF - ustawiony zawsze, wyswietlamy informacje o marzy dla dokumentu lub pozycji dokumentu
POZ_REF - ustawiony w momencie wyswietlenie informacji o marzy dla pozycji
TYP - typ dokumentu M - dokument magazynowy, F - faktura, Z - zamowienie
AKCEPT - w przypadku dokumentu magazynowego i faktury czy dokument jest zaakceptowany.
Jesli dokument jest zaakceptowany to nie ma sensu nic wyliczac tylko wyswietlamy odpowiednie wartosci
TRYB - dotyczy tylko pozycji, okresla czy wyswietlana pozycja jest w trybie edycji.
Jesli TRYB = 1 (edit lub insert) to wszystkie wartosci danego rekordu wyliczane sa w kodzie.
W przeciwnym przypadku wartosci mozna wyliczyc w procedurze poniewaz nie ulegna zmianie.
*/

  wartmag = 0;
  wartfak = 0;
  marza = 0;
  marzaproc = 0;
  wartmagpoz = 0;
  wartfakpoz = 0;
  marzapoz = 0;
  marzapozproc = 0;
  marzapozmin = 0;

  if(TYP='M') then
  begin
    if(akcept = 1) then
    begin
      select coalesce(dn.wartosc,0), coalesce(dn.wartsnetto,0)
        from dokumnag dn
        where dn.ref = :dok_ref
        into :wartmag, :wartfak;

      if (:poz_ref > 0) then
      begin
        select coalesce(dp.wartosc,0), coalesce(dp.wartsnetto,0), coalesce(w.marzamin,0)
          from dokumpoz dp
            join wersje w on (dp.wersjaref = w.ref)
          where dp.ref = :poz_ref
          into :wartmagpoz, :wartfakpoz, :marzapozmin;
      end
    end
    else begin
      select coalesce(sum(we.cena_zakn*dp.ilosc),0),coalesce(sum(wartsnetto),0)
        from dokumpoz dp
          join wersje we on dp.wersjaref = we.ref
        where dp.dokument = :dok_ref
          and (:tryb = 0 or (:tryb = 1 and dp.ref <> :poz_ref))
        into:wartmag, :wartfak;

      if (:poz_ref > 0 and :tryb = 0) then
      begin
        select coalesce(dp.wartosc,0), coalesce(dp.wartsnetto,0)
          from dokumpoz dp
          where dp.ref = :poz_ref
          into :wartmagpoz, :wartfakpoz;
      end
    end

    wartmag = cast(:wartmag as numeric(14,2));
    marza = wartfak-wartmag;
    if(wartfak <> 0) then marzaproc = cast((1-(:wartmag/:wartfak))*100 as numeric(14,2));
    else marzaproc = 0;

    if (:poz_ref > 0 and :tryb = 0) then
    begin
      wartmagpoz = cast(:wartmagpoz as numeric(14,2));
      marzapoz = wartfakpoz - wartmagpoz;
      if(wartfak <> 0) then marzapozproc = cast((1-(:wartmagpoz/:wartfakpoz))*100 as numeric(14,2));
      else marzapozproc = 0;
    end
    suspend;
  end
  else if(TYP='Z') then
  begin
    select coalesce(sum(w.cena_zakn*pz.ilosc),0), coalesce(sum(pz.wartnetzl),0)
      from pozzam pz
        join wersje w on pz.wersjaref = w.ref
      where pz.zamowienie = :dok_ref
        and (:tryb = 0 or (:tryb = 1 and pz.ref <> :poz_ref))
        and coalesce(pz.havefake,0) = 0
    into :wartmag, :wartfak;

    wartmag = cast(:wartmag as numeric(14,2));
    marza = wartfak-wartmag;
    if(wartfak <> 0) then marzaproc = cast((1-(:wartmag/:wartfak))*100 as numeric(14,2));
    else marzaproc = 0;

    if (:poz_ref > 0 and :tryb = 0) then
    begin
      select coalesce(w.cena_zakn*pz.ilosc,0), coalesce(pz.wartnetzl,0), coalesce(w.marzamin,0)
        from pozzam pz
          join wersje w on pz.wersjaref = w.ref
        where pz.ref = :poz_ref
          and coalesce(pz.havefake,0) = 0
      into :wartmagpoz, :wartfakpoz, :marzapozmin;

      wartmagpoz = cast(:wartmagpoz as numeric(14,2));
      marzapoz = wartfakpoz - wartmagpoz;
      if(wartfakpoz <> 0) then marzapozproc = cast((1-(:wartmagpoz/:wartfakpoz))*100 as numeric(14,2));
      else marzapozproc = 0;
    end

    suspend;
  end
  else if(TYP='F') then
  begin
    select nf.magrozlicz, nf.psumwartnetzl
      from nagfak nf
      where nf.ref = :dok_ref
      into :magrozlicz, :psumwartnetzl;

    if (:magrozlicz = 1) then
    begin
       select max(n.sumwartnetzl - n.psumwartnetzl),sum(case when dn.koryg = 1 then -dn.wartosc else dn.wartosc end)
        from nagfak n
          join dokumnag dn on (n.ref = dn.faktura)
        where n.ref = :dok_ref
        group by n.ref
      into :wartfak, :wartmag;

      if (:poz_ref > 0) then
      begin
        select coalesce(max(w.marzamin),0), max(p.wartnetzl), sum(case when dp.kortopoz>0 then -dp.wartosc else dp.wartosc end)
          from pozfak p
            join dokumpoz dp on (p.ref = dp.frompozfak)
            join wersje w on (p.wersjaref = w.ref)
          where dp.frompozfak = :poz_ref
          group by p.ref
        into :marzapozmin, :wartfakpoz, :wartmagpoz;
      end
    end
    else if (:akcept = 1 and :magrozlicz <> 1) then
    begin
      select n.sumwartnetzl - n.psumwartnetzl, n.kosztkat
        from nagfak n
        where n.ref = :dok_ref
      into :wartfak, :wartmag;

      if (:poz_ref > 0) then
      begin
        select p.wartnetzl,
               case when coalesce(p.kosztzak,0) > 0 then p.kosztzak else coalesce(p.kosztkat,0) end
          from pozfak p
          where p.ref = :poz_ref
        into :wartfakpoz, :wartmagpoz;
      end
    end
    else begin
      select sum(s.wartfak), sum(s.wartmag)
        from (select max(p.wartnetzl - p.pwartnetzl) as wartfak,
                     sum(case when dp.ref is not null then (case when dp.kortopoz>0 then -dp.wartosc else dp.wartosc end)
                           else case when coalesce(p.kosztzak,0) > 0 then p.kosztzak else coalesce(p.kosztkat,0) end
                        end) as wartmag
                from pozfak p
                  left join dokumpoz dp on (p.ref = dp.frompozfak)
                where p.dokument = :dok_ref
                  and (:tryb = 0 or (:tryb = 1 and p.ref <> :poz_ref))
                group by p.ref) as s
      into :wartfak, :wartmag;

      if (:poz_ref > 0 and :tryb = 0) then
      begin
        select max(p.wartnetzl - p.pwartnetzl),
             sum(case when dp.ref is not null then (case when dp.kortopoz>0 then -dp.wartosc else dp.wartosc end)
                   else case when coalesce(p.kosztzak,0) > 0 then p.kosztzak else coalesce(p.kosztkat,0) end
                 end)
        from pozfak p
          left join dokumpoz dp on (p.ref = dp.frompozfak)
        where p.ref = :poz_ref
      into :wartfakpoz, :wartmagpoz;
      end
    end

    wartmag = cast(:wartmag as numeric(14,2));
    marza = wartfak-wartmag;
    if(wartfak <> 0) then marzaproc = cast((1-(:wartmag/:wartfak))*100 as numeric(14,2));
    else marzaproc = 0;

    if (:poz_ref > 0 and :tryb = 0) then
    begin
      wartmagpoz = cast(:wartmagpoz as numeric(14,2));
      marzapoz = wartfakpoz - wartmagpoz;
      if(wartfak <> 0) then marzapozproc = cast((1-(:wartmagpoz/:wartfakpoz))*100 as numeric(14,2));
      else marzapozproc = 0;
    end
    suspend;
  end
end^
SET TERM ; ^
