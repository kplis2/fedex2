--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_9(
      BKDOC integer)
   as
declare variable ACCOUNT ACCOUNT_ID;
declare variable AMOUNT numeric(14,2);
declare variable DESCRIPT varchar(80);
declare variable AMYEAR smallint;
declare variable AMPERIOD smallint;
declare variable AMPR_STATUS smallint;
declare variable AMMONTH smallint;
declare variable TAMORT numeric(14,2);
declare variable SUM_TAMORT numeric(14,2);
declare variable SUM_TAMORTW numeric(14,2);
declare variable FA_SYMBOL varchar(20);
declare variable FA_ODDZIAL varchar(20);
declare variable FA_AMGRP varchar(1);
declare variable FA_TBKSYMBOL BKSYMBOLS_ID;
declare variable ODD varchar(10);
declare variable FABKSYMBOL BKSYMBOLS_ID;
declare variable FXDASSET integer;
declare variable PERIOD varchar(6);
declare variable SUM_SHAREAMOUNT numeric(14,2);
declare variable SHARE numeric(14,2);
declare variable FXDASSETCAACCOUNT ACCOUNT_ID;
declare variable DIST1DDEF integer;
declare variable DIST1SYMBOL SYMBOLDIST_ID;
declare variable DIST2DDEF integer;
declare variable DIST2SYMBOL SYMBOLDIST_ID;
declare variable DIST3DDEF integer;
declare variable DIST3SYMBOL SYMBOLDIST_ID;
declare variable DIST4DDEF integer;
declare variable DIST4SYMBOL SYMBOLDIST_ID;
declare variable DIST5DDEF integer;
declare variable DIST5SYMBOL SYMBOLDIST_ID;
begin
  -- schemat dekretowania - ksiegowanie amortyzacji

  descript = '';
  sum_tamort = 0;
  sum_tamortw = 0;
  sum_shareamount = 0;

  select B.descript, P.yearid, P.ord, p.id
    from bkdocs B
      join bkperiods P on (B.period = P.id)
      left join nagfak N on (B.oref = N.ref)
    where B.ref = :bkdoc
    into :descript, :amyear, :ammonth, :period;

  select status
    from amperiods
    where amyear = :amyear and ammonth = :ammonth
    into :ampr_status;

  if (ampr_status = 2) then exception amperiod_closed;

  for select FA.symbol, FA.oddzial, FA.amgrp, FA.tbksymbol, AM.tamort, AM.amperiod, AG.fabksymbol, O.symbol, FA.ref
    from fxdassets FA
      join amortization AM on (FA.ref = AM.fxdasset and AM.amyear=:amyear and AM.ammonth=:ammonth)
      left join amortgrp AG on (FA.amortgrp = AG.symbol)
      left join oddzialy O on (FA.oddzial = O.oddzial)
    order by FA.symbol
    into :fa_symbol, :fa_oddzial, :fa_amgrp, :fa_tbksymbol, :tamort, :amperiod, :fabksymbol, :odd, :fxdasset
  do begin
    -- ksiegowanie udzialu na inne konto
    sum_shareamount = 0;
    for
      select fca.share, fca.account, fca.dist1ddef, fca.dist1symbol,
          fca.dist2ddef, fca.dist2symbol, fca.dist3ddef, fca.dist3symbol,
          fca.dist4ddef, fca.dist4symbol, fca.dist5ddef, fca.dist5symbol
        from fxdassetca fca
        where fca.fxdasset = :fxdasset
          --and fca.amperiod <= :period
          and coalesce(fca.status,0) = 1
          and fca.typeamortization = 0
          --and fca.amperiod >= (select first 1 f.amperiod from fxdassetca f
          --    where f.fxdasset = fca.fxdasset order by f.amperiod desc)
        into :share, :fxdassetcaaccount, :dist1ddef, :dist1symbol,
          :dist2ddef, :dist2symbol, :dist3ddef, :dist3symbol,
          :dist4ddef, :dist4symbol, :dist5ddef, :dist5symbol
    do
    begin
      amount = (tamort * share) / 100;

      sum_shareamount = sum_shareamount + :amount;
      --if (sum_shareamount > tamort) then
      --  exception FXDASSETCA_ERROR 'Suma udziałów > 100% !';

      execute procedure insert_decree_dists(bkdoc, fxdassetcaaccount, 0, amount, descript,
        dist1ddef, dist1symbol, dist2ddef, dist2symbol, dist3ddef, dist3symbol, dist4ddef,
        dist4symbol, dist5ddef, dist5symbol, 0);

      --execute procedure insert_decree(bkdoc, fxdassetcaaccount, 0, amount, descript, 0);
    end 

    amount = tamort - sum_shareamount;
    if (:amount <> 0) then
    begin
      select first 1 fca.account, fca.dist1ddef, fca.dist1symbol,
          fca.dist2ddef, fca.dist2symbol, fca.dist3ddef, fca.dist3symbol,
          fca.dist4ddef, fca.dist4symbol, fca.dist5ddef, fca.dist5symbol
        from fxdassetca fca
        where fca.fxdasset = :fxdasset
          and coalesce(fca.status,0) = 1
          and fca.typeamortization = 0
        order by fca.share desc
        into :account, :dist1ddef, :dist1symbol,
          :dist2ddef, :dist2symbol, :dist3ddef, :dist3symbol,
          :dist4ddef, :dist4symbol, :dist5ddef, :dist5symbol;
      --account = fa_tbksymbol;
      --execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);
      execute procedure insert_decree_dists(bkdoc, account, 0, amount, descript,
        dist1ddef, dist1symbol, dist2ddef, dist2symbol, dist3ddef, dist3symbol, dist4ddef,
        dist4symbol, dist5ddef, dist5symbol, 0);
    end
    amount = tamort;

    if (fa_amgrp='W') then
    begin
      account = '071-'|| coalesce(fabksymbol,'');
      sum_tamortw = sum_tamortw + tamort;
    end else
    begin
      account = '070-' || coalesce(fabksymbol,'');
      sum_tamort = sum_tamort + tamort;
    end
    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);
  end

  amount = sum_tamort;
  account = '400';
  --execute procedure insert_decree_dists(bkdoc, account, 0, amount, descript, 31584, odd,  null, null, null, null, null ,null ,null, null, 0);
  execute procedure insert_decree_dists(bkdoc, account, 0, amount, descript, null, null,  null, null, null, null, null ,null ,null, null, 0);

  -- oznaczanie dokumentu
  update bkdocs set otable = 'AMPERIODS', oref = :amperiod where ref = :bkdoc;
  -- zaznaczenie okresu amortyzacji jako "zaksiegowany"
  update amperiods set status = 2 where ref = :amperiod;
end^
SET TERM ; ^
