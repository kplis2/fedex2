--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_X_SLOWNIKI_INS(
      TYP type of column INT_IMP_X_SLOWNIKI.TYP,
      DATAPOCZATEK type of column INT_IMP_X_SLOWNIKI.DATAPOCZATEK,
      AKT type of column INT_IMP_X_SLOWNIKI.AKT,
      DATAKONIEC type of column INT_IMP_X_SLOWNIKI.DATAKONIEC,
      WARTOSC type of column INT_IMP_X_SLOWNIKI.WARTOSC,
      IDSES type of column INT_IMP_X_SLOWNIKI.IDSES,
      OTABLE type of column INT_IMP_X_SLOWNIKI.OTABLE,
      OREF type of column INT_IMP_X_SLOWNIKI.OREF,
      SESJA type of column INT_IMP_X_SLOWNIKI.SESJA,
      SKADTABELA type of column INT_IMP_X_SLOWNIKI.SKADTABELA,
      SKADREF type of column INT_IMP_X_SLOWNIKI.SKADREF,
      SLOWNIKID type of column INT_IMP_X_SLOWNIKI.SLOWNIKID,
      TYP_ID type of column INT_IMP_X_SLOWNIKI.TYP_ID,
      GLOWNY type of column INT_IMP_X_SLOWNIKI.GLOWNY)
  returns (
      REF type of column INT_IMP_X_SLOWNIKI.REF)
   as
begin
  insert into INT_IMP_X_SLOWNIKI(typ, datapoczatek, akt, datakoniec, wartosc,  idses, otable, oref, sesja,  skadref, skadtabela, slownikid, typ_id, glowny)
    values (:typ, :datapoczatek, :akt, :datakoniec, :wartosc,  :idses, :otable, :oref, :sesja, :skadref, :skadtabela , :slownikid, :typ_id, :glowny)
    returning ref into :ref;
    suspend;
end^
SET TERM ; ^
