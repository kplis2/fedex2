--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NOTIFY_GET_EMAILWINDYK(
      SLODEF integer,
      SLOPOZ integer)
  returns (
      EMAIL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable cpodmiot integer;
declare variable klient integer;
declare variable e varchar(255);
begin
  -- funkcja okresla adresy email na jakie jest wysylana korespondencja windykacyjna
  email = '';
  cpodmiot = null;
  -- najpierw sprawdzamy czy jest to podmiot CRM
  select ref,coalesce(emailwindyk,'') from cpodmioty where slodef=:slodef and slopoz=:slopoz into :cpodmiot,:email;
  if(:cpodmiot is not null) then begin
    -- dokladamy adresy z osob windykacyjnych
    for select email
      from pkosoby
      where cpodmiot=:cpodmiot and windykacja=1
      and coalesce(email,'')<>''
      into :e
    do begin
      if(position(:e in :email)=0) then begin
        if(:email<>'') then email = :email || ';';
        email = :email || :e;
      end
    end

  end else if(:slodef=1) then begin
    -- jesli nie jest podmiot, to moze jest to klient
    email = '';
    klient = null;
    select ref,coalesce(emailwindyk,'') from klienci where ref=:slopoz into :klient,:email;
    if(:klient is not null) then begin
      -- dokladamy adresy z pracownikow klienta
      for select email
        from uzykli
        where klient=:klient and windykacja=1
        and coalesce(email,'')<>''
        into :e
      do begin
        if(position(:e in :email)=0) then begin
          if(:email<>'') then email = :email || ';';
          email = :email || :e;
        end
      end
    end
  end
  suspend;
end^
SET TERM ; ^
