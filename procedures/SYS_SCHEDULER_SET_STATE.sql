--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_SCHEDULER_SET_STATE(
      INSTANCE_STATE STRING,
      INSTANCE INTEGER_ID,
      ACTIVITY_SCHEDULE SYS_SCHEDULE_ID = null,
      SCHEDULER_SYMBOL SYS_SCHEDULER_SYMBOL_ID = null)
   as
begin
  if(nullif(SCHEDULER_SYMBOL,'') is null) then
  begin
    update sys_schedulehist sh set sh.return_msg = :instance_state
      where sh.ref = :instance
        and (sh.return_msg in ('Created','Working','Stop','Closing','Closed')
          or sh.return_msg is null or sh.return_msg='')  ; /*zabezpieczam sie, zeby nie zamazac faktycznego komunikatu*/
  end
  else
  begin
    update sys_schedulehist sh set sh.return_msg = :instance_state
      where sh.ref = :instance
        and (sh.scheduler_symbol = :scheduler_symbol)
        and (sh.return_msg in ('Created','Working','Stop','Closing','Closed')
          or sh.return_msg is null or sh.return_msg='')  ; /*zabezpieczam sie, zeby nie zamazac faktycznego komunikatu*/
  end
end^
SET TERM ; ^
