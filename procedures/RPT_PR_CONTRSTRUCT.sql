--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PR_CONTRSTRUCT(
      DATA_PODANA date,
      CZYPEL smallint,
      COMPANY integer)
  returns (
      RODZAJ_UMOWY integer,
      OPIS varchar(255) CHARACTER SET UTF8                           ,
      MAN integer,
      WOMAN integer,
      SUMA integer)
   as
begin
  for
    select t.contrtype, t.tname
      from emplcontracts c
        join employees e on (e.ref = c.employee)
        join econtrtypes T on (c.econtrtype = T.contrtype)
      where t.empltype = 1 and e.company = :company
        and c.fromdate <= :data_podana and (c.enddate >= :data_podana or c.enddate is null)
      group by T.contrtype,  T.tname
      into :rodzaj_umowy, :opis
  do begin
    suma = 0;
    man = 0;
    woman = 0;
    select count(distinct P.ref)
       from persons P
         join employees E on (E.person=P.ref)
         join emplcontracts S on (E.ref = S.employee)
       where (S.workdim = 1 or :czypel = 0)
         and S.fromdate <= :data_podana and (S.enddate >= :data_podana or S.enddate is null)
         and S.econtrtype = :rodzaj_umowy
         and e.company = :company
       into :suma;

    select count(distinct P.ref)
       from persons P
         join employees E on (E.person=P.ref)
         join emplcontracts S on (E.ref = S.employee)
       where (S.workdim = 1 or :czypel = 0)
         and S.fromdate <= :data_podana and (S.enddate >= :data_podana or S.enddate is null)
         and S.econtrtype = :rodzaj_umowy and P.sex = 1
         and e.company = :company
       into :man;

    select count(distinct P.ref)
       from persons P
         join employees E on (E.person=P.ref)
         join emplcontracts S on (E.ref = S.employee)
       where (S.workdim = 1 or :czypel = 0)
         and S.fromdate <= :data_podana and (S.enddate >= :data_podana or S.enddate is null)
         and S.econtrtype = :rodzaj_umowy and P.sex = 0
         and e.company = :company
       into :woman;

    suspend;
  end
end^
SET TERM ; ^
