--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDEDOC_INSERT_EDEDOCP(
      EDEDOCH integer,
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      VAL TEXT_BLOB,
      PARAMS varchar(255) CHARACTER SET UTF8                           )
   as
begin
/* uniwerslana metoda dodawania pozycji do importowanego dokumentu elektronicznego
do użycia w drajwerach fizycznych implementujących konkretne formaty*/
  ededoch = coalesce(ededoch,0);
  id = coalesce(id,0);
  parent = coalesce(parent,0);
  name = coalesce(name, '');
  params = coalesce(params, '');
  val = coalesce(val,'');

  if (ededoch > 0) then
    insert into ededocsp (ededoch, id, parent, name, params, val)
      values (:ededoch, :id, :parent, :name, :params, :val);
  else
    exception universal 'Nie wskazany nagłówek dokumentu';
end^
SET TERM ; ^
