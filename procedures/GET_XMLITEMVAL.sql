--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_XMLITEMVAL(
      ITEM varchar(100) CHARACTER SET UTF8                           ,
      XML varchar(8000) CHARACTER SET UTF8                           )
  returns (
      VAL varchar(4000) CHARACTER SET UTF8                           )
   as
  declare variable p1 integer;
  declare variable p2 integer;
  declare variable ibeg varchar(102);
  declare variable iend varchar(103);
begin
/*MWr: Procedura zwraca wartosc elementu ITEM z tesktu o strukturze XML.
       Obsluguje tylko elementy zapisane w tresci jako: <ITEM> VAL </ITEM>  */

  ibeg = '<' || item || '>';
  iend = '</' || item || '>';

  p1 = position(ibeg in xml);
  if (p1 > 0) then
  begin
    p1 = p1 + coalesce(char_length(ibeg),0); -- [DG] XXX ZG119346
    p2 = position(iend in xml);
    if (p2 = 0) then
      exception universal 'Niekomplenty kod XML dla elementu: ' || item;
    else
      val = substring(xml from p1 for (p2 - p1));
  end

  suspend;
end^
SET TERM ; ^
