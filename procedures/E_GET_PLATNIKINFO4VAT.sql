--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_PLATNIKINFO4VAT(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      COMPANY COMPANIES_ID)
  returns (
      K1 STRING255,
      K2 STRING255,
      K3 SMALLINT_ID,
      K4 STRING2,
      K5 YEARS_ID,
      K6 STRING255,
      K8 SMALLINT_ID,
      K9 STRING255,
      USP STRING255)
   as
begin
/*TS: FK - procedura zwraca podstawowe informacje do deklaracji VAT,
  ktore znajduja sie w danych naglowkowych niemal kazdej z nich.*/

  execute procedure e_get_platnikinfo4pit
    returning_values :k1,  -- NIP
      :k8,                 -- 1-podatnik niefizyczny, 2-fizy
      :k9;                 -- pelna nazwa

  -- Numer dokumentu i status jest zwracany dopiero
  -- po pomyslnym przetworzeniu wyslanej e-Deklaracji.
  k2 = null;               -- numer dokumentu
  k3 = null;               -- status dokumentu

  select trim(iif(b.ord<10,'0',''))||b.ord, b.yearid
    from bkperiods b
    where b.id = :period
      and b.company = :company
    into :k4,              -- miesiac w formacie MM
      :k5;                 -- rok w formacie RRRR

 select coalesce(k.cvalue,'')
    from get_config('INFONUSP', 0) k
  into :usp;               -- kod urzedu skarbowego

  select first 1 coalesce(name,'') || ','
      || coalesce(address,'') || ','
      || coalesce(city,'') || ' '
      || postcode
    from einternalrevs
    where code = :usp
  into :k6;                -- nazwa urzedu skarbowego

  suspend;
end^
SET TERM ; ^
