--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CREATE_PROD_FROM_MWSORD(
      DOCID integer,
      DOCGROUP integer,
      DOCPOSID integer,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      MWSORD integer,
      MWSACT integer,
      RECALC smallint,
      MWSORDTYPE integer)
  returns (
      REFMWSORD integer)
   as
declare variable stop smallint;
declare variable mwsactref integer;
declare variable quantity numeric(14,4);
declare variable vers integer;
declare variable good varchar(40);
declare variable prordreg varchar(3);
declare variable prschedguide integer;
declare variable gquantity numeric(14,4);
declare variable operref integer;
declare variable opquantity numeric(14,4);
declare variable prnagzam integer;
declare variable prpozzam integer;
declare variable prdepart varchar(3);
declare variable PRSCHEDGUIDEPOSOUT integer;
declare variable PRSCHEDOPEROUT integer;
declare variable WH wh_id;
declare variable DOCTYPEOUT varchar(3);
declare variable DOCTYPEIN varchar(3);
declare variable prdets integer;
declare variable sql varchar(1024);
declare variable newmwsactsref integer;
declare variable palgroup integer;
declare variable matgood varchar(40);
declare variable matvers integer;
declare variable stopmwsact smallint;
declare variable PRGRUPAROZLIN integer;
declare variable PRGRUPAROZLOUT integer;
declare variable doccount integer;
declare variable prdocid dokumnag_id;
declare variable prdocposid dokumpoz_id;
declare variable aktuoperator integer;
begin
  stop = 0;
  select a.palgroup, o.operator
    from mwsacts a
      left join mwsords o on (o.ref = a.mwsord)
    where a.ref = :mwsact
    into palgroup, aktuoperator;
  execute procedure get_config('PRREJZAMPROD',2) returning_values prordreg;
  if (exists (select first 1 1 from rdb$procedures p where p.rdb$procedure_name = 'XK_MWS_CREATE_PROD_FROM_MWSORD')
  )then
  begin
    sql = 'select status, msg from XK_MWS_CREATE_PROD_FROM_MWSORD('||:DOCID||','||DOCGROUP||','||DOCPOSID||','''||DOCTYPE||''','||MWSORD||','||RECALC||','||MWSORDTYPE||') ';
    execute statement :sql into :REFMWSORD;
    exit;
  end
  execute procedure PRGRUPAROZL_ID returning_values :PRGRUPAROZLIN;
  execute procedure PRGRUPAROZL_ID returning_values :PRGRUPAROZLOUT;
  while (stop = 0)
  do begin
    mwsactref = null;
    select first 1 ref, good, vers, quantity
      from mwsacts 
      where mwsord = :mwsord and status = 0 and palgroup = :palgroup
      into mwsactref, good, vers, quantity;
    if (mwsactref is not null) then
    begin
      stopmwsact = 0;
      for
        select g.ref, g.amount - g.amountzreal, g.zamowienie, g.pozzam,g.prdepart
          from nagzam n
            left join pozzam p on (p.zamowienie = n.ref)
            left join prschedguides g on (g.pozzam = p.ref)
          where n.rejestr = :prordreg and p.wersjaref = :vers
            and p.ilzreal < p.ilosc and g.amountzreal < g.amount
            and exists(select first 1 1 from prschedguidespos gp where gp.prschedguide = g.ref)
          order by n.datawe
          into prschedguide, gquantity, :prnagzam, :prpozzam, :prdepart
      do begin
        if(:quantity < :gquantity) then gquantity = :quantity;
        for
          select PRSCHEDGUIDEPOSOUT, PRSCHEDOPEROUT,WH, DOCTYPEOUT, DOCTYPEIN, QUANTITY, KTM, WERSJAREF
            from XK_PRSCHEDGUIDEPOS_MATERIALS (:prdepart,:prnagzam,:prpozzam,
              :prschedguide,null,null,null,:gquantity)
            where quantityp> 0 order by ref
            into :PRSCHEDGUIDEPOSOUT, :PRSCHEDOPEROUT, :WH, :DOCTYPEOUT, :DOCTYPEIN, :OPQUANTITY, :matgood, :matvers
        do begin
          execute procedure PRSCHEDGUIDEDETS_ADD(:prnagzam,:prpozzam,
              :prschedguide, :PRSCHEDGUIDEPOSOUT, :PRSCHEDOPEROUT,null,:matgood,
              :matvers, 0, :wh,:doctypeout, :opquantity, 1,0,0,0,0,'DOKUMNAG',null,null)
            returning_values :prdets;
          execute procedure PRGRUPAROZL_SET(:prdets, :prgruparozlout);
        end
        for
          select PRSCHEDGUIDEPOSOUT, PRSCHEDOPEROUT,WH, DOCTYPEIN, QUANTITY
            from XK_PRSCHEDGUIDE_PRODUCTS (:prdepart,:prnagzam, :prpozzam,
              :prschedguide,null, null)
            where quantityp> 0 order by ref
            into :PRSCHEDGUIDEPOSOUT, :PRSCHEDOPEROUT, :WH, :DOCTYPEIN, :OPQUANTITY
        do begin
          if(:quantity < :OPQUANTITY) then opquantity = :quantity;
          execute procedure PRSCHEDGUIDEDETS_ADD(:prnagzam,:prpozzam,
              :prschedguide, :PRSCHEDGUIDEPOSOUT, :PRSCHEDOPEROUT,null,:good,
              :vers, 0, :wh,:doctypein, :opquantity, 0,0,0,0,0,'DOKUMNAG',null,null)
            returning_values :prdets;
          update PRSCHEDGUIDEDETS set frommwsact = :mwsactref, frommwsord = :mwsord where ref = :prdets;
          execute procedure PRGRUPAROZL_SET(:prdets, :prgruparozlin);
          if(:quantity > :gquantity) then
          begin
            update mwsacts ma set ma.quantity = :gquantity where ma.ref = :mwsactref;
            execute procedure gen_ref('MWSACTS') returning_values :newmwsactsref;
            insert into mwsacts (ref, mwsord, status, stocktaking, good, vers,
                quantity, mwsconstlocp, mwspallocp, mwsconstlocl, mwspallocl,
                doctype, settlmode, closepalloc, wh, regtime, timestartdecl,
                timestopdecl,  lot, PALGROUP, ISPAL, MIXEDPALLGROUP, RECDOC,
                docid, h, l, wh2, mwspallocsize, segtype)
              select :newmwsactsref, mwsord, status, stocktaking, good, vers,
                  :quantity-:gquantity, mwsconstlocp, mwspallocp, mwsconstlocl,
                  mwspallocl, null, settlmode, closepalloc, wh, regtime, timestartdecl,
                  timestopdecl,  lot, PALGROUP, ISPAL, MIXEDPALLGROUP, RECDOC,
                  null, h, l, wh2, mwspallocsize, segtype
                from mwsacts
                where ref = :mwsactref;
          end
          update mwsacts ma set ma.status = 2, ma.quantityc = ma.quantity where ma.ref = :mwsactref;
          quantity = :quantity - :gquantity;
          mwsactref = :newmwsactsref;
          if (quantity = 0) then
          begin
            stopmwsact = 1;
            break;
          end
        end
        if (stopmwsact = 1) then
          break;
      end
      if (quantity > 0) then exception GOODSNOTENOUGH 'Nie można zrealizować przyjęcia. Brak przewodników produkcyjnych dla'|| :good;
    end else
      stop = 1;
  end
  update or insert into GLOBALPARAMS (connectionid, psymbol, pvalue)
    values (current_connection, 'AKTUOPERATOR',:aktuoperator)
    matching (connectionid, psymbol);
  execute procedure PR_GEN_DOC_PRGRUPAROZL(:prgruparozlin,current_date,0,1,0,null) returning_values :doccount;
  execute procedure PR_GEN_DOC_PRGRUPAROZL(:prgruparozlout,current_date,0,1,1,null) returning_values :doccount;
  for
    select dp.dokument, dp.ref, a.ref
      from PRSCHEDGUIDEDETS d
        left join mwsacts a on (d.frommwsact = a.ref)
        left join dokumpoz dp on (d.sref = dp.ref and d.ssource = 0)
      where d.frommwsord = :mwsord and a.docid = 0 and a.docposid = 0
      into :prdocid, :prdocposid, :mwsactref
  do begin
    update mwsacts a set a.docid = :prdocid, a.docposid = :prdocposid, a.doctype = 'M'
      where a.ref = :mwsactref;
  end
end^
SET TERM ; ^
