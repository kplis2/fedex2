--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PM_ANALIZA_KOSZTOW(
      PMPLAN_REF integer)
  returns (
      OKRES varchar(7) CHARACTER SET UTF8                           ,
      SUMPRZYCHPLANRET numeric(14,2),
      SUMKOSZTPLANRET numeric(14,2),
      SUMPRZYCHRZECZYW numeric(14,2),
      SUMKOSZTRZECZYW numeric(14,2),
      ROZNICAPRZYCH numeric(14,2),
      ROZNICAKOSZT numeric(14,2))
   as
declare variable datastart date;
declare variable dataend date;
declare variable datapokres date;
declare variable datakokres date;
declare variable przychplan numeric(14,6);
declare variable kosztplan numeric(14,6);
declare variable startelem date;
declare variable endelem date;
declare variable przychday numeric(14,6);
declare variable kosztday numeric(14,6);
declare variable ref integer;
declare variable sumprzychplan numeric(14,6);
declare variable sumkosztplan numeric(14,6);
declare variable przychrzeczyw numeric(14,6);
declare variable kosztrzeczyw numeric(14,6);
declare variable datastartpom date;
declare variable dataendpom date;
begin
roznicaprzych = 0;
roznicakoszt = 0;
  select plstartdate, plenddate from pmplans where ref = :pmplan_ref into :datastart, :dataend;
  select min(plstartdate), max(plenddate) from pmelements pe where pe.pmplan = :pmplan_ref into :datastartpom,  :dataendpom;
  if (datastartpom < datastart) then datastart = datastartpom; --szukamy czy istnieje element wykraczajacy datami poza daty projektu
  if (dataendpom > dataend) then dataend = dataendpom;
  for select ref from pmpositions where pmplan = :pmplan_ref into :ref
    do begin   --szukamy czy istnieje dokument kosztowy wykraczajacy datami poza daty projektu
      select min(data), max(data) from pm_rozliczenie_poz(:ref, null,null) into :datastartpom, :dataendpom;
      if (datastartpom < datastart) then
        datastart = datastartpom;
      if (dataendpom > dataend) then
        dataend = dataendpom;
    end
  for select ref from pmelements where pmplan = :pmplan_ref into :ref
    do begin  --szukamy czy istnieje dokument przychodowy wykraczajacy datami poza daty projektu
      select min(data), max(data) from pm_rozliczenie_elem (:ref, null,null) into :datastartpom, :dataendpom;
      if (datastartpom < datastart) then
        datastart = datastartpom;
      if (dataendpom > dataend) then
        dataend = dataendpom;
    end
  ref = null;
  datakokres = :datastart - 1;
  datapokres = :datastart;
  while (datapokres < dataend) do
  begin
    if (cast(extract(month from datapokres) as integer) < 12) then
      datakokres = extract(year from datapokres) ||'-'|| cast(cast(extract(month from datapokres) as integer)+1 as varchar(2)) ||'-01' ;
    else
      datakokres = cast(cast(extract(year from datapokres) as integer) +1 as varchar(5)) ||'-01-01' ;
    datakokres = datakokres - 1;
    okres = substring(datapokres from 1 for 4) || substring(datapokres from 6 for 7);
    sumprzychplan = 0;
    sumkosztplan  = 0;
    sumkosztrzeczyw = 0;
    sumprzychrzeczyw = 0;
    for select pe.ref, pe.calcsumval, pe.budsumval, coalesce(pe.plstartdate,:datastart), coalesce(pe.plenddate,:dataend)
      from pmelements pe
      where pe.pmplan = :pmplan_ref
      into :ref, :przychplan, :kosztplan, :startelem, :endelem
    do begin

      if (  (:datapokres >= :startelem and :datapokres <= :endelem)
         or (:datakokres >= :startelem and :datakokres <= :endelem)
         or (:datapokres <= :startelem and :datakokres >= :endelem) --czy okres zycia elementu zawiera sie w 1 okresie
         ) then
      begin
        if (endelem - startelem + 1 > 0) then
        begin
          przychday = przychplan / (endelem - startelem + 1);
          kosztday  = kosztplan  / (endelem - startelem + 1);
        end
        else
        begin
          przychday = 0;
          kosztday  = 0;
        end

        if (datapokres > startelem) then  startelem = datapokres;
        if (datakokres < endelem) then  endelem = datakokres;

        sumprzychplan = sumprzychplan + przychday * (endelem - startelem + 1);
        sumkosztplan  = sumkosztplan  + kosztday  * (endelem - startelem + 1);
      end
      select sum(kwota) from PM_ROZLICZENIE_POZFROMELEM(:ref, :datapokres, :datakokres) into :kosztrzeczyw;
      sumkosztrzeczyw = sumkosztrzeczyw + coalesce(kosztrzeczyw,0);
      select sum(kwota) from PM_ROZLICZENIE_ELEM(:ref, :datapokres, :datakokres) into :przychrzeczyw;
      sumprzychrzeczyw = sumprzychrzeczyw + coalesce(przychrzeczyw,0);
    end
    sumprzychplanret = sumprzychplan;
    sumkosztplanret  = sumkosztplan;

    roznicaprzych = roznicaprzych + sumprzychplanret - sumprzychrzeczyw;
    roznicakoszt = roznicakoszt + sumkosztplanret - sumkosztrzeczyw;
    suspend;
    datapokres = datakokres + 1;
  end
end^
SET TERM ; ^
