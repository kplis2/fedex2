--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZFAK_GET_FIRSTPOZ(
      KOREKTA integer)
  returns (
      POZFAK integer)
   as
declare variable refk integer;
begin
  pozfak = null;
  while(:korekta is not null) do begin
    pozfak = korekta;
    korekta = null;
    select REFK from POZFAK where REF=:pozfak into :korekta;
  end
  suspend;
end^
SET TERM ; ^
