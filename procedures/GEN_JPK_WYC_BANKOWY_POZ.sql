--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GEN_JPK_WYC_BANKOWY_POZ(
      ID INTEGER_ID)
  returns (
      DATA_OPERACJI TIMESTAMP_ID,
      KWOTA_OPERACJI MONEY,
      NAZWA_PODMIOTU STRING,
      NUMER_WIERSZA STRING,
      OPIS_OPERACJI STRING,
      SALDO_OPERACJI MONEY,
      TYP STRING)
   as
begin
  /* Procedure Text */

  data_operacji = current_date;
  kwota_operacji = 1.5;
  nazwa_podmiotu = 'test1';
  numer_wiersza = 1;
  opis_operacji = 'opis operacji 1 id: '||id;
  saldo_operacji = 100;
  typ = 1;
  suspend;

  data_operacji = current_date;
  kwota_operacji = 2.5;
  nazwa_podmiotu = 'test2';
  numer_wiersza = 2;
  opis_operacji = 'opis operacji 2'||:id;
  saldo_operacji = 200;
  typ = 2;
  suspend;
end^
SET TERM ; ^
