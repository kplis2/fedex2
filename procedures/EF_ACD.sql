--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_ACD(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      COL integer)
  returns (
      AMOUNT numeric(14,2))
   as
declare variable FROMDATE date;
declare variable TODATE date;
declare variable ESYSTEM_START date;
begin
--DU: Personel - do liczenia dni kalendarzowych nieobecnosci

  execute procedure ef_adh(:employee,:payroll, :prefix, 'CD', :col, 0)
    returning_values :amount;

  if (amount is null) then amount = 0;

  suspend;
end^
SET TERM ; ^
