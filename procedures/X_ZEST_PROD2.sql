--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_ZEST_PROD2(
      MAGAZ char(3) CHARACTER SET UTF8                           )
  returns (
      DYW varchar(20) CHARACTER SET UTF8                           ,
      WYKON numeric(14,4))
   as
begin


   for select substring(dp.ktm from 2 for 3), sum(dp.ilosc)
     from dokumnag dn join dokumpoz dp on (dn.ref = dp.dokument)
     where dn.typ = 'PWP' and dn.akcept = 1 and dn.magazyn = :magaz
     group by substring(dp.ktm from 2 for 3)

   into :dyw,  :wykon
   do begin
     suspend;

   end
end^
SET TERM ; ^
