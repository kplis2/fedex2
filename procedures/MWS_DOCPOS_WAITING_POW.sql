--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_DOCPOS_WAITING_POW(
      WH varchar(3) CHARACTER SET UTF8                           ,
      DOCPOSID integer,
      VERS integer)
   as
declare variable quantitywh numeric(14,4);
declare variable quantityrefill numeric(14,4);
declare variable ordered numeric(14,4);
declare variable quantityp numeric(14,4);
declare variable quantityexcl numeric(14,4);
declare variable quantityincl numeric(14,4);
declare variable quantityprepare numeric(14,4);
declare variable realdate date;
declare variable blockpos smallint;
declare variable preparegoods smallint;
declare variable takefromzeros varchar(100);
declare variable takefromzero smallint;
declare variable quantitywhzero numeric(14,4);
declare variable buffor smallint;
declare variable buffors varchar(100);
declare variable docgroup integer;
declare variable quantitytoprepare numeric(14,4);
declare variable quantityprepared numeric(14,4);
declare variable quantitypreparedc numeric(14,4);
declare variable lot integer;
declare variable indyviduallot smallint;
declare variable magbreak smallint;
declare variable realfaster smallint;
declare variable x_partia string20;
declare variable x_serial_no integer_id;
declare variable partiapoborowa date_id;
declare variable partiabuforowa date_id;
declare variable np string3;
begin
  -- wyliczamy ile jest w przygotowaniu juz w sektorach wstepnej konfekcji
  quantityprepare = 0;
  select g.grupasped, coalesce(p.dostawa,0), t.magbreak, coalesce(g.realfaster,0),
      p.x_partia, p.x_serial_no, g.typ
    from dokumpoz p
      left join dokumnag g on (g.ref = p.dokument)
      left join towary t on (t.ktm = p.ktm)
    where p.ref = :docposid
    into docgroup, lot, magbreak, realfaster,
      :x_partia, :x_serial_no, :np;
  if(:lot > 0) then
  begin
    select d.indywidualdost
      from dostawy d
      where d.ref = :lot
      into :indyviduallot;
  end
  -- nie wiem co to za tabela docpos2prepare (chyba ta wstepna konfekcja) ale w Abakusie jest pusta,
  -- i nie jest insestowana przez żadna procedur ani trigger (może w źródlach ale nie spradzalem ?)
  select sum(p.quantity), sum(p.quantity - p.tquantity)
    from docpos2prepare p
    where p.docposid = :docposid
    into quantityprepare, quantitytoprepare;
  if (quantityprepare is null) then quantityprepare = 0;
  if (quantitytoprepare is null) then quantitytoprepare = 0;
  select preparegoods from mwsgoodsrefill where docposid = :docposid
    into preparegoods;
  if (preparegoods is null) then
  begin
    preparegoods = 1;
    select d.blokada, cast(d.termdost as date)
      from dokumpoz p
        left join dokumnag d on (p.dokument = d.ref)
      where p.ref = :docposid
      into blockpos, realdate;
    if (blockpos = 1 or blockpos = 5) then
      preparegoods = 0;
    else if (realdate > current_date and realfaster = 0) then
      preparegoods = 0;
  end
  if (preparegoods is null) then preparegoods = 1;
  -- czy wolno brac towar z lokacji niedostepnych dla pickerow "0"
  execute procedure get_config('MWSTAKEFROMBUFFOR',2) returning_values buffors;
  if (buffors is null or buffors = '') then
    buffor = 0;
  else
    buffor = cast(buffors as smallint);

  execute procedure get_config('MWSTAKEFROMZERO',2) returning_values takefromzeros;
  if (takefromzeros is null or takefromzeros = '') then
    takefromzero = 0;
  else
    takefromzero = cast(takefromzeros as smallint);

  if (coalesce(x_partia,'') = '') then x_partia = null;
  if (coalesce(x_serial_no,0)=0) then x_serial_no = null;
  if (coalesce(np,'')='') then  np = null; --<-- typ dokumentu, NP tylko z lokacji marketing
  -- iloci na lokacjach (c.locdest in (1,2,3,9,5) to typ lokacji 1= Epal (W abakusie tylka taka jest z tej listy),  2 - polka kweta, 3 - nie wiem, 9 - 5 to jakie DYN)
  select sum (m.quantity - m.blocked + m.ordered)
    from mwsstock m
      left join mwsconstlocs c on (c.ref = m.mwsconstloc and c.act > 0)
      left join whareas w on (w.ref = c.wharea)
      left join wersje v on (v.ref = m.vers)
    where m.wh = :wh and m.vers = :vers
      and c.act > 0 and c.locdest in (1,2,3,9,5)
      --and (m.lot = :lot or :lot = 0)
      and ( (:indyviduallot = 1 and m.lot = :lot and c.indyviduallot = 1)
        or (:indyviduallot = 0 and m.lot = :lot and c.indyviduallot = 0)
        or (:lot = 0 and c.indyviduallot = 0)
        or (:lot > 0 and :magbreak < 2)
        or (:lot > 0 and :magbreak = 2 and m.lot = :lot) )
      and (:x_partia is null or m.x_partia = :x_partia)
      and (:x_serial_no is null or m.x_serial_no = :x_serial_no)
      and (:np <> 'NP' or c.ref = 13424)
      and ((m.goodsav in (1,2) and :buffor = 0) or :buffor = 1 or :np = 'NP')
      and (m.mixedpalgroup = 0 or v.mwscalcmix = 1 or c.takefrommix = 1)
      and (m.x_blocked<>1) --XXX Ldz ZG 126909
  into quantitywh;
  if (quantitywh is null) then quantitywh = 0;



  -- iloci z lokacji "0" (m.goodsav = 4)?  na dzień 20161222 nie ma takich  W Abakusie
  if (takefromzero > 0) then
  begin
    select sum (m.quantity - m.blocked + m.ordered)
    from mwsstock m
      left join mwsconstlocs c on (c.ref = m.mwsconstloc)
      left join wersje v on (v.ref = m.vers)
    where m.wh = :wh and c.locdest = 0
      and m.vers = :vers
      and ( (:indyviduallot = 1 and m.lot = :lot and c.indyviduallot = 1)
        or (:indyviduallot = 0 and m.lot = :lot and c.indyviduallot = 0)
        or (:lot = 0 and c.indyviduallot = 0)
        or (:lot > 0 and :magbreak < 2)
        or (:lot > 0 and :magbreak = 2 and m.lot = :lot) )
      and (:x_partia is null or m.x_partia = :x_partia)
      and (:x_serial_no is null or m.x_serial_no = :x_serial_no)
      and (:np <> 'NP' or c.ref = 13424)
      and m.goodsav = 4 and m.actloc > 0
      and (m.mixedpalgroup = 0 or v.mwscalcmix = 1 or c.takefrommix = 1)
      and (m.x_blocked<>1) --XXX Ldz ZG 126909
    into quantitywhzero;
    if (quantitywhzero is null) then quantitywhzero = 0;
    quantitywh = quantitywh + quantitywhzero;
  end

  -- XXX KBI ZG126343 bezwgledne fifo dla towarów wg partii
  if (quantitywh > 0 and :buffor = 0 and exists(select first 1 1 from wersje w where w.ref = :vers and w.x_mws_partie = 1)) then begin
    -- sprawdzamy jaka najstarsza data jest na lokacjach poborowych
    select first 1 m.x_partia
      from mwsstock m
        left join mwsconstlocs c on (c.ref = m.mwsconstloc and c.act > 0)
        left join whareas w on (w.ref = c.wharea)
        left join wersje v on (v.ref = m.vers)
      where m.wh = :wh and m.vers = :vers
        and c.act > 0 and c.locdest in (1,2,3,9,5)
        --and (m.lot = :lot or :lot = 0)
        and ( (:indyviduallot = 1 and m.lot = :lot and c.indyviduallot = 1)
          or (:indyviduallot = 0 and m.lot = :lot and c.indyviduallot = 0)
          or (:lot = 0 and c.indyviduallot = 0)
          or (:lot > 0 and :magbreak < 2)
          or (:lot > 0 and :magbreak = 2 and m.lot = :lot) )
        and (:x_partia is null or m.x_partia = :x_partia)
        and (:x_serial_no is null or m.x_serial_no = :x_serial_no)
        and (:np <> 'NP' or c.ref = 13424)
        and (m.goodsav in (1,2))
        and (m.mixedpalgroup = 0 or v.mwscalcmix = 1 or c.takefrommix = 1)
        and (m.x_blocked<>1) --XXX Ldz ZG 126909
      order by m.x_partia
    into :partiapoborowa;

    -- sprawdzamy jaka najstarsza data jest na lokacjach poborowych
    select first 1 m.x_partia
      from mwsstock m
        left join mwsconstlocs c on (c.ref = m.mwsconstloc and c.act > 0)
        left join whareas w on (w.ref = c.wharea)
        left join wersje v on (v.ref = m.vers)
      where m.wh = :wh and m.vers = :vers
        and c.act > 0 and c.locdest in (1,2,3,9,5)
        --and (m.lot = :lot or :lot = 0)
        and ( (:indyviduallot = 1 and m.lot = :lot and c.indyviduallot = 1)
          or (:indyviduallot = 0 and m.lot = :lot and c.indyviduallot = 0)
          or (:lot = 0 and c.indyviduallot = 0)
          or (:lot > 0 and :magbreak < 2)
          or (:lot > 0 and :magbreak = 2 and m.lot = :lot) )
        and (:x_partia is null or m.x_partia = :x_partia)
        and (:x_serial_no is null or m.x_serial_no = :x_serial_no)
        and (:np  <> 'NP' or c.ref = 13424)
        and (m.goodsav = 0)
        and (m.mixedpalgroup = 0 or v.mwscalcmix = 1 or c.takefrommix = 1)
        and (m.x_blocked<>1) --XXX Ldz ZG 126909
      into :partiabuforowa;
      if (partiabuforowa is not null and partiabuforowa < partiapoborowa) then
        quantitywh = 0;
  end
  -- XXX Koniec KBI ZG126343 bezwgledne fifo dla towarów wg partii

  -- tabela  docpos2prepare  w Abakusie jest pusta i nie insertowana przez żadna inna procedur ani trigger wiec  nie wiem o co chodzi (chyba jakas wstpna zbiórka)
  select sum(coalesce(p.dquantity,0))
    from docpos2prepare p
    where p.docgroup = :docgroup and p.vers = :vers
    into quantitypreparedc;
  if (quantitypreparedc > 0) then
  begin
    quantityprepared = 0;
    select sum(m.quantity - m.blocked)
      from mwsconstlocs c
        left join whsecs s on (s.ref = c.whsec)
        left join mwsstock m on (m.mwsconstloc = c.ref)
        left join mwspallocs p on (p.ref = m.mwspalloc)
      where c.locdest = 9 and c.wh = :wh
        and ( (:indyviduallot = 1 and m.lot = :lot and c.indyviduallot = 1)
          or (:indyviduallot = 0 and m.lot = :lot and c.indyviduallot = 0)
          or (:lot = 0 and c.indyviduallot = 0)
          or (:lot > 0 and :magbreak < 2)
          or (:lot > 0 and :magbreak = 2 and m.lot = :lot) )
        and m.vers = :vers and (p.docgroup = :docgroup or p.docgroup is null)
      and (m.x_blocked<>1) --XXX Ldz ZG 126909
      and (:np  <> 'NP' or c.ref = 13424)
      into quantityprepared;
    if (quantityprepared is null) then quantityprepared = 0;
  end
  if (quantityprepared is null) then quantityprepared = 0;
  quantitywh = quantitywh + quantityprepared;

  -- odejmujemy iloci z zaakceptowanego lub pobranego zlecenia MG z aktywnych lokacji buforowych (goodsav = 1) i lokacji nie wiem jakich bo nie ma takich w abakusie  (goodsav = 2)
  select sum(a.quantity)
    from mwsacts a
      left join mwsords o on (o.ref = a.mwsord)
      left join mwsconstlocs c on (a.mwsconstlocl = c.ref)
      left join mwsordtypes t on (t.ref = o.mwsordtype)
    where a.status in (1,2) and t.mwsordwaitingcalc = -1
      and c.goodsav in (1,2) and c.act > 0
      and (a.lot = :lot or :lot = 0)
      and c.goodssellav = 1 and a.vers = :vers and o.wh = :wh
      and (:x_partia is null or a.x_partia = :x_partia)
      and (:x_serial_no is null or a.x_serial_no = :x_serial_no)
      and (:np  <> 'NP' or c.ref = 13424)
    into quantityexcl;
  if (quantityexcl is null) then quantityexcl = 0;
  quantitywh = quantitywh - quantityexcl;

  --dodajemy ilosci z zaakceptowanego lub pobranego do realizacji WG (pozycje WG moga już być zrealizowane)
  select sum(a.quantity)
    from mwsacts a
      join mwsords o on (o.ref = a.mwsord)
      left join mwsordtypes t on (t.ref = o.mwsordtype)
    where o.status < 3 and o.status > 0 and t.mwsordwaitingcalc = 1 and a.status > 0 and a.status < 6
      and (a.lot = :lot or :lot = 0)
      and a.vers = :vers and o.wh = :wh
      and (:x_partia is null or a.x_partia = :x_partia)
      and (:x_serial_no is null or a.x_serial_no = :x_serial_no)
    into quantityincl;
  if (quantityincl is null) then quantityincl = 0;
  quantitywh = quantitywh + quantityincl;
  if (quantitywh < 0) then quantitywh = 0;

  select sum(quantity)
    from mwsgoodsrefill
    where vers = :vers and preparegoods = 1 and docposid <> :docposid
      and (lot = :lot or :lot = 0)
      and (:x_partia is null or x_partia = :x_partia)
      and (:x_serial_no is null or x_serial_no = :x_serial_no)
    into quantityrefill;
  if (quantityrefill is null) then quantityrefill = 0;

  -- sprawdzamy na dokumpozie jaka ilosc musimy jeszcze wygenerowac
  select case when (d.mwsdisposition = 1 or coalesce(t1.altposmode,0) = 1) then p.ilosc else p.iloscl end - p.ilosconmwsacts
    from dokumpoz p
      join dokumnag d on (p.dokument = d.ref)
      join towary t on (p.ktm = t.ktm)
      left join dokumpoz p1 on (p1.ref = p.alttopoz and p.fake = 1)
      left join towary t1 on (t1.ktm = p1.ktm and t1.usluga <> 1)
    where p.ref = :docposid
      and p.genmwsordsafter = 1
      and t.usluga <> 1
     -- and p.iloscl - p.ilosconmwsacts > 0
    into quantityp;
  if (quantityp is null) then quantityp = 0;
  quantitywh = quantitywh - quantityrefill;

  -- obliczamy jaką iloc potrzebujemy (quantitytoprepare to ilosc ze stpnej konfekcji chyba nie uzywanej w Abakusie)
  if (quantitywh >= quantityp - quantitytoprepare) then
    ordered = quantityp - quantitytoprepare;
  else
    ordered = quantitywh;

  if (ordered < 0) then ordered = 0;

  -- Wasciwe naliczenie tabeli oczekujących (zainsertowanie pozycji jeloi niema lub update jeli jest).
  -- (quantitytoprepare to ilosc ze stpnej konfekcji chyba nie uzywanej w Abakusie)
  -- preparegoods - wczeniej wyliczylimy czy obrabiac dana pozycj (czy nie jst zanaczona na dokumencie blokada generowani WMS lub dokument jest na późniejsza dat realizacji)
  if (not exists (select first 1 ref from mwsgoodsrefill where docposid = :docposid)) then
  begin
    insert into mwsgoodsrefill (tried, wh, doctype, docid, docposid, vers, quantity, posquantity, posquantityorg, status, ordered,lot,
            x_partia, x_serial_no, x_slownik)
      select 0, :wh, 'M', p.dokument, p.ref, p.wersjaref, :quantityp - :quantitytoprepare, :quantityp,
          case when (d.mwsdisposition = 1 or coalesce(t1.altposmode,0) = 1) then p.ilosc else p.iloscl end, 0,
          case when :preparegoods = 1 then :ordered else 0 end,coalesce(p.dostawa,0),
          p.x_partia, p.x_serial_no, p.x_slownik
        from dokumpoz p
          join dokumnag d on (p.dokument = d.ref)
          join towary t on (p.ktm = t.ktm)
          left join dokumpoz p1 on (p1.ref = p.alttopoz and p.fake = 1)
          left join towary t1 on (t1.ktm = p1.ktm and t1.usluga <> 1)
        where p.ref = :docposid
          and p.genmwsordsafter = 1
          and t.usluga <> 1;
          --and iloscl - ilosconmwsacts > 0;
  end
  else
    update mwsgoodsrefill set tried = 0, lot = :lot, quantity = :quantityp - :quantitytoprepare,
        ordered = case when :preparegoods = 1 then :ordered else 0 end where docposid = :docposid;

  -- oznaczenie na dokumnagu że dokument zosta naliczony w tabeli oczekujących (z tego wychodzi że si zaznaczy zaraz po przeleceniu pierwszej pozycji)
  --update dokumpoz set genmwsordsafter = 0 where ref = :docposid and genmwsordsafter <> 0;
end^
SET TERM ; ^
