--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PIT11_V16(
      CURRENTCOMPANY integer,
      YEARID char(4) CHARACTER SET UTF8                           ,
      EMPLOYEE integer,
      TYP smallint,
      DATA date)
  returns (
      P36 numeric(14,2),
      P37 numeric(14,2),
      P38 numeric(14,2),
      P39 numeric(14,2),
      P54 numeric(14,2),
      P55 numeric(14,2),
      P56 numeric(14,2),
      P57 numeric(14,2),
      P62 numeric(14,2),
      P63 numeric(14,2),
      P64 numeric(14,2),
      P65 numeric(14,2),
      P70 numeric(14,2),
      P71 numeric(14,2),
      P72 numeric(14,2),
      P73 numeric(14,2),
      P81 numeric(14,2),
      P82 numeric(14,2),
      EMPL integer,
      FROMDATE timestamp,
      TODATE timestamp,
      TAXOFFICE varchar(200) CHARACTER SET UTF8                           ,
      COSTS smallint)
   as
declare variable cfromdate timestamp;
declare variable person integer;
declare variable aktuoperator integer;
declare variable descript varchar(255);
begin
  if (employee = 0) then
    employee = null;

  for
    select PP.employee
      from epayrolls PR 
        join eprpos PP on (PR.ref = PP.payroll)
        join employees E on (PP.employee = E.ref)
      where PP.pvalue <> 0 and PP.ecolumn in (3000, 5950)
        and PR.tper starting with :yearid
        and (PP.employee = :employee or :employee is null)
        and PR.company = :currentcompany
      group by E.personnames, PP.employee
      into :empl
  do begin

    select min(fromdate), max(coalesce(enddate, :yearid || '-12-31'))
      from emplcontracts EC
      where EC.employee = :empl
      into :cfromdate, :todate;

    if (cfromdate < yearid || '-01-01') then
      fromdate = yearid || '-01-01'; --'01-01-rok';
    else begin
      fromdate = cfromdate;
    end

    /*
    --BS19840
    --jesli pracownik zostal zwolniony to 'todate' jest data jego ostatniej wyplaty
    select emplstatus
      from employees
    where ref = :empl
    into :tmpEmplStatus;

    if (tmpEmplStatus = 0) then
    */
    if (:todate < cast(:yearid || '-12-31' as date) ) then
    begin
      select max(PR.payday)
        from epayrolls PR
          join eprpos PP on (PR.ref = PP.payroll and PP.ecolumn = 7450)
        where PP.employee = :empl
      into :todate;
    end

    if ((todate is null) or (todate > yearid || '-12-31'))  then
      todate = yearid || '-12-31'; --'31-12-2005';

    p36 = null;
    p37 = null;
    p38 = null;
    p39 = null;
    p54 = null;
    p55 = null;
    p56 = null;
    p57 = null;
    p62 = null;
    p63 = null;
    p64 = null;
    p65 = null;
    p70 = null;
    p71 = null;
    p72 = null;
    p73 = null;
    p81 = null;
    p82 = null;
    costs = null;
    taxoffice = null;

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 1)
        join ecolumns C on (PP.ecolumn = C.number and C.cflags like '%;POD;%')
      where PP.employee = :empl and PR.tper starting with :yearid
      into :p36;   -- przychod

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 1)
      where PP.ecolumn = 6500 and PP.employee = :empl
        and PR.tper starting with :yearid
      into :p37;   -- koszty uzyskania

    if (p37 is null) then p37 = 0;

    p38 = :p36 - :p37;   -- dochod

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 1)
      where PP.ecolumn in (7350, 7370) and PP.employee = :empl
        and PR.tper starting with :yearid
      into :p39;   -- zaliczka na podatek dochodowy (razem z wyrownaniem)

    if (p39 is null) then p39 = 0;

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 2)
        join emplcontracts C on (C.ref = PR.emplcontract)
      where PP.ecolumn = 3000 and PP.employee = :empl
        and PR.tper starting with :yearid and C.prcosts <> 50
      into :p54;   -- przychod

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 2)
        join emplcontracts C on (C.ref = PR.emplcontract)
      where PP.ecolumn = 6500 and PP.employee = :empl
        and PR.tper starting with :yearid and C.prcosts <> 50
      into :p55;   -- koszty uzyskania

    p56 = :p54 - :p55;   -- dochod

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 2)
        join emplcontracts C on (C.ref = PR.emplcontract)
      where PP.ecolumn in (7350, 7370) and PP.employee = :empl
        and PR.tper starting with :yearid and C.prcosts <> 50
      into :p57;   -- zaliczka na podatek dochodowy (razem z wyrownaniem)

    --Rada nadzorcza
    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 3)
        join emplcontracts C on (C.ref = PR.emplcontract)
      where PP.ecolumn = 3000 and PP.employee = :empl
        and PR.tper starting with :yearid
      into :p62;   -- 62

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 3)
        join emplcontracts C on (C.ref = PR.emplcontract)
      where PP.ecolumn = 6500 and PP.employee = :empl
        and PR.tper starting with :yearid
      into :p63;   -- 63

    p64= :p62-:p63;

    -- przychod
    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 3)
        join emplcontracts C on (C.ref = PR.emplcontract)
      where PP.ecolumn = 7350 and PP.employee = :empl
        and PR.tper starting with :yearid
      into :p65;   -- 65

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 2)
        join emplcontracts C on (C.ref = PR.emplcontract)
      where PP.ecolumn = 3000 and PP.employee = :empl
        and PR.tper starting with :yearid and C.prcosts = 50
      into :p70;   -- przychod

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 2)
        join emplcontracts C on (C.ref = PR.emplcontract)
      where PP.ecolumn = 6500 and PP.employee = :empl
        and PR.tper starting with :yearid and C.prcosts = 50
      into :p71;   -- koszty uzyskania

    p72 = :p70 - :p71;   -- dochod

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 2)
        join emplcontracts C on (C.ref = PR.emplcontract)
      where PP.ecolumn in (7350, 7370) and PP.employee = :empl
        and PR.tper starting with :yearid and C.prcosts = 50
      into :p73;   -- zaliczka na podatek dochodowy (razem z wyrownaniem)

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll)
      where PP.ecolumn in (6100, 6110, 6120, 6130) and PP.employee = :empl
        and PR.tper starting with :yearid
      into :p81;   -- skladki na ubezpieczenie spoleczne

    if (p81 is null) then p81 = 0;

    select sum(PP.pvalue)
      from eprpos pp
        join epayrolls PR on (PR.ref = PP.payroll)
      where PP.ecolumn = 7210 and PP.employee = :empl
        and PR.tper starting with :yearid      
      into :p82;   -- skladki na ubezpieczenie zdrowotne

    if (p82 is null) then p82 = 0;

    --zapisz do 'costs' koszty uzyskania danego pracownika. dane pobieraj
    --z ostatniego okresu nie przekraczajacego podanego roku (yearid)
    select T.costs, IR.name || ', ' || IR.address || ', ' || IR.postcode || ' ' || IR.city
      from empltaxinfo T
        left join einternalrevs IR on (T.taxoffice = IR.ref)
      where T.employee = :empl
        and T.fromdate = (
          select max(T2.fromdate)
          from empltaxinfo T2
          where T2.employee = :empl and (T2.fromdate < :yearid || '-12-31')
        )
      into :costs, :taxoffice;

    if (p37 is null or p37 = 0) then
      costs = null;

    suspend;
  end
  if (typ = 1) then
  begin
    select person
      from employees
      where ref = :employee
      into :person;

    execute procedure get_global_param ('AKTUOPERATOR')
      returning_values aktuoperator;
    taxoffice = coalesce(taxoffice, 'Urzędu Skarbowego');
    descript = 'Przekazanie danych na formularzu PIT-11 do ' || :taxoffice;

    if (not exists (select ref
                      from epersdatasecur
                      where person = :person and kod = 2 and descript = :descript
                        and operator = :aktuoperator and regdate = :data)) then
    begin
      insert into epersdatasecur (person, kod, descript, operator, regdate)
        values (:person, 2, :descript, :aktuoperator, :data);
    end
  end
end^
SET TERM ; ^
