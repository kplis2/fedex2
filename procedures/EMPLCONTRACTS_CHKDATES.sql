--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMPLCONTRACTS_CHKDATES(
      CONTRACT integer,
      FROMDATE date,
      TODATE date,
      EMPLOYEE integer,
      EMPLTYPE smallint,
      CONTRTYPE ECONTRTYPES_ID = null)
   as
declare variable CREF integer;
declare variable AFROMDATE date;
declare variable CONTROVERLAP smallint;
begin

  if (fromdate > todate) then
    exception data_do_musi_byc_wieksza;

  --sprawdzenie, czy nie zamykamy umowy wczesniej niz wystawilismy do niej aneks (BS25412)
  if (todate is not null) then
  begin
    if (exists(select first 1 1 from econtrannexes a
                 join emplcontracts c on (a.emplcontract = c.ref)
                 where a.emplcontract = :contract
                   and a.fromdate > :todate)
    ) then
      exception annex_after_enddate;
  end

  --sprawdzenie zachodzenia na siebie umow (PR60331)
  select first 1 t.controverlap from emplcontracts e
    join econtrtypes t on (e.econtrtype = t.contrtype)
    where e.employee = :employee
      and t.controverlap = 3
    into :controverlap;

  if (controverlap is null) then
  begin
    select first 1 t.controverlap from emplcontracts e
      join econtrtypes t on (e.econtrtype = t.contrtype)
      where e.employee = :employee
        and t.controverlap = 2
        and t.empltype = :empltype
      into :controverlap;

    if (controverlap is null) then
      select controverlap from econtrtypes
        where contrtype = :contrtype
        into :controverlap;
  end

  if (controverlap <> 0) then
  begin
    --zamykam poprzedni okres, jezli byl otwarty (BS25499)
    select c.ref, max(a.fromdate)
      from emplcontracts c
        left join econtrannexes a on a.emplcontract = c.ref
      where c.employee = :employee and c.ref <> :contract
        and c.enddate is null and c.fromdate < :fromdate
        and (:controverlap = 3
          or (:controverlap = 2 and empltype = :empltype)
          or (:controverlap = 1 and econtrtype = :contrtype))
      group by c.ref
      into :cref, :afromdate;

    if (cref is not null) then
    begin
      if (coalesce(afromdate,fromdate - 1) < fromdate) then
      begin
       if (empltype = 1) then
         update emplcontracts set enddate = :fromdate - 1
           where ref = :cref and enddate is distinct from (:fromdate - 1);
      end else
        exception annex_exists;
    end

    --sprawdzenie daty "od", spr. czy okres rozpoczyna sie wewnatrz innego
    if (exists(select first 1 1 from emplcontracts
                 where employee = :employee and ref <> :contract
                   and fromdate <= :fromdate
                   and (coalesce(enddate,todate) >= :fromdate or coalesce(enddate,todate) is null)
                   and (:controverlap = 3
                     or (:controverlap = 2 and empltype = :empltype)
                     or (:controverlap = 1 and econtrtype = :contrtype)))
    ) then
      exception data_od_w_srodku_innego;

    -- sprawdzenie czy okres konczy sie wewnatrz innego
    if (exists(select first 1 1 from emplcontracts
                 where employee = :employee and ref <> :contract
                   and fromdate <= :todate
                   and (coalesce(enddate,todate) >= :fromdate or coalesce(enddate,todate) is null)
                   and (:controverlap = 3
                     or (:controverlap = 2 and empltype = :empltype)
                     or (:controverlap = 1 and econtrtype = :contrtype)))
    ) then
      exception data_do_w_srodku_innego;

    --sprawdzenie czy okres nie zawiera innego
    if (exists(select first 1 1 from emplcontracts
                 where employee = :employee and ref <> :contract
                   and (fromdate <= :todate or :todate is null)
                   and (coalesce(enddate,todate) >= :fromdate or coalesce(enddate,todate) is null)
                   and (:controverlap = 3
                     or (:controverlap = 2 and empltype = :empltype)
                     or (:controverlap = 1 and econtrtype = :contrtype)))
    ) then
      exception data_oddo_na_zewnatrz;
  end
end^
SET TERM ; ^
