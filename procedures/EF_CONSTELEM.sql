--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_CONSTELEM(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      ECOLUMN integer,
      AKRONIM varchar(20) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable FROMDATE date;
declare variable TODATE date;
declare variable NUM integer;
declare variable evalue numeric(14,2);
begin
--MWr: Personel - zwraca wartosc stalego skladnika

  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  if (exists(select first 1 1 from epayrolls where ref = :payroll and empltype = 2) -->czy lista jest rachunkiem (PR27914)
      or exists(select first 1 1 from emplcontracts where empltype <> 2   -->czy istnieje wazna umowa dla pracownika przy liscie plac za dany okres
                  and employee = :employee and fromdate <= :todate
                  and (enddate >= :fromdate or enddate is null))
  ) then begin

    select sum(case when etype = 0 then evalue else 0 end),
           sum(case when etype = 1 then etype else 0 end)
      from econstelems
      where (fromdate is null or fromdate <= :todate)
        and (todate is null or todate >= :fromdate)
        and employee = :employee and ecolumn = :ecolumn
      into :evalue, :num;

    ret = coalesce(evalue,0);

    if (num > 0) then
    begin
      if (akronim <> '') then
      begin
        execute procedure ef_pval(employee, payroll, prefix, akronim)
          returning_values evalue;
        ret = ret + evalue * num;
      end else
        exception universal 'Składnik ' || ecolumn || ' nie jest powiązany z parametrem płacowym - musi być zadany kwotowo. Należy poprawić definicję stałego składnika u pracownika.';
    end
  end

  suspend;
end^
SET TERM ; ^
