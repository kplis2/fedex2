--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CONTACT_REGISTER(
      CPODMIOT integer,
      INOUT smallint,
      SLODEF integer,
      SLOPOZ integer,
      MEDIUM integer,
      MSGTEXT varchar(2048) CHARACTER SET UTF8                           ,
      TOADRESS varchar(100) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE STRING VARCHAR(100);
DECLARE VARIABLE CTYPYKON INTEGER;
DECLARE VARIABLE CELL VARCHAR(100);
DECLARE VARIABLE PODMIOT VARCHAR(100);
DECLARE VARIABLE EMAILADR VARCHAR(100);
begin
  execute procedure getconfig('SMSPODMIOT') returning_values :podmiot;
  if(slodef is not null and slopoz is not null) then begin
    select cpodmioty.ref
      from cpodmioty
      where cpodmioty.slodef = :slodef and cpodmioty.slopoz = :slopoz
    into :cpodmiot;
  end else if (:cpodmiot is null or :cpodmiot = 0) then  begin
    cpodmiot = cast(:podmiot as integer);
    select min(ref) from slodef where slodef.typ = 'KLIENCI' into :slodef;
  end
  if(:medium = 1) then begin
    execute procedure getconfig('SMSKONTAKT') returning_values :string;
    emailadr = null;
    cell = :toadress;
    if( not exists(select ref from cphones
        where cphones.fullnumber = :toadress) ) then begin
      cpodmiot = cast(:podmiot as integer);
    end
  end
  if(:medium = 2) then begin
    execute procedure getconfig('EMAILKONTAKT') returning_values :string;
    cell = '';
    emailadr = :toadress;
    if(:cpodmiot is null) then
      cpodmiot = cast(:podmiot as integer);
  end
  if(:string is not null and :string <> '') then
    ctypykon = cast(:string as integer);
  if(:inout is null) then inout = 0;
  if(:cpodmiot is not null and :ctypykon is not null) then begin
    insert into kontakty (inout, rodzaj, nazwa, cpodmiot, opis, kontakttelefon, emailadr)
                   values(:inout, :ctypykon, 'Kontakt z serwera powiadomie', :cpodmiot, :msgtext, :cell, :emailadr);
  end
end^
SET TERM ; ^
