--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_DOK_FAK_W_INNYCH_ODD(
      DATAOD date,
      DATADO date,
      REJESTR varchar(3) CHARACTER SET UTF8                           ,
      ODDZIAL varchar(20) CHARACTER SET UTF8                           )
  returns (
      NODDZIAL varchar(20) CHARACTER SET UTF8                           ,
      WARTWZ numeric(14,2),
      WARTZZ numeric(14,2))
   as
DECLARE VARIABLE REF INTEGER;
begin
  for
    select nagfak.oddzial, dokumnag.wartosc, dokumnag.ref
      from dokumnag
      left join defdokum on (dokumnag.typ = defdokum.symbol)
      left join nagfak on (dokumnag.faktura = nagfak.ref)
      where nagfak.oddzial <> dokumnag.oddzial and defdokum.wydania = 1 and defdokum.koryg = 0
        and defdokum.zewn = 1 and (DOKUMNAG.AKCEPT =1 or dokumnag.akcept = 8)
        and  (NAGFAK.AKCEPTACJA=1 or NAGFAK.AKCEPTACJA=8)
        and NAGFAK.ZAKUP = 0 and NAGFAK.ANULOWANIE=0 and NAGFAK.NIEOBROT<2
        and dokumnag.faktura > 0
        and defdokum.symbol is not null and nagfak.ref is not null
        AND (nagfak.data >= :DATAOD and :dataod is not null or (:dataod is null))
        AND (nagfak.data <= :DATADo and :datado is not null or (:datado is null))
        AND (nagfak.rejestr = :rejestr and :rejestr is not null or (:rejestr is null))
        AND (dokumnag.oddzial = :oddzial and :oddzial is not null or (:oddzial is null))
      into :NODDZIAL, :WARTWZ, :REF
     do begin
       select sum(dokumnag.wartosc) from dokumnag
         where dokumnag.refk = :ref and (DOKUMNAG.AKCEPT =1 or dokumnag.akcept = 8)
         into :WARTZZ;
         if (:wartzz is null) then
           wartzz = 0;
       suspend;
     end
end^
SET TERM ; ^
