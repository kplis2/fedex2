--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYS_CHECKDOKUM(
      TYPDOK char(1) CHARACTER SET UTF8                           ,
      DOKUMENT integer,
      SPEDYCJA integer)
  returns (
      STATUS integer,
      DOKUMSPED integer)
   as
declare variable konf varchar(255);
declare variable cnt integer;
begin
  status = 0;
  dokumsped = 0;
  if(:typdok <> 'M' and :typdok<>'Z') then
    exit;
  execute procedure GETCONFIG('SPEDYTUZUP') returning_values :konf;
  if(:konf = '' or (:konf = '0') or (:konf is null)) then
    exit;
  if(:konf <> '')then begin
    dokumsped = null;
    select max(ref) from LISTYWYSD where REFDOK = :dokument and LISTAWYS = :spedycja into :dokumsped;
    if(:dokumsped is null) then dokumsped = 0;
    if(:dokumsped = 0) then exit;
    /*jest dokument spedycyjny na liscie*/
    if(:konf = '3') then begin
      /*uzupelnienie bezwarunkowe*/
      status = 3;
      exit;
    end
    if(:konf = '2') then begin
      /*uzupelnienie jak operator potiwerdzi*/
      status = 2;
      exit;
    end
    if(:konf = '1') then begin
      /*uzupelnienie, gdy nie ma pozycji i operator potiwerdzi*/
      cnt = null;
      select count(*) from LISTYWYSDPOZ where dokument = :dokumsped into :cnt;
      if(:cnt is null) then cnt = 0;
      if(:cnt > 0) then
        status = 0;
      else
        status = 2;
      exit;
    end
  end

end^
SET TERM ; ^
