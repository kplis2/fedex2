--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWIAD_NEWMAILFROMIMAP(
      TYTUL varchar(1024) CHARACTER SET UTF8                           ,
      DATA timestamp,
      EMAIL varchar(1024) CHARACTER SET UTF8                           ,
      ODKOGO varchar(1024) CHARACTER SET UTF8                           ,
      DOKOGO varchar(2048) CHARACTER SET UTF8                           ,
      CC varchar(2048) CHARACTER SET UTF8                           ,
      DELIVEREDTO varchar(255) CHARACTER SET UTF8                           ,
      IDWATKU integer,
      STAN smallint,
      OPERATOR integer,
      FOLDER integer,
      ID varchar(255) CHARACTER SET UTF8                           ,
      UID integer,
      IID integer,
      TRYB integer)
   as
declare variable sql varchar(8191);
begin
  if(:iid > 0 and exists(select ref from emailwiad where folder = :folder and iid = :iid)) then begin
       sql = 'update emailwiad set tytul = '||:tytul||', data = '||:data||', email = '||:email||', od = '||:odkogo||',';
      sql = :sql || ' dokogo = '||:dokogo||', cc = '||:cc||', deliveredto = '||:deliveredto||', idwatku = '||:idwatku||',  stan = '||:stan||',';
      sql = :sql ||   ' operator = '||:operator||', id = '||:id||', uid = '||:uid||', iid = '||:iid||', tryb = '||:tryb;
      sql = :sql ||' where folder = '||:folder||' and iid = '||:iid;
      execute statement :sql;
  end else
    insert into EMAILWIAD(TYTUL,DATA,EMAIL,OD, DOKOGO, CC, DELIVEREDTO, IDWATKU, STAN, OPERATOR, FOLDER,
       ID, UID, IID, TRYB)
    values (:tytul, :data, :email, :odkogo, :dokogo, :cc, :deliveredto, :idwatku, :stan, :operator, :folder,
       :id, :uid, :iid,  :tryb);
end^
SET TERM ; ^
