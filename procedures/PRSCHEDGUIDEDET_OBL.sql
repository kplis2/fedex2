--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDEDET_OBL(
      STABLE varchar(31) CHARACTER SET UTF8                           ,
      SOURCE smallint,
      SREF integer,
      ACK smallint)
   as
declare variable guidepozref integer;
declare variable pozzamref integer;
declare variable quantity numeric(14,4);
declare variable ilzreal numeric(14,4);
declare variable ilzrealack numeric(14,4);
declare variable status integer;
declare variable jedn integer;
declare variable przelicznik numeric(14,4);
declare variable rozl smallint;
declare variable ktm varchar(40);
declare variable wersjaref integer;
declare variable prschedguidedet integer;
declare variable amountp numeric(14,4);
declare variable oldamountp numeric(14,4);
declare variable shortageratio numeric(14,4);
declare variable prschedguidepos integer;
declare variable prschedoper integer;
declare variable overlimit smallint;
declare variable shortage smallint;
declare variable byproduct smallint;
begin
  --statusy przewodnika (jesli dokumnagi poaakceptpowane to przewodnik zamkniety)
  if (ack = 0) then rozl = 1;
  else if (ack = 1) then rozl = 2;
  else if (ack = 9) then rozl = 3;
  if (ack = 0) then
    select ilosc, ktm, wersjaref from dokumpoz where ref = :sref        
      into quantity, ktm, wersjaref;
  else
    select iloscl, ktm, wersjaref from dokumpoz where ref = :sref
      into quantity, ktm, wersjaref;
  if (quantity is null) then quantity = 0;
  if (quantity = 0) then rozl = 0;
  if (source = 3) then -- przychodowy dokument dla produkcji
  begin      -- dla przychodowych pozycji obslugujemy przeliczniki na pozycjach z Karty tech.
    select p.jedn
      from prschedguidedets d
        left join prschedguides g on (g.ref = d.prschedguide)
        left join prsheets p on (p.ref = g.prsheet)
      where d.ref = :prschedguidedet
      into :jedn;
    select j.przelicz from towjedn j where j.ref = :jedn into :przelicznik;
    quantity = quantity / przelicznik;
  end
  if (rozl <> 0) then
  begin
    update prschedguidedets d set d.sourceupdate = 1, d.oldquantity = d.quantity
      where d.stable = :stable and d.sref = :sref;
    update prschedguidedets d set d.sourceupdate = 1, d.quantity = 0
      where d.stable = :stable and d.sref = :sref;
    for
      select d.ref, d.prschedguidepos, d.prschedoper, d.overlimit, d.shortage, d.byproduct, d.oldquantity, d.shortageratio
        from prschedguidedets d
        where d.stable = :stable and d.sref = :sref
        order by d.overlimit, d.shortage, d.ref
        into prschedguidedet, prschedguidepos, prschedoper, overlimit, shortage, byproduct, oldamountp, shortageratio
    do begin
      if (shortage = 1) then
        oldamountp = oldamountp * shortageratio;
      if (quantity < oldamountp) then
        if (shortage = 1) then
          oldamountp = quantity * shortageratio;
        else
          oldamountp = quantity;
      update prschedguidedets d set d.accept = :rozl, d.quantity = :oldamountp,
          d.ktm = :ktm, d.wersjaref = :wersjaref, d.sref = :sref, d.sourceupdate = 1
        where d.ref = :prschedguidedet;
      if (prschedguidepos is not null) then
        EXECUTE PROCEDURE PRSCHEDGUIDEPOS_AMOUNTZREAL(:prschedguidepos);
      quantity = quantity - oldamountp;
    end
    if (quantity > 0 and source = 3) then
      exception prschedguidedet_error 'Próba zwiększenia ilości wyrobu gotowego. Wystaw nowy dokument';
    else if (quantity > 0 and source = 4) then
      exception prschedguidedet_error 'Próba zwiększenia ilości surowca. Wystaw nowy dokument'||' '||:ack||' '||sref||' '||quantity;
  end
  else if (rozl = 0) then
  begin
    if (ack >= 0) then
    begin
      update prschedguidedets d set d.accept = :rozl, d.quantity = :quantity, d.sourceupdate = 1
        where d.stable = :stable and d.sref = :sref;
      for
        select d.prschedguidepos
          from prschedguidedets d
          where d.stable = :stable and d.sref = :sref and d.prschedguidepos is not null
          group by d.prschedguidepos
          into prschedguidepos
      do begin
        EXECUTE PROCEDURE PRSCHEDGUIDEPOS_AMOUNTZREAL(:prschedguidepos);
      end
    end
    else begin
      update prschedguidedets d set d.sourceupdate = 1, d.quantity = 0 where d.stable = :stable and d.sref = :sref;
      for
        select d.prschedguidepos
          from prschedguidedets d
          where d.stable = :stable and d.sref = :sref and d.prschedguidepos is not null
          group by d.prschedguidepos
          into prschedguidepos
      do begin
        EXECUTE PROCEDURE PRSCHEDGUIDEPOS_AMOUNTZREAL(:prschedguidepos);
      end
      delete from prschedguidedets d where d.stable = :stable and d.sref = :sref;
    end
  end
  if (ack >= 0) then
    update prschedguidedets d set d.sourceupdate = 0, d.oldquantity = 0
      where d.stable = :stable and d.sref = :sref;
end^
SET TERM ; ^
