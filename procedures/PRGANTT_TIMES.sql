--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRGANTT_TIMES(
      PRMACHINE integer)
  returns (
      PRGANTTTIME timestamp)
   as
begin
  if (prmachine is null) then prmachine = 0;
  for
    select g.timefrom
      from prgantt g
      where g.timefrom is not null
        and (g.prmachine = :prmachine or :prmachine = 0)
      group by g.timefrom
      into prgantttime
  do begin
    suspend;
  end
  for
    select g.timeto
      from prgantt g
      where g.timeto is not null
        and (g.prmachine = :prmachine or :prmachine = 0)
      group by g.timeto
      into prgantttime
  do begin
    suspend;
  end
end^
SET TERM ; ^
