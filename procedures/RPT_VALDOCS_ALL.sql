--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_VALDOCS_ALL(
      DATAOD timestamp,
      DATADO timestamp)
  returns (
      DOCTYPE varchar(2) CHARACTER SET UTF8                           ,
      DOCDATE timestamp,
      OPERDATE timestamp,
      TVALUE numeric(15,2),
      TVALLIM numeric(15,2),
      TREDEMPTION numeric(15,2),
      FVALUE numeric(15,2),
      FREDEMPTION numeric(15,2),
      DEPARTMENT varchar(10) CHARACTER SET UTF8                           ,
      AMORTGRP varchar(10) CHARACTER SET UTF8                           ,
      SYMBOL1 varchar(20) CHARACTER SET UTF8                           ,
      NAME varchar(80) CHARACTER SET UTF8                           ,
      OPERIODNAME varchar(20) CHARACTER SET UTF8                           ,
      SYMBOL varchar(20) CHARACTER SET UTF8                           )
   as
declare variable refdoc integer;
begin

    for select VD.doctype, VD.docdate, VD.operdate, VD.tvalue, VD.tvallim,
           vd.tredemption, vd.fvalue, vd.fredemption, FA.department,
           FA.amortgrp, FA.symbol, FA.name, AP.name, VD.symbol,VD.ref
            from valdocs VD
            join fxdassets FA on (FA.ref=VD.fxdasset)
            join amperiods AP on (AP.ref=VD.operiod)
            where docdate>=:DATAOD and docdate<=:DATADO and FA.company = 1
            and AP.company = 1
        order by docdate
    into :doctype, :docdate, :operdate, :tvalue, :tvallim,
           :tredemption, :fvalue, :fredemption, :department,
           :amortgrp, :symbol1, :name, :operiodname, :symbol,:refdoc
  do begin
    if (:doctype = 'OT') then begin
       select VD.doctype, VD.docdate, VD.operdate, FA.tvalue, FA.tvallim,
           FA.tredemption, FA.fvalue, FA.fredemption, FA.department,
           FA.amortgrp, FA.symbol, FA.name, AP.name
            from valdocs VD
            join fxdassets FA on (FA.ref=VD.fxdasset)
            join amperiods AP on (AP.ref=VD.operiod)
            where docdate>=:DATAOD and docdate<=:DATADO and FA.company = 1
            and AP.company = 1 and vd.doctype = 'OT' and vd.ref=:refdoc
        order by docdate
    into :doctype, :docdate, :operdate, :tvalue, :tvallim,
           :tredemption, :fvalue, :fredemption, :department,
           :amortgrp, :symbol, :name, :operiodname;
    end
    suspend;
  end
end^
SET TERM ; ^
