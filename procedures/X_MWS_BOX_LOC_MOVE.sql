--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_BOX_LOC_MOVE(
      KODKRESK STRING100,
      LOKACJA STRING100,
      WYSOKOSC NUMERIC_14_4 = 0)
  returns (
      STATUS INTEGER_ID)
   as
begin
status  = 1;
  if (wysokosc  = 0) then -- kartony, bo wywoluje z HH bez wysokosci
    update listywysdroz_opk l
      set l.x_moved = 0, l.x_mwsconstlock = :lokacja
      where l.stanowisko = :kodkresk;

  if (wysokosc > 0) then -- palety, wysokosc z HH podawana
    update listywysdroz_opk l
      set l.x_moved = 0, l.x_mwsconstlock = :lokacja, l.x_wysokosc = :wysokosc
      where l.stanowisko = :kodkresk;

  suspend;
end^
SET TERM ; ^
