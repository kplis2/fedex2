--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_GET_FIRSTFAK(
      REFNAGFAK integer)
  returns (
      SYMBOL varchar(2094) CHARACTER SET UTF8                           ,
      DATA timestamp,
      DATASPRZ timestamp)
   as
declare variable nagfak integer;
declare variable symbolg varchar(20);
declare variable datag timestamp;
declare variable datasprzg timestamp;
begin
  symbol = '';
  data = null;
  datasprz = null;
  for select distinct nagfak
    from nagfak_get_firstfak_up(:refnagfak) g
    order by g.nagfak
    into :nagfak
  do begin
    symbolg = '';
    datag = null;
    datasprzg = null;
    select symbol, data, datasprz from nagfak where ref = :nagfak into :symbolg, :datag, :datasprzg;
    if(:symbol<>'') then symbol = :symbol || '; ';
    symbol = substring(symbol||' '||:symbolg||' - '||cast(:datag as date) from 1 for 2024);
    if(datasprz is null or datasprz is not null and datasprz > datasprzg)then begin
      datasprz = datasprzg;
      data = datag;
    end
  end
  suspend;
end^
SET TERM ; ^
