--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAILBOXES_FOR_OPERATOR(
      OPER integer)
  returns (
      REF integer)
   as
declare variable operlike varchar(40);
begin
 operlike = '%;'||:oper ||';%';
  --skrzynki wlasne oepratora
  for
  select ref from mailboxes where OPERATOR = :oper
  into :ref
  do begin
    suspend;
  end
  --skrzynki, do ktorych oeprator ma prawo zapisu lub odczytu
  for
  select distinct mailbox
  from deffold d
  where mailbox > 0
    and
      ((d.operator = :oper)
     or (d.operread like :operlike)
     or (d.operedit like :operlike)
     or (d.opersend like :operlike)
       )
  into :ref
  do begin
    suspend;
  end

end^
SET TERM ; ^
