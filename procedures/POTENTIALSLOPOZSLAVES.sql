--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POTENTIALSLOPOZSLAVES(
      SLOPOZREF varchar(4096) CHARACTER SET UTF8                           ,
      PATTERNSLODEF SLO_ID)
  returns (
      COMPANY COMPANIES_ID,
      NAMEB STRING20,
      REFB YEARS_ID)
   as
declare variable f smallint_id;
declare variable val string20;
declare variable whereexpression string8191;
declare variable tmpref slo_id;
declare variable allow_slodef smallint_id;
declare variable SQL string1024;
begin



  if (slopozref = '') then
    exception universal'Brak wybranych pozycji';

  slopozref = substring( slopozref from 1 for char_length(slopozref) );
  slopozref = substring( slopozref from 1 for char_length(slopozref));
  whereexpression = '( ';

  for
    select outstring
    from parse_lines(:slopozref,';')
    into :val
  do begin
    if (f is not null) then
      whereexpression = whereexpression||' , '||val;
    else
      whereexpression = whereexpression||val;
    f=0;
  end

  whereexpression = whereexpression||')';

  for
    select ref , nazwa, company
      from slodef s
      where s.patternref = :patternslodef
      into :tmpref, :nameb, :company
  do begin
    SQL = 'select first 1 1 from slopoz p where p.pattern_ref in '||whereexpression||' and p.slownik = '||:tmpref;
    allow_slodef = null;
    execute statement SQL
      into :allow_slodef;
    if (allow_slodef is null)then
    begin
      refb = tmpref;
      suspend;
    end
  end
end^
SET TERM ; ^
