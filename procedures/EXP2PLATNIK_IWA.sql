--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXP2PLATNIK_IWA(
      DYEAR integer,
      COMPANY integer)
  returns (
      IP2 varchar(6) CHARACTER SET UTF8                           ,
      IIIP1 integer,
      IVP1 varchar(30) CHARACTER SET UTF8                           ,
      IVP2 integer,
      IVP3 integer,
      IVP4 integer)
   as
declare variable INSURED_TMP integer;
declare variable I smallint;
declare variable FROMDATE timestamp;
declare variable TODATE timestamp;
declare variable IPERIOD varchar(6);
declare variable REFZUS varchar(10);
declare variable BREAKECOLS varchar(100);
begin
--MWr: Personel - Eksport do Platnika informacji dla ZUS IWA (ver.xml)

/* Ip1    -Kod terytorialny jednostki terenowej ZUS
   IIIp3  -Liczba ubezpieczonych
   IVp1   -Rodzaj dzialalnosci wedlug PKD
   IVp2   -Liczba poszkodowanych w wypadkach przy pracy ogolem
   IVp3   -Liczba poszkodowanych w wypadkach przy pracy smiertelnych i ciezkich
   IVp4   -Liczba zatrudnionych w warunkach zagrozenia */

--I. Dane organizacyjne
  execute procedure get_config('PODKODZUS', -1)
    returning_values :refzus;

  if (refzus <> '') then
    select code from edictzuscodes
      where ref = cast(:refzus as integer)
      into :Ip2;
  Ip2 = coalesce(Ip2,'');

--III. Dane o liczbie ubezpieczonych zgloszonych do ubezpieczenia wypadkowego
  i = 0;
  IIIp1 = 0;
  breakecols = ';140;150;260;270;280;290;300;330;440;450;'; --BS59098
  while (i < 12) do
  begin
    execute procedure e_func_periodinc(:dyear || '01', :i)
      returning_values :iperiod;

    execute procedure period2dates(:iperiod)
      returning_values :fromdate, :todate;

    insured_tmp = 0;
    select count(person) from (
       select distinct person from (
           select e.person as person
                , a.atodate - a.afromdate + 1 as adys
                , m.todate - m.fromdate + 1 as wdays
             from employees e
             left join e_get_employmentchanges(e.ref,:fromdate, :todate, '') m on (1=1)
             left join e_get_whole_eabsperiod(-e.ref, :fromdate, :todate, null, null, null, :breakecols) a on (1=1)
             where e.company = :company
               and m.fromdate is not null
         union
           select e.person as person
               , (atodate - afromdate + 1) as adys
               , iif(c.enddate is null or c.enddate > :todate, :todate, c.enddate)
               - iif(c.fromdate > :fromdate, c.fromdate , :fromdate) + 1  as wdays
             from employees e
             join emplcontracts c on (e.ref = c.employee)
             left join e_get_whole_eabsperiod(-e.ref, :fromdate, :todate, null, null, null, :breakecols) a on (1=1)
             where e.company = :company
               and c.empltype > 1
               and c.iflags like '%FW%'
               and c.fromdate <= :todate
               and (c.enddate >= :fromdate or c.enddate is null))
      group by person
      having max(coalesce(adys,0)) <  max(coalesce(wdays,0)))
      into :insured_tmp;

    if (insured_tmp = 0) then
    begin
      IIIp1 = 0;
      i = 12;
    end else
      IIIp1 = IIIp1 + insured_tmp;
    i = i + 1;
  end
  IIIp1 = round(cast(IIIp1 as numeric(14,2))/12.00);

--IV. Zestawienie danych do ustalenia kategorii ryzyka dla platnika skladki
  execute procedure get_config('INFOPKD', -1)
    returning_values :IVp1;

  IVp1 = replace(trim(IVp1),  '.',  ''); --PR36514
  if (coalesce(char_length(IVp1),0) not in (4,5)) then -- [DG] XXX ZG119346
    exception universal 'Brak numeru PKD lub niewłaściwy format numeru
(Menu >Definicje >Dane firmy >PKD)';

  select sum(case when eventtype like '%;%;%' then 1 else 0 end),
         sum(case when eventtype containing ';SMI;' or eventtype containing ';CIE;' then 1 else 0 end)
    from bhpaccident
    where extract(year from rptcreatedate) = :dyear
      and eventtype like '%;%;%'
      and eventmode = 0
    into :IVp2, :IVp3;

  IVp2 = coalesce(IVp2,0);
  IVp3 = coalesce(IVp3,0);
  IVp4 = 0;

  if (IIIp1 >= 10) then
    suspend;
end^
SET TERM ; ^
