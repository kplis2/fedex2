--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_WHOLE_EABSPERIOD(
      EABSREF integer,
      RFROMDATE date,
      RTODATE date,
      RCERTSERIAL varchar(2) CHARACTER SET UTF8                           ,
      RCERTNUMBER varchar(7) CHARACTER SET UTF8                           ,
      RSCODE integer,
      RECOL_LIST varchar(255) CHARACTER SET UTF8                           ,
      RBCODE integer = null,
      EABSCARD smallint = 0)
  returns (
      AFROMDATE date,
      ATODATE date)
   as
declare variable EMPL integer;
declare variable ECOLUMN integer;
declare variable FROMDATE_TEMP date;
declare variable TODATE_TEMP date;
declare variable LOOP_FROM smallint;
declare variable LOOP_TO smallint;
begin
/* MWr Personel: procedura na podstawie danej nieobecnosci (EABSREF) sprawdza
                 jej faktyczny zakres (czas trwania) przy zadanych warunkach

  Zadane warunki nadaja parametry:
   EABSREF - ref okresla tyb nieobecnosci (ecolumn) i nr pracownika
   RFROMDATE, RTODATE - dolne i gorne ograniczenie czasowe dla zwracanego okresu
   RCERTSERIAL, RCERTNUMBER - ograniczenie na serie i numer zaswiadczenia L4
   RSCODE - ogranicznie na kod nieobecnosci
   RBCODE - ogranicznie na kod swiadczenia przerwy (PR35887)
   RECOL_LIST - okresla numery ecolumn, ktore skladaja sie na ciaglosc nieobecnosci,
     numery podajemy jako liste odzielona srednikami, koniecznie z srednikiem na
     poczatku i koncu listy np: ';nr1;nr2;nr3;'. Istnienie parametru wylacza
     sprawdzenie ciaglosci na podstawie ecolumn branego z EABSREF
   EABSCARD - okresla, czy procedura ma zwracac wyniki dla kartoteki nieobecnosci
     uwzgledniajac correction = 1
*/

  recol_list = coalesce(recol_list,'');
  rscode = coalesce(rscode,0);
  rbcode = coalesce(rbcode,0);

  if (eabsref < 0 and recol_list <> '' and rfromdate < rtodate) then --BS59098
  begin
    select first 1 ref from eabsences
      where employee = abs(:eabsref)
        and correction in (0,2)
        and :recol_list like '%;' || ecolumn || ';%'
        and fromdate >= :rfromdate
        and todate <= :rtodate
      into :eabsref;

    if (eabsref < 0) then exit;
  end

  select first 1 a2.employee, a2.ecolumn, a2.fromdate, a2.todate
    from eabsences a
    join eabsences a2 on (a.employee = a2.employee
                          and a.ecolumn = a2.ecolumn
                          and (a2.correction in (0,2) or :eabscard = 1)
                          and (a2.fromdate >= :rfromdate or :rfromdate is null)
                          and (a2.todate <= :rtodate or :rtodate is null)
                          and (a.todate + 1) >= a2.fromdate
                          and a.fromdate <= (a2.todate + 1)
                          and (a2.scode = :rscode or :rscode = 0)
                          and ((a2.bcode = :rbcode or :rbcode = 0) or :eabscard = 1)
                          and (coalesce(a2.certserial,'') = :rcertserial or :rcertserial is null)
                          and (coalesce(a2.certnumber,'') = :rcertnumber or :rcertnumber is null))
    where a.ref = :eabsref and (:recol_list = '' or :recol_list like '%;' || a.ecolumn || ';%')
      and (a.correction in (0,2) or :eabscard = 1)
    into: empl, :ecolumn, :afromdate, :atodate;

  if (:afromdate is not null) then
  begin
    loop_from = 1;
    loop_to = 1;

    while (loop_from + loop_to > 0) do
    begin
      if (loop_from > 0) then
      begin
        fromdate_temp = null;
        select fromdate
          from eabsences a
          where ((ecolumn = :ecolumn and :recol_list = '') or :recol_list like '%;' || ecolumn || ';%')
            and employee = :empl
            and todate = (:afromdate - 1)
            and correction in (0,2)
            and (fromdate >= :rfromdate or :rfromdate is null)
            and (todate <= :rtodate or :rtodate is null)
            and (scode = :rscode or :rscode = 0)
            and ((bcode = :rbcode or :rbcode = 0) or :eabscard = 1)
            and (coalesce(certserial,'') = :rcertserial or :rcertserial is null)
            and (coalesce(certnumber,'') = :rcertnumber or :rcertnumber is null)
          into: fromdate_temp;
       if (fromdate_temp is null) then
         loop_from = 0;
       else
         afromdate = fromdate_temp;
     end
     if (loop_to > 0) then
     begin
       todate_temp = null;
       select todate
         from eabsences
         where ((ecolumn = :ecolumn and :recol_list = '') or :recol_list like '%;' || ecolumn || ';%')
           and employee = :empl
           and fromdate = (:atodate + 1)
           and correction in (0,2)
           and (fromdate >= :rfromdate or :rfromdate is null)
           and (todate <= :rtodate or :rtodate is null)
           and (scode = :rscode or :rscode = 0)
           and ((bcode = :rbcode or :rbcode = 0) or :eabscard = 1)
           and (coalesce(certserial,'') = :rcertserial or :rcertserial is null)
           and (coalesce(certnumber,'') = :rcertnumber or :rcertnumber is null)
         into: todate_temp;
       if (todate_temp is null) then
         loop_to = 0;
       else
         atodate = todate_temp;
      end
    end
  end
  suspend;
end^
SET TERM ; ^
