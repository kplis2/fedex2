--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_CHECK_DOC(
      SHIPPINGDOC LISTYWYSD_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable docgroup dokumnag_id;
declare variable vers wersje_id;
declare variable quantitydoc ilosci_mag;
declare variable quantitys ilosci_mag;
declare variable noaddress smallint_id;
declare variable iscorr smallint_id;
declare variable ismwsordcortoreal smallint_id;
begin
  status = 1;
  msg = '';

  select first 1 n.grupasped
    from listywysdpoz p
      left join dokumnag n on (p.dokref = n.ref)
    where p.dokument = :shippingdoc
      and p.doktyp = 'M'
  into :docgroup;

  for
    select p.wersjaref, sum(p.iloscl)
      from dokumnag n
        join dokumpoz p on (n.ref = p.dokument)
        left join wersje w on (w.ref = p.wersjaref)
      where n.grupasped = :docgroup
        and coalesce(n.koryg,0) = 0
        and coalesce(w.usluga,0) <> 1
        and coalesce(p.havefake,0) = 0
        and n.wydania = 1
      group by p.wersjaref
    into :vers, :quantitydoc
  do begin
    if (:quantitydoc is null) then quantitydoc = 0;
    select sum(coalesce(r.iloscmag,0))
      from listywysdpoz p
        left join listywysdroz r on (p.ref = r.listywysdpoz)
      where p.dokument = :shippingdoc
        and p.wersjaref = :vers
    into :quantitys;
    if (:quantitys is null) then quantitys = 0;

    if (:quantitys < :quantitydoc) then
    begin
      status = 0;
      msg = 'Nie wszystko zostało jeszcze spakowane!';
      exit;
      --<<XXX MKD po przeanalizowaniu
       -- status = 2;
      --  msg = 'Nie wszystko zostało jeszcze spakowane! Czy na pewno chcesz zamknac pakowanie i przeslac informacje o spakowanych ilosciach?';
      --  exit;
      --XXX>> MKD
    end
  end

  select first 1 1
    from listywysd l
      left join sposdost s on (l.sposdost = s.ref)
    where l.ref = :shippingdoc
      and s.listywys = 1
      and (coalesce(trim(l.kontrahent),'') = '' or
           coalesce(trim(l.miasto),'') = '' or
           coalesce(trim(l.kodp),'') = '' or
           coalesce(trim(l.adres),'') = '')
  into :noaddress;
  if (coalesce(:noaddress,0) = 1) then
  begin
    status = 0;
    msg = 'Nie podano pełnych danych adresowych!';
    exit;
  end

  select first 1 2
    from listywysd l
      left join sposdost s on (l.sposdost = s.ref)
      left join listywysdroz_opk o on (l.ref = o.listwysd)
      left join listywysdroz r on (o.listwysd = r.listywysd and o.ref = r.opk)
      left join listywysdroz_opk o1 on (o.listwysd = o1.listwysd and o.ref = o1.rodzic)
    where l.ref = :shippingdoc
      and s.listywys = 1
      and coalesce(l.kontrolapak,0) = 1
      and o.rodzic is null
      and r.ref is null
      and o1.ref is null
  into :status;
  if (:status is null) then status = 1;
  if (:status = 0) then
    msg = 'Pozostały puste opakowania';

  execute procedure X_LISTYWYSD_SHOW_RETURN(:shippingdoc)
    returning_values :iscorr;
  if (coalesce(:iscorr,0) > 0) then
  begin
    status = 0;
    msg = 'Nie zwrócono wszystkich towarów. Otwórz okno zwrotów';
  end
end^
SET TERM ; ^
