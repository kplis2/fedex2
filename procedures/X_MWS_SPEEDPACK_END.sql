--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_SPEEDPACK_END(
      LISTWYSD LISTYWYSD_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
    declare variable aktuoperator operator_id;
begin
--procedura sluzy do przeniesienia pozycji z listywysdpoz do listywysdroz
-- w trakcie szybkiego pakowania
-- nie potwierdza ona dokumentow spedycyjnych - robi to neos
    status = 1;
    msg = '';
    execute procedure GET_GLOBAL_PARAM('AKTUOPERATOR')
        returning_values :aktuoperator;
    if (coalesce(aktuoperator, '') = '') then
    begin
        status = 0;
        msg = msg || 'Niezalogowany operator';
    end
    --sprawdzenie czy podano istniejacy dokument spedycyjny
    if (not exists(
        select first 1 ld.ref from listywysd ld where ld.ref = :listwysd
    )) then
    begin
        status = 0;
        msg = msg || 'Brak dokumentu spedycyjnego o ref:'||coalesce(:listwysd,'<brak>');
    end
    if (status = 1) then
    begin
        insert into LISTYWYSDROZ ( LISTYWYSDPOZ, LISTYWYSD, ILOSCMAG, KTM, NAZWAT, PAKOWAL, WERSJAREF)
            select LP.REF, LP.DOKUMENT,  LP.ILOSC, w.ktm, w.nazwat, :aktuoperator, LP.WERSJAREF
                from LISTYWYSDPOZ LP
                    join wersje w on (lp.wersjaref = w.ref)
                where lp.dokument = :listwysd;
    end
  suspend;
end^
SET TERM ; ^
