--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWIAD_GETALL(
      MAILBOX integer,
      FOLDER integer)
  returns (
      UID integer,
      STAN smallint,
      FULLYLOADED smallint)
   as
begin
  for select uid,stan,fullyloaded
    from emailwiad
    where mailbox=:mailbox and folder=:folder and stan<>5 and uid>0
    into :uid,:stan,:fullyloaded
  do begin
    suspend;
  end
end^
SET TERM ; ^
