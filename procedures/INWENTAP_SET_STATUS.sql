--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INWENTAP_SET_STATUS(
      REFINWENTAP integer,
      WERSJAREF integer,
      INWENTA integer,
      DOSTAWA integer,
      ILSTAN integer,
      ILINW numeric(14,4))
  returns (
      STATUS smallint)
   as
  declare variable zpartiami integer;
  declare variable sumilinw numeric(14,4);
begin
  select zpartiami from INWENTA where ref = :inwenta into :zpartiami;
  if(:zpartiami is null) then zpartiami = 0;

  /* Obliczenie sumy pozycji dla inwentury partiami i wersjami */
  if(:zpartiami = 1) then begin
    select sum(ilinw)
      from inwentap
      where wersjaref = :wersjaref and inwenta = :inwenta and dostawa = :dostawa and  ref <> :refinwentap
    into :sumilinw;
  end else begin
    select sum(ilinw)
      from inwentap
      where wersjaref = :wersjaref and inwenta = :inwenta and  ref <> :refinwentap
    into :sumilinw;
  end
  if(:sumilinw is null) then sumilinw = 0;
  sumilinw = :sumilinw + :ilinw; /* W przypadku wywolania procedury z triggera ON DELETE ilinw = 0 */

  /* Status pozycji */
  if (:sumilinw <> :ilstan) then
    status = 2;
  else
    status = 0;

  /* Aktualizacja pozostalych rekordow dla pozycji magazynowej */
  if(:zpartiami = 1) then begin
    update inwentap set status = :status
    where wersjaref = :wersjaref and inwenta = :inwenta and dostawa = :dostawa and ref <> :refinwentap and status <> :status;
  end else begin
    update inwentap set status = :status
    where wersjaref = :wersjaref and inwenta = :inwenta and ref <> :refinwentap and status <> :status;
  end

  suspend;
end^
SET TERM ; ^
