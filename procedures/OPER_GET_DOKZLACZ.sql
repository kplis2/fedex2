--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_GET_DOKZLACZ(
      ZTABLE varchar(40) CHARACTER SET UTF8                           ,
      ZDOKUM integer,
      ZALDOKPLIK varchar(255) CHARACTER SET UTF8                           )
  returns (
      DOKPLIK varchar(255) CHARACTER SET UTF8                           )
   as
declare variable dokplikref integer;
begin
  dokplik = '';
  if(:zaldokplik is null) then zaldokplik = '';
  for select dokplik.ref
    from dokzlacz
    join dokplik on (dokplik.ref=dokzlacz.dokument)
    join dokpliktyp on (dokpliktyp.ref=dokplik.typ)
    where dokzlacz.ztable=:ztable and dokzlacz.zdokum=:zdokum
    and (position(dokpliktyp.nazwa in :zaldokplik)>0 or :zaldokplik='')
    into :dokplikref
  do begin
    if(:dokplik<>'') then dokplik = :dokplik||';';
    dokplik = :dokplik||cast(:dokplikref as varchar(20));
  end
end^
SET TERM ; ^
