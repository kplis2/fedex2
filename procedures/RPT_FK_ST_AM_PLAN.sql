--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_ST_AM_PLAN(
      FYEAR varchar(4) CHARACTER SET UTF8                           ,
      TYP integer,
      COMPANY integer)
  returns (
      AMORTGRP varchar(10) CHARACTER SET UTF8                           ,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      NAME varchar(80) CHARACTER SET UTF8                           ,
      PURCHASEDT timestamp,
      LIQUIDATIONDATE timestamp,
      TARATE numeric(10,5),
      TVALUE numeric(15,2),
      TREDEMPTION numeric(15,2),
      FX integer,
      TAMETOD varchar(3) CHARACTER SET UTF8                           )
   as
declare variable TREDEMPTIONTMP NUMERIC(15,2);
-- nowe
    declare variable N_AMPERIOD INTEGER;
    declare variable N_AMYEAR SMALLINT;
    declare variable N_AMMONTH SMALLINT;
-- koniec nowe
begin
    select max(amyear), max(ammonth)
      from amperiods
      where company = :company and amyear <= :FYEAR
    into :N_AMYEAR, :N_AMMONTH;
    select max(ref)
      from amperiods
      where company = :company and amyear <= :FYEAR
        and amyear = :N_AMYEAR and ammonth = :N_AMMONTH
    into :N_AMPERIOD;
    if (TYP = 0) then BEGIN
      for
        select f.ref, f.symbol, f.name, f.amortgrp, f.purchasedt, f.tameth, f.tarate,
          f.tvalue, coalesce(f.tredemption, 0), f.liquidationdate
          from fxdassets F
          where f.company = :company
          order by f.symbol
        into :fx, :SYMBOL, :NAME, :AMORTGRP, :PURCHASEDT, :TAMETOD, :TARATE,
          :TVALUE, :TREDEMPTION, :LIQUIDATIONDATE
        do begin
          if (TREDEMPTION IS NULL) then
            TREDEMPTION = 0;
-- mowe
          select coalesce(TAX_AMORT,0) from calculate_acc_amortization(:FX, :N_AMPERIOD)
            into :TREDEMPTIONTMP;
-- koniec nowego
          if (TREDEMPTIONTMP IS NOT NULL) then
            TREDEMPTION = TREDEMPTION + TREDEMPTIONTMP;
          suspend;
        end
    END
    else if (TYP = 1) then BEGIN
      for
        select f.ref, f.symbol, f.name, f.amortgrp, f.purchasedt, f.Fameth, f.Farate,
          f.Fvalue, coalesce(f.Fredemption,0), f.liquidationdate
          from fxdassets F
            where f.company = :company
          order by f.symbol
        into :fx, :SYMBOL, :NAME, :AMORTGRP, :PURCHASEDT, :TAMETOD, :TARATE,
          :TVALUE, :TREDEMPTION, :LIQUIDATIONDATE
        do begin
-- mowe
          select coalesce(FIN_AMORT,0) from calculate_acc_amortization(:FX, :N_AMPERIOD)
            into :TREDEMPTIONTMP;
-- koniec nowego
          if (TREDEMPTIONTMP IS NOT NULL) then
            TREDEMPTION = TREDEMPTION + TREDEMPTIONTMP;
          suspend;
        end
    END
end^
SET TERM ; ^
