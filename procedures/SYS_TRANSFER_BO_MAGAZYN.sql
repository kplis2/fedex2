--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_TRANSFER_BO_MAGAZYN as
declare variable s1 varchar(255); /* magazyn */
declare variable s2 varchar(255); /* wersja */
declare variable s3 varchar(255); /* ktm */
declare variable s4 varchar(255); /* ilosc */
declare variable s5 varchar(255); /* cena */
declare variable magazyn varchar(3);
declare variable wersja integer;
declare variable ktm varchar(40);
declare variable ilosc numeric(14,4);
declare variable cena numeric(14,4);
declare variable tmp integer;
declare variable oddzial varchar(10);
declare variable dokument integer;
begin

-- sprawdzanie dlugoscie danych w tabeli expimp
  select count(ref) from expimp where coalesce(char_length(s1),0)>3 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich symboli magazynu.';

  select count(ref) from expimp where coalesce(char_length(s3),0)>40 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich KTM.';

-- import
  for select distinct substring(s1 from 1 for 3) from expimp
  into :magazyn
  do begin

--INSERT DOKUMNAG

    select oddzial from defmagaz where symbol=:magazyn into :oddzial;
    if(oddzial is null) then
      exception universal 'Do magazynu ' ||:magazyn || ' nie jest przypisany oddzial.';

    INSERT INTO DOKUMNAG (MAGAZYN, OKRES, TYP, DATA, ODDZIAL, TOKEN)
    VALUES (:magazyn, '200901', 'BO', '2009-01-01', :oddzial, 9);
    select max(ref) from dokumnag into :dokument;

    for select s1, s2,  s3,  s4,  s5
      from expimp
      where substring(s1 from  1 for  3)=:magazyn
    into :s1, :s2,  :s3,  :s4,  :s5
    do begin

/* magazyn */
--    magazyn = substring(:s1 from  1 for  3);

/* wersja */
      wersja = cast(:s2 as integer);

/* ktm */
      ktm = substring(:s3 from  1 for  40);
      if(not exists (select first 1 1 from towary where ktm=:ktm)) then
        exception universal 'Towar: ' || :ktm || ' nie znajduje sie w bazie.';


/* ilosc */
      s4 = replace(:s4, ',', '.');
      ilosc = cast(:s4 as numeric(14,4));

/* cena */
      s5 = replace(:s5, ',', '.');
      cena = cast(:s5 as numeric(14,2));


-- INSERT
      insert into dokumpoz(dokument, ktm,  wersja,  ilosc,  cena)
                values  (:dokument, :ktm, :wersja, :ilosc,  :cena);
    end
-- akceptacja DOKUMNAG
    update dokumnag set akcept=1 where ref=:dokument;
  end
end^
SET TERM ; ^
