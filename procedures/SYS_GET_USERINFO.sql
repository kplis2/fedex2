--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_GET_USERINFO(
      USR varchar(40) CHARACTER SET UTF8                           ,
      PSW varchar(40) CHARACTER SET UTF8                           ,
      OLDPSW varchar(40) CHARACTER SET UTF8                           ,
      ID varchar(255) CHARACTER SET UTF8                           ,
      LOGININAD varchar(255) CHARACTER SET UTF8                            = 'NO')
  returns (
      USERID varchar(255) CHARACTER SET UTF8                           ,
      USERLOGIN varchar(255) CHARACTER SET UTF8                           ,
      IMIE varchar(255) CHARACTER SET UTF8                           ,
      NAZWISKO varchar(255) CHARACTER SET UTF8                           ,
      UWAGI varchar(1024) CHARACTER SET UTF8                           ,
      SUPERVISOR smallint,
      SUPERVISEGROUP varchar(255) CHARACTER SET UTF8                           ,
      EMAIL varchar(255) CHARACTER SET UTF8                           ,
      GRUPA varchar(1024) CHARACTER SET UTF8                           ,
      SENTE smallint,
      MODULES varchar(1024) CHARACTER SET UTF8                           ,
      APPLICATIONS varchar(255) CHARACTER SET UTF8                           ,
      OTABLE varchar(40) CHARACTER SET UTF8                           ,
      OREF integer,
      MUST_CHANGE smallint,
      DAYS_TO_CHANGE integer,
      LAST_CHANGE timestamp,
      NAMESPACE varchar(40) CHARACTER SET UTF8                           ,
      USERUUID varchar(40) CHARACTER SET UTF8                           ,
      LICENCELIMIT SMALLINT_ID)
   as
declare variable CRYPTEDPSW varchar(255);
declare variable LAYER integer;
declare variable ODDZIAL varchar(255);
declare variable OPERATOR integer;
declare variable COMPANY integer;
declare variable VOIPLOGIN STRING;
declare variable USERIDFROMOPER integer;
begin
  --exception test_break coalesce(:usr,'null')||';'||coalesce(:psw,'null')||';'||coalesce(:oldpsw,'null')||';'||coalesce(:id,'null');

  cryptedpsw = shash_hmac('hex','sha1',:psw,:usr);
--  select s.userpass from s_users s where s.userlogin  = :usr into :cryptedpsw;
  -- zamien haslo starej postaci na nowa postac
  if(:oldpsw<>'' and :oldpsw<>:psw and :oldpsw<>:cryptedpsw) then begin
    if(exists(select first 1 1 from s_users u where u.userlogin=:usr and u.userpass=:oldpsw)) then begin
      update s_users set userpass=:cryptedpsw where userlogin=:usr;
    end
  end

  if(LOGININAD = 'OK') then
  begin
    select o.userid
      from OPERATOR o
      where o.domainlogin = :usr
      into :useridfromoper;
  end
  -- odczytaj dane usera pod warunkiem zgodnosci loginu i hasla
  if(exists(select first 1 1 from s_users u where (u.userlogin = :usr and u.userpass = :cryptedpsw) or (u.userid = :useridfromoper ))) then begin
    if(:id is null or :id='' or :id='0') then begin
      -- odczytaj dane usera ktory autoryzuje wywolanie
      select first 1 u.userid, u.userlogin, u.imie, u.nazwisko, u.uwagi, u.supervisor, u.supervisegroup,
        u.email, u.grupa, u.sente, u.modules, u.applications, u.otable, u.oref,
        u.must_change, u.days_to_change, u.last_change, u.useruuid, u.licencelimit
      from s_users u
      where (u.userlogin = :usr and u.userpass = :cryptedpsw) or (u.userid = :useridfromoper )
      into :userid, :userlogin, :imie, :nazwisko, :uwagi, :supervisor, :supervisegroup,
        :email, :grupa, :sente, :modules, :applications, :otable, :oref,
        :must_change, :days_to_change, :last_change, :useruuid, :licencelimit;
    end else begin
      -- odczytaj dane usera przekazanego przez parametr id
      select first 1 u.userid, u.userlogin, u.imie, u.nazwisko, u.uwagi, u.supervisor, u.supervisegroup,
        u.email, u.grupa, u.sente, u.modules, u.applications, u.otable, u.oref,
        u.must_change, u.days_to_change, u.last_change, u.useruuid, u.licencelimit
      from s_users u
      where u.userid = :id
      into :userid, :userlogin, :imie, :nazwisko, :uwagi, :supervisor, :supervisegroup,
        :email, :grupa, :sente, :modules, :applications, :otable, :oref,
        :must_change, :days_to_change, :last_change, :useruuid, :licencelimit;
    end
    -- ustal biezacy namespace
    if(:supervisor=2) then namespace = 'SENTE';
    else if(:supervisor=1) then namespace = 'ADMIN';
    else namespace = 'USER_'||:userlogin;
    if(not exists(select first 1 1 from s_namespaces where namespace=:namespace)) then begin
      -- jesli nie ma wpisu w s_namespaces to go dodaj
      if(:namespace='SENTE') then layer = 0;
      else if(:namespace='ADMIN') then layer = 1000;
      else layer = 1001;
      insert into s_namespaces(namespace,layer) values (:namespace, :layer);
    end
    -- ustal uuid na podstawie tabeli OPERATOR
    if(:userid is not null) then begin
      select o.uuid,o.oddzial,o.ref,o.voiplogin
        from operator o
        where o.userid = :userid
        into :useruuid,:oddzial,:operator,:voiplogin;
    end
    if(:id is null) then begin
      -- jesli operator nie ma przypisanego oddzialu to wybierz glowny oddzial
      if(:oddzial is null) then
        select first 1 o.oddzial
        from oddzialy o
        where o.glowny=1
        order by o.company
        into :oddzial;
      if(:oddzial is null) then
        exception universal 'Nie ustalono oddzialu domyslnego w trakcie logowania';
      select o.company
        from oddzialy o
        where o.oddzial = :oddzial
        into :company;
      -- jesli jestesmy w trakcie logowaia to ustaw parametry obowiazkowe
      execute procedure SET_GLOBAL_PARAM('AKTUODDZIAL',:oddzial);
      execute procedure SET_GLOBAL_PARAM('AKTUOPERATOR',:operator);
      execute procedure SET_GLOBAL_PARAM('AKTUOPERGRUPY',:grupa);
      execute procedure SET_GLOBAL_PARAM('CURRENTCOMPANY',:company);
      execute procedure SET_GLOBAL_PARAM('GLOBALAPPLICATIONIDENTS',:applications);
      execute procedure SET_GLOBAL_PARAM('VOIPLOGIN',:voiplogin);
    end
    suspend;
  end
end^
SET TERM ; ^
