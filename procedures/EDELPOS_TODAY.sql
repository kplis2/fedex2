--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDELPOS_TODAY(
      DELEGATION integer)
   as
declare variable delegationid integer;
declare variable startdate timestamp;
declare variable enddate timestamp;
declare variable daycount numeric(14,2);
declare variable cnt integer;
declare variable datetemp timestamp;
declare variable dhours numeric(14,2);
declare variable incountry smallint;

declare variable delegationidb integer;
declare variable startdateb timestamp;
declare variable enddateb timestamp;
declare variable daycountb numeric(14,2);
declare variable cntb integer;
declare variable datetempb timestamp;
declare variable dhoursb numeric(14,2);
declare variable incountryb smallint;

begin
/* BS43287: edeldays.ref -> edeldays.daycounter */

---incountry = 1
  select p.delegation, min(p.fromdate), max(p.todate), max(p.todate) - min(p.fromdate),
      (max(p.todate) - min(p.fromdate)) * 24
    from edelpos p
    where :delegation=p.delegation and p.incountry = 1
    group by p.delegation
    into :delegationid, :startdate, :enddate, :daycount, :dhours;

  cnt = 1;
  datetemp = startdate;
  if (not(datetemp is null)) then
  begin
    while (:cnt  <= :daycount)
    do begin
      insert into edeldays(daycounter,delegation, fromdate, hours, incountry)
        values (:cnt, :delegation, cast(:startdate+:cnt-1 as timestamp), 24,1);
      dhours=dhours-24;
      cnt=cnt+1;
    end
    if (dhours > 0) then
    begin
      insert into edeldays(daycounter,delegation, fromdate, hours, incountry)
        values (:cnt,:delegation, cast(:startdate + :cnt - 1 as timestamp), :dhours, 1);
      cnt = cnt + 1;
    end
  end


  ---incountry = 0
  select p.delegation, min(p.fromdate), max(p.todate), max(p.todate) - min(p.fromdate),
      (max(p.todate) - min(p.fromdate)) * 24
    from edelpos p where :delegation=p.delegation and p.incountry = 0 group by p.delegation
    into :delegationidb, :startdateb, :enddateb, :daycountb, :dhoursb;

  cntb = 1;
  datetempb=startdateb;
  if (not(datetempb is null)) then
  begin
    while (cntb  <= :daycountb)
    do begin
      insert into edeldays(daycounter,delegation, fromdate, hours, incountry)
        values (:cntb + :cnt,:delegation, cast(:startdateb+:cntb-1 as timestamp), 24,0);
      dhoursb=dhoursb-24;
      cntb=cntb+1;
    end
    if (:dhoursb>0) then
    begin
      insert into edeldays(daycounter,delegation, fromdate, hours, incountry)
        values (:cntb + :cnt,:delegation, cast(:startdateb + :cntb - 1 as timestamp), :dhoursb, 0);
      cntb=cntb+1;
    end
  end
end^
SET TERM ; ^
