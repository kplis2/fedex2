--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REZ_SET_KTM_FAK(
      REFPOZ integer,
      ZREAL smallint)
   as
declare variable rezstan char(1);
declare variable ilosc numeric(14,4);
declare variable rezil numeric(14,4);
declare variable il numeric(14,4);
declare variable freeil numeric(14,4);
declare variable magazyn char(3);
declare variable cnt integer;
declare variable wydania smallint;
declare variable iltymblok numeric(14,4);
declare variable stan varchar(1);
declare variable fak integer;
declare variable KTM varchar(40);
declare variable WERSJA integer;
declare variable status integer;
declare variable cenamag numeric(14,4);
declare variable dostawa integer;
declare variable usluga smallint;
declare variable fake smallint;
declare variable altposmode smallint;
begin
  status = -2;
  if(:zreal=2) then
  begin
    select p.iloscm - p.piloscm, p.ktm, p.wersja, p.dokument, p.magazyn, p.cenamag, p.dostawa,
        coalesce(p.fake,0), coalesce(t.altposmode,0)
      from pozfak p
        left join pozfak p1 on (p1.ref = p.alttopoz)
        left join towary t on (p1.ktm = t.ktm)
      where p.ref=:refpoz
    into :ilosc, :ktm, :wersja,:fak, :magazyn, :cenamag, :dostawa,
      :fake, :altposmode;

    if (:fake = 1 and :altposmode = 1) then exit;
  end
  else if(:zreal=3) then begin
    select p.ilosc, p.ktm, p.wersja, p.dokument, n.magazyn, p.cena, p.dostawa,
        coalesce(p.fake,0), coalesce(t.altposmode,0)
      from dokumpoz p
        join dokumnag n on (n.ref = p.dokument)
        left join dokumpoz p1 on (p1.ref = p.alttopoz)
        join towary t on (p1.ktm = t.ktm)
      where p.ref=:refpoz
    into :ilosc, :ktm, :wersja,:fak, :magazyn, :cenamag, :dostawa,
      :fake, :altposmode;

    if (:fake = 1 and :altposmode = 1) then exit;
  end
  else exit;
  if(:dostawa=0) then dostawa = null;
  if(:cenamag=0 or (:dostawa is null)) then cenamag = null;
  if(:zreal=2) then begin
    select TYPFAK.ZAKUP
      from NAGFAK
      left join TYPFAK on (NAGFAK.TYP = TYPFAK.symbol)
      where NAGFAK.ref = :fak into :wydania;
    if(:wydania is null)then wydania = 0;
    if(:wydania = 1) then exit;/*bez blokad na zakupach*/
  end else if(:zreal=3) then begin
    select DEFDOKUM.WYDANIA
      from DOKUMNAG
      left join DEFDOKUM on (DOKUMNAG.TYP = DEFDOKUM.symbol)
      where DOKUMNAG.ref = :fak into :wydania;
      if(:wydania is null)then wydania = 0;
      if(:wydania = 0) then exit;/*bez blokad na przyjeciach*/
  end
  select usluga from towary where ktm=:ktm into :usluga;
  if(:usluga=1) then exit; /*bez blokad na uslugi*/
  if(:ilosc is null) then ilosc = 0;
  status = -1;
  /* zmniejszenie ilosci o ilosci na rezerawacjach/blokadach juz zrealizowanych */
/*  select sum(ilosc) from STANYREZ where POZZAM=:refpoz AND ZREAL=:zreal into :il;
  if(:il is null) then il = 0;
  ilosc = ilosc - il;*/
  if(:ilosc < 0) then ilosc = 0;
   /*stan 'Z' to tak na prawd stan bez rezerwacji i blokad, oznaczany w algorytmie jako 'N', co ma tez miejsce przy ilosci zero */
  if(:ktm is null) then exit;
  if(:magazyn is null) then begin
    exit;
  end
  if(:iltymblok is null) then iltymblok = 0;
  if(:wersja is null) then exit;
  stan = 'B';
  rezil = null;
  /* zawsze jeden rekord klucza POZZAM;STAN;ZREAL */
  /*odjecie lub zmiana ilosci, ktore juz sa w danym stanie */
  select ILOSC from STANYREZ where POZZAM=:refpoz AND STATUS=:stan AND ZREAL = :zreal into :rezil;
  if(:rezil is not null) then begin
   if(:ilosc = 0) then
     delete from STANYREZ where POZZAM=:refpoz AND ZREAL = :zreal AND STATUS = :stan;
   else if(:rezil > :ilosc) then begin
     update STANYREZ set ILOSC = :ilosc where POZZAM=:refpoz AND ZREAL=:zreal AND STATUS=:stan;
     ilosc = 0;
   end else
     ilosc = :ilosc - :rezil;
  end
  /* zamiana mozliwych zapisow o innych statusach niz zadany - przejscie statusu  */
  for select STATUS, ILOSC from STANYREZ where POZZAM=:refpoz AND ZREAL=:zreal AND STATUS <> :stan into :rezstan, :rezil do begin
    if(:rezil is null) then rezil = 0;
    if(:ilosc >0 and  :rezil > 0) then begin
      if(:ilosc > :rezil ) then il = rezil;
      else il = ilosc;
      /* zmiana stanu rezerwacjji lub dopisanie do istniejącej i skasowanie tej*/
      cnt = null;
      select count(*) from STANYREZ where STATUS = :stan AND ZREAL = :zreal AND POZZAM=:refpoz into :cnt;
      if(:cnt is null) then cnt = 0;
      if(:stan = 'B') then begin
          /* badanie ilosci dostepnej na magazynie */
          freeil = null;
          execute procedure REZ_GET_FREE_STAN(:magazyn, :ktm, :WERSJA, :cenamag, :dostawa) returning_values :freeil;
          if(:freeil is null) then freeil = 0;
          if(:freeil > 0) then begin
             if(:freeil > :il) then freeil = il;
             if(:cnt > 0) then begin
               if(:freeil = :il) then
                  delete from STANYREZ where STATUS=:rezstan AND POZZAM=:refpoz AND ZREAL=:zreal;
               update STANYREZ set ILOSC = ILOSC + :freeil where STATUS=:stan AND POZZAM=:refpoz AND ZREAL=:zreal;
             end else if(:freeil = :il) then
               update STANYREZ set STATUS=:STAN, ILOSC = :freeil where STATUS=:rezstan AND POZZAM=:refpoz AND ZREAL=:zreal;
             else
               insert into STANYREZ(POZZAM, STATUS, ZREAL, DOKUMMAG, MAGAZYN, KTM, WERSJA, DATA, ILOSC, CENA, DOSTAWA, DATABL)
                 values(:REFPOZ, :stan, :zreal, 0,:magazyn, :ktm, :wersja,current_date,:freeil,:cenamag, :dostawa, current_timestamp(0));
             il = il - freeil;
             ilosc = ilosc - freeil;
          end
          if(:il > 0) then begin
            stan = 'Z';
            cnt = null;
            select count(*) from STANYREZ where STATUS=:stan AND POZZAM=:refpoz AND ZREAL=:zreal into :cnt;
            if(:cnt is null) then cnt = 0;
            if(:cnt > 0 ) then begin
              if(:stan <> :rezstan ) then
               delete from STANYREZ where STATUS=:rezstan AND POZZAM=:refpoz AND ZREAL=:zreal;
              update STANYREZ set ILOSC = /*ILOSC + */:il where STATUS=:stan AND POZZAM=:refpoz AND ZREAL=:zreal;
            end else begin
               update STANYREZ set STATUS=:stan, ILOSC = :il where STATUS=:rezstan AND POZZAM=:refpoz AND ZREAL=:zreal;
            end
          end
      end
    end
  end
  if(:ilosc > 0) then begin
      il = ilosc;
      cnt = null;
      select count(*) from STANYREZ where STATUS = :stan AND ZREAL = :zreal AND POZZAM=:refpoz into :cnt;
      if(:cnt is null) then cnt = 0;
      if(:stan = 'B') then begin
          /* badanie ilosci dostepnej na magazynie */
          execute procedure REZ_GET_FREE_STAN(:magazyn, :ktm, :WERSJA, :cenamag, :dostawa) returning_values :freeil;
          if(:freeil is null) then freeil = 0;
          if(:iltymblok > 0) then begin
            if(:iltymblok > :freeil) then begin
              iltymblok = :iltymblok - :freeil;
              freeil = 0;
            end else begin
              freeil = :freeil - :iltymblok;
              iltymblok = 0;
            end
          end
          if(:freeil > 0) then begin
             if(:freeil > :il) then freeil = il;
             if(:cnt > 0) then
               update STANYREZ set ILOSC = ILOSC + :freeil where STATUS=:stan AND POZZAM=:refpoz AND ZREAL=:zreal;
             else
               insert into STANYREZ( POZZAM, STATUS, ZREAL, DOKUMMAG, MAGAZYN, KTM, WERSJA, DATA, ILOSC, CENA, DOSTAWA, DATABL)
                 values(:REFPOZ, :stan, :zreal, 0,:magazyn, :ktm, :wersja,CURRENT_DATE,:freeil,:cenamag, :dostawa, current_timestamp(0));
             il = il - freeil;
          end
          if(:il > 0) then begin
            stan = 'Z';
            cnt = null;
            select count(*) from STANYREZ where STATUS=:stan AND POZZAM=:refpoz AND ZREAL=:zreal into :cnt;
            if(:cnt is null) then cnt = 0;
            if(:cnt > 0 ) then begin
               update STANYREZ set ILOSC = ILOSC + :il where STATUS=:stan AND POZZAM=:refpoz AND ZREAL=:zreal;
            end else
               insert into STANYREZ( POZZAM, STATUS, ZREAL, DOKUMMAG, MAGAZYN, KTM, WERSJA, DATA, ILOSC, CENA, DOSTAWA, DATABL)
                 values(:REFPOZ, :stan, :zreal, 0,:magazyn, :ktm, :wersja,current_date,:il,:cenamag, :dostawa, current_timestamp(0));
          end
      end
      ilosc = ilosc - il;
  end
  /* usuniecie blokad typu 'N'  */
  delete from STANYREZ  where STATUS='N' AND POZZAM=:refpoz AND ZREAL=:zreal;
  STATUS = 1;
end^
SET TERM ; ^
