--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ECALENDARS_ADD_YEAR(
      COMPANY COMPANIES_ID,
      ECALENDAR ECALENDARS_ID,
      FROMYEAR YEARS_ID = null,
      TOYEAR YEARS_ID = null)
  returns (
      LOGFORMMSG STRING)
   as
declare variable nextyear years_id;
declare variable fromdate date;
declare variable ecalname string20;
declare variable d timestamp;

begin
/*TS Personel: generuje nastepne lata w wybranym kalendarzu. Domyslnie
  dla nastepnego roku. Zwraca wiadomosc do LogForm dla MultiAction.

  Parametry opcjonalne, gdy uzupelnione pola:
   - FROMYEAR > generujemy tylko rok FROMYEAR,
   - FROMYEAR, TOYEAR > generujemy przedzial lat od FROMYEAR do TOYEAR */


  select startdate, name, coalesce(:fromyear,lyear + 1)
    from ecalendars_view
    where company = :company
      and ref = :ecalendar
    into :fromdate, :ecalname, :nextyear;

  if (fromdate is not null) then --ECALENDAR istnieje
  begin

    if (fromdate < nextyear||'-01-01') then
      fromdate = nextyear||'-01-01';

    if (toyear is null) then
      toyear = nextyear;

    while (nextyear <= toyear)
    do begin
      if (not exists(select first 1 1 from ecaldays where calendar = :ecalendar and cyear = :nextyear)) then
      begin
        execute procedure ecal_setpattern(ecalendar,nextyear,fromdate) returning_values d;
        logformmsg = '• '||ecalname||': wygenerowano dni na rok '||nextyear;
      end else begin
        logformmsg = '¤ '||ecalname||': dni na rok '||nextyear||' już istnieją';
      end
      nextyear = nextyear + 1;
      fromdate = nextyear||'-01-01';

      when any do
      begin
        logformmsg = '× '||ecalname||': nie udało się wygenerować dni na rok '||nextyear;
        suspend;
      end
    end
  end else begin
    logformmsg = '× Nie znaleziono kalendarza dla zadanych parametrów!';
  end

  suspend;
end^
SET TERM ; ^
