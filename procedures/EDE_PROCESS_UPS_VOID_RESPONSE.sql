--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_PROCESS_UPS_VOID_RESPONSE(
      EDEDOCSH_REF integer)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
   as
declare variable listwysd integer_id;
declare variable val string10;
begin
    select oref
      from ededocsh
      where ref = :ededocsh_ref
      into :listwysd;

    select val
      from ededocsp
      where ededoch = :ededocsh_ref and name ='Result'
      into :val;

    if (val = 1) then val = '-1';

    update listywysd
      set spedresponse = :val, x_anulowany = 1
      where ref = :listwysd;

    --update listywysdroz_opk set nrpaczkistr = 'ANULOWANO'
      --where listwysd = :listawysd;

   otable = 'LISTYWYSD';
   oref = listwysd;

   suspend;
end^
SET TERM ; ^
