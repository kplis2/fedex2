--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_ALLTRACKING_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable shippingdoc integer;
declare variable shippingdocno varchar(20);
declare variable grandparent integer;
declare variable deliverytype integer;
declare variable accesscode string80;
begin
  select l.ref, substr(l.symbolsped,1,20), l.sposdost
    from listywysd l
    where l.ref = :oref
  into :shippingdoc, :shippingdocno, :deliverytype;

  --sprawdzanie czy istieje dokument spedycyjny
  if(shippingdoc is null) then
    exception EDE_FEDEX 'Brak listy spedycyjnej';

  select k.klucz
    from spedytwys w
      left join spedytkonta k on (w.spedytor = k.spedytor)
    where w.sposdost = :deliverytype
  into :accesscode;

  val = null;
  parent = null;
  params = null;
  id = 0;
  name = 'pobierzStatusyPrzesylki';
  suspend;

  grandparent = :id;

  val = 'PRODUCTION'; --jesli chcesz testowac zmien na TEST
  name = 'serviceUrlType';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = :accesscode;
  name = 'kodDostepu';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = :shippingdocno;
  name = 'numerPrzesylki';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = '0';
  name = 'czyOstatni';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

end^
SET TERM ; ^
