--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_CREATE_DOK_PRZECIW(
      DOKREF integer,
      TYPDOK varchar(3) CHARACTER SET UTF8                           ,
      DATA timestamp,
      DOSTAWA integer,
      POZYCJE integer,
      ACK integer,
      MAGTO varchar(10) CHARACTER SET UTF8                           ,
      CENYZAKUPU smallint,
      BEZPOLACZENIA smallint,
      CENYSPR smallint)
  returns (
      STATUS smallint,
      NEWDOK integer,
      NEWDOKSYM varchar(30) CHARACTER SET UTF8                           ,
      NEWDOKWART numeric(14,2))
   as
declare variable i smallint;
declare variable cena numeric(14,4);
declare variable ilosc numeric(14,4);
declare variable jedno integer;
declare variable wersjaref integer;
declare variable dokrozref integer;
declare variable ddokrozref integer;
declare variable dokpozref integer;
declare variable wydania integer;
declare variable cena_zakn numeric(14,4);
declare variable cnt integer;
declare variable datawazn timestamp;
declare variable paramn1 numeric(14,4);
declare variable paramn2 numeric(14,4);
declare variable paramn3 numeric(14,4);
declare variable paramn4 numeric(14,4);
declare variable paramn5 numeric(14,4);
declare variable paramn6 numeric(14,4);
declare variable paramn7 numeric(14,4);
declare variable paramn8 numeric(14,4);
declare variable paramn9 numeric(14,4);
declare variable paramn10 numeric(14,4);
declare variable paramn11 numeric(14,4);
declare variable paramn12 numeric(14,4);
declare variable params1 varchar(255);
declare variable params2 varchar(255);
declare variable params3 varchar(255);
declare variable params4 varchar(255);
declare variable params5 varchar(255);
declare variable params6 varchar(255);
declare variable params7 varchar(255);
declare variable params8 varchar(255);
declare variable params9 varchar(255);
declare variable params10 varchar(255);
declare variable params11 varchar(255);
declare variable params12 varchar(255);
declare variable paramd1 timestamp;
declare variable paramd2 timestamp;
declare variable paramd3 timestamp;
declare variable paramd4 timestamp;
declare variable dokumpozref integer;
declare variable dokumrozref integer;
declare variable operstr varchar(255);
declare variable oper integer;
declare variable dostawaref integer;
declare variable uwagi varchar(255);
declare variable ref2 integer;
declare variable symbol2 varchar(20);
declare variable gr_vat varchar(5);
begin
   select symbol2, ref2 from DOKUMNAG where ref=:dokref into :symbol2, :ref2;
   if(coalesce(ref2,0) <> 0) then exception universal 'Do dokumentu został już wystawiony dokument przeciwny '||coalesce(symbol2,'');
   STATUS = -1;
   if(:dostawa = 0) then dostawa = null;
   if(:magto is null or :magto='') then
     select mag2 from DOKUMNAG where ref=:dokref into :magto;
   select WYDANIA from DEFDOKUM where SYMBOL = :typdok into :wydania;
   execute procedure GET_GLOBAL_PARAM('AKTUOPERATOR') returning_values :operstr;
   if(:operstr is not null and :operstr<>'')
   then oper = cast(:operstr as integer);
   else oper = NULL;
   execute procedure GEN_REF('DOKUMNAG') returning_values :newdok;
   if(:wydania = 1) then
     insert into DOKUMNAG(REF,MAGAZYN, TYP, DATA, MAG2, REF2, SYMBOL2, WARTOSC2, UWAGI, OPERATOR)
     select :newdok, :magto, :typdok, :data, MAGAZYN, REF, SYMBOL,WARTOSC, UWAGI, :oper
     from DOKUMNAG where REF=:dokref;
   else
     insert into DOKUMNAG(REF,MAGAZYN, TYP, DATA, MAG2, REF2, SYMBOL2, WARTOSC2, DOSTAWCA, DOSTAWA, KLIENT, UWAGI, OPERATOR)
     select :newdok, :magto, :typdok, :data, MAGAZYN, REF, SYMBOL,WARTOSC, DOSTAWCA,DOSTAWA, KLIENT, UWAGI, :oper
     from DOKUMNAG where REF=:dokref;
   i = 1;
   if(:pozycje=1) then begin
     if(:cenyspr=1) then begin
       for select DOKUMPOZ.REF,DOKUMPOZ.WERSJAREF, DOKUMPOZ.ILOSC, DOKUMPOZ.CENANET, DOKUMPOZ.JEDNO, DOKUMPOZ.uwagi, DOKUMPOZ.gr_vat
        from DOKUMPOZ where DOKUMPOZ.dokument = :dokref
        order by DOKUMPOZ.numer
        into :dokumpozref,:wersjaref, :ilosc, :cena, :jedno, :uwagi, :gr_vat
       do begin
         execute procedure GEN_REF('DOKUMPOZ') returning_values :dokpozref;
         execute procedure GET_PARAMS_FOR_DOKUMPOZ(:dokumpozref,NULL)
         returning_values :paramd1,:paramd2,:paramd3,:paramd4,
                          :paramn1,:paramn2,:paramn3,:paramn4,
                          :paramn5,:paramn6,:paramn7,:paramn8,
                          :paramn9,:paramn10,:paramn11,:paramn12,
                          :params1,:params2,:params3,:params4,
                          :params5,:params6,:params7,:params8,
                          :params9,:params10,:params11,:params12;
         insert into DOKUMPOZ(REF,DOKUMENT,NUMER,ORD,WERSJAREF,CENA,ILOSC, JEDNO,
                              PARAMD1,PARAMD2,PARAMD3,PARAMD4,
                              PARAMN1,PARAMN2,PARAMN3,PARAMN4,
                              PARAMN5,PARAMN6,PARAMN7,PARAMN8,
                              PARAMN9,PARAMN10,PARAMN11,PARAMN12,
                              PARAMS1,PARAMS2,PARAMS3,PARAMS4,
                              PARAMS5,PARAMS6,PARAMS7,PARAMS8,
                              PARAMS9,PARAMS10,PARAMS11,PARAMS12,UWAGI,GR_VAT)
         values(:dokpozref,:newdok,:i,1, :wersjaref, :cena, :ilosc, :jedno,
                :paramd1,:paramd2,:paramd3,:paramd4,
                :paramn1,:paramn2,:paramn3,:paramn4,
                :paramn5,:paramn6,:paramn7,:paramn8,
                :paramn9,:paramn10,:paramn11,:paramn12,
                :params1,:params2,:params3,:params4,
                :params5,:params6,:params7,:params8,
                :params9,:params10,:params11,:params12,:uwagi,:gr_vat);
         i = :i + 1;
         uwagi = null;
       end
     end else begin
       for select DOKUMPOZ.REF, DOKUMPOZ.WERSJAREF, DOKUMROZ.REF, DOKUMROZ.ILOSC, DOKUMROZ.CENA, DOKUMPOZ.JEDNO,
          DOKUMROZ.REF, DOSTAWY.DATAWAZN, DOSTAWY.REF, dokumpoz.uwagi, dokumpoz.gr_vat
        from DOKUMPOZ
        join DOKUMROZ on (DOKUMROZ.pozycja = DOKUMPOZ.REF)
        left join DOSTAWY on (DOSTAWY.REF=DOKUMROZ.dostawa)
        where DOKUMPOZ.dokument = :dokref
        order by DOKUMPOZ.numer
        into :dokumpozref, :wersjaref, :dokumrozref, :ilosc, :cena, :jedno,
          :dokrozref, :datawazn, :dostawaref, :uwagi, :gr_vat
       do begin
         execute procedure GEN_REF('DOKUMPOZ') returning_values :dokpozref;
         /* jesli tworze dok. wydania z dok. przyjecia to nie kopiuje parametrow z dostawy dokumroza*/
         if(:wydania=1) then dokumrozref = null;
         execute procedure GET_PARAMS_FOR_DOKUMPOZ(:dokumpozref,:dokumrozref)
         returning_values :paramd1,:paramd2,:paramd3,:paramd4,
                          :paramn1,:paramn2,:paramn3,:paramn4,
                          :paramn5,:paramn6,:paramn7,:paramn8,
                          :paramn9,:paramn10,:paramn11,:paramn12,
                          :params1,:params2,:params3,:params4,
                          :params5,:params6,:params7,:params8,
                          :params9,:params10,:params11,:params12;
         insert into DOKUMPOZ(REF,DOKUMENT,NUMER,ORD,WERSJAREF,CENA,DOSTAWA,ILOSC, JEDNO, DATAWAZN,
                              PARAMD1,PARAMD2,PARAMD3,PARAMD4,
                              PARAMN1,PARAMN2,PARAMN3,PARAMN4,
                              PARAMN5,PARAMN6,PARAMN7,PARAMN8,
                              PARAMN9,PARAMN10,PARAMN11,PARAMN12,
                              PARAMS1,PARAMS2,PARAMS3,PARAMS4,
                              PARAMS5,PARAMS6,PARAMS7,PARAMS8,
                              PARAMS9,PARAMS10,PARAMS11,PARAMS12,UWAGI,GR_VAT)
         values(:dokpozref,:newdok,:i,1, :wersjaref, :cena, :dostawaref, :ilosc, :jedno, :datawazn,
                :paramd1,:paramd2,:paramd3,:paramd4,
                :paramn1,:paramn2,:paramn3,:paramn4,
                :paramn5,:paramn6,:paramn7,:paramn8,
                :paramn9,:paramn10,:paramn11,:paramn12,
                :params1,:params2,:params3,:params4,
                :params5,:params6,:params7,:params8,
                :params9,:params10,:params11,:params12, :uwagi, :gr_vat);
         i = :i + 1;
         uwagi = null;
         update DOKUMROZ set CENATOPOZ = :dokpozref where DOKUMROZ.REF = :dokrozref;
         /* kopiowanie numerów seryjnych przy kopiowaniu pozycji */
         execute procedure COPY_DOKUMSER(:dokumpozref, :dokpozref, :ilosc, 'M','M', 3);
       end
     end
     if(:cenyzakupu = 1) then begin
       for select REF, WERSJAREF, CENA
       from DOKUMPOZ where DOKUMENT = :newdok
       into :dokpozref, :wersjaref, :cena
       do begin
         cnt = null;
         select count(*) from DOKUMPOZ where DOKUMENT = :newdok and CENA > :cena and WERSJAREF = :wersjaref into :cnt;
         if(:cnt is null or (cnt = 0)) then begin
           cena_zakn = 0;
           select CENA_ZAKN from WERSJE where REF=:wersjaref into :cena_zakn;
           if(:cena_zakn is null) then cena_zakn = 0;
           if(:cena <> :cena_zakn) then
             update DOKUMPOZ set DOKUMPOZ.obliczcenzak = 1 where REF=:dokpozref;

         end
       end
     end
   end
   if(:ack > 0) then begin
     update DOKUMNAG set DOKUMNAG.akcept = 1 where REF=:newdok;
   end
   if(:bezpolaczenia = 1) then
     update DOKUMNAG set REF2=null, SYMBOL2='' where REF=:newdok;
   select DOKUMNAG.symbol, DOKUMNAG.wartosc from DOKUMNAG where REF=:newdok into :newdoksym, :newdokwart;
   status = 1;
end^
SET TERM ; ^
