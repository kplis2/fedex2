--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGZAM_TERMSTARTDOST(
      ZAM integer)
   as
declare variable sstart timestamp;
declare variable dost timestamp;
declare variable declstart timestamp;
declare variable decldost timestamp;
declare variable termdost timestamp;
declare variable wydania integer;
declare variable data timestamp;
declare variable techjedn numeric(14,6);
declare variable stechjednq varchar(100);
declare variable techjednq numeric(14,6);
declare variable worktime real;
begin
  /* Procedura okrela TERMDOST i TERMSTART danego zamóienai na podstawie kalkuaci dostpnoci towarów /surowców oraz operacji na harmonogramie domysnym danego 

zamówienia*/
  --najpierw sprawdzenie czasow na przewodnikach
  select typzam.wydania, nagzam.termstartdecl, nagzam.termdostdecl, nagzam.termdost, nagzam.datazm, nagzam.techjednquantity
    from TYPZAM join NAGZAM on (NAGZAM.typzam = typzam.symbol)
    where nagzam.ref = :zam
    into :wydania, :declstart, :decldost, :termdost, :data, techjedn;
  --obsluga tylko dla zlecen produkcyjnych
  if(:wydania <> 3) then exit;
  if(:techjedn is null) then techjedn = 1;
  execute procedure get_config('TECHJEDNQUANTITY',0) returning_values :stechjednq;
  if(:stechjednq <> '') then
    techjednq = :stechjednq;
  if(:techjednq is null or (:techjednq = 0))then techjednq = 1;
  worktime = 1/:techjednq*:techjedn;--czas to ilosc jednostek * czas jednostki, 1 - to jeden dzien
  select prschedzam.starttime, prschedzam.endtime
    from PRSCHEDZAM join prschedules on (prschedules.ref = prschedzam.prschedule)
    where prschedzam.zamowienie = :zam and prschedules.main = 1
  into :sstart,  :dost;
  if(:sstart is null and :dost is null) then begin
    --wyliczenie na podstawie czasow deklarowanych
    if(:decldost is not null) then begin
      dost = :decldost;
      sstart = cast(:dost - :worktime as timestamp);
      if(:sstart > :declstart) then
        sstart = :declstart;
    end else if(:declstart is not null) then begin
      sstart = :declstart;
      dost = cast(:sstart + :worktime as timestamp);
      if(:dost < :decldost) then
       dost = :decldost;
    end
  end else if(:dost is not null and :sstart is null) then
      sstart = cast(:dost - :worktime as timestamp);
  else if(:sstart is not null and :dost is null) then
    dost = cast(:sstart + :worktime as timestamp);
  if(:dost is null) then dost = :data;
  if(:sstart is null) then sstart = :data;
  if(:sstart >= :dost) then sstart = cast(:dost - :worktime as timestamp);
  update NAGZAM set TERMSTART = :sstart, TERMDOST = :dost where ref=:zam;

end^
SET TERM ; ^
