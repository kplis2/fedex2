--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DEVIDE_DECREEMATCHINGS(
      DREF DECREEMATCHINGS_ID,
      NEWCREDIT CT_AMOUNT,
      NEWDEBIT DT_AMOUNT)
   as
declare variable oldcredit ct_amount;
declare variable olddebit dt_amount;
declare variable drref decrees_id;
begin
  if (:newcredit is null) then newcredit = 0;
  if (:newdebit is null) then newdebit = 0;
  select m.credit, m.debit, m.decree
    from decreematchings m
    where m.ref = :dref
    into :oldcredit, :olddebit, :drref;
  if ((:oldcredit > 0 and :newdebit > 0) or (:olddebit > 0 and :newcredit > 0)) then
    exception decreematching_error 'Nie można zmienić strony Wn/Ma przy podziale.';
  if (:newcredit > 0 and :newdebit > 0) then
    exception decreematching_error 'Nie można podać kwoty Wn i Ma.';
  if (:newcredit > :oldcredit) then
    exception decreematching_error 'Kwota Ma jest wyższa niż poprzednia.';
  if (:newdebit> :olddebit) then
    exception decreematching_error 'Kwota Wn jest wyższa niż poprzednia.';
  if (:newcredit = 0 and :newdebit = 0) then
    exception decreematching_error 'Kwota Wn/Ma = 0.';
  if (:newcredit < 0 or :newdebit < 0) then
    exception decreematching_error 'Kwota Wn/Ma mniejsza niż 0.';
  update decreematchings m set m.credit = m.credit - :newcredit, m.debit = m.debit - :newdebit
    where m.ref = :dref;
  execute procedure insert_decreematching(:drref,:newcredit,:newdebit,1);
end^
SET TERM ; ^
