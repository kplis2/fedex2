--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FR_CALCULATE_REPORT(
      FRVHDR integer)
   as
declare variable COMPANY integer;
declare variable FRVERSION integer;
declare variable PERIOD varchar(6);
declare variable FRHDR varchar(20);
declare variable FRPSN integer;
declare variable SYMBOL varchar(15);
declare variable ALG integer;
declare variable COLREF integer;
declare variable COL integer;
declare variable FORBO integer;
declare variable FORPERIOD varchar(6);
declare variable FIRST_COLREF integer;
declare variable ALGTYPE integer;
declare variable TEMPCOL integer;
declare variable STATEMENT_TEXT varchar(255);
declare variable RET numeric(14,2);
declare variable AMOUNT numeric(14,2);
declare variable RATIO integer;
declare variable FRVPSN integer;
declare variable ROWSYMBOL varchar(20);
declare variable FUNCTIONNAME varchar(20);
declare variable DECL varchar(8191);
declare variable BODY varchar(8191);
begin
  select period, frhdr, company, frversion from frvhdrs where ref = :frvhdr
    into :period, :frhdr, :company, :frversion;

delete from frftempvals;

  for
    select P.ref, P.symbol, P.algorithm, C.ref, C.number, C.forbo
      from frpsns P
        join frcols C on (C.frhdr = P.frhdr and P.frhdr=:frhdr)
      order by P.countord, P.number, C.number
      into :frpsn, :symbol, :alg,  :colref, :col, :forbo
  do begin
    ret = null;

    if(not exists (select ref from frvpsns where frvhdr = :frvhdr and frpsn = :frpsn and frcol = :colref)) then
      insert into frvpsns (frvhdr, symbol, col, frpsn, frcol)
        values (:frvhdr, :symbol, :col, :frpsn, :colref);

    select ref from frvpsns where frvhdr = :frvhdr and frpsn = :frpsn and frcol = :colref
      into :frvpsn;

    if (col = 1) then
      first_colref = colref;

    if (forbo = 1) then
      forperiod = substring(period from 1 for 4) || '00';
      else
        forperiod = period;
-- wiersz pusty   - w zasadzie niepotrzebne bo ret i tak nulluje, ale warto zostawić komentarz - LG -
--    if (alg = 0) then
--      ret = null;
-- recznie PR26662
    if (alg = 1) then begin
      select amount from frvmanualpos fp
      join frvmanualls fm on (fp.frvmanuall = fm.ref)
       where fm.period = :period and fm.frhdr = :frhdr and fp.frpsn = :frpsn
       and fp.frcol = :colref
       into :ret;
    end
-- wyrażenie
    if (alg = 3) then
      if (exists(select frpsn from frpsnsalg where frpsn = :frpsn and frcol = :colref
        and frversion = :frversion)) then
      begin
        select algtype
          from frpsnsalg
          where frpsn = :frpsn and frcol = :colref and frversion = :frversion
          into :algtype;

        if (algtype = 2) then
          tempcol = first_colref;
        else
          tempcol = colref;

        select decl, body
          from frpsnsalg
          where frpsn = :frpsn and frcol = :tempcol and frversion = :frversion
          into :decl, :body;

        if (algtype > 0) then
        begin
          execute procedure STATEMENT_EXECUTE(:decl, :body, :frvpsn, :forperiod, 0)
            returning_values :ret;
          when any
            do exception universal 'Błąd w wykonaniu algorytmu dla pozycji ' || symbol;
        end
      end
 --suma wierszy
    if (alg = 2) then
    begin
      ret = 0;
      for
        select coalesce(VP.amount, 0), coalesce(S.ratio, 0), P.symbol
          from frsumpsns S join frvpsns VP on (S.sumpsn = VP.frpsn)
          join frpsns P on (VP.frpsn = P.ref)
          where S.frpsn = :frpsn and VP.frcol = :colref and S.frversion = :frversion
            and VP.frvhdr = :frvhdr
          into :amount, :ratio, :rowsymbol
      do begin
        if (ratio = 0) then
          ratio = -1;
        if (ratio = 1) then
          functionname = '+ POZYCJA';
          else
            functionname = '- POZYCJA';

        ret = ret + amount * ratio;
        insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue)
          values (:frvhdr, :frpsn, :colref, :functionname, null, :rowsymbol, :amount);
      end
    end

    if (ret = 0) then
      ret = null;
    update frvpsns set amount = :ret where ref = :frvpsn;
  end
  ret = null;
end^
SET TERM ; ^
