--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ZALEGLOSCI_NA_DZIEN(
      PSLODEF integer,
      PSLOPOZ integer,
      DATA timestamp)
  returns (
      SLODEF integer,
      SLOPOZ integer,
      SYMBFAK varchar(20) CHARACTER SET UTF8                           ,
      KONTOFK KONTO_ID,
      ZALEGLOSC numeric(14,2))
   as
begin
   for select ROZRACH.SLODEF, ROZRACH.SLOPOZ, ROZRACH.KONTOFK, ROZRACH.SYMBFAK, sum(ROZRACHP.WINIENZL) - sum(ROZRACHP.MAZL)
   from ROZRACH join ROZRACHP on (ROZRACHP.SLODEF=ROZRACH.SLODEF and ROZRACHP.SLOPOZ=ROZRACH.SLOPOZ and ROZRACHP.SYMBFAK=ROZRACH.SYMBFAK and ROZRACHP.KONTOFK=ROZRACH.KONTOFK)
   where ROZRACH.SLODEF = :pslodef and ROZRACH.SLOPOZ = :pslopoz
   and ROZRACH.DATAPLAT < :data and ROZRACHP.DATA < :data
   group by ROZRACH.SLODEF, ROZRACH.SLOPOZ, ROZRACH.KONTOFK, ROZRACH.SYMBFAK
   having sum(ROZRACHP.WINIENZL) - sum(ROZRACHP.MAZL) > 0
   into :slodef, :slopoz, :kontofk, :symbfak, :zaleglosc
   do begin
     suspend;
  end
end^
SET TERM ; ^
