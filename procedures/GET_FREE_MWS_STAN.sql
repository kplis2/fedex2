--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_FREE_MWS_STAN(
      MAGAZYN char(3) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      CENA numeric(14,4),
      DOSTAWA integer)
  returns (
      ILOSC numeric(14,4))
   as
declare variable altposmode smallint;
begin
  select coalesce(t.altposmode,0)
    from wersje w
      join towary t on (w.ktm = t.ktm)
    where w.ref = :wersjaref
  into :altposmode;

  if (:altposmode = 0) then
    execute procedure xk_mws_check_avquantity(:wersjaref, :dostawa, :magazyn)
      returning_values :ilosc;
  else
    execute procedure mws_calc_kpl_quantity(:magazyn, :wersjaref, :dostawa)
      returning_values :ilosc;
end^
SET TERM ; ^
