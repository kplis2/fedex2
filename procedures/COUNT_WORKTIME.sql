--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COUNT_WORKTIME(
      WORKSTART time,
      WORKEND time,
      WORKSTART2 time,
      WORKEND2 time,
      DAYKIND smallint = 1)
  returns (
      WORKTIME integer)
   as
  declare variable worktime2 integer;
begin
  --DS: wyliczenie czasu pracy
  --86400 sekund = 24h
  worktime = workend - workstart;
  --jesli workstart = workend i mamy swieto lub wolne, to nie ma potrzeby juz sprawdzac reszty
  if (worktime = 0 and daykind <> 1) then exit;

  --praca co najmniej 24h
  if(worktime <= 0) then
    worktime = 86400 + worktime;

  --poniewaz domyslnie mamy czas 00:00-00:00, traktowane jest to jako 0 a nie 24 godziny
  -- <= i < uzyte z premedytacja
  if (workend2 is not null) then
  begin
    worktime2 = workend2 - workstart2;
    if(worktime2 < 0) then
      worktime2 = 86400 + worktime2;
    worktime = worktime + worktime2;
  end

  suspend;
end^
SET TERM ; ^
