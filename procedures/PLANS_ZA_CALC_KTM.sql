--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PLANS_ZA_CALC_KTM(
      KTM varchar(80) CHARACTER SET UTF8                           ,
      ODDATY timestamp,
      DODATY timestamp)
  returns (
      VAL numeric(14,4))
   as
declare variable planval numeric(14,4);
declare variable realtime numeric(14,2);
declare variable ref integer;
declare variable bdata timestamp;
declare variable ldata timestamp;
declare variable termin integer;
declare variable subref integer;
declare variable ktmmain varchar(80);
declare variable grossquantity numeric(14,2);
begin
  val = 0;
  planval = 0;
  realtime = 0;
  ref = 0;
  ktmmain ='';
/*  select p.realtime, p.ref from prsheets p where p.ktm = :ktm and p.status = 2 into :realtime, :ref;
  if(:ref is not null and :ref>0) then begin
    val = 0;
    if(:realtime is not null and :realtime > 0)then begin
      if(floor(:realtime)<:realtime) then realtime = floor(:realtime) + 1;
      oddaty = :oddaty + :realtime;
      dodaty = :dodaty + :realtime;
    end
*/
    for select p.ktm, pm.grossquantity from prsheets p join prshmat pm on (p.ref = pm.sheet)
      where (pm.subsheet is null or pm.subsheet = 0) and pm.ktm = :ktm
      into :ktmmain, :grossquantity
    do begin
      if(:ktmmain is not null and :ktmmain<>'' and :grossquantity is not null and :grossquantity > 0) then begin
        for select pv.planval*:grossquantity,  pc.bdata, pc.ldata
         from plans p
         join planscol pc on (p.ref = pc.plans)
         join plansrow pr on (p.ref = pr.plans and pr.key1 = :ktmmain)
         join plansval pv on (p.ref = pv.plans and pv.planscol = pc.ref and pv.plansrow = pr.ref)
         where p.planstype like('%PRPLAN%') and
         (pc.bdata >= :oddaty and pc.bdata < :dodaty or pc.ldata > :oddaty and pc.ldata <= :dodaty)
         into :planval, :bdata, :ldata
        do begin
          termin = :ldata - :bdata;
          if(:oddaty>:bdata) then termin = :termin - (:oddaty-:bdata);
          if(:dodaty<:ldata) then termin = :termin - (:ldata-dodaty);
          planval = :planval * termin / (:ldata - :bdata);
          if(:planval < 0) then planval = 0;
          val = :val + :planval;
          if(floor(:val)<val) then val = floor(:val) + 1;
--if(:KTM)exception test_break val
          planval = 0;
        end
      end
      ktmmain = '';
      grossquantity = 1;
    end
  suspend;
end^
SET TERM ; ^
