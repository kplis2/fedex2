--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRDACCOUNTING_CHECK_PRDVERSIONS(
      PRDACCOUNT integer)
   as
declare variable sumdeductamount numeric(14,2);
declare variable sumfirstdeductamount numeric(14,2);
declare variable deductamount numeric(14,2);
declare variable firstdeductamount numeric(14,2);
declare variable fromperiod varchar(8);
declare variable prdversion integer;
begin
  select p.deductamount, p.firstdeductamount
    from prdaccounting P where p.ref=:prdaccount
    into :deductamount, :firstdeductamount;
  --procedura sprawdza, czy wszystkie wersje są zbalansowane
  for select PV.ref, PV.fromperiod
    from PRDVERSIONS PV
    where pv.prdaccounting = :prdaccount
    into :prdversion, :fromperiod
  do begin
    sumdeductamount = 0;
    sumfirstdeductamount = 0;
    select coalesce(sum(pp.deductamount),0), coalesce(sum(pp.firstdeductamount),0)
      from prdaccountingpos pp
      where pp.prdversion = :prdversion
      into :sumdeductamount, :sumfirstdeductamount;
    if (deductamount <> sumdeductamount) then
       exception universal 'Kwota odpisu ('||:deductamount||') nie zgadza się z sumą kwot na kontach odpisu (wersja od '||fromperiod||')';
    if (firstdeductamount <> sumfirstdeductamount) then
       exception universal 'Kwota pierwszego odp. ('||:firstdeductamount||') nie zgadza się z sumą kwot na kontach pierwszego odp.(wersja od '||fromperiod||')';

  end
end^
SET TERM ; ^
