--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_CALC_AVGLOAD(
      SCHEDULE integer,
      LASTOPER integer,
      OPERCOUNT integer)
  returns (
      SCHEDOPER integer,
      CNT integer,
      C integer)
   as
declare variable curtime timestamp;
declare variable schednum integer;
declare variable isoper smallint;
declare variable status smallint;
declare variable tstart timestamp;
declare variable tdepend timestamp;
declare variable tend timestamp;
declare variable machine integer;
declare variable verifiednum integer;
declare variable shoper integer;
declare variable worktime float;
declare variable workplace varchar(20);
declare variable prdepart varchar(20);
declare variable matreadytime timestamp;
declare variable laststatus smallint;
declare variable workload double precision;
declare variable tprevend timestamp;
declare variable rstart timestamp;
declare variable rend timestamp;
declare variable rmachine integer;
declare variable rschedoper integer;
declare variable latenes double precision;
declare variable minlatenes double precision;
declare variable minspace integer;
declare variable round integer;
declare variable minlength double precision;
declare variable rthoper integer;
declare variable tdependr timestamp;
declare variable rstartfwt timestamp;
declare variable tstartfwt timestamp;
declare variable machinecount integer;
declare variable altoper smallint;
declare variable brakmat smallint;
declare variable sql varchar(1024);
declare variable paramd2 double precision;
begin
  c = 0;
  laststatus = 2;
  select PRSCHEDULES.mintimebetweenopers, prschedules.roundstarttime,  prschedules.fromdate, prschedules.prdepart
      from prschedules
      where prschedules.ref = :schedule
      into :minspace, :round, :curtime, :prdepart;
  minlength = 1440;
  minlength = 1/:minlength;
  if(:round > :minspace or (:minspace is null) ) then minspace = :round;
  minlength = :minlength * :minspace;

  if(:lastoper = 0) then begin

    verifiednum = 0;
    --ustawianei poczatkowa readytoharm
    update prschedopers set readytoharm = 0 where schedule = :schedule;
    for select a.ref
      from PRSCHEDOPERS A
--      left join prschedoperdeps on (prschedoperdeps.depto = A.ref)
--      left join prschedopers B on (prschedoperdeps.depfrom = B.REF)
      where A.SCHEDULE = :schedule and A.VERIFIED = 0 and a.activ = 1
        and not exists(select b.ref
          from prschedoperdeps dp
          left join prschedopers b on (dp.depfrom = b.ref)
          where dp.depto = a.ref and b.ref is not null and b.verified = 0 and b.status < 3 and b.activ = 1
        )
        and a.status < 3
      into :schedoper
    do update prschedopers set readytoharm = 1 where ref = :schedoper;
    --wyliczenie obciażenia

    for select max(prschedopers.workplace),  sum(prschedopers.worktime)
      from prschedopers
      where schedule = :schedule and verified = 0
      group by workplace
      into :workplace, :workload
    do begin
--      select count(ref) from prmachines where prworkplace=:workplace into machinecount;
      select count (distinct w.prmachine) from prmachineswplaces w where w.prworkplace = :workplace into :machinecount;
      if (:machinecount=0 or machinecount is null) then machinecount=1;
      paramd2 = cast((:workload/:machinecount) as double precision);
      if (:paramd2 = 0) then
        paramd2 = 0;
      else
        paramd2 = 1/paramd2;
      update prschedopers
        set paramd1 = :workload/:machinecount,
          paramd2 = :paramd2,
          parami1 = (case  when status = 0 then 10 else 1 / cast (status as double precision) end)
      where schedule = :schedule and verified = 0 and WORKPLACE = :workplace;
    end
  end else
    select VERIFIED, STATUS from PRschedopers where ref=:lastoper into :verifiednum, :laststatus;

  cnt = 0;
  if(:curtime is null or (:curtime < current_timestamp(0)) ) then curtime = current_timestamp(0);
  while(:cnt < :opercount and :lastoper >= 0) do begin
    verifiednum = :verifiednum + 1;
    -- dla wszystkich operacji niezweryfikowanych, ktore nie posiadaja niezweryfikowanych operacji poprzedzajacych, okresl czas dostepnosci materialow
/*rth    for select a.ref
      from PRSCHEDOPERS A
        left join prschedoperdeps on (prschedoperdeps.depto = A.ref)
        left join prschedopers B on (prschedoperdeps.depfrom = B.REF and B.verified = 0)
      where A.SCHEDULE = :schedule and A.VERIFIED = 0 --and A.starttime is null --tylko operacje, ktore nie są zharmonogramowane
        and a.statusmat is null
        and B.ref is null--operacje, ktore nie maja niezweryfikowanych poprzednikow
      order by
        A.schedule, A.verified, A.starttime,
        A.paramd1 desc,
        A.status desc,  -- najpierw w trakcei realizacji
        A.schedzamnum, A.GUIDENUM -- zgodnie z priorytetami
*/
    for select first 1 a.ref
      from PRSCHEDOPERS A
      where A.SCHEDULE = :schedule and A.VERIFIED = 0 and a.statusmat is null
        and a.readytoharm = 1 and number=(select first 1 number from prschedopers b where b.guide=a.guide and b.activ=1 order by number)
      order by
        A.schedule, A.verified, A.starttime,
        A.paramd1 desc,
        A.status desc,  -- najpierw w trakcei realizacji
        A.schedzamnum, A.GUIDENUM -- zgodnie z priorytetami
      into :schedoper
    do begin
--    insert into expimp2 (s1) values (:schedoper);
      execute procedure PRSCHED_OPER_CHECKMATERIALSTATE(:schedoper);
    end
    schedoper = null;
    rschedoper = null;
    minlatenes = null;
    --dla kazdejoperacji okrel czas "opoxnienia" startu w stosunku do miejsca, w ktorym mozna posadzic operacje
/*rth    for
      select a.ref, a.Starttime, a.ENDTIME, a.STATUS, a.MACHINE, a.worktime, a.SHOPER, a.materialreadytime, a.workplace
      from PRSCHEDOPERS A
        left join prschedoperdeps on (prschedoperdeps.depto = A.ref)
        left join prschedopers B on (prschedoperdeps.depfrom = B.REF and B.verified = 0)
      where A.SCHEDULE = :schedule and A.VERIFIED = 0 and A.starttime is null --tylko operacje, ktore nie są zharmonogramowane
        and B.ref is null--operacje, ktore nie maja niezweryfikowanych poprzednikow
--      PLAN (JOIN (JOIN (A INDEX (PRSCHEDOPERS_IDX9),PRSCHEDOPERDEPS INDEX (PRSCHEDOPERDEPS_IDX1)),B INDEX (PK_PRSCHEDOPERS)))
--      PLAN SORT (JOIN (JOIN (A INDEX (PRSCHEDOPERS_IDX13),PRSCHEDOPERDEPS INDEX (PRSCHEDOPERDEPS_IDX1)),B INDEX (PK_PRSCHEDOPERS)))
      order by
        A.schedule, A.verified, A.starttime,
        A.paramd1 desc,
        A.status desc,  -- najpierw w trakcei realizacji
        A.schedzamnum, A.GUIDENUM, -- zgodnie z priorytetami
        A.materialreadytime nulls first
*/

/*
    sql = 'select a.ref, a.Starttime, a.ENDTIME, a.STATUS, a.MACHINE, a.worktime, a.SHOPER, a.materialreadytime, a.workplace ';
    sql = sql||'from PRSCHEDOPERS A where A.SCHEDULE = :schedule and A.VERIFIED = 0 and a.readytoharm = 1 ';
    if(exists (select ref from prschedopers where SCHEDULE = :schedule and VERIFIED = 0 and readytoharm = 1 and materialreadytime is not null)) then
    sql = sql ||' and a.materialreadytime is not null ';
    sql = sql ||'order by A.schedule, A.verified, a.readytoharm, A.paramd2 , A.parami1, A.schedzamnum, A.GUIDENUM ';
        --, -- zgodnie z priorytetami
        --A.materialreadytime nulls first

*/
    if(exists (select ref from prschedopers where SCHEDULE = :schedule and VERIFIED = 0 and readytoharm = 1 and materialreadytime is not null)) then
       brakmat = 1;
    else
       brakmat = 0;

    for select a.ref, a.Starttime, a.ENDTIME, a.STATUS, a.MACHINE, a.worktime, a.SHOPER, a.materialreadytime, a.workplace
      from PRSCHEDOPERS A where A.SCHEDULE = :schedule and A.VERIFIED = 0 and a.readytoharm = 1
      and (a.materialreadytime is not null or a.verified=:brakmat)
      order by A.schedule, A.verified, a.readytoharm, A.paramd2 , A.parami1, A.schedzamnum, A.GUIDENUM
      into :schedoper, :tstart,  :tend, :status, :machine, :worktime, :shoper, :matreadytime, :workplace
    do begin
      if(:minlatenes < :minlength) then
        break;  -- jesli ostatni odstep jest wystarczajaco maly, to nei szukam dalej
      tdepend = null;
      --okreslenie minimalnego czasu rozpoczecia
      select max(endtime)
        from prschedopers p
          join prschedoperdeps on (prschedoperdeps.depfrom = p.ref)
        where prschedoperdeps.depto = :schedoper
        into :tdepend;
      if(:tdepend is null) then tdepend = :curtime;
      if(:tstart is null or :tstart < :tdepend )then begin
        tstart = tdepend;
      end
      --sprawdzenie zaleznosci materialowej
      if(:matreadytime > :tstart) then
        tstart  = :matreadytime;
     --sprawdzenie, czy start nie wypad w czasie przerwy pracy - jesli tak, to na poczatek kolejnego czasu
      execute procedure PRSCHED_CALENDAR_FIRSTWORKTIME(:prdepart,:tstart) returning_values
         :tstartfwt;
      if(:workplace is not null) then begin
--        if(:schedoper = 62299) then
--          exception TEST_BREAK 'TU '||:tstart;
        execute procedure prsched_find_machine(:schedoper,:tstartfwt, 1) returning_values :tstart,  :tend, :machine;
        --sprawdzenie opoznienia w stosunku do poprzedzajacej oepracji na danej maszynie
--if (:tstart is null) then exception test_break :tstartfwt;
        tprevend = null;
        select max(prschedopers.endtime)
        from PRSCHEDOPERS
        where schedule = :schedule
          and machine = :machine
          and endtime <= :tstart
          and ref<> :schedoper
        into :tprevend;
--      if(:verifiednum = 44 and :schedoper = 66608) then
--        exception TEST_BREAK :tstart ||' '||:tprevend;
        if(:tprevend < :curtime) then
           tprevend = null;--jesli poprzednie operacje już si zakonczyly/powinny byly zakonczyc, to tak, jakby ich nie bylo
        if(:tprevend is null) then latenes = 0;
        else latenes = :tstart - :tprevend;
--        if(:schedoper = 61722) then begin
--          exception test_break :tprevend;
--          exception TEST_BREAK 'OP '||:tstart||' '||' '||cast(:latenes * 1440 as numeric(14,4))||' '||cast(:minlatenes * 1440 as numeric(14,4));
--        end
        if(:minlatenes is null or (minlatenes > :latenes))then begin
          rschedoper = schedoper;
          rmachine = :machine;
          rstart = :tstart;
          rend = :tend;
          minlatenes = :latenes;
          rstartfwt = :tstartfwt;
--          if(rstartfwt is null) then exception test_break 'totu';
--          if(rschedoper = 62312) then exception test_break tstart||' X '||rstartfwt||' aib';
        end
      end else begin
--        if(rschedoper = 62312) then exception test_break tstart||' X '||rstartfwt||' aibnow';
        machine = null;
        execute procedure prsched_checkoperstarttime(:schedoper, :tstart) returning_values :tstart;
        execute procedure pr_calendar_checkendtime(:prdepart, :tstart, :worktime) returning_values :tend;
        rschedoper = schedoper;
        rmachine = machine;
        rstart = :tstart;
        rstartfwt = :tstart;
        rend = :tend;
        minlatenes = 0;
      end
      c = :c + 1;

    end
    if(:rschedoper is null) then begin
      -- zakończenie harmonogramowania operacji
      schedoper = -1;
      exit;
    end
    --szukanie alternatywy tylko dla pierwszej operacji szeregowej
--    if(not exists (select p2.ref from prschedopers p1
--                     left join prschedopers p2 on (p1.guide = p2.guide and p1.ref <> p2.ref and p2.number < p1.number and p1.prshopermaster = p2.prshopermaster and p1.prshoperaltmaster = p2.prshoperaltmaster)
--                     where p1.ref = :rschedoper and p2.ref is not null)
--    ) then
    update prschedopers set verified  = :verifiednum where ref=:rschedoper;
--if (:rschedoper=112731) then exception test_break :minlatenes || ' ' || :minlength;
       select count(ref) from prschedoperdeps where depfrom=(select min(depfrom) from prschedoperdeps where depto=:rschedoper) into :altoper;
       if (:altoper>1) then
          execute procedure prsched_calc_aib1_setalt(:rschedoper, :rstart, :rend, :rmachine, :rstartfwt, :verifiednum) returning_values :rschedoper;
       else
          update prschedopers set STARTTIME = :rstart, ENDTIME = :rend, MACHINE = :rmachine, verified  = :verifiednum where ref=:rschedoper;
    --ustawianie readytoharm na innych
    for select o.ref
      from prschedoperdeps d
      left join prschedopers o on (o.ref = d.depto)
      where d.depfrom = :rschedoper and o.activ = 1
      into :rthoper
    do begin
      execute procedure prsched_calc_readytoharm_check(:rthoper);
    end
    schedoper = rschedoper;
    cnt = :cnt + 1;
  end
end^
SET TERM ; ^
