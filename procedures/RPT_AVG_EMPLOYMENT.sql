--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_AVG_EMPLOYMENT(
      FROMDATE date,
      TODATE date,
      COMPANY integer)
  returns (
      ODDZIAL varchar(10) CHARACTER SET UTF8                           ,
      WYDZIAL varchar(10) CHARACTER SET UTF8                           ,
      NAZWASTAN varchar(40) CHARACTER SET UTF8                           ,
      ZATRPOCZ numeric(14,4),
      ZATRKON numeric(14,4),
      AVG_ZATR numeric(14,4),
      ZATRPOCZ_PERSON integer,
      ZATRKON_PERSON integer,
      AVG_ZATR_PERSON numeric(14,4))
   as
declare variable ETAT numeric(14,4);
declare variable STAN integer;
declare variable CURR_DATE date;
declare variable PERSON integer;
begin
  for
    select distinct em.branch, em.department, em.workpost, w.workpost
      from employment em
        join employees e on (em.employee = e.ref)
        left join edictworkposts w on w.ref = em.workpost
      where e.company = :company
        and em.fromdate <= :todate and (em.todate >= :fromdate or em.todate is null)
      into :oddzial, :wydzial, :stan, :nazwastan
  do begin
--zatrudnienie poczatkowe
    select sum(em.workdim), count(distinct e.person)
      from employment em join employees e on (em.employee = e.ref)
      where e.company = :company and em.branch = :oddzial
        and em.department = :wydzial and em.workpost = :stan
        and em.fromdate <= :fromdate and (em.todate >= :fromdate or (em.todate is null))
      into :zatrpocz, :zatrpocz_person;
    zatrpocz = coalesce(zatrpocz,0);
    zatrpocz_person = coalesce(zatrpocz_person,0);
        
--zatrudnienie koncowe
    select sum(em.workdim), count(distinct e.person)
      from employment em join employees e on (em.employee = e.ref)
      where e.company = :company and em.branch = :oddzial
        and em.department = :wydzial and em.workpost = :stan
        and em.fromdate <= :todate and (em.todate >= :todate or (em.todate is null))
      into :zatrkon, :zatrkon_person;
    zatrkon = coalesce(zatrkon,0);
    zatrkon_person = coalesce(zatrkon_person,0);

--srednie zatrudnienie
    avg_zatr = 0; avg_zatr_person = 0;
    curr_date = fromdate;
    while (curr_date <= todate) do
    begin
      etat = null; person = null;
      select sum(em.workdim), count(distinct e.person)
        from employment em
          join employees e on (em.employee = e.ref)
        where em.fromdate <= :curr_date
          and (em.todate >= :curr_date or em.todate is null)
          and em.branch = :oddzial and em.department = :wydzial
          and em.workpost = :stan
        into :etat, :person;
      avg_zatr = avg_zatr + coalesce(etat,0);
      avg_zatr_person = avg_zatr_person + coalesce(person,0);
      curr_date = curr_date + 1;
    end
    avg_zatr = avg_zatr / (todate - fromdate + 1);
    avg_zatr_person = avg_zatr_person / (todate - fromdate + 1);
    suspend;
  end
end^
SET TERM ; ^
