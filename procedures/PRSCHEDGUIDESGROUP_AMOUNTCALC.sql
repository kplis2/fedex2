--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDESGROUP_AMOUNTCALC(
      PRNAGZAM integer,
      PRPOZZAM integer,
      PRSCHEDZAMDECL integer)
   as
declare variable NAGZAMAMOUNT numeric(14,4);
declare variable PSGAMOUNT numeric(14,4);
declare variable AMOUNTDIFF numeric(14,4);
declare variable BATCHQUANTITY numeric(14,4);
declare variable PRSHEETDESCRIPT varchar(255);
declare variable PRSCHEDGUIDE integer;
declare variable PRSCHEDZAM integer;
declare variable PRSCHEDPOZZAM integer;
declare variable PRSCHEDULESYMBOL varchar(20);
declare variable CURPOZZAM integer;
begin
  if (prpozzam is null) then
    select n.kilosc, p.batchquantity, p.descript
      from nagzam n
      left join prsheets p on (n.prsheet = p.ref)
      where n.ref = :prnagzam
    into :nagzamamount, :batchquantity, :prsheetdescript;
  else
    select p.ilosc, s.batchquantity, s.descript
      from pozzam p
      left join prsheets s on (p.prsheet = s.ref)
      where p.ref = :prpozzam
    into :nagzamamount, :batchquantity, :prsheetdescript;
  if(:batchquantity is null or :batchquantity = 0) then
    exception prschedguides_error substring('Brak wskazania ilości partii dla karty: '||:prsheetdescript from 1 for 55);
  if(nagzamamount is null) then nagzamamount = 0;

  for select p.ref, ps.symbol, pz.ref
    from prschedzam p
    left join prschedules ps on (p.prschedule = ps.ref)
    left join nagzam n on p.zamowienie = n.ref
    left join pozzam pz on pz.zamowienie = n.ref
    where (:prschedzamdecl is null and p.zamowienie = :prnagzam or :prschedzamdecl is not null and p.ref = :prschedzamdecl) and pz.out = 0 and
      (:prpozzam is null or pz.ref = :prpozzam)
    into :prschedzam, :prschedulesymbol, :curpozzam
  do begin
    update prschedguides p set p.pggamountcalc = 1 where p.prschedzam = :prschedzam and p.pozzam = :curpozzam;
    select sum(amount) from prschedguides where prschedzam = :prschedzam and pozzam = :curpozzam into :psgamount;
    if(psgamount is null) then psgamount = 0;
   --podbicie przewodników
    amountdiff = nagzamamount - psgamount;
    if(amountdiff > 0) then begin
      while(amountdiff > 0) do begin
        prschedguide = null;
        select min(p.ref) from prschedguides p where prschedzam = :prschedzam and p.pozzam = :curpozzam
          and p.status = 0 and p.amount < p.amountdecl into :prschedguide;
        if(prschedguide is not null) then begin
          select p.amountdecl - p.amount from prschedguides p where p.ref = :prschedguide into :psgamount;
          if(:amountdiff > :psgamount) then begin
            amountdiff = :amountdiff - :psgamount;
          end else begin
            psgamount = :amountdiff;
            amountdiff = 0;
          end
          update prschedguides p set p.amount = p.amount + :psgamount where p.ref = :prschedguide;
        end else begin
          execute procedure prsched_add_zamguide(:prschedzam, :prpozzam, :amountdiff);
          amountdiff = 0;
        end
      end
    end else if(amountdiff < 0) then begin
--obniżenie przewodników
      amountdiff = - amountdiff;
      while(amountdiff > 0) do begin
        prschedguide = null;
        select max(p.ref) from prschedguides p
          where prschedzam = :prschedzam and p.pozzam = :curpozzam
            and p.status = 0 and p.amount > p.amountzreal
          into :prschedguide;
        if(prschedguide is not null) then begin
          select p.amount - p.amountzreal from prschedguides p where p.ref = :prschedguide into :psgamount;
          if(:amountdiff > :psgamount) then begin
            amountdiff = :amountdiff - :psgamount;
          end else begin
            psgamount = :amountdiff;
            amountdiff = 0;
          end
          update prschedguides p set p.amount = p.amount - :psgamount where p.ref = :prschedguide;
        end else begin
          amountdiff = 0;
          exception prschedguides_error substring('Nie można zmniejszyć o taką ilość przew. na harm. '||prschedulesymbol from 1 for 55);
        end
      end
      delete from prschedguides p where p.prschedzam = :prschedzam and p.amount = 0 and p.status < 2 and p.pozzam = :curpozzam;
    end
    update prschedguides p set p.pggamountcalc = 0 where p.prschedzam = :prschedzam and p.pozzam = :curpozzam;
  end
end^
SET TERM ; ^
