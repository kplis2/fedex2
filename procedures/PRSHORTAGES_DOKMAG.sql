--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHORTAGES_DOKMAG(
      NAGZAM integer,
      PRSCHEDGUIDE integer,
      PRSCHEDOPER integer)
  returns (
      DOKUMNAGSYMBOLS varchar(255) CHARACTER SET UTF8                           )
   as
declare variable prdepart varchar(20);
declare variable typhar integer;
declare variable ktm varchar(80);
declare variable warehouse varchar(3);
declare variable defdokum varchar(3);
declare variable nagzamid varchar(30);
declare variable dokmagref integer;
declare variable dokpozref integer;
declare variable amount numeric(14,4);
declare variable price numeric(14,2);
declare variable dokumnagsymbol varchar(30);
declare variable prshortage integer;
declare variable prshortagetype varchar(20);
declare variable backasrecource smallint;
begin
  dokumnagsymbols = '';
  if(nagzam = 0) then nagzam = null;
  if(prschedguide = 0) then prschedguide = null;
  if(prschedoper = 0) then prschedoper = null;
  if(nagzam is null or nagzam = 0) then
    select zamowienie from prschedguides where ref = :prschedguide into :nagzam;
  select prdepart, id from nagzam where ref = :nagzam into :prdepart, :nagzamid;
  if(prdepart is null) then exception prshortages_error 'Brak wskazania wydziału na zleceniu produkcyjnym';
  select typhar from prdeparts where symbol = :prdepart into :typhar;
  if(typhar = 0) then begin
    for select pt.symbol ,psh.ktm,  sum(psh.amount - psh.amountzreal), pshd.warehouse, pshd.defdokum, pt.backasresourse
      from prshortages psh
        left join prshortagestypes pt on (pt.symbol = psh.prshortagestype)
        left join prshortagesprdeparts pshd on (pshd.prshortagetype = psh.prshortagestype and pshd.prdepart = psh.prdepart)
      where pshd.warehouse is not null and pshd.defdokum is not null
        and psh.dokumnag is null and psh.dokumpoz is null
        and (psh.amount - psh.amountzreal) > 0 and psh.prnagzam = :nagzam
        and (:prschedguide is null or psh.prschedguide = :prschedguide)
        --and (:prschedoper is null or psh.ref = :prschedoper)
        and (:prschedoper is null or psh.prschedoper = :prschedoper)
        and pt.noamountinfluence = 0
      group by pt.symbol, psh.ktm, pshd.warehouse, pshd.defdokum, pt.backasresourse
      into prshortagetype, :ktm, :amount, :warehouse, :defdokum, :backasrecource
    do begin
      if (backasrecource = 0 or backasrecource is null) then
        select ktm from prschedguides where ref = :prschedguide into :ktm;
      execute procedure CREATE_DOKUMNAG(:warehouse,:defdokum,null,null,null,
        null, :nagzam, :nagzamid, '', null) returning_values :dokmagref;
      price = 0;
      --wycena wedug procedury XK klienta
      execute procedure XK_PRSCHORTAGES_PRICE(PRSCHEDGUIDE,null,prshortagetype,ktm,amount) returning_values price;
      -- wycena standardowa
      if (price = 0 or price is null) then
      begin
        select max(dp.cenasr)
          from dokumnag d
          left join dokumpoz dp on (d.ref = dp.dokument)
          where d.skad = 4 and d.dokumentmain = :prschedguide and dp.ktm = :ktm
        into :price;
      end
      execute procedure GEN_REF('DOKUMPOZ') returning_values :dokpozref;
      insert into DOKUMPOZ(REF,DOKUMENT, NUMER, KTM, WERSJA, ILOSC, CENA)
      values(:dokpozref, :dokmagref, 1, :ktm, 0, :amount, :price);
      -- automatyczna akceptacja
      update dokumnag set akcept = 1 where ref = :dokmagref;
      if(coalesce(char_length(dokumnagsymbols),0)<220) then begin -- [DG] XXX ZG119346
        select symbol from dokumnag where ref = :dokmagref into :dokumnagsymbol;
        dokumnagsymbols = dokumnagsymbols||dokumnagsymbol||'; ';
      end
      --update prshortages set dokumnag = :dokmagref, dokumpoz = :dokpozref where ref = :prshortage;
      update prshortages p set dokumnag = :dokmagref, dokumpoz = :dokpozref
        where p.ref in (select psh.ref from prshortages psh
            left join prshortagestypes pt on (pt.symbol = psh.prshortagestype)
            left join prshortagesprdeparts pshd on (pshd.prshortagetype = psh.prshortagestype and pshd.prdepart = psh.prdepart)
          where pshd.warehouse is not null and pshd.defdokum is not null
            and psh.dokumnag is null and psh.dokumpoz is null
            and (psh.amount - psh.amountzreal) > 0 and psh.prnagzam = :nagzam
            and (:prschedguide is null or psh.prschedguide = :prschedguide)
            and (:prschedoper is null or psh.prschedoper = :prschedoper)
            and pt.noamountinfluence = 0
            and pt.backasresourse = :backasrecource);
    end
  end
  suspend;
end^
SET TERM ; ^
