--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNOTP_ACK(
      NOTPOZ integer,
      ACK smallint)
   as
declare variable ROZREF integer;
declare variable OLDCEN numeric(14,4);
declare variable TYMCEN numeric(14,4);
declare variable NEWCEN numeric(14,4);
declare variable CENANOTZAKUPU numeric(14,4);
declare variable DRCEN numeric(14,4);
declare variable DRDOST integer;
declare variable DRILOSC numeric(14,4);
declare variable DOKNOTREF integer;
declare variable WYDANIA smallint;
declare variable GRUPA integer;
declare variable KORREF integer;
declare variable MAGAZYN varchar(5);
declare variable MAG1 varchar(5);
declare variable MAG2 varchar(5);
declare variable TYP varchar(5);
declare variable DATADOK timestamp;
declare variable WERSJAREF integer;
declare variable KTM varchar(40);
declare variable WERSJA integer;
declare variable CNT integer;
declare variable CENATOROZ integer;
declare variable CENATOROZIL numeric(14,4);
declare variable AKTULOCAT integer;
declare variable STANMAGCHANGED numeric(14,4);
declare variable STANONSTANCEN numeric(14,4);
declare variable ROZILOSC numeric(14,4);
declare variable DOKUMNOTPREF integer;
declare variable ILOSC numeric(14,4);
declare variable REFCLONE integer;
declare variable DOKUMNOTPILOSC numeric(14,4);
declare variable DOKUMNOTPCLONE integer;
declare variable MAG_TYP char(1);
declare variable WART numeric(14,2);
declare variable REFSC integer;
declare variable PRZECENA smallint;
declare variable DATADOKT date;
declare variable DNPWARTOSC numeric(14,4);
declare variable POZREF integer;
declare variable A varchar(1024);
declare variable B varchar(1024);
declare variable C integer;
declare variable D integer;
declare variable NSYMBOL varchar(40);
declare variable NDATA date;
declare variable DOKUMROZEXISTS integer;
begin
  select DOKUMNOTP.dokument, DOKUMNOTP.dokumrozkor, dokumnotp.cenaold, dokumnotp.cenanew, dokumnotp.stanmagchanged,
    dokumnotp.ilosc, dokumnotp.ktm, dokumnotp.wersjaref,
      cast(cenanew * ilosc as numeric(14,2)) - cast(cenaold * ilosc as numeric(14,2))
    from DOKUMNOTP
    where ref = :notpoz
    into :doknotref, :rozref, :oldcen, :newcen, :stanmagchanged,
    :ilosc, :ktm, :wersjaref,
    :dnpwartosc;
  select wersje.nrwersji from wersje where wersje.ref=:wersjaref into :wersja;
  select defdokum.wydania, dokumnot.grupa, DOKUMNOT.DATA, DOKUMNOT.MAGAZYN, defdokum.przecena
  from DEFDOKUM join DOKUMNOT on(DOKUMNOT.TYP =DEFDOKUM.SYMBOL)
  where dokumnot.ref = :doknotref into :wydania, :grupa, :datadok, :magazyn, :przecena;
  select defmagaz.typ
  from DEFMAGAZ where symbol = :magazyn into :mag_typ;
  if(:wydania=1 and :mag_typ='S') then exception DOKUMNOT_AKC_EXPT 'Nota kor. rozchodowa dla magazynu cen srednich';
  if((:ack = 0) or (:ack = 7)) then begin
    /*zamiana cen do zamiany*/
    tymcen = oldcen;
    oldcen = newcen;
    newcen = tymcen;
  end
  datadokt = datadok;

  if(:przecena=1 and :mag_typ='S') then begin /*przecena na mag cen sr biez*/
    wart = cast(:newcen*:ilosc as numeric(14,2)) - cast(:oldcen*:ilosc as numeric(14,2));
    select max(ref) from STANYCEN where MAGAZYN = :magazyn and KTM =:ktm and WERSJA=:wersja and DOSTAWA=0 into :refsc;
    if(:refsc>0) then begin
      update STANYCEN set WARTOSC = WARTOSC+:wart where MAGAZYN=:magazyn and KTM = :ktm and WERSJA = :wersja and DOSTAWA = 0;
    end else begin
      execute procedure GEN_REF('STANYCEN') returning_values :refsc;
      insert into STANYCEN(REF,MAGAZYN,KTM,WERSJA,ILOSC,CENA,WARTOSC,DOSTAWA,DATA)
        values (:refsc,:magazyn,:ktm,:wersja,0,:newcen,:wart, 0,:datadok);
    end
    update DOKUMNOTP set AKCEPTPOZ=:ack where ref=:notpoz;
    exit;
  end

  -- mamy 3 przypadki:
  -- 1. akceptacja noty -> rozpiska musi istniec
  -- 2. wycofanie akceptacji noty -> rozpiska istnieje, gdyz po prostu usuwamy noty
  -- 3. wycofanie akceptacji noty -> rozpiska nie istnieje, gdyz jest to wycofanie wz z istniejaca nota
  if(:rozref is null and coalesce(:przecena,0)<>1) then exception DOKUMNOT_BEZDOKUMKOR;  /*nie wskazano rozpiski korygowanej - chyba ze to przecena*/
  dokumrozexists = 1;
  drilosc = null;
  select DOKUMROZ.ilosc, DOKUMROZ.cena, DOKUMROZ.dostawa, DOKUMROZ.cenatoroz,
  DOKUMNAG.SYMBOL, DOKUMNAG.DATA
  from DOKUMROZ
  join DOKUMPOZ on (DOKUMPOZ.REF = DOKUMROZ.POZYCJA)
  left join DOKUMNAG on (DOKUMNAG.REF = DOKUMPOZ.DOKUMENT)
  where DOKUMROZ.REF=:rozref into :drilosc, :drcen, :drdost, :cenatoroz,
  :nsymbol, :ndata;
  -- jesli nie sciagnieto ilosci, to znaczy ze rozpiski w ogole nie ma
  if(:drilosc is null) then dokumrozexists = 0;
  if(:dokumrozexists = 0 and :ack=1 and coalesce(:przecena,0)<>1) then exception DOKUMNOT_BEZDOKUMKOR;

  if((:ack = 0 or :ack = 7) and :dokumrozexists=1 and (:wydania = 0)) then begin
    /*sprawdzenie, czy dany stan cenowy mia późniejsze rozchody po zmienionej cenie*/
    cnt = 0;
    if(exists(select dokumroz.ref
      from DOKUMROZ left join DOKUMPOZ on (DOKUMPOZ.REF = DOKUMROZ.POZYCJA)
                    left join DOKUMNAG on (DOKUMNAG.REF = DOKUMPOZ.DOKUMENT)
                    left join DOKUMNOTP on (DOKUMNOTP.DOKUMROZKOR = DOKUMROZ.REF)
      where DOKUMNAG.DATA > :datadok and DOKUMROZ.MAGAZYN = :magazyn and DOKUMROZ.WERSJA = :wersja and DOKUMROZ.KTM = :ktm and
            DOKUMROZ.cena = :oldcen and DOKUMROZ.DOSTAWA =:drdost and (DOKUMROZ.REF <> :rozref)
            and ((DOKUMNOTP.REF is null) or (DOKUMNOTP.cenanew <> :oldcen))/*czyli ta cena na rozpisce innej zostaa naniesiona przez note - tak to moze byc*/
    )) then cnt = 1;
    if(:cnt > 0 and :ack < 7) then exception DOKUMNOTP_HASROZCHLATER;
  end
  if(:dokumrozexists=1 and :oldcen <> :drcen) then exception DOKUMNOTP_CENYNIEZGODNE 'Niezgodne ceny na '||:nsymbol||' z '||:ndata||'. Na dok:'||:drcen||', na nocie:'||:oldcen;
  /*poprawa dokumroz*/
  if(:dokumrozexists=1 and :ilosc < :drilosc) then begin
    /*deklarowana jest mniejsza ilosc korekty cenowej niz na rozpisce - koniecznosc rozbicia*/
    execute procedure DOKUMROZ_CLONE(:rozref, :ilosc) returning_values :refclone;
    --zmiana innych, jeszcze nie zaakceptowanych pozycji not na rozpiske wyklonowana
    for select dokumnotp.ref from DOKUMNOTP left join dokumnot on (DOKUMNOT.ref = DOKUMNOTP.dokument)
    where dokumnotp.dokumrozkor = :rozref and
    (dokumnot.akcept = 0 or (dokumnot.ref = :doknotref and dokumnotp.akceptpoz=0))
     and dokumnotp.ref <> :notpoz
    into :dokumnotpclone
    do begin
      update DOKUMNOTP set DOKUMnotp.dokumrozkor = :refclone where ref=:dokumnotpclone;
    end
    drilosc = :ilosc;
  end
  if(:dokumrozexists=1) then begin
    update DOKUMROZ set CENA = :newcen, NOTAUPDATE = 1 where REF=:rozref;
    update DOKUMROZ set NOTAUPDATE = 0 where REF=:rozref;
  end

  -- obsluga tabeli STANYARCH
  if (:ack = 0 and :dokumrozexists=1) then begin
    if (wydania = 0) then
      execute procedure STANYARCH_CHANGE('N',:magazyn,:ktm,:wersja,:notpoz,cast(:datadokt as date),-:ilosc,-:dnpwartosc, null);
    else
      execute procedure STANYARCH_CHANGE('N',:magazyn,:ktm,:wersja,:notpoz,cast(:datadokt as date),:ilosc,:dnpwartosc, null);
  end else if (:ack = 1 and :dokumrozexists=1) then begin
    if (wydania = 0) then
      execute procedure STANYARCH_CHANGE('N', :magazyn,:ktm,:wersja,:notpoz,cast(:datadokt as date),:ilosc,:dnpwartosc, null);
    else
      execute procedure STANYARCH_CHANGE('N',:magazyn,:ktm,:wersja,:notpoz,cast(:datadokt as date),-:ilosc,-:dnpwartosc, null);
  end
  /*poprawa korekt ilosciowych*/
  if(:ack = 1 and :mag_typ<>'S') then begin /*nie dziala dla mag cen sr biez*/
    for select ILOSC, REF from DOKUMROZ where kortoroz = :rozref
    into :rozilosc, :korref
    do begin
      select DOKUMNAG.TYP, DOKUMNAG.MAGAZYN, DOKUMNAG.MAG2
      from DOKUMROZ
      left join DOKUMPOZ on (DOKUMPOZ.REF = DOKUMROZ.POZYCJA)
      left join DOKUMNAG on (DOKUMNAG.REF = DOKUMPOZ.DOKUMENT)
      where DOKUMROZ.REF=:korref
      into :typ, :mag1, :mag2;
      -- BS52829 - noty do korekt tylko na tym samym magazynie co nota pierwotna bo nie wolno przeceniac rozchodu
      -- na innym magazynie, bo moze nie przecenimy tez przyjecia i bedzie rozjazd
      if(:magazyn=:mag1) then begin
        execute procedure DOKUMNOTP_CREATENOTPOZ(:grupa, :mag1, :typ, :mag2, :korref, :newcen, :notpoz,null);
        drilosc = :drilosc - :rozilosc;
      end
    end
  end
  if(:wydania = 0 and :mag_typ='S') then begin
    /*dokument przychodowy dla mag cen sr biez*/
    /*poprawa stanu magazynowego o odpowiednia wartosc, nowa cena srednia przelicza sie sama*/
    /*poniewaz przy wycofaniu ceny sa zamieniane, wystarczy ten sam kod do akceptacji i wycofania*/
    wart = cast(:newcen*:ilosc as numeric(14,2)) - cast(:oldcen*:ilosc as numeric(14,2));
    select max(ref) from STANYCEN where MAGAZYN = :magazyn and KTM =:ktm and WERSJA=:wersja and DOSTAWA=0 into :refsc;
    if(:refsc>0) then begin
      update STANYCEN set WARTOSC = WARTOSC+:wart where MAGAZYN=:magazyn and KTM = :ktm and WERSJA = :wersja and DOSTAWA = 0;
    end else begin
      execute procedure GEN_REF('STANYCEN') returning_values :refsc;
      insert into STANYCEN(REF,MAGAZYN,KTM,WERSJA,ILOSC,CENA,WARTOSC,DOSTAWA,DATA)
        values (:refsc,:magazyn,:ktm,:wersja,0,:newcen,:wart, 0,:datadok);
    end
  end
  if(:wydania = 0 and :mag_typ<>'S') then begin
    /*dokument przychodowy dla pozostalych magazynow */
    /*poprawa stanu magazynowego - do wysokosci iloscl, bo korety ilosciowe sie poprawia same*/
    if(:ack = 1) then begin
      cnt = 0;
      stanonstancen = 0;
      select STANYCEN.ILOSC from STANYCEN where MAGAZYN = :magazyn and KTM = :ktm and wersja = :wersja and DOSTAWA = :drdost and cena = :oldcen
        into :stanonstancen;
      if(:stanonstancen < 0) then stanonstancen = 0;
      if(:stanonstancen > :drilosc) then
        stanmagchanged = :drilosc;
      else
        stanmagchanged = :stanonstancen;
      if(exists(select wersja from STANYCEN where MAGAZYN = :magazyn and KTM = :ktm
                  and wersja = :wersja and DOSTAWA = :drdost and cena = :newcen))
      then begin
        /*stan docelowy istnieje - przepisanie*/
        update STANYCEN set ILOSC = ILOSC + :stanmagchanged where MAGAZYN = :magazyn and KTM = :ktm and wersja = :wersja and DOSTAWA = :drdost and cena = :newcen;
        update STANYCEN set ILOSC = ILOSC - :stanmagchanged where MAGAZYN = :magazyn and KTM = :ktm and wersja = :wersja and DOSTAWA = :drdost and cena = :oldcen;
        execute procedure STANYSER_PRZENIES(:stanmagchanged,:magazyn,:ktm,:wersja,:drdost,:oldcen,:newcen);
      end else if(:stanmagchanged < :stanonstancen) then begin
        /*przenoszona ilosc jest mniejsza - nowy stan cenowy*/
        update STANYCEN set ILOSC = ILOSC - :stanmagchanged where MAGAZYN = :magazyn and KTM = :ktm and wersja = :wersja and DOSTAWA = :drdost and cena = :oldcen;
        insert into STANYCEN(MAGAZYN, KTM, WERSJA, CENA, DOSTAWA, ILOSC,
            DATA, DATAFIFO, DATAWAZN, ODDZIAL)
          select MAGAZYN, KTM, WERSJA, :newcen, dostawa, :stanmagchanged,
              DATA, DATAFIFO, DATAWAZN, oddzial
            from STANYCEN
            where MAGAZYN = :magazyn and KTM = :ktm and wersja = :wersja and DOSTAWA = :drdost and cena = :oldcen;
        execute procedure STANYSER_PRZENIES(:stanmagchanged,:magazyn,:ktm,:wersja,:drdost,:oldcen,:newcen);
      end else if(:stanmagchanged>0) then begin
        update STANYCEN set CENA = :newcen where MAGAZYN = :magazyn and KTM = :ktm and wersja = :wersja and DOSTAWA = :drdost and cena = :oldcen and cena <> :newcen;
      end
      drilosc = :drilosc - :stanmagchanged;
      update DOKUMNOTP set stanmagchanged = :stanmagchanged where  ref = :notpoz;
    end else if(:ack = 0) then begin
      cnt = 0;
      stanonstancen = 0;
      select STANYCEN.ILOSC from STANYCEN where MAGAZYN = :magazyn and KTM = :ktm and wersja = :wersja and DOSTAWA = :drdost and cena = :oldcen into :stanonstancen;
      if(exists(select MAGAZYN from STANYCEN where MAGAZYN = :magazyn and KTM = :ktm and wersja = :wersja and DOSTAWA = :drdost and cena = :newcen)) then cnt = 1;
      if(:cnt is null) then cnt = 0;
      if(:stanonstancen is null) then stanonstancen = 0;
      if(:stanmagchanged is null) then stanmagchanged = :stanonstancen;/*zgodnoć ze starym - wycofanie caloci stanu cenowego*/
      if(:stanonstancen < :stanmagchanged) then exception DOKUMNOTP_HASROZCHLATER;
      if(:stanonstancen = :stanmagchanged and cnt = 0) then  begin
        update STANYCEN set CENA = :newcen where MAGAZYN = :magazyn and KTM = :ktm and wersja = :wersja and DOSTAWA = :drdost and cena = :oldcen and cena <> :newcen;
      end else begin
        update STANYCEN set ILOSC = ILOSC - :stanmagchanged where MAGAZYN = :magazyn and KTM = :ktm and wersja = :wersja and DOSTAWA = :drdost and cena = :oldcen;
        if(:cnt > 0) then
          update STANYCEN set ILOSC = ILOSC + :stanmagchanged where MAGAZYN = :magazyn and KTM = :ktm and wersja = :wersja and DOSTAWA = :drdost and cena = :newcen;
        else begin
          insert into STANYCEN(MAGAZYN, KTM, WERSJA, CENA, DOSTAWA, ILOSC)
          values (:magazyn, :ktm, :wersja, :newcen, :drdost, :stanmagchanged);
        end
        execute procedure STANYSER_PRZENIES(:stanmagchanged,:magazyn,:ktm,:wersja,:drdost,:oldcen,:newcen);
      end
    end
    if(:ack = 1) then begin /* nie dziala dla mag cen sr biez*/
      /*poprawa dokumentów rozchodowych za wyjątkeim tych powiązanych z dok. zrodlowym*/
      for select DOKUMROZ.REF, DOKUMROZ.ILOSC, DOKUMNAG.TYP, DOKUMNAG.MAG2
      from DOKUMROZ left join DOKUMPOZ on (DOKUMPOZ.REF = DOKUMROZ.pozycja)
                  left join DOKUMNAG on (DOKUMNAG.REF = DOKUMPOZ.dokument)
      where DOKUMROZ.WYDANIA = 1 and DOKUMROZ.MAGAZYN = :magazyn
        and DOKUMROZ.KTM = :ktm and DOKUMROZ.WERSJA = :wersja
        and DOKUMROZ.CENA = :oldcen and DOKUMROZ.DOSTAWA = :drdost
        and (DOKUMNAG.REFK is null or DOKUMROZ.kortoroz is null or DOKUMROZ.kortoroz<>:rozref)
      order by DOKUMNAG.DATA,DOKUMROZ.REF
      into :korref, :rozilosc, :typ, :mag2
      do begin
        if(:drilosc > 0) then begin
          /*sprawdzenie, czy danej rozpiski rozchodowej nie korygowaa już jaka wczeniejsza rozpiska z tej samej noty*/
          dokumnotpilosc = 0;
          select sum(DOKUMNOTP.ilosc)
            from DOKUMNOTP
              join DOKUMNOT on (DOKUMNOT.REF = DOKUMNOTP.DOKUMENT)
            where DOKUMNOT.GRUPA = :grupa and DOKUMNOT.akcept = 0 and DOKUMNOTP.dokumrozkor = :korref
          into :dokumnotpilosc;
          if(:dokumnotpilosc is null) then dokumnotpilosc = 0;
          if(:dokumnotpilosc  < :rozilosc) then begin
            rozilosc = :rozilosc - :dokumnotpilosc;
            if(:drilosc < :rozilosc) then begin
              rozilosc = :drilosc;
            end
            execute procedure DOKUMNOTP_CREATENOTPOZ(:grupa, :magazyn, :typ, :mag2, :korref, :newcen, :notpoz,:rozilosc);
            drilosc = :drilosc - :rozilosc;
          end
        end
      end
      if(:drilosc > 0) then
        exception DOKUMNOT_AKC_EXPT 'Pozostalo do skorygowania '||:drilosc||' z '||:ktm;
    end
  end
  if(((:ack = 1) or (:ack = 8))and :cenatoroz > 0 and :mag_typ<>'S') then begin /* nie dziala dla mag cen sr biez*/
    /*przlieczenie cen zależnych*/
      execute procedure getconfig('AKTULOCAT') returning_values :aktulocat;
      for select DOKUMROZ.REF, DOKUMNAG.TYP, DOKUMNAG.MAGAZYN, DOKUMNAG.MAG2, DOKUMROZ.ILOSC
      from DOKUMROZ left join DOKUMPOZ on (DOKUMPOZ.REF = DOKUMROZ.pozycja)
                  left join DOKUMNAG on (DOKUMNAG.REF = DOKUMPOZ.dokument)
                  left join DEFDOKUM on (DEFDOKUM.SYMBOL = DOKUMNAG.TYP)
                  left join DEFMAGAZ on (DOKUMNAG.MAGAZYN = DEFMAGAZ.symbol)
                  left join ODDZIALY on (DEFMAGAZ.oddzial = ODDZIALY.ODDZIAL)
      where dokumroz.ref = :cenatoroz and dokumroz.ilosc > 0 and ODDZIALY.location = :aktulocat
      into :korref, :typ, :magazyn, :mag2, :cenatorozil
      do begin
        -- cene wyliczamy jako sume wartosci po notach z dokumentow rozchodowych dzielona przez ilosc na danej pozycji dokumentu przychodowego
        newcen = 0;
        select sum(wartosc) from DOKUMROZ where CENATOROZ = :cenatoroz into :newcen;
        newcen = :newcen / :ilosc;

        -- ponadto do ceny dodajemy noty korygujace cene nabycia wystawione bezposrednio do tej pozycji dokumentu przychodowego
        -- (nie pochodzące z kaskady) Przyklad to dokumenty PZI->WSC->PSC, gdzie powstaja dodatkowe noty do PSC.
        cenanotzakupu = 0;
        select sum(p.cenanew - p.cenaold)
        from dokumnotp p
        left join dokumnot d on (d.ref=p.dokument)
        where p.dokumrozkor=:korref
        and d.ref=d.grupa
        and d.akcept=1
        and d.rodzaj<>-1
        into :cenanotzakupu;
        if(:cenanotzakupu is null) then cenanotzakupu = 0;
        newcen = :newcen + :cenanotzakupu;

        -- moglo sie zdazyc, ze nota zawiera kilka rozpisek kreujacych cene danego CENATOROZ.
        -- Wówczas liczy sie ta nova cena wynikajaca po korekcie ostatniej rozpiski kreujacej cene danej, czyli
        -- nalezy usunac DOKUMNOTP tej rozpiski z poprzednimi wycenami
        dokumnotpref = null;
        select max(DOKUMNOTP.REF) from DOKUMNOTP join DOKUMNOT on (DOKUMNOT.REF = DOKUMNOTP.DOKUMENT) where DOKUMNOT.GRUPA = :grupa and DOKUMNOT.akcept = 0 and DOKUMNOTP.dokumrozkor = :korref
        into :dokumnotpref;
        if(:dokumnotpref > 0) then
          delete from DOKUMNOTP where DOKUMNOTP.ref = :dokumnotpref;
        execute procedure DOKUMNOTP_CREATENOTPOZ(:grupa, :magazyn, :typ, :mag2, :korref, :newcen, :notpoz,null);
      end
  end
  update DOKUMNOTP set AKCEPTPOZ=:ack where ref=:notpoz;
end^
SET TERM ; ^
