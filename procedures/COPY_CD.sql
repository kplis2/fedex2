--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COPY_CD(
      CD integer)
   as
declare variable newcd integer;
begin
  execute procedure GEN_REF('COSTDISTRIBUTION') returning_values :newcd;
  insert into COSTDISTRIBUTION(ref, company, name, cdtype, costsource, status)
    select :newcd, cd.company, cd.name||'-kopia',cd.cdtype, cd.costsource, cd.status
      from COSTDISTRIBUTION cd where cd.ref = :cd;
end^
SET TERM ; ^
