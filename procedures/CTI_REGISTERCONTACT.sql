--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CTI_REGISTERCONTACT(
      CPODMIOT integer,
      EXTPHONE varchar(30) CHARACTER SET UTF8                           ,
      INOUT integer,
      DROPPED integer)
   as
DECLARE VARIABLE RODZAJ VARCHAR(255);
DECLARE VARIABLE RODZAJD VARCHAR(255);
--DECLARE VARIABLE MSGNAZWA VARCHAR(255);
DECLARE VARIABLE MSGTEXT VARCHAR(255);
DECLARE VARIABLE CPODMIOTD VARCHAR(255);
begin
  execute procedure getconfig('PHONEDROPKONTAKT') returning_values :rodzajd;
  execute procedure getconfig('PHONEKONTAKT') returning_values :rodzaj;
  execute procedure getconfig('PHONECPODMIOT') returning_values :cpodmiotd;
  if(:inout is null) then inout = 0;
  if((:cpodmiot is null or (:cpodmiot = 0)) and cpodmiotd <> '' )then cpodmiot = cpodmiotd;
  if(:cpodmiot is null or (:cpodmiot = 0)) then
    exit;
  if(:dropped = 1 and :rodzaj <> '') then begin
    --czy przypakdiem w otoczeniu czasowym nie ma rejestracji kontaktu odbytego
    if(exists(select ref from kontakty
                    where CPODMIOT=:cpodmiot and RODZAJ=:rodzaj and kontakttelefon=:extphone
                          and (data >= CURRENT_TIME - 1.0/1440.0)     -- czas okolo 1 minuty
    )) then
      exit;
  end
  if(:dropped  = 0 and :rodzajd <> '') then begin
  --czy przypadkiem w otoczeniu czasowym nie nalezy usunac informacji i dropie
    delete from KONTAKTY where CPODMIOT = :cpodmiot and rodzaj = :rodzajd and kontakttelefon=:extphone
                        and (data >= current_date);
  end
  if(:dropped = 0) then begin
    msgtext = 'Odbyta rozmowa telefoniczna';
  end else begin
    rodzaj = :rodzajd;
    msgtext = 'Utracona rozmowa telefoniczna';
  end
  if(inout = 0) then
    inout = 1;
  else
    inout = 0;
  if(:rodzaj <> '') then
    insert into kontakty (inout, rodzaj, cpodmiot, opis, kontakttelefon, data)
                values (:inout, :rodzaj, :cpodmiot, :msgtext, :extphone, current_timestamp(0));


end^
SET TERM ; ^
