--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POW_WMSMARKETREFILL_GEN_ORDS(
      WH DEFMAGAZ_ID,
      REFILLMWSORDTYPEUP MWSORDS_ID)
   as
declare variable VERS WERSJE_ID;
declare variable MINQ QUANTITY_MWS;
declare variable MAXQ QUANTITY_MWS;
declare variable MARKETQ QUANTITY_MWS;
declare variable GENQ QUANTITY_MWS;
declare variable BUFFQ QUANTITY_MWS;
declare variable QUICKQ QUANTITY_MWS;
declare variable WHSEC WHSECS_ID;
declare variable GOOD KTM_ID;
declare variable REFILLMWSORDTYPE MWSORDS_ID;
begin
  for
    select r.whsec, r.vers, r.good, r.minq, r.maxq, w.refillmwsordtype
      from wmsmarketrefill r
        left join whsecs w on w.ref = r.whsec
      where r.wh = :wh and r.act > 0 --and r.good = '24213'
      into whsec, vers, good, minq, maxq, refillmwsordtype
  do begin
    marketq = null;
    select sum(s.quantity - s.blocked + s.ordered)
      from mwsstock s
        left join mwsconstlocs c on c.ref = s.mwsconstloc
      where s.vers = :vers and c.whsec = :whsec
      into marketq;
    if (marketq is null) then marketq = 0;
    if (marketq < minq) then
    begin
      -- teraz wyliczamy ile trzeba towaru sciagnac do marketu
      genq = 0;
      select sum(a.quantity)
        from mwsords o
          left join mwsacts a on a.mwsord = o.ref
        where o.status < 5 and o.status > 0
          and o.wh = :wh and o.mwsordtype = :refillmwsordtype and a.vers = :vers and a.status > 0 and a.status < 6
        into genq;
      marketq = marketq + coalesce(genq,0);
      genq = 0;
      if (marketq < maxq) then
        execute procedure gen_marketrefill_order(:wh, :refillmwsordtype, :vers, :maxq - :marketq, 10)
          returning_values (:genq);
      -- dostawiamy towar z góry
      marketq = marketq + genq;
      if (maxq > marketq) then
      begin
        -- zanim wygenerujemy to sprawdzamy co juz wygenerowalismy
        genq = 0;
        select sum(a.quantity)
          from mwsords o
            left join mwsacts a on a.mwsord = o.ref
          where o.status < 5 and o.status > 0
            and o.wh = :wh and o.mwsordtype = :refillmwsordtypeup and o.doctype = 'L' and a.vers = :vers and a.status > 0 and a.status < 6
          into genq;
        marketq = marketq + coalesce(genq,0);
        genq = 0;
        select sum(a.quantity)
          from mwsords o
            left join mwsords u on u.ref = o.frommwsord
            left join mwsacts a on a.mwsord = u.ref
          where o.status < 5 and o.status > 0 and o.mwsordtypedest = 11
            and o.wh = :wh and o.mwsordtype = :refillmwsordtypeup and u.doctype = 'L' and a.vers = :vers and a.status > 0 and a.status < 5
          into genq;
        marketq = marketq + coalesce(genq,0);
        while (marketq < maxq) do
        begin
          execute procedure gen_marketrefill_up_order(:refillmwsordtypeup, :wh, :whsec, :vers, :maxq - :marketq)
            returning_values (:genq);
          if (coalesce(genq,0) = 0) then
            marketq = maxq;
          else
            marketq = maxq+coalesce(genq,0);
        end
      end
    end
  end
end^
SET TERM ; ^
