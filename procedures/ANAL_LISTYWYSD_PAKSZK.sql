--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ANAL_LISTYWYSD_PAKSZK(
      LISTWYSD integer)
  returns (
      OPER varchar(60) CHARACTER SET UTF8                           ,
      SYMBDOK varchar(60) CHARACTER SET UTF8                           ,
      DATADOK timestamp,
      TYP smallint)
   as
declare variable grupasped integer;
begin
--typ = 0 - wystawil
  typ = 0;
  for select distinct o.nazwa, d.symbol, d.data
        from listywysdpoz lwp
          left join  dokumnag d on (d.ref = lwp.dokref)
          left join operator o on (d.operator = o.ref)
        where lwp.dokument = :listwysd
      into :oper, :symbdok, :datadok
  do begin
    suspend;
  end

--typ = 1 - zaakceptowal
  typ = 1;
  for select distinct o.nazwa, d.symbol, d.dataakc
        from listywysdpoz lwp
          left join  dokumnag d on (d.ref = lwp.dokref)
          left join operator o on (d.operakcept = o.ref)
        where lwp.dokument = :listwysd
      into :oper, :symbdok, :datadok
  do begin
    suspend;
  end

--typ = 2 - szykowal zlecenie WT
  typ = 2;
  for select distinct d.grupasped
       from listywysdpoz lwp
          left join  dokumnag d on (d.ref = lwp.dokref)
        where lwp.dokument = :listwysd
      into :grupasped
  do begin
    for select distinct o.nazwa, mo.symbol, mo.timestop
          from mwsords mo
            left join operator o on (mo.operator = o.ref)
          where mo.docgroup = :grupasped
        into :oper, :symbdok, :datadok
    do begin
      suspend;
    end
  end

--typ = 3 - szykowal
  typ = 3;
  select o.nazwa, l.symbolsped, l.dataakc
    from listywysd l
      left join operator o on (l.acceptoper = o.ref)
    where l.ref = :listwysd
  into :oper, :symbdok, :datadok;
  suspend;
end^
SET TERM ; ^
