--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_FCOLSUM(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      FLAG varchar(4) CHARACTER SET UTF8                           ,
      FROMECOL integer = null,
      TOECOL integer = null)
  returns (
      RET numeric(14,2))
   as
begin
/*DU: Personel - funkcja liczy sume wartosci skladnikow z listy plac o zadanej
                 fladze FLAG. Mozliwe jest pobranie wartosci z zadanego zakresu
                 skladnikow: od FROMECOL do TOECOL.  */

  select sum(p.pvalue)
    from eprpos p
      join ecolumns c on (c.number = p.ecolumn and c.cflags containing ';' || :flag || ';')
    where p.employee = :employee
      and p.payroll = :payroll
      and p.ecolumn >= coalesce(:fromecol,0) --PR48961
      and p.ecolumn <= coalesce(:toecol,9999999)
    into :ret;

  if (ret is null) then
    ret = 0;
  suspend;
end^
SET TERM ; ^
