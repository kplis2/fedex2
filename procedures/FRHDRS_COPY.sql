--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRHDRS_COPY(
      OLD_FRHDRS_SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      NEW_FRHDRS_SYMBOL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable NEW_FRCOLS_REF1 varchar(20);
declare variable OLD_FRCOLS_REF1 varchar(20);
declare variable NEW_FRCOLS_REF2 varchar(20);
declare variable OLD_FRCOLS_REF2 varchar(20);
declare variable NEW_FRCOLS_REF3 varchar(20);
declare variable OLD_FRCOLS_REF3 varchar(20);
declare variable NEW_FRCOLS_REF4 varchar(20);
declare variable OLD_FRCOLS_REF4 varchar(20);
declare variable NEW_FRCOLS_REF integer;
declare variable OLD_FRCOLS_REF integer;
declare variable FRCOLS_NUMBER integer;
declare variable FRCOLS_NAME varchar(60);
declare variable FRCOLS_FORBO smallint;
declare variable OLD_FRPSNS_REF integer;
declare variable NEW_FRPSNS_REF integer;
declare variable NEW_FRPSNS_NUMBER integer;
declare variable NEW_FRPSNS_SYMBOL varchar(15);
declare variable NEW_FRPSNS_NAME varchar(255);
declare variable NEW_FRPSNS_ALGORITHM smallint;
declare variable NEW_FRPSNS_COUNTORD smallint;
declare variable NEW_FRPSNS_ORD smallint;
declare variable NEW_FRPSNS_ZEROMORE smallint;
declare variable NEW_FRPSNS_FORMULA varchar(4096);
declare variable NEW_FRPSNS_SHORTNAME varchar(20);
declare variable NEW_FRPSNS_ALARMVALUEPLUS numeric(3,2);
declare variable NEW_FRPSNS_ALARMVALUEMINUS numeric(3,2);
declare variable NEW_FRPSNSALG_ALGTYPE smallint;
declare variable NEW_FRPSNSALG_PROCED varchar(8191);
declare variable NEW_FRPSNSALG_BODY varchar(8191);
declare variable NEW_FRPSNSALG_DECL varchar(8191);
declare variable NEW_FRPSNSALG_FRVERSION integer;
declare variable MAX_VER integer;
declare variable I integer;
declare variable REF integer;
declare variable FRPSNS_COUNT integer;
begin

  if(coalesce(char_length(:new_frhdrs_symbol),0) > 20) then -- [DG] XXX ZG119346
    exception FRHDRS_COPY_EXPT 'Nazwa sprawozdania może mieć do 20 znaków';

  if(not exists (select first 1 1 from frhdrs where symbol = :old_frhdrs_symbol)) then
    exception FRHDRS_COPY_EXPT 'Sprawozdanie '||:old_frhdrs_symbol||' nie istnieje';

  if(exists (select first 1 1 from frhdrs where symbol = :new_frhdrs_symbol)) then
    exception FRHDRS_COPY_EXPT 'Sprawozdanie '||:new_frhdrs_symbol||' już istnieje';

  insert into frhdrs(
    symbol, name, status, firstbo, printerpattern, shortname,frhdrtype,planed,
    frprecision, generalization, timedimen, timefrdimhier, application, chktype)
  select
    :new_frhdrs_symbol, name, 0, firstbo, printerpattern, shortname,frhdrtype,planed,
    frprecision, generalization, timedimen, timefrdimhier, application, chktype
  from frhdrs
  where symbol = :old_frhdrs_symbol;

  i = 0;
  old_frcols_ref1 = 0;
  old_frcols_ref2 = 0;
  old_frcols_ref3 = 0;
  old_frcols_ref4 = 0;

  for select ref, number, name, forbo --kopiowanie z FRCOLS
    from frcols
    where frhdr = :old_frhdrs_symbol
    into :old_frcols_ref, :frcols_number, :frcols_name, :frcols_forbo
  do begin

    execute procedure GEN_REF('FRCOLS') returning_values :new_frcols_ref;

    insert into frcols (ref, frhdr, number, name, forbo)
      values(:new_frcols_ref, :new_frhdrs_symbol, :frcols_number, :frcols_name, :frcols_forbo);

    i = :i + 1;

    if(i = 1) then
      begin
      old_frcols_ref1 = :old_frcols_ref;
      new_frcols_ref1 = :new_frcols_ref;
      end

    if(i = 2) then
      begin
      old_frcols_ref2 = :old_frcols_ref;
      new_frcols_ref2 = :new_frcols_ref;
      end

    if(i = 3) then
      begin
      old_frcols_ref3 = :old_frcols_ref;
      new_frcols_ref3 = :new_frcols_ref;
      end

    if(i = 4) then
      begin
      old_frcols_ref4 = :old_frcols_ref;
      new_frcols_ref4 = :new_frcols_ref;
      end
  end


  for select --kopiowanie z FRPSNS
    ref, number, symbol, name, algorithm, countord, ord,
    zeromore, formula, shortname, alarmvalueplus, alarmvalueminus
  from frpsns
  where frhdr = :old_frhdrs_symbol
  into
    :old_frpsns_ref, :new_frpsns_number, :new_frpsns_symbol, :new_frpsns_name,   -- pobiera stary REF
    :new_frpsns_algorithm, :new_frpsns_countord, :new_frpsns_ord, :new_frpsns_zeromore,
    :new_frpsns_formula, :new_frpsns_shortname, :new_frpsns_alarmvalueplus, :new_frpsns_alarmvalueminus
  do begin

    execute procedure GEN_REF('FRPSNS') returning_values :new_frpsns_ref;  -- generuje nowy REF

    insert into frpsns (
      ref, frhdr, number, symbol, name, algorithm, countord, ord,
      zeromore, formula, shortname, alarmvalueplus, alarmvalueminus, kopia)
    values(
      :new_frpsns_ref, :new_frhdrs_symbol, :new_frpsns_number, :new_frpsns_symbol, :new_frpsns_name,
      :new_frpsns_algorithm, :new_frpsns_countord, :new_frpsns_ord, :new_frpsns_zeromore,
      :new_frpsns_formula, :new_frpsns_shortname, :new_frpsns_alarmvalueplus, :new_frpsns_alarmvalueminus, 1);

    if(old_frcols_ref1 <> 0) then
    begin
      select max(frversion) from frpsnsalg
      where frpsn = :old_frpsns_ref and frcol = :old_frcols_ref1
      into max_ver;

      select algtype, proced, body, decl, frversion from frpsnsalg
      where frpsn = :old_frpsns_ref and frcol = :old_frcols_ref1 and frversion = :max_ver
      into :new_frpsnsalg_algtype,:new_frpsnsalg_proced, :new_frpsnsalg_body,
           :new_frpsnsalg_decl, :new_frpsnsalg_frversion;

      insert into frpsnsalg(frpsn, frcol, algtype, proced, body, decl, frversion)
      values(:new_frpsns_ref, :new_frcols_ref1, :new_frpsnsalg_algtype,
            :new_frpsnsalg_proced, :new_frpsnsalg_body, :new_frpsnsalg_decl, 0);
    end

    if(old_frcols_ref2 <> 0) then
    begin
      select max(frversion) from frpsnsalg
      where frpsn = :old_frpsns_ref and frcol = :old_frcols_ref2
      into max_ver;

      select algtype, proced, body, decl, frversion from frpsnsalg
      where frpsn = :old_frpsns_ref and frcol = :old_frcols_ref2 and frversion = :max_ver
      into :new_frpsnsalg_algtype,:new_frpsnsalg_proced, :new_frpsnsalg_body,
           :new_frpsnsalg_decl, :new_frpsnsalg_frversion;

      insert into frpsnsalg(frpsn, frcol, algtype, proced, body, decl, frversion)
      values(:new_frpsns_ref, :new_frcols_ref2, :new_frpsnsalg_algtype,
            :new_frpsnsalg_proced, :new_frpsnsalg_body, :new_frpsnsalg_decl, 0);
    end

    if(old_frcols_ref3 <> 0) then
    begin
      select max(frversion) from frpsnsalg
      where frpsn = :old_frpsns_ref and frcol = :old_frcols_ref3
      into max_ver;

      select algtype, proced, body, decl, frversion from frpsnsalg
      where frpsn = :old_frpsns_ref and frcol = :old_frcols_ref3 and frversion = :max_ver
      into :new_frpsnsalg_algtype,:new_frpsnsalg_proced, :new_frpsnsalg_body,
           :new_frpsnsalg_decl, :new_frpsnsalg_frversion;

      insert into frpsnsalg(frpsn, frcol, algtype, proced, body, decl, frversion)
      values(:new_frpsns_ref, :new_frcols_ref3, :new_frpsnsalg_algtype,
            :new_frpsnsalg_proced, :new_frpsnsalg_body, :new_frpsnsalg_decl, 0);
    end

    if(old_frcols_ref4 <> 0) then
    begin
      select max(frversion) from frpsnsalg
      where frpsn = :old_frpsns_ref and frcol = :old_frcols_ref4
      into max_ver;

      select algtype, proced, body, decl, frversion from frpsnsalg
      where frpsn = :old_frpsns_ref and frcol = :old_frcols_ref4 and frversion = :max_ver
      into :new_frpsnsalg_algtype,:new_frpsnsalg_proced, :new_frpsnsalg_body,
           :new_frpsnsalg_decl, :new_frpsnsalg_frversion;

      insert into frpsnsalg(frpsn, frcol, algtype, proced, body, decl, frversion)
      values(:new_frpsns_ref, :new_frcols_ref4, :new_frpsnsalg_algtype,
            :new_frpsnsalg_proced, :new_frpsnsalg_body, :new_frpsnsalg_decl, 0);
    end

    end
  end^
SET TERM ; ^
