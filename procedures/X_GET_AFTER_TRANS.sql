--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_GET_AFTER_TRANS(
      BARCODE STRING255,
      AKTUOPERATOR INTEGER_ID)
  returns (
      ORDSNAGWT MWSORDS_ID,
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable dokumpozWZ dokumpoz_id;
declare variable dokumnagWZ dokumnag_id;
declare variable dokumnagRW dokumnag_id;
declare variable dokumnagPW dokumnag_id;
declare variable dokumpozPW dokumpoz_id;
declare variable ordsnagPZ mwsords_id;
declare variable ordsnagMG mwsords_id;
--declare variable ordsnagWT mwsords_id;
declare variable ordspozPZ mwsacts_id;
declare variable ordspozMG mwsacts_id;
declare variable ordspozWT mwsacts_id;
--declare variable aktuoperator string255;
declare variable ktm ktm_id;
declare variable wersja wersje_id;
declare variable ilosc quantity_mws;
declare variable iloscc quantity_mws;
declare variable iloscl quantity_mws;
declare variable palloc mwspallocs_id;
declare variable PERIOD varchar(6);
declare variable symbolwz symbol_id;
declare variable grupasped dokumnag_id;
declare variable magazyn wh_id;
declare variable tmpint integer_id;
declare variable tmpref string100;
declare variable MWSORDREF MWSORDS_ID;
declare variable CARTSYMB SYMBOL_ID;
declare variable MODKLADCZE SYMBOL_ID;
begin
--Procedura sluzaca do przyjecia programu komputerowego z serwisu po przeprogramowaniu
--mwsordref to ref dokumentu WT wygenerowanego na podstawie RW
    --poprawnosc formatow
 /*   if (modkladcze not similar to 'P[[:DIGIT:]]{2}') then
        exception universal 'Zly format miejsca odkladczego';
    if (cartsymb not similar to 'K[[:DIGIT:]]{3}') then
        exception universal 'Zly format koszyka';
    select ref from mwsconstlocs c where c.symbol = :cartsymb
        into tmpint;
    if (tmpint is null) then
        insert into mwsconstlocs (symbol, volume, act, deep, wh, wcategory )
            values (:cartsymb, 2, 0, 0, :magazyn, 'A');
    tmpint = null;                                   */
    --wyciaganie refa z barcode
    ORDSNAGWT = 0;
    status = 1;
    msg = '';
    tmpint = position('#' in barcode);
    if (tmpint = 0) then begin
      status = 0;
      msg = 'Bledny kod '||coalesce(barcode, '');
      suspend;
      exit;
    end
    tmpref = substring(barcode from 1 for tmpint-1);
    mwsordref = cast(tmpref as integer_id);
    --pobranie danych
    select docid from mwsords where ref = :mwsordref
        into :dokumnagRw;
    select grupaprod from dokumnag where ref = :dokumnagRW
        into :dokumpozWZ;
    if (dokumnagrw is null or dokumpozWZ is null) then begin
      status = 0;
      msg = 'Bledny kod - brak powiązanych dokumentów. '||coalesce(barcode, '');
      suspend;
      exit;
    end
    select d.dokument from dokumpoz d where d.ref = :dokumpozwz
        into :dokumnagWZ;
    select d.symbol, d.grupasped, d.magazyn from dokumnag d where d.ref = :dokumnagWZ
        into :symbolWZ, :grupasped, :magazyn;
    if (coalesce(aktuoperator,0) = 0) then
      execute procedure get_global_param('AKTUOPERATOR') returning_values  :aktuoperator;
    --if(aktuoperator is null) then
    --    aktuoperator = '74';
    select okres from datatookres(current_date,0,0,0,0) into period;

    select d.ref from dokumnag d
        where grupaprod = :dokumpozwz
            and d.typ = 'PW'
        into :dokumnagPW;
    /*
    --generowanie zlecenia magazynowego PZ
    -- generowanie palety
    execute procedure gen_ref('MWSPALLOCS') returning_values :palloc;
    insert into mwspallocs(ref, symbol,mwspalloctype)
        values (:palloc,:palloc,2);

    --naglowek PZ
    execute procedure gen_ref('MWSORDS') returning_values :ordsnagPZ;
    insert into mwsords (
        ref, mwsordtype, wh, doctype, operator, docid, docgroup,period,
        regtime, timestartdecl, timestopdecl, cor, rec )
    values(
        :ordsnagPZ,8,:magazyn ,'M',:aktuoperator, :dokumnagPW, :grupasped, :period,
        current_timestamp(0), null, current_timestamp(0),0,1 );
    --pozycja PZ
    --execute procedure gen_ref('MWSACTS') returning_values :ordspozPZ;

    insert into mwsacts (
        /*ref,*//*status, mwsord, good, vers, quantity,
        docid, docposid, mwspallocl, mwsconstlocl,
        doctype, palgroup )
     select 0, :ordsnagPZ, p.ktm, p.wersjaref, p.ilosc,
        :dokumnagPW ,p.ref,
        :palloc,1,
        'M', :palloc 
      from dokumpoz p where p.dokument = :dokumnagpw;

     */

    update dokumnag d set d.mwsblockmwsords = 0 , d.blockmwsords = 0 where d.ref = :dokumnagPW;
    -- wywolanie procedury generującej zlecenie PZ
    select refmwsord from XK_MWS_GEN_MWSORD_IN(:dokumnagPW,:grupasped,null,null,null,null,1,null) into :ordsnagPZ;


    --zmiana statusu pozycji zlecenia PZ
    update mwsacts a set a.status = 1 where a.mwsord = :ordsnagPZ;


--ponizszy wiersz generowal wyjatek MWSLOCSE_REQUIRED.Lokacje koĹ„cowe wymagane .At trigger 'MWSACTS_BIU0' line: 190, col: 1At trigger 'MWSACTS_AU_CLONEMWSACT' line: 18, col: 7.
--    update mwsacts a set a.quantityc = a.quantity, a.status = 2 where a.mwsord = :ordsnagPZ;
--starczylo dodać mwspallocl, i mwsconstlocl
--<<mkd
 execute procedure gen_ref('MWSPALLOCS') returning_values :palloc;
    insert into mwspallocs(ref, symbol,mwspalloctype)
        values (:palloc,:palloc,2);
    update mwsacts a set  a.status = 1 where a.mwsord = :ordsnagPZ;
    update mwsacts a set a.mwsconstlocl = 1, a.mwspallocl = :palloc, a.quantityc = a.quantity, a.status = 2 where a.mwsord = :ordsnagPZ;
-->>mkd
    select first 1 a.mwspallocl from mwsacts a where a.mwsord = :ordsnagpz into :palloc;
    --akceptacja zlecenia PZ

    update mwsords o set o.status = 5 where o.ref =: ordsnagPZ;

    --znalezienie dokumentu MG
    select first 1 ref from mwsords o where  o.palgroup = :palloc -- o.frommwsact = :ordsnagpz
        into :ordsnagMG;

   --potwierdzenie dokumentu MG
    update mwsacts a set a.operator = :aktuoperator, a.status = 1 where a.mwsord = :ordsnagMG;
    update mwsacts a set mwsconstlocp = 1, mwspallocp = :palloc, a.quantityc = a.quantity, a.status = 2 where a.mwsord = :ordsnagMG;


    update mwsords o set o.status = 5 where o.ref =: ordsnagMG;
-- stworzenie zlecenia wt
    execute procedure gen_ref('MWSORDS') returning_values :ordsnagWT;
    insert into mwsords (
        ref, mwsordtype, wh, doctype, operator, docgroup, docid, period,
        cor,rec,docsymbs, timestart )
    values(
        :ordsnagWT,6,:magazyn ,'M',:aktuoperator, :dokumnagwz, :dokumnagwz, :period,
        0,0, :symbolwz, current_timestamp(0));
    --pozycja wt
   -- execute procedure gen_ref('MWSACTS') returning_values :ordspozWT;
    insert into mwsacts (
        /*ref,*/ mwsord, good, vers, quantity,
        docid, docposid, docgroup,
        doctype, mwsconstlocp, mwspallocp,emwsconstloc)
     select :ordsnagWt, p.ktm, p.wersjaref, p.ilosc,
        :dokumnagWZ ,p.ref, :dokumnagwz,
        'M', 1, :palloc, :modkladcze
      from dokumpoz p where p.dokument = :dokumnagpw;

/*        values(
        :ordspozWT, :ordsnagWt, :ktm, :wersja, :ilosc,
        :dokumnagWZ, :dokumpozWZ, :dokumnagwz,
        'M', 1, :palloc, :modkladcze ); */
    --potwierdzenie dokumentu WT

    update mwsacts a set a.status = 1 where a.mwsord = :ordsnagWT;
    --update mwsacts a set  a.quantityc = a.quantity, a.status = 2 where a.mwsord = :ordsnagWT;
    --update mwsords o set  o.status = 1 where o.ref =: ordsnagWT;
    --update mwsords o set  o.status = 5 where o.ref =: ordsnagWT;
    --execute procedure XK_MWS_SET_CART_LOCATION(ordsnagWT,cartsymb,modkladcze);
  suspend;
end^
SET TERM ; ^
