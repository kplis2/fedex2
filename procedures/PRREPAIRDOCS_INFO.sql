--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRREPAIRDOCS_INFO
  returns (
      PRMACHREPAIRS integer,
      OTABLE varchar(40) CHARACTER SET UTF8                           ,
      OREF varchar(40) CHARACTER SET UTF8                           ,
      DOCTYPE varchar(40) CHARACTER SET UTF8                           ,
      WAREHOUSE varchar(40) CHARACTER SET UTF8                           ,
      REGISTER varchar(40) CHARACTER SET UTF8                           ,
      SYMBOL varchar(40) CHARACTER SET UTF8                           ,
      DOKDATE timestamp,
      CONTRACTOR varchar(80) CHARACTER SET UTF8                           ,
      KTM varchar(80) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      QUANTITY varchar(80) CHARACTER SET UTF8                           ,
      UOM varchar(5) CHARACTER SET UTF8                           ,
      PRICE numeric(14,2),
      VAL numeric(14,2))
   as
begin

FOR SELECT PRMACHREPAIRS, OTABLE, OREF
      FROM PRREPAIRDOCS INTO :PRMACHREPAIRS, :OTABLE, :OREF
DO BEGIN
  DOCTYPE=null; warehouse=null; REGISTER=null; SYMBOL=null; DOKDATE=null;
  CONTRACTOR=null; ktm=null; quantity=null; uom=null; price=null; val=null; nazwa=null;
  if(:otable='NAGFAK') then begin
    select N.TYP, N.MAGAZYN, N.REJESTR, N.SYMBOL, N.DATA, case when N.ZAKUP=1 then D.ID else K.ID end from NAGFAK N
      left join KLIENCI K on K.REF=N.KLIENT
      left join DOSTAWCY D on D.REF=N.DOSTAWCA
      where N.REF=:OREF
      into :DOCTYPE, :warehouse, :REGISTER, :SYMBOL, :DOKDATE, :CONTRACTOR;
  end else if(:otable='POZFAK') then begin
    select O.TYP, O.MAGAZYN, O.REJESTR, O.SYMBOL, O.DATA, case when O.ZAKUP=1 then D.ID else K.ID end,
           P.KTM ,P.ILOSC, J.JEDN, P.CENABRU, P.WARTBRU, T.NAZWA
      from POZFAK P
      left join TOWARY T on T.KTM=P.KTM
      left join NAGFAK O on O.REF=P.DOKUMENT
      left join KLIENCI K on K.REF=O.KLIENT
      left join DOSTAWCY D on D.REF=O.DOSTAWCA
      left join TOWJEDN J on J.REF=P.JEDN
      where P.REF=:OREF
      into :DOCTYPE, :warehouse, :REGISTER, :SYMBOL, :DOKDATE, :CONTRACTOR,
           :ktm, :quantity, :uom, :price, :val, :nazwa;
  end else if(:otable='DOKUMNAG') then begin
    select O.TYP, O.MAGAZYN, '' as REJESTR, O.SYMBOL, O.DATA, case when E.WYDANIA=1 then K.ID else D.ID end from DOKUMNAG O
      left join DEFDOKUM E on E.SYMBOL=O.TYP
      left join KLIENCI K on K.REF=O.KLIENT
      left join DOSTAWCY D on D.REF=O.DOSTAWCA
      where O.REF=:OREF
      into :DOCTYPE, :warehouse, :REGISTER, :SYMBOL, :DOKDATE, :CONTRACTOR;
  end else if(:otable='DOKUMPOZ') then begin
    select O.TYP, O.MAGAZYN, '' as REJESTR, O.SYMBOL, O.DATA, case when E.WYDANIA=1 then K.ID else D.ID end,
           P.KTM ,P.ILOSC, J.JEDN, P.CENA, P.WARTOSC, T.NAZWA
    from DOKUMPOZ P
      left join TOWARY T on T.KTM=P.KTM
      left join DOKUMNAG O on O.REF=P.DOKUMENT
      left join DEFDOKUM E on E.SYMBOL=O.TYP
      left join KLIENCI K on K.REF=O.KLIENT
      left join DOSTAWCY D on D.REF=O.DOSTAWCA
      left join TOWJEDN J on J.REF=P.JEDNO
      where P.REF=:OREF
      into :DOCTYPE, :warehouse, :REGISTER, :SYMBOL, :DOKDATE, :CONTRACTOR,
           :ktm, :quantity, :uom, :price, :val, :nazwa;
  end
  suspend;
END
end^
SET TERM ; ^
