--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_OBL_CENAKOSZT(
      DOKMAG integer)
   as
declare variable sumnet numeric(14,2);
declare variable sumkoszt numeric(14,2);
declare variable udzial numeric(14,4);
begin
  select WARTOSC, KOSZTDOST from DOKUMNAG where REF=:dokmag into :sumnet, sumkoszt;
  if(:sumkoszt is null) then sumkoszt = 0;
  if(:sumnet is null) then sumnet = 0;
  if(:sumnet<>0) then begin
    if(:sumkoszt = 0) then
      update DOKUMPOZ set CENA_KOSZT = CENA where DOKUMENT=:dokmag;
    else begin
      udzial = :sumkoszt/:sumnet;
      update DOKUMPOZ set CENA_KOSZT = CENA*(1+:udzial) where DOKUMENT=:dokmag and ILOSC<>0;
      update DOKUMPOZ set CENA_KOSZT = CENA where DOKUMENT=:dokmag and ILOSC=0;
    end
  end
end^
SET TERM ; ^
