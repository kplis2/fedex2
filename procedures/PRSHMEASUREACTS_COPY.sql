--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHMEASUREACTS_COPY(
      ISPRSHOPER integer,
      IOPRSHOPER integer,
      IREWRITE smallint = 0)
   as
declare variable vtooltype varchar(20);
declare variable vamount numeric(14,4);
declare variable vactivity integer;
declare variable vnumber integer;
declare variable vord smallint;
declare variable vdescript varchar(1024);
declare variable vdefaultvalue varchar(22);
declare variable vmeasuretools varchar(1024);
begin
  for select t.prtooltype, t.amount
    from prshmeasuretools t
    where t.prshoper = :isprshoper
    into :vtooltype, :vamount
  do begin
    if (not exists(select 1 from prshmeasuretools t
        where t.prshoper = :ioprshoper and t.prtooltype = :vtooltype)) then
    begin
      insert into prshmeasuretools(prshoper,prtooltype,amount)
        values(:ioprshoper, :vtooltype, :vamount);
    end else if (:irewrite > 0) then begin
      update prshmeasuretools
        set amount = :vamount
        where prshoper = :ioprshoper
        and prtooltype = :vtooltype;
    end
  end
  for select a.activity, a.number, a.ord, a.descript, a.defaultvalue, a.measuretools
    from prshmeasureacts a
    where a.prshoper = :isprshoper
    order by a.number
    into :vactivity, :vnumber, :vord, :vdescript, :vdefaultvalue, :vmeasuretools
  do begin
    if (not exists(select 1 from prshmeasureacts a
        where a.prshoper = :ioprshoper and a.activity = :vactivity)) then
    begin
      insert into prshmeasureacts(prshoper,activity,number,ord,descript,defaultvalue,measuretools)
        values(:ioprshoper, :vactivity, :vnumber, :vord, :vdescript, :vdefaultvalue, :vmeasuretools);
    end else if (:irewrite > 0) then begin
      update prshmeasureacts a
        set a.number = :vnumber, a.ord = :vord, a.descript = :vdescript,
          a.defaultvalue = :vdefaultvalue, a.measuretools = :vmeasuretools
        where a.prshoper = :ioprshoper
        and a.activity = :vactivity;
    end
  end
end^
SET TERM ; ^
