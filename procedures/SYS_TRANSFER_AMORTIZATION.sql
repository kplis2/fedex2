--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_TRANSFER_AMORTIZATION(
      STARTYEAR smallint)
   as
declare variable s1 varchar(255); /* symbol ST */
declare variable s2 varchar(255); /* rok amortyzacji */
declare variable s3 varchar(255); /* miesiac amortyzacji */
declare variable s4 varchar(255); /* kwota amortyzacji podatkowej */
declare variable s5 varchar(255); /* kwota amortyzacji finansowej */
declare variable s6 varchar(255); /* metoda amortyzacji podatkowej */
declare variable s7 varchar(255); /* metoda amortyzacji finansowej */
declare variable s8 varchar(255); /* stawka amortyzacji podatkowej */
declare variable s9 varchar(255); /* stawka amortyzacji finansowej */
declare variable symbol integer;
declare variable amyear smallint;
declare variable ammonth smallint;
declare variable amperiod integer;
declare variable tamort numeric(15,2);
declare variable famort numeric(15,2);
declare variable ttype smallint;
declare variable ftype smallint;
declare variable tarate numeric(10,5);
declare variable farate numeric(10,5);
declare variable tmp integer;
declare variable fxdasset integer;
begin
/* UWAGA!!
   Przy imporcie amortyzacji warto pilnować aby kolejnoć REF-ów w tabeli AMPERIODS
   odpowiadala kolejnym miesiacom. Aby to zrobić należy na początku usunąć do tej pory zalozone okresy:

  delete from amperiods;

   */
  select count(ref) from expimp where coalesce(char_length(s1),0)>20 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich symboli.';

  select count(ref) from expimp where coalesce(char_length(s2),0)<>4 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba blednie wpisanych lat.';

  select count(ref) from expimp where coalesce(char_length(s3),0)<>2 and coalesce(char_length(s3),0)<>1 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba blednie wpisanych miesiecy.';

-- zalozenie okresow amortyzacji od roku poczatkowego do obecnego.
  tmp = startyear;
  while (tmp <= extract(year from current_date))
  do begin
    execute procedure create_amperiods(1, tmp, 1);
    tmp = :tmp +1;
  end

  for select s1, s2,  s3,  s4,  s5,  s6,  s7,  s8,  s9
    from expimp 
  into :s1, :s2,  :s3,  :s4,  :s5,  :s6,  :s7,  :s8,  :s9
  do begin

-- symbol ST
    symbol = substring(:s1 from  1 for  20);
    if (exists(select first 1 1 from fxdassets where symbol=:symbol)) then
      exception universal 'ST o symbolu: ' || :s1 || ' juz znajduje sie w bazie';
    select ref from fxdassets where symbol=:symbol
    into :fxdasset;

-- rok i miesiac
    amyear = cast(:s2 as smallint);
    ammonth = cast(:s3 as smallint);


-- okres
    select ref from amperiods where amyear = :amyear and ammonth = :ammonth
    into :amperiod;


-- wartosc amortyzacji podatkowa
    s4 = replace(:s4, ',', '.');
    tamort = cast(:s4 as numeric(15,2));


-- wartosc amortyzacji finansowej
    s5 = replace(:s5, ',', '.');
    famort = cast(:s5 as numeric(15,2));


-- metoda amortyzacji podatkowej
    if(:s6='L') then
      ttype = 0;
    else if (:s6='D') then
      ttype = 1;
    else if (:s6='U') then
      ttype = 2;
    else
      exception universal 'Metoda amortyzacji podatkowej srodka: ' || :s1 || ' jest bledna.';


-- metoda amortyzacji finansowej
    if(:s7='L') then
      ftype = 0;
    else if (:s7 = 'D') then
      ftype = 1;
    else if (:s7 = 'U') then
      ftype = 2;
    else
      exception universal 'Metoda amortyzacji finansowej srodka: ' || :s1 || ' jest bledna.';


-- stawka amortyzacji podatkowej
    s8 = replace(:s8, ',', '.');
    tarate = cast(:s8 as numeric(10,5));
    if (tarate > 100 or tarate < 0) then
      exception universal 'Bledna stawka amortyzacji podatkowej srodka: '|| :s1;


-- stawka amortyzacji finansowej
    s9 = replace(:s9, ',', '.');
    farate = cast(:s9 as numeric(10,5));
    if (farate > 100.00 or farate < 0.0) then
      exception universal 'Bledna stawka amortyzacji finansowej srodka: '|| :s1;



-- INSERT

    INSERT INTO AMORTIZATION (FXDASSET, AMPERIOD, AMYEAR, AMMONTH, TAMORT,
                              TTYPE, TARATE, FAMORT, FTYPE, FARATE)
                      VALUES (:fxdasset, :amperiod, :amyear, :ammonth, :tamort,
                              :ttype, :tarate, :famort, :ftype, :farate);

  end

-- zamykanie okresow amortyzacji
  for select distinct amperiod from amortization
  order by amperiod
  into :tmp
  do begin
    update amperiods set status=1 where ref=:tmp and status=0;
  end
end^
SET TERM ; ^
