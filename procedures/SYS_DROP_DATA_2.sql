--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_DROP_DATA_2(
      TABLE_NAME varchar(31) CHARACTER SET UTF8                           ,
      TWHERE varchar(255) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint)
   as
  declare variable fromtable varchar(31);
  declare variable field_name varchar(31);
begin
  status = 1;
  for
    select fk.table_name, f.field_name
      from sys_tablesfk fk
        join sys_tablesfkfields f on (fk.fk_name = f.fk_name)
      where fk.ontable = :table_name and fk.null_flag is null
      into :fromtable, :field_name
  do begin
    execute statement 'update ' || fromtable || ' set ' || field_name || ' = null';
  end
  if (:twhere is null) then twhere = '';
  if (:twhere<>'') then
    execute statement 'delete from ' || :table_name || ' where ' || :twhere;
  else
    execute statement 'delete from ' || :table_name;
  when any do begin
    status = 0;
  end
end^
SET TERM ; ^
