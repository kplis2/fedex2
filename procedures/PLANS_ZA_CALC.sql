--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PLANS_ZA_CALC(
      PLANS integer,
      PLANSCOL integer,
      UZUPELNIANIE smallint,
      AKTUALIZACJA smallint,
      PLANSROW integer)
  returns (
      VAL numeric(14,4))
   as
declare variable czasprod smallint;
declare variable stanymag smallint;
DECLARE VARIABLE KEY1 VARCHAR(80);
DECLARE VARIABLE TEMPODNI NUMERIC(14,4);
DECLARE VARIABLE TEMPOOKR NUMERIC(14,4);
DECLARE VARIABLE DATAPOCZ TIMESTAMP;
DECLARE VARIABLE DATAKON TIMESTAMP;
DECLARE VARIABLE CNT INTEGER;
declare variable realtime numeric(14,2);
declare variable ref integer;
declare variable income numeric(14,2);
declare variable expend numeric(14,2);
DECLARE VARIABLE KTM VARCHAR(80);
declare variable grossquantity numeric(14,2);
begin
  val = -1;
  if (:AKTUALIZACJA is null) then AKTUALIZACJA = 0;
  if (:uzupelnianie is null) then uzupelnianie = 0;
  if (:czasprod is null) then czasprod = 0;
  if (:stanymag is null) then stanymag = 0;
  select planscol.bdata, planscol.ldata from planscol where planscol.ref = :PLANSCOL
    into :datapocz, :datakon;
  select plansrow.KEY1 from plansrow
    where plansrow.plans = :PLANS and PLANSrow.ref = :PLANSROW into :key1;
  val = 0;
  for select ps.ktm, pm.grossquantity from prshmat pm join prsheets ps on (pm.sheet = ps.ref)
   where pm.ktm = :key1 into :KTM,  :grossquantity
  do begin
    execute procedure plans_ktm_from_to(:ktm, :datapocz, :datakon) returning_values :income, :expend;
    val = :val + :income*:grossquantity;
  end
  execute procedure plans_ktm_from_to(:key1, :datapocz, :datakon) returning_values :income, :expend;
--  exception test_break :key1||' '||:val||' '||:income||:expend;
  val = :val + :income - :expend;
  if (:val is null) then val = -1;
  else if (:val<0) then val = 0;
--  exception test_break :key1||' '||:val;
  suspend;
end^
SET TERM ; ^
