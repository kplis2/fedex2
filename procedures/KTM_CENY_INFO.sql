--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KTM_CENY_INFO(
      WERSJAREF integer,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           )
  returns (
      CENA_ZAKN numeric(14,4),
      CENA_FABN numeric(14,4),
      RABAT numeric(14,2),
      STAN_MIN numeric(14,4),
      STAN_MAX numeric(14,4),
      DOST_MIN numeric(14,4),
      DOST_MAX numeric(14,4),
      CENA_ZAKNL numeric(14,4),
      CEN_KOSZTN numeric(14,4),
      CEN_SPRPODN numeric(14,4))
   as
declare variable towjed integer;
declare variable cennik varchar(10);
declare variable walpln varchar(3);
begin
  CENA_ZAKN = 0;
  CENA_FABN = 0;
  RABAT = 0;
  STAN_MIN = 0;
  STAN_MAX = 0;
  DOST_MIN = 0;
  DOST_MAX = 0;
  CEN_SPRPODN = 0;
  execute procedure GETCONFIG('WALPLN') returning_values :walpln;
  if(:walpln is null) then walpln = 'PLN';
  select CENA_ZAKN, CENA_FABN, CENA_ZAKNL, CENA_KOSZTN from WERSJE where REF=:wersjaref into :cena_zakn, :cena_fabn, :cena_zaknl, :cen_kosztn;
  select max(RABAT) from DOSTCEN where WERSJAREF = :wersjaref and GL = 1 into :rabat;
  select min(CENANET), max(CENANET) from DOSTCEN where wersjaref = :wersjaref and WALUTA = :walpln  into :dost_min, :dost_max;
  select min(CENA), max(cena) from STANYCEN where ILOSC > 0 and WERSJAREF = :wersjaref and (MAGAZYN = :magazyn or (:magazyn = '') or (:magazyn is null)) into :stan_min, :stan_max;
  execute procedure getconfig('CENNIK') returning_values :cennik;
  if(:cennik <> '') then begin
    select TOWJEDN.REF from TOWJEDN join WERSJE on (WERSJE.KTM = TOWJEDN.KTM and TOWJEDN.glowna = 1) where WERSJE.REF = :wersjaref into :towjed;
    if(:towjed > 0) then begin
      select CENNIK.cenanet from CENNIK where WERSJAREF = :wersjaref and CENNIK = :cennik and JEDN = :towjed into :cen_sprpodn;
    end
  end
  if(:CENA_ZAKN is null) then CENA_ZAKN = 0;
  if(:CENA_FABN is null) then   CENA_FABN = 0;
  if(:RABAT is null) then   RABAT = 0;
  if(:STAN_MAX is null) then   STAN_MIN = 0;
  if(:STAN_MAX is null) then   STAN_MAX = 0;
  if(:DOST_MIN is null) then   DOST_MIN = 0;
  if(:DOST_MAX is null) then   DOST_MAX = 0;
  if(:cena_zaknl is null) then CENA_ZAKNL = 0;
end^
SET TERM ; ^
