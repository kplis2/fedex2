--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PLANS_KNAGZAM_MINUS(
      ZAM integer)
   as
DECLARE VARIABLE KKTM VARCHAR(40);
DECLARE VARIABLE KILOSC NUMERIC(15,4);
DECLARE VARIABLE KILZREAL NUMERIC(15,4);
DECLARE VARIABLE PRSCHEDULE INTEGER;
DECLARE VARIABLE PLANSCOL INTEGER;
DECLARE VARIABLE PLANSVAL INTEGER;
DECLARE VARIABLE DYSPVAL NUMERIC(15,4);
DECLARE VARIABLE REALVAL NUMERIC(15,4);
begin
    select n.kktm, n.kilosc,  n.kilzreal, n.prschedule, n.planscol
    from nagzam n where n.ref = :zam
    into :kktm, :kilosc, :kilzreal, :prschedule, :planscol;
    if(:prschedule is not null and :prschedule <> 0 and :planscol is not null and :planscol <>0) then begin
      if(:kilosc is null) then kilosc = 0;
      if(:kilzreal is null) then kilzreal = 0;
      select PL.ref, PL.dyspval, PL.realval
      from PLANSVAL PL
       join plansrow pr on (pl.plansrow = pr.ref)
       where pl.planscol=:planscol and pr.key1 = :kktm
      into :plansval, :dyspval, :realval;
      dyspval = :dyspval - :kilosc;
      realval = :realval - :kilzreal;
      update PLANSVAL set DYSPVAL =:dyspval, REALVAL =+ :realval
       where REF=:plansval;

    end
end^
SET TERM ; ^
