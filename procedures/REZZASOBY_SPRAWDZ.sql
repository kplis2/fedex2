--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REZZASOBY_SPRAWDZ(
      REZNAGREF integer,
      ZASOB integer)
   as
declare variable zasoblimit integer;
declare variable oddaty timestamp;
declare variable dodaty timestamp;
declare variable status integer;
begin
  select LIMIT from REZZASOBY where (REF=:zasob) into :zasoblimit;
  select DATAOD,DATADO,STATUS from REZNAG where (REF=:reznagref) into :oddaty, :dodaty, :status;
  if(:status=0) then suspend;
  select count(*) from REZPOZ
  left join REZNAG on (REZNAG.REF=REZPOZ.REZNAG)
  where (REZPOZ.REZZASOB=:zasob) AND
        (REZNAG.DATAOD<:dodaty) AND
        (REZNAG.DATADO>:oddaty) AND
        (REZNAG.STATUS<>0) AND
        (REZPOZ.REZNAG<>:reznagref)
  into :status;
  if (:status>0) then exception REZZASOBY_KONFLIKT;
  suspend;
end^
SET TERM ; ^
