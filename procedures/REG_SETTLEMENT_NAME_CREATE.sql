--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REG_SETTLEMENT_NAME_CREATE(
      SETTLEMENTIN varchar(20) CHARACTER SET UTF8                           ,
      NUMER smallint)
  returns (
      SETTLEMENTOUT varchar(20) CHARACTER SET UTF8                           )
   as
declare variable numers varchar(20);
begin
  numers = cast (:numer as varchar(20));
  settlementout = substring(:settlementin from  1 for 19 - coalesce(char_length(:numers),0)) ||'-'||:numers; -- [DG] XXX ZG119346
  if(settlementout is null or (settlementout = '')) then
    exception universal 'Pusty symbol rozrachunku';
  suspend;
end^
SET TERM ; ^
