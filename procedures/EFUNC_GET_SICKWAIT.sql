--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EFUNC_GET_SICKWAIT(
      EMPL integer,
      FROMDATE timestamp)
  returns (
      TODATE timestamp)
   as
declare variable pfdate date;
declare variable ptdate date;
declare variable tdate date;
declare variable fdate date;
declare variable days integer;
declare variable empldate date;
declare variable certdate date;
declare variable schooldate date;
declare variable seniority_years integer;
declare variable pers integer;
declare variable last_empl integer;
declare variable company integer;
declare variable cfromdate date;
declare variable ctodate date;
declare variable certdays integer;
begin
  days = 0;
  todate = null;
  pfdate = null;
  select person from employees where ref = :empl into :pers;
-- Pobranie daty fromdate z employee z nieobecnosci
-- Najszybszy select i najczestszy przypadek
  select fromdate
    from employees
    where ref = :empl
      and empltype = 1
    into :empldate;
  if(empldate is not null and empldate + 30 < fromdate) then
    exit;
  else if(empldate is null) then
  begin
--Sprawdzamy innych pracownikow powiazanych z dana osoba
    select min(fromdate)
      from employees
      where person = :pers
        and (emplstatus = 1
        or (emplstatus = 0 and todate + 30 < :fromdate))
      into :empldate;
    if(empldate is not null and empldate + 30 < fromdate) then
      exit;
    else if(empldate is null) then
    begin
      for
      select fromdate, todate
        from emplcontracts
        where (empltype = 2
          or empltype = 3)
          and iflags containing ';DFC;'
          and employee = :empl
        order by fromdate desc
        into fdate,tdate
      do begin
        if(ptdate + 29 > fdate) then
          days = 0;
        days = days + tdate - fdate;
        if(days >= 90) then
          exit;
        ptdate = tdate;
        pfdate = fdate;
      end
      empldate = fdate;
    end
  end
-- Sprawdzamy date konca umowy ze swiadectwa pracy jesli istnieje
  if(empldate is not null) then
  begin
    certdate = empldate;
    certdays = 0;
    for
      select fromdate, todate
        from eworkcertifs
        where employee = :empl and fromdate < :certdate
        order by todate desc
        into :cfromdate, :ctodate
    do begin
      if (cfromdate < certdate) then
      begin
        if (certdate is not null and certdate < ctodate) then
          ctodate = certdate - 1;
        if (ctodate + 30 > certdate) then
        begin
          certdays = certdays + ctodate - cfromdate + 1;
          certdate = cfromdate;
        end
        if(empldate + 30 - certdays < fromdate) then
          exit;
      end
    end
  end
-- Pobranie daty ukończenia szkoly dla osoby powiazanej z pracownikiem
  select max(s.todate)
    from employees e
      join eperschools s on s.person = e.person
    where e.ref = :empl
    into :schooldate;
  if(schooldate + 90 > empldate) then
    exit;
  select min(fromdate)
      from employees
      where person = :pers
      having min(fromdate) is not null
      into :empldate;
  if(empldate is not null) then
    select first 1 ref, company
      from employees
      where person = :pers
        and fromdate = :empldate
      into :last_empl, :company;
  select seniority_years from efunc_get_jobseniority(:last_empl,null,1,:company,0) into :seniority_years;
  if(seniority_years >= 10) then
    exit;

  todate = empldate + 29 - certdays;
  if(todate < fromdate) then
  begin
    todate = null;
  end
  suspend;
end^
SET TERM ; ^
