--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CENAZAKUPU_PROCENT(
      DEFCENNIKZAK_REF integer,
      PNAME varchar(20) CHARACTER SET UTF8                           ,
      WAR numeric(14,2))
  returns (
      PROC numeric(14,2))
   as
declare variable wst numeric(14,2);
declare variable od numeric(14,2);
begin
-- exception test_break :defcennikzak_ref||'|'||:ktm||'|'||:pname||'|'||:war;

  -- funkcja zwraca wartosc procentowa obnizki ceny
  -- wynikajaca z wartosci <war> cechy <pname>
  select procent, wartdom, odwart from defcenzak
    where defcennikzak = :defcennikzak_ref
      and parsymbol = :PName
      and odwart < :war
      and dowart >= :war
    into :proc, :wst, :od;
--  exception test_break :proc||' '||:wst||' '||:od;
  proc = coalesce(cast((:war - :od) * :proc as integer) + :wst,0);
  suspend;
end^
SET TERM ; ^
