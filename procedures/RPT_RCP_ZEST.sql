--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_RCP_ZEST(
      EMPL integer,
      FROMDATE date,
      TODATE date)
  returns (
      JOBTIME integer,
      JOBTIMESTR varchar(8) CHARACTER SET UTF8                           ,
      DAYKIND smallint)
   as
declare variable tmpstddate date;
  declare variable stdcalendar integer;
BEGIN
  --DS: funkcja zwraca liczbe godzin w poszczegolnych dniach, jaka zostala zarejestrowana w systemie
  execute procedure get_config('STDCALENDAR',2) returning_values :stdcalendar;
  --przechodzimy przez wszystkie dni kalendarza firmowego
  for
    select cdate, daykind
      from ecaldays
      where calendar = :stdcalendar
        and cdate >= :FROMDATE
        and cdate <= :TODATE
      order by cdate
      into :tmpstddate, :daykind
  do begin
    jobtime=null;
    select sum(p.jobtime)
      from epresencelists n
        join epresencelistspos p on n.ref = p.epresencelists
      where n.listsdate = :tmpstddate
        and p.employee = :empl
      into :jobtime;
    execute procedure durationstr2(jobtime) returning_values jobtimestr;
    suspend;
  end
end^
SET TERM ; ^
