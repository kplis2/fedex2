--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_FUN_OBROTY_BO(
      COMPANY integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      ACCOUNTS ACCOUNT_ID,
      SIDE smallint)
  returns (
      AMOUNT numeric(15,2))
   as
declare variable YEARID integer;
declare variable PERIOD_BO varchar(6);
declare variable DEBIT numeric(15,2);
declare variable CREDIT numeric(15,2);
begin
  -- obroty bez BO
  select yearid from bkperiods where id = :period
    and company = :company
    into :yearid;
  period_bo = cast(yearid as varchar(4)) || '00';
  accounts = replace(accounts,  '?',  '_') || '%';
  select sum(T.debit), sum(T.credit)
    from turnovers T
      join accounting A on (T.accounting = A.ref)
    where A.currency = 'PLN' and T.account like :accounts
      and T.period > :period_bo and T.period <= :period
      and T.company = :company
    into :debit, :credit;
  if (side=0) then
    amount = debit;
  else
    amount = credit;
  suspend;
end^
SET TERM ; ^
