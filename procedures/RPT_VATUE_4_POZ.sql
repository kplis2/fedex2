--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_VATUE_4_POZ(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      COMPANY COMPANIES_ID,
      EDECLARATIONREF EDECLARATIONS_ID,
      EDECPOSFIELD EDECLPOS_ID,
      RPTSECTION CHAR_1,
      OLD_EDECLDEFREF EDECLARATIONS_ID = 0,
      TESTPOZROWCOUNT SMALLINT_ID = 0)
  returns (
      OUTEDECPOSFIELD EDECLPOS_ID)
   as
declare variable numer integer_id;
declare variable a_old string2;
declare variable b_old varchar(15);
declare variable c_old integer_id;
declare variable d_old smallint_id;
declare variable a string2;
declare variable b varchar(15);
declare variable c integer_id;
declare variable d smallint_id;
declare variable data_row smallint_id;
declare variable subquery_id string10;
declare variable field_prefix string30;
declare variable field_prefix_end_char char_1;
declare variable field_root_prefix char_1;
begin
/*TS: FK - generuje pozycje dla e-Deklaracji dla wskazanej sekcji dokumentu.

  > EDECLARATIONREF - numer generowanej deklaracji z procedury nadrzednej (ta
    sama nazwa, bez '_POZ'), wartosc pola EDECLARATIONS.REF,
  > EDECPOSFIELD - pierwszy w kolejnosci wolny numer pola z tabeli EDECLPOS.
    Procedura nadrzedna wywoluje kolejno ta procedure dla wszystkich sekcji
    wydruku o zmiennej liczbie pozycji i kazda kolejna generuje kolejno numerowane pola.
  > RPTSECTION - dla ktorej sekcji wydruku generujemy dane do e-Deklaracji.
  > TESTPOZROWCOUNT - liczba wierszy z testowymi danymi, ktore zostana
    wygenerowane dla wskazanej sekcji dokumentu.
*/

  -- Ustawiamy prefixy pol, ktore beda dodawane do EDECLPOS.
  data_row = 1;
  subquery_id = :rptsection||'POZ';
  field_prefix_end_char = '.';
  field_prefix = :subquery_id||'_';
  field_root_prefix = 'K';

  -- Dokument VAT-UE(4) posiada trzy sekcje ze zmienna liczba pozycji - C, D i E.
  if (:rptsection not in ('C', 'D', 'E')) then
  begin
   exception universal 'Dla deklaracji VAT-UE(4) nie istnieje sekcja danych: '
     ||:rptsection||'! Podaj jedną z następujących: C, D, E.';
  end

  for
    execute statement 'select d.numer, d.a_old, d.b_old, d.c_old, d.d_old, '
      || 'd.a, d.b, d.c, d.d '
      || 'from get_data_vatue_4_'||:subquery_id
      || '('||:period||', '||:company||', '||:old_edecldefref
      || ', 0, '||:testpozrowcount||') d'
    into :numer, :a_old, :b_old, :c_old, :d_old,
      :a, :b, :c, :d

  do begin
    field_prefix = :subquery_id||'_'||:data_row||:field_prefix_end_char;

    /* Symbole pol sa formatu XPOZ_Y.Z, gdzie:
     > X to nazwa sekcji wydruku,
     > Y to numer wiersza w danej sekcji,
     > Z to nazwa pola danego wiersza.
    */

    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (
      :edeclarationref, :edecposfield + 0, :numer, 0, :field_prefix||'NUMER');

    if (old_edecldefref > 0) then
    begin
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (
      :edeclarationref, :edecposfield + 1, :a_old, 0, :field_prefix||'A_OLD');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (
      :edeclarationref, :edecposfield + 2, :b_old, 0, :field_prefix||'B_OLD');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (
      :edeclarationref, :edecposfield + 3, :c_old, 0, :field_prefix||'C_OLD');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (
      :edeclarationref, :edecposfield + 4, :d_old, 0, :field_prefix||'D_OLD');

    -- Utawiamy licznik pol w EDECLPOS dla e-Deklaracji.
    edecposfield = :edecposfield + 4;
    end

    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (
      :edeclarationref, :edecposfield + 1, :a, 0, :field_prefix||'A');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (
      :edeclarationref, :edecposfield + 2, :b, 0, :field_prefix||'B');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (
      :edeclarationref, :edecposfield + 3, :c, 0, :field_prefix||'C');
    insert into EDECLPOS (EDECLARATION, FIELD, PVALUE, ORIGIN, FIELDSYMBOL) VALUES (
      :edeclarationref, :edecposfield + 4, :d, 0, :field_prefix||'D');

    -- Utawiamy licznik pol w EDECLPOS dla e-Deklaracji.
    edecposfield = :edecposfield + 5;

    -- Wymuszamy zmiane prefixu pola.
    data_row = :data_row + 1;
  end

  outedecposfield = :edecposfield;
  suspend;
end^
SET TERM ; ^
