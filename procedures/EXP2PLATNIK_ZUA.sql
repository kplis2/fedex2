--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXP2PLATNIK_ZUA(
      EMPLCONTR_REF integer,
      DTYPE smallint,
      T_FROMDATE date,
      T_TODATE date,
      COMPANY integer)
  returns (
      PERSON integer,
      PESEL varchar(11) CHARACTER SET UTF8                           ,
      NIP varchar(15) CHARACTER SET UTF8                           ,
      FNAME varchar(60) CHARACTER SET UTF8                           ,
      SNAME varchar(30) CHARACTER SET UTF8                           ,
      MNAME varchar(30) CHARACTER SET UTF8                           ,
      MIDDLENAME varchar(30) CHARACTER SET UTF8                           ,
      BIRTHDATE date,
      CITIZENSHIP varchar(20) CHARACTER SET UTF8                           ,
      FROMDATE date,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      DOCNO varchar(11) CHARACTER SET UTF8                           ,
      SEX varchar(1) CHARACTER SET UTF8                           ,
      POSTCODE varchar(10) CHARACTER SET UTF8                           ,
      CITY varchar(255) CHARACTER SET UTF8                           ,
      COMMUNITY varchar(255) CHARACTER SET UTF8                           ,
      STREET varchar(255) CHARACTER SET UTF8                           ,
      HOUSENO varchar(20) CHARACTER SET UTF8                           ,
      LOCALNO varchar(20) CHARACTER SET UTF8                           ,
      PHONE varchar(255) CHARACTER SET UTF8                           ,
      POSTCODE1 varchar(10) CHARACTER SET UTF8                           ,
      CITY1 varchar(255) CHARACTER SET UTF8                           ,
      COMMUNITY1 varchar(255) CHARACTER SET UTF8                           ,
      STREET1 varchar(255) CHARACTER SET UTF8                           ,
      HOUSENO1 varchar(20) CHARACTER SET UTF8                           ,
      LOCALNO1 varchar(20) CHARACTER SET UTF8                           ,
      PHONE1 varchar(255) CHARACTER SET UTF8                           ,
      POSTCODE2 varchar(10) CHARACTER SET UTF8                           ,
      CITY2 varchar(255) CHARACTER SET UTF8                           ,
      COMMUNITY2 varchar(255) CHARACTER SET UTF8                           ,
      STREET2 varchar(255) CHARACTER SET UTF8                           ,
      HOUSENO2 varchar(20) CHARACTER SET UTF8                           ,
      LOCALNO2 varchar(20) CHARACTER SET UTF8                           ,
      PHONE2 varchar(255) CHARACTER SET UTF8                           ,
      EMAIL2 varchar(80) CHARACTER SET UTF8                           ,
      EDUCATION varchar(20) CHARACTER SET UTF8                           ,
      INSURDATE date,
      ASCODE varchar(20) CHARACTER SET UTF8                           ,
      INVALIDITY varchar(20) CHARACTER SET UTF8                           ,
      RETIRED varchar(20) CHARACTER SET UTF8                           ,
      NFZ varchar(20) CHARACTER SET UTF8                           ,
      DIMNUM integer,
      DIMDEN integer,
      DISABILITY varchar(20) CHARACTER SET UTF8                           ,
      NFZNAME varchar(1024) CHARACTER SET UTF8                           ,
      SPECCONDITION varchar(20) CHARACTER SET UTF8                           ,
      SPECFROM date,
      SPECTO date,
      WPCODE varchar(20) CHARACTER SET UTF8                           ,
      FE varchar(1) CHARACTER SET UTF8                           ,
      FR varchar(1) CHARACTER SET UTF8                           ,
      FC varchar(1) CHARACTER SET UTF8                           ,
      FW varchar(1) CHARACTER SET UTF8                           ,
      DFC varchar(1) CHARACTER SET UTF8                           ,
      ICODE varchar(6) CHARACTER SET UTF8                           ,
      INSUREENTRY integer,
      CHANGEENTRY varchar(1) CHARACTER SET UTF8                           ,
      DFCDATE date)
   as
declare variable CREF integer;
declare variable EZREF integer;
declare variable DTYPE_TMP smallint;
declare variable FROMDATE_TMP date;
begin
/*MWr: Personel - Eksport do Platnika deklar. zgloszeniowej ZUS ZUA - Zgloszenie
       do ubezpieczen/ Zgloszenie zmiany danych osoby ubezpieczonej (ver.xml).

  Dla EMPLCONTR_REF <= 0 (gener. deklar. w opraciu o wszystkie umowy z zadanego okresu):
    DTYPE: = 0 - zakres dat dotyczy zarowno wystawionych umow jak i zmiany kodu ubezp.
           = 1 - zakres dat okresla, ze analizowane beda umowy wystawione w zadanym okresie
           = 2 - zakres dat okresla okres, w ktorym nastapila zmiana kodu ubezp.
    T_FROMDATE = data 'Od' zakresu dat danego typu
    T_TODATE   = data 'Do' zakresu dat danego typu

  Dla EMPLCONTR_REF > 0 (generowanie ZUA do konkretnej umowy o ref'ie EMPLCONTR_REF):
    DTYPE: = 0 - zgloszenie do ubezpieczen
           = 1 - zgloszenie zmiany danych ososby ubezpieczonej
           = 2 - zgloszenie korekty danych ososby ubezpieczonej
    T_FROMDATE oraz T_TODATE sa tu nieistotne i nieobslugiwane (ale musi byc wpis w EZUSDATA) */

  nip = ''; --PR57778
  dtype = coalesce(dtype,0);
  emplcontr_ref = coalesce(emplcontr_ref,0);

  for
    select p.ref,
        case when (ez.ispesel = 1) then p.pesel else '' end as pesel,
        upper(p.fname), upper(p.sname), upper(p.middlename), upper(p.mname), p.birthdate, upper(p.citizenship), c.fromdate,
        case when (ez.isother = 1 and p.evidenceno > '') then '1' when (ez.isother = 2 and p.passportno > '') then '2' else '' end as doctype,
        case when (ez.isother = 1 and p.evidenceno > '') then p.evidenceno when (ez.isother = 2 and p.passportno > '') then p.passportno else '' end as docno,
        case when (p.sex = 0) then 'K' else 'M' end as sex,
        a0.postcode, upper(a0.city), upper(g0.descript), upper(a0.street), upper(a0.houseno), upper(a0.localno), a0.phone,
        a1.postcode, upper(a1.city), upper(g1.descript), upper(a1.street), upper(a1.houseno), upper(a1.localno), a1.phone,
        a2.postcode, upper(a2.city), upper(g2.descript), upper(a2.street), upper(a2.houseno), upper(a2.localno), a2.phone, upper(p.email),
        case when c.empltype = 1 then ez.insurdate else c.fromdate end as insurdate,
        case when c.empltype = 1 then z1.code else z8.code end as ascode,
        z.code as education, z2.code as invalidity, z3.code as retired, z4.code as nfz, c.dimnum, c.dimden,
        z6.code as disability, z4.descript as nfzname, z7.code as speccondition, ez.specfrom, ez.specto, g2.code as wpcode,
        case when (t.empltype = 1 or c.iflags like '%%;FE;%%') then 'X' else '' end as fe,
        case when (t.empltype = 1 or c.iflags like '%%;FR;%%') then 'X' else '' end as fr,
        case when (t.empltype = 1 or c.iflags like '%%;FC;%%') then 'X' else '' end as fc,
        case when (t.empltype = 1 or c.iflags like '%%;FW;%%') then 'X' else '' end as fw,
        case when (c.iflags like '%%;DFC;%%') then 'X' else '' end as dfc, c.ref, ez.ref
      from employees e
        join persons p on (p.ref = e.person)
        join emplcontracts c on (e.ref = c.employee)
        join econtrtypes t on (c.econtrtype = t.contrtype)
        left join epersaddr a0 on (a0.person = p.ref and a0.addrtype = 0 and a0.status = 1)
        left join edictguscodes g0 on (a0.communityid = g0.ref)
        left join epersaddr a1 on (a1.person = p.ref and a1.addrtype = 1 and a1.status = 1)
        left join edictguscodes g1 on (a1.communityid = g1.ref)
        left join epersaddr a2 on (a2.person = p.ref and a2.addrtype = 2 and a2.status = 1)
        left join edictworkposts w on (e.workpost = w.ref)
        left join edictguscodes g2 on (w.guscode = g2.ref)
        left join edictzuscodes z on (p.education = z.ref)
        left join ezusdata ez on (ez.person = p.ref and (ez.ascode = c.ascode or c.ascode is null))
        left join edictzuscodes z1 on (z1.ref = ez.ascode)
        left join edictzuscodes z2 on (z2.ref = ez.invalidity)
        left join edictzuscodes z3 on (z3.ref = ez.retired)
        left join edictzuscodes z4 on (z4.ref = ez.nfz)
        left join edictzuscodes z6 on (z6.ref = ez.disability)
        left join edictzuscodes z7 on (z7.ref = ez.speccondition)
        left join edictzuscodes z8 on (z8.ref = c.ascode)
      where e.company = :company
        and (t.empltype = 1
          or c.iflags like '%%FC;%%'
          or c.iflags like '%%;FE;%%'
          or c.iflags like '%%;FR;%%'
          or c.iflags like '%%;FW;%%')
        --obsluga wybranej umowy
        and ((c.ref = :emplcontr_ref
            and ez.fromdate = (select max(ez1.fromdate) from ezusdata ez1
                                   where ez1.person = ez.person
                                     and (ez1.fromdate <= coalesce(c.enddate,c.todate) or c.todate is null)))
        --generowanie wszystkich pracownikow spelniajacych zadane warunki
          or (:emplcontr_ref <= 0
          --dane z ostatniej umowy
            and not exists (select first 1 1 from emplcontracts c2
                                where e.ref = c2.employee
                                  and ((c2.empltype = 1 and c2.fromdate < c.fromdate)
                                    or (c2.empltype > 1 and c2.enddate = c.fromdate - 1 and c2.ascode = c.ascode
                                      and (c2.iflags like '%%FC;%%'
                                        or c2.iflags like '%%;FE;%%'
                                        or c2.iflags like '%%;FR;%%'
                                        or c2.iflags like '%%;FW;%%'))))
            --umowy wystawione w zadanym okresie
            and (((:dtype <= 1 and c.fromdate >= :t_fromdate and c.fromdate <= :t_todate)
                 and ez.fromdate = (select max(ez1.fromdate) from ezusdata ez1
                                    where ez1.person = ez.person
                                      and ez1.fromdate <= :t_todate))
            --zmiana danych ubezpieczeniowych
              or (:dtype in (0,2)
                and (ez.fromdate <= coalesce(c.enddate,c.todate) or c.todate is null)    
                and ez.fromdate = (select max(ez1.fromdate)
                                     from ezusdata ez1
                                     where ez1.person = p.ref
                                       and ez1.fromdate <= :t_todate
                                       and ez1.fromdate >= :t_fromdate
                                       and exists (select first 1 1 from ezusdata ez2
                                                     where ez2.person = ez1.person
                                                       and ez2.ref <> ez1.ref
                                                       and (ez2.ascode <> ez1.ascode or ez2.retired <> ez1.retired or ez2.invalidity <> ez1.invalidity)
                                                       and ez2.fromdate < ez1.fromdate))))))
      into :person, :pesel, :fname, :sname, :middlename, :mname, :birthdate, :citizenship, :fromdate, :doctype, :docno, :sex,
        :postcode,  :city,  :community,  :street,  :houseno,  :localno,  :phone,
        :postcode1, :city1, :community1, :street1, :houseno1, :localno1, :phone1,
        :postcode2, :city2, :community2, :street2, :houseno2, :localno2, :phone2, :email2,
        :insurdate, :ascode, :education, :invalidity, :retired, :nfz,
        :dimnum, :dimden, :disability, :nfzname, :speccondition, :specfrom, :specto, :wpcode,
        :fe, :fr, :fc, :fw, :dfc, :cref, :ezref
  do begin
  --Kod ubezpieczenia dla deklaracji xml:
    icode = coalesce(ascode,'0000') || coalesce(retired,'0') || coalesce(invalidity,'0');

    if (dfc <> '') then --BS45112
    begin
      fromdate_tmp = fromdate;
      while (fromdate_tmp is not null) do
      begin
        dfcdate = fromdate_tmp;
        fromdate_tmp = null;
        select first 1 c.fromdate from emplcontracts c
          join employees e on (e.ref = c.employee and e.person = :person)
          where c.empltype > 1 and c.iflags like '%%;DFC;%%'
            and c.fromdate <= (:dfcdate - 1)
            and (c.enddate >= (:dfcdate - 1) or c.enddate is null)
          order by c.fromdate
          into :fromdate_tmp;
      end
    end else
      dfcdate = null;

  /*Okreslenie wartosci znacznikow na deklracji: 'zgloszenie do ubezpieczenia' INSUREENTRY
    oraz 'korekta/zmiana danych osoby' CHANGEENTRY (PR38232): */
    if (emplcontr_ref <= 0) then
    begin
      if (dtype = 0) then
      begin
      /*Proba okreslania, czy zwracana pozycja dotyczy zgloszenia czy zmiany danych;
        Zalozono, ze jezeli umowa wystawiona w zadanym okresie to mamy zgloszenie.*/
        if (exists(select first 1 1 from emplcontracts c
                     join ezusdata ez on (ez.ref = :ezref)
                     where c.ref = :cref
                       and c.fromdate >= :t_fromdate and c.fromdate <= :t_todate
                       and ez.fromdate = (select max(ez1.fromdate) from ezusdata ez1
                                            where ez1.person = ez.person
                                              and ez1.fromdate <= :t_todate))
        ) then
          dtype_tmp = 1; --zgloszenie danych
        else
          dtype_tmp = 2; --zmiana danych
      end else
        dtype_tmp = dtype;

      insureentry = iif(dtype_tmp = 1, 1, 0);
      changeentry = iif(dtype_tmp = 1, '', '1');
    end else begin
      insureentry = iif(dtype = 0, 1, 0);
      changeentry = iif(dtype = 0, '', cast(dtype as varchar(1)));
    end

    suspend;
  end
end^
SET TERM ; ^
