--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_TO_NAGZAM(
      NAGFAKREF integer,
      TYPZAM varchar(3) CHARACTER SET UTF8                           ,
      REJZAM varchar(3) CHARACTER SET UTF8                           ,
      OKRES varchar(6) CHARACTER SET UTF8                           )
  returns (
      STATUS integer)
   as
declare variable newnagzamref integer;
declare variable oddzial varchar(10);
declare variable klient integer;
declare variable platnik integer;
declare variable uzykli integer;
declare variable data timestamp;
declare variable sposzap integer;
declare variable odbiorcaid integer;
declare variable termdost timestamp;
declare variable sposdost integer;
declare variable trasadost integer;
declare variable wysylka integer;
declare variable rabat numeric(14,2);
declare variable uwagi varchar(1024);
declare variable waluta varchar(3);
declare variable kurs numeric(14,4);
declare variable operator integer;
declare variable magazyn varchar(3);
--zmienne dotyczace pozycji
declare variable pnumer integer;
declare variable pmagazyn varchar(3);
declare variable pktm varchar(80);
declare variable pwersja integer;
declare variable pjedn integer;
declare variable pjedno integer;
declare variable pilosco numeric(14,4);
declare variable pilosc numeric(14,4);
declare variable piloscm numeric(14,4);
declare variable pcenacen numeric(14,4);
declare variable pwalcen varchar(3);
declare variable prabat numeric(14,2);
declare variable pcenanet numeric(14,4);
declare variable pwersjaref integer;
declare variable pcenanetzl numeric(14,4);
declare variable pserialized smallint;
declare variable popis varchar(1024);
declare variable pparamn1 numeric(14,2);
declare variable pparamn2 numeric(14,2);
declare variable pparamn3 numeric(14,2);
declare variable pparamn4 numeric(14,2);
declare variable pparamn5 numeric(14,2);
declare variable pparamn6 numeric(14,2);
declare variable pparamn7 numeric(14,2);
declare variable pparamn8 numeric(14,2);
declare variable pparamn9 numeric(14,2);
declare variable pparamn10 numeric(14,2);
declare variable pparamn11 numeric(14,2);
declare variable pparamn12 numeric(14,2);
declare variable pparams1 varchar(40);
declare variable pparams2 varchar(40);
declare variable pparams3 varchar(40);
declare variable pparams4 varchar(40);
declare variable pparams5 varchar(40);
declare variable pparams6 varchar(40);
declare variable pparams7 varchar(40);
declare variable pparams8 varchar(40);
declare variable pparams9 varchar(40);
declare variable pparams10 varchar(40);
declare variable pparams11 varchar(40);
declare variable pparams12 varchar(40);
declare variable pparamd1 timestamp;
declare variable pparamd2 timestamp;
declare variable pparamd3 timestamp;
declare variable pparamd4 timestamp;
declare variable bn char(1);
declare variable prec smallint;
declare variable frompozfak pozfak_id;
declare variable alttopozn pozzam_id;
declare variable fake smallint;
declare variable AltToPoz pozzam_id;
begin

  status = 0;
  if (nagfakref is not null and nagfakref <> 0
    and typzam is not null and typzam <> ''
    and rejzam is not null and rejzam <> ''
    and okres is not null and okres <> '') then begin
-- kopiowanie naglowka
    execute procedure GEN_REF('NAGZAM') returning_values :newnagzamref;
    select nagfak.oddzial, nagfak.klient, nagfak.platnik, nagfak.uzykli, nagfak.data,
           nagfak.sposzap, nagfak.odbiorcaid, nagfak.termdost, nagfak.sposdost,
           nagfak.trasadost, nagfak.wysylka, nagfak.rabat, nagfak.uwagi, nagfak.waluta,
           nagfak.kurs, nagfak.operator, nagfak.magazyn, nagfak.bn
    from nagfak
    where nagfak.ref = :nagfakref
    into   :oddzial, :klient, :platnik, :uzykli, :data,
           :sposzap, :odbiorcaid, :termdost, :sposdost,
           :trasadost, :wysylka, :rabat, :uwagi, :waluta,
           :kurs, :operator, :magazyn,:bn;

    insert into nagzam (
            nagzam.ref, nagzam.rejestr, nagzam.typzam, nagzam.okres,
            nagzam.oddzial, nagzam.klient, nagzam.platnik, nagzam.uzykli, nagzam.datawe,
            nagzam.sposzap,  nagzam.odbiorcaid, nagzam.termdost,  nagzam.sposdost,
            nagzam.trasadost, nagzam.wysylka, nagzam.rabat, nagzam.uwagi, nagzam.waluta,
            nagzam.kurs, nagzam.operator, nagzam.magazyn, nagzam.bn)
      values(
            :newnagzamref, :rejzam, :typzam, :okres,
            :oddzial, :klient, :platnik, :uzykli, :data,
            :sposzap,  :odbiorcaid, :termdost, :sposdost,
            :trasadost, :wysylka, :rabat, :uwagi, :waluta,
            :kurs, :operator, :magazyn,:bn);
-- kopiowanie pozycji

    for select
            pozfak.numer, pozfak.ktm, pozfak.wersja, pozfak.jedn, pozfak.jedno,
            pozfak.ilosco, pozfak.ilosc, pozfak.iloscm, pozfak.cenacen, pozfak.rabat,
            pozfak.walcen, pozfak.opis, pozfak.magazyn,
            pozfak.serialized,
            pozfak.paramn1, pozfak.paramn2, pozfak.paramn3, pozfak.paramn4,
            pozfak.paramn5, pozfak.paramn6, pozfak.paramn7, pozfak.paramn8,
            pozfak.paramn9, pozfak.paramn10, pozfak.paramn11, pozfak.paramn12,
            pozfak.params1, pozfak.params2, pozfak.params3, pozfak.params4,
            pozfak.params5, pozfak.params6, pozfak.params7, pozfak.params8,
            pozfak.params9, pozfak.params10, pozfak.params11, pozfak.params12,
            pozfak.paramd1, pozfak.paramd2, pozfak.paramd3, pozfak.paramd4,
            pozfak.cenanet, pozfak.wersjaref, pozfak.cenanetzl, pozfak.prec,
            pozfak.fake   , pozfak.alttopoz, pozfak.ref
       from pozfak
       where pozfak.dokument = :nagfakref
       order by pozfak.numer, pozfak.alttopoz nulls first
       into
            :pnumer, :pktm, :pwersja, pjedn, pjedno,
            :pilosco, :pilosc, :piloscm, :pcenacen, :prabat,
            :pwalcen, :popis, :pmagazyn,
            :pserialized,
            :pparamn1, :pparamn2, :pparamn3, :pparamn4,
            :pparamn5, :pparamn6, :pparamn7, :pparamn8,
            :pparamn9, :pparamn10, :pparamn11, :pparamn12,
            :pparams1, :pparams2, :pparams3, :pparams4,
            :pparams5, :pparams6, :pparams7, :pparams8,
            :pparams9, :pparams10, :pparams11, :pparams12,
            :pparamd1, :pparamd2, :pparamd3, :pparamd4,
            :pcenanet, :pwersjaref, :pcenanetzl, :prec,
            :fake    , :AltToPoz,  :FromPozFak
    do begin
       if (AltToPoz is not null) then
       begin
         select ref
           from pozzam
           where frompozfak = AltToPoz
           into :AltToPozN;
       end
       insert into pozzam
           (zamowienie, numer, ord, magazyn, ktm, wersja, jedn, jedno,
            ilosco, ilosc, iloscm, cenacen, walcen, rabat, cenanet,
            wersjaref, cenanetzl, serialized, opis,
            paramn1, paramn2, paramn3, paramn4,
            paramn5, paramn6, paramn7, paramn8,
            paramn9, paramn10, paramn11, paramn12,
            params1, params2, params3, params4,
            params5, params6, params7, params8,
            params9, params10, params11, params12,
            paramd1, paramd2, paramd3, paramd4, prec,
            fake,    AltToPoz, FromPozFak )
       values
           (:newnagzamref, :pnumer, 1, :pmagazyn, :pktm, :pwersja, :pjedn, :pjedno,
            :pilosco, :pilosc, :piloscm, :pcenacen, :pwalcen, :prabat, :pcenanet,
            :pwersjaref, :pcenanetzl, :pserialized, :popis,
            :pparamn1, :pparamn2, :pparamn3, :pparamn4,
            :pparamn5, :pparamn6, :pparamn7, :pparamn8,
            :pparamn9, :pparamn10, :pparamn11, :pparamn12,
            :pparams1, :pparams2, :pparams3 ,:pparams4,
            :pparams5, :pparams6, :pparams7, :pparams8,
            :pparams9, :pparams10, :pparams11, :pparams12,
            :pparamd1, :pparamd2, :pparamd3 ,:pparamd4, :prec,
            :fake,     :AltToPoz,  :FromPozFak);
    end
    delete from nagfak where nagfak.ref = :nagfakref;
    status = 1;
  end
end^
SET TERM ; ^
