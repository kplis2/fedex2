--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ADD_SETTLEMENT_TO_CLRACC(
      FSCLRACCH integer,
      ACCOUNT ACCOUNT_ID,
      SETTLEMENT varchar(20) CHARACTER SET UTF8                           ,
      DICTDEF integer,
      DICTPOS integer,
      MAXDEBIT numeric(14,2),
      MAXCREDIT numeric(14,2))
   as
declare variable rozrachp integer;
declare variable debit numeric(14,2);
declare variable credit numeric(14,2);
declare variable currdebit numeric(14,2);
declare variable currcredit numeric(14,2);
declare variable debit2c numeric(14,2);
declare variable credit2c numeric(14,2);
declare variable currdebit2c numeric(14,2);
declare variable currcredit2c numeric(14,2);
declare variable rate numeric(12,4);
declare variable dorecord integer;
declare variable autopos integer;
declare variable operdate timestamp;
begin
  dorecord = 1;
  autopos = 1;
  if (maxdebit is null) then
    maxdebit = 0;
  if (maxcredit is null) then
    maxcredit = 0;
  for
    select ref, currdebit, currcredit, debit, credit, rate, operdate
      from get_unbalanced_rozrachp(:account, :settlement, :dictdef, :dictpos)
      order by operdate desc
      into :rozrachp, :currdebit, :currcredit, :debit, :credit, :rate, :operdate
  do begin
    if (maxdebit <> 0 or maxcredit <> 0) then
    begin
      if (maxcredit > currcredit) then
      begin
        currcredit2c  = currcredit;
        credit2c = credit;
      end else begin
        currcredit2c = maxcredit;
        credit2c = maxcredit * rate;
      end
      maxcredit = maxcredit - currcredit2c;
      if (maxdebit > currdebit) then
      begin
        currdebit2c = currdebit;
        debit2c = debit;
      end else begin
        currdebit2c = maxdebit;
        debit2c = maxdebit * rate;
      end
      maxdebit = maxdebit - currdebit2c;
      autopos = 0;
      if (debit2c = 0 and credit2c = 0) then
        dorecord = 0;
    end

    if (dorecord = 1) then
    begin
      if (not exists(select ref from fsclraccp where fsclracch = :fsclracch and rozrachp = :rozrachp)) then
        insert into fsclraccp (fsclracch, rozrachp, currdebit, currcredit, debit, credit,
          currdebit2c, currcredit2c, debit2c, credit2c, rate, autopos, operdate)
        values (:fsclracch, :rozrachp, :currdebit, :currcredit, :debit, :credit,
          :currdebit2c, :currcredit2c, :debit2c, :credit2c, :rate, :autopos, :operdate);
    end
  end
end^
SET TERM ; ^
