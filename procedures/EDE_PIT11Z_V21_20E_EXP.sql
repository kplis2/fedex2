--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_PIT11Z_V21_20E_EXP(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable ISDECLARATION integer;
declare variable DEF varchar(20);
declare variable CODE varchar(10);
declare variable SYSTEMCODE varchar(20);
declare variable OBLIGATIONKIND varchar(20);
declare variable SCHEMAVER varchar(10);
declare variable SYMBOL varchar(10);
declare variable VARIANT varchar(10);
declare variable PVALUE varchar(255);
declare variable TMPVAL1 varchar(255);
declare variable TMPVAL2 varchar(255);
declare variable TMP smallint;
declare variable CURR_P integer;
declare variable CORRECTION smallint;
declare variable ENCLOSUREOWNER integer;
declare variable ZALTYP varchar(20);
declare variable ISZAL smallint;
declare variable PARENT_LVL_0 integer;
declare variable PARENT_LVL_1 integer;
declare variable PARENT_LVL_2 integer;
declare variable PARENT_LVL_3 integer;
declare variable PARENT_LVL_4 integer;
declare variable FIELD integer;
declare variable pref integer;
begin

  --Sprawdzamy czy dana deklaracja istnieje do eksportu
  select first 1 1 from edeclarations where ref = :oref
    into :isDeclaration;

  if (isDeclaration is null) then
    exception universal 'Niepoprawny identyfikator e-deklaracji';

  --Pobieranie definicji
  select e.edecldef, ed.code, ed.systemcode, ed.obligationkind, ed.schemaver, ed.symbol, ed.variant
    from edeclarations e join edecldefs ed on (ed.ref = e.edecldef)
    where e.ref = :oref
    into :def, :code, :systemcode, :obligationkind, :schemaver, :symbol, :variant;

  --Generujemy naglowek pliku XML
  id = 0;
  name = 'tns:Deklaracja';
  parent = null;
  params = 'xmlns:tns="http://crd.gov.pl/wzor/2014/12/22/1949/" xmlns:etd="http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2011/06/21/eD/DefinicjeTypy/"';
  val = null;
  suspend;

  correction = 0;
  parent_lvl_0 = 0;
  --Rozpoczynamy naglowek e-deklaracji
  id = id + 1; --1
  name = 'tns:Naglowek';
  parent = parent_lvl_0;
  params = null;
  val = null;
  parent_lvl_1 = :id;
  suspend;
  ----------------------------
    id = id + 1;
    name = 'tns:KodFormularza';
    parent = parent_lvl_1;
    params = '';
    if(code is not null) then
      params = params || 'kodPodatku="' || code || '" ';
    if(systemcode is not null) then
      params = params || 'kodSystemowy="' || systemcode || '" ';
    if(obligationkind is not null) then
      params = params || 'rodzajZobowiazania="' || obligationkind || '" ';
    if(schemaver is not null) then
      params = params || 'wersjaSchemy="' || schemaver || '"';
    val = symbol;
    suspend;
    
    --WariantFormularza
    id = id + 1;
    name = 'tns:WariantFormularza';
    parent = parent_lvl_1;
    params = null;
    val = variant;
    suspend;
    --  A  ---------------------------------->
    -- miejsce i cel skladania formularza

    val = null;
    select field, pvalue from edeclpos
      where edeclaration = :oref
        and field = 6
        and pvalue is not null
      order by field
      into :field, :val;
    if (field = 6) then
    begin
      id = id + 1;
      params = 'poz="P_6"';
      name = 'tns:CelZlozenia';
      suspend;
    end

    params = null;

    for
      select field, pvalue from edeclpos
        where edeclaration = :oref
          and field in (4,5)
          and pvalue is not null
        order by field
        into :field, :val
    do begin
     id = id + 1;
     if (field = 4) then name = 'tns:Rok';
     else if (field = 5) then name = 'tns:KodUrzedu';
     suspend;
    end

  id = id + 1;
  name = 'tns:Podmiot1';
  parent = parent_lvl_0;
  params = 'rola="Płatnik"';
  val = null;
  parent_lvl_1 = :id;
  suspend;
    --  B  -------------------------------------------------------->
    -- Dane identygikacyjne platnika
    select pvalue from edeclpos
      where edeclaration = :oref
        and field = 7
        and pvalue is not null
      into :pvalue;

    id = id + 1;
    if (pvalue = 1) then name = 'etd:OsobaNiefizyczna';
    else if (pvalue = 2) then name = 'etd:OsobaFizyczna';
    params = null;
    parent = parent_lvl_1;
    parent_lvl_2 = :id;
    suspend;
    --  B  -------------------------------------------------------->
      val = null;
      params = null;
      parent = parent_lvl_2;
      for
        select field, pvalue from edeclpos
          where edeclaration = :oref
            and field in (1,8,9)
            and pvalue is not null
          order by field
          into :field, :val
      do begin
        if (field = 1) then
        begin
          name = 'etd:NIP';
          id = id + 1;
          suspend;
        end else
        if (field = 8) then
        begin
          for select upper(stringout), lp from parsestring(:val, ', ')
            order by lp
            into :tmpval2, tmp
          do begin
            id = id + 1;
            val = tmpval2;
            if (tmp = 1) then name = 'etd:PelnaNazwa';
            else if (tmp = 2) then name = 'etd:REGON';
            suspend;
          end
        end else
        if (field = 9) then
        begin
          for select upper(stringout), lp from parsestring(:val, ', ')
            order by lp
            into :tmpval2, tmp
          do begin
            if (tmp = 1) then tmpval1 = :tmpval2; --name = 'etd:Nazwisko';
            else if (tmp = 2) then
            begin
              id = id + 1;
              name = 'etd:ImiePierwsze';
              val = tmpval2;             
              suspend;
              id = id + 1;
              name = 'etd:Nazwisko';
              val = tmpval1;
              suspend;
            end else
            if (tmp = 3) then
            begin
              id = id + 1;
              val = tmpval2;
              name = 'etd:DataUrodzenia';
              suspend;
            end
          end
        end
      end

  --  C  ---------------------------------------->
  for
    select ref from edeclarations where declgroup = :oref
    into pref
  do begin

    id = id + 1;
    name = 'tns:Pozycja';
    parent = parent_lvl_0;
    params = 'typ="G"';
    val = null;
    parent_lvl_1 = :id;
    suspend;

    select pvalue from edeclpos
      where edeclaration = :pref
        and field = 5
        and pvalue is not null
      into :val;
    id = id + 1;
    parent = parent_lvl_1;
    params = null;
    name = 'tns:KodUrzeduP';
    suspend;
  
    id = id + 1;
    name = 'tns:Podmiot2';
    parent = parent_lvl_1;
    params = 'rola="Podatnik"';
    val = null;
    parent_lvl_2 = :id;
    suspend;
    -- Dane podatnika
      id = id + 1;
      name = 'etd:OsobaFizyczna';
      parent = parent_lvl_2;
      params = null;
      val = null;
      parent_lvl_3 = :id;
      suspend;
      --  C1 Dane identyfikacyjne -------------------------
        parent = parent_lvl_3;
        for
          select field, trim(upper(pvalue)) from edeclpos e
            where edeclaration = :pref
              and pvalue is not null
              and field in (10,12,13)
            order by field
            into :field, :val
        do begin
          id = id + 1;
          if (field = 10) then
          begin
            if (char_length(:val) <> 11) then name = 'etd:NIP';
            else name = 'etd:PESEL';
          end else
          if (field = 12) then  -- bramka wymaga najpierw podania imienia, nastepnie nazwiska
          begin
            name = 'etd:ImiePierwsze';
            suspend;
            id = id + 1;
            name = 'etd:Nazwisko';
            select trim(upper(pvalue)) from edeclpos
              where edeclaration = :pref
                and pvalue is not null
                and field = 11
              into :val;
          end else
          if (field = 13) then
            name = 'etd:DataUrodzenia';
          suspend;
        end
      --  C2  Adres Zamieszkania -------------------------
      id = id + 1;
      name = 'etd:AdresZamieszkania';
      parent = parent_lvl_2;
      params = 'rodzajAdresu="RAD"';
      val = null;
      parent_lvl_3 = :id;
      suspend;
      ------------------------------------------->
        id = id+1;
        name = 'etd:AdresPol';
        parent = parent_lvl_3;
        params = null;
        --val = null;  jest juz nullem
        parent_lvl_4 = :id;
        suspend;
        --------------------------------------------->
          parent = parent_lvl_4;
          for
            select field, trim(upper(pvalue)) from edeclpos
              where edeclaration = :pref
                and pvalue is not null
                and field in (14,15,16,17,18,19,20,21,22,23)
              order by field
              into :field, :val
          do begin
            id = id + 1;
            if (field = 14) then name = 'etd:KodKraju';
            else if (field = 15) then name = 'etd:Wojewodztwo';
            else if (field = 16) then name = 'etd:Powiat';
            else if (field = 17) then name = 'etd:Gmina';
            else if (field = 18) then name = 'etd:Ulica';
            else if (field = 19) then name = 'etd:NrDomu';
            else if (field = 20) then name = 'etd:NrLokalu';
            else if (field = 21) then name = 'etd:Miejscowosc';
            else if (field = 22) then name = 'etd:KodPocztowy';
            else if (field = 23) then name = 'etd:Poczta';
            suspend;
          end
  
      id = id + 1;
      name = 'tns:PozycjeSzczegolowe';
      parent = parent_lvl_1;
      params = null;
      val = null;
      parent_lvl_2 = :id;
      suspend;
        -- D,E,F  ------------------------------->
        parent = parent_lvl_2;
          for
            select field, upper(pvalue) from edeclpos
              where edeclaration = :pref
                and pvalue is not null
                and field >= 24
                and field <= 72
              order by field
              into :field, :val
          do begin
            id = id + 1;
            name = 'tns:P_'||field;
            suspend;
          end
    end
    id = id + 1;
    name = 'tns:Pouczenie';
    parent = parent_lvl_0;
    val = 'Za uchybienie obowiązkom płatnika grozi odpowiedzialność przewidziana w Kodeksie karnym skarbowym.';
    suspend;
end^
SET TERM ; ^
