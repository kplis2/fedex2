--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_SRVREQUESTS(
      PMPLAN integer = NULL)
  returns (
      REF integer,
      REQDATE date,
      CDAYS integer,
      SDAYS integer,
      SPRAWACRM varchar(255) CHARACTER SET UTF8                           ,
      TYPZGLOSZENIA varchar(255) CHARACTER SET UTF8                           ,
      FAZA varchar(255) CHARACTER SET UTF8                           ,
      SYMBOL varchar(255) CHARACTER SET UTF8                           ,
      KLIENTG varchar(255) CHARACTER SET UTF8                           ,
      KLIENT varchar(255) CHARACTER SET UTF8                           ,
      SERWISANTG varchar(255) CHARACTER SET UTF8                           ,
      SERWISANT varchar(255) CHARACTER SET UTF8                           ,
      UMOWA varchar(255) CHARACTER SET UTF8                           ,
      OPERACJA varchar(255) CHARACTER SET UTF8                           ,
      TYP varchar(10) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      PRZYCHOD numeric(14,2),
      KOSZT numeric(14,2),
      ZYSK numeric(14,2),
      CZASPLAN numeric(14,2),
      CZASWYK numeric(14,2),
      ILOSCZGL integer,
      ILOSCOPER integer,
      ROK varchar(4) CHARACTER SET UTF8                           ,
      OKRES varchar(6) CHARACTER SET UTF8                           ,
      GWARANCYJNE varchar(10) CHARACTER SET UTF8                           )
   as
declare variable crecdate date;
declare variable srecdate date;
declare variable spassdate date;
declare variable cpassdate date;
declare variable pnumber integer;
declare variable usluga integer;
begin
  for select
  p.ref,
  s.reqdate,
  s.crecdate,
  s.srecdate,
  s.spassdate,
  s.cpassdate,
  ckontrakty.nazwa,
  contrtypes.symbol,
  cfazy.nazwa,
  s.symbol,
  rslodef.nazwa,
  s.repname,
  eslodef.nazwa,
  s.exename,
  srvcontracts.symbol,
  srvdefpos.descript,
  srvdefpos.usluga,
  p.ktm,
  p.amount * p.netsaleprice,
  p.amount * p.price,
  p.contracttime,
  p.worktime,
  p.number,
  case when s.guaranty=1 then 'TAK' else 'NIE' end

  from srvrequests s
  join srvpositions p on (p.srvrequest=s.ref)
  left join ckontrakty on (ckontrakty.ref=s.contract)
  left join contrtypes on (contrtypes.ref=s.contrtype)
  left join cfazy on (cfazy.ref=s.status)
  left join slodef rslodef on (rslodef.ref=s.repdictdef)
  left join slodef eslodef on (eslodef.ref=s.exedictdef)
  left join srvcontracts on (srvcontracts.ref=s.srvcontract)
  left join srvdefpos on (srvdefpos.ref=p.srvdefpos)
  where (srvcontracts.pmplan=:pmplan or :pmplan is null)
  into
    :ref,
    :reqdate,
    :crecdate,
    :srecdate,
    :spassdate,
    :cpassdate,
    :sprawacrm,
    :typzgloszenia,
    :faza,
    :symbol,
    :klientg,
    :klient,
    :serwisantg,
    :serwisant,
    :umowa,
    :operacja,
    :usluga,
    :ktm,
    :przychod,
    :koszt,
    :czasplan,
    :czaswyk,
    :pnumber,
    :gwarancyjne

  do begin
    if(:crecdate is null) then crecdate = :reqdate;
    if(:srecdate is null) then srecdate = :reqdate;
    if(:spassdate is null) then spassdate = current_date;
    if(:cpassdate is null) then cpassdate = current_date;
    cdays = :cpassdate - :crecdate + 1;
    sdays = :spassdate - :srecdate + 1;
    if(:pnumber>1) then begin
      cdays = 0;
      sdays = 0;
    end
    if(:usluga=1) then typ='R'; else typ = 'M';
    zysk = :przychod - :koszt;
    if(:pnumber=1) then ilosczgl = 1;
    else ilosczgl = 0;
    iloscoper = 1;
    select first 1 OKRES from datatookres(:reqdate,null,null,null,null) into :okres;
    rok = substring(:okres from 1 for 4);
    suspend;
  end
end^
SET TERM ; ^
