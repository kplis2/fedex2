--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDZAM_ADDGROUP(
      SCHEDZAMREF integer)
  returns (
      COUNTADD integer)
   as
declare variable zamref integer;
declare variable groupdep varchar(20);
declare variable departref integer;
declare variable prschedule integer;
declare variable prnagzam integer;
declare variable nagzam integer;
begin
  countadd = 1;
  select s.prschedgroup, z.zamowienie, n.kpopzam
    from prschedzam z
      left join prschedules s on (s.ref = z.prschedule)
      left join nagzam n on (z.zamowienie = n.ref)
    where z.ref = :schedzamref
  into :groupdep, :prnagzam, :nagzam;
  for select s.ref, n.ref
    from nagzam n
    left join prschedules s on (n.prdepart = s.prdepart)
    where s.prschedgroup = :groupdep  and n.kpopzam = :nagzam
      and n.ref <> :prnagzam
    into :prschedule, :zamref
  do begin
    if (not exists(select ref from prschedzam z where z.zamowienie = :zamref and z.prschedule = :prschedule)) then begin
      insert into prschedzam(zamowienie, prschedule) values(:zamref, :prschedule);
      countadd = countadd + 1;
    end
  end
  suspend;
end^
SET TERM ; ^
