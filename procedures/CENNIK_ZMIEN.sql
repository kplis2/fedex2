--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CENNIK_ZMIEN(
      CENNIK integer,
      KTMMASKA varchar(40) CHARACTER SET UTF8                           ,
      ODKTM varchar(40) CHARACTER SET UTF8                           ,
      DOKTM varchar(40) CHARACTER SET UTF8                           ,
      PROCENT numeric(14,2),
      KWOTA numeric(14,2),
      CWARTOSC numeric(14,4),
      PRMIES numeric(14,2),
      CECHA varchar(20) CHARACTER SET UTF8                           ,
      WARTCECH varchar(40) CHARACTER SET UTF8                           )
  returns (
      STATUS integer,
      ILEPRZEL integer)
   as
declare variable wersjaref integer;
declare variable jedn integer;
declare variable cenanet numeric(14,4);
declare variable cenabru numeric(14,4);
declare variable narzut numeric(14,2);
declare variable marza numeric(14,2);
declare variable waluta varchar(3);
declare variable ktm varchar(40);
declare variable vat numeric(14,2);
declare variable zrodlomarzy integer;
declare variable cenamarzy numeric(14,4);
declare variable cena_zakn numeric(14,4);
declare variable cena_kosztn numeric(14,4);
declare variable cena_fabn numeric(14,4);
declare variable staracenanet numeric(14,4);
declare variable staramarza numeric(14,2);
declare variable starynarzut numeric(14,2);
declare variable staryprmies numeric(14,2);
declare variable sql varchar(4096);
begin
  ileprzel = 0;
  select ZRODLO from DEFCENNIK where REF=:CENNIK into :zrodlomarzy;

  sql = ' select distinct ';
  sql = sql ||' CENNIK.KTM,CENNIK.WERSJAREF,CENNIK.JEDN,CENNIK.CENANET,CENNIK.CENABRU, ';
  sql = sql ||' CENNIK.MARZA,CENNIK.NARZUT,CENNIK.WALUTA,CENNIK.procentmies, ';
  sql = sql ||' WERSJE.CENA_ZAKN,WERSJE.CENA_KOSZTN,WERSJE.CENA_FABN ';
  sql = sql ||'  from CENNIK ';
  sql = sql ||'  join WERSJE on (WERSJE.REF=CENNIK.WERSJAREF) ';

  if(coalesce(:cecha,'')<>'') then
    sql = sql ||'  left join atrybuty on (CENNIK.wersjaref = ATRYBUTY.wersjaref and atrybuty.cecha = '''|| :cecha ||''') ';
  sql = sql ||'  where (cennik.cennik= '||:CENNIK ||')  ';
  if(coalesce(:odktm,'')<> '') then
    sql = sql ||' and (CENNIK.KTM >= '''||:odktm||''' ) ';
  if(coalesce(:doktm,'')<> '') then
    sql = sql ||' and (CENNIK.KTM <= '''||:doktm||''' ) ';
  if(coalesce(:ktmmaska,'')<> '') then
    sql = sql ||' and (CENNIK.KTM like '''||:ktmmaska||''' ) ';
  if(coalesce(:wartcech,'')<>'' and coalesce(:cecha,'')<>'') then
    sql = sql ||' and (ATRYBUTY.WARTOSC like '''||:wartcech||''')';
  for
    execute statement sql
    into :ktm,:wersjaref,:jedn,:cenanet,:cenabru,:marza,:narzut,:waluta,:staryprmies,:cena_zakn,:cena_kosztn,:cena_fabn
  do begin
    /* zaktualizuj istniejaca pozycje cennika */
    staracenanet = :cenanet;
    staramarza = :marza;
    starynarzut = :narzut;
    if (:staryprmies is null) then staryprmies = 0;
    if(:procent is not null) then cenanet = :cenanet * ( 1 + :procent / 100.0000);
    if(:kwota is not null) then cenanet = :cenanet + :kwota;
    if (:cwartosc is not null and :cwartosc > 0) then cenanet = :cwartosc;
    if(:zrodlomarzy=0) then cenamarzy = :cena_zakn;
    else if(:zrodlomarzy=1) then cenamarzy = :cena_kosztn;
    else if(:zrodlomarzy=2) then cenamarzy = :cena_fabn;
    select VAT.STAWKA from TOWARY join VAT on (TOWARY.vat=VAT.grupa) where TOWARY.KTM=:ktm into :vat;
    if(:vat is null) then vat = 0;
    vat = 1+:vat/100;
    cenabru = :cenanet * :vat;
    if(:cenanet>0) then marza = (:cenanet - :cenamarzy) * 100 / :cenanet;
    else marza = 0;
    if(:cenamarzy>0) then narzut = (:cenanet - :cenamarzy) * 100 / :cenamarzy;
    else narzut = 100;
    if(:cenanet<>:staracenanet or (:marza<>:staramarza) or (:narzut<>:starynarzut) or (:prmies<>:staryprmies)) then begin
      update CENNIK set CENAPNET=CENANET, CENAPBRU=CENABRU,
      CENANET=:cenanet, CENABRU=:cenabru, MARZA=:marza, NARZUT=:narzut,PROCENTMIES=:prmies
      where (CENNIK=:CENNIK and WERSJAREF=:wersjaref and JEDN=:jedn);
      ileprzel = :ileprzel+1;
    end
  end
  status = 1;
end^
SET TERM ; ^
