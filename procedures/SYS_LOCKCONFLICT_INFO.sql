--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_LOCKCONFLICT_INFO(
      MSGIN varchar(1024) CHARACTER SET UTF8                           )
  returns (
      MSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable tablename varchar(255);
declare variable trans varchar(255);
declare variable eol varchar(10);
declare variable out varchar(1024);
declare variable operatorname varchar(255);
begin
  eol = '
';
  tablename = '';
  trans = '';
  for
    select stringout
      from parsestring(:msgin, :eol)
      into :out
  do begin
    if (position('on table' in  :out) > 0) then
    begin
      out = substring(out from position('"' in out)+1 for coalesce(char_length(out),0)); -- [DG] XXX ZG119346
      tablename = replace(substring(out from 1 for position('"' in out)-1),' ','');
    end else if (position('concurrent transaction number is' in  :out) > 0) then
    begin
      out = replace(out,  'concurrent transaction number is',  '');
      out = replace(out,  ' ',  '');
      if (out <> '') then
        select o.nazwa
          from mon$transactions t
            left join s_sessions s on (s.connectionid = t.mon$attachment_id)
            left join operator o on (o.userid = s.userid)
          where t.mon$transaction_id = :out
          into :operatorname;
      trans = :out;
    end
  end
  msg = 'Operacja czasowo niemożliwa do wykonania.'||:eol;
  if (coalesce(:operatorname,'') <> '') then
    msg = :msg || 'Operator '||:operatorname||' pracuje';
  else
    msg = :msg || 'Inny operator pracuje';
  if(:tablename<>'') then
    msg = :msg || ' na tabeli '||tablename;
  else
    msg = :msg || ' na tych samych danych';
  if(:trans<>'') then msg = :msg ||' w ramach transakcji '||:trans;
  msg = :msg ||'.'||:eol||'Spróbuj ponownie za chwilę.';
end^
SET TERM ; ^
