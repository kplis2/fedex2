--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHECK_CANREALIZE(
      KLIENT integer,
      DOSTAWCA integer,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      MAG2 varchar(3) CHARACTER SET UTF8                           ,
      REJZAM varchar(10) CHARACTER SET UTF8                           ,
      DOKUMENT integer,
      TYP char(1) CHARACTER SET UTF8                           )
  returns (
      SAREAL smallint)
   as
declare variable cnt integer;
begin
  sareal = 0;
  if(exists(select first 1 1 from BR_REALZAMPOZZAM(:rejzam, :klient, :dostawca, :magazyn, :mag2, :dokument,:typ))) then sareal = 1;
  suspend;
end^
SET TERM ; ^
