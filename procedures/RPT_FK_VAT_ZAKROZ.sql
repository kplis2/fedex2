--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_VAT_ZAKROZ(
      PERIODID varchar(6) CHARACTER SET UTF8                           ,
      VREG varchar(10) CHARACTER SET UTF8                           ,
      ZAKRES smallint,
      ZPOPMIES smallint,
      TYP smallint,
      COMPANY integer,
      VATDEBITTYPE integer)
  returns (
      REF integer,
      LP integer,
      VATREG varchar(10) CHARACTER SET UTF8                           ,
      VATREGNR integer,
      TRANSDATE timestamp,
      DOCDATE timestamp,
      VATDATE timestamp,
      DOCSYMBOL varchar(20) CHARACTER SET UTF8                           ,
      BKREG varchar(10) CHARACTER SET UTF8                           ,
      BKREGNR integer,
      KONTRAHKOD varchar(255) CHARACTER SET UTF8                           ,
      NIP varchar(15) CHARACTER SET UTF8                           ,
      WARTNET numeric(14,2),
      WARTVAT numeric(14,2),
      WARTVATV numeric(14,2),
      WARTVATE numeric(14,2),
      WARTVATN numeric(14,2),
      OINET numeric(14,2),
      OIVATV numeric(14,2),
      OIVATE numeric(14,2),
      OIVATN numeric(14,2),
      OIVAT numeric(14,2),
      NINET numeric(14,2),
      NIVATV numeric(14,2),
      NIVATE numeric(14,2),
      NIVATN numeric(14,2),
      NIVAT numeric(14,2),
      OPNET numeric(14,2),
      OPVATV numeric(14,2),
      OPVATE numeric(14,2),
      OPVATN numeric(14,2),
      OPVAT numeric(14,2),
      NPNET numeric(14,2),
      NPVATV numeric(14,2),
      NPVATE numeric(14,2),
      NPVATN numeric(14,2),
      NPVAT numeric(14,2),
      PNET numeric(14,2),
      PVATV numeric(14,2),
      PVATE numeric(14,2),
      PVATN numeric(14,2),
      PVAT numeric(14,2),
      PNET57 numeric(14,2),
      PVATV57 numeric(14,2),
      PVAT57 numeric(14,2),
      PVATE57 numeric(14,2),
      PVATN57 numeric(14,2))
   as
declare variable VATGR varchar(10);
declare variable NETV numeric(14,2);
declare variable VATV numeric(14,2);
declare variable VATE numeric(14,2);
declare variable VATN numeric(14,2);
declare variable DOCREF integer;
declare variable ODLICZONY smallint;
declare variable SQL varchar(5048);
declare variable VATREGTYPE integer; /* <<PR73894: PL>> */
declare variable PRINT integer;
begin
  if (typ = -1) then
    typ = 0; -- jak ktos wykasuje wpis w combo
  ref = 0;
  lp = 0;
  if (zakres is null) then zakres = 0;
  if (zpopmies is null) then zpopmies = 0;

  --BS08473
  sql = 'select B.REF, b.transdate, b.docdate, B.vatreg, b.vnumber, b.bkreg, b.number, B.NIP, b.contractor, b.symbol, b.vatdate, v.vatregtype'; --<<PR73894: PL>>
  sql = sql || ' from bkdocs B join VATREGS V on (B.vatreg = V.symbol and B.company = V.company) join BKDOCTYPES T on (B.doctype = t.ref)';
  sql = sql ||' where B.vatperiod = '''||:periodid||''' and b.status > 0';-- and v.vtype in (3,4,5,6)'
  --<<PR73894: PL
  sql = sql ||'  and (v.vatregtype in (1,2)
    or (V.VATREGTYPE = 3
         and exists(select first 1 1 from bkvatpos join grvat on (grvat.symbol = bkvatpos.taxgr) where bkvatpos.bkdoc = B.ref and grvat.vatregtype in (1,2))))';
  if(coalesce(typ,0)>0) then
  begin
    sql = sql ||' and v.vtype = '||
    case typ
      when 1 then '3' --Zakupy krajowe
      when 2 then '4' --Dostawy importowe
      when 3 then '5' --WNT
      when 4 then '6' --Usługi z importu
    end;
  end
-->>
  if(coalesce(zakres,0)=1)then sql = sql ||' and t.creditnote = 0';
  else if(coalesce(zakres,0)=2)then sql = sql ||' and t.creditnote = 1';
  if(coalesce(zpopmies,0)>0)then sql = sql ||' and (b.vatperiod <> b.period)';
  if(coalesce(vreg,'')<>'')then sql = sql ||' and b.vatreg = '''||:vreg||'''';
  sql = sql ||' and B.company ='||:company||' order by B.vatreg, b.vnumber';
  for execute statement :sql
  /*
  for
    select B.REF, b.transdate, b.docdate, B.vatreg, b.vnumber, b.bkreg, b.number,
        B.NIP, b.contractor, b.symbol, b.vatdate
      from bkdocs B
        join VATREGS V on (B.vatreg = V.symbol and B.company = V.company)
        join BKDOCTYPES T on (B.doctype = t.ref)
      where B.vatperiod = :periodid and b.status > 1
        and v.vtype in (3,4,5,6) --rejestry zakupu
        and (:typ = 0 or v.vtype = (:typ + 2)) -- wszystkie albo wybrany rejestr
        and (:zakres = 0 or (:zakres = 1 and t.creditnote = 0) or (:zakres = 2 and t.creditnote = 1))
        and (:zpopmies = 0 or b.vatperiod <> b.period)
        and (:vreg is null or :vreg = '' or b.vatreg = :vreg)
        and B.company = :company
    order by B.vatreg, b.vnumber  */
    into :docref, :transdate, :docdate, :vatreg, :vatregnr,:bkreg, :bkregnr,
       :nip, :kontrahkod, :docsymbol, :vatdate, :vatregtype   --<<PR73894: PL>>
  do begin
    wartnet = 0;
    wartvatv = 0;
    wartvate = 0;
    wartvatn = 0;
    oinet = 0;
    oivatv = 0;
    oivate = 0;
    oivatn = 0;
    ninet = 0;
    nivatv = 0;
    nivate = 0;
    nivatn = 0;
    opnet = 0;
    opvatv = 0;
    opvate = 0;
    opvatn = 0;
    npnet = 0;
    npvatv = 0;
    npvate = 0;
    npvatn = 0;
    pnet = 0;
    pvatv = 0;
    pvate = 0;
    pvatn = 0;
    pnet57 = 0;
    pvatv57 = 0;
    pvate57 = 0;
    pvatn57 = 0;
    print = 0;
    for
      select taxgr, sum(netv), sum(vatv), sum(vate),sum(vate)-sum(vatv) as vatn, bp.debited
        from bkvatpos bp
          join GRVAT on (GRVAT.SYMBOL = BP.TAXGR)  --<<PR73894: PL>>
        where bp.bkdoc = :docref and (coalesce(bp.debittype,-1) = :vatdebittype or :vatdebittype > 3)
          and (:vatregtype in (1,2) or (:vatregtype = 3 and (grvat.vatregtype in (1,2))))--<<PR73894: PL>>
        group by taxgr, bp.debited
        into :vatgr, :netv, :vatv, :vate,:vatn,  :odliczony
    do begin
      print = 1;
      if (vatgr = 'I11' and odliczony = 1) then
      begin
        oinet = oinet + netv;
        oivatv = oivatv + vatv;
        oivate = oivate + vate;
        oivatn = oivate - oivatv;
      end else if ((vatgr = 'I21' and odliczony = 1 )or (vatgr = 'INW' and odliczony = 1)) then
      begin
        ninet = ninet + netv;
        nivatv = nivatv + vatv;
        nivate = nivate + vate;
        nivatn = nivate - nivatv;
      end else if (vatgr = 'P11' and odliczony = 1) then
      begin
        opnet = opnet + netv;
        opvatv = opvatv + vatv;
        opvate = opvate + vate;
        opvatn = opvate - opvatv;
      end else if ((vatgr = 'P21' and odliczony = 1) or (vatgr = 'POZ' and odliczony = 1)) then
      begin
        npnet = npnet + netv;
        npvatv = npvatv + vatv;
        npvate = npvate + vate;
        npvatn = npvate - npvatv;
      end else
      begin
        pnet = pnet + netv;
        pvatv = pvatv + vatv;
        pvate = pvate + vate;
        pvatn = pvate - pvatv;
        if (vatgr = 'P57') then
        begin
           pnet57 = pnet57 + netv;
           pvatv57 = pvatv57 + vatv;
           pvate57 = pvate57 + vate;
           pvatn57 = pvate57 - pvatv57;
        end
      end
    end
    lp = lp + 1;
    ref = ref + 1;
    wartnet = wartnet+ninet+oinet+npnet+opnet+pnet;
    wartvatv = wartvatv+nivatv+oivatv+npvatv+opvatv+pvatv;
    wartvate = wartvate+nivate+oivate+npvate+opvate+pvate;
    wartvatn = wartvate - wartvatv;
    if (print = 1) then
      suspend;
  end
end^
SET TERM ; ^
