--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SERIAL_NUMBER_EXISTS(
      KTM varchar(80) CHARACTER SET UTF8                           ,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      SERIAL varchar(40) CHARACTER SET UTF8                           )
  returns (
      RET smallint)
   as
begin
ret = 0;
  if (exists(select stancen from stanyser join STANYCEN on (STANYSER.STANCEN = STANYCEN.REF)
               where STANYCEN.KTM = :KTM and STANYCEN.MAGAZYN = :magazyn and STANYSER.SERIALNR = :serial))
  then ret = 1;
  suspend;
end^
SET TERM ; ^
