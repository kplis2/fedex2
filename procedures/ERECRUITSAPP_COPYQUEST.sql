--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ERECRUITSAPP_COPYQUEST(
      SYMBOL STRING20,
      ECANDIDATEREF ECANDIDATES_ID,
      ERECCANDREF ERECCANDS_ID = null)
  returns (
      HEADERREF EQUESTHEADER_ID)
   as
declare variable QUESTREF EQUESTHEADERDEF_ID;
  declare variable RECREF ERECRUITS_ID;
begin
  select e.ref,e.equestheaderdef from erecruits e where e.symbol=:symbol
    into :recref, :questref;

  insert into equestheader(erecruit, ecandidate, ereccand)
    values(:recref, :ecandidateref, :ereccandref)
    returning ref into :headerref;

  insert into equestpos(EQUESTHEADER,LP,QUESTION,QUESTIONTYPE)
    select :headerref, qd.lp, qd.question, qd.questiontype
      from equestposdef qd
      where qd.equestheaderdef = :questref;

  suspend;
end^
SET TERM ; ^
