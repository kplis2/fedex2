--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STANYREZ_MANAGE_FROM_INCOME(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      KTM varchar(80) CHARACTER SET UTF8                           ,
      POZZAMIN integer)
  returns (
      POZZAM integer,
      AMOUNT numeric(14,4),
      AMOUNTDECL numeric(14,4),
      FSKROT varchar(255) CHARACTER SET UTF8                           ,
      ILOSC numeric(14,4),
      STAN varchar(1) CHARACTER SET UTF8                           ,
      ID varchar(255) CHARACTER SET UTF8                           )
   as
declare variable sql varchar(1024);
begin
  if(pozzamin = 0) then pozzamin = null;
  if (magazyn is null or ktm is null) then
    exception universal 'Brak wskazania magazynu lub KTM';
  if(:pozzamin is null) then begin
    for select p.ref, sum(s.ilosc), sum(case when s.status='B' then s.ilosc else 0 end), min(k.fskrot), min(p.ilosc-p.ilzreal), min(p.stan), min(n.id)
        from pozzam p
        left join stanyrez s on (s.pozzam = p.ref and (s.status='B' or s.status='Z') and s.zreal<>1)
        join nagzam n on (n.ref = p.zamowienie)
        join typzam t on (t.symbol = n.typzam)
        left join klienci k on (k.ref=n.klient)
        where p.magazyn = :magazyn and p.ktm = :ktm
        and t.wydania = 1 and n.typ in (0,1)
        group by p.ref
        into :pozzam, :amountdecl, :amount, :fskrot, :ilosc, :stan, :id
      do begin
        if(amount is null) then amount = 0;
        if(amountdecl is null) then amountdecl = 0;
        suspend;
      end
  end else begin
    for select p.ref, sum(s.ilosc), sum(case when s.status='B' then s.ilosc else 0 end), min(k.fskrot), min(p.ilosc-p.ilzreal), min(p.stan), min(n.id)
        from pozzam p
        left join stanyrez s on (s.pozzam = p.ref and (s.status='B' or s.status='Z') and s.zreal<>1)
        join nagzam n on (n.ref = p.zamowienie)
        join typzam t on (t.symbol = n.typzam)
        left join klienci k on (k.ref=n.klient)
        where p.ref = :pozzamin
        and t.wydania = 1 and n.typ in (0,1)
        group by p.ref
        into :pozzam, :amountdecl, :amount, :fskrot, :ilosc, :stan, :id
      do begin
        if(amount is null) then amount = 0;
        if(amountdecl is null) then amountdecl = 0;
        suspend;
      end
  end
end^
SET TERM ; ^
