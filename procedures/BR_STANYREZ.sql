--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_STANYREZ(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      TRYB integer)
  returns (
      POZZAM integer,
      STATUS char(1) CHARACTER SET UTF8                           ,
      ZREAL integer,
      KTO varchar(255) CHARACTER SET UTF8                           ,
      KTOKOD SHORTNAME_ID,
      DATA timestamp,
      ILPLUS numeric(14,4),
      ILMINUS numeric(14,4),
      ILAFTER numeric(14,4),
      ZAMOWIENIE integer,
      SYMBOLDOC varchar(30) CHARACTER SET UTF8                           ,
      TYP char(1) CHARACTER SET UTF8                           ,
      NUMER integer,
      OPER_NAZWA varchar(60) CHARACTER SET UTF8                           ,
      TERMDOST timestamp,
      REJESTR varchar(3) CHARACTER SET UTF8                           ,
      NUMERORG integer,
      PRIORYTET integer,
      DOKUMMAG integer,
      DOSTAWA integer)
   as
declare variable oper_ref INTEGER;
declare variable sql varchar(1024);
begin
  TYp = 'Z';
  NUMER = 0;
  if (:MAGAZYN is null) then
    select sum(ILOSC) from STANYIL where KTM = :KTM and WERSJA = :wersja into :ilafter;
  else
    select ILOSC from STANYIL where MAGAZYN = :MAGAZYN and KTM = :KTM and WERSJA = :wersja into :ilafter;
  if(:ilafter is null) then ilafter = 0;
  sql = 'select ZAMOWIENIE,POZZAM,STATUS,ILOSC, DATA, ZREAL, TYP, NUMER, PRIORYTET, DOKUMMAG, DOSTAWA ' ||
    'from STANYREZ where ';
  if (:MAGAZYN is not null) then
    sql = :sql || 'MAGAZYN = ''' ||:MAGAZYN|| ''' and ';
  sql = :sql || 'KTM = ''' ||replace(:KTM, '''', '''''')|| ''' and WERSJA = ' ||:wersja|| ' and ZREAL <> 1' ||
    'order by STANYREZ.PRIORYTET, STANYREZ.DATA, STATUS,POZZAM';
  for execute statement :sql
    into :zamowienie,:pozzam, :status,:ilminus, :data, :zreal, :typ, :numerorg, :priorytet, :dokummag, :dostawa
  do begin
    if(:status <> 'D')then begin
      ilplus = 0;
    end else begin
      ilplus = :ilminus;
      ilminus = 0;
    end
    if(:ilplus is null) then ilplus = 0;
    if(:ilminus is null) then ilminus = 0;
    if(:tryb = 1 or (:status <> 'R')) then
      ilafter = :ilafter + :ilplus - :ilminus;
    kto = '';
    ktokod = '';
    oper_nazwa = '';
    termdost = NULL;
    oper_ref = NULL;

    if(:zreal <> 1) then begin
      if(:status <>'D') then begin
        if(:typ = 'Z' or :typ = 'G') then
          select KLIENCI.FSKROT, KLIENCI.NAZWA, NAGZAM.REJESTR, NAGZAM.ID, NAGZAM.OPERATOR, NAGZAM.TERMDOST
            from NAGZAM
            join POZZAM on (POZZAM.ZAMOWIENIE = NAGZAM.REF)
            left join KLIENCI on(NAGZAM.KLIENT = KLIENCI.REF)
            where POZZAM.REF = :pozzam
            into :ktokod, :kto, :rejestr, :symboldoc, :oper_ref, :termdost;
        else if(:typ='F') then
          select KLIENCI.FSKROT, KLIENCI.NAZWA, NAGFAK.SYMBOL, NAGFAK.OPERATOR, NAGFAK.DATA, NAGFAK.REF
            from NAGFAK
            join POZFAK on (POZFAK.DOKUMENT = NAGFAK.REF)
            left join KLIENCI on(NAGFAK.KLIENT = KLIENCI.REF)
            where POZFAK.REF = :pozzam into :ktokod, :kto, :symboldoc, :oper_ref, :termdost, :zamowienie;
        else if(:typ='M') then
          select KLIENCI.FSKROT, KLIENCI.NAZWA, DOKUMNAG.SYMBOL, DOKUMNAG.OPERATOR, DOKUMNAG.DATA, DOKUMNAG.REF
            from DOKUMNAG
            join DOKUMPOZ on (DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
            left join KLIENCI on (DOKUMNAG.KLIENT = KLIENCI.REF)
            where DOKUMPOZ.REF = :pozzam into :ktokod, :kto, :symboldoc, :oper_ref, :termdost, :zamowienie;
      end else
        select DOSTAWCY.ID, DOSTAWCY.NAZWA, NAGZAM.REJESTR, NAGZAM.ID, NAGZAM.OPERATOR, NAGZAM.TERMDOST
          from NAGZAM
          join POZZAM on (POZZAM.ZAMOWIENIE = NAGZAM.REF)
          left join DOSTAWCY on(NAGZAM.DOSTAWCA = DOSTAWCY.REF)
          where POZZAM.REF = :pozzam into :ktokod, :kto, :rejestr, :symboldoc, :oper_ref, :termdost;
      if(:oper_ref is not null) then select NAZWA from OPERATOR where REF=:oper_ref into :oper_nazwa;
    end
    if(:ilplus = 0) then ilplus = null;
    if(:ilminus = 0) then ilminus = null;
    numer = :numer + 1;
    suspend;
  end
end^
SET TERM ; ^
