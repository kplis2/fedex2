--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INWENTAP_POZMASTER(
      POZREF integer)
  returns (
      MREF integer)
   as
declare variable zpartiami integer;
declare variable inwpar integer;
begin
  select PAR.REF, PAR.zpartiami
  from INWENTA PAR join INWENTA NAG on (PAR.ref = NAG.inwentaparent)
  join INWENTAP on (NAG.REF = INWENTAP.inwenta)
  where INWENTAP.ref =:pozref
  into :inwpar, :zpartiami;
  if(:zpartiami = 1) then
    select INWPPAR.REF from
    INWENTAP INWPOZ
    left join INWENTAP INWPPAR on (inwppar.inwenta = :inwpar
        and inwppar.wersjaref = inwpoz.wersjaref and
            inwppar.dostawa = inwpoz.dostawa and
            inwppar.cena = inwpoz.cena)
    where INWPOZ.ref=:pozref
    into :mref;
  else
    select INWPPAR.REF from
    INWENTAP INWPOZ
    left join INWENTAP INWPPAR on (inwppar.inwenta = :inwpar
        and inwppar.wersjaref = inwpoz.wersjaref)
    where INWPOZ.ref=:pozref
    into :mref;
end^
SET TERM ; ^
