--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZFAK_ZAPLACONO_CALC(
      NAGFAK integer)
   as
declare variable zaplaconon numeric(14,2);
declare variable zaplaconon2 numeric(14,2);
declare variable zaplaconozal numeric(14,2);
declare variable zaplaconop numeric(14,2);
declare variable termzapn timestamp;
declare variable termzapp timestamp;
declare variable pozfak integer;
declare variable wartbrup numeric(14,2);
declare variable dozaplaty numeric(14,2);
begin
  select zaplacono, termzap,
    sumwartbru-psumwartbru, (sumwartbru-psumwartbru)-(esumwartbru-pesumwartbru)
  from nagfak where ref = :nagfak
  into :zaplaconon, :termzapn,
    :dozaplaty, :zaplaconozal;
  -- bierzemy kwote zaplacona z faktur pierwotnych (np. z paragonu)
  select zaplacono from NAGFAK_FAKT_ZBIORCZE_PAR(:nagfak) into :zaplaconon2;
  -- kwota zaplacona = zaplacona na tej fakturze + zaplacona z faktur pierwotnych + zaliczkowana
  zaplaconon = :zaplaconon + :zaplaconon2 + :zaplaconozal;
  if(:dozaplaty=:zaplaconon) then begin
    update pozfak set zaplacono = wartbru-pwartbru where dokument = :nagfak;
  end else if(:zaplaconon = 0) then begin
    update pozfak set zaplacono = 0 where dokument = :nagfak;
  end else begin
    for select (case when p.termzap is null then :termzapn else p.termzap end), sum(p.wartbru - p.pwartbru)
      from pozfak p
      where p.dokument = :nagfak
      group by case when p.termzap is null then :termzapn else p.termzap end
      order by case when p.termzap is null then :termzapn else p.termzap end
      into :termzapp, :wartbrup
    do begin
      if(:zaplaconon = 0) then begin
        update pozfak set zaplacono = 0 where dokument = :nagfak and zaplacono<>0
          and (case when pozfak.termzap is null then :termzapn else pozfak.termzap end)=:termzapp;
      end else if(:zaplaconon >= :wartbrup) then begin
        update pozfak set zaplacono = wartbru-pwartbru where dokument = :nagfak and zaplacono<>(wartbru-pwartbru)
          and (case when pozfak.termzap is null then :termzapn else pozfak.termzap end)=:termzapp;
        zaplaconon = zaplaconon - :wartbrup;
      end else begin
        if (:wartbrup<>0) then
        update pozfak set zaplacono = (:zaplaconon / :wartbrup)*(wartbru-pwartbru) where dokument = :nagfak
          and (case when pozfak.termzap is null then :termzapn else pozfak.termzap end)=:termzapp;
        --wyrownanie koncowek
        select sum(zaplacono) from pozfak where dokument = :nagfak
          and (case when pozfak.termzap is null then :termzapn else pozfak.termzap end)=:termzapp
          into :zaplaconop;
        if(zaplaconon <> zaplaconop) then begin
          pozfak = null;
          select max(ref) from pozfak where dokument = :nagfak
            and (case when pozfak.termzap is null then :termzapn else pozfak.termzap end)=:termzapp
            into :pozfak;
          if(pozfak is not null) then begin
            update pozfak set zaplacono = zaplacono + (:zaplaconon - :zaplaconop) where ref = :pozfak;
          end
          zaplaconon = 0;
        end
      end
    end
  end
end^
SET TERM ; ^
