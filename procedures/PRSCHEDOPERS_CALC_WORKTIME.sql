--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDOPERS_CALC_WORKTIME(
      PRSCHEDOPER integer)
  returns (
      WORKTIME double precision,
      OPER varchar(20) CHARACTER SET UTF8                           )
   as
declare variable eopertime varchar(255);
declare variable opertime numeric(14,4);
declare variable prsheet integer;
declare variable prcalc integer;
declare variable amountrate numeric(14,4);
declare variable guide integer;
declare variable ret numeric(16,6);
begin
  select so.OPER, so.OPERTIME, so.eOPERTIME
    from PRSHOPERS so
    join prschedopers o on (so.ref = o.shoper)
    where o.ref =:prschedoper
    into :oper, :opertime, :eopertime;

  if(:eopertime <> '' and :opertime is null ) then
  begin
    select z.prsheet, z.prshcalc
      from prschedzam z
        join prschedopers o on (z.ref = o.schedzam)
      where o.ref=:prschedoper
      into :prsheet, :prcalc;

    execute procedure STATEMENT_PARSE(:eopertime,:prsheet,:prcalc,'XP_')
      returning_values :ret;

    if(:ret is not null) then
      opertime = :ret;
  end
  amountrate = 0;
  select p.quantity, o.guide
    from prsheets p
      join prshopers so on (p.ref = so.sheet)
      join prschedopers o on (o.shoper = so.ref)
    where o.ref = :prschedoper
    into :amountrate, :guide;
  if(:amountrate is null or (:amountrate = 0))then amountrate = 1;

  select amount / :amountrate
    from prschedguides
    where ref=:guide
    into :amountrate;

  worktime = coalesce(cast(:opertime as float) / 24 * :amountrate,0);
  suspend;
end^
SET TERM ; ^
