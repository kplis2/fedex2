--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_SESJE_AKTUALIZUJ(
      SESJAREF SESJE_ID,
      PRZETWORZONY SMALLINT_ID = 0)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable stabela string35;
declare variable szrodlo zrodla_id;
declare variable skierunek smallint_id;
declare variable sql memo;
declare variable tmpsql memo;
declare variable tmpsql2 memo;
declare variable tmptabela string35;
declare variable skadtabela string40;
declare variable skadref integer_id;
declare variable liczbabledow integer_id;
declare variable liczbabledowzal integer_id;
declare variable liczbabledowrazem integer_id;
declare variable liczbaobiektow integer_id;
declare variable liczbaobiektowzal integer_id;
declare variable tmpliczba integer_id;
begin

  status = 0;

  if (przetworzony is null) then
    przetworzony = 0;

  select s.tabela, s.zrodlo, s.kierunek
    from int_sesje s where ref = :sesjaref
    into :stabela, :szrodlo, :skierunek;

  if (coalesce(stabela,'') <> '' and skierunek = 1) then --sesje przychodzace
    begin
      status = 1;
      sql = 'update '||:stabela||' set errorzalezne = 0 where sesja = '||:sesjaref;
      execute statement sql;

      --w przyszlosci uwzglednic strukture drzewiasta !!!!

      sql = 'select zalezna from int_tabele where tabela = '''||:stabela||
        ''' and zrodlo = '||:szrodlo||' and kierunek = '||:skierunek||
        ' order by kolejnosc';

      for execute statement :sql into :tmptabela
        do begin
          tmpsql = 'select skadtabela, skadref from '||:tmptabela||
            ' where sesja = '||:sesjaref||' and coalesce(error,0) > 0 and coalesce(skadref,0) > 0
            and coalesce(skadtabela,'''') <> ''''';
          for execute statement :tmpsql into :skadtabela, :skadref
            do begin
              tmpsql2 = 'update '||:skadtabela||
                ' set errorzalezne = coalesce(errorzalezne,0) + 1 where ref = '||:skadref;
              execute statement tmpsql2;
            end
        end

      if (tmptabela is null) then
        begin
          sql = 'select zalezna from int_tabele where tabela = '''||:stabela||
            ''' and zrodlo is null and kierunek = '||:skierunek||
            ' order by kolejnosc';
    
          for execute statement :sql into :tmptabela
          do begin
            tmpsql = 'select skadtabela, skadref from '||:tmptabela||
              ' where sesja = '||:sesjaref||' and coalesce(error,0) > 0 and coalesce(skadref,0) > 0
              and coalesce(skadtabela,'''') <> ''''';
            for execute statement :tmpsql into :skadtabela, :skadref
              do begin
                tmpsql2 = 'update '||:skadtabela||
                  ' set errorzalezne = coalesce(errorzalezne,0) + 1 where ref = '||:skadref;  --
                execute statement tmpsql2;
              end
          end
        end

      sql = 'select sum(coalesce(error,0)), sum(case when coalesce(errorzalezne,0)>0 then 1 else 0 end),
      sum(case when coalesce(error,0) > 0 or coalesce(errorzalezne,0)>0  then 1 else 0 end)
      from '||stabela||' where sesja = '||:sesjaref;

      execute statement sql into :liczbabledow, :liczbabledowzal, :liczbabledowrazem;

      update int_sesje set
          dataostprzetw = case when :przetworzony = 1 then current_timestamp(0) else dataostprzetw end,
          liczbabledow = coalesce(:liczbabledow,0),
          liczbabledowzal = coalesce(:liczbabledowzal,0),
          liczbabledowrazem = coalesce(:liczbabledowrazem,0)
        where ref = :sesjaref;
    end

  else if (coalesce(stabela,'') <> '' and skierunek = 2) then --sesje wychodzace
    begin
      status = 1;

      --w przyszlosci uwzglednic strukture drzewiasta !!!!

      sql = 'select zalezna from int_tabele where tabela = '''||:stabela||
        ''' and zrodlo = '||:szrodlo||' and kierunek = '||:skierunek||
        ' order by kolejnosc';

      for execute statement :sql into :tmptabela
        do begin
          tmpliczba = null;
          tmpsql = 'select count (1)
      from '||tmptabela||' where sesja = '||:sesjaref;
          execute statement :tmpsql into :tmpliczba;
          liczbaobiektowzal = coalesce(liczbaobiektowzal, 0) + coalesce(tmpliczba,0);
        end

      if (tmptabela is null) then
        begin
          sql = 'select zalezna from int_tabele where tabela = '''||:stabela||
            ''' and zrodlo is null and kierunek = '||:skierunek||
            ' order by kolejnosc';
    
          for execute statement :sql into :tmptabela
        do begin
          tmpliczba = null;
          tmpsql = 'select count (1)
      from '||tmptabela||' where sesja = '||:sesjaref;
          execute statement :tmpsql into :tmpliczba;
          liczbaobiektowzal = coalesce(liczbaobiektowzal, 0) + coalesce(tmpliczba,0);
        end
        end


      sql = 'select count (1)
      from '||stabela||' where sesja = '||:sesjaref;

      execute statement sql into :liczbaobiektow;

      if (liczbaobiektow is null) then
        liczbaobiektow = 0;

      update int_sesje set
          dataostprzetw = case when :przetworzony = 1 then current_timestamp(0) else dataostprzetw end,
          liczbaobiektow = :liczbaobiektow
        where ref = :sesjaref;
    end
  else
    exception universal 'Cos poszlo nie tak.';

  suspend;
end^
SET TERM ; ^
