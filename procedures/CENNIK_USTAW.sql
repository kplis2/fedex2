--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CENNIK_USTAW(
      CENNIK integer,
      WERSJAREF integer,
      JEDN integer,
      CENANET numeric(14,2),
      KTM1 varchar(40) CHARACTER SET UTF8                           ,
      NAZWA varchar(80) CHARACTER SET UTF8                           )
  returns (
      STATUS integer,
      NOWAWERSJAREF integer)
   as
DECLARE VARIABLE CENABRU NUMERIC(14,2);
DECLARE VARIABLE MARZA NUMERIC(14,2);
DECLARE VARIABLE NARZUT NUMERIC(14,2);
DECLARE VARIABLE ZRODLOMARZY INTEGER;
DECLARE VARIABLE KURS NUMERIC(14,4);
DECLARE VARIABLE VAT NUMERIC(14,2);
DECLARE VARIABLE CENA_ZAKN NUMERIC(14,2);
DECLARE VARIABLE CENA_KOSZTN NUMERIC(14,2);
DECLARE VARIABLE CENA_FABN NUMERIC(14,2);
DECLARE VARIABLE CENAMARZY NUMERIC(14,2);
DECLARE VARIABLE KTM VARCHAR(40);
DECLARE VARIABLE NEWJEDN INTEGER;
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE NRWERSJI INTEGER;
DECLARE VARIABLE NIEOBROT INTEGER;
DECLARE VARIABLE AKT INTEGER;
DECLARE VARIABLE CENA_ZAKB NUMERIC(14,2);
DECLARE VARIABLE CENA_FABB NUMERIC(14,2);
DECLARE VARIABLE CENA_KOSZTB NUMERIC(14,2);
DECLARE VARIABLE DNIWAZN INTEGER;
DECLARE VARIABLE SYMBOL_DOST VARCHAR(20);
DECLARE VARIABLE KODKRESK VARCHAR(20);
DECLARE VARIABLE DEFSTALACENA SMALLINT;
begin
  if(:wersjaref is null or (:wersjaref=0)) then begin
    execute procedure GEN_REF('WERSJE') returning_values :wersjaref;
    select max(NRWERSJI) from WERSJE where KTM=:ktm1 into :nrwersji;
    if(:nrwersji is null) then begin
      nrwersji=0;
      nieobrot=0;
    end else begin
      nrwersji = :nrwersji+1;
      nieobrot = 1;
    end
    select AKT,CENA_ZAKB,CENA_ZAKN,CENA_FABB,CENA_KOSZTN,CENA_KOSZTB,DNIWAZN,
           SYMBOL_DOST,KODKRESK,MARZAMIN
    from TOWARY where KTM=:ktm1
    into :akt,:cena_zakn,:cena_fabn,:cena_fabb,:cena_kosztn,:cena_kosztb,:dniwazn,
         :symbol_dost,:kodkresk,:marza;
    insert into WERSJE(REF,NRWERSJI,NAZWA,KTM,AKT,NIEOBROT,CENA_ZAKB,CENA_ZAKN,
      CENA_FABN,CENA_FABB,CENA_KOSZTN,CENA_KOSZTB,DNIWAZN,SYMBOL_DOST,KODKRESK,MARZAMIN)
    values(:wersjaref,:nrwersji,:nazwa,:ktm1,:akt,:nieobrot,:cena_zakb,:cena_zakn,
      :cena_fabn,:cena_fabb,:cena_kosztn,:cena_kosztb,:dniwazn,:symbol_dost,:kodkresk,:marza);
  end
  select ZRODLO,KURS, DEFSTALACENA from DEFCENNIK where REF=:cennik into :ZRODLOMARZY, :KURS, :defstalacena;
  if(:ZRODLOMARZY is null) then ZRODLOMARZY = 0;
  if(:kurs is null or (:kurs = 0)) then kurs = 1;
  select KTM,CENA_ZAKN,CENA_KOSZTN,CENA_FABN from WERSJE where REF=:WERSJAREF into :ktm,:cena_zakn,:cena_kosztn,:cena_fabn;
  /*okreslenie ceny dla marzy*/
  cenamarzy = 0;
  if(:ZRODLOMARZY=0) then cenamarzy = :cena_zakn;
  else if(:ZRODLOMARZY=1) then cenamarzy = :cena_kosztn;
  else if(:ZRODLOMARZY=2) then cenamarzy = :cena_fabn;
  cenamarzy = :cenamarzy / :kurs;
  select VAT.STAWKA, TOWARY.DC from TOWARY join VAT on (TOWARY.vat=VAT.grupa) where TOWARY.KTM=:ktm into :vat, :newjedn;
  if(:jedn is null or (:jedn=0)) then jedn = :newjedn;
  if(:vat is null) then vat = 0;
  vat = 1+:vat/100;
  CENABRU = :CENANET * :vat;
  if(:CENANET>0) then MARZA = (:CENANET - :cenamarzy) * 100 / :CENANET;
  else MARZA = 0;
  if(:cenamarzy>0) then NARZUT = (:CENANET - :cenamarzy) * 100 / :cenamarzy;
  else NARZUT = 100;
  select count(*) from CENNIK where CENNIK=:cennik and WERSJAREF=:wersjaref and JEDN=:jedn into :cnt;
  if(cnt>0) then
    update CENNIK set
      CENAPNET=CENNIK.CENANET, CENAPBRU=CENNIK.CENABRU,
      CENANET=:cenanet, CENABRU=:cenabru, MARZA=:marza, NARZUT=:narzut
    where CENNIK=:cennik and WERSJAREF=:wersjaref and JEDN=:jedn;
  else
    insert into CENNIK(CENNIK,WERSJAREF,JEDN,CENANET,CENABRU,MARZA,NARZUT, STALACENA)
    values (:cennik,:wersjaref,:jedn,:cenanet,:cenabru,:marza,:narzut, :defstalacena);
  status = 1;
  nowawersjaref=:wersjaref;
end^
SET TERM ; ^
