--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_IS_REPROGRAMING(
      MWSORDREF INTEGER_ID)
  returns (
      PRINTER STRING,
      STATUS SMALLINT_ID,
      MSG STRING)
   as
declare variable rwref integer_id;
declare variable wersjaref integer_id;
declare variable wersjarefwz integer_id;
declare variable grupaprod integer_id;
declare variable typ string3;
declare variable zrodlo integer_id ;
begin
/*
  Porcedura wywoywana z HH w oknie potweirdzania wózków (EditMwsOrdCarts)
  sprawdza, czy przekazane zgoszneie pochodzi z przeprogramowania
  jeli tak to na HH wywitla sie komunikat aby przekazac towar do SERWISU
  i drukuje sie serwisowa etykieta
*/

/*
  status 0  - ogólny bąd, niepowodozenie
  status 1  - nie jst to dokuemt RW z przeprogramoania
  status 2 - dokument RW z przeprodukowania
*/

  status = 0;
  msg = '';
  printer = 'RAMPA1';

  select m.docid -- pobranie RWref
    from mwsords m
    where m.ref = :MWSORDREF
  into rwref;

  select d.grupaprod, d.typ, d.grupaprod, d.zrodlo
    from dokumnag d
    where d.ref = :rwref
  into :grupaprod, :typ, :grupaprod, :zrodlo;

  if (typ = 'RW' and coalesce(grupaprod,0) <> 0 and coalesce(zrodlo,0) = 99) then
    status = 2;
end^
SET TERM ; ^
