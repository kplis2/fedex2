--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_STANYIL_KOLOR(
      ILOSC numeric(14,4),
      STANMIN numeric(14,4),
      STANMAX numeric(14,4),
      KOLOR smallint,
      MINMAXSTAN smallint,
      KTM varchar(40) CHARACTER SET UTF8                           )
  returns (
      RKOLOR smallint)
   as
declare variable supplymethod smallint;
begin
  select supplymethod from towary where ktm=:ktm into :supplymethod;
  if(:supplymethod=3) then begin
    -- zarzadzanie kolorami wg DBM (TOC)
    if(:stanmax is null) then stanmax = 0;
    if(:ilosc>2.0*:stanmax/3.0) then rkolor = 3; -- green
    else if(:ilosc>:stanmax/3.0) then rkolor = 2; -- yellow
    else if(:ilosc>0) then rkolor = 1; -- red
    else rkolor = 0; -- black
  end else if(:supplymethod=2) then begin
    -- zarzadzanie kolorami wg stanow min-max
    rkolor = :minmaxstan;
  end else begin
    -- nie zmieniamy koloru
    rkolor = :kolor;
  end
  suspend;
end^
SET TERM ; ^
