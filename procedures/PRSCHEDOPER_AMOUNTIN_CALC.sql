--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDOPER_AMOUNTIN_CALC(
      PRSCHEDOPER integer)
  returns (
      AMOUNTIN numeric(14,4))
   as
declare variable PRSCHEDOPERFROM integer;
declare variable PRSCHEDGUIDE integer;
declare variable REPORTEDFACTOR numeric(14,4);
declare variable DOCFROMOPERRAP smallint;
declare variable AMOUNTINFROM smallint;
declare variable GUIDEPRSHEET integer;
declare variable OPERSHEET integer;
declare variable KAMOUNTNORMPOP numeric(14,4);
declare variable GUIDEAMOUNT numeric(14,4);
declare variable ref integer;
begin
  -- sprawdzenie czy jest to pierwsza operacja
  select first 1 psod.depfrom
     from prschedoperdeps psod
     left join prschedopers pso on (pso.ref = psod.depto)
     where psod.depto = :prschedoper
     into prschedoperfrom;
  select p.amountinfrom, pso.guide, so.reportedfactor, g.docfromoperrap, g.prsheet, coalesce(so.sheet, g.prsheet), g.amount
    from prschedopers pso
      left join prshopers so on (so.ref = pso.shoper)
      left join prschedguides g on (g.ref = pso.guide)
      left join prsheets p on (p.ref = so.sheet)
    where pso.ref = :prschedoper
    into amountinfrom, prschedguide, reportedfactor, docfromoperrap, guideprsheet, opersheet, :guideamount;
  if (amountinfrom is null) then amountinfrom = 0;
  if (reportedfactor is null) then reportedfactor = 1;
  if (docfromoperrap is null) then docfromoperrap = 0;
  -- dla operacji pierwszej wyliczamy z ilosci na prschedguidepos
  if (prschedoperfrom is null) then
  begin
    -- albo z wydanego surowca albo z ilosci oczekiwanej
    if (guideprsheet = opersheet) then  --
    begin
      if (amountinfrom = 0 and docfromoperrap = 0) then
        --wydane surowce
        execute procedure PR_GET_PRSCHEDOPER_AMOUNTIN(prschedguide, 0) returning_values amountin;
      else if (amountinfrom = 1) then       --poczatek KS
        -- cala ilosc materialow
        execute procedure PR_GET_PRSCHEDOPER_AMOUNTIN(prschedguide, 1) returning_values amountin;
      else if (amountinfrom = 2) then
        -- wielkosc przewodnika
        amountin = :guideamount;   --koniec KS
      --amoutnin na tym etapie pokazuje ilosac po ostatniej operacji, teraz trzeba cofnac sie po operacjach do pierwszej i dzielic przez reportedfactor

      select first 1 RPRSCHEDOPER
        from prsched_calc_next_oper((select first 1 ref from prschedopers po
            where po.guide = :prschedguide
            and not exists (select first 1 1 from prschedoperdeps d where d.depto = po.ref)),null,0)
        order by LEVEL_OUT desc
        into :ref;

      if (:ref is not null) then
      begin
        reportedfactor = null;
        select o.reportedfactor
          from prschedopers o
          where o.ref = :ref
          into :reportedfactor;

        if (coalesce(:reportedfactor,0) = 0) then
          reportedfactor = 1;
        amountin = :amountin / :reportedfactor;
  
        reportedfactor = null;
        for
          select min(o.reportedfactor)
            from prsched_calc_next_oper(:ref,null,1)
              join prschedopers o on (o.ref = RPRSCHEDOPER)
            group by level_out, o.reportedfactor
            order by level_out
            into :reportedfactor
        do begin
          if (coalesce(:reportedfactor,0) = 0) then
            reportedfactor = 1;
          amountin = :amountin / reportedfactor;
          reportedfactor = null;
        end
      end
    end else begin
      select max(p.kamountnorm)
        from prschedguidespos p
        where p.prsheet = :opersheet and p.out = 2 and p.prschedguide = :prschedguide
        into kamountnormpop;
      if (kamountnormpop is null or kamountnormpop = 0) then
        kamountnormpop = 1;
      if (amountinfrom = 0 and docfromoperrap = 0) then
        select max((pgp.amountzreal  + pgp.amountoverlimit)/ pgp.kamountnorm)
          from prschedguidespos pgp
          where pgp.prschedguide = :prschedguide and pgp.prsheet = :opersheet
          into amountin;
      else
        select max((pgp.amount + (case when :amountinfrom = 2 then 0 else pgp.amountoverlimit end)) / pgp.kamountnorm)
          from prschedguidespos pgp
          where pgp.prschedguide = :prschedguide and pgp.prsheet = :opersheet
          into amountin;
      amountin = amountin * kamountnormpop;
    end
  end else begin
    select min(pso.amountresult)
      from prschedoperdeps psod
        left join prschedopers pso on (pso.ref = psod.depfrom)
      where psod.depto = :prschedoper
        and pso.activ = 1
      into amountin;

    if (amountin is null) then
      select min(pso.amountresult)
        from prschedoperdeps psod
          left join prschedopers pso on (pso.ref = psod.depfrom)
        where psod.depto = :prschedoper
          into amountin;
  end
  if (amountin is null) then amountin = 0;
end^
SET TERM ; ^
