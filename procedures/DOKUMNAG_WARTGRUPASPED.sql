--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_WARTGRUPASPED(
      GRUPASPED integer)
  returns (
      WARTSNETTO numeric(14,2),
      WARTSBRUTTO numeric(14,2),
      WARTKNETTO numeric(14,2),
      WARTKBRUTTO numeric(14,2),
      WARTRNETTO numeric(14,2),
      WARTRBRUTTO numeric(14,2))
   as
declare variable gr_vat varchar(10);
declare variable stawka numeric(14,2);
begin
  select wartosc from konfig where akronim='VAT' into :gr_vat;
  select stawka from vat where grupa=:gr_vat into :stawka;
  if(:stawka is null) then stawka = 0;
  select sum(dokumnag.wartsnetto), sum(dokumnag.wartsbrutto), sum(dokumnag.kosztdost)
  from DOKUMNAG where GRUPASPED = :grupasped
  into  :wartsnetto, :wartsbrutto, :wartknetto;
  if(:wartsnetto is null) then wartsnetto= 0;
  if(:wartsbrutto is null) then wartsbrutto= 0;
  if(:wartknetto is null) then wartknetto= 0;
  wartkbrutto = :wartknetto * :stawka;
  wartrnetto = :wartsnetto + :wartknetto;
  wartrbrutto = :wartsbrutto + :wartkbrutto;

end^
SET TERM ; ^
