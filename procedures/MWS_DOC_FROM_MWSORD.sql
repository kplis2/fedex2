--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_DOC_FROM_MWSORD(
      DOCID integer,
      DOCTYPE char(1) CHARACTER SET UTF8                           ,
      ADDTOORDERS smallint = 1)
   as
declare variable WYDANIA smallint;
declare variable MWSORD integer;
declare variable QUANTITYC type of QUANTITY_MWS;
declare variable ILOSC type of ILOSCI_MAG;
declare variable DOKUMPOZ type of DOKUMPOZ_ID;
declare variable NEWDOKUMPOZ type of DOKUMPOZ_ID;
declare variable VERS type of WERSJE_ID;
declare variable MWSACT type of MWSACTS_ID;
declare variable ILOSCPOZ type of ILOSCI_MAG;
declare variable ILOSCPOZZAM type of ILOSCI_MAG;
declare variable TODISP type of ILOSCI_MAG;
declare variable DIFF type of ILOSCI_MAG;
declare variable TOINSERT type of ILOSCI_MAG;
declare variable LDOKUMPOZ type of DOKUMPOZ_ID;
declare variable DOSTAWCA type of DOSTAWCA_ID;
declare variable NREJESTR type of STRING80;
declare variable FROMPOZZAM type of POZZAM_ID;
declare variable FROMZAM type of NAGZAM_ID;
declare variable GRUPASPED integer;
begin
  mwsord = null;

  select d.wydania, d.dostawca, d.grupasped
     from dokumnag d
    where d.ref = :docid
    into :wydania, dostawca, :grupasped;

  if(wydania = 1) then
  begin
    update mwsacts set status = 0
      where docid = :docid and doctype = :doctype and status < 5;
    delete from mwsacts
      where docid = :docid and doctype = :doctype and status = 0;
    for select distinct docposid, vers
      from mws_doc_all_goods(:docid, :doctype, null, null)
      into :dokumpoz, :vers
    do begin
      ilosc = null;
      quantityc = null;
      if (dokumpoz > 0) then
        select p.ilosc
          from dokumpoz p
          where p.ref = :dokumpoz
          into :ilosc;
      else
        ilosc = 0;
      select sum(coalesce(a.quantityc,0))
        from mwsacts a
        where a.docid = :docid
          and a.docposid = :dokumpoz
          and a.vers = :vers
          and a.doctype = :doctype and a.status = 5
          and a.mwsconstlocl is null and a.mwspallocl is null
        group by a.docposid
        into :quantityc;
      if (:quantityc is null) then quantityc = 0;
      if(ilosc < quantityc and dokumpoz = 0) then
      begin
        execute procedure GEN_REF('DOKUMPOZ') returning_values :newdokumpoz;
        update mwsacts
          set docposid = :newdokumpoz
          where docid = :docid
            and docposid = :dokumpoz
            and vers = :vers
            and doctype = :doctype and status = 5
            and mwsconstlocl is null and mwspallocl is null;
        insert into dokumpoz (ref, dokument, wersjaref, ilosc, cena)
          values (:newdokumpoz, :docid, :vers,  (:quantityc - :ilosc), 0.00);
        newdokumpoz = null;
      end
      else if(ilosc > quantityc and dokumpoz > 0) then
      begin
        if (quantityc > 0) then
          update dokumpoz d
            set d.ilosc = :quantityc
            where d.ref = :dokumpoz;
        else begin
          select mwsord from mwsacts
            where docid = :docid and docposid = :dokumpoz and status < 5
          into :mwsord;
          update mwsacts set status = 0
            where docid = :docid and docposid = :dokumpoz and status < 5;
          delete from mwsacts
            where docid = :docid and docposid = :dokumpoz and status = 0;
          delete from dokumpoz
            where ref = :dokumpoz;
          if (not exists(select first 1 1 from mwsacts where mwsord = :mwsord)) then begin
            update mwsords set status = 0
              where ref = :mwsord;
            delete from mwsords where ref = :mwsord;
          end
        end
      end
      else if(ilosc <> quantityc) then
      begin
        exception MWS_ILOSC_DYSPOZYCJI;
      end
    end
  end else
  begin
    execute procedure get_config('REJZAMDOSMWSAUTOREAL',2) returning_values nrejestr;
    for select distinct vers
      from mws_doc_all_goods(:docid, :doctype, null, null)
      into :vers
    do begin
      ilosc = null;
      quantityc = null;
      select sum(p.ilosc)
        from dokumpoz p
        where p.dokument = :docid
          and p.wersjaref = :vers
        into :ilosc;
      select sum(coalesce(a.quantityc,0))
        from mwsacts a
        where a.docid = :docid
          and a.vers = :vers
          and a.doctype = :doctype and a.status = 5
          and a.mwsconstlocp is null and a.mwspallocp is null
        into :quantityc;
      diff = ilosc - quantityc;
      if(ilosc > quantityc) then
      begin
        for
          select p.ref, p.ilosc
            from dokumpoz p
            where p.dokument = :docid and p.wersjaref = :vers
            into :dokumpoz, :iloscpoz
        do begin
          if (iloscpoz <= diff) then
          begin
            delete from dokumpoz p where p.ref = :dokumpoz;
            ilosc = ilosc - iloscpoz;
            diff = diff - iloscpoz;
          end else
          begin
            update dokumpoz p set p.ilosc = p.ilosc - :diff where p.ref = :dokumpoz;
            ilosc = ilosc - diff;
            diff = 0;
          end
          if (diff <= 0) then
            break;
        end
      end
      else if (addtoorders = 1 and ilosc < quantityc) then
      begin
        todisp = quantityc - ilosc;
        for
          select (p.iloscm - p.ilzrealm), coalesce(dp.ilosc,0), p.zamowienie, p.ref, dp.ref
            from nagzam n
              left join pozzam p on (p.zamowienie = n.ref)
              left join dokumpoz dp on (dp.frompozzam = p.ref)
            where n.dostawca = :dostawca and p.iloscm - p.ilzrealm > 0
              and p.wersjaref = :vers and n.typ < 3
              and (dp.ref is null or dp.dokument = :docid)
              and position(';'||n.rejestr||';' in :nrejestr) > 0
            order by case when dp.ref is not null then 0 else 1 end, n.termdost
            into :iloscpozzam, :iloscpoz, :fromzam, :frompozzam, :dokumpoz
        do begin
          if (iloscpozzam > iloscpoz and dokumpoz is not null) then
          begin
            toinsert = iloscpozzam - iloscpoz;
            if (toinsert > todisp) then
              toinsert = todisp;
            if (toinsert > 0) then
              update dokumpoz set ilosc = ilosc + :toinsert where ref = :dokumpoz;
            todisp = todisp - toinsert;
            ilosc = ilosc + toinsert;
          end
          else
          begin
            toinsert = 0;
            if (iloscpozzam > todisp) then
              toinsert = todisp;
            else
              toinsert = iloscpozzam;
            if (toinsert > 0) then
            begin
              execute procedure GEN_REF('DOKUMPOZ') returning_values :newdokumpoz;
              insert into dokumpoz (ref, dokument, wersjaref, ilosc, cena, fromzam, frompozzam)
                values (:newdokumpoz, :docid, :vers,  :toinsert, 0.00, :fromzam, :frompozzam);
            end
            todisp = todisp - toinsert;
            ilosc = ilosc + toinsert;
          end
          if (todisp <= 0) then
            break;
        end
      end
      if (ilosc < quantityc) then
      begin
        execute procedure GEN_REF('DOKUMPOZ') returning_values :newdokumpoz;
        insert into dokumpoz (ref, dokument, wersjaref, ilosc, cena)
          values (:newdokumpoz, :docid, :vers,  :quantityc - :ilosc, 0.00);
      end
      else if(ilosc <> quantityc) then
      begin
        exception MWS_ILOSC_DYSPOZYCJI;
      end
    end
  end
end^
SET TERM ; ^
