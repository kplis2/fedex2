--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_FUN_DG091(
      PERIOD varchar(6) CHARACTER SET UTF8                           )
  returns (
      AMOUNT numeric(15,2))
   as
begin

  select sum(p.pvalue)
    from epayrolls e
    join EPRPOS p on (e.ref = p.payroll)
    where e.empltype = 1
      and e.tper = :PERIOD
      and p.ecolumn = 4000
  into :AMOUNT;
  AMOUNT = AMOUNT/1000;
  suspend;
end^
SET TERM ; ^
