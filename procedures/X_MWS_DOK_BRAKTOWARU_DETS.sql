--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_DOK_BRAKTOWARU_DETS(
      WH DEFMAGAZ_ID)
  returns (
      VERS WERSJE_ID,
      GOOD KTM_ID,
      POSQ ILOSCI_MAG,
      LOCQ ILOSCI_MAG)
   as
declare variable LI MWSORDS_ID;
declare variable PZ MWSORDS_ID;
begin
  --[PM] typowanie pozycji, do ktorych brakuje stanu

  for
    select l.vers, l.good, sum(p.iloscl - p.ilosconmwsacts)
      from mwsgoodsrefill l
        left join dokumpoz p on p.ref = l.docposid
        left join dokumnag d on l.docid =  d.ref
      where l.wh = :wh
      group by l.vers, l.good
      into vers, good, posq
  do begin
    locq = 0;
    select sum( s.quantity + s.ordered  - s.blocked)
      from mwsstock s
      left join mwsconstlocs c on c.ref = s.mwsconstloc
      where s.vers = :vers and s.wh = :wh and ((s.actloc = 1 and c.goodssellav = 1) or (c.symbol = 'X1') )
      into locq;
    if (locq is null) then locq = 0;
    li = 0;
    select first 1 o.ref
      from mwsstock s
        left join mwsconstlocs c on c.ref = s.mwsconstloc
        left join mwsords o on o.docsymbs = c.symbol and o.status in (1,2) and o.mwsordtype = 18
      where s.wh = :wh and s.vers = :vers
      into li;
    if (li is null) then li = 0;
    pz = 0;
    select first 1 a.ref
      from mwsacts a
        left join mwsords o on o.ref = a.mwsord
      where a.wh = :wh and a.vers = :vers and a.status = 0 and a.mwsordtypedest =2 and a.quantity > 0 and o.status < 5
      into pz;
    if (pz is null) then pz = 0;
    if (posq > locq and li = 0 and pz = 0) then
      suspend;
  end
end^
SET TERM ; ^
