--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_CHECK_DDL(
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      CREATE_OR_ALTER smallint)
  returns (
      DDL blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           )
   as
declare variable CONSTRAINT_RELATION_NAME varchar(80);
declare variable TRIGGER_SOURCE blob sub_type 1 segment size 80;
declare variable EOL varchar(2);
begin
    eol ='
    ';
    ddl = '';
    for select distinct cast(t.rdb$trigger_source as varchar(8191)), trim(rc.rdb$relation_name)
    from rdb$relation_constraints rc
      join rdb$check_constraints cc on rc.rdb$constraint_name = cc.rdb$constraint_name
      join rdb$triggers t on cc.rdb$trigger_name = t.rdb$trigger_name
    where rc.rdb$constraint_type = 'CHECK'
      and t.rdb$trigger_type = 1
      and rc.rdb$constraint_name not starting with 'ibe$'
      and rc.rdb$relation_name = :nazwa
    order by cast(t.rdb$trigger_source as varchar(8191))
    into :trigger_source, :CONSTRAINT_RELATION_NAME
    do begin
      if(ddl<>'') then
        ddl = ddl || eol;
      ddl = ddl || 'ALTER TABLE '||CONSTRAINT_RELATION_NAME||' ADD '||trigger_source||';';
    end

    suspend;
end^
SET TERM ; ^
