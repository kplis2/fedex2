--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WFEVENTS_UPDATEORINSERT(
      REF WFEVENTS_ID,
      UUID NEOS_ID,
      STATE SMALLINT_ID,
      OBJECTNAME NEOS_ID NOT NULL,
      SYMBOL STRING40 = null,
      KEY STRING40 = null,
      DATA STRING255 = null)
  returns (
      OUTPUT_REF WFEVENTS_ID)
   as
begin
  update or insert into WFEVENTS (REF, UUID, STATE, OBJECTNAME, SYMBOL, "KEY", DATA)
  values (:REF, :UUID, :STATE, :OBJECTNAME, :SYMBOL, :"KEY", :DATA)
  matching (REF)
  returning REF
  into :OUTPUT_REF;
  suspend;
end^
SET TERM ; ^
