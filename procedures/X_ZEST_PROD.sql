--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_ZEST_PROD(
      MAGAZ char(3) CHARACTER SET UTF8                           ,
      DATA varchar(6) CHARACTER SET UTF8                           )
  returns (
      DYW varchar(20) CHARACTER SET UTF8                           ,
      WYKONZB numeric(14,4),
      WYKONPOJ numeric(14,4),
      WYKONZB2 numeric(14,4),
      WYKONZB3 numeric(14,4),
      WYKONPOJ2 numeric(14,4),
      WYKONZB4 numeric(14,4))
   as
DECLARE VARIABLE SUBS INTEGER;
begin


for select distinct substring(dp.ktm from 2 for 3) from dokumnag dn join dokumpoz dp on (dn.ref = dp.dokument)

where dn.typ = 'PWP' and dn.akcept = 1 and dn.magazyn = :magaz


into :dyw


do begin
subs = cast(substring(:data from  5 for 6)as integer);

select sum(dp.ilosc) from dokumnag dn join dokumpoz dp on (dn.ref = dp.dokument)

where dn.typ = 'PWP' and dn.akcept = 1
and dn.magazyn = :magaz and substring(dp.ktm from 2 for 3) = :dyw
and dn.okres between (select okres from datatookres(:data, -1,:subs - 1,0,1))
and (select okres  from datatookres(:data, -1,-2,0,1))

group by substring(dp.ktm from 2 for 3)

into :wykonzb;

select sum(dp.ilosc) from dokumnag dn join dokumpoz dp on (dn.ref = dp.dokument)

where dn.typ = 'PWP' and dn.akcept = 1
and dn.magazyn = :magaz and substring(dp.ktm from 2 for 3) = :dyw
and dn.okres = (select okres  from datatookres(:data, -1,-1,0,1))

group by substring(dp.ktm from 2 for 3)

into :wykonpoj;

select sum(dp.ilosc) from dokumnag dn join dokumpoz dp on (dn.ref = dp.dokument)

where dn.typ = 'PWP' and dn.akcept = 1
and dn.magazyn = :magaz and substring(dp.ktm from 2 for 3) = :dyw
and dn.okres between (select okres from datatookres(:data, -1,:subs - 1,0,1))
and (select okres  from datatookres(:data, -1,-1,0,1))

group by substring(dp.ktm from 2 for 3)

into :wykonzb2;

select sum(dp.ilosc) from dokumnag dn join dokumpoz dp on (dn.ref = dp.dokument)

where dn.typ = 'PWP' and dn.akcept = 1
and dn.magazyn = :magaz and substring(dp.ktm from 2 for 3) = :dyw
and dn.okres between (select okres from datatookres(:data, 0,:subs - 1,0,1))
and (select okres  from datatookres(:data, -1,-2,0,1))

group by substring(dp.ktm from 2 for 3)

into :wykonzb3;





select sum(dp.ilosc) from dokumnag dn join dokumpoz dp on (dn.ref = dp.dokument)

where dn.typ = 'PWP' and dn.akcept = 1
and dn.magazyn = :magaz and substring(dp.ktm from 2 for 3) = :dyw
and dn.okres = (select okres  from datatookres(:data, 0,-1,0,1))

group by substring(dp.ktm from 2 for 3)

into :wykonpoj2;

select sum(dp.ilosc) from dokumnag dn join dokumpoz dp on (dn.ref = dp.dokument)

where dn.typ = 'PWP' and dn.akcept = 1
and dn.magazyn = :magaz and substring(dp.ktm from 2 for 3) = :dyw
and cast(dn.okres  as numeric(14,4)) between cast((select okres from datatookres(:data, 0,:subs - 2,0,1))as numeric(14,4))
and cast((select okres  from datatookres(:data, 0,-1,0,1)) as numeric(14,4))

group by substring(dp.ktm from 2 for 3)

into :wykonzb4;

suspend;

     end
end^
SET TERM ; ^
