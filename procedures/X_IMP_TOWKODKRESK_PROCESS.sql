--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_IMP_TOWKODKRESK_PROCESS(
      REF_IMP INTEGER_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING511)
   as
declare variable TOWAR_INT_ID integer;
declare variable GLOWNY SMALLINT_ID;
declare variable KODKRESK BARCODE;
declare variable X_IMP_REF INTEGER_ID;
declare variable KTM KTM_ID;
declare variable JEDNOSTKA TOWJEDN;
declare variable OPIS STRING1024;
declare variable TOWAR_INT_ID_STR STRING40;
declare variable kodkreskref integer_id;
begin
--procedura do pobierania informacji o kodach kreskowych z tabeli posredniej
    status = 1;
    msg = '';
    select kk.idartykulu, kk.domyslny, kk.ref, kk.kodkresk, kk.opis
        from X_IMP_TOWKODKRESK kk
        where kk.ref = :ref_imp
            into :towar_int_id, :glowny, :x_imp_ref, :kodkresk, :opis;

    towar_int_id_str = cast(towar_int_id as string40);

    select first 1 t.ktm, j.ref
        from towary t
            join towjedn j on (t.ktm = j.ktm)
        where t.int_id = :towar_int_id_str
        into :ktm, :jednostka;
    -- sprawdzenie czy sa wszystkie informacje
    if (coalesce(ktm,0) = 0) then
    begin
        status = 8;
        msg = 'brak towaru o id: '|| coalesce(:towar_int_id, '<brak>');
        exit;
    end
    if (coalesce(kodkresk,0) = 0) then
    begin
        status = 8;
        msg = 'nie podano kodu kreskowego';
        exit;
    end
    -- jezeli nie podano to kod kreskowy ma byc glowny
    glowny = coalesce(:glowny, 1);
    if (1 = status) then
    begin
        if (not exists(
                select ref from towkodkresk tw where tw.kodkresk = :kodkresk
            )) then
        begin
            --dodanie kodu kreskowego
            insert into TOWKODKRESK (KTM, WERSJAREF, KODKRESK, GL, TOWJEDNREF, INT_ID, INT_DATAOSTPRZETW, X_OPIS, x_imp_ref)
                values ( :KTM, 0, :KODKRESK, :glowny, :jednostka, :x_imp_ref, current_timestamp, :opis, :ref_imp);
        end
        else begin
            select ref
              from towkodkresk tw
              where   tw.kodkresk = :kodkresk
                  and tw.ktm = :ktm
              into :kodkreskref;
            if (:kodkreskref is not null) then
            begin
              update towkodkresk set ktm = :ktm, wersjaref = 0, kodkresk =  :kodkresk, 
                  gl= :glowny, Towjednref = :jednostka,  int_id = :x_imp_ref,INT_DATAOSTPRZETW = current_timestamp,
                  x_opis = :opis, x_imp_ref = :ref_imp where ref = :kodkreskref;
            end
            else begin
              status = 8;
              msg = msg || 'kod kreskowy zostal juz wczesniej zarejestrowany';
              exit;
            end
        end
    end
    suspend;
end^
SET TERM ; ^
