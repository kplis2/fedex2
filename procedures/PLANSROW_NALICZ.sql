--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PLANSROW_NALICZ(
      REF integer,
      KEYAMOUNT integer,
      SQLROW varchar(2000) CHARACTER SET UTF8                           ,
      STATICKEYAMOUNT smallint,
      TRYB integer)
   as
DECLARE VARIABLE KEY1 VARCHAR(20);
DECLARE VARIABLE KEY2 VARCHAR(40);
DECLARE VARIABLE KEY3 VARCHAR(40);
DECLARE VARIABLE KEY4 VARCHAR(40);
begin
  if (:statickeyamount is null) then statickeyamount = 0;
  if (tryb = 0) then
    delete from PLANSROW where PLANSROW.PLANS = :ref;
  if(:keyamount = 1) then begin
    for execute statement :SQLROW into :key1
    do begin
      if (not exists(select ref from PLANSROW where PLANSROW.plans = :ref
        and PLANSROW.key1 = :key1) or (:tryb = 0 or (:tryb = 2) ) ) then
        insert into PLANSROW (PLANS,KEY1)
                     values (:ref, :key1);
      else if (exists(select ref from PLANSROW where PLANSROW.plans = :ref
        and PLANSROW.key1 = :key1) and :tryb = 1) then
        if (:statickeyamount > 0 and :statickeyamount <= 4) then
          if (:statickeyamount = 1) then
            exception PLANS_AKTU_WSZYSTKIE_KLUCZE;
    end
  end
  if(:keyamount = 2) then
    for execute statement :SQLROW into :key1, :key2
    do begin
      if (not exists(select ref from PLANSROW where PLANSROW.plans = :ref
          and PLANSROW.key1 = :key1 and PLANSROW.key2 = :key2)
          or (:tryb = 0 or (:tryb = 2) ) ) then
        insert into PLANSROW (PLANS,KEY1, KEY2)
                       values (:ref, :key1, :key2);
      else if (:tryb = 1) then
        if (:statickeyamount > 0 and :statickeyamount <= 4) then
          if (:statickeyamount = 1 and exists(select ref from PLANSROW where PLANSROW.plans = :ref
              and PLANSROW.key1 = :key1)) then
            update PLANSROW set KEY2 = :key2 where key1 = :key1;
          else if (:statickeyamount = 2) then
            exception PLANS_AKTU_WSZYSTKIE_KLUCZE;
    end
  if(:keyamount = 3) then
    for execute statement :SQLROW into :key1, :key2, :key3
    do begin
      if (not exists(select ref from PLANSROW where PLANSROW.plans = :ref
          and PLANSROW.key1 = :key1 and PLANSROW.key2 = :key2 and PLANSROW.key3 = :key3)
          or (:tryb = 0 or (:tryb = 2) ) ) then
        insert into PLANSROW (PLANS,KEY1, KEY2, KEY3)
                       values (:ref, :key1, :key2, :key3);
      else if (:tryb = 1) then
        if (:statickeyamount > 0 and :statickeyamount <= 4) then
          if (:statickeyamount = 1 and exists(select ref from PLANSROW where PLANSROW.plans = :ref
              and PLANSROW.key1 = :key1)) then
            update PLANSROW set KEY3 = :key3, KEY2 = :KEY2 where key1 = :key1;
          if (:statickeyamount = 2 and exists(select ref from PLANSROW where PLANSROW.plans = :ref
              and PLANSROW.key1 = :key1 and PLANSROW.key2 = :key2)) then
            update PLANSROW set KEY3 = :key3  where key1 = :key1 and key2 = :key2;
          else if (:statickeyamount = 3) then
            exception PLANS_AKTU_WSZYSTKIE_KLUCZE;
    end
  if(:keyamount = 4) then
    for execute statement :SQLROW into :key1, :key2, :key3, :key4
    do begin
      if (not exists(select ref from PLANSROW where PLANSROW.plans = :ref
          and PLANSROW.key1 = :key1 and PLANSROW.key2 = :key2 and PLANSROW.key3 = :key3)
          or (:tryb = 0 or (:tryb = 2) ) ) then
        insert into PLANSROW (PLANS,KEY1, KEY2, KEY3, KEY4)
                       values (:ref, :key1, :key2, :key3, :key4);
      else if (:tryb = 1) then
        if (:statickeyamount > 0 and :statickeyamount <= 4) then
          if (:statickeyamount = 1 and exists(select ref from PLANSROW where PLANSROW.plans = :ref
              and PLANSROW.key1 = :key1)) then
            update PLANSROW set KEY4 = :key4, KEY3 = :key3, KEY2 = :KEY2 where key1 = :key1;
          if (:statickeyamount = 2 and exists(select ref from PLANSROW where PLANSROW.plans = :ref
              and PLANSROW.key1 = :key1 and PLANSROW.key2 = :key2)) then
            update PLANSROW set KEY4 = :key4 where key1 = :key1 and key2 = :key2;
          if (:statickeyamount = 3 and exists(select ref from PLANSROW where PLANSROW.plans = :ref
              and PLANSROW.key1 = :key1 and PLANSROW.key2 = :key2 and PLANSROW.key3 = :key3)) then
            update PLANSROW set KEY4 = :key4  where key1 = :key1 and key2 = :key2 and KEY3 = :key3;
          else if (:statickeyamount = 4) then
            exception PLANS_AKTU_WSZYSTKIE_KLUCZE;
    end
end^
SET TERM ; ^
