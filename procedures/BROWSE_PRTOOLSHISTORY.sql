--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BROWSE_PRTOOLSHISTORY(
      PRDSDEF varchar(20) CHARACTER SET UTF8                           ,
      EMPLOYEE integer,
      PRSCHEDZAM integer,
      PRSCHEDGUIDE integer,
      PRMACHINE integer,
      SLODEF integer,
      SLOPOZ integer,
      MODE smallint,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      DATEFROM varchar(20) CHARACTER SET UTF8                           ,
      DATETO varchar(20) CHARACTER SET UTF8                           )
  returns (
      RPRDSDEF varchar(20) CHARACTER SET UTF8                           ,
      RTOOLTYPE varchar(20) CHARACTER SET UTF8                           ,
      RPRTOOL varchar(20) CHARACTER SET UTF8                           ,
      RINVNO varchar(20) CHARACTER SET UTF8                           ,
      RPRICE numeric(14,2),
      REMPLOYEE integer,
      REMPLOYEE_S varchar(255) CHARACTER SET UTF8                           ,
      RPRSCHEDZAM integer,
      RPRSCHEDZAM_S varchar(255) CHARACTER SET UTF8                           ,
      RPRSCHEDGUIDE integer,
      RPRSCHEDGUIDE_S varchar(255) CHARACTER SET UTF8                           ,
      RPRMACHINE integer,
      RPRMACHINE_S varchar(255) CHARACTER SET UTF8                           ,
      RSLODEF integer,
      RSLOPOZ integer,
      RSLOPOZ_S varchar(255) CHARACTER SET UTF8                           ,
      RAMOUNT numeric(14,2),
      RPRTOOLNAME varchar(255) CHARACTER SET UTF8                           )
   as
begin
--  if (:mode = 0) then -- wypozyczone
  if (datefrom = '') then
    datefrom = null;
  if (dateto = '') then
    dateto = null;
  begin
    for
      select h.employees, h.prschedzam, h.prschedguide, h.prmachine, h.slodef, h.slopoz,
          h.prtools, h.tooltype, h.invno, h.price, h.prdsdef, t.name,
          sum(h.amount * (1-2*h.hsaction))
        from prtoolshistory h
          join prtoolsdocs d on (d.ref = h.prtoolsdoc)
          join prtoolsdefdocs df on (df.prdsdef = d.prdsdef and df.symbol = d.prtoolsdefdoc)
          join prtoolstypes t on (h.tooltype = t.symbol)
        where h.prdsdef = :prdsdef
          and (df.outside = 0 or :mode = 1)
          and (h.employees = :employee or coalesce(:employee,0) = 0)
          and (h.prschedzam = :prschedzam or coalesce(:prschedzam,0) = 0)
          and (h.prschedguide = :prschedguide or coalesce(:prschedguide,0) = 0)
          and (h.prmachine = :prmachine or coalesce(:prmachine,0) = 0)
          and ((h.slodef = :slodef and h.slodef = :slopoz) or coalesce(:slodef,0) = 0)
          and (d.period = :period or coalesce(:period,'') = '')
          and (d.docdate >= :datefrom or :datefrom is null)
          and (d.docdate <= :dateto or :dateto is null)
        group by h.employees, h.prschedzam, h.prschedguide, h.prmachine, h.slodef, h.slopoz,
          h.prtools, h.tooltype, h.invno, h.price, h.prdsdef, t.name
        having sum(h.amount * (1-2*h.hsaction)) <> 0
        into :remployee, :rprschedzam, :rprschedguide, :rprmachine, :rslodef, :rslopoz,
          :rprtool, :rtooltype, :rinvno, :rprice, :rPRDSDEF, :rprtoolname, :ramount
    do begin
      if (:remployee is not null ) then
        select e.personnames
          from employees e
          where e.ref = :remployee
          into :remployee_s;

      if (:rprschedzam is not null) then
        select z.number
          from prschedzam z
          where z.ref = :rprschedzam
          into :rprschedzam_s;

      if (:rprschedguide is not null) then
        select g.symbol
          from prschedguides g
          where g.ref = :rprschedguide
          into :rprschedguide_s;

      if (:rprmachine is not null) then
        select m.symbol
          from prmachines m
          where m.ref = :rprmachine
          into :rprmachine_s;

      if (:rslopoz is not null) then
        select s.nazwa
          from slopoz s
          where s.ref = :rslopoz
          into :rslopoz_s;

      suspend;
      remployee = null;
      rprschedzam = null;
      rprschedguide = null;
      rprmachine = null;
      rslodef = null;
      rslopoz = null;
      rprtool = null;
      rtooltype = null;
      rinvno = null;
      rprice = null;
      ramount  = null;
      remployee_s = null;
      rprschedzam_s = null;
      rprschedguide_s = null;
      rprmachine_s = null;
      rslopoz_s = null;
      rPRDSDEF = null;
    end
  end
end^
SET TERM ; ^
