--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_DEL_USERINFO(
      USR varchar(40) CHARACTER SET UTF8                           ,
      PSW varchar(40) CHARACTER SET UTF8                           ,
      USERID varchar(255) CHARACTER SET UTF8                           ,
      OTABLE varchar(40) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      STATUS smallint,
      MSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable currcryptedpsw varchar(255);
declare variable currsupervisor smallint;
declare variable currsupervisegroup varchar(255);
declare variable currsente smallint;
declare variable curruserid varchar(255);
declare variable oldsupervisegroup varchar(255);
begin
  currcryptedpsw = shash_hmac('hex','sha1',:psw,:usr);
  -- zautoryzuj usera ktory wywoluje procedure
  curruserid = NULL;
  select first 1 u.supervisor, u.supervisegroup, u.sente, u.userid
  from s_users u
  where u.userlogin = :usr and u.userpass = :currcryptedpsw
  into :currsupervisor, :currsupervisegroup, :currsente, :curruserid;
  if(:curruserid is null) then begin
    status = 0;
    msg = 'Brak autoryzacji do usuwania użytkowników!';
    suspend;
    exit;
  end
  -- identyfikacja przez userid lub otable i oref
  if((:userid is null or :userid='') and otable<>'' and oref is not null) then begin
    select first 1 userid from s_users where otable=:otable and oref=:oref into :userid;
  end

  -- czy user istnieje
  if(:userid<>'' and exists(select first 1 1 from s_users u where u.userid=:userid)) then begin
     -- sprawdzamy kogo usuwamy
     select first 1 u.supervisegroup from s_users u where u.userid=:userid into :oldsupervisegroup;
     if(:oldsupervisegroup is null) then oldsupervisegroup = '';
     -- czy biezacy user ma w ogole prawo go usunac
     if((:currsupervisor=1 and :oldsupervisegroup like :currsupervisegroup||'%') or  -- admin moze usunac wszystkich w swojej supervisegroup
        (:currsupervisor>1) or   -- developer moze usunac wszystkich
        (:currsente=1)  -- sente moze usunac wszystkich
       ) then begin
       -- usun usera
       delete from s_users u where u.userid=:userid;

       status = 1;
       msg = 'Usunieto użytkownika.';
       suspend;
       exit;
     end else begin
       status = 0;
       msg = 'Brak uprawnień do usuniecia użytkownika!';
       suspend;
       exit;
     end
  end else begin
    status = 0;
    msg = 'Nie znaleziono użytkownika do usuniecia!';
    suspend;
    exit;
  end
end^
SET TERM ; ^
