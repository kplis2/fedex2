--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_IS_REPROGRAMING(
      WTREF INTEGER_ID,
      WTPOZ INTEGER_ID)
  returns (
      YES SMALLINT_ID)
   as
  declare variable rwref integer_id;
  declare variable wersjaref integer_id;
  declare variable wersjarefwz integer_id;
  declare variable grupaprod integer_id;

begin
yes = 0; -- nie, nie rób nic

  select first 1  m.vers   -- pobranie wersji z WT
    from mwsacts m
    where m.mwsord = :wtref
      and m.ref = :wtpoz
  into :wersjaref;

  select m.docid -- pobranie RWref
    from mwsords m
    where m.ref = :wtref
  into rwref;

  select d.grupaprod  -- pobranie z RW, WZpozref
    from dokumnag d
    where d.ref = :rwref
  into :grupaprod;

  select first 1 d.wersjaref -- wersja towaru z WZ
    from dokumpoz d
    where d.ref = :grupaprod
 into :wersjarefwz;

  if (wersjaref <> wersjarefwz) then
  yes = 1;

  suspend;

end^
SET TERM ; ^
