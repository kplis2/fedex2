--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_WYDHIST_KTM(
      MAGAZYNY varchar(255) CHARACTER SET UTF8                           ,
      MASKAKTM varchar(40) CHARACTER SET UTF8                           ,
      DATAOD date,
      DATADO date,
      ZEWN smallint)
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      WERSJAREF integer,
      ILOSC numeric(14,4),
      ILOSCBRAK numeric(14,4),
      ILOSCINC numeric(14,4))
   as
begin
  for select g.ktm,g.wersja,g.wersjaref,
  sum(g.ilosc),sum(g.iloscbrak),sum(g.iloscinc)
  from get_wydhist(:magazyny,:maskaktm,:dataod,:datado,:zewn) g
  group by g.ktm,g.wersja,g.wersjaref
  into :ktm, :wersja, :wersjaref, :ilosc, :iloscbrak, :iloscinc
  do begin
    suspend;
  end
end^
SET TERM ; ^
