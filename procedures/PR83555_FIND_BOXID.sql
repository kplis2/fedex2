--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR83555_FIND_BOXID(
      INDEXIN STRING20)
  returns (
      COL integer,
      ROW integer,
      SIDE integer)
   as
begin
  col = cast(floor(rand() * 3 + 1) as integer);
  row = cast(floor(rand() * 3 + 1) as integer);
  side = cast(floor(rand() * 2 + 1) as integer);
  suspend;
end^
SET TERM ; ^
