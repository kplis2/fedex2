--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_SYMBOLZAM(
      DOKUMENT integer)
   as
declare variable symbzam varchar(255);
declare variable zamid varchar(40);
declare variable grupadok integer;
declare variable zamdata timestamp;
begin
  symbzam = '';
  select nagfak.grupadok from NAGFAK where ref=:dokument into :grupadok;
  for select NAGZAM.id, NAGZAM.datawe from
  NAGZAM join NAGFAK on (NAGZAM.faktura = NAGFAK.REF)
  where NAGFAK.grupadok = :grupadok
  into :zamid, :zamdata
  do begin
    if(:symbzam <>'') then symbzam= :symbzam||',';
    symbzam = :symbzam||:zamid;
    symbzam = substring(:symbzam from 1 for 80);
  end
  update NAGFAK set SYMBZAM = :SYMBZAM, DATAZAM = :ZAMDATA where GRUPADOK = :grupadok and (SYMBZAM is null or (SYMBZAM <> :symbzam));

end^
SET TERM ; ^
