--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_2(
      BKDOC integer)
   as
declare variable ACCOUNT varchar(20);
declare variable AMOUNT numeric(14,2);
declare variable SETTLEMENT varchar(20);
declare variable TMP varchar(255);
declare variable DOCDATE timestamp;
declare variable PAYDAY timestamp;
declare variable DESCRIPT varchar(255);
declare variable DICTPOS integer;
declare variable DICTDEF integer;
declare variable ODDZIAL varchar(255);
declare variable CRNOTE smallint;
declare variable KOSZTZAK numeric(14,2);
declare variable MAGAZYN varchar(255); /* dodanie magazynu potrezbnego do analityki 732 */
declare variable USLUGA smallint;
declare variable NAGFAK integer;
declare variable NAGFAK_REF integer;
declare variable FSOPERTYPE smallint;
declare variable BKPERIOD varchar(6);
declare variable VATPERIOD varchar(6);
declare variable ACCOUNTVATPERIOD varchar(1);
declare variable VATGR varchar(5);
declare variable TAXGR varchar(10);
declare variable SALEKIND varchar(2);
declare variable TERMZAP timestamp;
declare variable NUMER smallint;
declare variable SETTLEMENTNEXT varchar(20);
declare variable PROCENTPLAT numeric(14,2);
declare variable PROCAMOUNT numeric(14,2);
declare variable SUMPROCENTPLAT numeric(14,2);
declare variable SUMPROCAMOUNT numeric(14,2);
declare variable SALDOWM numeric(14,2);
declare variable DATAPLAT date;
declare variable SUMGROSSV numeric(14,2);
begin
   --  schemat dekretowania - dokumenty sprzedaży


  select B.period, B.vatperiod, B.sumgrossv, B.symbol, B.docdate, B.payday,
      B.descript, B.dictdef, B.dictpos, B.oref, T.creditnote, O.symbol, B.oref,
      case VR.vtype when 0 then '01' when 1 then '03' when 2 then '02' end
    from bkdocs B
      left join bkdoctypes T on (B.doctype = T.ref)
      left join nagfak N on (B.oref = N.ref)
      left join oddzialy O on (N.oddzial = O.oddzial)
      left join vatregs VR on (B.vatreg = VR.symbol and B.company = VR.company)
    where B.ref = :bkdoc
    into :bkperiod, :vatperiod, :sumgrossv, :settlement, :docdate, :payday,
      :descript, :dictdef, :dictpos, :nagfak, :crnote, :oddzial, :nagfak_ref,
      :salekind;

  if (coalesce(vatperiod,'') = bkperiod) then
    accountvatperiod = '0';
  else
    accountvatperiod = '1';

  execute procedure KODKS_FROM_DICTPOS(dictdef, dictpos)
    returning_values :tmp;

  if (crnote = 1) then
    fsopertype = 2;
  else
    fsopertype = 1;

  for
    select vate, vatgr, taxgr from bkvatpos where bkdoc = :bkdoc
      into :amount, :vatgr, :taxgr
  do
    execute procedure insert_decree(bkdoc, '222-' || accountvatperiod || '-' || taxgr || '-' || vatgr, 1, amount, descript, 0);

  select sum(wartnet) from nagfakzal
    where faktura = :nagfak_ref
    into :amount;

  execute procedure insert_decree(bkdoc, '845-' || tmp, 0, amount, descript, 0);


  for 
    select sum(P.wartnet-P.pwartnet), sum(P.pkosztzak), P.usluga, M.symbol
    from pozfak P
      left join defmagaz M on (P.magazyn = M.symbol)
      left join oddzialy O on (O.oddzial = M.oddzial)
    where dokument=:nagfak
    group by P.usluga, M.symbol
    into :amount, :kosztzak, :usluga, :magazyn
  do begin

    if (usluga = 1) then
       account = '703-' || salekind;
    else
       account = '702-' || salekind;

    execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);
  end
-- MW/MZ zapisy wg terminów platnosci z tabeli NAGFAKTERMPLAT
  account = '200-' || tmp;
  execute procedure fk_autodecree_termplat(bkdoc,account, sumgrossv, 0,0,'', 0 , settlement,
                                          nagfak_ref, 'NAGFAK',fsopertype,docdate,
                                          payday,descript,1)
    returning_values :sumprocentplat;
--SN zapisy wg terminów platnosci
  if (sumprocentplat = 0) then
  begin
    for select sum(p.wartbru - p.pwartbru), p.termzap
      from pozfak p
      where p.dokument = :nagfak
      group by p.termzap
      into :amount, :termzap
    do begin
      if(termzap is null) then settlementnext = settlement;
      else select settlementout from reg_settlement_name_create(:settlement, :numer) into :settlementnext;
      execute procedure insert_decree_settlement(bkdoc, '200-' || tmp, 0, amount, descript,
        :settlementnext, null, fsopertype, 1, docdate, (case when :termzap is null then :payday else :termzap end),
        null, 'NAGFAK', nagfak_ref, null, 0);
      if(termzap is not null) then numer = numer + 1;
    end
  end
end^
SET TERM ; ^
