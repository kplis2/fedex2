--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_MATERIAL_FIND_PROD(
      POZZAMMAT integer,
      MODE smallint)
  returns (
      POZZAMPROD integer)
   as
declare variable genpozref integer;
declare variable prpozzam integer;
declare variable prodpozzam integer;
declare variable prord integer;
declare variable prordp integer;
declare variable prodp integer;
declare variable out smallint;
begin
  prordp = null;
  pozzamprod = null;
  prpozzam = null;
  prodpozzam = null;
  genpozref = null;
  prord = null;
  select prpozzam, out
    from pozzam
    where ref = :pozzammat
    into prodpozzam, out;
  if (prodpozzam is null) then
    exception prnagzam_error 'Pozycja zlecenia nie jest skojarzona z żadnym produktem';
  select p.genpozref, p.prpozzam, p.zamowienie, p.out
    from pozzam p
      left join nagzam n on (n.ref = p.zamowienie)
      left join typzam t on (t.symbol = n.typzam)
    where p.ref = :prodpozzam and t.wydania = 3
    into genpozref, prpozzam, prord, out;
  if (out = 2) then
  begin
    genpozref = null;
    prpozzam = null;
    prordp = prord;
    prodp = prodpozzam;
  end
  if (prpozzam is not null and prord is not null) then
  begin
    select p.zamowienie
      from pozzam p
        left join nagzam n on (n.ref = p.zamowienie)
        left join typzam t on (t.symbol = n.typzam)
      where p.ref = :prpozzam and t.wydania = 3
      into prordp;
    prodp = prpozzam;
  end
  else if (genpozref is not null and prord is not null) then
  begin
    select p.zamowienie
      from pozzam p
        left join nagzam n on (n.ref = p.zamowienie)
        left join typzam t on (t.symbol = n.typzam)
      where p.ref = :genpozref and t.wydania = 3
      into prordp;
    prodp = genpozref;
  end else if (out <> 2) then
  begin
    pozzamprod = prodpozzam;
    exit;
  end
  if ((mode = 0 and prordp = prord and prord is not null) or (mode = 1 and prordp is not null)) then
  begin
    execute procedure PR_MATERIAL_FIND_PROD (:prodp,:mode) returning_values :pozzamprod;
  end else
    pozzamprod = prodpozzam;
end^
SET TERM ; ^
