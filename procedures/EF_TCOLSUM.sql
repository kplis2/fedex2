--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_TCOLSUM(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      COL1 integer,
      COL2 integer)
  returns (
      RET numeric(14,2))
   as
begin
/*DU: Personel - funkcja, dla zadanego zakresu skladnikow, podaje ich sume
                 wartosci z innych list plac z tego samego okresu podatkowego */

  execute procedure ef_ftcolsum(:employee, :payroll, :prefix, null, :col1, :col2)
    returning_values ret;

  suspend;
end^
SET TERM ; ^
