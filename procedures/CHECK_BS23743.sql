--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHECK_BS23743
  returns (
      COUT varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable ref integer ;
  declare variable fromdate date;
  declare variable todate date;
  declare variable employee varchar(120);
begin
  for
    select c.ref, c.fromdate, c.todate, e.personnames
      from emplcontracts c
        join employees e on (e.ref = c.employee)
      where coalesce(char_length(c.iflags),0) > 1 and c.ascode is null -- [DG] XXX ZG119346
      into :ref, :fromdate, :todate, :employee
  do begin
    cout = 'Pracownik '||:employee||' ma niewypelniony tytul ubezpieczenia na umowie '||:ref||' od '||:fromdate||' do '||:todate;
    suspend;
  end
end^
SET TERM ; ^
