--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GEN_ZAM_FROM_ANAL(
      GRUPAOPT integer)
   as
DECLARE VARIABLE WERSJEREF INTEGER;
DECLARE VARIABLE IDDOSTAWCY INTEGER;
DECLARE VARIABLE REJZAM VARCHAR(3);
DECLARE VARIABLE TYPZ VARCHAR(3);
DECLARE VARIABLE MAGAZYN VARCHAR(3);
DECLARE VARIABLE NAGZAMREF INTEGER;
DECLARE VARIABLE WALUTA VARCHAR(3);
DECLARE VARIABLE WALUTAGLOBAL VARCHAR(3);
DECLARE VARIABLE WAL SMALLINT;
DECLARE VARIABLE ILDOZAM NUMERIC(14,4);
DECLARE VARIABLE KTM VARCHAR(40);
DECLARE VARIABLE JEDNOSTKA INTEGER;
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE CENA NUMERIC(14,4);
DECLARE VARIABLE NULLREF INTEGER;
declare variable prec smallint;
begin
  nullref = null;
  execute procedure getconfig('NEWZAMREJZAK') returning_values(:rejzam);
  execute procedure getconfig('NEWZAMTYPZAK') returning_values(:typz);
  execute procedure getconfig('NEWZAMMAGOFE') returning_values(:magazyn);
  execute procedure getconfig('COUNTRY_CURRENCY') returning_values(:walutaGlobal);

  if (:nullref is null) then
  begin
    execute procedure GEN_REF('NAGZAM') returning_values(:nagzamref);
    insert into nagzam (ref, rejestr, typzam, datawe, magazyn, walutowe, grupazam, dostawca)
        values(:nagzamref, :rejzam, :typz, current_date, :magazyn, 0, :GRUPAOPT, :iddostawcy);
    nullref = :nagzamref;
  end
  for
   select wersje.ref, dostcen.dostawca, dostawcy.waluta, wersje.ktm, dostcen.cenanet, dostcen.prec
      from wersje
      left JOIN dostcen ON (dostcen.wersjaref = wersje.ref AND dostcen.gl = 1)
      left join dostawcy on (dostawcy.ref = dostcen.dostawca)
    where wersje.grupaopt = :grupaopt
      order by dostcen.dostawca
    into :wersjeref, :iddostawcy, :waluta, :ktm, :cena, :prec
  do
  begin
    nagzamref = null;
    if (not exists (
      select ref from nagzam where nagzam.grupazam = :grupaopt and nagzam.dostawca = :iddostawcy )
      and :iddostawcy is not null) then
    begin
      execute procedure GEN_REF('NAGZAM') returning_values(:nagzamref);
      if (:walutaGlobal = :waluta or :waluta is null ) then
        wal = 0;
      else
        wal = 1;
      insert into nagzam (ref, rejestr, typzam, datawe, magazyn, walutowe, grupazam, dostawca)
        values(:nagzamref, :rejzam, :typz, current_date, :magazyn, :wal, :GRUPAOPT, :iddostawcy);
    end 
    else
    begin
      if (:iddostawcy is null) then
        nagzamref = :nullref;
      else
      begin
        select ref
          from nagzam
        where grupazam = :grupaopt and dostawca = :iddostawcy
        into :nagzamref;
      end
    end
    if (:nagzamref is not null) then
    begin
      SELECT stanyil.stanmax - (stanyil.ilosc - stanyil.zarezerw - stanyil.zablokow + stanyil.zamowiono)
        FROM stanyil
      where stanyil.wersjaref = :wersjeref and stanyil.magazyn = :magazyn
      into :ildozam;
      if (:ildozam>0 ) then
      begin
        select ref
          from towjedn
        where ktm =:ktm and glowna = 1
        into :jednostka;
        insert into pozzam   (wersjaref,  ktm, ilosc, jedn, magazyn, zamowienie, cenacen, prec)
                       values(:wersjeref,  :ktm, cast(:ildozam as integer), :jednostka, :magazyn, :nagzamref, :cena, :prec);
      end
    end
  end
  for
    select ref
      from nagzam
    where grupazam = :grupaopt
    into :nagzamref
  do
  begin
    select count(*)
      from pozzam
    where pozzam.zamowienie = :nagzamref
    into :cnt;
    if (:cnt=0) then
      delete from nagzam where ref = :nagzamref;
  end
end^
SET TERM ; ^
