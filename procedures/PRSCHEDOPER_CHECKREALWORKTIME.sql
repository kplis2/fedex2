--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDOPER_CHECKREALWORKTIME(
      SCHEDOPER integer,
      MACHINE integer,
      STARTTIME timestamp,
      WORKTIME double precision)
  returns (
      REALWORKTIME float)
   as
declare variable endtime timestamp;
declare variable prdepart varchar(10);
declare variable schedule integer;
declare variable minlength double precision;
declare variable operendtime timestamp;
declare variable setuptime integer;
declare variable old_shoper varchar(255);
declare variable new_shoper varchar(255);
begin
  realworktime = worktime;
  minlength = 1;
  minlength = :minlength/1440;
  select prschedules.prdepart, prschedules.ref
    from prschedopers
      left join PRschedules on (prschedules.ref = prschedopers.schedule)
    where prschedopers.ref = :schedoper
    into :prdepart, :schedule;
  if(:machine is not null) then begin
    --srawdzenie, czy bedzie wystepować czas przezbrojenia - wowczas dodanie jego, jesli poprzednia operacja jest innej kartytechnologicznej
    select prmachines.meansetuptime from PRMACHINES where ref=:machine into :setuptime;

    if(:setuptime > 0) then begin
      -- sprawdzenie czy poprzednia operacja jest inna od nowej;
      select descript from prshopers where ref=(select shoper from prschedopers where prschedopers.ref = :schedoper) into :new_shoper;
      select descript from prshopers where ref=(select first 1 shoper from prschedopers where schedule = :schedule and machine = :machine order by endtime 

desc) into :old_shoper;
      if (:old_shoper<>:new_shoper) then begin
        select max(endtime) from prschedopers
        where schedule = :schedule and machine = :machine and endtime < :starttime
        into :operendtime;
        if(:operendtime + (:minlength * :setuptime) > :starttime) then
          realworktime = :realworktime + ((:minlength * :setuptime)-(:starttime - :operendtime));
      end
    end

    --TODO: sprawdzenie, czy w czasie realworktime od startu nie ma przerw maszyny, jesli tak, to dodanie ich do realworktime
  end
  execute procedure pr_calendar_checkendtime(:prdepart, :starttime, :realworktime) returning_values :endtime;
--  exception test_break :prdepart ||' '||:starttime||' '||:endtime;
  realworktime = :endtime - :starttime;
  --TODO: sprawdzenie, czy w czasie realworktime nie ma przerw w pracy wydzialu
end^
SET TERM ; ^
