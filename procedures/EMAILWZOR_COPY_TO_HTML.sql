--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWZOR_COPY_TO_HTML(
      EMAILWZORSYMBOL varchar(40) CHARACTER SET UTF8                           )
   as
declare variable EMAILWIADREF integer;
declare variable OD varchar(512);
declare variable DOKOGO varchar(2048);
declare variable TYTUL STRING1024;
declare variable TRESC MEMO5000;
begin
  --przenoszenie tresci z EMAILWZOR do EMAILWIAD po redakcji
  select EMAILWIAD, OD, DOKOGO, TYTUL, TRESC
   from EMAILWZOR where symbol=:emailwzorsymbol
  into :emailwiadref, :od, :dokogo, :tytul, :tresc;
  if(:emailwiadref is null or (:emailwiadref = 0) )then
    exit;
  update EMAILWIAD set od = :od, dokogo = :dokogo,
    tytul = :tytul, tresc = :tresc where REF=:emailwiadref;
end^
SET TERM ; ^
