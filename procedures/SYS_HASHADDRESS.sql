--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_HASHADDRESS(
      FIRMA integer)
  returns (
      IMIE varchar(255) CHARACTER SET UTF8                           ,
      NAZWISKO varchar(255) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      SKROT varchar(255) CHARACTER SET UTF8                           ,
      ULICA varchar(255) CHARACTER SET UTF8                           ,
      KODP varchar(20) CHARACTER SET UTF8                           ,
      MIASTO varchar(255) CHARACTER SET UTF8                           ,
      CPWOJ16M integer,
      NIP varchar(20) CHARACTER SET UTF8                           )
   as
declare variable r integer;
declare variable kodgus varchar(20);
declare variable i integer;
declare variable total integer;
declare variable digit integer;
declare variable scales integer;
begin
  -- imie, nazwisko i nazwa
  if(:firma=1) then begin
    imie = '';
    select max(number)-1 from s_hashdata where datatype=11 into :r;
    select first 1 val, val2 from s_hashdata where datatype=11 and number>=cast(rand()*:r as integer)+1 into :nazwa, :skrot;
    nazwisko = :nazwa;
  end else begin
    if(rand()>0.5) then begin -- mezczyzna
      select max(number)-1 from s_hashdata where datatype=1 into :r;
      select first 1 val from s_hashdata where datatype=1 and number>=cast(rand()*:r as integer)+1 into :imie;
      select max(number)-1 from s_hashdata where datatype=3 into :r;
      select first 1 val from s_hashdata where datatype=3 and number>=cast(rand()*:r as integer)+1 into :nazwisko;
      nazwa = :imie || ' ' ||:nazwisko;
      skrot = :nazwisko;
    end else begin -- kobieta
      select max(number)-1 from s_hashdata where datatype=2 into :r;
      select first 1 val from s_hashdata where datatype=2 and number>=cast(rand()*:r as integer)+1 into :imie;
      select max(number)-1 from s_hashdata where datatype=3 into :r;
      select first 1 val from s_hashdata where datatype=3 and number>=cast(rand()*:r as integer)+1 into :nazwisko;
      nazwa = :imie || ' ' ||:nazwisko;
      skrot = :nazwisko;
    end
  end

  --ulica i numer
  select max(number)-1 from s_hashdata where datatype=9 into :r;
  select first 1 val from s_hashdata where datatype=9 and number>=cast(rand()*:r as integer)+1 into :ulica;
  ulica = :ulica || ' ' || cast(rand()*100 as integer);
  if(rand()>0.9) then begin
    ulica = :ulica || '/' || cast(rand()*10 as integer);
  end

  --miasto
  select max(number)-1 from s_hashdata where datatype=10 into :r;
  select first 1 val, val2, val3 from s_hashdata where datatype=10 and number>=cast(rand()*:r as integer)+1
  into :miasto, :kodp, :kodgus;
  cpwoj16m = null;
  select ref from cpwoj16m where guscode=:kodgus into :cpwoj16m;

  --nip
  nip = '';
  i = 1;
  total = 0;
  while (i < 10) do begin
    digit = cast(rand()*9 as integer);
    nip = :nip || :digit;
    if (i = 1) then scales = 6;
    if (i = 2) then scales = 5;
    if (i = 3) then scales = 7;
    if (i = 4) then scales = 2;
    if (i = 5) then scales = 3;
    if (i = 6) then scales = 4;
    if (i = 7) then scales = 5;
    if (i = 8) then scales = 6;
    if (i = 9) then scales = 7;
    total = :total + (:scales * :digit);
    i = i + 1;
  end
  digit = mod(:total, 11);
  if(:digit=10) then digit = 0;
  nip = :nip || :digit;

  suspend;
end^
SET TERM ; ^
