--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POTENTIALSLAVES(
      BKACCOUNTSEF varchar(4096) CHARACTER SET UTF8                           ,
      PATTERNBKYEARID YEARS_ID)
  returns (
      NAMEC STRING80,
      NAMEB STRING40,
      REFB YEARS_ID)
   as
declare variable current_company companies_id;
declare variable val string;
declare variable f smallint;
declare variable whereexpression varchar(4096);
declare variable sql string8191;
declare variable patternbkyear bkyears_id;
declare variable tmpref bkyears_id;
declare variable tmpcompany companies_id;
declare variable tmpyear years_id;
declare variable allow_bkyear smallint_id;
begin

  if (bkaccountsef = '') then
    exception universal'Brak wybranych kont wzorcowych';

  --exception universal''||:bkaccountsef;

   -- ściągamy obecny zakład
  execute procedure get_global_param('CURRENTCOMPANY')
    returning_values :current_company;

  select b.ref
    from bkyears b
    where b.company = :current_company and b.yearid = :patternbkyearid
    into :patternbkyear;

  bkaccountsef = substring( bkaccountsef from 1 for char_length(bkaccountsef) );
  bkaccountsef = substring( bkaccountsef from 1 for char_length(bkaccountsef));
  whereexpression = '( ';

  for
    select outstring
    from parse_lines(:bkaccountsef,';')
    into :val
  do begin
    if (f is not null) then
      whereexpression = whereexpression||' , '||val;
    else
      whereexpression = whereexpression||val;
    f=0;
  end

  whereexpression = whereexpression||')';

 /* SQL ='select distinct b.ref, c.ref, c.name, b.name
    from companies c join bkyears b on (b.company = c.ref)
    left join bkaccounts ba on( ba.company = b.company and ba.yearid = b.yearid)
    where b.patternref =' ||patternbkyear; -- (ba.pattern_ref not in '||whereexpression||' or ba.pattern_ref is null ) and
  */

  for
    select distinct b.ref, c.ref, c.name, b.name
      from companies c join bkyears b on (b.company = c.ref)
      where b.patternref = :patternbkyear
      into :tmpref, :refb, :namec, :nameb
  do begin
    select b.company, b.yearid
      from bkyears b
      where b.ref = :tmpref
      into :tmpcompany, :tmpyear;
    SQL = 'select first 1 1 from bkaccounts b where b.pattern_ref in '||whereexpression||' and b.company ='||:tmpcompany||' and b.yearid = '||:tmpyear;
    allow_bkyear = null;
    execute statement SQL
      into :allow_bkyear;
    if (allow_bkyear is null)then
      suspend;
  end
end^
SET TERM ; ^
