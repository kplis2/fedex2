--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_VAT7K_9(
      PERIOD varchar(1) CHARACTER SET UTF8                           ,
      YEARID varchar(4) CHARACTER SET UTF8                           ,
      PAR_K40 numeric(14,2),
      PAR_K55 numeric(14,2),
      PAR_K56 numeric(14,2),
      PAR_K57 numeric(14,2),
      COMPANY integer)
  returns (
      WSP35 numeric(14,2),
      K10 numeric(14,2),
      K11 numeric(14,2),
      K12 numeric(14,2),
      K13 numeric(14,2),
      K14 numeric(14,2),
      K15 numeric(14,2),
      K16 numeric(14,2),
      K17 numeric(14,2),
      K18 numeric(14,2),
      K19 numeric(14,2),
      K20 numeric(14,2),
      K21 numeric(14,2),
      K22 numeric(14,2),
      K23 numeric(14,2),
      K24 numeric(14,2),
      K25 numeric(14,2),
      K26 numeric(14,2),
      K27 numeric(14,2),
      K28 numeric(14,2),
      K29 numeric(14,2),
      K30 numeric(14,2),
      K31 numeric(14,2),
      K32 numeric(14,2),
      K33 numeric(14,2),
      K34 numeric(14,2),
      K35 numeric(14,2),
      K36 numeric(14,2),
      K37 numeric(14,2),
      K38 numeric(14,2),
      K39 numeric(14,2),
      K40 numeric(14,2),
      K41 numeric(14,2),
      K42 numeric(14,2),
      K43 numeric(14,2),
      K44 numeric(14,2),
      K45 numeric(14,2),
      K46 numeric(14,2),
      K47 numeric(14,2),
      K48 numeric(14,2),
      K49 numeric(14,2),
      K50 numeric(14,2),
      K51 numeric(14,2),
      K52 numeric(14,2),
      K53 numeric(14,2),
      K54 numeric(14,2),
      K55 numeric(14,2),
      K56 numeric(14,2),
      K57 numeric(14,2),
      K58 numeric(14,2))
   as
declare variable VATGR varchar(6);
declare variable NETV numeric(14,2);
declare variable VATV numeric(14,2);
declare variable TAXGR varchar(10);
declare variable BKREG varchar(10);
declare variable WRONGDOC varchar(20);
declare variable BKDOCNUMBER integer;
declare variable VAT_NALEZNY varchar(255);
declare variable VAT_NALICZONY varchar(255);
declare variable SDATE date;
declare variable FDATE date;
declare variable NP numeric(14,2);
declare variable ODOKRESU varchar(6);
declare variable DOOKRESU varchar(6);
declare variable K15_3 numeric(14,2);
declare variable K15_5 numeric(14,2);
declare variable K17_7 numeric(14,2);
declare variable K17_8 numeric(14,2);
declare variable K19_22 numeric(14,2);
declare variable K19_23 numeric(14,2);
declare variable vregtype smallint;
declare variable vgrtype smallint;
begin

  -- paramert Konfiguracji
  execute procedure get_config('VAT7_NALEZNY', 0) -- naliczanie wg rejestrów
    returning_values :vat_nalezny;
  execute procedure get_config('VAT7_NALICZONY', 0) -- naliczanie staweK <> 0
    returning_values :vat_naliczony;

  if (PAR_K40 is null) then
    PAR_K40 = 0;
  if (PAR_K55 is null) then
    PAR_K55 = 0;
  if (PAR_K56 is null) then
    PAR_K56 = 0;
  if (PAR_K57 is null) then
    PAR_K57 = 0;
  WSP35 = 100; --domyslnie caly mozna odliczyc
  K10 = 0;
  K11 = 0;
  K12 = 0;
  K13 = 0;
  K14 = 0;
  K15 = 0;
  K15_3 = 0;
  K15_5 = 0;
  K16 = 0;
  K17 = 0;
  K17_7 = 0;
  K17_8 = 0;
  K18 = 0;
  K19 = 0;
  K19_22 = 0;
  K19_23 = 0;
  K20 = 0;
  K21 = 0;
  K22 = 0;
  K23 = 0;
  K24 = 0;
  K25 = 0;
  K26 = 0;
  K27 = 0;
  K28 = 0;
  K29 = 0;
  K30 = 0;
  K31 = 0;
  K32 = 0;
  K33 = 0;
  K34 = 0;
  K35 = 0;
  K36 = 0;
  K37 = 0;
  K38 = 0;
  K39 = 0;
  K40 = PAR_K40;
  K41 = 0;
  K42 = 0;
  K43 = 0;
  K44 = 0;
  K45 = 0;
  K46 = 0;
  K47 = 0;
  K48 = 0;
  K49 = 0;
  K50 = 0;
  K51 = 0;
  K52 = 0;
  K53 = 0;
  K54 = 0;
  K55 = PAR_K55;
  K56 = PAR_K56;
  K57 = PAR_K57;
  K58 = 0;
  -- sprzedaz Krajowa
  -- vtype = 1 Korygowana jest deKlaracja w Ktorym zostala wystawiony doKument WDT (oKres zrodlowy)
  -- vtype = 2 KoreKta jest w biezacej deKlaracji zgodnie z data otrzymania potwierdzenia EXPORT (oKres biezacy otrzymania KoreKty)

  if (period = 0) then
  begin
    odokresu = :yearid||'01';
    dookresu = :yearid||'03';
  end
  if (period = 1) then
  begin
    odokresu = :yearid||'04';
    dookresu = :yearid||'06';
  end
  if (period = 2) then
  begin
    odokresu = :yearid||'07';
    dookresu = :yearid||'09';
  end
  if (period = 3) then
  begin
    odokresu = :yearid||'10';
    dookresu = :yearid||'12';
  end



  for select bKvatpos.vatgr, sum(bKvatpos.netv), sum(bKvatpos.vatv)
      from bKdocs
      join vatregs on (vatregs.symbol = bKdocs.vatreg)
      join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
      join grvat on grvat.symbol = bkvatpos.taxgr   --<<PL:PR73894>>
      where bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
        and vatregs.vtype = 0 and bKdocs.status >= 0 and bKdocs.company= vatregs.company
        and bKdocs.company = :company 
        and bkvatpos.taxgr <> 'C41'
        and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1)) --<<PL:PR73894>>
    group by bKvatpos.vatgr, bKvatpos.taxgr
    into :vatgr, :netv, :vatv
  do begin
    if (vatgr = '22') then
    begin
      K19 = K19 + netv;
      K19_22 = K19_22 + netv;
      K20 = K20 + vatv;
    end
    else if (vatgr = '23') then
    begin
      K19 = K19 + netv;
      K19_23 = K19_23 + netv;
      K20 = K20 + vatv;
    end
    else if (vatgr = '07') then
    begin
      K17 = K17 + netv;
      K17_7 = K17_7 + netv;
      K18 = K18 + vatv;
    end
    else if (vatgr = '08') then
    begin
      K17 = K17 + netv;
      K17_8 = K17_8 + netv;
      K18 = K18 + vatv;
    end
    else if (vatgr = '03') then
    begin
      K15 = K15 + netv;
      K15_3 = K15_3 + netv;
      K16 = K16 + vatv;
    end else if (vatgr = '05') then
    begin
      K15 = K15 + netv;
      K15_5 = k15_5 + netv;
      K16 = K16 + vatv;
    end else if (vatgr = 'ZW') then
    begin
      K10 = K10 + netv;
    end
    else if (vatgr = '00') then
    begin
      K13 = K13 + netv;
      if (taxgr = 'P23') then
      begin
        K14 = K14 + netv;
      end
    end
    --else
      --exception test_breaK 'Nieznana stawKa podatKowa ' || vatgr;
  end

/*
  EKSPORT TOWARÓW (p.22)
  cały eksport z wyj. grup C21 i C22(11)
  vatregs.vtype = 2 and bKvatpos.taxgr <> 'C21' and bKvatpos.taxgr <> 'C22(11)'
*/
  K22 = 0;
  select sum(bKvatpos.netv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on grvat.symbol = bkvatpos.taxgr   --<<PL:PR73894>>
    where ((bKvatpos.taxgr <> 'C21' and bKvatpos.taxgr <> 'C22(11)') or bKvatpos.taxgr is null)
      and vatregs.vtype = 2 and bKdocs.status > 0 and bKdocs.company= vatregs.company
      and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and bKdocs.company = :company
      and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1)) --<<PL:PR73894>>
    into :K22;

/*
  DOSTAWA TOWARÓW ORAZ ŚWIADCZENIE USŁUG POZA TERYTORIUM KRAJU (p. 11)
  tylko eksport z grupą C21 lub C22(11)
  vatregs.vtype = 2 and (bKvatpos.taxgr = 'C21' or bKvatpos.taxgr = 'C22(11)')
*/
  select sum(bKvatpos.netv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on grvat.symbol = bkvatpos.taxgr   --<<PL:PR73894>>
    where (bKvatpos.taxgr = 'C21' or bKvatpos.taxgr = 'C22(11)') and bKdocs.company = :company
    and vatregs.vtype = 2 and bKdocs.status > 0 and bKdocs.company= vatregs.company
    and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
    and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1)) --<<PL:PR73894>>
    into :K11;

/*
  DOSTAWA TOWARÓW ORAZ ŚWIADCZENIE USŁUG POZA TERYTORIUM KRAJU art. 100 (p. 12)
  tylko eksport z grupą C22(11)
  vatregs.vtype = 2 and bKvatpos.taxgr = 'C22(11)'
*/
  select sum(bKvatpos.netv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on grvat.symbol = bkvatpos.taxgr   --<<PL:PR73894>>
    where bKvatpos.taxgr = 'C22(11)' and bKdocs.company = :company
    and vatregs.vtype = 2 and bKdocs.status > 0 and bKdocs.company= vatregs.company
    and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
    and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1)) --<<PL:PR73894>>
  into :K12;

/*
  DOSTAWA TOWARÓW ORAZ ŚWIADCZENIE USŁUG POZA TERYTORIUM KRAJU (p. 11)
  tylko WDT ze stawką NP
  vatregs.vtype = 1 and bKvatpos.vatgr = 'NP'  
*/
  select sum(bKvatpos.netv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on grvat.symbol = bkvatpos.taxgr   --<<PL:PR73894>>
    where bKvatpos.vatgr = 'NP' and bKdocs.company = :company
    and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
    and vatregs.vtype = 1 and bKdocs.status > 0 and bKdocs.company= vatregs.company
    and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1)) --<<PL:PR73894>>
    into :np;

  K11 = coalesce(K11, 0) + coalesce(np, 0);

/*
  WEWNĄTRZWSPÓLNOTOWA DOSTAWA TOWARÓW (p. 21)
  cała WDT z wyj. stawki NP
  vatregs.vtype = 1 and bKvatpos.vatgr <> 'NP'
*/
  select sum(bKvatpos.netv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on grvat.symbol = bkvatpos.taxgr   --<<PL:PR73894>>
    where bKdocs.company = :company
    and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and vatregs.vtype = 1 and bKdocs.status > 0 and bKdocs.company= vatregs.company
      and bKvatpos.vatgr <> 'NP'
      and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1)) --<<PL:PR73894>>
  into :K21;

/*
  WEWNĄTRZWSPÓLNOTOWE NABYCIE TOWARU (p. 23, 24)
  całe WNT z wyj. grup C36, C41 i C42(11)
  vatregs.vtype = 5 and (bKvatpos.taxgr <> 'C36' and bKvatpos.taxgr <> 'C42(11)' and bKvatpos.taxgr <> 'C41')
*/
    select sum(bKvatpos.netv),
      sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on grvat.symbol = bkvatpos.taxgr   --<<PL:PR73894>>
    where ((bKvatpos.taxgr <> 'C36' and bKvatpos.taxgr <> 'C42(11)' and bKvatpos.taxgr <> 'C41') or bKvatpos.taxgr is null)
      and vatregs.vtype = 5 and bKdocs.status > 0 and bKdocs.company= vatregs.company
      and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and bKdocs.company = :company
      and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1)) --<<PL:PR73894>>
    into :K23, :K24;

/*
  DOSTAWA TOWARÓW ORAZ ŚWIADCZENIE USŁUG, DLA KTÓRYCH PODATNIKIEM JEST NABYWCA (p. 31, 32)
  wszystkie dokumenty z grupą C36 lub C41 lub C42(11)
  bKvatpos.taxgr = 'C36' or bKvatpos.taxgr = 'C42(11)' or bKvatpos.taxgr = 'C41'
*/
/*  select sum(bKvatpos.netv),
      sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    where (bKvatpos.taxgr = 'C36' or bKvatpos.taxgr = 'C42(11)' or bKvatpos.taxgr = 'C41')
      and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and bKdocs.status > 0 and bKdocs.company= vatregs.company
      and bKdocs.company = :company
    into :K31, :K32;   */

/*
  KWOTA PODATKU NALEŻNEGO OD TOWARÓW I USŁUG OBJĘTYCH SPISEM Z NATURY (p. 33)
  sprzedaż krajowa z grupą P40 lub C43(11)
  vatregs.vtype = 0 and (bKvatpos.taxgr = 'P40' or bKvatpos.taxgr = 'C43(11)')
*/
    select sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on grvat.symbol = bkvatpos.taxgr   --<<PL:PR73894>>
    where (bKvatpos.taxgr = 'P40' or bKvatpos.taxgr = 'C43(11)' or bKvatpos.taxgr is null)
      and vatregs.vtype = 0 and bKdocs.status > 0 and bKdocs.company= vatregs.company
      and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and bKdocs.company = :company
      and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1)) --<<PL:PR73894>>
    into :K33;

/* 
  KWOTA PODATKU NALICZONEGO WYNIKAJĄCEGO ZE SPISU Z NATURY (p. 38)
  zakupy krajowe z grupą P45 lub D48(11)
  vatregs.vtype = 3 and (bKvatpos.taxgr = 'P45' or bKvatpos.taxgr = 'D48(11)')
*/
 /*   select sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    where (bKvatpos.taxgr = 'P45' or bKvatpos.taxgr = 'D48(11)' or bKvatpos.taxgr is null)
      and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and vatregs.vtype = 3 and bKdocs.status > 0 and bKdocs.company= vatregs.company
      and bKdocs.company = :company
    into :K38;
*/
/*
  WNT - NABYCIE ŚRODKÓW TRANSPORTU (p. 34)
  tylko WNT z grupą P39 lub C44(11)
  vatregs.vtype = 5 and (bKvatpos.taxgr = 'P39' or bKvatpos.taxgr = 'C44(11)')
*/
    select sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on grvat.symbol = bkvatpos.taxgr   --<<PL:PR73894>>
    where (bKvatpos.taxgr = 'P39' or bKvatpos.taxgr = 'C44(11)' or bKvatpos.taxgr is null)
      and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and vatregs.vtype = 5 and bKdocs.status > 0 and bKdocs.company= vatregs.company
      and bKdocs.company = :company
      and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1)) --<<PL:PR73894>>
    into :K34;

/*
  IMPORT USŁUG (p. 27, 28)
  dokumenty z rejestru VAT typu Import usług
  vatregs.vtype = 6
*/
    select sum(bKvatpos.netv),
      sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on grvat.symbol = bkvatpos.taxgr   --<<PL:PR73894>>
    where bKdocs.company = :company
      and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and vatregs.vtype = 6 and bKdocs.status > 0 and bKdocs.company= vatregs.company
      and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1)) --<<PL:PR73894>>
    into :K27, :K28;

/*             --
  IMPORT USŁUG art. 28b (p. 29, 30)
  dokumenty z rejestru VAT typu Import usług z grupą C39(11)
  vatregs.vtype = 6 and bKvatpos.taxgr = 'C39(11)'
*/
    select sum(bKvatpos.netv),
      sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (vatregs.symbol = bKdocs.vatreg)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on grvat.symbol = bkvatpos.taxgr   --<<PL:PR73894>>
    where bKdocs.company = :company
      and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
      and vatregs.vtype = 6 and bKdocs.status > 0 and bKdocs.company= vatregs.company
      and bKvatpos.taxgr = 'C39(11)'
      and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1)) --<<PL:PR73894>>
    into :K29, :K30;


    select sum(bKvatpos.netv),
      sum(bKvatpos.vatv)
    from bKdocs
    join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
      and vatregs.vtype = 6 and bKdocs.status > 0 and bKdocs.company= vatregs.company)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on grvat.symbol = bkvatpos.taxgr   --<<PL:PR73894>>
    where bKdocs.company = :company and bKvatpos.taxgr = 'C39(11)'
    and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1)) --<<PL:PR73894>>
    into :K29, :K30;

    /* Dostawa towarów, dla której podatnikiem jest nabywca (sprzedaż)
  wszystkie dokumenty z grupą C41 lub każda inna grupa zdefiniowana u klienta, która to oznacza
*/

    select sum(bKvatpos.netv)
    from bKdocs
    join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
      and vatregs.vtype = 0 and bKdocs.status > 0 and bKdocs.company= vatregs.company)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on grvat.symbol = bkvatpos.taxgr   --<<PL:PR73894>>
    where bKdocs.company = :company
    and (bKvatpos.taxgr = 'C41')
    and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1)) --<<PL:PR73894>>
    into :K31;

/* Dostawa towarów, dla której podatnikiem jest nabywca (zakup)
  wszystkie dokumenty z grupą C36 i oznaczajacymi to samo jak C42 i C42(11) lub każda inna grupa zdefiniowana u klienta, która to oznacza
*/
    select sum(bKvatpos.netv), sum(bKvatpos.vate)
    from bKdocs
    join vatregs on (bKdocs.vatperiod = :period and vatregs.symbol = bKdocs.vatreg
      and vatregs.vtype = 3 and bKdocs.status > 0 and bKdocs.company= vatregs.company)
    join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
    join grvat on grvat.symbol = bkvatpos.taxgr   --<<PL:PR73894>>
    where bKdocs.company = :company
    and (bKvatpos.taxgr = 'C36' or bKvatpos.taxgr = 'C42' or bKvatpos.taxgr = 'C42(11)')
    and (vatregs.vatregtype = 0 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 1)) --<<PL:PR73894>>
    into :K34, :K35;

  if (odokresu <= '200512') then
  begin
    K10 = cast(K10 * 100 as integer) / 100;
    K11 = cast(K11 * 100 as integer) / 100;
    K12 = cast(K12 * 100 as integer) / 100;
    K13 = cast(K13 * 100 as integer) / 100;
    K14 = cast(K14 * 100 as integer) / 100;
    K15 = cast(K15 * 100 as integer) / 100;
    K16 = cast(K16 * 100 as integer) / 100;
    K17 = cast(K17 * 100 as integer) / 100;
    K18 = cast(K18 * 100 as integer) / 100;
    K19 = cast(K19 * 100 as integer) / 100;
    K20 = cast(K20 * 100 as integer) / 100;
    K21 = cast(K21 * 100 as integer) / 100;
    K22 = cast(K22 * 100 as integer) / 100;
    K23 = cast(K23 * 100 as integer) / 100;
    K24 = cast(K24 * 100 as integer) / 100;
    K25 = cast(K25 * 100 as integer) / 100;
    K26 = cast(K26 * 100 as integer) / 100;
    K27 = cast(K27 * 100 as integer) / 100;
    K28 = cast(K28 * 100 as integer) / 100;
    K29 = cast(K29 * 100 as integer) / 100;
    K31 = cast(K31 * 100 as integer) / 100;
    K34 = cast(K34 * 100 as integer) / 100;
    K35 = cast(K35 * 100 as integer) / 100;
  end else
  begin
    K10 = round(K10);
    K11 = round(K11);
    K12 = round(K12);
    K13 = round(K13);
    K14 = round(K14);
    K15 = round(K15);
    if (vat_nalezny = '1') then
    begin
      K16 = round(0.03 * K15_3) + round(0.05 * K15_5);
      K18 = round(0.07 * K17_7) + round(0.08 * K17_8);
      K20 = round(0.22 * K19_22) + round(0.23 * K19_23);
    end else
    begin
      K16 = round(K16);
      K18 = round(K18);
      K20 = round(K20);
    end
    K17 = round(K17);
    K19 = round(K19);
    K21 = round(K21);
    K22 = round(K22);
    K23 = round(K23);
    K24 = round(K24);
    K25 = round(K25);
    K26 = round(K26);
    K27 = round(K27);
    K28 = round(K28);
    K29 = round(K29);
    K30 = round(K30);
    K31 = round(K31);
    K32 = round(K32);
    K34 = round(K34);
    K35 = round(K35);
  end

  -- zeby liczyl odpowiednio
  if (K10 is null) then K10 = 0;
  if (K11 is null) then K11 = 0;
  if (K12 is null) then K12 = 0;
  if (K13 is null) then K13 = 0;
  if (K14 is null) then K14 = 0;
  if (K15 is null) then K15 = 0;
  if (K16 is null) then K16 = 0;
  if (K17 is null) then K17 = 0;
  if (K18 is null) then K18 = 0;
  if (K19 is null) then K19 = 0;
  if (K20 is null) then K20 = 0;
  if (K21 is null) then K21 = 0;
  if (K22 is null) then K22 = 0;
  if (K23 is null) then K23 = 0;
  if (K24 is null) then K24 = 0;
  if (K25 is null) then K25 = 0;
  if (K26 is null) then K26 = 0;
  if (K27 is null) then K27 = 0;
  if (K28 is null) then K28 = 0;
  if (K29 is null) then K29 = 0;
  if (K30 is null) then K30 = 0;
  if (K31 is null) then K31 = 0;
  if (K32 is null) then K32 = 0;
  if (K33 is null) then K33 = 0;
  if (K34 is null) then K34 = 0;
  if (K35 is null) then K35 = 0;
  if (K36 is null) then K36 = 0;
  if (K37 is null) then K37 = 0;
  if (K38 is null) then K38 = 0;
  if (K39 is null) then K39 = 0;
  if (K40 is null) then K40 = 0;

  K38 = K10 + K11 + K13 + K15 + K17 + K19 + K21 + K22 + K23
    + K25 + K27 + K29 + K31 + K32 + K34;
  K39 = K16 + K18 + K20 + K24 + K25 + K26 + K28 + K30 + K33 + K35 + K36 - K37;
  -- zaKupy z tego miesiaca
  for
    select  bKvatpos.taxgr, sum(bKvatpos.netv), sum(bKvatpos.vatv), vatregs.vatregtype, grvat.vatregtype --<<PL:PR73894>>
      from bKdocs
        join vatregs on (vatregs.symbol = bKdocs.vatreg)
        join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref and bKvatpos.debited = 1)
        join grvat on (grvat.symbol = bkvatpos.taxgr)--<<PL:PR73894>>
      where bKdocs.company = :company and bKvatpos.taxgr<>'P45'
        and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
        and vatregs.vtype > 2 and bKdocs.status > 0 and bKdocs.company= vatregs.company
        and (vatregs.vatregtype = 1 or vatregs.vatregtype = 2 or (vatregs.vatregtype = 3 and coalesce(grvat.vatregtype,0) <> 0))--<<PL:PR73894>>
      group by bKvatpos.taxgr,  vatregs.vatregtype, grvat.vatregtype  --<<PL:PR73894>>
      into :taxgr, :netv, :vatv, vregtype, vgrtype
  do begin
    if (taxgr in ('I11','I21','INW')) then
    begin
      if (vat_naliczony = '1') then
        if (vatv = 0) then netv = 0;
      K41 = K41 + netv;
      K42 = K42 + vatv;
    --end else if (taxgr in ('P11','P21','POZ','C36','P39','C42(11)','C39(11)')) then
    end else if (vregtype = 1 or vregtype = 2 or vregtype = 3 and coalesce(vgrtype,0) <> 0) then
    begin
      if (vat_naliczony <> '' and vat_naliczony = '1') then
        if (vatv = 0) then netv = 0;
      K43 = K43 + netv;
      K44 = K44 + vatv;
    end
    else begin
      select first 1 bKdocs.symbol, bKdocs.bKreg, bKdocs.number
        from bKdocs
          join vatregs on (vatregs.symbol = bKdocs.vatreg)
          join bKvatpos on (bKvatpos.bKdoc = bKdocs.ref)
          where bKdocs.company = :company and (bKvatpos.taxgr = :taxgr or bKvatpos.taxgr is null)
          and vatregs.vtype > 2 and bKdocs.status > 0 and bKdocs.company= vatregs.company
          and bKdocs.vatperiod >= :odokresu and bKdocs.vatperiod <= :dookresu
          and bKvatpos.debited = 1
        into :wrongdoc, :bKreg, :bKdocnumber;

--      exception universal 'Wystąpiła grupa podatKowa nieuwzględniona w deKlaracji!';
      exception universal 'Błędna gr. podatK. na doKumen. ' || :wrongdoc || ' ('|| :bKreg ||','|| :bKdocnumber ||')';
      -- exception fK_vat7_unKnown_taxgr;
    end
  end

 -- zsumowanie calego do odliczenia
  if (K42 is null) then
    K42 = 0;
  if (K43 is null) then
    K43 = 0;
  if (odokresu <= '200512') then
  begin       
    K37 = cast(K37 * 100 as integer) / 100;
    K41 = cast(K41 * 100 as integer) / 100;
    K42 = cast(K42 * 100 as integer) / 100;
    K43 = cast(K43 * 100 as integer) / 100;
    K44 = cast(K44 * 100 as integer) / 100;
  end else
  begin
    K37 = round(K37);
    K38 = round(K38);
    K41 = round(K41);
    K42 = round(K42);
    K43 = round(K43);
    K44 = round(K44);
  end

  K45 = (K42) * ((WSP35 - 100) / 100); --wyliczenie KoreKt
  K46 = (K44) * ((WSP35 - 100) / 100);

  if (odokresu <= '200512') then
  begin
    K45 = cast(K45 * 100 as integer) / 100;
    K46 = cast(K46 * 100 as integer) / 100;
  end else
  begin
    K45 = round(K45);
    K46 = round(K46);
  end

  K48 = K40 + K42 + K44 + K45 + K46 + K47; -- caly odliczony

  if (K39 > K48) then
    K51 = K39 - K48 - K49 - K50;
  else
    K51 = 0;

  if (K48 > K39) then
    K53 = K48 - K39 + K52;
  else
    K53 = 0;

  K54 = K55 + K56 + K57;
  K58 = K53 - K54;

  if (K10 = 0) then K10 = null;
  if (K11 = 0) then K11 = null;
  if (K12 = 0) then K12 = null;
  if (K13 = 0) then K13 = null;
  if (K14 = 0) then K14 = null;
  if (K15 = 0) then K15 = null;
  if (K16 = 0) then K16 = null;
  if (K17 = 0) then K17 = null;
  if (K18 = 0) then K18 = null;
  if (K19 = 0) then K19 = null;
  if (K20 = 0) then K20 = null;
  if (K21 = 0) then K21 = null;
  if (K22 = 0) then K22 = null;
  if (K23 = 0) then K23 = null;
  if (K24 = 0) then K24 = null;
  if (K25 = 0) then K25 = null;
  if (K26 = 0) then K26 = null;
  if (K27 = 0) then K27 = null;
  if (K28 = 0) then K28 = null;
  if (K29 = 0) then K29 = null;
  if (K30 = 0) then K30 = null;
  if (K31 = 0) then K31 = null;
  if (K32 = 0) then K32 = null;
  if (K33 = 0) then K33 = null;
  if (K34 = 0) then K34 = null;
  if (K35 = 0) then K35 = null;
  if (K36 = 0) then K36 = null;
  if (K37 = 0) then K37 = null;
  if (K38 = 0) then K38 = null;
  if (K39 = 0) then K39 = null;
  if (K40 = 0) then K40 = null;
  if (K41 = 0) then K41 = null;
  if (K42 = 0) then K42 = null;
  if (K43 = 0) then K43 = null;
  if (K44 = 0) then K44 = null;
  if (K45 = 0) then K45 = null;
  if (K46 = 0) then K46 = null;
  if (K47 = 0) then K47 = null;
  if (K48 = 0) then K48 = null;
  if (K49 = 0) then K49 = null;
  if (K50 = 0) then K50 = null;
  if (K51 = 0) then K51 = null;
  if (K52 = 0) then K52 = null;
  if (K53 = 0) then K53 = null;
  if (K54 = 0) then K54 = null;
  if (K55 = 0) then K55 = null;
  if (K56 = 0) then K56 = null;
  if (K57 = 0) then K57 = null;
  if (K58 = 0) then K58 = null;
  suspend;
end^
SET TERM ; ^
