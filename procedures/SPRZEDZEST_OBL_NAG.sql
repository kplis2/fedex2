--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SPRZEDZEST_OBL_NAG(
      ZESTAWIENIE integer)
   as
declare variable wartnet numeric(14,2);
declare variable wartbru numeric(14,2);
declare variable prowizja numeric(14,2);
begin
  select sum(wartnet), sum(wartbru), sum(dowyplaty) from sprzedzestp
     where ZESTAWIENIE=:zestawienie into :wartnet, :wartbru, :prowizja;
  update sprzedzest set SUMWARTNET=:wartnet, SUMWARTBRU=:wartbru, SUMPROWIZJI=:prowizja
    where ref = :zestawienie;
end^
SET TERM ; ^
