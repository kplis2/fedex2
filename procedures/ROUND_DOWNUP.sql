--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ROUND_DOWNUP(
      LICZBA numeric(14,4),
      PODZ numeric(14,4),
      UP integer)
  returns (
      ZAOKRAGLENIE integer,
      RESZTA integer,
      LICZINT integer)
   as
declare variable licz numeric(14,2);
declare variable mnoz integer;
begin
  mnoz = 1;
  if (up is null) then up = 0;
  if (up <> 0) then mnoz = 0;
  if (liczba is null) then liczba = 0;
  if (podz <> 0 and podz is not null and up <> 2)
  then begin
    liczint = :liczba/:podz;
    if(:liczint * :podz < liczba) then
      liczint = :liczint + :mnoz*1;
    if(:liczint * :podz > liczba) then
      liczint = :liczint - :mnoz*1;
    zaokraglenie = :liczint * :podz;
    reszta = :liczba - zaokraglenie;
  end
  if (podz <> 0 and podz is not null and up = 2)
  then begin
    liczint = :liczba/:podz;
    if(:liczint * :podz < liczba) then
      liczint = :liczint + 1;
    zaokraglenie = :liczint * :podz;
    reszta = :liczba - zaokraglenie;
  end
  suspend;
end^
SET TERM ; ^
