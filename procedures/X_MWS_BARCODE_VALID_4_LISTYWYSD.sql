--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_BARCODE_VALID_4_LISTYWYSD(
      BARCODE STRING100,
      DOK INTEGER_ID = 0)
  returns (
      STATUS SMALLINT_ID,
      LISTYWYSDPOZ MWSACTS_ID,
      VERS WERSJE_ID)
   as
declare variable CNT INTEGER_ID;
begin

  if (position(';',BARCODE)>0) then
    BARCODE=substring(BARCODE from 1 for position(';',BARCODE)-1);

  select  count(distinct X_listywysdpoz) , min(X_listywysdpoz), min(VERS)
  from  X_MWS_BARCODELIST(:barcode, 0, 0, :dok)
  into :cnt, :listywysdpoz, :VERS;

  if (cnt > 1) then
  begin
    status = 2;
    suspend;
    exit;
  end
  else

  if (:listywysdpoz is not null) then status = 1;
  else status = 0;
  suspend;
end^
SET TERM ; ^
