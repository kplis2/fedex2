--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE UPDATE_SOCIALFUND_VALUE(
      EMPLOYEEREF integer,
      ECOLUMN integer,
      FDATE timestamp,
      SETVALUE numeric(14,2))
   as
declare variable soc integer;
begin
    select ref
      from esocialfunds
      where  employee = :employeeref and fundtype = :ecolumn and funddate = :fdate
      into :soc;

    if (coalesce(soc,0) = 0 and setvalue <> 0) then
    begin
      insert into esocialfunds (employee, fundtype, amount, funddate)
        values (:employeeref, :ecolumn, :setvalue, :fdate);
    end
      else
    if (setvalue <> 0) then
      update esocialfunds set amount = :setvalue, funddate = :fdate
        where employee = :employeeref and fundtype = :ecolumn;
    else if (setvalue = 0) then
     delete from esocialfunds where employee = :employeeref and fundtype = :ecolumn and funddate = :fdate;
end^
SET TERM ; ^
