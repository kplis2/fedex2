--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMPLOYMENT_RECHANGE_ANNEXEREFS(
      FROMDATE date,
      EMPLCONTRACT integer,
      OLDANNEXE integer,
      NEWANNEXE integer)
   as
declare variable WORKPOST integer;
declare variable WORKDIM float;
declare variable DIMNUM smallint;
declare variable DIMDEN smallint;
declare variable BRANCH varchar(10);
declare variable DEPARTMENT varchar(10);
declare variable SALARY numeric(14,2);
declare variable PAYMENTTYPE smallint;
declare variable CADDSALARY numeric(14,2);
declare variable FUNCSALARY numeric(14,2);
declare variable DICTDEF integer;
declare variable DICTPOS integer;
declare variable BKSYMBOL bksymbol_id;
declare variable LOCAL varchar(80);
declare variable PROPORTIONAL smallint;
declare variable EMPLGROUP integer;
declare variable WORKCAT integer;
declare variable WORKSYSTEM integer;
declare variable DAYLIMIT integer;
declare variable WEEKLIMIT integer;
declare variable ACCPERIODLIMIT integer;
declare variable DIMNUMLIMIT smallint;
declare variable DIMDENLIMIT smallint;
declare variable ISDIMLIMIT smallint;
declare variable WORKDIMLIMIT float;
declare variable EPAYRULE integer;
begin
/*MWr(PR24342) Procedura ma za zadanie uaktualnic pozycje w przebiegu zatrudnienia,
  oznaczone aneksem OLDANNEXE, wartosciami aneksu NEWANNEXE. Jezeli NEWANNEXE nie zostal
  okreslony wartosci zostana bezposrednio pobrane z umowy EMPLCONTRACT (ktora jest
  rowniez nadrzednym dokumentem dla aneksu OLDANNEXE, o ile OLDANNEXE zostal podany).
  Aktualizacja odbywa sie od zadanej daty FROMDATE i nie dotczy dat na przebiegu*/

  if (emplcontract is not null) then
  begin
    if (newannexe is null) then
      select workpost, workdim, dimnum, dimden, branch, department, salary,
         paymenttype, caddsalary, funcsalary, dictdef, dictpos, bksymbol,
         local, proportional, emplgroup, workcat, worksystem, epayrule
       from emplcontracts
       where ref = :emplcontract
       into: workpost, :workdim, :dimnum, :dimden, :branch, :department, :salary,
         :paymenttype, :caddsalary,  :funcsalary, :dictdef, :dictpos, :bksymbol,
         :local, :proportional, :emplgroup, :workcat, :worksystem, :epayrule;
   else
      select workpost, workdim, dimnum, dimden, branch, department, salary,
         paymenttype, caddsalary, funcsalary, dictdef, dictpos, bksymbol,
         local, proportional, emplgroup, workcat, worksystem, epayrule
       from econtrannexes
       where ref = :newannexe
       into: workpost, :workdim, :dimnum, :dimden, :branch, :department, :salary,
         :paymenttype, :caddsalary,  :funcsalary, :dictdef, :dictpos, :bksymbol,
         :local, :proportional, :emplgroup, :workcat, :worksystem, :epayrule;
    update employment
      set workpost = :workpost, workdim = :workdim, dimnum = :dimnum, dimden = :dimden,
        branch = :branch, department = :department, salary = :salary, paymenttype = :paymenttype,
        caddsalary = :caddsalary, funcsalary = :funcsalary, annexe = :newannexe,
        dictdef = :dictdef, dictpos = :dictpos, bksymbol = :bksymbol, local = :local,
        proportional = :proportional, emplgroup = :emplgroup, workcat = :workcat,
        worksystem = :worksystem, epayrule = :epayrule
      where fromdate >= :fromdate
        and coalesce(annexe,0) = coalesce(:oldannexe,0)
        and emplcontract = :emplcontract;
  end
end^
SET TERM ; ^
