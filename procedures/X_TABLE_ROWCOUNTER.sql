--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_TABLE_ROWCOUNTER
  returns (
      NAME STRING100,
      ROWCOUNTER INTEGER_ID)
   as
  declare variable sql string1024;
begin
  --mkd
  --procedura zwraca liste tabel z iloscia wierszy
  for
  select rdb$relation_name
    from rdb$relations
    where rdb$view_blr is null
      and (rdb$system_flag is null or rdb$system_flag = 0)
  into :name
  do begin
    if (:name is not null) then
    begin
      sql = 'select count(1) from '||:name;
      execute statement sql into rowcounter;
      suspend;
    end
    rowcounter = 0;
    name = null;
  end

end^
SET TERM ; ^
