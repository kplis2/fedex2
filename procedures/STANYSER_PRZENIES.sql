--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STANYSER_PRZENIES(
      ILOSC numeric(14,4),
      MAGAZYN varchar(10) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      DOSTAWA integer,
      OLDCEN numeric(14,4),
      NEWCEN numeric(14,4))
   as
declare variable oldref integer;
declare variable newref integer;
declare variable serialnr type of serial_id;
declare variable iloscser numeric(14,4);
declare variable data timestamp;
declare variable zablokow numeric(14,4);
declare variable oldzablokow numeric(14,4);
declare variable newzablokow numeric(14,4);
begin
  -- zidentyfikuj stary i nowy stan cenowy
  oldref = null;
  select ref from stanycen where magazyn=:magazyn and ktm=:ktm and wersja=:wersja and cena=:oldcen and dostawa = :dostawa into :oldref;
  newref = null;
  select ref from stanycen where magazyn=:magazyn and ktm=:ktm and wersja=:wersja and cena=:newcen and dostawa = :dostawa into :newref;
  if(:oldref is null or :newref is null) then exit;
  -- przejdz po wszytkich numerach seryjnych starego stanu
  for select serialnr,ilosc,data,zablokow
    from stanyser
    where stancen=:oldref
  into :serialnr,:iloscser,:data,:zablokow
  do begin
    if(:iloscser<=:ilosc) then begin
      -- caly rekord do przepiecia
      update stanyser set stancen=:newref where stancen=:oldref and serialnr=:serialnr;
      ilosc = :ilosc - :iloscser;
    end else begin
      -- oblicz przeniesienie blokady
      if(:zablokow>:iloscser-:ilosc) then begin
        oldzablokow = :iloscser - :ilosc;
        newzablokow = :zablokow - :oldzablokow;
      end else begin
        oldzablokow = :zablokow;
        newzablokow = 0;
      end
      -- zmniejsz ilosc w biezacym rekordzie
      update stanyser set ilosc = :iloscser - :ilosc, zablokow = :oldzablokow
        where stancen=:oldref and serialnr=:serialnr;
      -- dopisz nowy rekord na reszte w nowym stanie cenowym
      insert into stanyser(stancen, serialnr,ilosc,data,zablokow)
      values (:newref,:serialnr,:ilosc,:data,:newzablokow);
      ilosc = 0;
    end
    if(:ilosc=0) then exit;
  end

end^
SET TERM ; ^
