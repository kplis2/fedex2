--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ANAL_MWS_ADWORKS(
      WH varchar(3) CHARACTER SET UTF8                           ,
      BDATE date,
      EDATE date,
      WORKS varchar(255) CHARACTER SET UTF8                           )
  returns (
      NUMBER integer,
      OPERATOR integer,
      OPERATORDICT varchar(80) CHARACTER SET UTF8                           ,
      WORKTYPE varchar(255) CHARACTER SET UTF8                           ,
      TIMESTART timestamp,
      TIMESTOP timestamp,
      WORKTIME varchar(80) CHARACTER SET UTF8                           ,
      SUMWORK varchar(80) CHARACTER SET UTF8                           )
   as
declare variable sumtime double precision;
declare variable oldoperator integer;
begin
  sumtime = 0;
  oldoperator = 0;
  number = 0;
  if (works = '' or works = ';') then works = null;
  for
    select op.ref, op.nazwa, p.kod, o.timestart, o.timestop
      from mwsords o
        left join slopoz p on (p.slownik = o.slodef and o.slopoz = p.ref)
        left join operator op on (op.ref = o.operator)
      where o.mwsordtypedest = 13 and o.status = 5 and o.slodef is not null
        and o.slopoz is not null and p.ref is not null
        and ((o.timestop <= :edate and o.timestop >= :bdate)
          or (o.timestart <= :edate and o.timestart >= :bdate))
        and (o.wh = :wh or coalesce(:wh,'') = '')
        and (strmulticmp(';'||p.ref||';',:works)>0 or :works is null)
      order by o.operator, o.timestop
      into operator, operatordict, worktype, timestart, timestop
  do begin
    if (oldoperator = 0) then
      oldoperator = operator;
    worktime = '';
    if (timestart < bdate) then timestart = bdate;
    if (timestop > edate) then timestop = edate;
    sumtime = sumtime + (timestop - timestart);
    select difference from TIMESTAMP_DIFF_TOSTRING(current_timestamp(0), current_timestamp(0) + :sumtime)
      into sumwork;
    select difference from TIMESTAMP_DIFF_TOSTRING(:timestart, :timestop)
      into worktime;
    number = number + 1;
    suspend;
    if (oldoperator <> operator) then
    begin
      oldoperator = operator;
      number = number + 1;
      operator = null;
      worktype = 'Suma';
      timestart = null;
      timestop = null;
      worktime = null;
      suspend;
      sumtime = 0;
    end
  end
  oldoperator = operator;
  number = number + 1;
  worktype = 'Suma';
  timestart = null;
  timestop = null;
  worktime = null;
  suspend;
end^
SET TERM ; ^
