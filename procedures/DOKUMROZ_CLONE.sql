--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMROZ_CLONE(
      ROZREF integer,
      ILOSC numeric(14,4))
  returns (
      NEWREF integer)
   as
declare variable PILOSC numeric(14,4);
declare variable PILOSCL numeric(14,4);
declare variable PCENA numeric(14,4);
declare variable NUMER integer;
declare variable POZYCJA integer;
declare variable CENREF integer;
declare variable CENLOCAL smallint;
declare variable CENILOSC numeric(14,4);
declare variable CENATOROZ integer;
declare variable KORREF integer;
declare variable KORILOSC numeric(14,4);
declare variable KORLOCAL smallint;
declare variable KORROZ integer;
declare variable CNT integer;
declare variable OLDILOSCL numeric(14,4); /* ILOSCL do pozostawienia na rozpisce biezacej */
declare variable NEWILOSCL numeric(14,4); /* ILOSCL do wpisania na nowa rozpiske */
declare variable TMPILOSCL numeric(14,4);
declare variable ILOSCDOZOST numeric(14,4);
declare variable DOKUMNOTPREF integer;
declare variable CENANEW numeric(14,4);
declare variable CENAOLD numeric(14,4);
declare variable OLDILOSC numeric(14,4);
declare variable WARTOSCDOZOST numeric(14,2);
declare variable NEWILOSC numeric(14,4);
declare variable NEWWARTOSC numeric(14,2);
declare variable ROZILOSC numeric(14,4);
begin
  -- parametr ILOSC, to ilosc do pozostawienia na rozpisce biezacej (rozref)
  cenref = null;
  newref = null;
  select CENATOROZ, POZYCJA, ILOSC, ILOSCL from DOKUMROZ where ref=:rozref into :cenref,:pozycja,:pilosc, :piloscl;
  if(:ilosc<:piloscl) then oldiloscl = :ilosc;
  else oldiloscl = :piloscl;
  -- OLDILOSCL to ILOSCL do pozostawienia na rozpisce biezacej
  newiloscl = :piloscl - :oldiloscl;
  -- NEWILOSCL to ILOSCL do przeniesienia na nowa rozpiske
  -- BS59351 - MS: poprawiam warunek aby poprawnie przepinal KORTOROZ
  if(exists(select pozycja from dokumroz where kortoroz = :rozref)) then begin
    -- klonowanie rozpisek zaleznych
    execute procedure GEN_REF('DOKUMROZ') returning_values :newref;
    tmpiloscl = :pilosc - :ilosc - :newiloscl;
    -- TMPILOSCL to ilosc na rozpiskach zaleznych do przepiecia pod nowa rozpiske
    -- zmniejszamy w petli i przepinamy rozpiski zalezne
    for select REF, ILOSC
    from DOKUMROZ
    where kortoroz = :ROZREF
    into :korref, :korilosc
    do begin
      if(:tmpiloscl = 0) then break; -- jesli przepielismy calosc to nie przegladamy dalej rozpisek zaleznych
      if(:tmpiloscl>=:korilosc) then begin -- cala rozpiske przepinamy pod newref
        update dokumroz set KORTOROZ = :newref where ref=:korref;
        tmpiloscl = :tmpiloscl - :korilosc;
      end else begin -- tylko czesc rozpiski przepinamy pod newref
        execute procedure DOKUMROZ_CLONE(:korref, :korilosc - :tmpiloscl) returning_values :korroz;
        update dokumroz set KORTOROZ = :newref where ref=:korroz;
        tmpiloscl = 0;
      end
    end
  end


  cenatoroz = :cenref;
  /* czy rozpiska ma rozpiski zalezne, np MM-/MM+ lub RW/PW */
  if(:cenref > 0) then begin
    /* czy zaleznosc jest 1:1 czy 1:N. */
    select count(*) from DOKUMROZ where CENATOROZ=:cenref into :cnt;
    if(:cnt=1) then begin
      cenilosc = null;
      select ILOSC from DOKUMROZ where ref=:cenref into :cenilosc;
      /*  Jesli 1:1 i ilosci sie zgadzaja to klonujemy rozpiske zalezna aby miec dokladnie te same ceny po korekcie*/
      /* w przeciwnym razie nie klonujemy i pozwalamy na taka korekte ceny na rozpisce zaleznej aby zachowac taka sama wartosc*/
      if(:pilosc = :cenilosc) then begin
        execute procedure check_local('DOKUMROZ',:cenref) returning_values :cenlocal;
        if(:cenlocal > 0) then begin
           execute procedure DOKUMROZ_CLONE(:cenref, :ilosc) returning_values :cenatoroz;
        end
      end
    end
  end
  if(exists(select pozycja from dokumroz where ref = :rozref and (opkrozid > 0 ))) then begin
    exception DOKUMROZ_CLONE_EXPT 'Próba rozbicia rozpiski, do której są okreslone stany opakowań';
  end
  if(:pilosc > :ilosc) then begin -- klonujemy biezaca rozpiske
    ilosc = :pilosc - :ilosc; -- przeliczamy ILOSC jako ilosc do zalozenia na nowej rozpisce (newref)
    if(:newref is null) then
      execute procedure GEN_REF('DOKUMROZ') returning_values :newref;
    select max(NUMER) from DOKUMROZ where POZYCJA=:pozycja into :numer;
    numer = numer + 1;
    select PCENA from DOKUMROZ where REF=:rozref into :pcena;
    insert into DOKUMROZ(REF, POZYCJA, NUMER, ILOSC, CENA, DOSTAWA, SERIALNR,
      ACK, BLOKOBLNAG, CENASNETTO, CENASBRUTTO,CENATOROZ,CENATOPOZ)
    select :newref, POZYCJA, :NUMER, :ilosc, CENA, DOSTAWA, SERIALNR,
      7, 1, CENASNETTO, CENASBRUTTO,:cenatoroz,CENATOPOZ
      from DOKUMROZ where ref=:rozref;
    update DOKUMROZ set PCENA = :pcena, PWARTOSC = :pcena*ILOSC, ILOSCL = :newiloscl where REF=:newref;
    select ILOSC from DOKUMROZ where ref=:rozref into :rozilosc;
    update DOKUMROZ set ILOSC = :rozilosc - :ilosc, ILOSCL = :oldiloscl,
      PWARTOSC = (:rozilosc - :ilosc)*PCENA,
      ACK = 7, BLOKOBLNAG = 1 where ref=:rozref;
    update DOKUMROZ set BLOKOBLNAG = 0, ACK = 1 where REF in (:rozref, :newref);
    select ILOSC from DOKUMROZ where REF=:rozref into :iloscdozost;

    for select p.ref,p.cenanew,p.cenaold,p.ilosc
    from DOKUMNOTP P
    join DOKUMNOT D on (D.REF=P.dokument)
    where p.dokumrozkor=:rozref and d.akcept=1 and p.akceptpoz=1
    into :dokumnotpref,:cenanew,:cenaold,:oldilosc
    do begin
      wartoscdozost = cast(:cenanew*:iloscdozost as numeric(14,2)) - cast(:cenaold*:iloscdozost as numeric(14,2));
      newilosc = :oldilosc - :iloscdozost;
      newwartosc = cast(:cenanew*:newilosc as numeric(14,2)) - cast(:cenaold*:newilosc as numeric(14,2));
      if(:newilosc>0) then begin

        update DOKUMNOTP set ILOSC=:iloscdozost, WARTOSC=:wartoscdozost where REF=:dokumnotpref;
        insert into DOKUMNOTP(DOKUMENT,DOKUMROZKOR,CENAOLD,CENANEW,KTM,WERSJAREF,FROMDOKUMNOTP,STANMAGCHANGED,WARTOSC,ILOSC,PMPOSITION,DOSTAWA,DOKUMNAGDATE,CLONEDFROM,AKCEPTPOZ)
        select DOKUMENT, :newref, CENAOLD, CENANEW, KTM, WERSJAREF,FROMDOKUMNOTP,0,:newwartosc,:newilosc,PMPOSITION,DOSTAWA,DOKUMNAGDATE,:dokumnotpref,AKCEPTPOZ
        from DOKUMNOTP where REF=:dokumnotpref;
      end
    end
  end
end^
SET TERM ; ^
