--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDE_DIVIDE(
      PRSCHEDOPERS integer,
      AMOUNT numeric(14,4))
   as
declare variable schedzam INTEGER ;
declare variable schedpozzam INTEGER ;
declare variable guide INTEGER ;
declare variable orgnumber INTEGER ;
declare variable newguide INTEGER ;
declare variable tmpschedopers INTEGER ;
declare variable tmpschedopers1 INTEGER ;
declare variable number INTEGER ;
declare variable tmpnumber INTEGER ;
declare variable status INTEGER ;
declare variable reportedfactor numeric(14,4) ;
declare variable newamount numeric(14,4) ;
declare variable tmpamount numeric(14,4) ;
declare variable sumamount numeric(14,4) ;
declare variable propersrap INTEGER ;
declare variable maketimefrom timestamp;
declare variable maketimeto timestamp;
declare variable newtime double precision;
declare variable onetime double precision;
declare variable maketimefrom_to timestamp;
declare variable activ INTEGER ;
declare variable reported INTEGER ;
declare variable prschedguidesclone INTEGER ;
declare variable symbol VARCHAR(40) ;
begin
  select po.schedzam, po.guide, po.number, g.pozzam
    from prschedopers po
      left join prschedguides g on (g.ref = po.guide)
    where po.ref = :prschedopers
    into :schedzam, :guide, :orgnumber, :schedpozzam;

  --zmiana ilosći na wejsciu na starym przewodniku
  update prschedguidespos set amountzreal = (amount - :amount) * kamountshortage * kamountnorm
    where prschedguide = :guide;
  update prschedguides p set p.amount = p.amount - :amount
    where ref = :guide;

  --generowanie nowego przewodnika
  execute procedure prsched_add_zamguide(:schedzam, :schedpozzam, :amount);

  --pobranie refa nowego przewodnika
  select first 1 p.ref
    from prschedguides p
    where p.prschedzam = :schedzam
      and p.status = 0
    order by p.numer desc
    into :newguide;

  select max(p.prschedguidesclone)
    from prschedguides p
    where ref = :guide
    into :prschedguidesclone;

  select p.symbol
    from prschedguides p
    where p.ref = :guide
    into :symbol;

  if (coalesce(:prschedguidesclone,0) = 0) then
    prschedguidesclone = 1;
  else
    prschedguidesclone = :prschedguidesclone + 1;

  update prschedguidespos set amountzreal = :amount * kamountshortage * kamountnorm
    where prschedguide = :newguide;
  update prschedguides p set p.prschedguideorg = :guide, p.symbol = :symbol||'-'||:prschedguidesclone
    where p.ref = :newguide;

  tmpschedopers = :prschedopers;

  while (1=1)do
  begin
  --szukam ostatniej zamknitej operacji
    select first 1 ps.ref, ps.number, ps.status
      from PRSCHEDOPERDEPS p
        left join prschedopers ps on (ps.ref = p.depfrom)
      where p.depto = :tmpschedopers
      into :tmpschedopers1, :number, :status;
  
    if (:status = 3) then
      break;
    else if (:number <= 1) then
      tmpschedopers = :tmpschedopers1;
    else begin
      number = 0;
      break;
    end
  end

  --otwarcie nowego przewodnika
  update prschedguides p set p.status = 1 where ref = :newguide;

  --otwarcie operacji
  tmpschedopers = null;
  if (:number > 0) then
  begin
    while (1=1) do
    begin
      select first 1 p.ref
        from prschedopers p
        where p.guide = :guide
          and p.number <= :number
          and p.reported > 0
          and p.activ > 0
          and p.status = 3
        order by p.guide, p.number desc
        into :tmpschedopers;

      if (:tmpschedopers is null) then
        break;
      update prschedopers p set p.status = 2 where p.ref = :tmpschedopers;
      tmpschedopers = null;
    end 
  end 

  --ustawienie aktywnoci i raportowania na nowym tak jak jest na starym
  for 
    select p.number, p.activ, p.reported, p.status
      from prschedopers p
      where p.guide = :guide
      order by p.guide, p.number
      into :tmpnumber, :activ, :reported, :status
  do
  begin
    update prschedopers p set p.activ = :activ
      where p.guide = :newguide and p.number = :tmpnumber;
    update prschedopers p set p.reported = :reported
      where p.guide = :newguide and p.number = :tmpnumber;
    if (:status < 3) then
      update prschedopers p set p.status = :status
        where p.guide = :newguide and p.number = :tmpnumber;
  end

  --ustawienie iloci na wejsciu na 1 operacji nowego przewodnika
  update prschedopers p set p.amountin = :amount * coalesce(p.reportedfactor,1)
    where ref = (select first 1 p.ref from prschedopers p  where p.guide = :newguide
      and p.activ > 0 and p.reported > 0 order by p.number );

  --przepisywanie raportów
  for
    select p.ref,  p.number, coalesce(p.reportedfactor,1)
      from prschedopers p
      where p.guide = :guide  
        and p.number <= :orgnumber
        and p.activ = 1
        and p.reported > 0
      order by p.guide, p.number
      into :tmpschedopers, :tmpnumber, :reportedfactor
  do
  begin
    select p.ref
      from prschedopers p
      where p.guide = :newguide 
        and p.number = :tmpnumber
      into :tmpschedopers1;

    sumamount = 0;
    propersrap = null;
    newamount = :amount * :reportedfactor;

    --ustawienie ilosci na wejsciu
    select p1.amountresult
      from prschedoperdeps pd
        left join prschedopers p1  on (pd.depfrom = p1.ref)
      where pd.depto = :tmpschedopers1
        and p1.activ > 0 and p1.reported > 0
      into :tmpamount;

    if (coalesce(:tmpamount,0) = 0 ) then
      tmpamount = :newamount;

    update prschedopers p set p.amountin = :tmpamount
      where ref = :tmpschedopers1;

    select p.ref
      from propersraps p
      where p.prschedoper = :tmpschedopers
        and p.amount = :newamount
      into :propersrap;

    if (:propersrap is not null) then
      --istnieje raport na potrzebna ilosc
      update propersraps p set p.prschedoper = :tmpschedopers1 where ref = :propersrap;
    else begin
      for
        select p.ref, p.amount, p.maketimefrom, p.maketimeto
          from propersraps p
            left join econtractsdef e on (e.ref = p.econtractsdef)
          where p.prschedoper = :tmpschedopers
            and e.prtype = 1
          order by p.prschedoper, p.amount
          into :propersrap, :tmpamount, :maketimefrom, :maketimeto
      do
      begin
        if (:tmpamount  + :sumamount <= :newamount) then
        begin
          --przepisanie raportu
          update propersraps p set p.prschedoper = :tmpschedopers1 where ref = :propersrap;
          sumamount = sumamount + tmpamount;

          if (:sumamount >= :newamount) then  
            break;
        end else begin
          --podzielenie raportu
          newtime = :maketimeto - :maketimefrom;--obliczenie czasu produkcji
--          newtime = newtime *24 * 60 * 60; --zamiana na sekundy
          onetime = CAST ((newtime / :tmpamount) as double precision);-- / (60/60/24);
          maketimefrom_to = :maketimefrom + (((:tmpamount - (:newamount -  :sumamount)) * onetime));
          --zmniejszenie ilosci i srocenie czasu na potrzeby norm
          update propersraps p set p.amount = (:tmpamount - (:newamount -  :sumamount)), p.maketimeto = :maketimefrom_to
            where ref = :propersrap;

          --zalozenie nowego raportu
          insert into propersraps (person, employee, amount, econtractsdef, econtractsposdef, prschedoper,
                maketime, setuptime, regtime, status, maketimefrom, maketimeto,
                operator)
            select p.person, p.employee, (:newamount -  :sumamount), p.econtractsdef, p.econtractsposdef, :tmpschedopers1,
                p.maketime, p.setuptime, p.regtime, p.status, :maketimefrom_to, p.maketimeto,
                p.operator
              from propersraps p
              where p.ref = :propersrap;
          break;
        end 
      end
    end
    -- zamykanie operacji
    if (:tmpnumber < :number) then
      update propersraps p set p.status = 3 where ref = :tmpschedopers or ref = :tmpschedopers1;
  end
end^
SET TERM ; ^
