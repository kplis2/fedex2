--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE UPDATE_EMPLOYMENT_BD_ANNEXE(
      OLD_EMPLCONTRACT integer,
      OLD_FROMDATE timestamp,
      OLD_TODATE timestamp,
      OLD_ATYPE smallint,
      OLD_REF integer)
   as
declare variable PREV_EMPLOYMENT integer;
declare variable NEXT_EMPLOYMENT integer;
declare variable NEXT_TODATE date;
declare variable FROMDATE date;
declare variable TODATE date;
declare variable EMPLOYMENT integer;
declare variable BMREF integer;
declare variable IS_ODDELEG_PREV_EMPLOYMENT smallint = 0;
declare variable IS_INSERT_FROM_CONTR smallint = 0;
declare variable BANNEXE integer;
begin
/*MWr(PR24342) Przeniesienie zmian na przebieg zatrudnienia przed usunieciem aneksu/oddelegowania.
  Nie mozemy pobrac informacji o pozostalych parametrach wejsciowych przy uzyciu OLD_REF gdyz
  otrzymamy, przy wywolaniu z triggera after_update, nowe, a nie stare (potrzebne tu) dane */

  select first 1 m.ref, case when a.atype = 2 then 1 else 0 end
    from employment m
      left join econtrannexes a on (a.ref = m.annexe)
    where m.emplcontract = :old_emplcontract and m.fromdate < :old_fromdate
    order by m.fromdate desc
    into :prev_employment, :is_oddeleg_prev_employment;

  select ref, fromdate, todate from employment
    where emplcontract = :old_emplcontract and fromdate = :old_fromdate
    into :employment, :fromdate, :todate;

  select first 1 m.ref, m.annexe from employment m
    left join econtrannexes a on (a.ref = m.annexe)
    where m.emplcontract = :old_emplcontract and m.fromdate < :old_fromdate
      and (a.atype is null or a.atype <> 2)
    order by m.fromdate desc
    into :bmref, :bannexe;

/*Usuniecie rekordu moze wymagac update tych wartosci w przebiegu zatrud.,
  ktore pobieraja dane wlasnie z usuwanego rekordu. Nie dotyczy to oddelegowania*/
  if (old_atype = 1) then
    if (exists(select first 1 1 from employment
                 where coalesce(annexe,0) = :old_ref and fromdate > :old_fromdate)
    ) then
      execute procedure employment_rechange_annexerefs(:old_fromdate + 1, :old_emplcontract, :old_ref, :bannexe);

--usuwamy w przebiegu rekord, ktory bezposrednio odpowiada biezacej pozycji
  delete from employment where ref = :employment;

  if (is_oddeleg_prev_employment = 1) then
  begin
  /*usuwana pozycja zaczyna sie nastepnego dnia po zakonczeniu jakiegos oddelegowania,
    wiec usuwane miejsce musimy wypelnic tym co bylo wczesniej*/

    if (bmref is not null) then
    --informacje o tym co bylo wczesniej wezniemy z przebiegu zatrudnienia
      insert into employment (ref, employee, fromdate, todate, emplcontract, workpost, workdim,
          dimnum, dimden, branch, department, salary, paymenttype, caddsalary, funcsalary, annexe,
          dictdef, dictpos, bksymbol, local, proportional, emplgroup, workcat, worksystem, epayrule)
        select :employment, employee, :old_fromdate, :todate, emplcontract, workpost, workdim,
          dimnum, dimden, branch, department, salary, paymenttype, caddsalary, funcsalary, annexe,
          dictdef, dictpos, bksymbol, local, proportional, emplgroup, workcat, worksystem, epayrule
        from employment
        where ref = :bmref;
    else 
    --informacje o tym co bylo wczesniej wezniemy bezposrednio z umowy
      is_insert_from_contr = 1;
  end

  if (old_atype = 2) then
  begin
  /*kiedy usuwamy oddelegowanie musimy sprawdzic, czy w przebiegu zatrud. nastepny
    rekord nie stanowi wypelnienia miedzy oddelegowaniem, a innym oddeleg./aneksem.
    W/w rekord nie ma bezposredniego powiazania przez date fromdate z tabela umow czy aneksow*/

    select ref, todate
      from employment
      where emplcontract = :old_emplcontract and fromdate = :old_todate + 1
      into :next_employment, :next_todate;

    if(next_employment is not null and
      not(exists(select first 1 m.ref from employment m
                   left join econtrannexes a on (a.ref = m.annexe)
                   left join emplcontracts c on (c.ref = :old_emplcontract)
                   where m.ref = :next_employment
                     and ((m.fromdate = a.fromdate and a.ref is not null)
                       or (m.fromdate = c.fromdate and c.ref is not null))))
    ) then begin
      if (is_oddeleg_prev_employment = 1) then
        update employment set todate = :next_todate where ref = :employment;
      else
        update employment set todate = :next_todate where ref = :prev_employment;

      if (prev_employment is not null) then
        delete from employment where ref = :next_employment;
      else
      /*pierwszym aneksem jest aneks czasowy o tym samym fromdate co umowa,
        a w nastepnym rekordzie mamy wartosci umowy wyjsciowej*/
        update employment set fromdate = :fromdate where ref = :next_employment;
    end else if (is_oddeleg_prev_employment = 0) then begin
    --ponizsze ma zastosowanie, gdy usuwamy oddelegowanie od najstarszych pozycji
      if (prev_employment is not null) then
        update employment set todate = :todate where ref = :prev_employment;
      else
        is_insert_from_contr = 1;
    end
  end

  if (is_insert_from_contr = 1) then
  /*wykonujemy, kiedy pierwszym aneksem jest aneks czasowy o tym samym fromdate co umowa; usuwamy rekord,
    ktory jest za nim, a wiec musimy uzupelnic przebieg zatrudnienia wartosciami z umowy wyjsciowej*/
    insert into employment (ref, employee, fromdate, todate, emplcontract, workpost, workdim,
        dimnum, dimden, branch, department, salary, paymenttype, caddsalary, funcsalary, annexe,
        dictdef, dictpos, bksymbol, local, proportional, emplgroup, workcat, worksystem, epayrule
    ) select :employment, employee, :fromdate, :todate, ref, workpost, workdim, dimnum, dimden,
        branch, department, salary, paymenttype, caddsalary, funcsalary, null, dictdef,
        dictpos, bksymbol, local, proportional, emplgroup, workcat, worksystem, epayrule
      from emplcontracts
      where ref = :old_emplcontract;

  if (is_oddeleg_prev_employment = 0 and old_atype = 1) then
    update employment set todate = :todate where ref = :prev_employment;
end^
SET TERM ; ^
