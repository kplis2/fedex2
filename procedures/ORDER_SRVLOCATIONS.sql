--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ORDER_SRVLOCATIONS(
      SRVREQUEST integer,
      OLDN integer,
      NEWN integer)
   as
DECLARE VARIABLE REF INTEGER;
DECLARE VARIABLE NUMBER INTEGER;
begin
  --firts we find minor number
  if (oldn < newn) then
    newn = oldn;

  number = newn;
  for
    select ref
      from SRVLOCATIONS
      where srvrequest = :srvrequest and number >= :number
      order by number, ord
      into :ref
  do begin
    update SRVLOCATIONS set number = :number, ord = 2
      where ref = :ref;
    number = number + 1;
  end

  update SRVLOCATIONS set ord = 1
    where srvrequest = :srvrequest and number >= :newn;
end^
SET TERM ; ^
