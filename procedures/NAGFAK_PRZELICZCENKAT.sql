--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_PRZELICZCENKAT(
      REFFAK integer)
   as
DECLARE VARIABLE REFPOZ INTEGER;
declare variable koszt numeric(14,2);
DECLARE VARIABLE ILOSC NUMERIC(14,4);
DECLARE VARIABLE CENAZAK NUMERIC(14,4);
begin
  for select POZFAK.REF, POZFAK.iloscm - pozfak.piloscm, WERSJE.cena_zakn
    from pozfak
    left join WERSJE on (POZFAK.WERSJAREF = WERSJE.REF)
    where POZFAK.dokument = :reffak
    into :refpoz,:ilosc,:cenazak
  do begin
    if(:cenazak is null) then cenazak = 0;
    if(:ilosc is null) then ilosc = 0;
    koszt = :cenazak * :ilosc;
    update POZFAK set KOSZTKAT = :koszt where REF=:refpoz and (KOSZTKAT <> :koszt or KOSZTKAT is null);
  end
end^
SET TERM ; ^
