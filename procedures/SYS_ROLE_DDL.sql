--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_ROLE_DDL(
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      CREATE_OR_ALTER smallint)
  returns (
      DDL blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           )
   as
declare variable ROLENAME varchar(31);
declare variable OWNERNAME varchar(31);
declare variable FLAG smallint;
declare variable EOL varchar(2);
begin
  eol ='
    ';
  ddl = '';
  for select r.rdb$role_name, r.rdb$owner_name, r.rdb$system_flag
  from rdb$roles r
  where r.rdb$role_name = :nazwa
    and r.rdb$role_name NOT STARTING WITH 'RDB$'
  into :rolename, :ownername, :flag
  do begin
   if(ddl<>'') then
        ddl = ddl || eol;
   ddl = ddl || 'update or insert into rdb$roles (RDB$ROLE_NAME, RDB$OWNER_NAME, RDB$DESCRIPTION, RDB$SYSTEM_FLAG)
   values ('''||TRIM(:rolename)||''', '''||TRIM(:ownername)||''', '||'null'||', '||:flag||') matching(RDB$ROLE_NAME);';
  end
  suspend;
end^
SET TERM ; ^
