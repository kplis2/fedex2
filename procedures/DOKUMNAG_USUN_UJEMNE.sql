--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_USUN_UJEMNE(
      DOKMAGREF integer)
  returns (
      STATUS integer)
   as
declare variable magazyn varchar(3);
declare variable wersja integer;
declare variable ktm varchar(40);
declare variable scena numeric(14,4);
declare variable sdostawa integer;
declare variable silosc numeric(14,4);
declare variable dokilosc numeric(14,4);
declare variable korilosc numeric(14,4);
declare variable dokcena numeric(14,4);
declare variable dokdostawa integer;
declare variable dokrozref integer;
declare variable dokrozilosc numeric(14,4);
declare variable dokroznewref integer;
declare variable magtyp varchar(3);
declare variable mag2 varchar(3);
declare variable grupa integer;
declare variable notref integer;
declare variable data timestamp;
declare variable sdata timestamp;
declare variable scena_koszt numeric(14,4);
declare variable datadok timestamp;
declare variable magdata date;
begin
  grupa = null;
  status = 0;
  select data,magazyn from dokumnag where ref=:dokmagref into :data,:magazyn;
  datadok = data;
  -- przejdź po wszystkich rozpiskach nowo akceptowanego dokumentu przychodowego
  for select dokumroz.ktm, dokumroz.wersja, dokumroz.ilosc, dokumroz.cena, dokumroz.dostawa
    from dokumroz
    join dokumpoz on (dokumroz.pozycja = dokumpoz.ref)
    left join towary on (towary.ktm = dokumroz.ktm)
    where dokumpoz.dokument = :dokmagref and towary.usluga<>1
    into :ktm, :wersja, :dokilosc, :dokcena, :dokdostawa
  do begin
    -- przejdź po ujemnych stanach cenowych możliwych do wyrównania bieżącą rozpiską przychodową
    -- a wiec stanach na ten sam magazyn i ktm, ale innych cenach lub dostawach no i z ujemna iloscia
    for select stanycen.data, stanycen.cena, stanycen.dostawa, -stanycen.ilosc, stanycen.cena_koszt
      from stanycen
      where stanycen.magazyn=:magazyn and stanycen.ktm=:ktm and stanycen.wersja=:wersja and stanycen.ilosc<0
      and ((stanycen.dostawa<>:dokdostawa) or (stanycen.cena<>:dokcena))
      order by stanycen.datafifo
      into :sdata, :scena, :sdostawa, :silosc, :scena_koszt
    do begin
      -- ilosc do korygowania to min z ilosci z rozpiski przychodowej i ujemnego stanu
      if(:dokilosc>:silosc) then korilosc = :silosc;
      else korilosc = :dokilosc;
      -- przejdź po dokumentach rozchodowych możliwych do wyrównania
      for select dokumroz.ref, dokumroz.ilosc, dokumnag.typ, dokumnag.mag2, dokumnag.data
        from dokumroz
        left join dokumpoz on (dokumroz.pozycja = dokumpoz.ref)
        left join dokumnag on (dokumpoz.dokument = dokumnag.ref)
        where dokumroz.magazyn=:magazyn and dokumroz.ktm=:ktm and dokumroz.wersja=:wersja
        and dokumroz.cena=:scena and dokumroz.dostawa=:sdostawa and dokumroz.wydania=1 and dokumnag.akcept=1
        order by dokumroz.ref desc
        into :dokrozref, :dokrozilosc, :magtyp, :mag2, :magdata
      do begin
        -- jesli ilosc na rozpisce rozchodowej wieksza niz ilosc mozliwa do wyrownania to sklonuj rozpiske
        if(:korilosc<:dokrozilosc) then begin
          execute procedure DOKUMROZ_CLONE(:dokrozref,:korilosc) returning_values :dokroznewref;
          dokrozilosc = :korilosc;
        end
        if(:dokrozilosc<=0) then break;
        -- podmien dostawe na dokumencie rozchodowynm na dostawe z biezacego dokumentu przychodowego
        if(:sdostawa<>:dokdostawa ) then begin
          update DOKUMROZ set DOSTAWA = :dokdostawa, blokoblnag = 1 where ref=:dokrozref;
          update DOKUMROZ set BLOKOBLNAG = 0 where ref=:dokrozref;
          status = 1;
        end
        -- wygeneruj note aby skorygowac cene dokumentu rozchodowego na cene z dokumentu przychodowego
        if(:scena<>:dokcena) then begin
          if(:mag2 is null) then mag2 = '';
          if(:grupa is null) then execute procedure GEN_REF('DOKUMNOT') returning_values :grupa;
          notref = NULL;
          select REF from DOKUMNOT where DOKUMNOT.grupa = :grupa and DOKUMNOT.TYP = :magtyp and DOKUMNOT.MAGAZYN = :magazyn and DOKUMNOT.mag2 = :mag2 and DOKUMNOT.akcept = 0
          into :notref;
          if(:notref is null) then begin
            execute procedure gen_ref('DOKUMNOT') returning_values :notref;
            insert into DOKUMNOT(REF,MAGAZYN, TYP, MAG2, DATA, GRUPA)
              values (:notref, :magazyn, :magtyp, :mag2, :data, :grupa);
          end
          insert into DOKUMNOTP(DOKUMENT, DOKUMROZKOR,CENANEW, FROMDOKUMNOTP)
            values (:notref, :dokrozref, :dokcena, NULL);
          -- skoryguj recznie stany cenowe bo nota rozchodowa czegos takiego nie robi
/*          update STANYCEN set ILOSC = ILOSC + :dokrozilosc
            where magazyn=:magazyn and ktm=:ktm and wersja=:wersja and cena=:scena and dostawa=:dokdostawa;
          update STANYCEN set ILOSC = ILOSC - :dokrozilosc
            where magazyn=:magazyn and ktm=:ktm and wersja=:wersja and cena=:dokcena and dostawa=:dokdostawa;*/
          status = 1;
        end
        -- recznie dokonaj zmian na STANYCEN bo ani zmiana dostawy na rozpisce ani nota rozchodowa tego nie robi
        if(:sdostawa<>:dokdostawa or :scena<>:dokcena) then begin
          -- najpierw przychodowuje stare potem rozchoduje z nowych
          execute procedure PLUS_SC(:magazyn,:ktm,:wersja,:dokrozilosc,:scena,:sdostawa,'',:sdata,:scena_koszt,NULL,:magdata);
          execute procedure MINUS_SC(:magazyn,:ktm,:wersja,:dokrozilosc,:dokcena,:dokdostawa,'',:sdata,:scena_koszt,NULL,:magdata);
          execute procedure STANYARCH_CHANGE('N',:magazyn,:ktm,:wersja,null,:sdata,0,:dokrozilosc * (:dokcena - :scena), :dokdostawa,:magdata);
        end

        korilosc = :korilosc-:dokrozilosc;
        dokilosc = :dokilosc-:dokrozilosc;
        if(:korilosc<=0) then break;
      end
      if(:dokilosc<=0) then break;
    end
  end
  -- akceptacja zalozonej noty lub not
  if(:grupa is not null) then begin
     update DOKUMNOT set dokumnot.akcept = 1 where grupa = :grupa;
  end
end^
SET TERM ; ^
