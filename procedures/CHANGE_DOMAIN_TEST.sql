--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHANGE_DOMAIN_TEST(
      TABLENAME varchar(31) CHARACTER SET UTF8                           ,
      FIELDNAME varchar(31) CHARACTER SET UTF8                           ,
      NEWDOMAIN varchar(31) CHARACTER SET UTF8                           )
  returns (
      IS_OK smallint)
   as
declare variable newtype integer;
declare variable oldtype integer;
declare variable field blob sub_type 0 segment size 80;
declare variable query varchar(255);
declare variable flength integer;
begin
  is_ok = 1;

  /*
  is_ok = 0 - Bląd
  is_ok = 1 - Konwersja na ten sam typ sam typ
  is_ok = 2 - Konwersja z typu tekstowego na typ liczbowy
  is_ok = 3 - Konwersja na typ tesktowy
  is_ok = 4 - Konwersja na typ liczbowy
  is_ok = 5 - Poprawna inna konwersja
  */

  /**
   *   3 - char
   *   7 - smallint
   *   8 - integer
   *  10 - float
   *  12 - date
   *  13 - time
   *  16 - numeric
   *  27 - double precision
   *  35 - timestamp
   *  37 - varchar
   * 261 - blob
   */

  select f.rdb$field_type, f.rdb$field_length
    from rdb$fields f
    where f.rdb$field_name = :newdomain
    into :newtype, :flength;

  select f.rdb$field_type
    from rdb$relation_fields r join rdb$fields f
    on rdb$field_source = f.rdb$field_name
    where rdb$relation_name = :tablename
    and r.rdb$field_name = :fieldname
    into :oldtype;

  query = 'select '||fieldname||' from '||tablename;

  if(newtype = oldtype) then --zmiana dlugosci
    is_ok = 1;
  else if((oldtype in (3, 37)) and (newtype in (7,8,10,16,27))) then  --typ tekstowy -> typ liczbowy
    is_ok = 2;
  else if (newtype = 3) then -- char
    begin
      is_ok  = 1;
      for execute statement query into :field
      do begin
        if (coalesce(char_length(field),0) > flength) then exception universal; -- [DG] XXX ZG119346
      end
    end
  else if (newtype = 7) then --smallint
    is_ok = 4;
  else if (newtype = 8) then --integer
    is_ok = 4;
  else if (newtype = 10) then --float
    is_ok = 4;
  else if (newtype = 12) then --date
    is_ok = 3;
  else if (newtype = 13) then --time
    is_ok = 3;
  else if (newtype = 16) then --numeric
    is_ok = 4;
  else if (newtype = 27) then --double precision
    is_ok = 4;
  else if (newtype = 35) then --timestamp
    is_ok = 3;
  else if (newtype = 37) then --varchar
    is_ok = 3;
  else if (newtype = 261) then --blob
    is_ok = 5;
  else is_ok = 0;
  suspend;

  --jesli nie udala sie konwersja
  when any do
  begin
    is_ok = 0;
    suspend;
  end

end^
SET TERM ; ^
