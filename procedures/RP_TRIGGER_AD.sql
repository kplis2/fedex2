--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RP_TRIGGER_AD(
      TABLENAME varchar(31) CHARACTER SET UTF8                           ,
      OLD_KEY1 varchar(40) CHARACTER SET UTF8                           ,
      OLD_KEY2 varchar(40) CHARACTER SET UTF8                           ,
      OLD_KEY3 varchar(40) CHARACTER SET UTF8                           ,
      OLD_KEY4 varchar(120) CHARACTER SET UTF8                           ,
      OLD_KEY5 varchar(40) CHARACTER SET UTF8                           ,
      OLD_TOKEN integer,
      OLD_STATE integer)
   as
declare variable newtoken integer;
begin
  execute procedure RP_CHECK_TOKEN(tablename, old_token)
    returning_values :newtoken;
  execute procedure rp_desync_multistate(tablename, old_key1, old_key2, old_key3, old_key4, old_key5, old_STATE);
end^
SET TERM ; ^
