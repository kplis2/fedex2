--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRDCELLS_AUTOADD(
      FRHDR varchar(20) CHARACTER SET UTF8                           )
   as
declare variable frdhdrver integer;
declare variable frdhdr integer;
declare variable frdpesdimval integer;
declare variable timelem integer;
declare variable defelem integer;
declare variable elemhier varchar(80);
declare variable persdimhier integer;
begin
  for -- przejdz po wszystkich sprawozdaniach wg wybranej definicji
    select frdhdrs.ref
      from frdhdrs
      where frdhdrs.frhdr = :frhdr
      into :frdhdr
  do begin
    for -- przejdz po strukturze hierarhii wybranego sprawozdania
      select frdtree_structure.persdimhierout, frdtree_structure.elemhierout, frdtree_structure.elemref
        from frdtree_structure(:frdhdr,-1,0,null,null,null,null,null,null,0,1)
        where frdtree_structure.elemref is not null
        into :persdimhier, :elemhier ,:defelem
    do begin
      for -- przejdz po wersjach danego sprawozdania
        select frdhdrsvers.ref
          from frdhdrsvers
          where frdhdrsvers.frdhdr = :frdhdr
          into :frdhdrver
      do begin
        for -- przejdz po wierszach i kolumnach
          select  frdpersdimvals.ref,timeiems.dimelem
            from frdpersdimhier
              join frdperspects on (frdperspects.ref = frdpersdimhier.frdperspect)
              join frdimhier timehier on (timehier.ref = frdperspects.timefrdimhier)
              join frdimelems timeiems on (timehier.ref = timeiems.frdimhier)
              join frdpersdimvals on (frdpersdimvals.frdpersdimshier = frdpersdimhier.ref)
          where
            frdpersdimhier.ref = :persdimhier and timehier.istime = 1
          into :frdpesdimval, :timelem
        do begin -- zaloz komorke jesli jej nie ma
          if (not exists (select ref from frdcells
              where frdcells.frdhdrver = :frdhdrver and frdcells.frdpersdimval = :frdpesdimval
              and frdcells.timelem = :timelem and frdcells.dimelem = :defelem
              and frdcells.elemhier = :elemhier)) then
            insert into frdcells (frdhdrver, frdpersdimval, timelem, dimelem, elemhier)
               values (:frdhdrver, :frdpesdimval, :timelem, :defelem, :elemhier);
        end
      end
    end
  end
  execute procedure frdcells_set_company(:frhdr);
end^
SET TERM ; ^
