--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WF_FAKTZAK_GETDEPARTMENTROLE(
      DEPARTMENT DEPARTMENT_ID)
  returns (
      ROLEUUID NEOS_ID)
   as
begin
  select first 1 uuid from
    (select first 1 distinct o.uuid
       from departments d
         join operator o on d.superior=o.employee
       where d.symbol = :department
     union
     select first 1 distinct o.uuid
       from departments d
         join operator o on d.superiordeputy=o.employee
       where d.symbol = :department)
  into :roleuuid;
  if (roleuuid is null) then
    select uuid from operator where ref = 21
      into :roleuuid;
  suspend;
end^
SET TERM ; ^
