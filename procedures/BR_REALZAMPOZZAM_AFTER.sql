--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_REALZAMPOZZAM_AFTER(
      REJZAM varchar(3) CHARACTER SET UTF8                           ,
      DOKUMENT integer,
      TYP char(1) CHARACTER SET UTF8                           )
  returns (
      REF integer,
      ZAMOWIENIE integer,
      SYMBOL varchar(30) CHARACTER SET UTF8                           ,
      DATA timestamp,
      DATADOST timestamp,
      TYPP smallint,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      WERSJAREF integer,
      ILZAM numeric(14,4),
      ILZREAL numeric(14,4),
      ILDONE numeric(14,4),
      ILOSC numeric(14,4),
      ILOSCDOK numeric(14,4),
      DOSTAWA integer,
      NAZWAWER varchar(255) CHARACTER SET UTF8                           ,
      NAZWAT varchar(255) CHARACTER SET UTF8                           ,
      SYMBOL_DOST varchar(255) CHARACTER SET UTF8                           ,
      RMAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      DOSTAWA_SYMBOL varchar(60) CHARACTER SET UTF8                           ,
      CENA numeric(14,4),
      CENACEN numeric(14,4),
      WALCEN varchar(3) CHARACTER SET UTF8                           ,
      RABAT numeric(14,2))
   as
begin
  if(:typ = 'M' and coalesce(REJZAM,'') <> '') then
  begin
    for
      select p.REF, n.REF, n.ID, n.DATAWE, n.TERMDOST,n.TYP, p.KTM, p.WERSJA, p.WERSJAREF,
          p.ILOSC, p.ILZREAL,p.ILOSC - p.ILZREAL - p.ILZDYSP + p.ILZADYSP,p.DOSTAWAMAG,p.MAGAZYN, p.CENANETZL,
          p.CENACEN, p.WALCEN, p.RABAT, dp.ilosc
        from DOKUMNAG DN
          left JOIN DOKUMPOZ DP ON (DP.DOKUMENT = DN.REF)
          left JOIN NAGZAM  n on (((dn.klient is not null and (dn.klient = n.klient or dn.klient = n.fakturant))
              or (dn.dostawa is not null and dn.dostawa = n.dostawa))
            and n.typ < 2 and n.rejestr = :REJZAM)
          left join POZZAM p on (dp.wersjaref = p.wersjaref and p.zamowienie = n.ref)
        where dn.ref = :dokument
          and dp.fromzam is null
          and dp.frompozzam is null
          and p.ref is not null
       into :ref,:zamowienie,:symbol,:data,:datadost,:typp,:ktm,:wersja,:wersjaref,
        :ilzam, :ilzreal,:ilosc,:dostawa,:rmagazyn, :cena,
        :cenacen, :walcen, :rabat, :ILOSCDOK
    do begin
      if(:ilosc > 0) then
      begin
        select sum(ILOSC)
          from DOKUMPOZ
          where DOKUMENT = :DOKUMENT
            and FROMZAM = :zamowienie
            and FROMPOZZAM = :ref
          into :ildone;
        if(:ildone is null) then ildone = 0;
        if(:ildone > 0) then ilosc = :ilosc - :ildone;
        if(:ilosc>0) then
        begin
          select WERSJE.nazwa, NAZWAT, SYMBOL_DOST
            from WERSJE
            where ref = :wersjaref
            into :nazwawer, :nazwat, :symbol_dost;
          select max(symbol)
            from dostawy
            where dostawy.ref = :dostawa
            into :dostawa_symbol;
          suspend;
        end
      end
    end
  end
end^
SET TERM ; ^
