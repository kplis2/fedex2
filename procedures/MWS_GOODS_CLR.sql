--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_GOODS_CLR(
      GROUPID integer,
      CLRDIFF smallint)
   as
declare variable creffrom integer;
declare variable goodfrom varchar(40);
declare variable versfrom integer;
declare variable docidfrom integer;
declare variable quantityfrom numeric(14,4);
declare variable atr1from varchar(255);
declare variable atr2from varchar(255);
declare variable quantityfromclr numeric(14,4);
declare variable crefto integer;
declare variable goodto varchar(40);
declare variable versto integer;
declare variable docidto integer;
declare variable quantityto numeric(14,4);
declare variable atr1to varchar(255);
declare variable atr2to varchar(255);
declare variable quantitytoclr numeric(14,4);
declare variable regoper integer;
declare variable regdate timestamp;
begin
  quantityfrom = 0;
  for
    select ref, docidfrom, goodfrom, versfrom, atr1from, atr2from, quantityfrom, regoper, regdate
      from mwssupclr
      where groupid = :groupid and quantityfrom > 0 and docidto is null
      order by ref
      into creffrom, docidfrom, goodfrom, versfrom, atr1from, atr2from, quantityfrom, regoper, regdate
  do begin
    for
      select ref, docidfrom, goodfrom, versfrom, atr1from, atr2from, quantityto
      from mwssupclr
      where groupid = :groupid and quantityto > 0 and docidto is null
        and :quantityfrom > 0
      order by ref
      into crefto, docidto, goodto, versto, atr1to, atr2to, quantityto
    do begin
      if (quantityfrom >= quantityto and quantityto > 0 and quantityfrom > 0) then
      begin
        insert into mwssupclr (docidfrom, goodfrom, versfrom, quantityfrom,
            docidto, goodto, versto, quantityto,
            regoper, regdate, groupid,
            atr1from, atr2from, atr1to, atr2to)
          values (:docidfrom, :goodfrom, :versfrom, :quantityto,
              :docidto, :goodto, :versto, :quantityto,
              :regoper, :regdate, :groupid,
              :atr1from, :atr2from, :atr1to, :atr2to);
        update mwssupclr set quantityfrom = quantityfrom - :quantityto where ref = :creffrom;
        delete from mwssupclr where ref = :crefto;
        quantityfrom = quantityfrom - quantityto;
        if (quantityfrom <= 0) then
          delete from mwssupclr where ref = :creffrom;
      end
      else if (quantityfrom < quantityto and quantityto > 0 and quantityfrom > 0) then
      begin
        insert into mwssupclr (docidfrom, goodfrom, versfrom, quantityfrom,
            docidto, goodto, versto, quantityto,
            regoper, regdate, groupid,
            atr1from, atr2from, atr1to, atr2to)
          values (:docidfrom, :goodfrom, :versfrom, :quantityfrom,
              :docidto, :goodto, :versto, :quantityfrom,
              :regoper, :regdate, :groupid,
              :atr1from, :atr2from, :atr1to, :atr2to);
        update mwssupclr set quantityto = quantityto - :quantityfrom where ref = :crefto;
        delete from mwssupclr where ref = :creffrom;
        quantityfrom = 0;
      end
      if (quantityfrom <= 0) then
        break;
    end
  end
  if (clrdiff > 0) then
  begin
    update mwssupclr set quantityto = quantityfrom, docidto = 0
      where groupid = :groupid and quantityfrom > 0 and docidto is null;
    update mwssupclr set quantityfrom = 0, goodto = goodfrom, versto = versfrom,
        atr1to = atr1from, atr2to = atr2from, docidto = docidfrom
      where groupid = :groupid and quantityto > 0 and docidto is null;
    update mwssupclr set goodfrom = '', versfrom = 0, docidfrom = 0, atr1from = '', atr2from = ''
      where groupid = :groupid and quantityto > 0 and quantityfrom = 0 and docidto > 0;
  end
  else
  begin
    delete from mwssupclr where groupid = :groupid and quantityfrom > 0 and docidto is null;
    delete from mwssupclr where groupid = :groupid and quantityto > 0 and docidto is null;
  end
end^
SET TERM ; ^
