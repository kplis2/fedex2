--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRDTREE_STRUCTURE(
      FRDHDR integer,
      FRDTREEDEEP smallint,
      DEEP smallint,
      SORTTYPE smallint,
      PERSDIMHIERIN integer,
      FRDPERSPECTIN integer,
      REFIN integer,
      ELEMHIERIN varchar(255) CHARACTER SET UTF8                           ,
      ELEMDESCRIN varchar(1024) CHARACTER SET UTF8                           ,
      ELEMREFIN integer = 0,
      ALLROWS integer = 0)
  returns (
      FRDHDROUT integer,
      ELEMREF integer,
      NAME varchar(80) CHARACTER SET UTF8                           ,
      REFOUT integer,
      REFUPOUT integer,
      FRDPERSPECTOUT integer,
      PERSDIMHIEROUT integer,
      PIKTOGRAM integer,
      FRDPERSNAME varchar(20) CHARACTER SET UTF8                           ,
      ELEMHIEROUT varchar(255) CHARACTER SET UTF8                           ,
      ELEMDESCROUT varchar(1024) CHARACTER SET UTF8                           ,
      SHOWVAL1 smallint,
      VAL1NAME varchar(20) CHARACTER SET UTF8                           ,
      SHOWVAL2 smallint,
      VAL2NAME varchar(20) CHARACTER SET UTF8                           ,
      SHOWVAL3 smallint,
      VAL3NAME varchar(20) CHARACTER SET UTF8                           ,
      SHOWVAL4 smallint,
      VAL4NAME varchar(20) CHARACTER SET UTF8                           ,
      SHOWVAL5 smallint,
      VAL5NAME varchar(20) CHARACTER SET UTF8                           ,
      HASCHILDREN smallint)
   as
declare variable frhdr varchar(20);
declare variable elemhiertmp varchar(255);
declare variable elemdescrtmp varchar(1024);
declare variable operator integer;
declare variable opergrupy varchar(1024);
declare variable elemreftmp int;
declare variable num integer;
declare variable maxnum integer;
begin
  execute procedure get_global_param('AKTUOPERATOR') returning_values :operator;
  select grupa from operator where ref = :operator into :opergrupy;
  if (:sorttype is null) then sorttype = 0;
  if (:frdtreedeep is null) then frdtreedeep = -1;
  elemhiertmp = '';
  ELEMDESCRTMP = '';
  if (:frdtreedeep = -1) then  --pierwsze wywolanie (korzen)
  begin
    showval1 = null;
    val1name = null;
    showval2 = null;
    val2name = null;
    showval3 = null;
    val3name = null;
    showval4 = null;
    val4name = null;
    showval5 = null;
    val5name = null;
    refin = 0;
    refout = 0;
    FRDHDROUT = :FRDHDR;
    select max(frdpersdimhier.hierorder), max(frhdrs.symbol)
      from frdpersdimhier
        join frdperspects on (frdperspects.ref = frdpersdimhier.frdperspect)
        join frhdrs on (frdperspects.frhdr = frhdrs.symbol)
        join frdhdrs on (frhdrs.symbol = frdhdrs.frhdr)
      where frdhdrs.ref = :frdhdr
      into :deep, :frhdr;
    select max(frdperspects.shortname), max(frdpersdimhier.ref), MAX(frdperspects.REF),
        max(frdpersdimhier.showval1), max(frdpersdimhier.val1name), max(frdpersdimhier.showval2), max(frdpersdimhier.val2name),
        max(frdpersdimhier.showval3), max(frdpersdimhier.val3name), max(frdpersdimhier.showval4), max(frdpersdimhier.val4name),
        max(frdpersdimhier.showval5), max(frdpersdimhier.val5name)
      from frdperspects
        join frdpersdimhier on (frdperspects.ref = frdpersdimhier.frdperspect)
      where frdperspects.frhdr = :frhdr and frdperspects.mainperspect = 1
      into :name, :PERSDIMHIEROUT, :FRDPERSPECTOUT,
        :showval1, :val1name, :showval2, :val2name,
        :showval3, :val3name, :showval4, :val4name,
        :showval5, :val5name;
    select max(dimelemdef.ref), max(dimelemdef.name),
        max(frdpersdimhier.showval1), max(frdpersdimhier.val1name), max(frdpersdimhier.showval2), max(frdpersdimhier.val2name),
        max(frdpersdimhier.showval3), max(frdpersdimhier.val3name), max(frdpersdimhier.showval4), max(frdpersdimhier.val4name),
        max(frdpersdimhier.showval5), max(frdpersdimhier.val5name)
      from frdpersdimhier
        join frdimhier on (frdimhier.ref = frdpersdimhier.frdimshier)
        join frdimelems on (frdimelems.frdimhier = frdimhier.ref)
        join dimelemdef on (frdimelems.dimelem = dimelemdef.ref)
      where frdpersdimhier.frdperspect = :FRDPERSPECTOUT
        and ((FRDIMELEMS.READRIGHTS = ';' and FRDIMELEMS.READRIGHTSGROUP = ';') or (FRDIMELEMS.READRIGHTS like '%%;'||:operator||';%%') OR (strmulticmp(';'||:opergrupy||';',FRDIMELEMS.READRIGHTSGROUP)=1))
      into :elemref, :ELEMDESCROUT,
        :showval1, :val1name, :showval2, :val2name,
        :showval3, :val3name, :showval4, :val4name,
        :showval5, :val5name;
    if (:elemref is null) then piktogram = 0;
    else piktogram = 1;
    elemreftmp = elemref;
    ELEMHIEROUT = cast(:elemref as varchar(255));
    ELEMDESCRTMP = ELEMDESCROUT;
    elemhiertmp = ELEMHIEROUT;
    FRDHDROUT = :FRDHDR;
    haschildren = 1;
    suspend;
    showval1 = null;
    val1name = null;
    showval2 = null;
    val2name = null;
    showval3 = null;
    val3name = null;
    showval4 = null;
    val4name = null;
    showval5 = null;
    val5name = null;
    for
      select frdimhier.shortname, frdperspects.shortname, frdpersdimhier.ref, frdperspects.REF,
          frdpersdimhier.showval1, frdpersdimhier.val1name, frdpersdimhier.showval2, frdpersdimhier.val2name,
          frdpersdimhier.showval3, frdpersdimhier.val3name, frdpersdimhier.showval4, frdpersdimhier.val4name,
          frdpersdimhier.showval5, frdpersdimhier.val5name
        from frdperspects
          join frdpersdimhier on (frdpersdimhier.frdperspect = frdperspects.ref)
          join frdimhier on (frdpersdimhier.frdimshier = frdimhier.ref)
        where frdperspects.frhdr = :frhdr and frdperspects.mainperspect = 0
          and frdpersdimhier.hierorder = :frdtreedeep + 1
        into :name, :frdpersname, :PERSDIMHIEROUT, :FRDPERSPECTOUT,
          :showval1, :val1name, :showval2, :val2name,
          :showval3, :val3name, :showval4, :val4name,
          :showval5, :val5name
    do begin
      refout = :refout + 1;
      refupout = :refin;
      elemref = null;
      if (:elemref is null) then
      begin
        piktogram = 0;
      end else
      begin
        piktogram = 1;
      end
      ELEMHIEROUT = :elemhiertmp;
      ELEMDESCROUT = :ELEMDESCRTMP;
      FRDHDROUT = :FRDHDR;
      haschildren = 1;
      suspend;
      showval1 = null;
      val1name = null;
      showval2 = null;
      val2name = null;
      showval3 = null;
      val3name = null;
      showval4 = null;
      val4name = null;
      showval5 = null;
      val5name = null;
      for
        select FRDHDROUT,elemref,name,frdperspectout,refout,refupout,persdimhierout, ELEMHIEROUT, ELEMDESCROUT,
             showval1, val1name, showval2, val2name, showval3, val3name, showval4, val4name, showval5, val5name, haschildren
          from FRDTREE_STRUCTURE(:frdhdr,:frdtreedeep + 1,:deep,:sorttype,:PERSDIMHIEROUT,:FRDPERSPECTOUT,:refout, :ELEMHIEROUT, :ELEMDESCROUT, :elemreftmp, :allrows)
          into :FRDHDROUT, :elemref, :name, :frdperspectout, :refout, :refupout, :persdimhierout, :ELEMHIEROUT, :ELEMDESCROUT,
            :showval1, :val1name, :showval2, :val2name, :showval3, :val3name, :showval4, :val4name, :showval5, :val5name, :haschildren
      do begin
        if (:elemref is null) then piktogram = 0;
        else piktogram = 1;
        FRDHDROUT = :FRDHDR;
        suspend;
      end
    end
  end else
  begin -- kolejne wywolania (rekurencyjnie)
    if (:frdtreedeep <= :deep) then
    begin
      refout = :refin;
      showval1 = null;
      val1name = null;
      showval2 = null;
      val2name = null;
      showval3 = null;
      val3name = null;
      showval4 = null;
      val4name = null;
      showval5 = null;
      val5name = null;
      num = 0;
      for
        select dimelemdef.ref, dimelemdef.name, dimelemdef.name, frdpersdimhier.frdperspect, frdpersdimhier.ref,
            frdpersdimhier.showval1, frdpersdimhier.val1name, frdpersdimhier.showval2, frdpersdimhier.val2name,
            frdpersdimhier.showval3, frdpersdimhier.val3name, frdpersdimhier.showval4, frdpersdimhier.val4name,
            frdpersdimhier.showval5, frdpersdimhier.val5name, frdpersdimhier.maxnum
          from frdperspects
            join frhdrs on (frdperspects.frhdr = frhdrs.symbol)
            join frdhdrs on (frhdrs.symbol = frdhdrs.frhdr)
            join frdpersdimhier on (frdperspects.ref = frdpersdimhier.frdperspect)
            join frdimhier on (frdimhier.ref = frdpersdimhier.frdimshier)
            join frdimelems on (frdimelems.frdimhier = frdimhier.ref)
            join dimelemdef on (dimelemdef.ref = frdimelems.dimelem)
          where frdperspects.mainperspect = 0 and (frdperspects.ref = :FRDPERSPECTIN)
            and frdhdrs.ref = :frdhdr and frdpersdimhier.hierorder = :frdtreedeep and (dimelemdef.dimelemup = :elemrefin or dimelemdef.dimelemup is null)
           and ((FRDIMELEMS.READRIGHTS = ';' and FRDIMELEMS.READRIGHTSGROUP = ';') or (FRDIMELEMS.READRIGHTS like '%%;'||:operator||';%%') OR (strmulticmp(';'||:opergrupy||';',FRDIMELEMS.READRIGHTSGROUP)=1))
          into :elemref, :ELEMDESCRTMP, :name, :frdperspectout, :persdimhierout,
            :showval1, :val1name, :showval2, :val2name,
            :showval3, :val3name, :showval4, :val4name,
            :showval5, :val5name, :maxnum
      do begin
        num = :num + 1;
        if(:allrows=0) then begin --jesli nie zwracamy wszystkich wierszy to zakoncz po MAXNUM
          if(:maxnum is not null and :maxnum<>0 and :num=:maxnum) then name = '...';
          if(:maxnum is not null and :maxnum<>0 and :num>:maxnum) then break;
        end
        refupout = :refin;
        refout = :refout + 1;
        elemreftmp = :elemref;
        if (:elemref is not null) then
        begin
          piktogram = 1;
          ELEMHIEROUT = :ELEMHIERIN||','||cast(:elemref as varchar(255));
          ELEMDESCROUT = :ELEMDESCRIN||'->'||:ELEMDESCRTMP;
        end else
        begin
          piktogram = 0;
          ELEMHIEROUT = :elemhierin;
          ELEMDESCROUT = :elemdescrin;
        end
        FRDHDROUT = :FRDHDR;
        if (exists (select ref from frdpersdimhier where frdpersdimhier.frdperspect = :frdperspectout
            and frdpersdimhier.hierorder = :frdtreedeep + 1)) then begin
          haschildren = 1;
        end else begin
          haschildren = 0;
        end
        suspend;
        if (:haschildren = 1) then
        begin
          if (:elemref is null) then piktogram = 0;
          else piktogram = 1;
          elemref = null;
          refupout = :refout;
          refout = :refout + 1;
          FRDHDROUT = :FRDHDR;
          haschildren = 1;
          suspend;
          showval1 = null;
          val1name = null;
          showval2 = null;
          val2name = null;
          showval3 = null;
          val3name = null;
          showval4 = null;
          val4name = null;
          showval5 = null;
          val5name = null;
          for
            select FRDHDROUT ,elemref,name,frdperspectout,refout,refupout,persdimhierout, ELEMHIEROUT, ELEMDESCROUT,
              showval1, val1name, showval2, val2name, showval3, val3name, showval4, val4name, showval5, val5name, haschildren
              from FRDTREE_STRUCTURE(:frdhdr,:frdtreedeep + 1,:deep,:sorttype,:PERSDIMHIEROUT,:frdperspectout,:refout, :ELEMHIEROUT, :ELEMDESCROUT, :elemreftmp, :allrows)
              into :FRDHDROUT, :elemref, :name, :frdperspectout, :refout, :refupout, :persdimhierout, :ELEMHIEROUT, :ELEMDESCROUT,
                :showval1, :val1name, :showval2, :val2name, :showval3, :val3name, :showval4, :val4name, :showval5, :val5name, :haschildren
          do begin
            if (:elemref is not null) then
            begin
              piktogram = 1;
            end else
            begin
              piktogram = 0;
            end
            FRDHDROUT = :FRDHDR;
            suspend;
          end
        end
      end
      frdtreedeep = :frdtreedeep + 1;
    end
  end
end^
SET TERM ; ^
