--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_OBLICZSYMBOL(
      DOKUMSPED integer)
   as
DECLARE VARIABLE SYMBOL VARCHAR(1000);
DECLARE VARIABLE SSYMBOL VARCHAR(255);
DECLARE VARIABLE UWAGISPED VARCHAR(1000);
DECLARE VARIABLE FLAGISPED VARCHAR(1000);
DECLARE VARIABLE SUWAGISPED VARCHAR(1000);
DECLARE VARIABLE SFLAGISPED VARCHAR(1000);
begin
  symbol = '';
  uwagisped = '';
  flagisped = '';
  for select symbol, uwagisped, flagisped from br_listywysd_dok(:dokumsped) order by ref
  into :ssymbol, :suwagisped, :sflagisped
  do begin
    if(:suwagisped is null) then suwagisped = '';
    if(:sflagisped is null) then sflagisped = '';
    if(symbol <> '') then symbol = symbol ||';';
    symbol = symbol||ssymbol;
    if(flagisped <> '') then flagisped = flagisped ||';';
    flagisped = flagisped||sflagisped;
    if(uwagisped <> '') then uwagisped = uwagisped ||';';
    uwagisped = uwagisped||suwagisped;
    symbol =  substring(:symbol from 1 for 60);
    uwagisped =  substring(:uwagisped from 1 for 254);
    flagisped =  substring(:flagisped from 1 for 254);
  end
  update LISTYWYSD set SYMBOL = :symbol, UWAGISPED = :uwagisped, FLAGISPED = :flagisped
   where ((SYMBOL <> :symbol) or (SYMBOL is null)) and REF=:dokumsped;
end^
SET TERM ; ^
