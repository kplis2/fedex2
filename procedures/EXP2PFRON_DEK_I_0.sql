--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXP2PFRON_DEK_I_0(
      FROMDATE date,
      TODATE date,
      COMPANY integer,
      C18 numeric(14,2),
      C20 numeric(14,2),
      D24 numeric(14,2),
      D27 numeric(14,2),
      D33 numeric(14,2),
      E37 numeric(14,2),
      E39 numeric(14,2))
  returns (
      C16 numeric(14,2),
      C17 numeric(14,2),
      C19 numeric(14,2),
      C21 numeric(14,2),
      C22 numeric(14,2),
      C23 numeric(14,2),
      D25 numeric(14,2),
      D26 numeric(14,2),
      D28 numeric(14,2),
      D34 numeric(14,2),
      E35 numeric(14,2),
      E36 numeric(14,2),
      E38 numeric(14,2),
      E40 numeric(14,2),
      E41 numeric(14,2),
      MSG varchar(255) CHARACTER SET UTF8                           ,
      STATUS integer)
   as
declare variable TEMP numeric(14,2);
begin
  c18 = coalesce(c18,0);
  c20 = coalesce(c20,0);
  d24 = coalesce(d24,0);
  d27 = coalesce(d27,0);
  d33 = coalesce(d33,0);
  e37 = coalesce(e37,0);
  e39 = coalesce(e39,0);
  msg = '';
  status = 0;

  select coalesce(avg(ogolem3),0) as ogolem, coalesce(avg(ogolem_prac2),0) as niepel_ogolem,
         coalesce(avg(inv4),0) as lekki, coalesce(avg(inv5),0) as umiarkowany,
         coalesce(avg(inv6),0) as znaczny, coalesce(avg(cast(ogol_prac as numeric(14,2))),0) as ogolem_os,
         coalesce(avg(cast(ogol as numeric(14,2))),0) as niepel_ogolem_os,
         coalesce(avg(cast(inv0 as numeric(14,2))),0) as lekki_os,
         coalesce(avg(cast(inv1 as numeric(14,2))),0) as umiarkowany_os,
         coalesce(avg(cast(inv2 as numeric(14,2))),0) as znaczny_os
  from rpt_pr_invstruct(:fromdate,:todate,:company)
  into :c16, :c17,  :c22, :c21, :c19, :e35, :e36, :e41, :e40,  :e38;

  -------------- Pozycje z grupy C --------------
  c17 = :c17 + :c18 + :c20;
  c23 = 0.06*:c16 - (3*:c18 + 2*:c20 + :c17);

  if (:c23 <= 0) then
  begin
    msg = 'Dla podanych wartoci zatrudnienia nie jest konieczne skadanie deklaracji.Należy zożyć infromację INF-1.';
    status = 1;
    exit;
  end
  -------------- Pozycje z grupy D ---------------
  execute procedure get_pval(:fromdate, :company, 'PRZECIETNE_WYNAG')
    returning_values :d25;

  if (d24 < 0.2) then
    d26 = 0.4065 * :c23 * coalesce(:d25,0);
  else if (d24 < 0.3) then
    d26 = 0.75 * 0.4065 * :c23 * coalesce(:d25,0);
  else if (d24 < 0.4) then
    d26 = 0.5 * 0.4065 * :c23 * coalesce(:d25,0);
  else if (d24 < 0.5) then
    d26 = 0.25 * 0.4065 * :c23 * coalesce(:d25,0);
  else
    d26 = 0;

  d28 = d26 - d27;
  d34 = d26 - d27 - d33;

  ------------- Pozycje z grypy E ----------------

  e36 = e36 + e37 + e39;
  suspend;
end^
SET TERM ; ^
