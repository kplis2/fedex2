--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WRITE_STRING(
      SECTION varchar(100) CHARACTER SET UTF8                           ,
      IDENT varchar(50) CHARACTER SET UTF8                           ,
      VAL varchar(1024) CHARACTER SET UTF8                           ,
      PREFIX varchar(20) CHARACTER SET UTF8                            = null)
   as
declare variable cnt integer;
begin
  if(:prefix='') then prefix = null;
  if(:val = '' or (:val is null)) then
    delete from S_APPINI where SECTION=:section AND IDENT=:IDENT and coalesce(prefix, '') = coalesce(:prefix, '');
  else
    update or insert into S_APPINI(SECTION,IDENT,VAL,PREFIX) values (:SECTION, :IDENT, :VAL, :PREFIX)
    matching(SECTION,IDENT,PREFIX);
end^
SET TERM ; ^
