--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BTRANSFERS_GET_BANKACC(
      REFROZRACH integer,
      SLODEF integer,
      SLOPOZ integer)
  returns (
      BANKACCOUNT varchar(60) CHARACTER SET UTF8                           )
   as
declare variable invoice integer;
declare variable tmpslodef integer;
declare variable tmpslopoz integer;
declare variable slotyp varchar(15);
declare variable odecree integer;
begin

  bankaccount = '';

  /* Odczytanie numeru konta na podstawie ROZRACH.ACCOUNT */
  select r.slodef, r.slopoz, r.odecree, r.faktura, coalesce(b.account,'')
    from rozrach r left join bankaccounts b
    on (r.bankaccount = b.ref)
    where r.ref = :refrozrach
  into :tmpslodef, :tmpslopoz, :odecree, :invoice, :bankaccount;

  /* Odczytanie ref konta z bkdocs - zlączenie poprzez decrees */
  if(bankaccount = '' and odecree is not null) then begin
    select coalesce(b.account,'')
      from rozrach r left join decrees d on (r.odecree = d.ref)
      left join bkdocs bk on (d.bkdoc = bk.ref)
      left join bankaccounts b on (bk.bankaccount = b.ref)
      where r.ref = :refrozrach
    into :bankaccount;
  end

  /* Jeżeli dalej nie udalo sie odczytac a mamy fakture */
  else if(bankaccount = '' and invoice is not null) then begin
    select b.account from
      nagfak n left join bankaccounts b on (n.slobankacc = b.ref)
      where n.ref = :invoice
    into :bankaccount;
  end

  /* Jeżeli z jakiegos powodu konto nie zostalo dalej okreslone to jest brane z kartoteki kontrahenta */
  else if(bankaccount = '') then begin
    /* W przypadku nie przekazania jako argumentow wartosci slownikowe sa odczytywane z rozracha */
    if(slodef is null) then slodef = :tmpslodef;
    if(slopoz is null) then slopoz = :tmpslopoz;

    select typ from slodef where ref=:slodef into :slotyp;
    if(slotyp='KLIENCI') then
      select coalesce(klienci.rachunek,'') from klienci where ref=:slopoz into :bankaccount;
    else if(slotyp='DOSTAWCY') then
      select coalesce(dostawcy.rachunek,'') from dostawcy where ref=:slopoz into :bankaccount;
    else if(slotyp='EMPLOYEES') then
      select coalesce(b.account,'') from employees e
        left join persons p on (e.person = p.ref )
        left join bankaccounts b on (p.ref = b.dictpos)
        left join slodef s on (b.dictdef = s.ref)
        where e.ref = :slopoz and s.typ = 'PERSONS' and gl=1
      into :bankaccount;
    else if(slotyp='PERSONS') then
      select coalesce(b.account,'') from persons p
        left join bankaccounts b on (p.ref = b.dictpos)
        left join slodef s on (b.dictdef = s.ref)
        where p.ref = :slopoz and s.typ = 'PERSONS' and gl=1
      into :bankaccount;
  end

  suspend;

end^
SET TERM ; ^
