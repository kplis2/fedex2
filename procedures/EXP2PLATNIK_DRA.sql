--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXP2PLATNIK_DRA(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      COMPANY integer)
  returns (
      IIIP1 integer,
      IIIP2 smallint,
      IIIP3 numeric(14,2),
      IVP1 numeric(14,2),
      IVP2 numeric(14,2),
      IVP3 numeric(14,2),
      IVP4 numeric(14,2),
      IVP5 numeric(14,2),
      IVP6 numeric(14,2),
      IVP7 numeric(14,2),
      IVP8 numeric(14,2),
      IVP9 numeric(14,2),
      IVP10 numeric(14,2),
      IVP11 numeric(14,2),
      IVP12 numeric(14,2),
      IVP13 numeric(14,2),
      IVP14 numeric(14,2),
      IVP15 numeric(14,2),
      IVP16 numeric(14,2),
      IVP17 numeric(14,2),
      IVP18 numeric(14,2),
      IVP19 numeric(14,2),
      IVP20 numeric(14,2),
      IVP21 numeric(14,2),
      IVP22 numeric(14,2),
      IVP23 numeric(14,2),
      IVP24 numeric(14,2),
      IVP25 numeric(14,2),
      IVP26 numeric(14,2),
      IVP27 numeric(14,2),
      IVP28 numeric(14,2),
      IVP29 numeric(14,2),
      IVP30 numeric(14,2),
      IVP31 numeric(14,2),
      IVP32 numeric(14,2),
      IVP33 numeric(14,2),
      IVP34 numeric(14,2),
      IVP35 numeric(14,2),
      IVP36 numeric(14,2),
      IVP37 numeric(14,2),
      VP1 numeric(14,2),
      VP2 numeric(14,2),
      VP3 numeric(14,2),
      VP4 numeric(14,2),
      VP5 numeric(14,2),
      VIP1 numeric(14,2),
      VIP2 numeric(14,2),
      VIIP1 numeric(14,2),
      VIIP2 numeric(14,2),
      VIIP3 numeric(14,2),
      VIIP4 numeric(14,2),
      VIIP6 numeric(14,2),
      VIIP5 numeric(14,2),
      VIIP7 numeric(14,2),
      VIIIP1 numeric(14,2),
      VIIIP2 numeric(14,2),
      VIIIP3 numeric(14,2),
      IXP1 integer,
      IXP2 integer,
      IXP3 numeric(14,2),
      XP1 varchar(6) CHARACTER SET UTF8                           ,
      XP2 numeric(14,2),
      XP3 numeric(14,2),
      XP4 numeric(14,2),
      XP5 numeric(14,2),
      XP6 varchar(1) CHARACTER SET UTF8                           )
   as
declare variable PREVTODATE date;
declare variable PREVFROMDATE date;
declare variable TODATE date;
declare variable FROMDATE date;
declare variable PREVPERIOD varchar(6);
declare variable PERSON integer;
declare variable IVP10TMP numeric(14,2);
declare variable IVP11TMP numeric(14,2);
declare variable VIIP4TMP numeric(14,2);
begin
--MWr: Personel - Eksport do Platnika informacji dla ZUS DRA (ver.xml od 2014)

  execute procedure period2dates(:period)
    returning_values :fromdate, :todate;

  execute procedure e_func_periodinc(:period,-1)
    returning_values :prevperiod;

  execute procedure period2dates(:prevperiod)
    returning_values :prevfromdate, :prevtodate;

/*III. Inne informacje ============================================================
     p1 - Liczba ubezpieczonych
     p2 - Wniosek pracodawcy o dofinansowanie skladek za osoby nieplnosprawne ze srodkow PFRON i budzetu panstwa
     p3 - Stopa procentowa składek na ubezpieczenie wypadkowe */

  select count(distinct e.person)
    from employees e
    where e.company = :company
      and exists(select ec.ref
                   from emplcontracts ec
                  where ec.employee = e.ref
                    and ((ec.fromdate <= :todate and (ec.enddate >= :fromdate or ec.enddate is null))
                      or (ec.fromdate <= :prevtodate and (ec.enddate >= :prevfromdate or ec.enddate is null)
                        and exists(select pp5.employee
                                     from epayrolls r5 join eprpos pp5 on (pp5.payroll = r5.ref and r5.iper = :period)
                                    where pp5.employee = e.ref)))
                    and (ec.empltype = 1
                      or ec.iflags like '%%FC;%%'
                      or ec.iflags like '%%;FE;%%'
                      or ec.iflags like '%%;FR;%%'
                      or ec.iflags like '%%;NFZ;%%'))
    into :IIIp1;
  IIIp1 = coalesce(IIIp1,0);

  IIIp2 = 0;

  execute procedure get_pval(todate, company, 'FW')
    returning_values :IIIp3;

/*IV. Zestawienie naleznych skladek na ub. spoleczne oraz zrodel finasowania ======
     p1  - Suma składek - kwota składek na ubezpieczenie emerytalne
     p2  - Suma składek - kwota składek na ubezpieczenia rentowe
     p3  - Suma kwot składek na ubezpieczenia emerytalne i rentowe
     p4  - Kwota składek na ubezpieczenie emerytalne - finansowana przez ubezpieczonych
     p5  - Kwota składek na ubezpieczenia rentowe- finansowana przez ubezpieczonych
     p6  - Suma kwot składek na ubezpieczenia emerytalne i rentowe finansowana przez ubezpieczonych
     p7  - Kwota składek na ubezpieczenie emerytalne - finansowana przez płatnika
     p8  - Kwota składek na ubezpieczenia rentowe- finansowana przez płatnika
     p9  - Suma kwot składek na ubezpieczenia emerytalne i rentowe finansowana przez płatnika
     p10 - Kwota składek na ubezpieczenie emerytalne - finansowana przez budżet państwa
     p11 - Kwota składek na ubezpieczenia rentowe- finansowana przez budżet państwa
     p12 - Suma kwot składek na ubezpieczenia emerytalne i rentowe finansowana przez budżet państwa
     p13 - Kwota składek na ubezpieczenie emerytalne - finansowana przez PFRON
     p14 - Kwota składek na ubezpieczenia rentowe- finansowana przez PFRON
     p15 - Suma kwot składek na ubezpieczenia emerytalne i rentowe finansowana przez PFRON
     p16 - Kwota składek na ubezpieczenie emerytalne - finansowana przez Fundusz Kościelny
     p17 - Kwota składek na ubezpieczenia rentowe- finansowana przez Fundusz Kościelny
     p18 - Suma kwot składek na ubezpieczenia emerytalne i rentowe finansowana przez Fundusz Kościelny
     p19 - Suma składek - kwota składek na ubezpieczenie chorobowe
     p20 - Suma składek - kwota składek na ubezpieczenie wypadkowe
     p21 - Suma kwot składek na ubezpieczenia chorobowe i wypadkowe
     p22 - Kwota składek na ubezpieczenie chorobowe finansowana przez ubezpieczonych
     p23 - Kwota składek na ubezpieczenie wypadkowe finansowana przez ubezpieczonych
     p24 - Suma kwot na ubezpieczenia chorobowe i wypadkowe finansowane przez ubezpieczonych
     p25 - Kwota składek na ubezpieczenie chorobowe finansowana przez płatnika
     p26 - Kwota składek na ubezpieczenie wypadkowe finansowana przez płatnika
     p27 - Suma kwot na ubezpieczenia chorobowe i wypadkowe finansowane przez płatnika
     p28 - Kwota składek na ubezpieczenie chorobowe finansowana przez budżet państwa
     p29 - Kwota składek na ubezpieczenie wypadkowe finansowana przez budżet państwa
     p30 - Suma kwot na ubezpieczenia chorobowe i wypadkowe finansowane przez budżet państwa
     p31 - Kwota składek na ubezpieczenie chorobowe finansowana przez PFRON
     p32 - Kwota składek na ubezpieczenie wypadkowe finansowana przez PFRON
     p33 - Suma kwot na ubezpieczenia chorobowe i wypadkowe finansowane przez PFRON
     p34 - Kwota składek na ubezpieczenie chorobowe finansowana przez Fundusz Kościelny
     p35 - Kwota składek na ubezpieczenie wypadkowe finansowana przez Fundusz Kościelny
     p36 - Suma kwot na ubezpieczenia chorobowe i wypadkowe finansowane przez Fundusz Kościelny
     p37 - Kwota składek na ubezpieczenia społeczne, który powinien przekazac Płatnik  */

  select sum(case when p.ecolumn = 6100 then p.pvalue else 0 end)
       , sum(case when p.ecolumn = 6110 then p.pvalue else 0 end)
       , sum(case when p.ecolumn = 8200 then p.pvalue else 0 end)
       , sum(case when p.ecolumn = 8210 then p.pvalue else 0 end)
       , sum(case when p.ecolumn in (6120,6130) then p.pvalue else 0 end)
       , sum(case when p.ecolumn = 8220 then p.pvalue else 0 end)
    from epayrolls r
      join eprpos p on (p.payroll = r.ref)
    where r.company = :company and r.iper = :period
      and p.ecolumn in (6100, 6110, 6120, 6130, 8200, 8210, 8220)
    into :IVp4, :IVp5, :IVp7, :IVp8, :IVp22, :IVp26;

  IVp4 = coalesce(IVp4,0);
  IVp5 = coalesce(IVp5,0);
  IVp6 = IVp4 + IVp5;
  IVp7 = coalesce(IVp7,0);
  IVp8 = coalesce(IVp8,0);
  IVp9 = IVp7 + IVp8;

  IVp10 = 0;
  IVp11 = 0;
  VIIp4 = 0;
  for
    select distinct e.person from epayrolls r
        join eabsences a on (a.epayroll = r.ref and a.company = :company)
        join employees e on (a.employee = e.ref and e.company = :company)
      where a.ecolumn in (140, 150, 260, 270, 280, 290, 440, 450)
        and a.correction in (0,2)
        and r.iper = :period
      into :person
  do begin
    IVp10tmp = null;
    IVp11tmp = null;
    VIIp4tmp = null;
    select sum(B11), sum(B12), sum(B10)
      from evac_rca_dra(:period, :company, :person)
      into :IVp10tmp, :IVp11tmp, :VIIp4tmp;

    IVp10 = IVp10 + coalesce(IVp10tmp,0);
    IVp11 = IVp11 + coalesce(IVp11tmp,0);
    VIIp4 = VIIp4 + coalesce(VIIp4tmp,0);
  end

  IVp12 = IVp10 + IVp11;
  IVp13 = 0;
  IVp14 = 0;
  IVp15 = IVp13 + IVp14;
  IVp16 = 0;
  IVp17 = 0;
  IVp18 = IVp16 + IVp17;

  IVp22 = coalesce(IVp22,0);
  IVp23 = 0;
  IVp24 = IVp22 + IVp23;
  IVp25 = 0;
  IVp26 = coalesce(IVp26,0);
  IVp27 = IVp25 + IVp26;
  IVP28 = 0;
  IVP29 = 0;
  IVP30 = IVP28 + IVP29;
  IVp31 = 0;
  IVp32 = 0;
  IVp33 = IVp31 + IVp32;
  IVp34 = 0;
  IVp35 = 0;
  IVp36 = IVp34 + IVp35;

  IVp1 = IVp4 + IVp7 + IVp10 + IVp13 + IVp16;
  IVp2 = IVp5 + IVp8 + IVp11 + IVp14 + IVp17;
  IVp3 = IVp1 + IVp2; 

  IVp19 = IVp22 + IVp25 + IVp28 + IVp31 + IVp34;
  IVp20 = IVp23 + IVp26 + IVp29 + IVp32 + IVp35;
  IVp21 = IVp19 + IVp20;

  IVp37 = IVp6 + IVp9 + IVp24 + IVp27;

/* V. Zestawienie wyplaconych swiadczen podlegajacych rozliczeniu w ciezar
 skladek na ubezpieczenia spoleczne  ==============================================
     p1 - Kwota wypłaconych świadczeń z ubezpieczenia chorobowego
     p2 - Kwota wynagrodzenia należnego płatnikowi od wypłaconych świadczeń z ubezpieczenia chorobowego
     p3 - Kwota wypłaconych świadczeń z ubezpieczenia wypadkowego
     p4 - Kwota wypłaconych świadczeń finansowanych z budzetu państwa
     p5 - Laczna kwota do potracenia */

  select sum(case when p.ecolumn in (4100,4110,4200,4210,4350,4360,4450,4460,4500,4510,4700,4710) then p.pvalue else 0 end)
       , sum(case when p.ecolumn in (4150,4160,4250,4260) then p.pvalue else 0 end)
       , sum(case when p.ecolumn in (4300,4310,4400,4410,4420,4550,4560) then p.pvalue else 0 end)
       , sum(case when p.ecolumn = 4900 then p.pvalue else 0 end)
    from epayrolls r
      join eprpos p on (p.payroll = r.ref)
    where r.company = :company and r.iper = :period
      and p.ecolumn in (4100,4110,4200,4210,4150,4160,4250,4260,4300,4310,4350,4360,4400,4410,4420,4450,4460,4500,4510,4550,4560,4700,4710,4900)
    into :Vp1, :Vp3, :Vp4, :Vp5;

  Vp1 = coalesce(Vp1,0);
  Vp2 = Vp1 / 1000.00;  --0,1% z sumy zasilkow ZUS
  Vp3 = coalesce(Vp3,0);
  Vp4 = coalesce(Vp4,0);
  Vp5 = coalesce(Vp5,0) + Vp2;

/*VI. Rozliczenie części IV i V ===================================================
     p1 - Kwota do zwrotu przez ZUS
     p2 - Kwota do zapłaty przez płatnika */

  VIp1 = IVp37 - Vp5;
  if (VIp1 < 0) then
  begin
    VIp1 = -VIp1;
    VIp2 = 0;
  end else
  begin
    VIp2 = VIp1;
    VIp1 = 0;
  end

/*VII. Zestawienie naleznych skladek na ubezpieczneie zdrowotne ===================
     p1 - Kwota należnych skadek finansowana przez patnika
     p2 - Kwota należnych skadek finansowana przez ubezpieczonych
     p3 - Kwota należnych składek finansowana przez Fundusz Kościelny
     p4 - Kwota należnych składek finansowana z budzetu panstwa bezposrednio do ZUS
     p5 - Kwota należnych składek do przekazania przez płatnika
     p6 - Kwota należnego wynagrodzenia dla płatnika
     p7 - Kwota do zapłaty */

  select sum(p.pvalue)
    from epayrolls r
      join eprpos p on (p.payroll = r.ref)
    where r.company = :company and r.iper = :period
      and p.ecolumn = 7220
    into :VIIp2;

  VIIp1 = 0;
  VIIp2 = coalesce(VIIp2,0);
  VIIp3 = 0;
--VIIp4 policzone w procedurze EVAC_RCA_DRA
  VIIp5 = VIIp1 + VIIp2;
  VIIp6 = 0;
  VIIp7 = VIIp5 - VIIp6;

/*VIII. Zestawienie należnych składek na FP i FGSP ================================
     p1 - Kwota należnych składek na Fundusz Pracy
     p2 - Kwota należnych składek na Fundusz Gwarantowanych Swiadczeń Pracowniczych
     p3 - Kwota do zapłaty */

  select sum(case when p.ecolumn = 8810 then p.pvalue else 0 end)
       , sum(case when p.ecolumn = 8820 then p.pvalue else 0 end)
    from epayrolls r
      join eprpos p on (p.payroll = r.ref)
    where r.company = :company and r.iper = :period
      and p.ecolumn in (8810, 8820)
    into :VIIIp1, :VIIIp2;

  VIIIp1 = coalesce(VIIIp1,0);
  VIIIp2 = coalesce(VIIIp2,0);
  VIIIp3 = VIIIp1 + VIIIp2;

/*IX. Zestawienie naleznych skladek na Fundusz Emerytur Pomostowych ===============
     p1 - Liczba pracownikow, za ktorych jest oplacana skladka
     p2 - Liczba stanowisk pracy w szczegolnych warunkach lub o szczegolnym charkterze
     p3 - Suma naleznych skladek na Fundusz Emerytur Pomostowych */

  select count(distinct p.employee), sum(p.pvalue)
    from epayrolls r
      join eprpos p on (p.payroll = r.ref)
    where r.company = :company and r.iper = :period
      and p.ecolumn = 8830
    into :IXp1, :IXp3;

  select count(distinct workpost)
    from get_especconditwork(null, :fromdate, :todate, :company, 1)
    where workpost is not null
    into :IXp2;

  IXp1 = coalesce(IXp1,0);
  IXp2 = coalesce(IXp2,0);
  IXp3 = coalesce(IXp3,0);

--X. Deklaracja dochodu (dla osob oplacajacych skladki wylacznie za siebie) =======
  Xp1 = '';  -- Kod tytułu ubezpieczenia
  Xp2 = 0;   -- Podstawa wymiaru składek na ubezpieczenia emerytalne i rentowe
  Xp3 = 0;   -- Podstawa wymiaru składek na ubezpieczenia chorobowe
  Xp4 = 0;   -- Podstawa wymiaru składek na ubezpieczenia wypadkowe
  XP5 = 0;   -- Podstawa wymiaru składek na ubezpieczenie zdrowotne
  XP6 = '';  -- Informacja o przekroczeniu rocznej podstawy wymiaru składek na ubezp. emer-rentowe (slow. 1,2 lub 3)

--nic nie zwracamy, kiedy wartosci sa zerowe (wymagane przez kod zrodlowy)
  if (IVp3 + IVp21 + Vp5 + VIIp7 + VIIIp3 + IXp3 > 0) then
    suspend;
end^
SET TERM ; ^
