--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RKDOKPOZ_LIST_FROM_REFS(
      SERIES varchar(1024) CHARACTER SET UTF8                           )
  returns (
      RKDOKPOZ_REF integer,
      KWOTA numeric(14,2),
      DIRECTION char(1) CHARACTER SET UTF8                           ,
      DOK integer,
      DATA timestamp)
   as
declare variable REFOUT integer;
begin
  for select cast(stringout as integer)
    from parsestring(:series,';')
    into :refout
  do begin
    select rk.pm, rk.kwota, rk.ref, rk.dokument, ro.dataplat
      from rkdokpoz rk join rkdoknag rkn on (rk.dokument = rkn.ref)
        join rkrapkas rp on (rp.ref = rkn.raport)
        join rkstnkas sk on (sk.kod = rp.stanowisko)
        join oddzialy o on (o.oddzial = sk.oddzial)
        join rozrach ro on (rk.rozrachunek = ro.symbfak and rk.konto = ro.kontofk and ro.company = o.company)
      where rk.ref = :refout
    into :direction, :kwota, :rkdokpoz_ref, :dok, :data;
    --Jeżeli ref jest nullem znaczy ze nie ma juz tej transakcji bo przy wyrownaniu zostala wywalona
    if(:rkdokpoz_ref is not null) then
      suspend;
    rkdokpoz_ref = null;
  end
end^
SET TERM ; ^
