--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_17(
      REFF integer)
   as
declare variable S_ACCOUNT ACCOUNT_ID;
declare variable I_SIDE integer;
declare variable DEBIT numeric(14,2);
declare variable CREDIT numeric(14,2);
declare variable CURRDEBIT numeric(14,2);
declare variable CURRCREDIT numeric(14,2);
declare variable CURRENCY varchar(3);
declare variable N_VALUE numeric(14,2);
declare variable N_CURVALUE numeric(14,2);
declare variable N_CURRATE numeric(10,4);
declare variable S_DOSTAWCA ANALYTIC_ID;
declare variable S_DESCRIPT varchar(255);
declare variable I_DICTPOS integer;
declare variable I_DICTDEF integer;
declare variable I_ACCOUNT integer;
declare variable S_ODDZIAL varchar(255);
declare variable S_SETTLEMENT varchar(20);
declare variable S_SYMBOL varchar(20);
begin
 /* schemat dekretowania - dokumenty importowe PZI */

  credit = 0;
  debit = 0;
  currcredit = 0;
  currdebit = 0;

  s_descript = '';

  /* kwota */

  select B.symbol, B.descript, B.dictdef, B.dictpos, D.wartosc, D.sumwartwal,O.symbol, DS.kontofk, D.symbfak, D.kurs, D.waluta
    from bkdocs B
    join dokumnag D on (D.ref = B.oref and B.ref=:REFF)
    join dostawcy DS on (D.dostawca=DS.ref)
    join oddzialy O on (D.oddzial = O.oddzial)
    into :s_symbol, :s_descript, :i_dictdef, :i_dictpos, :n_value, :n_curvalue, :s_oddzial, :s_dostawca, :s_settlement, :n_currate, :currency;





  /* strona ksiegowa */
  i_side = 1;

  /* konto */
  s_account = '204-' || s_dostawca;

  /* końcowe operacje - wstawianie dekretu do bazy */
  if (:i_side = 0) then
  begin
    debit = n_value;
    currdebit = n_curvalue;
  end else
  begin
    credit = n_value;
    currcredit = n_curvalue;
  end

  insert into decrees (bkdoc, account, accref, side, debit, credit, descript, rate, currdebit, currcredit, currency, settlement)
              values (:REFF, :s_account, :i_account, :i_side, :debit, :credit, :s_descript, :n_currate, :currdebit, :currcredit, :currency, :s_settlement);
  /*  koniec dekretu*/

  /*  drugi */
  credit = 0;
  debit = 0;
  currcredit = 0;
  currdebit = 0;

  /* strona ksiegowa */
  i_side = 0;

  /* konto */
  s_account = '332-' || :s_oddzial || '-1';

  /* końcowe operacje - wstawianie dekretu do bazy */
  if (:i_side = 0) then
    debit = n_value;
  else
    credit = n_value;

  insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
              values (:REFF, :s_account, :i_account, :i_side, :debit, :credit, :s_descript);
  /*  koniec dekretu*/

  suspend;
end^
SET TERM ; ^
