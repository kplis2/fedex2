--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NOTYNAG_OBL(
      DOKUMENT integer)
   as
declare variable DEBIT numeric(14,2);
declare variable CREDIT numeric(14,2);
declare variable BDEBIT numeric(14,2);
declare variable BCREDIT numeric(14,2);
declare variable DEBITZL numeric(14,2);
declare variable CREDITZL numeric(14,2);
declare variable BDEBITZL numeric(14,2);
declare variable BCREDITZL numeric(14,2);
declare variable INTERESTS numeric(14,2);
declare variable INTERESTZL numeric(14,2);
declare variable DBT numeric(14,2);
declare variable DBTZL numeric(14,2);
declare variable STL varchar(20);
declare variable ACC varchar(120);
declare variable CHECKREF integer;
declare variable NOTAKIND smallint;
begin
  debit = 0;
  debitzl = 0;

  select ref, NOTAKIND from NOTYNAG where ref=:dokument into :checkref, :notakind;
  if(:checkref is null) then exit;/*dokumentu już nie ma*/
  if(:notakind < 2) then begin
    for select distinct NOTYPOZ.settlement, notypoz.account, notypoz.ammount, notypoz.ammountzl
    from notypoz where DOKUMENT = :dokument
    into :stl, :acc, :dbt, :dbtzl
    do begin
      debit = :debit + :dbt;
      debitzl = :debitzl + :dbtzl;
    end
    select sum(notypoz.ammountpaid), sum(notypoz.ammountpaidzl), sum(interest), sum(interestzl)
    from notypoz where dokument = :dokument
    into :credit, :creditzl, :interests, :interestzl;

  end else begin
    select sum(BDEBIT), sum(bcredit), sum(bdebitzl), sum(bcreditzl)
      from NOTYPOZ
      where DOKUMENT = :dokument
      into :debit, :credit, :debitzl, :creditzl;
  end
  if(:debit is null) then debit = 0;
  if(:credit is null) then credit = 0;
  if(:debitzl is null) then debitzl = 0;
  if(:creditzl is null) then creditzl = 0;
  if(:debit > :credit) then
    bdebit = :debit - :credit;
  else if(:debit < :credit) then
    bcredit = :credit - :debit;
  if(:debitzl > :creditzl) then
    bdebitzl = :debitzl - :creditzl;
  else if(:debitzl < :creditzl) then
    bcreditzl = :creditzl - :debitzl;
  if(:bdebit is null) then bdebit = 0;
  if(:bcredit is null) then bcredit = 0;
  if(:bdebitzl is null) then bdebitzl = 0;
  if(:bcreditzl is null) then bcreditzl = 0;
  update NOTYNAG set DEBIT = :debit, DEBITZL = :debitzl,  CREDIT = :credit,
        CREDITZL = :creditzl, bdebit = :bdebit, bcredit = :bcredit,
        bdebitzl = :bdebitzl, bcreditzl = :bcreditzl, interests = :interests, interestszl = :interestzl
  where REF=:dokument and (
     (debit <> :debit)
   or (credit <> :credit)
   or (bdebit <> :bdebit)
   or (bcredit <> :bcredit)
   or (debitzl <> :debitzl)
   or (creditzl <> :creditzl)
   or (bdebitzl <> :bdebitzl)
   or (bcreditzl <> :bcreditzl)
   or (interests <> :interests)
   or (interestszl <> :interestzl)
   );
end^
SET TERM ; ^
