--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FR_CALCULATE_MANUALS(
      FRVMANUALL integer)
   as
declare variable period varchar(6);
declare variable frhdr varchar(20);
declare variable frpsn integer;
declare variable symbol varchar(15);
declare variable colref integer;
declare variable col integer;
declare variable forbo integer;
declare variable colname varchar(60);
declare variable name varchar(255);
begin

  select period, frhdr from frvmanualls where ref =  :frvmanuall
    into :period, :frhdr;
  --  exception test_break :period||' '||frhdr||' '||company||' '||frversion;

  for
    select P.ref, P.symbol, P.name, C.ref, C.number, C.forbo, C.name
      from frpsns P
        join frcols C on (C.frhdr = P.frhdr and P.frhdr=:frhdr)
        where p.algorithm = 1
      order by P.countord, P.number, C.number
      into :frpsn, :symbol, :name, :colref, :col, :forbo, :colname
  do begin
   --exception test_break :symbol;

    insert into frvmanualpos (frvmanuall, symbol, name, colname, colnumber, forbo, frpsn, frcol)
      values (:frvmanuall, :symbol, :name, :colname, :col, :forbo, :frpsn, :colref);


  end
end^
SET TERM ; ^
