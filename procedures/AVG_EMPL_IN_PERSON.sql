--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AVG_EMPL_IN_PERSON(
      YEARID integer,
      WORKTYPE integer,
      COMPANY integer)
  returns (
      AMOUNT numeric(14,1))
   as
declare variable monthid integer;
declare variable firstday date;
declare variable lastday date;
declare variable curr_day date;
declare variable sum_empl float;
declare variable day_empl float;
declare variable okres integer;
declare variable month_empl float;
declare variable okres_pracy numeric(14,2);
declare variable absences integer;
begin
  --DS: wyliczanie zatrudnienia na potrzeby GUSu metoda liczenia zatrudnienia w kazdym dniu miesiaca
  -- i dzieleniu 12 srednich miesiecznych
  sum_empl = 0;
  monthid = 1;
  while (monthid  < 13) do
  begin
    firstday = cast(yearid || '/' || monthid || '/1' as date);
    if (monthid = 12) then
      lastday = cast(yearid || '/' || monthid || '/31' as date);
    else
      lastday = cast(yearid || '/' || (monthid + 1) || '/1' as date) - 1;
    okres = lastday - firstday ;
    okres_pracy = okres + 1;
    month_empl = 0;

    --dla kazdego dnia w miesiacu wylicza liczbe osob zatrudnionych minus osoby na urlopach bezplatnych
    while(okres >= 0) do
    begin
      curr_day = firstday + okres;

      select count(distinct e.person)
        from employment EM
          join employees e on (em.employee = e.ref)
        where EM.fromdate <= :curr_day  and (EM.todate >= :curr_day or EM.todate is null)
          and e.company = :company
          and (e.worktype = :worktype or :worktype is null)
        into :day_empl;

      select count(distinct e.person)
        from eabsences a
          join employees e on e.ref = a.employee
        where a.ecolumn in (260,330,300)
          and a.fromdate <= :curr_day and a.todate >= :curr_day
          and (e.worktype = :worktype or :worktype is null)
          and e.company = :company
        into :absences;

      day_empl = coalesce(:day_empl,0) - coalesce(:absences,0);
      month_empl = month_empl + day_empl;
      okres = okres - 1;
    end
    sum_empl = sum_empl + month_empl / okres_pracy;
    monthid = monthid + 1;
  end
  amount = sum_empl / 12;
  suspend;
end^
SET TERM ; ^
