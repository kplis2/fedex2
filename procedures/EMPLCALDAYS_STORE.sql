--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMPLCALDAYS_STORE(
      STYPE varchar(10) CHARACTER SET UTF8                           ,
      EMPLOYEE integer,
      FROMDATE date,
      TODATE date,
      DAYKIND smallint = 1)
  returns (
      RET integer)
   as
declare variable SQUERY varchar(40);
declare variable SPARAMS varchar(100);
declare variable TMPREF integer;
declare variable RET_SUM integer;
declare variable RET_CNT integer;
begin

  if (stype in ('SUM', 'COUNT')) then
    squery = 'EMPLCALDAYS_' || stype;
  else
    exception universal 'Nieznazny parametr!';

  employee = coalesce(employee,0);

  sparams = 'CDATE>=''' || fromdate || ''' and CDATE<=''' || todate || ''' and EMPLOYEE=' || employee;
  if (daykind is not null) then sparams = sparams || ' and DAYKIND=' || daykind;

  select ref, pvalue from epayrollparams_tmp
    where squery = :squery and sparams = :sparams and employee = :employee
    into tmpref, ret;

  if (tmpref is null) then
  begin
    if (daykind is not null) then
      select coalesce(sum(d.worktime),0), count(*)
        from emplcaldays d
        join edaykinds k on (k.ref = d.daykind)
        where d.cdate >= :fromdate and d.cdate <= :todate
          and d.employee = :employee
          and k.daykind = :daykind
        into :ret_sum, :ret_cnt;
    else
      select coalesce(sum(worktime),0), count(*)
        from emplcaldays
        where cdate >= :fromdate and cdate <= :todate
          and employee = :employee
        into :ret_sum, :ret_cnt;

    insert into epayrollparams_tmp(squery, sparams, pvalue, employee)
      values ('EMPLCALDAYS_SUM', :sparams, :ret_sum, :employee);

    insert into epayrollparams_tmp(squery, sparams, pvalue, employee)
      values ('EMPLCALDAYS_COUNT', :sparams, :ret_cnt, :employee);

    if (stype = 'SUM') then
      ret = ret_sum;
    else
      ret = ret_cnt;
  end
  suspend;
end^
SET TERM ; ^
