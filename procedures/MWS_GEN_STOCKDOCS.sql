--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_GEN_STOCKDOCS(
      STOCKDOC integer,
      WH varchar(3) CHARACTER SET UTF8                           ,
      WH2 varchar(3) CHARACTER SET UTF8                           ,
      DOCTYPE varchar(3) CHARACTER SET UTF8                           ,
      STOCKMWSORD integer,
      STOCKMWSORDSYMB varchar(30) CHARACTER SET UTF8                           ,
      OPERATOR integer,
      VERS integer,
      GOOD varchar(20) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4),
      MWSCONSTLOCP integer,
      MWSPALLOCP integer,
      MWSCONSTLOCL integer,
      MWSPALLOCL integer,
      LOT integer)
  returns (
      STOCKDOCEND integer)
   as
declare variable numberpos integer;
begin
  -- procedura generujaca dokumenty inwentaryzacyjne do rozliczenia zlecenia inwentaryzacyjnego
  if (:stockdoc is null) then
  begin
    if (doctype = '' or doctype is null) then
      exception MWS_SOTCKDOC_NOT_SET;
    else
    begin
      if (not exists (select typ from defdokummag where magazyn = :wh and typ = :doctype)) then
        exception DOC4WH_NOT_DEFINED 'Typ dokumentu: '||:doctype||' niezdefiniowany dla mag.: '||:wh;
      -- zalozenie naglowka dkoumentu inwentaryzacyjnego
      execute procedure GEN_REF('DOKUMNAG') returning_values :stockdoc;
      insert into DOKUMNAG(REF,MAGAZYN, TYP, DATA, AKCEPT,
          BLOKADA, MAG2, ZLECNAZWA, STOCKMWSORD,
          zrodlo, operator)
        values (:stockdoc, :wh, :doctype, current_date, 0,
          1, :wh2, :stockmwsordsymb, :stockmwsord,
          0, :operator);
    end
  end else
  begin
    select max(numer) from dokumpoz where dokument = :stockdoc into numberpos;
    insert into DOKUMPOZ(DOKUMENT, NUMER, KTM, WERSJA, ILOSC, ORD, MWSCONSTLOCP,
        MWSPALLOCP, MWSCONSTLOCL, MWSPALLOCL, DOSTAWA)
      values (:stockdoc, :numberpos, :good, :vers, :quantity, 1, :mwsconstlocp,
          :mwspallocp, :mwsconstlocl, :mwspallocl, :lot);
  end
  stockdocend = stockdoc;
end^
SET TERM ; ^
