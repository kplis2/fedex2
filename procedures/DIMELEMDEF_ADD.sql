--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DIMELEMDEF_ADD(
      DIMHIERDEF integer,
      MODE smallint)
  returns (
      STATUS integer,
      ADDTMP integer,
      UPDTMP integer,
      DELTMP integer)
   as
declare variable elemsaddmeth smallint;
declare variable act smallint;
declare variable slodef integer;
declare variable slopoz integer;
declare variable sqlelem varchar(1024);
declare variable dimelem integer;
declare variable dimelemname varchar(80);
declare variable dimelemshortname varchar(40);
declare variable ADDED integer;
declare variable UPDATED integer;
declare variable DELETED integer;
declare variable acc ANALYTIC_ID;
declare variable acc_vc timestamp;
declare variable acc2 timestamp;
declare variable bdate timestamp;
declare variable edate timestamp;
begin
  -- exception test_break dimhierdef ||' '|| mode ;
  -- mode 0 - tylko aktualizacja
  -- mode 1 - tylko dodaj nowe
  -- mode 2 - aktualizacja + dodaj nowe
  -- mode 3 - pelna synchronizacja - z próba usuniecia nieistniejacych
  status = 0;
  addtmp = 0;
  updtmp = 0;
  deltmp = 0;
  if (:mode is null) then
    exception DIMELEM_EXPT 'proszę określić tryb aktualizacji';
  if (:dimhierdef is null or :dimhierdef = 0) then
    exception DIMELEM_EXPT 'Brak wskazania do definicji wymiaru';
  else begin
    select dimhierdef.sqlelem, dimhierdef.slodef, dimhierdef.elemsaddmeth
      from dimhierdef
      where dimhierdef.ref = :dimhierdef
    into :sqlelem, :slodef, :elemsaddmeth;
    if (:elemsaddmeth = 0) then
      exception DIMELEM_EXPT 'W tym wymiarze elementy dodawane ręcznie';
    else if (:elemsaddmeth = 2) then
    begin
      if (:mode = 0) then
      begin
        for
          select dimelemdef.ref, substring( slopoz.nazwa from 1 for 80), slopoz.kod, slopoz.akt
            from dimelemdef
              join dimhierdef on (dimelemdef.dimhier = dimhierdef.ref and dimhierdef.isslodef = 1)
              join slopoz on (slopoz.slownik = dimelemdef.slodef and slopoz.ref = dimelemdef.slopoz)
            where dimhierdef.ref = :dimhierdef
              and (dimelemdef.shortname <> slopoz.kod or dimelemdef.name <> slopoz.nazwa)
            into :dimelem, :dimelemname, :dimelemshortname, :act
        do begin
          update dimelemdef set dimelemdef.shortname = :dimelemshortname, dimelemdef.name = :dimelemname,
              dimelemdef.act = :act where dimelemdef.ref = :dimelem;
          updtmp = :updtmp + 1;
        end
        status = 1;
      end else if (:mode = 1) then
      begin
        for
          select substring( slopoz.nazwa from 1 for 80), slopoz.kod, slopoz.slownik,
              slopoz.ref, slopoz.akt, slopoz.kontoks
            from slopoz
              left join dimelemdef on (slopoz.slownik = dimelemdef.slodef and slopoz.ref = dimelemdef.slopoz and dimelemdef.dimhier = :dimhierdef)
            where dimelemdef.ref is null  and slopoz.slownik = :slodef
            into :dimelemname, :dimelemshortname, :slodef, :slopoz, :act, :acc
        do begin
          insert into dimelemdef  (shortname, dimhier, slodef, slopoz, name, act, acc)
            values (:dimelemshortname, :dimhierdef, :slodef, :slopoz, :dimelemname, :act, :acc);
          addtmp = :addtmp + 1;
        end
        status = 1;
      end else if (:mode = 3) then
      begin
        for
          select dimelemdef.ref
            from dimelemdef
              join dimhierdef on (dimelemdef.dimhier = dimhierdef.ref and dimhierdef.isslodef = 1)
              left join slopoz on (slopoz.slownik = dimelemdef.slodef and slopoz.ref = dimelemdef.slopoz)
            where slopoz.ref is null
            into :dimelem
        do begin
          delete from dimelemdef where dimelemdef.ref = :dimelem;
          deltmp = :deltmp + 1;
        end
      end
    end else if (:elemsaddmeth = 3) then
    begin --###############################
      if (:mode = 3) then
        update dimelemdef set dimelemdef.dontdelete = 1 where dimelemdef.dimhier = :dimhierdef;
      for
        execute statement :sqlelem
        into :dimelemshortname, :dimelemname, :slodef, :slopoz, :act, :acc_vc, :acc2
      do begin
        bdate = acc_vc;
        edate = acc2;
        acc = cast(cast(acc_vc as date) as varchar(20));
        if (:mode = 0) then
        begin
          if (:slodef is not null and :slopoz is not null) then begin
            update dimelemdef set dimelemdef.shortname = :dimelemshortname,
                dimelemdef.name = :dimelemname, dimelemdef.act = :act, dimelemdef.acc = :acc,
                dimelemdef.bdate = :bdate, dimelemdef.edate = :edate
              where dimelemdef.slodef = :slodef and dimelemdef.slopoz = :slopoz;
            if (exists (select ref from dimelemdef where dimelemdef.slodef = :slodef
              and dimelemdef.slopoz = :slopoz and dimelemdef.dimhier = :dimhierdef)
            ) then
              updtmp = :updtmp + 1;
          end else if (:dimelemshortname is not null) then begin
            update dimelemdef set dimelemdef.name = :dimelemname, dimelemdef.act = :act, dimelemdef.acc = :acc,
                dimelemdef.bdate = :bdate, dimelemdef.edate = :edate
              where dimelemdef.shortname = :dimelemshortname;
            if (exists (select ref from dimelemdef where dimelemdef.shortname = :dimelemshortname
              and dimelemdef.dimhier = :dimhierdef)
            ) then
              updtmp = :updtmp + 1;
          end
        end else if (:mode = 1) then
        begin
          if (:slodef is not null and :slopoz is not null) then begin
            if (not exists (select ref from dimelemdef where dimelemdef.slodef = :slodef
              and dimelemdef.slopoz = :slopoz and dimelemdef.dimhier = :dimhierdef)
            ) then begin
              insert into dimelemdef  (shortname, dimhier, slodef, slopoz, name, act, acc, bdate, edate)
                values (:dimelemshortname, :dimhierdef, :slodef, :slopoz, :dimelemname, :act, :acc, :bdate, :edate);
              addtmp = :addtmp + 1;
            end
          end else if (:dimelemshortname is not null) then begin
            if (not exists (select ref from dimelemdef where dimelemdef.shortname = :dimelemshortname
              and dimelemdef.dimhier = :dimhierdef)
            ) then begin
              insert into dimelemdef  (shortname, dimhier, slodef, slopoz, name, act, acc, bdate, edate)
                values (:dimelemshortname, :dimhierdef, :slodef, :slopoz, :dimelemname, :act, :acc, :bdate, :edate);
              addtmp = :addtmp + 1;
            end
          end
        end else if (:mode = 3) then
        begin
          if (:slodef is not null and :slopoz is not null) then begin
            update dimelemdef set dimelemdef.dontdelete = 0 where dimelemdef.slodef = :slodef
              and dimelemdef.slopoz = :slopoz and dimelemdef.dimhier = :dimhierdef;
          end else if (:dimelemshortname is not null) then begin
            update dimelemdef set dimelemdef.dontdelete = 0
              where dimelemdef.shortname = :dimelemshortname and dimelemdef.dimhier = :dimhierdef;
          end
        end
      end
      if (:mode = 3) then
      begin
        select count(*) from dimelemdef where dimelemdef.dimhier = :dimhierdef and dimelemdef.dontdelete = 1
        into :deltmp;
        delete from dimelemdef where dimelemdef.dimhier = :dimhierdef and dimelemdef.dontdelete = 1;
        update dimelemdef set dimelemdef.dontdelete = 1 where dimelemdef.dimhier = :dimhierdef;
      end
    end
    if (:mode = 3 or :mode = 2) then begin
      execute procedure dimelemdef_add(:dimhierdef, 0)
        returning_values (:status, :added, :updated, :deleted);
      addtmp = :addtmp + :added;
      updtmp = :updtmp + :updated;
      deltmp = :deltmp + :deleted;
      execute procedure dimelemdef_add(:dimhierdef, 1)
        returning_values (:status, :added, :updated, :deleted);
      addtmp = :addtmp + :added;
      updtmp = :updtmp + :updated;
      deltmp = :deltmp + :deleted;
      status = 1;
    end
  end
end^
SET TERM ; ^
