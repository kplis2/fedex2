--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CLOSE_DISPOSITION(
      DOCID integer,
      CHECKORDER smallint,
      UPDATEDOC smallint,
      CREATESHIPPINGDOC smallint)
   as
declare variable TMPDOCID integer;
declare variable SHIPPINGDOCID integer;
begin
  for
    select distinct docid
      from mws_close_disposition_list(:docid,:checkorder)
      into tmpdocid
  do begin
    if (updatedoc = 1) then
      execute procedure MWS_DOC_FROM_MWSORD(:tmpdocid,'M',0);
    if (not exists(select first 1 1 from dokumpoz where dokument = :docid)) then
      delete from dokumnag where ref = :docid;
    else
      update dokumnag d set d.akcept = 1, d.blockmwsords = 1 where d.ref = :tmpdocid and d.akcept = 0;
    if (createshippingdoc = 1) then
      execute procedure listywys_addpoz(:shippingdocid,null,:tmpdocid,'M')
        returning_values (:shippingdocid);
  end
end^
SET TERM ; ^
