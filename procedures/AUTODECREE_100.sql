--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_100(
      BKDOC integer)
   as
declare variable ACCOUNT ACCOUNT_ID;
declare variable RESULT_ACCOUNT ACCOUNT_ID;
declare variable SIDE integer;
declare variable AMOUNT numeric(14,2);
declare variable DESCRIPT varchar(255);
declare variable YEARID integer;
declare variable SUMDEBIT numeric(14,2);
declare variable SUMCREDIT numeric(14,2);
declare variable SALDO numeric(14,2);
begin
  -- schemat dekretowania - przeksiegowania kont wynikowych na wynik
  result_account =  '860-2';


  select yearid, descript
    from bkdocs B
      join bkperiods P on (B.period = P.id)
    where B.ref = :bkdoc
    into :yearid, :descript;

  for
    select sum(debit), sum(credit), account
      from turnovers
      where account starting with '4' and yearid = :yearid and account <> '490'
      group by account
      having sum(debit) <> sum(credit)
      order by account
      into :sumdebit, :sumcredit, :account
  do begin
    saldo = sumdebit - sumcredit;
    if (saldo > 0) then
    begin
     amount = saldo;
     side = 1;
    end else begin
      amount = -saldo;
      side = 0;
    end

    -- zapis na dane konto
    execute procedure insert_decree(bkdoc, account, side, amount, descript, 0);

    side = 1 - side; --zmiana strony na przeciwna
    -- zapis na konto wynikowe
    execute procedure insert_decree(bkdoc, result_account, side, amount, descript, 0);
  end

  for
    select sum(debit), sum(credit), account
      from turnovers
      where account starting with '5' and yearid = :yearid
      group by account
      having sum(debit) <> sum(credit)
      order by account
      into :sumdebit, :sumcredit, :account
  do begin
    saldo = sumdebit - sumcredit;
    if (saldo > 0) then
    begin
     amount = saldo;
     side = 1;
    end else begin
      amount = -saldo;
      side = 0;
    end

    -- zapis na dane konto
    execute procedure insert_decree(bkdoc, account, side, amount, descript, 0);

    account = '490';

    side = 1 - side; --zmiana strony na przeciwna
    -- zapis na konto wynikowe
    execute procedure insert_decree(bkdoc, account, side, amount, descript, 0);
  end


  --pozostale wynikowe
  for
    select sum(debit), sum(credit), account
      from turnovers
      where bktype = 1 and yearid = :yearid
        and account not starting with '4' and account not starting with '5'
      group by account
      having sum(debit) <> sum(credit)
      order by account
      into :sumdebit, :sumcredit, :account
  do begin
    saldo = sumdebit - sumcredit;
    if (saldo > 0) then
    begin
     amount = saldo;
     side = 1;
    end else begin
      amount = -saldo;
      side = 0;
    end

    -- zapis na dane konto
    execute procedure insert_decree(bkdoc, account, side, amount, descript, 0);

    side = 1 - side; --zmiana strony na przeciwna
    -- zapis na konto wynikowe
    execute procedure insert_decree(bkdoc, result_account, side, amount, descript, 0);
  end
end^
SET TERM ; ^
