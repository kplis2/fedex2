--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYSD_OBLSYMPRZEW(
      DOKSPED integer)
   as
declare variable listyprzew varchar(255);
declare variable symprzew varchar(255);
declare variable numbers integer;
begin
  listyprzew = '';
  numbers = 0;
  for select listywysdsym.symbol
  from listywysdsym where listywysdsym.listywysddok = :doksped and listywysdsym.symbol <> ''
  order by numer
  into :symprzew
  do begin
    if(:listyprzew <>'') then listyprzew = :listyprzew||';';
    listyprzew = :listyprzew || :symprzew;
    numbers = numbers + 1;
  end
  update LISTYWYSD set listywysd.symbolsped = :listyprzew, SYMBOLSPEDNUM = :numbers
   where ref=:doksped and ((listywysd.symbolsped <> :listyprzew) or (listywysd.symbolspednum<> :numbers));
end^
SET TERM ; ^
