--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TOWAR_MWSNAZWA(
      KTM KTM_ID)
  returns (
      MWSNAZWA STRING80)
   as
begin
  if(exists(select first 1 1 from rdb$procedures where rdb$procedure_name = 'X_TOWAR_MWSNAZWA'))
  then begin
    execute statement 'execute procedure X_TOWAR_MWSNAZWA(:KTM)' into :mwsnazwa;
  end
  else if(exists(select first 1 1 from rdb$procedures where rdb$procedure_name = 'XK_TOWAR_MWSNAZWA'))
  then begin
    execute statement 'execute procedure XK_TOWAR_MWSNAZWA(:KTM)' into :mwsnazwa;
  end
  else
  begin
    select nazwa from towary where ktm = :ktm into :mwsnazwa;
  end
  suspend;
end^
SET TERM ; ^
