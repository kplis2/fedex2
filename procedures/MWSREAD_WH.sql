--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWSREAD_WH(
      FWH varchar(3) CHARACTER SET UTF8                           )
  returns (
      REF integer,
      COORDXB numeric(15,2),
      COORDXE numeric(15,2),
      COORDYB numeric(15,2),
      COORDYE numeric(15,2),
      AREATYPE smallint,
      LOGWHAREAS integer,
      SYMBOL varchar(40) CHARACTER SET UTF8                           ,
      HINT varchar(2000) CHARACTER SET UTF8                           )
   as
declare variable tmp varchar (20);
declare variable numer integer;
declare variable eol varchar(2);
begin
  eol = '
';
  for select w.REF, w.COORDXB, w.COORDXE, w.COORDYB, w.COORDYE, w.AREATYPE, w.LOGWHAREAS, w.SYMBOL
  from whareas w
  where w.wh = :fwh
  into :ref, :coordxb, :coordxe, :coordyb, :coordye, :areatype, :logwhareas, :symbol
  do begin
    tmp = null;
    for select s.symbol, s.MWSSTANDLEVELNUMBER
    from mwsconstlocsymbs s
    where wharea = :ref
    order by MWSSTANDLEVELNUMBER
    into :tmp, :numer
    do begin
      if(hint is not null and hint <> '') then
        hint = hint ||eol||cast(numer as varchar (3))||': '|| tmp;
      else
        hint = cast(numer as varchar (3))||': '||tmp;
    end
    suspend;
    ref = null;
    coordxb = null;
    coordxe = null;
    coordyb = null;
    coordye = null;
    areatype = null;
    logwhareas = null;
    symbol = null;
    hint = null;
  end
end^
SET TERM ; ^
