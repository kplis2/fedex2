--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_ZALACZNIKI_PROCESS(
      SESJAREF SESJE_ID,
      TABELA STRING35,
      REF INTEGER_ID,
      BLOKUJZALEZNE SMALLINT_ID = 0,
      TYLKONIEPRZETWORZONE SMALLINT_ID = 0,
      AKTUALIZUJSESJE SMALLINT_ID = 1)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable z_ref integer_id;
declare variable z_hash string255;
declare variable z_del integer_id;
declare variable z_rec string255;
declare variable z_skadtabela string40;
declare variable z_skadref integer_id;
declare variable z_tabelaid string120;
declare variable z_obiektid string120;
declare variable z_typ string60;
declare variable z_wartosc blob_utf8;
declare variable z_sciezka string255;
declare variable z_glowny integer_id;
declare variable z_nazwapliku string255;
declare variable z_rozszerzenie string10;
declare variable z_nazwa string255;
declare variable z_opis string1024;
declare variable z_zalacznikid string255;
--zalacznik
declare variable otable table_id;
declare variable oref integer_id;
declare variable typ multimedia_id;
declare variable nazwapliku string255;
declare variable rozszerzenie string10;
declare variable sciezka string255;
declare variable glowny smallint_id;
declare variable nazwa string255;
declare variable opis string1024;
--dodatkowe
declare variable error_row smallint_id;
declare variable errormsg_row string255;
declare variable errormsg_all string255;
declare variable sql memo;
declare variable stabela string35;
declare variable tmptabela string35;
declare variable sprocedura string255;
declare variable szrodlo zrodla_id;
declare variable skierunek smallint_id;
declare variable sesja sesje_id;
declare variable zalacznik zalaczniki_zew_id;


declare variable o_GLOWNY              SMALLINT_ID;
declare variable o_NAZWA               STRING255;
declare variable o_NAZWAPLIKU          STRING255;
declare variable o_OPIS                STRING1024;
declare variable o_OREF                INTEGER_ID;
declare variable o_OTABLE              TABLE_ID;
declare variable o_ROZSZERZENIE        STRING10;
declare variable o_SCIEZKA             STRING255;
declare variable o_TYP                 MULTIMEDIA_ID;
declare variable ktm ktm_id;
declare variable wersjaref integer_id;
declare variable wersja nrwersji_id;
declare variable numer integer_id;
begin
exception universal 'To nie powinno sid_fk_blokadaksiegowa uruchamiać INT_IMP_ZALACZNIKI_PROCESS';
  --[PM] przetwarzanie zalacznikow

  errormsg_all = '';
  status = 1;
  msg = '';

  --sprawdzenie czy wszystkie potrzebne informacje sa
  if (sesjaref is null and ref is null) then
    begin
      status = -1;
      msg = 'Za malo parametrow.';
      exit; --EXIT
    end

  if (sesjaref is null) then
    begin
      if (ref is null) then
        begin
          status = -1;
          msg = 'Jesli nie podales sesji to wypadaloby podac ref-a...';
          exit; --EXIT
        end
      if (tabela is null) then
        begin
          --status = -1;
          --msg = 'Wypadaloby podac nazwe tabeli...';
          --exit; --EXIT
          sql = 'select sesja from INT_IMP_ZALACZNIKI where ref='||:ref;
          execute statement sql into :sesja;
        end
      else
        begin
          sql = 'select sesja from '||:tabela||' where ref='||:ref;
          execute statement sql into :sesja;
        end

    end
  else
    sesja = sesjaref;

  if (sesja is null) then
    begin
      status = -1;
      msg = 'Nie znaleziono numeru sesji.';
      exit; --EXIT
    end

  --pobranie parametrow sesji
  select s.tabela, s.procedura, s.zrodlo, s.kierunek
    from int_sesje s
    where ref = :sesja
    into :stabela, :sprocedura, :szrodlo, :skierunek;

  -- [DG] XXX eksperymentalnie

  sql = 'select ref, tabelaid, obiektid, typ, wartosc, sciezka, glowny, nazwapliku,
          rozszerzenie, nazwa, opis, hash, del, rec, skadtabela, skadref,
          zalacznikid
        from int_imp_zalaczniki z
      where 1 = 1';
    
  if (:sesja is not null ) then
  begin
    sql = sql || ' and z.sesja = ' || :sesja;
  end
  if (ref is not null and tabela is null) then
  begin
    sql = sql || ' and z.ref = ' || :ref;
  end
  else if (ref is not null and tabela is not null) then
  begin
    sql = sql || ' and z.skadref = ' || :ref || ' and z.tabelaid = ''' || :tabela||'''';
  end
    for
      execute statement sql
        into :z_ref, :z_tabelaid, :z_obiektid, :z_typ, :z_wartosc, :z_sciezka, :z_glowny, :z_nazwapliku,
          :z_rozszerzenie, :z_nazwa, :z_opis, :z_hash, :z_del, :z_rec, :z_skadtabela, :z_skadref,
          :z_zalacznikid
    do begin
      error_row = 0;
      errormsg_row = '';
  
      z_tabelaid = upper(z_tabelaid);
  
      otable = null;
      oref = null;
      typ = 0;
      nazwapliku = null;
      rozszerzenie = null;
      sciezka = null;
      glowny = null;
      nazwa = null;
      opis = null;
      zalacznik = null;
  
      if (coalesce(trim(z_tabelaid),'') = '') then
        begin
          status = -1; -- XXX   KBI
          error_row = 1;
          errormsg_row = substring(errormsg_row || ' Brak nazwy tabeli.' from 1 for 255);
        end
  
      if (coalesce(trim(z_obiektid),'') = '') then
        begin
          status = -1; -- XXX   KBI
          error_row = 1;
          errormsg_row = substring(errormsg_row || ' Brak id. obiektu tabeli '||coalesce(trim(z_tabelaid),'') from 1 for 255);
        end
  
      if (coalesce(trim(z_nazwapliku),'') = '') then
        begin
           status = -1; -- XXX   KBI
          error_row = 1;
          errormsg_row = substring(errormsg_row || ' Brak nazwy pliku. ID '||z_obiektid from 1 for 255);
        end

      if (error_row = 0) then --sprawdzenie tabeli
        begin
          if (z_tabelaid not in ('TOWARY', 'TOWPLIKI')) then
            begin
              status = -1; -- XXX   KBI
              error_row = 1;
              errormsg_row = substring(errormsg_row || ' Nieznana tabela '||z_tabelaid from 1 for 255);
            end
          else
            begin
              otable = z_tabelaid;--tu dopisac obsule tabel o innych nazwach
              sql = 'select ref from '||:otable||' where int_zrodlo = '|| coalesce(:szrodlo,0) ||
               ' and int_id = '''||:z_obiektid || '''';
              execute statement sql into :oref;
              if (oref is null) then
                begin
                  error_row = 1;
                  errormsg_row = substring(errormsg_row || ' Nie znaleziono rekordu.' from 1 for 255);
                end
            end
        end
      if (error_row = 0) then
        begin
          numer = 0;
          for
            select w.ref, w.nrwersji
              from wersje w
              where w.ktm = :z_obiektid
                and w.akt = 1
            into :wersjaref, :wersja
          do begin

          update or insert into
            towpliki (ktm, wersja, numer, typ,  plik, wersjaref, oref, otable,
                int_zrodlo, int_id, int_dataostprzetw, int_sesjaostprzetw)
            values (:z_obiektid, :wersja, :numer, 1, :z_sciezka, :wersjaref, :oref,  :otable,
                :szrodlo, :z_zalacznikid, current_timestamp(0), :sesja)
            matching (otable, oref, ktm, wersjaref, numer)
            returning ref into :zalacznik;
            numer = numer + 1;
           end
        end
      else
      begin
        if(:error_row = 1)then
        begin
          status = -1; --ML jezeli nie przetworzy sie chociaz jeden wiersz przetwarzanie nieudane;
          errormsg_all = substring(errormsg_all || :errormsg_row from 1 for 255);
        end
      end
  
      update int_imp_zalaczniki z
        set dataostprzetw = current_timestamp(0),
          z.error = :error_row,
          z.errormsg = :errormsg_row,
          z.zalacznik_zew = :zalacznik
        where ref = :z_ref;
    end

  if(coalesce(status,-1) != 1) then
    msg = substring((msg || :errormsg_all) from 1 for 255);

  suspend;
end^
SET TERM ; ^
