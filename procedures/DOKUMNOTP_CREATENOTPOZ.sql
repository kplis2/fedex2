--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNOTP_CREATENOTPOZ(
      GRUPA integer,
      MAGAZYN varchar(6) CHARACTER SET UTF8                           ,
      TYP varchar(5) CHARACTER SET UTF8                           ,
      MAG2 varchar(5) CHARACTER SET UTF8                           ,
      DOKROZ integer,
      NEWCEN numeric(14,4),
      FROMNOTPOZ integer,
      ILOSC numeric(14,4))
   as
declare variable notref integer;
declare variable cnt integer;
declare variable oldcen numeric(14,4);
declare variable dokumnotpref integer;
declare variable datanoty timestamp;
declare variable orgdatanoty timestamp;
declare variable lokres varchar(10);
declare variable pozycja integer;
declare variable dokumnag integer;
begin
  if(:mag2 is null) then mag2 = '';

  select dokumroz.pozycja from dokumroz where ref=:dokroz into :pozycja;
  select dokumpoz.dokument from dokumpoz where ref=:pozycja into :dokumnag;
  select dokumnag.data from dokumnag where ref=:dokumnag into :datanoty;
  lokres = cast( extract( year from :datanoty) as char(4));
  if(extract(month from :datanoty) < 10) then
    lokres = :lokres ||'0' ||cast(extract(month from :datanoty) as char(1));
  else begin
    lokres = :lokres ||cast(extract(month from :datanoty) as char(2));
  end
  /*znalezienie dokumentu z grupy, który by spelnial kryteria */
  select min(coalesce(REF,REF)) from DOKUMNOT     --MS istotna optymalizacja
  where DOKUMNOT.grupa = :grupa and DOKUMNOT.TYP = :typ
  and DOKUMNOT.MAGAZYN = :magazyn and DOKUMNOT.mag2 = :mag2
  and DOKUMNOT.akcept = 0 and DOKUMNOT.okres=:lokres
  into :notref;
  if(:notref is null) then begin
    execute procedure GEN_REF('DOKUMNOT') returning_values :notref;
    insert into DOKUMNOT(REF,MAGAZYN, TYP, MAG2, DATA, FAKTURA, GRUPA, OPERATOR)
    select :notref, :magazyn, :typ, :mag2, (case when DATA>:datanoty then DATA else :datanoty end), FAKTURA, GRUPA,OPERATOR
    from DOKUMNOT join DOKUMNOTP on (DOKUMNOT.REF = DOKUMNOTP.DOKUMENT)
    where DOKUMNOTP.REF=:fromnotpoz;
  end else begin
    select DATA from DOKUMNOT where REF=:notref into :orgdatanoty;
    if(:orgdatanoty<:datanoty) then update DOKUMNOT set DATA=:datanoty where REF=:notref;
  end
  if(:notref is null) then notref = 0;
  if(:notref <= 0) then exception OPER_ERROR;
  /*sprawdzenie, czy ramach operacji dany DOKUMROZ nie by już korygowany - jeli tak,t o mamy do czynienia z zapetleniem*/
  dokumnotpref = null;
--  select max(DOKUMNOTP.REF) from DOKUMNOTP join DOKUMNOT on (DOKUMNOT.REF = DOKUMNOTP.DOKUMENT) where DOKUMNOT.GRUPA = :grupa and DOKUMNOT.akcept = 0 and DOKUMNOTP.dokumrozkor = :dokroz
--  into :dokumnotpref;
  if(:dokumnotpref > 0) then begin
    update DOKUMNOTP set CENANEW = :newcen where ref=:dokumnotpref;
  end else begin
    /*dodanie pozycji noty korygującej*/
    if (not exists(select * from inwenta where magazyn=:mag2 and zamk<>2)) then begin
    select cena from DOKUMROZ where REF=:dokroz into :oldcen;
    if(:newcen <> :oldcen) then
      insert into DOKUMNOTP(DOKUMENT, DOKUMROZKOR,CENANEW, FROMDOKUMNOTP,ILOSC) values (:notref, :dokroz, :newcen, :FROMNOTPOZ,:ilosc);
    end
    else begin
      exception universal 'Operacja niedozwolona, na magazynie '||:mag2||'jest otwarta inwentaryzacja!';
      end
  end
  suspend;
end^
SET TERM ; ^
