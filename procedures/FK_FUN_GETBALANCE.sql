--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_FUN_GETBALANCE(
      BALANCETYPE SMALLINT_ID,
      BDEBIT NUMERIC_14_2,
      BCREDIT NUMERIC_14_2)
  returns (
      SDEBIT NUMERIC_14_2,
      SCREDIT NUMERIC_14_2)
   as
begin
  if (balancetype = 0) then
    begin
      sdebit = bdebit - bcredit;
      scredit = 0;
    end else
    if (balancetype = 1) then
    begin
      if (abs(bdebit) > abs(bcredit)) then
      begin
        sdebit = bdebit - bcredit;
        scredit = 0;
      end else
      begin
        scredit = bcredit - bdebit;
        sdebit = 0;
      end
    end else
    if (balancetype = 2) then
    begin
      scredit = bcredit- bdebit;
      sdebit = 0;
    end
  suspend;
end^
SET TERM ; ^
