--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GEN_PAYROOL_TRANSFER(
      PAYROLL integer,
      TDATE date,
      BANKACC varchar(10) CHARACTER SET UTF8                           ,
      TYP smallint,
      TTITLE varchar(120) CHARACTER SET UTF8                            = '')
  returns (
      COUNTER integer)
   as
declare variable DICTDEF integer;
declare variable DICTPOS integer;
declare variable TOWHO varchar(255);
declare variable TOACC varchar(80);
declare variable TOADRESS varchar(255);
declare variable AMOUNT numeric(14,2);
declare variable CPER char(6);
declare variable DESCRIPT varchar(120);
declare variable COMPANY smallint;
declare variable AKTUOPERATOR integer;
declare variable BANK varchar(255);
declare variable PRZELEWTYPE varchar(10);
declare variable REFBTRANSF integer;
declare variable EMPLOYEE integer;
declare variable ECOLUMN integer;
begin
/*Generowanie przelewow bankowych do listy plac PAYROLL z data TDATE, z danego
  rachunku bankowego BANKACC, o tytule TTITLE*/

  counter = 0;
  select min(ref) from slodef where typ = 'PERSONS' into :dictdef;
  execute procedure get_global_param('AKTUOPERATOR') returning_values aktuoperator;
  execute procedure getconfig('PRZELEWWYPLATA') returning_values :przelewtype;
  if (tdate < current_timestamp(0)) then tdate = current_date;

--Uzupelnienie tytulu przelewu (PR41114)
  if (trim(coalesce(ttitle,'')) = '') then begin
    select cper from epayrolls where ref = :payroll into :cper;
    descript = 'Wynagrodzenie za ' || substring(cper from 5 for 2) || '/' || substring(cper from 1 for 4);
  end else
    descript = ttitle;

  for
    select b.account, e.personnames, e.person, e.company, coalesce(ba.nazwa,''),
           ps.employee, ps.ecolumn, ps.pvalue
      from eprpos ps
        join employees e on (ps.employee = e.ref)
        join emplbankaccounts eb on (eb.employee = e.ref and eb.ecolumn = ps.ecolumn)
        join bankaccounts b on (b.ref = eb.account)
        left join banki ba on (b.bank = ba.symbol)
      where ps.payroll = :payroll
        and ps.ecolumn in (9020,9030)
        and ps.btransfer is null
      into :toacc, :towho, :dictpos, :company, :bank, :employee, :ecolumn, :amount
  do begin
    toadress = '';  --zmiana sposobu generowania adresu wraz z PR41114
    select alladdress from get_persaddress(:dictpos)
      into :toadress;

    execute procedure gen_ref('BTRANSFERS')
      returning_values :refbtransf;

    insert into btransfers (ref, bankacc, typ, btype, slodef, slopoz, data,
        towho, toacc, toadress, todescript, curr, amount, autobtransfer, company)
      values (:refbtransf, :bankacc, 1, :przelewtype, :dictdef, :dictpos, :tdate,
        :towho, :toacc, :toadress, :descript, 'PLN', :amount, 1, :company);

    update eprpos set btransfer = :refbtransf
      where payroll = :payroll and employee = :employee and ecolumn = :ecolumn;

    if (typ = 1) then --ochrona danych osobowych (MWr_201108: jest bo bylo, z kodu nie ustawimy na 1)
      insert into epersdatasecur (person, kod, regdate, operator, descript)
        values (:dictpos, 2, :tdate, :aktuoperator, 'Przekazanie danych w postaci przelewu' || iif(:bank<>'', ' do: '||:bank, ''));

    counter = counter + 1;
  end
  suspend;
end^
SET TERM ; ^
