--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_DHL_SHIPMENT_REQUEST(
      OTABLE TABLE_NAME,
      OREF INTEGER_ID)
  returns (
      ID INTEGER_ID,
      PARENT INTEGER_ID,
      NAME STRING255,
      PARAMS STRING255,
      VAL STRING255)
   as
declare variable SHIPPINGDOC LISTYWYSD_ID;
declare variable SHIPPINGTYPE SPOSDOST_ID;
declare variable SHIPPINGDOCTYPE CHAR_1;
declare variable SHIPFROMPLACE STRING20;
declare variable SHIPPINGDOCBRANCH ODDZIAL_ID;
declare variable SHIPPINGPACKAGEDESC STRING60;
declare variable RECIPIENTREF ODBIORCY_ID;
declare variable RECIPIENTCUSTOMERREF KLIENCI_ID;
declare variable RECIPIENTISCOMPANY SMALLINT_ID;
declare variable RECIPIENTTAXID STRING20;
declare variable RECIPIENTNAME STRING255;
declare variable RECIPIENTFNAME STRING60;
declare variable RECIPIENTSNAME STRING60;
declare variable RECIPIENTPOSTCODE POSTCODE;
declare variable RECIPIENTCITY CITY_ID;
declare variable RECIPIENTSTREET STREET_ID;
declare variable RECIPIENTSTREETNO STREET_ID;
declare variable RECIPIENTHOUSENO NRDOMU_ID;
declare variable RECIPIENTLOCALNO NRDOMU_ID;
declare variable RECIPIENTCOUNTRY COUNTRY_ID;
declare variable RECIPIENTPHONE PHONES_ID;
declare variable RECIPIENTEMAIL EMAILWZOR_ID;
declare variable CODAMOUNT CENY;
declare variable INSURANCEAMOUNT CENY;
declare variable COMMENTS STRING1024;
declare variable SHIPPINGCOMMENTS STRING1024;
declare variable SHIPPINGCUSTOMERID STRING80;
declare variable SHIPPINGDRIVERID STRING30;
declare variable PAYERTYPE STRING40;
declare variable PAYERTAXID STRING20;
declare variable PAYERNAME STRING255;
declare variable PAYERPOSTCODE POSTCODE;
declare variable PAYERCITY CITY_ID;
declare variable PAYERSTREET STREET_ID;
declare variable PAYERSTREETNO STREET_ID;
declare variable PAYERHOUSENO NRDOMU_ID;
declare variable PAYERLOCALNO NRDOMU_ID;
declare variable PAYERPHONE PHONES_ID;
declare variable PAYEREMAIL STRING60;
declare variable PAYERCONTACT EMAILWZOR_ID;
declare variable ISSENDER SMALLINT_ID;
declare variable SENDERTAXID STRING20;
declare variable SENDERNAME STRING255;
declare variable SENDERPOSTCODE POSTCODE;
declare variable SENDERCITY CITY_ID;
declare variable SENDERSTREET STREET_ID;
declare variable SENDERSTREETNO STREET_ID;
declare variable SENDERHOUSENO NRDOMU_ID;
declare variable SENDERLOCALNO NRDOMU_ID;
declare variable SENDERPHONE PHONES_ID;
declare variable SENDEREMAIL STRING60;
declare variable SENDERCONTACT EMAILWZOR_ID;
declare variable SENDER_COUNTRY COUNTRY_ID;
declare variable ROOTPARENT INTEGER_ID;
declare variable FIRSTPARENT INTEGER_ID;
declare variable SECONDPARENT INTEGER_ID;
declare variable PACKAGESYMBOL SYMBOL_ID;
declare variable PACKAGESERVICETYPE STRING30;
declare variable PACKAGEWEIGHT WAGA_ID;
declare variable PACKAGELENGTH WAGA_ID;
declare variable PACKAGEWIDTH WAGA_ID;
declare variable PACKAGEHEIGHT WAGA_ID;
declare variable PACKAGENUMBER INTEGER_ID;
declare variable SHIPPINGDEPARTMENT ODDZIAL_ID;
declare variable SHIPPINGDOCSYMBOL STRING;
declare variable INVOICESYMBOL STRING;
declare variable RECIPIENT_CONTACT STRING;
declare variable RECIPIENTCUSTOMERREF_TMP KLIENCI_ID;
declare variable RECIPIENTISCOMPANY_TMP SMALLINT_ID;
declare variable RECIPIENTTAXID_TMP STRING20;
declare variable RECIPIENTNAME_TMP STRING255;
declare variable RECIPIENTFNAME_TMP STRING60;
declare variable RECIPIENTSNAME_TMP STRING60;
declare variable RECIPIENTPOSTCODE_TMP POSTCODE;
declare variable RECIPIENTCITY_TMP CITY_ID;
declare variable RECIPIENTSTREET_TMP STREET_ID;
declare variable RECIPIENTHOUSENO_TMP NRDOMU_ID;
declare variable RECIPIENTLOCALNO_TMP NRDOMU_ID;
declare variable RECIPIENTCOUNTRY_TMP COUNTRY_ID;
declare variable RECIPIENTPHONE_TMP PHONES_ID;
declare variable RECIPIENTEMAIL_TMP EMAILWZOR_ID;
declare variable RECIPIENT_CONTACT_TMP STRING;
declare variable SALESORDERFLAGS STRING;
declare variable SHIPPINGFLAGS STRING;
declare variable ORDPAYMENTTYPE PLATNOSC_ID;
declare variable INVOICEPAYMENTTYPE PLATNOSC_ID;
declare variable TERMDOST DATE_ID;
declare variable SHIPMENTSTARTHOUR STRING;
declare variable SHIPMENTENDHOUR STRING;
declare variable LABELPATH STRING;
declare variable LABELTYPE STRING;
declare variable nropk INTEGER_ID;
begin

/* DG:
zastanow sie dwa razy zanim zmienisz cos w tej procedurze,
szczegolnie nazwy tagow XML - moze to spowodowac problemy
z deserializacja pliku XML w sterownikach ktore go wykorzystuja!
*/
  if (:otable <> 'LISTYWYSD') then
    exception ede_listywysd_brak 'Brak obs?gi wybranego typu dokumentu';
  
 select l.ref, l.sposdost, l.typ, l.fromplace, l.oddzial,
      l.odbiorcaid,
      trim(coalesce(l.kontrahent,'')), trim(coalesce(l.imie,'n')), trim(coalesce(l.nazwisko,'n')),
      trim(replace(coalesce(l.kodp,''),'-','')), trim(coalesce(l.miasto,'')),
      trim(coalesce(l.adres,'')), trim(coalesce(l.nrdomu,'')), trim(coalesce(l.nrlokalu,'')),
      trim(coalesce(l.krajid,'')), trim(coalesce(l.telefon,'000000000')), trim(coalesce(l.email,'')),
      coalesce(l.pobranie,0), coalesce(l.ubezpieczenie,0),
      trim(coalesce(l.uwagi,'')), trim(coalesce(l.uwagisped,'')),
      iif(coalesce(l.symbol,'')!='',l.symbol,l.ref),
      trim(coalesce(l.flagi,'')), trim(coalesce(l.flagisped,'')),
      l.termdost,
      ddn.int_symbol
    from listywysd l
      left join odbiorcy o on (l.odbiorcaid = o.ref)
      left join klienci k on (o.klient = k.ref)
      left join dokumnag ddn on (ddn.ref=l.refdok)  --KP xxx138186
    where l.ref = :oref
  into :shippingdoc, :shippingtype, :shippingdoctype, :shipfromplace, :shippingdocbranch,
    :recipientref,
    :recipientname, :recipientfname, :recipientsname,
    :recipientpostcode, :recipientcity,
    :recipientstreet, :recipienthouseno, :recipientlocalno,
    :recipientcountry, :recipientphone, :recipientemail,
    :codamount, :insuranceamount,
    :comments, :shippingcomments,
    :shippingdocsymbol,
    :salesorderflags, :shippingflags,
    :termdost,
    :shippingpackagedesc;  --KP xxx138494;

  --sprawdzanie czy istnieje dokument spedycyjny
  if(:shippingdoc is null) then
    exception ede_listywysd_brak;

  if (coalesce(:shippingdocbranch,'') = '') then
    select d.oddzial
      from listywysd l
        left join defmagaz d on (l.fromplace = d.symbol)
      where l.ref = :shippingdoc
    into :shippingdocbranch;

  --jesli jest wybrany odbiorca do pomijamy pola uzupelnione z dokumentu spedycyjnego
  --i uzupelniamy dane adresowe z odbiorcy lub z klienta
  if (:recipientref is not null) then
  begin
    select k.ref, k.firma,
        trim(coalesce(iif(coalesce(k.nipue,'') = '',k.prostynip,k.nipue),'')),
        trim(coalesce(iif(coalesce(o.nazwa,'') = '',k.nazwa,o.nazwa),'')),
        trim(coalesce(iif(coalesce(o.imie,'') = '',k.imie,o.imie),'n')),
        trim(coalesce(iif(coalesce(o.nazwisko,'') = '',k.nazwisko,o.nazwisko),'n')),
        trim(replace(coalesce(iif(coalesce(o.dkodp,'') = '',k.kodp,o.dkodp),''),'-','')),
        trim(coalesce(iif(coalesce(o.dmiasto,'') = '',k.miasto,o.dmiasto),'')),
        trim(coalesce(iif(coalesce(o.dulica,'') = '',k.ulica,o.dulica),'')),
        trim(coalesce(iif(coalesce(o.dnrdomu,'') = '',k.nrdomu,o.dnrdomu),'')),
        trim(coalesce(iif(coalesce(o.dnrlokalu,'') = '',k.nrlokalu,o.dnrlokalu),'')),
        trim(coalesce(iif(coalesce(o.krajid,'') = '',k.krajid,o.krajid),'')),
        trim(coalesce(iif(coalesce(o.dtelefon,'') = '',k.telefon,o.dtelefon),'000000000')),
        trim(coalesce(iif(coalesce(o.email,'') = '',k.email,o.email),'')),
       iif( k.przedstaw = '','nn', k.przedstaw)


      from odbiorcy o
        join klienci k on (o.klient = k.ref)
      where o.ref = :recipientref
--<XXX SP
 into :recipientcustomerref_TMP, :recipientiscompany_TMP,
      :recipienttaxid_TMP,
      :recipientname_TMP,
      :recipientfname_TMP,
      :recipientsname_TMP,
      :recipientpostcode_TMP,
      :recipientcity_TMP,
      :recipientstreet_TMP,
      :recipienthouseno_TMP,
      :recipientlocalno_TMP,
      :recipientcountry_TMP,
      :recipientphone_TMP,
      :recipientemail_TMP,
      :recipient_contact_TMP;

  recipientcustomerref = case when coalesce(:recipientcustomerref,'') = '' then recipientcustomerref_TMP else recipientcustomerref end ;
  recipientiscompany = case when coalesce(:recipientiscompany,'') = '' then recipientiscompany_TMP else :recipientiscompany end ;
  recipienttaxid = case when coalesce(:recipienttaxid,'') = '' then :recipienttaxid_TMP else :recipienttaxid end ;
  recipientname = case when coalesce(:recipientname,'') = '' then :recipientname_TMP else :recipientname end ;
  recipientfname = case when coalesce(:recipientfname,'') = '' then :recipientfname_TMP else :recipientfname end ;
  recipientsname = case when coalesce(:recipientsname ,'') = '' then :recipientsname_TMP else :recipientsname end ;
  recipientpostcode = case when coalesce(:recipientpostcode,'') = '' then :recipientpostcode_TMP else :recipientpostcode end ;
  recipientcity = case when coalesce(:recipientcity,'') = '' then :recipientcity_TMP else :recipientcity end ;
  recipientstreet = case when coalesce(:recipientstreet,'') = '' then :recipientstreet_TMP else :recipientstreet end ;
  recipienthouseno = case when coalesce(:recipienthouseno,'') = '' then :recipienthouseno_TMP else :recipienthouseno end ;
  recipientlocalno = case when coalesce(:recipientlocalno,'') = '' then :recipientlocalno_TMP else :recipientlocalno end ;
  recipientcountry = case when coalesce(:recipientcountry,'') = '' then :recipientcountry_TMP else :recipientcountry end ;
  recipientphone = case when coalesce(:recipientphone,'') = '' then  :recipientphone_TMP else :recipientphone end ;
  recipientemail = case when coalesce(:recipientemail,'') = '' then :recipientemail_TMP else :recipientemail end ;
  recipient_contact = case when coalesce(:recipient_contact,'') = '' then :recipient_contact_TMP else :recipient_contact end ;

--XXX>

    execute procedure xk_usun_znaki(:recipienttaxid,0)
      returning_values :recipienttaxid;
  end
  else
    recipientiscompany = 0;
    recipienttaxid = '';

  recipientstreetno = :recipientstreet;
  if (:recipienthouseno <> '') then
  begin
    recipientstreetno = :recipientstreetno||' '||:recipienthouseno;
    if (:recipientlocalno <> '') then
      recipientstreetno = :recipientstreetno||'/'||:recipientlocalno;
  end

  --pobranie danych platnika i godziny dostawy
  select k.numerklienta, k.kurier, k.typplatnik,
      trim(coalesce(k.nipplatnika,'')), trim(coalesce(k.nazwaplatnika,'')),
      trim(replace(coalesce(k.kodpplatnika,''),'-','')), trim(coalesce(k.miastoplatnika,'')),
      trim(coalesce(k.ulicaplatnika,'')),
      trim(coalesce(k.nrdomuplatnika,'')), trim(coalesce(k.nrlokaluplatnika,'')),
      trim(coalesce(k.telefonplatnika,'')), trim(coalesce(k.emailplatnika,'')),
      trim(coalesce(k.osobakontaktowa,'nn')),
      --trim(coalesce(k.opispaczki,'')),  --KP xxx138186
      k.oddzial,
      coalesce(k.x_default_start_hour,'10:00'), coalesce(k.x_default_end_hour,'15:00'),
      k.sciezkapliku, k.x_dhl_labeltype
    from spedytwys w
      join spedytorzy s on (w.spedytor = s.symbol)
      join spedytkonta k on (s.symbol = k.spedytor)
    where w.sposdost = :shippingtype
      and k.oddzial = :shippingdocbranch
      and k.typ = s.typserwisu
  into :shippingcustomerid, :shippingdriverid, :payertype,
    :payertaxid, :payername,
    :payerpostcode, :payercity,
    :payerstreet, :payerhouseno, :payerlocalno,
    :payerphone, :payeremail, :payercontact,
   -- :shippingpackagedesc,  --KP xxx138186
    :shippingdepartment,
    :shipmentstarthour, :shipmentendhour,
    :labelPath, :labeltype;

  --pobranie danych nadawcy
  issender = 0;
  if (:shipfromplace <> '') then
  begin
    issender = 1;
    select trim(replace(coalesce(d.dkodp,''),'-','')), trim(coalesce(d.dmiasto,'')),
        trim(coalesce(d.dulica,'')), trim(coalesce(d.dnrdomu,'')), trim(coalesce(d.dnrlokalu,'')),
        trim(coalesce(d.dtelefon,'')), trim(coalesce(d.demail,'')),
        'HERMON'
      from defmagaz d
        left join operator o on (d.dosoba = o.ref)
      where d.symbol = :shipfromplace
    into :senderpostcode, :sendercity,
      :senderstreet, :senderhouseno, :senderlocalno,
      :senderphone, :senderemail, :sendercontact;

    sendername = :payername;

    senderstreetno = :payerstreet;
    if (:senderhouseno <> '') then
    begin
      senderstreetno = :senderstreetno||' '||:senderhouseno;
      if (:senderlocalno <> '') then
        senderstreetno = :senderstreetno||'/'||:senderlocalno;
    end

    if (:senderpostcode = '' or :sendercity = '' or :senderstreet = '') then
      issender = 0;
  end

  if (:payertaxid = '' or :payername = '') then
  begin
    --TODO jesli parametry sa nie ustawione
  end

  payerstreetno = :payerstreet;
  if (:payerhouseno <> '') then
  begin
    payerstreetno = :payerstreetno||' '||:payerhouseno;
    if (:payerlocalno <> '') then
      payerstreetno = :payerstreetno||'/'||:payerlocalno;
  end

  name = 'shippingObject';
  val = null;
  params = 'serviceProvider="DHL"';
  parent = null;
  id = 0;
  suspend;

  rootparent = :id;

  --START: wezel parametrow przesylki
    name = 'termdost';
    val = :termdost;
    params = null;
    parent = :rootparent;
    id = :id + 1;
    suspend;

    name = 'shipmentStartHour';
    val = :shipmentstarthour;
    params = null;
    parent = :rootparent;
    id = :id + 1;
    suspend;

    name = 'shipmentEndHour';
    val = :shipmentendhour;
    params = null;
    parent = :rootparent;
    id = :id + 1;
    suspend;

    name = 'labelPath';
    val = :labelpath;
    params = null;
    parent = :rootparent;
    id = :id + 1;
    suspend;

    name = 'labelType';
    val = :labeltype;
    params = null;
    parent = :rootparent;
    id = :id + 1;
    suspend;

    name = 'orderCourier';
    val = '0';
    params = null;
    parent = :rootparent;
    id = :id + 1;
    suspend;

    name = 'params';
    val = null;
    params = null;
    parent = :rootparent;
    id = :id + 1;
    suspend;

    firstparent = :id;

      name = 'customerID';
      val = :shippingcustomerid;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'driverID';
      val = :shippingdriverid;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'packageDesc';
      val = :shippingpackagedesc;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'department';
      val = :shippingdepartment;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

        --XXX JO: symbol faktury i sprawdzenie czy jest faktura
      select first 1 coalesce(f.symbol, d.symbol), z.sposzap, f.sposzap
        from listywysdpoz p
          left join nagzam z on (p.doktyp = 'Z' and p.dokref = z.ref)
          left join dokumnag d on (z.ref = d.zamowienie and d.akcept = 1)
          left join nagfak f on (d.faktura = f.ref and f.akceptacja = 1)
        where p.dokument = :shippingdoc
      into :invoicesymbol, :ordpaymenttype, :invoicepaymenttype;
      /*if (coalesce(:invoicesymbol,'') = '' and :salesorderflags not containing ';B;') then
        exception universal 'Brak faktury, a wysy?a wymaga faktury';
      else
        shippingdocsymbol = :invoicesymbol;
       */
      --zabezpieczenie pobrania    zakomentowalem sprawdzanie poniewaz my dostajemy kwote z systemu zewnetrznego
      /*if(((:ordpaymenttype = 689 and coalesce(:invoicepaymenttype,0) = 0) or coalesce(:invoicepaymenttype,0) = 689) and
          :codamount = 0) then
        exception universal 'Brak kwoty pobrania, dla sposobu zap?ty Pobranie';
      else
        if(((:ordpaymenttype <> 689 and coalesce(:invoicepaymenttype,0) = 0) or coalesce(:invoicepaymenttype,0) <> 689) and
          :codamount > 0) then
        exception universal 'B?dnie naliczona kwota pobrania - zg??to';  */
      --XXX JO: koniec

      name = 'documentSymbol';
      val = :shippingdocsymbol;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'remarks';
      val = :shippingcomments;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

  --KONIEC: wezel parametrow przesylki

  --START: wezel platnika
    name = 'payer';
    val = null;
    params = null;
    parent = :rootparent;
    id = :id + 1;
    suspend;

    firstparent = :id;

      name = 'type';
      val = :payertype;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'vatNumber';
      val = :payertaxid;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'name';
      val = :payername;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'postCode';
      val = :payerpostcode;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'city';
      val = :payercity;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'street';
      val = :payerstreet;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'houseNo';
      val = :payerhouseno;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'localNo';
      val = :payerlocalno;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'streetNo';
      val = :payerstreetno;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'phone';
      val = :payerphone;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'email';
      val = :payeremail;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'contact';
      val = :payercontact;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;
  --KONIEC: wezel platnika

  --START: wezel nadawcy
    name = 'sender';
    val = null;
    params = null;
    parent = :rootparent;
    id = :id + 1;
    suspend;

    firstparent = :id;
      name = 'contact';
      val = iif(:issender = 1,:sendercontact,:payercontact);
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'vatNumber';
      val = iif(:issender = 1,:sendertaxid,:payertaxid);
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'name';
      val = iif(:issender = 1,:sendername,:payername);
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'country';
      val = 'PL';
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'postCode';
      val = iif(:issender = 1,:senderpostcode,:payerpostcode);
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'city';
      val = iif(:issender = 1,:sendercity,:payercity);
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'street';
      val = iif(:issender = 1,:senderstreet,:payerstreet);
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'houseNo';
      val = iif(:issender = 1,:senderhouseno,:payerhouseno);
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'localNo';
      val = iif(:issender = 1,:senderlocalno,:payerlocalno);
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'phone';
      val = iif(:issender = 1,:senderphone,:payerphone);
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'email';
      val = iif(:issender = 1,:senderemail,:payeremail);
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'contact';
      val = iif(:issender = 1,:sendercontact,:payercontact);
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;
  --KONIEC: wezel nadawcy

    --STRAT: wezel odbiorcy
    name = 'recipient';
    val = null;
    params = null;
    parent = :rootparent;
    id = :id + 1;
    suspend;

    firstparent = :id;
      
      name = 'ID';
      val = :recipientref;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'isCompany';
      val = :recipientiscompany;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'vatNumber';
      val = :recipienttaxid;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'name';
      val = :recipientname;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'fName';
      val = :recipientfname;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'sName';
      val = :recipientsname;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'postCode';
      val = :recipientpostcode;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'city';
      val = :recipientcity;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'street';
      val = :recipientstreet;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'houseNo';
      val = iif(coalesce(:recipienthouseno,'')='','.',:recipienthouseno);
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'localNo';
      val = :recipientlocalno;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'streetNo';
      val = :recipientstreetno;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'country';
      val = :recipientcountry;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'phone';
      val = :recipientphone;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'email';
      val = :recipientemail;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      name = 'contact';
      val = :recipient_contact;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;
  --KONIEC: wezel odbiorcy

    --START: wezel uslugi
    name = 'services';
    val = null;
    params = null;
    parent = :rootparent;
    id = :id + 1;
    suspend;

    firstparent = :id;

      --pobranie
      name = 'cod';
      val = :codamount;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      --ubezpieczenie
      /*if (insuranceamount = 0) then
      begin
        name = 'insurance';
        val = 0;
        params = null;
        parent = :firstparent;
        id = :id + 1;
        suspend;
      end
      else
      begin
        name = 'insurance';
        val = 1;
        params = null;
        parent = :firstparent;
        id = :id + 1;
        suspend;

        name = 'insuranceValue';
        val = :insuranceamount;
        params = null;
        parent = :firstparent;
        id = :id + 1;
        suspend;
      end    */
      name = 'insurance';
      val = :insuranceamount;
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;


      --zwrot dokumentow
      name = 'returnDocs';
      val = 0; --TODO
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      --dostawa do 09
      name = 'deliveryBefore09';
      val = 0; --TODO
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      --dostawa do 12
      name = 'deliveryBefore12';
      val = 0; --TODO
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      --dostawa w sobote
      name = 'deliveryOnSaturday';
      val = 0; --TODO
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;

      --towary niebezpieczne
      name = 'adt';
      val = 0; --TODO
      params = null;
      parent = :firstparent;
      id = :id + 1;
      suspend;
  --KONIEC: wezel uslugi

  --START: wezel opakowan

    firstparent = :rootparent;

    for
      select o.nrpaczki, o.nropk, t.x_dhl_symbol, t.usluga,
          o.waga, o.dlugosc, o.szerokosc, o.wysokosc
        from listywysdroz_opk o
          left join typyopk t on (o.typopk = t.ref)
        where o.listwysd = :shippingdoc
          and o.rodzic is null
      into :packagenumber, :nropk, :packagesymbol, :packageservicetype,
        :packageweight, :packagelength, :packagewidth, :packageheight
    do begin
      --opakowanie
      name = 'package';
      val = null;
      --params = 'number="' || coalesce(:packagenumber,0) || '"';
      params = 'number="' || coalesce(:nropk,0) || '"';
      parent = :firstparent;
      id = :id + 1;
      suspend;

      secondparent = :id;

      -- ilosc opakowan - na razie zakladam ze 1, byc moze do zmiany jezeli beda laczone opakowania
        name = 'quantity';
        --val = :nropk;
        val = 1;
        params = null;
        parent = :secondparent;
        id = :id + 1;
        suspend;

      --typ opakowania
        name = 'symbol';
        val = :packagesymbol;
        params = null;
        parent = :secondparent;
        id = :id + 1;
        suspend;

      --typ uslugi opakowania np. schenker DB_PARCELS, DB_SYSTEM
        name = 'serviceType';
        val = :packageservicetype;
        params = null;
        parent = :secondparent;
        id = :id + 1;
        suspend;

      --waga
        name = 'weight';
        val = :packageweight;
        params = null;
        parent = :secondparent;
        id = :id + 1;
        suspend;

      --dlugosc
        name = 'length';
        val = :packagelength;
        params = null;
        parent = :secondparent;
        id = :id + 1;
        suspend;

      --szerokosc
        name = 'width';
        val = :packagewidth;
        params = null;
        parent = :secondparent;
        id = :id + 1;
        suspend;

      --szerokosc
        name = 'height';
        val = :packageheight;
        params = null;
        parent = :secondparent;
        id = :id + 1;
        suspend;
    end
  --KONIEC: wezel opakowania

  /*
    name = 'international';
    val = '1';
    params = null;
    parent = :rootparent;
    id = :id + 1;
    suspend;
  */
  --KONIEC: wezel opakowania
end^
SET TERM ; ^
