--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_TRACKING_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer,
      REF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable shippingdoc integer;
declare variable shippingdocno varchar(20);
declare variable grandparent integer;
declare variable deliverytype integer;
declare variable accesscode string80;
declare variable alltracking string10;
begin
  suspend;
end^
SET TERM ; ^
