--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_OKRESOWE(
      REF INTEGER_ID,
      LEN INTEGER_ID = 150)
  returns (
      OUT STRING1024)
   as
declare variable TMP STRING1024;
begin
  out = '';
  for
    select n.symbol||' z dnia '||cast(n.data as date)||'; '
      from pozfak p
        left join pozfakusl pu on (p.ref = pu.refpozfak)
        left join nagfak n on (pu.refnagfak = n.ref)
      where p.dokument = :ref
    into :tmp
  do begin
    if((char_length (tmp) + char_length (OUT)) > LEN) then begin
      suspend;
      OUT = tmp;
    end else begin
      out = coalesce(:out, '') ||:tmp;
    end
  end
  suspend;
end^
SET TERM ; ^
