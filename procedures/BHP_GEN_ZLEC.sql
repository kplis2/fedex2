--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BHP_GEN_ZLEC(
      BHPPLREF integer,
      REJZAM varchar(3) CHARACTER SET UTF8                           ,
      TYPZAM varchar(3) CHARACTER SET UTF8                           ,
      OPERATOR integer,
      DATEZ timestamp,
      MAGBHP varchar(3) CHARACTER SET UTF8                           )
  returns (
      NEWDOCREF integer)
   as
declare variable newposref integer;
begin
    if (exists(select ref from bhpplanpos where REF = :BHPPLREF and (nagzam is not null
            or nagzam <>''))) then
      exception test_break 'Dla generownej pozycji istnieje juz zlecenie.';
    newdocref = -1;
    execute procedure GEN_REF('NAGZAM') returning_values :newdocref;
    insert into NAGZAM(REF, REJESTR, TYPZAM, MAGAZYN, OPERATOR, datazm)
      values( :newdocref, :REJZAM, :TYPZAM, :MAGBHP, :Operator, :DATEZ);

    execute procedure GEN_REF('POZZAM') returning_values :newposref;
    insert into POZZAM(REF, ZAMOWIENIE, KTM, WERSJAREF, jedn, ILOSC)
      select :newposref, :newdocref, KTM, VERS, unit, amount
        from bhpplanpos where REF = :BHPPLREF;
    update
        BHPPLANPOS
          set nagzam = :newdocref, status = 1
            where ref = :BHPPLREF;

    suspend;
end^
SET TERM ; ^
