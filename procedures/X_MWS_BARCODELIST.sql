--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_BARCODELIST(
      BARCODE STRING100,
      TRYB INTEGER_ID = 0,
      MWSORD MWSORDS_ID = 0,
      LISTYWYSD_IN LISTYWYSD_ID = 0,
      VERS_IN WERSJE_ID = 0,
      MWSCONSTLOC MWSCONSTLOCS_ID = 0)
  returns (
      GOOD KTM_ID,
      VERS WERSJE_ID,
      QUANTITY NUMERIC_14_4,
      UNIT JEDN_MIARY,
      UNITID TOWJEDN,
      X_SERIA STRING255,
      X_PARTIA STRING20,
      X_SLOWNIK_REF INTEGER_ID,
      X_MWSACT MWSACTS_ID,
      X_LISTYWYSDPOZ LISTYWYSDPOZ_ID,
      X_SERIA_REF INTEGER_ID,
      X_MWSSTOCK MWSSTOCKS_ID)
   as
declare variable STRINGOUT STRING255;
declare variable LP SMALLINT_ID;
declare variable FLAGA STRING255;
declare variable NEXTLP SMALLINT_ID;
begin
  if (coalesce(BARCODE,'') = '' and coalesce(mwsord,0)<>0) then
  begin
     for select a.vers, a.good, o.ref, o.przelicz, o.jedn, s.serialno, a.x_slownik, a.x_partia, a.ref, s.ref
         from mwsacts a 
           left join towjedn o on (o.ktm = a.good and o.glowna = 1)
           left join x_mws_serie s on (s.ref=a.x_serial_no)
         where a.mwsord=:mwsord
           and (:VERS_IN=a.vers or :VERS_IN=0 )
           and a.status<4
         into :VERS, :GOOD, :UNITID, :QUANTITY, :UNIT, :X_SERIA, :X_SLOWNIK_REF, :X_PARTIA, :X_MWSACT, :x_seria_ref
     do begin
          suspend ;
        end 
    exit;
  end

  for
    select distinct k.wersjaref, k.ktm, o.ref, o.przelicz, o.jedn, ''  as serial, 0 as slownik, '' as partia
      from towkodkresk k
        left join towjedn o on(o.ref = k.towjednref)
      where k.kodkresk = :barcode
        and coalesce(char_length( :barcode),0) > 5
    union
    select distinct x.wersjaref, x.ktm, o.ref, o.przelicz, o.jedn, x.serialno as serial, 0 as slownik, '' as partia
      from x_mws_serie x
        left join towjedn o on (o.ktm=x.ktm and o.glowna=1)
      where upper(x.serialno) =  upper(:barcode)
        and coalesce(char_length( :barcode),0) > 5 
        and :TRYB <> 1
    union
    select distinct w.ref as wersjaref, w.ktm, o.ref, o.przelicz, o.jedn, '' as serial, 0 as slownik, '' as partia
      from wersje w
        left join towjedn o on (o.ktm = w.ktm and o.glowna = 1)
      where w.kodprod = :barcode
        and coalesce(char_length( :barcode),0) > 5
  into  :VERS, :GOOD, :UNITID, :QUANTITY, :UNIT, :X_SERIA, :X_SLOWNIK_REF, :X_PARTIA
  do begin
    if (mwsord <> 0) then
    begin
       for select mwsact, partia, seria_out, seria_ref
             from X_CHECK_VERS_IN_MWSORD(:VERS, :MWSORD, :X_SERIA)
       into :x_mwsact, :X_PARTIA, :x_seria, :X_SERIA_REF
       do suspend;
    end
    else if (listywysd_in <> 0) then
    begin
       for select POZ
             from X_CHECK_VERS_IN_LISTYWYSD(:VERS, :listywysd_in)
       into :X_listywysdpoz 
       do suspend;
    end  
    else if (MWSCONSTLOC <> 0) then
    begin
       for select X_MWSSTOCK, partia, seria_out, seria_ref
             from X_CHECK_VERS_IN_MWSCONSTLOC(:VERS, :MWSCONSTLOC)
       into :X_MWSSTOCK, :X_PARTIA, :x_seria, :X_SERIA_REF
       do
       begin
         select first 1 a.ref
           from mwsacts a
           where a.mwsconstlocp=:MWSCONSTLOC
             and a.vers = :vers
             and a.status in (1,2)
             and coalesce(a.x_partia,'') = coalesce(:x_partia,'')
             and coalesce(a.x_serial_no,'') = coalesce(:x_seria,'')
             and coalesce(a.x_slownik,0) = coalesce(:x_slownik_ref,0)
          into :x_mwsact;
         suspend;

       end
    end
    else
      suspend;
  end

  VERS=null;
  GOOD=null;
  UNITID=null;
  QUANTITY=null;
  UNIT=null;
  X_SERIA=null;
  X_SLOWNIK_REF=null;
  X_PARTIA=null;
  x_seria_ref=null;
  
  for select p.stringout,p.lp
    from parsestring(:barcode,'#') p
    into :stringout, :lp
  do begin
    if (stringout='B') then
    begin
      nextlp=lp+1;
      flaga='B';
    end
    else if (flaga='B' and nextlp=lp) then 
    begin
      X_PARTIA=stringout;
      nextlp=null;
      flaga=null;
    end

    else if (stringout='SN') then
    begin
      nextlp=lp+1;
      flaga='SN';
    end
    else if (flaga='SN' and nextlp=lp) then
    begin
      X_SERIA=stringout;
      nextlp=null;
      flaga=null;
    end

    else if (stringout='DR') then
    begin
      nextlp=lp+1;
      flaga='DR';
    end
    else if (flaga='DR' and nextlp=lp) then
    begin
      X_SLOWNIK_REF=stringout;
      nextlp=null;
      flaga=null;
    end

    else if (stringout='V') then
    begin
      nextlp=lp+1;
      flaga='V';
    end
    else if (flaga='V' and nextlp=lp) then
    begin
      select w.ref, w.ktm, o.ref, o.przelicz, o.jedn
      from wersje w
        left join towjedn o on (o.ktm = w.ktm and o.glowna = 1)
        where w.ref = :stringout
      into  :VERS, :GOOD, :UNITID, :QUANTITY, :UNIT;
      nextlp=null;
      flaga=null;
    end



  end
  if (VERS is not null) then
  begin
    if (mwsord <> 0) then
    begin
       for select mwsact, seria_ref
             from X_CHECK_VERS_IN_MWSORD(:VERS, :MWSORD, :X_SERIA)
       into :x_mwsact, :x_seria_ref
       do suspend;
    end
    else if (listywysd_in <> 0) then
    begin
       for select POZ
             from X_CHECK_VERS_IN_LISTYWYSD(:VERS, :listywysd_in)
       into :X_listywysdpoz
       do suspend;
    end
    else if (MWSCONSTLOC <> 0) then
    begin
       for select X_MWSSTOCK, seria_ref
             from X_CHECK_VERS_IN_MWSCONSTLOC(:VERS, :MWSCONSTLOC, :x_seria)
       into :X_MWSSTOCK, :X_SERIA_REF
       do
       begin

         select a.ref
           from mwsacts a
           where a.mwsconstlocp=:MWSCONSTLOC
             and a.vers = :vers
             and a.status in (1,2)
             and coalesce(a.x_partia,'') = coalesce(:x_partia,'')
             and coalesce(a.x_serial_no,'') = coalesce(:x_seria,'')
             and coalesce(a.x_slownik,0) = coalesce(:x_slownik_ref,0)
          into :x_mwsact;

         suspend;
      end
    end
    else
      suspend;
  end
end^
SET TERM ; ^
