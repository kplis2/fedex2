--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_REPLICAT_DDL(
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      CREATE_OR_ALTER smallint = 0)
  returns (
      DDL blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           )
   as
declare variable SOURCE_DBASE varchar(10);
declare variable DEST_DBASE varchar(15);
declare variable NAMEREP varchar(40);
declare variable REPLICAT_TYPE smallint;
declare variable PARAMSREP varchar(1024);
declare variable SD_TABLE varchar(30);
declare variable EI_TYPE smallint;
declare variable OVERWRITE char(1);
declare variable SD_FILEPATH varchar(255);
declare variable S_OBJ integer;
declare variable D_OBJ integer;
declare variable REF_ID integer;
declare variable STRANSBLOCK smallint;
declare variable DTRANSBLOCK smallint;
declare variable RSIGNATURE varchar(255);
declare variable PR_ITEM_REF_ID integer;
declare variable PROGRAM integer;
declare variable NUMER integer;
declare variable REPLICAT integer;
declare variable COMMITTRANSACTS smallint;
declare variable ORD smallint;
declare variable COMMITAFTEREACH smallint;
declare variable ID_OBJ integer;
declare variable ITEM_REF_ID integer;
declare variable PARENT_ID integer;
declare variable STABLE varchar(60);
declare variable FIELD varchar(20);
declare variable DEPENDS varchar(30);
declare variable SOURCE_ITEM integer;
declare variable TYP smallint;
declare variable NAME varchar(60);
declare variable TABLESET varchar(250);
declare variable MSTAFTERSET varchar(150);
declare variable MSTDOAFTER smallint;
declare variable DFCONST varchar(255);
declare variable REPKIND smallint;
declare variable DFAFTERVAL varchar(150);
declare variable DEPENDSNAME varchar(40);
declare variable DELDESTTABLE smallint;
declare variable SORDER varchar(255);
declare variable ITEMNUMER integer;
declare variable DTUPDATESQL varchar(255);
declare variable DFNOTEMPTY smallint;
declare variable DFCONDITION varchar(255);
declare variable ITEMORD smallint;
declare variable DTKEYFIELDS varchar(255);
declare variable SAFEPROCNAME varchar(32);
declare variable FROMCHARS varchar(20);
declare variable TOCHARS varchar(20);
declare variable PROCNUMBER integer;
declare variable I integer;
declare variable J integer;
declare variable POS integer;
declare variable MAXCHUNKSIZE integer;
declare variable NL varchar(2);
declare variable CH varchar(1);
declare variable sourcedbase varchar(20);
declare variable destdbase varchar(20);
begin
  nl = '
';
  FROMCHARS = 'ąęćłńóżźĄĆŁŃÓŻŹ() ';
  TOCHARS   = 'aeclnozzACLNOZZ___';

  pos = position('~', nazwa);
  if(pos>0) then
  begin
    sourcedbase = substring(nazwa from 1 for pos-1);
    nazwa = substring(nazwa from pos+1);
  end
  else exception universal 'Brak wymaganej nazwy bazy źródłowej';

  pos = position('~', nazwa);
  if(pos>0) then
  begin
    destdbase = substring(nazwa from 1 for pos-1);
    nazwa = substring(nazwa from pos+1);
  end
  else exception universal 'Brak wymaganej nazwy bazy docelowej';

  --generowanie jakiejs poprawnej nazwy procedurek transferowych
  i = 1;j = 0;
  safeprocname = 'RP_';
  while(i <= char_length(nazwa) and j < 27) do
  begin
    ch = substring(nazwa from i for 1);
    pos = position(ch, fromchars);
    if(pos>0) then
      ch = substring(tochars from pos for 1);
    if (ch similar to '[[:ALNUM:]_]') then
    begin
      safeprocname = safeprocname || ch;
      j = j + 1;
    end
    i = i + 1;
  end

  procnumber = 0;
  --uwaga, rozmiar musi być dłuższy niż dlugosc pojedynczej instrukcji dodawanej do ddl
  --inaczej numery nie beda mialy wlasnosci ciaglosci, z drugiej strony
  --podanie wartości zbyt bliskiej limitowi 65535 bajtów może spowodować przekroczenie zakresu
  --z trzeciej strony podanie zbyt malej wartosci spowoduje przekroczenie limitu 100 procedur czesciowych
  maxchunksize = 50000;

  ddl = ddl||nl||nl||'create or alter procedure '||safeprocname||(cast(procnumber as varchar(2)))||nl
  ||'as'||nl
  ||'begin';

  for
    select r.source_dbase, r.dest_dbase, r.name, r.replicat_type, r.params, r.sd_table, r.ei_type, r.overwrite,
        r.sd_filepath, r.s_obj, r.d_obj, r.ref_id, r.stransblock, r.dtransblock, r.signature
      from RP_REPLICAT r
        where r.name=:nazwa and r.source_dbase=:sourcedbase and r.dest_dbase=:destdbase
    into :source_dbase, :dest_dbase, :NAMEREP, :replicat_type, :PARAMSREP, :sd_table, :ei_type, :overwrite,
        :sd_filepath, :s_obj, :d_obj, :ref_id, :stransblock, :dtransblock, :rsignature
  do begin
    -- aktualizacja reguly replikacji
    DDL = ddl || nl || '    UPDATE OR INSERT INTO RP_REPLICAT (SOURCE_DBASE, DEST_DBASE, NAME, REPLICAT_TYPE, PARAMS, SD_TABLE, EI_TYPE, OVERWRITE, SD_FILEPATH,
    S_OBJ, D_OBJ, REF_ID, STRANSBLOCK, DTRANSBLOCK, signature)
    VALUES ('||coalesce(''''||:source_dbase||'''','null')||', '||coalesce(''''||:dest_dbase||'''','null')||', '||coalesce(''''||:NAMEREP||'''','null')||
    ', '||coalesce(:replicat_type,'null')||', '||coalesce(''''||:PARAMSREP||'''','null')||', '||coalesce(''''||:sd_table||'''','null')||', '||coalesce(:ei_type,'null')||
    ', '||coalesce(''''||:overwrite||'''','null')||', '||coalesce(''''||:sd_filepath||'''','null')||', '||coalesce(:s_obj,'null')||
    ', '||coalesce(:d_obj,'null')||', '||coalesce(:ref_id,'null')||', '||coalesce(:stransblock,'null')||', '||coalesce(:dtransblock,'null')||
    ','||coalesce(''''||:rsignature||'''','null')||') matching(SOURCE_DBASE, DEST_DBASE, NAME);';

    -- usuniecie wpisow z RP_PR_ITEM dla reguly
    ddl = ddl || nl || '    delete from RP_PR_ITEM where REPLICAT = 0'||:ref_id||';';

    -- zakladanie wpisow RP_PR_ITEM
    for
      select i.ref_id, i.program, i.numer, i.replicat, i.committransacts, i.ord, i.commitaftereach
        from RP_PR_ITEM i
          where i.replicat = :ref_id
          order by i.ref_id
      into :PR_ITEM_REF_ID, :program, :numer, :replicat, :committransacts, :ord, :commitaftereach
    do begin
      ddl = ddl || nl || '    INSERT INTO RP_PR_ITEM (REF_ID, PROGRAM, NUMER, REPLICAT, COMMITTRANSACTS, ORD, COMMITAFTEREACH)
        VALUES ('||coalesce(:PR_ITEM_REF_ID,'null')||', '||coalesce(:program,'null')||', '||coalesce(:numer,'null')||', '||coalesce(:replicat,'null')||
        ', '||coalesce(:committransacts,'null')||', '||coalesce(:ord,'null')||
        ', '||coalesce(:commitaftereach,'null')||');';

      i = div(char_length(ddl),maxchunksize);
      if(i > procnumber) then
      begin
        ddl = ddl || nl || 'end^';
        procnumber = i;
        ddl = ddl||nl||nl||'create or alter procedure '||safeprocname||(cast(procnumber as varchar(2)))||nl
              ||'as'||nl
              ||'begin';
      end
    end
    -- usuniecie RP_ITEM
    ddl = ddl || nl ||  '    delete from RP_ITEM where ID_OBJ in ('||:s_obj||','||:d_obj||');';
    -- zakladanie RP_ITEM
    for
      select i.id_obj, i.ref_id, i.parent_id, i.stable, i.field, i.depends, i.source_item, i.typ, i.name, replace(i.tableset,'''',''''''), replace(i.mstafterset,'''','''''') ,
          i.mstdoafter, i.dfconst, i.repkind, i.dfafterval, i.dependsname, i.deldesttable, i.sorder, i.numer, replace(i.dtupdatesql,'''',''''''),
          i.dfnotempty, i.dfcondition, i.ord, i.dtkeyfields
        from RP_ITEM i
          where i.id_obj in (:s_obj, :d_obj)
          order by i.id_obj, i.ref_id
      into :id_obj, :ITEM_REF_ID, :parent_id, :stable, :field, :depends, :source_item, :typ, :name, :tableset, :mstafterset,
          :mstdoafter, :dfconst, :repkind, :dfafterval, :dependsname, :deldesttable, :sorder, :ITEMnumer, :dtupdatesql,
          :dfnotempty, :dfcondition, :ITEMord, :dtkeyfields
    do begin
        
      ddl = ddl || nl || '    INSERT INTO RP_ITEM (ID_OBJ, REF_ID, PARENT_ID, STABLE, FIELD, DEPENDS, SOURCE_ITEM, TYP, NAME, TABLESET, MSTAFTERSET, MSTDOAFTER, DFCONST, REPKIND,
        DFAFTERVAL, DEPENDSNAME, DELDESTTABLE, SORDER, NUMER, DTUPDATESQL, DFNOTEMPTY, DFCONDITION, ORD, DTKEYFIELDS)
        VALUES ('||coalesce(:id_obj,'null')||', '||coalesce(:ITEM_REF_ID,'null')||', '||coalesce(:parent_id,'null')||
          ', '||coalesce(''''||:stable||'''','null')||', '||coalesce(''''||:field||'''','null')||
          ', '||coalesce(''''||:depends||'''','null')||', '||coalesce(:source_item,'null')||', '||coalesce(:typ,'null')||
          ', '||coalesce(''''||:name||'''','null')||', '||coalesce(''''||:tableset||'''','null')||
          ', '||coalesce(''''||:mstafterset||'''','null')||', '||coalesce(:mstdoafter,'null')||
          ', '||coalesce(''''||:dfconst||'''','null')||', '||coalesce(:repkind,'null')||', '||coalesce(''''||:dfafterval||'''','null')||
          ', '||coalesce(''''||:dependsname||'''','null')||', '||coalesce(:deldesttable,'null')||', '||coalesce(''''||:sorder||'''','null')||
          ', '||coalesce(:ITEMnumer,'null')||', '||coalesce(''''||:dtupdatesql||'''','null')||
          ', '||coalesce(:dfnotempty,'null')||', '||coalesce(''''||:dfcondition||'''','null')||', '||coalesce(:ITEMord,'null')||
          ', '||coalesce(''''||:dtkeyfields||'''','null')||');';

      i = div(char_length(ddl),maxchunksize);
      if(i > procnumber) then
      begin
        ddl = ddl || nl || 'end^';
        procnumber = i;
        ddl = ddl||nl||nl||'create or alter procedure '||safeprocname||(cast(procnumber as varchar(2)))||nl
              ||'as'||nl
              ||'begin';
      end
    end

    i = div(char_length(ddl),maxchunksize);
    if(i > procnumber) then
    begin
      ddl = ddl || nl || 'end^';
      procnumber = i;
      ddl = ddl||nl||nl||'create or alter procedure '||safeprocname||(cast(procnumber as varchar(2)))||nl
            ||'as'||nl
            ||'begin';
    end
  end
  ddl = ddl || nl || 'end^';


  ddl = ddl||nl||nl||'create or alter procedure '||safeprocname||nl
              ||'as'||nl
              ||'begin';
  i = 0;
  while (i<=procnumber) do
  begin
    ddl = ddl || nl || '    execute procedure '||safeprocname||(cast(i as varchar(2)))||';';
    i = i + 1;
  end
  ddl = ddl || nl || 'end^';



  ddl = ddl || nl || nl || 'execute procedure ' || safeprocname || ';' || nl || nl;

  ddl = ddl || 'drop procedure ' || safeprocname || ';';

   i = procnumber;
  while (i>=0) do
  begin
    ddl = ddl || nl || 'drop procedure '||safeprocname||(cast(i as varchar(2)))||';';
    i = i - 1;
  end

  ddl = ddl || nl || nl || 'commit work;' || nl;

  suspend;
end^
SET TERM ; ^
