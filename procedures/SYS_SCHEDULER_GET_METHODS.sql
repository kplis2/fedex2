--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_SCHEDULER_GET_METHODS(
      NEOS_NOW TIMESTAMP_ID,
      SCHEDULER_SYMBOL SYS_SCHEDULER_SYMBOL_ID = null)
  returns (
      METHOD INTEGER_ID,
      COMPILED SMALLINT_ID,
      EXEC_METHOD_BODY blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           ,
      ERROR_METHOD_BODY blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           ,
      REF SYS_SCHEDULE_ID,
      NAME STRING,
      ALIVE_TIMEOUT INTEGER_ID)
   as
declare variable NEXT_START TIMESTAMP_ID;
declare variable SCHEDULE_REF SYS_SCHEDULE_ID;
declare variable INTERVAL INTEGER_ID;
declare variable COUNT_OF_RULES INTEGER_ID;
declare variable COUNT_OF_TRUE_RULES INTEGER_ID;
declare variable AND_OR SMALLINT_ID;
declare variable RUN SMALLINT_ID;
begin
  /*Procedura przegl..da scheduler w poszukiwaniu metod do uruchomienia*/
  for
    select s.ref, s.timing, s.next_run, s.and_or, s.compiled, s.source_code,
           s.source_code_error_handling, s.name, s.alive_timeout
      from sys_schedule s
      where s.ACTIVEE = 1 and coalesce(s.scheduler_symbol, '') = coalesce(:scheduler_symbol, '')
      into :schedule_ref, :interval, :next_start, :and_or, :compiled, :exec_method_body,
           :error_method_body, :name, :alive_timeout
  do begin
    ref = schedule_ref;
    select count(1), sum(iif(((sa."TYPE" = 0) /*codziennie*/
           or(sa."TYPE" = 1 and sa.active_point = extract(weekday from: NEOS_NOW)) /*w dniu tygodnia*/
           or(sa."TYPE" = 2 and sa.active_point = extract(day from: NEOS_NOW)) /*w dniu miesiaca*/
           or(sa."TYPE" = 3 and sa.active_point = extract(month from: NEOS_NOW))) and((sa.start_point<cast(:neos_now as time)) and(sa.stop_point >= cast(:neos_now as time)) or(sa.wholeday = 1)), 1, 0))
      from sys_schedule_activity_rules sa
      where sa.sys_schedule = :schedule_ref
      into :count_of_rules, :count_of_true_rules;
    run = 0;
    if (next_start <= NEOS_NOW or next_start is null) then
      if ((and_or = 0 and count_of_true_rules > 0) or(and_or = 1 and count_of_rules = count_of_true_rules)) then
       run = 1;
    if (run = 1) then
    begin
      if (not exists(select first 1 1
                       from sys_schedule s
                       where s.ref = :schedule_ref
                         and s.lastinstance is not null
                         and coalesce(s.scheduler_symbol, '') = coalesce(:scheduler_symbol, '')
      )) then
          begin
            method = schedule_ref;
            if (:next_start is null) then
              next_start = dateadd(interval minute to NEOS_NOW);
            while (next_start<NEOS_NOW) do
              next_start = dateadd(interval minute to next_start);
    /*zaktualizuj moment nastepnego uruchomienia*/
    update sys_schedule set next_run = :next_start
      where ref = :schedule_ref;
            suspend;
          end
    end
  end
end^
SET TERM ; ^
