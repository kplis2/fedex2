--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_KSEGOWANIAREJDOK(
      ODDATY timestamp,
      DODATY timestamp,
      BKREG varchar(15) CHARACTER SET UTF8                           ,
      COMPANY integer)
  returns (
      REF integer,
      DOCREF integer,
      DOCSYMBOL varchar(30) CHARACTER SET UTF8                           ,
      TRANSDATE timestamp,
      DOCDATE timestamp,
      DOCREG varchar(10) CHARACTER SET UTF8                           ,
      NRLEDGER integer,
      NRREGISTER integer,
      DESCRIPT varchar(80) CHARACTER SET UTF8                           ,
      DEBIT numeric(15,2))
   as
DECLARE VARIABLE OLD_SYMBOL VARCHAR(3);
DECLARE VARIABLE ACCTYPE SMALLINT;
DECLARE VARIABLE I_YEARID INTEGER;
DECLARE VARIABLE OLD_ACCOUNT VARCHAR(20);
DECLARE VARIABLE S_ACCOUNT VARCHAR(20);
DECLARE VARIABLE N_DEBIT NUMERIC(15,2);
DECLARE VARIABLE N_CREDIT NUMERIC(15,2);
DECLARE VARIABLE I_REF INTEGER;
DECLARE VARIABLE S_SEP VARCHAR(1);
DECLARE VARIABLE I_DICTDEF INTEGER;
DECLARE VARIABLE I_LEN SMALLINT;
DECLARE VARIABLE S_TMP VARCHAR(20);
DECLARE VARIABLE OLD_PREFIX VARCHAR(20);
DECLARE VARIABLE I_I SMALLINT;
DECLARE VARIABLE I_J SMALLINT;
DECLARE VARIABLE I_K SMALLINT;
DECLARE VARIABLE DICTREF INTEGER;
DECLARE VARIABLE R_ACCOUNT INTEGER;
begin
  --TOFIX!!! styl jaki to wydruk?
  ref = 0;
  old_symbol = '';
  old_account = '';
  old_prefix = '';
  nrledger = 0;
  nrregister = 0;
  for select B.REF, b.TRANSDATE, b.symbol,b.number, b.bkreg, b.descript, b.docdate
    from BKDOCS B
    where  b.status > 1
       and ((:BKREG is null) or (:bkreg = '') or (B.bkreg = :bkreg))
       and b.transdate >= :oddaty and b.transdate <= :dodaty
       and b.company = :company
    order by B.transdate, B.ref
    into :DOCREF, :transdate, :docsymbol, :nrregister,:docreg,:descript, :docdate
  do begin
    debit = null;
    select sum(debit) from DECREES where BKDOC=:docref into :debit;
    ref = ref+1;
    suspend;
  end
end^
SET TERM ; ^
