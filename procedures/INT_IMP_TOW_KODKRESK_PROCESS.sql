--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_TOW_KODKRESK_PROCESS(
      SESJAREF SESJE_ID,
      TABELA TABLE_ID,
      REF INTEGER_ID,
      BLOKUJZALEZNE SMALLINT_ID = 0,
      TYLKONIEPRZETWORZONE SMALLINT_ID = 0,
      AKTUALIZUJSESJE SMALLINT_ID = 1)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable stabela table_id;
declare variable tmptabela table_id;
declare variable sprocedura string255;
declare variable szrodlo zrodla_id;
declare variable skierunek smallint_id;
declare variable sesja sesje_id;
declare variable sql memo;
declare variable tmpsql memo;
declare variable tmpstatus smallint_id;
declare variable tmpmsg string255;
declare variable error_row smallint_id;
declare variable errormsg_row string255;
declare variable tmperror smallint_id;
declare variable errormsg_all string255;
declare variable statuspoz smallint_id;
--TOWAR
declare variable T_REF INTEGER_ID;
--KOD KRESKOWY
declare variable K_REF INTEGER_ID;
declare variable K_TOWARID STRING120;
declare variable K_JEDNOSTKA STRING60;
declare variable K_JEDNOSTKAID STRING120;
declare variable K_KODKRESKOWY STRING255;
declare variable K_GLOWNY INTEGER_ID;
declare variable K_HASH STRING255;
declare variable K_DEL INTEGER_ID;
declare variable K_REC STRING255;
declare variable K_SKADTABELA STRING40;
declare variable K_SKADREF INTEGER_ID;
--zmienne ogolne
declare variable KTM KTM_ID;
declare variable WERSJAREF WERSJE_ID;
declare variable JEDN JEDN_MIARY;
declare variable TOWJEDN TOWJEDN;
declare variable TOWKODKRESK TOWKODKRESK_ID;
declare variable dataostprzetwtmp timestamp_id;
declare variable towkodkresktmp integer_id;

-- kod kreskowy oryginalny
declare variable o_kodkresk kodkreskowy;
declare variable o_gl smallint_id;
declare variable o_towjednref towjedn;
--declare variable int_zrodlo,
--declare variable int_id,
--declare variable int_dataostprzetw,
--declare variable int_sesjaostprzetw
begin
  --zmiana pustych wartosci na null-e
  if (:sesjaref = 0) then
    sesjaref = null;
  if (trim(:tabela) = '') then
    tabela = null;
  if (:ref = 0) then
    ref = null;
  if (:blokujzalezne is null) then
    blokujzalezne = 0;
  if (:tylkonieprzetworzone is null) then
    tylkonieprzetworzone = 0;
  if (:aktualizujsesje is null) then
    aktualizujsesje =1;

  status = 1;
  errormsg_all = '';


  --sprawdzenie czy wszystkie potrzebne informacje sa
  if (:sesjaref is null and :ref is null) then
  begin
    status = -1;
    msg = 'Za mało parametrow.';
    exit; --EXIT
  end

  if (:sesjaref is null) then
  begin
    if (:ref is null) then
    begin
      status = -1;
      msg = 'Jeśli nie podałes sesji to wypadałoby podać ref-a...';
      exit; --EXIT
    end
    if (:tabela is null) then
    begin
      status = -1;
      msg = 'Wypadałoby podać nazwę tabeli...';
      exit; --EXIT
    end

    sql = 'select sesja from '||:tabela||' where ref='||:ref;
    execute statement sql into :sesjaref;
  end
  else
    sesja = :sesjaref;

  if (:sesjaref is null) then
  begin
    status = -1;
    msg = 'Nie znaleziono numeru sesji.';
    exit; --EXIT
  end

  --pobranie parametrow sesji
  select s.tabela, s.procedura, s.zrodlo, s.kierunek
    from int_sesje s
    where ref = :sesja
    into :stabela, :sprocedura, :szrodlo, :skierunek;

  if (:tabela is not null) then --jesli podano tabele na wejsciu traktuje ja nadrzednie
    stabela = :tabela;

  for
    select ref, towarid, jednostka, jednostkaid, kodkreskowy, glowny,
        --"hash",
        del, rec, skadtabela, skadref
      from INT_IMP_TOW_KODYKRESKOWE k
      where sesja = :sesja and (ref = :ref or :ref is null)
    into :k_ref, :k_towarid, :k_jednostka, :k_jednostkaid, :k_kodkreskowy, :k_glowny,
      --:k_hash,
      :k_del, :k_rec, :k_skadtabela, :k_skadref
  do begin
    error_row = 0;
    errormsg_row = '';
    tmperror = null;

    t_ref = null;

    ktm = null;
    wersjaref = null;
    jedn = null;
    towjedn = null;
    towkodkresk = null;

    if (coalesce(:k_skadtabela,'') <> '' and coalesce(:k_skadref,0) > 0) then
    begin
      sql = 'select error from ' || :k_skadtabela || ' where ref = '|| :k_skadref;
      execute statement :sql into :tmperror;
    end

    if (:tmperror is null) then
      tmperror = 0;

    if (coalesce(trim(:k_kodkreskowy),'') = '') then
    begin
      error_row = 1;
      errormsg_row = substring(errormsg_row || ' ' || 'Brak kodu kreskowego.' from 1 for 255);
    end

    if (coalesce(:k_del,0) = 1 and :error_row = 0) then --rekord do skasowania
    begin
      --select status, msg from INT_IMP_TOW_KODKRESKOWY_DEL(k_kodkreskowy)
        --into :tmpstatus, :tmpmsg;
      tmpstatus = null;
    end
    else if (:error_row = 0 and coalesce(:k_del,0) = 0 and :tmperror = 0) then
    begin
      if (char_length(:k_kodkreskowy) > 20) then
      begin
        error_row = 1;
        errormsg_row = substring(errormsg_row || ' ' || 'Za długi kod kreskowy '||k_kodkreskowy||'.' from 1 for 255);
      end
  
      if (error_row = 0) then
      begin
        /*
        select w.ref, w.ktm from  towary t left join wersje w on w.ktm = t.ktm
          where t.int_id = :k_towarid and coalesce(t.int_zrodlo,0) = :szrodlo
          into :wersjaref, :ktm; --WERSJAREF KTM
        if (:wersjaref is null) then
          select w.ref, w.ktm from  towary t  join wersje w on w.ktm = t.ktm
          where t.int_id = :k_towarid
          into :wersjaref, :ktm; --WERSJAREF KTM
        if (:wersjaref is null) then
          select w.ref, w.ktm from  towary t left join wersje w on w.ktm = t.ktm
            where t.ktm = :k_towarid and coalesce(t.int_zrodlo,0) = :szrodlo
            into :wersjaref, :ktm; --WERSJAREF KTM
        */

        for
          select w.ref, w.ktm
            from  towary t
              left join wersje w on (w.ktm = t.ktm)
            where t.int_id = :k_towarid
            and t.int_zrodlo = :szrodlo
          into :wersjaref, :ktm
        do begin
        --if (:wersjaref is null) then
        --begin
        --  error_row = 1;
        --  errormsg_row = substring(errormsg_row || ' ' || 'Nie znaleziono towaru dla kodu kreskowego '||k_kodkreskowy||'.' from 1 for 255);
        --end
        --else
        -- begin
         if (coalesce(:k_jednostka,'') = '') then
            begin
              select tj.ref from towjedn tj where tj.ktm = :ktm
                and tj.glowna = 1 into :towjedn; --TOWJEDN
            end
          else
          begin

            -- XXX XX KBI Na razie tymczasowo ale moze tak zostanie ??
            if (k_jednostka is not null) then
            select sl.okey
              from INT_SLOWNIK sl
              where sl.wartoscid = :k_jednostkaid
                and sl.otable = 'MIARA'
            into :k_jednostka;
            -- XXX XX KBI


            select tj.ref from towjedn tj where tj.ktm = :ktm
              and tj.jedn = :k_jednostka into :towjedn; --TOWJEDN
            if (towjedn is null) then
              select tj.ref from towjedn tj where tj.ktm = :ktm
                and tj.int_id = :k_jednostka into :towjedn; --TOWJEDN
          end
          if (:towjedn is null) then
          begin
            error_row = 1;
            errormsg_row = substring(errormsg_row || ' ' || 'Nie znaleziono jednostki '||:k_jednostka||' dla kodu kreskowego '||k_kodkreskowy||'.' from 1 for 255);
          end

  
        if (:error_row = 0) then
        begin
          --nullowanie, zerowanie i blankowanie - kod kreskowy
          if (:k_glowny is null) then
            k_glowny = 0;


          select ref,/* wersjaref,*/ gl, towjednref
            from towkodkresk  tk
            where ktm = :ktm
              and wersjaref = :wersjaref
              --and int_zrodlo = :szrodlo
              --and int_id = :k_kodkreskowy
              and kodkresk = :k_kodkreskowy
          into :towkodkresk,/* :wersjaref,*/ :o_gl, :o_towjednref;

          if (towkodkresk is not null) then begin
            if (o_gl is distinct from k_glowny or
                o_towjednref is distinct from :towjedn) then begin

              update towkodkresk set gl = :k_glowny, towjednref = :towjedn,
                  --  int_id = :k_kodkreskowy,
                  int_dataostprzetw = current_timestamp(0), int_sesjaostprzetw = :sesja
                where ref =  :towkodkresk;

            end
          end else begin
            insert into towkodkresk (ktm, wersjaref, kodkresk, gl,
              towjednref, int_zrodlo, int_id,
              int_dataostprzetw, int_sesjaostprzetw)
            values(:ktm, :wersjaref, :k_kodkreskowy, :k_glowny,
              :towjedn, :szrodlo, :k_kodkreskowy,
              current_timestamp(0), :sesja)
            --matching(ktm, kodkresk)
            returning ref into :towkodkresk; --TOWKODKRESK            
          end

         /* update or insert into towkodkresk (ktm, wersjaref, kodkresk, gl,
              towjednref, int_zrodlo, int_id,
              int_dataostprzetw, int_sesjaostprzetw)
            values(:ktm, :wersjaref, :k_kodkreskowy, :k_glowny,
              :towjedn, :szrodlo, :k_kodkreskowy,
              current_timestamp(0), :sesja)
            matching(ktm, kodkresk)
            returning ref into :towkodkresk; --TOWKODKRESK */
            -- wersjaref = null;
             o_gl= null; o_towjednref = null;
  
          if (:k_skadtabela = 'INT_IMP_TOW_TOWARY') then
            t_ref = :k_skadref;
        end
        end
      end

      if (:wersjaref is null) then begin
          error_row = 1;
          errormsg_row = substring(errormsg_row || ' ' || 'Nie znaleziono towaru dla kodu kreskowego '||k_kodkreskowy||'.' from 1 for 255);
      end

      select dataostprzetw, towkodkresk  from int_imp_tow_kodykreskowe where ref = :k_ref into :dataostprzetwtmp, :towkodkresktmp;
      if (:towkodkresk is not null and (dataostprzetwtmp is null or towkodkresktmp is null) ) then
        update int_imp_tow_kodykreskowe set
          ktm = :ktm, wersjaref = :wersjaref, towjedn = :towjedn,
          ref_towar = :t_ref
          where ref = :k_ref and (dataostprzetw is null or towkodkresk is null)
            and :towkodkresk is not null;
      dataostprzetwtmp = null; towkodkresktmp = null;
    end
    if(error_row = 1)then
    begin
      status = -1; --ML jezeli nie przetworzy sie chociaz jeden wiersz przetwarzanie nieudane;
      errormsg_all = substring(coalesce(errormsg_all,'')||:errormsg_row from 1 for 255);
    end
    if (coalesce(error_row,0) <> 0 or coalesce(errormsg_row,'')  <> '' ) then
      update int_imp_tow_kodykreskowe
        set error = :error_row,
        errormsg = :errormsg_row,
        dataostprzetw = current_timestamp(0)
        where ref = :k_ref;
  end

  if(coalesce(status,-1) != 1) then
    msg = substring((coalesce(msg,'')||coalesce(:errormsg_all,'')) from 1 for 255);

  --aktualizacja informacji o sesji
  if (aktualizujsesje = 1) then
  begin
    select status, msg from int_sesje_aktualizuj(:sesja, :status) into :tmpstatus, :tmpmsg;
    if(coalesce(status,-1) = 1) then
      status = :tmpstatus; --ML jezeli przed aktualizacjas sesji status byl zly, to nie zmieniamy na ok
    msg = substring((msg || coalesce(:tmpmsg,'')) from 1 for 255);
  end
  suspend;
end^
SET TERM ; ^
