--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_GET_CART_INFO(
      MWSACT INTEGER_ID)
  returns (
      CARTSYMBOL STRING40,
      CARTREF INTEGER_ID,
      STATUS SMALLINT_ID,
      MSG STRING)
   as
begin
  status = 1;
  msg = '';

  select a.mwsconstloc, c.symbol
    from mwsacts a
      join mwsordcartcolours oc on (a.mwsconstloc = oc.cart and a.mwsord = oc.mwsord and a.docid = oc.docid)
      join mwsconstlocs c on (a.mwsconstloc = c.ref)
    where a.ref = :mwsact
  into :cartref,  :cartsymbol;

  if (cartref is null) then begin
    status = 0;
    msg = 'Brak przypisanego koszyka do pozycji zlecenia';
  end
end^
SET TERM ; ^
