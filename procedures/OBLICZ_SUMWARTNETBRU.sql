--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OBLICZ_SUMWARTNETBRU(
      TYPDOK char(1) CHARACTER SET UTF8                           ,
      REFDOK integer)
  returns (
      SUMWARTNET numeric(14,2),
      SUMWARTBRU numeric(14,2))
   as
declare variable bn char(1);
declare variable typfak varchar(3);
declare variable sumcz numeric(14,2);
declare variable svat numeric(14,2);
begin
  sumwartnet = 0;
  sumwartbru = 0;
  if(:typdok = 'M') then begin
    select DOKUMNAG.bn from DOKUMNAG where DOKUMNAG.REF = :refdok into :bn;
    if(:bn is null or :bn=' ') then bn = 'N';
  end
  if(:bn is null) then execute procedure getconfig('WGCEN') returning_values :bn;
  if(:bn is null or :bn = '' or :bn=' ') then BN = 'N';
  if(:typdok = 'M') then begin
     if(:bn = 'N') then begin
       for select sum(DOKUMPOZ.wartsnetto), VAT.stawka
       from DOKUMPOZ left join VAT on (dokumpoz.gr_vat = vat.grupa)
       where DOKUMPOZ.dokument = :refdok and vat.grupa is not null
       group by VAT.stawka
       into :sumcz, :svat
       do begin
          sumwartnet = :sumwartnet + :sumcz;
          sumcz = (:sumcz *(100+:svat))/100;
          sumwartbru = :sumwartbru + :sumcz;
       end
     end else begin
       for select sum(DOKUMPOZ.wartsbrutto), VAT.stawka
       from DOKUMPOZ left join VAT on (dokumpoz.gr_vat = vat.grupa)
       where DOKUMPOZ.dokument = :refdok and vat.grupa is not null
       group by VAT.stawka
       into :sumcz, :svat
       do begin
          sumwartbru = :sumwartbru + :sumcz;
          sumcz = ((:sumcz * 100)/(100+:svat));
          sumwartnet = :sumwartnet + :sumcz;
       end
     end
  end
  suspend;
end^
SET TERM ; ^
