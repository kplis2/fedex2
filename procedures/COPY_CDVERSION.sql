--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COPY_CDVERSION(
      CDV integer,
      OKRES varchar(6) CHARACTER SET UTF8                           )
   as
declare variable CD integer;
declare variable NEWCDV integer;
declare variable NEWSA integer;
declare variable SAREF integer;
declare variable SAACCOUNT ACCOUNT_ID;
declare variable SADESCRIPT varchar(255);
declare variable SATODIST1SYMBOL SYMBOLDIST_ID;
declare variable SATODIST2SYMBOL SYMBOLDIST_ID;
declare variable SATODIST3SYMBOL SYMBOLDIST_ID;
declare variable SATODIST4SYMBOL SYMBOLDIST_ID;
declare variable SATODIST5SYMBOL SYMBOLDIST_ID;
declare variable SATODIST6SYMBOL SYMBOLDIST_ID;
declare variable SADISTSREFLECT smallint;
declare variable REV smallint;
declare variable SOURCE integer;
begin
  ----sprawdzam jaki cd

  select cd.costdistribution from cdversions cd where cd.ref = :cdv into :cd;
  select c.reverse, c.costsource from costdistribution c where ref = :cd into :rev, :source;
  ----generowanie nowej wersji
  execute procedure GEN_REF('CDVERSIONS') returning_values :newcdv;
  insert into cdversions(ref, costdistribution, fromperiod) values (:newcdv, :cd, :okres);
  ----kopiowanie kont zrodlowych i docelowych oraz kont dodatkowych
if (:source <> 1) then begin
  for select sa.account, sa.ref, sa.descript, sa.TODIST1SYMBOL,
             sa.TODIST2SYMBOL, sa.TODIST3SYMBOL , sa.TODIST4SYMBOL, sa.TODIST5SYMBOL, sa.distsreflect, sa.TODIST6SYMBOL
  from cdsourceacc sa where sa.cdversion = :cdv
  into :saaccount, :saref, :sadescript, :SATODIST1SYMBOL, :SATODIST2SYMBOL, :SATODIST3SYMBOL,
       :SATODIST4SYMBOL, :SATODIST5SYMBOL, :sadistsreflect, :satodist6symbol
  do begin
     execute procedure GEN_REF('CDSOURCEACC') returning_values :newsa;
     insert into cdsourceacc(REF, COSTDISTRIBUTION, ACCOUNT, CDVERSION, DESCRIPT, TODIST1SYMBOL,
                             TODIST2SYMBOL, TODIST3SYMBOL, TODIST4SYMBOL, TODIST5SYMBOL, distsreflect, TODIST6SYMBOL)
                 values(:newsa, :cd, :saaccount, :newcdv, :sadescript, :SATODIST1SYMBOL,
                            :SATODIST2SYMBOL, :SATODIST3SYMBOL, :SATODIST4SYMBOL,
                            :SATODIST5SYMBOL, :sadistsreflect, :SATODIST5SYMBOL);

     insert into cddestacc(COSTDISTRIBUTION, KEYACCOUNT, KEYACCOUNTALG, TOACCOUNT, TOACCOUNTALG,
                        DIVQUERY, TODIST1SYMBOL, TODIST2SYMBOL, TODIST3SYMBOL , TODIST4SYMBOL,
                        TODIST5SYMBOL, DIVRATE, CDVERSION, cdsourceacc, TODIST6SYMBOL)
       select :cd, da.KEYACCOUNT, da.KEYACCOUNTALG, da.TOACCOUNT, da.TOACCOUNTALG,
         da.DIVQUERY, da.TODIST1SYMBOL, da.TODIST2SYMBOL, da.TODIST3SYMBOL , da.TODIST4SYMBOL,
         da.TODIST5SYMBOL, da.DIVRATE, :newcdv, :newsa, da.todist6symbol
       from cddestacc da where da.cdsourceacc = :saref;

     insert into cdaddonacc(account, side, pm, descript, cdsourceacc)
       select ca.account, ca.side, ca.pm, ca.descript, :newsa
       from cdaddonacc ca where ca.cdsourceacc = :saref;
  end
end
else if (:source = 1) then begin
      insert into cddestacc(COSTDISTRIBUTION, KEYACCOUNT, KEYACCOUNTALG, TOACCOUNT, TOACCOUNTALG,
                        DIVQUERY, TODIST1SYMBOL, TODIST2SYMBOL, TODIST3SYMBOL , TODIST4SYMBOL,
                        TODIST5SYMBOL, DIVRATE, CDVERSION, cdsourceacc, TODIST6SYMBOL)
       select :cd, da.KEYACCOUNT, da.KEYACCOUNTALG, da.TOACCOUNT, da.TOACCOUNTALG,
         da.DIVQUERY, da.TODIST1SYMBOL, da.TODIST2SYMBOL, da.TODIST3SYMBOL , da.TODIST4SYMBOL,
         da.TODIST5SYMBOL, da.DIVRATE, :newcdv, null, da.TODIST6SYMBOL
       from cddestacc da where da.cdversion = :cdv;
end

end^
SET TERM ; ^
