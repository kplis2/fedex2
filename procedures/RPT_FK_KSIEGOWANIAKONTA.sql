--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_KSIEGOWANIAKONTA(
      FROMPERIOD varchar(6) CHARACTER SET UTF8                           ,
      TOPERIOD varchar(6) CHARACTER SET UTF8                           ,
      BKREG varchar(10) CHARACTER SET UTF8                           ,
      ACCTYP smallint,
      ACCOUNTMASK ACCOUNT_ID,
      WITHMIDDLE smallint,
      COMPANY integer,
      MODE smallint = 0)
  returns (
      REF integer,
      DOCREF integer,
      DOCSYMBOL varchar(30) CHARACTER SET UTF8                           ,
      DOCDATE timestamp,
      TRANSDATE timestamp,
      DOCREG varchar(10) CHARACTER SET UTF8                           ,
      NRLEDGER integer,
      NRREGISTER integer,
      LP integer,
      ACCOUNT ACCOUNT_ID,
      DESCRIPT STRING1024,
      DEBIT numeric(15,2),
      CREDIT numeric(15,2),
      SDEBIT numeric(15,2),
      SCREDIT numeric(15,2),
      SETTLEMENT varchar(30) CHARACTER SET UTF8                           ,
      STLOPENDATE timestamp,
      STLPAYDATE timestamp,
      TOSUMMARY smallint,
      ACCHEAD smallint,
      BALANCETYPE SMALLINT_ID)
   as
declare variable YEARID integer;
declare variable OLD_ACCOUNT ACCOUNT_ID;
declare variable TMP_ACCOUNT ACCOUNT_ID;
declare variable BKACCOUNT_REF integer;
declare variable SEP varchar(1);
declare variable DICTDEF integer;
declare variable LEN smallint;
declare variable S_TMP ACCOUNT_ID;
declare variable I smallint;
declare variable J smallint;
declare variable K smallint;
declare variable DICTREF integer;
declare variable PERIOD varchar(6);
declare variable OLD_PERIOD varchar(6);
declare variable ODEBIT numeric(15,2);
declare variable OCREDIT numeric(15,2);
declare variable DDEBIT numeric(15,2);
declare variable DCREDIT numeric(15,2);
declare variable BDEBIT numeric(15,2);
declare variable BCREDIT numeric(15,2);
declare variable SYMBOL varchar(20);
declare variable WASMANYPERIODS smallint;
declare variable LDESCRIPT varchar(80);
declare variable TMP_DESCRIPT varchar(80);
declare variable TMP_LP integer;
declare variable tmp_BALANCETYPE smallint_id;
begin
  ref = 0;
  old_account = '';
  execute procedure create_account_mask(accountmask) returning_values accountmask;
  if (accountmask <> '') then
    accountmask = accountmask || '%';
  select yearid from BKPERIODS where company = :company and id = :fromperiod
    into :yearid;

  for
    select d.account, d.debit, d.credit, d.period, d.number, d.settlement, d.descript,
        b.docdate, b.transdate, b.bkreg, b.nlnumber, b.number, b.symbol, b.ref, d.settlement, a.saldotype
      from decrees d
        join bkdocs b on (d.bkdoc = b.ref)
        join bkaccounts a on (d.accref = a.ref)
      where D.period >= :fromperiod and D.period <= :toperiod
        and ((:accountmask is null) or (:accountmask = '') or (D.account like :accountmask))
        and ((:bkreg = '') or (d.bkreg = :bkreg))
        and D.status > 1 and D.company = :company
        and (:acctyp = 2 or (:acctyp = 0 and a.bktype < 2) or (:acctyp = 1 and (a.bktype = 2 or a.bktype = 3)))
      order by D.account, D.period, D.transdate, B.bkreg, B.number
      into :tmp_account, :ddebit, :dcredit, :period, :tmp_lp, :settlement, :tmp_descript,
        :docdate, :transdate, :docreg, :nrledger, :nrregister, :docsymbol, :docref, :settlement, :tmp_balancetype
  do begin
    lp = null;
    if (old_account <> tmp_account) then
    begin
      if (old_account <> '') then
      begin
        if (withmiddle = 1) then
        begin
          tosummary = 0;
          select DNAME from BKPERIODS where id = :old_period and company = :company
            into :descript;
          sdebit = odebit;
          scredit = ocredit;
          descript = 'za okres '||:descript;
          if(:mode = 1) then descript = 'Obroty '||:descript;
          debit = null;
          credit = null;
          ref = ref + 1;
          suspend;
        bdebit = bdebit + odebit;
        bcredit = bcredit + ocredit;
        if (:mode = 1) then begin
            tosummary = 0;
            sdebit = bdebit;
            scredit = bcredit;
            descript = 'Obroty narastająco: ';
            debit = null;
            credit = null;
            ref = ref + 1;
            suspend;
            descript = '';
          end
        end
        if (wasmanyperiods = 1 or mode = 1) then
        begin
          tosummary = 0;
          if(:mode = 0) then begin
            sdebit = bdebit;
            scredit = bcredit;
            descript = 'razem: ';
      -- dla trybu ciągłego wyliczamy persaldo
          end else if(:mode = 1) then begin
            execute procedure FK_FUN_GETBALANCE(:balancetype, :bdebit, :bcredit) returning_values sdebit, scredit;
            descript = 'Persaldo '||coalesce(:account,'');
          end
          debit = null;
          credit = null;
          ref = ref + 1;
          suspend;
        end
      end

      old_account = tmp_account;
      debit = null;
      credit = null;
      sdebit = null;
      scredit = null;
      acchead = 1;
      tosummary = 0;
      symbol = substring(tmp_account from 1 for 3);
      select descript, ref
        from bkaccounts where symbol = :symbol and yearid = :yearid and company = :company
        into :descript, :bkaccount_ref;
      debit = null;
      credit = null;
      account = tmp_account;
      if (:mode = 0) then begin
        ref = ref + 1;
        suspend;
      end
     -- zliczam ilosc poziomów anlitycznych
      if (:mode = 0) then begin
        select count(*) from accstructure  where bkaccount = :bkaccount_ref
          into :i;
  
        if (i > 0) then
        begin
          j = 0;
          k = 4;
          for
            select separator, dictdef, len
              from accstructure
              where bkaccount = :bkaccount_ref
              order by nlevel
              into :sep, :dictdef, :len
          do begin
            j = j + 1;
            if (j <= i) then --czyli jeszcze nie ostatni poziom
            begin
              if (sep <> ',') then
                k = k +1;
              s_tmp = substring(tmp_account from  k for  k + len -1);
              k = k + len;
              account = '      ' || s_tmp;
              execute procedure name_from_bksymbol(company, dictdef, s_tmp)
                returning_values descript, dictref;
              ref = ref + 1;
              suspend;
            end
          end
        end
      END else if (:mode = 1) then begin
        select count(*) from accstructure  where bkaccount = :bkaccount_ref
          into :i;
        if (coalesce(i,0) > 0) then
        begin
          j = 0;
          k = 4;
          for
            select separator, dictdef, len
              from accstructure
              where bkaccount = :bkaccount_ref
              order by nlevel
              into :sep, :dictdef, :len
          do begin
            j = j + 1;
            if (j <= i) then --czyli jeszcze nie ostatni poziom
            begin
              if (sep <> ',') then
                k = k +1;
              s_tmp = substring(tmp_account from  k for  k + len -1);
              k = k + len;
              --account = '      ' || s_tmp;
              execute procedure name_from_bksymbol(company, dictdef, s_tmp)
                returning_values ldescript, dictref;
              descript = coalesce(descript,'')||' - '||:ldescript;
            end
          end
        end
        ref = ref + 1;
        suspend;
        descript = :ldescript;
      end
      debit = 0;
      credit = 0;
      bdebit = 0;
      bcredit = 0;
      odebit = 0;
      ocredit = 0;
      old_period = '';
      wasmanyperiods = 0;
      account = tmp_account;
      descript = tmp_descript;
      acchead = 0;
    end
    descript = tmp_descript;
    if (settlement <> '') then
    begin
      select dataotw, dataplat
        from rozrach where kontofk = :tmp_account and symbfak = :settlement and company = :company
        into :stlopendate, :stlpaydate;
    end
    else begin
      stlopendate = null;
      stlpaydate = null;
    end

    if (old_period <> period and old_period <> '') then
    begin
      bdebit = bdebit + odebit;
      bcredit = bcredit + ocredit;
      if (withmiddle = 1) then
      begin
        tosummary = 0;
        ldescript = :descript;
        select DNAME from BKPERIODS where id = :old_period and company = :company
          into :descript;
        sdebit = :odebit;
        scredit = :ocredit;
        descript = 'za okres '||:descript;
        if(:mode = 1) then descript = 'Obroty '||:descript;
        ref = :ref + 1;
        debit = null;
        credit = null;
        suspend;
        if (:mode = 1) then begin
            tosummary = 0;
            sdebit = bdebit;
            scredit = bcredit;
            descript = 'Obroty narastająco: ';
            debit = null;
            credit = null;
            ref = ref + 1;
            suspend;
            descript = ldescript;
          end
        descript = :ldescript;
      end
      ocredit = 0;
      odebit = 0;
      wasmanyperiods = 1;
    end
    lp = tmp_lp;
    debit = ddebit;
    credit = dcredit;
    odebit = odebit + ddebit;
    ocredit = ocredit + dcredit;
    ref = ref + 1;
    tosummary = 1;
    sdebit = odebit;
    scredit = ocredit;
    BALANCETYPE = :tmp_BALANCETYPE;
    suspend;
    old_period = period;
  end
  -- ostatnie podsumowanie
  bdebit = bdebit + odebit;
  bcredit = bcredit + ocredit;
  if (withmiddle = 1) then
  begin
    tosummary = 0;
    ldescript = :descript;
    select DNAME from BKPERIODS where ID=:old_period and company = :company into :descript;
    sdebit = :odebit;
    scredit = :ocredit;
    descript = 'za okres '||:descript;
    if(:mode = 1) then descript = 'Obroty '||:descript;
    ref = :ref + 1;
    debit = null;
    credit = null;
    lp = null;
    suspend;
  end
    if (:mode = 1) then begin
      tosummary = 0;
      sdebit = bdebit;
      scredit = bcredit;
      descript = 'Obroty narastająco: ';
      debit = null;
      credit = null;
      ref = ref + 1;
      suspend;
      descript = '';
    end
    tosummary = 0;
    if(:mode = 0) then begin
      descript = 'razem: ';
      sdebit = :bdebit;
      scredit = :bcredit;
    end else if(:mode = 1) then begin
      execute procedure FK_FUN_GETBALANCE(:balancetype,:bdebit, :bcredit) returning_values sdebit, scredit;
      descript = 'Persaldo '||coalesce(:account,'');
    end
    ref = ref + 1;
    debit = null;
    credit = null;
    lp = null;
    suspend;
    descript = :ldescript;
end^
SET TERM ; ^
