--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_SCHEDULER_METHOD_COMPILED(
      SCHEDULE SYS_SCHEDULE_ID)
   as
begin
  --Procedura sprawdza czy nie istnieje instancja klasy schedulingu
  --Jeżeli nie istnieje to zaczyna otwierać
  update sys_schedule set compiled = 1
    where ref = :schedule;
  suspend;
end^
SET TERM ; ^
