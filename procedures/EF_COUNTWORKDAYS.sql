--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_COUNTWORKDAYS(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable fromdate date;
  declare variable todate date;
begin
  --DU: personel - funkcja podaej ilosc dni roboczych dla pracownika
  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;
  select wd from ecal_work(:employee, :fromdate, :todate)
    into ret;
  suspend;
end^
SET TERM ; ^
