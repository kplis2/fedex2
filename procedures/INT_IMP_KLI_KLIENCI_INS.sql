--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_KLI_KLIENCI_INS(
      KLIENTID type of column INT_IMP_KLI_KLIENCI.KLIENTID,
      SKROT type of column INT_IMP_KLI_KLIENCI.SKROT,
      FIRMA type of column INT_IMP_KLI_KLIENCI.FIRMA,
      NAZWA type of column INT_IMP_KLI_KLIENCI.NAZWA,
      NIP type of column INT_IMP_KLI_KLIENCI.NIP,
      IMIE type of column INT_IMP_KLI_KLIENCI.IMIE,
      NAZWISKO type of column INT_IMP_KLI_KLIENCI.NAZWISKO,
      GRUPA type of column INT_IMP_KLI_KLIENCI.GRUPA,
      GRUPAID type of column INT_IMP_KLI_KLIENCI.GRUPAID,
      TYP type of column INT_IMP_KLI_KLIENCI.TYP,
      TYPID type of column INT_IMP_KLI_KLIENCI.TYPID,
      REGON type of column INT_IMP_KLI_KLIENCI.REGON,
      KRAJ type of column INT_IMP_KLI_KLIENCI.KRAJ,
      KRAJID type of column INT_IMP_KLI_KLIENCI.KRAJID,
      ULICA type of column INT_IMP_KLI_KLIENCI.ULICA,
      NRDOMU type of column INT_IMP_KLI_KLIENCI.NRDOMU,
      NRLOKALU type of column INT_IMP_KLI_KLIENCI.NRLOKALU,
      KODPOCZTOWY type of column INT_IMP_KLI_KLIENCI.KODPOCZTOWY,
      MIASTO type of column INT_IMP_KLI_KLIENCI.MIASTO,
      TELEFON type of column INT_IMP_KLI_KLIENCI.TELEFON,
      KOMORKA type of column INT_IMP_KLI_KLIENCI.KOMORKA,
      FAX type of column INT_IMP_KLI_KLIENCI.FAX,
      EMAIL type of column INT_IMP_KLI_KLIENCI.EMAIL,
      WWW type of column INT_IMP_KLI_KLIENCI.WWW,
      UWAGI type of column INT_IMP_KLI_KLIENCI.UWAGI,
      AKTYWNY type of column INT_IMP_KLI_KLIENCI.AKTYWNY,
      LIMITKREDYTOWY type of column INT_IMP_KLI_KLIENCI.LIMITKREDYTOWY,
      BEZPSPOSPLATNOSCI type of column INT_IMP_KLI_KLIENCI.BEZPSPOSPLATNOSCI,
      TERMINDNI type of column INT_IMP_KLI_KLIENCI.TERMINDNI,
      ZAPLATA type of column INT_IMP_KLI_KLIENCI.ZAPLATA,
      ZAPLATAID type of column INT_IMP_KLI_KLIENCI.ZAPLATAID,
      DOSTAWA type of column INT_IMP_KLI_KLIENCI.DOSTAWA,
      DOSTAWAID type of column INT_IMP_KLI_KLIENCI.DOSTAWAID,
      WALUTA type of column INT_IMP_KLI_KLIENCI.WALUTA,
      RABAT type of column INT_IMP_KLI_KLIENCI.RABAT,
      CENNIK type of column INT_IMP_KLI_KLIENCI.CENNIK,
      CENNIKID type of column INT_IMP_KLI_KLIENCI.CENNIKID,
      SPRZEDAWCA type of column INT_IMP_KLI_KLIENCI.SPRZEDAWCA,
      SPRZEDAWCAID type of column INT_IMP_KLI_KLIENCI.SPRZEDAWCAID,
      TYPDOKFAK type of column INT_IMP_KLI_KLIENCI.TYPDOKFAK,
      TYPDOKMAG type of column INT_IMP_KLI_KLIENCI.TYPDOKMAG,
      BANK type of column INT_IMP_KLI_KLIENCI.BANK,
      RACHUNEK type of column INT_IMP_KLI_KLIENCI.RACHUNEK,
      DOSTAWCAID type of column INT_IMP_KLI_KLIENCI.DOSTAWCAID,
      SKADTABELA type of column INT_IMP_KLI_KLIENCI.SKADTABELA,
      SKADREF type of column INT_IMP_KLI_KLIENCI.SKADREF,
      DEL type of column INT_IMP_KLI_KLIENCI.DEL,
      HASHVALUE type of column INT_IMP_KLI_KLIENCI.HASH,
      REC type of column INT_IMP_KLI_KLIENCI.REC,
      SESJA type of column INT_IMP_KLI_KLIENCI.SESJA)
  returns (
      REF type of column INT_IMP_KLI_KLIENCI.REF)
   as
begin
  --ref = 1;
  insert into int_imp_kli_klienci (klientid, skrot, firma, nazwa, nip, imie, nazwisko, grupa, grupaid, typ, typid, regon,
      kraj, krajid, ulica, nrdomu, nrlokalu, kodpocztowy, miasto, telefon, komorka, fax, email, www,
      uwagi, aktywny, limitkredytowy, bezpsposplatnosci, termindni, zaplata, zaplataid, dostawa, dostawaid,
      waluta, rabat, cennik, cennikid, sprzedawca, sprzedawcaid, typdokfak, typdokmag, bank, rachunek, dostawcaid,
      "HASH", del, rec, skadtabela, skadref, sesja)
    values (:klientid, :skrot, :firma, :nazwa, trim(:nip), :imie, :nazwisko, :grupa, :grupaid, :typ, :typid, :regon,
     :kraj, :krajid, :ulica, :nrdomu, :nrlokalu, :kodpocztowy, :miasto, :telefon, :komorka, :fax, :email, :www,
     :uwagi, :aktywny, :limitkredytowy, :bezpsposplatnosci, :termindni, :zaplata, :zaplataid, :dostawa, :dostawaid,
     :waluta, :rabat, :cennik, :cennikid, :sprzedawca, :sprzedawcaid, :typdokfak, :typdokmag, :bank, :rachunek, :dostawcaid,
     :hashvalue, :del, :rec, :skadtabela, :skadref, :sesja)
   returning ref into :ref;

end^
SET TERM ; ^
