--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHEETS_COPY(
      ORGREF integer,
      NEWWERSJA varchar(20) CHARACTER SET UTF8                           ,
      COPYSUB integer)
  returns (
      STATUS integer)
   as
declare variable newref integer;
begin
  execute procedure GEN_REF('PRSHEETS') returning_values :newref;
  insert into PRSHEETS(REF,SHTYPE,SYMBOL,SYMBOLVER,DESCRIPT,
                       KTM,WERSJA,WERSJAREF,JEDN,
                       QUANTITY,STATUS,ACCOPER)
  select :newref,SHTYPE,SYMBOL,:newwersja,DESCRIPT,
         KTM,WERSJA,WERSJAREF,JEDN,
         QUANTITY,STATUS,ACCOPER
  from PRSHEETS where REF=:orgref;
end^
SET TERM ; ^
