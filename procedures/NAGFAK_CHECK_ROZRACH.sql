--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_CHECK_ROZRACH(
      FAKTURAREF integer)
  returns (
      SALDOWNMA numeric(14,2),
      SLODEFREF integer,
      SLOPOZREF integer)
   as
DECLARE VARIABLE PLATNIK INTEGER;
DECLARE VARIABLE DOSTAWCA INTEGER;
begin
  saldownma = 0;
  select nagfak.platnik, nagfak.dostawca from nagfak
    where nagfak.ref = :fakturaref
    into :platnik, :dostawca;
  if(platnik > 0) then
    begin
      select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'DOSTAWCY' into :slodefref;
      select max(dostawcy.ref) from klienci left join dostawcy on (klienci.dostawca = dostawcy.ref)
      where klienci.ref = :platnik
        into :dostawca;
        if (:dostawca > 0) then
          begin
            slopozref = :dostawca;
            select sum(rozrach.saldowm)
              from rozrach 
              where rozrach.saldowm < 0 and  rozrach.slodef = :slodefref and rozrach.slopoz = :dostawca
              into :saldownma;
          end
    end
  else begin
    select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'KLIENCI' into :slodefref;
    select min(klienci.ref) from klienci left join dostawcy on (klienci.dostawca = dostawcy.ref)
      where klienci.dostawca = :dostawca
      into :platnik;
      if(:platnik > 0 ) then
        begin
          slopozref = :platnik;
          select sum(rozrach.saldowm)
            from rozrach
            where rozrach.saldowm > 0 and rozrach.slodef = :slodefref and rozrach.slopoz = :platnik
            into :saldownma;
        end
  end
  if(slopozref is null) then slopozref = 0;
  if(:saldownma is null) then saldownma = 0;
end^
SET TERM ; ^
