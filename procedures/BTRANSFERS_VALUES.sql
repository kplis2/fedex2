--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BTRANSFERS_VALUES(
      ACCOUNT ACCOUNT_ID,
      SETTLEMENT varchar(30) CHARACTER SET UTF8                           ,
      DICTDEF integer,
      DICTPOZ integer)
  returns (
      AMOUNTA numeric(14,2),
      AMOUNTS numeric(14,2))
   as
begin
  select sum(BTRANSFERPOS.AMOUNT) from
    BTRANSFERS join BTRANSFERPOS on (BTRANSFERPOS.BTRANSFER = BTRANSFERS.REF)
  where
    BTRANSFERS.SLODEF = :dictdef
    and BTRANSFERS.slopoz = :dictpoz
    and BTRANSFERS.status = 1
    and BTRANSFERPOS.account = :account
    and BTRANSFERPOS.settlement = :settlement
  into :amounta;
  select sum(BTRANSFERPOS.AMOUNT) from
    BTRANSFERS join BTRANSFERPOS on (BTRANSFERPOS.BTRANSFER = BTRANSFERS.REF)
  where
    BTRANSFERS.SLODEF = :dictdef
    and BTRANSFERS.slopoz = :dictpoz
    and BTRANSFERS.status = 2
    and BTRANSFERPOS.account = :account
    and BTRANSFERPOS.settlement = :settlement
  into :amounts;
  if(:amounta is null) then amounta = 0;
  if(:amounts is null) then amounts = 0;
end^
SET TERM ; ^
