--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_REALZAMPOZZAM(
      REJZAM varchar(3) CHARACTER SET UTF8                           ,
      KLIENT integer,
      DOSTAWCA integer,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      MAG2 varchar(3) CHARACTER SET UTF8                           ,
      DOKUMENT integer,
      TYP char(1) CHARACTER SET UTF8                           )
  returns (
      REF integer,
      ZAMOWIENIE integer,
      SYMBOL varchar(30) CHARACTER SET UTF8                           ,
      DATA timestamp,
      DATADOST timestamp,
      TYPP smallint,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      WERSJAREF integer,
      ILZAM numeric(14,4),
      ILZREAL numeric(14,4),
      ILDONE numeric(14,4),
      ILOSC numeric(14,4),
      ILOSCO numeric(14,4),
      DOSTAWA integer,
      NAZWAWER varchar(255) CHARACTER SET UTF8                           ,
      NAZWAT varchar(255) CHARACTER SET UTF8                           ,
      SYMBOL_DOST varchar(255) CHARACTER SET UTF8                           ,
      RMAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      DOSTAWA_SYMBOL varchar(120) CHARACTER SET UTF8                           ,
      CENA numeric(14,4),
      CENACEN numeric(14,4),
      WALCEN varchar(3) CHARACTER SET UTF8                           ,
      RABAT numeric(14,2),
      JEDN integer,
      JEDNO integer)
   as
declare variable ILOSCDOST numeric(14,4);
declare variable USLUGA smallint;
declare variable REALZAMILDOST smallint;
begin
 if(:typ = 'M') then
   select defdokummag.realzamildost
    from DOKUMNAG
    left join defdokummag on defdokummag.typ = dokumnag.typ and defdokummag.magazyn = dokumnag.magazyn
    where DOKUMNAG.ref = :dokument
    into :realzamildost;
 else if (:typ <> 'O') then
   select stantypfak.realzamildost
    from NAGFAK
    left join stantypfak on nagfak.typ = stantypfak.typfak and nagfak.stanowisko = stantypfak.stansprzed
    where REF=:dokument
    into :realzamildost;
 if(:realzamildost is null) then realzamildost = 1;
 if(:mag2 is null) then mag2 = '';
 if((:klient > 0) or (:dostawca > 0)) then
   for select POZZAM.REF, NAGZAM.REF, NAGZAM.ID, NAGZAM.DATAWE, NAGZAM.TERMDOST,NAGZAM.TYP,
             POZZAM.KTM, POZZAM.WERSJA, POZZAM.WERSJAREF,POZZAM.ILOSC, POZZAM.ILZREAL,
             iif (:typ = 'O', ILOSCM , ILOSC - ILZREAL - ILZDYSP + ILZADYSP),
             POZZAM.DOSTAWAMAG,POZZAM.MAGAZYN, POZZAM.CENANETZL, POZZAM.CENACEN, POZZAM.WALCEN, POZZAM.RABAT,
             POZZAM.jedn, POZZAM.jedno, POZZAM.ilosco
    from NAGZAM
      join POZZAM on (POZZAM.zamowienie = NAGZAM.REF and NAGZAM.rejestr = :REJZAM and ((NAGZAM.KLIENT = :klient) or (NAGZAM.DOSTAWCA = :dostawca) or (NAGZAM.FAKTURANT = :dostawca)) and NAGZAM.typ < 2)
    where (:typ in('M','O') or (NAGzam.faktura is null))--do dok. VAT doaczanie tylko pozycji niewyfakturowanych
    into :ref,:zamowienie,:symbol,:data,:datadost,:typp,:ktm,:wersja,:wersjaref,:ilzam, :ilzreal,:ilosc,:dostawa,:rmagazyn, :cena, :cenacen, :walcen, :rabat, :jedn, :jedno, :ilosco
   do begin
     if(:ilosc > 0) then begin
       select USLUGA, NAZWA, NAZWAT, SYMBOL_DOST from WERSJE where ref = :wersjaref into :usluga, :nazwawer, :nazwat, :symbol_dost;
       if(:typ = 'M') then
          select sum(ILOSC) from DOKUMPOZ where DOKUMENT = :DOKUMENT and FROMZAM = :zamowienie and FROMPOZZAM = :ref into :ildone;
       else if (:typ = 'O') then
          select sum(m.quantity) from mwsacts m where m.mwsord = :dokument and m.doctype = 'Z' and m.docid = :zamowienie and m.docposid = :ref and m.status < 6 into :ildone;
       else
          select sum(ILOSC - PILOSC) from POZFAK where DOKUMENT = :DOKUMENT and FROMZAM = :zamowienie and FROMPOZZAM = :ref into :ildone;
       if(:ildone is null) then ildone = 0;
       if(:ildone > 0) then ilosc = :ilosc - :ildone;
       if(:ilosc > 0 and :klient>0 and :usluga<>1 and :realzamildost=1) then begin
          /*badanie ilosci dostepnej*/
          execute procedure MAG_CHECK_ILOSC(:zamowienie,:rmagazyn,:ktm,:wersja,0,0) returning_values :iloscdost;
          if(:iloscdost < :ilosc) then ilosc = :iloscdost;
       end
       if(:ilosc>0) then begin
         select max(symbol) from dostawy where dostawy.ref = :dostawa into :dostawa_symbol;
         suspend;
       end
     end
   end
 else if(:magazyn <> '') then
   for select POZZAM.REF, NAGZAM.REF, NAGZAM.ID, NAGZAM.DATAWE, NAGZAM.TERMDOST,NAGZAM.TYP,
             POZZAM.KTM, POZZAM.WERSJA, POZZAM.WERSJAREF,POZZAM.ILOSC, POZZAM.ILZREAL,
             iif (:typ = 'O', ILOSCM, ILOSC - ILZREAL - ILZDYSP + ILZADYSP),
             POZZAM.DOSTAWAMAG,POZZAM.MAGAZYN, POZZAM.CENANETZL, POZZAM.CENACEN, POZZAM.WALCEN, POZZAM.RABAT, POZZAM.jedn, POZZAM.jedno, POZZAM.ilosco
    from NAGZAM
      join POZZAM on (POZZAM.zamowienie = NAGZAM.REF and NAGZAM.rejestr = :REJZAM and NAGZAM.MAGAZYN = :magazyn and (NAGZAM.mag2 = :mag2 or :mag2='') and NAGZAM.typ < 2)
    where (:typ in('M','O') or (NAGzam.faktura is null))--do dok. VAT doaczanie tylko pozycji niewyfakturowanych
   into :ref,:zamowienie,:symbol,:data,:datadost,:typp,:ktm,:wersja,:wersjaref,:ilzam, :ilzreal,:ilosc,:dostawa,:rmagazyn, :cena, :cenacen, :walcen, :rabat, :jedn, :jedno, :ilosco
   do begin
     if(:ilosc > 0) then begin
       select USLUGA, NAZWA, NAZWAT, SYMBOL_DOST from WERSJE where ref = :wersjaref into :usluga, :nazwawer, :nazwat, :symbol_dost;
       ildone = null;
       if(:typ = 'M') then
          select sum(ILOSC) from DOKUMPOZ where DOKUMENT = :DOKUMENT and FROMZAM = :zamowienie and FROMPOZZAM = :ref into :ildone;
       else if (:typ = 'O') then
          select sum(m.quantity) from mwsacts m where m.mwsord = :dokument and m.doctype = 'Z' and m.docid = :zamowienie and m.docposid = :ref into :ildone;
       else
          select sum(ILOSC - PILOSC) from POZFAK where DOKUMENT = :DOKUMENT and FROMZAM = :zamowienie and FROMPOZZAM = :ref into :ildone;
       if(:ildone is null) then ildone = 0;
       if(:ildone > 0) then ilosc = :ilosc - :ildone;
       if(:ilosc>0 and (:klient>0 or (:mag2 <> '' and :mag2 is not null)) and :usluga<>1 and :realzamildost=1) then begin
          /*badanie ilosci dostepnej*/
          execute procedure MAG_CHECK_ILOSC(:zamowienie,:rmagazyn,:ktm,:wersja,NULL,NULL) returning_values :iloscdost;
          if(:iloscdost < :ilosc) then ilosc = :iloscdost;
       end
       if(:ilosc>0) then begin
          select max(symbol) from dostawy where dostawy.ref = :dostawa into :dostawa_symbol;
          suspend;
       end
     end
   end
end^
SET TERM ; ^
