--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE UPDATE_BO_TURNOVERS(
      COMPANY integer,
      FORYEAR integer,
      ACCOUNTINGREF integer)
   as
declare variable ACCOUNT ACCOUNT_ID;
declare variable CURRENCY varchar(3);
declare variable DEBIT numeric(14,2);
declare variable CREDIT numeric(14,2);
declare variable BO_DEBIT numeric(14,2);
declare variable BO_CREDIT numeric(14,2);
declare variable ACCOUNTING_REF integer;
declare variable PERIODBO varchar(6);
declare variable NACCOUNT ACCOUNT_ID;
declare variable RES smallint;
declare variable DIST integer;
declare variable MULTIVALUEDIST smallint;
declare variable EBDEBIT numeric(14,2);
declare variable EBCREDIT numeric(14,2);
declare variable BO_EBDEBIT numeric(14,2);
declare variable BO_EBCREDIT numeric(14,2);
begin
  periodbo = cast(foryear + 1 as varchar(4)) || '00';

  select A.account, A.currency, sum(T.debit), sum(T.credit),sum(T.ebdebit), sum(T.ebcredit)
    from accounting A
      join turnovers T on (T.accounting = A.ref)
    where A.ref = :accountingref
    group by A.account, A.currency
    into :account, :currency, :debit, :credit, :ebdebit, :ebcredit;

  bo_debit = 0;
  bo_credit = 0;
  bo_ebdebit = 0;
  bo_ebcredit = 0;

  naccount = null;
  select naccount from bkaccontschng
    where oaccount = :account and yearid = :foryear
    and company = :company
    into :naccount;
  if (naccount is not null) then
    account = naccount;

  if (debit > credit) then
    bo_debit = debit - credit;
  else
    bo_credit = credit - debit;
  if (ebdebit > ebcredit) then
    bo_ebdebit = ebdebit - ebcredit;
  else
    bo_ebcredit = ebcredit - ebdebit;

  if (bo_debit <> 0 or bo_credit <> 0) then
  begin
    execute procedure check_account(company, account, foryear + 1, 1)
      returning_values res, dist, dist, dist, dist, dist, dist, multivaluedist;
    if (res <> 0) then
    begin
      accounting_ref = null;
      select ref from accounting
        where account = :account and currency = :currency and yearid = :foryear+1 and company = :company
          into :accounting_ref;
      if (accounting_ref is null) then
      begin
        execute procedure GEN_REF('ACCOUNTING') returning_values accounting_ref;
        insert into accounting (ref, yearid, account, currency, company)
          values (:accounting_ref, :foryear + 1, :account, :currency, :company);
      end
      update turnovers set debit = :bo_debit, credit = :bo_credit, ebdebit = :bo_ebdebit, ebcredit = :bo_ebcredit
        where accounting = :accounting_ref and period = :periodbo;
    end
  end else begin
    select ref from accounting
      where account = :account and currency = :currency and yearid = :foryear+1 and company = :company
      into :accounting_ref;
    if (:accounting_ref is not null) then begin
      update turnovers set debit = 0, credit = 0, ebdebit = 0, ebcredit = 0
        where accounting = :accounting_ref and period = :periodbo;
    end
  end
end^
SET TERM ; ^
