--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDZAM_OBL(
      SCHEDZAM integer)
   as
declare variable status integer;
begin
  status = 0;
  --wyliczenie statusu
  select min(status) from PRSCHEDGUIDES where PRSCHEDGUIDES.prschedzam = :schedzam into :status;
  if(:status = 0) then --sprawdzenie, czy cos nei jest wydane - wowczas jest w toku.
    if(exists( select REF from PRSCHEDGUIDES where PRSCHEDGUIDES.prschedzam = :schedzam and STATUS > 0)) then
      status = 1;
  update PRSCHEDZAM set STATUS = :status where ref=:schedzam and (status <> :status or (status is null));

end^
SET TERM ; ^
