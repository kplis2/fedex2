--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_ZASWIADCZENIE_PRZYCHOD(
      EMPL integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           )
  returns (
      PERIOD1 varchar(6) CHARACTER SET UTF8                           ,
      PERIOD2 varchar(6) CHARACTER SET UTF8                           ,
      PERIOD3 varchar(6) CHARACTER SET UTF8                           ,
      PRZYCHOD1 numeric(14,2),
      PRZYCHOD2 numeric(14,2),
      PRZYCHOD3 numeric(14,2))
   as
begin

  period1 = period;

  execute procedure e_func_periodinc(:period1, -1)
    returning_values period2;

  execute procedure e_func_periodinc(:period2, -1)
    returning_values period3;

  select sum(PP.pvalue)
    from epayrolls PR
      join eprpos PP on (PR.ref = PP.payroll)
    where PP.ecolumn = 7000 and PR.cper = :period1 and PP.employee = :empl and PR.empltype =1
    into :przychod1;

  select sum(PP.pvalue)
    from epayrolls PR
      join eprpos PP on (PR.ref = PP.payroll)
    where PP.ecolumn = 7000 and PR.cper = :period2 and PP.employee = :empl and PR.empltype =1
    into :przychod2;

  select sum(PP.pvalue)
    from epayrolls PR
      join eprpos PP on (PR.ref = PP.payroll)
    where PP.ecolumn = 7000 and PR.cper = :period3 and PP.employee = :empl and PR.empltype =1
    into :przychod3;

  przychod1 = coalesce(przychod1, 0);
  przychod2 = coalesce(przychod2, 0);
  przychod3 = coalesce(przychod3, 0);

  suspend;
end^
SET TERM ; ^
