--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_UPDATEFXDASSETS_VALDOC as
declare variable refp integer;
  declare variable periodp integer;
  declare variable datep timestamp;
begin
  for
    select FXDASSET, OPERIOD, OPERDATE
    from valdocs
    where LIQUIDATION=1
    into :refp, :periodp, :datep
  do begin
    update fxdassets set LIQUIDATIONDATE=:datep where ref=:refp;
    update fxdassets set LIQUIDATIONPERIOD=:periodp where ref=:refp;
  end
end^
SET TERM ; ^
