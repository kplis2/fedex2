--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TOWARY_OBLJEDN(
      KTM varchar(40) CHARACTER SET UTF8                           )
   as
declare variable iss smallint;
declare variable isz smallint;
declare variable isc smallint;
declare variable ds integer;
declare variable dz integer;
declare variable dc integer;
declare variable dm integer;
declare variable kodkresk varchar(20);
declare variable szybkieopk smallint;
declare variable sszybkieopk varchar(255);
begin
  iss = 0;
  isc = 0;
  isz = 0;
  ds = 0;
  dz = 0;
  dc = 0;
  dm = 0;
  execute procedure get_config('SZYBKIEOPK',0) returning_values :sszybkieopk;
  if(:sszybkieopk = '1') then szybkieopk = 1;
  else szybkieopk = 0;
  select count(*) from TOWJEDN where KTM = :ktm and S > 0 and glowna <> 1 and (:szybkieopk = 0 or  rola <> 1) into :iss;
  if(:iss > 0) then iss = 1;
  select count(*) from TOWJEDN where KTM = :ktm and Z > 0 and glowna <> 1 into :isz;
  if(:isz > 0) then isz = 1;
  select count(*) from TOWJEDN where KTM = :ktm and C > 0 and glowna <> 1 into :isc;
  if(:isc > 0) then isc = 1;
  select REF from TOWJEDN where KTM = :KTM and S = 2 into :ds;
  select REF from TOWJEDN where KTM = :KTM and Z = 2 into :dz;
  select REF from TOWJEDN where KTM = :KTM and C = 2 into :dc;
  select REF, KODKRESK from TOWJEDN where KTM = :KTM and GLOWNA = 1 into :dm, :kodkresk;
  update TOWARY set
    iss = :iss,
    isz = :isz,
    isc = :isc,
    ds = :ds,
    dz = :dz,
    dc = :dc,
    dm = :dm
  where KTM = :ktm and
   ((iss <> :iss) or (:isz <> :isz) or (:isz <> isc) or (ds <> :ds) or (dz <> :dz) or (dc <> :dc) or (dm <> :dm)
     or (kodkresk <> :kodkresk) or (kodkresk is null and :kodkresk is not null) or (kodkresk is not null and :kodkresk is null)
   );

end^
SET TERM ; ^
