--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_TABELEZALEZNE_PROCESS(
      SESJA SESJE_ID,
      TABELA STRING35,
      REF INTEGER_ID,
      ZRODLO ZRODLA_ID,
      KIERUNEK SMALLINT_ID,
      BEFOREAFTER SMALLINT_ID,
      TABELAPRZYROSTEK STRING3 = '')
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable SQL MEMO;
declare variable TMPSQL MEMO;
declare variable TMPTABELA STRING35;
declare variable TMPPROCEDURA STRING255;
declare variable TMPSTATUS SMALLINT_ID;
declare variable TMPMSG STRING255;
begin

  --przetworzenie tabel zaleznych
  tmptabela = null;
  tmpstatus = null;
  tmpmsg = null;
  tmpprocedura = null;
  status = 0;
  msg = '';

  if (ref = 0) then
    ref = null;
   TABELA = trim(TABELA);
   TABELAPRZYROSTEK = trim(TABELAPRZYROSTEK);

  --jesli trzeba mozna zrobic selecta z sesji ;-)
  if (coalesce(sesja,0) = 0 or coalesce(tabela,'') = ''
      or zrodlo is null or kierunek is null
      or beforeafter is null) then
    exception universal 'Podaj wszystkie parametry!!!';


  sql = 'select zalezna from int_tabele where tabela = '''||:tabela||
    ''' and zrodlo = '||:zrodlo||' and kierunek = '||:kierunek||
    'and beforeafter = '||:beforeafter||
    ' order by kolejnosc';

  for execute statement :sql into :tmptabela
    do begin
      tmpprocedura = null;

      --XXX JO: zmiana dotyczaca obiektow - zamowienia / dokumenty magazynowe
      if (coalesce(:tabelaprzyrostek,'') <> '') then
        tmptabela = :tmptabela||'_'||:tabelaprzyrostek;
      --XXX JO: koniec

      select first 1 procedura from int_procedury where tabela = :tmptabela
          and (zrodlo = :zrodlo or zrodlo is null) and kierunek = :kierunek
        order by zrodlo nulls last
        into :tmpprocedura;

      if (coalesce(tmpprocedura,'') <> '') then
        begin
          if (status = 0) then
            status = 1;

          tmpsql = 'select status, msg from '||:tmpprocedura||'('
            ||:sesja||', '''||:tabela||''','||coalesce(:ref,'null')||', 1, 1, 0)';
          execute statement :tmpsql into :tmpstatus, :tmpmsg;

          if(:tmpstatus is distinct from 1)then
          begin
            status = -1;
            msg = substring(:msg || ' ' || coalesce(:tmpmsg,'') from 1 for 255);
          end
        end
    end

  if (tmptabela is null) then
    begin

      sql = 'select zalezna from int_tabele where tabela = '''||:tabela||
        ''' and zrodlo is null and kierunek = '||:kierunek||
        'and beforeafter = '||:beforeafter||
        ' order by kolejnosc';

      for execute statement :sql into :tmptabela
        do begin
          tmpprocedura = null;
    
          --XXX JO: zmiana dotyczaca obiektow - zamowienia / dokumenty magazynowe
          if (coalesce(:tabelaprzyrostek,'') <> '') then
            tmptabela = :tmptabela||'_'||:tabelaprzyrostek;
          --XXX JO: koniec

          select first 1 procedura from int_procedury where tabela = :tmptabela
              and (zrodlo = :zrodlo or zrodlo is null) and kierunek = :kierunek
            order by zrodlo nulls last
            into :tmpprocedura;
    
          if (coalesce(tmpprocedura,'') <> '') then
            begin
              if (status = 0) then
                status = 1;
    
              tmpsql = 'select status, msg from '||:tmpprocedura||'('
                ||:sesja||', '''||:tabela||''','||coalesce(:ref,'null')||', 1, 1, 0)';
              execute statement :tmpsql into :tmpstatus, :tmpmsg;

              if(:tmpstatus is distinct from 1)then
              begin
                status = -1;
                msg = substring(:msg || ' ' || coalesce(:tmpmsg,'') from 1 for 255);
              end
            end
        end
    end


  suspend;
end^
SET TERM ; ^
