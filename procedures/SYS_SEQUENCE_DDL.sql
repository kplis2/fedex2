--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_SEQUENCE_DDL(
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      CREATE_OR_ALTER smallint)
  returns (
      DDL blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           )
   as
begin
  ddl = 'CREATE SEQUENCE '||:nazwa||';';
  suspend;
end^
SET TERM ; ^
