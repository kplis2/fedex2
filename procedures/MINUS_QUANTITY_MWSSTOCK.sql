--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MINUS_QUANTITY_MWSSTOCK(
      WH char(3) CHARACTER SET UTF8                           ,
      WHSEC integer,
      WHAREA integer,
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      MWSCONSTLOC integer,
      MWSPALLOC integer,
      QUANTITY numeric(14,4),
      LOT integer,
      STANCEN integer,
      X_PARTIA DATE_ID,
      X_SERIAL_NO INTEGER_ID,
      X_SLOWNIK INTEGER_ID)
   as
declare variable refmwsstock integer;
declare variable stockquantity numeric(14,4);
declare variable blocked numeric(14,4);
declare variable stocktaking integer;
begin
  if((lot is null) or (lot = 0)) then exception STCEN_DOSTAWA_NULL;
  select max(ref), sum(quantity), sum(blocked)
    from MWSSTOCK
    where wh = :wh and good =:good and vers=:vers
      and mwspalloc = :mwspalloc and lot = :lot
      and ((:x_partia is not null and x_partia = :x_partia) or  :x_partia is null)        -- XXX KBI
      and ((:x_serial_no is not null and x_serial_no = :x_serial_no) or :x_serial_no is null)  -- XXX KBI
      and (coalesce(x_slownik,0) = coalesce(:x_slownik,0))      -- XXX KBI
      and ((:mwsconstloc is not null and mwsconstloc = :mwsconstloc) or mwsconstloc is null)
      into refmwsstock, stockquantity, blocked;
  if (blocked is null) then blocked = 0;
  if (stockquantity is null) then stockquantity = 0;
  if (stockquantity - blocked < quantity) then
  begin
    select stocktaking from mwsconstlocs where ref = :mwsconstloc
      into stocktaking;
    if (stocktaking is null) then stocktaking = 0;
    if (stocktaking = 0) then
      exception NO_STOCK_ON_MWSACT 'Brak stanow na lokacji dla: '||coalesce(:good,'null')||' '||:lot;
  end
  if(refmwsstock > 0) then
    update MWSSTOCK set quantity = quantity - :quantity
      where ref = :refmwsstock;
  else if (stocktaking = 1) then
  begin
    insert into MWSSTOCK (wh, whsec, wharea, mwsconstloc, mwspalloc, vers, good, lot, quantity, stancen, x_partia, x_serial_no, x_slownik) -- XXX KBI
      values (:wh, :whsec, :wharea, :mwsconstloc, :mwspalloc, :vers, :good, :lot, -:quantity, :stancen, :x_partia, :x_serial_no, :x_slownik); -- XXX KBI
  end
  else if (stocktaking = 0) then
   exception NO_STOCK_ON_MWSACT 'Brak stanow na lokacji dla: '||coalesce(:good,'null');
end^
SET TERM ; ^
