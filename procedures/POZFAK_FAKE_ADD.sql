--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZFAK_FAKE_ADD(
      KTM KTM_ID,
      WERSJAREF WERSJE_ID,
      DOKUMENT NAGFAK_ID,
      NUMER integer,
      ILOSC ILOSCI_FAK,
      ILOSCO ILOSCI_FAK,
      ILOSCM ILOSCI_MAG,
      MAGAZYN DEFMAGAZ_ID,
      DOSTAWA DOSTAWA_ID,
      REF POZFAK_ID)
   as
declare variable kplnagref integer;
begin
  
  select first 1 ref from kplnag
    where ktm = :ktm and wersjaref = :wersjaref and akt = 1 and glowna = 1
  into :kplnagref;

  if (:kplnagref is not null) then
  begin
    insert into pozfak(dokument, numer, ord, ktm, wersjaref, ilosc, ilosco, iloscm,
        magazyn, dostawa, alttopoz, fake)
      select :dokument, :numer, 1, p.ktm, p.wersjaref, p.ilosc * :ilosc, p.ilosc * :ilosco, p.ilosc * :iloscm,
          :magazyn, :dostawa, :ref, 1
        from kplpoz p
        where p.nagkpl = :kplnagref;
  end

end^
SET TERM ; ^
