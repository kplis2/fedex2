--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SET_CUSTOMIZED(
      SECTION varchar(255) CHARACTER SET UTF8                           ,
      IDENT varchar(255) CHARACTER SET UTF8                           ,
      CUSTOMIZED integer)
   as
declare variable ref integer;
declare variable val varchar(1024);
declare variable len integer;
begin
  len = position(':' in :ident)-1;
  ident = substring(:ident from  1 for  :len);
  update s_appini set customized = :customized
    where section = :section and substring(ident from  1 for  :len) = :ident;
end^
SET TERM ; ^
