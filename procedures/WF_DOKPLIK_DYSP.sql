--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WF_DOKPLIK_DYSP(
      DOKPLIK integer)
  returns (
      DYSP smallint)
   as
declare variable wfstatus smallint;
declare variable operator integer;
declare variable segregator integer;
begin
  dysp = 0;
  select min(w.status)
    from dokzlacz z
    left join wfworkflows w on z.ztable = 'WFWORKFLOWS' and z.zdokum = w.ref
    where z.dokument = :dokplik
  into :wfstatus;

  if(wfstatus = 2) then begin
    select segregator from dokplik where ref = :dokplik into :segregator;
    execute procedure get_global_param('AKTUOPERATOR') returning_values :operator;
    if(exists(select s.ref from segregator s where s.ref = :segregator and s.wlasciciel = :operator)) then dysp = 1;
  end
  suspend;
end^
SET TERM ; ^
