--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPK_DEL(
      SLODEF integer,
      SLOPOZ integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      ILOSC numeric(14,4),
      CENAMAG numeric(14,4),
      CENAKAUCJA numeric(14,4),
      POZYCJA integer,
      NUMER integer,
      BACK smallint)
   as
declare variable opkref integer;
declare variable stan numeric(14,4);
declare variable stanopkroz integer;
begin
  if(:cenakaucja is null) then cenakaucja = 0;
  if(:back = 0) then begin
    /*najpierw proba znalezienia odpowiedniej ilosci w danej cenie
      magazynowej i kaucji, wedlug dat*/
    --if (numer is null) then                                                   --SB/KK
      select stanopkroz from dokumpoz where ref = :pozycja into :stanopkroz;  --SB
    --exception test_break :stanopkroz;
    for select STAN, ref from STANYOPK where SLODEF = :slodef and SLOPOZ = :slopoz
    and KTM = :ktm and WERSJA = :wersja and CENAMAG = :cenamag and CENASPR = :CENAKAUCJA and ZREAL = 0
    and (ref = :stanopkroz or :stanopkroz is null or :stanopkroz = 0)
    into :stan,:opkref
    do begin
      if(:ilosc > 0 and :stan >= :ilosc) then begin
        ilosc = :ilosc - :stan;
        if(:ilosc < 0) then ilosc = 0;
        if (:numer is not null) then
          update DOKUMROZ set OPKROZID = :opkref where POZYCJA=:POZYCJA and NUMER = :numer;/*podlacenie rozpiski do stanu*/
        else begin
          update dokumpoz set opkrozid = :opkref where ref = :pozycja;
          execute procedure OPK_STANYAKTUREAL(:opkref);
        end
      end
    end
    if(:ilosc > 0) then begin
      for select STAN, ref from STANYOPK where SLODEF = :slodef and SLOPOZ = :slopoz
      and KTM = :ktm and WERSJA = :wersja and CENASPR = :CENAKAUCJA and ZREAL = 0
      and (ref = :stanopkroz or :stanopkroz is null or :stanopkroz = 0)
      into :stan,:opkref
      do begin
        if(:ilosc > 0 and :stan >= :ilosc) then begin
          ilosc = :ilosc - :stan;
          if(:ilosc < 0) then ilosc = 0;
          if (:numer is not null) then
            update DOKUMROZ set OPKROZID = :opkref where POZYCJA=:POZYCJA and NUMER = :numer;/*podlacenie rozpiski do stanu*/
          else begin
            update dokumpoz set opkrozid = :opkref where ref = :pozycja;
            execute procedure OPK_STANYAKTUREAL(:opkref);
          end
        end
      end
    end
--    if(:ilosc >0) then exception STANYOPK_BRAKSTANOW;
  end else begin
    /*jesli wywolano odjecie, to zakladam, ze trzeba podliczyc na nnowo realizacje stanu
      z faktu, ze wywolano procedure wynika, ze stan realizacji sie zmienil*/
    if (:numer is not null) then
      select OPKROZID from DOKUMROZ where POZYCJA=:POZYCJA and NUMER = :numer into :opkref;
    else
      select OPKROZID from DOKUMPOZ where REF=:POZYCJA into :opkref;
    if(:opkref > 0) then
      execute procedure OPK_STANYAKTUREAL(:opkref);
  end
end^
SET TERM ; ^
