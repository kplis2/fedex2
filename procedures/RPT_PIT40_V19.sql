--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PIT40_V19(
      CURRENTCOMPANY integer,
      YEARID char(4) CHARACTER SET UTF8                           ,
      PERSON_IN integer,
      TYP smallint,
      DATA date,
      ULGA numeric(14,2),
      CORRECT smallint,
      SIGNFNAME STRING255,
      SIGNSNAME STRING255,
      EDECLREF EDECLARATIONS_ID = 0)
  returns (
      P01 STRING100,
      P02 STRING255,
      P03 integer,
      P04 char(4) CHARACTER SET UTF8                           ,
      P05 STRING20,
      P05NAME STRING255,
      P06 integer,
      P07 integer,
      P08 STRING100,
      P09 STRING100,
      P09LEN integer,
      P10 STRING10,
      P11 STRING100,
      P12 STRING100,
      P13 STRING100,
      P14 STRING100,
      P15 STRING100,
      P16 STRING100,
      P17 STRING100,
      P18 STRING100,
      P19 STRING100,
      P20 STRING100,
      P21 STRING100,
      P22 STRING100,
      P24 smallint,
      P25 numeric(14,2),
      P26 numeric(14,2),
      P27 numeric(14,2),
      P28 numeric(14,2),
      P40 numeric(14,2),
      P41 numeric(14,2),
      P42 numeric(14,2),
      P43 numeric(14,2),
      P44 numeric(14,2),
      P45 numeric(14,2),
      P46 numeric(14,2),
      P47 numeric(14,2),
      P48 numeric(14,2),
      P53 numeric(14,2),
      P54 numeric(14,2),
      P55 numeric(14,2),
      P56 numeric(14,2),
      P57 numeric(14,2),
      P62 integer,
      P63 numeric(14,2),
      P65 numeric(14,2),
      P66 numeric(14,2),
      P68 numeric(14,2),
      P70 numeric(14,2),
      P71 numeric(14,2),
      P72 numeric(14,2),
      P73 numeric(14,2),
      P74 STRING255,
      P75 STRING255,
      PERSON PERSONS_ID)
   as
declare variable AKTUOPERATOR integer;
declare variable DESCRIPT varchar(255);
declare variable ULGA_ROCZNA numeric(14,2);
declare variable TAXSCALE numeric(14,2);
declare variable MINTAX numeric(14,2);
declare variable TAMOUNT numeric(14,2);
declare variable NEXTAMOUNT numeric(14,2);
declare variable FIELD integer;
declare variable VAL STRING255;
declare variable BUSINESSACTIV smallint;
declare variable NIP STRING20;
declare variable PESEL STRING20;
begin
--Personel: procedura do generowania PIT-40 w wersji 19 i 20

  if (edeclref > 0) then
  begin
  --dane do wydruku pobierane sa z wygenerowanej e-deklaracji

    select refid, status from edeclarations where ref = :edeclref
      into p02, p03; --BS70284 

    for select field, pvalue from edeclpos
      where edeclaration = :edeclref
      into :field, :val
    do begin
      if (field = 1) then p01 = val;
      else if (field = 4) then p04 = val;
      else if (field = 5) then
      begin
        p05 = val;
        select first 1 name || ',' || address || ',' || city || ' ' || postcode
          from einternalrevs
          where code = :p05
          into :p05name;
      end
      else if (field = 6) then p06 = val;
      else if (field = 7) then p07 = val;
      else if (field = 8) then p08 = val;
      else if (field = 9) then
      begin
        p09 = val;
        p09len = char_length(replace(replace(p09,'-',''),' ',''));
      end
      else if (field = 10) then p10 = val;
      else if (field = 11) then p11 = val;
      else if (field = 12) then p12 = val;
      else if (field = 13) then  --BS70284
      begin
        select name from countries where symbol = :val
          into :p13;
        p13 = coalesce(p13,val);
      end
      else if (field = 14) then p14 = val;
      else if (field = 15) then p15 = val;
      else if (field = 16) then p16 = val;
      else if (field = 17) then p17 = val;
      else if (field = 18) then p18 = val;
      else if (field = 19) then p19 = val;
      else if (field = 20) then p20 = val;
      else if (field = 21) then p21 = val;
      else if (field = 22) then p22 = val;
      else if (field = 24) then p24 = val;
      else if (field = 25) then p25 = val;
      else if (field = 26) then p26 = val;
      else if (field = 27) then p27 = val;
      else if (field = 28) then p28 = val;
      else if (field = 40) then p40 = val;
      else if (field = 41) then p41 = val;
      else if (field = 42) then p42 = val;
      else if (field = 43) then p43 = val;
      else if (field = 44) then p44 = val;
      else if (field = 45) then p45 = val;
      else if (field = 46) then p46 = val;
      else if (field = 47) then p47 = val;
      else if (field = 48) then p48 = val;
      else if (field = 53) then p53 = val;
      else if (field = 54) then p54 = val;
      else if (field = 55) then p55 = val;
      else if (field = 56) then p56 = val;
      else if (field = 57) then p57 = val;
      else if (field = 62) then p62 = val;
      else if (field = 63) then p63 = val;
      else if (field = 65) then p65 = val;
      else if (field = 66) then p66 = val;
      else if (field = 68) then p68 = val;
      else if (field = 70) then p70 = val;
      else if (field = 71) then p71 = val;
      else if (field = 72) then p72 = val;
      else if (field = 73) then p73 = val;
      else if (field = 74) then p74 = val;
      else if (field = 75) then p75 = val;
    end
    suspend;
  end else
  begin
  --generowanie danych do wydruku

    execute procedure get_pval(cast(yearid || '-01-01' as date), currentcompany, 'ROCZNAULGA')
      returning_values :ulga_roczna;

    p04 = yearid;
    p06 = correct + 1;
    p74 = signfname;
    p75 = signsname;

    select nip, phisical, descript from e_get_platnikinfo4pit
      into :p01, :p07, :p08;

    if (person_in = 0) then person_in = null;

    for
      select distinct e.person, p.sname, p.fname, f.dateout, p.nationality, p.nip, p.pesel
        from epayrolls pr 
          join eprpos pp on (pr.ref = pp.payroll)
          join employees e on (pp.employee = e.ref)
          join persons p on (e.person = p.ref)
          left join efunc_get_format_date(p.birthdate) f on (1 = 1)
        where pp.pvalue <> 0 and pp.ecolumn in (3000, 5950)
          and pr.tper starting with :yearid
          and pr.company = :currentcompany
          and e.company = :currentcompany
          and e.person = :person_in
        into :person, :p11, :p12, :p10, :p13, :nip, :pesel
    do begin

      select sum(p.pvalue)
        from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype = 1)
          join employees e on (e.ref = p.employee)
          join ecolumns c on (p.ecolumn = c.number)
        where r.tper starting with :yearid
          and e.person = :person
          and e.company = :currentcompany
          and r.company = :currentcompany
          and ((c.number = 5950 and r.prtype = 'SOC') --(BS36243)
            or (c.cflags like '%;POD;%' and r.prtype <> 'SOC' and c.number <> 5950))
        into :p25; -- przychod pracownika

      select sum(case when p.ecolumn = 6500 then p.pvalue else 0 end)
           , sum(case when p.ecolumn in (7350, 7370) then p.pvalue else 0 end)
        from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype = 1)
          join employees e on (e.ref = p.employee)
        where p.ecolumn in (6500, 7350, 7370)
          and r.tper starting with :yearid
          and e.person = :person
          and e.company = :currentcompany
          and r.company = :currentcompany
        into :p26, :p28;  -- koszty uzyskania i zaliczka na podatek dochodowy pracownika

      p25 = coalesce(p25,0);
      p26 = coalesce(p26,0);
      p28 = coalesce(p28,0);

      p27 = p25 - p26;  -- dochod pracownika

      select sum(case when p.ecolumn = 3000 and coalesce(t.rprcosts,0) <> 50 then p.pvalue else 0 end)
           , sum(case when p.ecolumn = 6500 and coalesce(t.rprcosts,0) <> 50 then p.pvalue else 0 end)
           , sum(case when p.ecolumn in (7350, 7370) and coalesce(t.rprcosts,0) <> 50 then p.pvalue else 0 end)
           , sum(case when p.ecolumn = 3000 and coalesce(t.rprcosts,0) = 50 then p.pvalue else 0 end)
           , sum(case when p.ecolumn = 6500 and coalesce(t.rprcosts,0) = 50 then p.pvalue else 0 end)
           , sum(case when p.ecolumn in (7350, 7370) and coalesce(t.rprcosts,0) = 50 then p.pvalue else 0 end)
        from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.empltype in (2,3))
          join emplcontracts c on (c.ref = r.emplcontract)
          join employees e on (e.ref = p.employee)
          left join e_get_realprcosts4ucp(r.ref, 50, 5) t on (1 = 1)
        where p.ecolumn in (3000, 6500, 7350, 7370)
          and r.tper starting with :yearid
          and e.person = :person
          and e.company = :currentcompany
          and r.company = :currentcompany
          and r.lumpsumtax = 0
        into :p40, :p41, :p43  -- przychod, koszty, zaliczka z UCP
           , :p47, :p48, :p46;

      p42 = p40 - p41;  -- dochod dla kosztow <> 50
      p45 = p47 - p48;  -- dochod dla kosztow = 50

      p53 = p25 + coalesce(p40,0) + coalesce(p47,0) + coalesce(p44,0);
      p54 = p26 + coalesce(p41,0) + coalesce(p48,0);
      p55 = p27 + coalesce(p42, 0) + coalesce(p45, 0);
      p56 = p28 + coalesce(p43, 0) + coalesce(p46, 0);
  
      select sum(case when p.ecolumn <> 7210 then p.pvalue else 0 end)
           , sum(case when p.ecolumn = 7210 then p.pvalue else 0 end)
        from epayrolls r
          join eprpos p on (r.ref = p.payroll)
          join employees e on (e.ref = p.employee)
        where p.ecolumn in (6100, 6110, 6120, 6130, 7210)
          and r.tper starting with :yearid
          and e.person = :person
          and e.company = :currentcompany
          and r.company = :currentcompany
          and r.lumpsumtax = 0
        into :p57, :p66; --skladka na ubezpieczenie spoleczne i zdrowotne

      p57 = coalesce(p57,0);
      p66 = coalesce(p66,0);

      p62 = round(p55 - p57);  --podstawa obliczenia podatku

      --ponizszy zapis to w wersji bardziej czytelnej:
      --  p63 = 0.18 * min(p62, kwota_zmiany_skali) - ulga_roczna
      --      + suma(taxscale * (min(p62,kwota_zmiany_skali) - kwota_poczatku_skali))
      p63 = 0;

      if (mintax is null) then
        select min(taxscale) / 100.00 from etaxlevels where taxyear = :yearid
          into :mintax;
      
      select first 1 amount from etaxlevels
        where taxyear = :yearid and amount > 0
        order by taxscale
        into :tamount;
      
      if (p62 < tamount or tamount is null) then
        p63 = mintax * p62 - ulga_roczna;
      else begin
        p63 = mintax * tamount - ulga_roczna;
        for
         select amount, taxscale / 100.00 from etaxlevels
            where taxyear = :yearid and amount > 0
            order by taxscale
            into :tamount, :taxscale
        do begin
          nextamount = null;
          select first 1 amount from etaxlevels
            where taxyear = :yearid and taxscale > :taxscale * 100
            order by taxscale
            into :nextamount;
          
          if (nextamount is null or p62 < nextamount) then
            p63 = p63 + taxscale * (p62 - tamount);
          else
            p63 = p63 + taxscale * (nextamount - tamount);
        end
      end
      
      if (p63 < 0) then p63 = 0;
  
      p65 = p63;  --podatek
  
      if (p66 > p65) then p68 = p65;
      else p68 = p66; --skladki na ubezpieczenie zdrowotne możliwe do odliczenia
  
      if (ulga <> 0) then p70 = ulga;
  
      if (p70 > p65 - p68) then p70 = p65 - p68;
  
      p71 = round(p65 - p68 - coalesce(p70,0));  --podatek nalezny
  
      if (p71 > p56) then p72 = p71 - p56;
      else p72 = 0; --do zaplaty
  
      if (p56 > p71) then p73 = p56 - p71;
      else p73 = 0; --nadplata
  
   -- nullowanie zerowych wartosci:
      if (p25 = 0) then --przychod um. o prace
      begin
        p25 = null;
        if (p26 = 0) then p26 = null;
        if (p27 = 0) then p27 = null;
        if (p28 = 0) then p28 = null;
      end

      if (p40 = 0) then --przychod UCP <> 50%
      begin
        p40 = null;
        if (p41 = 0) then p41 = null;
        if (p42 = 0) then p42 = null;
        if (p43 = 0) then p43 = null;
      end

      if (p47 = 0) then --przychod UCP = 50%
      begin
        p47 = null;
        if (p48 = 0) then p48 = null;
        if (p45 = 0) then p45 = null;
        if (p46 = 0) then p46 = null;
      end

   -- pobranie informacji podatkowych (US, koszty uzyskania, itp.)
      select costs, uscode, businessactiv, taxoffice
        from e_get_perstaxinfo4pit(:yearid, :person, 0, :currentcompany) --BS44508
        into :p24, :p05, :businessactiv, p05name;

      if (businessactiv = 1) then
      begin
        p09 = replace(:nip,'-','');
        p09len = 10;
      end else
      begin
        p09 = :pesel;
        p09len = 11;
      end
      if (p24 = 0) then p24 = null;

   -- pobranie informacji o adresie podatnika
      select cpwoj, district, community, street, home_nr,local_nr, city, post_code, post
        from get_persaddress(-:person, null, 1, '102')
        into :p14, :p15, :p16, :p17, :p18, :p19, :p20, :p21, :p22;
  
      suspend;
    
      if (typ = 1) then
      begin
        execute procedure get_global_param ('AKTUOPERATOR')
          returning_values aktuoperator;
        descript = 'Przekazanie danych na formularzu PIT-40 do ' || coalesce(:p05, 'Urzędu Skarbowego');
        if (not exists (select first 1 1 from epersdatasecur
                          where person = :person and descript = :descript and regdate = :data
                            and coalesce(operator,0) = coalesce(:aktuoperator,0) and kod = 2)
        ) then
          insert into epersdatasecur (person, kod, descript, operator, regdate)
            values (:person, 2, :descript, :aktuoperator, :data);
      end
    end
  end
end^
SET TERM ; ^
