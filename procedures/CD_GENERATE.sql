--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CD_GENERATE(
      COSTDISTRIBUTION integer,
      BKDOC integer,
      DECREE integer,
      AMOUNT numeric(14,2))
   as
declare variable company integer;
declare variable period varchar(6);
declare variable cdtype smallint;
declare variable costsource smallint;
declare variable source_account ACCOUNT_ID;
declare variable cost_value numeric(14,2);
declare variable cost_valuein numeric(14,2);
declare variable from_account ACCOUNT_ID;
declare variable key_account ACCOUNT_ID;
declare variable key_account_alg varchar(1024);
declare variable to_account ACCOUNT_ID;
declare variable to_account_alg varchar(1024);
declare variable exblock varchar(1024);
declare variable key_dist1 SYMBOLDIST_ID;
declare variable key_dist2 SYMBOLDIST_ID;
declare variable key_dist3 SYMBOLDIST_ID;
declare variable key_dist4 SYMBOLDIST_ID;
declare variable key_dist5 SYMBOLDIST_ID;
declare variable key_dist6 SYMBOLDIST_ID;
declare variable key_value numeric(14,2);
declare variable sum_key_value numeric(14,2);
declare variable divkey numeric(14,6);
declare variable sum_cvalue numeric(14,2);
declare variable suma numeric(14,6);
declare variable bkdoccostdistr integer;
declare variable cdversion integer;
declare variable dist1 integer;
declare variable dist2 integer;
declare variable dist3 integer;
declare variable dist4 integer;
declare variable dist5 integer;
declare variable dist6 integer;
declare variable cdsourceacc integer;
declare variable checksum integer;
declare variable sql varchar(4098);
declare variable distsreflect smallint;
declare variable multi smallint;
declare variable result smallint;
declare variable todistsymbol1 SYMBOLDIST_ID;
declare variable todistsymbol2 SYMBOLDIST_ID;
declare variable todistsymbol3 SYMBOLDIST_ID;
declare variable todistsymbol4 SYMBOLDIST_ID;
declare variable todistsymbol5 SYMBOLDIST_ID;
declare variable todistsymbol6 SYMBOLDIST_ID;
declare variable divrate numeric(14,6);
declare variable cref integer;
begin

  select period, company from bkdocs where ref = :bkdoc
    into :period, :company;

  select c.cdtype, c.costsource, c.destaccchecksum from costdistribution c
   where c.ref = :costdistribution
    into :cdtype, :costsource, :checksum;

  select first 1 v.ref from cdversions v
   where v.costdistribution = :costdistribution
     and v.fromperiod <= :period
    order by v.fromperiod desc
     into :cdversion;
  delete from bkdoccostdistr where bkdoc = :bkdoc;



  if (cdtype = 0) then
  begin
    if (costsource = 0) then
    begin
      for
        select account
          from cdsourceacc
          where cdversion = :cdversion
          into :source_account
       do begin
        source_account = replace(source_account,  '?',  '_');
        for
          select t.account, sum(t.debit)
            from turnovers t
            where t.company = :company and t.account like :source_account
            and t.period = :period
            group by t.account
            into :from_account, :cost_value
        do begin

          for
            select keyaccount, keyaccountalg, toaccount, toaccountalg
              from cddestacc
              where cdversion = :cdversion
              into :key_account, :key_account_alg, :to_account, :to_account_alg
          do begin
            if (key_account_alg <> '') then
            begin
              exblock = 'execute block returns (key_account varchar(20))
              as
                declare variable from_account varchar(20);
              begin
                from_account = ''' || from_account || ''';
                key_account = ' || key_account_alg || ';
                suspend;
              end';
              execute statement exblock into :key_account;
            end
            key_account = replace(key_account,  '?',  '_');

            -- pobieranie wyróżników
            execute procedure check_account(:company,:source_account, cast(substring(:period from  1 for 4) as integer), 0)
              returning_values :result, :dist1, :dist2, :dist3, :dist4, :dist5, :dist6, :multi;

            select sum(d.debit)
             from distpos d
              where d.account like :key_account and d.period = :period
              and d.tempdictdef = :dist1
              and d.tempdictdef2 = :dist2
              and d.tempdictdef3 = :dist3
              and d.tempdictdef4 = :dist4
              and d.tempdictdef5 = :dist5
              and d.tempdictdef6 = :dist6
            into :sum_key_value;

          for
            select d.account, d.tempsymbol, d.tempsymbol2, d.tempsymbol3,
                 d.tempsymbol4, d.tempsymbol5 , sum(d.debit), d.tempsymbol6
             from distpos d
              where d.account like :key_account and d.period = :period
              and d.tempdictdef = :dist1
              and d.tempdictdef2 = :dist2
              and d.tempdictdef3 = :dist3
              and d.tempdictdef4 = :dist4
              and d.tempdictdef5 = :dist5
              and d.tempdictdef6 = :dist6
            group by d.account, d.tempsymbol, d.tempsymbol2, d.tempsymbol3,
                     d.tempsymbol4, d.tempsymbol5, d.tempsymbol6
            into :key_account, :key_dist1, :key_dist2, :key_dist3,
                 :key_dist4, :key_dist5, :key_value, :key_dist6
            do begin
             if (to_account_alg <> '') then
             begin
               exblock = 'execute block returns (to_account varchar(20))
               as
                 declare variable from_account varchar(20);
                 declare variable key_account varchar(20);
               begin
                 from_account = ''' || from_account || ''';
                 key_account = ''' || key_account || ''';
                 to_account = ' || to_account_alg || ';
                 suspend;
               end';
               execute statement exblock into :to_account;
             end
             to_account = replace(to_account,  '?',  '_');

             divkey = 100 * key_value / sum_key_value;
             insert into bkdoccostdistr (bkdoc, fromaccount, fromvalue, keyaccount, keyvalue, account, divkey, cvalue,
                                         dist1ddef, dist1symbol, dist2ddef, dist2symbol, dist3ddef, dist3symbol,
                                         dist4ddef, dist4symbol, dist5ddef, dist5symbol, dist6ddef, dist6symbol)
                values (:bkdoc, :from_account, :cost_value, :key_account, :key_value, :to_account, :divkey, :cost_value * :divkey / 100,
                        :dist1, :key_dist1, :dist2, :key_dist2, :dist3, :key_dist3,
                        :dist4, :key_dist4, :dist5, :key_dist5, :dist6, :key_dist6);
            end
          end
        end
      end
    end else if (costsource = 1) then
    begin
      select sum(netv) from bkvatpos where bkdoc =:bkdoc
        into :cost_value;

      for
        select keyaccount, keyaccountalg, toaccount, toaccountalg
          from cddestacc
          where cdversion = :cdversion
          into :key_account, :key_account_alg, :to_account, :to_account_alg
      do begin
        if (key_account_alg <> '') then
        begin
          exblock = 'execute block returns (key_account varchar(20))
          as
            declare variable from_account varchar(20);
          begin
            from_account = ''' || from_account || ''';
             key_account = ' || key_account_alg || ';
             suspend;
          end';
          execute statement exblock into :key_account;
        end
        key_account = replace(key_account,  '?',  '_');

        -- pobieranie wyróżników
        execute procedure check_account(:company,:to_account, cast(substring(:period from  1 for 4) as integer), 0)
          returning_values :result, :dist1, :dist2, :dist3, :dist4, :dist5, :dist6, :multi;

        select sum(d.debit)
          from distpos d
            where d.account like :key_account and d.period = :period
            and d.tempdictdef = :dist1
            and d.tempdictdef2 = :dist2
            and d.tempdictdef3 = :dist3
            and d.tempdictdef4 = :dist4
            and d.tempdictdef5 = :dist5
            and d.tempdictdef6 = :dist6
          into :sum_key_value;

        for
          select d.account, d.tempsymbol, d.tempsymbol2, d.tempsymbol3,
                 d.tempsymbol4, d.tempsymbol5 , sum(d.debit), d.tempsymbol6
            from distpos d
            where d.account like :key_account and d.period = :period
            and d.tempdictdef = :dist1
            and d.tempdictdef2 = :dist2
            and d.tempdictdef3 = :dist3
            and d.tempdictdef4 = :dist4
            and d.tempdictdef5 = :dist5
            and d.tempdictdef6 = :dist6
            group by d.account, d.tempsymbol, d.tempsymbol2, d.tempsymbol3,
                     d.tempsymbol4, d.tempsymbol5, d.tempsymbol6
            into :key_account, :key_dist1, :key_dist2, :key_dist3,
                 :key_dist4, :key_dist5, :key_value, :key_dist6
        do begin
          if (to_account_alg <> '') then
          begin
            exblock = 'execute block returns (to_account varchar(20))
            as
              declare variable from_account varchar(20);
              declare variable key_account varchar(20);
            begin
              from_account = ''' || from_account || ''';
              key_account = ''' || key_account || ''';
              to_account = ' || to_account_alg || ';
              suspend;
            end';
            execute statement exblock into :to_account;
          end
          to_account = replace(to_account,  '?',  '_');

          divkey = 100 * key_value / sum_key_value;
          insert into bkdoccostdistr (bkdoc, fromaccount, fromvalue, keyaccount, keyvalue, account, divkey, cvalue,
                                      dist1ddef, dist1symbol, dist2ddef, dist2symbol, dist3ddef, dist3symbol,
                                      dist4ddef, dist4symbol, dist5ddef, dist5symbol, dist6ddef, dist6symbol)
             values (:bkdoc, :from_account, :cost_value, :key_account, :key_value, :to_account, :divkey, :cost_value * :divkey / 100,
                     :dist1, :key_dist1, :dist2, :key_dist2, :dist3, :key_dist3,
                     :dist4, :key_dist4, :dist5, :key_dist5, :dist6, :key_dist6);
        end
      end
    end else if (costsource = 2) then
    begin
      for
        select account
          from cdsourceacc
          where cdversion = :cdversion
          into :source_account
      do begin
        source_account = replace(source_account,  '?',  '_');
        for
          select d.account, sum(d.debit)
            from decrees d
            where d.company = :company and d.account like :source_account and d.bkdoc =:bkdoc
            group by d.account
            into :from_account, :cost_value
        do begin
          if (key_account_alg <> '') then
          begin
            exblock = 'execute block returns (key_account varchar(20))
            as
              declare variable from_account varchar(20);
            begin
              from_account = ''' || from_account || ''';
               key_account = ' || key_account_alg || ';
               suspend;
            end';
            execute statement exblock into :key_account;
          end
          key_account = replace(key_account,  '?',  '_');

          -- pobieranie wyróżników
        execute procedure check_account(:company,:source_account, cast(substring(:period from  1 for 4) as integer), 0)
          returning_values :result, :dist1, :dist2, :dist3, :dist4, :dist5, :dist6, :multi;

          select sum(d.debit)
            from distpos d
              where d.account like :key_account and d.period = :period
              and d.tempdictdef = :dist1
              and d.tempdictdef2 = :dist2
              and d.tempdictdef3 = :dist3
              and d.tempdictdef4 = :dist4
              and d.tempdictdef5 = :dist5
              and d.tempdictdef6 = :dist6
            into :sum_key_value;

          for
            select d.account, d.tempsymbol, d.tempsymbol2, d.tempsymbol3,
                   d.tempsymbol4, d.tempsymbol5 , sum(d.debit) , d.tempsymbol6
              from distpos d
              where d.account like :key_account and d.period = :period
              and d.tempdictdef = :dist1
              and d.tempdictdef2 = :dist2
              and d.tempdictdef3 = :dist3
              and d.tempdictdef4 = :dist4
              and d.tempdictdef5 = :dist5
              and d.tempdictdef6 = :dist6
              group by d.account, d.tempsymbol, d.tempsymbol2, d.tempsymbol3,
                       d.tempsymbol4, d.tempsymbol5,  d.tempsymbol6
              into :key_account, :key_dist1, :key_dist2, :key_dist3,
                   :key_dist4, :key_dist5, :key_value, :key_dist6
          do begin
            if (to_account_alg <> '') then
            begin
              exblock = 'execute block returns (to_account varchar(20))
              as
                declare variable from_account varchar(20);
                declare variable key_account varchar(20);
              begin
                from_account = ''' || from_account || ''';
                key_account = ''' || key_account || ''';
                to_account = ' || to_account_alg || ';
                suspend;
              end';
              execute statement exblock into :to_account;
            end
            to_account = replace(to_account,  '?',  '_');

            divkey = 100 * key_value / sum_key_value;
            insert into bkdoccostdistr (bkdoc, fromaccount, fromvalue, keyaccount, keyvalue, account, divkey, cvalue,
                                        dist1ddef, dist1symbol, dist2ddef, dist2symbol, dist3ddef, dist3symbol,
                                        dist4ddef, dist4symbol, dist5ddef, dist5symbol, dist6ddef, dist6symbol)
               values (:bkdoc, :from_account, :cost_value, :key_account, :key_value, :to_account, :divkey, :cost_value * :divkey / 100,
                       :dist1, :key_dist1, :dist2, :key_dist2, :dist3, :key_dist3,
                       :dist4, :key_dist4, :dist5, :key_dist5, :dist6, :key_dist6);
          end
        end
      end
    end
  end else if (cdtype = 1) then --procentowo
  begin
    if (costsource = 0) then --z salda konta
    begin
      for
        select account, ref, distsreflect
          from cdsourceacc
          where cdversion = :cdversion
          into :source_account, :cdsourceacc, :DISTSREFLECT
      do begin

        select sum(c.divrate) from cddestacc c where c.cdsourceacc = :cdsourceacc into :suma;
        if (cast(:suma as numeric(14,2))<>100 and :checksum > 0) then exception test_break 'Suma procentów musi wynosić 100%';

        source_account = replace(source_account,  '?',  '_');

        if (:DISTSREFLECT = 0 or :DISTSREFLECT is null) then
          sql = 'select t.account, sum(t.debit)
            from turnovers t
            where t.company = 1 and t.account like '''||:source_account||'''
            and t.period = '''||:period||'''
            group by t.account';
        else if (:DISTSREFLECT > 0) then
        begin
         dist1 = 0;
         dist2 = 0;
         dist3 = 0;
         dist4 = 0;
         dist5 = 0;
         dist6 = 0;
         key_dist1 = '';
         key_dist2 = '';
         key_dist3 = '';
         key_dist4 = '';
         key_dist5 = '';
         key_dist6 = '';
         select c.DISTSREFLECT, c.todist1symbol, c.todist2symbol, c.todist3symbol,
                c.todist4symbol, c.todist5symbol, c.todist6symbol
           from cdsourceacc c
           where c.ref = :cdsourceacc
           into
            :DISTSREFLECT, :key_dist1, :key_dist2, :key_dist3,
              :key_dist4, :key_dist5,:key_dist6;

         -- uzupelnianie wyróżników
         execute procedure check_account(:company,:source_account, cast(substring(:period from  1 for 4) as integer), 0)
           returning_values :result, :dist1, :dist2, :dist3, :dist4, :dist5, :dist6, :multi;

         sql = 'select account, sum(debit) from distpos where account like '''||:source_account||'''';
         if (:dist1 is not null) then sql = sql||' and tempdictdef = '||:dist1||' and tempsymbol = '''||:key_dist1||'''';
         if (:dist2 is not null) then sql = sql||' and tempdictdef2 = '||:dist2||' and tempsymbol2 = '''||:key_dist2||'''';
         if (:dist3 is not null) then sql = sql||' and tempdictdef3 = '||:dist3||' and tempsymbol3 = '''||:key_dist3||'''';
         if (:dist4 is not null) then sql = sql||' and tempdictdef4 = '||:dist4||' and tempsymbol4 = '''||:key_dist4||'''';
         if (:dist5 is not null) then sql = sql||' and tempdictdef5 = '||:dist5||' and tempsymbol5 = '''||:key_dist5||'''';
         if (:dist6 is not null) then sql = sql||' and tempdictdef6 = '||:dist6||' and tempsymbol6 = '''||:key_dist6||'''';
         sql = sql||'and period = '''||:period||''' group by account';

        end

        for execute statement sql
            into :from_account, :cost_value
        do begin

         for
          select c.divrate, c.toaccount,
                 c.todist1symbol, c.todist2symbol, c.todist3symbol, c.todist4symbol, c.todist5symbol, c.todist6symbol,
                 c.ref
            from cddestacc c
            where c.cdversion = :cdversion and c.cdsourceacc = :cdsourceacc
            into :divrate, :to_account,
                 :todistsymbol1, :todistsymbol2, :todistsymbol3, :todistsymbol4, :todistsymbol5 ,:todistsymbol6,
                 :cref
          do begin

            execute procedure check_account(:company,:from_account, cast(substring(:period from  1 for 4) as integer), 0)
              returning_values :result, :dist1, :dist2, :dist3, :dist4, :dist5, :dist6, :multi;

            insert into bkdoccostdistr (bkdoc, fromaccount, fromvalue, divkey, account, cvalue,
                                      dist1ddef, dist2ddef, dist3ddef, dist4ddef, dist5ddef, dist6ddef,
                                      dist1symbol, dist2symbol, dist3symbol, dist4symbol, dist5symbol, dist6symbol,
                                      costdistribution, cddestacc, cdsourceacc)
              values(:bkdoc,:from_account, :cost_value, :divrate, :to_account, :cost_value * :divrate / 100,
                 :dist1, :dist2, :dist3, :dist4, :dist5, :dist6,
                 :todistsymbol1, :todistsymbol2, :todistsymbol3, :todistsymbol4, :todistsymbol5 ,:todistsymbol6,
                 :costdistribution, :cref, :cdsourceacc);
            divrate = null;
            to_account = null;
            todistsymbol1 = null;
            todistsymbol2 = null;
            todistsymbol3 = null;
            todistsymbol4 = null;
            todistsymbol5 = null;
            todistsymbol6 = null;
            cref = null;
            dist1 = null;
            dist2 = null;
            dist3 = null;
            dist4 = null;
            dist5 = null;
            dist6 = null;
          end

          if(checksum > 0) then begin
            select sum(cvalue) from bkdoccostdistr where bkdoc = :bkdoc
            and cdsourceacc = :cdsourceacc
            into :sum_cvalue;
             if (sum_cvalue <> cost_value) then
             begin
              select first 1 ref from bkdoccostdistr where bkdoc = :bkdoc and cdsourceacc = :cdsourceacc
              order by abs(cvalue) desc
              into :bkdoccostdistr;
              update bkdoccostdistr set cvalue = cvalue + (:cost_value - :sum_cvalue)
              where ref = :bkdoccostdistr;
            end
          end
        end
      end
    end else if (costsource = 1) then  --z pozycji VAT
    begin
      select sum(c.divrate) from cddestacc c where c.cdversion = :cdversion into :suma;
      if (:suma<>100 and :checksum > 0) then exception test_break 'Suma procentów musi wynosić 100%';

      select sum(netv) from bkvatpos where bkdoc =:bkdoc
        into :cost_value;

      for
        select c.divrate, c.toaccount,
               c.todist1symbol, c.todist2symbol, c.todist3symbol, c.todist4symbol, c.todist5symbol, c.todist6symbol,
               c.ref
          from cddestacc c
          where c.cdversion = :cdversion and c.cdsourceacc = :cdsourceacc
          into :divrate,:to_account,
               :todistsymbol1, :todistsymbol2, :todistsymbol3, :todistsymbol4, :todistsymbol5 ,:todistsymbol6,
               :cref
        do begin

          execute procedure check_account(:company,:to_account, cast(substring(:period from  1 for 4) as integer), 0)
            returning_values :result, :dist1, :dist2, :dist3, :dist4, :dist5, :dist6, :multi;

          insert into bkdoccostdistr (bkdoc, fromvalue, divkey, account, cvalue,
                                  dist1ddef, dist2ddef, dist3ddef, dist4ddef, dist5ddef, dist6ddef,
                                  dist1symbol, dist2symbol, dist3symbol, dist4symbol, dist5symbol, dist6symbol,
                                  cddestacc, cdsourceacc)
            values(:bkdoc, :cost_value, :divrate, :to_account, :cost_value * :divrate / 100,
               :dist1, :dist2, :dist3, :dist4, :dist5, :dist6,
               :todistsymbol1, :todistsymbol2, :todistsymbol3, :todistsymbol4, :todistsymbol5 ,:todistsymbol6,
               :cref, :cdsourceacc);

          divrate = null;
          to_account = null;
          todistsymbol1 = null;
          todistsymbol2 = null;
          todistsymbol3 = null;
          todistsymbol4 = null;
          todistsymbol5 = null;
          todistsymbol6 = null;
          cref = null;
          dist1 = null;
          dist2 = null;
          dist3 = null;
          dist4 = null;
          dist5 = null;
          dist6 = null;
        end

      --korygowanie bledów zaokrągleń
      if(checksum > 0) then begin
        select sum(cvalue) from bkdoccostdistr where bkdoc = :bkdoc
        and cdsourceacc = :cdsourceacc
        into :sum_cvalue;
        if (sum_cvalue <> cost_value) then
        begin
          select first 1 ref from bkdoccostdistr where bkdoc = :bkdoc and cdsourceacc = :cdsourceacc
          order by abs(cvalue) desc
          into :bkdoccostdistr;
          update bkdoccostdistr set cvalue = cvalue + (:cost_value - :sum_cvalue)
          where ref = :bkdoccostdistr;
        end
      end
    end else if (costsource = 2) then  --z dekretów ksiegowych
    begin
      for
        select account, ref, distsreflect
          from cdsourceacc
          where cdversion = :cdversion
          into :source_account, :cdsourceacc, :distsreflect
      do begin
           --  exception test_break :source_account;
        select sum(c.divrate) from cddestacc c where c.cdsourceacc = :cdsourceacc into :suma;
        if (:suma<>100 and :checksum > 0) then exception test_break 'Suma procentów musi wynosić 100%';

        source_account = replace(source_account,  '?',  '_');

        if (:DISTSREFLECT = 0 or :DISTSREFLECT is null) then
          sql = 'select d.account, sum(d.debit)
                 from decrees d
            where d.company = '||coalesce(:company,1)||' and (d.account like '''||:source_account||'''
            or ('||coalesce(:decree,0)||' >0 and d.ref='||coalesce(:decree,0)||')) and d.bkdoc ='||coalesce(:bkdoc,0)||'
            group by d.account';
        else if (:DISTSREFLECT > 0) then
        begin
         dist1 = 0;
         dist2 = 0;
         dist3 = 0;
         dist4 = 0;
         dist5 = 0;
         dist6 = 0;
         key_dist1 = '';
         key_dist2 = '';
         key_dist3 = '';
         key_dist4 = '';
         key_dist5 = '';
         key_dist6 = '';
         select c.DISTSREFLECT, c.todist1symbol, c.todist2symbol, c.todist3symbol,
                c.todist4symbol, c.todist5symbol, c.todist6symbol
           from cdsourceacc c
           where c.ref = :cdsourceacc
           into
            :DISTSREFLECT, :key_dist1, :key_dist2, :key_dist3, :key_dist4, :key_dist5, :key_dist6;

         -- uzupelnianie wyróżników
         execute procedure check_account(:company,:source_account, cast(substring(:period from  1 for 4) as integer), 0)
           returning_values :result, :dist1, :dist2, :dist3, :dist4, :dist5, :dist6, :multi;

         sql = 'select account, sum(debit) from distpos where account like '''||:source_account||'''';
         if (:dist1 is not null) then sql = sql||' and tempdictdef = '||:dist1||' and tempsymbol = '''||:key_dist1||'''';
         if (:dist2 is not null) then sql = sql||' and tempdictdef2 = '||:dist2||' and tempsymbol2 = '''||:key_dist2||'''';
         if (:dist3 is not null) then sql = sql||' and tempdictdef3 = '||:dist3||' and tempsymbol3 = '''||:key_dist3||'''';
         if (:dist4 is not null) then sql = sql||' and tempdictdef4 = '||:dist4||' and tempsymbol4 = '''||:key_dist4||'''';
         if (:dist5 is not null) then sql = sql||' and tempdictdef5 = '||:dist5||' and tempsymbol5 = '''||:key_dist5||'''';
         if (:dist6 is not null) then sql = sql||' and tempdictdef6 = '||:dist6||' and tempsymbol6 = '''||:key_dist6||'''';
         sql = sql||' and bkdoc ='||coalesce(:bkdoc,0)||' group by account';

        end

        for  execute statement sql
            into :from_account, :cost_valuein
        do begin
          if(amount is not null) then cost_value = amount;
          else cost_value = :cost_valuein;

          for
            select c.divrate, c.toaccount,
                   c.todist1symbol, c.todist2symbol, c.todist3symbol, c.todist4symbol, c.todist5symbol, c.todist6symbol,
                   c.ref
              from cddestacc c
              where c.cdversion = :cdversion and c.cdsourceacc = :cdsourceacc
              into :divrate,:to_account,
                   :todistsymbol1, :todistsymbol2, :todistsymbol3, :todistsymbol4, :todistsymbol5 ,:todistsymbol6,
                   :cref
            do begin

              execute procedure check_account(:company,:to_account, cast(substring(:period from  1 for 4) as integer), 0)
                returning_values :result, :dist1, :dist2, :dist3, :dist4, :dist5, :dist6, :multi;

              insert into bkdoccostdistr (bkdoc, fromaccount, fromvalue, divkey, account, cvalue,
                                      dist1ddef, dist2ddef, dist3ddef, dist4ddef, dist5ddef, dist6ddef,
                                      dist1symbol, dist2symbol, dist3symbol, dist4symbol, dist5symbol, dist6symbol, costdistribution,
                                      cddestacc, cdsourceacc)
                values(:bkdoc,:from_account, :cost_value, :divrate, :to_account, :cost_value * :divrate / 100,
                   (case when :todistsymbol1 <> '' or :todistsymbol1 is not null then :dist1 else null end),
                   (case when :todistsymbol2 <> '' or :todistsymbol2 is not null then :dist2 else null end),
                   (case when :todistsymbol3 <> '' or :todistsymbol3 is not null then :dist3 else null end),
                   (case when :todistsymbol4 <> '' or :todistsymbol4 is not null then :dist4 else null end),
                   (case when :todistsymbol5 <> '' or :todistsymbol5 is not null then :dist5 else null end),
                   (case when :todistsymbol6 <> '' or :todistsymbol6 is not null then :dist6 else null end),
                   :todistsymbol1, :todistsymbol2, :todistsymbol3, :todistsymbol4, :todistsymbol5 ,:todistsymbol6,
                   :costdistribution, :cref, :cdsourceacc);

            divrate = null;
            to_account = null;
            todistsymbol1 = null;
            todistsymbol2 = null;
            todistsymbol3 = null;
            todistsymbol4 = null;
            todistsymbol5 = null;
            todistsymbol6 = null;
            cref = null;
            dist1 = null;
            dist2 = null;
            dist3 = null;
            dist4 = null;
            dist5 = null;
            dist6 = null;
            end
          --korygowanie bledów zaokrągleń
          if(checksum > 0) then begin
            select sum(cvalue) from bkdoccostdistr where bkdoc = :bkdoc
            and cdsourceacc = :cdsourceacc
            into :sum_cvalue;
             if (sum_cvalue <> cost_value) then
             begin
              select first 1 ref from bkdoccostdistr where bkdoc = :bkdoc and cdsourceacc = :cdsourceacc
              order by abs(cvalue) desc
              into :bkdoccostdistr;
              update bkdoccostdistr set cvalue = cvalue + (:cost_value - :sum_cvalue)
              where ref = :bkdoccostdistr;
            end
          end
        end
      end
    end
  end
end^
SET TERM ; ^
