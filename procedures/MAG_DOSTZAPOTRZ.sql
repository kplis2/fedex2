--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_DOSTZAPOTRZ(
      DOSTAWCA integer,
      MAGAZYNY varchar(255) CHARACTER SET UTF8                           ,
      TYPYDOK varchar(255) CHARACTER SET UTF8                           ,
      ODDATY date,
      DODATY date,
      ZAPASDNI integer)
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      WERSJAREF integer,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      WNAZWA varchar(255) CHARACTER SET UTF8                           ,
      SZUZYCIE numeric(14,4),
      STAN numeric(14,4),
      ZABLOKOW numeric(14,4),
      ZAREZERW numeric(14,4),
      ZAMOWIONO numeric(14,4),
      PROGNOZASPRZ numeric(14,4),
      ZAPOTRZ numeric(14,4),
      ZAMOWIC numeric(14,4))
   as
DECLARE VARIABLE ILOSCWOPAK VARCHAR(20);
declare variable zuzyciedni integer;
declare variable iloscmin numeric(14,4);
declare variable paczkamin numeric(14,4);
begin
  for select max(T.KTM), max(T.NAZWA), max(T.iloscwopak), sum(S.ilosc), sum(S.zablokow), sum(S.zarezerw), sum(S.zamowiono),
    max(D.ILOSCMIN), max(D.PACZKAMIN), W.ref, max(W.nrwersji), max(W.nazwa)
    from DOSTCEN D
    join WERSJE W on (D.wersjaref = W.ref)
    join TOWARY T on (W.ktm = T.ktm)
    join STANYIL S on (D.wersjaref = S.wersjaref)
    where D.dostawca = :dostawca
    and :magazyny like '%;'||s.magazyn||';%'
    group by w.ref
    into :ktm, :nazwa, :iloscwopak, :stan, :zablokow, :zarezerw, :zamowiono,
    :iloscmin, :paczkamin, :wersjaref, :wersja, :wnazwa
  do begin
    if(:zamowiono is null) then zamowiono = 0;
    if(:zablokow is null) then zablokow = 0;
    if(:zarezerw is null) then zarezerw = 0;
    szuzycie = 0;
    select sum(p.ilosc*(2*p.wydania-1))
      from dokumpoz p
      join dokumnag n on (n.ref=p.dokument)
      where p.wersjaref=:wersjaref
      and n.data>=:oddaty and n.data<=:dodaty
      and n.akcept=1
      and :magazyny like '%;'||n.magazyn||';%'
      and :typydok like '%;'||n.typ||';%'
    into :szuzycie;
    if(:szuzycie is null) then szuzycie = 0;
    zuzyciedni = cast((:dodaty - :oddaty) as integer);
    if(:zuzyciedni is null) then zuzyciedni = 0;
    zuzyciedni = :zuzyciedni + 1;
    szuzycie = :szuzycie / :zuzyciedni;
    prognozasprz = :szuzycie * :zapasdni;
    zapotrz = prognozasprz - :stan - :zamowiono;
    if(:zapotrz is null) then zapotrz = 0;
    if(:zapotrz<0) then zapotrz = 0;
    if(:iloscwopak is null or :iloscwopak='') then iloscwopak = 0;
    if(:paczkamin is null) then paczkamin = 0;
    if(:iloscmin is null) then iloscmin = 0;
    if(:paczkamin=0) then paczkamin = cast(:iloscwopak as integer);
    execute procedure DIGITIZE(:zapotrz,:paczkamin,:iloscmin) returning_values :zamowic;
    suspend;
  end
end^
SET TERM ; ^
