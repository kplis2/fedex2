--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRNAGZAM_GEN(
      MODE smallint,
      PRSHEET integer,
      AMOUNT numeric(14,4),
      REJESTR varchar(10) CHARACTER SET UTF8                           ,
      NAGZAM integer,
      POZZAM integer,
      PRNAGZAM integer,
      PRPOZZAM integer,
      ADDTOZLECPROD smallint)
   as
declare variable typzam varchar(3);
declare variable quantity numeric(14,4);
declare variable prdepart varchar(20);
declare variable wersjaref integer;
declare variable zlecprodnew integer;
declare variable prpozzamnew integer;
declare variable warehouseshe varchar(3);
declare variable warehousemat varchar(3);
declare variable prshmat integer;
declare variable matgrossquantity numeric(14,4);
declare variable jedn integer;
declare variable ilosc numeric(14,4);
declare variable ildost numeric(14,4);
declare variable kilosc numeric(14,4);
declare variable kildost numeric(14,4);
declare variable cnt smallint;
declare variable dostawa integer;
declare variable cena numeric(14,2);
declare variable brakdostaw smallint;
declare variable techjednperdays varchar(255);
declare variable techjednperday numeric(14,2);
declare variable techjednquantity numeric(14,4);
declare variable batchquantity numeric(14,4);
declare variable shortage numeric(14,4);
declare variable prsheetsymbol varchar(20);
declare variable datawe timestamp;

begin
--  if(mode = 0 and exists(select * from nagzam where kpopref = :pozzam and prsheet = :prsheet)) then
--    exception prsheets_data 'Istnieją zlecenia do wybranej pozycji zamówienia.';
  if(mode = 3 and exists(select * from pozzam where zamowienie = :prnagzam and (prsheet is null or prsheet = 0) and (prpozzam is null or prpozzam = 0 or prpozzam = :prpozzam))) then
    exception prsheets_data 'Istnieją pozycje przychodowe na wybranym zleceniu produkcyjnym.';

  if(mode = 3 and prnagzam <> 0) then zlecprodnew = :prnagzam;
  if(prsheet is null or prsheet = 0) then exception PRSHEETS_GENNAGZAM 'Nie można zidentyfikować domyslnej karty techn. dla towaru.';
  select p.symbol, p.wersjaref, pt.typzam, p.warehouse, p.techjednquantity, p.quantity, p.prdepart, p.batchquantity
    from prsheets p
    left join prshtypes pt on (p.shtype = pt.symbol)
    where p.ref = :prsheet
  into :prsheetsymbol, :wersjaref, :typzam, :warehouseshe, :techjednquantity, :quantity, :prdepart, :batchquantity;
  select datawe from nagzam where ref = :nagzam into :datawe;
  if(quantity is null or (quantity = 0)) then quantity = 1;
  if(batchquantity is null or (batchquantity = 0)) then batchquantity = 1;
  if(mode = 1 and addtozlecprod = 1) then begin
    select ref from nagzam where rejestr = rejestr and prsheet = :prsheet and kpopzam = :nagzam into :zlecprodnew;
  end
  if(mode = 3 and prnagzam <> 0) then begin zlecprodnew = :prnagzam;
  end else if(mode = 1 and addtozlecprod = 1 and zlecprodnew is not null and zlecprodnew > 0) then begin
    update nagzam set kilosc = kilosc + :amount where ref = :zlecprodnew;
    exit;
  end else begin
    execute procedure GEN_REF('NAGZAM') returning_values :zlecprodnew;
    execute procedure GETCONFIG('TECHJEDNQUANTITY') returning_values :techjednperdays;
    if(techjednperdays is not null and techjednperdays <> '') then techjednperday = cast(techjednperdays as numeric(14,2));

    else techjednperday = 8;

    techjednquantity = cast((techjednquantity /batchquantity * amount) as integer);
    insert into NAGZAM(REF, REJESTR, TYPZAM, MAGAZYN, DATAZM, DATAWE, KILOSC,

                         KWERSJAREF,PRSHEET, TERMDOST, KPOPREF, KINNE, PRDEPART, KPOPZAM, TECHJEDNQUANTITY)
    values(:zlecprodnew, :rejestr, :typzam, :warehouseshe, :datawe, :datawe, :amount,
                        :wersjaref,  :prsheet, current_date, :pozzam, 1, :prdepart, :nagzam, :techjednquantity);
  end
  /*dodanie pozycji*/
  if(not exists (select PS.ref
      from PRSHMAT PS
      left join prsheets P on (ps.sheet = p.ref)
      where PS.sheet = :prsheet and PS.ktm is not null and PS.ktm <> '')
  ) then begin
    exception prsheets_error substring('Brak pozycji materiałowej dla karty '||:prsheetsymbol from 1 for 70);
  end
  for select PS.ref, ps.grossquantity, ps.warehouse, ps.wersjaref,  p.shortage
    from PRSHMAT PS
    left join prsheets P on (ps.sheet = p.ref)
    where PS.sheet = :prsheet and PS.ktm is not null and PS.ktm <> ''
        and ps.mattype < 3
    into :prshmat, :matgrossquantity, :warehousemat, :wersjaref,  :shortage
  do begin
    ilosc = (:matgrossquantity*:amount)/:quantity;
    kilosc = :matGROSSQUANTITY/:quantity;
    shortage = (100+ shortage) / 100;
    if(shortage is not null and shortage > 0) then begin
      ilosc = ilosc * shortage;
      kilosc = kilosc * shortage;
    end
/*jesli blokowac wg konkretnych dostaw*/
/*wg dostaw - wrzucic do konfiga*/
    if(2=1) then begin
      brakdostaw = 0;
      cnt = 1;
      execute procedure GEN_REF('POZZAM') returning_values :prpozzamnew;
      while (ilosc > 0 and brakdostaw = 0)  do begin
        execute procedure get_free_dostawa(:warehousemat, :wersjaref) returning_values :dostawa, :cena, :cnt;
        execute procedure mag_checkilwol(0,:warehousemat, :wersjaref, 'Z',:dostawa) returning_values ildost;
        if(ilosc <= ildost) then ildost = ilosc;
        kildost = :ildost/:quantity;
        insert into POZZAM(REF, ZAMOWIENIE, WERSJAREF, JEDN, ILOSC, KILOSC, MAGAZYN, DOSTAWAMAG, CENAMAG, STAN, PRPOZZAM, PRSHMAT)
        values(:prpozzamnew, :zlecprodnew, :WERSJAREF, :JEDN, :ildost, :kildost, :warehousemat, :dostawa, :cena, 'B', :prpozzam, :prshmat);
        ilosc = ilosc - ildost;
        if(amount > 0) then kildost = :ilosc/:amount; else kildost = 0;
        if(cnt <= 1) then brakdostaw = 1;
        execute procedure GEN_REF('POZZAM') returning_values :prpozzamnew;
      end
      if(ilosc > 0) then
      insert into POZZAM(REF, ZAMOWIENIE, WERSJAREF, JEDN, ILOSC, KILOSC, MAGAZYN, PRPOZZAM, PRSHMAT)
      values(:prpozzamnew, :zlecprodnew, :WERSJAREF, :JEDN, :ilosc, :kilosc, :warehousemat, :prpozzam, :prshmat);
    end else begin
      insert into POZZAM(REF, ZAMOWIENIE, WERSJAREF, JEDN, ILOSC, KILOSC, MAGAZYN, PRPOZZAM, PRSHMAT)
      values(:prpozzamnew, :zlecprodnew, :WERSJAREF, :JEDN, :ilosc, :kilosc, :warehousemat, :prpozzam, :prshmat);
    end
  end
end^
SET TERM ; ^
