--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRTOOLSDOCPOS_ADD(
      PRTOOLSDOCREF integer,
      TOOLTYPE varchar(20) CHARACTER SET UTF8                           ,
      TOOLSYMBOL varchar(20) CHARACTER SET UTF8                           ,
      AMOUNT numeric(14,2),
      PRICE numeric(14,2),
      INVNO varchar(20) CHARACTER SET UTF8                           )
   as
begin
  insert into prtoolsdocpos (prtoolsdoc, prtoolstype, prtool, amount, invno, price)
    values (:prtoolsdocref, :tooltype, :toolsymbol, abs(:amount), :invno, :price);
end^
SET TERM ; ^
