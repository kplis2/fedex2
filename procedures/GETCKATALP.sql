--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GETCKATALP(
      CPODMIOT integer,
      KATEGORIA varchar(80) CHARACTER SET UTF8                           ,
      ELEMENT varchar(80) CHARACTER SET UTF8                           ,
      ATRYBUT varchar(80) CHARACTER SET UTF8                           )
  returns (
      WARTOSC varchar(255) CHARACTER SET UTF8                           )
   as
declare variable cdefkatal integer;
declare variable ckatal integer;
begin
  wartosc = '';
  select ref from cdefkatal where nazwa=:kategoria
  into :cdefkatal;
  if(:cpodmiot is not null and :cdefkatal is not null) then begin
    if(:element is not null and :element<>'') then begin
      select ref from ckatal
      where cpodmiot=:cpodmiot and cdefkatal=:cdefkatal and nazwa=:element
      into :ckatal;
    end else begin
      select ref from ckatal
      where cpodmiot=:cpodmiot and cdefkatal=:cdefkatal and numer=1
      into :ckatal;
    end
  end
  if(:ckatal is not null and atrybut is not null) then begin
    select wartosc from ckatalp
    where ckatal=:ckatal and symbol=:atrybut
    into :wartosc;
  end
  suspend;
end^
SET TERM ; ^
