--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAKZAL_PRZENIESTOWARY(
      FAKTURA integer,
      FAKTURAZAL integer,
      VAT varchar(5) CHARACTER SET UTF8                           )
  returns (
      STATUS integer)
   as
declare variable FROMBN char(1);
declare variable TOBN char(1);
declare variable STAWKAVAT numeric(14,2);
declare variable WSP numeric(14,4);
begin
  select BN from NAGFAK where REF=:FAKTURAZAL into :FROMBN;
  select BN from NAGFAK where REF=:FAKTURA into :TOBN;
  if(:FROMBN is null or (:FROMBN = '') or (:FROMBN = ' '))then FROMBN = 'N';
  if(:TOBN is null or (:TOBN = '') or (:TOBN = ' '))then TOBN = 'N';
  select VAT.STAWKA from VAT where VAT.GRUPA = :VAT into :STAWKAVAT;
  if(:STAWKAVAT is null) then STAWKAVAT = 0;
  WSP = 1;
  if(:TOBN = 'B' and :FROMBN = 'N') then begin
    WSP = (100 + :STAWKAVAT)/100;
  end else if(:TOBN = 'N' and :FROMBN = 'B')then begin
    WSP = 1.0 / ((100 + :STAWKAVAT)/100);
  end

  insert into POZFAK(DOKUMENT,KTM,WERSJAREF,DOSTAWA,
         ILOSC, ILOSCO, ILOSCM,
         JEDN, JEDNO,
         CENACEN, WALCEN,
         RABAT,RABATTAB,
         PCENAMAG,
         GR_VAT, VAT, PGR_VAT, PVAT,
         OPK, MAGAZYN, OPIS,
         PARAMS1,PARAMS2,PARAMS3,PARAMS4,PARAMS5,PARAMS6,PARAMS7,PARAMS8,PARAMS9,PARAMS10,PARAMS11,PARAMS12,
         PARAMN1,PARAMN2,PARAMN3,PARAMN4,PARAMN5,PARAMN6,PARAMN7,PARAMN8,PARAMN9,PARAMN10,PARAMN11,PARAMN12,
         PARAMD1,PARAMD2,PARAMD3,PARAMD4,PREC)
     select :faktura,KTM,WERSJAREF,DOSTAWA,
         ILOSC, ILOSCO, ILOSCM,
         JEDN, JEDNO,
         CENACEN*:wsp, WALCEN,
         RABAT, RABATTAB,
         CENAMAG,
         GR_VAT, VAT, GR_VAT, VAT,
         OPK, MAGAZYN, OPIS,
         PARAMS1,PARAMS2,PARAMS3,PARAMS4,PARAMS5,PARAMS6,PARAMS7,PARAMS8,PARAMS9,PARAMS10,PARAMS11,PARAMS12,
         PARAMN1,PARAMN2,PARAMN3,PARAMN4,PARAMN5,PARAMN6,PARAMN7,PARAMN8,PARAMN9,PARAMN10,PARAMN11,PARAMN12,
         PARAMD1,PARAMD2,PARAMD3,PARAMD4,PREC
     from POZFAK where DOKUMENT = :fakturazal and gr_VAT=:vat
     order by POZFAK.NUMER;
  status = 1;
end^
SET TERM ; ^
