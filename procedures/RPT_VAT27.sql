--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_VAT27(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      COMPANY integer)
  returns (
      ALLPAGES integer,
      CURRPAGE integer)
   as
declare variable temp float;
begin
  select count(*) from (  select bkdocs.contractor
    from bkdocs
      join bkvatpos on (bkvatpos.bKdoc = bkdocs.ref)
    where bkdocs.vatperiod =:period and bkdocs.company = :company and bkvatpos.unifiedtransaction = 1 and bkdocs.status > 1
    group by bkdocs.contractor, bkdocs.nip)
    into :temp;
  temp = temp/30;
  allpages = cast(temp as integer);
  if (allpages != temp) then allpages=allpages + 1;
  CURRPAGE = 1;
  suspend;
  while(currpage < allpages) do
  begin
    currpage =currpage +1;
    suspend;
  end

end^
SET TERM ; ^
