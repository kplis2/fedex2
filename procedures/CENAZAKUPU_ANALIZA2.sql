--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CENAZAKUPU_ANALIZA2(
      KTM varchar(40) CHARACTER SET UTF8                           ,
      CENA numeric(14,2),
      KROK numeric(14,2),
      OD1 numeric(14,2),
      DO1 numeric(14,2),
      OD2 numeric(14,2),
      DO2 numeric(14,2),
      PARAM1 varchar(40) CHARACTER SET UTF8                           ,
      PARAM2 varchar(40) CHARACTER SET UTF8                           ,
      REFCENNIKZAK integer)
  returns (
      WART numeric(14,2),
      WART1 numeric(14,2),
      WART2 numeric(14,2),
      WART3 numeric(14,2),
      WART4 numeric(14,2),
      WART5 numeric(14,2),
      WART6 numeric(14,2),
      WART7 numeric(14,2),
      WART8 numeric(14,2),
      WART9 numeric(14,2),
      WART10 numeric(14,2),
      WART11 numeric(14,2),
      WART12 numeric(14,2),
      WART13 numeric(14,2),
      WART14 numeric(14,2),
      WART15 numeric(14,2),
      WART16 numeric(14,2),
      WART17 numeric(14,2),
      WART18 numeric(14,2),
      WART19 numeric(14,2),
      WART20 numeric(14,2),
      WART21 numeric(14,2),
      WART22 numeric(14,2),
      WART23 numeric(14,2),
      WART24 numeric(14,2),
      WART25 numeric(14,2),
      WART26 numeric(14,2),
      WART27 numeric(14,2),
      WART28 numeric(14,2),
      WART29 numeric(14,2),
      WART30 numeric(14,2))
   as
declare variable sumaproc numeric(14,2);
declare variable cnt integer;
declare variable p1 numeric(14,2);
declare variable do_p1 numeric(14,2);
declare variable p2 numeric(14,2);
declare variable do_p2 numeric(14,2);
declare variable p1org numeric(14,2);
declare variable p2org numeric(14,2);
begin
  p1 = :Od1;
  p2 = :Od2;
  Do_p1 = Do1;
  Do_p2 = Do2;
  p1org = Od1;
  p2org = Od2;

  cnt = 1;
  sumaproc = 0;

  if (p2<=:Do_p2) then begin wart1 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart2 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart3 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart4 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart5 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart6 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart7 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart8 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart9 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart10 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart11 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart12 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart13 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart14 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart15 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart16 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart17 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart18 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart19 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart20 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart21 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart22 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart23 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart24 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart25 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart26 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart27 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart28 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart29 = :p2; p2 = :p2 + :krok; end
  if (p2<=:Do_p2) then begin wart30 = :p2; p2 = :p2 + :krok; end

  suspend;
  wart=0;
  WART1=0; WART2=0; WART3=0; WART4=0; WART5=0; WART6=0; WART7=0; WART8=0; WART9=0;
  WART10=0; WART11=0; WART12=0; WART13=0; WART14=0; WART15=0; WART16=0; WART17=0;
  WART18=0; WART19=0; WART20=0; WART21=0; WART22=0; WART23=0; WART24=0; WART25=0;
  WART26=0; WART27=0; WART28=0; WART29=0; WART30=0;
  cnt = 1;
  while (:p1 <= :Do_p1) do begin
    WART = :p1;
    if(Do_p1>0) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,1,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart1;
    if(Do_p1>1) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,2,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart2;
    if(Do_p1>2) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,3,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart3;
    if(Do_p1>3) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,4,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart4;
    if(Do_p1>4) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,5,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart5;
    if(Do_p1>5) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,6,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart6;
    if(Do_p1>6) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,7,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart7;
    if(Do_p1>7) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,8,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart8;
    if(Do_p1>8) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,9,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart9;
    if(Do_p1>9) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,10,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart10;
    if(Do_p1>10) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,11,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart11;
    if(Do_p1>11) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,12,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart12;
    if(Do_p1>12) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,13,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart13;
    if(Do_p1>13) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,14,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart14;
    if(Do_p1>14) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,15,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart15;
    if(Do_p1>15) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,16,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart16;
    if(Do_p1>16) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,17,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart17;
    if(Do_p1>17) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,18,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart18;
    if(Do_p1>18) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,19,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart19;
    if(Do_p1>19) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,20,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart20;
    if(Do_p1>20) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,21,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart21;
    if(Do_p1>21) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,22,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart22;
    if(Do_p1>22) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,23,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart23;
    if(Do_p1>23) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,24,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart24;
    if(Do_p1>24) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,25,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart25;
    if(Do_p1>25) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,26,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart26;
    if(Do_p1>26) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,27,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart27;
    if(Do_p1>27) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,28,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart28;
    if(Do_p1>28) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,29,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart29;
    if(Do_p1>29) then execute procedure cenazakupu_analiza(:ktm,:cena,:p1,30,:PARAM1,:PARAM2,:refcennikzak) returning_values :wart30;
    suspend;
    p1 = :p1 + :krok;
  end
end^
SET TERM ; ^
