--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_KLI_KLIENCI_PROCESS(
      SESJAREF SESJE_ID,
      TABELA STRING35,
      REF INTEGER_ID,
      BLOKUJZALEZNE SMALLINT_ID = 0,
      TYLKONIEPRZETWORZONE SMALLINT_ID = 0,
      AKTUALIZUJSESJE SMALLINT_ID = 1)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable stabela string35;
declare variable tmptabela string35;
declare variable sprocedura string255;
declare variable szrodlo zrodla_id;
declare variable skierunek smallint_id;
declare variable sesja sesje_id;
declare variable sql memo;
declare variable tmpsql memo;
declare variable tmpstatus smallint_id;
declare variable tmpmsg string255;
declare variable error_row smallint_id;
declare variable errormsg_row string255;
declare variable errormsg_all string255;
declare variable tmperror smallint_id;
declare variable statuspoz smallint_id;
--KLIENT
declare variable K_REF INTEGER_ID;
declare variable K_KLIENTID STRING120;
declare variable K_SKROT STRING255;
declare variable K_FIRMA INTEGER_ID;
declare variable K_NAZWA STRING255;
declare variable K_NIP STRING30;
declare variable K_IMIE STRING120;
declare variable K_NAZWISKO STRING120;
declare variable K_GRUPA STRING255;
declare variable K_GRUPAID STRING120;
declare variable K_TYP STRING255;
declare variable K_TYPID STRING120;
declare variable K_REGON STRING30;
declare variable K_KRAJ STRING80;
declare variable K_KRAJID STRING20;
declare variable K_ULICA STRING120;
declare variable K_NRDOMU STRING40;
declare variable K_NRLOKALU STRING40;
declare variable K_KODPOCZTOWY STRING40;
declare variable K_MIASTO STRING120;
declare variable K_TELEFON STRING255;
declare variable K_KOMORKA STRING255;
declare variable K_FAX STRING120;
declare variable K_EMAIL STRING255;
declare variable K_WWW STRING255;
declare variable K_UWAGI MEMO;
declare variable K_AKTYWNY INTEGER_ID;
declare variable K_LIMITKREDYTOWY KURS_ID;
declare variable K_BEZPSPOSPLATNOSCI INTEGER_ID;
declare variable K_TERMINDNI INTEGER_ID;
declare variable K_ZAPLATA STRING255;
declare variable K_ZAPLATAID STRING120;
declare variable K_DOSTAWA STRING255;
declare variable K_DOSTAWAID STRING120;
declare variable K_WALUTA STRING20;
declare variable K_RABAT KURS_ID;
declare variable K_CENNIK STRING255;
declare variable K_CENNIKID STRING120;
declare variable K_SPRZEDAWCA STRING255;
declare variable K_SPRZEDAWCAID STRING120;
declare variable K_TYPDOKFAK STRING20;
declare variable K_TYPDOKMAG STRING20;
declare variable K_BANK STRING255;
declare variable K_RACHUNEK STRING60;
declare variable K_DOSTAWCAID STRING120;
declare variable K_HASH STRING255;
declare variable K_DEL INTEGER_ID;
declare variable K_REC STRING255;
declare variable K_SKADTABELA STRING40;
declare variable K_SKADREF INTEGER_ID;
--zmienne ogolne
declare variable KLIENT KLIENCI_ID;
declare variable KLIENTHIST KLIENCIHIST_ID;
declare variable SPRZEDAWCA SPRZEDAWCA_ID;
declare variable SPOSDOST SPOSDOST_ID;
declare variable SPOSZAP PLATNOSC_ID;
declare variable GRUPA GRUPYKLI_ID;
declare variable ODDZIAL ODDZIAL_ID;
declare variable COMPANY COMPANIES_ID;
declare variable CENNIK CENNIK_ID;
declare variable DOSTAWCA DOSTAWCA_ID;
declare variable NIP NIP;
declare variable NIPUE STRING20;
declare variable PROSTYNIP NIP;
declare variable EMAIL EMAILS_ID;
declare variable KLIENTHIST_MANUAL SMALLINT_ID;

declare variable klientzrodla klienci_id;
begin
  --zakomentowanie exita
  --exit;
  --zmiana pustych wartosci na null-e
  status = 1;
  msg = '';

  if (sesjaref = 0) then
    sesjaref = null;
  if (trim(tabela) = '') then
    tabela = null;
  if (ref = 0) then
    ref = null;
  if (blokujzalezne is null) then
    blokujzalezne = 0;
  if (tylkonieprzetworzone is null) then
    tylkonieprzetworzone = 0;
  if (aktualizujsesje is null) then
    aktualizujsesje =1;

  status = 1;
  errormsg_all = '';
  --sprawdzenie czy wszystkie potrzebne informacje sa
  if (sesjaref is null and ref is null) then
    begin
      status = -1;
      msg = 'Za malo parametrow.';
      exit; --EXIT
    end

  if (sesjaref is null) then
    begin
      if (ref is null) then
        begin
          status = -1;
          msg = 'Jesli nie podales sesji to wypadaloby podac ref-a...';
          exit; --EXIT
        end
      if (tabela is null) then
        begin
          --status = -1;
          --msg = 'Wypadaloby podac nazwe tabeli...';
          --exit; --EXIT
          sql = 'select sesja from INT_IMP_KLI_KLIENCI where ref='||:ref;
          execute statement sql into :sesja;
        end
      else
        begin
          sql = 'select sesja from '||:tabela||' where ref='||:ref;
          execute statement sql into :sesja;
        end

    end
  else
    sesja = sesjaref;

  if (sesja is null) then
    begin
      status = -1;
      msg = 'Nie znaleziono numeru sesji.';
      exit; --EXIT
    end

  --pobranie parametrow sesji
  select s.tabela, s.procedura, s.zrodlo, s.kierunek
    from int_sesje s
    where ref = :sesja
    into :stabela, :sprocedura, :szrodlo, :skierunek;

  --if (tabela is not null) then --jesli podano tabele na wejsciu traktuje ja nadrzednie
    --stabela = tabela;

  oddzial = null;
  company = null;
  select oddzial, company, klient from int_zrodla where ref = :szrodlo
    into :oddzial, :company, :klientzrodla;
  if (oddzial is null) then
    oddzial = 'CENTRALA';
  if (company is null) then
    company = 1;


  --wlasciwe przetworzenie

  --if (tabela = 'INT_IMP_ZAM_NAGLOWKI') then
    --begin
      for select ref, klientid, skrot, firma, nazwa, nip,
              imie, nazwisko, grupa, grupaid, typ, typid,
              regon, kraj, krajid, ulica, nrdomu, nrlokalu,
              kodpocztowy, miasto, telefon, komorka, fax,
              email, www, uwagi, aktywny, limitkredytowy,
              bezpsposplatnosci, termindni, zaplata, zaplataid,
              dostawa, dostawaid, waluta, rabat, cennik, cennikid,
              sprzedawca, sprzedawcaid, typdokfak, typdokmag,
              bank, rachunek, dostawcaid, skadtabela, skadref,
              --hash,
              del, rec
            from INT_IMP_KLI_KLIENCI k
            where (k.sesja = :sesja or :sesja is null)
              and ((:ref is not null and k.ref = :ref and :tabela is null)
              or (:ref is not null and :tabela is not null and k.skadref = :ref and k.skadtabela = :tabela)
              or (:ref is null))
            order by ref
            into :k_ref, :k_klientid, :k_skrot,:k_firma, :k_nazwa, :k_nip,
              :k_imie, :k_nazwisko, :k_grupa, :k_grupaid, :k_typ, :k_typid,
              :k_regon, :k_kraj, :k_krajid, :k_ulica, :k_nrdomu, :k_nrlokalu,
              :k_kodpocztowy, :k_miasto, :k_telefon, :k_komorka, :k_fax,
              :k_email, :k_www, :k_uwagi, :k_aktywny, :k_limitkredytowy,
              :k_bezpsposplatnosci, :k_termindni, :k_zaplata, :k_zaplataid,
              :k_dostawa, :k_dostawaid, :k_waluta, :k_rabat, :k_cennik, :k_cennikid,
              :k_sprzedawca, :k_sprzedawcaid, :k_typdokfak, :k_typdokmag,
              :k_bank, :k_rachunek, :k_dostawcaid, :k_skadtabela, :k_skadref,
              --:k_hash,
              :k_del, :k_rec
        do begin
          error_row = 0;
          errormsg_row = '';
          tmperror = null;

          if (coalesce(trim(k_klientid),'') = '') then
            begin
              error_row = 1;
              errormsg_row = substring(errormsg_row || ' ' || 'Brak ID klienta.' from 1 for 255);
            end


          if (coalesce(k_del,0) = 1 and error_row = 0) then --rekord do skasowania
            begin
              --select status, msg from INT_IMP_KLI_KLIENCI_DEL(nz_nagid)
                --into :tmpstatus, :tmpmsg;
              tmpstatus = null;
            end
          else if (error_row = 0 and coalesce(k_del,0) = 0) then
          begin

            if (coalesce(trim(k_nazwa),'') = '' and coalesce(trim(k_imie),'') = ''
                and coalesce(trim(k_nazwisko),'') = '' ) then
              begin
                error_row = 1;
                errormsg_row = substring(errormsg_row || ' ' || 'Brak nazwy.' from 1 for 255);
              end

            if (error_row = 0) then
            begin
              --nullowanie, zerowanie i blankowanie - naglowek zamowienia
              if (coalesce(trim(k_nip),'') = '') then
                k_nip = null;
              if (coalesce(trim(k_email),'') = '') then
                k_email = null;
              if (k_firma is null) then
                begin
                  if (k_nip is not null) then
                    k_firma = 1;
                  else
                    k_firma = 0;
                end
              if (coalesce(trim(k_imie),'') = '') then
                k_imie = null;
              if (coalesce(trim(k_nazwisko),'') = '') then
                k_nazwisko = null;
              if (coalesce(trim(k_limitkredytowy),'') = '') then
                k_limitkredytowy = null;
              if (k_bezpsposplatnosci is null) then
                k_bezpsposplatnosci = 0;
              if (coalesce(trim(k_waluta),'') = '') then --dodac walidacje !!!!
                k_waluta = 'PLN';
              if (coalesce(trim(k_krajid),'') = '') then
              begin
                error_row = 1;
                errormsg_row = 'Brak id kraju';
              end
              else if(
                not exists (select first 1 1 from countries where symbol is not distinct from :k_krajid)
              )then
              begin
                error_row = 1;
                errormsg_row = 'Brak id kraju w sĹ‚owniku';
              end

              if (k_aktywny is null) then
                k_aktywny = 1;


              --dopisac obsluge !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

              k_bank = null;
              sposzap = null;
              sposdost = null;

              if(:k_zaplata is not null or :k_zaplataid is not null)then
                select p.ref from int_slownik s
                  join platnosci p on p.ref = s.oref
                  where s.zrodlo = :szrodlo
                    and (:k_zaplata is null or s.wartosc is not distinct from :k_zaplata)
                    and (:k_zaplataid is null or s.wartoscid is not distinct from :k_zaplataId)
                    and s.otable = 'PLATNOSCI'
                  into :sposzap;
              if(:k_dostawa is not null or :k_dostawaid is not null)then
                select sd.ref from int_slownik s
                  join sposdost sd on sd.ref = s.oref
                  where s.zrodlo = :szrodlo
                    and (:k_dostawa is null or s.wartosc is not distinct from :k_dostawa)
                    and (:k_dostawaid is null or s.wartoscid is not distinct from :k_dostawaid)
                    and s.otable = 'SPOSDOST'
                  into :sposdost;

              sprzedawca = null;
              select first 1 ref from grupykli into :grupa;
              cennik = null;
              dostawca = null;

              if (coalesce(trim(k_skrot),'') = '') then
                begin
                  if (coalesce(trim(k_nazwa),'') <> '') then
                    k_skrot = trim(substring(k_nazwa from 1 for 40));
                  else if (coalesce(trim(k_imie),'') <> '' or coalesce(trim(k_nazwisko),'') <> '') then
                    k_skrot = trim(substring(coalesce(:k_imie,'')||coalesce(' '||:k_nazwisko,'') from 1 for 40));
                  else
                    begin
                      error_row = 1;
                      errormsg_row = substring(errormsg_row || ' ' || 'Nie udalo sie wyliczyc skrotu klienta.' from 1 for 255);
                    end
                end

              --blokujzalezne = 1; --!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
              if (:blokujzalezne = 0) then --tabele zalezne BEFORE
              begin
                select status, msg from int_tabelezalezne_process(:sesja,
                    :stabela, :k_ref, :szrodlo, :skierunek, 0)
                  into :tmpstatus, :tmpmsg;
                if(coalesce(:tmpstatus,0) = -1 )then
                begin
                  error_row = 1;
                  errormsg_row = substring(errormsg_row || ' ' || coalesce(:tmpmsg,'') from 1 for 255);
                end 
              end
              if (error_row = 0) then
                begin
                klient =  null;

                  if (klient is null) then
                    begin
                     update or insert into klienci (fskrot, firma, nazwa, imie, nazwisko,
                          grupa, nip, regon, ulica, miasto, kodp,
                          kraj, krajid, telefon, fax, email, www,
                          uwagi, limitkr, sposplat, sposplatbezp,
                          termin, upust, waluta, dostawca, bank, rachunek,
                          sprzedawca, komorka, oddzial, zagraniczny,
                          grupanazwa, typdokvat, sposdost,
                          company, nrdomu, nrlokalu, cennik, aktywny,
                          int_zrodlo, int_id, int_dataostprzetw, int_sesjaostprzetw,
                          parent)
                        values(:k_skrot,:k_firma, :k_nazwa, :k_imie, :k_nazwisko,
                          :grupa, :k_nip, :k_regon, :k_ulica, :k_miasto, :k_kodpocztowy,
                          :k_kraj, :k_krajid, :k_telefon, :k_fax, :k_email, :k_www,
                          :k_uwagi, :k_limitkredytowy, :sposzap, :k_bezpsposplatnosci,
                          :k_termindni, :k_rabat, :k_waluta, :dostawca, :k_bank, :k_rachunek,
                          :sprzedawca, :k_komorka, :oddzial, case when :k_krajid = 'PL' then 0 else 1 end,
                          :k_grupa, :k_typdokfak, :sposdost,
                          :company, :k_nrdomu, :k_nrlokalu, :cennik, :k_aktywny,
                          :szrodlo, :k_klientid,  current_timestamp(0), :sesja,
                          :klientzrodla)
                        matching(int_id, int_zrodlo)
                        returning ref into :klient; --KLIENT
                      end
                  else
                    begin
                      if (email is null) then --update emaila na kliencie
                        update klienci set email = :k_email,
                            int_zrodlo = :szrodlo, int_id = :k_klientid
                          where ref = :klient;
                    end

                  klienthist = null;
                  klienthist_manual = null;
                  --sprawdzam czy jest konieczny insert do kiencihist
                  select kh.ref, kh.is_manual from int_kliencihist kh
                    where kh.klient is not distinct from :klient
                      and coalesce(lower(replace(kh.imie,' ','')),'') = coalesce(lower(replace(:k_imie,' ','')),'')
                      and coalesce(lower(replace(kh.nazwisko,' ','')),'') = coalesce(lower(replace(:k_nazwisko,' ','')),'')
                      and coalesce(lower(replace(kh.nazwa,' ','')),'') = coalesce(lower(replace(:k_nazwa,' ','')),'')
                      and coalesce(lower(replace(kh.ulica,' ','')),'') = coalesce(lower(replace(:k_ulica,' ','')),'')
                      and coalesce(lower(replace(kh.nrdomu,' ','')),'') = coalesce(lower(replace(:k_nrdomu,' ','')),'')
                      and coalesce(lower(replace(kh.nrlokalu,' ','')),'') = coalesce(lower(replace(:k_nrlokalu,' ','')),'')
                      and coalesce(lower(replace(replace(kh.kodp,' ',''),'-','')),'') = coalesce(lower(replace(replace(:k_kodpocztowy,' ',''),'-','')),'')
                      and coalesce(lower(replace(kh.miasto,' ','')),'') = coalesce(lower(replace(:k_miasto,' ','')),'')
                      and coalesce(lower(replace(kh.krajid,' ','')),'') = coalesce(lower(replace(:k_krajid,' ','')),'')
                      and coalesce(lower(replace(kh.nip,' ','')),'') = coalesce(lower(replace(:k_nip,' ','')),'')
                      --and coalesce(lower(replace(kh.typdokvat,' ','')),'') = coalesce(lower(replace(:k_typdokfak,' ','')),'')
                    into :klienthist, :klienthist_manual;
              
                  if(coalesce(klienthist, 0) = 0) then
                    begin
                      if (k_nip is null) then
                        k_nip = '';

                      if (k_nip not similar to '[[:ALNUM:]]*')then begin
                        error_row = 1;
                        errormsg_row = substring(errormsg_row || ' ' ||  'W polu NIP dopuszczalne tylko znaki alfanumeryczne. NIP: '||:k_nip
                                                  ||' Klient: '||:k_klientid||' '||coalesce(:k_skrot,'') from 1 for 255);

                      end

                      insert into int_kliencihist(klient, nazwa, imie, nazwisko, ulica,
                          nrdomu, miasto, kodp, krajid, nip, is_manual, typdokvat)
                        values(:klient, :k_nazwa, :k_imie, :k_nazwisko, :k_ulica,
                          :k_nrdomu, :k_miasto, :k_kodpocztowy, :k_krajid, :k_nip, 0, :k_typdokfak)
                        returning ref into :klienthist;
                    end
                  else if (coalesce(:klienthist_manual,0) = 1) then
                    begin
                      update int_kliencihist kh
                        set kh.is_manual = 0
                        where kh.ref = :klienthist;
                    end


                    if (coalesce(trim(k_skadtabela), '') <> ''
                      and coalesce(k_skadref,0) > 0 ) then
                    begin
                      sql = 'update '|| :k_skadtabela ||' set klient_sente = '|| :klient ||
                        ', klienthist_sente = '|| coalesce(klienthist, 'null') ||
                        ' where ref = '|| :k_skadref;
                      execute statement sql;
                    end
                    --blokujzalezne = 1; --!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    if (:blokujzalezne = 0) then --tabele zalezne BEFORE
                    begin
                      select status, msg from int_tabelezalezne_process(:sesja,
                          :stabela, :k_ref, :szrodlo, :skierunek, 1)
                        into :tmpstatus, :tmpmsg;
                      if(coalesce(:tmpstatus,0) = -1 )then
                      begin
                        error_row = 1;
                        errormsg_row = substring(errormsg_row || ' ' || coalesce(:tmpmsg,'') from 1 for 255);
                      end 
                    end
                  end
        end
      end
      if(:error_row = 1)then
      begin
        status = -1; --ML jezeli nie przetworzy sie chociaz jeden wiersz przetwarzanie nieudane;
        errormsg_all = substring(errormsg_all || :errormsg_row from 1 for 255);
      end
      update int_imp_kli_klienci set klient = :klient, klienthist = :klienthist,
        error = :error_row, errormsg = :errormsg_row,
        dataostprzetw = current_timestamp(0)
        where ref = :k_ref;
    end

  if(coalesce(status,-1) != 1) then
    msg = substring((msg || :errormsg_all) from 1 for 255);

  --aktualizacja informacji o sesji
  if (aktualizujsesje = 1) then
    begin
      select status, msg from int_sesje_aktualizuj(:sesja, 1) into :tmpstatus, :tmpmsg;
      if(coalesce(status,-1) = 1) then
        status = :tmpstatus; --ML jezeli przed aktualizacjas sesji status byl zly, to nie zmieniamy na ok
      msg = substring((msg || coalesce(:tmpmsg,'')) from 1 for 255);
    end
  suspend;
end^
SET TERM ; ^
