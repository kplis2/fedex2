--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_KLI_ODBIORCY_PROCESS(
      SESJAREF SESJE_ID,
      TABELA STRING35,
      REF INTEGER_ID,
      BLOKUJZALEZNE SMALLINT_ID = 0,
      TYLKONIEPRZETWORZONE SMALLINT_ID = 0,
      AKTUALIZUJSESJE SMALLINT_ID = 1)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable stabela string35;
declare variable tmptabela string35;
declare variable sprocedura string255;
declare variable szrodlo zrodla_id;
declare variable skierunek smallint_id;
declare variable sesja sesje_id;
declare variable sql memo;
declare variable tmpsql memo;
declare variable tmpstatus smallint_id;
declare variable tmpmsg string255;
declare variable error_row smallint_id;
declare variable errormsg_row string255; --ML blad przetwarzania pojedynczego odbiorcy
declare variable errormsg_all string255; --ML blad przetwarzania wszystkich odbiorców
--declare variable statuspoz smallint_id;
--ODBIORCA
declare variable O_REF INTEGER_ID;
declare variable O_KLIENTID STRING120;
declare variable O_ODBIORCAID STRING120;
declare variable O_GLOWNY INTEGER_ID;
declare variable O_NAZWA STRING255;
declare variable O_NAZWAFIRMY STRING255;
declare variable O_IMIE STRING120;
declare variable O_NAZWISKO STRING120;
declare variable O_KRAJ STRING80;
declare variable O_KRAJID STRING20;
declare variable O_ULICA STRING120;
declare variable O_NRDOMU STRING40;
declare variable O_NRLOKALU STRING40;
declare variable O_KODPOCZTOWY STRING40;
declare variable O_MIASTO STRING120;
declare variable O_TELEFON STRING255;
declare variable O_KOMORKA STRING255;
declare variable O_FAX STRING120;
declare variable O_EMAIL STRING255;
declare variable O_WWW STRING255;
declare variable O_HASH STRING255;
declare variable O_DEL INTEGER_ID;
declare variable O_REC STRING255;
declare variable O_SKADTABELA STRING40;
declare variable O_SKADREF INTEGER_ID;
--zmienne ogolne
declare variable KLIENT KLIENCI_ID;
declare variable ODBIORCA ODBIORCY_ID;
declare variable ODDZIAL ODDZIAL_ID;
declare variable COMPANY COMPANIES_ID;


declare variable OO_KRAJID STRING20;
declare variable OO_KODPOCZTOWY STRING40;
declare variable OO_TELEFON STRING255;
declare variable OO_FAX STRING120;
declare variable OO_EMAIL STRING255;
declare variable OO_WWW STRING255;
begin
  --exception universal 'test any';
  --zmiana pustych wartosci na null-e
  if (sesjaref = 0) then
    sesjaref = null;
  if (trim(tabela) = '') then
    tabela = null;
  if (ref = 0) then
    ref = null;
  if (blokujzalezne is null) then
    blokujzalezne = 0;
  if (tylkonieprzetworzone is null) then
    tylkonieprzetworzone = 0;
  if (aktualizujsesje is null) then
    aktualizujsesje =1;

  status = 1;
  msg ='';

  --sprawdzenie czy wszystkie potrzebne informacje sa
  if (sesjaref is null and ref is null) then
  begin
    status = -1;
    msg = 'Za malo parametrow.';
    exit; --EXIT
  end

  if (sesjaref is null) then
  begin
    if (ref is null) then
      begin
        status = -1;
        msg = 'Jesli nie podales sesji to wypadaloby podac ref-a...';
        exit; --EXIT
      end
    if (tabela is null) then
    begin
      status = -1;
      msg = 'Wypadaloby podac nazwe tabeli...';
      exit; --EXIT
    end
    else
    begin
      sql = 'select sesja from '||:tabela||' where ref='||:ref;
      execute statement sql into :sesja;
    end
  end
  else
    sesja = sesjaref;

  if (sesja is null) then
  begin
    status = -1;
    msg = 'Nie znaleziono numeru sesji.';
    exit; --EXIT
  end

  --pobranie parametrow sesji
  select s.tabela, s.procedura, s.zrodlo, s.kierunek
    from int_sesje s
    where ref = :sesja
    into :stabela, :sprocedura, :szrodlo, :skierunek;

  --if (tabela is not null) then --jesli podano tabele na wejsciu traktuje ja nadrzednie
    --stabela = tabela;

  if(coalesce(:szrodlo,-1) = -1)then
  begin
    status = -1;
    msg = 'Brakuje id zrodla';
    exit;
  end

  oddzial = null;
  company = null;
  select oddzial, company from int_zrodla where ref = :szrodlo
    into :oddzial, :company;
  if (oddzial is null) then
    oddzial = 'CENTRALA';
  if (company is null) then
    company = 1;

  errormsg_all = '';

  --wlasciwe przetworzenie
  for select ref, klientid, odbiorcaid, glowny, nazwa, nazwafirmy,
          imie, nazwisko, kraj, krajid, ulica, nrdomu, nrlokalu,
          kodpocztowy, miasto, telefon, komorka, fax, email, www,
          skadtabela, skadref,
          --hash,
          del, rec
        from INT_IMP_KLI_ODBIORCY k
        where (k.sesja = :sesja or :sesja is null)
          and ((:ref is not null and k.ref = :ref and :tabela is null)
          or (:ref is not null and :tabela is not null and k.skadref = :ref and k.skadtabela = :tabela)
          or (:ref is null))
        order by ref
        into :o_ref, :o_klientid, :o_odbiorcaid, :o_glowny, :o_nazwa, :o_nazwafirmy,
          :o_imie, :o_nazwisko, :o_kraj, :o_krajid, :o_ulica, :o_nrdomu, :o_nrlokalu,
          :o_kodpocztowy, :o_miasto, :o_telefon, :o_komorka, :o_fax, :o_email, :o_www,
          :o_skadtabela, :o_skadref,
          --:o_hash,
          :o_del, :o_rec
    do begin
      error_row = 0;
      errormsg_row = '';

      klient = null;
      odbiorca = null;

      /*if (coalesce(trim(o_odbiorcaid),'') = '') then  --[PM] XXX
        begin
          error = 1;
          errormsg = errormsg || 'Brak ID odbiorcy.';
        end  */
      if (coalesce(trim(o_klientid),'') = '') then
        begin
          error_row = 1;
          errormsg_row = substring(errormsg_row || ' ' || 'Brak ID klienta.' from 1 for 255);
        end

      select ref from klienci where int_zrodlo = :szrodlo and int_id = :o_klientid
        into :klient;

      if (klient is null) then
        begin
          error_row = 1;
          errormsg_row = substring(errormsg_row || ' ' || 'Nie znaleziono klienta.' from 1 for 255);
        end

      if (coalesce(o_del,0) = 1 and error_row = 0) then --rekord do skasowania
        begin
          --select status, msg from INT_IMP_KLI_KLIENCI_DEL(nz_nagid)
            --into :tmpstatus, :tmpmsg;
          tmpstatus = null;
        end
      else if (error_row = 0 and coalesce(o_del,0) = 0) then
      begin
        if (blokujzalezne = 0) then --tabele zalezne BEFORE
        begin
          select status, msg from int_tabelezalezne_process(:sesja,
            :stabela, :ref, :szrodlo, :skierunek, 0)
            into :tmpstatus, :tmpmsg;
        end
          
        --nullowanie, zerowanie i blankowanie - naglowek zamowienia
        if (coalesce(trim(o_imie),'') = '') then
          o_imie = null;
        if (coalesce(trim(o_nazwisko),'') = '') then
          o_nazwisko = null;
        if (coalesce(trim(o_nazwa),'') = '') then
          o_nazwa = null;
        if (coalesce(trim(o_nazwafirmy),'') = '') then
          o_nazwafirmy = null;
        if (coalesce(trim(o_krajid),'') = '') then --dodac walidacje !!!!
          o_krajid = 'PL';
        if (o_glowny is null) then
          o_glowny = 0;
        if (coalesce(trim(o_telefon),'') = '') then
          o_telefon = null;
        if (coalesce(trim(o_komorka),'') = '') then
          o_komorka = null;
        if (coalesce(trim(o_fax),'') = '') then
          o_fax = null;
        if (coalesce(trim(o_email),'') = '') then
          o_email = null;
        if (coalesce(trim(o_www),'') = '') then
          o_www = null;

        select first 1 o.ref, o.dtelefon, o.email, o.www, o.dkodp, o.krajid
          from odbiorcy o
          where o.klient = :klient
            and coalesce(lower(replace(o.nazwa,' ','')),'') = coalesce(lower(replace(:o_nazwa, ' ','')),'')
            and coalesce(lower(replace(o.nazwafirmy,' ','')),'') = coalesce(lower(replace(:o_nazwafirmy, ' ','')),'')
            --and coalesce(lower(replace(replace(o.dkodp, ' ',''),'-','')),'') = coalesce(lower(replace(replace(:o_kodpocztowy, ' ',''),'-','')),'')
            and coalesce(lower(replace(o.dmiasto,' ','')),'') = coalesce(lower(replace(:o_miasto, ' ','')),'')
            and coalesce(lower(replace(o.dulica,' ','')),'') = coalesce(lower(replace(:o_ulica, ' ','')),'')
            and coalesce(lower(replace(o.dnrdomu,' ','')),'') = coalesce(lower(replace(:o_nrdomu, ' ','')),'')
            and coalesce(lower(replace(o.dnrlokalu,' ','')),'') = coalesce(lower(replace(:o_nrlokalu, ' ','')),'')
            --and coalesce(lower(replace(o.krajid,' ','')),'') = coalesce(lower(replace(:o_krajid, ' ','')),'')
            --and coalesce(lower(replace(o.dtelefon,' ','')),'') = coalesce(lower(replace(:o_komorka, ' ','')), lower(replace(:o_telefon, ' ','')), '')
            --and coalesce(lower(replace(o.email,' ','')),'') = coalesce(lower(replace(:o_email, ' ','')),'')
            --and coalesce(lower(replace(o.www,' ','')),'') = coalesce(lower(replace(:o_www, ' ','')),'')
            --and coalesce(int_zrodlo,0) = coalesce(:szrodlo,0) [PM] XXX
            --and coalesce(int_id,'') = coalesce(:o_odbiorcaid,'') [PM] XXX
          into :odbiorca, :oo_telefon, :oo_email, :oo_www, :oo_kodpocztowy, :oo_krajid;

        if (coalesce(:odbiorca,0) = 0) then
          begin
            insert into odbiorcy
              (klient, imie, nazwisko, nazwa, nazwafirmy,
                dulica, dmiasto, dkodp, dtelefon, email, www,
                dnrdomu, dnrlokalu, krajid,
                gl, company,--, int_id,
                int_zrodlo)--, int_id, int_dataostprzetw, int_sesjaostprzetw) --[PM] XXX
              values(:klient, :o_imie, :o_nazwisko, :o_nazwa, :o_nazwafirmy,
               :o_ulica, :o_miasto, :o_kodpocztowy, coalesce(:o_komorka, :o_telefon), :o_email, :o_www,
               :o_nrdomu, :o_nrlokalu, :o_krajid,
               :o_glowny, :company, --, :o_odbiorcaid,
               :szrodlo) --, :o_odbiorcaid, current_timestamp(0), :sesja --[PM] XXX  ML zrodlo potrzebne, bo samo int_id nie jest unikalnym identyfikatorem
              returning ref into :odbiorca;
          end else if (coalesce(lower(replace(replace(:oo_kodpocztowy, ' ',''),'-','')),'') = coalesce(lower(replace(replace(:o_kodpocztowy, ' ',''),'-','')),'')
                      or coalesce(lower(replace(:oo_krajid,' ','')),'') = coalesce(lower(replace(:o_krajid, ' ','')),'')
                      or coalesce(lower(replace(:oo_telefon,' ','')),'') = coalesce(lower(replace(:o_komorka, ' ','')), lower(replace(:o_telefon, ' ','')), '')
                      or coalesce(lower(replace(oo_email,' ','')),'') = coalesce(lower(replace(:o_email, ' ','')),'')
                      or coalesce(lower(replace(oo_www,' ','')),'') = coalesce(lower(replace(:o_www, ' ','')),'')) then
          begin
            update odbiorcy o set o.dkodp =  :o_kodpocztowy, o.krajid =  :o_krajid,
                                  o.dtelefon = coalesce(:o_komorka, :o_telefon),
                                  o.email = :o_email, o.www = :o_www
              where o.ref = :odbiorca;

          end
        /*else if (coalesce(szrodlo,0) <> 0 and coalesce(:o_odbiorcaid,'') <> '') then --[PM] XXX
          begin
            update odbiorcy set int_dataostprzetw = current_timestamp(0),
              int_sesjaostprzetw = :sesja where ref = :odbiorca;
          end*/

          if (coalesce(trim(o_skadtabela), '') <> ''
            and coalesce(o_skadref,0) > 0 ) then
          begin
            sql = 'update '|| :o_skadtabela ||' set odbiorca_sente = '|| :odbiorca ||
              ' where ref = '|| :o_skadref;
            execute statement sql;
          end

          if (blokujzalezne = 0) then --tabele zalezne after
          begin
            select status, msg from int_tabelezalezne_process(:sesja,
              :stabela, :ref, :szrodlo, :skierunek, 1)
              into :tmpstatus, :tmpmsg;
          end

        end
        if(error_row = 1)then
        begin
          status = -1; --ML jezeli nie przetworzy sie chociaz jeden wiersz przetwarzanie nieudane;
          errormsg_all = substring(errormsg_all || :errormsg_row from 1 for 255);
        end
        update int_imp_kli_odbiorcy set klient = :klient, odbiorca = :odbiorca,
          dataostprzetw = current_timestamp(0),
          error = :error_row,
          errormsg = :errormsg_row
          where ref = :o_ref;
    end

  if(coalesce(status,-1) != 1) then
    msg = substring((msg || :errormsg_all) from 1 for 255);

  --aktualizacja informacji o sesji
  if (aktualizujsesje = 1) then
  begin
    select status, msg from int_sesje_aktualizuj(:sesja, :status) into :tmpstatus, :tmpmsg;
    if(coalesce(status,-1) = 1) then
      status = :tmpstatus; --ML jezeli przed aktualizacja sesji status byl zly, to nie zmieniamy na ok
    msg = substring((msg || coalesce(:tmpmsg,'')) from 1 for 255);
  end
  suspend;
end^
SET TERM ; ^
