--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_POZYCJE_ZALADOWANE(
      LISTWYSD INTEGER_ID)
  returns (
      OPAKOWANIE STRING40,
      STATUS STRING10,
      LOKACJA STRING40,
      RODZIC STRING40)
   as
  declare variable rodzicref integer_id;
  declare variable listywysref integer_id;
begin
  select ld.listawys
    from listywysd ld
    where ld.ref = :LISTWYSD
  into listywysref;




  for
    select l.stanowisko,
      case coalesce(l.x_zaladowane, 0)
        when 1 then 'Zaladowany'
        when 0 then 'nie'
       end, l.x_mwsconstlock, coalesce(l.rodzic, 0)
      from listywysdroz_opk l
      join listywysd ld on (ld.ref = l.listwysd)
        where l.listwysd = :listwysd
         or ld.listawys = :listywysref
        order by l.x_zaladowane, l.stanowisko asc
    into :opakowanie, :status, :lokacja , :rodzicref
  do begin
    rodzic = '';
    if (lokacja is null) then lokacja = '-';
    if (rodzicref <> 0) then
      select l.stanowisko
        from listywysdroz_opk l
        where l.ref = :rodzicref
      into rodzic;
    suspend;
  end
end^
SET TERM ; ^
