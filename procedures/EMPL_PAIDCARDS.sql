--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMPL_PAIDCARDS(
      REF integer,
      IS_EMPL smallint)
  returns (
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      P100 numeric(14,2),
      P101 numeric(14,2),
      P102 numeric(14,2),
      P200 numeric(14,2))
   as
begin
  --DS: procedura do wyliczania karty zarobkowej pracownika lub osoby
  if (is_empl is null) then is_empl = 0;
  --aby pokazaly sie wszystkie okresy, takze te z zerowymi wartosciami na liscie plac
  for
    select distinct p.cper
      from epayrolls p
        join eprpos r on r.payroll = p.ref
        join employees e on e.ref = r.employee
      where (e.person = :ref and :is_empl = 0)
        or (e.ref = :ref and :is_empl = 1)
      order by p.cper
      into :period
  do begin
    p100=null;p101=null;p102=null;p200=null;

    select sum(P.pvalue) as P100
    from EPAYROLLS PR
      left join EPRPOS P on (P.payroll = PR.ref and P.ecolumn = 1000)
      left join employees e on (e.ref = p.employee)
    where ((e.person = :ref and :is_empl = 0)
        or (e.ref = :ref and :is_empl = 1))
      and pr.cper = :period
    into :p100;

    select sum(P.pvalue) as P101
      from EPAYROLLS PR
        left join EPRPOS P on (P.payroll = PR.ref and P.ecolumn = 1010)
        left join employees e on (e.ref = p.employee)
      where ((e.person = :ref and :is_empl = 0)
          or (e.ref = :ref and :is_empl = 1))
        and pr.cper = :period
      into :p101;

    select sum(P.pvalue) as P102
      from EPAYROLLS PR
        left join EPRPOS P on (P.payroll = PR.ref and P.ecolumn = 1020)
        left join employees e on (e.ref = p.employee)
      where ((e.person = :ref and :is_empl = 0)
          or (e.ref = :ref and :is_empl = 1))
        and pr.cper = :period
      into :p102;

    select sum(P.pvalue) as P200
      from EPAYROLLS PR
        left join EPRPOS P on (P.payroll = PR.ref and P.ecolumn = 2000)
        left join employees e on (e.ref = p.employee)
      where ((e.person = :ref and :is_empl = 0)
          or (e.ref = :ref and :is_empl = 1))
        and pr.cper = :period
      into :p200;

    if (p100 is null) then p100 = 0;
    if (p101 is null) then p101 = 0;
    if (p102 is null) then p102 = 0;
    if (p200 is null) then p200 = 0;

    suspend;
  end
end^
SET TERM ; ^
