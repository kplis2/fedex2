--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_ZAM_POZYCJE_INS(
      NAGID type of column INT_IMP_ZAM_POZYCJE.NAGID,
      POZID type of column INT_IMP_ZAM_POZYCJE.POZID,
      NUMER type of column INT_IMP_ZAM_POZYCJE.NUMER,
      TOWARID type of column INT_IMP_ZAM_POZYCJE.TOWARID,
      JEDNOSTKA type of column INT_IMP_ZAM_POZYCJE.JEDNOSTKA,
      JEDNOSTKAID type of column INT_IMP_ZAM_POZYCJE.JEDNOSTKAID,
      VAT type of column INT_IMP_ZAM_POZYCJE.VAT,
      MAGAZYN type of column INT_IMP_ZAM_POZYCJE.MAGAZYN,
      MAGAZYN2 type of column INT_IMP_ZAM_POZYCJE.MAGAZYN2,
      ILOSC type of column INT_IMP_ZAM_POZYCJE.ILOSC,
      CENANETTO type of column INT_IMP_ZAM_POZYCJE.CENANETTO,
      CENABRUTTO type of column INT_IMP_ZAM_POZYCJE.CENABRUTTO,
      CENANETTOZL type of column INT_IMP_ZAM_POZYCJE.CENANETTOZL,
      CENABRUTTOZL type of column INT_IMP_ZAM_POZYCJE.CENABRUTTOZL,
      WARTOSCNETTO type of column INT_IMP_ZAM_POZYCJE.WARTOSCNETTO,
      WARTOSCBRUTTO type of column INT_IMP_ZAM_POZYCJE.WARTOSCBRUTTO,
      WARTOSCNETTOZL type of column INT_IMP_ZAM_POZYCJE.WARTOSCNETTOZL,
      WARTOSCBRUTTOZL type of column INT_IMP_ZAM_POZYCJE.WARTOSCBRUTTOZL,
      RABATWYLICZ type of column INT_IMP_ZAM_POZYCJE.RABATWYLICZ,
      RABATPROCENT type of column INT_IMP_ZAM_POZYCJE.RABATPROCENT,
      RABATWARTOSC type of column INT_IMP_ZAM_POZYCJE.RABATWARTOSC,
      TYPDOKFAK type of column INT_IMP_ZAM_POZYCJE.TYPDOKFAK,
      TYPDOKMAG type of column INT_IMP_ZAM_POZYCJE.TYPDOKMAG,
      PARTIA type of column INT_IMP_ZAM_POZYCJE.PARTIA,
      PARTIAID type of column INT_IMP_ZAM_POZYCJE.PARTIAID,
      SKADTABELA type of column INT_IMP_ZAM_POZYCJE.SKADTABELA,
      SKADREF type of column INT_IMP_ZAM_POZYCJE.SKADREF,
      HASHVALUE type of column INT_IMP_ZAM_POZYCJE.HASH,
      DEL type of column INT_IMP_ZAM_POZYCJE.DEL,
      REC type of column INT_IMP_ZAM_POZYCJE.REC,
      SESJA type of column INT_IMP_ZAM_POZYCJE.SESJA)
  returns (
      REF type of column INT_IMP_ZAM_POZYCJE.REF)
   as
begin
  insert into int_imp_zam_pozycje (nagid, pozid, numer, towarid, jednostka, jednostkaid, vat, magazyn, magazyn2,
      ilosc, cenanetto, cenabrutto, cenanettozl, cenabruttozl, wartoscnetto, wartoscbrutto, wartoscnettozl, wartoscbruttozl,
      rabatwylicz, rabatprocent, rabatwartosc, typdokfak, typdokmag, partia, partiaid,
      "HASH", del, rec, skadtabela, skadref, sesja)
    values (:nagid, :pozid, :numer, :towarid, :jednostka, :jednostkaid, :vat, :magazyn, :magazyn2,
      :ilosc, :cenanetto, :cenabrutto, :cenanettozl, :cenabruttozl, :wartoscnetto, :wartoscbrutto, :wartoscnettozl, :wartoscbruttozl,
      :rabatwylicz, :rabatprocent, :rabatwartosc, :typdokfak, :typdokmag, :partia, :partiaid,
      :hashvalue, :del, :rec, :skadtabela, :skadref, :sesja)
    returning ref into :ref;
end^
SET TERM ; ^
