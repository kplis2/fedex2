--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MS_CRM_ADD_MAIL(
      OD varchar(512) CHARACTER SET UTF8                           ,
      DOKOGO varchar(2048) CHARACTER SET UTF8                           ,
      CC varchar(2048) CHARACTER SET UTF8                           ,
      TYTUL varchar(128) CHARACTER SET UTF8                           ,
      TRESC varchar(5000) CHARACTER SET UTF8                           ,
      OPERATOR integer,
      CPODMIOT integer,
      KONTAKT integer,
      OKRES char(6) CHARACTER SET UTF8                           ,
      FOLDER integer)
  returns (
      EMAILWIAD_REF integer)
   as
declare variable data timestamp;
begin
  data=current_date;
  execute procedure gen_ref('EMAILWIAD')
    returning_values :emailwiad_ref;
  insert into emailwiad(ref, data, od, dokogo, cc, tytul, tresc, folder,  okres, cpodmiot, kontakt, operator, stan, plik, idwatku, email)
    values(:emailwiad_ref, :data, :od, :dokogo, :cc, :tytul, :tresc, :folder, :okres,  :cpodmiot,  :kontakt,  :operator, 0, :emailwiad_ref||'.eml', :emailwiad_ref, 1 );
  suspend;
end^
SET TERM ; ^
