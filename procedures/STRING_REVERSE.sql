--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STRING_REVERSE(
      STRING varchar(40) CHARACTER SET UTF8                           )
  returns (
      NEWSTRING varchar(40) CHARACTER SET UTF8                           )
   as
declare variable len smallint;
declare variable counter_tmp smallint;
declare variable counter smallint;
declare variable sign varchar(1);
begin
  len = coalesce(char_length(:string),0); -- [DG] XXX ZG119346
  if (:len = 0) then
    exit;
  counter = 0;
  newstring = '';
  while (:counter < :len)
  do begin
    counter = counter + 1;
    sign = substring(:string from 1 for 1);
    string = substring(:string from  2 for  :len - :counter + 1);
    newstring = :sign||:newstring;
  end
end^
SET TERM ; ^
