--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EFUNC_GET_EMPLOYEESTAXSCALE(
      PAYROLL integer,
      QTYPE smallint,
      CURRENTCOMPANY integer)
  returns (
      PERSONNAMES varchar(120) CHARACTER SET UTF8                           ,
      TAXSCALE numeric(14,2),
      TAXSCALE1 numeric(14,2))
   as
declare variable tper char(6);
declare variable employee integer;
declare variable income numeric(14,2);
declare variable income1 numeric(14,2);
declare variable amount numeric(14,2);
declare variable amount1 numeric(14,2);
declare variable taxyear integer;
declare variable leveldec smallint;
declare variable tmpamount numeric(14,2);
declare variable fromdate timestamp;
declare variable todate timestamp;
begin

  select tper
    from epayrolls
    where ref = :payroll
    into :tper;

  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  for
    select E.ref, E.personnames
      from employees E
        join emplcontracts C on (E.ref = C.employee)
      where C.empltype = 1 and E.company = :currentcompany
        and (C.fromdate <= :todate and (C.enddate >= :fromdate or C.enddate is null))
      group by E.personnames, E.ref
      into :employee, :personnames
  do begin

    select sum(P.pvalue)
      from epayrolls PR
        join eprpos P on (P.ecolumn = 7000 and P.payroll = PR.ref)
      where PR.empltype = 1 and PR.tper < :tper
        and PR.tper like substring(:tper from 1 for 4) || '%'
        and P.employee = :employee
      into :income;

    select sum(P.pvalue)
      from epayrolls PR
        join eprpos P on (P.ecolumn = 7000 and P.payroll = PR.ref)
      where PR.empltype = 1 and PR.tper <= :tper
        and PR.tper like substring(:tper from 1 for 4) || '%'
        and P.employee = :employee
      into :income1;

    if (income is null) then
      income = 0 ;
    if (income1 is null) then
      income1 = 0;

    taxyear = cast(substring(:tper from 1 for 4) as integer);

    select max(amount)
      from etaxlevels
      where amount <= :income and taxyear = :taxyear
      into :amount;

    select max(amount)
      from etaxlevels
      where amount <= :income1 and taxyear = :taxyear
      into :amount1;

    if (amount is null or amount1 is null) then
      exception NO_TAX_LEVELS_FOR_THIS_YEAR;

    execute procedure efunc_prdates(payroll, 1)
      returning_values fromdate, todate;

    select leveldec
      from empltaxinfo
      where employee = :employee
        and fromdate = (select max(fromdate)
          from empltaxinfo
          where employee = :employee and fromdate <= :todate)
      into :leveldec;

    if (leveldec = 1) then
    begin
      select max(amount)
        from etaxlevels
        where amount < :amount and taxyear = :taxyear
        into :tmpamount;
      if (tmpamount is not null) then
        amount = tmpamount;

      select max(amount)
        from etaxlevels
        where amount < :amount1 and taxyear = :taxyear
        into :tmpamount;
      if (tmpamount is not null) then
        amount1 = tmpamount;
    end

    select taxscale
      from etaxlevels
      where amount = :amount and taxyear = :taxyear
      into :taxscale;

    select taxscale
      from etaxlevels
      where amount = :amount1 and taxyear = :taxyear
      into :taxscale1;
    if (taxscale <> taxscale1 or qtype = 1) then
      suspend;
  end
end^
SET TERM ; ^
