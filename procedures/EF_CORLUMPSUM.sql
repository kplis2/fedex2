--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_CORLUMPSUM(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      COL1 integer,
      COL2 integer = 0)
  returns (
      RET numeric(14,2))
   as
declare variable tper char(6);
declare variable person integer;
begin
/*MWr: Personel - Pobranie sumy wartosci skladnikow od COL1 do COL2 z rachunkow
   UCP z polem corlumpsumtax = payroll. Jeżeli COL2 nie zostanie podany, to
   funkcja dodtyczy tlko pierwszego skladnika COL1. Procedura stworzona przy
   przenoszeniu funkcjonlanosci EF_UCPTAX na bezposredni zapis w alg. placowych */

  if (coalesce(col2,0) = 0) then
    col2 = col1;

  select person from employees where ref = :employee
    into :person;

  select tper from epayrolls where ref = :payroll
    into tper;

  select sum(p.pvalue)
    from eprpos p
      join epayrolls r on (p.payroll = r.ref)
      join eprempl e on (e.epayroll = p.payroll and e.employee = p.employee)
    where e.person = :person and r.tper = :tper and p.ecolumn >= :col1 and p.ecolumn <= :col2
      and r.corlumpsumtax = :payroll
    into :ret;

  if (ret is null) then
    ret = 0;

  suspend;
end^
SET TERM ; ^
