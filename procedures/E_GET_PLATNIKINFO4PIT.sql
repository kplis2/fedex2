--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_PLATNIKINFO4PIT
  returns (
      NIP STRING20,
      PHISICAL SMALLINT_ID,
      DESCRIPT STRING255)
   as
begin

  execute procedure getconfig('INFONIP') returning_values :nip;
  nip = replace(nip,'-','');
  nip = replace(nip,' ','');

--jesli ZUSNAZWISKO nieuzupelnione, to zakladamym, ze platnik jest osoba niefizyczna
  select coalesce(cvalue,'') from get_config('ZUSNAZWISKO',null) into :descript;

  if (descript = '') then
  begin
    phisical = 1;  --osoba niefizyczna
    select cvalue from get_config('INFO1',null) into :descript;
    select :descript||', '||cvalue from get_config('INFOREGON',null) into :descript;
  end else
  begin
    phisical = 2;  --osoba fizyczna
    select :descript||', '||cvalue from get_config('ZUSIMIEPIER',null) into :descript;
    select :descript||', '||cvalue from get_config('ZUSDATAURPDZENIA',null) into :descript;
  end
  suspend;
end^
SET TERM ; ^
