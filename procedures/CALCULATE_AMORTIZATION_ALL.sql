--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CALCULATE_AMORTIZATION_ALL(
      AMPERIOD integer,
      COMPANY integer)
   as
declare variable i_ref integer;
begin
  for
    select ref from fxdassets
      where (liquidationperiod is null or liquidationperiod > :amperiod)
        and fxdassets.company = :company
      into :i_ref
  do
    execute procedure calculate_amortization(i_ref, AMPERIOD);
end^
SET TERM ; ^
