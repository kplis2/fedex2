--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_INDEX_DDL(
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      CREATE_OR_ALTER smallint)
  returns (
      DDL blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           )
   as
declare variable INDEX_RELATION_NAME varchar(80);
declare variable INDEX_UNIQUE_FLAG smallint;
declare variable INDEX_TYPE smallint;
declare variable INDEX_INACTIVE smallint;
declare variable INDEX_FIELD varchar(1024);
declare variable EOL varchar(2);
declare variable INDEX_EXPRESSION varchar(1024);
begin
  eol ='
  ';
  ddl = '';
  nazwa = substring(:nazwa from position('.' in :nazwa)+1);
  if(exists(select first 1 1 from rdb$relation_constraints where rdb$constraint_name = :nazwa)) then
    execute procedure sys_constraint_ddl(:nazwa, :create_or_alter) returning_values :ddl;
  else
  begin
    if(:create_or_alter = 1) then
      ddl = :ddl || 'DROP INDEX ' || :nazwa || ';' || :eol;

    ddl = :ddl || 'CREATE ';
    select i.rdb$relation_name, i.rdb$unique_flag, i.rdb$index_type, i.rdb$index_inactive, i.rdb$expression_source
      from rdb$indices i
      where i.rdb$index_name = :nazwa
      into :index_relation_name, :index_unique_flag, :index_type, :index_inactive, :index_expression;
    index_relation_name = trim(:index_relation_name);

    if(index_unique_flag = 1) then
      ddl = :ddl || 'UNIQUE ';

    if(index_type = 1) then
      ddl = :ddl || 'DESCENDING ';

    ddl = :ddl || 'INDEX ' || :nazwa || ' ON ' || :index_relation_name;

    /* Różne ddl dla indeksu na formule ... */
    if(:index_expression is not null) then
    begin
      ddl = :ddl || ' COMPUTED BY ' || :index_expression || ';';
    end
    else
    /* ... i dla indeksu na pola */
    begin
      ddl = :ddl || '(';
      for select s.rdb$field_name
        from rdb$index_segments s
        where s.rdb$index_name = :nazwa
        order by s.rdb$field_position
        into :index_field
      do begin
        index_field = substring(:index_field from 1 for position(' ', :index_field) - 1);
        ddl = :ddl || :index_field ||', ';
      end
      ddl = substring(:ddl from 1 for char_length(:ddl)-2);
      ddl = :ddl || ');';
    end
  end
  suspend;
end^
SET TERM ; ^
