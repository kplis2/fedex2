--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDEL_SETFORALL_DAYS(
      EDELDAY integer)
   as
  declare variable breakfast smallint;
  declare variable dinner smallint;
  declare variable supper smallint;
  declare variable citytransport smallint;
  declare variable toairport smallint;
  declare variable accommodation smallint;
  declare variable meals smallint;
  declare variable delegation integer;
begin
  select delegation, breakfast,dinner,supper,citytransport,toairport,meals,accommodation
    from edeldays
    where ref = :edelday
  into :delegation, :breakfast, :dinner, :supper, :citytransport, :toairport,:meals, :accommodation;

  update edeldays set
    breakfast=:breakfast,
    dinner = :dinner,
    supper = :supper,
    citytransport = :citytransport,
    toairport = :toairport,
    meals = :meals,
    accommodation = :accommodation
  where delegation=:delegation;
end^
SET TERM ; ^
