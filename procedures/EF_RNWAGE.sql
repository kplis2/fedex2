--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_RNWAGE(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      FLAG varchar(3) CHARACTER SET UTF8                            = null)
  returns (
      SUMWAGE numeric(14,4))
   as
declare variable fromdate date;
declare variable todate date;
declare variable salary numeric(14,2);
declare variable efromdate date;
declare variable etodate date;
declare variable caldays smallint;
declare variable ecaldays numeric(14,2);
begin
/*MWr: Personel - do liczenia wynagrodzenia dla Rady Nadzorczej z uwzglednieniem
                  zmian w przebiegu zatrudnienia
  DS: gdy parametr flag jest ustawiony, liczone sa wylacznie umowy z zaznaczona odpowiednia flaga,
      uzywane przy wyliczeniu podstawy NFZ */

  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  sumwage = 0;
  if (fromdate is not null) then
  begin
    caldays = todate - fromdate + 1;
    for
      select salary, fromdate,
             case when enddate is null then todate else enddate end
        from emplcontracts c
        where employee = :employee
          and empltype = 3
          and fromdate <= :todate
          and ((enddate is not null and enddate >= :fromdate)
            or (enddate is null and (todate is null or todate >= :fromdate)))
          and (c.iflags containing ';'||:flag||';' or :flag is null)
        order by fromdate
        into :salary, :efromdate, :etodate
    do begin
      if (etodate is null or etodate > todate) then
        etodate = todate;
      if (efromdate < fromdate) then
        efromdate = fromdate;
      ecaldays = etodate - efromdate + 1;
      sumwage = sumwage + salary * ecaldays / caldays;
    end
  end

  suspend;
end^
SET TERM ; ^
