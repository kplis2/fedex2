--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_STANYIL(
      MAGAZYNY varchar(255) CHARACTER SET UTF8                           ,
      MASKAKTM varchar(40) CHARACTER SET UTF8                           )
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      WERSJAREF integer,
      NAZWAT varchar(255) CHARACTER SET UTF8                           ,
      NAZWAW varchar(255) CHARACTER SET UTF8                           ,
      MIARA varchar(5) CHARACTER SET UTF8                           ,
      ILOSC numeric(14,4),
      ZABLOKOW numeric(14,4),
      ZAREZERW numeric(14,4),
      ZAMOWIONO numeric(14,4),
      STANMIN numeric(14,4),
      STANMAX numeric(14,4))
   as
begin
  for select s.ktm, s.wersja, s.wersjaref, max(s.nazwat), max(s.wnazwa), min(s.miara),
  sum(s.ilosc), sum(s.zablokow), sum(s.zarezerw),
  sum(s.zamowiono), max(s.stanmin), min(s.stanmax)
  from stanyil s
  where :magazyny like '%;'||s.magazyn||';%'
  and s.ktm like :maskaktm
  group by s.ktm, s.wersja, s.wersjaref
  into :ktm, :wersja, :wersjaref, :nazwat, :nazwaw, :miara,
  :ilosc, :zablokow, :zarezerw,
  :zamowiono, :stanmin, :stanmax
  do begin
    if(:stanmin is null) then stanmin = 0;
    if(:stanmax is null) then stanmax = 0;
    if(:zamowiono is null) then zamowiono = 0;
    if(:zablokow is null) then zablokow = 0;
    if(:zarezerw is null) then zarezerw = 0;
    suspend;
  end
end^
SET TERM ; ^
