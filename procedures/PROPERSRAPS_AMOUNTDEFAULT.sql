--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PROPERSRAPS_AMOUNTDEFAULT(
      PRSCHEDOPER integer,
      ECONTRACTSDEF integer,
      ECONTRACTSPOSDEF integer)
  returns (
      AMOUNT numeric(14,4))
   as
declare variable amountin numeric(14,4);
declare variable amountshortage numeric(14,4);
declare variable amountused numeric(14,4);
declare variable reportedfactor numeric(14,4);
begin
  amount = 0;
  select  pscho.amountin, pscho.amountshortages, psho.reportedfactor
    from prschedopers pscho
    left join prshopers psho on (pscho.shoper = psho.ref)
    where pscho.ref = :prschedoper
  into :amountin, :amountshortage, :reportedfactor;
  if(reportedfactor is null or reportedfactor = 0) then reportedfactor = 1;
  if(econtractsdef = 0) then econtractsdef = null;
  if(econtractsposdef = 0) then econtractsposdef = null;
  select sum(amount)
    from propersraps
    where (:econtractsdef is null or econtractsdef = :econtractsdef)
      and (:econtractsposdef is null or econtractsposdef = :econtractsposdef)
      and prschedoper = :prschedoper
  into :amountused;
  if(amountused is null) then amountused = 0;
  amountin = amountin * reportedfactor;
  if(amountin - amountshortage - amountused > 0) then
    amount = amountin - amountshortage - amountused;
  suspend;
end^
SET TERM ; ^
