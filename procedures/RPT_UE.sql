--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_UE(
      PERIOD varchar(1) CHARACTER SET UTF8                           ,
      YEARID varchar(4) CHARACTER SET UTF8                           ,
      COMPANY integer)
  returns (
      CA1 varchar(2) CHARACTER SET UTF8                           ,
      CB1 varchar(15) CHARACTER SET UTF8                           ,
      CC1 numeric(14,2),
      CD1 varchar(1) CHARACTER SET UTF8                           ,
      CA2 varchar(2) CHARACTER SET UTF8                           ,
      CB2 varchar(15) CHARACTER SET UTF8                           ,
      CC2 numeric(14,2),
      CD2 varchar(1) CHARACTER SET UTF8                           ,
      CA3 varchar(2) CHARACTER SET UTF8                           ,
      CB3 varchar(15) CHARACTER SET UTF8                           ,
      CC3 numeric(14,2),
      CD3 varchar(1) CHARACTER SET UTF8                           ,
      CA4 varchar(2) CHARACTER SET UTF8                           ,
      CB4 varchar(15) CHARACTER SET UTF8                           ,
      CC4 numeric(14,2),
      CD4 varchar(1) CHARACTER SET UTF8                           ,
      CA5 varchar(2) CHARACTER SET UTF8                           ,
      CB5 varchar(15) CHARACTER SET UTF8                           ,
      CC5 numeric(14,2),
      CD5 varchar(1) CHARACTER SET UTF8                           ,
      CA6 varchar(2) CHARACTER SET UTF8                           ,
      CB6 varchar(15) CHARACTER SET UTF8                           ,
      CC6 numeric(14,2),
      CD6 varchar(1) CHARACTER SET UTF8                           ,
      CA7 varchar(2) CHARACTER SET UTF8                           ,
      CB7 varchar(15) CHARACTER SET UTF8                           ,
      CC7 numeric(14,2),
      CD7 varchar(1) CHARACTER SET UTF8                           ,
      CA8 varchar(2) CHARACTER SET UTF8                           ,
      CB8 varchar(15) CHARACTER SET UTF8                           ,
      CC8 numeric(14,2),
      CD8 varchar(1) CHARACTER SET UTF8                           ,
      CA9 varchar(2) CHARACTER SET UTF8                           ,
      CB9 varchar(15) CHARACTER SET UTF8                           ,
      CC9 numeric(14,2),
      CD9 varchar(1) CHARACTER SET UTF8                           ,
      CA10 varchar(2) CHARACTER SET UTF8                           ,
      CB10 varchar(15) CHARACTER SET UTF8                           ,
      CC10 numeric(14,2),
      CD10 varchar(1) CHARACTER SET UTF8                           ,
      CA11 varchar(2) CHARACTER SET UTF8                           ,
      CB11 varchar(15) CHARACTER SET UTF8                           ,
      CC11 numeric(14,2),
      CD11 varchar(1) CHARACTER SET UTF8                           ,
      CA12 varchar(2) CHARACTER SET UTF8                           ,
      CB12 varchar(15) CHARACTER SET UTF8                           ,
      CC12 numeric(14,2),
      CD12 varchar(1) CHARACTER SET UTF8                           ,
      CA13 varchar(2) CHARACTER SET UTF8                           ,
      CB13 varchar(15) CHARACTER SET UTF8                           ,
      CC13 numeric(14,2),
      CD13 varchar(1) CHARACTER SET UTF8                           ,
      CA14 varchar(2) CHARACTER SET UTF8                           ,
      CB14 varchar(15) CHARACTER SET UTF8                           ,
      CC14 numeric(14,2),
      CD14 varchar(1) CHARACTER SET UTF8                           ,
      CA15 varchar(2) CHARACTER SET UTF8                           ,
      CB15 varchar(15) CHARACTER SET UTF8                           ,
      CC15 numeric(14,2),
      CD15 varchar(1) CHARACTER SET UTF8                           ,
      CA16 varchar(2) CHARACTER SET UTF8                           ,
      CB16 varchar(15) CHARACTER SET UTF8                           ,
      CC16 numeric(14,2),
      CD16 varchar(1) CHARACTER SET UTF8                           ,
      CA17 varchar(2) CHARACTER SET UTF8                           ,
      CB17 varchar(15) CHARACTER SET UTF8                           ,
      CC17 numeric(14,2),
      CD17 varchar(1) CHARACTER SET UTF8                           ,
      CA18 varchar(2) CHARACTER SET UTF8                           ,
      CB18 varchar(15) CHARACTER SET UTF8                           ,
      CC18 numeric(14,2),
      CD18 varchar(1) CHARACTER SET UTF8                           ,
      CA19 varchar(2) CHARACTER SET UTF8                           ,
      CB19 varchar(15) CHARACTER SET UTF8                           ,
      CC19 numeric(14,2),
      CD19 varchar(1) CHARACTER SET UTF8                           ,
      CA20 varchar(2) CHARACTER SET UTF8                           ,
      CB20 varchar(15) CHARACTER SET UTF8                           ,
      CC20 numeric(14,2),
      CD20 varchar(1) CHARACTER SET UTF8                           ,
      CA21 varchar(2) CHARACTER SET UTF8                           ,
      CB21 varchar(15) CHARACTER SET UTF8                           ,
      CC21 numeric(14,2),
      CD21 varchar(1) CHARACTER SET UTF8                           ,
      CA22 varchar(2) CHARACTER SET UTF8                           ,
      CB22 varchar(15) CHARACTER SET UTF8                           ,
      CC22 numeric(14,2),
      CD22 varchar(1) CHARACTER SET UTF8                           ,
      CA23 varchar(2) CHARACTER SET UTF8                           ,
      CB23 varchar(15) CHARACTER SET UTF8                           ,
      CC23 numeric(14,2),
      CD23 varchar(1) CHARACTER SET UTF8                           ,
      CA24 varchar(2) CHARACTER SET UTF8                           ,
      CB24 varchar(15) CHARACTER SET UTF8                           ,
      CC24 numeric(14,2),
      CD24 varchar(1) CHARACTER SET UTF8                           ,
      CA25 varchar(2) CHARACTER SET UTF8                           ,
      CB25 varchar(15) CHARACTER SET UTF8                           ,
      CC25 numeric(14,2),
      CD25 varchar(1) CHARACTER SET UTF8                           ,
      CA26 varchar(2) CHARACTER SET UTF8                           ,
      CB26 varchar(15) CHARACTER SET UTF8                           ,
      CC26 numeric(14,2),
      CD26 varchar(1) CHARACTER SET UTF8                           ,
      CA27 varchar(2) CHARACTER SET UTF8                           ,
      CB27 varchar(15) CHARACTER SET UTF8                           ,
      CC27 numeric(14,2),
      CD27 varchar(1) CHARACTER SET UTF8                           ,
      CA28 varchar(2) CHARACTER SET UTF8                           ,
      CB28 varchar(15) CHARACTER SET UTF8                           ,
      CC28 numeric(14,2),
      CD28 varchar(1) CHARACTER SET UTF8                           ,
      CA29 varchar(2) CHARACTER SET UTF8                           ,
      CB29 varchar(15) CHARACTER SET UTF8                           ,
      CC29 numeric(14,2),
      CD29 varchar(1) CHARACTER SET UTF8                           ,
      CA30 varchar(2) CHARACTER SET UTF8                           ,
      CB30 varchar(15) CHARACTER SET UTF8                           ,
      CC30 numeric(14,2),
      CD30 varchar(1) CHARACTER SET UTF8                           ,
      DA1 varchar(2) CHARACTER SET UTF8                           ,
      DB1 varchar(15) CHARACTER SET UTF8                           ,
      DC1 numeric(14,2),
      DD1 varchar(1) CHARACTER SET UTF8                           ,
      DA2 varchar(2) CHARACTER SET UTF8                           ,
      DB2 varchar(15) CHARACTER SET UTF8                           ,
      DC2 numeric(14,2),
      DD2 varchar(1) CHARACTER SET UTF8                           ,
      DA3 varchar(2) CHARACTER SET UTF8                           ,
      DB3 varchar(15) CHARACTER SET UTF8                           ,
      DC3 numeric(14,2),
      DD3 varchar(1) CHARACTER SET UTF8                           ,
      DA4 varchar(2) CHARACTER SET UTF8                           ,
      DB4 varchar(15) CHARACTER SET UTF8                           ,
      DC4 numeric(14,2),
      DD4 varchar(1) CHARACTER SET UTF8                           ,
      DA5 varchar(2) CHARACTER SET UTF8                           ,
      DB5 varchar(15) CHARACTER SET UTF8                           ,
      DC5 numeric(14,2),
      DD5 varchar(1) CHARACTER SET UTF8                           ,
      DA6 varchar(2) CHARACTER SET UTF8                           ,
      DB6 varchar(15) CHARACTER SET UTF8                           ,
      DC6 numeric(14,2),
      DD6 varchar(1) CHARACTER SET UTF8                           ,
      DA7 varchar(2) CHARACTER SET UTF8                           ,
      DB7 varchar(15) CHARACTER SET UTF8                           ,
      DC7 numeric(14,2),
      DD7 varchar(1) CHARACTER SET UTF8                           ,
      DA8 varchar(2) CHARACTER SET UTF8                           ,
      DB8 varchar(15) CHARACTER SET UTF8                           ,
      DC8 numeric(14,2),
      DD8 varchar(1) CHARACTER SET UTF8                           ,
      DA9 varchar(2) CHARACTER SET UTF8                           ,
      DB9 varchar(15) CHARACTER SET UTF8                           ,
      DC9 numeric(14,2),
      DD9 varchar(1) CHARACTER SET UTF8                           ,
      DA10 varchar(2) CHARACTER SET UTF8                           ,
      DB10 varchar(15) CHARACTER SET UTF8                           ,
      DC10 numeric(14,2),
      DD10 varchar(1) CHARACTER SET UTF8                           ,
      DA11 varchar(2) CHARACTER SET UTF8                           ,
      DB11 varchar(15) CHARACTER SET UTF8                           ,
      DC11 numeric(14,2),
      DD11 varchar(1) CHARACTER SET UTF8                           ,
      DA12 varchar(2) CHARACTER SET UTF8                           ,
      DB12 varchar(15) CHARACTER SET UTF8                           ,
      DC12 numeric(14,2),
      DD12 varchar(1) CHARACTER SET UTF8                           ,
      DA13 varchar(2) CHARACTER SET UTF8                           ,
      DB13 varchar(15) CHARACTER SET UTF8                           ,
      DC13 numeric(14,2),
      DD13 varchar(1) CHARACTER SET UTF8                           ,
      DA14 varchar(2) CHARACTER SET UTF8                           ,
      DB14 varchar(15) CHARACTER SET UTF8                           ,
      DC14 numeric(14,2),
      DD14 varchar(1) CHARACTER SET UTF8                           ,
      DA15 varchar(2) CHARACTER SET UTF8                           ,
      DB15 varchar(15) CHARACTER SET UTF8                           ,
      DC15 numeric(14,2),
      DD15 varchar(1) CHARACTER SET UTF8                           ,
      DA16 varchar(2) CHARACTER SET UTF8                           ,
      DB16 varchar(15) CHARACTER SET UTF8                           ,
      DC16 numeric(14,2),
      DD16 varchar(1) CHARACTER SET UTF8                           ,
      DA17 varchar(2) CHARACTER SET UTF8                           ,
      DB17 varchar(15) CHARACTER SET UTF8                           ,
      DC17 numeric(14,2),
      DD17 varchar(1) CHARACTER SET UTF8                           ,
      DA18 varchar(2) CHARACTER SET UTF8                           ,
      DB18 varchar(15) CHARACTER SET UTF8                           ,
      DC18 numeric(14,2),
      DD18 varchar(1) CHARACTER SET UTF8                           ,
      DA19 varchar(2) CHARACTER SET UTF8                           ,
      DB19 varchar(15) CHARACTER SET UTF8                           ,
      DC19 numeric(14,2),
      DD19 varchar(1) CHARACTER SET UTF8                           ,
      DA20 varchar(2) CHARACTER SET UTF8                           ,
      DB20 varchar(15) CHARACTER SET UTF8                           ,
      DC20 numeric(14,2),
      DD20 varchar(1) CHARACTER SET UTF8                           ,
      DA21 varchar(2) CHARACTER SET UTF8                           ,
      DB21 varchar(15) CHARACTER SET UTF8                           ,
      DC21 numeric(14,2),
      DD21 varchar(1) CHARACTER SET UTF8                           ,
      DA22 varchar(2) CHARACTER SET UTF8                           ,
      DB22 varchar(15) CHARACTER SET UTF8                           ,
      DC22 numeric(14,2),
      DD22 varchar(1) CHARACTER SET UTF8                           ,
      DA23 varchar(2) CHARACTER SET UTF8                           ,
      DB23 varchar(15) CHARACTER SET UTF8                           ,
      DC23 numeric(14,2),
      DD23 varchar(1) CHARACTER SET UTF8                           ,
      DA24 varchar(2) CHARACTER SET UTF8                           ,
      DB24 varchar(15) CHARACTER SET UTF8                           ,
      DC24 numeric(14,2),
      DD24 varchar(1) CHARACTER SET UTF8                           ,
      DA25 varchar(2) CHARACTER SET UTF8                           ,
      DB25 varchar(15) CHARACTER SET UTF8                           ,
      DC25 numeric(14,2),
      DD25 varchar(1) CHARACTER SET UTF8                           ,
      DA26 varchar(2) CHARACTER SET UTF8                           ,
      DB26 varchar(15) CHARACTER SET UTF8                           ,
      DC26 numeric(14,2),
      DD26 varchar(1) CHARACTER SET UTF8                           ,
      DA27 varchar(2) CHARACTER SET UTF8                           ,
      DB27 varchar(15) CHARACTER SET UTF8                           ,
      DC27 numeric(14,2),
      DD27 varchar(1) CHARACTER SET UTF8                           )
   as
declare variable ile integer;
declare variable niptmp varchar(15);
declare variable nipue varchar(15);
declare variable nipuetmp varchar(15);
declare variable netto numeric(14,2);
declare variable odokresu varchar(2);
declare variable dookresu varchar(2);
begin
  ile = 1;
  cc1 = 0;
  cc2 = 0;
  cc3 = 0;
  cc4 = 0;
  cc5 = 0;
  cc6 = 0;
  cc7 = 0;
  cc8 = 0;
  cc9 = 0;
  cc10 = 0;
  cc11 = 0;
  cc12 = 0;
  cc13 = 0;
  cc14 = 0;
  cc15 = 0;
  cc16 = 0;
  cc17 = 0;
  cc18 = 0;
  cc19 = 0;
  cc20 = 0;
  cc21 = 0;
  cc22 = 0;
  cc23 = 0;
  cc24 = 0;
  cc25 = 0;
  cc26 = 0;
  cc27 = 0;
  cc28 = 0;
  cc29 = 0;
  cc30 = 0;
  DC1 = 0;
  DC2 = 0;
  DC3 = 0;
  DC4 = 0;
  DC5 = 0;
  DC6 = 0;
  DC7 = 0;
  DC8 = 0;
  DC9 = 0;
  DC10 = 0;
  DC11 = 0;
  DC12 = 0;
  DC13 = 0;
  DC14 = 0;
  DC15 = 0;
  DC16 = 0;
  DC17 = 0;
  DC18 = 0;
  DC19 = 0;
  DC20 = 0;
  DC21 = 0;
  DC22 = 0;
  DC23 = 0;
  DC24 = 0;
  DC25 = 0;
  DC26 = 0;
  DC27 = 0;
  if (period = 0) then
  begin
    odokresu = '01';
    dookresu = '03';
  end
  if (period = 1) then
  begin
    odokresu = '04';
    dookresu = '06';
  end
  if (period = 2) then
  begin
    odokresu = '07';
    dookresu = '09';
  end
  if (period = 3) then
  begin
    odokresu = '10';
    dookresu = '12';
  end
  nipuetmp = null;
--  k42 = cast(k42*100 as integer)/100;
  for select K.nip, sum(B.sumnetv)--, B.sumvatv, B.sumgrossv
        from bkdocs B
        join vatregs V on (V.symbol = B.vatreg and V.company = B.company)
        left join klienci K on (K.ref = B.dictpos)
          where v.vtype = 1
          and B.status > 0
          and substring(b.vatperiod from 1 for 4) = :yearid
          and substring(b.vatperiod from 5 for 2) >= :odokresu
          and substring(b.vatperiod from 5 for 2) <= :dookresu
          and B.company = :company
          group by K.nip
        into :nipue, :netto
  do begin
    if (nipuetmp is null) then
      nipuetmp = nipue;
    else if (nipuetmp = nipue) then
      ile = ile -1;

    if (ile = 1) then
    begin
      cb1 = nipue;
      cc1 = cc1 + netto;
      cc1 = cast(cc1 as integer);
    end
    else if (ile = 2) then
    begin
      cb2 = nipue;
      cc2 = cc2 + netto;
      cc2 = cast(cc2 as integer);
    end
    else if (ile = 3) then
    begin
      cb3 = nipue;
      cc3 = cc3 + netto;
      cc3 = cast(cc3 as integer);
    end
    else if (ile = 4) then
    begin
      cb4 = nipue;
      cc4 = cc4 + netto;
      cc4 = cast(cc4 as integer);
    end
    else if (ile = 5) then
    begin
      cb5 = nipue;
      cc5 = cc5 + netto;
      cc5 = cast(cc5 as integer);
    end
    else if (ile = 6) then
    begin
      cb6 = nipue;
      cc6 = cc6 + netto;
      cc6 = cast(cc6 as integer);
    end
    else if (ile = 7) then
    begin
      cb7 = nipue;
      cc7 = cc7 + netto;
      cc7 = cast(cc7 as integer);
    end
    else if (ile = 8) then
    begin
      cb8 = nipue;
      cc8 = cc8 + netto;
      cc8 = cast(cc8 as integer);
    end
    else if (ile = 9) then
    begin
      cb9 = nipue;
      cc9 = cc9 + netto;
      cc9 = cast(cc9 as integer);
    end
    else if (ile = 10) then
    begin
      cb10 = nipue;
      cc10 = cc10 + netto;
      cc10 = cast(cc10 as integer);
    end
    else if (ile = 11) then
    begin
      cb11 = nipue;
      cc11 = cc11 + netto;
      cc11 = cast(cc11 as integer);
    end
    else if (ile = 12) then
    begin
      cb12 = nipue;
      cc12 = cc12 + netto;
      cc12 = cast(cc12 as integer);
    end
    else if (ile = 13) then
    begin
      cb13 = nipue;
      cc13 = cc13 + netto;
      cc13 = cast(cc13 as integer);
    end
    else if (ile = 14) then
    begin
      cb14 = nipue;
      cc14 = cc14 + netto;
      cc14 = cast(cc14 as integer);
    end
    else if (ile = 15) then
    begin
      cb15 = nipue;
      cc15 = cc15 + netto;
      cc15 = cast(cc15 as integer);
    end
    else if (ile = 16) then
    begin
      cb16 = nipue;
      cc16 = cc16 + netto;
      cc16 = cast(cc16 as integer);
    end
    else if (ile = 17) then
    begin
      cb17 = nipue;
      cc17 = cc17 + netto;
      cc17 = cast(cc17 as integer);
    end
    else if (ile = 18) then
    begin
      cb18 = nipue;
      cc18 = cc18 + netto;
      cc18 = cast(cc18 as integer);
    end
    else if (ile = 19) then
    begin
      cb19 = nipue;
      cc19 = cc19 + netto;
      cc19 = cast(cc19 as integer);
    end
    else if (ile = 20) then
    begin
      cb20 = nipue;
      cc20 = cc20 + netto;
      cc20 = cast(cc20 as integer);
    end
    else if (ile = 21) then
    begin
      cb21 = nipue;
      cc21 = cc21 + netto;
      cc21 = cast(cc21 as integer);
    end
    else if (ile = 22) then
    begin
      cb22 = nipue;
      cc22 = cc22 + netto;
      cc22 = cast(cc22 as integer);
    end
    else if (ile = 23) then
    begin
      cb23 = nipue;
      cc23 = cc23 + netto;
      cc23 = cast(cc23 as integer);
    end
    else if (ile = 24) then
    begin
      cb24 = nipue;
      cc24 = cc24 + netto;
      cc24 = cast(cc24 as integer);
    end
    else if (ile = 25) then
    begin
      cb25 = nipue;
      cc25 = cc25 + netto;
      cc25 = cast(cc25 as integer);
    end
    else if (ile = 26) then
    begin
      cb26 = nipue;
      cc26 = cc26 + netto;
      cc26 = cast(cc26 as integer);
    end
    else if (ile = 27) then
    begin
      cb27 = nipue;
      cc27 = cc27 + netto;
      cc27 = cast(cc27 as integer);
    end
    else if (ile = 28) then
    begin
      cb28 = nipue;
      cc28 = cc28 + netto;
      cc28 = cast(cc28 as integer);
    end
    else if (ile = 29) then
    begin
      cb29 = nipue;
      cc29 = cc29 + netto;
      cc29 = cast(cc29 as integer);
    end
    else if (ile = 30) then
    begin
      cb30 = nipue;
      cc30 = cc30 + netto;
      cc30 = cast(cc30 as integer);
    end

    ile = ile + 1;
  end

  ile = 1;
  nipuetmp = null;

  for select D.nip, sum(B.sumnetv)--, B.sumvatv, B.sumgrossv
        from bkdocs B
        join vatregs V on (V.symbol = B.vatreg and V.company = B.company)
        left join dostawcy D on (D.ref = B.dictpos)
          where v.vtype = 5
          and B.status > 1
          and substring(b.vatperiod from 1 for 4) = :yearid
          and substring(b.vatperiod from 5 for 2) >= :odokresu
          and substring(b.vatperiod from 5 for 2) <= :dookresu
          and B.company = :company
          group by D.nip
        into :nipue, :netto
  do begin
    if (nipuetmp is null) then
      nipuetmp = nipue;
    else if (nipuetmp = nipue) then
      ile = ile -1;

    if (ile = 1) then
    begin
      db1 = nipue;
      dc1 = dc1 + netto;
      dc1 = cast(dc1 as integer);
    end
    else if (ile = 2) then
    begin
      db2 = nipue;
      dc2 = dc2 + netto;
      dc2 = cast(dc2 as integer);
    end
    else if (ile = 3) then
    begin
      db3 = nipue;
      dc3 = dc3 + netto;
      dc3 = cast(dc3 as integer);
    end
    else if (ile = 4) then
    begin
      db4 = nipue;
      dc4 = dc4 + netto;
      dc4 = cast(dc4 as integer);
    end
    else if (ile = 5) then
    begin
      db5 = nipue;
      dc5 = dc5 + netto;
      dc5 = cast(dc5 as integer);
    end
    else if (ile = 6) then
    begin
      db6 = nipue;
      dc6 = dc6 + netto;
      dc6 = cast(dc6 as integer);
    end
    else if (ile = 7) then
    begin
      db7 = nipue;
      dc7 = dc7 + netto;
      dc7 = cast(dc7 as integer);
    end
    else if (ile = 8) then
    begin
      db8 = nipue;
      dc8 = dc8 + netto;
      dc8 = cast(dc8 as integer);
    end
    else if (ile = 9) then
    begin
      db9 = nipue;
      dc9 = dc9 + netto;
      dc9 = cast(dc9 as integer);
    end
    else if (ile = 10) then
    begin
      db10 = nipue;
      dc10 = dc10 + netto;
      dc10 = cast(dc10 as integer);
    end
    else if (ile = 11) then
    begin
      db11 = nipue;
      dc11 = dc11 + netto;
      dc11 = cast(dc11 as integer);
    end
    else if (ile = 12) then
    begin
      db12 = nipue;
      dc12 = dc12 + netto;
      dc12 = cast(dc12 as integer);
    end
    else if (ile = 13) then
    begin
      db13 = nipue;
      dc13 = dc13 + netto;
      dc13 = cast(dc13 as integer);
    end
    else if (ile = 14) then
    begin
      db14 = nipue;
      dc14 = dc14 + netto;
      dc14 = cast(dc14 as integer);
    end
    else if (ile = 15) then
    begin
      db15 = nipue;
      dc15 = dc15 + netto;
      dc15 = cast(dc15 as integer);
    end
    else if (ile = 16) then
    begin
      db16 = nipue;
      dc16 = dc16 + netto;
      dc16 = cast(dc16 as integer);
    end
    else if (ile = 17) then
    begin
      db17 = nipue;
      dc17 = dc17 + netto;
      dc17 = cast(dc17 as integer);
    end
    else if (ile = 18) then
    begin
      db18 = nipue;
      dc18 = dc18 + netto;
      dc18 = cast(dc18 as integer);
    end
    else if (ile = 19) then
    begin
      db19 = nipue;
      dc19 = dc19 + netto;
      dc19 = cast(dc19 as integer);
    end
    else if (ile = 20) then
    begin
      db20 = nipue;
      dc20 = dc20 + netto;
      dc20 = cast(dc20 as integer);
    end
    else if (ile = 21) then
    begin
      db21 = nipue;
      dc21 = dc21 + netto;
      dc21 = cast(dc21 as integer);
    end
    else if (ile = 22) then
    begin
      db22 = nipue;
      dc22 = dc22 + netto;
      dc22 = cast(dc22 as integer);
    end
    else if (ile = 23) then
    begin
      db23 = nipue;
      dc23 = dc23 + netto;
      dc23 = cast(dc23 as integer);
    end
    else if (ile = 24) then
    begin
      db24 = nipue;
      dc24 = dc24 + netto;
      dc24 = cast(dc24 as integer);
    end
    else if (ile = 25) then
    begin
      db25 = nipue;
      dc25 = dc25 + netto;
      dc25 = cast(dc25 as integer);
    end
    else if (ile = 26) then
    begin
      db26 = nipue;
      dc26 = dc26 + netto;
      dc26 = cast(dc26 as integer);
    end
    else if (ile = 27) then
    begin
      db27 = nipue;
      dc27 = dc27 + netto;
      dc27 = cast(dc27 as integer);
    end

    ile = ile + 1;
  end

  suspend;
end^
SET TERM ; ^
