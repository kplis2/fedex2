--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ZAM_ANALIZA_OLAP(
      REJESTR varchar(10) CHARACTER SET UTF8                            = '',
      TYPZAM varchar(10) CHARACTER SET UTF8                            = '',
      PIVOT smallint = 0)
  returns (
      REF varchar(255) CHARACTER SET UTF8                           ,
      PARENT varchar(255) CHARACTER SET UTF8                           ,
      SYMBOL varchar(255) CHARACTER SET UTF8                           ,
      REFZAM integer,
      ZAMOWIENIE varchar(255) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      TYP varchar(10) CHARACTER SET UTF8                           ,
      DOKUMENT varchar(255) CHARACTER SET UTF8                           ,
      DATA date,
      OKRES varchar(6) CHARACTER SET UTF8                           ,
      PRZYCHPLAN numeric(14,2),
      PRZYCHREAL numeric(14,2),
      KOSZTPLAN numeric(14,2),
      KOSZTREAL numeric(14,2),
      ILOSCPLAN numeric(14,4),
      ILOSCREAL numeric(14,4),
      COLORINDEX smallint,
      NUMBER integer)
   as
DECLARE VARIABLE CURRZAM INTEGER;
DECLARE VARIABLE CURRPOZ INTEGER;
DECLARE VARIABLE POZPRZYCHREAL NUMERIC(14,2);
DECLARE VARIABLE POZKOSZTREAL NUMERIC(14,2);
DECLARE VARIABLE POZPRZYCHPLAN NUMERIC(14,2);
DECLARE VARIABLE POZKOSZTPLAN NUMERIC(14,2);
DECLARE VARIABLE ZAMPRZYCHREAL NUMERIC(14,2);
DECLARE VARIABLE ZAMKOSZTREAL NUMERIC(14,2);
DECLARE VARIABLE ZAMPRZYCHPLAN NUMERIC(14,2);
DECLARE VARIABLE ZAMKOSZTPLAN NUMERIC(14,2);
DECLARE VARIABLE PLENDDATE DATE;
DECLARE VARIABLE TERMDOST DATE;
DECLARE VARIABLE DATAWE DATE;
DECLARE VARIABLE CENANETZL NUMERIC(14,4);
DECLARE VARIABLE SHEET INTEGER;
DECLARE VARIABLE CALC INTEGER;
DECLARE VARIABLE WERSJA INTEGER;
DECLARE VARIABLE ILPLAN NUMERIC(14,4);
DECLARE VARIABLE ILREAL NUMERIC(14,4);
DECLARE VARIABLE CALCQUANTITY NUMERIC(14,4);
BEGIN
  COLORINDEX = NULL;
  NUMBER = 1;
  -- petla po zamowieniach
  FOR SELECT N.REF, N.ID, N.TERMDOST, N.DATAWE, N.PRSHEET, N.PRSHCALC
    FROM NAGZAM N
    WHERE (N.REJESTR=:REJESTR OR :REJESTR='')
    AND (N.TYPZAM=:TYPZAM OR :TYPZAM='')
    AND N.TYP<>2
    ORDER BY N.DATAWE
    INTO :CURRZAM, :ZAMOWIENIE, :TERMDOST, :DATAWE, :SHEET, :CALC
  DO BEGIN
    REFZAM = :CURRZAM;
    ZAMPRZYCHPLAN = 0;
    ZAMPRZYCHREAL = 0;
    ZAMKOSZTPLAN = 0;
    ZAMKOSZTREAL = 0;

    REF = 'NAGZAM'||CAST(:CURRZAM AS VARCHAR(10));
    PARENT = '';
    -- petla po pozycjach
    FOR SELECT P.REF, P.KTM, P.WERSJA, P.CENANETZL, P.TERMDOST
      FROM POZZAM P
      WHERE P.ZAMOWIENIE=:CURRZAM
      ORDER BY P.NUMER
      INTO :CURRPOZ, :KTM, :WERSJA, :CENANETZL, :PLENDDATE
    DO BEGIN
      POZKOSZTPLAN = 0;
      POZKOSZTREAL = 0;
      PRZYCHPLAN = 0;
      PRZYCHREAL = 0;
      KOSZTPLAN = 0;
      KOSZTREAL = 0;
      ILOSCPLAN = 0;
      ILOSCREAL = 0;
      CALCQUANTITY = NULL;
      COLORINDEX = NULL;

      -- rozliczenie kosztow rzeczywistych dla pozycji
        FOR SELECT 'PRORDANAL'||CAST(REF AS VARCHAR(10)),'PRORDANAL'||CAST(PARENT AS VARCHAR(10)),
          SYMBOL, TYP, DOKUMENT, KWOTA, KWOTAPLAN, COLORINDEX, DATA, ILOSC, ILOSCPLAN FROM PM_ROZLICZENIE_POZZAM(:CURRPOZ)
        ORDER BY PARENT,REF
        INTO :REF, :PARENT,
          :SYMBOL, :TYP, :DOKUMENT, :KOSZTREAL, :KOSZTPLAN, :COLORINDEX, :DATA, :ILREAL, :ILPLAN
        DO BEGIN
          IF(:PARENT IS NULL) THEN BEGIN
            PARENT = 'POZZAM'||CAST(:CURRPOZ AS VARCHAR(10));
            POZKOSZTREAL = :POZKOSZTREAL + :KOSZTREAL;
            POZKOSZTPLAN = :POZKOSZTPLAN + :KOSZTPLAN;
            ILOSCPLAN = :ILPLAN;
            ILOSCREAL = :ILREAL;
            POZPRZYCHPLAN = :ILOSCPLAN * :CENANETZL;
            POZPRZYCHREAL = :ILOSCREAL * :CENANETZL;
          END
          IF(COALESCE(:DATA,'')='') THEN DATA = :DATAWE;
          IF(COALESCE(:DATA,'')<>'') THEN SELECT OKRES FROM DATATOOKRES(CAST(:DATA AS VARCHAR(10)), 0,0,0,1) INTO :OKRES;
          ELSE OKRES = '';
          IF(:PIVOT=0 OR :COLORINDEX IN (3,5)) THEN BEGIN
            NUMBER = :NUMBER + 1;
            SUSPEND;
          END
        END
      COLORINDEX = NULL;
      KOSZTPLAN = 0;
      KOSZTREAL = 0;

      -- rozliczenie kosztow planowanych dla pozycji
/*      if(:calc is null) then begin
        if(:sheet is null) then begin
          select REF from PRSHEETS where KTM=:ktm and WERSJA=:wersja and STATUS=2
          into :sheet;
        end
        if(:sheet is not null) then begin
          select QUANTITY from PRSHEETS where REF=:sheet
          into :calcquantity;
          select first 1 ref from PRSHCALCS where SHEET=:sheet and STATUS = 2 into :calc;
        end
      end
      if(:calc is not null) then begin
        for select 'PRCALCCOL'||cast(ref as varchar(10))||'N'||cast(:currpoz as varchar(10)),
                   'PRCALCCOL'||cast(parent as varchar(10))||'N'||cast(:currpoz as varchar(10)),
          descript,cvalue,color
        from PRCALCCOLS where calc=:calc and calctype=0 and (fromprshoper is not null or fromprshmat is not null)
        order by prcolumn
        into :ref,:parent,:symbol,:kosztplan,:colorindex
        do begin
          if(:calcquantity=0 or :calcquantity is null) then calcquantity = 1;
          kosztplan = :kosztplan * :iloscreal / :calcquantity;
            parent = 'POZZAM'||cast(:currpoz as varchar(10));
            pozkosztplan = :pozkosztplan + :kosztplan;
          typ = '';
          dokument = 'KALKULACJA';
          data = :plenddate;
          if(coalesce(:data,'')<>'') then select OKRES from datatookres(cast(:data as varchar(10)), 0,0,0,1) into :okres;
          else okres = '';
          number = :number + 1;
          suspend;

        end
      end
      colorindex = NULL;*/

      -- zwroc rekord dla calej pozycji
      PRZYCHPLAN = :POZPRZYCHPLAN;
      PRZYCHREAL = :POZPRZYCHREAL;
      KOSZTPLAN = :POZKOSZTPLAN;
      KOSZTREAL = :POZKOSZTREAL;
      TYP = '';
      DOKUMENT = :KTM;
      COLORINDEX = 8;
      REF = 'POZZAM'||CAST(:CURRPOZ AS VARCHAR(10));
      PARENT = 'NAGZAM'||CAST(:CURRZAM AS VARCHAR(10));
      SYMBOL = :KTM;
      DATA = :PLENDDATE;
      IF(COALESCE(:DATA,'')<>'') THEN SELECT OKRES FROM DATATOOKRES(CAST(:DATA AS VARCHAR(10)), 0,0,0,1) INTO :OKRES;
      ELSE OKRES = '';
      NUMBER = :NUMBER + 1;
      IF(:PIVOT=0) THEN BEGIN
        NUMBER = :NUMBER + 1;
        SUSPEND;
      END
      ZAMPRZYCHPLAN = :ZAMPRZYCHPLAN + :PRZYCHPLAN;
      ZAMPRZYCHREAL = :ZAMPRZYCHREAL + :PRZYCHREAL;
      ZAMKOSZTPLAN = :ZAMKOSZTPLAN + :KOSZTPLAN;
      ZAMKOSZTREAL = :ZAMKOSZTREAL + :KOSZTREAL;
    END


    --zwroc rekord dla calego zamowienia
    PRZYCHPLAN = :ZAMPRZYCHPLAN;
    PRZYCHREAL = :ZAMPRZYCHREAL;
    KOSZTPLAN = :ZAMKOSZTPLAN;
    KOSZTREAL = :ZAMKOSZTREAL;
    KTM = '';
    TYP = '';
    DOKUMENT = '';
    COLORINDEX = NULL;
    IF(:TERMDOST IS NOT NULL) THEN DATA = :TERMDOST; ELSE DATA = :DATAWE;
    IF(COALESCE(:DATA,'')<>'') THEN SELECT OKRES FROM DATATOOKRES(CAST(:DATA AS VARCHAR(10)), 0,0,0,1) INTO :OKRES;
    ELSE OKRES = '';
    REF = 'NAGZAM'||CAST(:CURRZAM AS VARCHAR(10));
    PARENT = NULL;
    SYMBOL = :ZAMOWIENIE;
    NUMBER = :NUMBER + 1;
    IF(:PIVOT=0) THEN BEGIN
      NUMBER = :NUMBER + 1;
      SUSPEND;
    END
  END
END^
SET TERM ; ^
