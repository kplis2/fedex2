--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_CENAZAKUPU(
      DOKREF integer,
      DOKTYP char(1) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      ILOSC numeric(14,4),
      DOSTAWA integer,
      PARAMD1 timestamp,
      PARAMD2 timestamp,
      PARAMD3 timestamp,
      PARAMD4 timestamp,
      PARAMN1 numeric(14,2),
      PARAMN2 numeric(14,2),
      PARAMN3 numeric(14,2),
      PARAMN4 numeric(14,2),
      PARAMS1 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS2 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS3 varchar(255) CHARACTER SET UTF8                           ,
      PARAMS4 varchar(255) CHARACTER SET UTF8                           ,
      PARAMN5 numeric(14,2),
      PARAMN6 numeric(14,2),
      PARAMN7 numeric(14,2),
      PARAMN8 numeric(14,2),
      PARAMN9 numeric(14,2),
      PARAMN10 numeric(14,2),
      PARAMN11 numeric(14,2),
      PARAMN12 numeric(14,2))
  returns (
      STATUS integer,
      CENA numeric(14,2),
      RABAT numeric(14,2))
   as
DECLARE VARIABLE KTM VARCHAR(80);
DECLARE VARIABLE SUMAPROC NUMERIC(14,2);
DECLARE VARIABLE PROCENT NUMERIC(14,2);
DECLARE VARIABLE I INTEGER;
DECLARE VARIABLE SYMBPARAM VARCHAR(20);
DECLARE VARIABLE PROCEDURA VARCHAR(255);
DECLARE VARIABLE VAL NUMERIC(14,2);
DECLARE VARIABLE WARTDOM NUMERIC(14,2);
begin
  STATUS = 0;
  CENA = 0;
  RABAT = 0;
  sumaproc = 0;
  i = 1;
  select wersje.ktm, wersje.cena_zakn from WERSJE where REF=:wersjaref into :ktm,:cena;
  while (i < 5) do
   begin
     procent = 0;
     wartdom = 0;
     val = case when i = 1 then :paramn1
             else case when i = 2 then :paramn2
               else case when i = 3 then :paramn3
                 else case when i = 4 then :paramn4
                   else case when i = 5 then :paramn5
                     else case when i = 6 then :paramn6
                       else case when i = 7 then :paramn7
               else case when i = 8 then :paramn8
                 else case when i = 9 then :paramn9
                   else case when i = 10 then :paramn10
                     else case when i = 11 then :paramn11
                       else case when i = 12 then :paramn12
                         else Null end end end end end end end end end end end end;
     symbparam = 'PARAMN'||cast (i as varchar(2));
     symbparam =''''||symbparam||'''';
     procedura = 'select procent, wartdom from defcenzak
       where defcenzak.ktm = '''||:ktm||'''and defcenzak.parsymbol = '||:symbparam||
  ' and defcenzak.odwart <= '||cast(:val as varchar (80))||
  ' and defcenzak.dowart > '||cast(:val as varchar (80));
     execute statement  procedura into :procent, :wartdom;
     if (procent is null) then procent = :val;
     sumaproc =  sumaproc + (:val  - :wartdom)*procent;
     i = i + 1;
   end
  --exception universal :sumaproc;
  CENA = CENA  *  (100 - sumaproc) / 100;
  STATUS = 1;
end^
SET TERM ; ^
