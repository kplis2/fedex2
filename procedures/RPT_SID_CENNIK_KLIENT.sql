--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_SID_CENNIK_KLIENT(
      KLIENT integer,
      ODKTM varchar(40) CHARACTER SET UTF8                           ,
      DOKTM varchar(40) CHARACTER SET UTF8                           ,
      CECHA varchar(20) CHARACTER SET UTF8                           ,
      WARTCECHY varchar(20) CHARACTER SET UTF8                           ,
      WITHRABAT smallint)
  returns (
      REF integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      NAZWAT varchar(255) CHARACTER SET UTF8                           ,
      NAZWAW varchar(255) CHARACTER SET UTF8                           ,
      JEDN varchar(5) CHARACTER SET UTF8                           ,
      CENANET numeric(14,4),
      STVAT varchar(5) CHARACTER SET UTF8                           ,
      CENABRU numeric(14,4),
      ATRKOD varchar(70) CHARACTER SET UTF8                           ,
      ATRNAZWA varchar(255) CHARACTER SET UTF8                           ,
      DEFCEN integer,
      DEFCENNAZWA varchar(40) CHARACTER SET UTF8                           ,
      CENACENNET numeric(14,4),
      RABAT numeric(14,4),
      USLUGA smallint,
      PLIK BITMAPA,
      PLIKREF integer,
      OPISPOD MEMO2000)
   as
declare variable CECHTYP smallint;
declare variable CNT integer;
declare variable OLDATRKOD varchar(70);
declare variable KLICENNIK integer;
declare variable GRUPAKLI integer;
declare variable "FOUND" smallint;
declare variable AKTUODDZIAL varchar(10);
declare variable TCENANET numeric(14,4);
declare variable TCENABRU numeric(14,4);
declare variable TDEFCEN integer;
declare variable TTJEDN integer;
declare variable TJEDN integer;
declare variable KLIRABAT numeric(14,4);
declare variable ISCENA smallint;
declare variable WALUTA varchar(5);
declare variable WERSJA integer;
declare variable STAWKA numeric(14,2);
declare variable PROMOCJA integer;
declare variable STATUS smallint;
declare variable PREC smallint;
begin
  atrkod = '';
  atrnazwa = '';
  oldatrkod = '';
  ref = 1;
  rabat = 0;
  cnt = 0;
  execute procedure GETCONFIG('AKTUODDZIAL') returning_values :aktuoddzial;
  if(:klient is not null) then begin
    select grupykli.cennik, grupykli.ref, klienci.upust
    from GRUPYKLI join KLIENCI on (KLIENCI.grupa = GRUPYKLI.ref)
    where KLIENCI.REF = :klient into :klicennik, :grupakli, :klirabat;
    if(:klirabat is null) then klirabat = 0;
    if(:grupakli is null and klicennik is null) then exit;/*brak okrelonej grupy i cennika*/
  end else begin
    execute procedure GETCONFIG('CENNIK') returning_values :klicennik;
    grupakli = null;
    klirabat = 0;
  end
  if(:cecha <> '')then
    select count(*),max(typ) from DEFCECHY where SYMBOL = :cecha into :cnt, :cechtyp;
  if(:cechtyp = 2 and :wartcechy <> '') then wartcechy = :wartcechy||'%';
  if(:cnt > 0) then begin
    for select WERSJE.KTM, WERSJE.nrwersji, WERSJE.REF, WERSJE.NAZWAT, WERSJE.nazwa,
      towary.vat, atrybuty.wartosc, WERSJE.usluga, towary.opispod
    from wersje 
      join TOWARY on (towary.ktm = WERSJE.ktm)
      left join atrybuty on (atrybuty.cecha = :cecha and atrybuty.wersjaref = WERSJE.ref)
      left join towpliki on (towpliki.ktm = WERSJE.ktm)
    where WERSJE.akt = 1
      and ((:odktm is null) or (:odktm = '') or (:odktm <= WERSJE.KTM))
      and ((:doktm is null) or (:doktm = '') or (:doktm >= WERSJE.KTM))
      and ((:wartcechy is null) or (:wartcechy = '') or (atrybuty.wartosc like :wartcechy))
    order by atrybuty.wartosc, WERSJE.ktm, WERSJE.nrwersji
    into :ktm, :wersja, :wersjaref, :nazwat, :nazwaw,
      :stvat, :atrkod, :usluga, :opispod
    do begin
      if(:nazwaw='Podstawowa') then nazwaw = '';
      if(:atrkod is null) then atrkod = '';
      if(:atrkod <> :oldatrkod) then begin
        atrnazwa = '';
        if(:cechtyp < 2) then
          select defcechw.opis from DEFCECHW where DEFCECHW.cecha = :cecha and defcechw.wartnum = :atrkod into :atrnazwa;
        else if(:cechtyp = 2) then
          select defcechw.opis from DEFCECHW where DEFCECHW.cecha = :cecha and defcechw.wartstr = :atrkod into :atrnazwa;
        else
          select defcechw.opis from DEFCECHW where DEFCECHW.cecha = :cecha and defcechw.wartdate = :atrkod into :atrnazwa;
        if(:atrnazwa is null) then atrnazwa = '';
        oldatrkod = :atrkod;
      end
      defcen = 0;
      execute procedure CENNIK_ZNAJDZ(:klicennik,:wersjaref,null,:klient,:aktuoddzial,'') returning_values :defcen;
      if(:defcen > 0) then begin
        execute procedure pobierz_cene(:defcen,:wersjaref,null, 'N',null,null,2,null,null,:klient,null,null)
          returning_values :cenanet, :waluta, :tjedn, :status, :prec;

      /*for select CENNIK.cenanet, cennik.cenabru, cennik.cennik, cennik.jedn
      from CENNIK join DEFCENKLI on (CENNIK.CENNIK = DEFCENKLI.cennik)
      where CENNIK.wersjaref = :wersjaref
       and DEFCENKLI.grupakli = :grupakli and DEFCENKLI.oddzial = :aktuoddzial
      order by DEFCENKLI.NUMER
      into :tcenanet, :tcenabru, :tdefcen, :ttjedn
      do begin
        if(:found = 0) then begin
          cenanet = :tcenanet;
          cenabru = :tcenabru;
          tjedn = :ttjedn;
          defcen = :tdefcen;
          found = 1;
        end
      end
      if(:found = 0 and :klicennik > 0) then
        for select CENNIK.CENANET, cennik.cenabru, cennik.cennik, cennik.jedn
        from cennik
        where cennik.wersjaref = :wersjaref and cennik.cennik = :klicennik
        into :cenanet, :cenabru, :defcen, :tjedn
        do found = 1;
      if(:found = 1) then begin*/
        jedn = null;
        select TOWJEDN.jedn from TOWJEDN where REF = :tjedn into :jedn;
        select DEFCENNIK.nazwa from DEFCENNIK where REF = :defcen into :defcennazwa;
        cenacennet = :cenanet;
        if(:withrabat > 0) then begin
          execute procedure OBLICZ_RABAT(:ktm, :wersja, :klient,0,:cenanet,'N', '', :defcen, 0, 0) returning_values :rabat, :iscena, :waluta, :promocja;
          if(:iscena > 0) then begin
             cenanet = :rabat;
             rabat = 0;
          end else begin
            rabat = :rabat + :klirabat;
            if(:rabat <> 0) then begin
              cenanet = :cenanet * ( 100-:rabat)/100;
            end
          end
        end
        select VAT.stawka from VAT where vat.grupa = :stvat into :stawka;
        cenabru = :cenanet*(100+:stawka)/100;
        ref = :ref + 1;

        plikref = null;
        plik = null;
        select first 1 tp.ref, tp.plik from towpliki tp where tp.ktm = :ktm
        into :plikref, :plik;
        suspend;
      end
    end
  end else begin
    for select WERSJE.KTM, WERSJE.nrwersji, WERSJE.REF, WERSJE.NAZWAT, WERSJE.nazwa,
      towary.vat, WERSJE.usluga, towary.opispod
    from wersje 
      join TOWARY on (towary.ktm = WERSJE.ktm)
      left join TOWPLIKI on (WERSJE.ktm = TOWPLIKI.ktm)
    where WERSJE.akt = 1
      and ((:odktm is null) or (:odktm = '') or (:odktm <= WERSJE.KTM))
      and ((:doktm is null) or (:doktm = '') or (:doktm >= WERSJE.KTM))
    order by WERSJE.ktm, WERSJE.nrwersji
    into :ktm, :wersja, :wersjaref, :nazwat, :nazwaw,
      :stvat, :usluga, :opispod
    do begin
      if(:nazwaw='Podstawowa') then nazwaw = '';
      if(:atrkod is null) then atrkod = '';
      execute procedure CENNIK_ZNAJDZ(:klicennik,:wersjaref,null,:klient,:aktuoddzial,'') returning_values :defcen;
      if(:defcen > 0) then begin
        execute procedure pobierz_cene(:defcen,:wersjaref,null, 'N',null,null,2,null,null,:klient,null,null)
          returning_values :cenanet, :waluta, :tjedn, :status, :prec;
/*      for select CENNIK.cenanet, cennik.cenabru, cennik.cennik, cennik.jedn
      from CENNIK join DEFCENKLI on (CENNIK.CENNIK = DEFCENKLI.cennik)
      where CENNIK.wersjaref = :wersjaref
       and DEFCENKLI.grupakli = :grupakli and DEFCENKLI.oddzial = :aktuoddzial
      order by DEFCENKLI.NUMER
      into :tcenanet, :tcenabru, :tdefcen, :ttjedn
      do begin
        if(:found = 0) then begin
          cenanet = :tcenanet;
          cenabru = :tcenabru;
          tjedn = :ttjedn;
          defcen = :tdefcen;
          found = 1;
        end
      end
      if(:found = 0 and :klicennik > 0) then
        for select CENNIK.CENANET, cennik.cenabru, cennik.cennik, cennik.jedn
        from cennik
        where cennik.wersjaref = :wersjaref and cennik.cennik = :klicennik
        into :cenanet, :cenabru, :defcen, :tjedn
        do found = 1;
      if(:found = 1) then begin*/
        jedn = null;
        select TOWJEDN.jedn from TOWJEDN where REF = :tjedn into :jedn;
        select DEFCENNIK.nazwa from DEFCENNIK where REF = :defcen into :defcennazwa;
        cenacennet = :cenanet;
        if(:withrabat > 0) then begin
          execute procedure OBLICZ_RABAT(:ktm, :wersja, :klient,0,:cenanet,'N', '', :defcen, 0, 0) returning_values :rabat, :iscena, :waluta, :promocja;
          if(:iscena > 0) then begin
             cenanet = :rabat;
             rabat = 0;
          end else begin
            rabat = :rabat + :klirabat;
            if(:rabat > 0) then begin
              cenanet = :cenanet * ( 100-:rabat)/100;
            end
          end
        end
        --przeliczenie VATu
        select VAT.stawka from VAT where vat.grupa = :stvat into :stawka;
        cenabru = :cenanet*(100+:stawka)/100;
        ref = :ref + 1;

        plikref = null;
        plik = null;
        select first 1 tp.ref, tp.plik from towpliki tp where tp.ktm = :ktm
        into :plikref, :plik;
        suspend;
      end
    end
  end
end^
SET TERM ; ^
