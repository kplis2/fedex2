--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SLO_DANE_KODKS(
      COMPANY integer,
      KODKS varchar(20) CHARACTER SET UTF8                           ,
      PERIOD varchar(6) CHARACTER SET UTF8                           )
  returns (
      NAME varchar(80) CHARACTER SET UTF8                           )
   as
declare variable dictdef integer;
  declare variable bkaccount integer;
  declare variable len integer;
  declare variable nlevel integer;
  declare variable dl integer;
begin
  -- Procedura na podstawie konta ksigowego
  -- i okresu zwraca opis ostatniego poziomu analityki
  select ref, descript
    from bkaccounts
    where company = :company and symbol = substring(:kodks from 1 for 3)
      and yearid = substring(:period from 1 for 4)
  into :bkaccount, :name;

  select max(nlevel)
    from accstructure where bkaccount = :bkaccount
  into :nlevel;

  if (nlevel is not null) then
  begin
    select len, dictdef
      from accstructure
        where bkaccount = :bkaccount and nlevel = :nlevel
      into :len, :dictdef;
    dl = coalesce(char_length(kodks),0); -- [DG] XXX ZG119346
    kodks = substring(kodks from  dl - len + 1 for  dl);

    execute procedure name_from_bksymbol(company, dictdef, kodks)
      returning_values name, len;
  end
  suspend;
end^
SET TERM ; ^
