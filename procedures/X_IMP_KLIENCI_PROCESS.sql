--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_IMP_KLIENCI_PROCESS(
      REF_IMP INTEGER_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING511)
   as
declare variable KLIENTID integer;
declare variable NAZWA varchar(60);
declare variable NAZWAPELNA varchar(255);
declare variable FIRMA smallint;
declare variable NIP varchar(13);
declare variable IMIE varchar(255);
declare variable NAZWISKO varchar(255);
declare variable KRAJID varchar(20);
declare variable ULICA varchar(255);
declare variable NRDOMU varchar(10);
declare variable NRLOKALU varchar(10);
declare variable KODPOCZTOWY varchar(10);
declare variable MIASTO varchar(255);
declare variable TELEFON varchar(255);
declare variable EMAIL varchar(255);
declare variable UWAGI varchar(1024);
declare variable AKTYWNY smallint;
declare variable UWAGIDOKUMENT varchar(1024);
declare variable KLIENTREF integer;
declare variable INT_ID STRING40;
begin
    --procedura do przetwarzania klientow z tabeli przejsciowej X_IMP_KLIENCI
    status = 1;
    msg = '';
    select KLIENTID, NAZWA, NAZWAPELNA, FIRMA, NIP, IMIE, NAZWISKO, KRAJID, ULICA, NRDOMU, NRLOKALU, KODPOCZTOWY,
           MIASTO, TELEFON, EMAIL, UWAGI, AKTYWNY, UWAGIDOKUMENT
        from X_IMP_KLIENCI
        where ref = :ref_imp
        into  :KLIENTID, :NAZWA, :NAZWAPELNA, :FIRMA, :NIP, :IMIE, :NAZWISKO, :KRAJID, :ULICA, :NRDOMU, :NRLOKALU,
                :KODPOCZTOWY, :MIASTO, :TELEFON, :EMAIL, :UWAGI, :AKTYWNY, :UWAGIDOKUMENT;

    --sprawdzenie czy istnieja niezbedne dane
    if (coalesce(klientid,0) = 0) then
    begin
        status = 8;
        msg = 'brak id klienta';
    end
    if (coalesce(nazwa,'') = '') then
    begin
        status = 8;
        msg = 'brak nazwy';
    end
    --czy krajid poprawny
    krajid = upper(krajid);
    if (coalesce(krajid,'') = '') then
    begin
        status = 8;
        msg = 'brak id kraju';
    end else
    if (not exists (select first 1 * from countries c where c.symbol = :krajid ))
    then
    begin
        status = 8;
        msg = 'brak Kraju o kodzie alfa-2: '||coalesce(:krajid,'<brak>');
    end


    --dane domyslne brak nip to firma 0
    if (coalesce(nip,'') = '') then
    begin
        firma = 0;
    end
    aktywny = coalesce(aktywny, 1);

    --sprawdzenie czy taki klient juz istnieje insert lub update
    int_id = cast(klientid as string40);
    select ref from klienci k where k.int_id = :int_id
        into :klientref;
    -- jezeli nazwapelna jest pusta w WAPRO to musze czyms wypelnic pole nazwa
    if (coalesce(:nazwapelna,'')='') then
        nazwapelna = nazwa;
    nazwa = substring(nazwa from 1 for 40);
    if(status <> 8) then begin
      if (coalesce(klientref,0) = 0) then
      begin
          insert into KLIENCI (FSKROT, FIRMA, NAZWA, IMIE, NAZWISKO, NIP,  ULICA, MIASTO,
                           KODP,   KRAJID, TELEFON,  EMAIL, UWAGI, AKTYWNY, company, GRUPA,
                           INT_ID, INT_DATAOSTPRZETW, NRDOMU, NRLOKALU, X_UWAGI_DOKUMENT,
                           x_imp_ref
                           )
          values  (:nazwa, :firma, :nazwapelna, :imie, :nazwisko, :nip, :ulica, :miasto,
                  :kodpocztowy, :krajid, :telefon, :email, :uwagi, :aktywny, 1, 1,
                  :int_id, current_timestamp, :nrdomu, :nrlokalu, :uwagidokument,
                  :ref_imp);
      end
      else begin
          update KLIENCI k
              set FSKROT = :nazwa, FIRMA = :firma, NAZWA = :nazwapelna, IMIE = :imie,
                  NAZWISKO = :nazwisko, NIP = :nip,  ULICA = :ulica, MIASTO = :miasto,
                  KODP = :kodpocztowy, KRAJID = :krajid, TELEFON = :telefon,
                  EMAIL = :email, UWAGI = :uwagi, AKTYWNY = :aktywny,
                  INT_ID = :int_id, INT_DATAOSTPRZETW = current_timestamp,
                  NRDOMU = :nrdomu, NRLOKALU = :nrlokalu, X_UWAGI_DOKUMENT = :uwagidokument,
                  x_imp_ref = :ref_imp
              where k.ref = :klientref;
      end
      msg = 'ok';
    end
    suspend;
end^
SET TERM ; ^
