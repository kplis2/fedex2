--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE XX_E_ALGORITHM(
      KEY1 integer,
      KEY2 integer)
  returns (
      RET numeric(16,2))
   as
declare variable v1 numeric(16,6); 
declare variable v3 numeric(16,6); 
declare variable v4 numeric(16,6); 
declare variable v5 numeric(16,6); 
declare variable v2 numeric(16,6); 
declare variable v6 numeric(16,6); 
declare variable v7 numeric(16,6); 

begin
execute procedure EF_CIFLAG(key1, key2,'EF_', 'DFC')
  returning_values :v1; 
execute procedure EF_PVAL(key1, key2,'EF_', 'FC')
  returning_values :v3; 
execute procedure EF_COL(key1, key2,'EF_', 605)
  returning_values :v4; 
execute procedure EF_ICOL(key1, key2,'EF_', 605)
  returning_values :v5; 
execute procedure EF_ROUND(key1, key2,'EF_', (v3 * (v4 + v5) / 100),2)
  returning_values :v2; 
execute procedure EF_ICOL(key1, key2,'EF_', 612)
  returning_values :v6; 
execute procedure EF_ICOL(key1, key2,'EF_', 613)
  returning_values :v7; 
if (v1 = 1) then ret = v2 - v6 - v7;

  suspend;
end^
SET TERM ; ^
