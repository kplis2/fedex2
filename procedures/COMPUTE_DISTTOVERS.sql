--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COMPUTE_DISTTOVERS(
      DREF integer)
   as
declare variable debit numeric(15,2);
declare variable credit numeric(15,2);
declare variable period varchar(6);
declare variable yearid integer;
declare variable company integer;
declare variable dictdef integer;
declare variable symbol symboldist_id;
declare variable i integer;
declare variable sql varchar(255);
begin
  select dc.company, dc.period from bkdocs dc where dc.ref = :dref into :company, :period;
  select yearid from bkperiods where company = :company and id = :period into :yearid;
  ------wyroznik pozycja 1------------------------------------------------------
  select ref from slodef where isdist = 1
    and (COMPANY IS NULL or (COMPANY = :company))
    and foryear=:yearid
    into :dictdef;
  if (:dictdef is null) then
    select ref from slodef where isdist = 1
      and (COMPANY IS NULL or (COMPANY = :company))
      and foryear is null
    into :dictdef;
  symbol = '';
  debit = 0;
  credit = 0;
  for select dp.symbol, sum(dp.debit), sum(dp.credit) from distpos dp where dp.bkdoc = :dref
    and dp.symbol is not null and dp.symbol <> '' group by dp.bkdoc, dp.symbol into :symbol, :debit, :credit
  do begin
    if(:debit <> 0 or :credit <> 0) then begin
      if (not exists(select yearid from distobjects
            where yearid = :yearid and dictdef = :dictdef and symbol = :symbol and company = :company)) then
        insert into distobjects (yearid, dictdef, symbol, company)
          values (:yearid, :dictdef, :symbol, :company);

      if (exists(select company from disttovers
            where yearid = :yearid and dictdef = :dictdef
              and symbol = :symbol and period = :period and company = :company)) then
      begin
          update disttovers set debit = debit + :debit, credit = credit + :credit
            where yearid = :yearid and dictdef = :dictdef
              and symbol = :symbol and period = :period and company = :company;
      end else begin
               insert into disttovers (company, yearid, dictdef, symbol, period, debit, credit)
                values (:company, :yearid, :dictdef, :symbol, :period, :debit, :credit);
               end
    end
  end
  i = 2;
  while (i <= 40) do -- liczba wyroznikow
  begin
    ------wyroznik pozycja i------------------------------------------------------
    select ref from slodef where isdist = :i
      and (COMPANY IS NULL or (COMPANY = :company))
      and foryear=:yearid
      into :dictdef;

    if (:dictdef is null) then
    select ref from slodef where isdist = :i
      and (COMPANY IS NULL or (COMPANY = :company))
      and foryear is null
    into :dictdef;

    symbol = '';
    debit = 0;
    credit = 0;
    sql = 'select dp.symbol'||i||', sum(dp.debit), sum(dp.credit) from distpos dp where dp.bkdoc = '||coalesce(dref,0)||'
      and dp.symbol'||i||' is not null and dp.symbol'||i||' <> '''' group by dp.bkdoc, dp.symbol'||i;
    for
      execute statement sql
      into :symbol, :debit, :credit
    do begin
      if(:debit <> 0 or :credit <> 0) then begin
        if (not exists(select yearid from distobjects
            where yearid = :yearid and dictdef = :dictdef and symbol = :symbol and company = :company)) then
          insert into distobjects (yearid, dictdef, symbol, company)
            values (:yearid, :dictdef, :symbol, :company);

        if (exists(select company from disttovers
              where yearid = :yearid and dictdef = :dictdef
                and symbol = :symbol and period = :period and company = :company)) then
        begin
            update disttovers set debit = debit + :debit, credit = credit + :credit
              where yearid = :yearid and dictdef = :dictdef
                and symbol = :symbol and period = :period and company = :company;
        end else begin
                 insert into disttovers (company, yearid, dictdef, symbol, period, debit, credit)
                  values (:company, :yearid, :dictdef, :symbol, :period, :debit, :credit);
                 end
      end
    end
    i = i + 1;
  end
end^
SET TERM ; ^
