--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDECL_CHECKUPO(
      EDECLREF integer)
  returns (
      LOGMSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable STATE integer;
declare variable STATUS integer;
declare variable DECLGROUP integer;
declare variable SCODE varchar(20);
declare variable PERS varchar(200);
declare variable ISGROUP smallint;
declare variable GROUPNAME varchar(200);
declare variable PERSMSG varchar(255);
declare variable EYEAR integer;
begin

  select d.state, d.status, d.declgroup, df.systemcode, p.person, df.isgroup, d.groupname, d.eyear
    from edeclarations d
    left join edecldefs df on df.ref = d.edecldef
    left join persons p on d.person = p.ref
    where d.ref = :edeclref
  into state,status,declgroup,scode,pers,isgroup,groupname,eyear;
  if(coalesce(pers,'')<>'') then
    persmsg = :pers;
  else if(coalesce(groupname,'')<>'') then
    persmsg = :groupname;
  else
    persmsg = eyear;

  if(state <> 4) then
    logmsg = ascii_char(215)||' '||:scode||' : '||:persmsg||' - deklaracja nie została jeszcze wysłana';
  else if(status = 200) then
    logmsg = ascii_char(215)||' '||:scode||' : '||:persmsg||' - deklaracja została już zatwierdzona przez Ministerstwo Finansów';
  else if(coalesce(:declgroup,'')<>'') then
    logmsg = ascii_char(215)||' '||:scode||' : '||:persmsg||' - nie można pobrać UPO dla deklaracji zgrupowanej';
  suspend;
end^
SET TERM ; ^
