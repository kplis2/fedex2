--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TOWAR_DOSTAWCY(
      DOSTAWCA integer,
      WERSJAREF integer)
  returns (
      SYMBOL varchar(40) CHARACTER SET UTF8                           )
   as
begin
  symbol = null;
  select first(1) d.symbol_dost
    from dostcen d
    where d.dostawca = :dostawca
      and d.wersjaref = :wersjaref
    into :symbol;
  if (coalesce(symbol,'') = '') then
    select first (1) c.symbol
      from wersje w
        join cennikizewn c on (c.dostawca = :dostawca and c.ktm = w.ktm)
      where w.ref = :wersjaref
      into :symbol;
  if (coalesce(symbol,'') != '') then suspend;
end^
SET TERM ; ^
