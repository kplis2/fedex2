--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_PRINT_REPACK(
      WTREF INTEGER_ID)
  returns (
      ZPL STRING1024)
   as
  declare variable rwref integer_id;
  declare variable ktm integer_id;
  declare variable x_serial_no integer_id;
  declare variable wersjaref integer_id;
  declare variable symbolwt symbol_id;
  declare variable symbolwz symbol_id;
  declare variable wersjarefwz integer_id;
  declare variable programwt string255;
  declare variable programwz string255;
  declare variable eol string3;
  declare variable numerpozwt integer_id;
  declare variable numerpozwz integer_id;
  declare variable grupaprod integer_id;
  declare variable kodkresk string255;
  declare variable wzref integer_id;
begin
eol = '
';
zpl = '';


  select first 1 m.good, m.x_serial_no, m.vers, m.number    -- pobranie danych z WT
    from mwsacts m
    where m.mwsord = :wtref
      --and m.number = :wtpoz
  into :ktm, :x_serial_no, :wersjaref, :numerpozwt;

  select m.symbol, m.docid -- pobranie symbolu WT i RWref
    from mwsords m
    where m.ref = :wtref
  into :symbolwt, rwref;

  select d.grupaprod  -- pobranie z RW, WZpozref
    from dokumnag d
    where d.ref = :rwref
  into :grupaprod;

  select first 1 d.wersjaref, d.numer, d.dokument -- wersja towaru z WZ, pozycja WZ, WZref
    from dokumpoz d
    where d.ref = :grupaprod
 into :wersjarefwz, :numerpozwz, :wzref;

  select d.symbol -- symbol WZ
    from dokumnag d
    where d.ref = :wzref
  into :symbolwz;

  select x.nazwat
    from wersje x
    where x.ref = :wersjaref
  into :programwt;

  select x.nazwat||' '||x.nazwa
    from wersje x
    where x.ref = :wersjarefwz
  into :programwz;

  kodkresk = coalesce(:WTREF, ' ')||'#'||coalesce(numerpozwz, ' ');  --XXX  KBI

  zpl = 'CT~~CD,~CC^~CT~
        ^XA~TA000~JSN^LT0^MNW^MTD^PON^PMN^LH0,0^JMA^PR5,5~SD15^JUS^LRN^CI0^XZ
        ^XA
        ^MMT
        ^PW799
        ^LL1199
        ^LS0'||:eol;
  zpl = zpl||'
^FT714,331^A0R,56,55^FH\^FDPrzekazanie do serwisu^FS
^FT600,453^A0R,28,28^FH\^FD'||coalesce(x_serial_no,  0)||'^FS
^FT540,453^A0R,28,28^FH\^FD'||coalesce(symbolwt,  '-')||'^FS
^FT640,453^A0R,28,28^FH\^FD'||coalesce(ktm, '-')||'^FS
^FT440,453^A0R,28,28^FH\^FD'||coalesce(symbolwz,  '-')||'^FS
^FT600,57^A0R,28,28^FH\^FDSerial NO:^FS
^FT340,453^A0R,28,28^FH\^FD'||coalesce(programwt,  '-')||'^FS
^FT300,453^A0R,28,28^FH\^FD'||coalesce(programwz,  '-')||'^FS
^FT500,453^A0R,28,28^FH\^FD'||coalesce(numerpozwt,  0)||'^FS
^FT640,57^A0R,28,28^FH\^FDTowar:^FS
^FT540,57^A0R,28,28^FH\^FDDokument WT:^FS
^FT400,453^A0R,28,28^FH\^FD'||coalesce(numerpozwz,  0)||'^FS
^FT500,57^A0R,28,28^FH\^FDPozycja:^FS
^FT440,57^A0R,28,28^FH\^FDDokument WZ:^FS
^FT400,57^A0R,28,28^FH\^FDPozycja:^FS
^FT340,57^A0R,28,28^FH\^FDAktualne oprogramoanie:^FS
^FT300,57^A0R,28,28^FH\^FDOprogramowanie docelowe:^FS
^FT50,1170^BQN,2,10
^FH\^FDLA,'||coalesce(:kodkresk,'')||'^FS
^PQ1,0,1,Y^XZ';

  suspend;
end^
SET TERM ; ^
