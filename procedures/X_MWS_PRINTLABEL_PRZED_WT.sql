--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_PRINTLABEL_PRZED_WT(
      MWSORD MWSORDS_ID,
      CART MWSCONSTLOCS_ID)
  returns (
      ZPL STRING1024)
   as
  declare variable listher_id string80;
  declare variable listdokumnag string80;
  declare variable mwsord_symbol docsymbol_id;
  declare variable nazwa_klienta string255;   
  declare variable kolor string20;
  declare variable nazwa_odbiorcy string255;
  declare variable sposob_transportu string255;

begin

   --exception test_break 'mwsord: '||coalesce(:mwsord,-1)||' cart: '||coalesce(:cart,-1) ;

  select first 1 substring(list(distinct d.int_symbol) from 1 for 80),
          substring(list(distinct d.symbol) from 1 for 80),
          substring(list(distinct o.symbol) from 1 for 20),
          substring(list(distinct coalesce(k.nazwa,'')) from 1 for 255),
          substring(list(distinct coalesce(od.nazwa,'')) from 1 for 255),
          substring(list(distinct coalesce(s.nazwa,'')) from 1 for 255)

     from mwsacts a
       join dokumnag d on (d.ref = a.docid)
       join mwsords o on (o.ref = a.mwsord)
       left join odbiorcy od on (d.odbiorcaid=od.ref)
       left join klienci k on (d.klient=k.ref)
       left join sposdost s on (s.ref = d.sposdost)
     where a.mwsord=:mwsord 
       and a.mwsconstloc=:cart
    into :listher_id, :listdokumnag, :mwsord_symbol, :nazwa_klienta, :nazwa_odbiorcy, :sposob_transportu;

    kolor='';
   select upper(x.ntekst)
     from mwsconstlocs m
       join mwspallocs p on (p.mwsconstloc = m.ref)
       join MWSCARTCOLOURS c on (c.ref=p.x_mwscartcolour)
       left join x_usun_polskie_znaki(c.name,1) X on (1=1)
     where m.ref = :cart
   into :kolor;

-- budowanie etykiety
    zpl = 'CT~~CD,~CC^~CT~
          ^XA~TA000~JSN^LT0^MNW^MTD^PON^PMN^LH0,0^JMA^PR5,5~SD15^JUS^LRN^CI0^XZ
          ^XA
          ^MMT
          ^PW799
          ^LL1199
          ^LS0
^FT160,60^A0R,50,50^FH\^FDHER_ID:^FS
^FT90,330^A0R,50,50^FH\^FD'||:sposob_transportu||'^FS
^FT90,60^A0R,50,50^FH\^FDSPOS TRANS:^FS
^FT420,60^A0R,50,50^FH\^FDODBIORCA: ^FS
^FT420,330^A0R,50,50^FH\^FD'||:nazwa_odbiorcy||'^FS
^FT160,330^A0R,50,50^FH\^FD'||coalesce(substring(:listher_id from 1 for 32),'')||'^FS
^FT110,330^A0R,50,50^FH\^FD'||coalesce(substring(:listher_id from 33 for 32),'')||'^FS
^FT350,60^A0R,50,50^FH\^FDSENTE MG: ^FS
^FT350,330^A0R,50,50^FH\^FD'||coalesce(substring(:listdokumnag from 1 for 36),'')||'^FS
^FT300,330^A0R,50,50^FH\^FD'||coalesce(substring(:listdokumnag from 37 for 36),'')||'^FS
^FT230,60^A0R,50,50^FH\^FDSENTE WMS: ^FS
^FT230,330^A0R,50,50^FH\^FD'||coalesce(substring(:mwsord_symbol from 1 for 36),'')||'^FS
^FT610,60^A0R,70,70^FH\^FD'||coalesce(substring(:nazwa_klienta from 1 for 30),'')||'^FS
^FT550,60^A0R,70,70^FH\^FD'||coalesce(substring(:nazwa_klienta from 31 for 30),'')||'^FS
^FT490,60^A0R,70,70^FH\^FD'||coalesce(substring(:nazwa_klienta from 61 for 30),'')||'^FS
^FT430,60^A0R,70,70^FH\^FD'||coalesce(substring(:nazwa_klienta from 91 for 30),'')||'^FS
^FT710,800^A0R,80,80^FH\^FD'||coalesce(:kolor,'')||'^FS
^FO50,16^GB0,1147,4^FS
^PQ1,0,1,Y^XZ
';
  suspend;
end^
SET TERM ; ^
