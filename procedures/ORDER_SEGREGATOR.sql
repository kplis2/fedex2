--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ORDER_SEGREGATOR(
      RODZIC integer,
      OLDN integer,
      NEWN integer)
   as
declare variable ref integer;
  declare variable number integer;
begin
  --firts we find minor number
  if (oldn < newn) then
    newn = oldn;

  number = newn;
  for
    select ref
      from SEGREGATOR
      where ((rodzic = :rodzic)or(rodzic is null)) and numer >= :number
      order by numer, ord
      into :ref
  do begin
    update SEGREGATOR set numer = :number, ord = 2
      where ref = :ref;
    number = number + 1;
  end

  update SEGREGATOR set ord = 1
    where rodzic = :rodzic and numer >= :newn;
end^
SET TERM ; ^
