--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDULE_CALC_AVGLOAD(
      PRSCHEDULE integer,
      LASTOPER integer,
      OPERCOUNT integer)
  returns (
      SCHEDOPER integer,
      CNT integer,
      C integer)
   as
declare variable curtime timestamp;
declare variable schednum integer;
declare variable isoper smallint;
declare variable status smallint;
declare variable tstart timestamp;
declare variable tdepend timestamp;
declare variable tend timestamp;
declare variable machine integer;
declare variable verifiednum integer;
declare variable shoper integer;
declare variable worktime float;
declare variable workplace varchar(20);
declare variable prdepart varchar(20);
declare variable matreadytime timestamp;
declare variable laststatus smallint;
declare variable workload double precision;
declare variable tprevend timestamp;
declare variable rstart timestamp;
declare variable rend timestamp;
declare variable rmachine integer;
declare variable rschedoper integer;
declare variable latenes double precision;
declare variable minlatenes double precision;
declare variable minspace integer;
declare variable round integer;
declare variable minlength double precision;
declare variable rthoper integer;
declare variable tdependr timestamp;
declare variable rstartfwt timestamp;
declare variable tstartfwt timestamp;
declare variable machinecount integer;
declare variable altoper smallint;
declare variable brakmat smallint;
declare variable sql varchar(1024);
declare variable paramd2 double precision;
declare variable prganttref bigint;
declare variable rprganttref bigint;
begin
  c = 0;
  laststatus = 2;
  select PRSCHEDULES.mintimebetweenopers, prschedules.roundstarttime,  prschedules.fromdate, prschedules.prdepart
      from prschedules
      where prschedules.ref = :prschedule
      into :minspace, :round, :curtime, :prdepart;
  minlength = 1440;
  minlength = 1/:minlength;
  if(:round > :minspace or (:minspace is null) ) then minspace = :round;
  minlength = :minlength * :minspace;

  if(:lastoper = 0) then begin

    verifiednum = 0;
    --ustawianei poczatkowa readytoharm
    update prgantt set readytosched = 0 where prschedule = :prschedule and gtype = 0;
    for select a.ref
      from prgantt A
      where A.PRSCHEDULE = :prschedule and A.VERIFIED = 0 and a.gtype = 0
        and a.depfrom is null
      into :prganttref
    do begin
      update prgantt set readytosched = 1 where ref = :prganttref;
    end
    --wyliczenie obciażenia

    for select max(g.workplace),  sum(g.worktime)
      from prgantt g
      where prschedule = :prschedule and verified = 0
      group by workplace
      into :workplace, :workload
    do begin
--      select count(ref) from prmachines where prworkplace=:workplace into machinecount;
      select count (distinct w.prmachine) from prmachineswplaces w where w.prworkplace = :workplace into :machinecount;
      if (:machinecount=0 or machinecount is null) then machinecount=1;
      paramd2 = cast((:workload/:machinecount) as double precision);
      if (:paramd2 = 0) then
        paramd2 = 0;
      else
        paramd2 = 1/paramd2;
      update prgantt
        set paramd1 = :workload/:machinecount,
          paramd2 = :paramd2,
          parami1 = (case  when status = 0 then 10 else 1 / cast (status as double precision) end)
      where prschedule = :prschedule and verified = 0 and WORKPLACE = :workplace;
    end
  end else
    select VERIFIED, STATUS from PRschedopers where ref=:lastoper into :verifiednum, :laststatus;

  cnt = 0;
  if(:curtime is null or (:curtime < current_timestamp(0)) ) then curtime = current_timestamp(0);
  while(:cnt < :opercount and :lastoper >= 0) do begin
    verifiednum = :verifiednum + 1;
    for select first 1 a.ref
      from prgantt A
      where A.PRSCHEDULE = :prschedule and A.VERIFIED = 0
        and a.readytosched = 1
      order by coalesce(a.priority,0) desc,
        A.prschedule, A.verified,
        A.status desc
      into :prganttref
    do begin
      execute procedure PRSCHEDULE_CHECKMATERIALSTATE(:prganttref);
    end
    schedoper = null;
    rschedoper = null;
    minlatenes = null;

    if(exists (select ref from prgantt where PRSCHEDULE = :prschedule and VERIFIED = 0 and readytosched = 1 and materialreadytime is not null)) then
      brakmat = 1;
    else
      brakmat = 0;

    for
      select a.ref, a.timefrom, a.timeto, a.STATUS, a.prMACHINE, a.prshoper, a.materialreadytime, a.workplace, a.worktime, a.prschedoper
        from prgantt A
        where A.PRSCHEDULE = :prschedule and A.VERIFIED = 0 and a.readytosched = 1
          and (a.materialreadytime is not null or a.verified=:brakmat)
        order by A.prschedule, A.verified, a.readytosched, A.paramd2 , A.parami1
        into :prganttref, :tstart,  :tend, :status, :machine, :shoper, :matreadytime, :workplace, worktime, schedoper
    do begin
      if(:minlatenes <= :minlength) then
        break;  -- jesli ostatni odstep jest wystarczajaco maly, to nei szukam dalej
      tdepend = null;
      --okreslenie minimalnego czasu rozpoczecia
      select max(p.timeto)
        from prschedoperdeps d
          left join prgantt p on (p.prschedoper = d.depfrom)
        where d.depto = :schedoper
        into :tdepend;
      if(:tdepend is null) then tdepend = :curtime;
      if(:tstart is null or :tstart < :tdepend )then begin
        tstart = tdepend;
      end
      --sprawdzenie zaleznosci materialowej
      if(:matreadytime > :tstart) then
        tstart  = :matreadytime;
     --sprawdzenie, czy start nie wypad w czasie przerwy pracy - jesli tak, to na poczatek kolejnego czasu
       execute procedure PRSCHED_CALENDAR_FIRSTWORKTIME(:prdepart,:tstart) returning_values
         :tstartfwt;
      if(:workplace is not null) then
      begin
        execute procedure  prschedule_find_machine(:prganttref,:tstartfwt, 0) returning_values :tstart, :tend, :machine;
        --sprawdzenie opoznienia w stosunku do poprzedzajacej oepracji na danej maszynie
        tprevend = null;
        select max(f.timeto)
        from prgantt f
        where f.prschedule = :prschedule
          and f.prmachine = :machine
          and f.timeto <= :tstart
          and f.ref <> :prganttref
        into :tprevend;
        if(:tprevend < :curtime) then
           tprevend = null;--jesli poprzednie operacje już si zakonczyly/powinny byly zakonczyc, to tak, jakby ich nie bylo
        if(:tprevend is null) then latenes = 0;
        else latenes = :tstart - :tprevend;
        if(:minlatenes is null or (minlatenes > :latenes))then begin
          rschedoper = schedoper;
          rmachine = :machine;
          rprganttref = prganttref;
          rstart = :tstart;
          rend = :tend;
          minlatenes = :latenes;
          rstartfwt = :tstartfwt;
        end
      end else begin
        machine = null;
        execute procedure prsched_checkoperstarttime(:schedoper, :tstart) returning_values :tstart;
        execute procedure pr_calendar_checkendtime(:prdepart, :tstart, :worktime) returning_values :tend;
        rschedoper = schedoper;
        rmachine = machine;
        rstart = :tstart;
        rprganttref = prganttref;
        rstartfwt = :tstart;
        rend = :tend;
        minlatenes = 0;
      end
      c = :c + 1;
      if (c = 5000) then exit;
    end
    if(:rschedoper is null) then begin
      -- zakończenie harmonogramowania operacji
      schedoper = -1;
      exit;
    end
    update prgantt set verified  = :verifiednum where ref=:rschedoper;
--    select count(ref) from prschedoperdeps where depfrom=(select min(depfrom) from prschedoperdeps where depto=:rschedoper) into :altoper;
  --  if (:altoper>1) then
    --  execute procedure prsched_calc_aib1_setalt(:rschedoper, :rstart, :rend, :rmachine, :rstartfwt, :verifiednum) returning_values :rschedoper;
  --  else
    update prgantt set timefrom = :rstart, timeto = :rend, prMACHINE = :rmachine, verified  = :verifiednum
      where ref=:rprganttref;
    --ustawianie readytoharm na innych
    for select o.ref
      from prschedoperdeps d
      left join prschedopers o on (o.ref = d.depto)
      where d.depfrom = :rschedoper and o.activ = 1
      into :rthoper
    do begin
      execute procedure prschedule_readytosched_check(:rthoper);
    end
    schedoper = rschedoper;
    cnt = :cnt + 1;
  end
end^
SET TERM ; ^
