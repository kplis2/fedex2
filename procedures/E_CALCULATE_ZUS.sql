--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_CALCULATE_ZUS(
      PERIOD varchar(10) CHARACTER SET UTF8                           ,
      CURRENTCOMPANY integer)
  returns (
      VAL numeric(14,4))
   as
declare variable FC numeric(14,2);
declare variable FEP numeric(14,2);
declare variable FRP numeric(14,2);
declare variable ondate date;

begin
--JK

    ondate = cast(substring(period from 1 for 4)||'-'||substring(period from 5 for 2)||'-01' as date);

    execute procedure get_pval(ondate, currentcompany, 'FC')
     returning_values FC;
    execute procedure get_pval(ondate, currentcompany, 'FEP')
     returning_values FEP;
    execute procedure get_pval(ondate, currentcompany, 'FRP')
     returning_values FRP;

     val = (1 - (FEP + FRP + FC) * 0.01);

end^
SET TERM ; ^
