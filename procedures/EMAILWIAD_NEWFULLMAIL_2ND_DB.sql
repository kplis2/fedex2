--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWIAD_NEWFULLMAIL_2ND_DB(
      TYTUL STRING2048_UTF8,
      DATA timestamp,
      ODKOGO STRING1024_UTF8,
      DOKOGO STRING2048_UTF8,
      CC STRING2048_UTF8,
      DELIVEREDTO varchar(1024) CHARACTER SET UTF8                           ,
      IDWATKU integer,
      STAN smallint,
      MAILBOX integer,
      FOLDER varchar(1024) CHARACTER SET UTF8                           ,
      ID varchar(1024) CHARACTER SET UTF8                           ,
      UID integer,
      IID integer,
      TRYB integer,
      TRESC MEMO5000_UTF8,
      FULLHTMLBODY BLOB_UTF8)
  returns (
      REF integer,
      IDOUT varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable OPERATOR integer;
declare variable FOLDERREF integer;
declare variable CONNECTION STRING255 not null;
declare variable USR STRING255 not null;
declare variable PASSWD STRING255 not null;
begin
  connection = (select cvalue from GET_CONFIG('IMAP.2DB.CONNECTION',0));
  usr = (select cvalue from GET_CONFIG('IMAP.2DB.USER',0));
  passwd = (select cvalue from GET_CONFIG('IMAP.2DB.PASSWORD',0));

  -- okresl ref foldera
  folderref = null;
  if(:folder<>'') then begin
    select first 1 ref from DEFFOLD where mailbox=:mailbox and mailfolder=:folder into :folderref;
    if(:folderref is null) then
      exception UNIVERSAL 'Brak folderu IMAP '||:folder||' dla skrzynki '||:mailbox;
  end
  /* TODO: okresl operatora na podstawie mailboxa, a mailboxa na podstawie folderu */
  -- szukamy tego maila
  if(:id<>'') then
    select first 1 ref from emailwiad where id=:id into :ref;
  if(:ref is null and :iid>0) then
    select ref from emailwiad where folder = :folderref and iid = :iid into :ref;
  if(:ref is null and :uid>0) then
    select ref from emailwiad where folder = :folderref and uid = :uid into :ref;

  if(:ref is not null) then begin
      if(:id='') then id = :ref;
      update emailwiad set
        data = :data,
        email = 1,
        html = 1,
        deliveredto = :deliveredto,
        idwatku = :idwatku,
        stan = :stan,
        operator = :operator,
        mailbox = :mailbox,
        folder = :folderref,
        id = :id,
        uid = :uid,
        iid = :iid,
        tryb = :tryb
      where ref=:ref;

  end else begin
    execute procedure gen_ref('EMAILWIAD') returning_values :ref;
    if(:id='') then id = :ref;
    insert into EMAILWIAD(REF,TYTUL,DATA,EMAIL,HTML,OD, DOKOGO, CC, DELIVEREDTO, IDWATKU, STAN, OPERATOR, MAILBOX, FOLDER,
       ID, UID, IID, TRYB, tresc, fullhtmlbody)
    values (:ref, :tytul, :data, 1,1, :odkogo, :dokogo, :cc, :deliveredto, :idwatku, :stan, :operator, :mailbox, :folderref,
       :id, :uid, :iid,  :tryb, :tresc, :fullhtmlbody);

  end

  execute statement ('execute procedure emailwiad_newfullmailfromimap(:tytul,'||
        ':odkogo,:dokogo,:cc,:mailbox,:id,:tresc,:fullhtmlbody)') (
    tytul := tytul,
    odkogo := odkogo,
    dokogo := dokogo,
    cc := cc,
    mailbox := mailbox,
    id := id,
    tresc := tresc ,
    fullhtmlbody:=fullhtmlbody)
  on external :connection
  as user :usr
  password :passwd;

  idout = :id;
end^
SET TERM ; ^
