--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_TIME_UWAGI_ANAL(
      DATA_OD TIMESTAMP_ID,
      DATA_DO TIMESTAMP_ID)
  returns (
      OPERATORREF OPERATOR_ID,
      OPERNAME STRING255,
      SYMBOL SYMBOL_ID,
      TYPDOKUMENTU DEFDOKUM_ID,
      REFDOKUMENTU INTEGER_ID,
      REFPOZYCJI INTEGER_ID,
      KTM KTM_ID,
      NAZWATOW TOWARY_NAZWA,
      CZAS_ROZPOCZECIA TIMESTAMP_ID,
      CZAS_ZAKONCZENIA TIMESTAMP_ID,
      CZAS_TRWANIA TIME_DIFERENCE,
      REF INTEGER_ID)
   as
declare variable typ smallint_id;
begin
    --procedura zbierajaca dane analizujace czas czytania uwag
    --uwagi na zleceniach
   if (data_od is null) then
        data_od = cast('1900-1-1' as timestamp_id);
    if (data_do is null) then
        data_do = current_timestamp;
    for select xt.DOKUMENT, xt.REF, xt.TIME_START, xt.TIME_STOP, xt.OPERATOR,
                mo.SYMBOL, xt.POZYCJA, xt.TYP, o.nazwa, mp.good, t.nazwa
        from X_MWS_TIMEREADCOMMENTS xt
            left join mwsords mo on (mo.ref = xt.dokument)
            left join mwsacts mp on (mp.ref = xt.pozycja)
            left join operator o on (o.ref = xt.operator)
            left join towary t   on (t.ktm = mp.good)
        where xt.typ in(1,2,3)
            and xt.time_start >= :data_od
            and xt.time_stop <= :data_do
    into :refdokumentu, :REF, :czas_rozpoczecia, :czas_zakonczenia, :OPERATORref,
         :SYMBOL, :refpozycji, :typ, :opername, :ktm, :nazwatow
    do begin
        czas_trwania = czas_zakonczenia - czas_rozpoczecia;
        suspend;
    end
end^
SET TERM ; ^
