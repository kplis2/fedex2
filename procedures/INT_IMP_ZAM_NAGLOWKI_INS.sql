--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_ZAM_NAGLOWKI_INS(
      NAGID type of column INT_IMP_ZAM_NAGLOWKI.NAGID,
      NUMER type of column INT_IMP_ZAM_NAGLOWKI.NUMER,
      SYMBOL type of column INT_IMP_ZAM_NAGLOWKI.SYMBOL,
      TYP type of column INT_IMP_ZAM_NAGLOWKI.TYP,
      DATAZAM type of column INT_IMP_ZAM_NAGLOWKI.DATAZAM,
      WYDANIA type of column INT_IMP_ZAM_NAGLOWKI.WYDANIA,
      ZEWNETRZNY type of column INT_IMP_ZAM_NAGLOWKI.ZEWNETRZNY,
      BN type of column INT_IMP_ZAM_NAGLOWKI.BN,
      MAGAZYN type of column INT_IMP_ZAM_NAGLOWKI.MAGAZYN,
      MAGAZYN2 type of column INT_IMP_ZAM_NAGLOWKI.MAGAZYN2,
      PARTIA type of column INT_IMP_ZAM_NAGLOWKI.PARTIA,
      PARTIAID type of column INT_IMP_ZAM_NAGLOWKI.PARTIAID,
      ZAPLATA type of column INT_IMP_ZAM_NAGLOWKI.ZAPLATA,
      ZAPLATAID type of column INT_IMP_ZAM_NAGLOWKI.ZAPLATAID,
      DOSTAWA type of column INT_IMP_ZAM_NAGLOWKI.DOSTAWA,
      DOSTAWAID type of column INT_IMP_ZAM_NAGLOWKI.DOSTAWAID,
      WALUTA type of column INT_IMP_ZAM_NAGLOWKI.WALUTA,
      KURS type of column INT_IMP_ZAM_NAGLOWKI.KURS,
      UWAGIWEW type of column INT_IMP_ZAM_NAGLOWKI.UWAGIWEW,
      UWAGIZEW type of column INT_IMP_ZAM_NAGLOWKI.UWAGIZEW,
      UWAGISPED type of column INT_IMP_ZAM_NAGLOWKI.UWAGISPED,
      WARTOSCNETTO type of column INT_IMP_ZAM_NAGLOWKI.WARTOSCNETTO,
      WARTOSCBRUTTO type of column INT_IMP_ZAM_NAGLOWKI.WARTOSCBRUTTO,
      WARTOSCNETTOZL type of column INT_IMP_ZAM_NAGLOWKI.WARTOSCNETTOZL,
      WARTOSCBRUTTOZL type of column INT_IMP_ZAM_NAGLOWKI.WARTOSCBRUTTOZL,
      DOZAPLATY type of column INT_IMP_ZAM_NAGLOWKI.DOZAPLATY,
      BLOKADA type of column INT_IMP_ZAM_NAGLOWKI.BLOKADA,
      TYPDOKFAK type of column INT_IMP_ZAM_NAGLOWKI.TYPDOKFAK,
      TYPDOKMAG type of column INT_IMP_ZAM_NAGLOWKI.TYPDOKMAG,
      HASHVALUE type of column INT_IMP_ZAM_NAGLOWKI.HASH,
      DEL type of column INT_IMP_ZAM_NAGLOWKI.DEL,
      REC type of column INT_IMP_ZAM_NAGLOWKI.REC,
      SESJA type of column INT_IMP_ZAM_NAGLOWKI.SESJA,
      STATUS type of column INT_IMP_ZAM_NAGLOWKI.STATUS,
      DOSTAWCAID type of column INT_IMP_ZAM_NAGLOWKI.DOSTAWCAID,
      KLIENTID type of column INT_IMP_ZAM_NAGLOWKI.KLIENTID,
      ODBIORCAID type of column INT_IMP_ZAM_NAGLOWKI.ODBIORCAID,
      X_ZWOLNIJPAKOWANIE type of column INT_IMP_ZAM_NAGLOWKI.X_ZWOLNIJPAKOWANIE)
  returns (
      REF type of column INT_IMP_ZAM_NAGLOWKI.REF)
   as
begin

  insert into int_imp_zam_naglowki (
    nagid, numer, symbol, typ, datazam, wydania, zewnetrzny, bn, magazyn, magazyn2,
    partia, partiaid, zaplata, zaplataid, dostawa, dostawaid, waluta, kurs,
    uwagiwew, uwagizew, uwagisped, wartoscnetto, wartoscbrutto, wartoscnettozl, wartoscbruttozl,
    dozaplaty, blokada, typdokfak, typdokmag, "HASH", del, rec, sesja, status,
    dostawcaid, klientid, odbiorcaid, x_zwolnijpakowanie)
  values (
    :nagid, :numer, :symbol, :typ, :datazam, :wydania, :zewnetrzny, :bn, :magazyn, :magazyn2,
    :partia, :partiaid, :zaplata, :zaplataid, :dostawa, :dostawaid, :waluta, :kurs,
    :uwagiwew, :uwagizew, :uwagisped, :wartoscnetto, :wartoscbrutto, :wartoscnettozl, :wartoscbruttozl,
    :dozaplaty, :blokada, :typdokfak, :typdokmag, :hashvalue, :del, :rec, :sesja, :status,
    :dostawcaid, :klientid, :odbiorcaid, :x_zwolnijpakowanie)
    returning ref into :ref;


end^
SET TERM ; ^
