--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MSGORD_PARAM(
      MSGORD integer,
      AKRONIM varchar(30) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
begin
  insert into msgordpar(MSGORD, AKRONIM, VAL) values (:msgord,:akronim,:val);
end^
SET TERM ; ^
