--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_3(
      BKDOC integer)
   as
declare variable account ACCOUNT_ID;
  declare variable amount numeric(14,2);
  declare variable tmp varchar(255);
  declare variable settlement varchar(20);
  declare variable docdate timestamp;
  declare variable payday timestamp;
  declare variable descript varchar(255);
  declare variable dictpos integer;
  declare variable dictdef integer;
  declare variable debited smallint;
  declare variable account1 ACCOUNT_ID;
  declare variable account2 ACCOUNT_ID;
  declare variable vatv numeric(14,2);
  declare variable vate numeric(14,2);
  declare variable zagraniczny smallint;
  declare variable bankaccount integer;
  declare variable period varchar(6);
  declare variable vatperiod varchar(6);
  declare variable vperiod varchar(1);
  declare variable DIST1DDEF INTEGER;
  declare variable DIST1SYMBOL SYMBOLDIST_ID;
  declare variable DIST2DDEF INTEGER;
  declare variable DIST2SYMBOL SYMBOLDIST_ID;
  declare variable DIST3DDEF INTEGER;
  declare variable DIST3SYMBOL SYMBOLDIST_ID;
  declare variable DIST4DDEF INTEGER;
  declare variable DIST4SYMBOL SYMBOLDIST_ID;
  declare variable DIST5DDEF INTEGER;
  declare variable DIST5SYMBOL SYMBOLDIST_ID;
  declare variable fsopertype smallint;
  declare variable crnote smallint;
  declare variable taxgr varchar(10);
begin
  -- DEMO: standardowy schemat dekretowania - dokumenty zakupu kosztowe

  select B.sumgrossv, B.symbol, B.docdate, B.payday, B.descript, B.dictdef, B.dictpos,
      B.bankaccount, T.creditnote, B.period, B.vatperiod
    from bkdocs B
      join bkdoctypes T on (B.doctype = T.ref)
    where B.ref = :bkdoc
    into :amount, :settlement, :docdate, :payday, :descript, :dictdef, :dictpos,
      :bankaccount, :crnote, :period, :vatperiod;

  if (coalesce(vatperiod,'') = period) then
    vperiod = '0';
  else
    vperiod = '1';

  execute procedure KODKS_FROM_DICTPOS(:dictdef, :dictpos)
    returning_values :tmp;

  select zagraniczny from dostawcy where kontofk = :tmp
    into :zagraniczny;

  if (zagraniczny = 0) then
    account = '201-' || tmp;
  else
    account = '203-' || tmp;


  if (crnote = 1) then
    fsopertype = 4;
  else
    fsopertype = 3;



  execute procedure insert_decree_settlement(bkdoc, account, 1, amount, descript,
     settlement, null, fsopertype, 1, docdate, payday, null, null, null, :bankaccount, 0);


 for
    select netv, vatv, vate, account1, account2, descript, debited, taxgr,
        dist1ddef, dist1symbol, dist2ddef, dist2symbol, dist3ddef, dist3symbol,
        dist4ddef, dist4symbol, dist5ddef, dist5symbol
      from bkvatpos where bkdoc=:bkdoc
      into :amount, :vatv, :vate, :account1, :account2, :descript, :debited, :taxgr,
        :dist1ddef, :dist1symbol, :dist2ddef, :dist2symbol, :dist3ddef, :dist3symbol,
        :dist4ddef, :dist4symbol, :dist5ddef, :dist5symbol
  do begin

    if (debited = 1) then
    begin
      execute procedure insert_decree(bkdoc, '221-' || vperiod || '-' || taxgr, 0, vatv, descript, 1);
      if (vate - vatv > 0) then -- jeżeli nie odliczamy calosci
        amount = amount + (vate-vatv);
    end else
      amount = amount + vate;

    if (account1 is not null and account1 <> '') then
      execute procedure insert_decree_dists(bkdoc, account1, 0, amount, descript,
        dist1ddef, dist1symbol,dist2ddef, dist2symbol,dist3ddef, dist3symbol,
        dist4ddef, dist4symbol, dist5ddef, dist5symbol, 0);

    if (account2 is not null and account2 <> '') then
    begin
      execute procedure insert_decree(bkdoc, '490', 1, amount, descript, 1);

      execute procedure insert_decree_dists(bkdoc, account2, 0, amount, descript,
        dist1ddef, dist1symbol,dist2ddef, dist2symbol, dist3ddef, dist3symbol,
        dist4ddef, dist4symbol, dist5ddef, dist5symbol, 0);
    end
  end
  for
   select cvalue, account, dist1ddef, dist1symbol
     from bkdoccostdistr
     where bkdoc = :bkdoc
     into :amount, :account, :dist1ddef, :dist1symbol
  do begin
    execute procedure insert_decree_dists(bkdoc, account, 0, amount, 'rozliczenie kosztów',
      dist1ddef, dist1symbol,dist2ddef, dist2symbol,dist3ddef, dist3symbol,
      dist4ddef, dist4symbol, dist5ddef, dist5symbol, 0);


  end
end^
SET TERM ; ^
