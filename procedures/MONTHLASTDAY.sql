--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MONTHLASTDAY(
      D timestamp)
  returns (
      L timestamp)
   as
declare variable y int;
  declare variable m int;
begin
  y = extract(year from d);
  m = extract(month from d);
  if (m = 12) then
  begin
    m = 1;
    y = y + 1;
  end else
  begin
    m = m + 1;
  end
  l = y || '-' || m || '-01';
  l = l - 1;
  suspend;
end^
SET TERM ; ^
