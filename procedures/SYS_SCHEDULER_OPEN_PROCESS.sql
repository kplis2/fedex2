--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_SCHEDULER_OPEN_PROCESS(
      NEOS_NOW TIMESTAMP_ID,
      ACTIVITY_SCHEDULE SYS_SCHEDULE_ID,
      INSTANCE integer)
  returns (
      RET smallint)
   as
declare variable TMP timestamp;
begin
  --Procedura sprawdza czy nie istnieje instancja klasy schedulingu
  --Jezeli nie istnieje to zaczyna otwierac
  ret = 0;
  if (not exists(select first 1 1
                   from sys_schedule s
                   where s.ref = :activity_schedule
                     and s.lastinstance is not null)) then
  begin
    update sys_schedulehist set STARTT = :neos_now
      where schedule = :activity_schedule
        and ref = :instance;

    update sys_schedule set LASTINSTANCE = :instance
      where ref = :activity_schedule;

    if (not exists(select first 1 1
                     from sys_schedulehist
                     where ref = :instance)) then
      exception universal 'Nie istnieje instancja  ' || :instance;
    ret = 1;
  end
  suspend;
end^
SET TERM ; ^
