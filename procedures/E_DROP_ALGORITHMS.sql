--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_DROP_ALGORITHMS as
declare variable PROCEDURE_NAME varchar(31);
declare variable STATEMENT_TEXT varchar(255);
begin
  for
   select rdb$procedure_name from rdb$procedures
     where trim(rdb$procedure_name) = 'XX_E_ALGORITHM'
       --or rdb$procedure_name starting with 'XX_FR_ALGORITHM_'
       --or rdb$procedure_name starting with 'XX_FRD_ALGORITHM_'
     into :procedure_name
  do begin
    statement_text = 'drop procedure ' || procedure_name;
    execute statement statement_text;
  end
end^
SET TERM ; ^
