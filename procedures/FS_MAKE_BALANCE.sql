--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FS_MAKE_BALANCE(
      FSCLRACCH integer)
   as
declare variable currdebit2clear numeric(14,2);
  declare variable currcredit2clear numeric(14,2);
  declare variable currdebit2c numeric(14,2);
  declare variable currcredit2c numeric(14,2);
  declare variable currdebit numeric(14,2);
  declare variable currcredit numeric(14,2);
  declare variable fsclraccp_ref integer;
  declare variable autopos smallint;
  declare variable side smallint;
  declare variable toclear numeric(14,2);
begin
  update fsclraccp set autochange=1
    where fsclracch=:fsclracch;
  select sum(case autopos when 1 then currdebit else currdebit2c end), sum(case autopos when 1 then currcredit else currcredit2c end)
    from fsclraccp
    where fsclracch=:fsclracch
    into :currdebit2clear, :currcredit2clear;

  if (currdebit2clear>currcredit2clear) then
    currdebit2clear = currcredit2clear;
  else
    currcredit2clear = currdebit2clear;

  for
    select ref, currdebit, currcredit, autopos, currdebit2c, currcredit2c, side
    from fsclraccp
    where fsclracch=:fsclracch
    order by autopos, number
    into :fsclraccp_ref, :currdebit, :currcredit, :autopos, :currdebit2c, :currcredit2c, :side
  do begin
    if (side=0) then
    begin
      if (autopos=1) then
      begin
        if (currdebit2clear>currdebit) then
          toclear = currdebit;
        else
          toclear = currdebit2clear;
        update fsclraccp set currdebit2c=:toclear where ref=:fsclraccp_ref;
        currdebit2clear = currdebit2clear - toclear;
      end
      else
        currdebit2clear = currdebit2clear - currdebit2c;
    end
    else begin
      if (autopos=1) then
      begin
        if (currcredit2clear>currcredit) then
          toclear = currcredit;
        else
          toclear = currcredit2clear;
        update fsclraccp set currcredit2c=:toclear where ref=:fsclraccp_ref;
        currcredit2clear = currcredit2clear - toclear;
      end
      else
        currcredit2clear = currcredit2clear - currcredit2c;
    end
  end
  update fsclraccp set autochange=0
    where fsclracch=:fsclracch;
end^
SET TERM ; ^
