--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_MAG_OBROTY(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      ODDATY timestamp,
      DODATY timestamp,
      ODKTM varchar(40) CHARACTER SET UTF8                           ,
      DOKTM varchar(40) CHARACTER SET UTF8                           ,
      TRYB integer)
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      MIARA varchar(10) CHARACTER SET UTF8                           ,
      WERSJA integer,
      KTMNAZWA varchar(255) CHARACTER SET UTF8                           ,
      WERSJANAZWA varchar(255) CHARACTER SET UTF8                           ,
      STANP numeric(14,4),
      WARTP numeric(14,2),
      ILPRZY numeric(14,4),
      WARTPRZY numeric(14,2),
      ILROZ numeric(14,4),
      WARTROZ numeric(14,2),
      STANK numeric(14,4),
      WARTK numeric(14,2))
   as
declare variable stanpm numeric(14,4);
declare variable wartpm numeric(14,2);
begin
  if(:odktm is null) then odktm = '';
  if(:doktm is null) then doktm = '';
    for select max(dokumpoz.ktm), max(wersje.miara), max(dokumpoz.wersja),
               max(wersje.nazwat), max(wersje.nazwa)
         , sum(case when DOKUMNAG.DATA<:oddaty and DEFDOKUM.WYDANIA=0 then DOKUMPOZ.ilosc else 0 end)
         , sum(case when DOKUMNAG.DATA<:oddaty and DEFDOKUM.WYDANIA=0 then DOKUMPOZ.wartosc else 0 end)
         , sum(case when DOKUMNAG.DATA<:oddaty and DEFDOKUM.WYDANIA=1 then DOKUMPOZ.ilosc else 0 end)
         , sum(case when DOKUMNAG.DATA<:oddaty and DEFDOKUM.WYDANIA=1 then DOKUMPOZ.wartosc else 0 end)
         , sum(case when DOKUMNAG.DATA>=:oddaty and DEFDOKUM.WYDANIA=0 then DOKUMPOZ.ilosc else 0 end)
         , sum(case when DOKUMNAG.DATA>=:oddaty and DEFDOKUM.WYDANIA=0 then DOKUMPOZ.wartosc else 0 end)
         , sum(case when DOKUMNAG.DATA>=:oddaty and DEFDOKUM.WYDANIA=1 then DOKUMPOZ.ilosc else 0 end)
         , sum(case when DOKUMNAG.DATA>=:oddaty and DEFDOKUM.WYDANIA=1 then DOKUMPOZ.wartosc else 0 end)
         from DOKUMNAG
         join DOKUMPOZ on (DOKUMPOZ.dokument = DOKUMNAG.REF)
         left join DEFDOKUM on (DOKUMNAG.typ = DEFDOKUM.symbol)
         left join WERSJE on (WERSJE.REF=DOKUMPOZ.WERSJAREF)
         where DOKUMNAG.akcept in (1,8)
         and DOKUMNAG.MAGAZYN=:MAGAZYN
         AND DOKUMNAG.DATA <= :DODATY
         and (DOKUMPOZ.KTM>=:odktm or :odktm='')
         and (DOKUMPOZ.KTM<=:doktm or :doktm='')
         and coalesce(dokumpoz.fake,0) = 0
    group by dokumpoz.wersjaref
    into :ktm, :miara, :wersja,
         :ktmnazwa,:wersjanazwa,
         :stanp, :wartp,
         :stanpm, :wartpm,
         :ilprzy, :wartprzy,
         :ilroz, :wartroz
    do begin
      if(:stanp is null) then stanp = 0;
      if(:wartp is null) then wartp = 0;
      if(:stanpm is null) then stanpm = 0;
      if(:wartpm is null) then wartpm = 0;
      if(:ilprzy is null) then ilprzy = 0;
      if(:wartprzy is null) then wartprzy = 0;
      if(:ilroz is null) then ilroz = 0;
      if(:wartroz is null) then wartroz = 0;
      stanp = :stanp - :stanpm;
      wartp = :wartp - :wartpm;
      stank = :stanp + :ilprzy - :ilroz;
      wartk = :wartp + :wartprzy - :wartroz;
      if(
         (((:ilprzy <> 0)or (:ilroz <> 0)) or (:tryb = 0))
        and ((:stanp <> 0) or (:stank <> 0) or (:ilprzy <> 0)or (:ilroz <> 0))
      ) then suspend;
    end
end^
SET TERM ; ^
