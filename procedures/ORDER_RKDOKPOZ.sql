--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ORDER_RKDOKPOZ(
      DOKUMENT integer,
      OLDN integer,
      NEWN integer)
   as
declare variable ref integer;
  declare variable number integer;
begin
  --firts we find minor number
  if (oldn < newn) then
    newn = oldn;

  number = newn;
  for
    select ref
      from RKDOKPOZ
      where dokument = :dokument and lp >= :number
      order by lp, ord
      into :ref
  do begin
    update RKDOKPOZ set lp = :number, ord = 2
      where ref = :ref;
    number = number + 1;
  end

  update RKDOKPOZ set ord = 1
    where dokument = :dokument and lp >= :newn;
end^
SET TERM ; ^
