--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PERIOD2DATES(
      PERIOD varchar(6) CHARACTER SET UTF8                           )
  returns (
      FROMDATE date,
      TODATE date)
   as
declare variable PERIODMONTH smallint;
declare variable PERIODYEAR smallint;
begin
-- procedura zwaraca pierwszy i ostatni dzien zadanego okresu, jezeli w zadanym okresie miesiac jest rowny 0
-- to procedura zwraca pierwszy i ostatni dzien w danym roku
  periodmonth = substring(period from 5 for 2);
  periodyear = substring(period from 1 for 4);
  if (periodmonth > 0 and periodmonth <= 12) then
  begin
    fromdate = substring(period from 1 for 4) || '/' || substring(period from 5 for 2) || '/1';
    execute procedure monthlastday(fromdate) returning_values todate;
  end else
  if (periodmonth = 0) then
  begin
    fromdate = periodyear || '/1/1';
    todate = periodyear || '/12/31';
  end else
    exception universal 'Błędny format okresu.';
  suspend;
end^
SET TERM ; ^
