--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWSORDTYPEDEST4OP_GRANTS(
      CANCHANGEMWSCONSTLOCL smallint,
      SUPERVISOR smallint)
  returns (
      GRANTS varchar(1024) CHARACTER SET UTF8                           )
   as
begin
  grants = '';
  if (canchangemwsconstlocl > 0) then
  begin
    grants = ';';
    if (canchangemwsconstlocl = 1) then
      grants = grants||'MWSCONSTLOCLCHANGE;';
  end
  if (supervisor > 0) then
  begin
    grants = ';';
    if (supervisor = 1) then
      grants = grants||'SUPERVISOR;';
  end
end^
SET TERM ; ^
