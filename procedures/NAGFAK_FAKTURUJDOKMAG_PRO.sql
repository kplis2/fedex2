--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_FAKTURUJDOKMAG_PRO(
      FAKTURA integer,
      STAN varchar(20) CHARACTER SET UTF8                           ,
      REJFAK varchar(3) CHARACTER SET UTF8                           ,
      TYPFAK varchar(3) CHARACTER SET UTF8                           ,
      OPERATOR integer,
      DOKMAG integer,
      DATA timestamp,
      COPYCEN smallint,
      DOMAGKOR smallint,
      DOMAGOPKROZ smallint,
      PLATNIK integer,
      FAKKOSZTDOST integer,
      FAKGRUPASPED integer)
  returns (
      NEWFAK integer)
   as
declare variable bn char(1);
declare variable typdok varchar(3);
declare variable dostawca integer;
declare variable klient integer;
declare variable isdostawca integer;
declare variable isklient integer;
declare variable zakup smallint;
declare variable sposzap integer;
declare variable sposzap2 integer;
declare variable termin integer;
declare variable datazap timestamp;
declare variable rabnag numeric(14,4);
declare variable rabat numeric(14,4);
declare variable rabattab numeric(14,4);
declare variable waluta varchar(3);
declare variable waluta2 varchar(3);
declare variable iswaluta varchar(3);
declare variable sprzedawca integer;
declare variable sprzedawcazew integer;
declare variable walpln varchar(3);
declare variable walutowe smallint;
declare variable cennik integer;
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable wersjaref integer;
declare variable cenacen numeric(14,4);
declare variable cenamag numeric(14,4);
declare variable cenasprz numeric(14,4);
declare variable walcen varchar(3);
declare variable jedn integer;
declare variable jedno integer;
declare variable jednm integer;
declare variable ilosc numeric(14,4);
declare variable iloscm numeric(14,4);
declare variable ilosco numeric(14,4);
declare variable iloscmagmin numeric(14,4);
declare variable iscena smallint;
declare variable walrabcen varchar(3);
declare variable magazyn varchar(3);
declare variable opk smallint;
declare variable dokopk smallint;
declare variable dokcensprz smallint;
declare variable kosztdost numeric(14,4);
declare variable sumzal numeric(14,2);
declare variable dokpoz integer;
declare variable lokres varchar(6);
declare variable rok integer;
declare variable miesiac integer;
declare variable dokbn char(1);
declare variable vat numeric(14,4);
declare variable kurs numeric(14,4);
declare variable iskurs numeric(14,4);
declare variable sad integer;
declare variable cenawal numeric(14,4);
declare variable foc integer;
declare variable przeliczo numeric(14,4);
declare variable przeliczc numeric(14,4);
declare variable dokmagkor integer;
declare variable isfaktura integer;
declare variable katega integer;
declare variable kategb integer;
declare variable kategc integer;
declare variable zam integer;
declare variable pozfakref integer;
declare variable dokpozkorref integer;
declare variable oblcenzak integer;
declare variable oblcenkoszt integer;
declare variable oblcenfab integer;
declare variable dokumsped integer;
declare variable ssposdost integer;
declare variable skosztdostplat integer;
declare variable stermdost timestamp;
declare variable sdulica varchar(255);
declare variable sdmiasto varchar(255);
declare variable sdkodp varchar(255);
declare variable sodbiorcaid integer;
declare variable cnt integer;
declare variable grupasped integer;
declare variable kosztdostbez integer;
declare variable usltryb integer;
declare variable datazm timestamp;
declare variable fday timestamp;
declare variable lday timestamp;
declare variable pozref integer;
declare variable maxwartbru numeric(14,2);
declare variable zpakietem smallint;
declare variable promocja integer;
declare variable oddzial varchar(20);
declare variable prec smallint;
declare variable skad smallint;
begin
  select SAD from TYPFAK where SYMBOL=:typfak into :sad;
  if(:copycen is null) then copycen = 0;
  execute procedure GETCONFIG('WALPLN') returning_values :walpln;
  if(:walpln is null) then walpln = 'PLN';
  select klient,dostawca, magazyn, typ, kosztdost, kwotazal, waluta, kurs, walutowy, zamowienie, sposzap, grupasped, bn
  from DOKUMNAG where ref=:dokmag
  into :klient, :dostawca, :magazyn, :typdok, :kosztdost, :sumzal, :waluta, :kurs, :walutowe, :zam, :sposzap, :grupasped, :dokbn;
  if(:dokbn is null or (:dokbn = '') or (:dokbn = ' '))then dokbn = 'N';
  if(:platnik is null or (:platnik=0)) then platnik = :klient;
  else klient = :platnik;
  if(:kosztdost is null) then kosztdost = 0;
  if(:sumzal is null) then sumzal = 0;
  if(:waluta is null or (:waluta = ''))then waluta = :walpln;
  if(:walutowe is null) then walutowe = 0;
  if(:kurs is null or (:kurs = 0)) then kurs = 1;
  select OPAK from DEFDOKUM where symbol = :typdok into :dokopk;
  if(:dokopk is null) then dokopk = 0;
  select redcensprz from DEFDOKUMMAG where magazyn = :magazyn and typ = :typdok into :dokcensprz;
  if(:dokcensprz is null) then dokcensprz = 0;
  if(:dokcensprz = 0) then copycen = 0;/*wymuszenie nadawania cen z automatu*/
  if(:dokcensprz is null) then dokcensprz = 0;
  if(:dokopk > 0) then dokcensprz = 1;
  if(:fakkosztdost = 0) then begin
  if(:faktura = 0 or (:faktura is null))then begin
    execute procedure GEN_REF('NAGFAK') returning_values :faktura;
    if(:klient > 0) then begin
      select KLIENCI.sposplat, klienci.termin, klienci.upust, klienci.waluta, klienci.sprzedawca from KLIENCI where REF=:klient
       into :sposzap2, :termin, :rabnag,:waluta2,:sprzedawca;
    end else if(:dostawca > 0) then begin
      select dostawcy.sposplat, dostawcy.dniplat, dostawcy.rabat, dostawcy.waluta from DOSTAWCY where REF=:dostawca
       into :sposzap2, :termin, :rabnag,:waluta2;
    end else
      exception NAGFAK_DOKFROMMAG_BK;
    if(:sposzap is null or (:sposzap = 0)) then sposzap = sposzap2;
    /*jesli dok. mag. zwiazany z zamowieniem, to skopiowanie warunkow z zamowienia*/
    if(:zam > 0) then
      select SPRZEDAWCA, SPRZEDAWCAZEW, KATEGA, KATEGB, KATEGC,
            NAGZAM.sposzap, nagzam.termzap, nagzam.rabat
      from NAGZAM where REF=:zam
      into :sprzedawca,:sprzedawcazew, :katega, :kategb, :kategc,
        :sposzap, :termin, :rabnag;
    if(:rabnag is null) then rabnag = 0;
    if(:walutowe=0) then begin
      waluta = :waluta2;
      if(:waluta is null or (:waluta = ''))then waluta = :walpln;
      if(:waluta <> :walpln) then walutowe = 1;
    end
    if(:sposzap is null or (:sposzap = 0))then
      execute procedure GETCONFIG('SPOSZAP') returning_values :sposzap;
    if((:termin is null) or (:termin = 0)) then
      select PLATNOSCI.termin from PLATNOSCI where ref=:sposzap into :termin;
    if(:termin is null) then termin = 0;
    if(:data is null) then data = current_date;
    datazap = :data + :termin;
    /*okreslenie okresu n apodstawie daty*/
    lokres = cast(extract(year from :data) as varchar(4));
    miesiac = cast(extract(month from :data) as integer);
    if(:miesiac < 10) then lokres = :lokres || '0';
    lokres = :lokres || cast(:miesiac as varchar(2));
    if(:sad=1) then dostawca = NULL;
    select min(LISTYWYSD.ref)
    from LISTYWYSDPOZ join LISTYWYSD on (LISTYWYSDPOZ.dokument = LISTYWYSD.ref)
    where LISTYWYSD.akcept = 1 and LISTYWYSDPOZ.DOKTYP = 'M' and LISTYWYSDPOZ.DOKREF=:dokmag
    into :dokumsped;
    if(:dokumsped > 0) then begin
      select sposdost, kosztdostplat, termdost, adres, miasto, kodp, odbiorcaid
      from LISTYWYSD where ref=:dokumsped
      into :ssposdost, :skosztdostplat, :stermdost, :sdulica, :sdmiasto, :sdkodp, :sodbiorcaid;
    end
    if (:data <> current_date) then
      execute procedure datatookres(current_date,0,0,0,0) returning_values(:lokres,:datazm,:fday,:lday);
    insert into NAGFAK(REF, STANOWISKO,REJESTR,TYP,DATA, DATASPRZ, KLIENT,DOSTAWCA,SPOSZAP,TERMZAP,TERMIN,
              SUMWARTNET, SUMWARTBRU, PSUMWARTNET, PSUMWARTBRU,
              RABAT,AKCEPTACJA, /*REFK,*/ SYMBOLK, REFDOKM, ZAPLACONO,
              WALUTA, WALUTOWA, KURS,
              SPRZEDAWCA, SPRZEDAWCAZEW, OPERATOR, KOSZTDOST, KWOTAZAL, SKAD,
              KATEGA, KATEGB, KATEGC, MAGAZYN, LISTYWYSD,
              SPOSDOST, KOSZTDOSTPLAT, KOSZTDOSTROZ, TERMDOST, DULICA, DMIASTO, DKODP, ODBIORCAID, GRUPASPEDPRO)
       values(:faktura,:stan,:rejfak,:typfak, current_date, :data,:KLIENT,:DOSTAWCA,:SPOSZAP,:datazap,:termin,
              0,0,0,0,
              :rabnag,0,/*NULL,*/'',NULL,0,
              :WALUTA, :WALUTOWE, :kurs,
              :SPRZEDAWCA, :sprzedawcazew, :operator, :KOSZTDOST,:sumzal, 2,
              :katega, :kategb, :kategc, :magazyn, :dokumsped,
              :ssposdost,:skosztdostplat, 3, :stermdost, :sdulica, :sdmiasto, :sdkodp, :sodbiorcaid, :grupasped);
  end else begin
    /*sprawdzenie, czy klient lub dostawca sie zgadzają*/
    select DOSTAWCA, KLIENT, MAGAZYN, WALUTA, KURS from DOKUMNAG where REF=:DOKMAG into :DOSTAWCA, :klient, :magazyn, :waluta, :kurs;
    select ZAKUP, KLIENT, DOSTAWCA, RABAT, WALUTOWA, WALUTA, KURS, SAD, ODDZIAL, SKAD from NAGFAK where REF=:faktura into :zakup, :isklient, :isdostawca, :rabnag, :walutowe, :iswaluta, :iskurs, :sad, :oddzial, :skad;
    if (:skad<>2) then exception universal 'Można wskazać tylko fakturę wygenerowaną z dok. mag.'; --BS47151
    if(:platnik is not null and :platnik<>0) then begin
      select count(*) from platnicy where klient=:klient and platnik=:platnik into :cnt;
      if(:cnt>0) then klient = :platnik;
    end

    if(:zakup = 1 and :sad<>1 and ((:dostawca <> :isdostawca) or (:dostawca is null) or (:isdostawca is null))) then
      exception NAGFAK_DOKFORMMAG_DWRONG;
    else if(:zakup = 0 and :sad<>1 and ((:klient <> :isklient) or (:klient is null) or (:isklient is null))) then
      exception NAGFAK_DOKFROMMAG_KWRONG;
    else if(:zakup = 1 and :sad = 1 and ((:waluta <> :iswaluta) or (:kurs <> :iskurs))) then
      exception NAGFAK_DOKFROMMAG_WALUTA;
    if((:kosztdost > 0) or (:sumzal > 0)) then
      update NAGFAK set KOSZTDOST = KOSZTDOST + :kosztdost where ref=:faktura;
  end
  end
  if(:rabnag is null) then rabnag = 0;
  select bn,sad from NAGFAK where nagfak.ref = :faktura into :bn, :sad;
  if(:klient > 0) then
      select grupykli.cennik from klienci join grupykli on(klienci.grupa = grupykli.ref) where KLIENCI.REF=:klient
        into :cennik;
  cenacen = null;
  walcen = null;
  if(:fakkosztdost = 1) then begin
    select KOSZTDOSTGR, KOSZTDOSTBEZ, SPOSDOST, KOSZTDOSTROZ, GRUPASPED
    from DOKUMNAG where ref=:dokmag
    into :kosztdost, :kosztdostbez, :ssposdost, :usltryb, :grupasped;

    if(:kosztdostbez is null) then kosztdostbez = 0;
    if(:usltryb is null) then usltryb = 0;
    faktura = null;
    if(:kosztdost > 0 and :kosztdostbez = 0)then begin
      if(:usltryb = 1 or (:usltryb = 2)) then begin
        /*szukanie nagóka pasującego do zadanych parametrów*/
        select max(ref) from NAGFAK where GRUPASPEDPRO=:grupasped and AKCEPTACJA = 0 and NAGFAK.typ=:typfak into :faktura;
        if(:faktura is null and :usltryb = 2) then exception NAGFAK_FAKTURUJDOKMAG_PRO_EXPT;
      end
      if(:faktura is null) then begin
        if(:sposzap is null) then
          select max(nagfak.sposzap) from NAGFAK where grupaspedpro = :grupasped into :sposzap;
        if(:sposzap is null) then
          select klienci.sposplat from KLIENCI where REF=:KLIENT into :sposzap;
        execute procedure GEN_REF('NAGFAK') returning_values :faktura;
        insert into NAGFAK(REF,STANOWISKO, REJESTR, TYP,DATA,KLIENT,sposzap, sposdost, grupaspedpro, SKAD)
        select :faktura,:stan, :rejfak, :typfak, current_date,DOKUMNAG.kosztdostplat, :sposzap, :ssposdost, :grupasped, 2
        from DOKUMNAg left join KLIENCI on (dokumnag.kosztdostplat = klienci.ref)
        where dokumnag.ref=:dokMAG;
      end
      if (:USLTRYB = 0 or (:USLTRYB = 1) ) then
      begin
        select SPOSDOST.usluga from SPOSDOST where  ref=:ssposdost into :ktm;
        if(:ktm = '' or (:ktm is null)) then exception NAGFAK_FAKTURUJDOKMAG_PRO_EXPT 'Brak okrelonej usugi dla zadanego sposobu dostawy';
        insert into POZFAK(DOKUMENT, KTM, WERSJA, ILOSC, CENACEN, OPK)
        values(:faktura, :ktm, 0, 1, :kosztdost, 0);
      end
      if (:USLTRYB = 2) then
      begin
        select max(POZFAK.wartbru) from POZFAK
          join NAGFAK on (POZFAK.dokument = NAGFAK.REF)
          where NAGFAK.grupaspedpro = :GRUPASPED into :maxwartbru;
        select max(POZFAK.REF) from POZFAK
          join NAGFAK on (POZFAK.dokument = NAGFAK.REF)
          where NAGFAK.grupaspedpro = :GRUPASPED into :POZFAKREF;
        if(:pozfakref is null) then exception NAGFAK_FAKTURUJDOKMAG_PRO_EXPT 'Brak pozycji towarowej do rozliczenia kosztów uslugi trnsportowej';
        select ILOSC from POZFAK where REF=:pozfakref into :ilosc;
        if(:ilosc > 0) then
          update POZFAK set CENACEN  = CENACEN + cast(:kosztdost/:ilosc as numeric(14,2)) where REF=:pozfakref;
      end
    end
     else faktura = -1;
  end else
  for select DOKUMPOZ.REF,KTM, WERSJA, WERSJAREF,JEDNO,ILOSCO,ILOSC,CENA, CENACEN,walcen, DOKUMPOZ.RABAT, RABATTAB, OPK, CENAWAL, FOC, OBLICZCENZAK, OBLICZCENKOSZT, OBLICZCENFAB, PREC
  from DOKUMPOZ
    join DOKUMNAG on (DOKUMNAG.ref = DOKUMPOZ.dokument) where ILOSC > 0
    and (DOKUMNAG.REF =:dokmag and :FAKGRUPASPED = 0 or :FAKGRUPASPED is null)
    or (DOKUMNAG.grupasped = :GRUPASPED and :FAKGRUPASPED = 1)
  into :dokpoz, :ktm, :wersja, :wersjaref, :jedno, :ilosco,:iloscm,:cenamag,:cenacen, :walcen, :rabat, :rabattab, :opk, :cenawal, :foc, OBLCENZAK, OBLCENKOSZT, OBLCENFAB, :prec
  do begin
    /*obliczenie rabatu do pozycji*/
    ilosc = null;
    jedn = NULL;
    przeliczo = null;
    przeliczc = null;
    if(:copycen = 0) then begin
      WALCEN = NULL;
      cenacen = null;
    end
    if((:jedno is null) or (:ilosco is null))then begin
      ilosco = :iloscm;
    end
    /*zmniejszenie ilosci o korekty*/
    if(:domagkor = 1) then begin
      iloscmagmin = 0;
      select sum(POZKOR.ILOSC)
      from DOKUMPOZ POZKOR
      join DOKUMPOZ POZDOK on (POZKOR.KORTOPOZ = POZDOK.REF)
      join DOKUMNAG on (POZKOR.DOKUMENT = DOKUMNAG.REF and DOKUMNAG.FAKTURA is null and DOKUMNAG.AKCEPT > 0)
      where POZDOK.REF = :dokpoz
      into :iloscmagmin;
      if(:iloscmagmin > 0) then begin
        iloscm = :iloscm - :iloscmagmin;
        if(:przeliczo is null and jedno is not null) then
          select PRZELICZ from TOWJEDN where ref = :jedno into :przeliczo;
        else
          przeliczo = 1;
        ilosco = :iloscm / :przeliczo;
      end
    end
    /*zmniejszenie ilosci o rozliczenia*/
    if(:domagopkroz = 1) then begin
      iloscmagmin = 0;
      select sum(ROZKOR.ILOSC)
      from DOKUMROZ ROZKOR
      join STANYOPK on (STANYOPK.ref = ROZKOR.opkrozid)
      join DOKUMPOZ on (DOKUMPOZ.REF = STANYOPK.POZDOKUM )
      join DOKUMNAG on (DOKUMNAG.REF = DOKUMPOZ.DOKUMENT and DOKUMNAG.AKCEPT > 0)
      where DOKUMPOZ.REF = :dokpoz
      into :iloscmagmin;
      if(:iloscmagmin > 0) then begin
        iloscm = :iloscm - :iloscmagmin;
        if(:przeliczo is null and jedno is not null) then
          select PRZELICZ from TOWJEDN where ref = :jedno into :przeliczo;
        else
          przeliczo = 1;
        ilosco = :iloscm / :przeliczo;
      end
    end
      /*przyjcie jednostki magazynowej - na razie*/
    select ref from TOWJEDN where KTM = :ktm and towjedn.glowna = 1 into :jednm;
    if(:walutowe = 1) then begin
      cenacen = :cenawal;
      ilosc = :iloscm;
      jedn = :jednm;
    end else if(:copycen > 0 and ((:cenacen is not null and :dokopk > 0) or (:cenacen > 0)))then begin
      /*kopiowanie ceny z dokumentu magazynowego*/
      if(:bn = 'B' and :dokbn = 'N') then begin
        select vat.stawka from VAT join TOWARY on (TOWARY.VAT = VAT.GRUPA) where TOWARY.ktm = :ktm into :vat;
        if(:vat is null) then vat = 0;
        cenacen = :cenacen * (100 + :vat)/100;
      end else if(:bn = 'N' and :dokbn = 'B')then begin
        select vat.stawka from VAT join TOWARY on (TOWARY.VAT = VAT.GRUPA) where TOWARY.ktm = :ktm into :vat;
        if(:vat is null) then vat = 0;
        cenacen = :cenacen / ((100 + :vat)/100);
      end
      ilosc = :iloscm;
      jedn = :jednm;
    end else if(:klient > 0) then begin
      /*pobranie ceny z cennika dla danego klienta*/
      select ref, przelicz from TOWJEDN where KTM = :ktm and towjedn.c = 2 into :jedn, :przeliczc;
      if(:przeliczc is null) then przeliczc = 1;
      if(:jedn = :jednm) then
        ilosc = :iloscm;
      else if(:jedn = :jedno) then
        ilosc = :ilosco;
      else
        ilosc = :iloscm / :przeliczc;
      if(:bn = 'B') then
        select  cenabru, waluta from cennik where cennik = :cennik and ktm = :ktm and wersja = :wersja and jedn = :jedn into :cenacen, :walcen;
      else
        select  cenanet, waluta from cennik where cennik = :cennik and ktm = :ktm and wersja = :wersja and jedn = :jedn into :cenacen, :walcen;
      if(exists(select ref from dokumpoz where refdopakiet=:dokpoz)) then
        zpakietem = 1;
      else
        zpakietem = 0;
      execute procedure OBLICZ_RABAT(:ktm, :wersja, :klient,:iloscm,:cenacen,:bn, '', :cennik, 0, :zpakietem) returning_values :rabat,:iscena, :walrabcen, :promocja;
      if(:iscena = 1) then begin
        cenacen = :rabat;
        walcen = :walrabcen;
      end
      rabattab = :rabat;
    end else if(:dostawca > 0 and :walutowe<>1) then begin
      select jedn, rabat, waluta, cenanet
      from GET_DOSTCEN(:wersjaref,:dostawca,:walcen,:oddzial,0)
      into :jedn,:rabat,:walcen, :cenacen;
      if(:walcen is null or (:walcen = '')) then walcen = :walpln;
      if(:jedn is null or (:jedn = 0))then begin
        jedn = :jednm;
        ilosc = :iloscm;
      end
      if(:walcen = :walpln) then begin
        if(:jedn = :jednm) then begin
          cenacen = :cenamag;
          ilosc = :iloscm;
        end else begin
          select PRZELICZ from TOWJEDN where REF=:jedn into :przeliczc;
          if(:przeliczc is null) then przeliczc = 1;
          ilosc = :iloscm /:przeliczc;
          cenacen = :cenacen * :przeliczc;
        end
      end else begin
        /*cena zakupu walutowa - niech to uzytkownik wpisze recznie*/
        cenacen = 0;
      end
      /*dostcen sa w cenach netto zawsze */
      if(:bn = 'B') then begin
        select vat.stawka from VAT join TOWARY on (TOWARY.VAT = VAT.GRUPA) where TOWARY.ktm = :ktm into :vat;
        if(:vat is null) then vat = 0;
        cenacen = :cenacen * (100 + :vat)/100;
      end
    end else begin
      ilosc = :iloscm;
      jedn = :jednm;
      cenacen = null;
    end
    if(:cenacen is null) then cenacen = 0;
    if(:opk > 0) then opk = 1;
    if(:ilosc > 0) then begin
       execute procedure GEN_REF('POZFAK') returning_values :pozfakref;
       insert into pozfak(REF, DOKUMENT,KTM,WERSJA,ILOSC,CENACEN,RABAT,RABATTAB,WALCEN,
                       ILOSCO,ILOSCM,JEDN,JEDNO,MAGAZYN,
                       PILOSC,PCENACEN,PRABAT,PCENANET,PCENABRU,CENAMAG,PCENAMAG,
                       PWARTNET,PWARTBRU,OPK, FOC, obliczcenzak, obliczcenkoszt, obliczcenfab, prec)
       values(:pozfakref, :faktura,:ktm,:wersja,:ilosc,:cenacen,:rabat - :rabnag,:rabattab,:walcen,
              :ILOSCO, :ILOSCM,:jedn, :jedno,:magazyn,
              0,0,0,0,0,:cenamag,:cenamag,
              0,0,:opk,:foc, :oblcenzak, :oblcenkoszt, :oblcenfab, :prec);
    end
    cenacen = null;
    walcen = null;
  end
  newfak = :faktura;
end^
SET TERM ; ^
