--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EHRM_GET_OVERTIME(
      EMPLOYEE EMPLOYEES_ID,
      TODATE TIMESTAMP_ID)
  returns (
      CURRENTOVERTIME integer)
   as
declare variable WORKEDTIME integer;
declare variable NORMTIME integer;
declare variable FROMDATE TIMESTAMP_ID;
declare variable OLDOVERTIME integer;
declare variable WORKSECS integer;
begin
  --Ustawienie parametu fromdate
  Select CAST(k.wartosc as timestamp) from KONFIG k
  where k.akronim = 'RCP_START'
  into :fromdate;

  -- pobranie ilosci przepracownych godzin
  Select
    coalesce(SUM(lp.JOBTIME),0) as overtime
  from epresencelistspos lp
  inner join epresencelists l on l.ref = lp.epresencelists
  where
    lp.employee = :employee and
    lp.jobtime is not null and
    l.listsdate < :todate and
    l.listsdate >= :fromdate
  into :workedtime;

  -- pobranie ilosci godzin ktore nalezalo przepracowac
  if(todate <= fromdate) then
    normtime = 0;
  else
     select WS from ECAL_WORK(:employee,:fromdate,:todate)
        into :normtime;

  -- pobieranie nieobecnosci
  select coalesce(sum(e.worksecs), 0)
    from eabsences e
    where employee = :employee and fromdate < :todate and fromdate >= :fromdate
    into :worksecs;

  normtime = normtime - worksecs;

  --pobieramy zalegle nadgodziny
  select e.x_rcp_overtime from employees e
  where e.ref = :employee
  into :oldovertime;

  --jesli sa zalegle nadgodziny to dodajemy je do przepracownaego czasu
  if(oldovertime is not null) then
    workedtime = workedtime + oldovertime;

  currentovertime = (workedtime - normtime);
  suspend;
end^
SET TERM ; ^
