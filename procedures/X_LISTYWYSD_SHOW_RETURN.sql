--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_LISTYWYSD_SHOW_RETURN(
      SHIPPINGDOC LISTYWYSD_ID)
  returns (
      SHOWRETURN SMALLINT_ID)
   as
declare variable docgroup dokumnag_id;
declare variable ismwsordcortoreal smallint_id;
begin
  showreturn = 0;

  select dn.grupasped
    from listywysd ld
      left join dokumnag dn on (dn.ref = ld.refdok)
    where ld.ref = :shippingdoc
  into :docgroup;

  select first 1 1
    from dokumnag d
      join dokumnag dk on (d.ref = dk.refk) --powiazanie dokumentow korygujacych
      join mwsords o on (dk.ref = o.docid and o.mwsordtype = 25 and o.status < 5)
    where d.grupasped = :docgroup
  into :ismwsordcortoreal;

  if (coalesce(:ismwsordcortoreal,0) > 0) then
    showreturn = 1;

  suspend;
end^
SET TERM ; ^
