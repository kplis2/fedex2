--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NUMBERBLOCK_DOKNAGFAK_SET(
      NAGFAKREF integer,
      BLOCKNUMBERREF integer)
   as
begin
  if(NAGFAKREF is not null and blocknumberref is not null) then
  begin
    update NAGFAK set numblock=:BLOCKNUMBERREF where ref=:NAGFAKREF;
    update NUMBERBLOCK set doknagfak=:NAGFAKREF where ref=:blocknumberref;
  end
  else
    exception universal 'Nie podano poprawnych parametrów wejciowych';
end^
SET TERM ; ^
