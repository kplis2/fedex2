--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_STAN_MAG(
      FAKTURA integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer)
  returns (
      CENA numeric(14,4),
      DOSTAWA integer,
      ILOSC numeric(14,4))
   as
declare variable grupa integer;
declare variable ilroz integer;
begin
  select GRUPADOK from NAGFAK where REF=:FAKTURA into :grupa;
  if(:grupa is null) then exit;
  for select sum(DOKUMROZ.ilosc),DOKUMROZ.CENA,DOKUMROZ.DOSTAWA
  from DOKUMROZ join DOKUMPOZ on ( DOKUMPOZ.REF = DOKUMROZ.POZYCJA AND DOKUMPOZ.KTM = :KTM AND DOKUMPOZ.wersja = :wersja)
                join DOKUMNAG on ( DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
                join DEFDOKUM on ( DEFDOKUM.SYMBOL = DOKUMNAG.TYP and DEFDOKUM.WYDANIA = 1)
                join NAGFAK on ( DOKUMNAG.faktura = NAGFAK.REF and NAGFAK.grupadok = :grupa)
     group by DOKUMROZ.cena, DOKUMROZ.dostawa
     into :ilosc,:cena,:dostawa
  do begin
    ilroz = 0;
    select sum(DOKUMROZ.ilosc)
      from DOKUMROZ join DOKUMPOZ on ( DOKUMPOZ.REF = DOKUMROZ.POZYCJA AND DOKUMPOZ.KTM = :KTM AND DOKUMPOZ.wersja = :wersja and (DOKUMROZ.CENA = :cena or (:cena is null)) and (DOKUMROZ.DOSTAWA = :dostawa or (:dostawa is null)))
                join DOKUMNAG on ( DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
                join DEFDOKUM on ( DEFDOKUM.SYMBOL = DOKUMNAG.TYP and DEFDOKUM.WYDANIA = 0)
                join NAGFAK on ( DOKUMNAG.faktura = NAGFAK.REF and NAGFAK.grupadok = :grupa)
     into :ilroz;
    if(:ilroz is null) then ilroz = 0;
    if(:ilosc is null) then ilosc = 0;
    ilosc = :ilosc - :ilroz;
    if(:ilosc > 0) then suspend;
  end
end^
SET TERM ; ^
