--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_DZIENNIK_TXT(
      COMPANY integer,
      ODOKRESU varchar(6) CHARACTER SET UTF8                           ,
      DOOKRESU varchar(6) CHARACTER SET UTF8                           ,
      REJESTR varchar(10) CHARACTER SET UTF8                           ,
      ODLINII integer,
      ILELINII integer)
  returns (
      BKREG varchar(10) CHARACTER SET UTF8                           ,
      NLNUMBER integer,
      NUMBER integer,
      TRANSDATE timestamp,
      DOCDATE timestamp,
      REF integer,
      NAZWA varchar(60) CHARACTER SET UTF8                           ,
      NAME varchar(60) CHARACTER SET UTF8                           ,
      ODDZIAL varchar(10) CHARACTER SET UTF8                           ,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      SUMDEBIT numeric(14,2),
      SUMCREDIT numeric(14,2),
      SUMDEBITS numeric(14,2),
      SUMCREDITS numeric(14,2),
      SUMDEBITC numeric(14,2),
      SUMCREDITC numeric(14,2),
      ZPRZEN_DEBIT numeric(14,2),
      ZPRZEN_CREDIT numeric(14,2),
      NAWASTRONA smallint,
      DESCRIPT varchar(80) CHARACTER SET UTF8                           ,
      DEBIT numeric(14,2),
      CREDIT numeric(14,2),
      DNUMBER smallint,
      ACCOUNT ACCOUNT_ID,
      PODSUMSTR smallint,
      PODSUMDEK smallint,
      STRONA integer,
      NRLINII integer)
   as
declare variable lini smallint;
declare variable lmax smallint;
declare variable nlnumbertmp integer;
declare variable numbertmp integer;
declare variable dnumbertmp smallint;
declare variable sql varchar(6000);
declare variable sqlparams varchar(6000);
declare variable prejestr varchar(200);
begin
  nawastrona = 0;
  strona = 1;
  ref = null;
  podsumdek=0;
  podsumstr = 0;
  sumdebits = 0;
  sumcredits = 0;
  sumdebitc = 0;
  sumcreditc = 0;
  zprzen_debit = 0;
  zprzen_credit = 0;
  nrlinii = 0;
  sqlparams = '';
  odlinii =:odlinii -1;
  prejestr = '';

  if (coalesce(:ilelinii,0) > 0 ) then begin
    sqlparams = sqlparams||' first('||:ilelinii||')';
  end
  if (coalesce(:odlinii,0) > 0 ) then begin
    sqlparams = sqlparams||' skip('||:odlinii||')';
    --nrlinii = odlinii;
    end
  if (coalesce(rejestr,'') <> '') then
    prejestr = '  and b.bkreg = '''||:rejestr||'''';


    sql = 'select '||:sqlparams||' b.bkreg, b.nlnumber, b.number, b.transdate, b.docdate, o.nazwa,';
    sql = sql||' b.branch, b.sumdebit, b.sumcredit, b.ref, t.name, b.symbol';
    sql = sql||' from bkdocs b';
    sql = sql||' join bkdoctypes t on (t.ref = b.doctype)';
    sql = sql||' left join operator o on (o.ref = b.bookoper)';
    sql = sql||' where b.status = 3';
    sql = sql||' and b.company = '||:company;
    sql = sql||' and b.period >= '''||:odokresu||'''';
    sql = sql||'and b.period <='''||:dookresu||''''||:prejestr;
    sql = sql||' order by B.bkreg, B.nlnumber';

  for EXECUTE STATEMENT :sql
       into :bkreg, :nlnumber, :number, :transdate, :docdate, :nazwa,
        :oddzial, :sumdebit, :sumcredit, :ref, :name, :symbol
  do begin
    dnumber = 1;
    nrlinii = :nrlinii + 1;
    for
      select d.descript, d.debit, d.credit, d.account
        from decrees d
          join bkaccounts b on (d.accref = b.ref)
        where d.bkdoc = :ref and b.bktype <> 2 and b.bktype <> 3
        into :descript, :debit, :credit, :account
    do begin
      sumdebits = sumdebits + debit;
      sumcredits = sumcredits + credit;
      suspend;
      dnumber = dnumber + 1;
    end
    nazwa = '';
    name = '';
    oddzial = '';
    symbol = '';
    descript = '';
    bkreg = '';
    account = 'Razem:';
    debit = 0;
    credit = 0;
    number = null;
    nlnumber = null;
    transdate = null;
    podsumdek = 1;
    nlnumber = null;
    number = null;
    dnumber = null;
    suspend;
    podsumdek = 0;
  end
end^
SET TERM ; ^
