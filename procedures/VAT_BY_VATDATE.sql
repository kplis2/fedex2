--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE VAT_BY_VATDATE(
      OTABLE varchar(30) CHARACTER SET UTF8                           ,
      OREF integer,
      DATADOK timestamp,
      TERMPLAT timestamp)
  returns (
      VATDATE timestamp)
   as
declare variable NAGKOREKTA smallint;
declare variable DATAOTRZKOR timestamp;
declare variable DATAOTRZ timestamp;
declare variable ZAKUP integer;
begin
/*PL: Procedura zwraca date vat jako:
      date dokumentu dla faktur sprzedazy nie bedacych korektami,
      date otrzymania dla wszystkich korekt i faktur zakupu, 
      null jezeli nie udalo sie ustalic daty vat(np. puste pole dataotrzkor)
      (BS65846)
*/
   vatdate = :datadok;
   if(OTABLE = 'NAGFAK') then
   begin
    select t.korekta, n.dataotrzkor, n.dataotrz, t.zakup
      from typfak t
      join nagfak n on (n.typ = t.symbol and n.ref = :oref)
    into :nagkorekta, :dataotrzkor, :dataotrz, :zakup;
    if (nagkorekta = 1) then begin
      if (dataotrzkor is not null) then
        vatdate = :dataotrzkor;
      else
        vatdate = null;
      end
    else begin
    if (:zakup = 0) then
      vatdate = :datadok;
    else
      vatdate = :dataotrz;
    end
   end
  suspend;
end^
SET TERM ; ^
