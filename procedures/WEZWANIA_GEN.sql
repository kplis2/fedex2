--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WEZWANIA_GEN(
      DATA date,
      NOTATYP varchar(5) CHARACTER SET UTF8                           ,
      LASTBEFORE timestamp,
      MOREDAYSTHAN integer,
      FORACCOUNT ACCOUNT_ID,
      FORSLODEF integer,
      FORSLOPOZ integer,
      FORFAKTORING smallint,
      COMPANY integer,
      REFKURSU integer)
  returns (
      STATUS integer,
      GENNUMBER integer)
   as
declare variable slodef integer;
declare variable slopoz integer;
declare variable slodefref integer;
declare variable kontofk KONTO_ID;
declare variable symbfak varchar(20);
declare variable dataotw timestamp;
declare variable dataplat timestamp;
declare variable lastdat timestamp;
declare variable dorecord smallint;
declare variable maxst smallint;
declare variable ntyp varchar(5);
declare variable refnota integer;
declare variable datazm timestamp;
declare variable saldown numeric(14,2);
declare variable kwotafak numeric(14,4);
declare variable kwotazap numeric(14,4);
declare variable wn numeric(14,2);
declare variable ma numeric(14,2);
declare variable dt timestamp;
declare variable haspoz smallint;
declare variable maxdays integer;
declare variable oldmaxst integer;
declare variable interest numeric(14,2);
declare variable degree smallint;
declare variable faktoring smallint;
declare variable dni integer;
declare variable tabela integer;
declare variable sql varchar(2048);
declare variable vcurrency varchar(3);
declare variable currency varchar(3);
declare variable summarozrach numeric(14,4);  /* saldo ma dla rozrachunku */
declare variable nkurs numeric(14,4);
declare variable wnzl numeric(14,2);
declare variable mazl numeric(14,2);
begin
  status = 0;
  gennumber = 0;
  if (notatyp <> '') then
    degree = 0;
  else
    degree = 1;
  if (forfaktoring <> 0 and forfaktoring <> 1) then
    forfaktoring = NULL;
  if (lastbefore is null) then
   lastbefore = data;
  if (lastbefore > data) then
    lastbefore = data;

  sql = ' select RP.slodef, RP.slopoz from rozrachp RP';
  sql = sql ||' join rozrach R on (R.kontofk = RP.kontofk and R.symbfak = RP.symbfak and R.slodef = RP.slodef and R.slopoz = RP.slopoz)';
  sql = sql ||' left join nagfak NF on (NF.ref = R.faktura)';
  sql = sql ||' where R.company ='||:company||' and RP.data < cast('''||:data||''' as date)';
  sql = sql ||' and ((datazamk is null) or (datazamk >= cast('''||:data||''' as date)+1))';
  sql = sql ||' and R.dataplat < cast('''||:data||''' as date)';
  if(coalesce(forslodef,0)>0)then  sql = sql ||' and RP.slodef ='||:forslodef;
  if(coalesce(forslopoz,0)>0)then  sql = sql ||' and RP.slopoz ='||:forslopoz;
  if(coalesce(foraccount,'')<>'')then  sql = sql ||' and RP.kontofk like '''||:foraccount||'''';
  if(forfaktoring is not null)then  sql = sql ||' (NF.faktoring = '||:forfaktoring||' or NF.faktoring is null)';
  sql = sql ||' group by RP.slodef, RP.slopoz, RP.symbfak, RP.kontofk having sum(RP.winienzl) > sum(RP.mazl) ';
  for execute statement :sql
  into :slodef, :slopoz
  do begin
    /*sprawdzenie, czy ostatnie wezwanie nie bylo zbyt wczesnie*/
    tabela = null;
    select min(ref) from slodef where typ = 'KLIENCI'
      into :slodefref;

    if (slodef = :slodefref) then
      select interesttables
        from klienci
          where ref = :slopoz
      into :tabela;

    if (tabela is null) then
      select ref
        from interesttables
        where isdefault = 1
      into :tabela;

    if (:tabela is null) then
      exception test_break 'Nie wybrana domyślna tabela odsetkowa';

    lastdat = null;
    dorecord = 1;
    select max(data) from notynag where slopoz = :slopoz and slodef = :slodef and notakind = 0
      into :lastdat;
    if (lastdat is not null) then
      if (cast(lastdat as date) >= cast(lastbefore as date)) then
        dorecord = 0;

    if (dorecord = 1) then
    begin

      /*są rozrachunki przeterminowane, bdzei generowanie - ustalenie typu, jesli niesutalony i stopniowanie*/
      ntyp = null;
      if (degree = 1) then
      begin
        maxst = null;
        /*dla rozrachunków okrelenie makymalnego stopnia, jaki już bygenerowany*/
        select max(NT.degree)
          from rozrach R
            join notynag NN on (NN.slodef = R.slodef and NN.slopoz = R.slopoz )
            join notypoz NP on (NP.dokument = NN.ref and NP.settlement = R.symbfak and NP.account = R.kontofk)
            join notytyp NT on (NN.notatyp = NT.typ)
            left join nagfak NF on (NF.ref = R.faktura)
          where R.slodef = :slodef and R.slopoz = :slopoz and ((R.datazamk is null) or (R.datazamk >= :data+1))
            and NN.notakind = 0
            and ((NF.faktoring = :forfaktoring) or (NF.faktoring is null) or (:forfaktoring is null))
            and ((:foraccount is null) or (:foraccount = '') or (R.kontofk like :foraccount))  -- BS95923
            and R.company = :company
          into :maxst;

        oldmaxst = :maxst;
        if (oldmaxst is null) then
        begin
          select min(degree) from notytyp where kind = 0
            into :oldmaxst;
        end else
        begin
          select min(degree) from notytyp where degree > :oldmaxst and kind = 0
            into :maxst;
        end
        if (:maxst is null) then maxst = :oldmaxst;/*brak*/
        select min(typ) from notytyp where degree = :maxst and kind = 0
          into :ntyp;
      end
      else begin
        ntyp = notatyp;
      end

      if (ntyp is null) then exception WEZWANIA_GEN_BEZWEZWGEN;

      ---tutaj zaczynamy forselecta
      for
        select R.waluta
          from rozrach R
            left join nagfak N on (N.ref = R.faktura)
        where R.slodef = :slodef and R.slopoz = :slopoz
           and dataotw  < :data and ((datazamk is null) or (datazamk >= :data+1))
           and dataplat < :data
           and ((N.faktoring = :forfaktoring) or (N.faktoring is null) or (:forfaktoring is null))
           and ((:foraccount is null) or (:foraccount = '') or (R.kontofk like :foraccount))  -- BS95923
           and R.company = :company
         group by R.waluta
        into vcurrency
      do begin
        /*naglówek dokumentu*/
        if (vcurrency <> 'PLN' and :refkursu = 0) then
          exception TABKURSNULL;

        execute procedure GEN_REF('NOTYNAG')
          returning_values :refnota;

        select tp.sredni from tabkursp tp
          where tp.waluta = :vcurrency and tp.tabkurs = :refkursu
          into :nkurs;

        if (:nkurs is null ) then
          nkurs = 1;

        insert into notynag(ref, notakind, notatyp, slodef, slopoz, data, status, company, currency,rateavg)
          values(:refnota, 0, :ntyp, :slodef, :slopoz, :data, 0, :company, :vcurrency,:nkurs);

        for
          select R.symbfak, R.kontofk, R.dataotw, R.dataplat, N.faktoring, R.waluta
            from rozrach R
              left join nagfak N on (N.ref = R.faktura)
          where R.slodef = :slodef and R.slopoz = :slopoz
             and dataotw  < :data and ((datazamk is null) or (datazamk >= :data+1))
             and dataplat < :data
             and ((N.faktoring = :forfaktoring) or (N.faktoring is null) or (:forfaktoring is null))
             and R.company = :company
             and R.waluta = :vcurrency
          into :symbfak, :kontofk, :dataotw, :dataplat, :faktoring, :currency
        do begin
          /*analiza pojedynczego rozrachunku*/
          kwotafak = 0;
          kwotazap = 0;
          saldown = 0;
          datazm = :dataplat;
          haspoz = 0;
        
          select sum(winien), sum(ma)
            from rozrachp
            where slodef =: slodef and slopoz =: slopoz and symbfak =: symbfak
              and kontofk =: kontofk and company = :company
            into :kwotafak , :summarozrach;

          /* jeżeli powstala nadplata dla rozrachunku a nie zostal skompensowany to jest pomijany */
          if(:kwotafak>=:summarozrach) then begin
            for
              select winien, ma, coalesce(interestdate,stransdate), winienzl, mazl
              from rozrachp
              where slodef = :slodef
                and slopoz = :slopoz
                and symbfak = :symbfak
                and kontofk = :kontofk
                and stransdate <:data + 1
                and company = :company
              order by stransdate, ma
              into :wn, :ma, :dt, :wnzl, :mazl
            do begin
         
              if (wn > 0 and kwotafak = 0) then
              begin
                kwotafak = :wn;
              end else if (ma < 0 and kwotafak = 0) then
              begin
                kwotafak = -ma;
              end
  
  
            /*obliczenie dni i odsetek do dnia zmiany*/
              if (ma > 0) then
                kwotazap = kwotazap + ma;  -- tu zwieksza
              if (ma > 0 and kwotafak > 0) then
              begin
                if (cast(dt as date) >= cast(datazm as date)) then
                  execute procedure interest_count(:kontofk, :symbfak, cast(:dataplat as date), cast(:dt as date), :kwotazap, :tabela, :company)
                    returning_values :interest;
                else
                  interest = 0;
                dni = cast(dt as date) - cast(dataplat as date);
                if (dni < 0) then
                  dni = null;
                insert into notypoz(dokument, account, settlement, datastl, datapay,
                    datapaid, ammount, ammountpaid, ammountleft, daysof, interest, faktoring, currency,
                    AMMOUNTZL, AMMOUNTPAIDZL, interestzl, ammountleftzl)
                  values(:refnota, :kontofk, :symbfak, :dataotw, :dataplat, :dt, :kwotafak,
                    :kwotazap, :saldown, :dni, :interest, :faktoring, :currency,
                    :kwotafak*:nkurs, :kwotazap*:nkurs, :interest * :nkurs,:saldown*:nkurs);
                kwotazap = :kwotazap - :ma; -- tu zmniejsza
                haspoz = 1;
              end
  
  /*            else begin
                insert into notypoz(dokument, account, settlement, datastl, datapay,
                    datapaid, ammount, ammountpaid, ammountleft, daysof, interest, faktoring)
                values(:refnota, :kontofk, :symbfak, :dataotw, :dataplat,
                  :dt, :kwotafak, :kwotazap, :saldown, :dt - :dataplat, 0, :faktoring);
              kwotazap = :kwotazap - :ma; -- jak nie wyzej to nizej bo sie wykrzacza obliczenia ;)
              haspoz = 1;
            end */
              if (ma <> 0) then
                saldown = saldown - ma;
              else
                saldown = :saldown + :wn;
              datazm = :dt;
            end

            if (saldown > 0) then
            begin
              dt = data;
              dni = cast(dt as date) - cast(dataplat as date);
              if (dni < 0) then
                dni = null;
              execute procedure interest_count(:kontofk, :symbfak, cast(:dataplat as date), cast(:dt as date), :saldown, :tabela, :company)
                returning_values :interest;
              insert into notypoz(dokument, account, settlement, datastl, datapay,
                  datapaid, ammount, ammountpaid, ammountleft, daysof, interest, faktoring, currency,
                  bdebitzl, bcreditzl, AMMOUNTZL, AMMOUNTPAIDZL, interestzl, ammountleftzl)
                values(:refnota, :kontofk, :symbfak, :dataotw, :dataplat,
                  null, :kwotafak, 0, :saldown, :dni, :interest, :faktoring, :currency,
                  :wnzl, :mazl, :kwotafak*:nkurs, :kwotazap*:nkurs, :interest * :nkurs,:saldown*:nkurs);
            end
          end

        end

        if (moredaysthan > 0) then
        begin
          select max(daysof) from notypoz where dokument = :refnota
            into :maxdays;
          if (maxdays < moredaysthan) then
          begin
            delete from notynag where ref=:refnota;
            refnota = 0;
          end
        end
        if (refnota > 0) then
        begin
          if (not exists(select ref from notypoz where dokument = :refnota)) then
          begin
            delete from notynag where ref = :refnota;
            refnota = 0;
          end
        end
        if (refnota > 0) then
          gennumber = gennumber + 1;
        nkurs = NULL;
      end
     -- koniec forselecta     to na gorze
    end
  end
  status = 1;
end^
SET TERM ; ^
