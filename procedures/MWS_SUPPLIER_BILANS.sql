--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_SUPPLIER_BILANS(
      SUPLIER integer,
      DOCID integer,
      BDATE date,
      EDATE date,
      DOCTYPE varchar(3) CHARACTER SET UTF8                           ,
      WH varchar(3) CHARACTER SET UTF8                           ,
      PALINCLUDE smallint,
      ONLYMISTAKES smallint,
      GROUPMODE smallint)
  returns (
      SUPLIEROUT varchar(40) CHARACTER SET UTF8                           ,
      DOCIDOUT integer,
      DOCSYMBOL varchar(40) CHARACTER SET UTF8                           ,
      ORDREF integer,
      ORDSYMBOL varchar(40) CHARACTER SET UTF8                           ,
      ORDSTATUS smallint,
      ATR1 varchar(255) CHARACTER SET UTF8                           ,
      ATR2 varchar(255) CHARACTER SET UTF8                           ,
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      QUANTITYONPLUS numeric(14,4),
      QUANTITYONMINUS numeric(14,4),
      BILANS numeric(14,4),
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      POSDESCRIPT varchar(1024) CHARACTER SET UTF8                           ,
      REF integer)
   as
declare variable posdesc varchar(255);
begin
  -- wartosci groupmode
  -- 0 dokument, kategoria, podkategoria, KTM, wersja --- dokument, kategoria, podkategoria, KTM|dokument, kategoria, podkategoria|dokument, kategoria|dokument|kategoria, podkategoria, KTM, wersja|kategoria, podkategoria, KTM|kategoria
  -- 1 dokument, kategoria, podkategoria, KTM
  -- 2 dokument, kategoria, podkategoria
  -- 3 dokument, kategoria
  -- 4 dokument
  -- 5 kategoria, podkategoria, KTM, wersja
  -- 6 kategoria, podkategoria, KTM
  -- 7 kategoria, podkategoria
  -- 8 kategoria
  descript = '';
  posdescript = '';
  ref = 0;
  if (groupmode = 0) then
  begin
    for
      select x.suplierout, x.docidout, x.docsymbol, x.ordsymbol, a1.wartosc, a2.wartosc,
          x.good, x.vers, x.quantityonplus,x.quantityonminus, x.ordref, x.ordstatus,
          x.quantityonplus - x.quantityonminus
        from ANAL_MWS_REC_DOCS(:suplier,:docid,:bdate,:edate,:doctype,:wh,:palinclude,:onlymistakes) x
          left join atrybuty a1 on (x.vers = a1.wersjaref and a1.cecha = 'KATEGORIA')
          left join atrybuty a2 on (x.vers = a2.wersjaref and a2.cecha = 'PODKATEGORIA')
        where quantityonplus <> 0 or quantityonminus <> 0
        into suplierout, docidout, docsymbol, ordsymbol, atr1, atr2,
          good, vers, quantityonplus, quantityonminus, ordref, ordstatus,
          bilans
    do begin
      select substring(uwagi from 1 for 255) from dokumnag where ref = :docidout
        into descript;
      for
        select uwagi from dokumpoz where dokument = :docidout and wersjaref = :vers
          into posdesc
      do begin
        if (posdesc is null) then posdesc = '';
        if (coalesce(char_length(posdescript),0) <= 768 and coalesce(char_length(posdesc),0) > 0) then -- [DG] XXX ZG119346
          posdescript = posdescript||';'||posdesc;
      end
      ref = ref + 1;
      suspend;
    end
  end else if (groupmode = 1) then
  begin
    for
      select x.suplierout, x.docidout, x.docsymbol, x.ordsymbol, a1.wartosc, a2.wartosc,
          x.good, null, sum(x.quantityonplus),sum(x.quantityonminus), max(x.ordref), max(x.ordstatus),
          sum(x.quantityonplus - x.quantityonminus)
        from ANAL_MWS_REC_DOCS(:suplier,:docid,:bdate,:edate,:doctype,:wh,:palinclude,:onlymistakes) x
          left join atrybuty a1 on (x.vers = a1.wersjaref and a1.cecha = 'KATEGORIA')
          left join atrybuty a2 on (x.vers = a2.wersjaref and a2.cecha = 'PODKATEGORIA')
        where quantityonplus <> 0 or quantityonminus <> 0
        group by  x.suplierout, x.docidout, x.docsymbol, x.ordsymbol, a1.wartosc, a2.wartosc, x.good
        into suplierout, docidout, docsymbol, ordsymbol, atr1, atr2,
          good, vers, quantityonplus, quantityonminus, ordref, ordstatus,
          bilans
    do begin
      ref = ref + 1;
      suspend;
    end
  end else if (groupmode = 2) then
  begin
    for
      select x.suplierout, x.docidout, x.docsymbol, x.ordsymbol, a1.wartosc, a2.wartosc,
          null, null, sum(x.quantityonplus),sum(x.quantityonminus), max(x.ordref), max(x.ordstatus),
          sum(x.quantityonplus - x.quantityonminus)
        from ANAL_MWS_REC_DOCS(:suplier,:docid,:bdate,:edate,:doctype,:wh,:palinclude,:onlymistakes) x
          left join atrybuty a1 on (x.vers = a1.wersjaref and a1.cecha = 'KATEGORIA')
          left join atrybuty a2 on (x.vers = a2.wersjaref and a2.cecha = 'PODKATEGORIA')
        where quantityonplus <> 0 or quantityonminus <> 0
        group by  x.suplierout, x.docidout, x.docsymbol, x.ordsymbol, a1.wartosc, a2.wartosc
        into suplierout, docidout, docsymbol, ordsymbol, atr1, atr2,
          good, vers, quantityonplus, quantityonminus, ordref, ordstatus,
          bilans
    do begin
      ref = ref + 1;
      suspend;
    end
  end else if (groupmode = 3) then
  begin
    for
      select x.suplierout, x.docidout, x.docsymbol, x.ordsymbol, a1.wartosc, null,
          null, null, sum(x.quantityonplus),sum(x.quantityonminus), max(x.ordref), max(x.ordstatus),
          sum(x.quantityonplus - x.quantityonminus)
        from ANAL_MWS_REC_DOCS(:suplier,:docid,:bdate,:edate,:doctype,:wh,:palinclude,:onlymistakes) x
          left join atrybuty a1 on (x.vers = a1.wersjaref and a1.cecha = 'KATEGORIA')
        where quantityonplus <> 0 or quantityonminus <> 0
        group by  x.suplierout, x.docidout, x.docsymbol, x.ordsymbol, a1.wartosc
        into suplierout, docidout, docsymbol, ordsymbol, atr1, atr2,
          good, vers, quantityonplus, quantityonminus, ordref, ordstatus,
          bilans
    do begin
      ref = ref + 1;
      suspend;
    end
  end else if (groupmode = 4) then
  begin
    for
      select x.suplierout, x.docidout, x.docsymbol, x.ordsymbol, null, null,
          null, null, sum(x.quantityonplus),sum(x.quantityonminus), max(x.ordref), max(x.ordstatus),
          sum(x.quantityonplus - x.quantityonminus)
        from ANAL_MWS_REC_DOCS(:suplier,:docid,:bdate,:edate,:doctype,:wh,:palinclude,:onlymistakes) x
        where quantityonplus <> 0 or quantityonminus <> 0
        group by  x.suplierout, x.docidout, x.docsymbol, x.ordsymbol
        into suplierout, docidout, docsymbol, ordsymbol, atr1, atr2,
          good, vers, quantityonplus, quantityonminus, ordref, ordstatus,
          bilans
    do begin
      ref = ref + 1;
      suspend;
    end
  end  if (groupmode = 5) then
  begin
    for
      select x.suplierout, null, null, null, a1.wartosc, a2.wartosc,
          x.good, x.vers, sum(x.quantityonplus),sum(x.quantityonminus), null, null,
          sum(x.quantityonplus - x.quantityonminus)
        from ANAL_MWS_REC_DOCS(:suplier,:docid,:bdate,:edate,:doctype,:wh,:palinclude,:onlymistakes) x
          left join atrybuty a1 on (x.vers = a1.wersjaref and a1.cecha = 'KATEGORIA')
          left join atrybuty a2 on (x.vers = a2.wersjaref and a2.cecha = 'PODKATEGORIA')
        where quantityonplus <> 0 or quantityonminus <> 0
        group by x.suplierout, a1.wartosc, a2.wartosc, x.good, x.vers
        into suplierout, docidout, docsymbol, ordsymbol, atr1, atr2,
          good, vers, quantityonplus, quantityonminus, ordref, ordstatus,
          bilans
    do begin
      ref = ref + 1;
      suspend;
    end
  end else if (groupmode = 6) then
  begin
    for
      select x.suplierout, null, null, null, a1.wartosc, a2.wartosc,
          x.good, null, sum(x.quantityonplus),sum(x.quantityonminus), null, null,
          sum(x.quantityonplus - x.quantityonminus)
        from ANAL_MWS_REC_DOCS(:suplier,:docid,:bdate,:edate,:doctype,:wh,:palinclude,:onlymistakes) x
          left join atrybuty a1 on (x.vers = a1.wersjaref and a1.cecha = 'KATEGORIA')
          left join atrybuty a2 on (x.vers = a2.wersjaref and a2.cecha = 'PODKATEGORIA')
        where quantityonplus <> 0 or quantityonminus <> 0
        group by x.suplierout, a1.wartosc, a2.wartosc, x.good
        into suplierout, docidout, docsymbol, ordsymbol, atr1, atr2,
          good, vers, quantityonplus, quantityonminus, ordref, ordstatus,
          bilans
    do begin
      ref = ref + 1;
      suspend;
    end
  end else if (groupmode = 7) then
  begin
    for
      select x.suplierout, null, null, null, a1.wartosc, a2.wartosc,
          null, null, sum(x.quantityonplus),sum(x.quantityonminus), null, null,
          sum(x.quantityonplus - x.quantityonminus)
        from ANAL_MWS_REC_DOCS(:suplier,:docid,:bdate,:edate,:doctype,:wh,:palinclude,:onlymistakes) x
          left join atrybuty a1 on (x.vers = a1.wersjaref and a1.cecha = 'KATEGORIA')
          left join atrybuty a2 on (x.vers = a2.wersjaref and a2.cecha = 'PODKATEGORIA')
        where quantityonplus <> 0 or quantityonminus <> 0
        group by x.suplierout, a1.wartosc, a2.wartosc
        into suplierout, docidout, docsymbol, ordsymbol, atr1, atr2,
          good, vers, quantityonplus, quantityonminus, ordref, ordstatus,
          bilans
    do begin
      ref = ref + 1;
      suspend;
    end
  end else if (groupmode = 8) then
  begin
    for
      select x.suplierout, null, null, null, a1.wartosc, null,
          null, null, sum(x.quantityonplus),sum(x.quantityonminus), null, null,
          sum(x.quantityonplus - x.quantityonminus)
        from ANAL_MWS_REC_DOCS(:suplier,:docid,:bdate,:edate,:doctype,:wh,:palinclude,:onlymistakes) x
          left join atrybuty a1 on (x.vers = a1.wersjaref and a1.cecha = 'KATEGORIA')
        where quantityonplus <> 0 or quantityonminus <> 0
        group by x.suplierout, a1.wartosc
        into suplierout, docidout, docsymbol, ordsymbol, atr1, atr2,
          good, vers, quantityonplus, quantityonminus, ordref, ordstatus,
          bilans
    do begin
      ref = ref + 1;
      suspend;
    end
  end
end^
SET TERM ; ^
