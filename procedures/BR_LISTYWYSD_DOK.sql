--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_LISTYWYSD_DOK(
      DOKSPED integer)
  returns (
      TYP char(1) CHARACTER SET UTF8                           ,
      REF integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      DATA timestamp,
      AKCEPT smallint,
      KLIENT integer,
      FAKTURA integer,
      SYMBOLFAK varchar(20) CHARACTER SET UTF8                           ,
      WYDRUKOWANO integer,
      WASDEAKCEPT integer,
      WARTSNETTO numeric(14,2),
      WARTSBRUTTO numeric(14,2),
      SPOSZAP integer,
      SPOSZAPDICT varchar(255) CHARACTER SET UTF8                           ,
      KOSZTDOST numeric(14,2),
      UWAGISPED varchar(255) CHARACTER SET UTF8                           ,
      FLAGISPED varchar(255) CHARACTER SET UTF8                           ,
      BLOKADA integer)
   as
begin
  for select distinct DOKTYP, DOKREF
  from LISTYWYSDPOZ
  where DOKUMENT = :doksped
  into :typ, :ref
  do begin
    if(:typ = 'M') then begin
       select DOKUMNAG.SYMBOL, DOKUMNAG.DATA, DOKUMNAG.klient, DOKUMNAG.AKCEPT, DOKUMNAG.FAKTURA, DOKUMNAG.WYDRUKOWANO,
       DOKUMNAG.wasdeakcept, dokumnag.wartsnetto, dokumnag.wartsbrutto, DOKUMNAG.SPOSZAP, PLATNOSCI.opis, DOKUMNAG.kosztdost,
       DOKUMNAG.UWAGISPED, DOKUMNAG.FLAGISPED, DOKUMNAG.BLOKADA
       from DOKUMNAG left join PLATNOSCI on (PLATNOSCI.REF = DOKUMNAG.sposzap)
       where DOKUMNAG.REF=:ref
       into :symbol, :data, :klient, :akcept, :faktura, :wydrukowano, :wasdeakcept, :wartsnetto, :wartsbrutto, :sposzap, :sposzapdict, :kosztdost,
          :uwagisped, :flagisped, :blokada;
    end if(:typ = 'Z') then begin
       select NAGZAM.ID, NAGZAM.DATAWE, NAGZAM.klient, 1, NAGZAM.FAKTURA, NAGZAM.WYDRUKOWANO,
       0, NAGZAM.sumwartnet, NAGZAM.sumwartbru, NAGZAM.SPOSZAP, PLATNOSCI.opis, NAGZAM.kosztdost,
       NAGZAM.UWAGI, '', NAGZAM.BLOKADA
       from NAGZAM left join PLATNOSCI on (PLATNOSCI.REF = NAGZAM.sposzap)
       where NAGZAM.REF=:ref
       into :symbol, :data, :klient, :akcept, :faktura, :wydrukowano, :wasdeakcept, :wartsnetto, :wartsbrutto, :sposzap, :sposzapdict, :kosztdost,
          :uwagisped, :flagisped, :blokada;
    end if (typ = 'R') then begin
      select sreq.symbol, sreq.reqdate, null, 1, sreq.content, sreq.contentflags
        from srvrequests sreq
        where sreq.ref = :ref
        into :symbol, :data, :klient, :akcept, :uwagisped, :flagisped;
    end
    if(:faktura is null) then symbolfak = '';
    else select SYMBOL from NAGFAK where REF=:faktura into :SYMBOLFAK;
    suspend;
  end
end^
SET TERM ; ^
