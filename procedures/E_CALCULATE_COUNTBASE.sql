--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_CALCULATE_COUNTBASE(
      REF integer,
      ISINSERT smallint,
      MFLAG smallint,
      EMPLOYEE integer,
      ECOLUMN integer,
      FROMDATE timestamp,
      TODATE timestamp,
      DOKDATE timestamp,
      LBASE varchar(6) CHARACTER SET UTF8                           ,
      FBASE varchar(6) CHARACTER SET UTF8                           ,
      BASEABSENCE integer,
      CALCPAYBASE smallint,
      CORRECTION smallint,
      WORKSECS integer,
      CERTSERIAL varchar(2) CHARACTER SET UTF8                           ,
      CERTNUMBER varchar(7) CHARACTER SET UTF8                           ,
      DIMNUM smallint,
      DIMDEN smallint,
      NPDAYS integer,
      COMPANY integer)
  returns (
      MONTHPAYBASE numeric(14,2),
      DAYPAYAMOUNT numeric(14,2),
      HOURPAYAMOUNT numeric(14,2),
      PAYAMOUNT numeric(14,2))
   as
declare variable wsk numeric(14,6);
declare variable min_krajowa numeric(14,6);
declare variable basepr numeric(14,2);
declare variable basetype varchar(1);
declare variable sumbase numeric(14,2);
declare variable sumdays numeric(14,2);
declare variable sumhours numeric(14,2);
declare variable eperiod varchar(6);
declare variable tperiod varchar(6);
declare variable efromdate date;
declare variable ayear integer;
declare variable amonth smallint;
declare variable days integer;
declare variable paymenttype integer;
declare variable addsalary numeric(14,2);
declare variable salary numeric(14,2);
declare variable workhours numeric(14,2);
declare variable mindate date;
declare variable workdim numeric(14,6);
declare variable eworkdays integer;
declare variable workdays integer;
declare variable etodate date;
declare variable mfromdate date;
declare variable mtodate date;
declare variable eworkdim numeric(14,6);
declare variable deduction numeric(14,2);
declare variable illpay_reduction varchar(255);
declare variable deduction_days integer;
declare variable aref integer;
declare variable ondate timestamp;
declare variable afromdate timestamp;
declare variable atodate timestamp;
declare variable stdcalendar integer;
begin
/*MWr Personel (201104): Rozliczenie nieobecnosci. Procedura zawiera tresc i pobiera
  wartosci bezposrednio z triggerow EABSENCES_BI _BU _COUNTBASE (parametr ISINSERT =0
  gdy wywolanie przez update, =1 gdy przez insert). Debug danej nieobecnosci REF mozna
  latwo przeprowadzic podajac w parametrach wejsciowych REF ze znakiem minus*/

  if (ref < 0) then
  begin
  --pobranie wartosci z nieobecnosci REF (wywolywane tylko recznie)
    ref = abs(ref);
    select employee, ecolumn, fromdate, todate, dokdate, lbase, fbase,
        baseabsence, calcpaybase, correction, worksecs, certserial, certnumber,
        dimnum, dimden, npdays, company
      from eabsences
      where ref = :ref
      into :employee, :ecolumn, :fromdate, :todate, :dokdate, :lbase, :fbase,
       :baseabsence, :calcpaybase, :correction, :worksecs, :certserial, :certnumber,
       :dimnum, :dimden, :npdays, :company;
  end

  baseabsence = coalesce(baseabsence,0);
  calcpaybase = coalesce(calcpaybase,0);
  isinsert = coalesce(isinsert,0);
  mflag = coalesce(mflag,0);

  select pval from ecolparams
    where param = 'BASETYPE' and ecolumn = :ecolumn
    into :basetype;

  if (basetype = 'V') then
  begin
--rozliczenie nieobecnosci urlopowej -------------------------------------------
    if (isinsert = 1 and baseabsence = ref and mflag = 0) then
    --nie stwierdzono ciaglosci, wyliczamy stawke z biezacych podstaw
      select sum(vbase), sum(vworkeddays), sum(vworkedhours)
        from eabsemplbases
        where employee = :employee
          and period >= :fbase and period <= :lbase
          and (coalesce(vworkedhours,0) > 0 or coalesce(vworkeddays,0) > 0)
        into :sumbase, :sumdays, :sumhours;
    else
    --jest ciaglosc, przeliczamy stawki wg podstaw przypisanych do nieob. BASEABSENCE
      select sum(vbase), sum(vworkeddays), sum(vworkedhours)
        from eabsencebases
        where eabsence = :baseabsence
          and period >= :fbase and period <= :lbase
          and (coalesce(vworkedhours,0) > 0 or coalesce(vworkeddays,0) > 0)
        into :sumbase, :sumdays, :sumhours;

    if (sumdays <> 0) then daypayamount = sumbase / sumdays;
    else daypayamount = 0;

    if (sumhours <> 0) then hourpayamount = sumbase / sumhours;
    else hourpayamount = 0;

    payamount = hourpayamount * (worksecs / 3600.00);
    monthpaybase = null;
  end else if (basetype = 'S') then
  begin
--rozliczenie nieobecnosci chorobowej ------------------------------------------
    days = todate - fromdate + 1;
    ayear = extract(year from fromdate);
    amonth = extract(month from fromdate);
    npdays = coalesce(npdays,0);

    execute procedure e_calculate_zus(ayear || amonth, company)
      returning_values wsk;

    if (ref = baseabsence) then
    begin
    --nie ma ciaglosci podstaw, nalezy wyliczyc podstawe miesieczna

      if (isinsert = 1 and mflag = 0) then
      --wyliczeie stawki z biezacych podstaw
        select sum(sbase)/cast(count(*) as numeric(14,2))
          from eabsemplbases
          where employee = :employee
            and period >= :fbase and period <= :lbase
            and takeit = 1
          into :monthpaybase;
      else
      --przeliczenie stawki wg podstaw przypisanych do nieob. BASEABSENCE
        select sum(sbase)/cast(count(*) as numeric(14,2))
          from eabsencebases
          where eabsence = :baseabsence
            and period >= :fbase and period <= :lbase
            and takeit = 1
          into :monthpaybase;

      if (monthpaybase is null) then
      begin
      /*brak podstaw z tabli podstaw; sprawdzamy czy pracownik zostal zatrudniony w miesiacu choroby
        lub zachorowal w miesiacu nastepnym po m-cu zatrudnienia (gdy zatr. nie pierwszego dnia miesiaca)*/

        if (fbase = lbase and calcpaybase = 0) then
          select min(fromdate) from emplcontracts
            where employee = :employee
              and (empltype = 1 or iflags like '%DFC%')  --BS36832
            into :efromdate;
  
        if (efromdate is not null) then
        begin
          execute procedure e_func_periodinc(null, 0, :efromdate)
            returning_values :eperiod;
    
          execute procedure e_func_periodinc(null, 1, :efromdate - 1)
            returning_values :tperiod;
  
          if (fbase in (:eperiod, :tperiod)) then
          begin
            select paymenttype, coalesce(salary,0), (coalesce(caddsalary,0) + coalesce(funcsalary,0)), coalesce(workdim,0)
              from employment
              where employee = :employee and fromdate = :efromdate
              into :paymenttype, :salary, :addsalary, :workdim;
    
            if (paymenttype = 0) then   -- wynagrodzenie miesieczne
              monthpaybase = salary + addsalary;
            else if (paymenttype = 1) then   -- wynagrodzenie godzinowe
            begin
              execute procedure get_config('STDCALENDAR',2)
                returning_values :stdcalendar;

              select sum(cd.workhours) from ecaldays cd
                  join ecalendars c on (c.ref = cd.calendar)
                where c.ref = :stdcalendar  --BS81336
                  and cd.cyear = :ayear
                  and cd.cmonth = :amonth
                into :workhours;
              monthpaybase = workhours * salary * workdim + addsalary;
            end
            monthpaybase = monthpaybase * wsk;
          end
        end
        monthpaybase = coalesce(monthpaybase,0);
      end
    end else begin
    --pobranie podstawy miesiecznej wynikajacej z ciaglosci
      select monthpaybase from eabsences
        where ref = :baseabsence
        into :monthpaybase;
    end

    if (monthpaybase is not null) then
    begin
    --wyliczenie wartosci minimalnej krajowej (BS24010)
      select count(*) from emplcaldays cd
        join edaykinds edk on edk.ref = cd.daykind
        where cd.employee = :employee
          and cd.pyear = :ayear
          and cd.pmonth = :amonth
          and edk.daytype = 1
        into :workdays;
      workdays = coalesce(workdays, 0);
  
      if (workdays > 0) then
      begin
        execute procedure period2dates(:ayear || :amonth)
          returning_values :mfromdate, :mtodate;
        workdim = 0;
        for
          select workdim, fromdate, todate
             from e_get_employmentchanges(:employee, :mfromdate, :mtodate, ';WORKDIM;')
             into :eworkdim, :efromdate, :etodate
        do begin
          eworkdays = null;
          select count(*) from emplcaldays cd
          join edaykinds edk on edk.ref = cd.daykind
            where cd.employee = :employee
              and cd.cdate <= :etodate
              and cd.cdate >= :efromdate
              and edk.daytype = 1
            into :eworkdays;
  
          workdim = workdim + (eworkdim * coalesce(eworkdays,0)) / workdays;
        end
        execute procedure get_pval(:fromdate, :company, 'MINIMALNAKRAJOWA')
          returning_values min_krajowa;
        min_krajowa = min_krajowa * wsk;
        min_krajowa = min_krajowa * workdim;
      end
  
    --MW: waloryzacja swiadczenia rehabilitacyjnego
      if (ecolumn in (350, 360, 370)) then
      begin
        select min(fromdate) from eabsences
          where employee = :employee and ecolumn in (350,360,370)
            and lbase = :lbase and correction in (0,2)
          into :ondate;
        ondate = coalesce(ondate, fromdate); --(BS33009)
  
        select min(fromdate) from wscfgval
          where wscfgdef = 'WSK_WALORYZACJI' and company = :company
          into :mindate;
  
        if (ondate > mindate) then
          execute procedure get_pval(ondate, :company, 'WSK_WALORYZACJI')
            returning_values :wsk;
  
        if (wsk > 100) then
          monthpaybase = wsk * monthpaybase / 100.00;
      end
  
      select pval from ecolparams
        where ecolumn = :ecolumn and param = 'BASEPR'
        into :basepr;
  
      if (monthpaybase < min_krajowa ) then
        monthpaybase = min_krajowa;
      monthpaybase = cast(monthpaybase * dimnum / dimden as numeric(14,2));
      daypayamount = monthpaybase / 30.00;
      daypayamount = daypayamount * basepr / 100.00;
      payamount = daypayamount * (days - npdays);
    end else
    begin
      monthpaybase = 0;
      daypayamount = 0;
      payamount = 0;
    end
    hourpayamount = null;

  /*obnizenie zasilku za niedostarczenie w terminie zwolnienia lekarskiego;
    liczymy od osmego dnia nieobecnosci do dnia dostarczenia wlacznie*/
    if (dokdate is not null and ecolumn in (90,100,110,140,150,160,170,270,280,290,440,450)
      and daypayamount > 0 and correction in (0,2)) then
    begin
      execute procedure get_config('ILLPAY_REDUCTION',2)
        returning_values :illpay_reduction;

      if (illpay_reduction = '1') then
      begin
        if (certserial is null) then certserial = '';
        if (certnumber is null) then certnumber = '';

        select first 1 ref from eabsences
          where todate + 1 = :fromdate and correction in (0,2) and employee = :employee
            and coalesce(certserial,'') = :certserial and coalesce(certnumber,'') = :certnumber --BS50346
          into :aref;

        if (aref is not null) then  --BS35450
          select afromdate from e_get_whole_eabsperiod(:aref,null,null,:certserial,:certnumber,null,';40;50;60;90;100;110;140;150;160;170;270;280;290;440;450;') --BS42203
            into :afromdate;
        afromdate = coalesce(afromdate,fromdate);

        afromdate = afromdate + 7; --dzien, od ktorego nalezy zmniejszyc wynagrodzenie
        atodate = dokdate;         --dzien, do ktorego nalezy zmniejszyc wynagrodzenie

        if (fromdate > afromdate) then
          afromdate = fromdate;
        if (todate < atodate) then
          atodate = todate;

        deduction_days = atodate - afromdate + 1;
        if (deduction_days > 0) then
        begin
          deduction = deduction_days * 0.25 * daypayamount;
          payamount = payamount - deduction;
        end
      end
    end
  end
  suspend;
end^
SET TERM ; ^
