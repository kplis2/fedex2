--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_VAT7_17_POZ(
      EDECLARATIONREF EDECLARATIONS_ID)
  returns (
      K10 INTEGER_ID,
      K11 INTEGER_ID,
      K12 INTEGER_ID,
      K13 INTEGER_ID,
      K14 INTEGER_ID,
      K15 INTEGER_ID,
      K16 INTEGER_ID,
      K17 INTEGER_ID,
      K18 INTEGER_ID,
      K19 INTEGER_ID,
      K20 INTEGER_ID,
      K21 INTEGER_ID,
      K22 INTEGER_ID,
      K23 INTEGER_ID,
      K24 INTEGER_ID,
      K25 INTEGER_ID,
      K26 INTEGER_ID,
      K27 INTEGER_ID,
      K28 INTEGER_ID,
      K29 INTEGER_ID,
      K30 INTEGER_ID,
      K31 INTEGER_ID,
      K32 INTEGER_ID,
      K33 INTEGER_ID,
      K34 INTEGER_ID,
      K35 INTEGER_ID,
      K36 INTEGER_ID,
      K37 INTEGER_ID,
      K38 INTEGER_ID,
      K39 INTEGER_ID,
      K40 INTEGER_ID,
      K41 INTEGER_ID,
      K42 INTEGER_ID,
      K43 INTEGER_ID,
      K44 INTEGER_ID,
      K45 INTEGER_ID,
      K46 INTEGER_ID,
      K47 INTEGER_ID,
      K48 INTEGER_ID,
      K49 INTEGER_ID,
      K50 INTEGER_ID,
      K51 INTEGER_ID,
      K52 INTEGER_ID,
      K53 INTEGER_ID,
      K54 INTEGER_ID,
      K55 INTEGER_ID,
      K56 INTEGER_ID,
      K57 INTEGER_ID,
      K58 INTEGER_ID,
      K59 INTEGER_ID,
      K60 INTEGER_ID,
      K61 INTEGER_ID,
      K62 SMALLINT_ID,
      K63 SMALLINT_ID,
      K64 SMALLINT_ID,
      K65 SMALLINT_ID,
      K66 SMALLINT_ID,
      K67 SMALLINT_ID,
      K68 SMALLINT_ID,
      K69 INTEGER_ID)
   as
declare variable edecpos_pvalue type of column edeclpos.pvalue;
declare variable edecpos_fieldsymbol type of column edeclpos.fieldsymbol;
declare variable filed_prefix char_1;
begin
/*TS: FK - pobiera dane dla wydruku na podstawie danych wskazanej e-Deklaracji.
  Zwrocone zostana tylko czesci stale wydruku (dla sekcji, ktore listuja pozycje,
  gdzie ich liczba jest zmienna, dane generuje osobna procedura z dopiskiem _POZ).

  > EDECLARATIONREF - numer e-Deklaracji, ktora chcemy wydrukowac.
*/

  /* Prefix nazw pol e-Deklaracji */
  filed_prefix = 'K';

  for
    select ep.fieldsymbol, ep.pvalue
      from edeclarations e
        join edeclpos ep on (e.ref = ep.edeclaration)
      where e.ref = :edeclarationref
      into :edecpos_fieldsymbol, :edecpos_pvalue
  do begin
    if(:edecpos_fieldsymbol = :filed_prefix||'10') then K10 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'11') then K11 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'12') then K12 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'13') then K13 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'14') then K14 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'15') then K15 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'16') then K16 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'17') then K17 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'18') then K18 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'19') then K19 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'20') then K20 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'21') then K21 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'22') then K22 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'23') then K23 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'24') then K24 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'25') then K25 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'26') then K26 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'27') then K27 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'28') then K28 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'29') then K29 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'30') then K30 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'31') then K31 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'32') then K32 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'33') then K33 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'34') then K34 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'35') then K35 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'36') then K36 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'37') then K37 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'38') then K38 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'39') then K39 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'40') then K40 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'41') then K41 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'42') then K42 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'43') then K43 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'44') then K44 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'45') then K45 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'46') then K46 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'47') then K47 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'48') then K48 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'49') then K49 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'50') then K50 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'51') then K51 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'52') then K52 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'53') then K53 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'54') then K54 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'55') then K55 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'56') then K56 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'57') then K57 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'58') then K58 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'59') then K59 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'60') then K60 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'61') then K61 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'62') then K62 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'63') then K63 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'64') then K64 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'65') then K65 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'66') then K66 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'67') then K67 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'68') then K68 = :edecpos_pvalue;
    else if(:edecpos_fieldsymbol = :filed_prefix||'69') then K69 = :edecpos_pvalue;
  end
  suspend;
end^
SET TERM ; ^
