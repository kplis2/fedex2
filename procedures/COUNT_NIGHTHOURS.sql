--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COUNT_NIGHTHOURS(
      ECALENDAR integer,
      EMPLWORKSTART time,
      EMPLWORKEND time,
      EMPLWORKSTART2 time,
      EMPLWORKEND2 time)
  returns (
      NIGHTHOURS integer)
   as
  declare variable zero_time time;
  declare variable nighthours_start time;
  declare variable nighthours_end time;
begin
  select c.nighthours_start, c.nighthours_end
    from ecalendars c
    where c.ref = :ecalendar
    into :nighthours_start, :nighthours_end;
  zero_time = '00:00';
  --czy wystapily jakies nocne
  if (emplworkstart < nighthours_start or emplworkstart > nighthours_end
    or emplworkend < nighthours_start or emplworkend > nighthours_end) then
  begin
    --1.jezeli praca zaczela sie po polnocy w godzinach nocnych
    if (emplworkstart < nighthours_end) then
    begin
      --1.1.i skonczyla sie przed zakonczeniem nocnych
      if (emplworkend < nighthours_end and emplworkend >= emplworkstart) then
        nighthours = emplworkend - emplworkstart;
      --1.2. i zakonczyla sie nastepnego dnia w godzinach nocnych (niemozliwe)
      else if (emplworkend < nighthours_end) then
        nighthours = 8 * 60 * 60 - (emplworkstart - emplworkend);
      --1.3. skonczyla sie dopiero po rozpoczeciu nastepnych nocnych
      else if (emplworkend > nighthours_start) then
        nighthours = nighthours_end - emplworkstart + (emplworkend - nighthours_start);
      else nighthours = nighthours_end - emplworkstart;
    end
    --2.jezeli praca zaczela sie przed polnoca w godzinach nocnych
    else if (emplworkstart >= nighthours_start) then
    begin
      --2.1.i skonczyla sie jeszcze przed polnoca
      if (emplworkend > emplworkstart) then
        nighthours = emplworkend - emplworkstart;
      --2.2.skonczyla sie przed zakonczenie godzin nocnych
      else if (emplworkend < nighthours_end) then
        nighthours = 86400 - (emplworkstart - zero_time) + (emplworkend - zero_time);
      else nighthours = 86400 - (emplworkstart - zero_time) + (nighthours_end - zero_time);
    end
    --3.praca nie zaczela sie w godzinach nocnych (nalezy sprawdzic czy sie skonczyla)
    --moga wystapic nadgodziny w porze nocnej
    else
    begin
      --3.1 skonczyla sie w godzinach nocnych przed polnoca
      if (emplworkend > nighthours_start) then
        nighthours = emplworkend - nighthours_start;
      --3.2 skonczyla sie w godzinach nocnych po polnocy
      else if (emplworkend < nighthours_end) then
        nighthours = 86400 - (nighthours_start - zero_time) + (emplworkend - zero_time);
      --3.3 skonczyla sie po godzinach nocnych
      else if (emplworkend < emplworkstart) then
        nighthours = 8 * 60 * 60;
    end
  end
  suspend;
end^
SET TERM ; ^
