--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_CHECKOPERSTARTTIME(
      SCHEDOPER integer,
      PSTARTTIME timestamp)
  returns (
      STARTTIME timestamp)
   as
declare variable minspace integer;
declare variable "ROUND" integer;
declare variable minlength double precision;
declare variable roundtime double precision;
declare variable daytime double precision;
declare variable startdate date;
declare variable daytimepers integer;
declare variable dpndtime timestamp;
begin
  --sprawdzenie czasu zaknczenia ostatniej oepracji poprzedzajacej
  select max(OPER.endtime)
    from PRSCHEDOPERS OPER
      join prschedoperdeps DEP on (DEP.depfrom = OPER.ref)
    where DEP.depto = :schedoper
  into :dpndtime;
  starttime = :pstarttime;
  if(:starttime < :dpndtime) then
    starttime = :dpndtime;
  select PRSCHEDULES.mintimebetweenopers, prschedules.roundstarttime
    from prschedules join PRSCHEDOPERS on (PRSCHEDOPERS.schedule = prschedules.ref)
    where prschedopers.ref = :schedoper
    into :minspace, :round;
  minlength = 1440;
  minlength = 1/:minlength;
  if(:starttime < (:dpndtime + (minspace * minlength)))then
    starttime = :dpndtime + (minspace * minlength);

  if(:round > 0) then begin
    roundtime = :minlength * :round;
    startdate = cast(:starttime as date);-- sama data, czyli godzina 00:00
    daytime = :starttime - cast(:startdate as timestamp);--czas w danym dniu liczony nie w sekundach, ale w dniach
    daytimepers = :daytime /:roundtime;--ilosc okresow wyrownania w ciagu dnia
    if(:daytimepers < :daytime/:roundtime) then--zaokraglenei w gore
      daytimepers = :daytimepers + 1;
    starttime = cast(startdate as timestamp) + (:daytimepers * :roundtime);
    if(:starttime < :pstarttime) then
      starttime = :pstarttime;--przesuniecie na skutek zaokrąglen
  end
end^
SET TERM ; ^
