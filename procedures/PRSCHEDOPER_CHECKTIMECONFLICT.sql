--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDOPER_CHECKTIMECONFLICT(
      SCHEDOPER integer,
      SCHEDULE integer,
      MACHINE integer,
      STARTTIME timestamp,
      ENDTIME timestamp)
  returns (
      TIMECONFLICT integer)
   as
declare variable seclen double precision;
begin
    seclen = 1;
    seclen = :seclen/24/60/60;
    timeconflict = 0;
    --sprawdzenie konfliktow na maszynie
    if(exists(
           select ref from PRSCHEDOPERS where schedule = :schedule and machine = :machine
             and starttime < :starttime-:seclen and endtime > :starttime +:seclen and endtime <= :endtime
             and ref<>:schedoper
    )) then begin
      timeconflict = 1;
    end
    if(timeconflict = 0) then begin
      --sprawdzanie, czy operacje poprzedzajace nie koncza sie po moim starcie
      if(exists(
             select PRSCHEDOPERS.ref from prschedopers
             join prschedoperdeps on (prschedopers.ref = prschedoperdeps.depfrom)
               where prschedoperdeps.depto = :schedoper and prschedopers.endtime > :starttime + :seclen
               and prschedopers.ref <> :schedoper
       ))then begin
         timeconflict = 2;
       end
    end
end^
SET TERM ; ^
