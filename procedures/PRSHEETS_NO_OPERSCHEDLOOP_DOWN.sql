--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHEETS_NO_OPERSCHEDLOOP_DOWN(
      OPER integer)
  returns (
      TEST integer)
   as
DECLARE VARIABLE SCHEDULED INTEGER;
DECLARE VARIABLE DESCRIPT VARCHAR(255);
DECLARE VARIABLE OPERDOWN INTEGER;
begin
  operdown = null;
  scheduled = null;
  for select ref, scheduled, descript from prshopers po where po.opermaster = :oper  into :operdown, :scheduled, :descript
  do begin
    if(scheduled is not null and scheduled = 1) then exception prsheets_loop 'Operacja podrzędna "'||substring(:descript from 1 for 20)||'" jest już harmonogramowana.';
    if(operdown is not null and operdown > 0) then execute procedure prsheets_no_operschedloop_down(operdown) returning_values :test;
  end
end^
SET TERM ; ^
