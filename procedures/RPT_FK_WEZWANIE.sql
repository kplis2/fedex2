--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_WEZWANIE(
      REFNOTA integer,
      ODDATY timestamp,
      DODATY timestamp,
      WEZWTYP varchar(10) CHARACTER SET UTF8                           ,
      STATUS smallint,
      COMPANY integer)
  returns (
      REF integer,
      DOCREF integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      NUMER integer,
      DATA timestamp,
      KHKOD varchar(255) CHARACTER SET UTF8                           ,
      KHKODKS varchar(255) CHARACTER SET UTF8                           ,
      KHNAZWA varchar(255) CHARACTER SET UTF8                           ,
      KHADRESUL varchar(255) CHARACTER SET UTF8                           ,
      KHADRESM varchar(255) CHARACTER SET UTF8                           ,
      KHADRES varchar(255) CHARACTER SET UTF8                           ,
      SUMAMOUNT numeric(14,2),
      SUMINTEREST numeric(14,2),
      NAME varchar(20) CHARACTER SET UTF8                           ,
      TEXT varchar(1024) CHARACTER SET UTF8                           ,
      CURRENCY varchar(3) CHARACTER SET UTF8                           )
   as
declare variable notatyp varchar(10);
declare variable slodef integer;
declare variable slopoz integer;
declare variable nic varchar(255);
declare variable typ varchar(15);
begin
  ref = 0;
  for
    select NOTYNAG.ref, NOTYNAG.data, NOTYNAG.symbol, NOTYNAG.notatyp,
      NOTYNAG.slokod, NOTYNAG.slokodks, notynag.slodef, notynag.slopoz,
      bdebit, notynag.interests, notynag.currency
    from NOTYNAG
    where ((:refnota is null ) or (:refnota = 0) or (:refnota = notynag.ref))
      and notynag.notakind = 0
      and ((:oddaty is null) or (notynag.data >= :oddaty))
      and ((:dodaty is null) or (notynag.data <= :dodaty))
      and ((:wezwtyp is null) or (:wezwtyp = '') or (:wezwtyp = notynag.notatyp))
      and ((:status is null) or (notynag.status = :status))
      and notynag.company = :company
    order by notynag.data,notynag.slokodks
    into :docref, :data, :symbol, :notatyp, :khkod, :khkodks, :slodef, :slopoz,
      :sumamount, :suminterest, :currency
  do begin
    execute procedure SLO_DANE(:slodef, :slopoz) returning_values :khkod,:khnazwa,:nic;
    select typ from slodef where ref = :slodef into :typ;
    if (typ = 'KLIENCI') then
      select klienci.ulica||iif(coalesce(klienci.nrdomu,'')<>'',' '||klienci.nrdomu,'')||iif(coalesce(klienci.nrlokalu,'')<>'','/'||klienci.nrlokalu,'')||
        iif(klienci.poczta<>'' and klienci.poczta<>klienci.miasto, ' ' || klienci.miasto,''),
        klienci.kodp||' '||iif(klienci.poczta<>'',klienci.poczta,klienci.miasto)
      from KLIENCI where REF=:slopoz into :khadresul, :khadresm;
    else if (typ = 'DOSTAWCY') then
      select dostawcy.ulica||iif(coalesce(dostawcy.nrdomu,'')<>'',' '||dostawcy.nrdomu,'')||iif(coalesce(dostawcy.nrlokalu,'')<>'','/'||dostawcy.nrlokalu,'')||
        iif(dostawcy.poczta<>'' and dostawcy.poczta<>dostawcy.miasto, ' ' || dostawcy.miasto,''),
        dostawcy.kodp||' '||iif(dostawcy.poczta<>'',dostawcy.poczta,dostawcy.miasto)
      from DOSTAWCY where REF=:slopoz into :khadresul, :khadresm;
    else if (typ = 'EMPLOYEES') then
      select first 1 A.stpref||' '||A.street||' '||A.houseno||case when A.localno <> '' then '/' else ' ' end
        || A.localno || iif(A.post<>'' and A.post<>A.city, ' ' || A.city,''),
        A.postcode||' '||iif(A.post<>'',A.post,A.city)
        from epersaddr A join persons P on (A.person = P.ref and A.addrtype = 0 and A.status = 1)
          join employees E on (P.ref = E.person)
        where E.ref = :slopoz
        into :khadresul, :khadresm;
    else if (typ = 'PERSONS') then
      select A.stpref||' '||A.street||' '||A.houseno||case when A.localno <> '' then '/' else ' ' end
        ||A.localno || iif(A.post<>'' and A.post<>A.city, ' ' || A.city,''),
        A.postcode||' '||iif(A.post<>'',A.post,A.city)
        from epersaddr A join persons P on (A.person = P.ref and A.addrtype = 0 and A.status = 1)
        where P.ref = :slopoz
        into :khadresul, :khadresm;

    select NOTYTYP.degree, NOTYTYP.name, NOTYTYP.descript from NOTYTYP where NOTYTYP.TYP = :notatyp into :numer, :name, :text;
    ref = :ref + 1;
    if (khadresul is null) then
      khadresul = '';
    if (khadresm is null) then
      khadresm = '';
    KHADRES = khadresul || ', ' || khadresm; -- zmiena uzywania w niekturych szblonach wydruków wezwań do zapaty i po transferze trzeba by modyfukowac wydruki
    suspend;
  end
end^
SET TERM ; ^
