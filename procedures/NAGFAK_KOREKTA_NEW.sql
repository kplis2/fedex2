--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_KOREKTA_NEW(
      FAKTURA integer,
      REJKOR varchar(3) CHARACTER SET UTF8                           ,
      TYPKOR varchar(3) CHARACTER SET UTF8                           ,
      STANOWISKO varchar(10) CHARACTER SET UTF8                           ,
      DATA timestamp,
      OPERATOR integer,
      CALOSC integer)
  returns (
      STATUS integer,
      KOREKTA integer)
   as
declare variable KORCALOSC integer;
declare variable REFK integer;
declare variable REFFAK integer;
declare variable NIEOBROT integer;
declare variable AKCEPTACJA integer;
declare variable ZALICZKOWY integer;
declare variable KORZALICZKA integer;
declare variable PARAMN1 numeric(14,4);
declare variable PARAMN2 numeric(14,4);
declare variable PARAMN3 numeric(14,4);
declare variable PARAMN4 numeric(14,4);
declare variable PARAMN5 numeric(14,4);
declare variable PARAMN6 numeric(14,4);
declare variable PARAMN7 numeric(14,4);
declare variable PARAMN8 numeric(14,4);
declare variable PARAMN9 numeric(14,4);
declare variable PARAMN10 numeric(14,4);
declare variable PARAMN11 numeric(14,4);
declare variable PARAMN12 numeric(14,4);
declare variable PARAMS1 varchar(255);
declare variable PARAMS2 varchar(255);
declare variable PARAMS3 varchar(255);
declare variable PARAMS4 varchar(255);
declare variable PARAMS5 varchar(255);
declare variable PARAMS6 varchar(255);
declare variable PARAMS7 varchar(255);
declare variable PARAMS8 varchar(255);
declare variable PARAMS9 varchar(255);
declare variable PARAMS10 varchar(255);
declare variable PARAMS11 varchar(255);
declare variable PARAMS12 varchar(255);
declare variable PARAMD1 timestamp;
declare variable PARAMD2 timestamp;
declare variable PARAMD3 timestamp;
declare variable PARAMD4 timestamp;
declare variable ANULOWANIE integer;
declare variable REFKOLD integer;
declare variable SAD smallint;
declare variable POZSAD integer;
declare variable NEWPOZSAD integer;
declare variable REFDOPAKIET integer;
declare variable POZREF integer;
declare variable NT varchar(200);
declare variable NUMER integer;
declare variable KTM varchar(40);
declare variable NAZWAT TOWARY_NAZWA;
declare variable PKWIU PKWIU;
declare variable WERSJAREF integer;
declare variable ILOSC numeric(14,4);
declare variable ILOSCO numeric(14,4);
declare variable ILOSCM numeric(14,4);
declare variable DOSTAWA integer;
declare variable JEDN TOWJEDN;
declare variable JEDNO TOWJEDN;
declare variable CENACEN CENYWAL;
declare variable WALCEN WALUTA_ID;
declare variable KURSCEN KURS;
declare variable TABKURSCEN TABKURS_ID;
declare variable RABAT CENY;
declare variable RABATTAB CENY;
declare variable CENAMAG numeric(14,2);
declare variable CENANET CENYWAL;
declare variable CENABRU numeric(14,4);
declare variable WARTNET numeric(14,2);
declare variable WARTBRU numeric(14,2);
declare variable GR_VAT VAT_ID;
declare variable VAT CENY;
declare variable CENANETZL CENYSPR;
declare variable CENABRUZL CENYSPR;
declare variable WARTNETZL CENY;
declare variable wartbruzl CENY;
declare variable PREC smallint;
declare variable ALTTOPOZ POZZAM_ID;
declare variable FAKE smallint;
declare variable ALTTOPOZN DOKUMPOZ_ID;
declare variable MAGAZYN DEFMAGAZ_ID;
declare variable OPK smallint;
declare variable OPIS STRING255;
declare variable REF POZFAK_ID;
declare variable SYMBOL_DOST SYMBOLDOST_ID;
declare variable FROMPROMOREF PROMOCJE_ID;
declare variable ILOSCZADOPAKIET ILOSCI_FAK;
begin

  STATUS = -1;
  /* szukamy ostatniej korekty */
  refk = :faktura;
  anulowanie = 0;
  while(:refk is not null and :anulowanie=0) do begin
    faktura = :refk;
    refk = null;
    anulowanie = 0;
    select korekta from NAGFAK n where n.ref = :faktura into :refk;
    if(:refk is not null) then select anulowanie from NAGFAK n where n.ref = :refk into :anulowanie;
  end
  execute procedure GEN_REF('NAGFAK') returning_values :korekta;
  if(:korekta = 0 or (:korekta is null)) then exit;
  /* sprawdzenie, czy juz nie ma korekty lub faktury lub jest nieobrotowy */
  select REFFAK, NIEOBROT, AKCEPTACJA, ZALICZKOWY from NAGFAK where REF=:faktura into  :REFFAK, :nieobrot, :akceptacja, :zaliczkowy;
  refk = 0;
  if(exists(select first 1 1 from NAGFAK where REFK = :faktura and anulowanie = 0)) then refk = 1;
  if(:akceptacja = 0 or :akceptacja = 9 or :akceptacja = 10) then exception KOREKTA_NEW_DOKNIEACK;
  if(:refk > 0 and anulowanie = 0) then exception KOREKTA_NEW_ISKOR;
  if(:reffak > 0) then exception KOREKTA_NEW_ISFAK;
  if(:nieobrot > 1 and :zaliczkowy=0) then exception KOREKTA_NEW_NIEOBROT;
  insert into NAGFAK(REF,REJESTR,TYP,STANOWISKO,DATA,AKCEPTACJA,
        KLIENT,PLATNIK,DOSTAWCA,SPOSZAP,TERMIN,UZYKLI, RABAT,
        REFK, SYMBOLK,UWAGI,
        TABKURS,WALUTA,KURS,WALUTOWA,PKURS,VKURS,PVKURS,
        SUMWARTNET, PSUMWARTNET,SUMWARTBRU, PSUMWARTBRU,
        ESUMWARTNET, PESUMWARTNET, ESUMWARTBRU, PESUMWARTBRU,
        SUMWARTNETZL, PSUMWARTNETZL,SUMWARTBRUZL, PSUMWARTBRUZL,
        ESUMWARTNETZL, PESUMWARTNETZL, ESUMWARTBRUZL, PESUMWARTBRUZL,
        SUMKAUCJA, PSUMKAUCJA, SUMKAUCJAZL, PSUMKAUCJAZL,
        SPRZEDAWCA, OPERATOR, GRUPADOK, BN, numpar, datafisk, MAGAZYN,
        CPLUCZESTID, CPLUCZESTKARTID, SLODEF, SLOPOZ,
        DKODP, DMIASTO, DULICA, ODBIORCA, ODBIORCAID)
     select :korekta, :rejkor, :typkor,:stanowisko,:data,0,
        KLIENT,PLATNIK, DOSTAWCA,SPOSZAP, TERMIN, UZYKLI, RABAT,
        REF,SYMBOL,UWAGI,
        TABKURS,WALUTA,KURS,WALUTOWA,KURS,VKURS,VKURS,
        SUMWARTNET, SUMWARTNET, SUMWARTBRU, SUMWARTBRU,
        ESUMWARTNET, ESUMWARTNET, ESUMWARTBRU, ESUMWARTBRU,
        SUMWARTNETZL, SUMWARTNETZL, SUMWARTBRUZL, SUMWARTBRUZL,
        ESUMWARTNETZL, ESUMWARTNETZL, ESUMWARTBRUZL, ESUMWARTBRUZL,
        SUMKAUCJA, SUMKAUCJA, SUMKAUCJAZL, SUMKAUCJAZL,
        SPRZEDAWCA, :operator, GRUPADOK, BN, NUMPAR, DATAFISK, MAGAZYN,
        CPLUCZESTID, CPLUCZESTKARTID, SLODEF, SLOPOZ,
        DKODP, DMIASTO, DULICA, ODBIORCA, ODBIORCAID
      from NAGFAK where REF=:faktura;
  update nagfak set korekta = :korekta where ref = :faktura;
  select TYPFAK.KORCALOSC, TYPFAK.ZALICZKOWY, typfak.sad from TYPFAK right join NAGFAK on ( NAGFAK.TYP = TYPFAK.SYMBOL) where NAGFAk.REF = :korekta into :korcalosc, :korzaliczka, :sad;
  if(:calosc = 1) then korcalosc = 1;

  if(:korcalosc = 1 and :sad = 0) then begin
    for
      select NUMER,KTM,NAZWAT,PKWIU,WERSJAREF,DOSTAWA,
         ILOSC,ILOSCO,ILOSCM, JEDN, JEDNO,
         CENACEN,WALCEN, KURSCEN,TABKURSCEN,
         RABAT, RABATTAB, CENANET, CENABRU, WARTNET, WARTBRU,
         CENAMAG, GR_VAT, VAT,CENANETZL, CENABRUZL,
         WARTNETZL, WARTBRUZL, SYMBOL_DOST, OPK, MAGAZYN,OPIS,
         PARAMN1, PARAMN2, PARAMN3, PARAMN4,
         PARAMN5, PARAMN6, PARAMN7, PARAMN8,
         PARAMN9, PARAMN10, PARAMN11, PARAMN12,
         PARAMS1, PARAMS2, PARAMS3, PARAMS4,
         PARAMS5, PARAMS6, PARAMS7, PARAMS8,
         PARAMS9, PARAMS10, PARAMS11, PARAMS12,
         PARAMD1, PARAMD2, PARAMD3, PARAMD4, REF, frompromoref, ilosczadopakiet, prec,
         FAKE, ALTTOPOZ
      from POZFAK where DOKUMENT = :faktura
      order by POZFAK.NUMER, POZFAK.alttopoz nulls first
      into :NUMER, :KTM, :NAZWAT, :PKWIU, :WERSJAREF, :DOSTAWA,
         :ILOSC, :ILOSCO, :ILOSCM, :JEDN, :JEDNO,
         :CENACEN,:WALCEN,:KURSCEN,:TABKURSCEN,
         :RABAT, :RABATTAB, :CENANET, :CENABRU, :WARTNET, :WARTBRU,
         :CENAMAG, :GR_VAT, :VAT, :CENANETZL, :CENABRUZL,
         :WARTNETZL, :WARTBRUZL, :SYMBOL_DOST, :OPK, :MAGAZYN,OPIS,
         :PARAMN1, :PARAMN2, :PARAMN3, :PARAMN4,
         :PARAMN5, :PARAMN6, :PARAMN7, :PARAMN8,
         :PARAMN9, :PARAMN10, :PARAMN11, :PARAMN12,
         :PARAMS1, :PARAMS2, :PARAMS3, :PARAMS4,
         :PARAMS5, :PARAMS6, :PARAMS7, :PARAMS8,
         :PARAMS9, :PARAMS10, :PARAMS11, :PARAMS12,
         :PARAMD1, :PARAMD2, :PARAMD3, :PARAMD4, :REF, :frompromoref, :ilosczadopakiet, :prec,
         :FAKE, :ALTTOPOZ
    do begin

      -- ustawienie odwoań w polu ALTTOPOZ
      AltToPozN = null;
      if (AltToPoz is not null) then
      begin
        select first 1 p.ref
        from pozfak p
        where p.dokument = :korekta
          and refk = :ALTTOPOZ
        into :AltToPozN;
      end

      insert into POZFAK(DOKUMENT,NUMER,ORD,KTM,NAZWAT,PKWIU,WERSJAREF,DOSTAWA,
           ILOSC, ILOSCO, ILOSCM, PILOSC,PILOSCO,PILOSCM,PNAZWAT,
           JEDN,JEDNO, CENACEN, WALCEN,PCENACEN,PWALCEN,KURSCEN,PKURSCEN,TABKURSCEN,
           RABAT,PRABAT,RABATTAB, PRABATTAB, CENANET, PCENANET, CENABRU, PCENABRU,
           WARTNET, PWARTNET, WARTBRU, PWARTBRU, PCENAMAG,GR_VAT, VAT, PGR_VAT, PVAT,
           CENANETZL, PCENANETZL,CENABRUZL, PCENABRUZL, WARTNETZL, PWARTNETZL,
           WARTBRUZL, PWARTBRUZL, SYMBOL_DOST, OPK, MAGAZYN,OPIS, PARAMN1, PARAMN2,
           PARAMN3, PARAMN4, PARAMN5, PARAMN6, PARAMN7, PARAMN8, PARAMN9, PARAMN10,
           PARAMN11, PARAMN12, PARAMS1, PARAMS2, PARAMS3, PARAMS4, PARAMS5, PARAMS6,
           PARAMS7, PARAMS8, PARAMS9, PARAMS10, PARAMS11, PARAMS12, PARAMD1, PARAMD2,
           PARAMD3, PARAMD4, REFK, frompromoref, ilosczadopakiet, prec, FAKE, ALTTOPOZ)
       values( :korekta, :NUMER, 1, :KTM, :NAZWAT, :PKWIU, :WERSJAREF, :DOSTAWA,
           0, 0,0, :ILOSC, :ILOSCO, :ILOSCM, :NAZWAT, :JEDN, :JEDNO,
           :CENACEN, :WALCEN, :CENACEN, :WALCEN, :KURSCEN, :KURSCEN, :TABKURSCEN,
           :RABAT, :RABAT, :RABATTAB, :RABATTAB, NULL, :CENANET, NULL, :CENABRU,
           0, :WARTNET, 0, :WARTBRU, :CENAMAG, :GR_VAT, :VAT, :GR_VAT, :VAT,
           :CENANETZL, :CENANETZL, :CENABRUZL, :CENABRUZL, 0, :WARTNETZL, 0, :WARTBRUZL,
           :SYMBOL_DOST, :OPK, :MAGAZYN, :OPIS,
           :PARAMN1, :PARAMN2, :PARAMN3, :PARAMN4, :PARAMN5, :PARAMN6, :PARAMN7, :PARAMN8,
           :PARAMN9, :PARAMN10, :PARAMN11, :PARAMN12,
           :PARAMS1, :PARAMS2, :PARAMS3, :PARAMS4, :PARAMS5, :PARAMS6, :PARAMS7, :PARAMS8,
           :PARAMS9, :PARAMS10, :PARAMS11, :PARAMS12,
           :PARAMD1, :PARAMD2, :PARAMD3, :PARAMD4, :REF, :frompromoref, :ilosczadopakiet, :prec,
           :FAKE, :AltToPozN);
    end
   end else if (:korcalosc = 0 and :sad = 0) then begin

    for
      select NUMER,KTM,NAZWAT,PKWIU,WERSJAREF,DOSTAWA,
         ILOSC, ILOSCO, ILOSCM, NAZWAT, JEDN, JEDNO,
         CENACEN, WALCEN, KURSCEN, TABKURSCEN, RABAT, RABATTAB, CENAMAG,
         GR_VAT, VAT, CENANET, CENABRU, WARTNET,  WARTBRU,
         CENANETZL, CENABRUZL,  WARTNETZL, WARTBRUZL,
         OPK, MAGAZYN,OPIS,
         PARAMN1, PARAMN2, PARAMN3, PARAMN4,
         PARAMN5, PARAMN6, PARAMN7, PARAMN8,
         PARAMN9, PARAMN10, PARAMN11, PARAMN12,
         PARAMS1, PARAMS2, PARAMS3, PARAMS4,
         PARAMS5, PARAMS6, PARAMS7, PARAMS8,
         PARAMS9, PARAMS10, PARAMS11, PARAMS12,
         PARAMD1, PARAMD2, PARAMD3, PARAMD4,
         REF, frompromoref, ilosczadopakiet, prec,
         FAKE, ALTTOPOZ
      from POZFAK where DOKUMENT = :faktura
      order by POZFAK.NUMER, POZFAK.alttopoz nulls first
      into  :NUMER, :KTM, :NAZWAT, :PKWIU, :WERSJAREF, :DOSTAWA,
         :ILOSC, :ILOSCO, :ILOSCM, :NAZWAT, :JEDN, :JEDNO,
         :CENACEN, :WALCEN, :KURSCEN, :TABKURSCEN, :RABAT, :RABATTAB, :CENAMAG,
         :GR_VAT, :VAT, :CENANET, :CENABRU, :WARTNET, :WARTBRU,
         :CENANETZL, :CENABRUZL, :WARTNETZL, :WARTBRUZL,
         :OPK, :MAGAZYN,:OPIS,
         :PARAMN1, :PARAMN2, :PARAMN3, :PARAMN4,
         :PARAMN5, :PARAMN6, :PARAMN7, :PARAMN8,
         :PARAMN9, :PARAMN10, :PARAMN11, :PARAMN12,
         :PARAMS1, :PARAMS2, :PARAMS3, :PARAMS4,
         :PARAMS5, :PARAMS6, :PARAMS7, :PARAMS8,
         :PARAMS9, :PARAMS10, :PARAMS11, :PARAMS12,
         :PARAMD1, :PARAMD2, :PARAMD3, :PARAMD4,
         :REF, :frompromoref, :ilosczadopakiet, :prec,
         :FAKE, :ALTTOPOZ
       do begin

        -- ustawienie odwoań w polu ALTTOPOZ
        AltToPozN = null;
        if (AltToPoz is not null) then
        begin
          select first 1 p.ref
          from pozfak p
          where p.dokument = :korekta
            and refk = :ALTTOPOZ
          into :AltToPozN;
        end

        insert into POZFAK(DOKUMENT,NUMER,ORD,KTM,NAZWAT,PKWIU,WERSJAREF,DOSTAWA,
             ILOSC, ILOSCO, ILOSCM, PILOSC,PILOSCO, PILOSCM, PNAZWAT,
             JEDN, JEDNO,
             CENACEN, WALCEN,PCENACEN,PWALCEN,KURSCEN,PKURSCEN,TABKURSCEN,
             RABAT,PRABAT,RABATTAB, PRABATTAB, PCENAMAG,
             GR_VAT, VAT, PGR_VAT, PVAT,
             CENANET, PCENANET, CENABRU, PCENABRU,
             WARTNET, PWARTNET, WARTBRU, PWARTBRU,
             CENANETZL, PCENANETZL,CENABRUZL, PCENABRUZL,
             WARTNETZL, PWARTNETZL, WARTBRUZL, PWARTBRUZL, OPK, MAGAZYN,OPIS,
             PARAMN1, PARAMN2, PARAMN3, PARAMN4,
             PARAMN5, PARAMN6, PARAMN7, PARAMN8,
             PARAMN9, PARAMN10, PARAMN11, PARAMN12,
             PARAMS1, PARAMS2, PARAMS3, PARAMS4,
             PARAMS5, PARAMS6, PARAMS7, PARAMS8,
             PARAMS9, PARAMS10, PARAMS11, PARAMS12,
             PARAMD1, PARAMD2, PARAMD3, PARAMD4,REFK, frompromoref, ilosczadopakiet, prec,
             FAKE, ALTTOPOZ)
         values( :korekta, :NUMER,1,:KTM,:NAZWAT,:PKWIU,:WERSJAREF,:DOSTAWA,
             :ILOSC, :ILOSCO, :ILOSCM, :ILOSC, :ILOSCO, :ILOSCM, :NAZWAT,
             :JEDN, :JEDNO,
             :CENACEN, :WALCEN, :CENACEN, :WALCEN, :KURSCEN, :KURSCEN, :TABKURSCEN,
             :RABAT, :RABAT, :RABATTAB, :RABATTAB, :CENAMAG,
             :GR_VAT, :VAT, :GR_VAT, :VAT, NULL, :CENANET, NULL, :CENABRU,
             :WARTNET, :WARTNET, :WARTBRU, :WARTBRU,
             :CENANETZL, :CENANETZL, :CENABRUZL, :CENABRUZL,
             :WARTNETZL, :WARTNETZL, :WARTBRUZL, :WARTBRUZL, :OPK, :MAGAZYN, :OPIS,
             :PARAMN1, :PARAMN2, :PARAMN3, :PARAMN4,
             :PARAMN5, :PARAMN6, :PARAMN7, :PARAMN8,
             :PARAMN9, :PARAMN10, :PARAMN11, :PARAMN12,
             :PARAMS1, :PARAMS2, :PARAMS3, :PARAMS4,
             :PARAMS5, :PARAMS6, :PARAMS7, :PARAMS8,
             :PARAMS9, :PARAMS10, :PARAMS11, :PARAMS12,
             :PARAMD1, :PARAMD2, :PARAMD3, :PARAMD4,:REF, :frompromoref, :ilosczadopakiet, :prec,
             :FAKE, :AltToPozN);
    end
   end else if (:sad = 1) then begin
    --kopiowanie pozsad i pozfaków
    for select p.ref
      from pozsad p
      where p.faktura = :faktura
      order by p.numer
      into :pozsad
    do begin
      execute procedure GEN_REF('POZSAD') returning_values :newpozsad;
      insert into pozsad(ref, faktura, numer, opis, podstawa, grupacla, stawkacla, clo,
            wartnet, gr_vat, vat,  clovat,
            ppodstawa, pgrupacla, pstawkacla, pclo,
            pwartnet, pgr_vat, pvat,  pclovat,
            ord, refk)
        select :newpozsad, :korekta, numer, opis, podstawa, grupacla, stawkacla, clo,
            wartnet, gr_vat, vat,  clovat,
            podstawa, grupacla, stawkacla, clo,
            wartnet, gr_vat, vat,  clovat,
            1, ref
          from pozsad where ref = :pozsad;
      --kopiowane pozfaków powiązanych z pozsad
      insert into POZFAK(DOKUMENT,NUMER,ORD,KTM,NAZWAT,PKWIU,WERSJAREF,DOSTAWA,
         ILOSC, ILOSCO, ILOSCM, PILOSC,PILOSCO, PILOSCM, PNAZWAT,
         JEDN, JEDNO,
         CENACEN, WALCEN,PCENACEN,PWALCEN,KURSCEN,PKURSCEN,TABKURSCEN,
         RABAT,PRABAT,RABATTAB, PRABATTAB,
         PCENAMAG,
         GR_VAT, VAT, PGR_VAT, PVAT,
         CENANET, PCENANET, CENABRU, PCENABRU,
         WARTNET, PWARTNET, WARTBRU, PWARTBRU,
         CENANETZL, PCENANETZL,CENABRUZL, PCENABRUZL,
         WARTNETZL, PWARTNETZL, WARTBRUZL, PWARTBRUZL, OPK, MAGAZYN,OPIS,
         PARAMN1, PARAMN2, PARAMN3, PARAMN4,
         PARAMN5, PARAMN6, PARAMN7, PARAMN8,
         PARAMN9, PARAMN10, PARAMN11, PARAMN12,
         PARAMS1, PARAMS2, PARAMS3, PARAMS4,
         PARAMS5, PARAMS6, PARAMS7, PARAMS8,
         PARAMS9, PARAMS10, PARAMS11, PARAMS12,
         PARAMD1, PARAMD2, PARAMD3, PARAMD4, REFK, REFPOZSAD, frompromoref, ilosczadopakiet, prec)
      select :korekta,NUMER,1,KTM,NAZWAT,PKWIU,WERSJAREF,DOSTAWA,
         ILOSC, ILOSCO, ILOSCM, ILOSC, ILOSCO, ILOSCM, NAZWAT,
         JEDN, JEDNO,
         CENACEN, WALCEN,CENACEN,WALCEN,KURSCEN,KURSCEN,TABKURSCEN,
         RABAT, RABAT, RABATTAB, RABATTAB,
         CENAMAG,
         GR_VAT, VAT, GR_VAT, VAT,
         NULL, CENANET, NULL, CENABRU,
         WARTNET, WARTNET, WARTBRU, WARTBRU,
         CENANETZL, CENANETZL, CENABRUZL, CENABRUZL,
         WARTNETZL, WARTNETZL, WARTBRUZL, WARTBRUZL, OPK, MAGAZYN,OPIS,
         PARAMN1, PARAMN2, PARAMN3, PARAMN4,
         PARAMN5, PARAMN6, PARAMN7, PARAMN8,
         PARAMN9, PARAMN10, PARAMN11, PARAMN12,
         PARAMS1, PARAMS2, PARAMS3, PARAMS4,
         PARAMS5, PARAMS6, PARAMS7, PARAMS8,
         PARAMS9, PARAMS10, PARAMS11, PARAMS12,
         PARAMD1, PARAMD2, PARAMD3, PARAMD4, REF, :newpozsad, frompromoref, ilosczadopakiet, prec
      from POZFAK
      where DOKUMENT = :faktura and refpozsad = :pozsad
        and coalesce(fake,0) = 0
      order by POZFAK.NUMER;
    end  
   end
   if(:zaliczkowy=1 and :korzaliczka=2 and :korcalosc=1) then begin
     insert into NAGFAKZAL(FAKTURA,FAKTURAZAL,VAT,SYMBFAKZAL,WARTNET,WARTBRU,WARTNETZL,WARTBRUZL)
       select :korekta,:faktura,vat,symbol,wartnet,wartbru,wartnetzl,wartbruzl
       from NAGFAK_ZALICZKIDOROZ(:korekta,0,:faktura);
   end

   --odtwarzanie REFDOPAKIET korekty na podstawie faktury zródowej
   for select PK.REF, PKP.REF
     from POZFAK PF
     join POZFAK PFP on (PF.refdopakiet = PFP.ref)
     join POZFAK PK on (pk.refk = pf.ref and pk.dokument = :korekta)
     join POZFAK PKP on (PKP.refk = PFP.ref and pkp.dokument = :korekta)
     where PF.dokument = :faktura and pF.refdopakiet > 0
     into :pozref, :refdopakiet
   do begin
     update POZFAK set REFDOPAKIET = :refdopakiet where ref=:pozref;
   end

   status = 1;
end^
SET TERM ; ^
