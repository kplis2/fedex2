--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMPL_SENIORITY(
      EMPLREF integer,
      ONDATE date)
  returns (
      YEARSC integer,
      MONTHSC integer,
      DAYSC integer,
      FIRSTJOB integer)
   as
declare variable fromd date;
  declare variable tod date;
  declare variable days integer;
  declare variable education_years integer;
  declare variable ed_fromdate date;
  declare variable ed_todate date;
begin

  --staż pracy na podstawie ukończonych szkol
  select first 1 Z.addinfo
    from eperschools E
      join edictzuscodes Z on (E.education = Z.ref)
      join employees EM on (EM.person = E.person)
    where EM.ref = :emplref and E.isvacbase = 1 and E.todate <= :ondate
    order by E.fromdate desc
    into  :education_years;

  days = 0;

  --staż pracy na podstawie swiadectw pracy
  for
    select fromdate, todate
      from eworkcertifs
      where employee = :EmplRef
      into :fromd, :tod
  do begin
    days = days + (tod - fromd + 1);
    for
      select s.fromdate, s.todate
        from eperschools s
          join employees e on (e.person = s.person)
         where e.ref = :emplref and s.isvacbase = 1 and s.todate <= :ondate
           and s.fromdate <= :tod and s.todate >= :fromd
        into :ed_fromdate, :ed_todate
    do begin
      if (ed_fromdate < fromd) then ed_fromdate = fromd;
      if (ed_todate > tod) then ed_todate = tod;
      days = days - (ed_todate - ed_fromdate + 1);
    end
  end

  for
    select fromdate, coalesce(cast(todate as date), :ondate)
      from employment E
      where E.employee = :EmplRef and fromdate <= :ondate
      into :fromd, :tod
  do begin
    if (tod > ondate) then
      tod = ondate;
    days = days + (tod - fromd + 1);

    for
      select s.fromdate, s.todate
        from eperschools s
          join employees e on (e.person = s.person)
         where e.ref = :emplref and s.isvacbase = 1 and s.todate <= :ondate
           and s.fromdate <= :tod and s.todate >= :fromd
        into :ed_fromdate, :ed_todate
    do begin
      if (ed_fromdate < fromd) then ed_fromdate = fromd;
      if (ed_todate > tod) then ed_todate = tod;
      days = days - (ed_todate - ed_fromdate + 1);
    end
  end


  yearsc  = cast(extract(YEAR from ondate) as int) - cast(extract(YEAR from ondate - days) as int);
  monthsc = cast(extract(MONTH from ondate) as int) - cast(extract(MONTH from ondate - days) as int);
  daysc   = cast(extract(DAY from ondate) as int) - cast(extract(DAY from ondate - days) as int);
  firstjob = 0;

  if (daysc < 0) then
  begin
    monthsc = monthsc - 1;
    daysc = daysc + 30;
  end

  if (monthsc < 0) then
  begin
    yearsc = yearsc - 1;
    monthsc = monthsc + 12;
  end

  if (yearsc = 0 and (cast(extract(MONTH from ondate) as int) - monthsc > 1
    or (cast(extract(MONTH from ondate) as int) - monthsc = 1 and cast(extract(DAY from ondate) as int) - daysc > 2))) then
    firstjob = 1;

  if (yearsc = 9) then
    firstjob = 10;

  yearsc = yearsc + coalesce(education_years, 0);

  YEARSC = coalesce(yearsc, 0);
  monthsc = coalesce(monthsc, 0);
  daysc = coalesce(daysc, 0);

  suspend;
end^
SET TERM ; ^
