--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAKEVACPLAN_INS2(
      F varchar(24) CHARACTER SET UTF8                           ,
      T varchar(24) CHARACTER SET UTF8                           ,
      D1 varchar(2) CHARACTER SET UTF8                           ,
      D2 varchar(2) CHARACTER SET UTF8                           ,
      M1 integer,
      M2 integer)
  returns (
      F2 varchar(24) CHARACTER SET UTF8                           ,
      T2 varchar(24) CHARACTER SET UTF8                           )
   as
begin
  execute procedure EMAKEVACPLAN_INS( F, D1, M1 ) returning_values F;
  if( M1<M2 ) then
  begin
    execute procedure EMAKEVACPLAN_INS( T, '--', M1 ) returning_values T;
    execute procedure EMAKEVACPLAN_INS( F, '--', M2 ) returning_values F;
  end
  M1 = M1 + 1;
  while (M1 < M2) do
  begin
    execute procedure EMAKEVACPLAN_INS( F, '--', M1 ) returning_values F;
    execute procedure EMAKEVACPLAN_INS( T, '--', M1 ) returning_values T;
    M1 = M1 + 1;
  end
  execute procedure EMAKEVACPLAN_INS( T, D2, M2 ) returning_values T;
  F2 = F;
  T2 = T;
  suspend;
end^
SET TERM ; ^
