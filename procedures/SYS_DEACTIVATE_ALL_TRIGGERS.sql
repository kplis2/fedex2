--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_DEACTIVATE_ALL_TRIGGERS(
      ACTIV smallint)
   as
declare variable trigger_name varchar(31);
begin
  for
    select rdb$trigger_name
      from rdb$triggers
      where (rdb$system_flag is null or rdb$system_flag = 0)
--        and rdb$trigger_type in (5,6) --tu sprawa jest bardziej skomplikowana, lepiej wszystkie
      into :trigger_name
  do begin
    if (activ = 0) then
      execute statement 'alter trigger ' || :trigger_name || ' inactive;';
    else
      execute statement 'alter trigger ' || :trigger_name || ' active;';
  end
end^
SET TERM ; ^
