--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_INPOST_CANCELPACK_RESPONSE(
      EDEDOCSH_REF integer)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
   as
declare variable listywysd_ref listywysd_id;
declare variable shippingdocno string40;
declare variable parent integer_id;
declare variable packageref integer_id;
declare variable packageno string40;
declare variable labelformat string40;
declare variable cnt integer_id;
declare variable do_cancel smallint_id;
declare variable root_node integer_id;
declare variable cancel_status smallint_id;
declare variable package_node_id integer_id;
begin
  select oref
    from ededocsh
    where ref = :ededocsh_ref
  into :listywysd_ref;

  otable = 'LISTYWYSD';
  oref = :listywysd_ref;

  do_cancel = 1;

  select p.id
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.name = 'CancelPackegeResponse'
    into :root_node;

  for select ep.id, trim(ep.val)
    from ededocsp ep
    where ep.ededoch = :EDEDOCSH_REF
      and ep.parent = :root_node
      and ep.name = 'package'
    into :package_node_id, :packageno
  do begin
    select trim(ep.val)
      from ededocsp ep
      where ep.ededoch = :ededocsh_ref
        and ep.parent = :package_node_id
        and ep.name = 'cancelStatus'
    into :cancel_status;

    if(coalesce(:cancel_status, 0) = 0)then
      do_cancel = 0;
  end

  if(coalesce(:do_cancel,0) = 1)then
  begin
    update listywysd lwd set lwd.x_anulowany = 1
      where lwd.ref is not distinct from :listywysd_ref;
  end

  suspend;
end^
SET TERM ; ^
