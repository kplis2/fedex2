--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_WORKDIM4RCA(
      PERSON PERSONS_ID,
      COMPANY COMPANIES_ID,
      FROMDATE date,
      TODATE date)
  returns (
      B03L smallint,
      B03M smallint,
      EMPLSTATUS smallint)
   as
declare variable employee employees_id;
declare variable b03l_part smallint;
declare variable b03m_part smallint;
declare variable new_b03m smallint;
declare variable isrecord smallint;
begin
--MWr: Personel - Procedura wyliczajaca wymiar czasu pracy dla raportu ZUS RCA

  b03l = 0;        --wymiar czasu pracy licznik
  b03m = 1;        --wymiar czasu pracy minownik
  emplstatus = 0;  --informacja, czy osoba jest zatrudniona w okresie

  for
    select distinct e.ref from employees e
      join employment m on (m.employee = e.ref)
      where e.person = :person and e.company = :company
      into :employee
  do begin
    b03l_part = 0;
    b03m_part = b03m;

    select first 1 dimnum, dimden, 1
      from employment
      where employee = :employee and fromdate <= :todate
        and (todate is null or todate >= :fromdate)
      order by fromdate desc
      into :b03l_part, :b03m_part, :isrecord;

    if (emplstatus = 0 and isrecord = 1) then
      emplstatus = 1;

    if (b03m_part <> b03m) then
    begin
    --sprowadzanie do wspolnego mianownika
      select ret from nww(:b03m_part, :b03m) into :new_b03m;
      b03l = b03l * (new_b03m / b03m);
      b03l_part = b03l_part * (new_b03m / b03m_part);
      b03m = new_b03m;
    end
    b03l = b03l_part + b03l;
  end

  if (b03l = 0 and b03m = 1) then
  begin
    b03l = null;
    b03m = null;
  end else
  if (mod(b03l, b03m) = 0 or b03l > b03m) then
  begin
    b03l = 1;
    b03m = 1;
  end

  suspend;
end^
SET TERM ; ^
