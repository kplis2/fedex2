--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_SHIPPLABEL_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable shippingdoc integer;
declare variable shippingdocno varchar(20);
declare variable grandparent integer;
declare variable deliverytype integer;
declare variable accesscode string80;
declare variable filepath string255;
declare variable format string10;
declare variable branch oddzial_id;
begin
  select l.ref, substr(l.symbolsped,1,20), l.sposdost, l.oddzial
    from listywysd l
    where l.ref = :oref
  into :shippingdoc, :shippingdocno, :deliverytype, :branch;

  --sprawdzanie czy istieje dokument spedycyjny
  if (shippingdoc is null) then exception ede_fedex 'Brak listy spedycyjnej.';

  val = null;
  parent = null;
  params = null;
  id = 0;
  name = 'wydrukujEtykiete';
  suspend;

  grandparent = id;

  --pola nadawcy
  select first 1 k.klucz, k.sciezkapliku
    from sposdost s
      join spedytwys w on (s.ref = w.sposdost)
      left join spedytkonta k on (w.spedytor = k.spedytor)
    where s.ref = :deliverytype
      and k.oddzial = :branch
  into :accesscode, :filepath;

  val = 'PRODUCTION'; --jesli chcesz testowac zmien na TEST
  name = 'serviceUrlType';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;
  
  val = :accesscode;
  name = 'kodDostepu';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;
/*
  val = ''; --:filepath;
  name = 'sciezkaPliku';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;
*/
  val = :shippingdocno;
  name = 'numerPrzesylki';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = 'EPL';
  name = 'format';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;
end^
SET TERM ; ^
