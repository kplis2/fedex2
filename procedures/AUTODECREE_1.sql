--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_1(
      BKDOC integer)
   as
declare variable ACCOUNT ACCOUNT_ID;
declare variable SIDE integer;
declare variable AMOUNT numeric(14,2);
declare variable STRANSDATE date;
declare variable CURRAMOUNT numeric(14,2);
declare variable TMP varchar(255);
declare variable SETTLEMENT varchar(20);
declare variable DESCRIPT varchar(1024);
declare variable PM varchar(1);
declare variable ACC1 ACCOUNT_ID;
declare variable ACC2 ACCOUNT_ID;
declare variable CURRENCY varchar(3);
declare variable COUNTRY_CURRENCY varchar(3);
declare variable RATE numeric(14,4);
declare variable SREF integer;
declare variable MULTICURRENCY smallint;
declare variable BD_DESCRIPT varchar(80);
declare variable FSOPERTYPE integer;
declare variable KASABANK char(1);
declare variable SMULTICURR smallint;
declare variable SWRATE numeric(10,4);
declare variable REDKURS smallint;
declare variable UJROZNKURS account_id;
declare variable PLROZNKURS account_id;
declare variable DIST1DDEF integer;
declare variable DIST1SYMBOL SYMBOLDIST_ID;
declare variable DIST2DDEF integer;
declare variable DIST2SYMBOL SYMBOLDIST_ID;
declare variable DIST3DDEF integer;
declare variable DIST3SYMBOL SYMBOLDIST_ID;
declare variable DIST4DDEF integer;
declare variable DIST4SYMBOL SYMBOLDIST_ID;
declare variable DIST5DDEF integer;
declare variable DIST5SYMBOL SYMBOLDIST_ID;
declare variable DIST6DDEF integer;
declare variable DIST6SYMBOL SYMBOLDIST_ID;
declare variable BANKACCOUNT integer;
begin

   -- schemat dekretowania - raporty kasowe i wyciagow bankowych
  select BKD.descript, STN.konto, STN.waluta, STN.kasabank, BKD.bankaccount
    from bkdocs BKD
      join rkrapkas RK on (RK.ref=BKD.oref and BKD.ref=:bkdoc)
      join rkstnkas STN on (STN.kod=RK.stanowisko)
    into :bd_descript, :acc2, :currency, :kasabank, :bankaccount;

  select wartosc from konfig where akronim='COUNTRY_CURRENCY'
    into :country_currency;

  if (kasabank = 'B') then
    fsopertype = 5;
  else
    fsopertype = 6;



  if (currency = country_currency) then
  begin
    for
      select DP.ref, dn.data, DN.opis, DP.rozrachunek, DP.pm, DP.konto, DP.kwotazl, DP.kwota, DP.kurs, DN.walutowy, DN.waluta,
          dp.dist1ddef, dp.dist1symbol, dp.dist2ddef, dp.dist2symbol, dp.dist3ddef, dp.dist3symbol,
          dp.dist4ddef, dp.dist4symbol, dp.dist5ddef, dp.dist5symbol, dp.dist6ddef, dp.dist6symbol
        from bkdocs BKD
          join rkrapkas RK on (RK.ref=BKD.oref and BKD.ref=:bkdoc)
          join rkdoknag DN on (DN.raport=RK.ref and DN.anulowany=0)
          join rkdokpoz DP on (DP.dokument = DN.ref)
        order by DN.lp, DP.lp
        into :sref, :stransdate, :descript, :tmp, :pm, :acc1, :amount,  :curramount, :rate, :multicurrency, :currency,
          :dist1ddef, :dist1symbol, :dist2ddef, :dist2symbol, :dist3ddef, :dist3symbol, :dist4ddef, :dist4symbol,
          :dist5ddef, :dist5symbol, :dist6ddef, :dist6symbol
    do begin

      descript = substring(bd_descript || ' ' || descript from 1 for 80);
      if (:acc1 is not null and :acc1 <> '') then
      begin

        if (pm = '+') then side = 0; else side = 1;
        account = acc2;

        execute procedure insert_decree(bkdoc, account, side, amount, descript, 0);

        side = 1 - side; -- odwrocenie strony
        account = acc1;  settlement = tmp;
        if (multicurrency = 0) then
        begin
          if (settlement <> '') then
            execute procedure insert_decree_settlement(bkdoc, account, side, amount, descript, settlement, :stransdate, :fsopertype, null, null, null, null, 'RKDOKPOZ', :sref, :bankaccount, 0);
          else
            execute procedure insert_decree_dists(bkdoc, account, side, amount, descript, dist1ddef, dist1symbol,
              dist2ddef, dist2symbol, dist3ddef, dist3symbol, dist4ddef, dist4symbol, dist5ddef, dist5symbol, 0, 1,
              dist6ddef, dist6symbol );
        end else
        begin
          if (settlement <> '') then
            execute procedure insert_decree_currsettlement(bkdoc, account, side, amount, curramount, rate, currency, descript, settlement, :stransdate, :fsopertype, null, null, null, null, 'RKDOKPOZ', :sref, null, null, null, 0);
          else
            execute procedure insert_decree_currency(bkdoc, account, side, amount, curramount, rate, currency, descript, 0);
        end
      end
    end
  end else

  -- STANOWISKO WALUTOWE
  begin
    for
      select dn.data, DN.opis, DP.rozrachunek, DP.pm, DP.konto, DP.kurs,
             PO.smulticurr, dr.kwota, dr.kwota * sw.kurs, sw.kurs, dr.ref,
             coalesce(defo.redkurs,0), defo.kontoujrozkurs, defo.kontoplrozkurs,
             dp.dist1ddef, dp.dist1symbol, dp.dist2ddef, dp.dist2symbol, dp.dist3ddef, dp.dist3symbol,
             dp.dist4ddef, dp.dist4symbol, dp.dist5ddef, dp.dist5symbol, dp.dist6ddef, dp.dist6symbol
        from bkdocs BKD
          join rkrapkas RK on (RK.ref=BKD.oref and BKD.ref=:bkdoc)
          join rkdoknag DN on (DN.raport=RK.ref and DN.anulowany=0)
          join rkdefoper defo on (defo.symbol = dn.operacja and defo.techoper = 0)
          join rkdokpoz DP on (DP.dokument = DN.ref)
          join rkdokroz dr on (dr.rkdokpoz = dp.ref and coalesce(dr.anulata,0) = 0)
          join rkstanwal sw on (sw.ref = dr.rkstanwal)
          join rkpozoper PO on (PO.ref = dp.pozoper)
        order by DN.lp, DP.lp
        into :stransdate, :descript, :tmp, :pm, :acc1, :rate,
          :smulticurr, :curramount, :amount, :swrate, :sref,
          :redkurs, :ujroznkurs, :plroznkurs,
          :dist1ddef, :dist1symbol, :dist2ddef, :dist2symbol,
          :dist3ddef, :dist3symbol, :dist4ddef, :dist4symbol,
          :dist5ddef, :dist5symbol, :dist6ddef, :dist6symbol
    do begin
      descript = substring(bd_descript || ' ' || descript from 1 for 80);
      if (:acc1 is not null and :acc1 <> '') then
      begin
         if (smulticurr is null) then smulticurr = 0;
        if (pm = '+') then side = 0; else side = 1;
        if (:amount < 0 ) then
        begin
          curramount = -:curramount;
          amount = -:amount;
        end

        account = acc2;
        if(smulticurr <> 1) then
          execute procedure insert_decree(bkdoc, account, side, amount, descript, 0);
        else
          execute procedure insert_decree_currency(bkdoc, account, side, amount, curramount, swrate, currency, descript, 0);

        side = 1 - side; -- zmiana strony
        account = acc1;  settlement = tmp;

        if (:settlement <> '') then
        begin
          if(smulticurr <> 1) then
            execute procedure insert_decree_settlement(bkdoc, account, side, amount, descript, settlement,
              :stransdate, :fsopertype, null, null, null, null, 'RKDOKROZ', :sref, :bankaccount, 0);
          else
            execute procedure insert_decree_currsettlement(bkdoc, account, side, amount, curramount, swrate, currency, descript, settlement,
              :stransdate, :fsopertype, null, null, null, null, 'RKDOKROZ', :sref, null, null, null, 0);
        end
        else begin
          if(smulticurr <> 1) then
            execute procedure insert_decree_dists(bkdoc, account, side, amount, descript, dist1ddef, dist1symbol,
              dist2ddef, dist2symbol, dist3ddef, dist3symbol, dist4ddef, dist4symbol, dist5ddef, dist5symbol, 0, 1,
              dist6ddef, dist6symbol );
          else
            execute procedure insert_decree_currency(bkdoc, account, side, amount, curramount, swrate, currency, descript, 0);
        end
        if (:redkurs = 1 and :rate <> :swrate) then
        begin
          -- bankowe roznice kursowe
          amount = cast((:curramount * rate)as numeric(14,2)) - cast((curramount * swrate)as numeric(14,2));
          if (:amount > 0) then
            descript = substring('Dod. ban. różn. kurs. '||cast(curramount as varchar(25)) || ' * (' || cast(rate as varchar(25)) || '-' || cast(swrate as varchar(25)) || ')' from 1 for 80);
          else
            descript = substring('Uj. ban. różn. kurs '||cast(curramount as varchar(25)) || ' * (' || cast(rate as varchar(25)) || '-' || cast(swrate as varchar(25)) || ')' from 1 for 80);
          if (:amount > 0) then
          begin
            execute procedure insert_decree(bkdoc, plroznkurs, 1, amount, descript, 0);
            execute procedure insert_decree(bkdoc, acc2, 0, amount, descript, 0);
          end else begin
            execute procedure insert_decree(bkdoc, ujroznkurs, 0, (-1)* amount, descript, 0);
            execute procedure insert_decree(bkdoc, acc2, 1, (-1)* amount, descript, 0);
          end
        end
      end
    end --for select

    --<BS59416 - ksiegowanie operacji technicznych
    for
      select dn.data, DN.opis, DP.rozrachunek, DP.pm, DP.konto, DP.kurs,
             PO.smulticurr, dp.kwota, dp.kwotazl, dp.kurs,
             coalesce(defo.redkurs,0), defo.kontoujrozkurs, defo.kontoplrozkurs,
             dp.dist1ddef, dp.dist1symbol, dp.dist2ddef, dp.dist2symbol, dp.dist3ddef, dp.dist3symbol,
             dp.dist4ddef, dp.dist4symbol, dp.dist5ddef, dp.dist5symbol, dp.dist6ddef, dp.dist6symbol
        from bkdocs BKD
          join rkrapkas RK on (RK.ref=BKD.oref and BKD.ref=:bkdoc)
          join rkdoknag DN on (DN.raport=RK.ref and DN.anulowany=0)
          join rkdefoper defo on (defo.symbol = dn.operacja and defo.techoper = 1)
          join rkdokpoz DP on (DP.dokument = DN.ref)
          join rkpozoper PO on (PO.ref = dp.pozoper)
        order by DN.lp, DP.lp
        into :stransdate, :descript, :tmp, :pm, :acc1, :rate,
          :smulticurr, :curramount, :amount, :swrate,
          :redkurs, :ujroznkurs, :plroznkurs,
          :dist1ddef, :dist1symbol, :dist2ddef, :dist2symbol,
          :dist3ddef, :dist3symbol, :dist4ddef, :dist4symbol,
          :dist5ddef, :dist5symbol, :dist6ddef, :dist6symbol
    do begin
      descript = substring(bd_descript || ' ' || descript from 1 for 80);
      if (:acc1 is not null and :acc1 <> '') then
      begin
        if (smulticurr is null) then smulticurr = 0;
        if (pm = '+') then side = 0; else side = 1;
        if (:amount < 0 ) then
        begin
          curramount = -:curramount;
          amount = -:amount;
        end

        account = acc2;
        if(smulticurr <> 1) then
          execute procedure insert_decree(bkdoc, account, side, amount, descript, 0);
        else
          execute procedure insert_decree_currency(bkdoc, account, side, amount, curramount, swrate, currency, descript, 0);

        side = 1 - side; -- zmiana strony
        account = acc1;  settlement = tmp;

        if(smulticurr <> 1) then
          execute procedure insert_decree_dists(bkdoc, account, side, amount, descript, dist1ddef, dist1symbol,
            dist2ddef, dist2symbol, dist3ddef, dist3symbol, dist4ddef, dist4symbol, dist5ddef, dist5symbol, 0, 1,
            dist6ddef, dist6symbol );
        else
          execute procedure insert_decree_currency(bkdoc, account, side, amount, curramount, swrate, currency, descript, 0);
      end
    end --BS59416>

  end --stanowiska walutowe
end^
SET TERM ; ^
