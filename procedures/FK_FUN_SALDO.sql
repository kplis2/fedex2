--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_FUN_SALDO(
      COMPANY integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      ACCOUNTS ACCOUNT_ID,
      SIDE smallint)
  returns (
      AMOUNT numeric(15,2))
   as
declare variable YEARID integer;
declare variable S_PERIOD varchar(6);
declare variable DEBIT numeric(15,2);
declare variable CREDIT numeric(15,2);
declare variable ACCOUNTING_REF integer;
declare variable SALDO_DEBIT numeric(14,2);
declare variable SALDO_CREDIT numeric(14,2);
begin

  select yearid from bkperiods where id = :period
    and company = :company
    into :yearid;
  s_period = cast(yearid as varchar(4)) || '00';
  accounts = replace(accounts,  '?',  '_') || '%';
  amount = 0;
  for
    select ref
    from accounting
    where currency = 'PLN' and yearid = :yearid and account like :accounts
    into :accounting_ref
  do begin
    select sum(debit), sum(credit) from turnovers
      where accounting = :accounting_ref and period >= :s_period
        and period <= :period and company = :company
      into :debit, :credit;

    if (debit is null) then
      debit = 0;
    if (credit is null) then
      credit = 0;

    saldo_debit = 0;
    saldo_credit = 0;

    if (abs(debit) > abs(credit)) then
      saldo_debit = debit - credit;
    else
      saldo_credit = credit - debit;

    if (side = 0) then
      amount = amount + saldo_debit;
    else
      amount = amount + saldo_credit;
  end
  suspend;
end^
SET TERM ; ^
