--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_VAT_ZAKROZ_STVAT_BKPER(
      PERIODID varchar(6) CHARACTER SET UTF8                           ,
      VREG varchar(10) CHARACTER SET UTF8                           ,
      ZAKRES smallint,
      ZPOPMIES smallint,
      COMPANY integer,
      VATDEBITTYPE integer)
  returns (
      REF integer,
      LP integer,
      VATREG VATREGS_ID,
      VATREGNR integer,
      TRANSDATE timestamp,
      DOCDATE timestamp,
      VATDATE timestamp,
      DOCSYMBOL varchar(20) CHARACTER SET UTF8                           ,
      BKREG BKREGS_ID,
      BKREGNR integer,
      KONTRAHKOD varchar(255) CHARACTER SET UTF8                           ,
      NIP varchar(15) CHARACTER SET UTF8                           ,
      WARTBRU numeric(14,2),
      WARTNET numeric(14,2),
      WARTVATE numeric(14,2),
      WARTVATV numeric(14,2),
      WART22 numeric(14,2),
      VATE22 numeric(14,2),
      VATV22 numeric(14,2),
      VATN22 numeric(14,2),
      WART23 numeric(14,2),
      VATE23 numeric(14,2),
      VATV23 numeric(14,2),
      VATN23 numeric(14,2),
      WART12 numeric(14,2),
      VATE12 numeric(14,2),
      VATV12 numeric(14,2),
      VATN12 numeric(14,2),
      WART07 numeric(14,2),
      VATE07 numeric(14,2),
      VATV07 numeric(14,2),
      VATN07 numeric(14,2),
      WART08 numeric(14,2),
      VATE08 numeric(14,2),
      VATV08 numeric(14,2),
      VATN08 numeric(14,2),
      WART03 numeric(14,2),
      VATE03 numeric(14,2),
      VATV03 numeric(14,2),
      VATN03 numeric(14,2),
      WART05 numeric(14,2),
      VATE05 numeric(14,2),
      VATV05 numeric(14,2),
      VATN05 numeric(14,2),
      WART00 numeric(14,2),
      VATE00 numeric(14,2),
      VATV00 numeric(14,2),
      WARTZW numeric(14,2),
      VATZW numeric(14,2),
      WARTPOZ numeric(14,2),
      VATEPOZ numeric(14,2),
      VATVPOZ numeric(14,2),
      VATNPOZ numeric(14,2),
      VATN numeric(14,2),
      WARTVATN numeric(14,2),
      VATGR VAT_ID)
   as
declare variable NETV numeric(14,2);
declare variable VATE numeric(14,2);
declare variable VATV numeric(14,2);
declare variable DOCREF integer;
declare variable SQL varchar(5048);
declare variable VATREGTYPE integer; /* <<PR73894: PL>> */
declare variable print integer;
begin
  ref = 0;
  lp = 0;
  if(:zakres is null) then zakres = 0;
  if(:zpopmies is null) then zpopmies = 0;

--BS08473
  sql = 'select B.REF, b.transdate, b.docdate, B.vatdate, B.vatreg, b.vnumber, b.bkreg, b.number, B.NIP, b.contractor, b.symbol, v.vatregtype'; --<<PR73894: PL>>
  sql = sql || ' from bkdocs B join VATREGS V on (B.vatreg = V.symbol and B.company = V.company) join BKDOCTYPES T on (B.doctype = t.ref)';
  sql = sql ||' where B.period = '''||:periodid||''' and b.status > 0';-- and v.vtype in (3,4,5,6)';
  --<<PR73894: PL
  sql = sql ||'  and (v.vatregtype in (1,2)
    or (V.VATREGTYPE = 3
         and exists(select first 1 1 from bkvatpos join grvat on (grvat.symbol = bkvatpos.taxgr) where bkvatpos.bkdoc = B.ref and grvat.vatregtype in (1,2))))';
-->>
  if(coalesce(zakres,0)=1)then sql = sql ||' and t.creditnote = 0';
  else if(coalesce(zakres,0)=2)then sql = sql ||' and t.creditnote = 1';
  if(coalesce(zpopmies,0)>0)then sql = sql ||' and (b.vatperiod <> b.period)';
  if(coalesce(vreg,'')<>'')then sql = sql ||' and b.vatreg = '''||:vreg||'''';
  sql = sql ||' and B.company ='||:company||' order by B.vatreg, B.vatdate, B.number';
  for execute statement :sql

  /*for select B.REF, b.transdate, b.docdate, B.vatdate, B.vatreg, b.vnumber, b.bkreg, b.number,
              B.NIP, b.contractor, b.symbol
    from bkdocs B
    join VATREGS V on (B.vatreg = V.symbol and B.company = V.company)
    join BKDOCTYPES T on (B.doctype = t.ref)

    where
      B.period = :periodid and b.status > 1
      and v.vtype in (3,4,5,6)
      and ((:zakres = 0) or (:zakres = 1 and t.creditnote = 0) or (:zakres = 2 and t.creditnote = 1))
      and ((:zpopmies = 0) or (b.vatperiod <> b.period))
      and ((:vreg is null) or (:vreg = '') or (b.vatreg = :vreg))
      and B.company = :company
    order by B.vatreg, B.vatdate, B.number  */

  into :docref, :transdate, :docdate, :vatdate, :vatreg, :vatregnr,:bkreg, :bkregnr,
       :nip, :kontrahkod, :docsymbol, :vatregtype   --<<PR73894: PL>>
  do begin
    wartbru = 0;
    wartnet = 0;
    wartvate = 0;
    wartvatv = 0;
    WART22 = 0;
    VATE23 = 0;
    VATV23 = 0;
    VATN23 = 0;
    WART23 = 0;
    VATE22 = 0;
    VATV22 = 0;
    VATN22 = 0;
    WART12 = 0;
    VATE12 = 0;
    VATV12 = 0;
    VATN12 = 0;
    WART07 = 0;
    VATE07 = 0;
    VATV07 = 0;
    VATN07 = 0;
    WART08 = 0;
    VATE08 = 0;
    VATV08 = 0;
    VATN08 = 0;
    WART03 = 0;
    VATE03 = 0;
    VATV03 = 0;
    VATN03 = 0;
    WART05 = 0;
    VATE05 = 0;
    VATV05 = 0;
    VATN05 = 0;
    WART00 = 0;
    VATE00 = 0;
    VATV00 = 0;
    WARTZW = 0;
    VATZW = 0;
    WARTPOZ = 0;
    VATEPOZ = 0;
    VATVPOZ = 0;
    VATNPOZ = 0;
    print = 0;
    for select VATGR,sum(NETV), sum(VATV), sum(VATE),sum(VATE)-sum(VATV) as VATN
      from BKVATPOS
        join GRVAT on (GRVAT.SYMBOL = BKVATPOS.TAXGR)--<<PR73894: PL>>
      where bkdoc = :docref
        and (debittype = :vatdebittype or :vatdebittype = 4)
        and (:vatregtype in (1,2) or (:vatregtype = 3 and (grvat.vatregtype in (1,2))))--<<PR73894: PL>>
      group by VATGR
      into :vatgr, :netv, :vatv, :vate,:vatn
    do begin
        print = 1;
        if(:vatgr = '22') then begin
          WART22 = :wart22 + :netv;
          VATE22 = :vate22 + :vate;
          VATV22 = :vatv22 + :vatv;
          VATN22 = vate22-vatv22;
        end else if(:vatgr = '23') then begin
          WART23 = :wart23 + :netv;
          VATE23 = :vate23 + :vate;
          VATV23 = :vatv23 + :vatv;
          VATN23 = vate23- vatv23;
        end else if(:vatgr = '12') then begin
          WART12 = :wart12 + :netv;
          VATE12 = :vate12 + :vate;
          VATV12 = :vatv12 + :vatv;
          VATN12 = vate12-vatv12;
        end else if(:vatgr = '07') then begin
          WART07 = :wart07 + :netv;
          VATE07 = :vate07 + :vate;
          VATV07 = :vatv07 + :vatv;
          VATN07 = vate07-vatv07;
        end else if(:vatgr = '08') then begin
          WART08 = :wart08 + :netv;
          VATE08 = :vate08 + :vate;
          VATV08 = :vatv08 + :vatv;
          VATN08 = vate08-vatv08;
        end else if(:vatgr = '03') then begin
          WART03 = :wart03 + :netv;
          VATE03 = :vate03 + :vate;
          VATV03 = :vatv03 + :vatv;
          VATN03 = vate03-vatv03;
        end else if(:vatgr = '05') then begin
          WART05 = :wart05 + :netv;
          VATE05 = :vate05 + :vate;
          VATV05 = :vatv05 + :vatv;
          VATN05 = vate05-vatv05;
        end else if(:vatgr = '00') then begin
          WART00 = :wart00 + :netv;
          VATE00 = :vate00 + :vate;
          VATV00 = :vatv00 + :vatv;
        end else if(:vatgr = 'ZW') then begin
          WARTZW = :wartZW + :netv;
          VATZW = :vatZW + :vatv;
        end else begin
          WARTPOZ = :wartPOZ + :netv;
          VATEPOZ = :vatePOZ + vate;
          VATVPOZ = :vatvPOZ + vatv;
          VATNPOZ = vatepoz-vatvpoz;
        end
    end
    wartvatv = :vatv22 + :vatv23 + :vatv12 + :vatv07 + :vatv08 + :vatv03 + :vatv05 + :vatvpoz;
    wartvate = :vate22 + :vate23 + :vate12 + :vate07 + :vate08 + :vate03 + :vate05 + :vatepoz;
    wartvatn = wartvate-wartvatv;
    wartbru = :wartvate + :wart22 + :wart23 + :wart12 + :wart07 + :wart08 + :wart03 + :wart05 + :wart00 + :wartZW + :wartpoz;
    wartnet = :wart22 + :wart23 + :wart12 + :wart07 + :wart08 + :wart03 + :wart05 + :wart00 + :wartZW + :wartpoz;
    lp = :lp + 1;
    ref = :ref + 1;
    vate22 = :vate22 + :vate23;
    vatv22 = :vatv22 + :vatv23;
    vatn22 = vate22- vatv22;
    vate07 = :vate07 + vate08;
    vatv07 = :vatv07 + vatv08;
    vatn07 = vate07-vatv07;
    vate03 = :vate03 + :vate05;
    vatv03 = :vatv03 + :vatv05;
    vatn03 = vate03-vatv03;
    wart22 = :wart22 + :wart23;
    wart07 = :wart07 + :wart08;
    wart03 = :wart03 + :wart05;
    if (print = 1) then
      suspend;
  end
end^
SET TERM ; ^
