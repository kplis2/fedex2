--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REZ_SET_BLOKADA(
      POZREF integer,
      ILOSC numeric(14,4))
   as
declare variable ilblok numeric(14,4);
declare variable ilzmien numeric(14,4);
declare variable cnt integer;
begin
/*!!!uwaga bez brania pod uwage partii (cena, dostawa)!!!*/
   select ILOSC from STANYREZ where POZZAM=:POZREF and STATUS = 'Z' and ZREAL = 0 into :ilblok;
   if(:ilblok is null or :ilosc is null) then  exception REZ_WRONG 'Brak blokad do zwolnienia/nalożenia';
   if(:ilosc < :ilblok) then ilzmien = :ilosc;
   else ilzmien = :ilblok;
   if(:ilzmien > 0) then begin
      select count(*) from STANYREZ where POZZAM=:POZREF and STATUS = 'B' and ZREAL = 0 into :cnt;
      if(:cnt is null) then cnt = 0;
      if(:cnt > 0) then
        update STANYREZ set ILOSC = ILOSC + :ilzmien where POZZAM=:POZREF and STATUS = 'B' and ZREAL = 0;
      else insert into STANYREZ(POZZAM,STATUS,ZREAL, DOKUMMAG,
                        MAGAZYN, KTM, WERSJA, ZAMOWIENIE, DATA, ILOSC, CENA, DOSTAWA, DATABL,ONSTCEN, PRIORYTET)
                        select POZZAM, 'B', 0, 0,
                        MAGAZYN, KTM, WERSJA, ZAMOWIENIE, DATA, :ilzmien, CENA, DOSTAWA, DATABL, ONSTCEN, PRIORYTET from STANYREZ where
                           POZZAM = :POZREF and STATUS = 'Z' and zreal = 0;
      if(:ilzmien < :ilblok) then update STANYREZ set ILOSC = ILOSC - :ilzmien where POZZAM=:POZREF and STATUS = 'Z' and ZREAL = 0;
      else delete from STANYREZ where POZZAM=:POZREF and STATUS = 'Z' and ZREAL = 0;
   end
end^
SET TERM ; ^
