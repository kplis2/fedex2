--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_GET_CART_FROM_ORD(
      MWSORD INTEGER_ID)
  returns (
      SYMBOL STRING20)
   as
begin
  select first 1 m.symbol
    from mwsordcartcolours mc
      join mwsconstlocs m on (mc.cart = m.ref)
    where mc.mwsord = :mwsord
      and mc.status = 0
  into :symbol;
  -- Ldz wyciaganie symbolu wozka, przy WT
  if (symbol is null) then symbol = '';
  suspend;
end^
SET TERM ; ^
