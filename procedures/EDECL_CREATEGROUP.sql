--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDECL_CREATEGROUP(
      DECLREF integer,
      GROUPREF integer = null)
  returns (
      LOGMSG varchar(1024) CHARACTER SET UTF8                           ,
      RETGROUP integer)
   as
declare variable EYEAR integer;
declare variable EDECLDEF integer;
declare variable SCODE varchar(20);
declare variable GSCODE varchar(20);
declare variable GEYEAR integer;
declare variable NEWREF integer;
declare variable PERS varchar(200);
declare variable DECLGROUP integer;
declare variable STATUS integer;
declare variable PERSMSG varchar(255) = '';
declare variable GROUPNAME type of column EDECLARATIONS.GROUPNAME;
begin
/*Procedura tworzy grupe jako deklaracje zbiorcza, lub dodaje deklaracje do
  istniejacej już deklaracji zbiorczej. Opis parametrow:
    DECLREF - REF deklaracji do zgrupowania
    GROUPREF - REF grupy do ktorej należy dodać deklaracje
    LOGMSG - zwracany komunikat
    RETGROUP - w przypadku multiselekcji grupa jest tworzona na podstawie
      pierwszej deklaracji a reszta jest dolaczana do tej grupy. Zerowanie
      i ustawianie RETGROUP jest obsluzone rowniez w akcji grupujacej */

  select d.eyear, df.declgroup, df.systemcode, coalesce(p.person,''), d.declgroup, d.state, coalesce(d.groupname,'')
    from edeclarations d
      join edecldefs df on df.ref = d.edecldef
      left join persons p on d.person = p.ref
    where d.ref = :declref
    into eyear, edecldef, scode, pers, declgroup, status, groupname;

  if(groupref is not null) then
    select d.eyear, df.systemcode
      from edeclarations d
        join edecldefs df on df.ref = d.edecldef
      where d.ref = :groupref
      into geyear, gscode;

  if(pers <> '') then persmsg = pers;
  else if(groupname <> '') then persmsg = groupname;

  persmsg =  ' ' || scode || ' : ' || persmsg || ' - ';

  if(eyear < 2014) then
    logmsg = ascii_char(215) || persmsg || 'deklaracje zbiorcze można wysyłać od 2014 roku';
  else if(coalesce(declgroup,0) <> 0) then
    logmsg = ascii_char(164) || persmsg || 'deklaracja jest już zgrupowana';
  else if(coalesce(edecldef,0) = 0) then
    logmsg = ascii_char(215) || persmsg || 'nie istnieje definicja deklaracji zbiorczej';
  else if(status > 1) then
    logmsg = ascii_char(164) || persmsg || 'grupować można tylko deklaracje zatwierdzone lub redagowane';
  else if(exists(select first 1 1 from edeclarations where ref = :groupref and state >= 1)) then
    logmsg = ascii_char(164) || persmsg || 'dodawać można tylko do deklaracji zbiorczych w stanie redagowania';
  else if(geyear <> eyear or gscode <> scode) then
    logmsg = ascii_char(215) || persmsg || 'deklaracja grupowana i grupująca musi dotyczyć tego samego roku i typu';
  else begin

    if(exists(select first 1 1 from edeclarations where ref = :groupref and eyear = :eyear)) then
      newref = groupref;
    else
      insert into edeclarations (eyear, edecldef) values (:eyear,:edecldef)
        returning ref into :newref;
  
    retgroup = newref;
  
    update edeclarations
      set declgroup = :newref
      where ref = :declref;

    if (groupname = '') then
      select groupname from edeclarations where ref = :newref into :groupname;
  
    logmsg = ascii_char(149) || persmsg || 'przypisano do grupy ''' || groupname || '''';
  end

  if (newref is null)  then
    retgroup = groupref;

  suspend;

  when any do
  begin
    logmsg = ascii_char(215) || persmsg || 'nie udało się zgrupować deklaracji';
    suspend;
  end
end^
SET TERM ; ^
