--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_FAKTURUJDOKMAG_CHECK(
      DOKMAG integer)
  returns (
      HASKOR integer,
      HASOPKROZ integer)
   as
begin
  /* sprawdzenie, czy pozycje dokumentu mają powiazane pozycje korygujące, które sa na dokumentach niezafakturowanych*/
  HASKOR = 0;
  HASOPKROZ = 0;
  if(exists(select pozkor.ref from DOKUMPOZ POZDOK
      left join DOKUMPOZ POZKOR on (POZKOR.KORTOPOZ = POZDOK.REF)
      left join DOKUMNAG on (POZKOR.DOKUMENT = DOKUMNAG.REF)
      where POZDOK.DOKUMENT   = :DOKMAG
       and POZKOR.REF is not null
       and DOKUMNAG.FAKTURA is null and DOKUMNAG.AKCEPT > 0)
  ) then haskor = 1;
  else haskor = 0;
  /*sprawdzenie, czy jesli dokument jest naliczjacy opakowania,
    to czy do jego dokumrozow są jakies dokumrozy rozliczajace*/
    select count(*)
    from DOKUMROZ ROZKOR
    join STANYOPK on (STANYOPK.ref = ROZKOR.opkrozid)
    join DOKUMPOZ on (DOKUMPOZ.REF = STANYOPK.POZDOKUM)
    join DOKUMNAG on (DOKUMNAG.REF = DOKUMPOZ.DOKUMENT and DOKUMNAG.AKCEPT > 0)
    where DOKUMPOZ.DOKUMENT = :dokmag
    into :hasopkroz;
  if(:hasopkroz > 0) then hasopkroz = 1;
  else hasopkroz = 0;
end^
SET TERM ; ^
