--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TEST_BS74350(
      VALIN numeric(14,4))
  returns (
      VALOUT numeric(14,4))
   as
begin
  valout = :valin + 1;
  suspend;
end^
SET TERM ; ^
