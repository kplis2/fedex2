--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_MOVE_BOX_CART(
      KODKRESK STRING40,
      REF INTEGER_ID,
      LOK STRING40)
   as
begin
-- przesuwanie kartonów i wózków
  if (kodkresk = '#BOX#' or kodkresk = 'BOX' ) then
    begin
      update listywysdroz_opk l
        set l.x_mwsconstlock = :lok
        where l.ref = :ref;
    end
  else if (kodkresk = '#CART#') then
    begin
      update mwsordcartcolours m
        set m.mwsconstlocsymb = :lok
        where m.cart = :ref
          and m.status <0 ;
    end
end^
SET TERM ; ^
