--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDES_STATUSCHANGE(
      PRSCHEDGUIDESREF integer,
      STATUSCHANGETO smallint,
      STATUSMATCHANGETO smallint)
   as
declare variable status smallint;
declare variable statusmat smallint;
declare variable symboldok varchar(20);
declare variable source smallint;
declare variable msg varchar(255);
declare variable usluga integer;
begin
  if(statuschangeto is not null and statusmatchangeto is not null and statuschangeto >0 and statusmatchangeto >0) then
    exception prschedguides_error 'Niedozwolona jednoczesna zmiana statusu glownego i materialowego';
  select status, statusmat, usluga
    from prschedguides p
      join towary t on (t.ktm = p.ktm)
    where p.ref = :prschedguidesref
  into :status, :statusmat, :usluga;

  if ((:status = :statuschangeto and :statuschangeto is not null)
    or (:statusmat = :statusmatchangeto and :statusmatchangeto is not null)
  ) then
    exit; -- nic sie nie zmiania

  symboldok = null;
  source = null;
  if (statuschangeto is not null) then
  begin
    statusmatchangeto = null;
    if (statuschangeto = 0) then
    begin
      select min(d.ssource)
        from prschedguidedets d
        where d.prschedguide = :prschedguidesref and d.out = 1
          and d.accept > 0 and d.quantity > 0
      into :source;
      if (source is not null) then
      begin
        msg = 'Wycofanie realizacji przew. niemożliwe. ';
        if (source = 0) then
          select max(n.symbol)
            from prschedguidedets d
              left join dokumpoz p on (p.prschedguidedet = d.ref)
              left join dokumnag n on (n.ref = p.dokument)
            where d.prschedguide = :prschedguidesref and d.out = 1 and d.accept > 0
              and d.ssource = :source and d.quantity > 0
            into symboldok;
        if (symboldok is not null) then
          msg = msg||'Istnieje dok. mag. '||symboldok;
        else
          msg = msg||'Istnieją realizacje';
        exception prschedguides_error substring(msg from 1 for 70);
      end
      statusmatchangeto = 0;
    end
    if(statuschangeto < 2) then
    begin
      select min(d.ssource)
        from prschedguidedets d
        where d.prschedguide = :prschedguidesref and d.out = 0
          and d.accept > 0 and d.quantity > 0
          and coalesce(d.shortage,0) = 0
      into :source;
      if (source is not null) then
      begin
        msg = 'Wycofanie realizacji przew. niemożliwe. ';
        if (source = 0) then
          select max(n.symbol)
            from prschedguidedets d
              left join dokumpoz p on (p.prschedguidedet = d.ref)
              left join dokumnag n on (n.ref = p.dokument)
            where d.prschedguide = :prschedguidesref and d.out = 0 and d.accept > 0
              and d.ssource = :source and d.quantity > 0
            into symboldok;
        if (symboldok is not null) then
          msg = msg||'Istnieje dok. mag. '||symboldok;
        else
          msg = msg||'Istnieją realizacje';
        exception prschedguides_error substring(msg from 1 for 70);
      end
    end
    else if (:statuschangeto = 2 and :usluga = 1) then
      statuschangeto = 3;
    update prschedguides set status = :statuschangeto where ref = :prschedguidesref;
    if(statusmatchangeto is not null) then
      update prschedguides set statusmat = :statusmatchangeto where ref = :prschedguidesref;
  end
  if(statusmatchangeto is not null) then
  begin
    if (statusmatchangeto = 0) then
    begin
    end else if (statusmatchangeto = 1) then
    begin
    end else if (statusmatchangeto = 2) then
    begin
    end else if (statusmatchangeto = 3) then
    begin
    end else if (statusmatchangeto = 4) then
    begin
      if(status < 2) then
        exception prschedguides_error 'Przewodnik niezrealizowany. Zamknięcie przyjęcia wyrobów niedozwolone';
      select max(d.ssource)
        from prschedguidedets d
        where d.prschedguide = :prschedguidesref and d.out = 0 and d.accept > 0 and d.quantity > 0
        into source;
      if (:symboldok is null) then
        exception prschedguides_error 'Brak przyjęcia do przewodnika. Zamknięcie przyjęcia niedozwolone';
    end
    update prschedguides set statusmat = :statusmatchangeto where ref = :prschedguidesref;
  end
end^
SET TERM ; ^
