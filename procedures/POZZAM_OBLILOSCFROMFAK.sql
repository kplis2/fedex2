--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZZAM_OBLILOSCFROMFAK(
      POZZAMREF integer)
   as
DECLARE VARIABLE GRUPADOK INTEGER;
DECLARE VARIABLE FAKREF INTEGER;
DECLARE VARIABLE NUMER INTEGER;
DECLARE VARIABLE ILONZAM NUMERIC(14,4);
DECLARE VARIABLE ILZREALONZAM NUMERIC(14,4);
DECLARE VARIABLE ILOSC NUMERIC(14,4);
DECLARE VARIABLE KTM VARCHAR(40);
begin
  select NAGFAK.REF
  from POZZAM
  left join NAGZAM on (POZZAM.zamowienie = NAGZAM.REF)
  left join NAGFAK on (NAGZAM.FAKTURA = NAGFAK.REF)
  where POZZAM.REF=:pozzamref and NAGZAM.FAKDEPEND = 1
  into :grupadok;
  if(:grupadok > 0) then begin
    select max(REF) from NAGFAK where GRUPADOK = :grupadok and AKCEPTACJA = 1 into :fakref;
    if(:fakref > 0) then begin
      select numer from POZFAK where DOKUMENT = :grupadok and FROMPOZZAM = :pozzamref into :numer;
      select ILOSC from POZFAK where DOKUMENT = :fakref and NUMER = :numer into :ilosc;
      select ilosc,ilzreal, ktm from POZZAM where REF=:pozzamref into :ilonzam, :ilzrealonzam ,:ktm;
      if(:ilosc < :ilzrealonzam) then ilosc = :ilzrealonzam ;
      if(:ilosc <> :ilonzam) then
        update POZZAM set ILOSC = :ilosc where ref=:pozzamref;
    end
  end
end^
SET TERM ; ^
