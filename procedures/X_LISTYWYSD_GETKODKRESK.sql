--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_LISTYWYSD_GETKODKRESK(
      KODKRESK KODKRESKOWY,
      SPOSDOSTREF integer)
  returns (
      TRYB smallint,
      TYPOPK integer,
      MSG STRING255)
   as
begin
/* tryb - tryb dalszej pracy
0 - nic nie rob
1 - otworz domyslne opakowanie dla wybranego sposobu dostawy
2 - otworz opakowanie wg kod OPK-XX#, mozliwosc otwierania wielu opakowan
9 - zamkniecie aktywnego opakowania, w danym momencie moze byc tylko jedno otwarte opakowanie
10 - koniec pakowania
*/
/* typopk
ref typu opakowania jesli zostal uzyty odpowiedni kod kreskowy
*/
  tryb = 0;
  typopk = 0;
  msg = '';

  if (coalesce(:kodkresk,'') = '') then
  begin
    msg = 'Nie podano kodu kreskowego';
  end
  else
  begin
    --sprawdzenie czy kod kreskowy nie jest jednym z technicznych kodow
    if (:kodkresk = 'STARTPAK') then --rozpoczecie pakowania
      msg = 'Jesteś już w trybie pakowania';
    --na razie nie bedziemy uzywali komendy opkotw
    /*else if (:kodkresk = 'OPKOTW') then --otwarcie domyslnej paczki dla sposobu dostawy
    begin
      tryb = 1;
      select typopk from sposdost where ref = :sposdostref
      into :typopk;
    end */
    else if (:kodkresk = 'OPKZAMK') then --zamkniecie aktywnego opakowania
      tryb = 9;
    else if (:kodkresk = 'KONIECPAK') then
      tryb = 10;

    --sprawdzenie czy kod kreskowy nie jest kodem opakowania
    if (:tryb = 0 and :msg = '') then
    begin
      select ref from typyopk where (kodkresk = :kodkresk or symbol = :kodkresk)
      into :typopk;
      if (:typopk is null) then typopk = 0;
      if (:typopk > 0) then
        tryb = 1; --zwracamy 1 gdyz wykonujemy ta sama operacje co w przypadku kodu OPKOTW
    end

    if (:tryb = 0 and :msg = '' and
        substring(:kodkresk from  1 for  3) = 'OPK' and substring(:kodkresk from  4 for 4) = '-' and
        substring(:kodkresk from  coalesce(char_length(:kodkresk),0) for coalesce(char_length(:kodkresk),0)) = '#') then
    begin
      tryb = 2;
      typopk = 0;
    end
  end
end^
SET TERM ; ^
