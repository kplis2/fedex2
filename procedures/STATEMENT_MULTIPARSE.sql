--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STATEMENT_MULTIPARSE(
      SOURCE varchar(1024) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      DECL varchar(1024) CHARACTER SET UTF8                           ,
      BODY varchar(1024) CHARACTER SET UTF8                           ,
      LASTVAR integer,
      DECLTYPE integer)
  returns (
      RDECL varchar(1024) CHARACTER SET UTF8                           ,
      RBODY varchar(1024) CHARACTER SET UTF8                           ,
      RLASTVAR integer)
   as
declare variable ndx integer;
declare variable localdecl varchar(8191);
declare variable localproc varchar(8191);
declare variable c varchar(1);
declare variable a integer;
declare variable eol varchar(2);
declare variable l integer;
begin
--decltype=1 - numeric(14,4), decltype=2 - varchar(255)
  eol = '
';
  ndx = 1;
  l = coalesce(char_length(source),0); -- [DG] XXX ZG119346
  localdecl = '';
  /* deklaracje zmiennych przenies do czesci decl */
  while(substring(:source from :ndx for 16)='declare variable') do begin     --substring(:source from :ndx for :ndx+15)
    c = substring(:source from :ndx for 1);       --substring(:source from :ndx for :ndx)
    while(:c<>';') do begin
      localdecl = :localdecl || :c;
      ndx = :ndx+1;
      c = substring(:source from :ndx for 1);     --substring(:source from :ndx for :ndx);
    end
    a = ascii_val(:c);
    while((:c=';') or (:c=' ') or (:a=13) or (:a=10)) do begin
      localdecl = :localdecl || :c;
      ndx = :ndx+1;
      c = substring(:source from :ndx for 1);   --substring(:source from :ndx for :ndx)
      a = ascii_val(:c);
    end
    localdecl = :localdecl || :eol;
  end

  execute procedure STATEMENT_MULTIPARSE_LINE(substring(:source from :ndx for (1+:l-:ndx)),:prefix,:decl,'',:body,:lastvar,'', :decltype)  --substring(:source from :ndx for :l)
    returning_values :decl,:localproc,:body,:lastvar,:ndx;

  rdecl = :localdecl||:decl;
  rbody = :localproc||:body;
  rlastvar = :lastvar;
end^
SET TERM ; ^
