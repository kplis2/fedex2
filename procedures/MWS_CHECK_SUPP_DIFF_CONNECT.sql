--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CHECK_SUPP_DIFF_CONNECT(
      MWSORD integer)
  returns (
      STATUS smallint)
   as
declare variable docgroup integer;
declare variable docid integer;
begin
  status = 1;

  select docgroup, docid from mwsords where ref = :mwsord
  into :docgroup, docid;
  if (:docgroup is null) then
    docgroup = :docid;

  if (exists(select first 1 1 from mwsords where docgroup = :docgroup and ref <> :mwsord and status < 5)) then
    status = 0;
  if (:status = 0) then exit;

  if (exists(select first 1 1
              from dokumnag dn
                left join dokumpoz dp on (dn.ref = dp.dokument)
              where dn.grupasped = :docgroup
                and dn.mwsdisposition = 0
                and dp.ilosc > dp.ilosconmwsacts)) then
    status = 0;
end^
SET TERM ; ^
