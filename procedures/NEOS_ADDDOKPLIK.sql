--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NEOS_ADDDOKPLIK(
      AKTUOPER integer,
      DOKZLACZTABLE varchar(20) CHARACTER SET UTF8                           ,
      DOKZLACZREF integer,
      SEGREGATOR integer)
  returns (
      REFDOKPLIK integer)
   as
declare variable DEFAULTTYP integer;
declare variable GRUPY varchar(1024);
declare variable ADMINISTRATOR smallint;
declare variable SQL varchar(2048);
begin
  --exception test_break aktuoper||';'||DOKZLACZTABLE||';'||DOKZLACZREF||';'||:segregator;
  refdokplik = 0;
  aktuoper = coalesce(:aktuoper,0);
  dokzlacztable = coalesce(:dokzlacztable,'');
  dokzlaczref = coalesce(:dokzlaczref,0);
  if (:aktuoper = 0) then exit;
  select o.administrator, o.grupa from operator o where o.ref = :aktuoper
     into :administrator, :grupy;
  administrator = coalesce(:administrator,0);
  grupy = coalesce(:grupy,'');
  segregator = coalesce(:segregator,0);

  sql = 'select first 1 dts.dokpliktyp, dts.segregator
    from dokpliktypsegregator dts
    join dokpliktyp dt on (dts.dokpliktyp = dt.ref)
    join segregator s on (dts.segregator = s.ref)
    where (dt.rights = '';'' and dt.rightsgroup = '';''
         or dt.rights like ''%%;'||:aktuoper||';%%''
         or strmulticmp('';'||:grupy||';'', dt.rightsgroup) = 1)
         and (s.rights = '';'' and s.rightsgroup = '';''
         or s.rights like ''%%;'||:aktuoper||';%%''
         or strmulticmp('';'||:grupy||';'', s.rightsgroup) = 1)';
  if (:segregator > 0) then
    sql = :sql || ' and dts.segregator = '||:segregator;
  execute statement :sql
    into :defaulttyp, :segregator;
  --if (coalesce(:defaulttyp,0)=0 or coalesce(:segregator,0)=0) then exit;

  if (coalesce(:defaulttyp,0)=0) then
   exception universal 'Nie znaleziono żadnego typu dokumentu przypisanego do tego segregatora.';


  execute procedure gen_ref('DOKPLIK') returning_values :refdokplik;
  insert into dokplik (REF,SYMBOL,TYP,SEGREGATOR, BLOKADA,  OPERBLOK, OPERATOR, DATADOK)
   values(:refdokplik,'',:defaulttyp,:segregator,1,:aktuoper, :aktuoper, current_timestamp(0));

  if (:dokzlacztable <> '' and :dokzlaczref > 0) then
    insert into dokzlacz(ZTABLE,ZDOKUM,DOKUMENT)
     values(:dokzlacztable,:dokzlaczref, :refdokplik);
  suspend;
end^
SET TERM ; ^
