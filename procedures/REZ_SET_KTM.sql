--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REZ_SET_KTM(
      ZAM integer,
      REFPOZ integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      ILOSC numeric(14,4),
      CENA numeric(14,4),
      DOSTAWA integer,
      STAN varchar(2) CHARACTER SET UTF8                           ,
      TERMIN timestamp)
  returns (
      STATUS integer)
   as
DECLARE VARIABLE REZSTAN CHAR(1);
DECLARE VARIABLE REZIL NUMERIC(14,4);
DECLARE VARIABLE IL NUMERIC(14,4);
DECLARE VARIABLE FREEIL NUMERIC(14,4);
DECLARE VARIABLE MAGAZYN CHAR(3);
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE WYDANIA SMALLINT;
DECLARE VARIABLE ILTYMBLOK NUMERIC(14,4);
DECLARE VARIABLE NOTROZPISZ SMALLINT;
DECLARE VARIABLE DOROZPISZ SMALLINT;
DECLARE VARIABLE POZZAMREF INTEGER;
DECLARE VARIABLE ONMAINSCHEDULE INTEGER;
declare variable fake smallint;
declare variable altposmode smallint;
begin
  status = -2;
  dorozpisz = 0;
  pozzamref = :refpoz;
  if(:ktm='' or :ktm is null) then exception rez_wrong 'Nie podano KTMu. Nie można naliczyć rezerwacji ani blokad towarowych!';

  select coalesce(p.fake,0), coalesce(t.altposmode,0)
    from pozzam p
      left join pozzam p1 on (p1.ref = p.alttopoz)
      join towary t on (p1.ktm = t.ktm)
    where p.ref = :refpoz
      and coalesce(p.fake,0) = 1
      and t.usluga <> 1
  into :fake, :altposmode;
  if (:fake = 1 and :altposmode = 1) then
  begin
    status = 1;
    exit;
  end

  select TYPZAM.WYDANIA
    from NAGZAM
      left join TYPZAM on (NAGZAM.typzam = TYPZAM.symbol)
    where NAGZAM.ref = :zam
    into :wydania;
  if (wydania = 3) then
  begin
    select coalesce(s.main,0)
      from prschedzam z
        left join prschedules s on (s.ref = z.prschedule)
      where z.zamowienie = :zam
      into onmainschedule;
  end
  if (onmainschedule is null) then onmainschedule = 0;
  if (:wydania is null) then wydania = 0;
  notrozpisz = 0;
/*  if(:wydania = 0) then exit; *//* liczymy rezerwacje tylko dla zamowien wydania */
  if (:ilosc is null) then ilosc = 0;
  status = -1;
  if (:stan = 'W') then begin
    notrozpisz = 1;
    stan = 'N';
  end
  if(coalesce(char_length(:stan),0)=2) then begin -- [DG] XXX ZG119346
    notrozpisz = 1;
    stan = substring(:stan from 1 for 1);
    exception REZ_SET_KTM_EXPT 'REZ_SET_KTM'||:stan;
  end

  /* zmniejszenie ilosci o ilosci na rezerawacjach/blokadach juz zrealizowanych */
  select sum(ilosc) from STANYREZ where ZAMOWIENIE = :zam and POZZAM=:refpoz AND ZREAL=1 into :il;
  if(:il is null) then il = 0;
  ilosc = ilosc - il;
  if(:ilosc < 0) then ilosc = 0;
  if((:ilosc = 0) or (:stan = 'Z'))then stan = 'N';
   /*stan 'Z' to tak na prawd stan bez rezerwacji i blokad, oznaczany w algorytmie jako 'N', co ma tez miejsce przy ilosci zero */
  if(:ktm is null) then begin
    exit;
  end
  if(:refpoz = 0) then -- rezerwacja naglowka zamowienia
    select MAGAZYN, 0 from NAGZAM where REF=:zam into :magazyn, :iltymblok;
  else if(:refpoz < 0) then begin--rezerwacja naglowka
    pozzamref = 0;
    select prschedguides.warehouse, 0 from prschedguides where ref=-:refpoz into :magazyn, :iltymblok;
  end else if(:onmainschedule = 1) then begin--rezerwacja pozycji przewodnika PRSCHEDGUIDESPOS
    select WAREHOUSE, POZZAMREF, 0 from prschedguidespos where REF=:refpoz into :magazyn, :pozzamref, :iltymblok;
  end else
    select MAGAZYN,ILTYMBLOK from POZZAM where ref=:refpoz into :magazyn,:iltymblok;
  if(:magazyn is null or (:magazyn = '')) then begin
    exit;
  end

  if(:iltymblok is null) then iltymblok = 0;
  if(:wersja is null) then exit;
  rezil = null;
  /* zawsze jeden rekord klucza POZZAM;STAN;ZREAL */
  /*odjecie lub zmiana ilosci, ktore juz sa w danym stanie */
  select ILOSC from STANYREZ where ZAMOWIENIE = :zam and POZZAM=:refpoz AND STATUS=:stan AND ZREAL = 0 into :rezil;
  if(:rezil is not null) then begin
   if(:ilosc = 0) then
     delete from STANYREZ where ZAMOWIENIE = :zam and POZZAM=:refpoz AND ZREAL = 0 AND STATUS = :stan;
   else if(:stan = 'B' and :iltymblok > 0) then begin
     freeil = :rezil - :iltymblok;
     if(:freeil < 0) then freeil = 0;
     if(:freeil = 0) then begin
         delete from  STANYREZ where ZAMOWIENIE = :zam and POZZAM=:refpoz AND ZREAL=0 AND STATUS=:stan;
     end else begin
         update STANYREZ set ILOSC = :freeil where ZAMOWIENIE = :zam and POZZAM=:refpoz AND ZREAL=0 AND STATUS=:stan;
     end
     ilosc = :ilosc - :freeil;
   end else if(:rezil > :ilosc) then begin
     update STANYREZ set ILOSC = :ilosc where ZAMOWIENIE = :zam and POZZAM=:refpoz AND ZREAL=0 AND STATUS=:stan;
     if(:stan = 'B' and :notrozpisz = 0) then   begin
       /*rozpisanei statusów typu 'Z'*/
       dorozpisz = 1;
     end
     ilosc = 0;
   end else ilosc = :ilosc - :rezil;
  end
  /* zamiana mozliwych zapisow o innych statusach niz zadany - przejscie statusu  */
  for select STATUS, ILOSC from STANYREZ where ZAMOWIENIE = :zam and POZZAM=:refpoz AND ZREAL=0 AND STATUS <> :stan
  order by STATUS desc /*by najpierw kasować Z, a potem B*/
  into :rezstan, :rezil
  do begin
    if(:rezil is null) then rezil = 0;
    if(:ilosc >0 and  :rezil > 0) then begin
      if(:ilosc > :rezil ) then il = rezil;
      else il = ilosc;
      /* zmiana stanu rezerwacjji lub dopisanie do istniejącej i skasowanie tej*/
      cnt = null;
      select count(*) from STANYREZ where ZAMOWIENIE = :zam and STATUS = :stan AND ZREAL = 0 AND POZZAM=:refpoz into :cnt;
      if(:cnt is null) then cnt = 0;
      if(:stan = 'B') then begin
          /* badanie ilosci dostepnej na magazynie */
          freeil = null;
          execute procedure REZ_GET_FREE_STAN(:magazyn, :ktm, :WERSJA, :cena, :dostawa) returning_values :freeil;
          if(:freeil is null) then freeil = 0;
          if(:iltymblok > 0) then begin
            if(:iltymblok > :freeil) then begin
              iltymblok = :iltymblok - :freeil;
              freeil = 0;
            end else begin
              freeil = :freeil - :iltymblok;
              iltymblok = 0;
            end
          end
          if(:freeil > 0) then begin
             if(:freeil > :il) then freeil = il;
             if(:cnt > 0) then begin
               if(:freeil = :il) then
                  delete from STANYREZ where ZAMOWIENIE = :zam and STATUS=:rezstan AND POZZAM=:refpoz AND ZREAL=0;
               update STANYREZ set ILOSC = ILOSC + :freeil where ZAMOWIENIE = :zam and STATUS=:stan AND POZZAM=:refpoz AND ZREAL=0;
             end else if(:freeil = :il) then
               update STANYREZ set STATUS=:STAN, ILOSC = :freeil where ZAMOWIENIE = :zam and STATUS=:rezstan AND POZZAM=:refpoz AND ZREAL=0;
             else
               insert into STANYREZ(ZAMOWIENIE, POZZAM, STATUS, ZREAL, DOKUMMAG, MAGAZYN, KTM, WERSJA, DATA, ILOSC, CENA, DOSTAWA, DATABL)
                 values(:ZAM,:REFPOZ, :stan, 0, 0,:magazyn, :ktm, :wersja,:termin,:freeil,:cena, :dostawa, current_timestamp(0));
             il = il - freeil;
             ilosc = ilosc - freeil;
          end
          if(:il > 0) then begin
            stan = 'Z';
            cnt = null;
            select count(*) from STANYREZ where ZAMOWIENIE = :zam and STATUS=:stan AND POZZAM=:refpoz AND ZREAL=0 into :cnt;
            if(:cnt is null) then cnt = 0;
            if(:cnt > 0 ) then begin
              if(:stan <> :rezstan ) then
               delete from STANYREZ where ZAMOWIENIE = :zam and STATUS=:rezstan AND POZZAM=:refpoz AND ZREAL=0;
              update STANYREZ set ILOSC = :il where ZAMOWIENIE = :zam and STATUS=:stan AND POZZAM=:refpoz AND ZREAL=0;
            end else begin
               update STANYREZ set STATUS=:stan, ILOSC = :il where ZAMOWIENIE = :zam and STATUS=:rezstan AND POZZAM=:refpoz AND ZREAL=0;
            end
          end
      end else begin
          /* stany R,N */
        if(:cnt > 0) then begin
          if(:stan <> :rezstan ) then
            delete from STANYREZ where ZAMOWIENIE = :zam and STATUS=:rezstan AND POZZAM=:refpoz AND ZREAL=0;
          update STANYREZ set ILOSC = ILOSC + :il where ZAMOWIENIE = :zam and STATUS=:STAN AND POZZAM=:refpoz AND ZREAL=0;
        end else begin
          update STANYREZ set STATUS=:stan, ILOSC = :il where ZAMOWIENIE = :zam and STATUS=:rezstan AND POZZAM=:refpoz AND ZREAL=0;
        end
      end
      ilosc = ilosc - il;
    end else begin
       /* ilosc zerowa - kasowanie rezerwacji lub ustawienie na zero */
       delete from STANYREZ where ZAMOWIENIE = :zam and STATUS=:rezstan AND POZZAM=:refpoz AND ZREAL = 0;
       if(:rezstan = 'B' and :notrozpisz = 0 and :iltymblok = 0) then begin
         /*rozpisanei statusów typu 'Z'*/
         dorozpisz = 1;
       end
    end
  end
  if(:ilosc > 0) then begin
      il = ilosc;
      cnt = null;
      select count(*) from STANYREZ where ZAMOWIENIE = :zam and STATUS = :stan AND ZREAL = 0 AND POZZAM=:refpoz into :cnt;
      if(:cnt is null) then cnt = 0;
      if(:stan = 'B') then begin
    /* badanie ilosci dostepnej na magazynie */
          execute procedure REZ_GET_FREE_STAN(:magazyn, :ktm, :WERSJA, :cena, :dostawa) returning_values :freeil;
          if(:freeil is null) then freeil = 0;
          if(:iltymblok > 0) then begin
            if(:iltymblok > :freeil) then begin
              iltymblok = :iltymblok - :freeil;
              freeil = 0;
            end else begin
              freeil = :freeil - :iltymblok;
              iltymblok = 0;
            end
          end
          if(:freeil > 0) then begin
             if(:freeil > :il) then freeil = il;
             if(:cnt > 0) then
               update STANYREZ set ILOSC = ILOSC + :freeil where ZAMOWIENIE = :zam and STATUS=:stan AND POZZAM=:refpoz AND ZREAL=0;
             else begin
               insert into STANYREZ(ZAMOWIENIE, POZZAM, STATUS, ZREAL, DOKUMMAG, MAGAZYN, KTM, WERSJA, DATA, ILOSC, CENA, DOSTAWA, DATABL)
                 values(:ZAM,:REFPOZ, :stan, 0, 0,:magazyn, :ktm, :wersja,:termin,:freeil,:cena, :dostawa, current_timestamp(0));
             end
             il = il - freeil;
          end
          if(:il > 0) then begin
            stan = 'Z';
            cnt = null;
            select count(*) from STANYREZ where ZAMOWIENIE = :zam and STATUS=:stan AND POZZAM=:refpoz AND ZREAL=0 into :cnt;
            if(:cnt is null) then cnt = 0;
            if(:cnt > 0 ) then begin
               update STANYREZ set ILOSC = ILOSC + :il where ZAMOWIENIE = :zam and STATUS=:stan AND POZZAM=:refpoz AND ZREAL=0;
            end else
               insert into STANYREZ(ZAMOWIENIE, POZZAM, STATUS, ZREAL, DOKUMMAG, MAGAZYN, KTM, WERSJA, DATA, ILOSC, CENA, DOSTAWA, DATABL)
                 values(:ZAM,:REFPOZ, :stan, 0, 0,:magazyn, :ktm, :wersja,:termin,:il,:cena, :dostawa, current_timestamp(0));
          end
      end else begin
          /* stany R,N */
        if(:cnt > 0) then begin
          update STANYREZ set ILOSC = ILOSC + :il where STATUS=:STAN AND POZZAM=:refpoz AND ZREAL=0;
        end else begin
               insert into STANYREZ(ZAMOWIENIE, POZZAM, STATUS, ZREAL, DOKUMMAG, MAGAZYN,
                                    KTM, WERSJA, DATA, ILOSC, CENA, DOSTAWA, DATABL)
                 values(:ZAM,:REFPOZ, :stan, 0, 0,:magazyn,
                        :ktm, :wersja,:termin,:il,:cena, :dostawa, current_timestamp(0));
        end
      end
      ilosc = ilosc - il;
  end
  /* usuniecie blokad typu 'N'  */
  delete from STANYREZ  where ZAMOWIENIE = :zam and STATUS='N' AND POZZAM=:refpoz AND ZREAL=0;
  if(:dorozpisz = 1) then
     execute procedure REZ_BLOK_ROZPISZ(:magazyn, :ktm, :wersja);
  update POZZAM set ILTYMBLOK = 0 where REF=:refpoz and ILTYMBLOK > 0;
  STATUS = 1;
end^
SET TERM ; ^
