--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DECREEMATCHINGGROUP_CALC(
      DECREEMATCHINGGROUP DECREEMATCHINGGROUPS_ID)
   as
declare variable status smallint_id;
declare variable matchingdate date_id;
declare variable matchingperiod period_id;
declare variable sumcredit ct_amount;
declare variable sumdebit dt_amount;
declare variable cnt integer_id;
begin
  status = 0;
  select count(m.ref), sum(m.credit), sum(m.debit)
    from decreematchings m
    where m.decreematchinggroup = :decreematchinggroup
    into :cnt, :sumcredit, :sumdebit;
  if (:sumcredit is null) then sumcredit = 0;
  if (:sumdebit is null) then sumdebit = 0;
  if (:sumcredit = :sumdebit and :sumcredit > 0) then
    status = 1;
  if (status > 0) then
  begin
    select max(d.period), max(d.transdate)
      from decreematchings m
        left join decrees d on (d.ref = m.decree)
      where m.decreematchinggroup = :decreematchinggroup
      into matchingperiod, matchingdate;
  end
  if (:cnt = 0) then
    delete from decreematchinggroups g where g.ref = :decreematchinggroup;
  else
  begin
    update decreematchinggroups g set g.matchingdate = :matchingdate, g.matchingperiod = :matchingperiod, g.status = :status
      where g.ref = :decreematchinggroup;
    update decreematchings m set m.blockcalc = 0 where m.decreematchinggroup = :decreematchinggroup;
  end
end^
SET TERM ; ^
