--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXP2PLATNIK_RCA(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      COMPANY integer,
      FORPERSON integer = 0)
  returns (
      ROWNUMBER integer,
      PERSON integer,
      SNAME varchar(60) CHARACTER SET UTF8                           ,
      FNAME varchar(60) CHARACTER SET UTF8                           ,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      DOCNO varchar(11) CHARACTER SET UTF8                           ,
      B01 varchar(6) CHARACTER SET UTF8                           ,
      B02 smallint,
      B03L smallint,
      B03M smallint,
      B04 numeric(14,2),
      B05 numeric(14,2),
      B06 numeric(14,2),
      B07 numeric(14,2),
      B08 numeric(14,2),
      B09 numeric(14,2),
      B10 numeric(14,2),
      B11 numeric(14,2),
      B12 numeric(14,2),
      B13 numeric(14,2),
      B14 numeric(14,2),
      B15 numeric(14,2),
      B16 numeric(14,2),
      B17 numeric(14,2),
      B18 numeric(14,2),
      B19 numeric(14,2),
      B20 numeric(14,2),
      B21 numeric(14,2),
      B22 numeric(14,2),
      B23 numeric(14,2),
      B24 numeric(14,2),
      B25 numeric(14,2),
      B26 numeric(14,2),
      B27 numeric(14,2),
      B28 numeric(14,2),
      C01 numeric(14,2),
      C02 numeric(14,2),
      C03 numeric(14,2),
      C04 numeric(14,2),
      C05 numeric(14,2),
      D01 numeric(14,2),
      D02 numeric(14,2),
      D03 numeric(14,2),
      D04 numeric(14,2))
   as
declare variable fromdate date;
declare variable todate date;
declare variable zatrudniony smallint;
declare variable b1b varchar(2);
declare variable dni_macierzynskiego smallint;
declare variable dni_wychowawczego smallint;
declare variable dni_macierzynskiego_tmp smallint;
declare variable dni_wychowawczego_tmp smallint;
declare variable dni_kalendarzowe smallint;
declare variable ascode integer;
declare variable prev_todate date;
declare variable prev_fromdate date;
declare variable cper varchar(6);
declare variable b04_ucp numeric(14,2);
declare variable b05_ucp numeric(14,2);
declare variable b06_ucp numeric(14,2);
declare variable b07_ucp numeric(14,2);
declare variable b08_ucp numeric(14,2);
declare variable b09_ucp numeric(14,2);
declare variable b11_ucp numeric(14,2);
declare variable b12_ucp numeric(14,2);
declare variable b14_ucp numeric(14,2);
declare variable b27_ucp numeric(14,2);
declare variable b28_ucp numeric(14,2);
declare variable c01_ucp numeric(14,2);
declare variable c04_ucp numeric(14,2);
declare variable d01_ucp numeric(14,2);
declare variable d02_ucp numeric(14,2);
declare variable d03_ucp numeric(14,2);
declare variable d04_ucp numeric(14,2);
declare variable b01_ucp varchar(6);
declare variable employee integer;
declare variable afromdate date;
declare variable atodate date;
declare variable premium numeric(14,2);
declare variable adays integer;
declare variable zl_todate date;
declare variable t_b01 varchar(6);
declare variable t_b02 smallint;
begin
/*MWr: Personel - Eksport do Platnika deklaracji rozliczeniowej ZUS RCA (ver.xml od 2014)
    Parametr FORPERSON jest wykorzystywany przy wydruku RMUA, pozwala wykonac deklaracje
    dla danej osoby (otrzymanie ROWNUMBER < 0 oznacza brak eksportu do Platnika) */

  if (exists(select first 1 p.pvalue from epayrolls r --nic nie zwracamy, kiedy brak danych (wymagane przez kod zrodlowy)
        join eprpos p on (p.payroll = r.ref and r.iper = :period and r.company = :company))
  ) then begin

    rownumber = 0;
    forperson = coalesce(forperson,0);
    
    execute procedure period2dates(period)
      returning_values fromdate, todate;
    dni_kalendarzowe = todate - fromdate + 1;

    for
      select p.ref, p.sname, p.fname,
          case when (p.pesel > '') then 'P' when (p.evidenceno > '') then '1' when (p.passportno > '') then '2' else '' end,
          case when (p.pesel > '') then p.pesel when (p.evidenceno > '') then p.evidenceno when(p.passportno > '') then p.passportno else '' end
        from cpersons p
        where (p.ref = :forperson or :forperson = 0) --PR50885
          and p.company = :company
        order by sname, fname
        into :person, :sname, :fname, :doctype, :docno
    do begin
    
      if (doctype = '') then doctype = '0'; --zabezpieczenie przed bledem struktury danych przy imporcie do Platnika
      if (docno = '') then docno = '0';
      B01 = '000000'; --Kod tytułu ubezpieczenia
      B02 = null;     --Informacja o przekroczeniu rocznej podstawy wymiaru składek na ubezpieczenia emerytalne i rentowe
      B03L = 0;       --Wymiar czasu pracy Licznik
      B03M = 1;       --Wymiar czasu pracy Minownik
      B04 = 0;        --Podstawa wymiaru składki na ubezpieczenia emerytalne i rentowe
      B05 = 0;        --Podstawa wymiaru składki na ubezpieczenia chorobowe
      B06 = 0;        --Podstawa wymiaru składki na ubezpieczenia wypadkowe
      B07 = 0;        --Kwota składki na ubezpieczenie emerytalne finansowana przez ubezpieczonego
      B08 = 0;        --Kwota składki na ubezpieczenie rentowe finansowana przez ubezpieczonego
      B09 = 0;        --Kwota składki na ubezpieczenie chorobowe finansowana przez ubezpieczonego
      B10 = 0;        --Kwota składki na ubezpieczenie wypadkowe finansowana przez ubezpieczonego
      B11 = 0;        --Kwota składki na ubezpieczenie emerytalne finansowana przez płatnika
      B12 = 0;        --Kwota składki na ubezpieczenie rentowe finansowana przez płatnika
      B13 = 0;        --Kwota składki na ubezpieczenie chorobowe finansowana przez płatnika
      B14 = 0;        --Kwota składki na ubezpieczenie wypadkowe finansowana przez płatnika
      B15 = 0;        --Kwota składki na ubezpieczenie emerytalne finansowana przez budzet panstwa
      B16 = 0;        --Kwota składki na ubezpieczenie rentowe finansowana przez budzet panstwa
      B17 = 0;        --Kwota składki na ubezpieczenie chorobowe finansowana przez budzet panstwa
      B18 = 0;        --Kwota składki na ubezpieczenie wypadkowe finansowana przez budzet panstwa
      B19 = 0;        --Kwota składki na ubezpieczenie emerytalne finansowana przez PFRON
      B20 = 0;        --Kwota składki na ubezpieczenie rentowe finansowana przez PFRON
      B21 = 0;        --Kwota składki na ubezpieczenie chorobowe finansowana przez PFRON
      B22 = 0;        --Kwota składki na ubezpieczenie wypadkowe finansowana przez PFRON
      B23 = 0;        --Kwota składki na ubezpieczenie emerytalne finansowana przez fundusz kocielny
      B24 = 0;        --Kwota składki na ubezpieczenie rentowe finansowana przez fundusz kocielny
      B25 = 0;        --Kwota składki na ubezpieczenie chorobowe finansowana przez fundusz kocielny
      B26 = 0;        --Kwota składki na ubezpieczenie wypadkowe finansowana przez fundusz kocielny
      B27 = 0;        --Kwota obniżenia podstawy wymiaru składek na ubezpieczenia społeczne z tytułu opłacania składki w ramach pracowniczego programu emerytalnego
      B28 = 0;        --Lączna kwota składek
      C01 = 0;        --Podstawa wymiaru skadki na ubezpieczenie zdrowotne
      C02 = 0;        --Kwota należnej skadki finansowana przez patnika
      C03 = 0;        --Kwota należnej skadki finansowana z Budżetu państwa bezporednio do ZUS
      C04 = 0;        --Kwota należnej skadki finansowana przez ubezpieczonego
      C05 = 0;        --Kwota należnej skadki finansowana przez fundusz koscielny           
      D01 = 0;        --Kwota wypłaconego zasiłku rodzinnego
      D02 = 0;        --Kwota wypłaconego zasiłku wychowawczego
      D03 = 0;        --Kwota wypłaconego zasiłku pielęgnacyjneg
      D04 = 0;        --Laczna kwota wypłaconych zasiłków (rodzinnego, wychowawczego, pielęgnacyjnego)
      B1B = null;
      cper = null;
      employee = null;
      zatrudniony = 0;
    
      --jezeli dana osoba jest na dwoch etatach sprawdz tylko pracownika tego ktory ma najpozniejsza umowe
      --jezeli jest na chorobowym powinien miec nieobecnosc wpisana w obydwa etaty
      select first 1 e.ref from employees e
        join employment m on (m.employee = e.ref)
        where e.person = :person and e.company = :company 
        order by m.fromdate desc
        into :employee;
    
      --jezeli pracownik w danym okresie caly miesiac mial nieobecnosci nieoskladkowane
      adays = 0;
      for
        select a.fromdate, a.todate from eabsences a
            join ecolumns c on (c.number = a.ecolumn)
          where (c.cflags not containing ';SKL;' or c.cflags is null) --BS43036;BS47414
            and a.employee = :employee and a.correction in (0,2)
            and a.todate >= :fromdate and a.fromdate <= :todate
          order by a.fromdate
          into :afromdate, :atodate
      do begin
        if (afromdate < fromdate) then afromdate = fromdate;
        if (atodate > todate) then atodate = todate;
        adays = adays + (atodate - afromdate + 1);
      end
    
      -- ... i nie odprowadzil nic do ZUS oraz na NFZ...
      premium = null;
      select sum(p.pvalue) from epayrolls r
          join eprpos p on (r.ref = p.payroll and r.iper = :period)
          join employees e on (e.ref = p.employee and e.person = :person)
          join ecolumns c on (c.number = p.ecolumn)
        where r.company = :company and r.empltype = 1 
          and e.company = :company and e.empltype <> 3
          and (c.cflags containing ';ZUS;' or c.cflags containing ';NFZ;') --BS33410
        into premium;
      premium = coalesce(premium,0);
    
      -- ... to nie wykazujemy go na RCA
      if (adays < extract(day from todate) or premium <> 0) then
      begin
        --jezeli dany pracownik ma zmiane etatu w ciagu miesiaca, bierze najnowszy wymiar
        --jesli pracuje na dwoch stanowiskach (2 wpisy w employees) to sumuje etaty

        select b03l, b03m, emplstatus from e_get_workdim4rca(:person,:company,:fromdate,:todate) --BS98453
          into :b03l, :b03m, :zatrudniony;

        for
          select distinct c.code || zc1.code || zc2.code, zc3.code
            from ezusdata zd
              join edictzuscodes c on (c.ref = zd.ascode)
              left join edictzuscodes zc1 on (zc1.ref = zd.retired)
              left join edictzuscodes zc2 on (zc2.ref = zd.invalidity)
              left join edictzuscodes zc3 on (zc3.ref = zd.blockade
                                            and zd.baseincyear = substring(:period from 1 for 4) --BS37973
                                            and zd.blockdate <= :todate) --BS42321
            where zd.person = :person and zd.fromdate <= :todate
              and (zd.todate >= :fromdate or zd.todate is null)
            order by zd.fromdate
            into :t_b01, :t_b02
        do begin
          if (b1b is not null) then
          begin
            rownumber = rownumber + 1;
            suspend;
          end
          b01 = t_b01;
          b02 = t_b02;
          b1b = right(b01,2);
        end

        --skladki platne przez budzet panstwa (urlopy macierzynskie, wychowawcze)
        t_b01 = b01;
        dni_macierzynskiego = 0;
        dni_wychowawczego = 0;
        for
          select b01, b04, b06, b10, b11, b12, coalesce(dni_macierzynskiego,0), coalesce(dni_wychowawczego,0)
            from evac_rca_dra(:period, :company, :person, :b01)
            into :b01, :b04, :c01, :c03, :b15, :b16, :dni_macierzynskiego_tmp, :dni_wychowawczego_tmp
        do begin
          dni_macierzynskiego = dni_macierzynskiego + dni_macierzynskiego_tmp;
          dni_wychowawczego = dni_wychowawczego + dni_wychowawczego_tmp;

          if (b04 < 0 or b04 is null) then b04 = 0;
          if (c01 < 0 or c01 is null) then c01 = 0;
          if (c03 < 0 or c03 is null) then c03 = 0;
          b28 = coalesce(b15,0) + coalesce(b16,0);
          if (b15 < 0 or b15 is null) then b15 = 0;
          if (b16 < 0 or b16 is null) then b16 = 0;
          if (b28 < 0) then b28 = 0;
          rownumber = rownumber + 1;
          suspend;
          b01 = t_b01;
          b04 = 0;
          c01 = 0;
          c03 = 0;
          b15 = 0;
          b16 = 0;
          b28 = 0;
        end

        --'podstawowe' naliczenie RCA
        select sum(case when p.ecolumn = 6000 then p.pvalue else 0 end),
            sum(case when p.ecolumn = 6050 then p.pvalue else 0 end),  --BS50665
            sum(case when p.ecolumn = 6070 then p.pvalue else 0 end),
            sum(case when p.ecolumn = 6100 then p.pvalue else 0 end),
            sum(case when p.ecolumn = 6110 then p.pvalue else 0 end),
            sum(case when p.ecolumn in (6120,6130) then p.pvalue else 0 end),
            sum(case when p.ecolumn = 8200 then p.pvalue else 0 end),
            sum(case when p.ecolumn = 8210 then p.pvalue else 0 end),
            sum(case when p.ecolumn = 8220 then p.pvalue else 0 end),
            sum(case when p.ecolumn in (6200,6210) then p.pvalue else 0 end),
            sum(case when p.ecolumn = 7150 then p.pvalue else 0 end),
            sum(case when p.ecolumn = 7220 then p.pvalue else 0 end),
            sum(case when p.ecolumn in (4400,4410) then p.pvalue else 0 end),
            sum(case when p.ecolumn in (4300,4310,4420) then p.pvalue else 0 end),
            sum(case when p.ecolumn in (4550,4560) then p.pvalue else 0 end)
          from epayrolls r
            join eprpos p on (p.payroll = r.ref)
            join employees e on (e.ref = p.employee)
          where r.company = :company and r.iper = :period and r.empltype = 1
            and e.company = :company and e.person = :person and e.empltype <> 3
          into :b04, :b05, :b06, :b07, :b08, :b09, :b11, :b12, :b14, :b27,
               :c01, :c04, :d01, :d02, :d03;

        b28 = coalesce(b07 + b08 + b09 + b10 + b11 + b12 + b13 + b14 + b15 + b16
                     + b17 + b18 + b19 + b20 + b21 + b22 + b23 + b24 + b25 + b26, 0);
        d04 = coalesce(d01 + d02 + d03, 0);

        if (b04 < 0 or b04 is null) then b04 = 0;
        if (b05 < 0 or b05 is null) then b05 = 0;
        if (b06 < 0 or b06 is null) then b06 = 0;
        if (b07 < 0 or b07 is null) then b07 = 0;
        if (b08 < 0 or b08 is null) then b08 = 0;
        if (b09 < 0 or b09 is null) then b09 = 0;
        if (b11 < 0 or b11 is null) then b11 = 0;
        if (b12 < 0 or b12 is null) then b12 = 0;
        if (b14 < 0 or b14 is null) then b14 = 0;
        if (b27 < 0 or b27 is null) then b27 = 0;
        if (b28 < 0) then b28 = 0;
        if (c01 < 0 or c01 is null) then c01 = 0;
        if (c04 < 0 or c04 is null) then c04 = 0;
        if (d01 < 0 or d01 is null) then d01 = 0;
        if (d02 < 0 or d02 is null) then d02 = 0;
        if (d03 < 0 or d03 is null) then d03 = 0;
        if (d04 < 0) then d04 = 0;

        if (zatrudniony = 0 and (b04 <> 0 or b05 <> 0 or C01 <> 0)) then
        begin
          select first 1 r.cper from epayrolls r
              join eprpos p on (p.payroll = r.ref)
              join employees e on (e.ref = p.employee)
            where r.company = :company and r.iper = :period and r.empltype = 1 
              and e.company = :company and e.person = :person and e.empltype <> 3
              and p.ecolumn in (6000, 6050, 6070, 7150)
            order by r.cper desc
            into :cper;
    
          execute procedure period2dates(cper)
            returning_values prev_fromdate, prev_todate;

          select b03l, b03m from e_get_workdim4rca(:person,:company,:prev_fromdate,:prev_todate) --BS98453
            into :b03l, :b03m ;

          b01 = '3000' || b1b;
          rownumber = rownumber + 1;
          suspend;
        end

      --Rachunki do umow zlecen
        if (not(zatrudniony = 1 and (dni_kalendarzowe > dni_wychowawczego + dni_macierzynskiego or b04 <> 0 or b05 <> 0 or C01 <> 0))) then
          zatrudniony = 0;

        t_b01 = b01;
        for
          select z.code, c.ascode, max(coalesce(c.enddate,c.todate))
            from emplcontracts c
              join employees e on (c.employee = e.ref)
              join edictzuscodes z on (z.ref = c.ascode)
              left join epayrolls r on (r.company = :company
                                      and ((r.emplcontract = c.ref and r.empltype = 2) or r.empltype = 3))
              left join eprpos p on (p.payroll = r.ref and p.employee = e.ref and p.ecolumn in (1090))
              left join period2dates(r.cper) d on (1 = 1)
          where e.company = :company and e.person = :person and c.empltype in (2,3)
            and (c.iflags containing 'FC;' or c.iflags containing ';FE;' or c.iflags containing ';FR;')
            and ((c.empltype = 2 and ((r.empltype = 2 and r.iper = :period and p.ref is null)
                 or (c.fromdate <= :todate and (coalesce(c.enddate,c.todate) is null or coalesce(c.enddate,c.todate) >= :fromdate))))
              or (c.empltype = 3 and r.empltype = 3 and r.iper = :period and p.ref is not null
                 and c.fromdate <= d.todate and (coalesce(c.enddate,c.todate) is null or coalesce(c.enddate,c.todate) >= d.fromdate)))
          group by z.code, c.ascode, e.person
          into :b01_ucp, :ascode, :zl_todate
        do begin

          if (zatrudniony >= 1) then
          begin
            b01_ucp = substring(t_b01 from 1 for 4);
            zatrudniony = 2;
          end

        --jesli wyplata jest w miesiacu poprzedzajacym, a kody sa rozne (BS31181), to kod 3000
          if (zl_todate < fromdate and (substring(b01 from 1 for 4) <> b01_ucp or b01 is null
            or b01_ucp is null or (substring(b01 from  1 for 4) = b01_ucp and substring(b01 from 1 for 2) = '04'))
          ) then
            b01_ucp = '3000';

          b01_ucp = b01_ucp || b1b;
          --jesli kody sa rozne to nie zbija danych, tylko wyswietla osobne rekordy
          if (b01 <> b01_ucp) then
          begin
            B03L = null;   B03M = null;   B04 = 0;       B05 = 0;       B06 = 0;
            B07 = 0;       B08 = 0;       B09 = 0;       B11 = 0;       B12 = 0;
            B14 = 0;       B27 = 0;       B28 = 0;       C01 = 0;       C04 = 0;
            D01 = 0;       D02 = 0;       D03 = 0;       D04 = 0;
          end

          b01 = b01_ucp;
          b04_ucp = null;   b05_ucp = null;   b06_ucp = null;   b07_ucp = null;
          b08_ucp = null;   b09_ucp = null;   b11_ucp = null;   b12_ucp = null;
          b14_ucp = null;   b27_ucp = null;   c01_ucp = null;   c04_ucp = null;
          d01_ucp = null;   d02_ucp = null;   d03_ucp = null;
    
          select sum(case when p.ecolumn in (6000,8500) then p.pvalue else 0 end),
              sum(case when p.ecolumn = 6050 and coalesce(p2.pvalue,p3.pvalue) <> 0 then p.pvalue else 0 end), --BS50665,BS59274
              sum(case when p.ecolumn = 6070 then p.pvalue else 0 end),
              sum(case when p.ecolumn = 6100 then p.pvalue else 0 end),
              sum(case when p.ecolumn = 6110 then p.pvalue else 0 end),
              sum(case when p.ecolumn in (6120,6130) then p.pvalue else 0 end),
              sum(case when p.ecolumn in (8200,8510) then p.pvalue else 0 end),
              sum(case when p.ecolumn in (8210,8520) then p.pvalue else 0 end),
              sum(case when p.ecolumn = 8220 then p.pvalue else 0 end),
              sum(case when p.ecolumn in (6200,6210) then p.pvalue else 0 end),
              sum(case when p.ecolumn = 7150 then p.pvalue else 0 end),
              sum(case when p.ecolumn = 7220 then p.pvalue else 0 end),
              sum(case when p.ecolumn in (4400,4410) then p.pvalue else 0 end),
              sum(case when p.ecolumn in (4300,4310,4420) then p.pvalue else 0 end),
              sum(case when p.ecolumn in (4550,4560) then p.pvalue else 0 end)
            from epayrolls r
              join eprpos p on (r.ref = p.payroll)
              join employees e on (e.ref = p.employee)
              left join emplcontracts c on (c.employee = e.ref and (r.emplcontract = c.ref or c.empltype = 3))
              left join eprpos p2 on (p2.eprempl = p.eprempl and p2.employee = p.employee and p2.payroll = p.payroll and p2.ecolumn = 6120 and p.ecolumn = 6050) --BS59274
              left join eprpos p3 on (p3.eprempl = p.eprempl and p3.employee = p.employee and p3.payroll = p.payroll and p3.ecolumn = 6130 and p.ecolumn = 6050)
              left join eprpos p4 on (p4.eprempl = p.eprempl and p4.employee = p.employee and p4.payroll = p.payroll and p4.ecolumn = 1090) --BS98453
              left join period2dates(r.cper) d on (1 = 1)
            where r.company = :company and r.iper = :period and r.empltype in (2,3)
              and e.company = :company and e.person = :person and c.ascode = :ascode
              and (r.empltype = 2
                or (r.empltype = 3 and c.empltype = 3 and p4.ref is not null
                  and c.fromdate <= d.todate and (coalesce(c.enddate,c.todate) is null or coalesce(c.enddate,c.todate) >= d.fromdate)))
            into :b04_ucp, :b05_ucp, :b06_ucp, :b07_ucp, :b08_ucp, :b09_ucp,
                 :b11_ucp, :b12_ucp, :b14_ucp, :b27_ucp, :c01_ucp, :c04_ucp,
                 :d01_ucp, :d02_ucp, :d03_ucp;

          b28_ucp = coalesce(b07_ucp + b08_ucp + b09_ucp + b11_ucp + b12_ucp + b14_ucp, 0);
          d04_ucp = coalesce(d01_ucp + d02_ucp + d03_ucp, 0);

          b04 = b04 + coalesce(b04_ucp, 0);
          b05 = b05 + coalesce(b05_ucp, 0);
          b06 = b06 + coalesce(b06_ucp, 0);
          b07 = b07 + coalesce(b07_ucp, 0);
          b08 = b08 + coalesce(b08_ucp, 0);
          b09 = b09 + coalesce(b09_ucp, 0);
          b11 = b11 + coalesce(b11_ucp, 0);
          b12 = b12 + coalesce(b12_ucp, 0);
          b14 = b14 + coalesce(b14_ucp, 0);
          b27 = b27 + coalesce(b27_ucp, 0);
          b28 = b28 + b28_ucp;
          c01 = c01 + coalesce(c01_ucp, 0);
          c04 = c04 + coalesce(c04_ucp, 0);
          d01 = d01 + coalesce(d01_ucp, 0);
          d02 = d02 + coalesce(d02_ucp, 0);
          d03 = d03 + coalesce(d03_ucp, 0);
          d04 = d04 + d04_ucp;
    
          if (b04 < 0) then b04 = 0;
          if (b05 < 0) then b05 = 0;
          if (b06 < 0) then b06 = 0;
          if (b07 < 0) then b07 = 0;
          if (b08 < 0) then b08 = 0;
          if (b09 < 0) then b09 = 0;
          if (b11 < 0) then b11 = 0;
          if (b12 < 0) then b12 = 0;
          if (b14 < 0) then b14 = 0;
          if (b27 < 0) then b27 = 0;
          if (b28 < 0) then b28 = 0;
          if (c01 < 0) then c01 = 0;
          if (c04 < 0) then c04 = 0;
          if (d01 < 0) then d01 = 0;
          if (d02 < 0) then d02 = 0;
          if (d03 < 0) then d03 = 0;
          if (d04 < 0) then d04 = 0;

          rownumber = rownumber + 1;
          suspend;
        end

        if (zatrudniony = 1) then
        begin
           rownumber = rownumber + 1;
           suspend;
        end
      end else
      if (forperson <> 0) then --BS52260
      begin
        rownumber = -1;
        suspend;
      end
    end
  end
end^
SET TERM ; ^
