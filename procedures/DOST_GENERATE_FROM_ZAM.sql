--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOST_GENERATE_FROM_ZAM(
      NAGZAMREF integer,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      TRYB integer,
      DTPOKRY timestamp,
      OTWARTE integer,
      AKTUAL integer,
      ZDANYCH integer)
  returns (
      STATUS integer,
      ILZMIEN integer)
   as
declare variable genval numeric(14,4);
declare variable ogenval numeric(14,4);
declare variable minus numeric(14,4);
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable stan numeric(14,4);
declare variable zamowiono numeric(14,4);
declare variable zarezerw numeric(14,4);
declare variable zablokow numeric(14,4);
declare variable cnt integer;
declare variable mag varchar(3);
declare variable dostawca integer;
declare variable cenanet numeric(14,4);
declare variable waluta varchar(3);
declare variable dcwaluta varchar(3);
declare variable cenawal numeric(14,4);
declare variable kurs numeric(14,4);
declare variable walutapln varchar(255);
declare variable dtnextdost timestamp;
declare variable datadost timestamp;
declare variable rejestr integer;
declare variable stmin numeric(14,4);
declare variable tabkontr integer;
declare variable dnireal integer;
declare variable pozref integer;
declare variable nagrabat numeric(14,4);
declare variable rabat numeric(14,4);
declare variable VAT numeric(14,4);
declare variable wgcen char(1);
declare variable ilmin numeric(14,4);
declare variable jedn integer;
declare variable przelicz numeric(14,4);
declare variable dokladnosc numeric(14,4);
declare variable prec smallint;
begin
 STATUS = -1;
 ILZMIEN = 0;
 if(:dtpokry = '1899-12-30') then dtpokry = null;
 /* okrelsenie daty do */
  select RABAT,DOSTAWCA,WALUTA,KURS, TERMDOST, BN
    from NAGZAM
    where REF=:NAGZAMREF
    into :nagrabat,:dostawca,:waluta, :kurs, :datadost, :wgcen;
  if (wgcen is null) then
    execute procedure GETCONFIG('WGCEN') returning_values :wgcen;
  if(:nagrabat is null) then nagrabat = 0;
  if(:kurs is null or (:kurs = 0)) then kurs = 1;
  if(:tryb = 1) then begin
    select DOSTAWCY.dnireal, NAGZAM.TERMDOST from DOSTAWCY join NAGZAM on ( DOSTAWCY.ref = NAGZAM.dostawca) where NAGZAM.REF=:NAGZAMREF into :dnireal,:dtpokry;
    if(:dnireal is null or (:dnireal=0)) then begin
      dtpokry = null;
    end else begin
      dtpokry = :dtpokry + :dnireal + 1;
    end
    if(:datadost is null) then exception GENERATE_DOSTAWY_DATA_REALIZACJ;
    select min(termdost) from NAGZAM where STAN = 'D' and DOSTAWCA = :dostawca and typ = 0 and DATAWE >:datadost into :dtnextdost;
    if(:dtnextdost is not null and ((:dtnextdost < :dtpokry) or (:dtpokry is null))) then dtpokry = :dtnextdost;
  end
  execute procedure GETCONFIG('WALPLN') returning_values :walutapln;
  if(:walutapln is null) then walutapln = 'PLN';
  if(:waluta is null) then waluta = :walutapln;
  for select DOSTCEN.ktm, DOSTCEN.wersja, STANYIL.MAGAZYN, sum(dostcen.iloscmin), sum(STANYIL.ILOSC), sum(STANYIL.zamowiono), sum(stanyil.zarezerw), sum(stanyil.zablokow), sum(stanyil.stanmin)
     from DOSTCEN left join STANYIL on (DOSTCEN.WERSJAREF = STANYIL.WERSJAREF)
                  left join POZZAM on ( STANYIL.KTM = POZZAM.KTM AND STANYIL.WERSJA = POZZAM.WERSJA AND POZZAM.ZAMOWIENIE = :NAGZAMREF)
     where
      (STANYIL.MAGAZYN = :MAGAZYN or (:MAGAZYN='') or(stanyil.magazyn is null)) and
      ((:aktual = 0) or (POZZAM.KTM is not null and POZZAM.WERSJA is not null)) and
      (DOSTCEN.DOSTAWCA = :dostawca and DOSTCEN.AKT=1)
     group by DOSTCEN.KTM, DOSTCEN.WERSJA, STANYIL.MAGAZYN
     into :ktm,:wersja, :mag, :ilmin, :stan, :zamowiono, :zarezerw, :zablokow, :stmin
  do begin
     /* okrelsenie zapotrzebowania dla danego ktm'o wersji */
     genval = 0;
     minus = 0;
     if(:mag is null) then mag = :magazyn;
     if(:stan is null) then stan = 0;
     if(:zamowiono is null) then zamowiono = 0;
     if(:zarezerw is null) then zarezerw = 0;
     if(:zablokow is null) then zablokow = 0;
     stan = stan - zablokow;
     /*zmniejszenie stanu o stany minimalne*/
     if(:stmin > 0) then stan = :stan - :stmin;
     /* podliczenie na pozycjach "do zablokowania" */
     minus = null;
     genval = null;
     if(:zdanych = 2) then begin
       select sum(ILOSC) from STANYREZ where KTM = :ktm AND WERSJA = :WERSJA and MAGAZYN=:mag AND
             STATUS = 'Z' AND ZREAL = 0 into :genval;
       if(:genval is null) then genval = 0;
       genval = :genval + :zablokow;/* w ilosciach zarezerwowanych są za rwno stany R jak i Z rezerwacji */
     end else if(:zdanych = 1) then begin
       genval = :zablokow + :zarezerw;
     end else begin
       zamowiono = null;
       select sum(ILOSC - ILZREAL) from POZZAM join NAGZAM on (POZZAM.KTM = :ktm and POZZAM.WERSJA = :wersja and POZZAM.ZAMOWIENIE = NAGZAM.REF and NAGZAM.TYP < 2 )
        join TYPZAM on (TYPZAM.SYMBOL = NAGZAM.TYPZAM and TYPZAM.WYDANIA > 0)
        left join DEFREJZAM on (DEFREJZAM.SYMBOL = NAGZAM.REJESTR)
        where DEFREJZAM.ARCHIWALNY<>1
       into :zamowiono;
       if(:zamowiono is null) then zamowiono = 0;
       genval = :zamowiono;

     end
     genval = :genval - :stan;
     if(:genval < 0 ) then genval = 0;
     if(:genval > 0 and (:dtpokry is not null)) then begin
       /* obnizenie o ilosci ograniczenia czasowego */
       minus = null;
       select sum(ILOSC) from STANYREZ where KTM = :ktm AND WERSJA = :WERSJA and MAGAZYN=:mag AND
            ((STATUS = 'Z') or (STATUS = 'B')) AND (STANYREZ.data > :DTPOKRY) AND ZREAL = 0 into :minus;
       if(:minus is null) then minus = 0;
       genval = :genval - :minus;
       if(:genval < 0 ) then genval = 0;
       if(:zdanych < 2 and :genval > 0 ) then begin
         minus = null;
         select sum(ILOSC) from STANYREZ where KTM = :ktm AND WERSJA = :WERSJA and MAGAZYN=:mag AND
            (STATUS = 'R') AND (STANYREZ.data > :DTPOKRY)  AND ZREAL = 0 into :minus;
         if(:minus is null) then minus = 0;
         genval = :genval - :minus;
       end
       /* tu dodac odejmowanie z  zamowiono*/
     end
     if(:genval > 0 ) then begin
       /* obnizenie o ilosci zamowione */

       minus = null;
       if(:otwarte = 1) then begin
         select sum(ILOSC - ILZREAL) from POZZAM join NAGZAM on (POZZAM.KTM = :ktm and POZZAM.WERSJA = :wersja and POZZAM.ZAMOWIENIE = NAGZAM.REF and NAGZAM.TYP < 2 and NAGZAM.REF <> :NAGZAMREF and (:dtpokry is null or (nagzam.termdost < :dtpokry)))
          join TYPZAM on (TYPZAM.SYMBOL = NAGZAM.TYPZAM and TYPZAM.WYDANIA = 0)
          left join DEFREJZAM on (DEFREJZAM.SYMBOL = NAGZAM.REJESTR)
          where DEFREJZAM.ARCHIWALNY<>1
         into :minus;
       end else begin
          select sum(ILOSC) from STANYREZ where KTM = :ktm AND WERSJA = :WERSJA and MAGAZYN=:mag AND
             STATUS = 'Z' AND ZREAL = 0 and (:dtpokry is null or (STANYREZ.data < :dtpokry))
          into :minus;
       end
       if(:minus is null) then minus = 0;
       genval = genval - minus;
       if(:genval < 0) then genval = 0;
     end
     if(:genval > 0) then begin
       /*przeksztalcenie ilosci wygenerowanej do ilosci w jednostkach domyslnych zakupu*/
       ilmin = 0;
       dokladnosc = 0;
       przelicz = 1;
       jedn = null;
       prec = 0;
       if(:dostawca is not null) then
         select DOSTCEN.ILOSCMIN, DOSTCEN.paczkamin, TOWJEDN.REF, TOWJEDN.PRZELICZ
         from DOSTCEN join TOWJEDN on (DOSTCEN.JEDN = TOWJEDN.REF)
         where DOSTCEN.dostawca = :dostawca and DOSTCEN.KTM = :ktm and DOSTCEN.wersja = :wersja and DOSTCEN.AKT=1
         into :ilmin,:dokladnosc,:jedn, :przelicz;
       if(:jedn is null) then begin
         select ref, :przelicz from TOWJEDN where KTM = :ktm and Z = 2 into :jedn, :przelicz;
         if(:jedn is null) then
           select ref,1 from TOWJEDN where KTM = :ktm and GLOWNA = 1 into :jedn,:przelicz;
       end
       if(:przelicz > 0) then
         genval = :genval / :przelicz;
       if(:genval < :ilmin) then genval = :ilmin;
       if(:dokladnosc > 0) then begin
         /*zaokraglenie w gore*/
         ogenval = :genval;
         genval = cast((:genval / :dokladnosc) as integer) * :dokladnosc;
         if(:genval < :ogenval ) then genval = :genval + :dokladnosc;
       end
       /* zapis wartosci wygenerowanej */

       select count(*) from POZZAM where ZAMOWIENIE=:NAGZAMREF AND KTM = :KTM AND WERSJA = :WERSJA into :cnt;
       if(:cnt is null) then cnt = 0;
       if(:cnt > 0) then begin
         pozref = null;
         select max(ref) from POZZAM where ZAMOWIENIE=:NAGZAMREF AND KTM = :KTM AND WERSJA = :WERSJA and ((ILOSC<> :genval) or (ILOSC is null)) into :pozref;
         if(:pozref > 0) then begin
           update POZZAM set ILOSCO =:genval,  ILOSC = :genval, ILOSCM = null, JEDN = :jedn, JEDNO = :jedn where REF=:pozref;
           ilzmien = :ilzmien + 1;
         end
       end else begin
        cenanet = 0;
        dcwaluta = '';
        cenawal = 0;
        if(:dostawca is not null) then begin
          select CENANET, CENA_FAB, WALUTA, RABAT, PREC from DOSTCEN where DOSTAWCA = :DOSTAWCA AND KTM = :KTM AND WERSJA = :WERSJA into :cenanet, :cenawal, dcwaluta, :rabat, :prec;
          if(:dcwaluta is null) then dcwaluta = :walutapln;
          if(:cenawal > 0) then begin
            if(:rabat > 0) then
              rabat = :rabat - :nagrabat;
            cenanet = :cenawal;
            if(:waluta <> :dcwaluta) then
               cenanet = :cenanet / :kurs;
           end else
            rabat = 0;
        end
        if(:cenanet is null) then cenanet = 0;
        if(:cenanet = 0) then begin
          rabat = 0;
          /*szukanei ceny na wersjach*/
          select CENA_ZAKN from WERSJE where KTM = :KTM and NRWERSJI=:wersja into :cenanet;
          if(:cenanet is null) then cenanet = 0;
          if(:waluta <> :walutapln ) then
            cenanet = :cenanet/:kurs;
          if(:cenanet = 0) then begin
            select CENA_ZAKN from TOWARY where KTM = :KTM into :cenanet;
            if(:cenanet is null) then cenanet = 0;
            if(:waluta <> :walutapln ) then
              cenanet = :cenanet/:kurs;
          end
        end

        if(:wgcen = 'B')then begin
          select VAT.stawka from VAT join TOWARY on (TOWARY.VAT = VAT.GRUPA) where TOWARY.KTM = :ktm into :vat;
          cenanet = :cenanet *(1+:vat/100);
        end
        if(:genval > 0) then begin
         insert into POZZAM (ZAMOWIENIE,MAGAZYN, KTM, WERSJA, ILOSCO, ILOSC, JEDNO, JEDN,CENACEN, WALCEN, RABAT, OPK, PREC)
         values(:NAGZAMREF,:mag, :ktm, :wersja, :genval, :genval, :jedn, :jedn, :cenanet, :waluta, :rabat,0, :prec);
         ilzmien = :ilzmien + 1;
        end
       end
     end
  end
  STATUS=1;
end^
SET TERM ; ^
