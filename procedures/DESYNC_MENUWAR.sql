--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DESYNC_MENUWAR(
      REFF integer)
   as
DECLARE VARIABLE IL INTEGER;
begin
   select count(*) from LOG_FILE where TABLE_NAME='MENUWAR' and KEY1=:reff into :il;
   if(:il is null) then il = 0;
   if(:il = 0) then
      insert into LOG_FILE(TABLE_NAME,TIME_STAMP,KEY1) values ('MENUWAR',CURRENT_TIMESTAMP(0),:reff);
   else
     update LOG_FILE set TIME_STAMP=CURRENT_TIMESTAMP(0) where TABLE_NAME='MENUWAR' and KEY1=:reff;
end^
SET TERM ; ^
