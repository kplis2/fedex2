--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_MWSSTOCK_BLOCKING(
      BLOCKING SMALLINT_ID,
      PARTIA STRING10 = null,
      KTM KTM_ID = null,
      REF INTEGER_ID = null)
  returns (
      MSG STRING255,
      STATUS SMALLINT_ID)
   as
declare variable MWSSTOCKREF INTEGER_ID;
declare variable RECORDS INTEGER_ID;
begin
/*
Procedurka do blokowania towaru

blocking - 1 - zablokuj
blocking - 0 - odblokuj

*/
--exception universal coalesce(blocking, 1)||' '||coalesce(ktm, '')||' '||ref;
  if (ktm = 'null') then begin          --Nie wiem jak z grubego klienta przekazać nulla
    ktm = null;
    partia = null;
  end

  records = 0;
  status = 1;
  msg = '';
  if (blocking is null or blocking not in (0,1)) then begin
    msg = 'Nie określiłeś trybu lub tryb nie znany.';
    status = 0;
    suspend;
    exit;
  end
  if (partia is null and ref is null) then begin
    msg = 'Nie określiłeś partii towaru.';
    status = 0;
    suspend;
    exit;
  end
  if (ktm is null and ref is null) then begin
    msg = 'Nie podałeś KTM towaru.';
    status = 0;
    suspend;
    exit;
  end
  if (not exists(select * from mwsstock m where m.x_partia = :partia) and ref is null) then begin
    msg = 'Wybrana partia '||partia||' nie istnieje na MWS.';
    status = 0;
    suspend;
    exit;
  end
  if (not exists(select * from towary t where t.ktm = :ktm and t.akt = 1 and t.x_mws_partie = 1) and ref is null) then begin
    msg = 'Wybrany towar '||ktm||' nie istnieje w kartotece, jest nie aktywny lub nie podlega kontroli partii.';
    status = 0;
    suspend;
    exit;
  end
  if (ref is not null and partia is null and ktm is null) then
    select m.good, m.x_partia
      from mwsstock m
      where m.ref = :ref
    into :ktm, :partia;

  if (blocking in (0,1)) then begin
    for
      select m.ref
        from mwsstock m
        where m.good = :ktm
          and m.x_partia = :partia
      into :mwsstockref
    do begin

      update mwsstock ms
       set ms.x_blocked = :blocking
       where ms.ref = :mwsstockref;
     records = records + 1;
    end
  end else
    status = 0;

  if (status = 1 and records > 0 and blocking = 1) then
    msg = 'Zablokowano '||records||' wpisów. Towaru '||ktm||', partii '||partia||'.';

  if (status = 1 and records > 0 and blocking = 0) then
    msg = 'Odblokowano '||records||' wpisów. Towaru '||ktm||', partii '||partia||'.';

  if (status = 1 and records = 0) then
    msg = 'Nie odnaleziono towaru '||ktm||' z partią '||partia||' na magazynie';

  if (status = 0) then
    msg = 'Bląd wykonywania operacji.';
  suspend;
end^
SET TERM ; ^
