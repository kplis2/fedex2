--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKSPED_CHECKFAK(
      DOKSPED integer)
  returns (
      FAKMAG integer,
      FAKUSL integer,
      FAKUSLSPOSPLAT integer,
      FAKUSLTYPFAK varchar(10) CHARACTER SET UTF8                           )
   as
declare variable dokref integer;
declare variable akcept integer;
begin
  FAKUSLSPOSPLAT = 0;
  FAKUSLTYPFAK = '';
  akcept = 0;

  select AKCEPT from LISTYWYSD where REF = :DOKSPED into :akcept;
  if(:akcept > 0) then begin
    select count(*) from BR_listywysd_dok(:doksped) where FAKTURA is null and AKCEPT = 1 into :fakmag;
    if(:fakmag > 0) then fakmag = 1;
    else fakmag = 0;
    select count(*) from LISTYWYSD where ref = :doksped and KOSZTDOSTFAKTURA is null
    and( KOSZTDOSTBEZ = 0 or (kosztdostbez is null)) and KOSZTDOST > 0
    into :fakusl;
    if(:fakusl > 0) then fakusl = 1;
    else fakusl = 0;
    if(:fakusl > 0) then begin
      for select ref from BR_listywysd_dok(:doksped) into :dokref
      do begin
        if(:FAKUSLTYPFAK = '') then begin
          select DOKUMNAG.TYPDOKVAT from DOKUMNAG where ref=:dokref into :FAKUSLTYPFAK;
        end
        if(:fakuslsposplat = 0) then begin
          select DOKUMNAG.sposzap from DOKUMNAG where ref=:dokref into :fakuslsposplat;
        end
        if(:fakuslsposplat is null) then fakuslsposplat = 0;
        if(:fakusltypfak is null) then fakusltypfak = '';
        if(:fakusltypfak = '   ') then fakusltypfak = '';
      end
    end
  end else begin
    FAKMAG = 0;
    FAKUSL = 0;
    FAKUSLSPOSPLAT = 0;
    FAKUSLTYPFAK = '';
  end
end^
SET TERM ; ^
