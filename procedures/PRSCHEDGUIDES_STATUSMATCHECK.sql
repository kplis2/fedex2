--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDES_STATUSMATCHECK(
      GUIDE integer)
   as
declare variable statusmat integer;
begin
  --sprzawdzenie, czy gdzies nei ma za malo materialu na przewodniku w stosunku do ilosci oczekiwanej
  select STATUSMAT from PRSCHEDGUIDES where ref=:guide into :statusmat;
  if(:statusmat >= 3) then
    exit;
  statusmat = 0;
  if(exists( select PM.ref
    from prschedguidespos PM
      join prschedguides G on (PM.prschedguide = G.ref)
    where PM.prschedguide = :guide
     and (PM.amount - PM.amountzreal)*PM.kamountnorm < (G.amount - g.amountzreal)
  )) then
    statusmat = -1;
  select max(prschedopers.statusmat) from prschedopers where prschedopers.guide = :guide and statusmat < 3 into :statusmat;
  if(:statusmat is null) then statusmat = 0;
  update prschedguides set statusmat = :statusmat where ref =:guide and statusmat <3;
end^
SET TERM ; ^
