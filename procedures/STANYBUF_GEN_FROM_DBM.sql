--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STANYBUF_GEN_FROM_DBM(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           )
   as
declare variable ktm varchar(40);
declare variable wersjaref integer;
declare variable nowystanmax numeric(14,4);
declare variable dnibezzmiany integer;
declare variable dnizielone integer;
declare variable dnizolte integer;
declare variable dniczerwone integer;
declare variable dniczarne integer;
declare variable ikona varchar(20);
begin
  delete from stanybuf where magazyn=:magazyn;
  for select KTM,WERSJAREF,NOWYSTANMAX,DNIBEZZMIANY,DNIZIELONE,DNIZOLTE,DNICZERWONE,DNICZARNE,IKONA
    from GET_DBM_ADJUST(:magazyn)
    where IKONA<>''
    into :ktm, :wersjaref, :nowystanmax, :dnibezzmiany, :dnizielone, :dnizolte, :dniczerwone, :dniczarne, :ikona
  do begin
    update or insert into STANYBUF(MAGAZYN,KTM,WERSJAREF,STANMAXNOWY,STANMAXRECZNY,STATUS,
      DNIBEZZMIANY,DNIZIELONE,DNIZOLTE,DNICZERWONE,DNICZARNE,IKONA)
      values (:magazyn,:ktm,:wersjaref,:nowystanmax,:nowystanmax,0,
      :dnibezzmiany,:dnizielone,:dnizolte,:dniczerwone,:dniczarne,:ikona)
      matching (MAGAZYN, WERSJAREF);
  end
end^
SET TERM ; ^
