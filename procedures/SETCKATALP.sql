--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SETCKATALP(
      CPODMIOT integer,
      KATEGORIA varchar(80) CHARACTER SET UTF8                           ,
      ELEMENT varchar(80) CHARACTER SET UTF8                           ,
      ATRYBUT varchar(80) CHARACTER SET UTF8                           ,
      WARTOSC varchar(255) CHARACTER SET UTF8                           )
  returns (
      STATUS integer)
   as
declare variable cdefkatal integer;
declare variable ckatal integer;
declare variable cnt integer;
begin
  status = 0;
  select ref from cdefkatal where nazwa=:kategoria
  into :cdefkatal;
  if(:cpodmiot is not null and :cdefkatal is not null) then begin
    if(:element is not null and :element<>'') then begin
      select ref from ckatal
      where cpodmiot=:cpodmiot and cdefkatal=:cdefkatal and nazwa=:element
      into :ckatal;
      if(:ckatal is null) then begin
        execute procedure GEN_REF('CKATAL') returning_values :ckatal;
        insert into ckatal(ref,cpodmiot,cdefkatal,nazwa,akt)
        values (:ckatal,:cpodmiot,:cdefkatal,:element,1);
      end
    end else begin
      select ref from ckatal
      where cpodmiot=:cpodmiot and cdefkatal=:cdefkatal and numer=1
      into :ckatal;
      if(:ckatal is null) then begin
        execute procedure GEN_REF('CKATAL') returning_values :ckatal;
        insert into ckatal(ref,cpodmiot,cdefkatal,nazwa,numer,akt)
        values (:ckatal,:cpodmiot,:cdefkatal,'Informacje',1,1);
      end
    end
  end
  if(:ckatal is not null and atrybut is not null) then begin
    select count(*) from ckatalp
    where ckatal=:ckatal and symbol=:atrybut
    into :cnt;
    if(cnt=0) then begin
      insert into ckatalp(ckatal,symbol,wartosc)
      values(:ckatal,:atrybut,:wartosc);
      status = 1;
    end else begin
      update ckatalp set wartosc=:wartosc
      where ckatal=:ckatal and symbol=:atrybut;
      status = 1;
    end
  end
end^
SET TERM ; ^
