--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MOVE_NUMBER(
      NAZWA varchar(40) CHARACTER SET UTF8                           ,
      REJESTR varchar(20) CHARACTER SET UTF8                           ,
      DOKUMENT varchar(20) CHARACTER SET UTF8                           ,
      DATA date,
      NUMBER integer,
      NEWDATA date)
   as
declare variable KEY_VAL varchar(30);
declare variable IS_REJ smallint;
declare variable IS_DOK smallint;
declare variable TYP_OKRES smallint;
declare variable OKRES varchar(8);
declare variable OLD_NUMBER integer;
declare variable BRAK smallint;
declare variable ODDZIAL varchar(20);
declare variable CHRONO smallint;
declare variable LASTDATA timestamp;
declare variable IS_ODDZIAL smallint;
declare variable NUMREF integer;
declare variable prevdata timestamp;
declare variable nextdata timestamp;
declare variable ISf_company smallint;
declare variable current_company integer;
begin
  execute procedure GETCONFIG('AKTUODDZIAL') returning_values :oddzial;
  select rejestr, dokument, oddzial, okres, braki, chrono, company  from NUMBERGEN where NAZWA = :nazwa
      into :is_rej, :is_dok, :is_oddzial, :typ_okres, :brak, :chrono, :isf_company;
  if (isf_company = 1) then
    execute procedure get_global_param('CURRENTCOMPANY')
      returning_values :current_company;  
  if(:is_oddzial=0) then oddzial='XXX'; /* numeracja wspolna dla oddzialow prowadzonych w jednej bazie */
  if(:brak = 1) then begin
      key_val = '';
      if(:rejestr is null) then rejestr = '';
      if(:dokument is null) then dokument = '';
      if(:is_rej = 1) then key_val = key_val || rejestr;
      if(:is_dok = 1) then key_val = key_val || dokument;
      okres = cast( extract( year from :data) as char(4));
      if(:typ_okres = 1) then begin
        /* data pelna*/
        if(extract(month from :data) < 10) then
           okres = :okres ||'0'||cast(extract(month from :data) as char(1));
        else
           okres = :okres ||cast(extract(month from :data) as char(2));
      end
      if(:okres is null) then okres = '';
      key_val = key_val || okres;

      if(:chrono > 0) then begin
        -- odczytaj date, na jaki zajeto numer
        lastdata = null;
        select lastdata from NUMBERFREE where oddzial = :oddzial and nazwa = :nazwa and key_val = :key_val and typ = 2 and number = :number and (:ISF_company != 1 or company = :current_company)
          into :lastdata;

        if(:lastdata is not null and :newdata<>:lastdata) then begin
          if(:newdata<:lastdata) then begin
            -- jesli ta date chcemy zmienic na wczesniejsza
            -- to odczytaj date z poprzedniego numeru
            prevdata = null;
            select first 1 lastdata from NUMBERFREE
              where oddzial = :oddzial and nazwa = :nazwa and key_val = :key_val and typ = 2 and number < :number and (:ISF_company != 1 or company = :current_company)
              order by NAZWA desc, KEY_VAL desc, TYP desc, ODDZIAL desc, LASTDATA desc
              into :prevdata;
            -- zmiana daty nie moze naruszyc chronologii
            if(:prevdata is null or :newdata>=:prevdata) then begin
              update NUMBERFREE set lastdata=:newdata where oddzial = :oddzial and nazwa = :nazwa and key_val = :key_val and typ = 2 and number = :number and (:ISF_company != 1 or company = :current_company);
            end else exception GENERATOR_CHRONOSAAFTER 'Nie można zmienić daty, gdyż narusza chronologię numeracji.';
          end else if(:newdata>:lastdata) then begin
            -- jesli ta date chcemy zmienic na pozniejsza
            -- to odczytaj date z nastepnego numeru
            nextdata = null;
            select first 1 lastdata from NUMBERFREE
              where oddzial = :oddzial and nazwa = :nazwa and key_val = :key_val and typ = 2 and number > :number and (:ISF_company != 1 or company = :current_company)
              order by NAZWA, KEY_VAL, TYP, ODDZIAL, LASTDATA
              into :nextdata;
            -- zmiana daty nie moze naruszyc chronologii
            if(:nextdata is null or :newdata<=:nextdata) then begin
              update NUMBERFREE set lastdata=:newdata where oddzial = :oddzial and nazwa = :nazwa and key_val = :key_val and typ = 2 and number = :number and (:ISF_company != 1 or company = :current_company);
            end else exception GENERATOR_CHRONOSAAFTER 'Nie można zmienić daty, gdyż narusza chronologię numeracji.';
          end
        end
      end
      -- jesli ten numer jest ostatnim zajetym to zmien date takze tam
      select number, lastdata from NUMBERFREE where oddzial = :oddzial and nazwa = :nazwa and key_val = :key_val and typ = 0 and (:ISF_company != 1 or company = :current_company) into :old_number, :lastdata;
      if(:old_number=:number and :newdata<>:lastdata) then
        update NUMBERFREE set lastdata=:newdata where oddzial = :oddzial and nazwa = :nazwa and key_val = :key_val and typ = 0 and (:ISF_company != 1 or company = :current_company);
  end
end^
SET TERM ; ^
