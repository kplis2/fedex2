--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BANKACCOUTS_UPDATE(
      OLDACC varchar(80) CHARACTER SET UTF8                           ,
      NEWACC varchar(80) CHARACTER SET UTF8                           ,
      EMPLOYEE EMPLOYEES_ID)
   as
declare variable OLDACCREF integer;
declare variable NEWACCREF integer;
declare variable person integer;
declare variable activ smallint;
declare variable counter integer;
declare variable GLACCREF integer;
begin
/*Procedura zmieniajaca numer rachunku bankowego z OLDACC na NEWACC dla danego
  pracownika EMPLOYEE; wowolywana po zaakceptowaniu wniosku eHRM */

  if (oldacc = newacc) then
    exception universal 'Nie można zmienić numeru konta na ten sam numer';
  else if (oldacc is null) then
  begin
  --Dodanie nowego konta bankowego
    if (exists (select first 1 1 from emplbankaccounts where employee = :employee)) then
      exception universal 'To stanowisko pracy ma juz dodane konto bankowe';
    select person from employees
      where ref = :employee
      into :person;

    select ref, act from bankaccounts
      where dictpos = :person
        and eaccount = :newacc
        and dictdef = 9
      into :newaccref, :activ;

    if (newaccref is null) then
    begin
      insert into bankaccounts (bank, dictdef, dictpos, account, act)
        values (null, 9, :person, :newacc, 1)
        returning ref into :newaccref;
    end else
    if (coalesce(activ,0) = 0) then
      update bankaccounts set act = 1 where ref = :newaccref;

    insert into emplbankaccounts (employee, ecolumn, account, atype)
      values (:employee, 9020, :newaccref, 0);
  end else
  begin
  --Aktualizacja numeru konta

    select b.ref, e.person from bankaccounts b
        join emplbankaccounts m on (b.ref = m.account)
        join employees e on (e.ref = m.employee)
      where b.eaccount = :oldacc and m.employee = :employee
      into :oldaccref, :person;

    if (oldaccref is null) then
      exception universal 'Nie znaleziono zmienianego numeru konta: ' || OLDACC;

    select ref, act from bankaccounts
      where dictpos = :person
        and eaccount = :newacc
        and dictdef = 9
      into :newaccref, :activ;

    --sprawdzenie, czy nowe konto nie jest juz uzywane do wyplaty u tego pracownika
    if (exists(select first 1 1 from emplbankaccounts
          where employee = :employee and account = :newaccref)
    ) then
      exception universal 'Próba zdefiniowania różnych poleceń wypłaty na to samo konto!';

    if (newaccref is null) then
    begin
      insert into bankaccounts (bank, dictdef, dictpos, account, act)
        values (null, 9, :person, :newacc, 1)
        returning ref into :newaccref;
    end else if (coalesce(activ,0) = 0) then
      update bankaccounts set act = 1 where ref = :newaccref;

    --znalezienie konta glownego dla pracownika
    select b.ref from bankaccounts b
      join persons p on (p.ref = b.dictpos)
      where b.dictpos = :person
        and b.gl = 1
        and b.dictdef = 9
      into :glaccref;

    --sprawdzenie w ilu miejscach jest wykorzystywane konto glowne w dowolnej z kartotek
    --pracownika poza przypadkiem ktory chcemy zmienic
    select count(*)
      from bankaccounts b
      join employees e on (e.person = b.dictpos)
      join emplbankaccounts m on (m.employee = e.ref and m.account = b.ref)
      where b.gl = 1
        and b.dictpos = :person
        and b.dictdef = 9
        and (m.employee <> :employee or m.account <> :oldaccref)
      into :counter;

    if (coalesce(counter,0) < 1 and oldaccref = glaccref) then
    begin
      update bankaccounts set gl = 1, offdate = null
        where ref = :newaccref;
      update bankaccounts set offdate = current_date
        where ref = :oldaccref;
    end

    update emplbankaccounts set account = :newaccref
      where employee = :employee and account = :oldaccref;
  end
end^
SET TERM ; ^
