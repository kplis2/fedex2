--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BKACCOUNT_MATCHING_CHECK(
      BKACCOUNT BKACCOUNTS_ID)
  returns (
      MATCHABLE SMALLINT_ID)
   as
begin
  select b.matchable
    from bkaccounts b
    where b.ref = :bkaccount
    into matchable;
end^
SET TERM ; ^
