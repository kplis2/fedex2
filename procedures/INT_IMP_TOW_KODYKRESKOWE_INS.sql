--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_TOW_KODYKRESKOWE_INS(
      TOWARID type of column INT_IMP_TOW_KODYKRESKOWE.TOWARID,
      JEDNOSTKA type of column INT_IMP_TOW_KODYKRESKOWE.JEDNOSTKA,
      JEDNOSTKAID type of column INT_IMP_TOW_KODYKRESKOWE.JEDNOSTKAID,
      KODKRESKOWY type of column INT_IMP_TOW_KODYKRESKOWE.KODKRESKOWY,
      GLOWNY type of column INT_IMP_TOW_KODYKRESKOWE.GLOWNY,
      SKADTABELA type of column INT_IMP_TOW_KODYKRESKOWE.SKADTABELA,
      SKADREF type of column INT_IMP_TOW_KODYKRESKOWE.SKADREF,
      DEL type of column INT_IMP_TOW_KODYKRESKOWE.DEL,
      HASHVALUE type of column INT_IMP_TOW_KODYKRESKOWE.HASH,
      REC type of column INT_IMP_TOW_KODYKRESKOWE.REC,
      SESJA type of column INT_IMP_TOW_KODYKRESKOWE.SESJA)
  returns (
      REF type of column INT_IMP_TOW_KODYKRESKOWE.REF)
   as
begin
  insert into int_imp_tow_kodykreskowe (towarid, jednostka, jednostkaid, kodkreskowy, glowny,
      "HASH", del, rec, skadtabela, skadref, sesja)
    values (:towarid, :jednostka, :jednostkaid, :kodkreskowy, :glowny,
      :hashvalue, :del, :rec, :skadtabela, :skadref, :sesja)
    returning ref into :ref;
end^
SET TERM ; ^
