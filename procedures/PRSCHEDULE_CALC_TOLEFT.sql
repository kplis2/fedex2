--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDULE_CALC_TOLEFT(
      PRSCHEDULE integer,
      LASTOPER integer,
      OPERCOUNT integer)
  returns (
      SCHEDOPER integer,
      CNT integer)
   as
declare variable curtime timestamp;
declare variable schednum integer;
declare variable isoper smallint;
declare variable status smallint;
declare variable tstart timestamp;
declare variable tdepend timestamp;
declare variable tend timestamp;
declare variable machine integer;
declare variable verifiednum integer;
declare variable shoper integer;
declare variable worktime float;
declare variable workplace varchar(20);
declare variable prdepart varchar(20);
declare variable matreadytime timestamp;
declare variable laststatus smallint;
declare variable workload double precision;
declare variable tprevend timestamp;
declare variable rstart timestamp;
declare variable rend timestamp;
declare variable rmachine integer;
declare variable rschedoper integer;
declare variable latenes double precision;
declare variable minlatenes double precision;
declare variable minspace integer;
declare variable "ROUND" integer;
declare variable minlength double precision;
declare variable rthoper integer;
declare variable tdependr timestamp;
declare variable rstartfwt timestamp;
declare variable tstartfwt timestamp;
declare variable machinecount integer;
declare variable altoper smallint;
declare variable brakmat smallint;
declare variable sql varchar(1024);
declare variable c integer;
declare variable depfrom integer;
declare variable prganttref bigint;
declare variable rprganttref bigint;
begin
  c = 0;
  laststatus = 2;
  select mintimebetweenopers, roundstarttime,  fromdate, prdepart
      from prschedules
      where ref = :prschedule
      into :minspace, :round, :curtime, :prdepart;
  minlength = 1440;
  minlength = 1/:minlength;
  if(:round > :minspace or (:minspace is null) ) then minspace = :round;
  minlength = :minlength * :minspace;
  if(:lastoper = 0) then
  begin
    verifiednum = 0;
    --ustawianei poczatkowa readytoharm
    update prgantt set readytosched = 0 where prschedule = :prschedule and gtype = 0;
    for select a.ref
      from prgantt A
        left join prschedoperdeps d on (d.depto = a.prschedoper)
        left join prgantt b on (b.prschedoper = d.depfrom)
      where A.PRSCHEDULE = :prschedule and A.VERIFIED = 0 and a.gtype = 0 --and a.workplace = 'WIERTARKI PRZELOTOWE'
        and b.ref is null or b.status = 3 or b.verified > 0
      into :prganttref
    do begin
      update prgantt set readytosched = 1 where ref = :prganttref;
    end
  end else
    select VERIFIED, STATUS from prgantt where prschedoper=:lastoper into :verifiednum, :laststatus;
  cnt = 0;
  if(:curtime is null or (:curtime < current_timestamp(0)) ) then curtime = current_timestamp(0);
  while(:cnt < :opercount and :lastoper >= 0)
  do begin
    verifiednum = :verifiednum + 1;
    -- dla wszystkich operacji niezweryfikowanych, ktore nie posiadaja niezweryfikowanych operacji poprzedzajacych, okresl czas dostepnosci materialow
    for select first 1 a.ref
      from prgantt A
      where A.PRSCHEDULE = :prschedule and A.VERIFIED = 0
        and a.readytosched = 1
      order by coalesce(a.priority,0) desc,
        A.prschedule, A.verified,
        A.status desc
      into :prganttref
    do begin
      execute procedure PRSCHEDULE_CHECKMATERIALSTATE(:prganttref);
    end
    schedoper = null;
    rschedoper = null;
    minlatenes = null;
    --dla kazdejoperacji okrel czas "opoznienia" startu w stosunku do miejsca, w ktorym mozna posadzic operacje
    if(exists (select ref from prgantt where PRSCHEDULE = :prschedule and VERIFIED = 0 and readytosched = 1 and materialreadytime is not null)) then
       brakmat = 1;
    else
       brakmat = 0;
    for
      select a.ref, a.timefrom, a.timeto, a.STATUS, a.prMACHINE, a.prshoper, a.materialreadytime, a.workplace, a.worktime, a.prschedoper
        from PRGANTT A
      where A.PRSCHEDULE = :prschedule and A.VERIFIED = 0 and a.readytosched = 1
            and (a.materialreadytime is not null or a.verified=:brakmat)
      order by A.prschedule, A.verified, a.readytosched, a.priority desc
      into :prganttref, :tstart,  :tend, :status, :machine, :shoper, :matreadytime, :workplace, worktime, schedoper
    do begin
      if(:minlatenes <= :minlength) then
        break;  -- jesli ostatni odstep jest wystarczajaco maly, to nei szukam dalej
      tdepend = null;
      --okreslenie minimalnego czasu rozpoczecia
      select max(p.timeto)
        from prschedoperdeps d
          left join prgantt p on (p.prschedoper = d.depfrom)
        where d.depto = :schedoper
        into :tdepend;
      if(:tdepend is null) then tdepend = :curtime;
      if(:tstart is null or :tstart < :tdepend )then
        tstart = tdepend;
      --sprawdzenie zaleznosci materialowej
      if(:matreadytime > :tstart) then
        tstart  = :matreadytime;
      --sprawdzenie, czy start nie wypad w czasie przerwy pracy - jesli tak, to na poczatek kolejnego czasu
      execute procedure PRSCHED_CALENDAR_FIRSTWORKTIME(:prdepart,:tstart) returning_values
         :tstartfwt;
      if(:workplace <>'') then
      begin
        execute procedure prschedule_find_machine(:prganttref,:tstartfwt, 0) returning_values :tstart,  :tend, :machine;
 --       if (workplace = 'WIERTARKI PRZELOTOWE') then exception test_break tstart||' '||tend;

        if(:machine is null) then
        begin
          execute procedure prschedule_checkoperstarttime(:prganttref, :tstartfwt) returning_values :tstart;
          execute procedure pr_calendar_checkendtime(:prdepart, :tstart, :worktime) returning_values :tend;
        end
        --sprawdzenie opoznienia w stosunku do poprzedzajacej oepracji na danej maszynie
        tprevend = null;
        select max (timeto)
        from prgantt
        where prschedule = :prschedule
          and prmachine = :machine
          and timeto <= :tstart
          and ref <> :prganttref
        into :tprevend;
        if(:tprevend < :curtime) then
           tprevend = null;--jesli poprzednie operacje już si zakonczyly/powinny byly zakonczyc, to tak, jakby ich nie bylo
        if(:tprevend is null) then latenes = 0;
        else latenes = :tstart - :tprevend;
        if(:minlatenes is null or (minlatenes > :latenes))then
        begin
          rschedoper = schedoper;
          rprganttref = prganttref;
          rmachine = :machine;
          rstart = :tstart;
          rend = :tend;
          minlatenes = :latenes;
          rstartfwt = :tstartfwt;
        end
      end else begin
        machine = null;
        execute procedure prschedule_checkoperstarttime(:prganttref, :tstart) returning_values :tstart;
        execute procedure pr_calendar_checkendtime(:prdepart, :tstart, :worktime) returning_values :tend;
        rschedoper = schedoper;
        rprganttref = prganttref;
        rmachine = machine;
        rstart = :tstart;
        rstartfwt = :tstart;
        rend = :tend;
        minlatenes = 0;
      end
      c = :c + 1;
--      if (rschedoper is not null and rstart is not null and rend is not null) then
  --      break;
    end
    if(:rschedoper is null) then
    begin
      -- zakończenie harmonogramowania operacji
      prganttref = -1;
      exit;
    end
    update prgantt set timefrom = :rstart, timeto = :rend, prMACHINE = :rmachine, verified  = :verifiednum
      where ref=:rprganttref;
    --ustawianie readytoharm na innych
    for select o.ref
      from prschedoperdeps d
      left join prschedopers o on (o.ref = d.depto)
      where d.depfrom = :rschedoper and o.activ = 1
      into :rthoper
    do begin
      execute procedure prschedule_readytosched_check(:rthoper);
    end
    schedoper = rschedoper;
    cnt = :cnt + 1;
  end
end^
SET TERM ; ^
