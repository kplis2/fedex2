--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZZAM_OBL(
      POZZAMREF integer)
   as
DECLARE VARIABLE ILZREAL NUMERIC(14,4);
DECLARE VARIABLE ILZREALM NUMERIC(14,4);
DECLARE VARIABLE ILZDYSP NUMERIC(14,4);
DECLARE VARIABLE ILZADYSP NUMERIC(14,4);
DECLARE VARIABLE IL NUMERIC(14,4);
DECLARE VARIABLE ILD NUMERIC(14,4);
DECLARE VARIABLE ILM NUMERIC(14,4);
DECLARE VARIABLE ILONKLON NUMERIC(14,4);
DECLARE VARIABLE ILONKLONM NUMERIC(14,4);
DECLARE VARIABLE TYP INTEGER;
DECLARE VARIABLE WARTRMAG NUMERIC(14,2);
DECLARE VARIABLE JEDNMAG INTEGER;
DECLARE VARIABLE JEDN INTEGER;
DECLARE VARIABLE PRZELICZ NUMERIC(14,4);
declare variable wydania smallint;
begin
   select POZZAM.ILREAL,POZZAM.ILREALM, POZZAM.ILDYSP,NAGZAM.TYP,typzam.wydania,
          POZZAM.JEDN, TOWJEDN.REF, coalesce(towjedn.przelicz,1)
   from POZZAM
     left join NAGZAM on ( NAGZAM.REF=POZZAM.ZAMOWIENIE)
     left join typzam on (typzam.symbol = nagzam.typzam)
     left join TOWJEDN on (TOWJEDN.KTM = POZZAM.KTM and TOWJEDN.glowna = 1)
   where POZZAM.REF=:pozzamref
   into :ilzreal, :ilzrealm, :ilzdysp,:typ,:wydania,:jedn, :jednmag, :przelicz;

   /*ilosc zrealizowana to ilosc realizowana daną pozycją plus ilosci zrealizowane
     z pozycji klonow*/
   if(:ilzreal is null or (:typ <2)) then ilzreal = 0;
   if(:ilzrealm is null or (:typ <2)) then ilzrealm = 0;
   ilzdysp = 0;
   il = null;
   select sum(ilosc),sum(iloscm) from POZZAM where POZZAM.popref = :pozzamref and pozzam.blokadarez = 0 into :ilonklon,:ilonklonm;
   select sum(ILZREAL), sum(ilzrealm) from POZZAM where POZZAM.popref = :pozzamref into :il, :ilm;
   if(:il is null) then il = 0;
   if(:ilm is null) then ilm = 0;
   ilzreal = :ilzreal + :il;
   ilzrealm = :ilzrealm + :ilm;
   ilm = null;
   il = null;
   if (wydania = 3) then
   begin
     select sum(g.amountzreal)
       from prschedguides g
       where g.pozzam = :pozzamref
       into il;
     ilzreal = :ilzreal + :il;
     ilzrealm = :ilzrealm + :il;
   end

   /* Aktualizacja ilosci na podstawie dyspozycji */
   il = null;
   select sum(p.ilosc)
     from dokumpoz p
       left join dokumnag n on (p.dokument = n.ref)
     where p.frompozzam = :pozzamref
       and n.mwsdisposition = 1
     into :il;
   if(:il is null ) then
     il = 0;
   ilzdysp = :ilzdysp + il;

/*   select sum(KILOSC) from NAGZAM where KPOPREF = :pozzamref and KISREAL = 1 into :ilm;
   if(:ilm is null) then ilm = 0;
   if(:ilm > 0) then begin

     if(:jedn <> :jednmag) then
       select przelicz from TOWJEDN where REF=:jedn into :przelicz;
     else
       przelicz = 1;
     il = :ilm/:przelicz;
     ilzreal = :ilzreal + :il;
     ilzrealm = :ilzrealm + :ilm;
   end*/
   /* ilosć rozdysponowana to ilosc z zamowien pochodnych oraz
     ilosc rozdysponowana z klonów oraz ilosc z kompletacji pochodnych*/
   il = 0;
   select sum(ILOSC) from POZZAM where POZZAM.genpozref = :pozzamref into :il;
   if(:il is null) then il = 0;
   ilzdysp = :ilzdysp + :il;
   il = null;
   select sum(ILZDYSP) from POZZAM where POPREF = :pozzamref into :il;
   if(:il is null) then il = 0;
   ilzdysp = :ilzdysp + :il;
   il = null;
   select sum(KILOSC) from NAGZAM where KPOPREF = :pozzamref and KISREAL = 0 and KINNE = 0 into :il;
   if(:il is null) then il = 0;
   ilzdysp = :ilzdysp + :il;
   il = null;
   select sum(r.quantityto)
     from relations r
     where r.stableto = 'POZZAM' and r.srefto = :pozzamref and r.act > 0
       and r.relationdef = 1
     into il;
   if(:il is null) then il = 0;
   ilzdysp = :ilzdysp + :il;
   /*ilosc zadysponowana(realizowanych dyspozycji) to ilosc zrealizowana z zzamowien podlaczonych
    oraz ilosc zadysponowana z klonów*/
    ilzadysp = 0;
   il = null;
   select sum(ILZREAL) from POZZAM where POZZAM.GENPOZREF = :pozzamref into :il;
   if(:il is null) then il = 0;
   ilzadysp = :ilzadysp + :il;
   il = null;
   select sum(ILZADYSP) from POZZAM where POPREF = :pozzamref into :il;
   if(:il is null) then il = 0;
   ilzadysp = :ilzadysp + :il;
   il = null;
   select sum(KILZREAL) from NAGZAM where KPOPREF = :pozzamref and KISREAL = 0 and KINNE = 0 into :il;
   if(:il is null) then il = 0;
   ilzadysp = :ilzadysp + :il;
   il = null;
   select sum(r.quantitytoreal)
     from relations r
     where r.stableto = 'POZZAM' and r.srefto = :pozzamref and r.act > 0
       and r.relationdef = 1
     into il;
   if(:il is null) then il = 0;
   ilzadysp = :ilzadysp + :il;
   /*wartoć magazynowa to suma wartoci magazynowych z klonów oraz
     suma z dokumentów magazynowych wystawionych przez ta pozycj*/
   il = null;
   ild = NULL;
   select sum(P.WARTOSC),
     sum(case when P.WARTREALTRYB=2 then P.ILOSC else 0 end),
     sum(case when P.WARTREALTRYB=3 then P.ILOSC else 0 end)
     from DOKUMPOZ P
     left join DOKUMNAG N on (N.ref=P.dokument)
     where P.WARTREALPOZ = :pozzamref and N.AKCEPT in (1,8)
   into :wartrmag,:il,:ild;
   if(:wartrmag is null) then wartrmag = 0;
   if(:il is null) then il = 0;
   if(:ild is null) then ild = 0;
   ilzdysp = :ilzdysp + :ild;
   ilzrealm = :ilzrealm + :il;
   ilzreal = :ilzreal + :il;
   update POZZAM set
      WARTRMAG = :wartrmag,
      ILONKLON = :ilonklon,
      ILONKLONM = :ilonklonm,
      ILZREAL = :ilzreal,
      ILZREALM = :ilzrealm,
      ILZDYSP = :ilzdysp,
      ILZADYSP = :ilzadysp
    where REF=:pozzamref;
end^
SET TERM ; ^
