--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CREATE_SHIPPINGPOINT(
      SYMBOL varchar(10) CHARACTER SET UTF8                           ,
      POINTTYPE char(1) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable SHIPPINGPOINTSYMB varchar(20);
declare variable FNAME STRING60;
declare variable SNAME STRING60;
declare variable CITY CITY_ID;
declare variable POSTCODES POSTCODE;
declare variable STREET STREET_ID;
declare variable HOUSENO STRING10;
declare variable LOCALNO STRING10;
declare variable PHONE PHONES_ID;
declare variable EMAIL STRING255;
declare variable OTABLE varchar(31);
begin
/* pointtype:
M - magazyn
O - oddzial
*//*$$IBEC$$ 
  shippingpointsymb = '';
  status = 1;
  msg = '';

  if (:pointtype = 'M') then
  begin
    otable = 'DEFMAGAZ';
    select 'Magazyn: '||d.symbol, o.imie, o.nazwisko, d.dmiasto, d.dkodp,
        d.dulica, d.dnrdomu, d.dnrlokalu, d.dtelefon, d.demail
      from defmagaz d
        left join operator o on (d.dosoba = o.ref)
      where d.symbol = :symbol
    into :shippingpointsymb, :fname, :sname, :city, :postcodes,
      :street, :houseno, :localno, :phone, :email;
  end
  else if (:pointtype = 'O') then
  begin
    otable = 'ODDZIALY';
    select 'Oddział: '||o.oddzial, op.imie, op.nazwisko, o.city, o.postcode,
        o.street, o.houseno, o.localno, o.phone, o.email
      from oddzialy o
        left join operator op on (o.operator = op.ref)
      where o.oddzial = :symbol
    into :shippingpointsymb, :fname, :sname, :city, :postcodes,
      :street, :houseno, :localno, :phone, :email;
  end
  else
    exception universal 'Nie obsługiwany typ miejsca nadania przesyłki';

  if (coalesce(:shippingpointsymb,'') = '') then
  begin
    msg = 'Nie znaleziono obiektu';
    status = 0;
    exit;
  end

  if (coalesce(:fname,'') = '') then msg = :msg||iif(:msg = '', '', ', ')||'imienia';
  if (coalesce(:sname,'') = '') then msg = :msg||iif(:msg = '', '', ', ')||'nazwiska';
  if (coalesce(:city,'') = '') then msg = :msg||iif(:msg = '', '', ', ')||'miasta';
  if (coalesce(:postcodes,'') = '') then msg = :msg||iif(:msg = '', '', ', ')||'kodu pocztowego';
  if (coalesce(:street,'') = '') then msg = :msg||iif(:msg = '', '', ', ')||'ulicy';
  if (coalesce(:houseno,'') = '') then msg = :msg||iif(:msg = '', '', ', ')||'numeru domu';
  if (coalesce(:phone,'') = '') then msg = :msg||iif(:msg = '', '', ', ')||'telefonu';

  if (msg <> '') then
  begin
    msg = 'Nie podano: '||:msg;
    status = 0;
  end

  if (shippingpointsymb <> '' and status = 1) then
  begin
    if (not exists(select first 1 1 from shippingpoints s where s.symbol = :shippingpointsymb)) then
      insert into shippingpoints(symbol, fname, sname, city, postcode,
          street, houseno, localno, phone, email, otable, oref)
        values(:shippingpointsymb, :fname, :sname, :city, :postcodes,
          :street, :houseno, :localno, :phone, :email, :otable, :symbol);
    else
      update shippingpoints
        set fname = :fname, sname = :sname, city = :city, postcode = :postcodes,
          street = :street, houseno = :houseno, localno = :localno, phone = :phone, email = :email
        where symbol = :shippingpointsymb
          and otable = :otable
          and oref = :symbol;
  end $$IBEC$$*/
end^
SET TERM ; ^
