--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RK_WYSTAW_DOKUMENT(
      DOKUMENT integer)
  returns (
      STATUS integer)
   as
declare variable CZY_ROZRACH varchar(255);
declare variable CZY_ROZR integer;
declare variable DATA timestamp;
declare variable ROZR varchar(20);
declare variable KWOTA numeric(14,2);
declare variable PM char(1);
declare variable STANOWISKO char(2);
declare variable NUMER integer;
declare variable TYP char(3);
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable NEWSLODEF integer;
declare variable NEWSLOPOZ integer;
declare variable FAKTURA integer;
declare variable KOD varchar(100);
declare variable NAZWA varchar(255);
declare variable KONTOFK ACCOUNT_ID;
declare variable WALUTA varchar(3);
declare variable WALUTOWY integer;
declare variable DATAROZ timestamp;
declare variable OPIS varchar(1024);
declare variable SYMBOL varchar(30);
declare variable GETNUMDOK varchar(40);
declare variable BLOKADA smallint;
declare variable BTRANS integer;
declare variable KWOTA_WN numeric(15,2);
declare variable KWOTA_MA numeric(15,2);
declare variable RKDOKPOZ_REF integer;
declare variable KWOTAZL numeric(14,2);
declare variable KURS numeric(14,4);
declare variable WINIENZL numeric(14,2);
declare variable MAZL numeric(14,2);
declare variable SMULTICURR smallint;
declare variable PERIOD varchar(6);
declare variable NUMEROLD integer;
declare variable REF integer;
declare variable POZOPER integer;
declare variable REFNAG integer;
declare variable KONTO ACCOUNT_ID;
declare variable COMPANY integer;
declare variable RKSTNKASPIN varchar(20);
declare variable TRUEPIN varchar(20);
declare variable RKDOKROZ_REF integer;
declare variable WALUTOWA integer;
declare variable NUMBLOCKGET integer;
declare variable HASOWNROZRACHP smallint;
begin
  STATUS = -1;

  -- zapytanie o dane pomocnicze dla operacji
  select D.blokada, D.slodef, D.slopoz, D.data, D.typ, D.kwota, D.numer, d.numblockget,
         R.stanowisko, D.opis, D.btransfer, R.okres, K.company, K.pin, D.rkstnkaspin, K.walutowa
    from RKDOKNAG D
    join RKRAPKAS R on (R.ref = D.raport)
    join rkstnkas K on (k.kod = R.stanowisko)
    where D.ref=:dokument
    into :blokada, :slodef, :slopoz, :data, :typ, :kwota,:numer, :numblockget,
     :stanowisko, :opis, :btrans, :period, :company, :truepin, :rkstnkaspin, :walutowa;
  hasownrozrachp = 0;
  if (numer>0 and blokada=0) then
  begin
    status = 2; -- Dokument do operacji jest już wystawiony. Drukować dokument?
    exit;
  end

  if (slodef is null) then
    slodef = 0;
  if (slopoz is null) then
    slopoz = 0;

 --if (kwota is null or kwota<=0) then
   --exception RK_ZEROWA_WARTOSC;

  /* sprawdzenie pinu*/
  if(:rkstnkaspin is null) then rkstnkaspin = '';
  if(:truepin is not null and :truepin<>'') then begin
    if(:truepin<>:rkstnkaspin) then exception RKRAPKAS_WRONG_PIN;
  end

  if (numer is null or numer=0) then
  begin
    select gennumdok from RKSTNKAS
      where KOD=:stanowisko
      into :getnumdok;

    execute procedure GET_NUMBER(:getnumdok, :stanowisko, :typ, :data, numblockget, :dokument)
      returning_values :numer, :symbol;
    update RKDOKNAG set NUMER=:numer, numblockget = 0 where REF=:dokument;
  end
  status = 1;

  execute procedure GET_CONFIG('KBAUTOROZRACH', 2)
    returning_values :czy_rozrach;
  czy_rozr = cast(czy_rozrach as integer);
  -- (ZP) Z tego co widze znaczenie KBAUTOROZRACH to:
  -- 0 - niezakadaj rozrachunków
  -- 1 - zakadaj tylko jeli rozrachunek juz istnieje
  -- 2 - zawsze zakladaj rozrachunek
  if (czy_rozr > 0) then begin
    for
      select P.ref, P.rozrachunek, P.faktura, P.kwota, P.pm, P.konto, P.kwotazl, O.smulticurr
        from RKDOKPOZ P
          left join rkpozoper O on (O.ref=P.pozoper)
        where DOKUMENT=:dokument and (O.rozrachblok is null or O.rozrachblok=0)
        into :rkdokpoz_ref, :rozr, :faktura, :kwota, :pm, :kontofk, :kwotazl, :smulticurr
    do begin
      if (rozr is not null and rozr<>'') then
      begin
        if (kontofk is null or kontofk='' or kontofk = '???') then
          execute procedure SLO_DANE(:slodef, :slopoz)
            returning_values :kod, :nazwa, :kontofk;
        else begin
          execute procedure get_dictdefpos4settlement(company, period, kontofk)
            returning_values :newslodef, :newslopoz;
          if (newslodef is not null) then slodef = newslodef;
          if (newslopoz is not null) then slopoz = newslopoz;
        end

        if (not exists(select company from rozrach
              where company = :company and slodef=:slodef and slopoz = :slopoz
                and symbfak = :rozr and kontofk = :kontofk)) then
        begin
          if (czy_rozr=1) then
            exception RK_BRAK_ROZRACHUNKU;

          if (czy_rozr=2) then
          begin
            select walutowy, waluta, kurs, data
              from RKDOKNAG where ref=:dokument
              into :walutowy, :waluta, :kurs, :dataroz;

            if (smulticurr is null or smulticurr<>1) then
            begin
              execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
                returning_values :waluta;
              walutowy = 0;
              kurs = 0;
              kwota = kwotazl;
            end
          end

          insert into ROZRACH (company, SLODEF, SLOPOZ, SYMBFAK, FAKTURA, DATAOTW, DATAPLAT, WALUTOWY, WALUTA, KONTOFK, SKAD)
            values(:company, :slodef, :slopoz,:rozr, :faktura, :dataroz, :dataroz, :walutowy, :waluta, :kontofk, 2);
        end

        select walutowy from rozrach
          where company = :company and slodef=:slodef and slopoz = :slopoz
            and symbfak = :rozr and kontofk = :kontofk
          into :walutowy;

        if (walutowa = 0) then
        begin
          if (walutowy = 0) then begin
            kurs = 0;
            kwota = kwotazl;
          end
          if ((pm='+' and kwota > 0) or (pm = '-' and kwota < 0)) then
          begin
            kwota_wn = 0;
            winienzl = 0;
            kwota_ma = abs(kwota);
            mazl = abs(kwotazl);
          end else begin
            kwota_wn = abs(kwota);
            winienzl = abs(kwotazl);
            kwota_ma = 0;
            mazl = 0;
          end

          insert into ROZRACHP(company, SLODEF, SLOPOZ, SYMBFAK, kontofk, FAKTURA, DATA, TRESC, WINIEN, MA, SKAD, STABLE, SREF, kurs, winienzl, mazl, STRANSDATE)
            values (:company, :slodef, :slopoz, :rozr, :kontofk, :faktura, :data, :opis, :kwota_wn, :kwota_ma, 2, 'RKDOKPOZ', :rkdokpoz_ref, :kurs, :winienzl, :mazl, :data);
        end else begin
          for
            select r.kwota, s.kurs, r.ref
              from rkdokroz r
                join rkstanwal s on (r.rkstanwal = s.ref)
              where r.rkdokpoz = :rkdokpoz_ref
                and coalesce(r.anulata,0) = 0
              into :kwota, :kurs, :rkdokroz_ref
          do begin
            if (:kwota > 0) then
            begin
              kwota_wn = 0;
              winienzl = 0;
              kwota_ma = abs(kwota);
              mazl = abs(kwota * :kurs);
            end else begin
              kwota_wn = abs(kwota);
              winienzl = abs(kwota * :kurs);
              kwota_ma = 0;
              mazl = 0;
            end

            insert into ROZRACHP(company, SLODEF, SLOPOZ, SYMBFAK, kontofk, FAKTURA, DATA, TRESC, WINIEN, MA, SKAD, STABLE, SREF, kurs, winienzl, mazl, STRANSDATE)
              values (:company, :slodef, :slopoz, :rozr, :kontofk, :faktura, :data, :opis, :kwota_wn, :kwota_ma, 2, 'RKDOKROZ', :rkdokroz_ref, :kurs, :winienzl, :mazl, :data);

          end
        end
        hasownrozrachp = 1;

      end
    end
    if(:hasownrozrachp = 1) then begin
        --jesli operacja ma powiazany przelew, to zaznacza go jako zaksiegowany - bo naniesiony na rozrachunki
        if (btrans>0) then
         update BTRANSFERS set STATUS=4
          where ref=:btrans and STATUS<>4;
    end
  end
end^
SET TERM ; ^
