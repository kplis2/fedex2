--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_CALC_VERIFY(
      SCHEDULE integer)
   as
declare variable curtime timestamp;
declare variable schednum integer;
declare variable isoper smallint;
declare variable status smallint;
declare variable tstart timestamp;
declare variable tdepend timestamp;
declare variable tend timestamp;
declare variable machine integer;
declare variable verifiednum integer;
declare variable schedoper integer;
declare variable shoper integer;
declare variable worktime float;
declare variable workplace varchar(20);
declare variable prdepart varchar(20);
declare variable losttime float;
declare variable schedzam integer;
declare variable matready timestamp;
declare variable matstatus smallint;
declare variable operendtime timestamp;
begin
  --analiza po kolei operacji tak
  isoper = 1;
  verifiednum = 1;
  select prschedules.prdepart from prschedules where ref=:schedule into :prdepart;
  while (:isoper = 1) do begin
    --wybor operacji do analizy
    schedoper = null;
    select first 1 A.REF, A.materialreadytime, a.statusmat,
      a.starttime, a.endtime, a.status, a.machine, a.worktime, a.shoper
    from PRSCHEDOPERS A
      left join prschedoperdeps on (prschedoperdeps.depto = A.ref)
      left join prschedopers B on (prschedoperdeps.depfrom = B.REF and B.verified = 0)
      left join prschedopers M on (m.schedule = A.schedule and m.machine = a.machine and m.starttime < a.starttime and m.verified = 0)
    where A.SCHEDULE = :schedule and A.VERIFIED = 0 and A.starttime is not null --tylko operacje, ktore wczeniewj juz byly zharmongoramowane
--      and A.timeconflict > 0
      and B.ref is null--operacje, ktore nie maja niezweryfikowanych poprzednikow
      and M.ref is null--opreacje, ktore nie maja niezwyryfikowanych operacji poprzedzajacych na maszynie
    order by A.starttime, A.schedzamnum, A.GUIDENUM
    into :schedoper, :matready, :matstatus,
     tstart,  :tend, :status, :machine, :worktime, :shoper;
    if(:schedoper is null) then begin
      --zakonczenei oepracji
      update PRSCHEDOPERS set verified = 1, calcdeptimes = (case when calcdeptimes = 0 then 2 else calcdeptimes  end)
       where verified = 0;--bo nie wszystkie musialy podejsc pod werfikacje, co zostaly zazanczone

      exit;
    end
    workplace = null;
    select prshopers.workplace from prshopers where ref=:shoper into :workplace;
    --okreslenie minimalnego czasu rozpoczecia
    tdepend = null;
    select prschedules.fromdate from prschedules join prschedzam on (prschedzam.prschedule = prschedules.ref)
      where prschedzam.ref=:schedzam into :curtime;
    if(:curtime is null) then curtime = current_time;
    --operacje poprzedzające w przewodniku
    select max( endtime) from prschedopers p join prschedoperdeps on (prschedoperdeps.depfrom = p.ref)
          where prschedoperdeps.depto = :schedoper into :tdepend;
    if(:tdepend is null) then tdepend = :curtime;
    if(:tstart is null or :tstart < :tdepend )then begin
      tstart = tdepend;
    end
    --operacje poprzedzajace na maszynie
    if(:workplace is not null) then begin
      execute procedure prsched_find_machine(:schedoper,:tstart, 1) returning_values :tstart,  :tend, :machine;
    end else begin
      --wyrównanie czasu startu
      execute procedure prsched_checkoperstarttime(:schedoper, :tstart) returning_values :tstart;
      --sprawdzenie, czy start nie wypad w czasie przerwy pracy - jesli tak, to na poczatek kolejnego czasu
      execute procedure PRSCHEDOPER_CHECKREALWORKTIME(:schedoper, :machine, :tstart, :worktime) returning_values :worktime;
    end
    update prschedopers set STARTTIME = :tstart, ENDTIME = :tend, MACHINE = :machine,
    verified  = :verifiednum, WORKTIME = :worktime,
      calcdeptimes = 1--naliczenie wlasnych dependencies i sprawdzenie, dla tych, co nie bdą podlegać weryfikacji, czy przypadkiem nie ma konfliktow
      where ref=:schedoper;
    verifiednum = :verifiednum + 1;
  end
      update PRSCHEDOPERS set verified = 1, calcdeptimes = (case when calcdeptimes = 0 then 2 else calcdeptimes  end)
       where verified = 0;--bo nie wszystkie musialy podejsc pod werfikacje, co zostaly zazanczone
end^
SET TERM ; ^
