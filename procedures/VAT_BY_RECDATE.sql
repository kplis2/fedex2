--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE VAT_BY_RECDATE(
      OTABLE varchar(30) CHARACTER SET UTF8                           ,
      OREF integer,
      DATADOK timestamp,
      TERMPLAT timestamp)
  returns (
      VATDATE timestamp)
   as
declare variable DATA timestamp;
declare variable DATASPRZ timestamp;
begin
/*PL: Procedura wprowadza kompatybilnosc na przelomie lat 2013-2014,
      ze wzgledu na zmiany w przepisach.(PR57428)
*/
   if(OTABLE = 'NAGFAK') then
   begin
    select dataodbioru
      from nagfak
      where ref = :oref
    into :data;
    if (:data is not null) then
      vatdate = data;
    else
      vatdate = null;
   end
  suspend;
end^
SET TERM ; ^
