--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_WORKEDHOURS(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      AMOUNT numeric(14,2))
   as
declare variable frvhdr integer;
declare variable frpsn integer;
declare variable frcol integer;
declare variable cacheparams varchar(255);
declare variable ddparams varchar(255);
declare variable iyear smallint;
declare variable imonth smallint;
declare variable company smallint;
declare variable frversion integer;
begin
--Czas przepracowany od początku roku (wyliczany tylko w miesiącu III, VI, IX i XII) - DG-1
  select frvhdr, frpsn, frcol
    from frvpsns
    where ref = :frvpsn
    into :frvhdr, :frpsn, :frcol;

  select company, frversion
    from frvhdrs
    where ref = :frvhdr
    into :company, :frversion;
  cacheparams = period||';'||company;
  ddparams = '';

  if (not exists (select ref from frvdrilldown where functionname = 'WORKEDHOURS' and cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr)) then
  begin
    iyear = cast(substring(period from 1 for 4) as integer);
    imonth = cast(substring(period from 5 for 2) as smallint);

    select sum(EP.pvalue)
      from epayrolls PR join eprpos EP on (EP.payroll = PR.ref)
      where EP.ecolumn in (520,600,610) and PR.cper starting with :iyear and PR.cper <= :period
        and PR.company = :company
      into :amount;
  end else
  select first 1 fvalue from frvdrilldown where cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr
    into :amount;


  amount = coalesce(amount,0);

  insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
     values (:frvhdr, :frpsn, :frcol, 'WORKEDHOURS', :cacheparams, :ddparams, :amount, :frversion);

  if (mod(imonth, 3) > 0) then
    amount = null;

suspend;
end^
SET TERM ; ^
