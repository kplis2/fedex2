--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHCALC_UPDATE_EXCLUDED(
      PRCALCCOL integer)
   as
declare variable fromprshoper integer;
declare variable isalt smallint;
declare variable prshoperalt integer;
declare variable calc integer;
declare variable ref integer;
declare variable excluded smallint;
begin
  select fromprshoper, isalt, prshoperalt, calc, excluded from prcalccols where ref=:prcalccol
  into :fromprshoper, :isalt, :prshoperalt, :calc, :excluded;

    -- zmien znaczniki na operacjach podrzednych
    for select ref from prshopers where opermaster=:fromprshoper
    into :ref
    do begin
      if(:isalt=1 and :ref<>:prshoperalt) then
        update prcalccols set excluded=1 where calc=:calc and fromprshoper=:ref and calctype=0;
      else
        update prcalccols set excluded=:excluded where calc=:calc and fromprshoper=:ref and calctype=0;
    end

    -- zmien znaczniki na surowcach pod operacje
    for select ref from prshmat where opermaster=:fromprshoper
    into :ref
    do begin
      update prcalccols set excluded=:excluded where calc=:calc and fromprshmat=:ref and calctype=0;
    end

    -- zmien znaczniki na dodatkach pod operacje
    for select ref from prshtools where opermaster=:fromprshoper
    into :ref
    do begin
      update prcalccols set excluded=:excluded where calc=:calc and fromprshtool=:ref and calctype=0;
    end

end^
SET TERM ; ^
