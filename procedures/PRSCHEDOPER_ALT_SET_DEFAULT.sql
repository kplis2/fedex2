--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDOPER_ALT_SET_DEFAULT(
      GUIDEREF integer)
   as
declare variable prsheet integer;
declare variable prshoper integer;
declare variable prshoperrep integer;
declare variable reported smallint;
declare variable complex smallint;
declare variable operdefault integer;
begin
  -- aktywowanie operacji dla naglowka przewodnika
  select prsheet from prschedguides where ref = :guideref into :prsheet;
  for select ref, reported, complex, operdefault
    from prshopers
    where sheet = :prsheet and opermaster is null
    into :prshoper, :reported, :complex, :operdefault
  do begin
    if(complex = 0) then
      update prschedopers set activ = 1 where guide = :guideref and shoper = :prshoper;
    else if(complex = 1) then
      execute procedure prschedoper_alt_set(:guideref, :prshoper, 2, :operdefault);
  end
  -- aktywowanie operacji dla pozycji posrednich
  for
    select p.prsheet
    from prschedguidespos p
    where p.prschedguide = :guideref and p.prsheet <> :prsheet
    into prsheet
  do begin
    for select ref, reported, complex, operdefault
      from prshopers
      where sheet = :prsheet and opermaster is null
      into :prshoper, :reported, :complex, :operdefault
    do begin
      if(complex = 0) then
        update prschedopers set activ = 1 where guide = :guideref and shoper = :prshoper;
      else if(complex = 1) then
        execute procedure prschedoper_alt_set(:guideref, :prshoper, 2, :operdefault);
    end
  end
end^
SET TERM ; ^
