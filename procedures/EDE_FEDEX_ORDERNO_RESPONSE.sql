--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_ORDERNO_RESPONSE(
      EDEDOCSH_REF integer)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
   as
declare variable shippingdoc integer;
declare variable shippingdocno varchar(80);
begin
  select oref
    from ededocsh
    where ref = :ededocsh_ref
  into :shippingdoc;

  select p.val
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.name = 'numerDokumentu'
  into :shippingdocno;

  update listywys l set l.symbolzewn = :shippingdocno
    where l.ref = :shippingdoc;
  suspend;
end^
SET TERM ; ^
