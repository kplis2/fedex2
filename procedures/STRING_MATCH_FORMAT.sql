--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STRING_MATCH_FORMAT(
      TOPARSE STRING,
      SUBSTRLEN smallint,
      DELIM varchar(1) CHARACTER SET UTF8                           ,
      LINENO smallint = 0,
      CUTCHAR varchar(1) CHARACTER SET UTF8                            = '.',
      SUBDELIM varchar(1) CHARACTER SET UTF8                            = ' ')
  returns (
      PARSEDSTRING STRING)
   as
declare variable delimpos smallint;
declare variable subdelimpos smallint = 1;
declare variable line string;
declare variable linelength smallint;
declare variable newline string;
declare variable lines smallint = 0;
begin

  /* TS: Procedura zwraca tekst zgodny z podanymi parametrami.
     Zwracany tekst skladac sie bedzie z okreslonej liczby linii (LINENO),
     gdzie kazda z nich sklada sie z co najwyzej SUBSTRLEN znakow oraz
     od nastepnej jest oddzielona znakiem SUBSTRLEN.
     W przypadku, gdy dana linia (fragment tekstu wejsciowego miedzy znakami DELIM)
     zawiera wiecej niz SUBSTRLEN znakow, kolejne jego fragmenty (oddzielone znakiem SUBDELIM)
     sa skracane do pierwszego ich znaku, po czym konczone znakiem CUTCHAR.
     Na przykad:
       execute procedure string_match_format('Ala ma bardzo; ladnego kota.', 8, ';', 1, ',')
       >> ' A, m, b,'
  */

  toparse = :toparse||:delim;
  delimpos = position(:delim, :toparse);
  while (:delimpos > 0) do
  begin
    line = substring(:toparse from 1 for :delimpos - 1)||:subdelim;
    linelength = coalesce(char_length(:line),0); -- [DG] XXX ZG119346
    newline = '';
    while (linelength > :substrlen and :subdelimpos > 0) do  --skracaj kolejna linie
    begin
      subdelimpos = position(:subdelim, :line);
      if (:subdelimpos > 0) then
      begin
        newline = :newline||substring(:line from 1 for 1)||:cutchar||:subdelim;  --skracaj pierwszy wyraz
        line = substring(line from :subdelimpos + 1 for :linelength - :subdelimpos);
        linelength = coalesce(char_length(:newline),0) + coalesce(char_length(:line),0);  --sprawdz czy dlugosc linii jest juz ok -- [DG] XXX ZG119346
      end else
      begin
        newline = substring(trim(:newline) from 1 for :linelength);
      end
    end
    --line = left(:line, coalesce(char_length(:line),0) - 1); -- [DG] XXX ZG119346
    toparse = trim(substring(:toparse from :delimpos + 1 for coalesce(char_length(:toparse),0) - :delimpos)); -- [DG] XXX ZG119346
    delimpos = position(:delim, :toparse);
    newline = :newline||:line;
    if (:lineno = 0 or :lines < :lineno) then
    begin
      if (coalesce(char_length(trim(:newline)),0) = 0) then  --jesli pomiedzy separatorami byly same biale znaki -- [DG] XXX ZG119346
        parsedstring = :parsedstring||:delim||:newline;
      else
        parsedstring = :parsedstring||:delim||:subdelim||:newline;
      parsedstring = trim(:parsedstring);
      lines = :lines + 1;
    end else break;
  end
  parsedstring = right(:parsedstring, coalesce(char_length(:parsedstring),0) - 2); --usuniecie pierwszego delim i subdelim -- [DG] XXX ZG119346
  suspend;
end^
SET TERM ; ^
