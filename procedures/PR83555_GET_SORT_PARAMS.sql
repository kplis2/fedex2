--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR83555_GET_SORT_PARAMS(
      SORTERID integer,
      COLIN integer = null,
      ROWIN integer = null,
      SIDEIN integer = null)
  returns (
      COLOUT integer,
      ROWOUT integer,
      SIDEOUT integer,
      LABEL STRING)
   as
begin
  sideout = 1;
  while (sideout <= 2) do
  begin
    rowout = 1;
    while (rowout <= 3) do
    begin
      colout = 1;
      while (colout <= 3) do
      begin
        if ((rowin is null or rowin = rowout) and
            (colin is null or colout = colin) and
            (sidein is null or sideout = sidein)) then
        begin
          --label = 'Kubeł ' || rowout || colout || iif(sideout = 1, 'L', 'P');
          label='';
          suspend;
        end
        colout = colout + 1;
      end
      rowout = rowout + 1;
    end
    sideout = sideout + 1;
  end
end^
SET TERM ; ^
