--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_TOW_TOWARY_INS(
      TOWARID type of column INT_IMP_TOW_TOWARY.TOWARID,
      SYMBOL type of column INT_IMP_TOW_TOWARY.SYMBOL,
      NAZWA type of column INT_IMP_TOW_TOWARY.NAZWA,
      GRUPA type of column INT_IMP_TOW_TOWARY.GRUPA,
      GRUPAID type of column INT_IMP_TOW_TOWARY.GRUPAID,
      RODZAJ type of column INT_IMP_TOW_TOWARY.RODZAJ,
      RODZAJID type of column INT_IMP_TOW_TOWARY.RODZAJID,
      AKTYWNY type of column INT_IMP_TOW_TOWARY.AKTYWNY,
      VAT type of column INT_IMP_TOW_TOWARY.VAT,
      OPIS type of column INT_IMP_TOW_TOWARY.OPIS,
      CENAZAKUPUNET type of column INT_IMP_TOW_TOWARY.CENAZAKUPUNET,
      CENAZAKUPUBRU type of column INT_IMP_TOW_TOWARY.CENAZAKUPUBRU,
      CHODLIWY type of column INT_IMP_TOW_TOWARY.CHODLIWY,
      WITRYNA type of column INT_IMP_TOW_TOWARY.WITRYNA,
      SKADTABELA type of column INT_IMP_TOW_TOWARY.SKADTABELA,
      SKADREF type of column INT_IMP_TOW_TOWARY.SKADREF,
      DEL type of column INT_IMP_TOW_TOWARY.DEL,
      HASHVALUE type of column INT_IMP_TOW_TOWARY.HASH,
      REC type of column INT_IMP_TOW_TOWARY.REC,
      SESJA type of column INT_IMP_TOW_TOWARY.SESJA)
  returns (
      REF type of column INT_IMP_TOW_TOWARY.REF)
   as
begin
  insert into int_imp_tow_towary (towarid, symbol, nazwa, grupa, grupaid, rodzaj, rodzajid,
      aktywny, vat, opis, cenazakupunet, cenazakupubru, chodliwy, witryna,
      "HASH", del, rec, skadtabela, skadref, sesja)
    values (:towarid, :symbol, :nazwa, :grupa, :grupaid, :rodzaj, :rodzajid,
      :aktywny, :vat, :opis, :cenazakupunet, :cenazakupubru, :chodliwy, :witryna,
      :hashvalue, :del, :rec, :skadtabela, :skadref, :sesja)
    returning ref into :ref;
end^
SET TERM ; ^
