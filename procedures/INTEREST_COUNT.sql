--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INTEREST_COUNT(
      ACCOUNT KONTO_ID,
      SETTLEMENT varchar(20) CHARACTER SET UTF8                           ,
      FDATE timestamp,
      TDATE timestamp,
      AMOUNT numeric(14,2),
      TABELA integer,
      COMPANY integer)
  returns (
      INTEREST numeric(14,2))
   as
DECLARE VARIABLE STOPA NUMERIC(14,4);
DECLARE VARIABLE CDATE TIMESTAMP;
DECLARE VARIABLE NEWCDATE TIMESTAMP;
begin
  interest = 0;
  stopa = 0;

  -- pobranie wartoci stopy ods. z rozrachunku, o ile jest okreslona
  select ROZRACH.odsetki
    from ROZRACH where KONTOFK = :account and SYMBFAK = :settlement
    and rozrach.company = :company
    into :stopa;

  if (stopa > 0) then
  begin
    -- naliczenie kwoty odsetek wg stalej stopy
    interest = amount * (cast(TDATE as date) - cast(FDATE as date)) * (stopa / 365 / 100);
  end else
  begin
    -- pobranie stopy procentowej obowiazujacej w momencie powstania
    cdate = null;
    select max(data) from INTERESTS where cast(data as date) <= cast(:fdate as date) and interesttables = :TABELA
      into :cdate;

    if (cdate is not null) then
    begin
      select RATEY from INTERESTS
        where cast(data as date) = cast(:cdate as date)
          and interesttables = :TABELA
        into :stopa;
    end

    cdate = fdate;

    if (stopa is null) then
      stopa = 0;
    while (cast(cdate as date) < cast(tdate as date)) do
    begin
      -- analiza zmian stop odsetkowych w okresie od terminu platnosci do daty zaplaty
      newcdate = null;

      select min(data) from INTERESTS where (cast(DATA as date) > cast(:cdate as date) and cast(DATA as date) < cast(:tdate as date) and interesttables = :TABELA)
        into :newcdate;

      if (newcdate is null) then
        newcdate = tdate;

      -- naliczanie kwoty odsetek przedzialami zmian stop
      interest = interest + (amount * stopa * (cast(newcdate as date) - cast(cdate as date))/100/365);

      cdate = newcdate;
      if (cast(cdate as date) < cast(tdate as date)) then
      begin
        -- pobranie kolejnej stopy
        select RATEY
          from INTERESTS
          where cast(data as date) = cast(:cdate as date) and interesttables = :TABELA
          into :stopa;
        if (stopa is null) then
          stopa = 0;
      end
    end
  end
end^
SET TERM ; ^
