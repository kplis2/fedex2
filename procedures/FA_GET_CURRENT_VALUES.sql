--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FA_GET_CURRENT_VALUES(
      FXDASSET integer,
      AMPERIOD integer)
  returns (
      TVALUE numeric(15,2),
      TREDEMPTION numeric(15,2),
      FVALUE numeric(15,2),
      FREDEMPTION numeric(15,2))
   as
declare variable amyear smallint;
  declare variable ammonth smallint;
  declare variable tax_value_doc numeric(15,2);
  declare variable tax_redemption_doc numeric(15,2);
  declare variable fin_value_doc numeric(15,2);
  declare variable fin_redemption_doc numeric(15,2);
  declare variable ldate date;
  declare variable tamort numeric(15,2);
  declare variable famort numeric(15,2);
begin
  select tvalue, tredemption, fvalue, fredemption
    from fxdassets where ref = :fxdasset
    into :tvalue, :tredemption, :fvalue, :fredemption;

  select amyear, ammonth from amperiods where ref = :amperiod
    into :amyear, :ammonth;


  select lday from datatookres(cast(cast(:amyear || '/' || :ammonth || '/1' as date) as varchar(10)) , 0, 0, 0, 1)
    into :ldate;

  select sum(tvalue), sum(tredemption), sum(fvalue), sum(fredemption)
    from valdocs
    where operdate <= :ldate and fxdasset = :fxdasset
    into :tax_value_doc, :tax_redemption_doc, :fin_value_doc, :fin_redemption_doc;

  if (tax_value_doc is not null) then tvalue = tvalue + tax_value_doc;
  if (tax_redemption_doc is not null) then tredemption = tredemption + tax_redemption_doc;

  if (fin_value_doc is not null) then fvalue = fvalue + fin_value_doc;
  if (fin_value_doc is not null) then fredemption = fredemption + fin_redemption_doc;

  select sum(tamort), sum(famort)
    from amortization where fxdasset = :fxdasset and (amyear < :amyear or (amyear = :amyear and ammonth <= ammonth))
    into :tamort, :famort;

  if (tamort is not null) then tredemption = tredemption + tamort;
  if (famort is not null) then fredemption = fredemption + famort;

  suspend;
end^
SET TERM ; ^
