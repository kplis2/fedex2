--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_CHECK_BLOKADY(
      ZAM integer)
  returns (
      STATUS integer,
      POZNOTB varchar(255) CHARACTER SET UTF8                           )
   as
declare variable typ integer;
declare variable ktm varchar(40);
declare variable POZREF integer;
declare variable ilosc numeric(14,4);
declare variable ilreal numeric(14,4);
begin
  STATUS = -1;
  select TYP from NAGZAM where REF=:zam into :typ;
  POZNOTB='';
  for select POZZAM.REF,POZZAM.KTM,POZZAM.ILOSCM-POZZAM.ILZREALM, POZZAM.ILREALM
  from POZZAM join TOWARY on (TOWARY.KTM=POZZAM.KTM)
  where POZZAM.ZAMOWIENIE=:zam and (TOWARY.USLUGA<>1)
  into :pozref, :ktm,:ilosc, :ilreal
  do begin
    if(:typ >=2) then ilosc = :ilreal;
    if(:ilosc is null) then ilosc=0;
    select sum(ilosc) from STANYREZ where POZZAM=:pozref AND STATUS='B' into :ilreal;
    if(:ilreal is null) then ilreal=0;
    if(:ilreal < :ilosc and coalesce(char_length(:poznotb),0)<210) then begin -- [DG] XXX ZG119346
      STATUS = 0;
      if (strmulticmp('; '||:ktm||'; ','; '||:poznotb)=0) then POZNOTB = :poznotb || :ktm || '; ';
    end
  end
  if(:status <> 0)then STATUS = 1;
end^
SET TERM ; ^
