--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_SET_GENMWSORDSAFTER(
      DOCID integer,
      DOCPOSID integer,
      MWSDOC smallint,
      MWSDISPOSITION smallint,
      AKCEPT integer,
      ILOSC ILOSCI_MAG,
      ILOSCL ILOSCI_MAG,
      ILOSCONMWSACTS ILOSCI_MAG)
  returns (
      GENMWSORDSAFTER smallint)
   as
declare variable altposmode smallint;
declare variable fake smallint;
declare variable havefake smallint;
declare variable alttopoz integer;
begin
  if (:mwsdisposition is null) then mwsdisposition = 0;
  select coalesce(t.altposmode,0), p.fake, p.havefake, p.alttopoz
    from dokumpoz p
      join towary t on (p.ktm = t.ktm)
    where p.ref = :docposid
  into :altposmode, :fake, :havefake, :alttopoz;
  if (:fake is null) then fake = 0;
  if (:havefake is null) then havefake = 0;

  if (:mwsdoc = 0 or :altposmode = 1 or :havefake = 1) then
  begin
    genmwsordsafter = 0;
    exit;
  end

  if (:mwsdisposition = 1 and :fake = 0 and (:ilosc > 0 or :ilosconmwsacts > 0)) then
  begin
    if (:akcept <> 1 and :ilosc > :ilosconmwsacts) then genmwsordsafter = 1;
    else genmwsordsafter = 0;
  end
  else if (:fake = 1 and (:ilosc > 0 or :ilosconmwsacts > 0)) then
  begin
    select coalesce(t.altposmode,0)
      from dokumpoz p
        join towary t on (p.ktm = t.ktm)
      where p.ref = :alttopoz
    into :altposmode;

    if (:altposmode = 0) then genmwsordsafter = 0;
    else
    begin
      if (((:mwsdisposition = 1 and :akcept <> 1) or (:mwsdisposition = 0 and :akcept = 1)) and :ilosc > :ilosconmwsacts) then
        genmwsordsafter = 1;
      else genmwsordsafter = 0;
    end
  end
  else if (:mwsdisposition = 0 and :fake = 0 and (:iloscl > 0 or :ilosconmwsacts > 0)) then
  begin
    if(akcept = 1 and :iloscl > :ilosconmwsacts) then genmwsordsafter = 1;
    else genmwsordsafter = 0;
  end
end^
SET TERM ; ^
