--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE LISTYWYS_ADDPOZ(
      DOKUMSPED integer,
      LISTAWYS integer,
      DOKUM integer,
      TYP char(1) CHARACTER SET UTF8                           )
  returns (
      POZ integer)
   as
declare variable wydania varchar(3);
declare variable ZEWN varchar(3);
declare variable SYMBOL varchar(40);
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable KLIENT integer;
declare variable DOSTAWCA integer;
declare variable FROMPLACE varchar(60); --XXX ZG133796 MKD
declare variable TOPLACE varchar(60); --XXX ZG133796 MKD
declare variable KH varchar(255);
declare variable ODBIORCA varchar(255);
declare variable ADRES varchar(255);
declare variable NRDOMU nrdomu_id;
declare variable NRLOKALU nrdomu_id;
declare variable MIASTO varchar(255);
declare variable ADRTYP char(1);
declare variable ADRES1 varchar(255);
declare variable MIASTO1 varchar(255);
declare variable IMIE varchar(60);
declare variable NAZWISKO varchar(60);
declare variable UWAGI varchar(1024);
declare variable ZAMOW integer;
declare variable POZREF integer;
declare variable ILOSC numeric(14,4);
declare variable ILOSCO numeric(14,4);
declare variable JEDNO integer;
declare variable WAGA numeric(14,4);
declare variable OLDILOSCO numeric(14,4);
declare variable OLDILOSC numeric(14,4);
declare variable ILOSCWYS numeric(14,4);
declare variable WERSJAREF integer;
declare variable KOSZTDOSTPLAT integer;
declare variable SPOSDOST integer;
declare variable TERMDOST integer;
declare variable GRUPASPED integer;
declare variable ODBIORCAID integer;
declare variable KOSZTDOST numeric(14,2);
declare variable KOSZTDOSTBEZ integer;
declare variable KOSZTDOSTROZ integer;
declare variable KODSPED varchar(3);
declare variable KODP varchar(10);
declare variable CNT integer;
declare variable SPRZEDAWCA integer;
declare variable UWAGISPED varchar(2000);
declare variable FLAGISPED varchar(200);
declare variable FLAGI STRING40;
declare variable ORGFLAGI STRING40;
declare variable NEWFLAGI STRING40;
declare variable CZYNAADRESKORESP smallint;
declare variable SERWIS integer;
declare variable FLAGATEMP varchar(1);
declare variable POBRANIE numeric(14,2);
declare variable SLODEFREF integer;
declare variable ILOSCPACZEK numeric(14,2);
declare variable ILOSCPALET numeric(14,2);
declare variable ILOSCPALETBEZ numeric(14,2);
declare variable WARTDEKLAR numeric(14,2);
declare variable HAVEFAKE smallint;
declare variable altposmode integer_id;
begin
  --exception universal 'kurcze';
  /* Procedure Text */
  poz = 0;
  if(:listawys = 0) then listawys = null;
  -- wartdeklar = wartość brutto w cenach sprzedaży
  if(:typ = 'M')then begin
    select d.magazyn, d.slodef, d.slopoz, d.symbol, d.kodsped,
        dd.wydania, dd.zewn, d.uwagi,
        d.dulica, d.dnrdomu, d.dnrlokalu, d.dmiasto, d.dkodp,
        d.sposdost, d.odbiorcaid, d.odbiorca, d.kosztdostplat, d.grupasped, d.kosztdost,
        d.kosztdostbez, d.kosztdostroz, d.uwagisped, d.flagisped, d.flagi, d.operator,
        d.iloscpaczek, d.iloscpalet, d.iloscpaletbez, d.wartsbrutto
      from dokumnag d
        join defdokum dd on (d.typ = dd.symbol)
      where d.ref = :dokum
    into :fromplace, :slodef, :slopoz, :symbol, :kodsped,
       :wydania, :zewn, :uwagi,
       :adres, :nrdomu, :nrlokalu, :miasto, :kodp,
       :sposdost, :odbiorcaid, :odbiorca, :kosztdostplat,:grupasped,:kosztdost,
       :kosztdostbez, :kosztdostroz, :uwagisped, :flagisped, :flagi, :sprzedawca,
       :iloscpaczek, :iloscpalet, :iloscpaletbez, :wartdeklar;
    if(:uwagisped is null) then uwagisped = '';
    if(:flagisped is null) then flagisped = '';
    if(:flagi is null) then flagi = '';
    if(:adres is null) then adres = '';
    if(:miasto is null) then miasto = '';


    if(:wydania = '1') then begin
      select MAGAZYN from DOKUMNAG where ref = :dokum into :fromplace;
      if(:zewn = '0') then
        select dm.SYMBOL, dm.opis, dm.dmiasto, dm.dulica, dm.dnrdomu, dm.dnrlokalu,
            o.imie, o.nazwisko
          from DEFMAGAZ dm
            join DOKUMNAG d on (dm.SYMBOL = d.mag2 and d.REF = :dokum)
            left join OPERATOR o on (dm.dosoba = o.ref)
        into :toplace, :kh, :miasto, :adres, :nrdomu, :nrlokalu,
          :imie, :nazwisko;
      select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'KLIENCI' into :slodefref;
      if(:slodef = :slodefref and zewn <> '0') then begin
        select KLIENCI.fskrot, KLIENCI.fskrot from KLIENCI where ref=:slopoz
        into :toplace, :kh ;
      end
      select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'DOSTAWCY' into :slodefref;
      if(:slodef = :slodefref and :zewn <> '0') then
        select DOSTAWCY.id, DOSTAWCY.nazwa from DOSTAWCY where ref=:slopoz
        into :toplace, :kh ;
      adrtyp = 'T';
    end else begin
      select MAGAZYN from DOKUMNAG where ref = :dokum into :toplace;
      if(:zewn = '0') then
        select dm.SYMBOL, dm.opis, dm.dmiasto, dm.dulica, dm.dnrdomu, dm.dnrlokalu,
            o.imie, o.nazwisko
          from DEFMAGAZ dm
            join DOKUMNAG d on (dm.SYMBOL = d.mag2 and d.REF = :dokum)
            left join OPERATOR o on (dm.dosoba = o.ref)
        into :fromplace, :kh, :miasto, :adres, :nrdomu, :nrlokalu,
          :imie, :nazwisko;
      select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'KLIENCI' into :slodefref;
      if(:slodef = :slodefref and :zewn <> '0') then
        select KLIENCI.fskrot, KLIENCI.fskrot from KLIENCI where ref=:slopoz
        into :fromplace, :kh ;
      select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'DOSTAWCY' into :slodefref;
      if(:slodef = :slodefref and :zewn <> '0') then
        select DOSTAWCY.id, DOSTAWCY.nazwa from DOSTAWCY where ref=:slopoz
        into :fromplace, :kh;
      adrtyp = 'F';
    end
  end else if(:typ = 'Z')then begin
    select NAGZAM.MAGAZYN, NAGZAM.mag2, NAGZAM.KLIENT, NAGZAM.DOSTAWCA, NAGZAM.ID,
      TYPZAM.WYDANIA, TYPZAM.zewn, NAGZAM.UWAGI, NAGZAM.dmiasto, NAGZAM.dulica, nagzam.dkodp,
      nagzam.flagisped, nagzam.flagi, nagzam.uwagisped, nagzam.iloscpaczek, nagzam.iloscpalet, nagzam.iloscpaletbez, nagzam.odbiorca,
      nagzam.kosztdost, nagzam.kosztdostbez, nagzam.kosztdostroz, nagzam.kosztdostplat, nagzam.odbiorcaid
      from NAGZAM join TYPZAM on (NAGZAM.typzam = TYPZAM.SYMBOL)
    where NAGZAM.REF = :dokum
    into :fromplace, :toplace, :klient, :dostawca, :symbol, :wydania, :zewn, :uwagi, :miasto, :adres, :kodp,
      :flagisped, :flagi, :uwagisped, :iloscpaczek, :iloscpalet,  :iloscpaletbez, :odbiorca,
      :kosztdost, :kosztdostbez, :kosztdostroz, :kosztdostplat, :odbiorcaid;

    if(:uwagisped is null) then uwagisped = '';
    if(:flagisped is null) then flagisped = '';
    if(:flagi is null) then flagi = '';
    if(:adres is null) then adres = '';
    if(:miasto is null) then miasto = '';
    if(:wydania = '1') then begin
      select MAGAZYN from NAGZAM where ref = :dokum into :fromplace;
      if(:zewn = '0') then
        select dm.SYMBOL, dm.opis, dm.dmiasto, dm.dulica, dm.dnrdomu, dm.dnrlokalu,
            o.imie, o.nazwisko
          from DEFMAGAZ dm
            join NAGZAM n on (dm.SYMBOL = n.mag2 and n.REF = :dokum)
            left join OPERATOR o on (dm.dosoba = o.ref)
        into :toplace, :kh, :miasto1, :adres1, :nrdomu, :nrlokalu,
          :imie, :nazwisko;
      else if(:klient > 0) then begin
        select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'KLIENCI' into :slodefref;
        select KLIENCI.fskrot, KLIENCI.fskrot, KLIENCI.miasto, KLIENCI.ULICA, KLIENCI.dkodp from KLIENCI where ref=:klient
        into :toplace, :kh, :miasto1, :adres1, :kodp;
        slodef = :slodefref;
        slopoz = :klient;
      end
      if(:miasto = '') then miasto = :miasto1;
      if(:adres = '') then adres = :adres1;
      adrtyp = 'T';
    end else if(:wydania = '0')then begin
      select MAGAZYN from NAGZAM where ref = :dokum into :toplace;
      if(:zewn = '0') then
        select DEFMAGAZ.SYMBOL, DEFMAGAZ.opis, DEFMAGAZ.dmiasto, DEFMAGAZ.dulica from DEFMAGAZ join NAGZAM on (DEFMAGAZ.SYMBOL = NAGZAM.mag2 and NAGZAM.REF = :dokum)
        into :fromplace, :kh, :miasto, :adres;
      else if(:dostawca > 0) then begin
        select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'DOSTAWCY' into :slodefref;
        select DOSTAWCY.id, DOSTAWCY.nazwa, DOSTAWCY.miasto, DOSTAWCY.ULICA from DOSTAWCY where ref=:dostawca
        into :fromplace, :kh, :miasto, :adres;
        slodef = :slodefref;
        slopoz = :dostawca;
      end
      adrtyp = 'F';
    end
    kodsped = null;
    zamow = :dokum;
    dokum = null;
  end
-- (R - zgloszenia serwisowe)
  else if(:typ = 'R')then begin
    select sreq.deliveryaddress
    from srvrequests sreq
    where sreq.ref = :dokum into :czynaadreskoresp;
    if (czynaadreskoresp = 1) then begin
-- wysylka na adres korespondencyjny
      select sreq.repdictpos, sreq.repstreet, sreq.repcity, sreq.reppostcode, sreq.repname,
        sreq.symbol, sreq.content, sreq.sposdost
      from srvrequests sreq
      where sreq.ref = :dokum
      into :klient, :adres, :miasto, :kodp, :toplace,
        :symbol, :uwagi, :sposdost;
    end else begin
-- wysylka na adres podany
      select sreq.repdictpos,sreq.recstreet, sreq.reccity, sreq.recpostcode, sreq.recname,
        sreq.symbol, sreq.content, sreq.sposdost
      from srvrequests sreq
      where sreq.ref = :dokum
      into :klient, :adres, :miasto, :kodp, :toplace,
        :symbol, :uwagi, :sposdost;
    end
    kh = :toplace;
    select sum(netsaleprice * amount)
      from srvpositions
        left join srvdefpos on srvdefpos.ref = srvpositions.srvdefpos
      where srvpositions.srvrequest = :dokum and srvdefpos.createfak = 1
      into :pobranie;
    select sposdost.uwagi from sposdost where sposdost.ref = :sposdost into :uwagisped;
    if (flagisped is null) then flagisped = '';
    for select sposdostflagi.flaga from sposdostflagi
      where sposdostflagi.sposdost = :sposdost and sposdostflagi.wartdom = 1
      into :flagatemp
    do begin
      if (flagatemp is null) then flagatemp = ';';
      flagisped = :flagisped ||:flagatemp||';';
    end
    adrtyp = 'T';
    if (adres is null) then adres = '';
    if (miasto is null) then miasto = '';
    if (toplace = '') then toplace = ' ';
    kodsped = null;
    serwis = :dokum;
    dokum = null;
    slopoz = :klient;
  end
  toplace = substring(:toplace from   1 for 20);
  fromplace = substring(:fromplace from 1 for 20);
  if(:kosztdostbez=1) then kosztdost = 0;
  if(:kosztdost is null) then kosztdost = 0;
  if(:iloscpaczek is null) then iloscpaczek = 0;
  if(:iloscpalet is null) then iloscpalet = 0;
  if(:iloscpaletbez is null) then iloscpaletbez = 0;
    if(:dokumsped is null or (:dokumsped = 0))then begin
      execute procedure GEN_REF('LISTYWYSD') returning_values :poz;
      insert into LISTYWYSD(REF, LISTAWYS, REFDOK, REFZAM, REFSRV, SYMBOL, FROMPLACE, TOPLACE, KODSPED,
         KONTRAHENT, ADRES, MIASTO, KODP, ADRTYP, SLODEF, SLOPOZ, UWAGI, SELLOPER,
         KOSZTDOST, KOSZTDOSTPLAT, ODBIORCAID, SPOSDOST, KOSZTDOSTBEZ, KOSZTDOSTROZ, UWAGISPED, FLAGISPED, flagi,
         POBRANIE,ILOSCPACZEK,ILOSCPALET,ILOSCPALETBEZ,WARTDEKLAR)
      values (:poz, :listawys, :dokum, :zamow, :serwis, :symbol, :fromplace, :toplace, :kodsped,
         :kh, :adres, :miasto, :KODP, :adrtyp, :slodef, :slopoz, :uwagi, :sprzedawca, 
         :kosztdost, :kosztdostplat, :odbiorcaid, :sposdost, :kosztdostbez, :kosztdostroz, :uwagisped, :flagisped, :flagi,
         :pobranie,:iloscpaczek,:iloscpalet,:iloscpaletbez,:wartdeklar);
    end else begin
      poz = :dokumsped;
/*      if(:kosztdostbez > 0) then
        update LISTYWYSD set KOSZTDOST = 0 where ref=:poz;
      else
        update LISTYWYSD set KOSZTDOST = KOSZTDOST + :kosztdost where ref=:poz;*/

      select flagi from listywysd where ref = :poz into :orgflagi;
      if (:orgflagi is null) then orgflagi = '';
      select list(distinct p.stringout,';')
        from parsestring(:orgflagi||';'||:flagi,';') p
        into :newflagi;

      update LISTYWYSD set KOSZTDOSTROZ = :kosztdostroz, KOSZTDOSTPLAT = :kosztdostplat, SYMBOL = '',
        LISTYWYSD.kosztdost = LISTYWYSD.kosztdost + :kosztdost, 
        LISTYWYSD.kosztdostbez = LISTYWYSD.kosztdostbez * :kosztdostbez,
        LISTYWYSD.iloscpaczek = LISTYWYSD.iloscpaczek + :iloscpaczek, 
        LISTYWYSD.iloscpalet = LISTYWYSD.iloscpalet + :iloscpalet,
        LISTYWYSD.iloscpaletbez = LISTYWYSD.iloscpaletbez + :iloscpaletbez,
        listywysd.flagisped = :newflagi
        where ref=:poz;
      execute procedure listywysd_obliczsymbol(:poz);
    end
    if(:typ = 'M') then begin
      for
        select dp.REF, dp.havefake,
          case when coalesce(dp.alttopoz,0) = 0 then dp.ILOSCL else (select first 1 d.ilosc from dokumpoz d where d.dokument = :dokum and d.ref = dp.alttopoz) end,
          dp.JEDNO,
          case when coalesce(dp.alttopoz,0) = 0 then dp.ilosclwys else (select first 1 d.ilosclwys from dokumpoz d where d.dokument = :dokum and d.ref = dp.alttopoz) end,
          dp.waga, dp.wersjaref, dp.alttopoz
        from dokumnag dn
          join dokumpoz dp on (dn.ref = dp.dokument)
          left join towary t on (dp.ktm = t.ktm)
          left join defdokum dd on (dn.typ = dd.symbol)
        where dn.ref = :dokum
          and dd.koryg = 0
          and t.usluga <> 1
          and coalesce (dp.havefake,0) = 0
          --and (coalesce(dp.fake,0) = 0 or (coalesce(t.altposmode,0) > 0 and coalesce(dp.fake,0) = 1))    LDz XXX - pakowanie fake
      into :pozref, :havefake, :ilosc, :jedno, :iloscwys, :waga, :wersjaref, :altposmode
      do begin
        if(:iloscwys is null) then iloscwys = 0;
        if(:iloscwys>0) then begin
          oldilosco = 0; oldilosc = 0;
          select sum(ilosco), sum(ilosc) from LISTYWYSDPOZ where DOKPOZ = :pozref into :oldilosco, :oldilosc;
          ilosc = :ilosc - oldilosc;
          ilosco = :ilosco - :oldilosco;
          waga = 0;
        end
        if(:ilosc <> 0 or havefake = 1) then begin
          cnt = null;
          select count(*) from LISTYWYSDPOZ where DOKPOZ = :pozref and DOKUMENT = :dokumsped into :cnt;
          if(:cnt is null) then cnt = 0;
          if(:cnt = 0) then
            insert into LISTYWYSDPOZ(DOKUMENT, DOKTYP, DOKREF, DOKPOZ, WERSJAREF, ILOSCO, JEDNO, ILOSC, ILOSCMAX, WAGA, altposmode)
            values (:poz, :typ, :dokum, :pozref, :wersjaref, :ilosco, :jedno, :ilosc, :ilosc, :waga, :altposmode);
          else begin
            update LISTYWYSDPOZ set ILOSC = ILOSC + :ilosc, ILOSCO = ILOSCO + :ilosco where DOKPOZ = :pozref and DOKUMENT = :dokumsped;
            if (havefake = 0) then
              delete from LISTYWYSDPOZ where ILOSC <= 0 and DOKPOZ = :pozref and DOKUMENT = :dokumsped;
          end
        end
      end
      /*dodanie pozostaych dok. mag z grupy spedycyjnej*/
      if(:dokumsped is NULL or (:dokumsped = 0)) then begin
        for select REF from DOKUMNAG where GRUPASPED = :grupasped and ref <> :dokum and WYSYLKADONE = 0 and wydania = 1
        order by ref
        into :dokum
        do begin
          execute procedure LISTYWYS_ADDpoz(:poz,:listawys,:dokum,'M') returning_values :poz;
        end
      end
    end if(:typ = 'Z') then begin
      for select REF, ILOSC, ILOSCO, JEDNO, waga, wersjaref
      from POZZAM
      where ZAMOWIENIE = :zamow
      into :pozref, :ilosc, :ilosco, :jedno, :waga, :wersjaref
      do begin
        oldilosco = 0; oldilosc = 0;
        select sum(ilosco), sum(ilosc) from LISTYWYSDPOZ where POZZAM = :pozref into :oldilosco, :oldilosc;
        if(:oldilosc is null) then oldilosc = 0;
        if(:oldilosco is null) then oldilosco = 0;
        ilosc = :ilosc - oldilosc;
        ilosco = :ilosco - :oldilosco;
        waga = 0;
        if(:ilosc <> 0) then begin
          cnt = null;
          select count(*) from LISTYWYSDPOZ where POZZAM = :pozref and DOKUMENT = :dokumsped into :cnt;
          if(:cnt is null) then cnt = 0;
          if(:cnt = 0) then
            insert into LISTYWYSDPOZ(DOKUMENT, DOKTYP, DOKREF, POZZAM, WERSJAREF, ILOSCO, JEDNO, ILOSC, ILOSCMAX, WAGA)
            values (:poz, :typ, :zamow, :pozref, :wersjaref, :ilosco, :jedno, :ilosc, :ilosc, :waga);
          else begin
            update LISTYWYSDPOZ set ILOSC = ILOSC + :ilosc, ILOSCO = ILOSCO + :ilosco where POZZAM = :pozref and DOKUMENT = :dokumsped;
            delete from LISTYWYSDPOZ where ILOSC <= 0 and POZZAM = :pozref and DOKUMENT = :dokumsped;
          end
        end
      end
    end if(:typ = 'R') then begin
      insert into LISTYWYSDPOZ(DOKUMENT, DOKTYP, DOKREF, ILOSC)
            values (:poz, :typ, :serwis, 0);
    end if(:typ = 'S')then
      update LISTYWYSD set LISTAWYS = :listawys where ref=:poz;
end^
SET TERM ; ^
