--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CALCULATE_KOMPLETY_POT(
      WERSJAREF integer,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      USLUGA smallint)
   as
declare variable towtypekpl varchar(100);
declare variable towtypetow varchar(100);
declare variable calculatekomplety varchar(100);
declare variable mag varchar(3);
declare variable kplwersjaref integer;
declare variable kplref integer;
declare variable minkplil numeric(14,4);
declare variable mintemp numeric(14,4);
declare variable ilpozkpl numeric(14,4);
declare variable ilstan numeric(14,4);
begin
  -- sprawdzamy czy jest wogóle wlaczona kalkulacja kompletow
  execute procedure get_config('CALCULATEKOMPLETY',2) returning_values :calculatekomplety;
  if (calculatekomplety is null or calculatekomplety <> '1') then
    exit;
  -- procedura aktualizuje on-line na stanach ilosciowych ilosc mozliwych do wykonania kompletow wedlug receptury glownej kompletu
  execute procedure get_config('TOWTYPEKOMPLET',2) returning_values :towtypekpl;
  if (:towtypekpl is null or :towtypekpl = '') then
    towtypekpl = '2';
  execute procedure get_config('TOWTYPETOWAR',2) returning_values :towtypetow;
  if (:towtypetow is null or :towtypetow = '') then
    towtypetow = '0';
  -- obliczenie ilosci potencjalnych kompletow po dodaniu nowego kompletu lub jego modyfikacji
  if (:usluga = cast(:towtypekpl as integer)) then
  begin
    select kplnag.ref from kplnag
      where kplnag.glowna = 1 and kplnag.wersjaref = :wersjaref
      into :kplref;
    for
      select defmagaz.symbol
        from defmagaz
        where (:magazyn is not null and :magazyn <> '' and defmagaz.symbol = :magazyn)
          or (:magazyn is null) or (:magazyn = '')
        order by defmagaz.symbol
        into :mag
    do begin
      minkplil = -1;
      for
        select kplpoz.ilosc, stanyil.ilosc - stanyil.zablokow
          from kplpoz
            left join stanyil on (stanyil.wersjaref = kplpoz.wersjaref)
          where stanyil.magazyn = :mag and kplpoz.nagkpl = :kplref
        into :ilpozkpl, :ilstan
      do begin
        if (:ilpozkpl is null) then ilpozkpl = 0;
        if (:minkplil <> 0) then
        begin
          if (:ilstan is null or :ilstan = 0) then minkplil = 0;
          if (:ilpozkpl > 0) then
          begin
            mintemp = :ilstan/:ilpozkpl;
            if (:minkplil = -1 or (:mintemp < :minkplil and :minkplil <> -1) ) then
              minkplil = :mintemp;
          end else exception CALC_KOMPLETY_EXPT ;
        end
      end
      if (:minkplil < 0) then minkplil = 0;
      if (exists (select wersjaref from stanyil where stanyil.magazyn = :mag and stanyil.wersjaref = :wersjaref)) then
        update stanyil set stanyil.iloscpot = :minkplil where stanyil.magazyn = :mag and stanyil.wersjaref = :wersjaref;
      else
        insert into stanyil (magazyn, wersjaref, ilosc, usluga, iloscpot)
          values (:mag, :wersjaref, 0, cast(:towtypekpl as integer),:minkplil);
    end
  end
  -- obliczenie ilosci potencjalnych kompletow po zmianie stanu ilosciowego jednego ze skladnikow
  else if (:usluga = cast(:towtypetow as integer)) then
  begin
    for
      select distinct kplnag.wersjaref
        from kplpoz 
          join kplnag on (kplnag.ref = kplpoz.nagkpl)
        where kplpoz.wersjaref = :wersjaref and kplnag.glowna = 1
        into :kplwersjaref
    do begin
      execute procedure CALCULATE_KOMPLETY_POT(:kplwersjaref,:magazyn,cast(:towtypekpl as integer));
    end
  end
  else
    exit;
end^
SET TERM ; ^
