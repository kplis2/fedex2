--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PMPLANS_CALC(
      REF integer)
   as
declare variable CALCMVAL numeric(14,2);
declare variable CALCSVAL numeric(14,2);
declare variable CALCW numeric(14,4);
declare variable CALCWVAL numeric(14,2);
declare variable CALCE numeric(14,4);
declare variable CALCEVAL numeric(14,2);
declare variable BUDMVAL numeric(14,2);
declare variable BUDSVAL numeric(14,2);
declare variable BUDWVAL numeric(14,2);
declare variable BUDEVAL numeric(14,2);
declare variable REALMVAL numeric(14,2);
declare variable REALSVAL numeric(14,2);
declare variable REALW numeric(14,4);
declare variable REALE numeric(14,4);
declare variable BUDW numeric(14,4);
declare variable BUDE numeric(14,4);
declare variable REALWVAL numeric(14,2);
declare variable REALEVAL numeric(14,2);
declare variable EREALMVAL numeric(14,2);
declare variable EREALSVAL numeric(14,2);
declare variable EREALW numeric(14,4);
declare variable EREALWVAL numeric(14,2);
declare variable EREALE numeric(14,4);
declare variable CALCVAL numeric(14,4);
declare variable EREALVAL numeric(14,4);
declare variable BUDVAL numeric(14,4);
declare variable REALVAL numeric(14,4);
declare variable PLANMASTER integer;
begin
 select coalesce(sum(CALCMVAL),0), coalesce(sum(CALCSVAL),0), coalesce(sum(CALCW),0), coalesce(sum(CALCWVAL),0), coalesce(sum(CALCE),0), coalesce(sum(CALCEVAL),0),
        coalesce(sum(BUDMVAL),0), coalesce(sum(BUDSVAL),0), coalesce(sum(BUDWVAL),0), coalesce(sum(BUDEVAL),0), coalesce(sum(REALMVAL),0), coalesce(sum(REALSVAL),0),
        coalesce(sum(REALW),0), coalesce(sum(REALE),0), coalesce(sum(BUDW),0), coalesce(sum(BUDE),0), coalesce(sum(REALWVAL),0), coalesce(sum(REALEVAL),0), coalesce(sum(EREALMVAL),0),
        coalesce(sum(EREALSVAL),0), coalesce(sum(EREALW),0), coalesce(sum(EREALWVAL),0), coalesce(sum(EREALE),0),
        coalesce(sum(CALCVAL),0), coalesce(sum(EREALVAL),0), coalesce(sum(BUDVAL),0), coalesce(sum(REALVAL),0)
   from pmelements e where e.pmplan = :ref
   into :CALCMVAL, :CALCSVAL, :CALCW, :CALCWVAL, :CALCE, :CALCEVAL,
        :BUDMVAL, :BUDSVAL, :BUDWVAL, :BUDEVAL, :REALMVAL, :REALSVAL, 
        :REALW, :REALE, :BUDW, :BUDE, :REALWVAL, :REALEVAL, :EREALMVAL, 
        :EREALSVAL, :EREALW, :EREALWVAL, :EREALE,
        :CALCVAL, :EREALVAL, :BUDVAL, :REALVAL;

 select coalesce(sum(CALCMVAL),0)+ :CALCMVAL, coalesce(sum(CALCSVAL),0)+ :CALCSVAL, coalesce(sum(CALCW),0)+ :CALCW,
        coalesce(sum(CALCWVAL),0)+ :CALCWVAL, coalesce(sum(CALCE),0)+ :CALCE, coalesce(sum(CALCEVAL),0)+ :CALCEVAL,
        coalesce(sum(BUDMVAL),0)+ :BUDMVAL, coalesce(sum(BUDSVAL),0)+ :BUDSVAL, coalesce(sum(BUDWVAL),0)+ :BUDWVAL,
        coalesce(sum(BUDEVAL),0)+ :BUDEVAL, coalesce(sum(REALMVAL),0)+ :REALMVAL, coalesce(sum(REALSVAL),0)+ :REALSVAL,
        coalesce(sum(REALW),0)+ :REALW, coalesce(sum(REALE),0)+ :REALE, coalesce(sum(BUDW),0)+ :BUDW, coalesce(sum(BUDE),0)+ :BUDE,
        coalesce(sum(REALWVAL),0)+ :REALWVAL, coalesce(sum(REALEVAL),0)+ :REALEVAL, coalesce(sum(EREALMVAL),0)+ :EREALMVAL,
        coalesce(sum(EREALSVAL),0)+ :EREALSVAL, coalesce(sum(EREALW),0)+ :EREALW, coalesce(sum(EREALWVAL),0)+ :EREALWVAL,
        coalesce(sum(EREALE),0)+ :EREALE, coalesce(sum(CALCVAL),0)+ :CALCVAL, coalesce(sum(EREALVAL),0)+ :EREALVAL,
        coalesce(sum(BUDVAL),0)+ :BUDVAL, coalesce(sum(REALVAL),0)+ :REALVAL
   from pmplans pp where pp.pmplanmaster = :ref
   into :CALCMVAL, :CALCSVAL, :CALCW,
        :CALCWVAL, :CALCE, :CALCEVAL,
        :BUDMVAL, :BUDSVAL, :BUDWVAL, :BUDEVAL, :REALMVAL, :REALSVAL, 
        :REALW, :REALE, :BUDW, :BUDE, :REALWVAL, :REALEVAL, :EREALMVAL, 
        :EREALSVAL, :EREALW, :EREALWVAL, :EREALE,
        :CALCVAL, :EREALVAL, :BUDVAL, :REALVAL;

 update pmplans p set
        p.CALCMVAL=:CALCMVAL, p.CALCSVAL=:CALCSVAL, p.CALCW=:CALCW, p.CALCWVAL=:CALCWVAL, 
        p.CALCE=:CALCE, p.CALCEVAL=:CALCEVAL, p.BUDMVAL=:BUDMVAL, p.BUDSVAL=:BUDSVAL, 
        p.BUDWVAL=:BUDWVAL, p.BUDEVAL=:BUDEVAL, p.REALMVAL=:REALMVAL, p.REALSVAL=:REALSVAL, 
        p.REALW=:REALW, p.REALE=:REALE, p.BUDW=:BUDW, p.BUDE=:BUDE, p.REALWVAL=:REALWVAL, 
        p.REALEVAL=:REALEVAL, p.EREALMVAL=:EREALMVAL, p.EREALSVAL=:EREALSVAL, p.EREALW=:EREALW, 
        p.EREALWVAL=:EREALWVAL, p.EREALE=:EREALE, P.CALCVAL=:CALCVAL, P.EREALVAL=:EREALVAL,
        p.BUDVAL=:BUDVAL, p.REALVAL=:REALVAL
  where p.ref = :ref;
  
  select p.pmplanmaster from pmplans p where ref = :ref into :planmaster;
  if (planmaster is not null and planmaster <> 0) then
    execute procedure pmplans_calc(:planmaster);
end^
SET TERM ; ^
