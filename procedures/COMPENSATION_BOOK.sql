--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COMPENSATION_BOOK(
      KOMPENSAID integer,
      DOCTYPE integer,
      OPERID integer,
      AKCEPT smallint,
      DATA timestamp,
      SYMBOL varchar(20) CHARACTER SET UTF8                           )
  returns (
      BKDOC integer)
   as
declare variable BKREG varchar(10);
declare variable NUMER integer;
declare variable CURRENCY varchar(3);
declare variable DICTDEF_DEBIT integer;
declare variable DICTPOS_DEBIT integer;
declare variable ACCOUNT_DEBIT ACCOUNT_ID;
declare variable SETTLEMENT_DEBIT varchar(20);
declare variable DICTDEF_CREDIT integer;
declare variable DICTPOS_CREDIT integer;
declare variable ACCOUNT_CREDIT ACCOUNT_ID;
declare variable SETTLEMENT_CREDIT varchar(20);
declare variable CURRDEBIT numeric(14,2);
declare variable DEBIT numeric(14,2);
declare variable CURRCREDIT numeric(14,2);
declare variable TOCLEAR numeric(14,2);
declare variable FSCLRACCP_REF_CREDIT integer;
declare variable FSCLRACCP_REF_DEBIT integer;
declare variable FSCLRACCH_DATE timestamp;
declare variable RATE numeric(12,4);
declare variable DESCRIPTION_DEBIT varchar(20);
declare variable PAYDAY timestamp;
declare variable PAYDAY2 timestamp;
declare variable DEBIT_OPERDATE timestamp;
declare variable CREDIT_OPERDATE timestamp;
declare variable DEBIT_RATE numeric(12,4);
declare variable CREDIT_RATE numeric(12,4);
declare variable COMPANY integer;
declare variable BOOKING varchar(255);
declare variable DICTSYMBOL varchar(40);
declare variable CURRENCYCOUNTY varchar(3);
declare variable DATANOWA timestamp;
begin
--exception test_break 'x';
  bkdoc = 0;
  numer = 0;
  select bkreg from bkdoctypes where ref = :doctype
    into :bkreg;


  select company, currency, regdate, dictsymbol
    from fsclracch
    where ref = :kompensaid
    into :company, :currency, :fsclracch_date, :dictsymbol;

  if (coalesce(:symbol,'') = '') then
    select symbol
      from fsclracch
      where ref = :kompensaid
      into :symbol;

  execute procedure GEN_REF('BKDOCS') returning_values bkdoc;
  insert into bkdocs(ref, bkreg, doctype, transdate, symbol, descript, docdate,
      vatdate, regoper, dictdef, dictpos, otable, oref, company, contractor)
    select :bkdoc, :bkreg, :doctype, :data, :symbol, :symbol, :data,
        :data, :operid, dictdef, dictpos, 'FSCLRACCH', ref, :company, :dictsymbol
      from fsclracch
      where ref = :kompensaid;

  for
    select dictdef, dictpos, account, settlement, currdebit2c,
        debit, rate, operdate, payday, substring(description from 1 for 20), ref
      from fsclraccp
      where fsclracch = :KOMPENSAID
        and side = 0
      into :dictdef_debit, :dictpos_debit, :account_debit, :settlement_debit, :currdebit,
        :debit, :debit_rate, :debit_operdate, :payday, description_debit, :fsclraccp_ref_debit
  do
  begin
    for
      select fp.ref, fp.dictdef, fp.dictpos, fp.account,
          fp.settlement, fp.currcredit2c - fp.cleared, fp.operdate, fp.rate, fp.payday,rozrachp.stransdate
        from fsclraccp fp
          left join rozrachp on (fp.rozrachp=rozrachp.ref)
        where fp.fsclracch = :KOMPENSAID
          and fp.side = 1
          and fp.currcredit - fp.cleared > 0
        into :fsclraccp_ref_credit, :dictdef_credit, :dictpos_credit, :account_credit,
          :settlement_credit, :currcredit, :credit_operdate, :credit_rate, :payday2 ,
          :datanowa
    do
    begin
      if (currdebit>currcredit) then
      begin
        toclear = currcredit;
        rate = credit_rate;   -- kurs brany jest z rozrachunku rozliczanego calociowo
      end else begin
        toclear = currdebit;
        rate = debit_rate;
      end

      execute procedure get_global_param('WALPLN') returning_values :currencycounty;
      if (coalesce(currencycounty,'')='' ) then
        currencycounty = 'PLN';

      if (currencycounty <> currency) then
      begin -- rozliczenie kompensat walutowych
        execute procedure insert_decree_currsettlement(bkdoc, account_debit, 1, :toclear * :rate, :toclear, rate,
          currency, 'Rozliczenie z rozrachunkiem ' || :settlement_credit, settlement_debit, null, 8, null, null,
          null, null, null, null ,  null, null, null, 0);
        execute procedure insert_decree_currsettlement(bkdoc, account_credit, 0, :toclear * :rate, :toclear, rate,
          currency, 'Rozliczenie z rozrachunkiem ' || :settlement_debit, settlement_credit, null, 8, null, null,
          null, null, null,null ,  null, null, null, 0);
      end else begin -- rozliczenie kompensat PLN
        execute procedure insert_decree_settlement(bkdoc, account_debit, 1, toclear, 'Rozliczenie z rozrachunkiem ' || :settlement_credit,
          settlement_debit, :datanowa, 8, null, null, null, null, null, null, null,0);
        execute procedure insert_decree_settlement(bkdoc, account_credit, 0, toclear, 'Rozliczenie z rozrachunkiem ' || :settlement_debit,
          settlement_credit, null, 8, null, null, null, null, null, null, null,0);
      end
      update fsclraccp set cleared = cleared + :toclear
        where ref = :fsclraccp_ref_credit;

      currdebit = currdebit - toclear;
      if (currdebit=0) then
        break;
    end
  end

/*  for
    select account, settlement, currdebit2c, currcredit2c, rate,
        number, description, operdate, payday
      from fsclraccp
      where fsclracch = :kompensaid and (currdebit2c > 0 or currcredit2c > 0)
      order by number
      into :account, :settlement, :debit, :credit, :rate,
        :number, :descript, :opendate, :payday
  do begin
    if (debit > 0) then
    begin
      side = 1;
      credit = debit;
      debit = 0;
    end else
    begin
      side = 0;
      debit = credit;
      credit = 0;
    end

    --- TOFIX: tu jest blad przy kompensatach walutowych!!!!

    if (exists(select company from rozrach where symbfak = :settlement and kontofk = :account and company = :company)) then
    begin
      insert into decrees(account, number, descript, currency, rate, side, bkdoc,
        debit, credit, currdebit, currcredit, settlement, fsopertype)
      values (:account, :number, :descript, :currency, :rate, :side, :bkdoc,
        :debit, :credit, :debit * :rate, :credit * :rate, :settlement, 8);
    end else
    begin
      insert into decrees (account, number, descript, currency, rate, side, bkdoc,
          debit, credit, currdebit, currcredit, settlement, opendate, payday, settlcreate, fsopertype)
        values (:account, :number, :descript, :currency, :rate, :side, :bkdoc,
          :debit, :credit, :debit * :rate, :credit * :rate, :settlement, :opendate, :payday, 1, 8);
    end
  end
*/
  if (akcept = 1) then
  begin
    execute procedure GET_CONFIG('BOOK_WITH_ACCEPT', 2)
      returning_values booking;

    if (booking = '1') then
      update bkdocs set status = 2, accoper = :operid, bookoper = :operid where ref = :bkdoc;
    else
      update bkdocs set status = 1, accoper = :operid where ref = :bkdoc;

  end

  if (bkdoc > 0) then
    update fsclracch set status = 2 where ref = :kompensaid;
end^
SET TERM ; ^
