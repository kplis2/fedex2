--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_ASSIGNATION(
      EABSENCE integer,
      ASSETTLING smallint = 0)
  returns (
      BENEFIT_NAME varchar(40) CHARACTER SET UTF8                           ,
      BENEFIT_FROMDATE date,
      BENEFIT_TODATE date,
      L4_SERIAL varchar(2) CHARACTER SET UTF8                           ,
      L4_NUMBER varchar(7) CHARACTER SET UTF8                           ,
      L4_FROMDATE date,
      L4_TODATE date,
      ILLPAY_FROMDATE date,
      ILLPAY_TODATE date,
      ILLPAYROLL_NR varchar(20) CHARACTER SET UTF8                           ,
      ILLPAYROLL_DATE date,
      PREV_BENEFIT_NAME varchar(40) CHARACTER SET UTF8                           ,
      PREV_BENEFIT_FROMDATE date,
      PREV_BENEFIT_TODATE date,
      PREV_BENEFIT_DECISION varchar(40) CHARACTER SET UTF8                           ,
      PREV_BENEFITPAYROLL_NR varchar(20) CHARACTER SET UTF8                           ,
      PREV_BENEFITPAYROLL_DATE date,
      TAB_NUMBER integer,
      TAB_PAYDAY date,
      TAB_NAME varchar(30) CHARACTER SET UTF8                           ,
      TAB_PVAL varchar(10) CHARACTER SET UTF8                           ,
      TAB_FROMDATE date,
      TAB_TODATE date,
      TAB_DAYS integer,
      TAB_PAYAMOUNT numeric(14,2))
   as
declare variable AYEAR integer;
declare variable AMONTH integer;
declare variable TMP_FROMDATE date;
declare variable TMP_TODATE date;
declare variable AECOLUMN integer;
declare variable ILLEABS_REF integer;
declare variable EMPLOYEE integer;
declare variable RECOL_LIST varchar(50);
begin
/*MWr Personel: Procedura odpowiada za uzupelnienie wydruku asygnaty zasilkowej
      Parametr ASSETTLING = 0 odpowiada za wypelnienie czesci opisowej wydruku,
               ASSETTLING > 0 odpowiada za wypelnienie tabeli wyplat*/

  select a.ecolumn, a.employee, a.certserial, a.certnumber, amonth, ayear
    from eabsences a
    where a.ref = :eabsence
    into :aecolumn, :employee, :l4_serial, :l4_number, :amonth, :ayear;

  if (aecolumn in (90, 100, 110, 140, 150, 160, 170, 270, 280, 290, 350, 360, 370, 440, 450)) then
  begin
  --nazwa zasilku
    if(aecolumn < 130) then benefit_name = 'chorobowego';
    else if(aecolumn in (160, 170)) then benefit_name = 'opiekuńczego';
    else if(aecolumn in (140, 150, 270, 280, 290, 440, 450)) then benefit_name = 'macierzyńskiego';
    else benefit_name = 'świadczenia rehabilitacyjnego';

  --nr i seria zaswiadczenia o niezdolnosci do pracy
    l4_serial = coalesce(l4_serial,'');
    l4_number = coalesce(l4_number,'');

    if (:l4_serial <> '') then
      select max(a.todate), min(a.fromdate)
        from eabsences a
        where a.certserial = :l4_serial
          and a.certnumber = :l4_number
          and a.correction in (0,2)
          and a.employee = :employee
        into :l4_todate, :l4_fromdate;

  --dalszy zasilek
    execute procedure period2dates(:ayear || :amonth)
      returning_values :tmp_fromdate, :tmp_todate;

    select afromdate, atodate
      from e_get_whole_eabsperiod(:eabsence, :tmp_fromdate, :tmp_todate, :l4_serial, :l4_number, null, null)
      into :benefit_fromdate, :benefit_todate;

  --wynagrodzenie chorobowe (dla okresu wczesniejeszego)
    tmp_todate = tmp_fromdate - 1;
    tmp_fromdate = extract(year from tmp_todate) || '/' ||extract(month from tmp_todate) || '/1';

    select first 1 a.ref, case when r.symbol <> '' then r.symbol else cast(r.number as varchar(20)) end, r.payday
      from eabsences a
        left join epayrolls r on (r.ref = a.epayroll)
      where coalesce(a.certserial,'') = :l4_serial
        and coalesce(a.certnumber,'') = :l4_number
        and a.fromdate >= :tmp_fromdate
        and a.todate <= :tmp_todate
        and a.ecolumn in (40, 50, 60)
        and a.employee = :employee
        and a.correction in (0,2)
      order by a.fromdate desc
      into :illeabs_ref, :illpayroll_nr, :illpayroll_date;

    if (illeabs_ref is not null) then
      select afromdate, atodate
        from e_get_whole_eabsperiod(:illeabs_ref, :tmp_fromdate, :tmp_todate, :l4_serial, :l4_number, null, null)
        into :illpay_fromdate, :illpay_todate;

  --poprzedni zasilek
    select afromdate, atodate
      from e_get_whole_eabsperiod(:eabsence, :tmp_fromdate, :tmp_todate, :l4_serial, :l4_number, null, null)
      into :prev_benefit_fromdate, :prev_benefit_todate;

    if ( prev_benefit_fromdate is not null) then
    begin
      if(aecolumn < 130) then prev_benefit_name = 'chorobowy';
      else if(aecolumn in (160, 170)) then prev_benefit_name = 'opiekuńczy';
      else if(aecolumn in (140, 150, 270, 280, 290, 440, 450)) then benefit_name = 'macierzyński';
      else prev_benefit_name = 'świadczenie rehabilitacyjne';

      if (aecolumn >= 350) then
        prev_benefit_decision = 'decyzji ZUS';
      else
        prev_benefit_decision = 'zaświadczenia lekarskiego ZUS ZLA jw.';

      select first 1 case when r.symbol <> '' then r.symbol else cast(r.number as varchar(20)) end, r.payday
        from eabsences a
        left join epayrolls r on (r.ref = a.epayroll)
        where a.fromdate >= :prev_benefit_fromdate
          and a.todate <= :prev_benefit_todate
          and a.ecolumn = :aecolumn
          and a.employee = :employee
          and a.correction in (0,2)
        order by fromdate desc
        into :prev_benefitpayroll_nr, :prev_benefitpayroll_date;
    end

    if (coalesce(assettling,0) = 0) then
      suspend;
    else if (prev_benefit_todate is not null) then
    begin
    --generwoanie tabeli wyplat ------------------------------------------------
      tmp_fromdate = null;
      tmp_todate = null;
      if (aecolumn in (40, 50, 60, 90, 100, 110, 160, 170) and l4_fromdate is not null) then
      begin
      --dla zasilkow chorobowych i opieki wymagamy nr i serie zaswiadczenia
        recol_list = ';40;50;60;90;100;110;160;170;';
        tmp_fromdate = l4_fromdate;
        tmp_todate = prev_benefit_todate;
      end else
      if (aecolumn in (140, 150, 270, 280, 290, 350, 360, 370, 440, 450)) then
      begin
      --dla macierzynskiego i sw. rehabilitacyjnych zaswiadczenia nie uwzgledniamy
        l4_serial = '';
        l4_number = '';
        if (aecolumn in (140, 150, 270, 280, 290, 440, 450)) then
          recol_list = ';140;150;270;280;290;440;450;';
        else
          recol_list = ';350;360;370;';

        select afromdate, atodate
          from e_get_whole_eabsperiod(:eabsence, null, :prev_benefit_todate, null, null, null, :recol_list)
          into :tmp_fromdate, :tmp_todate;
      end

      for
        select ep.number, ep.payday, ec.name, cp.pval, a.days, a.fromdate, a.todate, a.payamount
          from eabsences a
          join ecolumns ec on (a.ecolumn = ec.number)
          left join ecolparams cp on (a.ecolumn = cp.ecolumn and cp.param = 'BASEPR')
          left join epayrolls ep on (ep.ref = a.epayroll)
        where a.employee = :employee
          and :recol_list like '%;' || a.ecolumn || ';%'
          and (coalesce(a.certserial,'') = :l4_serial or :l4_serial = '')
          and (coalesce(a.certnumber,'') = :l4_number or :l4_number = '')
          and a.fromdate >= :tmp_fromdate
          and a.todate <= :tmp_todate
          and a.correction in (0,2)
        order by a.fromdate
        into :tab_number, :tab_payday, :tab_name, :tab_pval, :tab_days, :tab_fromdate, :tab_todate, :tab_payamount
      do
        suspend;
    end
  end
end^
SET TERM ; ^
