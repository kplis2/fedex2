--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHECK_DICT_USING_IN_ACCOUNTS(
      DICTDEF integer,
      SYMBOL ANALYTIC_ID,
      COMPANY integer)
   as
declare variable bkaccount integer;
declare variable a_ref integer;
declare variable i_ref integer;
declare variable mask ACCOUNT_ID;
declare variable len smallint;
declare variable sep char(1);
declare variable yearid integer;
declare variable bk_symbol bksymbol_id;
declare variable ISCOMPANY smallint;
begin
 -- exception test_break:dictdef||' '||:symbol||' '||company;
  select s.iscompany
    from slodef s where s.ref=:dictdef
    into :iscompany;
  for
    select bkaccount, ref
      from accstructure where dictdef = :dictdef
      into :bkaccount, :a_ref
  do begin
    --  zbudowanie maski konta
    select symbol, yearid
      from bkaccounts where ref = :bkaccount
      into :bk_symbol, :yearid;
    mask = bk_symbol;
    for
      select ref, separator, len
      from accstructure where bkaccount = :bkaccount
      order by nlevel
      into :i_ref, :sep, :len
    do begin
      if (sep <> ',') then
        mask = mask || sep;
      if (a_ref = i_ref) then
        mask = mask || :symbol;
      else
        mask = mask || substring('____________________' from  1 for  len);
    end
    if (exists(select ref from accounting
        where yearid = :yearid and account starting with :bk_symbol
          and account like :mask and
          (:iscompany=0 or (:iscompany=1 and company = :company))))
    then
      exception cannot_modify_slopoz;
  end
end^
SET TERM ; ^
