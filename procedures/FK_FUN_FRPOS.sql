--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_FUN_FRPOS(
      COMPANY integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      POS smallint)
  returns (
      AMOUNT numeric(15,2))
   as
begin
  if (not exists (select ref from frvhdrs where frhdr = :symbol and period = :period and company = :company))
    then exception NO_FRVHDR;

  select F.amount
    from frvpsns F
      join frvhdrs FH on (F.frvhdr = FH.ref) -- ref frvhdr
      join frpsns FP on (F.frpsn = FP.ref) -- ref frpsns
      join frcols FC on (F.frcol = FC.ref) -- ref frcols
    where FH.frhdr = :symbol and FH.period = :period
      and FP.number = :pos and FC.forbo = 0 and FH.company = :company
    into :amount;
  suspend;
end^
SET TERM ; ^
