--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PIT40_V17(
      CURRENTCOMPANY integer,
      YEARID char(4) CHARACTER SET UTF8                           ,
      PERSON_IN integer,
      TYP smallint,
      DATA date,
      ULGA numeric(14,2))
  returns (
      P35 numeric(14,2),
      P36 numeric(14,2),
      P37 numeric(14,2),
      P38 numeric(14,2),
      P50 numeric(14,2),
      P51 numeric(14,2),
      P52 numeric(14,2),
      P53 numeric(14,2),
      P54 numeric(14,2),
      P55 numeric(14,2),
      P56 numeric(14,2),
      P57 numeric(14,2),
      P62 numeric(14,2),
      P63 numeric(14,2),
      P64 numeric(14,2),
      P65 numeric(14,2),
      P66 numeric(14,2),
      P68 integer,
      P69 numeric(14,2),
      P71 numeric(14,2),
      P72 numeric(14,2),
      P73 numeric(14,2),
      P74 numeric(14,2),
      P75 numeric(14,2),
      P76 numeric(14,2),
      P79 numeric(14,2),
      P80 numeric(14,2),
      P81 numeric(14,2),
      PERSON integer,
      COSTS smallint,
      TAXOFFICE varchar(200) CHARACTER SET UTF8                           ,
      INFONIP varchar(13) CHARACTER SET UTF8                           )
   as
declare variable AKTUOPERATOR integer;
declare variable DESCRIPT varchar(255);
declare variable ULGA_ROCZNA numeric(14,2);
declare variable TAXSCALE numeric(14,2);
declare variable MINTAX numeric(14,2);
declare variable TAMOUNT numeric(14,2);
declare variable NEXTAMOUNT numeric(14,2);
begin

  execute procedure getconfig('INFONIP') returning_values :infonip;
  execute procedure efunc_get_format_nip(:infonip) returning_values :infonip;

  if (person_in = 0) then person_in = null;

  for
    select distinct e.person
      from epayrolls PR 
        join eprpos PP on (PR.ref = PP.payroll)
        join employees e on (pp.employee = e.ref)
      where PP.pvalue <> 0 and PP.ecolumn in (3000, 5950)
        and PR.tper starting with :yearid
        and (e.person = :person_in or :person_in is null)
        and PR.company = :currentcompany
      into :person
  do begin

    p35 = null;
    p36 = null;
    p37 = null;
    p38 = null;
    p50 = null;
    p51 = null;
    p52 = null;
    p53 = null;
    p54 = null;
    p55 = null;
    p56 = null;
    p57 = null;
    p62 = null;
    p63 = null;
    p64 = null;
    p65 = null;
    p66 = null;
    p68 = null;
    p72 = null;
    p81 = null;

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 1)
        join employees e on (e.ref = pp.employee)
        join ecolumns c on (pp.ecolumn = c.number)
      where pr.tper starting with :yearid
        and e.person = :person
        and ((c.number = 5950 and pr.prtype = 'SOC') --(BS36243)
          or (c.cflags like '%;POD;%' and pr.prtype <> 'SOC' and c.number <> 5950))
      into :p35;
    p35 = coalesce(p35,0);  -- przychod

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 1)
        join employees e on (e.ref = pp.employee)
      where PP.ecolumn = 6500 and e.person = :person
        and PR.tper starting with :yearid
      into :p36;
    p36 = coalesce(p36,0);  -- koszty uzyskania

    p37 = :p35 - :p36;  -- dochod

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 1)
        join employees e on (e.ref = pp.employee)
      where PP.ecolumn = 7350 and e.person = :person
        and PR.tper starting with :yearid
      into :p38;
    p38 = coalesce(p38,0);  -- zaliczka na podatek dochodowy

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 2)
        join emplcontracts C on (C.ref = PR.emplcontract)
        join employees e on (e.ref = pp.employee)
      where PP.ecolumn = 3000 and e.person = :person
        and PR.tper starting with :yearid and C.prcosts <> 50
      into :p50;   -- przychod

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 2)
        join emplcontracts C on (C.ref = PR.emplcontract)
        join employees e on (e.ref = pp.employee)
      where PP.ecolumn = 6500 and e.person = :person
        and PR.tper starting with :yearid and C.prcosts <> 50
      into :p51;   -- koszty uzyskania

    p52 = :p50 - :p51;   -- dochod

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 2)
        join emplcontracts C on (C.ref = PR.emplcontract)
        join employees e on (e.ref = pp.employee)
      where PP.ecolumn in (7350, 7370) and e.person = :person
        and PR.tper starting with :yearid and C.prcosts <> 50
      into :p53;   -- zaliczka na podatek dochodowy (razem z wyrownaniem)

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 2)
        join emplcontracts C on (C.ref = PR.emplcontract)
        join employees e on (e.ref = pp.employee)
      where PP.ecolumn = 3000 and e.person = :person
        and PR.tper starting with :yearid and C.prcosts = 50
      into :p54;   -- przychod

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 2)
        join emplcontracts C on (C.ref = PR.emplcontract)
        join employees e on (e.ref = pp.employee)
      where PP.ecolumn = 6500 and e.person = :person
        and PR.tper starting with :yearid and C.prcosts = 50
      into :p55;   -- koszty uzyskania

    p56 = :p54 - :p55;   -- dochod

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll and PR.empltype = 2)
        join emplcontracts C on (C.ref = PR.emplcontract)
        join employees e on (e.ref = pp.employee)
      where PP.ecolumn in (7350, 7370) and e.person = :person
        and PR.tper starting with :yearid and C.prcosts = 50
      into :p57;   -- zaliczka na podatek dochodowy (razem z wyrownaniem)

    p62 = coalesce(p35, 0) + coalesce(p50, 0) + coalesce(p54, 0);
    p63 = coalesce(p36, 0) + coalesce(p51, 0) + coalesce(p55, 0);
    p64 = coalesce(p37, 0) + coalesce(p52, 0) + coalesce(p56, 0);
    p65 = coalesce(p38, 0) + coalesce(p53, 0) + coalesce(p57, 0);

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos PP on (PR.ref = PP.payroll)
        join employees e on (e.ref = pp.employee)
      where PP.ecolumn in (6100, 6110, 6120) and e.person = :person
        and PR.tper starting with :yearid
      into :p66;   -- skladki na ubezpieczenie spoleczne
    p66 = coalesce(p66, 0);

    p68 = round(p64 - p66); --round_downup(p64 - p66, 1, 1);

    execute procedure get_pval(cast(:yearid || '-01-01' as date), currentcompany, 'ROCZNAULGA')
    returning_values :ulga_roczna;
    
    --ponizszy zapis to w wersji bardziej czytelnej:
    --p69 = 0.18 * min(p68, kwota_zmiany_skali) - ulga_roczna
    --      + suma(taxscale * (min(p68,kwota_zmiany_skali) - kwota_poczatku_skali))
    p69 = 0;
    --poniewaz do wyliczenia podatku potrzebujemy procentu ze skali biezacej
    --i kwoty ograniczenia, ktora zapisana jest przy nastepnej skali (jako: powyzej kwoty)
    --musimy zrobic 2 selecty
    select min(t.taxscale) / 100.00
      from etaxlevels t
      where t.taxyear = :yearid
      into :mintax;
    
    select first 1 t.amount
      from etaxlevels t
      where t.taxyear = :yearid
        and t.amount > 0
      order by t.taxscale
      into :tamount;
    
    if (p68 < tamount or tamount is null) then
      p69 = mintax * p68 - ulga_roczna;
    else begin
      p69 = mintax * tamount - ulga_roczna;
      for
        select t.amount, t.taxscale / 100.00
          from etaxlevels t
          where t.taxyear = :yearid
            and t.amount > 0
          order by t.taxscale
          into :tamount, :taxscale
      do begin
        nextamount = null;
        select first 1 t.amount
          from etaxlevels t
          where t.taxyear = :yearid
            and t.taxscale > :taxscale * 100
          order by t.taxscale
          into :nextamount;
        
        if (nextamount is null or p68 < nextamount) then
          p69 = p69 + taxscale * (p68 - tamount);
        else
          p69 = p69 + taxscale * (nextamount - tamount);
      end
    end
    
    if (p69 < 0) then p69 = 0;

    p71 = p69;  --podatek

    select sum(PP.pvalue)
      from epayrolls PR
        join eprpos pp on (PR.ref = PP.payroll)
        join employees e on (e.ref = pp.employee)
      where PP.ecolumn = 7210 and e.person = :person
        and PR.tper starting with :yearid      
      into :p72;   -- skladki na ubezpieczenie zdrowotne
    p72 = coalesce(p72, 0);

    if (p72 > p71) then p73 = p71;
    else p73 = p72; --skladki na ubezpieczenie zdrowotne możliwe do odliczenia

    if (ulga <> 0) then p81 = ulga;

    if (p81 > p71 - p73) then  p81 = p71 - p73;

    p74 = round(p71 - p73 - coalesce(p81,0));  --podatek nalezny

    if (p74 > p65) then p75 = p74 - p65;
    else p75 = 0; --do zaplaty

    if (p65 > p74) then p76 = p65 - p74;
    else p76 = 0; --nadplata

    p79 = p76;
    p80 = p79 - p76;

    --pobranie informacji podatkowych (US, koszty uzyskania, itp.)
    select costs, taxoffice from e_get_perstaxinfo4pit(:yearid, :person) --BS44508
      into :costs, :taxoffice;

    suspend;
  
    if (typ = 1) then
    begin
      execute procedure get_global_param ('AKTUOPERATOR')
        returning_values aktuoperator;
      descript = 'Przekazanie danych na formularzu PIT-40 do ' || coalesce(taxoffice, 'Urzędu Skarbowego');
      if (not exists (select first 1 1 from epersdatasecur
                        where person = :person and descript = :descript and regdate = :data
                          and coalesce(operator,0) = coalesce(:aktuoperator,0) and kod = 2)
      ) then
        insert into epersdatasecur (person, kod, descript, operator, regdate)
          values (:person, 2, :descript, :aktuoperator, :data);
    end
  end
end^
SET TERM ; ^
