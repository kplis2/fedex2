--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SETTLE_DECREEMATCHING(
      DM1REF DECREEMATCHINGS_ID,
      DM2REF DECREEMATCHINGS_ID,
      DMGROUPIN DECREEMATCHINGGROUPS_ID)
  returns (
      DMGROUP DECREEMATCHINGGROUPS_ID)
   as
declare variable dmg1 decreematchinggroups_id;
declare variable dmg2 decreematchinggroups_id;
declare variable dmd1 date_id;
declare variable dmd2 date_id;
declare variable dmdate date_id;
declare variable bcaslodef1 bkaccounts_id;
declare variable bcaslodef2 bkaccounts_id;
begin
  select m.decreematchinggroup, a.matchingslodef
    from decreematchings m
      left join decrees d on (d.ref = m.decree)
      left join bkdocs b on (b.ref = d.bkdoc)
      left join bkaccounts a on (a.ref = m.bkaccount)
    where m.ref = :dm1ref
    into :dmg1, :bcaslodef1;
  select m.decreematchinggroup, a.matchingslodef
    from decreematchings m
      left join decrees d on (d.ref = m.decree)
      left join bkdocs b on (b.ref = d.bkdoc)
      left join bkaccounts a on (a.ref = m.bkaccount)
    where m.ref = :dm2ref
    into :dmg2,:bcaslodef2;
  if (:bcaslodef1 <> :bcaslodef2) then
    exception decreematching_error 'Nie można rozliczać różnych kont z różnymi słownikami rozliczeń.';
  if (:dmg1 > 0 and :dmg2 > 0 and :dmg1 <> :dmg2) then
    exception decreematching_error 'Próba powiązania rekordów rozliczonych w różnych grupach.';
  if (:dmg1 > 0 and :dmg2 > 0 and :dmg1 = :dmg2) then
    exception decreematching_error 'Rekordy są już powiązane w tej samej grupie.';
  if (:dmg1 > 0) then
    dmgroup = :dmg1;
  else if (:dmg2 > 0) then
    dmgroup = :dmg2;
  else
    dmgroup = :dmgroupin;
  execute procedure insert_decreematchinggroup(:dmgroup)
    returning_values (:dmgroup);
  if (:dmgroup is null) then
    update decreematchings m set m.decreematchinggroup = :dmgroup where m.ref in (:dm1ref,:dm2ref);
end^
SET TERM ; ^
