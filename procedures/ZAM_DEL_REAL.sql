--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ZAM_DEL_REAL(
      ZAM integer,
      REAAL integer)
   as
declare variable ilreal decimal(14,4);
declare variable toreal decimal(14,4);
declare variable tozreal decimal(14,4);
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable cena decimal (14,2);
declare variable mag char(3);
declare variable rabat decimal(14,2);
declare variable numer integer;
declare variable histzam integer;
declare variable cnt integer;
begin
  /* funkja zwiksza ilosci zmniejsza na pozycjach dokumentu na podstawie
    wartoci pól ILZREAL z pozycji realizacji */
    for select MAGAZYN,KTM,WERSJA,CENACEN,RABAT,ILZREAL from POZZAM where ZAMOWIENIE=:reaal
     into :mag, :ktm, :wersja, :cena, :rabat, :ilreal
    do begin
       if(:ilreal > 0) then
       for select NUMER, ILZREAL from POZZAM where KTM=:ktm AND WERSJA=:wersja AND ZAMOWIENIE=:zam into :numer, :tozreal
       do begin
         if(:ilreal > 0) then begin
           if(:tozreal is null) then tozreal = 0;
           if(:ilreal >  :tozreal) then toreal = tozreal;
           else toreal = ilreal;
           tozreal = tozreal - toreal;
           update POZZAM set ILZREAL=:tozreal where ZAMOWIENIE=:zam AND NUMER=:numer;
           ilreal = ilreal - toreal;
         end
       end
    end
    select org_oper from NAGZAM where REF=:REAAL into :histzam;
    if(:histzam > 0) then begin
       select count(*) from NAGZAM where ORG_OPER = :histzam and REF<>:REAAL into :cnt;
       if(:cnt is null or (:cnt = 0))then
         update HISTZAM set REALOUT = 0 where ref = :histzam;
    end
end^
SET TERM ; ^
