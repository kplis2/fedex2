--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_POZZAM_DEL_CASCADE(
      POZZAMIN integer,
      DELCURR smallint)
   as
declare variable pozzam integer;
declare variable nagzam integer;
begin
  if (delcurr is null) then delcurr = 0;
  for
    select  c.ref, c.prnagzamout
      from PR_POZZAM_CASCADE(:pozzamin,0,0,0,1) c
        left join pozzam p on (p.ref = c.ref)
      where c.ordertypeout = 3 and p.ilosc = 0
      order by c.numberout desc
      into pozzam, nagzam
  do begin
    if (delcurr = 1 or pozzam <> pozzamin) then
    begin
      delete from pozzam where ref = :pozzam;
      if (not exists(select first 1 ref from pozzam where zamowienie = :nagzam)) then
        delete from nagzam where ref = :nagzam;
    end
  end
end^
SET TERM ; ^
