--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_CROSSDOC_CHECK_BEZKLIENTA(
      ZMIENNA varchar(1024) CHARACTER SET UTF8                           )
  returns (
      BEZKLIENTA smallint)
   as
declare variable tmp varchar(1024);
begin
  bezklienta = 1;
  if (coalesce(:zmienna,'') <> '') then
  begin
    execute procedure get_config('X_CROSSDOC_BEZKLI',0) returning_values :tmp;
    if (:tmp is null) then tmp = '';
    if (:tmp like '%;'||:zmienna||';%') then
      bezklienta = 0;
  end
end^
SET TERM ; ^
