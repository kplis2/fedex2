--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_TRANSFER_KLIENCI as
declare variable s1 varchar(255); /* kod ksiegowy */
declare variable s2 varchar(255); /* nazwa */
declare variable s3 varchar(255); /* nip */
declare variable s4 varchar(255); /* kraj */
declare variable s5 varchar(255); /* waluta domyslna */
declare variable s6 varchar(255); /* firma */
declare variable s7 varchar(255); /* czy klient jest aktywny ? */
declare variable s8 varchar(255); /* grupa klientow */
declare variable s9 varchar(255); /* skrot */
declare variable s10 varchar(255); /* czy klient zagraniczny */
declare variable s11 varchar(255); /* ulica */
declare variable s12 varchar(255); /* miasto */
declare variable s13 varchar(255); /* wojewodztwo */
declare variable s14 varchar(255); /* telefon */
declare variable s15 varchar(255); /* komorka */
declare variable s16 varchar(255); /* fax */
declare variable s17 varchar(255); /* regon */
declare variable s18 varchar(255); /* zaklad */
declare variable s19 varchar(255); /* rachunek bankowy */
declare variable s20 varchar(255); /* email */
declare variable s21 varchar(255); /* uwagi */
declare variable s22 varchar(255); /* kod zewnetrzny */
declare variable s23 varchar(255); /* limit kredytowy */
declare variable s24 varchar(255); /* liczba dni zwloki */
declare variable s25 varchar(255); /* sposob platnosci */
declare variable kontofk varchar(20);
declare variable nazwa varchar(255);
declare variable nip varchar(15);
declare variable kraj varchar(40);
declare variable krajid varchar(20);
declare variable waluta varchar(3);
declare variable firma smallint;
declare variable aktywny smallint;
declare variable grupa integer; /* ref na grupykli */
declare variable fskrot shortname_id;  --XXX ZG133796 MKD
declare variable zagraniczny smallint;
declare variable ulica varchar(60);
declare variable miasto varchar(60);
declare variable cpwoj16m integer; /* ref na cpwoj16m */
declare variable telefon varchar(255);
declare variable komorka varchar(255);
declare variable fax varchar(255);
declare variable regon varchar(20);
declare variable company integer; /* ref na companies */
declare variable rachunek varchar(40);
declare variable email varchar(255);
declare variable uwagi varchar(1024);
declare variable kodzewn varchar(40);
declare variable limitkr numeric(14,2);
declare variable termin integer;
declare variable sposplat integer; /* ref na platnosci */
declare variable tmp integer;
begin

-- sprawdzanie dlugoscie danych w tabeli expimp
  select count(ref) from expimp where coalesce(char_length(s1),0)>20 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich kont ksiegowych.';

  select count(ref) from expimp where coalesce(char_length(s2),0)>80 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich nazw.';

  select count(ref) from expimp where coalesce(char_length(s3),0)>15 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich NIP.';

  select count(ref) from expimp where coalesce(char_length(s4),0)>40 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich nazw krajow.';

  select count(ref) from expimp where coalesce(char_length(s5),0)>3 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich walut.';

  select count(ref) from expimp where cast(:s6 as smallint)<>0 and cast(:s6 as smallint)<>1
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba zlych wartosci w kolumnie FIRMA.';

  select count(ref) from expimp where cast(:s7 as smallint)<>0 and cast(:s7 as smallint)<>1
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba zlych wartosci w kolumnie AKTYWNY.';

  select count(ref) from expimp where coalesce(char_length(s8),0)>60 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich grup klientow.';

  select count(ref) from expimp where coalesce(char_length(s9),0)>40 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich skrotow.';

  select count(ref) from expimp where cast(:s10 as smallint)<>0 and cast(:s10 as smallint)<>1
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba zlych wartosci w kolumnie AKTYWNY.';

  select count(ref) from expimp where coalesce(char_length(s11),0)>60 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich ulic.';

  select count(ref) from expimp where coalesce(char_length(s12),0)>60 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich miast.';

  select count(ref) from expimp where coalesce(char_length(s12),0)>40 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich wojewodztw.';

  select count(ref) from expimp where coalesce(char_length(s17),0)>20 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich regonow.';

  select count(ref) from expimp where coalesce(char_length(s18),0)>10 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich nazw zakladow.';


  select count(ref) from expimp where coalesce(char_length(s19),0)>40 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich rachunkow bankowych.';

  select count(ref) from expimp where coalesce(char_length(s22),0)>40 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich kodow zewnetrznych.';


-- import

  for select s1, s2,  s3,  s4,  s5,  s6,  s7,  s8,  s9,  s10,
            s11, s12, s13, s14, s15, s16, s17, s18, s19, s20,
            s21, s22, s23, s24, s25
    from expimp 
  into :s1, :s2,  :s3,  :s4,  :s5,  :s6,  :s7,  :s8,  :s9,  :s10,
      :s11, :s12, :s13, :s14, :s15, :s16, :s17, :s18, :s19, :s20,
      :s21, :s22, :s23, :s24, :s25
  do begin

/* kod ksiegowy */
     kontofk = substring(:s1 from  1 for  20);

/* nazwa */
     nazwa = substring(:s2 from  1 for  80);

/* nip */
     nip = substring(:s3 from  1 for  15);

/* kraj */
     kraj = substring(:s4 from  1 for  40);
     select symbol from countries where name=:kraj
     into :krajid;
     if (:krajid is null) then
       exception universal 'W tabeli COUNTRIES nie ma kraju: ' || :kraj;

/* waluta domyslna */
    waluta = substring(:s5 from  1 for  3);

/* firma */
    firma = cast(:s6 as smallint);

/* czy klient jest aktywny ? */
    aktywny = cast(:s7 as smallint);

/* grupa klientow */
    select ref from grupykli 
      where opis = substring(:s8 from  1 for 60)
    into :grupa;
    -- jesli nie istnieje dana grupa to jest tworzona
    if(grupa is null) then
      insert into grupykli(opis)
      values (substring(:s8 from 1 for 60));

/* skrot */
    fskrot = substring(:s9 from 1 for 40);

/* czy klient zagraniczny */
    zagraniczny = cast(:s10 as smallint);

/* ulica */
    ulica = substring(:s11 from  1 for  60);

/* miasto */
    miasto = substring(:s12 from  1 for  60);

/* wojewodztwo */
    select ref from cpwoj16m
      where opis = substring(:s13 from 1 for 40)
    into :cpwoj16m;

/* telefon */
    telefon = :s14;

/* komorka */
    komorka = :s15;

/* fax */
    fax = :s16;

/* regon */
    regon = :s17;

/* zaklad */
    select ref from companies
      where symbol = substring(:s18 from 1 for 10)
    into :company;
    if (company is null) then
      exception universal 'Zaklad: ' || substring(:s18 from 1 for 10) || ' nie znajduje sie w  bazie.';

/* rachunek bankowy */
    rachunek = substring(:s19 from  1 for  40);

/* email */
    email = :s20;

/* uwagi */
    uwagi = :s21;

/* kod zewnetrzny */
    kodzewn = :s22;

/* limit kredytowy */
    s23 = replace(:s23, ',', '.');
    limitkr = cast(:s23 as numeric(14,2));

/* liczba dni zwloki */
    termin = cast(:s24 as integer);

/* sposob platnosci */
    select ref from platnosci
      where opis = substring(:s25 from 1 for 40)
    into :sposplat;
    if (sposplat is null) then
      exception universal 'Sposob platnosci: ' || substring(:s25 from 1 for 40) || ' nie znajduje sie w  bazie.';

-- INSERT
    insert into klienci (kontofk,  nazwa, nip, kraj, krajid,  waluta,  firma, aktywny, grupa,
                         fskrot,  zagraniczny, ulica, miasto, cpwoj16m, telefon, komorka, fax,
                         regon, company, rachunek, email, uwagi, kodzewn, limitkr, termin, sposplat)
                values (:kontofk, :nazwa, :nip, :kraj, :krajid,  :waluta,  :firma, :aktywny, :grupa,
                         :fskrot,  :zagraniczny, :ulica, :miasto, :cpwoj16m, :telefon, :komorka, :fax,
                         :regon, :company, :rachunek, :email, :uwagi, :kodzewn, :limitkr, :termin, :sposplat);

  end
end^
SET TERM ; ^
