--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_GRUPASED_KOSZT(
      GRUPASPED integer)
   as
declare variable waga numeric(14,4);
declare variable objetosc numeric(14,4);
declare variable koszt numeric(14,2);
begin
  select sum(KOSZTDOST), sum(WAGA * (1-defdokum.koryg*2)), sum(OBJETOSC * (1-defdokum.koryg*2))
    from DOKUMNAG
    left join defdokum on (defdokum.symbol = dokumnag.typ)
    where GRUPASPED = :grupasped
    into :koszt, :waga, :objetosc;
  update DOKUMNAG set KOSZTDOSTGR = :koszt, WAGAGR = :waga, OBJETOSCGR = :objetosc
    where GRUPASPED = :grupasped;
end^
SET TERM ; ^
