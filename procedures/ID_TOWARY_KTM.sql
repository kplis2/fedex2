--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ID_TOWARY_KTM
  returns (
      ID integer)
   as
BEGIN
  id = gen_id(KTM_GEN, 1);
  execute procedure RP_CHECK_REF('TOWARY', :id)
    returning_values :id;
  suspend;
END^
SET TERM ; ^
