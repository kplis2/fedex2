--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_CALC_READYTOHARM_CHECK(
      PRSCHEDOPER integer)
   as
begin
  if (not exists (select o.ref
   from  prschedoperdeps d
   left join prschedopers o on (d.depfrom = o.ref)
    where d.depto = :prschedoper and o.activ = 1 and o.verified = 0 and o.status < 3)
  ) then
     update prschedopers set readytoharm = 1 where ref = :prschedoper;
end^
SET TERM ; ^
