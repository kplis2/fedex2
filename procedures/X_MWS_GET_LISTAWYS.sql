--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_GET_LISTAWYS(
      OPERATORREF integer,
      INLISTAWYS integer)
  returns (
      LISTAWYS integer,
      SYMBOL varchar(40) CHARACTER SET UTF8                           ,
      CNT smallint)
   as
begin
  cnt = 0;

  if (coalesce(inlistawys,0) = 0) then
  begin
    select count(l.ref)
      from listywys l
        left join kierowcy k on (l.kierowca = k.ref)
        left join operator o on (k.x_operator = o.ref)
      where l.stan = 'O'
        and o.ref = :operatorref
    into :cnt;
  
    select first 1 l.ref, l.trasanazwa||' dn. '||l.dataplwys
      from listywys l
        left join kierowcy k on (l.kierowca = k.ref)
        left join operator o on (k.x_operator = o.ref)
      where l.stan = 'O'
        and o.ref = :operatorref
      order by l.dataotw, l.ref
    into :listawys, :symbol;
  end
  else
  begin
    cnt = 1;
    select first 1 l.ref, l.trasanazwa||' dn. '||l.dataplwys
      from listywys l
      where l.ref = :inlistawys
    into :listawys, :symbol;
  end

  suspend;
end^
SET TERM ; ^
