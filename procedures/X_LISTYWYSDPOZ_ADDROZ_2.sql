--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_LISTYWYSDPOZ_ADDROZ_2(
      KODKRESK STRING40,
      PACKOPER OPERATOR_ID,
      MWSORD MWSORDS_ID,
      TYPOPK INTEGER_ID,
      MNOZNIK INTEGER_ID = 1)
  returns (
      MWSACT MWSACTS_ID,
      LISTWYSD LISTYWYSD_ID,
      LISTWYSDPOZ LISTYWYSDPOZ_ID,
      AKTOPK INTEGER_ID,
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable ktm ktm_id;
declare variable wersjaref wersje_id;
declare variable ilosc ilosci_mag;
declare variable jedn jedn_miary;
declare variable towjednref towjedn;
declare variable pozycja integer_id;
declare variable ilosctmp ilosci_mag;  --[PM] XXX
declare variable docref dokumnag_id;
declare variable iloscdospk ilosci_mag;
declare variable aktopkstr string40;
declare variable multi smallint_id;
begin
  --[PM] XXX #POJEDYNKI
  --na podstawie X_LISTYWYSDPOZ_ADDROZ
  exception universal mwsord ||' '||typopk;

  status = 1;
  msg = '';
  kodkresk = upper(:kodkresk);

  execute procedure XK_MWS_BARCODE_VALIDATION(:kodkresk)
    returning_values :ktm, :wersjaref, :ilosc, :status, :jedn, :towjednref;

  --sprawdzenie towaru
  if (wersjaref is null) then
  begin
    msg = 'Nie znaleziono towaru. ';
    status = 0;
  end

  if (:mnoznik < 0) then mnoznik = 1;
  ilosc = :ilosc * :mnoznik;
  ilosctmp = ilosc;

  if (coalesce(ilosctmp,0) <> 1) then
  begin
    msg = msg||'Czy ilosc nie powinna byc 1 ?! '; --[PM] na wszelki wypadek, to sa POJEDYNKI
    status = 0;
  end

  select multi from mwsords where ref = :mwsord into :multi;
  if (coalesce(multi,0) <> 2) then
  begin
    msg = 'To nie jest zlecenie na pojedynki. ';
    status = 0;
  end  

  if (:status = 1) then
  begin
    select first 1 ma.ref, ma.quantityc, ma.docid, ld.ref, lp.ref, lp.ilosc-coalesce(lp.iloscspk,0)
      from mwsacts ma left join listywysdpoz lp on ma.docposid = lp.dokpoz and ma.doctype = lp.doktyp
          left join listywysd ld on lp.dokument = ld.ref
        where ma.mwsord = :mwsord and ma.vers = :wersjaref
          and ma.quantityc = :ilosctmp and ma.status = 5
          and (ld.ref is null or ld.akcept < 2)
      order by ld.ref nulls last
      into :mwsact, :ilosc, :docref, :listwysd, :listwysdpoz, :iloscdospk;

    if (mwsact is null) then
      begin
        msg = 'Brak towaru o podanym kodzie kreskowym';
        status = 0;
        suspend;
        exit;
      end

    if (listwysd is null) then --zaloz list spedycyjny
      begin
        execute procedure listywysd_add_doc(:docref,'M',null,0,:packoper)
          returning_values :listwysd;
      end
    if (listwysdpoz is not null and iloscdospk = 0) then
      begin
        status = 1;
        msg = 'Zaczales kiedys pakowanie tego produktu, ale nie skonczyles. ';
      end
    else
     begin
      --[PM] XXX start , jesli jest mniej niz jeden to dobijam roznice
      --if (:pozycja = -1 and mnoznik = 1 and ilosc > -1.00 and ilosc < 0) then
        --begin
          --ilosc = ilosctmp + ilosc;
      select rpozycja, rilosc
        from XK_LISTYWYSDROZ_GETPOZ(:listwysd, :wersjaref, :ilosctmp)
        into :pozycja, :ilosc;
        --end
      --[PM] XXX koniec
          
      if (:pozycja = -1) then
        begin
          wersjaref = 0;
          status = 0;
          pozycja = 0;
          if (:ilosc is null) then
            msg = 'Brak towaru na dokumencie magazynowym!';
          else if(:ilosc < 0) then
            msg = 'Przekroczono ilość do spakowania o '||cast(abs(:ilosc) as numeric(14,2))||' sztuk!';
          else msg = 'Brak towaru na dokumencie lub przekroczono ilość do spakowania!';
          ilosc = 1;
          suspend;
          exit;
        end
 
      listwysdpoz = pozycja;

      execute procedure x_listywysd_getopk('OPKOTW', :listwysd, :typopk) --[PM] tak samo jak w okienku do pakowania
        returning_values :aktopk, :aktopkstr;

      if (aktopk is null) then
        begin
        status = 0;
        msg = 'Nie udalo sie pobrac opakowania. Zadzwon do Sente.';
      end

      insert into listywysdroz(listywysd, listywysdpoz, iloscmag, opk)
        values(:listwysd, :pozycja, :ilosc, :aktopk);

      update listywysdroz_opk lo set lo.x_spakowano = 1 where lo.ref = :aktopk
        and coalesce(lo.x_spakowano,0) = 0;

      if (not exists(select first 1 1 from listywysdroz_opk where listwysd = :listwysd and x_spakowano = 0)) then
        begin
          if (exists(select first 1 1 from mwsords mo left join mwsacts ma on ma.mwsord = mo.ref
                  left join listywysdpoz lp on lp.doktyp = ma.doctype and lp.dokpoz = ma.docposid
                  left join listywysdroz_opk lro on lro.listwysd = lp.dokument
                  --left join listywysd ld on lro.listwysd = ld.ref
                where mo.ref = :mwsord and coalesce(lro.x_spakowano,0) = 0)) then
            begin
              msg = msg||'Spakowano wszystkie pozycje z dokumentu';
              status = 2;
            end
          else
            begin
              msg = msg||'Spakowano WSZYSTKIE pozycje.';
              status = 3;
            end
        end
      else
        begin
          msg = 'Cos poszlo nie tak. Zadzwon do Sente.'; --[PM] dla pojedynek status ZAWSZE powinien byc 2
          status = 0;
        end
    end
  end

  suspend;
end^
SET TERM ; ^
