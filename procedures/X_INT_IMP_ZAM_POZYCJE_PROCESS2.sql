--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_INT_IMP_ZAM_POZYCJE_PROCESS2(
      SESJAREF SESJE_ID,
      TABELA STRING35,
      REF INTEGER_ID,
      BLOKUJZALEZNE SMALLINT_ID = 0,
      TYLKONIEPRZETWORZONE SMALLINT_ID = 0,
      AKTUALIZUJSESJE SMALLINT_ID = 1)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable stabela string35;
declare variable tmptabela string35;
declare variable sprocedura string255;
declare variable szrodlo zrodla_id;
declare variable skierunek smallint_id;
declare variable sesja sesje_id;
declare variable sql memo;
declare variable tmpsql memo;
declare variable tmpstatus smallint_id;
declare variable tmpmsg string255;
declare variable error_row smallint_id;
declare variable errormsg_row string255;
declare variable errormsg_all string255;
declare variable tmperror smallint_id;
declare variable statuspoz smallint_id;
declare variable KTM KTM_ID;
declare variable WERSJAREF WERSJE_ID;
declare variable SPOSZAP PLATNOSC_ID;
declare variable SPOSDOST SPOSDOST_ID;
declare variable KLIENT KLIENCI_ID;
declare variable KLIENTHIST KLIENCIHIST_ID;
declare variable DOSTAWCA DOSTAWCA_ID;
declare variable DOKUMENT DOKUMNAG_ID;
declare variable ODDZIAL ODDZIAL_ID;
declare variable COMPANY COMPANIES_ID;
declare variable magazyn defmagaz_id;
declare variable mag2 defmagaz_id;
declare variable nagid string120;
declare variable typdokmag defdokum_id;
declare variable typdokvat typfak_id;
declare variable typdokmagkor defdokum_id;
declare variable sprzedawca sprzedawca_id;
declare variable operator operator_id;
declare variable termdost timestamp_id;
declare variable dostawa dostawa_id;
declare variable odbiorca odbiorcy_id;
declare variable symbol string40;
declare variable bn char_1;
declare variable waluta waluta_id;
declare variable kurs kurs;
declare variable pozid string120;
declare variable ilosc ilosci_mag;
declare variable sumilosc ilosci_mag;
declare variable refdoc dokumnag_id;
declare variable kordoc dokumnag_id;
declare variable refkordoc dokumnag_id;
declare variable uwagi memo;
declare variable uwagiwewn memo;
declare variable uwagisped memo;
declare variable grupasped dokumnag_id;
declare variable stop smallint_id;
declare variable refkorpoz dokumpoz_id;
declare variable korpozil ilosci_mag;
declare variable korilosc ilosci_mag;
declare variable data timestamp_id;
declare variable cenanet ceny;
declare variable cenabru ceny;
declare variable cenanetzl ceny;
declare variable cenabruzl ceny;
begin
  status = 1;
  msg = '';
  errormsg_all = '';

  --zmiana pustych wartosci na null-e
  if (:sesjaref = 0) then
    sesjaref = null;
  if (trim(:tabela) = '') then
    tabela = null;
  if (:ref = 0) then
    ref = null;
  if (:blokujzalezne is null) then
    blokujzalezne = 0;
  if (:tylkonieprzetworzone is null) then
    tylkonieprzetworzone = 0;
  if (:aktualizujsesje is null) then
    aktualizujsesje =1;

  --sprawdzenie czy wszystkie potrzebne informacje sa
  if (:sesjaref is null and :ref is null) then
  begin
    status = -1;
    msg = 'Za mało parametrów. Brak numeru sesji oraz REF';
    exit; --EXIT
  end

  if (:sesjaref is null) then
  begin
    if (:tabela is null) then
    begin
      status = -1;
      msg = 'Za mało parametrów. Brak numeru sesji oraz nazwy tabeli';
      exit; --EXIT
    end

    sql = 'select sesja from '||:tabela||' where ref='||:ref;
    execute statement :sql into :sesja;

  end
  else
    sesja = :sesjaref;

  if (:sesja is null) then
  begin
    status = -1;
    msg = 'Nie znaleziono numeru sesji.';
    exit; --EXIT
  end

  --pobranie parametrow sesji
  select s.tabela, s.procedura, s.zrodlo, s.kierunek
    from int_sesje s
    where ref = :sesja
  into :stabela, :sprocedura, :szrodlo, :skierunek;

  if (:tabela is not null) then --jesli podano tabele na wejsciu traktuje ja nadrzednie
    stabela = :tabela;

  --wlasciwe przetworzenie
  execute procedure set_global_param ('AKTUOPERATOR',74); --#JOTOOD
  for
    select p.nagid, p.pozid, n.symbol, n.datazam, n.bn_sente, n.company_sente,
        'MWS' as magazyn, --case when p.magazyn is not null and p.magazyn <> '' then p.magazyn else n.magazyn_sente end, #JOTOOD
        null as mag2, --case when p.magazyn2 is not null and p.magazyn2 <> '' then p.magazyn else n.mag2_sente end, #JOTOOD
        n.typdokmag_sente, n.typdokvat_sente, n.sposzap_sente, n.sposdost_sente,
        n.waluta_sente, coalesce(n.kurs_sente,1), n.sprzedawca_sente,
        n.klient_sente, n.klienthist_sente, n.odbiorca_sente, n.dostawca_sente,
        n.termdost_sente, m.korygujacy,
        n.uwagiwew, n.uwagizew, n.uwagisped,
        t.ktm, p.ilosc * coalesce(j.przelicz,1),
        p.cenanetto, p.cenabrutto, p.cenanettozl, p.cenabruttozl
      from int_imp_zam_pozycje p
        left join int_imp_zam_naglowki n on (n.ref = p.skadref)
        left join towary t on (t.int_id = p.towarid and t.int_zrodlo = :szrodlo)
        left join towjedn j on (j.int_id = p.jednostkaid and j.int_zrodlo = :szrodlo and j.ktm = t.ktm)
        left join defdokum m on (m.symbol = n.typdokmag_sente)
      where (p.sesja = :sesja or :sesja is null)
        and ((:ref is not null and p.ref = :ref and :tabela is null) or
             (:ref is not null and :tabela is not null and p.skadref = :ref and p.skadtabela = :tabela) or
             (:ref is null))
      order by p.ref
    into :nagid, :pozid, :symbol, :data, :bn, :company,
      :magazyn, :mag2,
      :typdokmag, :typdokvat, :sposzap, :sposdost,
      :waluta, :kurs, :sprzedawca,
      :klient, :klienthist, :odbiorca, :dostawca,
      :termdost, :typdokmagkor,
      :uwagiwewn, :uwagi, :uwagisped,
      :ktm, :ilosc,
      :cenanet, :cenabru, :cenanetzl, :cenabruzl
  do begin
    sumilosc = null;
    error_row = 0;
    errormsg_row = '';

    select sum(case when d.akcept = 1 then p.iloscl else ilosc end)
      from dokumpoz p
        left join dokumnag d on d.ref = p.dokument
      where p.int_zrodlo = :szrodlo and p.int_id = :pozid and d.typ = :typdokmag and d.magazyn = :magazyn and p.ktm = :ktm
      into sumilosc;

    if (sumilosc is null) then sumilosc = 0;
    ilosc = ilosc - sumilosc;
    -- sprawdzamy czy trzeba dolozyc pozycje czy skorygować jesli jest > 0 to dokladam a jak mniej do ujmuje
    if (ilosc > 0) then
    begin
      if (refdoc is null) then
      begin
        select first 1 d.grupasped from dokumnag d where d.int_zrodlo = :szrodlo and d.int_id = :nagid and d.magazyn = :magazyn
        into :grupasped;

        insert into dokumnag(magazyn, mag2, symbol, typ, typdokvat, data, dostawa,
            dostawca, klient, odbiorcaid, int_klienthist, akcept,
            sposdost, sposzap, termdost,
            uwagi, uwagiwewn, uwagisped, grupasped,
            walutowy, kurs, waluta,
            zrodlo,
            int_zrodlo, int_id, int_sesjaostprzetw, int_dataostprzetw, int_symbol)
          values (:magazyn, :mag2, null, :typdokmag, :typdokvat, :data, :dostawa,
            :dostawca, :klient, :odbiorca, :klienthist, 0,
            :sposdost, :sposzap, :termdost,
            :uwagi, :uwagiwewn, :uwagisped, :grupasped,
            case when :waluta = 'PLN' then 0 else 1 end, :kurs, :waluta,
            0,
            :szrodlo, :nagid, :sesja, current_timestamp, :symbol)
          returning ref into :refdoc;

          if (grupasped is null) then grupasped = refdoc;
      end
      insert into dokumpoz (dokument, ktm, wersja, ilosc, opk,
          int_zrodlo, int_id, int_sesjaostprzetw, int_dataostprzetw)
        values (:refdoc, :ktm, 0, :ilosc, 0,
            :szrodlo, :pozid, :sesja, current_timestamp);
    end
    else if (ilosc < 0) then
    begin
      ilosc = -ilosc;
      stop = 0;
      while (ilosc > 0 and stop = 0)
      do begin
        refkordoc = null;
        refkorpoz = null;
        korpozil = null;
        kordoc = null;
        select first 1 d.ref, p.ref, p.iloscl
          from dokumnag d
            left join dokumpoz p on p.dokument = d.ref and p.ktm = :ktm
          where d.int_zrodlo = :szrodlo and d.int_id = :nagid and d.akcept = 1 and d.typ = :typdokmag and d.magazyn = :magazyn
            and p.iloscl > 0 and p.ktm = :ktm
          order by d.ref, d.int_dataostprzetw
          into refkordoc, refkorpoz, korpozil;
        if (refkordoc is null and stop = 0) then
          stop = 1;
        if (stop = 0) then
        begin
          kordoc = null;
          select first 1 d.ref
            from dokumnag d where d.refk = :refkordoc and d.akcept = 0 and d.typ = :typdokmagkor and d.magazyn = :magazyn
            into kordoc;
          if (kordoc is null) then
          begin
            select first 1 d.grupasped from dokumnag d where d.int_zrodlo = :szrodlo and d.int_id = :nagid and d.magazyn = :magazyn
              into grupasped;
            insert into DOKUMNAG(REFK, MAGAZYN, TYP, DATA, DOSTAWA, DOSTAWCA, KLIENT, ODBIORCA, AKCEPT,
                  BLOKADA, MAG2, SPOSDOST, SPOSZAP, TERMDOST, UWAGI, UWAGIWEWN, UWAGISPED, GRUPASPED,
                  int_zrodlo, int_id, INT_SESJAOSTPRZETW, INT_DATAOSTPRZETW, INT_symbol)
              values (:refkordoc, :magazyn, :typdokmagkor, current_date, :dostawa,:dostawca,:klient, :odbiorca,0,
                   0, :mag2, :sposdost, :sposzap, :termdost, :uwagi, :uwagiwewn, :uwagisped, :grupasped,
                   :szrodlo, :nagid, :sesja, current_timestamp, :symbol)
              returning ref into :kordoc;
          end
          korilosc = 0;
          if (korpozil > ilosc) then
            korilosc = ilosc;
          else
            korilosc = korpozil;
          insert into dokumpoz (dokument, kortopoz, ktm, wersja, ilosc, opk,
              int_zrodlo, int_id, INT_SESJAOSTPRZETW, INT_DATAOSTPRZETW)
            values (:kordoc, :refkorpoz, :ktm, 0, :korilosc, 0,
                :szrodlo, :pozid, :sesja, current_timestamp);
          ilosc = ilosc - coalescE(korilosc,0);
        end
      end
    end
  end

  --XXX JO: aktualizacja
  if (coalesce(trim(:tabela), '') <> '' and coalesce(:ref,0) > 0 and :refdoc is not null) then
  begin
    sql = 'update '|| :tabela ||' set dokument = '|| :refdoc ||
      ', klienthist_sente = '|| coalesce(klienthist, 'null') ||
      ' where ref = '|| :ref;
    execute statement sql;
  end
  --XXX JO: Koniec

  -- akceptacja dokumentów
  if (grupasped is not null) then
  begin
    -- [DG] XXX - na razie akcept na 8 - bo nie mamy stanów !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    --update dokumnag d set d.akcept = 8 where d.grupasped = :grupasped and d.akcept = 0;
  end

  -- teraz korygujemy wszystko co jest już zaciagniete ale nalezy skasowac
  nagid = null;
  pozid = null;
  magazyn = null;
  typdokmag = null;
  ktm = null;
  ilosc = null;
  grupasped = null;
  select first 1 n.nagid
    from int_imp_zam_naglowki n
    where n.ref = :ref
    into nagid;
  if (nagid is not null) then
  begin
    for
      select d.int_id, p.int_id, d.magazyn, d.typ, p.ktm, sum(p.iloscl)
        from dokumnag d
          left join dokumpoz p on p.dokument = d.ref
          left join defdokum f on f.symbol = d.typ
        where d.int_zrodlo = :szrodlo and d.int_id = :nagid and p.iloscl > 0 and f.koryg = 0
        group by d.int_id, p.int_id, d.magazyn, d.typ, p.ktm
        into nagid, pozid, magazyn, typdokmag, ktm, ilosc 
    do begin
      sumilosc = 0;
      select sum(p.ilosc * coalesce(j.przelicz,1))
        from int_imp_zam_pozycje p
          left join int_imp_zam_naglowki n on n.ref = p.skadref
          left join towary t on t.int_id = p.towarid and t.int_zrodlo = :szrodlo
          left join towjedn j on j.int_id = p.jednostkaid and j.int_zrodlo = :szrodlo and j.ktm = t.ktm
        where (p.sesja = :sesja or :sesja is null)
          and ((:ref is not null and p.ref = :ref and :tabela is null)
          or (:ref is not null and :tabela is not null and p.skadref = :ref and p.skadtabela = :tabela)
          or (:ref is null))
          and t.ktm = :ktm and p.nagid = :nagid and p.pozid = :pozid
          and case when p.magazyn is not null and p.magazyn <> '' then p.magazyn else n.magazyn_sente end = :magazyn
          and n.typdokmag_sente = :typdokmag
        into sumilosc;
      if (sumilosc is null) then sumilosc = 0;
      if (sumilosc < ilosc) then
      begin
        ilosc = ilosc - sumilosc;
        stop = 0;
        while (ilosc > 0 and stop = 0)
        do begin
          refkordoc = null;
          refkorpoz = null;
          korpozil = null;
          kordoc = null;
          select first 1 d.ref, p.ref, p.iloscl
            from dokumnag d
              left join dokumpoz p on p.dokument = d.ref and p.ktm = :ktm
            where d.int_zrodlo = :szrodlo and d.int_id = :nagid and d.akcept = 1 and d.typ = :typdokmag and d.magazyn = :magazyn
              and p.iloscl > 0 and p.ktm = :ktm
            order by d.ref, d.int_dataostprzetw
            into refkordoc, refkorpoz, korpozil;
          if (refkordoc is null and stop = 0) then
            stop = 1;
          if (stop = 0) then
          begin
            kordoc = null;
            select first 1 d.ref
              from dokumnag d where d.refk = :refkordoc and d.akcept = 0 and d.typ = :typdokmagkor and d.magazyn = :magazyn
              into kordoc;
            if (kordoc is null) then
            begin
              select first 1 d.grupasped from dokumnag d where d.int_zrodlo = :szrodlo and d.int_id = :nagid and d.magazyn = :magazyn
                into grupasped;
              insert into DOKUMNAG(REFK, MAGAZYN, TYP, DATA, DOSTAWA, DOSTAWCA, KLIENT, ODBIORCA, AKCEPT,
                    BLOKADA, MAG2, SPOSDOST, SPOSZAP, TERMDOST, UWAGI, UWAGIWEWN, UWAGISPED, GRUPASPED,
                    int_zrodlo, int_id, INT_SESJAOSTPRZETW, INT_DATAOSTPRZETW, INT_symbol,
                    operator) --XXX JO COTO do zmiany
                values (:refkordoc, :magazyn, :typdokmagkor, current_date, :dostawa,:dostawca,:klient, :odbiorca,0,
                     0, :mag2, :sposdost, :sposzap, :termdost, :uwagi, :uwagiwewn, :uwagisped, :grupasped,
                     :szrodlo, :nagid, :sesja, current_timestamp, :symbol,
                     74) --do zmiany
                returning ref into :kordoc;
            end
            korilosc = 0;
            if (korpozil > ilosc) then
              korilosc = ilosc;
            else
              korilosc = korpozil;
            insert into dokumpoz (dokument, kortopoz, ktm, wersja, ilosc, opk,
                int_zrodlo, int_id, INT_SESJAOSTPRZETW, INT_DATAOSTPRZETW)
              values (:kordoc, :refkorpoz, :ktm, 0, :korilosc, 0,
                  :szrodlo, :pozid, :sesja, current_timestamp);
            ilosc = ilosc - coalescE(korilosc,0);
          end
        end
      end
    end
    -- akceptacja dokumentów
    if (grupasped is not null) then
    begin
      execute procedure set_global_param ('AKTUOPERATOR',74);
      --update dokumnag d set d.akcept = 1 where d.grupasped = :grupasped and d.akcept = 0;
    end
  end

  --aktualizacja informacji o sesji
  if (aktualizujsesje = 1) then
  begin
    select status, msg from int_sesje_aktualizuj(:sesja, 1) into :tmpstatus, :tmpmsg;
  end
  suspend;
end^
SET TERM ; ^
