--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KLIENT_CHECKLIMIT(
      KLIENT integer,
      PRCHECK smallint)
  returns (
      LIMITKR numeric(14,2),
      LIMITUSED numeric(14,2),
      LIMITTOUSE numeric(14,2),
      OVERLIMIT smallint)
   as
DECLARE VARIABLE SLODEF INTEGER;
DECLARE VARIABLE LIMITRYGOR VARCHAR(20);
begin
  overlimit = 0;
  limitkr = 0;
  limittouse  = 0;
  overlimit = 0;
  execute procedure getconfig('KONTRAHLIMITRYGOR') returning_values :limitrygor;
  if(:limitrygor is null) then limitrygor = '0';
  select KLIENCI.limitkr from KLIENCI where REF=:klient into :limitkr;
  if(:limitkr is null) then limitkr = 0;
  if((:limitkr > 0) or (:limitrygor = '1')) then begin
    select REF from SLODEF where SLODEF.typ='KLIENCI' into :slodef;
    select sum(WINIENZL - MAZL) from ROZRACH where SLODEF = :slodef and slopoz = :KLIENT into :limitused;
    if(:limitused is null) then limitused = 0;
    if(:limitused < 0) then exit;
    if(:limitused > :limitkr) then begin
      overlimit = 2;
      limittouse = 0;
      exit;
    end else begin
        limittouse = :limitkr - :limitused;
        if((:limitkr * :prcheck / 100) > :limittouse) then
          overlimit = 1;
    end
  end else begin
    limitkr = 0;
    limitused = 0;
    limittouse = 0;
  end

end^
SET TERM ; ^
