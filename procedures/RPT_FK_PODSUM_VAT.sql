--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_PODSUM_VAT(
      PERIODID varchar(6) CHARACTER SET UTF8                           ,
      BKPERIOD varchar(6) CHARACTER SET UTF8                           ,
      VREG varchar(10) CHARACTER SET UTF8                           ,
      ZAKRES smallint,
      ZPOPMIES smallint,
      TYP smallint,
      COMPANY integer,
      ZAKUP integer)
  returns (
      VATREG varchar(10) CHARACTER SET UTF8                           ,
      WARTBRU numeric(14,2),
      WARTNET numeric(14,2),
      WARTVAT numeric(14,2),
      WARTVATDEB numeric(14,2),
      WARTPOZ numeric(14,2),
      VATPOZ numeric(14,2),
      VATPOZDEB numeric(14,2),
      VATGR varchar(5) CHARACTER SET UTF8                           ,
      REGNAME varchar(60) CHARACTER SET UTF8                           ,
      SUMWARTBRU numeric(14,2),
      SUMWARTNET numeric(14,2),
      SUMWARTVAT numeric(14,2),
      SUMWARTVATDEB numeric(14,2),
      TAXGR varchar(10) CHARACTER SET UTF8                           ,
      DEBITED smallint)
   as
declare variable netv numeric(14,2);
declare variable vatv numeric(14,2);
declare variable VATDEB numeric(14,2);
declare variable docref integer;
declare variable sql varchar(5048);
begin
  if (typ = -1) then
    typ = 0; -- jak ktos wykasuje wpis w combo
  if(:zakres is null) then zakres = 0;
  if(:zpopmies is null) then zpopmies = 0;
  SQL = 'select v.name, VATGR, sum(NETV), sum(VATE), min(B.vatreg), sum(VATV), BVP.taxgr';
  SQL = SQL ||' from BKVATPOS BVP join bkdocs B on BVP.bkdoc = B.ref ';
  if(:bkperiod <> '') then sql = sql || 'and B.period='''||:bkperiod||''' ';
  SQL = SQL ||' join VATREGS V on (B.vatreg = V.symbol and B.company = V.company)';
  SQL = SQL ||' join grvat G on G.symbol = BVP.taxgr ';
  sql = sql ||' where ';
  if(:periodid <> '') then sql = sql ||'B.vatperiod = '''||:periodid||''' and ';
  sql = sql ||'b.status > 0';
  --<<PR73894: PL
    if(coalesce(zakup,0)>0)then
    begin
      sql = sql ||'  and (v.vatregtype in (1,2)
    or (V.VATREGTYPE = 3
         and exists(select first 1 1 from bkvatpos join grvat on (grvat.symbol = bkvatpos.taxgr) where bkvatpos.bkdoc = B.ref and grvat.vatregtype in (1,2))))';
    end
    else
    begin
      sql = sql ||'  and (v.vatregtype in (0,2)
    or (V.VATREGTYPE = 3
         and exists(select first 1 1 from bkvatpos join grvat on (grvat.symbol = bkvatpos.taxgr) where bkvatpos.bkdoc = B.ref and grvat.vatregtype in (0,2))))';
    end
    -->>
  if(coalesce(zakres,0)=1)then sql = sql ||' and t.creditnote = 0';
  else if(coalesce(zakres,0)=2)then sql = sql ||' and t.creditnote = 1';
  if(coalesce(zpopmies,0)>0)then sql = sql ||' and (b.vatperiod <> b.period)';
  if(coalesce(vreg,'')<>'')then sql = sql ||' and b.vatreg = '''||:vreg||'''';
  SQL = SQL ||' and B.company ='||:COMPANY||'group by v.name, B.vatreg, VATGR, BVP.taxgr';-- order by B.vatreg, b.vnumber';
  for execute statement :sql
    INTO :REGNAME, :VATGR, :NETV, :VATV, :VATREG, :VATDEB, :TAXGR
  do begin
    wartbru = 0;
    wartnet = 0;
    wartvat = 0;
    WARTVATDEB = 0;
    WARTPOZ = 0;
    VATPOZ = 0;
    VATPOZDEB = 0;
    WARTPOZ = :wartPOZ + :netv;
    VATPOZ = :vatPOZ + vatv;
    VATPOZDEB = :VATPOZDEB + VATDEB;
    wartvat = :vatpoz;
    WARTVATDEB = :VATPOZDEB;
    wartbru = :wartvat + :wartpoz;
    wartnet = :wartpoz;
    IF(:VATGR NOT IN ('ZW', 'NP')) THEN  VATGR = :VATGR || '%';
    SUMWARTBRU = wartbru;
    SUMWARTNET = wartnet;
    SUMWARTVAT = wartvat;
    SUMWARTVATDEB = WARTVATDEB;
    suspend;
  end
end^
SET TERM ; ^
