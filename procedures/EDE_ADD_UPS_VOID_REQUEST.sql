--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_ADD_UPS_VOID_REQUEST(
      LISTA integer)
  returns (
      REFDOK integer)
   as
declare variable ederule integer;
begin
  select es.ref
    from ederules es
    where es.symbol = 'EDE_UPS_VOID'
    into : ederule;

  insert into ededocsh( ederule, direction, createdate, otable, oref, status, filename, manualinit)
    values(:ederule, 1, current_timestamp,'LISTYWYSD', :lista,0,:lista||'exp',0)
    returning ref into :refdok;

  suspend;
end^
SET TERM ; ^
