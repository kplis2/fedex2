--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NUMERICIS(
      PHRASE varchar(255) CHARACTER SET UTF8                           )
  returns (
      RET smallint,
      RESULT numeric(16,6))
   as
declare variable phrasecur varchar(255);
begin
  ret = 1;
  result = -1;
  phrase = replace(phrase,',','.');
  phrasecur = phrase;
  if(substring(phrasecur from 1 for 1) = '-' or substring(phrasecur from 1 for 1) = '+') then phrasecur = substring(phrasecur from 2 for coalesce(char_length(phrasecur),0)); -- [DG] XXX ZG119346
  if(phrasecur = '' or phrasecur like '.%' or phrasecur like '%.' or phrasecur like '%.%.%') then ret = 0;
  while (coalesce(char_length(phrasecur),0)>0 and ret = 1) -- [DG] XXX ZG119346
  do begin
    if(position(substring(phrasecur from 1 for 1) in '0123456789.') > 0) then phrasecur = substring(phrasecur from 2 for coalesce(char_length(phrasecur) ,0)); -- [DG] XXX ZG119346
    else ret = 0;
  end
  if(ret = 1) then result = cast(phrase as numeric(16,6));
  suspend;
end^
SET TERM ; ^
