--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_Z17(
      EMPLOYEE integer,
      FROMPERIOD varchar(6) CHARACTER SET UTF8                           ,
      TOPERIOD varchar(6) CHARACTER SET UTF8                           )
  returns (
      ECOLUMN integer,
      NAME varchar(25) CHARACTER SET UTF8                           ,
      FROMDATE timestamp,
      TODATE timestamp,
      DAYS integer,
      FBASE varchar(10) CHARACTER SET UTF8                           ,
      LBASE varchar(10) CHARACTER SET UTF8                           ,
      DAYPAYAMOUNT numeric(14,2),
      MONTHPAYBASE numeric(14,2),
      WORKPAY numeric(14,2),
      ZUSPAY numeric(14,2),
      PAYDAY timestamp,
      NUMBER integer,
      PVAL varchar(255) CHARACTER SET UTF8                           ,
      CORRECT_INFO varchar(10) CHARACTER SET UTF8                           ,
      ISZUS smallint,
      SICKBDAYS integer)
   as
declare variable CORRECTED integer;
declare variable CORRECTION smallint;
declare variable T_DAYS integer;
declare variable T_WORKPAY numeric(14,2);
declare variable T_ZUSPAY numeric(14,2);
declare variable T_PAYDAY timestamp;
declare variable T_NUMBER integer;
declare variable T_ISZUS smallint;
declare variable REF integer;
begin
--MWr Personel: Procedura generuje dane do wydruku 'Karta zasilkowa'

  for
    select a.ref, a.correction , a.ecolumn, ec.name , a.fromdate, a.todate, a.days, a.monthpaybase,
      substring(a.fbase from 1 for 4) || '.' || substring(a.fbase from 5 for 6),
      substring(a.lbase from 1 for 4) || '.' || substring(a.lbase from 5 for 6),
      a.daypayamount, ep.payday,  ep.number, cp.pval, a.corrected,
      case when a.ecolumn in (90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450) then 1 else 0 end,
      case when a.ecolumn in (90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450) then a.payamount else null end,
      case when a.ecolumn in (90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450) then null else a.payamount end
    from eabsences a
      join ecolumns ec on (a.ecolumn = ec.number)
      join ecolparams cp on (a.ecolumn = cp.ecolumn and cp.param = 'BASEPR')
      left join epayrolls ep on (ep.ref = a.epayroll)
    where a.employee = :employee
       and a.ecolumn in (40,50,60,90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450)
       and ep.cper >= :fromperiod and ep.cper <= :toperiod
    order by a.fromdate, a.correction, case when a.corrected is null then 0 else 1 end
    into :ref, :correction, :ecolumn, :name, :fromdate, :todate, :days, :monthpaybase,
         :fbase, :lbase, :daypayamount, :payday, :number, :pval, :corrected, :iszus, :zuspay, :workpay
  do begin
    correct_info = '';

    if (pval > '') then begin
    --wylaczenie symbolu '%' z opisu nieobecnosci
      if (position(name in  ' %') > 0) then name = replace(name,  pval || ' %',  '');
      else name = replace(name,  pval || '%',  '');
      name = trim(name);
      pval = pval || '%';
    end

    select sickbenefitdays from e_get_sickbenefitdays(:ref,';40;50;60;90;100;110;120;130;140;150;160;170;270;280;290;350;360;370;440;450;',1,1)
      into :sickbdays;  --BS44480

    if (correction = 1) then
    begin
      if (corrected is not null) then
      begin
        select days, workpay, zuspay, iszus, correct_info
          from e_get_diffval_after_eabscorrect(:corrected, null, :days, :iszus, :workpay, :zuspay)
          into :days, :workpay, :zuspay, :iszus, :correct_info;
      end else
      begin
        select ref from eabsences
          where corrected = :ref
          into :corrected;
  
        t_iszus = null;
        select r.payday, r.number,
            case when a.ecolumn in (90,100,110,120,130,140,150,160,170,270,280,290,350,360,370,440,450) then 1 else 0 end
          from eabsences a
            left join epayrolls r on (r.ref = a.epayroll)
          where a.ref = :corrected
          into :t_payday, :t_number, :t_iszus;
  
        if (iszus <> :t_iszus) then
        begin
          correct_info = 'K-';
          days = -days;
          workpay = -workpay;
          zuspay = -zuspay;
          payday = t_payday;
          number = t_number;
        end else
        if (corrected is null) then
          correct_info = 'S';
      end
    end else
    if (correction = 2) then
    begin
      select days, workpay, zuspay, iszus, correct_info
        from e_get_diffval_after_eabscorrect(:corrected, null, :days, :iszus, :workpay, :zuspay)
        into :days, :workpay, :zuspay, :iszus, :correct_info;
    end

    suspend;
  end
end^
SET TERM ; ^
