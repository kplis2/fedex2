--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KOMPLET_GET_CENAMAG(
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      TYPCEN smallint)
  returns (
      CENAMAG numeric(14,4))
   as
begin
  if(:typcen = 0) then
     select DOSTCEN.cenanet from DOSTCEN where WERSJAREF = :wersjaref and GL = 1 into :cenamag;
  else if(:typcen = 1) then
     select min(DOSTCEN.cenanet) from DOSTCEN where WERSJAREF = :wersjaref into :cenamag;
  else if(:typcen = 2) then
     select max(DOSTCEN.cenanet) from DOSTCEN where WERSJAREF = :wersjaref into :cenamag;
  else if(:typcen = 3) then
     select min(STANYCEN.cena) from STANYCEN where WERSJAREF = :wersjaref into :cenamag;
  else if(:typcen = 4) then
     select max(STANYCEN.cena) from STANYCEN where WERSJAREF = :wersjaref into :cenamag;
  else if(:typcen = 5) then
     select max(STANYIL.cena) from STANYIL where WERSJAREF = :wersjaref into :cenamag;
  suspend;
end^
SET TERM ; ^
