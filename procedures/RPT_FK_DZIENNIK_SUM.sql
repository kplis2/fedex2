--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_DZIENNIK_SUM(
      COMPANY integer,
      ODOKRESU varchar(6) CHARACTER SET UTF8                           ,
      DOOKRESU varchar(6) CHARACTER SET UTF8                           )
  returns (
      LP integer,
      REJESTR varchar(10) CHARACTER SET UTF8                           ,
      NAZWA varchar(60) CHARACTER SET UTF8                           ,
      SUMDEBIT numeric(14,2),
      SUMCREDIT numeric(14,2),
      SUMPAGEDEBIT numeric(14,2),
      SUMPAGECREDIT numeric(14,2),
      SUMCREDITTOOKRES numeric(14,2),
      SUMDEBITTOOKRES numeric(14,2),
      DOPRZENIESIENIA_DEBIT numeric(14,2),
      DOPRZENIESIENIA_CREDIT numeric(14,2),
      ZPRZENIESIENIA_DEBIT numeric(14,2),
      ZPRZENIESIENIA_CREDIT numeric(14,2),
      PODSUMSTR smallint,
      STRONA smallint,
      KONIEC smallint)
   as
declare variable lini integer;
  declare variable maxlini integer;
begin
  koniec = 0;
  lp = 0;
  lini = 0;
  strona = 0;
  maxlini = 40;
  podsumstr = 0;
  sumpagedebit = 0;
  sumpagecredit = 0;
  doprzeniesienia_debit = 0;
  doprzeniesienia_credit = 0;
  zprzeniesienia_debit = 0;
  zprzeniesienia_credit = 0;

  for
    select r.symbol, r.name
      from bkregs r
       where r.company = :company
      into :rejestr, :nazwa
  do begin
    for
      select sum(b.sumdebit), sum(b.sumcredit)
        from bkdocs b
          join bkregs d on (b.bkreg = d.symbol and d.company = b.company)
        where b.company = :company and b.status = 3
          and b.period >= :odokresu
          and b.period <= :dookresu
          and b.bkreg = :rejestr
        into :sumdebit, :sumcredit
    do begin
      podsumstr = 0;
      if (:sumdebit is null) then sumdebit = 0;
      if (:sumcredit is null) then sumcredit = 0;

      sumpagedebit = sumpagedebit + :sumdebit;
      sumpagecredit = sumpagecredit + :sumcredit;

      if (lini = maxlini) then begin
        podsumstr = 1;
        zprzeniesienia_debit = doprzeniesienia_debit;
        zprzeniesienia_credit = doprzeniesienia_credit;

        doprzeniesienia_debit = :zprzeniesienia_debit + sumpagedebit;
        doprzeniesienia_credit = :zprzeniesienia_credit + sumpagecredit;

        lp = lp +1;
        lini = 1;
        sumpagedebit = 0;
        sumpagecredit = 0;
        sumpagedebit = sumpagedebit + :sumdebit;
        sumpagecredit = sumpagecredit + :sumcredit;
        strona = :strona +1;
        suspend;
      end

      if (lini < maxlini and podsumstr = 0) then begin
        doprzeniesienia_debit = :zprzeniesienia_debit + sumpagedebit;
        doprzeniesienia_credit = :zprzeniesienia_credit + sumpagecredit;

        lp = lp +1;
        lini = lini + 1;
        suspend;
      end

    end
  end

  podsumstr = 1;
  koniec = 1;

  sumdebit = 0;
  sumcredit = 0;

  doprzeniesienia_debit = :zprzeniesienia_debit + sumpagedebit;
  doprzeniesienia_credit = :zprzeniesienia_credit + sumpagecredit;



  select sum(b.sumdebit), sum(b.sumcredit)
    from bkdocs b
    where b.company = :company and b.status = 3
      and b.period > substring(:odokresu from 1 for 4)||'00'
      and b.period < :odokresu -- dla wszystkich okresów od poczatku roku
    --and b.period = (select okres from datatookres(:odokresu,0,-1,0,1)) -- dla jednego poprzedniego okresu
    into :sumdebittookres, :sumcredittookres;

  if (:sumdebittookres is null) then sumdebittookres = 0;
  if (:sumcredittookres is null) then sumcredittookres = 0;

  suspend;
end^
SET TERM ; ^
