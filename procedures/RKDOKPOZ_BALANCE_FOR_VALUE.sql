--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RKDOKPOZ_BALANCE_FOR_VALUE(
      SERIES varchar(1024) CHARACTER SET UTF8                           ,
      VAL numeric(14,2))
   as
declare variable VALD numeric(14,2);
declare variable VALDTMP numeric(14,2);
declare variable TMP numeric(14,2);
declare variable DIR char(1);
declare variable POS integer;
declare variable COUNTR integer;
declare variable DIRECTION char(1);
begin
  vald = 0;
  --szukanie kierunku operacji
  select first 1 d.pm
    from rkdoknag d
    where d.ref = (select first 1 dok from RKDOKPOZ_LIST_FROM_REFS(:series) )
  into :direction;

  --Poldiczenie na jaką kwote rozpisaly sie transakcje
  for select r.kwota, r.direction
    from RKDOKPOZ_LIST_FROM_REFS(:series) r
  into :valdtmp, :dir
  do begin
    if(dir=:direction) then
      vald = :vald + :valdtmp;
    else
      vald = :vald - :valdtmp;
  end
  --Sprawdzanie czy kwota rozpisana jest taka jaką chcielismy rozpisac
  --jak nie to korygujemy najmlodsza transakcje zgodna z kierunkiem operacji
  if(vald <> val) then
  begin
    select count(*)
      from rkdokpoz_list_from_refs(:series) rkd
      where kwota >0
    into :countr;
    if(vald < val) then
    begin
      --Jeżeli brakuje do kwoty do rozpisania
      while(:vald<val) do
        begin
          select skip (:countr - 1) rk.RKDOKPOZ_REF, rk.kwota, rk.direction
            from rkdokpoz_list_from_refs(:series) rk
            where rk.kwota >0
           order by rk.data
           into :pos, :tmp, :dir;
          --I najmlodsza ma kierunek zgodny to powieksz
          if(dir = :direction ) then
          begin
            valdtmp = (:tmp + (:val-:vald));
            vald = vald + (:val-:vald);
            update rkdokpoz set kwota = :valdtmp where ref = :pos;
          end else begin
           --Jeżeli przeciwny to sprawdzamy czy można zmniejszyć dosttecznie4
           --Jeżeli nie to usuwamy ( kwota = 0 żeby byl slad)
           if((:val-:vald) > :tmp) then
           begin
             vald = :vald + :tmp;
             update rkdokpoz set kwota = 0 where ref = :pos;
             countr = :countr - 1;
           end else begin
             --Jak transakcja wieksza od roznicy to tylko pomniejszamy
             valdtmp = :tmp - (val - :vald);
             vald = (vald+(:val-:vald));
             update rkdokpoz set kwota = :valdtmp where ref = :pos;
           end
         end
       end
    end else begin
      select skip (:countr - 1) rk.RKDOKPOZ_REF, rk.kwota, rk.direction
        from rkdokpoz_list_from_refs(:series) rk
        where rk.kwota >0
        order by rk.data
       into :pos, :tmp, :dir;
        while(:vald>val) do
        begin
          select skip (:countr - 1) rk.RKDOKPOZ_REF, rk.kwota, rk.direction
            from rkdokpoz_list_from_refs(:series) rk
            where rk.kwota >0
           order by rk.data
           into :pos, :valdtmp, :dir;
         if(dir <> :direction ) then
         begin
          --Jeżeli kierunek przeciwny to powieksz kwote o roznice zeby wyrownac
           valdtmp = (:tmp + (:vald-:val));
           vald = vald - (:vald-:val);
           update rkdokpoz set kwota = :valdtmp where ref = :pos;
        end else begin
        --Jeżeli transakcja mniejsza niz roznica
        --to usuwamy
           if((:vald-:val) > :valdtmp) then
           begin
             vald = :vald - :valdtmp;
             update rkdokpoz set kwota = 0 where ref = :pos;
             countr = :countr - 1;
           end else begin
             --Jak transakcja wieksza od roznicy to tylko pomniejszamy
             valdtmp = :valdtmp - (vald - val);
             vald = (vald-val);
             update rkdokpoz set kwota = :valdtmp where ref = :pos;
           end
        end
      end
    end
  end
end^
SET TERM ; ^
