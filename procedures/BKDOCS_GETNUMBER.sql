--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BKDOCS_GETNUMBER(
      COMPANY integer,
      BKREG varchar(10) CHARACTER SET UTF8                           ,
      PERIODID varchar(6) CHARACTER SET UTF8                           )
  returns (
      NUMBER integer)
   as
declare variable YEARNUM smallint;
declare variable NUMGAPFILLING smallint;
begin
  select yearnum, numgapfilling
    from bkregs
    where symbol = :bkreg
      and company = :company
    into :yearnum, :numgapfilling;
  --Przypadek gdy numeracja jest roczna
  if (yearnum = 1) then
  begin
    -- Jezeli numerujemy z uwzglednieniem luk
    if (numgapfilling = 1) then
    begin
      --Jesli brak numeru 1 to uzpelniamy numeracje
      if (not exists (select first 1 1
                        from bkdocs b
                        where b.company = :company
                          and b.bkreg = :bkreg
                          and substring(b.period from 1 for 4) = substring(:periodid from 1 for 4)
                          and b.number=1)
          ) then number=1;
      else
        -- Znajdujemy najmniejszy niewykorzystany numer
        select first 1 b.number + 1
          from bkdocs b
          where b.company = :company and b.bkreg = :bkreg
            and substring(b.period from 1 for 4) = substring(:periodid from 1 for 4)
            and not exists (select b1.ref from bkdocs b1
                                          where b1.company = b.company
                                            and b1.bkreg = b.bkreg
                                            and substring(b.period from 1 for 4) = substring(b1.period from 1 for 4)
                                            and b1.number = b.number + 1)
          order by b.number
          into :number;
    end else
    begin
    -- Wybieramy najwiekszy wykorzystany numer + 1
      select first 1 number + 1
        from bkdocs
        where company = :company and bkreg = :bkreg
          and substring(period from 1 for 4) = substring(:periodid from 1 for 4)
        order by company desc, bkreg desc, number desc
        into :number;
    end
  end else
  --Przypadek gdy numeracja jest inna dla każdego okresu
  begin
    if (numgapfilling = 1) then
    begin
      --Jesli brak numeru 1 to uzpelniamy numeracje
      if (not exists (select first 1 1
                        from bkdocs b
                        where b.company = :company
                          and b.bkreg = :bkreg
                          and b.period = :periodid
                          and b.number=1)
          ) then number=1;
      else
    -- Znajdujemy najmniejszy niewykorzystany numer
        select first 1 b.number + 1
          from bkdocs b
          where b.company = :company and b.bkreg = :bkreg
            and b.period = :periodid
            and not exists (select b1.ref from bkdocs b1
                                          where b1.company = b.company
                                          and b1.bkreg = b.bkreg
                                          and b1.number = b.number + 1
                                          and b1.period = b.period)
          order by b.number
        into :number;
    end else
    begin
    -- Wybieramy najwiekszy wykorzystany numer + 1
      select first 1 number + 1
        from BKDOCS
        where company = :company and bkreg = :bkreg and period = :periodid
        order by company desc, period desc, bkreg desc, number desc
        into :number;
    end
  end
  if (number is null) then number = 1;
  suspend;
end^
SET TERM ; ^
