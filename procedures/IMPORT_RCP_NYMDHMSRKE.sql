--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE IMPORT_RCP_NYMDHMSRKE(
      LINE varchar(1024) CHARACTER SET UTF8                           )
  returns (
      RET smallint)
   as
declare variable cardident integer;
  declare variable regtime timestamp;
  declare variable gate smallint;

begin
  --DS: linia ma postac NNNNNYYYYMMDDHHmmSSRKE
  ret = 1;
  cardident = substring(line from 1 for 5);
  regtime = substring(line from 6 for 9)||'-'||substring(line from 10 for 11)||'-'||substring(line from 12 for 13)||' '
    ||substring(line from 14 for 15)||':'||substring(line from 16 for 17)||':'||substring(line from 18 for 19);
  gate = substring(line from 21 for 21);
  insert into regisevents (idident, idgate, timeevent)
    values (:cardident, :gate, :regtime);
  suspend;

  when any do
  begin
    ret = 0;
    suspend;
  end
end^
SET TERM ; ^
