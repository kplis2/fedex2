--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDOPER_AMOUNTIN(
      PRSCHEDOPER integer)
   as
declare variable prschedoperfrom integer;
declare variable prschedguide integer;
declare variable reportedfactor numeric(14,4);
declare variable docfromoperrap smallint;
declare variable amountin numeric(14,4);
declare variable amountinfrom smallint;
begin
  -- sprawdzenie czy jest to pierwsza operacja
  execute procedure PRSCHEDOPER_AMOUNTIN_CALC(:prschedoper)
    returning_values(:amountin);
  update prschedopers o set o.amountin = :amountin where o.ref = :prschedoper;
end^
SET TERM ; ^
