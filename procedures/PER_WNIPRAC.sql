--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PER_WNIPRAC(
      EMPLOYEE integer,
      AREF integer,
      EPERSADDR integer,
      EPERCHANGE integer)
   as
declare variable person integer;
declare variable pesel varchar(11);
declare variable fname varchar(60);
declare variable sname varchar(20);
declare variable evidenceno varchar(10);
declare variable passportno varchar(11);
declare variable nip varchar(15);
declare variable birthdate timestamp;
begin
  select person
    from employees
    where ref = :EMPLOYEE
    into :person;

  if (epersaddr <> 0) then
  begin
    update EPERSADDR SET STATUS=2,TODATE=current_date
      where STATUS=1 and addrtype = (select addrtype from epersaddr where ref = :epersaddr) and PERSON=:person;
    UPDATE EPERSADDR SET STATUS=1,FROMDATE=current_date WHERE ref = :epersaddr;
  end
  if (eperchange <> 0) then
  begin
    select pesel, fname, sname, EVIDENCENO, PASSPORTNO, nip, birthdate
      from eperchanges
      where ref = :eperchange
      into :pesel, :fname, :sname, :EVIDENCENO, :PASSPORTNO, :nip, :birthdate;
    update persons set pesel = :pesel, FNAME = :FNAME, SNAME = :SNAME,
      EVIDENCENO = :EVIDENCENO, PASSPORTNO = :PASSPORTNO, NIP = :NIP,
      BIRTHDATE = :BIRTHDATE
    where ref = :person;
    update eperchanges set status = 1 where ref = :eperchange;
  end

  UPDATE EAPPLICATIONS SET STATUS=1 WHERE REF=:AREF;

end^
SET TERM ; ^
