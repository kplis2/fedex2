--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_STARTPAK_INSERT(
      PACKOPER OPERATOR_ID)
  returns (
      STATUS SMALLINT_ID)
   as
  declare variable czaspak timestamp_id;
begin
  status = 1;
  select first 1 xs.czas from X_STARTPAK xs into :czaspak; -- probuje pobrac czas z wpisu w x_startpak
  if (:czaspak is not null and datediff(second from :czaspak to current_timestamp(0)) > 90) then -- jezeli istnieje wpis w tej tabeli
  begin
    in autonomous transaction do
      delete from x_startpak;
  end
  insert into x_startpak(operator,czas)
    values (:packoper, current_timestamp(0));
  when any do
  begin
    status = -1;
  end
end^
SET TERM ; ^
