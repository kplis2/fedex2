--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RKDOKNAG_CHECK_RATE(
      RKSTANKAS varchar(2) CHARACTER SET UTF8                           ,
      DATADOK timestamp)
  returns (
      RATE numeric(14,4))
   as
DECLARE VARIABLE DATA TIMESTAMP;
DECLARE VARIABLE REF INTEGER;
DECLARE VARIABLE CNT INTEGER;
begin
  rate = null;
  select max(TABKURS.DATA)
  from RKSTNKAS
    join TABKURS on (rkstnkas.bank = TABKURS.bank and TABKURS.akt >0 and TABKURS.data <= :datadok)
    join TABKURSP on (TABKURSP.tabkurs = TABKURS.ref and TABKURSP.waluta = rkstnkas.waluta)
    where RKSTNKAS.kod=:rkstankas
  into :data;
  select max(TABKURS.ref)
  from RKSTNKAS
    join BANKI on (BANKI.symbol = rkstnkas.bank and RKSTNKAS.kod = :rkstankas)
    join TABKURS on (BANKI.symbol = TABKURS.bank and TABKURS.akt >0  and TABKURS.data = :data)
  into :ref;
  select TABKURSP.sredni
  from RKSTNKAS join TABKURSP on (TABKURSP.waluta = RKSTNKAS.waluta and RKSTNKAS.KOD = :rkstankas and TABKURSP.tabkurs = :ref)
  into :rate;
  if(:rate is null) then rate = 1;
end^
SET TERM ; ^
