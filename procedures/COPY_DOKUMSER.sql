--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COPY_DOKUMSER(
      REFPOZ_Z integer,
      REFPOZ_D integer,
      ILOSC_NRSER numeric(14,4),
      TYP_Z char(1) CHARACTER SET UTF8                           ,
      TYP_D char(1) CHARACTER SET UTF8                           ,
      TRYB smallint)
   as
declare variable odserial_tmp varchar(40);
declare variable doserial_tmp varchar(40);
declare variable odserialnr_tmp integer;
declare variable doserialnr_tmp integer;
declare variable ilosc_tmp numeric(14,4);
declare variable orgdokumser_tmp integer;
declare variable numer_tmp integer;
declare variable ord_tmp smallint;
declare variable ref_dokumser integer;
declare variable orgmmdokumser_tmp integer;
begin
  if(tryb = 0) then --tryb kopiowania
  begin
    for select numer, ord, odserial, doserial, odserialnr, doserialnr, ilosc, coalesce(orgdokumser, ref)
      from DOKUMSER where refpoz = :refpoz_z and TYP = :typ_z
    into :numer_tmp, :ord_tmp, :odserial_tmp, :doserial_tmp, :odserialnr_tmp, :doserialnr_tmp, :ilosc_tmp, :orgdokumser_tmp
    do begin
      if(:ilosc_nrser - :ilosc_tmp >= 0) then
        insert into DOKUMSER(refpoz, typ, numer, ord, odserial, doserial, odserialnr, doserialnr, ilosc, orgdokumser)
          values(:refpoz_d, :typ_d, :numer_tmp, :ord_tmp, :odserial_tmp, :doserial_tmp, :odserialnr_tmp, :doserialnr_tmp, :ilosc_tmp, :orgdokumser_tmp);
      else
        insert into DOKUMSER(refpoz, typ, numer, ord, odserial, doserial, odserialnr, doserialnr, ilosc, orgdokumser)
          values(:refpoz_d, :typ_d, :numer_tmp, :ord_tmp, :odserial_tmp, :doserial_tmp, :odserialnr_tmp, :doserialnr_tmp, :ilosc_nrser, :orgdokumser_tmp);
      ilosc_nrser = :ilosc_nrser - :ilosc_tmp;
      if(:ilosc_nrser <= 0) then exit;
    end
  end
  if(tryb = 1) then --tryb przepinania - realizacja
  begin
    for select ref, ilosc
      from DOKUMSER where refpoz = :refpoz_z and typ = :typ_z
    into :ref_dokumser, :ilosc_tmp
    do begin
      if(:ilosc_nrser - :ilosc_tmp >= 0) then
        update DOKUMSER set refpoz = :refpoz_d where ref = :ref_dokumser;
      else begin
        insert into DOKUMSER(refpoz, typ, numer, ord, odserial, doserial, odserialnr, doserialnr, ilosc, iloscroz, stan, orgdokumser, numbergen, gennumber, genrej, gendok, gendokref)
         select :refpoz_d, typ, numer, ord, odserial, doserial, odserialnr, doserialnr, :ilosc_nrser, iloscroz, stan, coalesce(orgdokumser, :ref_dokumser), numbergen, gennumber, genrej, gendok, gendokref
           from DOKUMSER where ref = :ref_dokumser;
        update DOKUMSER set ilosc = ilosc - :ilosc_nrser where ref = :ref_dokumser;
      end
      ilosc_nrser = :ilosc_nrser - :ilosc_tmp;
      if(:ilosc_nrser <= 0) then exit;
    end
  end
  if(tryb = 2) then --tryb przepinania - wycofanie realizacji
  begin
    for select ref, odserial, odserialnr, orgdokumser
      from dokumser where refpoz = :refpoz_z and typ = :typ_z
    into :ref_dokumser , :odserial_tmp, :odserialnr_tmp, :orgdokumser_tmp
    do begin
      if(exists (select first 1 1 from dokumser where ref = :orgdokumser_tmp)) then--refpoz = :refpoz_d and typ = :typ_d and odserial = :odserial_tmp)) then
        update dokumser set ilosc = ilosc + (select ilosc from dokumser where ref = :ref_dokumser) where refpoz = :refpoz_d and typ = :typ_d and odserial = :odserial_tmp;
      else
        update dokumser set refpoz = :refpoz_d where ref = :ref_dokumser;
    end
  end
  if(tryb = 3) then --tryb kopiowania dla MM
  begin
    for select ref, numer, ord, odserial, doserial, odserialnr, doserialnr, ilosc
      from DOKUMSER where refpoz = :refpoz_z and TYP = :typ_z
        and (ilosc - coalesce(ilosccopy,0)) >= :ilosc_nrser
    into :ref_dokumser, :numer_tmp, :ord_tmp, :odserial_tmp, :doserial_tmp, :odserialnr_tmp, :doserialnr_tmp, :ilosc_tmp
    do begin
      if(:ilosc_nrser - :ilosc_tmp >= 0) then
        insert into DOKUMSER(refpoz, typ, numer, ord, odserial, doserial, odserialnr, doserialnr, ilosc, orgmmdokumser)
          values(:refpoz_d, :typ_d, :numer_tmp, :ord_tmp, :odserial_tmp, :doserial_tmp, :odserialnr_tmp, :doserialnr_tmp, :ilosc_tmp, :ref_dokumser);
      else
        insert into DOKUMSER(refpoz, typ, numer, ord, odserial, doserial, odserialnr, doserialnr, ilosc, orgmmdokumser)
          values(:refpoz_d, :typ_d, :numer_tmp, :ord_tmp, :odserial_tmp, :doserial_tmp, :odserialnr_tmp, :doserialnr_tmp, :ilosc_nrser, :ref_dokumser);

      -- aktualizujemy ilosc skopiowana w źródlowym rekordzie dokumser
      update dokumser d
        set d.ilosccopy = (coalesce(d.ilosccopy,0) + :ilosc_tmp)
        where ref = :ref_dokumser;

      ilosc_nrser = :ilosc_nrser - :ilosc_tmp;
      if(:ilosc_nrser <= 0) then exit;
    end
  end
end^
SET TERM ; ^
