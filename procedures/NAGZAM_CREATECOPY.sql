--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGZAM_CREATECOPY(
      AKTZAM integer,
      CELZAM integer,
      REJESTR varchar(3) CHARACTER SET UTF8                           ,
      POZYCJE smallint,
      KUMULACJA smallint)
  returns (
      NOWEZAM integer,
      STATUS smallint)
   as
declare variable aktoper integer;
declare variable typzam varchar(3);
declare variable tmptypzam varchar(3);
declare variable tmprejestr varchar(3);
declare variable aktklient integer;
declare variable aktwaluta varchar(3);
declare variable celklient integer;
declare variable celwaluta varchar(3);
declare variable wersjaref integer;
declare variable magazyn varchar(3);
declare variable mag2 varchar(3);
declare variable ilosc numeric(14,4);
declare variable ilreal numeric(14,4);
declare variable ilzreal numeric(14,4);
declare variable jedn integer;
declare variable jedno integer;
declare variable opk smallint;
declare variable cenamag numeric(14,2);
declare variable cenacen numeric(14,2);
declare variable walcen varchar(3);
declare variable rabat numeric(14,2);
declare variable rabattab numeric(14,2);
declare variable cenanet numeric(14,2);
declare variable cenabru numeric(14,2);
declare variable aktpozzamref integer;
declare variable pozzamref integer;
declare variable dostawamag integer;
declare variable zamowienie nagzam_id;
declare variable ktm ktm_id;
declare variable wersja nrwersji_id;
declare variable ilosco ilosci_Zam;
declare variable paramn1 paramn;
declare variable paramn2 paramn;
declare variable paramn3 paramn;
declare variable paramn4 paramn;
declare variable paramn5 paramn;
declare variable paramn6 paramn;
declare variable paramn7 paramn;
declare variable paramn8 paramn;
declare variable paramn9 paramn;
declare variable paramn10 paramn;
declare variable paramn11 paramn;
declare variable paramn12 paramn;
declare variable params1 params;
declare variable params2 params;
declare variable params3 params;
declare variable params4 params;
declare variable params5 params;
declare variable params6 params;
declare variable params7 params;
declare variable params8 params;
declare variable params9 params;
declare variable params10 params;
declare variable params11 params;
declare variable params12 params;
declare variable paramd1 paramd;
declare variable paramd2 paramd;
declare variable paramd3 paramd;
declare variable paramd4 paramd;
declare variable prec smallint_id;
declare variable OrgPozZam pozzam_id;
declare variable AltToPoz pozzam_id;
declare variable AltToPozN pozzam_id;
declare variable fake smallint;
declare variable iloscm ilosci_mag;
declare variable HaveFake smallint;

begin
  status = 0;
  select typzam, rejestr, klient, waluta from nagzam where ref = :aktzam
    into :tmptypzam, :tmprejestr, :aktklient, :aktwaluta;
  if (:rejestr is null or :rejestr = '') then rejestr = :tmprejestr;

  select d.typzam from defrejzam d where d.symbol = :rejestr
    into :typzam;
  if (:typzam is null or typzam = '') then typzam = :tmptypzam;
  if ((:typzam is null or typzam = '') or (:rejestr is null or :rejestr = '')) then
    exception createcopy_typ;

  if (:pozycje is null) then pozycje = 0;
  if (:kumulacja is null) then kumulacja = 0;

  execute procedure get_global_param ('AKTUOPERATOR') returning_values :aktoper;

  if (coalesce(:celzam, 0) = 0) then begin
    execute procedure gen_ref('NAGZAM') returning_values :nowezam;

    insert into nagzam(ref, znakzewn, rejestr, typzam, klient, dostawca, uzykli, magazyn, mag2, datawe,
                       datazm, odbiorcaid, odbiorca, dulica, dnrdomu, dnrlokalu, dmiasto, dkodp, dpoczta,
                       sposdost, termdost, sposzap, termzap, rabat, uwagi, stan, flagi, wysylka, walutowe,
                       waluta, sprzedawca, operator, skad, ckontrakty, bn, datazewn)
      select :nowezam, znakzewn, :rejestr, :typzam, klient, dostawca, uzykli, magazyn, mag2, current_date,
          current_date, odbiorcaid, odbiorca, dulica, dnrdomu, dnrlokalu, dmiasto, dkodp, dpoczta,
          sposdost, termdost, sposzap, termzap, rabat, uwagi, 'N', flagi, wysylka, walutowe,
          waluta, sprzedawca, :aktoper, 0, ckontrakty, bn, datazewn
        from nagzam
        where ref=:aktzam;
  end
  else begin
    select klient, waluta from nagzam where ref = :celzam into :celklient, :celwaluta;
    if (:aktklient <> :celklient or :aktwaluta <> :celwaluta) then begin
      status = 0;
      nowezam = 0;
      exit;
    end
    nowezam = :celzam;
  end
  -- 0-nie kopiuj pozycji
  -- 1-kopiuj wszystkie
  -- 2-kopiuj niezrealizowane
  if(:pozycje > 0) then begin
    if(:kumulacja = 0) then begin
      for select :nowezam, magazyn, mag2, ktm, wersja, wersjaref,
               case when :pozycje = 1 then ilosc else ilosc - ilzreal end,
               case when :pozycje = 1 then iloscm else ilosc - ilzreal end,
               case when :pozycje = 1 then ilosco else ilosc - ilzreal end,
               case when :pozycje = 1 then ilreal else 0 end,
               case when :pozycje = 1 then ilzreal else 0 end,
               jedn, jedno, dostawamag, cenamag, cenacen, walcen, rabat, cenanet, cenabru, opk,
               paramn1, paramn2, paramn3, paramn4, paramn5, paramn6, paramn7, paramn8,
               paramn9, paramn10, paramn11, paramn12,
               params1, params2, params3, params4, params5, params6, params7, params8,
               params9, params10, params11, params12,
               paramd1, paramd2, paramd3, paramd4, prec, AltToPoz, fake, orgpozzam
          from pozzam
          where zamowienie = :aktzam
            and (:pozycje=1 or (:pozycje=2 and ilosc - ilzreal > 0))
          order by pozzam.alttopoz nulls first
          into :zamowienie, :magazyn, :mag2, :ktm, :wersja, :wersjaref,
               :ilosc, :iloscm, :ilosco, :ilreal, :ilzreal,
               :jedn, :jedno, :dostawamag, :cenamag, :cenacen, :walcen, :rabat, :cenanet, :cenabru, :opk,
               :paramn1, :paramn2, :paramn3, :paramn4, :paramn5, :paramn6, :paramn7, :paramn8,
               :paramn9, :paramn10, :paramn11, :paramn12,
               :params1, :params2, :params3, :params4, :params5, :params6, :params7, :params8,
               :params9, :params10, :params11, :params12,
               :paramd1, :paramd2, :paramd3, :paramd4, :prec, :AltToPoz, :fake, :OrgPozZam
      do begin
        AltToPozN = null;
        if (:AltToPoz is not null) then
        begin
          select pz.ref
            from pozzam pz
            where pz.orgpozzam = AltToPoz and zamowienie = :nowezam
            into :AltToPozN;
        end
        insert into pozzam(zamowienie, magazyn, mag2, ktm, wersja, wersjaref,
                         ilosc, iloscm, ilosco, ilreal, ilzreal,
                         jedn, jedno, dostawamag, cenamag, cenacen, walcen, rabat, cenanet, cenabru, opk,
                         paramn1, paramn2, paramn3, paramn4, paramn5, paramn6, paramn7, paramn8,
                         paramn9, paramn10, paramn11, paramn12,
                         params1, params2, params3, params4, params5, params6, params7, params8,
                         params9, params10, params11, params12,
                         paramd1, paramd2, paramd3, paramd4, prec, AltToPoz, fake)
          values(:nowezam, :magazyn, :mag2, :ktm, :wersja, :wersjaref,
                 case when :pozycje = 1 then :ilosc else :ilosc - :ilzreal end,
                 case when :pozycje = 1 then :iloscm else :ilosc - :ilzreal end,
                 case when :pozycje = 1 then :ilosco else :ilosc - :ilzreal end,
                 case when :pozycje = 1 then :ilreal else 0 end,
                 case when :pozycje = 1 then :ilzreal else 0 end,
                 :jedn, :jedno, :dostawamag, :cenamag, :cenacen, :walcen, :rabat, :cenanet, :cenabru, :opk,
                 :paramn1, :paramn2, :paramn3, :paramn4, :paramn5, :paramn6, :paramn7, :paramn8,
                 :paramn9, :paramn10, :paramn11, :paramn12,
                 :params1, :params2, :params3, :params4, :params5, :params6, :params7, :params8,
                 :params9, :params10, :params11, :params12,
                 :paramd1, :paramd2, :paramd3, :paramd4, :prec, :AltToPozN, :fake);
      end
      status = 1;
    end
    else begin
      for select ref, wersjaref, magazyn, mag2, ilosc, ilreal, ilzreal, jedn, jedno,
                 opk, dostawamag, cenamag, cenacen, walcen, rabat, rabattab, cenanet, cenabru,
                 fake, alttopoz,  orgPozzam, havefake
            from pozzam
            where zamowienie = :aktzam
            order by alttopoz nulls first
          into :aktpozzamref, :wersjaref, :magazyn, :mag2, :ilosc, :ilreal, :ilzreal, :jedn, :jedno,
               :opk, :dostawamag, :cenamag, :cenacen, :walcen, :rabat, :rabattab, :cenanet, :cenabru,
               :fake, :AltToPoz, :OrgPozZam, :HaveFake
      do begin
        AltToPozN = null;
        if (AltToPoz is null and HaveFake is null) then
        begin
          select min(ref) from pozzam
            where zamowienie=:nowezam
              and wersjaref = :wersjaref and magazyn = :magazyn
              and coalesce(mag2, '') = coalesce(:mag2, '') and cenacen=:cenacen
              and rabat=:rabat and rabattab=:rabattab and walcen=:walcen
              and jedn=:jedn and jedno=:jedno and opk=:opk
              and dostawamag=:dostawamag
          into :pozzamref;
        end else
          pozzamref = null;
        if (:pozzamref is null or :pozzamref = 0) then begin
          if (AltToPoz is not null) then
          begin
            select ref
              from pozzam
              where OrgPozZam = AltToPoz and zamowienie = :nowezam
              into :AltToPozN;
          end

          insert into pozzam(zamowienie, magazyn, mag2, ktm, wersja, wersjaref,
                             ilosc, iloscm, ilosco, ilreal, ilzreal,
                             jedn, jedno, dostawamag, cenamag, cenacen, walcen, rabat, cenanet, cenabru, opk,
                             paramn1, paramn2, paramn3, paramn4, paramn5, paramn6, paramn7, paramn8,
                             paramn9, paramn10, paramn11, paramn12,
                             params1, params2, params3, params4, params5, params6, params7, params8,
                             params9, params10, params11, params12,
                             paramd1, paramd2, paramd3, paramd4, prec,OrgPozZam, alttopoz,  fake)
            select :nowezam, magazyn, mag2, ktm, wersja, wersjaref,
                   case when :pozycje = 1 then ilosc else ilosc - ilzreal end,
                   case when :pozycje = 1 then iloscm else ilosc - ilzreal end,
                   case when :pozycje = 1 then ilosco else ilosc - ilzreal end,
                   case when :pozycje = 1 then ilreal else 0 end,
                   case when :pozycje = 1 then ilzreal else 0 end,
                   jedn, jedno, dostawamag, cenamag, cenacen, walcen, rabat, cenanet, cenabru, opk,
                   paramn1, paramn2, paramn3, paramn4, paramn5, paramn6, paramn7, paramn8,
                   paramn9, paramn10, paramn11, paramn12,
                   params1, params2, params3, params4, params5, params6, params7, params8,
                   params9, params10, params11, params12,
                   paramd1, paramd2, paramd3, paramd4, prec, ref, :AltToPozN, :fake
              from pozzam
              where ref = :aktpozzamref;
          status = 1;
        end
        else begin
          update pozzam
            set ilosc = ilosc + (case when :pozycje = 1 then :ilosc else :ilosc - :ilzreal end),
                iloscm = ilosc + (case when :pozycje = 1 then :ilosc else :ilosc - :ilzreal end),
                ilosco = ilosc + (case when :pozycje = 1 then :ilosc else :ilosc - :ilzreal end),
                ilreal = (case when :pozycje = 1 then ilreal + :ilreal else 0 end),
                ilzreal = (case when :pozycje = 1 then ilzreal + :ilzreal else 0 end)
            where ref = :pozzamref;
          status = 1;
        end
      end
    end
  end
  execute procedure oblicz_nagzam(:nowezam);
end^
SET TERM ; ^
