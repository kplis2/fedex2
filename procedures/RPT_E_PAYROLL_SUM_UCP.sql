--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_E_PAYROLL_SUM_UCP(
      BILLFROMDATE date,
      BILLTODATE date,
      PAYDAY date,
      STATUS smallint,
      COMPANY integer,
      EMPLOYEE integer = 0)
  returns (
      NUM1 integer,
      NAME1 varchar(22) CHARACTER SET UTF8                           ,
      VAL1 numeric(14,2),
      REPSUM1 integer,
      NUM2 integer,
      NAME2 varchar(22) CHARACTER SET UTF8                           ,
      VAL2 numeric(14,2),
      REPSUM2 integer,
      NUM3 integer,
      NAME3 varchar(22) CHARACTER SET UTF8                           ,
      VAL3 numeric(14,2),
      REPSUM3 integer,
      NUM4 integer,
      NAME4 varchar(22) CHARACTER SET UTF8                           ,
      VAL4 numeric(14,2),
      REPSUM4 integer,
      NUM5 integer,
      NAME5 varchar(22) CHARACTER SET UTF8                           ,
      VAL5 numeric(14,2),
      REPSUM5 integer)
   as
declare variable num2from integer;
declare variable num2to integer;
begin
--MWr: Personel - Generowanie wydruku list plac dla rachunkow UCP
--jezeli employee=0 dzialanie anlogiczne jak przy liscie plac dla: rpt_e_payroll_summary_sum

  status = coalesce(status,0);
  employee = coalesce(employee,0);
  company = coalesce(company,1);
  name1 = null;
  val1 = null;
  repsum1 = null;
  num1 = null;
  num2 = 4000;
  num3 = 5900;
  num4 = 8000;
  num5 = 9050;
 --dla UCP nie mamy brutto(400), dlatego posluzymy sie zakresem skladnikow od-do:
  num2from = 1000;
  num2to = 3999;

  select sum(case when p.ecolumn between :num2from and :num2to then p.pvalue end),
         sum(case when p.ecolumn = :num3 then p.pvalue end),
         sum(case when p.ecolumn = :num4 then p.pvalue end),
         sum(case when p.ecolumn = :num5 then p.pvalue end)
    from epayrolls r
      join eprpos p on (r.ref = p.payroll and r.empltype = 2 and r.company = :company)
    where r.billdate >= :billfromdate and r.billdate <= :billtodate
      and (r.payday = :payday or :payday is null) and status >= :status
      and (p.employee = :employee or :employee = 0)
      and (p.ecolumn in (:num3, :num4, :num5) or (p.ecolumn between :num2from and :num2to))
    into :val2, :val3, :val4, :val5;

  val2 = coalesce(val2,0);
  val3 = coalesce(val3,0);
  val4 = coalesce(val4,0);
  val5 = coalesce(val5,0);

  select name, repsum
    from ecolumns
    where number = :num2
    into :name2, :repsum2;

  select name, repsum
    from ecolumns
    where number = :num3
    into :name3, :repsum3;

  select name, repsum
    from ecolumns
    where number = :num4
    into :name4, :repsum4;

  select name, repsum
    from ecolumns
    where number = :num5
    into :name5, :repsum5;

  suspend;
end^
SET TERM ; ^
