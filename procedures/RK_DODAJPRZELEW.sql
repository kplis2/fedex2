--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RK_DODAJPRZELEW(
      RAPKAS integer,
      BTRANSFER integer,
      RKOPER varchar(10) CHARACTER SET UTF8                           ,
      RKPOPER integer,
      DATA timestamp,
      TABKURSID integer)
  returns (
      RKDOKUM integer)
   as
declare variable CNT integer;
declare variable AMOUNT numeric(14,4);
declare variable WALUTAB varchar(5);
declare variable WALUTAR varchar(5);
declare variable KURSWAL numeric(14,4);
declare variable STANBANK char(2);
declare variable STAN smallint;
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable RKDLA varchar(255);
declare variable LP smallint;
declare variable SETTLEMENT varchar(20);
declare variable KONTO2 ACCOUNT_ID;
declare variable TYP varchar(20);
declare variable RKPOPEROPPOSED integer;
begin
  RKPOPEROPPOSED = 0;
  kurswal = 1;
  rkdokum = 0;
  /*sprawdzenie, czy sa pozycje przelewu*/
  select amount, CURR, btype
    from BTRANSFERS  where ref = :btransfer
    into :amount, :walutab, :typ;
  select rkpozoperopposed
    FROM btrantype
    WHERE symbol = :TYP
    INTO :RKPOPEROPPOSED;
  select STANOWISKO , STATUS from RKRAPKAS where REF=:RAPKAS into :stanbank, :stan;
  select s.konto
    from rkstnkas s
    where s.kod = :stanbank
    into :konto2;
  if(amount is null or (amount = 0))then exit;
  select count(*) from btransferpos where BTRANSFER = :btransfer into :cnt;
  if(:stan <> 0) then exception
    RK_DODAJPRZELEW_RAPZAMKNIETY;
  select SLODEF, SLOPOZ from BTRANSFERS where REF=:btransfer into :slodef, :slopoz;
  execute procedure NAME_FROM_DICTPOSREF(:slodef, :slopoz) returning_values :rkdla;
  execute procedure GEN_REF('RKDOKNAG') returning_values :rkdokum;

  insert into RKDOKNAG(REF, RAPORT, DATA, OPERACJA, SLODEF, SLOPOZ, OPIS,
     KURS, DLA, BTRANSFER)
  select :rkdokum,:rapkas, :data, :rkoper, :slodef, :slopoz, substring(BTRANSFERs.todescript from 1 for 40),
    :kurswal,:rkdla, REF
  from BTRANSFERS where ref = :btransfer;
  /*dodawani epozycji*/
  if(:cnt > 0) then begin
   lp = 0;
    for select SETTLEMENT, AMOUNT
    from  BTRANSFERPOS where BTRANSFER = :btransfer and amount <> 0
    order by amount desc
    into :settlement, :amount
    do begin
       lp = :lp + 1;
       if (amount > 0) then begin
         insert into RKDOKPOZ(DOKUMENT, LP, ROZRACHUNEK, KWOTA, POZOPER, kwotazl, konto2)
         values (:rkdokum, :lp, :settlement, :amount, :rkpoper, :amount*:kurswal, :konto2);
       end
       else begin
         AMOUNT = - AMOUNT;
         insert into RKDOKPOZ(DOKUMENT, LP, ROZRACHUNEK, KWOTA, POZOPER, kwotazl, konto2)
         values (:rkdokum, :lp, :settlement, :amount, :RKPOPEROPPOSED, :amount*:kurswal, :konto2);
       end
    end
  end else begin
    select substring(replace(todescript, 'f-ra:', '') from 1 for 20)
      from  btransfers where ref = :btransfer
    into :settlement;
    insert into RKDOKPOZ(DOKUMENT, LP, ROZRACHUNEK, KWOTA, POZOPER, kwotazl, konto2)
    values (:rkdokum, 1, :settlement, :amount, :rkpoper, :amount*:kurswal, :konto2);
  end
  update BTRANSFERS set BTRANSFERS.status = 3 where ref = :btransfer;
end^
SET TERM ; ^
