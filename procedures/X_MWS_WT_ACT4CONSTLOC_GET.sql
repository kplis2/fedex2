--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_WT_ACT4CONSTLOC_GET(
      MWSACTREF integer)
  returns (
      REF integer,
      LEFTTOREALIZE varchar(20) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      WNAZWA varchar(255) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4),
      BUFOR smallint)
   as
declare variable mwsconslocp integer;
declare variable mwsord integer;
declare variable vers integer;
begin
  bufor = 0;
  ref = 0;
  for
    select towar, wersja, ilosc, iloscp, good, vers
      from X_MWS_WT_ACT4CONSTLOC(:mwsactref)
    into :nazwa, :wnazwa, :lefttorealize, :quantity, :ktm, :ref
  do begin
    suspend;
  end
end^
SET TERM ; ^
