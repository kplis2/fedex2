--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_PROD_POW_ZAMCANREAL
  returns (
      TRESC varchar(2048) CHARACTER SET UTF8                           ,
      MEDIUM varchar(20) CHARACTER SET UTF8                           ,
      ADRES varchar(255) CHARACTER SET UTF8                           ,
      ZAMREF integer)
   as
begin
  update pozzam set canreal = 0;
  update pozzam set canreal = 1 where ref in
   (select pozzam from nagzam_check_can_real_predict('2005-12-12'));
  update nagzam n set n.canreal = (case when (exists(select p.ref from pozzam p
    where p.zamowienie = n.ref and p.canreal = 1)) then 1 else 0 end);
  tresc = 'Wykonywalność zamówień oznaczona.';
  suspend;
end^
SET TERM ; ^
