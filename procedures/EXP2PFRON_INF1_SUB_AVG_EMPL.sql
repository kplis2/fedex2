--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXP2PFRON_INF1_SUB_AVG_EMPL(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      COMPANY integer,
      INVALIDITY integer,
      TYP smallint)
  returns (
      AVG_EMPLOYMENT_RETURN numeric(14,2))
   as
declare variable perioddate date;
declare variable noofdays integer;
declare variable tmp date;
declare variable curr_date date;
declare variable curr_day integer;
declare variable sum_employment numeric(14,2);
declare variable workdim numeric(14,2);
declare variable person numeric(14,2);
begin
  /*
    typ=0 - przecietne zatrudnienie w ETATACH
    typ=1 - przecietne zatrudnienie w OSOBACH

    dla invalidity = 0 nie bedzie dzialalo poprawnie
    jezeli invalidityfrom lub invalidityto jest ustawione
    ale na szczescie dla 0 nie bedziemy funkcji uzywac
  */
  perioddate = substring(:period from 1 for 4)||'-'||substring(:period from 5 for 6)||'-01';
  tmp = perioddate - EXTRACT(DAY FROM perioddate) + 32;
  noofdays  = EXTRACT(DAY FROM (tmp - EXTRACT(DAY FROM tmp)));
  curr_day = 0;
  sum_employment = 0;
  while (curr_day < noofdays) do
  begin
    curr_date = :perioddate + :curr_day;
    select sum(EM.workdim), count(e.person)
      from employees e
        join employment EM on (em.employee = e.ref)
      where e.company = :company
        and EM.fromdate <= :curr_date
        and (EM.todate >= :curr_date or EM.todate is null)
        and (:invalidity is null or e.person in (select z.person
                          from ezusdata z
                            join edictzuscodes c on (z.invalidity = c.ref)
                          where c.dicttype = 2
                            and c.code = :invalidity
                            and (z.invalidityfrom <= :curr_date or z.invalidityfrom is null)
                            and (:curr_date <= z.invalidityto or z.invalidityto is null)))
        and not exists (select first 1 1 from eabsences A
                          where A.employee = E.ref
                            and A.fromdate <= :curr_date and :curr_date <= A.todate
                            and A.ecolumn in (260, 300, 350, 360, 370, 390))
      into :workdim, :person;
    if (typ = 0) then
      avg_employment_return = :workdim;
    else if (typ = 1) then
      avg_employment_return = :person;
    sum_employment = :sum_employment + :avg_employment_return;
    curr_day = curr_day + 1;
  end
  avg_employment_return = coalesce(:sum_employment / :noofdays,0);
  suspend;
end^
SET TERM ; ^
