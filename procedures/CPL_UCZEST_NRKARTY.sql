--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CPL_UCZEST_NRKARTY(
      CPLUCZESTREF integer)
   as
declare variable symbolekart varchar(100);
declare variable symbolkarty varchar(40);
begin
  symbolekart = '';
  for select SYMBOL from CPLUCZESTKART where CPLUCZESTID = :CPLUCZESTREF
  into :symbolkarty
  do begin
    if(:symbolekart <> '') then symbolekart = :symbolekart || ';';
    symbolekart = :symbolekart || :symbolkarty;
    symbolekart = substring(:symbolekart from 1 for 40);
  end
  update CPLUCZEST set cpluczest.nrkarty = :symbolekart
    where ref=:cpluczestref and (nrkarty is null or nrkarty <> :symbolekart);
end^
SET TERM ; ^
