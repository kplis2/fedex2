--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_MWSORD_ZT(
      ZZ_REF INTEGER_ID)
   as
declare variable PERIOD varchar(6);
declare variable WH varchar(3);
declare variable frommwsact integer_id;
declare variable branch string10;
declare variable ref_mwsact integer_id;
declare variable realtime double precision;
declare variable newmwsord integer_id;
declare variable wharea integer_id;
declare variable ilosc numeric_14_4;
declare variable GOOD ktm_id;
declare variable VERS integer_id;
declare variable mwsconstloclbl integer_id;
declare variable mwspalloclbl integer_id;
declare variable WHAREAL integer_id;
declare variable refill smallint_id;
declare variable wharealogl integer_id;
begin
  -- generujemy zlecenie ZT do dokumentu ZZ
  -- naglowek ZT
  wh = 'MWS';
  execute procedure gen_ref('MWSORDS') returning_values newmwsord;
  select oddzial from defmagaz where symbol = 'MWS' into :branch;
  select okres from datatookres(current_date,0,0,0,0) into period;
  insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator, priority,
        description, wh,  regtime, timestartdecl, branch, period,
        flags, docsymbs, cor, rec, status, shippingarea, SHIPPINGTYPE, takefullpal,
        slodef, slopoz, slokod, palquantity, repal)
  values(:newmwsord, 25, 0, 'O', null, null, null, 1,
          '', :wh, current_timestamp(0), null, :branch, :period,
          '', '', 0, 0, 1, null, null, 0,
          null, null, 'Auto ZT', null, 0);
  -- pozycje ZT na podstawie pozycji mwsacts, z ktorych powstal dokumpoz
  for
    select dp.frommwsact, dp.ilosc, dp.ktm, dp.wersjaref
     from dokumpoz dp
    where dp.dokument = :zz_ref
    into :frommwsact, :ilosc, :good, :vers
  do begin
    if (frommwsact is null) then
      exception universal 'Brak numeru zlecenia.';
    execute procedure gen_ref('MWSACTS') returning_values :ref_mwsact;
    -- szukanie najlepszej lokacji
    execute procedure XK_MWS_GET_BEST_LOCATION(:good,:vers,null,null,25,
    :wh,null,null,null,null,null,null,null,null,null,null,null,null,null)
    returning_values(mwsconstloclbl, mwspalloclbl, whareal, wharealogl, refill);

    insert into mwsacts (ref, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp, mwsconstlocl, mwspallocl, doctype, settlmode,
              closepalloc, wh, wharea, whsec, regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, recdoc, plus, whareap, whareal, wharealogp,
              wharealogl, number, disttogo, mixedpallgroup, palgroup, mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, refilltry, x_partia, x_serial_no, x_slownik,
              lot, frommwsact)
    select :ref_mwsact, :newmwsord, 0, 0, good, vers, :ilosc, mwsconstlocl, mwspallocl, :mwsconstloclbl, :mwspalloclbl, 'O', 0,
              0, wh, :wharea, null, current_timestamp(0), current_timestamp(0), current_timestamp(0), :realtime, null, null, 1, 0, 1, null, :whareal, null,
              :wharealogl, 1, 0, null, null, mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, refilltry, x_partia, x_serial_no, x_slownik,
              lot, frommwsact
    from mwsacts ma
    where ma.ref = :frommwsact;
  end
  update mwsacts ma set ma.status = 1, ma.quantityc = ma.quantity
    where ma.mwsord  = :newmwsord;
  -- auto commit mwsord
  if (exists(select first 1 1 from mwsacts ma where ma.mwsord = :newmwsord)) then begin
    update mwsords mo set mo.status = 1, mo.operator = null
      where mo.ref = :newmwsord
        and mo.status = 0;
  end else begin
  -- kasowanie pustego mwsord
    delete from mwsords where ref = :newmwsord;
  end
end^
SET TERM ; ^
