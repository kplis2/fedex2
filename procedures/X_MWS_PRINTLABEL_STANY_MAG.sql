--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_PRINTLABEL_STANY_MAG(
      WERSJAREF INTEGER_ID,
      KTM KTM_ID,
      ILKOP INTEGER_ID)
  returns (
      ZPL STRING8191)
   as
  declare variable eol string3;
  declare variable nazwa towary_nazwa;
  declare variable kodkresk string1024;
  declare variable x_indeks_kat string40;
  declare variable zpl_temp string8191;
  declare variable i integer_id;

begin
zpl_temp='';
i=0;
eol = '
';
zpl = '';
-- pobranie nazwy towaru
    select t.nazwa, t.x_indeks_kat, t.kodkresk
      from towary t
      where t.ktm = :ktm
    into :nazwa, :x_indeks_kat, :kodkresk;
-- usuwanie polskich znakow
    execute procedure x_usun_znaki2(nazwa, 1, null, null) returning_values :nazwa;
    execute procedure x_usun_znaki2(ktm, 1, null, null) returning_values :ktm;

-- budowanie kodu kreskowego
--    kodkresk = '#V#'||coalesce(wersjaref,'')||'#K#'||coalesce(ktm,'')||'#IK#'||coalesce(x_indeks_kat,'');
-- budowanie etykiety  75x35
    zpl = 'CT~~CD,~CC^~CT~
^XA~TA000~JSN^LT0^MNW^MTD^PON^PMN^LH0,0^JMA^PR5,5~SD15^JUS^LRN^CI0^XZ
^XA
^MMT
^PW599
^LL0280
^LS0'||:eol;

      zpl = zpl||'^FT40,75^A0L,28,28^FH\^FDNazwa: ^FS' || :eol;
      zpl = zpl||'^FT40,100^A0L,28,28^FH\^FDKTM: ^FS'|| :eol;
if (coalesce(x_indeks_kat, '') <> '') then
zpl = zpl ||'^FT40,125^A0L,28,28^FH\^FDIK: ^FS' || :eol;
zpl = zpl ||'
^BY3,3,130^FT40,265^BCN,,Y,N
^FD>:'||coalesce(kodkresk, '')||'^FS'||:eol||
'^FT120,75^A0L,28,28^FH\^FD'||coalesce(:nazwa,'')||'^FS' || :eol ||
'^FT120,100^A0L,28,28^FH\^FD'||coalesce(:ktm,'')||'^FS' || :eol ||
'^FT120,125^A0L,28,28^FH\^FD'||coalesce(:x_indeks_kat,'')||'^FS' || :eol  ||
'^PQ1,0,1,Y^XZ';

while (i < ILKOP) do
begin
    zpl_temp = zpl_temp || :eol || zpl;
    if (strlen(zpl_temp) > 8190) then exception universal 'prosze zmniejszyc liczbe etykiet do wydruku';
  i=i+1;
end
 --^FT50,1100^BQN,2,10
-- ^FH\^FDLA,'||coalesce(:kodkresk,'')||'^FS
zpl=zpl_temp;

    suspend;
end^
SET TERM ; ^
