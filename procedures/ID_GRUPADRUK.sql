--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ID_GRUPADRUK
  returns (
      ID integer)
   as
BEGIN
  ID = gen_id(GRUPADRUK,1);
  execute procedure RP_CHECK_REF('GRUPADRUK', :id)
    returning_values :id;
  suspend;
END^
SET TERM ; ^
