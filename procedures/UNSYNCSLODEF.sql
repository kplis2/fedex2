--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE UNSYNCSLODEF(
      DEST BKYEARS_ID)
  returns (
      RET smallint)
   as
begin
  -- Procedura odwiązująca wzorzec do roku ksigowego.
  ret = 0;
  update slodef b
    set b.patternref = null
    where b.ref = :dest;
  ret = 1;
  suspend;
end^
SET TERM ; ^
