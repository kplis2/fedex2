--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDE_REAL(
      PROPERSRAP integer,
      PRSHORTAGE integer,
      MODE smallint,
      OUT smallint)
   as
declare variable PRSCHEDGUIDE integer;
declare variable PRSCHEDOPER integer;
declare variable REPORTEDFACTOR numeric(14,4);
declare variable KAMOUNTNORM numeric(14,4);
declare variable POZZAMREF integer;
declare variable ISPOS smallint;
declare variable OPER varchar(3);
declare variable ZAM integer;
declare variable TMP_S varchar(4096);
declare variable TMP_I integer;
declare variable HIST integer;
declare variable AMOUNT numeric(14,4);
declare variable TOREJ varchar(3);
declare variable TOSTAN integer;
declare variable NOTBLOKKLON smallint;
declare variable REALIZACJA smallint;
declare variable RAPAMOUNT numeric(14,4);
declare variable OPERATOR integer;
declare variable KTM varchar(40);
declare variable OPERNUMBER integer;
declare variable PRGRUPAROZL integer;
declare variable DOCREF integer;
declare variable AKCEPT smallint;
declare variable MULTIDOC smallint;
declare variable PGREF integer;
declare variable PRNAGZAM integer;
declare variable PRPOZZAM integer;
declare variable WERSJAREF integer;
declare variable WH varchar(3);
declare variable TYPDOKSH varchar(3);
declare variable TYPDOKIN varchar(3);
declare variable TYPDOKOUT varchar(3);
declare variable DOCCNT integer;
declare variable STYPE varchar(20);
declare variable PRDEPART varchar(20);
declare variable CNT smallint;
begin
  --mode: 0-> dodanie
  --      1-> poprawienie
  --      2-> kasowanie

  -- out PW/RW
  --      0-> PW
  --      1-> RW
  prgruparozl = null;
  if (:propersrap is not null) then
    select p.reportedfactor, p.ref, p.guide, o.amount, o.operator, p.number, o.prgruparozl, p.prdepart
      from prschedopers p
        join propersraps o on (o.prschedoper = p.ref)
      where o.ref = :propersrap
      into:reportedfactor, :prschedoper, :prschedguide, :rapamount, :operator, :opernumber, :prgruparozl, :prdepart;
  else if (:prshortage is not null) then
    select p.reportedfactor, p.ref, p.guide, s.amount, s.operator, s.ktm, s.wersjaref, s.prgruparozl, s.prshortagestype, p.prdepart
      from prschedopers p
        join prshortages s on (s.prschedoper = p.ref)
      where s.ref = :prshortage
      into:reportedfactor, :prschedoper, :prschedguide, :rapamount, :operator, :ktm, :wersjaref, :prgruparozl, :stype, :prdepart;
  else
    exception prschedguides_error 'Brak wskazania raportu do realizacji';

  if (mode in (1,2) and prgruparozl is not null) then  -- wycofanie
  begin
    multidoc = 0;
    for
      select n.ref, n.akcept
        from prschedguidedets d
          left join dokumpoz p on (p.prschedguidedet = d.ref)
          left join dokumnag n on (n.ref = p.dokument)
          left join defdokum f on (f.symbol = n.typ)
        where d.prgruparozl = :prgruparozl and f.wydania = :out
        group by n.ref, n.akcept
        into docref, akcept
    do begin
      -- sprawdzamy czy na dokumencie nie ma innych pozycji z innych raportów
      if (exists(select first 1 1 from dokumpoz p
            left join prschedguidedets d on (d.ref = p.prschedguidedet)
            where p.dokument = :docref and d.prgruparozl <> :prgruparozl and d.out = :out)
      ) then
        multidoc = 1;
      if (multidoc = 0) then
      begin
        if (akcept > 0) then
          update dokumnag set akcept = 0 where ref = :docref;
        delete from dokumpoz where dokument = :docref;
        delete from dokumnag where ref = :docref;
      end
      else
      begin
        if (akcept > 0) then
          update dokumnag set akcept = 9 where ref = :docref;
        for
          select d.ref
            from prschedguidedets d
            where d.prgruparozl = :prgruparozl and d.out = :out
            into pgref
        do begin
          delete from dokumpoz where prschedguidedet = :pgref;
        end
      end
    end
    update prschedguidedets d set d.accept = 0 where prgruparozl = :prgruparozl and out = :out;
    delete from prschedguidedets where prgruparozl = :prgruparozl and out = :out;
  end
  if (mode in (0,1)) then -- dodanie
  begin
    prgruparozl = null;
    select p.prgruparozl
      from propersraps p
      where p.ref = :propersrap
      into prgruparozl;
    if (prgruparozl is null) then
      execute procedure PRGRUPAROZL_ID returning_values prgruparozl;
    ispos = 0;
    select zamowienie, pozzam
      from prschedguides where ref = :prschedguide
      into prnagzam, prpozzam;
    EXECUTE PROCEDURE XK_PR_DEFAULTDOCTYPES(:prpozzam) returning_values (:typdokin, :typdokout);
    if (:out = 1) then  --RW
    begin
      if (:propersrap is not null) then
      begin
        for
          select pg.ref, pg.ktm, pg.wersjaref, pg.kamountnorm, pg.warehouse
            from prschedguidespos pg
              left join prshmat s on (s.ref = pg.prshmat)
            where pg.prschedguide = :prschedguide and pg.prschedoper = :prschedoper and pg.out = 1
              and (s.mattype = 0 or s.mattype is null)  and s.autodocout = 1 --tylko surowce rozchodowane automatycznie
            into pgref, ktm, wersjaref, kamountnorm, wh
        do begin
          if (ispos = 0) then
            ispos = 1;
          amount = (rapamount / coalesce(reportedfactor,1)) * coalesce(kamountnorm,1);
          insert into prschedguidedets (prnagzam, prpozzam, prschedguide, prschedguidepos, ktm, wersjaref,
              quantity, wh, doctype, out, shortage, ssource, prschedoper, autodoc, prgruparozl)
            values (:prnagzam, :prpozzam, :prschedguide, :pgref, :ktm, :wersjaref,
             :amount, :wh, :typdokout, 1, 0, 0,:prschedoper, 1, :prgruparozl);
        end
        update propersraps set prgruparozl = :prgruparozl where ref = :propersrap;
      end else if (:prshortage is not null) then
      begin
        for
          select pg.ref, pg.ktm, pg.wersjaref, pg.kamountnorm, pg.warehouse
            from prschedguidespos pg
            where pg.prschedguide = :prschedguide and pg.prschedoper = :prschedoper
              and pg.ktm = :ktm
            into pgref, ktm, wersjaref, kamountnorm, wh
        do begin
          if (ispos = 0) then
            ispos = 1;
          amount = (rapamount / coalesce(reportedfactor,1)) * coalesce(kamountnorm,1);
          insert into prschedguidedets (prnagzam, prpozzam, prschedguide, prschedguidepos, ktm, wersjaref,
              quantity, wh, doctype, out, shortage, ssource, prschedoper, prshortage, autodoc, prgruparozl)
            values (:prnagzam, :prpozzam, :prschedguide, :pgref, :ktm, :wersjaref,
             :amount, :wh, :typdokout, 1, 1, 0,:prschedoper, :prshortage, 1, :prgruparozl);
        end
        update prshortages set prgruparozl = :prgruparozl where ref = :prshortage;
      end 
      if (ispos > 0) then
        execute PROCEDURE PR_GEN_DOC_PRGRUPAROZL (:prgruparozl,current_date,0,1,:out)
          returning_values doccnt;
    end if (out = 0 and rapamount > 0) then --PW
    begin
      select g.zamowienie, g.pozzam, g.warehouse, g.ktm, v.ref
       from prschedguides g
         left join wersje v on (v.ktm = g.ktm and v.nrwersji = g.wersja)
       where g.ref = :prschedguide
      into prnagzam, prpozzam, wh, ktm, wersjaref;
      if (prshortage is not null) then
      begin
        select p.defdokum
          from prshortagesprdeparts p
          where p.prdepart = :prdepart and p.prshortagetype = :stype
          into typdokin;
      end
      insert into prschedguidedets (prnagzam, prpozzam, prschedguide, prschedguidepos, ktm, wersjaref,
          quantity, wh, doctype, out, shortage, ssource, prschedoper, autodoc,prgruparozl)
        values (:prnagzam, :prpozzam, :prschedguide, null, :ktm, :wersjaref,
            :rapamount, :wh, :typdokin, 0, case when :prshortage is null then 0 else 1 end, 0,:prschedoper, 1,:prgruparozl);
      update propersraps set prgruparozl = :prgruparozl where ref = :propersrap;
      execute PROCEDURE PR_GEN_DOC_PRGRUPAROZL (:prgruparozl,current_date,0,1,:out)
        returning_values doccnt;
    end 
  end
end^
SET TERM ; ^
