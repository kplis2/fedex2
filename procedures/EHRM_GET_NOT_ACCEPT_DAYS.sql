--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EHRM_GET_NOT_ACCEPT_DAYS(
      EMPLOYEEREF EMPLOYEES_ID,
      SELECTEDYEAR SMALLINT_ID,
      SELECTEDMONTH SMALLINT_ID)
  returns (
      PERSONNAMES STRING120,
      ROWDATE TIMESTAMP_ID,
      EMPLOYEEID EMPLOYEES_ID)
   as
begin
    for select e.personnames, d.cdate, e.ref from emplcaldays d
    join employees e on d.employee = e.ref
    left join EMPLSUPERIORS es on(es.employee = e.ref)   --PR55566
    where es.superior = :employeeref and
           (:selectedmonth = 0 or d.pmonth = :selectedmonth) and
           (:selectedyear = 0 or d.pyear = :selectedyear) and
          d.status = 0
    order by d.cdate asc
    into :personnames, :rowdate, :employeeid
    do begin
        suspend;
    end
end^
SET TERM ; ^
