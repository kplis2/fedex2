--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDEDETS_ACCEPT(
      PRSCHEDGUIDEDET integer,
      OLDSOURCE integer,
      NEWSOURCE smallint)
  returns (
      ACCEPT smallint)
   as
declare variable prschedguidepos integer;
begin
  accept = 0;
  if (oldsource = 0 and newsource <> 0) then
  begin
    if (exists (select p.ref from dokumpoz p where p.prschedguidedet = :prschedguidedet)) then
      exception prschedguidedets_error 'Istnieją pozycje dok. magazynowych dla tej rozpiski';
  end
  else if (oldsource = 1 and newsource <> 1) then
  begin
    if (exists (select d.ref from prschedguidedets d where d.sref = :prschedguidedet and d.ssource = 1)) then
      exception prschedguidedets_error 'Istnieją pozycje powiązane dla tej rozpiski';
  end
  else if (oldsource = 2 and newsource <> 2) then
  begin
    select prschedguidepos
      from prschedguidedets
      where ref = :prschedguidedet
      into prschedguidepos;
    if (exists (select p.ref from pozzam p where p.prschedguidepos = :prschedguidepos)) then
      exception prschedguidedets_error 'Istnieją pozycje zleceń produkcyjnych powiązane dla tej rozpiski';
  end
  if (newsource = 3) then accept = 2;
end^
SET TERM ; ^
