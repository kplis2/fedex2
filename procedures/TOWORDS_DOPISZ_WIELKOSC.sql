--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TOWORDS_DOPISZ_WIELKOSC(
      P varchar(1024) CHARACTER SET UTF8                           ,
      FORMA integer,
      RZAD integer,
      LANGUAGE integer)
  returns (
      POUT varchar(1024) CHARACTER SET UTF8                           )
   as
begin
  -- Funkcja dopisuje liczbe do wyjsciowego lancucha
--exception test_break 'miechu;p='||:p||';forma='||:forma||';rzad='||:rzad||';la'||:language;
  if (rzad = 2) then begin
    if (language = 0) then begin
      if (forma = 1) then p = :p ||'tysiąc ';
      else if (forma = 2) then p = :p ||'tysiące ';
      else if (forma = 3) then p = :p ||'tysięcy ';
    end else if (language = 1) then p = :p || ' thousand ';
    else if (language = 2) then p = :p || 'tausend';
  end else if (rzad = 3) then begin
    if (language = 0) then begin
      if (forma = 1) then p = :p ||'milion ';
      else if (forma = 2) then p = :p ||'miliony ';
      else if (forma = 3) then p = :p ||'milionów ';
    end else if (language = 1) then p = :p ||' million ';
     else if (language = 2) then p = :p ||' Million ';
  end else if (rzad = 4) then begin
    if (language = 0) then begin
      if (forma = 1) then p = :p ||'miliard ';
      else if (forma = 2) then p = :p ||'miliardy ';
      else if (forma = 3) then p = :p ||'miliardów ';
    end if (language = 1) then p = :p ||'bilion ';
    if (language = 2) then p = :p ||'Milliarde ';
  end else if (rzad = 5) then begin
    if (language = 0) then begin
      if (forma = 1) then p = :p ||'bilion ';
      else if (forma = 2) then p = :p ||'biliony ';
      else if (forma = 3) then p = :p ||'bilionów ';
    end if (language = 1) then p = :p ||' ';
    if (language = 2) then p = :p ||'billion ';
  end

  pout = p;
  suspend;
end^
SET TERM ; ^
