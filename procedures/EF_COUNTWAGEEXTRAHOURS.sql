--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_COUNTWAGEEXTRAHOURS(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      ECOLLIST varchar(100) CHARACTER SET UTF8                           ,
      COUNTBONUS smallint = 0,
      PERCENT numeric(14,2) = 100,
      PVALBASE varchar(20) CHARACTER SET UTF8                            = null)
  returns (
      SUMWAGE numeric(14,4))
   as
declare variable FROMDATE date;
declare variable TODATE date;
declare variable PAYMENTTYPE smallint;
declare variable EFROMDATE date;
declare variable ETODATE date;
declare variable SALARY numeric(14,2);
declare variable CADDSALARY numeric(14,2);
declare variable FUNCSALARY numeric(14,2);
declare variable BASE numeric(14,2);
declare variable XBASE numeric(14,2);
declare variable WORKHOURS numeric(14,2);
declare variable EHOURS numeric(14,2);
declare variable WAGE numeric(14,2);
declare variable DIMNUM smallint;
declare variable DIMDEN smallint;
declare variable PROPORTIONAL smallint;
declare variable ADDSALARY numeric(14,2);
declare variable SUMWAGE_TMP numeric(14,2);
declare variable BASEWAGE numeric(14,2);
declare variable minimalwage numeric(14,2);
declare variable minwage numeric(14,2);
declare variable nworkhours numeric(14,2);
declare variable minbase numeric(14,2);
declare variable etype smallint;
begin
--MWr: Personel - do liczenia wynagrodzenia i dodatku za godziny nadliczbowe

  sumwage = 0;

  if (ecollist = '1200') then  --Wynagrodzenie za godziny nadliczbowe
  begin
    execute procedure ef_countwageextrahours(employee, payroll, prefix, ';590;600;610;')
      returning_values sumwage;
  end else
  ------------------------------------------------------------------------------
  if (ecollist = '1210') then  --Dodatek za godziny nadliczbowe
  begin
    execute procedure ef_countwageextrahours(employee, payroll, prefix, ';610;630;', 1, 100)
      returning_values sumwage;

    execute procedure ef_countwageextrahours(employee, payroll, prefix, ';600;620;', 1, 50)
     returning_values sumwage_tmp;

    sumwage = sumwage + sumwage_tmp;
  end else
    ------------------------------------------------------------------------------
  if (ecollist = '1250') then  --Dodatek za godziny nocne
  begin
    execute procedure ef_countwageextrahours(employee, payroll, prefix, ';640;', null, 20, 'MINIMALNAKRAJOWA')
      returning_values sumwage;
  end else
  ------------------------------------------------------------------------------
  begin
    ecollist = ';' || trim(';' from ecollist) || ';' ;
  
    execute procedure efunc_prdates(payroll, 0)
      returning_values fromdate, todate;

    execute procedure get_config('EWORKEDHOURS_TYPE', 2)
      returning_values :etype;

    if (etype = 0) then
    begin
    /*Zachowanie spojnosci z EF_WORKEDHOURS. Przyjeto uproszczony sposob rozliczenia
      tego trubu (nie badamy przebiegu zatrudnienia)*/
      select sum(pvalue) from eprpos
        where employee = :employee and payroll = :payroll
          and strmulticmp(';'||ecolumn||';', :ecollist) = 1
        into :ehours;
    end else begin
      select sum(amount) from eworkedhours
        where employee = :employee
          and strmulticmp(';'||ecolumn||';', :ecollist) = 1
          and hdate >= :fromdate and hdate <= :todate
        into :ehours;
    end

    if (ehours <> 0) then
    begin
      select ws from e_get_nominalworktime(:employee, :fromdate, :todate)
        into :nworkhours;
  
      if (coalesce(nworkhours,0) = 0) then exit;
      else nworkhours = nworkhours / 3600.00;

      percent = :percent * 0.01;
  
      if (pvalbase > '') then
      begin
        execute procedure ef_pval (employee, payroll, prefix, pvalbase)
          returning_values basewage;

        basewage = basewage / nworkhours;
        sumwage = ehours * percent * basewage;
      end else
      begin
      /*Aby wlaczyc wyrownanie do minimalnej krajowej nalezy odkomentowac ponizsze:
        execute procedure ef_pval(employee, payroll, prefix, 'MINIMALNAKRAJOWA')
          returning_values minimalwage; */

        for
          select fromdate, todate, paymenttype, coalesce(dimnum,1), coalesce(dimden,1)
               , salary, caddsalary, funcsalary, proportional
            from e_get_employmentchanges(:employee, :fromdate, :todate, ';WORKDIM;PAYMENTTYPE;SALARY;CADDSALARY;FUNCSALARY;PROPORTIONAL;')
            order by fromdate
            into :efromdate, :etodate, :paymenttype, :dimnum, :dimden
               , :salary, :caddsalary, :funcsalary, :proportional
        do begin

          if (etodate is null or etodate > todate) then
            etodate = todate;
          if (efromdate < fromdate) then
            efromdate = fromdate;
    
          if (etype > 0) then
          begin
            ehours = null;
            select sum(amount) from eworkedhours
              where employee = :employee
                and strmulticmp(';'||ecolumn||';', :ecollist) = 1
                and hdate >= :efromdate and hdate <= :etodate
              into :ehours;
          end

          if (ehours <> 0) then
          begin
            addsalary = 0; -- moze byc to np. wysluga = salary * staz * 0.01;
            minwage = cast(minimalwage * dimnum / dimden as numeric(14,2));
            if (proportional = 1) then
              workhours = cast(nworkhours * dimnum / dimden as numeric(14,2));
            else
              workhours = nworkhours;

            if (paymenttype = 1) then
            begin --godzinowi
              base = salary + (caddsalary + funcsalary + addsalary) / workhours;
              minbase = minwage / nworkhours;
              if (base < minbase) then base = minbase;
              xbase = salary + funcsalary / workhours;
            end else
            begin --miesieczni
              base = (salary + caddsalary + funcsalary + addsalary);
              if (base < minwage) then base = minwage;
              base = base / workhours;  --stawka godz. do wynagrodzenia
              xbase = (salary + funcsalary) / workhours;  --stawka godz. do dodatku
            end
    
            if (countbonus = 1) then
              wage = xbase * ehours * :percent;  --rozliczenie godzin - jako dodatek
            else
              wage = base * ehours * :percent;   --rozliczenie godzin - jako wynagrodzenie
        
            sumwage = sumwage + wage;

            if (etype = 0) then leave;
          end
        end
      end
    end
  end
  suspend;
end^
SET TERM ; ^
