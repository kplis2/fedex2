--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_BARCODE_VALID_4_MWSORD(
      BARCODE STRING100,
      MWSORD INTEGER_ID = 0)
  returns (
      STATUS SMALLINT_ID,
      MWSACT MWSACTS_ID)
   as
declare variable CNT INTEGER_ID;
begin

/* status:
   0 - nie ma takiego kodu na MWSORD
   1 - jest kod na MWSORD i zwracany jest MWSACT
   2 - jest wiele MWSACOW pasujacych
   3 - prawdopodobnie przekazana ilosc bo podany kod kreskowy ma dlugosc <=5

 */

  if (position(';',BARCODE)>0) then
    BARCODE=substring(BARCODE from 1 for position(';',BARCODE)-1);

  if (char_length(:barcode)>5) then
  begin
    select  count(distinct x_mwsact) , min(x_mwsact)
      from  X_MWS_BARCODELIST(:barcode, 0, :MWSORD)
    into :cnt, :mwsact;

    if (cnt > 1) then
    begin
      status = 2;
      suspend;
      exit;
    end
    else if (:mwsact is not null) then
      status = 1;
    else
      status = 0;
  end
  else
    status=3;

  suspend;
end^
SET TERM ; ^
