--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BHP_GEN_PLAN(
      BHPEMPLSORT integer,
      FROMDATE date,
      TODATE date,
      PLANCONTINUE smallint,
      DELOUTOFPLAN smallint)
  returns (
      COUNTGEN integer,
      COUNTDEL integer)
   as
declare variable EEMPL integer;
declare variable BHPEMPLREF integer;
declare variable BHPFROMDATE date;
declare variable BHPTODATE date;
declare variable EKTM varchar(40);
declare variable EVER integer;
declare variable PERUNITCOUNT smallint;
declare variable EAMOUNT smallint;
declare variable EUNIT integer;
declare variable EMONTHSCOUNT smallint;
declare variable EDAYSCOUNT smallint;
declare variable NEXTDATE timestamp;
declare variable I smallint;
declare variable PLANPOSREF integer;
declare variable ISNEXTDATE smallint;
declare variable PREVNEXTDATE date;
declare variable BFROMDATE date;
declare variable BTODATE date;
begin

/*
  FROMDATE, TODATE - zakres czasowy, w ktorym ma sie odbywac generowanie planu.
    Ostatecznie zakres ten zostanie zestawiony z ograniczeniamy czasowmi na
    definicji sortu dla pracownika. Daty nie moga byc null'owe.

  PLANCONTINUE - czy kontynuwac/uzupelnic poprzedni plan BHP pracownika. Jezeli
    = 1: znajdz ostatnie wydanie w zadanym okresie. Wszystkie wydania z data
         wieksza od tego wydania w zadanym okresie usun
    = 0: usun wszystkie wydania z data wieksza lub rowna poczatku zadanego okresu

  DELOUTOFPLAN - jezeli rowne 1 to przy usuwaniu wydan przed generowaniem zostana
    tez w zadanym okresie usuniete wydania reczne, oznaczone statusem jako niewydane
*/

  countgen = 0;
  countdel = 0;
  bhpemplsort = coalesce(bhpemplsort,0);
  PlanContinue = coalesce(PlanContinue,0);
  DelOutOfPlan = coalesce(DelOutOfPlan,0);

  for
    select ref, employee, fromdate, todate
      from bhpemplsortdef d
      where ref = :bhpemplsort or :bhpemplsort = 0
      order by fromdate
    into :bhpemplref, :eempl, :bhpfromdate, :bhptodate
  do begin

  --okreslenie rzeczywistego zakresu dat, w ktorym moze sie odbyc generowanie
    bfromdate = iif(fromdate < bhpfromdate, bhpfromdate, fromdate);
    btodate = iif(todate > bhptodate, bhptodate, todate);

    if (bfromdate <= btodate) then
    begin
      for
        select p.ktm, p.vers, p.period, p.amount, p.unit, u.monthscount, u.dayscount
          from bhpemplsortpos p
            join bhptimeunits u on (u.ref = p.periodtunit)
          where p.bhpemplsortdef = :bhpemplref
        order by p.ktm, p.vers
        into :ektm, :ever, :perunitcount, :eamount, :eunit, :emonthscount, :edayscount
      do begin
        nextdate = null;
        IsNextDate = 1;
        i = 0;

      /*pobranie ostatniej daty, dla ktorej jest wydanie o ile mamy wlaczana
        opcje uzupelnienie/kontynuacji planu wydania */
        if (PlanContinue = 1) then
        begin
          select max(datereal) from bhpplanpos
            where employee = :eempl
              and ktm = :ektm and vers = :ever
              and outofplan = 0
          group by employee, ktm, vers
          into :nextdate;
        end

        if (nextdate is null) then
        begin
          nextdate = bfromdate;
          IsNextDate = 0;
        end

      --czyszczenie wczesniej okreslonego planu
        for
          select ref from bhpplanpos
            where employee = :eempl
              and ktm = :ektm and vers = :ever
              and status = 0 and outofplan = 0
              and datereal <= :btodate
              and (datereal > :nextdate or (datereal = :nextdate and :IsNextDate = 0))
              into :planposref
        do begin
          delete from bhpplanpos where ref = :planposref;
          countdel = countdel + 1;
        end

      --jezeli artykul nie mial planu to zapoczatkuj pierwszy wpis dla planu
        if (IsNextDate = 0) then
        begin
          insert into bhpplanpos (employee, datereal, status, ktm, vers, amount, unit, outofplan)
            values(:eempl, :nextdate, 0, :ektm, :ever, :eamount, :eunit, 0);
          countgen = countgen + 1;
          IsNextDate = 1;
        end

        while (IsNextDate = 1) --wejscie do petli zawsze przy pierwszym przebiegu
        do begin
        /*okreslenie nastepnej daty wydania artykulu i utworzenie planu wydania.
          zmienna prevnextdate stanowi zabezpieczenie przed petla nieskonczona*/
          prevnextdate = nextdate;
          i = 0;
          while (i < PerUnitCount)
          do begin
            select datazm
              from datatookres(cast(cast(:nextdate as date) as varchar(15)), 0, :emonthscount, :edayscount, 0)
              into :nextdate;
            i = i + 1;
          end

          if (bfromdate <= nextdate and nextdate <= btodate and prevnextdate <> nextdate) then
          begin
            insert into bhpplanpos (employee, datereal, status, ktm, vers, amount, unit, outofplan)
              values(:eempl, :nextdate, 0, :ektm, :ever, :eamount, :eunit, 0);
            countgen = countgen + 1;
          end else
            IsNextDate = 0;
        end
      end

    --czyszczenie pozycji pozaplanowych (BS47387)
      if (DelOutOfPlan = 1) then
      begin
        for
          select ref from bhpplanpos
            where employee = :eempl
              and status = 0 and outofplan = 1
              and datereal <= :todate
              and datereal >= :fromdate
            into :planposref
        do begin
          delete from bhpplanpos where ref = :planposref;
          countdel = countdel + 1;
        end
      end
    end
  end
  suspend;
end^
SET TERM ; ^
