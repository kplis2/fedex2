--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RDBINDICIES_TABELA(
      INDEXNAME varchar(100) CHARACTER SET UTF8                           )
  returns (
      TABLENAME varchar(255) CHARACTER SET UTF8                           ,
      COLUMNSNAME varchar(255) CHARACTER SET UTF8                           )
   as
declare variable ilicznik integer;
declare variable tmp varchar(255);
begin
  ilicznik = 0;
  select rdb$relation_name from rdb$indices where rdb$index_name = :IndexName into :TableName;
  for
    select RDB$FIELD_NAME
      from rdb$index_segments
      where rdb$index_name = :IndexName
      into :tmp
    do begin
      if (ilicznik > 0) then
        columnsname = columnsname||';'||tmp;
      else
        columnsname = tmp;
      ilicznik = ilicznik + 1;
    end
end^
SET TERM ; ^
