--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_PRINTLABEL(
      WERSJAREF INTEGER_ID,
      KTM KTM_ID)
  returns (
      ZPL STRING1024)
   as
  declare variable eol string3;
  declare variable nazwa towary_nazwa;
  declare variable kodkresk string1024;
  declare variable x_indeks_kat string40;

begin
eol = '
';
zpl = '';
-- pobranie nazwy towaru
    select t.nazwa, t.x_indeks_kat, t.kodkresk
      from towary t
      where t.ktm = :ktm
    into :nazwa, :x_indeks_kat, :kodkresk;
-- usuwanie polskich znakow
    execute procedure x_usun_znaki2(nazwa, 1, null, null) returning_values :nazwa;
    execute procedure x_usun_znaki2(ktm, 1, null, null) returning_values :ktm;

-- budowanie kodu kreskowego
--    kodkresk = '#V#'||coalesce(wersjaref,'')||'#K#'||coalesce(ktm,'')||'#IK#'||coalesce(x_indeks_kat,'');
-- budowanie etykiety  75x35
    zpl = 'CT~~CD,~CC^~CT~
^XA~TA000~JSN^LT0^MNW^MTD^PON^PMN^LH0,0^JMA^PR5,5~SD15^JUS^LRN^CI0^XZ
^XA
^MMT
^PW599
^LL0280
^LS0'||:eol;

      zpl = zpl||'^FT10,35^A0L,28,28^FH\^FDNazwa: ^FS
^FT10,60^A0L,28,28^FH\^FDKTM: ^FS';
if (coalesce(x_indeks_kat, '') <> '') then
zpl = zpl ||'^FT10,85^A0L,28,28^FH\^FDIK: ^FS';
zpl = zpl ||'
^FT10,110^A0L,28,28^FH\^FDData: ^FS
^BY3,3,130^FT10,245^BCN,,Y,N
^FD>:'||coalesce(kodkresk, '')||'^FS
^FT90,35^A0L,28,28^FH\^FD'||coalesce(:nazwa,'')||'^FS
^FT90,60^A0L,28,28^FH\^FD'||coalesce(:ktm,'')||'^FS
^FT90,110^A0L,28,28^FH\^FD'||substr(current_timestamp(0),1,16)||'^FS
^FT90,85^A0L,28,28^FH\^FD'||coalesce(:x_indeks_kat,'')||'^FS
^PQ1,0,1,Y^XZ';
 --^FT50,1100^BQN,2,10
-- ^FH\^FDLA,'||coalesce(:kodkresk,'')||'^FS
  suspend;
end^
SET TERM ; ^
