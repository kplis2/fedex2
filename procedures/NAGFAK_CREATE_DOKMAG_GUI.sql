--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_CREATE_DOKMAG_GUI(
      FAK integer,
      TYPDOK varchar(3) CHARACTER SET UTF8                           ,
      TYPDOKO varchar(3) CHARACTER SET UTF8                           ,
      TYPDOKK varchar(3) CHARACTER SET UTF8                           ,
      TYPDOKKO varchar(3) CHARACTER SET UTF8                           ,
      DATA timestamp,
      AKCEPT smallint,
      KORGEN smallint default 0)
  returns (
      STATUS integer,
      DOKSYM varchar(255) CHARACTER SET UTF8                           )
   as
declare variable MAGAZYN varchar(3);
declare variable SSTATUS integer;
declare variable SDOKSYM varchar(255);
declare variable DOKMAG integer;
declare variable DOKMAGBLOK smallint;
declare variable SPAROWANY integer;
declare variable KOREKTA integer;
begin
--exception test_break''||fak||' '||typdok||' '||typdoko||' '||typdokk||' '||typdokko||' '||data||' '||akcept;
  status = 1;
  doksym = '';
  select NAGFAK.REFDOKM from NAGFAK where NAGFAK.REF =:fak into :dokmag;
  if(:dokmag >= 0) then begin
--    select max(BLOKADA) from DOKUMNAG where FAKTURA = :FAK into :dokmagblok;
--    if(:dokmagblok < 2) then /*MS: i tak kontrola jest w C */
      exception  FAK_DOK_CREATE_HASDOK;
  end
  if(:dokmag is null) then begin
    for select distinct MAGAZYN
    from POZFAK where DOKUMENT = :FAK
    into MAGAZYN
    do begin
      if(:status > 0 and :magazyn is not null) then begin
        execute procedure NAGFAK_CREATE_DOKMAG(:FAK,:MAGAZYN,:TYPDOK, :TYPDOKK,:TYPDOKO, :TYPDOKKO, :DATA,:AKCEPT,0,0) returning_values :sstatus, :sdoksym;
        if(:sstatus >0) then begin
          for select SYMBOL from DOKUMNAG where FAKTURA = :fak into  :sdoksym
          do begin
            if(:doksym <>'')then
              doksym = :doksym||';';
            doksym = :doksym || :sdoksym;
          end
        end else
          status = 0;
      end else if(:magazyn is null) then status = 2;
    end
    execute procedure NAGFAK_CREATE_DOKUMNOT(:fak,:data,:akcept) returning_values :sparowany,:sdoksym;
    if(:doksym <>'')then doksym = :doksym||';';
    doksym = :doksym || :sdoksym;

    if(coalesce(:korgen,0) = 1) then
    begin
      select korekta from nagfak where ref = :fak into :korekta;

      if(coalesce(:korekta,0) <> 0) then
        begin
          execute procedure NAGFAK_CREATE_DOKMAG_GUI(:korekta,:typdok,:typdoko,:typdokk,:typdokko,:data,:akcept,coalesce(:korgen,0))
          returning_values :status, :sdoksym;
          doksym = :doksym || :sdoksym;
        end
    end
  end
end^
SET TERM ; ^
