--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SRV_REALIZE(
      SRVREQUEST integer,
      SRVPOSITION integer,
      SRVHISTORY integer,
      SRVDEFPOS integer,
      CREATEDOKMAG smallint,
      MAGAZYN char(3) CHARACTER SET UTF8                           ,
      DEFDOKUM char(3) CHARACTER SET UTF8                           ,
      MAG2 char(3) CHARACTER SET UTF8                           ,
      KLIENT integer,
      DOSTAWCA integer,
      CREATEFAK smallint,
      REJFAK char(3) CHARACTER SET UTF8                           ,
      TYPFAK char(3) CHARACTER SET UTF8                           ,
      SERIALNR varchar(40) CHARACTER SET UTF8                           )
  returns (
      STATUS integer,
      SYMBOLMAG varchar(40) CHARACTER SET UTF8                           ,
      SYMBOLFAK varchar(40) CHARACTER SET UTF8                           )
   as
declare variable dokmagref integer;
declare variable dokpozref integer;
declare variable srvsymbol varchar(40);
declare variable numer integer;
declare variable ktm varchar(40);
declare variable ilosc numeric(14,4);
declare variable cenamag numeric(14,4);
declare variable cenanet numeric(14,4);
declare variable newmagref smallint;
declare variable tempsrednikpozycja integer;
declare variable tempserialnr varchar(30);
declare variable tempcnt integer;
declare variable przyjecia smallint;
declare variable wydania smallint;
declare variable zewn smallint;
declare variable doksernr integer;

begin
  tempsrednikpozycja = 0;
  if(klient=0) then klient=NULL;
  if(dostawca=0) then dostawca=NULL;
  select defdokum.wydania, defdokum.zewn from defdokum
    where defdokum.symbol = :DEFDOKUM
    into :wydania, :zewn;
  if (wydania = 1 and zewn = 1 and klient is null) then exception DOKUMNAG_FROMSRV_BK;
  symbolmag = '';
  symbolfak = '';
  select SYMBOL from SRVREQUESTS where REF=:srvrequest into :srvsymbol;
  update SRVPOSITIONS set REALIZED=1, SRVHISTORY = :srvhistory
  where (REF=:srvposition) and ((REALIZED=0) or (REALIZED is null));
  if(:createdokmag=1) then begin
    newmagref = 0;
    select REF from DOKUMNAG where MAGAZYN=:magazyn and TYP=:defdokum
      and HISTORIA=:srvhistory and SRVREQUEST=:srvrequest into :dokmagref;
    if(:dokmagref is null) then begin
      execute procedure CREATE_DOKUMNAG(:magazyn,:defdokum,:dostawca,:klient,:mag2,
      :srvhistory, NULL, :srvsymbol, '', :srvrequest) returning_values :dokmagref;
      newmagref = 1;
    end
    select KTM,AMOUNT,PRICE,NETSALEPRICE from SRVPOSITIONS
    where (REF=:srvposition) into :ktm,:ilosc,:cenamag,:cenanet;
    execute procedure GEN_REF('DOKUMPOZ') returning_values :dokpozref;

    insert into DOKUMPOZ(REF,DOKUMENT, KTM, WERSJA, ILOSC, CENA,CENACEN,CENANET, FROMSRVPOSITION)
    values(:dokpozref, :dokmagref, :ktm, 0, :ilosc, :cenamag,:cenanet,:cenanet, :srvposition);

    tempsrednikpozycja = 1;



    if (wydania = 0) then przyjecia = 1;
    else przyjecia = 0;

    while ( tempsrednikpozycja <> 0 and serialnr<>'') do begin

      doksernr = :doksernr + 1;
      tempsrednikpozycja = position(';' in :SERIALNR);

      tempcnt = 1;
      tempserialnr = '';
      while (tempcnt < :tempsrednikpozycja) do begin
        tempserialnr = :tempserialnr ||substring( serialnr from 1 for 1);
        serialnr = substring(serialnr from 2);
        tempcnt = :tempcnt +1;
      end
      if (substring(serialnr from 1 for 1) = ';') then serialnr = substring(serialnr from 2);
      if (tempserialnr = '') then tempserialnr = serialnr;

      insert into dokumser  (dokumser.refpoz,dokumser.typ,dokumser.numer,
        dokumser.odserial,dokumser.ilosc,dokumser.iloscroz)
      values (:dokpozref,'M',:doksernr,
        :tempserialnr,1,:przyjecia);

    end

    if(:newmagref=1) then select SYMBOL from DOKUMNAG where REF=:dokmagref into :symbolmag;
    else symbolmag='';
  end
  status=1;
end^
SET TERM ; ^
