--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDOPER_SHEET_DEP_GEN(
      GUIDEREF integer)
   as
declare variable fromoper integer;
declare variable mainsheet integer;
declare variable fromsheet integer;
declare variable tooper integer;
declare variable kamount numeric(14,4);
declare variable shortage numeric(14,4);
declare variable sheetfrom integer;
begin
  select g.prsheet from prschedguides g where g.ref = :guideref
    into mainsheet;
  for
    select o.ref, too.ref
      from prschedopers o
        left join prshopers p on (p.ref = o.shoper)
        left join prschedopers too on (too.guide = :guideref and too.shoper = o.tooper)
      where o.guide = :guideref and p.sheet <> :mainsheet and o.tooper is not null and too.ref is not null
        and not exists(select first 1 1 from prschedoperdeps d where d.depfrom = o.ref and d.depto is not null)
      group by o.ref, too.ref
      into fromoper, tooper
  do begin
    -- trzeba wyliczyc kamount pomiedzy kartami teachnilogicznymi
    kamount = null;
    shortage = null;
    fromsheet = null;
    select s.shortage, s.ref
      from prschedopers p
        left join prshopers o on (o.ref = p.shoper)
        left join prsheets s on (s.ref = o.sheet)
      where p.ref = :fromoper
      into shortage, fromsheet;
    if (shortage is null or shortage = 0) then shortage = 0;
    shortage = shortage / 100;
    shortage = 1 + shortage;
    select m.grossquantity
      from prschedopers o
        left join prshopers p on (p.ref = o.shoper)
        left join prshmat m on (p.ref = m.opermaster)
      where o.ref = :tooper and m.subsheet = :fromsheet
      into kamount;
    if (kamount is null or kamount = 0) then kamount = 1;
    insert into prschedoperdeps(DEPTO,DEPFROM,KAMOUNT,SHORTAGE) values (:tooper, :fromoper,:kamount,:shortage);
  end
  for
    select o.ref
      from prschedopers o
        left join prshopers p on (p.ref = o.shoper)
      where o.guide = :guideref and p.sheet <> :mainsheet and o.tooper is not null
        and not exists(select first 1 1 from prschedoperdeps d where d.depfrom = o.ref and d.depto is not null)
      group by o.ref
      into fromoper
  do begin
    tooper = null;
    select first 1 o.ref
      from prschedopers o
        left join prshopers p on (p.ref = o.shoper)
      where o.guide = :guideref and p.sheet = :mainsheet and o.reported > 0
      order by p.number
      into tooper;
    if (tooper is not null) then
    begin
      -- trzeba wyliczyc kamount pomiedzy kartami teachnilogicznymi
      kamount = null;
      shortage = null;
      fromsheet = null;
      select s.shortage, s.ref
        from prschedopers p
          left join prshopers o on (o.ref = p.shoper)
          left join prsheets s on (s.ref = o.sheet)
        where p.ref = :fromoper
        into shortage, fromsheet;
      if (shortage is null or shortage = 0) then shortage = 0;
      shortage = shortage / 100;
      shortage = 1 + shortage;
      select m.grossquantity
        from prschedopers o
          left join prshopers p on (p.ref = o.shoper)
          left join prshmat m on (p.ref = m.opermaster)
        where o.ref = :tooper and m.subsheet = :fromsheet
        into kamount;
      if (kamount is null or kamount = 0) then kamount = 1;
      insert into prschedoperdeps(DEPTO,DEPFROM,KAMOUNT,SHORTAGE) values (:tooper, :fromoper,:kamount,:shortage);
    end
  end
end^
SET TERM ; ^
