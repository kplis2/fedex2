--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PANELKONTRAHENTA_DANEADRESOWE
  returns (
      REF integer,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      NIP NIP,
      PESEL PESEL,
      REGON integer,
      ADRES varchar(255) CHARACTER SET UTF8                           ,
      ADRESWYSYLKOWY varchar(255) CHARACTER SET UTF8                           ,
      TELEFON PHONES_ID,
      FAX FAX_ID,
      EMAIL STRING255,
      WWW WEBPAGES_ID)
   as
begin

  for
    select p.ref,  p.nazwa, p.kodp, p.pesel, p.zawod,
        p.ulica||iif(coalesce(p.nrdomu,'') <> '', ' '||p.nrdomu,'')||iif(coalesce(p.nrlokalu,'') <> '', '/'||p.nrlokalu,''),
        p.pulica||iif(coalesce(p.pnrdomu,'') <> '', ' '||p.pnrdomu,'')||iif(coalesce(p.pnrlokalu,'') <> '', '/'||p.pnrlokalu,''),
        p.telefon, p.fax, p.email, p.www
      from pkosoby p
    --where p.ref=:ref
    into :ref, :nazwa, :nip, :pesel, :regon, :adres, :adreswysylkowy, :telefon, :fax, :email, :www
  do
  begin
    suspend;
  end
end^
SET TERM ; ^
