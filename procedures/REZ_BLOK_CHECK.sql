--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REZ_BLOK_CHECK(
      POZYCJA integer,
      NUMER integer,
      ILOSC numeric(14,4),
      ZDEJBLOK numeric(14,4))
   as
declare variable ildost numeric(14,4);
declare variable magazyn char(3);
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable cena numeric(14,2);
declare variable dostawa integer;
begin
  select DOKUMNAG.MAGAZYN,DOKUMPOZ.KTM, DOKUMPOZ.WERSJA, DOKUMROZ.cena, DOKUMROZ.dostawa from
    dokumroz join dokumpoz on (dokumpoz.ref=dokumroz.pozycja)
             join dokumnag on(dokumnag.ref = dokumpoz.dokument)
   where DOKUMROZ.POZYCJA=:POZYCJA and DOKUMROZ.NUMER = :numer
   into :magazyn, :ktm, :wersja, :cena, :dostawa;
   execute procedure REZ_GET_FREE_STAN(:magazyn,:ktm, :wersja, :cena, :dostawa) returning_values :ildost;
/*   if(:zdejblok > 0) then
     ildost = :ildost + :zdejblok;*/
   if(:ildost <:ilosc) then begin
     /*wyjatek tylko wtedy, kiedy rzeczywiscie sa jakies blokady
      - inaczej to moze byc zwykle zejscie na stan ujemny magazynu */
     ildost = null;
     select sum(ilosc) from STANYREZ where MAGAZYN=:magazyn and KTM=:ktm AND WERSJA=:wersja and STATUS='B' and ZREAL=0
           into :ildost;
     if(:ildost > 0) then
        exception ROZ_ILDOST_CHECK substring('Brak ilosci na mag. do akceptacji rozpiski dla ' || :ktm from 1 for 75);
   end
end^
SET TERM ; ^
