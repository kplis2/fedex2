--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EFUNC_GET_BANK_FROM_ACC(
      ACCNUMBER varchar(26) CHARACTER SET UTF8                           ,
      CONTROL_ACCOUNT smallint = 0)
  returns (
      BANKNAME STRING1024,
      BANKSHORTNAME varchar(64) CHARACTER SET UTF8                           )
   as
declare variable CORRECT integer = 1;
declare variable MSG varchar(60);
begin
--WG: Funkcja wskazujaca nazwe banku ze slownika na podstawie numeru konta ACCNUMBER

  if (control_account = 0) then 
  begin
      select correct, msg
        from control_bankaccount_number(:accnumber)
      into :correct, :msg;

    if (correct = 0) then
      exception universal :msg;

    accnumber = substring(accnumber from 3 for 6);

    select coalesce(nazwa,''), coalesce(symbol,'')
      from banki
      where bankid = :accnumber
    into bankname, bankshortname;
  end
  suspend;
end^
SET TERM ; ^
