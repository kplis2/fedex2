--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WHAREAS_FIND_LOGWHAREAS(
      WH varchar(3) CHARACTER SET UTF8                           )
   as
declare variable xp numeric (15,2);
declare variable xk numeric (15,2);
declare variable yp numeric (15,2);
declare variable yk numeric (15,2);
declare variable maxx numeric(15,2);
declare variable maxy numeric(15,2);
declare variable maxV numeric(15,2);
declare variable maxH numeric(15,2);
declare variable mindist numeric(15,2);
declare variable distE numeric (15,2);
declare variable distW numeric (15,2);
declare variable distN numeric (15,2);
declare variable distS numeric (15,2);
declare variable distEW numeric (15,2);
declare variable distWW numeric (15,2);
declare variable distNW numeric (15,2);
declare variable distSW numeric (15,2);
declare variable refN integer;
declare variable refS integer;
declare variable refW integer;
declare variable refE integer;
declare variable oref integer;
begin
  mindist = 25;
  select max(coordxe)+10, max(coordye)+10 from whareas
  into :maxx, :maxy;
  for
    select ref, coordxb, coordxe, coordyb, coordye, maxvertdist, maxhordist
    from whareas
    where wh = :wh and (areatype = 1 or areatype = 3)
  into :oref, :xp, :xk, :yp, :yk, :maxV, :maxH
  do begin
    if (maxV is null) then maxV = 89;
    if (maxh is null) then maxH = 89;
    distN = null;
    distS = null;
    distE = null;
    distW = null;
    distNW = null;
    distSW = null;
    distEW = null;
    distWW = null;
    refN = null;
    refS = null;
    refW = null;
    refE = null;     --    exception test_break :maxH;
    if(maxH <> 0) then begin
      select min(coordxb)
      from whareas
      where wh = :wh and areatype = 0
        and ((coordyb <= :yk - :mindist and coordye > :yp) or (coordye >= :yp + :mindist and coordyb < :yk)) --(coordyb <= :yk and coordye >= :yp)
        and (coordxb - :xk < :maxH and coordxb - :xk >=0)
      into :distE;
/*      select min(coordxb)
      from whareas
      where areatype = 1
        and ((coordyb <= :yk - :mindist and coordye > :yp) or (coordye >= :yp + :mindist and coordyb < :yk)) --(coordyb <= :yk and coordye >= :yp)
        and (coordxb - :xk < :maxH and coordxb - :xk >=0)
      into :distEW;
      if (distE > distEW) then distE = null;   */
      select max(coordxe)
      from whareas
      where wh = :wh and areatype = 0
        and ((coordyb <= :yk - :mindist and coordye > :yp) or (coordye >= :yp + :mindist and coordyb < :yk))
        and (:xp - coordxe < :maxH and :xp - coordxe >=0)
      into :distW;
/*      exception test_break :distW;
      select max(coordxe)
      from whareas
      where areatype = 1
        and ((coordyb <= :yk - :mindist and coordye > :yp) or (coordye >= :yp + :mindist and coordyb < :yk))
        and (:xp - coordxe < :maxH and :xp - coordxe >=0)
      into :distWW;
      if (distW < distWW) then distW = null; */
--      exception test_break :distW||' '||xp||' '||xk;
      select min(ref)
      from whareas 
      where wh = :wh and areatype = 0
        and ((coordyb <= :yk - :mindist and coordye > :yp) or (coordye >= :yp + :mindist and coordyb < :yk)) --(coordyb <= :yk and coordye >= :yp)
        and (:xp - coordxe < :maxH and :xp - coordxe >=0)
        and coordxe = :distW
        and not exists
          (select ref
             from whareas
             where wh = :wh and areatype = 2 and coordxe <=:xp and coordxb >= :distW
               and ((coordyb <= :yk - :mindist and coordye > :yp)
                 or (coordye >= :yp + :mindist and coordyb < :yk)))

      into :refW;
      select min(ref)
      from whareas 
      where wh = :wh and areatype = 0
        and ((coordyb <= :yk - :mindist and coordye > :yp) or (coordye >= :yp + :mindist and coordyb < :yk)) --(coordyb <= :yk and coordye >= :yp)
        and (coordxb - :xk < :maxH and coordxb - :xk >=0)
        and coordxb = :distE
        and not exists
          (select ref
            from whareas
            where wh = :wh and areatype = 2 and coordxe >=:xk and coordxb <= :distE
              and ((coordyb <= :yk - :mindist and coordye > :yp)
                or (coordye >= :yp + :mindist and coordyb < :yk))
            )
      into :refE;

    end
    if(maxV <> 0) then begin
      select max(coordye)
      from whareas
      where wh = :wh and areatype = 0
        and ((coordxb <= :xk - :mindist and coordxe > :xp) or (coordxe >= :xp + :mindist and coordxb < :xk)) --(coordxb <= :xk and coordxe >= :xp)
        and (:yp - coordye < :maxV and :yp - coordye >=0)
      into :distS;
/*      select max(coordye)
      from whareas
      where areatype = 1
        and ((coordxb <= :xk - :mindist and coordxe > :xp) or (coordxe >= :xp + :mindist and coordxb < :xk)) --(coordxb <= :xk and coordxe >= :xp)
        and (:yp - coordye < :maxV and :yp - coordye >=0)
      into :distSW;
      if (distS > distSW) then distS = null;  */
      select min(coordyb)
      from whareas
      where wh = :wh and areatype = 0
        and ((coordxb <= :xk - :mindist and coordxe > :xp) or (coordxe >= :xp + :mindist and coordxb < :xk)) --(coordxb <= :xk and coordxe >= :xp)
        and (coordyb - :yk < :maxV and coordyb - :yk >=0)
      into :distN;
/*      select min(coordyb)
      from whareas
      where areatype = 1
        and ((coordxb <= :xk - :mindist and coordxe > :xp) or (coordxe >= :xp + :mindist and coordxb < :xk)) --(coordxb <= :xk and coordxe >= :xp)
        and (:yp - coordye < :maxV and :yp - coordye >=0)
      into :distNW;
      if (distN < distNW) then distW = null;   */
      select min(ref)
      from whareas
      where wh = :wh and areatype = 0
        and ((coordxb <= :xk - :mindist and coordxe > :xp) or (coordxe >= :xp + :mindist and coordxb < :xk)) --(coordxb <= :xk and coordxe >= :xp)
        and (:yp - coordye < :maxV and :yp - coordye >=0)
        and coordye = :distS
        and not exists
          (select ref
            from whareas
            where wh = :wh and areatype = 2 and coordye <=:yp and coordyb >= :distS
              and ((coordxb <= :xk - :mindist and coordxe > :xp)
                or (coordxe >= :xp + :mindist and coordxb < :xk))
          )
      into :refS;
      select min(ref)
      from whareas 
      where wh = :wh and areatype = 0
        and ((coordxb <= :xk - :mindist and coordxe > :xp) or (coordxe >= :xp + :mindist and coordxb < :xk)) --(coordxb <= :xk and coordxe >= :xp)
        and (coordyb - :yk < :maxV and coordyb - :yk >=0)
        and coordyb = :distN
        and not exists
          (select ref
            from whareas
            where wh = :wh and areatype = 2 and coordye <=:distN and coordyb >= :yk
              and ((coordxb <= :xk - :mindist and coordxe > :xp)
                or (coordxe >= :xp + :mindist and coordxb < :xk))
          )
      into :refN;
    end
    if (distN is not null) then distN = distN - yk;
    if (distS is not null) then distS = yp - distS;
    if (distW is not null) then distW = xk - distW;
    if (distE is not null) then distE = distE - xp;
    update whareas set LOGWHAREAN = :refN, LOGWHAREANDIST = :distN, LOGWHAREAS = :refS, LOGWHAREASDIST = :distS,
      LOGWHAREAE = :refE, LOGWHAREAEDIST = :distE, LOGWHAREAW = :refW, LOGWHAREAWDIST = :distW
    where whareas.ref = :oref;
  end
end^
SET TERM ; ^
