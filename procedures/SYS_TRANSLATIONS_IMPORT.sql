--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_TRANSLATIONS_IMPORT(
      KEY0 varchar(40) CHARACTER SET UTF8                           ,
      COL0 varchar(255) CHARACTER SET UTF8                           ,
      COL1 varchar(255) CHARACTER SET UTF8                           ,
      COL2 varchar(255) CHARACTER SET UTF8                           ,
      COL3 varchar(255) CHARACTER SET UTF8                           )
  returns (
      STATUS integer,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
begin
  status = 1;
  msg = '';
  if(:col0='' or :col1='' or :col2='') then begin
    status = 0;
    msg = 'Pusta wartosc';
    exit;
  end
  if(:col3='') then exit;
  if(:key0<>'' and :key0<>'0') then begin
    update or insert into sys_translations(ref,SYS_TRANSLATION_TYPEID,ID,LANGUAGEID,"TRANSLATION")
      values(:key0,:col0,:col1,:col2,:col3)
      matching(ref);
  end else begin
    update or insert into sys_translations(SYS_TRANSLATION_TYPEID,ID,LANGUAGEID,"TRANSLATION")
      values(:col0,:col1,:col2,:col3)
      matching(SYS_TRANSLATION_TYPEID,ID,LANGUAGEID);
  end
end^
SET TERM ; ^
