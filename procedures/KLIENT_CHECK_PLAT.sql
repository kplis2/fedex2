--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KLIENT_CHECK_PLAT(
      KLIENT integer,
      TYP integer,
      SPOSZAP integer,
      KWOTA numeric(14,2),
      TERMIN integer)
  returns (
      BLOKADA integer,
      NIEWYPL integer,
      LIMITKR numeric(14,2),
      LIMITOVER numeric(14,2),
      POPLAT numeric(14,2),
      POPLATDNI integer,
      SPOSPLATBEZP integer,
      MAXTERMIN integer,
      MAXTERMINOVER integer)
   as
declare variable SLODEF integer;
declare variable POPLATDAY date;
declare variable BEZPIECZNY integer;
declare variable GLOBALBEZP varchar(30);
declare variable MAXSPOSPLAT integer;
declare variable KONTRAHLIMITRYGOR varchar(10);
declare variable KW_Z_WZ numeric(14,2);
begin
  blokada = 0;
  bezpieczny = -1;
  maxtermin = -1;
  maxterminover = 0;
  if(:klient is null) then exit;
  select KLIENCI.niewyplac, KLIENCI.limitkr, SPOSPLATBEZP, TERMIN, klienci.sposplat
  from KLIENCI where REF=:klient
  into :niewypl, :limitkr, :sposplatbezp, :maxtermin, :maxsposplat;
  if(:niewypl is null) then niewypl = 0;
  if(:limitkr is null) then limitkr = 0;
  if(:sposplatbezp is null) then sposplatbezp = 0;
  if((:maxtermin is null or :maxtermin <= 0) and :maxsposplat > 0)then
    select platnosci.termin from PLATNOSCI where ref=:maxsposplat into :maxtermin;
  if(:maxtermin is null) then maxtermin = -1;
  select REF from SLODEF where SLODEF.typ = 'KLIENCI' into :slodef;
  if(:slodef is null) then exception slodef_bezklientow;
  --jesli termin nieokreslony,m to zgodny ze sposobem platnosci
  if(coalesce(:termin,0) = 0)then
    select PLATNOSCI.termin from PLATNOSCI where REF=:sposzap into :termin;

  execute procedure getconfig('KONTRAHLIMITRYGOR') returning_values :KONTRAHLIMITRYGOR;
  if(:limitkr > 0 or :KONTRAHLIMITRYGOR = '1') then begin
    select sum(ROZRACH.winienzl) - sum(ROZRACH.MAZL) from ROZRACH
     where SLODEF = :slodef and slopoz = :klient and WINIEN<>MA into :limitover;
    if(:limitover is null) then limitover = 0;
    if(:kwota > 0) then begin
--      if(:maxtermin is null or :maxtermin = -1)then
--        select PLATNOSCI.termin from PLATNOSCI where REF=:sposzap into :maxtermin;
--      if(:maxtermin is null) then maxtermin = -1;
--      if(:maxtermin >= 0 ) then
      if(coalesce(:termin,0) > 0)then
      begin
        limitover = :limitover + kwota;
        if (coalesce(:maxtermin,-1) = -1) then
          select PLATNOSCI.termin from PLATNOSCI where REF=:sposzap into :maxtermin;
        maxtermin = coalesce(:maxtermin,-1);
      end
    end
      /*obliczenie zaleglosci wynikajacych z WZ*/
    select sum(case when defdokum.wydania = 1 then WARTSBRUTTO else -WARTSBRUTTO end)
      from DOKUMNAG
        left join defdokum on (defdokum.symbol=dokumnag.typ)
      where dokumnag.klient=:klient
        and coalesce(dokumnag.faktura,0)=0
        and dokumnag.akcept = 1
        and defdokum.zewn=1
        and defdokum.rozliczeniowy=1
        and dokumnag.blokadafak = 0
    into :KW_Z_WZ;
    if(:KW_Z_WZ is null or :KW_Z_WZ<0) then KW_Z_WZ=0;
    limitover = :limitover + :kw_z_wz - :limitkr;
    if(:limitover < 0) then limitover = 0;
  end   else
    limitover = 0;
  select sum(ROZRACH.winienzl) - sum(ROZRACH.MAZL), min(CASE when
  (ROZRACH.saldowm > 0)
  THEN DATAPLAT
  ELSE NULL
  END) from ROZRACH
     where SLODEF = :slodef and slopoz = :klient and WINIEN<>MA
     and ROZRACH.dataplat < current_date
  into :POPLAT, :POPLATDAY;
  poplatdni = current_date - :poplatday;
  if(:poplat is null or (:poplatdni is null) or (:poplat < 0) )then begin
    poplatdni = 0;
    poplat = 0;
  end
  if(:KONTRAHLIMITRYGOR = '1' and :termin > :maxtermin and :maxtermin >= 0) then begin
    maxterminover = 1;
  end
  if(:kwota <= 0) then
    sposplatbezp = 0;/*bezpieczny sposób nie jest wymagany, jak kwota kontrolowana jest równa 0 -
                    bo np. jest operacja kasowa powiązana z dokumentem*/
  if(:sposplatbezp > 0) then begin
    select platnosci.bezpieczny from PLATNOSCI where ref=:sposzap into :bezpieczny;
    if(:bezpieczny = 1) then
      sposplatbezp = 0;
  end
  if(:niewypl > 0 or (:limitover > 0) or (:poplat > 0) or (:sposplatbezp > 0) or (:maxterminover > 0))then begin
    blokada = 1;
    if(:sposplatbezp > 0) then blokada = 2;
    else if(:niewypl > 0 and :typ > 0) then blokada = 2;
    else if(:poplat > 0 and :typ = 2) then blokada = 2;
    else if((:limitover > 0 or :maxterminover > 0) and :typ = 3) then blokada = 2;
    else if(:typ = 4) then blokada = 2;
    if(:blokada = 2 and :bezpieczny = -1) then
      select platnosci.bezpieczny from PLATNOSCI where ref=:sposzap into :bezpieczny;
    if(:blokada = 2 and :bezpieczny = 1) then begin
      execute procedure GETCONFIG('SPRZBEZPPLATZAW') returning_values :globalbezp;
      if(:globalbezp = '1') then
        blokada = 1;
    end
  end
end^
SET TERM ; ^
