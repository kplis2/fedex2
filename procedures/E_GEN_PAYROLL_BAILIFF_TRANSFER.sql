--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GEN_PAYROLL_BAILIFF_TRANSFER(
      PAYROLL integer,
      TDATE date,
      BANKACC varchar(10) CHARACTER SET UTF8                           )
  returns (
      COUNTER integer)
   as
declare variable toacc varchar(60);
declare variable amount numeric(14,2);
declare variable descript varchar(255);
declare variable toadress varchar(255);
declare variable address varchar(255);
declare variable postcode varchar(6);
declare variable city varchar(255);
declare variable towho varchar(255);
declare variable komornik integer;
declare variable varcourtcase varchar(80);
declare variable varsignature varchar(80);
declare variable varperson varchar(81);
declare variable przelewtype varchar(10);
declare variable company integer;
declare variable btransfer integer;
declare variable ecolldedinstal integer;
begin

  counter = 0;
  if (tdate < current_timestamp(0)) then tdate = current_date;
  execute procedure getconfig('PRZELEWKOM') returning_values :przelewtype;
  select company from epayrolls where ref = :payroll into :company;

  for
    select d.bailiff, trim(coalesce(d.courtcase,'')), trim(coalesce(d.signature,''))
         , p.person, i.insvalue, i.ref
      from ecolldeductions d
        join ecolldedinstals i on (d.ref = i.colldeduction)
        join persons p on (p.ref = d.person)
      where i.payroll = :payroll
        and i.btransfer is null
        and i.insvalue > 0
      order by p.sname, p.fname
      into :komornik, :varcourtcase,  :varsignature, :varperson, :amount, :ecolldedinstal
  do begin

    if (komornik is not null) then
    begin
      select account, bailiffname, street, city, postcode
        from ebailiff
        where ref = :komornik
        into :toacc, :towho, :address, :city, :postcode;

      toadress = coalesce(address,'') || '; ' || coalesce(postcode,'') || ' ' || coalesce(city,'');
    end else
      toadress = '';

    descript = 'Przelew komorniczy ' || :varperson;
    if (varcourtcase <> '') then descript = descript || ', Sprawa: ' || varcourtcase;
    if (varsignature <> '') then descript = descript || ', Sygnatura: ' || varsignature;

    execute procedure gen_ref('BTRANSFERS')
      returning_values btransfer;

    insert into btransfers (ref, bankacc, typ, btype, data, towho, toacc,
        toadress, todescript, curr, amount, autobtransfer, company)
      values (:btransfer, :bankacc, 1, :przelewtype, :tdate, :towho, :toacc,
        :toadress, :descript, 'PLN', :amount, 1, :company);

    update ecolldedinstals set btransfer = :btransfer where ref = :ecolldedinstal;

    counter = counter + 1;
  end
  suspend;
end^
SET TERM ; ^
