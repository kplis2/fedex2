--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMENT_PRZELICZCENYSPR(
      DOK integer,
      TYP char(3) CHARACTER SET UTF8                           )
  returns (
      STATUS integer,
      SUMPOZYC integer,
      POZYCNOTGEN integer)
   as
declare variable POZREF integer;
declare variable KLIENT integer;
declare variable CENNIKDOM integer;
declare variable SCENNIKDOM varchar(255);
declare variable CENACEN numeric(14,4);
declare variable WALCEN varchar(3);
declare variable CONSTCEN numeric(14,4);
declare variable CONSTWAL varchar(3);
declare variable BN char(1);
declare variable WERSJAREF integer;
declare variable JEDN integer;
declare variable CENJEDN integer;
declare variable ILOSC numeric(14,4);
declare variable KTM varchar(40);
declare variable WERSJA integer;
declare variable DEFCEN integer;
declare variable PRZELICZ numeric(14,4);
declare variable RABAT numeric(14,4);
declare variable ODDZIAL varchar(255);
declare variable PRZELICZ2 numeric(14,4);
declare variable ZAFISK integer;
declare variable MAGAZYN varchar(3);
declare variable STANOWISKO varchar(20);
declare variable ZPAKIETEM smallint;
declare variable PROMOCJA integer;
declare variable PROCEDURA varchar(30);
declare variable PROCSTATUS integer;
declare variable CENA numeric(14,2);
declare variable DOSTAWA integer;
declare variable NAGODDZIAL varchar(20);
declare variable TMP varchar(420);
declare variable PREC smallint;
declare variable ZEWN integer;
declare variable WYDAN integer;
declare variable KORYG integer;
declare variable DATACEN timestamp;
begin
  STATUS = 0;
  SUMPOZYC = 0;
  POZYCNOTGEN = 0;
  execute procedure getconfig('CENNIK') returning_values :scennikdom;
  execute procedure getconfig('AKTUODDZIAL') returning_values :oddzial;
  if(:scennikdom <> '') then
    select REF from DEFCENNIK where REF=:scennikdom and akt > 0 into :cennikdom;
  if(:cennikdom is null or (cennikdom < 0)) then
    cennikdom = 0;
  if(:typ = 'F') then begin
    select KLIENT,BN, zafisk,stanowisko, oddzial, data
      from NAGFAK
      where REF=:dok
      into :klient, :bn, :zafisk, :stanowisko, :nagoddzial, :datacen;
    if(:klient is null or (:klient <= 0)) then exception PRZELICZCENYSPR_BRAKKLI;
    if((:bn <> 'N' and :bn <> 'B') or (:bn is null)) then exception PRZELICZCENYSPR_BRAKBN;
    if(:zafisk > 0) then exception PRZELICZCENYSPR_EXPT ;
    for select REF, KTM, WERSJA, WERSJAREF, JEDN, ILOSC, MAGAZYN, cenamag, dostawa, prec
    from POZFAK where DOKUMENT = :dok
    order by NUMER
    into :pozref, :ktm, :wersja, :wersjaref, :jedn, :ilosc, :magazyn, :cena, :dostawa, :prec
    do begin
      defcen = 0;
      execute procedure CENNIK_ZNAJDZ(:cennikdom, :wersjaref, :jedn, :klient, :oddzial, :stanowisko, :datacen) returning_values :defcen;
      if(:defcen > 0) then begin
      execute procedure pobierz_cene(:defcen,:wersjaref,:jedn, :bn,:cenacen,:walcen,:prec,:cena,:dostawa,:klient,:typ,:pozref)
        returning_values :cenacen, :walcen, :cenjedn, :status, :prec;
      if(:jedn > 0 and :cenjedn > 0 and :jedn <> :cenjedn) then begin
            /*przeliczenie ceny wg jednostek*/
            przelicz = 0;
            select PRZELICZ from TOWJEDN where ref=:jedn into :przelicz;
            if(:przelicz is null or (:przelicz = 0)) then przelicz = 1;
            przelicz2 = 0;
            select PRZELICZ from TOWJEDN where ref=:cenjedn into :przelicz2;
            if(:przelicz2 is null or (:przelicz2 = 0)) then przelicz2 = 1;
            if(:przelicz <> :przelicz2) then
              cenacen = :cenacen / :przelicz2 * przelicz;/*sprowadzenie do ceny jednostki magazynowej i podniesienie do jednostki sprzedazy*/
          end
        if(:cenacen is null) then cenacen = 0;
        if(:cenacen > 0) then begin
          /*okreslenie rabatu*/
           rabat = 0;
           constcen = 0;
           constwal = '';
           if(exists(select ref from pozfak where refdopakiet=:pozref)) then
             zpakietem = 1;
           else
             zpakietem = 0;
           execute procedure OBLICZ_RABAT(:ktm, :wersja, :klient, :ilosc, :cenacen, :bn, :magazyn, :defcen, 0, :zpakietem) returning_values :rabat, :constcen, :constwal, :promocja;
           if(:constcen > 0) then begin
             if(:constwal <> '') then walcen = :constwal;
             cenacen = :rabat;
             rabat = 0;
           end
           /*update pozycji dokumentu - reszt wartoci powinien przeliczyc triger*/
           update POZFAK set CENACEN = :cenacen, WALCEN = :walcen, RABAT = :rabat, RABATTAB = :rabat, refpromocji = :promocja where ref = :pozref;
           SUMPOZYC = :sumpozyc + 1;
        end else
          POZYCNOTGEN = :POZYCNOTGEN + 1;
      end else
        POZYCNOTGEN = :POZYCNOTGEN + 1;
    end
  end
  else if(:typ = 'Z') then begin
    select KLIENT, BN, oddzial, datawe
      from NAGZAM
      where REF=:dok
      into :klient, :bn, :nagoddzial, :datacen;
    if(:klient is null or (:klient <= 0)) then exception PRZELICZCENYSPR_BRAKKLI;
    if((:bn <> 'N' and :bn <> 'B') or (:bn is null)) then exception PRZELICZCENYSPR_BRAKBN;
    for select REF, KTM, WERSJA, WERSJAREF, JEDN, ILOSC, MAGAZYN, cenamag, dostawamag ,prec
    from POZZAM where ZAMOWIENIE = :dok
    order by NUMER
    into :pozref, :ktm, :wersja, :wersjaref, :jedn, :ilosc, :magazyn, :cena, :dostawa, :prec
    do begin
      defcen = 0;
      execute procedure CENNIK_ZNAJDZ(:cennikdom, :wersjaref, :jedn, :klient, :oddzial, '', :datacen) returning_values :defcen;
      if(:defcen > 0) then begin
        execute procedure pobierz_cene(:defcen,:wersjaref,:jedn, :bn,:cenacen,:walcen,:prec,:cena,:dostawa,:klient,:typ,:pozref)
          returning_values :cenacen, :walcen, :cenjedn, :status, :prec;
        if(:jedn > 0 and :cenjedn > 0 and :jedn <> :cenjedn) then begin
            /*przeliczenie ceny wg jednostek*/
            przelicz = 0;
            select PRZELICZ from TOWJEDN where ref=:jedn into :przelicz;
            if(:przelicz is null or (:przelicz = 0)) then przelicz = 1;
            przelicz2 = 0;
            select PRZELICZ from TOWJEDN where ref=:cenjedn into :przelicz2;
            if(:przelicz2 is null or (:przelicz2 = 0)) then przelicz2 = 1;
            if(:przelicz <> :przelicz2) then
              cenacen = :cenacen / :przelicz2 * przelicz;/*sprowadzenie do ceny jednostki magazynowej i podniesienie do jednostki sprzedazy*/
          end
        if(:cenacen is null) then cenacen = 0;
        if(:cenacen > 0) then begin
          /*okreslenie rabatu*/
           rabat = 0;
           constcen = 0;
           constwal = '';
           if(exists(select ref from pozzam where refdopakiet=:pozref)) then
             zpakietem = 1;
           else
             zpakietem = 0;
           execute procedure OBLICZ_RABAT(:ktm, :wersja, :klient, :ilosc, :cenacen, :bn, :magazyn, :defcen, 0, :zpakietem) returning_values :rabat, :constcen, :constwal, :promocja;
           if(:constcen > 0) then begin
             if(:constwal <> '') then walcen = :constwal;
             cenacen = :rabat;
             rabat = 0;
           end
           /*update pozycji dokumentu - reszt wartoci powinien przeliczyc triger*/
           update POZZAM set CENACEN = :cenacen, WALCEN = :walcen, RABAT = :rabat, RABATTAB = :rabat, refpromocji = :promocja where ref = :pozref;
           SUMPOZYC = :sumpozyc + 1;
        end else
          POZYCNOTGEN = :POZYCNOTGEN + 1;
      end else
        POZYCNOTGEN = :POZYCNOTGEN + 1;
    end
  end
  else if(:typ = 'D') then begin
    select df.zewn, df.wydania, df.koryg
      from DOKUMNAG dn join defdokum df on (dn.typ=df.symbol)
      where dn.ref = :dok
      into  :zewn, :wydan, :koryg ;
    if(:zewn is distinct from 1 or :wydan is distinct from 1 or :koryg is distinct from 0 ) then exception PRZELICZCENYSPR_WZ;
    select KLIENT,BN, stansprzed, oddzial, data
      from DOKUMNAG
      where ref =: dok
      into :klient, :bn, :stanowisko, :nagoddzial, :datacen;
    if(:klient is null or (:klient <= 0)) then exception PRZELICZCENYSPR_BRAKKLI;
    if((:bn <> 'N' and :bn <> 'B') or (:bn is null)) then exception PRZELICZCENYSPR_BRAKBN;
    for select dp.REF, dp.KTM, WERSJA, WERSJAREF, t.dm, ILOSC, dn.MAGAZYN, cena, dp.dostawa, dp.prec
    from DOKUMPOZ dp  left join towary t on t.ktm = dp.ktm join dokumnag dn on dn.ref = dp.dokument
     where DOKUMENT = :dok
    order by dp.NUMER
    into :pozref, :ktm, :wersja, :wersjaref, :jedn, :ilosc, :magazyn, :cena, :dostawa, :prec
    do begin
      defcen = 0;
      execute procedure CENNIK_ZNAJDZ(:cennikdom, :wersjaref, :jedn, :klient, :oddzial, :stanowisko, :datacen) returning_values :defcen;
      if(:defcen > 0) then begin
        execute procedure pobierz_cene(:defcen,:wersjaref,:jedn, :bn,:cenacen,:walcen,:prec,:cena,:dostawa,:klient,:typ,:pozref)
          returning_values :cenacen, :walcen, :cenjedn, :status, :prec;
        if(:jedn > 0 and :cenjedn > 0 and :jedn <> :cenjedn) then begin
            /*przeliczenie ceny wg jednostek*/
            przelicz = 0;
            select PRZELICZ from TOWJEDN where ref=:jedn into :przelicz;
            if(:przelicz is null or (:przelicz = 0)) then przelicz = 1;
            przelicz2 = 0;
            select PRZELICZ from TOWJEDN where ref=:cenjedn into :przelicz2;
            if(:przelicz2 is null or (:przelicz2 = 0)) then przelicz2 = 1;
            if(:przelicz <> :przelicz2) then
              cenacen = :cenacen / :przelicz2 * przelicz;/*sprowadzenie do ceny jednostki magazynowej i podniesienie do jednostki sprzedazy*/
          end
        if(:cenacen is null) then cenacen = 0;
        if(:cenacen > 0) then begin
          /*okreslenie rabatu*/
           rabat = 0;
           constcen = 0;
           constwal = '';
           if(exists(select ref from dokumpoz where refdopakiet=:pozref)) then
             zpakietem = 1;
           else
             zpakietem = 0;
           execute procedure OBLICZ_RABAT(:ktm, :wersja, :klient, :ilosc, :cenacen, :bn, :magazyn, :defcen, 0, :zpakietem) returning_values :rabat, :constcen, :constwal, :promocja;
           if(:constcen > 0) then begin
             if(:constwal <> '') then walcen = :constwal;
             cenacen = :rabat;
             rabat = 0;
           end
           /*update pozycji dokumentu - reszt wartoci powinien przeliczyc triger*/
           update dokumpoz set CENACEN = :cenacen, WALCEN = :walcen, RABAT = :rabat, RABATTAB = :rabat, refpromocji = :promocja where ref = :pozref;
           SUMPOZYC = :sumpozyc + 1;
        end else
          POZYCNOTGEN = :POZYCNOTGEN + 1;
      end else
        POZYCNOTGEN = :POZYCNOTGEN + 1;
    end
  end
  STATUS = 1;
end^
SET TERM ; ^
