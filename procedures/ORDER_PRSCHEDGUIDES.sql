--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ORDER_PRSCHEDGUIDES(
      PRSCHEDULE integer,
      OLDN integer,
      NEWN integer)
   as
DECLARE VARIABLE REF INTEGER;
DECLARE VARIABLE NUMBER INTEGER;
begin
  --firts we find minor number
  if (oldn < newn) then
    newn = oldn;

  number = newn;
  for
    select ref
      from PRSCHEDGUIDES
      where prschedule = :prschedule and number >= :number
      order by number, ord
      into :ref
  do begin
    update PRSCHEDGUIDES set number = :number, ord = 2
      where ref = :ref;
--exception test_break number||' '||ref;
    number = number + 1;
  end
  update PRSCHEDGUIDES set ord = 1
    where prschedule = :prschedule and number >= :newn;
end^
SET TERM ; ^
