--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_MWSORDIN_CHECK_PACKINGLIST(
      KODKRESK varchar(100) CHARACTER SET UTF8                           ,
      MWSORD integer)
  returns (
      PACKINGLIST integer)
   as
declare variable docid integer;
declare variable oref integer;
begin
  -- szukamy listy pakowej
  select docid from mwsords where ref = :mwsord
    into docid;
  if (docid is not null) then
  begin
    select first 1 p.dokument
      from listywysdpoz p
      where p.doktyp = 'M' and p.dokref = :docid
      into packinglist;
  end
  if (packinglist is null) then packinglist = 0;
  -- jak juz jest lista to sprawdzamy, czy paczka nalezy do tego listy
  if (packinglist > 0) then
  begin
    select first 1 o.ref
      from listywysdroz_opk o
      where o.listwysd = :packinglist and cast(o.nropk as varchar(100)) = :kodkresk
      into oref;
    if (oref is null) then packinglist = 0;
  end
end^
SET TERM ; ^
