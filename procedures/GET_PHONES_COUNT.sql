--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_PHONES_COUNT(
      TYP STRING,
      ID STRING)
  returns (
      NCOUNT INTEGER_ID,
      NUMBER STRING)
   as
begin
  select count(ref)
    from cphones p
    where p.stable = :typ and p.skey = :id
    into :ncount;
  if (ncount = 1) then
    select p.fullnumber
      from cphones p
      where p.stable = :typ and p.skey = :id
      into :number;
  suspend;
end^
SET TERM ; ^
