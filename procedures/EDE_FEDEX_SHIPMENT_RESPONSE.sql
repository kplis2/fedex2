--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_SHIPMENT_RESPONSE(
      EDEDOCSH_REF integer)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
   as
declare variable shippingdoc integer;
declare variable shippingdocno varchar(20);
declare variable parent integer;
declare variable packref integer;
declare variable packno varchar(20);
begin
  select oref
    from ededocsh
    where ref = :ededocsh_ref
  into :shippingdoc;

  select p.val
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.name = 'nrPrzesylki'
  into :shippingdocno;

  update listywysd
    set spedresponse = 1, symbolsped = :shippingdocno, numerprzesylkistr = :shippingdocno
    where ref = :shippingdoc;

  for
    select p.id
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.name = 'paczka'
    into :parent
  do begin
    select cast(p.val as integer)
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'nrExtPp'
    into :packref;

    select p.val
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'nrpp'
    into :packno;

    update listywysdroz_opk set nrpaczkistr = :packno
      where ref = :packref;
  end

  otable = 'LISTYWYSD';
  oref = :shippingdoc;

  suspend;
end^
SET TERM ; ^
