--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_NOTY_GEN(
      DATA timestamp,
      DOUNCLOSED smallint,
      FORACCOUNT ACCOUNT_ID,
      FORSLODEF integer,
      COMPANY integer)
  returns (
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      SLODEF integer,
      SLOPOZ integer,
      KONTOFK KONTO_ID,
      ILDOKUM integer,
      SUMINTEREST numeric(14,2))
   as
declare variable symbfak varchar(20);
declare variable dataotw timestamp;
declare variable dataplat timestamp;
declare variable datazm timestamp;
declare variable saldown numeric(14,2);
declare variable kwotafak numeric(14,4);
declare variable kwotazap numeric(14,4);
declare variable wn numeric(14,2);
declare variable ma numeric(14,2);
declare variable dt timestamp;
declare variable notadokum integer;
declare variable interest numeric(14,2);
declare variable refnagfak integer;
declare variable tabela integer;
declare variable counted integer;
declare variable slodefref integer;
begin
  for select ROZRACH.SLODEF, ROZRACH.SLOPOZ
    from ROZRACH
    where rozrach.dataplat < :data
          and ((:forslodef is null) or (:forslodef = 0) or (rozrach.slodef = :forslodef))
--          and ((:forslopoz is null) or (:forslopoz = 0) or (rozrach.slopoz = :forslopoz))
          and ((:FORACCOUNT is null) or (:FORACCOUNT = '') or (rozrach.kontofk like :FORACCOUNT))
          and ((:dounclosed = 1) or (cast(rozrach.datazamk as date) <= :data))
          and ((rozrach.datazamk > rozrach.dataplat) or (rozrach.datazamk is null))
          and rozrach.company = :company
    group by rozrach.slodef, rozrach.slopoz
    into :slodef, :slopoz
  do begin
    --nagówek dokument
    select nazwa from slo_dane(:slodef, :slopoz) into :nazwa;
    tabela = null;
    select min(ref) from slodef where slodef.typ = 'KLIENCI' into :slodefref;
    if (slodef = :slodefref) then
      select interesttables
        from klienci
          where ref = :slopoz
      into :tabela;
    if (tabela is null) then
      select ref
        from interesttables
        where isdefault = 1
      into :tabela;
    if (:tabela is null) then
      exception FK_EXPT_TAB_ODSET;

    suminterest = 0;
    ildokum = 0;
    for
      select SYMBFAK, KONTOFK, dataotw, dataplat, faktura
        from ROZRACH
        where SLODEF = :slodef and slopoz = :slopoz
           and dataplat < :data and (datazamk <= :data or (:dounclosed = 1))
           and ((datazamk > dataplat) or (datazamk is null))
           and rozrach.company = :company
        into :symbfak, :kontofk, :dataotw, :dataplat, :refnagfak
    do begin
      --analiza pojedynczego rozrachunku
      kwotafak = 0;
      kwotazap = 0;
      saldown = 0;
      datazm = :dataplat;

      for
        select WINIEN, MA, stransdate, notadokum
          from ROZRACHP
          where slodef = :slodef and slopoz = :slopoz
            and symbfak = :symbfak and kontofk = :kontofk and cast(data as date) <= :data
            and rozrachp.company = :company
          order by data
          into :wn, :ma, :dt, :notadokum
      do begin
        counted = 0;
        --obliczenie dni i odsetek do dnia zmiany
        if (wn > 0 and kwotafak = 0) then
          kwotafak = wn;

        if (ma > 0) then
          kwotazap = kwotazap + ma;

        if (ma > 0) then
          saldown = saldown - ma;
        else
          saldown = saldown + wn;

        if (ma > 0 and kwotafak > 0) then
        begin
          if (saldown < 0) then
            kwotazap = kwotazap + saldown; --zmniejszenie kwoty zaplconej
          if (notadokum is null) then
          begin
            if (dt > dataplat) then
            begin
              interest = 0;
              if (dt >= datazm ) then
              begin
                execute procedure INTEREST_COUNT(:kontofk, :symbfak, :dataplat,:dt,:kwotazap, :tabela, :company) returning_values :interest;
                if (interest is not null) then
                begin
                  suminterest = suminterest + interest;
                  if (counted = 0) then
                  begin
                    ildokum = ildokum + 1;
                    counted = 1;
                  end
                end
              end
            end

            if (saldown < 0) then
              kwotazap = kwotazap - saldown;  --zmniejszenie kwoty zaplconej
          end
          kwotazap = kwotazap - ma;
        end
        datazm = dt;
      end
    end
     if (suminterest <> 0) then
       suspend;
  end
end^
SET TERM ; ^
