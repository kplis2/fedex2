--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_EDEFPCARDS_GET_SUBGROUPS(
      GROUPREF integer,
      SIGNIN smallint = 1)
  returns (
      REF integer,
      SIGN smallint)
   as
declare variable tmp_gr integer;
declare variable tmp_sign smallint;
begin
/*MWr: Procedura uzywana do generowania kartotek definiowalnych. Zwraca wszystkie
       podgrupy, ktore wchodza w sklad zadanej grupy GROUPREF wraz ze znakiem
       udzialu: SIGN=0 - udzial ujemny; SIGN=1 - udzial dodatni (PR31747)*/

  signin = coalesce(signin, 1);

  if (exists(select first 1 1 from edefpcardsgroup
               where ref = :groupref and isgroupsum = 0)
  ) then begin
    sign = signin;
    ref = :groupref;
    suspend;
  end else
    for
      select subgroup, sign from edefpcardsgrpos
        where defpcardgr = :groupref
        into: tmp_gr, :tmp_sign
    do
      for
        select ref, case when sign <> :signin then 0 else 1 end
          from rpt_edefpcards_get_subgroups(:tmp_gr, :tmp_sign)
          into :ref, :sign
      do
        suspend;
end^
SET TERM ; ^
