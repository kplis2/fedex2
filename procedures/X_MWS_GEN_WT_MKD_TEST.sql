--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_GEN_WT_MKD_TEST(
      NAGZAMREF NAGZAM_ID)
  returns (
      XREFNAG INTEGER_ID)
   as
begin
-- PROCEDURA DO TESTOWANIA
-- dodanie do tabeli przejsciowych x_imp_dokumnag i x_imp_dokumpoz
-- dokumentu WZ wygenerowanego z zamowienia sprzedaży.
    insert into X_IMP_DOKUMNAG (NAGID, SYMBOL, NUMER, TYP, DATA, MAGAZYN, MAG2, WYDANIA, ZEWNETRZNY, KLIENTID ,
                                ODBIORCAID, UWAGIWEW, UWAGIZEW, UWAGISPED,
                                PRIORYTET, SOBOTA, ODROCZONEWYDANIE, STATUS,
                                DATAPRZET, IDSTATUS, DOSTAWA,
                                dulica, dnrdomu, dnrlokalu, dmiastoid, dkodpocztowy, dnazwa, platnosc, datawysylki)
        select  N.REF, N.ID, N.NUMER, 'WZ', current_timestamp, N.MAGAZYN, N.MAG2, 1, 1, k.int_id,
                o.int_id, N.UWAGIWEWN, 'uwaga zewnetrzna' || N.ref, N.UWAGISPED,
                N.PRPRIORITY, 0, 0, 9, -- status 9 czyli do przetworzenia
                current_timestamp, N.ref, n.sposdost,
                n.dulica, n.dnrdomu, n.dnrlokalu, n.dmiasto, n.dkodp, n.odbiorca, n.sposzap, n.datazm
        from NAGZAM N
            left join klienci k on n.klient = k.ref
            left join odbiorcy o on n.odbiorcaid = o.ref
        where n.ref = :nagzamref
        returning ref into :xrefnag;
    insert into X_IMP_DOKUMPOZ (NAGID, POZID, NUMER, IDARTYKULU, JEDNOSTKA, VAT, MAGAZYN, MAG2, ILOSC, CENANETTO,
                                CENABRUTTO, WARTOSCNETTO, WARTOSCBRUTTO,
                                STATUS, DATAPRZET, IDSTATUS, nagimpref)
        select
                p.ZAMOWIENIE,p.REF, p.NUMER, t.int_id , tj.jedn, p.GR_VAT, p.MAGAZYN, p.MAG2, p.ILOSC, p.CENANET,
                p.CENABRU, p.WARTRNET, p.WARTRBRU,
                9, current_timestamp, p.ref, :xrefnag
        from POZZAM p
            left join towjedn tj on p.jedn = tj.ref
            join towary t on p.ktm = t.ktm
        where p.zamowienie = :nagzamref;
            
    suspend;
end^
SET TERM ; ^
