--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_KASTOZAM(
      ZAM integer)
  returns (
      REF integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      OPIS varchar(80) CHARACTER SET UTF8                           ,
      WARTDOKKAS numeric(10,4),
      SYM varchar(20) CHARACTER SET UTF8                           )
   as
begin
  for
    select d.ref, d.typ || ' ' || d.numer || ' (' || cast(d.data as date) || ')',
           substring(d.opis from 1 for 80), sum(p.kwota), d.symbol
      from rkdoknag d
        join rkdokpoz p on (d.ref = p.dokument)
      where p.zamowienie=:zam
      group by d.ref, d.typ || ' ' || d.numer || ' (' || cast(d.data as date) || ')', substring(d.opis from 1 for 80), d.symbol
      into :ref, :symbol, :opis, :wartdokkas, :sym
  do begin
    suspend;
  end
end^
SET TERM ; ^
