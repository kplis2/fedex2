--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BLANK_KODFK(
      TYP varchar(10) CHARACTER SET UTF8                           )
  returns (
      NUMER ANALYTIC_ID)
   as
begin
  if (:typ = 'KLIENCI') then
    select max(KONTOFK) from KLIENCI into :numer;
  if (:typ = 'DOSTAWCY') then
    select max(KONTOFK) from DOSTAWCY into :numer;
end^
SET TERM ; ^
