--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EVAC_RCA_DRA(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      COMPANY integer,
      PERSON integer = 0,
      INB01 varchar(6) CHARACTER SET UTF8                            = null)
  returns (
      B01 varchar(6) CHARACTER SET UTF8                           ,
      B04 numeric(14,2),
      B06 numeric(14,2),
      B10 numeric(14,2),
      B11 numeric(14,2),
      B12 numeric(14,2),
      DNI_MACIERZYNSKIEGO smallint,
      DNI_WYCHOWAWCZEGO smallint)
   as
declare variable fromdate date;
declare variable todate date;
declare variable dni_kalendarzowe smallint;
declare variable fep numeric(14,2);
declare variable fez numeric(14,2);
declare variable frp numeric(14,2);
declare variable frz numeric(14,2);
declare variable uz numeric(14,2);
declare variable prev_todate date;
declare variable prev_fromdate date;
begin

  b01 = inb01;
  b10 = 0;
  b11 = 0;
  b12 = 0;

  select sum(case when a.ecolumn = 260 and (a.ayear || lpad(a.amonth,2,0)) = r.cper then days else 0 end),
         sum(case when a.ecolumn in (140, 150, 270, 280, 290, 440, 450, 460) then a.days else 0 end)
   from epayrolls r
      join eabsences a on (r.ref = a.epayroll)
      join employees e on (e.ref = a.employee)
    where r.company = :company and r.iper = :period
      and e.company = :company and e.person = :person
      and a.correction in (0,2)
    into :dni_wychowawczego, :dni_macierzynskiego;

  dni_wychowawczego = coalesce(dni_wychowawczego, 0);
  dni_macierzynskiego = coalesce(dni_macierzynskiego, 0);

  if (dni_wychowawczego > 0 or dni_macierzynskiego > 0) then
  begin
    execute procedure period2dates(period)
      returning_values fromdate, todate;

    execute procedure get_pval(fromdate, company, 'FEP') returning_values fep;
    execute procedure get_pval(fromdate, company, 'FEZ') returning_values fez;
    execute procedure get_pval(fromdate, company, 'FRP') returning_values frp;
    execute procedure get_pval(fromdate, company, 'FRZ') returning_values frz;

  --obsluga urlopu wychowawczego
    if (dni_wychowawczego > 0) then
    begin
      execute procedure e_get_podst_er_wychowawczy(:person,:period,:company) --PR44589
        returning_values b04;
  
      if (b04 <> 0) then
      begin
        if (exists (select first 1 1 from epayrolls R
                        join eprpos p on (p.payroll = r.ref)
                        join employees e on (e.ref = p.employee)
                where r.company = :company and r.iper = :period and r.cper <> :period
                  and e.company = :company and e.person = :person
                  and p.ecolumn = 260)
        ) then begin
          prev_todate = (fromdate - 1);
          prev_fromdate = cast(extract(year from prev_todate) || '/' ||extract(month from prev_todate) || '/1' as date);
          dni_kalendarzowe = prev_todate - prev_fromdate + 1;
        end else
          dni_kalendarzowe = todate - fromdate + 1;
  
        if (dni_wychowawczego < dni_kalendarzowe) then
          b04 = 1.00 * dni_wychowawczego * b04 / dni_kalendarzowe;
    
        b11 = b04 * (fep + fez) /100;
        b12 = b04 * (frp + frz) /100;
    
        execute procedure get_pval(fromdate, company, 'PODST_UZ_WYCHOWAWCZY') returning_values b06;
        execute procedure get_pval(fromdate, company, 'UZ') returning_values uz;
        b10 = b06 * uz / 100;
    
        b01 = '1211' || substring(b01 from 5 for 2);
        if (B04 < 0 or B04 is null) then B04 = 0;
        if (B06 < 0 or B06 is null) then B06 = 0;
        if (B10 < 0 or B10 is null) then B10 = 0;
        if (B11 < 0 or B11 is null) then B11 = 0;
        if (B12 < 0 or B12 is null) then B12 = 0;
        suspend;
        B04 = 0;
        B06 = 0;
        B10 = 0;
        B11 = 0;
        B12 = 0;
        dni_wychowawczego = 0;
      end
    end
  
  --obsluga urlopu macierzynskiego
    if (dni_macierzynskiego > 0) then
    begin
      b04 = 0;
  
      select sum(p.pvalue) from epayrolls r
          join eprpos p on (p.payroll = r.ref)
          join employees e on (e.ref = p.employee)
        where r.company = :company and r.iper = :period
          and e.company = :company and e.person = :person
          and p.ecolumn in (4350, 4360)
        into :b04;
  
      if (b04 <> 0) then
      begin
        b11 = b04 * (fep + fez) /100;
        b12 = b04 * (frp + frz) /100;
    
        b01 = '1240' || substring(b01 from 5 for 2);
        if (B04 < 0 or B04 is null) then B04 = 0;
        if (B06 < 0 or B06 is null) then B06 = 0;
        if (B11 < 0 or B11 is null) then B11 = 0;
        if (B12 < 0 or B12 is null) then B12 = 0;
        suspend;
      end
    end
  end
end^
SET TERM ; ^
