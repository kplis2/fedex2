--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_SET_APPINI(
      NAMESPACE varchar(40) CHARACTER SET UTF8                           ,
      SECTION varchar(255) CHARACTER SET UTF8                           ,
      IDENT varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable currlayer integer;
declare variable prevval varchar(1024);
declare variable newnamespace varchar(40);
begin
  select layer from s_namespaces where namespace=:namespace into :currlayer;
  -- sprawdz warstwe poprzedzajaca, czy klucz jest potrzebny na tej warstwie
  if(:val is not null and :currlayer is not null and :currlayer>0) then begin
    prevval = NULL;
    select first 1 s.val
    from s_appini s join s_namespaces n on (n.namespace=s.namespace)
    where s.section=:section and s.ident=:ident and n.layer<:currlayer
    order by n.layer desc
    into :prevval;
    -- jesli wartosc z warstwy poprzedzajacej jest taka sama, to nie zapisuj jej na tej warstwie
    if(:prevval is not null and :prevval=:val) then val = NULL;
  end

  -- dodaj, popraw lub usun klucz
  if(:val is null) then begin
    delete from s_appini
    where namespace=:namespace and section=:section and ident=:ident;
    val = :prevval;
  end else begin
    update or insert into s_appini(namespace,section,ident,val)
      values (:namespace,:section,:ident,:val)
      matching (namespace,section,ident);
  end

  -- sprawdz warstwy potomne, czy tam jeszcze jest klucz potrzebny
  if(:currlayer is not null) then begin
    for select distinct s.namespace
    from s_appini s
    join s_namespaces n on (n.namespace=s.namespace)
    where s.section=:section and s.ident=:ident and n.layer>:currlayer
    order by n.layer
    into :newnamespace
    do begin
      execute procedure SYS_SET_APPINI(:newnamespace,:section,:ident,:val);
    end
  end
end^
SET TERM ; ^
