--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CKONTRAKTY_UPDATE_CPODMIOTYNOT(
      CKONTRAKT integer)
   as
declare variable lista VARCHAR(255);
declare variable cpodm integer;
begin
  lista = ';';
  for
      select distinct CNOTATKI.CPODMIOT
      from CNOTATKI
      where (CNOTATKI.CKONTRAKT=:ckontrakt)
      into :cpodm
  do begin
  lista = :lista || cast(:cpodm as varchar(10)) || ';';
  end
  update CKONTRAKTY set CPODMIOTYNOT = :lista where REF=:ckontrakt;
end^
SET TERM ; ^
