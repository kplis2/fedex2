--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAME_FROM_DICTPOSREF(
      DICTDEF integer,
      DICTREF integer)
  returns (
      NAME varchar(80) CHARACTER SET UTF8                           )
   as
declare variable s_typ varchar(20);
declare variable strloc varchar(255);
begin
  select typ from slodef where ref= :dictdef
    into :s_typ;
  strloc = '';
  if (:s_typ = 'ESYSTEM') then
    select nazwa from slopoz where slownik=:dictdef and ref=:dictref
      into :strloc;
  else if (:s_typ = 'KLIENCI') then
    select nazwa from klienci where  ref=:dictref
      into :strloc;
  else if (:s_typ = 'DOSTAWCY') then
    select nazwa from dostawcy where  ref=:dictref
      into :strloc;
  else if (:s_typ = 'PERSONS') then
    select person from persons where ref = :dictref
      into :strloc;
  else if (:s_typ = 'EMPLOYEES') then
    select personnames from employees where ref = :dictref
      into :strloc;
  if (coalesce(char_length(:strloc),0)>80)then -- [DG] XXX ZG119346
    name = substring(:strloc from 1 for 80);
  else
    name = :strloc;
  suspend;
end^
SET TERM ; ^
