--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_FUNDS(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      FUNDTYPE integer)
  returns (
      RET numeric(14,2))
   as
declare variable FROMDATE date;
declare variable TODATE date;
declare variable COLTYPE varchar(3);
declare variable EPRPOS integer;
declare variable INITVALUE numeric(14,2);
declare variable FINSTALMENT numeric(14,2);
declare variable INSTALMENT numeric(14,2);
declare variable INSVALUE numeric(14,2);
declare variable CREDIT integer;
begin
--MW: personel - funkcja liczy skladke i rate na KZP, rate i odsetki na ZFM

  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

--wpisowe
  if (fundtype = 7540) then
  begin
    select sum(entryfee) from efunds
      where employee = :employee and enterdate >= :fromdate and enterdate <= :todate
      into :ret;
  end else
  begin
    select coltype from ecolumns
      where number = :fundtype
      into :coltype;

----ZFM, raty i inne kredyty
    if (coltype = 'KRE') then
    begin
      select ref from eprpos
        where payroll = :payroll and ecolumn = :fundtype and employee = :employee
        into :eprpos;
      eprpos = coalesce(eprpos,0);
  
      select c.ref, c.finstalment, c.instalment, c.initvalue
        from ecredits c
          left join ecredinstals i on (c.ref = i.credit)
        where c.employee = :employee and c.credtype = :fundtype and c.fromdate <= :todate
          and i.eprpos = :eprpos
          and not exists(select d.ref from ecreditdelays d
                           where d.fromdate <= :fromdate and d.todate >= :todate and d.credit = c.ref)
        into :credit, :finstalment, :instalment, :initvalue;
  
      if (credit is null) then
        select c.ref, c.finstalment, c.instalment, c.initvalue
         from ecredits c
         where c.employee = :employee and c.credtype = :fundtype and c.fromdate <= :todate
            and c.actvalue > 0
            and not exists(select d.ref from ecreditdelays d
                             where d.fromdate <= :fromdate and d.todate >= :todate and d.credit = c.ref)
          into :credit, :finstalment, :instalment, :initvalue;
  
      if (credit is not null) then
      begin
      --jezeli nie ma wpisow badz jedyny to aktualny wpis (przy updacie) to bierz zwykla rate, nie poczatkowa
        select sum(insvalue) from ecredinstals
          where credit = :credit and eprpos <> :eprpos
          into :insvalue;
        insvalue = coalesce(insvalue,0);
  
        if (insvalue = 0 and finstalment > 0) then
          ret = finstalment;
        else if (instalment < initvalue - insvalue) then
          ret = instalment;
        else
          ret = initvalue - insvalue;
      end
    end else
----Obsluga skladek
    if (coltype = 'SKL') then
    begin
      select f.premium from efunds f
        where f.employee = :employee and f.fundtype = :fundtype and f.enterdate <= :todate
          and not exists(select d.ref from ecreditdelays d
                           where d.fromdate <= :fromdate and d.todate >= :todate and d.credit = f.ref)
        into :ret;
    end
  end

  suspend;
end^
SET TERM ; ^
