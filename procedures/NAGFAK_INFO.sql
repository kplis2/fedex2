--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_INFO(
      NAGFAKREF integer)
  returns (
      ZMIANA_ILOSCI smallint,
      ZMIANA_WART smallint,
      ZWROT smallint,
      LICZBA_POZ smallint)
   as
begin
  select count(*) from POZFAK where POZFAK.DOKUMENT=:nagfakref and POZFAK.ILOSC<>POZFAK.PILOSC
    into zmiana_ilosci;
  select count(*) from POZFAK where POZFAK.DOKUMENT=:nagfakref and POZFAK.PCENANET<>POZFAK.CENANET
    into zmiana_wart;
  select count(*) from POZFAK where POZFAK.DOKUMENT=:nagfakref and POZFAK.ILOSC=0
    into zwrot;
  select count(*) from POZFAK where POZFAK.DOKUMENT=:nagfakref
    into liczba_poz;
  suspend;
end^
SET TERM ; ^
