--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_BARCODE_VALID4MWSCONSTLOC(
      BARCODE STRING100,
      MWSACTREF INTEGER_ID,
      MWSCONSTLOC INTEGER_ID = 0)
  returns (
      STATUS SMALLINT_ID,
      MWSSTOCK MWSSTOCKS_ID,
      MWSACT MWSACTS_ID)
   as
declare variable CNT INTEGER_ID;
declare variable vers integer_id;
begin

/* status:
   0 - nie ma takiego kodu na MWSCONSTLOC
   1 - jest kod na MWSCONSTLOC i zwracany jest MWSSTOCK
   2 - jest wiele MWSSTOCK pasujacych
   3 - prawdopodobnie przekazana ilosc bo podany kod kreskowy ma dlugosc <=5

 */
 --exception universal 'bar'||coalesce(barcode,'<brak>')||'actref'||coalesce(mwsactref, 0)||'MWSCONSTLOC'||coalesce(MWSCONSTLOC, 0);
  select ma.vers
    from mwsacts ma
    where ma.ref = :mwsactref
  into :vers;

  if (position(';',BARCODE)>0) then
    BARCODE=substring(BARCODE from 1 for position(';',BARCODE)-1);

  if (char_length(:barcode)>5) then
  begin
    select  count(distinct X_MWSSTOCK) , min(X_MWSSTOCK), min(X_mwsact)
      from  X_MWS_BARCODELIST(:barcode, 0, 0,0,0,:MWSCONSTLOC)
      where  VERS=:vers
    into :cnt, :mwsstock, :mwsact;

    if (cnt > 1) then
    begin
      status = 2;
      suspend;
      exit;
    end
    else if (:mwsact is not null) then
      status = 1; 
    else if (:MWSSTOCK is not null) then
      status = 3;
    else
      status = 0;
  end
  else
    status=4;

  suspend;
end^
SET TERM ; ^
