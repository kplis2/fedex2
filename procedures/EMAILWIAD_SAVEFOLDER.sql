--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWIAD_SAVEFOLDER(
      MAILBOX integer,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      MAILFOLDER varchar(255) CHARACTER SET UTF8                           ,
      PARENTFOLDER varchar(255) CHARACTER SET UTF8                           ,
      ROLA integer)
  returns (
      REF integer)
   as
declare variable domyslny integer;
declare variable prawa    varchar(1024);
declare variable icon     varchar(255);
declare variable rodzic   integer;
begin
  domyslny = 0;
  if(:rola<>0) then domyslny = 1;

  select ';'||operator||';' from mailboxes where ref=:mailbox into :prawa;

  icon = 'MI_2';
  if(:rola=1) then icon = 'MI_EMAILREAD';
  else if(:rola=2) then icon = 'MI_EMAILEDIT';
  else if(:rola=3) then icon = 'MI_EMAILSENT';
  else if(:rola=4) then icon = 'MI_REDX';

  rodzic = null;
  if(:parentfolder<>'') then begin
    -- jesli podano parentfolder to szukamy refa folderu rodzica
    select ref from deffold where mailbox=:mailbox and mailfolder=:parentfolder into :rodzic;
    -- jesli nie ma jeszcze rodzica to go zakladamy. Podajemy tylko najwazniejsze informacje bo i tak folder zostanie dokladniej zsynchronizowany później
    if(:rodzic is null) then begin
      execute procedure EMAILWIAD_SAVEFOLDER(:mailbox,:parentfolder,:parentfolder,'',0) returning_values :rodzic;
    end
  end else begin
    select first 1 ref from deffold where mailbox=:mailbox and rola=5 into :rodzic;
  end
  ref = null;
  select first 1 ref from deffold where mailbox=:mailbox and mailfolder=:mailfolder into :ref;
  if(:ref is null) then begin
    INSERT INTO DEFFOLD (MAILBOX, RODZIC, TYP,  NAZWA, SYMBOL, PIKTOGRAM, DOMYSLNY, ROLA,
       OPERREAD, OPEREDIT, OPERSEND, MAILFOLDER, maxuid, nowewiadomosci, ICON, SYNCHROBYSERVER )
        VALUES ( :mailbox, :rodzic, '', :nazwa, substring(:nazwa from 1 for 30), 9, :domyslny, :rola,
        :prawa,:prawa,:prawa,:mailfolder, 0, 0, :icon, 1)
    returning ref into :ref;
  end else begin
    update DEFFOLD set rodzic=:rodzic, nazwa=:nazwa,  symbol = substring(:nazwa from 1 for 30),
      rola = :rola, icon = :icon
    where ref=:ref;
  end
end^
SET TERM ; ^
