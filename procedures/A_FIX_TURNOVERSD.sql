--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE A_FIX_TURNOVERSD(
      COMPANY integer)
   as
declare variable TRNDATE timestamp;
declare variable DEBIT numeric(14,2);
declare variable CREDIT numeric(14,2);
declare variable ACCOUNTING integer;
declare variable PERIOD varchar(6);
declare variable BKTYPE integer;
declare variable ACCOUNT ACCOUNT_ID;
declare variable EBDEBIT numeric(14,2);
declare variable EBCREDIT numeric(14,2);
begin
  for
   select D.transdate, sum(D.debit), sum(D.credit), A.ref, D.period, B.bktype, D.account, sum(case when d.status = 3 then d.debit else 0 end), sum(case when d.status = 3 then d.credit else 0 end)
      from decrees D
        left join accounting A on (A.account = D.account and A.yearid = substring(D.period from 1 for 4) and D.company = A.company and D.status > 1)
        left join bkaccounts B on (D.accref = B.ref)
      where A.currency = 'PLN' and A.company = :company
      group by D.transdate, A.ref, D.period, B.bktype, D.account
      into :trndate, :debit, :credit, :accounting, :period, :bktype, :account, :ebdebit, :ebcredit
  do begin
    if (exists(select first 1 period from turnoversd
                 where accounting = :accounting
                   and company = :company
                   and trndate = :trndate)) then
      update turnoversd
        set debit = coalesce(:debit, 0), credit = coalesce(:credit, 0), ebdebit = coalesce(:ebdebit, 0), ebcredit = coalesce(:ebcredit, 0)
        where accounting = :accounting and trndate = :trndate and company = :company;
    else
      insert into turnoversd(period,trndate,accounting,debit,credit,account,yearid,bktype,company,ebdebit,ebcredit)
        values (:period,:trndate,:accounting,coalesce(:debit,0),coalesce(:credit,0),:account,
                substring(:period from 1 for 4),:bktype,:company,:ebdebit,:ebcredit);

  end
end^
SET TERM ; ^
