--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COMPENSATORY2OVERTIME(
      EMPLOYEE integer,
      CALENDAR integer,
      FROMPERIOD varchar(6) CHARACTER SET UTF8                           ,
      TOPERIOD varchar(6) CHARACTER SET UTF8                           ,
      FROMDATE_IN varchar(10) CHARACTER SET UTF8                           ,
      TODATE_IN varchar(10) CHARACTER SET UTF8                           ,
      OVERTIME_TYPE smallint)
   as
  declare variable fromdate date;
  declare variable todate date;
  declare variable ref integer;
  declare variable overtime0 integer;
  declare variable overtime50 integer;
  declare variable overtime100 integer;
  declare variable compensatory_time integer;
  declare variable nighthours integer;
begin
  if (coalesce(char_length(fromperiod),0) = 6) then -- [DG] XXX ZG119346
  begin
    select fromdate from period2dates(:fromperiod) into :fromdate;
    select todate from period2dates(:toperiod) into :todate;
  end else begin
    fromdate = cast(fromdate_in as date);
    todate = cast(todate_in as date);
  end
  for
    select e.ref
      from emplcaldays e
      where e.employee = :employee
        and e.ecalendar = :calendar
        and e.cdate >= :fromdate
        and e.cdate <= :todate
        and e.compensatory_time - e.compensated_time > 0
      into :ref
  do begin
    overtime0=null; overtime50=null; overtime100=null; compensatory_time=null; nighthours=null;
    execute procedure count_overtime(:ref,:overtime_type)
      returning_values :overtime0, :overtime50, :overtime100, :compensatory_time, :nighthours;
    update emplcaldays c set c.overtime0 = :overtime0, c.overtime50 = :overtime50,
        c.overtime100 = :overtime100, c.compensatory_time = :compensatory_time, c.nighthours = :nighthours
      where c.ref = :ref;
  end
end^
SET TERM ; ^
