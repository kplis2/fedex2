--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ESECSTODAYSSTR(
      S integer)
  returns (
      STR varchar(20) CHARACTER SET UTF8                           )
   as
declare variable d int;
declare variable h int;
declare variable m int;
begin
  /* uzupełnienie pól ACTLIMITH, PREVLIMITH, LIMCONSH */
  d = s / 28800;  /*dni*/
  h = (s - :d*28800) / 3600; /*godziny*/
  m = (s - :d*28800 - :h*3600) / 60; /*minuty*/
  if (d>0 or (h=0 and m=0)) then
  begin
    str = d;
    if (h>0 or m>0) then str = str || '.';
  end
  else
    str = '';

  if( h>0 or m>0 ) then
    begin
      str = str || h;
    end
  if( m>0 ) then
    begin
      if( m < 10  ) then str = str || ':0' || m;
                    else str = str ||  ':' || m;
    end
  suspend;
end^
SET TERM ; ^
