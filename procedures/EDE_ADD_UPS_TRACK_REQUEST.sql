--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_ADD_UPS_TRACK_REQUEST(
      OTABLE varchar(31) CHARACTER SET UTF8                           ,
      OREF integer,
      TRACKOPTION smallint = 0)
  returns (
      REFDOK integer)
   as
declare variable EDERULE integer;
declare variable SPEDRESPONSE varchar(255);
begin
  select es.ref
    from ederules es
    where es.symbol = 'EDE_UPS_TRACK'
    into : ederule;

  if (:trackoption = 1) then
    otable = :otable||':ALL';
  else if (:trackoption = 8) then
    otable = :otable||':SIGN';

  insert into ededocsh( ederule, direction, createdate, otable, oref, status, manualinit,
      filename)
    values(:ederule, 1, current_timestamp, :otable, :oref, 0, 0,
      (case when :otable = 'LISTYWYS' then '0_' else '1_' end)||:oref||'track')
    returning ref into :refdok;

  suspend;
end^
SET TERM ; ^
