--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NEOS_DOKPLIKTYP(
      AKTUOPER integer)
  returns (
      REF integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      NAZWA varchar(40) CHARACTER SET UTF8                           ,
      OPIS varchar(255) CHARACTER SET UTF8                           ,
      SEGREGATOR integer,
      CONTRTYPEDEFAULT integer)
   as
declare variable grupy         varchar(1024);
declare variable administrator smallint;
begin
--exception test_break;
  aktuoper = coalesce(:aktuoper,0);
  if (:aktuoper = 0) then exit;
    select o.administrator, o.grupa from operator o where o.ref = :aktuoper
     into :administrator, :grupy;
  administrator = coalesce(:administrator,0);
  grupy = coalesce(:grupy,'');

  for select dt.ref, dt.symbol, dt.nazwa, dt.opis, dt.segregator, dt.contrtypedefault
    from dokpliktyp dt
    where dt.rights = ';' and dt.rightsgroup = ';'
         or dt.rights like '%;'||:aktuoper||';%'
         or strmulticmp(';'||:grupy||';', dt.rightsgroup) = 1
    into :ref, :symbol, :nazwa, :opis, :segregator, :contrtypedefault
    do suspend;
end^
SET TERM ; ^
