--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STKRMAG_CHECKCHANGED(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           )
  returns (
      CHANGED smallint)
   as
declare variable lastdata timestamp;
declare variable cnt integer;
declare variable gstkrtab integer;
begin
  changed = 1;
  select defmagaz.lastminmaxaktu from DEFMAGAZ where SYMBOL = :magazyn into :lastdata;
  if(:lastdata is null) then exit;
  select count(*) from STKRMAG where MAGAZYN = :magazyn and LASTCHANGE > :lastdata into :cnt;
  if(:cnt > 0) then exit;
  select count(*)
  from STKRPOZ join STKRMAG on (STKRMAG.STKRTAB = STKRPOZ.tabela)
  where STKRPOZ.lastdatachange > :lastdata  and STKRMAG.magazyn = :magazyn
  into :cnt;
  if(:cnt > 0) then exit;
  select DEFMAGAZ.stkrtab from DEFMAGAZ where SYMBOL = :magazyn into :gstkrtab;
  if(:gstkrtab > 0) then begin
    select count(*)
    from STKRPOZ
    where STKRPOZ.lastdatachange > :lastdata  and STKRPOZ.tabela = :gstkrtab
    into :cnt;
    if(:cnt > 0) then exit;
  end
  changed = 0;

end^
SET TERM ; ^
