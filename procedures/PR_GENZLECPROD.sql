--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_GENZLECPROD(
      NAGZAMREF integer,
      POZZAMREF integer,
      REJESTR varchar(10) CHARACTER SET UTF8                           ,
      TYP varchar(10) CHARACTER SET UTF8                           ,
      MAGAZYN varchar(10) CHARACTER SET UTF8                           ,
      HASOWNSUPPLY smallint)
  returns (
      ILWYGZAM integer)
   as
declare variable ktm varchar(80);
DECLARE VARIABLE SHEET INTEGER;
DECLARE VARIABLE WERSJAREF INTEGER;
DECLARE VARIABLE ZAM INTEGER;
DECLARE VARIABLE ILOSCZAM NUMERIC(14,4);
DECLARE VARIABLE GROSSQUANTITY NUMERIC(14,4);
DECLARE VARIABLE PLANSCOL INTEGER;
DECLARE VARIABLE PRSHMAT INTEGER;
DECLARE VARIABLE POZZAM INTEGER;
DECLARE VARIABLE ILOSCDOZAM NUMERIC(14,4);
DECLARE VARIABLE ILOSCJUZZAM NUMERIC(14,4);
DECLARE VARIABLE SHEETCUR INTEGER;
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE QUANTITY NUMERIC(14,4);
DECLARE VARIABLE BATCHQUANTITY NUMERIC(14,4);
DECLARE VARIABLE MAG2 VARCHAR(10);
DECLARE VARIABLE PRDEPART VARCHAR(20);
begin
  ilwygzam = 0;
  sheet = 0;
  grossquantity = 1;
  magazyn = 'CS';
  mag2='CG';
  rejestr='PAK';
  select wersjaref, ilosc from pozzam where ref = :pozzamref into :wersjaref, :ilosczam;
  select ref, batchquantity from prsheets where wersjaref = :wersjaref into :sheet,:batchquantity;
  if(:ilosczam < :batchquantity)then ilosczam = batchquantity;
  if(:sheet is null or :sheet = 0) then exception PRSHEETS_GENNAGZAM 'Nie można zidentyfikować domyslnej karty techn. dla towaru.';
  cnt =0;
  for
    select sheetcur, grossquantity
      from prsheets_sheetsdown(:sheet, 100)
      into :sheetcur, :grossquantity
  do begin
    execute procedure GEN_REF('NAGZAM') returning_values :zam;
    select ktm from prsheets where ref  = :sheetcur into :ktm;
    iloscdozam = :ilosczam * :grossquantity;
    SELECT p.quantity, p.prdepart from prsheets p where p.ref=:sheetcur into :quantity, :prdepart;
    if(quantity is null or (quantity = 0)) then quantity = 1;
    select sum(kilosc) from nagzam where kpopref = :pozzamref and kktm = :ktm into :iloscjuzzam;
    if(iloscjuzzam is null) then iloscjuzzam = 0;
    if(iloscjuzzam < iloscdozam) then begin
      iloscdozam = iloscdozam - iloscjuzzam;
      insert into NAGZAM(REF, REJESTR, TYPZAM, MAGAZYN, MAG2, DATAZM, DATAWE, KILOSC,
                         KKTM, KWERSJA, PRSHEET, PLANSCOL, TERMDOST, KPOPREF, KINNE,
                         PRLOT, PRDEPART)
      values(:zam, :rejestr, :typ, :magazyn, :mag2, current_date, current_date, :iloscdozam,
                         :ktm, 0, :sheetcur, :planscol, current_date, :pozzamref, 1,
                         1, :prdepart);
      ilwygzam = :ilwygzam + 1;
      /*dodanie pozycji*/
      for select PS.ref from PRSHMAT PS where PS.sheet = :sheetcur and PS.ktm is not null and
        PS.ktm <> ''
      into :prshmat
      do begin
        execute procedure GEN_REF('POZZAM') returning_values :pozzam;
        insert into POZZAM(REF, ZAMOWIENIE, KTM, WERSJA, JEDN, ILOSC, KILOSC)
        select :pozzam, :zam, KTM, WERSJA, JEDN, GROSSQUANTITY*:iloscdozam/:quantity, GROSSQUANTITY/:quantity
        from PRSHMAT where ref = :prshmat;
      end
    end
  end
end^
SET TERM ; ^
