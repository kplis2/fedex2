--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INWENTA_NALICZ_PRZECENY(
      PRZECENA integer,
      STANYZEROWE integer)
   as
declare variable data timestamp;
declare variable magazyn char(3);
declare variable wersjaref integer;
declare variable ktm varchar(30);
declare variable wersja integer;
declare variable cena numeric(14,2);
begin
  select data, magazyn from inwenta where ref = :przecena into :data, :magazyn;
  for
    select rktm, rwersja, rcena
        from MAG_STANY(:magazyn,NULL,NULL,NULL,NULL,:data,0,0)
        where (stan <> 0 or :stanyzerowe=1)
      into :ktm, :wersja, :cena
  do begin
    select ref from wersje where ktm = :ktm and nrwersji = :wersja into :wersjaref;
    if(not exists(select ref from inwentap where inwenta=:przecena and ktm=:ktm and wersja=:wersja)) then
    begin
      insert into inwentap (INWENTA, KTM, WERSJA, CENAPLUS, CENA, WERSJAREF)
        values (:przecena, :ktm, :wersja, :cena, :cena, :wersjaref);
    end
  end
end^
SET TERM ; ^
