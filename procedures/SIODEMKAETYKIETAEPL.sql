--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SIODEMKAETYKIETAEPL(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
  declare variable parent_lvl_0 integer;
  declare variable numerprzesylki string20;
begin
  select numerprzesylki from listywysd where ref = :oref
  into :numerprzesylki;
  parent_lvl_0 = 0;

  --Rozpoczynamy naglowek
  id = 0; --1
  name = 'wydrukEtykietaEPLElement';
  parent = null;
  params = null;
  val  = null;
  parent_lvl_0 = :id;
  suspend;
    id = :id + 1;
    name = 'numery';
    parent = parent_lvl_0;
    params = null;
    val  = :numerprzesylki;
    suspend;
    id = :id + 1;
    name = 'klucz';
    parent = parent_lvl_0;
    params = null;
    val  = 'E85BF1498DF3D6C77CA955E2A9621557';
    suspend;
    id = :id + 1;
    name = 'separator';
    parent = parent_lvl_0;
    params = null;
    val  = ' ';
    suspend;
end^
SET TERM ; ^
