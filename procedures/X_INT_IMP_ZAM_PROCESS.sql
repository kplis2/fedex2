--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_INT_IMP_ZAM_PROCESS(
      SESJAREF SESJE_ID,
      TABELA STRING35,
      REF INTEGER_ID,
      BLOKUJZALEZNE SMALLINT_ID = 0,
      TYLKONIEPRZETWORZONE SMALLINT_ID = 0,
      AKTUALIZUJSESJE SMALLINT_ID = 1)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable stabela string35;
declare variable tmptabela string35;
declare variable sprocedura string255;
declare variable szrodlo zrodla_id;
declare variable skierunek smallint_id;
declare variable sesja sesje_id;
declare variable sql memo;
declare variable tmpsql memo;
declare variable tmpstatus smallint_id;
declare variable tmpmsg string255;
declare variable error smallint_id;
declare variable errormsg string255;
declare variable error2 smallint_id;
declare variable tmperror smallint_id;
declare variable errormsg2 string255;
declare variable statuspoz smallint_id;
--TOWAR
declare variable T_REF INTEGER_ID;
declare variable T_TOWARID STRING120;
declare variable T_SYMBOL STRING255;
declare variable T_NAZWA STRING255;
declare variable T_GRUPA STRING255;
declare variable T_GRUPAID STRING120;
declare variable T_RODZAJ STRING255;
declare variable T_RODZAJID STRING120;
declare variable T_AKTYWNY INTEGER_ID;
declare variable T_VAT STRING10;
declare variable T_OPIS MEMO;
declare variable T_CENAZAKUPUNET  KURS_ID;
declare variable T_CENAZAKUPUBRU  KURS_ID;
declare variable T_CHODLIWY INTEGER_ID;
declare variable T_WITRYNA INTEGER_ID;
declare variable T_HASH STRING255;
declare variable T_DEL INTEGER_ID;
declare variable T_REC STRING255;
declare variable T_SKADTABELA STRING40;
declare variable T_SKADREF INTEGER_ID;
--towar zmienne pomocnicze
declare variable T_TMP_TOWTYPE SMALLINT_ID; --nie zmieniac na domene towtype_id !!!!
declare variable T_TMP_NAZWA TOWARY_NAZWA;
--JEDNOSTKA
declare variable J_REF INTEGER_ID;
declare variable J_TOWARID STRING120;
declare variable J_JEDNOSTKA STRING60;
declare variable J_JEDNOSTKAID STRING120;
declare variable J_GLOWNA INTEGER_ID;
declare variable J_LICZNIK NUMERIC_14_4;
declare variable J_MIANOWNIK NUMERIC_14_4;
declare variable J_WAGA NUMERIC_14_4;
declare variable J_WYSOKOSC NUMERIC_14_4;
declare variable J_DLUGOSC NUMERIC_14_4;
declare variable J_SZEROKOSC NUMERIC_14_4;
declare variable J_HASH STRING255;
declare variable J_DEL INTEGER_ID;
declare variable J_REC STRING255;
declare variable J_SKADTABELA STRING40;
declare variable J_SKADREF INTEGER_ID;
--jednostka zmienne pomocnicze
declare variable J_TMP_PRZELICZ NUMERIC_14_4;
declare variable J_GLOWNASENTE SMALLINT_ID;
--KOD KRESKOWY
declare variable K_REF INTEGER_ID;
declare variable K_TOWARID STRING120;
declare variable K_JEDNOSTKA STRING60;
declare variable K_JEDNOSTKAID STRING120;
declare variable K_KODKRESKOWY STRING255;
declare variable K_GLOWNY INTEGER_ID;
declare variable K_HASH STRING255;
declare variable K_DEL INTEGER_ID;
declare variable K_REC STRING255;
declare variable K_SKADTABELA STRING40;
declare variable K_SKADREF INTEGER_ID;
--kod kreskowy zmienne pomocnicze
--NAGLOWEK ZAMOWIENIA
declare variable NZ_REF INTEGER_ID;
declare variable NZ_NAGID STRING120;
declare variable NZ_SYMBOL STRING255;
declare variable NZ_TYP STRING20;
declare variable NZ_DATAZAM TIMESTAMP_ID;
declare variable NZ_WYDANIA INTEGER_ID;
declare variable NZ_ZEWNETRZNY INTEGER_ID;
declare variable NZ_BN CHAR_1;
declare variable NZ_MAGAZYN STRING20;
declare variable NZ_MAGAZYN2 STRING20;
declare variable NZ_PARTIA STRING255;
declare variable NZ_PARTIAID STRING120;
declare variable NZ_ZAPLATA STRING255;
declare variable NZ_ZAPLATAID STRING120;
declare variable NZ_DOSTAWA STRING255;
declare variable NZ_DOSTAWAID STRING120;
declare variable NZ_WALUTA STRING20;
declare variable NZ_KURS KURS_ID;
declare variable NZ_UWAGIWEW MEMO;
declare variable NZ_UWAGIZEW MEMO;
declare variable NZ_DATAWYSYLKI TIMESTAMP_ID;
declare variable NZ_WARTOSCNETTO KURS_ID;
declare variable NZ_WARTOSCBRUTTO KURS_ID;
declare variable NZ_WARTOSCNETTOZL KURS_ID;
declare variable NZ_WARTOSCBRUTTOZL KURS_ID;
declare variable NZ_DOZAPLATY KURS_ID;
declare variable NZ_RABATWYLICZ INTEGER_ID;
declare variable NZ_RABATPROCENT CENY;
declare variable NZ_RABATWARTOSC KURS_ID;
declare variable NZ_BLOKADA CHAR_1;
declare variable NZ_TYPDOKFAK STRING20;
declare variable NZ_TYPDOKMAG STRING20;
declare variable NZ_STATUSZAM STRING60;
declare variable NZ_HASH STRING255;
declare variable NZ_DEL INTEGER_ID;
declare variable NZ_REC STRING255;
--zamowienie zmienne pomocnicze
declare variable nz_tmp_typdok defdokum_id;

    /*ZAMOWIENIE       NAGZAM_ID,
    DOKUMENT         DOKUMNAG_ID,
    KLIENT           KLIENCI_ID,
    ODBIORCA         ODBIORCY_ID,
    KLIENTHIST       INTEGER_ID,
    DOSTAWCA         DOSTAWCA_ID
    */
--zmienne ogolne
declare variable KTM KTM_ID;
declare variable WERSJAREF WERSJE_ID;
declare variable JEDN JEDN_MIARY;
declare variable TOWJEDN TOWJEDN;
declare variable SPOSZAP PLATNOSC_ID;
declare variable SPOSDOST SPOSDOST_ID;
declare variable KLIENT KLIENCI_ID;
declare variable ODBIORCA ODBIORCY_ID;
declare variable DOSTAWCA DOSTAWCA_ID;
declare variable DOKUMENT DOKUMNAG_ID;
declare variable ODDZIAL ODDZIAL_ID;
declare variable COMPANY COMPANIES_ID;
declare variable DOSTAWA DOSTAWA_ID;
declare variable OPERATOR OPERATOR_ID;
declare variable SPRZEDAWCA SPRZEDAWCA_ID;
begin

  --zmiana pustych wartosci na null-e
  if (sesjaref = 0) then
    sesjaref = null;
  if (trim(tabela) = '') then
    tabela = null;
  if (ref = 0) then
    ref = null;
  if (blokujzalezne is null) then
    blokujzalezne = 0;
  if (tylkonieprzetworzone is null) then
    tylkonieprzetworzone = 0;
  if (aktualizujsesje is null) then
    aktualizujsesje =1;

  status = 1;

  --sprawdzenie czy wszystkie potrzebne informacje sa
  if (sesjaref is null and ref is null) then
    begin
      status = -1;
      msg = 'Za malo parametrow.';
      exit; --EXIT
    end

  if (sesjaref is null) then
    begin
      if (ref is null) then
        begin
          status = -1;
          msg = 'Jesli nie podales sesji to wypadaloby podac ref-a...';
          exit; --EXIT
        end
      if (tabela is null) then
        begin
          status = -1;
          msg = 'Wypadaloby podac nazwe tabeli...';
          exit; --EXIT
        end


      sql = 'select sesja from '||:tabela||' where ref='||:ref;
      execute statement sql into :sesja;

    end
  else
    sesja = sesjaref;

  if (sesja is null) then
    begin
      status = -1;
      msg = 'Nie znaleziono numeru sesji.';
      exit; --EXIT
    end

  --pobranie parametrow sesji
  select s.tabela, s.procedura, s.zrodlo, s.kierunek
    from int_sesje s
    where ref = :sesja
    into :stabela, :sprocedura, :szrodlo, :skierunek;

  if (tabela is not null) then --jesli podano tabele na wejsciu traktuje ja nadrzednie
    stabela = tabela;


  --wlasciwe przetworzenie

  oddzial = null;
  company = null;
  select oddzial, company from int_zrodla where ref = :szrodlo
    into :oddzial, :company;
  if (oddzial is null) then
    oddzial = 'CENTRALA';
  if (company is null) then
    company = 1;


  if (stabela = 'INT_IMP_ZAM_NAGLOWKI') then
    begin
      for select ref, nagid, symbol, typ, datazam, wydania,
              zewnetrzny, bn, magazyn, magazyn2, partia, partiaid,
              zaplata, zaplataid, dostawa, dostawaid,
              waluta, kurs, uwagiwew, uwagizew, datawysylki,
              wartoscnetto, wartoscbrutto, wartoscnettozl, wartoscbruttozl,
              dozaplaty, rabatwylicz, rabatprocent, rabatwartosc,
              blokada, typdokfak, typdokmag, statuszam,
              --"hash",
              del, rec
            from INT_IMP_ZAM_NAGLOWKI t
            where sesja = :sesja and (ref = :ref or :ref is null)
            order by ref
            into :nz_ref, :nz_nagid, :nz_symbol, :nz_typ, :nz_datazam, :nz_wydania,
              :nz_zewnetrzny, :nz_bn, :nz_magazyn, :nz_magazyn2, :nz_partia, :nz_partiaid,
              :nz_zaplata, :nz_zaplataid, :nz_dostawa, :nz_dostawaid,
              :nz_waluta, :nz_kurs, :nz_uwagiwew, :nz_uwagizew, :nz_datawysylki,
              :nz_wartoscnetto, :nz_wartoscbrutto, :nz_wartoscnettozl, :nz_wartoscbruttozl,
              :nz_dozaplaty, :nz_rabatwylicz, :nz_rabatprocent, :nz_rabatwartosc,
              :nz_blokada, :nz_typdokfak, :nz_typdokmag, :nz_statuszam,
              --:nz_hash,
              :nz_del, :nz_rec
        do begin
          error = 0;
          errormsg = '';
          error2 = 0;
          errormsg2 = '';
          tmperror = null;

          if (coalesce(trim(nz_nagid),'') = '') then
            begin
              error = 1;
              errormsg = errormsg || 'Brak ID naglowka.';
            end

          if (exists (select first 1 1 from dokumnag dn
                where dn.int_zrodlo = :szrodlo and dn.int_id = :nz_nagid)
             ) then
            begin
              error = 1;
              errormsg = errormsg || 'W grupie znajduje sie niezaakceptowany dokument.';
            end

          if (exists (select first 1 1 from listywysdpoz lp left join dokumpoz dp
              on lp.doktyp = 'M' and lp.dokref = dp.dokument and lp.dokpoz = dp.ref
              left join dokumnag dn on dp.ref = dp.dokument
              where dn.int_zrodlo = :szrodlo and dn.int_id = :nz_nagid)
             ) then
            begin
              error = 1;
              errormsg = errormsg || 'Dokumenty sa w trakcie pakowania/spakowane.';
            end

          if (coalesce(nz_del,0) = 1 and error = 0) then --rekord do skasowania
            begin
              --select status, msg from INT_IMP_ZAM_NAGLOWKI_DEL(nz_nagid)
                --into :tmpstatus, :tmpmsg;
              tmpstatus = null;
            end
          else if (error = 0 and coalesce(nz_del,0) = 0) then
          begin

            if (coalesce(trim(nz_zaplata),'') = '' and coalesce(trim(nz_zaplataid),'') = '') then
              begin
                error = 1;
                errormsg = errormsg || 'Brak sposobu zaplaty.';
              end
            else
              begin
               --sposob zaplaty
               select first 1 ref from platnosci into :sposzap; --!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
               error = 0;
              end

            if (coalesce(trim(nz_dostawa),'') = '' and coalesce(trim(nz_dostawaid),'') = '') then
              begin
                error = 1;
                errormsg = errormsg || 'Brak sposobu dostawy.';
              end
            else
              begin
              --sposob dostawy
              select first 1 ref from sposdost into :sposdost; --!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
              error = 0;
              end


            if (error = 0) then
              begin
                
                blokujzalezne = 1; --!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                if (blokujzalezne = 0) then --tabele zalezne BEFORE
                  begin
                    /*select status, msg from int_tabelezalezne_process(:sesja,
                        :stabela, :szrodlo, :skierunek, 0)
                      into :tmpstatus, :tmpmsg; */
                  end
                  

                --nullowanie, zerowanie i blankowanie - naglowek zamowienia
                if (coalesce(trim(nz_symbol),'') = '') then
                  nz_symbol = null;
                if (coalesce(trim(nz_typ),'') = '') then
                  nz_typ = 'ZAM';
                if (nz_wydania is null) then
                  nz_wydania = 1;
                if (nz_zewnetrzny is null) then
                  nz_zewnetrzny = 1;
                if (coalesce(trim(nz_bn),'') = '') then
                  nz_bn = 'N';
                if (coalesce(trim(nz_magazyn),'') = '') then
                  nz_magazyn = 'MWS';
                if (coalesce(trim(nz_magazyn2),'') = '') then
                  nz_magazyn2 = null;
                if (coalesce(trim(nz_partia),'') = '') then
                  nz_partia = null;
                if (coalesce(trim(nz_partiaid),'') = '') then
                  nz_partiaid = null;
                if (coalesce(trim(nz_waluta),'') = '') then
                  nz_waluta = 'PLN';
                if (coalesce(trim(nz_uwagiwew),'') = '') then
                  nz_uwagiwew = null;
                if (coalesce(trim(nz_uwagizew),'') = '') then
                  nz_uwagizew = null;
                if (nz_dozaplaty is null) then
                  nz_dozaplaty = 0;
                if (coalesce(trim(nz_blokada),'') = '') then
                  nz_blokada = null;
                if (coalesce(trim(nz_typdokfak),'') = '') then
                  nz_typdokfak = null;
                if (coalesce(trim(nz_typdokmag),'') = '') then
                  nz_typdokmag = null;
                if (coalesce(trim(nz_statuszam),'') = '') then
                  nz_statuszam = null;

                if (not exists (select first 1 1 from defmagaz where symbol = :nz_magazyn)) then
                  nz_magazyn = 'MWS';
                if (nz_magazyn2 is not null and
                    not exists (select first 1 1 from defmagaz where symbol = :nz_magazyn)
                   ) then
                  nz_magazyn2 = 'MWS';

                if (not exists(select first 1 1 from defdokummag dm where dm.magazyn = :nz_magazyn
                     and dm.typ = :nz_typdokmag)) then
                  begin
                    nz_tmp_typdok = null;

                    select first 1 dd.symbol from defdokummag ddm join defdokum dd
                        on ddm.typ = dd.symbol
                      where dd.symbol = :nz_magazyn and dd.wydania = :nz_wydania
                        and dd.zewn = :nz_zewnetrzny and dd.koryg = 0
                      order by case when dd.symbol in ('WZ', 'PZ', 'RW', 'PW') then 0
                        else 1 end
                      into :nz_tmp_typdok;
                  end
                else
                  nz_tmp_typdok = nz_typdokmag;

                update int_imp_zam_naglowki set klient_sente = :klient,
                    odbiorca_sente = :odbiorca,
                    /*klienthist_sente,*/ dostawca_sente = :dostawca,
                    sposzap_sente = :sposzap, sposdost_sente = :sposdost,
                    magazyn_sente = :nz_magazyn, mag2_sente = :nz_magazyn2,
                    typ_sente = :nz_typ,
                    data_sente = coalesce(data_sente, current_date),
                    dostawa_sente = :dostawa, operator_sente = :operator,
                    oddzial_sente = :oddzial, company_sente = :company,
                    walutowy_sente = case when :nz_waluta = 'PLN' then 0 else 1 end,
                    kurs_sente = :nz_kurs, waluta_sente = :nz_waluta,
                    typdokvat_sente = :nz_typdokfak, typdokmag_sente = :nz_typdokmag,
                    sprzedawca_sente = :sprzedawca, bn_sente = :nz_bn,
                    termdost_sente = :nz_datawysylki,
                    dataostprzetw = case when :aktualizujsesje = 0 then dataostprzetw else current_timestamp(0) end
                  where ref = :nz_ref;

                if (exists (select first 1 1 from dokumnag dn
                     where dn.int_zrodlo = :szrodlo and dn.int_id = :nz_nagid) --UPDATE naglowkow
                    ) then
                  begin
                    update dokumnag set klient = :klient, odbiorca = :odbiorca,
                        /*klienthist_sente,*/ dostawca = :dostawca,
                        sposzap = :sposzap, sposdost = :sposdost,
                        magazyn = :nz_magazyn, mag2 = :nz_magazyn2,
                        /*dostawa = :dostawa,*/ operator = :operator,
                        oddzial = :oddzial, company = :company,
                        walutowy = case when :nz_waluta = 'PLN' then 0 else 1 end,
                        kurs = :nz_kurs, waluta = :nz_waluta,
                        typdokvat = :nz_typdokfak, /*typdokmag = :nz_typdokmag,*/
                        sprzedawca = :sprzedawca, bn = :nz_bn,
                        termdost = :nz_datawysylki
                      where int_zrodlo = :szrodlo and int_id = :nz_nagid;

                  end 
          end

        end
      end
    end

  --aktualizacja informacji o sesji
  if (aktualizujsesje = 1) then
    begin
      select status, msg from int_sesje_aktualizuj(:sesja, 1) into :tmpstatus, :tmpmsg;
    end


  suspend;
end^
SET TERM ; ^
