--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TEST_PR84610(
      NAME STRING,
      DEADLOCK_TIME SMALLINT_ID)
  returns (
      HELLO STRING)
   as
declare variable operator_ref integer_id;
declare variable now timestamp_id;
begin
  /* Procedure Text */
  operator_ref = (select first 1 o.ref from operator o order by o.ref);
  update operator o set o.nazwisko = o.nazwisko where  o.ref = :operator_ref;
  now = current_timestamp;

  while((datediff(second from :now to cast('NOW' as timestamp))<:deadlock_time))
  do
  begin

  end
  hello = 'Witaj '||name;
  suspend;
end^
SET TERM ; ^
