--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STANYARCH_CHANGE(
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      WH varchar(3) CHARACTER SET UTF8                           ,
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERSNUMBER integer,
      DOKUMNOTP integer,
      DOCDATE timestamp,
      QUANTITY numeric(14,4),
      VAL numeric(14,4),
      DELIVERY integer,
      KORROZDATE date = null)
   as
declare variable stanarch integer;
declare variable quantityb numeric(14,4);
declare variable quantityc numeric(14,4);
declare variable valb numeric(14,4);
declare variable avalb numeric(14,4);
begin
  if(val is null) then val = 0;
  if (doctype = 'N') then
  begin -- jesli to nota magazynowa to znajdz rekord do modyfikacji wartosci
    quantityc = 0;
    if (korrozdate is null) then
      select cast(p.dokumnagdate as date), p.dostawa
        from dokumnotp p
        where p.ref = :dokumnotp
        into korrozdate, :delivery;
    if (not exists(select ref from stanyarch where data = cast(:korrozdate as date)
        and ktm = :good and wersja = :versnumber and magazyn = :wh and dostawa = :delivery)) then
      exception STANYARCH_INVALID;
  end
  else
    quantityc = quantity;

  DOCDATE = cast(DOCDATE as date);
  if (not exists(select ref from stanyarch where data = cast(:docdate as date)
        and ktm = :good and wersja = :versnumber and magazyn = :wh and dostawa = :delivery)) then
  begin
    -- jezeli nie istnieje to trzeba sprawdzic jaki jest ostatni wpis dla tego towaru na magazynie
    select first 1 ref
      from stanyarch
      where data < cast(:docdate as date)
        and ktm = :good
        and wersja = :versnumber
        and magazyn = :wh
        and dostawa = :delivery
      order by data desc
      into stanarch;
    if (stanarch is not null) then
      select stan, wartosc, kwartosc
        from stanyarch
        where ref = :stanarch
        into QUANTITYB, VALB, AVALB;
    if (quantityb is null) then quantityb = 0;
    if (valb is null) then valb = 0;
    if (avalb is null) then avalb = 0;
    if (val is null) then
      exception STANYARCH_INVALID;
    insert into stanyarch (magazyn, ktm, wersja, data, stan, wartosc, kwartosc, dostawa)
      values (:wh, :good, :versnumber, :docdate, :quantityb + :quantityc, :valb + :val, :avalb + :val, :delivery);
  end else
  begin
    update stanyarch set stan = stan + :quantityc, wartosc = wartosc + :val, kwartosc = kwartosc + :val
      where cast(data as date) = cast(:docdate as date)
        and ktm = :good
        and magazyn = :wh
        and wersja = :versnumber
        and dostawa = :delivery;
  end
  -- zmiana wartosci w przod gdy zmienila sie ilosc i wartosc to aktualizujemy wszystkie wpisy kolejne
  update stanyarch set stan = stan + :quantityc, wartosc = wartosc + :val, kwartosc = kwartosc + :val
      where data > cast(:docdate as date)
        and ktm = :good
        and magazyn = :wh
        and wersja = :versnumber
        and dostawa = :delivery;
  if (doctype = 'N') then
    update stanyarch set wartosc = wartosc + :val
      where data >= cast(:korrozdate as date)
        and data < cast(:docdate as date)
        and ktm = :good
        and magazyn = :wh
        and wersja = :versnumber
        and dostawa = :delivery;
end^
SET TERM ; ^
