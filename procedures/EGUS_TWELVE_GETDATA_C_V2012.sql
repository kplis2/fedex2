--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EGUS_TWELVE_GETDATA_C_V2012(
      GYEAR varchar(4) CHARACTER SET UTF8                           ,
      EMPLOYEE integer)
  returns (
      C1 numeric(14,2),
      C2 numeric(14,2),
      C3 numeric(14,2),
      C4 numeric(14,2),
      C5 numeric(14,2),
      C6 numeric(14,2),
      C7 numeric(14,2),
      C8 numeric(14,2),
      C9 numeric(14,2),
      C10 numeric(14,2),
      C11 numeric(14,2),
      C12 numeric(14,2),
      C13 numeric(14,2))
   as
declare variable fromdate date;
declare variable todate date;
declare variable period varchar(6);
declare variable stdemplcal integer;
declare variable hourpay numeric(14,2);
declare variable addnominalpay numeric(14,2);
begin
/*MWr Personel: procedura generuje dane do sprawozdania GUS Z-12
  dla zadanego pracownika EMPLOYEE. Odpowiada za czesc C raportu - czas pracy
  oraz wynagr. za pazdzernik roku GYEAR. Specyfikacja zm. wyjsc. w dokumenacji*/

--okreslenie zmiennych pomocniczych
  period = :gyear || '10';
  execute procedure period2dates(period)
    returning_values :fromdate, :todate;

/*C1: miesieczny wskaznik wymiaru czasu pracy za pazdziernik oraz*/
  select wdimdays /(1.00 * nomworkdays), stdemplcal
    from egus_twelve_worktime(1, :employee, :fromdate, :todate)
    into :c1, :stdemplcal;  --pobranie kalendarza wg ktorego bedzie rozliczany pracownik

--C2: tygodniowa liczba godzin obowiazkowego czasu pracy
  select norm_per_week /3600.00 from ecalendars
    where ref = :stdemplcal
    into :c2;
  c2 = coalesce(c2,40.00);

/*C3: czas faktycznie przepracowny w godz nominalnych,
  C4: czas faktycznie przepracowny w godz nadliczbowych
  C5: czas nieprzepracowany ogolem,
  C6: czas nieprzepracowany oplacony tylko przez zaklad pracy (C5 >= C6)*/
  select workedhours, extrahours, notworkedhours, notwhours_works
    from egus_twelve_worktime(2, :employee, :fromdate, :todate)
    into :c3, :c4, :c5, :c6;

--WYNAGRODZENIA ZA PAZDZIERNIK -------------------------------------------------
  select sum(case when p.ecolumn in (920,930,940,960) then p.pvalue else 0 end) --dodatkowe wynagr. nominl. do C8
    , sum(case when p.ecolumn = 900 then p.pvalue else 0 end) --C9: wynagrodzenie zasad. brutto (nominalne)
    , sum(case when p.ecolumn = 910 then p.pvalue else 0 end) --stawka za godzine, aby wyliczyc C9 dla godzinowych
    , 0 --C10: dodatek za prace zmianowa (za czas nominalny) [BRAK W SYSTEMIE]
    , 0 --C11: tylko premia REGULAMINOWA (np 1/3 premi za kwartal) [BRAK W SYSTEMIE]
    , sum(case when p.ecolumn in (1200, 1210) then p.pvalue else 0 end) --C12: wyn. za prace w godz. nadliczowych
    from epayrolls r
      join eprpos p on (p.payroll = r.ref and r.cper = :period and r.empltype = 1)
    where p.employee = :employee
    into :addnominalpay, :c9, :hourpay, :c10, :c11, :c12;

   c9 = c9 + (hourpay * (c3 + c5 /*czas nominalny w godz*/));

   c8 = addnominalpay + c9 + c10 + c11; --wynagrodzenie osobowe brutto za czas nominalny

   c7 = c8 + c12; --wynagr. osobowe ogolem brutto

   c13 = 0.0;
  suspend;
end^
SET TERM ; ^
