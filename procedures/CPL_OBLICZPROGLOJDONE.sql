--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CPL_OBLICZPROGLOJDONE(
      STABLE varchar(31) CHARACTER SET UTF8                           ,
      SREF integer)
   as
declare variable proced varchar(255);
declare variable cnt integer;
declare variable proglojdone smallint;
begin
  proced = 'select PROGLOJDONE from '||:stable||' where ref='||:sref;
  execute statement :proced into :proglojdone;
  if(:proglojdone is null or (:proglojdone <> 2))then begin
    /*czy istnieje regula dla aktywnych prog. lojalnosciowych tego typu niezablokowana,
      ktora nei naliczyla punktow dla tego dokumentu*/
    select count(*)
    from cplreguly
       join cpl on (cpl.ref = cplreguly.progloj and cpl.status > 0)
       left join cploper on (cploper.cplregula = cplreguly.ref and cploper.typdok = :stable and cploper.refdok = :sref)
    where cplreguly.typdok = :stable and cplreguly.autooblicz > 0  and cploper.ref is null
    into :cnt;
    if(:cnt > 0) then
      proced = 'update '||:stable||' set PROGLOJDONE=0 where PROGLOJDONE = 1 and REF='||:sref;
    else
      proced = 'update '||:stable||' set PROGLOJDONE=1 where PROGLOJDONE = 0 and REF='||:sref;
    execute statement :proced;
  end
end^
SET TERM ; ^
