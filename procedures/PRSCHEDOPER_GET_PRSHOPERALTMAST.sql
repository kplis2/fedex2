--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDOPER_GET_PRSHOPERALTMAST(
      PRSHOPER integer,
      PRSHOPERALTMASTERCUR integer)
  returns (
      PRSHOPERALTMASTER integer)
   as
declare variable prshopermaster integer;
declare variable prshoperaltmastertocur integer;
declare variable complex smallint;
declare variable opertype smallint;
begin
  prshopermaster = null;
  select opermaster, complex, opertype
    from prshopers
    where ref = :prshoper
  into :prshopermaster, :complex, :opertype;
  if(complex = 1 and opertype = 1) then prshoperaltmastertocur = :prshoper;
  else prshoperaltmastertocur = prshoperaltmastercur;
  if(prshopermaster is not null) then
    execute procedure prschedoper_get_prshoperaltmast(:prshopermaster, :prshoperaltmastertocur)
      returning_values :prshoperaltmaster;
  else prshoperaltmaster = prshoperaltmastertocur;
  suspend;
end^
SET TERM ; ^
