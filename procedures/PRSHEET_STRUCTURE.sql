--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHEET_STRUCTURE(
      PRSHEET integer,
      TREESUBSHEET smallint,
      TREEZAKRES smallint)
  returns (
      NAMEOUT varchar(255) CHARACTER SET UTF8                           ,
      REFOUT varchar(1024) CHARACTER SET UTF8                           ,
      REFOUTUP varchar(1024) CHARACTER SET UTF8                           ,
      PIKTOGRAM integer,
      STATEINDEX integer,
      NUMBEROUT integer,
      TAG integer)
   as
declare variable ref integer;
declare variable opermaster integer;
declare variable complex integer;
declare variable opertype integer;
declare variable number integer;
declare variable name varchar(255);
declare variable lastrefout varchar(1024);
declare variable lastopermaster integer;
declare variable subsheet integer;
declare variable prshtoolmaster integer;
declare variable maxoper integer;
declare variable maxmat integer;
declare variable maxtool integer;
declare variable maxref integer;
declare variable lastsubsheet varchar(1024);
begin
  /* zakladamy ze root 0,0 jest juz zalozony*/
  /* zwroc wszystkie operacje do karty */
  lastrefout = null;
  lastopermaster = -1;
  if(:treezakres<>1) then begin
    for select REF,OPERMASTER,NUMBER,DESCRIPT,COMPLEX,OPERTYPE
      from prshopers
      where sheet=:prsheet
      order by opermaster,number
      into :ref,:opermaster,:number,:name,:complex,:opertype
    do begin
      if(:lastopermaster = -1) then begin
        /* zwroc naglowek o nazwie operacje */
        tag = 0;
        nameout = 'Operacje';
        refout = :prsheet||'/O'||:ref;
        lastrefout = :refout;
        refoutup = null;
        piktogram = 1;
        stateindex = -1;
        numberout = 0;
        lastopermaster = 0;
        suspend;
      end
      tag = :ref;
      nameout = :name;
      refout = :prsheet||'/o'||:ref;
      if(:opermaster is null) then refoutup = :lastrefout;
      else refoutup = :prsheet||'/o'||:opermaster;
      piktogram = 1;
      stateindex = 1;
      numberout = :number;
      if(:complex>0) then begin
        if(:opertype=0) then begin
          nameout = :nameout || ' (S)';
          stateindex = 5;
        end else if(:opertype=1) then begin
          nameout = :nameout || ' (A)';
          stateindex = 6;
        end else if(:opertype=2) then begin
          nameout = :nameout || ' (R)';
          stateindex = 7;
        end
      end
      suspend;
    end
  end

  /* zwroc wszystkie materialy do karty */
  lastrefout = null;
  lastopermaster = -1;
  for select REF,coalesce(OPERMASTER,0),NUMBER,DESCRIPT,SUBSHEET
    from prshmat
    where sheet=:prsheet
    order by opermaster,number
    into :ref,:opermaster,:number,:name,:subsheet
  do begin
    if(:treezakres<>1 and :opermaster<>:lastopermaster) then begin /* zwroc naglowek dla materialow */
      tag = 0;
      nameout = 'Materiały';
      refout = :prsheet||'/M'||:ref;
      lastrefout = :refout;
      if(:opermaster is null) then refoutup = null;
      else refoutup = :prsheet||'/o'||:opermaster;
      piktogram = 2;
      stateindex = -1;
      numberout = 0;
      lastopermaster = :opermaster;
      suspend;
    end
    tag = :ref;
    nameout = :name;
    refout = :prsheet||'/m'||:ref;
    refoutup = :lastrefout;
    piktogram = 2;
    stateindex = 2;
    if(:subsheet is not null) then stateindex = 4;
    numberout = :number;
    suspend;
    /* rekurencyjnie zwroc karty podrzdne*/
    if(:treesubsheet=1 and :subsheet is not null) then begin
      lastsubsheet = :refout;
      for select NAMEOUT,REFOUT,REFOUTUP,PIKTOGRAM,STATEINDEX,NUMBEROUT,TAG
        from PRSHEET_STRUCTURE(:subsheet,:treesubsheet,:treezakres)
        into :NAMEOUT,:REFOUT,:REFOUTUP,:PIKTOGRAM,:STATEINDEX,:NUMBEROUT,:TAG
      do begin
        refout = :prsheet||';'||:refout;
        if(:refoutup is null) then refoutup = :lastsubsheet;
        else refoutup = :prsheet||';'||:refoutup;
        suspend;
      end
    end
  end


  /* zwroc wszystkie narzedzia do karty */
  if(:treezakres<>1) then begin
    lastrefout = null;
    lastopermaster = -1;
    numberout = 0;
    for select REF,coalesce(OPERMASTER,0),PRSHTOOLMASTER,SYMBOL
      from prshtools
      where sheet=:prsheet
      order by opermaster,prshtoolmaster,symbol
      into :ref,:opermaster,:prshtoolmaster,:name
    do begin
      if(:opermaster<>:lastopermaster) then begin /* zwroc naglowek dla narzedzi */
        tag = 0;
        nameout = 'Narzędzia';
        refout = :prsheet||'/T'||:ref;
        lastrefout = :refout;
        if(:opermaster is null) then refoutup = null;
        else refoutup = :prsheet||'/o'||:opermaster;
        piktogram = 3;
        stateindex = -1;
        numberout = :numberout + 1;
        lastopermaster = :opermaster;
        suspend;
      end
      tag = :ref;
      nameout = :name;
      refout = :prsheet||'/t'||:ref;
      if(:prshtoolmaster is null) then begin
        refoutup = :lastrefout;
      end else begin
        refoutup = :prsheet||'/t'||:prshtoolmaster;
      end
      piktogram = 3;
      stateindex = 3;
      numberout = :numberout + 1;
      suspend;
    end
  end
end^
SET TERM ; ^
