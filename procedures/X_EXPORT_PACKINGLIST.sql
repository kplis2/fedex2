--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_EXPORT_PACKINGLIST(
      LISTWYSD INTEGER_ID)
  returns (
      KOD_SES STRING100,
      KTM KTM_ID,
      NAZWA TOWARY_NAZWA,
      WERSJAOPIS STRING100,
      OPAKOWANIE STRING20,
      OPAKOWANIE_RODZIC STRING20,
      ILOSC NUMERIC_14_4,
      MIARA JEDN_MIARY,
      WAGA NUMERIC_14_4,
      SZEROKOSC NUMERIC_14_4,
      DLUGOSC NUMERIC_14_4,
      WYSOKOSC NUMERIC_14_4)
   as
  declare variable wersjaref integer_id;
begin
  for
    select t.x_kod_towaru_ses, l.ktm, t.nazwa, lwp.wersjaref, lo.stanowisko, lo.rodzic, sum(l.iloscmag), lwp.miarao, sum(tj.waga), lo.x_szerokosc, lo.x_wysokosc, lo.x_dlugosc
      from listywysdroz l
        join towary t on (t.ktm = l.ktm)
        join listywysdroz_opk lo on (lo.ref = l.opk)
        join listywysdpoz lwp on (lwp.ref = l.listywysdpoz)
        join towjedn tj on (tj.ktm = l.ktm and tj.glowna = 1)
      where l.listywysd = :listwysd
      group by  t.x_kod_towaru_ses, l.ktm, t.nazwa, lwp.wersjaref, lo.stanowisko, lo.rodzic, lwp.miarao, lo.x_szerokosc, lo.x_wysokosc, lo.x_dlugosc
      order by lo.stanowisko, lo.rodzic, l.ktm
    into :kod_ses, :ktm, :nazwa, :wersjaref, :opakowanie, :opakowanie_rodzic, :ilosc, :miara, :waga, :szerokosc, :wysokosc, :dlugosc
  do begin
    if (opakowanie_rodzic is null) then
      opakowanie_rodzic = '';
    else
      select first 1 lo.stanowisko
        from listywysdroz_opk lo
        where lo.listwysd = :listwysd and lo.ref = :opakowanie_rodzic
      into :opakowanie_rodzic;
    if (wersjaref is null) then
      wersjaopis = '';
    else
      select w.opis
        from wersje w
        where w.ref = :wersjaref
      into wersjaopis;
    if (opakowanie is null) then opakowanie = '';
    if (kod_ses is null) then kod_ses = '';
    if (waga is null) then waga = 0;
    if (dlugosc is null) then dlugosc = 0;
    if (wysokosc is null) then wysokosc = 0;
    if (szerokosc is null) then szerokosc = 0;

    suspend;
  end
end^
SET TERM ; ^
