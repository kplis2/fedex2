--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_ALLTRACKING_RESPONSE(
      EDEDOCSH_REF integer)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
   as
declare variable shippingdoc integer;
declare variable shippingno varchar(40);
declare variable packageno varchar(40);
declare variable datestr varchar(20);
declare variable statuscode varchar(20);
declare variable statuscodedesc varchar(40);
declare variable descript memo;
declare variable stationcode varchar(20);
begin
  select oref
    from ededocsh
    where ref = :ededocsh_ref
  into :shippingdoc;

  select symbolsped from listywysd where ref = :shippingdoc
  into :shippingno;

  select p.val
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.name = 'nrP'
  into :packageno;

  select p.val
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.name = 'dataS'
  into :datestr;

  select p.val
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.name = 'skrot'
  into :statuscode;

  if (:statuscode = 'DS') then statuscodedesc = 'Przesyłka dostarczona';
  else if (:statuscode = 'WE') then statuscodedesc = 'Przyjęte do punktu FEDEX';
  else if (:statuscode = 'WK') then statuscodedesc = 'Przesyłka pobrana przez kuriera';
  else statuscodedesc = 'Brak opisu kodu statusu';

  select substring(p.val from 1 for 1024)
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.name = 'opis'
  into :descript;

  select p.val
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.name = 'oddSymbol'
  into :stationcode;

  update or insert into listywysdstatusy(listwysd, symbolsped, opksymbolsped, data, kod, kodopis, opis, kodpunktu)
    values(:shippingdoc,:shippingno,:packageno,cast(:datestr as timestamp),:statuscode,:statuscodedesc,:descript,:stationcode)
    matching(listwysd,kod);
  suspend;
end^
SET TERM ; ^
