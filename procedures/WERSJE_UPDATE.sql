--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WERSJE_UPDATE as
declare variable ktm VARchar(40);
declare variable nrwersji integer;
declare variable reff integer;
begin
  for
    select ktm, nrwersji, ref
      from wersje
      order by ktm
      into :ktm, :nrwersji, :reff
  do begin

    update TOWPLIKI set WERSJAREF=:reff where KTM = :ktm AND WERSJA=:nrwersji;
    update CENNIK set WERSJAREF=:reff where KTM=:ktm AND WERSJA=:nrwersji;
    update POZZAM set WERSJAREF=:reff where KTM=:ktm AND WERSJA=:nrwersji;
    update POZFAK set WERSJAREF=:reff where KTM=:ktm AND WERSJA=:nrwersji;
    update DOKUMPOZ set WERSJAREF=:reff where KTM=:ktm AND WERSJA=:nrwersji;
    update STANYIL  set WERSJAREF=:reff where KTM=:ktm and wersja=:nrwersji;
    update STANYCEN set WERSJAREF=:reff where KTM=:ktm and wersja=:nrwersji;
    update PROMOCT set WERSJAREF=:reff where KTM=:ktm and wersja=:nrwersji;
    update PROMOCJE set WERSJAREF=:reff where KTM=:ktm and wersja=:nrwersji;
    update ATRYBUTY set WERSJAREF=:reff where KTM=:ktm and nrwersji=:nrwersji;

  end
end^
SET TERM ; ^
