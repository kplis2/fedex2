--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CREATE_AMPERIODS(
      COMPANY integer,
      AMYEAR smallint,
      AMMONTH smallint)
   as
declare variable ref integer;
  declare variable i smallint;
  declare variable name varchar(20);
  declare variable i_year smallint;
begin
  -- parametr AMMONTH mówi o tym od którego miesiąca ma sie zaczynać rok obrachunkowy
  select max(ref) from amperiods where company = :company
    into :ref;
  if (ref is not null) then
  begin
    -- tutaj ma być sprawdzanie poprzednich lat i na podstawie ich danych zalozenie kolejnego
    select amyear, ammonth from amperiods where ref = :ref
      into :amyear, :ammonth;
    ammonth = ammonth + 1;
    if (ammonth > 12) then
    begin
      ammonth = 1;
      amyear = amyear + 1;
    end

  end

  i = 1;
  i_year = amyear;
  while (i <= 12) do
  begin
    execute procedure get_month_name(ammonth)
      returning_values :name;
    name = cast(i_year as varchar(4)) || ' - ' || name;
    insert into amperiods (company, amyear, ammonth, number, name, status)
      values (:company, :amyear, :ammonth, :i, :name, 0);
    ammonth = ammonth + 1;
    if (ammonth > 12) then
    begin
      ammonth = 1;
      i_year = i_year + 1;
    end
    i = i + 1;
  end
end^
SET TERM ; ^
