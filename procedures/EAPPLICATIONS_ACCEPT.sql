--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EAPPLICATIONS_ACCEPT(
      REF_EAPP integer)
   as
declare variable EPERADDR integer;
declare variable PERSON integer;
declare variable EPERCHANGE integer;
declare variable FNAME varchar(60);
declare variable SNAME varchar(30);
declare variable EVIDENO varchar(10);
declare variable PASSPNO varchar(11);
declare variable PESEL varchar(11);
declare variable NIP varchar(15);
declare variable BIRTHDATE timestamp;
declare variable ADDRTYPE smallint;
declare variable STATUS smallint;
declare variable CITY varchar(30);
declare variable POST varchar(30);
declare variable POSTCODE varchar(10);
declare variable STREET varchar(30);
declare variable HOUSENO varchar(20);
declare variable LOCALNO varchar(20);
declare variable PHONE varchar(20);
declare variable CPWOJ16M integer;
declare variable POWIAT varchar(40);
declare variable STPREF varchar(10);
declare variable COMMUNITYID integer;
declare variable POWIATID integer;
declare variable AREF integer;
declare variable DESCRIPT STRING1024;
declare variable OLDACC varchar(26);
declare variable NEWACC varchar(26);
declare variable ACCOUNT varchar(52) = '';
declare variable EMPLOYEE EMPLOYEES_ID;
begin

  select epersaddr, eperchange, status, employee,
        case when akind in (8,9) then descript else '' end --wnioski dotyczace rach. bank.
    from eapplications
    where ref = :ref_eapp
    into :eperaddr, :eperchange, status, :employee, :descript;

  if (status > 0) then
    exception universal 'Wniosek został juz rozpatrzony!';

  if (eperchange is not null) then
  begin
  --Aktualizacja danych w tabeli persons na podstawie wpisu w eperchange
    select person, fname, sname, evidenceno, passportno, pesel, nip, birthdate
      from eperchanges
      where ref = :eperchange
      into :person, :fname, :sname, :evideno, :passpno, :pesel, :nip, :birthdate;

    update persons set fname = :fname, sname = :sname, evidenceno = :evideno,
        passportno = :passpno, pesel = :pesel, nip = :nip, birthdate = :birthdate
      where ref = :person
        and (fname is distinct from :fname or sname is distinct from :sname
          or evidenceno is distinct from :evideno or passportno is distinct from :passpno
          or pesel is distinct from :pesel or nip is distinct from :nip
          or birthdate is distinct from :birthdate);
  end else

  if (eperaddr is not null) then
  begin
  /*Aktualizacja adresu - nalezy pobrac dane adresowe z wniosku i nadpisac nimi
    biezacy adres przy jednoczesnym ustawieniu statusu na 2 (w ten sposob adres
    biezacy trafi automatem do historii) */
    select person, addrtype, city, post, postcode, street, houseno,
           localno, phone, cpwoj16m, powiat, stpref, communityid, powiatid
      from epersaddr
      where ref = :eperaddr
      into :person, :addrtype, :city, :post, :postcode, :street, :houseno,
           :localno, :phone, :cpwoj16m, :powiat, :stpref, :communityid, :powiatid;

    update epersaddr set city = :city, post = :post, postcode = :postcode,
        street = :street, houseno = :houseno, localno = :localno, phone = :phone,
        cpwoj16m = :cpwoj16m, powiat = :powiat, stpref = :stpref,
        communityid = :communityid, powiatid = :powiatid, status = 2
      where status = 1 and person = :person and addrtype = :addrtype
      returning ref into aref;

    if (aref is null) then begin
      insert into epersaddr (person, addrtype, city, post, postcode, street, houseno,
          localno, phone, cpwoj16m, powiat, stpref, communityid, powiatid, fromdate, status)
        values (:person, :addrtype, :city, :post, :postcode, :street, :houseno, :localno,
          :phone, :cpwoj16m, :powiat, :stpref, :communityid, :powiatid, current_date, 1);
    end
  end else

  if (descript <> '') then
  begin
  --Aktualizacja rachunku bankowego (PR55109)

    execute procedure efunc_get_numbers_from_string(:descript,' ', '', 0)
      returning_values :descript;

    for
      select stringout from parsestring(:descript, ' ')
        where char_length(stringout) = 26
        order by lp desc
        into :account
    do begin
      if (newacc is null) then newacc = account;
      else oldacc = account;
    end

    if (coalesce(account,'') <> '') then
      execute procedure bankaccouts_update(oldacc, newacc, employee);
  end

  update eapplications set status = 1 where ref = :ref_eapp;

end^
SET TERM ; ^
