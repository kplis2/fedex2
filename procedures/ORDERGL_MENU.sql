--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ORDERGL_MENU(
      RODZIC integer,
      SYMBOL varchar(32) CHARACTER SET UTF8                           )
  returns (
      NUMERRET integer)
   as
declare variable numergl integer;
declare variable numer integer;
declare variable r integer;
begin
   select numergl from MENU where REF=:RODZIC AND SYMBOL=:SYMBOL into :numergl;
    if(:numergl is null) then numergl = 0;
    for select numer, ref from MENU where RODZIC = :RODZIC AND SYMBOL = :SYMBOL
       order by numer,ord into :numer, :r
    do begin
      numergl = :numergl + 1;
      update MENU set NUMERGL = :numergl where REF=:r;
      execute procedure ORDERGL_MENU(:r,:symbol) returning_values :numergl;
    end
   numerret = :numergl;
end^
SET TERM ; ^
