--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PX_OVERVIEW(
      DOCNAGREF integer)
  returns (
      REF integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      ILOSC numeric(14,4),
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      MIARAT varchar(5) CHARACTER SET UTF8                           ,
      LP varchar(6) CHARACTER SET UTF8                           ,
      POZREF integer)
   as
declare variable lpp integer;
begin
        lpp=0;

        for select d.ref, d.ktm, d.ilosc, w.nazwat, w.miara, d.ref from dokumpoz d
         left join wersje w on w.ref=d.wersjaref
         where d.dokument = :docnagref
         into :ref,  :ktm, :ilosc, :nazwa, :miarat, :pozref do
         begin
            lpp=lpp+1;
            lp=lpp||'.';
           suspend;
         end
end^
SET TERM ; ^
