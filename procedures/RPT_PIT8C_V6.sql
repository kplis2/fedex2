--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PIT8C_V6(
      PERSON_INPUT integer,
      CURRENTCOMPANY integer,
      YEARID char(4) CHARACTER SET UTF8                           ,
      CORRECT smallint,
      SIGNFNAME STRING255,
      SIGNSNAME STRING255,
      EDECLREFS STRING8191 = '')
  returns (
      P01 STRING100,
      P02 STRING255,
      P03 integer,
      P04 char(4) CHARACTER SET UTF8                           ,
      P05 STRING20,
      P05NAME STRING255,
      P06 integer,
      P07 integer,
      P08 STRING100,
      P09 STRING100,
      P10LEN integer,
      P10 STRING100,
      P11 STRING100,
      P12 STRING100,
      P13 STRING10,
      P14 STRING100,
      P15 STRING100,
      P16 STRING100,
      P17 STRING100,
      P18 STRING100,
      P19 STRING100,
      P20 STRING100,
      P21 STRING100,
      P22 STRING100,
      P23 STRING100,
      P24 varchar(255) CHARACTER SET UTF8                           ,
      P25 numeric(14,2),
      P26 varchar(255) CHARACTER SET UTF8                           ,
      P27 numeric(14,2),
      P28 varchar(255) CHARACTER SET UTF8                           ,
      P29 numeric(14,2),
      P30 numeric(14,2),
      P31 numeric(14,2),
      P32 numeric(14,2),
      P33 numeric(14,2),
      P34 numeric(14,2),
      P35 numeric(14,2),
      P36 numeric(14,2),
      P37 numeric(14,2),
      P38 numeric(14,2),
      P39 numeric(14,2),
      P40 numeric(14,2),
      P41 numeric(14,2),
      P42 numeric(14,2),
      P43 numeric(14,2),
      P44 numeric(14,2),
      P45 numeric(14,2),
      P46 numeric(14,2),
      P47 numeric(14,2),
      P48 numeric(14,2),
      P49 numeric(14,2),
      P50 STRING255,
      P51 STRING255,
      P53 STRING255,
      P54 STRING255,
      PERSON PERSONS_ID,
      BTYPE varchar(50) CHARACTER SET UTF8                           ,
      BVALUE numeric(14,2))
   as
declare variable I integer;
declare variable ZAS_CHOR numeric(14,2);
declare variable ZAS_MAC numeric(14,2);
declare variable ZAS_OPK numeric(14,2);
declare variable SWI_REH numeric(14,2);
declare variable ZAS_WYP numeric(14,2);
declare variable NIP STRING20;
declare variable PESEL STRING20;
declare variable BUSINESSACTIV smallint;
declare variable NAME STRING255;
declare variable FIELD INTEGER_ID;
declare variable VAL STRING255;
declare variable EDECLREF EDECLARATIONS_ID;
begin
--Personel: generowanie danych do PITu-8C w wersji 6 i 7 (wydruk i e-deklaracja)

  if (edeclrefs <> '') then
  begin
  -- Pobieranie danych do wydruku e-deklaracji -----------------------------------
    edeclrefs = ',' || :edeclrefs || ',';
    for
      select distinct e.ref, e.refid, e.status from edeclarations e
        join edecldefs d on (e.edecldef = d.ref)
        where (:edeclrefs containing ',' || e.ref || ','
            or :edeclrefs containing ',' || e.declgroup || ',')
          and d.isgroup = 0
          and e.person is not null
          and d.symbol = 'PIT-8C'
          and d.variant in (6,7)
        into :edeclref, :p02, :p03 --BS70284 
    do begin

      p01 = null;    p04 = null;    p05 = null;    p05name = null; p06 = null;
      p07 = null;    p08 = null;    p09 = null;    p10len = null;  p10 = null;
      p11 = null;    p12 = null;    p13 = null;    p14 = null;     p15 = null;
      p16 = null;    p17 = null;    p18 = null;    p19 = null;     p20 = null;
      p21 = null;    p22 = null;    p23 = null;    p24 = null;     p25 = null;
      p26 = null;    p27 = null;    p28 = null;    p29 = null;     p30 = null;
      p31 = null;    p32 = null;    p33 = null;    p34 = null;     p35 = null;
      p36 = null;    p37 = null;    p38 = null;    p39 = null;     p40 = null;
      p41 = null;    p42 = null;    p43 = null;    p44 = null;     p45 = null;
      p46 = null;    p47 = null;    p48 = null;    p49 = null;     p50 = null;
      p51 = null;    p53 = null;    p54 = null;

      for select field, pvalue
          from edeclpos
          where edeclaration = :edeclref
          into :field, :val
      do begin
        if (field = 1) then p01 = val;
        else if (field = 4) then p04 = val;
        else if (field = 5) then
        begin
          p05 = val;
          select first 1 name || ',' || address || ','|| city || ' ' || postcode
            from einternalrevs
            where code = :p05
            into :p05name;
        end
        else if (field = 6) then p06 = val;
        else if (field = 7) then p07 = val;
        else if (field = 8) then p08 = val;
        else if (field = 9) then p09 = val;
        else if (field = 10) then
        begin
          p10 = val;
          p10len = char_length(trim(replace(replace(p10,'-',''),' ','')));
        end
        else if (field = 11) then p11 = val;
        else if (field = 12) then p12 = val;
        else if (field = 13) then p13 = val;
        else if (field = 14) then --BS70284
        begin
          select name from countries where symbol = :val
            into :p14;
          p14 = coalesce(p14,val);
        end
        else if (field = 15) then p15 = val;
        else if (field = 16) then p16 = val;
        else if (field = 17) then p17 = val;
        else if (field = 18) then p18 = val;
        else if (field = 19) then p19 = val;
        else if (field = 20) then p20 = val;
        else if (field = 21) then p21 = val;
        else if (field = 22) then p22 = val;
        else if (field = 23) then p23 = val;
        else if (field = 24) then p24 = val;
        else if (field = 25) then p25 = val;
        else if (field = 26) then p26 = val;
        else if (field = 27) then p27 = val;
        else if (field = 28) then p28 = val;
        else if (field = 29) then p29 = val;
        else if (field = 30) then p30 = val;
        ------------------------------------
        else if (field = 50) then p50 = val;
        else if (field = 51) then p51 = val;
        ------------------------------------
      end
      suspend;
    end
  end
  else
  begin
-- Generowanie danych do wydruku badz do e-deklaracji --------------------------
  
    /* Pola przygotowane pod przyszla obsluge, drukowane na szablonie:
    P31 = 0;   P32 = 0;   P33 = 0;   P34 = 0;   P35 = 0;   P36 = 0;   P37 = 0;
    P38 = 0;   P39 = 0;   P40 = 0;   P41 = 0;   P42 = 0;   P43 = 0;   P44 = 0;
    P45 = 0;   P46 = 0;   P47 = 0;   P48 = 0;   P49 = 0; */
  
    if (currentcompany > 0) then
    begin
      p04 = yearid;
      p06 = correct + 1;
      p50 = signfname;
      p51 = signsname;
  
      -- Czesc 'B' ---------------------------------------------------------------
      select nip, phisical, descript
        from e_get_platnikinfo4pit
        into :p01, :p07, :name;
      if (p07 = 1) then p08 = name;
      else if (p07 = 2) then p09 = name;
  
      if (person_input = 0) then person_input = null;
  
      for select distinct p.ref, p.sname, p.fname, f.dateout, p.nationality,
                          replace(p.nip,'-','') as nip, p.pesel
        from epayrolls pr
          join eprpos pp on (pr.ref = pp.payroll)
          join employees e on (pp.employee = e.ref)
          join persons p on (e.person = p.ref)
          left join efunc_get_format_date(p.birthdate) f on (1 = 1)
        where pr.tper starting with :yearid
          and pr.company = :currentcompany
          and e.company = :currentcompany
          --and pr.lumpsumtax = 0
          --and pp.pvalue <> 0
          and ((pp.ecolumn in (4100, 4150, 4200, 4350, 4450, 4500) -- zasilki
                and :person_input is null
                and pr.empltype = 2)
               or e.person = :person_input)
        order by e.personnames
        into :person, :p11, :p12, :p13, :p14, :nip, :pesel
      do begin
        -- Pobranie informacji podatkowych ---------------------------------------
        select uscode, businessactiv, taxoffice
          from e_get_perstaxinfo4pit(:yearid, :person, 0 ,:currentcompany)
          into :p05, :businessactiv, :p05name;
  
        if (businessactiv = 1) then
        begin
          p10 = :nip;
          p10len = 10;
        end
        else
        begin
          p10 = :pesel;
          p10len = 11;
        end
  
        -- Pobranie informacji o adresie podatnika -------------------------------
        select cpwoj, district, community, street, home_nr, local_nr, city, post_code, post
          from get_persaddress(-:person, null, 1, '102')
          into :p15, :p16, :p17, :p18, :p19, :p20, :p21, :p22, :p23;
    
        -- Wypelnianie pol czesci 'D' --------------------------------------------
        P24 = '';
        P25 = null;
        P26 = '';
        P27 = null;
        P28 = '';
        P29 = null;
  
        i = 0;
        for select btype, bvalue
          -- Wywolanie rekurencyjne z currentcompany < 0
          from rpt_pit8c_v6(:person, -:currentcompany, :yearid, :correct, :signfname, :signsname)
          into :btype, :bvalue
        do begin
          if (i = 0) then
          begin
            P24 = btype;
            P25 = bvalue;
          end
          else if (i = 1) then
          begin
            P26 = btype;
            P27 = bvalue;
          end
          else if (i = 2) then
          begin
            P28 = btype;
            P29 = bvalue;
          end
          else if (i >= 3) then
          begin
            P28 = P28 || '; ' || btype;
            P29 = P29 + bvalue;
          end
          i = i + 1;
        end
  
        P30 = coalesce(P25,0) + coalesce(P27,0) + coalesce(P29,0);
        if (P30 = 0) then P30 = null;
  
        suspend;
  
        /* Pola przygotowane pod przyszla obsluge, drukowane na szablonie:
        P45 = coalesce(P35,0) + coalesce(P37,0) + coalesce(P39,0) + coalesce(P41,0) + coalesce(P43, 0);
        P46 = coalesce(P36,0) + coalesce(P38,0) + coalesce(P40,0) + coalesce(P42,0) + coalesce(P44, 0);
        P47 = coalesce(P45,0) - coalesce(P46,0);
        P48 = coalesce(P46,0) - coalesce(P45,0);
        P33 = coalesce(P31,0) - coalesce(P32,0);
        if (P31 = 0) then P31 = null;
        if (P32 = 0) then P32 = null;
        if (P33 = 0) then P33 = null;
        if (P34 = 0) then P34 = null;
        if (P35 = 0) then P35 = null;
        if (P36 = 0) then P36 = null;
        if (P37 = 0) then P37 = null;
        if (P38 = 0) then P38 = null;
        if (P39 = 0) then P39 = null;
        if (P40 = 0) then P40 = null;
        if (P41 = 0) then P41 = null;
        if (P42 = 0) then P42 = null;
        if (P43 = 0) then P43 = null;
        if (P44 = 0) then P44 = null;
        if (P45 = 0) then P45 = null;
        if (P46 = 0) then P46 = null;
        if (P47 = 0) then P47 = null;
        if (P48 = 0) then P48 = null;
        if (P49 = 0) then P49 = null; */
      end
    end
    else if (currentcompany < 0) then
    begin
    -- Naliczanie wartosci do wypelnienia czesci 'D' -----------------------------
  
      currentcompany = -currentcompany;
  
      select sum(case when p.ecolumn in (4100,4200) then p.pvalue else 0 end),
             sum(case when p.ecolumn = 4150 then p.pvalue else 0 end),
             sum(case when p.ecolumn = 4350 then p.pvalue else 0 end),
             sum(case when p.ecolumn = 4450 then p.pvalue else 0 end),
             sum(case when p.ecolumn = 4500 then p.pvalue else 0 end)
        from epayrolls r
          join eprpos p on (r.ref = p.payroll)
          join employees e on (e.ref = p.employee)
        where p.ecolumn in (4100, 4150, 4200, 4350, 4450, 4500)
          and r.tper starting with :yearid
          and e.person = :person_input
          and e.company = :currentcompany
          and r.company = :currentcompany
          and r.empltype = 2
        into :zas_chor, :zas_wyp, :zas_mac, :zas_opk, :swi_reh;
  
      if (zas_chor is not null) then
      begin
        if (zas_chor <> 0) then
        begin
          btype = 'Zasiłek chorobowy';
          bvalue = zas_chor;
          suspend;
        end
        if (zas_wyp <> 0) then
        begin
          btype = 'Zasiłek wypadkowy';
          bvalue = zas_wyp;
          suspend;
        end
        if (zas_mac <> 0) then
        begin
          btype = 'Zasiłek macierzyński';
          bvalue = zas_mac;
          suspend;
        end
        if (zas_opk <> 0) then
        begin
          btype = 'Zasiłek opiekuńczy';
          bvalue = zas_opk;
          suspend;
        end
        if (swi_reh <> 0) then
        begin
          btype = 'Świadczenie rehabilitacyjne';
          bvalue = swi_reh;
          suspend;
        end
      end
    end
  end
end^
SET TERM ; ^
