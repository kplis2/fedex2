--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SETTLEBADDEBIT(
      KORYGOWANY BKDOCS_ID)
  returns (
      RET smallint)
   as
declare variable SUMA MONEY;
declare variable VATPOS BKVATPOS_ID;
declare variable NEWCORR BKDOCS_ID;
declare variable VTRANSDATE TIMESTAMP_ID;
declare variable VVATREG varchar(10);
declare variable COMPANY COMPANIES_ID;
declare variable PERIOD INTEGER_ID;
declare variable CORRTYPE BKDOCTYPES_ID;
declare variable DOCTYPE BKDOCTYPES_ID;
begin
  -- Procedura przeglądająca korekty zlych dugów danego bkdocs
  -- sprawdza, czy wszystko sie bilansuje do zero, jezeli nie,
  -- to zaklada korekte i dopiera takie pozycje, zeby zbilansowalo sie do zera.
  newcorr = null; -- bo nie wiemy czy oplaca sie zakladac nowa korekte bo moze sie bilansuje
  for
    select sum(p.netv), p.bkvatposvatcorrenctionref
      from bkvatpos p join bkdocs b on (p.bkdoc = b.ref)
      where b.bkdocsvatcorrectionref = :korygowany and b.status > 0
      group by p.bkvatposvatcorrenctionref
      into :suma, :vatpos
    do begin
      if(suma <> 0) then
      begin
      -- tutaj wejdziemy jeżeli któraj pozycja sie nie bilansuje i nalezy dobrac
      -- pozycje bilanusjącą
        if( newcorr is null ) then
        begin
        -- Sytuacja, gdzie pierwsza pozcja nie jest zbilansowana, wiec jeszcze nie
        -- ma nowej korekty i trzeba ją zalożyć

        -- Wiec sciagam parametry potrzebne do zalozenia kolejnej korekty
          select bk.transdate, bk.vatreg, bk.doctype
            from bkdocs bk
            where bk.ref = :korygowany
            into :vtransdate, :vvatreg, :doctype;
        -- Ściągamy aktualny okres
          execute procedure get_global_param('CURRENTCOMPANY')
            returning_values :company;
          execute procedure date2period(current_timestamp(0),:company)
            returning_values :period;
        -- Ściągamy domyln typ korekty
          select bkt.corrbkdoctypes
            from bkdoctypes bkt
            where bkt.ref = :doctype
            into :corrtype;
        -- Procedura zakadająca orekte - na triggerach zalożą, sie zerowe pozycje.
          execute procedure newbkdoc_korekta(:korygowany,:corrtype,:period, :vtransdate,:vvatreg,null,1)
            returning_values :newcorr;
        -- Dla pewnosci sprawdzamy, czy udalo sie zalozyc korekte
          if (:newcorr is null) then
            exception universal'Błąd podczas zakładania korekty VAT';
        end
      end
      -- Obecna pozycja sie nie bilansuje i na pewno mamy już nową korekte
      -- rozliczającą wiec wystarczy uzupelnic wartosci w jej pozycjach
        update bkvatpos bkp
          set  bkp.netv = -(:suma)
          where bkp.bkvatposvatcorrenctionref = :vatpos and bkp.bkdoc = :newcorr;
        vatpos = null;
        suma = 0;
    end
    if (newcorr is null) then
      ret = 0;
    else
      ret = 1;
  suspend;
end^
SET TERM ; ^
