--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_MOVE_MATERIALS(
      PRNAGZAMFROM integer,
      PRPOZZAMFROM integer,
      CANCELED smallint,
      PRNAGZAMTO integer,
      PRPOZZAMTO integer)
   as
DECLARE VARIABLE PREFFROM INTEGER;
DECLARE VARIABLE PREFTO INTEGER;
DECLARE VARIABLE MREFFROM INTEGER;
DECLARE VARIABLE MREFTO INTEGER;
DECLARE VARIABLE NEWGPREF INTEGER;
DECLARE VARIABLE PRSHMAT INTEGER;
DECLARE VARIABLE GPREFFROM INTEGER;
DECLARE VARIABLE GPREFTO INTEGER;
DECLARE VARIABLE GREFTO INTEGER;
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE VERSTO INTEGER;
DECLARE VARIABLE PRSHMATTO INTEGER;
DECLARE VARIABLE QUANTITY NUMERIC(14,4);
DECLARE VARIABLE QUANTITYTO NUMERIC(14,4);
DECLARE VARIABLE OVERLIMIT SMALLINT;
DECLARE VARIABLE STOP SMALLINT;
BEGIN
  IF (PRPOZZAMFROM = 0) THEN PRPOZZAMFROM = NULL;
  IF (PRPOZZAMTO = 0) THEN PRPOZZAMTO = NULL;
  IF (CANCELED IS NULL) THEN CANCELED = 0;
  FOR
    SELECT P.REF
      FROM POZZAM P
      WHERE P.ZAMOWIENIE = :PRNAGZAMFROM AND P.OUT = 0 AND P.ANULOWANO = :CANCELED
        AND (P.REF = :PRPOZZAMFROM OR :PRPOZZAMFROM IS NULL)
      INTO PREFFROM
  DO BEGIN
    FOR
      SELECT PR.REF, P.PRSHMAT, P.WERSJAREF
        FROM PR_POZZAM_CASCADE(:PREFFROM,0,0,0,1) PR
          LEFT JOIN POZZAM P ON (P.REF = PR.REF)
        WHERE PR.PRNAGZAMOUT = :PRNAGZAMFROM AND P.OUT = 1
        INTO MREFFROM, PRSHMAT, VERSTO
    DO BEGIN
      FOR
        SELECT G.REF, G.QUANTITY, G.OVERLIMIT
          FROM PRSCHEDGUIDESPOS GP
            LEFT JOIN PRSCHEDGUIDEDETS G ON (G.PRSCHEDGUIDEPOS = GP.REF)
          WHERE GP.POZZAMREF = :MREFFROM AND G.QUANTITY > 0 AND GP.PRSHMAT = :PRSHMAT
          ORDER BY G.OVERLIMIT DESC
          INTO GPREFFROM, QUANTITY, OVERLIMIT
      DO BEGIN
        STOP = 0;
        CNT = 0;
        WHILE (QUANTITY > 0 AND STOP = 0)
        DO BEGIN
          FOR
            SELECT P.REF
              FROM POZZAM P
              WHERE P.ZAMOWIENIE = :PRNAGZAMTO AND P.OUT = 0 --and p.anulowano = 0
                AND (P.REF = :PRPOZZAMTO OR :PRPOZZAMTO IS NULL)
              INTO PREFTO
          DO BEGIN
            MREFTO = NULL;
            SELECT FIRST 1 PR.REF, P.PRSHMAT, G.REF, G.AMOUNT - G.AMOUNTZREAL, G.PRSCHEDGUIDE
              FROM PR_POZZAM_CASCADE(:PREFTO,0,0,0,1) PR
                LEFT JOIN POZZAM P ON (P.REF = PR.REF)
                LEFT JOIN PRSCHEDGUIDESPOS G ON (G.POZZAMREF = P.REF)
              WHERE PR.PRNAGZAMOUT = :PRNAGZAMTO AND P.OUT = 1
                AND G.AMOUNT - G.AMOUNTZREAL > 0 AND P.WERSJAREF = :VERSTO
              ORDER BY G.AMOUNT - G.AMOUNTZREAL
              INTO MREFTO, PRSHMATTO, GPREFTO, QUANTITYTO, GREFTO;
            IF (MREFTO > 0) THEN
              BREAK;
          END
          IF (MREFTO IS NULL) THEN
            STOP = 1;
          IF (STOP = 0) THEN
          BEGIN
            IF (OVERLIMIT = 1 AND QUANTITY > 0) THEN
            BEGIN
              UPDATE PRSCHEDGUIDEDETS S SET S.PRSHMAT = :PRSHMATTO, S.PRNAGZAM = :PRNAGZAMTO,
                  S.PRPOZZAM = :MREFTO, S.PRSCHEDGUIDE = :GREFTO, S.PRSCHEDGUIDEPOS = :GPREFTO
                WHERE S.REF = :GPREFFROM;
              QUANTITY = 0;
            END ELSE IF (QUANTITY > 0) THEN
            BEGIN
              IF (QUANTITY > QUANTITYTO) THEN
              BEGIN
                -- sklonowanie rozpiski pierwotnej, bo trzeba przepisac mniej surowca
                EXECUTE PROCEDURE PRSCHEDGUIDEDET_CLONE(:GPREFFROM,:QUANTITYTO,0) RETURNING_VALUES NEWGPREF;
                UPDATE PRSCHEDGUIDEDETS S SET S.PRSHMAT = :PRSHMATTO, S.PRNAGZAM = :PRNAGZAMTO, S.PRPOZZAM = :MREFTO,
                    S.PRSCHEDGUIDE = :GREFTO, S.PRSCHEDGUIDEPOS = :GPREFTO
                  WHERE S.REF = :NEWGPREF;
              END
              ELSE
              BEGIN
                QUANTITYTO = QUANTITY;
                UPDATE PRSCHEDGUIDEDETS S SET S.PRSHMAT = :PRSHMATTO, S.PRNAGZAM = :PRNAGZAMTO, S.PRPOZZAM = :MREFTO,
                    S.PRSCHEDGUIDE = :GREFTO, S.PRSCHEDGUIDEPOS = :GPREFTO
                  WHERE S.REF = :GPREFFROM;
              END
              QUANTITY = QUANTITY - QUANTITYTO;
            END
          END
          CNT = CNT + 1;
          IF (CNT >= 1000) THEN
            EXCEPTION UNIVERSAL 'za duzo iteracji';
        END
       /* for
          select p.ref
            from pozzam p
            where p.zamowienie = :prnagzamto and p.out = 0 --and p.anulowano = 0
              and (p.ref = :prpozzamto or :prpozzamto is null)
            into prefto
        do begin
          for
            select pr.ref, g.ref, g.amount - g.amountzreal, g.prschedguide
              from PR_POZZAM_CASCADE(:prefto,0,0,0,1) pr
                left join pozzam p on (p.ref = pr.ref)
                left join prschedguidespos g on (g.pozzamref = p.ref)
              where pr.prnagzamout = :prnagzamto and p.out = 1 and p.prshmat = :prshmat
                and g.amount - g.amountzreal > 0
              into mrefto, gprefto, quantityto, grefto
          do begin
            if (overlimit = 1 and quantity > 0) then
            begin
              update prschedguidedets s set s.prnagzam = :prnagzamto, s.prpozzam = :mrefto, s.prschedguide = :grefto
                where s.ref = :gpreffrom;
              quantity = 0;
            end else if (quantity > 0) then
            begin
              if (quantity > quantityto) then
              begin
                -- sklonowanie rozpiski pierwotnej, bo trzeba przepisac mniej surowca
                execute procedure prschedguidedet_clone(:gpreffrom,:quantityto,0) returning_values newgpref;
              end
              else
                quantityto = quantity;
              update prschedguidedets s set s.prnagzam = :prnagzamto, s.prpozzam = :mrefto,
                  s.prschedguide = :grefto, s.prschedguidepos = :gprefto
                where s.ref = :gpreffrom;
              quantity = quantity - quantityto;
            end
          end
        end  */
      END
    END
  END
END^
SET TERM ; ^
