--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_GLOBAL_PARAM(
      PNAME varchar(30) CHARACTER SET UTF8                           )
  returns (
      PVALUE varchar(255) CHARACTER SET UTF8                           )
   as
declare variable id integer;
declare variable sid varchar(40);
declare variable neosid neos_id;
begin
  sid = null;
  neosid=null;
  -- najierw ustalmy connectionid i neosid ze zmiennych kontekstowych
  select rdb$get_context('USER_SESSION','CONNECTIONID') from rdb$database into :sid;
  select rdb$get_context('USER_SESSION','NEOSID') from rdb$database into :neosid;
  -- jesli w zmiennych tego nie ma, to po staremu szukamy w globalparamsach
  if(:sid is null and :neosid is null) then begin
    select pvalue from globalparams
      where connectionid=current_connection and psymbol='CONNECTIONID'
      into :sid;

    select pvalue from globalparams
      where connectionid=current_connection and psymbol='NEOSID'
      into :neosid;
  end

  -- Pusty lub -1 tzn. nie zostalo ustanowione polaczenie (patrz SubsystemWorkingContextID).
  if(:sid is null or :sid='' or :sid='-1' ) then id = current_connection; -- PR107373
  else id = cast(:sid as integer);


  -- teraz czytamy wlasciwego globala ale z innego identyfikatora
  select pvalue from globalparams
    where connectionid=:id and psymbol=:pname
    and (:neosid is null or (:neosid is not null and neosid=:neosid))
    into :pvalue;

  suspend;
end^
SET TERM ; ^
