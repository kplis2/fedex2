--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_GET_ALL_ACCSYMBS(
      YEARID integer,
      NAME varchar(2048) CHARACTER SET UTF8                           ,
      ACCSYMB varchar(255) CHARACTER SET UTF8                           ,
      NLEVEL integer,
      ACC varchar(3) CHARACTER SET UTF8                           )
  returns (
      YEARIDOUT integer,
      NAMEOUT varchar(2048) CHARACTER SET UTF8                           ,
      ACCSYMBOUT varchar(255) CHARACTER SET UTF8                           ,
      NLEVELOUT integer,
      ACCOUT varchar(3) CHARACTER SET UTF8                           )
   as
declare variable bkacc integer;
declare variable slodef integer;
declare variable slokod ANALYTIC_ID;
declare variable sloname varchar(255);
declare variable accsynt BKSYMBOL_ID;
declare variable accname varchar(255);
declare variable tymname varchar(2048);
declare variable tymaccsymb varchar(255);
declare variable tymnlevel integer;
begin
  for
    select min(str.nlevel), str.bkaccount, bk.symbol , bk.descript
      from accstructure str
        join bkaccounts bk on (bk.ref = str.bkaccount)
      where bk.yearid = :yearid and bk.symbol = :acc
        and ((:nlevel is null) or (str.nlevel > :nlevel))
      group by str.bkaccount, bk.symbol, bk.descript
      into nlevelout, bkacc, accsynt, accname
    do begin
      accout = acc;
      select dictdef
        from accstructure
        where nlevel = :nlevelout and bkaccount = :bkacc
        into slodef;
      if (exists(select first 1 ref
       from slopoz where slownik = :slodef)) then
      begin
        for
          select kontoks, nazwa
            from slopoz
            where slownik = :slodef
            into slokod, sloname
        do begin
          if (nlevelout > 1) then
          begin
            tymname = name;
            tymaccsymb = accsymb;
            tymnlevel = nlevel;
            name = name||'->'||sloname;
            accsymb = accsymb||'-'||slokod;
          end else begin
            name = accname||'->'||sloname;
            accsymb = accsynt||'-'||slokod;
          end
          if (exists(select ref from accstructure where nlevel > :nlevelout
              and bkaccount = :bkacc)) then
           begin
             yearidout = :yearid;
             for
               select yearidout, nameout, accsymbout, nlevelout, accout
                 from sys_get_all_accsymbs(:yearidout,:name,:accsymb,:nlevelout, :accout)
                 into yearid,nameout,accsymbout,nlevel, acc
             do begin
               suspend;
               name = tymname;
               accsymb = tymaccsymb;
               nlevel = tymnlevel;
             end
           end else begin
             nameout = name;
             accsymbout = accsymb;
             yearidout = yearid;
             suspend;
             name = tymname;
             accsymb = tymaccsymb;
             nlevel = tymnlevel;
           end
        end
      end
    end
end^
SET TERM ; ^
