--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_ADD_DHL_REQUEST(
      SHIPPINGDOC INTEGER_ID,
      EDERULESYMB STRING40)
  returns (
      EDEDOCHREF EDEDOCH_ID)
   as
declare variable EDERULE EDERULES_ID;
declare variable SPEDRESPONSE SMALLINT_ID;
declare variable OTABLE TABLE_NAME;
declare variable OREF INTEGER_ID;
declare variable SHIPPINGDOCNO STRING40;
declare variable ADT SMALLINT_ID;
begin
  --exception universal coalesce(shippingdoc,0) || coalesce(ederulesymb,'brak');
  --insert into x_test_pk(createdate, message, param1, param2) values (current_timestamp, 'start ede_add_dhl_request', :shippingdoc, :ederulesymb);
  if (:ederulesymb like 'EDE_DHL_ORDER%') then
  begin

    select ref from listywysd where ref = :shippingdoc
    into :oref;

    otable = 'LISTYWYS';
  end
  else
  begin
    select l.ref, l.symbolsped, l.statussped
      from listywysd l where l.ref = :shippingdoc
    into :oref, :shippingdocno, :spedresponse;
    if (:oref is not null and coalesce(:shippingdocno,'') = '' and :ederulesymb not in ('EDE_DHL_SHIPMENT', 'EDE_DHL_SHIPPING')) then
      exception ede_listywysd_brak 'Dokument nie został jeszcze nadany';
    if (coalesce(:spedresponse,0) = 1 and :ederulesymb = 'EDE_DHL_SHIPMENT') then
      exception ede_listywysd_brak 'Dokument został już wysłany. Anuluj wysyłkę i nadaj ją jeszcze raz.';

    otable = 'LISTYWYSD';

    --exception universal 'test czy poszlo dalej' || coalesce(:shippingdoc,'') || ' ' || coalesce(:ederulesymb,'');

    --XXX JO: zablokowanie pakowania ADT i SENT
    --if (:adt > 0) then
     -- exception ede_listywysd_brak 'Wysyłka SENT lub ADT wyślij ręcznie z programu DHL';
  end

  select es.ref
    from ederules es
    where es.symbol = :ederulesymb
    into :ederule;

  insert into ededocsh( ederule, direction, createdate, otable, oref, status, filename, manualinit)
    values(:ederule, 1, current_timestamp, :otable, :oref, 0, :shippingdoc||'exp',0)
    returning ref into :ededochref;

  suspend;
end^
SET TERM ; ^
