--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PIT8A(
      CURRENTCOMPANY integer,
      PYEAR smallint,
      PMONTH smallint,
      R52 numeric(14,2) = 0)
  returns (
      P22 numeric(14,2),
      P38 numeric(14,2),
      P48 numeric(14,2),
      P49 numeric(14,2),
      P50 numeric(14,2),
      P51 numeric(14,2),
      P52 numeric(14,2),
      P53 numeric(14,2))
   as
declare variable PERIOD varchar(6);
declare variable EDECLARATION EDECLARATIONS_ID;
begin

  if(currentcompany < 0) then
  begin
    edeclaration = abs(currentcompany);

    select p.pvalue from edeclpos p
        join edeclposdefs d on (d.ref = p.edeclposdef)
      where p.edeclaration = :edeclaration
        and d.field = case(:pmonth)
          when 1 then 21
          when 2 then 22
          when 3 then 23
          when 4 then 24
          when 5 then 25
          when 6 then 26
          when 7 then 27
          when 8 then 28
          when 9 then 29
          when 10 then 30
          when 11 then 31
          when 12 then 32
        end
      into P22;

    select p.pvalue from edeclpos p
        join edeclposdefs d on (d.ref = p.edeclposdef)
      where p.edeclaration = :edeclaration
        and d.field = case(:pmonth)
          when 1 then 93
          when 2 then 94
          when 3 then 95
          when 4 then 96
          when 5 then 97
          when 6 then 98
          when 7 then 99
          when 8 then 100
          when 9 then 101
          when 10 then 102
          when 11 then 103
          when 12 then 104
        end
      into P38;

    select p.pvalue from edeclpos p
        join edeclposdefs d on (d.ref = p.edeclposdef)
      where p.edeclaration = :edeclaration
        and d.field = case(:pmonth)
          when 1 then 153
          when 2 then 154
          when 3 then 155
          when 4 then 156
          when 5 then 157
          when 6 then 158
          when 7 then 159
          when 8 then 160
          when 9 then 161
          when 10 then 162
          when 11 then 163
          when 12 then 164
        end
      into P50;

    select p.pvalue from edeclpos p
        join edeclposdefs d on (d.ref = p.edeclposdef)
      where p.edeclaration = :edeclaration
        and d.field = case(:pmonth)
          when 1 then 165
          when 2 then 166
          when 3 then 167
          when 4 then 168
          when 5 then 169
          when 6 then 170
          when 7 then 171
          when 8 then 172
          when 9 then 173
          when 10 then 174
          when 11 then 175
          when 12 then 176
        end
      into P51;

    select p.pvalue from edeclpos p
        join edeclposdefs d on (d.ref = p.edeclposdef)
      where p.edeclaration = :edeclaration
        and d.field = case(:pmonth)
          when 1 then 177
          when 2 then 178
          when 3 then 179
          when 4 then 180
          when 5 then 181
          when 6 then 182
          when 7 then 183
          when 8 then 184
          when 9 then 185
          when 10 then 186
          when 11 then 187
          when 12 then 188
        end
      into P52;

    select p.pvalue from edeclpos p
        join edeclposdefs d on (d.ref = p.edeclposdef)
      where p.edeclaration = :edeclaration
        and d.field = case(:pmonth)
          when 1 then 189
          when 2 then 190
          when 3 then 191
          when 4 then 192
          when 5 then 193
          when 6 then 194
          when 7 then 195
          when 8 then 196
          when 9 then 197
          when 10 then 198
          when 11 then 199
          when 12 then 200
        end
      into P53;
  end else
  ------------------------------------------------------------------------------
  begin
    p52 = r52;

    execute procedure e_func_periodinc(:pyear || :pmonth, 0)
      returning_values :period;

    select min(taxscale) from etaxlevels
      where taxyear = :pyear
      into :p49;
  
    if (p49 is null) then
      exception no_tax_levels_for_this_year;
  
    select sum(p.pvalue) from epayrolls PR
        join eprpos P on (P.payroll = PR.ref)
      where PR.tper = :period
        and P.ecolumn = 7000
        and PR.company = :currentcompany
        and pr.lumpsumtax = 1
      into :p48;
    p48 = coalesce(p48,0);
    p48 = round(p48);
  
    select sum(p.pvalue) from epayrolls PR
        join eprpos P on (P.payroll = PR.ref)
      where PR.tper = :period
        and P.ecolumn = 7350
        and PR.company = :currentcompany
        and pr.lumpsumtax = 1
      into :p50;
  
    p50 = coalesce(p50,0);
    p50 = round(p50);
    p52 = coalesce(p52,0);

    p51 = coalesce(p22,0) + coalesce(p38,0) + p50;  --PR67815

    if (p51 <= p52) then
      p53 = 0;
    else
      p53 = p51 - p52;

  end
  suspend;
end^
SET TERM ; ^
