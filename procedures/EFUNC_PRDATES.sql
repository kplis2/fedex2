--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EFUNC_PRDATES(
      PAYROLL integer,
      PTYPE smallint)
  returns (
      FROMDATE date,
      TODATE date)
   as
declare variable period char(6);
  declare variable iyear integer;
  declare variable imonth smallint;
  declare variable cper char(6);
  declare variable tper char(6);
  declare variable iper char(6);
begin
  select cper, tper, iper
    from epayrolls
    where ref = :payroll
    into :cper, :tper, :iper;
  if (ptype = 0) then
    period = cper;
  else if (ptype = 1) then
    period = tper;
  else if (ptype = 2) then
    period = iper;

  iyear = cast(substring(period from 1 for 4) as integer);
  imonth = cast(substring(period from 5 for 2) as smallint);

  fromdate = cast(iyear || '/' || imonth || '/1' as date);
  if (imonth = 12) then
     todate = cast((iyear + 1) || '/1/1' as date) - 1;
  else
     todate = cast(iyear || '/' || (imonth+1) || '/1' as date) - 1;
  suspend;
end^
SET TERM ; ^
