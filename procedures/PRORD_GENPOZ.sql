--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRORD_GENPOZ(
      PRSHEETDECL integer,
      REJESTRDECL varchar(3) CHARACTER SET UTF8                           ,
      NAGZAMDECL integer,
      POZZAMDECL integer,
      PRNAGZAMDECL integer,
      PRSHEETDOWNDECL smallint)
  returns (
      PRSHEET integer,
      WERSJAREF integer,
      AMOUNT numeric(14,4),
      REJESTR varchar(3) CHARACTER SET UTF8                           ,
      NAGZAM integer,
      POZZAM integer,
      PRNAGZAM integer,
      PRPOZZAM integer,
      ADDTOZLECPROD smallint,
      DELPOZ smallint,
      PRDEPART varchar(40) CHARACTER SET UTF8                           ,
      SHORTAGE numeric(14,4),
      PRSHEETDOWN smallint)
   as
declare variable sheetdown integer;
declare variable grossquantity numeric(14,4);
declare variable prpozzamdecl integer;
declare variable amountdecl numeric(14,4);
declare variable autogenzam smallint;
begin
  prsheet = prsheetdecl;
  delpoz = 1;
  prsheetdown = prsheetdowndecl;
  if(prsheetdecl = 0 and ((prnagzamdecl is null or prnagzamdecl = 0) and nagzamdecl > 0 and pozzamdecl >= 0))then begin
    for select wersjaref, ilosc, prsheet, ref
      from pozzam
      where (:pozzamdecl = 0 or ref = :pozzamdecl) and zamowienie = :nagzamdecl
      into :wersjaref, :amountdecl, :prsheet, :prpozzamdecl
    do begin
      addtozlecprod = 0;
      rejestr = rejestrdecl;
      nagzam = nagzamdecl;
      pozzam = pozzamdecl;
      if(pozzam = 0) then pozzam = prpozzamdecl;
      --generowanie pozycji rozchodowych zlecenia z pozycji przychodowych tego zlecenia
      if(prsheet is not null and prsheet > 0) then begin
        prnagzam = :prnagzamdecl;
        addtozlecprod = 1;
        delpoz = 0;
        prpozzam = :prpozzamdecl;
      end
      if(prsheet is null or prsheet = 0) then select ref from prsheets where wersjaref = :wersjaref and status = 2 into :prsheet;
      amount = amountdecl;
      select s.prdepart, d.autogenzam
        from prsheets s
        left join prdeparts d on (s.prdepart = d.symbol)
        where ref = :prsheet
      into :prdepart, :autogenzam;
      if(autogenzam = 1) then suspend;
      select ref from prsheets where wersjaref = :wersjaref and status = 2 into :sheetdown;
      if(sheetdown is not null) then begin
        for select sheetcur, grossquantity
          from prsheets_sheetsdown(:sheetdown, 100)
        into :prsheet, :grossquantity
        do begin
          addtozlecprod = 1;
          if(grossquantity is null) then grossquantity = 1;
          amount = amountdecl * grossquantity;
          select s.prdepart, d.autogenzam
            from prsheets s
            left join prdeparts d on (s.prdepart = d.symbol)
            where ref = :prsheet
          into :prdepart, :autogenzam;
          if(prsheet <> sheetdown and autogenzam = 1) then suspend;
        end
      end
    end
  end else begin
    for
      select p.ilosc - p.ilzreal, p.wersjaref, s.prdepart, p.ref, coalesce(p.prsheet,0)
        from pozzam p
           left join prsheets s on (p.prsheet = s.ref)
          where p.zamowienie = :prnagzamdecl and p.out = 0 and p.ilosc - p.ilzreal > 0
        into amountdecl, wersjaref, prdepart, prpozzam, prsheet
    do begin
      addtozlecprod = 1;
      rejestr = rejestrdecl;
      nagzam = nagzamdecl;
      pozzam = pozzamdecl;
      prnagzam = prnagzamdecl;
      amount = amountdecl;
      suspend;
    end
  end
end^
SET TERM ; ^
