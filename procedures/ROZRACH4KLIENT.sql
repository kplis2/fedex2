--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ROZRACH4KLIENT(
      KLIENT KLIENCI_ID)
  returns (
      ILEDOKUMENTOW integer,
      ILEPRZETERMINOWANE integer,
      WINIENZL numeric(14,2),
      POTERMINIEZL numeric(14,2))
   as
declare variable SALDO numeric(14,2);
declare variable DATAPLATNOSCI timestamp;
begin
    iledokumentow = 0;
    ileprzeterminowane = 0;
    winienzl = 0;
    POTERMINIEZL = 0;

    for
    select r.saldowmzl, r.dataplat
    from rozrach r
    where (r.klient = :klient) AND (r.SALDOWM < 0 or r.SALDOWM > 0)
    into :saldo, :dataplatnosci
    do begin
        iledokumentow = :iledokumentow + 1;
        winienzl = :winienzl + saldo;

        if(dataplatnosci <  current_date) then
        begin
           ileprzeterminowane = :ileprzeterminowane + 1;
           POTERMINIEZL = :POTERMINIEZL + saldo;
        end
    end

    suspend;
end^
SET TERM ; ^
