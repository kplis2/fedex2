--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXECUTION_SET_DIV(
      SEQUENCE smallint = 0,
      PRIORITY smallint = 0,
      PAYROLL integer = null,
      EMPLOYEE integer = null,
      ESIGN smallint = 1)
  returns (
      RET numeric(14,2))
   as
declare variable COUNTER NUMERIC_14_2;
declare variable SPLATA NUMERIC_14_2;
declare variable KOMORNIK NUMERIC_14_2;
declare variable TVAL NUMERIC_14_2;
declare variable TVAL_PART NUMERIC_14_2;
declare variable I integer;
declare variable DIFF NUMERIC_14_2;
declare variable DREF integer;
declare variable SERVCOUNT integer;
begin
/*MWr: Personel - Przenoszenie potracen z list plac do modulu komorniczego,
  z uwzglednieniem podzialu zajmowanego wynagrodzenia pomiedzy wiecej niz jednego
  komornika. Obsluga w zakresie konkretnej kolejnosci i danego priorytetu*/

  select max(tval), count(*) from execution_tmp
    where servnr is null
      and priority = :priority and sequence = :sequence
    into :tval, :counter;

  i = 0;
  ret = 0;
  komornik = tval;
  while (counter > 0 and (splata is not null or i = 0)) do
  begin
    i = i + 1;
    tval_part = komornik/counter;

    update execution_tmp
      set servnr = :i,
          evalue = pvalue
      where pvalue <= :tval_part and servnr is null
        and priority = :priority and sequence = :sequence;

    splata = null;
    select sum(evalue), count(*) from execution_tmp
      where servnr = :i
        and priority = :priority and sequence = :sequence
      into :splata, :servcount;

    if (splata is not null) then
    begin
      ret = ret + splata;
      komornik = komornik - splata;
      counter = counter - servcount;
    end
  end

  if (counter > 0) then
  begin
    update execution_tmp
      set servnr = :i,
          evalue = :tval_part
      where servnr is null
        and priority = :priority and sequence = :sequence;

    ret = tval;

  --Obsluga ewentualnych roznic groszowych wynikajacych z podzialu kwoty
    diff = komornik - tval_part * counter;
    if (diff <> 0) then
    begin
      select first 1 dref from execution_tmp
        where servnr = :i
          and priority = :priority and sequence = :sequence
        into :dref;

      update execution_tmp
        set servnr = servnr + 1,
            evalue = evalue + :diff
        where dref = :dref;
    end
  end

  if (payroll is not null) then
  begin
  --Podpiecie splat do komornikow w module komorniczym

    for
      select dref, evalue * :esign from execution_tmp
        where priority = :priority and sequence = :sequence
        into :dref, :splata
    do begin
      update or insert into ecolldedinstals (colldeduction, insvalue, insdate, payroll, employee)
        values (:dref, :splata, current_timestamp(0), :payroll, :employee)
        matching (colldeduction, payroll, employee);
    end
  end

  suspend;
end^
SET TERM ; ^
