--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_CHILDBENEFIT(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      MODE smallint)
  returns (
      RET numeric(14,2))
   as
DECLARE VARIABLE FROMDATE DATE;
DECLARE VARIABLE TODATE DATE;
DECLARE VARIABLE KIDS SMALLINT;
DECLARE VARIABLE I SMALLINT;
begin
  --DU: personel - liczenie zasilkow rodzinnych

  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  select count(*)
    from emplfamily
    where familyaid = 1 and employee = :employee
      and faidfrom <= :fromdate
      and faidto >= :todate
    into :kids;
  if (mode = 0) then
  begin
    ret = kids;
  end else
  begin
    i = 1;
    ret = 0;
    while (i <= kids) do
    begin
      if (i < 3) then
        ret = ret + 43;
      else if (i = 3) then
        ret = ret + 53;
      else if (i > 3) then
        ret = ret + 66;
      i = i + 1;
    end
  end
  suspend;
end^
SET TERM ; ^
