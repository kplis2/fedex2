--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_SERIEINERT(
      KTM KTM_ID,
      USERID USER_ID,
      MWSORDOUT INTEGER_ID,
      WMSACTOUT INTEGER_ID,
      SERIALNO VARCHAR_60,
      VERS VARCHAR_60)
  returns (
      OK SMALLINT_ID)
   as
declare variable REF_SERIE INTEGER_ID;
begin
ok = 1;
-- Ldz inputowanie do x_serie serii - staus 2 - RealizeWmsWzOrd
  execute procedure gen_ref('X_MWS_SERIE') returning_values ref_serie;
  insert into x_mws_serie
        (ref, ktm, serialno, wersjaref, mwsordout, wmsactout, typ, dataprzyjecia, "USER")
  values (:ref_serie, :ktm, :serialno, :vers, :mwsordout, :wmsactout, 2, current_date, :userid);
  suspend;
end^
SET TERM ; ^
