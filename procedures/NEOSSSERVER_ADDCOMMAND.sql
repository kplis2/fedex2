--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NEOSSSERVER_ADDCOMMAND(
      FUNC STRING60,
      PARAMETERS STRING1024)
  returns (
      REFOUT INTEGER_ID)
   as
begin
  if(FUNC<>'_  check  _') then
  begin
      insert into neossserver_data (
        "FUNCTION",
        parameters)
      values (
        :FUNC,
        :parameters)
      returning ref into :refout;
  end
  suspend;
end^
SET TERM ; ^
