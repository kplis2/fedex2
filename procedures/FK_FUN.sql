--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FK_FUN(
      COMPANY integer,
      NAME varchar(20) CHARACTER SET UTF8                           ,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      P1 varchar(20) CHARACTER SET UTF8                           ,
      P2 varchar(20) CHARACTER SET UTF8                           ,
      P3 varchar(20) CHARACTER SET UTF8                           )
  returns (
      AMOUNT numeric(15,2))
   as
declare variable p2int smallint;
begin
  if (p2 is null) then
    exception universal 'Brak okrelonej strony Wn/Ma ' || name;
  else if (p2 <> '') then
    p2int = cast(p2 as smallint);

  if (name = 'FK_FUN_BO') then
    execute procedure FK_FUN_BO(:company, :period, :p1, :p2int)
      returning_values amount;
  else if (name = 'FK_FUN_BOSALDO') then
    execute procedure FK_FUN_BOSALDO(:company, :period, :p1, :p2int)
      returning_values amount;
  else if (name = 'FK_FUN_PERSALDO') then
    execute procedure FK_FUN_PERSALDO(:company, :period, :p1, :p2int)
      returning_values amount;
  else if (name = 'FK_FUN_SALDO') then
    execute procedure FK_FUN_SALDO(:company, :period, :p1, :p2int)
      returning_values amount;
  else if (name = 'FK_FUN_OBROTYBM') then
    execute procedure FK_FUN_OBROTYBM(:company, :period, :p1, :p2int)
      returning_values amount;
  else if (name = 'FK_FUN_OBROTY_BO') then
    execute procedure FK_FUN_OBROTY_BO(:company, :period, :p1, :p2int)
      returning_values amount;
  else if (name = 'FK_FUN_FRPOS') then
    execute procedure FK_FUN_FRPOS(:company, :period, :p1, :p3)
      returning_values amount;
  else if (name = 'FK_FUN_FRPOSBO') then
    execute procedure FK_FUN_FRPOSBO(:company, :period, :p1, :p3)
      returning_values amount;
  else
    exception universal 'Brak procedury funkcji ' || name;
  suspend;
end^
SET TERM ; ^
