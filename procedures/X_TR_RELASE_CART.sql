--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_TR_RELASE_CART as
declare variable CART INTEGER_ID;
declare variable DOCID INTEGER_ID;
declare variable MWSORD INTEGER_ID;
begin
  for select M.CART, M.DOCID, M.MWSORD
      from MWSORDCARTCOLOURS M
      into :CART, :DOCID, :MWSORD
  do
  begin
    if (exists(select FIRST 1 1
               from LISTYWYSD L
               where L.REFDOK = :DOCID and
                     L.LISTAWYS is not null)) then
    begin
      update MWSACTS MA
      set MA.MWSCONSTLOC = null
      where MA.MWSORD = :MWSORD and
            MA.DOCGROUP = :DOCID and
            MA.MWSCONSTLOC = :CART;

      if (not exists(select first 1 1
                     from MWSACTS M
                     where M.MWSCONSTLOC = :CART)) then
      begin
        update mwsordcartcolours mw
          set mw.status = 2
          where mw.cart = :cart
           and mw.docid = :mwsord;
      end
    end
  end
end^
SET TERM ; ^
