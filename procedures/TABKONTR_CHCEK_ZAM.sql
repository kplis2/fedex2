--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TABKONTR_CHCEK_ZAM(
      ZAMOWIENIE integer)
  returns (
      KTMMIN varchar(300) CHARACTER SET UTF8                           ,
      KTMMAX varchar(300) CHARACTER SET UTF8                           )
   as
declare variable typ integer;
declare variable magazyn varchar(3);
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable ilosc numeric(14,4);
declare variable ilreal numeric(14,4);
declare variable ilmin numeric(14,4);
declare variable ilmax numeric(14,4);
begin
  ktmmin = '';
  ktmmax = '';
  select typ from NAGZAM where REF=:zamowienie into :typ;
  for select MAGAZYN,KTM, WERSJA, ILOSC-ILZREAL, ILREAL from POZZAM where ZAMOWIENIE=:ZAMOWIENIE
  into :magazyn, :ktm, :wersja, :ilosc, :ilreal
  do begin
    if(:typ >= 2) then ilosc = :ilreal;
    ilmin = null;
    select ILMIN, ILMAX from STKRPOZ join DEFMAGAZ on (DEFMAGAZ.SYMBOL=:magazyn and  STKRPOZ.TABELA = DEFMAGAZ.stkrtab)
      where STKRPOZ.KTM = :ktm and STKRPOZ.WERSJA = :wersja
    into :ilmin, :ilmax;
    if(:ilmin is not null and :ilmin < :ilosc and coalesce(char_length(:ktmmin),0)<260) then -- [DG] XXX ZG119346
      ktmmin = :ktm||','||:ktmmin;
    if(:ilmax is not null and :ilmax > :ilosc and coalesce(char_length(:ktmmax),0)<260) then -- [DG] XXX ZG119346
      ktmmax = :ktm||','||:ktmmax;
  end
end^
SET TERM ; ^
