--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PR_EDUSTRUCT(
      DATA_PODANA date,
      CZYPEL smallint,
      COMPANY integer)
  returns (
      WYKSZTALCENIE integer,
      OPIS varchar(1024) CHARACTER SET UTF8                           ,
      MAN integer,
      WOMAN integer,
      SUMA integer)
   as
begin

  for
    select distinct p.education, z.descript
      from persons p
        left join edictzuscodes z on (p.education = z.ref)
      order by z.code, z.descript
      into :wyksztalcenie, :opis
  do begin
    suma = null;
    man = null;

    select count(distinct p.ref)
       from persons p
         join employees e on (e.person=p.ref)
         join employment em on (em.employee=e.ref)
         left join eperschools s on (p.ref = s.person)
       where (em.workdim = 1 or :czypel = 0)
         and e.company = :company
         and em.fromdate <= :data_podana and (em.todate >= :data_podana or em.todate is null)
         and ((s.fromdate = (select max(s1.fromdate) from eperschools s1 where s1.person = p.ref and s1.fromdate <= :data_podana and s1.education is not null) and s.education = :wyksztalcenie)
           or (s.education is null and :wyksztalcenie is null and not exists(select first 1 1 from eperschools s2 where s2.person = p.ref and s2.fromdate <= :data_podana and s2.education is not null)))
    into :suma;
    suma = coalesce(suma,0);

    select count(distinct p.ref)
      from persons p
        join employees e on (e.person=p.ref)
        join employment em on (em.employee=e.ref)
        left join eperschools s on (p.ref = s.person)
      where (em.workdim = 1 or :czypel = 0)
        and e.company = :company
        and em.fromdate <= :data_podana and (em.todate >= :data_podana or em.todate is null)
        and ((s.fromdate = (select max(s1.fromdate) from eperschools s1 where s1.person = p.ref and s1.fromdate <= :data_podana and s1.education is not null) and s.education = :wyksztalcenie)
          or (s.education is null and :wyksztalcenie is null and not exists(select first 1 1 from eperschools s2 where s2.person = p.ref and s2.fromdate <= :data_podana and s2.education is not null)))
        and p.sex = 1
    into :man;
    man = coalesce(man,0);

    woman = suma - man;
    suspend;
  end
end^
SET TERM ; ^
