--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RECOUNT_EWORKEDHOURS(
      EMPLOYEE integer,
      CALENDAR integer,
      FROMPERIOD varchar(6) CHARACTER SET UTF8                           ,
      TOPERIOD varchar(6) CHARACTER SET UTF8                           ,
      FROMDATE_IN varchar(10) CHARACTER SET UTF8                           ,
      TODATE_IN varchar(10) CHARACTER SET UTF8                           )
  returns (
      IS_OVERTIME smallint)
   as
  declare variable fromdate date;
  declare variable todate date;
  declare variable compensated_time_minus integer;
  declare variable compensated_time_plus integer;
  declare variable ref_minus integer;
  declare variable ref_plus integer;

  declare variable eof_plus smallint;
  declare variable eof_minus smallint;
  declare variable sfromdate date;
  declare variable cdate date;
  declare variable ecdate date;
  declare variable old_ref_plus integer;
  declare variable old_ref_minus integer;
begin
  --DS: zadaniem funkcji jest uzupelnienie brakujacych godzin do odbioru
  --z nadwyzek wystepujacych w innych dniach
  --jezeli pomimo uzupelnienia dalej sa godziny do odbioru, uzytkownik zostanie zapytany,
  --czy nadwyzke przemianowac na nadgodziny
  if (coalesce(char_length(fromperiod),0) = 6) then -- [DG] XXX ZG119346
  begin
    select fromdate from period2dates(:fromperiod) into :fromdate;
    select todate from period2dates(:toperiod) into :todate;
  end else begin
    fromdate = cast(fromdate_in as date);
    todate = cast(todate_in as date);
  end
  is_overtime = 0;
  --recounted = 1;
  eof_plus = 1;
  eof_minus = 1;
  sfromdate = '1899-12-31';
  while (1=1) do begin

    --brakujace godziny
    if (eof_minus = 1) then begin
      old_ref_minus = ref_minus;
      ref_minus=null;
      select first 1 e.ref, e.compensatory_time - e.compensated_time, e.cdate
        from emplcaldays e
        where e.employee = :employee
          and e.ecalendar = :calendar
          and e.cdate >= :fromdate
          and e.cdate <= :todate
          and e.compensatory_time -  e.compensated_time < 0
        into :ref_minus, :compensated_time_minus, :ecdate;
        if (ref_minus is null) then begin
          --gdy nie ma juz wiecej pol do przeliczenia, zrob update'a na ostatnich polach
          if (old_ref_minus is not null) then begin
            update emplcaldays c set c.compensated_time = c.compensatory_time - :compensated_time_minus where c.ref = :old_ref_minus;
            update emplcaldays c set c.compensated_time = c.compensatory_time - :compensated_time_plus where c.ref = :ref_plus;
          end --else recounted = 0;
          break;
        end
      fromdate = ecdate+1;
    end

    --nadmiar
    if (eof_plus = 1) then
    begin
      old_ref_plus = ref_plus;
      ref_plus = null;
      select first 1 e2.ref ,e2.compensatory_time -  e2.compensated_time, e2.cdate
        from emplcaldays e2
        where e2.employee = :employee
          and e2.ecalendar = :calendar
          and e2.compensatory_time -  e2.compensated_time > 0
          and e2.cdate > :sfromdate
        order by e2.cdate
        into :ref_plus, :compensated_time_plus, :cdate;
        if (ref_plus is null) then begin
          if (old_ref_plus is not null) then begin
            update emplcaldays c set c.compensated_time = c.compensatory_time - :compensated_time_minus where c.ref = :ref_minus;
            update emplcaldays c set c.compensated_time = c.compensatory_time - :compensated_time_plus where c.ref = :old_ref_plus;
          end --else recounted = 0;
          break;
        end
      sfromdate = cdate;
    end

    --do odbioru jest wiecej niz potrzeba
    if (compensated_time_plus > - compensated_time_minus) then
    begin
      compensated_time_plus = compensated_time_plus + compensated_time_minus;
      compensated_time_minus = 0;
      eof_plus = 0;
      eof_minus = 1;
    end else if (compensated_time_plus = - compensated_time_minus) then
    begin
      compensated_time_minus = 0;
      compensated_time_plus = 0;
      eof_plus = 1;
      eof_minus = 1;
    end
    --jeszcze trzeba dobrac z innych dni
    else begin
      compensated_time_minus = compensated_time_plus + compensated_time_minus;
      compensated_time_plus = 0;
      eof_plus = 1;
      eof_minus = 0;
    end

    if (eof_minus = 1) then
      update emplcaldays c set c.compensated_time = c.compensatory_time - :compensated_time_minus where c.ref = :ref_minus;
    if (eof_plus = 1) then
      update emplcaldays c set c.compensated_time = c.compensatory_time - :compensated_time_plus where c.ref = :ref_plus;
  end 

  if (exists(select e.ref
      from emplcaldays e
      where e.employee = :employee
        and e.ecalendar = :calendar
        and e.cdate >= :fromdate
        and e.cdate <= :todate
        and e.compensatory_time -  e.compensated_time > 0)) then is_overtime = 1;
  suspend;
end^
SET TERM ; ^
