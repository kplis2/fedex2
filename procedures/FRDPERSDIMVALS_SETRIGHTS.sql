--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRDPERSDIMVALS_SETRIGHTS(
      FRDPERSDIMVAL integer,
      OPERATOR integer,
      CHECKOPER integer)
   as
declare variable readrights varchar(80);
begin
  select frdpersdimvals.readrights
    from frdpersdimvals
    where frdpersdimvals.ref = :frdpersdimval
    into :readrights;
  if (:checkoper = 1) then
  begin
    if (:readrights is null) then
      readrights = '';
    if(position(';'||:operator||';' in :readrights) > 0) then
      exit;
    if(position(';' in :readrights) = 1) then
      readrights = ';'||:operator||:readrights;
    else
      readrights = ';'||:operator||';'||:readrights;
    update frdpersdimvals set frdpersdimvals.readrights = :readrights
      where frdpersdimvals.ref = :frdpersdimval;
  end else if (:checkoper = 0) then
  begin
    update frdpersdimvals set frdpersdimvals.readrights = replace(frdpersdimvals.readrights, ';'||:operator||';', '')
      where frdpersdimvals.ref = :frdpersdimval;
  end
end^
SET TERM ; ^
