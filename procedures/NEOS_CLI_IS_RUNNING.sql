--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NEOS_CLI_IS_RUNNING(
      REF INTEGER_ID)
  returns (
      STATUS SMALLINT_ID)
   as
declare variable last_alive timestamp_id;
begin
  /* Procedure Text */


  select n.status, coalesce(n.lastalivetime, n.starttime) from neoscli_data n where n.ref = :ref into :status, :last_alive;
  if(datediff(second from :last_alive to current_timestamp) > 5) then
  begin
   status = 4;
   update neoscli_data n set n.status = :status where n.ref = :ref;
  end
  suspend;
end^
SET TERM ; ^
