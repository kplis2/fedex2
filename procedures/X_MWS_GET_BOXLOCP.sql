--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_GET_BOXLOCP(
      SYMBOL STRING20)
  returns (
      LOKP STRING40,
      ROZLADOWANO SMALLINT_ID)
   as
begin
  select l.x_mwsconstlock_p, l.x_zaladowane
    from listywysdroz_opk l
    where l.stanowisko = :symbol
  into :lokp, :rozladowano;
  if (lokp is null) then lokp = '';
  if (rozladowano is null) then rozladowano = 0;
  suspend;
end^
SET TERM ; ^
