--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_EP_SALCARD(
      COMPANY integer,
      EMP integer,
      ROK integer,
      PARAM1 integer)
  returns (
      DEP varchar(255) CHARACTER SET UTF8                           ,
      ODD varchar(255) CHARACTER SET UTF8                           ,
      DOD varchar(255) CHARACTER SET UTF8                           ,
      TYP varchar(255) CHARACTER SET UTF8                           ,
      KWOTA varchar(255) CHARACTER SET UTF8                           ,
      PAY_TYP integer,
      LICZ numeric(14,2),
      MIAN numeric(14,2),
      P100 numeric(14,2),
      P200 numeric(14,2),
      P121 numeric(14,2),
      P120 numeric(14,2),
      P160 numeric(14,2),
      P150 numeric(14,2),
      P125 numeric(14,2),
      P205 numeric(14,2),
      P400 numeric(14,2),
      P610 numeric(14,2),
      P611 numeric(14,2),
      P612 numeric(14,2),
      P490 numeric(14,2),
      P350 numeric(14,2),
      S_ZUS numeric(14,2),
      K_OB numeric(14,2),
      P_INNE numeric(14,2),
      PN100 integer,
      PN50 integer,
      PNN integer,
      PWD integer,
      MIES varchar(4) CHARACTER SET UTF8                           ,
      U_OD varchar(10) CHARACTER SET UTF8                           ,
      U_DO varchar(10) CHARACTER SET UTF8                           ,
      U_TYP varchar(22) CHARACTER SET UTF8                           ,
      U_PLATNY varchar(2) CHARACTER SET UTF8                           ,
      PO varchar(4) CHARACTER SET UTF8                           ,
      P650 numeric(14,2),
      P702 numeric(14,2),
      P710 numeric(14,2),
      P706 numeric(14,2),
      P715 numeric(14,2),
      P722 numeric(14,2),
      P721 numeric(14,2),
      P2625 numeric(14,2),
      P735737 numeric(14,2),
      P760 numeric(14,2),
      P768 numeric(14,2),
      P777 numeric(14,2),
      P771 numeric(14,2),
      P900 numeric(14,2),
      POT_R numeric(14,2),
      D_IMIE varchar(60) CHARACTER SET UTF8                           ,
      D_BD varchar(10) CHARACTER SET UTF8                           )
   as
declare variable DEP_T varchar(255);
declare variable ODD_T timestamp;
declare variable DOD_T timestamp;
declare variable TYP_T varchar(255);
declare variable KWOTA_T varchar(255);
declare variable PAY_TYP_T integer;
declare variable LICZNIK integer;
declare variable LICZ_SK integer;
declare variable LICZ_T numeric(14,2);
declare variable MIAN_T numeric(14,2);
declare variable OKRES varchar(6);
declare variable U_OD_T timestamp;
declare variable U_DO_T timestamp;
declare variable U_ECNUM integer;
declare variable P26_T numeric(14,2);
declare variable P25_T numeric(14,2);
declare variable P735_T numeric(14,2);
declare variable P737_T numeric(14,2);
declare variable D_IMIE_T varchar(60);
declare variable D_BD_T timestamp;
begin
--exception test_break company||' '||emp||' '||rok;
------------------------------PRZEBIEG ZATRUDNIENIA -------------------------------------
if (param1 = 1) then
begin
licznik = 0;
for select first 6 e.department,e.fromdate,e.todate,wt.descript,e.salary, e.paymenttype
    from employees ep
    left join employment e on (ep.ref = e.employee)
    left join edictworktypes wt on (ep.worktype = wt.ref)
    where ep.company = :company
    and ep.ref = :emp
    and (
         (extract(year from cast(e.fromdate as timestamp)) = :rok)
         or
         (cast(e.todate as timestamp) > current_date)
         or
         (e.todate is null)
        )
    order by e.fromdate
      into dep_t,odd_t,dod_t,typ_t,kwota_t,pay_typ_t
    do
     begin
        licznik = licznik + 1;
        dep = :dep_t;
        odd = :odd_t;
        dod = :dod_t;
        typ = :typ_t;
        kwota = :kwota_t;
        pay_typ = :pay_typ_t;
        suspend;
     end

     if (licznik < 6) then
     while (licznik <> 6)
     do
      begin
       licznik = licznik + 1;
       dep = '';
       odd = '';
       dod = '';
       typ = '';
       kwota = '';
       pay_typ = 0;
       suspend;
      end
end
----------------------------Skladniki---------------------------------------
if (param1 = 2) then
 begin
  licz_sk = 0;
  while (licz_sk < 12) do
   begin
     licz = null;
     mian = null;
     p100 = null;
     p200 = null;
     p160 = null;
     p121 = null;
     p125 = null;
     p205 = null;
     p400 = null;
     p610 = null;
     p611 = null;
     p612 = null;
     p490 = null;
     p350 = null;
     s_zus = null;
     p_inne = null;
     k_ob = null;
     pn100 = null;
     pn50 = null;
     pnn = null;
     pwd = null;
     licz_sk = licz_sk + 1;

     if (licz_sk = 1) then mies = 'I';
     else if (licz_sk = 2) then mies = 'II';
     else if (licz_sk = 3) then mies = 'III';
     else if (licz_sk = 4) then mies = 'IV';
     else if (licz_sk = 5) then mies = 'V';
     else if (licz_sk = 6) then mies = 'VI';
     else if (licz_sk = 7) then mies = 'VII';
     else if (licz_sk = 8) then mies = 'VIII';
     else if (licz_sk = 9) then mies = 'IX';
     else if (licz_sk = 10) then mies = 'X';
     else if (licz_sk = 11) then mies = 'XI';
     else if (licz_sk = 12) then mies = 'XII';

     if (licz_sk < 10) then
       okres = rok||'0'||licz_sk;
     else
       okres = rok||licz_sk;

     select first 1 e.dimnum, e.dimden
       from employment e
       where e.employee = :emp
         and ((extract(year from e.fromdate) = :rok and extract(month from e.fromdate) = :licz_sk)
           or -- (extract(year from e.todate) >= :rok)
              (cast(e.todate as timestamp) > current_date) or (e.todate is null))
     order by e.fromdate
     into :licz_t,:mian_t;

     if (licz_sk > extract(month from current_date)) then
       begin
         licz = null;
         mian = null;
       end
     else
       begin
         licz = licz_t;
         mian = mian_t;
       end

     select sum(pp.pvalue)
       from epayrolls pr
         join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
         join ecolumns c on (pp.ecolumn = c.number and c.number = 1000)
        where pp.employee = :emp
        and pr.tper = :okres
        into :p100;


     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 2000)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p200;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 1600)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p160;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 1210)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p121;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 1200)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p120;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 1250)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p125;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and  c.number = 2000)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p205;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 4000)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p400;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 6100)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p610;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 6110)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p611;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number in (6120, 6130))
      where pp.employee = :emp
      and pr.tper = :okres
      into :p612;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 4900)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p490;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 3500)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p350;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 600)
      where pp.employee = :emp
      and pr.tper = :okres
      into :pn100;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 610)
      where pp.employee = :emp
      and pr.tper = :okres
      into :pn50;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 640)
      where pp.employee = :emp
      and pr.tper = :okres
      into :pnn;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 500)
      where pp.employee = :emp
      and pr.tper = :okres
      into :pwd;

     --- wynagrodzenie z a urlop a wic skladniki 150 + 151 + 160
     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number in (1500,1510,1600))
      where pp.employee = :emp
      and pr.tper = :okres
      into :p150;

      p120 = :p120 + :p121;

      s_zus = coalesce(:p610,0.00) + coalesce(:p611,0.00) + coalesce(:p612,0.00);
      p_inne = coalesce(:p400,0.00) - (coalesce(:p100,0.00) + coalesce(:p125,0.00) + coalesce(:p205,0.00)
             + coalesce(:p150,0) +  coalesce(:p120,0));
      k_ob = coalesce(:p490,0.00) + coalesce(:p350,0.00);

      if (:p610 = 0.00) then p610 = null;
      if (:p611 = 0.00) then p611 = null;
      if (:p612 = 0.00) then p612 = null;
      if (:p400 = 0.00) then p400 = null;
      if (:p100 = 0.00) then p100 = null;
      if (:p200 = 0.00) then p200 = null;
      if (:p125 = 0.00) then p125 = null;
      if (:p205 = 0.00) then p205 = null;
      if (:p160 = 0.00) then p160 = null;
      if (:p490 = 0.00) then p490 = null;
      if (:p350 = 0.00) then p350 = null;
      if (:s_zus = 0.00) then s_zus = null;
      if (:p_inne = 0.00) then p_inne = null;
      if (:k_ob = 0.00) then k_ob = null;

     suspend;
   end
 end

 --------------urlopy---------------
 if (param1 = 3) then
  begin
   licznik = 0;
    for select first 7 a.fromdate,a.todate,e.name,e.number
       from eabsences a
       left join ecolumns e on (a.ecolumn = e.number)
       where a.ayear = :rok and a.employee = :emp and e.number in (220,230,250,300) and a.correction in (0,2)
       into :u_od_t,:u_do_t,:u_typ,:u_ecnum
     do
      begin
        licznik = licznik + 1;
        u_od = cast(:u_do_t as date);
        u_do = cast(:u_do_t as date);
        if (u_ecnum = 30) then u_platny = 'NP';
        else u_platny = 'P';
        suspend;
      end

      while (licznik < 7) do
       begin
         licznik = licznik + 1;
         u_od = '';
         u_do = '';
         u_typ = '';
         u_platny = '';
         suspend;
       end
end
---------------------------------- potracenia ----------------------------------------
if (param1 = 4) then
  begin
   licznik = 0;

   while (licznik < 12) do
   begin
     licznik = licznik + 1;
     p650 = 0.00;
     p706 = 0.00;
     p702 = 0.00;
     p2625 = 0.00;
     p735737 = 0.00;
     p900 = 0.00;
     p26_t = 0.00;
     p25_t = 0.00;
     p735_t = 0.00;
     p737_t = 0.00;
     p760 = 0.00;
     p768 = 0.00;
     p777 = 0.00;
     p771 = 0.00;
     pot_r = 0.00;

     if (licznik < 10) then
       okres = rok||'0'||licznik;
     else
       okres = rok||licznik;

     if (licznik = 1) then po = 'I';
     else if (licznik = 2) then po = 'II';
     else if (licznik = 3) then po = 'III';
     else if (licznik = 4) then po = 'IV';
     else if (licznik = 5) then po = 'V';
     else if (licznik = 6) then po = 'VI';
     else if (licznik = 7) then po = 'VII';
     else if (licznik = 8) then po = 'VIII';
     else if (licznik = 9) then po = 'IX';
     else if (licznik = 10) then po = 'X';
     else if (licznik = 11) then po = 'XI';
     else if (licznik = 12) then po = 'XII';

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 6500)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p650;

     select max(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 7020)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p702;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 7100)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p710;

     select sum(pp.pvalue)
       from epayrolls pr
         join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
         join ecolumns c on (pp.ecolumn = c.number  and c.number = 7060)
       where pp.employee = :emp
       and pr.tper = :okres
       into :p706;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 7150)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p715;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 7220)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p722;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 7210)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p721;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 260)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p26_t;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 250)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p25_t;

      p2625 = coalesce(:p25_t,0.00) - coalesce(:p26_t,0.00);

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 7350)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p735_t;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 7370)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p737_t;

      p735737 = coalesce(:p735_t,0.00) + coalesce(:p737_t,0.00);

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 7600)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p760;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 7680)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p768;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 7770)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p777;

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 7710)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p771;

      pot_r = coalesce(:p760,0.00) + coalesce(:p768,0.00) + coalesce(:p777,0.00) + coalesce(:p771,0.00);

     select sum(pp.pvalue) from epayrolls pr
      join eprpos pp on (pr.ref = pp.payroll and pr.empltype = 1)
      join ecolumns c on (pp.ecolumn = c.number  and c.number = 9000)
      where pp.employee = :emp
      and pr.tper = :okres
      into :p900;

      if (:p650 = 0.00) then p650 = null;
      if (:p702 = 0.00) then p702 = null;
      if (:p710 = 0.00) then p710 = null;
      if (:p706 = 0.00) then p706 = null;
      if (:p715 = 0.00) then p715 = null;
      if (:p722 = 0.00) then p722 = null;
      if (:p721 = 0.00) then p721 = null;
      if (:p2625 = 0.00) then p2625 = null;
      if (:p735737 = 0.00) then p735737 = null;
      if (:p760 = 0.00) then p760 = null;
      if (:p768 = 0.00) then p768 = null;
      if (:p777 = 0.00) then p777 = null;
      if (:p771 = 0.00) then p771 = null;
      if (:pot_r = 0.00) then pot_r = null;
      if (:p900 = 0.00) then p900 = null;

     suspend;
   end
  end
------- rodzina ----------------------------------------------------------------------
 if (param1 = 5) then
    begin
     licznik = 0;
     for select first 6 ef.fname,ef.birthdate from emplfamily ef
        left join edictzuscodes ec on (ef.conecttype = ec.ref)
        where ec.code = 11 and ef.employee = :emp
        order by ef.birthdate
        into :d_imie_t, :d_bd_t
     do
       begin
         licznik = licznik + 1;
         d_imie = licznik||'  '||d_imie_t;
         d_bd = cast(:d_bd_t as date);
         suspend;
       end

     while (licznik < 6) do
      begin
        licznik = licznik + 1;
        d_imie = licznik;
        d_bd = '';
        suspend;
      end
    end
end^
SET TERM ; ^
