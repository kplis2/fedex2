--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWZOR_CREATE_HTML(
      EMAILWZORSYMBOL varchar(40) CHARACTER SET UTF8                           )
   as
declare variable EMAILWIADREF integer;
begin
  select EMAILWIAD from EMAILWZOR where symbol=:emailwzorsymbol
  into :emailwiadref;
  if(:emailwiadref is null or (:emailwiadref = 0) )then begin
     execute procedure GEN_REF('EMAILWIAD')
      returning_values :emailwiadref;
     insert into EMAILWIAD(REF,EMAIL,TRYB,OD, DOKOGO,  TYTUL, TRESC)
     select :emailwiadref, 0,0,OD, DOKOGO, TYTUL, TRESC
     from EMAILWZOR where symbol=:emailwzorsymbol;
     update EMAILWZOR set EMAILWIAD=:emailwiadref
       where symbol=:emailwzorsymbol;
  end


end^
SET TERM ; ^
