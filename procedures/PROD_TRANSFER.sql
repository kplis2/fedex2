--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PROD_TRANSFER(
      PROPERRAP integer,
      MODE smallint)
   as
declare variable preffrom integer;
declare variable prefto integer;
declare variable quantity numeric(14,4);
declare variable quantityto numeric(14,4);
declare variable quantitytoreal numeric(14,4);
declare variable toinsert numeric(14,4);
declare variable prgengroup integer;
declare variable newreffrom integer;
declare variable newrefto integer;
declare variable prshmat integer;
declare variable wersjaref integer;
declare variable prschedoper integer;
declare variable prschedguidepos integer;
declare variable prschedguideto integer;
declare variable prschedguidefrom integer;
declare variable pozzamto integer;
declare variable nagzamto integer;
declare variable pozzamfrom integer;
declare variable nagzamfrom integer;
declare variable ktm varchar(40);
begin
  select g.pozzam, o.amount, p.ref, g.ref, g.zamowienie, g.pozzam, o.prgengroup
    from prschedopers p
      join propersraps o on (o.prschedoper = p.ref)
      left join prschedguides g on (g.ref = p.guide)
    where o.ref = :properrap
    into preffrom, quantity, prschedoper, prschedguidefrom, nagzamfrom, pozzamfrom, prgengroup;
  if (mode in (1,2)) then
  begin
    if (prgengroup is not null) then
    begin
      update prschedopers o set o.checkprschedguidedets = 0 where o.ref = :prschedoper;
      delete from prschedguidedets d where d.prgengroup = :prgengroup and d.out = 0;
      update prschedopers o set o.checkprschedguidedets = 1 where o.ref = :prschedoper;
    end
  end
  if (mode in (0,1)) then
  begin
    for
      select pt.ref, p.amount, p.amountzreal, p.ktm, p.wersjaref, p.prshmat, p.ref,
          p.prschedguide, p.pozzamref, pt.zamowienie
        from pozzam pf
          left join relations r on (r.stablefrom = 'POZZAM' and r.sreffrom = pf.ref and r.act > 0 and r.relationdef = 1)
          left join pozzam pt on (r.stableto = 'POZZAM' and r.srefto = pt.ref and r.act > 0)
          left join prschedguidespos p on (p.pozzamref = pt.ref)
        where pf.ref = :preffrom and pt.ref is not null
        into prefto, quantityto, quantitytoreal, ktm, wersjaref, prshmat, prschedguidepos,
            prschedguideto, pozzamto, nagzamto
    do begin
      if (quantityto is null) then quantityto = 0;
      if (quantitytoreal is null) then quantitytoreal = 0;
      quantityto = quantityto - quantitytoreal;
      if (quantityto > 0 and quantity > 0) then
      begin
        if (prgengroup is null) then
          execute procedure gen_ref('PRGENGROUP') returning_values prgengroup;
        toinsert = 0;
        if (quantityto > quantity) then
          toinsert = quantity;
        else
          toinsert = quantityto;
        execute procedure PRSCHEDGUIDEDETS_ADD(:nagzamfrom,:pozzamfrom,:prschedguidefrom,
            null,:prschedoper,null,:ktm,:wersjaref,1,
            '','',:toinsert,0,0,0,null,0,null,null,:prgengroup)
          returning_values newreffrom;
        execute procedure PRSCHEDGUIDEDETS_ADD(:nagzamto,:pozzamto,:prschedguideto,
            :prschedguidepos,null,:prshmat,:ktm,:wersjaref,1,
            '','',:toinsert,1,0,0,null,0,null,null,:prgengroup)
          returning_values newrefto;
        execute procedure PR_PRSCHEDGUIDEDET_DEPEND_ADD(:newreffrom,:newrefto,:toinsert,0);
      end
      quantity = quantity - toinsert;
      if (quantity <= 0) then
        break;
    end
    update propersraps r set r.prgengroup = :prgengroup where r.ref = :properrap;
  end
end^
SET TERM ; ^
