--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_STANYHIST(
      MAGAZYNY varchar(255) CHARACTER SET UTF8                           ,
      MASKAKTM varchar(40) CHARACTER SET UTF8                           ,
      DATAOD date,
      DATADO date,
      ZEWN smallint)
  returns (
      DATA date,
      ROK varchar(4) CHARACTER SET UTF8                           ,
      OKRES varchar(6) CHARACTER SET UTF8                           ,
      TYDZIEN varchar(7) CHARACTER SET UTF8                           ,
      STANMAG numeric(14,4),
      WARTMAG numeric(14,2),
      ILOSC numeric(14,4),
      ILOSCBRAK numeric(14,4),
      ILOSCINC numeric(14,4),
      ZAREZERW numeric(14,4),
      ZABLOKOW numeric(14,4),
      ZAMOWIONO numeric(14,4),
      STANPROGNOZ numeric(14,4),
      STANMIN numeric(14,4),
      STANMAX numeric(14,4),
      KOLOR smallint)
   as
declare variable prognoza numeric(14,4);
declare variable zarezerwadd numeric(14,4);
declare variable zablokowadd numeric(14,4);
declare variable zamowionoadd numeric(14,4);
begin
  if(:magazyny<>'') then magazyny = ';'||trim(:magazyny)||';';
  if(:maskaktm is null or :maskaktm='') then maskaktm = '%';
  if(:datado is null) then datado = current_date;
  if(:dataod is null) then dataod = cast(extract(year from current_date)-1 as varchar(4))||'-01-01';
  if(:datado<:dataod) then datado = :dataod;
  prognoza = 0;
  for select d.data,d.rok,d.okres,d.tydzien,
    s.stan, s.wartosc,
    w.ilosc, w.iloscbrak, w.iloscinc,
    r.zarezerw, r.zablokow, r.zamowiono,r.zarezerwadd, r.zablokowadd, r.zamowionoadd,
    m.stanmin, m.stanmax
    from get_dates(:dataod,:datado) d
    join get_stanyarch_data(:magazyny,:maskaktm,:dataod,:datado) s on (s.data=d.data)
    join get_wydhist_data(:magazyny,:maskaktm,:dataod,:datado,:zewn) w on (w.data=d.data)
    join get_stanyrez_data(:magazyny,:maskaktm,:dataod,:datado) r on (r.data=d.data)
    join get_stanyminmax_data(:magazyny, :maskaktm,:dataod,:datado) m on (m.data=d.data)
    order by d.data
    into :data, :rok, :okres, :tydzien,
      :stanmag, :wartmag,
      :ilosc, :iloscbrak, :iloscinc,
      :zarezerw, :zablokow, :zamowiono, :zarezerwadd, :zablokowadd, :zamowionoadd,
      :stanmin, :stanmax
    do begin
      prognoza = :prognoza - :zarezerwadd - :zablokowadd + :zamowionoadd;
      stanprognoz = :stanmag + :prognoza;
      -- okreslamy poziom zapasu w buforze
      if(:stanmag>2.0*:stanmax/3.0) then kolor = 3;
      else if(:stanmag>:stanmax/3.0) then kolor = 2;
      else if(:stanmag>0) then kolor = 1;
      else kolor = 0;
      suspend;
    end
end^
SET TERM ; ^
