--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_CHECK_VERS_IN_LISTYWYSD(
      VERS WERSJE_ID,
      DOKUMENT LISTYWYSD_ID)
  returns (
      POZ LISTYWYSDPOZ_ID)
   as
begin

   for select distinct lwp.ref
         from listywysdpoz lwp
         where lwp.dokument = :DOKUMENT
           and lwp.wersjaref = :vers
   into :POZ
   do suspend;
end^
SET TERM ; ^
