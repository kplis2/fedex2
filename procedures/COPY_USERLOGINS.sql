--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE COPY_USERLOGINS(
      USR varchar(40) CHARACTER SET UTF8                           ,
      PSW varchar(40) CHARACTER SET UTF8                           ,
      FROMUSER integer,
      TOUSER integer)
  returns (
      STATUS integer)
   as
declare variable cryptedpsw varchar(255);
begin
  cryptedpsw = shash_hmac('hex','sha1',:psw,:usr);
  status = 0;
  if(exists(select first 1 1 from s_users u where u.userlogin = :usr and u.userpass = :cryptedpsw and (u.supervisor>0 or u.sente=1))) then begin
    delete from S_USERLOGINS where USERID = :touser;
    insert into S_USERLOGINS(USERID,DBASE,USERLOGIN,USERPASS)
    select :touser,a1.DBASE,a1.USERLOGIN,a1.USERPASS from s_userlogins a1
    where a1.USERID = :fromuser;
    status = 1;
  end
  suspend;
end^
SET TERM ; ^
