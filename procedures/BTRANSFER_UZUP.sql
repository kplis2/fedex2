--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BTRANSFER_UZUP(
      BTRANSFER integer)
   as
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable SYMBFAK varchar(30);
declare variable KONTOFK KONTO_ID;
declare variable CNT integer;
declare variable MAXAMOUNT numeric(14,2);
declare variable AMOUNT numeric(14,2);
declare variable CURRENCY varchar(5);
declare variable COMPANY integer;
begin
  --TOFIX!!! wielozakladowosc
  select SLODEF, SLOPOZ,  CURR, company
    from BTRANSFERS
    where REF=:btransfer
    into :slodef, :slopoz, :currency, :company;
  for select SYMBFAK, KONTOFK, MA - WINIEN
   from ROZRACH
   where SLODEF = :slodef
     and SLOPOZ = :slopoz
     and MA - WINIEN > 0
     and company = :company
  into :symbfak, :kontofk, :maxamount
  do begin
    cnt = 0;
    select count(*) from BTRANSFERPOS where BTRANSFER = :btransfer
      and SETTLEMENT = :SYMBFAK and ACCOUNT = :kontofk
      into :cnt;
    if(:cnt is null) then cnt = 0;
    if(:cnt = 0) then
      insert into BTRANSFERPOS(BTRANSFER, ACCOUNT, SETTLEMENT,MAXAMOUNT, AMOUNT)
        values (:btransfer, :kontofk, :symbfak, :maxamount, :maxamount);
    else begin
      select amount from BTRANSFERPOS where
        BTRANSFER = :btransfer and ACCOUNT = :kontofk and SETTLEMENT = :symbfak
        into :amount;
      if(:amount > :maxamount) then amount = :maxamount;
      update BTRANSFERPOS set AMOUNT = :amount, MAXAMOUNT = :maxamount where
        BTRANSFER = :btransfer and ACCOUNT = :kontofk and SETTLEMENT = :symbfak;
    end
    execute procedure BTRANSFER_CHECKAMOUNT(:btransfer);
  end
end^
SET TERM ; ^
