--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_SCHEDULER_METHOD_PROCESS(
      NEOS_NOW TIMESTAMP_ID,
      INSTANCE INTEGER_ID = null,
      ACTIVITY_SCHEDULE SYS_SCHEDULE_ID = null)
  returns (
      RET smallint)
   as
begin
  --Zamyka wybrany proces schedulingu lub wszystkie
  ret = 1;
  --exception universal'  '||:instance;
  update sys_schedulehist s
    set stop = :neos_now
    where s.schedule = coalesce(:activity_schedule, s.schedule) and s.ref = coalesce(:instance, s.ref)
      and s.stop is null;
  if ( exists(
               select first 1 1
                 from sys_schedulehist s
                 where s.schedule = coalesce(:activity_schedule, s.schedule)
                   and s.ref = coalesce(:instance, s.ref)
                   and s.stop is null )
      ) then ret = 0;
  suspend;
end^
SET TERM ; ^
