--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NADAJ_RABAT(
      TYPDOKUM varchar(1) CHARACTER SET UTF8                           ,
      REFDOKUM integer,
      RABAT numeric(14,2))
  returns (
      STATUS smallint)
   as
begin
  if (rabat > 100) then exception RABAT_TOHIGH;
  status = 0;
  if (typdokum = 'F') then begin
    update pozfak P set P.rabat = :rabat where P.dokument = :refdokum;
    status = 1;
  end
  else if (typdokum = 'M') then begin
    update dokumpoz P set P.rabat = :rabat where P.dokument = :refdokum;
    status = 1;
  end
  else if (typdokum = 'Z') then begin
    update pozzam P set P.rabat = :rabat where P.zamowienie = :refdokum;
    status = 1;
  end
  else 
    exception WRONG_DOK_TYP;
  suspend;
end^
SET TERM ; ^
