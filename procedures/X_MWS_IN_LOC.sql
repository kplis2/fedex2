--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_IN_LOC(
      MWSCONSTLOC INTEGER_ID)
  returns (
      WH WH_ID,
      NAZWA STRING40,
      GOOD KTM_ID,
      SYMBOLLOC STRING20,
      QUANTITY NUMERIC_14_4,
      RESERVED NUMERIC_14_4,
      BLOCKED NUMERIC_14_4,
      ORDERED NUMERIC_14_4,
      MIXEDPALGROUP SMALLINT_ID,
      MWSPALLOC INTEGER_ID,
      STANCEN INTEGER_ID,
      LOT DOSTAWA_ID,
      SYMBOL STRING120,
      KODKRESKOWY KODKRESKOWY,
      TYP SMALLINT_ID)
   as
begin
  wh = '';
  nazwa = '';
  good = '';
  symbol = '';
  quantity = 0;
  reserved = 0;
  blocked = 0;
  ordered = 0;
  mixedpalgroup = 0;
  mwspalloc = 0;
  stancen = 0;
  lot = 0;
  symbol = '';
  kodkreskowy = '';
  typ = 0;
  symbolloc = '';
-- szukanie kartonów na lokacji
  for
    select 'Karton: '||l.stanowisko, 'Dokument: '||d.symbol, l.x_mwsconstlock, 1, 0
      from listywysdroz_opk l
      join listywysd d on (l.listwysd = d.ref)
      join mwsconstlocs m on (m.symbol = l.x_mwsconstlock)
      where m.ref = :mwsconstloc
    into :good, :nazwa, :symbolloc, :quantity, :typ
   do begin
     suspend;
   end
-- szukanie wózków na lokacji
  for
    select first 1 'Wózek: '||mwc.name, 'Dokument: '||mo.symbol, mw.mwsconstlocsymb, 1, 2
      from mwsordcartcolours mw
      join mwsords mo on (mo.docid = mw.docid)
      join mwscartcolours mwc on (mwc.ref = mw.mwscartcolor)
      join mwsconstlocs m on (m.symbol = mw.mwsconstlocsymb)
      where m.ref = :mwsconstloc and mw.status <2
    into :good, :nazwa, :symbolloc, :quantity, :typ
   do begin
     suspend;
   end
-- szukanie na mwsstock
   for
     select s.wh, t.nazwa, s.good, c.symbol, s.quantity, s.reserved, s.blocked, s.ordered, s.mixedpalgroup, s.mwspalloc, s.stancen, s.lot, d.symbol, t.kodkresk, 1
       from mwsstock s
       left join towary t on (t.ktm = s.good) left join mwsconstlocs c on (c.ref = s.mwsconstloc)
       left join dostawy d on d.ref = s.lot
       where s.mwsconstloc = :mwsconstloc
     into :wh, :nazwa, :good, :symbolloc, :quantity, :reserved, :blocked, :ordered, :mixedpalgroup, :mwspalloc, :stancen, :lot, :symbol, :kodkreskowy, :typ
   do begin
     suspend;
   end
end^
SET TERM ; ^
