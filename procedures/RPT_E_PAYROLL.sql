--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_E_PAYROLL(
      PAYROLL integer,
      EMPLOYEE integer)
  returns (
      NUM1 integer,
      NAME1 varchar(22) CHARACTER SET UTF8                           ,
      VAL1 numeric(14,2),
      REPPAY1 integer,
      NUM2 integer,
      NAME2 varchar(22) CHARACTER SET UTF8                           ,
      VAL2 numeric(14,2),
      REPPAY2 integer,
      NUM3 integer,
      NAME3 varchar(22) CHARACTER SET UTF8                           ,
      VAL3 numeric(14,2),
      REPPAY3 integer,
      NUM4 integer,
      NAME4 varchar(22) CHARACTER SET UTF8                           ,
      VAL4 numeric(14,2),
      REPPAY4 integer,
      NUM5 integer,
      NAME5 varchar(22) CHARACTER SET UTF8                           ,
      VAL5 numeric(14,2),
      REPPAY5 integer)
   as
DECLARE VARIABLE COL1 SMALLINT;
DECLARE VARIABLE COL2 SMALLINT;
DECLARE VARIABLE COL3 SMALLINT;
DECLARE VARIABLE COL4 SMALLINT;
DECLARE VARIABLE COL5 SMALLINT;
DECLARE VARIABLE MAXCOL SMALLINT;
begin
  select count(*)
    from eprpos
      join ecolumns C on (C.number=eprpos.ecolumn)
    where payroll=:payroll and employee=:employee and ecolumn<1000 and reppay>0
    into :col1;
  maxcol = col1;

  select count(*)
    from eprpos
      join ecolumns C on (C.number=eprpos.ecolumn)
    where payroll=:payroll and employee=:employee and ecolumn>=1000 and ecolumn<4000  and reppay>0
    into :col2;
  if (col2>maxcol) then
    maxcol = col2;

  select count(*)
    from eprpos
      join ecolumns C on (C.number=eprpos.ecolumn)
    where payroll=:payroll and employee=:employee and ecolumn>4000 and ecolumn<5900  and reppay>0
    into :col3;
  if (col3>maxcol) then
    maxcol = col3;

  select count(*)
    from eprpos
      join ecolumns C on (C.number=eprpos.ecolumn)
    where payroll=:payroll and employee=:employee and ecolumn>5900 and ecolumn<8000  and reppay>0
    into :col4;
  if (col4>maxcol) then
    maxcol = col4;

  select count(*)
    from eprpos
      join ecolumns C on (C.number=eprpos.ecolumn)
    where payroll=:payroll and employee=:employee and ecolumn>8000 and ecolumn<9050 and reppay>0
    into :col5;
  if (col5>maxcol) then
    maxcol = col5;

  while (maxcol>0) do
  begin
    if (col1>=maxcol) then
      select first 1 skip (:col1-:maxcol) c.number, c.name, P.pvalue, c.reppay
        from eprpos P
          join ecolumns C on (C.number=P.ecolumn)
        where payroll=:payroll and employee=:employee
          and ecolumn<1000 and reppay>0
        order by P.ecolumn
        into :num1, :name1, val1, :REPPAY1;

    if (col2>=maxcol) then
      select first 1 skip (:col2-:maxcol) c.number, c.name, P.pvalue, c.reppay
        from eprpos P
          join ecolumns C on (C.number=P.ecolumn)
        where payroll=:payroll and employee=:employee
          and ecolumn>=1000 and ecolumn<4000 and reppay>0
        order by P.ecolumn
        into :num2, :name2, val2, :REPPAY2;

    if (col3>=maxcol) then
      select first 1 skip (:col3-:maxcol) c.number, c.name, P.pvalue, c.reppay
        from eprpos P
          join ecolumns C on (C.number=P.ecolumn)
        where payroll=:payroll and employee=:employee
          and ecolumn>4000 and ecolumn<5900 and reppay>0
        order by P.ecolumn
        into :num3, :name3, val3, :REPPAY3;

    if (col4>=maxcol) then
      select first 1 skip (:col4-:maxcol) c.number, c.name, P.pvalue, c.reppay
        from eprpos P
          join ecolumns C on (C.number=P.ecolumn)
        where payroll=:payroll and employee=:employee
           and ecolumn>5900 and ecolumn<8000 and reppay>0
        order by P.ecolumn
        into :num4, :name4, :val4, :REPPAY4;

    if (col5>=maxcol) then
      select first 1 skip (:col5-:maxcol) c.number, c.name, P.pvalue, c.reppay
        from eprpos P
          join ecolumns C on (C.number=P.ecolumn)
        where payroll=:payroll and employee=:employee
           and ecolumn>8000 and ecolumn<9050 and reppay>0
        order by P.ecolumn
        into :num5, :name5, :val5, :REPPAY5;

    suspend;
    maxcol = maxcol - 1;
  end
end^
SET TERM ; ^
