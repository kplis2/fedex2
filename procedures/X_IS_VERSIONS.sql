--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_IS_VERSIONS(
      BARCODE varchar(100) CHARACTER SET UTF8                           )
  returns (
      ISVERSIONS INTEGER_ID)
   as
declare variable CNT INTEGER_ID;
begin
  ISVERSIONS = 0;
  select count(distinct K.WERSJAREF)
    from TOWKODKRESK K
    left join WERSJE W on (W.REF = K.WERSJAREF)
    where K.KODKRESK = :BARCODE and
        coalesce(char_length(:BARCODE), 0) > 5
  group by K.KODKRESK
  into CNT;

  if (CNT > 1) then
    ISVERSIONS = 1;

  suspend;
end^
SET TERM ; ^
