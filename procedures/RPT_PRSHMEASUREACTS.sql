--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_PRSHMEASUREACTS(
      IPRSCHEDOPER integer)
  returns (
      RNUMBER integer,
      RACTIVITY varchar(255) CHARACTER SET UTF8                           ,
      RDESCRIPT varchar(1024) CHARACTER SET UTF8                           ,
      RDEFAULTVALUE varchar(22) CHARACTER SET UTF8                           ,
      RMEASURETOOL varchar(20) CHARACTER SET UTF8                           ,
      RAMOUNT numeric(14,4),
      RSHOPDESC varchar(255) CHARACTER SET UTF8                           ,
      RSHOPNR smallint,
      RKTM varchar(40) CHARACTER SET UTF8                           ,
      RSHEETSYMBOL varchar(20) CHARACTER SET UTF8                           ,
      RSHEETSYMBOLVER varchar(20) CHARACTER SET UTF8                           )
   as
declare variable vprshoper integer;
--declare variable vnumber integer;
--declare variable vactivity varchar(255);
--declare variable vdescript varchar(1024);
--declare variable vdefaultvalue varchar(22);
declare variable vmeasuretools varchar(1024);
declare variable vstrlen integer;
declare variable vpos integer;
begin
  select o.shoper
    from prschedopers o
    where o.ref = :iprschedoper
    into :vprshoper;
  select o.descript, o.number, k.ktm, k.symbol, k.symbolver
    from prshopers o
      join prsheets k on (k.ref = o.sheet)
    where o.ref = :vprshoper
    into :rshopdesc, :rshopnr, :rktm, :rsheetsymbol, :rsheetsymbolver;
  for select a.number, s.nazwa, a.descript, a.defaultvalue, a.measuretools
    from prshmeasureacts a
     join slopoz s on (s.ref = a.activity)
    where a.prshoper = :vprshoper
    into :rnumber, :ractivity, :rdescript, :rdefaultvalue, :vmeasuretools
  do begin
    vstrlen = coalesce(char_length(:vmeasuretools),0); -- [DG] XXX ZG119346
    while (:vstrlen > 1) do begin
      vmeasuretools = substring(:vmeasuretools from 2 for :vstrlen); --ucialem 1. srednik
      vpos = position(';' in :vmeasuretools);
      vstrlen = coalesce(char_length(:vmeasuretools),0); -- [DG] XXX ZG119346
      rmeasuretool = substring(:vmeasuretools from 1 for :vpos-1); -- 1. symbol
      vmeasuretools = substring(:vmeasuretools from :vpos for :vstrlen); --wyciety 1. symbol
      vstrlen = coalesce(char_length(:vmeasuretools),0);  -- [DG] XXX ZG119346
      select n.amount
        from prshmeasuretools n
        where n.prtooltype = :rmeasuretool
          and n.prshoper = :vprshoper
        into :ramount;
      suspend;
      rnumber = null;
      ractivity = null;
      rdescript = null;
      rdefaultvalue = null;
    end
  end
end^
SET TERM ; ^
