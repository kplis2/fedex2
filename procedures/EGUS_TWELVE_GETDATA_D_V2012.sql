--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EGUS_TWELVE_GETDATA_D_V2012(
      GYEAR varchar(4) CHARACTER SET UTF8                           ,
      EMPLOYEE integer)
  returns (
      D1 numeric(4,2),
      D2 numeric(14,2),
      D3 numeric(14,2),
      D4 numeric(14,2),
      D5 numeric(14,2),
      D6 numeric(14,2),
      D7 numeric(14,2),
      D8 numeric(14,2),
      D9 numeric(14,2),
      D10 numeric(14,2),
      D11 numeric(14,2),
      D12 numeric(14,2),
      D13 numeric(14,2),
      D14 numeric(14,2),
      D15 numeric(14,2))
   as
declare variable PERIOD varchar(6);
declare variable PNOMWORKDAYS integer;
declare variable FROMDATE date;
declare variable TODATE date;
declare variable WDIMDAYS numeric(14,2);
declare variable PWDIMDAYS numeric(14,2);
declare variable BRUTTO numeric(14,2);
declare variable OUTBRUTTO numeric(14,2);
declare variable D2TMP numeric(14,2);
declare variable vc integer;
declare variable vc2 integer;
begin
/*MWr Personel: procedura generuje dane do sprawozdania GUS Z-12 (na rok 2010)
  dla zadanego pracownika EMPLOYEE. Odpowiada za czesc D raportu - czas pracy
  oraz wynagr. za rok GYEAR. Specyfikacja zmiennych wyjsciowych w dokumenacji*/

  d2tmp = 0;
  wdimdays = 0;
  period = gyear || '01';

/*D1: suma miesiecznych wskaznikow wymiaru czasu pracy za rok GYEAR oraz */
  while (substring(period from 1 for 4) = gyear) do
  begin
    execute procedure period2dates(period)
      returning_values :fromdate, :todate;

    select wdimdays, nomworkdays
      from egus_twelve_worktime(1, :employee, :fromdate, :todate)
      into :pwdimdays, :pnomworkdays;

    wdimdays = wdimdays + pwdimdays;
    d2tmp = d2tmp + pnomworkdays;

    execute procedure e_func_periodinc(period,1)
      returning_values :period;
  end

  if (d2tmp > 0) then
    d1 = (12.00 * wdimdays) / (1.00 * d2tmp);

  fromdate = cast(gyear || '/1/1' as date);
  todate = cast(gyear || '/12/31' as date);

/*D2: czas faktycznie przepracowny w godz nominalnych,
  D3: czas faktycznie przepracowny w godz nadliczbowych,
  D4: czas nieprzepracowany ogolem,
  D5: czas nieprzepracowany oplacony tylko przez zaklad pracy (D4 >= D5)*/
  select workedhours, extrahours, notworkedhours, notwhours_works
    from egus_twelve_worktime(2, :employee, :fromdate, :todate)
    into :d2, :d3, :d4, :d5;

--D6: liczba dni urlopow wypoczynkowych wykorzystanych w roku
  select limitconsd from evaclimits
    where employee = :employee and vyear = :gyear
    into :d6;
  d6 = coalesce(d6,0);

  if (d6 = 0) then
  begin
    execute procedure get_config('VACCOLUMN', 2) returning_values :vc;
    execute procedure get_config('VACREQCOLUMN', 2) returning_values :vc2;
  
    select sum(workdays) from eabsences
      where employee = :employee
        and ayear = :gyear
        and ecolumn in (:vc,:vc2)
        and correction in (0,2)
     into: d6;
    d6 = coalesce(d6,0);
  end

--WYNAGRODZENIA ZA CALY ROK ---------------------------------------------------
  select sum(case when p.ecolumn = 4000 then p.pvalue else 0 end) --brutto do wyliczenia D8
    , sum(case when p.ecolumn in (1600,2100,2110,2120,2200,2300,2400)then p.pvalue else 0 end) --wykluczenia z brutto
    , 0 --D9: tylko premia REGULAMINOWA (np 1/3 premi za kwartal) [BRAK W SYSTEMIE]
    , sum(case when p.ecolumn in (1400,2000) then p.pvalue else 0 end) --D10: premie i nagrody uznaniowe
    , sum(case when p.ecolumn in (1200,1210) then p.pvalue else 0 end) --D11: wyn. za prace w godz. nadliczowych
    , 0 --D12: honoraria [BRAK W SYSTEMIE]
    , 0 --D13: dodatkowe wynagr. dla pracownikow budzetowki [BRAK W SYSTEMIE]
    , 0 --D14: udzial w zysku lub w nadwyzce bilansowej w spoldzielniach [BRAK W SYSTEMIE]
    from epayrolls r
      join eprpos p on (p.payroll = r.ref and r.cper starting with :gyear and r.empltype = 1)
    where p.employee = :employee
    into :brutto, :outbrutto, :d9, :d10, :d11, :d12, :d13, :d14;

  d8 = brutto - outbrutto - d9 - d10 - d11 - d12 - d13 - d14;
  d7 = d8 + d11;

  suspend;
end^
SET TERM ; ^
