--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_GOODS_CLR_SET(
      DOCID integer,
      OPERATOR integer,
      ATR1 varchar(255) CHARACTER SET UTF8                           ,
      ATR2 varchar(255) CHARACTER SET UTF8                           ,
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      QUANTITYPLUS numeric(15,4),
      QUANTITYMINUS numeric(15,4),
      GROUPID integer)
   as
begin
  if (quantityplus is null) then quantityplus = 0;
  if (quantityminus is null) then quantityminus = 0;
  insert into mwssupclr (docidfrom, atr1from, atr2from, goodfrom, versfrom, quantityfrom, quantityto, regoper, regdate, groupid)
    values (:docid, :atr1, :atr2, :good, :vers, :quantityplus, :quantityminus, :operator, current_timestamp(0), :groupid);
end^
SET TERM ; ^
