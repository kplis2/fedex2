--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYNCHRONIZE_SLOPOZ(
      PATTERN BKACCOUNTS_ID,
      DEST BKACCOUNTS_ID,
      OLDNAME STRING255,
      INTERNAL SMALLINT_ID = 0)
   as
declare variable VKOD varchar(40);
declare variable VNAZWA STRING;
declare variable VKONTOKS KONTO_ID;
declare variable VCRM smallint;
declare variable VTOKEN smallint;
declare variable VSTATE STATE;
declare variable VAKT SMALLINT_ID;
declare variable VCPODMIOT CPODMIOTY_ID;
declare variable VMASTERREF INTEGER_ID;
declare variable VMASTERIDENT INTEGER_ID;
declare variable VOPIS STRING;
declare variable VDFLAGS STRING80;
declare variable VRIGHTS STRING255;
declare variable VRIGHTSGROUP STRING255;
begin
  -- Procedura synchronizująca dwa slopozy. Flaga internal = 1 powoduje uruchomienie upodatu bez kontroli praw przy
  -- sychronizacji - powinno być stosowane tylko dla wzorcowego planu kont.
  select   kod, nazwa, kontoks, crm, token, state, akt, cpodmiot, masterref,
    masterident, opis, dflags, rights, rightsgroup
  from slopoz
  where ref = :pattern
  into  :vkod, :vnazwa, :vkontoks, :vcrm, :vtoken, :vstate, :vakt, :vcpodmiot, :vmasterref,
    :vmasterident, :vopis, :vdflags, :vrights, :vrightsgroup;
  update slopoz
    set kod = :vkod, nazwa = iif(nazwa = :oldname,:vnazwa, nazwa), kontoks = :vkontoks, crm = :vcrm, token = :vtoken, state = :vstate, akt = :vakt, cpodmiot = :vcpodmiot, masterref = :vmasterref,
   masterident = :vmasterident, opis = :vopis, dflags = :vdflags, rights = :vrights, rightsgroup = :vrightsgroup, internalupdate = :internal
   where ref = :dest;
end^
SET TERM ; ^
