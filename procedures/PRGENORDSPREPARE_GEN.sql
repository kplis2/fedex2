--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRGENORDSPREPARE_GEN(
      GRUPAGEN integer,
      ACCUMULATIONMODE smallint = 0,
      PRORD_ADD INTEGER_ID = 0)
   as
declare variable PRORD integer;
declare variable MODE smallint;
declare variable PRSHEET integer;
declare variable AMOUNT numeric(14,4);
declare variable REJESTR varchar(3);
declare variable NAGZAM integer;
declare variable POZZAM integer;
declare variable PRNAGZAM integer;
declare variable PRPOZZAM integer;
declare variable PRSCHEDZAMDATE date;
declare variable TMP integer;
declare variable PRORDOUT integer;
declare variable AUTOSCHED smallint;
declare variable PREPREF integer;
declare variable STATUS smallint;
begin
  PRORDOUT = :PRORD_ADD;
  for
    select REF, PRORD, MODE, PRSHEET, AMOUNT, REJESTR, NAGZAM, POZZAM,
        PRNAGZAM, PRPOZZAM, GRUPAGEN, PRSCHEDZAMDATE, AUTOSCHED, STATUS
      from PRGENORDSPREPARE P
      where P.GRUPAGEN = :GRUPAGEN
       -- and P.STATUS < 2
      into :PREPREF, :PRORD, MODE, PRSHEET, AMOUNT, REJESTR, NAGZAM, POZZAM,
        PRNAGZAM, PRPOZZAM, GRUPAGEN, PRSCHEDZAMDATE, AUTOSCHED, STATUS
  do begin
    if(:STATUS > 1) then exception prnagzam_error 'Pozycja zostala już dodana do zlecenia.';
    if (:amount <= 0) then exception prnagzam_error 'Nie można generować zleceń na ilosci ujemne!';
    else if (:amount is null) then exception prnagzam_error 'Nie podano ilosci do generowania!';
    if (ACCUMULATIONMODE = 1 and :prordout is not null) then
    begin
      if (exists (select first 1 1 from prschedzam sz where sz.zamowienie = :prordout)) then
        exception prnagzam_error 'Zlecenie jest już zaharmonogramowane.';
      PRORD = :PRORDOUT;
      if(coalesce(PRORD,0) = 0) then
      begin
        select first 1 coalesce(max(pzp.zamowienie),0)
          from pozzam pzs
            left join relations r on pzs.ref = r.srefto and r.stableto = 'POZZAM' and r.relationdef = 1
            left join pozzam pzp on pzp.ref = r.sreffrom and r.stablefrom = 'POZZAM'
          where pzs.zamowienie = :nagzam
            and not exists (select first 1 1 from prschedzam sz where sz.zamowienie = pzp.zamowienie)
          into :prord;
      end
    end
    execute procedure PR_GEN_PRODORDS(PRORD, MODE, PRSHEET, AMOUNT, REJESTR,
      NAGZAM, POZZAM, PRNAGZAM, PRPOZZAM, 0, 0, 0, 1, :GRUPAGEN, 0, 0)
      returning_values :PRORDOUT, :TMP, :TMP;

    if(:AUTOSCHED = 1) then
      execute procedure PRSCHED_ADD_PRORD(null ,:PRORDOUT);
    update PRGENORDSPREPARE P set P.STATUS = 2, P.AMOUNTGEN = P.AMOUNTGEN + :AMOUNT, P.PRORD = :PRORDOUT
      where P.REF = :PREPREF;
  end
end^
SET TERM ; ^
