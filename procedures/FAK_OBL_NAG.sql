--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FAK_OBL_NAG(
      REFFAK integer)
   as
declare variable gr_vat varchar(5);
declare variable sposobl char(1);
declare variable sumwart decimal(14,2);
declare variable sumvat decimal(14,2);
declare variable psumvat decimal(14,2);
declare variable bn char(1);
declare variable sumkaucja decimal(14,2);
declare variable sumwartbru decimal(14,2);
declare variable sumwartnet decimal(14,2);
declare variable psumwartbru decimal(14,2);
declare variable psumwartnet decimal(14,2);
declare variable sumkaucjazl decimal(14,2);
declare variable sumwartbruzl decimal(14,2);
declare variable sumwartnetzl decimal(14,2);
declare variable psumwartbruzl decimal(14,2);
declare variable psumwartnetzl decimal(14,2);
declare variable vat_val decimal(14,2);
declare variable wart decimal(14,2);
declare variable kurs numeric(14,4);
declare variable pkurs numeric(14,4);
declare variable waga numeric(14,3);
declare variable local smallint;
declare variable korsamo smallint;
declare variable pesumwartnet decimal(14,2);
declare variable pesumwartbru decimal(14,2);
declare variable pesumwartnetzl decimal(14,2);
declare variable pesumwartbruzl decimal(14,2);
begin
  execute procedure GETCONFIG('FAKSUMNAG') returning_values :sposobl;
  select bn from nagfak where nagfak.ref = :reffak into :bn;
  execute procedure check_local('NAGFAK',:reffak) returning_values :local;
  if(:local = 1) then begin
    if(:sposobl = '1') then begin
      -- liczenie po sumie z vato-pozycji
      select sum(SUMWARTNET), sum(SUMWARTBRU), sum(PSUMWARTNET), sum(PSUMWARTBRU),
        sum(SUMWARTNETZL), sum(SUMWARTBRUZL), sum(PSUMWARTNETZL), sum(PSUMWARTBRUZL),
        sum(PESUMWARTNET), sum(PESUMWARTBRU), sum(PESUMWARTNETZL), sum(PESUMWARTBRUZL)
      from ROZFAK where dokument = :reffak
      into :sumwartnet, :sumwartbru, :psumwartnet, :psumwartbru,
        :sumwartnetzl, :sumwartbruzl, :psumwartnetzl, :psumwartbruzl,
        :pesumwartnet, :pesumwartbru, :pesumwartnetzl, :pesumwartbruzl;
    end else begin
      -- liczenie po sumie z pozycji
      select sum(wartnet), sum(wartbru), sum(pwartnet), sum(pwartbru),
        sum(wartnetzl), sum(wartbruzl), sum(pwartnetzl), sum(pwartbruzl)
      from POZFAK where dokument = :reffak and opk = 0
      into :sumwartnet, :sumwartbru, :psumwartnet, :psumwartbru,
        :sumwartnetzl, :sumwartbruzl, :psumwartnetzl, :psumwartbruzl;
      -- korekty samodzielne nie moga byc zaliczkowe wiec pola sum  i ESUM sa takie same
      -- dlatego kopiuje ESUM z SUM
      select  PSUMWARTNET, PSUMWARTBRU, PSUMWARTNETZL, PSUMWARTBRUZL from NAGFAK
        where ref = :REFFAK
        into :pesumwartnet, :pesumwartbru, :pesumwartnetzl, :pesumwartbruzl;
    end
    sumkaucja = 0;
    if(:bn = 'N') then
      select sum(wartnet) from POZFAK where dokument = :reffak and opk = 1 into :sumkaucja;
    else
      select sum(wartbru) from POZFAK where dokument = :reffak and opk = 1 into :sumkaucja;
    if(:sumkaucja is null) then sumkaucja = 0;
    select N.kurs, N.pkurs, T.korsamo  from NAGFAK N
      join TYPFAK T on T.SYMBOL = N.TYP
      where ref = :reffak
      into :kurs, :pkurs, :korsamo;
    if(:kurs is null) then kurs = 0;
    --sumwartnetzl = :sumwartnet * :kurs;
    --sumwartbruzl = :sumwartbru * :kurs;
    --psumwartnetzl = :psumwartnet * :pkurs;
    --psumwartbruzl = :psumwartbru * :pkurs;*/
    sumkaucjazl = :sumkaucja * :kurs;
    --select sum(WAGA) from POZFAK where dokument = :reffak into :waga;
    if (:korsamo=1) then begin -- PR09761
      update NAGFAK set
        SUMWARTNET    = :sumwartnet,    SUMWARTBRU    = :sumwartbru,
        SUMWARTNETZL  = :sumwartnetzl,  SUMWARTBRUZL  = :sumwartbruzl,
        PSUMWARTNET   = :psumwartnet,   PSUMWARTBRU   = :psumwartbru,
        PSUMWARTNETZL = :psumwartnetzl, PSUMWARTBRUZL = :psumwartbruzl,
        SUMKAUCJA     = :sumkaucja,     SUMKAUCJAZL   = :sumkaucjazl, --, WAGA = :waga
        PESUMWARTNET  =:pesumwartnet,   PESUMWARTBRU  =:pesumwartbru,
        PESUMWARTNETZL=:pesumwartnetzl, PESUMWARTBRUZL=:pesumwartbruzl
        where ref = :reffak;
    end else begin
      update NAGFAK set
        SUMWARTNET    = :sumwartnet,    SUMWARTBRU    = :sumwartbru,
        SUMWARTNETZL  = :sumwartnetzl,  SUMWARTBRUZL  = :sumwartbruzl,
        PSUMWARTNET   = :psumwartnet,   PSUMWARTBRU   = :psumwartbru,
        PSUMWARTNETZL = :psumwartnetzl, PSUMWARTBRUZL = :psumwartbruzl,
        SUMKAUCJA     = :sumkaucja,     SUMKAUCJAZL   = :sumkaucjazl --, WAGA = :waga
        where ref = :reffak;
    end
  end
end^
SET TERM ; ^
