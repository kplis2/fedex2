--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_FEPOMOST(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable fromdate date;
declare variable todate date;
declare variable birthdate date;
declare variable person integer;
declare variable company integer;
begin
/*MWr: procedura zwraca informacje czy za pracownika ma być odprowadzana skadka
       na Fundusz Emerytur Pomostowych. Nie dotyczy zleceniobiorcow.*/

  ret = 0;

  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  select p.birthdate, e.person, e.company
    from employees e
    join persons p on (p.ref = e.person and e.ref = :employee)
    into :birthdate, :person, :company;

  --skladka obowiazuje od poczatku roku 2010
  if (fromdate >= '2010-01-01') then
  begin
    /*dla osob, ktore urodzily sie po 31 grudnia 1948 r. oraz wykonujacych
      prace w szczegolnych warunkach lub o szczegolnym charakterze*/
    if (exists(select first 1 1 from get_especconditwork(:person, :fromdate, :todate, :company, 0)
          where employee = :employee) and coalesce(:birthdate,current_date) >= '1949-01-01')
    then
      ret = 1;
  end
  suspend;
end^
SET TERM ; ^
