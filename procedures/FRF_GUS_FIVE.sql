--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_GUS_FIVE(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      COL smallint)
  returns (
      AMOUNT integer)
   as
declare variable FRVHDR integer;
declare variable FRPSN integer;
declare variable FRCOL integer;
declare variable YEARID integer;
declare variable CACHEPARAMS varchar(255);
declare variable DDPARAMS varchar(255);
declare variable COMPANY integer;
declare variable FRVERSION integer;
declare variable MONTHID integer;
declare variable LASTDAY date;
declare variable FIRSTDAY date;
declare variable SYMBOL varchar(15);
begin

  if (frvpsn = 0) then exit;
  select frvhdr, frpsn, frcol from frvpsns
    where ref = :frvpsn
    into :frvhdr, :frpsn, :frcol;

  select company, frversion from frvhdrs
    where ref = :frvhdr
    into :company, :frversion;

  yearid = cast(substring(period from 1 for 4) as integer);
  cacheparams = frpsn||';'||frcol||';'||yearid||';'||company;
  ddparams = '';

  if (not exists (select first 1 1 from frvdrilldown where functionname = 'GUS_FIVE'
        and cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr)
  ) then begin

    monthid = cast(substring(period from 5 for 2) as integer);
    if (monthid <= 3) then lastday = cast(yearid || '/' || 3 || '/31' as date);
    else if (monthid <= 6) then lastday = cast(yearid || '/' || 6 || '/30' as date);
    else if (monthid <= 9) then lastday = cast(yearid || '/' || 9 || '/30' as date);
    else lastday = cast(yearid || '/' || 12 || '/31' as date);
    firstday = cast(yearid || '/' || (extract(month from lastday) - 2) || '/1' as date);

    select symbol from frpsns
      where ref = :frpsn
      into :symbol;

    select count(distinct p.ref) from persons p
        join employees e on (e.person = p.ref)
        join employment em on (em.employee = e.ref)
        join edictworkposts w on (em.workpost = w.ref)
        join edictguscodes g on (w.guscode = g.ref and g.dicttype = 2)
      where e.company = :company
        and em.fromdate <= :lastday
        and (em.todate >= :lastday or em.todate is null)
        and substring(g.code from 1 for 2) = :symbol
        and (:col = 1 or (:col = 2 and p.sex = 0))
        and p.ref not in (select person from gus_persons_on_unpaidvac(:firstday, :lastday, :company))
      into :amount;
  end else
    select first 1 fvalue from frvdrilldown where cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr
      into :amount;

  amount = coalesce(amount,0);
  insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
     values (:frvhdr, :frpsn, :frcol, 'GUS_FIVE', :cacheparams, :ddparams, :amount, :frversion);

  suspend;
end^
SET TERM ; ^
