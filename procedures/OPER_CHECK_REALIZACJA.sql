--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_CHECK_REALIZACJA(
      CRTNEW integer,
      ZAM integer,
      HISTZAM integer,
      REJDYSP varchar(10) CHARACTER SET UTF8                           ,
      TYPDYSP varchar(10) CHARACTER SET UTF8                           ,
      CHECKSER integer)
  returns (
      STATUS integer,
      NEWZAM integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      SYMBDYSP varchar(255) CHARACTER SET UTF8                           )
   as
declare variable realizacja_mniejsza smallint;
declare variable empty smallint;
declare variable wydania smallint;
declare variable ilreal decimal(14,4);
declare variable nrreal decimal(14,4);
declare variable nrsreal integer;
declare variable stan char(1);
declare variable operator integer;
declare variable magazyn varchar(3);
declare variable magdocl varchar(3);
declare variable magzrodl varchar(3);
declare variable pozzamref integer;
declare variable ildysp numeric(14,4);
declare variable emptydysp integer;
declare variable emptydyspout integer;
declare variable opozzamref integer;
begin
  realizacja_mniejsza = 0;
  status = -1;
  symbol = '';
  newzam = -1;
  nrreal = 0;
  empty=1;
  emptydysp = 1;
  emptydyspout = 1;
  ildysp = 0;
  select TYPZAM.WYDANIA from NAGZAM left join TYPZAM on (NAGZAM.typzam = TYPZAM.symbol) where NAGZAM.ref = :zam into :wydania;

  if(:wydania is null)then wydania = 0;
  SYMBDYSP = '';
  /* wystawienie dyspozycji*/
  for select POZZAM.MAGAZYN, POZZAMDYSP.MAGAZYN, POZZAMDYSP.MAG2
      from POZZAMDYSP join POZZAM on(POZZAM.REF = POZZAMDYSP.POZZAM)
      where POZZAM.ZAMOWIENIE = :ZAM
      group by POZZAM.MAGAZYN, POZZAMDYSP.MAGAZYN, POZZAMDYSP.MAG2
   into :magazyn, :magzrodl, :magdocl
  do begin
    select OPERATOR from HISTZAM where REF=:HISTZAM into :operator;
    if(:operator is null) then operator=0;
    execute procedure GEN_REF('NAGZAM') returning_values :newzam;
    select ID from NAGZAM where REF=:zam into :symbol;
    if(:histzam is null or (:histzam = 0))then
      exception OPER_CHECK_REALIZACJA_NOT_HISTZ;
    /* dyspozycja zewntrzne przekazujace - z obcego magazynu na obcy magazyn */
    if(:magdocl <> :magazyn) then begin
      select max(NR_REAL) from NAGZAM where ORG_REF = :ZAM into :nrreal;
      if(:nrreal is null)then nrreal = 0;
      nrreal = nrreal + 1;
      insert into NAGZAM(REF,NR_REAL,NUMER,ID,ZNAKZEWN,REJESTR,TYPZAM,OKRES,KLIENT,UZYKLI,MAGAZYN,MAG2,DATAWE,DATAZM,
       ODBIORCA,DULICA,DNRDOMU,DNRLOKALU,DMIASTO,DKODP,DPOCZTA,SPOSDOST,TERMDOST,SPOSZAP,TERMZAP,
       RABAT,UWAGI,STAN/*zawsze nie zablokowany */, DOSTAWA,/*parametry faktury bedzie miala realizacja wlasne*/
       KOSZTDOST,FLAGI,WYSYLKA, TYP,
       WALUTOWE, WALUTA, TABKURS, KURS,
       SPRZEDAWCA, OPERATOR, REFZRODL, SYMBZRODL)
       select :newzam,:nrsreal,NULL,NULL,ZNAKZEWN,:rejdysp,:typdysp,OKRES,KLIENT,UZYKLI,:magzrodl,:magdocl,current_timestamp(0),DATAZM,
       ODBIORCA,DULICA,DNRDOMU,DNRLOKALU,DMIASTO,DKODP,DPOCZTA,SPOSDOST,TERMDOST,SPOSZAP,TERMZAP,
       RABAT,UWAGI,'N',DOSTAWA,
       KOSZTDOST,FLAGI,WYSYLKA, 0,
       WALUTOWE,WALUTA,TABKURS,KURS,
       SPRZEDAWCA,:operator, REF, SYMBZRODL
       from NAGZAM where REF=:zam;
      /* zalozenie pozycji dyspozycji */
      insert into POZZAM(ZAMOWIENIE,MAGAZYN,MAG2,KTM,WERSJA,
                      ILOSC,ILREAL,ILZREAL,CENACEN,RABAT,CENANET,CENABRU,
                      WARTNET,WARTBRU,WERSJAREF,PREC)
         select :newzam,:magzrodl,:magdocl,POZZAM.KTM,POZZAM.WERSJA,
                       POZZAMDYSP.ilosc,POZZAMDYSP.ilosc,POZZAMDYSP.ilosc,POZZAM.CENACEN,POZZAM.RABAT,POZZAM.CENANET,POZZAM.CENABRU,
                       0,0,POZZAM.WERSJAREF,POZZAM.PREC
         from POZZAM join POZZAMDYSP on( POZZAMDYSP.POZZAM = POZZAM.REF) where
            POZZAMDYSP.MAGAZYN = :magzrodl and POZZAMDYSP.MAG2 = :magdocl and
            POZZAMDYSP.ilosc > 0 and POZZAM.ZAMOWIENIE=:ZAM
            order by POZZAM.NUMER;
      /* dolozenie ilosci do realizacji */
      for select POZZAM.REF, sum(POZZAMDYSP.ILOSC)
        from POZZAM join POZZAMDYSP on ( POZZAMDYSP.pozzam = POZZAM.REF)
        where POZZAM.MAGAZYN = :MAGAZYN and POZZAMDYSP.MAGAZYN = :magzrodl and POZZAMDYSP.MAG2 = :magdocl and POZZAM.ZAMOWIENIE = :zam
        group by POZZAM.REF
        into :pozzamref, :ILREAL
      do begin
          if(:ilreal is null) then ilreal = 0;
          update POZZAM set ILZREAL = ILZREAL + :ilreal where ref = :pozzamref;
          ilreal = null;
      end
      /* zaznaczenie, ze dyspozycja jest realizaja */
      update NAGZAM set ORG_REF = :zam, ORG_ID=:symbol, ORG_OPER =:histzam where REF=:newzam;
      /* uaktualnienie rekordu histori operacji */
      update HISTZAM set REALOUT = 1 where  REF=:histzam;
      /* zanzaczenie zamowienia, ze ma realizacje czesciowe */
      update NAGZAM set TYP=1 where REF=:zam and TYP = 0;
    end else begin
      /* zalozenie dyspozycji wewnetrznej na wlasny magazyn  z obcego*/
      insert into NAGZAM(REF,NR_REAL,NUMER,ID,ZNAKZEWN,REJESTR,TYPZAM,OKRES,KLIENT,UZYKLI,MAGAZYN,MAG2,DATAWE,DATAZM,
       ODBIORCA,DULICA,DNRDOMU,DNRLOKALU,DMIASTO,DKODP,DPOCZTA,SPOSDOST,TERMDOST,SPOSZAP,TERMZAP,
       RABAT,UWAGI,STAN/*zawsze nie zablokowany */, DOSTAWA,/*parametry faktury bedzie miala realizacja wlasne*/
       KOSZTDOST,FLAGI,WYSYLKA, TYP,
       WALUTOWE, WALUTA, TABKURS, KURS,
       SPRZEDAWCA, OPERATOR, REFZRODL, SYMBZRODL)
       select :newzam,NULL,NULL,NULL,ZNAKZEWN,:rejdysp,:typdysp,OKRES,KLIENT,UZYKLI,:magzrodl,:magdocl,current_timestamp(0),DATAZM,
       ODBIORCA,DULICA,DNRDOMU,DNRLOKALU,DMIASTO,DKODP,DPOCZTA,SPOSDOST,TERMDOST,SPOSZAP,TERMZAP,
       RABAT,UWAGI,'N',DOSTAWA,
       KOSZTDOST,FLAGI,WYSYLKA, 0,
       WALUTOWE,WALUTA,TABKURS,KURS,
       SPRZEDAWCA,:operator, REF, ID
       from NAGZAM where REF=:zam;
      /* zalozenie pozycji dyspozycji */
      insert into POZZAM(ZAMOWIENIE,MAGAZYN,MAG2,KTM,WERSJA,
                      ILOSC,ILREAL,ILZREAL,CENACEN,RABAT,CENANET,CENABRU,
                      WARTNET,WARTBRU,WERSJAREF,PREC)
         select :newzam,:magzrodl,:magdocl,POZZAM.KTM,POZZAM.WERSJA,
                       POZZAMDYSP.ilosc,0,0,POZZAM.CENACEN,POZZAM.RABAT,POZZAM.CENANET,POZZAM.CENABRU,
                       0,0,POZZAM.WERSJAREF,POZZAM.PREC
         from POZZAM join POZZAMDYSP on( POZZAMDYSP.POZZAM = POZZAM.REF) where
            POZZAMDYSP.MAGAZYN = :magzrodl and POZZAMDYSP.MAG2 = :magdocl and
            POZZAMDYSP.ilosc > 0 and POZZAM.ZAMOWIENIE=:ZAM
            order by POZZAM.NUMER;
    end
    symbol = null;
    select ID from NAGZAM where REF=:newzam into :symbol;
    if(:symbol is not null) then
      SYMBDYSP = :SYMBDYSP || :symbol || ';';

  end
   /* doliczenie realizacji do pozycji oryginalnych  -
   jesli byly blokady na zamowieniu macierzystym, to sie troche pozwalnia*/
   update POZZAM set ILZDYSP = ILZDYSP + ILDYSP where ZAMOWIENIE=:zam;
   /* wyzerowanie ilosci dysponowanych  */
   update POZZAM set ILDYSP = 0 where ZAMOWIENIE = :zam;

  /*sprawdzenie, czy ilosci realizacji sie roznia od zamowionych*/
  for select REF,MAGAZYN, ILREAL - (ILOSC - ILZREAL),ILREAL from POZZAM where ZAMOWIENIE=:zam into :pozzamref, :magdocl,:ilreal,:nrreal do begin
    if(:ilreal < 0) then realizacja_mniejsza = 1;
    if(:nrreal > 0) then empty = 0;
    ildysp = null;
    select sum(ilosc) from POZZAMDYSP where POZZAM = :pozzamref and MAG2 = :magdocl into :ildysp;
    if(:ildysp >0 ) then emptydysp = 0;
    ildysp = null;
    select sum(ilosc) from POZZAMDYSP where POZZAM = :pozzamref and MAG2 <> :magdocl into :ildysp;
    if(:ildysp >0 ) then emptydyspout = 0;
  end
  if(:empty = 1 and :emptydysp = 1 and :emptydyspout = 1) then
     exception OPER_CHECK_REALIZACJA_NOT_REAL;
  /*sprawdzenie, czy wszystkie pozycje serializowane mają zgodne ilosci na tabeli serializacji z iloscia realziowana*/
  if(:checkser = 1) then
  for select POZZAM.REF from POZZAM left join DOKUMSER on (DOKUMSER.REFPOZ = POZZAM.REF and DOKUMSER.TYP = 'Z')
    where POZZAM.ZAMOWIENIE = :zam and POZZAM.SERIALIZED = 1
    group by POZZAM.REF
    having max(POZZAM.ILREAL) <> sum(DOKUMSER.ilosc)
    into :pozzamref
  do
    exception OPER_CHECKSER_FAILED;

  if(:realizacja_mniejsza = 1 AND :crtnew = 1 and :empty = 0) then begin
    /* tworzenie nowej realizacji */
    select OPERATOR from HISTZAM where REF=:HISTZAM into :operator;
    if(:operator is null) then operator=0;
    select max(NR_REAL) from NAGZAM where ORG_REF = :ZAM into :nrreal;
    if(:nrreal is null)then nrreal = 0;
    nrreal = nrreal + 1;
    execute procedure GEN_REF('NAGZAM') returning_values :newzam;
    select ID from NAGZAM where REF=:zam into :symbol;
    if(:histzam is null or (:histzam = 0))then
      exception OPER_CHECK_REALIZACJA_NOT_HISTZ;
    /*zalozenie naglowka realizacji */
    nrsreal = cast(:nrreal as integer);
    insert into NAGZAM(REF,NR_REAL,NUMER,ID,ZNAKZEWN,REJESTR,TYPZAM,OKRES,KLIENT,UZYKLI,MAGAZYN,MAG2,DATAWE,DATAZM,
       ODBIORCA,DULICA,DNRDOMU,DNRLOKALU,DMIASTO,DKODP,DPOCZTA,SPOSDOST,TERMDOST,SPOSZAP,TERMZAP,
       RABAT,UWAGI,STAN/*zawsze nie zablokowany */, DOSTAWA,/*parametry faktury bedzie miala realizacja wlasne*/
       KOSZTDOST,FLAGI,WYSYLKA, TYP,
       WALUTOWE, WALUTA, TABKURS, KURS,
       SPRZEDAWCA, OPERATOR, REFZRODL, SYMBZRODL)
       select :newzam,:nrsreal,NUMER,:symbol ||'/'||:nrsreal,ZNAKZEWN,REJESTR,TYPZAM,OKRES,KLIENT,UZYKLI,MAGAZYN,MAG2,DATAWE,DATAZM,
       ODBIORCA,DULICA,DNRDOMU,DNRLOKALU,DMIASTO,DKODP,DPOCZTA,SPOSDOST,TERMDOST,SPOSZAP,TERMZAP,
       RABAT,UWAGI,'N',DOSTAWA,
       KOSZTDOST,FLAGI,WYSYLKA, 2,
       WALUTOWE,WALUTA,TABKURS,KURS,
       SPRZEDAWCA,:operator, NULL, SYMBZRODL
       from NAGZAM where REF=:zam;
    /* zaozenie pozycji realizacji */
    insert into POZZAM(POPREF,ZAMOWIENIE,NUMER,ORD,MAGAZYN,MAG2,KTM,WERSJA,
                      ILOSC,ILREAL,ILZREAL,CENACEN,RABAT,CENANET,CENABRU,
                      WARTNET,WARTBRU,WERSJAREF,OPK,
                      paramn1, paramn2, paramn3, paramn4,
                      paramn5, paramn6, paramn7, paramn8,
                      paramn9, paramn10, paramn11, paramn12,
                      params1, params2, params3, params4,
                      params5, params6, params7, params8,
                      params9, params10, params11, params12,
                      paramd1, paramd2, paramd3, paramd4, PREC)
      select REF,:newzam,NUMER,1,MAGAZYN,MAG2,KTM,WERSJA,
                      ILREAL,ILREAL,ILREAL,CENACEN,RABAT,CENANET,CENABRU,
                      0,0,WERSJAREF,OPK,
                      paramn1, paramn2, paramn3, paramn4,
                      paramn5, paramn6, paramn7, paramn8,
                      paramn9, paramn10, paramn11, paramn12,
                      params1, params2, params3, params4,
                      params5, params6, params7, params8,
                      params9, params10, params11, params12,
                      paramd1, paramd2, paramd3, paramd4, PREC
      from POZZAM where ZAMOWIENIE=:ZAM
      order by POZZAM.NUMER;
    /*przepisanie pozycji serializacyjnych na pozycje realizacji*/
    for select ref, POPREF from POZZAM where ZAMOWIENIE = :newzam into :pozzamref, :opozzamref
    do begin
      update DOKUMSER set REFPOZ = :pozzamref where REFPOZ = :opozzamref and typ = 'Z';
    end
    /*zaznaczenie, ze dane zamowienie jest realizacja*/
    update NAGZAM set ORG_REF = :zam, ORG_ID=:symbol, ORG_OPER =:histzam where REF=:newzam;
    symbol=symbol||'/'||:nrsreal;
    /* przesuniecie blokad*/
    select STAN from NAGZAM where REF=:zam into :stan;
/*    execute procedure REZ_BLOK_ZAM_PRZES(:zam, :newzam, :stan);*/
    /* doliczenie realizacji do pozycji oryginalnych  -
     jesli byly blokady na zamowieniu macierzystym, to sie troche pozwalnia*/
    update POZZAM set ILZREAL = ILZREAL + ILREAL where ZAMOWIENIE=:zam;

    /*ilosci realizowane na zamowieniu realizowanym są kasowane, na realizujacym pozostaja jako slad*/
    update POZZAM set ILREAL =0 where ZAMOWIENIE=:zam;
    /* naniesienie blokad na realizaje czesciowa takich samych jak byly na zamowieniu macierzystym */
    update NAGZAM set STAN=:stan where REF=:newzam AND STAN<>:stan;
    /* uaktualnienie rekordu histori operacji */
    update HISTZAM set REALOUT = 1 where  REF=:histzam;
    /* zanzaczenie zamowienia, ze ma realizacje czesciowe */
    update NAGZAM set TYP=1 where REF=:zam and TYP = 0;
    status = 2;
  end else begin
    /*akceptacja tego samego zamowienia - przepisanie pozycji
      ilosci realizowanych do ilosci zamowienia */
    /* doliczenie realizacji do pozycji oryginalnych */
    update POZZAM set ILZREAL = ILZREAL + ILREAL where ZAMOWIENIE=:zam;
    /*zaznaczenie zamowienia jako realizacja - samego siebie */
    update NAGZAM set org_ref = ref, org_id = id, typ = 3 where REF=:zam;
    /*ilosci realizowane pozostaja jako slad, ile to zrealizowano ta realiazcja */
    status=1;
  end
end^
SET TERM ; ^
