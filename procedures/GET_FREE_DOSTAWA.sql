--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_FREE_DOSTAWA(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      WERSJAREF integer)
  returns (
      DOSTAWA integer,
      CENA numeric(14,2),
      CNT integer)
   as
declare variable dostawap integer;
declare variable cenap numeric(14,2);
declare variable data timestamp;
declare variable datap timestamp;
begin
  dostawa = 0; dostawap = 0;
  cena = 0; cenap = 0;
  cnt = 0;
  data = null;
  if(:magazyn is null or (:magazyn='')) then exit;
  if(:wersjaref is null or (:wersjaref=0)) then exit;
  select count(*) from stanycen where magazyn=:magazyn and wersjaref=:wersjaref and ilosc>zablokow
  into :cnt;
  for select cena, dostawa,datafifo from stanycen
  where magazyn=:magazyn and wersjaref=:wersjaref and ilosc>zablokow
  order by datafifo into :cenap, :dostawap, :datap
  do begin
    if(:data is null or(:data is not null and :datap < :data)) then begin
      data = :datap;
      cena = :cenap;
      dostawa = dostawap;
    end
  end
end^
SET TERM ; ^
