--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_R_ZSTANAL(
      DATA timestamp,
      ACCOUNTMASK ACCOUNT_ID,
      RANGE smallint,
      ANALIT smallint,
      COMPANY integer)
  returns (
      REF integer,
      ACCOUNT ACCOUNT_ID,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      ACCOUNTD ACCOUNT_ID,
      SETTLEMENT varchar(30) CHARACTER SET UTF8                           ,
      PAYDATE timestamp,
      BKREG varchar(15) CHARACTER SET UTF8                           ,
      DOCREF integer,
      DOCNR integer,
      DOCLP integer,
      NR integer,
      NRSETTL integer,
      DOCDATE timestamp,
      DOCSYMBOL varchar(30) CHARACTER SET UTF8                           ,
      DESCRIPT varchar(80) CHARACTER SET UTF8                           ,
      DEBIT numeric(15,2),
      CREDIT numeric(15,2),
      BDEBIT numeric(15,2),
      BCREDIT numeric(15,2),
      ISSETTL smallint,
      CURRENCY varchar(3) CHARACTER SET UTF8                           ,
      CURRATE numeric(14,4),
      CURDEBIT numeric(14,4),
      CURCREDIT numeric(14,4),
      BALCURDEBIT numeric(14,4),
      BALCURCREDIT numeric(14,4),
      BALDEBIT numeric(15,2),
      BALCREDIT numeric(15,2),
      BALCREDITSETT numeric(15,2),
      BALCREDITACC numeric(15,2),
      BALDEBITSETT numeric(15,2),
      BALDEBITACC numeric(15,2),
      ISMORTHAN1 smallint)
   as
declare variable old_symbol varchar(3);
declare variable i_yearid integer;
declare variable old_account ACCOUNT_ID;
declare variable s_account ACCOUNT_ID;
declare variable i_ref integer;
declare variable s_sep varchar(1);
declare variable dictdef integer;
declare variable i_len smallint;
declare variable s_tmp ACCOUNT_ID;
declare variable old_prefix ACCOUNT_ID;
declare variable i_i smallint;
declare variable i_j smallint;
declare variable i_k smallint;
declare variable dictpos integer;
declare variable old_account_s ACCOUNT_ID;
declare variable old_settlement varchar(30);
declare variable bcurdebit numeric(14,2);
declare variable bcurcredit numeric(14,2);
begin
  --TOFIX!!! - styl
  ISMORTHAN1 = 0;
  ref = 0;
  old_symbol = '';
  old_account = '';
  old_prefix = '';
  old_account_s = '';
  old_settlement = '';
  balcreditacc = 0;
  balcreditsett = 0;
  balcreditacc = 0;
  balcreditsett = 0;
  execute procedure create_account_mask(accountmask)
    returning_values accountmask;
  select max(YEARID) from BKPERIODS
    where company = :company and sdate <= :data and fdate >=: data
    into :i_yearid;
  if (accountmask <> '') then accountmask = accountmask || '%';
  if (accountmask is null) then accountmask = '';
  for
    select P.kontofk, P.symbfak, R.dataplat
      from rozrachp P
        left join rozrach R on (R.slodef = P.slodef and R.slopoz = P.slopoz and P.company = R.company
          and R.kontofk = P.kontofk and R.symbfak = P.symbfak)
      where (:accountmask = '' or P.kontofk like :accountmask)
        and cast(P.data as date) <= :data
        --rozrachunki walutowe?
        and (R.walutowy = 1 or :analit = 0 or :analit = 1)
        and R.company = :company
      group by P.kontofk, P.symbfak, R.dataplat
      having :range = 0 or (:range = 1 and sum(P.ma) <> sum(P.winien))
        or (:range = 2 and sum(P.ma) = sum(P.winien))
      into :s_account, :settlement, :paydate
  do begin
    if(:old_account_s <> :s_account) then
      begin
        balcreditacc = 0;
        baldebitacc = 0;
      end
    symbol = cast(substring(:s_account from 1 for 3) as varchar(3));
    account = s_account;
    debit = null;
    credit = null;
    curdebit = null;
    curcredit = null;
    bcurdebit = null;
    bcurcredit = null;
    currate = null;
    currency = null;
    bdebit = null;
    bcredit = null;
    issettl = 0;
    nr = 0;
    select count(*)
      from rozrachp
      where symbfak = :settlement and kontofk = :s_account and company = :company
    into :ismorthan1;
    if (ismorthan1 is null) then
      ismorthan1 = 0;
    if (ismorthan1 = 1) then
      ismorthan1 = 0;
    if (ismorthan1 > 1) then
      ismorthan1 = 1;

    if (old_account <> s_account) then
      nrsettl = 0;
    nrsettl = nrsettl + 1;
    if (old_account <> s_account) then -- szukamy nazwy syntetyki
    begin
      select descript, ref
        from bkaccounts where symbol=:symbol and yearid = :i_yearid
          and bkaccounts.company = :company
        into :descript, :i_ref;
      debit = null;
      credit = null;
      curdebit = null;
      curcredit = null;
      currate = null;
      currency = null;
      accountd = :symbol;
      if (symbol <> s_account) then
      begin
        ref = ref + 1;
        suspend;
      end
      dictdef = null;
      i_i = 0;
      -- zliczam ilosc poziomw nalitycznych
      for
       select separator, dictdef, len
          from accstructure
          where bkaccount = :i_ref
          order by nlevel
          into :s_sep, :dictdef, :i_len
      do
        i_i = i_i + 1;
    end
    if (i_i > 0 and old_account <> s_account) then
    begin
      old_prefix = '';
      if (i_i>0 and old_prefix <> substring(s_account from  1 for coalesce(char_length(s_account),0) - i_len)) then -- [DG] XXX ZG119346
      begin  -- drukowanie opisow posrednich analityk
        i_j = 0;
        i_k = 4;
        for
          select separator, dictdef, len
            from accstructure
            where bkaccount = :i_ref
            order by nlevel
            into :s_sep, :dictdef, :i_len
        do begin
          i_j = i_j + 1;
          if (i_j <= i_i) then
          begin
            if (s_sep <> ',') then
              i_k = i_k +1;
            s_tmp = substring(s_account from  i_k for  i_k + i_len-1);
            accountd = '      ' || s_tmp;
            execute procedure name_from_bksymbol(company, dictdef, s_tmp)
              returning_values descript, dictpos;
            ref = ref + 1;
            suspend;
            i_k = i_k + i_len;
          end
        end
        old_prefix = substring(s_account from  1 for coalesce(char_length(s_account) , 0) - i_len); -- [DG] XXX ZG119346
      end
      s_tmp = substring(s_account from  coalesce(char_length(s_account) ,0) - i_len+1 for coalesce(char_length(s_account),0)); -- [DG] XXX ZG119346
      execute procedure name_from_bksymbol(company, dictdef, s_tmp)
        returning_values descript, dictpos;
    end
    debit = 0;
    credit = 0;
    curdebit = 0;
    curcredit = 0;
    bcurdebit = 0;
    bcurcredit = 0;
    bdebit = 0;
    bcredit = 0;
    for
      select P.kurs, P.waluta, P.winien, P.ma, P.bkdoc, P.winienzl, P.mazl,
          B.docdate, b.bkreg, b.number, d.number, b.symbol
        from ROZRACHP P
          left join BKDOCS B on (P.bkdoc = b.ref)
          left join DECREES D on (P.decrees = d.ref)
        where P.kontofk = :s_account and P.symbfak = :settlement and cast(P.data as date) <= :data
          and P.company = :company and P.winien is not null
        order by P.data
        into :currate, :currency, :curdebit, :curcredit, :docref, :debit, :credit, :docdate, :bkreg, :docnr, :doclp, :docsymbol
    do begin
      if(:old_settlement <> :settlement) then
        begin
          balcreditsett = 0;
          baldebitsett = 0;
        end
      if (analit = 1 or analit = 3) then
      --walutowe i zwykle analitycznie
      begin
        account = s_account;
        if(:debit >= :credit) then
          begin
            baldebit = :debit - :credit;
            balcurdebit = :curdebit - :curcredit;
            baldebitsett = :baldebitsett + :baldebit;
            baldebitacc = :baldebitacc + :baldebit;
          end
        else begin
          baldebit = 0;
          balcurdebit = 0;
        end
        if(:credit > :debit) then
          begin
            balcredit = :credit - :debit;
            balcurcredit = :curcredit - :curdebit;
            balcreditsett = :balcreditsett + :balcredit;
            balcreditacc = :balcreditacc + :balcredit;
          end
        else begin
          balcredit = 0;
          balcurcredit = 0;
        end
        if(:balcreditsett > :baldebitsett) then
          begin
             balcreditsett = :balcreditsett - :baldebitsett;
             baldebitsett = 0;
          end
        else if(:balcreditsett < :baldebitsett) then
          begin
             baldebitsett = :baldebitsett - :balcreditsett;
             balcreditsett = 0;
          end
        else
          begin
             baldebitsett = 0;
             balcreditsett = 0;
          end
        if(:balcreditacc > :baldebitacc) then
          begin
             balcreditacc = :balcreditacc - :baldebitacc;
             baldebitacc = 0;
          end
        else if(:balcreditacc < :baldebitacc) then
          begin
             baldebitacc = :baldebitacc - :balcreditacc;
             balcreditacc = 0;
          end
        else
          begin
             baldebitacc = 0;
             balcreditacc = 0;
          end
        ref = ref + 1;
        nr = nr + 1;
        issettl = 1;
        suspend;
        balcredit = null;
        baldebit = null;
        balcurdebit = null;
        balcurcredit = null;
        old_settlement =:settlement;
      end else
      begin
        bdebit = bdebit + debit;
        bcredit = bcredit + credit;
        bcurdebit = bcurdebit + curdebit;
        bcurcredit = bcurcredit + curcredit;
      end
    end
    if (analit = 0 or analit = 2) then
    --walutowe i zwykle syntetycznie
    begin
      account = s_account;
      ref = ref + 1;
      nr = nr + 1;
      issettl = 1;
      debit = bdebit;
      credit = bcredit;
      curdebit = bcurdebit;
      curcredit = bcurcredit;
      if (debit > credit) then
      begin
        bdebit = debit - credit;
        bcredit = 0;
      end else
      begin
        bcredit = credit - debit;
        bdebit = 0;
     end
     if (curdebit > curcredit) then
      begin
        bcurdebit = curdebit - curcredit;
        bcurcredit = 0;
      end else
      begin
        bcurcredit = curcredit - curdebit;
        bcurdebit = 0;
     end
      suspend;
    end
    old_symbol = symbol;
    old_account = account;
    old_settlement = :settlement;
    old_account_s = :s_account;
  end
end^
SET TERM ; ^
