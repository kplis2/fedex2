--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_PROCESS_GOV_RESP(
      EDEDOCSH_REF integer)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
   as
declare variable EDECLREF integer;
declare variable NAME varchar(255);
declare variable VAL varchar(255);
declare variable REFID type of column EDECLARATIONS.REFID;
declare variable STATUS type of column EDECLARATIONS.STATUS;
declare variable STATUSDICT type of column EDECLARATIONS.STATUSDICT;
begin
  
  select oref from ededocsh where ref = :ededocsh_ref
    into :edeclref;
  
  for
    select name, val from ededocsp
      where ededoch = :ededocsh_ref
      into :name, :val
  do begin
    if (name = 'refId') then refid = val;
    else if (name = 'status') then status = val;
    else if (name = 'statusOpis') then statusdict = val;
  end
  
  if (name <> '') then
  begin
    rdb$set_context('USER_TRANSACTION', 'EDE_PROCESS_RESPOND_Run', 1);

    update edeclarations
      set refid = :refid,
          status = :status,
          statusdict = :statusdict
      where ref = :edeclref;

    rdb$set_context('USER_TRANSACTION', 'EDE_PROCESS_RESPOND_Run', 0);
  end

  otable = 'EDECLARATIONS';
  oref = :edeclref;

  suspend;
end^
SET TERM ; ^
