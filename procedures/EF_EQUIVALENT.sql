--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_EQUIVALENT(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      ETYPE smallint)
  returns (
      RET numeric(14,2))
   as
declare variable wage numeric(14,2);
declare variable varwage numeric(14,2);
declare variable period integer;
declare variable firstperiod integer;
declare variable equvratio numeric(14,2);
declare variable daybase numeric(14,2);
declare variable hourbase numeric(14,2);
declare variable workdim numeric(14,2);
declare variable prtype varchar(10);
declare variable company smallint;
declare variable maxdate date;
declare variable lastday date;
declare variable salary numeric(14,2);
declare variable caddsalary numeric(14,2);
declare variable funcsalary numeric(14,2);
declare variable paymenttype smallint;
declare variable months smallint;
begin
--MW: personel - do liczenia ekwiwalentu za zalegly urlop (stawka dzienna)

  ret = 0;

  select prtype, company
    from epayrolls
    where ref = :payroll
    into :prtype, :company;

--data ostatniej umowy
  select max(fromdate)
    from employment
    where employee = :employee
    into :maxdate;

--data zwolnienia
  select first 1 todate
    from employment
    where employee = :employee and fromdate = :maxdate
    into :lastday;

-- sprawdzenie, czy pracownik zostal juz zwolniony, jezeli nie to konczymy
  if(:lastday IS NULL and :etype <> 1) then
    exit;

--okres, w ktrym zwolniony uzyskal prawo do ekwiwalentu
  if (cast (extract(month from lastday) as integer) < 10) then
    period = cast (extract(year from lastday) || '0' || extract(month from lastday) as integer);
  else
    period = cast (extract(year from lastday) || extract(month from lastday) as integer);

  if (cast(substring(period from 5 for 2) as integer) > 3) then
    firstperiod = period - 3;
  else
    firstperiod = period - 91;

-- wsplczynnik ekwiwalentu za urlop
  select first 1 workdim
    from emplcontracts C join employees E on (C.employee = E.ref)
    left join econtrtypes EC on (C.econtrtype = EC.contrtype)
    where E.ref = :employee and EC.empltype = 1
    order by C.fromdate desc
    into :workdim;

  workdim = coalesce(workdim, 1);

  execute procedure ef_pval (employee, payroll, prefix, 'WSK_EKWIWALENTU')
    returning_values :EquvRatio;

-- stale skladniki wynagrodzenia miesiecznego
  select first 1 salary, caddsalary, funcsalary, paymenttype
    from employment
    where employee = :employee and fromdate = :maxdate
    into :salary, :caddsalary, :funcsalary, :paymenttype;

  salary = coalesce(salary,0);
  caddsalary = coalesce(caddsalary,0);
  funcsalary = coalesce(funcsalary,0);
  paymenttype = coalesce(paymenttype,0);

  if (:paymenttype = 0) then
    wage = salary + caddsalary + funcsalary;
  if (:paymenttype = 1) then
    wage = EquvRatio * salary * workdim * 8 + caddsalary + funcsalary;

-- zmienne skadniki wynagrodzenia z 3 miesiecy
  select sum(case when PP.ecolumn = 510 then 0 else PP.pvalue end), count(distinct P.cper)
    from epayrolls P
    join eprpos PP on (PP.payroll = P.ref)
    join ecolumns C on (C.number = PP.ecolumn)
    join employees E on (E.ref = PP.employee)
  where PP.employee = :employee and P.cper >= :firstperiod and P.cper < :period
    and P.prtype = :prtype and P.company = :company
    and (C.cflags containing ';URL;' or PP.ecolumn = 510)
  into :varwage, :months;

  months = coalesce(months, 0);
  varwage = coalesce(varwage, 0);

  if (months <> 0) then
    varwage = varwage / months;

-- ekwiwalent za dzie i godzine urlopu
  if (etype = 0) then  -- ekwiwalent za urlop
    ret = (wage + varwage) / EquvRatio;
  else if (etype = 1) then --odprawa wojskowa
    ret = (wage + varwage) / 2;
  else if (etype = 2) then --odprawa rentowa
    ret = wage + varwage;
  else if (etype = 3) then --odprawa emerytalna
    ret = wage + varwage;

  suspend;
end^
SET TERM ; ^
