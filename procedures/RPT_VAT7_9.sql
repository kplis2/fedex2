--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_VAT7_9(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PAR_K44 numeric(14,2),
      PAR_K59 numeric(14,2),
      PAR_K60 numeric(14,2),
      PAR_K61 numeric(14,2),
      COMPANY integer)
  returns (
      WSP45 numeric(14,2),
      K20 numeric(14,2),
      K21 numeric(14,2),
      K22 numeric(14,2),
      K23 numeric(14,2),
      K24 numeric(14,2),
      K25 numeric(14,2),
      K26 numeric(14,2),
      K27 numeric(14,2),
      K28 numeric(14,2),
      K29 numeric(14,2),
      K30 numeric(14,2),
      K31 numeric(14,2),
      K32 numeric(14,2),
      K33 numeric(14,2),
      K34 numeric(14,2),
      K35 numeric(14,2),
      K36 numeric(14,2),
      K37 numeric(14,2),
      K38 numeric(14,2),
      K39 numeric(14,2),
      K40 numeric(14,2),
      K41 numeric(14,2),
      K42 numeric(14,2),
      K43 numeric(14,2),
      K44 numeric(14,2),
      K45 numeric(14,2),
      K46 numeric(14,2),
      K47 numeric(14,2),
      K48 numeric(14,2),
      K49 numeric(14,2),
      K50 numeric(14,2),
      K51 numeric(14,2),
      K52 numeric(14,2),
      K53 numeric(14,2),
      K54 numeric(14,2),
      K55 numeric(14,2),
      K56 numeric(14,2),
      K57 numeric(14,2),
      K58 numeric(14,2),
      K59 numeric(14,2),
      K60 numeric(14,2),
      K61 numeric(14,2),
      K62 numeric(14,2))
   as
declare variable vatgr varchar(6);
declare variable netv numeric(14,2);
declare variable vatv numeric(14,2);
declare variable taxgr varchar(10);
declare variable bkreg varchar(10);
declare variable wrongdoc varchar(20);
declare variable bkdocnumber integer;
declare variable vat_nalezny varchar(255);
declare variable vat_naliczony varchar(255);
declare variable sdate date;
declare variable fdate date;
declare variable np numeric(14,2);
begin
  -- paramert konfiguracji
  execute procedure get_config('VAT7_NALEZNY', 0) -- naliczanie wg rejestrów
    returning_values :vat_nalezny;
  execute procedure get_config('VAT7_NALICZONY', 0) -- naliczanie stawek <> 0
    returning_values :vat_naliczony;

  if (PAR_K44 is null) then
    PAR_K44 = 0;
  if (PAR_K59 is null) then
    PAR_K59 = 0;
  if (PAR_K60 is null) then
    PAR_K60 = 0;
  if (PAR_K61 is null) then
    PAR_K61 = 0;
  WSP45 = 100; --domyslnie caly mozna odliczyc
  k20 = 0;
  k21 = 0;
  k22 = 0;
  k23 = 0;
  k24 = 0;
  k25 = 0;
  k26 = 0;
  k27 = 0;
  K28 = 0;
  K29 = 0;
  K30 = 0;
  K31 = 0;
  K32 = 0;
  K33 = 0;
  K34 = 0;
  K35 = 0;
  K36 = 0;
  K37 = 0;
  K38 = 0;
  K39 = 0;
  K40 = 0;
  K41 = 0;
  K42 = 0;
  K43 = 0;
  K44 = PAR_K44;
  K45 = 0;
  K46 = 0;
  K47 = 0;
  K48 = 0;
  K49 = 0;
  K50 = 0;
  K51 = 0;
  K52 = 0;
  K53 = 0;
  K54 = 0;
  K55 = 0;
  K56 = 0;
  K57 = 0;
  K58 = 0;
  K59 = PAR_K59;
  K60 = PAR_K60;
  K61 = PAR_K61;
  K62 = 0;
  -- sprzedaz krajowa
  -- vtype = 1 korygowana jest deklaracja w ktorym zostala wystawiony dokument WDT (okres zrodlowy)
  -- vtype = 2 korekta jest w biezacej deklaracji zgodnie z data otrzymania potwierdzenia EXPORT (okres biezacy otrzymania korekty)
  select cast(B.sdate as date), cast(B.fdate as date)
    from bkperiods B
      where B.id = :period and B.company = :company
  into :sdate, :fdate;
  for select CASE
        WHEN (vatregs.vtype = 0) THEN (bkvatpos.vatgr)
--        WHEN (vatregs.vtype = 1 and bkdocs.vatexpconfirm is null) THEN bkvatpos.vatgrf
--        WHEN (vatregs.vtype = 2 and (bkdocs.vatexpconfirm is null or bkdocs.vatexpconfirm>:fdate)) THEN bkvatpos.vatgrf
        else 0
      END,
      sum(CASE
        WHEN (vatregs.vtype = 0) THEN (bkvatpos.netv)
--        WHEN (vatregs.vtype = 1 and bkdocs.vatexpconfirm is null) THEN bkvatpos.netv
--        WHEN (vatregs.vtype = 2 and (bkdocs.vatexpconfirm is null or bkdocs.vatexpconfirm>:fdate)) THEN bkvatpos.netv
        else 0
      END),
      sum(CASE
        WHEN (vatregs.vtype = 0) THEN (bkvatpos.vatv)
--        WHEN (vatregs.vtype = 1 and bkdocs.vatexpconfirm is null) THEN bkvatpos.vatvf
--        WHEN (vatregs.vtype = 2 and (bkdocs.vatexpconfirm is null or bkdocs.vatexpconfirm>:fdate)) THEN bkvatpos.vatvf
        else 0
      END)
      from bkdocs
      join vatregs on (bkdocs.vatperiod = :period and vatregs.symbol = bkdocs.vatreg
        and vatregs.vtype = 0 and bkdocs.status >= 0 and bkdocs.company= vatregs.company)
      join bkvatpos on (bkvatpos.bkdoc = bkdocs.ref)
    where bkdocs.company = :company
    group by
      CASE
        WHEN (vatregs.vtype = 0) THEN (bkvatpos.vatgr)
--        WHEN (vatregs.vtype = 1 and bkdocs.vatexpconfirm is null) THEN bkvatpos.vatgrf
--        WHEN (vatregs.vtype = 2 and (bkdocs.vatexpconfirm is null or bkdocs.vatexpconfirm>:fdate)) THEN bkvatpos.vatgrf
        else 0
      END, bkvatpos.taxgr
    into :vatgr, :netv, :vatv
  do begin
    if (vatgr = '22') then
    begin
      k28 = k28 + netv;
      k29 = k29 + vatv;
    end
    else if (vatgr = '07') then
    begin
      k26 = k26 + netv;
      k27 = k27 + vatv;
    end
    else if (vatgr = '03') then
    begin
      k24 = k24 + netv;
      k25 = k25 + vatv;
    end else if (vatgr = 'ZW') then
    begin
      k20 = k20 + netv;
    end
    else if (vatgr = '00') then
    begin
      k22 = k22 + netv;
      if (taxgr = 'P23') then
      begin
        k23 = k23 + netv;
      end
    end
    else
      exception test_break 'Nieznana stawka podatkowa ' || vatgr;
  end


  -- jeszcze eksport
-- vtype = 2 korekta jest w biezacej deklaracji zgodnie z data otrzymania potwierdzenia EXPORT
  k31 = 0;
  select sum(bkvatpos.netv)
    from bkdocs
    join vatregs on (vatregs.symbol = bkdocs.vatreg
      and vatregs.vtype = 2 and bkdocs.status > 0 and bkdocs.company= vatregs.company and bkdocs.vatperiod = :period)
    join bkvatpos on (bkvatpos.bkdoc = bkdocs.ref)
    where (bkvatpos.taxgr <> 'C21' or bkvatpos.taxgr is null)
      and bkdocs.company = :company and bkdocs.vatperiod = :period
--      and bkdocs.vatexpconfirm is not null and bkdocs.vatexpconfirm >= :sdate and bkdocs.vatexpconfirm <= :fdate
    into :k31;

  -- tax free pole k21
  -- to jest pytanie czy do tax free tez brac potwierdzenie
  -- TO FIXX ustalic jak ma być
  select sum(bkvatpos.netv)
    from bkdocs
    join vatregs on (bkdocs.vatperiod = :period and vatregs.symbol = bkdocs.vatreg
      and vatregs.vtype = 2 and bkdocs.status > 0 and bkdocs.company= vatregs.company)
    join bkvatpos on (bkvatpos.bkdoc = bkdocs.ref)
    where bkvatpos.taxgr = 'C21' and bkdocs.company = :company
    into :k21;

  -- NP
  select sum(bkvatpos.netv)
    from bkdocs
    join vatregs on (bkdocs.vatperiod = :period and vatregs.symbol = bkdocs.vatreg
      and vatregs.vtype = 1 and bkdocs.status > 0 and bkdocs.company= vatregs.company)
    join bkvatpos on (bkvatpos.bkdoc = bkdocs.ref)
    where bkvatpos.vatgr = 'NP' and bkdocs.company = :company
    into :np;

  k21 = coalesce(k21, 0) + coalesce(np, 0);

  -- dostawa wewnatrzwspolnotowa
  -- vtype = 1 korygowana jest deklaracja w ktorym zostala wystawiony dokument WDT
  select sum(bkvatpos.netv)
    from bkdocs
    join vatregs on (vatregs.symbol = bkdocs.vatreg
      and vatregs.vtype = 1 and bkdocs.status > 0 and bkdocs.company= vatregs.company and bkdocs.vatperiod = :period)
    join bkvatpos on (bkvatpos.bkdoc = bkdocs.ref)
    where bkdocs.company = :company
--      and bkdocs.vatexpconfirm is not null
  into :k30;

  -- nabycie wewnatrzwspolnotowe
    select sum(bkvatpos.netv),
      sum(bkvatpos.vatv)
    from bkdocs
    join vatregs on (bkdocs.vatperiod = :period and vatregs.symbol = bkdocs.vatreg
      and vatregs.vtype = 5 and bkdocs.status > 0 and bkdocs.company= vatregs.company)
    join bkvatpos on (bkvatpos.bkdoc = bkdocs.ref)
    where (bkvatpos.taxgr <> 'C36' or bkvatpos.taxgr is null)
      and bkdocs.company = :company
    into :k32, :k33;

  -- nabycie wewnatrzwspolnotowe - pole K39
    select sum(bkvatpos.netv),
      sum(bkvatpos.vatv)
    from bkdocs
    join vatregs on (bkdocs.vatperiod = :period and vatregs.symbol = bkdocs.vatreg
      and vatregs.vtype = 5 and bkdocs.status > 0 and bkdocs.company= vatregs.company)
    join bkvatpos on (bkvatpos.bkdoc = bkdocs.ref)
    where (bkvatpos.taxgr = 'C36' or bkvatpos.taxgr is null)
      and bkdocs.company = :company
    into :k38, :k39;

  -- nabycie wewnatrzwspolnotowe - pole K41
    select sum(bkvatpos.vatv)
    from bkdocs
    join vatregs on (bkdocs.vatperiod = :period and vatregs.symbol = bkdocs.vatreg
      and vatregs.vtype = 5 and bkdocs.status > 0)
    join bkvatpos on (bkvatpos.bkdoc = bkdocs.ref)
    where (bkvatpos.taxgr = 'P39' or bkvatpos.taxgr is null)
      and bkdocs.company = :company
    into :k41;

  -- import uslug
    select sum(bkvatpos.netv),
      sum(bkvatpos.vatv)
    from bkdocs
    join vatregs on (bkdocs.vatperiod = :period and vatregs.symbol = bkdocs.vatreg
      and vatregs.vtype = 6 and bkdocs.status > 0 and bkdocs.company= vatregs.company)
    join bkvatpos on (bkvatpos.bkdoc = bkdocs.ref)
    where bkdocs.company = :company
    into :k36, :k37;


  if (period <= '200512') then
  begin
    k20 = cast(k20 * 100 as integer) / 100;
    k21 = cast(k21 * 100 as integer) / 100;
    k22 = cast(k22 * 100 as integer) / 100;
    k23 = cast(k23 * 100 as integer) / 100;
    k24 = cast(k24 * 100 as integer) / 100;
    k25 = cast(k25 * 100 as integer) / 100;
    k26 = cast(k26 * 100 as integer) / 100;
    k27 = cast(k27 * 100 as integer) / 100;
    k28 = cast(k28 * 100 as integer) / 100;
    k29 = cast(k29 * 100 as integer) / 100;
    k30 = cast(k30 * 100 as integer) / 100;
    k31 = cast(k31 * 100 as integer) / 100;
    k32 = cast(k32 * 100 as integer) / 100;
    k33 = cast(k33 * 100 as integer) / 100;
    k34 = cast(k34 * 100 as integer) / 100;
    k35 = cast(k35 * 100 as integer) / 100;
    k36 = cast(k36 * 100 as integer) / 100;
    k37 = cast(k37 * 100 as integer) / 100;
    k38 = cast(k38 * 100 as integer) / 100;
    k39 = cast(k39 * 100 as integer) / 100;
    k41 = cast(k41 * 100 as integer) / 100;
  end else
  begin
    k20 = round(k20);
    k21 = round(k21);
    k22 = round(k22);
    k23 = round(k23);
    k24 = round(k24);
    if (vat_nalezny = '1') then
    begin
      k25 = round(0.03 * k24);
      k27 = round(0.07 * k26);
      k29 = round(0.22 * k28);
    end else
    begin
      k25 = round(k25);
      k27 = round(k27);
      k29 = round(k29);
    end
    k26 = round(k26);
    k28 = round(k28);
    k30 = round(k30);
    k31 = round(k31);
    k32 = round(k32);
    k33 = round(k33);
    k34 = round(k34);
    k35 = round(k35);
    k36 = round(k36);
    k37 = round(k37);
    k38 = round(k38);
    k39 = round(k39);
    k41 = round(k41);
  end

  -- zeby liczyl odpowiednio
  if (k20 is null) then k20 = 0;
  if (k21 is null) then k21 = 0;
  if (k22 is null) then k22 = 0;
  if (k23 is null) then k23 = 0;
  if (k24 is null) then k24 = 0;
  if (k25 is null) then k25 = 0;
  if (k26 is null) then k26 = 0;
  if (k27 is null) then k27 = 0;
  if (k28 is null) then k28 = 0;
  if (k29 is null) then k29 = 0;
  if (k30 is null) then k30 = 0;
  if (k31 is null) then k31 = 0;
  if (k32 is null) then k32 = 0;
  if (k33 is null) then k33 = 0;
  if (k34 is null) then k34 = 0;
  if (k35 is null) then k35 = 0;
  if (k36 is null) then k36 = 0;
  if (k37 is null) then k37 = 0;
  if (k38 is null) then k38 = 0;
  if (k39 is null) then k39 = 0;
  if (k41 is null) then k41 = 0;

  K42 = K20 + K21 + K22 + K24 + K26 + K28 + K30 + K31 + K32
    + K34 + K36 + K38;
  K43 = K25 + K27 + K29 + K33 + K35 + K37 + K39 + K40 - K41;
  -- zakupy z tego miesiaca
  for
    select  bkvatpos.taxgr, sum(bkvatpos.netv), sum(bkvatpos.vatv)
      from bkdocs
        join vatregs on (bkdocs.vatperiod = :period and vatregs.symbol = bkdocs.vatreg
          and vatregs.vtype > 2 and bkdocs.status > 0 and bkdocs.company= vatregs.company)
        join bkvatpos on (bkvatpos.bkdoc = bkdocs.ref and bkvatpos.debited = 1)
      where bkdocs.company = :company
      group by bkvatpos.taxgr
      into :taxgr, :netv, :vatv
  do begin
    if (taxgr = 'I11' or taxgr = 'I21' or taxgr = 'INW') then
    begin
      if (vat_naliczony = '1') then
        if (vatv = 0) then netv = 0;
      K46 = K46 + netv;
      K47 = K47 + vatv;
    end else if (taxgr = 'P11' or taxgr = 'P21' or taxgr = 'POZ' or taxgr = 'C36' or taxgr = 'P39') then
    begin
      if (vat_naliczony <> '' and vat_naliczony = '1') then
        if (vatv = 0) then netv = 0;
      K48 = K48 + netv;
      K49 = K49 + vatv;
    end
    else begin
      select first 1 bkdocs.symbol, bkdocs.bkreg, bkdocs.number
        from bkdocs
          join vatregs on (bkdocs.vatperiod = :period and vatregs.symbol = bkdocs.vatreg
            and vatregs.vtype > 2 and bkdocs.status > 0 and bkdocs.company= vatregs.company)
          join bkvatpos on (bkvatpos.bkdoc = bkdocs.ref and bkvatpos.debited = 1)
        where bkdocs.company = :company and (bkvatpos.taxgr = :taxgr or bkvatpos.taxgr is null)
        into :wrongdoc, :bkreg, :bkdocnumber;

--      exception universal 'Wystąpiła grupa podatkowa nieuwzględniona w deklaracji!';
      exception universal 'Błędna gr. podatk. na dokumen. ' || :wrongdoc || ' ('|| :bkreg ||','|| :bkdocnumber ||')';
      -- exception fk_vat7_unknown_taxgr;
    end
  end

 -- zsumowanie calego do odliczenia
  if (k47 is null) then
    k47 = 0;
  if (k48 is null) then
    k48 = 0;
  if (period <= '200512') then
  begin       
    k41 = cast(k41 * 100 as integer) / 100;
    k46 = cast(k46 * 100 as integer) / 100;
    k47 = cast(k47 * 100 as integer) / 100;
    k48 = cast(k48 * 100 as integer) / 100;
    k49 = cast(k49 * 100 as integer) / 100;
  end else
  begin
    k41 = round(k41);
    k46 = round(k46);
    k47 = round(k47);
    k48 = round(k48);
    k49 = round(k49);
  end

  K50 = (k47) * ((WSP45 - 100) / 100); --wyliczenie korekt
  K51 = (k49) * ((WSP45 - 100) / 100);

  if (period <= '200512') then
  begin
    k50 = cast(k50 * 100 as integer) / 100;
    k51 = cast(k51 * 100 as integer) / 100;
  end else
  begin
    k50 = round(k50);
    k51 = round(k51);
  end

  K52 = K44 + k47 + k49 + K50 + K51; -- caly odliczony

  if (k43 > k52) then
    k55 = k43 - k52;
  else
    k55 = 0;

  if (k52 > k43) then
    k57 = k52 - k43;
  else
    k57 = 0;

  k58 = k59 + k60 + K61;
  k62 = k56 + k57 - k58;

  if (k20 = 0) then k20 = null;
  if (k21 = 0) then k21 = null;
  if (k22 = 0) then k22 = null;
  if (k23 = 0) then k23 = null;
  if (k24 = 0) then k24 = null;
  if (k25 = 0) then k25 = null;
  if (k26 = 0) then k26 = null;
  if (k27 = 0) then k27 = null;
  if (k28 = 0) then k28 = null;
  if (k29 = 0) then k29 = null;
  if (k31 = 0) then k31 = null;
  if (k34 = 0) then k34 = null;
  if (k35 = 0) then k35 = null;
  if (k36 = 0) then k36 = null;
  if (k37 = 0) then k37 = null;
  if (k38 = 0) then k38 = null;
  if (k39 = 0) then k39 = null;
  if (k41 = 0) then k41 = null;
  if (k44 = 0) then k44 = null;
  if (k47 = 0) then k47 = null;
  if (k48 = 0) then k48 = null;
  if (k50 = 0) then k50 = null;
  if (k51 = 0) then k51 = null;
  if (k56 = 0) then k56 = null;
  if (k58 = 0) then k58 = null;
  if (k59 = 0) then k59 = null;
  if (k60 = 0) then k60 = null;
  if (k61 = 0) then k61 = null;
  if (k62 = 0) then k62 = null;
  suspend;
end^
SET TERM ; ^
