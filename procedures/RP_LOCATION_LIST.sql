--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RP_LOCATION_LIST
  returns (
      LOCNUM integer)
   as
declare variable loc_numbers integer;
declare variable s_loc_numbers varchar(255);
begin
  /* procedura dokonująca w trybie select wylistowania numeró aktywnych lokacji */
  locnum = 1;
  execute procedure GET_CONFIG('LOCATIONNUMMAX',2)
    returning_values :s_loc_numbers;
  loc_numbers = :s_loc_numbers;
  while(:locnum <= :loc_numbers) do
  begin
   suspend;
   locnum = :locnum + 1;
  end
end^
SET TERM ; ^
