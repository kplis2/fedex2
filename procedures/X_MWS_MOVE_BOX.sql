--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_MOVE_BOX(
      REF INTEGER_ID,
      LOK STRING40)
   as
begin
-- przesuwanie kartonów
 if (exists(select * from listywysd l join listywysdroz_opk ll on (l.ref = ll.listwysd) where l.akcept = 1 and ll.ref = :ref)) then
    exception universal 'Nie możesz modyfikować pozycji na zaakceptowanym dokumencie.';

    update listywysdroz_opk l
      set l.x_mwsconstlock = :lok
      where l.ref = :ref;
end^
SET TERM ; ^
