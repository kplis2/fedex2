--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_DOKUMENTY(
      NAGFAKREF integer,
      COMPANY integer)
  returns (
      DOKTABLE varchar(20) CHARACTER SET UTF8                           ,
      DOKREF integer,
      DOKSYMBOL varchar(40) CHARACTER SET UTF8                           )
   as
begin
  dokref = 0;
  doksymbol = '';
  for select dokumnag.ref,dokumnag.symbol
  from dokumnag
  left join defdokum on (defdokum.symbol=dokumnag.typ)
  where dokumnag.faktura=:nagfakref
    and dokumnag.company = :company
  into :dokref,:doksymbol
  do begin
    doktable = 'DOKUMNAG';
    suspend;
  end

  dokref = 0;
  doksymbol = '';
  for select dokumnot.ref,dokumnot.symbol
  from dokumnot
  left join defdokum on (defdokum.symbol=dokumnot.typ)
  where dokumnot.faktura=:nagfakref
  and dokumnot.ref=dokumnot.grupa
  and dokumnot.company = :company
  into :dokref,:doksymbol
  do begin
    doktable = 'DOKUMNOT';
    suspend;
  end

  dokref = 0;
  doksymbol = '';
  for select dokumnot.ref,dokumnot.symbol
  from dokumnot
  join dokumnag on (dokumnag.ref=dokumnot.dokumnagkor)
  left join defdokum on (defdokum.symbol=dokumnot.typ)
  where dokumnot.faktura is null and dokumnag.faktura=:nagfakref
  and dokumnot.ref=dokumnot.grupa
  and dokumnot.company = :company
  into :dokref,:doksymbol
  do begin
    doktable = 'DOKUMNOT';
    suspend;
  end
end^
SET TERM ; ^
