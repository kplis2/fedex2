--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_ROUND(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      VAL numeric(12,2),
      PREC smallint)
  returns (
      RET numeric(14,2))
   as
declare variable i smallint;
  declare variable j numeric(14,4);
  declare variable sign integer;
begin
  --DU: personel - funkcja liczy zaokrąglenie
  j = 1;

  if (prec > 0) then
  begin
    i = prec;
    while (i > 0) do
    begin
      j = j * 10;
      i = i - 1;
    end
  end

  if (prec < 0) then
  begin
    i = prec;
    while (i < 0) do
    begin
      j = j / 10;
      i = i + 1;
    end
  end

  val = val * j;
  if (val <> 0) then
  begin
    sign = val / abs(val);
    val = abs(val);
    val = round(val);
    val = val * sign;
  end

  ret = val / j;

  suspend;
end^
SET TERM ; ^
