--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_TRANS_MWSCONSLOCS
  returns (
      MSG STRING100,
      MAG_1 INTEGER_ID,
      MAG_2 INTEGER_ID,
      MAG_3 INTEGER_ID,
      MAG_4 INTEGER_ID,
      MAG_5 INTEGER_ID,
      MAG_6 INTEGER_ID)
   as
declare variable constloc_ref integer_id;
declare variable constloc_symbol string20;
declare variable regal_h smallint_id;
declare variable pozycja_h smallint_id;
declare variable lvl_h smallint_id;
declare variable constloc_ref_hermon string10;
declare variable magazyn_h string10;
declare variable expimp_hex string10;
declare variable expimp_dec smallint_id;
declare variable ref_expimp integer_id;
begin
mag_1 = 0;
mag_2 = 0;
mag_3 = 0;
mag_4 = 0;
mag_5 = 0;
mag_6 = 0;
msg = '';
 /* for
    select e.s2, e.s3, e.s4
      from expimp e
      where e.s1 = 'mg4'
    into :regal_h, :pozycja_h, :lvl_h
   do begin
   constloc_symbol = 'M4-'||lpad(regal_h, 2, '0')||'-'||lpad(pozycja_h, 2, '0')||'-'||lpad(lvl_h, 2, '0');
   if (not exists(select first 1 1 from mwsconstlocs m where m.symbol = :constloc_symbol)) then begin
     suspend;
     end
   end  */

-- tu hexuje, bo exel mnie oklamal
  for
    select e.s6, e.ref
      from expimp e
    into :expimp_dec, :ref_expimp
  do begin
    execute procedure inttohex(expimp_dec)
      returning_values :expimp_hex;
    update expimp e
      set e.s5 = lpad(:expimp_hex, 8, '0')
      where e.ref = :ref_expimp;
  end


-- wyczyszczenie starego przyporzadkowania
  update mwsconstlocs m
    set m.x_ref_hermon = null
    where 1=1;

-- przypisanie x_ref_hermon z zaimportowanego pliku
  for
    select m.ref, m.symbol
      from mwsconstlocs m
        where m.symbol like 'M%'
          and m.x_ref_hermon is null
    into :constloc_ref, :constloc_symbol
  do begin
    magazyn_h = substring(:constloc_symbol from 2 for 1);
    if(char_length(:constloc_symbol) = 11) then
    begin
        regal_h = cast(substring(:constloc_symbol from 4 for 2) as smallint_id);
        pozycja_h = cast(substring(:constloc_symbol from 7 for 2) as smallint_id);
        lvl_h = cast(substring(:constloc_symbol from 10 for 2) as smallint_id);
    end
    if(char_length(:constloc_symbol) = 12) then
    begin
        regal_h = cast(substring(:constloc_symbol from 5 for 2) as smallint_id);
        pozycja_h = cast(substring(:constloc_symbol from 8 for 2) as smallint_id);
        lvl_h = cast(substring(:constloc_symbol from 11 for 2) as smallint_id);
    end
    select e.s5
      from expimp e
      where e.s1 = 'mg'||:magazyn_h                  --00-__-__-__
        and cast(e.s2 as smallint_id) = :regal_h     --__-00-__-__
        and cast(e.s3 as smallint_id) = :pozycja_h   --__-__-00-__
        and cast(e.s4 as smallint_id) = :lvl_h       --__-__-__-00  -- numeruja parter od 0

    into :constloc_ref_hermon;

    if (:constloc_ref_hermon is not null) then begin
      update mwsconstlocs m
        set m.x_ref_hermon = :constloc_ref_hermon
        where m.ref = :constloc_ref;

      if (cast(:magazyn_h as smallint_id) = 1) then mag_1 = mag_1 + 1;
      else if (cast(:magazyn_h as smallint_id) = 2) then mag_2 = mag_2 + 1;
      else if (cast(:magazyn_h as smallint_id) = 3) then mag_3 = mag_3 + 1;
      else if (cast(:magazyn_h as smallint_id) = 4) then mag_4 = mag_4 + 1;
      else if (cast(:magazyn_h as smallint_id) = 5) then mag_5 = mag_5 + 1;
      else if (cast(:magazyn_h as smallint_id) = 6) then mag_6 = mag_6 + 1;
      else exception universal 'brak magazynu';
    end
    constloc_ref_hermon = null;
  end

-- blokowanie lokacji nie wystepujacych w zaimportowanym pliku
  update mwsconstlocs m
    set m.act = 0
    where m.ref > 3
      and m.x_ref_hermon is null;

-- raport
  msg = 'dodano do constloc';
  suspend;
  mag_1 = 0;
  mag_2 = 0;
  mag_3 = 0;
  mag_4 = 0;
  mag_5 = 0;
  mag_6 = 0;
  msg = 'w pliku xls';
  select count (*)
    from expimp e
    where e.s1 in ('mg1')
  into :mag_1;
  select count (*)
    from expimp e
    where e.s1 in ('mg2')
  into :mag_2;
  select count (*)
    from expimp e
    where e.s1 in ('mg3')
  into :mag_3;
  select count (*)
    from expimp e
    where e.s1 in ('mg4')
  into :mag_4;
  select count (*)
    from expimp e
    where e.s1 in ('mg5')
  into :mag_5;
  select count (*)
    from expimp e
    where e.s1 in ('mg6')
  into :mag_6;
  suspend;
end^
SET TERM ; ^
