--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_PRORD_CANCEL(
      PRORD integer,
      PRPOZZAM integer)
   as
declare variable wydania smallint;
declare variable pref integer;
declare variable genpozref integer;
begin
  if (prpozzam = 0) then prpozzam = null;
  select t.wydania
    from nagzam n
      left join typzam t on (t.symbol = n.typzam)
    where n.ref = :prord
    into wydania;
  if (wydania <> 3) then
    exception prnagzam_error 'Operacja dozwolona tylko dla zleceń produkcyjnych.';
  for
    select p.ref, p.genpozref
      from pozzam p
      where p.zamowienie = :prord and p.out = 0
        and (p.ref = :prpozzam or :prpozzam is null)
      into pref, genpozref
  do begin
    update pozzam set anulowano = 1 where ref = :pref;
  end
end^
SET TERM ; ^
