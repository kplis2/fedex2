--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CENNIK_AKTUALIZUJ(
      REF integer,
      WERSJAREF integer,
      DODAJ integer,
      FORCECONSTS integer)
  returns (
      STATUS integer,
      ILEPRZEL integer,
      ILEDOD integer)
   as
declare variable ktm varchar(40);
declare variable ok integer;
declare variable odktm varchar(40);
declare variable doktm varchar(40);
declare variable ktmmaska varchar(40);
declare variable grupa varchar(60);
declare variable tryb integer;
declare variable zrodlo integer;
declare variable wartosc numeric(14,2);
declare variable iletuprzel integer;
declare variable iletudod integer;
declare variable stalacena integer;
declare variable staracenanet numeric(14,4);
declare variable starynarzut numeric(14,2);
declare variable staramarza numeric(14,2);
declare variable kurs numeric(14,4);
declare variable nowacenanet numeric(14,4);
declare variable nowacenabru numeric(14,4);
declare variable nowamarza numeric(14,2);
declare variable nowynarzut numeric(14,2);
declare variable jedn integer;
declare variable werref integer;
declare variable przelicz numeric(14,4);
declare variable tolerancja numeric(14,2);
declare variable grupatow varchar(120);
declare variable magazyn varchar(3);
declare variable cenniknad integer;
begin
  ileprzel = 0;
  iledod = 0;
  ktm = '';
  if(:WERSJAREF is not null and :WERSJAREF>0) then
    select KTM, GRUPA
      from WERSJE
      where REF=:WERSJAREF
    into :ktm, :grupatow;
  /* przetwarzanie regul cennikowych */
  for
    select ODKTM, DOKTM, KTMMASKA, GRUPA, TRYB, ZRODLO, WARTOSC, TOLERANCJA, MAGAZYN, CENNIKNAD
      from DEFCENREG
      where (DEFCENNIK=:ref and AUTONALICZ=1)
      order by NUMER
    into :odktm, :doktm, :ktmmaska, :grupa, :tryb, :zrodlo, :wartosc, :tolerancja, :magazyn, :cenniknad
  do begin
    if (:tolerancja is null) then tolerancja = 0;
    ok = 1;
    if(:ktm<>'' and :ok=1) then begin
      if(:ok=1 and :odktm is not null and :odktm<>'' and :ktm<:odktm) then ok = 0;
      if(:ok=1 and :doktm is not null and :doktm<>'' and :ktm>:doktm) then ok = 0;
      if(:ok=1 and :ktmmaska is not null and :ktmmaska<>'' and not(:ktm like :ktmmaska)) then ok = 0;
      if(:ok=1 and :grupa is not null and :grupa<>'' and :grupa<>:grupatow) then ok = 0;
    end
    if(ok=1) then begin
      execute procedure CENNIK_AKTUREG(:ref, :ktmmaska, :odktm, :doktm, :grupa, :tryb, :zrodlo, :wartosc, :wersjaref,
                                       :dodaj, :forceconsts, :tolerancja, :magazyn, :cenniknad, 0)
        returning_values :STATUS,:iletuprzel,:iletudod;
      ileprzel = :ileprzel+:iletuprzel;
      iledod = :iledod+:iletudod;
    end
  end
  /* przeliczanie pozycji ze stala cena i marza */
  if(:WERSJAREF is not null and :WERSJAREF<>0) then begin
    for
        select CENNIK.KTM, CENNIK.JEDN, CENNIK.WERSJAREF, CENNIK.STALACENA, CENNIK.CENANET, CENNIK.NARZUT,
               CENNIK.MARZA, DEFCENNIK.TRYB, DEFCENNIK.ZRODLO, DEFCENNIK.KURS, TOWJEDN.przelicz
          from CENNIK left join DEFCENNIK on (DEFCENNIK.REF=CENNIK.CENNIK)
                      left join TOWJEDN on (CENNIK.JEDN = TOWJEDN.REF)
          where (CENNIK.CENNIK=:ref
            and ((CENNIK.wersjaref=:WERSJAREF) or (:WERSJAREF=-1))
            and CENNIK.stalacena<>0)
        into :ktm,:jedn,:werref, :stalacena, :staracenanet, :starynarzut,
             :staramarza, :tryb, :zrodlo, :kurs, :przelicz
    do begin
      if((:przelicz is null) or (:przelicz = 0)) then
        przelicz = 1;
      if(:stalacena=1) then begin /* stala cena */
        execute procedure CENNIK_OBLICZREG(:ktm, :werref, 1, :zrodlo, :starynarzut, :przelicz, :zrodlo, :kurs,
                                           :staracenanet, 1, :tolerancja, 0, :magazyn, :cenniknad)
          returning_values :nowacenanet,:nowacenabru,:nowamarza,:nowynarzut;
        if((:nowynarzut<>:starynarzut) or (:nowamarza<>:staramarza)) then begin
          update cennik set marza = :nowamarza, narzut = :nowynarzut, cenanet = :nowacenanet, cenabru = :nowacenabru
            where CENNIK = :ref
              and WERSJAREF = :werref
              and JEDN=:jedn;
          ileprzel = :ileprzel+1;
        end
      end else if(:stalacena=2) then begin /* stala marza lub narzut */
        execute procedure CENNIK_OBLICZREG(:ktm, :werref, 1, :zrodlo, :starynarzut, :przelicz, :zrodlo, :kurs, 0, 0,
                                           :tolerancja, 0, :magazyn, :cenniknad)
          returning_values :nowacenanet,:nowacenabru,:nowamarza,:nowynarzut;
        if(:nowacenanet<>:staracenanet) then begin
          update cennik set CENAPNET = CENANET, CENAPBRU = CENABRU, cenanet = :nowacenanet, cenabru = :nowacenabru
            where CENNIK = :ref
              and WERSJAREF = :werref
              and JEDN=:jedn;
          ileprzel = :ileprzel+1;
        end
      end
    end
  end
end^
SET TERM ; ^
