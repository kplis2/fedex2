--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DURATIONSTR2(
      D bigint,
      ADD_LEADING_ZERO smallint = 0,
      ALWAYS_SHOW smallint = 0,
      SHOW_ZEROMINUTES smallint = 1)
  returns (
      TS varchar(24) CHARACTER SET UTF8                           )
   as
declare variable h bigint;
declare variable m bigint;
declare variable minus smallint;
begin
/* Procedura zamienia liczbe sekund D na godziny format czasu GG:MM. Parametry:
   - jezeli ADD_LEADING_ZERO = 0 to 0G:MM = G:MM w.pp = 0G:MM
   - jezeli ALWAYS_SHOW = 1 i czas D < 60 to TS = 00:00 w.pp TS = ''
   - jezeli SHOW_ZEROMINUTES = 1 to GG:00 = GG w.pp. = GG:00 (BS44368)  */

  if (d < 0) then
  begin
    d = -d;
    minus = 1;
  end

  m = d/60; --minut w sumie
  h = m/60; --godzin
  m = m - h*60; --minuty pozostale
  if (h = 0 and m = 0 and always_show = 0) then
    ts = '';
  else begin
    --dodaje godziny
    ts = cast(h as varchar(20));
    if (h < 10 and add_leading_zero = 1) then
      ts = '0' || ts;

    --dodaje minuty
    if (m > 0 or (m = 0 and show_zerominutes = 1)) then
    begin
      ts = ts || ':';
      if (m < 10) then
        ts  = ts || '0';
      ts = ts || cast(m as varchar(3));
    end

    if (minus = 1) then
      ts = '-' || ts;
  end

  suspend;
end^
SET TERM ; ^
