--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_FEDEX_ORDERPRINT_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer,
      REF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable transportorder integer;
declare variable transportordersymb varchar(20);
declare variable externalsymb varchar(20);
declare variable grandparent integer;
declare variable shipper integer;
declare variable accesscode string80;
declare variable filepath string255;
declare variable format string10;
begin
  select l.ref, l.symbol, l.symbolzewn, l.spedytor
    from listywys l
    where l.ref = :oref
  into :transportorder, :transportordersymb, :externalsymb, :shipper;

  --sprawdzanie czy istieje dokument spedycyjny
  if(:transportorder is null) then exception ede_fedex 'Brak listy spedycyjnej.';

  val = null;
  parent = null;
  params = null;
  id = 0;
  name = 'pobierzDokumentWydania';
  suspend;

  grandparent = :id;

  --dane autoryzacyjne
  select k.klucz, k.sciezkapliku
    from spedytkonta k
    where k.spedytor = :shipper
  into :accesscode, :filepath;

  val = :accesscode;
  name = 'kodDostepu';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = :filepath;
  name = 'sciezkaPliku';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = 'PRODUCTION'; --jesli chcesz testowac zmien na TEST
  name = 'serviceUrlType';
  id = id + 1;
  parent = GrandParent;
  suspend;

  select symboldok from ededocsh where ref = :ref
  into :format;
  if (coalesce(:format,'') = '') then format = 'PDF';
  val = :format;
  name = 'format';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = :transportorder;
  name = 'zlecTransportowe';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  execute procedure usun_znaki(:transportordersymb,0,'','')
    returning_values :transportordersymb;
  val = :transportordersymb;
  name = 'zlecTransportoweSymb';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;

  val = :externalsymb;
  name = 'numerDokumentu';
  params = null;
  parent = grandparent;
  id = id + 1;
  suspend;
end^
SET TERM ; ^
