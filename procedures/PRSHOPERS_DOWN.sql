--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHOPERS_DOWN(
      PRSHEET integer,
      PRSHOPERCUR integer)
  returns (
      PRSHOPER integer)
   as
declare variable complex integer;
begin
  for select ref, complex
    from prshopers
    where sheet = :prsheet and
      (:prshopercur is null and opermaster is null or :prshopercur is not null and opermaster is not null and opermaster = :prshopercur)
    order by number
    into :prshoper, :complex
  do begin
    if(complex = 1) then begin
      suspend;
      for select prshoper
        from prshopers_down (:prsheet, :prshoper)
        into :prshoper
      do begin
        suspend;
      end
    end else suspend;
  end
end^
SET TERM ; ^
