--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_LISTYWYSD_GET_PACKOPER(
      KODKRESK KODKRESKOWY)
  returns (
      OPERREF integer,
      OPERNAZWA varchar(60) CHARACTER SET UTF8                           ,
      STARTPAK smallint)
   as
declare variable intstatus smallint;
begin
  operref = 0;
  startpak = 0;

  select ref, nazwa from operator where upper(login) = :kodkresk
  into :operref, :opernazwa;

  if (:operref = 0) then
    select ref, nazwa from operator where upper(nazwa) = :kodkresk
    into :operref, :opernazwa;

  if (:operref = 0) then
  begin
    execute procedure CHECK_STRING_TO_INTEGER (:kodkresk) returning_values :intstatus;
    if (:intstatus is null) then intstatus = 0;

    if (:intstatus = 1) then
      select ref, nazwa from operator where ref = cast(:kodkresk as integer)
      into :operref, :opernazwa;
  end

  if (:startpak is null) then startpak = 0;
end^
SET TERM ; ^
