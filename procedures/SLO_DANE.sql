--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SLO_DANE(
      SLODEF integer,
      SLOPOZ integer)
  returns (
      KOD varchar(100) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      KONTOFK ACCOUNT_ID)
   as
declare variable kodks ANALYTIC_ID;
begin
  execute procedure SLO_DANEKS(slodef,  slopoz)
    returning_values kod, nazwa, kontofk, kodks;
  suspend;
end^
SET TERM ; ^
