--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SCHEDULEVERITY_DELETE(
      GENID integer)
   as
declare variable nref integer;
declare variable pref integer;
declare variable zref integer;
declare variable wydania smallint;
begin
  select n.ref, t.wydania
    from nagzam n
      left join typzam t on (t.symbol = n.typzam)
    where n.schedview = -:genid
    into nref, wydania;
  if (nref is not null) then
  begin
    for
      select p.ref
        from pozzam p
        where p.zamowienie = :nref
        into pref
    do begin
      for
        select distinct p.zamowienie
          from PR_POZZAM_CASCADE(:pref,0,0,0,1) pr
            left join pozzam p on (p.ref = pr.ref)
          where p.out = 1 and pr.ordertypeout = 3
          order by p.zamowienie desc
          into zref
      do begin
        update prschedguides g set g.status = 0 where g.zamowienie = :zref;
        delete from prschedzam s where s.zamowienie = :zref;
      end
      if (wydania <> 1) then
      begin
        update pozzam p set p.ilosc = 0, p.iloscm = 0 where p.ref = :pref;
      end
      else
      begin
        delete from relations where stablefrom = 'POZZAM' and sreffrom = :pref;
        delete from relations where stableto = 'POZZAM' and srefto = :pref;
        delete from pozzam where ref = :pref;
      end
      if (not exists (select first 1 1 from pozzam p where p.zamowienie = :nref and p.ilosc > 0)) then
        delete from nagzam n where n.ref = :nref;
    end
  end
  delete from PRSCHEDULEVERIFY;
end^
SET TERM ; ^
