--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRDSCR_FK_FUN_GETACC(
      ACCMASK ACCOUNT_ID,
      ELEMHIER varchar(40) CHARACTER SET UTF8                           ,
      COMPANYIN integer,
      BKYEAR integer,
      DICTPOS smallint,
      DICTHIERPOS varchar(40) CHARACTER SET UTF8                           )
  returns (
      COMPANYOUT integer,
      ACCOUNT ACCOUNT_ID,
      D1DICTDEF integer,
      D1SYMBOL SYMBOLDIST_ID,
      D2DICTDEF integer,
      D2SYMBOL SYMBOLDIST_ID,
      D3DICTDEF integer,
      D3SYMBOL SYMBOLDIST_ID,
      D4DICTDEF integer,
      D4SYMBOL SYMBOLDIST_ID,
      D5DICTDEF integer,
      D5SYMBOL SYMBOLDIST_ID)
   as
declare variable ACCMASK_TMP ACCOUNT_ID;
declare variable CUT integer;
declare variable CUTELEMHIER integer;
declare variable NLEVEL integer;
declare variable DICTDEF integer;
declare variable ACTUCOMPANY integer;
declare variable ANALLEN integer;
declare variable MAXNLEVEL integer;
declare variable ELEMHIER_AKT varchar(40);
declare variable ACCOUNT_PART ANALYTIC_ID;
declare variable SEPARATOR varchar(1);
declare variable ELEMHIERCNT integer;
declare variable DICTHIERCNT integer;
declare variable DICTCNT integer;
declare variable DICTHIERPOS_AKT varchar(40);
declare variable DICTHIERPOS_INT integer;
declare variable DXDICTDEF integer;
declare variable DXSYMBOL account_id;
begin
  account = '';
  accmask_tmp = :accmask;
  nlevel = 0;
  if (:companyin is null) then
    select max(dimelemdef.company)
      from dimelemdef
      where position(','||ref||',' in ','||:elemhier||',')>0
        and dimelemdef.company is not null
      into :companyin;
  actucompany = companyin;
  -- okreslamy jaki poziom analityki dostalismy w parametrze procedury
  if (:accmask is not null and :accmask <> '') then
  begin
    account = substring(:accmask from 1 for 3);
    accmask = substring(:accmask from  4 for coalesce(char_length(:accmask),0)); -- [DG] XXX ZG119346
    if (:accmask is not null and :accmask <> '') then
    begin
      while (:accmask is not null and :accmask <> '' and coalesce(char_length(:accmask),0) > 0) -- [DG] XXX ZG119346
      do begin
        nlevel = :nlevel + 1;
        select max(accstructure.separator), min(accstructure.len)
          from accstructure
            join bkaccounts on (accstructure.bkaccount = bkaccounts.ref)
          where bkaccounts.symbol = :account and accstructure.nlevel = :nlevel
            and bkaccounts.yearid = :bkyear
            and ((bkaccounts.company = :companyin and :companyin is not null)
                 or (:companyin is null))
          into :separator, :anallen;
        cut = position(:separator in :accmask);
        if (:cut = 0) then
          exception FRDSCR_ACCOUNTMASK_ERROR;
        else
          accmask = substring(:accmask from  :cut + :anallen + 1 for coalesce(char_length(:accmask),0)); -- [DG] XXX ZG119346
      end
    end
  end
  -- skladamy konto ksiegowe z ktorego liczymy komorke sprawozdania
  if (:elemhier is not null and :elemhier <> '') then
  begin
    account_part = '';
    nlevel = :nlevel + 1;
    select max(accstructure.nlevel)
      from accstructure
        join bkaccounts on (accstructure.bkaccount = bkaccounts.ref)
      where bkaccounts.symbol = :account and bkaccounts.yearid = :bkyear
        and ((bkaccounts.company = :actucompany and :actucompany is not null)
             or (:actucompany is null))
      into :maxnlevel;
    while (:nlevel <= :maxnlevel)
    do begin
      account_part = '';
      select max(accstructure.separator), min(accstructure.len),min(accstructure.dictdef)
        from accstructure
          join bkaccounts on (accstructure.bkaccount = bkaccounts.ref)
        where bkaccounts.symbol = :account and accstructure.nlevel = :nlevel
          and bkaccounts.yearid = :bkyear
          and ((bkaccounts.company = :actucompany and :actucompany is not null)
               or (:actucompany is null))
        into :separator, :anallen, :dictdef;
      nlevel = :nlevel + 1;
      select max(dimelemdef.acc)
        from dimelemdef
        where position(','||ref||',' in ','||:elemhier||',')>0
          and dimelemdef.slodef = :dictdef
        into :account_part;
      accmask_tmp = :accmask_tmp||:separator;
      if (:account_part is null or :account_part = '') then
      begin
        account_part = '';
        while (coalesce(char_length(:account_part),0)<:anallen) -- [DG] XXX ZG119346
        do begin
          account_part = '_'||:account_part;
        end
      end
      accmask_tmp = :accmask_tmp||:account_part;
    end
  end
  -- skladamy wyrozniki jakie nas interesuja przy naliczaniu komorki sprawozdania
  if (:dictpos is not null and :dictpos > 0 and :dicthierpos <> '' and :dicthierpos is not null) then
  begin
    elemhier = ','||:elemhier||',';
    dicthierpos = ','||:dicthierpos||',';
    dicthiercnt = 0;
    elemhiercnt = 1;
    dictcnt = 1;
    cut = 0;
    cutelemhier = 0;
    -- dla okreslonych pozycji w hierarchii szukamy wyroznikow !!!
    while (coalesce(char_length(:DICTHIERPOS),0) > 1) -- [DG] XXX ZG119346
    do begin
      cut = position(',' in :DICTHIERPOS);
      DICTHIERPOS_akt = substring(:DICTHIERPOS from 1 for :cut-1);
      if (:cut < coalesce(char_length(:DICTHIERPOS),0)) then -- [DG] XXX ZG119346
        DICTHIERPOS = substring(:DICTHIERPOS from :cut + 1 for coalesce(char_length(:DICTHIERPOS) ,0)); -- [DG] XXX ZG119346
      else
        DICTHIERPOS = '';
      if (:DICTHIERPOS_akt <> '' and :DICTHIERPOS_akt <> ',' and :DICTHIERPOS_akt is not null) then
      begin
        dicthierpos_int = cast(:DICTHIERPOS_akt as integer);
        while ((:elemhiercnt <= :dicthierpos_int) and (coalesce(char_length(:elemhier),0) > 0)) -- [DG] XXX ZG119346
        do begin
          cutelemhier = position(',' in :elemhier);
          elemhier_akt = substring(:elemhier from 1 for :cutelemhier - 1);
          if (:cutelemhier < coalesce(char_length(:elemhier),0)) then -- [DG] XXX ZG119346
            elemhier = substring(:elemhier from :cutelemhier + 1 for coalesce(char_length(:elemhier) , 0)); -- [DG] XXX ZG119346
          else
            elemhier = '';
          if (:elemhier_akt <> '' and :elemhier_akt <> ',' and :elemhier_akt is not null) then
          begin
            if (:elemhiercnt = :dicthierpos_int) then
            begin
              select dimelemdef.slodef, dimelemdef.acc
                from dimelemdef
                where dimelemdef.ref = cast(:elemhier_akt as integer)
                into :dxdictdef, :dxsymbol;
              if (:dictcnt = 1) then
              begin
                d1dictdef = dxdictdef;
                d1symbol = dxsymbol;
              end else if (:dictcnt = 2) then
              begin
                d2dictdef = dxdictdef;
                d2symbol = dxsymbol;
              end else if (:dictcnt = 3) then
              begin
                d3dictdef = dxdictdef;
                d3symbol = dxsymbol;
              end else if (:dictcnt = 4) then
              begin
                d4dictdef = dxdictdef;
                d4symbol = dxsymbol;
              end else if (:dictcnt = 5) then
              begin
                d5dictdef = dxdictdef;
                d5symbol = dxsymbol;
              end
              dictcnt = :dictcnt + 1;
            end
            elemhiercnt = :elemhiercnt + 1;
          end
        end
      end
    end
  end
  account = :accmask_tmp;
  companyout = :actucompany;
end^
SET TERM ; ^
