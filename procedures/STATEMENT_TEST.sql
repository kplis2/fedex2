--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STATEMENT_TEST(
      SOURCE varchar(8191) CHARACTER SET UTF8                           ,
      KEY1 integer,
      KEY2 integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      TEST integer)
  returns (
      RET numeric(14,2),
      SQLSTR varchar(8191) CHARACTER SET UTF8                           )
   as
declare variable decl varchar(8191);
declare variable body varchar(8191);
declare variable lastvar integer;
begin
  execute procedure STATEMENT_MULTIPARSE(:source, :prefix, '', '', 1, 1)
    returning_values :decl, :body, :lastvar;
  execute procedure STATEMENT_EXECUTE(:decl, :body, :key1, :key2,1)
    returning_values :ret;
end^
SET TERM ; ^
