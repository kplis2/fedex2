--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZFAKUSL_FKO_CREATE(
      POZFAKREF POZFAK_ID,
      DATAOD TIMESTAMP_ID,
      DATADO TIMESTAMP_ID,
      NAGFAKREF NAGFAK_ID)
   as
declare variable KWOTA numeric(14,4);
declare variable KLIENT KLIENCI_ID;
begin
    kwota = 0;

    if(:pozfakref is null) then
    begin
        exception POZFAKUSL_NIEUSLUGA 'Nie numeru pozycji faktury korygującej';
    end
    if (:nagfakref is not null and
        exists(select first 1 1 from pozfak p where ref = :pozfakref and dokument = :nagfakref)) then
      exception POZFAKUSL_NIEUSLUGA 'Nie można dodawać samej siebie';

    -- dodanie rekordu jeżeli podano poprawny REF faktury
    if((nagfakref is not null) and (exists (select first 1 1 from nagfak g where g.ref=:nagfakref))
        and not exists(select first 1 1 from pozfakusl p where p.refpozfak=:pozfakref and p.refnagfak=:nagfakref)) then
    begin
      insert into POZFAKUSL(REFNAGFAK, REFPOZFAK, KWOTA, TRYB)
        values(:nagfakref, :pozfakref, 0, 3);
    end
    --  dodanie rekordów jeżeli nie ma podanego FAKKORYGREF, ale jest okres
    else if((dataod is not null) and (datado is not null)) then
    begin
        select n.klient
          from pozfak p
            join nagfak n on (p.dokument = n.ref)
          where p.ref = :pozfakref
        into :klient;

        for select n.ref
            from nagfak n
              join typfak t on (n.typ = t.symbol)
            where (n.klient = :klient)
              and (n.data>=:dataod and n.data<=:datado)
              and t.sad = 0 and t.zaliczkowy = 0 and t.korokresowa = 0
              and not exists(select first 1 1 from pozfakusl p where p.refpozfak=:pozfakref and p.refnagfak=n.ref)
            into :nagfakref
        do begin
          insert into POZFAKUSL(REFNAGFAK, REFPOZFAK, KWOTA, TRYB)
            values(:nagfakref, :pozfakref, 0, 3);
        end
    end
    --jeżeli nie podano odpowiednich pól, to wyjątek
    else begin
       exception universal 'Musisz podać numer faktury korygowanej lub okrelić okres czasowy (daty od i do)';
    end

end^
SET TERM ; ^
