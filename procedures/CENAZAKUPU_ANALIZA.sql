--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CENAZAKUPU_ANALIZA(
      KTM varchar(40) CHARACTER SET UTF8                           ,
      CENA numeric(14,2),
      WART1 numeric(14,2),
      WART2 numeric(14,2),
      PARAM1 varchar(40) CHARACTER SET UTF8                           ,
      PARAM2 varchar(40) CHARACTER SET UTF8                           ,
      REFCENNIKZAK integer)
  returns (
      CENAWYJ numeric(14,2))
   as
declare variable procent1 numeric(14,2);
declare variable procent2 numeric(14,2);
begin
  execute procedure CENAZAKUPU_PROCENT(:refcennikzak, :param1, :wart1)
    returning_values :procent1;
  execute procedure CENAZAKUPU_PROCENT(:refcennikzak, :param2, :wart2)
    returning_values :procent2;
  CENAWYJ = CENA  *  (100 -:procent1 -:procent2) / 100;
  suspend;
end^
SET TERM ; ^
