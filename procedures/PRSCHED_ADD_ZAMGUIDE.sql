--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_ADD_ZAMGUIDE(
      SCHEDZAMREF integer,
      SCHEDPOZZAMREF integer,
      AMOUNT numeric(14,4))
   as
declare variable OPERREF integer;
declare variable OPERNUM integer;
declare variable PRSCHEDOPERREF integer;
declare variable PRSCHEDULE integer;
declare variable NUM integer;
declare variable ZAM integer;
declare variable PRSHEET integer;
declare variable GUIDEREF integer;
declare variable TYPHAR smallint;
declare variable SCHEDGUIDEREF smallint;
declare variable PRDEPART varchar(20);
declare variable AMOUNTNAGZAM numeric(14,4);
declare variable AMOUNTPOZZAM numeric(14,4);
declare variable ID varchar(30);
declare variable KTM varchar(80);
declare variable WERSJA integer;
declare variable AMOUNTDECL numeric(14,4);
declare variable QUANTITY numeric(14,4);
declare variable KAMOUNTSHORTAGE numeric(14,4);
declare variable NUMBER integer;
declare variable KTMPOS varchar(80);
declare variable WERSJAPOS integer;
declare variable KAMOUNT numeric(14,4);
declare variable KAMOUNTNORM numeric(14,4);
declare variable WAREHOUSE varchar(3);
declare variable OUT smallint;
declare variable PRSHMAT integer;
declare variable MATGROSSQUANTITY numeric(14,4);
declare variable PROPER varchar(20);
declare variable POZZAMREF integer;
declare variable NUMER integer;
declare variable PRSCHEDOPER integer;
declare variable AMOUNTDECLDECL numeric(14,4);
declare variable PRSCHEDGUIDESPOS integer;
declare variable OPERTYPE smallint;
declare variable OPERACTIV smallint;
declare variable PRSHOPERALTMASTER integer;
declare variable PRSHOPERMASTER integer;
declare variable WORKPLACE varchar(20);
declare variable ADDZAMGUIDE varchar(255);
declare variable REPORTEDFACTOR numeric(14,4);
declare variable PRSCHMETHOD varchar(20);
declare variable PRSCHDATE timestamp;
declare variable PRSCHMACHINE smallint;
declare variable SQL varchar(60);
declare variable OUTONPOZZAMENABLED smallint;
declare variable PRPOZZAM integer;
declare variable KILORG numeric(14,4);
declare variable SHORTAGES numeric(14,4);
declare variable KILCALCORG numeric(14,4);
declare variable TOINSERT numeric(14,4);
declare variable POSPRSHEET integer;
declare variable DOCTYPEIN varchar(3);
declare variable DOCTYPEOUT varchar(3);
declare variable AUTODOCOUT smallint;
declare variable PRPREPROD smallint;
declare variable PRSCHEDGUIDEPOSTO integer;
declare variable TOOPER integer;
declare variable OPENOPER integer;
declare variable DESCRIPT varchar(255);
declare variable SYMBOL varchar(40);
declare variable UNREPORTED smallint;
declare variable PRODTRANSFER smallint;
declare variable SCHEDVIEW smallint;
declare variable PROPERPOZZAM integer;
declare variable TMPGUIDE integer;
begin
  select t.praddzamguideproc, t.outonpozzamenabled, n.prpreprod
    from prschedzam s
      left join nagzam n on (n.ref = s.zamowienie)
      left join typzam t on (t.symbol = n.typzam)
    where s.ref = :schedzamref
    into :addzamguide, :outonpozzamenabled, prpreprod;
  if (outonpozzamenabled is null) then outonpozzamenabled = 0;
  if (coalesce(addzamguide,'') <> '') then begin
    execute statement 'execute procedure '||:addzamguide||' ('||coalesce(:SCHEDZAMREF,'null')||','||coalesce(:amount,0)||')';
    exit;
  end
  typhar = null;
  if (outonpozzamenabled = 0 and prpreprod = 0) then
  begin
    select psz.prschedule, psz.prsheet, psz.zamowienie, pd.typhar, pd.symbol, psz.prschmethod, psz.prschdate, psz.prschmachine
      from prschedzam psz
        left join prdeparts pd on (psz.prdepart = pd.symbol)
      where psz.ref=:schedzamref
      into :prschedule, :prsheet,  :zam, :typhar, :prdepart, :prschmethod, :prschdate, :prschmachine;
    if(prdepart is null) then exception prschedules_error 'Nieokreślony wydział produkcyjny. Dodanie zlecenia niemożliwe.';
    select id, kilosc - kilzreal, kktm, kwersja
     from nagzam
     where ref = :zam
     into :id, :amountnagzam, :ktm, :wersja;
    if(amount is null or amount = 0) then amount = :amountnagzam;
    select batchquantity, quantity, shortage
      from prsheets
      where ref = :prsheet
      into :amountdecl, :quantity, :kamountshortage;
    if(typhar = 0) then
      amountdecldecl = :amountdecl;
    else begin
      amountdecldecl = amount;
      amountdecl = amount;
    end
    kamountshortage = (100+ :kamountshortage) / 100;
    if(amountdecl is null or amountdecl = 0) then exception prschedules_error 'Brak wskazania ilości przewodnika.';
    --wygenerowanie przewodników wg ilosci deklarowanej na kartach
    while (amount > 0) do begin
      if(amount > amountdecl) then
        amount = amount - amountdecl;
      else begin
        amountdecl = amount;
        amount = 0;
      end
      select max(numer) from prschedguides where prschedzam = :SCHEDZAMREF into :number;
      if(number is null) then number = 1;
      else number = number + 1;
      execute procedure GEN_REF('PRSCHEDGUIDES') returning_values :guideref;
      prschdate = current_timestamp;
      insert into prschedguides (ref, zamowienie, amount, amountzreal, symbol, numer, outonpozzamenabled,
                               prdepart, status, ktm, wersja, prschedule, prschedzam,
                               prsheet, amountdecl, pggamountcalc, prschmethod, prschdate, prschmachine)
        values(:guideref, :zam, :amountdecl, 0,
            substring(:id||'/'||cast(:number as varchar(8)) from 1 for 40),
            :number, :outonpozzamenabled, :prdepart, 0, :ktm, :wersja, :prschedule, :schedzamref, :prsheet, :amountdecldecl, 1,
            :prschmethod, :prschdate, :prschmachine);
      --wygenerowanie operacji do przewodników
      num = 1;
      reportedfactor = 1;
      for
        select d.prshoper, A.number, A.oper, b.ref, a.workplace, A.reportedfactor
          from prshopers_down(:prsheet, null) d
            left join prshopers A on (d.prshoper = a.ref)
            left join prshopers B on (A.opermaster = B.REF)
          where --(a.opermaster is null or b.opertype <> 1 or (a.number = 1)) and --tylko te oepracje, które nie sa alternatywne
            a.scheduled = 1
          into :operref,  :opernum, :proper , :prshopermaster, :workplace, :reportedfactor  --?? opernum
      do begin
        if(reportedfactor is null or reportedfactor = 0) then reportedfactor = 1;
        execute procedure prschedoper_get_prshoperaltmast(:operref, null) returning_values :prshoperaltmaster;
        execute procedure GEN_REF('PRSCHEDOPERS') returning_values :prschedoperref;
        insert into prschedopers(REF, NUMBER, SHOPER, OPER, zamowienie, schedule, schedzam, guide, status, prshoperaltmaster, prshopermaster, workplace, ord, reportedfactor, prdepart)
          VALUES (:prschedoperref,:NUM, :OPERREF, :proper,:ZAM, :prschedule, :schedzamref, :guideref, 0, :prshoperaltmaster, :prshopermaster, :workplace, 1, :reportedfactor, :prdepart);
        execute procedure PRSCHED_OPER_CHECKDEPEND(:prschedoperref);
        num = num + 1;
      end
      --wygenerowanie pozycji do przewodników
      for
        select p.ref, p.ktm, p.wersja,  p.kilosc, p.magazyn, p.prshmat, p.out, pshm.grossquantity, p.numer, pso.ref
          from pozzam p
            left join prshmat pshm on (p.prshmat = pshm.ref)
            left join prschedopers pso on (pso.shoper = pshm.opermaster and pso.guide = :guideref)
            left join prsheets s on s.ref = pshm.sheet
            left join prshopers o on pso.shoper = o.ref
          where p.zamowienie = :zam
          into :pozzamref, :ktmpos, :wersjapos,  :kamount, :warehouse, :prshmat, :out, :matgrossquantity, :numer, :prschedoper
      do begin
        if(quantity <> 0) then kamountnorm = :matgrossquantity/:quantity;
        if(kamountnorm is null or kamountnorm = 0) then kamountnorm = 1;
        if(prschedoper is null) then
          select min(ref) from prschedopers where guide = :guideref into :prschedoper;
        if(prschedoper is null) then exception prschedguides_error 'Brak dowiązania pozycji zlecenia do operacji harmonogramu';
        insert into prschedguidespos (pozzamref, prschedguide, ktm, wersja, amount, kamountshortage,
                                      prshmat, warehouse, out, prschedoper, kamountnorm, number)
          values(:pozzamref, :guideref, :ktmpos, :wersjapos, :amountdecl * :kamount, :kamountshortage,
                                   :prshmat, :warehouse, :out, :prschedoper, :kamountnorm, :numer);
      end
      --aktywowanie operacji
      execute procedure prschedoper_alt_set_default(:guideref);
      --zaharmonogramowanie przewodnika
      select pr.sql
        from prschmethods pr
        where pr.symbol = :prschmethod
        into :sql;
      if (coalesce(:sql,'') <> '') then
      begin
        execute statement 'execute procedure ' || :sql || '('||:guideref||');';
      end
    end
    --Sebatodo tu przydaloby sie sparametryzowac nadawanie stanow
    update prschedguides set pggamountcalc = 0, kstan = 'D', stan = 'R'  where prschedzam = :schedzamref;
    update prschedguidespos set pggamountcalc = 0 where prschedguide in
      (select ref from prschedguides where ref = :schedzamref);
    execute procedure prschedzam_obl(:SCHEDZAMREF);
  end else if (outonpozzamenabled > 0 and prpreprod = 0) then
  begin
    for
      select psz.prschedule, p.prsheet, psz.zamowienie, pd.typhar, pd.symbol, psz.prschmethod, psz.prschdate, psz.prschmachine, p.ref, p.prodtransfer, n.schedview
        from prschedzam psz
          left join prdeparts pd on (psz.prdepart = pd.symbol)
          left join pozzam p on (p.zamowienie = psz.zamowienie)
          left join nagzam n on (n.ref = p.zamowienie)
          left join prsheets s on (s.ref = p.prsheet)
        where psz.ref=:schedzamref and s.prdepart = psz.prdepart and p.out = 0 and (p.ref = :schedpozzamref or :schedpozzamref is null)
        into :prschedule, :prsheet, :zam, :typhar, :prdepart, :prschmethod, :prschdate, :prschmachine, :prpozzam, :prodtransfer, :schedview
    do begin
      if(prdepart is null) then exception prschedules_error 'Nieokreślony wydział produkcyjny. Dodanie zlecenia niemożliwe.';
      select n.id, p.ilosc - p.ilzreal, p.ktm, p.wersja
       from pozzam p
         left join nagzam n on (n.ref = p.zamowienie)
       where p.ref = :prpozzam
       into :id, :amountpozzam, :ktm, :wersja;
      if(amount is null or amount = 0) then amount = :amountpozzam;
      select batchquantity, quantity, shortage
        from prsheets
        where ref = :prsheet
        into :amountdecl, :quantity, :kamountshortage;
      if(typhar = 0) then
        amountdecldecl = :amountdecl;
      else begin
        amountdecldecl = amount;
        amountdecl = amount;
      end
      kamountshortage = (100+ :kamountshortage) / 100;
      if(amountdecl is null or amountdecl = 0) then exception prschedules_error 'Brak wskazania ilości przewodnika.';
      --wygenerowanie przewodników wg ilosci deklarowanej na kartach
      while (amount > 0) do begin
        if(amount > amountdecl) then
          amount = amount - amountdecl;
        else begin
          amountdecl = amount;
          amount = 0;
        end
        select max(numer) from prschedguides where prschedzam = :SCHEDZAMREF into :number;
        if(number is null) then number = 1;
        else number = number + 1;
        execute procedure GEN_REF('PRSCHEDGUIDES') returning_values :guideref;
        prschdate = current_timestamp;
        EXECUTE PROCEDURE XK_PR_DEFAULTDOCTYPES(:prpozzam) returning_values (:doctypein, :doctypeout);
        symbol = substring(:id||'/'||cast(:number as varchar(8)) from 1 for 40);
        insert into prschedguides (ref, zamowienie, amount, amountzreal, symbol, pozzam,
                                 numer, outonpozzamenabled, prdepart, status, ktm, wersja, prschedule, prschedzam,
                                 prsheet, amountdecl, pggamountcalc, prschmethod, prschdate, prschmachine,
                                 amountshortagedecl, amountshortage, kamountshortage,
                                 doctypein, doctypeout, prodtransfer, schedview)
          values(:guideref, :zam, :amountdecl, 0,
              :symbol, :prpozzam,
              :number, :outonpozzamenabled, :prdepart, 0, :ktm, : wersja, :prschedule, :schedzamref,
              :prsheet, :amountdecldecl, 1, :prschmethod, :prschdate, :prschmachine,
              (:kamountshortage - 1) * :amountdecl, 0, :kamountshortage - 1,
              :doctypein, :doctypeout, :prodtransfer, :schedview);
        --wygenerowanie operacji do przewodników
        num = 1;
        reportedfactor = 1;
        for
          select d.refout, A.number, A.oper, b.ref, a.workplace, A.reportedfactor, d.tooperout, d.unreported
            from XK_PRSHEET_TREE_OPERS (:prsheet,null) d
              left join prshopers A on (d.refout = a.ref)
              left join prshopers B on (A.opermaster = B.REF)
            where a.scheduled = 1
            into :operref,  :opernum, :proper , :prshopermaster, :workplace, :reportedfactor, :tooper, :unreported
        do begin
          descript = '';
          select p.descript
            from prshopers o
              left join prsheets p on (p.ref = o.sheet)
            where o.ref = :operref
            into descript;
          if(reportedfactor is null or reportedfactor = 0) then reportedfactor = 1;
          execute procedure prschedoper_get_prshoperaltmast(:operref, null) returning_values :prshoperaltmaster;
          execute procedure GEN_REF('PRSCHEDOPERS') returning_values :prschedoperref;
          insert into prschedopers(REF, NUMBER, SHOPER, OPER, zamowienie, schedule, schedzam, guide, status, prshoperaltmaster, prshopermaster, workplace, ord, reportedfactor, prdepart, tooper, descript, unreported)
            VALUES (:prschedoperref,:NUM, :OPERREF, :proper,:ZAM, :prschedule, :schedzamref, :guideref, 0, :prshoperaltmaster, :prshopermaster, :workplace, 1, :reportedfactor, :prdepart, :tooper, :descript, :unreported);
          execute procedure PRSCHED_OPER_CHECKDEPEND(:prschedoperref);
          num = num + 1;
        end
        --wygenerowanie pozycji do przewodników
        posprsheet = null;
        for
          select p.ref, p.ktm, p.wersja,  p.kilosc, p.magazyn, p.prshmat, p.out,
              pshm.grossquantity, p.numer, pso.ref, p.kilorg, p.shortages,
              case when p.prsheet is null then pshm.sheet else p.prsheet end, pshm.autodocout, coalesce(p.kilcalcorg,1)
            from PR_POZZAM_CASCADE(:prpozzam,0,0,0,1) pr
              left join pozzam p on (pr.ref = p.ref)
              left join prshmat pshm on (p.prshmat = pshm.ref)
              left join prschedopers pso on (pso.shoper = pshm.opermaster and pso.guide = :guideref)
              left join prshopers o on pso.shoper = o.ref
            where pr.ref <> :prpozzam and pr.prnagzamout = :zam
              and p.zamowienie = :zam and p.out <> 0 --and pso.ref is not null
            into :pozzamref, :ktmpos, :wersjapos, :kamount, :warehouse, :prshmat, :out,
                :matgrossquantity, :numer, :prschedoper, :kilorg, :shortages,
                :posprsheet, autodocout, kilcalcorg
        do begin
          if (number = 1 and autodocout = 0) then
          if (kilorg is null) then kilorg = kamount;
          if (shortages is null) then shortages = 0;
          if (kamount = kilorg * shortages) then
            kilorg = 1;
          else
            kilorg = kamount/kamountshortage/kilorg;
          toinsert = amountdecl * kamount;
          kamountnorm = toinsert / amountdecl;
          --kamountshortage = kamountnorm;
          if(kamountnorm is null or kamountnorm = 0) then kamountnorm = 1;
          if(prschedoper is null) then
            select min(ref) from prschedopers where guide = :guideref into :prschedoper;
          if(prschedoper is null) then exception prschedguides_error 'Brak dowiązania pozycji zlecenia do operacji harmonogramu';
          EXECUTE PROCEDURE XK_PR_DEFAULTDOCTYPES(:pozzamref) returning_values (:doctypein, :doctypeout);
          insert into prschedguidespos (pozzamref, prschedguide, ktm, wersja, amount, kamountshortage,
                                        prshmat, warehouse, out, prschedoper, kamountnorm, number, prsheet,
                                        doctypein, doctypeout, autodocout)
            values(:pozzamref, :guideref, :ktmpos, :wersjapos, :toinsert, :kamountshortage,
                                     :prshmat, :warehouse, :out, :prschedoper, :kamountnorm, :numer, :posprsheet,
                                     :doctypein, :doctypeout, :autodocout);
        end
        --aktywowanie operacji
        execute procedure prschedoper_alt_set_default(:guideref);
        -- powiazanie pomiedzy operacjami z roznych kart technologicznych
        execute procedure prschedoper_sheet_dep_gen(:guideref);
        -- otwarcie pierwszej operacj i przewodnika, gdy na karcie zdefinio - otwarcie na calosc
        openoper = null;
        for
          select s.ref
            from prschedopers s
              left join prschedoperdeps d on (d.depto = s.ref)
              left join prshopers op on (op.ref = s.shoper)
              left join prsheets p on (p.ref = op.sheet)
            where s.guide = :guideref and d.depto is null and p.amountinfrom = 2
            into openoper
        do begin
          execute procedure PRSCHEDOPER_AMOUNTIN (:openoper);
        end
        if (openoper is not null and openoper > 0) then
          execute procedure prschedguides_statuschange(:guideref,1,null);
        --zaharmonogramowanie przewodnika
        select pr.sql
          from prschmethods pr
          where pr.symbol = :prschmethod
          into :sql;
         if (coalesce(:sql,'') <> '') then
        begin
          execute statement 'execute procedure ' || :sql || '('||:guideref||');';
        end
      end
      --Sebatodo tu przydaloby sie sparametryzowac nadawanie stanow
      update prschedguides set pggamountcalc = 0, kstan = 'D', stan = 'R'  where prschedzam = :schedzamref;
      for
        select ref
          from prschedguides
          where prschedzam = :schedzamref
          into tmpguide
      do begin
        update prschedguidespos set pggamountcalc = 0 where prschedguide = :tmpguide;
      end
      execute procedure prschedzam_obl(:SCHEDZAMREF);
    end
  end else if (prpreprod > 0) then
  begin
    select psz.prschedule, psz.prsheet, psz.zamowienie, pd.typhar, pd.symbol, psz.prschmethod, psz.prschdate, psz.prschmachine
      from prschedzam psz
        left join prdeparts pd on (psz.prdepart = pd.symbol)
      where psz.ref=:schedzamref
      into :prschedule, :prsheet,  :zam, :typhar, :prdepart, :prschmethod, :prschdate, :prschmachine;
    if(prdepart is null) then exception prschedules_error 'Nieokreślony wydział produkcyjny. Dodanie zlecenia niemożliwe.';
    amount = 1;
    amountdecl = 1;
    quantity = 1;
    kamountshortage = 0;
    kamountshortage = (100+ :kamountshortage) / 100;
    if(amountdecl is null or amountdecl = 0) then exception prschedules_error 'Brak wskazania ilości przewodnika.';
    --wygenerowanie przewodnika na zbitke
    select max(numer) from prschedguides where prschedzam = :SCHEDZAMREF into :number;
    if(number is null) then number = 1;
    else number = number + 1;
    execute procedure GEN_REF('PRSCHEDGUIDES') returning_values :guideref;
    prschdate = current_timestamp;
    insert into prschedguides (ref, zamowienie, amount, amountzreal, symbol, numer, outonpozzamenabled,
                             prdepart, status, ktm, wersja, prschedule, prschedzam,
                             prsheet, amountdecl, pggamountcalc, prschmethod, prschdate, prschmachine, prpreprod)
      values(:guideref, :zam, :amountdecl, 0,
          'PREPROD/'||:guideref,
          :number, :outonpozzamenabled, :prdepart, 0, :ktm, :wersja, :prschedule, :schedzamref, :prsheet, :amountdecldecl, 1,
          :prschmethod, :prschdate, :prschmachine, 1);
    --wygenerowanie pozycji do przewodników
    for
      select p.ref, p.ktm, p.wersja,  p.kilosc, p.magazyn, p.prshmat, p.out, pshm.grossquantity, p.numer, pso.ref, p.prschedguidepos
        from pozzam p
          left join prshmat pshm on (p.prshmat = pshm.ref)
          left join prschedopers pso on (pso.shoper = pshm.opermaster and pso.guide = :guideref)
        where p.zamowienie = :zam
        into :pozzamref, :ktmpos, :wersjapos,  :kamount, :warehouse, :prshmat, :out, :matgrossquantity, :numer, :prschedoper, :prschedguideposto
    do begin
      kamountnorm = kamount;
      if(kamountnorm is null or kamountnorm = 0) then kamountnorm = 1;
      if(prshmat is null) then exception prschedguides_error 'Brak dowiązania pozycji zlecenia do pozycji karty technologicznej';
      insert into prschedguidespos (pozzamref, prschedguide, ktm, wersja, amount, kamountshortage,
                                    prshmat, warehouse, out, prschedoper, kamountnorm, number, prschedguideposto)
        values(:pozzamref, :guideref, :ktmpos, :wersjapos, :kamountnorm, 1,
                                 :prshmat, :warehouse, :out, :prschedoper, :kamountnorm, :numer, null);
    end
    --Sebatodo tu przydaloby sie sparametryzowac nadawanie stanow
    update prschedguides set pggamountcalc = 0, kstan = 'D', stan = 'R'  where prschedzam = :schedzamref;
    update prschedguidespos set pggamountcalc = 0 where prschedguide in
      (select ref from prschedguides where ref = :schedzamref);
    execute procedure prschedzam_obl(:SCHEDZAMREF);
  end
end^
SET TERM ; ^
