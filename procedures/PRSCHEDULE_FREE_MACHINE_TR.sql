--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDULE_FREE_MACHINE_TR(
      WORKPLACE varchar(20) CHARACTER SET UTF8                           ,
      MACHINE integer,
      MODE integer)
  returns (
      RMACHINE integer,
      RSTART timestamp,
      REND timestamp,
      WORKTIME double precision,
      REF integer)
   as
declare variable fromdate timestamp;
  declare variable todate timestamp;
  declare variable inactionfrom timestamp;
  declare variable inactionto timestamp;
  declare variable sql varchar(1024);
  declare variable operstart timestamp;
  declare variable operend timestamp;
  declare variable tmpend timestamp;
  declare variable stop smallint;
  declare variable cnt smallint;
  declare variable wasoper smallint;
  declare variable firstsuspend smallint;
begin
  -- mode 0 -> z uwzgldnieniem operacji produkcyjnych
  -- mode 1 -> tylko przypisania, bez operacji produkcyjnych

  sql = 'select mwp.prmachine, mwp.fromdate, mwp.todate';
  sql = sql || ' from prmachineswplaces mwp';
  sql = sql || ' where mwp.prworkplace = '''||:workplace||'''';
  if (:machine is not null) then
    sql = sql || ' and mwp.prmachine = '||:machine;
  sql = sql || ' order by mwp.fromdate';
  ref = 0;
  for
    execute statement :sql
      into :rmachine, :fromdate, :todate
  do begin
    -- operacje ktore juz zajely ta mazyne
    rstart = fromdate;
    rend = todate;
    operstart = null;
    operend = null;
    stop = 0;
    cnt = 0;
    wasoper = 0;
    while (stop = 0 and cnt < 500)
    do begin
      cnt = cnt + 1;
      operstart = null;
      operend = null;
      select first 1 g.timefrom, g.timeto
        from prgantt g
        where g.prmachine = :rmachine
          and ((g.status < 3 and g.verified > 0) or g.gtype > 0)
          and (g.timefrom < :todate or :todate is null)
          and (g.timeto > :rstart)
          and :mode = 0
          and g.gtype = 0
        order by g.timefrom
        into operstart, operend;
      if (operstart is null) then
        stop = 1;
      else
      begin
        if (:rstart  < :operstart and (:rstart < :todate or :todate is null)) then
        begin
          if (:operend < :todate or :todate is null) then
            rend = :operstart;
          else
            rend = :todate;
          -- awarie, remonty, wypadki
          inactionfrom = null;
          inactionto = null;
          tmpend = :rend;
          if (wasoper = 0) then wasoper = 1;
            for
              select r.inactionfrom, r.inactionto
                from prmachrepairs r
                where r.machine = :rmachine
                  and r.inactionfrom < :rend
                  and r.inactionto > :rstart
                into :inactionfrom, :inactionto
            do begin
              if (:rstart < :inactionfrom and :rstart < :tmpend) then
              begin
                if (:inactionto < :tmpend) then
                  rend = :inactionfrom;
                else
                  rend = :inactionto;
                worktime = null;
                execute procedure prmachine_free_worktime(:rmachine, :rstart, :rend) returning_values :worktime;
                ref = ref  + 1;
                suspend;
              end 
              rstart = :inactionto;
            end
          worktime = null;
          rend = tmpend;
          execute procedure prmachine_free_worktime(:rmachine, :rstart, :rend) returning_values :worktime;
          ref = ref + 1;
          suspend;
        end
        rstart = operend;
      end
    end
    if (wasoper = 1) then
    begin
      rend = null;
    end else
    begin
      rstart = fromdate;
      rend = null;
    end

    if (:rstart is not null and :rend is null) then -- brak przypisac
    begin
      --remonty bez maszyn
      inactionfrom = null;
      inactionto = null;
      tmpend = :rend;
      for
        select r.inactionfrom, r.inactionto
          from prmachrepairs r
          where r.machine = :rmachine
            and (r.inactionfrom < :tmpend or :tmpend is null)
            and r.inactionto > :rstart
          into :inactionfrom, :inactionto
      do begin
        if (:rstart < :inactionfrom and (:rstart < :tmpend or :tmpend is null)) then
        begin
          if (:inactionto < :tmpend or :tmpend is null) then
            rend = :inactionfrom;
          else
            rend = :inactionto;
          worktime = null;
          execute procedure prmachine_free_worktime(:rmachine, :rstart, :rend) returning_values :worktime;
          ref = ref + 1;
          suspend;
        end
        rstart = :inactionto;
      end
      if (:inactionfrom is null and :inactionto is null) then
      begin --brak remontow ipt
        worktime = null;
        execute procedure prmachine_free_worktime(:rmachine, :rstart, :rend) returning_values :worktime;
        ref = ref + 1;
        suspend;
      end
      else if (:inactionto < :tmpend or :tmpend is null) then
      begin
        rend = :tmpend;
        worktime = null;
        execute procedure prmachine_free_worktime(:rmachine, :rstart, :rend) returning_values :worktime;
        ref = ref + 1;
        suspend;
      end
    end
  end
end^
SET TERM ; ^
