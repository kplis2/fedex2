--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STKRTAB_GETMINMAX(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer)
  returns (
      STANMIN numeric(14,4),
      STANMAX numeric(14,4))
   as
declare variable "FOUND" smallint;
declare variable lstanmin numeric(14,4);
declare variable lstanmax numeric(14,4);
declare variable gstkrtab integer;
declare variable wersjaref integer;
declare variable nazwawersji varchar(255);
begin
  found = 0;
  -- szukanie przez konkretny KTM i wersje
  for select STKRMAG.stkrtab
  from STKRMAG
  where STKRMAG.magazyn = :magazyn
  order by STkrmag.priorytet desc
  into :gstkrtab
  do begin
    if(:found = 0) then begin
      select wersjaref, ilmin, ilmax from  STKRPOZ
       where STKRPOZ.tabela = :gstkrtab and STKRPOZ.ISMASKA=0 and STKRPOZ.KTM = :KTM and STKRPOZ.WERSJA = :wersja
      into :wersjaref, :lstanmin, :lstanmax;
      if(:wersjaref > 0) then begin
        stanmin = :lstanmin;
        stanmax = :lstanmax;
        found = 1;
      end
    end
  end
  if(:found = 0) then begin
    select DEFMAGAZ.stkrtab from DEFMAGAZ where SYMBOL = :magazyn into :gstkrtab;
    select wersjaref, ilmin, ilmax from  STKRPOZ
     where STKRPOZ.tabela = :gstkrtab and STKRPOZ.ISMASKA=0 and STKRPOZ.KTM = :KTM and STKRPOZ.WERSJA = :wersja
    into :wersjaref, :lstanmin, :lstanmax;
    if(:wersjaref > 0) then begin
        stanmin = :lstanmin;
        stanmax = :lstanmax;
        found = 1;
    end
  end
  -- szukanie przez maske KTM i maske wersji
  if(:found=0) then begin
    select nazwa from wersje where ktm=:ktm and nrwersji=:wersja into :nazwawersji;
    for select STKRMAG.stkrtab
    from STKRMAG
    where STKRMAG.magazyn = :magazyn
    order by STkrmag.priorytet desc
    into :gstkrtab
    do begin
      if(:found = 0) then begin
        lstanmin = -1; lstanmax = -1;
        select min(ilmin), max(ilmax) from STKRPOZ
          where STKRPOZ.tabela = :gstkrtab and STKRPOZ.ISMASKA=1 and :ktm like STKRPOZ.KTM||'%' and (:nazwawersji like STKRPOZ.MASKAWER||'%' or STKRPOZ.MASKAWER='')
          into :lstanmin, :lstanmax;
        if(:lstanmin<>-1 and :lstanmax<>-1) then begin
          stanmin = :lstanmin;
          stanmax = :lstanmax;
          found = 1;
        end
      end
    end
    if(:found = 0) then begin
      select DEFMAGAZ.stkrtab from DEFMAGAZ where SYMBOL = :magazyn into :gstkrtab;
      lstanmin = -1; lstanmax = -1;
      select min(ilmin), max(ilmax) from STKRPOZ
        where STKRPOZ.tabela = :gstkrtab and STKRPOZ.ISMASKA=1 and :ktm like STKRPOZ.KTM||'%' and (:nazwawersji like STKRPOZ.MASKAWER||'%' or STKRPOZ.MASKAWER='')
        into :lstanmin, :lstanmax;
      if(:lstanmin<>-1 and :lstanmax<>-1) then begin
        stanmin = :lstanmin;
        stanmax = :lstanmax;
        found = 1;
      end
    end
  end
  suspend;
end^
SET TERM ; ^
