--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_NUMERYSERYJNE(
      REFPOZ integer,
      TYP char(1) CHARACTER SET UTF8                           )
  returns (
      NUMERYSER varchar(255) CHARACTER SET UTF8                           )
   as
declare variable serial varchar(40);
begin
  numeryser='';
  for select d.odserial from dokumser d where d.refpoz=:refpoz and d.typ=:typ into :serial do begin
    if(coalesce(char_length(numeryser),0)<235) then -- [DG] XXX ZG119346
      if(numeryser='') then numeryser = :serial;
      else numeryser = :numeryser ||'; '||:serial;
  end
  suspend;
end^
SET TERM ; ^
