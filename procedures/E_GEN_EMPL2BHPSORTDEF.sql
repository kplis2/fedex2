--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GEN_EMPL2BHPSORTDEF(
      WORKPOST integer,
      EMPLDATE date,
      FROMDATE date,
      TODATE date,
      COMPANY integer)
  returns (
      COUNTER integer,
      ERRCOUNTER integer,
      ERREMPLLIST varchar(200) CHARACTER SET UTF8                           )
   as
declare variable employee integer;
declare variable sortsymbol varchar(20);
declare variable empl_descript varchar(150);
declare variable is_overfill smallint;
begin

/* MWr - Procedura generuje pozycje sortow pracowniczych w tabeli BHPEMPLSORTDEF
         wg zadanego stanowiska WORKPOST i tabeli BHPSORTDEF. Wartosc stanowiska
         pobierana jest z zadanego dnia zatrudnienia EMPLDATE. Sorty sa
         przypisywane na zadany okres waznosci FROMDATE-TODATE.*/

  counter = 0;
  workpost = coalesce(workpost, 0);
  errcounter = 0;   --licznik niewygenerownych sortow z powodu bledu
  errempllist = ''; --lista pracownikow przy ktorych wystapil blad
  is_overfill = 0;  --znacznik przepelnienia

  for
    select distinct e.ref, e.personnames, b.symbol
      from bhpsortdef b
        join edictworkposts w on (w.ref = b.workpost)
        join employment emp on (w.ref = emp.workpost)
        join employees e on (emp.employee = e.ref)
      where :empldate >= emp.fromdate
        and (:empldate <= emp.todate or emp.todate is null)
        and (emp.workpost = :workpost or :workpost = 0)
        and e.company = :company
      into :employee, :empl_descript, :sortsymbol
  do begin
      counter = counter + 1;
    
      insert into bhpemplsortdef (employee, bhpsortdef, fromdate, todate)
        values (:employee, :sortsymbol, :fromdate, :todate);
    
      when any do
      begin
        if (is_overfill = 0 ) then
        begin
          if ((coalesce(char_length(errempllist),0) + coalesce(char_length(empl_descript),0) + 4) >= 150) then -- [DG] XXX ZG119346
          begin
            is_overfill = 1;
            errempllist = errempllist || '..';
          end else
          begin
            if (errempllist = '') then
              errempllist = empl_descript;
            else
              errempllist = errempllist || '; ' || empl_descript;
          end
        end
        errcounter = errcounter + 1;
      end
  end
  counter = counter - errcounter;
  suspend;
end^
SET TERM ; ^
