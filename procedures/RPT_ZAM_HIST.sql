--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_ZAM_HIST(
      ZAMOWIENIE integer)
  returns (
      OPERACJA varchar(255) CHARACTER SET UTF8                           ,
      REJESTR varchar(5) CHARACTER SET UTF8                           ,
      BLOKADY varchar(2) CHARACTER SET UTF8                           ,
      OPIS varchar(400) CHARACTER SET UTF8                           ,
      DATA timestamp,
      OPERATOR varchar(255) CHARACTER SET UTF8                           )
   as
declare variable soperator varchar(255);
declare variable sdata timestamp;
declare variable sopis varchar(400);
declare variable soperacja varchar(255);
begin
  if(:zamowienie is null) then exit;
  select DATAWE, OPERATOR.NAZWA from NAGZAM join OPERATOR on ( NAGZAM.OPERATOR = OPERATOR.REF) where NAGZAM.REF=:ZAMOWIENIE into :DATA, :operator;
  OPIS = 'rejestracja zamówienia';
  OPERACJA = :OPIS;
  for select DEFOPERZAM.OPIS, HISTZAM.org_rej, HISTZAM.org_blokada, HISTZAM.OPIS, HISTZAM.DATA,OPERATOR.NAZWA
    from HISTZAM left join DEFOPERZAM on ( DEFOPERZAM.SYMBOL = HISTZAM.OPERACJA)
                 left join OPERATOR on (HISTZAM.OPERATOR  = OPERATOR.REF)
    where HISTZAM.zamowienie = :zamowienie
    order by HISTZAM.data
    into :soperacja, :rejestr, :blokady, :sopis, :sdata, :soperator
  do begin
    suspend;
    OPERACJA = :soperacja;
    OPERATOR = :soperator;
    DATA = :sdata;
    OPIS = :sopis;
  end
  select REJESTR, NAGZAM.stan from NAGZAM where REF=:ZAMOWIENIE into :rejestr, :blokady;
  suspend;
end^
SET TERM ; ^
