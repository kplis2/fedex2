--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_CTI_SHOWCONNECT(
      MPHONE varchar(20) CHARACTER SET UTF8                           )
  returns (
      INFO varchar(2048) CHARACTER SET UTF8                           ,
      SLODEF integer,
      SLOPOZ integer,
      BNTIDENT varchar(40) CHARACTER SET UTF8                           ,
      LINES integer,
      GRIDCOLOUR varchar(40) CHARACTER SET UTF8                           ,
      LASTUSERTEXT varchar(100) CHARACTER SET UTF8                           ,
      STABLE varchar(31) CHARACTER SET UTF8                           )
   as
declare variable pom varchar(10);
declare variable fskrot shortname_id; --XXX ZG133796 MKD
declare variable nip varchar(20);
declare variable nazwa varchar(255);
declare variable dulica varchar(255);
declare variable dkodp varchar(10);
declare variable dmiasto varchar(255);
declare variable telefon varchar(255);
declare variable komorka varchar(255);
declare variable email varchar(255);
declare variable operopieknazwa varchar(60);
declare variable cpodmiot integer;
declare variable maxref integer;
declare variable kontaktdata date;
declare variable kontakttyp varchar(40);
declare variable pkosoba varchar(60);
declare variable nagfakdata date;
declare variable nagfakwartosc numeric(14,2);
declare variable notatkatresc varchar(1000);
declare variable cnt integer;
declare variable toshow smallint;
declare variable slodefref integer;
begin
  INFO = 'Kontrahent nie został odnaleziony w bazie';
  SLODEF = null;
  SLOPOZ = null;
  BNTIDENT = '';
  pom = '
  ';
  GRIDCOLOUR = 'clWindow';
  LASTUSERTEXT = '';
  STABLE = 'KLIENCI';
  LINES = 1;
  toshow = 1;
  if (:mphone is null or :mphone = '' or :mphone = ' ' or :mphone = '1') then
  begin
    if (:mphone = '1') then
      INFO = 'Brak aktualnie prowadzonej rozmowy';
    suspend;
    if (:toshow = 1) then
      toshow = 0;
  end else
  begin
    select count(*)
      from cphones
      where cphones.mirrornumber like :mphone||'%'
      into :cnt;
    if (:cnt > 1) then
      GRIDCOLOUR = 'clRed';
    else
      GRIDCOLOUR = 'clWindow';
    for
      select cphones.stable, cphones.skey
        from cphones
        where cphones.mirrornumber like :mphone||'%'
      into :stable, :slopoz
    do begin
      cpodmiot = null;
      lines = 0;
      select min(ref) from slodef where slodef.typ = :stable into :slodef;
      -- wyświetlenie informacji odnosnie podmiotu z ktorym rozmawiamy
      select min(ref) from slodef where slodef.typ = 'KLIENCI' into :slodefref;
      if (:slodef = :slodefref) then begin
        bntident = 'KLIENCI';
        select klienci.fskrot, klienci.nip, klienci.nazwa, klienci.dulica,
            KLIENCI.DKODP, KLIENCI.DMIASTO, KLIENCI.TELEFON, KLIENCI.KOMORKA,
            KLIENCI.EMAIL, CPODMIOTY.OPEROPIEKNAZWA, cpodmioty.ref
          from klienci
            left join CPODMIOTY on (cpodmioty.slopoz = klienci.ref)
          where KLIENCI.REF = :slopoz and CPODMIOTY.slodef = 1
          into :fskrot, :nip, :nazwa, :dulica,
            :dkodp, :dmiasto , :telefon, :komorka,
            :email, :operopieknazwa, :cpodmiot;
          if (:operopieknazwa is null) then operopieknazwa = '';
          info = :fskrot||' - '||:nip||:pom||
            :nazwa||:pom||
            :dulica||' '||:dkodp||' '||:dmiasto||:pom||
            :telefon||' '||:komorka||' '||:email||:pom||
            :operopieknazwa||:pom;
          lines = :lines + 5;
      end
      maxref = null;
      select min(ref) from slodef where slodef.typ = 'KLIENCI' into :slodefref;
      if (:slodef = :slodefref) then
      begin
        select max(nagfak.ref) from nagfak where nagfak.klient = :slopoz
          into :maxref;
        select nagfak.data, (nagfak.sumwartnet - nagfak.psumwartnet)
          from nagfak where nagfak.ref = :maxref
          into :nagfakdata, :nagfakwartosc;
        if (:maxref is not null) then begin
          info = :info||'Ostatnia faktura VAT: '||:nagfakdata||' o wartości netto: '||:nagfakwartosc||:pom;
          lines = :lines + 1;
        end
      end
      -- wyświetlenie informacji przetrzymywanych w CRM
      if (:cpodmiot is not null) then
      begin
        bntident = :bntident||'CPODMIOTY';
        -- wyświetlenie informacji odnosnie ostatniego kontaktu: data kontktu, medium, z kim
        select max(kontakty.ref) from kontakty where kontakty.cpodmiot = :cpodmiot
          into :maxref;
        select kontakty.data, ctypykon.nazwa, pkosoby.nazwa
          from kontakty
            left join ctypykon on (kontakty.rodzaj = ctypykon.ref)
            left join pkosoby on (pkosoby.ref = kontakty.osoba)
          where kontakty.ref = :maxref
          into :kontaktdata, :kontakttyp, :pkosoba;
        if (:maxref is not null) then
        begin
          info = :info||'Ostatni kontakt: '||:kontaktdata||' typu: '||:kontakttyp||:pom;
          lines = :lines + 1;
          if (:pkosoba is not null and :pkosoba <> '') then
          begin
            info = :info||'z osobą: '||:pkosoba||:pom;
            lines = :lines + 1;
          end
        end
        -- wyświetlenie notatek dla podmiotu
        for
          select substring(cnotatki.tresc from 1 for 1000)
            from cnotatki
            where cnotatki.cpodmiot = :cpodmiot and cnotatki.powiadom = 1
              and (cnotatki.powiadomtime is null or cnotatki.POWIADOMTIME <= current_date)
            into :notatkatresc
        do begin
          info = :info||'----------------'||:pom||
            :notatkatresc||:pom;
          lines = :lines + 2;
        end
        info = :info||'----------------'||:pom;
        lines = :lines + 1;
      end
      if (:slodef = 1 and :fskrot <> '' and :fskrot is not null) then begin
        if (:operopieknazwa is null) then
          operopieknazwa = '';
        LASTUSERTEXT = :fskrot||' '||:operopieknazwa;
      end
      suspend;
      if (:toshow = 1) then
        toshow = 0;
    end
  end
  if (:toshow = 1) then
  begin
    toshow = 0;
    suspend;
  end
end^
SET TERM ; ^
