--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRDIMELEMS_COPYDIMELEMSDEF(
      FRDIMSHIER integer,
      DIMHIERDEF integer)
   as
begin
  insert into frdimelems (frdimhier, dimelem, act)
    select :frdimshier, dimelemdef.ref, dimelemdef.act
      from dimelemdef
      where dimelemdef.dimhier = :dimhierdef;
end^
SET TERM ; ^
