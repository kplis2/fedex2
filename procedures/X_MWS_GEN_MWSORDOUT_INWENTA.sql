--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_GEN_MWSORDOUT_INWENTA(
      DOCTYPE DOCTYPE_ID,
      DOCID integer)
  returns (
      REFMWSORD integer)
   as
declare variable docposid integer;
declare variable mwsordtype integer;
declare variable REC smallint;
declare variable WH varchar(3);
declare variable STOCKTAKING smallint;
declare variable OPERATOR integer;
declare variable PRIORITY smallint;
declare variable BRANCH varchar(10);
declare variable PERIOD varchar(6);
declare variable FLAGS varchar(40);
declare variable SYMBOL varchar(20);
declare variable COR smallint;
declare variable QUANTITY numeric(14,4);
declare variable TODISP numeric(14,4);
declare variable TOINSERT numeric(14,4);
declare variable GOOD varchar(40);
declare variable VERS integer;
declare variable OPERSETTLMODE smallint;
declare variable MWSCONSTLOCP integer;
declare variable MWSPALLOCP integer;
declare variable MWSCONSTLOCL integer;
declare variable AUTOLOCCREATE smallint;
declare variable MWSPALLOCL integer;
declare variable WHAREAG integer;
declare variable POSFLAGS varchar(40);
declare variable DESCRIPTION varchar(1024);
declare variable POSDESCRIPTION varchar(1024);
declare variable LOT integer;
declare variable CONNECTTYPE smallint;
declare variable SHIPPINGAREA varchar(3);
declare variable TYPDOK varchar(3);
declare variable QUANTITYONLOCATION numeric(15,4);
declare variable MAXNUMBER integer;
declare variable INSERTVOLUME numeric(14,4);
declare variable PRZELICZ numeric(14,4);
declare variable OBJ numeric(14,4);
declare variable DOCOBJ numeric(14,4);
declare variable PALTYPE varchar(40);
declare variable SHIPPINGTYPE integer;
declare variable NEXTMWSORD smallint;
declare variable MAXPALVOL numeric(14,4);
declare variable TAKEFULLPAL smallint;
declare variable FROMMWSCONSTLOC integer;
declare variable PALGROUPMM integer;
declare variable TAKEFULLPALMM integer;
declare variable TAKEFROMMWSCONSTLOCMM integer;
declare variable TAKEFROMMWSPALLOCMM integer;
declare variable POSCNT integer;
declare variable SLODEF integer;
declare variable SLOPOZ integer;
declare variable SLOKOD varchar(40);
declare variable DOCREAL smallint;
declare variable PALQUANTITY integer;
declare variable MWSWARNING varchar(255);
declare variable DIVWHENMORE integer;
declare variable DIVWHENMORES varchar(100);
declare variable DONTDIV smallint;
declare variable POSLEFT integer;
declare variable MWSPALLOCLPAL integer;
declare variable MWSGENPARTSS varchar(100);
declare variable MWSGENPARTS smallint;
declare variable POSREAL smallint;
declare variable NEXTPOS smallint;
declare variable DOCID1 integer;
declare variable DOCGROUP1 integer;
declare variable DOCPOSID1 integer;
declare variable VERS1 integer;
declare variable QUANTITY1 numeric(14,4);
declare variable QUANTITYACTS numeric(14,4);
declare variable QUANTITYADD numeric(14,4);
declare variable MIXPOS smallint;
declare variable TAKEFROMZEROS varchar(100);
declare variable KRAJID varchar(100);
declare variable TAKEFROMZERO smallint;
declare variable LOCSEC integer;
declare variable LOCATIONLOT integer;
declare variable ACTUVOLUME numeric(14,4);
declare variable BUFFORS varchar(100);
declare variable BUFFOR smallint;
declare variable UE smallint;
declare variable ZAGRANICZNY smallint;
declare variable INDYVIDUALLOT smallint;
declare variable MAGBREAK smallint;
declare variable quantityondoc quantity_mws;
declare variable quantityonstock quantity_mws;
declare variable x_partia date_id;
declare variable x_serial_no integer_id;
declare variable x_slownik integer_id;
begin
  -- MSt: Wykorzystuje te procedure do zrobienia wydania z lokacji INWENTA
  -- exception universal 'docid'||coalesce(docid,0) ;
  select first 1 t.ref, stocktaking, opersettlmode, autoloccreate
    from mwsordtypes t
    where t.mwsortypedets = 1
    order by t.ref
  into :mwsordtype, :stocktaking, :opersettlmode, :autoloccreate;

  select d.wydania, d.koryg, coalesce(d.mwsmag,d.magazyn), d.operator,
      d.uwagi,
      d.priorytet, d.oddzial, d.okres, d.flagi,
      d.symbol, d.kodsped, d.sposdost,
      d.takefullpal, d.palgroup, d.takefrommwsconstloc,
      d.takefrommwspalloc, 1, k.ref, substring(k.fskrot from 1 for 40),
      d.mwsdocreal, d.iloscpalet, d.typ, c.ue, k.krajid, k.zagraniczny
    from dokumnag d
      left join klienci k on (k.ref = d.klient)
      left join countries c on (c.symbol = k.krajid)
    where d.ref = :docid
    into rec, cor, wh, operator,
      description,
      priority, branch, period, :flags,
      symbol, shippingarea, shippingtype,
      takefullpalmm, palgroupmm, takefrommwsconstlocmm,
      takefrommwspallocmm, slodef, slopoz, slokod,
      docreal, palquantity, typdok, ue, krajid, zagraniczny;

  if (rec = 0) then rec = 1; else rec = 0;
  execute procedure gen_ref('MWSORDS') returning_values :refmwsord;
  insert into mwsords (ref, mwsordtype, stocktaking, doctype, docgroup, docid, operator, priority,
      description, wh,  regtime, timestartdecl, branch, period,
      flags, docsymbs, cor, rec, status, shippingarea, SHIPPINGTYPE, takefullpal,
      slodef, slopoz, slokod, palquantity, repal)
    values (:refmwsord, :mwsordtype, :stocktaking, 'M', :docid, :docid, :operator, :priority,
        :description, :wh, current_timestamp, null, :branch, :period,
        :flags, :symbol, :cor, :rec, 0, :shippingarea, :SHIPPINGTYPE, 0,
        :slodef, :slopoz, :slokod, :palquantity, 0);

  maxnumber = 0;
  mwsconstlocp = null;
  select first 1 c.ref
    from mwsconstlocs c
    where c.wh = :wh
      and c.locdest = 6
    into :mwsconstlocp;
  if (:mwsconstlocp is null) then
    exception universal 'Brak definicji lokacji inwentaryzacyjnej';

  for
    select p.ref, p.ktm, p.wersjaref, p.ilosc,
        p.flagi, p.uwagi, t.magbreak, p.dostawa,
        p.x_partia, p.x_serial_no, p.x_slownik
      from dokumpoz p
        left join towary t on (p.ktm = t.ktm)
        left join dokumpoz p1 on (p1.ref = p.alttopoz and p.fake = 1)
        left join towary t1 on (t1.ktm = p1.ktm)
      where p.dokument = :docid
        and t.usluga <> 1
        and ((coalesce(p.havefake,0) = 0 and coalesce(p.fake,0) = 0) or
             (coalesce(p.fake,0) = 1 and coalesce(t1.usluga,0) <> 1 and coalesce(t1.altposmode,0) = 1))
    into :docposid, :good, :vers, :quantityondoc,
      :posflags, :posdescription, :magbreak, :lot,
       :x_partia, :x_serial_no, :x_slownik
  do begin
    select max(number) from mwsacts where mwsord = :refmwsord into maxnumber;
    if (maxnumber is null) then maxnumber = 0;

    for
      select s.wharea, s.whsec, s.mwspalloc, s.quantity, s.lot
        from mwsstock s
        where s.wh = :wh
          and s.mwsconstloc = :mwsconstlocp
          and s.vers = :vers
          and s.quantity > 0
          and ((:x_partia is not null and s.x_partia = :x_partia) or  :x_partia is null)        -- XXX KBI
          and ((:x_serial_no is not null and s.x_serial_no = :x_serial_no) or :x_serial_no is null)  -- XXX KBI
          and ((:x_slownik is not null and s.x_slownik = :x_slownik) or :x_slownik is null)      -- XXX KBI
      into :whareag, :locsec, :mwspallocp, :quantityonstock, :lot
    do begin
      toinsert = minvalue(:quantityondoc,:quantityonstock);

      if (:toinsert > 0) then
      begin
        maxnumber = :maxnumber  + 1;
        insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
            mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
            regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc, plus,
            whareal, whareap, wharealogl, number, disttogo, palgroup, autogen,
            x_partia, x_serial_no, x_slownik)
          values (:refmwsord, 1, :stocktaking, :good, :vers, :toinsert, :mwsconstlocp, :mwspallocp,
            null, null, :docid, :docposid, 'M', :opersettlmode, :autoloccreate, 'MWS', :whareag, :locsec,
            current_timestamp, null, null, null, :posflags, substr(:posdescription,1,250), :priority, :lot, :rec, 1,
            null, null, null, :maxnumber, 0, null, 1,
            :x_partia, :x_serial_no, :x_slownik);
        quantityondoc = :quantityondoc - :toinsert;
      end
    end
    -- jeszcze cos zostalo ale nie mam (jeszcze) na lokacji inwentaryzacyjnej
    if (:quantityondoc > 0) then
    begin
      toinsert = :quantityondoc;
      lot = null;
      mwspallocp = null;
      select first 1 p.ref
        from mwspallocs p
        where p.mwsconstloc = :mwsconstlocp
        into :mwspallocp;
      if (:mwspallocp is null) then
      begin
        execute procedure gen_ref('MWSPALLOCS') returning_values mwspallocp;
        insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status,
            fromdocid, fromdocposid, fromdoctype)
          select :mwspallocp, symbol, ref, 2, 0,
              :docid, :docposid, :doctype
            from mwsconstlocs
            where ref = :mwsconstlocp;
      end

      maxnumber = :maxnumber  + 1;
      insert into mwsacts (mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
          mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
          regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, lot, recdoc, plus,
          whareal, whareap, wharealogl, number, disttogo, palgroup, autogen,
          x_partia, x_serial_no, x_slownik)
        values (:refmwsord, 1, :stocktaking, :good, :vers, :toinsert, :mwsconstlocp, :mwspallocp,
          null, null, :docid, :docposid, 'M', :opersettlmode, :autoloccreate, 'MWS', :whareag, :locsec,
          current_timestamp, null, null, null, :posflags, substr(:posdescription,1,250), :priority, :lot, :rec, 1,
          null, null, null, :maxnumber, 0, null, 1,
          :x_partia, :x_serial_no, :x_slownik);
      quantityondoc = :quantityondoc - :toinsert;

    end
  end
  update mwsords o set o.status = 1 where o.ref = :refmwsord and o.status = 0;
  update mwsacts a set a.status = 2, a.quantityc = a.quantity where a.mwsord = :refmwsord and a.status = 1;
  update mwsords o set o.status = 5 where o.ref = :refmwsord and o.status > 0;
end^
SET TERM ; ^
