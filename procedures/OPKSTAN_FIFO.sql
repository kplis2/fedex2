--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPKSTAN_FIFO(
      SLODEF integer,
      SLOPOZ integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      OPKTRYB smallint,
      DOROZPISANIA numeric(14,4),
      CENNET numeric(14,4),
      CENBRU numeric(14,4),
      MAG varchar(3) CHARACTER SET UTF8                           ,
      BN char(1) CHARACTER SET UTF8                           ,
      VAT numeric(14,4),
      STANOPKREF integer,
      WYDANIE smallint)
  returns (
      ILOSC numeric(14,4),
      CENASNETTO numeric(14,2),
      CENASBRUTTO numeric(14,2))
   as
declare variable iloscbezmag numeric(14,4);
declare variable cenamag numeric(14,2);
declare variable cenaspr numeric(14,2);
declare variable stanmag numeric(14,2);
begin
  ILOSC = :dorozpisania;
  CENASNETTO = :cennet;
  CENASBRUTTO = :cenbru;
  if(:opktryb < 2) then begin
    suspend;
    exit;
  end
  /*podrzucanie odpowiednich stanow do rozliczania*/
  /*najpierw ilosc zgodna z cenami magazynowymi**/
  iloscbezmag = 0;
  if(:stanopkref is null) then stanopkref= 0;
  while(:stanopkref >=0) do begin
    for select STAN,CENAMAG,CENASPR
    from STANYOPK where SLODEF = :SLODEF and SLOPOZ = :SLOPOZ and KTM = :ktm and WERSJA = :wersja and ZREAL = 0
    and ((:stanopkref = 0) or (:stanopkref = STANYOPK.REF))
    order by data
    into :ilosc,:cenamag,:cenaspr
    do begin
      if(:dorozpisania > 0) then begin
        if(:ilosc > :dorozpisania) then ilosc = :dorozpisania;
        stanmag = null;
        select sum(ILOSC) from STANYCEN where MAGAZYN = :MAG and KTM = :ktm and WERSJA = :wersja and CENA = :cenamag into :stanmag;
        if(:stanmag is null) then stanmag = 0;
        dorozpisania = :dorozpisania - :ilosc;
        if(:stanmag < :ilosc) then begin
          iloscbezmag = :iloscbezmag + (:ilosc - :stanmag);
          ilosc = :stanmag;
        end
        if(:ilosc > 0) then begin
          if(:bn = 'B') then begin
            cenasbrutto = :cenaspr;
            cenasnetto = :cenasbrutto/(1+:vat/100);
          end else begin
            cenasnetto = :cenaspr;
            cenasbrutto = :cenasnetto*(1+:vat/100);
          end
          suspend;
        end
        if(:iloscbezmag > 0) then begin
          ilosc = iloscbezmag;
          suspend;
          iloscbezmag = 0;
        end
      end
    end
    if(:stanopkref > 0) then stanopkref = 0;
    else stanopkref = -1;
  end
/*  if(:iloscbezmag > 0) then begin
    for select STAN,CENAMAG,CENASPR
    from STANYOPK where SLODEF = :SLODEF and SLOPOZ = :SLOPOZ and KTM = :ktm and WERSJA = :wersja and ZREAL = 0
    order by data
    into :ilosc,:cenamag,:cenaspr
    do begin
      if(:iloscbezmag > 0) then begin
        if(:ilosc > :iloscbezmag) then ilosc = :iloscbezmag;
        iloscbezmag = :iloscbezmag - :ilosc;
        if(:ilosc > 0) then begin
          if(:bn = 'B') then begin
            cenasbrutto = :cenaspr;
            cenasnetto = :cenasbrutto/(1+:vat/100);
          end else begin
            cenasnetto = :cenaspr;
            cenasbrutto = :cenasnetto*(1+:vat/100);
          end
          suspend;
        end
      end
    end
  end*/
  ilosc = :dorozpisania + :iloscbezmag;
  if(:ilosc > 0) then begin
--     exception OPKSTANY_BRAKSTANOWDOREAL;
  end
end^
SET TERM ; ^
