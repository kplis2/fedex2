--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPK_ADD(
      SLODEF integer,
      SLOPOZ integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      ILOSC numeric(14,4),
      CENAMAG numeric(14,2),
      CENAKAUCJA numeric(14,2),
      DOKPOZREF integer,
      BACK smallint)
   as
declare variable cnt integer;
declare variable opkref integer;
declare variable oldilosc numeric(14,4);
declare variable oldiloscroz numeric(14,4);
begin
  if(:cenakaucja is null) then cenakaucja = 0;
  if(:back = 0) then begin
    select max(ref) from STANYOPK where SLODEF = :slodef and SLOPOZ = :slopoz
            and POZDOKUM = :DOKPOZREF AND KTM = :ktm and WERSJA = :wersja and CENAMAG = :CENAMAG and CENASPR = :cenakaucja
        into :cnt;
    if(:cnt > 0) then
      update STANYOPK set ILOSC = ILOSC + :ilosc, WARTMAG = WARTMAG + (:cenamag * :ilosc), KAUCJA = KAUCJA + (:ilosc *:cenakaucja) where REF =:cnt;
    else
      insert into STANYOPK(SLODEF,SLOPOZ,POZDOKUM,KTM,WERSJA,ilosc,CENAMAG,WARTMAG,CENASPR,KAUCJA)
      values (:SLODEF,:SLOPOZ,:DOKPOZREF,:KTM,:WERSJA,:ILOSC,:CENAMAG,:CENAMAG*:ilosc,:CENAKAUCJA,:CENAKAUCJA*:ilosc);
  end else begin
    /*wycofanie zapisu*/
    select max(ref),max(ilosc) from STANYOPK where SLODEF = :slodef and SLOPOZ = :slopoz
            and POZDOKUM = :DOKPOZREF AND KTM = :ktm and WERSJA = :wersja and CENAMAG = :CENAMAG and CENASPR = :cenakaucja
        into :opkref,:oldilosc;
    if(:opkref is null or (:opkref = 0))then exception OPKSTAN_WRONGADDBACK;
    select sum(DOKUMROZ.ilosc) from DOKUMROZ where DOKUMROZ.opkrozid = :opkref into :oldiloscroz;
    if(:oldiloscroz > (:oldilosc - :ilosc))then exception STANYOPK_MAROLICZENIA;
    if(:ilosc = :oldilosc) then delete from STANYOPK where REF=:opkref;
    else
      update STANYOPK set ILOSC = ILOSC - :ilosc, WARTMAG = WARTMAG - (:cenamag * :ilosc), KAUCJA = KAUCJA - (:ilosc *:cenakaucja) where REF =:opkref;
  end
end^
SET TERM ; ^
