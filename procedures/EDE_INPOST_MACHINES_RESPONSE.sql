--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_INPOST_MACHINES_RESPONSE(
      EDEDOCSH_REF integer)
  returns (
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
   as
declare variable parent integer;
declare variable symbol string20;
declare variable typ string20;
declare variable kodp postcode;
declare variable woj string40;
declare variable cpwoj16mref integer;
declare variable ulica street_id;
declare variable nrdomu nrdomu_id;
declare variable nrlokalu nrdomu_id;
declare variable miasto city_id;
declare variable pobraniechar char(1);
declare variable pobranie smallint;
declare variable platnoscchar char(1);
declare variable platnosc smallint;
declare variable platnoscopis string255;
declare variable lokalizacja string255;
declare variable godziny string255;
declare variable intstatus smallint;
declare variable statusdesc string;
declare variable status smallint_id;
begin
  select otable, oref
    from ededocsh
    where ref = :ededocsh_ref
  into :otable, :oref;

  update x_sposdostpunktyodbioru o
    set o.status = 0
    where o.sposdost = :oref;

  --delete from x_sposdostpunktyodbioru o
    --where o.sposdost = :oref;

  for
    select p.id
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.name = 'machine'
    into :parent
  do begin
    select substring(trim(p.val) from 1 for 20)
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'name'
    into :symbol;

    select substring(trim(p.val) from 1 for 20)
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'type'
    into :typ;

    select substring(trim(p.val) from 1 for 255)
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'status'
    into :statusdesc;

    if(coalesce(statusdesc,'') = 'Operating')then
    begin
      status = 1;
    end
    else
    begin
      status = 0;
    end

    select substring(trim(p.val) from 1 for 10)
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'postcode'
    into :kodp;

    select lower(substring(trim(p.val) from 1 for 40))
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'province'
    into :woj;

    select c.ref
      from cpwoj16m c
      where lower(c.opis) = :woj
    into :cpwoj16mref;

    select substring(trim(p.val) from 1 for 10)
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'street'
    into :ulica;

    select substring(trim(p.val) from 1 for 10)
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'buildingnumber'
    into :nrdomu;

    select substring(trim(p.val) from 1 for 255)
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'town'
    into :miasto;

    select lower(trim(p.val))
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'paymentavailable'
    into :pobraniechar;

    if (:pobraniechar is null or :pobraniechar = 'f') then
    begin
      pobranie = 0;
      platnosc = 0;
    end
    else
    begin
      pobranie = 1;

      select trim(p.val)
        from ededocsp p
        where p.ededoch = :ededocsh_ref
          and p.parent = :parent
          and p.name = 'paymenttype'
      into :platnoscchar;
      if (:platnoscchar is null) then platnosc = 0;
      else
      begin
        execute procedure CHECK_STRING_TO_INTEGER (:platnoscchar) returning_values :intstatus;
        if (:intstatus is null) then intstatus = 0;
  
        if (:intstatus = 0) then platnosc = 0;
        else platnosc = cast(:platnoscchar as smallint);
      end
    end

    select substring(trim(p.val) from 1 for 255)
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'paymentpointdescr'
    into :platnoscopis;

    select substring(trim(p.val) from 1 for 255)
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'locationdescription'
    into :lokalizacja;

    select substring(trim(p.val) from 1 for 255)
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'operatinghours'
    into :godziny;

    update or insert into x_sposdostpunktyodbioru (sposdost, symbol, nazwa, typ, kodp, wojewodztwo,
        ulica, nrdomu, nrlokalu, miasto, pobranie, platnosc, platnoscopis, lokalizacja, godziny,
        status, statusdesc)
      values (:oref, :symbol, :symbol, :typ, :kodp, :cpwoj16mref,
        :ulica, :nrdomu, '', :miasto, :pobranie, :platnosc, :platnoscopis, :lokalizacja, :godziny,
        :status, :statusdesc)
      matching(sposdost, symbol);

    symbol = null;
    typ = null;
    kodp = null;
    cpwoj16mref = null;
    ulica = null;
    nrdomu = null;
    miasto = null;
    pobranie = null;
    platnosc = null;
    platnoscopis = null;
    lokalizacja = null;
    godziny = null;
    status = 0;
    statusdesc = null;
  end

  suspend;
end^
SET TERM ; ^
