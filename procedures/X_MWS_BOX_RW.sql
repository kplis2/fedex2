--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_BOX_RW(
      SPED_DOC INTEGER_ID)
  returns (
      DOCID INTEGER_ID,
      MSG STRING255)
   as
declare variable docpoz integer_id;
declare variable x_box_ktm ktm_id;
declare variable x_box_count integer_id;
declare variable ack smallint_id;
declare variable box_avaiable integer_id;
begin
  ack = 1;
  msg = '';
--XXX LDz automatyczne wystawianie RW na opakowania do dokumentu spedycyjnego
  if (sped_doc is not null and sped_doc <> 0) then begin
    execute procedure gen_ref('DOKUMNAG') returning_values docid;
    insert into DOKUMNAG(REF, MAGAZYN, TYP, DATA, DOSTAWA, DOSTAWCA, KLIENT, ZAMOWIENIE, AKCEPT, BLOKADA, MWSBLOCKMWSORDS, frommwsord, zrodlo, operator, operakcept)
      values (:docid, 'BOX', 'RW', current_date, null, null, null, null, 0, 0, 1, :sped_doc, 99, 74, 74);
    for select opk.x_box_ktm, count(*)
      from listywysdroz_opk opk
      where opk.listwysd = :SPED_DOC
        and opk.typ = 0
      group by opk.x_box_ktm
    into :x_box_ktm, :x_box_count
  do begin
      execute procedure gen_ref('DOKUMPOZ') returning_values docpoz;
      insert into DOKUMPOZ(REF, DOKUMENT, KTM, ILOSC, opk, WERSJA)
        values (:docpoz, :docid, :x_box_ktm, :x_box_count, 0, 0);
      
      select s.ilosc - s.zablokow - s.zablokow
        from stanyil s
        where s.ktm = :x_box_ktm
        and s.magazyn = 'BOX'
      into :box_avaiable;
      if (box_avaiable < x_box_count or box_avaiable is null ) then
        ack = 0;

  end
    if (ack = 1) then begin
      update dokumnag set AKCEPT=1 where ref=:docid;
      msg = ' . Dokument zaakceptowany.';
    end else
      msg = ' . Dokument RW nie zaakceptowany.';

    select 'Wystawiono dokument '||d.symbol||:msg
      from dokumnag d
      where d.ref = :docid
    into :msg;
  end
  suspend;
end^
SET TERM ; ^
