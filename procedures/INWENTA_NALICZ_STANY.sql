--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INWENTA_NALICZ_STANY(
      INWENTA integer,
      ODKTM varchar(40) CHARACTER SET UTF8                           ,
      DOKTM varchar(40) CHARACTER SET UTF8                           ,
      MASKAKTM varchar(255) CHARACTER SET UTF8                           ,
      DOSTAWCA integer,
      TYLKOAKTU smallint,
      ZEROWE smallint,
      TOWTYPE varchar(255) CHARACTER SET UTF8                           )
  returns (
      STATUS integer,
      NUMADD integer,
      NUMUPD integer)
   as
declare variable mag varchar(3);
declare variable data timestamp;
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable cena numeric(14,4);
declare variable dostawa integer;
declare variable stan numeric(14,4);
declare variable oldstan numeric(14,4);
declare variable cnt integer;
declare variable maogr smallint;
declare variable zpartiami smallint;
declare variable zbiorczy integer;
declare variable dalej integer;
declare variable oldupdated integer;
declare variable tmpzerowe smallint;
declare variable zamk smallint;
begin
  STATUS = -1;
  maogr = 0;
  select MAGAZYN,DATA, zpartiami, zbiorczy, zamk from INWENTA where REF=:INWENTA into :mag, :data, :zpartiami, :zbiorczy, :zamk;
  if(:zbiorczy = 1) then tylkoaktu = 1;
  if(:zpartiami is null) then zpartiami = 0;
  if(:data is null) then exception INWENTA_NO_DATA;
  if(:mag is null) then exception INWENTA_NO_MAGAZYN;
  if(:maskaktm = '') then maskaktm = null;
  NUMADD = 0;
  NUMUPD = 0;
  if(:odktm <> '' or (:doktm <> '') or (:maskaktm <> '') or (:dostawca>0)) then maogr = 1;
  update INWENTAP set blokadanalicz = 1, status = -1 where INWENTA=:inwenta;
  update INWENTAP set UPDATED=0, STATUS = -1 where INWENTA =:inwenta and ((UPDATED <> 0) or (UPDATED is null));
  if(:tylkoaktu=2) then begin
    update INWENTAP set CENAPLUS=0, blokadanalicz=0, status = -1 where INWENTA=:inwenta;
    update INWENTAP set blokadanalicz = 1, status = -1 where INWENTA=:inwenta;
  end
  if(:tylkoaktu<2 and :zpartiami = 0) then begin
    dostawa = null;
    cena = 0;
    for
      select rktm, rwersja, sum(stan)
        from MAG_STANY(:mag,NULL,NULL,NULL,NULL,:data,0,:zerowe)
        join towary on (towary.ktm = mag_stany.rktm)
        where ((:odktm is null) or (:odktm = '') or (:odktm <= RKTM))
          and ((:doktm is null) or (:doktm = '') or (:doktm >= RKTM))
          and ((:maskaktm is null) or (RKTM like :maskaktm))
          and (stan <> 0 or :zerowe = 1)
          and (coalesce(:towtype,'') = '' or position(';'||towary.usluga||';' in :towtype)>0)
      group by rktm,rwersja
      into :KTM, :wersja, :stan
    do begin
      oldstan = 0;
      cnt = null;
      dalej = 1;
      if(:dostawca > 0) then begin
        cnt = 0;
        if(exists (select first 1 1 from DOSTCEN where DOSTAWCA = :dostawca and KTM = :KTM)) then cnt = 1;
        if(:cnt = 0) then dalej = 0;
      end
      if(:dalej = 1) then begin
        cnt = 0;
        for select REF, ILSTAN, UPDATED from INWENTAP
          where INWENTA=:inwenta and KTM=:KTM and WERSJA=:wersja
          into :cnt, :oldstan, :oldupdated
        do begin /* jesli znaleziono juz jakies wpisy to za pierwszym razem nalicz a potem dodawaj */
          if(:oldstan is null) then oldstan = 0;
          if(:oldupdated = 0) then
            update INWENTAP set ILSTAN = :stan, UPDATED = 1, STATUS = -1 where REF=:cnt;
          else
            update INWENTAP set ILSTAN = ILSTAN + :stan, UPDATED = 1, STATUS = -1 where REF=:cnt;
        end
        if((:stan <> 0 or :zerowe = 1)  and :cnt = 0 and :tylkoaktu = 0 ) then begin /* jesli nie znaleziono wpisu to dodaj*/
          insert into INWENTAP(INWENTA,KTM,WERSJA,CENA,DOSTAWA,ILSTAN,ILINW,STATUS, BLOKADANALICZ)
            values(:inwenta, :ktm, :wersja, :cena, :dostawa, :stan, 0, 1,1);
          numadd = :numadd + 1;
        end
      end
    end
    if(:maogr = 0)then
      update INWENTAP set ILSTAN=0, STATUS=-1 where INWENTA =:inwenta and STATUS = 4;
    update inwentap set ilstan = 0, updated = 1, status = -1 where inwentap.inwenta = :inwenta and ilinw <> 0 and updated = 0;
    update inwentap set ilstan = 0 where updated = 0 and ilinw = 0 and inwenta=:inwenta;
    if (zamk = 0) then
      delete from inwentap where updated = 0 and ilinw = 0 and inwenta = :inwenta;
  end
  if(:tylkoaktu<2 and :zpartiami <> 0) then begin
    for
      select MAG_STANY.rktm, MAG_STANY.rwersja, MAG_STANY.rcena, MAG_STANY.rdostawa, MAG_STANY.stan
        from MAG_STANY(:mag,NULL,NULL,NULL,NULL,:data,0,:zerowe)
        join towary on (towary.ktm = mag_stany.rktm)
        where ((:odktm is null) or (:odktm = '') or (:odktm <= MAG_STANY.RKTM))
          and ((:doktm is null) or (:doktm = '') or (:doktm >= MAG_STANY.RKTM))
          and ((:maskaktm is null) or (MAG_STANY.RKTM like :maskaktm))
          and (MAG_STANY.stan <> 0 or :zerowe = 1)
          and (coalesce(:towtype,'') = '' or position(';'||towary.usluga||';' in :towtype)>0)
      into :KTM, :wersja, :cena, :dostawa, :stan
    do begin
      oldstan = 0;
      cnt = null;
      dalej = 1;
      if(:dostawca > 0) then begin
        cnt = 0;
        if(exists (select first 1 1 from DOSTCEN where DOSTAWCA = :dostawca and KTM = :KTM)) then cnt = 1;
        if(:cnt = 0) then dalej = 0;
      end
      if(:dalej = 1) then begin
        cnt = 0;
        for select REF, ILSTAN, UPDATED from INWENTAP
          where INWENTA=:inwenta and KTM=:KTM and WERSJA=:wersja
            and  (cena = :cena or (:zpartiami = 0))
            and  ( dostawa  = :dostawa or (:dostawa is null))
         into :cnt, :oldstan, :oldupdated
        do begin /* jesli znaleziono juz jakies wpisy to za pierwszym razem nalicz a potem dodawaj */
          if(:oldstan is null) then oldstan = 0;
          if(:oldupdated = 0) then
            update INWENTAP set ILSTAN = :stan, UPDATED = 1, STATUS = -1 where REF=:cnt;
          else
            update INWENTAP set ILSTAN = ILSTAN + :stan, UPDATED = 1, STATUS = -1 where REF=:cnt;
        end
        if((:stan <> 0 or :zerowe = 1)  and :cnt = 0 and :tylkoaktu = 0 ) then begin /* jesli nie znaleziono wpisu to dodaj*/
          insert into INWENTAP(INWENTA,KTM,WERSJA,CENA,DOSTAWA,ILSTAN,ILINW,STATUS, BLOKADANALICZ)
            values(:inwenta, :ktm, :wersja, :cena, :dostawa, :stan, 0, 1,1);
          numadd = :numadd + 1;
        end
      end
    end
    if(:maogr = 0)then
      update INWENTAP set ILSTAN=0, STATUS=-1 where INWENTA =:inwenta and STATUS = 4;
    update inwentap set ilstan = 0, updated = 1, status = -1 where inwentap.inwenta = :inwenta and ilinw <> 0 and updated = 0;
    update inwentap set ilstan = 0 where updated = 0 and ilinw = 0 and inwenta=:inwenta;
    if (zamk = 0) then
      delete from inwentap where updated = 0 and ilinw = 0 and inwenta = :inwenta;
  end
  update INWENTAP set BLOKADANALICZ = 0, STATUS = -1 where INWENTA = :inwenta;
  execute procedure INWENTA_OBLNAG(:inwenta);
  select count(*) from INWENTAP where INWENTA = :inwenta and UPDATED = 1 into :numupd;
  if(:numupd is null) then numupd = 0;
  STATUS = 1;
end^
SET TERM ; ^
