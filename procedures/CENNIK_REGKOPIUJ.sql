--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CENNIK_REGKOPIUJ(
      ZCENREF integer,
      REGREF integer,
      NACENREF integer,
      NADP varchar(1) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE CENREF INTEGER;
DECLARE VARIABLE NAREGREF INTEGER;
DECLARE VARIABLE IL INTEGER;
DECLARE VARIABLE NAZWAREG VARCHAR(60);
DECLARE VARIABLE ODKTM VARCHAR(40);
DECLARE VARIABLE DOKTM VARCHAR(40);
DECLARE VARIABLE KTMMASKA VARCHAR(40);
DECLARE VARIABLE GRUPA VARCHAR(60);
DECLARE VARIABLE TRYB SMALLINT;
DECLARE VARIABLE ZRODLO SMALLINT;
DECLARE VARIABLE AUTONALICZ SMALLINT;
DECLARE VARIABLE WARTOSC NUMERIC(14,2);
DECLARE VARIABLE ORD SMALLINT;
begin
  if(:nacenref!=0) then
      for select d.grupa,d.nazwa, d.odktm, d.doktm, d.ktmmaska, d.tryb, d.zrodlo, d.autonalicz, d.wartosc, d.ord
       from DEFCENREG d where d.defcennik = :zcenref and (:regref=0 or (d.ref = :regref))
       into :grupa ,:nazwareg, :odktm, :doktm, :ktmmaska, :tryb, :zrodlo, :autonalicz, :wartosc, :ord
       do begin
        naregref = 0;
        select d.ref from defcenreg d where d.nazwa =:nazwareg and d.defcennik = :NACENREF into :naregref;
        if(naregref>0) then begin
         if(nadp='1') then
           update defcenreg d set d.odktm=:odktm, d.doktm=:doktm, d.grupa = :grupa, d.ktmmaska=:ktmmaska, d.tryb=:TRYB,
            d.zrodlo=:zrodlo, d.autonalicz=:AUTONALICZ, d.wartosc=:wartosc, d.ord=:ord
            where d.ref=:naregref;
        end else
          insert into DEFCENREG(DEFCENNIK,NAZWA,ODKTM,DOKTM,GRUPA,KTMMASKA,TRYB,ZRODLO,AUTONALICZ,WARTOSC,ORD)
          values (:nacenref,:nazwareg,:odktm,:doktm,:grupa,:ktmmaska,:tryb,:zrodlo,:autonalicz,:wartosc,:ord) ;
       end
  else begin
    select d.nazwa,d.odktm, d.doktm, d.grupa, d.ktmmaska, d.tryb, d.zrodlo, d.autonalicz, d.wartosc, d.ord
     from defcenreg d where d.ref =:regref
     into :nazwareg, :odktm, :doktm,  :grupa, :ktmmaska, :tryb, :zrodlo, :autonalicz, :wartosc, :ord;
    for select d.ref from defcennik d where d.ref<>:zcenref order by d.ref into :nacenref do begin
      naregref=0;
      select d.ref from defcenreg d where d.nazwa =:nazwareg and d.defcennik = :nacenref into :naregref;
      if(naregref>0) then begin
        if(nadp='1') then
          update defcenreg d set d.odktm=:odktm, d.doktm=:doktm, d.grupa  = :grupa , d.ktmmaska=:ktmmaska, d.tryb=:TRYB,
           d.zrodlo=:zrodlo, d.autonalicz=:AUTONALICZ, d.wartosc=:wartosc, d.ord=:ord
           where d.ref=:naregref;
      end else
        insert into DEFCENREG(DEFCENNIK,NAZWA,ODKTM,DOKTM,GRUPA,KTMMASKA,TRYB,ZRODLO,AUTONALICZ,WARTOSC,ORD)
        values (:nacenref,:nazwareg,:odktm,:doktm,:grupa,:ktmmaska,:tryb,:zrodlo,:autonalicz,:wartosc,:ord);
    end
  end
end^
SET TERM ; ^
