--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_DOSTZAPOTRZ(
      DOSTAWCA integer,
      MAGAZYNY varchar(255) CHARACTER SET UTF8                           ,
      MASKAKTM varchar(40) CHARACTER SET UTF8                           ,
      DATAOD date,
      DATADO date,
      DNIZAPASU integer,
      ZEWN smallint,
      TYPMIARY smallint,
      ZAMOWIENIE integer)
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      WERSJAREF integer,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      WNAZWA varchar(255) CHARACTER SET UTF8                           ,
      RMIARA varchar(20) CHARACTER SET UTF8                           ,
      GRUPA varchar(255) CHARACTER SET UTF8                           ,
      TYPTOWARU varchar(255) CHARACTER SET UTF8                           ,
      RDOSTAWCA varchar(255) CHARACTER SET UTF8                           ,
      SZUZYCIE numeric(14,4),
      SZUZYCIED numeric(14,4),
      STAN numeric(14,4),
      ZABLOKOW numeric(14,4),
      ZAREZERW numeric(14,4),
      ZAMOWIONO numeric(14,4),
      STANMIN numeric(14,4),
      STANMAX numeric(14,4),
      PROGNOZASPRZ numeric(14,4),
      ZAPOTRZ numeric(14,4),
      ZAMOWIC numeric(14,4),
      ILOSCNAZAM numeric(14,4),
      NAGZAMREF integer,
      POZZAMREF integer,
      STATUS smallint)
   as
declare variable dostawcat integer;
declare variable ok smallint;
declare variable iloscdni integer;
declare variable ilmin numeric(14,4);
declare variable pakmin numeric(14,4);
declare variable przelicz numeric(14,4);
declare variable waga numeric(15,4);
declare variable vol numeric(15,4);
begin
  pozzamref = NULL;
  nagzamref = NULL;
  iloscnazam = NULL;
  if(:magazyny<>'') then magazyny = ';'||:magazyny||';';
  if(:maskaktm is null or :maskaktm='') then maskaktm = '%';
  if(:zamowienie is null) then zamowienie = 0;
  if(:dostawca is null) then dostawca = 0;
  if(:datado is null) then datado = current_date;
  if(:dataod is null) then dataod = '2008-01-01';
  if(:datado<:dataod) then datado = :dataod;
  iloscdni = :datado - :dataod + 1;

  for select s.ktm, s.wersja, s.wersjaref, s.nazwat, s.nazwaw, s.miara, t.grupa, towtypes.nazwa,
  s.ilosc, s.zablokow, s.zarezerw, s.zamowiono, s.stanmin, s.stanmax,
  x.ilosc
  from get_stanyil(:magazyny,:maskaktm) s
  join get_wydhist_ktm(:magazyny,:maskaktm,:dataod,:datado,:zewn) x on (s.ktm=x.ktm and s.wersja=x.wersja and s.wersjaref=x.wersjaref)
  left join towary t on (t.ktm=s.ktm)
  left join towtypes on (towtypes.numer=t.usluga)
  order by s.ktm
  into :ktm, :wersja, :wersjaref, :nazwa, :wnazwa, :rmiara, :grupa, :typtowaru,
  :stan, :zablokow, :zarezerw, :zamowiono, :stanmin, :stanmax,
  :szuzycie
  do begin
    status = 0;
    ok = 1;
    -- okresl dostawce i parametry logistyczne
    ilmin = 0;
    pakmin = 0;
    dostawcat = NULL;
    if(:dostawca<>0) then begin --tylko towary dostawcy
      dostawcat = :dostawca;
      if (exists(select first 1 1 from dostcen d where d.ktm=:ktm and d.dostawca=:dostawcat)) then begin
        select first 1 d.iloscmin, d.paczkamin from dostcen d where d.ktm=:ktm and d.dostawca=:dostawcat into :ilmin, :pakmin;
      end else begin
        ok = 0;
      end
    end else begin
      select first 1 d.iloscmin, d.paczkamin, d.dostawca from dostcen d where d.ktm=:ktm and d.gl=1 into :ilmin, :pakmin, :dostawcat;
    end
    if(:ilmin is null) then ilmin = 0;
    if(:pakmin is null) then pakmin = 0;
    if(:dostawcat is null) then rdostawca = '';
    else select id from dostawcy where ref=:dostawcat into :rdostawca;

    if(:ok=1) then begin
      -- oblicz ilosc sugerowana
      szuzycied = :szuzycie / :iloscdni;
      prognozasprz = :szuzycied * :dnizapasu;
      zapotrz = :prognozasprz - :stan + :zarezerw + :zablokow - :zamowiono;
      -- zaokraglenia dostawcy
      if(:zapotrz>0 and (:ilmin<>0 or :pakmin<>0)) then
        execute procedure DIGITIZE(:zapotrz,:pakmin,:ilmin) returning_values :zamowic;
      else
        zamowic = cast (:zapotrz as integer);
      if(:zamowic<0) then zamowic = 0;
      if(:zamowic>0) then status = 1;
      -- wskazanie ilosci na zamowieniu redagowanym
      if(:zamowienie<>0 and :wersjaref is not null) then begin
        iloscnazam = NULL;
        pozzamref = NULL;
        nagzamref = NULL;
        select min(POZZAM.REF), min(POZZAM.ZAMOWIENIE), sum(POZZAM.ILOSC) from POZZAM
        where (POZZAM.ZAMOWIENIE=:zamowienie) and (POZZAM.WERSJAREF=:wersjaref)
        into :pozzamref, :nagzamref, :iloscnazam;
      end
      -- przelicz ilosci na jednostke wymagana
      przelicz = NULL;
      if(:typmiary=1) then begin -- przelicz na wage
        select waga from towjedn where ktm=:ktm and glowna=1 into :przelicz;
        rmiara = 'kg';
      end else if (:typmiary=2) then begin -- przelicz na objetosc
        select vol from towjedn where ktm=:ktm and glowna=1 into :przelicz;
        rmiara = 'dm3';
      end else if (:typmiary=3) then begin -- przelicz na wartosc magazynowa
        select CENA_ZAKN from towary where ktm=:ktm into :przelicz;
        rmiara = 'PLN';
      end
      if(:przelicz is not null) then begin
          SZUZYCIE = :szuzycie * :przelicz;
          SZUZYCIED = :szuzycied * :przelicz;
          STAN = :stan * :przelicz;
          ZABLOKOW = :zablokow * :przelicz;
          ZAREZERW = :zarezerw * :przelicz;
          ZAMOWIONO = :zamowiono * :przelicz;
          STANMIN = :stanmin * :przelicz;
          STANMAX = :stanmax * :przelicz;
          PROGNOZASPRZ = :prognozasprz * :przelicz;
          ZAPOTRZ = :zapotrz * :przelicz;
          ZAMOWIC = :zamowic * :przelicz;
          ILOSCNAZAM = :iloscnazam * :przelicz;
      end
      suspend;
    end
  end
end^
SET TERM ; ^
