--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NOTIFY_WINDYKACJA_MAIL(
      MINDNIPRZETERM integer,
      MAXDNIPRZETERM integer,
      KONTOFK KONTO_ID = '')
  returns (
      SLODEF integer,
      SLOPOZ integer,
      KONTRAHENT varchar(255) CHARACTER SET UTF8                           ,
      EMAIL varchar(255) CHARACTER SET UTF8                           ,
      SYMBOL varchar(40) CHARACTER SET UTF8                           ,
      ROZRACHUNEK varchar(255) CHARACTER SET UTF8                           ,
      TABELA varchar(4096) CHARACTER SET UTF8                           )
   as
declare variable dni integer;
declare variable planpayday date;
declare variable dataplat date;
declare variable saldowm numeric(14,2);
declare variable opis varchar(255);
declare variable eol varchar(10);
declare variable company integer;
declare variable rozrachref integer;
declare variable ref integer;
declare variable uwagi varchar(255);
begin
  -- rozrachunki nierozliczone do maila
  -- KONTOFK - prefiks konta
eol='
';
  -- petla po zakladach
  for select ref
  from companies
  into :company
  do begin
    -- petla po rozrachunkach zgrupowanych wg klientow
    for select slodef, slopoz, max(slonazwa), min(REF)
    from ROZRACH
    where SALDOWM>0 and DNI>=:mindniprzeterm and DNI<=:maxdniprzeterm and TYP='N' and PLANPAYDAY<current_date
    and company = :company and slodef>0 and slopoz>0
    and (KONTOFK like :kontofk||'%' or :kontofk='')
    group by slodef,slopoz,company
    into :slodef, :slopoz, :kontrahent, :rozrachref
    do begin
      rozrachunek = '';
      -- unikalny symbol dla maila
      symbol = 'ROZRACH'||:mindniprzeterm||'X'||:rozrachref;
      -- szukanie adresu e-mail
      email = '';
      select email from NOTIFY_GET_EMAILWINDYK(:slodef,:slopoz) into :email;
      -- tresc
      tabela = 'Poniżej zamieszczamy zestawienie wszystkich należności na dzień <b> ' || current_date || '</b>
  <br>
  <br>
  <table cellpadding="4">
  <tr bgcolor="#C0C0C0">
  <th>Rozrachunek</th>
  <th>Saldo należności</th>
  <th>Termin płatności</th>
  <th>Uwagi</th>
  </tr>';
      -- zwroc w tabeli wszystkie nierozliczone rozrachunki
      for select symbfak, dni, dataplat, planpayday, saldowm, ref
      from ROZRACH
      where SALDOWM>0 and TYP='N'
      and company = :company and slodef=:slodef and slopoz=:slopoz
      and (KONTOFK like :kontofk||'%' or :kontofk='')
      order by dataplat
      into :opis, :dni, :dataplat, :planpayday, :saldowm, :ref
      do begin
        if(:ref=:rozrachref) then rozrachunek = :opis;
        if(:dni<0) then uwagi = 'w terminie płatności';
        else if(:dni=0) then uwagi = 'termin upływa w dniu dzisiejszym';
        else uwagi = 'przeterminowane '||:dni||' dni';
        tabela = tabela||eol|| '<tr><td>' || coalesce(:opis, '') || '</td>'||
        '<td><div align="right">' || :saldowm || '</div></td>'||
        '<td>' || :dataplat || '</td>'||
        '<td>' || :uwagi ||'</td>'||
        '</tr>'  ;
      end
      tabela = tabela || '</table>';
      suspend;
    end
  end
end^
SET TERM ; ^
