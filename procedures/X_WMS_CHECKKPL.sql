--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_WMS_CHECKKPL(
      VERS INTEGER_ID,
      PARTS_CNT SMALLINT_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING80,
      VERS_OUT INTEGER_ID,
      KTM_OUT KTM_ID)
   as
declare variable KPLNAG KPLNAG_ID;
declare variable I SMALLINT_ID;
declare variable KTM KTM_ID;
declare variable NRWERSJI INTEGER_ID;
begin

   --procedura zwraca wersje czesci skladowych kompletu ktory ma PARTS_CNT czesci

   select ktm from wersje where ref=:vers into :ktm;

   if (exists(select first 1 1
                from towary t
                  where t.usluga<>2
                    and t.ktm = :ktm)) then
   begin
     update towary t
       set t.usluga = 2, t.altposmode = 1
       where t.usluga<>2
         and t.ktm = :ktm;
   end

   select k.ref, count(p.ref)
     from KPLNAG k
       left join kplpoz p on (p.nagkpl=k.ref)
     where k.ktm = :ktm
     group by k.ref 
     having count(p.ref)=:parts_cnt
   into :kplnag, :i;

   if (kplnag is not null) then
   begin
     for select p.wersjaref, p.ktm
           from kplpoz p
            where p.nagkpl=:kplnag
     into :vers_out, :ktm_out
     do begin

       STATUS = 0;
       MSG = 'OK';
       suspend;

     end
   end
   else
   begin

     insert into WERSJE(KTM,NAZWA,OPIS,AKT,NAZWAT,
          CENA_ZAKN,CENA_ZAKB,CENA_FABN, CENA_FABB, CENA_KOSZTN, CENA_KOSZTB, CENA_ZAKNL,
          DNIWAZN,SYMBOL_DOST,KODKRESK,MARZAMIN,USLUGA,BKSYMBOL,KODPROD)
        select t.KTM,:parts_cnt||' - czesciowa',:parts_cnt||' - czesciowa',1,t.nazwa,
          t.cena_zakn,t.cena_zakb,t.cena_fabn, t.cena_fabb, t.cena_kosztn, t.cena_kosztb, t.CENA_ZAKNL,
          t.dniwazn, t.symbol_dost,t.kodkresk||'_'||:parts_cnt,t.marzamin,t.usluga,t.bksymbol, t.kodprod
        from towary t where t.ktm=:ktm
     returning  ref, nrwersji, KTM into :vers_out, :nrwersji, :ktm_out;




     insert into KPLNAG (NAZWA, KTM, WERSJAREF, Wersja)
       values (:PARTS_CNT||' paletowa', :KTM, :vers_out, :nrwersji)
     returning ref into :kplnag;

     i=1;

     while(:i <= :parts_cnt)
     do begin



        insert into towary(ktm, kodzewn, pkwiu, nazwa, miara, vat, akt, opispod, opisroz,
                           kodkresk, usluga, symbol_dost)
           select ktm||'_'||:i||'z'||:parts_cnt, kodzewn||'_'||:i||'z'||:parts_cnt, pkwiu, trim(nazwa)||'_'||:i||'z'||:parts_cnt, miara, vat, akt, trim(opispod)||'_'||:i||'z'||:parts_cnt, trim(opisroz)||'_'||:i||'z'||:parts_cnt,
                           kodkresk||'_'||:i||'z'||:parts_cnt, usluga, symbol_dost
           from TOWARY
           where ktm=:ktm;

        select ref from wersje where ktm=:ktm||'_'||:i||'z'||:parts_cnt into :vers_out;

        insert into KPLPOZ (NAGKPL, KTM, WERSJAREF, Wersja, ILOSC)
          values (:kplnag,  :KTM, :vers_out,0, 1);

        STATUS = 0;
        MSG = 'OK';
        suspend;

       i = i + 1;
     end


   end
end^
SET TERM ; ^
