--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_UPDATE_XXX_DESCRIPTION(
      SYMBOLIN varchar(1024) CHARACTER SET UTF8                           )
   as
  declare variable symbol varchar(30);
begin
  for
    select p.stringout
      from parsestring(:symbolin,';') p
    into :symbol
  do begin
    -- procedury
    update rdb$procedures
      set rdb$description = 'XXX;' || coalesce(rdb$description,'')
        where rdb$procedure_name not like 'XK_%'
          and rdb$procedure_name not like 'XX_%'
          and rdb$procedure_name not like 'X_%'
          and (rdb$description is null  or upper(rdb$description) not like '%XXX%')
          and rdb$procedure_source like '%'|| :symbol || '%';
      
    -- triggery
    update rdb$triggers
      set rdb$description = 'XXX;' || coalesce(rdb$description,'')
        where rdb$trigger_name not like 'XK_%'
          and rdb$trigger_name not like 'X_%'
          and (rdb$description is null  or upper(rdb$description) not like '%XXX%')
          and rdb$trigger_source like '%'|| :symbol || '%';
  end
end^
SET TERM ; ^
