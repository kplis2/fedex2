--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXP2PLATNIK_ZWUA(
      EMPLCONTR_REF integer,
      DATETYPE smallint,
      T_FROMDATE date,
      T_TODATE date,
      COMPANY integer,
      T_OUTDATE date = null)
  returns (
      PERSON integer,
      PESEL varchar(11) CHARACTER SET UTF8                           ,
      NIP varchar(15) CHARACTER SET UTF8                           ,
      FNAME varchar(60) CHARACTER SET UTF8                           ,
      SNAME varchar(30) CHARACTER SET UTF8                           ,
      MIDDLENAME varchar(30) CHARACTER SET UTF8                           ,
      MNAME varchar(30) CHARACTER SET UTF8                           ,
      BIRTHDATE date,
      EMAIL varchar(255) CHARACTER SET UTF8                           ,
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      DOCNO varchar(11) CHARACTER SET UTF8                           ,
      OUTDATE date,
      CODE varchar(20) CHARACTER SET UTF8                           ,
      REASONCODE varchar(3) CHARACTER SET UTF8                           ,
      ICODE varchar(6) CHARACTER SET UTF8                           ,
      EMPLOYEE integer,
      ZUSDATA integer,
      EMPLCONTRACT integer)
   as
declare variable ZUS_FROMDATE date;
declare variable ASCODE varchar(20);
declare variable INVALIDITY varchar(20);
declare variable RETIRED varchar(20);
begin
--MWr: Personel - Eksport do Platnika ZUS ZWUA -Wyrejestrowanie z ubezpieczen (ver.xml)

  /*DATETYPE:  = 0 - zakres dat dotyczy zakonczonych umów oraz zmiany kodu ubezp.
               = 1 - zakres dat okresla, ze analizowane beda umowy zakonczone w zadanym okresie
               = 2 - zakres dat okresla okres, w ktorym nastapila zmiana kodu ubezp.
    T_OUTDATE  = data wyrejestrowania zadana przez operatora przy eksporcie danej umowy
    T_FROMDATE = data 'Od' zakresu dat danego typu, istotne dla EMPLCONTR_REF <= 0
    T_TODATE   = data 'Do' zakresu dat danego typu, istotne dla EMPLCONTR_REF <= 0
 EMPLCONTR_REF < 0 - zapytanie o domyslne parametry eksportu dla wybranej umowy */

  datetype = coalesce(datetype,0);
  emplcontr_ref = coalesce(emplcontr_ref,0);

  if (company > 0 and emplcontr_ref >=0) then
  begin
    for
      select distinct p.ref,
          case when (ez.ispesel = 1) then p.pesel else '' end as pesel,
          case when (ez.isnip = 1) then p.nip else '' end as nip,
          upper(p.fname), upper(p.sname), upper(p.middlename), upper(p.mname), p.birthdate, upper(p.email),
          case when (ez.isother = 1 and p.evidenceno > '') then '1' when (ez.isother = 2 and p.passportno > '') then '2' else '' end as doctype,
          case when (ez.isother = 1 and p.evidenceno > '') then p.evidenceno when (ez.isother = 2 and p.passportno > '') then p.passportno else '' end as docno,
          k.outdate, z.code, case when c.empltype = 1 then z1.code else z4.code end as ascode, z2.code, z3.code,
          case when r.reasontype = 5 then '500'     --zgon
            else case when :datetype = 2 then '600' --inna przyczyna wyrejestrowania
            else '100' end                          --ustanie stosunku pracy
          end
       from exp2platnik_zwua(:emplcontr_ref, :datetype, :t_fromdate, :t_todate, -:company, :t_outdate) k
         join employees e on (k.employee = e.ref)
         join persons p on (p.ref = e.person)
         join emplcontracts c on (c.ref = k.emplcontract)
         left join ezusdata ez on (ez.ref = k.zusdata)
         left join edictzuscodes z on (p.education = z.ref)
         left join edictzuscodes z1 on (z1.ref = ez.ascode)
         left join edictzuscodes z2 on (z2.ref = ez.invalidity)
         left join edictzuscodes z3 on (z3.ref = ez.retired)
         left join edictzuscodes z4 on (z4.ref = c.ascode)
         left join eterminations r on (r.ref = c.etermination)
       order by p.person
       into :person, :pesel, :nip, :fname, :sname, :middlename, :mname, :birthdate, :email,
            :doctype, :docno, :outdate, :code, :ascode, :invalidity, :retired, :reasoncode
    do begin
      --Kod ubezpieczenia dla deklaracji xml:
      icode = coalesce(ascode,'0000') || coalesce(retired,'0') || coalesce(invalidity,'0');
      suspend;
    end
  end else
--==============================================================================
  if (emplcontr_ref < 0) then --PR52884 (domyslne parametry eksportu dla danej umowy)
  begin
    emplcontr_ref = -emplcontr_ref;

    select first 1 r.reasontype, c.enddate, z.fromdate
      from employees e
        join emplcontracts c on (e.ref = c.employee)
        left join ezusdata z on (z.person = e.person)
        left join eterminations r on (r.ref = c.etermination)
      where c.ref = :emplcontr_ref
      order by z.fromdate desc
      into :reasoncode, :outdate, :zus_fromdate;

    --jesli w bliskim okresie nastapila zmiana danych do ZUS
    if (current_date >= zus_fromdate - 3 and current_date <= zus_fromdate + 6) then
    begin
      outdate = zus_fromdate;
      reasoncode = 600;  --inna przyczyna wyrejestrowania
    end else
    begin
      --brak daty konca umowy
      if (outdate is null) then
      begin
        outdate = current_date;
        reasoncode = 600;  --inna przyczyna wyrejestrowania
      end else
      begin
        outdate = outdate + 1;  --wyrejestrowanie podpowiadane jako data konca umowy + 1 dzien
        if (reasoncode = 5) then reasoncode = 500;  --smierc
        else reasoncode = 100;  --ustanie tytulu do ubezpieczen
      end
    end
    suspend;

  end else
--==============================================================================
  if (company < 0) then  --BS61979 (obsluga poszczegolnych przypadkow)
  begin
    company = -company;

    if (emplcontr_ref > 0) then  --obsluga wybranej umowy ----------------------
    begin
      select first 1 e.ref, c.ref, person,
          case when :t_outdate is not null then :t_outdate else cast(c.enddate + 1 as date) end
        from employees e
          join emplcontracts c on (e.ref = c.employee)
        where c.ref = :emplcontr_ref
          and (c.empltype = 1 or trim (';' from coalesce(c.iflags,'')) <> '')
        into :employee, :emplcontract, :person, :outdate;

      if (employee is not null) then
      begin
        select first 1 ref from ezusdata
          where person = :person
            and ((:t_outdate is not null and fromdate + 1 <= :t_outdate)
              or (:t_outdate is null and (fromdate <= :outdate or :outdate is null)))
          order by fromdate desc
          into :zusdata;

        suspend;
      end
    end else
    begin
      if (datetype in (0,1)) then  --zmiana danych ubezpieczeniowych -----------
      begin
        for
          select e.ref, z.todate + 1, z.ref, c.ref
            from employees e
              join emplcontracts c on (e.ref = c.employee)
              join ezusdata z on (z.person = e.person and z.fromdate <= :t_todate and z.todate >= :t_fromdate)
            where e.company = :company
               and (c.empltype = 1 or trim (';' from coalesce(c.iflags,'')) <> '')
               and (:t_todate >= c.fromdate and (:t_fromdate <= coalesce(c.enddate,c.todate) or coalesce(c.enddate,c.todate) is null))
               and exists (select first 1 1
                             from ezusdata z2
                             where z2.person = e.person
                               and z2.fromdate <= :t_todate
                               and (z2.todate + 1 >= :t_fromdate or z2.todate is null)
                               and z2.ref <> z.ref
                               and z.todate + 1 = z2.fromdate
                               and (z2.ascode <> z.ascode or z2.retired <> z.retired or z2.invalidity <> z.invalidity)
                               and z2.fromdate > z.fromdate)
            into :employee, :outdate, :zusdata, :emplcontract
        do
          suspend;
      end

      if (datetype in (0,2)) then  --umowy zakonczone w zadanym okresie --------
      begin
        for
          select e.ref, c.enddate + 1, z.ref, c.ref
            from employees e
              join emplcontracts c on (e.ref = c.employee)
              left join ezusdata z on (z.person = e.person
                and z.fromdate <= :t_todate
                and z.fromdate = (select max(z1.fromdate) from ezusdata z1
                                    where z1.person = e.person and z1.fromdate <= c.enddate))
              where e.company = :company
                and (c.empltype = 1 or trim (';' from coalesce(c.iflags,'')) <> '')
                and c.enddate <= :t_todate and c.enddate >= :t_fromdate
                and not exists(select first 1 1 from emplcontracts c2 --dane z ostatniej umowy
                                 where e.ref = c2.employee and c2.enddate > c.enddate and c2.empltype = c.empltype)
               into :employee, :outdate, :zusdata, :emplcontract
        do
          suspend;
      end
    end
  end
end^
SET TERM ; ^
