--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_STANYARCH_DATA(
      MAGAZYNY varchar(255) CHARACTER SET UTF8                           ,
      MASKAKTM varchar(40) CHARACTER SET UTF8                           ,
      DATAOD date,
      DATADO date)
  returns (
      DATA date,
      STAN numeric(14,4),
      WARTOSC numeric(14,2))
   as
declare variable newdata date;
declare variable prevdata date;
declare variable ilbefore numeric(14,4);
declare variable wartbefore numeric(14,2);
declare variable il numeric(14,4);
declare variable wart numeric(14,2);
declare variable ilafter numeric(14,4);
declare variable wartafter numeric(14,2);
begin
  prevdata = :dataod - 1;
  -- pobierz szybko stan na dzień poczatkowy minus 1
  execute procedure MAG_STANQ(:MAGAZYNY,:MASKAKTM,NULL,:prevdata,NULL) returning_values :ilafter, :wartafter;
  if(:ilafter is null) then ilafter = 0;
  if(:wartafter is null) then wartafter = 0;
  ilbefore = ilafter;
  wartbefore = wartafter;
  -- pokaz kolejne obroty w zadanym przedziale dat
  for select
        p.ilosc*(1-2*p.wydania),    -- przyjecia na plus, wydania na minus
        p.wartosc*(1-2*p.wydania),
        n.DATA
  from DOKUMPOZ p
  left join DOKUMNAG n on (p.DOKUMENT = n.ref)
  where p.data>=:dataod and p.data<=:datado
    and (p.ktm like :maskaktm or :maskaktm='')
    and (:magazyny like '%;'||trim(n.magazyn)||';%')
    and n.akcept in (1,8)
  order by p.DATA
  into
    :il,
    :wart,
    :newdata
  do begin
    ilbefore = :ilafter;
    wartbefore = :wartafter;
    ilafter = :ilafter + :il;
    wartafter = :wartafter + :wart;
    while(:prevdata<:newdata-1) do begin -- zwroc informacje dla dat od ostatniej zwroconej do teraz-1
      prevdata = :prevdata + 1;
      data = :prevdata;
      stan = :ilbefore;
      wartosc = :wartbefore;
      suspend;
    end
  end
  -- zwroc informacje dla dat pozniejszych
  while(prevdata<:datado) do begin
    prevdata = :prevdata + 1;
    data = :prevdata;
    stan = :ilafter;
    wartosc = :wartafter;
    suspend;
  end
end^
SET TERM ; ^
