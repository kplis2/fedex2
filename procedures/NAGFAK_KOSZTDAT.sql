--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_KOSZTDAT(
      NAGFAKREF integer)
  returns (
      KOSZT numeric(14,2))
   as
DECLARE VARIABLE MIES INTEGER;
DECLARE VARIABLE ROK INTEGER;
DECLARE VARIABLE DATATMP VARCHAR(10);
DECLARE VARIABLE DATE_END TIMESTAMP;
DECLARE VARIABLE DATE_ROZLICZ TIMESTAMP;
DECLARE VARIABLE KOSZT_KATAL NUMERIC(14,2);
DECLARE VARIABLE KOSZT_PIERWOTNY NUMERIC(14,2);
DECLARE VARIABLE ZMIANA_KOSZTOW NUMERIC(14,2);
DECLARE VARIABLE KORYG SMALLINT;
begin
    select extract(month from nagfak.data),  extract(year from nagfak.data),
        magrozliczdata, kosztkat, pkoszt
       from nagfak
       where nagfak.ref = :nagfakref
       into :mies, :rok, :DATE_ROZLICZ,  :koszt_katal, :koszt_pierwotny;
    if(:mies = 12) then begin
      mies = 1;
      rok = :rok + 1;
    end
    else
      mies = mies + 1;

    datatmp = :mies;
    if(:mies < 10) then
      datatmp = '0'||:mies;
    datatmp = :rok||'-'||:datatmp||'-01';
    date_end = cast(:datatmp as timestamp);
    date_end = :date_end - 1;

    select
        sum(dokumroz.ilosc * (dokumnotp.cenanew - dokumnotp.cenaold))
      from nagfak
         left join dokumnag on (nagfak.ref = dokumnag.faktura)
         left join dokumpoz on (dokumnag.ref = dokumpoz.dokument)
         left join dokumroz on (dokumpoz.ref = dokumroz.pozycja)
         left join dokumnotp on (dokumroz.ref = dokumnotp.dokumrozkor)
         left join dokumnot on (dokumnotp.dokument = dokumnot.ref)
      where nagfak.ref = :nagfakref and
         dokumnot.data <= :date_end into :zmiana_kosztow;


    if(:date_rozlicz is null or :date_rozlicz > :date_end) then koszt_pierwotny = :koszt_katal;
    if(:koszt_pierwotny is null) then koszt_pierwotny = 0;
    if(:zmiana_kosztow is null) then zmiana_kosztow = 0;
    koszt = :koszt_pierwotny + :zmiana_kosztow;
  suspend;
end^
SET TERM ; ^
