--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EXECUTION_GET_TVAL(
      EMPLOYEE integer,
      PAYROLL integer,
      COL integer,
      ART140U1P3 smallint,
      LIMITKP4UCP smallint,
      IN_BRUTTO numeric(14,2),
      IN_SBDAYS integer,
      IN_SBENEFIT numeric(14,2),
      IN_ZUS numeric(14,2),
      IN_COST numeric(14,2),
      IN_PTAX numeric(14,2),
      IN_ALLOWANCE numeric(14,2),
      IN_NETTO numeric(14,2),
      IN_ALIMENTY numeric(14,2),
      IN_NFZBASE numeric(14,2),
      IN_ZUSBASE numeric(14,2),
      IN_FREEWAGE numeric(14,2))
  returns (
      BRUTTO numeric(14,2),
      SBDAYS integer,
      SBENEFIT numeric(14,2),
      ZUS numeric(14,2),
      COST numeric(14,2),
      PTAX numeric(14,2),
      CTAX numeric(14,2),
      ALLOWANCE numeric(14,2),
      NETTO numeric(14,2),
      ALIMENTY numeric(14,2),
      NFZBASE numeric(14,2),
      ZUSBASE numeric(14,2),
      FREEWAGE numeric(14,2),
      RET numeric(14,2))
   as
declare variable FROMDATE timestamp;
declare variable TODATE timestamp;
declare variable MINWAGE numeric(14,2);
declare variable UZ numeric(14,2);
declare variable UZO numeric(14,2);
declare variable TAX numeric(14,2);
declare variable NFZ numeric(14,2);
declare variable NFZO numeric(14,2);
declare variable COMPANY smallint;
declare variable WSK numeric(14,4);
declare variable EMPLTYPE integer;
declare variable TVAL numeric(14,2);
declare variable SBTVAL numeric(14,2);
declare variable MINRETIRED numeric(14,2);
declare variable GETSBENEFIT numeric(14,2);
declare variable SBENEFIT2GET numeric(14,2);
declare variable IFROMDATE timestamp;
declare variable ITODATE timestamp;
declare variable IPER varchar(6);
declare variable TVAL_TMP numeric(14,2);
declare variable SBENEFIT_TMP numeric(14,2);
declare variable SFREEWAGE numeric(14,2);
declare variable IS_SBENEFIT smallint = 0;
declare variable DIMNUM smallint;
declare variable DIMDEN smallint;
begin
--MWr: Personel - Wyliczenie maksymalnej kwoty do potracenia przez komornika

  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  execute procedure efunc_prdates(payroll, 2)
    returning_values ifromdate, itodate;

  select empltype, company, iper from epayrolls
    where ref = :payroll
    into :empltype, :company, :iper;

  if (not(empltype = 2 and limitkp4ucp = 1)) then
    limitkp4ucp = 0;

  if (in_brutto is null) then
  begin
    select
        sum(case when c.cflags containing ';POD;' and p.ecolumn not between 4100 and 4900 then p.pvalue else 0 end),
        sum(case when c.cflags containing ';ZAS;' and c.coltype = 'NIE' then p.pvalue else 0 end),
        sum(case when p.ecolumn = 4900 then p.pvalue else 0 end),
        sum(case when p.ecolumn in (6100,6110,6120,6130) then p.pvalue else 0 end),
        sum(case when p.ecolumn = 6500 then p.pvalue else 0 end),
        max(case when p.ecolumn = 7020 then p.pvalue else 0 end),
        sum(case when p.ecolumn = 7060 then p.pvalue else 0 end),
        sum(case when p.ecolumn = 7350 then p.pvalue else 0 end),
        sum(case when p.ecolumn in (7450,7470) then p.pvalue else 0 end),
        sum(case when p.ecolumn = 7700 then p.pvalue else 0 end),
        sum(case when c.cflags containing ';NFZ;' then p.pvalue else 0 end),
        sum(case when c.cflags containing ';ZUS;' then p.pvalue else 0 end)
      from eprpos p
        join ecolumns c on (c.number = p.ecolumn)
      where p.employee = :employee and p.payroll = :payroll
      into :brutto, :sbdays, :sbenefit, :zus, :cost, :ptax, :allowance, :ctax, :netto, :alimenty, :nfzbase, zusbase;

    is_sbenefit = iif(sbdays > 0 or sbenefit > 0, 1, 0);

    if (brutto > 0 or is_sbenefit = 1) then
    begin
      execute procedure get_pval(ifromdate, company, 'UZ')
        returning_values uz;
      execute procedure get_pval(ifromdate, company, 'UZO')
        returning_values uzo;
      execute procedure e_calculate_zus(iper, company)
        returning_values wsk;

    --wyliczenie netto dla wynagrodzenia bez zasilkow
      if (is_sbenefit = 1) then
      begin
        zus = zusbase * (1 - wsk);
        tax = round(brutto - zus - cost) * ptax / 100.00 - allowance;
        if (tax < 0) then tax = 0;
        nfz = (nfzbase - zus) * uz / 100.00;
        if (nfz > tax) then nfz = tax;
        if (nfz < 0) then nfz = 0;
        nfzo = (nfzbase - zus) * uzo / 100.00;
        if (nfzo > tax) then nfzo = tax;
        if (nfzo < 0) then nfzo = 0;
        tax = round(tax - nfzo);
        netto = brutto - zus - tax - nfz;
        ctax = tax;
      end

    /*Wyznaczenie kwoty wolnej dla zajec niealimentacyjnych. Kwota ta moze zostac
      zmniejszona tylko z uwagi na wymiar etatu, przepracowanie jedynie czesci
      miesiaca nie ma wplywu na jej wysokosc*/
      if (empltype = 1 and col <> 7700) then
      begin
        execute procedure get_pval(ifromdate, company, 'MINIMALNAKRAJOWA')
          returning_values minwage;

        select first 1 dimnum, dimden from employment
          where employee = :employee and fromdate <= :todate
            and (todate is null or todate >= :fromdate)
          order by fromdate desc
          into dimnum, dimden;

        nfz = wsk * minwage * uz / 100.00;
        nfzo = wsk * minwage * uzo / 100.00;
        tax = round(round(wsk * minwage - cost) * ptax / 100.00 - allowance - nfzo);
        freewage = wsk * minwage - tax - nfz;
        freewage = cast(freewage * coalesce(dimnum,1) / coalesce(dimden,1) as numeric(14,2));
      end else
        freewage = 0;
    end

    if (brutto is null) then
      brutto = -1;
  end else
  begin
    brutto = in_brutto;
    sbdays = in_sbdays;
    sbenefit = in_sbenefit;
    zus = in_zus;
    cost = in_cost;
    ptax = in_ptax;
    allowance = in_allowance;
    netto = in_netto;
    alimenty = in_alimenty;
    nfzbase = in_nfzbase;
    zusbase = in_zusbase;
    freewage = in_freewage;
  end

  if (brutto > 0 or sbenefit > 0) then
  begin
    sbenefit_tmp = sbenefit;
    --wyliczenie progu potracenia na alimenty --------------------------------
    if (col = 7700) then
    begin
      if (empltype = 1 or limitkp4ucp = 1) then
      begin
      /*potracenie nie moze byc wyzsze niz 3/5 kwoty wynagrodzenia;
        dla potracen z wyngarodzenia brutto nie wystepuje kwota wolna,
        dla zasikow kwota wolna wystepuje i jest wyliczana w dalszej czesci*/
        tval = 0.6 * netto;
      end else begin
      --dla UCP mozna potracic cala kwote netto rachunku
        tval = netto;
      end
    --dopuszczalna kwota potracenia z zasilku chorobowego (sickbenefit threshold):
      sbtval = 0.6 * sbenefit;
    end else
    --wyliczenie progu potracenia na komornika -------------------------------
    if (col = 7710) then
    begin
      if (empltype = 1 or limitkp4ucp = 1) then
      begin
      /*Potracenie nie moze byc wyzsze niz: netto - alimenty - kwota wolna od potracen
        oraz nie moze przekraczac polowy wynagrodzenia, a lacznie z alimentami 3/5 */

        tval = netto - alimenty - freewage;
        if (tval > 0) then
        begin
          if (alimenty > 0) then
          begin
            tval_tmp = 0.6 * netto - alimenty;
            if (tval_tmp < tval ) then tval = tval_tmp;
          end
          tval_tmp = 0.5 * netto;
          if (tval_tmp < tval ) then tval = tval_tmp;
        end else
          tval = 0;
      end else
        tval = netto - alimenty;

    --okreslenie dopuszczalnej kwoty potracenia z zasilku chorobowego
      if (sbenefit > 0) then
      begin
        sbtval = sbenefit * iif(art140u1p3 = 0, 0.5, 0.25);
      /*jezeli wsrod potracen wystepuja naleznosci alimentacyjne, to
        egzekucje lacznie nie moga przekraczac 60% kwoty zasilku */
        getsbenefit = alimenty - (0.6 * netto);
        sbenefit2get = 0.6 * sbenefit - getsbenefit;
        if (sbtval > sbenefit2get) then
          sbtval = sbenefit2get;
      end
    end else
      exception universal col || ' - nieznany typ potracenia komorniczego!';

    if (sbenefit > 0 and sbtval > 0) then
    begin
    --ustalenie kwoty wolnej od potracen z zasilku chorob. (proporcjonalnie do trwania zasilku)

      execute procedure get_pval(ifromdate, company, 'MINIMALNAEMERYTURA')
        returning_values minretired;

      sfreewage = minretired * iif(col <> 7700 and ifromdate >= '2017-07-01', 0.75, 0.5); --BS109261
      sfreewage = sbdays * (cast(sfreewage / (todate - fromdate + 1) as numeric(14,2)));
    --ustalenie, czy potracenie z zasilku SBTVAL nie naruszy kwoty wolnej
      tax = round(sbenefit * ptax / 100.00);
      if (empltype = 2) then tax = 0;
      if (sbenefit - tax - sbtval < sfreewage) then
        sbtval = sbenefit - tax - sfreewage;
    end else
      sbtval = 0;

    sbenefit = sbenefit_tmp;
  end

  if (tval is null or tval < 0) then tval = 0;
  if (sbtval is null or sbtval < 0) then sbtval = 0; 
  tval = tval + sbtval;  

  ret = tval;
  suspend;
end^
SET TERM ; ^
