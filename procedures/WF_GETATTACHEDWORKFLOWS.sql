--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WF_GETATTACHEDWORKFLOWS(
      ATTABLE STRING40 NOT NULL,
      ATREF STRING255 NOT NULL,
      WFSTATUS_FILTER smallint = 15,
      ONLYPRIMARYATT smallint = 0)
  returns (
      OBJECTLABEL STRING255 NOT NULL,
      UUID NEOS_ID NOT NULL,
      WFACTIVE SMALLINT_ID NOT NULL,
      OBJECTUUID NEOS_ID NOT NULL,
      DISPLAYNAME STRING)
   as
begin
for select wi.objectlabel,wi.uuid,wi.wfactive,wi.objectuuid,wi.displayname from wfattachment wa
  join wfinstance wi on wa.wfinstanceuuid=wi.uuid and
    (:onlyprimaryatt=0 or wi.primaryattachment=wa.uuid)
  where wa.attable = :attable and
        (:atref is null or wa.atref=:atref) and
        ((bin_and(:wfstatus_filter,1)=1 and wi.wfactive=0) or --aktywny
         (bin_and(:wfstatus_filter,2)=2 and wi.wfactive=1) or --zakonczony
         (bin_and(:wfstatus_filter,4)=4 and wi.wfactive=2) or --utworzony
         (bin_and(:wfstatus_filter,8)=8 and wi.wfactive=3))   --porzucony
into objectlabel, uuid, wfactive, objectuuid, displayname
  do
  suspend;
objectlabel = '';
  uuid = '';
  objectuuid = '';
  wfactive = 0;
end^
SET TERM ; ^
