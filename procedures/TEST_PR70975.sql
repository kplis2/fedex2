--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TEST_PR70975 as
declare variable i bigint;
begin
  i = 0;
  while(i<40000000) do begin
    i = :i + 1;
  end
end^
SET TERM ; ^
