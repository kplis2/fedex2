--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHANGE_NUMERIC_TO_STR(
      NUMBER numeric(14,2))
  returns (
      STR varchar(20) CHARACTER SET UTF8                           )
   as
declare variable strtmp varchar(100);
declare variable len integer;
declare variable STATUS SMALLINT;
begin
  str = replace(coalesce(number,''),',','.');
  status = 0;
  len = coalesce(char_length(str),0); -- [DG] XXX ZG119346
  if (len > 0) then
    status = 1;
  while(len > 0 and status = 1)
  do begin
    strtmp = substring(str from len for len);
    if (strtmp = '0') then begin
      str = substring(str from 1 for len-1);
      len = coalesce(char_length(str),0); -- [DG] XXX ZG119346
    end else if(strtmp = '.') then begin
      str = substring(str from 1 for len-1);
      len = coalesce(char_length(str),0); -- [DG] XXX ZG119346
      status = 0;
    end else
      status = 0;
  end
  suspend;
end^
SET TERM ; ^
