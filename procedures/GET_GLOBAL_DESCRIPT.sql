--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_GLOBAL_DESCRIPT(
      PNAME varchar(40) CHARACTER SET UTF8                           ,
      PVALUE varchar(255) CHARACTER SET UTF8                           ,
      PSUFFIX varchar(40) CHARACTER SET UTF8                           )
  returns (
      DESCRIPT varchar(255) CHARACTER SET UTF8                           )
   as
declare variable s1 varchar(255);
declare variable s2 varchar(255);
declare variable s3 varchar(255);
declare variable s4 varchar(255);
declare variable s5 varchar(255);
declare variable s6 varchar(255);
declare variable statementtxt varchar(255);
begin
  if(:pvalue is NULL or :pvalue='') then exit;
  descript = '';
  statementtxt = 'select DESCRIPT from x_get_global_descript('''||:pname||''','''||:pvalue||''','''||:psuffix||''')';
  if (exists(select RDB$PROCEDURE_NAME from RDB$PROCEDURES where upper(RDB$PROCEDURE_NAME)='X_GET_GLOBAL_DESCRIPT')) then
  begin
    execute statement statementtxt into :descript;
    if(:descript<>'') then begin
      suspend;
      exit;
    end
  end
  if(:pname='CURRENTCOMPANY') then begin
    select SYMBOL, NAME from COMPANIES where REF=:pvalue into :s1, :s2;
    if(:psuffix='SYMBOL') then descript = :s1;
    if(:psuffix='NAME') then descript = :s2;
  end
  if(:pname='AKTUOPERATOR') then begin
    select NAZWA,LOGIN from OPERATOR where REF=:pvalue into :s1, :s2;
    if(:psuffix='NAZWA') then descript = :s1;
    if(:psuffix='LOGIN') then descript = :s2;
  end
  if(:pname='CRMPODMIOT' and :pvalue<>'NEW' and :pvalue<>'') then begin
    select SKROT,NAZWA,MIASTO,ULICA,TELEFON,NIP from CPODMIOTY where REF=:pvalue into :s1, :s2, :s3, :s4, :s5, :s6;
    if(:psuffix='SKROT') then descript = :s1;
    if(:psuffix='NAZWA') then descript = :s2;
    if(:psuffix='MIASTO') then descript = :s3;
    if(:psuffix='ULICA') then descript = :s4;
    if(:psuffix='TELEFON') then descript = :s5;
    if(:psuffix='NIP') then descript = :s6;
  end
  if(:pname='LocalPERPAYROLL') then begin
    select SYMBOL from EPAYROLLS where REF=:pvalue into :s1;
    if(:psuffix='SYMBOL') then descript = :s1;
  end
  suspend;
end^
SET TERM ; ^
