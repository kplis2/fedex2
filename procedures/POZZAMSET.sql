--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZZAMSET(
      NAGZAMREF integer,
      POZZAMREF integer,
      WERSJAREF integer,
      ILOSC numeric(14,4),
      CENACEN numeric(14,4),
      RABAT numeric(14,4))
   as
begin
  if(:ilosc=0 and :pozzamref<>0) then begin
    -- usun pozycje z zamowienia
    delete from pozzam where ref=:pozzamref;
    exit;
  end
  if(:cenacen=-1 and coalesce(:pozzamref,0)=0) then begin
    -- nie podano ceny wiec ja ustal samodzielnie, okresl tez czy pozycja zamówienia już istnieje
    select cenabazowa,rabat,pozzamref
    from pozzamgetnew(:wersjaref,:nagzamref,:ilosc)
    into :cenacen,:rabat,:pozzamref;
  end
  if(:pozzamref is not null and :pozzamref<>0) then begin
    update pozzam p set
      ilosc = :ilosc,
      cenacen = :cenacen,
      rabat = :rabat
    where p.ref=:pozzamref;
  end else begin
    insert into pozzam(zamowienie,wersjaref,ilosc,cenacen,rabat)
    values(:nagzamref,:wersjaref,:ilosc,:cenacen,:rabat);
  end
end^
SET TERM ; ^
