--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZFAK_DODAJ_OPAKOWANIE(
      POZFAKREF integer,
      ILOSC integer)
  returns (
      STATUS integer)
   as
declare variable opakowanie varchar(255);
declare variable ktm varchar(80);
declare variable wersja integer;
declare variable pozktm varchar(80);
declare variable dokument integer;
declare variable magazyn varchar(3);
declare variable milosc numeric(14,2);
declare variable iloscwopak varchar(255);
declare variable oblilosc numeric(14,2);
begin
  status = 0;
  execute procedure GETCONFIG('OPAKOWANIE') returning_values :opakowanie;
  if(:opakowanie is null or (:opakowanie='')) then exit;
  select KTM,DOKUMENT,MAGAZYN,ILOSCM
    from POZFAK where REF=:pozfakref
    into :pozktm,:dokument,:magazyn,:milosc;
  select ILOSCWOPAK from TOWARY where KTM=:pozktm into :iloscwopak;

  for select TOWAKCES.AKTM,TOWAKCES.AWERSJA
    from TOWAKCES
    left join TOWARY on (TOWARY.KTM=TOWAKCES.AKTM)
    where TOWAKCES.KTM=:pozktm and TOWAKCES.TYP=:opakowanie and TOWARY.USLUGA=1
    into :ktm,:wersja
    do begin
      /* jesli nie podano ilosci opakowan to ja oblicz */
      if(:ilosc is null or (:ilosc=0)) then begin
        /* jesli zdefiniowano ilosc w opakowaniu glownym na towarze to oblicz */
        if(:iloscwopak<>'' and :iloscwopak<>'0') then begin
          /* podziel ilosc magazynowa przez ilosc w opakowaniu */
          oblilosc = :milosc/cast(:iloscwopak as numeric(14,2));
          ilosc = cast(:oblilosc as integer);
          /* zaokraglij w gore */
          if(:oblilosc>:ilosc) then ilosc = :ilosc+1;
        end else begin
          ilosc = cast(:milosc as integer);
          if(:milosc>:ilosc) then ilosc = :ilosc+1;
        end
      end
      insert into pozfak(DOKUMENT,KTM,WERSJA,ILOSC,MAGAZYN,OPK)
      values(:dokument,:ktm,:wersja,:ilosc,:magazyn,1);
      status = 1;
    end
end^
SET TERM ; ^
