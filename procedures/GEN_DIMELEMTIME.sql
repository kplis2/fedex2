--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GEN_DIMELEMTIME(
      PERIOD varchar(20) CHARACTER SET UTF8                           ,
      FROMDATE date,
      TODATE date)
  returns (
      SHORTNAME varchar(40) CHARACTER SET UTF8                           ,
      NAME varchar(80) CHARACTER SET UTF8                           ,
      SLODEF integer,
      SLOPOZ integer,
      ACT smallint,
      NAMEFROMDATE timestamp,
      NAMETODATE timestamp)
   as
declare variable fromdatetmp date;
declare variable cnt integer;
declare variable mth integer;
declare variable yr integer;
declare variable mth1 integer;
declare variable yr1 integer;
declare variable okres varchar(6);
declare variable okresout varchar(6);
declare variable okrespor varchar(6);
declare variable mths varchar(2);
declare variable fromdays varchar(2);
declare variable todays varchar(2);
declare variable namefromdatecur timestamp;
declare variable nametodatecur timestamp;
declare variable shortnamecur varchar(40);
begin
  cnt = 0;
  fromdatetmp = :FROMDATE;
  if (:period = 'DZIEN') then begin
    fromdatetmp = :FROMDATE - 1;
    while (:fromdatetmp <= :todate)
    do begin
      fromdatetmp = :fromdatetmp + 1;
      name = cast(:fromdatetmp as varchar(80));
      shortname = substring(:name from 1 for 40);
      ACT = 1;
      namefromdate = fromdatetmp;
      nametodate = fromdatetmp;
      suspend;
    end
  end else if (:period = 'TYDZIEN') then begin
    namefromdate = :FROMDATE;
    yr = extract(year from :fromdatetmp);
    cnt = 1;
    while (:namefromdate <= :todate)
    do begin
      shortname = cast(yr as varchar(4))||'_TYDZ_'||cast(cnt as varchar(2));
      mths = cast(extract(month from namefromdate) as varchar(2));
      if(coalesce(char_length(mths),0) = 1) then mths = '0'||mths; -- [DG] XXX ZG119346
      fromdays = cast(extract(day from namefromdate) as varchar(2));
      if(coalesce(char_length(fromdays),0) = 1) then fromdays = '0'||fromdays; -- [DG] XXX ZG119346
      nametodate = namefromdate + 6;
      todays = cast(extract(day from nametodate) as varchar(2));
      if(coalesce(char_length(todays),0) = 1) then todays = '0'||todays; -- [DG] XXX ZG119346
      name = cast(extract(year from namefromdate) as varchar(4))||mths||fromdays||'-'||todays;
      ACT = 1;
      if(yr < extract(year from :nametodate)) then begin
        yr = extract(year from :nametodate);
        cnt = 1;
      end else cnt = cnt + 1;
      suspend;
      namefromdate = :namefromdate + 7;
    end
  end else if (:period = 'TYDZIENM') then begin
    namefromdate = :FROMDATE;
    yr = extract(year from :fromdatetmp);
    cnt = 1;
    while (:namefromdate <= :todate)
    do begin
      shortname = cast(yr as varchar(4))||'_TYDZ_'||cast(cnt as varchar(2));
      mths = cast(extract(month from namefromdate) as varchar(2));
      if(coalesce(char_length(mths),0) = 1) then mths = '0'||mths; -- [DG] XXX ZG119346
      fromdays = cast(extract(day from namefromdate) as varchar(2));
      if(coalesce(char_length(fromdays),0) = 1) then fromdays = '0'||fromdays; -- [DG] XXX ZG119346
      nametodate = namefromdate + 6;
      todays = cast(extract(day from nametodate) as varchar(2));
      if(coalesce(char_length(todays),0) = 1) then todays = '0'||todays; -- [DG] XXX ZG119346
      name = cast(extract(year from namefromdate) as varchar(4))||mths||fromdays||'-'||todays;
      ACT = 1;
      if(yr < extract(year from :nametodate)) then begin
        yr = extract(year from :nametodate);
        cnt = 1;
      end else cnt = cnt + 1;
      namefromdatecur = namefromdate;
      if(extract(month from namefromdate) <> extract(month from nametodate))then begin
        shortnamecur = shortname;
        shortname = shortname||'_1';
        nametodatecur = nametodate;
        select d.lday from datatookres (substring(cast(:namefromdate as varchar(100)) from 1 for 10),0,0,0,1) d into :nametodate;
        mths = cast(extract(month from namefromdate) as varchar(2));
        if(coalesce(char_length(mths),0) = 1) then mths = '0'||mths; -- [DG] XXX ZG119346
        fromdays = cast(extract(day from namefromdate) as varchar(2));
        if(coalesce(char_length(fromdays),0) = 1) then fromdays = '0'||fromdays; -- [DG] XXX ZG119346
        todays = cast(extract(day from nametodate) as varchar(2));
        if(coalesce(char_length(todays),0) = 1) then todays = '0'||todays; -- [DG] XXX ZG119346
        name = cast(extract(year from namefromdate) as varchar(4))||mths||fromdays||'-'||todays;
        suspend;
        shortname = shortnamecur||'_2';
        namefromdate = nametodate + 1;
        nametodate = nametodatecur;
        mths = cast(extract(month from namefromdate) as varchar(2));
        if(coalesce(char_length(mths),0) = 1) then mths = '0'||mths; -- [DG] XXX ZG119346
        fromdays = cast(extract(day from namefromdate) as varchar(2));
        if(coalesce(char_length(fromdays),0) = 1) then fromdays = '0'||fromdays; -- [DG] XXX ZG119346
        todays = cast(extract(day from nametodate) as varchar(2));
        if(coalesce(char_length(todays),0) = 1) then todays = '0'||todays; -- [DG] XXX ZG119346
        name = cast(extract(year from namefromdate) as varchar(4))||mths||fromdays||'-'||todays;
        if(extract(year from :namefromdatecur) < extract(year from :nametodate)) then begin
          yr = extract(year from :nametodate);
          cnt = 2;
        end
      end
      suspend;
      namefromdate = namefromdatecur;
      namefromdate = :namefromdate + 7;
    end
  end else if (:period = 'DEKADA') then begin
    yr = extract(year from :fromdatetmp);
    select fday from datatookres(substring(:FROMDATE from  1 for  10),null,null,null,1) into :namefromdate;
    if(extract(day from fromdate) < 11) then begin
      cnt = 1;
    end
    else if(extract(day from fromdate) < 20) then begin
      cnt = 2;
      namefromdate = namefromdate + 10;
    end
    else begin
      cnt = 3;
      namefromdate = namefromdate + 20;
    end
    while (:namefromdate <= :todate)
    do begin
      mths = cast(extract(month from namefromdate) as varchar(2));
      if(coalesce(char_length(mths),0) = 1) then mths = '0'||mths; -- [DG] XXX ZG119346
      shortname = cast(yr as varchar(4))||mths||'_DEKADA_'||cast(cnt as varchar(2));
      if(cnt = 3) then begin
        select lday from datatookres(substring(:nametodate from  1 for  10),null,null,null,1) into :nametodate;
        cnt = 1;
      end else begin
        nametodate = namefromdate + 9;
        cnt = cnt +1;
      end
      todays = cast(extract(day from nametodate) as varchar(2));
      if(coalesce(char_length(todays),0) = 1) then todays = '0'||todays; -- [DG] XXX ZG119346
      fromdays = cast(extract(day from namefromdate) as varchar(2));
      if(coalesce(char_length(fromdays),0) = 1) then fromdays = '0'||fromdays; -- [DG] XXX ZG119346
      name = cast(extract(year from namefromdate) as varchar(4))||mths||fromdays||'-'||todays;
      ACT = 1;
      suspend;
      if(cnt = 1) then select fday from datatookres(substring(:nametodate-5 from  1 for  10),null,1,null,1) into :namefromdate;
      else begin
        namefromdate = :namefromdate + 10;
      end
    end
  end else if (period = 'MIESIAC') then begin
    yr1 = extract(year from :fromdatetmp);
    mth1 = extract(month from :fromdatetmp);
    if (:mth1 >= 10) then
      okres = :yr1||:mth1;
    else
      okres = :yr1||'0'||:mth1;
    yr = extract(year from :todate);
    mth = extract(month from :todate);
    if (:mth >= 10) then
      okrespor = :yr||:mth;
    else
      okrespor = :yr||'0'||:mth;
    while (:okres <= :okrespor)
    do begin
      select datatookres.fday, datatookres.lday from datatookres(:okres,0,0,0,1)
        into :namefromdate, :nametodate;
      select datatookres.okres from datatookres(:okres,0,1,0,0)
        into :okresout;
      name = :okres;
      shortname = substring(:name from 1 for 40);
      ACT = 1;
      okres = :okresout;
      suspend;
    end
  end else if (period = 'KWARTAL') then begin
    yr1 = extract(year from :fromdatetmp);
    mth1 = extract(month from :fromdatetmp);
    if (:mth1 >= 10) then
      okres = :yr1||:mth1;
    else
      okres = :yr1||'0'||:mth1;
    yr = extract(year from :todate);
    mth = extract(month from :todate);
    if (:mth >= 10) then
      okrespor = :yr||:mth;
    else
      okrespor = :yr||'0'||:mth;
    while (:okres <= :okrespor)
    do begin
      select datatookres.fday from datatookres(:okres,0,0,0,1)
        into :namefromdate;
      select datatookres.okres from datatookres(:okres,0,2,0,0)
        into :okresout;
      select datatookres.lday from datatookres(:okresout,0,0,0,1)
        into :nametodate;
      name = :okres||'-'||:okresout;
      cnt = cast(substring(:okresout from 5 for 2) as integer);
      if (:cnt = 3) then cnt = 1;
      else if (:cnt = 6) then cnt = 2;
      else if (:cnt = 9) then cnt = 3;
      else if (:cnt = 12) then cnt = 4;
      shortname = substring(:okres from 1 for 4)||'_KW_'||:cnt;
      ACT = 1;
      select datatookres.okres from datatookres(:okresout,0,1,0,0)
        into :okres;
      suspend;
    end
  end else if (period = 'POLROCZE') then begin
    yr1 = extract(year from :fromdatetmp);
    mth1 = extract(month from :fromdatetmp);
    if (:mth1 >= 10) then
      okres = :yr1||:mth1;
    else
      okres = :yr1||'0'||:mth1;
    yr = extract(year from :todate);
    mth = extract(month from :todate);
    if (:mth >= 10) then
      okrespor = :yr||:mth;
    else
      okrespor = :yr||'0'||:mth;
    while (:okres <= :okrespor)
    do begin
      select datatookres.fday from datatookres(:okres,0,0,0,1)
        into :namefromdate;
      select datatookres.okres from datatookres(:okres,0,5,0,0)
        into :okresout;
      select datatookres.lday from datatookres(:okresout,0,0,0,1)
        into :nametodate;
      name = :okres||'-'||:okresout;
      cnt = cast(substring(:okresout from 5 for 2) as integer);
      if (:cnt = 6) then cnt = 1;
      else if (:cnt = 12) then cnt = 2;
      shortname = substring(:okres from 1 for 4)||'_POL_'||:cnt;
      ACT = 1;
      select datatookres.okres from datatookres(:okresout,0,1,0,0)
        into :okres;
      suspend;
    end
  end else if (period = 'ROK') then begin
    yr1 = extract(year from :fromdatetmp);
    yr = extract(year from :todate);
    while (:yr1 <= :yr)
    do begin
      select datatookres.fday from datatookres(cast(:yr1 as varchar(4))||'01',0,0,0,1)
        into :namefromdate;
      select datatookres.lday from datatookres(cast(:yr1 as varchar(4))||'12',0,0,0,1)
        into :nametodate;
      name = cast(yr1 as varchar(80));
      shortname = substring(:name from 1 for 40);
      ACT = 1;
      suspend;
      yr1 = :yr1 + 1;
    end
  end
end^
SET TERM ; ^
