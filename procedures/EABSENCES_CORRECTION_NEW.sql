--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EABSENCES_CORRECTION_NEW(
      REF integer)
  returns (
      NEWREF integer)
   as
declare variable ayear integer;
declare variable amonth smallint;
declare variable bcode integer;
declare variable company integer;
declare variable certserial varchar(2);
declare variable certnumber varchar(7);
declare variable dokdate timestamp;
declare variable days integer;
declare variable daypayamount numeric(14,2);
declare variable ecolumn integer;
declare variable employee integer;
declare variable fromdate timestamp;
declare variable fbase varchar(6);
declare variable lbase varchar(6);
declare variable monthpaybase numeric(14,2);
declare variable npdays smallint;
declare variable scode integer;
declare variable scodeb integer;
declare variable todate timestamp;
declare variable workdays integer;
declare variable worksecs integer;
declare variable workhours varchar(20);
declare variable payamount numeric(14,2);
declare variable hourpayamount numeric(14,2);
declare variable firstabsence integer;
declare variable baseabsence integer;
declare variable calcpaybase smallint;
declare variable dimden smallint;
declare variable dimnum smallint;
declare variable emplcontract integer; -- PR60323
begin
--Personel: Procedura tworzy korekte nieobecnosci NEWREF z nieobecnosci REF

  select ref, ayear, amonth, bcode, company, certserial, certnumber,
         dokdate, days, ecolumn, employee, fromdate, fbase, lbase,
         monthpaybase, daypayamount, payamount, hourpayamount,
         npdays, scode, scodeb, todate, workdays, worksecs, workhours,
         firstabsence, baseabsence, calcpaybase, dimden, dimnum,
     emplcontract
    from eabsences e
    where ref = :ref
    into :ref, :ayear, :amonth, :bcode, :company, :certserial, :certnumber,
        :dokdate, :days, :ecolumn, :employee, :fromdate, :fbase, :lbase,
        :monthpaybase, :daypayamount, :payamount, :hourpayamount,
        :npdays, :scode, :scodeb, :todate, :workdays, :worksecs, :workhours,
        :firstabsence, :baseabsence, :calcpaybase, :dimden, :dimnum,
        :emplcontract;

--oznaczenie strego rekordu jako skorygowanego
  update eabsences
    set correction = 1
    where ref = :ref;

--utworzenie korekty
  execute procedure gen_ref('EABSENCES')
    returning_values newref;

  insert into eabsences (ref, ecolumn, employee, ayear, amonth, fromdate, todate,
      dokdate, days, workdays, worksecs, workhours, scode, bcode, monthpaybase,
      daypayamount, fbase, lbase, epayroll, npdays, company, certserial, certnumber,
      scodeb, correction, corrected, corepayroll, baseabsence, firstabsence,
      calcpaybase, payamount, hourpayamount, dimden, dimnum, emplcontract)
    values (:newref, :ecolumn, :employee, :ayear, :amonth, :fromdate, :todate,
     :dokdate, :days, :workdays, :worksecs, :workhours, :scode, :bcode, :monthpaybase,
     :daypayamount, :fbase, :lbase, null, :npdays, :company, :certserial, :certnumber,
     :scodeb, 2, :ref, null, :baseabsence, :firstabsence,
     :calcpaybase, :payamount, :hourpayamount, :dimden, :dimnum, :emplcontract);

  suspend;
end^
SET TERM ; ^
