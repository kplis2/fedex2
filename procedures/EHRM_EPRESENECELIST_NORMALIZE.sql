--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EHRM_EPRESENECELIST_NORMALIZE(
      SELECTDATE DATE_ID,
      EMPLOYEEREF EMPLOYEES_ID)
   as
declare variable epresencelistsposref type of epresencelistspos_id;
declare variable timestart type of timestamp_id;
declare variable timestop type of timestamp_id;
declare variable tmpepresencelistsposref type of epresencelistspos_id;
declare variable tmptimestart type of timestamp_id;
declare variable tmptimestop type of timestamp_id;
declare variable epresencelistsref type of epresencelists_id;
declare variable epresencelistnagref type of epresencelistnag_id;
begin
    --- znajdz liste obecnosci
    select l.ref from epresencelists l
    where l.listsdate = :selectdate
    into :epresencelistsref;

    --- znajdz naga w liscie obecnosci
    select n.ref from epresencelistnag n
    where n.epresencelist = :epresencelistsref and
          n.employee = :employeeref
    into :epresencelistnagref;


    --- Usuniecie rekordów z statusem do usuniecia
    delete from epresencelistspos p
    where p.epresencelistnag = :epresencelistnagref and
          p.state = 2;

    --- Aktualizacja rekordów z statusem do akceptacji
    Update epresencelistspos p
    SET p.timestartstr = p.newstartat,
        p.timestopstr = p.newstopat,
        p.state = 0,
        p.newstartat = null, 
        p.newstopat = null,
        p.descapply = null
    where p.epresencelistnag = :epresencelistnagref and
          p.state = 1;

    for select p.ref, p.timestart, p.timestop
     from epresencelistspos p
     where p.epresencelistnag = :epresencelistnagref
     order by p.timestart asc
     into :epresencelistsposref, :timestart, :timestop

    do begin
     if(:tmpepresencelistsposref is null) then begin
        tmpepresencelistsposref = :epresencelistsposref;
        tmptimestart = :timestart;
        tmptimestop = :timestop;
     end
     else begin
        --- okresy sie w ogole nie zazebiaja
        if(:tmptimestop < :timestart) then begin
            --- zapisanie scalonego wniosku
            update epresencelistspos lp
            set lp.timestopstr = trim(substring(:tmptimestop from 12 for 16))
            where lp.ref = :tmpepresencelistsposref;

            --- pobranie nowego wniosku
            tmpepresencelistsposref = :epresencelistsposref;
            tmptimestart = :timestart;
            tmptimestop = :timestop;
        end
        --- okresy nachodzą na sibie
        else if (:tmptimestop >= :timestart and :tmptimestop < :timestop) then begin
            --- rozszerzamy zakres pierwszego wniosku
            tmptimestop = :timestop;

            --- oznaczenie drugiego wniosku do usuniecia
            update epresencelistspos lp
            set lp.state = 2
            where lp.ref = :epresencelistsposref;
        end
        --- drugi okres zawiera sie w pierwszym
        else if (:tmptimestop > :timestart and :tmptimestop >= :timestop) then begin
            --- usuniecie drugiego wniosku
            update epresencelistspos lp
            set lp.state = 2
            where lp.ref = :epresencelistsposref;
        end
     end
    end

    --- zapisanie scalonego wniosku
    update epresencelistspos lp
            set lp.timestopstr = trim(substring(:tmptimestop from 12 for 16))
            where lp.ref = :tmpepresencelistsposref;

    --- usuniecie elementow oznaczonych do usuniecia podczas scalania
    delete from epresencelistspos lp
    where lp.epresencelistnag = :epresencelistnagref and
          lp.state = 2;
end^
SET TERM ; ^
