--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KOMPLET_GENZAM(
      ZAMOWIENIE integer,
      FROMKPL integer)
   as
declare variable cnt integer;
declare variable werref integer;
declare variable il numeric(14,4);
declare variable ill numeric(14,4);
declare variable ilsum numeric(14,4);
declare variable ilkomplet numeric(14,4);
declare variable typmag integer;
declare variable cenamag numeric(14,4);
declare variable ktm varchar(40);
declare variable nieobrot integer;
begin
  select KILOSC from NAGZAM where REF=:zamowienie into :ilkomplet;
  select KPLNAG.typmag from KPLNAg where REF=:fromkpl into :typmag;
  for select KPLPOZ.KTM, KPLPOZ.WERSJAREF, KPLPOZ.ILOSC from KPLPOZ where NAGKPL = :fromkpl
  order by KPLPOZ.numer
  into :ktm, :werref, :il
  do begin
    select NIEOBROT from WERSJE where REF=:werref into :nieobrot;
    if(:nieobrot=1) then select REF from WERSJE where KTM=:ktm and NRWERSJI=0 into :werref;
    cnt = null;
    /*execute procedure KOMPLET_GET_CENAMAG(:ktm, :werref,:typmag) returning_values :cenamag;*/
    cenamag = 0;
    select count(*) from POZZAM where ZAMOWIENIE = :ZAMOWIENIE and WERSJAREF = :werref into :cnt;
    if(:cnt is null) then cnt = 0;
    if(:cnt = 0) then begin
      ilsum = :il*:ilkomplet;
      insert into POZZAM(ZAMOWIENIE, WERSJAREF,KILOSC,ILOSC,CENAMAG,opk) values(:ZAMOWIENIE, :werref, :il,:ilsum,:cenamag,0);
    end
    else begin
      ill = null;
      select sum(KILOSC) from POZZAM where ZAMOWIENIE = :ZAMOWIENIE and WERSJAREF = :werref into :ill;
      if(:ill is null) then ill = 0;
      if(:ill <> :il) then begin
         delete from POZZAM where ZAMOWIENIE = :zamowienie and WERSJAREF = :werref;
         ilsum = :il * :ilkomplet;
         insert into POZZAM(ZAMOWIENIE,WERSJAREF,KILOSC,ilosc,cenamag,opk) values(:ZAMOWIENIE,:werref, :il,:ilsum,:cenamag,0);
      end
    end
  end
  delete from POZZAM where ZAMOWIENIE = :ZAMOWIENIE and (select count(*) from KPLPOZ where NAGKPL = :fromkpl and KTM = POZZAM.KTM) = 0;
end^
SET TERM ; ^
