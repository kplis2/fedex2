--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZZAM_PART_CREATE(
      POZZAMREF integer,
      ILOSC numeric(14,4))
   as
declare variable orgilosc numeric (14,4);
declare variable nagzam integer;
begin
  if (:pozzamref is null or :pozzamref = 0
    or :ilosc is null or :ilosc = 0
  ) then exit;

  select pozzam.ilosc, pozzam.zamowienie
    from pozzam
    where pozzam.ref = :pozzamref
    into :orgilosc, :nagzam;

  if (:ilosc >= :orgilosc ) then
    exception universal 'Nieprawidowa iloćć';
  update pozzam set pozzam.ilosc = :ilosc where ref = :pozzamref;

  insert into POZZAM(ZAMOWIENIE, MAGAZYN, MAG2, KTM, WERSJA, wersjaref,
      ILOSC, ILOSCM, ILOSCO, JEDN, JEDNO, CENAMAG, CENACEN, WALCEN,
      RABAT, CENANET, CENABRU, OPK, paramn1, paramn2, paramn3, paramn4,
      paramn5, paramn6, paramn7, paramn8, paramn9, paramn10, paramn11, paramn12,
      params1, params2, params3, params4, params5, params6, params7, params8,
      params9, params10, params11, params12, paramd1, paramd2, paramd3, paramd4, prec)
    select :nagzam, MAGAZYN, MAG2, KTM, WERSJA, wersjaref,
        :orgilosc - :ilosc, ILOSCM, ILOSCO, JEDN, JEDNO, CENAMAG, CENACEN, WALCEN,
        RABAT, CENANET, CENABRU, OPK, paramn1, paramn2, paramn3, paramn4,
        paramn5, paramn6, paramn7, paramn8, paramn9, paramn10, paramn11, paramn12,
        params1, params2, params3, params4, params5, params6, params7, params8,
        params9, params10, params11, params12, paramd1, paramd2, paramd3, paramd4, prec
      from pozzam where pozzam.ref = :pozzamref ;
end^
SET TERM ; ^
