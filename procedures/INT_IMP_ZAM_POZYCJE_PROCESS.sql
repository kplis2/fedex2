--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_ZAM_POZYCJE_PROCESS(
      SESJAREF SESJE_ID,
      TABELA STRING35,
      REF INTEGER_ID,
      BLOKUJZALEZNE SMALLINT_ID = 0,
      TYLKONIEPRZETWORZONE SMALLINT_ID = 0,
      AKTUALIZUJSESJE SMALLINT_ID = 1)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable sesja sesje_id;
declare variable tmpstatus smallint_id;
declare variable tmpmsg string255;
begin
  if (exists(select first 1 1 from rdb$procedures p where p.rdb$procedure_name = 'X_INT_IMP_ZAM_POZYCJE_PROCESS')) then
  begin
    execute statement 'execute procedure X_INT_IMP_ZAM_POZYCJE_PROCESS('||:sesjaref||','''||:tabela||''','||:ref||','||
        :blokujzalezne||','||:tylkonieprzetworzone||','||:aktualizujsesje||')'
    into :status, :msg;
  end
  else
  begin
    --TODO, standardowa procedura
  end

  suspend;
end^
SET TERM ; ^
