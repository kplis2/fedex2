--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ANAL_PRAWAOPER(
      OPERATORREF INTEGER_ID)
  returns (
      OBJECT STRING1024,
      RRIGHT SMALLINT_ID,
      RRESUFROM STRING1024,
      ID INTEGER_ID,
      PARENTID INTEGER_ID,
      TYP SMALLINT_ID)
   as
declare variable RIGHTSGROUP STRING1024;
declare variable ISRIG SMALLINT_ID;
declare variable ISRIGGR SMALLINT_ID;
declare variable RIGHTSGROUPZAP STRING1024;
declare variable GROUPSYM STRING1024;
declare variable OLDGROUPSYM STRING1024;
declare variable ALLRIGHT SMALLINT_ID;
declare variable QUERYTYPESREF INTEGER_ID;
declare variable IDGROUP INTEGER_ID;
declare variable TMPOBJECT STRING1024;
declare variable TMPRRIGHT SMALLINT_ID;
declare variable TMPRRESUFROM STRING1024;
declare variable RIGHTSAPPLICATIONS STRING1024;
declare variable QUERYAPPLICATIONS string1024;
declare variable QUERYALLRIGHT SMALLINT_ID;
declare variable QUERYISRIG SMALLINT_ID;
declare variable QUERYISRIGGR SMALLINT_ID;
declare variable QUERYRIGHTSGROUPZAP STRING1024;
begin
  -- pobranie uprawnien operatora
  select coalesce(o.grupa,''), coalesce(o.applications,'')
    from operator o
      where o.ref = :operatorref into :rightsgroup, :rightsapplications;
  -- analizy
  TYP = 1;
  id = 1;
  parentid = 0;
  rresufrom = '';
  rright = null;
  object = 'Analizy';
  suspend;
  for
    select coalesce(q.name, ''), q.ref, q.application, iif(coalesce(q.rights,'') = '' and coalesce(q.rightsgroup,'') = '', 1, 0),
        strmulticmp(coalesce(q.rights,''),';'||cast(:operatorref as string)||';'),
        strmulticmp(coalesce(q.rightsgroup,''), :rightsgroup),
        coalesce(q.rightsgroup,'')
      from querytypes q
    into :object, :querytypesref, :queryapplications, :queryallright, :queryisrig, :queryisriggr, :queryrightsgroupzap
  do begin
    parentid = 1;
    id = id + 1;
    rresufrom = '';
    rright = null;
    suspend;
    parentid = :id;
    for
      select z.nazwa, iif(coalesce(z.rights,'') = '' and coalesce(z.rightsgroup,'') = '', 1, 0),
          strmulticmp(coalesce(z.rights,''), ';'||cast(:operatorref as string)||';'),
          strmulticmp(coalesce(z.rightsgroup,''), :rightsgroup),
          coalesce(z.rightsgroup,'')
        from zapytania z
        where
          z.grupa = 0 and z.querytype = :querytypesref
        order by z.nazwa
      into :object, :allright, :isrig, :isriggr, :rightsgroupzap
    do begin
      rresufrom = '';
      rright = 0;
      if (strmulticmp(coalesce(:rightsapplications,''), ';'||:queryapplications||';') = 1) then
      begin
        rresufrom = 'Uprawnienie do modułu aplikacji';
        rright = 1;


        if (queryallright = 1) then
        begin
          rresufrom = 'Brak ograniczeń do grupy analiz';
          rright = 1;
        end
        else
        if (queryisrig = 1) then
        begin
          rresufrom = 'Prawo do grupy analiz dla operatora';
          rright = 1;
        end
        else
        if (queryisriggr = 1) then
        begin
          rresufrom = 'Prawo do grupy analiz dla grupy operatorów'||:groupsym;
          rright = 1;
        end
        else
        begin
          rresufrom = 'Brak uprawnień do grupy analiz';
          rright = 0;
        end

        if (rright = 1) then
        begin
          if (allright = 1) then
          begin
            rresufrom = 'Bez ograniczeń';
            rright = 1;
          end
          else
          if (isrig = 1) then
          begin
            rresufrom = 'Prawo dla użykownika';
            rright = 1;
          end
          else
          if (isriggr = 1) then
          begin
            select list(o.nazwa, ', ') from opergrupy o
            where o.symbol in
              (select y.stringout
                from parsestring(:rightsgroup,';') x
                  join parsestring(:rightsgroupzap,';') y on (y.stringout = x.stringout))
              into :groupsym;
            rresufrom = 'Prawa dla grup: '||:groupsym;
            rright = 1;
          end
          else
          begin
           rresufrom = 'Brak uprawnień do zapytania';
            rright = 0;
          end
        end
      end
      else
      begin
        rresufrom = 'Brak uprawnień do modułu '||queryapplications;
        rright = 0;
      end

      id = id + 1;
      suspend;
    end
  end
  ------------
  TYP = 2;
  parentid = 0;
  object = 'Prawa operatorów';
  Rresufrom = '';
  id = id + 1;
  rright = null;
  suspend;
  parentid = :id;
  for
    select d.nazwa, iif(d.defvalue = '1' or o.operator is not null,1,0),
      iif(d.defvalue = '1', 'Domyślna wartość prawa',
        iif(o.operator is not null, 'Operator ma nadane prawo','Brak prawa'))
      from DEFOPERRIGHT d
        left join OPERRIGHT o on (o.prawo = d.akronim and o.operator = :operatorref)
    into :object, :RRIGHT, :rresufrom
  do begin
    id = id + 1;
    suspend;
  end
  ------------
  TYP = 3;
  parentid = 0;
  object = 'Uprawnienia akcji';
  Rresufrom = '';
  RRIGHT = null;
  id = id + 1;
  suspend;
  parentid = :id;
  oldgroupsym = '';
  for
    select r.zb_danych, r.el_zb, iif(r.rodz > 0,0,1), iif(r.rodz = 1,'Nałoźona ochrona(wyszarzone)', iif(r.rodz = 2,'Nałoźona ochrona(niewidoczne)', ''))
      from RIGHTS_OCHRONY(:operatorref) r
    into :groupsym, :tmpobject, :tmpRRIGHT, :tmprresufrom
  do begin
    if (groupsym != oldgroupsym) then
    begin
      object = groupsym;
      RRIGHT = null;
      rresufrom = '';
      id = id + 1;
      suspend;
      parentid = :id;
    end
    id = id + 1;
    object = tmpobject;
    RRIGHT = tmpRRIGHT;
    rresufrom = tmprresufrom;
    suspend;
    oldgroupsym = groupsym;
  end
end^
SET TERM ; ^
