--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CRM_PROGRAMLOJ_NS(
      CPLREF integer)
  returns (
      REF integer)
   as
BEGIN
  EXECUTE PROCEDURE crm_programloj_obliczfromprog(:CPLREF,'A');
  REF=1;
  SUSPEND;
END^
SET TERM ; ^
