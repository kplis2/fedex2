--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TEST_PR84610_2(
      NAME STRING)
  returns (
      HELLO STRING)
   as
declare variable operator_ref integer_id;
begin
  /* Procedure Text */
  operator_ref = (select first 1 o.ref from operator o order by o.ref);
  update operator o set o.nazwisko = o.nazwisko where  o.ref = :operator_ref;

  hello = 'Witaj '||name;
  --exception universal 'TEST';
  suspend;
end^
SET TERM ; ^
