--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TOWORDS_PODAJ_SLOWNIE(
      I integer,
      W varchar(1024) CHARACTER SET UTF8                           ,
      LANGUAGE integer)
  returns (
      RET integer,
      WOUT varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable q integer;
declare variable k integer;
declare variable atr integer;
declare variable w2 varchar(1024);
declare variable z varchar(1024);
declare variable p char;

begin
-- Funkcja dopisuje slowna nazwe dla liczby "i" oraz
-- zwraca numer odpowiedniej formy fleksyjnej dla rzedu liczby.

  atr = 3;
  if (language = 0) then begin
    if (i/100 <> 0) then

        -- W wypadku gdy  i - (100..999)
      q = 3;
    else if (i/10 <> 0) then begin
        -- W wypadku gdy  i - (10..99)
      q = 2;
    end else
        -- W wypadku gdy  i - (1..9)
      q = 1;
    p = 0;
      -- Tyle razy ile cyfr w liczbie.q - numer cyfry
    while (q > 0)
    do begin
        -- k = aktualna cyfra
      k = floor(:i / power(10,:q - 1));
      if (k = 0) then begin
        if (p = 0) then atr = 0;
      end else if (k = 1) then begin
        if (q = 3) then w = :w ||'sto ';
        else if (q = 2) then begin
            -- dla wszystkich dwucyfrowych liczb
          execute procedure towords_nascie(:w, :i, :language) returning_values w2;
          w = :w2;
            -- q = 0 bo cala liczba jest juz zapisana do "w"
          q = 0;
        end else begin
          w = :w || 'jeden ';
          -- aby nie bylo "101 tysiac"
          if (p = 0) then atr = 1;
        end
      end else if (k = 2) then begin
        if (q = 3) then w = :w || 'dwieście ';
        else if (q = 2) then w = :w || 'dwadzieścia ';
        else begin
          w = :w || 'dwa ';
          atr = 2;
        end
      end else if (k = 3) then begin
        w = :w || 'trzy';
      end else if (k = 4) then begin
        w = :w || 'czter';
      end else if (k = 5) then begin
        w = :w || 'pięć';
      end else if (k = 6) then begin
        w = :w || 'sześć';
      end else if (k = 7) then begin
        w = :w || 'siedem';
      end else if (k = 8) then begin
        w = :w || 'osiem';
      end else if (k = 9) then begin
        w = :w || 'dziewięć';
      end

      p = (cast (:p as integer)) + 1;
      if (k <> 0 and k <> 1 and k <> 2) then begin
        atr = 2;
        if (q = 1) then
          if (k = 4) then w = :w || 'y ';
          else w = :w || ' ';
        else if (q = 2) then
          if (k = 3 or k = 4) then w = :w ||'dzieści ';
          else w = :w || 'dziesiąt ';
        else if (q = 3) then
          if (4 < k and k < 10) then w = :w ||'set ';
          else begin
            if (k = 4) then w = :w || 'y';
            w = :w || 'sta ';
          end
      end
      if (q <> 1) then
        i = i - k * power(10, :q - 1);
      q = :q - 1;
    end

    if (atr = 2) then begin
      if (i = 2) then atr = 2;
      else if (i = 3) then atr = 2;
      else if (i = 4) then atr = 2;
      else atr = 3;
    end
    wout = w;
    ret = atr;
    suspend;
  end else if (language = 1) then begin

    if (i/100 <> 0) then
      -- W wypadku gdy  i - (100..999)
      q = 3;
    else if (i/10 <> 0) then begin
     -- W wypadku gdy  i - (10..99)
        q = 2;
    end else
     -- W wypadku gdy  i - (1..9)
      q=1;
    p = 0;
    -- Tyle razy ile cyfr w liczbie.q - numer cyfry

    while (q > 0)
    do begin
    -- k - aktualna cyfra

      k = floor(:i / power(10, :q - 1));
      if (k = 0) then begin
        if (p = 0) then atr = 0;
      end else if (k = 1) then begin
        if (q = 3) then w = w ||'one ';
        else if (q = 2) then begin
        -- dla wszystkich dwucyfrowych liczb
          execute procedure TOWORDS_NASCIE(:w, :i, :Language) returning_values w2;
          w = w2;
          -- q = 0 bo cala liczba jest juz zapisana do "w"
          q = 0;
        end else begin
          w = :w || 'one ';
          -- aby np. nie bylo "101 tysiac"
          if (p = 0) then atr = 1;
        end
      end else if (k = 2) then begin
        if (q = 2) then w = :w ||'twenty ';
        else w = :w ||'two';
      end else if (k = 3) then begin
        if (q = 2) then w = :w ||'thirty ';
        else w = :w ||'three';
      end else if (k = 4) then begin
        if (q = 2) then w = :w ||'forty ';
        else w = :w ||'four';
      end else if (k = 5) then begin
        if (q = 2) then w = :w || 'fifty ';
        else w = :w ||'five';
      end else if (k = 6) then begin
        if (q = 2) then w = :w ||'sixty ';
        else w = :w ||'six';
      end else if (k = 7) then begin
        if (q = 2) then w = :w ||'seventy ';
        else w = :w || 'seven';
      end else if (k = 8) then begin
        if (q = 2) then w = :w || 'eighty ';
        else w = :w ||'eight';
      end else if (k = 9) then begin
        if (q = 2) then w = :w ||'ninety ';
        else w = :w || 'nine';
      end
      p = (cast (:p as integer)) + 1;
      if (q = 3) then
        w = :w ||' hundred ';
      if (q <> 1) then
        i = :i - :k * power(10, :q - 1);
      q = :q - 1;
    end
    if (atr = 2) then begin
      if (i = 2) then atr = 2;
      else if (i = 3) then atr = 2;
      else if (i = 4) then atr = 2;
      else atr = 3;
    end
    wout = w;
    ret = atr;
    suspend;
  end else if (language = 2) then begin
                                         ----- NIEMIECKI ---------
    if (i/100 <> 0) then
    -- W wypadku gdy  i - (100..999)
      q = 3;
    else if (i/10 <> 0) then
    -- W wypadku gdy  i - (10..99)
      q = 2;
    else
    -- W wypadku gdy  i - (1..9)
      q = 1;
    p = 0;
    while (q > 0)
    do begin
      -- k - aktualna cyfra
      k = floor(:i / power(10, :q - 1));
      if (k = 0) then begin
        if (p = 0) then atr = 0;
      end else if (k = 1) then begin
        if (q = 3) then w = :w ||'ein';
        else if (q = 2) then begin
          execute procedure TOWORDS_NASCIE(:w, :i, :language) returning_values w2;
          w = :w2;
          --q = 0 bo cala liczba jest juz zapisana do "w"
          q = 0;
        end else begin
          w = :w ||'ein';
          --aby np. nie bylo "101 tysiac"
          if (p = 0) then atr = 1;
        end
      end else if (k = 2) then begin
        if (q = 2) then w = :w||'zwanzig ';
        else w = :w ||'zwei';
      end else if (k = 3) then begin
        if (q = 2) then w = :w||'dreiBig ';
        else w = :w ||'drei';
      end else if (k = 4) then begin
        if (q = 2) then w = :w||'vierzig ';
        else w = :w ||'vier';
      end else if (k = 5) then begin
        if (q = 2) then w = :w||'funfzig ';
        else w = :w ||'funf';
      end else if (k = 6) then begin
        if (q = 2) then w = :w||'sechzig ';
        else w = :w ||'sechs';
      end else if (k = 7) then begin
        if (q = 2) then w = :w||'siebzig ';
        else w = :w ||'sieben';
      end else if (k = 8) then begin
        if (q = 2) then w = :w||'achtzig ';
        else w = :w ||'acht';
      end else if (k = 9) then begin
        if (q = 2) then w = :w||'neunzig ';
        else w = :w ||'neun';
      end

      p = (cast (:p as integer)) + 1;
      if (q = 3) then
        w = :w ||' hundert ';
      if (q <> 1) then
        i = :i - :k * power(10, :q - 1);
      q = :q - 1;
      -- zapisana w 'w' cyfra jest usuwana z "i"
      if(z <> '') then w = :w ||'und' || :z;
    end
    if (atr = 2) then begin
      if (i = 2) then atr = 2;
      else if (i = 3) then atr = 2;
      else if (i = 4) then atr = 2;
      else atr = 3;
    end
    wout = w;
    ret = atr;
    suspend;
  end
end^
SET TERM ; ^
