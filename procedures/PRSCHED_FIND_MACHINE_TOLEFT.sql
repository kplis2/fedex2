--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_FIND_MACHINE_TOLEFT(
      SCHEDOPER integer,
      TSTART timestamp,
      TRYB smallint)
  returns (
      STARTTIME timestamp,
      ENDTIME timestamp,
      MACHINE integer)
   as
declare variable defmachine integer;
declare variable workplace varchar(20);
declare variable schedule integer;
declare variable schedzamnum integer;
declare variable gapfound smallint;
declare variable localtime timestamp;
declare variable schedzam integer;
declare variable worktime float;
declare variable cnt integer;
declare variable shoper integer;
declare variable prschedule integer;
declare variable tmpstart timestamp;
begin
  select PRSCHEDOPERS.MACHINE, prschedopers.worktime, PRSHOPERS.workplace,PRSCHEDOPERS.schedule,
      prschedzam.number, prschedopers.schedzam
    from PRSCHEDOPERS
      join prshopers on (prschedopers.shoper = prshopers.ref)
      join prschedzam on (prschedzam.ref = prschedopers.schedzam)
    where prschedopers.ref=:schedoper
    into :machine, :worktime, :workplace, :schedule, :schedzamnum, :schedzam;
  starttime = :tstart;
  defmachine = :machine;  --sprawdzenie, czy maszyna juz jest przypisana

  gapfound = 0;
  cnt = 0;
  while(:gapfound = 0) do begin
    for select prmachines.ref, max(prschedopers.endtime)
      from prmachines
        left join prschedopers on (prschedopers.machine = prmachines.ref
            and prschedopers.schedule = :schedule
            and prschedopers.endtime > :starttime and prschedopers.endtime is not null
            and prschedopers.starttime <= :starttime
            and ((prschedopers.schedzam  = :schedzam and prschedopers.verified > 0) or (prschedopers.schedzam <> :schedzam)))--biez pod uwage jedynie wczesniejsze oepracje swojego zlecenia, ktore juz zweryfikowales
        left join PRMACHINESWPLACES MWP on (MWP.prmachine = prmachines.ref and MWP.FROMDATE<=:TSTART and (MWP.TODATE>=:TSTART or MWP.TODATE is NULL))
      where
           MWP.prworkplace = :workplace
        and (prmachines.ref = :defmachine or :defmachine is null)
    group by prmachines.ref
    order by 2 nulls first
    into :machine, :localtime
    do begin
      gapfound = 1;
      if(:localtime is not null) then
        starttime = :localtime;--zaczynaj nie wczesniej, niz zakonczy sie ostatnia operacja na tej maszynie
      --okresl, kiedy zaczyna sie pierwsza operacja na tej maszynie
      localtime = null;
      select min(prschedopers.starttime)
        from prschedopers
      where prschedopers.schedule = :schedule
        and prschedopers.starttime >= :starttime
        and prschedopers.starttime  <= :starttime + :worktime
        and (prschedopers.machine = :machine)
      into :localtime;
      if(:localtime is null)then begin
        --brak operacji zaczynajacej sie w naszej przerwie - to nas interesuje
        endtime = :starttime + :worktime;
        exit;
      end
      starttime = :localtime;
    end
    if(:gapfound = 0) then
      gapfound = 1;--blad - nie znalazl zadnej maszyny
    else gapfound = 0;
    cnt = cnt + 1;
  end
  endtime = :starttime + :worktime;
  machine = null;
end^
SET TERM ; ^
