--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RP_DESYNC(
      TABELA varchar(31) CHARACTER SET UTF8                           ,
      KEY1 varchar(65) CHARACTER SET UTF8                           ,
      KEY2 varchar(65) CHARACTER SET UTF8                           ,
      KEY3 varchar(65) CHARACTER SET UTF8                           ,
      KEY4 varchar(65) CHARACTER SET UTF8                           ,
      KEY5 varchar(65) CHARACTER SET UTF8                           ,
      STATE integer)
   as
declare variable il integer;
declare variable slocat varchar(255);
declare variable locationbit varchar(255) ;
declare variable fromstate integer;
declare variable fromstate2 integer;
declare variable oldstate integer;
begin
  fromstate = 0;
  if (state<-10) then
  begin
    --state stary zostal nadpisany przez UPDATE do wartosci ujemnej
    -- - wskazanie pojedynczej lokalizacji kasujacej rekord - to robi
    -- replikator lub celowo (np. DOKUMNAG, NAGFAK, by nie skasowac przez
    -- przypadek nigdy z lokalizacji z ktorej pochodzi ten dokument)
    fromstate = 0 - state - 10;
    state = null;
  end
  fromstate2 = 0;
  oldstate = 0;
  if (state>65535) then
  begin
    --na starszej czesci bitu sa zakodowane lokalizacje,
    -- gdzie nie propagowac skasowania - replikator starsza wersja
    -- lub recznie z poziomu trigera.

    --przesuniecie 16 bitow w prawo
    fromstate2 = state / 65536;
    --tylko mlodsza czesc state - czyli wartosc rzeczywista
    state = bin_and(state, 65535);
    --lokalizacje xrodlowe to suma lokalizacji
    fromstate = bin_or(:fromstate, :fromstate2);
  end
  if (state>0) then
    oldstate = state;
  -- if(:state is null) then -- tu chyba wicznie tkwiacy blad - zawsze
  -- powinien podczytac wszystkie lokalizacje, gdzie to moglo trafic.

  -- badanie, do jakich lokalizacji ten rekord mogl kiedykolwiek
  -- trafic - zgodnie z regulami kierunkow replikacji
  execute procedure RP_CHECK_STATE(tabela, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)
    returning_values state;

  if (state is null or state = 0) then
     exit;

  state = bin_or(:state, :oldstate);
  if (slocat<>'') then
  begin
    --wyciecie z celu wlasnej lokalizacji
    execute procedure GET_CONFIG('AKTULOCATBIT', 2)
      returning_values locationbit;
    state = cast(slocat as integer);
    if ((state/cast(locationbit as integer)/2*2) <> (state/cast(locationbit as integer))) then
      state = state - cast(locationbit as integer);
  end
  if (fromstate>0) then
  begin
    --wyciecie lokalizacji usuwajacej rekord
     fromstate = bin_and(:state,:fromstate);
     state = state - fromstate;
  end

  if (state<=0) then
    exit;

  if (key1<>'' and key2<>'' and key3<>'' and key4<>'' and key5<>'') then
  begin
    if (exists(select ref from LOG_FILE  where TABLE_NAME=:TABELA
        and KEY1=:KEY1 and KEY2=:KEY2 and KEY3=:KEY3 and KEY4=:KEY4 and KEY5=:key5)) then
      update LOG_FILE set TIME_STAMP=CURRENT_TIMESTAMP(0), STATE = :state
        where TABLE_NAME=:tabela and KEY1=:key1 and KEY2=:key2 and KEY3=:key3 and KEY4=:key4;
    else
      insert into LOG_FILE (TABLE_NAME, TIME_STAMP, KEY1, KEY2, KEY3, KEY4, KEY5, STATE)
        values (:TABELA, CURRENT_TIMESTAMP(0), :key1, :key2, :key3, :key4, :key5, :state);
  end else if (key1<>'' and key2<>'' and key3<>'' and key4<>'') then
  begin
    if (exists(select ref from LOG_FILE  where TABLE_NAME=:TABELA
        and KEY1=:KEY1 and KEY2=:KEY2 and KEY3=:KEY3 and KEY4=:KEY4)) then
      update LOG_FILE set TIME_STAMP=CURRENT_TIMESTAMP(0), STATE = :state
        where TABLE_NAME=:tabela and KEY1=:key1 and KEY2=:key2 and KEY3=:key3;
    else
      insert into LOG_FILE (TABLE_NAME, TIME_STAMP, KEY1, KEY2, KEY3, KEY4, STATE)
        values (:TABELA, CURRENT_TIMESTAMP(0), :key1, :key2, :key3, :key4, :state);
  end else if (key1<>'' and key2<>'' and key3<>'') then
  begin
    if (exists(select ref from LOG_FILE where TABLE_NAME=:TABELA and KEY1=:KEY1
        and KEY2=:KEY2 and KEY3=:KEY3)) then
      update LOG_FILE set TIME_STAMP=CURRENT_TIMESTAMP(0), STATE = :state
        where TABLE_NAME=:tabela and KEY1=:key1 and KEY2=:key2 and KEY3=:key3;
    else
      insert into LOG_FILE (TABLE_NAME, TIME_STAMP, KEY1, KEY2, KEY3, STATE)
        values (:TABELA, CURRENT_TIMESTAMP(0), :key1, :key2, :key3, :state);
  end else if (key1<>'' and key2<>'') then
  begin
    if (exists(select ref from LOG_FILE where TABLE_NAME=:TABELA and KEY1=:KEY1
        and KEY2=:KEY2)) then
      update LOG_FILE set TIME_STAMP=CURRENT_TIMESTAMP(0), STATE=:state
        where TABLE_NAME=:tabela and KEY1=:key1 and KEY2=:key2;
    else
      insert into LOG_FILE (TABLE_NAME, TIME_STAMP, KEY1, KEY2, STATE)
        values (:TABELA, CURRENT_TIMESTAMP(0), :key1, :key2, :state);
  end else if (key1<>'') then
  begin
    if (exists(select ref from LOG_FILE where TABLE_NAME=:TABELA and KEY1=:KEY1)) then
      update LOG_FILE set TIME_STAMP=CURRENT_TIMESTAMP(0), STATE=:state
        where TABLE_NAME=:tabela and KEY1=:key1;
    else
      insert into LOG_FILE (TABLE_NAME, TIME_STAMP, KEY1, state)
        values (:TABELA, CURRENT_TIMESTAMP(0), :key1, :state);
  end else
    exception RP_DESYNC_WRONG_KEY;
end^
SET TERM ; ^
