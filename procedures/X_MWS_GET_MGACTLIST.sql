--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_GET_MGACTLIST(
      ORDREF integer)
  returns (
      MWSCONSTLOCLS STRING20,
      QUANTITY ILOSCI_MAG,
      JEDN JEDN_MIARY,
      NAME STRING255,
      GOOD KTM_ID,
      VERS integer,
      WEIGHT NUMERIC_14_4,
      REF integer,
      SYMBOL STRING20,
      QUANTITYMAX ILOSCI_MAG,
      POSSIBLEQUAN ILOSCI_MAG,
      LOTSYMB STRING120,
      X_PARTIA STRING20,
      X_SERIAL_NO INTEGER_ID,
      X_SLOWNIK INTEGER_ID,
      LOT INTEGER_ID,
      X_SERIAL_STR STRING,
      X_SLOWNIK_STR STRING,
      MWSORDTYPE integer)
   as
declare variable mwsconstlocp integer_id;
declare variable mwspallocp integer_id;
begin

  for
    select distinct m.symbol as mwsconstlocls, sum(st.QUANTITY) as QUANTITY, j.jedn,
        max(coalesce(t.mwsnazwa, t.nazwa)||' '||we.nazwa) as name,
        st.GOOD as GOOD, st.vers, sum(st.weight) as weight,
        max(st.ref) as ref, mo.symbol, max(st.quantityp) as quantitymax,
        /*max((select sum(a1.quantity) from mwsacts a1
                where a1.vers = st.vers and a1.mwsord = st.mwsord and a1.mwsconstlocp = st.mwsconstlocp
                    and a1.mwspallocp = st.mwspallocp
                    and a1.x_partia =
                    and a1.x_serial_no = st.x_serial_no)) as possiblequan, */
        d.ref as LOT, d.symbol as LOTSYMB ,-- d.datawazn as EXPDATE,
        st.x_partia, st.x_serial_no, st.x_slownik,
        st.mwsconstlocp, st.mwspallocp,
        mo.mwsordtype
      from mwsacts st
        join mwsords mo on (mo.ref = st.mwsord)
        left join towary t on(st.good = t.ktm)
        left join mwsconstlocs m on(m.ref = st.mwsconstlocl)
        left join wersje we on(we.ref = st.vers)
        left join towjedn j on(j.ktm = t.ktm and j.glowna = 1)
        left join dostawy d on (st.lot = d.ref)
      where st.mwsord =  :ordref
        and st.status < 3
        and (st.mwsconstlocp <> st.mwsconstlocl or st.mwsconstlocl is null)
      group by st.GOOD,m.symbol, st.vers,j.jedn, mo.symbol, st.mwsconstlocp, st.mwsconstlocl,
          st.mwspallocp, st.mwspallocl, d.ref, d.symbol,/* d.datawazn, */ st.x_partia, st.x_serial_no, st.x_slownik, mo.mwsordtype
    into :mwsconstlocls, :quantity, :jedn,
        :name,
        :good, :vers, :weight,
        :ref, :symbol, :quantitymax,
        --:possiblequan,
        :lot,  :lotsymb,
        :x_partia, :x_serial_no, :x_slownik,
        :mwsconstlocp, :mwspallocp,
        :mwsordtype
  do begin
    select sum(a1.quantity) - sum(a1.quantityc)
      from mwsacts a1
        where a1.vers = :vers
          and a1.mwsord = :ordref
          and a1.mwsconstlocp = :mwsconstlocp
          and a1.mwspallocp = :mwspallocp
          and a1.status in (0,1,2)
          and (:x_partia is null or a1.x_partia = :x_partia)
          and (:x_serial_no is null or a1.x_serial_no = :x_serial_no)
          and (:x_slownik is null or a1.x_slownik = :x_slownik)
     into :possiblequan;

     if (coalesce(x_serial_no,0) <> 0) then
      select s.serialno from X_MWS_SERIE s where s.ref = :x_serial_no into :x_serial_str;

    suspend;
    possiblequan = null;

  end
end^
SET TERM ; ^
