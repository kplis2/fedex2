--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_VAT7_17_POZ(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PAR_K25 NUMERIC_14_2,
      PAR_K26 NUMERIC_14_2,
      PAR_K37 NUMERIC_14_2,
      PAR_K39 NUMERIC_14_2,
      PAR_K47 NUMERIC_14_2,
      PAR_K52 NUMERIC_14_2,
      PAR_K55 NUMERIC_14_2,
      PAR_K58 NUMERIC_14_2,
      PAR_K59 NUMERIC_14_2,
      PAR_K60 NUMERIC_14_2,
      PAR_K62 SMALLINT_ID,
      PAR_K63 SMALLINT_ID,
      PAR_K64 SMALLINT_ID,
      PAR_K65 SMALLINT_ID,
      PAR_OZ SMALLINT_ID,
      PAR_OP SMALLINT_ID,
      PAR_K68 SMALLINT_ID,
      PAR_K69 INTEGER_ID,
      DOCDATE STRING255,
      COMPANY INTEGER_ID,
      CORRECT SMALLINT_ID,
      USERFNAME STRING255,
      USERSNAME STRING255,
      EDECLARATIONREF EDECLARATIONS_ID,
      EDECPOSFIELD EDECLPOS_ID)
  returns (
      OUTEDECPOSFIELD EDECLPOS_ID)
   as
declare variable k10 integer_id;
declare variable k11 integer_id;
declare variable k12 integer_id;
declare variable k13 integer_id;
declare variable k14 integer_id;
declare variable k15 integer_id;
declare variable k16 integer_id;
declare variable k17 integer_id;
declare variable k18 integer_id;
declare variable k19 integer_id;
declare variable k20 integer_id;
declare variable k21 integer_id;
declare variable k22 integer_id;
declare variable k23 integer_id;
declare variable k24 integer_id;
declare variable k25 integer_id;
declare variable k26 integer_id;
declare variable k27 integer_id;
declare variable k28 integer_id;
declare variable k29 integer_id;
declare variable k30 integer_id;
declare variable k31 integer_id;
declare variable k32 integer_id;
declare variable k33 integer_id;
declare variable k34 integer_id;
declare variable k35 integer_id;
declare variable k36 integer_id;
declare variable k37 integer_id;
declare variable k38 integer_id;
declare variable k39 integer_id;
declare variable k40 integer_id;
declare variable k41 integer_id;
declare variable k42 integer_id;
declare variable k43 integer_id;
declare variable k44 integer_id;
declare variable k45 integer_id;
declare variable k46 integer_id;
declare variable k47 integer_id;
declare variable k48 integer_id;
declare variable k49 integer_id;
declare variable k50 integer_id;
declare variable k51 integer_id;
declare variable k52 integer_id;
declare variable k53 integer_id;
declare variable k54 integer_id;
declare variable k55 integer_id;
declare variable k56 integer_id;
declare variable k57 integer_id;
declare variable k58 integer_id;
declare variable k59 integer_id;
declare variable k60 integer_id;
declare variable k61 integer_id;
declare variable k62 smallint_id;
declare variable k63 smallint_id;
declare variable k64 smallint_id;
declare variable k65 smallint_id;
declare variable k66 smallint_id;
declare variable k67 smallint_id;
declare variable k68 smallint_id;
declare variable k69 integer_id;
declare variable field_root_prefix char_1;
begin
/*TS: FK - generuje pozycje dla e-Deklaracji dla wskazanej sekcji dokumentu.

  > EDECLARATIONREF - numer generowanej deklaracji z procedury nadrzednej (ta
    sama nazwa, bez '_POZ'), wartosc pola EDECLARATIONS.REF,
  > EDECPOSFIELD - pierwszy w kolejnosci wolny numer pola z tabeli EDECLPOS.
    Procedura nadrzedna wywoluje kolejno ta procedure dla wszystkich sekcji
    wydruku o zmiennej liczbie pozycji i kazda kolejna generuje kolejno numerowane pola.
*/

  -- Ustawiamy prefixy pol, ktore beda dodawane do EDECLPOS.
  field_root_prefix = 'K';

  select d.k10, d.k11, d.k12, d.k13, d.k14, d.k15, d.k16, d.k17, d.k18,
    d.k19, d.k20, d.k21, d.k22, d.k23, d.k24, d.k25, d.k26, d.k27, d.k28,
    d.k29, d.k30, d.k31, d.k32, d.k33, d.k34, d.k35, d.k36, d.k37, d.k38,
    d.k39, d.k40, d.k41, d.k42, d.k43, d.k44, d.k45, d.k46, d.k47, d.k48,
    d.k49, d.k50, d.k51, d.k52, d.k53, d.k54, d.k55, d.k56, d.k57, d.k58,
    d.k59, d.k60, d.k61, d.k62, d.k63, d.k64,  d.k65, d.k66, d.k67, d.k68,
    d.k69
  from get_data_vat7_17(:period, :par_k25, :par_k26, :par_k37, :par_k39,
    :par_k47, :par_k52, :par_k55, :par_k58, :par_k59, :par_k60, :par_k62,
    :par_k63, :par_k64, :par_k65, :par_oz, :par_op, :par_k68, :par_k69,
    :docdate, :company, :correct, :userfname, :usersname) d
  into k10, k11, k12, k13, k14, k15, k16, k17, k18,
    k19, k20, k21, k22, k23, k24, k25, k26, k27, k28,
    k29, k30, k31, k32, k33, k34, k35, k36, k37, k38,
    k39, k40, k41, k42, k43, k44, k45, k46, k47, k48,
    k49, k50, k51, k52, k53, k54, k55, k56, k57, k58,
    k59, k60, k61, k62, k63, k64,  k65, k66, k67, k68,
    k69;

    /* Symbole pol sa formatu XPOZ_Y.Z, gdzie:
     > X to nazwa sekcji wydruku,
     > Y to numer wiersza w danej sekcji,
     > Z to nazwa pola danego wiersza.
    */
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 0, :k10, 0, :field_root_prefix||'10');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 1, :k11, 0, :field_root_prefix||'11');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 2, :k12, 0, :field_root_prefix||'12');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 3, :k13, 0, :field_root_prefix||'13');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 4, :k14, 0, :field_root_prefix||'14');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 5, :k15, 0, :field_root_prefix||'15');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 6, :k16, 0, :field_root_prefix||'16');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 7, :k17, 0, :field_root_prefix||'17');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 8, :k18, 0, :field_root_prefix||'18');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 9, :k19, 0, :field_root_prefix||'19');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 10, :k20, 0, :field_root_prefix||'20');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 11, :k21, 0, :field_root_prefix||'21');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 12, :k22, 0, :field_root_prefix||'22');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 13, :k23, 0, :field_root_prefix||'23');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 14, :k24, 0, :field_root_prefix||'24');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 15, :k25, 0, :field_root_prefix||'25');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 16, :k26, 0, :field_root_prefix||'26');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 17, :k27, 0, :field_root_prefix||'27');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 18, :k28, 0, :field_root_prefix||'28');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 19, :k29, 0, :field_root_prefix||'29');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 20, :k30, 0, :field_root_prefix||'30');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 21, :k31, 0, :field_root_prefix||'31');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 22, :k32, 0, :field_root_prefix||'32');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 23, :k33, 0, :field_root_prefix||'33');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 24, :k34, 0, :field_root_prefix||'34');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 25, :k35, 0, :field_root_prefix||'35');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 26, :k36, 0, :field_root_prefix||'36');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 27, :k37, 0, :field_root_prefix||'37');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 28, :k38, 0, :field_root_prefix||'38');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 29, :k39, 0, :field_root_prefix||'39');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 30, :k40, 0, :field_root_prefix||'40');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 31, :k41, 0, :field_root_prefix||'41');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 32, :k42, 0, :field_root_prefix||'42');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 33, :k43, 0, :field_root_prefix||'43');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 34, :k44, 0, :field_root_prefix||'44');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 35, :k45, 0, :field_root_prefix||'45');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 36, :k46, 0, :field_root_prefix||'46');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 37, :k47, 0, :field_root_prefix||'47');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 38, :k48, 0, :field_root_prefix||'48');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 39, :k49, 0, :field_root_prefix||'49');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 40, :k50, 0, :field_root_prefix||'50');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 41, :k51, 0, :field_root_prefix||'51');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 42, :k52, 0, :field_root_prefix||'52');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 43, :k53, 0, :field_root_prefix||'53');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 44, :k54, 0, :field_root_prefix||'54');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 45, :k55, 0, :field_root_prefix||'55');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 46, :k56, 0, :field_root_prefix||'56');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 47, :k57, 0, :field_root_prefix||'57');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 48, :k58, 0, :field_root_prefix||'58');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 49, :k59, 0, :field_root_prefix||'59');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 50, :k60, 0, :field_root_prefix||'60');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 51, :k61, 0, :field_root_prefix||'61');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 52, :k62, 0, :field_root_prefix||'62');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 53, :k63, 0, :field_root_prefix||'63');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 54, :k64, 0, :field_root_prefix||'64');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 55, :k65, 0, :field_root_prefix||'65');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 56, :k66, 0, :field_root_prefix||'66');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 57, :k67, 0, :field_root_prefix||'67');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 58, :k68, 0, :field_root_prefix||'68');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, :edecposfield + 59, :k69, 0, :field_root_prefix||'69');

  outedecposfield = :edecposfield + 60;
  suspend;
end^
SET TERM ; ^
