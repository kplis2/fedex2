--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGZAM_CHECKWYS(
      ZAM integer)
   as
declare variable wyslane integer;
declare variable pref integer;
declare variable ilosc numeric(14,4);
declare variable ilsped numeric(14,4);
declare variable newilsped numeric(14,4);
begin
  wyslane = 1;
  for select ref, ilosc, coalesce(ilsped,0) from POZZAM
    where ZAMOWIENIE=:zam
    into :pref, :ilosc, :ilsped
  do begin
    select coalesce(sum(ILOSC),0) from LISTYWYSDPOZ where POZZAM=:pref into :newilsped;
    if(:ilsped<>:newilsped) then update POZZAM set ILSPED=:newilsped where ref=:pref;
    if(:ilosc<>:newilsped) then wyslane = 0;
  end
  update NAGZAM set WYSYLKADOD = :wyslane where ref=:zam and WYSYLKADOD <> :wyslane;
end^
SET TERM ; ^
