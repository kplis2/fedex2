--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_RECALC_MWSFREEMWSCONSTLOC as
declare variable mwsconstloc integer;
declare variable act smallint;
begin
  for
    select ref, act from mwsconstlocs
      into mwsconstloc, act
  do begin
    execute procedure MWS_CALC_FREELOCS (:mwsconstloc,:act);
  end
end^
SET TERM ; ^
