--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_CENNIK_ZDOK(
      CENAZAK numeric(14,2),
      WERSJAREFF integer)
  returns (
      CENNIK integer,
      WERSJAREF integer,
      JEDN integer,
      CENANET numeric(14,2),
      CENABRU numeric(14,2),
      WALUTA varchar(3) CHARACTER SET UTF8                           ,
      MARZA numeric(14,2),
      NARZUT numeric(14,2),
      NAZWACENNIKA varchar(30) CHARACTER SET UTF8                           ,
      CENAZDOK numeric(14,2),
      OLDMARZA numeric(12,2),
      OLDNARZUT numeric(14,2))
   as
begin
  for select CENNIK.CENNIK, CENNIK.WERSJAREF, CENNIK.jedn,  CENNIK.CENANET, CENNIK.CENABRU,
  CENNIK.waluta,  DEFCENNIK.nazwa, cennik.marza, cennik.narzut
  from CENNIK join DEFCENNIK on (DEFCENNIK.ref = CENNIk.CENNIK)
  where WERSJAREF = :wersjareff
  into :cennik, :wersjaref, :jedn, :cenanet, :cenabru, :waluta, :nazwacennika, :oldmarza, :oldnarzut
  do begin
    marza = 0;
    narzut = 100;
    if(:cenanet > 0) then
      marza = (:cenanet - :cenazak)/:cenanet * 100;
    if(:cenazak > 0)
      then NARZUT = (:CENANET - :cenazak) * 100 / :cenazak;
    cenazdok = :cenazak;
    suspend;
  end
end^
SET TERM ; ^
