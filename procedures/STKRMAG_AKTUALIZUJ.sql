--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STKRMAG_AKTUALIZUJ(
      MAGAZYN varchar(3) CHARACTER SET UTF8                           )
  returns (
      STATUS integer,
      NUMROWS integer)
   as
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable stanmin numeric(14,4);
declare variable stanmax numeric(14,4);
declare variable oldstanmin numeric(14,4);
declare variable oldstanmax numeric(14,4);
begin
  numrows = 0;
  for select KTM, WERSJA, STANYIL.stanmin, stanmax
  from STANYIl where MAGAZYN = :magazyn
  into :ktm, :wersja, :oldstanmin, :oldstanmax
  do begin
    stanmin = null;
    stanmax = null;
    execute procedure STKRTAB_GETMINMAX(:magazyn, :ktm, :wersja) returning_values :stanmin, :stanmax;
    if(  (:stanmin <> :oldstanmin) or (:stanmin is null and :oldstanmin is not null) or (:stanmin is not null and :oldstanmin is null)
      or (:stanmax <> :oldstanmax) or (:stanmax is null and :oldstanmax is not null) or (:stanmax is not null and :oldstanmax is null)
     ) then begin
        update STANYIL set STANMIN = :stanmin, stanmax = :stanmax where MAGAZYN = :magazyn and ktm = :ktm and WERSJA = :wersja;
        numrows = :numrows + 1;
     end
  end
  update DEFMAGAZ set lastminmaxaktu = current_time where SYMBOL = :magazyn;
  status = 1;
end^
SET TERM ; ^
