--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_CHECK_PRORD_DEPEND(
      PRPOZZAM integer,
      PRNAGZAM integer)
  returns (
      STATUS smallint)
   as
begin
  status = 0;
  select first 1 1
    from PR_POZZAM_CASCADE(:prpozzam,0,0,0,1) pr
    where pr.ref <> :prpozzam
    into status;
  if (status is null) then status = 0;
end^
SET TERM ; ^
