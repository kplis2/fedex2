--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHEETS_COPY1(
      PRSHEETREFIN integer,
      PRSHEETREFOUT integer,
      KTM varchar(34) CHARACTER SET UTF8                           ,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      OPEROLDINREF integer,
      OPERNEWINREF integer,
      MODE smallint)
  returns (
      PRSHEETOUT integer)
   as
declare variable operoldref integer;
declare variable opernewref integer;
declare variable opermasterref integer;
declare variable jedn integer;
declare variable operdown integer;
declare variable operdefoult integer;
begin
 -- exception test_break 'dzwon';
  if(operoldinref = 0) then operoldinref = null;
  if(opernewinref = 0) then opernewinref = null;
  /*zakladanie naglowka karty tylko jesli nie jest to dodanie operacji podrzednej*/
  if(operoldinref is null) then
  begin
    execute procedure gen_ref('PRSHEETS') returning_values :prsheetout;
    jedn = null;
    select ref
      from towjedn
      where ktm = :ktm and jedn = (select t.jedn from towjedn t where t.ref = (select p.jedn from prsheets p where p.ref = :prsheetrefin))
      into :jedn;
     if (:mode = 2) then -- nowa wersja karty technologicznej
     begin
      insert into prsheets(ref, shtype, symbol, DESCRIPT, ktm,
                 jedn,  quantity, status, accoper, realtime, shortage,
                 prdepart, batchquantity, techjednquantity, warehouse, NOTE, PARAMN1,
                 PARAMN2, PARAMN3, PARAMN4, PARAMN5, PARAMS1, PARAMS2, PARAMS3, PARAMS4,
                 PARAMS5,
                 AUTODOCIN, AMOUNTINFROM)
      select :prsheetout,  shtype, :symbol, DESCRIPT, :ktm,
                 :jedn,  quantity, 0, accoper, realtime, shortage,
                 prdepart, batchquantity, techjednquantity, warehouse, NOTE, PARAMN1,
                 PARAMN2, PARAMN3, PARAMN4, PARAMN5, PARAMS1, PARAMS2, PARAMS3, PARAMS4,
                 PARAMS5,
                 AUTODOCIN, AMOUNTINFROM
        from prsheets where ref =:prsheetrefin;
      end
      else --kopiowanie karty technologicznej
      begin
        insert into prsheets(ref, shtype, symbol, symbolver, DESCRIPT, ktm,
                 jedn,  quantity, status, accoper, realtime, shortage,
                 prdepart, batchquantity, techjednquantity, warehouse, NOTE, PARAMN1,
                 PARAMN2, PARAMN3, PARAMN4, PARAMN5, PARAMS1, PARAMS2, PARAMS3, PARAMS4,
                 PARAMS5,
                 AUTODOCIN, AMOUNTINFROM)
        select :prsheetout,  shtype, :symbol, '1.0', DESCRIPT, :ktm,
                 :jedn,  quantity, 0, accoper, realtime, shortage,
                 prdepart, batchquantity, techjednquantity, warehouse, NOTE, PARAMN1,
                 PARAMN2, PARAMN3, PARAMN4, PARAMN5, PARAMS1, PARAMS2, PARAMS3, PARAMS4,
                 PARAMS5,
                 AUTODOCIN, AMOUNTINFROM
          from prsheets where ref =:prsheetrefin;
      end
  end else begin
    prsheetout = prsheetrefout;
  end
  for select ref
    from prshopers
    where sheet = :prsheetrefin and (:operoldinref is null  and opermaster is null) or
      (:operoldinref is not null and opermaster = :operoldinref)
    into :operoldref
  do begin
    ---operacje
    execute procedure gen_ref('PRSHOPERS') returning_values opernewref;
    insert into prshopers(ref, sheet, opermaster, number, descript,  econdition,
                        condition, oper, eoperfactor, operfactor, eopertime,
                        opertime, workplace, eplacetime, placetime,
                        complex, ord, OPERTYPE, SCHEDULED, CALCULATED, REPORTED,
                        ORDANALIZE, HOURRATE, ECONTRACTSDEF, NOTE, PRQCONTROLMETH,
                        PRQCONTROLINTERVAL, REPORTEDFACTOR, PARAMN1, PARAMN2, PARAMN3, PARAMN4,
                        PARAMN5, PARAMS1, PARAMS2, PARAMS3, PARAMS4, PARAMS5, REPORTEDFACTORTYPE,
                        OPERDEFAULT, COPYFROMPRSHOPER,
                        externaloper, service, amountinfrom, opertimepf, serial, SERIALNUMBERGEN, PRSHOPER_CLONE,
                        prmachine)  ---- na worka
      select  :opernewref ,:prsheetout, :opernewinref, number, descript,  econdition,
                        condition, oper, eoperfactor, operfactor, eopertime,
                        opertime, workplace, eplacetime, placetime,
                        complex, 1, OPERTYPE, SCHEDULED, CALCULATED, REPORTED,
                        ORDANALIZE, HOURRATE, ECONTRACTSDEF, NOTE, PRQCONTROLMETH,
                        PRQCONTROLINTERVAL, REPORTEDFACTOR, PARAMN1, PARAMN2, PARAMN3, PARAMN4,
                        PARAMN5, PARAMS1, PARAMS2, PARAMS3, PARAMS4, PARAMS5, REPORTEDFACTORTYPE,
                        OPERDEFAULT, ref,
                        externaloper, service, amountinfrom, opertimepf, serial, SERIALNUMBERGEN, 1,
                        prmachine  ---na worka
      from prshopers
      where ref =:operoldref
      order by number;
    update prshopers set PRSHOPER_CLONE = 0 where ref = :opernewref;
    ---- materiay
    insert into prshmat(sheet, opermaster, number,  descript, mattype,
                      KTM, wersja, wersjaref, jedn, enetquantity, netquantity,
                      egrossquantity, grossquantity, econdition, condition,
                      subsheet, ord, supplier, warehouse,
                      REQKTMTAG, STOPCASCADE, AUTODOCOUT, UNREPORTED )
      select  :prsheetout, :opernewref, number,  descript, mattype,
                      KTM, wersja, wersjaref, jedn, enetquantity, netquantity,
                      egrossquantity, grossquantity, econdition, condition,
                      subsheet, ord, supplier, warehouse,
                      REQKTMTAG, STOPCASCADE, AUTODOCOUT, UNREPORTED
        from prshmat
        where sheet = :prsheetrefin and opermaster = :operoldref
        order by prshmat.number;
    ----- narzdzia
    insert into prshtools(sheet, opermaster, symbol, TOOLTYPE, econdition, condition,
                       quantity, ehourfactor, hourfactor, esheetrate, sheetrate,
                       DESCRIPT, LONGDESCRIPT, PARAMS1, PARAMS2, PARAMS3, PARAMS4, PARAMS5,
                       PARAMS6, PRTOOLSTYPENAME)
      select  :prsheetout, :opernewref, symbol, TOOLTYPE, econdition, condition,
                       quantity, ehourfactor, hourfactor, esheetrate, sheetrate,
                       DESCRIPT, LONGDESCRIPT, PARAMS1, PARAMS2, PARAMS3, PARAMS4, PARAMS5,
                       PARAMS6, PRTOOLSTYPENAME
        from prshtools
        where sheet = :prsheetrefin and opermaster = :operoldref;
    ------operacje RCP
    insert into prshopersecontrdefs (prshoper, econtractsdef,econtractsposdef, hourrate)
    select  :opernewref, econtractsdef,econtractsposdef, hourrate
      from prshopersecontrdefs p where p.prshoper = :operoldref
      and not exists(select first 1 1 from prshopersecontrdefs d where d.prshoper = p.prshoper
                and d.econtractsdef = p.econtractsdef and d.econtractsposdef = p.econtractsposdef);
    -----stanowiska
    insert into prshopersworkplaces(prshoper , prworkplace ,prmachine , opertimepf ,opertime, main, prshoper_clone)
    select  :opernewref , prworkplace ,prmachine , opertimepf ,opertime,main, 1
      from prshopersworkplaces where prshoper = :operoldref order by main desc;
    update prshopersworkplaces set prshoper_clone = 0 where prshoper = :opernewref;
    -----kontrola jakosci
    insert into prshmeasuretools (prshoper, prtooltype, amount )
    select  :opernewref , pm.prtooltype, pm.amount
      from prshmeasuretools pm where pm.prshoper = :operoldref;

    execute procedure PRSHEETS_COPY1(:prsheetrefin, :prsheetout, :ktm, :symbol, :operoldref, :opernewref, :mode)
      returning_values :prsheetout;
  end
  -- na koniec trzeba pozamieniac operacje domyslne, bo zmienily sie refy
  if(operoldinref is null) then
  begin
    for
      select o.ref, o.operdefault
        from prshopers o
        where o.sheet = :prsheetout and o.operdefault is not null
        into opernewref, operdefoult
    do begin
      update prshopers o set o.operdefault = (select d.ref from prshopers d where d.sheet = :prsheetout and d.copyfromprshoper = :operdefoult)
        where o.ref = :opernewref;
    end
  end
end^
SET TERM ; ^
