--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_LISTYWYSDTOSRV(
      SRV integer)
  returns (
      REF integer,
      AKCEPTACJA smallint,
      SYMBOL varchar(255) CHARACTER SET UTF8                           ,
      WARTOSC numeric(14,2),
      KINDSTR varchar(20) CHARACTER SET UTF8                           )
   as
declare variable POZFAK_REF integer;
declare variable GRUPASPED integer;
begin
  for select distinct l.ref, l.akcept, substring(l.symbol||' ('||l.symbolsped||')' from 1 for 255)
       from listywysd l
         join listywysdpoz p on p.dokument = l.ref
       where (l.refsrv = :srv or (p.dokref = :srv) )and l.typ='R'
  into :ref, :akceptacja, :symbol
  do begin
    if(:akceptacja=0) then kindstr = 'MI_REDX';
    else if(:akceptacja=1) then kindstr = '14';
    else if(:akceptacja=2) then kindstr = 'MI_PADLOCK';
    suspend;
  end
end^
SET TERM ; ^
