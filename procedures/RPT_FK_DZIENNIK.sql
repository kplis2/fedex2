--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_DZIENNIK(
      COMPANY integer,
      ODOKRESU varchar(6) CHARACTER SET UTF8                           ,
      DOOKRESU varchar(6) CHARACTER SET UTF8                           ,
      REJESTR varchar(10) CHARACTER SET UTF8                           ,
      ODSTRONY integer,
      ILESTRON integer)
  returns (
      BKREG varchar(10) CHARACTER SET UTF8                           ,
      NLNUMBER integer,
      NUMBER integer,
      TRANSDATE timestamp,
      DOCDATE timestamp,
      REF integer,
      NAZWA varchar(60) CHARACTER SET UTF8                           ,
      NAME varchar(60) CHARACTER SET UTF8                           ,
      ODDZIAL varchar(10) CHARACTER SET UTF8                           ,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      SUMDEBIT numeric(14,2),
      SUMCREDIT numeric(14,2),
      SUMDEBITS numeric(14,2),
      SUMCREDITS numeric(14,2),
      SUMDEBITC numeric(14,2),
      SUMCREDITC numeric(14,2),
      ZPRZEN_DEBIT numeric(14,2),
      ZPRZEN_CREDIT numeric(14,2),
      NAWASTRONA smallint,
      DESCRIPT varchar(80) CHARACTER SET UTF8                           ,
      DEBIT numeric(14,2),
      CREDIT numeric(14,2),
      DNUMBER smallint,
      ACCOUNT varchar(80) CHARACTER SET UTF8                           ,
      PODSUMSTR smallint,
      PODSUMDEK smallint,
      STRONA integer,
      SUMASTRON integer,
      NRLINII integer,
      LINIA integer)
   as
begin

  select max(strona) from rpt_fk_dziennik_lines(:company, :odokresu, :dookresu, :rejestr)
    into :sumastron;

  for select BKREG, NLNUMBER, NUMBER, TRANSDATE, DOCDATE, REF, NAZWA, NAME, ODDZIAL,
    SYMBOL, SUMDEBIT, SUMCREDIT, SUMDEBITS, SUMCREDITS, SUMDEBITC, SUMCREDITC,
    ZPRZEN_DEBIT, ZPRZEN_CREDIT, NAWASTRONA, DESCRIPT, DEBIT, CREDIT, DNUMBER,
    ACCOUNT, PODSUMSTR, PODSUMDEK, STRONA, NRLINII, LINIA from rpt_fk_dziennik_lines(:company, :odokresu, :dookresu, :rejestr)
    into :BKREG, :NLNUMBER, :NUMBER, :TRANSDATE, :DOCDATE, :REF, :NAZWA, :NAME, :ODDZIAL,
         :SYMBOL, :SUMDEBIT, :SUMCREDIT, :SUMDEBITS, :SUMCREDITS, :SUMDEBITC, :SUMCREDITC,
         :ZPRZEN_DEBIT, :ZPRZEN_CREDIT, :NAWASTRONA, :DESCRIPT, :DEBIT, :CREDIT, :DNUMBER,
         :ACCOUNT, :PODSUMSTR, :PODSUMDEK, :STRONA, :NRLINII, :LINIA
  do begin
    if (strona>=odstrony and (strona<odstrony+ilestron or (ilestron = 0)) ) then
      suspend;
  end

end^
SET TERM ; ^
