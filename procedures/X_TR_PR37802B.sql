--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_TR_PR37802B as
declare variable maxnum integer;
begin
  maxnum = 0;
  select max(numer)+1 from konfig where grupa = 'Y'
  into :maxnum;
  INSERT INTO KONFIG (AKRONIM, NAZWA, WARTOSC, GRUPA, NUMER, TYP, WYSOKOSC, DICTBASE, DICTTABLE, DICTFIELD,
        DICTINDEX, DICTPREFIX, DICTFILTER, DICTKEYFIELDS, MULTI, ORD, STATE, TOKEN, ISREPLICATE)
    VALUES ('LISTYWYSDACKCTRL', 'Kontrola akceptacji dokumentów spedycyjnych?', '0', 'Y', :maxnum, 0, NULL, '', '', '',
        '', '', '', '', 0, 1, 0, 9, 0);

  maxnum = :maxnum + 1;
  INSERT INTO KONFIG (AKRONIM, NAZWA, WARTOSC, GRUPA, NUMER, TYP, WYSOKOSC, DICTBASE, DICTTABLE, DICTFIELD,
        DICTINDEX, DICTPREFIX, DICTFILTER, DICTKEYFIELDS, MULTI, ORD, STATE, TOKEN, ISREPLICATE)
    VALUES ('LISTYWYSDWAGAOPK', 'Czy liczyć wagę z zamkniętych opakowań?', '0', 'Y', :maxnum, 0, NULL, '', '', '',
        '', '', '', '', 0, 1, 0, 9, 0);

  maxnum = :maxnum + 1;
  INSERT INTO KONFIG (AKRONIM, NAZWA, WARTOSC, GRUPA, NUMER, TYP, WYSOKOSC, DICTBASE, DICTTABLE, DICTFIELD,
        DICTINDEX, DICTPREFIX, DICTFILTER, DICTKEYFIELDS, MULTI, ORD, STATE, TOKEN, ISREPLICATE)
    VALUES ('LISTYWYSAUTONEW', 'Czy zakładać automatycznie zlecenia transportowe?', '0', 'Y', :maxnum, 0, NULL, '', '', '',
        '', '', '', '', 0, 1, 0, 9, 0);
end^
SET TERM ; ^
