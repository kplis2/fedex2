--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EHRM_GET_BACKGROUND(
      EMPLOYEEREF EMPLOYEES_ID,
      FROMDATE TIMESTAMP_ID,
      TODATE TIMESTAMP_ID)
  returns (
      DAYOFMONTH integer,
      PRESENCE smallint,
      DAYDESCRIPTION varchar(255) CHARACTER SET UTF8                           ,
      COLOR varchar(10) CHARACTER SET UTF8                           ,
      ROWDATE date,
      ACCEPTED smallint,
      DOUBLEHEIGHT smallint,
      REF EMPLCALDAYS_ID)
   as
declare variable VACPLANTYPE SMALLINT_ID;
declare variable NAMEABSENCE STRING22;
declare variable TYPEABSENCE VARCHAR_60;
declare variable DAYKIND SMALLINT_ID;
declare variable DAYOFWEEK SMALLINT_ID;
begin
  for select e.ref, e.pday, edk.daytype, e.descript, e.cdate, e.status , p.vtype, c.name , c.typ, ec.dayofweek
    from emplcaldays e
    left join ecaldays ec on e.ecalday = ec.ref
    left join eabsences a on e.absence = a.ref
    left join ecolumns c on c.number = a.ecolumn
    left join evacplan p on p.ref = e.vacplan
    join edaykinds edk on edk.ref = e.daykind
    where e.employee=:employeeref
    and e.cdate>=:fromdate and e.cdate<:todate
    order by e.cdate asc
    into :ref, :dayofmonth, :daykind, :daydescription, :rowdate, :accepted, :vacplantype, :nameabsence, :typeabsence,
      :dayofweek
  do begin
    color = '#FFFFFF';
    presence = 0;
    doubleheight = 0;

    ---  sprawdzenie czy jest zlozony plan badz wniosek urlopowy na dzien
    if(:vacplantype is not null) then begin
        if(:vacplantype = 1) then begin
            presence = 1;
            daydescription = 'wniosek urlopowy';
            color = '#0FBF58';
        end
        else if(:vacplantype = 0) then begin
            presence = 2;
            daydescription = 'plan urlopowy';
            color = '#057233';
        end
    end

    --- sprawdzenie czy w danym dniu byla nieobecnosc.
    if(:typeabsence is not null) then begin
        if(typeabsence = ';CHR;') then begin
            presence = 3;
            daydescription = :nameabsence;
            color = '#7E0889';
        end
        else if (typeabsence = ';URL;') then begin
            presence = 4;
            daydescription = :nameabsence;
            color = '#D238E0';
        end
        else if (typeabsence = ';INNE;') then begin
            presence = 5;
            daydescription = :nameabsence;
            color = '#B387B7';
        end
    end

    --- sprawdzenie czy jest swieto badz weeekend
    if(:daykind = 0) then begin
        presence = 6;
        if( :daydescription = '' or :daydescription is null) then
        begin
        daydescription = case when :dayofweek = 1 then 'Poniedziałek'
                              when :dayofweek = 2 then 'Wtorek'
                              when :dayofweek = 3 then 'Środa'
                              when :dayofweek = 4 then 'Czwartek'
                              when :dayofweek = 5 then 'Piątek'
                              when :dayofweek = 6 then 'Sobota'
                              when :dayofweek = 0 then 'Niedziela'
                              end;
        end
        else daydescription = :daydescription;
       -- daydescription = case when :daydescription = '' or :daydescription is null then 'Niedziela' else :daydescription end;
        color = '#9B8B11';
    end


    --- ustalenie czy podwojny wiersz
    if(exists(select first 1 1
      from epresencelistspos e
      join epresencelists l on (l.ref=e.epresencelists)
      where e.employee=:employeeref and l.listsdate=cast(:rowdate as timestamp)
      and e.state<>0)) then doubleheight = 1;
    suspend;
  end
end^
SET TERM ; ^
