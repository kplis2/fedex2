--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHEETS_SECURITYLOG_COPY(
      NEWSHEETREF integer,
      OLDSHEETREF integer)
   as
begin
  insert into security_log(tablename,TIMESTMP,key1,item,opis, operator)
    select 'PRSHEETS', current_timestamp(0), :newsheetref,log.item, log.opis, case when log.operator is null then -1 else log.operator end
      from security_log log
      where log.tablename = 'PRSHEETS'
        and log.key1 = cast(:oldsheetref as varchar(20));
end^
SET TERM ; ^
