--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_VAT_FOREGIN_IN_PERIOD(
      PERIODID varchar(6) CHARACTER SET UTF8                           ,
      COMPANY integer)
  returns (
      NAG integer,
      REF integer,
      LP integer,
      VATREG varchar(10) CHARACTER SET UTF8                           ,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      BKREG varchar(10) CHARACTER SET UTF8                           ,
      KONTRAHKOD varchar(1150) CHARACTER SET UTF8                           ,
      NIP varchar(40) CHARACTER SET UTF8                           ,
      NETV numeric(14,2),
      VATV numeric(14,2),
      BRUV numeric(14,2),
      TRANSDATE date,
      DOCDATE date,
      NR integer)
   as
declare variable sdate date;
declare variable fdate date;
declare variable okres varchar(6);
begin
  -- PAWEL W REALIZACJI
  -- TO FIX
  ref = 0;
  lp = 0;
  select cast(B.sdate as date), cast(B.fdate as date)
    from bkperiods B
      where B.id = :periodid and B.company = :company
  into :sdate, :fdate;
  for select bkdocs.symbol, bkdocs.bkreg, bkdocs.number, bkdocs.vatreg,
      bkdocs.nip, bkdocs.CONTRACTOR, sum(bkvatpos.netv), sum(bkvatpos.vatvf),
      bkdocs.transdate, bkdocs.docdate
      from bkdocs
      join vatregs on (vatregs.symbol = bkdocs.vatreg
        and vatregs.vtype in (0,1,2) and bkdocs.status > 0 and bkdocs.company= vatregs.company)
      join bkvatpos on (bkvatpos.bkdoc = bkdocs.ref)
    where bkdocs.company = :company
    and vatregs.vtype in (1,2)
    and ((bkdocs.vatexpconfirm >= :sdate and bkdocs.vatexpconfirm <= :fdate)
      or bkdocs.vatperiod = :periodid)
    group by bkdocs.symbol, bkdocs.bkreg, bkdocs.number, bkdocs.vatreg,
      bkdocs.nip, bkdocs.contractor, bkdocs.transdate, bkdocs.docdate
    into :symbol, :bkreg, :nr, :vatreg, :nip, :kontrahkod, :netv, :vatv,
      :transdate, :docdate
  do begin
    select okres
      from datatookres(cast(:sdate as varchar(15)),0,0,0,-1)
    into :okres;
    if (:okres = :periodid) then
      VATV = 0;
    nr = 0;
    lp = :lp + 1;
    ref = ref + 1;
    BRUV = netv + vatv;
    suspend;
    nag = 0;
    netv = null;
    vatv = null;
    bruv = null;
  end
end^
SET TERM ; ^
