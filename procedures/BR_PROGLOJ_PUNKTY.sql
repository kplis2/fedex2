--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_PROGLOJ_PUNKTY(
      STABLE varchar(31) CHARACTER SET UTF8                           ,
      SREF integer)
  returns (
      REF integer,
      SUMAPKT integer,
      CPLREGNAZWA varchar(255) CHARACTER SET UTF8                           ,
      CPLPROGNAZWA varchar(255) CHARACTER SET UTF8                           ,
      REGULA integer,
      PROCSQL varchar(255) CHARACTER SET UTF8                           ,
      RTABLE varchar(255) CHARACTER SET UTF8                           ,
      RREF integer,
      OPISTRAN varchar(255) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE CPLREF INTEGER;
DECLARE VARIABLE CPLUCZESTREF INTEGER;
DECLARE VARIABLE TYPPKT INTEGER;
DECLARE VARIABLE PROCED VARCHAR(255);
DECLARE VARIABLE ZAKRESFROM VARCHAR(255);
DECLARE VARIABLE ZAKRESWHERE VARCHAR(255);
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE CPLDEFOPERREF INTEGER;
DECLARE VARIABLE CPLDEFOPERREFMIN INTEGER;
DECLARE VARIABLE CPLDEFOPERREFPLU INTEGER;
DECLARE VARIABLE SCPODMIOT INTEGER;
DECLARE VARIABLE SCPLUCZEST INTEGER;
DECLARE VARIABLE KLIENT INTEGER;
DECLARE VARIABLE DOSTAWCA INTEGER;
DECLARE VARIABLE ZEW INTEGER;
DECLARE VARIABLE WYD INTEGER;
DECLARE VARIABLE KOR INTEGER;
DECLARE VARIABLE ZAKUP INTEGER;
DECLARE VARIABLE DOKDATA TIMESTAMP;
DECLARE VARIABLE DOKDATAACK TIMESTAMP;
DECLARE VARIABLE DOKKONTRAH VARCHAR(255);
DECLARE VARIABLE DOKODDZIAL VARCHAR(10);
DECLARE VARIABLE DOKSYMBOL VARCHAR(20);
DECLARE VARIABLE NRKARTY INTEGER;
DECLARE VARIABLE MNOZNIK NUMERIC(14,4);
DECLARE VARIABLE SLODEFREF INTEGER;
begin
  /*okreslenie podmiotu i/lub uczestnika na podstawie danych dokumentu*/
  ref = 0;
  RTABLE = :stable;
  RREF = :sref;
  if(:stable = 'DOKUMNAG') then begin
    select defdokum.zewn, defdokum.wydania, defdokum.koryg,
        dokumnag.klient, dokumnag.dostawca,
        dokumnag.data, dokumnag.dataakc, dokumnag.symbol, dokumnag.oddzial
      from defdokum join DOKUMNAG on (DOKUMNAG.typ = DEFDOKUM.SYMBOL)
      where dokumnag.ref = :sref
      into :zew, :wyd, :kor, :klient, :dostawca,
        :dokdata, :dokdataack, :doksymbol, :dokoddzial;
    if (:zew is null) then zew = 1;
    if (:wyd is null) then wyd = 1;
    if (:kor is null) then kor = 1;
    if(:zew = 1) then begin
      if ((:wyd = 1 and :kor = 0) or (:wyd = 0  and :kor = 1)) then begin
        select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'KLIENCI' into :slodefref;
        select ref from CPODMIOTY where CPODMIOTY.SLODEF = :slodefref and CPODMIOTY.SLOPOZ = :klient into :scpodmiot;
      end
      if ((:wyd = 0 and kor = 0) or (:wyd = 1 and :KOR = 1)) then begin
        select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'DOSTAWCY' into :slodefref;
        select ref from CPODMIOTY where CPODMIOTY.SLODEF = :slodefref and SLOPOZ = :dostawca into :scpodmiot;
      end
    end
  end else if(:stable = 'NAGFAK') then begin
    select NAGFAK.zakup, NAGFAK.klient, nagfak.dostawca, cpluczestid, cpluczestkartid,
           NAGFAK.SYMBOL, NAGFAK.DATA, NAGFAK.dataakc, NAGFAK.ODDZIAL
      from NAGFAK
      where ref=:sref
      into :zakup, :klient, :dostawca, :scpluczest, :nrkarty,
            :doksymbol, :dokdata, :dokdataack, :dokoddzial;
    if(:scpluczest > 0) then
      select CPODMIOT from CPLUCZEST where ref=:scpluczest into :scpodmiot;
    else if(:zakup = 0 and :klient > 0) then begin
      select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'KLIENCI' into :slodefref;
      select REF from CPODMIOTY where CPODMIOTY.SLODEF = :slodefref and CPODMIOTY.SLOPOZ = :klient into :scpodmiot;
    end else if(:zakup = 1 and :dostawca > 0) then begin
      select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'DOSTAWCY' into :slodefref;
      select ref from CPODMIOTY where SLODEF = :slodefref and SLOPOZ = :dostawca into :scpodmiot;
    end
    if(:zakup = 0) then
      select NAZWA from KLIENCI where REF=:klient into :dokkontrah;
    else
      select NAZWA from DOSTAWCY where REF=:dostawca into :dokkontrah;
  end
  if(:scpodmiot is null) then exit;

  /*dla kazdego programu lojalnosciowego podmiotu, na którym dany dokument nie dokonal zapisu*/
  for select cpluczest.ref, cpluczest.cpl, cpluczest.mnoznik
  from cpluczest

  where cpluczest.cpodmiot = :scpodmiot

    and ((:scpluczest is null) or (cpluczest.ref = :scpluczest))
  into :cpluczestref, :cplref, :mnoznik
  do begin
     /*dla kazdej reguly, ktora podlega pod dany program i jest dla danego typu dokumentu*/
     for select cplreguly.ref, cplreguly.procsql, cplreguly.typpunkt, ZAKRESFROM, ZAKRESWHERE,
         OPERACJAPLUS, OPERACJAMINUS,
         CPL.nazwa, cplreguly.nazwa
     from cplreguly left join CPL on (CPL.ref = cplreguly.progloj)
     where cplreguly.progloj = :cplref
       and cplreguly.typdok = :stable
       and cplreguly.autooblicz > 0
     into  :regula, :procsql, :typpkt, :zakresfrom, :zakreswhere,
        :cpldefoperrefplu, :cpldefoperrefmin,
        :CPLPROGNAZWA,  :CPLREGNAZWA
     do begin
       /*sprawdzenie, czy rekord pasuje do zadanej dziedziny*/
       cnt = 1;
       if(:cnt > 0) then begin
         /*wykonanie liczenia punktów*/
         proced = 'select PKT, OPIS from '||procsql||'('||:regula ||','''||:stable||''','||:sref||',0)';
         execute statement :proced into :sumapkt, :opistran;
         if(:mnoznik is not null) then
           sumapkt = :sumapkt * :mnoznik;
         suspend;
         ref = ref + 1;
       end
     end
  end
end^
SET TERM ; ^
