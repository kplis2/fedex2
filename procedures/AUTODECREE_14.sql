--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_14(
      BKDOC integer)
   as
declare variable GROSSV numeric(14,2);
declare variable SETTLEMENT varchar(20);
declare variable TMP varchar(255);
declare variable DOCDATE timestamp;
declare variable PAYDAY timestamp;
declare variable DESCRIPT varchar(255);
declare variable DICTPOS integer;
declare variable DICTDEF integer;
declare variable NAGFAK integer;
declare variable SUMNETV numeric(14,2);
declare variable AMOUNT numeric(14,2);
declare variable BKPERIOD varchar(6);
declare variable VATPERIOD varchar(6);
declare variable ACCOUNTVATPERIOD varchar(1);
declare variable VATGR varchar(5);
declare variable TAXGR varchar(10);
begin
  -- schemat dekretowania - faktury zaliczkowe sprzedazy

  select sumgrossv, sumnetv, symbol, docdate, payday, descript, dictdef,
      dictpos, oref, period, vatperiod
    from bkdocs
    where ref = :bkdoc
    into :grossv, :sumnetv, :settlement, :docdate, :payday, :descript, :dictdef,
      :dictpos, :nagfak, :bkperiod, :vatperiod;

  if (coalesce(vatperiod,'') = bkperiod) then
    accountvatperiod = '0';
  else
    accountvatperiod = '1';

  if (payday < docdate) then payday = docdate;

  execute procedure KODKS_FROM_DICTPOS(dictdef, dictpos)
    returning_values tmp;
  execute procedure insert_decree_settlement(bkdoc, '200-' || tmp, 0, grossv, descript, settlement, null, 1, 1, docdate, payday, null, 'NAGFAK', nagfak, null, 0);
  execute procedure insert_decree(bkdoc, '845-' || tmp, 1, sumnetv, descript, 0);

  for
    select vate, vatgr, taxgr from bkvatpos where bkdoc = :bkdoc
      into :amount, :vatgr, :taxgr
  do
    execute procedure insert_decree(bkdoc, '222-' || accountvatperiod || '-' || taxgr || '-' || vatgr, 1, amount, descript, 0);

end^
SET TERM ; ^
