--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_FK_CALCULATE_CIT2(
      COMPANY integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      P27 numeric(14,2),
      P28 numeric(14,2),
      P29 numeric(14,2),
      P31 numeric(14,2),
      P32 numeric(14,2),
      P38 numeric(14,2),
      P40 numeric(14,2),
      P43 numeric(14,2))
  returns (
      INCOMES numeric(14,2),
      COSTS numeric(14,2),
      PROFIT numeric(14,2),
      LOSS numeric(14,2),
      P30 numeric(14,2),
      P33 numeric(14,2),
      P34 numeric(14,2),
      P35 numeric(14,2),
      P36 numeric(14,2),
      P37 numeric(14,2),
      P39 numeric(14,2),
      P41 numeric(14,2),
      P42 numeric(14,2),
      P44 numeric(14,2))
   as
declare variable amount numeric(14,2);
begin
  incomes = 0;
  p34 = 0;
  p36 = 19;
  p37 = 0;
  p39 = 0;

  if (P27 is null) then
    P27 = 0;
  if (P28 is null) then
    P28 = 0;
  if (P29 is null) then
    P29 = 0;
  if (P31 is null) then
    P31 = 0;
  if (P32 is null) then
    P32 = 0;
  if (P38 is null) then
    P38 = 0;
  if (P40 is null) then
    P40 = 0;
  if (P43 is null) then
    P43 = 0;

  execute procedure fk_fun_saldo(company, period, '700', 1)
    returning_values amount;
  incomes = incomes + amount;

  execute procedure fk_fun_saldo(company, period, '701', 1)
    returning_values amount;
  incomes = incomes + amount;

  execute procedure fk_fun_saldo(company, period, '702', 1)
    returning_values amount;
  incomes = incomes + amount;

  execute procedure fk_fun_saldo(company, period, '730', 1)
    returning_values amount;
  incomes = incomes + amount;

  execute procedure fk_fun_saldo(company, period, '750', 1)
    returning_values amount;
  incomes = incomes + amount;

  execute procedure fk_fun_saldo(company, period, '750', 0)
    returning_values amount;
  incomes = incomes - amount;


  execute procedure fk_fun_saldo(company, period, '760', 1)
    returning_values amount;                      
  incomes = incomes + amount;

  execute procedure fk_fun_saldo(company, period, '770', 1)
    returning_values amount;
  incomes = incomes + amount;

  /* korekta */
  execute procedure fk_fun_saldo(company, period, '750-05', 0)
    returning_values amount;
  incomes = incomes + amount;

  execute procedure fk_fun_saldo(company, period, '750-03', 0)
    returning_values amount;
  incomes = incomes + amount;

  execute procedure fk_fun_saldo(company, period, '750-09', 0)
    returning_values amount;
  incomes = incomes + amount;

  execute procedure fk_fun_saldo(company, period, '760-05', 0)
    returning_values amount;
  incomes = incomes + amount;

  execute procedure fk_fun_saldo(company, period, '760-09', 0)
    returning_values amount;
  incomes = incomes + amount;


  execute procedure fk_fun_saldo(company, period, '750-05', 1)
    returning_values amount;
  incomes = incomes - amount;

  execute procedure fk_fun_saldo(company, period, '750-03', 1)
    returning_values amount;
  incomes = incomes - amount;

  execute procedure fk_fun_saldo(company, period, '750-09', 1)
    returning_values amount;
  incomes = incomes - amount;
/*
  execute procedure fk_fun_saldo(company, period, '760-05', 1)
    returning_values amount;
  incomes = incomes - amount;
*/
  execute procedure fk_fun_saldo(company, period, '760-09', 1)
    returning_values amount;
  incomes = incomes - amount;


  /* koszty */
  costs = 0;
  execute procedure fk_fun_saldo(company, period, '500', 0)
    returning_values amount;
  costs = costs + amount;

  execute procedure fk_fun_saldo(company, period, '501', 0)
    returning_values amount;
  costs = costs + amount;

  execute procedure fk_fun_saldo(company, period, '510', 0)
    returning_values amount;
  costs = costs + amount;

  execute procedure fk_fun_saldo(company, period, '520', 0)
    returning_values amount;
  costs = costs + amount;

  execute procedure fk_fun_saldo(company, period, '530', 0)
    returning_values amount;
  costs = costs + amount;

  execute procedure fk_fun_saldo(company, period, '550', 0)
    returning_values amount;
  costs = costs + amount;

  execute procedure fk_fun_saldo(company, period, '710', 0)
    returning_values amount;
  costs = costs + amount;

  execute procedure fk_fun_saldo(company, period, '711', 0)
    returning_values amount;
  costs = costs + amount;

  execute procedure fk_fun_saldo(company, period, '712', 0)
    returning_values amount;
  costs = costs + amount;

  execute procedure fk_fun_saldo(company, period, '735', 0)
    returning_values amount;
  costs = costs + amount;

  /*Orika*/
  execute procedure fk_fun_saldo(company, period, '736', 0)
    returning_values amount;
  costs = costs + amount;


  execute procedure fk_fun_saldo(company, period, '755', 0)
    returning_values amount;
  costs = costs + amount;

  execute procedure fk_fun_saldo(company, period, '755', 1)
    returning_values amount;
  costs = costs - amount;


  execute procedure fk_fun_saldo(company, period, '765', 0)
    returning_values amount;
  costs = costs + amount;

  execute procedure fk_fun_saldo(company, period, '765', 1)
    returning_values amount;
  costs = costs - amount;


  execute procedure fk_fun_saldo(company, period, '775', 0)
    returning_values amount;
  costs = costs + amount;

  /* korekta */

  execute procedure fk_fun_saldo(company, period, '755-03', 1)
    returning_values amount;
  costs = costs + amount;

  execute procedure fk_fun_saldo(company, period, '755-08', 1)
    returning_values amount;
  costs = costs + amount;

  execute procedure fk_fun_saldo(company, period, '755-09', 1)
    returning_values amount;
  costs = costs + amount;

  execute procedure fk_fun_saldo(company, period, '765-03', 1)
    returning_values amount;
  costs = costs + amount;

  execute procedure fk_fun_saldo(company, period, '765-08', 1)
    returning_values amount;
  costs = costs + amount;

  execute procedure fk_fun_saldo(company, period, '765-09', 1)
    returning_values amount;
  costs = costs + amount;


  execute procedure fk_fun_saldo(company, period, '755-03', 0)
    returning_values amount;
  costs = costs - amount;

  execute procedure fk_fun_saldo(company, period, '755-08', 0)
    returning_values amount;
  costs = costs - amount;

  execute procedure fk_fun_saldo(company, period, '755-09', 0)
    returning_values amount;
  costs = costs - amount;

  execute procedure fk_fun_saldo(company, period, '765-03', 0)
    returning_values amount;
  costs = costs - amount;

  execute procedure fk_fun_saldo(company, period, '765-08', 0)
    returning_values amount;
  costs = costs - amount;

  execute procedure fk_fun_saldo(company, period, '765-09', 0)
    returning_values amount;                         
  costs = costs - amount;

  execute procedure fk_fun_saldo(company, period, '400-3', 0)
    returning_values amount;                         
  costs = costs - amount;

  execute procedure fk_fun_saldo(company, period, '460-9', 0)
    returning_values amount;                         
  costs = costs - amount;





  execute procedure fk_fun_saldo(company, period, '440-2', 0)
    returning_values amount;

  if (amount > (0.0025*incomes)) then
    costs = costs - (amount - 0.0025*incomes);


  profit = 0;
  loss = 0;
  if (incomes-costs>0) then
    profit = incomes - costs;
  else
    loss = costs - incomes;

  p30 = p27 + p28 + p29;
  p33 = p31 + p32;

  if (profit>0) then
    p34 = profit - p30 + p33;
  else
    if (loss<p33) then
      p34 = p33-(loss+p30);

  if (p34=0) then
    p34 = 0;

  p35 = loss + p30 - (profit+p33);

  if (p35<0) then
    p35 = 0;

  /* obciecie groszy */
  p34 = cast(p34-0.5 as integer);

  if (p34>0) then
  begin
    p37 = p36*p34/100;
    p39 = p37 - p38;

    /* obciecie groszy */
    p39 = cast(p39-0.5 as integer);
  end

  p41 = p39 - p40;
  if (p41<0) then
    p41 = 0;

  p42 = p40 + p41;
  p44 = p41 - p43;

  if (P27 = 0) then
    P27 = null;
  if (P28 = 0) then
    P27 = null;
  if (P29 = 0) then
    P27 = null;
  if (P31 = 0) then
    P27 = null;
  if (P32 = 0) then
    P27 = null;
  if (P38 = 0) then
    P27 = null;
  if (P40 = 0) then
    P27 = null;
  if (P43 = 0) then
    P27 = null;

  suspend;
end^
SET TERM ; ^
