--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_COL(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      COL integer)
  returns (
      RET numeric(14,2))
   as
begin
/*DU: Personel - funkcja podaje wartosc skladnika z listy plac. Jezeli parametr
                 COL < 0, to wynik null'owy nie będzie rzutowany na zero (PR48961)*/

  select pvalue
    from eprpos
    where employee = :employee and payroll = :payroll
      and ecolumn = abs(:col)
    into :ret;

  if (ret is null and col >= 0) then
    ret = 0;

  suspend;
end^
SET TERM ; ^
