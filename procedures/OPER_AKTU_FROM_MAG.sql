--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_AKTU_FROM_MAG(
      ZAM integer)
  returns (
      STATUS integer)
   as
declare variable magazyn varchar(3);
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable ilosc numeric(14,4);
declare variable ilreal numeric(14,4);
declare variable pozref integer;
begin
  STATUS = -1;
  for select distinct MAGAZYN,KTM,WERSJA from POZZAM where ZAMOWIENIE=:ZAM
   into :magazyn, :ktm, :wersja
  do begin
    ilosc = null;
    select sum(DOKUMPOZ.ilosc) from DOKUMPOZ join DOKUMNAG on (DOKUMPOZ.dokument = DOKUMNAG.REF)
         join DEFDOKUM on ( DEFDOKUM.symbol = DOKUMNAG.typ )
      where DOKUMNAG.magazyn = :magazyn and DOKUMPOZ.KTM = :ktm and DOKUMPOZ.wersja =:wersja and
         DOKUMNAG.zamowienie = :zam AND DEFDOKUM.zewn = 1 AND DEFDOKUM.wydania=1
    into :ilosc;
    if(:ilosc is null) then ilosc = 0;
    pozref = null;
    for select ref,pozzam.ilreal from POZZAM
    where ZAMOWIENIE=:ZAM AND KTM=:KTM AND WERSJA=:WERSJA order by NUMER into :pozref, :ilreal
    do begin
      if(:ilreal > :ilosc) then begin
        update POZZAM set ILREAL=:ilosc where REF=:pozref;
        ilosc = 0;
      end else
        ilosc = :ilosc - :ilreal;
    end
    if((:ilosc > 0) and (pozref is not null)) then begin
      /*dodanie nadwyzek do ostatniej pozycji zamowiniea na dany towar */
      update POZZAM set ILREAL = ILREAL+:ilosc where REF=:pozref;
    end
  end
  status = 1;
end^
SET TERM ; ^
