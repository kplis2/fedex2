--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_CONSTLOC_GET_REASON(
      SYMBOL varchar(40) CHARACTER SET UTF8                           )
  returns (
      REASON STRING)
   as
begin
    select first 1 m.x_reason_blok
      from mwsconstlocs m
      where m.symbol = :symbol
    into :reason;

    if (reason is null) then reason = '';
    suspend;
end^
SET TERM ; ^
