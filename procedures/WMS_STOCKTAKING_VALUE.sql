--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WMS_STOCKTAKING_VALUE(
      WH varchar(3) CHARACTER SET UTF8                           ,
      PRICEWH varchar(3) CHARACTER SET UTF8                           )
  returns (
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      GOODDICT varchar(255) CHARACTER SET UTF8                           ,
      VERS integer,
      VERSDICT varchar(80) CHARACTER SET UTF8                           ,
      QUANTITY numeric(14,4),
      PRICE numeric(14,4),
      AMOUNT numeric(14,4))
   as
declare variable mwsconstloc integer;
declare variable priceplus numeric(14,4);
declare variable priceminus numeric(14,4);
declare variable tmpquantity numeric(14,4);
declare variable cquantity numeric(14,4);
declare variable whout varchar(3);
begin
  for
    select first 1 c.ref
      from mwsconstlocs c
      where c.wh = :wh and c.locdest = 6
      into mwsconstloc
  do begin
    for
      select s.vers, s.good, sum(s.quantity)
        from mwsstock s
        where s.mwsconstloc = :mwsconstloc
        group by s.mwsconstloc, s.vers, s.good
        having sum(s.quantity) < 0
        into vers, good, quantity
    do begin
      versdict = '';
      gooddict = '';
      priceplus = 0;
      priceminus = 0;
      amount = 0;
      tmpquantity = quantity;
      select t.nazwa, t.nazwat, a.cena_zakn
        from wersje t
          left join towary a on (a.ktm = t.ktm)
        where t.ref = :vers
        into versdict, gooddict, priceplus;
      if (quantity < 0) then
      begin
        select first 1 cena from wmsinpluswycena w where w.wersjaref = :vers and w.status = 1
          into priceplus;
        if (priceplus = 0) then priceplus = 0;
        if (priceplus = 0) then
          execute procedure mws_stocktakin_settl_price(:good,:vers, :pricewh) returning_values (:priceplus, :whout);
        price = priceplus;
        amount = priceplus * quantity;
        suspend;
      end else
      begin
        while (tmpquantity > 0)
        do begin
          for
            select c.ilosc, c.cena
              from stanycen c
              where c.magazyn = :wh and c.wersjaref = :vers
              into cquantity, priceminus
          do begin
            if (tmpquantity < cquantity) then
              quantity = tmpquantity;
            else
              quantity = cquantity;
            price = priceminus;
            amount = quantity * priceminus;
            suspend;
            tmpquantity = tmpquantity - quantity;
            if (tmpquantity <= 0) then
              break;
          end
        end
      end
    end
  end
end^
SET TERM ; ^
