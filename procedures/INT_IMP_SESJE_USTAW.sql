--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_SESJE_USTAW(
      REGULA STRING255,
      TABELA TABLE_ID,
      LOGIN STRING20,
      KIERUNEK SMALLINT_ID)
  returns (
      REF SESJE_ID)
   as
declare variable procedura table_id;
declare variable zrodloint integer_id;
begin
-- insert into expimp (s1,s2,s3,s4,s5,s6)
--values ( 'INT_IMP_SESJE_USTAW', current_timestamp, :REGULA, :TABELA , :LOGIN , :KIERUNEK );
  regula = upper(:regula);
  tabela = upper(:tabela);
  login = upper(:login);
  select first 1 z.ref from int_zrodla z where z.nazwa = :login
  into :zrodloint;

  select first 1 p.procedura
    from int_procedury p
    where p.tabela = :tabela and p.zrodlo = :zrodloint and p.kierunek = :kierunek
  into :procedura;

  insert into int_sesje (regula, tabela, procedura, zrodlo, kierunek)
    values(:regula, :tabela, :procedura, :zrodloint, :kierunek)
    returning ref into :ref;

  suspend;
end^
SET TERM ; ^
