--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMSER_HIST(
      SERIALNR varchar(30) CHARACTER SET UTF8                           )
  returns (
      OPIS varchar(60) CHARACTER SET UTF8                           ,
      SYMBOLDOKUMNAG varchar(30) CHARACTER SET UTF8                           ,
      DATA timestamp,
      REFDOKUMNAG integer,
      SYMBOLNAGFAK varchar(20) CHARACTER SET UTF8                           ,
      REFNAGFAK integer)
   as
declare variable TEMPDATA1 timestamp;
declare variable TEMPDATA2 timestamp;
declare variable TEMPSTR1 varchar(30);
declare variable TEMPSTR2 varchar(30);
declare variable TEMPWYDANIA smallint;
begin
  serialnr = coalesce(:serialnr,'');
  for select dokumnag.symbol, dokumnag.dataakc, dokumnag.ref, defdokum.wydania,
    nagfak.symbol, nagfak.ref
    from dokumser
      join dokumpoz on dokumpoz.ref = dokumser.refpoz
      join dokumnag on dokumnag.ref = dokumpoz.dokument
      join defdokum on defdokum.symbol = dokumnag.typ
      left join nagfak on nagfak.ref = dokumnag.faktura
    where (dokumser.odserial = :serialnr or :serialnr = '') and dokumser.typ = 'M'
    order by dokumnag.dataakc, dokumnag.ref
    into :symboldokumnag, :data, :refdokumnag, :tempwydania,
      :symbolnagfak, :refnagfak
  do begin
    if (tempwydania = 0) then opis = 'Przyjęcie do magazynu';
    else opis = 'Wydanie z magazynu';
    suspend;
  end
end^
SET TERM ; ^
