--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_SET_USERPASS_EHRM(
      PERSON integer,
      PASSWD varchar(40) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint)
   as
declare variable currcryptedpsw varchar(255);
declare variable userid integer;
declare variable login varchar(20);
begin
  status = 1;

  select s.userid, s.userlogin
    from persons p
      join s_users s on (p.userid = s.userid)
    where p.ref = :person
  into :userid, :login;

  currcryptedpsw = shash_hmac('hex','sha1',:passwd,:login);

  update s_users set userpass = :currcryptedpsw,
      must_change = 0, last_change = current_timestamp(0)
    where userid = :userid
      and userlogin = :login;
  suspend;

  when any do
  begin
    status = 0;
    suspend;
  end
end^
SET TERM ; ^
