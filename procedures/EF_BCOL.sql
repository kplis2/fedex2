--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_BCOL(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      COL integer,
      M integer)
  returns (
      RET numeric(14,2))
   as
declare variable cper varchar(6);
  declare variable fcper varchar(6);
begin
  --DU: personel -

  select cper from epayrolls where ref = :payroll into :cper;

  if (m = 1) then
  begin
    for
      select first 1 cper from epayrolls
        where empltype = 1 and cper < :cper
        order by cper desc
        into :fcper
    do begin
    end
  end else
  if (m = 2) then
  begin
    for
      select first 2 cper from epayrolls
        where empltype = 1 and cper < :cper
        order by cper desc
        into :fcper
    do begin
    end
  end else
  if (m = 3) then
  begin
    for
      select first 3 cper from epayrolls
        where empltype = 1 and cper < :cper
        order by cper desc
        into :fcper
    do begin
    end
  end




  select sum(p.pvalue)
    from eprpos p
      join epayrolls pr on (pr.ref = p.payroll)
    where p.employee = :employee and p.ecolumn = :col and pr.cper = :fcper
    into :ret;
  if (ret is null) then
    ret = 0;
  suspend;
end^
SET TERM ; ^
