--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMPOZ_CHANGE_DEC_DELIVERY(
      KORTOPOZ integer,
      POZDOKUM integer,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      ILOSC numeric(14,5),
      SERIALIZED smallint,
      BLOKOBLNAG smallint,
      BLOKPOZNALICZ integer,
      CENAMAG numeric(14,4),
      DOSTAWA integer,
      MAG_TYP char(1) CHARACTER SET UTF8                           ,
      MAG char(3) CHARACTER SET UTF8                           ,
      FIFO char(1) CHARACTER SET UTF8                           ,
      MINUS_STAN smallint,
      MAGBREAK integer,
      ZAMOWIENIE integer,
      WERSJAREF integer,
      OPK smallint,
      OPKTRYB smallint,
      SLODEF integer,
      SLOPOZ integer,
      CENASNET numeric(14,4),
      CENASBRU numeric(14,4),
      BN char(1) CHARACTER SET UTF8                           ,
      VAT numeric(14,4),
      STANOPKREF integer,
      ORG_ILOSC numeric(14,4),
      DOK_ILOSC numeric(14,4),
      CENA_KOSZT numeric(14,4))
   as
declare variable numer integer;
declare variable i numeric(14,5);
declare variable j numeric(14,5);
declare variable lcenamag numeric(14,4);
declare variable oldcena numeric(14,4);
declare variable rozref integer;
declare variable korilosc numeric(14,4);
declare variable koriloscl numeric(14,4);
begin
      if(:ilosc < 0)then ilosc = -ilosc;
      if(:org_ilosc < 0)then org_ilosc = -org_ilosc;
      oldcena = null;
      if(:mag_typ = 'E') then begin
        select CENA from STANYIL where MAGAZYN=:mag and KTM = :KTM and WERSJA=:WERSJA and ILOSC <> 0 into :oldcena;
        if(:oldcena = 0) then oldcena = null;
      end
      for select NUMER,ilosc, cena from DOKUMROZ where pozycja = :pozdokum order by numer  desc into :numer,:i, :lcenamag do begin
         if(:i is null) then i = 0;
         if(:oldcena <> :lcenamag) then exception DOKUM_ROZ_CENA_E_NIEAKTUALNA;
         if((:i > :ilosc) and (:ilosc <> 0)) then begin
           update DOKUMROZ set ILOSC = (ILOSC - :ilosc), PCENA = null where pozycja = :pozdokum and numer = :numer;
           ilosc = 0;
         end else if(:ilosc > 0) then begin
           delete from DOKUMROZ where pozycja = :pozdokum and numer = :numer;
           ilosc = ilosc - i;
         end
      end
      if(:blokpoznalicz = 0) then
        execute procedure DOKPOZ_OBL_FROMROZ(:pozdokum);
      /*jesli to korekta ilosciowa, to poprawienie rozpisek na pozycji korygowanej*/
/*      if(:ilosc < :org_ilosc and :kortopoz > 0) then begin
           org_ilosc = :org_ilosc - :ilosc;
           for select REF, ILOSC, ILOSCL
           from DOKUMROZ where POZYCJA = :kortopoz
           order by NUMER DESC
           into :rozref, :korilosc, :koriloscl
           do begin
            j = :korilosc - :koriloscl;
            if(:j > 0) then begin
              if(:j > :org_ilosc) then j = :org_ilosc;
              if(:j > 0) then
                update DOKUMROZ set ILOSCL = ILOSCL + :j, PCENA = null where ref = :rozref;
              org_ilosc = :org_ilosc - :j;
            end
           end
      end*/ --MS wylaczam w ramach BS32921
end^
SET TERM ; ^
