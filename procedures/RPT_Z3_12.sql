--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_Z3_12(
      EMPLOYEE integer,
      ONDATE date = null)
  returns (
      COL1 varchar(4) CHARACTER SET UTF8                           ,
      COL2 varchar(2) CHARACTER SET UTF8                           ,
      COL3 integer,
      COL4 integer,
      COL5 varchar(1000) CHARACTER SET UTF8                           ,
      COL6 numeric(14,2),
      COL7 numeric(14,2),
      COL8 numeric(14,2),
      COL9 numeric(14,2))
   as
declare variable FBASE varchar(6);
declare variable LBASE varchar(6);
declare variable PERIOD varchar(6);
declare variable FEP numeric(14,2);
declare variable FRP numeric(14,2);
declare variable FC numeric(14,2);
declare variable PDATE date;
declare variable CURRENTCOMPANY integer;
declare variable AYEAR integer;
declare variable AMONTH integer;
declare variable FROMDATE date;
declare variable MINEFROMDATE date;
declare variable TMP varchar(22);
begin

  ondate = coalesce(ondate, current_date);

  select max(fbase), max(lbase) from eabsences
    where ecolumn in (40,50,60,90,100,110,120,140,150,270,280,290,350,360,370,440,450)
      and employee = :employee and fromdate < :ondate and correction in (0,2)
    into :fbase, :lbase;

  select company from employees
    where ref = :employee
    into :currentcompany;

  select min(fromdate) from employment
    where employee = :employee
      and fromdate <= :ondate
    into :minefromdate;

  if (exists(select first 1 1 from e_func_periodinc(null,1,:minefromdate)
        where periodout = :fbase) --BS35854
  ) then
    execute procedure e_func_periodinc(null,0,minefromdate)
      returning_values fbase;

  for
    select distinct p.cper
      from epayrolls p join eprpos ep on (p.ref = ep.payroll)
      where ep.employee = :employee
        and p.cper <= :lbase and p.cper >= :fbase
      order by p.cper
    into :period
  do begin
    col1 = substring(period from 1 for 4);
    col2 = substring(period from 5 for 2);

    ayear = cast(col1 as integer);
    amonth = cast(col2 as smallint);
    fromdate = cast(ayear || '/' || amonth || '/1' as date);

    select sum(e.pvalue)
      from epayrolls p join eprpos e on (e.payroll = p.ref)
      where p.cper = :period and e.employee = :employee
        and e.ecolumn in (220, 250, 510, 310)
      into :col3;
  
    select sum(e.pvalue)
      from epayrolls p join eprpos e on (e.payroll = p.ref)
      where p.cper = :period and e.employee = :employee
        and e.ecolumn = 500
      into :col4;
  
  --przyczyna nieobecnosci
    col5 = ' ';
    if (minefromdate > fromdate) then col5 = 'Początek zatrudnienia';

    for
      select distinct c.name
        from eabsences a join ecolumns c on (a.ecolumn = c.number)
        where a.employee = :employee and a.ayear = :ayear and a.amonth = :amonth
          and c.number not in (220, 250, 310) and a.correction in (0,2)
        into :tmp
    do begin
      if (col5 <> ' ') then col5 = col5 || ', ';
      col5 = col5 || :tmp;
    end

  --skladniki pomniejszane proporcjonalnie
    select sum(e.pvalue)
      from epayrolls p join eprpos e on (e.payroll = p.ref)
      where p.cper = :period and e.employee = :employee
        and ((e.ecolumn >= 1000 and e.ecolumn <= 1100)
          or (e.ecolumn >= 1500 and e.ecolumn <= 2000 and e.ecolumn <> 1600)) --BS38769
      into :col7;
  
  --skladniki nie pomniejszane proporcjonalnie
    select sum(e.pvalue)
      from epayrolls p join eprpos e on (e.payroll = p.ref)
      where p.cper = :period and e.employee = :employee
        and ((e.ecolumn >= 1200 and e.ecolumn < 1500 )
          or e.ecolumn = 1030)
      into :col8;                                                                                                                     
                                                                                                                                      
  --procent skladki DO ZROBIENIA
    select todate + 1 from period2dates(:period)
      into :pdate;
    execute procedure get_pval(pdate, currentcompany, 'FC')
      returning_values fc;
    execute procedure get_pval(pdate, currentcompany, 'FEP')
      returning_values fep;
    execute procedure get_pval(pdate, currentcompany, 'FRP')
      returning_values frp;

    col9 = fep + frp + fc;

    suspend;
  end
end^
SET TERM ; ^
