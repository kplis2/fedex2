--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRGANTT_CALC(
      PRSCHEDULE integer,
      PRSCHEDGUIDE integer = 0)
   as
declare variable prdepart varchar(20);
begin
  if (exists (select first 1 1 from rdb$procedures where RDB$PROCEDURE_NAME ='XK_PRGANTT_CALC')) then
    execute statement 'execute procedure XK_PRGANTT_CALC(0'||:prschedule||','||:prschedguide||'0)';
  else if (prschedguide = 0) then
  begin
    select s.prdepart
      from prschedules s
      where s.ref = :prschedule
      into prdepart;
    delete from prgantt;
    -- operacje
    insert into prgantt (timefrom, timeto,
        prschedoper, prschedoperto, prmachine,
        prmachinesymb, descript, operstatus, opermatstatus, opertimestatus,
        nagzam, pozzam, prschedguide, prshoper, prdepart, workplace, depfrom,
        prschedule, priority, verified, status, worktime, statusmat, realtime, good, genid, gtype, REALPRECENT)
      select coalesce(o.starttime,current_timestamp(0)), coalesce(o.endtime,current_timestamp(0)),
          o.ref, null, coalesce(o.machine,p.prmachine),
          coalesce(m2.symbol,m1.symbol), '', o.status, o.statusmat, case when o.endtime > current_timestamp(0)  then 1 else 0 end, 
          g.zamowienie, g.pozzam, g.ref, o.shoper, g.prdepart, p.workplace,
          (select first 1 dep.depfrom
             from prschedoperdeps dep
               left join prschedopers op on (op.ref = dep.depfrom)
             where dep.depto = o.ref and op.status < 3 and op.activ > 0),
          g.prschedule, g.prpriority, 0, o.status, o.worktime, o.statusmat, pz.termdost, g.ktm, g.schedview, 0,
          100 * (case when o.reported <> 0 then o.amountresult else 0 end / case when o.amountin > 0 then o.amountin else 1 end)
        from prschedguides g
          left join prschedopers o on (o.guide = g.ref)
          left join prshopers p on (p.ref = o.shoper)
          left join prmachines m1 on (m1.ref = p.prmachine)
          left join prmachines m2 on (m2.ref = o.machine)
          left join nagzam n on (n.ref = g.zamowienie)
          left join pozzam pz on (pz.ref = g.pozzam)
          left join towary t on (t.ktm = pz.ktm)
        where g.status < 2 and o.status <= 2 and g.pozzam is not null and o.activ = 1
          and g.prschedule = :prschedule and (coalesce(o.machine,p.prmachine) is not null or p.workplace is not null)
          and n.datawe >= current_date - 365;
    -- awarie maszyn
    insert into prgantt (timefrom, timeto, prmachine, gtype, prschedule, prschedoper, prrepairtype, prdepart)
      select distinct r.inactionfrom, r.inactionto, r.machine, 1, :prschedule, r.ref, r.prrepairs, p.prdepart
         from prmachrepairs r
           left join prmachineswplaces w on (w.prmachine = r.machine)
           left join prworkplaces p on (p.symbol = w.prworkplace)
         where w.fromdate <= current_timestamp(0) and (w.todate is null or w.todate > current_timestamp(0))
           and p.prdepart = :prdepart;
  end else if (prschedguide > 0) then
  begin
    delete from prgantt where prschedguide = :prschedguide;
    insert into prgantt (timefrom, timeto,
        prschedoper, prschedoperto, prmachine,
        prmachinesymb, descript, operstatus, opermatstatus, opertimestatus,
        nagzam, pozzam, prschedguide, prshoper, prdepart, workplace, depfrom,
        prschedule, priority, verified, status, worktime, statusmat, genid, gtype)
      select coalesce(o.starttime,current_timestamp(0)), coalesce(o.endtime,current_timestamp(0)),
          o.ref, null, coalesce(o.machine,p.prmachine),
          coalesce(m2.symbol,m1.symbol), '', o.status, o.statusmat, case when o.endtime > current_timestamp(0)  then 1 else 0 end, 
          g.zamowienie, g.pozzam, g.ref, o.shoper, g.prdepart, p.workplace,
          (select first 1 dep.depfrom
             from prschedoperdeps dep
               left join prschedopers op on (op.ref = dep.depfrom)
             where dep.depto = o.ref and op.status < 3),
          g.prschedule, g.prpriority, 0, o.status, o.worktime, o.statusmat, g.schedview, 0
        from prschedguides g
          left join prschedopers o on (o.guide = g.ref)
          left join prshopers p on (p.ref = o.shoper)
          left join prmachines m1 on (m1.ref = p.prmachine)
          left join prmachines m2 on (m2.ref = o.machine)
        where g.ref = :prschedguide and g.status <= 2 and o.status <= 2 and g.pozzam is not null and o.activ = 1
          and g.prschedule = :prschedule and (coalesce(o.machine,p.prmachine) is not null or p.workplace is not null);
  end
end^
SET TERM ; ^
