--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYNC_SYS_TRANSLATIONS(
      TYPEID STRING,
      ID STRING,
      TRANSLATION STRING1024_UTF8,
      LANGUAGEID CODEISO639 = 'pl')
   as
begin
  --procedura synchronizuje jezyk polski z tabel tj. platnosci/ sposdost
  --zalozenie takie ze typeid istnieje w tabeli sys_translation_types
  update or insert into sys_translations (id, sys_translation_typeid, languageid, "TRANSLATION")
    values (:id, :typeid, :languageid, :translation)
    matching (id, sys_translation_typeid, languageid);
end^
SET TERM ; ^
