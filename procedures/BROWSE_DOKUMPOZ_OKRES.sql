--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BROWSE_DOKUMPOZ_OKRES(
      MAGAZYN varchar(5) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      OKR varchar(6) CHARACTER SET UTF8                           ,
      ODDATY timestamp,
      AKC integer)
  returns (
      REF integer,
      OKRES varchar(6) CHARACTER SET UTF8                           ,
      TYP varchar(3) CHARACTER SET UTF8                           ,
      NUMER integer,
      POZ integer,
      DATA timestamp,
      DOSTAWA integer,
      DOSTAWADICT varchar(120) CHARACTER SET UTF8                           ,
      ILOSCP numeric(14,4),
      ILOSCM numeric(14,4),
      CENA numeric(14,2),
      WARTOSC numeric(14,2),
      CENASR numeric(14,4),
      ILOSCPO numeric(14,4),
      WARTOSCPO numeric(14,2),
      AKCEPT integer)
   as
declare variable DATAPOCZ timestamp;
declare variable WYDANIA integer;
begin
  /*stan na początku okresu*/
  datapocz = :oddaty - 1;
  okres = :okr;
  numer = 0;
  poz = 0;
  dostawa = 0;
  ref = 0;
  data = :datapocz;
  execute procedure MAG_STAN(:MAGAZYN,:KTM,:WERSJA,NULL,NULL,:datapocz)
    returning_values :iloscpo, :wartoscpo;
  suspend;
  for select DOKUMNAG.TYP,DOKUMNAG.NUMER,DOKUMPOZ.NUMER as POZ,
    DOKUMNAG.DATA,DOKUMPOZ.ILOSC,DOKUMPOZ.CENA,DOKUMPOZ.DOSTAWA,
    DOKUMPOZ.WARTOSC,DOKUMNAG.AKCEPT,DOKUMPOZ.REF,
    DOKUMPOZ.CENASR, DOSTAWY.SYMBOL AS DOSTAWADICT, DEFDOKUM.WYDANIA as WYDANIA
  from DOKUMPOZ join DOKUMNAG on ( DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
    left join DEFDOKUM on (DOKUMNAG.TYP = DEFDOKUM.SYMBOL)
    left join DOSTAWY on ( DOSTAWY.REF = DOKUMPOZ.DOSTAWA)
 where DOKUMNAG.okres = :okr and ((DOKUMNAG.akcept = 1) or (:akc = 0)) and
    DOKUMPOZ.KTM = :KTM and DOKUMPOZ.WERSJA = :wersja
 order by DOKUMNAG.DATA, DEFDOKUM.WYDANIA, DOKUMNAG.TYP, DOKUMNAG.NUMER
 into :TYP, :numer, :poz, :data, :iloscp, :cena,:dostawa,
      :wartosc, :akcept, :ref,
      :cenasr,:dostawadict, :wydania
 do begin
   if(:wydania = 1) then begin
     iloscm = :iloscp;
     iloscp = NULL;
     iloscpo = :iloscpo - :iloscm;
     wartoscpo = :wartoscpo - :wartosc;
   end else begin
     iloscm = null;
     iloscpo = :iloscpo + :iloscm;
     wartoscpo = :wartoscpo + :wartosc;
   end
   suspend;
 end

end^
SET TERM ; ^
