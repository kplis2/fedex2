--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRGENORDSPREPARE_NAGZAMSPR_REL(
      REJESTR varchar(3) CHARACTER SET UTF8                           ,
      TYPZAM varchar(3) CHARACTER SET UTF8                           ,
      DATAOD varchar(10) CHARACTER SET UTF8                           ,
      DATADO varchar(10) CHARACTER SET UTF8                           ,
      KLIENT integer,
      MASKA varchar(40) CHARACTER SET UTF8                           ,
      PRREJESTR varchar(3) CHARACTER SET UTF8                           ,
      BYPOZZAM smallint,
      ILWMS smallint,
      SHOWEMPTY smallint,
      AUTOSCHED smallint,
      PRDEPART PRDEPARTS_ID = null)
   as
declare variable sql varchar(4000);
declare variable wersjaref integer;
declare variable amount numeric(14,4);
declare variable descript varchar(1024);
declare variable nagzam integer;
declare variable pozzam integer;
declare variable prsheet integer;
declare variable amountinwh numeric(14,4);
declare variable amountinprord numeric(14,4);
declare variable amountinord numeric(14,4);
declare variable ilzdysp numeric(14,4);
declare variable ilzreal numeric(14,4);
declare variable actupos integer_id;
declare variable amountleft numeric(14,4);
declare variable termdost timestamp;
declare variable actuprdepart prdeparts_id;

begin
  delete from PRGENORDSPREPARE;
  if(:showempty is null) then showempty = 0;
  if(:ilwms is null) then ilwms =0;
  if( not exists (select first 1 1 from prdeparts pd where pd.symbol = :prdepart))
    then prdepart = null;
  sql = 'select p.wersjaref, sum(p.ilosc - p.ilzreal),';
  if (:bypozzam = 1) then
    sql = sql ||' p.ref, n.ref, min(k.fskrot), min(n.termdostdecl)';
  else
    sql = sql ||'min(null), min(null), min(null), min(n.termdostdecl)';
  sql = sql||'    from nagzam n
        join pozzam p on (p.zamowienie = n.ref)
        left join klienci k on (k.ref = n.klient)
      where 1 = 1';
  if (coalesce(:rejestr,'') <> '') then
    sql = sql ||' and n.rejestr = '''||:rejestr||'''';
  if (coalesce(:typzam,'') <> '') then
    sql = sql ||' and n.typzam = '''||:typzam||'''';
  if (coalesce(:dataod, '') <> '' and coalesce(:datado, '') <> '' and dataod = datado) then
    sql = sql ||' and /*n.datazm*/ n.termdostdecl = '''||:dataod||'''';
  else begin
    if (coalesce(:dataod, '') <> '' ) then
      sql = sql ||' and /*n.datazm*/ n.termdostdecl >= '''||:dataod||'''';
    if (coalesce(:datado, '') <> '' ) then
      sql = sql ||' and /*n.datazm*/ n.termdostdecl <= '''||:datado||'''';
  end
  if (coalesce(:klient,0) > 0) then
    sql = sql ||' and n.klient = '||:klient;
  if (coalesce(:maska,'') <> '') then
    sql = sql ||' and p.ktm like '''||:maska||'%''';
  sql = sql ||' group by p.wersjaref ';
  if (:bypozzam = 1) then
    sql = sql ||', p.ref,  n.ref';
  sql = sql ||' having sum(p.ilosc - p.ilzreal) > 0';
  sql = sql ||' order by p.wersjaref';
  --insert into expimp (s71)values(:sql);
  for
    execute statement :sql
      into :wersjaref, :amountinord, :pozzam, :nagzam, :descript, :termdost
  do begin
    amount = amountinord;
    prsheet = null;
    select p.ref, p.prdepart
      from prsheets p
      where p.wersjaref = :wersjaref and p.status = 2
        and (:PRDEPART is null or p.prdepart = :PRDEPART)
      into :prsheet, :actuprdepart;
    if ((:amount > 0 or :showempty = 1) and (:PRDEPART is null or :prsheet is not null)) then
      insert into PRGENORDSPREPARE(MODE, PRSHEET, AMOUNT, REJESTR, NAGZAM, POZZAM,
          PRNAGZAM, PRPOZZAM, OTABLE, OREF, WERSJAREF, DESCRIPT, PRSCHEDZAMDATE,
          AMOUNTINPRORD, AMOUNTINWH, AMOUNTINORD, AUTOSCHED, termdost, prdepart)
        values(2, :prsheet, :amount, :prrejestr, :nagzam, :pozzam,
          null, null, null, null, :wersjaref, :descript, current_date,
          :amountinprord, :amountinwh, :amountinord, :AUTOSCHED, :termdost, :actuprdepart);
  end
  for
    select distinct pzs.wersjaref
      from PRGENORDSPREPARE pzs
      into :wersjaref
  do begin
    amountinprord = null;
    select sum(p.ilosc - p.ilzreal)
      from pozzam p
        left join nagzam n on (p.zamowienie = n.ref)
        left join relations r on r.sreffrom = p.ref and r.stablefrom = 'POZZAM' and r.relationdef = 0 --sprawd
      where p.wersjaref = :wersjaref and p.out = 0
        aND n.rejestr = :prrejestr
        and r.ref is null
      into :amountinprord;
    amountinprord = coalesce(amountinprord,0);
    if(ilwms = 0) then
    begin
      select sum(s.ilosc)
        from stanyil s
        where s.wersjaref = :wersjaref
        into :amountinwh;
    end
    else begin
      select sum(s.quantity - s.blocked + s.ordered)
        from mwsstock s
        where s.vers = :wersjaref
        into :amountinwh;
    end
    amountinwh = coalesce(amountinwh,0);
    --szukanie zlece produkcyjnych pod zamwienia sprzeday
    for
      select pzs.ref, pzs.pozzam, pzs.amount, sum(pzp.ilosc), sum(pzp.ilzreal)
        from PRGENORDSPREPARE pzs
          join relations r on r.srefto = pzs.pozzam and r.stableto = 'POZZAM'
          left join pozzam pzp on r.sreffrom = pzp.ref and r.stablefrom = 'POZZAM'
          where pzs.wersjaref = :wersjaref and pzs.amount > 0
        group by pzs.ref,pzs.pozzam, pzs.amount
        into :actupos, :pozzam, :amount, :ilzdysp, :ilzreal
    do begin
      if(:ilzdysp <> 0) then
      begin
        if(:amount <= :ilzdysp) then begin
          update PRGENORDSPREPARE pzs set pzs.amount = 0, pzs.status = 3,
              pzs.amountinprord = :ilzdysp - :ilzreal
            where pzs.ref = :actupos;
        end else begin
          update PRGENORDSPREPARE pzs set pzs.amount = pzs.amount - :ilzdysp,
              pzs.status = 3, pzs.amountinprord = :ilzdysp
            where pzs.ref = :actupos;
        end
        amountinwh = :amountinwh - :ilzreal;
      end
      actupos = null;
      pozzam = null;
      amount = 0;
      ilzdysp = 0;
      ilzreal = 0;
    end
    -- rozliczenie niepowizanych zamwie i stanw na magazynie
    amountleft = :amountinprord + :amountinwh;
    for
      select pzs.ref, pzs.pozzam, pzs.amount
        from PRGENORDSPREPARE pzs
        where pzs.wersjaref = :wersjaref and pzs.amount > 0
        into :actupos, :pozzam, :amount
    do begin
      if(:amount <= :amountleft) then
      begin
        amountleft = :amountleft - :amount;
        update PRGENORDSPREPARE pzs set pzs.amount = 0, pzs.status = 3,
            pzs.amountinprord = pzs.amount
          where pzs.ref = :actupos;
      end else begin
        update PRGENORDSPREPARE pzs set pzs.amount = pzs.amount - :amountleft,
            pzs.status = 3, pzs.amountinprord = :amountleft
          where pzs.ref = :actupos;
        amountleft = 0;
      end
    end
  end
  --czyszczenie z zapotrzebowania poniej zera
  if (:SHOWEMPTY <> 1 ) then
    delete from PRGENORDSPREPARE where amount <= 0;
end^
SET TERM ; ^
