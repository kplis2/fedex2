--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_KPL_QUANTITY(
      BRANCH ODDZIAL_ID,
      WH DEFMAGAZ_ID,
      VERS integer,
      MODE smallint,
      KPLNAGREF integer = 0,
      GL smallint = 0)
  returns (
      NUMBER integer,
      DESCRIPT varchar(100) CHARACTER SET UTF8                           ,
      PM varchar(1) CHARACTER SET UTF8                           ,
      QUANTITY numeric(15,4))
   as
declare variable kplvers integer;
declare variable convertval numeric(15,4);
declare variable kplversq numeric(15,4);
declare variable kplpozq numeric(15,4);
declare variable constlocq numeric(15,4);
declare variable wgordsq numeric(15,4);
declare variable waitingq numeric(15,4);
declare variable sellordsq numeric(15,4);
begin
  if (coalesce(:kplnagref,0) = 0 and coalesce(:vers,0) > 0 and coalesce(:gl,0) > 0) then
  begin
    select ref
      from kplnag n
      where n.wersjaref = :vers
        and n.akt = 1
        and n.glowna = 1
    into :kplnagref;
  end

  --magazyn
  if (coalesce(:wh,'') <> '') then
  begin
    for
      select p.wersjaref, p.ilosc
        from kplpoz p
        where p.nagkpl = :kplnagref
      into :kplvers, :convertval
    do begin
      --dostepne na lokacjach
      if (:constlocq is null or :constlocq > 0) then
      begin
        kplversq = 0;
        select coalesce(sum(s.quantity - s.blocked + s.ordered),0)
          from mwsstock s
            left join mwsconstlocs c on (s.mwsconstloc = c.ref)
            left join defmagaz d on (d.symbol = c.wh)
          where c.act > 0
            and c.locdest in (1,2,3,4,5,9)
            and d.mws = 1
            and s.vers = :kplvers
            and s.wh = :wh
        into :kplversq;
  
        if (coalesce(:kplversq, 0) <> 0) then
        begin
          kplpozq = :kplversq / :convertval;
          if (:constlocq is null or :constlocq > :kplpozq) then
            constlocq = :kplpozq;
        end
        else constlocq = 0;
      end

      --na zleceniach wg
      if (:wgordsq is null or :wgordsq > 0) then
      begin
        kplversq = 0;
        select coalesce(sum(a.quantity),0)
          from mwsords o
            left join mwsacts a on (a.mwsord = o.ref)
          where o.mwsordtypedest = 9
            and o.status in (1,2)
            and a.status > 0 and a.status < 6
            and a.vers = :vers
            and a.wh = :wh
        into :kplversq;
  
        if (coalesce(:kplversq, 0) <> 0) then
        begin
          kplpozq = :kplversq / :convertval;
          if (:wgordsq is null or :wgordsq > :kplpozq) then
            wgordsq = :kplpozq;
        end
        else wgordsq = 0;
      end

      --oczekuje na zlecenia
      if (:waitingq is null or :waitingq > 0) then
      begin
        kplversq = 0;
        select coalesce(sum(case when n.mwsdisposition = 1 then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0)),0)
          from dokumpoz p
            left join dokumnag n on (n.ref = p.dokument)
            left join defdokummag d on (d.typ = n.typ and d.magazyn = n.magazyn)
          where p.wersjaref = :vers
            and p.genmwsordsafter = 1
            and n.magazyn = :wh
            and n.wydania = 1
            and n.mwsdoc = 1
            and n.frommwsord is null
            and ((n.akcept = 1 and p.iloscl > p.ilosconmwsacts) or
                 (n.mwsdisposition = 1 and p.ilosc > p.ilosconmwsacts))
            and coalesce(p.fake,0) = 0
            and coalesce(p.havefake,0) = 0
            and coalesce(d.afterackproc,'') <> ''
            and d.mwsordwaiting = 1
        into kplversq;

        if (coalesce(:kplversq, 0) <> 0) then
        begin
          kplpozq = :kplversq / :convertval;
          if (:waitingq is null or :waitingq > :kplpozq) then
            waitingq = :kplpozq;
        end
        else waitingq = 0;
      end

      --blokady na zamowieniach
      if (:mode = 0 and :sellordsq is null or :sellordsq > 0) then
      begin
        kplversq = 0;
        select sum(s.zablokow)
          from stanyil s
          where s.wersjaref = :vers and s.magazyn = :wh
        into :kplversq;

        if (coalesce(:kplversq, 0) <> 0) then
        begin
          kplpozq = :kplversq / :convertval;
          if (:sellordsq is null or :sellordsq > :kplpozq) then
            sellordsq = :kplpozq;
        end
        else sellordsq = 0;
      end

    end

    number = 0;
    if (coalesce(:constlocq,0) <> 0) then
    begin
      number = :number + 1;
      descript = 'Na lokacjach do sprzedaży';
      quantity = :constlocq;
      pm = '+';
      suspend;
    end
    if (coalesce(:wgordsq,0) <> 0) then
    begin
      number = :number + 1;
      descript = 'Dostawienie na lok. do sprz.';
      quantity = :wgordsq;
      pm = '+';
      suspend;
    end
    if (coalesce(:waitingq,0) <> 0) then
    begin
      number = :number + 1;
      descript = 'Oczekuje na zlecenia';
      quantity = :waitingq;
      pm = '-';
      suspend;
    end

    if (coalesce(:sellordsq,0) <> 0) then
    begin
      number = :number + 1;
      descript = 'Zablokowano na zamówieniach';
      quantity = :sellordsq;
      pm = '-';
      suspend;
    end

    number = :number + 1;
    descript = 'Suma';
    quantity = coalesce(:constlocq,0) + coalesce(:wgordsq,0) - coalesce(:waitingq,0) - coalesce(:sellordsq,0);
    pm = '';
    suspend;
  end
  --oddzial
  else if (coalesce(:branch,'') <> '') then
  begin
    for
      select p.wersjaref, p.ilosc
        from kplpoz p
        where p.nagkpl = :kplnagref
      into :kplvers, :convertval
    do begin
      --dostepne na lokacjach
      if (:constlocq is null or :constlocq > 0) then
      begin
        kplversq = 0;
        select coalesce(sum(s.quantity - s.blocked + s.ordered),0)
          from mwsstock s
            left join mwsconstlocs c on (s.mwsconstloc = c.ref)
            left join defmagaz d on (d.symbol = c.wh)
          where c.act > 0
            and c.locdest in (1,2,3,4,5,9)
            and d.mws = 1
            and s.vers = :kplvers
            and s.branch = :branch
        into :kplversq;
  
        if (coalesce(:kplversq, 0) <> 0) then
        begin
          kplpozq = :kplversq / :convertval;
          if (:constlocq is null or :constlocq > :kplpozq) then
            constlocq = :kplpozq;
        end
        else constlocq = 0;
      end

      --na zleceniach wg
      if (:wgordsq is null or :wgordsq > 0) then
      begin
        kplversq = 0;
        select coalesce(sum(a.quantity),0)
          from mwsords o
            left join mwsacts a on (a.mwsord = o.ref)
          where o.mwsordtypedest = 9
            and o.status in (1,2)
            and a.status > 0 and a.status < 6
            and a.vers = :vers
            and o.branch = :branch
        into :kplversq;
  
        if (coalesce(:kplversq, 0) <> 0) then
        begin
          kplpozq = :kplversq / :convertval;
          if (:wgordsq is null or :wgordsq > :kplpozq) then
            wgordsq = :kplpozq;
        end
        else wgordsq = 0;
      end

      --oczekuje na zlecenia
      if (:waitingq is null or :waitingq > 0) then
      begin
        kplversq = 0;
        select coalesce(sum(case when n.mwsdisposition = 1 then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsacts,0)),0)
          from dokumpoz p
            left join dokumnag n on (n.ref = p.dokument)
            left join defdokummag d on (d.typ = n.typ and d.magazyn = n.magazyn)
          where p.wersjaref = :vers
            and p.genmwsordsafter = 1
            and n.oddzial = :branch
            and n.wydania = 1
            and n.mwsdoc = 1
            and n.frommwsord is null
            and ((n.akcept = 1 and p.iloscl > p.ilosconmwsacts) or
                 (n.mwsdisposition = 1 and p.ilosc > p.ilosconmwsacts))
            and coalesce(p.fake,0) = 0
            and coalesce(p.havefake,0) = 0
            and coalesce(d.afterackproc,'') <> ''
            and d.mwsordwaiting = 1
        into kplversq;

        if (coalesce(:kplversq, 0) <> 0) then
        begin
          kplpozq = :kplversq / :convertval;
          if (:waitingq is null or :waitingq > :kplpozq) then
            waitingq = :kplpozq;
        end
        else waitingq = 0;
      end

      --blokady na zamowieniach
      if (:mode = 0 and :sellordsq is null or :sellordsq > 0) then
      begin
        kplversq = 0;
        select sum(s.zablokow)
          from stanyil s
          where s.wersjaref = :vers
            and s.oddzial = :branch
        into :kplversq;

        if (coalesce(:kplversq, 0) <> 0) then
        begin
          kplpozq = :kplversq / :convertval;
          if (:sellordsq is null or :sellordsq > :kplpozq) then
            sellordsq = :kplpozq;
        end
        else sellordsq = 0;
      end
    end

    number = 0;
    if (coalesce(:constlocq,0) <> 0) then
    begin
      number = :number + 1;
      descript = 'Na lokacjach do sprzedaży';
      quantity = :constlocq;
      pm = '+';
      suspend;
    end
    if (coalesce(:wgordsq,0) <> 0) then
    begin
      number = :number + 1;
      descript = 'Dostawienie na lok. do sprz.';
      quantity = :wgordsq;
      pm = '+';
      suspend;
    end
    if (coalesce(:waitingq,0) <> 0) then
    begin
      number = :number + 1;
      descript = 'Oczekuje na zlecenia';
      quantity = :waitingq;
      pm = '-';
      suspend;
    end

    if (coalesce(:sellordsq,0) <> 0) then
    begin
      number = :number + 1;
      descript = 'Zablokowano na zamówieniach';
      quantity = :sellordsq;
      pm = '-';
      suspend;
    end

    number = :number + 1;
    descript = 'Suma';
    quantity = coalesce(:constlocq,0) + coalesce(:wgordsq,0) - coalesce(:waitingq,0) - coalesce(:sellordsq,0);
    pm = '';
    suspend;
  end
end^
SET TERM ; ^
