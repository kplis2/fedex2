--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ACCOUNTING_GET_DESCRIPT(
      ACCOUNT ACCOUNT_ID,
      YEARID integer,
      COMPANY smallint)
  returns (
      DESCRIPT varchar(80) CHARACTER SET UTF8                           ,
      FULLDESCRIPT varchar(255) CHARACTER SET UTF8                           )
   as
declare variable BKACCOUNT integer;
declare variable DOOPISU smallint;
declare variable K smallint;
declare variable SEP varchar(1);
declare variable DICTDEF integer;
declare variable LEN smallint;
declare variable TMP varchar(20);
declare variable TMPDESCRIPT varchar(2048);
declare variable TMPDESCRIPT2 varchar(2048);
declare variable TMPFULLDESCRIPT varchar(2048);
declare variable DICTPOS integer;
begin
  select ref, coalesce(descript,''), coalesce(doopisu,0)
    from bkaccounts where symbol = substring(:account from 1 for 3) and yearid = :yearid
      and company = :company
    into :bkaccount, :tmpfulldescript, :doopisu;



  -- uzupelniamy descript jeżeli descript konta syntetycznego ma być brany pod uwage
  if(doopisu = 1) then
    tmpdescript = :tmpfulldescript;
  else
    tmpdescript = '';

  k = 4;
  for
    select separator, dictdef, len, coalesce(doopisu, 0)
      from accstructure
      where bkaccount = :bkaccount
      order by nlevel
      into :sep, :dictdef, :len, :doopisu
  do begin
    if (sep <> ',') then
      k = k +1;
    tmp = substring(:account from  k for  k + len - 1);
    execute procedure name_from_bksymbol(:company, :dictdef, :tmp)
      returning_values :tmpdescript2, :dictpos;
    k = k + len;
    if(:tmpdescript2 is not null) then begin
      -- uzupelniamy fulldescript o opis z konta analitycznego
      if(:tmpfulldescript <> '') then
        tmpfulldescript = :tmpfulldescript || ' | ';
      tmpfulldescript = :tmpfulldescript || :tmpdescript2;
      -- uzupelniamy opis jezeli descript konta syntetycznego ma byc brany pod uwage
      if(:doopisu = 1) then begin
        if(:tmpdescript <> '') then
          tmpdescript = :tmpdescript || ' | ';
        tmpdescript = :tmpdescript || :tmpdescript2;
      end
    end
  end

  if(k = 4) then  -- jeżeli jest tylko syntetyka to bierzemy opis z syntetyki niezależnie od flagi ...
    tmpdescript = tmpfulldescript;
  else if(:tmpdescript = '') then -- w innym wypadku jezeli nie wybrano zadnego poziomu do opisu bierzemy descript z najniższej analityki
    tmpdescript = :tmpdescript2;

  -- zgodnosc dlugosci pol
  descript = substring(:tmpdescript from 1 for 80);
  fulldescript = substring(:tmpfulldescript from 1 for 255);

  suspend;
end^
SET TERM ; ^
