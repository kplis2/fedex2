--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_SOCIALFUNDS(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      ECOLUMN integer)
  returns (
      RET numeric(14,2))
   as
declare variable fromdate date;
  declare variable todate date;
begin

--   exception test_break EMPLOYEE||' '||payroll||' '||prefix||' '||ecolumn;

  --JK: personel - do liczenia sumy swiadczen socjalnych
  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  select sum(e.amount)
    from esocialfunds e
    where e.employee = :employee and e.fundtype = :ecolumn
      and e.funddate >= :fromdate and e.funddate <= :todate
      and (e.epayroll is null or e.epayroll = :payroll)
    into :ret;

  update esocialfunds set epayroll = :payroll
    where employee = :employee and fundtype = :ecolumn
      and funddate >= :fromdate and funddate <= :todate
      and epayroll is null;

  if (ret is null) then
    ret = 0;
  suspend;
end^
SET TERM ; ^
