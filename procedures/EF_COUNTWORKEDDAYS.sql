--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_COUNTWORKEDDAYS(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable FROMDATE date;
declare variable TODATE date;
declare variable WORKDAYS integer;
begin
  --DU: personel - funkcja podaje wartosc skladnika z listy plac
  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;
  select wd from ecal_work(:employee, :fromdate, :todate)
    into ret;

  select sum(workdays)
    from eabsences
    where employee = :employee and correction = 0
      and fromdate >= :fromdate and todate <= :todate
    into :workdays;

  if (workdays is not null) then
    ret = ret - workdays;
  suspend;
end^
SET TERM ; ^
