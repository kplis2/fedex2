--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR83555_GET_INDEX_LIST(
      SESSIONID integer)
  returns (
      INDEXOUT STRING20,
      NAME STRING)
   as
declare variable cnt    integer;
declare variable i      integer;
declare variable los    string20;
begin
  cnt = cast(floor(rand()*50) as integer);
  i = 0;
  while (i<cnt) do
  begin
    los = '';
    while(char_length(los)<15)
      do los = los || CAST(CAST(FLOOR(RAND()*10) as integer) AS varchar(2));
    indexout = los;

    name = 'Towar '||los;
    suspend;
    i = i + 1;
  end
end^
SET TERM ; ^
