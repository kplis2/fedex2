--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHECK_ADMIN as
declare variable cnt integer;
begin
  select count(*) from S_USERS where  USERLOGIN = 'Administrator' into :cnt;
  if(:cnt is null) then cnt = 0;
  if(:cnt = 0)  then
    insert into S_USERS(USERLOGIN,SUPERVISOR) VALUES('Administrator',1);
end^
SET TERM ; ^
