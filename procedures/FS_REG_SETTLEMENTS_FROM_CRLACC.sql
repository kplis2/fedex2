--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FS_REG_SETTLEMENTS_FROM_CRLACC(
      FSCLRACCH integer)
   as
declare variable dictdef_debit integer;
declare variable dictpos_debit integer;
declare variable nagfak_debit integer;
declare variable account_debit ACCOUNT_ID;
declare variable settlement_debit varchar(20);
declare variable dictdef_credit integer;
declare variable dictpos_credit integer;
declare variable nagfak_credit integer;
declare variable account_credit ACCOUNT_ID;
declare variable settlement_credit varchar(20);
declare variable currdebit numeric(14,2);
declare variable debit numeric(14,2);
declare variable currcredit numeric(14,2);
declare variable toclear numeric(14,2);
declare variable fsclraccp_ref_credit integer;
declare variable fsclracch_date timestamp;
declare variable rozrachp_date timestamp;
declare variable rozrachp_operdate timestamp;
declare variable rate numeric(12,4);
declare variable direction smallint;
declare variable description_credit varchar(20);
declare variable description_debit varchar(20);
declare variable payday timestamp;
declare variable debit_operdate timestamp;
declare variable credit_operdate timestamp;
declare variable debit_rate numeric(12,4);
declare variable credit_rate numeric(12,4);
declare variable company integer;
begin

  select regdate, direction, company from fsclracch where ref=:fsclracch
    into :fsclracch_date, :direction, :company;

  for
    select dictdef, dictpos, account, settlement, currdebit2c, debit, rate, operdate, payday, substring(description from 1 for 20)
    from fsclraccp
    where fsclracch = :fsclracch and side = 0
    order by operdate, rate
    into :dictdef_debit, :dictpos_debit, :account_debit, :settlement_debit, :currdebit, :debit, :debit_rate, :rozrachp_date, :payday, description_debit
  do begin
    -- kontrola czy istnieje rozrachunek
    if (not exists(select company from rozrach where kontofk = :account_debit
        and slodef = :dictdef_debit and slopoz = :dictpos_debit
        and symbfak = :settlement_debit and company = :company)) then
    begin
      if (:settlement_debit is not null) then
        select ref from nagfak where nagfak.symbol = :settlement_debit into :nagfak_debit;
      insert into rozrach (kontofk, slodef, slopoz, symbfak, company, faktura, dataotw, skad, dataplat)
        values(:account_debit, :dictdef_debit, :dictpos_debit, :settlement_debit, :company, :nagfak_debit, current_date, 3, :payday);
    end

    for
      select ref, dictdef, dictpos, account, settlement, currcredit2c - cleared,
          operdate, rate
        from fsclraccp
        where fsclracch = :fsclracch and side = 1 and currcredit - cleared > 0
        order by operdate, rate
        into :fsclraccp_ref_credit, :dictdef_credit, :dictpos_credit, :account_credit, :settlement_credit, :currcredit, :credit_operdate, :credit_rate
    do begin
      if (currdebit>currcredit) then
        toclear = currcredit;
      else
        toclear = currdebit;
      -- kontrola czy istnieje rozrachunek
      if (not exists(select company from rozrach where kontofk = :account_credit
          and slodef = :dictdef_credit and slopoz = :dictpos_credit
          and symbfak = :settlement_credit and company = :company)) then
      begin
        if (:settlement_credit is not null) then
          select ref from nagfak where symbol = :settlement_debit into :nagfak_credit;
        insert into rozrach (kontofk, slodef, slopoz, symbfak, company, faktura, dataotw, skad)
          values(:account_credit, :dictdef_credit, :dictpos_credit, :settlement_credit, :company, :nagfak_credit, current_date, 3);
      end

       -- biore starsza date i przenosze kwote z data starszej operacji
      rozrachp_date = debit_operdate;
      rate = debit_rate;
      if (credit_operdate > rozrachp_date or rozrachp_date is null) then
      begin
        rozrachp_date = credit_operdate;
        rate = credit_rate;
      end
      if (rozrachp_date is null) then
        rozrachp_date = fsclracch_date;

      insert into rozrachp (kontofk, symbfak, slodef, slopoz, company, data, ma, tresc,
          fsclracch, kurs, skad, stable, sref, operacja, fsopertype,stransdate)
        values (:account_debit, :settlement_debit, :dictdef_debit, :dictpos_debit, :company,
          :fsclracch_date, :toclear, 'Rozliczenie z rozrachunkiem ' || :settlement_credit,
          :fsclracch, :rate, 3, 'FSCLRACCH',:fsclracch, :description_credit, 7, :rozrachp_date);

      insert into rozrachp (kontofk, symbfak, slodef, slopoz, company, data, winien, tresc,
          fsclracch, kurs, skad, stable, sref, operacja, fsopertype,stransdate)
        values (:account_credit, :settlement_credit, :dictdef_credit, :dictpos_credit, :company,
          :fsclracch_date, :toclear, 'Rozliczenie z rozrachunkiem ' || :settlement_debit,
          :fsclracch, :rate, 3, 'FSCLRACCH',:fsclracch, :description_credit, 7,:credit_operdate);

      update fsclraccp set cleared = cleared + :toclear
        where ref = :fsclraccp_ref_credit;

      currdebit = currdebit - toclear;
      if (currdebit=0) then
        break;
    end
  end
end^
SET TERM ; ^
