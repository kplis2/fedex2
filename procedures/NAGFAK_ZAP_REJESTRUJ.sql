--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_ZAP_REJESTRUJ(
      TRYB varchar(20) CHARACTER SET UTF8                           ,
      KLIENT integer,
      FAKTURA integer,
      ZAKUP integer,
      KWOTA numeric(15,2),
      KURS numeric(15,2),
      DATA timestamp,
      OPERACJA varchar(20) CHARACTER SET UTF8                           ,
      TRESC varchar(255) CHARACTER SET UTF8                           ,
      SLODEF integer,
      SYMBFAK varchar(30) CHARACTER SET UTF8                           )
   as
declare variable dozaplaty numeric(14,2);
declare variable wn numeric(14,2);
declare variable ma numeric(14,2);
declare variable cnt integer;
declare variable company integer;
declare variable kodks KONTO_ID;
declare variable kontofk KONTO_ID;
begin
  execute procedure get_global_param('CURRENTCOMPANY') returning_values company;
  if(:tryb = 'Faktura') then begin
    if(:zakup = 1) then
      select ref from SLODEF where TYP='DOSTAWCY' into :slodef;
    else
      select ref from SLODEF where TYP='KLIENCI' into :slodef;
    select dozaplaty from NAGFAK where REF=:faktura into :dozaplaty;
    if(:zakup = 1) then dozaplaty = - :dozaplaty;
  end else if(:tryb = 'Rozrachunek') then begin
    select WINIEN - MA, KONTOFK
      from ROZRACH where SLODEF = :slodef and SLOPOZ = :klient and SYMBFAK = :SYMBFAK and company = :company
      into :dozaplaty, :kontofk;
    if(:kontofk is null) then kontofk = '';
  end else begin
    dozaplaty = 0;
    select ref from SLODEF where TYP='KLIENCI' into :slodef;
  end
  if(:kontofk is null) then begin
    select PREFIXFK from SLODEF where REF=:slodef into :kontofk;
    if(:kontofk is null) then kontofk = '';
    if(:kurs is null) then kurs = 1;
    execute procedure KODKS_FROM_DICTPOS(:slodef, :klient) returning_values  :kodks;
    if(:kodks is null) then kodks = '';
    kontofk = :kontofk || :kodks;
  end
  /* srawdzenie naglówka */
  if(:tryb = 'Faktura') then begin
    select SYMBFAK, KONTOFK from ROZRACH where SLOPOZ=:klient and FAKTURA=:faktura into :symbfak, :kontofk;
    if(:symbfak is null) then exception ZAP_REJ_BEZ_ROZRACH;
  end else if(:tryb = 'Zaliczka') then begin
    cnt = NULL;
    select count(*)
      from ROZRACH
      where SLODEF = :slodef AND SLOPOZ = :klient and SYMBFAK = :symbfak and KONTOFK = :kontofk and company = :company
      into :cnt;
    if(cnt is null or (:cnt = 0)) then
      insert into ROZRACH(company, SLODEF, SLOPOZ, SYMBFAK, KONTOFK, KLIENT, FAKTURA,DATAOTW, DATAPLAT)
      values(:company, :SLODEF, :klient, :symbfak, :kontofk, :klient, NULL,:data,:data);

  end
  if(:dozaplaty >=0) then begin
    ma = :kwota;
    wn = 0;
  end else begin
    ma = 0;
    wn = :kwota;
  end
  if(:faktura = 0) then faktura = NULL;
  if(slodef is null or (:slodef = 0)) then exception NAGFAK_BEZSLOKL;
  cnt = NULL;
  select count(*)
    from ROZRACHP
    where SLODEF = :slodef and SLOPOZ = :klient and SYMBFAK = :symbfak and OPERACJA = :operacja and company = :company
    into :cnt;
  if(:cnt > 0) then exception  ROZRACH_ISPRESENT;
  insert into ROZRACHP(SLODEF, SLOPOZ,KLIENT,SYMBFAK,KONTOFK,OPERACJA,FAKTURA,DATA,TRESC,KURS,WINIEN, MA, company)
    values(:slodef,:klient,:klient,:symbfak,:kontofk,:operacja,:faktura,:data,:tresc,:kurs,:wn,:ma, :company);
end^
SET TERM ; ^
