--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_RELASE_MWSORDS(
      REFORD MWSORDS_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
begin
  --procedura zwalniajaca wozki, przypianie wozkow do mwsacta i operatora w przypadku
  --gdy zadna pozycja nie zostala pobrana

  if (exists (select first 1 1 from mwsacts a where a.mwsord=:REFORD and a.status>1)) then
  begin
    status=0;
    msg='Zlecenie czesciowo zrealizowane. Wozek pozostanie przypisany do zlecenia.';
    exit;
  end

  status=1;
  msg='';


/*$$IBEC$$   update mwsords o   --usuniecie przypisania operatora
    set o.operator=null,
        o.status = 1,
        o.timestart = null
    where o.ref=:reford
      and (o.operator is not null or o.status = 2) ; $$IBEC$$*/

  update mwsacts ma  --zwalnianie z mwsacts
    set ma.mwsconstloc = null
    where ma.mwsord = :REFORD
      and ma.mwsconstloc is not null;

  delete from mwsordcartcolours mw
    where mw.mwsord = :REFORD;

end^
SET TERM ; ^
