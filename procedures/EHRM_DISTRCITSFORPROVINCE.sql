--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EHRM_DISTRCITSFORPROVINCE(
      PROVINCEID CPWOJ16M_ID)
  returns (
      DISTRICTID ZUSCODE_ID,
      NAME STRING)
   as
declare variable Provincecode type of varchar_6;
begin
    --pobieramy guscode dla danego wojewodztwa
    select w.guscode from cpwoj16m w
    where w.ref = :provinceid
    into :Provincecode;

    for select gc.ref, gc.descript
     from edictguscodes gc
     where gc.dicttype = 3 and
           (substring(gc.code from 1 for 2) = :Provincecode or :Provincecode is null)
     order by lower(gc.descript) asc
     into :districtid, :name
     do begin
        suspend;
     end
end^
SET TERM ; ^
