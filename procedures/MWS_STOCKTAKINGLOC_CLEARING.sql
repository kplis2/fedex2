--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_STOCKTAKINGLOC_CLEARING(
      MWSSTOCK integer,
      OPERATOR integer,
      DOCTYPEMINUS varchar(3) CHARACTER SET UTF8                           ,
      DOCTYPEPLUS varchar(3) CHARACTER SET UTF8                           ,
      MWSINWGROUPIN integer,
      ACCEPT smallint)
  returns (
      MWSINWGROUPOUT integer,
      DOCSYMBS varchar(255) CHARACTER SET UTF8                           )
   as
declare variable DOCTYPE varchar(3);
declare variable QUANTITY numeric(15,4);
declare variable DOCID integer;
declare variable WH varchar(3);
declare variable GOOD varchar(40);
declare variable PGOOD varchar(40);
declare variable GRUPA varchar(40);
declare variable VERS integer;
declare variable LOT integer;
declare variable DOCSYMBSTMP varchar(255);
declare variable CENA numeric(14,2);
declare variable MAGDOCELOWY varchar(3);
declare variable QUANTITYAVAIBLE numeric(15,4);
declare variable QUANTITYTAKEN numeric(15,4);
declare variable nagkpl kplnag_id;
declare variable alttopoz dokumpoz_id;
begin
  if (exists (select first 1 1 from inwenta where magazyn = :wh and zamk = 0)) then
    exception universal 'Inwentaryzacja roczna - rozliczanie różnic przez Arkusz Inw.';
  if (coalesce(mwsinwgroupin,0) = 0) then mwsinwgroupin = null;
  -- generowanie dokumentów
  if (accept = 0) then
  begin
    select quantity, wh, good, vers, lot
      from mwsstock where ref = :mwsstock
      into quantity, wh, good, vers, lot;
    if (quantity is null) then quantity = 0;
    if (quantity = 0) then
      exit;
    if (quantity < 0) then
    begin
/*      docid = docplusin;
      docminusout = docminusin;*/
      doctype = doctypeplus;
    end
    else
    begin
/*      docid = docminusin;
      docplusout = docplusin;*/
      doctype = doctypeminus;
    end

    for select symbol
      from defmagaz
      where symbol = :wh --XXX Patch
      order by case when :quantity < 0 then magmasterplusord else magmasterminusord end
      into :magdocelowy
    do begin
      cena = 0;
      quantitytaken = 0;
      if (quantity < 0) then
      begin
        quantitytaken = quantity;
        select first 1 i.cena from wmsinpluswycena i where i.wersjaref = :vers
          into cena;
        if (cena = 0) then
          execute procedure INWENTAP_ILPLUS_WYCENA(null,:vers,:magdocelowy,:lot) returning_values cena;
      end
      else
      begin
        quantityavaible = 0;
        quantitytaken = 0;
        select ilosc from stanyil
          where magazyn = :magdocelowy and wersjaref = :vers
          into :quantityavaible;
        if (coalesce(quantityavaible,0) > 0) then
        begin
          if (quantity > quantityavaible) then quantitytaken = quantityavaible;
          else quantitytaken = quantity;
        end
      end
      quantity = quantity - quantitytaken;
      if (abs(quantitytaken) > 0) then
      begin
        docid = null;
        select ref from dokumnag
          where grupasped = :mwsinwgroupin and magazyn = :magdocelowy and typ = :doctype
          into :docid;
        if (coalesce(docid,0) = 0) then
        begin
          execute procedure gen_ref('DOKUMNAG') returning_values docid;
          if (mwsinwgroupin is null) then mwsinwgroupin = docid;
          insert into dokumnag(ref, magazyn, typ, data, zrodlo, operator, operakcept, grupasped)
            values (:docid, :magdocelowy, :doctype, current_date, 0, :operator, :operator, :mwsinwgroupin);
        end
    --    insert into dokumpoz (dokument, ktm, wersjaref, ilosc, cena, dostawa, takefrommwsstock)
      --    values (:docid, :good, :vers, abs(:quantitytaken), :cena, :lot, :mwsstock);
        --<<XXX MKD
        select k.nagkpl from kplpoz k where k.wersjaref = :vers
            into :nagkpl;
        if (:nagkpl is not null) then begin
            insert into dokumpoz (dokument,ktm, wersjaref, ilosc, fake, havefake)
                select :docid, k.ktm, k.wersjaref, abs(:quantitytaken), 0, 1
                    from kplnag k where k.ref = :nagkpl
                returning ref into :alttopoz;

            insert into dokumpoz (dokument, ktm, wersjaref, ilosc, cena,
                    dostawa, takefrommwsstock,fake, havefake, alttopoz)
                values (:docid, :good, :vers, abs(:quantitytaken), :cena, :lot,
                    :mwsstock,1, 0, :alttopoz);

            insert into dokumpoz (dokument, ktm, wersjaref, ilosc, fake,
                        havefake, alttopoz)
                select :docid, k.ktm, k.wersjaref, abs(:quantitytaken), 1,
                        0, :alttopoz
                    from kplpoz k where k.nagkpl = :nagkpl and k.wersjaref <> :vers;

        end else begin
            insert into dokumpoz (dokument, ktm, wersjaref, ilosc, cena, dostawa, takefrommwsstock)
            values (:docid, :good, :vers, abs(:quantitytaken), :cena, :lot, :mwsstock);
        end
        --XXX>> MKD
      end
      if (quantity = 0) then break;
    end
    if (quantity < 0) then exception universal 'Brak magazynu docelowego!';
    if (quantity > 0) then exception universal 'Niewystarczająca ilość na stanach magazynowych dla towaru '||:good;
    mwsinwgroupout = mwsinwgroupin;

  end else if (accept = 1) then
  begin
    docsymbs = '';
    for select ref
      from dokumnag
      where grupasped = :mwsinwgroupin
      order by typ
      into :docid
    do begin
      update dokumnag set akcept = 1 where ref = :docid and akcept = 0;
      select symbol from dokumnag where ref = :docid into :docsymbstmp;
      if (coalesce(docsymbstmp,'') <> '' and docsymbs = '') then
        docsymbs = docsymbs || docsymbstmp;
      else if (coalesce(docsymbstmp,'') <> '' and docsymbs <> '') then
        docsymbs = docsymbs || '; ' || docsymbstmp;
    end
  end
end^
SET TERM ; ^
