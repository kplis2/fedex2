--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_BANKACCOUNT(
      NAGFAKREF integer)
  returns (
      BANK varchar(60) CHARACTER SET UTF8                           ,
      KONTO varchar(40) CHARACTER SET UTF8                           ,
      HBSTATUS integer)
   as
declare variable KLIENTREF integer;
declare variable PLATNIKREF integer;
begin
bank = '';
konto = '';
hbstatus = 0;
  if(nagfakref is null or nagfakref = 0) then exit;
  select nagfak.klient, nagfak.platnik from nagfak
    where nagfak.ref = :nagfakref
    into :klientref, :platnikref;
  if(:platnikref is not null and :platnikref <> 0) then begin
    select first 1 bankaccounts.hbstatus, bankaccounts.account, bankacc.bank from bankaccounts
      join klienci on (klienci.ref = bankaccounts.dictpos and bankaccounts.dictdef = 1)
      join bankacc on (bankacc.symbol = bankaccounts.bankacc)
      where bankaccounts.hbstatus = 1 and bankaccounts.act=1
        and klienci.ref = :platnikref
      into :hbstatus, :konto, :bank;
  end else if(:klientref is not null) then begin
    select first 1 bankaccounts.hbstatus, bankaccounts.account, bankacc.bank from bankaccounts
      join klienci on (klienci.ref = bankaccounts.dictpos and bankaccounts.dictdef = 1)
      join bankacc on (bankacc.symbol = bankaccounts.bankacc)
      where bankaccounts.hbstatus = 1 and bankaccounts.act=1
        and klienci.ref = :klientref
      into :hbstatus, :konto, :bank;
  end
  suspend;
end^
SET TERM ; ^
