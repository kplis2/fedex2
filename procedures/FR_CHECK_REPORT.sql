--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FR_CHECK_REPORT(
      FRVHDR integer)
   as
declare variable company integer;
declare variable frversion integer;
declare variable period varchar(6);
declare variable frhdr varchar(20);
declare variable frpsn integer;
declare variable symbol varchar(15);
declare variable alg integer;
declare variable colref integer;
declare variable col integer;
declare variable forbo integer;
declare variable forperiod varchar(6);
declare variable first_colref integer;
declare variable algtype integer;
declare variable tempcol integer;
declare variable statement_text varchar(255);
declare variable ret numeric(14,2);
declare variable amount numeric(14,2);
declare variable ratio integer;
declare variable frvpsn integer;
declare variable rowsymbol varchar(20);
declare variable functionname varchar(20);
declare variable decl varchar(8191);
declare variable body varchar(8191);
begin

  select period, frhdr, company, frversion from frvhdrs where ref = :frvhdr
    into :period, :frhdr, :company, :frversion;

  delete from frchkaccounts where frhdr = :frhdr and period = :period;
  delete from frchkaccounts where frhdr = :frhdr and period = substring(period from 1 for 4) || '00';

  for
    select P.ref, P.symbol, P.algorithm, C.ref, C.number, C.forbo
      from frpsns P
        join frcols C on (C.frhdr = P.frhdr and P.frhdr=:frhdr)
      order by P.countord, P.number, C.number
      into :frpsn, :symbol, :alg,  :colref, :col, :forbo
  do begin

    insert into frvpsns (frvhdr, symbol, col, frpsn, frcol)
      values (:frvhdr, :symbol, :col, :frpsn, :colref);

    select ref from frvpsns where frvhdr = :frvhdr and frpsn = :frpsn and frcol = :colref
      into :frvpsn;

    if (col = 1) then
      first_colref = colref;

    forperiod = period;
-- wyrażenie
    if (alg = 3 and forbo <> 1) then
      if (exists(select frpsn from frpsnsalg where frpsn = :frpsn and frcol = :colref
        and frversion = :frversion)) then
      begin
        select algtype
          from frpsnsalg
          where frpsn = :frpsn and frcol = :colref and frversion = :frversion
          into :algtype;

        if (algtype = 2) then
          tempcol = first_colref;
        else
          tempcol = colref;

        select decl, body
          from frpsnsalg
          where frpsn = :frpsn and frcol = :tempcol and frversion = :frversion
          into :decl, :body;

        if (algtype > 0) then
        begin
        /* [DK] Zg. 40548 - wyrownanie z fr_calculate_report
          if (not exists(select * from rdb$procedures
            where rdb$procedure_name = 'XX_FR_ALGORITHM_'||:frpsn||'_'||:tempcol)) then
          begin
            update frpsnsalg set body = ''
              where frpsn = :frpsn and frcol = :tempcol and frversion = 0;
          end

          statement_text = 'select ret from XX_FR_ALGORITHM_' || :frpsn || '_'
            || :tempcol ||'(' || :frvpsn || ', ' || :forperiod || ')';

          execute statement statement_text into :ret;  */

          execute procedure STATEMENT_EXECUTE(:decl, :body, :frvpsn, :forperiod, 0)
            returning_values :ret;

          when any
            do exception universal 'Błąd w wykonaniu algorytmu dla pozycji ' || symbol;

        end
      end
  end
end^
SET TERM ; ^
