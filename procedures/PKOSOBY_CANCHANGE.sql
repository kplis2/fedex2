--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PKOSOBY_CANCHANGE(
      REF integer)
  returns (
      STATUS integer)
   as
declare variable cnt integer;
begin
  status = 0;
  select count(*) from CAKCJEPOZ where PKOSOBA=:ref into :cnt;
  if(:cnt>0) then exit;
  select count(*) from CKONTRAKTY where OSOBA=:ref into :cnt;
  if(:cnt>0) then exit;
  select count(*) from CNOTATKI where PKOSOBA=:ref into :cnt;
  if(:cnt>0) then exit;
  select count(*) from EMAILADR where PKOSOBA=:ref into :cnt;
  if(:cnt>0) then exit;
  select count(*) from KONTAKTY where PKOSOBA=:ref into :cnt;
  if(:cnt>0) then exit;
  select count(*) from REZNAG where PKOSOBA=:ref into :cnt;
  if(:cnt>0) then exit;
  select count(*) from ZADANIA where OSOBA=:ref into :cnt;
  if(:cnt>0) then exit;
  status = 1;
end^
SET TERM ; ^
