--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_HASHPRORDS as
declare variable N integer;
declare variable REFNAGZAM integer;
declare variable REFPOZZAM integer;
declare variable DATAWE timestamp;
declare variable MAGAZYN varchar(3);
declare variable REJZAM varchar(3);
declare variable TYPZAM varchar(3);
declare variable SHEET integer;
declare variable KTM varchar(40);
declare variable ILOSC integer;
declare variable OUT1 integer;
declare variable OUT2 integer;
declare variable OUT3 integer;
declare variable PRDEPART varchar(10);
declare variable ID varchar(40);
declare variable REF integer;
begin
  n = 500;
  magazyn = 'CG';
  rejzam = 'PPP';
  typzam = 'PRO';
  prdepart = 'GL';

  while(n>0) do begin
    --wygeneruj naglowek zlecenia
    execute procedure GEN_REF('NAGZAM') returning_values :refnagzam;
    datawe = cast('2001-01-01' as timestamp) + 3650*rand();
    insert into nagzam (REF,DATAWE,MAGAZYN,MAG2,REJESTR,TYPZAM,PRDEPART)
    values (:refnagzam,:datawe,:magazyn,:magazyn,:rejzam,:typzam,:prdepart);

    --wygeneruj jedna pozycje zlecenia
    select first 1 ref,ktm from prsheets
    where status=2 order by rand()
    into :sheet, :ktm;
    ilosc = cast(rand()*1000 as integer);
    execute procedure GEN_REF('POZZAM') returning_values :refpozzam;
    insert into pozzam (REF,ZAMOWIENIE,magazyn, KTM, PRSHEET, ILOSC, OUT)
    values (:refpozzam, :refnagzam, :magazyn, :ktm, :sheet, :ilosc, 0);

    --wygeneruj pozycje surowcowe
    execute procedure pr_gen_prodords(:refnagzam,3,:sheet, :ilosc, :rejzam, :refnagzam, :refpozzam, 0, 0, 0, 0, 0, 1)
    returning_values out1, out2, out3;

    --dodaj do harmonogramu i wygeneruj przewodniki
    execute procedure PRSCHED_ADD_PRORD(null, :refnagzam);

    n = :n -1;
  end

  for select g.ref,n.datawe
  from prschedguides g
  join pozzam p on (g.pozzam=p.ref)
  join nagzam n on (p.zamowienie=n.ref)
  where g.starttime is null
  into :ref,:datawe
  do begin
    datawe = :datawe +30*rand();
    update prschedguides set starttime=:datawe, endtime=:datawe+2*rand() where ref=:ref;
  end
end^
SET TERM ; ^
