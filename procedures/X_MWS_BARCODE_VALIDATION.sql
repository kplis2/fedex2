--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_BARCODE_VALIDATION(
      BARCODE STRING100,
      TRYB INTEGER_ID = 0)
  returns (
      GOOD KTM_ID,
      VERS WERSJE_ID,
      QUANTITY NUMERIC_14_4,
      STATUS SMALLINT_ID,
      UNIT JEDN_MIARY,
      UNITID TOWJEDN,
      CNT INTEGER_ID,
      VERSLIST STRING255,
      X_SERIA STRING255,
      X_PARTIA STRING20,
      X_SLOWNIK_REF INTEGER_ID)
   as
declare variable TYMPRZELICZ numeric(14,4);
declare variable TYMVERS integer;
begin
-- tryb
--     0 szukam wszedzie,  
--     1 nie szukam w x_mws_serie ,

--  exception test_break :barcode;
  -- weryfikacja czy kod kreskowy jest unikalny !!!
  --<<<XXX MKD zg133618
  execute procedure x_mws_barcodehermon (barcode)
     returning_values :barcode;
  -->>>XXX MKD zg133618

  if (position(';',BARCODE)>0) then
    BARCODE=substring(BARCODE from 1 for position(';',BARCODE)-1);

  select  count(distinct vers) , min(vers), min(good), min(unitid), min(quantity), min(unit), min(X_SERIA), min(X_PARTIA), min(X_SLOWNIK_REF)
  from  X_MWS_BARCODELIST(:barcode, :tryb)
  into :cnt, :verslist, :good, :unitid, :quantity, :unit, :X_SERIA,  :X_PARTIA, :X_SLOWNIK_REF;

  if (cnt > 1) then
  begin
    --delete from towkodkresk k where k.kodkresk = :barcode and coalesce(char_length(:barcode),0) > 5; --[PM] XXX no nieee
    --exception universal 'W systemie istnieje wiecej niz jeden towar o tym kodzie!';
    status = 2;
    suspend;
    exit;
  end
  else
    vers=verslist;

  if (good is not null and vers is null) then
  begin
    select first 1 ref from wersje w where w.akt = 1 and w.ktm = :good and coalesce(char_length(:barcode),0) > 5 order by nrwersji
      into vers;
  end

  if (good is null and position('/' in :barcode) > 0) then
  begin
    tymvers = cast(substring(:barcode from 1 for position('/' in :barcode)-1) as integer);
    tymprzelicz = cast(substring(:barcode from position('/' in :barcode)+1 for coalesce(char_length(:barcode),0)) as numeric(15,4));
    select first 1 w.ref, w.ktm, o.ref, o.jedn
      from wersje w
        left join towjedn o on (o.ktm = w.ktm and o.przelicz = :tymprzelicz)
      where w.ref = :tymvers
      into vers, good, unitid, unit;
    quantity = :tymprzelicz;
  end

   --  MatJ Ehrle do zmiany na nasze pakowanie
  -- [DG] XXX Logbox: nowe okno do pakowania: jezeli dostaje kod w postaci 'KTM:ABCD',
  -- to trzeba szukac towaru po KTMie 'ABCD'       --do zmiany na nasze pakowanie
  if (:good is null
        and :barcode starting with 'KTM:') then
  begin
    select first 1 w.ref, w.ktm, o.ref, o.jedn, o.przelicz
      from wersje w
        left join towjedn o on (o.ktm = w.ktm and o.glowna = 1)
      where w.ktm = substring(:barcode from 5)
    into vers, good, unitid, unit, :quantity;
  end
  -- [DG] XXX end

  if (vers is null) then vers = 0;
  if (:good is not null) then status = 1;
  else status = 0;
  suspend;
end^
SET TERM ; ^
