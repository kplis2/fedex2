--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ERECRUITSAPP_SAVEANSWER(
      QUESTIONREF EQUESTPOS_ID,
      LP SMALLINT_ID,
      ANSWER EQUESTANSWERSTRING)
   as
begin
  insert into equestposans(EQUESTPOS,ANSWER,LP)
  values (:questionref,:answer,:lp);
end^
SET TERM ; ^
