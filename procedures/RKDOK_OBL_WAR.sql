--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RKDOK_OBL_WAR(
      DOKUMENT integer)
   as
declare variable PRZYCHOD numeric(14,2);
declare variable PRZYCHODZL numeric(14,2);
declare variable ROZCHOD numeric(14,2);
declare variable ROZCHODZL numeric(14,2);
declare variable ELECTRONIC integer;
declare variable WALUTOWA smallint;
begin
  select n.electronic, s.walutowa
    from rkdoknag n
      join rkstnkas s on (n.stanowisko = s.kod)
    where n.ref = :dokument
    into :electronic, :walutowa;
  select sum(iif((p.pm = '+' and p.kwota > 0) or (p.pm = '-' and p.kwota < 0),abs(p.kwota),0)),
      sum(iif((p.pm = '-' and p.kwota > 0) or (p.pm = '+' and p.kwota < 0),abs(p.kwota),0)),
      sum(iif((p.accept = 1 ), (iif((p.pm = '+' and p.kwotazlfifo > 0) or (p.pm = '-' and p.kwotazlfifo < 0),abs(p.kwotazlfifo),0)),
        (iif((p.pm = '+' and p.kwotazl > 0) or (p.pm = '-' and p.kwotazl < 0),abs(p.kwotazl),0)))),
      sum(iif((p.accept = 1 ), (iif((p.pm = '-' and p.kwotazlfifo > 0) or (p.pm = '+' and p.kwotazlfifo < 0),abs(p.kwotazlfifo),0)),
         (iif((p.pm = '-' and p.kwotazl > 0) or (p.pm = '+' and p.kwotazl < 0),abs(p.kwotazl),0))))
    from rkdokpoz p
    where p.dokument = :dokument
      and p.inneks=0 --and p.accept = 1
    into :przychod, :rozchod, :przychodzl, :rozchodzl;

  przychod = coalesce(przychod,0);
  przychodzl = coalesce(przychodzl,0);
  rozchod = coalesce(rozchod,0);
  rozchodzl = coalesce(rozchodzl,0);
  if (:electronic = 0) then
    update RKDOKNAG n set
        n.KWOTA = abs(:przychod - :rozchod),
        n.KWOTAZL= abs(:przychodzl - :rozchodzl),
        n.przychodzl = :przychodzl,
        n.rozchodzl = :rozchodzl
      where n.REF=:dokument;
  else if (:walutowa = 1) then
    update rkdoknag n set n.kwotazl = abs(:przychodzl - :rozchodzl) where n.ref = :dokument;
  else
    update rkdoknag n set n.kwota = abs(:przychod - :rozchod) where n.ref = :dokument;
end^
SET TERM ; ^
