--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRHDRS_AUTORECALC as
declare variable symbol varchar(40);
begin
  for
    select f.symbol
      from frhdrs f
      where f.autorefilled = 1
      into symbol
  do begin
    execute procedure FRDCELLS_AUTOADD(:symbol);
  end
end^
SET TERM ; ^
