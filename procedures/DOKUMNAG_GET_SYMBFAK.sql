--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_GET_SYMBFAK(
      DOKUMNAGREF integer,
      NAGFAKREF integer)
  returns (
      NEWSYMBOL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable symbol varchar(40);
begin
  newsymbol = '';
  -- znajdz wszystkie faktury do dokumentu magazynowego dzieki powiazaniu przez pozycje
  for select distinct NAGFAK.SYMBOL
    from DOKUMPOZ
    left join POZFAK on (DOKUMPOZ.FROMPOZFAK=POZFAK.REF)
    left join NAGFAK on (NAGFAK.REF=POZFAK.DOKUMENT)
    where DOKUMPOZ.dokument=:dokumnagref
    into :symbol
  do begin
    if(:symbol is not null) then begin
      if (coalesce(char_length(:newsymbol),0) < 200)then begin -- [DG] XXX ZG119346
        if(:newsymbol <> '') then newsymbol = :newsymbol || ';';
        newsymbol = :newsymbol || :symbol;
      end
    end
  end
  -- dla dokumagow ktore jeszcze nie maja pozycji, sprawdz takze powiazanie naglowkowe przekazane w parametrze
  symbol = '';
  if(:nagfakref is not null) then begin
    select SYMBOL from NAGFAK where ref = :nagfakref into :symbol;
    if(position(:symbol in :newsymbol)=0) then begin
      if (coalesce(char_length(:newsymbol),0) < 200)then begin -- [DG] XXX ZG119346
        if(:newsymbol <> '') then newsymbol = :newsymbol || ';';
        newsymbol = :newsymbol || :symbol;
      end
    end
  end
end^
SET TERM ; ^
