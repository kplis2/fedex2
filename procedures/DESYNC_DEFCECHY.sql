--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DESYNC_DEFCECHY(
      NAZWA varchar(30) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE il INTEGER;
begin
   select count(*) from LOG_FILE where TABLE_NAME='DEFCECHY' and KEY1=:NAZWA into :il;
   if(:il is null) then il = 0;
   if(:il = 0) then
      insert into LOG_FILE(TABLE_NAME,TIME_STAMP,KEY1) values ('DEFCECHY',CURRENT_TIMESTAMP(0),:NAZWA);
   else
     update LOG_FILE set TIME_STAMP=CURRENT_TIMESTAMP(0) where TABLE_NAME='DEFCECHY' and KEY1=:NAZWA;
end^
SET TERM ; ^
