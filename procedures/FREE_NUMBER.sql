--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FREE_NUMBER(
      NAZWA varchar(40) CHARACTER SET UTF8                           ,
      REJESTR varchar(20) CHARACTER SET UTF8                           ,
      DOKUMENT varchar(20) CHARACTER SET UTF8                           ,
      DATA date,
      NUMBER integer,
      DOBLOCK integer)
  returns (
      BLOCKREF integer)
   as
declare variable KEY_VAL varchar(30);
declare variable IS_REJ smallint;
declare variable IS_DOK smallint;
declare variable TYP_OKRES smallint;
declare variable OKRES varchar(8);
declare variable OLD_NUMBER integer;
declare variable BRAK smallint;
declare variable ODDZIAL varchar(20);
declare variable CHRONO smallint;
declare variable LASTDATA timestamp;
declare variable IS_ODDZIAL smallint;
declare variable NUMREF integer;
declare variable BLOCKMASTER integer;
declare variable ISf_company smallint;
declare variable current_company integer;
begin
  --exception test_break :NAZWA ||';'|| :REJESTR ||';'|| :DOKUMENT ||';'|| :DATA ||';'|| :NUMBER ||';'|| DOBLOCK;
  if(:doblock is null) then doblock = 0;
  if(:doblock < 0) then begin
    blockmaster = - :doblock;
    doblock = 1;
  end
  execute procedure GETCONFIG('AKTUODDZIAL') returning_values :oddzial;
  select rejestr, dokument, oddzial, okres, braki, chrono, company  from NUMBERGEN where NAZWA = :nazwa
      into :is_rej, :is_dok, :is_oddzial, :typ_okres, :brak, :chrono, :ISf_company;
  if (isf_company = 1) then
    execute procedure get_global_param('CURRENTCOMPANY')
      returning_values :current_company;
  else
    current_company = -1;
  if(:is_oddzial=0) then oddzial='XXX'; /* numeracja wspolna dla oddzialow prowadzonych w jednej bazie */
  if(:brak = 1) then begin
      key_val = '';
      if(:rejestr is null) then rejestr = '';
      if(:dokument is null) then dokument = '';
      if(:is_rej = 1) then key_val = key_val || rejestr;
      if(:is_dok = 1) then key_val = key_val || dokument;
      okres = cast( extract( year from :data) as char(4));
      if(:typ_okres = 1) then begin
        /* data pelna*/
        if(extract(month from :data) < 10) then
           okres = :okres ||'0'||cast(extract(month from :data) as char(1));
        else
           okres = :okres ||cast(extract(month from :data) as char(2));
      end
      if(:okres is null) then okres = '';
      key_val = key_val || okres;
      -- pobierz ostatni zajety numer i jego date
      select min(number), min(lastdata) from NUMBERFREE where oddzial = :oddzial and nazwa = :nazwa and key_val = :key_val and typ = 0 and (:ISF_company != 1 or company = :current_company) into :old_number, :lastdata;
      if(:chrono > 1 and :doblock = 0) then begin
        select min(lastdata) from NUMBERFREE where oddzial = :oddzial and nazwa = :nazwa and key_val = :key_val and typ = 0 and (:ISF_company != 1 or company = :current_company) into :lastdata;
        if(:lastdata > :data) then
          exception GENERATOR_CHRONOSAAFTER;
      end
      -- usun numer z listy numerow zajetych
      if(:chrono > 0) then begin
        delete from NUMBERFREE where oddzial = :oddzial and nazwa = :nazwa and key_val = :key_val and typ = 2 and number = :number and (:ISF_company != 1 or company = :current_company);
        --okrelenie daty ostatniego dokumentu
        lastdata = null;
        select first 1 lastdata from NUMBERFREE
          where oddzial = :oddzial and nazwa = :nazwa and key_val = :key_val and typ = 2 and (:ISF_company != 1 or company = :current_company)
          order by NAZWA desc,KEY_VAL desc,TYP desc,ODDZIAL desc,LASTDATA desc
          into :lastdata;
        if(:lastdata is null) then lastdata  = :data;
        update NUMBERFREE set lastdata = :lastdata where oddzial = :oddzial and nazwa = :nazwa and key_val = :key_val and typ = 0 and (:ISF_company != 1 or company = :current_company);
      end
      if(:old_number = :number and :doblock=0) then begin
         -- wejdz tutaj jesli usuwany numer byl ostatnim zajetym
         -- pousuwaj numery zwolnione
         number = number - 1;
         while(exists(select first 1 1 from NUMBERFREE where oddzial = :oddzial and nazwa = :nazwa and key_val = :key_val and typ = 1 and NUMBER=:number and numblock is null and (:ISF_company != 1 or company = :current_company))) do begin
           delete from NUMBERFREE where oddzial = :oddzial and nazwa = :nazwa and key_val = :key_val and typ = 1 and number = :number and (:ISF_company != 1 or company = :current_company);
           number =  number - 1;
         end
         -- zmniejsz ostatni zajety numer
         if(:number > 0) then
           update NUMBERFREE set NUMBER = :number where oddzial = :oddzial and nazwa = :nazwa and key_val = :key_val and typ = 0 and (:ISF_company != 1 or company = :current_company);
         else
           delete from NUMBERFREE where oddzial = :oddzial and nazwa = :nazwa and key_val = :key_val and typ = 0 and (:ISF_company != 1 or company = :current_company);
      end
      else begin
        execute procedure GEN_REF('NUMBERFREE') returning_values :numref;
        if (ISF_company = 1) then
          insert into NUMBERFREE(REF, ODDZIAL, NAZWA, KEY_VAL, TYP, NUMBER, lastdata, company) values(:numref, :oddzial, :nazwa,:key_val,1,:number, :data, :current_company);
        else
        insert into NUMBERFREE(REF, ODDZIAL, NAZWA, KEY_VAL, TYP, NUMBER, lastdata) values(:numref, :oddzial, :nazwa,:key_val,1,:number, :data);
        if(:doblock > 0) then begin
          execute procedure GEN_REF('NUMBERBLOCK') returning_values :blockref;
          if (ISF_company = 1) then
            insert into NUMBERBLOCK(REF, NUMERATOR, NUMBER, DATA, OPERATOR, NUMOKRES, NUMTYPDOK, NUMREJDOK, NUMNUMBER, BLOCKMASTER , company)
              values (:blockref, :nazwa, :numref, :data, NULL, :okres, :dokument, :rejestr, :number, :blockmaster, :current_company);
          else
          insert into NUMBERBLOCK(REF, NUMERATOR, NUMBER, DATA, OPERATOR, NUMOKRES, NUMTYPDOK, NUMREJDOK, NUMNUMBER, BLOCKMASTER )
          values (:blockref, :nazwa, :numref, :data, NULL, :okres, :dokument, :rejestr, :number, :blockmaster);
        end
      end
  end
end^
SET TERM ; ^
