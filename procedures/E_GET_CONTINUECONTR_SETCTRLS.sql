--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GET_CONTINUECONTR_SETCTRLS(
      EMPLCONTRACT integer)
  returns (
      E_FROMDATE smallint,
      E_TODATE smallint,
      E_ENDDATE smallint,
      E_ECONTRTYPE smallint)
   as
declare variable minfd timestamp;
declare variable maxfd timestamp;
declare variable actfd timestamp;
declare variable employee integer;
begin

/*MWr: Procedura jest wywolywana przy przegladaniu i edycji umow o prace. Przy
  przegladaniu umow w kartotece pracownika steruje aktywnoscia przycisku 'Usun'.
  Przy edycji odpowiada za edytowalnosc tych kontrolek na formie danej umowy o
  prace EMPLCONTRACT, ktorych zmiana wartosci i tak nie bylaby mozliwa z uwagi
  na kontrole ciaglosci umow/zatrudnienia (PR24342)*/

  select fromdate, employee from emplcontracts
    where ref = :emplcontract
    into :actfd, :employee;

  select min(fromdate), max(fromdate) from emplcontracts
    where employee = :employee and empltype = 1
    group by employee
    into :minfd, :maxfd;

  --ustawienie edycji kontrolki 'Data od' lub aktywnosci przycisku 'Usun' dla umowy w kartotece pracownika.
  if (actfd in (minfd, maxfd)) then e_fromdate = 1;
  else e_fromdate = 0;

  --ustawienie edycji kontrolki 'Data do' i 'Data konca umowy'
  if (maxfd = actfd) then e_todate = 1;
  else e_todate = 0;

  e_enddate = e_todate;

  --ustawienie edycji kontrolki 'Rodzaj umowy'
  if (maxfd = actfd and (minfd <> actfd or minfd = maxfd)) then e_econtrtype = 1;
  else e_econtrtype = 0;

  suspend;
end^
SET TERM ; ^
