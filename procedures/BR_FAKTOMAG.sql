--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_FAKTOMAG(
      MAG integer)
  returns (
      REF integer,
      AKCEPTACJA smallint,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      WARTOSC numeric(14,2),
      KINDSTR varchar(20) CHARACTER SET UTF8                           )
   as
declare variable pozfak_ref integer;
begin
  for select nf.ref, nf.akceptacja, nf.symbol
       from pozfak pf
       join dokumpoz dp on (pf.ref = dp.frompozfak)
       join nagfak nf on (nf.ref = pf.dokument)
       where dp.dokument = :mag
  into :ref, :akceptacja, :symbol
  do begin
    if(:akceptacja=0) then kindstr = 'MI_REDX';
    else if(:akceptacja=1 or :akceptacja=8) then kindstr = '14';
    else if(:akceptacja=9 or :akceptacja=10) then kindstr = 'MI_PADLOCK';
    suspend;
  end
end^
SET TERM ; ^
