--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMPOZ_CLONE(
      DOKUMPOZREF integer,
      ILOSC numeric(14,4))
  returns (
      NEWREF integer)
   as
declare variable ILOSCOBECNA numeric(14,4);
declare variable ILOSCDOROZ numeric(14,4);
declare variable DOKUMENT integer;
declare variable WERSJAREF integer;
declare variable CENA numeric(14,4);
declare variable JEDNO integer;
declare variable PARAMN1 numeric(14,4);
declare variable PARAMN2 numeric(14,4);
declare variable PARAMN3 numeric(14,4);
declare variable PARAMN4 numeric(14,4);
declare variable PARAMN5 numeric(14,4);
declare variable PARAMN6 numeric(14,4);
declare variable PARAMN7 numeric(14,4);
declare variable PARAMN8 numeric(14,4);
declare variable PARAMN9 numeric(14,4);
declare variable PARAMN10 numeric(14,4);
declare variable PARAMN11 numeric(14,4);
declare variable PARAMN12 numeric(14,4);
declare variable PARAMS1 varchar(255);
declare variable PARAMS2 varchar(255);
declare variable PARAMS3 varchar(255);
declare variable PARAMS4 varchar(255);
declare variable PARAMS5 varchar(255);
declare variable PARAMS6 varchar(255);
declare variable PARAMS7 varchar(255);
declare variable PARAMS8 varchar(255);
declare variable PARAMS9 varchar(255);
declare variable PARAMS10 varchar(255);
declare variable PARAMS11 varchar(255);
declare variable PARAMS12 varchar(255);
declare variable PARAMD1 timestamp;
declare variable PARAMD2 timestamp;
declare variable PARAMD3 timestamp;
declare variable PARAMD4 timestamp;
declare variable DOKUMROZNEWREF integer;
declare variable TEMPILOSC numeric(14,4);
declare variable CENANET numeric(14,4);
declare variable CENABRU numeric(14,4);
declare variable KTM varchar(40);
declare variable USLUGA smallint;
declare variable PREC smallint;
declare variable numer integer;
declare variable gr_vat varchar(5);
begin
-- ILOSC, to ilosc do pozostawienia w obecnym dokumpozie
  select ILOSC,DOKUMENT,WERSJAREF,CENA,JEDNO,CENANET,CENABRU,KTM,PREC,GR_VAT
    from DOKUMPOZ
    where ref=:dokumpozref
    into :iloscobecna,:dokument,:wersjaref,:cena,:jedno,:cenanet,:cenabru, :ktm, :prec, :gr_vat;
  select USLUGA from TOWARY where KTM=:ktm into :usluga;
  iloscdoroz = :iloscobecna - :ilosc;
  if(:iloscdoroz<=0) then exit;
-- dodaj nowego dokumpoza
  execute procedure GEN_REF('DOKUMPOZ') returning_values :newref;
  execute procedure GET_PARAMS_FOR_DOKUMPOZ(:newref,NULL)
         returning_values :paramd1,:paramd2,:paramd3,:paramd4,
                          :paramn1,:paramn2,:paramn3,:paramn4,
                          :paramn5,:paramn6,:paramn7,:paramn8,
                          :paramn9,:paramn10,:paramn11,:paramn12,
                          :params1,:params2,:params3,:params4,
                          :params5,:params6,:params7,:params8,
                          :params9,:params10,:params11,:params12;
  insert into DOKUMPOZ(REF,DOKUMENT,WERSJAREF,CENA,ILOSC, JEDNO, ROZ_BLOK,CENANET,CENABRU,
                              PARAMD1,PARAMD2,PARAMD3,PARAMD4,
                              PARAMN1,PARAMN2,PARAMN3,PARAMN4,
                              PARAMN5,PARAMN6,PARAMN7,PARAMN8,
                              PARAMN9,PARAMN10,PARAMN11,PARAMN12,
                              PARAMS1,PARAMS2,PARAMS3,PARAMS4,
                              PARAMS5,PARAMS6,PARAMS7,PARAMS8,
                              PARAMS9,PARAMS10,PARAMS11,PARAMS12,PREC,GR_VAT)
  values(:newref,:dokument, :wersjaref, :cena, :iloscdoroz, :jedno, 1,:cenanet,:cenabru, 
                :paramd1,:paramd2,:paramd3,:paramd4,
                :paramn1,:paramn2,:paramn3,:paramn4,
                :paramn5,:paramn6,:paramn7,:paramn8,
                :paramn9,:paramn10,:paramn11,:paramn12,
                :params1,:params2,:params3,:params4,
                :params5,:params6,:params7,:params8,
                :params9,:params10,:params11,:params12,:prec,:gr_vat);
-- zmniejsz ilosc na obecnym dokumpozie
  update DOKUMPOZ set ILOSC=:ilosc,ROZ_BLOK=1 where ref=:dokumpozref;

-- przenies czesc rozpisek na nowego dokumpoza
  while(:iloscdoroz>0) do begin
    dokumroznewref = null;
    -- czy jest taka rozpiska ktora ma dokladnie potrzebna ilosc
    select min(ref) from dokumroz where pozycja=:dokumpozref and ilosc=:iloscdoroz into :dokumroznewref;
    if(:dokumroznewref is not null) then begin
      -- jesli jest to przepinamy calosc
      update dokumroz set pozycja=:newref, blokoblnag = 1 where ref=:dokumroznewref;
      iloscdoroz = 0;
    end else begin
      -- jesli nie, to szukamy rozpiski z jak najmniejsza iloscia, mniejsza niz potrzebna
      select first 1 ref,ilosc from dokumroz where pozycja=:dokumpozref and ilosc<:iloscdoroz order by ilosc into :dokumroznewref,:tempilosc;
      if(:dokumroznewref is not null) then begin
        -- jesli jest to przepinamy ja
        update dokumroz set pozycja=:newref, blokoblnag = 1 where ref=:dokumroznewref;
        iloscdoroz = :iloscdoroz - :tempilosc;
      end else begin
        -- jesli nie to bierzemy najmniejsza rozpiske i dzielimy na 2
        select first 1 ref,ilosc from dokumroz where pozycja=:dokumpozref order by ilosc into :dokumroznewref,:tempilosc;
        execute procedure DOKUMROZ_CLONE(:dokumroznewref,:tempilosc-:iloscdoroz) returning_values :dokumroznewref;
        select coalesce(max(numer),0)+1 from dokumroz where pozycja = :newref into :numer;
        update dokumroz set pozycja=:newref, blokoblnag = 1, numer=:numer where ref=:dokumroznewref;
        iloscdoroz = 0;
      end
    end
  end
  update dokumroz set blokoblnag = 0 where pozycja=:dokumpozref and blokoblnag = 1;
  update dokumroz set blokoblnag = 0 where pozycja=:newref and blokoblnag = 1;

-- zaktualizuj ilosci i inne dane w obecnym i nowym dokumpozie
  if(:usluga<>1) then begin
    execute procedure DOKPOZ_OBL_FROMROZ(:dokumpozref);
    execute procedure DOKPOZ_OBL_FROMROZ(:newref);
  end
end^
SET TERM ; ^
