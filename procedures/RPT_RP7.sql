--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_RP7(
      EMPL integer)
  returns (
      P1 integer,
      P2 numeric(14,2),
      P3 numeric(14,2),
      P3A numeric(14,2),
      P3B varchar(4000) CHARACTER SET UTF8                           ,
      P4A numeric(14,2),
      P4B numeric(14,2),
      P5A numeric(14,2),
      P5B numeric(14,2),
      P6 numeric(14,2),
      P7 numeric(14,2))
   as
declare variable TEMP varchar(22);
begin

  for
    select distinct cast (substring (P.cper from 1 for 4) as integer)
      from epayrolls P join eprpos EP on (P.ref = EP.payroll)
      where EP.employee = :empl
      into :p1
  do begin
    select sum (EP.pvalue)
      from epayrolls P join eprpos EP on (P.ref = EP.payroll)
        join employees E on (EP.employee = E.ref)
      where E.ref = :empl and P.cper starting with :p1
        and ((EP.ecolumn >= 1000 and EP.ecolumn <= 1070)
           or EP.ecolumn in (1500, 1510, 1550))
      into :p2;

    select sum (EP.pvalue)
      from epayrolls P join eprpos EP on (P.ref = EP.payroll)
        join employees E on (EP.employee = E.ref)
      where E.ref = :empl and P.cper starting with :p1
        and EP.ecolumn >= 1100 and EP.ecolumn <= 2999
        and EP.ecolumn not in (1500, 1510, 1550, 2100, 2110, 2120, 2200, 2300, 2400) --wykluczenie: wyng. za urlop, odpraw, nagrody jubileuszowej
      into :p3a;

    p3b = '';
    for
      select distinct C.name
        from epayrolls P join eprpos EP on (P.ref = EP.payroll)
          join employees E on (EP.employee = E.ref)
          join ecolumns C on (EP.ecolumn = C.number)
        where E.ref = :empl and P.cper starting with :p1
          and EP.ecolumn >= 1100 and EP.ecolumn <= 2999
          and EP.ecolumn not in (1500, 1510, 1550, 2100, 2110, 2120, 2200, 2300, 2400)
        into :temp
    do begin
      if (p3b <> '') then p3b = p3b || ', ';
      p3b = p3b || :temp;
    end

    select sum (EP.pvalue)
      from epayrolls P join eprpos EP on (P.ref = EP.payroll)
        join employees E on (EP.employee = E.ref)
      where E.ref = :empl and P.cper starting with :p1
        and ((EP.ecolumn >= 3000 and EP.ecolumn <= 3999)
           or EP.ecolumn = 4900)
      into :p6;

    p7 = coalesce (p2, 0) + coalesce (p3a, 0) + coalesce (p4a, 0) + coalesce (p5a, 0) + coalesce (p6, 0);

    suspend;
  end
end^
SET TERM ; ^
