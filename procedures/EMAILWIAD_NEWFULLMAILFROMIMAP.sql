--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWIAD_NEWFULLMAILFROMIMAP(
      TYTUL STRING2048_UTF8,
      DATA timestamp,
      ODKOGO STRING1024_UTF8,
      DOKOGO STRING2048_UTF8,
      CC STRING2048_UTF8,
      DELIVEREDTO varchar(255) CHARACTER SET UTF8                           ,
      IDWATKU integer,
      INREPLYTO varchar(255) CHARACTER SET UTF8                           ,
      REFERENCES_LIST STRING255,
      STAN smallint,
      MAILBOX integer,
      FOLDER DEFFOLD_ID,
      ID varchar(1024) CHARACTER SET UTF8                           ,
      UID integer,
      IID integer,
      TRYB integer,
      TRESC MEMO5000_UTF8,
      FULLHTMLBODY BLOB_UTF8,
      FULLYLOADED smallint,
      HASATTACHMENTS SMALLINT_ID,
      BCC STRING2048_UTF8)
  returns (
      REFOUT integer,
      IDOUT varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable REF integer;
declare variable ORGREF integer;
declare variable AKTUOPER integer;
declare variable reference string255;
begin
  /* TODO: okresl operatora na podstawie mailboxa, a mailboxa na podstawie folderu */
  -- szukamy tego maila
  if(:id<>'') then
    select first 1 ref from emailwiad where id=:id and mailbox=:mailbox into :ref;
  if(:ref is null and :iid>0) then
    select ref from emailwiad where folder = :folder and iid = :iid and mailbox=:mailbox into :ref;
  if(:ref is null and :uid>0) then
    select ref from emailwiad where folder = :folder and uid = :uid  and mailbox=:mailbox into :ref;

  -- sprawdzamy czy mail należy podlaczyc do jakiegos istniejacego watku
  orgref = null;
  if(:inreplyto<>'') then begin
    select ref,idwatku from emailwiad where id=:inreplyto and mailbox=:mailbox into :orgref,:idwatku;
    if((:idwatku is null or :idwatku=0) and :orgref<>0) then begin
      update emailwiad set idwatku=:orgref where ref=:orgref;
      idwatku = :orgref;
    end
  end else if(coalesce(:references_list,'') <> '') then
  begin
     select first 1 q.outstring from parse_lines(:references_list,',') q into :reference;
     select ref,idwatku from emailwiad where id=:reference and mailbox=:mailbox into :orgref,:idwatku;
    if((:idwatku is null or :idwatku=0) and :orgref<>0) then begin
      update emailwiad set idwatku=:orgref where ref=:orgref;
      idwatku = :orgref;
    end
  end

  -- dodajemy lub updateujemy maila
  if(:ref is not null) then begin
      if(:id='' or id is null) then id = :ref;
      update emailwiad set
        tytul = :tytul,
        data = :data,
        email = 1,
        html = 1,
        od = :odkogo,
        dokogo = :dokogo,
        cc = :cc, 
        deliveredto = :deliveredto,
        idwatku = :idwatku,
        previousmail = :orgref,
        stan = :stan,
        mailbox = :mailbox,
        folder = :folder,
        id = :id,
        uid = :uid,
        iid = :iid,
        tryb = :tryb,
        tresc = :tresc,
        fullhtmlbody = :fullhtmlbody,
        fullyloaded = :fullyloaded,
        hasattachments = :hasattachments,
        bcc = :bcc,
        refl = :references_list
      where ref=:ref;
  end else begin
    execute procedure gen_ref('EMAILWIAD') returning_values :ref;
    if(:id='' or id is null) then id = :ref;
    select PVALUE from get_global_param('AKTUOPERATOR') into aktuoper;
    insert into EMAILWIAD(REF,TYTUL,DATA,EMAIL,HTML,OD, DOKOGO, CC, DELIVEREDTO, IDWATKU, PREVIOUSMAIL, STAN, MAILBOX, FOLDER,
       ID, UID, IID, TRYB, tresc, fullhtmlbody, fullyloaded, hasattachments, BCC, OPERATOR, REFL
       )
    values (:ref, :tytul, :data, 1,1, :odkogo, :dokogo, :cc, :deliveredto, :idwatku, :orgref, :stan, :mailbox, :folder,
       :id, :uid, :iid,  :tryb, :tresc, :fullhtmlbody, :fullyloaded, :hasattachments, :bcc, coalesce(:aktuoper,'brak'), :REFERENCES_LIST
       );
  end
  idout = :id;
  refout = :ref;
end^
SET TERM ; ^
