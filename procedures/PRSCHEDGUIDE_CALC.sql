--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDGUIDE_CALC(
      REF integer)
   as
declare variable sql varchar(60);
begin
  select pr.sql
    from prschmethods pr
      join prschedguides pg on (pr.symbol = pg.prschmethod)
    where pg.ref = :ref
    into :sql;
  if (:sql is not null) then begin
    execute statement 'execute procedure ' || :sql || '('||:ref||');';
  end
end^
SET TERM ; ^
