--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_CHECK_MAGAZYN(
      REFNAGFAK integer)
  returns (
      STATUS integer,
      MSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable nagfakmag varchar(3);
declare variable magazyn varchar(3);
declare variable aktulocat varchar(255);
declare variable location varchar(255);
declare variable cnt integer;
declare variable refk integer;
begin
  status = 1;
  msg = 'Dokument obejmuje nastepujace nietypowe magazyny:@';
  execute procedure GETCONFIG('AKTULOCAT') returning_values :aktulocat;
--SN
--  select magazyn, refk from nagfak where ref=:refnagfak into :nagfakmag, :refk;
  select max(refk) from pozfak where dokument=:refnagfak into :refk;
  if(:nagfakmag is null) then nagfakmag = '';
  select oddzialy.location from defmagaz
  left join ODDZIALY on (ODDZIALY.oddzial = DEFMAGAZ.oddzial)
  where defmagaz.symbol=:nagfakmag
  into :location;
  if(:location<>:aktulocat) then begin
    msg = msg || ' - ' || :nagfakmag || ' - magazyn z innej lokalizacji@';
    status = 0;
  end

  if(:nagfakmag<>'') then begin
    for select distinct POZFAK.MAGAZYN, ODDZIALY.LOCATION from POZFAK
    left join DEFMAGAZ on (DEFMAGAZ.symbol = POZFAK.MAGAZYN)
    left join ODDZIALY on (ODDZIALY.oddzial = DEFMAGAZ.oddzial)
    where POZFAK.DOKUMENT = :refnagfak and POZFAK.MAGAZYN<>:nagfakmag
    and POZFAK.USLUGA<>1 into :magazyn,:location
    do begin
      if(:location<>:aktulocat) then begin
        msg = msg || ' - ' || :magazyn || ' - magazyn z innej lokalizacji@';
      end else begin
        msg = msg || ' - ' || :magazyn || ' - magazyn inny niz na naglowku dokumentu@';
      end
      status = 0;
    end
  end

  select count(*) from POZFAK
  where POZFAK.DOKUMENT = :refnagfak and (POZFAK.MAGAZYN='' or (POZFAK.MAGAZYN is null))
  and POZFAK.USLUGA<>1 into :cnt;
  if(:cnt>0) then begin
    msg = msg || ' - zawiera pozycje towarowe BEZ wskazanego magazynu!@';
    status = 0;
  end
  if(:refk > 0) then begin
    for select distinct PK.MAGAZYN
      from POZFAK PK
--SN
--      left join NAGFAK NK on (NK.REF = PK.dokument)
--      left join POZFAK PO on (PO.numer = PK.NUMER and PO.DOKUMENT = NK.REFK)
      left join POZFAK PO on (PK.refk = PO.ref)
    where Pk.DOKUMENT = :refnagfak
      and PO.magazyn <> PK.MAGAZYN
    into :magazyn
    do begin
      msg = msg || ' - ' || :magazyn || ' - magazyn inny, niż na dokumencie korygowanym';
      status = 0;
    end
  end

  msg = msg || '@Czy chcesz kontynuowac akceptacje faktury ?';
end^
SET TERM ; ^
