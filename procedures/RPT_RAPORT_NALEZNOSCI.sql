--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_RAPORT_NALEZNOSCI(
      ZAKRES integer,
      NADZIEN date,
      ODDATY date,
      DODATY date,
      TERMPLAT date,
      SPRZEDAWCA integer,
      KLIENT integer,
      TYPKLI integer,
      ODDZIAL varchar(10) CHARACTER SET UTF8                           ,
      PRZEKROCZONO integer,
      PRZEKROCZONO2 integer,
      AKTUOPERATOR varchar(40) CHARACTER SET UTF8                           ,
      AKTUOPERGRUPY varchar(2048) CHARACTER SET UTF8                           ,
      COMPANY smallint)
  returns (
      DATAPLAT date,
      DATAZAMK date,
      KWOTA numeric(12,2),
      KWOTAZL numeric(12,2),
      WALUTA varchar(3) CHARACTER SET UTF8                           ,
      SYMBFAK varchar(20) CHARACTER SET UTF8                           ,
      DATAOTW date,
      SLODEF integer,
      KONTOFK varchar(20) CHARACTER SET UTF8                           ,
      FSKROT SHORTNAME_ID)
   as
declare variable slodefref integer;
declare variable slopozref integer;
begin
  select min(ref) from slodef where slodef.typ = 'KLIENCI' into :slodefref;
  if(:AKTUOPERATOR is null) then AKTUOPERATOR = '';
  if(:AKTUOPERGRUPY is null) then AKTUOPERGRUPY = '';

for
  select distinct ROZRACH.SLOPOZ from ROZRACH join KLIENCI on (KLIENCI.REF = ROZRACH.SLOPOZ)
     where ROZRACH.SLODEF = :slodefref
     and ROZRACH.company = :company
     and ((:TYPKLI is not null and KLIENCI.TYP = :TYPKLI) or :TYPKLI is null)
     and ((:ZAKRES = 0 and ROZRACH.SALDO <> 0) or :ZAKRES = 1)
     and ((ROZRACH.DATAPLAT + :PRZEKROCZONO <= :NADZIEN and :przekroczono <> 0) or :PRZEKROCZONO = 0)
     and ((:PRZEKROCZONO2 = 0 /*and  ROZRACH.DATAPLAT <= :NADZIEN*/)
       or (:PRZEKROCZONO2 = 1 and  ROZRACH.DATAPLAT >= :NADZIEN-30)
       or (:PRZEKROCZONO2 = 2 and  (ROZRACH.DATAPLAT <= :NADZIEN-31 and ROZRACH.DATAPLAT >= :NADZIEN-60))
       or (:PRZEKROCZONO2 = 3 and  (ROZRACH.DATAPLAT <= :NADZIEN-61 and ROZRACH.DATAPLAT >= :NADZIEN-90))
       or (:PRZEKROCZONO2 = 4 and  (ROZRACH.DATAPLAT <= :NADZIEN-91)))
    and ((:ODDATY is not null and ROZRACH.DATAOTW >= :ODDATY) or :ODDATY is null)
    and ((:DODATY is not null and ROZRACH.DATAOTW <= :DODATY) or :DODATY is null)
    and ((:ODDZIAL is not null and ROZRACH.ODDZIAL = :ODDZIAL) or :ODDZIAL is null)
    and ((:TERMPLAT is not null and ROZRACH.DATAPLAT <= :TERMPLAT) or :TERMPLAT is null)
    and ((:SPRZEDAWCA is not null and KLIENCI.SPRZEDAWCA =:SPRZEDAWCA) or :SPRZEDAWCA is null)
    and ((:KLIENT is not null and KLIENCI.REF =:KLIENT) or :KLIENT is null)
    order by ROZRACH.SLODEF
  into :slopozref
  do begin
    for
      select ROZRACH.DATAPLAT, ROZRACH.DATAZAMK, (ROZRACH.WINIEN - ROZRACH.MA) as KWOTA,
             (ROZRACH.WINIENZL - ROZRACH.MAZL) AS KWOTAZL, ROZRACH.WALUTA,
             ROZRACH.SYMBFAK, ROZRACH.DATAOTW, ROZRACH.SLODEF, KLIENCI.KONTOFK, KLIENCI.FSKROT
        from ROZRACH join KLIENCI on (KLIENCI.REF = ROZRACH.SLOPOZ)
       where ROZRACH.SLODEF = :slodefref
         and ((ROZRACH.RIGHTS = '' and ROZRACH.RIGHTSGROUP = '') or (ROZRACH.RIGHTS like '%;'||:AKTUOPERATOR||';%') OR (strmulticmp(';'||:AKTUOPERGRUPY||';',ROZRACH.RIGHTSGROUP)=1))
         --and ROZRACH.SLOPOZ in ()
         and ROZRACH.SLOPOZ = :slopozref
         and ROZRACH.WINIEN <> ROZRACH.MA
         and ((ROZRACH.DATAPLAT + :PRZEKROCZONO <= :NADZIEN and :przekroczono <> 0) or :PRZEKROCZONO = 0)
         and ((:PRZEKROCZONO2 = 0 /*and  ROZRACH.DATAPLAT <= :NADZIEN*/)
          or (:PRZEKROCZONO2 = 1 and  ROZRACH.DATAPLAT >= :NADZIEN-30)
          or (:PRZEKROCZONO2 = 2 and  (ROZRACH.DATAPLAT <= :NADZIEN-31 and ROZRACH.DATAPLAT >= :NADZIEN-60))
          or (:PRZEKROCZONO2 = 3 and  (ROZRACH.DATAPLAT <= :NADZIEN-61 and ROZRACH.DATAPLAT >= :NADZIEN-90))
          or (:PRZEKROCZONO2 = 4 and  (ROZRACH.DATAPLAT <= :NADZIEN-90 )))
         and ((:ODDATY is not null and ROZRACH.DATAOTW >= :ODDATY) or :ODDATY is null)
         and ((:DODATY is not null and ROZRACH.DATAOTW <= :DODATY) or :DODATY is null)
         and ((:TERMPLAT is not null and ROZRACH.DATAPLAT <= :TERMPLAT) or :TERMPLAT is null)
         and ((:SPRZEDAWCA is not null and KLIENCI.SPRZEDAWCA =:SPRZEDAWCA) or :SPRZEDAWCA is null)
         and ((:KLIENT is not null and KLIENCI.REF =:KLIENT) or :KLIENT is null)
      order by KLIENCI.FSKROT, ROZRACH.DATAPLAT, ROZRACH.SLOPOZ, ROZRACH.SYMBFAK
        into  :dataplat, :datazamk, :kwota, :kwotazl, :waluta, :symbfak, :dataotw, :slodef, :kontofk, :fskrot
      do begin
        suspend;
      end
    end
end^
SET TERM ; ^
