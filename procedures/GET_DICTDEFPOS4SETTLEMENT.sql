--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_DICTDEFPOS4SETTLEMENT(
      COMPANY integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      ACCOUNT ACCOUNT_ID)
  returns (
      DICTDEF integer,
      DICTPOS integer)
   as
declare variable k smallint;
  declare variable sep varchar(1);
  declare variable len smallint;
  declare variable kartoteka smallint;
  declare variable dictdef2 integer;
  declare variable tmp varchar(20);
  declare variable yearid integer;
  declare variable name varchar(80);
begin
  select P.yearid, Y.synlen
    from bkperiods P
      join bkyears Y on (P.yearid=Y.yearid and p.company = y.company)
    where id = :period and P.company = :company
    into :yearid, :k;

  if (exists(select ref from bkaccounts
      where yearid=:yearid and symbol=substring(:account from  1 for  :k)
        and company = :company)) then
  begin
    dictdef = 0;
    dictpos = 0;

    for
      select S.separator, S.dictdef, S.len
        from accstructure S
        join bkaccounts B on (S.bkaccount=B.ref)
        where B.yearid=:yearid and B.symbol=substring(:account from  1 for  :k)
          and company = :company
        order by nlevel
        into :sep, :dictdef2, :len
    do begin
      if (sep <> ',') then
        k = k + 1;
      tmp = substring(account from  k + 1 for  k + len);
      select kartoteka from slodef
        where ref = :dictdef2
        into :kartoteka;
      if (kartoteka = 1) then
      begin
        execute procedure name_from_bksymbol(company, dictdef2, tmp)
          returning_values name, dictpos;
        dictdef = dictdef2;
      end
      k = k + len;
    end
  end else
  begin
    dictdef = null;
    dictpos = null;
  end
  suspend;
end^
SET TERM ; ^
