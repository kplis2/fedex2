--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAV_OLAPCUBES
  returns (
      REF integer,
      PARENT integer,
      NUMBER integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      HINT varchar(255) CHARACTER SET UTF8                           ,
      KINDSTR varchar(20) CHARACTER SET UTF8                           ,
      ACTIONID varchar(60) CHARACTER SET UTF8                           ,
      APPLICATIONS varchar(60) CHARACTER SET UTF8                           ,
      FORMODULES varchar(60) CHARACTER SET UTF8                           ,
      RIGHTS varchar(255) CHARACTER SET UTF8                           ,
      RIGHTSGROUP varchar(255) CHARACTER SET UTF8                           ,
      ISNAVBAR smallint,
      ISTOOLBAR smallint,
      ISWINDOW smallint,
      ISMAINMENU smallint,
      ITEMTYPE smallint,
      GROUPPROCEDURE varchar(60) CHARACTER SET UTF8                           ,
      CHILDRENCOUNT smallint,
      RIBBONTAB varchar(40) CHARACTER SET UTF8                           ,
      RIBBONGROUP varchar(40) CHARACTER SET UTF8                           ,
      NATIVETABLE varchar(40) CHARACTER SET UTF8                           ,
      POPUPMENU varchar(40) CHARACTER SET UTF8                           ,
      NATIVEFIELD varchar(40) CHARACTER SET UTF8                           ,
      FORADMIN smallint)
   as
begin
  parent = NULL;
  hint = '';
  actionid = 'ExecuteOLAPFromNavigator';
  isnavbar = 0;
  istoolbar = 1;
  iswindow = 0;
  ismainmenu = 0;
  itemtype = 0;
  childrencount = 1;
  ribbontab = '';
  ribbongroup = '';
  nativetable = 'OLAPCUBES';
  popupmenu = 'PopupOLAPCUBES';
  foradmin = 0;
  number = 1;
  for select REF, NAME, ICON, APPLICATIONS, FORMODULES, RIGHTS, RIGHTSGROUP
  from OLAPCUBES
  order by SYMBOL
  into :REF, :NAME, :KINDSTR, :APPLICATIONS, :FORMODULES, :RIGHTS, :RIGHTSGROUP
  do begin
    groupprocedure = 'NAV_OLAPLAYOUTS('||:REF||')';
    if(coalesce(:kindstr,'')='') then kindstr = 'MI_PROGRAMY';
    suspend;
    number = :number + 1;
  end


  ref = 1000;
  groupprocedure = '';
--  popupmenu = '';
  childrencount = 0;
  nativetable = '';
  actionid = 'ShowOLAPFromNavigator';
  name ='Okno przeglądania analiz OLAP';
  suspend;
end^
SET TERM ; ^
