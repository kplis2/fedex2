--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DATE_ADD(
      SDATE date,
      Y smallint,
      M smallint,
      D smallint)
  returns (
      FDATE date)
   as
declare variable ty smallint;
declare variable tm smallint;
declare variable td smallint;
declare variable lastday_date date;
declare variable lastday smallint;
begin
  --DS: funkcja najpierw dodaje lata, pozniej miesiace, na koncu dni
  ty = extract(year from sdate) + y;
  tm = extract(month from sdate) + m;

  td = extract(day from sdate);
  while (tm > 12)
  do begin
    ty = ty + 1;
    tm = tm - 12;
  end
  while (tm <= 0)
  do begin
    ty = ty - 1;
    tm = tm + 12;
  end

  execute procedure monthlastday(cast(ty||'-'||tm||'-01' as date)) returning_values lastday_date;
  lastday = extract(day from lastday_date);
  if (td > lastday) then
    td = lastday;
  fdate = ty || '/' || tm || '/' || td;

  fdate = fdate + d;
  suspend;
end^
SET TERM ; ^
