--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_NAGFAKZAL(
      DATAOD date,
      DATADO date,
      SPRZEDAWCA integer,
      KLIENT integer,
      TYPKLIENT integer,
      ODDZIAL varchar(20) CHARACTER SET UTF8                           ,
      ZAKRES integer)
  returns (
      KONTOFK varchar(20) CHARACTER SET UTF8                           ,
      FSKROT SHORTNAME_ID,
      DATAOTW date,
      REF integer,
      VAT varchar(5) CHARACTER SET UTF8                           ,
      OKRES varchar(6) CHARACTER SET UTF8                           ,
      SUMWARTBRU numeric(14,2),
      SYMBOL varchar(100) CHARACTER SET UTF8                           ,
      EWARTNET numeric(14,2),
      EWARTBRU numeric(14,2),
      PEWARTNET numeric(14,2),
      PEWARTBRU numeric(14,2),
      WARTNET numeric(14,2),
      WARTBRU numeric(14,2),
      EWARTNETZL numeric(14,2),
      EWARTBRUZL numeric(14,2),
      PEWARTNETZL numeric(14,2),
      PEWARTBRUZL numeric(14,2),
      WARTNETZL numeric(14,2),
      WARTBRUZL numeric(14,2),
      KWOTAZAL numeric(14,2),
      ODDZIAL2 varchar(20) CHARACTER SET UTF8                           ,
      TYP varchar(10) CHARACTER SET UTF8                           )
   as
begin
    if(:zakres = -1)  then
      zakres = 2;
    for select klienci.kontofk,klienci.fskrot,nagfak.data,nagfak.sumwartbru,nagfak.ref,
      nagfak.symbol, rozfak.vat,
      nagfak.okres,nagfak.oddzial, nagfak.typ,
       nagfak.kwotazal,
      rozfak.esumwartnet ,rozfak.esumwartbru,
      rozfak.esumwartnetzl,rozfak.esumwartbruzl
      from nagfak
        join rozfak on (rozfak.dokument=nagfak.ref)
        left join klienci on (nagfak.klient = klienci.ref)
      where nagfak.zaliczkowy > 0
        and ((:DATAOD is not null and nagfak.data >= :DATAOD) or :DATAOD is null)
        and ((:DATADO is not null and nagfak.DATA <= :DATADO) or :DATADO is null)
        and ((:ODDZIAL is not null and NAGFAK.ODDZIAL = :ODDZIAL) or :ODDZIAL is null)
        and ((:SPRZEDAWCA is not null and nagfak.SPRZEDAWCA =:SPRZEDAWCA) or :SPRZEDAWCA is null)
        and ((:KLIENT is not null and nagfak.klient =:KLIENT) or :KLIENT is null)
        and ((:TYPKLIENT is not null and KLIENCI.TYP = :TYPKLIENT) or :TYPKLIENT is null)
      into
        :kontofk, :fskrot,:dataotw,:sumwartbru,
        :ref,:symbol, :vat,
        :okres, :oddzial2, :typ,
        :kwotazal,
        :ewartnet,:ewartbru,:ewartnetzl,:ewartbruzl
    do begin
      select sum(nagfakzal.wartnet),sum(nagfakzal.wartbru),sum(nagfakzal.wartnetzl),sum(nagfakzal.wartbruzl)
        from nagfakzal
        left join nagfak on (nagfak.ref=nagfakzal.faktura)
        where nagfakzal.fakturazal=:ref and nagfakzal.vat=:vat and nagfak.anulowanie=0
        into :pewartnet, :pewartbru, :pewartnetzl, :pewartbruzl;
      if(:pewartnet is null) then pewartnet = 0;
      if(:pewartbru is null) then pewartbru = 0;
      if(:pewartnetzl is null) then pewartnetzl = 0;
      if(:pewartbruzl is null) then pewartbruzl = 0;
      wartnet = :ewartnet - :pewartnet;
      wartbru = :ewartbru - :pewartbru;
      wartnetzl = :ewartnetzl - :pewartnetzl;
      wartbruzl = :ewartbruzl - :pewartbruzl;
      if(:zakres = 0 and (:wartnet = 0 and :wartbru = 0))  then
        suspend;
      else if(:zakres = 1 and (:wartnet <> 0 and :wartbru <> 0))  then
        suspend;
      else if(:zakres = 2)  then
        suspend;
    end
end^
SET TERM ; ^
