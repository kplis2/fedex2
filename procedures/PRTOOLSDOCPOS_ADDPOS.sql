--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRTOOLSDOCPOS_ADDPOS(
      PRTOOLSDOCREF integer,
      WNRSER integer,
      WSYMBOL integer,
      WTYPNARZ integer,
      ILOSCDOM numeric(14,2),
      FROMREADER varchar(20) CHARACTER SET UTF8                           )
  returns (
      STATUS integer,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable zewnetrzny smallint;
declare variable wydania smallint;
declare variable counter integer;
declare variable dysp varchar(20);
declare variable symbol varchar(20);
declare variable invno varchar(20);
declare variable tooltype varchar(20);
declare variable amountfree numeric(14,2);
declare variable addamount numeric(14,2);
declare variable amount numeric(14,2);
declare variable price numeric(14,2);
begin
  status = 1;
  msg = '';
  counter = 0;
  select t.inssue, t.outside, n.prdsdef
    from prtoolsdocs n
    join prtoolsdefdocs t on(n.prtoolsdefdoc = t.symbol)
    where n.ref = :prtoolsdocref
    into :wydania, :zewnetrzny, :dysp;
  addamount = 0;
  amountfree = 0;
  if (wnrser > 0) then begin
    select count(*) from prtools s where s.prdsdef = :dysp and s.invno = :fromreader into :counter;
    if (counter > 0) then begin
      for
        select p.symbol, p.invno, p.tooltype,
          case when :wydania = 0 then :iloscdom else p.amountfree end,
          case when :wydania = 0 then 0 else p.price end
          from prtools p
          where p.prdsdef = :dysp
            and p.invno = :fromreader
            and (:wydania = 0 or p.amountfree > 0)
          into :symbol, :invno, :tooltype, :amountfree, :price
      do begin
        if (:iloscdom - :addamount > :amountfree) then
          amount = :amountfree;
        else
          amount =:iloscdom - :addamount;
        insert into prtoolsdocpos (prtoolsdoc, prtoolstype, prtool, amount, invno, price)
          values(:prtoolsdocref, :tooltype, :symbol, :amount, :invno, :price);
        addamount = :addamount + :amount;
        symbol = null;
        invno = null;
        tooltype = null;
        amountfree = 0;
        price = 0;
        if (:addamount = :iloscdom) then
          break;
      end
      if (:addamount < :iloscdom) then begin
        msg = 'Nie ma wystarczajacej ilosci narzedzia!';
        status = 0;
        exception prtoolsdocs_error 'Nie ma wystarczajacej ilosci narzedzia!';

      end
    end
  end
  if ((wsymbol > 0) and (counter = 0)) then begin
    select count(*) from prtools s where s.prdsdef = :dysp and s.symbol = :fromreader into :counter;
    if (counter > 0) then begin
      for
        select p.symbol, p.invno, p.tooltype,
          case when (:wydania = 0) then :iloscdom else (p.amountfree) end,
          case when (:wydania = 0) then 0 else (p.price) end
          from prtools p
          where p.prdsdef = :dysp
            and p.symbol = :fromreader
            and (:wydania = 0 or p.amountfree > 0)
          into :symbol, :invno, :tooltype, :amountfree, :price
      do begin
        if (:iloscdom - :addamount > :amountfree) then
          amount = :amountfree;
        else
          amount =:iloscdom - :addamount;
        insert into prtoolsdocpos (prtoolsdoc, prtoolstype, prtool, amount, invno, price)
          values(:prtoolsdocref, :tooltype, :symbol, :amount, :invno, :price);
        addamount = :addamount + :amount;
        symbol = null;
        invno = null;
        tooltype = null;
        amountfree = 0;
        price = 0;
        if (:addamount = :iloscdom) then
          break;
      end
      if (:addamount < :iloscdom) then begin
        msg = 'Nie ma wystarczajacej ilosci narzedzia!';
        status = 0;
        exception prtoolsdocs_error 'Nie ma wystarczajacej ilosci narzedzia!';

      end
    end
  end
  if ((wtypnarz > 0) and (counter = 0)) then begin
    select count(*) from prtools s where s.prdsdef = :dysp and s.tooltype = :fromreader into :counter;
    if (counter > 0) then begin
      for
        select p.symbol, p.invno, p.tooltype,
          case when :wydania = 0 then :iloscdom else p.amountfree end,
          case when :wydania = 0 then 0 else p.price end
          from prtools p
          where p.prdsdef = :dysp
            and p.tooltype = :fromreader
            and (:wydania = 0 or p.amountfree > 0)
          into :symbol, :invno, :tooltype, :amountfree, :price
      do begin
        if (:iloscdom - :addamount > :amountfree) then
          amount = :amountfree;
        else
          amount =:iloscdom - :addamount;
        insert into prtoolsdocpos (prtoolsdoc, prtoolstype, prtool, amount, invno, price)
          values(:prtoolsdocref, :tooltype, :symbol, :amount, :invno, 0);
        addamount = :addamount + :amount;
        symbol = null;
        invno = null;
        tooltype = null;
        amountfree = 0;
        price = 0;
        if (:addamount = :iloscdom) then
          break;
      end
      if (:addamount < :iloscdom) then begin
        msg = 'Nie ma wystarczajacej ilosci narzedzia!';
        status = 0;
        exception prtoolsdocs_error 'Nie ma wystarczajacej ilosci narzedzia!';
      end
    end
  end
  if (counter = 0) then begin
    msg = 'Nie znaleziono narzedzia o zczytanym symbolu!';
    status = 0;
    exception prtoolsdocs_error 'Nie znaleziono narzedzia o zczytanym symbolu!';
  end
  suspend;
end^
SET TERM ; ^
