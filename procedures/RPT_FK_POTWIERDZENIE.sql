--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_POTWIERDZENIE(
      REFNOTA integer,
      ODDATY timestamp,
      DODATY timestamp,
      WEZWTYP varchar(10) CHARACTER SET UTF8                           ,
      COMPANY integer)
  returns (
      REF integer,
      DOCREF integer,
      NUMER integer,
      NUMERP integer,
      NAZWA varchar(40) CHARACTER SET UTF8                           ,
      DATA timestamp,
      KHKOD varchar(255) CHARACTER SET UTF8                           ,
      KHKODKS varchar(255) CHARACTER SET UTF8                           ,
      KHNAZWA varchar(255) CHARACTER SET UTF8                           ,
      KHADRES varchar(500) CHARACTER SET UTF8                           ,
      KHNIP varchar(40) CHARACTER SET UTF8                           ,
      DEBIT numeric(15,2),
      CREDIT numeric(15,2),
      TEXT varchar(1024) CHARACTER SET UTF8                           ,
      SLODEF1 integer,
      SLOPOZ1 integer)
   as
declare variable notatyp varchar(10);
declare variable slodef integer;
declare variable slopoz integer;
declare variable nic varchar(255);
declare variable slodefref integer;
begin
  ref = 0;
  for select NOTYNAG.ref, NOTYNAG.data, NOTYNAG.notatyp,
    NOTYNAG.slokod, NOTYNAG.slokodks, notynag.slodef, notynag.slopoz,
    debit, credit, notynag.number, NOTYNAG.slodef, NOTYNAG.slopoz
  from NOTYNAG
  where ((:refnota is null ) or (:refnota = 0) or (:refnota = notynag.ref))
  and notynag.notakind = 2
  and ((:oddaty is null) or (notynag.data >= :oddaty))
  and ((:dodaty is null) or (notynag.data <= :dodaty))
  and ((:wezwtyp is null) or (:wezwtyp = '') or (:wezwtyp = notynag.notatyp))
  and notynag.company = :company
  order by notynag.data, notynag.slokodks
  into :docref, :data, :notatyp, :khkod, :khkodks, :slodef, :slopoz,
       :debit, :credit, :NUMERP, :slodef1, :slopoz1
  do begin
    execute procedure SLO_DANE(slodef, slopoz) returning_values khkod, khnazwa, nic;
    select min(slodef.ref) from slodef where slodef.typ = 'KLIENCI' into :slodefref;
    if (:slodef = :slodefref) then
      select klienci.ulica||iif(coalesce(klienci.nrdomu,'')<>'',' '||klienci.nrdomu,'')||iif(coalesce(klienci.nrlokalu,'')<>'','/'||klienci.nrlokalu,'')||
        iif(klienci.poczta<>'' and klienci.poczta<>klienci.miasto, ' ' || klienci.miasto,'')||', '||
        klienci.kodp||' '||iif(klienci.poczta<>'',klienci.poczta,klienci.miasto), nip
      from KLIENCI where REF=:slopoz
      into :khadres, :khnip;
    select min(slodef.ref) from slodef where slodef.typ = 'DOSTAWCY' into :slodefref;
    if (:slodef = :slodefref) then
      select dostawcy.ulica||iif(coalesce(dostawcy.nrdomu,'')<>'',' '||dostawcy.nrdomu,'')||iif(coalesce(dostawcy.nrlokalu,'')<>'','/'||dostawcy.nrlokalu,'')||
        iif(dostawcy.poczta<>'' and dostawcy.poczta<>dostawcy.miasto, ' ' || dostawcy.miasto,'')||
        dostawcy.kodp||' '||iif(dostawcy.poczta<>'',dostawcy.poczta,dostawcy.miasto), nip
      from DOSTAWCY where REF=:slopoz
      into :khadres, :khnip;
    select NOTYTYP.NAME, NOTYTYP.degree, NOTYTYP.descript
    from NOTYTYP
    where NOTYTYP.TYP = :notatyp
    into :nazwa, :numer, :text;
    ref = :ref + 1;
    suspend;
  end
end^
SET TERM ; ^
