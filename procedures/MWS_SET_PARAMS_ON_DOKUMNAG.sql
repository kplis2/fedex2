--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_SET_PARAMS_ON_DOKUMNAG(
      WHIN varchar(3) CHARACTER SET UTF8                           )
  returns (
      WHOUT varchar(3) CHARACTER SET UTF8                           )
   as
declare variable sql varchar(1024);
begin
  if (exists(select first 1 1 from rdb$procedures p where p.rdb$procedure_name = 'XK_MWS_SET_PARAMS_ON_DOKUMNAG')
  ) then
  begin
    sql = 'select whout from XK_MWS_SET_PARAMS_ON_DOKUMNAG('''||:whin||''')';
    execute statement sql into whout;
  end else
    whout = null;
end^
SET TERM ; ^
