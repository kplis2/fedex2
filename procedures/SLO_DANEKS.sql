--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SLO_DANEKS(
      SLODEF integer,
      SLOPOZ integer)
  returns (
      KOD varchar(100) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      KONTOFK ACCOUNT_ID,
      KODKS ACCOUNT_ID)
   as
declare variable TYP varchar(15);
declare variable COMPANY integer;
begin
  kontofk = '';
  kodks  = '';
  select TYP, PREFIXFK from SLODEF where REF = :SLODEF
    into :typ, :kontofk;

  if (typ = 'KLIENCI') then
  begin
     select FSKROT, NAZWA, KONTOFK from KLIENCI where REF=:slopoz into
       :kod, :nazwa, :kodks;
  end else if (typ = 'PKLIENCI') then
  begin
     select SKROT, NAZWA from PKLIENCI where REF=:slopoz into
       :kod, :nazwa;
  end else if (typ = 'DOSTAWCY') then
  begin
     select ID, NAZWA, KONTOFK from DOSTAWCY where REF=:slopoz into
       :kod, :nazwa, :kodks;
  end else if (typ = 'SPRZEDAWCY') then
  begin
     select NAZWA, NAZWA, KONTOFK from SPRZEDAWCY where REF=:slopoz into
       :kod, :nazwa, :kodks;
  end else if (typ = 'PERSONS') then
  begin
     execute procedure get_global_param('CURRENTCOMPANY')
       returning_values :company;
     select symbol, person, symbol from cpersons where ref = :slopoz and company = :company  --PR59074
       into :kod, :nazwa, :kodks;
  end else if (typ = 'EMPLOYEES') then
  begin
     select symbol, personnames, symbol from employees where ref = :slopoz
       into :kod, :nazwa, :kodks;
  end else if (typ = 'EINTERNALREVS') then
  begin
     select symbol, name, symbol from einternalrevs where ref = :slopoz
       into :kod, :nazwa, :kodks;
  end else if (typ = 'ESYSTEM') then
  begin
    select KOD, NAZWA, KONTOKS from SLOPOZ where SLOWNIK = :slodef and REF=:slopoz
      into :kod, :nazwa, :kodks;
  end
  if (kodks is not null) then
    kontofk = kontofk || kodks;
  suspend;
end^
SET TERM ; ^
