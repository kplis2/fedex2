--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_CONSTRAINT_DDL(
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      CREATE_OR_ALTER smallint)
  returns (
      DDL blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           )
   as
declare variable CONSTRAINT_RELATION_NAME varchar(80);
declare variable CONSTRAINT_RELATION_PK_NAME varchar(80);
declare variable CONSTRAINT_RELATION_FK_NAME varchar(80);
declare variable CONSTRAINT_RELATION_FK_REF varchar(80);
declare variable CONSTRAINT_TYPE varchar(80);
declare variable CONSTRAINT_PK varchar(80);
declare variable CONSTRAINT_PK_INDEX varchar(80);
declare variable CONSTRAINT_NAME varchar(80);
declare variable CONSTRAINT_PK_TYPE smallint;
declare variable CONSTRAINT_FIELD varchar(1024);
declare variable CONSTRAINT_UPDATE_RULE varchar(80);
declare variable CONSTRAINT_DELETE_RULE varchar(80);
declare variable EOL varchar(2);
declare variable WYGENEROWANO smallint;
declare variable POS integer;
begin
    eol ='
    ';
    ddl = '';
    if(position('.' in :nazwa) > 0)then
    begin
    nazwa = substring(:nazwa from position('.' in :nazwa)+1);
    end
    select trim(r.rdb$relation_name), trim(r.rdb$constraint_type), trim(r.rdb$index_name)
      , trim(r.rdb$constraint_name), trim(c.rdb$foreign_key)
      from rdb$relation_constraints r left join rdb$indices c on c.rdb$index_name=r.rdb$index_name
      where (r.rdb$constraint_name = :nazwa or r.rdb$index_name = :nazwa)
      into :constraint_relation_name, :constraint_type, constraint_pk_index,
      constraint_name, constraint_relation_fk_ref;

    if(constraint_name is null and upper(nazwa) starting with 'PK_') then
    begin
      CONSTRAINT_RELATION_PK_NAME = substring(nazwa from 4);
       select trim(r.rdb$relation_name), trim(r.rdb$constraint_type), trim(r.rdb$index_name)
      , trim(r.rdb$constraint_name)
      from rdb$relation_constraints r
      where r.rdb$relation_name = :CONSTRAINT_RELATION_PK_NAME and r.rdb$constraint_type='PRIMARY KEY'
      into :constraint_relation_name, :constraint_type, constraint_pk_index,
      constraint_name;
    end

    if(constraint_name is null and upper(nazwa) starting with 'FK_') then
    begin
      CONSTRAINT_RELATION_FK_NAME = substring(nazwa from 4);
      pos = position('_',CONSTRAINT_RELATION_FK_NAME);
      if(pos<1) then
        exception universal 'Nie mozna wygenerować ddl dla wiezu '||NAZWA||', ponieważ' ||
          ' nie znaleziono pasującego';
      constraint_relation_fk_ref = substring(CONSTRAINT_RELATION_FK_NAME from pos+1);
      CONSTRAINT_RELATION_FK_NAME = substring(CONSTRAINT_RELATION_FK_NAME from 1 for pos-1);

      select trim(r.rdb$relation_name), trim(r.rdb$constraint_type), trim(r.rdb$index_name)
      , trim(r.rdb$constraint_name), trim(c.rdb$foreign_key)
      from rdb$relation_constraints r left join rdb$indices c on c.rdb$index_name=r.rdb$index_name
      where r.rdb$relation_name = :CONSTRAINT_RELATION_FK_NAME
        and c.rdb$foreign_key starting with :constraint_relation_fk_ref
        and r.rdb$constraint_type='FOREIGN KEY'
      into :constraint_relation_name, :constraint_type, constraint_pk_index,
      constraint_name, constraint_relation_fk_ref;
    end

    if(constraint_name is null) then exception universal 'Nie mozna wygenerować ddl dla więzu '
      ||NAZWA||', ponieważ go nie znaleziono';

    if(:create_or_alter = 1) then
      ddl = :ddl || 'ALTER TABLE ' || :constraint_relation_name || ' DROP CONSTRAINT ' || :nazwa || ';' || :eol;

    ddl = :ddl || 'ALTER TABLE ';
    constraint_type = substring(:constraint_type from 1 for 11);
    if(constraint_type = 'UNIQUE') then
      ddl = :ddl || :constraint_relation_name || ' ADD CONSTRAINT ' || :constraint_name || ' UNIQUE (';
    else if(constraint_name starting with 'INTEG_' and nazwa starting with 'PK_') then
      ddl = :ddl || :constraint_relation_name || ' ADD CONSTRAINT ' || 'PK_' || :constraint_relation_name || ' ' || :constraint_type || ' (';
    else if(constraint_name starting with 'INTEG_' and nazwa starting with 'FK_') then
      ddl = :ddl || :constraint_relation_name || ' ADD CONSTRAINT ' ||
        (iif(char_length('FK_' || constraint_relation_name || '_' || constraint_relation_fk_ref)>31,
        substring(('FK_' || constraint_relation_name || '_' || constraint_relation_fk_ref) from 1 for 31),
        'FK_' || constraint_relation_name || '_' || constraint_relation_fk_ref
        )) || ' ' || :constraint_type || ' (';
    else
      ddl = :ddl || :constraint_relation_name || ' ADD CONSTRAINT ' || :constraint_name || ' ' || :constraint_type || ' (';

    for select trim(s.rdb$field_name)
      from rdb$index_segments s
      where s.rdb$index_name = :constraint_pk_index
      order by s.rdb$field_position
    into :constraint_field
    do begin
      constraint_field = (select outname from sys_quote_reserved(:constraint_field));
      ddl = :ddl || :constraint_field ||', ';
    end
    ddl = substring(:ddl from 1 for char_length(:ddl)-2);

    select ref.rdb$const_name_uq, ref.rdb$update_rule, ref.rdb$delete_rule
      from rdb$ref_constraints ref
      where ref.rdb$constraint_name = :nazwa or ref.rdb$constraint_name = :constraint_name
      into :constraint_pk, :constraint_update_rule, constraint_delete_rule;

    if(constraint_type = 'FOREIGN KEY') then
    begin
      select r.rdb$relation_name
        from rdb$relation_constraints r
        where r.rdb$constraint_name = :constraint_pk
        into :constraint_relation_name;
      constraint_relation_name = substring(:constraint_relation_name from 1 for position(' ', :constraint_relation_name) - 1);
      ddl = :ddl || ') REFERENCES ' || :constraint_relation_name || '(';

      wygenerowano = 0;
      for select trim(s.rdb$field_name)
        from rdb$index_segments s
        where s.rdb$index_name = :constraint_pk
        order by s.rdb$field_position
      into :constraint_field
      do begin
        constraint_field = (select outname from sys_quote_reserved(:constraint_field));
        ddl = :ddl || :constraint_field ||', ';
        wygenerowano = 1;
      end

      if(wygenerowano=0) then
      begin
        for select s.rdb$field_name
        from rdb$indices w
        join rdb$index_segments s on w.rdb$foreign_key=s.rdb$index_name
        where w.rdb$index_name = :nazwa or (w.rdb$index_name = :constraint_pk_index)
        order by s.rdb$field_position
        into :constraint_field
        do begin
          constraint_field = trim(:constraint_field);
          constraint_field = (select outname from sys_quote_reserved(:constraint_field));
          ddl = :ddl || :constraint_field ||', ';
          wygenerowano = 1;
        end
      end

      if(wygenerowano=0) then
      begin
        for select s.rdb$field_name
        from rdb$relation_constraints w
        join rdb$indices i on w.rdb$index_name=i.rdb$index_name
        join rdb$index_segments s on i.rdb$foreign_key=s.rdb$index_name
        where w.rdb$constraint_name = :nazwa
        order by s.rdb$field_position
        into :constraint_field
        do begin
          constraint_field = trim(:constraint_field);
          constraint_field = (select outname from sys_quote_reserved(:constraint_field));
          ddl = :ddl || :constraint_field ||', ';
          wygenerowano = 1;
        end
      end

      ddl = substring(:ddl from 1 for char_length(:ddl)-2);
      ddl = :ddl || ')';

      constraint_delete_rule = substring(:constraint_delete_rule from 1 for position('  ', :constraint_delete_rule) - 1);
      constraint_update_rule = substring(:constraint_update_rule from 1 for position('  ', :constraint_update_rule) - 1);
      if(constraint_delete_rule <> 'RESTRICT') then
        ddl = :ddl || ' ON DELETE ' || constraint_delete_rule;
      if(constraint_update_rule <> 'RESTRICT') then
        ddl = :ddl || ' ON UPDATE ' ||  constraint_update_rule;

      if(constraint_name <> constraint_pk_index) then
      begin
          if(position('RDB$' in :constraint_pk_index)=0) then
            if(constraint_pk_type = 1) then
              ddl = :ddl || ' USING DESCENDING INDEX ' || :constraint_pk_index;
            else
              ddl = :ddl || ' USING INDEX ' || :constraint_pk_index;
      end
    end
    else if(constraint_type = 'PRIMARY KEY' or constraint_type = 'UNIQUE') then
    begin
      select i.rdb$index_type
        from rdb$relation_constraints r
        join rdb$indices i on r.rdb$index_name = i.rdb$index_name
        where r.rdb$index_name = :nazwa
        into :constraint_pk_type;

      constraint_pk_index = trim(constraint_pk_index);

      if(constraint_name <> constraint_pk_index) then
      begin
        if(position('RDB$' in :constraint_pk_index)>0) then
          ddl = :ddl || ') ';
        else
          if(constraint_pk_type = 1) then
            ddl = :ddl || ') USING DESCENDING INDEX ' || :constraint_pk_index;
          else
            ddl = :ddl || ') USING INDEX ' || :constraint_pk_index;
      end else
        ddl = :ddl || ')';
    end
    ddl = :ddl || ';';
  suspend;
end^
SET TERM ; ^
