--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_VATZD_POS(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      YEARID varchar(4) CHARACTER SET UTF8                           ,
      COMPANY integer,
      RODZAJ integer)
  returns (
      B varchar(255) CHARACTER SET UTF8                           ,
      C varchar(20) CHARACTER SET UTF8                           ,
      D1 varchar(20) CHARACTER SET UTF8                           ,
      D2 date,
      E date,
      F numeric(14,2),
      G numeric(14,2))
   as
declare variable CORRECTIONBKDOCS integer;
begin
  for
    select sum(b.sumnetv), sum(b.sumvatv), b.bkdocsvatcorrectionref
      from bkdocs b join bkdoctypes bt on (b.doctype = bt.ref)
      where b.bkdocsvatcorrectionref is not null and b.period = :period and b.company = :company
        and bt.kind = iif(:rodzaj = 0,2,1) and b.status > 0
      group by b.sumvatv, b.sumnetv, b.bkdocsvatcorrectionref
      into :f, :g, :correctionbkdocs
    do begin
      select b.nip, b.contractor, symbol,regdate, payday
        from bkdocs b
        where ref = :correctionbkdocs
        into :c, :b, :d1, :D2, :e;
    suspend;

    B = null;
    C = null;
    D1 = null;
    D2 = null;
    E = null;
    F = null;
    G = null;
    end
end^
SET TERM ; ^
