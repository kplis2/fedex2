--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_PREVYEAR(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      COL integer)
  returns (
      AMOUNT numeric(15,2))
   as
declare variable frvhdr integer;
declare variable frpsn integer;
declare variable frcol integer;
declare variable cacheparams varchar(255);
declare variable ddparams varchar(255);
declare variable prevyear varchar(4);
declare variable prevmonth varchar(2);
declare variable prevperiod varchar(6);
declare variable prevfrvhdr integer;
declare variable frversion integer;
declare variable company integer;
begin
  if (frvpsn = 0) then exit;
  -- wartosc z poprzedniego roku
  select fp.frvhdr, fp.frpsn, fp.frcol, f.company
    from frvpsns fp
    left join frvhdrs f on fp.frvhdr = f.ref
    where fp.ref = :frvpsn
  into :frvhdr, :frpsn, :frcol, :company;

  select frversion
    from frvhdrs
    where ref = :frvhdr
    into :frversion;
  prevyear = cast(cast(substring(period from 1 for 4) as integer) - 1 as varchar(4));
  prevmonth = substring(period from 5 for 2);
  prevperiod = prevyear||prevmonth;

  select first 1 ref
    from frvhdrs
    where frhdr = (select frhdr from frvhdrs where ref = :frvhdr) and period = :prevperiod
    and company = :company
    order by regdate desc
  into :prevfrvhdr;

  cacheparams = period;

  select amount
    from frvpsns
    where frpsn = :frpsn and frvhdr = :prevfrvhdr and col = :col
    into :amount;

  amount = coalesce(amount,0);
  insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
    values (:frvhdr, :frpsn, :frcol, 'PREVYEAR', :cacheparams, :ddparams, :amount, :frversion);

  suspend;
end^
SET TERM ; ^
