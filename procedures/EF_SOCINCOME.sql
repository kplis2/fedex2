--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_SOCINCOME(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           )
  returns (
      RET numeric(14,2))
   as
declare variable FROMDATE date;
declare variable TODATE date;
declare variable TPER varchar(6);
declare variable LIMIT_SOCIAL numeric(14,2);
declare variable LIMIT_BENEFIT numeric(14,2);
declare variable SOCIALS_PREV numeric(14,2);
declare variable BENEFITS_PREV numeric(14,2);
declare variable SOCIALS numeric(14,2);
declare variable BENEFITS numeric(14,2);
declare variable RET_SOC numeric(14,2);
declare variable RET_BEN numeric(14,2);
declare variable RET_NOLIMTS numeric(14,2);
declare variable COMPANY integer;
begin
--MWr Personel: Wyliczanie przychodu na liscie socjalnej

  select r.tper, r.company, p.fromdate, p.todate
    from epayrolls r
    left join period2dates(r.cper) p on (1 = 1)
    where r.ref = :payroll
    into :tper, :company, :fromdate, :todate;

  --kwota wolna od podatku dla swiadczen ZFSS
  execute procedure get_pval(:todate, :company, 'LIMIT_ZFSS')
    returning_values :limit_social;

  if (limit_social <= 0) then
    limit_social = 0;

  --kwota wolna od podatku dla zapomogi
  execute procedure get_pval(:todate, :company, 'LIMIT_ZAPOMOGA')
    returning_values :limit_benefit;

  if (limit_benefit <= 0) then
    limit_benefit = 0;

  --PR26581 limit swiadczen socjalnych dla emerytow/rencistow jest identyczny jak dla zapomog...
  if (exists(select first 1 1 from eperpension p
                 join employees e on (e.person = p.person)
               where e.ref = :employee and p.fromdate <= :todate
                 and (p.todate is null or p.todate >= :fromdate)) --PR54458
  ) then begin
  --BS39228 ...gdy emeryt nie jest zatrudniony
    if (not(exists(select first 1 1 from employment
                     where employee = :employee and fromdate <= :todate
                       and (todate >= :fromdate or todate is null)))
    ) then
      limit_social = limit_benefit;
  end

  select sum(case when r.ref <> :payroll and c.cflags containing ';LSF;' then p.pvalue else 0 end),
         sum(case when r.ref <> :payroll and c.cflags containing ';LSZ;' then p.pvalue else 0 end),
         sum(case when r.ref = :payroll and c.cflags containing ';LSF;' then p.pvalue else 0 end),
         sum(case when r.ref = :payroll and c.cflags containing ';LSZ;' then p.pvalue else 0 end),
         sum(case when r.ref = :payroll and c.cflags not containing ';LSF;'
                                        and c.cflags not containing ';LSZ;' then p.pvalue else 0 end)
    from epayrolls r
      join eprpos p on (p.payroll = r.ref
                    and p.employee = :employee
                    and r.tper >= substring(:tper from 1 for 4) || '01'
                    and r.tper <= :tper)
      join ecolumns c on (c.number = p.ecolumn
                      and c.coltype = 'SOC'
                      and c.cflags containing ';POD;')
      into :socials_prev, :benefits_prev, :socials, :benefits, :ret_nolimts;

  --przychod ze skladninkow socjalnych, objetych limitem ZFSS i zapomogi, wyliczony
  --odpowiednio z list wczesniejszych w biezacym roku oraz z listy biezacej
  socials_prev = coalesce(socials_prev,0);
  benefits_prev = coalesce(benefits_prev,0);
  socials = coalesce(socials,0);
  benefits = coalesce(benefits,0);

  --przychod nie podlegajacy limitom
  ret_nolimts = coalesce(ret_nolimts,0);

  --przychod z uwzgledniem limitu ZFSS
  ret_soc = 0;
  if (socials + socials_prev > limit_social) then
  begin
    ret_soc = socials + socials_prev - limit_social;
    if (ret_soc > socials) then ret_soc = socials;
  end

  --przychod z uwzgledniem limitu dla zapomogi
  ret_ben = 0;
  if (benefits + benefits_prev > limit_benefit) then
  begin
    ret_ben = benefits + benefits_prev - limit_benefit;
    if (ret_ben > benefits) then ret_ben = benefits;
  end

  --przychod ogolem
  ret = ret_soc + ret_ben + ret_nolimts;

  if (ret < 0) then
    ret = 0;

  suspend;
end^
SET TERM ; ^
