--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_REC_DOC_ALL_GOODS_DETS(
      DOCID integer,
      PALINCLUDE smallint)
  returns (
      VERS integer,
      DOC smallint,
      ORD smallint,
      DOCQ numeric(14,4),
      ORDQ numeric(14,4),
      OSTATUS smallint)
   as
declare variable mwsord integer;
begin
  select first 1 o.ref, o.status from mwsords o where o.docid = :docid
    into mwsord, ostatus;
  for select vers from MWS_REC_DOC_ALL_GOODS (:docid,:palinclude) group by vers
    into vers
  do begin
    doc = 0;
    ord = 0;
    docq = 0;
    ordq = 0;
    if (exists (select first 1 1 from dokumpoz where dokument = :docid and wersjaref = :vers)) then
      doc = 1;
    if (exists (select first 1 1 from mwsacts a where a.mwsord = :mwsord and a.vers = :vers and a.status > 0)) then
      ord = 1;
    --select sum(case when d.akcept = 1 then p.iloscl else p.ilosc end)
    select sum(case when d.mwsdisposition = 1 then p.ilosc else p.iloscl end)
      from dokumpoz p
        left join dokumnag d on (d.ref = p.dokument)
      where p.dokument = :docid and p.wersjaref = :vers
      into docq;
    select sum(a.quantityc)
      from mwsacts a
      where a.mwsord = :mwsord and a.vers = :vers and a.status = 5
      into ordq;
    suspend;
  end
end^
SET TERM ; ^
