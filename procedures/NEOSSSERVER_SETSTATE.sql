--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NEOSSSERVER_SETSTATE(
      REF INTEGER_ID,
      STATE SMALLINT_ID,
      MESSAGE MEMO = null,
      PID INTEGER_ID = null)
   as
begin
  update neossserver_data nsd set
    nsd.starttime = coalesce(iif(state = 2, current_timestamp, null), nsd.starttime),
    nsd.finishtime = coalesce(iif(state = 3 or state = 4, current_timestamp, null), nsd.finishtime),
    nsd.state = :state,
    nsd.MSG = :message,
    nsd.pid = :pid
  where
    nsd.ref = :ref;
end^
SET TERM ; ^
