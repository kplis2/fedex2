--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INTRASTATH_IS_EXIST(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      FROMDATE date,
      TODATE date,
      INTRTYPE smallint,
      INTRVERSION varchar(20) CHARACTER SET UTF8                           ,
      DECLTYPE smallint)
   as
declare variable locfromdate date;
declare variable loctodate date;
declare variable locintrtype smallint;
declare variable locintrversion varchar(20);
declare variable locdecltype smallint;
declare variable msg varchar(250);
begin
  for select intrastath.fromdate,  intrastath.todate, intrastath.intrtype,
        intrastath.intrversion, intrastath.decltype
        from intrastath
        where intrastath.period = :period and intrastath.intrtype = :intrtype
        and intrastath.intrversion = :intrversion and intrastath.decltype = :decltype
      into :locfromdate, :loctodate,  :locintrtype, :locintrversion, :locdecltype
  do begin
    msg = '';
    msg = msg || 'Dla podanych dat granicznych istnieje deklaracja intrastat takiego samego rodzaju ';
    if (:locintrtype = 0) then
      msg = msg || '(Przyjęcie)';
    else if (:locintrtype = 1) then
      msg = msg || '(Wydanie)';
    msg = msg || ', wersji (' || :locintrversion || ') oraz o takim samym typie (';
    if (:locdecltype = 0) then
      msg = msg || 'Zgłoszenie deklaracji)';
    if (:locdecltype = 1) then
      msg = msg || 'Zmiana deklaracji)';
    if (:locdecltype = 0) then
      msg = msg || 'Korekta deklaracji)';
    if(:locfromdate >= :fromdate and :locfromdate <= :todate) then
      exception universal msg;
    if(:locfromdate <= :fromdate and :loctodate >= :fromdate) then
      exception universal msg;
  end
end^
SET TERM ; ^
