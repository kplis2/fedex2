--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_CHECKILWOL(
      POZZAM integer,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      TYP char(1) CHARACTER SET UTF8                           ,
      DOSTAWA integer)
  returns (
      ILDOST numeric(14,4))
   as
declare variable ILBLOK numeric(14,4);
declare variable ILNIEDOST numeric(14,4);
declare variable ILBLOKSTANCEN numeric(14,4);
declare variable USLUGA smallint;
declare variable POZZAM2 integer;
declare variable MWS smallint;
begin
  execute procedure mag_checkilwol_c(null,:pozzam,:magazyn,:wersjaref,:typ,:dostawa,null)
    returning_values :ildost;
end^
SET TERM ; ^
