--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NEOS_ZMIENSEGREGATOR(
      PLIK integer,
      SEGREGATOR integer,
      OPERATOR integer)
   as
begin
  --exception universal :plik || '-' || :segregator;
  if(coalesce(:PLIK,0)=0) then exception universal 'Nie można ustalić pliku!';
  if(coalesce(:PLIK,0)=0) then exception universal 'Nie można ustalić segregatora docelowego!';
  if(not exists(select dp.ref from dokplik dp where dp.ref = :plik and (dp.blokada=0 or (dp.blokada = 1 and dp.operblok = :operator))))
    then exception universal 'Dokument pobrany przez innego operatora!';
  else update dokplik d set d.segregator = :segregator where d.ref = :plik;
end^
SET TERM ; ^
