--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GET_DOSTCEN(
      PWERSJAREF integer,
      PDOSTAWCA integer,
      PWALUTA varchar(10) CHARACTER SET UTF8                           ,
      PODDZIAL varchar(20) CHARACTER SET UTF8                           ,
      WALUTADOWOLNA smallint)
  returns (
      CENANET numeric(14,4),
      CENABRU numeric(14,4),
      CENA_FAB numeric(14,4),
      CENA_KOSZT numeric(14,4),
      CENA_DETAL numeric(14,4),
      JEDN integer,
      WALUTA varchar(3) CHARACTER SET UTF8                           ,
      CENAWAL numeric(14,4),
      RABAT numeric(14,4),
      DATAOD timestamp,
      DATADO timestamp,
      SYMBOL_DOST varchar(100) CHARACTER SET UTF8                           ,
      NAZWA_DOST varchar(255) CHARACTER SET UTF8                           ,
      STATUS smallint,
      PREC smallint)
   as
declare variable wref integer;
declare variable company integer;
declare variable oldoddzial varchar(20);
begin
  if(:poddzial is null or :poddzial='') then execute procedure GETCONFIG('AKTUODDZIAL') returning_values :poddzial;
  cenanet = null;
  cenabru = null;
  cena_fab = null;
  cena_koszt = null;
  cena_detal = null;
  jedn = 0;
  wref = null;
  status = 0;
  --wyszukiwanie dla zadanej waluty lub polskiej
    if(:pwaluta is null or :pwaluta='') then begin
      execute procedure GETCONFIG('WALPLN') returning_values :pwaluta;
      if(:pwaluta is null or :pwaluta='') then waluta = 'PLN';
    end
    -- najpierw szukamy dla danej waluty i oddzialu
    select wersjaref,cenanet,cenabru,cena_fab,cena_koszt,cena_detal,jedn,waluta,cenawal,rabat,dataod,datado,symbol_dost, nazwa_dost, prec
      from dostcen
      where WERSJAREF=:pwersjaref and DOSTAWCA=:pdostawca and WALUTA=:pwaluta and ODDZIAL=:poddzial
      into :wref, :cenanet, :cenabru, :cena_fab, :cena_koszt, :cena_detal, :jedn, :waluta, :cenawal, :rabat, :dataod, :datado, :symbol_dost, :nazwa_dost, :prec;
      oldoddzial = :poddzial;
    if(:wref is null) then begin
    -- potem szukamy dla oddzialu centrali
      execute procedure get_global_param('CURRENTCOMPANY') returning_values :company;
      select min(ODDZIAL) from ODDZIALY where COMPANY=:company and GLOWNY=1 into :poddzial;
      if(:poddzial<>:oldoddzial) then begin
        select wersjaref,cenanet,cenabru,cena_fab,cena_koszt,cena_detal,jedn,waluta,cenawal,rabat,dataod,datado,symbol_dost, nazwa_dost, prec
          from dostcen
          where WERSJAREF=:pwersjaref and DOSTAWCA=:pdostawca and WALUTA=:pwaluta and ODDZIAL=:poddzial
          into :wref, :cenanet, :cenabru, :cena_fab, :cena_koszt, :cena_detal, :jedn, :waluta, :cenawal, :rabat, :dataod, :datado, :symbol_dost, :nazwa_dost, :prec;
        oldoddzial = :poddzial;
      end
    end
    if(:wref is null) then begin
    -- potem szukamy dla dowolnego innego oddzialu w ramach biezacego zakladu
      select min(dostcen.oddzial)
        from DOSTCEN
        join ODDZIALY on (ODDZIALY.ODDZIAL=DOSTCEN.ODDZIAL)
        where DOSTCEN.WERSJAREF=:pwersjaref and DOSTCEN.DOSTAWCA=:pdostawca and DOSTCEN.WALUTA=:pwaluta
        and ODDZIALY.COMPANY=:company
        into :poddzial;
      if(:poddzial<>:oldoddzial) then begin
        select wersjaref,cenanet,cenabru,cena_fab,cena_koszt,cena_detal,jedn,waluta,cenawal,rabat,dataod,datado,symbol_dost, nazwa_dost, prec
          from dostcen
          where WERSJAREF=:pwersjaref and DOSTAWCA=:pdostawca and WALUTA=:pwaluta and ODDZIAL=:poddzial
          into :wref, :cenanet, :cenabru, :cena_fab, :cena_koszt, :cena_detal, :jedn, :waluta, :cenawal, :rabat, :dataod, :datado, :symbol_dost, :nazwa_dost, :prec;
        oldoddzial = :poddzial;
      end
    end

  if(:walutadowolna=1 and :wref is null) then begin -- wyszukiwanie dla dowolnej waluty
    -- najpierw szukamy dla danego oddzialu
    select min(waluta)
      from DOSTCEN
      where WERSJAREF=:pwersjaref and DOSTAWCA=:pdostawca and ODDZIAL=:poddzial
      into :pwaluta;
    select wersjaref,cenanet,cenabru,cena_fab,cena_koszt,cena_detal,jedn,waluta,cenawal,rabat,dataod,datado,symbol_dost,nazwa_dost, prec
      from dostcen
      where WERSJAREF=:pwersjaref and DOSTAWCA=:pdostawca and WALUTA=:pwaluta and ODDZIAL=:poddzial
      into :wref, :cenanet, :cenabru, :cena_fab, :cena_koszt, :cena_detal, :jedn, :waluta, :cenawal, :rabat, :dataod, :datado, :symbol_dost, :nazwa_dost, :prec;
      oldoddzial = :poddzial;
    if(:wref is null) then begin
    -- potem szukamy dla oddzialu centrali
      execute procedure get_global_param('CURRENTCOMPANY') returning_values :company;
      select min(ODDZIAL) from ODDZIALY where COMPANY=:company and GLOWNY=1 into :poddzial;
      if(:poddzial<>:oldoddzial) then begin
        select min(waluta)
          from DOSTCEN
          where WERSJAREF=:pwersjaref and DOSTAWCA=:pdostawca and ODDZIAL=:poddzial
          into :pwaluta;
        select wersjaref,cenanet,cenabru,cena_fab,cena_koszt,cena_detal,jedn,waluta,cenawal,rabat,dataod,datado,symbol_dost,nazwa_dost, prec
          from dostcen
          where WERSJAREF=:pwersjaref and DOSTAWCA=:pdostawca and WALUTA=:pwaluta and ODDZIAL=:poddzial
          into :wref, :cenanet, :cenabru, :cena_fab, :cena_koszt, :cena_detal, :jedn, :waluta, :cenawal, :rabat, :dataod, :datado, :symbol_dost,:nazwa_dost,:prec;
        oldoddzial = :poddzial;
      end
    end
    if(:wref is null) then begin
    -- potem szukamy dla dowolnego innego oddzialu w ramach biezacego zakladu
      select min(dostcen.oddzial)
        from DOSTCEN
        join ODDZIALY on (ODDZIALY.ODDZIAL=DOSTCEN.ODDZIAL)
        where DOSTCEN.WERSJAREF=:pwersjaref and DOSTCEN.DOSTAWCA=:pdostawca
        and ODDZIALY.COMPANY=:company
        into :poddzial;
      if(:poddzial<>:oldoddzial) then begin
        select min(waluta)
          from DOSTCEN
          where WERSJAREF=:pwersjaref and DOSTAWCA=:pdostawca and ODDZIAL=:poddzial
          into :pwaluta;
        select wersjaref,cenanet,cenabru,cena_fab,cena_koszt,cena_detal,jedn,waluta,cenawal,rabat,dataod,datado,symbol_dost,nazwa_dost, prec
          from dostcen
          where WERSJAREF=:pwersjaref and DOSTAWCA=:pdostawca and WALUTA=:pwaluta and ODDZIAL=:poddzial
          into :wref, :cenanet, :cenabru, :cena_fab, :cena_koszt, :cena_detal, :jedn, :waluta, :cenawal, :rabat, :dataod, :datado, :symbol_dost,:nazwa_dost,:prec;
        oldoddzial = :poddzial;
      end
    end
  end
  if(:wref is not null) then status=1;
  if(:cenanet is null) then cenanet = 0;
  if(:cenabru is null) then cenabru = 0;
  if(:cena_fab is null) then cena_fab = 0;
  if(:cena_koszt is null) then cena_koszt = 0;
  if(:cena_detal is null) then cena_detal = 0;
  if(:cenawal is null) then cenawal = 0;
  if(:rabat is null) then rabat = 0;
  if(:dataod is null) then dataod = '1900-01-01';
  if(:datado is null) then datado = '2999-12-31';
  if(:jedn is null) then jedn = 0;
  if(:prec is null) then prec = 0;
  suspend;
end^
SET TERM ; ^
