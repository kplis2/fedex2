--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SERIAL_NUMBER_DOKUMSER_EXISTS(
      SERIAL varchar(40) CHARACTER SET UTF8                           ,
      REF integer,
      TYP varchar(1) CHARACTER SET UTF8                           )
  returns (
      RET smallint)
   as
begin
ret = 0;
if (:typ is null) then typ = 'M';
  if(ref = 0 or ref is null) then begin
    if (exists(select dokumser.ref from dokumser where DOKUMSER.ODSERIAL = :serial)) then ret = 1;
    suspend;
  end else begin
    if (exists(select * from DOKUMSER D
               where D.ODSERIAL = :serial and D.REFPOZ = :REF and D.TYP = :typ)) then ret = 1;
    suspend;
  end
end^
SET TERM ; ^
