--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CONTROL_PESEL_NUMBER(
      PESEL varchar(11) CHARACTER SET UTF8                           )
  returns (
      BIRTHDATE timestamp,
      SEX smallint,
      CORRECT integer,
      MSG varchar(60) CHARACTER SET UTF8                           )
   as
declare variable TOTAL integer;
declare variable I integer;
declare variable DIGIT integer;
declare variable SCALES integer;
declare variable RESULTMOD integer;
declare variable BIRTHYEAR integer;
begin
  if (pesel is NULL or pesel = '') then begin
    msg = ''; correct = 1;
  end
  else begin
    msg = '';
    correct = 0;
    sex = -1;
    --mod = 10;
    total = 0;
    if((coalesce(char_length(pesel),0) = 11) and (pesel <> '' or pesel is not null)) then begin -- [DG] XXX ZG119346
      i = 1;
      while (i < 11) do begin
        if (i = 1) then begin digit = substring(PESEL from 1 for 1 ); scales = 1; end
        if (i = 2) then begin digit = substring(PESEL from 2 for 1 ); scales = 3; end
        if (i = 3) then begin digit = substring(PESEL from 3 for 1 ); scales = 7; end
        if (i = 4) then begin digit = substring(PESEL from 4 for 1 ); scales = 9; end
        if (i = 5) then begin digit = substring(PESEL from 5 for 1 ); scales = 1; end
        if (i = 6) then begin digit = substring(PESEL from 6 for 1 ); scales = 3; end
        if (i = 7) then begin digit = substring(PESEL from 7 for 1 ); scales = 7; end
        if (i = 8) then begin digit = substring(PESEL from 8 for 1 ); scales = 9; end
        if (i = 9) then begin digit = substring(PESEL from 9 for 1 ); scales = 1; end
        if (i = 10) then begin digit = substring(PESEL from 10 for 1 ); scales = 3; end
        total = total + (scales * digit);
        i = i + 1;
      end
      resultmod = total - (floor(total / 10) * 10);
      if (resultmod = 0) then resultmod = 10;
      digit = (substring(PESEL from 11 for 1 ));
      if (:digit = 10 - :resultmod) then
      begin
        correct = 1;

        if (substring(PESEL from 3 for 1) in (0,1)) then
          birthyear = 1900 + cast(substring(PESEL from 1 for 2) as smallint);
        else if (substring(PESEL from 3 for 1) in (2,3)) then
          birthyear = 2000 + cast(substring(PESEL from 1 for 2) as smallint);
        else if (substring(PESEL from 3 for 1) in (4,5)) then
          birthyear = 2100 + cast(substring(PESEL from 1 for 2) as smallint);
        else if (substring(PESEL from 3 for 1) in (6,7)) then
          birthyear = 2200 + cast(substring(PESEL from 1 for 2) as smallint);
        else if (substring(PESEL from 3 for 1) in (8,9)) then
          birthyear = 1800 + cast(substring(PESEL from 1 for 2) as smallint);

        birthdate = cast(
              (cast(birthyear as char(4))
                ||'-'||
              cast(cast(mod(cast(substring(PESEL from 3 for 1) as smallint), 2) as smallint) as char(1))
                ||
              substring(PESEL from 4 for 1)
                ||'-'||
              substring(PESEL from 5 for 2)) as date);

        sex = mod(cast(substring(PESEL from 10 for 1) as smallint),2);

      end
      else begin
        msg = 'Nie zgadza się cyfra kontrolna';
        correct = 0;
      end
    end else begin
      msg = 'Nieprawidłowa ilość cyfr';
      correct = 0;
    end
  end
  suspend;
end^
SET TERM ; ^
