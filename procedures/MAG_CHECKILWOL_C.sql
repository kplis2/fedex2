--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_CHECKILWOL_C(
      ZAM integer,
      POZZAM integer,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      WERSJAREF integer,
      TYP char(1) CHARACTER SET UTF8                           ,
      DOSTAWA integer,
      CENA numeric(14,4))
  returns (
      ILDOST numeric(14,4))
   as
declare variable ILBLOK numeric(14,4);
declare variable ILNIEDOST numeric(14,4);
declare variable ILBLOKSTANCEN numeric(14,4);
declare variable ILFREE numeric(14,4);
declare variable USLUGA smallint;
declare variable POZZAM2 integer;
declare variable WERSJA integer;
declare variable MWS smallint;
declare variable KTM varchar(40);
declare variable fake smallint;
begin
  if(cena=0) then cena = null;
  select w.ktm, w.nrwersji, w.usluga
    from wersje w
      join towary t on (w.ktm = t.ktm)
    where w.ref=:wersjaref
  into :ktm, :wersja, :usluga;
  ildost = 0;
  ilfree = 0;
  if(:usluga = 1) then
  begin
    ildost = -1;
    exit;
  end
  select coalesce(mws,0)
    from defmagaz d
    where d.symbol = :magazyn
    into mws;
  if(dostawa=0) then dostawa = null;
  ilblok = 0;
  if (coalesce(zam,0) = 0) then
  begin
    if(:typ = 'Z') then
    begin
      select coalesce(p.fake,0)
        from pozzam p
        where p.ref = :pozzam
        into :fake;

      select sum(ILOSC)
        from STANYREZ
        where WERSJAREF = :wersjaref and magazyn = :magazyn and (dostawa = :dostawa or :dostawa is null)
          and STATUS = 'B' and ZREAL = 0 and POZZAM=:pozzam
        into :ilblok;
    end else if (:typ = 'F') then
    begin
      select p.frompozzam, coalesce(p.fake,0)
        from pozfak p
        where p.ref = :pozzam
        into :pozzam2, :fake;

      select sum(ILOSC)
        from STANYREZ
        where WERSJAREF = :wersjaref and magazyn = :magazyn and (dostawa = :dostawa or :dostawa is null)
          and STATUS = 'B' and ((ZREAL = 2 and POZZAM=:pozzam) or (ZREAL = 0 and POZZAM=:pozzam2 and :pozzam2>0))
        into :ilblok;
    end else if(typ = 'M')then
    begin
      select p.frompozzam, coalesce(p.fake,0)
        from dokumpoz p
        where p.ref = :pozzam
        into :pozzam2, :fake;

      select sum(ILOSC)
        from STANYREZ
        where WERSJAREF = :wersjaref and magazyn = :magazyn and (dostawa = :dostawa or :dostawa is null)
          and STATUS = 'B' and ((ZREAL = 3 and POZZAM=:pozzam) or (ZREAL = 0 and POZZAM=:pozzam2 and :pozzam2>0))
        into :ilblok;
    end
  end else if (:zam > 0) then
  begin
    select sum(STANYREZ.ilosc)
      from STANYREZ
        join POZZAM on (POZZAM.REF = STANYREZ.POZZAM)
      where POZZAM.zamowienie = :ZAM and POZZAM.wersjaref = :wersjaref
        and ((pozzam.cenamag = :cena) or (:cena = 0) or (:cena is null))
        and ((pozzam.dostawamag = :dostawa) or(:dostawa = 0) or (:dostawa is null))
        and STANYREZ.ZREAL = 0 and STANYREZ.STATUS='B'
      into :ilblok;
  end
  if(:ilblok is null) then ilblok = 0;
  if (mws > 0) then
  begin
    execute procedure get_free_mws_stan(:magazyn,:wersjaref,:cena,:dostawa)
      returning_values ildost;

    execute procedure rez_get_free_stan(:magazyn,:ktm,:wersja,:cena,:dostawa)
      returning_values ilfree;

    if (coalesce(:fake,0) = 1) then ilfree = :ildost;
    if (ilfree is null) then ilfree = 0;
  end else if (mws = 0 and dostawa is null) then
  begin
    execute procedure rez_get_free_stan(:magazyn,:ktm,:wersja,:cena,:dostawa)
      returning_values :ilfree;
  end else if (mws = 0 and dostawa is not null) then
  begin
    execute procedure rez_get_free_stan(:magazyn,:ktm,:wersja,:cena,:dostawa)
      returning_values :ilfree;
  end
  if(:ildost is null) then ildost = 0;
  ilfree = :ilfree + :ilblok;
  if (mws > 0) then
  begin
    if (ilfree < ildost) then
      ildost = ilfree;
  end
  else
    ildost = ilfree;
  if (ildost < 0) then ildost = 0;
end^
SET TERM ; ^
