--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PERSON_ADD_COMPANY(
      PERSON integer,
      COMPANY integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                            = null)
   as
begin
/*Dodanie powiazania Osoba-Zaklad. Procedura wywolywana m.in. przez editpersonsf.cpp
  Blokada wywolania, gdy dodajemy osobe przez widok CPERSONS */

  if(rdb$get_context('USER_TRANSACTION', 'PERSON_ADD_COMPANY_EXIT') = '1') then
    exit;

  if (company is null) then
    execute procedure get_global_param('CURRENTCOMPANY')
      returning_values :company;

  insert into eperscompany(person, company, symbol)
    values(:person, :company, :symbol);

end^
SET TERM ; ^
