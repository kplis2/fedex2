--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GETBINARYPHOTOS4REF(
      REF INTEGER_ID)
  returns (
      X_ZDJECIE FOTO_BINARY)
   as
begin
  select tp.x_zdjecie
        from towpliki tp
        where tp.ref = :ref
      into :x_zdjecie;
  suspend;
end^
SET TERM ; ^
