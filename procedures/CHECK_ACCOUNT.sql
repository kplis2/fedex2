--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHECK_ACCOUNT(
      COMPANY integer,
      ACCOUNT ACCOUNT_ID,
      YEARID integer,
      MODE smallint,
      RETURNZERO smallint = 0)
  returns (
      RESULT smallint,
      DIST1 integer,
      DIST2 integer,
      DIST3 integer,
      DIST4 integer,
      DIST5 integer,
      DIST6 integer,
      MULTI smallint)
   as
declare variable HASDISTS smallint;
declare variable NUMBER integer;
declare variable DISTDEF integer;
declare variable PREVIOUS_REF integer;
declare variable CURRENT_REF integer;
begin

  --BS69913  - dodajemy unikalnego refa, aby nie rzezbilo po calym dysku, gdy wykonujemy N operacji w ramach jednej transakcji
  previous_ref = rdb$get_context('USER_TRANSACTION', 'CHECK_ACCOUNT.CONTEXT_ID');
  current_ref = coalesce(previous_ref + 1, 0);
  rdb$set_context('USER_TRANSACTION', 'CHECK_ACCOUNT.CONTEXT_ID', :current_ref);

  execute procedure check_account_ext(:company, :account, :yearid, :mode, :current_ref)
    returning_values :result, :hasdists, :multi;
  --/BS69913
  if (:hasdists = 1) then
    for
      select NUMBER, DISTDEF
        from FK_CHECK_ACCOUNT_DISTDEF
        --BS69913  - indeks
        where context_id = :current_ref
        --/BS69913
        order by coalesce(number, number)
      into :number, :distdef
    do begin
      if (:number = 1) then
        dist1 = :distdef;
      else if (:number = 2) then
        dist2 = :distdef;
      else if (:number = 3) then
        dist3 = :distdef;
      else if (:number = 4) then
        dist4 = :distdef;
      else if (:number = 5) then
        dist5 = :distdef;
      else if (:number = 6) then
        dist6 = :distdef;
    end
  if (RETURNZERO <> 0) then
  begin
    if (DIST1 is null) then
      dist1 = 0;
    if (DIST2 is null) then
      dist2 = 0;
    if (DIST3 is null) then
      dist3 = 0;
    if (DIST4 is null) then
      dist4 = 0;
    if (DIST5 is null) then
      dist5 = 0;
    if (DIST6 is null) then
      dist6 = 0;
    if (MULTI is null) then
      multi = 0;
  end
  --BS69913  sprzatamy po sobie przed wyjsciem z procedury, oczywiscie po indeksie
  delete from fk_check_account_distdef fcad
    where fcad.context_id = :current_ref;
  --/BS69913

  suspend;
end^
SET TERM ; ^
