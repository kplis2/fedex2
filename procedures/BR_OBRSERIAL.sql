--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_OBRSERIAL(
      REJDOK varchar(3) CHARACTER SET UTF8                           ,
      TYPDOK varchar(3) CHARACTER SET UTF8                           ,
      SERIALNR varchar(40) CHARACTER SET UTF8                           ,
      SERIALNRR integer)
  returns (
      REFSER integer,
      SERIAL varchar(40) CHARACTER SET UTF8                           ,
      DATA timestamp,
      DOSTAWCA integer,
      KLIENT integer,
      SYMBOLFAK varchar(30) CHARACTER SET UTF8                           ,
      SYMBOLFAKD varchar(30) CHARACTER SET UTF8                           ,
      REF integer,
      ILOSC numeric(14,4),
      CENAFAB numeric(14,2),
      CENAZAK numeric(14,2),
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      WERSJAREF integer,
      KTO SHORTNAME_ID,
      KTONAZWA varchar(255) CHARACTER SET UTF8                           ,
      NAZWAT varchar(255) CHARACTER SET UTF8                           ,
      NAZWAW varchar(255) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE ODSERIALNR INTEGER;
DECLARE VARIABLE DOSERIALNR INTEGER;
DECLARE VARIABLE SSERIAL VARCHAR(41);
begin
  sserial = :serialnr||'%';
  for select DOKUMSER.REF, NAGFAK.dostawca, NAGFAK.klient,NAGFAK.symbol,NAGFAK.symbfak,
             NAGFAK.REF, NAGFAK.data,
             DOKUMSER.ILOSC, DOKUMSER.ODSERIAL, DOKUMSER.ODSERIALNR, DOKUMSER.DOSERIALNR,
             POZFAK.cenacen, POZFAK.cenanet,
             POZFAK.KTM, POZFAK.WERSJA, POZFAK.WERSJAREF,
             WERSJE.NAZWA, WERSJE.NAZWAT
    from DOKUMSER join POZFAK on (DOKUMSER.refpoz = POZFAK.REF and DOKUMSER.TYP = 'F')
                  join NAGFAK on (NAGFAK.REF = POZFAK.DOKUMENT)
                  join WERSJE on (WERSJE.REF = POZFAK.WERSJAREF)
    where
      (DOKUMSER.odserial like :sserial or (DOKUMSER.odserialnr <= :serialnrr and DOKUMSER.doserialnr >=:serialnrr))
     and (NAGFAK.rejestr = :rejdok or (:rejdok = '')) and (NAGFAK.typ = :typdok or (:typdok = '')) and NAGFAK.akceptacja = 1
    order by DOKUMSER.odserial
  into :refser, :dostawca, :klient,:symbolfak, :SYMBOLFAKD,:ref,:data,
        :ILOSC,:serial, :odserialnr, :doserialnr,
        :cenafab, :cenazak, :ktm, :wersja, :wersjaref,
        :NAZWAW, :NAZWAT
  do begin
   if(:klient > 0) then
     select FSKROT, NAZWA from KLIENCI where REF=:klient into :kto, :ktonazwa;
   else if(:dostawca > 0) then
     select ID, NAZWA from DOSTAWCY where REF=:dostawca into :kto, :ktonazwa;
   if(:odserialnr is not null and :doserialnr is not null) then begin
     if(:odserialnr <= :serialnrr and :doserialnr >= :serialnrr) then  begin
       serial = :serialnrr;
       ilosc = 1;
       suspend;
     end
   end else
     suspend;
  end

end^
SET TERM ; ^
