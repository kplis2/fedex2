--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RP_CHECK_REF(
      TABELA varchar(31) CHARACTER SET UTF8                           ,
      OLDREF integer)
  returns (
      REF integer)
   as
declare variable aktulocat varchar(255);
begin
  --jesli jest wpis w rp_syncdef,
  --to wówczas obliczam odpowiedniego ref'a
  ref = oldref;
  if (exists(select tabela from RP_SYNCDEF where TABELA=:tabela)) then
  begin
    execute procedure GETCONFIG('AKTULOCAT')
      returning_values :aktulocat;
    if (aktulocat<>'' and cast(aktulocat as integer)>0) then
      ref = ref * 16 + (cast(aktulocat as integer)-1);
  end
  suspend;
end^
SET TERM ; ^
