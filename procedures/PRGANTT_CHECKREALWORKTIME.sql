--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRGANTT_CHECKREALWORKTIME(
      PRGANTTREF integer,
      PRMACHINE integer,
      STARTTIME timestamp,
      WORKTIME double precision)
  returns (
      REALWORKTIME float)
   as
declare variable endtime timestamp;
declare variable prdepart varchar(10);
declare variable prschedule integer;
declare variable minlength double precision;
declare variable operendtime timestamp;
declare variable setuptime integer;
declare variable old_shoper varchar(255);
declare variable new_shoper varchar(255);
begin
  realworktime = worktime;
  minlength = 1;
  minlength = :minlength/1440;
  select s.prdepart, s.ref
    from prgantt g
      left join PRschedules s on (s.ref = g.prschedoper)
    where g.ref = :prganttref
    into :prdepart, :prschedule;
  if(:prmachine is not null) then
  begin
    --srawdzenie, czy bedzie wystepować czas przezbrojenia - wowczas dodanie jego, jesli poprzednia operacja jest innej kartytechnologicznej
    select meansetuptime from PRMACHINES where ref=:prmachine
      into :setuptime;
    if(:setuptime > 0) then begin
      -- sprawdzenie czy poprzednia operacja jest inna od nowej;
      select descript from prshopers where ref=(select prshoper from prgantt where ref = :prganttref) into :new_shoper;
      select descript from prshopers where ref=(select first 1 prshoper from prgantt where prschedule = :prschedule and prmachine = :prmachine order by timeto desc) into :old_shoper;
      if (:old_shoper<>:new_shoper) then begin
        select max(timeto) from prgantt
        where prschedule = :prschedule and prmachine = :prmachine and timeto < :starttime
        into :operendtime;
        if(:operendtime + (:minlength * :setuptime) > :starttime) then
          realworktime = :realworktime + ((:minlength * :setuptime)-(:starttime - :operendtime));
      end
    end
    --TODO: sprawdzenie, czy w czasie realworktime od startu nie ma przerw maszyny, jesli tak, to dodanie ich do realworktime
  end
  execute procedure pr_calendar_checkendtime(:prdepart, :starttime, :realworktime) returning_values :endtime;
  realworktime = :endtime - :starttime;
  --TODO: sprawdzenie, czy w czasie realworktime nie ma przerw w pracy wydzialu
end^
SET TERM ; ^
