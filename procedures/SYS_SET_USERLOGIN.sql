--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_SET_USERLOGIN(
      USR varchar(40) CHARACTER SET UTF8                           ,
      PSW varchar(40) CHARACTER SET UTF8                           ,
      USERID integer,
      DBASE varchar(40) CHARACTER SET UTF8                           ,
      DBUSR varchar(40) CHARACTER SET UTF8                           ,
      DBPSW varchar(40) CHARACTER SET UTF8                           )
  returns (
      STATUS integer,
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable currcryptedpsw varchar(255);
declare variable currsupervisor smallint;
declare variable currsupervisegroup varchar(255);
declare variable currsente smallint;
declare variable curruserid varchar(255);
declare variable oldsupervisegroup varchar(255);
declare variable userlogin varchar(255);
begin
  status = 0;
  msg = '';
  currcryptedpsw = shash_hmac('hex','sha1',:psw,:usr);
  -- zautoryzuj usera ktory wywoluje procedure
  curruserid = NULL;
  select first 1 u.supervisor, u.supervisegroup, u.sente, u.userid
  from s_users u
  where u.userlogin = :usr and u.userpass = :currcryptedpsw
  into :currsupervisor, :currsupervisegroup, :currsente, :curruserid;
  if(:curruserid is null) then begin
    status = 0;
    msg = 'Brak autoryzacji do zarządzania użytkownikami!';
    suspend;
    exit;
  end
  -- jesli userid jest null to znaczy ze ustawiamy dostep biezacemu uzytkownikowi
  if(:userid is null) then userid = :curruserid;
  -- czy user istnieje
  if(:userid<>0 and exists(select first 1 1 from s_users u where u.userid=:userid)) then begin
     -- sprawdzamy kogo poprawiamy
     select first 1 u.supervisegroup, u.userlogin from s_users u where u.userid=:userid into :oldsupervisegroup, :userlogin;
     if(:oldsupervisegroup is null) then oldsupervisegroup = '';
     -- czy biezacy user ma w ogole prawo go poprawic
     if((:currsupervisor=1 and :oldsupervisegroup like :currsupervisegroup||'%') or  -- admin moze zarzadzac wszystkimi w swojej supervisegroup
        (:currsupervisor>1) or   -- developer moze zarzadzac wszystkimi
        (:currsente=1)  -- sente moze zarzadzac wszystkimi
       ) then begin

       if(exists(select first 1 1 from s_userlogins l where l.userid=:userid and l.dbase=:dbase)) then begin
         if(:dbusr<>'') then begin
           update s_userlogins l set l.userlogin=:dbusr, l.userpass=:dbpsw where l.userid=:userid and l.dbase=:dbase;
           msg = 'Zaktualizowano login bazodanowy użytkownika: '||:userlogin;
         end else begin
           delete from s_userlogins l where l.userid=:userid and l.dbase=:dbase;
           msg = 'Usunieto login bazodanowy użytkownika: '||:userlogin;
         end
       end else begin
         insert into s_userlogins(dbase, userlogin, userpass) values (:dbase, :dbusr, :dbpsw);
         msg = 'Dodano login bazodanowy użytkownika: '||:userlogin;
       end
       status = 1;
     end else begin
       status = 0;
       msg = 'Brak autoryzacji do użytkownika: '||:userlogin;
     end
  end else begin
    status = 0;
    msg = 'Nie znaleziono użytkownika!';
  end
end^
SET TERM ; ^
