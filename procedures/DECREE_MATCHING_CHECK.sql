--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DECREE_MATCHING_CHECK(
      DECREE DECREES_ID)
  returns (
      MATCHABLE SMALLINT_ID)
   as
declare variable BKACCOUNT BKACCOUNTS_ID;
begin
  select a.bkaccount
    from decrees d
      left join bkdocs b on (b.ref = d.bkdoc)
      left join bkperiods p on (p.id = b.period and p.company = b.company)
      left join accounting a on (a.account = d.account and a.company = b.company and a.yearid = p.yearid and a.currency = d.currency)
    where d.ref = :decree
    into :bkaccount;
 if (:bkaccount > 0) then
   execute procedure bkaccount_matching_check(:bkaccount)
     returning_values (:matchable);
  suspend;
end^
SET TERM ; ^
