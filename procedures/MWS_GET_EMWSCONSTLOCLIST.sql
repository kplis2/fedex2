--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_GET_EMWSCONSTLOCLIST(
      MWSORD MWSORDS_ID,
      CART MWSCONSTLOCS_ID)
  returns (
      EMWSCONSTLOCLIST STRING255)
   as
declare variable releasedoc smallint_id; --ha ha ha brak pomyslu na nazwe ;)
declare variable out smallint_id;
declare variable wh2 defmagaz_id;

declare variable docgroup dokumnag_id;
begin
  select docgroup from mwsords where ref = :mwsord
  into :docgroup;

  emwsconstloclist = '';

  select substring(list(l.symbol||': '||c.mwsconstlocsymb, '; ') from 1 for 255)
    from dokumnag d
      left join mwsordcartcolours c on (d.ref = c.docid)
      left join mwsconstlocs l on (c.cart = l.ref)
    where d.grupasped = :docgroup
      and c.status = 1
  into :emwsconstloclist;

  if (coalesce(char_length(:emwsconstloclist),0) > 60) then -- [DG] XXX ZG119346
  begin
    emwsconstloclist = substring(:emwsconstloclist from 1 for 60)||'; ...';
  end
end^
SET TERM ; ^
