--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_CHECK_CART_LIST(
      LISTWYSD LISTYWYSD_ID)
  returns (
      IS_ALL_CHECK SMALLINT_ID,
      STATUS SMALLINT_ID,
      MSG STRING1024)
   as
  declare variable cartref mwsconstlocs_id;
  declare variable symbol_tmp string20;
  declare variable lokacja_tmp string20;
  declare variable eol string3;
begin
  --procedura sluzy do wylistowania jeszcze nie zeskanowanych wozkow dla dokumentu spedycyjnego
  -- zwraca tez informacje czy zostaly jeszcze jakies wozki do zeskanowania
  --arguments:
  --listwysd ref dokumentu spedycyjnego z tabeli listywysd
  --cart symbol wozka
  --returns:
  --is_all_check 1 - wszystkie wozki zeskanowane 0 - jeszcze nie wszystkie wozki zeskanowane
  --status  1 - bez bledu 0 - blad
  --msg komunikat bledu lub w przypdaku braku bledu lista wozkow
  eol ='
  ';
  status = 1;
  msg = 'Pobierz wózki:' || eol;
  is_all_check = 1;
  --sprawdzenie poprawnosci danych
  if(listwysd is null ) then
  begin
    status = 0;
    msg = 'Nie podano numeru listu spedycyjnego.';
    exit;
  end
  --budowanie komunikatu
  for
    select distinct ms.symbol, mc.mwsconstlocsymb
    from mwsordcartcolours mc
      join dokumnag dg on dg.ref = mc.docid
      join listywysd ld on ld.refdok = dg.grupasped
      join mwsconstlocs ms on mc.cart = ms.ref
      where ld.ref = :listwysd
        and mc.status < 2
        and coalesce(mc.x_check_on_pack, 0) = 0
    into :symbol_tmp, :lokacja_tmp
  do begin
    msg = msg || coalesce(symbol_tmp,'<brak wózka>') || ' z ' || coalesce(lokacja_tmp,'<brak lokacji>') || eol;
    is_all_check = 0;
  end
  if (is_all_check = 1) then
  begin
    msg = 'Wszystkie wózki zostaly pobrane.';
  end
end^
SET TERM ; ^
