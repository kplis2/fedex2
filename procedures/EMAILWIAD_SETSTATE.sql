--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAILWIAD_SETSTATE(
      MAILBOX integer,
      MAILFOLDER varchar(255) CHARACTER SET UTF8                           ,
      FOLDER DEFFOLD_ID,
      UIDS varchar(2048) CHARACTER SET UTF8                           ,
      STAN smallint,
      SCOPE smallint = 0)
   as
begin
  if(folder is null) then
    select first 1 ref from deffold
    where mailbox=:mailbox and mailfolder=:mailfolder
    into :folder;

  if(SCOPE=1) then
    update emailwiad set stan=:stan where mailbox=:mailbox and folder=:folder;
  else if (SCOPE=2) then
    update emailwiad set stan=:stan where mailbox=:mailbox;
   else if (uids<>'') then
    execute statement ('update emailwiad set stan=:stan where mailbox=:mailbox and folder=:folder and
      uid in ('||:uids||')') (stan:=stan, mailbox:=mailbox, folder:=folder);
end^
SET TERM ; ^
