--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EHRM_ADD_APPLICATION(
      EPRESENCELISTSPOS EPRESENCELISTSPOS_ID,
      FROMTIME STRING20,
      TOTIME STRING20,
      DESCRIPTION STRING5000,
      EMPLOYEE EMPLOYEES_ID,
      STATE SMALLINT_ID,
      PRESENCEDATE DATE_ID)
   as
declare variable EPRESENCELISTS type of EPRESENCELISTS_ID; /* STATE */
declare variable EPRESENCELISTNAG type of EPRESENCELISTNAG_ID;
declare variable PRDEPARTS type of PRDEPARTS_ID;
declare variable OPERATOR integer;
declare variable ECONTRACTSPOSDEF integer;
declare variable ECONTRACTSDEF integer;
begin
  --- Sprawdzanie poprawnoci danych
  if (:fromtime >= :totime) then exception EHRM_ADD_APPLICATION_LOGIC_ERRO;

    --- Ustalamy Ref presencelists
  select distinct l.ref
    from epresencelists l
    where l.listsdate = cast(:presencedate as timestamp)
    into :epresencelists;

  --- Update rekordu
  if(:epresencelistspos is not null) then
    update epresencelistspos lp set lp.newstartat = :fromtime, lp.newstopat = :totime, lp.state = :state, lp.descapply = :description
      where lp.ref = :epresencelistspos;
  --- Dodanie nowego rekordu
  else
  begin
  --- Ustalenie danych pomocniczych
  --prdeparts =  jak ustalic wydzial pracownika

    select distinct ep.operator, eppos.econtractsposdef, eppos.econtractsdef
      from econtrpayrollspos eppos
        left join econtrpayrolls ep on (ep.ref = eppos.econtrpayrolls)
      where cast(eppos.sdate as date) <= :presencedate and cast(eppos.sdate as date) >= :presencedate
        and eppos.accord = 1
      into :operator, :econtractsposdef, :econtractsdef;

    --- Ustalamy Ref presencelistnag
    if(epresencelists is not null) then
      select ln.ref
        from epresencelistnag ln
        where ln.employee = :employee and ln.epresencelist = :epresencelists
        into :epresencelistnag ;

    --- W przyadku braku rekordów doadanie nowych
    if(epresencelists is null) then
      insert into EPRESENCELISTS (LISTSDATE) values (:presencedate)
        returning Ref into :epresencelists;

    if(epresencelistnag is null) then
      insert into epresencelistnag (employee, timestart, timestop, epresencelist, wastoday, shift, operator)
        values(:employee, '08:00', '16:00', :epresencelists, 1, 0, null )  -- dodać obsuge uzupelniania timestart timeend
        returning Ref into :epresencelistnag;

    --- Wykonanie inserta
    if(epresencelists is not null or epresencelistnag is not null) then
      insert into epresencelistspos (epresencelists, econtractsdef, operator, accord,
        timestartstr,timestopstr,epresencelistnag,pointssource,econtractposdef,employee,
        NEWSTARTAT, NEWSTOPAT, DESCAPPLY, STATE)
      values(:epresencelists, :econtractsdef, :operator, 1, '00:00',
        '00:00', :epresencelistnag, 0,:econtractsposdef,:employee,
        :fromtime, :totime, :description, :state);
    else
      exception UNIVERSAL 'Nie udalo sie dodac rekoedu epresenclist =' || :epresencelists || 'epresencelistnag= ' || :epresencelistnag;
  end

  update epresencelistspos e set e.newstopat = :fromtime, e.newstartat = e.timestartstr, e.state = 1
    where e.epresencelists = :epresencelists and employee = :employee and timestop is null and e.timestartstr < :fromtime;
end^
SET TERM ; ^
