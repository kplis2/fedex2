--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_GET_GOODQUANTITY(
      BRANCH varchar(20) CHARACTER SET UTF8                           ,
      WH varchar(3) CHARACTER SET UTF8                           ,
      VERS integer)
  returns (
      MWSQUICK numeric(15,4),
      MWSSELLAV numeric(15,4),
      MWSTOTAL numeric(15,4))
   as
begin
  if (wh is null and branch is not null) then
    select sum(m.mwsquick), sum(m.mwssellav), sum(m.mwstotal)
      from mwsversq m
        left join defmagaz d on (d.symbol = m.wh)
      where m.vers = :vers and d.oddzial = :branch
      into mwsquick, mwssellav, mwstotal;
  else if (wh is null and branch is null) then
    select sum(m.mwsquick), sum(m.mwssellav), sum(m.mwstotal)
      from mwsversq m
      where m.vers = :vers
      into mwsquick, mwssellav, mwstotal;
  else if (wh is not null) then
    select sum(m.mwsquick), sum(m.mwssellav), sum(m.mwstotal)
      from mwsversq m
      where m.vers = :vers and m.wh = :wh
      into mwsquick, mwssellav, mwstotal;
  if (mwsquick is null) then mwsquick = 0;
  if (mwstotal is null) then mwstotal = 0;
  if (mwssellav is null) then mwssellav = 0;
  if (mwsquick < 0) then mwsquick = 0;
  if (mwstotal < 0) then mwstotal = 0;
  if (mwssellav < 0) then mwssellav = 0;
  suspend;
end^
SET TERM ; ^
