--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PLANSCOLS_GEN(
      PLANS integer,
      FROMDATE timestamp,
      TODATE timestamp,
      PERIOD varchar(255) CHARACTER SET UTF8                           ,
      AKTIV varchar(1) CHARACTER SET UTF8                           )
  returns (
      COLSAMOUNT integer)
   as
declare variable fromdatecol timestamp;
declare variable todatecol timestamp;
declare variable activcol smallint;
declare variable periodcol varchar(20);
begin
  colsamount = 0;
  if(aktiv = '1') then activcol = 1; else activcol = 0;
  if(period = '0') then periodcol = 'DZIEN';
  if(period = '1') then periodcol = 'TYDZIEN';
  if(period = '2') then periodcol = 'DEKADA';
  if(period = '3') then periodcol = 'MIESIAC';
  if(period = '4') then periodcol = 'KWARTAL';
  if(period = '5') then periodcol = 'POLROCZE';
  if(period = '6') then periodcol = 'ROK';

  for select namefromdate, nametodate from gen_dimelemtime(:periodcol,:fromdate,:todate)
   into :fromdatecol, :todatecol
   do begin
     insert into planscol(plans,bdata,ldata,akt) values (:plans, :fromdatecol, :todatecol, :activcol);
     colsamount = colsamount + 1;
   end
  suspend;
end^
SET TERM ; ^
