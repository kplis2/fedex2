--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_ADD_COLUMN(
      TABLENAME varchar(255) CHARACTER SET UTF8                           ,
      FIELDNAME varchar(255) CHARACTER SET UTF8                           ,
      FIELDSIZE varchar(255) CHARACTER SET UTF8                           ,
      FIELDPREC varchar(255) CHARACTER SET UTF8                           ,
      FIELDDOMAIN varchar(255) CHARACTER SET UTF8                           ,
      FIELDNOTNULL varchar(1) CHARACTER SET UTF8                           ,
      DICTTABLE varchar(255) CHARACTER SET UTF8                           ,
      DICTFIELD varchar(255) CHARACTER SET UTF8                           ,
      DESCRIPTION varchar(1024) CHARACTER SET UTF8                           ,
      FIELDSCALE varchar(255) CHARACTER SET UTF8                           )
   as
declare variable SQL varchar(1024);
declare variable QUERYHEADER varchar(255);
declare variable ISCREATING smallint;
begin
  if(exists(SELECT 1 FROM RDB$RELATIONS WHERE RDB$RELATION_NAME = :tablename))
  then
    queryheader = 'ALTER TABLE ' || tablename || ' ADD ';
  else begin
    queryheader = 'CREATE TABLE ' || tablename || '( ';
    iscreating = 1;
  end
    sql = queryheader || fieldname || ' ' || fielddomain;
    if (fielddomain = 'VARCHAR' or fielddomain = 'CHAR') then sql = sql || '(' || fieldsize || ')';
    if (fielddomain = 'NUMERIC') then sql = sql || '(' || fieldprec || ',' || fieldscale || ')';
    if (fieldnotnull = 'Y') then sql = sql || ' NOT NULL';
    if(iscreating = 1) then sql = sql || ')';
    sql = sql || ';';
    --exception universal sql;
    execute statement sql;

    --jeżeli zalozona zostala tabela to trzeba jeszcze nalozyc na nia granty
    if(iscreating = 1) then
    begin
      sql = 'GRANT ALL ON ' || :tablename || ' TO SENTE;';
      execute statement sql;
    end

    --zaloz klucz obcy jesli jest taka potrzeba
    if (dicttable <> '') then
    begin
      sql = 'ALTER TABLE ' || tablename || ' ADD CONSTRAINT FK_' || tablename || '_' || fieldname || ' FOREIGN KEY (' || fieldname || ') REFERENCES ' || dicttable || ' (' || dictfield || ')' || ' ON UPDATE CASCADE' || ';';
      --exception universal sql;
      execute statement sql;
    end
    --uzupelnij description pola jesli zostal zdefiniowany
    if(description <> '')  then
    execute statement 'comment on column ' || tablename || '.' || fieldname || ' is ''' || description || ''';';
end^
SET TERM ; ^
