--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NOTIFSTATE_TEST(
      NOTIFSTEP integer)
   as
declare variable ID varchar(20);
declare variable PARENTID varchar(20);
declare variable DESCRIPT varchar(255);
declare variable VAL varchar(255);
declare variable NUMVAL numeric(14,2);
declare variable WATCHBELOW smallint;
declare variable COLORINDEX integer;
declare variable PRIORITY integer;
declare variable ICON varchar(255);
declare variable MODULE varchar(255);
declare variable ACTIONID varchar(255);
declare variable ACTIONPARAMS varchar(255);
declare variable ACTIONNAME varchar(255);
declare variable RIGHTSGROUP varchar(255);
declare variable RIGHTS varchar(255);
declare variable company integer;
declare variable query varchar(5000);
declare variable notiftype smallint;
declare variable notifsheet integer;
begin
  select query,notiftype,statesheet from notifsteps where ref=:notifstep into :query,:notiftype,:notifsheet;
  if(:notiftype<>2) then exception UNIVERSAL 'Funkcja dotyczy tylko monitora systemu';
  execute procedure NOTIFSTATE_PREPARE(:notifstep);
  for execute statement :query
  into :id,:parentid,:descript,:val,:numval,:watchbelow,:colorindex,:priority,
       :icon,:module,:actionid,:actionparams,:actionname,:rightsgroup,:rights,:company
  do begin
    execute procedure NOTIFSTATE_SET (:notifstep,:id,:parentid,:notifsheet,
    :descript,:val,:numval,:watchbelow,:colorindex,:priority,
    :icon,:module,:actionid,:actionparams,:actionname,:rightsgroup,:rights,:company);
  end
end^
SET TERM ; ^
