--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_UPS_TRACK_REQUEST(
      OTABLE varchar(31) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable SHIPINGREF integer;
declare variable SHIPINGSYMBOL varchar(60);
declare variable GRANDPARENT integer;
declare variable SUBPARENT integer;
declare variable LICENCENUMBER varchar(255);
declare variable USERNAME varchar(255);
declare variable USERPASSWORD varchar(255);
declare variable FILEPATH varchar(255);
declare variable requestOption char(1);
begin
  val = 1;
  parent = null;
  params = null;
  id = 0;
  name = 'SendTrackingRequest';
  suspend;

  GrandParent = id;

  select k.klucz, k.login, k.haslo, k.sciezkapliku
    from spedytkonta k
    where k.spedytor = 'UPS'
  into :licencenumber, :username, :userpassword, :filepath;

  val = :username;
  name = 'UserName';
  id = id + 1;
  parent = GrandParent;
  suspend;

  val = :userpassword;
  name = 'UserPassword';
  id = id + 1;
  parent = GrandParent;
  suspend;

  val = :licencenumber;
  name = 'AccesLicenseNumber';
  id = id + 1;
  parent = GrandParent;
  suspend;

  val = 'TEST'; --jesli chcesz testowac zmien na TEST
  name = 'ServiceUrlType';
  id = id + 1;
  parent = GrandParent;
  suspend;

  requestOption = '0'; --tylko ostania operacja
  /*
  if (:otable starting with 'LISTYWYSD' and position(':' in  :otable) > 0) then
  begin
    if (substr(:otable, position(':' in  :otable) + 1, coalesce(char_length(:otable),0)) = 'ALL') then
      requestOption = '1';
    else if (substr(:otable, position(':' in  :otable) + 1, coalesce(char_length(:otable),0)) = 'SIGN') then
      requestOption = '8';
  end
  */

  val = :requestOption; --todo
  name = 'RequestOption';
  id = id + 1;
  parent = GrandParent;
  suspend;

  val = :filepath;
  name = 'FilePath';
  id = id + 1;
  parent = GrandParent;
  suspend;

  --nie zmieniac, dotyczy pomijania bledow z bramki
  val = '1';
  name = 'ShowErrors';
  id = id + 1;
  parent = GrandParent;
  suspend;

  if (:otable starting with 'LISTYWYSD') then
  begin
    if (not exists(select first 1 1 from listywysd where ref = :oref)) then
      exception ede_ups_listywysd_brak;

    select l.ref, l.symbolsped from listywysd l
      where l.ref = :oref
        and coalesce(l.spedresponse,'') <> '-1'
        and coalesce(l.x_anulowany,0) = 0
        --and coalesce(trim(l.symbolsped),'') not in ('','ZEWNUPS','ANULOWANO')
        and coalesce(l.symbolsped,'') != ''
    into :shipingRef, :shipingSymbol;

    val = 1;
    id = :id + 1;
    name = 'Shipment';
    parent = :GrandParent;
    SubParent = id;
    suspend;

    val = :shipingRef;
    id = :id + 1;
    name = 'DocRef';
    parent = :SubParent;
    suspend;

    val = :shipingSymbol;
    id = :id + 1;
    name = 'TrackingNumber';
    parent = :SubParent;
    suspend;
  end
  else if (:otable starting with 'LISTYWYS') then
  begin
    if (not exists(select first 1 1 from listywys where ref = :oref)) then
      exception ede_ups_listywysd_brak;

    for
      select l.ref, l.symbolsped from listywysd l
        where l.listawys = :oref
          and coalesce(l.spedresponse,'') <> '-1'
          and coalesce(l.x_anulowany,0) = 0
          and coalesce(trim(l.symbolsped),'') not in ('','ZEWNUPS','ANULOWANO')
        order by l.ref
      into :shipingRef, :shipingSymbol
    do begin
      val = 1;
      id = :id + 1;
      name = 'Shipment';
      parent = :GrandParent;
      SubParent = id;
      suspend;

      val = :shipingRef;
      id = :id + 1;
      name = 'DocRef';
      parent = :SubParent;
      suspend;

      val = :shipingSymbol;
      id = :id + 1;
      name = 'TrackingNumber';
      parent = :SubParent;
      suspend;
    end
  end
end^
SET TERM ; ^
