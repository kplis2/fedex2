--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHEET_CHECKOPER(
      REFOPER integer)
  returns (
      OPMSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable nr smallint;
declare variable complex smallint;
declare variable opertype smallint;  --typ operacji : szereg. rownol.
declare variable oper varchar(20);  --typ operacji - propers.symbol
declare variable scheduled smallint;
declare variable calculated smallint;
declare variable eoperfactor varchar(255); --wsp. taryfy
declare variable eopertime varchar(255); --czas pracy
declare variable workplace varchar(20); --stanowisko pracy
begin
  opmsg = '';
  select o.number, o.complex, o.oper, o.opertype, o.calculated, o.eoperfactor,
      o.scheduled, o.eopertime, o.workplace
    from prshopers o
    where o.ref = :refoper
    into :nr, :complex, :oper, :opertype, :calculated, :eoperfactor,
      :scheduled, :eopertime, :workplace;
  if (coalesce(:nr,0)=0) then opmsg = :opmsg || 'Numer;';
  if (:complex is null) then opmsg = :opmsg || 'Typ;';
  if (:complex = 1 and :opertype is null) then opmsg = :opmsg || 'Rodzaj;';
  if (:calculated = 1 and coalesce(:eoperfactor,'') = '') then opmsg = :opmsg || 'Wsp. taryfy;';
  if ((:calculated = 1 or :scheduled = 1) and coalesce(:oper,'') = '') then opmsg = :opmsg || 'Typ operacji;';
  if (:scheduled = 1) then begin
    if (coalesce(:eopertime,'') = '') then opmsg = :opmsg || 'Czas pracy;';
    if (coalesce(:workplace,'') = '') then opmsg = :opmsg || 'Stanowisko;';
  end
  if (coalesce(char_length(:opmsg),0) > 0) then -- [DG] XXX ZG119346
    opmsg = 'W operacji nr ' || :nr || ' brak nast. danych:' || :opmsg;
end^
SET TERM ; ^
