--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CRM_PROGRAMLOJ_OBLICZFROMPROG(
      CPLREF integer,
      SPOSOBURUCH varchar(1) CHARACTER SET UTF8                           )
   as
declare variable stable varchar(31);
declare variable sref integer;
declare variable regula integer;
declare variable proced varchar(520);
declare variable zakresfrom varchar(255);
declare variable zakreswhere varchar(255);
declare variable cnt integer;
begin
  /*dla kazdego typu dokumentu, ktory jest liczony automatycznie*/
  for select distinct cplreguly.typdok, cplreguly.zakresfrom, cplreguly.zakreswhere
  from CPLREGULY where PROGLOJ=:cplref
   and cplreguly.autooblicz = 2
  into :stable, :zakresfrom, :zakreswhere
  do begin
    if(:zakresfrom is null) then zakresfrom = '';
    if(:zakreswhere is null) then zakreswhere = '';
    if(:zakresfrom <> '') then begin
       /*sprawdzenie, czy rekord pasuje do zadanej dziedziny*/
       proced = 'select REF from '||:zakresfrom;
       if(:zakreswhere <> '') then begin
         proced = :proced||' where ('||:zakreswhere||')';
       end
       proced = :proced || ' order by REF';
    end else
      proced = 'select '||:stable||'.REF from '||:stable||' where PROGLOJDONE = 0';
    for execute statement :proced
    into :sref
    do begin                                
      execute procedure CRM_PROGRAMLOJ_OBLICZFORDOK(:stable, :sref, 2,:sposoburuch);
    end
  end
end^
SET TERM ; ^
