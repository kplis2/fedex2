--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EF_FTCOLSUM(
      EMPLOYEE integer,
      PAYROLL integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      FLAG varchar(4) CHARACTER SET UTF8                           ,
      FROMECOL integer = null,
      TOECOL integer = null)
  returns (
      RET numeric(14,2))
   as
declare variable TPER char(6);
declare variable PAYDAY timestamp;
declare variable PERSON integer;
declare variable SEPARATE smallint;
begin
/*DU: Personel - funkcja liczy sume wartosci skladnikow o zadanej fladze z
                 innych listy plac z tego samego okresu podatkowego.
                 Mozliwe jest pobranie wartosci z zadanego zakresu
                 skladnikow: od FROMECOL do TOECOL. */

  select tper, payday, coalesce(separate,0)
    from epayrolls
    where ref = :payroll
    into :tper, :payday, separate;
 
  if (separate = 0) then
  begin
    select person
      from employees
      where ref = :employee
      into :person;
  
    select sum(p.pvalue)
      from eprpos p
        join epayrolls r on (p.payroll = r.ref)
        join eprempl e on (e.epayroll = p.payroll and e.employee = p.employee)
        join ecolumns c on (c.number = p.ecolumn)
      where e.person = :person
        and p.payroll <> :payroll
        and r.tper = :tper
        and r.empltype = 1
        and (c.cflags containing ';' || :flag || ';' or :flag is null)
        and p.ecolumn >= coalesce(:fromecol,0) --BS54341
        and p.ecolumn <= coalesce(:toecol,9999999)
        and (r.payday < :payday or (r.payday = :payday and r.ref < :payroll))
        and coalesce(r.separate,0) = 0
      into :ret;
  end

  if (ret is null) then
    ret = 0;

  suspend;
end^
SET TERM ; ^
