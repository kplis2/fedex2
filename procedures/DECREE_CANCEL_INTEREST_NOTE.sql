--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DECREE_CANCEL_INTEREST_NOTE(
      NOTYNAG integer,
      DOCTYPE integer,
      OPERID integer,
      AKCEPT smallint,
      DATA timestamp,
      KWOTA NUMERIC_14_2 = 0)
  returns (
      BKDOC integer)
   as
declare variable BKREG varchar(10);
declare variable DICTDEF integer;
declare variable DICTPOS integer;
declare variable TMP varchar(255);
declare variable DEBIT numeric(14,2);
declare variable CREDIT numeric(14,2);
declare variable SIDE smallint;
declare variable ACCOUNT ACCOUNT_ID;
declare variable SETTLEMENT varchar(20);
declare variable DOCDATE timestamp;
declare variable DESCRIPT varchar(80);
declare variable BOOKING varchar(255);
declare variable COMPANY integer;
declare variable SALDOWM numeric(14,2);
declare variable KODKS ACCOUNT_ID;
declare variable AMOUNT numeric(14,2);
declare variable SYMBOL varchar(20);
declare variable NOTAREF integer;
declare variable cnt integer;
begin
  cnt = -1;
  select count(*) from bkdocs where symbol like (select SYMBOL from NOTYNAG where ref=:notynag)||'/A%' and status = 0
   into cnt;
  if (cnt > 0) then
    exception universal 'Istnieją niezakceptowane anulowania noty.
Zaakceptuj je przed wystawieniem kolejnej!';
  amount = kwota/100;  -- blednie przekazywalo wartosci po przecinku wiec jest na okolo
  if(amount <= 0) then exception interest_note_amount 'Kwota anulowania mniejsza lub równa 0. Anulowanie nie możliwe.';
  BKDOC = 0;
  select company, bkreg from BKDOCTYPES where REF=:doctype
    into :company, :bkreg;
  execute procedure GEN_REF('BKDOCS')
    returning_values bkdoc;

  select slodef, slopoz, symbol, data, slokodks, symbol
    from notynag where ref = :notynag
    into :dictdef, :dictpos, :settlement, :docdate, :kodks, symbol;

  select saldowm from rozrach
    where slodef = :dictdef and slopoz = :dictpos and symbfak = :settlement
          and company = :company and kontofk like '%'||:kodks
    into :saldowm;

  if(amount > saldowm) then exception interest_note_amount;
  cnt = 0;
  select count(*) from bkdocs where symbol like (select SYMBOL from NOTYNAG where ref=:notynag)||'/A%' into cnt;

  insert into BKDOCS(REF, company, BKREG, DOCTYPE, TRANSDATE, SYMBOL, DESCRIPT, DOCDATE,
                     REGOPER, DICTDEF, DICTPOS, OTABLE, OREF)
              select :bkdoc, :company, :bkreg, :doctype, :data, SYMBOL||'/A'||:cnt, SYMBOL, :data,
                     :OPERID, SLODEF, SLOPOZ, 'NOTYNAG_A', REF
              from NOTYNAG
              where ref=:notynag;

  execute procedure KODKS_FROM_DICTPOS(dictdef, dictpos)
    returning_values tmp;
  account = '200-' || tmp;

  if (amount < 0 or amount = 0) then
    exception NO_NOTE_AMOUNT_TO_CANCEL :amount;

  amount = -amount;

  if (settlement <> '') then
    execute procedure insert_decree_settlement(:bkdoc, :account, 0, :amount, :symbol||'-anulowanie', :settlement, null, null, null, null, null, null, 'NOTYNAG', :notynag, null, 0);
  else
    execute procedure insert_decree(:bkdoc, :account, 0, :amount, :symbol||'-anulowanie', 0);

  execute procedure insert_decree(:bkdoc, '750-05', 1, :amount, :symbol||'-anulowanie', 0);

  if (akcept = 1) then
  begin
    update BKDOCS set STATUS = 1, ACCOPER=:operid where REF=:bkdoc;
    execute procedure GET_CONFIG('BOOK_WITH_ACCEPT', 2)
      returning_values :booking;
    if (booking='1') then
      update BKDOCS set STATUS=2, BOOKOPER=:operid where REF=:bkdoc;
  end
  if (bkdoc>0) then
    update NOTYNAG set status=2
      where ref=:notynag;

  update notynag set anulowanie = 1
    where ref = :notynag;
end^
SET TERM ; ^
