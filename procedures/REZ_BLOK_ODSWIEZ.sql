--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REZ_BLOK_ODSWIEZ(
      REJESTR varchar(3) CHARACTER SET UTF8                           )
   as
declare variable wydruk smallint;
declare variable nagzam integer;
begin
  for select n.ref, n.wydrukowano
   from nagzam n where n.stan = 'B' and n.rejestr = :REJESTR
   into :nagzam, :wydruk
  do begin
    if(:wydruk is null or :wydruk = 0) then wydruk = 10;
    else wydruk = 11;
    update nagzam set stan = 'N', wydrukowano = :wydruk where ref = :nagzam;
  end
  for select n.ref, n.wydrukowano
   from nagzam n where n.stan = 'N' and n.rejestr = :REJESTR and n.wydrukowano is not null
     and n.wydrukowano >= 10
   into :nagzam, :wydruk
  do begin
    if(:wydruk = 11) then wydruk = 1;
    else wydruk = 0;
    update nagzam set stan = 'B', wydrukowano = :wydruk where ref = :nagzam;
  end
end^
SET TERM ; ^
