--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_ADD_GLS_REQUEST(
      SHIPPINGDOC integer,
      EDERULESYMB varchar(40) CHARACTER SET UTF8                           )
  returns (
      EDEDOCHREF integer)
   as
declare variable EDERULE integer;
declare variable SPEDRESPONSE varchar(255);
declare variable OTABLE varchar(31);
declare variable OREF integer;
declare variable SHIPPINGDOCNO varchar(20);
declare variable anulowany smallint_id;
declare variable listywysd_akcept smallint_id;
declare variable SYMBOLSPED STRING40;
begin
  if (:ederulesymb like 'EDE_GLS_ORDER%' or :ederulesymb like 'EDE_GLS_PICKUPCREATE%'
  ) then
  begin
    select lw.ref, lw.symbolsped from listywys lw where ref = :shippingdoc
    into :oref, :symbolsped;

    if (:oref is null) then
      exception ede_listywysd_brak;
    if (coalesce(:symbolsped,'') != '') then
      exception ede_listywysd_brak 'Zlecenie transportowe zostało juz wysłane';

    otable = 'LISTYWYS';
  end
  else
  begin
    select l.ref, substring(l.symbolsped from 1 for 20), l.statussped, coalesce(l.x_anulowany,0), l.akcept from listywysd l where l.ref = :shippingdoc
    into :oref, :shippingdocno, :spedresponse, :anulowany, :listywysd_akcept;
    if (:oref is null) then
      exception ede_listywysd_brak;
    if (:oref is not null and coalesce(:shippingdocno,'') = '' and :ederulesymb <> 'EDE_GLS_SHIPMENT') then
      exception ede_listywysd_brak 'Dokument nie został jeszcze nadany '|| :oref || ' ' || coalesce(:shippingdocno,'') || ' ' || :ederulesymb || ' ' || :shippingdoc;
    if (coalesce(:spedresponse,'') = '1' and :ederulesymb = 'EDE_GLS_SHIPMENT' and :anulowany != 1) then
      exception ede_listywysd_brak 'Dokument został już wysłany. Anuluj wysyłkę i nadaj ją jeszcze raz.';
    if(coalesce(:listywysd_akcept,0) != 2)then
      exception ede_listywysd_brak 'Nie można nadać niezamkniętego dokumentu';
      

    otable = 'LISTYWYSD';
  end

  select es.ref
    from ederules es
    where es.symbol = :ederulesymb
    into :ederule;

  insert into ededocsh( ederule, direction, createdate, otable, oref, status, filename, manualinit)
    values(:ederule, 1, current_timestamp, :otable, :oref, 0, :shippingdoc||'exp',0)
    returning ref into :ededochref;

  suspend;
end^
SET TERM ; ^
