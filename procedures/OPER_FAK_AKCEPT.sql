--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_FAK_AKCEPT(
      GRUPAFAK integer)
  returns (
      STATUS integer)
   as
begin
  status = -1;
  update NAGFAk set AKCEPTACJA=1 where GRUPAFAK = :grupafak and AKCEPTACJA = 0;
  status = 1;
end^
SET TERM ; ^
