--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GEN_EMPLCALDAYS(
      EMPLOYEE integer)
   as
declare variable calendar integer;
declare variable fromdate timestamp;
declare variable todate date;
declare variable minfromdate date;
declare variable enddate date;
declare variable cdate date;
declare variable ref integer;
declare variable autosync smallint;
declare variable cyear integer;
declare variable cmonth smallint;
declare variable cday smallint;
declare variable daykind smallint;
declare variable workstart time;
declare variable workend time;
declare variable nomworktime integer;
declare variable descript varchar(30);
declare variable startdate date;
begin
/*MWr: Personel - Generowanie dni kalendarza pracowniczego (EMPLCALDAYS) dla
       danego pracownika EMPLOYEE na podstawie przypisanych mu kalendarzy (EMPLCALENDAR)*/
  for
    select fromdate, todate, calendar
      from emplcalendar
      where employee = :employee
      order by fromdate
      into :fromdate, :todate, :calendar
  do begin

    if (minfromdate is null) then
    begin
      select min(cdate) from ecaldays
        where calendar = :calendar
        group by calendar
        into :minfromdate;

      if (minfromdate > fromdate ) then
        fromdate = minfromdate;   --zabezpieczenie, kiedy 'Data Od' pierwszego kalendarza wychodzi poza zakres jego dni
      else
        minfromdate = fromdate;
   end

    select max(cdate) from ecaldays
      where calendar = :calendar
      group by calendar
      into :enddate;

    if (enddate > todate) then
      enddate = todate;

    cdate = fromdate;

    select startdate from ecalendars
      where ref = :calendar
      into :startdate;

    if (startdate > cdate) then  --BS58039
      cdate = startdate;

    --przegladamy po kolei kazdy dzien
    while(cdate <= enddate) do
    begin

      if(not exists(select first 1 1 from ecaldays
           where calendar = :calendar and cdate = :cdate))
      then
        exception brak_dnia_w_kalendarzu;

      select ref, cyear, cmonth, cday, daykind, workstart, workend, worktime, descript
        from ecaldays
        where calendar = :calendar
          and cdate = :cdate
        into :ref, :cyear, :cmonth, :cday, :daykind, :workstart,:workend, :nomworktime, :descript;

      autosync = null;
      select autosync from emplcaldays
        where cdate = :cdate
          and employee = :employee
        into :autosync;

      if (autosync is null) then --nie ma dnia w tabeli EMPLCALDAYS
      begin
        insert into emplcaldays (ecalday, daykind, cdate, descript, pyear, pmonth,
            pday, autosync, employee, workstart, workend, nomworktime, ecalendar)
          values (:ref, :daykind, :cdate, :descript, :cyear, :cmonth,
            :cday, 1, :employee, :workstart, :workend, :nomworktime, :calendar);
      end else
      if (autosync = 1) then     --brak modyfikacji indywidualnej
      begin
        update emplcaldays
          set ecalday = :ref, ecalendar = :calendar,
              workstart = :workstart, workend = :workend, daykind = :daykind,
              nomworktime = :nomworktime, descript = :descript
          where cdate = :cdate and employee = :employee;
      end else
      begin                      --jest modyfikacja indywidualna
        update emplcaldays
          set ecalday = :ref, ecalendar = :calendar
          where cdate = :cdate and employee = :employee;
      end

      cdate = :cdate + 1;
    end
  end

  --usuniecie dni kalendarza poza zakresem przewidzianym w EMPLCALENDAR
  delete from emplcaldays
    where employee = :employee
      and autosync = 1
      and ((:minfromdate is not null and (cdate > :enddate or cdate < :minfromdate))
        or :minfromdate is null);

  --aktualizacja informacji o ostatnim kalendarzu na pracowniku
  update employees
    set calendar = :calendar
    where ref = :employee and calendar is distinct from :calendar;
end^
SET TERM ; ^
