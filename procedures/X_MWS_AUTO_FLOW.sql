--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_AUTO_FLOW(
      MWSCONSTLOCL INTEGER_ID,
      AKTUOPERATOR OPERATOR_ID,
      FROMMWSORD INTEGER_ID = 0)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING1024)
   as
declare variable X_ROW_TYPE smallint_id;
declare variable X_FLOW_GROUP integer_id;
declare variable X_FLOW_ORDER smallint_id;
declare variable ref_mwsconstlocp integer_id;
declare variable ref_mwsact mwsords_id;
declare variable ref_mwsord mwsords_id;
declare variable mwspalloc integer_id;
declare variable wh string3;
declare variable timestart timestamp_id;
declare variable timestartdcl timestamp_id;
declare variable timestopdcl timestamp_id;
declare variable mwsaccessory integer_id;
declare variable period period_id;
declare variable branch string10;
declare variable good string40;
declare variable vers integer_id;
declare variable whareap integer_id;
declare variable maxnumber integer_id;
declare variable quantity quantity_mws;
declare variable realtime double precision;
declare variable mixtakepal integer_id;
declare variable mixleavepal integer_id;
declare variable mixmwsconstlocl integer_id;
declare variable mixmwspallocl integer_id;
declare variable mixrefill smallint_id;
declare variable check_x_flow_order smallint_id;
declare variable x_partia date_id;
declare variable x_serial_no integer_id;
declare variable x_slownik integer_id;
declare variable lot integer_id;
begin
  status = 1;
  maxnumber = 1;
  msg = '';
-- pobranie grupy z pozycji magazynowej - mwsconsloc
  select mc.x_row_type, mc.x_flow_group, mc.x_flow_order, mc.wh
    from mwsconstlocs mc
    where mc.ref = :mwsconstlocl
  into :x_row_type, :x_flow_group, :x_flow_order, :wh;
-- sprawdzamy czy magazyn przeplywowy i pozycja pobrania = 1 - pierwsza
  if (x_row_type = 1 and x_flow_order = 1) then begin
-- generowanie mwsord
    select oddzial from defmagaz where symbol = :wh into branch;   --branch
    select ref from mwsaccessories where aktuoperator = :aktuoperator into :mwsaccessory;  --mwsaccessory
    select okres from datatookres(current_date,0,0,0,0) into period; --period
    execute procedure gen_ref('MWSORDS') returning_values ref_mwsord; --ref_mwsord
    insert into mwsords (ref, mwsordtype, stocktaking, doctype, operator, priority, description, wh,
          regtime, timestartdecl, timestopdecl, mwsaccessory, branch, period, cor, rec,
          bwharea, ewharea, status, frommwsord )
    values (:ref_mwsord, 9, 0, 'O', :aktuoperator, 0, 'auto przeplyw palet', :wh,
          current_timestamp(0), :timestart, current_timestamp(0), :mwsaccessory, :branch, :period, 0, 0,
          null, null, 0, :frommwsord);

    if (exists(select first 1 1 from mwsconstlocs mc join mwsstock ms on mc.ref = ms.mwsconstloc
      where ms.quantity > 0
        and mc.x_flow_group = :x_flow_group)) then begin
      check_x_flow_order = 2; -- zabezpieczenie kolejnosci palet w grupie - max(x_flow_order -1)
      LoopA:
      for
        select mc.ref, mc.x_flow_order  -- dla kazdej pozycji magazynowej z grupy pozycji  2, 3, 4, 5
          from mwsconstlocs mc
          where mc.x_flow_group = :x_flow_group
            and mc.x_flow_order <> 1 --pominiecie pustej pozycji 1
          order by mc.x_flow_order asc --kolejnosc 2, 3, 4, 5
        into :ref_mwsconstlocp, :x_flow_order --ref dla pozycji  2, 3, 4, 5 i x_flow_order
      do begin
        if (check_x_flow_order = x_flow_order) then begin -- aby nie wykonywac ruchow jesli z jakiegos powodu grupa nie pelna np. 1-2-4-5
          select wharea from mwsconstlocs where ref = :ref_mwsconstlocp into whareap; -- whareap
-- dla kazdej pozycji na stoku
          for
            select good, vers, quantity, mwspalloc, mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, mixrefill, x_partia, x_serial_no, x_slownik, lot
              from mwsstock ms
              where ms.mwsconstloc = :ref_mwsconstlocp
                and quantity > 0
            into :good, :vers, :quantity, :mwspalloc, :mixtakepal, :mixleavepal, :mixmwsconstlocl, :mixmwspallocl, :mixrefill, :x_partia, :x_serial_no, :x_slownik, :lot
--               :good, :vers, :quantity, :mwspalloc, :mixtakepal, :mixleavepal, :mixmwsconstlocl, :mixmwspallocl, :mixrefill
          do begin
  -- znalezienie max. numeru operacji - kolejne zwiekszamy o jeden
            select max(number) from mwsacts where mwsord = :ref_mwsord into maxnumber;
            if (maxnumber is null) then maxnumber = 0;
            execute procedure gen_ref('MWSACTS') returning_values ref_mwsact;  --ref_mwsact
            insert into mwsacts (ref, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp, mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode,
              closepalloc, wh, wharea, whsec, regtime, timestartdecl, timestopdecl, realtimedecl, flags, descript, priority, recdoc, plus, whareap, whareal, wharealogp,
              wharealogl, number, disttogo, mixedpallgroup, palgroup, mixtakepal, mixleavepal, mixmwsconstlocl, mixmwspallocl, refilltry, x_partia, x_serial_no, x_slownik,
              lot)
            values (:ref_mwsact, :ref_mwsord, 0, 0, :good, :vers, :quantity, :ref_mwsconstlocp, :mwspalloc, :mwsconstlocl, :mwspalloc, null, null, 'O', 0,
              0, :wh, :whareap, null, current_timestamp(0), :timestartdcl, :timestopdcl, :realtime, null, null, 1, 0, 1, null, null, null,
              null, :maxnumber, 0, null, null, :mixtakepal, :mixleavepal, :mixmwsconstlocl, :mixmwspallocl, :mixrefill, :x_partia, :x_serial_no, :x_slownik,
              :lot);
            maxnumber = maxnumber + 1;
          end
          mwsconstlocl = ref_mwsconstlocp; -- zapamietanie poprzedniej lokacji
          check_x_flow_order = check_x_flow_order + 1; -- przejscie do nastepnej pozycji - kontrola na poczatku
-- auto commit mwsacts
          update mwsacts ma set ma.status = 1
            where ma.mwsord = :ref_mwsord
              and ma.status = 0;
          update mwsacts ma set ma.status = 2, ma.quantityc = ma.quantity
            where ma.mwsord = :ref_mwsord
              and ma.status = 1;
        end
        else begin
          msg = msg || 'Palety nie sa w prawidlowej sekwencji lub pozycje sa puste.'; -- blad lokacji np. 1, 2, 3, 5
          status = 0;
          leave LoopA;
        end
      end
    end
  end
  else begin
    msg = msg || 'Regal nie przystosowany do automatycznych przesuniec.'; -- x_row_type <> 1(przeplywowy) i x_flow_order <> 1(ostatnia pozycja)
    status = 0;
  end
-- auto commit mwsord
  if (exists(select first 1 1 from mwsacts ma where ma.mwsord = :ref_mwsord)) then begin
    update mwsords mo set mo.status = 1, mo.timestopdecl = :timestopdcl, mo.operator = null, mo.mwsaccessory = :mwsaccessory
      where mo.ref = :ref_mwsord
        and mo.status = 0;
    update mwsords mo set mo.status = 5, mo.operator = :aktuoperator
      where mo.ref = :ref_mwsord
        and mo.status = 1;
  end
  else begin
-- kasowanie pustego mwsord
    delete from mwsords where ref = :ref_mwsord;
  end
end^
SET TERM ; ^
