--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MAG_CHECK_CENMINMAX(
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           )
  returns (
      CENAMIN numeric(14,2),
      CENAMAX numeric(14,2))
   as
begin
  if(:magazyn  is null) then magazyn = '';
  if(magazyn <> '') then
    select MIN(cena), max(cena) from STANYCEN where ilosc > 0 and KTM = :KTM and WERSJA = :wersja and  MAGAZYN = :magazyn
     into :cenamin, :cenamax;
  else
    select MIN(cena), max(cena) from STANYCEN where ilosc > 0 and KTM = :KTM and WERSJA = :wersja
     into :cenamin, :cenamax;
  if(:cenamin is null) then cenamin = 0;
  if(:cenamax is null) then cenamax = 0;
end^
SET TERM ; ^
