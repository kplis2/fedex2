--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SLO_DANEDOD(
      SLODEF integer,
      SLOPOZ integer)
  returns (
      SPRZEDAWCA integer)
   as
DECLARE VARIABLE TYP VARCHAR(10);
begin
  sprzedawca = null;
  select TYP from SLODEF where REF = :SLODEF
    into :typ;

  if (typ = 'KLIENCI') then
  begin
     select SPRZEDAWCA from KLIENCI where REF=:slopoz into
       :sprzedawca;
  end else if (typ = 'DOSTAWCY') then
  begin
     select first(1) SPRZEDAWCA from KLIENCI where DOSTAWCA=:slopoz and sprzedawca is not null into
       :sprzedawca;
  end else if (typ = 'SPRZEDAWCY') then
  begin
    sprzedawca = :slopoz;
  end
  suspend;
end^
SET TERM ; ^
