--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_ZAMIENNIKI(
      PKTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      MAG char(3) CHARACTER SET UTF8                           ,
      TYP varchar(100) CHARACTER SET UTF8                           ,
      PROMOCJA varchar(40) CHARACTER SET UTF8                           ,
      TRYB integer)
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      ZNAZWAT varchar(40) CHARACTER SET UTF8                           ,
      ZSTOPIEN varchar(40) CHARACTER SET UTF8                           ,
      ZILOSC integer)
   as
DECLARE VARIABLE ILOSC INTEGER;
DECLARE VARIABLE REFPROM INTEGER;
DECLARE VARIABLE ILPROMOCJA INTEGER;
DECLARE VARIABLE PWERSJA INTEGER;
DECLARE VARIABLE ZAMILOSC INTEGER;
DECLARE VARIABLE TYPPROM VARCHAR(3);
DECLARE VARIABLE CECHAPROM VARCHAR(30);
DECLARE VARIABLE WARTCECHYPROM VARCHAR(255);
DECLARE VARIABLE KTMMASKA VARCHAR(40);
DECLARE VARIABLE ODKTM VARCHAR(40);
DECLARE VARIABLE DOKTM VARCHAR(40);
DECLARE VARIABLE CECHATABELA VARCHAR(30);
DECLARE VARIABLE CECHAPOLE VARCHAR(30);
DECLARE VARIABLE CECHARODZAJ INTEGER;
begin
  refprom = null;
  if(:promocja is not null and :promocja<>'') then refprom = cast(:PROMOCJA as integer);
  if (:wersja is null) then wersja=0;
  execute procedure MAG_CHECK_ILOSC (0,:MAG,:PKTM,:WERSJA,0,0) returning_values :ilosc;
  if (:ilosc is null or (:ilosc < 0) ) then ilosc=0;
  if (((:ilosc>0 and :TRYB = 2) or (:tryb = 1)) and :PROMOCJA is not null ) then begin
  /* jezeli stan na magazynie jest > 0 to proponujemny
  tylko zamienniki promocyjne*/
    select PROMOCJE.TYP, PROMOCJE.CECHA, PROMOCJE.WARTCECHY, PROMOCJE.KTMMASKA, PROMOCJE.ODKTM,
    PROMOCJE.DOKTM from PROMOCJE
    where PROMOCJE.ref = :refprom and cast(PROMOCJE.dataod as DATE) <= current_date
    and cast(PROMOCJE.datado as DATE) >= current_date
    into :typprom, :cechaprom, :wartcechyprom, :ktmmaska, :odktm,
    :doktm;
    if (:typprom is null) then typprom = '';
    if(:typprom = 'T') then begin
      for select promoct.wersjaref from PROMOCT where PROMOCJA=:refprom into :pwersja
      do begin
        ktm = null;
        znazwat = null;
        zstopien = null;
        select stanyil.ktm, stanyil.nazwat, TOWAKCES.AKCESDEGREE from STANYIL
          left join TOWAKCES on (TOWAKCES.AKTM=STANYIL.KTM)
          where STANYIL.ILOSC>0 and TOWAKCES.TYP=:typ and TOWAKCES.KTM=:pktm
          and STANYIL.MAGAZYN=:mag and STANYIL.wersjaref = :pwersja into :ktm, :znazwat, :zstopien;
        execute procedure MAG_CHECK_ILOSC (0,:MAG,:ktm,0,0,0) returning_values :zamilosc;
        if (zamilosc is null) then zamilosc=0;
        zilosc=:zamilosc;
        if (:zamilosc > 0) then suspend;
      end
    end
    else if(:typprom = 'G') then begin
      select DEFCECHY.rodzaj, DEFCECHY.tabela, DEFCECHY.pole from DEFCECHY where DEFCECHY.symbol=:cechaprom into :cecharodzaj, :cechatabela, :cechapole;
      for select ATRYBUTY.wersjaref from ATRYBUTY where CECHA=:cechaprom and WARTOSC=:wartcechyprom
        into :pwersja
      do begin
        ktm = null;
        znazwat = null;
        zstopien = null;
        select stanyil.ktm, stanyil.nazwat, TOWAKCES.AKCESDEGREE from STANYIL
          left join TOWAKCES on (TOWAKCES.AKTM=STANYIL.KTM)
          where STANYIL.ILOSC>0 and TOWAKCES.TYP=:typ and TOWAKCES.KTM=:pktm
          and STANYIL.MAGAZYN=:mag and STANYIL.wersjaref = :pwersja into :ktm, :znazwat, :zstopien;
        execute procedure MAG_CHECK_ILOSC (0,:MAG,:ktm,0,0,0) returning_values :zamilosc;
        if (zamilosc is null) then zamilosc=0;
        zilosc=:zamilosc;
        if (:zamilosc > 0) then suspend;
      end
    end else if(:typprom = 'Z') then begin
      if(:odktm is not null and :odktm<>'' and :doktm is not null and :doktm<>'' or (:ktmmaska is not null and :ktmmaska<>'') )
      then begin
        for select wersje.ref from wersje where wersje.ktm like :ktmmaska
          or (wersje.ktm >= :odktm and wersje.ktm <= :doktm) into :pwersja
        do begin
          ktm = null;
          znazwat = null;
          zstopien = null;
          select stanyil.ktm, stanyil.nazwat, TOWAKCES.AKCESDEGREE from STANYIL
            left join TOWAKCES on (TOWAKCES.AKTM=STANYIL.KTM)
            where STANYIL.ILOSC>0 and TOWAKCES.TYP=:typ and TOWAKCES.KTM=:pktm
            and STANYIL.MAGAZYN=:mag and STANYIL.wersjaref = :pwersja into :ktm, :znazwat, :zstopien;
          execute procedure MAG_CHECK_ILOSC (0,:MAG,:ktm,0,0,0) returning_values :zamilosc;
          if (zamilosc is null) then zamilosc=0;
          zilosc=:zamilosc;
          if (:zamilosc > 0) then suspend;
          zamilosc=0;
        end
      end
    end
  end
  /* jezeli ilosc podstawowego towaru na magazynei jest zerowa to proponujemy wszystkie
  zamienniki*/
  else if ((:ilosc=0 and :TRYB = 2) or (:tryb = 0 or (:tryb = 3) )  ) then begin
    for select stanyil.ktm, stanyil.nazwat, TOWAKCES.AKCESDEGREE from STANYIL
    left join TOWAKCES on (TOWAKCES.AKTM=STANYIL.KTM)
    where STANYIL.ILOSC>0 and TOWAKCES.TYP=:typ and TOWAKCES.KTM=:pktm
    and STANYIL.MAGAZYN=:mag into :ktm, :znazwat, :zstopien
    do begin
      execute procedure MAG_CHECK_ILOSC (0,:MAG,:ktm,0,0,0) returning_values :zamilosc;
      if (zamilosc is null) then zamilosc=0;
      zilosc=:zamilosc;
      if (:zamilosc > 0 or (:TRYB = 3) ) then suspend;
    end
  end
end^
SET TERM ; ^
