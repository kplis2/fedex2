--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TEST_BS72532(
      TEXT varchar(255) CHARACTER SET UTF8                           ,
      VAL integer)
  returns (
      OUTTEXT varchar(255) CHARACTER SET UTF8                           ,
      OUTVAL integer,
      OUTDATA date)
   as
begin
  outtext = text;
  outval  = val;
  suspend;
end^
SET TERM ; ^
