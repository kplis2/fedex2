--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_18(
      REFF integer)
   as
declare variable S_ACCOUNT ACCOUNT_ID;
declare variable I_SIDE integer;
declare variable N_DEBIT numeric(14,2);
declare variable N_CREDIT numeric(14,2);
declare variable N_AMOUNT numeric(14,2);
declare variable S_TYP varchar(3);
declare variable N_KAUCJA numeric(14,2);
declare variable S_DESCRIPT varchar(255);
declare variable I_DICTPOS integer;
declare variable I_DICTDEF integer;
declare variable I_ACCOUNT integer;
declare variable S_ODDZIAL ANALYTIC_ID;
declare variable S_ODDZIAL2 ANALYTIC_ID;
declare variable S_DOSTAWCA ANALYTIC_ID;
declare variable S_KLIENT ANALYTIC_ID;
declare variable S_SETTLEMENT varchar(20);
declare variable S_SYMBOL varchar(20);
begin
 /* schemat dekretowania - dokumenty magazynowe */

  n_credit = 0;
  n_debit = 0;
  s_descript = '';

  /* kwota */

  select B.symbol, B.descript, B.dictdef, B.dictpos, D.wartosc, D.typ, O1.symbol, O2.symbol
    from bkdocs B
    join dokumnag D on (D.ref = B.oref and B.ref=:REFF)
    join oddzialy O1 on (D.oddzial = O1.oddzial)
    left join oddzialy O2 on (D.oddzial2 = O2.oddzial)
    into :s_symbol, :s_descript, :i_dictdef, :i_dictpos, :n_amount, :s_typ, :s_oddzial, :s_oddzial2;


  if (:s_typ = 'PZ') then
  begin
    /* strona ksiegowa */
    i_side = 1;

    /* konto */
    s_account = '301';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then n_debit = :n_amount;
    else n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
  /*  koniec dekretu*/

  /*  drugi */
    n_credit = 0;
    n_debit = 0;

    /* strona ksiegowa */
    i_side = 0;

    /* konto */
    s_account = '332-' || :s_oddzial || '-1';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then
      n_debit = :n_amount;
    else
      n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
    /*  koniec dekretu*/
  end
  else


  if (:s_typ = 'PZK') then
  begin
    /* strona ksiegowa */
    i_side = 1;

    /* konto */
    s_account = '301';

    n_amount = - :n_amount;

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then n_debit = :n_amount;
    else n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
  /*  koniec dekretu*/

  /*  drugi */
    n_credit = 0;
    n_debit = 0;

    /* strona ksiegowa */
    i_side = 0;

    /* konto */
    s_account = '332-' || :s_oddzial || '-1';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then
      n_debit = :n_amount;
    else
      n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
    /*  koniec dekretu*/
  end
  else

  if (:s_typ = 'WZ') then
  begin
    /* strona ksiegowa */
    i_side = 0;

    /* konto */
    if (:s_oddzial2 is not null) then
      s_account = '332-' || :s_oddzial2 || '-3';
    else
      s_account = '332-' || :s_oddzial || '-3';


    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then n_debit = :n_amount;
    else n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
  /*  koniec dekretu*/

  /*  drugi */
    n_credit = 0;
    n_debit = 0;

    /* strona ksiegowa */
    i_side = 1;

    /* konto */
    s_account = '332-' || :s_oddzial || '-1';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then
      n_debit = :n_amount;
    else
      n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
    /*  koniec dekretu*/
  end

  if (:s_typ = 'MM-') then
  begin
    /* strona ksiegowa */
    i_side = 0;

    /* konto */
    s_account = '332-' || :s_oddzial2 || '-2';


    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then n_debit = :n_amount;
    else n_credit = :n_amount;

    if (:s_account is null) then
      s_account = '';
    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
  /*  koniec dekretu*/

  /*  drugi */
    n_credit = 0;
    n_debit = 0;

    /* strona ksiegowa */
    i_side = 1;

    /* konto */
    s_account = '332-' || :s_oddzial || '-1';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then
      n_debit = :n_amount;
    else
      n_credit = :n_amount;

    if (:s_account is null) then
      s_account = '';
    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
    /*  koniec dekretu*/
  end

  if (:s_typ = 'MM+') then
  begin
    /* strona ksiegowa */
    i_side = 1;

    /* konto */
    s_account = '332-' || :s_oddzial || '-2';


    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then n_debit = :n_amount;
    else n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
  /*  koniec dekretu*/

  /*  drugi */
    n_credit = 0;
    n_debit = 0;

    /* strona ksiegowa */
    i_side = 0;

    /* konto */
    s_account = '332-' || :s_oddzial || '-1';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then
      n_debit = :n_amount;
    else
      n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
    /*  koniec dekretu*/
  end
  else

  if (:s_typ = 'IN+') then
  begin
    /* strona ksiegowa */
    i_side = 1;

    /* konto */
    s_account = '760-2';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then n_debit = :n_amount;
    else n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
  /*  koniec dekretu*/

  /*  drugi */
    n_credit = 0;
    n_debit = 0;

    /* strona ksiegowa */
    i_side = 0;

    /* konto */
    s_account = '332-' || :s_oddzial || '-1';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then
      n_debit = :n_amount;
    else
      n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
    /*  koniec dekretu*/
  end
  else

  if (:s_typ = 'IN-') then
  begin
    /* strona ksiegowa */
    i_side = 0;

    /* konto */
    s_account = '761-2';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then n_debit = :n_amount;
    else n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
  /*  koniec dekretu*/

  /*  drugi */
    n_credit = 0;
    n_debit = 0;

    /* strona ksiegowa */
    i_side = 1;

    /* konto */
    s_account = '332-' || :s_oddzial || '-1';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then
      n_debit = :n_amount;
    else
      n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
    /*  koniec dekretu*/
  end
  else

  if (:s_typ = 'RW') then
  begin
    n_credit = 0;
    n_debit = 0;

    /* strona ksiegowa */
    i_side = 0;

    /* konto */
    s_account = '411';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then n_debit = :n_amount;
    else n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
  /*  koniec dekretu*/

  /* strona ksiegowa */
    n_credit = 0;
    n_debit = 0;
    i_side = 1;

    /* konto */
    s_account = '490';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then n_debit = :n_amount;
    else n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
  /*  koniec dekretu*/

  /* strona ksiegowa */
    n_credit = 0;
    n_debit = 0;
    i_side = 0;

    /* konto */
    s_account = '512-' || :s_oddzial;

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then n_debit = :n_amount;
    else n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
  /*  koniec dekretu*/

  /*  drugi */
    n_credit = 0;
    n_debit = 0;

    /* strona ksiegowa */
    i_side = 1;

    /* konto */
    s_account = '332-' || :s_oddzial || '-1';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then
      n_debit = :n_amount;
    else
      n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
    /*  koniec dekretu*/
  end

  else
  if (:s_typ = 'ZZ') then
  begin
    s_oddzial2 = null;
    select O.symbol
    from bkdocs B
    join dokumnag D on (D.ref = B.oref and B.ref=:REFF)
    left join nagfak N on (D.faktura = N.ref)
    left join oddzialy O on (N.oddzial = O.oddzial)
    into :s_oddzial2;


    /* strona ksiegowa */
    i_side = 1;

    /* konto */
    if (:s_oddzial2 is not null) then
      s_account = '332-' || :s_oddzial2 || '-3';
    else
      s_account = '332-' || :s_oddzial || '-3';


    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then n_debit = :n_amount;
    else n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
  /*  koniec dekretu*/

  /*  drugi */
    n_credit = 0;
    n_debit = 0;

    /* strona ksiegowa */
    i_side = 0;

    /* konto */
    s_account = '332-' || :s_oddzial || '-1';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then
      n_debit = :n_amount;
    else
      n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
    /*  koniec dekretu*/
  end

  else
  if (:s_typ = 'OP') then
  begin
    select DS.kontofk, D.wartkaucja
    from bkdocs B
    join dokumnag D on (D.ref = B.oref and B.ref=:REFF)
    join dostawcy DS on (D.dostawca=DS.ref)
    into :s_dostawca, :n_kaucja;


    /* strona ksiegowa */
    i_side = 1;

    s_settlement = :s_symbol;
    /* konto */
    s_account = '201-' || :s_dostawca;

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then n_debit = :n_amount;
    else n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript, settlement)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript, :s_settlement);
  /*  koniec dekretu*/

  /*  drugi */
    n_credit = 0;
    n_debit = 0;

    /* strona ksiegowa */
    i_side = 0;

    /* konto */
    s_account = '332-' || :s_oddzial || '-1';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then
      n_debit = :n_amount;
    else
      n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
    /*  koniec dekretu*/
  end

  else
  if (:s_typ = 'OPK') then
  begin
    select DS.kontofk, D.wartkaucja
    from bkdocs B
    join dokumnag D on (D.ref = B.oref and B.ref=:REFF)
    join dostawcy DS on (D.dostawca=DS.ref)
    into :s_dostawca, :n_kaucja;


    /* strona ksiegowa */
    i_side = 1;

    s_settlement = :s_symbol;
    /* konto */
    s_account = '201-' || :s_dostawca;
    n_amount = -:n_amount;

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then n_debit = :n_amount;
    else n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript, settlement)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript, :s_settlement);
  /*  koniec dekretu*/

  /*  drugi */
    n_credit = 0;
    n_debit = 0;

    /* strona ksiegowa */
    i_side = 0;

    /* konto */
    s_account = '332-' || :s_oddzial || '-1';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then
      n_debit = :n_amount;
    else
      n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
    /*  koniec dekretu*/
  end

  else
  if (:s_typ = 'OD') then
  begin
    select DS.kontofk, D.wartkaucja
    from bkdocs B
    join dokumnag D on (D.ref = B.oref and B.ref=:REFF)
    join dostawcy DS on (D.dostawca=DS.ref)
    into :s_dostawca, :n_kaucja;


    /* strona ksiegowa */
    i_side = 0;

    s_settlement = :s_symbol;
    /* konto */
    s_account = '201-' || :s_dostawca;

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then n_debit = :n_amount;
    else n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript, settlement)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript, :s_settlement);
  /*  koniec dekretu*/

  /*  drugi */
    n_credit = 0;
    n_debit = 0;

    /* strona ksiegowa */
    i_side = 1;

    /* konto */
    s_account = '332-' || :s_oddzial || '-1';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then
      n_debit = :n_amount;
    else
      n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
    /*  koniec dekretu*/
  end


  else
  if (:s_typ = 'OW') then
  begin
    select K.kontofk, D.wartkaucja
    from bkdocs B
    join dokumnag D on (D.ref = B.oref and B.ref=:REFF)
    join klienci K on (D.klient=K.ref)
    into :s_klient, :n_kaucja;


    /* strona ksiegowa */
    i_side = 0;

    s_settlement = :s_symbol;
    /* konto */
    s_account = '200-' || :s_klient;

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then n_debit = :n_amount;
    else n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript, settlement)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript, :s_settlement);
  /*  koniec dekretu*/

  /*  drugi */
    n_credit = 0;
    n_debit = 0;

    /* strona ksiegowa */
    i_side = 1;

    /* konto */
    s_account = '332-' || :s_oddzial || '-1';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then
      n_debit = :n_amount;
    else
      n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
    /*  koniec dekretu*/
  end

  else
  if (:s_typ = 'OZ') then
  begin
    select K.kontofk, D.wartkaucja
    from bkdocs B
    join dokumnag D on (D.ref = B.oref and B.ref=:REFF)
    join klienci K on (D.klient=K.ref)
    into :s_klient, :n_kaucja;

    /* strona ksiegowa */
    i_side = 1;

    s_settlement = :s_symbol;
    /* konto */
    s_account = '200-' || :s_klient;

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then n_debit = :n_amount;
    else n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript, settlement)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript, :s_settlement);
  /*  koniec dekretu*/

  /*  drugi */
    n_credit = 0;
    n_debit = 0;

    /* strona ksiegowa */
    i_side = 0;

    /* konto */
    s_account = '332-' || :s_oddzial || '-1';

    /* końcowe operacje - wstawianie dekretu do bazy */
    if (:i_side = 0) then
      n_debit = :n_amount;
    else
      n_credit = :n_amount;

    insert into decrees (bkdoc, account, accref, side, debit, credit, descript)
                values (:REFF, :s_account, :i_account, :i_side, :n_debit, :n_credit, :s_descript);
    /*  koniec dekretu*/
  end
  suspend;
end^
SET TERM ; ^
