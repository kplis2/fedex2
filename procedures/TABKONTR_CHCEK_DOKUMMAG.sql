--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TABKONTR_CHCEK_DOKUMMAG(
      DOKUMENT integer)
  returns (
      KTMMIN varchar(300) CHARACTER SET UTF8                           ,
      KTMMAX varchar(300) CHARACTER SET UTF8                           )
   as
declare variable typ integer;
declare variable magazyn varchar(3);
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable ilosc numeric(14,4);
declare variable ilreal numeric(14,4);
declare variable ilmin numeric(14,4);
declare variable ilmax numeric(14,4);
declare variable typdok varchar(20);
declare variable wydania integer;
begin
  ktmmin = '';
  ktmmax = '';
  select MAGAZYN,TYP from DOKUMNAG where REF=:DOKUMENT into :magazyn,:typdok;
  select WYDANIA from DEFDOKUM where SYMBOL=:typdok into :wydania;
  for select STANYIL.KTM, STANYIL.WERSJA, STANYIL.ILOSC, STANYIL.stanmin, stanyil.stanmax from DOKUMPOZ join STANYIL on (STANYIL.MAGAZYN = :MAGAZYN and STANYIL.KTM = DOKUMPOZ.KTM and STANYIl.wersja = DOKUMPOZ.wersja)
     where DOKUMPOZ.dokument = :dokument
  into :ktm, :wersja, :ilosc, :ilmin, :ilmax
  do begin
    if(:wydania=1 and :ilmin is not null and :ilmin < :ilosc and coalesce(char_length(:ktmmin),0)<260) then -- [DG] XXX ZG119346
      ktmmin = :ktm||','||:ktmmin;
    if(:wydania=0 and :ilmax is not null and :ilmax > :ilosc and coalesce(char_length(:ktmmax),0)<260) then -- [DG] XXX ZG119346
      ktmmax = :ktm||','||:ktmmax;
  end
end^
SET TERM ; ^
