--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWSACTS_STATUSCHANGE(
      MWSORDTYPE integer,
      MWSORD integer,
      MWSACT integer,
      OLDSTATUS smallint)
   as
declare variable STOCKLOT integer;
declare variable TOREAL numeric(15,4);
declare variable AVAIBLE numeric(15,4);
declare variable TOINSERT numeric(15,4);
declare variable TOUPDATE numeric(15,4);
declare variable DETSQ numeric(15,4);
declare variable MWSACTDET integer;
declare variable MAXNUMBER integer;
declare variable NEWMWSACT integer;
declare variable OPERSETTLMODE integer;
declare variable STOCKMWSORDSETTL integer;
declare variable DOCID integer;
declare variable DOCPOSID integer;
declare variable DOCTYPE char(1);
declare variable NEWSTATUS smallint;
declare variable MWSCONSTLOCP integer;
declare variable MWSPALLOCP integer;
declare variable MWSCONSTLOCL integer;
declare variable MWSPALLOCL integer;
declare variable GOOD varchar(40);
declare variable VERS integer;
declare variable QUANTITY numeric(14,4);
declare variable QUANTITYC numeric(14,4);
declare variable STOCKTAKING smallint;
declare variable PRIORITY smallint;
declare variable LOT integer;
declare variable WH varchar(3);
declare variable WHSEC integer;
declare variable WHAREA integer;
declare variable RECDOC smallint;
declare variable CORTOMWSACT integer;
declare variable PLUS smallint;
declare variable PACKSYMBOL varchar(20);
declare variable MWSCONSTLOCSTOCK smallint;
declare variable STOCKTAKINGLOT integer;
declare variable REFSC integer;
declare variable X_PARTIA DATE_ID; /* XXX KBI */
declare variable X_SERIAL_NO INTEGER_ID; /* XXX KBI */
declare variable X_SLOWNIK INTEGER_ID; /* XXX KBI */
begin

--exception universal MWSORDTYPE||'  '|| MWSORD ||'  '|| MWSACT  ||'  '|| OLDSTATUS;
  select docid,docposid, doctype, status, mwsconstlocp,mwspallocp, mwsconstlocl, mwspallocl,
      good, vers, quantity, quantityc,stocktaking, priority,lot,wh, whsec, wharea,
      recdoc, cortomwsact, plus, packsymbol,
      x_partia, x_serial_no, x_slownik  -- XXX KBI
    from mwsacts  x
    where ref = :mwsact
    into docid,docposid, doctype, newstatus, mwsconstlocp,mwspallocp, mwsconstlocl, mwspallocl,
      good, vers, quantity, quantityc,stocktaking, priority,lot,wh, whsec, wharea,
      recdoc, cortomwsact, plus, packsymbol,
      :x_partia, :x_serial_no, :x_slownik; -- XXX KBI
  if (stocktaking = 0) then
  begin
    if (recdoc = 1 and mwspallocl is null) then exception MWSACTDETS_CANT_BE_DONE 'Pozycja przychodowa bez lokacji koncowej'||mwsordtype;--||'  '|| MWSORD ||'  '|| MWSACT  ||'  '|| OLDSTATUS;
    if (recdoc = 1 and lot is null) then  exception MWSACTDETS_CANT_BE_DONE 'Pozycja przychodowa bez dostawy '||mwsordtype;
    if (recdoc = 0 and mwspallocp is null) then exception MWSACTDETS_CANT_BE_DONE 'Pozycja rozchodowa bez lokacji poczatkowej'||mwsordtype;
  end
  if (newstatus > 6) then
    exception MWSACT_STATUS_NOTAVAILBLE;
-- TO DO TRZEBA ZROBIC OBSLUGE KOREKT WYDANIA I PRZYJECIA ZEBY ROZPISKI TWIRZYLY SIE NA PODSTAWIE KORYGOWANYCH
  maxnumber = 0;
  -- zlecenia inwentaryzacji nie maja rozpisek
  -- okreslenie max number z rozpisek, zeby nie bylo orderowania i sie dopisaly na koncu
  if (doctype = 'M' and mwsact > 0 and stocktaking = 0) then
  begin
    select max(number) from mwsactdets where mwsord = :mwsord and mwsact = :mwsact
      into maxnumber;
  end
  if (maxnumber is null) then maxnumber = 0;
  -- dla zleceń inwentaryzacji okreslenie jak rozliczac pozycje
  if (stocktaking = 1) then
  begin
    select opersettlmode, stockmwsordsettl
      from mwsordtypes
      where ref = :mwsordtype
      into :opersettlmode, stockmwsordsettl;
      if (opersettlmode is null) then opersettlmode = 0;
      if (stockmwsordsettl is null) then stockmwsordsettl = 0;
  end
  else
  begin
    opersettlmode = 0;
    stockmwsordsettl = 0;
  end
  -- blokada zmiany statusu z 0 na 5, bo konieczne jest wygenerowanie rozpisek na poczatek
  if (newstatus = 5 and oldstatus = 0) then
    exception STATUSCHANGE_NOTALLOWED;
  -- Najpierw zlecenia magazynowe (inwentaryzacyjne nizej gdyz sa inaczej troche obsluzone)
  if ((newstatus = 1 or newstatus = 2) and stocktaking = 0) then
  begin
    if (recdoc = 1) then
    begin
      ---------------------------- PRZYCHÓD -------------------------------------------------
      -- trzeba wygenerowac "rozpiske" do operacji zlecenia magazynowego
      if (oldstatus = 0) then
      begin
        toreal = quantity;
        maxnumber = maxnumber + 1;
        insert into MWSACTDETS (mwsact, number, quantity, lot, vers, wh, whsec,
            good, rec, mwsord, status, mwsconstlocp, mwspallocp, mwsconstlocl, mwspallocl,
            doctype, docid, docposid, stocktaking, cortomwsact, plus,
            x_partia, x_serial_no, x_slownik) -- XXX KBI
          values(:mwsact, :maxnumber, :quantity, :lot, :vers, :wh, :whsec,
            :good, 1, :mwsord, :newstatus,:mwsconstlocp, :mwspallocp, :mwsconstlocl, :mwspallocl,
            :doctype, :docid, :docposid, :stocktaking, :cortomwsact, :plus,
            :x_partia, :x_serial_no, :x_slownik);  -- XXX KBI
      end
      else
      begin
        -- trzeba poodwracac stany na lokacjach gdy odtwierdzamy operacje to ilosc potwierdzona ustawiamy na 0
        if (oldstatus = 5) then
        begin
          update mwsactdets set status = :newstatus, quantityc = 0,
               mwsconstlocp = :mwsconstlocp, mwspallocp = :mwspallocp,
               mwsconstlocl = :mwsconstlocl, mwspallocl = :mwspallocl
             where mwsact = :mwsact;
          update mwsacts set quantityc = 0 where ref = :mwsact;
        end
        else
          update mwsactdets set status = :newstatus,
               mwsconstlocp = :mwsconstlocp, mwspallocp = :mwspallocp,
               mwsconstlocl = :mwsconstlocl, mwspallocl = :mwspallocl
             where mwsact = :mwsact;
      end
    end
    else if (recdoc = 0 and mwsconstlocp is not null) then
    begin
      ---------------------------- ROZCHÓD -------------------------------------------------
      -- trzeba wygenerowac "rozpiske" do operacji zlecenia magazynowego
      if (oldstatus = 0) then
      begin
        toreal = quantity;
        -- kontrola czy sie da rozpisac rozchod
        select sum(ms.quantity - ms.blocked)
          from MWSSTOCK ms
          where ms.good = :good and ms.vers = :vers and ms.mwsconstloc = :mwsconstlocp
            and (ms.mwspalloc = :mwspallocp or :mwspallocp is null)
            and (ms.lot = :lot or :lot is null or :lot = 0)
            and ms.quantity - ms.blocked > 0
          into avaible;

        if (avaible < quantity or (avaible is null and quantity > 0)) then
        begin
          select stocktaking from mwsconstlocs where ref = :mwsconstlocp into mwsconstlocstock;
          if (mwsconstlocstock is null) then mwsconstlocstock = 0;
          if (mwsconstlocstock = 0) then
            exception MWSSTOCK_TO_MUCH_BLOCKED 'Brak stanow na lokacji dla: '||:good||' 1 '||:mwsconstlocp||' '||mwspallocp||' '||quantity||' '||coalesce(:lot,0)||' '||coalesce(:avaible,0)||' mwsact '||:mwsact||' mwsord '||:mwsord||' ordtype '||:mwsordtype;
        end
        for
          select ms.lot, ms.quantity - ms.blocked
            from MWSSTOCK ms
            where ms.good = :good and vers = :vers and ms.mwsconstloc = :mwsconstlocp
              and (ms.mwspalloc = :mwspallocp or :mwspallocp is null)
              and (ms.lot = :lot or :lot is null or :lot = 0)
             /* and (ms.x_partia = :x_partia or :x_partia is null)                                -- XXX KBI
              and (ms.x_serial_no = :x_serial_no or :x_serial_no is null or :x_serial_no = 0)   -- XXX KBI
              and (ms.x_slownik = :x_slownik or :x_slownik is null or :x_slownik = 0)           -- XXX KBI */
              and ms.quantity - ms.blocked > 0
            order by ms.REF
            into stocklot, avaible
        do begin
          if(toreal > 0) then begin
            if(avaible is null) then avaible = 0;
            if(avaible > toreal) then begin
               toinsert = toreal;
            end else begin
              toinsert = avaible;  -- zdjecie calej ilosci potrzebnej
            end
            if(:toinsert > 0) then
            begin
              maxnumber = maxnumber + 1;
              insert into MWSACTDETS (mwsact, number, quantity, lot, vers, wh, whsec,
                  good, rec, mwsord, status, mwsconstlocp, mwspallocp, mwsconstlocl, mwspallocl,
                  doctype, docid, docposid, stocktaking, cortomwsact, plus,
                  x_partia, x_serial_no, x_slownik)     -- XXX KBI
                values(:mwsact, :maxnumber, :toinsert, :stocklot, :vers, :wh, :whsec,
                  :good, 1, :mwsord, :newstatus,:mwsconstlocp, :mwspallocp, :mwsconstlocl, :mwspallocl,
                  :doctype, :docid, :docposid, :stocktaking, :cortomwsact, :plus,
                  :x_partia, :x_serial_no, :x_slownik); -- XXX KBI
            end
            toreal = toreal - toinsert;
            if (toreal = 0) then
              break;
          end
        end
        -- obsuga rozliczenia inwentaryzacyjnej lokacji - czyli zejscie na stany ujemne !!!
        if (toreal > 0 and mwsconstlocstock = 1) then
        begin
          if (lot is null) then
          begin
            execute procedure GEN_REF('DOSTAWY')
              returning_values stocktakinglot;
            insert into DOSTAWY(ref, DOSTAWCA, DATA, STATUS, MAGAZYN, DATAWAZN, REFDOKUMNAG)
              values (:stocktakinglot, null, current_date, 'Z', :wh, null, null);
          end else
            stocktakinglot = lot;
          maxnumber = maxnumber + 1;
          insert into MWSACTDETS (mwsact, number, quantity, lot, vers, wh, whsec,
              good, rec, mwsord, status, mwsconstlocp, mwspallocp, mwsconstlocl, mwspallocl,
              doctype, docid, docposid, stocktaking, cortomwsact, plus,
              x_partia, x_serial_no, x_slownik)   -- XXX KBI
            values(:mwsact, :maxnumber, :toreal, :stocktakinglot, :vers, :wh, :whsec,
                :good, 1, :mwsord, :newstatus,:mwsconstlocp, :mwspallocp, :mwsconstlocl, :mwspallocl,
                :doctype, :docid, :docposid, :stocktaking, :cortomwsact, :plus,
                :x_partia, :x_serial_no, :x_slownik);  -- XXX KBI
          toreal = 0;
        end else if (toreal > 0 and mwsconstlocstock = 0) then
          exception MWSSTOCK_TO_MUCH_BLOCKED 'Brak stanow na lokacji dla: '||:good||' '||:mwsconstlocp||' '||mwspallocp||' '||quantity;
      end
      else 
      begin
        -- trzeba poodwracac stany na lokacjach - jak odtwierdzamu to zerujemy ilosc potwierdzona
        if (oldstatus = 5) then
        begin
          update mwsactdets set status = :newstatus, quantityc = 0,
               mwsconstlocp = :mwsconstlocp, mwspallocp = :mwspallocp,
               mwsconstlocl = :mwsconstlocl, mwspallocl = :mwspallocl
              where mwsact = :mwsact;
          update mwsacts set quantityc = 0 where ref = :mwsact;
        end
        else
          update mwsactdets set status = :newstatus,
               mwsconstlocp = :mwsconstlocp, mwspallocp = :mwspallocp,
               mwsconstlocl = :mwsconstlocl, mwspallocl = :mwspallocl
             where mwsact = :mwsact;
      end
    end
    select sum(quantity) from mwsactdets where mwsact = :mwsact into detsq;
    if (quantity <> detsq) then
      exception MWSACT_NOT_DISPOSED;
  end
  else if (stocktaking = 0 and newstatus = 5 and (oldstatus = 1 or oldstatus = 2)) then
  begin
    -- obsluga rozbijania rozpisek na operacjach
    if ((recdoc = 1 and mwspallocl is not null) or (recdoc = 0 and mwspallocp is not null)) then
    begin
      if (quantity <> quantityc and quantityc > 0) then
      begin
        -- rozbicie operacji magazynowej gdy potwierdzono za malo albo za duzo
        toreal = quantityc;
        select max(number) from mwsactdets where mwsact = :mwsact into maxnumber;
        if (maxnumber is null) then maxnumber = 0;
        for
          select ref, quantity
            from mwsactdets
            where mwsact = :mwsact
            into mwsactdet, toupdate
        do begin
          if (toupdate <= toreal and toreal > 0) then
          begin
            -- gdy jest wiecej jak na rozpisce
             update mwsactdets set status = :newstatus, quantityc = :toupdate,
                 mwsconstlocp = :mwsconstlocp, mwspallocp = :mwspallocp,
                 mwsconstlocl = :mwsconstlocl, mwspallocl = :mwspallocl
               where ref = :mwsactdet;
             toreal = toreal - toupdate;
          end else if (toupdate > toreal and toreal > 0) then
          begin
            -- gdy jest mniej jak na rozpisce
             update mwsactdets set status = :newstatus, quantity = :toreal, quantityc = :toreal,
                 mwsconstlocp = :mwsconstlocp, mwspallocp = :mwspallocp,
                 mwsconstlocl = :mwsconstlocl, mwspallocl = :mwspallocl
               where ref = :mwsactdet;
             toinsert = toupdate - toreal;
             toreal = 0;
          end else if (toreal = 0) then
            toinsert = toinsert + toupdate;
        end
        if (toreal > 0) then
        begin
          maxnumber = maxnumber + 1;
          -- trzeba dostawic rozpiske na to co potwierdzamy wiecej do tej samej operacji
          insert into MWSACTDETS (mwsact, number, quantity, quantityc, lot, vers, wh, whsec,
              good, rec, mwsord, status, mwsconstlocp, mwspallocp, mwsconstlocl, mwspallocl,
              doctype, docid, docposid, stocktaking, cortomwsact, plus,
              x_partia, x_serial_no, x_slownik)     -- XXX KBI
            values(:mwsact, :maxnumber, :toreal, :toreal, :lot, :vers, :wh, :whsec,
              :good, 1, :mwsord, :newstatus,:mwsconstlocp, :mwspallocp, :mwsconstlocl, :mwspallocl,
              :doctype, :docid, :docposid, :stocktaking, :cortomwsact, :plus,
              :x_partia, :x_serial_no, :x_slownik);  -- XXX KBI
        end
        -- trzeba zaktualizowac pozycje i ustawic na ilosc potwierdzona gdy jest klonowanie
        if (quantity > quantityc) then
          update mwsacts set quantity = :quantityc where ref = :mwsact and status = 5;
        if (toinsert > 0) then
        begin
          -- trzeba dostawic nowa pozycje - klon na ilosc brakujaca
          execute procedure gen_ref('MWSACTS') returning_values newmwsact;
          insert into mwsacts (ref, frommwsact, clonefrommwsact, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
              mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
              regtime, timestartdecl, timestopdecl, flags, descript, priority, lot, recdoc,
              cortomwsact, plus, number, accepttime,
              x_partia, x_serial_no, x_slownik)  --- XXX KBI
            select :newmwsact, frommwsact, :mwsact, mwsord, :oldstatus, stocktaking, good, vers, :toinsert, mwsconstlocp, mwspallocp,
                mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
                regtime, timestartdecl, timestopdecl, flags, descript, priority, lot, recdoc,
                cortomwsact, plus, number + 1, accepttime,
                x_partia, x_serial_no, x_slownik
              from mwsacts where ref = :mwsact;
          -- teraz trzeba przepisac pozostale rozpiski pod nowa pozycje
          update mwsactdets set mwsact = :newmwsact where mwsact = :mwsact and quantityc = 0
            and status = :oldstatus;
        end
      end
      else
        update mwsactdets set status = :newstatus, quantityc = quantity,
            mwsconstlocp = :mwsconstlocp, mwspallocp = :mwspallocp,
            mwsconstlocl = :mwsconstlocl, mwspallocl = :mwspallocl
          where mwsact = :mwsact;
    end
    else
    begin
      -- trzeba poodwracac stany na lokacjach
      -- nie zerujemy tu ilosci potwierdzonej bo tu nie wejdzie (to jest potwierdzanie)
      update mwsactdets set status = :newstatus,
          mwsconstlocp = :mwsconstlocp, mwspallocp = :mwspallocp,
          mwsconstlocl = :mwsconstlocl, mwspallocl = :mwspallocl
        where mwsact = :mwsact;
    end
  end
  else if (stocktaking = 0 and ((recdoc = 0 and mwspallocp is not null) or (recdoc = 1 and mwspallocl is not null))) then
  begin
    -- nalezy posprzatac stany na lokacjach i jak odtwierdzamy to rowniez ilosc potwierdzona
    if (oldstatus = 5) then
    begin
      update mwsactdets set status = :newstatus, quantityc = 0,
          mwsconstlocp = :mwsconstlocp, mwspallocp = :mwspallocp,
          mwsconstlocl = :mwsconstlocl, mwspallocl = :mwspallocl
        where mwsact = :mwsact;
      update mwsacts set quantityc = 0 where ref = :mwsact;
    end
    else
    begin
      update mwsactdets set status = :newstatus,
          mwsconstlocp = :mwsconstlocp, mwspallocp = :mwspallocp,
          mwsconstlocl = :mwsconstlocl, mwspallocl = :mwspallocl
        where mwsact = :mwsact;
    end
    if (newstatus = 0) then
      delete from MWSACTDETS where mwsact = :mwsact;
  end
  -- TU ZACZYNA SIE OBSLUGA ZLECEN INWENTARYZACJI
  -- najpierw obsluga potwierdzenia operacji i jej klonowania
  else if (stocktaking = 1 and newstatus = 5 and oldstatus < 5) then
  begin
    if (quantity <> quantityc  and quantityc > 0) then
    begin
      -- trzeba zaktualizowac pozycje i ustawic na ilosc potwierdzona gdy jest klonowanie
      toreal = quantity - quantityc;
      if (quantity > quantityc) then
        update mwsacts set quantity = quantityc where ref = :mwsact and status = 5;
      -- rozbicie operacji magazynowej gdy potwierdzono za malo
      newmwsact = null;
      if (toreal > 0) then
      begin
        -- trzeba dostawic nowa pozycje - klon na ilosc brakujaca
        execute procedure gen_ref('MWSACTS') returning_values newmwsact;
        insert into mwsacts (ref, frommwsact, clonefrommwsact, mwsord, status, stocktaking, good, vers, quantity, mwsconstlocp, mwspallocp,
            mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
            regtime, timestartdecl, timestopdecl, flags, descript, priority, lot, recdoc,
            cortomwsact, plus, number,
            x_partia, x_serial_no, x_slownik) -- XXX KBI
          select :newmwsact, frommwsact,:mwsact, mwsord, :oldstatus, stocktaking, good, vers, :toreal, mwsconstlocp, mwspallocp,
              mwsconstlocl, mwspallocl, docid, docposid, doctype, settlmode, closepalloc, wh, wharea, whsec,
              regtime, timestartdecl, timestopdecl, flags, descript, priority, lot, recdoc,
              cortomwsact, plus, number + 1,
              x_partia, x_serial_no, x_slownik -- XXX KBI
            from mwsacts where ref = :mwsact;
      end
    end
    -- rozliczenie operacji inwentaryzacji - nowe zlecenie
    if (opersettlmode = 2) then
    begin
      if (stockmwsordsettl = 0) then
        exception STOCKMWSORDSETTL_NOT_SET;
      -- generowanie zlecenia rozliczajacego zleceie inwentaryzacji
      execute procedure MWSACT_STOCK_SETTL(:docid, :docid, :docposid, :mwsord, :mwsact, 0, :stockmwsordsettl, :wh);
      if (newmwsact is not null) then
        execute procedure MWSACT_STOCK_SETTL(:docid, :docid, :docposid, :mwsord, :mwsact, 0, :stockmwsordsettl, :wh);
    end
  end
  -- sprawdzenie czy mozna wycofac potwierdzenie operacji inwentaryzacji
end^
SET TERM ; ^
