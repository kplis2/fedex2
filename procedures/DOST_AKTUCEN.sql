--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOST_AKTUCEN(
      FAKTURA integer,
      TRYB char(1) CHARACTER SET UTF8                           ,
      WYZSZE smallint,
      UZUP smallint,
      USTAW smallint,
      RABAT smallint,
      SYMBDOST smallint)
  returns (
      STATUS integer)
   as
declare variable wersjaref integer;
declare variable cenafab numeric(14,2);
declare variable cenazak numeric(14,2);
declare variable cenazakzl numeric(14,2);
declare variable cenakoszt numeric(14,2);
declare variable dostawca integer;
declare variable oldcenafab numeric(14,2);
declare variable oldcenazak numeric(14,2);
declare variable oldcenakoszt numeric(14,2);
declare variable doo integer;
declare variable nagrabat numeric(14,2);
declare variable pozrabat numeric(14,2);
declare variable symdost varchar(40);
declare variable sumkosztzak numeric(14,2);
declare variable summagzak numeric(14,2);
declare variable ilosc numeric(14,4);
declare variable wartosc numeric(14,2);
declare variable vat numeric(14,2);
declare variable ktm varchar(40);
declare variable nowacenasprnet numeric(14,2);
declare variable nowacenasprbru numeric(14,2);
declare variable obliczcenzak integer;
declare variable obliczcenkoszt integer;
declare variable obliczcenfab integer;
declare variable jedn integer;
declare variable oldjedn integer;
declare variable isdostcen integer;
declare variable przelicz1 float;
declare variable przelicz2 float;
declare variable rabatsum smallint;
declare variable aktu smallint;
declare variable walcen varchar(10);
declare variable oddzial varchar(40);
declare variable walpln varchar(3);
declare variable prec smallint;
begin
  if(:tryb = 'M') then begin
    select dostawca, kosztdost, wartosc, oddzial from DOKUMNAG where REF=:faktura into :dostawca, :sumkosztzak, :summagzak, :oddzial;
    if(:dostawca is null) then dostawca = 0;
    nagrabat = 0;
  end else begin
    select dostawca, kosztdost, sumwartnet, oddzial  from NAGFAK where REF=:faktura into :dostawca, :sumkosztzak, summagzak, :oddzial;
    if(:rabat = 1) then select RABAT from NAGFAK where REF=:faktura into :nagrabat;
  end
  select rabatsum from dostawcy where ref = :dostawca into :rabatsum;
  execute procedure GETCONFIG('WALPLN') returning_values :walpln;
  if(:walpln='' or :walpln is null) then walpln = 'PLN';
  if(:nagrabat is null) then nagrabat = 0;
  if(:dostawca is null) then dostawca = 0;
  if(tryb = 'F') then begin--tryb z dokumentu VAT zakupu
    select SUM(WARTNETZL) from POZFAK
    left join TOWARY on (TOWARY.KTM=POZFAK.KTM)
    where POZFAK.DOKUMENT=:FAKTURA and POZFAK.OPK=0 and TOWARY.USLUGA<>1 into :summagzak;
    for select KTM, WERSJAREF, JEDN, ILOSC, WARTNETZL, CENANET, CENANETZL, RABAT, SYMBOL_DOST, OBLICZCENZAK, OBLICZCENKOSZT, OBLICZCENFAB, NOWACENASPRNET, WALCEN, PREC
    from POZFAK where DOKUMENT = :FAKTURA and ((OBLICZCENZAK=1) or (OBLICZCENKOSZT=1) or (OBLICZCENFAB=1))
    into :ktm, :wersjaref, :jedn, :ilosc, :wartosc, :cenazak, :cenazakzl, :pozrabat, :symdost, :obliczcenzak, :obliczcenkoszt, :obliczcenfab, :nowacenasprnet, :walcen, :prec
    do begin
      doo = 1;
      if(:walcen='' or :walcen is null) then walcen = :walpln;
      if(:pozrabat is null) then pozrabat = 0;
      pozrabat = :pozrabat + :nagrabat;
      -- sprawdzamy czy istnieje juz ten dostawca w DOSTCEN aby sprawdzic czy mamy dodawac nowego czy nie
      if (exists(select first 1 1 from DOSTCEN where WERSJAREF = :WERSJAREF and DOSTAWCA = :dostawca)) then isdostcen = 1;
      else isdostcen = 0;
      if(:uzup=0 and :isdostcen=0) then doo = 0;
      -- sprawdzamy pelny klucz czy rekord bedzie dodawany czy aktualizowany
      if (exists(select first 1 1 from DOSTCEN where WERSJAREF = :WERSJAREF and DOSTAWCA = :dostawca and WALUTA = :walcen and ODDZIAL = :oddzial)) then isdostcen = 1;
      else isdostcen = 0;
      if(:nowacenasprnet is null) then nowacenasprnet = 0;
      if(:cenazak is null) then cenazak = 0;
      if(:cenazakzl is null) then cenazakzl = 0;
      if(:ilosc > 0 and :summagzak > 0) then cenakoszt =  :cenazakzl + :sumkosztzak * :wartosc / :summagzak / :ilosc;
      else cenakoszt = :cenazakzl;
      cenafab = :nowacenasprnet;
      if(:doo = 1) then begin--aktualizacja dostcen
        if(:isdostcen=0) then begin--insert into dostcen
          select CENA_ZAKN, CENA_FABN, CENA_KOSZTN from WERSJE where REF = :WERSJAREF
          into :oldcenazak, :oldcenafab, :oldcenakoszt;
          if(:walcen=:walpln) then begin
            if(:wyzsze > 0) then begin
              if((:wyzsze = 1 and :oldcenazak < :cenazakzl)
                or(:wyzsze = 2 and :oldcenafab < :cenafab)
                or(:wyzsze = 3 and :oldcenakoszt < :cenakoszt)
              )then aktu = 1;
              else aktu = 0;
            end else aktu = 1;
          end else begin
            aktu = 1;
          end
          przelicz2 = 1;
          if(:jedn > 0) then select przelicz from TOWJEDN where REF=:jedn into :przelicz2;
          if(:obliczcenzak=0 or (:cenazak=0)) then cenazak = :oldcenazak*:przelicz2;
          if(:obliczcenkoszt=0 or (:cenakoszt=0)) then cenakoszt = :oldcenakoszt*:przelicz2;
          if(:obliczcenfab=0 or (:cenafab=0)) then cenafab = :oldcenafab*:przelicz2;
          if(:dostawca > 0) then
            insert into dostcen(DOSTAWCA, WERSJAREF, WALUTA, ODDZIAL, JEDN, CENANET, CENAWAL, CENA_FAB, cena_koszt, GL, RABAT, SYMBOL_DOST, CENAZAKGL, PREC)
              values(:dostawca, :wersjaref, :walcen, :oddzial, :jedn, :cenazakzl, :cenazak, :cenafab, :cenakoszt, :ustaw, :pozrabat, :symdost, :aktu, :prec);
        end else begin--update dostcen
          select CENANET, CENA_FAB, CENA_KOSZT, JEDN
            from DOSTCEN
            where WERSJAREF = :WERSJAREF and DOSTAWCA = :dostawca and WALUTA = :walcen and ODDZIAL = :oddzial
          into :oldcenazak, :oldcenafab, :oldcenakoszt, :oldjedn;
          przelicz1 = 1;
          przelicz2 = 1;
          if(:oldjedn<>:jedn) then begin
            if(:oldjedn > 0) then select przelicz from TOWJEDN where REF=:oldjedn into :przelicz1;
            if(:jedn > 0) then select przelicz from TOWJEDN where REF=:jedn into :przelicz2;
          end
          if(:obliczcenzak=0 or (:cenazak=0)) then cenazak = :oldcenazak * :przelicz2 / :przelicz1;
          if(:obliczcenzak=0 or (:cenazakzl=0)) then cenazakzl = :oldcenazak * :przelicz2 / :przelicz1;
          if(:obliczcenkoszt=0 or (:cenakoszt=0)) then cenakoszt = :oldcenakoszt * :przelicz2 / :przelicz1;
          if(:obliczcenfab=0 or (:cenafab=0)) then cenafab = :oldcenafab * :przelicz2 / :przelicz1;
          select CENA_ZAKN, CENA_FABN, CENA_KOSZTN from WERSJE where REF = :WERSJAREF
          into :oldcenazak, :oldcenafab, :oldcenakoszt;
          if(:walcen=:walpln) then begin
            if(:wyzsze > 0) then begin
              if((:wyzsze = 1 and :oldcenazak < :cenazakzl)
                or(:wyzsze = 2 and :oldcenafab < :cenafab)
                or(:wyzsze = 3 and :oldcenakoszt < :cenakoszt)
              )then aktu = 1;
              else aktu = 0;
            end else aktu = 1;
          end else begin
            aktu = 1;
          end
          update DOSTCEN set JEDN = :jedn, CENANET = :cenazakzl, CENAWAL = :cenazak, CENA_FAB = :cenafab, CENA_KOSZT = :cenakoszt,
             GL = case when :ustaw = 1 and GL = 0 then :ustaw else GL end, CENAZAKGL = :aktu, PREC = :prec
             where DOSTAWCA = :dostawca and WERSJAREF = :wersjaref and WALUTA=:walcen and ODDZIAL=:oddzial;
          if(:rabat = 1 and (:rabatsum is null or :rabatsum = 0)) then
            update DOSTCEN set RABAT = :pozrabat where DOSTAWCA = :dostawca and WERSJAREF = :wersjaref and WALUTA=:walcen and ODDZIAL=:oddzial;
          if(:symbdost = 1 and :symdost is not null and :symdost<>'') then
            update DOSTCEN set SYMBOL_DOST = :symdost where DOSTAWCA = :dostawca and WERSJAREF = :wersjaref and WALUTA=:walcen and ODDZIAL=:oddzial;
        end--update dostcen
      end else begin --end aktualizacjadostcen / begin update wersje
        select CENA_ZAKN, CENA_FABN, CENA_KOSZTN from WERSJE where REF = :WERSJAREF
        into :oldcenazak, :oldcenafab, :oldcenakoszt;
        if(:walcen=:walpln) then begin
          if(:wyzsze > 0) then begin
            if((:wyzsze = 1 and :oldcenazak < :cenazak)
              or(:wyzsze = 2 and :oldcenafab < :cenafab)
              or(:wyzsze = 3 and :oldcenakoszt < :cenakoszt)
            )then aktu = 1;
            else aktu = 0;
          end else aktu = 1;
        end else begin
          aktu = 1;
        end
        if (aktu = 1) then begin--aktualizacja kartoteji towarowej
          przelicz2 = 1;
          if(:jedn > 0) then select przelicz from TOWJEDN where REF=:jedn into :przelicz2;
          if(:obliczcenzak=0 or (:cenazak=0)) then cenazak = :oldcenazak * :przelicz2;
          if(:obliczcenzak=0 or (:cenazakzl=0)) then cenazakzl = :oldcenazak * :przelicz2;
          if(:obliczcenkoszt=0 or (:cenakoszt=0)) then cenakoszt = :oldcenakoszt * :przelicz2;
          if(:obliczcenfab=0 or (:cenafab=0)) then cenafab = :oldcenafab * :przelicz2;
          select VAT.STAWKA from TOWARY join VAT on (TOWARY.vat=VAT.grupa) where TOWARY.KTM=:ktm into :vat;
          if(:vat is null) then vat = 0;
          vat = 1+:vat/100;
          update WERSJE set CENA_ZAKN = :cenazakzl / :przelicz2,
                            CENA_ZAKB = :cenazakzl * :vat / :przelicz2,
                            CENA_KOSZTN = :cenakoszt / :przelicz2,
                            CENA_KOSZTB = :cenakoszt * :vat / :przelicz2,
                            CENA_FABN = :cenafab / :przelicz2,
                            CENA_FABB = :cenafab * :vat / :przelicz2
          where REF = :wersjaref;
        end --aktualizacja kartoteji towarowej
      end --koniec else do nanoszenia zmian w dostcen
    end --for select w trybie 'F'
  end else begin--tryb z dokumentu magazynowego
    select SUM(WARTOSC) from DOKUMPOZ left join TOWARY on (TOWARY.KTM=DOKUMPOZ.KTM)
    where DOKUMPOZ.DOKUMENT=:FAKTURA and DOKUMPOZ.OPK=0 and TOWARY.USLUGA<>1 into :summagzak;
    for select KTM, WERSJAREF, ILOSC, WARTOSC, CENASR, NOWACENASPRNET, NOWACENASPRBRU, OBLICZCENZAK, OBLICZCENKOSZT, OBLICZCENFAB, WALCEN, PREC
    from DOKUMPOZ where DOKUMENT = :FAKTURA and ((OBLICZCENZAK=1) or (OBLICZCENKOSZT=1) or(OBLICZCENFAB=1))
    into :ktm, :wersjaref, :ilosc, :wartosc, :cenazak, :nowacenasprnet, :nowacenasprbru, :obliczcenzak, :obliczcenkoszt, :obliczcenfab, :walcen, :prec
    do begin
      select ref from TOWJEDN where KTM = :ktm and GLOWNA = 1 into :jedn;
      if(:walcen='' or :walcen is null) then walcen = :walpln;
      cenafab = 0;
      doo = 1;
      pozrabat = 0;
      isdostcen = 0;
      if(:dostawca > 0) then begin
        -- sprawdzamy czy istnieje juz ten dostawca w DOSTCEN aby sprawdzic czy mamy dodawac nowego czy nie
        if (exists(select first 1 1 from DOSTCEN where WERSJAREF = :WERSJAREF and DOSTAWCA = :dostawca)) then isdostcen = 1;
        else isdostcen = 0;
        if(:uzup=0 and :isdostcen=0) then doo = 0;
        -- sprawdzamy pelny klucz czy rekord bedzie dodawany czy aktualizowany
        if (exists(select first 1 1 from DOSTCEN where WERSJAREF = :WERSJAREF and DOSTAWCA = :dostawca and WALUTA = :walcen and ODDZIAL = :oddzial)) then isdostcen = 1;
        else isdostcen = 0;
      end else doo = 0;
      if(:nowacenasprnet is null) then nowacenasprnet = 0;
      if(:cenazak is null) then cenazak = 0;
      if(:ilosc > 0 and :summagzak > 0) then cenakoszt =  :cenazak + :sumkosztzak * :wartosc / :summagzak / :ilosc;
      else cenakoszt = :cenazak;
      cenafab = :nowacenasprnet;
      if(:doo = 1) then begin--aktrualizacja dostcen
        if(:isdostcen=0) then begin--insert into dostcen
          select CENA_ZAKN, CENA_FABN, CENA_KOSZTN from WERSJE where REF = :WERSJAREF
          into :oldcenazak, :oldcenafab, :oldcenakoszt;
          if(:walcen=:walpln) then begin
            if(:wyzsze > 0) then begin
              if((:wyzsze = 1 and :oldcenazak < :cenazak)
                or(:wyzsze = 2 and :oldcenafab < :cenafab)
                or(:wyzsze = 3 and :oldcenakoszt < :cenakoszt)
              )then aktu = 1;
              else aktu = 0;
            end else aktu = 1;
          end else aktu = 0;
          if(:obliczcenzak=0 or (:cenazak=0)) then cenazak = :oldcenazak;
          if(:obliczcenkoszt=0 or (:cenakoszt=0)) then cenakoszt = :oldcenakoszt;
          if(:obliczcenfab=0 or (:cenafab=0)) then cenafab = :oldcenafab;
          if(:dostawca > 0) then
            insert into dostcen(DOSTAWCA, WERSJAREF, WALUTA, ODDZIAL, JEDN, CENANET, CENA_FAB, cena_koszt, GL, CENAZAKGL, PREC)
              values(:dostawca, :wersjaref, :walcen, :oddzial, :jedn, :cenazak, :cenafab, :cenakoszt, :ustaw, :aktu, :prec);
        end else begin--end insert into dostcen / begin update dostcen
          select CENANET, CENA_FAB, CENA_KOSZT, JEDN
          from DOSTCEN where WERSJAREF = :WERSJAREF and DOSTAWCA = :dostawca and WALUTA=:walcen and ODDZIAL=:oddzial
          into :oldcenazak, :oldcenafab, :oldcenakoszt, :oldjedn;
          przelicz1 = 1;
          if(:oldjedn > 0) then select przelicz from TOWJEDN where REF=:oldjedn into :przelicz1;
          if(:obliczcenzak=0 or (:cenazak=0)) then cenazak = :oldcenazak / :przelicz1;
          if(:obliczcenkoszt=0 or (:cenakoszt=0)) then cenakoszt = :oldcenakoszt / :przelicz1;
          if(:obliczcenfab=0 or (:cenafab=0)) then cenafab = :oldcenafab / :przelicz1;
          select CENA_ZAKN, CENA_FABN, CENA_KOSZTN from WERSJE where REF = :WERSJAREF
          into :oldcenazak, :oldcenafab, :oldcenakoszt;
          if(:walcen=:walpln) then begin
            if(:wyzsze > 0) then begin
              if((:wyzsze = 1 and :oldcenazak < :cenazak)
                or(:wyzsze = 2 and :oldcenafab < :cenafab)
                or(:wyzsze = 3 and :oldcenakoszt < :cenakoszt)
              )then aktu = 1;
              else aktu = 0;
            end else aktu = 1;
          end else aktu = 0;
          update DOSTCEN set JEDN = :jedn, CENANET = :cenazak, CENA_FAB = :cenafab, CENA_KOSZT = :cenakoszt,
             GL = case when :ustaw = 1 and GL = 0 then :ustaw else GL end, CENAZAKGL = :aktu, PREC = :prec
             where DOSTAWCA = :dostawca and WERSJAREF = :wersjaref and WALUTA=:walcen and ODDZIAL=:oddzial;
        end--update dostcen
      end else begin-- end aktualizacja dostcen / begin update wersje
        select CENA_ZAKN, CENA_FABN, CENA_KOSZTN from WERSJE where REF = :WERSJAREF
        into :oldcenazak, :oldcenafab, :oldcenakoszt;
        if(:walcen=:walpln) then begin
          if(:wyzsze > 0) then begin
            if((:wyzsze = 1 and :oldcenazak < :cenazak)
              or(:wyzsze = 2 and :oldcenafab < :cenafab)
              or(:wyzsze = 3 and :oldcenakoszt < :cenakoszt)
            ) then aktu = 1;
            else aktu = 0;
          end else aktu = 1;
        end else aktu = 0;
        if (aktu = 1) then begin--aktualizacja kartoteki towarowej
          if(:obliczcenzak=0 or (:cenazak=0)) then cenazak = :oldcenazak;
          if(:obliczcenkoszt=0 or (:cenakoszt=0)) then cenakoszt = :oldcenakoszt;
          if(:obliczcenfab=0 or (:cenafab=0)) then cenafab = :oldcenafab;
          select VAT.STAWKA from TOWARY join VAT on (TOWARY.vat=VAT.grupa) where TOWARY.KTM=:ktm into :vat;
          if(:vat is null) then vat = 0;
          vat = 1+:vat/100;
          update WERSJE set CENA_ZAKN = :cenazak,
                            CENA_ZAKB = :cenazak * :vat,
                            CENA_KOSZTN = :cenakoszt,
                            CENA_KOSZTB = :cenakoszt * :vat,
                            CENA_FABN = :cenafab,
                            CENA_FABB = :cenafab * :vat
          where REF = :wersjaref;
        end--aktualizacja kartoteki towarowej
      end--bez aktualizacji dosten
    end
  end
  status  = 1;
end^
SET TERM ; ^
