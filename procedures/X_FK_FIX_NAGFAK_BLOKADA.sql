--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_FK_FIX_NAGFAK_BLOKADA as
DECLARE VARIABLE REF INTEGER;
DECLARE VARIABLE NREF INTEGER;
DECLARE VARIABLE oldnumer INTEGER;
DECLARE VARIABLE POZOPER INTEGER;
DECLARE VARIABLE SYMBOL VARCHAR(20);
DECLARE VARIABLE SYMBOL1 VARCHAR(255);
DECLARE VARIABLE ile integer;
begin
  -- PW
  -- procedura aktualizuje znacznik blokada na dokumentach z innych tabel powiazanych z bkdoksami
  -- przyczyny niezgodnosci moga byc rozne (np replikator) wiec moze sie komus przydac
  for select n.ref
        from nagfak N
        left join bkdocs B on (B.otable = 'NAGFAK' and B.oref = N.ref)
        where B.ref is null
          AND N.BLOKADA = 3
          and n.okres > '200500'
    into :NREF
  do begin
    update nagfak N set N.blokada = 0 where N.ref = :Nref;
  end

  for select dD.ref
    from dokumnag DD
    left join bkdocs B on (B.otable = 'DOKUMNAG' and B.oref = dD.ref)
  where
    B.ref is null
    and dD.okres > '200500'
    AND bin_and(Dd.BLOKADA,4) > 0
    into :NREF
  do begin
    update dokumNAG set blokada = bin_and(BLOKADA,3) where ref = :Nref;
  end


  for select d.ref
    from dokumnot D
    left join bkdocs B on (B.otable = 'DOKUMNOT' and B.oref = d.ref)
  where
    B.ref is null
    and d.okres > '200500'
    AND bin_and(d.BLOKADA,4) > 0
    into :NREF
  do begin
    update dokumnot set blokada = bin_and(BLOKADA,3) where ref = :Nref;
  end

  for select K.ref
        from RKRAPKAS k
        left join bkdocs B on (B.otable = 'RKRAPKAS' and B.oref = K.ref)
        where B.ref is null
          AND K.status > 1
          and K.okres > '200500'
    into :NREF
  do begin
    update RKRAPKAS set STATUS = 0 where ref = :Nref;
  end
end^
SET TERM ; ^
