--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CPLOPER_WYSTAWUSUN(
      REFPOZZAM integer)
  returns (
      RET integer)
   as
declare variable refnagzam integer;
declare variable status integer;
declare variable refcploper integer;
declare variable refuczest integer;
declare variable refprogram integer;
declare variable data date;
declare variable typpkt integer;
declare variable ilosc integer;
declare variable cenapkt integer;
declare variable ilpkt integer;
declare variable nagroda integer;
declare variable opis varchar(1024);
declare variable refdefoper integer;
declare variable operlast integer;
begin
  select CPLNAGZAM,CPLOPER,ILOSC,CENAPKT,ILPKT,NAGRODA,OPIS from CPLPOZZAM where REF=:refpozzam into :refnagzam, :refcploper, :ilosc, :cenapkt, :ilpkt, :nagroda, :opis;
  select STATUS,CPL,CPLUCZEST,DATAREAL,TYPPKT,OPERLAST from CPLNAGZAM where REF=:refnagzam into :status, :refprogram, :refuczest, :data, :typpkt, :operlast;
  refdefoper = NULL;
  select REF from CPLDEFOPER where (CPL=:refprogram) and (TYPPKT=:typpkt) and (OPERDOZAM=1) into :refdefoper;
  if((:status>0) and (:status<4)) then begin
    if(:refdefoper is NULL) then select REF from CPLDEFOPER where (CPL=:refprogram) and (OPERDOZAM=1) into :refdefoper;
    if(:refdefoper is NULL) then exception CPLDEFOPER_OPERDOZAM;
  end
  if(:data is NULL) then data = current_date;
  if(:refcploper is not NULL) then begin
    /* usun CPLOPER */
    update CPLOPER set CPLNAGZAM=NULL where REF=:refcploper;
    delete from CPLOPER where REF=:refcploper;
    update CPLPOZZAM set CPLOPER=NULL where REF=:refpozzam;
    refcploper = NULL;
    ret = 0;
  end
  if((:status>0) and (:status<4)) then begin
    /* wystaw nowy CPLOPER */
    refcploper = gen_id(GEN_CPLOPER,1);
    insert into CPLOPER(REF,OPERATOR,CPLUCZEST,CPL,DATA,OPERACJA,TYPPKT,PM,ILOSC,CENAPKT,ILPKT,NAGRODA,OPIS,ZREAL,DATAOPER,CPLNAGZAM)
    values(:refcploper,:operlast,:refuczest,:refprogram,:data,:refdefoper,:typpkt,'-',:ilosc,:cenapkt,:ilpkt,:nagroda,:opis,0,current_date,:refnagzam);
    update CPLPOZZAM set CPLOPER=:refcploper where REF=:refpozzam;
    ret = 1;
  end
end^
SET TERM ; ^
