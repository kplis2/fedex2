--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PMPLAN_NEW_VERSION(
      PMPLAN integer,
      WERSJA varchar(20) CHARACTER SET UTF8                           )
   as
declare variable pmelement integer;
declare variable pmplannew integer;
begin
  execute procedure GEN_REF('PMPLANS') returning_values :pmplannew;
  insert into pmplans(ref, regdate, regoper, name, symbol, displayname,
    contrtype, descript, country, region, city, street, phone, fax, pmcontract,
    magazyn, faza, grupa, readrights, writerights, readrightsgroup, writerightsgroup,
    plstartdate, plenddate, plclosedate, klient, platnik, pmvallimit, pmplanmaster,
    osymbol, oregdate, oinvitedate, oproddate, oprodplace, oopendate, oopenplace,
    ostartdate, oenddate, odescript, ostatus, opublic, otype, vadiumdate, vadium,
    operson, ovalue, ocurrency, oreport, oremarks, ovalid, contract, cpodmiot,
    mainref,  mainversion, version, versionnum)
    select :pmplannew, regdate, regoper, name, symbol, displayname,
    contrtype, descript, country, region, city, street, phone, fax, pmcontract,
    magazyn, faza, grupa, readrights, writerights, readrightsgroup, writerightsgroup,
    plstartdate, plenddate, plclosedate, klient, platnik, pmvallimit, pmplanmaster,
    osymbol, oregdate, oinvitedate, oproddate, oprodplace, oopendate, oopenplace,
    ostartdate, oenddate, odescript, ostatus, opublic, otype, vadiumdate, vadium,
    operson, ovalue, ocurrency, oreport, oremarks, ovalid, contract, cpodmiot,
    ref, :wersja, version, versionnum from pmplans where ref=:pmplan;
  update pmplans set version=:wersja, versionnum=versionnum+1,regdate = current_date where ref=:pmplan;
  for
    select ref from pmelements where pmplan=:pmplan into :pmelement
  do begin
    insert into pmelements (name,  number, ord,  pmplan, symbol, plstartdate,
    plenddate,calcmval, calcsval, calcw, calcwval, calce, calceval, budmval, budsval, budwval,
    budeval, realmval, realsval, realw, reale, calcval, budval, realval, status,
    readrights, writerights, readrightsgroup, writerightsgroup, descript, calcsumval,
    budsumval, realsumval, mainref)
    select name,  number, ord, :pmplannew, symbol, plstartdate,
    plenddate,calcmval, calcsval, calcw, calcwval, calce, calceval, budmval, budsval, budwval,
    budeval, realmval, realsval, realw, reale, calcval, budval, realval, status,
    readrights, writerights, readrightsgroup, writerightsgroup, descript, calcsumval,
    budsumval, realsumval, :pmelement from pmelements where ref=:pmelement;
  end
end^
SET TERM ; ^
