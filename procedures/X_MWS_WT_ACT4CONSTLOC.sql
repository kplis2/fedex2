--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_WT_ACT4CONSTLOC(
      MWSACTREF integer)
  returns (
      TOWAR TOWARY_NAZWA,
      WERSJA STRING60,
      ILOSC ILOSCI_MAG,
      ILOSCP ILOSCI_MAG,
      ZREAL ILOSCI_MAG,
      GOOD KTM_ID,
      VERS integer)
   as
declare variable mwsconslocp integer;
declare variable mwsord integer;
begin
  select a.mwsconstlocp, a.mwsord
    from mwsacts a
    where a.ref= :mwsactref
  into :mwsconslocp, :mwsord;

  for
    select a.good, a.vers, sum(a.quantity), sum(a.quantityc)
      from mwsacts a
      where a.mwsord = :mwsord
        --and a.status < 5
        and a.status > 0
        and a.mwsconstlocp = :mwsconslocp
      group by a.good, a.vers
      having sum(a.quantity)- sum(a.quantityc) > 0
    into :good, :vers, :iloscp, :zreal


  do begin
    if (zreal is null) then zreal = 0;
    ilosc = iloscp - zreal;
    select t.nazwa from towary t where t.ktm = :good into :towar;
    select w.nazwa from wersje w where w.ref = :vers into :wersja;
    suspend;
    ilosc = 0;
  end
end^
SET TERM ; ^
