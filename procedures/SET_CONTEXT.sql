--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SET_CONTEXT(
      NAME1 varchar(255) CHARACTER SET UTF8                           ,
      VAL1 varchar(255) CHARACTER SET UTF8                           ,
      NAME2 varchar(255) CHARACTER SET UTF8                           ,
      VAL2 varchar(255) CHARACTER SET UTF8                           )
   as
begin
  --procedura ustawia jedna lub dwie dowolne zmienne kontekstowe
  --uzywana w connection poolerze neosa
  if(:name1 is not null and :name1<>'') then
    rdb$set_context('USER_SESSION',name1,val1);
  if(:name2 is not null and :name2<>'') then
    rdb$set_context('USER_SESSION',name2,val2);
end^
SET TERM ; ^
