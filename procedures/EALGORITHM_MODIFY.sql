--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EALGORITHM_MODIFY(
      EALGTYPE integer,
      ECOLUMN integer,
      ISDELETING smallint = 0)
   as
declare variable EALGNAME varchar(31);
declare variable PROCNAME varchar(31);
declare variable EALGPREFIX varchar(20);
declare variable PROC varchar(8191);
declare variable ECOL integer;
declare variable ATYPE integer;
declare variable DECL varchar(1024);
declare variable BODY varchar(2048);
begin
/*MWr: Personel - Procedura tworzy/modyfikuje (ISDELETING=0) badz usuwa (ISDELETING=1)
  algorytmy placowe XX_EF_ALGORITHM o zadanym typie EALGTYPE (badz kazdym, gdy EALGTYPE=0)
  oraz zadanym numerze skladnika placowego ECOLUMN (badz kazdym gdym ECOLUMN = 0) */

  ealgtype = coalesce(ealgtype,0);
  ecolumn = coalesce(ecolumn,0);
  ealgprefix = 'XX_EF_ALGORITHM_';

  if (isdeleting > 0) then  -- usuwanie algorytmow
  begin
    execute procedure ealgorithm_run_gen(1);

    ealgname = ealgprefix || trim(iif (ealgtype = 0, '%', ealgtype))
                   || '_' || trim(iif (ecolumn = 0, '%', ecolumn));

    for select rdb$procedure_name from rdb$procedures
      where trim(rdb$procedure_name) like :ealgname
      into :procname
    do
      execute statement 'drop procedure ' || procname;

    execute procedure ealgorithm_run_gen;
  end else

  if (ecolumn >= 0 and ealgtype >= 0) then --dodawanie/modyfikacja algorytmow
  begin
    for
      select ecolumn, ealgtype, decl, body from ealgorithms
        where (ealgtype = :ealgtype or :ealgtype = 0)
          and (ecolumn = :ecolumn or :ecolumn = 0)
        order by ecolumn, ealgtype
        into :ecol, :atype, :decl, :body
    do begin
      ealgname = ealgprefix || atype || '_' || ecol ;
      proc = 'CREATE OR ALTER PROCEDURE ' || ealgname ||
        '(key1 integer, key2 integer)
returns (ret numeric(16,2))
as
  ' || decl || '
begin
  ' || body || '
  suspend;
end;';
      execute statement proc;  --od FB 2.5 mozna stosowac dopisek 'AS user USER password PASSWORD'
      execute statement 'GRANT EXECUTE ON PROCEDURE ' || ealgname || ' TO "SENTE"';
      execute statement 'GRANT EXECUTE ON PROCEDURE ' || ealgname || ' TO "SYSDBA"';
    end
    if (ecol is not null) then execute procedure ealgorithm_run_gen;
  end
end^
SET TERM ; ^
