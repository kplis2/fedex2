--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RKRAPKAS_FIND(
      STANKAS char(2) CHARACTER SET UTF8                           ,
      STANSPRZED varchar(10) CHARACTER SET UTF8                           ,
      DATADOKKAS timestamp)
  returns (
      RAPKAS integer)
   as
declare variable autocreaterkrap integer;
begin
  select min(REF)
    from RKRAPKAS
    where  STANOWISKO = :stankas and DATAOD <= :datadokkas and DATADO >= :datadokkas and status = 0
    into :rapkas;
  if (rapkas is null or (rapkas = 0))then
  begin
    autocreaterkrap = null;
    select AUTORAPKASCREATE
      from STANSPRZED
      where STANSPRZED.stanowisko = :stansprzed
      into :autocreaterkrap;
    if (autocreaterkrap > 0) then
    begin
      insert into RKRAPKAS(STANOWISKO,DATAOD,DATADO) values(:stankas, :datadokkas, :datadokkas);
      select min(REF)
        from RKRAPKAS
        where  STANOWISKO = :stankas and DATAOD <= :datadokkas and DATADO >= :datadokkas and status = 0
        into :rapkas;
    end
  end
  if (rapkas is null) then rapkas = 0;
end^
SET TERM ; ^
