--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_KONTAREG(
      PERIODFROM varchar(6) CHARACTER SET UTF8                           ,
      PERIODTO varchar(6) CHARACTER SET UTF8                           ,
      CURRENCY varchar(3) CHARACTER SET UTF8                           ,
      BKREG varchar(15) CHARACTER SET UTF8                           ,
      ACCTYP smallint,
      ACCOUNTMASK ACCOUNT_ID,
      FROMACCOUNT ACCOUNT_ID,
      TOACCOUNT ACCOUNT_ID,
      COMPANY integer)
  returns (
      REF integer,
      SYMBOL varchar(3) CHARACTER SET UTF8                           ,
      ACCOUNT ACCOUNT_ID,
      DESCRIPT varchar(80) CHARACTER SET UTF8                           ,
      DEBIT numeric(15,2),
      CREDIT numeric(15,2),
      BDEBIT numeric(15,2),
      BCREDIT numeric(15,2),
      ISACCOUNT smallint)
   as
declare variable old_symbol varchar(3);
declare variable yearid integer;
declare variable old_account ACCOUNT_ID;
declare variable s_account ACCOUNT_ID;
declare variable n_debit numeric(15,2);
declare variable n_credit numeric(15,2);
declare variable i_ref integer;
declare variable s_sep varchar(1);
declare variable i_dictdef integer;
declare variable i_len smallint;
declare variable s_tmp ACCOUNT_ID;
declare variable old_prefix ACCOUNT_ID;
declare variable i_i smallint;
declare variable i_j smallint;
declare variable i_k smallint;
declare variable dictref integer;
begin
  --TOFIX!!! - niewykorzystana waluta, styl
  ref = 0;
  old_symbol = '';
  old_account = '';
  old_prefix = '';
  execute procedure create_account_mask(:accountmask) returning_values accountmask;
  if (accountmask = '%') then accountmask = null;

  select yearid from BKPERIODS where company = :company and ID = :periodfrom into yearid;
  if (toaccount is not null and toaccount <> '') then
  begin
    select max(account)
      from accounting
      where (account <= :toaccount or account like :toaccount || '%')
        and yearid = :yearid and accounting.company = :company
      into :toaccount;
  end

  for select D.account, sum(D.debit), sum(D.credit)
    from decrees D
      join bkaccounts B on (B.ref = D.accref)
    where D.status > 1 and D.bkreg = :bkreg
      and (:ACCOUNTMASK is null or :accountmask = '' or D.account like :accountmask)
      and (:fromaccount is null or :fromaccount = '' or :fromaccount <= D.account)
      and (:toaccount is null or :toaccount = '' or :toaccount >= D.account)
      and D.period >= :periodfrom and D.period <= :periodto
      and ((:acctyp = 0 and B.bktype < 2) or (:acctyp=1 and (B.bktype = 2 or B.bktype = 3)) or :acctyp = 2)
      and D.company = :company
    group by D.account
    order by D.account
    into  :s_account, :n_debit, :n_credit
  do begin
    symbol = cast(substring(:s_account from 1 for 3) as varchar(3));
    debit = null;
    credit = null;
    bdebit = null;
    bcredit = null;
    if (n_debit is null) then n_debit = 0;
    if (n_credit is null) then n_credit = 0;
    isaccount = 0;
    if (old_symbol <> symbol) then -- szukamy nazwy syntetyki
    begin
      select descript, ref
        from bkaccounts where symbol=:symbol and yearid = :yearid and bkaccounts.company = :company
        into :descript, :i_ref;
      debit = null;
      credit = null;
      account = symbol;
      if (symbol <> s_account) then begin
        ref = ref + 1;
        suspend;
      end
      i_dictdef = null;
      i_i = 0;
      -- zliczam ilosc poziomów nalitycznych
      for select separator, dictdef, len
        from accstructure where bkaccount = :i_ref
        order by nlevel
        into :s_sep, :i_dictdef, :i_len
      do begin
        i_i = i_i + 1;
      end
    end
    if (i_i > 0) then
    begin
      if (i_i > 1 and old_prefix <> substring(s_account from  1 for coalesce(char_length(s_account),0) - i_len)) then -- [DG] XXX ZG119346
      -- drukowanie opisow posrednich analityk
      begin
        i_j = 0;
        i_k = 4;
        for select separator, dictdef, len
          from accstructure where bkaccount = :i_ref
          order by nlevel
          into :s_sep, :i_dictdef, :i_len
        do begin
          i_j = i_j + 1;
          if (i_j < i_i) then
          begin
            if (s_sep <> ',') then
              i_k = i_k +1;
            s_tmp = substring(s_account from  i_k for  i_k + i_len - 1);
            account = '      ' || s_tmp;
            execute procedure name_from_bksymbol(company, i_dictdef, s_tmp)
              returning_values descript, dictref;
            ref = ref + 1;
            suspend;
            i_k = i_k + i_len;
          end
        end
        old_prefix = substring(s_account from  1 for coalesce(char_length(s_account) , 0) - i_len); -- [DG] XXX ZG119346
      end
      s_tmp = substring(s_account from  coalesce(char_length(s_account) , 0) - i_len+1 for coalesce(char_length(s_account),0)); -- [DG] XXX ZG119346
      execute procedure name_from_bksymbol(company, i_dictdef, s_tmp)
        returning_values descript, dictref;
    end
    debit = n_debit;
    credit = n_credit;
    if (credit > debit) then
      bcredit = credit - debit;
    else
      bdebit = debit - credit;
    if (bdebit is null) then bdebit = 0;
    if (bcredit is null) then bcredit = 0;
    account = s_account;
    old_symbol = symbol;
    old_account = account;
    ref = ref + 1;
    isaccount = 1;
    suspend;
  end
end^
SET TERM ; ^
