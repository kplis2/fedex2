--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHED_OPER_SAVEDEPEND(
      SCHOPERTO integer,
      OPERFROM integer)
   as
declare variable opercomplex smallint;
declare variable opertype smallint;
declare variable schoperfrom integer;
declare variable schedule integer;
declare variable schedzam integer;
declare variable prevoper integer;
declare variable schedguide integer;
begin
  /* Procedure Text */
--if(operfrom <> 23092) then exception test_break schoperto||' '||operfrom;
  select prshopers.complex, prshopers.opertype from prshopers where ref=:operfrom
    into :opercomplex, :opertype;
  if(:opercomplex is null) then exception universal 'PRSCHED_OPER_SAVEDEPEND nieokrelony typ operacji';
  if(:opercomplex = 0) then begin
    --odczytanie operacji na harm. ktora uzaleznia
    select prschedopers.schedule, prschedopers.schedzam, prschedopers.guide
      from prschedopers where ref=:schoperto
    into :schedule, :schedzam, :schedguide;
    select ref from prschedopers where schedzam = :schedzam and (:schedguide is null or guide = :schedguide) and shoper = :operfrom
      into schoperfrom;
     if(:schoperfrom > 0) then -- tylko wtedy, kiedy ta oepracjaistnieje w tym harmonogramie
       insert into prschedoperdeps(DEPTO,DEPFROM) values (:schoperto, :schoperfrom);
  end else if(:opertype = 0) then begin
    --szeregowo - trzeba uzaleznic ostatni lisc
    select max(NUMBER) from prshopers where opermaster = : operfrom into :prevoper;
    if(:prevoper > 0) then begin
      select REF from PRSHOPERS where opermaster = :operfrom and number = :prevoper into :prevoper;
      execute procedure PRSCHED_OPER_SAVEDEPEND (:schoperto,:prevoper);
    end
  end else begin
    for select prshopers.ref from prshopers where opermaster = :operfrom
    order by number
    into :prevoper
    do begin
      execute procedure PRSCHED_OPER_SAVEDEPEND (:schoperto,:prevoper);
    end
  end
end^
SET TERM ; ^
