--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CONST_LOC_SET_ACT(
      SYMBOL varchar(40) CHARACTER SET UTF8                           ,
      REASON STRING,
      ACT smallint)
   as
begin
    update mwsconstlocs set mwsconstlocs.x_reason_blok = :reason, mwsconstlocs.act = :act where mwsconstlocs.symbol = :symbol;
end^
SET TERM ; ^
