--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ORDER_WERSJE(
      KTM varchar(40) CHARACTER SET UTF8                           ,
      OLDN integer,
      NEWN integer)
   as
declare variable ref integer;
  declare variable number integer;
begin
  --firts we find minor number
  if (oldn < newn) then
    newn = oldn;

  number = newn;
  for
    select ref
      from WERSJE
      where ktm = :ktm and nrwersji >= :number
      order by nrwersji, ord
      into :ref
  do begin
    update WERSJE set nrwersji = :number, ord = 2
      where ref = :ref;
    number = number + 1;
  end

  update WERSJE set ord = 1
    where ktm = :ktm and nrwersji >= :newn;
end^
SET TERM ; ^
