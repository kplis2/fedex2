--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_MWSACT_CALC_QUANTITYL(
      MWSACT integer)
   as
declare variable quantity numeric(15,5);
declare variable corquantity numeric(15,5);
declare variable status smallint;
declare variable doctype varchar(1);
declare variable docid integer;
declare variable docposid integer;
declare variable stocktaking smallint;
declare variable recdoc smallint;
begin

--exception universal mwsact;
  select quantity, status from mwsacts where ref = :mwsact
    into quantity, status;
  if (quantity is null) then quantity = 0;
  select sum(quantity)
    from mwsacts where cortomwsact = :mwsact and status < 6
    into corquantity;
  if (corquantity is null) then corquantity = 0;
  quantity = quantity - corquantity;
  if (status = 0) then
    quantity = 0;
  update mwsacts set quantityl = :quantity where ref = :mwsact;
  select doctype, docid,docposid,stocktaking,recdoc
    from mwsacts
    where ref = :mwsact
    into doctype, docid,docposid,stocktaking,recdoc;
  if (doctype = 'M') then
    execute procedure DOKUMPOZ_OBL_FROM_MWSACTS(doctype, docid, docposid, stocktaking, recdoc, 1, 1);
  else if (doctype = 'Z') then
    execute procedure POZZAM_OBL_FROM_MWSACTS(doctype, docid, docposid, stocktaking, recdoc, 1, 1);
end^
SET TERM ; ^
