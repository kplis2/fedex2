--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_FK_BANKACC(
      DATA_MAX timestamp,
      ZAKRES smallint,
      COMPANY smallint)
  returns (
      REF integer,
      SYMBOL varchar(20) CHARACTER SET UTF8                           ,
      NAZWA varchar(40) CHARACTER SET UTF8                           ,
      STAN_KONTA numeric(14,2),
      SUMTR_0 numeric(14,2),
      SUMTR_1 numeric(14,2),
      SUMTR_2 numeric(14,2),
      SALDO numeric(14,2))
   as
declare variable data_rap timestamp;
declare variable rk_ref integer;
declare variable stbank char(2);
begin
  ref = 0;
  for
    select BA.symbol, BA.name, BA.stanbank, max(RK.dataod)
      from bankacc BA
        left join rkrapkas RK on (BA.stanbank = RK.stanowisko)
      where BA.company = :company
      group by BA.symbol, BA.name, BA.stanbank
      into :symbol, :nazwa, :stbank, :data_rap
  do begin
    --tresc
    stan_konta = 0;
    sumtr_0 = 0;
    sumtr_1 = 0;
    sumtr_2 = 0;
    saldo = 0;

    select max(RK.ref)
      from rkrapkas RK
      where RK.dataod = :data_rap and RK.stanowisko = :stbank
      into  :rk_ref;

    select case when S.walutowa = 1 then RK.skwota else RK.skwotazl end
      from rkrapkas RK
        join rkstnkas S on (S.kod = RK.stanowisko)
      where RK.ref = :rk_ref into :stan_konta;

    select sum(BT.amount)
      from btransfers BT
      where BT.bankacc = :symbol and BT.data >= :data_rap and BT.data <= :data_max
        and BT.status between 0 and 2
      into :sumtr_0;

    select sum(BT.amount)
      from btransfers BT
      where BT.bankacc = :symbol and BT.data >= :data_rap and BT.data <= :data_max
        and BT.status between 1 and 2
      into :sumtr_1;

    select sum(BT.amount)
      from btransfers BT
      where BT.bankacc = :symbol and BT.data >= :data_rap and BT.data <= :data_max
        and BT.status = 2
      into :sumtr_2;

    if (sumtr_0 is null) then sumtr_0 = 0;
    if (sumtr_1 is null) then sumtr_1 = 0;
    if (sumtr_2 is null) then sumtr_2 = 0;

    if (zakres = 0) then saldo = stan_konta - sumtr_0;
    if (zakres = 1) then saldo = stan_konta - sumtr_1;
    if (zakres = 2) then saldo = stan_konta - sumtr_2;

    suspend;
    ref = ref +1;
  end
end^
SET TERM ; ^
