--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSCHEDOPER_CHECK_AMOUNTZREAL(
      PRSCHEDOPER integer)
   as
declare variable amountdet numeric(14,4);
declare variable amountoper numeric(14,4);
declare variable amountopers numeric(14,4);
declare variable quantity numeric(14,4);
begin
  select sum(d.quantity)
    from prschedguidedets d
    where d.prschedoper = :prschedoper and d.out = 0 and d.overlimit = 0 and d.shortage = 0 and d.byproduct = 0
    into amountdet;
  if (amountdet is null) then amountdet = 0;
  select p.amountresult, p.amountshortages
    from prschedopers p where p.ref = :prschedoper
    into amountoper, amountopers;
  if (amountoper is null) then amountoper = 0;
  if (amountoper is null) then amountopers = 0;
  if (amountoper < amountdet) then
    exception prschedguidedets_error 'Przyjęto więcej niż zaraportowano na operacji';
  amountdet = 0;
  for
    select max(s.headergroupamount)
      from prschedguidedets d
        left join prshortages s on (s.ref = d.prshortage)
      where d.prschedoper = :prschedoper and d.out = 0 and d.overlimit = 0 and d.shortage = 1 and d.byproduct = 0
      group by s.headergroupamount
      into quantity
  do begin
    if (quantity is null) then quantity = 0;
    amountdet = amountdet + quantity;
  end
  if (amountopers < amountdet) then
    exception prschedguidedets_error 'Przyjęto więcej braków niż zaraportowano na operacji';
end^
SET TERM ; ^
