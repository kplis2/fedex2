--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_BROWSE_DOKUMPOZ_TOKOR(
      DOKUMREF INTEGER_ID,
      DOKUMREFK INTEGER_ID)
  returns (
      NUMER INTEGER_ID,
      KTM KTM_ID,
      NAZWA TOWARY_NAZWA,
      WERSJA TOWARY_NAZWA,
      ILOSC ILOSCI_MAG,
      ILOSCL ILOSCI_MAG,
      REF INTEGER_ID,
      X_PARTIA STRING,
      X_NUMER_NO INTEGER_ID,
      X_SLOWNIK INTEGER_ID,
      X_NUMER_NOSTR STRING,
      X_SLOWNIKSTR STRING)
   as
begin
  for
    select dp.numer, dp.ktm, wersje.nazwat as nazwa, wersje.nazwa as wersja,
        max(dp.ilosc) as ilosc,(max(dp.iloscl) - sum(coalesce(kor.ilosc,0))) as iloscl, dp.ref as ref,
        dp.x_partia, dp.x_serial_no, dp.x_slownik

      from dokumpoz dp left join wersje on (wersje.ref = dp.wersjaref)
        left join dokumpoz kor on(kor.kortopoz = dp.ref and kor.dokument = :dokumref)
      where dp.dokument = :dokumrefk
        and (kor.ref is null or dp.iloscl<>kor.ilosc)
      group by dp.numer, dp.ktm, wersje.nazwat,wersje.nazwa,dp.ref,kor.kortopoz, dp.x_partia, dp.x_serial_no, dp.x_slownik
      having max(dp.iloscl) <> sum(coalesce(kor.ilosc,0))
      order by dp.ref
    into :numer, :ktm, :nazwa, :wersja,
        :ilosc, :iloscl, :ref, 
        :x_partia, :x_numer_no, :x_slownik
  do begin
    if (coalesce(x_numer_no,0) <> 0) then
      select s.serialno from x_mws_serie s where s.ref = :x_numer_no into :x_numer_nostr;


    suspend;
    x_numer_nostr = null;
  end
end^
SET TERM ; ^
