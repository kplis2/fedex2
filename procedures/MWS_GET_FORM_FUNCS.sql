--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_GET_FORM_FUNCS(
      AKTUOPERATOR integer,
      AKTUMAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      FORMNAME varchar(255) CHARACTER SET UTF8                           )
  returns (
      ENABLED varchar(4096) CHARACTER SET UTF8                           )
   as
declare variable func varchar(255);
begin
  enabled = 'Aktywne;';
  for
    select f.hhfuncname
      from mwshhfuncdefs f
      where f.form = :formname and f.alwaysenabled = 1 and f.disabled = 0
      into func
  do begin
    enabled = enabled||func||';';
  end
  for
    select f.hhfuncname
      from opermagmwshhfuncs o
        left join mwshhfuncdefs f on (f.ref = o.mwshhfunc)
      where f.form = :formname and f.alwaysenabled = 0 and f.disabled = 0
        and o.operator = :aktuoperator and o.magazyn = :aktumagazyn
      into func
  do begin
    enabled = enabled||func||';';
  end
end^
SET TERM ; ^
