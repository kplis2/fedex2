--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_UE3_APOZ(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      YEARID varchar(4) CHARACTER SET UTF8                           ,
      COMPANY integer,
      RODZAJ integer,
      CURRPAGEA integer)
  returns (
      NUMER integer,
      A smallint,
      B varchar(15) CHARACTER SET UTF8                           ,
      C numeric(14,2),
      D numeric(14,2))
   as
declare variable odokresu varchar(2);
declare variable dookresu varchar(2);
declare variable skp integer;
declare variable ile integer;
begin

if (rodzaj = 0) then
  begin
    if (period = 0) then
    begin
      odokresu = '01';
      dookresu = '03';
    end
    if (period = 1) then
    begin
      odokresu = '04';
      dookresu = '06';
    end
    if (period = 2) then
    begin
      odokresu = '07';
      dookresu = '09';
    end
    if (period = 3) then
    begin
      odokresu = '10';
      dookresu = '12';
    end
  end

if (currpagea > 0) then
begin
  skp = 59*(:currpagea-1);
  ile = 59;
end
else
begin
  skp = 0;
  ile = 12;
end
numer=0;

if (rodzaj = 1) then
  begin
  for  select first (:ile) skip (:skp) K.nip, sum(BV.netv)
        from bkdocs B
        join vatregs V on (V.symbol = B.vatreg and V.company = B.company)
        left join klienci K on (K.ref = B.dictpos)
        join bkvatpos BV on (BV.bkdoc = B.ref)
          where v.vtype = 1 and BV.vatgr <> 'NP'
          and B.status > 0
          and b.vatperiod = :period
          and B.company = :company
          group by K.nip
          into :b, :c
  do begin
   numer = numer+1;

   suspend;
  end
  end
    else begin
    for select first (:ile) skip (:skp) K.nip, sum(BV.netv)
        from bkdocs B
        join vatregs V on (V.symbol = B.vatreg and V.company = B.company)
        left join klienci K on (K.ref = B.dictpos)
        join bkvatpos BV on (BV.bkdoc = B.ref)
          where v.vtype = 1 and BV.vatgr <> 'NP'
          and B.status > 0
          and substring(b.vatperiod from 1 for 4) = :yearid
          and substring(b.vatperiod from 5 for 2) >= :odokresu
          and substring(b.vatperiod from 5 for 2) <= :dookresu
          and B.company = :company
          group by K.nip
          into :b, :c
       do begin
   numer = numer+1;
   suspend;
  end
  end
  a=0;
  b='';
  c=0;
  d=0;
  while(numer < ile) do
  begin
  numer = numer + 1;
   suspend;
  end
end^
SET TERM ; ^
