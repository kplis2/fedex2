--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ROZRACHROZ_OBLDOK(
      DOKTYP char(1) CHARACTER SET UTF8                           ,
      DOKREF integer)
   as
declare variable winien numeric(14,2);
declare variable ma numeric(14,2);
declare variable winienzl numeric(14,2);
declare variable mazl numeric(14,2);
declare variable zakup smallint;
declare variable wydania smallint;
declare variable kurs numeric(14,4);
begin
  --SN
  select sum(WINIEN), sum(MA) from ROZRACHROZ where DOKTYP = :doktyp and DOKREF = :dokref and stable <> 'POZFAK' into :winien, :ma;
  if(:winien is null) then winien = 0;
  if(:ma is null) then ma = 0;
  if(:doktyp = 'F') then begin
    select NAGFAK.zakup from NAGFAK where ref=:dokref into :zakup;
    if(:zakup = 1) then
      update NAGFAK set nagfak.rozrachrozval = :ma - :winien where ref=:dokref;
    else
      update NAGFAK set nagfak.rozrachrozval = :winien - :ma where ref=:dokref;

  end else if(:DOKTYP = 'D') then begin
    update LISTYWYSD set ROZRACHROZVAL = :winien - :ma where ref=:dokref;
    execute procedure listywysd_obliczpobranie(:dokref);
  end else if(:doktyp = 'Z') then begin
    select TYPZAM.wydania, nagzam.kurs from TYPZAM join nagzam on(typzam.symbol = nagzam.typzam) where NAGZAM.ref = :DOKREF into :wydania, :kurs;
    if(:kurs is null or (:kurs = 0))then kurs = 1;
    winienzl = winien * kurs;
    mazl = ma * kurs;
    if(:wydania = 0) then
      update NAGZAM set nagzam.sumazaliczek = :ma - :winien, nagzam.sumazaliczekzl = :mazl - :winienzl where ref=:dokref;
    else
      update NAGZAM set nagzam.sumazaliczek = :winien - :ma, nagzam.sumazaliczekzl = :winienzl - :mazl where ref=:dokref;
  end
end^
SET TERM ; ^
