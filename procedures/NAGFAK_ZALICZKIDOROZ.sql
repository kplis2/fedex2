--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NAGFAK_ZALICZKIDOROZ(
      FAKTURA integer,
      TRYB integer,
      FAKTURAZAL integer)
  returns (
      REF integer,
      VAT varchar(5) CHARACTER SET UTF8                           ,
      SYMBOL varchar(100) CHARACTER SET UTF8                           ,
      EWARTNET numeric(14,2),
      EWARTBRU numeric(14,2),
      PEWARTNET numeric(14,2),
      PEWARTBRU numeric(14,2),
      WARTNET numeric(14,2),
      WARTBRU numeric(14,2),
      EWARTNETZL numeric(14,2),
      EWARTBRUZL numeric(14,2),
      PEWARTNETZL numeric(14,2),
      PEWARTBRUZL numeric(14,2),
      WARTNETZL numeric(14,2),
      WARTBRUZL numeric(14,2))
   as
declare variable KLIENT integer;
declare variable DOSTAWCA integer;
declare variable ZAM integer;
declare variable ZALICZKOWY integer;
declare variable NIEOBROT integer;
declare variable ORGZAM integer;
declare variable KOREKTA integer;
begin
  select n.zaliczkowy, n.nieobrot, t.korekta
    from nagfak n left join typfak t on n.typ = t.symbol
    where ref=:faktura
  into :zaliczkowy, :nieobrot, :korekta;
  if(:korekta=1 and :zaliczkowy = 0) then exit;
  if(:zaliczkowy=1) then exit; /* nie rozliczamy zaliczek faktura zaliczkowa */
  if(:nieobrot not in (0,2)) then exit; /* nie rozliczamy zaliczek z faktura nieobrotowa/nieksiegowa */
  if(:fakturazal is null) then begin
    klient = null;
    select klient, dostawca, fromnagzam from nagfak where ref=:faktura into :klient, :dostawca, :zam;
    if(:zam>0) then select org_ref from nagzam where ref=:zam into :orgzam;
    if(:orgzam is null) then orgzam = :zam;
    if(:zam is null) then tryb = 0;
    if(:klient is not null and :klient>0) then begin
      for select nagfak.ref,rozfak.vat,nagfak.symbol,
        rozfak.esumwartnet,rozfak.esumwartbru,
        rozfak.esumwartnetzl,rozfak.esumwartbruzl
        from nagfak join rozfak on (rozfak.dokument=nagfak.ref)
        where nagfak.zaliczkowy >= 1 and nagfak.klient = :klient and nagfak.akceptacja in (1,8)
          and (( :tryb = 0)
               or (:tryb = 1 and nagfak.fromnagzam in (:zam,:orgzam))
               or (:tryb = 2)
             )
        into :ref,:vat,:symbol,:ewartnet,:ewartbru,:ewartnetzl,:ewartbruzl
      do begin
        select sum(nagfakzal.wartnet),sum(nagfakzal.wartbru),sum(nagfakzal.wartnetzl),sum(nagfakzal.wartbruzl)
          from nagfakzal
          left join nagfak on (nagfak.ref=nagfakzal.faktura)
          where nagfakzal.fakturazal=:ref and nagfakzal.vat=:vat and nagfak.anulowanie=0
          into :pewartnet, :pewartbru, :pewartnetzl, :pewartbruzl;
        if(:pewartnet is null) then pewartnet = 0;
        if(:pewartbru is null) then pewartbru = 0;
        if(:pewartnetzl is null) then pewartnetzl = 0;
        if(:pewartbruzl is null) then pewartbruzl = 0;
        wartnet = :ewartnet - :pewartnet;
        wartbru = :ewartbru - :pewartbru;
        wartnetzl = :ewartnetzl - :pewartnetzl;
        wartbruzl = :ewartbruzl - :pewartbruzl;
        if(:wartbru<>0) then suspend;
      end
    end else if(:dostawca is not null and :dostawca>0) then begin
      for select nagfak.ref,rozfak.vat,nagfak.symbol,
        rozfak.esumwartnet,rozfak.esumwartbru,
        rozfak.esumwartnetzl,rozfak.esumwartbruzl
        from nagfak join rozfak on (rozfak.dokument=nagfak.ref)
        where nagfak.zaliczkowy >= 1 and nagfak.dostawca=:dostawca and nagfak.akceptacja in (1,8)
          and (( :tryb = 0)
               or (:tryb = 1 and nagfak.fromnagzam = :zam)
               or (:tryb = 2)
             )
        into :ref,:vat,:symbol,:ewartnet,:ewartbru,:ewartnetzl,:ewartbruzl
      do begin
        select sum(nagfakzal.wartnet),sum(nagfakzal.wartbru),sum(nagfakzal.wartnetzl),sum(nagfakzal.wartbruzl)
          from nagfakzal
          left join nagfak on (nagfak.ref=nagfakzal.faktura)
          where nagfakzal.fakturazal=:ref and nagfakzal.vat=:vat and nagfak.anulowanie=0
          into :pewartnet, :pewartbru, :pewartnetzl, :pewartbruzl;
        if(:pewartnet is null) then pewartnet = 0;
        if(:pewartbru is null) then pewartbru = 0;
        if(:pewartnetzl is null) then pewartnetzl = 0;
        if(:pewartbruzl is null) then pewartbruzl = 0;
        wartnet = :ewartnet - :pewartnet;
        wartbru = :ewartbru - :pewartbru;
        wartnetzl = :ewartnetzl - :pewartnetzl;
        wartbruzl = :ewartbruzl - :pewartbruzl;
        if(:wartbru<>0) then suspend;
      end
    end
  end else begin
    select zaliczkowy from nagfak where ref=:fakturazal into :zaliczkowy;
    while(:zaliczkowy=2) do begin
      select refk from nagfak where ref=:fakturazal into :fakturazal;
      select zaliczkowy from nagfak where ref=:fakturazal into :zaliczkowy;
    end
    for select nagfak.ref,rozfak.vat,nagfak.symbol,
      rozfak.esumwartnet,rozfak.esumwartbru,
      rozfak.esumwartnetzl,rozfak.esumwartbruzl
      from nagfak join rozfak on (rozfak.dokument=nagfak.ref)
      where nagfak.ref=:fakturazal
      into :ref,:vat,:symbol,:ewartnet,:ewartbru,:ewartnetzl,:ewartbruzl
    do begin
      select sum(nagfakzal.wartnet),sum(nagfakzal.wartbru),sum(nagfakzal.wartnetzl),sum(nagfakzal.wartbruzl)
        from nagfakzal
        left join nagfak on (nagfak.ref=nagfakzal.faktura)
        where nagfakzal.fakturazal=:ref and nagfakzal.vat=:vat and nagfak.anulowanie=0
        into :pewartnet, :pewartbru, :pewartnetzl, :pewartbruzl;
      if(:pewartnet is null) then pewartnet = 0;
      if(:pewartbru is null) then pewartbru = 0;
      if(:pewartnetzl is null) then pewartnetzl = 0;
      if(:pewartbruzl is null) then pewartbruzl = 0;
      wartnet = :ewartnet - :pewartnet;
      wartbru = :ewartbru - :pewartbru;
      wartnetzl = :ewartnetzl - :pewartnetzl;
      wartbruzl = :ewartbruzl - :pewartbruzl;
      if(:wartbru<>0) then suspend;
    end
  end
end^
SET TERM ; ^
