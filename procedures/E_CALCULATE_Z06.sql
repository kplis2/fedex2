--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_CALCULATE_Z06(
      YEARID integer)
  returns (
      D1P1 integer,
      D1W1 numeric(14,1),
      D1W5 numeric(14,1),
      D1W6 numeric(14,1),
      D2W1 numeric(14,1),
      D2W6 numeric(14,1),
      D3O1 numeric(14,1),
      D3O3 numeric(14,1),
      D3O4 numeric(14,1),
      D3O5 numeric(14,1),
      D3O6 numeric(14,1),
      D4O1 integer,
      D4K1 integer,
      D5O1 integer,
      D5K1 integer)
   as
declare variable monthid integer;
declare variable firstday date;
declare variable lastday date;
declare variable sumworkdim float;
declare variable fworkdim float;
declare variable lworkdim float;
begin
  monthid = 1;
  sumworkdim = 0;
  while (monthid  < 13) do
  begin

    firstday = cast(yearid || '/' || monthid || '/1' as date);

    if (monthid = 12) then
      lastday = cast(yearid || '/' || monthid || '/31' as date);
    else
      lastday = cast(yearid || '/' || (monthid + 1) || '/1' as date) - 1;


    select sum(workdim)
      from emplcontracts
      where fromdate <= :firstday
        and (enddate is null or enddate >= :firstday)
      into :fworkdim;

  fworkdim = coalesce(fworkdim, 0);


    select sum(workdim)
      from emplcontracts
      where fromdate <= :lastday
        and (enddate is null or enddate >= :lastday)
      into :lworkdim;

  lworkdim = coalesce(lworkdim, 0);

    sumworkdim = sumworkdim + (fworkdim + lworkdim) / 2;

    monthid = monthid + 1;
  end
  d1p1 = sumworkdim / 12;


  select sum(P.pvalue) / 1000
    from epayrolls PR
      join eprpos P on (P.payroll = PR.ref)
      where PR.cper starting with :yearid and P.ecolumn = 4000
    into :d1w1;

  select sum(P.pvalue) / 1000
    from epayrolls PR                      
      join eprpos P on (P.payroll = PR.ref)
      where PR.cper starting with :yearid and P.ecolumn in (6100, 6110, 6120, 6130)
    into :d1w5;


  select sum(P.pvalue) / 1000
    from epayrolls PR
      join eprpos P on (P.payroll = PR.ref)
      where PR.cper starting with :yearid and P.ecolumn = 7100
    into :d1w6;

  --DZIAL 2

  select sum(P.pvalue) / 1000
    from epayrolls PR
      join eprpos P on (P.payroll = PR.ref)
      where PR.cper starting with :yearid and P.ecolumn = 4000
    into :d2w1;


  select sum(P.pvalue) / 1000
    from epayrolls PR
      join eprpos P on (P.payroll = PR.ref)
      where PR.cper starting with :yearid and P.ecolumn = 3000
    into :d2w6;


  --DZIAL 3

  select sum(P.pvalue) / 1000
    from epayrolls PR
      join eprpos P on (P.payroll = PR.ref)
      where PR.cper starting with :yearid and P.ecolumn = 520
    into :d3o1;


  select sum(A.worksecs) / 3600000
    from eabsences A where A.fromdate >= :yearid || '/1/1'
      and A.todate <= :yearid || '/12/31'
      and (exists(select ref from emplcontracts C join econtrtypes T on (T.contrtype = C.econtrtype) where C.employee = A.employee and C.fromdate <= A.fromdate and (C.enddate is null or C.enddate >= A.fromdate) and T.empltype = 1))
      and A.correction in (0,2)
    into :d3o3;

  select sum(A.worksecs) / 3600000
    from eabsences A where A.fromdate >= :yearid || '/1/1'
      and A.todate <= :yearid || '/12/31'
      and A.ecolumn in (40, 50, 60, 180, 220, 230, 250)
      and (exists(select ref from emplcontracts C join econtrtypes T on (T.contrtype = C.econtrtype) where C.employee = A.employee and C.fromdate <= A.fromdate and (C.enddate is null or C.enddate >= A.fromdate) and T.empltype = 1))
      and A.correction in (0,2)
    into :d3o4;


  select sum(A.worksecs) / 3600000
    from eabsences A where A.fromdate >= :yearid || '/1/1'
      and A.todate <= :yearid || '/12/31'
      and A.ecolumn in (220, 230)
      and (exists(select ref from emplcontracts C join econtrtypes T on (T.contrtype = C.econtrtype) where C.employee = A.employee and C.fromdate <= A.fromdate and (C.enddate is null or C.enddate >= A.fromdate) and T.empltype = 1))
      and A.correction in (0,2)
    into :d3o5;

  select sum(A.worksecs) / 3600000
    from eabsences A where A.fromdate >= :yearid || '/1/1'
      and A.todate <= :yearid || '/12/31'
      and A.ecolumn in (40, 50, 60)
      and (exists(select ref from emplcontracts C join econtrtypes T on (T.contrtype = C.econtrtype) where C.employee = A.employee and C.fromdate <= A.fromdate and (C.enddate is null or C.enddate >= A.fromdate) and T.empltype = 1))
      and A.correction in (0,2)
    into :d3o6;


  -- DZIAL 4
  select count(*)
    from employees E
      join emplcontracts C on (E.ref = C.employee)
      join econtrtypes T on (T.contrtype = C.econtrtype)
    where T.empltype = 1 and C.fromdate <= :yearid || '/12/13'
      and (C.enddate is null or C.enddate >= :yearid || '/12/31')
    into :d4o1;

  select count(*)
    from employees E
      join persons P on (P.ref = E.person)
      join emplcontracts C on (E.ref = C.employee)
      join econtrtypes T on (T.contrtype = C.econtrtype)
    where T.empltype = 1 and C.fromdate <= :yearid || '/12/13'
      and (C.enddate is null or C.enddate >= :yearid || '/12/31')
      and P.sex = 0
    into :d4k1;

  --DZIAL 5
  select count(*)
    from employees E
      join emplcontracts C on (E.ref = C.employee)
      join econtrtypes T on (T.contrtype = C.econtrtype)
    where T.empltype = 1 and C.fromdate <= (:yearid - 1) || '/12/13'
      and (C.enddate is null or C.enddate >= (:yearid -1) || '/12/31')
      and C.workdim >= 1
    into :d5o1;


  select count(*)
    from employees E
      join persons P on (P.ref = E.person)
      join emplcontracts C on (E.ref = C.employee)
      join econtrtypes T on (T.contrtype = C.econtrtype)
    where T.empltype = 1 and C.fromdate <= (:yearid - 1) || '/12/13'
      and (C.enddate is null or C.enddate >= (:yearid - 1) || '/12/31')
      and C.workdim >= 1 and P.sex = 0
    into :d5k1;



--ecolumns
  suspend;
end^
SET TERM ; ^
