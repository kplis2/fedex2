--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR_GET_PRSCHEDOPER_AMOUNTIN(
      PRSCHEDGUIDE integer,
      MODE smallint)
  returns (
      AMOUNTOUT numeric(14,4))
   as
declare variable amount double precision;
declare variable amountoverlimit double precision;
declare variable kilosc double precision;
declare variable kilcalcorg double precision;
declare variable amounttmp double precision;
declare variable guideamount numeric(14,4);
begin
  if (mode = 0) then
  begin
    amountout = 0;
    for
      select pgp.amountzreal, pgp.amountoverlimit, p.kilcalcorg, p.kilosc, g.amount
        from prschedguidespos pgp
          left join pozzam p on (p.ref = pgp.pozzamref)
          left join prschedguides g on (g.ref = pgp.prschedguide)
        where pgp.prschedguide = :prschedguide
        into amount, amountoverlimit, kilcalcorg, kilosc, guideamount
    do begin
      amounttmp = amount + amountoverlimit;
      amounttmp = amounttmp * kilcalcorg;
      amounttmp = amounttmp / kilosc;
      if (amountoverlimit = 0 and amounttmp > guideamount) then
        amounttmp = guideamount;
      if (amountout < amounttmp) then
        amountout = amounttmp;
    end
  end
  else if (mode = 1) then
  begin
    amountout = 0;
    for
      select pgp.amount, pgp.amountoverlimit, p.kilcalcorg, p.kilosc, g.amount
        from prschedguidespos pgp
          left join pozzam p on (p.ref = pgp.pozzamref)
          left join prschedguides g on (g.ref = pgp.prschedguide)
        where pgp.prschedguide = :prschedguide
        into amount, amountoverlimit, kilcalcorg, kilosc, guideamount
    do begin
      amounttmp = amount + amountoverlimit;
      amounttmp = amounttmp * kilcalcorg;
      amounttmp = amounttmp / kilosc;
      if (amountoverlimit = 0 and amounttmp > guideamount) then
        amounttmp = guideamount;
      if (amountout < amounttmp) then
        amountout = amounttmp;
    end
  end
end^
SET TERM ; ^
