--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_ADD_INPOST_IMP_REQUEST(
      EDERULESYMB varchar(40) CHARACTER SET UTF8                           )
  returns (
      EDEDOCHREF integer)
   as
declare variable ederuleref integer;
declare variable sposdostref integer;
begin
  select r.ref
    from ederules r
    where r.symbol = :ederulesymb
    into :ederuleref;

  select p.val
    from ederuleparams p
    where p.ederule = :ederuleref
      and p.name = 'SPOSDOST'
  into :sposdostref;

  insert into ededocsh (ederule, direction, regoper, createdate, createoper, otable, oref, path, filename)
    values (:ederuleref, 0, 0, current_timestamp, null, 'SPOSDOST', :sposdostref, 'Inpost', 'API:ListMachines[scheduler]')
    returning ref into :ededochref;
  suspend;
end^
SET TERM ; ^
