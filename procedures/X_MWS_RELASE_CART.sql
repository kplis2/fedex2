--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_RELASE_CART(
      REFDOC INTEGER_ID)
   as
  declare variable cart integer_id;
  declare variable mwsord integer_id;
  declare variable grupasped integer_id;
begin
-- porpawki dla grupy spedycyjnej
  select d.grupasped
     from dokumnag d
     where d.ref = :refdoc
  into :grupasped;

  for
    select m.cart, m.mwsord
      from mwsordcartcolours m
      join dokumnag d on d.ref = m.docid
      where d.grupasped = :grupasped
    into :cart, :mwsord
  do begin
    update mwsacts ma  --zwalnianie z mwsacts
    set ma.mwsconstloc = null
    where ma.mwsord = :mwsord and ma.docgroup = :grupasped and ma.mwsconstloc = :cart;

    if (not exists(select first 1 1 from mwsacts m where m.mwsconstloc = :cart)) then begin  --usuwanie z mwsordcartcolor, wtedy gdy wozek nie jest przypisany do jakiegokolwiek mwsacts
      update mwsordcartcolours mw
        set mw.status = 2
        where mw.cart = :cart
         and mw.mwsord = :mwsord;
    end
  end
end^
SET TERM ; ^
