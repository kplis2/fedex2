--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYNCHRONIZE_BKACCOUNTS(
      PATTERN BKACCOUNTS_ID,
      DEST BKACCOUNTS_ID,
      INTERNAL SMALLINT_ID default 0)
   as
declare variable vsymbol bksymbol_id;
declare variable vdescript string60;
declare variable vbktype bkacctstypes_id;
declare variable vsaldotype saldotype_id;
declare variable vsettl settl_id;
declare variable vmulticurr multicurr_id;
declare variable vsttltype char(1);
declare variable vprd smallint_id;
declare variable vmultiprd smallint;
declare variable vrights string255;
declare variable vrightsgroup string255;
declare variable vdoopisu smallint_id;
begin
  --Procedura do synchronizacji dwóch kont. Jeżeli internal = 1 to update nie uruchomi mechanizmów synchronizacyjnych
  --jezeli konto docelowe bedzie objete synchronizacja. Domyslenie 0 bo jak ktos niecelowo cos zrobi zeby nie rozspójnic
  --danych ze wzorcem
  select symbol, descript, bktype, saldotype, settl, multicurr, sttltype,
    prd, multiprd, rights, rightsgroup, doopisu
    from bkaccounts
    where ref = :pattern
    into :vsymbol, :vdescript, :vbktype, :vsaldotype, :vsettl, :vmulticurr, :vsttltype,
      :vprd, :vmultiprd, :vrights, :vrightsgroup, :vdoopisu;
  update bkaccounts
    set symbol = :vsymbol, descript = :vdescript, bktype = :vbktype, saldotype = :vsaldotype,
      settl = :vsettl, multicurr = :vmulticurr, sttltype = :vsttltype, prd = :vprd,
      multiprd = :vmultiprd, rights = :vrights, rightsgroup = :vrightsgroup, doopisu = :vdoopisu,
      internalupdate = :internal
    where ref = :dest;
end^
SET TERM ; ^
