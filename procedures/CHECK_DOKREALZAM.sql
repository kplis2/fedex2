--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHECK_DOKREALZAM(
      DOKUMENT integer,
      TYP char(1) CHARACTER SET UTF8                           )
  returns (
      GRUPA integer)
   as
declare variable rejzam varchar(3);
declare variable klient integer;
declare variable dostawca integer;
declare variable magazyn varchar(3);
declare variable typp varchar(3);
declare variable cnt integer;
declare variable stan varchar(10);
declare variable pozcnt integer;
declare variable zamnag integer;
declare variable operzam varchar(10);
declare variable realizacja integer;
declare variable refzam integer;
declare variable realzamclose smallint_id;
begin
  grupa = 0;
  pozcnt = 0;
  if(:typ = 'M') then begin
    select KLIENT,DOSTAWCA,MAGAZYN,typ,zamowienie
      from DOKUMNAG where REF=:dokument
      into :klient,:dostawca,:magazyn,:typp,:zamnag;
    select DEFDOKUMMAG.realzamrej,DEFDOKUMMAG.realzamoper, DEFDOKUMMAG.realzamclose
      from DEFDOKUMMAG where MAGAZYN = :magazyn and TYP = :typp into :rejzam, :operzam, :realzamclose;
    if(exists(select first 1 1 from DOKUMPOZ where DOKUMENT = :dokument and (FROMZAM > 0 or FROMPOZZAM > 0))) then pozcnt = 1;
    if((:pozcnt is null or (:pozcnt = 0)) and (:zamnag is null or (:zamnag = 0))) then exit;
  end else begin
    select KLIENT, DOSTAWCA, TYP, STANOWISKO, FROMNAGZAM
      from NAGFAK where REF=:dokument
      into :klient, :dostawca,:typp,:stan, :zamnag;
    select REALZAMREJ, REALZAMOPER, realzamclose
      from STANTYPFAK where STANFAKTYCZNY = :stan and TYPFAK = :typp into :rejzam, :operzam, :realzamclose;
    if(exists(select first 1 1 from POZFAK where DOKUMENT = :dokument and (FROMZAM > 0 or FROMPOZZAM > 0))) then pozcnt = 1;
    if(:pozcnt is null or (:pozcnt = 0)) then exit;
  end
  /* Sprawdz, czy operacja jest realizacja. Jesli nie, to niedopusc do wykonania operacji, */
  /* bo system moglby podchwycic tez inne zamowienia klienta lub dostawcy */
  select coalesce(REALIZACJA,0)+coalesce(DYSPP,0)+coalesce(DYSPB,0)+coalesce(DYSPK,0)+
    coalesce(DYSPD,0)+coalesce(ASREALIZACJA,0)
    from DEFOPERZAM where SYMBOL=:operzam into :realizacja;
  if(:realizacja<=0) then exception UNIVERSAL 'Błąd konfiguracji. Operacja '||:operzam||' musi realizować zamówienie.';
  cnt = 0;
  if(:klient > 0) then begin
    if(exists(select first 1 1 from NAGZAM where NAGZAM.rejestr = :rejzam and (KLIENT = :klient or ref = :zamnag))) then cnt = 1;
  end else if(:dostawca > 0) then begin
    if(exists(select first 1 1 from NAGZAM where NAGZAM.REJESTR = :rejzam and ((DOSTAWCA = :dostawca) or (FAKTURANT = :dostawca)) or ref = :zamnag)) then cnt = 1;
  end else if(:typ = 'M') then begin
    if(exists(select first 1 1 from NAGZAM where NAGZAM.REJESTR = :rejzam and MAGAZYN = :MAGAZYN)) then cnt = 1;
  end
  if(:realzamclose = 1) then begin
    if(:cnt > 0) then begin
      execute procedure GRUPAZAM returning_values  :grupa;
      update NAGZAM set NAGZAM.grupa = :grupa where ref = :zamnag;
       if( not(:klient > 0 and :dostawca > 0) and :typ = 'M') then
        update NAGZAM set GRUPA = :grupa where NAGZAM.REJESTR = :rejzam and MAGAZYN = :MAGAZYN ;
    end
    if(:typ = 'M') then begin
      for select distinct ref
        from br_zamtomag(:dokument)
        where rejestr=:rejzam
        into :refzam
      do begin
        if(:grupa=0) then execute procedure GRUPAZAM returning_values :grupa;
        update nagzam set grupa=:grupa where ref=:refzam;
      end
    end else begin
      for select distinct ref
        from br_zamtofak(:dokument)
        where rejestr=:rejzam
        into :refzam
      do begin
        if(:grupa=0) then execute procedure GRUPAZAM returning_values :grupa;
        update nagzam set grupa=:grupa where ref=:refzam;
      end
    end
  end else begin
    if(:cnt > 0) then begin
      execute procedure GRUPAZAM returning_values  :grupa;
      if(:klient > 0) then
        update NAGZAM set NAGZAM.grupa = :grupa where NAGZAM.rejestr = :rejzam and KLIENT = :klient or ref = :zamnag  ;
      else if(:dostawca > 0) then
        update NAGZAM set GRUPA = :grupa where NAGZAM.REJESTR = :rejzam and ((DOSTAWCA = :dostawca) or (FAKTURANT =:dostawca )) or ref = :zamnag  ;
      else if(:typ = 'M') then
        update NAGZAM set GRUPA = :grupa where NAGZAM.REJESTR = :rejzam and MAGAZYN = :MAGAZYN ;
    end
    if(:typ = 'M') then begin
      for select distinct ref
        from br_zamtomag(:dokument)
        where rejestr=:rejzam
        into :refzam
      do begin
        if(:grupa=0) then execute procedure GRUPAZAM returning_values :grupa;
        update nagzam set grupa=:grupa where ref=:refzam;
      end
    end else begin
      for select distinct ref
        from br_zamtofak(:dokument)
        where rejestr=:rejzam
        into :refzam
      do begin
        if(:grupa=0) then execute procedure GRUPAZAM returning_values :grupa;
        update nagzam set grupa=:grupa where ref=:refzam;
      end
    end
  end
end^
SET TERM ; ^
