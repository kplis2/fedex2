--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_POW_GEN_MWSORD(
      WHIN varchar(3) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint)
   as
declare variable DOCID integer;
declare variable DOCGROUP integer;
declare variable MWSORD integer;
declare variable DOCPOSID integer;
declare variable STATUS3 smallint;
declare variable CNT integer;
declare variable ILLINIS varchar(100);
declare variable ILLINI integer;
declare variable RECALC smallint;
declare variable DNIWSTECZ integer;
declare variable TMPDATE date;
declare variable LISTYWYS smallint;
declare variable GENALLWAYS smallint;
begin
  dniwstecz = 30;
  status = 1;
  recalc = 1;
  genallways = null;
  tmpdate = current_date - dniwstecz;
  execute procedure get_config('MWSILOSCACTS',2) returning_values illinis;
  if (illinis is null or illinis = '') then
    illini = 0;
  else
    illini = cast(illinis as smallint);
--  illini = 1400;
  select count(a.ref)
    from mwsacts a
      left join mwsords o on (o.ref = a.mwsord)
    where o.wh = :whin and o.status < 5 and a.status < 3 and a.status > 0
      and a.mwsordtypedest = 1
      and o.takefullpal = 0 and (o.regtime > current_date - 14) and coalesce(a.ispal,0) <> 1
  into cnt;
  if (cnt is null) then cnt = 0;
  if (cnt > :illini) then genallways = 1;
  if (cnt <= 300) then
    recalc = 0;
  for
    select first 20 d.ref, d.grupasped, s.listywys
      from dokumnag d
        left join defdokummag dm on (dm.typ = d.typ and dm.magazyn = d.magazyn)
        left join sposdost s on (s.ref = d.sposdost)
      where coalesce(d.mwsmag,d.magazyn) = :whin
        and d.data > :tmpdate
        and d.wydania = 1
        and dm.mwsordwaiting = 1
        and ((d.akcept = 1 and d.mwsdisposition = 0)
          or (d.akcept = 0 and d.mwsdisposition = 1))
        and d.genmwsordsafter = 1
        --and (d.blokada = 0 or d.blokada = 4)
        and coalesce(d.blockmwsords,0) = 0
        and coalesce(d.wysylkadone,0) = 0
        and (coalesce(d.priorytet,5) = 0 or coalesce(:genallways,0) = 0 or s.mwsgenallways = 1 or (exists(select first 1 1 from mwsords o where o.docid = d.ref and o.mwsordtype = 6)))
      order by coalesce(s.mwsgenallways,0) desc, s.mwspriority desc, d.ref
    into docid, docgroup, :listywys
  do begin
    if (:listywys = 0) then recalc = 0;

    status3 = 0;
    execute procedure xk_mws_pow_mwsordswaiting_doc(:docid)
      returning_values :status3;
    if (status3 is null) then status3 = 0;
    mwsord = null;
    if (status3 = 1 or status3 = 2 or status3 = 5) then
    begin
      execute procedure XK_MWS_GEN_MWSORD_OUT(:docid,:docgroup,null,'M',null,null,:recalc,null)
        returning_values mwsord;
      for
        select p.ref
          from dokumpoz p
            left join dokumnag d on (d.ref = p.dokument)
            join towary t on (p.ktm = t.ktm)
            left join dokumpoz p1 on (p1.ref = p.alttopoz and p.fake = 1)
            left join towary t1 on (t1.ktm = p1.ktm)
          where p.dokument = :docid
            and p.genmwsordsafter = 0
            and ((coalesce(p.havefake,0) = 0 and coalesce(p.fake,0) = 0) or
                 (coalesce(p.fake,0) = 1 and t1.usluga <> 1 and coalesce(t1.altposmode,0) = 1))
            and ((d.mwsdisposition = 0 and p.fake = 0 and p.iloscl = coalesce(p.ilosconmwsacts,0)) or
                 ((d.mwsdisposition = 1 or p.fake = 1) and p.ilosc = coalesce(p.ilosconmwsacts,0)))
            and t.usluga <> 1
          into docposid
      do begin
        delete from mwsgoodsrefill where docposid = :docposid;
      end
    end
    else
      update dokumnag set genmwsordsafter = 0, mwsdocreal = 4 where ref = :docid;
  end
end^
SET TERM ; ^
