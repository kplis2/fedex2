--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NEOS_DOKUMENTY(
      AKTUOPER integer,
      DOKZLACZTABLE varchar(20) CHARACTER SET UTF8                           ,
      DOKZLACZREF integer,
      SEGREGATOR integer)
  returns (
      ID varchar(80) CHARACTER SET UTF8                           ,
      PARENT varchar(80) CHARACTER SET UTF8                           ,
      ICON varchar(20) CHARACTER SET UTF8                           ,
      DISPLAYNAME varchar(1024) CHARACTER SET UTF8                           ,
      SYMBOL varchar(255) CHARACTER SET UTF8                           ,
      OPIS varchar(1024) CHARACTER SET UTF8                           ,
      FORMATFIELD varchar(255) CHARACTER SET UTF8                           ,
      PARENTSEGR integer,
      DOKPLIKREF integer,
      DOKLINKREF integer,
      PLIK varchar(255) CHARACTER SET UTF8                           ,
      TYP varchar(40) CHARACTER SET UTF8                           ,
      DLSYMBOL varchar(60) CHARACTER SET UTF8                           ,
      DLSTATUS smallint,
      BLOKADA smallint,
      OPERBLOK integer,
      OPERBLOKN varchar(255) CHARACTER SET UTF8                           ,
      VERSIONING smallint,
      LOCALFILE varchar(255) CHARACTER SET UTF8                           ,
      COMPANY COMPANIES_ID)
   as
declare variable SQL varchar(2048);
declare variable ADMINISTRATOR smallint;
declare variable GRUPY varchar(1024);
declare variable WASDOKPLIK smallint;
declare variable DOKPLIKREFTMPT integer;
declare variable PARENTSEGRTMPT integer;
declare variable IDTMPT varchar(80);
declare variable DISPLAYNAMETMPT varchar(255);
declare variable TYPTMPT varchar(40);
declare variable BLOKADATMPT smallint;
declare variable OPERBLOKTMPT integer;
declare variable OPERBLOKNTMPT varchar(60);
declare variable VERSIONINGTMPT smallint;
declare variable LOCALFILETMPT varchar(255);
declare variable PLIKTMPT varchar(255);
declare variable DOKLINKREFTMPT integer;
declare variable DLSYMBOLTMPT varchar(60);
declare variable DLSTATUSTMPT smallint;
declare variable PETLA smallint;
declare variable segregatornazwa varchar(255);
begin
  aktuoper = coalesce(:aktuoper,0);
  dokzlacztable = coalesce(:dokzlacztable,'');
  dokzlaczref = coalesce(:dokzlaczref,0);
  if (:aktuoper = 0) then exit;
    select o.administrator, o.grupa from operator o where o.ref = :aktuoper
     into :administrator, :grupy;
  administrator = coalesce(:administrator,0);
  grupy = coalesce(:grupy,'');
  segregator = coalesce(:segregator,0);
  dokplikref =0;
  parentsegr =0;
  wasdokplik = 0;
  petla = 0;


    sql = 'select dp.ref, ''DOKPLIK;''|| dp.ref, ds.SEGREGATOR,
     coalesce(dp.symbol,''''), coalesce(dp.opis,''''), dl.ref,
      dl.symbol, dl.status, coalesce(dl.plik,''''), dt.nazwa,
     dp.blokada, o.ref, o.nazwa, dl.versioning, coalesce(dl.localfile,''''), dp.company, s.nazwa';

    sql = :sql || ' from dokpliksegregatory ds';

    if (:dokzlacztable <> '' and :dokzlaczref <> 0) then
      sql = :sql || ' join dokzlacz dz on (dz.dokument = ds.dokplik)';

    sql = :sql || ' join dokplik dp on (dp.ref = ds.dokplik)
    left join dokpliktyp dt on (dt.ref = dp.typ)
    left join segregator s on (s.ref = ds.segregator)
    left join doklink dl on (dl.dokplik = dp.ref and dl.status=2)
    left join operator o on (dp.operblok = o.ref)
    where ds.segdefault in (0,1)';

    if (:dokzlacztable <> '' and :dokzlaczref <> 0) then
      sql = :sql || ' and dz.ztable = ''' || :dokzlacztable || ''' and dz.zdokum = ' ||:dokzlaczref;

    if (:segregator > 0) then begin
      -- dla zalacznikow pokazujemy takze dokumenty w podsegregatorach
      if (:dokzlacztable <> '' and :dokzlaczref <> 0) then
        sql = :sql ||' and dp.segregatory like ''%;'||:segregator||';%''';
      else
        sql = :sql || ' and ds.segregator = '||:segregator;
    end
    sql = :sql || ' and (dt.rights = '';'' and dt.rightsgroup = '';''
         or dt.rights like ''%%;'||:aktuoper||';%%''
         or strmulticmp('';'||:grupy||';'', dt.rightsgroup) = 1)
         and (s.rights = '';'' and s.rightsgroup = '';''
         or s.rights like ''%%;'||:aktuoper||';%%''
         or strmulticmp('';'||:grupy||';'', s.rightsgroup) = 1)';

    for execute statement :sql
    into :dokplikref, :id, :parentsegr,
         :symbol, :opis, :doklinkref,
         :dlsymbol, :dlstatus, :plik, :typ,
         :blokada, :operblok, :operblokn, :versioning, :localfile, :company, :segregatornazwa
    do begin
      petla = 1;

      --if(coalesce(displaynametmpt,'')='') then displaynametmpt = 'Brak nazwy';

/*      if (:dokplikref <> 0 and :parentsegr <> 0 and
          (:dokplikreftmpt <> :dokplikref or :parentsegrtmpt <> :parentsegr))
      then begin
        if (:wasdokplik = 0) then
        begin
          doklinkref = null;
          dlsymbol = null;
          dlstatus = :dlstatustmpt;
          plik = null;
          versioning = null;
          localfile = null;
          id = :parent;
          parent = null;
          icon = 'MI_DOC';
          suspend;
        end
        wasdokplik = 0;
      end

      dokplikref = :dokplikreftmpt;
      parentsegr = :parentsegrtmpt;
      id = :idtmpt;
      typ = :typtmpt;
      displayname = :displaynametmpt;
      blokada = :blokadatmpt;
      operblok = :operbloktmpt;
      operblokn = :operblokntmpt;
      versioning = :versioningtmpt;
      localfile = :localfiletmpt;
      doklinkref = :doklinkreftmpt;
      dlsymbol = :dlsymboltmpt;
      dlstatus = :dlstatustmpt;
      plik = :pliktmpt;*/

      if (coalesce(:blokada,0)=1 and coalesce(:operblok,0)=:aktuoper) then
        formatfield = 'PLIK=*=@      *;DISPLAYNAME=*=%B*';
      else if (coalesce(:blokada,0)=1) then
        formatfield = 'PLIK=*=@      *;DISPLAYNAME=*=%B%TclRed*';
      else
        formatfield = 'PLIK=*=@      *';

      if (:doklinkref is null or coalesce(:dlstatus,0)=2) then
      begin
        -- glowny zalacznik albo dokument bez pliku
        --wasdokplik = 1;
        if(:symbol<>'') then displayname = :symbol;
        else displayname = :plik;
        if(:opis<>'') then begin
          if(:displayname<>'') then displayname = :displayname || ' | ';
          displayname = :displayname || substring(:opis from 1 for 100);
        end
        parent = '';
        icon = 'MI_DOC';
        -- dla zalacznikow dodaj nazwe segregatora bo wyswietlamy takze dokumenty w segregatorach podrzednych
        if (:dokzlacztable <> '' and :dokzlaczref <> 0) then begin
          displayname = :typ || ' w ' || :segregatornazwa || ': ' || :displayname;
        end else begin
          displayname = :typ || ': ' || :displayname;
        end
        if(:operblok is not null and :localfile<>'') then
          operblokn = :operblokn ||' w: '|| substring(:localfile from 1 for 100);
        suspend;
      end
      else begin
        -- inny zalacznik
        parent = :id;
        displayname = :plik;
        id = 'DOKLINK;' || :doklinkref;
        icon = 'MI_FASTENER';
        -- suspend;
      end
    end

/*    if (:petla = 1 and :wasdokplik = 0) then
    begin
      doklinkref = null;
      dlsymbol = null;
      dlstatus = :dlstatustmpt;
      plik = null;
      versioning = null;
      localfile = null;
      id = :parent;
      parent = null;
      icon = 'MI_DOC';
      suspend;
   end*/
end^
SET TERM ; ^
