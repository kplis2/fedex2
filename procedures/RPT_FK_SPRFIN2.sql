--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_SPRFIN2(
      FRHDR varchar(20) CHARACTER SET UTF8                           ,
      REF integer)
  returns (
      NUMBER integer,
      SYMBOL varchar(15) CHARACTER SET UTF8                           ,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      VALUE1 numeric(14,5),
      VALUE2 numeric(14,5),
      COLOURS smallint)
   as
declare variable GEN integer;
declare variable PREC integer;
declare variable AMOUNT numeric(15,2);
declare variable AMOUNT2 numeric(15,2);
declare variable TEMP1 numeric(18,6);
declare variable STRTEMP varchar(18);
declare variable GDZIEP integer;
declare variable ILMSC integer;
declare variable TMPVALUE1 varchar(20);
declare variable TMPVALUE2 varchar(20);
begin
  for
    select P.number, P.symbol, P.name, VP1.amount, VP2.amount, coalesce(FP.generalization,0), coalesce(FP.frprecision,0), P.colours      from frpsns P
        left join FRHDRS FP on (FP.symbol=P.frhdr)
          join frvpsns VP1 on (VP1.frpsn=P.ref and VP1.col=1 and VP1.frvhdr=:ref)
          join frvpsns VP2 on (VP2.frpsn=P.ref and VP2.col=2 and VP2.frvhdr=:ref)
      where P.frhdr=:frhdr
      order by P.number
    into :number, :symbol, :name, :amount, :amount2, :gen, :prec, :colours
  do begin
    value1  = 0;
    value2 = 0;
    if(amount <> 0) then begin
    temp1 = amount;
    temp1 = temp1 / power(10,gen);
    tmpvalue1 = cast(temp1 as varchar(20));
    gdziep = position('.' in  tmpvalue1);
    ilmsc = coalesce(char_length(tmpvalue1),0); -- [DG] XXX ZG119346
    strtemp = substring(tmpvalue1 from  gdziep+1 for  ilmsc);
    ilmsc = coalesce(char_length(strtemp),0); -- [DG] XXX ZG119346
    tmpvalue1 = substring(tmpvalue1 from  1 for  gdziep+prec);
    if(tmpvalue1 = '') then
      tmpvalue1 = NULL;
    value1 = cast(coalesce(tmpvalue1,0) as numeric(14,2));
    end
    temp1 = amount2;
    temp1 = temp1 / power(10,gen);
    tmpvalue2 = cast(temp1 as varchar(20));
    gdziep = position('.' in  tmpvalue2);
    ilmsc = coalesce(char_length(tmpvalue2),0); -- [DG] XXX ZG119346
    strtemp = substring(tmpvalue2 from  gdziep+1 for  ilmsc);
    ilmsc = coalesce(char_length(strtemp),0); -- [DG] XXX ZG119346
    tmpvalue2 = substring(tmpvalue2 from  1 for  gdziep+prec);
    if(tmpvalue2 = '') then
      tmpvalue2 = NULL;
    value2 = cast(coalesce(tmpvalue2,0) as numeric(14,5));

    suspend;
  end
end^
SET TERM ; ^
