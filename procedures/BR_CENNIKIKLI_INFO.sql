--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE BR_CENNIKIKLI_INFO(
      KLIENT integer,
      KTM varchar(80) CHARACTER SET UTF8                           ,
      WERSJAREF integer)
  returns (
      GRUPAKLI integer,
      CENNIK integer,
      CENANET numeric(14,4),
      CENABRU numeric(14,4),
      RABAT numeric(14,4),
      ISCENA smallint,
      MIARA varchar(10) CHARACTER SET UTF8                           ,
      CENANETR numeric(14,4),
      CENABRUR numeric(14,4),
      WALUTA varchar(3) CHARACTER SET UTF8                           )
   as
declare variable ODDZIAL varchar(20);
declare variable TESTCENNIK integer;
declare variable JEDN integer;
declare variable VAT numeric(14,2);
declare variable GRUPCENNIK integer;
declare variable NRWERSJI integer;
declare variable GRUPAKLIM integer;
declare variable PROMOCJA integer;
begin
  execute procedure getconfig('AKTUODDZIAL') returning_values :oddzial;

  select TOWARY.dc from TOWARY where KTM = :ktm into :jedn;
  select nrwersji from WERSJE where ref=:wersjaref into :nrwersji;
  select JEDN from TOWJEDn where ref=:jedn into :miara ;
  select vat.stawka from VAT join TOWARY on (TOWARY.VAT = VAT.GRUPA) where TOWARY.KTM = :ktm into :vat;
  if(:vat is null) then vat = 0;
  if(:Klient > 0) then begin
    cenanet = 0;
    cenabru = 0;
    rabat = 0;
    iscena = 0;
    cenanetr = 0;
    cenabrur = 0;
    cennik = 0;
    grupakli = 0;
    select GRUPYKLI.REF, GRUPYKLI.cennik from KLIENCI
    join GRUPYKLI on (GRUPYKLI.REF = KLIENCI.grupa)
    where KLIENCI.ref=:klient into :grupakli, :grupcennik;
    execute procedure cennik_znajdz(:grupcennik,:wersjaref,:jedn,:klient,oddzial,NULL,NULL) returning_values :cennik;

    if(:cennik > 0) then begin
      select cenanet, cenabru from cennik where CENNIK = :cennik and WERSJAREF = :wersjaref and JEDN = :jedn into :cenanet, :cenabru;
      execute procedure oblicz_rabat(:ktm,:nrwersji,:klient,1,:cenanet,'N','',:cennik,0,0) returning_values :rabat, :iscena, :waluta, :promocja;
    end
    if(:rabat is null) then rabat = 0;
    if(:iscena > 0) then
      cenanetr = :rabat;
    else
      cenanetr = :cenanet * (100-:rabat)/100;
    cenabrur = :cenanetr * (100+:vat)/100;
    exit;
  end
  for
    select ref, CENNIK
    from GRUPYKLI
    order by cennik
    into :grupakli, :grupcennik
  do begin
    cennik = 0;
    /* okrelenie cennika dla domylnej jednostki cennika*/
    for select CENNIK
        from DEFCENKLI
        where (GRUPAKLI=:grupakli and (ODDZIAL=:ODDZIAL or (ODDZIAL = '' or (ODDZIAL is null))))
        order by NUMER
        into :testcennik
    do begin
      if(:cennik = 0) then begin
        select CENNIK, cenanet, cenabru
        from CENNIK
        where CENNIK=:testcennik and WERSJAREF=:wersjaref and JEDN = :jedn and CENANET<>0
        into :cennik, :cenanet, :cenabru;
      end
    end
    if(:cennik = 0 or (:cennik is null)) then
      cennik = :grupcennik;
    if(:cennik > 0) then begin
      select CENANET, CENABRU from CENNIK where WERSJAREF = :wersjaref and CENNIK = :cennik into :cenanet, :cenabru;
      grupaklim = 0 - :grupakli;
      execute procedure OBLICZ_RABAT(:ktm, :nrwersji,:grupaklim, 1,:cenanet,'N','',:cennik,0,0) returning_values :rabat, :iscena, :waluta, :promocja;
      if(:rabat is null) then rabat = 0;
      if(:iscena > 0) then
        cenanetr = :rabat;
      else
        cenanetr = :cenanet * (100-:rabat)/100;
      cenabrur = :cenanetr * (100+:vat)/100;
      suspend;
    end
  end
end^
SET TERM ; ^
