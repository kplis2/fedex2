--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZFAK_KAUCJA_OPAKOWANIE(
      POZFAKREF integer,
      ILOSC integer,
      KLIENTOPK smallint)
  returns (
      STATUS integer)
   as
declare variable opakowanie varchar(255);
  declare variable ktm varchar(80);
  declare variable wersja integer;
  declare variable pozktm varchar(80);
  declare variable dokument integer;
  declare variable magazyn varchar(3);
  declare variable milosc numeric(14,4);
  declare variable iloscwopak varchar(255);
  declare variable oblilosc numeric(14,4);
  declare variable klient integer;
  declare variable jedn integer;
  declare variable wersjaref integer;
  declare variable cennik integer;
  declare variable cenanet numeric(14,4);
  declare variable cenabru numeric(14,4);
  declare variable bn char(1);
declare variable skad smallint;
begin
  status = 0;
  execute procedure GETCONFIG('OPAKOWANIE') returning_values :opakowanie;
  if(:opakowanie is null or (:opakowanie='')) then exit;
  select p.KTM, p.DOKUMENT, p.MAGAZYN, p.ILOSCM, n.klient, p.jedn, n.bn, n.skad
    from POZFAK p
    left join nagfak n on (n.ref = p.dokument)
    where p.REF=:pozfakref
    into :pozktm, :dokument, :magazyn, :milosc, :klient, :jedn, :bn, :skad;
  select ILOSCWOPAK from TOWARY where KTM=:pozktm into :iloscwopak;
  if (skad < 2) then
    for select TOWAKCES.AKTM,TOWAKCES.AWERSJA, TOWAKCES.AWERSJAREF
    from TOWAKCES
    left join TOWARY on (TOWARY.KTM=TOWAKCES.AKTM)
    where TOWAKCES.KTM=:pozktm and TOWAKCES.TYP=:opakowanie
    into :ktm, :wersja, :wersjaref
    do begin
      if (wersjaref is null) then
        select ref from wersje where ktm = :ktm and wersje.nrwersji = 0 into :wersjaref;
      /* jesli nie podano ilosci opakowan to ja oblicz */
      if(:ilosc is null or (:ilosc=0)) then begin
        /* jesli zdefiniowano ilosc w opakowaniu glownym na towarze to oblicz */
        if(:iloscwopak<>'' and :iloscwopak<>'0') then begin
          /* podziel ilosc magazynowa przez ilosc w opakowaniu */
          oblilosc = :milosc/cast(:iloscwopak as numeric(14,2));
          ilosc = cast(:oblilosc as integer);
          /* zaokraglij w gore */
          if(:oblilosc>:ilosc) then ilosc = :ilosc+1;
        end else begin
          ilosc = cast(:milosc as integer);
          if(:milosc>:ilosc) then ilosc = :ilosc+1;
        end
      end
      if (:klientopk = 2) then
      begin
        select gk.cennik from klienci k left join grupykli gk on gk.ref = k.grupa
          where k.ref = :klient into :cennik;
        select cenanet,cenabru from cennik where cennik = :cennik and wersjaref = :wersjaref
          into :cenanet,:cenabru;
      end
      if(cenanet is null) then cenanet = 0;
      if(:bn='N') then
        insert into pozfak(DOKUMENT,KTM,WERSJA,ILOSC,MAGAZYN,OPK,CENACEN,RABAT,POZFAKOPK,wersjaref)
        values(:dokument,:ktm,:wersja,:ilosc,:magazyn,1,:CENANET,0,:pozfakref,:wersjaref);
      else
        insert into pozfak(DOKUMENT,KTM,WERSJA,ILOSC,MAGAZYN,OPK,CENACEN,RABAT,POZFAKOPK,wersjaref)
        values(:dokument,:ktm,:wersja,:ilosc,:magazyn,1,:CENABRU,0,:pozfakref,:wersjaref);
      status = 1;
    end
end^
SET TERM ; ^
