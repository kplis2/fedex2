--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_CALC_EBKDOC_POSITION(
      EMPLOYEE integer,
      PAYROLL integer,
      EBKDOC integer,
      ECOLUMN integer,
      DEBIT integer,
      CREDIT integer,
      RATIO integer,
      ECOST integer)
   as
declare variable D_ACCOUNT ACCOUNT_ID;
declare variable D_SETTLEMENT varchar(40);
declare variable D_DESCRIPT varchar(1024);
declare variable C_ACCOUNT ACCOUNT_ID;
declare variable C_SETTLEMENT varchar(40);
declare variable C_DESCRIPT varchar(1024);
declare variable D_OPTIMALIZE smallint;
declare variable C_OPTIMALIZE smallint;
declare variable D_REF integer;
declare variable C_REF integer;
declare variable PVALUE numeric(16,2);
declare variable STATEMENT_TEXT varchar(255);
declare variable FROMDATE date;
declare variable TODATE date;
declare variable PCOSTS numeric(14,2);
declare variable PVALUE2 numeric(14,2);
declare variable PVALUE3 numeric(14,2);
declare variable D_ACCOUNT2 ACCOUNT_ID;
declare variable SUMA numeric(14,2);
begin
  execute procedure efunc_prdates(payroll, 0)
    returning_values fromdate, todate;

  if (not exists(select * from rdb$procedures
        where rdb$procedure_name = 'XX_ED_ALGORITHM_' || :debit
           || '_A' or rdb$procedure_name = 'XX_ED_ALGORITHM_' || :debit
           || '_R' or rdb$procedure_name = 'XX_ED_ALGORITHM_' || :debit
           || '_D')) then
  begin
    update edalgorithms set body1 = ''
      where ref = :debit;
  end

  if (not exists(select * from rdb$procedures
        where rdb$procedure_name = 'XX_ED_ALGORITHM_' || :credit
           || '_A' or rdb$procedure_name = 'XX_ED_ALGORITHM_' || :credit
           || '_R' or rdb$procedure_name = 'XX_ED_ALGORITHM_' || :credit
           || '_D' )) then
  begin
    update edalgorithms set body1 = ''
      where ref = :credit;
  end

  pvalue = null;
  pvalue2 = null;
  pvalue3 = null;

  select sum(pvalue)
    from eprpos
    where ecolumn = :ecolumn and payroll = :payroll and employee = :employee
    into :pvalue;

  if (pvalue is null) then
    pvalue = 0;
  if (ratio = 0) then
    pvalue = -pvalue;

  if (debit is not null) then
  begin
    statement_text = 'select ret from XX_ED_ALGORITHM_' || debit || '_A' ||
        '(' || employee || ', ' || payroll || ')';
    execute statement statement_text into :d_account;

    statement_text = 'select ret from XX_ED_ALGORITHM_' || debit || '_R' ||
        '(' || employee || ', ' || payroll || ')';
    execute statement statement_text into :d_settlement;

    statement_text = 'select ret from XX_ED_ALGORITHM_' || debit || '_D' ||
        '(' || employee || ', ' || payroll || ')';
    execute statement statement_text into :d_descript;

    select optimalize
      from edalgorithms
      where ref = :debit
      into d_optimalize;

    select optimalize
      from edalgorithms
      where ref = :credit
      into c_optimalize;

    pvalue2 = pvalue;
    pvalue3 = pvalue;

    if (ecost = 1) then
    begin
      pcosts = 0;
      d_account2 = d_account;
      suma = 0;
      for
        select substring(P.bksymbol from  1 for  6)||:d_account2||substring(P.bksymbol from 15 for 35), coalesce(percent, 0)
          from ecostspos P join ecosts C on (C.ref = P.ecost)
          where C.employee = :employee
            and C.fromdate <= :todate and (C.todate >= :todate or C.todate is null)
          into :d_account, :pcosts
      do begin
        if (d_account is null) then
        begin
          d_account = '1';
          pcosts = 100;
        end
        pcosts = coalesce(pcosts, 0);
        suma = suma + pcosts;
        pvalue = pvalue2 * pcosts / 100.00;
        pvalue3 = pvalue3 - pvalue;

        pvalue = coalesce(pvalue, 0);
        if (d_optimalize = 1) then
        begin
          select max(ref)
          from ebkdocpos
          where ebkdoc = :ebkdoc and account = :d_account and descript = :d_descript
        into :d_ref;

        if (d_ref is not null) then
          update ebkdocpos set debit = debit + :pvalue where ref = :d_ref;
        else
          insert into ebkdocpos (ebkdoc, account, debit, credit, descript)
            values (:ebkdoc, :d_account, :pvalue, 0, :d_descript);
        end else
        begin
        if (pvalue <> 0) then
          insert into ebkdocpos (ebkdoc, account, debit, credit, descript)
            values (:ebkdoc, :d_account, :pvalue, 0, :d_descript);
        end
      end
      if (d_optimalize = 1) then
      begin
        select max(ref)
        from ebkdocpos
          where ebkdoc = :ebkdoc and account = :d_account and descript = :d_descript
      into :d_ref;

      if (d_ref is not null) then
        update ebkdocpos set debit = debit + :pvalue3 where ref = :d_ref;
      else
        insert into ebkdocpos (ebkdoc, account, debit, credit, descript)
          values (:ebkdoc, :d_account, :pvalue3, 0, :d_descript);
      end else
      begin
      if (pvalue3 <> 0) then
        insert into ebkdocpos (ebkdoc, account, debit, credit, descript)
          values (:ebkdoc, :d_account, :pvalue3, 0, :d_descript);
      end
    end
    else if (d_optimalize = 1) then
        begin
          select max(ref)
          from ebkdocpos
          where ebkdoc = :ebkdoc and account = :d_account and descript = :d_descript
        into :d_ref;

        if (d_ref is not null) then
          update ebkdocpos set debit = debit + :pvalue where ref = :d_ref;
        else
          insert into ebkdocpos (ebkdoc, account, debit, credit, descript)
            values (:ebkdoc, :d_account, :pvalue, 0, :d_descript);
        end else
        begin
        if (pvalue <> 0) then
          insert into ebkdocpos (ebkdoc, account, debit, credit, descript)
            values (:ebkdoc, :d_account, :pvalue, 0, :d_descript);
        end

  end
--brak konta
  if (d_account = '30111100 ') then exception test_break 'Brak konta dla: ' || :employee;
  if (credit is not null) then
  begin
    statement_text = 'select ret from XX_ED_ALGORITHM_' || credit || '_A' ||
        '(' || employee || ', ' || payroll || ')';
    execute statement statement_text into :c_account;

    statement_text = 'select ret from XX_ED_ALGORITHM_' || credit || '_R' ||
        '(' || employee || ', ' || payroll || ')';
    execute statement statement_text into :c_settlement;

    statement_text = 'select ret from XX_ED_ALGORITHM_' || credit || '_D' ||
        '(' || employee || ', ' || payroll || ')';
    execute statement statement_text into :c_descript;

    if (c_optimalize = 1) then
    begin
      select max(ref)
        from ebkdocpos
        where ebkdoc = :ebkdoc and account = :c_account and descript = :c_descript and credit is not null
        into :c_ref;
      if (c_ref is not null) then
        update ebkdocpos set credit = credit + :pvalue2 where ref = :c_ref;
      else
        insert into ebkdocpos (ebkdoc, account, credit, debit, descript)
          values (:ebkdoc, :c_account, :pvalue2, 0, :c_descript);
    end else
    begin
      if (pvalue2 <> 0) then
        insert into ebkdocpos (ebkdoc, account, credit, debit, descript)
          values (:ebkdoc, :c_account, :pvalue2, 0, :c_descript);
    end
  end
end^
SET TERM ; ^
