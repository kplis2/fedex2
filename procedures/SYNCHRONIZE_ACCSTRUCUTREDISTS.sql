--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYNCHRONIZE_ACCSTRUCUTREDISTS(
      PATTERN ACCSTRUCTUREDIST,
      DEST ACCSTRUCTUREDIST,
      INTERNAL SMALLINT_ID default 0)
   as
declare variable vnumber integer_id;
declare variable vdistdef integer;
declare variable vdistfilter string255;
declare variable votable string20;
declare variable voref integer_id;
declare variable vord smallint_id;
begin
  --Procedura do synchronizacji dwóch dowiązań wyróżnikowych. Jeżeli internal = 1 to update nie
  -- uruchomi mechanizmów synchronizacyjnych jezeli konto dowiązanie wyróżnikowe bedzie objete synchronizacja.
  --Domyslenie 0 bo jak ktos niecelowo cos zrobi zeby nie rozspójnic danych ze wzorcem
  select number, distdef, distfilter, ord
    from accstructuredists
    where ref = :pattern
    into :vnumber, :vdistdef, :vdistfilter, :vord;
  update accstructuredists a
    set a.number = :vnumber, a.distdef = :vdistdef, a.distfilter = :vdistfilter,
      a.ord = :vord, a.internalupdate = :internal
    where ref = :dest;
end^
SET TERM ; ^
