--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ROUND_TIME(
      INPUTTIME timestamp,
      ACCURACY smallint)
  returns (
      OUTPUTTIME timestamp)
   as
declare variable minutes smallint;
  declare variable hours smallint;
  declare variable seconds smallint;
  declare variable accuracy_sec integer;
begin
  --DS: funkcja uzywana w RCPie do zaokraglania czasu
  --dziala w przedziale 1-60 minutZ
  minutes = extract(minute from inputtime);
  seconds = extract(second from inputtime);
  hours = extract(hour from inputtime);
  inputtime = cast(inputtime as date);
  seconds = minutes * 60 + seconds;
  accuracy_sec = accuracy * 60;
  if (mod(seconds,accuracy_sec) < accuracy_sec - mod(seconds,accuracy_sec)) then
    minutes = minutes/ accuracy  * accuracy;
  else minutes = (minutes + accuracy)/ accuracy * accuracy;
  if (minutes = 60) then
  begin
    minutes = 0;
    if (hours < 23) then
      hours = hours +1;
    else begin
      hours = 0;
      inputtime = inputtime + 1;
    end
  end
  outputtime = cast(inputtime as date) || ' ' ||cast(hours as varchar(2)) ||':'|| cast(minutes as varchar(2));
  suspend;
end^
SET TERM ; ^
