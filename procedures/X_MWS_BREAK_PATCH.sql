--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_BREAK_PATCH(
      MWSORD integer)
  returns (
      STATUS smallint)
   as
declare variable wh string3;
declare variable cnt int;
begin

  select count(ref) from mwsacts where mwsord = :mwsord and status in (1, 2) into :cnt;

  if (coalesce(cnt,0) < 2) then
    status = 0;
  else
    status = 1;

end^
SET TERM ; ^
