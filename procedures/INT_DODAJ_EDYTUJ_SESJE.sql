--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_DODAJ_EDYTUJ_SESJE(
      REGULA STRING255,
      TABELA STRING255,
      PROCEDURA STRING255,
      ZRODLO ZRODLA_ID,
      KIERUNEK SMALLINT_ID,
      SESJA_REF_IN SESJE_ID = null)
  returns (
      SESJA_REF_OUT SESJE_ID)
   as
begin
exception universal regula||tabela||procedura||zrodlo||kierunek||sesja_ref_in;
  update or insert into int_sesje (ref, regula, tabela, procedura, zrodlo, kierunek)
    values(:sesja_ref_in, :regula, :tabela, :procedura, :zrodlo, :kierunek)
    matching (ref)
    returning ref into :sesja_ref_out;
  suspend;
end^
SET TERM ; ^
