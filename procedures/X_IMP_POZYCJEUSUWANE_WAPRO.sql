--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_IMP_POZYCJEUSUWANE_WAPRO(
      TYP_DEL INTEGER_ID,
      NAZWA_TABELI STRING100,
      POZ_WAPRO INTEGER_ID,
      POZ_SENTE KTM_ID)
   as
declare variable status integer_id;
begin
    status = 10; -- wg dokumentacji 10 oznacza dokument do przertworzenia czyli do exportu do wapro
  insert into X_IMP_POZYCJEUSUWANE (TYP,TABELA, ID_POZYCJI,ID_SENTE, STATUS, DATAPRZET)
  values (:TYP_DEL,:nazwa_tabeli ,:POZ_WAPRO,:POZ_SENTE, :status, current_timestamp);

end^
SET TERM ; ^
