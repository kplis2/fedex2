--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CHECK_LOGGED(
      USERID integer)
  returns (
      STATUS integer,
      APPLICATIONS varchar(255) CHARACTER SET UTF8                           )
   as
declare variable app varchar(255);
begin
  status = 0;
  applications = '';
  app = '';
  for select application from s_sessions
    where userid = :userid and number=0
    into :app
  do begin
    status = 1;
    if(:app is null) then app = '';
    if(:applications<>'') then applications = :applications||';';
    applications = :applications||:app;
  end
end^
SET TERM ; ^
