--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SRV_DESCRIPT(
      REF integer)
  returns (
      LABEL varchar(255) CHARACTER SET UTF8                           ,
      STR varchar(255) CHARACTER SET UTF8                           )
   as
declare variable contrtype integer;
declare variable symbol varchar(255);
declare variable proc varchar(255);
declare variable fieldtype integer;
declare variable num numeric(14,2);
begin
  contrtype = 0;
  select contrtype from srvrequests where ref=:ref into :contrtype;
  for select descript,symbol,fieldtype
  from srvgooddef
  where contrtype=:contrtype
  order by number
  into :label,:symbol,:fieldtype
  do begin
    if(:symbol like '%DICT') then symbol = replace(:symbol, 'DICT', '');
    proc = 'select '||:symbol||' from srvrequests where ref='||cast(:ref as varchar(20));
    if(:fieldtype=0) then begin
      execute statement :proc into :num;
      if(:num=1) then str = 'TAK'; else str = 'NIE';
    end else if(:fieldtype=1) then begin
      execute statement :proc into :num;
      if(:num=cast(:num as integer)) then str = cast(:num as integer);
      else str = cast(:num as varchar(255));
    end else begin
      execute statement :proc into :str;
    end
    suspend;
  end
end^
SET TERM ; ^
