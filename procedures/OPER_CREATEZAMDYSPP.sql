--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_CREATEZAMDYSPP(
      ZAM integer,
      HISTZAM integer,
      DEFOPER varchar(5) CHARACTER SET UTF8                           ,
      MAGAZYN varchar(5) CHARACTER SET UTF8                           ,
      NDATAMAG date)
  returns (
      DOSTZAM integer,
      SYMBOLZAM varchar(20) CHARACTER SET UTF8                           )
   as
declare variable cenanet numeric(14,4);
declare variable jedn integer;
declare variable ktm varchar(40);
declare variable przelicz numeric(14,4);
declare variable pozzamdref integer;
declare variable datamag timestamp;
declare variable dpnewtyp char(3);
declare variable dpnewrej char(3);
begin
  symbolzam = '';
  select DEFOPERZAM.dpnewtyp, DEFOPERZAM.dpnewrej
  from DEFOPERZAM where SYMBOL = :defoper
  into :dpnewtyp,:dpnewrej;
  if(coalesce(:dpnewtyp,'')='' or coalesce(:dpnewrej,'')='') then exit;

  if(:ndatamag is null) then
    datamag = current_timestamp(0);
  execute procedure GEN_REF('NAGZAM') returning_values :dostzam;
  insert into NAGZAM(REF, TYPZAM, REJESTR, MAGAZYN, MAG2, STAN, TYP, DOSTAWCA, REFZRODL, symbzrodl,
        TERMDOST, OPERATOR, UWAGI,
        ODBIORCA,ODBIORCAID,DULICA,DNRDOMU,DNRLOKALU,DMIASTO,DKODP,DATAWE,PRDEPART,BN,
        klient , uzykli, platnik, sposdost, sposdostauto, trasadost, wysylka, wysylkadod
        , uwagiwewn, uwagisped, kosztdostbez, kosztdost, kosztdostroz, kosztdostplat,
        flagi, flagisped, KURS, WALUTA, tabkurs, zlecnazwa, znakzewn)
  select :dostzam, :dpnewtyp, :dpnewrej, MAGAZYN, :magazyn, 'N', 0, NULL, NAGZAM.REF, NAGZAM.id,
        TERMDOST, OPERATOR, UWAGI,
        ODBIORCA,ODBIORCAID,DULICA,DNRDOMU,DNRLOKALU,DMIASTO,DKODP, :datamag,PRDEPART,BN,
        klient , uzykli, platnik,  sposdost, sposdostauto, trasadost, nagzam.wysylka, wysylkadod
        , uwagiwewn, uwagisped,kosztdostbez, kosztdost, kosztdostroz, kosztdostplat,
        flagi, flagisped,KURS, WALUTA, tabkurs, zlecnazwa, znakzewn
  from NAGZAM
  where NAGZAM.REF = :zam;
  /*zaloznie pozycji zamówienia*/
  for select POZZAMDYSP.REF, POZZAM.JEDN, POZZAM.KTM
  from POZZAM join POZZAMDYSP on (POZZAMDYSP.pozzam = POZZAM.REF)
  where POZZAM.zamowienie = :zam and POZZAMDYSP.magazyn = :magazyn
  order by POZZAM.NUMER
  into :pozzamdref, :jedn, :ktm
  do begin
    insert into POZZAM(ZAMOWIENIE, KTM, WERSJAREF, MAGAZYN, mag2, JEDNO, JEDN, ILOSCO, ILOSC, ILOSCM, GENPOZREF,
      cenacen, CENANET, cenabru, rabat, opk, walcen, prec)
    select :dostzam,  POZZAM.KTM, POZZAM.wersjaref,  POZZAM.magazyn, :magazyn,
      POZZAM.jedno, POZZAM.JEDN, POZzamdysp.ilosco, pozzamdysp.ilosc, pozzamdysp.iloscm, POZZAM.REF,
      POZZAM.CENACEN, POZZAM.CENANET, POZZAM.CENABRU, pozzam.rabat, pozzam.opk, pozzam.walcen, pozzam.prec
      from POZZAMDYSP join POZZAM on (POZZAMDYSP.pozzam = POZZAM.REF)
      where POZZAMDYSP.REF = :pozzamdref;
  end
end^
SET TERM ; ^
