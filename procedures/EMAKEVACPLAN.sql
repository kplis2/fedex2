--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EMAKEVACPLAN(
      EMPLREF integer,
      PYEAR integer,
      PTYPE integer)
  returns (
      F1 varchar(24) CHARACTER SET UTF8                           ,
      T1 varchar(24) CHARACTER SET UTF8                           )
   as
declare variable tmp integer;
declare variable fd timestamp;
declare variable td timestamp;
declare variable m1 integer;
declare variable m2 integer;
declare variable d1 char(2);
declare variable d2 char(2);
declare variable f2 varchar(24);
declare variable t2 varchar(24);
declare variable f3 varchar(24);
declare variable t3 varchar(24);
declare variable f4 varchar(24);
declare variable t4 varchar(24);
declare variable f5 varchar(24);
declare variable t5 varchar(24);
begin

  if (coalesce(pyear,0) = 0) then
    exception universal 'Nieokreślony rok dla planu urlopowego';

  f1='                        ';  t1='                        ';
  f2='                        ';  t2='                        ';
  f3='                        ';  t3='                        ';
  f4='                        ';  t4='                        ';
  f5='                        ';  t5='                        ';
  for
    select fromdate, todate from evacplan
      where employee = :emplref
        and pyear = :pyear
        and (state > 0 or vtype = 0) /*tylko plan!*/
        and ((:ptype = 1 and state = 0) --niezatwierdzony
          or (:ptype = 2 and state > 0 and state < 3) --zatwierdzony
          or (:ptype = 3 and state = 3)  --odrzucone
          or (:ptype = 4 and state < 2)  --niezrealizowany
          or (:ptype = 5 and state = 2)  --zrealizowany
          or (:ptype < 1 ))              --wszystko
      order by fromdate
      into fd, td
  do begin
    m1 = extract(month from :fd);
    m2 = extract(month from :td);
    tmp = extract(day from :fd); if (tmp<10) then d1='0'||tmp; else d1=tmp;
    tmp = extract(day from :td); if (tmp<10) then d2='0'||tmp; else d2=tmp;

    if (substring(f1 from  m1*2 for  m1*2+2)='  ' and substring(t1 from  m2*2 for  m2*2+2)='  ') then
      execute procedure emakevacplan_ins2(f1, t1, d1, d2, m1, m2 ) returning_values f1, t1;
    else if (substring(f2 from  m1*2 for  m1*2+2)='  ' and substring(t2 from  m2*2 for  m2*2+2)='  ') then
      execute procedure emakevacplan_ins2(f2, t2, d1, d2, m1, m2 ) returning_values f2, t2;
    else if (substring(f3 from  m1*2 for  m1*2+2)='  ' and substring(t3 from  m2*2 for  m2*2+2)='  ') then
      execute procedure emakevacplan_ins2(f3, t3, d1, d2, m1, m2 ) returning_values f3, t3;
    else if (substring(f4 from  m1*2 for  m1*2+2)='  ' and substring(t4 from  m2*2 for  m2*2+2)='  ') then
      execute procedure emakevacplan_ins2(f4, t4, d1, d2, m1, m2 ) returning_values f4, t4;
    else if (substring(f5 from  m1*2 for  m1*2+2)='  ' and substring(t5 from  m2*2 for  m2*2+2)='  ') then
      execute procedure emakevacplan_ins2(f5, t5, d1, d2, m1, m2 ) returning_values f5, t5;
  end
  if (f1<>'                        ') then suspend;
  f1 = f2; t1 = t2; if (f1<>'                        ') then suspend;
  f1 = f3; t1 = t3; if (f1<>'                        ') then suspend;
  f1 = f4; t1 = t4; if (f1<>'                        ') then suspend;
  f1 = f5; t1 = t5; if (f1<>'                        ') then suspend;
end^
SET TERM ; ^
