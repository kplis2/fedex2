--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TIMESTAMP_DIFF_TOSTRING(
      MINUEND timestamp,
      SUBTRAHEND timestamp)
  returns (
      DIFFERENCE varchar(20) CHARACTER SET UTF8                           )
   as
declare variable roznica double precision;
declare variable sekundy integer;
declare variable minuty integer;
declare variable godziny integer;
begin
  roznica = :subtrahend - :minuend;
  roznica = :roznica * 86400;
  if (:roznica >= 60) then
  begin
    select r.zaokraglenie
      from ROUND_DOWNUP(:roznica/60,1,0) r
      into :minuty;
    sekundy = mod (:roznica,60);
    godziny = 0;
  end
  if (:roznica > 3600) then
  begin
    select r.zaokraglenie
      from ROUND_DOWNUP(:minuty/60,1,0) r
      into godziny;
    minuty = mod(:minuty,60);
  end
  if (:roznica < 60) then
  begin
    godziny = 0;
    minuty = 0;
    sekundy = :roznica;
  end
  difference = substring('0'||:godziny from coalesce(char_length('0'||:godziny),0)-1 for coalesce(char_length('0'||:godziny),0))||':' -- [DG] XXX ZG119346
  ||substring('0'||:minuty from coalesce(char_length('0'||:minuty),0)-1 for coalesce(char_length('0'||:minuty),0))||':' -- [DG] XXX ZG119346
  ||substring('0'||:sekundy from coalesce(char_length('0'||:sekundy),0)-1 for coalesce(char_length('0'||:sekundy),0)); -- [DG] XXX ZG119346
  suspend;
end^
SET TERM ; ^
