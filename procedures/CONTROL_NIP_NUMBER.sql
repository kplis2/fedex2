--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CONTROL_NIP_NUMBER(
      NIP varchar(40) CHARACTER SET UTF8                           )
  returns (
      CORRECT integer,
      MSG varchar(60) CHARACTER SET UTF8                           )
   as
declare variable total integer;
declare variable i integer;
declare variable digit integer;
declare variable scales integer;
declare variable mod integer;
declare variable resultmod integer;
begin

  if (:nip is NULL or :nip = '') then begin
    msg = ''; correct = 1;
  end else begin
    msg = '';
    nip = replace(:nip,  '-',  '');
    correct = 0;
    mod = 11;
    total = 0;
    if((coalesce(char_length(:nip),0) = 10) and (:nip <> '' or :nip is not null)) then begin -- [DG] XXX ZG119346
      i = 1;
      while (i < 10) do begin
        if (i = 1) then begin digit = substring(:nip from 1 for 1 ); scales = 6; end
        if (i = 2) then begin digit = substring(:nip from 2 for 1 ); scales = 5; end
        if (i = 3) then begin digit = substring(:nip from 3 for 1 ); scales = 7; end
        if (i = 4) then begin digit = substring(:nip from 4 for 1 ); scales = 2; end
        if (i = 5) then begin digit = substring(:nip from 5 for 1 ); scales = 3; end
        if (i = 6) then begin digit = substring(:nip from 6 for 1 ); scales = 4; end
        if (i = 7) then begin digit = substring(:nip from 7 for 1 ); scales = 5; end
        if (i = 8) then begin digit = substring(:nip from 8 for 1 ); scales = 6; end
        if (i = 9) then begin digit = substring(:nip from 9 for 1 ); scales = 7; end
        total = total + (scales * digit);
        i = i + 1;
      end
      resultmod = mod(total,11);
      if(:resultmod=10) then resultmod = 0;
      digit = (substring(:NIP from 10 for 1 ));

      if (:digit = :resultmod) then
      begin
        correct = 1;
      end else begin
        msg = 'Nie zgadza się cyfra kontrolna';
        correct = 0;
      end
    end else begin
      msg = 'Nieprawidłowa ilość cyfr';
      correct = 0;
    end
  end
  suspend;
end^
SET TERM ; ^
