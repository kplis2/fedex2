--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INSERT_DECREEMATCHING(
      DECREE DECREES_ID,
      CREDIT CT_AMOUNT,
      DEBIT DT_AMOUNT,
      NEWRECORD SMALLINT_ID = 0)
   as
declare variable COMPANY COMPANIES_ID;
declare variable BKACCOUNT BKACCOUNTS_ID;
declare variable MATCHINGSLODEF SLO_ID;
declare variable MATCHINGSLOPOZ SLOPOZ_ID;
declare variable MATCHINGLONAZWA STRING;
declare variable MATCHINGSLOKOD STRING40;
declare variable MATCHINGSYMBOL STRING40;
declare variable MREF DECREEMATCHINGS_ID;
begin
  if (:credit is null) then credit = 0;
  if (:debit is null) then debit = 0;
  if (:credit > 0 and :debit > 0) then
    exception decreematching_error 'Kwota Wn i Ma nie mogą wystąpić razem !';
  execute procedure decree_matching_params(:decree)
    returning_values(:company, :bkaccount, :matchingslodef, :matchingslopoz, :matchinglonazwa, :matchingslokod, :matchingsymbol);
  mref = null;
  -- nie robimy update na rekordach juz rozliczonych
  -- ale na potrzeby dzielenia rekordu doszla opcja bez update istniejacego
  if (newrecord = 0) then
    select first 1 m.ref
      from decreematchings m
      where m.decree = :decree
      order by case when m.decreematchinggroup is null then 0 else 1 end, m.ref
      into :mref;
  if (:mref is null) then
    insert into decreematchings (company, decree, bkaccount, slodef, slopoz, matchingsymbol, slonazwa, slokod, credit, debit)
      values (:company, :decree, :bkaccount, :matchingslodef, :matchingslopoz, :matchingsymbol, :matchinglonazwa, :matchingslokod, :credit,:debit);
  else 
    update decreematchings m set m.credit = m.credit + :credit, m.debit = m.debit + :debit where m.ref = :mref;
end^
SET TERM ; ^
