--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CTI_SHOWCONNECT(
      MPHONES varchar(255) CHARACTER SET UTF8                           ,
      TRYB smallint)
  returns (
      INFO varchar(4098) CHARACTER SET UTF8                           ,
      SLODEF integer,
      SLOPOZ integer,
      BNTIDENT varchar(80) CHARACTER SET UTF8                           ,
      LINES integer,
      GRIDCOLOUR varchar(40) CHARACTER SET UTF8                           ,
      LASTUSERTEXT varchar(255) CHARACTER SET UTF8                           ,
      STABLE varchar(31) CHARACTER SET UTF8                           ,
      CPODMIOT integer,
      INFOSMALL varchar(2048) CHARACTER SET UTF8                           ,
      LINESSMALL integer,
      PHONENUM varchar(30) CHARACTER SET UTF8                           ,
      REF integer,
      GROUPNUMBER smallint,
      ORGPHONENUM varchar(40) CHARACTER SET UTF8                           )
   as
declare variable k varchar(30);
declare variable len integer;
declare variable buf varchar(80);
declare variable ch char(1);
declare variable singleline smallint;
begin
  ref = 0;
  singleline = 0;
  groupnumber = 0;
  for
  select OUTSTRING from PARSE_LINES(:mphones,';')
  into phonenum
  do begin
    orgphonenum = phonenum;
    --odciecie przednich zer
    len = coalesce(char_length(:phonenum),0); -- [DG] XXX ZG119346
    while(:len > 0) do
    begin
      ch = substring(:phonenum from 1 for 1);
      if(:ch = '0' and :len > 1) then
        phonenum = substring(:phonenum from 2 for :len);
      else
        len = 0;
      len = len - 1;
    end
    --przepisanie cyft w odwrotnej kolenosci
    buf = '';
    len = coalesce(char_length(:phonenum),0); -- [DG] XXX ZG119346
    while(:len > 0) do
    begin
      ch = substring(:phonenum from 1 for 1);
      if(:len > 1) then
        phonenum = substring(:phonenum from 2 for :len);
      buf = :ch||:buf;
      len = len - 1;
    end
    if(:ref = 0 and :tryb = 1) then -- male okno, pierwszy numer
      singleline = 1;
    if(:ref > 0) then
      groupnumber = 1;
    if(:phonenum != '') then begin
      for select info,  SLODEF, SLOPOZ, bntident, lines, gridcolour,  lastusertext,
         stable, cpodmiot, infosmall, linessmall, phonenum
      from xk_cti_showconnect(:buf, :ref, :singleline, :groupnumber) s
      into :info, :slodef, :slopoz, :bntident, :lines, :gridcolour, :lastusertext,
        :stable, :cpodmiot, :infosmall, :linessmall, :phonenum
      do begin
        ref = ref + 1;
        if(singleline = 0) then
          suspend;
      end
      if(singleline = 1) then
        suspend;
    end
    groupnumber = 1;
    singleline = 0;
  end
END^
SET TERM ; ^
