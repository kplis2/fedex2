--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMNAG_UPDATE_DOKMAG_DOSTAWA(
      REFDOKUMNAG integer,
      DOSTAWCA integer,
      DATA timestamp,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      DATAWAZN timestamp,
      POZREF integer)
   as
declare variable DOSTSYMBFAK varchar(20);
declare variable SYMBFAK varchar(80);
declare variable SYMBOL varchar(120);
declare variable SKROT varchar(60);  --XXX ZG133796 MKD
declare variable SYMBKONC varchar(120);
declare variable WERSJAREF integer;
declare variable PARAMN1 numeric(14,4);
declare variable PARAMN2 numeric(14,4);
declare variable PARAMN3 numeric(14,4);
declare variable PARAMN4 numeric(14,4);
declare variable PARAMN5 numeric(14,4);
declare variable PARAMN6 numeric(14,4);
declare variable PARAMN7 numeric(14,4);
declare variable PARAMN8 numeric(14,4);
declare variable PARAMN9 numeric(14,4);
declare variable PARAMN10 numeric(14,4);
declare variable PARAMN11 numeric(14,4);
declare variable PARAMN12 numeric(14,4);
declare variable PARAMS1 varchar(255);
declare variable PARAMS2 varchar(255);
declare variable PARAMS3 varchar(255);
declare variable PARAMS4 varchar(255);
declare variable PARAMS5 varchar(255);
declare variable PARAMS6 varchar(255);
declare variable PARAMS7 varchar(255);
declare variable PARAMS8 varchar(255);
declare variable PARAMS9 varchar(255);
declare variable PARAMS10 varchar(255);
declare variable PARAMS11 varchar(255);
declare variable PARAMS12 varchar(255);
declare variable PARAMD1 timestamp;
declare variable PARAMD2 timestamp;
declare variable PARAMD3 timestamp;
declare variable PARAMD4 timestamp;
declare variable XPARAMN1 numeric(14,4);
declare variable XPARAMN2 numeric(14,4);
declare variable XPARAMN3 numeric(14,4);
declare variable XPARAMN4 numeric(14,4);
declare variable XPARAMN5 numeric(14,4);
declare variable XPARAMN6 numeric(14,4);
declare variable XPARAMN7 numeric(14,4);
declare variable XPARAMN8 numeric(14,4);
declare variable XPARAMN9 numeric(14,4);
declare variable XPARAMN10 numeric(14,4);
declare variable XPARAMN11 numeric(14,4);
declare variable XPARAMN12 numeric(14,4);
declare variable XPARAMS1 varchar(255);
declare variable XPARAMS2 varchar(255);
declare variable XPARAMS3 varchar(255);
declare variable XPARAMS4 varchar(255);
declare variable XPARAMS5 varchar(255);
declare variable XPARAMS6 varchar(255);
declare variable XPARAMS7 varchar(255);
declare variable XPARAMS8 varchar(255);
declare variable XPARAMS9 varchar(255);
declare variable XPARAMS10 varchar(255);
declare variable XPARAMS11 varchar(255);
declare variable XPARAMS12 varchar(255);
declare variable XPARAMD1 timestamp;
declare variable XPARAMD2 timestamp;
declare variable XPARAMD3 timestamp;
declare variable XPARAMD4 timestamp;
declare variable PARTYP integer;
declare variable PARSYMBOL varchar(40);
declare variable PARSKROT varchar(40);
declare variable PARAMVAL varchar(255);
declare variable ISPARAM integer;
declare variable CURRDOSTAWA integer;
declare variable ISRECZNYSYMBOL integer;
declare variable SYMBOLRECZNY varchar(255);
declare variable CNT integer;
begin
  symbkonc = '';
  isparam = 0;
  if(:dostawca=0) then dostawca = null;
  /* pobierz z dokumpoza parametry do zmiennych lokalnych */
  xparamn1 = null; xparamn2 = null; xparamn3 = null; xparamn4 = null;
  xparamn5 = null; xparamn6 = null; xparamn7 = null; xparamn8 = null;
  xparamn9 = null; xparamn10 = null; xparamn11 = null; xparamn12 = null;

  xparams1 = null; xparams2 = null; xparams3 = null; xparams4 = null;
  xparams5 = null; xparams6 = null; xparams7 = null; xparams8 = null;
  xparams9 = null; xparams10 = null; xparams11 = null; xparams12 = null;

  xparamd1 = null; xparamd2 = null; xparamd3 = null; xparamd4 = null;
  if(:pozref is not null) then begin
    select wersjaref,dostawa,
           paramn1,paramn2,paramn3,paramn4,
           paramn5,paramn6,paramn7,paramn8,
           paramn9,paramn10,paramn11,paramn12,
           params1,params2,params3,params4,
           params5,params6,params7,params8,
           params9,params10,params11,params12,
           paramd1,paramd2,paramd3,paramd4
    from dokumpoz where ref=:pozref
    into :wersjaref,:currdostawa,
         :paramn1,:paramn2,:paramn3,:paramn4,
         :paramn5,:paramn6,:paramn7,:paramn8,
         :paramn9,:paramn10,:paramn11,:paramn12,
         :params1,:params2,:params3,:params4,
         :params5,:params6,:params7,:params8,
         :params9,:params10,:params11,:params12,
         :paramd1,:paramd2,:paramd3,:paramd4;

    /* czy jest to pierwszy dokument przychodowy na ta partie */
    if (exists(select dokumpoz.ref
      from dokumpoz
      left join dokumnag on (dokumpoz.dokument=dokumnag.ref)
      left join defdokum on (dokumnag.typ=defdokum.symbol)
      where dokumnag.dataakc<current_timestamp(0) and dokumnag.ref<>:refdokumnag
      and strmulticmp(';'||dokumnag.akcept||';',';1;8;' )> 0
      and defdokum.wydania=0
      and dokumpoz.dostawa=:currdostawa)) then exit;

    /* na podstawie definicji parametrow pozostaw tylko te ktore wchodza do dostawy i utworz frgment symbolu */
    for select PARTYP,PARSYMBOL,PARSKROT
    from GET_PARAMS_FOR_KTM(:wersjaref,'M',1)
    where PARTYP>1
    into :partyp, :parsymbol, :parskrot
    do begin
      isparam = 1;
      if(:parsymbol='PARAMN1') then begin xparamn1 = :paramn1; paramval = cast(:paramn1 as varchar(40)); end
      if(:parsymbol='PARAMN2') then begin xparamn2 = :paramn2; paramval = cast(:paramn2 as varchar(40)); end
      if(:parsymbol='PARAMN3') then begin xparamn3 = :paramn3; paramval = cast(:paramn3 as varchar(40)); end
      if(:parsymbol='PARAMN4') then begin xparamn4 = :paramn4; paramval = cast(:paramn4 as varchar(40)); end
      if(:parsymbol='PARAMN5') then begin xparamn5 = :paramn5; paramval = cast(:paramn5 as varchar(40)); end
      if(:parsymbol='PARAMN6') then begin xparamn6 = :paramn6; paramval = cast(:paramn6 as varchar(40)); end
      if(:parsymbol='PARAMN7') then begin xparamn7 = :paramn7; paramval = cast(:paramn7 as varchar(40)); end
      if(:parsymbol='PARAMN8') then begin xparamn8 = :paramn8; paramval = cast(:paramn8 as varchar(40)); end
      if(:parsymbol='PARAMN9') then begin xparamn9 = :paramn9; paramval = cast(:paramn9 as varchar(40)); end
      if(:parsymbol='PARAMN10') then begin xparamn10 = :paramn10; paramval = cast(:paramn10 as varchar(40)); end
      if(:parsymbol='PARAMN11') then begin xparamn11 = :paramn11; paramval = cast(:paramn11 as varchar(40)); end
      if(:parsymbol='PARAMN12') then begin xparamn12 = :paramn12; paramval = cast(:paramn12 as varchar(40)); end
      if(:parsymbol='PARAMS1') then begin xparams1 = :params1; paramval = :params1; end
      if(:parsymbol='PARAMS2') then begin xparams2 = :params2; paramval = :params2; end
      if(:parsymbol='PARAMS3') then begin xparams3 = :params3; paramval = :params3; end
      if(:parsymbol='PARAMS4') then begin xparams4 = :params4; paramval = :params4; end
      if(:parsymbol='PARAMS5') then begin xparams5 = :params5; paramval = :params5; end
      if(:parsymbol='PARAMS6') then begin xparams6 = :params6; paramval = :params6; end
      if(:parsymbol='PARAMS7') then begin xparams7 = :params7; paramval = :params7; end
      if(:parsymbol='PARAMS8') then begin xparams8 = :params8; paramval = :params8; end
      if(:parsymbol='PARAMS9') then begin xparams9 = :params9; paramval = :params9; end
      if(:parsymbol='PARAMS10') then begin xparams10 = :params10; paramval = :params10; end
      if(:parsymbol='PARAMS11') then begin xparams11 = :params11; paramval = :params11; end
      if(:parsymbol='PARAMS12') then begin xparams12 = :params12; paramval = :params12; end
      if(:parsymbol='PARAMD1') then begin xparamd1 = :paramd1; paramval = cast(:paramd1 as varchar(40)); end
      if(:parsymbol='PARAMD2') then begin xparamd2 = :paramd2; paramval = cast(:paramd2 as varchar(40)); end
      if(:parsymbol='PARAMD3') then begin xparamd3 = :paramd3; paramval = cast(:paramd3 as varchar(40)); end
      if(:parsymbol='PARAMD4') then begin xparamd4 = :paramd4; paramval = cast(:paramd4 as varchar(40)); end
      if(:partyp=3 and :parskrot<>'') then begin
        symbkonc = substring(:symbkonc||'/'||:parskrot||:paramval from 1 for 120);
      end
    end
  end
  /* stworz symbol dostawy */
  select RECZNYSYMBOL,SYMBOL from DOSTAWY where ref=:currdostawa into :isrecznysymbol,:symbolreczny;
  if(:isrecznysymbol is null or (:isrecznysymbol=0)) then begin
    skrot = null;
    select ID from DOSTAWCY where REF = :dostawca into :skrot;
    if(:skrot is null or(:skrot = '')) then skrot = :magazyn;
    if(:skrot is null or(:skrot = '')) then skrot = 'nieznany';
    if(:data is null) then data = current_date;
    symbol = '';
    execute procedure GETCONFIG('DOSTSYMBFAK') returning_values :dostsymbfak;
    if(:dostsymbfak='1' and :refdokumnag is not null) then begin
      select SYMBFAK from DOKUMNAG where REF=:refdokumnag into :symbfak;
      if(:symbfak is null or :symbfak='') then symbfak = substring(:skrot from 1 for 40); --XXX ZG133796 MKD
      if(:datawazn is not null) then symbol = substring('*'||cast(:datawazn as date)||'/'||cast(:symbfak as varchar(35))||'/'||cast(:data as date)||:symbkonc from 1 for 120);
      else symbol = substring(:symbfak || '/'||cast(:data as date)||:symbkonc from 1 for 120);
    end else if (:dostsymbfak='2') then begin
      symbol = :currdostawa;
    end else begin
      if(:datawazn is not null) then symbol = substring('*'||cast(:datawazn as date)||'/'||cast(:skrot as varchar(35))||'/'||cast(:data as date)||:symbkonc from 1 for 120);
      else symbol = substring(:skrot || '/'||cast(:data as date)||:symbkonc from 1 for 120);
    end
  end else begin
    symbol = substring(:symbolreczny from 1 for 120);
    refdokumnag = null;
  end

  /* jesli dostawa byla podana na pozycji to ja updatujemy */
  if(:currdostawa is not null and :currdostawa<>0) then begin
    update DOSTAWY set
      SYMBOL = :symbol,
      DOSTAWCA = :dostawca,
      DATA = :data,
      MAGAZYN = :magazyn,
      DATAWAZN = :datawazn,
      REFDOKUMNAG = :refdokumnag,
      PARAMN1 = :xparamn1,
      PARAMN2 = :xparamn2,
      PARAMN3 = :xparamn3,
      PARAMN4 = :xparamn4,
      PARAMN5 = :xparamn5,
      PARAMN6 = :xparamn6,
      PARAMN7 = :xparamn7,
      PARAMN8 = :xparamn8,
      PARAMN9 = :xparamn9,
      PARAMN10 = :xparamn10,
      PARAMN11 = :xparamn11,
      PARAMN12 = :xparamn12,
      PARAMS1 = :xparams1,
      PARAMS2 = :xparams2,
      PARAMS3 = :xparams3,
      PARAMS4 = :xparams4,
      PARAMS5 = :xparams5,
      PARAMS6 = :xparams6,
      PARAMS7 = :xparams7,
      PARAMS8 = :xparams8,
      PARAMS9 = :xparams9,
      PARAMS10 = :xparams10,
      PARAMS11 = :xparams11,
      PARAMS12 = :xparams12,
      PARAMD1 = :xparamd1,
      PARAMD2 = :xparamd2,
      PARAMD3 = :xparamd3,
      PARAMD4 = :xparamd4
    where REF=:currdostawa;
  end
end^
SET TERM ; ^
