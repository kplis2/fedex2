--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_GLS_SHIPPINGLABEL_RESPONSE(
      EDEDOCSH_REF INTEGER_ID)
  returns (
      OTABLE STRING20,
      OREF INTEGER_ID)
   as
declare variable SHIPPINGDOC INTEGER_ID;  
declare variable LABELFORMAT STRING10;
declare variable PACKAGEREF INTEGER_ID;  
declare variable LABEL blob_utf8;
declare variable ROOTPARENT INTEGER_ID;
declare variable PARENT INTEGER_ID;
begin
    
  select oref
    from ededocsh
    where ref = :ededocsh_ref
    into :shippingdoc; 

  select p.id
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.name = 'ArrayOfCLabel'
  into :rootparent;

  select p.id
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.parent = :rootparent
      and p.name = 'label'
  into :parent;

  select trim(p.val)
    from ededocsp p
    where p.ededoch = :ededocsh_ref
      and p.parent = :parent
      and p.name = 'labelType'
    into :labelformat;

  select oref, otable
    from ededocsh
    where ref = :ededocsh_ref
  into :oref, :otable;


      select first 1 o.ref from listywysdroz_opk o
      where o.listwysd = :shippingdoc
        and o.rodzic is null
        and coalesce(o.symbolsped,'') = ''
      order by o.ref
    into :packageref;

    select p.val
      from ededocsp p
      where p.ededoch = :ededocsh_ref
        and p.parent = :parent
        and p.name = 'file'
    into :label;

    update or insert into listywysdopkrpt(doksped, opk, rpt, format)
      values(:shippingdoc,:packageref,:label,:labelformat)
      matching (doksped, opk);

  suspend;
end^
SET TERM ; ^
