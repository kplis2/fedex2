--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KODKS_FROM_DICTPOS(
      DICTDEF integer,
      DICTPOS integer)
  returns (
      SYMBOL ANALYTIC_ID)
   as
declare variable DICT_TYPE varchar(20);
declare variable company integer;
begin
  select TYP from slodef where ref = :DICTDEF
    into :dict_type;
  if (:dict_type = 'ESYSTEM') then
    select kontoks from slopoz where ref = :DICTPOS
      into :SYMBOL;
  else if (:dict_type = 'KLIENCI') then
    select kontofk from klienci where ref = :DICTPOS
      into :SYMBOL;
  else if (:dict_type = 'DOSTAWCY') then
    select kontofk from dostawcy where ref = :DICTPOS
      into :SYMBOL;
  else if (:dict_type = 'PERSONS') then
  begin
    execute procedure get_global_param('CURRENTCOMPANY')
      returning_values :company;
    select symbol from cpersons where ref = :DICTPOS and company = :company  --PR59074
      into :SYMBOL;
  end
  else if (:dict_type = 'EMPLOYEES') then
    select symbol from employees where ref = :DICTPOS
      into :SYMBOL;
end^
SET TERM ; ^
