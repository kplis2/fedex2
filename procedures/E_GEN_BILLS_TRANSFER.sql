--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_GEN_BILLS_TRANSFER(
      FROMDATE date,
      TODATE date,
      BANKACC varchar(10) CHARACTER SET UTF8                           ,
      CURRENTCOMPANY integer,
      NOACCEPT integer,
      TYP integer = 0)
  returns (
      COUNTER integer)
   as
declare variable DICTDEF integer;
declare variable DICTPOS integer;
declare variable TDATE timestamp;
declare variable TOWHO varchar(255);
declare variable TOACC varchar(80);
declare variable TOADRESS varchar(255);
declare variable STREET varchar(30);
declare variable HOUSENO varchar(20);
declare variable LOCALNO varchar(20);
declare variable POSTCODE varchar(10);
declare variable POST varchar(30);
declare variable AMOUNT numeric(14,2);
declare variable TITLE varchar(40);
declare variable CITY varchar(30);
declare variable BTRANSFER integer;
declare variable PAYROLL integer;
declare variable AKTUOPERATOR integer;
declare variable BANK varchar(255);
declare variable DESCRIPT varchar(255);
declare variable PRZELEWTYPE varchar(10);
begin

  counter = 0;
  select min(ref) from slodef where typ = 'PERSONS' into :dictdef;
  execute procedure get_global_param('AKTUOPERATOR') returning_values aktuoperator;
  execute procedure getconfig('PRZELEWWYPLATA') returning_values :przelewtype;

  for
    select r.ref, b.account, r.payday, e.personnames, a.street, a.houseno,
        a.localno, a.postcode, a.post, p.pvalue, t.title, e.person, a.city, coalesce(ba.nazwa,'<brak nazwy>')
      from epayrolls r
        join emplcontracts c on (r.emplcontract = c.ref)
        join econtrtypes t on (t.contrtype = c.econtrtype)
        join employees e on (c.employee = e.ref)
        join eprpos p on (p.payroll = r.ref)
        join bankaccounts b on (b.ref = (select first 1 h.bankaccount from emplcontrhist h  --PR55960 
                                           where h.fromdate <= r.billdate and h.emplcontract = r.emplcontract
                                           order by h.fromdate desc))
        left join epersaddr a on (a.person = e.person and a.addrtype = 0 and a.status = 1)
        left join banki ba on (b.bank = ba.symbol)
      where e.company = :currentcompany
        and p.ecolumn = 9000
        and r.empltype > 1
        and r.btransfer is null
        and r.payday >= :fromdate and r.payday <= :todate
        and (:noaccept = 1 or r.status > 0)
      order by e.personnames COLLATE UNICODE, r.payday
      into :payroll, :toacc, :tdate, :towho, :street, :houseno, :localno,
        :postcode, :post, :amount, :title, :dictpos, :city, :bank
  do begin

    if (tdate < current_timestamp(0)) then
      tdate = current_date;

    toadress = street || ' ' || houseno;
    if (localno <> '') then
      toadress = toadress || '/' || localno;
    toadress = toadress || '; ' || postcode || ' ' || post;
    if (post = '' or post is null) then
      toadress = toadress || city;

    execute procedure gen_ref('BTRANSFERS')
       returning_values btransfer;

    insert into btransfers (ref, typ, bankacc, btype, slodef, slopoz, data,
        towho, toacc, toadress, todescript, curr, amount, autobtransfer, company)
      values (:btransfer, 1, :bankacc, :przelewtype, :dictdef, :dictpos, :tdate,
        :towho, :toacc, :toadress, 'wynagrodzenie - ' || :title, 'PLN', :amount, 1,
        :currentcompany);

    update epayrolls set btransfer = :btransfer where ref = :payroll;

    if (typ = 1) then
    begin
      descript = 'Przekazanie danych w postaci przelewu do: ' || :bank;
      insert into epersdatasecur (person, kod, regdate, operator, descript)
        values (:dictpos, 2, :tdate, :aktuoperator, :descript);
    end

    counter = counter + 1;
  end
  suspend;
end^
SET TERM ; ^
