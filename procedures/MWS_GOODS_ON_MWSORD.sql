--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_GOODS_ON_MWSORD(
      MWSORD integer)
  returns (
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      DOCID integer)
   as
begin
  select o.docid
    from mwsords o where o.ref = :mwsord
    into docid;
  for
    select a.good, a.vers
      from mwsacts a
      where a.status = 5 and a.mwsord = :mwsord
      into good, vers
  do begin
    suspend;
  end
  if (docid > 0) then
  begin
    for
      select p.ktm, p.wersjaref
        from dokumpoz p
        where p.dokument = :docid
        into good, vers
    do begin
      suspend;
    end
  end
end^
SET TERM ; ^
