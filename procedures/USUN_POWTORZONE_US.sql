--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE USUN_POWTORZONE_US as
declare variable ref integer;
declare variable address varchar(60);
declare variable postcode varchar(6);

begin
for
select distinct E1.address, E1.postcode
from einternalrevs E1
where exists(select count(*)
from einternalrevs E2
where E1.name=E2.name and E1.address=E2.address and E1.city=E2.city and E1.postcode = E2.postcode
having count(*)>1 )
into :address, :postcode
do begin

select first 1 E.ref from einternalrevs E
where E.address = :address and E.postcode = :postcode
into: ref;

delete from einternalrevs
where ref = :ref;
end
end^
SET TERM ; ^
