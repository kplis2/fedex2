--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WYREJESTRUJ_KONTAKT(
      EMADRREF integer)
   as
declare variable zalezny smallint;
begin
  if (emadrref is not NULL) then
  begin
    select ZALEZNY from KONTAKTY
      where EMAILADR=:emadrref
      into :zalezny;
    if (zalezny=1) then
    begin
      update KONTAKTY set ZALEZNY=2 where EMAILADR=:emadrref;
      delete from KONTAKTY where EMAILADR=:emadrref;
    end
  end
end^
SET TERM ; ^
