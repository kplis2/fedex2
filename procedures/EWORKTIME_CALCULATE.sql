--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EWORKTIME_CALCULATE(
      EMPLOYEE integer,
      CALENDAR integer,
      SHIFT integer,
      EPRESENCELIST integer,
      WORKDATE timestamp)
  returns (
      WORKSTART time,
      WORKEND time)
   as
declare variable CONTRACT integer;
declare variable WORKDIM float;
declare variable PROPORTIONAL smallint;
declare variable ONLYONESHIFT varchar(255);
declare variable SHIFTSTART varchar(255);
declare variable SHIFTEND varchar(255);
begin
  if (epresencelist is not null) then
    select listsdate from epresencelists where ref = :epresencelist into workdate;
  select max(ref)
    from emplcontracts C
      join econtrtypes T on (C.econtrtype = T.contrtype)
    where employee = :employee and T.empltype = 1
      and C.fromdate <= :workdate and (C.enddate is null or C.enddate >= :workdate)
    into contract;
  -- okreslenie wymiaru pracy
  if (contract is not null) then
  begin
    select workdim, proportional
      from emplcontracts
      where ref = :contract
      into workdim, proportional;
  end
  -- okreslenie czasu pracy pracownika
  select workstart, workend
    from ecaldays e
    join edaykinds edk on edk.ref = e.daykind
    where calendar = :calendar and cdate = :workdate
      and edk.daytype = 1
    into workstart, workend;
  -- zmniejszenie ilosci godzin do wymiaru etatu
  execute procedure get_config('ONLYONESHIFT',2) returning_values onlyoneshift;
  -- 0 kilka zmian; 1 - jedna zmiana; 2 - jedna zmiana w kalendarzu ale wiele rejestrowanych
  if (onlyoneshift = '2' and shift > 0) then
  begin
    if (shift = 1) then
    begin
      execute procedure get_config('SHIFT2START',2) returning_values shiftstart;
      execute procedure get_config('SHIFT2END',2) returning_values shiftend;
    end else
    begin
      execute procedure get_config('SHIFT3START',2) returning_values shiftstart;
      execute procedure get_config('SHIFT3END',2) returning_values shiftend;
    end
    workstart = cast(shiftstart as time);
    workend = cast(shiftend as time);
  end
  -- dodajemy do poczatku czasu pracy proporcje
  if (proportional > 0) then
  begin
    workstart = workstart + (1 - workdim)*60*60*8;
  end
end^
SET TERM ; ^
