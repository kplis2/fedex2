--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_GLS_PICKUPCREATE_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable TRANSPORTORDER INTEGER_ID;
declare variable TRANSPORTORDERSYMB STRING40;
declare variable SHIPPINGDOCID STRING40;
declare variable ROOTPARENT INTEGER_ID;
declare variable GRANDPARENT INTEGER_ID;
declare variable SHIPPER SPEDYTOR_ID;
declare variable USERNAME STRING40;
declare variable USERPASSWORD STRING40;
declare variable FILEPATH STRING255;
declare variable SERVICETYPE STRING20;
declare variable STATUS varchar(1);
begin
  --typ serwisu
  --jesli chcesz testowac ustaw na TEST, dla produkcyjnego ustaw PRODUCTION
  servicetype = 'TEST';

  select l.ref, l.symbol, l.spedytor, l.stan
    from listywys l
    where l.ref = :oref
  into :transportorder, :transportordersymb, :shipper, :status;

  if(coalesce(status,'') != 'A')then
    exception EDE_LISTYWYSD_BRAK 'Nie mozna potwierdzić niezamkniętego zlecenia.';
  --sprawdzanie czy istieje dokument spedycyjny

  --sprawdzanie czy istieje dokument spedycyjny
  if(:transportorder is null) then exception EDE_LISTYWYSD_BRAK;

  val = null;
  parent = null;
  if (:servicetype = 'PRODUCTION') then
    params = 'xmlns:ade="https://adeplus.gls-poland.com/adeplus/pm1/ade_webapi2.php?wsdl"';
  else
    params = 'xmlns="https://ade-test.gls-poland.com/adeplus/pm1/ade_webapi2.php?wsdl"';
  id = 0;
  name = 'adePickup_Create';
  suspend;

  rootparent = id;

  --dane autoryzacyjne
  select k.login, k.haslo, k.sciezkapliku
    from spedytkonta k
    where k.spedytor = :shipper
  into :username, :userpassword, :filepath;

  val = :username;
  name = 'username';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = :userpassword;
  name = 'password';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = :filepath;
  name = 'filepath';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = :transportordersymb;
  name = 'shippingorder';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = :servicetype;
  name = 'serviceType';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  val = :servicetype;
  name = 'consigns_ids';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;

  grandparent = id;

  for
    select l.symbolspedid
      from listywysd l
      where l.listawys = :transportorder
    into :shippingdocid
  do begin
    val = :shippingdocid;
    name = 'items';
    params = null;
    parent = grandparent;
    id = id + 1;
    suspend;
  end

  val = 'Przesyłki zlecenia: '||:transportordersymb;
  name = 'desc';
  params = null;
  parent = rootparent;
  id = id + 1;
  suspend;
end^
SET TERM ; ^
