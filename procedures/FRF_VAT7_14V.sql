--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_VAT7_14V(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      VTYPE smallint)
  returns (
      AMOUNT integer)
   as
declare variable company integer;
declare variable yearid integer;
declare variable frvhdr integer;
declare variable frpsn integer;
declare variable frcol integer;
declare variable cacheparams varchar(255);
declare variable ddparams varchar(255);
declare variable periodbo varchar(6);
declare variable k10 numeric(14,2);
declare variable k11 numeric(14,2);
declare variable k12 numeric(14,2);
declare variable k13 numeric(14,2);
declare variable k14 numeric(14,2);
declare variable k15 numeric(14,2);
declare variable k16 numeric(14,2);
declare variable k17 numeric(14,2);
declare variable k18 numeric(14,2);
declare variable k19 numeric(14,2);
declare variable k20 numeric(14,2);
declare variable k21 numeric(14,2);
declare variable k22 numeric(14,2);
declare variable k23 numeric(14,2);
declare variable k24 numeric(14,2);
declare variable k25 numeric(14,2);
declare variable k26 numeric(14,2);
declare variable k27 numeric(14,2);
declare variable k28 numeric(14,2);
declare variable k29 numeric(14,2);
declare variable k30 numeric(14,2);
declare variable k31 numeric(14,2);
declare variable k32 numeric(14,2);
declare variable k33 numeric(14,2);
declare variable k34 numeric(14,2);
declare variable k35 numeric(14,2);
declare variable k36 numeric(14,2);
declare variable k37 numeric(14,2);
declare variable k38 numeric(14,2);
declare variable k39 numeric(14,2);
declare variable k40 numeric(14,2);
declare variable k41 numeric(14,2);
declare variable k42 numeric(14,2);
declare variable k43 numeric(14,2);
declare variable k44 numeric(14,2);
declare variable k45 numeric(14,2);
declare variable k46 numeric(14,2);
declare variable k47 numeric(14,2);
declare variable k48 numeric(14,2);
declare variable k49 numeric(14,2);
declare variable k50 numeric(14,2);
declare variable k51 numeric(14,2);
declare variable k52 numeric(14,2);
declare variable k53 numeric(14,2);
declare variable k54 numeric(14,2);
declare variable k55 numeric(14,2);
declare variable cachevalue numeric(14,2);
declare variable frversion integer;
declare variable stringparams varchar(1024);
declare variable stringout varchar(1024);
declare variable i integer;
declare variable sinsert varchar(4096);
begin

  if (frvpsn = 0) then exit;
  if (:period < 201401 or :period > 201412) then
  begin
    amount = 0;
    exit;
  end
--vtype - pozycja z deklaracji VAT 7

  select frvhdr, frpsn, frcol
    from frvpsns
    where ref = :frvpsn
    into :frvhdr, :frpsn, :frcol;

  select company, frversion
    from frvhdrs
    where ref = :frvhdr
    into :company, :frversion;

  select yearid from bkperiods where id = :period and company = :company
    into :yearid;

  periodbo = cast(yearid as varchar(4)) || '00';


  amount = 0;
    while (periodbo <= period) do
    begin
      cacheparams = periodbo||';'||vtype||';'||company;
      ddparams = periodbo||' '||vtype;

      if( not exists ( select ref
                         from frftempvals f
                         where f.field_id = 'VAT7_14V'||:ddparams and f.frvhdr = :frvhdr)) then
      begin
        /*Obliczanie deklaracji*/
        select coalesce(k10,0), coalesce(k11,0), coalesce(k12,0), coalesce(k13,0), coalesce(k14,0), coalesce(k15,0), coalesce(k16,0), coalesce(k17,0), coalesce(k18,0), coalesce(k19,0), coalesce(k20,0),
            coalesce(k21,0), coalesce(k22,0), coalesce(k23,0), coalesce(k24,0), coalesce(k25,0), coalesce(k26,0), coalesce(k27,0), coalesce(k28,0), coalesce(k29,0), coalesce(k30,0),
            coalesce(k31,0), coalesce(k32,0), coalesce(k33,0), coalesce(k34,0), coalesce(k35,0), coalesce(k36,0), coalesce(k37,0), coalesce(k38,0), coalesce(k39,0), coalesce(k40,0),
            coalesce(k41,0), coalesce(k42,0), coalesce(k43,0), coalesce(k44,0), coalesce(k45,0), coalesce(k46,0), coalesce(k47,0), coalesce(k48,0), coalesce(k49,0), coalesce(k50,0),
            coalesce(k51,0), coalesce(k52,0), coalesce(k53,0), coalesce(k54,0), coalesce(k55,0)
          from rpt_vat7_14(:periodbo, 0, 0, 0, 0, 0, 0, 0, :company)
          into :k10, :k11, :k12, :k13, :k14, :k15, :k16, :k17, :k18, :k19, :k20, 
            :k21, :k22, :k23, :k24, :k25, :k26, :k27, :k28, :k29 ,:k30,
            :k31, :k32, :k33, :k34, :k35, :k36, :k37, :k38, :k39, :k40,
            :k41, :k42, :k43, :k44, :k45, :k46, :k47, :k48, :k49, :k50,
            :k51, :k52, :k53, :k54, :k55;

        stringparams = :k10||';'||:k11||';'||:k12||';'||:k13||';'||:k14||';'||:k15||';'||:k16||';'||:k17||';'||:k18||';'||:k19||';'||:k20
          ||';'||:k21||';'||:k22||';'||:k23||';'||:k24||';'||:k25||';'||:k26||';'||:k27||';'||:k28||';'||:k29||';'||:k30
          ||';'||:k31||';'||:k32||';'||:k33||';'||:k34||';'||:k35||';'||:k36||';'||:k37||';'||:k38||';'||:k39||';'||:k40
          ||';'||:k41||';'||:k42||';'||:k43||';'||:k44||';'||:k45||';'||:k46||';'||:k47||';'||:k48||';'||:k49||';'||:k50
          ||';'||:k51||';'||:k52||';'||:k53||';'||:k54||';'||:k55;

        /*cachowanie wartoci deklaracji na potem bo to i tak jeden koszt*/
        i = 10;
        for select stringout from parsestring(:stringparams,';')
          into :stringout
        do begin
          sinsert =' insert into frftempvals (field_id, val, frvhdr)
                          values (''VAT7_14V'||periodbo||' '||:i||''','||:stringout||','||:frvhdr||');';
          execute statement sinsert;
          i = i + 1;
        end
      end
   select first 1 val from frftempvals f where f.field_id = 'VAT7_14V'||:ddparams and f.frvhdr = :frvhdr
        into :cachevalue;
    amount = amount + cachevalue;
    periodbo = cast ((cast(periodbo as integer) + 1) as varchar(6));
    amount = coalesce(amount,0);
    insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
      values (:frvhdr, :frpsn, :frcol, 'VAT7_14V', :cacheparams, :ddparams, :amount, :frversion);
  end
  suspend;
end^
SET TERM ; ^
