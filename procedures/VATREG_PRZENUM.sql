--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE VATREG_PRZENUM(
      VATREG varchar(10) CHARACTER SET UTF8                           ,
      VATPERIOD varchar(6) CHARACTER SET UTF8                           )
  returns (
      STATUS smallint)
   as
declare variable num integer;
declare variable bkdocref integer;
begin
  status = 0;
  num = 1;
  for select ref
  from bkdocs where VATREG=:vatreg and VATPERIOD=:vatperiod and STATUS>1
  order by vnumber
  into :bkdocref
  do begin
    update bkdocs set VNUMBER=:num where REF=:bkdocref;
    num = :num+1;
  end
  status = 1;
end^
SET TERM ; ^
