--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_WHSEC_GETNAME(
      WHSEC STRING40)
  returns (
      WHSECNAME STRING40)
   as
declare variable cnt integer_id;
begin
  /* [FH] Procedura wykorzystywana na HH do sciagania nazwy sektora na podstawie jego stringa */

  if (:whsec is null) then
    exit;

  select count(*)
    from whsecgroups wh
    where wh.whsecgroups similar to '%'||:whsec||'%'
    into :cnt;

  if (:cnt > 1) then
    exception universal 'Błąd konfiguracji WHSECGROUPS. Skontaktuj się z SENTE';

  select descript
    from whsecgroups wh
    where wh.whsecgroups similar to '%'||:whsec||'%'
    into :whsecname;

  suspend;
end^
SET TERM ; ^
