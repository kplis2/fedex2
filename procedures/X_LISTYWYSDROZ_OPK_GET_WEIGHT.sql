--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_LISTYWYSDROZ_OPK_GET_WEIGHT(
      LISTWYSDROZ_OPK LISTYWYSDROZ_OPK_ID)
  returns (
      WEIGHT NUMERIC_14_2)
   as
begin
  if (coalesce(listwysdroz_opk,0) = 0) then
    exception universal 'Nie podano identyfikatora opakowania';
  
  select sum(coalesce(tj.waga,0) * coalesce(lr.iloscmag,0))
    from listywysdroz_opk op
      left join listywysdroz lr on (lr.opk = op.ref)
      left join towjedn tj on (lr.ktm = tj.ktm and tj.glowna = 1)
    where op.ref = :listwysdroz_opk
    group by op.ref
    into :weight;

  if (weight is null) then
    weight = 0;
end^
SET TERM ; ^
