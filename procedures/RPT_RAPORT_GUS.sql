--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_RAPORT_GUS(
      DATA timestamp)
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      PROCN integer,
      PROCP integer,
      PROCK integer,
      ILOSCN numeric(14,4),
      ILOSCP numeric(14,4),
      ILOSCK numeric(14,4),
      TSTAN numeric(14,4),
      TWARTOSC numeric(14,2),
      NPK smallint,
      N smallint,
      P smallint,
      K smallint,
      POZ smallint)
   as
begin
  for select cast(substring(mag_stany.rktm from 9 for 2) as integer),
           cast(substring(mag_stany.rktm from 11 for 2) as integer),
           cast(substring(mag_stany.rktm from 13 for 2) as integer),
           stan, wartosc, rktm
  from mag_stany(null,'NR%',null,null,null,:data,null,0)
  into :procn, :procp, :prock, tstan, twartosc, :ktm
  do begin
    npk = 0; n = 0; p = 0; k = 0; poz = 0;
    if (:procn is null) then procn = 0;
    if (:procp is null) then procp = 0;
    if (:prock is null) then prock = 0;
    if (:tstan is null) then tstan = 0;
    if (:procn = 0 and :procp = 0 and :prock = 0) then poz = 1;
    if (:procn <> 0 and (:procp <> 0 or (:prock <> 0))) then npk = 1;
    if (:procp <> 0 and (:procn <> 0 or (:prock <> 0))) then npk = 1;
    if (:prock <> 0 and (:procp <> 0 or (:procn <> 0))) then npk = 1;
    if (npk = 0) then
    begin
      if (:procn <> 0) then n = 1;
      if (:procp <> 0) then p = 1;
      if (:prock <> 0) then k = 1;
    end
    iloscn = :procn * tstan / 100;
    iloscp = :procp * tstan / 100;
    ilosck = :prock * tstan / 100;
    suspend;
  end
end^
SET TERM ; ^
