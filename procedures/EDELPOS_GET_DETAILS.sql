--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDELPOS_GET_DETAILS(
      DELEGATION integer,
      ODDZIAL varchar(10) CHARACTER SET UTF8                           )
  returns (
      FROMPLACE varchar(80) CHARACTER SET UTF8                           ,
      FROMDATE timestamp,
      TOPLACE varchar(80) CHARACTER SET UTF8                           ,
      TODATE timestamp)
   as
begin

  fromplace = '';
  fromdate = NULL;
  toplace = '';
  todate = NULL;

  /* Istnieją pozycje delegacji */
  if(exists(select first 1 1 from edelpos where delegation = :delegation)) then begin
    select first 1 toplace
      from edelpos
      where delegation = :delegation
      order by fromdate desc
    into :fromplace;

    select todate
      from edelegations
      where ref = :delegation
    into :fromdate;

    select city
      from oddzialy
      where oddzial = :oddzial
    into :toplace;
  end
  /* Pierwsza pozycja delegacji */
  else begin
    select fromdate, substring(destination from 1 for 80)
      from edelegations
      where ref = :delegation
    into :fromdate, :toplace;
    select city
      from oddzialy
      where oddzial = :oddzial
    into :fromplace;
  end

  /* Data końcowa przejazdu taka jak data początkowa */
  todate = :fromdate;

   suspend;
end^
SET TERM ; ^
