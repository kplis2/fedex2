--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STKRTAB_NALICZ(
      TABELA integer,
      ANALDOST integer,
      PRM numeric(14,2),
      PRP numeric(14,2))
  returns (
      STATUS integer,
      NUMROWS integer)
   as
declare variable ilmin numeric(14,4);
declare variable ilmax numeric(14,4);
declare variable oldilmin numeric(14,4);
declare variable oldilmax numeric(14,4);
declare variable potrzeba numeric(14,4);
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable stat integer;
begin
  numrows = 0;
  STATUS =-1;
  if(:prm is null) then prm =0;
  if(:prp is null) then prp = 0;
  if((:prm < 0) or (:prm > 100) or (:prp < 0)) then exception STKRTAB_NALICZ_WP;
  for select KTM, WERSJA, POTRZEBA from ANALDOSTP  where ANALIZA=:ANALDOST and POTRZEBA > 0 order by KTM, WERSJA
  into :ktm, :wersja, :potrzeba
  do begin
    /*okreslenie ilosci min i max*/
    ilmin = :potrzeba * (1 - :prm/100);
    ilmax = :potrzeba * (1 + :prp/100);
    stat = null;
    select STATUS,ilmin, ilmax from STKRPOZ where TABELA=:TABELA and KTM = :ktm and WERSJA = :wersja into :stat, :oldilmin, :oldilmax;
    if(:stat is null or (:stat <> 3)) then begin
      if(:stat is null) then begin
        insert into STKRPOZ(TABELA, KTM, WERSJA, ILMIN, ILMAX, STATUS)
        values(:TABELA, :KTM, :WERSJA, :ilmin, :ilmax,1);
        numrows = :numrows + 1;
      end else if(((:ilmin <> :oldilmin) or (:ilmax <> oldilmax))and :stat <> 3) then begin
        update STKRPOZ set ILMIN = :ilmin, ILMAX = :ilmax, STATUS = 1 where
          TABELA=:TABELA AND KTM=:KTM and WERSJA=:WERSJA;
        numrows = :numrows + 1;
      end
    end
  end
  UPDATE STKRTAB set LANALDOST=:ANALDOST, LDATAaktu=CURRENT_DATE WHERE ref=:TABELA;
  STATUS = 1;
end^
SET TERM ; ^
