--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ACCOUNT_MATCHING_CHECK(
      ACCOUNT ACCOUNT_ID,
      COMPANY COMPANIES_ID,
      YEARID YEARS_ID,
      CURRENCY CURRENCY_ID = null)
  returns (
      MATCHABLE SMALLINT_ID)
   as
declare variable BKACCOUNT BKACCOUNTS_ID;
begin
  matchable = 0;
  select a.bkaccount
    from accounting a
    where a.account = :account
      and a.company = :company
      and a.yearid = :yearid
      and a.currency = :currency
    into :bkaccount;
  if (:bkaccount > 0) then
    execute procedure bkaccount_matching_check(:bkaccount)
      returning_values (:matchable);
end^
SET TERM ; ^
