--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SP_EWORKEDHOURS(
      CALENDAR_IN integer,
      DEPARTMENT varchar(10) CHARACTER SET UTF8                           ,
      FROMPERIOD varchar(6) CHARACTER SET UTF8                           ,
      TOPERIOD varchar(6) CHARACTER SET UTF8                           ,
      FROMDATE_IN varchar(10) CHARACTER SET UTF8                           ,
      TODATE_IN varchar(10) CHARACTER SET UTF8                           ,
      NOT_NULL smallint,
      COMPANY integer,
      EMPLOYEE_IN integer = null)
  returns (
      EMPLOYEE integer,
      CALENDAR integer,
      PERSONNAMES varchar(120) CHARACTER SET UTF8                           ,
      CALENDAR_NAME varchar(20) CHARACTER SET UTF8                           ,
      STD_HOURS_MIN integer,
      STD_HOURS varchar(8) CHARACTER SET UTF8                           ,
      NOMINAL_HOURS_MIN integer,
      NOMINAL_HOURS varchar(8) CHARACTER SET UTF8                           ,
      WORKED_HOURS_MIN integer,
      WORKED_HOURS varchar(8) CHARACTER SET UTF8                           ,
      SHORTAGE_HOURS_MIN integer,
      SHORTAGE_HOURS varchar(8) CHARACTER SET UTF8                           ,
      TAKE_AWAY_HOURS_MIN integer,
      TAKE_AWAY_HOURS varchar(8) CHARACTER SET UTF8                           ,
      OVERTIME_DAYTYPE50_MIN integer,
      OVERTIME_DAYTYPE50_HOURS varchar(8) CHARACTER SET UTF8                           ,
      OVERTIME_WEEKTYPE50_MIN integer,
      OVERTIME_WEEKTYPE50_HOURS varchar(8) CHARACTER SET UTF8                           ,
      OVERTIME_DAYTYPE100_MIN integer,
      OVERTIME_DAYTYPE100_HOURS varchar(8) CHARACTER SET UTF8                           ,
      OVERTIME_WEEKTYPE100_MIN integer,
      OVERTIME_WEEKTYPE100_HOURS varchar(8) CHARACTER SET UTF8                           )
   as
declare variable FROMDATE date;
declare variable TODATE date;
declare variable NOMINAL_WITH_WORKDIM integer;
declare variable COMPENSATORY_TIME_NOMINAL integer;
declare variable STDCALENDAR integer;
declare variable CFROMDATE date;
declare variable CTODATE date;
declare variable NORM_PER_DAY integer;
declare variable WORKDAYS integer;
begin
  --DS:widok sluzacy do rozliczania brakow obecnosci pracownika jak rowniez do wyznaczania
  --ktore nadgodziny sa sredniotygodniowe
  if (calendar_in = 0) then calendar_in = null;
  if (department = '') then department = null;
  if (coalesce(char_length(fromperiod),0) = 6) then -- [DG] XXX ZG119346
  begin
    select fromdate from period2dates(:fromperiod) into :fromdate;
    select todate from period2dates(:toperiod) into :todate;
  end else begin
    fromdate = cast(fromdate_in as date);
    todate = cast(todate_in as date);
  end
  --nominalny czas pracy nalezy brac z kalendarza standardowego,
  --poniewaz kazdy inny kalendarz moze miec inna liczbe godzin nominalnych co wcale nie bedzie bledem
  execute procedure get_config('STDCALENDAR',2) returning_values stdcalendar;

  for
    select ecd.employee, e.personnames, ecd.ecalendar, c.name, c.norm_per_day, min(ecd.cdate), max(ecd.cdate),
        sum(ecd.worktime), sum(ecd.nomworktime),
        --braki
        sum(case when ecd.compensatory_time - coalesce(ecd.compensated_time,0) < 0 then -ecd.compensatory_time + coalesce(ecd.compensated_time,0) else 0 end),
        sum(case when ecd.overtime_type = 1 then ecd.compensatory_time / 1.50 else ecd.compensatory_time end), --do odbioru (na wniosek pracownika sa razy 1,5)
        sum(case when edk.daytype = 1 then ecd.overtime50 else 0 end),
        sum(case when edk.daytype = 1 then ecd.overtime100 else 0 end),
        sum(case when edk.daytype = 0 and edk.name = 'wolne' then ecd.overtime50 else 0 end),
        sum(m.dimden * ecd.nomworktime / m.dimnum)   --mnozymy czas nominalny przez wymiar etatu, zeby nie naliczac nadgodzin sredniotygodniowych niepelnoetatowcom
      from ecalendars c
        join emplcaldays ecd on ecd.ecalendar = c.ref
        join employees e on e.ref = ecd.employee
        join employment m on m.employee = e.ref
        join edaykinds edk on edk.ref = ecd.daykind
      where ecd.cdate >= :fromdate
        and ecd.cdate <= :todate
        and (ecd.ecalendar = :calendar_in or :calendar_in is null)
        and e.company = :company
        and m.fromdate <= :todate
        and (m.todate >= :fromdate or m.todate is null)
        and (ecd.employee = :employee_in or :employee_in is null)
        and (m.department = :department or :department is null)
      group by 1,2,3,4, 5
      into :employee, :personnames, :calendar, :calendar_name, :norm_per_day, :cfromdate, :ctodate,
        :worked_hours_min, :nominal_hours_min, :shortage_hours_min, :compensatory_time_nominal,
        :overtime_daytype50_min, :overtime_daytype100_min,
        :overtime_weektype50_min, :nominal_with_workdim
  do begin
    --nadwyzka godzin od poczatku istnienia wszechswiata
    select sum(cm.compensatory_time - coalesce(cm.compensated_time,0))
      from emplcaldays cm
      where cm.employee = :employee
        and cm.cdate <= :todate
        and (cm.ecalendar = :calendar_in or :calendar_in is null)
        and cm.compensatory_time > 0
      into take_away_hours_min;

    workdays = 0;
    select count(c.ref)
      from ecaldays c
      join edaykinds edk on edk.ref = c.daykind
      where c.calendar = :stdcalendar
        and c.cdate >= :cfromdate
        and c.cdate <= :ctodate
        and edk.daytype = 1
      into :workdays;
    if (norm_per_day > 28800) then norm_per_day = 28800;
    std_hours_min = workdays * norm_per_day;

    overtime_weektype100_min = 0;
    --metoda PIP wyliczania nadg. sredniotyg., od sumy godzin odejmujemy godziny nominalne
    overtime_weektype100_min = worked_hours_min - nominal_with_workdim - compensatory_time_nominal;
    if (overtime_weektype100_min < 0) then overtime_weektype100_min = 0;
    --a nastepnie nadgodziny dobowe, jezeli cos zostanie to to sa nadg. sredniotygodniowe
    overtime_weektype100_min = overtime_weektype100_min - overtime_daytype50_min
      - overtime_daytype100_min - overtime_weektype50_min;
    if (not_null = 0 or take_away_hours_min <> 0 or shortage_hours_min <> 0 or overtime_daytype50_min <> 0
        or overtime_weektype50_min <> 0 or overtime_daytype100_min <> 0 or overtime_weektype100_min <> 0) then
    begin
      execute procedure durationstr2(worked_hours_min) returning_values worked_hours;
      execute procedure durationstr2(take_away_hours_min) returning_values take_away_hours;
      execute procedure durationstr2(std_hours_min) returning_values std_hours;
      execute procedure durationstr2(nominal_hours_min) returning_values nominal_hours;
      execute procedure durationstr2(shortage_hours_min) returning_values shortage_hours;
      execute procedure durationstr2(overtime_daytype50_min) returning_values overtime_daytype50_hours;
      execute procedure durationstr2(overtime_weektype50_min) returning_values overtime_weektype50_hours;
      execute procedure durationstr2(overtime_daytype100_min) returning_values overtime_daytype100_hours;
      execute procedure durationstr2(overtime_weektype100_min) returning_values overtime_weektype100_hours;

      suspend;
    end
  end
end^
SET TERM ; ^
