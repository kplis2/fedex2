--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_TRANSFER_SETTLEMENTS(
      BKDOC integer)
   as
declare variable s1 varchar(255); /* symbol */
declare variable s2 varchar(255); /* opis */
declare variable s3 varchar(255); /* kwota Winien */
declare variable s4 varchar(255); /* kwota Ma */
declare variable s5 varchar(255); /* data */
declare variable s6 varchar(255); /* termin platnosci */
declare variable s7 varchar(255); /* kod zewnetrzny kontrahenta */
declare variable s8 varchar(255); /* waluta */
declare variable s9 varchar(255); /* kurs */
declare variable s10 varchar(255); /* kwota Winien PLN */
declare variable s11 varchar(255); /* kwota Ma PLN */
declare variable s12 varchar(255); /* konto ksiegowe */
declare variable s13 varchar(255); /* oddzial */
declare variable s14 varchar(255); /* typ podmiotu (1 - klient, 6 - dostawca, 9 - pracownik) */
declare variable symbol varchar(20);
declare variable descript varchar(255);
declare variable winien numeric(14,2);
declare variable ma numeric(14,2);
declare variable opendate timestamp;
declare variable payday timestamp;
declare variable kod_zewn varchar(40);
declare variable currency varchar(3);
declare variable kurs numeric(14,4);
declare variable winienzl numeric(14,2);
declare variable mazl numeric(14,2);
declare variable kontofk varchar(20);
declare variable oddzial varchar(10);
declare variable stable varchar(40);
declare variable sref integer;
declare variable account varchar(20);
declare variable tmp integer;
begin

  select count(ref) from expimp where coalesce(char_length(s1),0)>20 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich symboli.';


  select count(ref) from expimp where coalesce(char_length(s7),0)>40 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich symboli zewnetrznych.';

  select count(ref) from expimp where coalesce(char_length(s8),0)>3 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich symboli waluty.';

  select count(ref) from expimp where coalesce(char_length(s12),0)>20 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich kont ksiegowych.';

  select count(ref) from expimp where coalesce(char_length(s13),0)>10 -- [DG] XXX ZG119346
  into :tmp;
  if(:tmp > 0) then
    exception universal :tmp || ' - liczba za dlugich oddzialow.';



  for select s1, s2,  s3,  s4,  s5,  s6,  s7,  s8,  s9,  s10,
            s11, s12, s13, s14
    from expimp 
  into :s1, :s2,  :s3,  :s4,  :s5,  :s6,  :s7,  :s8,  :s9,  :s10,
      :s11, :s12, :s13, :s14
  do begin

-- symbol
    symbol = substring(:s1 from  1 for  20);


-- opis
    descript = :s2;


-- kwota WN
    s3 = replace(:s3, ',', '.');
    winien = cast(:s3 as numeric(14,2));


-- kwota MA
    s4 = replace(:s4, ',', '.');
    ma = cast(:s4 as numeric(14,2));


-- data
    if(s5 like '____-__-__') then
      opendate = cast(:s5 as date);
    else
      exception universal 'Bledny format daty przy rozrachunku: ' || :s1;


-- termian platnosci
    if(s6 like '____-__-__') then
      payday = cast(:s6 as date);
    else
      exception universal 'Bledny format terminu platnosci przy rozrachunku: ' || :s1;


-- kod zewnetrzny
    kod_zewn = substring(:s7 from  1 for  40);

-- waluta
    currency = substring(:s8 from  1 for  3);


-- kurs
    s9 = replace(:s9, ',', '.');
    kurs = cast(:s9 as numeric(14,4));


-- kwota WN PLN
    s10 = replace(:s10, ',', '.');
    winienzl = cast(:s10 as numeric(14,2));


-- kwata MA PLN
    s11 = replace(:s11, ',', '.');
    mazl = cast(:s11 as numeric(14,2));


-- konto ksiegowe
    account = substring(:s12 from  1 for  20);

-- rozbicie account (wszystkie analityki oprocz ostatniej + kontofk kontrahenta)
    tmp = 0;
    kontofk=:account;
    while(position('-' in  :kontofk)>0)
    do begin
      tmp = tmp + position('-' in  :kontofk);
      kontofk = substring(:kontofk from position('-' in  :kontofk)+1 for coalesce(char_length(:kontofk),0)); -- [DG] XXX ZG119346
    end
    account= substring(:account from  1 for  tmp);

-- oddzial
    oddzial = substring(:s13 from  1 for  10);
    
    if (not exists(select first 1 1 from oddzialy where oddzial=:oddzial)) then
      exception universal 'Oddzial ' || :oddzial || ' nie istnieje.';


-- typ podmiotu
    if(:s14='1') then begin
      stable='KLIENCI';
      execute procedure get_symbol_from_number(cast(:kontofk as integer), 7) returning_values :kontofk;
      select ref from klienci where kontofk=:kontofk
      into :sref;
      if(sref is null) then
        exception universal 'Nie ma w bazie klienta o koncie ksiegowym: ' || :kontofk ;


    end else if (:s14='6') then begin
      stable='DOSTAWCY';
      execute procedure get_symbol_from_number(cast(:kontofk as integer), 7) returning_values :kontofk;
      select ref from dostawcy where kontofk=:kontofk
      into :sref;
      if(sref is null) then
        exception universal 'Nie ma w bazie dostawcy o koncie ksiegowym: ' || :kontofk ;

    end else if (:s14='9') then begin
      stable='EMPLOYEES';
      execute procedure get_symbol_from_number(cast(:kontofk as integer), 4) returning_values :kontofk;
      select ref from employees where symbol=:kontofk
      into :sref;
      if(sref is null) then
        exception universal 'Nie ma w bazie pracownika o koncie ksiegowym: ' || :kontofk ;

    end else
      exception universal 'Typ podmiotu rozrachunku: ' || :s1 || ' jest bledny.';


-- sklejenie account
    account = :account || :kontofk;


-- INSERTY
-- rozrachunek PLN
    if(currency='PLN' or currency='' or currency is null) then
    begin
      if(winienzl > 0) then
        execute procedure insert_decree_settlement(:bkdoc, :account, 0, :winienzl, :descript, :symbol,
                                 :opendate, 1, 1, :opendate,  :payday, :oddzial, :stable,  :sref, null, 0);
      if(mazl > 0) then
        execute procedure insert_decree_settlement(:bkdoc, :account, 1, :mazl, :descript, :symbol,
                                 :opendate, 3, 1, :opendate,  :payday, :oddzial, :stable,  :sref, null, 0);
    end
-- rozrachunek walutowy
    else
    begin
      if(winienzl > 0) then
        execute procedure insert_decree_currsettlement(:bkdoc, :account, 0, :winienzl, :winien, :kurs,
                                 :currency, :descript, :symbol, :opendate,  1, 1, :opendate,  :payday,
                                    :oddzial, :stable,  :sref, null, null, null, 0);
      if(mazl > 0) then
        execute procedure insert_decree_currsettlement(:bkdoc, :account, 0, :mazl, :ma, :kurs, :currency,
                                    :descript, :symbol, :opendate,  3, 1, :opendate,  :payday,
                                    :oddzial, :stable,  :sref, null, null, null, 0);
    end
  end
end^
SET TERM ; ^
