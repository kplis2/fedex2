--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_AVG_EMPLOYMENT(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      COL smallint)
  returns (
      AMOUNT numeric(14,2))
   as
declare variable okres integer;
  declare variable okres_pracy numeric(14,2);
  declare variable data1 date;
  declare variable data2 date;
  declare variable etat numeric(14,2);
  declare variable iyear smallint;
  declare variable imonth smallint;
  declare variable fromdate date;
  declare variable todate date;
  declare variable frvhdr integer;
  declare variable frpsn integer;
  declare variable frcol integer;
  declare variable company integer;
  declare variable cacheparams varchar(255);
  declare variable ddparams varchar(255);
  declare variable frversion integer;
  declare variable employee integer;
  declare variable maternity_leave integer;
begin
--Przeciętna liczba zatrudnionych
  select frvhdr, frpsn, frcol
    from frvpsns
    where ref = :frvpsn
    into :frvhdr, :frpsn, :frcol;

  select company, frversion
    from frvhdrs
    where ref = :frvhdr
    into :company, :frversion;
  cacheparams = period||';'||company||';'||col;
  ddparams = '';

  if (not exists (select ref from frvdrilldown where functionname = 'AVG_EMPL' and cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr)) then
  begin
    iyear = cast(substring(period from 1 for 4) as integer);
    imonth = cast(substring(period from 5 for 2) as smallint);

    if (col = 1) then
      fromdate = cast(iyear || '/' || imonth || '/1' as date);
    else
      fromdate = cast(iyear || '/1/1' as date);

    if (imonth = 12) then
      todate = cast((iyear + 1) || '/1/1' as date) - 1;
    else
      todate = cast(iyear || '/' || (imonth+1) || '/1' as date) - 1;

    okres = todate - fromdate + 1;
    okres_pracy = 0;
    amount = 0;
    for
      select EM.fromdate, EM.todate, EM.workdim, employee
        from employment EM left join edictworkposts EW on (EM.workpost = EW.ref)
          join employees E on (EM.employee = E.ref)
        where EM.fromdate <= :todate and (EM.todate >= :fromdate or EM.todate is null)
          and E.company = :company and E.econtrtype <> 9
        into :data1, :data2, :etat, :employee
    do begin
      data2 = coalesce(data2, todate);
      if (data2 > todate) then data2 = todate;
      if (data1 < fromdate) then data1 = fromdate;
      select coalesce(sum(ab.todate - ab.fromdate+1),0) from eabsences ab where ab.todate <= :data2 and ab.fromdate >=:data1 and ab.employee = :employee and ab.ecolumn = 260 into :maternity_leave;

      okres_pracy = okres_pracy + (data2 - data1 + 1 - :maternity_leave) * etat;
    end
    amount = okres_pracy / okres;
  end else
    select first 1 fvalue from frvdrilldown where cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr
      into :amount;

  insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
     values (:frvhdr, :frpsn, :frcol, 'AVG_EMPL', :cacheparams, :ddparams, :amount, :frversion);
  suspend;
end^
SET TERM ; ^
