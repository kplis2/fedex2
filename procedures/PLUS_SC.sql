--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PLUS_SC(
      MAGAZYN char(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      ILOSC numeric(15,5),
      CENA numeric(15,4),
      DOSTAWA integer,
      SERIALNR varchar(40) CHARACTER SET UTF8                           ,
      DATA timestamp,
      CENA_KOSZT numeric(14,4),
      ORGDOKUMSER integer,
      DATADOK timestamp)
   as
declare variable typ_mag char(1);
declare variable fifo char(1);
declare variable delzero integer;
declare variable ilpoz integer;
declare variable ilstan numeric(15,4);
declare variable censtan numeric(14,4);
declare variable refsc integer;
declare variable refser integer;
begin
          /*
  exception universal     'MAGAZYN ' ||coalesce(:magazyn,'brak')||' ktm '||coalesce(KTM,'brak')||
      ' wersja '||coalesce( WERSJA,'brak') ||' ILOSC '||coalesce(ilosc,'brak')||
    ' CENA '||coalesce(cena,'brak')||' DOSTAWA '||coalesce(dostawa,'brak')||
    ' SERIALNR '||coalesce(SERIALNR,'brak')||' DATA '||coalesce(:data,'brak')||
    ' CENA_KOSZT '||coalesce(CENA_KOSZT,'brak')||
    'ORGDOKUMSER '||coalesce(ORGDOKUMSER,'brak')||' DATADOK '||coalesce(DATADOK,'brak');
                */

  /* sprawdzenie czy nie bylo inwentaryzacji zamknietych z pozniejsza data*/
  if(exists (select first 1 1
               from inwentap inwp
               join inwenta inw on inwp.inwenta = inw.ref
               where inwp.ktm = :ktm and inwp.wersja = :wersja and inw.data > :data and inw.zamk = 2 and inw.magazyn=:magazyn)) then
    exception INWENTA_ZAAKCEPTOWANE 'Inwentaryzacja z pozycją '||:ktm||' zamknięta z datą późniejszą niż data dokumentu.';

  if(:ilosc is null) then ilosc = 0;
  if(:cena is null) then cena = 0;
  if(:dostawa is null) then dostawa = 0;
  if(:cena_koszt is null) then cena_koszt = 0;
  select TYP,FIFO,USUNZERA from DEFMAGAZ where SYMBOL=:magazyn into :typ_mag,:fifo,:delzero;
  if(:typ_mag = 'E') then begin
    select max(ref) from STANYCEN where MAGAZYN = :magazyn and KTM =:ktm and WERSJA=:wersja into :refsc;
    select cena,ilosc from STANYIL where MAGAZYN=:magazyn and KTM=:ktm and WERSJA = :wersja into :censtan,:ilstan;
    if(:ilstan <> 0 and :cena <> :censtan) then exception MAGDOK_WRONG_PRICE_E;
    if(:refsc >0) then
      update STANYCEN set ILOSC = ILOSC + :ilosc, cena = :cena where MAGAZYN=:magazyn and KTM = :ktm and WERSJA = :wersja and DOSTAWA = 0;
    else begin
      execute procedure GEN_REF('STANYCEN') returning_values :refsc;
      insert into STANYCEN(REF, MAGAZYN,KTM,WERSJA,ILOSC,CENA,DOSTAWA,DATA)
        values (:REFSC,:magazyn,:ktm,:wersja,:ilosc,:cena,0,:data);
    end
  end else
  if(:typ_mag = 'S') then begin
    select max(ref) from STANYCEN where MAGAZYN = :magazyn and KTM =:ktm and WERSJA=:wersja into :refsc;
    if(:refsc >0) then
      update STANYCEN set WARTOSC = WARTOSC+:ilosc*:cena,ILOSC = ILOSC + :ilosc where MAGAZYN=:magazyn and KTM = :ktm and WERSJA = :wersja and DOSTAWA = 0;
    else begin
      execute procedure GEN_REF('STANYCEN') returning_values :refsc;
      insert into STANYCEN(REF, MAGAZYN,KTM,WERSJA,ILOSC,CENA,WARTOSC,DOSTAWA,DATA)
        values (:REFSC,:magazyn,:ktm,:wersja,:ilosc,:cena,:ilosc*:cena, 0,:data);
    end
  end else
  if(:typ_mag = 'C' ) then begin
    if(:cena is null) then cena = 0;
    select max(ref) from STANYCEN where MAGAZYN = :magazyn and KTM =:ktm and WERSJA=:wersja and cena = :cena into :refsc;
    if(:refsc >0) then
      update STANYCEN set ILOSC = ILOSC + :ilosc, CENA_KOSZT = :cena_koszt where MAGAZYN=:magazyn and KTM = :ktm and WERSJA = :wersja and CENA = :cena and DOSTAWA = 0;
    else begin
      execute procedure GEN_REF('STANYCEN') returning_values :refsc;
      insert into STANYCEN(REF,MAGAZYN,KTM,WERSJA,ILOSC,CENA,DOSTAWA,DATA,CENA_KOSZT)
        values (:refsc, :magazyn,:ktm,:wersja,:ilosc,:cena,0,:data,:cena_koszt);
    end
  end else
  if(:typ_mag = 'P')then begin
    if((:dostawa is null) or (:dostawa = 0))then exception STCEN_DOSTAWA_NULL;
    select max(REF) from STANYCEN where MAGAZYN = :magazyn and KTM =:ktm and WERSJA=:wersja and cena = :cena and DOSTAWA = :dostawa into :refsc;
    if(:refsc >0) then begin
      if(:cena_koszt<>0) then
        update STANYCEN set ILOSC = ILOSC + :ilosc, CENA_KOSZT = :cena_koszt where MAGAZYN=:magazyn and KTM = :ktm and WERSJA = :wersja and CENA = :cena and DOSTAWA = :dostawa;
      else
        update STANYCEN set ILOSC = ILOSC + :ilosc where MAGAZYN=:magazyn and KTM = :ktm and WERSJA = :wersja and CENA = :cena and DOSTAWA = :dostawa;
    end else begin
      select max(REF) from STANYCEN where MAGAZYN = :magazyn and KTM =:ktm and WERSJA=:wersja and ILOSC = 0 and DOSTAWA = :dostawa into :refsc;
      if(:refsc >0) then begin
        if(:cena_koszt<>0) then
          update STANYCEN set ILOSC = ILOSC + :ilosc, CENA = :cena, CENA_KOSZT = :cena_koszt where REF=:refsc;
        else
          update STANYCEN set ILOSC = ILOSC + :ilosc, CENA = :cena where REF=:refsc;
      end else begin
        execute procedure GEN_REF('STANYCEN') returning_values :refsc;
        insert into STANYCEN(REF,MAGAZYN,KTM,WERSJA,ILOSC,CENA,DOSTAWA,DATA,CENA_KOSZT)
          values (:refsc,:magazyn,:ktm,:wersja,:ilosc,:cena,:dostawa,:data,:cena_koszt);
      end
    end
 end
 if(:serialnr <>'' and :refsc > 0) then begin
   select count(*) from STANYSER where STANCEN = :refsc and serialnr = :serialnr into :refser;
   if(:refser > 0) then begin
     update STANYSER set ILOSC = ILOSC+:ILOSC where STANCEN = :refsc and serialnr = :serialnr ;
   end else begin
     insert into STANYSER(STANCEN, SERIALNR,ILOSC,DATA) values(:refsc, :serialnr, :ilosc, :data);
   end
   update dokumser set stan = 'N' where ref = :orgdokumser and stan='W';
 end
 execute procedure STANYARCH_CHANGE('M',:magazyn,:ktm,:wersja,null,cast(:DATADOK as date),:ilosc,cast(:ilosc * :cena as numeric(14,2)), :dostawa);
 if(:delzero = 1) then
   delete from STANYCEN where ILOSC = 0 and MAGAZYN=:magazyn and KTM = :ktm and WERSJA=:wersja and ZAREZERW = 0 and ZABLOKOW = 0 and ZAMOWIONO = 0 and WARTOSC = 0;
end^
SET TERM ; ^
