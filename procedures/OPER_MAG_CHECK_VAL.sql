--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE OPER_MAG_CHECK_VAL(
      ZAM integer,
      OPER varchar(3) CHARACTER SET UTF8                           ,
      AUTOSER integer,
      AUTODYSPPMAG varchar(3) CHARACTER SET UTF8                           ,
      AUTODYSPBMAG varchar(3) CHARACTER SET UTF8                           ,
      AUTODYSPKMAG varchar(3) CHARACTER SET UTF8                           )
  returns (
      STATUS integer)
   as
DECLARE VARIABLE RESULT INTEGER;
DECLARE VARIABLE KTM VARCHAR(40);
DECLARE VARIABLE WERSJA INTEGER;
DECLARE VARIABLE POZREF INTEGER;
DECLARE VARIABLE ILZAM INTEGER;
DECLARE VARIABLE ILOSC DECIMAL(14,4);
DECLARE VARIABLE ILOSCR DECIMAL(14,4);
DECLARE VARIABLE ILREAL DECIMAL(14,4);
DECLARE VARIABLE ILDYSP DECIMAL(14,4);
DECLARE VARIABLE MAG CHAR(3);
DECLARE VARIABLE CENAMAG NUMERIC(14,4);
DECLARE VARIABLE DOSTAWA INTEGER;
DECLARE VARIABLE WYDANIA SMALLINT;
DECLARE VARIABLE AUTOSERROZPISZ SMALLINT;
DECLARE VARIABLE I NUMERIC(14,4);
DECLARE VARIABLE OLDILREALM NUMERIC(14,4);
DECLARE VARIABLE WERSJAREF INTEGER;
DECLARE VARIABLE KILREAL NUMERIC(14,4);
DECLARE VARIABLE NEWKILREAL NUMERIC(14,4);
DECLARE VARIABLE KTYMILREAL NUMERIC(14,4);
DECLARE VARIABLE KILWKOMPLET NUMERIC(14,4);
DECLARE VARIABLE JEDN INTEGER;
DECLARE VARIABLE ILOSCM DECIMAL(14,4);
DECLARE VARIABLE ILOSCRM DECIMAL(14,4);
DECLARE VARIABLE ILZDYSP DECIMAL(14,4);
DECLARE VARIABLE ILOSCZDYSP DECIMAL(14,4);
DECLARE VARIABLE ILREALM NUMERIC(14,4);
DECLARE VARIABLE PRZELICZ NUMERIC(14,4);
DECLARE VARIABLE USLUGA SMALLINT;
DECLARE VARIABLE DORE INTEGER;
DECLARE VARIABLE DODP INTEGER;
DECLARE VARIABLE DODB INTEGER;
DECLARE VARIABLE DODK INTEGER;
DECLARE VARIABLE DODD INTEGER;
DECLARE VARIABLE DODKREAL INTEGER;
DECLARE VARIABLE REONLYNAG SMALLINT;
DECLARE VARIABLE DYSPPROMTBLOCK INTEGER;
declare variable rewszystko integer;
declare variable ilonanotherm numeric(14,4);
declare variable repozzamout smallint;
begin
  /* procedura analizuje rekordy danego zamówienia i wpisue do pola
   ILREAL ilosci jakie mozna zrealizowac na dane zamowieniae z danego magazynu:
   Jesli towar jest serializowany, i jest wskazana aotuomatyczne uzupelnienie na operacji,
   to nastepuje automatyczne uzupelnienie pozycji do ich numerow seryjnych
   Zależnosci:
      MAG_CHEK_ILOSC - sprawdzenie ilosci dostepnej danego asortymentu na danym magazynie dla realizacji danego zamowienia */
  STATUS = -1;
  result = 1;
  if(:zam is null or (:zam = 0))then exit;
  select TYPZAM.WYDANIA from NAGZAM left join TYPZAM on (NAGZAM.typzam = TYPZAM.symbol) where NAGZAM.ref = :zam into :wydania;
  select REALIZACJA, DYSPP, DYSPB, DYSPK, DYSPD, DKISREAL, REONLYNAG, DYSPPROMTBLOCK, REWSZYSTKO, REPOZZAMOUT
  from DEFOPERZAM where symbol = :oper
  into :dore, :dodp, :dodb, :dodk, :dodd, :dodkreal, :reonlynag, :DYSPPROMTBLOCK, :rewszystko, repozzamout;
  if(repozzamout is null) then repozzamout = 0;
  if((:dore + :dodp + :dodb + :dodk + :dodd) = 0) then
   dore = 1;
  if(:DYSPPROMTBLOCK is null) then DYSPPROMTBLOCK = 0;
  if(:reonlynag is null) then reonlynag = 0;
  if(:rewszystko is null) then rewszystko = 0;
  if(:wydania is null)then wydania = 0;
  select count(*) from NAGZAM where REF=:zam into :ilzam;
  if(:ilzam <>1) then exception OPER_MAG_CHECK_VAL_NOT_ZAM;
  if(:wydania in (2,3) and :dore > 0) then begin
    update NAGZAM set KILREAL = 0 where REF=:zam;
    update NAGZAM set KILREAL = KILOSC - KILZREAL where REF=:zam;
    select KILREAL from NAGZAM where REF=:zam into :kilreal;
    newkilreal = :kilreal;
  end else begin
    update NAGZAM set AUTODYSPPMAG = :autodysppmag, AUTODYSPBMAG = :autodyspbmag, AUTODYSPKMAG = :autodyspkmag where REF=:zam;
  end
  update poZZAM set ILREAL = 0, ILREALM = 0 where ZAMOWIENIE = :zam;

  for
    select p.ref, p.ktm, p.wersja, p.wersjaref, p.magazyn, p.ilosc - p.ilzreal - p.ilzdysp,
        p.ilosc - p.ilzreal, p.ilrealm, p.iloscm - p.ilzrealm, p.ilzdysp, p.cenamag, p.dostawamag, p.jedn
      from pozzam p
      where p.zamowienie = :zam
        and (:repozzamout = 0 or p.out = (:repozzamout - 1))
      order by coalesce(p.fake,0) desc
     into :pozref,:ktm, :wersja, :wersjaref, :mag, :ilosc,
         :iloscr,:oldilrealm, :iloscm,:ilzdysp, :cenamag, :dostawa,:jedn
  do begin
     select WERSJE.usluga from WERSJE where REF=:wersjaref into :usluga;
     ilreal = 0;
     ildysp = 0;
     ilrealm = 0;
     iloscrm = :iloscm;
     przelicz = null;
     if(:ilzdysp > 0) then begin
       /*doliczenie ilosci magazynowych zdysponowanych do iloscm*/
         select PRZELICZ from TOWJEDN where ref=:jedn into :przelicz;
         if(:przelicz is null) then przelicz = 1;
         iloscm = :iloscm - :ilzdysp * :przelicz;
     end
     if(:wydania >0 and :mag <> '' and :usluga <> 1 and :dore = 1 and :rewszystko = 0) then begin

       execute procedure MAG_CHECK_ILOSC(:zam,:mag, :ktm, :wersja, :cenamag,:dostawa) returning_values :ilrealm;
       if(:ilrealm > 0) then begin
         ilonanotherm = 0;
         select sum(pozzam.ilrealm)
           from POZZAM
           where pozzam.ZAMOWIENIE = :zam and pozzam.KTM = :ktm and pozzam.wersja = :wersja and pozzam.MAGAZYN=:mag
           and ((pozzam.cenamag = :cenamag) or (:cenamag = 0) or (:cenamag is null))
           and ((pozzam.dostawamag = :dostawa) or (:dostawa = 0) or (:dostawa is null))
         into :ilonanotherm;
         if(:ilonanotherm is null) then ilonanotherm = 0;
         if(:ilonanotherm > 0) then
           ilrealm = :ilrealm - :ilonanotherm;
         if(:ilrealm < 0) then
           ilrealm = 0;
       end

       if(:ilrealm < :oldilrealm and :kilreal > 0 and :reonlynag = 0) then begin
         /*ustalenie ilosci kompletów mozliwych do realizowania*/
         kilwkomplet = :oldilrealm / :kilreal;
         ktymilreal = cast (:ilrealm / :kilwkomplet as integer);
         if(:ktymilreal < :newkilreal) then begin
           newkilreal = :ktymilreal;
         end
       end
     end else begin
       if(:dore = 1) then
         ilrealm = :iloscrm;
     end
     if(/*:wydania <> 3*/ 1=1) then begin
       if(:dore > 0 /*and :wydania <> 2*/) then begin
         if(:ilrealm < :iloscrm) then begin
           /*zamiana magazynowych na rozliczeniowe*/
           if(:przelicz is null) then begin
             select PRZELICZ from TOWJEDN where ref =:jedn into :przelicz;
            if(:przelicz is null) then przelicz = 1;
           end
           if(:przelicz > 0) then
             ilreal = :ilrealm / :przelicz;
           else
             ilreal = :ilrealm;
           result = 2;
         end else begin
           ilreal = :iloscr;
           ilrealm = :iloscrm;
         end
       end
       /*usuniecie rekordów dyspozycji, jakie mogly pozostac z poprzedniej operacji*/
       delete from POZZAMDYSP where POZZAM = :pozref;
       update POZZAM set ILDYSP = 0, AUTOPOZZAMDYSP = '' where ref=:pozref;
       /*sprawdzenie, czy montaz jest realizacją - jesli tak, to raczej wszystko do montazu, niz realizacja*/
       if(:dodp = 0 and :dodb = 0 and :dodk > 0 and :dore > 0 and :dodkreal > 0 and exists(select numer from towtypes where isproduct=1 and numer = :usluga)) then begin
         ilreal = 0;
         ilrealm = 0;
       end
       if((:dodp +:dodb + :dodk + :dodd )> 0 and :ilreal + :ildysp < :ilosc and :DYSPPROMTBLOCK = 0) then begin
         ildysp = :ilosc - :ilreal;
       end
       /*sprawdzenie, czy jesli ma trafic do montazu, to jest to produkt(komplet)*/
       if(:dodp = 0 and :dodb = 0 and dodk > 0 and not exists(select numer from towtypes where isproduct=1 and numer = :usluga)) then begin
          ildysp = 0;
       end
       if(:ilreal > 0) then
         update POZZAM set ILREAL = :ilreal, ILREALM = :ilrealm where ref = :pozref;
       else if (:ilrealm <> :oldilrealm) then
         update POZZAM set ILREAL = 0, ILREALM = 0 where ref = :pozref;
       if(:ildysp > 0) then
         update POZZAM set ILDYSP = ILDYSP + :ildysp, AUTOPOZZAMDYSP = :OPER where REF=:pozref;
       /*auto nadanie numerów seryjnych*/
       if(:wydania = 1 and :autoser = 1 and dore = 1) then begin
         select TOWARY.serautowyd from TOWARY where KTM = :ktm into :autoserrozpisz;
         if(:autoserrozpisz = 1) then begin
           i = null;
           select sum(ilosc) from DOKUMSER where REFPOZ = :pozref and typ = 'Z' into :i;
           if(:i is null) then i = 0;
           if(:i < :ilrealm) then begin
             i = :ilrealm - :i;
             execute procedure SERNR_AUTOROZPISZ(:pozref, 'Z',:mag, :wersjaref, :i);
           end
         end
       end
     end
  end
  if(:wydania in (2,3) and :dore > 0) then begin
    update nagzam set KILREAL = :newkilreal where REF=:zam and KILREAL <> :newkilreal;
  end
  STATUS = result;
end^
SET TERM ; ^
