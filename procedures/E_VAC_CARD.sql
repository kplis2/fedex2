--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE E_VAC_CARD(
      ONDATE timestamp,
      EMPL integer = null,
      COMPANY integer = null)
   as
declare variable EREF integer;
declare variable FIRSTJOB smallint;
declare variable VYEAR integer;
declare variable VACCARDYEAR integer;
declare variable ADDVLIMITM smallint;
declare variable ADDVDATE date;
declare variable DISVFROMDATE date;
declare variable DISVTODATE date;
declare variable VLIMITM integer;
declare variable DISABLELIMIT integer;
declare variable LIMITCONSM integer;
declare variable MAX_VYEAR integer;
declare variable ECOMPANY integer;
declare variable TVYEAR integer;
declare variable MREF integer;
declare variable vc2 smallint;
declare variable vmc smallint;
declare variable vrused integer;
declare variable vrcrtlimit integer;
declare variable mdcrtlimit integer;
declare variable mdused integer;
declare variable vrl smallint;
declare variable mdl smallint;
declare variable msg varchar(1024);
begin
--MW: Personel - zakladanie kart urlopowych

  vyear = cast(extract(year from ondate) as integer);

  execute procedure get_config('VACCARDYEAR', 2) returning_values vaccardyear;
  execute procedure get_config('VACREQCOLUMN', 2) returning_values :vc2;
  execute procedure get_config('VACMDCOLUMN', 2) returning_values :vmc;

  if (vyear < vaccardyear) then
    if (empl < 0) then vyear = vaccardyear;  --BS53156
    else exception universal 'Nie można naliczać kart dla tego roku!!!';

  if (coalesce(company,0) = 0) then
  begin
    execute procedure get_global_param('CURRENTCOMPANY') returning_values :company;
    company = coalesce(company,0);
  end

  empl = abs(coalesce(empl,0));

  if (empl = 0) then
  begin
    if (company = 0) then
      exception universal 'Błąd parametrów wejściowych';

    update evaclimits set actlimit = 0
      where vyear = :vyear and actlimit_cust = 0 and company = :company;
  end else
    select max(vyear) from evaclimits where employee = :empl
      into :max_vyear;

  max_vyear = coalesce(max_vyear, vyear);

  if(max_vyear < vyear) then max_vyear = vyear; --BS60822

  for
    select distinct e.ref, e.company, m.employee
        from employees e
        left join employment m on (m.employee = e.ref
                                 and m.fromdate <= cast(:vyear || '/12/31' as date)
                                 and (m.todate is null or m.todate >= cast(:vyear || '/1/1' as date)))
      where e.ref = :empl or (:empl = 0 and e.company = :company) --BS45713
      into :eref, :ecompany, mref
  do begin
    tvyear = vyear;
    while (tvyear <= max_vyear) do
    begin
      if (extract(year from :ondate) <> :tvyear) then
        ondate = cast(:tvyear || '/1/1' as date);
    --naliczanie limitow
      select vlimitm, addvlimitm, disablelimitm, limitconsm, addvdate,
             firstjob, disvfromdate, disvtodate
        from e_calculate_vaclimit(:eref, :tvyear)
        into :vlimitm, :addvlimitm, :disablelimit, :limitconsm, :addvdate,
             :firstjob, :disvfromdate, :disvtodate;
    --pobranie urlopow na zadanie i 188kp (PR60316)
      select sum(case when ecolumn = :vc2 then workdays else 0 end),
             sum(case when ecolumn = :vmc then workdays else 0 end)
        from eabsences
        where ayear = :tvyear
          and correction in (0,2)
          and employee = :eref
      into :vrused,:mdused;
      execute procedure e_get_reqmdlimits(:eref,:ONDATE) returning_values :vrl, :mdl, :msg;
    --Pobranie limitow wynikajacych ze swiadectwa pracy
      select sum(case when l.ecolumn = :vc2 then l.daylimit else 0 end),
             sum(case when l.ecolumn = :vmc then l.daylimit else 0 end)
        from ewrkcrtlimits l
          join eworkcertifs c on c.ref = l.workcertif
        where c.employee = :eref
          and extract(year from c.todate) = :tvyear
        into vrcrtlimit, mdcrtlimit;
      vrused = coalesce(vrused,0) + coalesce(vrcrtlimit,0);
      mdused = coalesce(mdused,0) + coalesce(mdcrtlimit,0);
    --zakladanie/aktualizacja kart
      if (mref is not null  --warunek do zerowania karty (BS53156)
        and not exists(select first 1 1 from evaclimits
                         where vyear = :tvyear and employee = :eref)
      ) then
        insert into evaclimits (employee, vyear, actlimit, addlimit, company,
            limitcons, addvdate, firstjob, disablelimit, disvfromdate, disvtodate,
            vacreqlimit, vacreqused, vacmdlimit, vacmdused
        ) values (:eref, :tvyear, :vlimitm, :addvlimitm, :ecompany,
            :limitconsm, :addvdate, :firstjob, :disablelimit, :disvfromdate, :disvtodate,
            :vrl, :vrused, :mdl, :mdused);
      else
        update evaclimits set firstjob = :firstjob, actlimit = :vlimitm,
            limitcons = :limitconsm, addlimit = :addvlimitm, addvdate = :addvdate, addvdate_cust = 2,
            disablelimit = :disablelimit, disvfromdate = :disvfromdate, disvtodate = :disvtodate,
            vacreqlimit = :vrl, vacreqused = :vrused, vacmdlimit = :mdl, vacmdused = :mdused
          where employee = :eref and vyear = :tvyear;

      tvyear = tvyear + 1;
    end
  end
end^
SET TERM ; ^
