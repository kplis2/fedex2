--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GENERATORS_RESTORE
  returns (
      ILPRZELICZ integer)
   as
declare variable STABLE varchar(31);
declare variable ID varchar(31);
declare variable PROCSTRING varchar(255);
declare variable REF integer;
declare variable LOC_STRING varchar(100);
declare variable WARUNEK varchar(100);
declare variable LOCATION integer;
declare variable AKTUGEN integer;
begin
  ILPRZELICZ = 0;
  execute procedure get_config('AKTULOCAT', 2)
    returning_values :loc_string;
  location = cast(:loc_string as integer);
  location = location - 1;
  for
    select substring(rdb$generators.rdb$generator_name from 5 for 27)
      from rdb$generators
      where substring(rdb$generators.rdb$generator_name from 1 for 4) like 'GEN_%'
      and substring(rdb$generators.rdb$generator_name from 5 for 27) <> 'S_NAVIGATOR'
      into :STABLE
  do begin
    if (exists(select first 1 1 from rdb$relations
            where rdb$relations.rdb$relation_name = :STABLE)
        and (exists(select first 1 1 from rdb$relation_fields f
            where f.rdb$field_name = 'REF' and f.rdb$relation_name = :stable)
          or (:stable in ('DEFCECHW','RPT_PAYSUMMARY_TABLE')))
    ) then
    begin
     if (:stable = 'DEFCECHW') then id = 'id_defcechw';
      else if (stable = 'RPT_PAYSUMMARY_TABLE') then id = 'print_no';
      else id = 'ref';
      warunek = '('||:id||' - ' || location || ') / 16 * 16 = ('||:id||' - ' || location || ')';
      procstring = 'select coalesce(cast(max(coalesce('||:id||',0)) as integer ),0 ) from ' || STABLE || ' where ' || warunek;
      execute statement procstring into :ref;
      if (ref is null) then ref = 0;
      ref = ref - location;
      if (exists(select tabela from RP_SYNCDEF where RP_SYNCDEF.tabela=:STABLE)) then
      begin
        warunek = '('||:id||' - ' || location || ') / 16 * 16 = ('||:id||' - ' || location || ')';
        procstring = 'select coalesce(cast(max(coalesce('||:id||',0)) as integer ),0 ) from ' || STABLE || ' where ' || warunek;
        execute statement procstring into :ref;
        if (ref is null) then ref = 0;
        ref = ref - location;
        ref = ref / 16;
      end
      else begin
        procstring = 'select coalesce(cast(max(coalesce('||:id||',0)) as integer ),0 ) from ' || STABLE;
        execute statement procstring into ref;
        if (ref is null) then ref = 0;
        ref = ref - location;
        ref = ref;
      end
      execute statement 'select cast(gen_id(GEN_' || STABLE
          || ', 0) as integer) from rdb$database'
        into :aktugen;
      if (ref <> aktugen and ref >= 0) then begin
        procstring = 'SET GENERATOR GEN_' || STABLE || ' to ' || cast(ref as integer);
        ILPRZELICZ = :ILPRZELICZ + 1;
        execute statement procstring;
      end
    end
  end
  suspend;
end^
SET TERM ; ^
