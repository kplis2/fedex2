--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDECL_DISCARD(
      DECLREF integer)
  returns (
      LOGMSG varchar(1024) CHARACTER SET UTF8                           )
   as
declare variable STATE integer;
declare variable ISGROUP smallint;
declare variable SCODE varchar(20);
declare variable PERS varchar(200);
declare variable DECLGROUP integer;
declare variable STATUS integer;
declare variable GROUPNAME varchar(40);
declare variable EDEDOCSEND integer;
declare variable EDEDOCCONFIRM integer;
declare variable PERSMSG varchar(255);
declare variable EYEAR integer;
declare variable GROUPSTATE smallint;
begin
--Wycofanie eDeklaracji DECLREF

  select d.state, df.isgroup, df.systemcode, coalesce(p.person,''), d.declgroup
       , d.status, d.eyear, coalesce(d.groupname,''), d.ededocsend, d.ededocconfirm
    from edeclarations d
      join edecldefs df on (df.ref = d.edecldef)
      left join persons p on (d.person = p.ref)
    where d.ref = :declref
    into state, isgroup, scode, pers, declgroup, status, eyear, groupname, ededocsend, :ededocconfirm;

  if (declgroup > 0) then
    select state, coalesce(groupname,ref) from edeclarations where ref = :declgroup
      into :groupstate, :groupname;

  if (pers <> '') then persmsg = pers;
  else if (groupname <> '') then persmsg = groupname;
  else persmsg = eyear;

  persmsg = coalesce(' ' || scode || ' : ' || persmsg || ' - ', '');

  if (status = 200) then
    logmsg = ascii_char(215) || persmsg || 'deklaracja jest już zatwierdzona przez Ministerstwo Finansów';
  else if (status between 301 and 399) then
    logmsg = ascii_char(215) || persmsg || 'dokument przetwarzany przez Ministerstwo Finansów';
  else if (isgroup = 0 and groupstate >= 1) then
    logmsg = ascii_char(215) || persmsg || 'nie można wycofać zgrupowanej deklaracji, gdyż deklaracja grupująca ''' || groupname || ''' jest już zatwierdzona';
  else if (state = 0) then
    logmsg = ascii_char(164) || persmsg || 'deklaracja jest już w statusie redakcji';

  if (logmsg > '') then
    exit;
  else begin
    delete from ededocsh where ref in (:ededocsend, :ededocconfirm);

    update edeclarations
      set state = 0,
          ededocsend = null,
          ededocconfirm = null
      where ref = :declref;

    if (isgroup = 1) then
      update edeclarations set state = 1 where declgroup = :declref;

    logmsg = ascii_char(149) || persmsg || 'wycofano deklarację';
    suspend;
  end

  when any do
  begin
    logmsg = ascii_char(215) || persmsg || 'nie udało się wycofać deklaracji';
    suspend;
  end
end^
SET TERM ; ^
