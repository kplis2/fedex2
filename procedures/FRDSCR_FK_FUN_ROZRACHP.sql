--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRDSCR_FK_FUN_ROZRACHP(
      KEY1 integer,
      KEY2 integer,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      TIMEELEM integer,
      ELEMENT integer,
      TYP integer)
  returns (
      RET numeric(14,2))
   as
declare variable winien numeric(14,2);
  declare variable ma numeric(14,2);
  declare variable saldo numeric(14,2);
  declare variable oddzialsymbol varchar(10);
  declare variable oddzial varchar(10);
  declare variable period char(6);
  declare variable iyear integer;
  declare variable imonth smallint;
  declare variable todate date;
begin
  ret = 0;
  saldo = 0;
  if (:TIMEELEM is null) then
    select frdcells.timelem
      from frdcells
      where frdcells.ref = :key1
      into :timeelem;
  if (:element is null) then
    select frdcells.dimelem
      from frdcells
      where frdcells.ref = :key1
      into :element;
  select first 1 shortname
    from frdimelems
    where dimelem = :element
    into :oddzialsymbol;
  select oddzial
    from oddzialy
    where symbol = :oddzialsymbol
    into :oddzial;
  select first 1 shortname
    from frdimelems
    where dimelem = :timeelem
    into :period;

  iyear = cast(substring(period from 1 for 4) as integer);
  imonth = cast(substring(period from 5 for 2) as smallint);
  if (imonth = 12) then
     todate = cast((iyear + 1) || '/1/1' as date) - 1;
  else
     todate = cast(iyear || '/' || (imonth+1) || '/1' as date) - 1;
  if (typ = 0) then
    for
      select sum(rozrachp.winienzl), sum(rozrachp.mazl)
        from rozrachp
        where (rozrachp.oddzial = :oddzial or :oddzial is null) and rozrachp.data <= :todate
        group by kontofk, symbfak, oddzial
        having sum(winienzl) - sum(mazl) > 0
        into :winien, :ma
    do begin
      winien = coalesce(winien, 0);
      ma = coalesce(ma, 0);
      saldo = saldo + winien - ma;
    end
  if (typ = 1) then
    for
      select sum(rozrachp.winienzl), sum(rozrachp.mazl)
        from rozrachp
        where (rozrachp.oddzial = :oddzial or :oddzial is null) and rozrachp.data <= :todate
        group by kontofk, symbfak, oddzial
        having sum(winienzl) - sum(mazl) < 0
        into :winien, :ma
    do begin
      winien = coalesce(winien, 0);
      ma = coalesce(ma, 0);
      saldo = saldo + ma - winien;
    end
  ret = saldo;
  suspend;
end^
SET TERM ; ^
