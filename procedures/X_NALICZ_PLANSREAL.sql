--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_NALICZ_PLANSREAL(
      PLANS integer,
      PLANSCOL integer,
      PLANSROW integer)
  returns (
      VAL numeric(14,4))
   as
DECLARE VARIABLE KEY1 VARCHAR(20);
DECLARE VARIABLE TEMPODNI NUMERIC(14,4);
DECLARE VARIABLE TEMPOOKR NUMERIC(14,4);
DECLARE VARIABLE BDATA TIMESTAMP;
DECLARE VARIABLE LDATA TIMESTAMP;
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE iloscwyd NUMERIC(14,4);
DECLARE VARIABLE ilosczwr NUMERIC(14,4);
begin
  select PLANSCOL.BDATA, PLANSCOL.LDATA from PLANSCOL where PLANSCOL.ref = :PLANSCOL
    into :BDATA, :LDATA;
  select PLANSROW.KEY1 from PLANSROW
    where PLANSROW.plans = :PLANS and PLANSROW.ref = :PLANSROW into :key1;
  select sum(case when (defdokum.wydania = 1  and defdokum.koryg = 0) then dokumpoz.ilosc else 0 end),
    sum(case when (defdokum.wydania = 0  and defdokum.koryg = 1) then dokumpoz.ilosc else 0 end)
    from dokumpoz
    left join dokumnag on (dokumnag.ref = dokumpoz.dokument)
    left join defdokum on (dokumnag.typ = defdokum.symbol)
    where dokumpoz.ktm = :key1  and defdokum.zewn = 1 and
    ((defdokum.wydania = 1  and defdokum.koryg = 0)
    or (defdokum.wydania = 0  and defdokum.koryg = 1)) and dokumnag.akcept = 1
    and dokumnag.data <= :LDATA and dokumnag.data >= :BDATA
    into :iloscwyd, :ilosczwr;
  val = :iloscwyd - :ilosczwr;
  if (:val is null) then val = 0;
end^
SET TERM ; ^
