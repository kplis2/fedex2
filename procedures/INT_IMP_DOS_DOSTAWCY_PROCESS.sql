--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INT_IMP_DOS_DOSTAWCY_PROCESS(
      SESJAREF SESJE_ID,
      TABELA STRING35,
      REF INTEGER_ID,
      BLOKUJZALEZNE SMALLINT_ID = 0,
      TYLKONIEPRZETWORZONE SMALLINT_ID = 0,
      AKTUALIZUJSESJE SMALLINT_ID = 1)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable stabela string35;
declare variable tmptabela string35;
declare variable sprocedura string255;
declare variable szrodlo zrodla_id;
declare variable skierunek smallint_id;
declare variable sesja sesje_id;
declare variable sql memo;
declare variable tmpsql memo;
declare variable tmpstatus smallint_id;
declare variable tmpmsg string255;
declare variable error_row smallint_id;
declare variable errormsg_row string255;  --ML blad przetwarzania pojedynczego dostawcy
declare variable errormsg_all string255;  --ML blad przetwarzania wszystkich dostawców
declare variable statuspoz smallint_id;

--dostawca
declare variable d_ref integer_id;
declare variable d_dostawcaid string120;
declare variable d_id string255;
declare variable d_firma integer_id;
declare variable d_nazwa string255;
declare variable d_nip string30;
declare variable d_regon string30;
declare variable d_kraj string80;
declare variable d_krajid string20;
declare variable d_ulica string120;
declare variable d_nrdomu string40;
declare variable d_nrlokalu string40;
declare variable d_kodpocztowy string40;
declare variable d_miasto string120;
declare variable d_telefon string255;
declare variable d_fax string120;
declare variable d_email string255;
declare variable d_www string255;
declare variable d_uwagi memo;
declare variable d_aktywny integer_id;
declare variable d_dnirealizacji integer_id;
declare variable d_limitkredytowy kurs_id;
declare variable d_waluta string20;
declare variable d_rabat kurs_id;
declare variable d_typdokmag string20;
declare variable d_bank string255;
declare variable d_rachunek string60;
declare variable d_hash string255;
declare variable d_del integer_id;
declare variable d_rec string255;
declare variable d_skadtabela string40;
declare variable d_skadref integer_id;
declare variable d_zaplataid string;
declare variable d_zaplata string;
declare variable d_dostawa string;
declare variable d_dostawaid string;
--zmienne ogolne
declare variable sposdost sposdost_id;
declare variable sposzap platnosc_id;
declare variable oddzial oddzial_id;
declare variable company companies_id;
declare variable dostawca dostawca_id;
declare variable dostawcazrodla dostawca_id;
begin

  --zmiana pustych wartosci na null-e
  if (sesjaref = 0) then
    sesjaref = null;
  if (trim(tabela) = '') then
    tabela = null;
  if (ref = 0) then
    ref = null;
  if (blokujzalezne is null) then
    blokujzalezne = 0;
  if (tylkonieprzetworzone is null) then
    tylkonieprzetworzone = 0;
  if (aktualizujsesje is null) then
    aktualizujsesje =1;

  status = 1;
  msg = '';
  errormsg_all = '';

  --sprawdzenie czy wszystkie potrzebne informacje sa
  if (sesjaref is null and ref is null) then
  begin
    status = -1;
    msg = 'Za malo parametrow.';
    exit; --EXIT
  end
  if (sesjaref is null) then
  begin
    if (ref is null) then
    begin
      status = -1;
      msg = 'Jesli nie podales sesji to wypadaloby podac ref-a...';
      exit; --EXIT
    end
    if (tabela is null) then
    begin
      status = -1;
      msg = 'Wypadaloby podac nazwe tabeli...';
      exit; --EXIT
      --sql = 'select sesja from INT_IMP_DOS_DOSTAWCY where ref='||:ref;
      --execute statement sql into :sesja;
    end
    else
    begin
      sql = 'select sesja from '||:tabela||' where ref='||:ref;
      execute statement sql into :sesja;
    end
  end
  else
    sesja = sesjaref;

  if (sesja is null) then
  begin
    status = -1;
    msg = 'Nie znaleziono numeru sesji.';
    exit; --EXIT
  end

  --pobranie parametrow sesji
  select s.tabela, s.procedura, s.zrodlo, s.kierunek
    from int_sesje s
    where ref = :sesja
    into :stabela, :sprocedura, :szrodlo, :skierunek;

  --if (tabela is not null) then --jesli podano tabele na wejsciu traktuje ja nadrzednie
    --stabela = tabela;

  if(coalesce(:szrodlo,-1) = -1)then
  begin
    status = -1;
    msg = 'Brakuje id zrodla';
    exit;
  end

  oddzial = null;
  company = null;
  select oddzial, company, dostawca from int_zrodla where ref = :szrodlo
    into :oddzial, :company, :dostawcazrodla;
  if (oddzial is null) then
    oddzial = 'CENTRALA';
  if (company is null) then
    company = 1;


  --wlasciwe przetworzenie

  --if (tabela = 'INT_IMP_ZAM_NAGLOWKI') then
    --begin
  for select d.ref, d.dostawcaid, d.id, d.firma, d.nazwa, d.nip,  d.regon,
    d.kraj, d.krajid, d.ulica, d.nrdomu, d.nrlokalu, d.kodpocztowy, d.miasto,
    d.telefon, d.fax, d.email, d.www, d.uwagi, d.aktywny, d.dnireal,
    d.limitkredytowy, d.waluta, d.rabat, d.typdokmag, d.bank,
    d.rachunek, d."HASH", d.del, d.rec, d.skadtabela, d.skadref,
    d.zaplata, d.zaplataid, d.dostawa, d.dostawaid
    from INT_IMP_DOS_DOSTAWCY d
    where (:sesja is null or d.sesja = :sesja)
      and ((:ref is not null and d.ref = :ref and :tabela is null)
      or (:ref is not null and :tabela is not null and d.skadref = :ref and d.skadtabela = :tabela)
      or (:ref is null))
    order by ref
    into :d_ref, :d_dostawcaid, :d_id,  :d_firma, :d_nazwa, :d_nip, :d_regon,
      :d_kraj, :d_krajid, :d_ulica, :d_nrdomu, :d_nrlokalu, :d_kodpocztowy, :d_miasto,
      :d_telefon, :d_fax, :d_email, :d_www, :d_uwagi, :d_aktywny, :d_dnirealizacji,
      :d_limitkredytowy, :d_waluta, :d_rabat, :d_typdokmag, :d_bank,
      :d_rachunek, :d_hash, :d_del, :d_rec, :d_skadtabela, :d_skadref,
      :d_zaplata, :d_zaplataid, :d_dostawa, :d_dostawaid

  do begin
    error_row = 0;
    errormsg_row = '';

    if (coalesce(trim(d_dostawcaid),'') = '') then
    begin
      error_row = 1;
      errormsg_row = substring(errormsg_row || ' ' || 'Brak ID dostawcy.'||coalesce(d_nazwa,'') from 1 for 255);
    end

    if (coalesce(d_del,0) = 1 and error_row = 0) then --rekord do skasowania
    begin
      tmpstatus = null;
    end
    else if (coalesce(d_del,0) = 0 and error_row = 0) then
    begin
      if (coalesce(trim(d_nazwa),'') = '') then
      begin
        error_row = 1;
        errormsg_row = substring(errormsg_row || ' ' || 'Brak nazwy dostawcy.' from 1 for 255);
      end
      if (coalesce(trim(d_nip),'') = '') then
        d_nip = null;
      if (d_firma is null) then
      begin
        if (d_nip is not null) then
          d_firma = 1;
        else
          d_firma = 0;
      end
      if (coalesce(trim(d_limitkredytowy),'') = '') then
        d_limitkredytowy = null;
      if (coalesce(trim(d_waluta),'') = '') then --dodac walidacje !!!!
        d_waluta = 'PLN';
      if (coalesce(trim(d_krajid),'') = '') then --dodac walidacje !!!!
        d_krajid = 'PL';
      if (d_aktywny is null) then
        d_aktywny = 1;

      sposzap = null;
      sposdost = null;

      if(:d_zaplata is not null or :d_zaplataid is not null)then
        select p.ref from int_slownik s
          join platnosci p on p.ref = s.oref
          where s.zrodlo = :szrodlo
            and (:d_zaplata is null or s.wartosc is not distinct from :d_zaplata)
            and (:d_zaplataid is null or s.wartoscid is not distinct from :d_zaplataId)
            and s.otable = 'PLATNOSCI'
          into :sposzap;
      if(:d_dostawa is not null or :d_dostawaid is not null)then
        select sd.ref from int_slownik s
          join sposdost sd on sd.ref = s.oref
          where s.zrodlo = :szrodlo
            and (:d_dostawa is null or s.wartosc is not distinct from :d_dostawa)
            and (:d_dostawcaid is null or s.wartoscid is not distinct from :d_dostawaid)
            and s.otable = 'SPOSDOST'
          into :sposdost;


      /*
      select p.ref from platnosci p where p.id_zewn is not null and p.id_zewn = :d_zaplataid
        into :sposzap;
      select sd.ref from sposdost sd where sd.id_zewn is not null and sd.id_zewn = :d_dostawaid
        into :sposdost;
      */


      if(not exists(select first 1 1 from waluty w where w.symbol is not distinct from :d_waluta)
      )then
      begin
        error_row = 1;
        errormsg_row = substring(
          errormsg_row || ' ' || 'W słowniku nie ma waluty o symbolu: ' || coalesce(:d_waluta,'<pusty>') from 1 for 255);

      end
      if(not exists(select first 1 1 from countries c where c.symbol is not distinct from :d_krajid)
      )then
      begin
        error_row = 1;
        errormsg_row = substring(
          errormsg_row || ' ' || 'W słowniku nie ma kraju o symbolu: ' || coalesce(:d_krajid,'<pusty>') from 1 for 255);
      end

      --dopisac obsluge !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      d_bank = null;
      if(coalesce(error_row, 1) = 0)then
      begin
        if (:blokujzalezne = 0) then --tabele zalezne BEFORE
        begin
          select status, msg from int_tabelezalezne_process(:sesja,
              :stabela, :ref, :szrodlo, :skierunek, 0)
            into :tmpstatus, :tmpmsg;
        end

        update or insert into dostawcy(id, firma, nazwa, nip, regon, kraj, krajid,
          ulica, nrdomu, nrlokalu, kodp, miasto, telefon, fax,  email, www,
          uwagi, aktywny, dnireal, waluta, rabat, typdokmag, bank, rachunek,
          company, sposplat, sposdost,
          int_zrodlo, int_id, parent) --, int_dataostprzetw, int_sesjaostprzetw)
          values(:d_id, :d_firma, :d_nazwa, :d_nip, :d_regon, :d_kraj, :d_krajid,
            :d_ulica, :d_nrdomu, :d_nrlokalu, :d_kodpocztowy, :d_miasto, :d_telefon,
            :d_fax, :d_email, :d_www,
            :d_uwagi, :d_aktywny, :d_dnirealizacji, :d_waluta, :d_rabat, :d_typdokmag,
            :d_bank, :d_rachunek, :company, :sposzap, :sposdost,
            :szrodlo, :d_dostawcaid, :dostawcazrodla) --, current_timestamp, :sesja)
          matching (int_id, int_zrodlo)
          returning ref into :dostawca;
  
        if (coalesce(trim(d_skadtabela), '') <> ''
            and coalesce(d_skadref,0) > 0 ) then
        begin
          sql = 'update '|| :d_skadtabela ||' set dostawca_sente = '|| :dostawca ||
            ' where ref = '|| :d_skadref;
          execute statement sql;
        end

        if (:blokujzalezne = 0) then --tabele zalezne after
        begin
          select status, msg from int_tabelezalezne_process(:sesja,
              :stabela, :ref, :szrodlo, :skierunek, 1)
            into :tmpstatus, :tmpmsg;
        end
      end
    end
    if(:error_row = 1)then
    begin
      status = -1; --ML jezeli nie przetworzy sie chociaz jeden wiersz przetwarzanie nieudane;
      errormsg_all = substring(coalesce(errormsg_all,'') || :errormsg_row from 1 for 255);
    end
    update int_imp_dos_dostawcy d set d.dostawca = :dostawca,
        dataostprzetw = current_timestamp(0),
        d.error = :error_row,
        d.errormsg = :errormsg_row
        where ref = :d_ref;
  end
    --end

  if(coalesce(status,-1) != 1) then
    msg = substring((coalesce(msg,'') || :errormsg_all) from 1 for 255);

  --aktualizacja informacji o sesji
  if (aktualizujsesje = 1) then
  begin
    select status, msg from int_sesje_aktualizuj(:sesja, :status) into :tmpstatus, :tmpmsg;
    if(coalesce(status,-1) = 1) then
      status = :tmpstatus; --ML jezeli przed aktualizacjas sesji status byl zly, to nie zmieniamy na ok
    msg = substring((msg || coalesce(:tmpmsg,'')) from 1 for 255);
  end
  suspend;
end^
SET TERM ; ^
