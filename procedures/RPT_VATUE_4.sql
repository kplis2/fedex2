--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_VATUE_4(
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      COMPANY COMPANIES_ID,
      USERFNAME STRING255,
      USERSNAME STRING255,
      DOCDATE STRING10,
      OPERATION SMALLINT_ID = 0,
      EDECLARATIONREF EDECLARATIONS_ID = 0,
      TESTCPOZROWCOUNT SMALLINT_ID = 0,
      TESTDPOZROWCOUNT SMALLINT_ID = 0,
      TESTEPOZROWCOUNT SMALLINT_ID = 0,
      VK7_IN SMALLINT_ID = 0)
  returns (
      K1 STRING10,
      K2 STRING35,
      K3 SMALLINT_ID,
      K4 STRING2,
      K5 YEARS_ID,
      K6 STRING255,
      K7 SMALLINT_ID,
      VK7 SMALLINT_ID,
      K8 STRING255,
      K9 STRING255,
      K10 STRING255,
      K11 STRING255,
      K12 STRING10)
   as
declare variable syscode string35;
declare variable edecldefref edecldefs_id;
declare variable old_edecldefref edecldefs_id = 0;
declare variable edecpos_field type of column edeclpos.field;
declare variable usp string255;
declare variable filed_prefix char_1;
begin
/*TS: FK - procedura obslugujaca tradycyjny wydruk deklaracji oraz jej
  elektroniczny odpowiednik. Procedura dziala w jednym z nastepujacych trybow,
  ktory jest okreslany przez parametr OPERATION.

  > 0: Procedura generuje sam wydruk tj. zwraca tylko dane potrzebne do
       przygotowania wydruku. W takim przypadku generowany dokument
       nigdy nie bedzie korekta.
  > 1: Generuje dane potrzebne do e-Deklaracji tj. tworzy jej naglowek oraz
       dodaje pozycje deklaracji do EDECLPOS. Generowana jest korekta.
  > 2: Generuje dane potrzebne do e-Deklaracji jak wyzej. Generowana jest korekta.
  > 3: Przelicza wskazana e-Deklaracje o podanym EDECLARATIONREF.
       Nie bedzie generowany nowy naglowek, zostana tylko na nowo
       wygenerowane pozycje dla e-Deklaracji w EDECLPOS (sa one wpierw usuwane).
  > 4: Przelicza wskazana e-Deklaracje o podanym EDECLARATIONREF jak wyzej.
       Generowana jest korekta.

  Procedura przyjmuje dodatkowo opcjonalne parametry:
  > TESTXPOZROWCOUNT - liczba testowych pozycji dla sekcji X na e-Deklaracji.
    Jesli w systemie nie ma prawdziwych danych (lub bedziemy chcieli wysylac
    dokument na bramke testowoa, gdzie wymagane sa fikcyjne dane),
    mozemy w ten sposob przetestowac calosc e-Deklaracji na sucho.
  > VK7_IN - wymuszenie stylu wydruku, ignorujac parametr OPERATION (np. chcemy
    wydrukowac korekte, ale bez generowania e-deklaracji).
*/

  -- Symbol e-Deklaracji
  syscode = 'VAT-UE (4)';
  -- Prefix nazw pol e-Deklaracji.
  filed_prefix = 'K';

  -- Jesli generujemy korekte istniejacego dokumentu lub go przeliczamy.
  if (:operation > 1 and :edeclarationref = 0) then
  begin
    exception universal 'W przypadku generowania korekty / przeliczania e-Deklaracji'
      ||' należy wskazać e-Deklarację, której korekta dotyczy / którą chcemy przeliczyć.';
  end

  -- Jesli podalismy numer dokumentu, musimy zweryfikowac czy istnieje i czy okresy sie zgadzaja.
  if (:edeclarationref > 0) then
  begin
    if (not exists(select first 1 1 from edeclarations e where e.ref = :edeclarationref)) then
    begin
      exception universal 'Wskazana deklaracja, dla której ma być wygenerowana korekta, nie istnieje.';
    end else if ((select e.period from edeclarations e where e.ref = :edeclarationref) != :period) then
    begin
      exception universal 'Okres składania korekty deklaracji nie zgadza się'
        ||' z okresem deklaracji, dla której korekta jest generowana.';
    end
  end

  -- Generuj lub przeliczaj korekte e-Deklaracje
  -- W przypadku VAT-UE (4) mamy osobne wydruki:
  -- VAT-UE (4) oraz VAT-UEK (4) w charakterze korekty.
  -- Jako że ten drugi jest tylko rozszeżeniem pierwszego
  -- o dodatkowe informacje z deklaracji korygowanej,
  -- obslugujemy ja na tym samym wydruku.
  if (:operation in (2, 4) or :vk7_in = 2) then
  begin
    -- korekta
    vk7 = 2;
    old_edecldefref = :edeclarationref;
    syscode = 'VAT-UEK (4)';
  end else
  begin
    vk7 = 1;
  end

  -- Sprawdzenie, czy istnieje definicja e-Deklaracji.
  select e.ref
    from edecldefs e
    where e.systemcode = :syscode
  into :edecldefref;

  -- Generowanie danych do wydruku.

  execute procedure e_get_platnikinfo4vat(:period, :company)
    returning_values :k1,  -- NIP
      :k2,                 -- numer dokumentu
      :k3,                 -- status dokumentu
      :k4,                 -- miesiac w formacie MM
      :k5,                 -- rok w formacie RRRR
      :k6,                 -- nazwa urzedu skarbowego
      :k7,                 -- 1-podatnik niefizyczny, 2-fizy
      :k8,                 -- pelna nazwa
      :usp;                -- kod urzedu skarbowego

  k9 = :userfname;         -- imie
  k10 = :usersname;        -- nazwisko

  select coalesce(k.cvalue,'')
    from get_config('PODTEL', 0) k
  into :k11;               -- numer telefonu

  k12 = :docdate;          -- data dokumentu

  -- Usuwamy pozycje deklaracji dla przeliczenia.
  if (:operation in (3, 4)) then
  begin
    delete from edeclpos e
      where e.edeclaration = :edeclarationref;
  end

  -- Generujemy dane do e-Deklaracji.
  if (:operation > 0) then
  begin
    if (:operation in (1, 2)) then
    begin
      execute procedure edeclaration_add_header(:edecldefref, :period)
        returning_values :edeclarationref;

      -- Jesli generujemy lub przeliczamy korekte.
      if (:operation in (2, 4) or :vk7_in = 2) then  /* :old_edecldefref > 0 */
      begin
        update edeclarations e
          set e.correction = :old_edecldefref
          where e.ref = :edeclarationref;
      end
    end

    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, 1, :k1, 0, :filed_prefix||'1');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, 2, :k2, 0, :filed_prefix||'2');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, 3, :k3, 0, :filed_prefix||'3');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, 4, :k4, 0, :filed_prefix||'4');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, 5, :k5, 0, :filed_prefix||'5');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, 6, :k6, 0, :filed_prefix||'6');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, 7, :k7, 0, :filed_prefix||'7');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, 8, :k8, 0, :filed_prefix||'8');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, 9, :k9, 0, :filed_prefix||'9');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, 10, :k10, 0, :filed_prefix||'10');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, 11, :k11, 0, :filed_prefix||'11');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, 12, :k12, 0, :filed_prefix||'12');
    insert into edeclpos (edeclaration, field, pvalue, origin, fieldsymbol)
      values (:edeclarationref, 13, :usp, 0, 'USP');

    -- Wygeneruj dane dla sekcji C dokumentu dla e-Deklaracji.
    execute procedure rpt_vatue_4_poz(:period, :company,
      :edeclarationref, 14, 'C',
      :old_edecldefref, :testcpozrowcount)
    returning_values edecpos_field;

    -- Wygeneruj dane dla sekcji D dokumentu dla e-Deklaracji.
    execute procedure rpt_vatue_4_poz(:period, :company,
      :edeclarationref, :edecpos_field, 'D',
      :old_edecldefref, :testdpozrowcount)
    returning_values edecpos_field;

    -- Wygeneruj dane dla sekcji E dokumentu dla e-Deklaracji.
    execute procedure rpt_vatue_4_poz(:period, :company,
      :edeclarationref, :edecpos_field, 'E',
      :old_edecldefref, :testepozrowcount)
    returning_values edecpos_field;
  end
  suspend;
end^
SET TERM ; ^
