--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_NOTIF_VERSIONHISTORY
  returns (
      TABELA varchar(8191) CHARACTER SET UTF8                           ,
      SYMBOL STRING255)
   as
declare variable EOL varchar(10); 
declare variable SEND smallint; 
declare variable KTO STRING255; 
declare variable CO STRING255; 
declare variable ODKIEDY TIMESTAMP_ID; 
declare variable DOKIEDY TIMESTAMP_ID; 
declare variable ILERAZY SMALLINT_ID; 
declare variable LICZBADNI SMALLINT_ID; 
declare variable DATA DATE_ID;
declare variable DESCRIPT varchar(255);
begin 
/*eol=' 
'; 
 send = 0; 
 liczbadni = 1; 
 if (EXTRACT(WEEKDAY from current_date) = 1) then liczbadni = 3; 
 data = current_date - liczbadni; 
 symbol = 'VERHIST_X' || current_date || 'X';
 tabela = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1250" /></head>
<body>';
 tabela = tabela || '<h2>Lista zmian w bazie sente od dnia '||data||' w podziale na użykowników</h2>';
 tabela = tabela || '<table cellpadding="5"> 
 <tr bgcolor="#C0C0C0"> 
 <th>Kto</th> 
 <th>Co zmieniał</th> 
 <th>Od kiedy</th> 
 <th>Do kiedy</th> 
 <th>Ile razy</th> 
 <th>Opis</th>
 </tr>'; 
 for 
   select h.ibe$vh_user_name, h.ibe$vh_object_name, min(h.ibe$vh_modify_date), max(h.ibe$vh_modify_date), count(1), max(h.ibe$vh_description)
     from IBE$VERSION_HISTORY h where h.ibe$vh_modify_date >= :data 
     group by h.ibe$vh_user_name, h.ibe$vh_object_name 
     order by h.ibe$vh_user_name 
   into :kto, :co, :odkiedy, :dokiedy, :ilerazy, :descript
 do begin 
     tabela = tabela||eol|| '<tr><td>' || :kto || '</td>'|| 
     '<td>' || :co || '</td>'|| 
     '<td>' || :odkiedy || '</td>'|| 
     '<td>' || :dokiedy|| '</td>'|| 
     '<td>' || :ilerazy|| '</td>'|| 
     '<td>' || coalesce(:descript,'-')|| '</td>'||
     '</tr>'; 
     send = 1; 
 end 
 tabela = tabela || '</table>';
 tabela = tabela || '<br><br>Niniejsza wiadomość została wygenerowana automatycznie przy pomocy <b>Sente S4</b>
<br>Prosimy na nią nie odpowiadać. 
</body></html>';
 --tabela = tabela || '<br>'; 
 if (send = 1) then */
   suspend; 
end^
SET TERM ; ^
