--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CHECK_MWSORDTYPE(
      MWSORD MWSORDS_ID)
  returns (
      MWSALLOWMORE SMALLINT_ID)
   as
begin
  mwsallowmore = 0;
  
  select coalesce(t.mwsallowmore,0)
    from mwsords o
      left join mwsordtypes4wh t on (o.mwsordtype = t.mwsordtype and o.wh = t.wh)
    where o.ref = :mwsord
  into :mwsallowmore;

  suspend;
end^
SET TERM ; ^
