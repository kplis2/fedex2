--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE GEN_EDEDOCSH_FROM_EDECL(
      DECLREF integer)
  returns (
      EDOCREF integer)
   as
declare variable SYM varchar(10);
declare variable VAR varchar(10);
declare variable SCHM varchar(10);
declare variable NUM varchar(10);
declare variable PROC varchar(4096);
declare variable EDRULE integer;
declare variable STATUS smallint;
declare variable PFNAME varchar(60);
declare variable PSNAME varchar(30);
declare variable EYEAR integer;
declare variable EDERULEGEN integer;
declare variable EDERULECONFIRM integer;
declare variable FILENAME varchar(255);
declare variable EMONTH smallint;
declare variable GROUPNAME varchar(1024);
begin
  select e.state,ed.symbol, ed.variant, ed.schemaver, ed.code,
         p.fname,p.sname,e.eyear,ed.ederulegen, ed.ederuleconfirm, e.emonth,
         e.groupname
    from edeclarations e
      join edecldefs ed on ed.ref = e.edecldef
      left join persons p on p.ref = e.person
    where e.ref = :declref
  into :status,:sym, :var, :schm, :num, :pfname, :psname, :eyear,
       :ederulegen,:ederuleconfirm, :emonth, :groupname;
  
  filename = REPLACE(:sym,'-','')||'('||:var||')';
  if(coalesce(:groupname,'') <> '') then filename = :filename||replace(:groupname,'-','');
  if(coalesce(:pfname,'') <> '') then filename = :filename||:pfname;
  if(coalesce(:psname,'') <> '') then filename = :filename||:psname;
  if(coalesce(:eyear, 0) <> 0) then filename = :filename||:eyear;
  if(coalesce(:emonth,0) <> 0) then filename = :filename||:emonth;
  filename = :filename||'.xml';
  filename = replace(:filename,'/','_');
  filename = replace(:filename,'\','_');
  filename = replace(:filename,':','_');
  filename = replace(:filename,'*','_');
  filename = replace(:filename,'?','_');
  filename = replace(:filename,'''','_');
  filename = replace(:filename,'"','_');
  filename = replace(:filename,'<','_');
  filename = replace(:filename,'>','_');
  filename = replace(:filename,'|','_');
  
  if(status = 1) then --Deklaracja zatwierdzona
  begin
    insert into EDEDOCSH(ederule,direction,otable,oref,status,path,filename,
                       manualinit,signstatus)
    values (:ederulegen,1,'EDECLARATIONS',:declref,0,'',:filename,0,
            1) returning ref into :edocref;
  end else
  if(status = 4) then --Deklaracja wyslana
  begin
  
    insert into EDEDOCSH(ederule,direction,otable,oref,status,path,
                       manualinit,signstatus)
    values (:ederuleconfirm,1,'EDECLARATIONS',:declref,0,'',0,
            0) returning ref into :edocref;
  end
end^
SET TERM ; ^
