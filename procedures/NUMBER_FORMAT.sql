--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NUMBER_FORMAT(
      NAZWA varchar(40) CHARACTER SET UTF8                           ,
      REJESTR char(3) CHARACTER SET UTF8                           ,
      DOKUMENT char(3) CHARACTER SET UTF8                           ,
      DATA date)
  returns (
      KEY_VAL varchar(40) CHARACTER SET UTF8                           ,
      ODDZIAL varchar(40) CHARACTER SET UTF8                           )
   as
declare variable p_rej smallint;
declare variable p_dok smallint;
declare variable p_okres smallint;
declare variable is_rej smallint;
declare variable is_dok smallint;
declare variable typ_okres smallint;
declare variable old_number integer;
declare variable delim varchar(1);
declare variable okres varchar(8);
declare variable lokres varchar(8);
declare variable suffix varchar(20);
declare variable chrono smallint;
declare variable yer integer;
declare variable lastdata timestamp;
declare variable lastdata_next timestamp;
declare variable p_oddzial smallint;
declare variable oddzialsym varchar(10);
declare variable is_oddzial smallint;
begin
  if(:nazwa is null or (:nazwa = '')) then
     exception GENERATOR_NOT_DEFINED 'Numerator dla '||coalesce(:rejestr,'')||'/'||coalesce(:dokument,'')||' nie jest zdefiniowany!';
  execute procedure GETCONFIG('AKTUODDZIAL') returning_values :oddzial;
  select symbol from ODDZIALY where oddzial=:oddzial into :oddzialsym;
  select rejestr, dokument, oddzial, okres, s_rejestr, s_dokument, s_okres, delimiter, suffix, chrono, s_oddzial from NUMBERGEN where NAZWA = :nazwa
      into :is_rej, :is_dok, :is_oddzial, :typ_okres, :p_rej, :p_dok, :p_okres, :delim, :suffix, :chrono, :p_oddzial;
  if(:is_oddzial=0) then oddzial='XXX'; /* numeracja wspolna dla oddzialow prowadzonych w jednej bazie */
  /* kontrukcja klucza generatora*/
  key_val = '';
  if(:delim=' ') then delim='';
  if(:rejestr is null) then rejestr = '';
  if(:dokument is null) then dokument = '';
  if(:is_rej = 1) then key_val = :key_val || :rejestr;
  if(:is_dok = 1) then key_val = :key_val || :dokument;
  okres = cast( extract( year from :data) as char(4));
  if(:typ_okres = 1) then begin
     /* data pelna*/
     if(extract(month from :data) < 10) then
       okres = :okres ||'0' ||cast(extract(month from :data) as char(1));
     else
       okres = :okres ||cast(extract(month from :data) as char(2));
  end
  if(:okres is null) then okres = '';
  key_val = :key_val || :okres;
end^
SET TERM ; ^
