--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE AUTODECREE_13(
      BKDOC integer)
   as
declare variable account ACCOUNT_ID;
  declare variable ref integer;
  declare variable doctype smallint;
  declare variable amount numeric(14,2);
  declare variable descript varchar(80);
  declare variable period varchar(6);
begin
  -- [KW] schemat dekretowania - przeksiegowania vat przesunietego w czasie

  select period
    from bkdocs
    where ref = :bkdoc
    into :period;

  for
    select B.ref, V.vate, B.symbol, DT.kind
      from bkdocs B
        join bkdoctypes DT on (DT.ref = B.doctype)
        join bkvatpos V on (V.bkdoc = B.ref)
      where B.vatperiod = :period
        and V.debited = 1 and B.status > 1
      into :ref, :amount ,:descript, :doctype
  do begin
    account = null;
    select account
      from decrees
      where bkdoc = :ref
      and ((account starting with '221-1-' and :doctype = 1)
        or (account starting with '222-1-' and :doctype = 2))
      into :account;

    if (account is not null) then
    begin
      execute procedure insert_decree(bkdoc, account, 1, amount, descript, 0);
      account = substring(account from 1 for 3) || '-0-' || substring(account from 7 for 6);
      execute procedure insert_decree(bkdoc, account, 0, amount, descript, 0);
    end
  end
end^
SET TERM ; ^
