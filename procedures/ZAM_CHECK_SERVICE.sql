--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ZAM_CHECK_SERVICE(
      NAGZAMREF integer)
  returns (
      USLUGA smallint)
   as
begin
  usluga = 0;
  if(exists(select first 1 1
    from pozzam pzz left join towary t on (t.ktm = pzz.ktm)
    where t.usluga = 1 and pzz.zamowienie = :nagzamref)
    ) then usluga = 1;
end^
SET TERM ; ^
