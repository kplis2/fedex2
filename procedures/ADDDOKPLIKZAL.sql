--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ADDDOKPLIKZAL(
      SPRAWA integer,
      OPERATOR integer)
  returns (
      KLASA varchar(20) CHARACTER SET UTF8                           ,
      SLODEF integer,
      SEGREGATOR integer,
      TYP integer)
   as
declare variable par integer;
begin
   for select dpt.ref, dpt.segregator, sg.slodef, sg.klasa from dokpliktyp dpt left join segregator sg on (sg.ref=dpt.segregator) where dpt.wymagany=1 into :typ, :segregator, :slodef, :klasa do begin
    par = 0;
    select dp.ref from dokplik dp where dp.ckontrakt = :sprawa and dp.typ = :typ into :par;
      if (par = 0) then insert into DOKPLIK (SYMBOL, TYP, DATA, OPIS, SLOWAKLUCZ, OPERATOR, BLOKADA, SEGREGATOR, PLIK, ROZMIAR, SLODEF, CKONTRAKT, KLASA, STATUS, RODZAJ, AKTYWNY) values
                                            ('', :typ, current_timestamp(0), '', '', :operator, 0, :segregator, '', '', :slodef, :sprawa, :klasa, 0, 0, 0);
   end
end^
SET TERM ; ^
