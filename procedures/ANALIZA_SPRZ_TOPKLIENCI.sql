--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ANALIZA_SPRZ_TOPKLIENCI(
      REJESTR varchar(3) CHARACTER SET UTF8                           ,
      TYP varchar(80) CHARACTER SET UTF8                           ,
      DATAOD timestamp,
      DATADO timestamp,
      ILOSC integer,
      USLUGA varchar(80) CHARACTER SET UTF8                           )
  returns (
      REFNAG integer,
      OPIS SHORTNAME_ID,
      SPRZNET numeric(14,2),
      SPRZBRU numeric(14,2),
      KOSZT numeric(14,2),
      MARZA numeric(14,2),
      MARZAPR numeric(14,2))
   as
begin
--  if (:rejestr is null ) then rejestr = '';
--  if (:typ is null) then typ = '';
--  if (:dataod is null) then dataod = '';
--  if (:datado is null) then datado = '';
--  if (:dataod is not null and :dataod <> '') then NAGFAK.DATA >= :dataod;
--  if (:datado is not null and :datado <> '') then NAGFAK.DATA >= ':DATADO');
--  if (:typ is not null and :typ <> '') then NAGFAK.REJESTR = :REJESTR;
if (ilosc = 0) then ilosc = null;
  for select nagfak.klient as ref, MAX(KLIENCI.FSKROT) as OPIS,
    SUM(POZFAK.wartnetzl- POZFAK.pwartnetzl) as SPRZNET,
    SUM(POZFAK.wartbruzl - POZFAK.pwartbruzl) as SPRZBRU,
    SUM(POZFAK.KOSZTZAK) as KOSZT,
    SUM(POZFAK.wartnetzl - POZFAK.pwartnetzl)-SUM(POZFAK.KOSZTZAK) as MARZA,
    (CASE WHEN SUM(POZFAK.wartnetzl - POZFAK.pwartnetzl) = 0 then 0 else
    100*(SUM(POZFAK.wartnetzl - POZFAK.pwartnetzl) - SUM(POZFAK.KOSZTZAK)) / SUM(POZFAK.wartnetzl- POZFAK.pwartnetzl) END) as MARZAPR
       from POZFAK
       left join NAGFAK on (NAGFAK.REF=POZFAK.DOKUMENT)
       left join KLIENCI on (KLIENCI.REF = NAGFAK.KLIENT)
         where (NAGFAK.ZAKUP = 0) and (strmulticmp(';'||NAGFAK.AKCEPTACJA||';',';1;8;')>0)
         and (NAGFAK.ANULOWANIE = 0) and (NAGFAK.NIEOBROT < 2)
         and (position(NAGFAK.TYP in :TYP) > 0 /*or :TYP = ''*/ )
         --and position(';'||POZFAK.USLUGA||';' in '@USLUGA@') > 0
         and (NAGFAK.REJESTR = :REJESTR or :REJESTR is null or :REJESTR = '')
         and (NAGFAK.DATA >= :DATAOD or :dataod is null)
         and (NAGFAK.DATA <= :DATADO or :datado is null)
         and  (position(';'||POZFAK.USLUGA||';' in :USLUGA) > 0)
         --and  (position(';'||POZFAK.USLUGA||';' in ':USLUGA') > 0)
    group by NAGFAK.KLIENT
    order by SUM(NAGFAK.sumwartnetzl - NAGFAK.psumwartnetzl) desc

  into:refnag,:opis, :sprznet, :sprzbru, :koszt, :marza, :marzapr
  do begin
    if(ilosc is null or ilosc>0) then suspend;
    ilosc = ilosc - 1;
  end
end^
SET TERM ; ^
