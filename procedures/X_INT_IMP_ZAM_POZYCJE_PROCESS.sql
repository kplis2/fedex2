--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_INT_IMP_ZAM_POZYCJE_PROCESS(
      SESJAREF SESJE_ID,
      TABELA STRING35,
      REF INTEGER_ID,
      BLOKUJZALEZNE SMALLINT_ID = 0,
      TYLKONIEPRZETWORZONE SMALLINT_ID = 0,
      AKTUALIZUJSESJE SMALLINT_ID = 1)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable stabela string35;
declare variable tmptabela string35;
declare variable sprocedura string255;
declare variable szrodlo zrodla_id;
declare variable skierunek smallint_id;
declare variable sesja sesje_id;
declare variable sql memo;
declare variable tmpsql memo;
declare variable tmpstatus smallint_id;
declare variable tmpmsg string255;
declare variable error_row smallint_id;
declare variable errormsg_row string255;
declare variable errormsg_all string255;
declare variable tmperror smallint_id;
declare variable statuspoz smallint_id;
declare variable KTM KTM_ID;
declare variable WERSJAREF WERSJE_ID;
declare variable SPOSZAP PLATNOSC_ID;
declare variable SPOSDOST SPOSDOST_ID;
declare variable KLIENT KLIENCI_ID;
declare variable KLIENTHIST KLIENCIHIST_ID;
declare variable DOSTAWCA DOSTAWCA_ID;
declare variable DOKUMENT DOKUMNAG_ID;
declare variable ODDZIAL ODDZIAL_ID;
declare variable COMPANY COMPANIES_ID;
declare variable magazyn defmagaz_id;
declare variable mag2 defmagaz_id;
declare variable nagid string120;
declare variable typzam typzam_id;
declare variable rejzam defrejzam_id;
declare variable typdokmag defdokum_id;
declare variable typdokvat typfak_id;
declare variable typdokmagkor defdokum_id;
declare variable sprzedawca sprzedawca_id;
declare variable operator operator_id;
declare variable termdost timestamp_id;
declare variable dostawa dostawa_id;
declare variable odbiorca odbiorcy_id;
declare variable symbol string40;
declare variable bn char_1;
declare variable waluta waluta_id;
declare variable kurs kurs;
declare variable pozid string120;
declare variable ilosc ilosci_mag;
declare variable sumilosc ilosci_mag;
declare variable refdoc dokumnag_id;
declare variable kordoc dokumnag_id;
declare variable refkordoc dokumnag_id;
declare variable uwagi memo;
declare variable uwagiwewn memo;
declare variable uwagisped memo;
declare variable grupasped dokumnag_id;
declare variable stop smallint_id;
declare variable refkorpoz dokumpoz_id;
declare variable korpozil ilosci_mag;
declare variable korilosc ilosci_mag;
declare variable data timestamp_id;
declare variable cenanet ceny;
declare variable cenabru ceny;
declare variable cenanetzl ceny;
declare variable cenabruzl ceny;
declare variable skadtabela string40;
declare variable skadref integer_id;
begin
  status = 1;
  msg = '';
  errormsg_all = '';

  --zmiana pustych wartosci na null-e
  if (:sesjaref = 0) then
    sesjaref = null;
  if (trim(:tabela) = '') then
    tabela = null;
  if (:ref = 0) then
    ref = null;
  if (:blokujzalezne is null) then
    blokujzalezne = 0;
  if (:tylkonieprzetworzone is null) then
    tylkonieprzetworzone = 0;
  if (:aktualizujsesje is null) then
    aktualizujsesje =1;

  --sprawdzenie czy wszystkie potrzebne informacje sa
  if (:sesjaref is null and :ref is null) then
  begin
    status = -1;
    msg = 'Za mało parametrów. Brak numeru sesji oraz REF';
    exit; --EXIT
  end

  if (:sesjaref is null) then
  begin
    if (:tabela is null) then
    begin
      status = -1;
      msg = 'Za mało parametrów. Brak numeru sesji oraz nazwy tabeli';
      exit; --EXIT
    end

    sql = 'select sesja from '||:tabela||' where ref='||:ref;
    execute statement :sql into :sesja;

  end
  else
    sesja = :sesjaref;

  if (:sesja is null) then
  begin
    status = -1;
    msg = 'Nie znaleziono numeru sesji.';
    exit; --EXIT
  end

  --pobranie parametrow sesji
  select s.tabela, s.procedura, s.zrodlo, s.kierunek
    from int_sesje s
    where ref = :sesja
  into :stabela, :sprocedura, :szrodlo, :skierunek;

  if (:tabela is not null) then --jesli podano tabele na wejsciu traktuje ja nadrzednie
    stabela = :tabela;

  --wlasciwe przetworzenie
  execute procedure set_global_param ('AKTUOPERATOR',74); --#JOTOOD
  for
    select p.nagid, p.pozid, n.symbol, n.datazam, n.bn_sente, n.company_sente,
        --p.magazyn, p.magazyn2,  --TODO do zrobienia, wydania z wielu magazynow
        n.magazyn_sente, n.mag2_sente, --TODO do zrobienia, wydania z wielu magazynow
        n.typ_sente, n.rejestr_sente, n.typdokmag_sente, n.typdokvat_sente, n.sposzap_sente, n.sposdost_sente,
        n.waluta_sente, coalesce(n.kurs_sente,1), n.sprzedawca_sente,
        n.klient_sente, n.klienthist_sente, n.odbiorca_sente, n.dostawca_sente,
        n.termdost_sente, m.korygujacy,
        n.uwagiwew, n.uwagizew, n.uwagisped,
        t.ktm, p.ilosc * coalesce(j.przelicz,1),
        p.cenanetto, p.cenabrutto, p.cenanettozl, p.cenabruttozl,
        --n.dokument,
        p.skadtabela, p.skadref
      from int_imp_zam_pozycje p
        left join int_imp_zam_naglowki n on (n.ref = p.skadref)
        left join towary t on (t.int_id = p.towarid and t.int_zrodlo = :szrodlo)
        left join towjedn j on (j.int_id = p.jednostkaid and j.int_zrodlo = :szrodlo and j.ktm = t.ktm)
        left join defdokum m on (m.symbol = n.typdokmag_sente)
      where (p.sesja = :sesja or :sesja is null)
        and ((:ref is not null and p.ref = :ref and :tabela is null) or
             (:ref is not null and :tabela is not null and p.skadref = :ref and p.skadtabela = :tabela) or
             (:ref is null))
      order by p.ref
    into :nagid, :pozid, :symbol, :data, :bn, :company,
      :magazyn, :mag2,
      :typzam, :rejzam, :typdokmag, :typdokvat, :sposzap, :sposdost,
      :waluta, :kurs, :sprzedawca,
      :klient, :klienthist, :odbiorca, :dostawca,
      :termdost, :typdokmagkor,
      :uwagiwewn, :uwagi, :uwagisped,
      :ktm, :ilosc,
      :cenanet, :cenabru, :cenanetzl, :cenabruzl,
      --:refdoc,
      :skadtabela, :skadref
  do begin
    sumilosc = null;
    error_row = 0;
    errormsg_row = '';

    select sum(p.ilosc - p.ilzreal)
      from pozzam p
        left join nagzam n on (p.zamowienie = n.ref)
      where n.int_id = :nagid
        and p.int_zrodlo = :szrodlo
        and p.int_id = :pozid
        and n.typzam = :typdokmag
        and n.magazyn = :magazyn
        and p.ktm = :ktm
    into :sumilosc;

    if (:sumilosc is null) then sumilosc = 0;
    ilosc = :ilosc - :sumilosc;
    -- sprawdzamy czy trzeba dolozyc pozycje czy skorygować jesli jest > 0 to dokladam a jak mniej do ujmuje
    if (:ilosc > 0) then
    begin
      if (:refdoc is null) then
      begin
        insert into nagzam(magazyn, mag2, id, rejestr, typzam, typdokvat, datawe, dostawa,
            dostawca, klient, odbiorcaid, int_klienthist,
            sposdost, sposzap, termdost,
            uwagi, uwagiwewn, uwagisped,
            walutowe, kurs, waluta,
            int_zrodlo, int_id, int_sesjaostprzetw, int_dataostprzetw, int_symbol)
          values (:magazyn, :mag2, null, :rejzam, :typzam, :typdokvat, :data, :dostawa,
            :dostawca, :klient, :odbiorca, :klienthist,
            :sposdost, :sposzap, :termdost,
            :uwagi, :uwagiwewn, :uwagisped,
            case when :waluta = 'PLN' then 0 else 1 end, :kurs, :waluta,
            :szrodlo, :nagid, :sesja, current_timestamp, :symbol)
          returning ref into :refdoc;

          --if (grupasped is null) then grupasped = refdoc;
      end
      insert into pozzam (zamowienie, ktm, wersja, ilosc, opk,
          int_zrodlo, int_id, int_sesjaostprzetw, int_dataostprzetw)
        values (:refdoc, :ktm, 0, :ilosc, 0,
            :szrodlo, :pozid, :sesja, current_timestamp);
    end
    else if (ilosc < 0) then
    begin
      --TODO
      error_row = 1;
      errormsg_row = substring(:errormsg_row || ' ' || 'Nie obsłużony przypadek' from 1 for 255);
    end
  end

  --XXX JO: aktualizacja
  if (coalesce(trim(:skadtabela), '') <> '' and coalesce(:skadref,0) > 0 and :refdoc is not null) then
  begin
    sql = 'update '|| :tabela ||' set dokument = '|| :refdoc ||
      ', klienthist_sente = '|| coalesce(klienthist, 'null') ||
      ' where ref = '|| :ref;
    execute statement sql;
  end
  --XXX JO: Koniec

  suspend;
end^
SET TERM ; ^
