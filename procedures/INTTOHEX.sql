--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INTTOHEX(
      INPUTNUMBER bigint)
  returns (
      OUTPUTNUMBER varchar(8) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE Q BigInt;
DECLARE VARIABLE R BigInt;
DECLARE VARIABLE T BigInt;
DECLARE VARIABLE H VARCHAR(1);
DECLARE VARIABLE S VARCHAR(6);

begin
  /* Max input value allowed is: 4294967295 */

  S = 'ABCDEF';

  Q = 1;   
  OUTPUTNUMBER = '';
  T = INPUTNUMBER;
  WHILE (Q <> 0) DO
  BEGIN        

    Q = T / 16;
    R = MOD(T, 16);
    T = Q;

    IF (R > 9) THEN
     H = SUBSTRING(S FROM (R-9) FOR 1); 
    ELSE
     H = R;                                

    OUTPUTNUMBER = H || OUTPUTNUMBER ;
  END


  SUSPEND;
end^
SET TERM ; ^
