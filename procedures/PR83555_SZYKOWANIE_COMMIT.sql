--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PR83555_SZYKOWANIE_COMMIT(
      INDEXIN STRING20)
  returns (
      ROW STRING10,
      SEC STRING10,
      LVL STRING10,
      BOXID STRING20,
      INDEXOUT STRING20)
   as
declare variable los string20;
begin
  los = CAST(CAST(FLOOR(RAND()*100) as integer) AS varchar(9));
  while(char_length(los)<2) do los = '0' || los;
  row = ASCII_CHAR(ASCII_VAL('A')+FLOOR(RAND()*26)) || los;

  los = CAST(CAST(FLOOR(RAND()*100) as integer) AS varchar(9));
  while(char_length(los)<2) do los = '0' || los;
  sec = los;

  los = CAST(CAST(FLOOR(RAND()*10) as integer) AS varchar(9));
  lvl = los;

  los = CAST(CAST(FLOOR(RAND()*1000000) as integer) AS varchar(9));
  while(char_length(los)<6) do los = '0' || los;
  boxid = los;

  los = '';
  while(char_length(los)<15)
    do los = los || CAST(CAST(FLOOR(RAND()*10) as integer) AS varchar(2));

  indexout = los;
  suspend;
end^
SET TERM ; ^
