--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE INTRASTAT_DANE(
      DATAOD date,
      DATADO date,
      IOKRES varchar(255) CHARACTER SET UTF8                           ,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      DOKWYDANIA integer,
      DOSTAWCA integer,
      KLIENT integer,
      IKTM varchar(40) CHARACTER SET UTF8                           ,
      MKTM varchar(40) CHARACTER SET UTF8                           ,
      POZAUE integer,
      PROGSTAT integer,
      COMPANY integer)
  returns (
      KTM varchar(40) CHARACTER SET UTF8                           ,
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      SYMBDOK varchar(20) CHARACTER SET UTF8                           ,
      KRAJWYSYLKI varchar(20) CHARACTER SET UTF8                           ,
      ILOSC numeric(14,4),
      WARTOSC numeric(14,2),
      WARDOSTAWY varchar(255) CHARACTER SET UTF8                           ,
      RODZTRANSPORT varchar(255) CHARACTER SET UTF8                           ,
      RODZTRANSAKCJA varchar(255) CHARACTER SET UTF8                           ,
      KRAJPOCHODZENIA varchar(20) CHARACTER SET UTF8                           ,
      KODTOWARU varchar(20) CHARACTER SET UTF8                           ,
      JEDNPOMOC varchar(5) CHARACTER SET UTF8                           ,
      ILOSCJEDNPOMOC integer,
      MASA numeric(14,4),
      OTABLE varchar(40) CHARACTER SET UTF8                           ,
      OREF integer,
      SREF integer,
      DATA date,
      SLODEF integer,
      SLOPOZ integer,
      WARTOSCSTAT numeric(14,2),
      WYDANIA integer,
      OKRES varchar(6) CHARACTER SET UTF8                           ,
      NOTA smallint,
      TNAZWA varchar(80) CHARACTER SET UTF8                           ,
      REFDOKUMNAG integer)
   as
declare variable PRZELICZNIK numeric(14,4);
declare variable UE integer;
declare variable SQL varchar(4096);
declare variable OK integer;
declare variable KOREKTA smallint;
declare variable WARIANT smallint; /* Wariant naliczania deklaracji: 0 - z DOKUMPOZ, 1 - z POZFAK */
declare variable SLODOSTAW integer; /* Ref słownika dostaw dla Intrastatu */
declare variable SLOTRANSPORT integer; /* Ref słownika transportu dla Intrastatu */
declare variable SLOGRUPY integer; /* Ref słownika grup towarowych dla Intrastatu */
declare variable WAGAJEDNOPOM numeric(15,4); /* Waga jednostki pomocniczej */
declare variable ZAKUP smallint; /* Flaga zakupu NAGFAK */
declare variable NKLIENT integer;
declare variable NDOSTAWCA integer;
begin

  execute procedure get_config('INTRASTAT_DOKUMENTY', 2)
    returning_values wariant;
  execute procedure get_config('INTRASTAT_DOSTAWY', 2)
    returning_values slodostaw;
  execute procedure get_config('INTRASTAT_TRANSPORT', 2)
    returning_values slotransport;
  execute procedure get_config('INTRASTAT_GRUPY', 2)
    returning_values slogrupy;

  sql = '';
  if (:iokres='') then iokres = null;
  if (:magazyn='') then magazyn = null;
  if (:dokwydania = 2) then dokwydania = null;
  if (:dostawca=0) then dostawca = null;
  if (:klient=0) then klient = null;
  if (:iktm='') then iktm = null;
  if (:mktm='') then mktm = null;
  if (:pozaue is null) then pozaue = 0;
  if (:progstat is null) then progstat = 0;
  refdokumnag = null;

  if(wariant=0) then begin      /* Naliczanie deklaracji na podstawie DOKUMNAG i DOKUMPOZ */
    otable = 'DOKUMNAG';
    sql = ' select (DOKUMNAG.ref), (DOKUMPOZ.ktm) as KTM, (IGRUPY.NAZWA) as NAZWA, (DOKUMNAG.symbol) as SYMBDOK, (DOKUMNAG.data) as data, ';
    sql = sql || ' (coalesce(dokumnag.krajwysylki,(case when defdokum.wydania = 1 then (KLIENCI.krajid ) else (dostawcy.krajid ) end))) as KRAJWYSYLKI,';
--    sql = sql || ' (case when defdokum.wydania = 1 then (KLIENCI.krajid ) else (dostawcy.krajid ) end) as KRAJWYSYLKI,';
    sql = sql || ' (dokumpoz.ilosco) as ILOSC, (dokumpoz.pwartosc   ) as WARTOSC,  (WARDOSTAWY.nazwa) as WARDOSTAWY,';
    sql = sql || ' (RODZTRANSPORT.nazwa) as RODZTRANSPORT,';
    sql = sql || ' case when DEFDOKUM.WYDANIA = 1 then '''' else (case when DOKUMPOZ.krajpochodzenia is not null and DOKUMPOZ.krajpochodzenia<>'''' then (dokumpoz.krajpochodzenia ) else (towary.krajpochodzenia )end) end as KRAJPOCHODZENIA, ';
    sql = sql || ' substring(TOWARY.ktc from 1 for 8) as KODTOWARU, JEDNPOMOC.jedn as JEDNPOMOC, (JEDNPOMOC.przelicz) as PRZELICZNIK, ';
    sql = sql || ' (DOKUMPOZ.waga) as WAGA, (DOKUMNAG.ref) as REFDOKUMNAG, (DOKUMPOZ.ref) as REF, (DOKUMNAG.slodef) as SLODEF, ';
    sql = sql || ' (DOKUMNAG.slopoz) as SLOPOZ, (DOKUMNAG.okres) as okres,  (DEFDOKUM.wydania) as WYDANIA, (DEFDOKUM.KORYG) as KOREKTA, 0 as NOTA, TOWARY.nazwa as TNAZWA, ';
    sql = sql || ' (DOKUMPOZ.intrtransakcja) as RODZTRANSAKCJA , (JEDNPOMOC.waga) as WAGAJEDNOPOMOC';
    sql = sql || ' from DOKUMPOZ  join DOKUMNAG on (DOKUMNAG.REF = DOKUMPOZ.DOKUMENT) ';
    sql = sql || ' join DEFMAGAZ on (DEFMAGAZ.SYMBOL = DOKUMNAG.MAGAZYN) ';
    sql = sql || ' join DEFDOKUM on (DEFDOKUM.SYMBOL = DOKUMNAG.TYP) ';
    sql = sql || ' join TOWARY on (DOKUMPOZ.KTM = TOWARY.KTM) ';
    sql = sql || ' left join KLIENCI on (DOKUMNAG.KLIENT = KLIENCI.REF) ';
    sql = sql || ' left join DOSTAWCY on (DOKUMNAG.DOSTAWCA = DOSTAWCY.REF) ';
    sql = sql || ' left join SPOSDOST on (DOKUMNAG.SPOSDOST = SPOSDOST.ref) ';
    sql = sql || ' left join SLOPOZ WARDOSTAWY on (SPOSDOST.intrdostawy = WARDOSTAWY.kod and WARDOSTAWY.slownik=' || :slodostaw ||') ';
    sql = sql || ' left join SLOPOZ RODZTRANSPORT on (SPOSDOST.intrtransport = RODZTRANSPORT.kod and RODZTRANSPORT.slownik=' || :slotransport ||') ';
    sql = sql || ' left join SLOPOZ IGRUPY on (TOWARY.ktc = IGRUPY.kod and IGRUPY.SLOWNIK='|| :slogrupy ||') ';
    sql = sql || ' left join TOWJEDN JEDNPOMOC on (JEDNPOMOC.KTM = DOKUMPOZ.ktm and JEDNPOMOC.intrastjedn = 1) ';
    sql = sql || ' where 1 = 1 ';
    if(:iokres is not null) then
      sql = sql || ' and (DOKUMNAG.okres =  '''|| :iokres ||''')';
    if(:magazyn is not null) then
      sql = sql || ' and (DOKUMNAG.magazyn = '''|| :magazyn ||''')';
      sql = sql || 'and DEFDOKUM.intrastat = 1 and defmagaz.pozabilansowy = 0 and defdokum.zewn = 1 and DOKUMNAG.akcept in (1,8) ';
    if(:dataod is not null) then
      sql = sql || ' and (DOKUMNAG.data >= ''' || :dataod ||''')';
    if(:datado is not null) then
      sql = sql || ' and (DOKUMNAG.data <= '''|| :datado ||''')';
    if(:dostawca is not null)then
      sql = sql || ' and (DOKUMNAG.dostawca = '|| :dostawca ||')';
    if(:klient is not null)then
      sql = sql || ' AND (DOKUMNAG.klient = '||:klient||')';
    if(:iktm is not null) then
      sql = sql || ' and (DOKUMPOZ.ktm = '''|| :iktm ||''')';
    if(:mktm is not null) then
      sql = sql || ' and (DOKUMPOZ.ktm like ''%'||:mktm||'%'')';
    if(:company is not null) then
      sql = sql || ' and (DOKUMNAG.company = ' || :company || ')';
    sql = sql || ' and (coalesce(DOKUMPOZ.foc,0) <> 1) ';
    for execute statement sql
      into :refdokumnag, :ktm, :nazwa, :symbdok, :data, :krajwysylki, :ilosc, :wartosc,
        :wardostawy, :rodztransport, :krajpochodzenia, :kodtowaru, :jednpomoc, :przelicznik,
        :masa, :oref, :sref, :slodef,  :slopoz, :okres, :wydania, :korekta, :nota, :tnazwa,
        :rodztransakcja, :wagajednopom
    do begin
      if (rodztransakcja='') then rodztransakcja = null;
      if(coalesce(:masa, 0.0000)=0.0000) then
        masa = cast((:ilosc * :przelicznik * :wagajednopom) as numeric(14,4));
      ue = null;
      ok = 0;
      select ue from countries where symbol=:krajwysylki and symbol <> 'PL' into :ue;
      if(:dokwydania is null) then ok = 1;
      else if(:dokwydania = 1) then begin
        if((:wydania=1 and :korekta=0) or (:wydania=1 and :korekta=1 and :ilosc=0 and :wartosc<>0) or (:wydania=0 and :korekta=1 and :ilosc<>0)) then ok = 1;
      end else if(:dokwydania = 0) then begin
        if((:wydania=0 and :korekta=0) or (:wydania=0 and :korekta=1 and :ilosc=0 and :wartosc<>0) or (:wydania=1 and :korekta=1 and :ilosc<>0)) then ok = 1;
      end
      if(:ue = 1 and :ok = 1) then begin
          if(:korekta=1 and :ilosc<>0) then begin
            if(rodztransakcja is null) then     /* rodztranskacja is not null tylko w wyniku recznej korekty */
              rodztransakcja = '21';
            ilosc = -:ilosc;
            wartosc = -:wartosc;
            masa = -:masa;
          end else begin
            if(rodztransakcja is null) then     /* rodztranskacja is not null tylko w wyniku recznej korekty */
              rodztransakcja = '11';
          end
          if (przelicznik is null) then przelicznik = 1;
          if(:jednpomoc is not null) then begin
            iloscjednpomoc = round(:ilosc * :przelicznik);
            if (iloscjednpomoc = 0) then iloscjednpomoc = null;
          end else begin
            iloscjednpomoc = null;
          end
          if(:progstat=1) then begin
            wartoscstat = :wartosc;
          end else begin
            wartoscstat = null;
            wardostawy = null;
            rodztransport = null;
          end
          suspend;
      end
      refdokumnag = null;
      rodztransakcja = null;
      masa = 0.0;
    end
    otable = 'DOKUMNOT';
    sql = '';
    sql = sql || ' select (DOKUMNAG.ref), (DOKUMPOZ.ktm) as KTM, (IGRUPY.NAZWA) as NAZWA, (DOKUMNOT.symbol) as SYMBDOK, (DOKUMNAG.data) as data, ';
    sql = sql || ' (coalesce(dokumnag.krajwysylki,(case when defdokum.wydania = 1 then (KLIENCI.krajid ) else (dostawcy.krajid ) end))) as KRAJWYSYLKI, ';
--    sql = sql || ' (case when defdokum.wydania = 1 then ( KLIENCI.krajid ) else ( dostawcy.krajid ) end) as KRAJWYSYLKI, ';
    sql = sql || ' 0 as ILOSC, (DOKUMNOTP.wartosc) as WARTOSC,  (WARDOSTAWY.nazwa) as WARDOSTAWY, (RODZTRANSPORT.nazwa) as RODZTRANSPORT, ';
    sql = sql || ' (case when DOKUMPOZ.krajpochodzenia is not null and DOKUMPOZ.krajpochodzenia<>'''' then (dokumpoz.krajpochodzenia ) else (towary.krajpochodzenia )end)  as KRAJPOCHODZENIA, ';
    sql = sql || ' substring(TOWARY.ktc from 1 for 8) as KODTOWARU, (JEDNPOMOC.jedn) as JEDNPOMOC, (JEDNPOMOC.przelicz) as PRZELICZNIK, ';
    sql = sql || ' 0 as WAGA, DOKUMNOT.ref as REFDOKUMNAG, (DOKUMPOZ.ref) as REF, (DOKUMNAG.slodef) as SLODEF, ';
    sql = sql || ' (DOKUMNAG.slopoz) as SLOPOZ, (DOKUMNAG.okres) as okres,  (DEFDOKUM.wydania) as WYDANIA, (DEFDOKUM.KORYG) as KOREKTA, 1 as NOTA, TOWARY.NAZWA as TNAZWA,  ';
    sql = sql || ' (DOKUMPOZ.intrtransakcja) as RODZTRANSAKCJA ';
    sql = sql || ' from DOKUMNOTP  join dokumnot on (dokumnot.ref = dokumnotp.dokument) ';
    sql = sql || ' left join nagfak on (nagfak.ref = dokumnot.faktura) ';
    sql = sql || ' left join typfak on (typfak.symbol = nagfak.typ) ';
    sql = sql || ' join DOKUMROZ on (DOKUMROZ.ref = DOKUMNOTP.dokumrozkor) ';
    sql = sql || ' join DOKUMPOZ on (DOKUMPOZ.REF = DOKUMROZ.pozycja) ';
    sql = sql || ' join DOKUMNAG on (DOKUMNAG.REF = DOKUMPOZ.DOKUMENT)  ';
    sql = sql || ' join DEFMAGAZ on (DEFMAGAZ.SYMBOL = DOKUMNAG.MAGAZYN)  ';
    sql = sql || ' join DEFDOKUM on (DEFDOKUM.SYMBOL = DOKUMNAG.TYP) ';
    sql = sql || ' left join KLIENCI on (DOKUMNAG.KLIENT = KLIENCI.REF) ';
    sql = sql || ' left join DOSTAWCY on (DOKUMNAG.DOSTAWCA = DOSTAWCY.REF) ';
    sql = sql || ' left join SPOSDOST on (DOKUMNAG.SPOSDOST = SPOSDOST.ref)  ';
    sql = sql || ' left join TOWARY on (DOKUMPOZ.KTM = TOWARY.KTM) ';
    sql = sql || ' left join SLOPOZ WARDOSTAWY on (SPOSDOST.intrdostawy = WARDOSTAWY.kod and WARDOSTAWY.slownik=' || :slodostaw ||') ';
    sql = sql || ' left join SLOPOZ RODZTRANSPORT on (SPOSDOST.intrtransport = RODZTRANSPORT.kod and RODZTRANSPORT.slownik=' || :slotransport ||') ';
    sql = sql || ' left join SLOPOZ IGRUPY on (TOWARY.ktc = IGRUPY.kod and IGRUPY.SLOWNIK='|| :slogrupy ||') ';
    sql = sql || ' left join TOWJEDN JEDNPOMOC on (JEDNPOMOC.KTM = DOKUMPOZ.ktm and JEDNPOMOC.intrastjedn = 1) ';
    sql = sql || ' where 1 = 1 ';
    if(:iokres is not null) then
      sql = sql || ' and (DOKUMNAG.okres =  '''|| :iokres ||''')';
    sql = sql || ' and DEFDOKUM.intrastat = 1 and defmagaz.pozabilansowy = 0 and defdokum.zewn = 1   ';
    sql = sql || ' and DOKUMNOT.akcept in (1,8) and (dokumnot.faktura is null or typfak.intrastat = 1) ';
    if(:dataod is not null) then
      sql = sql || ' and (DOKUMNAG.data >= ''' || :dataod ||''')';
    if(:datado is not null) then
      sql = sql || ' and (DOKUMNAG.data <= '''|| :datado ||''')';
    if(:magazyn is not null) then
      sql = sql || ' and (DOKUMNAG.magazyn = '''|| :magazyn ||''')';
    if(:dostawca is not null)then
      sql = sql || ' and (DOKUMNAG.dostawca = '|| :dostawca ||')';
    if(:klient is not null)then
      sql = sql || ' AND (DOKUMNAG.klient = '||:klient||')';
    if(:iktm is not null) then
      sql = sql || ' and (DOKUMPOZ.ktm = '''|| :iktm ||''')';
    if(:mktm is not null) then
      sql = sql || ' and (DOKUMPOZ.ktm like ''%'||:mktm||'%'')';
    if(:company is not null) then
      sql = sql || ' and (DOKUMNAG.company = ' || :company || ')';
    sql = sql || ' and (coalesce(DOKUMPOZ.foc,0) <> 1) ';
    for execute statement sql
      into :refdokumnag, :ktm, :nazwa,:symbdok, :data, :krajwysylki, :ilosc, :wartosc, :wardostawy,
        :rodztransport, :krajpochodzenia, :kodtowaru, :jednpomoc,
        :przelicznik, :masa, :oref, :sref, :slodef,  :slopoz, :okres, :wydania, :korekta, :nota, :tnazwa, :rodztransakcja
    do begin
      if (rodztransakcja='') then rodztransakcja = null;
      ue = null;
      ok = 0;
      select ue from countries where symbol=:krajwysylki and symbol <> 'PL' into :ue;
      if(:dokwydania is null) then ok = 1;
      else if(:dokwydania = 1) then begin
        if((:wydania=1 and :korekta=0) or (:wydania=1 and :korekta=1 and :ilosc=0 and :wartosc<>0) or (:wydania=0 and :korekta=1 and :ilosc<>0)) then ok = 1;
      end else if(:dokwydania = 0) then begin
        if((:wydania=0 and :korekta=0) or (:wydania=0 and :korekta=1 and :ilosc=0 and :wartosc<>0) or (:wydania=1 and :korekta=1 and :ilosc<>0)) then ok = 1;
      end
      if(:ue = 1 and :ok = 1) then begin
        if(:korekta=1 and :ilosc<>0) then begin
          if(rodztransakcja is null) then     /* rodztranskacja is not null tylko w wyniku recznej korekty */
            rodztransakcja = '21';
          ilosc = -:ilosc;
          wartosc = -:wartosc;
          masa = -:masa;
        end else begin
          if(rodztransakcja is null) then     /* rodztranskacja is not null tylko w wyniku recznej korekty */
            rodztransakcja = '11';
        end
        if (przelicznik is null) then przelicznik = 1;
        if(:jednpomoc is not null) then begin
          iloscjednpomoc = round(:ilosc * :przelicznik);
          if (iloscjednpomoc = 0) then iloscjednpomoc = null;
        end else begin
          iloscjednpomoc = null;
        end
        if(:progstat=1) then begin
          wartoscstat = :wartosc;
        end else begin
          wartoscstat = null;
          wardostawy = null;
          rodztransport = null;
        end
        suspend;
      end
      refdokumnag = null;
      rodztransakcja = null;
    end
  end else begin                /* Naliczanie deklaracji na podstawie NAGFAK i POZFAK */
    otable = 'NAGFAK';
    for select
      (p.ktm) as ktm, igrupy.nazwa as nazwa,
      (n.symbol) as symbdok, (n.data) as data,
      (coalesce(n.krajwysylki,(case when n.zakup = 0 then coalesce(n.krajwysylki,k.krajid)
                             else coalesce(n.krajwysylki,d.krajid)
       end))) as krajwysylki,
      (p.iloscm - p.piloscm) as ilosc,
      (p.wartnetzl -p.pwartnetzl) as wartosc,
      (wardostawy.nazwa) as wardostawy,
      (rodztransport.nazwa) as rodztransport,
      (coalesce(p.krajpochodzenia, t.krajpochodzenia)) as krajpochodzenia,
      (t.ktc) as kodtowaru,
      (jednpomoc.jedn) as jednpomoc,
      (jednpomoc.przelicz) as przelicznik,
      (p.waga) as masa,
      (n.ref) as refnagfak,
      (p.ref) as ref,
      (n.slodef) as slodef,
      (n.slopoz) as slopoz,
      (n.okres) as okres,
      (1-n.zakup) as wydania,
      typfak.korekta as korekta,
      0 as nota,
      t.nazwa as tnazwa,
      p.intrtransakcja as rodztransakcja,
      n.zakup as zakup,
      n.klient as nklient, 
      n.dostawca as ndostawca
    from pozfak p
      join nagfak n on (n.ref = p.dokument)
      left join typfak on (typfak.symbol = n.typ)
      left join klienci k on (n.klient = k.ref)
      left join dostawcy d on (n.dostawca = d.ref)
      left join sposdost s on (n.sposdost = s.ref)
      left join towary t on (p.ktm = t.ktm)
      left join slopoz wardostawy on (s.intrdostawy = wardostawy.kod and wardostawy.slownik= :slodostaw)
      left join slopoz rodztransport on (s.intrtransport = rodztransport.kod and rodztransport.slownik= :slotransport)
      left join slopoz igrupy on (t.ktc = igrupy.kod and igrupy.slownik= :slogrupy)
      left join towjedn jednpomoc on (jednpomoc.ktm = p.ktm and jednpomoc.intrastjedn = 1)
    where
      n.akceptacja in (1,8) and typfak.intrastat = 1 and n.nieobrot < 2 --and p.usluga<>1
      and ((:dataod is not null and n.data >= :dataod) or :dataod is null)
      and ((:datado is not null and n.data <= :datado) or :datado is null)
      and ((:iokres is not null and n.okres = :iokres) or :iokres is null)
      and ((:magazyn is not null and p.magazyn = :magazyn) or :magazyn is null)
      and ((:dostawca is not null and n.dostawca = :dostawca) or :dostawca is null)
      and ((:klient is not null and n.klient = :klient) or :klient is null)
      and ((:iktm is not null and p.ktm = :iktm) or :iktm is null)
      and ((:mktm is not null and p.ktm like '%'||:mktm||'%') or :mktm is null)
      and (coalesce(p.foc,0) <> 1)
    into :ktm, :nazwa, :symbdok, :data, :krajwysylki, :ilosc, :wartosc, :wardostawy,
         :rodztransport, :krajpochodzenia, :kodtowaru, :jednpomoc,
         :przelicznik, :masa, :oref, :sref, :slodef,  :slopoz, :okres, :wydania, :korekta, :nota, :tnazwa, :rodztransakcja,
         :zakup, :nklient, :ndostawca
    do begin
      if (rodztransakcja='') then rodztransakcja = null;
      if(coalesce(:masa, 0.0000)=0.0000) then
        masa = cast((:ilosc * :przelicznik * :wagajednopom) as numeric(14,4));
      if (:slodef is null or :slopoz is null) then begin
        if(:zakup=1) then begin
          slodef = 6;
          slopoz = :ndostawca;
        end else begin
          slodef = 1;
          slopoz = :nklient;
        end
      end
      ue = null;
      ok = 0;
      select ue from countries where symbol=:krajwysylki and symbol <> 'PL' into :ue;
      if(:dokwydania is null) then ok = 1;
      else if(:dokwydania = 1) then begin
        if((:wydania=1 and :korekta=0)
            or (:wydania=1 and :korekta=1 and :ilosc=0)
            or (:wydania=0 and :korekta=1 and :ilosc<>0))
          then ok = 1;
      end else if(:dokwydania = 0) then begin
        if((:wydania=0 and :korekta=0)
            or (:wydania=0 and :korekta=1 and :ilosc=0)
            or (:wydania=1 and :korekta=1 and :ilosc<>0))
          then ok = 1;
      end
      if(:ue = 1 and :ok = 1) then begin
        if(:korekta=1 and :ilosc<>0) then begin
          if(rodztransakcja is null) then     /* rodztranskacja is not null tylko w wyniku recznej korekty */
            rodztransakcja = '21';
          ilosc = -:ilosc;
          wartosc = -:wartosc;
          masa = -:masa;
        end else begin
          if(rodztransakcja is null) then     /* rodztranskacja is not null tylko w wyniku recznej korekty */
            rodztransakcja = '11';
        end
        if (przelicznik is null) then przelicznik = 1;
        if(:jednpomoc is not null) then begin
          iloscjednpomoc = round(:ilosc * :przelicznik);
          if (iloscjednpomoc = 0) then iloscjednpomoc = null;
        end else begin
          iloscjednpomoc = null;
        end
        if(:progstat=1) then begin
          wartoscstat = :wartosc;
        end else begin
          wartoscstat = null;
          wardostawy = null;
          rodztransport = null;
        end
        suspend;
      end
      slodef = null;
      slopoz = null;
      zakup = null;
      nklient = null;
      ndostawca = null;
      rodztransakcja = null;
      masa = 0.0;
    end
  end
end^
SET TERM ; ^
