--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE STATEMENT_MULTIPARSE_LINE(
      SOURCE varchar(1024) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      DECL varchar(1024) CHARACTER SET UTF8                           ,
      PROC varchar(1024) CHARACTER SET UTF8                           ,
      BODY varchar(1024) CHARACTER SET UTF8                           ,
      LASTVAR integer,
      DELIMITER varchar(1) CHARACTER SET UTF8                           ,
      DECLTYPE integer)
  returns (
      RDECL varchar(1024) CHARACTER SET UTF8                           ,
      RPROC varchar(1024) CHARACTER SET UTF8                           ,
      RBODY varchar(1024) CHARACTER SET UTF8                           ,
      RLASTVAR integer,
      RLASTNDX integer)
   as
declare variable LOCALPROC varchar(8191);
declare variable LOCALBODY varchar(8191);
declare variable PROCNAME varchar(8191);
declare variable DECLNAME varchar(8191);
declare variable VARNAME varchar(8191);
declare variable NDX integer;
declare variable SUBNDX integer;
declare variable L integer;
declare variable C varchar(1);
declare variable EOL varchar(2);
declare variable INSTRING integer;
declare variable INBRACKETS integer;
declare variable POS integer;
declare variable SOURCE_PART varchar(8191);
begin
--decltype=1 - numeric(14,4), decltype=2 - varchar(255)
  eol = '
';
  instring = 0;
  inbrackets = 0;
  if(:prefix is null) then prefix = '';
  localproc = '';
  localbody = '';
  ndx = 1;
  l = coalesce(char_length(source),0); -- [DG] XXX ZG119346
  /* tresc wyrazenia */
  while(ndx<=l) do begin
    c = substring(:source from :ndx for 1);       --substring(:source from :ndx for :ndx)
    if(:c='#' and :instring=0) then begin
      ndx = :ndx+1;
      c = substring(:source from :ndx for 1);   ----substring(:source from :ndx for :ndx)
      if(:c='#') then begin
        localbody = :localbody || :c;
      end else begin
        procname = :prefix;
        varname = 'v' || cast(:lastvar as varchar(10));
        lastvar = :lastvar+1;
        while((c>='A' and c<='Z') or c='_' or (c>='0' and c<='9')) do begin
          procname = :procname || :c;
          ndx = :ndx+1;
          c = substring(:source from :ndx for 1);       --substring(:source from :ndx for :ndx)
        end
        if(exists(select rdb$procedure_name from rdb$procedures where rdb$procedure_name='X_'||:procname))
          then procname = 'X_'||:procname;
        procname = 'execute procedure ' || :procname || '(key1, key2,''' || :prefix || '''';
        if(:c='(') then begin
          procname = :procname || ', ';
          ndx = :ndx+1;
          source_part = substring(:source from :ndx for (1+:l-:ndx));
          execute procedure STATEMENT_MULTIPARSE_LINE(source_part,:prefix,:decl,:localproc,:procname,:lastvar,')', :decltype) --substring(:source from :ndx for :l)
            returning_values :decl,:localproc,:procname,:lastvar,:subndx;
          ndx = :ndx+:subndx-1;

        end else begin
          procname = :procname || ')';
          ndx = :ndx-1;
        end
        procname = :procname || :eol ||'  returning_values :' || :varname || '; ';
        if (decltype = 1) then
          declname = 'declare variable ' || :varname || ' numeric(14,4); ';
        if (decltype = 2) then
          declname = 'declare variable ' || :varname || ' varchar(255); ';
        decl = :decl || :declname || :eol;
        localproc = :localproc || :procname || :eol;
        localbody = :localbody || :varname;
      end
    end else begin
      localbody = :localbody || :c;
      if(:c='''' and :instring=0) then instring = 1;
      else if(:c='''' and :instring=1) then instring = 0;
      else if(:c='(' and :instring=0) then inbrackets = :inbrackets+1;
      else if(:c=')' and :instring=0 and :inbrackets>0) then inbrackets = :inbrackets-1;
      else if((:c=';' --BS49392 
               or (:ndx>4 and substring(:source from :ndx-4 for 5)='begin')
               or (:ndx>2 and substring(:source from :ndx-2 for 3)='end')
              ) and :instring=0
      ) then begin
        localproc = :localproc || :localbody || :eol;
        localbody = '';
      end
      else if(:delimiter<>'' and :c=:delimiter and :instring=0 and :inbrackets=0) then break;
    end
    ndx = :ndx+1;
  end
  pos = position('end' in :localbody);
  l = coalesce(char_length(:localbody),0); -- [DG] XXX ZG119346
  if(pos>0 and l-pos = 2) then delimiter = 'X';
  if(:delimiter='' and position(';' in :localbody) = 0 and :localbody<>'') then localbody = 'ret = '||iif(coalesce(:localbody,'') = '','0','')||:localbody||';';
  rdecl = :decl;
  rproc = :proc||:localproc;
  rbody = :body||:localbody;
  rlastvar = :lastvar;
  rlastndx = :ndx;
end^
SET TERM ; ^
