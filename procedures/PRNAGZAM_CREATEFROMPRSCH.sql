--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRNAGZAM_CREATEFROMPRSCH(
      REJESTR varchar(3) CHARACTER SET UTF8                           ,
      TYP varchar(3) CHARACTER SET UTF8                           ,
      MAGAZYN varchar(3) CHARACTER SET UTF8                           ,
      KTM varchar(80) CHARACTER SET UTF8                           ,
      ILOSC numeric(14,2),
      PRSCHEDULE integer,
      PLANSCOL integer,
      DATA timestamp)
  returns (
      ZAM integer,
      ILWYGZAM integer)
   as
declare variable sheet integer;
declare variable ktmpoz varchar(80);
declare variable pozzam integer;
declare variable prshmat integer;
declare variable techjednperdays varchar(255);
declare variable techjednperday numeric(14,2);
declare variable batchquantity numeric(14,4);
declare variable techjednquantity numeric(14,4);
declare variable ilosczam numeric(14,2);
declare variable datareal timestamp;
declare variable datalast timestamp;
declare variable ilosclast numeric(14,2);
declare variable ltyp varchar(20);
declare variable lmagazyn varchar(10);
declare variable prdepart varchar(20);
declare variable msg varchar(40);
declare variable prsheet integer;
begin
  ilwygzam = 0;
  if(:magazyn is null) then magazyn = '';
  if(:typ is null) then typ = '';
  ltyp = :typ;
  lmagazyn = :magazyn;
  /*nagowek zamówienia*/
  execute procedure get_config('TECHJEDNQUANTITY',1) returning_values :techjednperdays;
  techjednperday = cast(:techjednperdays as numeric(14,2));
  select max(P.ref) from PRSHEETS P where P.ktm = :ktm and P.STATUS = 2 into :sheet;
  select p.batchquantity, p.techjednquantity, p.prdepart from prsheets p where p.ref = :sheet into :batchquantity, :techjednquantity, :prdepart;
  select pc.bdata from planscol pc where pc.ref = :planscol into :data;
  datareal = :data;
-- rozbicie na zamówienia wg dat
  while(:ilosc>0) do begin
-- wyszukanie ostatniego zamówienia do harmonogramu i kolumny planu
    if(ilwygzam = 0) then begin
      select max(n.termdost) from nagzam n where n.prschedule = :prschedule and n.planscol = :planscol into :datalast;
      select sum(n.kilosc) from nagzam n where n.prschedule = :prschedule and n.planscol = :planscol and substring(n.termdost from 1 for 10) = substring(:datalast from 1 for 10) into :ilosclast;
      if(:datalast is null) then datalast = :data;
      if(:ilosclast is null) then ilosclast = 0;
      if(:ilosclast >= :batchquantity*:techjednperday/:techjednquantity) then begin
        datareal = :datalast + 1;
        if(:ilosc > :batchquantity*:techjednperday/:techjednquantity) then ilosczam = :batchquantity*:techjednperday/:techjednquantity;
        else ilosczam = :ilosc;
      end else begin
        datareal = :datalast;
        if(:ilosc > (:batchquantity*:techjednperday/:techjednquantity - :ilosclast)) then ilosczam = :batchquantity*:techjednperday/:techjednquantity - :ilosclast;
        else ilosczam = :ilosc;
      end
    end else begin
      if(:ilosc > :batchquantity*:techjednperday/:techjednquantity) then ilosczam = :batchquantity*:techjednperday/:techjednquantity;
      else ilosczam = :ilosc;
    end
    if(floor(:ilosczam)<:ilosczam) then ilosczam = floor(:ilosczam) + 1;
    zam = 0;
    execute procedure GEN_REF('NAGZAM') returning_values :zam;
    /*okreslenie typu zleceniea, i magazynow*/
    prsheet = null;
    select max(PRSHEETs.ref) from prsheets
    where prsheets.ktm = :ktm and prsheets.wersja = 0 and prsheets.status = 2
      and (prsheets.prdepart = :prdepart or (prdepart is null and prsheets.prdepart is null))
    into :prsheet;
    if(:prsheet is null) then begin
      msg = 'Brak domylnej karty dla towaru '||:ktm;
      if(:prdepart <> '') then
        msg = :msg||' dla wydzialu '||:prdepart;
      exception universal :msg;
    end
    if(:typ = '') then begin
      ltyp = '';
      select prshtypes.typzam from prshtypes join prsheets on (prsheets.shtype = prshtypes.symbol)
        where prsheets.ref = :prsheet
      into :ltyp;
    end
    if(:magazyn='') then begin
      lmagazyn = '';
      if(:prdepart <> '') then
        select prdeparts.warehousein from prdeparts join prsheets on (prsheets.prdepart = prdeparts.symbol)
        where prsheets.ref = :prsheet
        into :lmagazyn;
      if(:lmagazyn is null or (:lmagazyn = '') ) then
        select typzam.magazyn from typzam where typzam.symbol = :ltyp into :magazyn;
    end
    if(:lmagazyn is null) then lmagazyn = '';
    if(:ltyp is null) then ltyp = '';
    if(:ltyp = '') then exception UNIVERSAL 'Brak typu zlec. dla tow. '||:ktm;
    if(:lmagazyn = '') then exception UNIVERSAL 'Brak magazynu dla tow. '||:ktm;
    insert into NAGZAM(REF, REJESTR, TYPZAM, MAGAZYN, DATAZM, DATAWE, KILOSC,
                       PRSCHEDULE, KKTM, KWERSJA, PRSHEET, PLANSCOL, TERMDOST)
    values(:zam, :rejestr, :ltyp, :lmagazyn, current_date, current_date, :ilosczam,
                       :prschedule, :ktm, 0, :sheet, :planscol, :datareal);
    ilwygzam = :ilwygzam + 1;
    /*dodanie pozycji*/
    for select PS.ref from PRSHMAT PS where PS.sheet = :sheet and PS.ktm is not null and
      PS.ktm <> ''
    into :prshmat
    do begin
      execute procedure GEN_REF('POZZAM') returning_values :pozzam;
      insert into POZZAM(REF, ZAMOWIENIE, KTM, WERSJA, JEDN, ILOSC, KILOSC)
      select :pozzam, :zam, KTM, WERSJA, JEDN, GROSSQUANTITY*:ilosczam, GROSSQUANTITY
      from PRSHMAT where ref = :prshmat;
    end
    /*dodanie zlecenia do danego harmonogramu*/
    insert into PRSCHEDZAM(PRSCHEDULE,ZAMOWIENIE) values (:prschedule, :zam);
    datareal = :datareal +1;
    ilosc = :ilosc - :ilosczam;
  end
  suspend;
end^
SET TERM ; ^
