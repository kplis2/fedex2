--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FRF_GUS_TWO(
      FRVPSN integer,
      PERIOD varchar(6) CHARACTER SET UTF8                           ,
      PREFIX varchar(10) CHARACTER SET UTF8                           ,
      FIELD varchar(6) CHARACTER SET UTF8                           )
  returns (
      AMOUNT numeric(15,2))
   as
declare variable FRVHDR integer;
declare variable FRPSN integer;
declare variable FRCOL integer;
declare variable YEARID integer;
declare variable CACHEPARAMS varchar(255);
declare variable DDPARAMS varchar(255);
declare variable COMPANY integer;
declare variable FRVERSION integer;
declare variable MONTHID integer;
declare variable FIRSTDAY date;
declare variable LASTDAY date;
declare variable TMPDATE date;
declare variable FULLEMPLPERS numeric(14,2);
declare variable NOTFULLEMPLPERS numeric(14,2);
declare variable SUMWORKDIM numeric(14,2);
declare variable MONTHPERIODDAYS numeric(14,2);
declare variable EMPLOYEE integer;
declare variable CPER varchar(6);
declare variable WORKHOURS numeric(14,2);
declare variable WORKEDHOURS numeric(14,2);
declare variable NOTWORKEDHOURS numeric(14,2);
declare variable EABSHOURS numeric(14,2);
declare variable EXTRAHOURS numeric(14,2);
declare variable TMPAMOUNT numeric(14,2);
declare variable ODDZIAL varchar(10);
begin

  select F.frvhdr, F.frpsn, F.frcol
    from frvpsns F
    where F.ref = :frvpsn
    into :frvhdr, :frpsn, :frcol;

  select F.company, F.frversion
    from frvhdrs F
    where F.ref = :frvhdr
    into :company, :frversion;

  cacheparams = field||';'||yearid||';'||company;
  ddparams = '';

  if (not exists (select ref from frvdrilldown where functionname = 'GUS_TWO'
           and cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr)) then
  begin
    yearid = cast(substring(:period from 1 for 4) as integer);
    amount = 0;
    monthid = 1;

-- OKRESLENIE GLOWNEGO MIEJSCA PRACY (BS52749)
    select first 1 oddzial from oddzialy where glowny = 1 and company = :company
      into :oddzial;

    if (oddzial is null) then
    begin
      oddzial = 'CENTRALA';
      if (not exists(select first 1 1 from oddzialy where oddzial = :oddzial and company = :company)) then
        oddzial = '';
    end

--- LICZBA PRACUJACYCH W OSOBACH NA KONIEC ROKU
    if (field = 'D1P1') then
    begin
      TmpDate = cast(yearid || '/12/31' as date);
      select count(distinct E.person)
        from emplcontracts C
          join employees E on (C.employee = E.ref)
          left join employment EM on (Em.employee = E.ref and Em.emplcontract = C.ref
                                      and Em.fromdate <= :TmpDate and (Em.todate >= :TmpDate or Em.todate is null))
        where C.fromdate <= :TmpDate
          and (coalesce(C.enddate,C.todate) >= :TmpDate or coalesce(C.enddate,C.todate) is null)
          and (:oddzial = '' or coalesce(EM.branch,'') = :oddzial or (EM.branch is null and coalesce(C.branch,'') = :oddzial))
        into: amount;
    end

--- PRZECIETNA LICZBA OSOB PELNOZATRUDNIONYCH I NIEPELNOZATRUDNIONYCH
    else if (field like 'D2P%') then
    begin
      while (monthid  < 13) do
      begin
        FirstDay = cast(yearid || '/' || monthid || '/1' as date);
        if (monthid = 12) then
          LastDay = cast(yearid || '/' || monthid || '/31' as date);
        else
          LastDay = cast(yearid || '/' || (monthid + 1) || '/1' as date) - 1;
        MonthPeriodDays = LastDay - FirstDay + 1;
        TmpDate = FirstDay;
        SumWorkDim = 0;
        FullEmplPers = 0;
        NotFullEmplPers = 0;

        while (TmpDate <= LastDay) do
        begin

          select :FullEmplPers + count(*), :NotFullEmplPers + sum(counter), :SumWorkDim + sum(workdim) from (
            select E.person as PERSON, count(E.person) as COUNTER, sum(EM.workdim) as WORKDIM
              from employees e
                join employment EM on (em.employee = e.ref and
                                       ((:field = 'D2P1' and EM.workdim = 1)       --liczenie przecietnej osbo pelnozatrudnionych
                                        or (:field <> 'D2P1' and EM.workdim < 1))) --liczenie przecietnej osbo niepelnozatrudnionych
             where e.company = :company
               and EM.fromdate <= :TmpDate
               and (EM.todate >= :TmpDate or EM.todate is null)
               and (coalesce(EM.branch,'') = :oddzial or :oddzial = '')
               and not exists (select first 1 1 from eabsences A
                                 where A.employee = E.ref
                                   and A.correction in (0,2)
                                   and A.fromdate <= :TmpDate and :TmpDate <= A.todate
                                   and A.ecolumn in (260, 300, 390))
            group by E.person )
            into :FullEmplPers, :NotFullEmplPers, :SumWorkDim;

          TmpDate = TmpDate + 1;
        end
        if (field = 'D2P1') then
          amount = amount + FullEmplPers/MonthPeriodDays;     --przecietna liczba osob pelnozatrudnionych
        else if (field = 'D2P2.1') then
          amount = amount + NotFullEmplPers/MonthPeriodDays;  --przec. l. osob niepelnozatrudnionych - pelne etaty
        else if (field = 'D2P2.2') then
          amount = amount + SumWorkDim/MonthPeriodDays ;      --przec. l. osob niepelnozatrudnionych -  niepelne etaty
        monthid = monthid + 1;
      end
      amount = amount / 12;
    end

--- CZAS PRACY
    else if (field like 'D3W%') then
    begin
    --z uwagi na brak wytycznych co do sposobu naliczenia w przypadku jak
    --pracownik ma zmiane etatu z pelnego na niepelny w trakcie miesiaca
    --informacja o etacie pobierana bedzie najaktualniesza z konca miesiaca

      while (monthid  < 13) do
      begin
        FirstDay = cast(yearid || '/' || monthid || '/1' as date);
        if (monthid = 12) then
          LastDay = cast(yearid || '/' || monthid || '/31' as date);
        else
          LastDay = cast(yearid || '/' || (monthid + 1) || '/1' as date) - 1;

        if (field like 'D3W_K1') then
        begin
        --liczenie czasu pracy dla osob pelnozatrudnionych
          for
            select distinct E.ref
              from employees e
                join employment EM on (em.fromdate = (select max(em2.fromdate) from employment em2
                                                        where em2.employee = em.employee and em2.fromdate <= :LastDay)
                                       and em.employee = e.ref)
               where e.company = :company and EM.workdim = 1
                 and (coalesce(EM.branch,'') = :oddzial or :oddzial = '')
               into :employee
          do begin
          --okreslenie liczby godzin do przepracownia
            workhours = 0;
            select ws /3600.00from ecal_work(:employee, :FirstDay, :LastDay)
              into :workhours;

            if (workhours <> 0) then
            begin
            -- badanie na wykluczenie pracownika jesli mial urlop wychowawczy, bezplatny itp
              eabshours = null;
              select sum(worksecs)/3600.00
                from eabsences
                where employee = :employee
                  and correction in (0,2)
                  and fromdate >= :FirstDay and todate <= :LastDay
                  and ecolumn in (260, 300, 390)
                into: eabshours;
               eabshours = coalesce(eabshours,0);
               if (eabshours < workhours ) then
               begin
              -- okreslenie liczby godzin nieprzepracownych
                 notworkedhours = null;
                 select sum(worksecs)
                   from eabsences
                   where employee = :employee
                     and correction in (0,2)
                     and fromdate >= :FirstDay and todate <= :LastDay
                   into :notworkedhours;
                 notworkedhours = coalesce(notworkedhours,0) / 3600.00;
             -- okreslenie liczby godzin przepracowanych
                workedhours = workhours - notworkedhours;
             -- okreslenie liczby godzin nadliczbowych
                extrahours = null;

                select sum(amount)
                  from eworkedhours
                  where employee = :employee and ecolumn in (600,610)
                    and hdate >= :FirstDay and hdate <= :LastDay
                  into :extrahours;
                extrahours = coalesce(extrahours,0);

               if (:field like 'D3W2K_') then         --godziny przepracowane w czsie normalnym
                 amount = amount + workedhours;
               else if (:field like 'D3W3K_') then    --godziny przepracowane w czasie nadliczbowym
                 amount = amount + extrahours;
               else if (:field like 'D3W4K_') then    --godziny nieprzepracowane
                 amount = amount + notworkedhours;
              end
            end
          end --for
        end else
        begin
        --liczenie czasu pracy dla osob niepelnozatrudnionych
          for
            select distinct E.ref
              from employees e
                  join employment EM on (em.fromdate = (select max(em2.fromdate) from employment em2
                                                          where em2.employee = em.employee and em2.fromdate <= :LastDay)
                                         and em.employee = e.ref)
               where e.company = :company and EM.workdim <> 1
                 and (coalesce(EM.branch,'') = :oddzial or :oddzial = '')
               into :employee
          do begin
          --okreslenie liczby godzin do przepracownia
            workhours = 0;
            select ws /3600.00 from ecal_work(:employee, :FirstDay, :LastDay)
              into :workhours;

            if (workhours <> 0) then
            begin
            -- badanie na wykluczenie pracownika jesli mial urlop wychowawczy, bezplatny itp
              eabshours = null;
              select sum(worksecs)/3600.00
                from eabsences
                where employee = :employee
                  and correction in (0,2)
                  and fromdate >= :FirstDay and todate <= :LastDay
                  and ecolumn in (260, 300, 390)
                into: eabshours;
               eabshours = coalesce(eabshours,0);
               if (eabshours < workhours ) then
               begin
              -- okreslenie liczby godzin nieprzepracownych
                 notworkedhours = null;
                 select sum(worksecs)
                   from eabsences
                   where employee = :employee
                     and correction in (0,2)
                     and fromdate >= :FirstDay and todate <= :LastDay
                   into :notworkedhours;
                 notworkedhours = coalesce(notworkedhours,0) / 3600.00;
             -- okreslenie liczby godzin przepracowanych
                workedhours = workhours - notworkedhours;
             -- okreslenie liczby godzin nadliczbowych
                extrahours = null;

                select sum(amount)
                  from eworkedhours
                  where employee = :employee and ecolumn in (600,610)
                    and hdate >= :FirstDay and hdate <= :LastDay
                  into :extrahours;
                extrahours = coalesce(extrahours,0);

               if (:field like 'D3W2K_') then         --godziny przepracowane w czsie normalnym
                 amount = amount + workedhours;
               else if (:field like 'D3W3K_') then    --godziny przepracowane w czasie nadliczbowym
                 amount = amount + extrahours;
               else if (:field like 'D3W4K_') then    --godziny nieprzepracowane
                 amount = amount + notworkedhours;
              end
            end
          end --for
        end
        monthid = monthid + 1;
      end --while
      amount = amount /1000.00;
    end

--- SKLADNIKI KOSZTU PRACY
    else if (field like 'D4W%'
           --Nieobslugiwane w momencie tworzenia sprawozdania:
             and field <> 'D4W06' --Nagrody(bez wyplat z zysku)
             and field <> 'D4W09' --Wynagrodzenie za czas przestojów
             and field <> 'D4W14' --Dodatkowe wynagrodzenie roczna dla pracownikow jednostek budzetowych
             and field <> 'D4W16' --Honoraria ogolem
             and field <> 'D4W17' --Wydatki na doskonalenie, ksztalcenie i przekwalifikowanie kadr
             and field <> 'D4W18' --Wydatki na delegacje sluzbowe
             and field <> 'D4W20' --Dobrowolne skladki na ubezpieczenia spoleczne
             and field <> 'D4W21' --  w tym skladnki na pracownicze systemy emertytalno-rentowe
             and field <> 'D4W22' --Wydatki zwiazane z bezepieczenstwem i higiena pracy
             and field <> 'D4W24' --Swiadczenia o charkterze rzeczowym
             and field <> 'D4W25' --Pozostale wydatki
    ) then
    begin

      while (monthid  < 13) do
      begin
        FirstDay = cast(yearid || '/' || monthid || '/1' as date);
        if (monthid = 12) then
          LastDay = cast(yearid || '/' || monthid || '/31' as date);
        else
          LastDay = cast(yearid || '/' || (monthid + 1) || '/1' as date) - 1;

        if (monthid < 10) then
          cper = :yearid || '0' || monthid;
        else
          cper = :yearid || monthid;

        for
          select distinct E.ref
            from employees e
              join employment EM on (em.fromdate = (select max(em2.fromdate) from employment em2
                                                      where em2.employee = em.employee and em2.fromdate <= :LastDay)
                                    and em.employee = e.ref)
            where e.company = :company
              and (coalesce(EM.branch,'') = :oddzial or :oddzial = '')
          into :employee
        do begin
         -- okreslenie liczby godzin do przepracownia
            workhours = 0;
            select ws /3600.00
              from ecal_work(:employee, :FirstDay, :LastDay)
              into :workhours;

            if (workhours <> 0) then
            begin
            --badanie na wykluczenie pracownika jesli mial urlop wychowawczy, bezplatny itp
              eabshours = null;
              select sum(worksecs)/3600.00
                from eabsences
                where employee = :employee
                  and fromdate >= :FirstDay and todate <= :LastDay
                  and correction in (0,2)
                  and ecolumn in (260, 300, 390)
                into: eabshours;
              eabshours = coalesce(eabshours,0);
              if (eabshours < workhours ) then
              begin
                 TmpAmount = null;
                 if (field = 'D4W03') then
                 begin
                 --Wynagrodzenia zasadnicze
                   select sum(P.pvalue) / 1000.00
                     from epayrolls PR
                       join eprpos P on (P.payroll = PR.ref)
                       where PR.cper = :cper and pr.company = :company
                         and p.employee = :employee and pr.empltype = 1
                         and P.ecolumn = 1000
                      into :TmpAmount;
                 end else if (field = 'D4W04') then
                 begin
                 --Dodatki za staz pracy
                   select sum(P.pvalue) / 1000.00
                     from epayrolls PR
                       join eprpos P on (P.payroll = PR.ref)
                       where PR.cper = :cper and pr.company = :company
                         and p.employee = :employee and pr.empltype = 1
                         and P.ecolumn = 1050
                      into :TmpAmount;
                 end else if (field = 'D4W05') then
                 begin
                 --Premie wyplacane periodycznie
                   select sum(P.pvalue) / 1000.00
                     from epayrolls PR
                       join eprpos P on (P.payroll = PR.ref)
                       where PR.cper = :cper and pr.company = :company
                         and p.employee = :employee and pr.empltype = 1
                         and P.ecolumn in (2000,1400)
                      into :TmpAmount;
                 end else if (field = 'D4W07') then
                 begin
                 --Wynagrodzenie za prace w godzinach nadliczbowych
                   select sum(P.pvalue) / 1000.00
                     from epayrolls PR
                       join eprpos P on (P.payroll = PR.ref)
                       where PR.cper = :cper and pr.company = :company
                         and p.employee = :employee and pr.empltype = 1
                         and P.ecolumn in (1200,1210)
                     into :TmpAmount;
                 end else if (field = 'D4W08') then
                 begin
                 --Wynagrodzenie za czas choroby (ze srodkow pracodawcy)
                   select sum(P.pvalue) / 1000.00
                     from epayrolls PR
                       join eprpos P on (P.payroll = PR.ref)
                       where PR.cper = :cper and pr.company = :company
                         and p.employee = :employee and pr.empltype = 1
                         and P.ecolumn in (3500,3510)
                     into :TmpAmount;
                 end else if (field = 'D4W10') then
                 begin
                 --Pozostale wynagrodzenia za czas niewykonywania pracy
                   select sum(P.pvalue) / 1000.00
                     from epayrolls PR
                       join eprpos P on (P.payroll = PR.ref)
                       where PR.cper = :cper and pr.company = :company
                         and p.employee = :employee and pr.empltype = 1
                         and P.ecolumn in (1500,1510,3600,3610)
                     into :TmpAmount;
                 end else if (field = 'D4W11') then
                 begin
                 --Nagrody jubileuszowe, gratyfikacje
                   select sum(P.pvalue) / 1000.00
                     from epayrolls PR
                       join eprpos P on (P.payroll = PR.ref)
                       where PR.cper = :cper and pr.company = :company
                         and p.employee = :employee and pr.empltype = 1
                         and P.ecolumn = 2200
                     into :TmpAmount;
                 end else if (field = 'D4W12') then
                 begin
                 --Odprawy wyplacone przy przejsciu na emeryture lub rente
                   select sum(P.pvalue) / 1000.00
                     from epayrolls PR
                       join eprpos P on (P.payroll = PR.ref)
                       where PR.cper = :cper and pr.company = :company
                         and p.employee = :employee and pr.empltype = 1
                         and P.ecolumn in (2100, 2110)
                     into :TmpAmount;
                 end else if (field = 'D4W13') then
                 begin
                 --Pozostale skladniki wynagrodzen osobowych
                   select sum(P.pvalue) / 1000.00
                     from epayrolls PR
                       join eprpos P on (P.payroll = PR.ref)
                       where PR.cper = :cper and pr.company = :company
                         and p.employee = :employee and pr.empltype = 1
                         and P.ecolumn > 1000 and P.ecolumn < 4000
                         and P.ecolumn not in (1050, 1200, 1210, 1400, 1500, 1510, 2000, 2100, 2110, 2200, 3500, 3510, 3600, 3610 )
                     into :TmpAmount;
                 end else if (field = 'D4W15') then
                 begin
                 --Wynagrodzenia z tytuu umowy zlecennia lub umowy o dzielo
                   select sum(P.pvalue) / 1000.00
                     from epayrolls PR
                       join eprpos P on (P.payroll = PR.ref)
                       where PR.cper = :cper and pr.company = :company
                         and p.employee = :employee and pr.empltype = 2
                         and P.ecolumn = 3000
                     into :TmpAmount;
                 end else if (field = 'D4W19') then
                 begin
                 --Obligatoryjne skladki na ubezp. emer. rent. i wypadk. oplacane
                 --przez pracodawce lacznie z FSP i FP
                   select sum(P.pvalue) / 1000.00
                     from epayrolls PR
                       join eprpos P on (P.payroll = PR.ref)
                       where PR.cper = :cper and pr.company = :company
                         and p.employee = :employee and pr.empltype in (1,2)
                         and P.ecolumn in (8200, 8210, 8220, 8810, 8820)
                     into :TmpAmount;
                 end else if (field = 'D4W23') then
                 begin
                 --Zakladowy fundusz swiadczen socjalnych
                   select sum(P.pvalue) / 1000.00
                     from epayrolls PR
                       join eprpos P on (P.payroll = PR.ref)
                       where PR.cper = :cper and pr.company = :company
                         and p.employee = :employee and pr.empltype in (1,2)
                         and P.ecolumn = 5050
                     into :TmpAmount;
                 end 
                 amount = amount + coalesce(TmpAmount,0);
              end
            end
         end --for
         monthid = monthid + 1;
      end --while
    end
  end else
    select first 1 fvalue from frvdrilldown
      where cacheparams = :cacheparams and frversion = :frversion and frvhdr = :frvhdr
      into :amount;
  if (amount is null) then amount = 0;
  insert into frvdrilldown (frvhdr, frpsn, frcol, functionname, cacheparams, ddparams, fvalue, frversion)
     values (:frvhdr, :frpsn, :frcol, 'GUS_TWO', :cacheparams, :ddparams, :amount, :frversion);
  suspend;
end^
SET TERM ; ^
