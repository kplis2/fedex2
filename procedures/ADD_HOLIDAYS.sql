--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ADD_HOLIDAYS(
      CALENDAR integer,
      PYEAR integer)
   as
declare variable d date;
begin
  -- Nowy Rok
  d = cast(pyear as varchar(4)) || '/01/01';
  insert into ECALPATTDAYS (calendar, pdate, daykind, workstart, workend, worktime, descript, pattkind)
    values (:calendar, :d, 0, '00:00:00', '00:00:00', 0, 'Nowy Rok', 0);

  -- Trzech Kroli
  if (pyear >= 2011) then
  begin
    d = cast(pyear as varchar(4)) || '/01/06';
    insert into ECALPATTDAYS (calendar, pdate, daykind, workstart, workend, worktime, descript, pattkind)
      values (:calendar, :d, 0, '00:00:00', '00:00:00', 0, 'Trzech Króli', 0);
  end

  -- Wielkanoc
  execute procedure wielkanoc(pyear) returning_values d;
  insert into ECALPATTDAYS (calendar, pdate, daykind, workstart, workend, worktime, descript, pattkind)
    values (:calendar, :d, 0, '00:00:00', '00:00:00', 0, 'Wielkanoc',0);

  -- Wielkanoc drugi dzień
  d = d + 1;
  insert into ECALPATTDAYS (calendar, pdate, daykind, workstart, workend, worktime, descript, pattkind)
    values (:calendar, :d, 0, '00:00:00', '00:00:00', 0, 'Wielkanoc',0);

  -- Zeslanie ducha swietego (zielone swiatki)
  d = d + 48;
  insert into ECALPATTDAYS (calendar, pdate, daykind, workstart, workend, worktime, descript, pattkind)
    values (:calendar, :d, 0, '00:00:00', '00:00:00', 0, 'Zesłanie ducha świętego',0);

  -- Boże Cialo
  d = d + 11;
  insert into ECALPATTDAYS (calendar, pdate, daykind, workstart, workend, worktime, descript, pattkind)
    values (:calendar, :d, 0, '00:00:00', '00:00:00', 0, 'Boże Ciało',0);

  -- 1 Maja
  d = cast(pyear as varchar(4)) || '/05/01';
  insert into ECALPATTDAYS (calendar, pdate, daykind, workstart, workend, worktime, descript, pattkind)
    values (:calendar, :d, 0, '00:00:00', '00:00:00', 0, 'Święto Pracy',0);

  -- 3 Maja
  d = cast(pyear as varchar(4)) || '/05/03';
  insert into ECALPATTDAYS (calendar, pdate, daykind, workstart, workend, worktime, descript, pattkind)
    values (:calendar, :d, 0, '00:00:00', '00:00:00', 0, 'Konstytucji 3 Maja',0);

  -- Wniebowzi‘cie N.M.P.
  d = cast(pyear as varchar(4)) || '/08/15';
  insert into ECALPATTDAYS (calendar, pdate, daykind, workstart, workend, worktime, descript, pattkind)
    values (:calendar, :d, 0, '00:00:00', '00:00:00', 0, 'Wniebowzięcie N.M.P',0);

  -- 1 Listopad
  d = cast(pyear as varchar(4)) || '/11/01';
  insert into ECALPATTDAYS (calendar, pdate, daykind, workstart, workend, worktime, descript, pattkind)
    values (:calendar, :d, 0, '00:00:00', '00:00:00', 0, 'Wszystkich Świętych',0);

  -- 11 Listopad
  d = cast(pyear as varchar(4)) || '/11/11';
  insert into ECALPATTDAYS (calendar, pdate, daykind, workstart, workend, worktime, descript, pattkind)
    values (:calendar, :d, 0, '00:00:00', '00:00:00', 0, 'Święto Niepodległości',0);

  -- Boże Narodzenia
  d = cast(pyear as varchar(4)) || '/12/25';
  insert into ECALPATTDAYS (calendar, pdate, daykind, workstart, workend, worktime, descript, pattkind)
    values (:calendar, :d, 0, '00:00:00', '00:00:00', 0, 'Boże Narodzenie',0);

  -- Boże narodzenia drugi dzień
  d = cast(pyear as varchar(4)) || '/12/26';
  insert into ECALPATTDAYS (calendar, pdate, daykind, workstart, workend, worktime, descript, pattkind)
    values (:calendar, :d, 0, '00:00:00', '00:00:00', 0, 'Boże Narodzenie', 0);
end^
SET TERM ; ^
