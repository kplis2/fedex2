--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MINUS_ORDERED_MWSSTOCK(
      WH char(3) CHARACTER SET UTF8                           ,
      WHSEC integer,
      WHAREA integer,
      GOOD varchar(40) CHARACTER SET UTF8                           ,
      VERS integer,
      MWSCONSTLOC integer,
      MWSPALLOC integer,
      QUANTITY numeric(14,4),
      LOT integer,
      STANCEN integer,
      X_PARTIA DATE_ID,
      X_SERIAL_NO INTEGER_ID,
      X_SLOWNIK INTEGER_ID)
   as
declare variable refmwsstock integer;
declare variable ordered numeric(14,4);
begin
  if((lot is null) or (lot = 0)) then exception STCEN_DOSTAWA_NULL;
  select max(ref), sum(ordered)
    from MWSSTOCK
    where wh = :wh and vers=:vers
      and mwspalloc = :mwspalloc and lot = :lot
      and ((:x_partia is not null and x_partia = :x_partia) or  :x_partia is null)        -- XXX KBI
      and ((:x_serial_no is not null and x_serial_no = :x_serial_no) or :x_serial_no is null)  -- XXX KBI
      and (coalesce(x_slownik,0) = coalesce(:x_slownik,0))      -- XXX KBI
      and ((:mwsconstloc is not null and mwsconstloc = :mwsconstloc) or mwsconstloc is null)
      into refmwsstock, ordered;
  if (ordered is null) then ordered = 0;
  if (ordered < quantity) then
    exception NO_ORDERED_FOR_MWSACT;
  if(refmwsstock > 0) then
    update MWSSTOCK set ordered = ordered - :quantity
      where ref = :refmwsstock;
  else if (quantity <> 0) then
    exception NO_ORDERED_FOR_MWSACT;
end^
SET TERM ; ^
