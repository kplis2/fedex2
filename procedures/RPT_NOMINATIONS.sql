--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_NOMINATIONS(
      PAYROLL integer)
  returns (
      NOMINATION numeric(14,2),
      AMOUNT integer)
   as
declare variable pvalue numeric(14,2);
  declare variable currency varchar(3);
begin
  --DS: wyliczanie nominalow wymaganych do wyplaty wynagrodzenia w kasie
  execute procedure get_config('WALDOM',2) returning_values currency;

  for
    select r.pvalue
      from epayrolls p
        join eprpos r on (r.payroll = p.ref and r.ecolumn = 9050)
      where p.ref = :payroll
      into :pvalue
  do begin
    for
      select n.cvalue
        from nominations n
        where n.currency = :currency
        order by n.cvalue desc
        into :nomination
    do begin
      amount = floor(pvalue / nomination);
      pvalue = pvalue - amount * nomination;
      suspend;
    end
  end
end^
SET TERM ; ^
