--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_KTMISFAKE(
      KTM KTM_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
begin
  status = 0;
  -- XXX Ldz sprawdzanie przed wydrukowaniem na przyjeciu czy KTM jest fake, w zaleznosci od tego drukujemy min 1 etykiete lub nie
  select first 1 1
    from towary t
    where t.ktm = ktm
      and t.altposmode = 1
  into :status;
  if (status is null) then status = 0;
  suspend;
end^
SET TERM ; ^
