--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE DOKUMPOZ_OBL_FROM_MWSACTS(
      DOCTYPE varchar(1) CHARACTER SET UTF8                           ,
      DOCID integer,
      DOCPOSID integer,
      STOCKTAKING smallint,
      REC smallint,
      QUANTITYCHANGE smallint,
      QUANTITYCCHANGE smallint)
   as
declare variable quantity numeric(14,4);
declare variable mwsactsquantity numeric(14,4);
declare variable quantityc numeric(14,4);
declare variable mwsactsquantityc numeric(14,4);
declare variable quantityl numeric(14,4);
declare variable mwsactsdone smallint;
begin
--  exception universal 'doctype '||coalesce(doctype,0)||'docid '||coalesce(docid,0)||'docposid '||coalesce(docposid,0)||'stoctaking'||coalesce(stocktaking,0)||'rec '||
 --coalesce(rec,0)||'quantitychange '|| coalesce(quantitychange,0)||'quantitycchange '||coalesce(quantitycchange,0);
  if (docposid is null) then
    exit;
  mwsactsdone = 0;
  quantity = 0;
  quantityc = 0;
  mwsactsquantity = 0;
  mwsactsquantityc = 0;
  -- ilosc z pozycji na zleceniach to:
  -- ilosc przychodowa na lokacje stala zwiazana z pozycja dokumentu
  -- odjac ilosc rozchodowana z lokacji stalej ale bez ilosci rozchodowanych na zewnatrz magazynu
  -- czyli takich, ktore nie maja lokacji koncowej, DLA ZLECEN MAGAZYNOWYCH
  -- dla INWENTARYZACYJNYCH LICZYMY ILOSCI WYDANE Z MAGAZYNU

  -- okreslam ilosc przychodowana na kazda lokacje paletowa zwiazana z pozycja dokumentu magazynowego
  if (doctype = 'M') then
  begin
    select sum(quantity), sum(quantityc), sum(quantityl)
      from mwsacts
      where doctype = :doctype
        and docid = :docid and docposid = :docposid
        and ((recdoc = 0 and autogen = 1) or (recdoc = 1 and mwsconstlocp is null))
        and stocktaking = 0
        and status <> 6
        into :mwsactsquantity, :mwsactsquantityc, :quantityl;

    if (mwsactsquantity is null) then mwsactsquantity = 0;
    if (mwsactsquantityc is null) then mwsactsquantityc = 0;
    if (quantityl is null) then quantityl = 0;
    if (quantityl < mwsactsquantity) then mwsactsquantity = quantityl;
    if (quantityl < mwsactsquantityc) then mwsactsquantityc = quantityl;

    quantity = quantity + mwsactsquantity;
    quantityc = quantityc + mwsactsquantityc;
    mwsactsquantity = 0;
    mwsactsquantityc = 0;
    if (not exists(select m.ref from mwsacts ma join mwsords m on (m.ref = ma.mwsord)
         where m.status <> 5 and ma.status <> 5 and ma.docid = :docid
           and ma.docposid = :docposid and ma.doctype = 'M')
         and not exists(select ref from dokumpoz where ref = :docposid --and ilosconmwsactsc < iloscl)
             and genmwsordsafter = 1)
    ) then
      mwsactsdone = 1;
    else
      mwsactsdone = 0;
  --           if (:quantity = 0 or :quantityc = 0 or (1=1) ) then
  --    exception universal :mwsactsquantity||' jak2 ? '||:mwsactsquantityc||'a'||:quantity||'b'||:quantityc;
    update dokumpoz set ilosconmwsacts = :quantity, ilosconmwsactsc = :quantityc,
        mwsactsdone = :mwsactsdone
      where ref = :docposid and dokument = :docid;
  end
end^
SET TERM ; ^
