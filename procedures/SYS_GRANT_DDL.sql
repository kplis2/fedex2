--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE SYS_GRANT_DDL(
      NAZWA varchar(255) CHARACTER SET UTF8                           ,
      TYP integer,
      GRANTFILTER integer)
  returns (
      DDL blob sub_type 1 SEGMENT SIZE 80 CHARACTER SET UTF8                           )
   as
declare variable username       varchar(255);
declare variable grantor        varchar(255);
declare variable grantname      varchar(10);
declare variable eol            varchar(32);
declare variable objecttype     varchar(10);
declare variable usertype       smallint;
begin
    eol =';
';
    ddl = '';
    if(TYP=4 or TYP=6 or TYP=7) then begin -- 4 - tabele, 6-procedury, 7-widoki
        for select
           trim(rdb$user),
           trim(rdb$grantor),
           case rdb$privilege
               when 'S' then 'SELECT'
               when 'U' then 'UPDATE'
               when 'D' then 'DELETE'
               when 'I' then 'INSERT'
               when 'X' then 'EXECUTE'
               when 'R' then 'REFERENCES'
           end,
           case :TYP
               when 4 then ''
               when 6 then 'PROCEDURE'
               when 7 then ''
           end,
           rdb$user_type
       from rdb$user_privileges
       where rdb$relation_name = upper(:nazwa)
        into :username, :grantor, :grantname, :objecttype, :usertype
        do begin
            select outname from sys_quote_reserved(:username) into :username;

            if (grantfilter=0 or (grantfilter=1 and :usertype = 8 and (username='"SYSDBA"' or username='"SENTE"' or username='"SENTELOGIN"')) ) then
            ddl = :ddl || 'GRANT ' || :grantname || ' ON ' || :objecttype || ' ' || :nazwa || ' TO ' || :username || ' GRANTED by ' || :grantor || :eol;
        end
    end
  suspend;
end^
SET TERM ; ^
