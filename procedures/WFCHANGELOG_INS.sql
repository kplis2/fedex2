--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE WFCHANGELOG_INS(
      WFINSTANCEREF WFINSTANCE_ID,
      WFTASKREF WFTASK_ID,
      USERUUID NEOS_ID,
      PROPERTY STRING40,
      NEWVALUE STRING1024,
      OLDVALUE STRING1024,
      DESCRIPT STRING2500,
      TYPE SMALLINT_ID)
   as
begin
  insert into wfchangelog (
    wfinstanceref,
    wftaskref,
    useruuid,
    property,
    newvalue,
    oldvalue,
    descript,
    type)
  values (
    :wfinstanceref,
    :wftaskref,
    :useruuid,
    :property,
    :newvalue,
    :oldvalue,
    :descript,
    :type);
end^
SET TERM ; ^
