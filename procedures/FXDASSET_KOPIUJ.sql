--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FXDASSET_KOPIUJ(
      ILOSC integer,
      FXDASSET integer)
   as
declare variable SYMBOL varchar(20);
declare variable NAME varchar(80);
declare variable AMORTGRP varchar(10);
declare variable ODDZIAL varchar(10);
declare variable DEPARTMENT varchar(10);
declare variable PURCHASEDT timestamp;
declare variable USINGDT timestamp;
declare variable SERIALNO varchar(20);
declare variable DESCRIPT varchar(255);
declare variable TBKSYMBOL BKSYMBOLS_ID;
declare variable TAMETH varchar(3);
declare variable TARATE numeric(10,5);
declare variable TACRATE numeric(10,5);
declare variable TACHDATE timestamp;
declare variable FAMETH varchar(3);
declare variable FARATE numeric(10,5);
declare variable FACRATE numeric(10,5);
declare variable FACHDATE timestamp;
declare variable ACTIV smallint;
declare variable TVALUE numeric(15,2);
declare variable TVALLIM numeric(15,2);
declare variable TALLOWENCE numeric(15,2);
declare variable TREDEMPTION numeric(15,2);
declare variable FVALUE numeric(15,2);
declare variable FREDEMPTION numeric(15,2);
declare variable FREST numeric(15,2);
declare variable PERSON integer;
declare variable COMPARMENT varchar(10);
declare variable FXDAMINOUT integer;
declare variable AMGRP varchar(1);
declare variable FBKSYMBOL BKSYMBOLS_ID;
declare variable PVALUE numeric(15,2);
declare variable LIQUIDATIONDATE timestamp;
declare variable LIQUIDATIONPERIOD integer;
declare variable ISINTASSET smallint;
declare variable COMPANY integer;
declare variable TVALUEGROSS numeric(15,2);
declare variable FVALUEGROSS numeric(15,2);
declare variable FXDTYPE integer;
declare variable NUM integer;
declare variable FXDASSETNEW integer;
declare variable KOPIA integer;
declare variable ELEMSYMBOL varchar(20);
declare variable ELEMNAME varchar(80);
declare variable ELEMDESCRIPT varchar(255);
declare variable ELEMSERIALNO varchar(20);
declare variable ELEMOPER integer;
declare variable CAREGOPER integer;
declare variable CAAMPERIOD integer;
declare variable CASHARE numeric(14,2);
declare variable CAACCOUNT ACCOUNT_ID;
declare variable CASTATUS smallint;
declare variable CATYPEAMORTIZATION smallint;
declare variable DQAMPERIOD integer;
declare variable DQTYPE integer;
declare variable DQDQTYPE integer;
declare variable DPUNIT varchar(10);
declare variable DPCOSTUNIT varchar(10);
begin
   select symbol, name, amortgrp, oddzial, department, purchasedt, usingdt, serialno,
          descript, tbksymbol, tameth, tarate, tacrate, tachdate, fameth, farate,
          facrate, fachdate, activ, tvalue, tvallim, tallowence, tredemption, fvalue,
          fredemption, frest, person, comparment, fxdaminout, amgrp, fbksymbol, pvalue,
          liquidationdate, liquidationperiod, isintasset, company, tvaluegross, fvaluegross,
          fxdtype, num, dpunit, dpcostunit
          from fxdassets where ref = :fxdasset into
          :symbol, :name, :amortgrp, :oddzial, :department, :purchasedt, :usingdt, :serialno,
          :descript, :tbksymbol, :tameth, :tarate, :tacrate, :tachdate, :fameth, :farate,
          :facrate, :fachdate, :activ, :tvalue, :tvallim, :tallowence, :tredemption, :fvalue,
          :fredemption, :frest, :person, :comparment, :fxdaminout, :amgrp, :fbksymbol, :pvalue,
          :liquidationdate, :liquidationperiod, :isintasset, :company, :tvaluegross, :fvaluegross,
          :fxdtype, :num, :dpunit, :dpcostunit;


   select coalesce(max(kopia),0) from fxdassets where fxdassetpre = :fxdasset into :kopia;
   kopia = :kopia + 1;

   execute procedure GEN_REF('FXDASSETS') returning_values fxdassetnew;

   insert into fxdassets (ref, symbol, name, amortgrp, oddzial, department, purchasedt, usingdt, serialno,
          descript, tbksymbol, tameth, tarate, tacrate, tachdate, fameth, farate,
          facrate, fachdate, activ, tvalue, tvallim, tallowence, tredemption, fvalue,
          fredemption, frest, person, comparment, fxdaminout, amgrp, fbksymbol, pvalue,
          liquidationdate, liquidationperiod, isintasset, company, tvaluegross, fvaluegross,
          fxdtype, num, kopia, fxdassetpre, dpunit, dpcostunit)
          values (
          :fxdassetnew, :symbol||'/'||:kopia, :name, :amortgrp, :oddzial, :department, :purchasedt, :usingdt, :serialno,
          :descript, :tbksymbol, :tameth, :tarate, :tacrate, :tachdate, :fameth, :farate,
          :facrate, :fachdate, :activ, :tvalue, :tvallim, :tallowence, :tredemption, :fvalue,
          :fredemption, :frest, :person, :comparment, :fxdaminout, :amgrp, :fbksymbol, :pvalue,
          :liquidationdate, :liquidationperiod, :isintasset, :company, :tvaluegross, :fvaluegross,
          :fxdtype, :num, :kopia, :fxdasset, :dpunit, :dpcostunit);

    for select symbol, name, descript, serialno, regoper
        from fxdaelems where fxdasset = :fxdasset
        into :elemsymbol, :elemname, :elemdescript, :elemserialno, elemoper
    do
      insert into fxdaelems (fxdasset,symbol, name, descript, serialno, regoper)
      values (:fxdassetnew, :elemsymbol, :elemname, :elemdescript, :elemserialno, :elemoper);

    for select regoper, amperiod, share, account, status, typeamortization
        from fxdassetca where fxdasset = :fxdasset
        into :caregoper, :caamperiod, :cashare, :caaccount, :castatus, :catypeamortization
    do
      insert into fxdassetca (fxdasset, regoper, amperiod, share, account, status, typeamortization)
      values (:fxdassetnew, :caregoper, :caamperiod, :cashare, :caaccount, :castatus, :catypeamortization);

    for select amperiod, dq.dqftype
        from dqfamperiods dq where dq.fxdasset = :fxdasset
        into :dqamperiod, :dqdqtype
    do
      insert into dqfamperiods (fxdasset, amperiod, dqftype)
      values (:fxdassetnew, :dqamperiod, :dqdqtype);

end^
SET TERM ; ^
