--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE UPDATE_EPRPOS_VALUE(
      EMPLOYEEREF integer,
      ECOLUMN integer,
      PAYROLL integer,
      SETVALUE numeric(14,2))
   as
declare variable chgoperator integer;
declare variable pref integer;
begin

  select ref from eprpos
    where payroll = :payroll and employee = :employeeref and ecolumn = :ecolumn
    into :pref;

  if (setvalue <> 0) then
  begin
    if (pref is null) then
      insert into eprpos (payroll, employee, ecolumn, pvalue)
        values (:payroll, :employeeref, :ecolumn, :setvalue);
    else begin
      execute procedure get_global_param ('AKTUOPERATOR') returning_values chgoperator;
      update eprpos set pvalue = :setvalue, chgoperator = :chgoperator where ref = :pref;
    end
  end else
  if (setvalue = 0) then
    delete from eprpos where ref = :pref;
end^
SET TERM ; ^
