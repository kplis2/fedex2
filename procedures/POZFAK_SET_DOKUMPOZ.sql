--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE POZFAK_SET_DOKUMPOZ(
      POZFAKREF integer,
      DOKUMPOZREF integer)
   as
declare variable cnt integer;
declare variable iloscmag numeric(14,4);
declare variable iloscfak numeric(14,4);
declare variable nagfakref integer;
declare variable piloscm integer;
declare variable pcenamag numeric(14,4);
begin
  --powiaz pozycje
  update dokumpoz set frompozfak = :pozfakref where ref=:dokumpozref;
  select count(*) from dokumpoz where frompozfak=:pozfakref into :cnt;
  if(:cnt=1) then
    update pozfak set fromdokumpoz = :dokumpozref where ref=:pozfakref;
  else
    update pozfak set fromdokumpoz = null where ref=:pozfakref;
  -- nadaj pcenamag, aby poprawnie tworzyc noty
  -- ale tylko do faktur z reki a nie korygowanych
  select pcenasr from dokumpoz where ref=:dokumpozref into :pcenamag;
  update pozfak set pcenamag=:pcenamag where ref=:pozfakref and refk is null;

  --sprawdz czy ilosci powiazane sie zgadzaja - zadziala tylko dla zakupu i nie korekt
  select sum(case when defdokum.wydania=0 then dokumpoz.ilosc else -dokumpoz.ilosc end)
    from dokumpoz
    left join dokumnag on (dokumnag.ref=dokumpoz.dokument)
    left join defdokum on (defdokum.symbol=dokumnag.typ)
    where dokumpoz.frompozfak=:pozfakref
    into :iloscmag;
  select iloscm-piloscm, piloscm, dokument from pozfak where ref=:pozfakref into :iloscfak, :piloscm, :nagfakref;
  if(:piloscm=0 and :iloscfak<:iloscmag) then exception DOKUMNOT_AKC_EXPT 'Zbyt duża ilosc na skojarzonych dok. mag.';
  execute procedure NAGFAK_UPDATE_MAGCONNECT(:nagfakref);
end^
SET TERM ; ^
