--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_FK_NOTA_CANCEL(
      REFNOTA integer,
      ODDATY timestamp,
      DODATY timestamp,
      WEZWTYP varchar(10) CHARACTER SET UTF8                           )
  returns (
      REF integer,
      DOCREF integer,
      NUMER integer,
      SYMBOL varchar(30) CHARACTER SET UTF8                           ,
      DATA timestamp,
      KHKOD varchar(255) CHARACTER SET UTF8                           ,
      KHBANK varchar(40) CHARACTER SET UTF8                           ,
      KHACCOUNT varchar(40) CHARACTER SET UTF8                           ,
      KHKODKS varchar(255) CHARACTER SET UTF8                           ,
      KHNAZWA varchar(255) CHARACTER SET UTF8                           ,
      KHADRES varchar(500) CHARACTER SET UTF8                           ,
      SUMAMOUNT numeric(14,2),
      SUMINTEREST numeric(14,2),
      TEXT varchar(1024) CHARACTER SET UTF8                           ,
      KHNIP varchar(20) CHARACTER SET UTF8                           ,
      KHMIASTO varchar(30) CHARACTER SET UTF8                           ,
      KHULICA varchar(80) CHARACTER SET UTF8                           )
   as
declare variable notatyp varchar(10);
declare variable slodef integer;
declare variable slopoz integer;
declare variable nic varchar(255);
declare variable slodefref integer;
begin
  ref = 0;
  for select NOTYNAG.ref, NOTYNAG.data, NOTYNAG.notatyp, NOTYNAG.symbol,
    NOTYNAG.slokod, NOTYNAG.slokodks, notynag.slodef, notynag.slopoz,
    bdebit, notynag.interests
  from NOTYNAG
  where ((:refnota is null ) or (:refnota = 0) or (:refnota = notynag.ref))
  and notynag.notakind = 1
  and ((:oddaty is null) or (notynag.data >= :oddaty))
  and ((:dodaty is null) or (notynag.data <= :dodaty))
  and ((:wezwtyp is null) or (:wezwtyp = '') or (:wezwtyp = notynag.notatyp))
  order by notynag.data,notynag.slokodks
  into :docref, :data, :notatyp, :symbol, :khkod, :khkodks, :slodef, :slopoz,
       :sumamount,:suminterest
  do begin
    execute procedure SLO_DANE(:slodef, :slopoz) returning_values :khkod,:khnazwa,:nic;
    select min(ref) from slodef where slodef.typ = 'KLIENCI' into :slodefref;
    if(:slodef = :slodefref) then
      select KLIENCI.ULICA||iif(coalesce(KLIENCI.NRDOMU,'')<>'',' '||KLIENCI.NRDOMU,'')||iif(coalesce(KLIENCI.NRLOKALU,'')<>'','/'||KLIENCI.NRLOKALU,'')
        ||', '||KLIENCI.kodp||' '||KLIENCI.MIASTO, KLIENCI.NIP,
        KLIENCI.ULICA, KLIENCI.kodp||' '||KLIENCI.MIASTO
          from KLIENCI
            where REF=:slopoz into :khadres, :khnip, :khulica, :khmiasto;
    select NOTYTYP.degree, NOTYTYP.descript from NOTYTYP where NOTYTYP.TYP = :notatyp into :numer, :text;
    ref = :ref + 1;
    select first 1 b.bank, b.account from bankaccounts b
      join notynag n on (n.slodef = b.dictdef and n.slopoz = b.dictpos)
      where b.hbstatus = 1
        and n.ref = :docref
      into :khbank, khaccount;
    suspend;
  end
end^
SET TERM ; ^
