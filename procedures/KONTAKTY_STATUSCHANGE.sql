--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KONTAKTY_STATUSCHANGE(
      KONTAKT integer,
      PRZECZYTANO smallint)
   as
BEGIN
  if(:kontakt is not null) then begin
    if(:przeczytano = 1) then
      update KONTAKTY set przeczytano = 0 where ref=:kontakt and (przeczytano = 1 or przeczytano is null);
    else if(:przeczytano = 0) then
      update KONTAKTY set przeczytano = 1 where ref=:kontakt and (przeczytano = 0 or przeczytano is null);
  end
END^
SET TERM ; ^
