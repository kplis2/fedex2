--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PRSHORTAGE_AMOUNT_CALC(
      AMOUNT numeric(14,4),
      PRSHORTAGETYPE varchar(20) CHARACTER SET UTF8                           ,
      PRSCHEDGUIDESPOS integer,
      PRSCHEDOPER integer)
  returns (
      AMOUNTSHORTAGES numeric(14,4))
   as
declare variable reportedfactor numeric(14,4);
declare variable tmpreportedfactor numeric(14,4);
declare variable knorm numeric(14,4);
declare variable backasresourse smallint;
declare variable noamountinfluence smallint;
begin
  select psht.backasresourse, psht.noamountinfluence
    from prshortagestypes psht
    where psht.symbol = :prshortagetype
    into :backasresourse, :noamountinfluence;

  if (coalesce(:noamountinfluence,0) = 1) then
    amountshortages = 0;
  else if (:backasresourse = 0) then
    amountshortages = :amount;
  else begin
    if (:reportedfactor is null) then
    begin
      -- wyliczenie reported factor od bieżacej operacji do ostatniej
      -- wazne !!! pomijam repotedfactor biezacej operacji
      reportedfactor = 1;
      for
        select min(o.reportedfactor)
          from prsched_calc_next_oper(:prschedoper,null,0)
            join prschedopers o on (o.ref = RPRSCHEDOPER)
          group by level_out, o.reportedfactor
          into :tmpreportedfactor
      do begin
        if (coalesce(tmpreportedfactor,0) <> 0) then
          reportedfactor = :reportedfactor * :tmpreportedfactor;
        tmpreportedfactor = null;
      end
    end
    select p.kamountnorm
      from prschedguidespos p
      where p.ref = :prschedguidespos
      into :knorm;

    amountshortages = (coalesce(:amount,0) * :knorm * :reportedfactor);
  end
  amountshortages = coalesce(:amountshortages,0);
end^
SET TERM ; ^
