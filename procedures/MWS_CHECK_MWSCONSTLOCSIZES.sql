--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_CHECK_MWSCONSTLOCSIZES(
      MWSCONSTLOC integer,
      MODE smallint)
   as
declare variable mwsstandlevel integer;
declare variable segtype integer;
declare variable minn integer;
declare variable maxn integer;
declare variable free integer;
declare variable palloc integer;
begin
 exit; -- XXXX KBI bo nie chcemu aby sprawdza nam rozmiarów palety i deaktywowal lokacji jak za duza
  if (mode is null or mode not in (0,1)) then
    exit;
  select c.mwsstandlevel, coalesce(t.segtype,0)
    from mwsconstlocs c
      left join mwsstands s on (s.ref = c.mwsstand)
      left join mwsstandtypes t on (t.ref = s.mwsstandtype)
    where c.ref = :mwsconstloc
    into mwsstandlevel, segtype;
  if (mwsstandlevel is null) then
    exit;
  if (mode = 1) then
  begin
    if (exists (select first 1 ref from mwsconstlocs where act = -1 and mwsstandlevel = :mwsstandlevel)) then
      exit;
    else
    begin
      select min(c.whareanumber), max(c.whareanumber)
        from mwsconstlocs c
        where c.mwsstandlevel = :mwsstandlevel
        into minn, maxn;
      -- najpierw probujemy zdezaktywowac pomiedzy
      select first 1 c.ref
        from mwsconstlocs c
          left join mwsfreemwsconstlocs f on (f.ref = c.ref)
        where c.mwsstandlevel = :mwsstandlevel and (c.whareanumber > :minn or :minn is null)
          and (c.whareanumber < :maxn or :maxn is null) and c.ref <> :mwsconstloc
          and f.ref is not null
        order by c.whareanumber
        into free;
      if (free is null) then
        select first 1 c.ref
          from mwsconstlocs c
            left join mwsfreemwsconstlocs f on (f.ref = c.ref)
          where c.mwsstandlevel = :mwsstandlevel and c.whareanumber in (:minn,:maxn) and c.ref <> :mwsconstloc
            and f.ref is not null
          order by c.whareanumber
          into free;
      if (free is not null) then
        update mwsconstlocs set act = -1 where ref = :free;
    end
  end else if (mode = 0) then
  begin
    select first 1 s.mwspalloc
      from mwsstock s
        left join mwspallocs p on (p.ref = s.mwspalloc)
      where s.mwsconstloc = :mwsconstloc and s.mwspalloc is not null and p.segtype = 2
      into palloc;
    if (palloc is null) then
      update mwsconstlocs set act = 1 where mwsstandlevel = :mwsstandlevel and act = -1;
  end
end^
SET TERM ; ^
