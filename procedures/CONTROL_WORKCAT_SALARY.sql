--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CONTROL_WORKCAT_SALARY(
      WORKCAT integer,
      SALARY numeric(14,2),
      AFTERWORKCAT smallint)
  returns (
      MSG varchar(255) CHARACTER SET UTF8                           )
   as
declare variable frompay numeric(14,2);
declare variable topay numeric(14,2);
begin
--MWr: Personel - kontrola okreslania kat. zaszeregownia na umowie/aneksie

  msg = '';
--jezeli nie okreslono kategorii kontrola niepotrzebna
  if (coalesce(workcat,0) = 0) then
    exit;
--afterworkcat = 1 = controla nastapila po wyjsciu z pola kategor. zaszer.
--jezeli kat. jest okreslona i placa = 0, to prawdopodobnie nie zdazono jej ustalic
  if (afterworkcat = 1 and coalesce(SALARY,0) = 0) then
    exit;
--okreslenie zakresu wynagrodzenia dla wybranej kategorii
  select frompay, topay from eworkcatpos
    where ref = :workcat
    into: frompay, :topay;
--wylacz kontrole jezeli zakres wynagrodzenia = (0,0)
  if (coalesce(:frompay,0) = 0 and coalesce(:topay,0) = 0) then
    exit;
--kontrola zakresu stawki zasadniczej
  if ( frompay <= salary and salary <= topay ) then
    exit;
  else if (afterworkcat = 1) then
    msg = 'Wybrana kategoria zaszeregowania nie obejmuje swoim zakresem (' || frompay || '÷' || topay ||') podanej stawki wynagrodzenia.';
  else
    msg = 'Podana wartość stawki wynagrodzenia nie odpowiada wybranej kategorii zaszeregowania.';

  suspend;
end^
SET TERM ; ^
