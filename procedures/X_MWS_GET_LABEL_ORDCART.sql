--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_GET_LABEL_ORDCART(
      MWSORDREF INTEGER_ID,
      WH DEFMAGAZ_ID)
  returns (
      LABEL STRING40,
      MSG STRING80,
      STATUS SMALLINT_ID)
   as
declare variable mwsordtype integer_id;
declare variable multi smallint_id;
begin
  -- procedura okrela labelki w oknie podawania koszyków/wózków na HH
  -- Koszyki w przypadku multipikingu
  -- obecnie mulipking jest stosowany dla wydania z kardexu (zlecenie WTK)
  status = 1;
  msg = '';
  label = '';

  select o.mwsordtype, o.multi
    from mwsords o
    where o.ref = :mwsordref
  into :mwsordtype, :multi;

  if (mwsordtype is null) then begin
    status = 0;
    msg = 'Nie znaleziono zlecenia o numerze REF = '||:mwsordref;
  end

  -- Zlecenie z kardeksu raczej zawsze bdzeimy traktować jako multi czyli poobieramy koszyki
  if (mwsordtype = 4) then begin
    status = 1;
    label= 'Koszyk';
  -- pozostale zlecenia to wózek
  end else begin
    status = 2;
    label= 'Wózek';
  end
end^
SET TERM ; ^
