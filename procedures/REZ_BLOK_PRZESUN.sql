--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE REZ_BLOK_PRZESUN(
      MAGAZYN char(3) CHARACTER SET UTF8                           ,
      KTM varchar(40) CHARACTER SET UTF8                           ,
      WERSJA integer,
      CENA numeric(14,4),
      DOSTAWA integer,
      ILOSC integer)
   as
declare variable iltoprzes numeric(14,4);
declare variable ilfreecen numeric(14,4);
declare variable ilfreeil numeric(14,4);
declare variable ilfree numeric(14,4);
declare variable typ char(1);
declare variable ilzab numeric(14,4);
declare variable il numeric(14,4);
declare variable refpoz integer;
declare variable cnt integer;
declare variable zam integer;
begin
 /*obliczenie ilosci koniecznej do wycofania na dana dostawe*/
 if(:dostawa > 0) then begin
   ilfreecen = 0;
   select sum(ILOSC-ZABLOKOW) from STANYCEN where MAGAZYN=:MAGAZYN and KTM=:KTM AND WERSJA=:WERSJA AND CENA=:CENA AND DOSTAWA=:DOSTAWA into :ilfreecen;
   if(:ilfreecen is null)then ilfreecen = 0;
   if(:ilfreecen < :ilosc) then begin
      ilfreeil = :ilosc - :ilfreecen;
      /*tu napisać przerobienie zadanej iloci z poszczególnych STANYREZ */
      ilfree = 0;
      for select ZAMOWIENIE, POZZAM, ILOSC
      from STANYREZ where MAGAZYN=:MAGAZYN AND KTM=:KTM AND WERSJA=:WERSJA
        AND CENA=:CENA And dostawa=:dostawa AND STATUS='B' and ZREAL=0
      order by PRIORYTET DESC, DATA DESC
      into :zam, :refpoz, :ilzab
      do begin
        if(:ilfreeil > 0) then begin
          il = :ilfreeil;
          if(:il > :ilzab) then il = :ilzab;
          cnt = null;
          select count(*) from STANYREZ where ZAMOWIENIE = :zam and POZZAM=:refpoz and STATUS='Z' and ZREAL=0 into :cnt;
          if(:cnt is null) then cnt = 0;
          if(:cnt > 0) then
            update STANYREZ set ILOSC=ILOSC+:il where ZAMOWIENIE = :zam and POZZAM=:refpoz and STATUS='Z' and ZREAL=0;
          else
            insert into STANYREZ(POZZAM,STATUS,ZREAL,DOKUMMAG,MAGAZYN,KTM,WERSJA,ZAMOWIENIE,DATA,ILOSC,CENA,DOSTAWA,DATABL,ONSTCEN,PRIORYTET)
              select POZZAM,'Z',0,NULL,MAGAZYN,KTM,WERSJA,ZAMOWIENIE,DATA,:il,CENA,DOSTAWA,DATABL,ONSTCEN,PRIORYTET from STANYREZ where POZZAM=:refpoz AND STATUS='B' AND ZREAL = 0;
          if(:il < :ilzab) then
            update STANYREZ set ILOSC = ILOSC-:il where ZAMOWIENIE = :zam and POZZAM=:refpoz and STATUS='B' and ZREAL=0;
          else
            delete from STANYREZ where ZAMOWIENIE = :zam and POZZAM=:refpoz and STATUS='B' and ZREAL=0;
          ilfreeil = :ilfreeil - :il;
          ilfree = :ilfree + :il;
        end
      end
      ilosc = :ilosc - :ilfree;
   end
 end
 /*obliczenie ilosci koniecznej do wycofania na dana cene*/
 if(:cena > 0 and :ilosc > 0) then begin
   ilfreecen = 0;
   select sum(ILOSC-ZABLOKOW) from STANYCEN where MAGAZYN=:MAGAZYN and KTM=:KTM AND WERSJA=:WERSJA AND CENA=:CENA
     into :ilfreecen;
   if(:ilfreecen is null)then ilfreecen = 0;
   /*podliczenie rezerwacji na ceny ktore nie weszly na stany cenowe*/
   ilzab = null;
   select sum(ILOSC) from STANYREZ where MAGAZYN=:MAGAZYN AND KTM=:KTM AND WERSJA=:WERSJA AND CENA=:CENA and STATUS='B' AND ZREAL=0 AND ONSTCEN=0
     into :ilzab;
   if(:ilzab is null) then ilzab = 0;
   ilfreecen = :ilfreecen - :ilzab;
   if(:ilfreecen < 0) then ilfreecen = 0;
   if(:ilfreecen < :ilosc) then begin
      ilfreeil = :ilosc - :ilfreecen;
      ilfree = 0;
      /*tu napisać przerobienie zadanej iloci z poszczególnych STANYREZ */
      for select ZAMOWIENIE, POZZAM, ILOSC
      from STANYREZ
      where MAGAZYN=:MAGAZYN AND KTM=:KTM AND WERSJA=:WERSJA AND CENA=:CENA
        AND STATUS='B' and ZREAL=0
      order by PRIORYTET DESC, DATA DESC
      into :zam, :refpoz, :ilzab
      do begin
        if(:ilfreeil > 0) then begin
          il = :ilfreeil;
          if(:il > :ilzab) then il = :ilzab;
          cnt = null;
          select count(*) from STANYREZ where ZAMOWIENIE = :zam and  POZZAM=:refpoz and STATUS='Z' and ZREAL=0 into :cnt;
          if(:cnt is null) then cnt = 0;
          if(:cnt > 0) then
            update STANYREZ set ILOSC=ILOSC+:il where ZAMOWIENIE = :zam and  POZZAM=:refpoz and STATUS='Z' and ZREAL=0;
          else
            insert into STANYREZ(POZZAM,STATUS,ZREAL,DOKUMMAG,MAGAZYN,KTM,WERSJA,ZAMOWIENIE,DATA,ILOSC,CENA,DOSTAWA,DATABL,ONSTCEN,PRIORYTET)
              select POZZAM,'Z',0,NULL,MAGAZYN,KTM,WERSJA,ZAMOWIENIE,DATA,:il,CENA,DOSTAWA,DATABL,ONSTCEN,PRIORYTET from STANYREZ where POZZAM=:refpoz AND STATUS='B' AND ZREAL = 0;
          if(:il < :ilzab) then
            update STANYREZ set ILOSC = ILOSC-:il where ZAMOWIENIE = :zam and  POZZAM=:refpoz and STATUS='B' and ZREAL=0;
          else
            delete from STANYREZ where ZAMOWIENIE = :zam and  POZZAM=:refpoz and STATUS='B' and ZREAL=0;
          ilfreeil = :ilfreeil - :il;
          ilfree = :ilfree + :il;
        end
      end
      ilosc = :ilosc - :ilfree;
   end

 end
 if(:ilosc > 0) then begin
   /* kontrola ze stanem ilosciowym */
   ilfreeil = null;
   select ILOSC-ZABLOKOW from STANYIL where MAGAZYN=:MAGAZYN AND KTM=:KTM AND WERSJA=:WERSJA into :ilfreeil;
   if(:ilfreeil is null) then ilfreeil = 0;
   if(:ilfreeil < :ilosc) then begin
      ilfreeil = :ilosc - :ilfreeil;
      ilfree = 0;
      /*tu napisać przerobienie zadanej iloci z poszczególnych STANYREZ */
      for select ZAMOWIENIE, POZZAM,ILOSC
      from STANYREZ
      where MAGAZYN=:MAGAZYN AND KTM=:KTM AND WERSJA=:WERSJA
         AND STATUS='B' and ZREAL=0
      order by PRIORYTET DESC, DATA DESC
      into :zam, :refpoz, :ilzab
      do begin
        if(:ilfreeil > 0) then begin
          il = :ilfreeil;
          if(:il > :ilzab) then il = :ilzab;
          cnt = null;
          select count(*) from STANYREZ where ZAMOWIENIE = :zam and  POZZAM=:refpoz and STATUS='Z' and ZREAL=0 into :cnt;
          if(:cnt is null) then cnt = 0;
          if(:cnt > 0) then
            update STANYREZ set ILOSC=ILOSC+:il where ZAMOWIENIE = :zam and  POZZAM=:refpoz and STATUS='Z' and ZREAL=0;
          else
            insert into STANYREZ(POZZAM,STATUS,ZREAL,DOKUMMAG,MAGAZYN,KTM,WERSJA,ZAMOWIENIE,DATA,ILOSC,CENA,DOSTAWA,DATABL,ONSTCEN,PRIORYTET)
              select POZZAM,'Z',0,NULL,MAGAZYN,KTM,WERSJA,ZAMOWIENIE,DATA,:il,CENA,DOSTAWA,DATABL,ONSTCEN,PRIORYTET from STANYREZ where POZZAM=:refpoz AND STATUS='B' AND ZREAL = 0;
          if(:il < :ilzab) then
            update STANYREZ set ILOSC = ILOSC-:il where ZAMOWIENIE = :zam and  POZZAM=:refpoz and STATUS='B' and ZREAL=0;
          else
            delete from STANYREZ where ZAMOWIENIE = :zam and  POZZAM=:refpoz and STATUS='B' and ZREAL=0;
          ilfreeil = :ilfreeil - :il;
          ilfree = :ilfree + :il;
        end
      end
      ilosc = :ilosc - :ilfree;
   end
 end
end^
SET TERM ; ^
