--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE EDE_INPOST_PACKSTATUS_REQUEST(
      OTABLE varchar(20) CHARACTER SET UTF8                           ,
      OREF integer)
  returns (
      ID integer,
      PARENT integer,
      NAME varchar(255) CHARACTER SET UTF8                           ,
      PARAMS varchar(255) CHARACTER SET UTF8                           ,
      VAL varchar(255) CHARACTER SET UTF8                           )
   as
declare variable shippingdoc integer_id;
declare variable shippingdocno string40;
declare variable grandparent integer_id;
begin
  select l.ref--, substring(l.symbolsped from 1 for 20)
    from listywysd l
    where l.ref = :oref
  into :shippingdoc; --, :shippingdocno;

  --sprawdzanie czy istieje dokument spedycyjny
  if(shippingdoc is null) then exception ede_ups_listywysd_brak;

  val = null;
  parent = null;
  params = null;
  id = 0;
  name = 'GetPackStatus';
  suspend;

  grandparent = :id;

  val = 'TEST'; --jesli chcesz testowac zmien na TEST inaczej PRODUCTION
  name = 'ServiceUrlType';
  id = id + 1;
  parent = :grandparent;
  suspend;
  for
    select lwdr.symbolsped from LISTYWYSDROZ_OPK lwdr where :otable = 'LISTYWYSDROZ_OPK' and lwdr.ref = :oref
    union
    select lwdr.symbolsped from LISTYWYSDROZ_OPK lwdr where :otable = 'LISTYWYSD' and lwdr.listwysd = :oref
      into :shippingdocno
  do begin
    val = :shippingdocno;
    name = 'packcode';
    params = null;
    parent = grandparent;
    id = id + 1;
    suspend;
  end
end^
SET TERM ; ^
