--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE FXDASSETCA_ACCOUNT_ACCEPT(
      PARAM varchar(1) CHARACTER SET UTF8                           ,
      REF integer)
   as
declare variable srodek integer;
declare variable amperiod integer;
declare variable sum_share numeric(14,2);
declare variable share numeric(14,2);
declare variable typeamort smallint;
begin
  if (param = 1) then begin
    select fxc.fxdasset, fxc.amperiod, fxc.share, fxc.typeamortization
      from fxdassetca fxc
      where fxc.ref = :ref
    into :srodek, :amperiod, :share, :typeamort;

    --sprawdzenie sumy udzialow
    select sum(coalesce(fxc.share,0))
      from fxdassetca fxc
      where fxc.fxdasset = coalesce(:srodek,0)
        and fxc.amperiod = coalesce(:amperiod,0)
        and fxc.typeamortization = coalesce(:typeamort,0)
    into :sum_share;

    sum_share = coalesce(sum_share, 0);

    if (coalesce(:sum_share,0) <> 100) then begin
      exception universal 'Suma udzialów różna od 100% w danym okresie - akceptacja niemożliwa';
      exit;
    end else begin
      --odakceptowanie aktywnego okresu
      update fxdassetca fxc
        set fxc.status = 0
        where fxc.fxdasset = coalesce(:srodek,0)
          and coalesce(fxc.status,0) = 1
          and fxc.typeamortization = coalesce(:typeamort,0);

      --akceptacja nowego okresu
      update fxdassetca fxc
        set fxc.status = 1
        where fxc.fxdasset = coalesce(:srodek,0)
          and fxc.amperiod = coalesce(:amperiod,0)
          and fxc.typeamortization = coalesce(:typeamort,0);
    end
  end else if (param = 2) then begin
    --anulowanie
    select fxc.fxdasset, fxc.amperiod, fxc.typeamortization
      from fxdassetca fxc
      where fxc.ref = :ref
    into :srodek, :amperiod, :typeamort;

    update fxdassetca fxc
      set fxc.status = 0
      where fxc.fxdasset = coalesce(:srodek,0)
        and fxc.amperiod = coalesce(:amperiod,0)
        and fxc.typeamortization = coalesce(:typeamort,0);
  end else if (param = 3) then begin
    --przeniesienie do archiwum
    select fxc.fxdasset, fxc.amperiod, fxc.typeamortization
      from fxdassetca fxc
      where fxc.ref = :ref
    into :srodek, :amperiod, :typeamort;

    update fxdassetca fxc
      set fxc.status = 2
      where fxc.fxdasset = coalesce(:srodek,0)
        and fxc.amperiod = coalesce(:amperiod,0)
        and fxc.typeamortization = coalesce(:typeamort,0);
  end else
    exception universal 'Nieprawidlowy parametr wejsciowy procedury FXDASSETCA_ACCOUNT_ACCEPT';
end^
SET TERM ; ^
