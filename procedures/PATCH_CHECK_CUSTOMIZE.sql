--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE PATCH_CHECK_CUSTOMIZE(
      SECTION STRING,
      ITEMS_REL STRING1024)
  returns (
      CUSTOMIZED integer)
   as
declare variable ITEMS_KLI STRING1024;
declare variable ITEM STRING1024;
begin
  if (not exists (select 1 from s_appini where section = :section)) then --nie ma sekcji u klienta
  begin
    customized = 0;
    suspend;
    exit;
  end

  select max(customized) from s_appini where section = :section into :customized; --oznaczenie customize
  if (customized = 1) then
  begin
    suspend;
    exit;
  end
  
  if (exists (select 1 from s_appini where section = :section and prefix = 'A')) then -- istnieje sekcja adminowa
  begin
    customized = 1;
    suspend;
    exit;
  end
  
  if (exists (select 1 from s_appini where section = :section and upper(val) like 'X%')) then --podpito akcje X-owa, pole X-owe, itp
  begin
    customized = 1;
    suspend;
    exit;
  end

  select val from s_appini where section = :section and ident = 'Items' and prefix is null into :items_kli; --badanie pola items: czy u klienta dodano pola / elementy menu

  if (coalesce(items_kli,'') <> '') then
  begin
    if (coalesce(items_rel,'') <> '') then
    begin
       while (position(';' in items_kli)>0) do
       begin
         item = substring(items_kli from 1 for position(';' in items_kli));
         items_kli = substring(items_kli from position(';' in items_kli)+1 for 255);
         if (items_rel not like '%'||item||'%') then
         begin
           customized = 1;
           suspend;
           exit;
         end
       end
       if (items_rel not like '%'||items_kli||'%') then
         begin
           customized = 1;
           suspend;
           exit;
         end
    end
    else
    begin
       customized = 1;
       suspend;
       exit;
    end
  end
  customized = 0;
  suspend;
end^
SET TERM ; ^
