--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE RPT_WORKREFERENCE(
      EMPLOYEE integer,
      FROMCONTRACT integer,
      INFOTYPE integer,
      TOCONTRACT integer = 0,
      ADDPARAMS varchar(100) CHARACTER SET UTF8                            = '',
      AFROMDATE date = null)
  returns (
      FROMDATE date,
      TODATE date,
      DESCRIPT varchar(255) CHARACTER SET UTF8                           ,
      DESCRIPT2 varchar(40) CHARACTER SET UTF8                           ,
      ADDINFO integer,
      USEDVACDAYS numeric(14,2),
      USEDVACHOURS numeric(14,2),
      EQUIVALDAYS numeric(14,2),
      EQUIVALHOURS numeric(14,2),
      REQUESTVACDAYS numeric(14,2),
      SIGNATURE varchar(80) CHARACTER SET UTF8                           ,
      COURTCASE varchar(60) CHARACTER SET UTF8                           ,
      BDEDUCTED numeric(14,2),
      PAGENO integer,
      PARTINFO varchar(50) CHARACTER SET UTF8                           )
   as
declare variable DIMNUM smallint;
declare variable DIMDEN smallint;
declare variable FROMDATE_TMP date;
declare variable TODATE_TMP date;
declare variable FROMDATE_TMP2 date;
declare variable TODATE_TMP2 date;
declare variable CFROMDATE date;
declare variable CTODATE date;
declare variable WORKDIM numeric(14,2);
declare variable WORKDIM_TMP numeric(14,2);
declare variable WORKPOST integer;
declare variable WORKPOST_TMP integer;
declare variable DESCRIPT_TMP varchar(255);
declare variable DESCRIPT_TMP2 varchar(80);
declare variable ADDINFO_TMP integer;
declare variable ADDINFO_TMP2 integer;
declare variable BACKVACDAYS numeric(14,2);
declare variable BACKVACHOURS numeric(14,2);
declare variable RECORDVACDAYS numeric(14,2);
declare variable RECORDVACHOURS numeric(14,2);
declare variable OVERVACDAYS numeric(14,2);
declare variable OVERVACHOURS numeric(14,2);
declare variable RESTVACDAYS numeric(14,2);
declare variable RESTVACHOURS numeric(14,2);
declare variable CPERDEDUCT varchar(6);
declare variable INITDEDUCTION numeric(14,2);
declare variable STOPWORKREASON smallint;
declare variable PERSON integer;
declare variable PAGE_COUNTER integer;
declare variable PRTEMPTYTEXT varchar(30);
declare variable VYEAR integer;
declare variable CREFS varchar(100);
declare variable CTXTODATE varchar(10);
declare variable WORKMINUTES numeric(14,2);
declare variable VVAL numeric(14,2);
declare variable I smallint = 0;
declare variable SHOWASWEEK smallint = 0;
declare variable J smallint;
declare variable REQUESTVACHOURS numeric(14,2);
declare variable COUNTER integer;
declare variable WEEKSCOUNT smallint;
begin
--MWr: Personel - Porcedura generuje informacje do wydruku Swiadectwa Pracy
/*
  InfoType:
          040 - parsowanie parametru ADDPARAMS z info. o wykorzystanym urlopie
          050 - formatowanie opisu dotyczacego liczby dni
          051 - formatowanie opisu dotyczacego liczby tygodni
          100 - okres zatrudnienia
          101 - okresy zatrudnienia poszczegolnych umow
          102 - okresy zatrudnienia pogrupowane wedlug wymiaru zatrudnienia
          300 - okresy zatrudnienia pogrupowane wedlug zajmowanego stanowiska
          400 - powod ustania stsosunku pracy
          601 - wykorzystany url wypocz., na zadanie, wyplata ekwiwalentu za dni
          602 - wykorzystanie urlopu bezplatnego
          603/0 - wykorzystanie urlopu ojcowskiego / informacja naglowkowa
          604/0 - wykorzystanie urlopu rodzicielskiego / informacja naglowkowa
          605 - wykorzystanie urlopu wychowawczego
          606 - okresy ochrony stosunku pracy
          607 - wykorzystane zwolnienie od pracy przewidziane w art. 188 KP
          608 - liczba dni, za ktore prac. otrzymal wynagr. zgodnie z art. 92
          609 - dni, za ktore pracownik nie zachowal prawa do wynagrodzenia (przepis obowiazujacy w 2003 roku)
          610 - odbywanie czynnej sluzby wojskowej w okresie zatrudnienia
          611 - praca w szczegolnych warunkach
          613 - okresy nieskadkowe
          701 - informacje o zajeciu wynagordzenia
*/
  ctxtodate = '';
  fromcontract = coalesce(fromcontract, 0);
  tocontract = coalesce(tocontract, 0);

  if (infotype >= 100) then
  begin
--< Okres zatrudnienia >========================================================

    select min(fromdate) from emplcontracts
      where empltype = 1 and employee = :employee
        and (ref = :fromcontract or :fromcontract = 0)
      into :fromdate;

    select max(case when ref = :tocontract or :tocontract = 0 then fromdate else null end)
         , max(fromdate)
     from emplcontracts
      where empltype = 1 and employee = :employee
      into :fromdate_tmp   -- 'data od' umowy z uwzglednieniem parmetru TOCONTRACT
         , :fromdate_tmp2; -- 'data od' umowy najmlodszej; wyznaczamy do sprawdzenia, czy na pewno musimy ustawic zmienna kontekstowa

    select enddate from emplcontracts
      where empltype = 1 and employee = :employee
       and fromdate = :fromdate_tmp
      into :todate;

    if (fromcontract > 0 or tocontract > 0) then
    begin
      select ';' || list(ref,';') || ';' from emplcontracts c
        where empltype = 1 and employee = :employee
          and fromdate >= :fromdate
          and fromdate <= :fromdate_tmp
      group by employee
      into :crefs;   --lista REFow umow z zadnego zakresu
    end else
      crefs = '';

    if (todate is null) then
      todate = coalesce(todate, current_date);
    else if (fromdate_tmp is distinct from fromdate_tmp2) then
      ctxtodate = cast(todate as varchar(10));

    cfromdate = fromdate;
    ctodate = todate;

    if (infotype = 100) then
      suspend;
  end

  if (infotype = 101) then
  begin
--< Zwraca okresy zatrudnienia poszczegolnych umow >============================
  --AddInfo = 1: wiersz nr 1 informacji -> zatrudnienie od-do, rodzaj umowy
  --        = 2: wiersz nr 2 informacji -> powod zakonczenia umowy
    for
      select c.fromdate, c.enddate, c.todate, r.reason, lower(t.tname)
        from emplcontracts c
          join econtrtypes t on (c.econtrtype = t.contrtype and c.empltype = 1)
          left join eterminations r on (r.ref = c.etermination)
        where c.employee = :employee
          and (:crefs = '' or :crefs containing ';'||c.ref||';')
        order by c.fromdate
        into :fromdate, :todate, :todate_tmp, :descript_tmp, :descript_tmp2
    do begin
      addinfo = 1;
      todate = coalesce(todate,current_date);
      if (descript_tmp2 like 'umowa%') then
        descript = substring(descript_tmp2 from  7 for coalesce(char_length(descript_tmp2),0)); -- [DG] XXX ZG119346
      else
        descript = descript_tmp2;
      suspend;
      addinfo = 2;
      if (fromdate = fromdate_tmp) then --fromdate_tmp = max(emplcontracts.fromdate)
        descript = 'jak w punkcie 4';
      else
        descript = coalesce(descript_tmp,'z upływem okresu, na który była zawarta');
      suspend;
    end
  end
  else if (infotype = 102) then
  begin
--< Zwraca okresy zatrudnienia pogrupowane wg wymiaru zatrudnienia >============

    workdim = null;
    for
      select fromdate, todate, workdim, dimnum, dimden
        from employment
        where employee = :employee
         and (:crefs = '' or :crefs containing ';'||emplcontract||';')
        order by fromdate
        into :fromdate_tmp, :todate_tmp, :workdim_tmp, :dimnum, :dimden
    do begin
      if (workdim_tmp <> coalesce(workdim,workdim_tmp)) then
      begin
        if (workdim = 1.00) then
          descript = 'pełny etat';
        suspend;
      end
      if (workdim is null or workdim_tmp <> workdim) then
      begin
        fromdate = fromdate_tmp;
        workdim = workdim_tmp;
      end
      todate = todate_tmp;
      descript = dimnum || '/' || dimden || ' etatu';
    end

    if (workdim is not null) then
    begin
      todate = coalesce(todate, ctodate);
      if (workdim = 1.00) then
        descript = 'pełny etat';
      suspend;
    end
  end
  else if (infotype = 300) then
  begin
--< Zwraca okresy zatrudnienia pogrupowane wedlug stanowiska >==================

    workpost = null;
    for
      select e.fromdate, e.todate, e.workpost, coalesce(w.fullname,w.workpost)
        from employment e
          left join edictworkposts w on (w.ref = e.workpost)
        where e.employee = :employee
          and (:crefs = '' or :crefs containing ';'||e.emplcontract||';')
        order by e.fromdate
        into :fromdate_tmp, :todate_tmp, :workpost_tmp, :descript_tmp
    do begin
      if (workpost_tmp <> coalesce(workpost,workpost_tmp)) then
        suspend;

      if (workpost is null or workpost_tmp <> workpost) then
      begin
        fromdate = fromdate_tmp;
        workpost = workpost_tmp;
        descript = descript_tmp;
      end
      todate = todate_tmp;
    end

    if (workpost is not null) then
    begin
      todate = coalesce(todate, ctodate);
      suspend;
    end
  end
  else if (infotype = 400) then
  begin
--< Powod ustania stosunku pracy >==============================================

    select first 1 e.reason, e.stopworkreason
      from emplcontracts c
      left join eterminations e on (e.ref = c.etermination)
      where c.empltype = 1
        and c.employee = :employee
        and (:crefs = '' or :crefs containing ';'||c.ref||';')
        and e.stopworkreason > 0
     order by c.contrdate desc
      into :descript_tmp, :stopworkreason;

    counter = 1;
    while (counter <= 2)
    do begin
      descript = 'nie dotyczy';
      if (counter = 1) then begin
        descript2 = 'a) rozwiązania:';
        if (stopworkreason in (1,2)) then descript = descript_tmp;
      end else if (counter = 2) then begin
        descript2 = 'b) wygaśnięcia:';
        if (stopworkreason = 3) then descript = descript_tmp;
      end

      suspend;
      counter = counter + 1;
    end
  end
  else if (infotype = 601) then
  begin
--< Wykorzystany urlop wypoczynkowy, na zadanie (w tym ekwiwalent) >============
    vyear = extract(year from :ctodate);

    --url. zalegly (BackVac), pozostaly (RestVac), urlopy zarejestrowane (RecordVac)
    select prevlimitd, restlimitd, limitconsd,
           prevlimit/60.00, restlimit/60.00, limitcons/60.00
      from evaclimits
      where employee = :employee
        and vyear = :vyear
      into :backvacdays, :restvacdays, :recordvacdays,
           :backvachours, :restvachours, :recordvachours;

    backvacdays = coalesce(backvacdays,0);
    backvachours = coalesce(backvachours,0);

    if (ctxtodate <> '') then
    begin
    /*Proba okreslenia limtow urlopowych do danej umowy. Wyliczenie urlopu zaleglego
      oparte na EVACLIMITS_BIU_RESTLIMIT, przeliczenie na dni na podstawie EVACLIMITS_BIU_FILL_D */

      rdb$set_context('USER_TRANSACTION', 'RPT_WORKREFERENCE_TODATE', :ctxtodate);

      select limitconsm, (coalesce(vlimitm,0) + :backvachours * 60.00
               + coalesce(addvlimitm,0) - limitconsm /*+ supplementallimit */
               + coalesce(disablelimitm,0))
        from e_calculate_vaclimit(:employee, :vyear)
        into :recordvachours, :restvachours;

      if (vyear >= 2012) then
      begin
        select first 1 dayworkdim/60
          from e_get_employmentchanges(:employee,null,:ctodate,';DAYWORKDIM;')
          order by fromdate desc
          into :workminutes;
      end
  
      if (coalesce(workminutes,0) = 0 or vyear < 2012) then
        workminutes = 60 * 8;
  
      restvacdays = restvachours / workminutes;
      recordvacdays = recordvachours / workminutes;

      recordvachours = recordvachours / 60.00;
      restvachours = restvachours / 60.00;

      rdb$set_context('USER_TRANSACTION', 'RPT_WORKREFERENCE_TODATE', '');
    end

    restvacdays = coalesce(restvacdays,0);
    restvachours = coalesce(restvachours,0);
    recordvacdays = coalesce(recordvacdays,0);
    recordvachours = coalesce(recordvachours,0);

    --urlop przyznany ponad limit(OverVac)
    if (restvachours < 0) then
    begin
      overvacdays = abs(restvacdays);
      overvachours = abs(restvachours);
    end else
    begin
      overvacdays = 0;
      overvachours = 0;
    end

    if (extract(year from (:ctodate)) || '-01-01' > cfromdate) then
      cfromdate = extract(year from (:ctodate)) || '-01-01';

    --urlop na zadanie (RequestVac); RequestVac <= RecordVac
    select sum(workdays), sum(worksecs/3600.00) from eabsences
      where employee = :employee and ecolumn = 230
        and fromdate >= :cfromdate and todate <= :ctodate
        and coalesce(correction,0) in (0,2)
      into :requestvacdays, :requestvachours;

    requestvacdays = coalesce(requestvacdays,0);
    requestvachours = coalesce(requestvachours,0);

    --urlop wykorzystany jaki pojawi sie na wydruku (UsedVac)
    usedvacdays = recordvacdays - backvacdays + restvacdays + overvacdays;
    usedvachours = recordvachours - backvachours + restvachours + overvachours;

    if (usedvachours < 0) then
    begin
      usedvachours = 0;
      usedvacdays = 0;
    end

    --liczba dni/godz urlopu, za ktore nalezy sie ekwiwalent (Equival) (BS32663,BS59552)
    recordvacdays = recordvacdays - requestvacdays;
    recordvachours = recordvachours - requestvachours;
    if (recordvachours < backvachours) then
    begin
      equivaldays = restvacdays - backvacdays + recordvacdays;
      equivalhours = restvachours - backvachours + recordvachours;
    end else
    begin
      equivaldays = restvacdays;
      equivalhours = restvachours;
    end

    if (equivalhours < 0) then
    begin
      equivaldays = 0;
      equivalhours = 0;
    end

  --Nadpisanie wyliczonych limitow przez parametry, jesli zostaly zadane (PR54992)
    execute procedure efunc_get_numbers_from_string(addparams)
      returning_values descript_tmp;
    if (descript_tmp <> '') then
    begin
      select coalesce(usedvacdays, :usedvacdays)
         , coalesce(usedvachours, :usedvachours)
         , coalesce(equivaldays, :equivaldays)
         , coalesce(equivalhours, :equivalhours)
         , coalesce(requestvacdays, :requestvacdays)
        from rpt_workreference(null,null,'040',null,:addparams)
        into :usedvacdays, :usedvachours, :equivaldays, :equivalhours, :requestvacdays;
    end

    suspend;
  end if (infotype in (6030, 6040)) then
  begin
/*-<Infromacja naglowkowa do punktow 6.3 i 6.4. Obsluga informacji o tygodniach oraz obejscie
    problemu braku mozliwosci zmiany na wydruku rozmiaru ramki gdy wykorzystujemy zmianne agregujace */
    select sum(addinfo) from rpt_workreference(:employee,:fromcontract,:infotype/10,:tocontract,:addparams,:afromdate)
      into :addinfo;

    if (addinfo > 0) then
      select descript from rpt_workreference(:addinfo, null, '051') into :descript;
    else
      descript = '';

    suspend;
  end
  else if (infotype in (602, 603, 604, 605, 607, 608, 610, 613)) then
  begin
/*-<(602) wykorzystany urlop bezplatny
    (603) wykorzystany urlop ojcowski
    (604) wykorzystany urlop rodzicielski
    (605) wykorzystany urlopu wychowawczy
    (607) zwolnienie wg art.188 KP w roku kalend.,w którym ustal stosunek pracy
    (608) dni oplacone wg art.92 KP w roku kalend., w ktorym ustal stosunek pracy (chorobowe platne przez pracodawce)
    (610) odbywanie czynnej sluzby wojskowej w okresie zatrudnienia
    (613) zwraca okresy nieskladkowe (laczenie okresow) >=====================*/

    i = null; -- jezeli ustawimy wartosc "1", to na wydruku w pozycjach punktow 60-3/4/5 pojawi sie przedrostek 'Czesc..'
    pageno = 0;
    page_counter = 0;
    counter = 0;
    addinfo_tmp = 0;
    fromdate_tmp = null;
    todate_tmp = null;

    if (infotype in (603,604)) then
    begin
      showasweek = 1;

      if (addparams = '1' or (addparams = '2' and afromdate is null)) then  --nie wykazuj urlopow [1] lub nie wykazuj gdy nie wybrano daty [2]
        exit;
      else if (addparams in ('-1','0')) then --wykazuj domslnie
      begin
        if (infotype = 603) then afromdate = dateadd(-2 year to ctodate);
        else afromdate = dateadd(-6 year to ctodate);
      end
    end else
      afromdate = null;

    if (infotype in (607,608)) then --liczone od poczatku roku, w ktorym ustal s. pracy
      cfromdate = extract(year from :ctodate) || '-01-01';

    --ponizszy mechanizm laczy ze soba okresy 'stykajace' sie
    --dla InfoType = 613 dodatkowy podzial ze wzgledu na rodzaj nieobecnosci (PR26329)
    for
      select fromdate, todate,
             case when :infotype <> 613 then '' else case when ecolumn < 160 then 'absencja chorobowa'  --BS50962
                                                else case when ecolumn < 350 then 'zasiłek opiekuńczy'
                                                else 'świadczenie rehabilitacyjne' end end end,
             case when :infotype in (603, 604, 605, 608, 610, 613) then days else iif(ecolumn = 181, -worksecs, workdays) end
        from eabsences
        where employee = :employee
          and fromdate >= :cfromdate
          and (fromdate <= :ctodate or :ctodate is null)
          and (fromdate >= :afromdate or :afromdate is null)
          and coalesce(correction, 0) in (0,2)
          and ((:infotype = 602 and ecolumn = 300 and worksecs > 0)
            or (:infotype = 603 and ecolumn = 290)
            or (:infotype = 604 and ecolumn in (140, 450, 460))
            or (:infotype = 605 and ecolumn = 260)
            or (:infotype = 607 and ecolumn in (180, 181) and worksecs > 0)  --180 i 181 nie powinny wystapic jednoczesnie w danym roku
            or (:infotype = 608 and ecolumn in (40, 50, 60))
            or (:infotype = 610 and ecolumn = 330)
            or (:infotype = 613 and ecolumn in (40, 50, 60, 90, 100, 110, 120, 160, 170, 350, 360, 370)))
        order by fromdate
        into :fromdate, :todate, :descript, :addinfo
    do begin

      if (todate > ctodate ) then
      begin
      --jezli okres nieobecnosci jest poza data zakonczenia stosunku pracy
        todate = ctodate;
        if (infotype in (608, 610, 613)) then
          addinfo = todate - fromdate + 1;   -- liczba dni kalednarzowych
        else
          select wd from ecal_work (:employee, :fromdate, :todate)
            into :addinfo;                   -- liczba dni roboczych
      end

      if (counter = 0) then
      begin
        fromdate_tmp = fromdate;
        todate_tmp = todate;
        addinfo_tmp = addinfo;
        descript_tmp = descript;
      end else
      begin
      --jezli ponizszy warunek nie jest spelniony to nie ma laczenia okresow
        if (counter > 0 and (todate_tmp + 1) >= :fromdate
             and (infotype <> 613 or (infotype = 613 and descript = descript_tmp)))
        then begin
          todate_tmp = todate;
          addinfo_tmp = addinfo + addinfo_tmp;
        end else
        begin
          fromdate_tmp2 = fromdate;
          todate_tmp2 = todate;
          addinfo_tmp2 = addinfo;
          descript_tmp2 = descript;
          fromdate = fromdate_tmp;
          todate = todate_tmp;
          addinfo = addinfo_tmp;
          descript2 = descript_tmp;

          select descript
            from rpt_workreference(:addinfo, null, '05'  || :showasweek)
            into :descript;

        --BS37794:
          page_counter = page_counter + 1;
          if (page_counter = 42) then begin
            page_counter = 0;
            pageno = pageno + 1;
          end
          partinfo = coalesce('Część ' || i || ':', '-');

          suspend;

          i = i + 1;
          fromdate_tmp = fromdate_tmp2;
          todate_tmp = todate_tmp2;
          addinfo_tmp = addinfo_tmp2;
          descript_tmp = descript_tmp2;
         end
       end
       counter = counter +1;
    end --for

    if (counter >= 1 or infotype = 608) then
    begin
      fromdate = fromdate_tmp;
      todate = todate_tmp;
      addinfo = addinfo_tmp;
      descript2 = descript_tmp;

      select descript
        from rpt_workreference(:addinfo, null, '05'||:showasweek)
        into :descript;

      partinfo = coalesce('Część ' || i || ':', '-');
      suspend;
    end
  end
  else if (infotype = 606) then
  begin
/*-< 606 - okres korzystania z ochrony art. 186(8) par.1 pkt.2 >==============*/
   fromdate = null;
   todate = null;
   i = 1;
   for
     select first 2 outstring from parse_lines(:addparams,'$')
       into :descript_tmp
    do begin
      if (descript_tmp > '') then
      begin
        if (i = 1) then fromdate = cast(descript_tmp as date);
        else if (i = 2) then todate = cast(descript_tmp as date);
      end
      i = i + 1;
    end

    if (fromdate is not null or todate is not null) then
    begin
      addinfo = coalesce(todate - fromdate + 1, 0);
      if (addinfo = 0) then
        descript = ' ??? dni';
      else if (addinfo = 1) then
        descript = addinfo || ' dzień';
      else
        descript = addinfo || ' dni';
      suspend;
    end
  end
  else if (infotype = 611) then
  begin
/*-< 611 - praca w szczególnych warunkach >===================================*/

    for
      select fromdate, todate, descript
        from rpt_workreference(:employee, :fromcontract, 300, :tocontract)  --rozne stanowiska
        into :fromdate_tmp2, :todate_tmp2, :descript
    do begin
      todate = null;
      for
        select coalesce(z.specfrom,z.fromdate), z.specto, e.person
          from employees e
            join ezusdata z on (z.person = e.person and z.speccondition is not null)
          where e.ref = :employee
            and z.fromdate <= :todate_tmp2
            and (z.todate >= :fromdate_tmp2 or z.todate is null)
          order by z.fromdate
          into :fromdate_tmp, :todate_tmp, :person
      do begin

        if (todate_tmp is null) then
        begin
        --nalezy okreslic gorna granice okresu waznosci spec. warunkow gdy jej brak
         select first 1 (fromdate - 1) from ezusdata
           where person = :person and fromdate > :fromdate_tmp
           order by fromdate
           into :todate_tmp;
        end

        --zakres dat spec. warunkow musi miescic sie w granicach czasu pracy na danym stanowisku
        if (fromdate_tmp < fromdate_tmp2) then
          fromdate_tmp = fromdate_tmp2;
        if (todate_tmp > todate_tmp2 or todate_tmp is null) then
          todate_tmp = todate_tmp2;

        --badnie ciaglosci dat
        if (todate <> (fromdate_tmp - 1)) then
          suspend;

        if (todate is null or todate <> (fromdate_tmp - 1)) then
          fromdate = fromdate_tmp;
        todate = todate_tmp;
      end

      if (todate is not null) then
        suspend;
    end
  end
  else if (infotype = 609) then
  begin
/*-< dni, za ktore pracownik nie zachowal prawa do wynagrodzenia
    (przepis obowiazujacy w 2003 roku) >======================================*/

    select sum(npdays)
      from eabsences
      where employee = :employee
        and ayear = 2003
      into :addinfo;
    addinfo = coalesce(addinfo, 0);
    suspend;
  end
  else if (infotype = 701) then
  begin
--< Informacja o zajeciach komorniczych >=======================================

    prtemptytext = '---------------';
    if (coalesce(ctxtodate,'') = '') then
    begin
    --informacja o aktualnych zajeciach
      for
        select coalesce(d.signature,''), coalesce(b.bailiffname,''),
               coalesce(d.courtcase,''), coalesce(d.deducted,0)
          from ecolldeductions d
            join ecolumns c on (c.number = d.dkind and c.coltype = 'KOM')
            join employees e on (d.person = e.person and e.ref = :employee)
            left join ebailiff b on (b.ref = d.bailiff)
          where d.actdeduction > 0
            and d.fromdate <= :ctodate
            and (d.todate >= :ctodate or d.todate is null) --BS49784
          order by d.fromdate
          into :signature, :descript, :courtcase, :bdeducted
      do begin
        if (descript = '') then descript = prtemptytext;
        if (signature = '') then signature = prtemptytext;
        if (courtcase = '') then courtcase = prtemptytext;
        suspend;
      end
    end else
    begin
    --CTXTODATE not null: informacja o stanie zajec komorniczych do konca wybranej umowy do

      execute procedure e_func_periodinc(null,0,:ctodate)
        returning_values cperdeduct;

      for
        select coalesce(b.bailiffname,''), coalesce(d.signature,''),
               coalesce(d.courtcase,''), coalesce(d.initdeduction,0),
               sum(coalesce(i.insvalue,0))
          from ecolldeductions d
            join ecolumns c on (c.number = d.dkind and c.coltype = 'KOM')
            join employees e on (d.person = e.person and e.ref = :employee)
            left join ecolldedinstals i on (d.ref = i.colldeduction)
            left join epayrolls r on (r.ref = i.payroll and r.cper <= :cperdeduct)
            left join ebailiff b on (b.ref = d.bailiff)
          where d.fromdate <= :ctodate
            and (d.todate >= :ctodate or d.todate is null)  --BS49784
          group by b.bailiffname, d.signature, d.courtcase, d.initdeduction
          into: descript, :signature, :courtcase, :initdeduction, :bdeducted
      do begin
        if (bdeducted < initdeduction) then
        begin
          if (descript = '') then descript = prtemptytext;
          if (signature = '') then signature = prtemptytext;
          if (courtcase = '') then courtcase = prtemptytext;
          suspend;
        end
      end
    end -- if (CTXTODATE is not null)
  end else
  if (infotype = 40) then
  begin
/*< Parsowanie informacji o wykorzystanym urlopie, parametr ADDPARAMS zawiera
    informacje odpowiednio o zadanym url. wykorzystanym, w tym do wyplaty ekwiwalentu
    oraz na zadanie rozdzielone znakiem '$' np.'20/160$4/32$1' (PR54992) >====*/
    i = 1;
    for
      select outstring from parse_lines(:addparams,'$')
        into :descript_tmp
    do begin
      if (descript_tmp <> '') then
      begin
        j = 1;
        for
          select outstring from parse_lines(:descript_tmp,'/')
            into :descript_tmp2
        do begin
          execute procedure efunc_get_numbers_from_string(descript_tmp2,null,'.,')
            returning_values descript_tmp2;
  
          descript_tmp2 = replace(:descript_tmp2, ',', '.');
          if (descript_tmp2 <> '') then
          begin
            vval = cast(descript_tmp2 as numeric(14,2));
            if (i = 1) then begin
              if (j = 1) then usedvacdays = vval;
              else usedvachours = vval;
            end else
            if (i = 2) then begin
              if (j = 1) then equivaldays = vval;
              else equivalhours = vval;
            end else
            if (i = 3 and j = 1) then
              requestvacdays = vval;
          end
          j = j + 1;
        end
      end
      i = i + 1;
    end
    suspend;
  end if (infotype in (50, 51)) then
  begin
--< Formatowanie informacji o dniach/tygodniach (BS99748) >=====================
    addinfo = employee;
    descript = '';

    if (addinfo < 0) then
    begin
      execute procedure durationstr2(-addinfo,0,0,0)
        returning_values descript;
      descript = descript || ' godz.';
    end else begin

      if (infotype = 51 and addinfo > 0) then -- przeliczenie dni na tygodnie
      begin
        weekscount = addinfo / 7;

        if (weekscount = 1) then
          descript = weekscount || ' tydzień';
        else if (weekscount between 2 and 4) then
          descript = weekscount || ' tygodnie';
        else if (weekscount > 0) then
          descript = weekscount || ' tygodni';

        addinfo = addinfo - (weekscount * 7);

        if (addinfo > 0) then
        begin
          if (weekscount > 0) then descript = descript || ' i ';
        end else
          addinfo = null;
      end

      if (addinfo = 1) then
        descript = descript || addinfo || ' dzień';
      else if (addinfo >= 0) then
        descript = descript || addinfo || ' dni';
    end
    suspend;
  end
end^
SET TERM ; ^
