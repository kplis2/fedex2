--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE MWS_DOK_BRAKTOWARU_OZNACZ(
      WH DEFMAGAZ_ID)
   as
declare variable DREF DOKUMNAG_ID;
declare variable PREF DOKUMPOZ_ID;
declare variable VERS WERSJE_ID;
declare variable GOOD KTM_ID;
declare variable POSIL ILOSCI_MAG;
declare variable STAN ILOSCI_MAG;
declare variable ROZNICA ILOSCI_MAG;
declare variable DOKOR ILOSCI_MAG;
declare variable ILOSCORG ILOSCI_MAG;
declare variable DOKKORREF DOKUMNAG_ID;
declare variable STATUS SMALLINT_ID;
declare variable NEWDOK DOKUMNAG_ID;
declare variable NEWDOKSYM STRING30;
declare variable GLOBALPARAMSET SMALLINT_ID;
begin
  --[PM] oznaczanie braku towaru na dokumentach, wywolywane w szedulerze

  update mwsconstlocs c set c.act = 1 where c.act = -1;
  for
    select s.vers, s.good, s.posq, s.locq
      from MWS_DOK_BRAKTOWARU_DETS (:wh) s
       into vers, good, posil, stan
  do begin
    roznica = posil - stan;
    if (roznica > 0 and coalesce(globalparamset, 0) = 0) then
      execute procedure set_global_param('AKTUOPERATOR',33);
    for
      select l.docid, l.docposid, p.iloscl - p.ilosconmwsacts
        from mwsgoodsrefill l
          left join dokumpoz p on p.ref = l.docposid
          left join dokumnag n on (p.dokument = n.ref and n.braktowaru = 0)
        where l.wh = :wh and l.vers = :vers
          and p.iloscl - p.ilosconmwsacts > 0
        order by l.ordered desc
        into dref, pref, iloscorg
    do begin
      if (roznica > 0 and iloscorg > 0) then
      begin
        if (roznica > iloscorg) then
          dokor = iloscorg;
        else
          dokor = roznica;
        /*execute procedure MAG_CREATE_DOK_KORYG (:dref, current_date,0) --[PM] XXX start ZG105282
          returning_values(:status, :newdok, :newdoksym);
        insert into dokumpoz(dokument, ktm, wersjaref, ilosc, kortopoz)
          values(:newdok,:good,:vers,:dokor,:pref);
        roznica = roznica - dokor;
        update dokumnag d set d.akcept = 1 where d.ref = :newdok;*/

        update dokumnag set braktowaru = 1
          where ref = :dref and braktowaru = 0;

        update dokumpoz set braktowaru = 1, braktowaru_ilosc = :dokor
          where ref = :pref and coalesce(braktowaru,0) <> 1
            and coalesce(braktowaru_ilosc,0) <> :dokor;

        roznica = roznica - dokor;

        if (roznica <= 0) then
          break;
      end
    end
  end
end^
SET TERM ; ^
