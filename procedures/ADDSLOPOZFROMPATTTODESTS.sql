--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE ADDSLOPOZFROMPATTTODESTS(
      PATTERN_SLODEFS varchar(4096) CHARACTER SET UTF8                           ,
      DEST_SLODEFS varchar(4096) CHARACTER SET UTF8                           )
   as
declare variable VAL_dest integer;
declare variable VAL_patt integer;
declare variable CURRENT_COMPANY COMPANIES_ID;
declare variable pattern_copany companies_id;
declare variable ret slo_id;
declare variable slopozpatt slopoz_id;
declare variable newslopoz slopoz_id;
begin
  -- exception universal;
  -- ściągamy obecny zakład
  --exception universal' '||:pattern_slodefs||'  '||:dest_slodefs;
  execute procedure get_global_param('CURRENTCOMPANY')
    returning_values :current_company;

  pattern_slodefs = substring( pattern_slodefs from 1 for char_length(pattern_slodefs));
  pattern_slodefs = substring( pattern_slodefs from 1 for char_length(pattern_slodefs));

  dest_slodefs = substring( dest_slodefs from 1 for char_length(dest_slodefs));
  dest_slodefs = substring( dest_slodefs from 1 for char_length(dest_slodefs));

  for
    select outstring
      from parse_lines(:dest_slodefs,';')
      into :val_dest
  do begin
    for
      select outstring
       from parse_lines(:pattern_slodefs,';')
        into :val_patt
    do begin
      insert into slopoz( REF, SLOWNIK, KOD, NAZWA, KONTOKS, CRM, TOKEN, STATE,
        AKT, CPODMIOT, MASTERREF, MASTERIDENT, OPIS, DFLAGS, RIGHTS, RIGHTSGROUP,
        PATTERN_REF, INTERNALUPDATE)
      select null, :val_dest, KOD, NAZWA, KONTOKS, CRM, TOKEN, STATE,
        AKT, CPODMIOT, MASTERREF, MASTERIDENT, OPIS, DFLAGS, RIGHTS, RIGHTSGROUP,
        REF, INTERNALUPDATE
        from slopoz
        where ref = :val_patt
      returning REF, PATTERN_REF into :newslopoz, :slopozpatt;

       insert into accstructuredists(number,distdef,distfilter,otable,oref,ord, pattern_ref, internalupdate)
         select number,distdef,distfilter,otable,:newslopoz, ord,ref, 1
           from accstructuredists a
           where oref = :slopozpatt and otable = 'SLOPOZ';
    end
  end
  suspend;
end^
SET TERM ; ^
