--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_INT_IMP_DOKUMENTY(
      SESJAREF SESJE_ID,
      TABELA STRING35,
      REF INTEGER_ID,
      BLOKUJZALEZNE SMALLINT_ID = 0,
      TYLKONIEPRZETWORZONE SMALLINT_ID = 0,
      AKTUALIZUJSESJE SMALLINT_ID = 1)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
declare variable stabela string35;
declare variable tmptabela string35;
declare variable sprocedura string255;
declare variable szrodlo zrodla_id;
declare variable skierunek smallint_id;
declare variable sesja sesje_id;
declare variable sql memo;
declare variable tmpsql memo;
declare variable tmpstatus smallint_id;
declare variable tmpmsg string255;
declare variable error_row smallint_id;
declare variable errormsg_row string255;
declare variable errormsg_all string255;
declare variable statuspoz smallint_id;
--TOWAR
declare variable T_REF INTEGER_ID;
declare variable T_TOWARID STRING120;
declare variable T_SYMBOL STRING255;
declare variable T_NAZWA STRING255;
declare variable T_GRUPA STRING255;
declare variable T_GRUPAID STRING120;
declare variable T_RODZAJ STRING255;
declare variable T_RODZAJID STRING120;
declare variable T_AKTYWNY INTEGER_ID;
declare variable T_VAT STRING10;
declare variable T_OPIS MEMO;
declare variable T_CENAZAKUPUNET  KURS_ID;
declare variable T_CENAZAKUPUBRU  KURS_ID;
declare variable T_CHODLIWY INTEGER_ID;
declare variable T_WITRYNA INTEGER_ID;
declare variable T_HASH STRING255;
declare variable T_DEL INTEGER_ID;
declare variable T_REC STRING255;
declare variable T_SKADTABELA STRING40;
declare variable T_SKADREF INTEGER_ID;
--towar zmienne pomocnicze
declare variable T_TMP_TOWTYPE SMALLINT_ID; --nie zmieniac na domene towtype_id !!!!
declare variable T_TMP_NAZWA TOWARY_NAZWA;
--JEDNOSTKA
declare variable J_REF INTEGER_ID;
declare variable J_TOWARID STRING120;
declare variable J_JEDNOSTKA STRING60;
declare variable J_JEDNOSTKAID STRING120;
declare variable J_GLOWNA INTEGER_ID;
declare variable J_LICZNIK NUMERIC_14_4;
declare variable J_MIANOWNIK NUMERIC_14_4;
declare variable J_WAGA NUMERIC_14_4;
declare variable J_WYSOKOSC NUMERIC_14_4;
declare variable J_DLUGOSC NUMERIC_14_4;
declare variable J_SZEROKOSC NUMERIC_14_4;
declare variable J_HASH STRING255;
declare variable J_DEL INTEGER_ID;
declare variable J_REC STRING255;
declare variable J_SKADTABELA STRING40;
declare variable J_SKADREF INTEGER_ID;
--jednostka zmienne pomocnicze
declare variable J_TMP_PRZELICZ NUMERIC_14_4;
declare variable J_GLOWNASENTE SMALLINT_ID;
--KOD KRESKOWY
declare variable K_REF INTEGER_ID;
declare variable K_TOWARID STRING120;
declare variable K_JEDNOSTKA STRING60;
declare variable K_JEDNOSTKAID STRING120;
declare variable K_KODKRESKOWY STRING255;
declare variable K_GLOWNY INTEGER_ID;
declare variable K_HASH STRING255;
declare variable K_DEL INTEGER_ID;
declare variable K_REC STRING255;
declare variable K_SKADTABELA STRING40;
declare variable K_SKADREF INTEGER_ID;
--kod kreskowy zmienne pomocnicze
--NAGLOWEK ZAMOWIENIA
declare variable NZ_REF INTEGER_ID;
declare variable NZ_NAGID STRING120;
declare variable NZ_SYMBOL STRING255;
declare variable NZ_TYP STRING20;
declare variable NZ_DATAZAM TIMESTAMP_ID;
declare variable NZ_WYDANIA INTEGER_ID;
declare variable NZ_ZEWNETRZNY INTEGER_ID;
declare variable NZ_BN CHAR_1;
declare variable NZ_MAGAZYN STRING20;
declare variable NZ_MAGAZYN2 STRING20;
declare variable NZ_KLIENTID STRING120;
declare variable NZ_ODBIORCAID STRING120;
declare variable NZ_DOSTAWCAID STRING120;
declare variable NZ_PARTIA STRING255;
declare variable NZ_PARTIAID STRING120;
declare variable NZ_ZAPLATA STRING255;
declare variable NZ_ZAPLATAID STRING120;
declare variable NZ_DOSTAWA STRING255;
declare variable NZ_DOSTAWAID STRING120;
declare variable NZ_WALUTA STRING20;
declare variable NZ_KURS KURS_ID;
declare variable NZ_UWAGIWEW MEMO;
declare variable NZ_UWAGIZEW MEMO;
declare variable NZ_UWAGISPED MEMO;
declare variable NZ_DATAWYSYLKI TIMESTAMP_ID;
declare variable NZ_WARTOSCNETTO KURS_ID;
declare variable NZ_WARTOSCBRUTTO KURS_ID;
declare variable NZ_WARTOSCNETTOZL KURS_ID;
declare variable NZ_WARTOSCBRUTTOZL KURS_ID;
declare variable NZ_DOZAPLATY KURS_ID;
declare variable NZ_RABATWYLICZ INTEGER_ID;
declare variable NZ_RABATPROCENT CENY;
declare variable NZ_RABATWARTOSC KURS_ID;
declare variable NZ_BLOKADA CHAR_1;
declare variable NZ_TYPDOKFAK STRING20;
declare variable NZ_TYPDOKMAG STRING20;
declare variable NZ_STATUSZAM STRING60;
declare variable NZ_HASH STRING255;
declare variable NZ_DEL INTEGER_ID;
declare variable NZ_REC STRING255;
--zamowienie zmienne pomocnicze
declare variable nz_tmp_typdok defdokum_id;
--zmienne ogolne
declare variable KTM KTM_ID;
declare variable WERSJAREF WERSJE_ID;
declare variable JEDN JEDN_MIARY;
declare variable TOWJEDN TOWJEDN;
declare variable SPOSZAP PLATNOSC_ID;
declare variable SPOSDOST SPOSDOST_ID;
declare variable KLIENT KLIENCI_ID;
declare variable ODBIORCA ODBIORCY_ID;
declare variable DOSTAWCA DOSTAWCA_ID;
declare variable DOKUMENT DOKUMNAG_ID;
declare variable ODDZIAL ODDZIAL_ID;
declare variable COMPANY COMPANIES_ID;
declare variable DOSTAWA DOSTAWA_ID;
declare variable OPERATOR OPERATOR_ID;
declare variable SPRZEDAWCA SPRZEDAWCA_ID;
declare variable magazyn defmagaz_id;
declare variable mag2 defmagaz_id;
declare variable x_dokumnag integer_id;
begin
  sesja = :sesjaref;

  status = 1;
  msg = '';
  errormsg_all = '';
  tmpstatus = 1;
  tmpmsg = '';

  --pobranie parametrow sesji
  select s.tabela, s.procedura, s.zrodlo, s.kierunek
    from int_sesje s
    where ref = :sesja
  into :stabela, :sprocedura, :szrodlo, :skierunek;

  if (:tabela is not null) then --jesli podano tabele na wejsciu traktuje ja nadrzednie
    stabela = :tabela;

  --wlasciwe przetworzenie
  oddzial = null;
  company = null;
  select oddzial, company from int_zrodla where ref = :szrodlo
  into :oddzial, :company;

  if (:oddzial is null or :company is null) then
  begin
    status = -1;
    msg = 'Nie ustawiono parametrów konfiguracyjnych źródła danych: oddział, firma';
    exit; --EXIT
  end

  if (stabela = 'INT_IMP_ZAM_NAGLOWKI') then
  begin
    for
      select n.ref, n.nagid, n.symbol, n.typ, n.datazam, n.wydania, n.zewnetrzny, n.bn, n.magazyn, n.magazyn2,
          n.klientid, n.odbiorcaid, n.dostawcaid,
          n.partia, n.partiaid, n.zaplata, n.zaplataid, n.dostawa, n.dostawaid, n.waluta, n.kurs,
          n.uwagiwew, n.uwagizew, n.uwagisped, n.datawysylki,
          n.wartoscnetto, n.wartoscbrutto, n.wartoscnettozl, n.wartoscbruttozl, n.dozaplaty,
          n.rabatwylicz, n.rabatprocent, n.rabatwartosc,
          n.blokada, n.typdokfak, n.typdokmag, n.statuszam,
          n.del, n.rec
        from int_imp_zam_naglowki n
        where n.sesja = :sesja
          and (n.ref = :ref or :ref is null)
        order by ref
      into :nz_ref, :nz_nagid, :nz_symbol, :nz_typ, :nz_datazam, :nz_wydania, :nz_zewnetrzny, :nz_bn, :nz_magazyn, :nz_magazyn2,
        :nz_klientid, :nz_odbiorcaid, :nz_dostawcaid,
        :nz_partia, :nz_partiaid, :nz_zaplata, :nz_zaplataid, :nz_dostawa, :nz_dostawaid, :nz_waluta, :nz_kurs,
        :nz_uwagiwew, :nz_uwagizew, :nz_uwagisped, :nz_datawysylki,
        :nz_wartoscnetto, :nz_wartoscbrutto, :nz_wartoscnettozl, :nz_wartoscbruttozl, :nz_dozaplaty,
        :nz_rabatwylicz, :nz_rabatprocent, :nz_rabatwartosc,
        :nz_blokada, :nz_typdokfak, :nz_typdokmag, :nz_statuszam,
        :nz_del, :nz_rec
    do begin
      error_row = 0;
      errormsg_row = '';

      if (coalesce(trim(:nz_nagid),'') = '') then
      begin
        error_row = 1;
        errormsg_row = substring(:errormsg_row || ' ' || 'Brak ID nagłówka.' from 1 for 255);
      end

      select first 1 ref from dokumnag dn
            where dn.int_zrodlo = :szrodlo and dn.int_id = :nz_nagid  into :x_dokumnag;

      --XXX JO: COTO
      if (coalesce(:nz_del,0) = 0 and  x_dokumnag is not null
            ---exists(select first 1 1 from dokumnag dn
            ---where dn.int_zrodlo = :szrodlo and dn.int_id = :nz_nagid)
         ) then
      begin
        error_row = 1;
        errormsg_row = substring(:errormsg_row || ' ' || 'Zamówienie o podanym identyfikatorze już istnieje.' from 1 for 255);
      end


      if (exists(select first 1 1 from listywysdpoz lp
                    --left join dokumpoz dp on (lp.doktyp = 'M' and lp.dokref = dp.dokument and lp.dokpoz = dp.ref)
                    left join dokumnag dn on (lp.doktyp = 'M' and lp.dokref = dn.ref)
                  where dn.int_zrodlo = :szrodlo and dn.int_id = :nz_nagid)
         ) then
      begin
        error_row = 1;
        errormsg_row = substring(:errormsg_row || ' ' || 'Dokumenty sa w trakcie pakowania/spakowane.' from 1 for 255);
      end

      --ML walidacja
      --czy towary istnieja
      for select zp.towarid, t.ktm from int_imp_zam_pozycje zp
        left join towary t
          on t.int_zrodlo is not distinct from :szrodlo
            and t.int_id is not distinct from zp.towarid
        where zp.sesja = :sesja and zp.nagid = :nz_nagid
        into :t_towarid, :ktm
      do begin
        if(coalesce(:ktm,'') = '')then
        begin
          error_row = 1;
          errormsg_row = substring(errormsg_row || ' ' || 'Brak towaru o id: ' || coalesce(:t_towarid,'<pusty>') from 1 for 255);
        end
      end
      --koniec ML walidacja

      if (coalesce(:nz_del,0) = 1 and :error_row = 0 and x_dokumnag is not null) then --rekord do skasowania
      begin
        tmpstatus = 0;
        select status, msg from INT_IMP_ZAM_NAGLOWKI_DEL(:x_dokumnag)
          into :tmpstatus, :tmpmsg;
       -- tmpstatus = null;
        if (tmpstatus <> 1) then  begin
          error_row = 1;
          errormsg_row = substring(:errormsg_row || ' ' || :tmpmsg from 1 for 255);
        end
      end
      else if (:error_row = 0 and coalesce(:nz_del,0) = 0) then
      begin
        if (coalesce(trim(:nz_zaplata),'') = '' and coalesce(trim(:nz_zaplataid),'') = '') then
        begin
          error_row = 1;
          errormsg_row = substring(:errormsg_row || ' ' || 'Brak sposobu zapłaty.' from 1 for 255);
        end
        else
        begin

          select first 1 p.ref from platnosci p
            where lower(p.opis) = lower(trim(:nz_zaplata))
              or lower(p.id_zewn) = lower(trim(:nz_zaplataid))
          into :sposzap;

          --ML podmienić po uzupelnieniu slownikow
          /* 
          select p.ref from int_slownik s
            join platnosci p on p.ref = s.oref
            where s.zrodlo = :szrodlo
              and (:nz_zaplata is null or s.wartosc is not distinct from :nz_zaplata)
              and s.wartoscid is not distinct from :nz_zaplataId
              and s.otable = 'PLATNOSCI'
            into :sposzap;
         */

          if (:sposzap is null) then
          begin
            error_row = 1;
            errormsg_row = substring(:errormsg_row || ' ' || 'Brak sposobu zapłaty.' from 1 for 255);
          end
        end

        if (coalesce(trim(:nz_dostawa),'') = '' and coalesce(trim(:nz_dostawaid),'') = '') then
        begin
          error_row = 1;
          errormsg_row = substring(:errormsg_row || ' ' || 'Brak sposobu dostawy.' from 1 for 255);
        end
        else
        begin

          select first 1 s.ref from sposdost s
            where lower(s.nazwa) = lower(trim(:nz_dostawa))
              or lower(s.id_zewn) = lower(trim(:nz_dostawaid))
          into :sposdost;
          --ML podmienić po uzupelnieniu slownikow
          if (sposdost is null) then begin
          select sd.ref from int_slownik s
            join sposdost sd on sd.ref = s.oref
            where s.zrodlo = :szrodlo
              and (:nz_dostawa is null or s.wartosc is not distinct from :nz_dostawa)
              and s.wartoscid is not distinct from :nz_dostawaid
              and s.otable = 'SPOSDOST'
            into :sposdost;
          end

          if (:sposdost is null) then
          begin
            error_row = 1;
            errormsg_row = substring(:errormsg_row || ' ' || 'Brak sposobu dostawy.' from 1 for 255);
          end
        end

        if (:error_row = 0) then
        begin
          --blokujzalezne = 1; --!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          if (:blokujzalezne = 0) then --tabele zalezne BEFORE
          begin
            select status, msg from int_tabelezalezne_process(:sesja,
                :stabela, :nz_ref, :szrodlo, :skierunek, 0)
              into :tmpstatus, :tmpmsg;
          end

          if(:tmpstatus is distinct from 1)then
          begin
            error_row = 1;
            errormsg_row = substring(:errormsg_row || ' ' || :tmpmsg from 1 for 255);

         --   tmpstatus = 1;
         --   tmpmsg = '';
          end

          --nullowanie, zerowanie i blankowanie - naglowek zamowienia
          if (coalesce(trim(:nz_symbol),'') = '') then
            nz_symbol = null;
          if (coalesce(trim(:nz_typ),'') = '') then
            nz_typ = 'ZAM';
          if (:nz_wydania is null) then
            nz_wydania = 1;
          if (:nz_zewnetrzny is null) then
            nz_zewnetrzny = 1;
          if (coalesce(trim(:nz_bn),'') = '') then
            nz_bn = 'N';

          --JO: ustawienie magazynow
          select symbol from defmagaz d where d.int_zrodlo = :szrodlo and d.int_symbol = :nz_magazyn
          into :magazyn;
          select symbol from defmagaz d where d.int_zrodlo = :szrodlo and d.int_symbol = :nz_magazyn2
          into :mag2;
          if (coalesce(:magazyn,'') = '') then
            select symbol from defmagaz d where d.int_zrodlo = :szrodlo and d.int_id = :nz_magazyn
            into :magazyn;
          if (coalesce(:mag2,'') = '') then
            select symbol from defmagaz d where d.int_zrodlo = :szrodlo and d.int_id = :nz_magazyn2
            into :mag2;

          if (coalesce(:magazyn,'') = '') then
          begin
            error_row = 1;
            errormsg_row = substring(:errormsg_row || ' ' || 'Brak magazynu w systemie.' from 1 for 255);
          end
          if (coalesce(trim(:mag2),'') = '') then
            mag2 = null;
          --JO: koniec

          if (coalesce(trim(:nz_partia),'') = '') then
            nz_partia = null;
          if (coalesce(trim(:nz_partiaid),'') = '') then
            nz_partiaid = null;
          if (coalesce(trim(:nz_waluta),'') = '') then
            nz_waluta = 'PLN';
          if (coalesce(trim(:nz_uwagiwew),'') = '') then
            nz_uwagiwew = null;
          if (coalesce(trim(:nz_uwagizew),'') = '') then
            nz_uwagizew = null;
          if (:nz_dozaplaty is null) then
            nz_dozaplaty = 0;
          if (coalesce(trim(:nz_blokada),'') = '') then
            nz_blokada = null;
          if (coalesce(trim(:nz_typdokfak),'') = '') then
            nz_typdokfak = null;
          if (coalesce(trim(:nz_typdokmag),'') = '') then
            nz_typdokmag = null;
          if (coalesce(trim(:nz_statuszam),'') = '') then
            nz_statuszam = null;

          --JO: wywalam, zrobione wyzej
          /*if (not exists(select first 1 1 from defmagaz where symbol = :nz_magazyn)) then
            nz_magazyn = 'MWS';
          if (:nz_magazyn2 is not null and
              not exists(select first 1 1 from defmagaz where symbol = :nz_magazyn)
             ) then
            nz_magazyn2 = 'MWS';*/
          --JO: koniec

          if (not exists(select first 1 1 from defdokummag dm where dm.magazyn = :magazyn
               and dm.typ = :nz_typdokmag)) then
          begin
            nz_tmp_typdok = null;

            select first 1 dd.symbol
              from defdokummag ddm join defdokum dd on (ddm.typ = dd.symbol)
              where ddm.magazyn = :magazyn and dd.wydania = :nz_wydania
                and dd.zewn = :nz_zewnetrzny and dd.koryg = 0
              order by case when dd.symbol in ('WZ', 'PZ', 'RW', 'PW') then 0 else 1 end
            into :nz_tmp_typdok;
          end
          else
            nz_tmp_typdok = :nz_typdokmag;

          update int_imp_zam_naglowki set
              --klient_sente = :klient, odbiorca_sente = :odbiorca, dostawca_sente = :dostawca, --usuwam poniewaz parametry te uzupelnione sa w zaleznych
              sposzap_sente = :sposzap, sposdost_sente = :sposdost,
              magazyn_sente = :magazyn, mag2_sente = :mag2,
              typ_sente = :nz_typ,
              data_sente = coalesce(data_sente, current_date),
              dostawa_sente = :dostawa, operator_sente = :operator,
              oddzial_sente = :oddzial, company_sente = :company,
              walutowy_sente = case when :nz_waluta = 'PLN' then 0 else 1 end,
              kurs_sente = :nz_kurs, waluta_sente = :nz_waluta,
              typdokvat_sente = :nz_typdokfak, typdokmag_sente = :nz_tmp_typdok,
              sprzedawca_sente = :sprzedawca, bn_sente = :nz_bn,
              termdost_sente = :nz_datawysylki,
              dataostprzetw = case when :aktualizujsesje = 0 then dataostprzetw else current_timestamp(0) end
            where ref = :nz_ref;

          if (exists (select first 1 1 from dokumnag dn
               where dn.int_zrodlo = :szrodlo and dn.int_id = :nz_nagid) --UPDATE naglowkow
              ) then
          begin
            select klient_sente, odbiorca_sente, dostawca_sente
              from int_imp_zam_naglowki n
              where n.ref = :nz_ref
            into :klient, :odbiorca, :dostawca;

            update dokumnag set klient = :klient, odbiorca = :odbiorca,
                /*klienthist_sente,*/ dostawca = :dostawca,
                sposzap = :sposzap, sposdost = :sposdost,
                magazyn = :magazyn, mag2 = :mag2,
                /*dostawa = :dostawa,*/ operator = :operator,
                oddzial = :oddzial, company = :company,
                walutowy = case when :nz_waluta = 'PLN' then 0 else 1 end,
                kurs = :nz_kurs, waluta = :nz_waluta,
                typdokvat = :nz_typdokfak, /*typdokmag = :nz_typdokmag,*/
                sprzedawca = :sprzedawca, bn = :nz_bn,
                termdost = :nz_datawysylki
              where int_zrodlo = :szrodlo and int_id = :nz_nagid;
          end 

          if (:blokujzalezne = 0) then --tabele zalezne AFTER
          begin
            select status, msg from int_tabelezalezne_process(:sesja,
                :stabela, :nz_ref, :szrodlo, :skierunek, 1, 'DOK')
              into :tmpstatus, :tmpmsg;
          end

          if(:tmpstatus is distinct from 1)then
          begin
            error_row = 1;
            errormsg_row = substring(:errormsg_row || ' ' || :tmpmsg from 1 for 255);

            tmpstatus = 1;
            tmpmsg = '';
          end

        end

      end
      if(error_row = 1)then
      begin
        status = -1; --ML jezeli nie przetworzy sie chociaz jeden wiersz przetwarzanie nieudane;
        errormsg_all = substring(errormsg_all || :errormsg_row from 1 for 255);
      end
    end
  end

  if(coalesce(:status,-1) != 1) then
    msg = substring((msg || :errormsg_all) from 1 for 255);

  suspend;
end^
SET TERM ; ^
