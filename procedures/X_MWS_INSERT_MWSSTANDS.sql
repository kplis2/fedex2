--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_INSERT_MWSSTANDS(
      WH varchar(3) CHARACTER SET UTF8                           ,
      WHSEC integer,
      STORIESQUANT smallint,
      LOADCAPACITY numeric(15,4),
      MWSSTANDTYPE integer,
      L numeric(15,4),
      W numeric(15,4),
      MWSSTANDSEGQ integer,
      WHSECROW integer,
      ORD smallint,
      WHSECDICT varchar(3) CHARACTER SET UTF8                           ,
      WHSECROWDICT varchar(3) CHARACTER SET UTF8                           ,
      QUANTITY integer)
   as
    declare variable i integer;
begin
--procedura sluzaca do dodawania konkretnej liczby regalow do danego rzedu
    i = 0;
    if (coalesce(quantity, 0) > 0) then
    begin
        while (i < quantity) do
        begin
            insert into MWSSTANDS ( WH, WHSEC, STORIESQUANT, LOADCAPACITY, MWSSTANDTYPE, L, W, MWSSTANDSEGQ, WHSECROW,
                                    ORD, WHSECDICT, WHSECROWDICT)
            values ( :WH, :WHSEC, :STORIESQUANT, :LOADCAPACITY, :MWSSTANDTYPE, :L, :W, :MWSSTANDSEGQ, :WHSECROW,
                     :ORD, :WHSECDICT, :WHSECROWDICT);
            i = i+1;
        end
    end
end^
SET TERM ; ^
