--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE CENNIK_KOPIUJ(
      CENNIK integer,
      CENNIKZRODL integer,
      KTMMASKA varchar(40) CHARACTER SET UTF8                           ,
      ODKTM varchar(40) CHARACTER SET UTF8                           ,
      DOKTM varchar(40) CHARACTER SET UTF8                           ,
      PRECYZJA smallint,
      PRZELICZNIK numeric(14,4))
  returns (
      STATUS integer,
      ILEPRZEL integer,
      ILEDOD integer,
      KTM varchar(40) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE WERSJAREF INTEGER;
DECLARE VARIABLE JEDN INTEGER;
DECLARE VARIABLE CENANET NUMERIC(14,4);
DECLARE VARIABLE CENABRU NUMERIC(14,4);
DECLARE VARIABLE CENANETC NUMERIC(14,4);
DECLARE VARIABLE CENABRUC NUMERIC(14,4);
DECLARE VARIABLE NARZUT NUMERIC(14,4);
DECLARE VARIABLE MARZA NUMERIC(14,4);
DECLARE VARIABLE STARACENANET NUMERIC(14,4);
DECLARE VARIABLE STARYNARZUT NUMERIC(14,4);
DECLARE VARIABLE STARAMARZA NUMERIC(14,4);
DECLARE VARIABLE WALUTA VARCHAR(3);
DECLARE VARIABLE WALUTAZROD VARCHAR(3);
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE DEFSTALACENA SMALLINT;
DECLARE VARIABLE NARZUTC NUMERIC(14,4);
DECLARE VARIABLE MARZAC NUMERIC(14,4);
DECLARE VARIABLE CENAZAK NUMERIC(14,4);
DECLARE VARIABLE CENAPNET NUMERIC(14,4);
DECLARE VARIABLE CENAPBRU NUMERIC(14,4);
begin
  ileprzel = 0;
  iledod = 0;
  if (:precyzja is null or (:precyzja < 0) ) then precyzja = 0;
  if (:przelicznik is null or :przelicznik = 0) then przelicznik = 0;
  if(:cennik=:cennikzrodl) then begin
    status = 0;
    exit;
  end
  select defcennik.waluta from defcennik where ref=:cennik
    into :waluta;
  if (:PRZELICZNIK is null) then przelicznik = 1;
  select DEFSTALACENA from DEFCENNIK where DEFCENNIK.ref = :cennik into :defstalacena;
  for
    select CENNIK.WERSJAREF,CENNIK.JEDN,CENNIK.CENANET,CENNIK.CENABRU,CENNIK.MARZA,CENNIK.NARZUT,CENNIK.WALUTA
    from CENNIK
    where (cennik.cennik=:CENNIKZRODL)
    and (CENNIK.KTM >=:odktm or (:odktm = '' or :odktm is null))
    and (CENNIK.KTM <= :doktm or (:doktm = '' or :doktm is null))
    and (CENNIK.KTM like :ktmmaska or (:ktmmaska = '' or :ktmmaska is null))
    into :wersjaref,:jedn,:cenanet,:cenabru,:marza,:narzut,:walutazrod
  do begin
    if (:waluta <> :walutazrod) then
      CENAZAK = cast((:cenanet*(1-:marza/100))/:przelicznik as numeric(14,2));
    else
      CENAZAK = cast(:cenanet*(1-:marza/100) as numeric(14,2));
    if (:cenazak is null) then cenazak = 0;
    if (:CENANET <> 0) then
    MARZAC = (:cenanet/:przelicznik - :cenazak)/(:CENANET/:przelicznik);
      else MARZAC = 0;
    marzac = :marzac*100;
    if (:marzac <> 100 and :marzac is not null) then
      NARZUTC = MARZAC/(100 - MARZAC);
    else
      NARZUTC = 0;
    CENANETC = :cenanet/:przelicznik;
    CENABRUC = :cenabru/:przelicznik;
    if (:PRECYZJA = 0) then  begin
      MARZAC = cast(:MARZAC as numeric(14,2));
      NARZUTC = cast(:NARZUTC as numeric(14,2));
      CENANETC = cast(:CENANETC as numeric(14,2));
      CENABRUC = cast(:CENABRUC as numeric(14,2));
    end else begin
      MARZAC = cast(:MARZAC as numeric(14,4));
      NARZUTC = cast(:NARZUTC as numeric(14,4));
      CENANETC = cast(:CENANETC as numeric(14,4));
      CENABRUC = cast(:CENABRUC as numeric(14,4));
    end
    select count(*) from CENNIK where (CENNIK=:CENNIK and WERSJAREF=:wersjaref and JEDN=:jedn) into :cnt;
    if(:cnt>0) then begin
      /* zaktualizuj istniejaca pozycje cennika */
--      select CENANET,CENABRU,MARZA,NARZUT from CENNIK where (CENNIK=:CENNIK and WERSJAREF=:wersjaref and JEDN=:jedn) into :CENAPNET, :CENAPBRU,:staramarza,:starynarzut;
--      if(:cenanet<>:staracenanet or (:marza<>:staramarza) or (:narzut<>:starynarzut)) then begin
        update CENNIK set CENAPNET=CENAPNET, CENAPBRU=CENAPBRU,
          CENANET=:CENANETC, CENABRU=:CENABRUC, MARZA=:MARZAC, NARZUT=:NARZUTC, WALUTA=:waluta
          where (CENNIK=:CENNIK and WERSJAREF=:wersjaref and JEDN=:jedn);
        ileprzel = :ileprzel+1;
--      end
    end else begin
      /* dodaj nowa pozycje cennika */
      insert into CENNIK(CENNIK,WERSJAREF,JEDN,CENANET,CENABRU,STALACENA,MARZA,NARZUT,WALUTA)
      values (:cennik,:wersjaref,:jedn,:cenanetc,:cenabruc, :defstalacena, :marzac, :narzutc,:waluta);
      iledod = :iledod+1;
    end
  end
  status = 1;
end^
SET TERM ; ^
