--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE NEOS_SEGREGATORY(
      AKTUOPER integer = 0,
      DOKZLACZTABLE varchar(20) CHARACTER SET UTF8                            = '',
      DOKZLACZREF integer = 0,
      SPRPUSTE smallint = 0)
  returns (
      REF integer,
      PARENT integer,
      ICON varchar(20) CHARACTER SET UTF8                           ,
      DISPLAYNAME varchar(255) CHARACTER SET UTF8                           ,
      FORMATFIELD varchar(255) CHARACTER SET UTF8                           ,
      RIGHTS varchar(255) CHARACTER SET UTF8                           ,
      RIGHTSGROUP varchar(255) CHARACTER SET UTF8                           ,
      PUSTY smallint,
      NUMER integer)
   as
declare variable ADMINISTRATOR smallint;
declare variable GRUPY varchar(1024);
begin

    aktuoper = coalesce(:aktuoper,0);
    dokzlacztable = coalesce(:dokzlacztable,'');
    dokzlaczref = coalesce(:dokzlaczref,0);
    --zabezpieczenie przed blednie przekazanym operatorem
    if (:aktuoper = 0) then exit;
    select o.administrator, o.grupa from operator o where o.ref = :aktuoper
     into :administrator, :grupy;
    administrator = coalesce(:administrator,0);
    grupy = coalesce(:grupy,'');
    sprpuste = coalesce(:sprpuste,0);


    for select s.ref, s.numer, s.rodzic,
    s.icon, s.nazwa, '', s.rights, s.rightsgroup
      from segregator s
    where (:administrator = 1
         or s.rights = ';' and s.rightsgroup = ';'
         or s.rights like '%%;'||:aktuoper||';%%'
         or strmulticmp(:grupy, s.rightsgroup) = 1
         )
    into  :ref, :numer, :parent, :icon, :displayname, :formatfield, :rights, :rightsgroup
    do begin
      pusty = 0;
      if(coalesce(displayname,'')='') then displayname = 'Brak nazwy';
      if(SPRPUSTE = 1) then
        if(dokzlacztable<>'' and dokzlaczref<>0) then begin
          if(not exists(select first 1 1 from DOKPLIKSEGREGATORY ds
                            left join dokzlacz dz on (dz.dokument = ds.dokplik)
                                where ds.segregator = :ref and dz.ztable = :dokzlacztable
                                    and dz.zdokum = :dokzlaczref))
            then PUSTY = 1;
        end else begin
          if(not exists(select first 1 1 from DOKPLIKSEGREGATORY ds where ds.segregator = :ref)) then PUSTY = 1;
        end
      suspend;
    end
end^
SET TERM ; ^
