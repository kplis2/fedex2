--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE X_MWS_SPEEDPACK_ADDOPK(
      LISTWYSD LISTYWYSD_ID,
      TYPOPK STRING10,
      ILOSC INTEGER_ID)
  returns (
      STATUS SMALLINT_ID,
      MSG STRING255)
   as
    declare variable licznik integer_id;
    declare variable typopkref integer_id;
begin
-- procedura sluzy do dodawania opakowan do listu spedycyjnego w trybie szybkiego pakowania
    status = 1;
    msg = '';
    licznik = 0;
    --sprawdzenie czy podano dopuszczalna ilosc
    ilosc = coalesce(ilosc, 0);
    if (ilosc <= 0) then
    begin
        status = 0;
        msg = msg || 'Nieprawidlowa ilosc ('||coalesce(ilosc,'<brak>')||')';
    end
    --pobranie typu opakowania
    select tp.ref
            from typyopk tp
            where tp.symbol = :typopk
        into :typopkref;
    --sprawdzenie czy podano istniejacy typ opakowania
    if (coalesce(typopkref,0)=0) then
    begin
        status = 0;
        msg = msg || 'Brak typu opakowania o symbolu:' || coalesce(:typopk,'<brak>');
    end
    --sprawdzenie czy podano istniejacy dokument spedycyjny
    if (not exists(
        select first 1 ld.ref from listywysd ld where ld.ref = :listwysd
    )) then
    begin
        status = 0;
        msg = msg || 'Brak dokumentu spedycyjnego o ref:'||coalesce(:listwysd,'<brak>');
    end
    if (status = 1) then
    begin
        -- zaznaczenie ze dokument spedycyjny zostal pobrany do szybkiego pakowania
        update listywysd ld
            set ld.x_szybko = 1
            where ld.ref = :listwysd;
        --petla dodajaca opakowania
        while (licznik < ilosc ) do
        begin
        insert into LISTYWYSDROZ_OPK ( LISTWYSD, STAN, TYPOPK, X_SZYBKO)
            values (:LISTWYSD, 1, :typopkref, 1);
            licznik = licznik + 1;
        end
    end
  suspend;
end^
SET TERM ; ^
