--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE KTM_ATRYBUTY(
      KTM varchar(80) CHARACTER SET UTF8                           ,
      ATRYBUT varchar(255) CHARACTER SET UTF8                           ,
      WARTOSC varchar(255) CHARACTER SET UTF8                           )
   as
DECLARE VARIABLE CNT INTEGER;
DECLARE VARIABLE WERSJAREF INTEGER;
DECLARE VARIABLE NRWERSJI SMALLINT;
declare variable vwersje smallint;
begin
  if (not exists(select wersjaref from ATRYBUTY where KTM = :KTM and CECHA = :atrybut)) then begin
    select wersje from defcechy where symbol = :ATRYBUT into :vwersje;
    if (:vwersje = 1) then begin
      -- zakadamy atrybut dla wszystkich wersji
      for select ref, nrwersji
        from wersje
        where ktm = :ktm
        into :wersjaref, :nrwersji
      do begin
        insert into ATRYBUTY (KTM, NRWERSJI, CECHA, WARTOSC, WERSJAREF)
                       values(:ktm,:nrwersji,:atrybut,:wartosc,:wersjaref);
      end
    end else begin
      -- zakadamy atrybut dla wersji zerowej
      select ref
        from wersje
        where ktm = :ktm  and NRWERSJI = 0
        into :wersjaref;
      insert into ATRYBUTY (KTM, NRWERSJI, CECHA, WARTOSC, WERSJAREF)
                       values(:ktm,0,:atrybut,:wartosc,:wersjaref);
    end
  end else if(:wartosc<>'' and :wartosc is not null) then
    update ATRYBUTY set WARTOSC = :wartosc where KTM = :KTM and CECHA = :ATRYBUT;
end^
SET TERM ; ^
