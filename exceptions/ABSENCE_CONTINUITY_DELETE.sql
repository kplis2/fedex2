--@@@***SKEY***@@@--
CREATE OR ALTER EXCEPTION ABSENCE_CONTINUITY_DELETE 'Usuwana nieobecność stanowi ciągłość dla co najmniej jednej z zarejestrowanych nieobecności. Aby usunąć wybraną nieobecność, należy w pierwszej kolejności usunąć powiązane z nią przez ciągłość inne nieobecności. ';
