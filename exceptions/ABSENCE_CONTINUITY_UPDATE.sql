--@@@***SKEY***@@@--
CREATE OR ALTER EXCEPTION ABSENCE_CONTINUITY_UPDATE 'Modyfikowana nieobecność stanowi ciągłość dla co najmniej jednej z zarejestrowanych nieobecności. Aby zmodyfikować wybraną nieobecność, należy w pierwszej kolejności usunąć powiązane z nią przez ciągłość inne nieobecności. ';
