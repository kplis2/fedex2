--@@@***SKEY***@@@--
CREATE GLOBAL TEMPORARY TABLE SYS_ORDERAPPINI (
       SYMBOL STRING40 NOT NULL,
       FROMSYMBOL STRING40,
       DISPLACEMENT INTEGER_ID,
       COUNTBEFORE INTEGER_ID,
       COUNTAFTER INTEGER_ID,
       NUMBER INTEGER_ID,
       LAYER INTEGER_ID
  ) ON COMMIT DELETE ROWS;
