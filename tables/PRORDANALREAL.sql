--@@@***SKEY***@@@--
CREATE GLOBAL TEMPORARY TABLE PRORDANALREAL (
       REF PRORDANALREAL_ID NOT NULL,
       PARENT PRORDANALREAL_ID,
       SYMBOL STRING80,
       NREF NAGZAM_ID,
       PREF POZZAM_ID,
       VERS WERSJE_ID,
       ONDATE DATE_ID,
       QUANTITY QUANTITY_MWS,
       QUANTITYREAL QUANTITY_MWS,
       PERCENTREAL PERCENT,
       COLORINDEX SMALLINT_ID,
       STATUS SMALLINT_ID,
       MATCOSTS CENY_CEN,
       WORKCOSTS CENY_CEN,
       ID INTEGER_ID,
       TKW NUMERIC_14_4
  ) ON COMMIT PRESERVE ROWS;
