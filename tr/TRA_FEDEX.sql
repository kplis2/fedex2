--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER PROCEDURE TRA_FEDEX as
  declare variable spdost sposdost_id;
  declare variable oddzial oddzial_id;
  declare variable ederule ederules_id;
begin
  -- insert regul do ededocsh
  INSERT INTO EDERULES (SYMBOL, IMPEXP, DRIVER, GENPROCEDURE, VERIFYPROCEDURE, IMPORTPROCEDURE, AUTOPROCESS, AUTOEXCHANGE, MANUALINIT, CONFIRMPATH, NEEDSIGN)
    VALUES ('EDE_FEDEX_TRACKING', 1, 'EXP_FEDEX_XML', 'EDE_FEDEX_TRACKING_REQUEST', NULL, 'EDE_FEDEX_TRACKING_RESPONSE', 0, 1, 0, 0, 0)
    returning ref into ederule;
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'SERVICEURL','PROD');
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'ACCESSCODE','');
  INSERT INTO EDERULES (SYMBOL, IMPEXP, DRIVER, GENPROCEDURE, VERIFYPROCEDURE, IMPORTPROCEDURE, AUTOPROCESS, AUTOEXCHANGE, MANUALINIT, CONFIRMPATH, NEEDSIGN)
    VALUES ('EDE_FEDEX_ORDERPRINT', 1, 'EXP_FEDEX_XML', 'EDE_FEDEX_ORDERPRINT_REQUEST', NULL, 'EDE_FEDEX_ORDER_RESPONSE', 0, 1, 0, 0, 0)
    returning ref into ederule;
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'SERVICEURL','PROD');
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'ACCESSCODE','');
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'FILEPATH','.\pdf\fedex');
  INSERT INTO EDERULES (SYMBOL, IMPEXP, DRIVER, GENPROCEDURE, VERIFYPROCEDURE, IMPORTPROCEDURE, AUTOPROCESS, AUTOEXCHANGE, MANUALINIT, CONFIRMPATH, NEEDSIGN)
    VALUES ('EDE_FEDEX_ORDER', 1, 'EXP_FEDEX_XML', 'EDE_FEDEX_ORDER_REQUEST', NULL, 'EDE_FEDEX_ORDERNO_RESPONSE', 0, 1, 0, 0, 0)
    returning ref into ederule;
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'SERVICEURL','PROD');
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'ACCESSCODE','');
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'FILEPATH','.\pdf\fedex');
  INSERT INTO EDERULES (SYMBOL, IMPEXP, DRIVER, GENPROCEDURE, VERIFYPROCEDURE, IMPORTPROCEDURE, AUTOPROCESS, AUTOEXCHANGE, MANUALINIT, CONFIRMPATH, NEEDSIGN)
    VALUES ('EDE_FEDEX_SHIPPINGDOCLABEL', 1, 'EXP_FEDEX_XML', 'EDE_FEDEX_SHIPPLABEL_REQUEST', '', 'EDE_FEDEX_SHIPPLABEL_RESPONSE', 0, 1, 0, 0, 0)
    returning ref into ederule;
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'SERVICEURL','PROD');
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'ACCESSCODE','');
  INSERT INTO EDERULES (SYMBOL, IMPEXP, DRIVER, GENPROCEDURE, VERIFYPROCEDURE, IMPORTPROCEDURE, AUTOPROCESS, AUTOEXCHANGE, MANUALINIT, CONFIRMPATH, NEEDSIGN)
    VALUES ('EDE_FEDEX_SHIPMENT', 1, 'EXP_FEDEX_XML', 'EDE_FEDEX_SHIPMENT_REQUEST', '', 'EDE_FEDEX_SHIPMENT_RESPONSE', 0, 1, 0, 0, 0)
    returning ref into ederule;
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'SERVICEURL','PROD');
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'ACCESSCODE','');
  INSERT INTO EDERULES (SYMBOL, IMPEXP, DRIVER, GENPROCEDURE, VERIFYPROCEDURE, IMPORTPROCEDURE, AUTOPROCESS, AUTOEXCHANGE, MANUALINIT, CONFIRMPATH, NEEDSIGN)
    VALUES ('EDE_FEDEX_ADDCUSTOMER', 1, 'EXP_FEDEX_XML', 'EDE_FEDEX_ADDCUSTOMER_REQUEST', '', 'EDE_FEDEX_ADDCUSTOMER_RESPONSE', 0, 1, 0, 0, 0)
    returning ref into ederule;
  INSERT INTO EDERULES (SYMBOL, IMPEXP, DRIVER, GENPROCEDURE, VERIFYPROCEDURE, IMPORTPROCEDURE, AUTOPROCESS, AUTOEXCHANGE, MANUALINIT, CONFIRMPATH, NEEDSIGN)
    VALUES ('EDE_FEDEX_DOCLABEL', 1, 'EXP_FEDEX_XML', 'EDE_FEDEX_DOCLABEL_REQUEST', '', 'EDE_FEDEX_DOCLABEL_RESPONSE', 0, 1, 0, 0, 0)
    returning ref into ederule;
  INSERT INTO EDERULES (SYMBOL, IMPEXP, DRIVER, GENPROCEDURE, VERIFYPROCEDURE, IMPORTPROCEDURE, AUTOPROCESS, AUTOEXCHANGE, MANUALINIT, CONFIRMPATH, NEEDSIGN)
    VALUES ('EDE_FEDEX_ORDERNO', 1, 'EXP_FEDEX_XML', 'EDE_FEDEX_ORDERNO_REQUEST', '', 'EDE_FEDEX_ORDERNO_RESPONSE', 0, 1, 0, 0, 0)
    returning ref into ederule;
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'SERVICEURL','PROD');
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'ACCESSCODE','');
  INSERT INTO EDERULES (SYMBOL, IMPEXP, DRIVER, GENPROCEDURE, VERIFYPROCEDURE, IMPORTPROCEDURE, AUTOPROCESS, AUTOEXCHANGE, MANUALINIT, CONFIRMPATH, NEEDSIGN)
    VALUES ('EDE_FEDEX_ALLTRACKING', 1, 'EXP_FEDEX_XML', 'EDE_FEDEX_ALLTRACKING_REQUEST', '', 'EDE_FEDEX_ALLTRACKING_RESPONSE', 0, 1, 0, 0, 0)
    returning ref into ederule;
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'SERVICEURL','PROD');
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'ACCESSCODE','');
  INSERT INTO EDERULES (SYMBOL, IMPEXP, DRIVER, GENPROCEDURE, VERIFYPROCEDURE, IMPORTPROCEDURE, AUTOPROCESS, AUTOEXCHANGE, MANUALINIT, CONFIRMPATH, NEEDSIGN)
    VALUES ('EDE_FEDEX_LASTTRACKING', 1, 'EXP_FEDEX_XML', 'EDE_FEDEX_LASTTRACKING_REQUEST', '', 'EDE_FEDEX_LASTTRACKING_RESPONSE', 0, 1, 0, 0, 0)
    returning ref into ederule;
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'SERVICEURL','PROD');
  insert into ederuleparams(ederule,name,val)
    values (:ederule,'ACCESSCODE','');
  INSERT INTO EDERULES (SYMBOL, IMPEXP, DRIVER, GENPROCEDURE, VERIFYPROCEDURE, IMPORTPROCEDURE, AUTOPROCESS, AUTOEXCHANGE, MANUALINIT, CONFIRMPATH, NEEDSIGN)
    VALUES ('EDE_FEDEX_PACKAGELABEL', 1, 'EXP_FEDEX_XML', 'EDE_FEDEX_PACKAGELABEL_REQUEST', '', 'EDE_FEDEX_PACKAGELABEL_RESPONSE', 0, 1, 0, 0, 0)
    returning ref into ederule;

  -- insert sposobu dostawcy dla spedytora
  insert into sposdost (NAZWA, KOSZT, PROG, OPIS, CZAS, WITRYNA, USLUGA, TRASY, LISTYWYS, STATE, TOKEN, LP, AFTERACKACTION)
    values ('FEDEX', 0, 0, '', 0, 0, null, 0, 1, 0, 9, 0, 'EDE_FEDEX_SHIPMENT_REQUEST')
    returning REF into spdost;

  -- operacje
  INSERT INTO LISTYWYSDDEFOPER (NAZWA, PIKTOGRAM, AKCJA, ZAKRES, ORD, SPOSDOST)
    VALUES ('-', NULL, NULL, 3, 1, NULL);
  INSERT INTO LISTYWYSDDEFOPER (NAZWA, PIKTOGRAM, AKCJA, ZAKRES, ORD, SPOSDOST)
    VALUES ('FEDEX - nadanie przesyłki', NULL, 'EDE_FEDEX_SHIPMENT_REQUEST', 4, 1, :spdost);
  INSERT INTO LISTYWYSDDEFOPER (NAZWA, PIKTOGRAM, AKCJA, ZAKRES, ORD, SPOSDOST)
    VALUES ('FEDEX - pobranie etykiet', NULL, 'EDE_FEDEX_SHIPLABEL_REQUEST', 4, 1, :spdost);
  INSERT INTO LISTYWYSDDEFOPER (NAZWA, PIKTOGRAM, AKCJA, ZAKRES, ORD, SPOSDOST)
    VALUES ('FEDEX - status przesyłki', NULL, 'EDE_FEDEX_ALLTRACKING_REQUEST', 4, 1, :spdost);

  -- spedytor
  insert into spedytorzy(SYMBOL, NAZWA)
    values ('FEDEX', 'Fedex');
  
  -- powiazanie spedytora ze sposobem dostawy
  insert into spedytwys(SPEDYTOR,SPOSDOST)
  values ('FEDEX',:spdost);

  -- konta spedytorow dla kazdego oddzialu
  for
    select o.oddzial
      from oddzialy o
      into :oddzial
  do begin
    insert into spedytkonta (SPEDYTOR, ODDZIAL, SCIEZKAPLIKU,TYP)
      values ('FEDEX', :oddzial, '.\labels\fedex','TEST');
  end
end^
SET TERM ; ^
