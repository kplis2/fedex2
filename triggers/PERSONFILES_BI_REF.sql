--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PERSONFILES_BI_REF FOR PERSONFILES                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('PERSONFILES')
      returning_values new.REF;
end^
SET TERM ; ^
