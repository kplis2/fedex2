--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MENUWAR_BU_REPLICAT FOR MENUWAR                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.cecha <> old.cecha or (new.cecha is null and old.cecha is not null) or (new.cecha is not null and old.cecha is null) or
     new.zal <> old.zal or (new.zal is null and old.zal is not null) or (new.zal is not null and old.zal is null) or
     new.war1 <> old.war1 or (new.war1 is null and old.war1 is not null) or (new.war1 is not null and old.war1 is null) or
     new.war2 <> old.war2 or (new.war2 is null and old.war2 is not null) or (new.war2 is not null and old.war2 is null)
  )
    then
      execute procedure REPLICAT_STATE('MENUWAR',new.state, new.ref, NULL, NULL, NULL) returning_values new.state;

end^
SET TERM ; ^
