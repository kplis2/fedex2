--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_AU_CPL FOR CPODMIOTY                      
  ACTIVE AFTER UPDATE POSITION 6 
as
begin
  if((new.aktywny <> old.aktywny) or (new.nazwa <> old.nazwa) or (new.skrot <> old.skrot) or (new.nazwafirmy <> old.nazwafirmy)) then begin
    update CPLUCZEST set AKT = new.aktywny, SKROT = new.skrot, NAZWA = new.nazwa, NAZWAFIRMY = new.nazwafirmy, CSTATUS = new.cstatus where CPODMIOT = new.ref and PKOSOBA is null;
    update CPLUCZEST set AKT = new.aktywny, SKROT = new.skrot, NAZWAFIRMY = new.nazwa, CSTATUS = new.cstatus where CPODMIOT = new.ref and PKOSOBA is not null;
  end
end^
SET TERM ; ^
