--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FXDASSETS_BI0 FOR FXDASSETS                      
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable kodln smallint;
  declare variable isintasset smallint;
  declare variable numberg varchar(40);
begin
  if (new.tvallim is null) then new.tvallim = 0;
  if (new.amortgrp = '') then new.amortgrp = null;
  if (new.name = '') then new.name = null;
  if (new.symbol = '') then new.symbol = null;
  if (new.tbksymbol = '') then new.tbksymbol = null;
  if (new.fbksymbol = '') then new.fbksymbol = null;
  if (new.oddzial = '') then new.oddzial = null;
  if (new.tredemption is null) then new.tredemption = 0;
  if (new.fredemption is null) then new.fredemption = 0;
  if (new.tvalue is null) then new.tvalue = 0;
  if (new.fvalue is null) then new.fvalue = 0;

  if (new.amortgrp is not null) then
  begin
    select isintasset from amortgrp where symbol = new.amortgrp into :isintasset;
    new.isintasset = isintasset;
  end
  new.amgrp = substring(new.amortgrp from 1 for 1);
 
 select kodln from slodef where typ='FXDASSETS'
    into :kodln;

  if (kodln is not null and coalesce(char_length(new.symbol),0)<>kodln) then  -- [DG] XXX ZG119346
    exception BAD_LENGTH_FKKOD;

  new.tvaluegross = new.tvalue;
  new.fvaluegross = new.fvalue;
  new.tredemptiongross = new.tredemption;
  new.fredemptiongross = new.fredemption;

  execute procedure GETCONFIG('NUMBERGEN_ASSETS') returning_values :numberg;
  if (numberg <> '') then begin
    if (new.isintasset = 0) then
      execute procedure get_number(:numberg,'',new.amgrp,new.purchasedt, 0, new.ref) returning_values new.num, new.symbol;
    else execute procedure get_number(:numberg,'W',new.amgrp,new.purchasedt, 0, new.ref) returning_values new.num, new.symbol;
  end
end^
SET TERM ; ^
