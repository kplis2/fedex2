--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_BU_LBASE_FBASE FOR EABSENCES                      
  ACTIVE BEFORE UPDATE POSITION 2 
as
declare variable period varchar(6);
declare variable sbase numeric(14,2);
declare variable takeit smallint;
declare variable vbase numeric(14,2);
declare variable workdays integer;
declare variable vworkeddays integer;
declare variable sworkeddays integer;
declare variable sbasegross numeric(14,2);
declare variable vworkedhours numeric(14,2);
begin
--Aktualizacja zakresu podstaw pracowniczych przypisanych do nieobecnosci

  if (new.lbase <> old.lbase or new.fbase <> old.fbase or
      new.employee <> old.employee or new.baseabsence <> old.baseabsence) then
  begin
  --nie stwierdzono ciaglosci podstaw:
    if (new.baseabsence = new.ref) then
    begin
    --gdy zmiana pracownika, to czyscimy historie rozliczenia (BS56609)
      if (new.employee <> old.employee) then
      begin
        delete from eabsencebases
          where employee = old.employee and eabsence = new.baseabsence;
      end

    --przypisz do nieobecnosci biezace podstawy
      for
        select period, sbase, takeit, vbase, workdays, vworkeddays, sworkeddays,
               sbasegross, vworkedhours
          from eabsemplbases
          where employee = new.employee
            and new.fbase <= period
            and period <= new.lbase
          into :period, :sbase, :takeit, :vbase, :workdays, :vworkeddays, :sworkeddays,
               :sbasegross, :vworkedhours
      do begin
      --przypisz szukana podstawe w przypadku jej braku
        if (not exists (select first 1 1 from eabsencebases
                          where eabsence = new.baseabsence
                            and employee = new.employee
                            and period = :period)
        ) then begin
          insert into eabsencebases (eabsence, employee, period, sbase, takeit,
            vbase, workdays, vworkeddays, sworkeddays, sbasegross, vworkedhours)
          values (new.baseabsence, new.employee, :period, :sbase, :takeit,
            :vbase, :workdays, :vworkeddays, :sworkeddays, :sbasegross, :vworkedhours);
        end
      end
    end

  --tabela podstaw do nieobecnosci ma przechowywac tylko podstawy z zadanego zakresu
    delete from eabsencebases
      where eabsence = new.baseabsence
        and not (new.fbase <= period and period <= new.lbase);
  end
end^
SET TERM ; ^
