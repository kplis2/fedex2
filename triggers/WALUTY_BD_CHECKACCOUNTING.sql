--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WALUTY_BD_CHECKACCOUNTING FOR WALUTY                         
  ACTIVE BEFORE DELETE POSITION 0 
AS
  declare variable dictdef integer;
begin
  select ref from slodef where typ = 'WALUTY'
    into :dictdef;
  execute procedure check_dict_using_in_accounts(dictdef, old.symbol, null);
end^
SET TERM ; ^
