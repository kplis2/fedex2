--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CAKCJEPOZ_BI FOR CAKCJEPOZ                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable status smallint;
BEGIN
  select status from CAKCJENAG where REF=new.cakcja into :status;
  if(:status <> 0) then exception CAKCJEPOZ_AKCJACLOSED;
END^
SET TERM ; ^
