--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INT_IMP_ZAM_POZYCJE_BI FOR INT_IMP_ZAM_POZYCJE            
  ACTIVE BEFORE INSERT POSITION 5 
as
begin
  if (new.datains is null) then
    new.datains = current_timestamp(0);
  if (new.dataprzetw is null and new.dataostprzetw is not null) then
    new.dataprzetw = new.dataostprzetw;
  if (new.error is null) then
    new.error = 0;
  if (new.errorzalezne is null) then
    new.errorzalezne = 0;
end^
SET TERM ; ^
