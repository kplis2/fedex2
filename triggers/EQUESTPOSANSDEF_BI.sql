--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EQUESTPOSANSDEF_BI FOR EQUESTPOSANSDEF                
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_equestposansdef,1);
end^
SET TERM ; ^
