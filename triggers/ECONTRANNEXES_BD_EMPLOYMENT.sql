--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRANNEXES_BD_EMPLOYMENT FOR ECONTRANNEXES                  
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
/*MWr(PR24342) Przeniesienie zmian na przebieg zatrudnienia po usunieciu aneksu/oddelegowania.
  Trigger ma sie wykonac tylko w przypadku, kiedy rekord nie jest usuwany przez trigger na update*/

  if (coalesce(old.mflag,0) = 0) then
  begin
    if (exists(select first 1 1 from emplcontracts --PR60202
                 where ref = old.emplcontract and empltype = 1)
    ) then
      execute procedure update_employment_bd_annexe(old.emplcontract, old.fromdate, old.todate, old.atype, old.ref);
  end
end^
SET TERM ; ^
