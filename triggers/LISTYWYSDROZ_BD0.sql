--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDROZ_BD0 FOR LISTYWYSDROZ                   
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (exists(select first 1 1 from listywysdroz_opk where listwysd = old.listywysd and nropk = old.nrkartonu and stan = 1)) then
    exception universal 'Nie można usuwać towaru z zamknietego opakowania.';
end^
SET TERM ; ^
