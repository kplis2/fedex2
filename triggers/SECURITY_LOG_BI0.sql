--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SECURITY_LOG_BI0 FOR SECURITY_LOG                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.key1 is null) then new.key1 = '';
  if(new.key2 is null) then new.key2 = '';
  if(new.key3 is null) then new.key3 = '';
  if(new.key4 is null) then new.key4 = '';
  if(new.key5 is null) then new.key5 = '';
  if (new.logfil is null) then new.logfil = '';
  if (new.timestmp is null) then new.timestmp = current_timestamp(0);
  if(new.operator <=0) then new.operator = null;
  if (new.operator is null) then
    execute procedure get_global_param ('AKTUOPERATOR') returning_values new.operator;
  if(new.operator is not null) then
    select OPERATOR.nazwa from OPERATOR where REF=new.operator into new.opername;
end^
SET TERM ; ^
