--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDS_AU_WHAERAS_ORD FOR MWSSTANDS                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable cnt smallint;
declare variable number integer;
declare variable wharearef integer;
begin
  -- szukam obszarow z regalów ustawionych przed zakladanym i biore max. numer
  if (new.number = old.number) then
    exit;
  cnt = 0;
  -- szukam obszarow z regalów ustawionych przed zakladanym i biore max. numer
  select max(number)
    from whareas
    where whsecrow = new.whsecrow
      and mwsstandnumber < new.number
    into :number;
  if (number is null) then
    number = 0;
  -- zmiana kolejnosci obszarow po zmianie kolejnosci regalow
  -- trzeba sprawdzic czy mozna zalozyc nowy obszar czy jest tu na przyklad obszar nie zwiazany z regalami, np logistyczny
  if (exists (select first 1 ref from whareas where number = :number + 1 and whsecrow = new.whsecrow
      and (mwsstandseg is null or areatype = 1))) then
    number = number + 1;
  -- zmiana kolejnosci obszarów razem ze zmiana kolejnosci segmentow regalu
  for
    select ref from whareas w where w.mwsstand = new.ref order by w.ref
      into wharearef
  do begin
    cnt = cnt + 1;
    update whareas set number = :number + :cnt where ref = :wharearef;
  end
end^
SET TERM ; ^
