--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRCALCCOLS_AU_EXECUTE FOR PRCALCCOLS                     
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable ret numeric(16,6);
declare variable parent integer;
begin
  -- jesli jest to wyrazenie
  if(new.calctype=0) then begin
    -- jesli uleglo zmianie wyrazenie lub rezygnujemy z wartosci obliczonej recznie to na nowo je przelicz
    if((coalesce(new.expr,'')<>coalesce(old.expr,'') or (old.modified=1 and new.modified=0) or (new.excluded<>old.excluded) )
      and new.modified=0 and new.calctype=0
    ) then begin
      execute procedure PRSHCALCS_SUBEXECUTE(new.ref) returning_values :ret;
      update prcalccols set cvalue=coalesce(:ret,0) where ref=new.ref;
    end
    -- jesli zmienila sie wartosc to przelicz w gore
    if(coalesce(new.cvalue,0)<>coalesce(old.cvalue,0) and new.parent is not null and new.calctoparent = 1) then
    begin
      execute procedure PRSHCALCS_SUBEXECUTE(new.parent) returning_values :ret;
      update prcalccols set cvalue=coalesce(:ret,0) where ref=new.parent;
    end
    --przeliczenie zaleznych komurek w obrebie bierzacej kalkulacji (komurki pobierane przez metode #COL())
    if (coalesce(new.cvalue,0) <> coalesce(old.cvalue,0) and new.parent is not null) then
    begin
      for
        select parent from prcalccols where fromprcalccolpar = new.ref and calctype = 1 and modified = 0
          into :parent
      do begin
        execute procedure prshcalcs_subexecute(:parent) returning_values :ret;
        update prcalccols set cvalue = coalesce(:ret,0) where ref = :parent;
      end
    end
    -- jesli zmienila sie wartosc TKW to przelicz kalkulacje zalezne od tej
    if(coalesce(new.cvalue,0)<>coalesce(old.cvalue,0) and new.parent is null) then
    begin
      for select parent from prcalccols where fromprcalccol=new.ref and calctype=1 and modified=0
      into :parent
      do begin
        execute procedure PRSHCALCS_SUBEXECUTE(:parent) returning_values :ret;
        update prcalccols set cvalue=:ret where ref=:parent;
      end
    end
  end

  -- jesli jest to parametr
  if(new.calctype=1) then begin
    -- jesli parametr ulegl zmianie recznie lub rezygnujemy z wartosci obliczonej recznie to przelicz na nowo
    if(((coalesce(new.cvalue,0)<>coalesce(old.cvalue,0) and new.modified=1) or
        (old.modified=1 and new.modified=0)
       ) and new.parent is not null) then begin
      execute procedure PRSHCALCS_SUBEXECUTE(new.parent) returning_values :ret;
      update prcalccols set cvalue=coalesce(:ret,0) where ref=new.parent;
    end
  end
end^
SET TERM ; ^
