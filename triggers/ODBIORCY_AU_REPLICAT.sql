--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ODBIORCY_AU_REPLICAT FOR ODBIORCY                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if((new.ref <> old.ref)
   or (new.klient <> old.klient)
   or (new.nazwa <> old.nazwa)
   or (new.dulica <> old.dulica) or (new.dulica is not null and old.dulica is null) or (new.dulica is null and old.dulica is not null)
   or (new.dmiasto <> old.dmiasto ) or (new.dmiasto is not null and old.dmiasto is null) or (new.dmiasto is null and old.dmiasto is not null)
   or (new.dkodp <> old.dkodp ) or (new.dkodp is not null and old.dkodp is null) or (new.dkodp is null and old.dkodp is not null)
   or (new.dpoczta <> old.dpoczta ) or (new.dpoczta is not null and old.dpoczta is null) or (new.dpoczta is null and old.dpoczta is not null)
   or (new.dtelefon <> old.dtelefon ) or (new.dtelefon is not null and old.dtelefon is null) or (new.dtelefon is null and old.dtelefon is not null)
   or (new.onwww <> old.onwww ) or (new.onwww is not null and old.onwww is null) or (new.onwww is null and old.onwww is not null)
   or (new.email <> old.email ) or (new.email is not null and old.email is null) or (new.email is null and old.email is not null)
   or (new.www <> old.www ) or (new.www is not null and old.www is null) or (new.www is null and old.www is not null)
   or (new.cpwoj16m <> old.cpwoj16m ) or (new.cpwoj16m is not null and old.cpwoj16m is null) or (new.cpwoj16m is null and old.cpwoj16m is not null)
  ) then begin
    update KLIENCi set STATE = -1 where REF=new.klient;
    if(new.klient <> old.klient) then
      update KLIENCi set STATE = -1 where REF=old.klient;
  end
end^
SET TERM ; ^
