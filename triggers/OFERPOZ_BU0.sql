--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OFERPOZ_BU0 FOR OFERPOZ                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable vrabat ceny;
declare variable stawka ceny;
declare variable eksport smallint;
begin
  if (new.ukryta is null) then new.ukryta = 0;
  if(new.dostawca = 0) then new.dostawca = null;
  if(new.faza=0) then new.faza = null;
  if(new.rozdzial=0) then new.rozdzial = null;
  if(new.miara = '') then new.miara = null;
  if(new.ktm = '') then new.ktm = null;
  if(new.jedn = 0) then new.jedn = null;
  if(new.wersjaref = 0) then new.wersjaref = null;
  if (coalesce(new.walcen,'') = '') then
  begin
    select o.waluta
      from oferty o
      where o.ref = new.oferta
      into new.walcen;
    if (coalesce(new.walcen,'') = '') then
      execute procedure get_config('WALPLN',2) returning_values new.walcen;
  end
  -- wyliczanie cen z uwzglednieniem rabatu
  select stawka from vat where grupa = new.gr_vat into :stawka;
  select coalesce(rabatzewn,0),eksport from oferty where ref = new.oferta into :vrabat,:eksport;
  vrabat = vrabat + new.rabat;
  new.cenanet = new.cenacen * (100 - :vrabat) / 100;
  if(:eksport=1) then begin
    new.cenabru = new.cenanet;
  end else begin
    new.cenabru = new.cenanet * (100 + :stawka) / 100;
  end
  new.wartnet = new.cenanet * new.ilosc;
  new.wartbru = new.wartnet * (100 + :stawka) / 100;
  new.vat = new.wartbru - new.wartnet;
  new.wartzak = new.cena_zakn * new.ilosc;
  new.marza = new.wartnet - new.wartzak;
end^
SET TERM ; ^
