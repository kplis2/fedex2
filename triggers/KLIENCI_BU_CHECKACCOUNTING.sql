--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_BU_CHECKACCOUNTING FOR KLIENCI                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable dictdef integer;
begin
  if (new.kontofk <> old.kontofk) then
  begin
    select ref from slodef where typ = 'KLIENCI'
      into :dictdef;
    execute procedure check_dict_using_in_accounts(dictdef, old.kontofk, old.company);
  end
end^
SET TERM ; ^
