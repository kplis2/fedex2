--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TYPYKLI_BU_FIELD_CHECK FOR TYPYKLI                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if ((coalesce(new.opis,'') <> coalesce(old.opis,''))
    and (coalesce(new.opis,'') = '')) then
  begin
    exception universal 'Podaj opis kontrahenta.';
  end
  if ((coalesce(replace(new.slodefs, ';', ''),'') <> coalesce(replace(old.slodefs, ';', ''),''))
    and (coalesce(replace(new.slodefs, ';', ''),'') = '')) then
  begin
    exception universal 'Wybierz grupe kontrahenta.';
  end
end^
SET TERM ; ^
