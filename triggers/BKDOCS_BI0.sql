--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_BI0 FOR BKDOCS                         
  ACTIVE BEFORE INSERT POSITION 1 
as
  declare variable status int;
  declare variable bkreg varchar(10);
  declare variable kind smallint;
  declare variable aktuoperator integer;
begin
  new.regdate = current_timestamp(0);

  if (new.period is null) then
  begin
    select id from bkperiods
    where sdate <= cast(new.transdate as date) and fdate >= cast(new.transdate as date) and ptype = 1 and company = new.company
    into new.period;
  end
  
  if (new.autobo is null) then new.autobo = 0;
  if (new.vatperiod = '') then new.vatperiod = null;
  if (new.vatreg = '') then new.vatreg = null;
  if (new.currency is null or new.currency = '') then
    execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2) returning_values new.currency;
  if (new.number is null) then
    execute procedure BKDOCS_GETNUMBER(new.company, new.bkreg, new.period) returning_values new.number;
  
  if (new.vatreg is not null and new.vnumber is null) then
  begin
    select max(vnumber) from bkdocs where company = new.company and vatreg = new.vatreg and period=new.period
    into new.vnumber;
    if (new.vnumber is null) then new.vnumber = 0;
    new.vnumber = new.vnumber + 1;
  end
  
  select status from BKPERIODS where id = new.period and company = new.company
    into :status;
  if (status <> 1) then exception PERIOD_NOT_OPENED;

  select bkreg, kind
    from bkdoctypes where ref = new.doctype
    into :bkreg, :kind;

  if (kind = 0) then new.vat = 0;
  else new.vat = 1;
  if (bkreg <> new.bkreg) then exception BAD_DOCTYPE;
  if (kind > 0 and new.vatreg is null) then exception VATTYPE_DOC;

  select rights, rightsgroup
  from bkregs where symbol = new.bkreg and company = new.company
  into new.rights, new.rightsgroup;

  if (new.vatperiod is not null) then begin
    execute procedure get_global_param ('AKTUOPERATOR')
      returning_values aktuoperator;
    new.vatoper = :aktuoperator;
    new.vatoperdate = current_timestamp(0);
  end

  if (new.costdistribution is not null) then
    select cdtype from costdistribution where ref = new.costdistribution
      into new.cdtype;

  --Jeżeli jestemy korektą dokumentu VAT to należy przepisać symbol
  if (coalesce(new.bkdocsvatcorrectionref,0)>0) then
    select b.symbol
      from bkdocs b
      where b.ref = new.bkdocsvatcorrectionref
      into new.bkdocsvatcorrectionsymbol;
  --Jeszcze do skopiowania BKVATCORRECTIONTYPE z typu
  select b.bkvatcorrectiontype
    from bkdoctypes b
    where b.ref = new.doctype
    into new.bkvatcorrectiontype;
  new.bkdocsvatcorrectionsymbol = coalesce(new.bkdocsvatcorrectionsymbol,0);
end^
SET TERM ; ^
