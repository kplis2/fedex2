--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDES_BI FOR PRSCHEDGUIDES                  
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable blocked smallint;
begin
  if (new.prschedguideorg is null) then new.prschedguideorg = new.ref;
  select schedblocked from prschedzam pz where pz.ref = new.prschedzam into :blocked;
  if(new.schedblocked is null) then new.schedblocked = blocked;
  if(new.amount is null) then new.amount = 0;
  if (new.amountdecl is null) then
    select batchquantity from prsheets where ref = new.prsheet into new.amountdecl;
  if(new.amountzreal is null) then new.amountzreal = 0;
  if(new.stan is null or (new.stan = '') or (new.stan = ' ')) then new.stan = 'N';
  if(new.kstan is null or (new.kstan = '') or (new.kstan = ' ')) then new.kstan = 'N';
  if(new.lastone is null) then new.lastone = 0;
  if(new.pggamountcalc is null) then new.pggamountcalc = 0;
  if(new.statusmat is null) then new.statusmat = 0;
  if(new.status is null) then new.status = 0;
  if (new.amountshortagedecl is null) then new.amountshortagedecl = 0;
  if (new.amountshortage is null) then new.amountshortage = 0;
  if (new.kamountshortage is null) then new.kamountshortage = 0;
  if (new.prpreprod is null) then new.prpreprod = 0;
  if (new.anulowano is null) then new.anulowano = 0;
  select kstan, stan
     from NAGZAM
     where ref=new.zamowienie
     into new.kstan, new.stan;
  select p.magazyn
    from pozzam p
    where p.ref = new.pozzam
    into new.warehouse;
  select nazwa from towary where ktm = new.ktm into new.ktmname;
  select n.prpriority
    from nagzam n
    where n.ref = new.zamowienie
    into new.prpriority;
  if (new.prguidepriority is null) then
    new.prguidepriority = new.prpriority;
end^
SET TERM ; ^
