--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAKNOT_BI_REF FOR NAGFAKNOT                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('NAGFAKNOT')
      returning_values new.REF;
end^
SET TERM ; ^
