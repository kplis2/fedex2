--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECOLLDEDINSTALS_BIU_BLANK FOR ECOLLDEDINSTALS                
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.insdate is null and (inserting or new.insdate is distinct from old.insdate)) then
     new.insdate = current_timestamp(0);
end^
SET TERM ; ^
