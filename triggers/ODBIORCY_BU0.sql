--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ODBIORCY_BU0 FOR ODBIORCY                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable cnt integer;
declare variable skrotkl varchar(40);
begin
  if(new.gl is null) then new.gl = 0;
  if(new.dulica is null) then new.dulica = '';
  if(new.dmiasto is null) then new.dmiasto = '';
  if(new.dpoczta is null) then new.dpoczta = '';
  if(new.dkodp is null) then new.dkodp = '';
  if(new.nazwafirmy is null) then new.nazwafirmy = '';
  if(new.nazwa is null) then new.nazwa = '';
  if(new.nazwa='' and coalesce(old.nazwa,'')<>coalesce(new.nazwa,'')) then
    exception ODBIORCY_PUSTA_NAZWA;
  if(new.gl = 0) then begin
    select count(*) from ODBIORCY where KLIENT= new.KLIENT and ref <> new.ref into :cnt;
    if(:cnt is null or (:cnt = 0)) then
      new.gl = 1;
  end
  if(new.odbiorcaspedyc is null and old.odbiorcaspedyc is not null) then
    new.odbiorcaspedycdesc = '';
  else if((new.odbiorcaspedyc <> old.odbiorcaspedyc) or (new.odbiorcaspedyc is not null and old.odbiorcaspedyc is null))then
    select NAZWAFIRMY||' '|| NAZWA from ODBIORCY where ref=new.odbiorcaspedyc into new.odbiorcaspedycdesc;
  if (new.klient <> old.klient and new.klient is not null) then
    select k.company from klienci k where k.ref = new.klient into new.company;
end^
SET TERM ; ^
