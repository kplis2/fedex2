--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDROZ_AU0 FOR LISTYWYSDROZ                   
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable ilosc numeric(14,4);
begin
  select sum(lwr.iloscmag) from listywysdroz lwr
    where lwr.listywysdpoz = new.listywysdpoz
  into :ilosc;

  update listywysdpoz set iloscspk = :ilosc
    where ref = new.listywysdpoz;
end^
SET TERM ; ^
