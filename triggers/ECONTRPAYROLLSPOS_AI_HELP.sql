--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRPAYROLLSPOS_AI_HELP FOR ECONTRPAYROLLSPOS              
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable cnt integer;
declare variable prlist integer;
declare variable operatorassign integer;
declare variable operatorhelp integer;
declare variable operassname varchar(60);
declare variable operhelpname varchar(80);
declare variable operpayroll integer;
declare variable operpayrolnag integer;
declare variable jobtime numeric(14,2);
declare variable pph numeric(14,2);
declare variable points numeric(14,2);
declare variable amount numeric(14,2);
declare variable moneyperpoint numeric(14,2);
declare variable bdate date;
declare variable edate date;
begin
  if (new.epresencelistspos is not null and new.econtractsdef is not null
      and new.accord = 0) then
  begin
    select count(*)
      from econtractsassign
      where econtractsassign.econtractsdef = new.econtractsdef
      into :cnt;
    if (:cnt = 1) then
    begin
      select first 1 econtractsassign.operator, econtractsassign.moneyperpoint
        from econtractsassign
        where econtractsassign.econtractsdef = new.econtractsdef
        into :operatorassign, :moneyperpoint;
      if (:operatorassign is null) then
        exit;
      select epresencelistnag.epresencelist, epresencelistspos.jobtimeq, epresencelistspos.operator
        from epresencelistspos
          join epresencelistnag on (epresencelistnag.ref = epresencelistspos.epresencelistnag)
        where epresencelistspos.ref = new.epresencelistspos
        into :prlist, :jobtime, :operatorhelp;
      if (:operatorassign = :operatorhelp) then
        exit;
      if (exists (select epresencelistspos.ref from epresencelistspos
            join epresencelistnag on (epresencelistnag.ref = epresencelistspos.epresencelistnag)
          where epresencelistnag.epresencelist = :prlist and epresencelistnag.operator = :operatorassign
            and epresencelistspos.econtractsdef = new.econtractsdef and epresencelistspos.accord = 1
            and epresencelistnag.wastoday = 1)) then
      begin
        if (not exists (select ref from econtrpayrollsnag
            where econtrpayrollsnag.econtrpayroll = new.econtrpayrolls
            and econtrpayrollsnag.operator = :operatorassign)) then
        begin
          select operator.nazwa
            from operator
            where operator.ref = :operatorassign
            into :operassname;
          exception universal 'Brak listy dla '||:operatorassign;
        end else
        begin
          select econtrpayrollsnag.ref, econtrpayrollsnag.econtrpayroll
            from econtrpayrollsnag
            where econtrpayrollsnag.econtrpayroll = new.econtrpayrolls
            and econtrpayrollsnag.operator = :operatorassign
            into :operpayrolnag, :operpayroll;
          select datatookres.fday, datatookres.lday
            from datatookres(cast(new.sdate as date),0,-1,0,1)
            into :bdate, :edate;
          execute procedure CALCULATE_ACCORDPOINTS(:operatorassign,:bdate,:edate)
            returning_values :pph;
          if (:pph < 600) then
            pph = 600;
          points = -1*(:pph * :jobtime);
          amount = :points * :moneyperpoint;
          select operator.nazwa
            from operator
            where operator.ref = :operatorhelp
            into :operhelpname;
          operhelpname = :operhelpname||' pomoc '||new.ssymbol;
          -- pomagal przy konkretnych dokumentach
          if ((new.stable = 'LISTYWYSD' or new.stable = 'DOKUMNAG') and new.sref > 0) then
          begin
            delete from econtrpayrollspos where econtrpayrollspos.epresencelistsposhelp = new.epresencelistspos
              and econtrpayrollspos.econtrpayrollnag = :operpayrolnag and econtrpayrollspos.econtractsdef = new.econtractsdef
              and econtrpayrollspos.econtractsposdef = new.econtractsposdef and econtrpayrollspos.stable = new.stable
              and econtrpayrollspos.sref = new.sref;
            insert into econtrpayrollspos (epresencelistsposhelp, countnag,econtrpayrolls, econtrpayrollnag, accord, stable, sref, ssymbol, points, topay,
                econtractsdef, econtractsposdef, sdate)
              values (new.epresencelistspos, 0, :operpayroll, :operpayrolnag, 0, new.stable,new.sref,substring(:operhelpname from 1 for 40), :points, :amount,
                  new.econtractsdef, new.econtractsposdef, new.sdate);
          end else
          begin
            delete from econtrpayrollspos where econtrpayrollspos.epresencelistsposhelp = new.epresencelistspos
              and econtrpayrollspos.econtrpayrollnag = :operpayrolnag and econtrpayrollspos.econtractsdef = new.econtractsdef
              and econtrpayrollspos.econtractsposdef = new.econtractsposdef;
            insert into econtrpayrollspos (epresencelistsposhelp, countnag,econtrpayrolls, econtrpayrollnag, accord, stable, sref, ssymbol, points, topay,
                econtractsdef, econtractsposdef, sdate)
              values (new.epresencelistspos, 0, :operpayroll, :operpayrolnag, 0, new.stable,new.sref,substring(:operhelpname from 1 for 40), :points, :amount,
                  new.econtractsdef, new.econtractsposdef, new.sdate);
          end
        end
      end else
        exit;
    end else
      exit;
  end else
    exit;
end^
SET TERM ; ^
