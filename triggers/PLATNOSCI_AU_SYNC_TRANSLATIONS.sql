--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PLATNOSCI_AU_SYNC_TRANSLATIONS FOR PLATNOSCI                      
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if (new.opis is distinct from old.opis) then
    execute procedure SYNC_SYS_TRANSLATIONS('PLATNOSCI', new.nazwadruk, new.opis);
end^
SET TERM ; ^
