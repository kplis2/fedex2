--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMAILWIAD_AU0 FOR EMAILWIAD                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
    if (new.stan > 0 and old.stan = 0 and old.folder = new.folder) then
        update DEFFOLD set nieprzeczytane = (select count(ref) from EMAILWIAD where folder = new.folder and stan = 0) where ref = new.folder;
    else if (new.stan = 0 and old.stan = 1 and old.folder = new.folder) then
        update DEFFOLD set nieprzeczytane = (select count(ref) from EMAILWIAD where folder = new.folder and stan = 0) where ref = new.folder;
    else if ((new.stan = 0 or (old.stan = 0)) and old.folder <> new.folder) then begin
        update DEFFOLD set nieprzeczytane = (select count(ref) from EMAILWIAD where folder = new.folder and stan = 0) where ref = new.folder;
        update DEFFOLD set nieprzeczytane = (select count(ref) from EMAILWIAD where folder = old.folder and stan = 0) where ref = old.folder;
    end
    if(new.folder <> old.folder) then begin
      execute procedure emailwiad_calc_maxuid(old.folder);
      execute procedure emailwiad_calc_maxuid(new.folder);
      if(old.iid > 0) then
        update EMAILWIAD set IID = IID - 1 where FOLDER = old.folder and IID > old.iid;            
    end else if(coalesce(new.uid,'') <> coalesce (old.uid,''))then
      execute procedure emailwiad_calc_maxuid(new.folder);
end^
SET TERM ; ^
