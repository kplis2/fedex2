--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVREQUESTS_BI_AUTONR FOR SRVREQUESTS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable nazwa varchar(40);
declare variable rejestr varchar(40);
declare variable dokument varchar(40);
declare variable symbol varchar(40);
declare variable autonr integer;
begin
  select ct.numbergen, ct.symbol, ct.autonr  from contrtypes ct where ct.ref = new.contrtype
  into :nazwa, :dokument, :autonr;
  if (:autonr = 1) then
  begin
    if (new.srvcontract is not null) then
      select sct.symbol
        from srvcontracts sct where sct.ref = new.srvcontract
      into :rejestr;
    else
      rejestr = '';
    execute procedure get_number(:nazwa, substring(:rejestr from 1 for 20), substring(:dokument from 1 for 20), current_date, null, new.ref)
      returning_values new.number, :symbol;
    new.symbol = substring(:symbol from 1 for 20);
  end
end^
SET TERM ; ^
