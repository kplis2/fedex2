--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CHANGE_DOMAIN_TABLE_BI_REF FOR CHANGE_DOMAIN_TABLE            
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CHANGE_DOMAIN_TABLE')
      returning_values new.REF;
end^
SET TERM ; ^
