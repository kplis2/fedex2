--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FSBALANCES_BU0 FOR FSBALANCES                     
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (old.dictgrp is null and new.dictgrp is not null) then
    select max(grpdebit), max(grpcredit)
      from fsbalances where dictgrp = new.dictgrp and company = new.company and currency = new.currency
      into new.grpdebit, new.grpcredit;

  if (old.dictgrp is not null and new.dictgrp is null) then
  begin
    new.grpdebit = new.debit;
    new.grpcredit = new.credit;

  end
  if (new.btranamountbank is null) then new.btranamountbank = 0;
  if (new.grpbtranamountbank is null) then new.grpbtranamountbank = 0;
  new.grpdebit = coalesce(new.grpdebit, 0);
  new.grpcredit = coalesce(new.grpcredit, 0);

end^
SET TERM ; ^
