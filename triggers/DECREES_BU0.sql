--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_BU0 FOR DECREES                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable yearid int;
  declare variable settl smallint;
  declare variable res smallint;
  declare variable settl_curr varchar(3);
  declare variable dist1 integer;
  declare variable dist2 integer;
  declare variable dist3 integer;
  declare variable dist4 integer;
  declare variable dist5 integer;
  declare variable dist6 integer;
  declare variable ptype smallint;
  declare variable summadists numeric(14,2);
  declare variable sumwndists numeric(14,2);
  declare variable multivaluedist smallint;
  declare variable country_currency varchar(3);
  declare variable currvalue numeric(14,2);
begin
  if (new.autobo = 1 and (new.account <> old.account or new.side <> old.side
    or new.debit <> old.debit or new.credit <> old.credit
    or new.settlement <> old.settlement)) then
    exception fk_autobo;

  if (new.opendate is not null and new.payday is not null
      and new.opendate>new.payday) then
    exception ROZRACH_DATA_OTW ;
  select yearid from bkperiods where company = new.company and id = new.period into :yearid;
  if (new.credit is null) then new.credit = 0;
  if (new.debit is null) then new.debit = 0;
  if (new.currcredit is null) then new.currcredit = 0;
  if (new.currdebit is null) then new.currdebit = 0;
  if (new.sscredit is null) then new.sscredit = 0;
  if (new.ssdebit is null) then new.ssdebit = 0;
  if (new.sscurrcredit is null) then new.sscredit = 0;
  if (new.sscurrdebit is null) then new.ssdebit = 0;

  if (new.settlement = '') then
    new.settlement = null;
  if (new.settlement is not null and new.stransdate is null) then
    new.stransdate = new.transdate;

  execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
      returning_values :country_currency;

  if (new.currency is null or new.currency='') then
    new.currency = country_currency;
  if (new.sscurrency is null or new.sscurrency='') then
    new.sscurrency = country_currency;

  select ref from bkaccounts
    where symbol = substring(new.account from 1 for 3) and yearid = :yearid
      and company = new.company
    into new.accref;

  if (new.status=0 and old.status=0 and new.settlement<>old.settlement) then
  begin
    select settl from bkaccounts where ref = new.accref and company = new.company into :settl;
    if (settl = 2 and (new.settlement = '' or new.settlement is null)
        and new.autodoc <> 1 and new.autobo <> 1) then
      exception FK_SETTLEMENT_IS_EMPTY new.account || ' - konto rozrachunkowe, wypełnij rozrachunek!';
  end
  if (old.status = 0 and new.status > 0) then
  begin
    execute procedure check_account(new.company, new.account, yearid, new.autodoc)
      returning_values res, dist1, dist2, dist3, dist4, dist5, dist6, multivaluedist;

    if(multivaluedist = 1) then begin
      dist1 = null;
      dist2 = null;
      dist3 = null;
      dist4 = null;
      dist5 = null;
      dist6 = null;
    end
    if (res = 1) then
    begin
      select settl from bkaccounts
        where ref = new.accref
          and company = new.company
      into :settl;
      if (settl = 2) then
       if ((new.settlement = '' or new.settlement is null) and new.autobo <> 1) then
          if (new.autodoc=1) then
            new.status=old.status;
          else
            exception FK_SETTLEMENT_IS_EMPTY new.account || ' - konto rozrachunkowe, wypełnij rozrachunek!';
      if (settl = 0) then
        if (new.settlement <> '' and new.settlement is not null) then
          new.settlement = null;
      if (new.settlement is not null) then
      begin
        settl_curr = null;
        select waluta from rozrach
          where company = new.company and kontofk = new.account and symbfak=new.settlement
          into :settl_curr;
        if (settl_curr is not null and settl_curr <> new.currency) then
          if (new.autodoc = 1) then
            new.status=old.status;
          else
            exception BAD_CURENCY_SETTLMENT 'Waluta dekretu niezgodna z walutą rozrachunku! (' || new.settlement || ')';
      end

      select ptype from bkperiods where id = new.period and company = new.company
        into :ptype;

      if (ptype = 1) then
      begin
        --sprawdzenie wyroznikow
        if (dist1 is not null  and (new.dist1ddef is null or new.dist1symbol is null
            or new.dist1symbol = '')) then
        begin
          if (new.autodoc=1) then
            new.status=old.status;
          else
            exception DECREES_EXPT 'Nieuzupełnione wyróżniki dla pozycji ' || new.number;
        end
        if (dist2 is not null  and (new.dist2ddef is null or new.dist2symbol is null
            or new.dist2symbol = '')) then
        begin
          if (new.autodoc=1) then
            new.status=old.status;
          else
            exception DECREES_EXPT 'Nieuzupełnione wyróżniki dla pozycji ' || new.number;
        end
        if (dist3 is not null  and (new.dist3ddef is null or new.dist3symbol is null
            or new.dist3symbol = '')) then
        begin
          if (new.autodoc=1) then
            new.status=old.status;
          else
            exception DECREES_EXPT 'Nieuzupełnione wyróżniki dla pozycji ' || new.number;
        end
        if (dist4 is not null  and (new.dist4ddef is null or new.dist4symbol is null
            or new.dist4symbol = '')) then
        begin
          if (new.autodoc=1) then
            new.status=old.status;
          else
            exception DECREES_EXPT 'Nieuzupełnione wyróżniki dla pozycji ' || new.number;
        end
        if (dist5 is not null  and (new.dist5ddef is null or new.dist5symbol is null
            or new.dist5symbol = '')) then
        begin
          if (new.autodoc=1) then
            new.status=old.status;
          else
            exception DECREES_EXPT 'Nieuzupełnione wyróżniki dla pozycji ' || new.number;
        end
        if (dist6 is not null  and (new.dist6ddef is null or new.dist6symbol is null
            or new.dist6symbol = '')) then
        begin
          if (new.autodoc=1) then
            new.status=old.status;
          else
            exception DECREES_EXPT 'Nieuzupełnione wyróżniki dla pozycji ' || new.number;
        end

        -- 5015
        if (dist1 is null  and (new.dist1symbol is not null
            and new.dist1symbol <> '')) then
        begin
          if(multivaluedist = 1) then
             new.dist1symbol = null;
          else if (new.autodoc=1) then
            new.status=old.status;
          else
            exception DECREES_EXPT 'Niepotrzebne wyróżniki na pozycji ' || new.number;
        end
        if (dist2 is null  and (new.dist2symbol is not null
            and new.dist2symbol <> '')) then
        begin
          if(multivaluedist = 1) then
             new.dist2symbol = null;
          else if (new.autodoc=1) then
            new.status=old.status;
          else
            exception DECREES_EXPT 'Niepotrzebne wyróżniki na pozycji ' || new.number;
        end
        if (dist3 is null  and (new.dist3symbol is not null
            and new.dist3symbol <> '')) then
        begin
          if(multivaluedist = 1) then
             new.dist3symbol = null;
          else if (new.autodoc=1) then
            new.status=old.status;
          else
            exception DECREES_EXPT 'Niepotrzebne wyróżniki na pozycji ' || new.number;
        end
        if (dist4 is null  and (new.dist4symbol is not null
            and new.dist4symbol <> '')) then
        begin
          if(multivaluedist = 1) then
             new.dist4symbol = null;
          else if (new.autodoc=1) then
            new.status=old.status;
          else
            exception DECREES_EXPT 'Niepotrzebne wyróżniki na pozycji ' || new.number;
        end
        if (dist5 is null  and (new.dist5symbol is not null
            and new.dist5symbol <> '')) then
        begin
          if(multivaluedist = 1) then
             new.dist5symbol = null;
          else if (new.autodoc=1) then
            new.status=old.status;
          else
            exception DECREES_EXPT 'Niepotrzebne wyróżniki na pozycji ' || new.number;
        end
        if (dist6 is null  and (new.dist6symbol is not null
            and new.dist6symbol <> '')) then
        begin
          if(multivaluedist = 1) then
             new.dist6symbol = null;
          else if (new.autodoc=1) then
            new.status=old.status;
          else
            exception DECREES_EXPT 'Niepotrzebne wyróżniki na pozycji ' || new.number;
        end
        select coalesce(sum(credit),0),coalesce(sum(debit),0) from distpos where decrees = new.ref into :sumwndists,:summadists;
        if (multivaluedist = 1 and ((new.credit <> :sumwndists) or (new.debit <> :summadists))) then
        begin
          if (new.autodoc=1) then
            new.status=old.status;
          else
            exception DECREES_EXPT 'Suma na wyróżnikach nie zgadza się z sumą dekretu na pozycji ' || new.number;
        end

        --sprawdzanie wypelnienia wyroznikow wielowartosciowych
        if (multivaluedist = 1)then
        begin
          if (exists(select d.ref
                      from distpos d
                      where d.decrees = new.ref and
                        (d.tempdictdef is not null and coalesce(d.tempsymbol,'') = ''
                        or d.tempdictdef2 is not null and coalesce(d.tempsymbol2,'') = ''
                        or d.tempdictdef3 is not null and coalesce(d.tempsymbol3,'') = ''
                        or d.tempdictdef4 is not null and coalesce(d.tempsymbol4,'') = ''
                        or d.tempdictdef5 is not null and coalesce(d.tempsymbol5,'') = ''
                        or d.tempdictdef6 is not null and coalesce(d.tempsymbol6,'') = '')
          )) then
          begin
            if (new.autodoc = 1) then
              new.status = old.status;
            else
              exception DECREES_EXPT 'Nieuzupełnione wyróżniki dla pozycji ' || new.number;
          end
          if (exists(select d.ref
                      from distpos d
                      where d.decrees = new.ref and
                        (d.tempdictdef is null and coalesce(d.tempsymbol,'') <> ''
                        or d.tempdictdef2 is null and coalesce(d.tempsymbol2,'') <> ''
                        or d.tempdictdef3 is null and coalesce(d.tempsymbol3,'') <> ''
                        or d.tempdictdef4 is null and coalesce(d.tempsymbol4,'') <> ''
                        or d.tempdictdef5 is null and coalesce(d.tempsymbol5,'') <> ''
                        or d.tempdictdef6 is null and coalesce(d.tempsymbol6,'') <> '')
          )) then
          begin
            if (new.autodoc = 1) then
              new.status = old.status;
            else
              exception DECREES_EXPT 'Niepotrzebne wyróżniki na pozycji ' || new.number;
          end
        end
      end
    end else
      new.status = old.status;
  end

  --zablokowanie mozliwosci zmian niektorych danych na zaakceptowanym dokumencie
  if ((coalesce(new.period,'')<>coalesce(old.period,'') or coalesce(new.account,'')<> coalesce(old.account,'')
       or coalesce(new.credit,0)<>coalesce(old.credit,0) or coalesce(new.debit,0)<>coalesce(old.debit,0)
       or coalesce(new.currcredit,0)<>coalesce(old.currcredit,0) or coalesce(new.currdebit,0)<>coalesce(old.currdebit,0)
       or coalesce(new.currency,'')<>coalesce(old.currency,'')) and old.status>0 ) then
     exception BKDOCS_ZAKSIEGOWANY 'Nie można dokonywać zmian na zaakceptowanym dokumencie.';

  -- obsluzenie wyjatku gdy MA i WINIEN jest = 0 dla wielowartosciowych wyroznikow
  if (new.credit = 0 and new.debit = 0 and (new.account <> old.account or new.company <> old.company)) then begin
    execute procedure check_account(new.company, new.account, yearid, 1)
      returning_values res, dist1, dist2, dist3, dist4, dist5, dist6, multivaluedist;
    if (res = 1) then
    begin
      if (coalesce(multivaluedist,0) <> 1) then
        exception decrees_expt 'Kwota dekretu musi być różna od 0!';
    end
  end

  if (country_currency <> new.currency and
      (new.currdebit<>old.currdebit or new.currcredit<>old.currcredit or new.rate<>old.rate)) then
  begin
    if (new.currdebit <> 0 ) then
      currvalue = new.currdebit;
    if (new.currcredit <> 0 ) then
      currvalue = new.currcredit;
    if (currvalue<>0) then begin
      select descript from bkdocs
        where ref = new.bkdoc
        into new.descript;
      new.descript = substring(cast(currvalue as varchar(255)) || ' ' || new.currency ||  ' x ' || cast(new.rate as varchar(255)) || ' - ' || new.descript from 1 for 80);
    end
  end
end^
SET TERM ; ^
