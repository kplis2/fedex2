--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LOG_FILE_BI_REF FOR LOG_FILE                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('LOG_FILE')
      returning_values new.REF;
end^
SET TERM ; ^
