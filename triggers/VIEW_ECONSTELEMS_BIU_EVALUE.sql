--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VIEW_ECONSTELEMS_BIU_EVALUE FOR VIEW_ECONSTELEMS               
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (coalesce(new.evalue, 0) = 0) then exception ECONSTELEMNS_EVALUE;
end^
SET TERM ; ^
