--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLPOZZAM_BI0 FOR CPLPOZZAM                      
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if((new.NAGRODA<>0) and (new.NAGRODA is not NULL)) then
    select ILPKT from CPLNAGRODY where REF=new.NAGRODA into new.CENAPKT;
  else
    new.CENAPKT = 0;
  new.ILPKT = new.ILOSC*new.CENAPKT;
end^
SET TERM ; ^
