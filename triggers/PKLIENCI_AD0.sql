--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PKLIENCI_AD0 FOR PKLIENCI                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable slodef integer;
begin
  if(old.crm = 1) then begin
     select ref from SLODEF where TYP='PKLIENCI' into :slodef;
     if(:slodef is null) then exception SLODEF_BEZPKLIENCI;
     delete from CPODMIOTY where SLODEF = :slodef and SLOPOZ = old.ref;
  end
end^
SET TERM ; ^
