--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDLEVELTYPES_BI0 FOR MWSSTANDLEVELTYPES             
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.l is null) then new.l = 0;
  if (new.w is null) then new.w = 0;
  if (new.h is null) then new.h = 0;
  if (new.levelheight is null) then new.levelheight = 0;
  if (new.loadcapacity is null) then new.loadcapacity = 0;
  if (new.l is not null and new.w is not null and new.h is not null) then
    new.volume = new.l * new.h * new.w / 1000000;
  if (new.mwsconstloctype is not null) then
    select maxpallocq from mwsconstloctypes where ref = new.mwsconstloctype
      into new.maxpallocq;
end^
SET TERM ; ^
