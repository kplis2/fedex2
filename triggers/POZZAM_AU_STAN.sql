--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_AU_STAN FOR POZZAM                         
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable termdost date;
declare variable termstart date;
declare variable ilosc numeric(14,4);
declare variable oldilosc numeric(14,4);
declare variable status integer;
declare variable reff integer;
declare variable typ integer;
declare variable wydania integer;
declare variable onmainschedule integer;
begin
  if(new.ord = 0) then begin
    --TODO: jesli do pozycji są wygenerowane przewodniki na harmonogrami eglownym, to blokuje sobie samodzielne generowanie rezerwacji
     select wydania, TYP, cast(TERMDOST as date), cast(TERMstart as date), REF, ONMAINSCHEDULE
       from NAGZAM join typzam on (typzam.symbol = nagzam.typzam)
       where nagzam.ref=new.zamowienie
       into :wydania, :typ, :termdost, :termstart, :reff, :onmainschedule;
     if(:termdost is null) then termdost = current_date;
     if(:termstart is null) then termstart = :termdost;
     if(:wydania = 3 and new.stan <> 'D' and old.stan <> 'D')then-- pozycja nie-przychodowa, wiec czas rezerwacji to czas rozpoczecia zamowienia
       termdost = :termstart;
     if (:wydania = 3 and :onmainschedule = 0) then
     begin
       if(new.ktm <> old.ktm or new.wersja <> old.wersja or new.magazyn <> old.magazyn) then
       begin
         if (new.out in (1,2)) then
           update PRSCHEDGUIDESPOS set KTM = new.ktm, wersja = new.wersja, WAREHOUSE = new.magazyn
             where pozzamref = new.ref;
         else if (new.out = 0) then
           update prschedguides set WAREHOUSE = new.magazyn, ktm = new.ktm, wersja = new.wersja
             where pozzam = new.ref;
       end
     end
     else if(:wydania = 3 and :onmainschedule = 1) then
     begin
       if(new.stan <> old.stan or (new.stan is not null and old.stan is null)
         or (new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.magazyn <> old.magazyn)
       ) then
       begin
         if (new.out in (1,2)) then
           update PRSCHEDGUIDESPOS set STAN = new.stan,  KTM = new.ktm, wersja = new.wersja, WAREHOUSE = new.magazyn
             where pozzamref = new.ref;
         else if (new.out = 0) then
           update prschedguides set WAREHOUSE = new.magazyn, stan = new.stan, ktm = new.ktm, wersja = new.wersja
             where pozzam = new.ref;
       end
     end else if(new.stan = 'N' and old.stan = 'N') then begin
       -- cokolwiek bysmy nie zmieniali to nie wplywa to na rezerwacje i blokady
     end else begin
       if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or
           ((new.cenamag <> old.cenamag) or (new.cenamag is null and old.cenamag is not null) or (new.cenamag is not null and old.cenamag is null)) or
           ((new.dostawamag <> old.dostawamag) or (new.dostawamag is null and old.dostawamag is not null) or (new.dostawamag is not null and old.dostawamag is null)) or
           (coalesce(old.magazyn,'') <> coalesce(new.magazyn,'')) or
           (new.stan <> old.stan and old.stan <> 'N' and old.stan is not null))
       then begin
           -- nastepuje przerezerwowanie calej pozycji
           -- zdjecie blokad/rezerwacji na starych zasadach
           ilosc = old.iloscm;
           execute procedure REZ_SET_KTM(old.zamowienie, old.ref, old.ktm, old.wersja, 0,old.cenamag, old.dostawamag, 'N',:termdost) returning_values :status;
           if(:status <> 1) then exception REZ_WRONG;
           -- zalozenie blokad/rezerwacji od nowa
           if(:typ >=2) then ilosc = new.ilrealm;
           else ilosc = new.iloscm - new.ILONKLONM;
           if(new.blokadarez = 1) then
             ilosc = 0;
           if(:ilosc > 0) then begin
             execute procedure REZ_SET_KTM(new.zamowienie, new.ref, new.ktm, new.wersja,:ilosc, new.cenamag, new.dostawamag, new.stan, :termdost) returning_values :status;
             if(:status <> 1) then exception REZ_WRONG;
           end
       end else if(
           (new.stan <> 'N' and old.stan <> new.stan and old.stan is not null)
            or (((new.ilosc <> old.ilosc) or (new.ilonklon <> old.ilonklon)
                  or (new.iloscm <> old.iloscm) or (new.ilonklonm <> old.ilonklonm)
                ) and :typ <=1
               )
            or ( (new.ilreal <> old.ilreal)
                 and :typ>=2
               )
          )then begin
           if(:typ >=2) then begin
              oldilosc = old.ilrealm;
              ilosc = new.ilrealm;
           end else begin
              oldilosc = old.iloscm - old.ilonklonm;
              ilosc = new.iloscm - new.ilonklonm;
           end
           if(((:ilosc <>:oldilosc) or (old.ilzrealm <> new.ilzrealm) or (new.stan <> old.stan)) and new.blokadarez = 0) then begin
             execute procedure REZ_SET_KTM(new.zamowienie, new.ref, new.ktm, new.wersja,:ilosc, new.cenamag, new.dostawamag, new.stan, :termdost) returning_values :status;
             if(:status <> 1) then exception REZ_WRONG;
           end
       end
     end
     if(new.ilosc <> old.ilosc) then begin
       execute procedure NAGZAM_CHECK_ZREAL(new.zamowienie);
     end
  end
end^
SET TERM ; ^
