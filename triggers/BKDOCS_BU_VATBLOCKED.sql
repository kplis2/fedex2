--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_BU_VATBLOCKED FOR BKDOCS                         
  ACTIVE BEFORE UPDATE POSITION 10 
AS
begin
  if (coalesce(new.vatperiod,'') != coalesce(old.vatperiod,'')) then begin
    if (coalesce(old.vatperiod, '') != '') then
      if (exists(select p.id from bkperiods p where p.id = old.vatperiod and p.company = new.company and  p.vatblocked = 1)) then
        exception bkperiods_vatblocked;
    if (coalesce(new.vatperiod, '') != '') then
      if (exists(select p.id from bkperiods p where p.id = new.vatperiod and p.company = new.company and  p.vatblocked = 1)) then
        exception bkperiods_vatblocked;
  end
end^
SET TERM ; ^
