--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_AD_SLOPOZ FOR CPODMIOTY                      
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable slotyp varchar(40);
begin
  select TYP from SLODEF where ref = old.slodef into :slotyp;
  if(:slotyp = 'ESYSTEM') then update SLOPOZ set CRM=0 where ref = old.slopoz;
end^
SET TERM ; ^
