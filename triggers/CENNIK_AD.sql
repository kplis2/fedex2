--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CENNIK_AD FOR CENNIK                         
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable symbol varchar(20);
declare variable oddzial varchar(10);
begin
  symbol = NULL;

  select SYMBOL, ODDZIAL from DEFCENNIK where REF=old.cennik into :symbol, :oddzial;
  if(:symbol='A') then
    update STANYIL set CENANETA=NULL, CENABRUA=NULL where WERSJAREF=old.wersjaref and ODDZIAL = :oddzial;
  if(:symbol='B') then
    update STANYIL set CENANETB=NULL, CENABRUB=NULL where WERSJAREF=old.wersjaref and ODDZIAL = :oddzial;
end^
SET TERM ; ^
