--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EFUNDS_BIU_DOUBLED_FUND FOR EFUNDS                         
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (exists(select f.ref
    from efunds f
    where f.employee = new.employee
      and f.fundtype = new.fundtype
      and f.ref <> new.ref)) then
  exception doubled_fund;
end^
SET TERM ; ^
