--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ETRAINNEEDS_BU_COST FOR ETRAINNEEDS                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable enumber numeric(14,2);
begin
  if (new.costtype <> old.costtype or (new.costtype = 0 and new.personcost <> old.personcost)
    or (new.costtype = 1 and new.totalcost <> old.totalcost)) then
  begin
    select count(ref)
      from empltrainneeds
      where etrainneed = new.ref
      into :enumber;

    enumber = coalesce(:enumber, 0);

    if (new.costtype = 0) then
      new.totalcost = new.personcost * :enumber;
    else if (new.costtype = 1 and :enumber > 0) then
      new.personcost = new.totalcost / :enumber;
    else if (new.costtype = 1 and :enumber = 0) then
      new.personcost = 0;
  end
end^
SET TERM ; ^
