--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECREDITS_BU0 FOR ECREDITS                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
-- przeliczenie aktualnej wartosci kredytu przy zmianie wartosci poczatkowej
  if (new.initvalue <> old.initvalue) then
    new.actvalue = new.initvalue - (old.initvalue - old.actvalue);
end^
SET TERM ; ^
