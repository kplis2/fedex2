--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_AU_SYNCHRO FOR KLIENCI                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable procedura varchar(1024);
declare variable synchro smallint;
declare variable d integer;
declare variable d2 integer;
begin
 select konfig.wartosc from konfig where konfig.akronim = 'SYNCHROKLIENT'
    into :synchro;
  if ((synchro = 1 or synchro = 2) and new.dostawca is not null ) then
  begin
    if (new.synchro is null and old.synchro is null) then
    begin
      procedura = 'update dostawcy set';
      d = coalesce(char_length(:procedura),0);  -- [DG] XXX ZG119346
      if (new.nazwa <> old.nazwa)
        then procedura = :procedura ||' nazwa = '''||new.nazwa||''',';
      if (new.fskrot <> old.fskrot)
        then procedura = :procedura ||' id = '''||new.fskrot||''',';
      if (new.kontofk <> old.kontofk)
        then procedura = :procedura ||' KONTOFK = '''||new.kontofk||''',';
      if (coalesce(new.miasto,'') <> coalesce(old.miasto,''))
        then procedura = :procedura ||' MIASTO = '''||coalesce(new.miasto,'')||''',';
      if (coalesce(new.kodp,'') <> coalesce(old.kodp,''))
        then procedura = :procedura ||' KODP = '''||coalesce(new.kodp,'')||''',';
      if (coalesce(new.ulica,'') <> coalesce(old.ulica,''))
        then procedura = :procedura ||' ULICA = '''||coalesce(new.ulica,'')||''',';
      if (coalesce(new.nrdomu,'') <> coalesce(old.nrdomu,''))
        then procedura = :procedura ||' NRDOMU = '''||coalesce(new.nrdomu,'')||''',';
      if (coalesce(new.nrlokalu,'') <> coalesce(old.nrlokalu,''))
        then procedura = :procedura ||' NRLOKALU = '''||coalesce(new.nrlokalu,'')||''',';
      if (coalesce(new.telefon,'') <> coalesce(old.telefon,''))
        then procedura = :procedura ||' TELEFON = '''||coalesce(new.telefon,'')||''',';
      if (coalesce(new.fax,'') <> coalesce(old.fax,''))
        then procedura = :procedura ||' FAX = '''||coalesce(new.fax,'')||''',';
      if (coalesce(new.email,'') <> coalesce(old.email,''))
        then procedura = :procedura ||' EMAIL = '''||coalesce(new.email,'')||''',';
      if (coalesce(new.www,'') <> coalesce(old.www,''))
        then procedura = :procedura ||' WWW = '''||coalesce(new.www,'')||''',';
      if (new.aktywny <> old.aktywny)
        then procedura = :procedura ||' AKTYWNY = '''||new.aktywny||''',';
      if (coalesce(new.poczta,'') <> coalesce(old.poczta,''))
        then procedura = :procedura ||' POCZTA = '''||coalesce(new.poczta,'')||''',';
      if (new.kraj <> old.kraj)
        then procedura = :procedura ||' KRAJ = '''||new.kraj||''',';
      if (new.nip <> old.nip)
        then procedura = :procedura ||' NIP = '''||new.nip||''',';
      if (coalesce(new.regon,'') <> coalesce(old.regon,''))
        then procedura = :procedura ||' REGON = '''||new.regon||''',';
      if (new.firma <> old.firma)
        then procedura = :procedura ||' FIRMA = '''||new.firma||''',';
      if (new.typ <> old.typ)
        then procedura = :procedura ||' TYP = '''||new.typ||''',';
      if (new.kodzewn <> old.kodzewn)
        then procedura = :procedura ||' KODZEWN = '''||new.kodzewn||''',';
      if (new.powiazany <> old.powiazany)
        then procedura = :procedura ||' POWIAZANY = '''||new.powiazany||''',';
      if (new.waluta <> old.waluta)
        then procedura = :procedura ||' WALUTA = '''||new.waluta||''',';
      if (coalesce(new.bank,'') <> coalesce(old.bank,''))
        then if (new.bank is null) then
          procedura = :procedura ||' BANK = NULL,';
        else
          procedura = :procedura ||' BANK = '''||new.bank||''',';
      if (coalesce(new.rachunek,'') <> coalesce(old.rachunek,''))
        then procedura = :procedura ||' RACHUNEK = '''||new.rachunek||''',';
      if (coalesce(new.cpowiat,0) <> coalesce(old.cpowiat,0))then
        if (new.cpowiat is null) then
          procedura = :procedura ||' cpowiat = NULL,';
        else
          procedura = :procedura ||' cpowiat = '||new.cpowiat||',';
      d2 = coalesce(char_length(:procedura),0);  -- [DG] XXX ZG119346
      if (d2 > d) then
      begin
        procedura = :procedura ||' SYNCHRO = 1 where dostawcy.ref = '
          ||cast(new.dostawca as varchar (60));
        execute statement procedura;
      end
    end
    if (new.synchro = 1) then begin
      update klienci set klienci.synchro = null where klienci.ref = new.ref;
    end
  end
end^
SET TERM ; ^
