--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLOPOZ_BI0 FOR SLOPOZ                         
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable crm integer;
  declare variable ka smallint;
  declare variable an smallint;
  declare variable kodln smallint;
  declare variable emptykodallow smallint;
  declare variable isdist smallint;
begin
 if(new.akt is null) then new.akt = 1;
 select KARTOTEKA, ANALITYKA, KODLN, isdist
   from SLODEF where REF=new.slownik
   into :ka, :an, :kodln, :isdist;
 if (ka = 0 and (an = 1 or isdist = 1)) then
   new.kod = new.kontoks;

 if (an=1 and coalesce(char_length(new.kontoks),0)<>:kodln) then -- [DG] XXX ZG119346
     exception fk_error_synlen;

 if (new.crm is null) then new.crm = 0;
 if (new.crm = 1) then begin
   select crm from SLODEF where ref = new.slownik into :crm;
   if (:crm is null or (:crm = 0)) then new.crm = 0;
 end
end^
SET TERM ; ^
