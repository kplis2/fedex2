--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_TOWARY_BI_DANEDOD FOR TOWARY                         
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.x_mws_typ_lokacji is null) then new.x_mws_typ_lokacji=0;
  if (new.x_mws_partie is null) then new.x_mws_partie=0;
  if (new.x_mws_serie is null) then new.x_mws_serie=0;     
  if (new.x_mws_slownik_ehrle is null) then new.x_mws_slownik_ehrle=0;
end^
SET TERM ; ^
