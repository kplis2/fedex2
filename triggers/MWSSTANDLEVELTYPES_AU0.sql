--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDLEVELTYPES_AU0 FOR MWSSTANDLEVELTYPES             
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable maxcap numeric(14,2);
declare variable maxvol numeric(14,2);
begin
  if (new.loadcapacity <> old.loadcapacity or new.volume <> old.volume) then
  begin
    select sum(l.loadcapacity), sum(l.volume)
      from mwsstandleveltypes l
        join mwsstandsegtypes s on (s.mwsstandtype = l.mwsstandtype)
      where l.mwsstandtype = new.mwsstandtype
      into :maxcap, :maxvol;
    update mwsstandtypes set loadcapacity = :maxcap, volume = :maxvol
      where ref = new.mwsstandtype;
  end
  if (new.w <> old.w or new.l <> old.l or new.h <> old.h
      or new.loadcapacity <> old.loadcapacity or new.levelheight <> old.levelheight
      or new.volume <> old.volume) then
    update mwsstandlevels set loadcapacity = new.loadcapacity, volume = new.volume,
       l = new.l, h = new.h, w = new.w, levelheight = new.levelheight
      where mwsstandleveltype = new.ref;
end^
SET TERM ; ^
