--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTUREDISTS_BI_WPK FOR ACCSTRUCTUREDISTS              
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable company_slodef companies_id;
begin
  -- Jeżeli skopiowaliśmy z WPK konto i stworzylimy kopie slownikow analitycznych
  -- oraz jezeli pozycje mialy dowiazane pozycje wyroznikowe to należy sprawdzić,
  -- czy nie trzena skopiowac slownika wyróznikowego. Lub jest to wyróżnik
  -- na bkaccount ze wzorca
  company_slodef = null;
  if (new.pattern_ref is distinct from null) then
  begin
   select company
      from slodef s
      where s.ref = new.distdef
      into :company_slodef;
    if (company_slodef is distinct from null) then
    begin
      execute procedure copy_slodef_slopoz_dists(new.distdef, new.pattern_ref)
        returning_values new.distdef;
    end
  end else
  begin
    if (new.otable = 'BKACCOUNTS') then
    begin
      if (exists (select first 1 1 from bkaccounts b where b.ref = new.oref and coalesce(b.pattern_ref,0)>0)) then
        exception universal'Konto syntetyczne synchronizowane, brak możliwości dodania wyróżnika z poza wzorca';
    end else if (new.otable = 'SLOPOZ') then
    begin
      if (exists (select first 1 1 from slopoz b where b.ref = new.oref and coalesce(b.pattern_ref,0)>0)) then
        exception universal'Wartoć synchronizowana, brak możliwości dodania wyróżnika z poza wzorca';
    end
  end
end^
SET TERM ; ^
