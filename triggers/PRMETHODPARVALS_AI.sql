--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRMETHODPARVALS_AI FOR PRMETHODPARVALS                
  ACTIVE AFTER INSERT POSITION 0 
as
begin
  if (new.fromdate <= current_date
    and not exists (select first 1 1 from PRMETHODPARVALS p where p.method = new.method
      and p.prmethodpar = new.prmethodpar and p.fromdate >= new.fromdate and p.fromdate <= current_date
      and p.rdb$db_key <> new.rdb$db_key)
  ) then
    update prcalcpars p set p.pvalue = new.pvalue
      where p.symbol = new.prmethodpar and p.partype = 'G'
        and exists (select first 1 1 from  prshcalcs c
          where c.ref = p.calc and c.method = new.method);
end^
SET TERM ; ^
