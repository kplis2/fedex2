--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_BD_DISPOSITION FOR DOKUMPOZ                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if(1 = (select first 1 d.mwsdisposition from dokumnag d where d.ref = old.dokument)) then
  begin
    if(1 < (select first 1 m.status from mwsacts m where m.docid = old.dokument and m.docposid = old.ref and m.doctype = 'M')) then
      exception dokumpoz_disposition;
    else
    begin
      update mwsacts set status = 0
        where docid = old.dokument and docposid = old.ref and doctype = 'M';
      delete from mwsacts where docid = old.dokument and docposid = old.ref and doctype = 'M';
    end
  end
end^
SET TERM ; ^
