--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLOPOZ_AU0 FOR SLOPOZ                         
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable slodefcrm integer;
declare variable dimhier integer;
declare variable currentyear integer;
begin
  -- aktulizacja opisu kont ksiegowych dla bieżącego roku
  if(old.nazwa <> new.nazwa) then begin
    currentyear = extract(year from current_timestamp(0));
    execute procedure accounting_recalc_descript(new.slownik, :currentyear, null, new.kontoks);
  end

  if(new.crm = 0 and old.crm = 1) then
     delete from CPODMIOTY where SLODEF = old.slownik and SLOPOZ = old.ref;
  if(new.crm = 1 and old.crm = 0) then begin
    select crm from slodef where ref=new.slownik into :slodefcrm;
    if(:slodefcrm=1) then
      insert into CPODMIOTY(SLODEF, SLOPOZ, AKTYWNY, SKROT, NAZWA, KONTOFK) values(new.slownik, new.ref, 1, new.kod, new.nazwa, new.kontoks);
  end
  -- synchronizacja elementów sownika z elementami wymiarów, w których sownik jest wskazany na definicji wymiaru
  -- a ma zaznaczoną autosynchronizacje ze slownikiem
  for
    select dimhierdef.ref
      from dimhierdef
      where dimhierdef.slodef = new.slownik and dimhierdef.elemsaddmeth = 2
      into :dimhier
  do begin
    update dimelemdef  set dimelemdef.shortname = new.kod, dimelemdef.name = new.nazwa, dimelemdef.act = new.akt
      where dimelemdef.dimhier = :dimhier and dimelemdef.slodef = new.slownik and  dimelemdef.slopoz = new.ref;
  end
end^
SET TERM ; ^
