--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_AI_DISTS FOR DECREES                        
  ACTIVE AFTER INSERT POSITION 0 
as
begin
  if ((new.dist1ddef is not null and coalesce(new.dist1symbol, '') <> '')
       or (new.dist2ddef is not null and coalesce(new.dist2symbol, '') <> '')
       or (new.dist3ddef is not null and coalesce(new.dist3symbol, '') <> '')
       or (new.dist4ddef is not null and coalesce(new.dist4symbol, '') <> '')
       or (new.dist5ddef is not null and coalesce(new.dist5symbol, '') <> '')
       or (new.dist6ddef is not null and coalesce(new.dist6symbol, '') <> '')) then
    insert into distpos (number, tempdictdef, tempsymbol, tempdictdef2, tempsymbol2,tempdictdef3, tempsymbol3,
                          tempdictdef4, tempsymbol4, tempdictdef5, tempsymbol5, tempdictdef6, tempsymbol6,
                          decrees, bkdoc, period, account, debit, credit, transdate, company, status)
      values (1, new.dist1ddef, new.dist1symbol,new.dist2ddef, new.dist2symbol,new.dist3ddef, new.dist3symbol,
               new.dist4ddef, new.dist4symbol, new.dist5ddef, new.dist5symbol, new.dist6ddef, new.dist6symbol,
               new.ref, new.bkdoc, new.period, new.account, new.debit, new.credit, new.transdate, new.company, new.status);
end^
SET TERM ; ^
