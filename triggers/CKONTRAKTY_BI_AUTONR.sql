--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKONTRAKTY_BI_AUTONR FOR CKONTRAKTY                     
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable nazwa varchar(40);
declare variable contrtype varchar(40);
declare variable symbol varchar(40);
declare variable autonr smallint_id;
begin
  select ct.numbergen, ct.symbol, ct.autonr
    from contrtypes ct where ct.ref = new.contrtype
  into :nazwa, :contrtype, :autonr;
  if (:autonr = 1) then
  begin
    execute procedure get_number(:nazwa, '', substring(:contrtype from 1 for 20), new.datazal, null, new.ref)
      returning_values new.numer, :symbol;
   -- exception test_break''||:nazwa||' '|| substring(:contrtype from 1 for 20)||' '||substring(:symbol from 1 for 20)||' '||:symbol;
    new.symbol = substring(:symbol from 1 for 20);
  end
end^
SET TERM ; ^
