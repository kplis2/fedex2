--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EASSETPOS_BI_ORDER FOR EASSETPOS                      
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable maxnum integer;
begin
  new.ord = 1;
  select max(number) from eassetpos where eassetofcomp = new.eassetofcomp
    into :maxnum;
  if (new.number is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum) then
  begin
    update eassetpos set ord = 0, number = number + 1
      where number >= new.number and eassetofcomp = new.eassetofcomp;
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
