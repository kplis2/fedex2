--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKVATPOS_AU_COMPUTE_BKDOC FOR BKVATPOS                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if (coalesce(new.netv,0) <> coalesce(old.netv,0) or coalesce(new.vatv,0) <> coalesce(old.vatv,0)
    or coalesce(new.grossv,0) <> coalesce(old.grossv,0) or coalesce(new.currnetv,0) <> coalesce(old.currnetv,0)
    or coalesce(new.net,0) <> coalesce(old.net,0) or coalesce(new.vate,0) <> coalesce(old.vate,0)
  ) then
  begin
    update bkdocs set
      sumnetv = sumnetv + new.netv - old.netv,
      sumvatv = sumvatv + new.vatv - old.vatv,
      sumgrossv = sumgrossv + new.grossv - old.grossv,
      curnetval = curnetval + new.currnetv - old.currnetv,
      sumnet = sumnet + new.net - old.net,
      sumvate = sumvate + new.vate - old.vate
    where ref = new.bkdoc;
  end
end^
SET TERM ; ^
