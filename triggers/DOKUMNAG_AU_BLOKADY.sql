--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_AU_BLOKADY FOR DOKUMNAG                       
  ACTIVE AFTER UPDATE POSITION 10 
AS
declare variable operzam varchar(3);
declare variable kiedystan smallint;
begin
  if(new.akcept = 1 and old.akcept <> 1 and new.zrodlo = 1 and new.historia is not null and
     new.zamowienie is not null) then begin
    select operacja from histzam where ref = new.historia into :operzam;

    select kiedystan from defoperzam where symbol = :operzam into :kiedystan;

    if (:kiedystan is not null and :kiedystan = 1) then begin
      update nagzam set stan = 'N'
        where ref = new.zamowienie;
    end
  end
  --JC - badanie, czy niew są przekroczone promocje pakietowe
  if(new.akcept = 1 and old.akcept = 0 and new.wydania = 1 and new.zewn = 1) then begin
    execute procedure pakiet_weryfikuj(new.ref, 'M',0,0,0);
  end
end^
SET TERM ; ^
