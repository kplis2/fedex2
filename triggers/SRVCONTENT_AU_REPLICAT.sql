--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVCONTENT_AU_REPLICAT FOR SRVCONTENT                     
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if((new.contrtype <> old.contrtype ) or (new.contrtype is not null and old.contrtype is null) or (new.contrtype is null and old.contrtype is not null)
    or (new.name <> old.name ) or (new.name is not null and old.name is null) or (new.name is null and old.name is not null)
  ) then
     update CONTRTYPES set STATE = -1 where ref=new.contrtype;
end^
SET TERM ; ^
