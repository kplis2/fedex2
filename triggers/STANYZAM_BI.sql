--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYZAM_BI FOR STANYZAM                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.ref is null) then
    execute procedure GEN_REF('STANYZAM') returning_values new.ref;
  if(new.status is null) then new.status = 0;
  if(new.status in (0,1)) then begin
    if(new.iloscsugerowana=new.iloscdozam) then new.status = 0;
    else new.status = 1;
  end
end^
SET TERM ; ^
