--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYREZ_BU0 FOR STANYREZ                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable numer integer;
declare variable iloscpo numeric(14,4);
declare variable iloscmag numeric(14,4);
begin
  if((new.wersjaref <> old.wersjaref or (new.ktm is null) or (new.wersja is null)) and new.wersjaref is not null and new.wersjaref <> 0) then begin
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  end else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)or (new.wersjaref is null)) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if(new.status = 'D') then begin
    new.ilplus = new.ilosc;
    new.ilminus = 0;
  end else begin
    new.ilminus = new.ilosc;
    new.ilplus = 0;
  end
  /*rozpoznanie typu STANYREZ - dokumentu powiazanego*/
  if(new.typ is null or (new.typ = '') or (new.typ = ' ')) then begin
    if(new.zamowienie > 0) then begin
      if(new.pozzam > 0) then begin
        if(
          exists (select REF from POZZAM where ZAMOWIENIE = new.zamowienie and REF=new.pozzam)
        )then begin
          new.typ = 'Z';--pozuycja zamowienia
          new.pozzamref = new.pozzam;
        end else begin
          new.typ = 'G';--pozycja przewodnika
          select prschedguidespos.pozzamref from prschedguidespos where ref=new.pozzam into new.pozzamref;
        end
      end else if(new.pozzam = 0) then begin
          new.typ = 'Z';--naglowek zamowienia
      end else
          new.typ = 'G';--naglowek przewodnika
    end else if(new.zreal=2) then begin
      new.typ = 'F';--faktura pozycje
    end else if(new.zreal=3) then begin
      new.typ = 'M';--pozycja dokumentu magazynowego
    end
  end
  if(new.status <> old.status) then new.datazmbl = current_date;
end^
SET TERM ; ^
