--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACPLAN_AU_STATE FOR EVACPLAN                       
  ACTIVE AFTER UPDATE POSITION 1 
AS
begin
  if (old.state = 2 and new.state <> 2) then
  begin
    delete from eabsences
      where correction = 0
        and employee = old.employee
        and ecolumn = old.ecolumn
        and fromdate >= old.fromdate and todate <= old.todate
      order by fromdate desc;
  end
end^
SET TERM ; ^
