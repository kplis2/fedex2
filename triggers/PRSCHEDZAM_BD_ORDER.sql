--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDZAM_BD_ORDER FOR PRSCHEDZAM                     
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update prschedzam set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and prschedule = old.prschedule;
end^
SET TERM ; ^
