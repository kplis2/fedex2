--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDROZ_AI0 FOR LISTYWYSDROZ                   
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  update listywysdpoz
    set iloscspk = coalesce(iloscspk, 0) + new.iloscmag
    where ref = new.listywysdpoz;
end^
SET TERM ; ^
