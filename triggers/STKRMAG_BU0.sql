--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STKRMAG_BU0 FOR STKRMAG                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.priorytet is null) then new.priorytet = 0;
  if(new.priorytet < 0) then exception STKRMAG_PRIORYTETTOSMALL;
  if(new.priorytet <> old.priorytet or new.stkrtab <> old.stkrtab) then
    new.lastchange = current_time;
end^
SET TERM ; ^
