--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DK_EXP_BI_REF FOR DK_EXP                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DK_EXP')
      returning_values new.REF;
end^
SET TERM ; ^
