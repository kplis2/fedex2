--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAKUSL_BI_REF FOR POZFAKUSL                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('POZFAKUSL')
      returning_values new.REF;
end^
SET TERM ; ^
