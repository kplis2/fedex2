--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WFWORKFLOWS_AU0 FOR WFWORKFLOWS                    
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable fazaold varchar(80);
declare variable fazanew varchar(80);
begin
  if(coalesce(new.faza,0) <> coalesce(old.faza,0)) then execute procedure wfworkflow_status_set(new.ref);
  if(new.faza <> old.faza) then begin
    select nazwa from cfazy where ref = old.faza into :fazaold;
    select nazwa from cfazy where ref = new.faza into :fazanew;
    insert into wfhistory(wfworkflow, field, oldvalue, newvalue) values(new.ref, 'Faza', :fazaold, :fazanew);
  end
end^
SET TERM ; ^
