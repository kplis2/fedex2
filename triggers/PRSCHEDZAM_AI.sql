--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDZAM_AI FOR PRSCHEDZAM                     
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable main integer;
declare variable guide integer;
declare variable stan char(1);
declare variable kstan char(1);
begin
  --przenisienie blokad i rezerwacji z zamowienia na przewodniki
  select prschedules.main from prschedules where ref=new.prschedule into :main;
--  if(:main = 1) then begin
  --  update NAGZAM set nagzam.onmainschedule = 1 where ref=new.zamowienie;--tutaj nastepuje wycofanie przez NAGZAM wlasnych blokad
--  end
  if(new.prsheet > 0 or new.outonpozzamenabled > 0)then begin
    --tworzenie wpisów do operacji podstawowych
    execute procedure PRSCHED_ADD_ZAMGUIDE(new.ref,null,null);
    --harmonogramowanie podstawowe - dorownanie do lewej
  end
  /* przeniesione na triger przed dodanie PRSCHEDGUIDES - automatycne podczytanie stanó z NAGZAM'a
    select NAGZAM.STAN, nagzam.KSTAN from NAGZAM where REF=new.zamowienie into :stan, :kstan;
    for select prschedguides.ref
    from PRSCHEDGUIDES
    where PRSCHEDGUIDES.prschedzam = new.ref
    order by prschedguides.numer
    into :guide
    do begin
       update PRSCHEDGUIDES set STAN = :stan, KSTAN = :kstan where ref=:guide;
    end
    */
end^
SET TERM ; ^
