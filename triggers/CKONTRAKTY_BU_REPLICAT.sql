--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKONTRAKTY_BU_REPLICAT FOR CKONTRAKTY                     
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (
     (new.CPODMIOT <>old.CPODMIOT ) or (new.cpodmiot is null and old.cpodmiot is not null)
     or (new.CPODMIOT2 <> old.CPODMIOT2 ) or (new.CPODMIOT2 is null and old.CPODMIOT2 is not null) or (new.CPODMIOT2 is not null and old.CPODMIOT2 is null)
     or (new.DATAZAL <> old.DATAZAL  ) or (new.DATAZAL is null and old.DATAZAL is not null) or (new.DATAZAL is not null and old.DATAZAL is null)
     or (new.OPERZAL <> old.OPERZAL ) or (new.OPERZAL is null and old.OPERZAL is not null) or (new.OPERZAL is not null and old.OPERZAL is null)
     or (new.OPEROPIEK <> old.OPEROPIEK ) or (new.OPEROPIEK is null and old.OPEROPIEK is not null) or (new.OPEROPIEK is not null and old.OPEROPIEK is null)
     or (new.NAZWA <> old.NAZWA ) or (new.NAZWA is null and old.NAZWA is not null) or (new.NAZWA is not null and old.NAZWA is null)
     or (new.OPIS <> old.OPIS ) or (new.OPIS is null and old.OPIS is not null) or (new.OPIS is not null and old.OPIS is null)
     or (new.WARTPOT <> old.WARTPOT ) or (new.WARTPOT is null and old.WARTPOT is not null) or (new.WARTPOT is not null and old.WARTPOT is null)
     or (new.PEWNOSC <> old.PEWNOSC ) or (new.PEWNOSC is null and old.PEWNOSC is not null) or (new.PEWNOSC is not null and old.PEWNOSC is null)
     or (new.DATAPLREAL <> old.DATAPLREAL ) or (new.DATAPLREAL is null and old.DATAPLREAL is not null) or (new.DATAPLREAL is not null and old.DATAPLREAL is null)
     or (new.OSOBA <> old.OSOBA ) or (new.OSOBA is null and old.OSOBA is not null) or (new.OSOBA is not null and old.OSOBA is null)
     or (new.DATAREAL <> old.DATAREAL ) or (new.DATAREAL is null and old.DATAREAL is not null) or (new.DATAREAL is not null and old.DATAREAL is null)
     or (new.WARTREAL <> old.WARTREAL ) or (new.WARTREAL is null and old.WARTREAL is not null) or (new.WARTREAL is not null and old.WARTREAL is null)
     or (new.STOPREAL <> old.STOPREAL ) or (new.STOPREAL is null and old.STOPREAL is not null) or (new.STOPREAL is not null and old.STOPREAL is null)
     or (new.DATAZREAL <> old.DATAZREAL ) or (new.DATAZREAL is null and old.DATAZREAL is not null) or (new.DATAZREAL is not null and old.DATAZREAL is null)
     or (new.PRAWADEF <> old.PRAWADEF ) or (new.PRAWADEF is null and old.PRAWADEF is not null) or (new.PRAWADEF is not null and old.PRAWADEF is null)
     or (new.PRAWAGRUP <> old.PRAWAGRUP ) or (new.PRAWAGRUP is null and old.PRAWAGRUP is not null) or (new.PRAWAGRUP is not null and old.PRAWAGRUP is null)
     or (new.PRIORYTET <> old.PRIORYTET ) or (new.PRIORYTET is null and old.PRIORYTET is not null) or (new.PRIORYTET is not null and old.PRIORYTET is null)
     or (new.CONTRTYPE <> old.CONTRTYPE ) or (new.CONTRTYPE is null and old.CONTRTYPE is not null) or (new.CONTRTYPE is not null and old.CONTRTYPE is null)
  ) then
   waschange = 1;

  execute procedure rp_trigger_bu('CKONTRAKTY',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
