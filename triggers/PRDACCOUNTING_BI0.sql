--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRDACCOUNTING_BI0 FOR PRDACCOUNTING                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.deductedamount is null) then new.deductedamount = 0;
  if (new.status is null) then new.status = 0;
  if(new.firstprdversion is null) then begin
          execute procedure GEN_REF('PRDVERSIONS')
      returning_values new.firstprdversion;
  end
end^
SET TERM ; ^
