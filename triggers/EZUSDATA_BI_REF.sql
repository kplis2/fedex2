--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EZUSDATA_BI_REF FOR EZUSDATA                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EZUSDATA')
      returning_values new.REF;
end^
SET TERM ; ^
