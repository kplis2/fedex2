--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_AU_MWS FOR DOKUMPOZ                       
  ACTIVE AFTER UPDATE POSITION 20 
as
declare variable ilosc ilosci_mag;
begin
  if (old.iloscl <> new.iloscl) then
    begin
      --if (new.mwsactsdone = 0) then
        execute procedure DOKUMPOZ_OBL_FROM_MWSACTS('M', new.dokument,new.ref,
          null,null,null,null);
      if (new.braktowaru = 1) then
        begin
          if (new.iloscl <= new.ilosco - coalesce(new.braktowaru_ilosc, 0)) then
            begin
              update dokumpoz set braktowaru = null, braktowaru_ilosc = null
                where ref = new.ref;
              if (not exists(select first 1 1 from dokumpoz
                  where dokument = new.dokument and braktowaru = 1)
                 ) then
                update dokumnag set braktowaru = 0 where ref = new.dokument;
            end
          else
            begin
              update dokumpoz set braktowaru_ilosc = ilosc - ilosconmwsacts
                where ref = new.ref;
            end
        end
    end

end^
SET TERM ; ^
