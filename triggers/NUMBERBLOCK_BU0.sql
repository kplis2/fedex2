--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NUMBERBLOCK_BU0 FOR NUMBERBLOCK                    
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable keyval varchar(20);
declare variable numoddzial varchar(20);
declare variable number integer;
declare variable lastdata timestamp;
declare variable oldnumber integer;
declare variable olddata timestamp;
declare variable typ smallint;
begin
  if(new.numrejdok is null) then new.numrejdok = '';
  if(new.numtypdok is null) then new.numtypdok = '';
  /*zwolnienie poprzedniego numeru*/
  if(old.number > 0 and (
     new.data <> old.data or (new.numrejdok <> old.numrejdok) or (new.numtypdok <> old.numtypdok) or (new.numerator <> old.numerator)
  ))
  then begin
    execute procedure NUMBER_FORMAT(old.numerator, old.numrejdok, old.numtypdok, old.data) returning_values :keyval, :numoddzial;
    select min(number), min(LASTDATA) from NUMBERFREE where oddzial = :numoddzial and nazwa=old.numerator and key_val = :keyval and typ = 0 into :number, :lastdata;
    if(:number is null) then number = 0;
    select NUMBER, LASTDATA, typ from NUMBERFREE where ref=old.number into :oldnumber, :olddata, :typ;
    if(:typ <> 1) then exception NUMBERBLOCK_NUMUSED;
    if(:olddata = :lastdata and :oldnumber = :number) then begin
      update NUMBERFREE set NUMBER = NUMBER - 1 where oddzial = :numoddzial and nazwa=old.numerator and key_val = :keyval and typ = 0;
      update NUMBERFREE set NUMBLOCK = null where ref=old.number;
      delete from NUMBERFREE where ref=old.number;
      delete from NUMBERFREE where NUMBER = 0 and oddzial = :numoddzial and nazwa=old.numerator and key_val = :keyval and typ = 0;
    end else
      update NUMBERFREE set NUMBLOCK = null where ref=old.number;
    new.number = null;
    new.numnumber = null;
  end
  if(new.number is null and new.data <= current_date) then begin
    execute procedure NUMBER_FORMAT(new.numerator, new.numrejdok, new.numtypdok, new.data) returning_values :keyval, :numoddzial;
    select min(ref) from NUMBERFREE
    where nazwa = new.numerator and key_val = :keyval and lastdata = new.data and typ = 1
    into new.number;
    if(new.number is null)then begin
      select min(number), min(LASTDATA) from NUMBERFREE where oddzial = :numoddzial and nazwa=new.numerator and key_val = :keyval and typ = 0 into :number, :lastdata;
      if((new.data >= :lastdata or (:lastdata is null)) and new.data <= current_date) then begin
        if(:number is null) then begin
          /* zalozenie pierwszego rekordu w danej dziedzinie numeratora */
           insert into NUMBERFREE( ODDZIAL, NAZWA, KEY_VAL,NUMBER,TYP, LASTDATA) values(:numoddzial, new.numerator,:keyval,0,0,new.data);
          number = 0;
        end
        /*numer kolejny */
        number = number + 1;
        /* zapis ostatniego użytego numeru */
        update NUMBERFREE set NUMBER=:number, LASTDATA = new.data where oddzial = :numoddzial and nazwa=new.numerator and key_val =:keyval and typ = 0;
        execute procedure GEN_REF('NUMBERFREE') returning_values new.number;
        insert into NUMBERFREE (REF, NAZWA, KEY_VAL, NUMBER, TYP, ODDZIAL, LASTDATA)
        values (new.number, new.numerator, :keyval, :number, 1, :numoddzial, new.data);
      end
    end
    if(new.number is null) then exception NUMBERBLOCK_BRAKFREENUM;
    else select NUMBERFREE.number from NUMBERFREE where ref=new.number into new.numnumber;
  end
end^
SET TERM ; ^
