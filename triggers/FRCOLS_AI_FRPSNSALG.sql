--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRCOLS_AI_FRPSNSALG FOR FRCOLS                         
  ACTIVE AFTER INSERT POSITION 0 
AS
 declare variable pref integer;
begin
  for
    select ref
    from frpsns
    where frhdr = new.frhdr
    into :pref
  do begin
    if (not exists (select frpsn from frpsnsalg where frpsn = :pref and algtype = 1 and frversion = 0)) then
      insert into frpsnsalg (frpsn, frcol, algtype, frversion)
        values (:pref, new.ref, 1, 0);
    else
      insert into frpsnsalg (frpsn, frcol, algtype, frversion)
        values (:pref, new.ref, 2, 0);
  end
end^
SET TERM ; ^
