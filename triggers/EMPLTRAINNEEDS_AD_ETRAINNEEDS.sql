--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLTRAINNEEDS_AD_ETRAINNEEDS FOR EMPLTRAINNEEDS                 
  ACTIVE AFTER DELETE POSITION 0 
AS
  declare variable enumber numeric(14,2);
  declare variable costtype smallint;
  declare variable personcost numeric(14,2);
  declare variable totalcost numeric(14,2);
begin
  select count(ref)
    from empltrainneeds
    where etrainneed = old.etrainneed
    into :enumber;

  enumber = coalesce(:enumber, 0);

  select costtype, personcost, totalcost
    from etrainneeds
    where ref = old.etrainneed
    into :costtype, :personcost, :totalcost;

  if (costtype = 0) then
    totalcost = personcost * enumber;
  else if (costtype = 1 and enumber > 0) then
    personcost = totalcost / enumber;
  else if (costtype = 1 and enumber = 0) then
    personcost = 0;

  if (costtype is not null) then
    update etrainneeds set personcost = :personcost, totalcost = :totalcost where ref = old.etrainneed;
end^
SET TERM ; ^
