--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHPRICEADV_BI_REF FOR WHPRICEADV                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('WHPRICEADV')
      returning_values new.REF;
end^
SET TERM ; ^
