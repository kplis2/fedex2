--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_AD_EABSENCESBASES FOR EABSENCES                      
  ACTIVE AFTER DELETE POSITION 3 
as
begin
/*MWr: z realizacja BS20435 usunieto klucz obcy na EABSENCE w tabeli EABSENCESBASES,
  stad koniecznosc manulanego wymuszenia usuniecia podstaw usuwanej nieobecnosci*/

   if (coalesce(old.mflag,0) = 0) then
     delete from eabsencebases
       where eabsence = old.ref;
end^
SET TERM ; ^
