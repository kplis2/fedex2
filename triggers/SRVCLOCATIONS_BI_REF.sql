--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVCLOCATIONS_BI_REF FOR SRVCLOCATIONS                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SRVCLOCATIONS')
      returning_values new.REF;
end^
SET TERM ; ^
