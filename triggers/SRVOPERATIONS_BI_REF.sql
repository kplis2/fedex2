--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVOPERATIONS_BI_REF FOR SRVOPERATIONS                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SRVOPERATIONS')
      returning_values new.REF;
end^
SET TERM ; ^
