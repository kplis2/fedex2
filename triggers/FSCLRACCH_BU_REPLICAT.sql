--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FSCLRACCH_BU_REPLICAT FOR FSCLRACCH                      
  ACTIVE BEFORE UPDATE POSITION 1 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if((new.ref <> old.ref)
   or (new.period <> old.period) or (new.period is not null and old.period is null) or (new.period is null and old.period is not null)
   or (new.okres  <> old.okres ) or (new.okres is not null and old.okres is null) or (new.okres is null and old.okres is not null)
   or (new.number <> old.number ) or (new.number is not null and old.number is null) or (new.number is null and old.number is not null)
   or (new.symbol <> old.symbol ) or (new.symbol is not null and old.symbol is null) or (new.symbol is null and old.symbol is not null)
   or (new.regdate <> old.regdate ) or (new.regdate is not null and old.regdate is null) or (new.regdate is null and old.regdate is not null)
   or (new.accdate <> old.accdate ) or (new.accdate is not null and old.accdate is null) or (new.accdate is null and old.accdate is not null)
   or (new.dictdef <> old.dictdef ) or (new.dictdef is not null and old.dictdef is null) or (new.dictdef is null and old.dictdef is not null)
   or (new.dictpos <> old.dictpos ) or (new.dictpos is not null and old.dictpos is null) or (new.dictpos is null and old.dictpos is not null)
   or (new.dictgrp <> old.dictgrp ) or (new.dictgrp is not null and old.dictgrp is null) or (new.dictgrp is null and old.dictgrp is not null)
   or (new.dictsymbol <> old.dictsymbol ) or (new.dictsymbol is not null and old.dictsymbol is null) or (new.dictsymbol is null and old.dictsymbol is not 

null)
   or (new.status <> old.status ) or (new.status is not null and old.status is null) or (new.status is null and old.status is not null)
   or (new.currency <> old.currency ) or (new.currency is not null and old.currency is null) or (new.currency is null and old.currency is not null)
   or (new.regoper <> old.regoper ) or (new.regoper is not null and old.regoper is null) or (new.regoper is null and old.regoper is not null)
   or (new.direction <> old.direction ) or (new.direction is not null and old.direction is null) or (new.direction is null and old.direction is not null)
   or (new.multicurr <> old.multicurr ) or (new.multicurr is not null and old.multicurr is null) or (new.multicurr is null and old.multicurr is not null)
   or (new.rights <> old.rights ) or (new.rights is not null and old.rights is null) or (new.rights is null and old.rights is not null)
   or (new.rightsgroup <> old.rightsgroup ) or (new.rightsgroup is not null and old.rightsgroup is null) or (new.rightsgroup is null and old.rightsgroup is 

not null)
   or (new.rightsdef <> old.rightsdef ) or (new.rightsdef is not null and old.rightsdef is null) or (new.rightsdef is null and old.rightsdef is not null)
   or (new.stable <> old.stable ) or (new.stable is not null and old.stable is null) or (new.stable is null and old.stable is not null)
   or (new.sref <> old.sref ) or (new.sref is not null and old.sref is null) or (new.sref is null and old.sref is not null)
   or (new.fstypeakc <> old.fstypeakc ) or (new.fstypeakc is not null and old.fstypeakc is null) or (new.fstypeakc is null and old.fstypeakc is not null)
   or (new.fstype <> old.fstype ) or (new.fstype is not null and old.fstype is null) or (new.fstype is null and old.fstype is not null)
   or (new.docdate <> old.docdate ) or (new.docdate is not null and old.docdate is null) or (new.docdate is null and old.docdate is not null)

  ) then
   waschange = 1;

  execute procedure rp_trigger_bu('FSCLRACCH',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
