--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTAWCY_BI_REPLICAT FOR DOSTAWCY                       
  ACTIVE BEFORE INSERT POSITION 2 
AS
begin
 execute procedure rp_trigger_bi('DOSTAWCY',new.ref, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
