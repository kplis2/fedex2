--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSD_AU0 FOR LISTYWYSD                      
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable ildok integer;
declare variable ilspr integer;
declare variable grupasped integer;
declare variable waga numeric(15,4);
declare variable objetosc numeric(15,4);
declare variable koszt numeric(14,2);
begin
  if((new.listawys <> old.listawys) or (new.listawys is null and old.listawys is not null) or (new.listawys is not null and old.listawys is null)) then begin
    if(old.listawys is not null) then begin
      ildok = 0;
      select count(*) from listywysd where listawys =  old.listawys  into :ildok;
      if(:ildok is null) then ildok = 0;
      waga = 0;
      objetosc = 0;
      koszt = 0;
      select sum(waga),sum(kosztdost), sum(objetosc) from listywysd where listawys=old.listawys into :waga,:koszt,:objetosc;
      if(:waga is null) then waga = 0;
      if(:koszt is null) then koszt = 0;
      if(:objetosc is null) then objetosc = 0;
      ilspr = 0;
      select count(*) from listywysd where listawys=old.listawys and wyschecked=1 into :ilspr;
      if(:ilspr is null) then ilspr = 0;
      update listywys set ildok = :ildok, waga = :waga, kosztdost = :koszt, ilspr = :ilspr, objetosc = :objetosc
        where ref=old.listawys;
    end
    if(new.listawys is not null) then begin
      ildok = 0;
      select count(*) from listywysd where listawys = new.listawys  into :ildok;
      if(:ildok is null) then ildok = 0;
      waga = 0;
      objetosc = 0;
      koszt = 0;
      select sum(waga),sum(kosztdost),sum(objetosc) from listywysd where listawys=new.listawys into :waga,:koszt,:objetosc;
      if(:waga is null) then waga = 0;
      if(:koszt is null) then koszt = 0;
      if(:objetosc is null) then objetosc = 0;
      ilspr = 0;
      select count(*) from listywysd where listawys=new.listawys and wyschecked=1 into :ilspr;
      if(:ilspr is null) then ilspr = 0;
      update listywys set ildok = :ildok, waga = :waga, kosztdost = :koszt, ilspr = :ilspr, objetosc = :objetosc
        where ref=new.listawys;
    end
  end else begin
    if((new.waga <> old.waga) or (new.objetosc <> old.objetosc) or
       (new.wyschecked <> old.wyschecked or (new.wyschecked is not null and old.wyschecked is null) or (new.wyschecked is null and old.wyschecked is not null)) or
       (new.kosztdost <> old.kosztdost or (new.kosztdost is not null and old.kosztdost is null) or (new.kosztdost is null and old.kosztdost is not null))
      ) then begin
      waga = 0;
      objetosc = 0;
      koszt = 0;
      select sum(waga),sum(kosztdost),sum(objetosc) from listywysd where listawys=new.listawys into :waga,:koszt,:objetosc;
      if(:waga is null) then waga = 0;
      if(:koszt is null) then koszt = 0;
      if(:objetosc is null) then objetosc = 0;
      ilspr = 0;
      select count(*) from listywysd where listawys=new.listawys and wyschecked=1 into :ilspr;
      if(:ilspr is null) then ilspr = 0;
      update listywys set waga = :waga, kosztdost = :koszt, ilspr = :ilspr, objetosc = :objetosc
        where ref=new.listawys;
    end
  end
  if (new.akcept <> old.akcept) then
  begin
    for
      select d.grupasped
        from listywysdpoz p
          left join dokumnag d on (d.ref = p.dokref and p.doktyp = 'M')
        where p.dokument = new.ref
        into grupasped
    do begin
      execute procedure dokumnag_mwsrealstatus(grupasped,1);
    end
  end
  if (new.akcept < 2 and old.akcept = 2) then
  begin
    --usuwanie zlecen transportowych
    if (not exists(select first 1 1 from listywysd where listawys = old.listawys and ref <> new.ref)) then begin
      delete from listywys where ref = old.listawys;
    end
  end
  if (new.akcept = 2 and old.akcept <2) then
  begin
    execute procedure X_MWS_RELASE_CART(new.x_grupasped);
  end
end^
SET TERM ; ^
