--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BHPPLANPOS_BU_CHGTIMESTAMP FOR BHPPLANPOS                     
  ACTIVE BEFORE UPDATE POSITION 9 
as
begin
--MWr Ustawienie znacznika czasowego przy zmianie statusu

  if (new.status <> old.status) then
    new.chgtimestamp = current_timestamp(0);
end^
SET TERM ; ^
