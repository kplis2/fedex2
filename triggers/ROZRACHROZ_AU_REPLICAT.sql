--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACHROZ_AU_REPLICAT FOR ROZRACHROZ                     
  ACTIVE AFTER UPDATE POSITION 1 
as
declare variable newstate integer;
begin
     if(new.ref <> old.ref or
      new.kontofk <> old.kontofk or (new.kontofk is not null and old.kontofk is null) or (new.kontofk is null and old.kontofk is not null) or
      new.slodef <> old.slodef or (new.slodef is not null and old.slodef is null) or (new.slodef is null and old.slodef is not null) or
      new.slopoz <> old.slopoz or (new.slopoz is not null and old.slopoz is null) or (new.slopoz is null and old.slopoz is not null) or
      new.symbfak <> old.symbfak or (new.symbfak is not null and old.symbfak is null) or (new.symbfak is null and old.symbfak is not null) or
      new.doktyp <> old.doktyp or (new.doktyp is not null and old.doktyp is null) or (new.doktyp is null and old.doktyp is not null) or
      new.dokref <> old.dokref or (new.dokref is not null and old.dokref is null) or (new.dokref is null and old.dokref is not null) or
      new.winien <> old.winien or (new.winien is not null and old.winien is null) or (new.winien is null and old.winien is not null) or
      new.ma <> old.ma or (new.ma is not null and old.ma is null) or (new.ma is null and old.ma is not null) or
      new.rozmaster <> old.rozmaster or (new.rozmaster is not null and old.rozmaster is null) or (new.rozmaster is null and old.rozmaster is not null) or
      new.payday <> old.payday or (new.payday is not null and old.payday is null) or (new.payday is null and old.payday is not null) or
      new.waluta <> old.waluta or (new.waluta is not null and old.waluta is null) or (new.waluta is null and old.waluta is not null) or
      new.stable <> old.stable or (new.stable is not null and old.stable is null) or (new.stable is null and old.stable is not null) or
      new.sref  <> old.sref or (new.sref is not null and old.sref is null) or (new.sref is null and old.sref is not null)
     )
  then begin
    if(new.doktyp = 'F') then
      update NAGFAK set STATE=-1 where AKCEPTACJA = 1 and ref = new.dokref;
  end
end^
SET TERM ; ^
