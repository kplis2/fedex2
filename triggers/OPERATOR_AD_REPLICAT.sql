--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OPERATOR_AD_REPLICAT FOR OPERATOR                       
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  execute procedure RP_TRIGGER_AD('OPERATOR', old.ref, NULL, NULL, NULL, NULL, old.token, old.STATE);
end^
SET TERM ; ^
