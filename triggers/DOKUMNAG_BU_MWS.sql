--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_BU_MWS FOR DOKUMNAG                       
  ACTIVE BEFORE UPDATE POSITION 30 
as
begin
  if (old.braktowaru is distinct from new.braktowaru
      and new.braktowaru is null) then
    new.braktowaru = 0;

  --[PM] do integracji
  if (old.braktowaru is distinct from new.braktowaru) then
    new.braktowaru_czaszm = current_timestamp(0);

end^
SET TERM ; ^
