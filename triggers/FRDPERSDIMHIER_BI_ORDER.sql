--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDPERSDIMHIER_BI_ORDER FOR FRDPERSDIMHIER                 
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable maxnum integer;
begin
  new.ord = 1;
  select max(hierorder) from frdpersdimhier where frdperspect = new.frdperspect
    into :maxnum;
  if (new.hierorder is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.hierorder = maxnum + 1;
  end else if (new.hierorder <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update frdpersdimhier set ord = 0, hierorder = hierorder + 1
      where hierorder >= new.hierorder and frdperspect = new.frdperspect;
  end else if (new.hierorder > maxnum) then
    new.hierorder = maxnum + 1;
end^
SET TERM ; ^
