--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWRKCRTNOCONTR_BIU_DATE FOR EWRKCRTNOCONTR                 
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
begin
  if (new.fromdate is null or new.todate is null) then
    exception DATY_OD_DO_MUSZA_BYC_WYPELNIONE;
end^
SET TERM ; ^
