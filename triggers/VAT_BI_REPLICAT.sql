--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VAT_BI_REPLICAT FOR VAT                            
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  execute procedure rp_trigger_bi('VAT',new.grupa, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
