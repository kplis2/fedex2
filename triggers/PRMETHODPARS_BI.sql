--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRMETHODPARS_BI FOR PRMETHODPARS                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.partype is null) then new.partype = 'M';
  if (new.status is null or new.partype = 'G') then new.status = 0;
end^
SET TERM ; ^
