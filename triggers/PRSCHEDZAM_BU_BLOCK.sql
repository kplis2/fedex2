--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDZAM_BU_BLOCK FOR PRSCHEDZAM                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable gref integer;
begin
  if(new.schedblocked < 2) then
  begin 
    for select ref from prschedguides pg where pg.prschedzam = new.ref into :gref
    do begin
      update prschedguides pg set pg.schedblocked = new.schedblocked where pg.ref = :gref;
    end
  end
  else if(new.schedblocked = 2) then new.schedblocked = 0;
  else if(new.schedblocked = 3) then new.schedblocked = 1;
end^
SET TERM ; ^
