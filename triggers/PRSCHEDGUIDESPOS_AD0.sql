--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDESPOS_AD0 FOR PRSCHEDGUIDESPOS               
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  if(old.amount > 0 and old.pggamountcalc = 0) then
    execute procedure PRSCHEDGUIDESPOS_AMOUNTcalc(null, old.ref);
  if (old.amountzreal > 0 and old.pozzamref is not null) then
    execute PROCEDURE PRPOZZAM_AMOUNTZREAL(old.pozzamref);
end^
SET TERM ; ^
