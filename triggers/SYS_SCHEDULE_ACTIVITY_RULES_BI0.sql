--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SYS_SCHEDULE_ACTIVITY_RULES_BI0 FOR SYS_SCHEDULE_ACTIVITY_RULES    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('SYS_SCHEDULE_ACTIVITY_RULES')
      returning_values new.ref;
end^
SET TERM ; ^
