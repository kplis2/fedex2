--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYZAM_BU FOR STANYZAM                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable typzam varchar(3);
declare variable rejzam varchar(3);
declare variable nagzamref integer;
declare variable pozzamref integer;
begin
  if(new.iloscsugerowana<>old.iloscsugerowana or new.iloscdozam<>old.iloscdozam) then begin
    -- ustal status czy ilosc jest z automatu czy poprawiona recznie
    if(new.status in (0,1)) then begin
      if(new.iloscsugerowana=new.iloscdozam) then new.status = 0;
      else new.status = 1;
    end
    if(new.status>1) then exception UNIVERSAL 'Pozycja zamknieta! Nie można modyfikować.';
  end
  if(new.status=2 and old.status<2) then begin
    nagzamref = null;
    pozzamref = null;
    -- zatwierdz pozycje - wygeneruj zamowienie
    if(new.dostawca is not null) then begin
      -- jesli okreslono dostawce to bedzie zamowienie zewnetrzne
      execute procedure GETCONFIG('NEWZAMTYPZAK') returning_values :typzam;
      execute procedure GETCONFIG('NEWZAMREJZAK') returning_values :rejzam;
      select first 1 ref from NAGZAM where rejestr=:rejzam and typzam=:typzam and magazyn=new.zmagazynu
        and dostawca=new.dostawca and datawe=current_date into :nagzamref;
      if(:nagzamref is null) then begin
        insert into NAGZAM(REJESTR,TYPZAM,MAGAZYN,MAG2,DOSTAWCA,DATAWE)
        values (:rejzam,:typzam,new.zmagazynu,new.zmag2,new.dostawca,current_date)
        returning REF into :nagzamref;
      end
    end else if(new.zmagazynu is not null) then begin
      -- jesli nie, to wewnetrzne
      execute procedure GETCONFIG('NEWZAMTYPWYD') returning_values :typzam;
      execute procedure GETCONFIG('NEWZAMREJWYD') returning_values :rejzam;
      select first 1 ref from NAGZAM where rejestr=:rejzam and typzam=:typzam and magazyn=new.zmagazynu
        and mag2=new.zmag2 and datawe=current_date into :nagzamref;
      if(:nagzamref is null) then begin
        insert into NAGZAM(REJESTR,TYPZAM,MAGAZYN,MAG2,DATAWE)
        values (:rejzam,:typzam,new.zmagazynu,new.zmag2,current_date)
        returning REF into :nagzamref;
      end
    end
    -- znajdz pozycje na zamowieniu
    select first 1 ref from POZZAM where ZAMOWIENIE=:nagzamref and MAGAZYN=new.zmagazynu and WERSJAREF=new.wersjaref into :pozzamref;
    if(:pozzamref is not null) then begin
      update POZZAM set ILOSC = new.iloscdozam
      where REF=:pozzamref;
    end else begin
      insert into POZZAM(ZAMOWIENIE,WERSJAREF,ILOSC,MAGAZYN,MAG2)
      values (:nagzamref,new.wersjaref,new.iloscdozam,new.zmagazynu,new.zmag2)
      returning REF into :pozzamref;
    end
    -- oznacz status na wygenerowane
    new.iloscsugerowana = old.iloscsugerowana - new.iloscdozam;
    new.iloscdozam = 0;
    if(new.iloscsugerowana=new.iloscdozam) then new.status = 0;
    else new.status = 1;
  end
end^
SET TERM ; ^
