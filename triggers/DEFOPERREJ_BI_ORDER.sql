--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERREJ_BI_ORDER FOR DEFOPERREJ                     
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable maxnum integer;
begin
  new.ord = 1;
  select max(numer) from defoperrej where rejestr = new.rejestr
    into :maxnum;
  if (new.numer is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.numer = maxnum + 1;
  end else if (new.numer <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update defoperrej set ord = 0, numer = numer + 1
      where numer >= new.numer and rejestr = new.rejestr;
  end else if (new.numer > maxnum) then
    new.numer = maxnum + 1;
end^
SET TERM ; ^
