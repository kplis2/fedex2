--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_APPINI_BI FOR S_APPINI                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  IF (NEW.REF IS NULL) THEN
      NEW.REF = GEN_ID(GEN_S_APPINI,1);
  new.change = 1;
  new.lastchangedate = current_timestamp(0);
  if(new.customized is null) then new.customized = 0;
  if(new.namespace is null) then new.namespace = 'SENTE';
END^
SET TERM ; ^
