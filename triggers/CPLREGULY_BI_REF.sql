--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLREGULY_BI_REF FOR CPLREGULY                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CPLREGULY')
      returning_values new.REF;
end^
SET TERM ; ^
