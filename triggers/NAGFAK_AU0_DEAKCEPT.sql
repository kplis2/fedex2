--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_AU0_DEAKCEPT FOR NAGFAK                         
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable rozrachauto varchar(255);
declare variable cnt integer;
declare variable autozap smallint;
declare variable rozrachaut integer;
declare variable rkanulowano integer;
declare variable rknumer integer;
declare variable rkstanowisko varchar(3);
declare variable rktyp varchar(3);
declare variable rkusun varchar(20);
declare variable dokmagref integer;
declare variable pozfakref integer;
declare variable typzakup smallint;
declare variable typblok smallint;
declare variable local smallint;
declare variable POZZAMREF INTEGER;
declare variable rkdata timestamp;
declare variable oldakcept varchar(60);
declare variable newakcept varchar(60);
begin
  if(new.akceptacja <> old.akceptacja) then begin
    if(old.akceptacja = 0) then
      oldakcept  = 'Niezaakceptowana';
    if(old.akceptacja = 1) then
      oldakcept = 'Zaakceptowana';
    if(old.akceptacja = 9) then
      oldakcept = 'Wycofana do poprawy';
    if(old.akceptacja = 10) then
      oldakcept = 'Wycofana do poprawy bez wycofania dokumentu magazynowego';
    if(new.akceptacja = 0) then
      newakcept = 'Niezaakceptowana';
    if(new.akceptacja = 1) then
      newakcept = 'Zaakceptowana';
    if(new.akceptacja = 9) then
      newakcept = 'Wycofana do poprawy';
    if(new.akceptacja = 10) then
      newakcept = 'Wycofana do poprawy bez wycofania dokumentu magazynowego';
  end
  execute procedure CHECK_LOCAL('NAGFAK',new.ref) returning_values :local;
  if((new.akceptacja = 0 or (new.akceptacja >= 9)) and (old.akceptacja = 1) and (:local = 1 ) and (new.nieobrot <> 1)) then
  begin
    insert into security_log (logfil, tablename, item, key1, oldvalue, newvalue)
      values ('NAGFAK', 'NAGFAK', 'AKCEPTACJA', new.ref, :oldakcept, :newakcept);
     if(new.akceptacja<>10) then begin
       update POZFAKUSL set ROZLICZONO=0 where REFNAGFAK = new.ref and ROZLICZONO=1;
       for select REF from POZFAK where (DOKUMENT = new.ref)
       into :pozfakref
       do begin
         update POZFAKUSL set ROZLICZONO=0 where (POZFAKUSL.REFPOZFAK = :pozfakref and POZFAKUSL.ROZLICZONO=2);
       end
     end
     /*usunicie zamówienia utworzone w trakcie akceptacji zamówienia*/
     if(new.skad = 0 and new.akceptacja <> 10) then
       delete from NAGZAM where faktura = new.ref and FAKDEPEND = 1;
     select GOTOWKA from STANSPRZED where  stanowisko=new.stanowisko into :autozap;
     if(new.zakup = 0) then
       execute procedure GETCONFIG('ROZRACHAUTO') returning_values :rozrachauto;
     else
       execute procedure GETCONFIG('DROZRACHAUTO') returning_values :rozrachauto;
     if(:rozrachauto is null or (:rozrachauto = '')) then rozrachauto = '0';
     if(new.nieobrot = 1 or (new.nieobrot = 3)) then rozrachauto = '-1';
     cnt = null;
     select count(*) from NAGFAK where REFFAK = new.ref and NIEOBROT = 0 into :cnt;
     if(:cnt > 0) then rozrachauto = '-1';
     rozrachaut = cast(:rozrachauto as integer);
     if(:autozap > 0 and :rozrachaut = 0) then begin
         update NAGFAK set zaplacono= 0 where REF=new.ref;
     end
    /* sprawdzenie, czy są anulowane dok. kasowe związane z danym */
    select ANULOWANY, NUMER, STANOWISKO,TYP, DATA from RKDOKNAG where REF=new.rkdoknag into :rkanulowano, :rknumer, :rkstanowisko, :rktyp, :rkdata;
    if(:rkanulowano = 0) then begin
      if(new.akceptacja >=9) then begin
        update RKDOKNAG set ANULOWANY=1, BLOKADA = 1,
        OPIS = 'Wycofany do poprawy',ANULOPIS = 'Wycofany do poprawy'
        where REF=new.rkdoknag;
        delete from RKDOKPOZ where RKDOKPOZ.dokument = new.rkdoknag;
      end else begin
        execute procedure GETCONFIG('KASSPRZUSU') returning_values :rkusun;
        if(:rkusun = '1' ) then begin
          if(:rknumer > 0) then begin
            cnt = null;
            select count(*) from RKDOKNAG where STANOWISKO = :rkstanowisko and TYP=:rktyp and NUMER > :rknumer and DATA >= :rkdata into :cnt;
            if(:cnt > 0) then begin
              rkusun = '';
            end
          end
        end else if(:rkusun = '2') then
          rkusun = '1';
        else rkusun = '';
        if(:rkusun <>'')then begin
          execute procedure GETCONFIG('KASUSUNLAST') returning_values :rkusun;
          if(:rkusun = '1' and :rknumer > 0) then begin
            /*sprawdzenie, czy usuwac, czy anulowac jednak*/
            cnt = null;
            select count(*) from RKDOKNAG where STANOWISKO = :rkstanowisko and TYP=:rktyp and NUMER > :rknumer and data >= :rkdata into :cnt;
            if(:cnt > 0) then rkusun = '';
          end
          if(:rkusun ='1') then begin
            update RKDOKNAG set NUMER = 0 where REF=new.rkdoknag;
            delete from  RKDOKNAG where REF=new.rkdoknag;
          end else
            update RKDOKNAG set ANULOWANY=1, ANULOPIS='Wycofanie akceptacji dok. sprzedazy' where REF=new.rkdoknag;
        end else
          exception NAGFA_ACK_SARKDOKNAG;
     end
    end
    /* sprawdzenie, czy są wystawione recznie dok. mag po akceptacji*/
    cnt = null;
    select count(*) from DOKUMNAG where FAKTURA = new.ref and ZRODLO = 2 into :cnt;
    if(:cnt > 0) then exception NAGFA_AKC_SADOKMAGRECZ;
    /* usuniecie dokumentów magazynowych wystawionych automatycznie do dokumentu sprzedazy */
    if(new.akceptacja = 9) then begin
      for select DOKUMNAG.REF from DOKUMNAG join defdokum on (DOKUMNAG.TYP = DEFDOKUM.SYMBOL)
      where DOKUMNAG.FAKTURA = new.ref and zrodlo = 3
      order by DEFDOKUM.wydania desc
      into :dokmagref
      do begin
        update DOKUMNAG set DOKUMNAG.akcept = 0, DOKUMNAG.blokada = 2 where REF = :dokmagref;
        delete from DOKUMPOZ where DOKUMENT = :dokmagref;
      end
    end else if(new.akceptacja < 10) then
      for select DOKUMNAG.REF from DOKUMNAG join defdokum on (DOKUMNAG.TYP = DEFDOKUM.SYMBOL)
      where DOKUMNAG.FAKTURA = new.ref and zrodlo = 3
      order by DEFDOKUM.wydania desc
      into :dokmagref
      do begin
        if(new.numblockget > 1 and new.numblock > 0) then
          update DOKUMNAG set NUMBLOCKGET = -new.numblock where ref=:dokmagref;
        update dokumnag set akcept = 0 where ref=:dokmagref;
        delete from DOKUMNAG where REF = :dokmagref;
      end
    if(new.akceptacja < 10 and (new.grupadok <> new.ref)) then begin
      /*odtworzenie ilosci na zamowieniach zaleznych od faktury*/
      for select POZZAM.ref from POZZAM join NAGZAM on (NAGZAM.REF = POZZAM.ZAMOWIENIE)
      where NAGZAM.FAKTURA = new.grupadok and NAGZAM.fakdepend = 1
      into :pozzamref
      do begin
        execute procedure POZZAM_OBLILOSCFROMFAK(:pozzamref);
      end
    end
    /*usunicie not magazynowych */
    if(new.akceptacja < 10) then begin
      for select ref from DOKUMNOT where FAKTURA = new.ref
      order by ref desc
      into :dokmagref
      do begin
        update DOKUMNOT set AKCEPT = 0 where REF=:dokmagref;
        delete from DOKUMNOT where ref=:dokmagref;
      end
    end
    cnt = null;
    /*usuniecie wpisów w rozrachunkach*/
    update fsclracch set status = 0 where STABLE = 'NAGFAK' and SREF = new.ref;
    delete  from fsclracch where STABLE='NAGFAK' and SREF=new.ref;
    delete from ROZRACHP where FAKTURA = new.ref and SKAD = 1;
    select count(*) from ROZRACHP  where ROZRACHP.FAKTURA = new.ref and skad <> 6 and SKAD <> 3 into :cnt;
    if(:cnt > 0) then
         exception NAGFAF_ODAKCPET_BYLY_ZAPLATY;
    delete from ROZRACH where FAKTURA=new.ref and SKAD = 1;
     /* nalozenie blokad na magazynie*/
    select TYPFAK.ZAKUP, TYPFAK.BLOKADA from TYPFAK join NAGFAK on (NAGFAK.TYP = TYPFAK.SYMBOL) where NAGFAK.REF=new.ref into :typzakup, :typblok;
    if(:typblok = 1 and :typzakup <> 1 and new.skad<>2 and new.skad<>1)then
      for select REF from POZFAK
        where DOKUMENT = new.ref into :pozfakref
      do
        execute procedure REZ_SET_KTM_FAK(:pozfakref,2);
     /*aktualizacja znacznikow rozliczenia magazynowego*/
     execute procedure NAGFAK_OBL_MAGROZLICZ(new.ref);
  end
end^
SET TERM ; ^
