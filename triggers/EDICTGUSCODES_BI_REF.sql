--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDICTGUSCODES_BI_REF FOR EDICTGUSCODES                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EDICTGUSCODES')
      returning_values new.REF;
end^
SET TERM ; ^
