--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHG_BD_ORDER FOR DEFCECHG                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update DEFCECHG set ord = 0, lp = lp - 1
    where ref <> old.ref and lp > old.lp;
end^
SET TERM ; ^
