--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDECLARATIONS_AI_FILL_KONFIG FOR EDECLARATIONS                  
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable edeclposdef integer;
declare variable sourcename varchar(255);
declare variable pvalue varchar(255);
begin
--DS: dopelnienie wygenerowanej deklaracji wartosciami pobranymi z konfiga
  for
    select ref, sourcename
      from edeclposdefs
      where edecldef = new.edecldef
        and (sourcename starting with '%'  --bez '?', gdyz obsluga wyrazen warunkowych i tak wymaga sprawdzenia konfiga (BS55095)
          or sourcename starting with '='
          or sourcename starting with 'THIS.' ) --PR65868
      into :edeclposdef , :sourcename
  do begin
    execute procedure edeclpos_parse(:sourcename,0,new.ref) returning_values pvalue;
    if (pvalue <> '') then
      insert into edeclpos (edeclaration, edeclposdef, pvalue, origin)
        values (new.ref, :edeclposdef, :pvalue, 1);
  end
end^
SET TERM ; ^
