--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_ZALICZKA_BU FOR NAGFAK                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable wartbru numeric(14,2);
declare variable wartbruzl numeric(14,2);
declare variable korekta smallint;
declare variable zaliczkowy smallint;
begin
  select typfak.korekta,typfak.zaliczkowy from typfak
    where typfak.symbol = new.typ
    into :korekta,:zaliczkowy;
  if((new.akceptacja = 1 and old.akceptacja <> 1) and :zaliczkowy = 1 and :korekta = 1) then begin
    select sum(WARTBRU),sum(WARTBRUZL)
      from NAGFAKZAL
      where FAKTURA = new.ref
      into :wartbru,:wartbruzl;
    if(:wartbru is null) then wartbru = 0;
    if(:wartbruzl is null) then wartbruzl = 0;
    if(new.sumwartbru > :wartbruzl) then
       exception universal 'Zbyt duza wartosc korekty ujemnej - maksymalny zwrot: '||:wartbruzl;
  end
end^
SET TERM ; ^
