--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_BD_DECREES_ORDEF FOR BKDOCS                         
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  update decrees set ord = 0 where bkdoc = old.ref;
end^
SET TERM ; ^
