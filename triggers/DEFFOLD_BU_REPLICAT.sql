--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFFOLD_BU_REPLICAT FOR DEFFOLD                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
 if(new.nazwa <> old.nazwa or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null) or
    new.typ <> old.typ or (new.typ is null and old.typ is not null) or (new.typ is not null and old.typ is null) or
    new.flagi <> old.flagi or (new.flagi is null and old.flagi is not null) or (new.flagi is not null and old.flagi is null) or
    new.uzytk <> old.uzytk or (new.uzytk is null and old.uzytk is not null) or (new.uzytk is not null and old.uzytk is null) or
    new.symbol <> old.symbol or (new.symbol is null and old.symbol is not null) or (new.symbol is not null and old.symbol is null)
 )then
      execute procedure REPLICAT_STATE('DEFFOLD',new.state, new.ref, NULL, NULL, NULL) returning_values new.state;
end^
SET TERM ; ^
