--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_BI0 FOR RKDOKNAG                       
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable data1 timestamp;
  declare variable data2 timestamp;
  declare variable slownik integer;
  declare variable slodef integer;
  declare variable slowymog integer;
  declare variable typ_k varchar(3);
  declare variable typ_b varchar(3);
  declare variable kasabank char;
  declare variable waluta varchar(3);
  declare variable country_currency varchar(3);
  declare variable status integer;
  declare variable electronic integer;
  declare variable stanowisko_walutowe smallint;
  declare variable operacja_walutowa smallint;
begin
  --if (new.numerold is null) then new.numerold = 0;
  select RKSTNKAS.WALUTA, RKSTNKAS.KOD, RKSTNKAS.kasabank, walutowa
    from RKSTNKAS, RKRAPKAS
    where RKRAPKAS.REF = new.RAPORT and RKSTNKAS.KOD = RKRAPKAS.STANOWISKO
    into :waluta, new.STANOWISKO, :kasabank, :stanowisko_walutowe;

  if (new.waluta is null or new.waluta = '') then
    new.waluta = waluta;
  if (new.symbol is null) then new.symbol = '';

  execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
    returning_values country_currency;
  if (new.waluta <> country_currency) then
    new.walutowy = 1;

  if ((new.LP is null) or (new.lp = 0)) then
  begin
    select max(LP) from RKDOKNAG where RAPORT = new.RAPORT into new.LP;
    if (new.LP is null) then new.LP = 0;
    new.LP = new.LP + 1;
  end
  if (new.kwota is null) then new.kwota = 0;
  if (new.kwotazl is null) then new.kwotazl = 0;
  if (new.blokada is null) then new.blokada = 0;

  select PM, TYP_DOK, BTYPDOK, SLOWNIK, SLODEF, SLOWYMOG, WALUTOWA
    from RKDEFOPER where SYMBOL = new.OPERACJA
    into new.pm, :typ_k, :typ_b, :slownik, :slodef, :slowymog, :operacja_walutowa;

  if (kasabank='K') then
    new.typ = typ_k;
  else
    new.typ = typ_b;

  if (new.slodef > 0 and :slownik = 0)
    then exception RK_DOKNAG_NOTSLO;
  if (:slowymog = 1 and (new.slopoz = 0 or (new.slopoz is null))) then
    exception RK_DOKNAG_SLOWYMOG;
  if (:slowymog = 1 and coalesce(new.dla,'')='') then
    exception RK_DOKNAG_BEZDLA;
  if (:slodef is not null and :slodef <> new.slodef ) then exception RK_DOKNAG_SLONOTDEFOPER;

  select electronic, DATAOD, DATADO from RKRAPKAS where ref=new.raport into electronic, data1, data2;
  if (:electronic = 0 and (new.data<:data1 or new.data>:data2)) then exception RK_NIEPRAWIDLOWA_DATA;

  if (new.anulowany is nulL) then new.anulowany = 0;

  if (new.PM = '+') then begin
    new.PRZYCHODZL = new.KWOTAZL;
    new.ROZCHODZL = 0;
    if (waluta <> country_currency) then -- dla rozliczenia stanu waluty
      new.stanwal = new.kwota;
  end else
  begin
    new.PRZYCHODZL = 0;
    new.ROZCHODZL = new.KWOTAZL;
  end
  select status, electronic from RKRAPKAS where REF= new.raport
    into :status, new.electronic;
  if (status = 1) then new.blokada = 1;
  new.status = 0;

  --kontrola waluty, kursu
  if(stanowisko_walutowe = 0 and operacja_walutowa = 0 and (new.waluta <> :waluta or new.kurs <> 1)) then
    exception universal 'Wybrana nieprawidłowa waluta lub podany nieprawidłowy kurs.';
  if(stanowisko_walutowe = 1 and new.waluta <> :waluta) then
    exception universal 'Wybrana nieprawidłowa waluta.';
end^
SET TERM ; ^
