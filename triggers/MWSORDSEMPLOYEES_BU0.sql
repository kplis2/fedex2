--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDSEMPLOYEES_BU0 FOR MWSORDSEMPLOYEES               
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.canceled = 1 and old.timestop is null) then
    new.timestop = current_timestamp(0);
end^
SET TERM ; ^
