--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLOPOZ_AU_WPK FOR SLOPOZ                         
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable ispattern smallint_id;
declare variable slaveref slopoz_id;
begin
  --sprawdzic trzeba ze krotka nie jest wzorcowa czyli na pewno musi nie miec wzorca
  if (new.pattern_ref is null) then
  begin
    --żeby być wzorcową musi należeć do slownika indywidualnego
    select c.pattern
      from slodef s join companies c on ( c.ref = s.company)
      where s.ref = new.slownik
      into :ispattern;
    --jezeli faktycznie wzorcowa to synchronizuj podlegle
    if (ispattern is not null) then
    begin
      --wiec skoro wzorzec to propaguj na zalezne
      for
        select s.ref
          from slopoz s
          where s.pattern_ref = new.ref
          into :slaveref
      do begin
        execute procedure synchronize_slopoz(new.ref, slaveref, old.nazwa, 1);
      end
    end
  end
end^
SET TERM ; ^
