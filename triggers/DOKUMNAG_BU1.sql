--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_BU1 FOR DOKUMNAG                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable koryg smallint;
declare variable korygpow smallint;
declare variable newline varchar(3);
begin
  newline = '
';
  select defdokum.koryg, defdokum.korygpow
    from dokumnag left join defdokum on (defdokum.symbol = dokumnag.typ)
    where dokumnag.ref = new.ref
    into :koryg,:korygpow;
  --jesli to akceptacja, dokument korygujacy i ma miec dowiazanie, jednak nie jest generowany przez fakture automatycznie
  -- bo np. ZZ'ty recznie musza miec dowiazanie, lecz generowane automatuycznie moga jego nei miec.
  if((new.akcept = 1 and old.akcept <> 1) and (:koryg = 1 and :korygpow = 1) and (new.zrodlo <> 3)) then
    if((new.refk is null or new.refk = 0) and (new.ref2 is null or new.ref2 = 0) ) then
      exception universal 'Dokument nie posiada wymaganego dowiązania.'||newline||'Akceptacja niemożliwa.';
end^
SET TERM ; ^
