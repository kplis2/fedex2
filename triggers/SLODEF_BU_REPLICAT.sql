--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLODEF_BU_REPLICAT FOR SLODEF                         
  ACTIVE BEFORE UPDATE POSITION 1 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if((new.ref <> old.ref)
   or (new.nazwa <> old.nazwa)
   or(new.predef <> old.predef)
   or(new.typ <> old.typ ) or (new.typ is null and old.typ is not null) or (new.typ is not null and old.typ is null)
   or(new.crm <> old.crm ) or (new.crm is null and old.crm is not null) or (new.crm is not null and old.crm is null)
   or(new.kasa <> old.kasa ) or (new.kasa is null and old.kasa is not null) or (new.kasa is not null and old.kasa is null)
   or(new.prefixfk <> old.prefixfk ) or (new.prefixfk is null and old.prefixfk is not null) or (new.prefixfk is not null and old.prefixfk is null)
   or(new.gridname <> old.gridname ) or (new.gridname is null and old.gridname is not null) or (new.gridname is not null and old.gridname is null)
   or(new.gridnamedict <> old.gridnamedict ) or (new.gridnamedict is null and old.gridnamedict is not null) or (new.gridnamedict is not null and old.gridnamedict is null)
   or(new.formname <> old.formname ) or (new.formname is null and old.formname is not null) or (new.formname is not null and old.formname is null)
   or(new.firma <> old.firma ) or (new.firma is null and old.firma is not null) or (new.firma is not null and old.firma is null)
   or(new.trybred <> old.trybred ) or (new.trybred is null and old.trybred is not null) or (new.trybred is not null and old.trybred is null)
   or(new.mag <> old.mag ) or (new.mag is null and old.mag is not null) or (new.mag is not null and old.mag is null)
   or(new.opak <> old.opak ) or (new.opak is null and old.opak is not null) or (new.opak is not null and old.opak is null)
   or(new.kartoteka <> old.kartoteka ) or (new.kartoteka is null and old.kartoteka is not null) or (new.kartoteka is not null and old.kartoteka is null)
   or(new.analityka <> old.analityka ) or (new.analityka is null and old.analityka is not null) or (new.analityka is not null and old.analityka is null)
   or(new.kodln <> old.kodln ) or (new.kodln is null and old.kodln is not null) or (new.kodln is not null and old.kodln is null)

  )then
   waschange = 1;

  execute procedure rp_trigger_bu('SLODEF',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
