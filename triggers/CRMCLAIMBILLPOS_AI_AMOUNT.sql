--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CRMCLAIMBILLPOS_AI_AMOUNT FOR CRMCLAIMBILLPOS                
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable crmclaim integer;
declare variable amount numeric(14,2);
declare variable suma numeric(14,2);
begin
  crmclaim = 0;
  select crmclaim from crmclaimbills where ref = new.crmclaimbill into :crmclaim;
  if (crmclaim <> 0) then update crmclaimpos set amountbill = new.amount where crmclaim = :crmclaim and crmclaimdef = new.crmclaimdef;

-- Zliczanie pozycji rozliczenia
suma = 0;
  for
    select amount from crmclaimbillpos CA where CA.crmclaimbill = new.crmclaimbill
    into :amount
  do begin
    suma = suma + amount;
  end
  update crmclaimbills CS set CS.amount = :suma where CS.ref = new.crmclaimbill;
end^
SET TERM ; ^
