--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INT_IMP_ZAM_POZYCJE_BI_REF FOR INT_IMP_ZAM_POZYCJE            
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure GEN_REF('INT_IMP_ZAM_POZYCJE')
      returning_values new.ref;
end^
SET TERM ; ^
