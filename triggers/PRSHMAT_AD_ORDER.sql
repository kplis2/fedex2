--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHMAT_AD_ORDER FOR PRSHMAT                        
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  execute procedure order_PRSHMAT(old.sheet, old.opermaster, old.number, old.number);
end^
SET TERM ; ^
