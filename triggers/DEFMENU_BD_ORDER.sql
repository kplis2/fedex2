--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFMENU_BD_ORDER FOR DEFMENU                        
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update defmenu set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and poziom = old.poziom;
end^
SET TERM ; ^
