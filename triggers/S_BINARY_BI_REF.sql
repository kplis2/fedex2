--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_BINARY_BI_REF FOR S_BINARY                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('S_BINARY')
      returning_values new.REF;
end^
SET TERM ; ^
