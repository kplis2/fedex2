--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER UZYKLI_BI FOR UZYKLI                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable local integer;
BEGIN
  if(new.nazwisko is null or (new.nazwisko = ''))then exception UZYKLI_NONAME;
  if(new.imie is null) then new.imie = '';
  if(new.aktywny is null) then new.aktywny = 1;
  new.nazwa = new.imie  ||' '||new.nazwisko;
  if(new.id is null) then new.id = '';
  if(new.id<>'') then begin
    if (exists (select ID from uzykli where uzykli.id = new.id))
      then exception UZYKLI_ID_EXISTS 'Uzytkownik o podanym ID ('||new.id||') juz istnieje! Podaj Inny ID.';
  end
  if (new.pesel='') then new.pesel = null;
  if (new.haslo is not null and new.haslo<>'' and coalesce(char_length(new.haslo),0)<32) then -- [DG] XXX ZG119346
  begin
    new.haslo1 = md5sum('SENTE_' || new.haslo || '_NewPass');
    new.haslo = md5sum(new.haslo);
  end
END^
SET TERM ; ^
