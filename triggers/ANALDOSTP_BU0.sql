--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ANALDOSTP_BU0 FOR ANALDOSTP                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if((new.wersjaref <> old.wersjaref or (new.ktm is null) or (new.wersja is null)) and new.wersjaref is not null and new.wersjaref <> 0) then begin
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  end else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)or (new.wersjaref is null)) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if(new.reczny <> 3 and old.reczny = 3) then new.reczny = 1;
  if(new.sprzedaz is null) then new.sprzedaz = 0;
  if(new.srednia is null) then new.srednia = 0;
  if(new.sredniaw is null) then new.sredniaw = 0;
  if(new.potrzeba is null) then new.potrzeba = 0;
  if(new.reczny is null) then new.reczny = 0;
end^
SET TERM ; ^
