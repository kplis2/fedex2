--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ODEFFORMY_BU0 FOR ODEFFORMY                      
  ACTIVE BEFORE UPDATE POSITION 1 
as
begin
  if(new.state = 1 and old.state = 0) then begin
     if(new.akt = 0) then new.state = 0;
  end
  else begin
    if(new.akt <> old.akt )then begin
      if(new.akt = 0) then
         execute procedure desync_odefformy(old.symbol);
      else new.state = 1;
    end
    if(new.akt = 1 AND
       ( new.TYTUL <> old.tytul or
         new.prefix <> old.prefix or
         new.metoda <> old.metoda or
         new.akcja <> old.akcja )
    )then
       new.state = 1;
    if(new.symbol <> old.symbol and new.akt = 1 and old.akt = 1) then begin
         execute procedure DESYNC_ODEFFORMY(old.symbol);
         new.state = 1;
    end
    if(new.akt = 0) then new.state = 0;
  end
end^
SET TERM ; ^
