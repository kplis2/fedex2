--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTYPOZ_AU0 FOR NOTYPOZ                        
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if((new.bcredit <> old.bcredit)
    or (new.bdebit <> old.bdebit)
    or (new.ammount <> old.ammount)
    or (new.ammountpaid <> old.ammountpaid)
    or (new.interest <> old.interest))
  then begin
    execute procedure NOTYNAG_OBL(new.dokument);
    execute procedure NOTYPOZ_OBL(new.ref,new.dokument);
  end
end^
SET TERM ; ^
