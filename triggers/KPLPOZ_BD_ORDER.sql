--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KPLPOZ_BD_ORDER FOR KPLPOZ                         
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  update kplpoz set ord = 1, numer = numer - 1
    where ref <> old.ref and numer > old.numer and nagkpl = old.nagkpl;
end^
SET TERM ; ^
