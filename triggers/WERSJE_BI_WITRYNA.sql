--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WERSJE_BI_WITRYNA FOR WERSJE                         
  ACTIVE BEFORE INSERT POSITION 9 
AS
declare variable www smallint;
begin
  if (new.nrwersji = 0) then begin
    select coalesce(witryna, 0) from towary where ktm = new.ktm into :www;
    new.witryna = :www;
  end
  else if (new.witryna is null) then new.witryna = 0;
end^
SET TERM ; ^
