--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TABKURSP_BI0 FOR TABKURSP                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.zakupu is null) then new.zakupu = 0;
  if(new.sprzedazy is null) then new.sprzedazy = 0;
  if(new.zakupu > 0 and new.sprzedazy > 0) then
    new.sredni = (new.zakupu + new.sprzedazy)/2;
end^
SET TERM ; ^
