--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_BI_QUANTYTYSTRING FOR MWSACTS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable procsql varchar(1024);
declare variable quantitystring varchar(80);
declare variable quantity numeric(14,4);
begin
  if (new.good is not null and new.quantity > 0) then
  begin
    quantity = new.quantity;
    if (quantity < 0) then quantity = 0;
    execute procedure get_config('MWS_QYANTITYS_PROC',2) returning_values(procsql);
    procsql = 'select QUANTITYSTRING from '||procsql||'('''||new.good||''','||new.vers||','||quantity||',1)';
    execute statement procsql into quantitystring;
    new.quantitystring = quantitystring;
  end
end^
SET TERM ; ^
