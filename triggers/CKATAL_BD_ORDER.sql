--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKATAL_BD_ORDER FOR CKATAL                         
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then
    exit;
  select max(numer) from ckatal where cdefkatal = old.cdefkatal and cpodmiot = old.cpodmiot
     into :maxnum;
  if (old.numer = :maxnum) then
    exit;
  update ckatal set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and cdefkatal = old.cdefkatal and cpodmiot = old.cpodmiot;
end^
SET TERM ; ^
