--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVCDEVICES_BI_DANE FOR SRVCDEVICES                    
  ACTIVE BEFORE INSERT POSITION 1 
AS
declare variable tokresp integer;
declare variable odbiorca varchar(255);
declare variable ulica varchar(80);
declare variable nrdomu nrdomu_id;
declare variable nrlokalu nrdomu_id;
declare variable miasto varchar(80);
declare variable kodp varchar(10);
declare variable sprzedawca varchar(255);
begin
  if (new.dokumpozref is not null) then
  begin
    select t.srvdaysinterval
      from towary t
      where t.ktm = new.ktm
    into :tokresp;

    select (case when dn.odbiorca is not null and dn.odbiorca <> '' then dn.odbiorca else k.nazwa end) nazwaodb,
           (case when dn.dulica is not null and dn.dulica <> '' then dn.dulica else k.ulica end) ulica,
           (case when dn.dnrdomu is not null and dn.dnrdomu <> '' then dn.dnrdomu else k.nrdomu end) nrdomu,
           (case when dn.dnrlokalu is not null and dn.dnrlokalu <> '' then dn.dnrlokalu else k.nrlokalu end) nrlokalu,
           (case when dn.dmiasto is not null and dn.dmiasto <> '' then dn.dmiasto else k.miasto end) miasto,
           (case when dn.dkodp is not null and dn.dkodp <> '' then dn.dkodp else k.kodp end) kodp,
           (case when dn.sprzedawca is null then (select op.nazwa from operator op where op.ref = dn.operakcept)
            else (select sp.nazwa from sprzedawcy sp where sp.ref = dn.sprzedawca) end) as sprzedawca
      from dokumnag dn join dokumpoz dp on dn.ref = dp.dokument
                       join klienci k on dn.klient = k.ref
      where dp.ref = new.dokumpozref
    into :odbiorca, :ulica, :nrdomu, :nrlokalu, :miasto, :kodp, :sprzedawca;

    new.srvcourse = :tokresp;
    new.companyname = :odbiorca;
    new.street = :ulica;
    new.houseno = :nrdomu;
    new.localno = :nrlokalu;
    new.city = :miasto;
    new.srvpostcode = :kodp;
    new.seller = substring(:sprzedawca from  1 for  60);
    if (new.status is null) then new.status = 0;
    if (new.warrtype is null) then new.warrtype = 0;
  end
end^
SET TERM ; ^
