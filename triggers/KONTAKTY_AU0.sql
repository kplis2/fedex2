--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONTAKTY_AU0 FOR KONTAKTY                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.ckontrakt<>old.ckontrakt or (new.ckontrakt is not null and old.ckontrakt is null) or (new.ckontrakt is null and old.ckontrakt is not null)) then begin
    if(old.ckontrakt is not null) then execute procedure CKONTRAKTY_UPDATE_CPODMIOTYKON(old.ckontrakt);
    if(new.ckontrakt is not null) then execute procedure CKONTRAKTY_UPDATE_CPODMIOTYKON(new.ckontrakt);
  end
end^
SET TERM ; ^
