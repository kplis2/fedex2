--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRACTSASSIGN_AI0 FOR ECONTRACTSASSIGN               
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable prlistnag integer;
declare variable prlist integer;
begin
  for
    select en.ref, en.epresencelist
      from epresencelistnag en
        join epresencelists el on (el.ref = en.epresencelist)
      where en.employee = new.employees
        and cast(en.startat as date) <= current_date
        and el.status < 1
      into :prlistnag, :prlist
  do begin
    if (not exists(select ref from epresencelistspos
        where epresencelistnag = :prlistnag
          and epresencelists = :prlist
          and econtractsdef = new.econtractsdef
          and employee = new.employees and accord = new.accord)) then
      insert into epresencelistspos (EPRESENCELISTNAG, epresencelists, econtractsdef,
          operator, accord, timestartstr, timestopstr, employee)
        values (:prlistnag, :prlist ,new.econtractsdef,
            new.operator,new.accord,null,null,new.employees);
  end
end^
SET TERM ; ^
