--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVDEFPOSTRANS_BI_ORDER FOR SRVDEFPOSTRANS                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable maxnum integer;
begin
  new.ord = 1;
  select max(number) from srvdefpostrans where srvdefpos = new.srvdefpos
    into :maxnum;
  if (new.number is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update srvdefpostrans set ord = 0, number = number + 1
      where number >= new.number and srvdefpos = new.srvdefpos;
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
