--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKPOZOPER_BI0 FOR RKPOZOPER                      
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable slotyp varchar(15);
begin
  select SLODEF.TYP
    from slodef
      JOIN RKDEFOPER on ( SLODEF.ref = RKDEFOPER.slodef)
    where RKDEFOPER.symbol = new.operacja
    into :slotyp;
  if(:slotyp is null) then
  begin
    select RKDEFOPER.slowymog
      from RKDEFOPER where symbol = new.operacja
      into :slotyp;
    if(:slotyp <> '1') then begin
      slotyp = null;
    end
  end
  if (new.konto = '') then
    new.konto = null;
  if(:slotyp is null and new.typ > 0)then exception RKPOZOPER_NOTSLO;
  if(:slotyp <> 'KLIENCI' and :slotyp <> 'DOSTAWCY' and new.typ > 1) then exception RKPOZOPER_NOTKLI;
  if (new.pm is null or (new.pm <> '+' and new.pm <> '-')) then
    exception RKPOZOPER_PM new.pm;
end^
SET TERM ; ^
