--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TYPZAM_BI0 FOR TYPZAM                         
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.magazyn = '') then new.magazyn = null;
  if(new.mag2 = '') then new.mag2 = null;
  if(new.danedod = '') then new.danedod = null;
  if(new.wydania in(0,1) and new.prschedule = 1) then exception typzam_prschedule;
  if (new.walutowe is null) then new.walutowe = 0;
  if(new.outonpozzamenabled is null) then new.outonpozzamenabled = 0;
end^
SET TERM ; ^
