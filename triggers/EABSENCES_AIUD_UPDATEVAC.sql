--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_AIUD_UPDATEVAC FOR EABSENCES                      
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 3 
AS
  declare variable vc integer;
  declare variable vc2 integer;
  declare variable dwp integer;
  declare variable dwnp integer;
  declare variable vacyear integer;
  declare variable vmc smallint;
begin

  if (deleting or inserting or new.correction <> old.correction
    or coalesce(new.vacsecs,new.worksecs,0) <> coalesce(old.vacsecs,old.worksecs,0)
    or new.ecolumn <> old.ecolumn or new.employee <> old.employee or new.ayear <> old.ayear
    or new.workdays is distinct from old.workdays
  ) then begin

    execute procedure get_config('VACCARDYEAR', 2) returning_values :vacyear;
    execute procedure get_config('VACCOLUMN', 2) returning_values :vc;
    execute procedure get_config('VACREQCOLUMN', 2) returning_values :vc2;
    execute procedure get_config('VACMDCOLUMN', 2) returning_values :vmc;
    execute procedure get_config('VACUCPPAYCOLUMN', 2) returning_values dwp;
    execute procedure get_config('VACUCPNOPAYCOLUMN', 2) returning_values dwnp;
  
    if (old.ayear >= vacyear) then
    begin
      if (old.ecolumn = vc) then
        update evaclimits set limitcons = limitcons - coalesce(old.vacsecs,old.worksecs,0)/ 60
           where vyear = old.ayear and employee = old.employee;
-- Update kart urlopowych przy zmianie urlopu na zadanie i 188kp PR60316
      if (old.ecolumn = vc2) then
        update evaclimits set limitcons = limitcons - coalesce(old.vacsecs,old.worksecs,0)/ 60,
                              vacreqused = vacreqused - coalesce(old.workdays,0)
          where vyear = old.ayear and employee = old.employee;
      if (old.ecolumn = vmc) then
        update evaclimits set limitcons = limitcons - coalesce(old.vacsecs,old.worksecs,0)/ 60,
                      vacmdused = vacmdused - coalesce(old.workdays,0)
          where vyear = old.ayear and employee = old.employee;

    --obsluga przypadku, gdy zmiana z new.correction = 1 na old.correction = 0 badz odwrotnie:
      if (updating and (new.correction + old.correction) = 1) then
      begin
        if(new.ecolumn = vc) then
          update evaclimits
            set limitcons = limitcons + (old.correction - new.correction) * coalesce(old.vacsecs,old.worksecs,0) / 60
            where vyear = old.ayear and employee = old.employee;
        if (new.ecolumn = vc2) then
          update evaclimits
            set limitcons = limitcons + (old.correction - new.correction) * coalesce(old.vacsecs,old.worksecs,0) / 60,
                vacreqused = vacreqused + (old.correction - new.correction) * coalesce(old.workdays,0)
            where vyear = old.ayear and employee = old.employee;
        if (new.ecolumn = vmc) then
          update evaclimits
            set vacmdused = vacmdused + (old.correction - new.correction) * coalesce(old.workdays,0)
            where vyear = old.ayear and employee = old.employee;
      end

      if (old.ecolumn in (10,260,300,330)) then
        execute procedure e_vac_card(old.ayear || '/1/1', old.employee);

      if(old.ecolumn = dwp) then
        update evaclimitsucp
          set paylimitcons = paylimitcons - old.workdays
        where vyear = old.ayear and emplcontract = old.emplcontract;

      if(old.ecolumn = dwnp) then
        update evaclimitsucp
          set nopaylimitcons = nopaylimitcons - old.workdays
        where vyear = old.ayear and emplcontract = old.emplcontract;
    end
  
    if (new.ayear >= vacyear) then
    begin
      if (new.ecolumn = vc) then
        update evaclimits set limitcons = limitcons + coalesce(new.vacsecs,new.worksecs,0) / 60
          where vyear = new.ayear and employee = new.employee;
      if (new.ecolumn = vc2) then
        update evaclimits set limitcons = limitcons + coalesce(new.vacsecs,new.worksecs,0) / 60,
        vacreqused = vacreqused + coalesce(new.workdays,0)
          where vyear = new.ayear and employee = new.employee;
      if (new.ecolumn = vmc) then
        update evaclimits set vacmdused = vacmdused + coalesce(new.workdays,0)
          where vyear = new.ayear and employee = new.employee;

      if (new.ecolumn in (10,260,300,330)) then
        execute procedure e_vac_card(new.ayear || '/1/1', new.employee);

      if(new.ecolumn = dwp) then
        update evaclimitsucp
          set paylimitcons = paylimitcons + new.workdays
        where vyear = new.ayear and emplcontract = new.emplcontract;

      if(new.ecolumn = dwnp) then
        update evaclimitsucp
          set nopaylimitcons = nopaylimitcons + new.workdays
        where vyear = new.ayear and emplcontract = new.emplcontract;
    end
  end
end^
SET TERM ; ^
