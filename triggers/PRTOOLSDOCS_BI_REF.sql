--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSDOCS_BI_REF FOR PRTOOLSDOCS                    
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (coalesce(new.ref,0) = 0) then
    execute procedure gen_ref('PRTOOLSDOCS') returning_values new.ref;
end^
SET TERM ; ^
