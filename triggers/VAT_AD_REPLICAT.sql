--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VAT_AD_REPLICAT FOR VAT                            
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
    execute procedure RP_TRIGGER_AD('VAT', old.GRUPA, NULL, NULL, NULL, NULL, old.token, old.STATE);
end^
SET TERM ; ^
