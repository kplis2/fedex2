--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRDACCOUNTINGPOS_BI_REF FOR PRDACCOUNTINGPOS               
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_prdaccountingpos,1);
end^
SET TERM ; ^
