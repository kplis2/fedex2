--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_AU0 FOR POZZAM                         
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable montpojed varchar(4);
declare variable wydania smallint;
declare variable pref integer;
declare variable kilosc numeric(14,4);
declare variable kilkalc numeric(14,4);
declare variable ilosc numeric(14,4);
declare variable ilosctmp numeric(14,4);
declare variable shortages numeric(14,4);
declare variable rquantityto numeric(14,4);
declare variable prodref integer;
declare variable rref integer;
begin
  select t.wydania
    from nagzam n
      join typzam t on (t.symbol = n.typzam)
    where n.ref = new.zamowienie
    into wydania;
  execute procedure GET_CONFIG('MONTAZPOJEDYNCZO',0) returning_values :montpojed;
  if(new.ilosc <> old.ilosc and exists (select ref from nagzam where kpopref = new.ref)) then
  begin
    if(old.ilosc <> 0 and cast(montpojed as integer) <> 1) then
    begin
      --wykonaj dla zamowien innych niz kompletacji lub gdy nie ma przeliczania recznego ilosci
      if (coalesce(wydania,0) <> 2) then
        update nagzam set kilosc = kilosc * (new.ilosc/old.ilosc) where kpopref = new.ref;
    end
  end
  if(new.ilosc <> old.ilosc and new.kilosc > 0 and new.prrecalcdepend = 0 and exists(select ref from prschedguidespos where pozzamref = new.ref)) then
    execute procedure prschedguidespos_amountcalc(new.ref, null);
  -- przeliczenie ilosci na surowcach i zleceniach zaleznych po zmianie ilosci na zleceniu
  if(coalesce(new.ilosc,0) <> coalesce(old.ilosc,0) and wydania = 3 and new.prrecalcdepend = 1) then
  begin
    ilosc = coalesce(new.ilosc,0) - coalesce(old.ilosc,0);
    if (new.out = 0 or new.out = 2) then
    begin
      for
        select p.ref, p.kilorg, p.kilcalcorg, p.shortages, r.ref, r.quantityto
          from relations r
            left join pozzam p on (p.ref = r.sreffrom and r.stablefrom = 'POZZAM')
          where r.stableto = 'POZZAM' and r.srefto = new.ref and r.relationdef = 2 and r.act > 0
            and p.out in (1,2,3)
          into pref, kilosc, kilkalc, shortages, rref, rquantityto
      do begin
        ilosc = coalesce(new.ilosc,0) - coalesce(old.ilosc,0);
        if (ilosc < 0) then
        begin
          if (abs(ilosc) >= rquantityto) then
            ilosctmp = -rquantityto;
          else
            ilosctmp = ilosc;
        end else
          ilosctmp = ilosc;
        if (ilosctmp <> 0) then
        begin
          update relations set quantityto = quantityto + :ilosctmp where ref = :rref;
          ilosctmp = ilosctmp * kilosc;
          ilosctmp = ilosctmp * shortages;
          ilosctmp = ilosctmp / kilkalc;
          update pozzam set ilosc = iif(ilosc + :ilosctmp < 0, 0, ilosc + :ilosctmp), prrecalcdepend = 1, relationtocalc = :rref where ref = :pref;
          ilosc = ilosc - ilosctmp;
        end
      end
    end else if (new.out = 1) then
    begin
      for
        select p.ref, p.kilorg, p.kilcalcorg, p.shortages, r.ref, r.quantityto
          from relations r
            left join pozzam p on (p.ref = r.sreffrom and r.stablefrom = 'POZZAM')
          where r.stableto = 'POZZAM' and r.srefto = new.ref and r.relationdef = 1 and r.act > 0
            and p.out = 0
          into pref, kilosc, kilkalc, shortages, rref, rquantityto
      do begin
        if (ilosc < 0) then
        begin
          if (abs(ilosc) >= rquantityto) then
            ilosctmp = -rquantityto;
          else
            ilosctmp = ilosc;
        end else
          ilosctmp = ilosc;
        update relations set quantityto = quantityto + :ilosctmp where ref = :rref;
        update pozzam set ilosc = ilosc + :ilosctmp, prrecalcdepend = 1, relationtocalc = :rref where ref = :pref;
        ilosc = ilosc - ilosctmp;
        if (ilosc = 0) then
          break;
      end
    end
    ilosc = coalesce(new.ilosc,0) - coalesce(old.ilosc,0);
    execute procedure pr_recalc_relations('POZZAM',new.ref,ilosc,new.relationtocalc,0);
    update pozzam set prrecalcdepend = 0, relationtocalc = null where ref = new.ref;
    if(exists(select ref from prschedguides g where pozzam = new.ref)) then
      execute procedure prschedguidesgroup_amountcalc(new.zamowienie, new.ref, null);
  end
  if (wydania = 3 and new.out = 0) then
  begin
    if (new.anulowano = 1 and old.anulowano = 0) then
    begin
      -- odpiecie pozycji od zlecenia poprzedniego
      execute procedure PR_PRORD_CANCEL_RELATIONS(new.zamowienie, new.ref);
      -- anulowanie przewodnika produkcyjnego
      update prschedguides p set p.anulowano = 1 where p.pozzam = new.ref;
      -- anulowanie zleceń zaleznych
      if (not exists (select first 1 1 from pozzam where zamowienie = new.zamowienie and out = new.out and anulowano = 0)
      ) then
        update nagzam set anulowano = 1 where ref = new.zamowienie;
      for
        select pr.ref
          from PR_POZZAM_CASCADE(new.ref,0,0,0,1) pr
            left join pozzam p on (p.ref = pr.ref)
          where pr.ref <> new.ref and pr.prnagzamout = new.zamowienie and p.out = 1
          into pref
      do begin
        prodref = null;
        select p.ref
          from relations r
            left join pozzam p on (p.ref = r.sreffrom and r.stableto = 'POZZAM')
          where r.srefto = :pref and r.stableto = 'POZZAM' and p.out = 0 and r.relationdef = 1
          into prodref;
        if (prodref is not null) then
          update pozzam set anulowano = 1 where ref = :prodref;
      end
    end
  end
end^
SET TERM ; ^
