--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KPLNAG_BI0 FOR KPLNAG                         
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.typmag is null) then new.typmag = 0;
  new.dataotw = current_date;
  if(new.akt is null) then new.akt = 1;
  if(new.glowna is null) then new.glowna = new.akt;
  if(new.akt = 0) then new.glowna = 0;
  if(new.nazwa is null) then new.nazwa = '';
end^
SET TERM ; ^
