--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFDZIALATR_AI_REPLICAT FOR DEFDZIALATR                    
  ACTIVE AFTER INSERT POSITION 2 
as
begin
  if (new.DZIAL is not null) then
    UPDATE DZIALY set STATE=-2 where ref=new.DZIAL;
  if (new.klasa is not null) then
    update DEFKLASY set STATE=-2 where ref=new.klasa;
end^
SET TERM ; ^
