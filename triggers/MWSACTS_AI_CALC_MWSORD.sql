--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_AI_CALC_MWSORD FOR MWSACTS                        
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable maxnumber integer;
declare variable weight numeric(14,4);
declare variable volume numeric(14,4);
declare variable timestartdecl timestamp;
declare variable timestopdecl timestamp;
declare variable closemode smallint;
declare variable posquantity integer;
declare variable maxstatus smallint;
declare variable maxactsstatus integer;
begin
  if (new.status <> 6) then   --Marcin F.
    select sum(weight),sum(volume), min(timestartdecl), min(timestopdecl), max(number)
      from mwsacts
      where mwsord = new.mwsord
      into weight, volume, timestartdecl, timestopdecl, maxnumber;
  if (weight is null) then weight = 0;
  if (volume is null) then volume = 0;
  select status from mwsords where ref = new.mwsord into maxstatus;
  -- oblcizenie statusu naglowka zlecenia
  if (new.status > 0 and new.status < 6) then
  begin
    select mwsordtypes.closemode
      from mwsords
        join mwsordtypes on (mwsordtypes.ref = mwsords.mwsordtype)
      where mwsords.ref = new.mwsord
      into closemode;
    if (closemode = 1) then
    begin
      select max(status) from mwsacts where status < 6 and mwsord = new.mwsord
        into maxactsstatus;
      if (maxstatus is null) then maxstatus = 0;
      execute procedure mws_mwsord_status_fromacts(new.ref,new.mwsord,0,null,maxactsstatus, null)
        returning_values posquantity;
      if (posquantity = 0) then
        maxstatus = maxactsstatus;
    end
  end
  update mwsords set status = :maxstatus, mwsactsnumber = :maxnumber,
      weight = :weight, volume = :volume, timestartdecl = :timestartdecl,
      timestopdecl = :timestopdecl
    where ref = new.mwsord;
end^
SET TERM ; ^
