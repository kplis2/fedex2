--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INTRASTATP_BI FOR INTRASTATP                     
  ACTIVE BEFORE INSERT POSITION 1 
as
  declare variable varsumvalue numeric(14,4);
  declare variable varsumvaluestat numeric(14,4);
  declare variable varposcount integer;
  declare variable intrastathstatus smallint;
begin
  select coalesce(ith.sumval,0), coalesce(ith.sumvalstat,0), coalesce(ith.poscount,0), coalesce(ith.statusflag,0)
    from intrastath ith
    where new.intrastath = ith.ref
  into  :varsumvalue, :varsumvaluestat, :varposcount, :intrastathstatus;

  if (:intrastathstatus <> 0) then        -- różny od statusu 'Otwarty'
    exception intrastat_docum_notopened;
  else begin
    if(new.val is null) then new.val = 0;
    if(new.valstat is null) then new.valstat = 0;
    if(new.statusflag is null) then new.statusflag = 0;

    varsumvalue = :varsumvalue + new.val;
    varsumvaluestat = :varsumvaluestat + new.valstat;
    varposcount = :varposcount + 1;

    update intrastath ith set
      ith.sumval = :varsumvalue,
      ith.sumvalstat = :varsumvaluestat,
      ith.poscount = :varposcount
    where ith.ref = new.intrastath;
  end
end^
SET TERM ; ^
