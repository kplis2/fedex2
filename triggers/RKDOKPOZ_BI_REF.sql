--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_BI_REF FOR RKDOKPOZ                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('RKDOKPOZ')
      returning_values new.REF;
end^
SET TERM ; ^
