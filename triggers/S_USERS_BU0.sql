--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_USERS_BU0 FOR S_USERS                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
   if(new.userpass <> old.userpass) then begin
     new.last_change  = current_timestamp(0);
   end
end^
SET TERM ; ^
