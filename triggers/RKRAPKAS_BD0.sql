--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKRAPKAS_BD0 FOR RKRAPKAS                       
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable ile integer;
declare variable local smallint;
begin
  execute procedure CHECK_LOCAL('RKRAPKAS',old.ref) returning_values :local;
  if(:local = 1) then begin
    select count(*) from RKDOKNAG where raport=old.ref into :ile;
    if (:ile>0) then exception RK_RAPORT_POSIADA_DOKUMENTY;
  end
end^
SET TERM ; ^
