--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHMEASUREACTS_BI_ORDER FOR PRSHMEASUREACTS                
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable maxnum integer;
begin
  new.ord = 1;
  select max(number)
    from PRSHMEASUREACTS p
    where p.prshoper = new.prshoper
    into :maxnum;
  if (new.number is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update PRSHMEASUREACTS p set p.ord = 0, p.number = p.number + 1
      where p.number >= new.number and p.prshoper = new.prshoper;
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
