--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWPLIKI_AU_REPLICAT FOR TOWPLIKI                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.typ <> old.typ or (new.typ is null and old.typ is not null) or (new.typ is not null and old.typ is null) or
     new.plik <> old.plik or (new.plik is null and old.plik is not null) or (new.plik is not null and old.plik is null)
   )then
     update WERSJE set STATE=-1 where ktm = new.ktm and nrwersji = new.wersja;
end^
SET TERM ; ^
