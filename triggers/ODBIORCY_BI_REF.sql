--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ODBIORCY_BI_REF FOR ODBIORCY                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ODBIORCY')
      returning_values new.REF;
end^
SET TERM ; ^
