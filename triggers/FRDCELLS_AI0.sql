--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDCELLS_AI0 FOR FRDCELLS                       
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable calcmeth smallint;
declare variable hierorder integer;
declare variable frpsn integer;
declare variable frdperspect integer;
begin
  execute procedure FRDBOTTOMUP_DEPGEN(new.frdperspects, new.elemhier, new.frdhdrver,
      new.ref, new.timelem, new.frdpersdimhier,new.frdpersdimval);
end^
SET TERM ; ^
