--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EZUSDATA_AIUD_EVACLIMITS FOR EZUSDATA                       
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 
AS
  declare variable todate date;
  declare variable vacyear integer;
  declare variable cyear integer;
  declare variable fyear integer;
  declare variable tyear integer;
  declare variable vyear integer;
  declare variable year_iterator integer;
  declare variable employee integer;
begin
  --DS: jezeli zmienilo sie cos z niepelnosprawnoscia, przelicz karty urlopowe w danym okresie
  if (inserting or deleting or
    new.invalidity <> old.invalidity or new.invalidity is null and old.invalidity is not null  or new.invalidity is not null and old.invalidity is null or
    new.invalidityfrom <> old.invalidityfrom or new.invalidityfrom is null and old.invalidityfrom is not null or new.invalidityfrom is not null and old.invalidityfrom is null or
    new.invalidityto <> old.invalidityto or new.invalidityto is null and old.invalidityto is not null or new.invalidityto is not null and old.invalidityto is null or
    new.fulldim <> old.fulldim or new.fulldim is null and old.fulldim is not null or new.fulldim is not null and old.fulldim is null
  ) then
  begin
    select min(fromdate) - 1
      from ezusdata
      where person = coalesce(new.person,old.person)
        and fromdate > coalesce(new.fromdate,new.fromdate)
      into :todate;

    execute procedure get_config('VACCARDYEAR',1)
    returning_values :vacyear;

  /*
   *  cyear - aktualny rok
   *  fyear - rok daty waznosci
   *  tyear - rok zakonczenia waznosci biezacego wpisu
   *  vyear - rok do ktorego nalezy przeliczyc karty (data do lub aktualny rok)
   */

  cyear = cast (extract (year from current_date) as integer);
  fyear = cast (extract (year from coalesce(new.fromdate,old.fromdate)) as integer);

  tyear = cyear;
  if (todate is not null) then
    tyear = cast (extract (year from todate) as integer);

  if (tyear < cyear) then
    vyear = tyear;
  else
    vyear = cyear;

  year_iterator = fyear;

  while (year_iterator <= vyear)
  do begin
    if (year_iterator >= vacyear) then
    begin
      for
        select ref
          from employees
          where person = coalesce(new.person,old.person)
          into :employee
      do
        execute procedure e_vac_card (:year_iterator || '/1/1', :employee);
    end
    year_iterator = year_iterator + 1;
  end
  end
end^
SET TERM ; ^
