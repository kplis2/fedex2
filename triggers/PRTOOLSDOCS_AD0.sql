--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSDOCS_AD0 FOR PRTOOLSDOCS                    
  ACTIVE AFTER DELETE POSITION 0 
as
  declare variable numberg varchar(20);
  declare variable blockref integer;
begin
  if (coalesce(old.number,-1) > 0) then
  begin
    select p.numbergen
      from prtoolsdefdocs p
      where p.prdsdef = old.prdsdef
        and p.symbol = old.prtoolsdefdoc
      into :numberg;

    if (:numberg is null) then
      exception prtoolsdocs_error 'Brak zdefiniowanego numeratora';

    execute procedure free_number(:numberg, old.prdsdef, old.prtoolsdefdoc, old.docdate, old.number, 0)
      returning_values :blockref;
  end
end^
SET TERM ; ^
