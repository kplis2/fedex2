--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVREQUESTS_AU_REMONTY FOR SRVREQUESTS                    
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable PRREPAIRS smallint;
begin
  select PRREPAIRS from CONTRTYPES where REF=new.CONTRTYPE into :PRREPAIRS;
  if (:PRREPAIRS>0 and (new.SYMBOL<>old.SYMBOL or new.STATUS<>old.STATUS)) then begin
    update PRMACHREPAIRS set
        STATUS=new.STATUS,
        SYMBOL=new.SYMBOL
      where SRVREQUEST=new.REF;
  end
end^
SET TERM ; ^
