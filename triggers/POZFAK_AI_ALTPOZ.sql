--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAK_AI_ALTPOZ FOR POZFAK                         
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable docsetsautoadd smallint;
declare variable kplnagref integer;
declare variable skad smallint;
declare variable fromnagzam integer;
declare variable korekta smallint;
begin
  execute procedure getconfig('DOCSETSAUTOADD')
    returning_values :docsetsautoadd;
  if (:docsetsautoadd is null) then docsetsautoadd = 0;

  select coalesce(skad,0), fromnagzam, t.korekta
    from nagfak n
      left join typfak t on (n.typ = t.symbol)
    where n.ref = new.dokument
  into skad, :fromnagzam, :korekta;

  if (:korekta > 0 and
      exists(select first 1 1 from towary t where t.ktm = new.ktm and usluga <> 1 and coalesce(t.altposmode,0) > 0) and
      exists(select first 1 1 from pozfak p where p.ref = new.refk and coalesce(p.havefake,0) = 0) and
      exists(select first 1 1 from defmagaz m where m.symbol = new.magazyn and m.mws = 1)) then
    exception FAKE_NOT_ALLOW 'Operacja niedozwolona! Wystaw korektę na magazyn innego typu niż MWS';

  if (:korekta = 0 and coalesce(new.fake,0) = 0 and :skad = 0 and :fromnagzam is null and new.orgpozfak is null and
      :docsetsautoadd > 0 and new.refparpro is null and
      exists(select first 1 1 from towary where ktm = new.ktm and usluga <> 1 and coalesce(altposmode,0) > 0)) then
  begin
    select first 1 ref from kplnag
      where ktm = new.ktm and wersjaref = new.wersjaref and akt = 1 and glowna = 1
    into :kplnagref;

    if (:kplnagref is not null) then
    begin
      insert into pozfak(dokument, numer, ord, ktm, wersjaref,
          ilosc, ilosco, iloscm, pilosc,
          magazyn, dostawa, alttopoz, fake,
          cenacen, cenabru, cenanet, cenabruzl, cenanetzl)
        select new.dokument, new.numer, 1, p.ktm, p.wersjaref,
            p.ilosc * new.ilosc, p.ilosc * new.ilosco, p.ilosc * new.iloscm, p.ilosc * new.pilosc,
            new.magazyn, new.dostawa, new.ref, 1,
            0, 0, 0, 0, 0
          from kplpoz p
          where p.nagkpl = :kplnagref;
    end

  end

  if (coalesce(new.fake,0) > 0) then
  begin
    update pozfak
      set havefake = 1
      where ref = new.alttopoz;
  end
end^
SET TERM ; ^
