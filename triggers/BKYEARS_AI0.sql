--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKYEARS_AI0 FOR BKYEARS                        
  ACTIVE AFTER INSERT POSITION 0 
as
  declare variable mths varchar(2);
  declare variable mth smallint;
  declare variable mtname varchar(40);
  declare variable sdate date;
  declare variable fdate date;
  declare variable status smallint;
begin
 
  sdate = new.yearid || '/' || new.firstperiod || '/1';
  fdate = sdate;
  insert into bkperiods (yearid, id, ord, name, sdate, fdate, status, ptype,company)
    values (new.yearid, new.yearid || '00', 0, 'Bilans Otwarcia', :sdate, :fdate, 1, 0, new.company);
 

  mth = 0;
  while (mth < new.periodamount) do
  begin
 
    if (new.firstperiod + mth > 12) then
    begin
      sdate = (new.yearid + 1) || '/' || (new.firstperiod + mth - 12) || '/1';
      execute procedure get_month_name(new.firstperiod + mth - 12)
        returning_values mtname;
      mtname = upper(substring(mtname from 1 for 1)) || substring(mtname from 2);
    end else
    begin
      sdate = new.yearid || '/' || (new.firstperiod + mth) || '/1';
      execute procedure get_month_name(new.firstperiod + mth)
        returning_values mtname;
      mtname = upper(substring(mtname from 1 for 1)) || substring(mtname from 2);
    end
 
 
 
    if (new.firstperiod + mth > 11) then
      if (new.firstperiod + mth > 23) then
        fdate = cast((new.yearid + 2) || '/' || (new.firstperiod + mth - 23) || '/1' as date) - 1;
      else
        fdate = cast((new.yearid + 1) || '/' || (new.firstperiod + mth - 11) || '/1' as date) - 1;
    else
      fdate = cast(new.yearid || '/' || (new.firstperiod + mth + 1) || '/1' as date) - 1;
 
    execute procedure get_symbol_from_number(new.firstperiod + mth, 2)
      returning_values mths;
 
    if (mth < 2) then
      status = 1;
    else
      status = 0;
 
    insert into bkperiods (yearid, id, ord, name, sdate, fdate, status, ptype,company)
      values (new.yearid, new.yearid || :mths, :mth + 1, :mtname, :sdate, :fdate, :status, 1, new.company);
    mth = mth + 1;
  end
 
  sdate = fdate;
  execute procedure get_symbol_from_number(new.firstperiod + mth, 2)
    returning_values mths;
  insert into bkperiods (yearid, id, ord, name, sdate, fdate, status, ptype,company)
    values (new.yearid, new.yearid || :mths, :mth + 1, 'Bilans zamknięcia', :sdate, :fdate, :status, 2, new.company);
end^
SET TERM ; ^
