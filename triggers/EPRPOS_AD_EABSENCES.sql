--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRPOS_AD_EABSENCES FOR EPRPOS                         
  ACTIVE AFTER DELETE POSITION 0 
AS
  declare variable ctype varchar(3);
begin
/*MWr: aktualizuj na danej nieobecnosci pracownika, ktorej nr ecolumn usuwamy
       z listy plac, info o rozliczeniu tej nieobecnosci*/

  if (old.cauto <> 2) then
  begin
    select coltype from ecolumns where number = old.ecolumn
      into :ctype;

    if (ctype = 'NIE') then
    begin
      update eabsences set epayroll = null
        where epayroll = old.payroll and employee = old.employee and ecolumn = old.ecolumn;
      update eabsences set corepayroll = null
        where corepayroll = old.payroll and employee = old.employee and ecolumn = old.ecolumn;
    end
  end
end^
SET TERM ; ^
