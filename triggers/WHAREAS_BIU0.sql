--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHAREAS_BIU0 FOR WHAREAS                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.w is null) then new.w = 0;
  if (new.l is null) then new.l = 0;
  if (new.coordxb is null) then new.coordxb = 0;
  if (new.coordxe is null) then new.coordxe = 0;
  if (new.coordyb is null) then new.coordyb = 0;
  if (new.coordye is null) then new.coordye = 0;
  if (new.areatype is null) then new.areatype = 0;
  if (new.logwhareaedist is null) then new.logwhareaedist = 0;
  if (new.logwhareawdist is null) then new.logwhareawdist = 0;
  if (new.logwhareasdist is null) then new.logwhareasdist = 0;
  if (new.logwhareandist is null) then new.logwhareandist = 0;
  if (new.w = 0 and new.coordxb <> new.coordxe) then
    new.w = new.coordxe - new.coordxb;
  if (new.w > 0 and new.coordxb >= 0 and new.coordxe <> new.w + new.coordxb) then
    new.coordxe = new.coordxb + new.w;
  if (new.w > 0 and new.coordxe >= 0 and new.coordxb <> new.coordxe - new.w) then
    new.coordxb = new.coordxe - new.w;
  if (new.l = 0 and new.coordyb <> new.coordye) then
    new.l = new.coordye - new.coordyb;
  if (new.l > 0 and new.coordyb >= 0 and new.coordye <> new.l + new.coordyb) then
    new.coordye = new.coordyb + new.l;
  if (new.l > 0 and new.coordye >= 0 and new.coordyb <> new.coordye - new.l) then
    new.coordxb = new.coordxe - new.w;
  if ((new.maxvertdist is null or new.maxhordist is null) and new.mwsstandseg is not null) then
    select mwsstandsegtypes.maxhordist, mwsstandsegtypes.maxvertdist
      from mwsstandsegs
        join mwsstandsegtypes on (mwsstandsegtypes.ref = mwsstandsegs.mwsstandsegtype)
      where mwsstandsegs.ref = new.mwsstandseg
      into new.maxhordist, new.maxvertdist;
end^
SET TERM ; ^
