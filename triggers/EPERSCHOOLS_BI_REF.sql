--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPERSCHOOLS_BI_REF FOR EPERSCHOOLS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EPERSCHOOLS')
      returning_values new.REF;
end^
SET TERM ; ^
