--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PLATNICY_AU_REPLICAT FOR PLATNICY                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.platnik <> old.platnik or (new.klient <> old.klient))then begin

    update KLIENCI set STATE = -1 where ref = new.klient;
    if(new.klient <> old.klient) then
     update KLIENCI set STATE = -1 where ref = old.klient;
  end

end^
SET TERM ; ^
