--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER GRUPYKLI_AI0 FOR GRUPYKLI                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable aktuoddzial varchar(100);
begin
  if(new.cennik is not null ) then begin
     execute procedure GETCONFIG('AKTUODDZIAL') returning_values :aktuoddzial;
     update defcennik set akt = 1 where ref = new.cennik and akt <> 1 and ((ODDZIAL=:aktuoddzial) or (ODDZIAL='') or (ODDZIAL is NULL));
  end
end^
SET TERM ; ^
