--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_BU_EDI FOR NAGFAK                         
  ACTIVE BEFORE UPDATE POSITION 10 
as
declare variable local smallint;
declare variable gln type of gln_id;
declare variable edipath varchar(255);
declare variable baza varchar(255);
declare variable edocref integer;
declare variable ederulesym varchar(60);
declare variable ederule integer;
declare variable isedocnew integer;
begin
  execute procedure CHECK_LOCAL('NAGFAK',new.ref) returning_values :local;
  --akceptacja
  if((new.akceptacja = 1) and (old.akceptacja =0 or (old.akceptacja >= 9)) and (:local = 1 and new.zakup=0)) then begin

    select edi from typfak where symbol=new.typ into new.edi;

    if(new.edi = 1 and new.klient is not null) then begin
      select gln,edipath from klienci where ref = new.klient into :gln,:edipath;
      if(:gln is null) then new.edi = 0;
      if(:edipath is null) then edipath = '';

    --okreslenie typu ede
      new.editype = null;
      for select eg.ederule, ederules.symbol
        from edegen eg
           join ederules on (eg.ederule = ederules.ref)
        where eg.slodef = 1 and eg.slopoz = new.klient
            and new.data >= eg.fromdate and (new.data <= eg.todate or eg.todate is null)
            and eg.dokumtype = 'NAGFAK'
            into :ederule, :ederulesym
      do begin
        --proba eksportu naglowka dokumentu - ewentualnie procedura weryfikujaca odrzuca dany dokument
        if(:ederule is not null) then begin
          execute procedure EDEDOC_INIT(:ederule,'NAGFAK',new.ref,'','','') returning_values :edocref, :isedocnew;
          if(:edocref > 0) then begin
            new.edi = 1;
            new.editype = :ederulesym;
          end
            else new.edi = 0;
         end
      end
    end else new.edi = 0;
  end
  --wycofanie akceptacji
  if((new.akceptacja <> 1) and (old.akceptacja=1) and (:local = 1 and new.zakup=0)) then begin
    delete from EDEDOCSH where otable='NAGFAK' and oref=new.ref;
    new.edi = 0;
  end
end^
SET TERM ; ^
