--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NUMBERFREE_BI0 FOR NUMBERFREE                     
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable oddzial smallint;
declare variable ISF_company smallint;
begin
  if(new.ref is null or (new.ref = 0)) then
    execute procedure GEN_REF('NUMBERFREE') returning_values new.ref;
  --if (new.oddzial is null or new.oddzial = '') then
  begin                                   
    select ODDZIAL, COMPANY
      from numbergen
      where NAZWA = new.nazwa
      into :oddzial, :ISF_company;

    if(coalesce(:oddzial, 0) != 0) then
      execute procedure get_global_param('AKTUODDZIAL') returning_values new.oddzial;
    if(ISF_company != 0) then
      execute procedure get_global_param('CURRENTCOMPANY') returning_values new.company;
    if (NEW.COMPANY is null) then new.company = -1;
  end
end^
SET TERM ; ^
