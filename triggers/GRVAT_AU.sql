--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER GRVAT_AU FOR GRVAT                          
  ACTIVE AFTER UPDATE POSITION 0 
as
  declare variable slodef integer;
  declare variable currentyear integer;
begin
  -- aktulizacja opisu kont ksiegowych dla bieżącego roku
  if(new.descript<>old.descript) then begin
    currentyear = extract(year from current_timestamp(0));
    for select ref from slodef where typ = 'GRVAT'
      into :slodef
    do begin
      execute procedure accounting_recalc_descript(:slodef, :currentyear, null, new.symbol);
    end
  end
end^
SET TERM ; ^
