--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RP_ITEM_AD0 FOR RP_ITEM                        
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable cnt integer;
begin
    delete from RP_ITEM where PARENT_ID = old.REF_ID and ID_OBJ = old.ID_OBJ;
    update RP_ITEM set DEPENDS=0 where DEPENDS=old.REF_ID;
    select count(*) from RP_ITEM where ID_OBJ = old.ID_OBJ and REF_ID = old.parent_id into :cnt;
    if(:cnt > 0) then begin
      execute procedure order_RP_ITEM(old.id_obj,old.parent_id,0);
    end
end^
SET TERM ; ^
