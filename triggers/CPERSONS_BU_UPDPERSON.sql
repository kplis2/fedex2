--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPERSONS_BU_UPDPERSON FOR CPERSONS                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin

  if (old.symbol is distinct from new.symbol
   or old.company is distinct from new.company
  ) then begin
    update eperscompany
      set symbol = new.symbol, company = new.company
      where person = old.ref and company = old.company;
  end

  if ( old.ref is distinct from new.ref
    or old.person is distinct from new.person
    or old.fname is distinct from new.fname
    or old.middlename is distinct from new.middlename
    or old.sname is distinct from new.sname
    or old.mname is distinct from new.mname
    or old.fathername is distinct from new.fathername
    or old.mothername is distinct from new.mothername
    or old.mothmname is distinct from new.mothmname
    or old.sex is distinct from new.sex
    or old.birthdate is distinct from new.birthdate
    or old.nationality is distinct from new.nationality
    or old.citizenship is distinct from new.citizenship
    or old.foreigner is distinct from new.foreigner
    or old.steadystaycard is distinct from new.steadystaycard
    or old.evidenceno is distinct from new.evidenceno
    or old.evidissuedby is distinct from new.evidissuedby
    or old.passportno is distinct from new.passportno
    or old.passissuedby is distinct from new.passissuedby
    or old.pesel is distinct from new.pesel
    or old.nip is distinct from new.nip
    or old.profession is distinct from new.profession
    or old.education is distinct from new.education
    or old.email is distinct from new.email
    or old.cellphone is distinct from new.cellphone
    or old.activ is distinct from new.activ
    or old.oemail is distinct from new.oemail
    or old.omobile is distinct from new.omobile
    or old.ophone is distinct from new.ophone
    or old.phone is distinct from new.phone
    or old.evidencedate is distinct from new.evidencedate
    or old.passportdate is distinct from new.passportdate
    or old.marstatus is distinct from new.marstatus
    or old.convicted is distinct from new.convicted
    or old.birthplace is distinct from new.birthplace
    or old.expevidencedate is distinct from new.expevidencedate
    or old.exppassportdate is distinct from new.exppassportdate
    or old.ehrm is distinct from new.ehrm
    or old.login is distinct from new.login
  ) then begin
    update persons
      set ref = new.ref,
          person = new.person,
          fname = new.fname,
          middlename = new.middlename,
          sname = new.sname,
          mname = new.mname,
          fathername = new.fathername,
          mothername = new.mothername,
          mothmname = new.mothmname,
          sex = new.sex,
          birthdate = new.birthdate,
          nationality = new.nationality,
          citizenship = new.citizenship,
          foreigner = new.foreigner,
          steadystaycard = new.steadystaycard,
          evidenceno = new.evidenceno,
          evidissuedby = new.evidissuedby,
          passportno = new.passportno,
          passissuedby = new.passissuedby,
          pesel = new.pesel,
          nip = new.nip,
          profession = new.profession,
          education = new.education,
          email = new.email,
          cellphone = new.cellphone,
          activ = new.activ,
          oemail = new.oemail,
          omobile = new.omobile,
          ophone = new.ophone,
          phone = new.phone,
          evidencedate = new.evidencedate,
          passportdate = new.passportdate,
          marstatus = new.marstatus,
          convicted = new.convicted,
          birthplace = new.birthplace,
          expevidencedate = new.expevidencedate,
          exppassportdate = new.exppassportdate,
          ehrm = new.ehrm,
          login = new.login
      where ref = old.ref;
  end
end^
SET TERM ; ^
