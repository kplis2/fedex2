--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCALDAYS_BU_HOUR2MIN FOR EMPLCALDAYS                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  --DS: jezeli sa zaznaczone znaczniki modyf. indywidualna, na podstawie godzin przelicz sekundy
  --a potem to juz mechanizm zrobi reszte
  if (new.customized = 1) then
  begin
    if (new.overtime0str is null or new.overtime0str = '') then new.overtime0str = '0';
    if (new.overtime50str is null or new.overtime50str = '') then new.overtime50str = '0';
    if (new.overtime100str is null or new.overtime100str = '') then new.overtime100str = '0';
    if (new.nighthoursstr is null or new.nighthoursstr = '') then new.nighthoursstr = '0';
    if (new.compensatory_timestr is null or new.compensatory_timestr = '') then new.compensatory_timestr = '0';

    execute procedure hours2minutes(new.overtime0str) returning_values new.overtime0;
    execute procedure hours2minutes(new.overtime50str) returning_values new.overtime50;
    execute procedure hours2minutes(new.overtime100str) returning_values new.overtime100;
    execute procedure hours2minutes(new.nighthoursstr) returning_values new.nighthours;
    execute procedure hours2minutes(new.compensatory_timestr) returning_values new.compensatory_time;

    --mamy wynik w minutach a potrzebujemy w sekundach
    new.overtime0 = new.overtime0 * 60;
    new.overtime50 = new.overtime50 * 60;
    new.overtime100 = new.overtime100 * 60;
    new.nighthours = new.nighthours * 60;
    new.compensatory_time = new.compensatory_time * 60;
  end

  --Obsluga pola 'Godziny odebrane' (BS52333)
  if (coalesce(trim(new.compensated_timestr),'') <> coalesce(trim(old.compensated_timestr),'')) then
  begin
    if (new.compensated_timestr is null or new.compensated_timestr = '') then new.compensated_timestr = '0';
    execute procedure hours2minutes(new.compensated_timestr) returning_values new.compensated_time;
    new.compensated_time = new.compensated_time * 60;
  end
end^
SET TERM ; ^
