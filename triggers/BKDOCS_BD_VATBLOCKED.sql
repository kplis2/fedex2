--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_BD_VATBLOCKED FOR BKDOCS                         
  ACTIVE BEFORE DELETE POSITION 10 
AS
begin
  if (coalesce(old.vatperiod,'') != '') then
    if (exists(select p.id from bkperiods p where p.id = old.vatperiod and p.company = old.company  and p.vatblocked = 1)) then
      exception bkperiods_vatblocked;
end^
SET TERM ; ^
