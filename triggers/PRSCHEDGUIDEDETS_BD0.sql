--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDEDETS_BD0 FOR PRSCHEDGUIDEDETS               
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.accept > 0 and old.ssource <> 3 and old.ssource <> 1 and old.sourceupdate = 0) then
    exception PRSCHEDGUIDEDETS_ERROR 'Rozliczenie zaakceptopwane - usuń źródło rozliczenia';
  if (old.accept > 1 and old.ssource = 1 and old.sourceupdate = 0) then
    exception PRSCHEDGUIDEDETS_ERROR 'Rozliczenie zaakceptopwane - usuwanie niemożliwe';
end^
SET TERM ; ^
