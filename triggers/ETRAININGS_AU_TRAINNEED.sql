--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ETRAININGS_AU_TRAINNEED FOR ETRAININGS                     
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.trainneed is not null and old.trainneed is null or (new.trainneed <> old.trainneed)) then
  begin
    update empltrainneeds N
      set N.etraining = new.ref
      where N.etrainneed = new.trainneed and N.employee in (select T.employee from empltrainings T where T.training = new.ref);

    if (old.trainneed is not null) then
      update empltrainneeds N
        set N.etraining = null
        where N.etrainneed = old.trainneed and N.etraining = old.ref;
  end
  else if (new.trainneed is null and old.trainneed is not null) then
    update empltrainneeds N
      set N.etraining = null
      where N.etrainneed = old.trainneed and N.etraining = old.ref;
end^
SET TERM ; ^
