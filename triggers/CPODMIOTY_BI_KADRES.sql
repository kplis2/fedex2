--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_BI_KADRES FOR CPODMIOTY                      
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable nazwafizycznych varchar(255);
begin
 if(new.historia is null) then new.historia = 0;
 if(new.firma = 1 and new.korespond = 2) then new.korespond = 0;
 if(new.cpaleja is NULL) then new.cpaleja = '';
 if(new.pcpaleja is NULL) then new.pcpaleja = '';
 if(new.nrdomu is NULL) then new.nrdomu = '';
 if(new.pnrdomu is NULL) then new.pnrdomu = '';
 if(new.korespond = 0) then begin
   /* adres firmowy*/
   if(new.cpaleja<>'') then
     new.kulica = new.cpaleja || ' ' || new.ulica || ' ' || new.nrdomu;
   else
     new.kulica = new.ulica || ' ' || new.nrdomu;
   new.kmiasto = new.miasto;
   new.kkodp = new.kodp;
   new.kpoczta = new.poczta;
   new.kkraj = new.kraj;
 end
 if(new.korespond = 2) then begin
   /* adres prywatny */
   if(new.pcpaleja<>'') then
     new.kulica = new.pcpaleja || ' ' || new.pulica || ' ' || new.pnrdomu;
   else
     new.kulica = new.pulica || ' ' || new.pnrdomu;
   new.kmiasto = new.pmiasto;
   new.kkodp = new.pkodp;
   new.kpoczta = new.ppoczta;
   new.kkraj = new.pkraj;
 end
 if(new.imie is null) then new.imie = '';
 if(new.nazwisko is null) then new.nazwisko = '';
 if(new.nazwafirmy is null) then new.nazwafirmy = '';

 if(new.firma = 0) then begin
   if(new.imie<>'' or new.nazwisko<>'') then begin
     execute procedure getconfig('NAZWAFIZYCZNYCH')
       returning_values :nazwafizycznych;
     if(nazwafizycznych = '1') then
       new.nazwa = new.nazwisko || ' ' || new.imie ;
     else
       new.nazwa = new.imie || ' ' || new.nazwisko;
   end else
     new.nazwa = new.nazwafirmy;
 end else begin
   new.nazwisko = new.nazwa;
   new.nazwafirmy = new.nazwa;
   new.imie = '';
 end

 if(new.operuprawn is NULL) then new.operuprawn = '';
 if (new.operopiek is not null) then
   new.prawa = ';' || cast(new.operopiek as varchar(10)) || ';' || new.operuprawn;
 else
   new.prawa = ';' || new.operuprawn;
 execute procedure GET_SUPERIORS(new.prawa) returning_values new.prawa;
 if(new.slodef is not null) then select NAZWA from SLODEF where REF=new.slodef into new.slodefnazwa;
 if(new.operopiek is not null) then select NAZWA from OPERATOR where REF=new.operopiek into new.operopieknazwa;
 if(new.cstatus is not null) then select NAZWA from CSTATUSY where REF=new.cstatus into new.cstatusnazwa;

end^
SET TERM ; ^
