--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_AD_MWSORDSTATUS FOR MWSACTS                        
  ACTIVE AFTER DELETE POSITION 1 
AS
declare variable closemode smallint;
declare variable posquantity integer;
declare variable maxstatus integer;
begin
  if (old.status < 6) then
  begin
    select mwsordtypes.closemode
      from mwsords
        join mwsordtypes on (mwsordtypes.ref = mwsords.mwsordtype)
      where mwsords.ref = old.mwsord
      into closemode;
    if (closemode = 1) then
    begin
      select max(status) from mwsacts where status < 6 and mwsord = old.mwsord
        into maxstatus;
      if (maxstatus is null) then maxstatus = 0;
      execute procedure mws_mwsord_status_fromacts(old.ref,old.mwsord,0,null,maxstatus, null)
        returning_values posquantity;
      if (posquantity = 0) then
        update mwsords set status = old.status where ref = old.mwsord and status <> old.status;
    end
    update mwsords set weight = (select sum(weight) from mwsacts where mwsord = old.mwsord) where ref = old.mwsord; --Marcin F.
    update mwsords set volume = (select sum(volume) from mwsacts where mwsord = old.mwsord) where ref = old.mwsord;
  end
end^
SET TERM ; ^
