--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRACTS_AIUD_EMPLCALENDAR FOR EMPLCONTRACTS                  
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 5 
as
declare variable employee integer;
declare variable maxfromdate date;
begin
--MWr: Ustawienie daty waznosci na kalendarzach parcowniczych

  if (deleting or inserting or
    new.todate <> old.todate or (new.todate is null and old.todate is not null) or (new.todate is not null and old.todate is null) or
    new.enddate <> old.enddate or (new.enddate is null and old.enddate is not null) or (new.enddate is not null and old.enddate is null))
  then begin
    if (deleting) then employee = old.employee;
    else employee = new.employee;

    select max(fromdate) from emplcalendar
      where employee = :employee
      into :maxfromdate;

    update emplcalendar
      set todate = '3000-01-01'
      where employee = :employee
        and fromdate = :maxfromdate;
  end
end^
SET TERM ; ^
