--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDPERSDIMVALS_BI_REF FOR FRDPERSDIMVALS                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FRDPERSDIMVALS')
      returning_values new.REF;
end^
SET TERM ; ^
