--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WERSJE_AU0 FOR WERSJE                         
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable przelicz numeric(14,4);
declare variable cnt integer;
begin
    if(((new.CENA_ZAKB <> old.CENA_ZAKB) or (new.CENA_ZAKN <> old.CENA_ZAKN)
      or (new.SYMBOL_DOST <> old.SYMBOL_DOST) or(new.nrwersji <> old.nrwersji)
      or (new.kodkresk <> old.kodkresk) or (new.kodkresk is not null and old.kodkresk is null) or (new.kodkresk is null and old.kodkresk is not null)
      or (new.CENA_FABB <> old.CENA_FABB) or (new.CENA_FABN <> old.CENA_FABN) or (new.CENA_ZAKNL <> old.CENA_ZAKNL)
      or (new.CENA_KOSZTB <> old.CENA_KOSZTB) or (new.CENA_KOSZTN <> old.CENA_KOSZTN)
      or (new.dniwazn <> old.dniwazn)
      or (new.nrwersji <> old.nrwersji)
    ) and (new.nrwersji = 0)) then begin
       update towary set SYMBOL_DOST = new.SYMBOL_DOST, BKSYMBOL = new.bksymbol, CENA_ZAKN = new.CENA_ZAKN, CENA_ZAKB = new.cena_zakb
        ,CENA_fabB = new.cena_fabb,CENA_fabn = new.cena_fabn
        ,CENA_kosztB = new.cena_kosztb,CENA_kosztn = new.cena_kosztn
        ,DNIWAZN = new.dniwazn
        ,KODKRESK= new.kodkresk
         where KTM = new.KTM;
       if (new.symbol_dost<>old.symbol_dost) then update stanyil set symbol_dost = new.symbol_dost where ktm = new.ktm;

       select TOWJEDN.przelicz
        from TOWJEDN
          join DOSTCEN on
          (DOSTCEN.jedn = TOWJEDN.REF)
        where DOSTCEN.WERSJAREF = new.ref and GL = 1 and dostcen.cenazakgl = 1
        into :przelicz;

       if(:przelicz is not null) then begin
         update DOSTCEN set CENANET = new.CENA_ZAKN * :przelicz, CENABRU = new.CENA_ZAKB * :przelicz,
           CENA_fab = new.cena_fabn * :przelicz,
           CENA_koszt = new.cena_kosztn * :przelicz
           where WERSJAREF = new.ref and CENAZAkGL = 1;
       end
       if(new.symbol_dost <> old.symbol_dost) then
         update DOSTCEN set SYMBOL_DOST = new.SYMBOL_DOST
           where WERSJAREF = new.ref and GL = 1;
    end
    if(new.kodkresk <> old.kodkresk or (new.kodkresk is not null and old.kodkresk is null) or (new.kodkresk is null and old.kodkresk is not null))then begin
       cnt = null;
       select count(*) from towkodkresk where wersjaref = new.ref and gl = 1 into :cnt;
       if(:cnt is null) then cnt = 0;
       if(new.kodkresk <> '') then begin
         if(:cnt > 0) then begin
           update towkodkresk set kodkresk = new.kodkresk where wersjaref = new.ref and gl = 1 and kodkresk <> new.kodkresk;
         end else
           insert into towkodkresk(wersjaref, kodkresk) values(new.ref, new.kodkresk);
       end else begin
         delete from towKODKRESK where WERSJAREF = new.ref;
       end
    end
    if (new.ktm <> old.ktm) then begin
      update towkodkresk set ktm = new.ktm where wersjaref = new.ref;
    end
    if(new.AKT <> old.AKT or new.NAZWA<>old.NAZWA) then begin
       update STANYIL set AKT = new.AKT, WNAZWA = new.NAZWA where WERSJAREF = new.REF;
       update STANYOPK set AKT = new.AKT where WERSJAREF = new.REF;
    end
end^
SET TERM ; ^
