--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHOPERS_BI_ORDER FOR PRSHOPERS                      
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 0;
  if (new.ord = 1 and coalesce(new.number,0) > 0) then begin
    new.ord = 0;
    exit;
  end
  select max(number) from prshopers where sheet = new.sheet
    and coalesce(opermaster, 0) = coalesce(new.opermaster, 0)
    into :maxnum;
  if (maxnum is null) then maxnum = 0;
  if (coalesce(new.number,0) <= 0) then
  begin
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update prshopers set ord = 1, number = number + 1
      where number >= new.number and sheet = new.sheet
        and coalesce(opermaster, 0) = coalesce(new.opermaster, 0);
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
