--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WFTASK_BI FOR WFTASK                         
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_wftask,1);
end^
SET TERM ; ^
