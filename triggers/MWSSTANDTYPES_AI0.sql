--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDTYPES_AI0 FOR MWSSTANDTYPES                  
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable counter integer;
declare variable goodsav smallint_id; -- XXX
begin
  -- zaozenie poziomów regalu
  if (new.levelsquant > 0) then
  begin
    counter = 0;
    goodsav = 1; -- XXX
    while (:counter < new.levelsquant)
    do begin
      -- XXX KBI ustawiamy dopmylny goodsav dla poziomu (poborowa buforowa)
      if (new.x_default_qgoodsav is null) then
        goodsav = 0;
      else begin
        if (counter <= new.x_default_qgoodsav) then
          goodsav = 1;
        else
          goodsav = 0;
      end
      -- XXX KBI Koniec

      if (new.l > 0 and new.mwsstandsegq > 0) then
        insert into mwsstandleveltypes (mwsstandtype, l, number, w, act, mwsconstloctype, x_default_goodsav)   -- XXX KBI Domylny typ lokacji i goodsav
          values (new.ref, new.w, :counter, new.l/new.mwsstandsegq, 1, new.x_default_constloctypes, :goodsav);   -- XXX KBI Domylny typ lokacji i goodsav
      else
        insert into mwsstandleveltypes (mwsstandtype, l, number, w, act, mwsconstloctype, x_default_goodsav)    -- XXX KBI Domylny typ lokacji i goodsav
          values (new.ref, new.w, :counter, 0, 1, new.x_default_constloctypes, :goodsav);          -- XXX KBI Domylny typ lokacji i goodsav
      counter = :counter + 1;
    end
  end
  -- zalozenie segmentow regalu
  if (new.mwsstandsegq > 0) then
  begin
    counter = 0;
    while (:counter < new.mwsstandsegq)
    do begin
      if (new.l > 0 and new.mwsstandsegq > 0) then
        insert into mwsstandsegtypes (mwsstandtype, l, symbol, w, whareatype)
          values (new.ref, new.w, cast(:counter as varchar(2)), new.l/new.mwsstandsegq, new.whareatype);
      else
        insert into mwsstandsegtypes (mwsstandtype, l, symbol, w, whareatype)
          values (new.ref, new.w, cast(:counter as varchar(2)), 0, new.whareatype);
      counter = :counter + 1;
    end
  end
end^
SET TERM ; ^
