--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_AU_DECREES_COMPANY FOR BKDOCS                         
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.company <> old.company) then
    update decrees set decrees.company = new.company where decrees.bkdoc = new.ref;
end^
SET TERM ; ^
