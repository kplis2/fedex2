--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWAKCES_BI0 FOR TOWAKCES                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.Awersjaref is not null and new.awersjaref <> 0) then
    select nrwersji, ktm from WERSJE where ref=new.awersjaref into new.awersja, new.aktm;
  else if(new.aktm is not null and new.awersja is not null) then
    select ref from WERSJE where ktm = new.aktm and nrwersji = new.awersja into new.awersjaref;

  if (new.typ = '') then
    new.typ = null;
end^
SET TERM ; ^
