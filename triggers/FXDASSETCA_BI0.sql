--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FXDASSETCA_BI0 FOR FXDASSETCA                     
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable status smallint;
begin
  if (new.regdate is null) then new.regdate = current_timestamp(0);
  if (new.regoper is null) then
    execute procedure get_global_param('AKTUOPERATOR') returning_values new.regoper;
  if (new.share is null) then new.share = 0;
  if (new.share < 0) then 
    exception fxdassetca_error 'Wartosc udzialu < 0';
  if (new.account is null) then
    exception fxdassetca_error 'Brak konta';
  /*if (not exists(select * from BKSYMBOLS b where b.symbol = new.account and b.sgroup = 'EFK')) then
    exception fxdassetca_error 'Nieprawidowy symbol konta';*/
  select a.status
    from amperiods a
    where a.ref = new.amperiod
    into :status;
  if (status = 2) then
    exception fxdassetca_error 'Okres jest już zamkniety.';

end^
SET TERM ; ^
