--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_BD_CHECKACCOUNTING FOR KLIENCI                        
  ACTIVE BEFORE DELETE POSITION 0 
as
  declare variable dictdef integer;
begin
  select ref from slodef where typ = 'KLIENCI'
    into :dictdef;
  execute procedure check_dict_using_in_accounts(dictdef, old.kontofk, old.company);
end^
SET TERM ; ^
