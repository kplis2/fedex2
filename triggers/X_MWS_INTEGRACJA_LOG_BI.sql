--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWS_INTEGRACJA_LOG_BI FOR X_MWS_INTEGRACJA_LOG           
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_x_mws_INTEGRACJA_LOG,1);
  if (new.DATA is null) then
    new.DATA = current_timestamp(0);
end^
SET TERM ; ^
