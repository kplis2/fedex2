--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER QUERYTYPES_BD FOR QUERYTYPES                     
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if(exists(select first 1 1 from ZAPYTANIA where QUERYTYPE=old.ref)) then
    exception UNIVERSAL 'Istnieją analizy tego typu.';
end^
SET TERM ; ^
