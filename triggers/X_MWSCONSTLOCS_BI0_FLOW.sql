--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWSCONSTLOCS_BI0_FLOW FOR MWSCONSTLOCS                   
  ACTIVE BEFORE INSERT POSITION 99 
AS
declare variable number integer_id;
begin
  -- Dla regaów przepywowych obliczamy grupe lokacji i kolejnisc w grupie (x_flow_group i x_flow_order)
  /*
    Grupa połączonych lokacji w regale przepywowym.
    Zbudowana wg zasady:  SSKKPP
    gdzie:
    SS - ref WHSECS
    KK - numer kolejny palety (MWSSTANDSEGTYPEDICT)
    PP - numer kolejny piętra (MWSSTANDLEVELNUMBER)

  */
  if (new.x_row_type = 1) then begin
    select a.number from whareas a where ref = new.wharea into :number;
    -- hmm, może powinniem sparadzać czy new.mwsstandsegtypedict i new.mwsstandlevelnumber  są uzupenione? Ewentualnie do zrobienia w przyszloci.
    new.x_flow_group = new.whsec||lpad(:number, 2,'0')||lpad(new.mwsstandlevelnumber, 2 ,'0');

    new.x_flow_order = new.whsecrowdict;


  end
end^
SET TERM ; ^
