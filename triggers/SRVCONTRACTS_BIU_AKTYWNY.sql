--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVCONTRACTS_BIU_AKTYWNY FOR SRVCONTRACTS                   
  ACTIVE BEFORE INSERT OR UPDATE POSITION 10 
AS
begin
--Nieaktywny klient nie moze miec aktywnej umowy serwisowej
  --Jezeli wybrano umowe serwisowa z klientem (SRVCONTRACTS.dictdef = 1) to
  if(new.dictdef = 1) then
    --Jezeli nastapila proba aktywowania umowy serwisowej dla nieaktywnego klienta to
    if(new.aktywny=1 and coalesce(old.aktywny,0) = 0 and exists (select aktywny from klienci where ref=new.dictpos and aktywny=0)) then
    begin
      --Nie mozna aktywowac umowy serwisowej dla nieaktywnego klienta)
      exception SRVCONTRACTS_NIEAKTYWNY_KLIENT;
    end
end^
SET TERM ; ^
