--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTURE_BI0 FOR ACCSTRUCTURE                   
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable tmp BKSYMBOL_ID;
  declare variable yearid int;
  declare variable company int;
  declare variable i int;
  declare variable sep char(1);
  declare variable len smallint;
begin
  if (new.dictdef is null) then exception null_dictdef;
  if (new.dictdef is not NULL) then
    select kodln from slodef where (slodef.ref = new.dictdef) into new.len;

  select symbol, yearid, company from bkaccounts where ref=new.bkaccount
    into :tmp, :yearid, :company;
  if (exists(select ref from accounting where yearid = :yearid and company = :company and account starting with :tmp)) then
    exception FK_CANT_UPDATE_ACCSTRUCTURE;

  -- sprawdzenie czy konto nie jest zbyt długie
  select synlen from bkyears
    join bkaccounts on (bkaccounts.yearid = bkyears.yearid and bkaccounts.company = bkyears.company)
    where bkaccounts.ref = new.bkaccount
    into :i;

  for
    select separator, len
    from accstructure
    where bkaccount = new.bkaccount
    into :sep, :len
  do
  begin
    if (:sep<>',') then
      i = :i + 1;
    i = :i + :len;
  end

  if (new.separator <>',') then
    i = :i + 1;
  i = :i + new.len;

  if (i > 120) then
    exception TOO_LONG_ACCOUNT;

end^
SET TERM ; ^
