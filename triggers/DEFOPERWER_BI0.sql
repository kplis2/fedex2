--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERWER_BI0 FOR DEFOPERWER                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.TYP='ZAM' or new.przed = 0)then new.wynik = 0;
  if(new.regula < 12 and (new.pole is null or new.pole = '')) then
    exception DEFOPERWER_TYP_ZAM 'Wskazanie pola dla reguły jest wymagane.';
end^
SET TERM ; ^
