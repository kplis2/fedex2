--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EASRESULT_BI_BLANK FOR EASRESULT                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.eassetofcomp is null) then
    select eassetofcomp from easemplappr where ref = new.easemplappr
      into new.eassetofcomp;
end^
SET TERM ; ^
