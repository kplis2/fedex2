--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDECLENCLOSUREPOS_BI_EDECLPOS FOR EDECLENCLOSUREPOS              
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  insert into edeclpos (edeclaration, field, pvalue, edeclposdef)
    values (new.edeclaration, new.field, new.pvalue, new.edeclposdef);
end^
SET TERM ; ^
