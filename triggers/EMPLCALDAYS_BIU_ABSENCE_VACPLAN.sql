--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCALDAYS_BIU_ABSENCE_VACPLAN FOR EMPLCALDAYS                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
  declare variable absref integer;
  declare variable planref integer;
begin
--Aktualizacja info o nieobecnosciach i planach urlopowych na kalendarzu pracownika

  if (inserting or new.employee <> old.employee or new.cdate <> old.cdate) then
  begin
    --sprawdzenie czy pracownik jest w dany dzien nieobecny
    select first 1 ref from eabsences
      where new.employee = employee
        and new.cdate between fromdate and todate
        and correction in (0,2)
      into :absref;

    new.absence = absref;
  
    --sprawdzenie czy pracownik ma zaplanowany urlop lub zlozony wniosek
    select first 1 ref from evacplan
      where new.employee = employee
        and new.cdate between fromdate and todate
        and state <> 3 --BS49115
      order by vtype desc
      into :planref;
  
    new.vacplan = planref;
  end
end^
SET TERM ; ^
