--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKPERIODS_AU0 FOR BKPERIODS                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (old.status = 1 and new.status = 2 and new.ptype = 2) then
    execute procedure fk_make_yearclosing(new.company, new.yearid);
end^
SET TERM ; ^
