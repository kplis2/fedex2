--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRHDRS_AU0 FOR FRHDRS                         
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.status <> old.status and new.status = 1) then
    execute procedure frdcells_AUTOADD(new.symbol);
end^
SET TERM ; ^
