--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDTYPES_BI_REF FOR MWSORDTYPES                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSORDTYPES') returning_values new.ref;
end^
SET TERM ; ^
