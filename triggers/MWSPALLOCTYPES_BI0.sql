--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSPALLOCTYPES_BI0 FOR MWSPALLOCTYPES                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSPALLOCTYPES') returning_values new.ref;
end^
SET TERM ; ^
