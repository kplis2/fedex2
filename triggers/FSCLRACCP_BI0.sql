--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FSCLRACCP_BI0 FOR FSCLRACCP                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable dictgrp integer;
  declare variable company integer;
begin
  if (new.currdebit2c is null) then
    new.currdebit2c = 0;
  if (new.currcredit2c is null) then
    new.currcredit2c = 0;

  if (new.autopos is null) then
    new.autopos = 1;
  if (new.autochange is null) then
    new.autochange = 0;

  select kontofk, symbfak, slodef, slopoz,company
    from rozrachp where ref=new.rozrachp
    into new.account, new.settlement, new.dictdef, new.dictpos, :company;
  select coalesce(dataplat, dataotw), dictgrp
    from rozrach
    where symbfak=new.settlement
      and kontofk=new.account
      and slodef=new.dictdef
      and slopoz=new.dictpos
      and company = :company
    into new.payday, new.dictgrp;

  if (new.currdebit-new.currcredit>0) then
    new.side = 0;
  else
    new.side = 1;

  if (new.cleared is null) then
    new.cleared = 0;

  if (new.number is null) then
  begin
    select max(number) from fsclraccp
      where fsclracch=new.fsclracch and side=new.side
      into new.number;
    if (new.number is null) then
      new.number = 1;
    else
      new.number = new.number + 1;
  end
  select dictgrp from fsclracch 
    where ref=new.fsclracch
    into :dictgrp;
  if (dictgrp is null) then
    update fsclracch set dictgrp=:dictgrp where ref=new.fsclracch;
end^
SET TERM ; ^
