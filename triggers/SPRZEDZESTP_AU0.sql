--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDZESTP_AU0 FOR SPRZEDZESTP                    
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable typ char(1);
begin
  if((new.wartnet <> old.wartnet) or (new.wartbru <> old.wartbru) or (new.dowyplaty<> old.dowyplaty)) then
    execute procedure SPRZEDZEST_OBL_NAG(new.zestawienie);
  select TYP from SPRZEDZEST where REF=new.zestawienie into :typ;
  if(:typ = 'Z')then begin
    if(old.fak <> new.fak) then begin
      if(old.fak > 0) then update NAGFAK set SPRZEDROZL =0 where REF=old.fak;
      if(new.fak > 0) then update NAGFAK set SPRZEDROZL =1 where REF=new.fak;
    end
    if(old.zam <> new.zam) then begin
      if(old.zam > 0) then update NAGZAM set SPRZEDROZL =0 where REF=old.zam;
      if(new.zam > 0) then update NAGZAM set SPRZEDROZL =1 where REF=new.zam;
    end
  end
end^
SET TERM ; ^
