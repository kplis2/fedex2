--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ERECATTR_BI_REF FOR ERECATTR                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ERECATTR')
      returning_values new.REF;
end^
SET TERM ; ^
