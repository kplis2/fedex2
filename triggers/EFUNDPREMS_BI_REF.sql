--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EFUNDPREMS_BI_REF FOR EFUNDPREMS                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EFUNDPREMS')
      returning_values new.REF;
end^
SET TERM ; ^
