--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRCALCCOLSLOG_BI FOR PRCALCCOLSLOG                  
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.ref is null) then begin
    execute procedure GEN_REF('PRCALCCOLSLOG') returning_values new.ref;
  end
  if(new.validtill is null) then new.validtill = current_timestamp(0);
end^
SET TERM ; ^
