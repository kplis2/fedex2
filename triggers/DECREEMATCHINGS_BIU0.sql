--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREEMATCHINGS_BIU0 FOR DECREEMATCHINGS                
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.credit is null) then new.credit = 0;
  if (new.debit is null) then new.debit = 0;
  if (new.blockcalc is null) then new.blockcalc = 0;
end^
SET TERM ; ^
