--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZALACZNIKI_ZEW_BI_REF FOR ZALACZNIKI_ZEW                 
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure GEN_REF('ZALACZNIKI_ZEW')
      returning_values new.ref;
end^
SET TERM ; ^
