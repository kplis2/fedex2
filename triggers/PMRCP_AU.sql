--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMRCP_AU FOR PMRCP                          
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.pmposition is distinct from old.pmposition or new.amount<>old.amount) then begin
    update pmpositions set USEDQ = USEDQ - old.amount, USEDVAL = USEDQ * BUDPRICE where ref = old.pmposition;
    update pmpositions set USEDQ = USEDQ + new.amount, USEDVAL = USEDQ * BUDPRICE where ref = new.pmposition;
  end
end^
SET TERM ; ^
