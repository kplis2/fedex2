--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHORTAGES_AD0 FOR PRSHORTAGES                    
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable amount numeric(14,4);
declare variable kamountnorm numeric(14,4);
declare variable amountshortages numeric(14,4);
declare variable prschedguidespos integer;
begin
  if(old.amount <> 0 and old.prschedoper is not null) then
  begin
    select sum(s.headergroupamount)
      from prshortages s
        join prshortagestypes t on (t.symbol = s.prshortagestype)
      where s.prschedoper = old.prschedoper
        and coalesce(t.noamountinfluence,0) = 0
        and coalesce(s.headergroup,s.ref) = s.ref
      into :amountshortages;

    if (amountshortages is null) then amountshortages = 0;
    update prschedopers pso set pso.amountshortages = cast ((cast(:amountshortages as numeric(14,2)) + 0.4999) as integer )
      where pso.ref = old.prschedoper;
  end
  if(old.prschedguidespos is not null) then
  begin
    update prschedguides g set g.pggamountcalc = 1 where g.ref = old.prschedguide;
    for
      select p.ref, coalesce(p.kamountnorm,1)
        from prschedguidespos p
        where p.prschedguide = old.prschedguide
          and p.prschedopernumber <= (select o.number from prschedopers o where o.ref = old.prschedoper)
        into :prschedguidespos, :kamountnorm
    do begin
      amount = 0;
      select sum((s.amount / coalesce(iif(so.reportedfactor is null,o.reportedfactor,so.reportedfactor),1)) * :kamountnorm)
        from prshortages s
          join prshortagestypes t on (t.symbol = s.prshortagestype)
          join prschedopers o on (o.ref = s.prschedoper)
          left join prshopers so on (so.ref = o.shoper)
        where s.prschedguide = old.prschedguide
          and coalesce(t.noamountinfluence,0) = 0
          and coalesce(t.backasresourse,0) = 0
        into :amount;

      select coalesce(sum(s.amount),0) + :amount
        from prshortages s
          join prshortagestypes t on (t.symbol = s.prshortagestype)
        where s.prschedguide = old.prschedguide
          and s.prschedguidespos = :prschedguidespos
          and coalesce(t.noamountinfluence,0) = 0
          and coalesce(t.backasresourse,0) = 1
        into :amount;

      select :amount * (select first 1 oo.reportedfactor from prschedopers oo
          where oo.guide = old.prschedguide and oo.activ = 1 and oo.reported > 0
          order by oo.number desc)
        from rdb$database
        into :amount;
      update prschedguidespos p set p.amountshortage = :amount where p.ref = :prschedguidespos;

      kamountnorm = null;
    end
    update prschedguides g set g.pggamountcalc = 0 where g.ref = old.prschedguide;
  end
  if(old.prschedoper is not null and not exists(select ref from propersraps where prschedoper = old.prschedoper and ref <> old.ref))
    then execute procedure PRSCHEDOPERS_STATUSCHECK(null, null, old.prschedoper);
end^
SET TERM ; ^
