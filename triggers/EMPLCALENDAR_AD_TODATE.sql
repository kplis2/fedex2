--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCALENDAR_AD_TODATE FOR EMPLCALENDAR                   
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable todate timestamp;
begin
/*MWr: Ustalenie czasu trwania przy usunieciu rekordu. Mozna by wykorzystac
       EMPLCALENDAR_AIU_TODATE ale nie ma potrzeby 'odbudowywania' tabeli na nowo*/

  select first 1 fromdate - 1 from emplcalendar
    where employee = old.employee
      and fromdate > old.fromdate
    order by fromdate
    into :todate;

  if (todate is null) then
  begin
    select max(coalesce(enddate, todate, '3000-01-01'))
      from emplcontracts
      where employee = old.employee
      into :todate;

    if (todate is null) then
    begin
    --nie wykryto umow, data TODATE dla najmlodszego wpisu zostanie uzupelniona koncem roku
      select cast(extract(year from max(fromdate)) || '/12/31' as date)
        from emplcalendar
        where employee = old.employee
        into :todate;
    end else if (todate = '3000-01-01') then
      todate = null;
  end

  update emplcalendar
     set todate = :todate
     where ref in (select first 1 ref from emplcalendar
                    where employee = old.employee
                      and fromdate < old.fromdate
                    order by fromdate desc);
end^
SET TERM ; ^
