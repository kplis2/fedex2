--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER AMPERIODS_BU0 FOR AMPERIODS                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable amyear integer;
  declare variable ammonth integer;
begin
  if (old.status = 2 and new.status = 0) then
    exception AMPERIOD_CLOSED 'Nie można otwierać zaksiegowanych okresów';
  if (old.status = 1 and new.status = 0) then
  begin
    select first 1 max(amyear), max(ammonth)
      from amperiods
      where company = new.company and status >= 1
      group by amyear
      order by amyear desc
      into :amyear, :ammonth;

    if (amyear > new.amyear or (amyear = new.amyear and ammonth > new.ammonth)) then
      exception AMPERIOD_CLOSED 'Można otworzyć tylko ostatni zamkniety okres';
  end
end^
SET TERM ; ^
