--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKYEARS_BI0 FOR BKYEARS                        
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable firstperiod_prev smallint_id;
declare variable periodamount_prev smallint_id;
declare variable yearid_prev integer;
begin
  if (new.periodamount<6 or new.periodamount>18) then
    exception BKYEARS_LICZBA_OKRESOW;

  if (new.firstperiod = 0) then
  begin
    --pobrnie danych z poprzedniego roku danego zakladu
    select first 1 yearid, periodamount,firstperiod
    from bkyears
      where yearid < new.yearid and company = new.company
    order by yearid desc
    into :yearid_prev, :periodamount_prev,:firstperiod_prev;
    --pierwszy okres to roznica miedzy liczbą miesicy od poczatku poprzedniego roku a iloscia lat roznicy * 12 m-cy
    new.firstperiod = :firstperiod_prev + :periodamount_prev - 12 * (new.yearid - :yearid_prev);
  end
end^
SET TERM ; ^
