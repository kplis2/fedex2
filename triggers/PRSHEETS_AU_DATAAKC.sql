--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHEETS_AU_DATAAKC FOR PRSHEETS                       
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.status<>old.status and (new.status = 1 or (new.status=2))  and old.status =0 ) /*and old.status is null*/ then begin
  update prsheets set dataakc = current_date where ref = new.ref ;
    end/* Trigger text */
  if ((old.ktm <> new.ktm or new.wersja <> old.wersja)and not exists (select ref from wersje w where w.ktm = new.ktm and w.ref = new.wersjaref)) then
    exception PRSHEETS_DATA 'Niezgodna wersja';
end^
SET TERM ; ^
