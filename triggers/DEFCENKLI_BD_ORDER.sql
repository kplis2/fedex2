--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCENKLI_BD_ORDER FOR DEFCENKLI                      
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update defcenkli set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and grupakli = old.grupakli;
end^
SET TERM ; ^
