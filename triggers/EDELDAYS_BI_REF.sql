--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDELDAYS_BI_REF FOR EDELDAYS                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('EDELDAYS') returning_values new.ref;
end^
SET TERM ; ^
