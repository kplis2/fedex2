--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_AI0 FOR DOKUMPOZ                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable ack integer;
declare variable fifo char(1);
declare variable typ char(1);
declare variable mag char(3);
declare variable wyd smallint;
declare variable stan numeric(14,4);
declare variable nieobrot smallint;
declare variable skad smallint;
declare variable dokumentmain integer;
declare variable usluga smallint;
declare variable ilosckor numeric(14,4);
declare variable mwsdisposition smallint;
begin
  /*zatwierdzanie pozycji w locie */
  select D.AKCEPT, D.SKAD, D.DOKUMENTMAIN, d.mwsdisposition
    from DOKUMNAG D
    where D.ref = new.dokument
  into :ack, :skad, :dokumentmain, :mwsdisposition;
  if(new.roz_blok=0) then begin
    if(:ack = 1 or :ack=8) then begin
      exception DOKUMNAG_AKCEPT;
--      execute procedure DOKUMPOZ_CHANGE(new.ref,new.KTM,new.wersja,new.cena,new.dostawa,new.ilosc,0,0);
    end else begin
      select DEFMAGAZ.FIFO, DEFMAGAZ.TYP, DEFDOKUM.WYDANIA, DOKUMNAG.MAGAZYN
         from DOKUMNAG left join DEFMAGAZ on( DOKUMNAG.magazyn = DEFMAGAZ.symbol)
                       left join DEFDOKUM on ( DOKUMNAG.typ = DEFDOKUM.symbol) where (DOKUMNAG.REF = new.DOKUMENT)
                       into :fifo, :typ, :wyd, :mag;
      if(:fifo = 'R' and (:typ = 'C' or (:typ = 'P')) and :wyd = 1 and :ack <> 9) then begin
          select ILOSC from STANYCEN where MAGAZYN = :mag AND KTM = new.ktm and WERSJA = new.wersja AND CENA=new.cena into :stan;
          if(:stan is null) then stan = 0;
          if(new.ilosc < :stan) then stan = new.ilosc;
          if (coalesce(new.fake,0) < 1) then
            insert into DOKUMROZ(POZYCJA,NUMER,ILOSC,CENA,DOSTAWA,ACK)
              values(new.ref,1,:stan,new.cena,new.dostawa,0);
      end
      execute procedure DOKMAG_OBL_WAR(new.dokument,0);
    end
  end
  if(new.wartrealpoz > 0) then
     execute procedure POZZAM_OBL(new.wartrealpoz);
  if(new.wartrealzam > 0) then
     execute procedure NAGZAM_OBL(new.wartrealzam);
  if(new.frompozfak > 0 and ((new.wartosc <> 0) or (new.pwartosc <> 0)) ) then
    execute procedure POZFAK_OBL_KOSZT(new.frompozfak);
  -- obliczanie ILOSCL dla pozycji uslugowych
  if(new.kortopoz is not null) then begin
    select USLUGA from TOWARY where KTM=new.ktm into :usluga;
    if(:usluga=1) then begin
      select sum(ILOSC) from DOKUMPOZ where KORTOPOZ=new.kortopoz into :ilosckor;
      if(:ilosckor is null) then ilosckor = 0;
      update DOKUMPOZ set ILOSCL = ILOSC - :ilosckor where REF=new.kortopoz;
    end
  end
--naliczenie realizacji przewodnikow produkcyjnych
  if(skad >= 3 and new.ilosc > 0) then begin
    execute procedure PRSCHEDGUIDEDET_OBL('DOKUMNAG',skad,new.ref,0);
  end
  if(new.roz_blok = 1) then
    update dokumpoz set roz_blok = 0 where ref=new.ref;

  /* Uzgodnienie ilosci na zamówieniu */
  if(:mwsdisposition = 1) then
    execute procedure pozzam_obl(new.ref);
end^
SET TERM ; ^
