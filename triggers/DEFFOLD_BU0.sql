--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFFOLD_BU0 FOR DEFFOLD                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
    declare variable wiadomosci integer;
begin
  if (coalesce(new.nazwa,'') <> coalesce(old.nazwa,'') or (coalesce(new.nowewiadomosci,0) <> coalesce(old.nowewiadomosci,0))
        or (coalesce(new.nieprzeczytane,0) <> coalesce(old.nieprzeczytane,0))
        or (coalesce(new.synchrobyserver,0) <> coalesce(old.synchrobyserver,0))
        or (coalesce(new.TYP,0) <> coalesce(old.TYP,0))) then begin
  --BC poprawki z rawy, ilość maili w () to ma być ilość nieprzeczytanych maili (wytłuszczonych)
    wiadomosci = coalesce(new.nieprzeczytane,0);

    new.FORMATFIELD = '';
    --ustawienie pogrubienia nazwy gdy s a nieprzeczytane bądź nowe wiadomosci
    if (wiadomosci = 0) then begin
        new.NAZWAWYS = new.NAZWA;
    end else begin
        new.NAZWAWYS = new.NAZWA||' ('||(wiadomosci)||')';
        new.FORMATFIELD = '*=*=%B';
    end

    --Ustawienie wyszarzenia nazwy dla niesynchronizownych folderów
    if(new.synchrobyserver = 2) then begin
        if(new.FORMATFIELD = '') then begin
            new.FORMATFIELD = '*=*=%TclDkGray';
        end else begin
            new.FORMATFIELD = new.FORMATFIELD || '%TclDkGray';
        end
    end

    --Ustawaimy na nazwie folderu kursywe gdy jest on publiczny
    --tylko dla folderu reprezentującego skrzynke
    if(new.rodzic is null) then begin
        if(new.TYP = 'P') then begin
            if(new.FORMATFIELD = '') then begin
                new.FORMATFIELD = '*=*=%I';
            end else begin
                new.FORMATFIELD = new.FORMATFIELD || '%I';
            end
        end
    end

    --Jesli dodalismy jakies formatowanie to string zamykamy '*'
    if(new.FORMATFIELD <> '') then begin
        new.FORMATFIELD = new.FORMATFIELD || '*';
    end
  end
end^
SET TERM ; ^
