--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMAILWIAD_BU0 FOR EMAILWIAD                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable lokres varchar(6);
begin
  if((new.data <> old.data) or (new.data is not null and old.data is null))then begin
    lokres = new.okres;
    lokres = cast( extract( year from new.data) as char(4));
    if(extract(month from new.data) < 10) then
         lokres = :lokres ||'0' ||cast(extract(month from new.data) as char(1));
    else begin
        lokres = :lokres ||cast(extract(month from new.data) as char(2));
    end
    new.okres = :lokres;
  end
  --BS41102
  if(new.folder is null) then
    new.mailbox = null;
  else if (new.folder <> old.folder or (old.folder is null) ) then
    select deffold.mailbox from deffold where ref=new.folder into new.mailbox;
end^
SET TERM ; ^
