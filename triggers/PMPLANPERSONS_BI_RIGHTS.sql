--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPLANPERSONS_BI_RIGHTS FOR PMPLANPERSONS                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  select READRIGHTS,WRITERIGHTS,READRIGHTSGROUP,WRITERIGHTSGROUP
  from PMPLANS where REF=new.pmplan
  into new.readrights,new.writerights,new.readrightsgroup,new.writerightsgroup;
end^
SET TERM ; ^
