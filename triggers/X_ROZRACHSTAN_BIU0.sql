--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_ROZRACHSTAN_BIU0 FOR ROZRACHSTAN                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  new.x_ma = 0;
  new.x_winien = 0;
  if(new.winien > new.ma)then new.x_winien = new.winien - new.ma;
  else new.x_ma = new.ma - new.winien;
end^
SET TERM ; ^
