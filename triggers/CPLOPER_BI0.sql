--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLOPER_BI0 FOR CPLOPER                        
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable nilpkt integer;
declare variable oilpkt integer;
declare variable opktconst smallint;
declare variable oisnagroda smallint;
declare variable onagroda integer;
declare variable ozreal integer;
declare variable okontyp integer;
declare variable npktconst smallint;
declare variable pkosob integer;
declare variable cpodmiot integer;
begin
  if(new.ilpkt is null) then new.ilpkt = 0;
  if(new.zreal is null) then new.zreal = 0;
  if(new.operacja is not null) then begin
     select PM, ILPKT, ILPKTCONST, ISNAGRODA, NAGRODA, AUTOZREAL, KONTAKTTYP
      from CPLDEFOPER where ref=new.operacja
      into new.pm, :oilpkt, :opktconst, :oisnagroda, :onagroda, :ozreal, :okontyp;
      select PKOSOBA from CPLUCZEST where REF=new.cpluczest into :pkosob;
      if(:pkosob > 0) then new.pkosoba = :pkosob;
/*      if(:oilpkt <> 0 and :opktconst = 1) then new.ilpkt = :oilpkt;
      if(:oisnagroda = 1 and :onagroda is not null) then new.nagroda = :onagroda;
      else if(:oisnagroda = 0) then new.nagroda = null;
      if(:ozreal = 1) then new.zreal = 1;*/
/*      if(:okontyp > 0) then begin
        select PKOSOBA from CPLUCZEST where REF=new.cpluczest into :pkosob;
        if(:pkosob > 0) then new.pkosoba = :pkosob;
        execute procedure id_kontakty returning_values new.kontakt;
        select CPODMIOT from CPLUCZEST where REF=new.cpluczest into :cpodmiot;
        insert into KONTAKTY(REF, PKOSOBA, DATA, INOUT, RODZAJ, OPIS, OPERATOR, CPODMIOT)
        values (new.kontakt, new.pkosoba, new.data, new.inout, :okontyp, new.opis,new.operator, :cpodmiot);
      end*/
  end
/*  if(new.nagroda is not null) then begin
    select ilpkt,pktconst from CPLNAGRODY where REF=new.nagroda into :nilpkt, :npktconst;
    if(:npktconst =1) then
      new.ilpkt = :nilpkt*new.ilosc;
  end*/
end^
SET TERM ; ^
