--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OPERATOR_BU_REPLICAT FOR OPERATOR                       
  INACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.REF<>old.REF
      or new.imie<>old.imie or (new.imie is null and old.imie is not null) 
      or (new.imie is not null and old.imie is null)
      or new.nazwisko<>old.nazwisko or (new.nazwisko is null and old.nazwisko is not null) 
      or (new.nazwisko is not null and old.nazwisko is null)
      ) then
   waschange = 1;

  execute procedure rp_trigger_bu('OPERATOR',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
