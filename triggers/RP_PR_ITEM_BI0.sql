--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RP_PR_ITEM_BI0 FOR RP_PR_ITEM                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.REF_ID is NULL or (new.REF_ID = 0)) then
        new.REF_ID = gen_id(GEN_RP_PR_ITEM,1);
end^
SET TERM ; ^
