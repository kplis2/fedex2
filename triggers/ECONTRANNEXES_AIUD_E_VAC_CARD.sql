--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRANNEXES_AIUD_E_VAC_CARD FOR ECONTRANNEXES                  
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 10 
AS
declare variable ondate timestamp;
begin
--Aktualizacja kart urlopowych (BS53156)

  if(coalesce(new.workdim,0) <> coalesce(old.workdim,0) or deleting) then
  begin
    if (exists(select first 1 1 from emplcontracts  --PR60202
                 where ref = coalesce(new.emplcontract, old.emplcontract) and empltype = 1)
    ) then begin
      if (not updating) then ondate = coalesce(new.fromdate,old.fromdate);
      else if (new.fromdate <= old.fromdate) then ondate = new.fromdate;
      else ondate = old.fromdate;
  
      execute procedure e_vac_card (:ondate, coalesce(-new.employee, -old.employee));
    end
  end

  if(new.fromdate is distinct from old.fromdate or deleting) then
  begin
    if (exists(select first 1 1 from emplcontracts
                 where ref = coalesce(new.emplcontract, old.emplcontract) and empltype > 1)
    ) then begin
      if (not updating) then ondate = coalesce(new.fromdate,old.fromdate);
      else if (new.fromdate < old.fromdate) then
      begin
        execute procedure refresh_evacucp(new.fromdate, coalesce(new.emplcontract, old.emplcontract));
      end
      else if(new.fromdate > old.fromdate and old.fromdate is not null) then
        if(exists(select first 1 1
                    from eabsences
                    where fromdate < new.fromdate
                      and emplcontract = coalesce(new.emplcontract, old.emplcontract))) then
        begin
          exception universal'Istnieja zarejestrowane nieobecnosci. Zmiana niemozliwa';
        end
        else
          execute procedure refresh_evacucp(old.fromdate, new.emplcontract);
      else
      begin
        execute procedure refresh_evacucp(old.fromdate, old.emplcontract);
      end
    end
  end
end^
SET TERM ; ^
