--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYOPK_BU0 FOR STANYOPK                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if((new.wersjaref <> old.wersjaref or (new.ktm is null) or (new.wersja is null)) and new.wersjaref is not null and new.wersjaref <> 0) then begin
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  end else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)or (new.wersjaref is null)) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if(new.ilzreal is null) then new.ilzreal =  0;
  if(new.wartmagzreal is null) then new.wartmagzreal = 0;
  if(new.kaucjazreal is null) then new.kaucjazreal = 0;
  if(new.kaucja is null) then new.kaucja  = 0;
  if(new.cenaspr is null) then new.cenaspr = 0;
  if(new.data is null) then new.data = current_date;
  new.stan = new.ilosc - new.ilzreal;
  new.wartmagstan = new.wartmag - new.wartmagzreal;
  new.kaucjastan = new.kaucja - new.kaucjazreal;
  if(new.ilosc = new.ilzreal) then begin
    new.zreal = 1;
    if(old.zreal = 0) then
    new.datazamk = current_date;
  end else begin
    new.zreal = 0;
    new.datazamk = null;
  end
end^
SET TERM ; ^
