--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_AU_CORRECTION FOR EABSENCES                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  --DS: jezeli na wystawionej korekcie zmienimy liste plac, nalezy updatowac pole corepayroll
  --zarowno na korekcie jak i nieobecnosci korygowanej

  if (new.correction = 2 and new.epayroll is distinct from old.epayroll) then
  begin
    update eabsences
      set corepayroll = new.epayroll
      where ref = new.corrected         --nieobecnosc korygowana (czerwona)
         or corrected = new.corrected;  --korekta (nieob. zielona, biezaca)
  end
end^
SET TERM ; ^
