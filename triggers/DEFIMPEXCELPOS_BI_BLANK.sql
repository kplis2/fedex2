--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFIMPEXCELPOS_BI_BLANK FOR DEFIMPEXCELPOS                 
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if (new.startrow is null) then new.startrow = 0;
  if (new.transactionmode is null) then new.transactionmode = 0;
  if (new.breakonerror is null) then new.breakonerror = 0;
  if (new.activ is null) then new.activ = 1;
end^
SET TERM ; ^
