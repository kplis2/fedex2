--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAK_BIU_ALTPOZ FOR POZFAK                         
  ACTIVE BEFORE INSERT OR UPDATE POSITION 11 
as
declare variable altposmode smallint;
begin
  if (coalesce(new.havefake,0) = 1 and coalesce(new.fake,0) = 1) then
    exception poz_fake 'Nie można dodawać pozycji alternatywne do pozycji będącej alternatywną';

  if (updating and coalesce(old.ktm,'') <> '' and new.ktm <> old.ktm and coalesce(old.havefake,0) = 1) then
  begin
    select coalesce(t.altposmode,0) from towary t where t.ktm = old.ktm
    into :altposmode;

    if (:altposmode > 0) then exception poz_fake 'Nie można modyfikować KTM dla pozycji definiującej pozycje alternatywne';
  end
end^
SET TERM ; ^
