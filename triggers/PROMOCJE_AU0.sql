--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROMOCJE_AU0 FOR PROMOCJE                       
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable progref integer;
begin
  if(new.procentrab<>old.procentrab or (new.procentrab is not null and old.procentrab is null) or (new.procentrab is null and old.procentrab is not null)) then begin
    progref = NULL;
    select min(REF) from PROMOCR where PROMOCJA=new.ref and (ilosc=0 or (ilosc is null)) and (wartosc=0 or (wartosc is null)) into :progref;
    if(progref is NULL) then begin
      insert into PROMOCR(PROMOCJA,ILOSC,WARTOSC,UPUST,PROCENT,CONSTCENA,CONSTWAL)
      values (new.ref,0,0,0,new.PROCENTRAB,0,NULL);
    end else begin
      update PROMOCR set PROCENT=new.PROCENTRAB where PROMOCJA=new.ref and (ilosc=0 or (ilosc is null)) and (wartosc=0 or (wartosc is null));
    end
  end
end^
SET TERM ; ^
