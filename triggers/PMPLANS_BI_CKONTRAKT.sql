--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPLANS_BI_CKONTRAKT FOR PMPLANS                        
  ACTIVE BEFORE INSERT POSITION 5 
AS
begin
  if (new.contract is null) then begin
    execute procedure GEN_REF('CKONTRAKTY') returning_values new.contract;
    insert into CKONTRAKTY(REF,FAZA,CONTRTYPE,NAZWA,DATAZAL,OPERZAL,CPODMIOT,OPIS,WARTPOT,DATAPLREAL,PRAWADEF,PRAWA)
      values(new.contract,new.faza,new.contrtype,new.displayname,new.regdate,new.regoper,new.cpodmiot,new.descript,new.ovalue,new.plstartdate,1,new.writerights);
  end
end^
SET TERM ; ^
