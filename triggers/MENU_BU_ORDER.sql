--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MENU_BU_ORDER FOR MENU                           
  ACTIVE BEFORE UPDATE POSITION 1 
as
begin
 if((new.rodzic <> old.rodzic) or (new.rodzic is not null and old.rodzic is null)) then
   if(new.rodzic > 0) then
      select poziom + 1 from MENU where REF=new.rodzic into new.poziom;
   else new.poziom = 0;
  if (new.ord = 1 and old.ord <> 2) then
  begin
    if (new.numer > old.numer) then
      new.ord = 3;
    else if (new.numer < old.numer) then
      new.ord = 0;
  end
end^
SET TERM ; ^
