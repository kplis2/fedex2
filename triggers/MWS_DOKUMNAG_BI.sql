--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWS_DOKUMNAG_BI FOR DOKUMNAG                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  execute procedure MWS_SET_PARAMS_ON_DOKUMNAG(new.magazyn)
    returning_values (new.mwsmag);
end^
SET TERM ; ^
