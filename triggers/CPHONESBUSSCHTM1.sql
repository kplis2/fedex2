--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPHONESBUSSCHTM1 FOR CPHONES                        
  ACTIVE BEFORE UPDATE POSITION 100 
AS
begin
  if(new."AREACODE" <> old."AREACODE" or (new."AREACODE" is null and old."AREACODE" is not null) or (old."AREACODE" is null and new."AREACODE" is not null)
     or new."DESCRIPTION" <> old."DESCRIPTION" or (new."DESCRIPTION" is null and old."DESCRIPTION" is not null) or (old."DESCRIPTION" is null and new."DESCRIPTION" is not null)
     or new."DIRECTNUMBER" <> old."DIRECTNUMBER" or (new."DIRECTNUMBER" is null and old."DIRECTNUMBER" is not null) or (old."DIRECTNUMBER" is null and new."DIRECTNUMBER" is not null)
     or new."FULLNUMBER" <> old."FULLNUMBER" or (new."FULLNUMBER" is null and old."FULLNUMBER" is not null) or (old."FULLNUMBER" is null and new."FULLNUMBER" is not null)
     or new."INTERNATIONAL" <> old."INTERNATIONAL" or (new."INTERNATIONAL" is null and old."INTERNATIONAL" is not null) or (old."INTERNATIONAL" is null and new."INTERNATIONAL" is not null)
     or new."MIRRORNUMBER" <> old."MIRRORNUMBER" or (new."MIRRORNUMBER" is null and old."MIRRORNUMBER" is not null) or (old."MIRRORNUMBER" is null and new."MIRRORNUMBER" is not null)
     or new."REF" <> old."REF" or (new."REF" is null and old."REF" is not null) or (old."REF" is null and new."REF" is not null)
     or new."SFIELD" <> old."SFIELD" or (new."SFIELD" is null and old."SFIELD" is not null) or (old."SFIELD" is null and new."SFIELD" is not null)
     or new."SKEY" <> old."SKEY" or (new."SKEY" is null and old."SKEY" is not null) or (old."SKEY" is null and new."SKEY" is not null)
     or new."SPOSITION" <> old."SPOSITION" or (new."SPOSITION" is null and old."SPOSITION" is not null) or (old."SPOSITION" is null and new."SPOSITION" is not null)
     or new."STABLE" <> old."STABLE" or (new."STABLE" is null and old."STABLE" is not null) or (old."STABLE" is null and new."STABLE" is not null)
  ) then
    new."SYSCHANGETIME" = current_timestamp(0);
end^
SET TERM ; ^
