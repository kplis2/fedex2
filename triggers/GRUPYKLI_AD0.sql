--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER GRUPYKLI_AD0 FOR GRUPYKLI                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable il integer;
declare variable aktuoddzial varchar(100);
begin
  execute procedure DESYNC_GRUPYKLI(old.ref);
    if(old.cennik is not null) then begin
      select count(*) from GRUPYKLI where cennik = old.cennik into :il;
      if(:il is null) then il = 0;
      if(:il = 0) THEN begin
        execute procedure GETCONFIG('AKTUODDZIAL') returning_values :aktuoddzial;
        UPDATE defcennik set akt = 0 where akt <> 0 and ref = old.cennik and ((ODDZIAL=:aktuoddzial) or (ODDZIAL='') or (ODDZIAL is NULL));
      end
    end
end^
SET TERM ; ^
