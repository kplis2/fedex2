--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFFORMP_BD_ORDER FOR DEFFORMP                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update defformp set ord = 0, numer = numer - 1
    where (forma <> old.forma or ident <> old.ident) and numer > old.numer and forma = old.forma;
end^
SET TERM ; ^
