--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVCONTRACTS_BI_DANE FOR SRVCONTRACTS                   
  ACTIVE BEFORE INSERT POSITION 9 
AS
declare variable ktel varchar(255);
declare variable kkom varchar(255);
begin
  if (coalesce(new.dictdef,0) <> 0 and coalesce(new.dictpos, 0) = 0) then
    exception SRVCONTRACTS_BRAK_DICTPOS;
  if (new.dictdef = 1) then begin
    select fskrot, prostynip, ulica, nrdomu, nrlokalu, miasto, kodp, telefon, komorka
      from klienci
      where ref = new.dictpos
    into new.name, new.taxid, new.street, new.houseno, new.localno, new.city, new.postcode, :ktel, :kkom;

    if (coalesce(:ktel,'') <> '') then new.phone = :ktel;
    if (coalesce(:kkom, '') <> '') then new.phone = new.phone || '| ' || :kkom;
  end
  else if (new.dictdef = 6) then begin
    select id, prostynip, ulica, nrdomu, nrlokalu, miasto, kodp, telefon
      from dostawcy
      where ref = new.dictpos
    into  new.name, new.taxid,  new.street, new.houseno, new.localno, new.city, new.postcode, new.phone;
  end
end^
SET TERM ; ^
