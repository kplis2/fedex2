--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PERSONS_BIU_BLANK FOR PERSONS                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
begin
  if (inserting or old.middlename is distinct from new.middlename
    or old.fname is distinct from new.fname or old.sname is distinct from new.sname
  ) then begin
    new.sname = coalesce(trim(new.sname),'');
    new.fname = coalesce(trim(new.fname),'');
    new.middlename = coalesce(trim(new.middlename),'');

    if (new.sname = '' or new.fname = '') then
      exception brak_pdstawowych_danych;

    new.person = trim(new.sname || ' ' || new.fname || ' ' || new.middlename);
  end

  new.passportno = coalesce(trim(new.passportno),'');
  new.nip = trim(new.nip);
  new.pesel = trim(new.pesel);
  if (new.pesel = '') then new.pesel = null;

  if (new.foreigner = 0 and old.foreigner = 1) then
  begin  --PR61315
   new.citizenship = 'Polskie';
   new.steadystaycard = 0;
  end
end^
SET TERM ; ^
