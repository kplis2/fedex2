--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTIFSTATE_BIU0 FOR NOTIFSTATE                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (coalesce(new.val,'') != coalesce(old.val, '')) then
  begin
    if (exists (select first 1 1 from notifsteps n where n.ref = new.notifstep and n.soundnotif = 1)) then
      new.psound = 1; -- ustaw dzwiek do odegrania
  end
end^
SET TERM ; ^
