--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDICTWORKPOSTS_AU FOR EDICTWORKPOSTS                 
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if (old.worktype <> new.worktype) then
    update employees e set e.worktype = new.worktype where e.workpost = new.ref;
end^
SET TERM ; ^
