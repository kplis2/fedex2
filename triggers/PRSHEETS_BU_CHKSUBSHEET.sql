--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHEETS_BU_CHKSUBSHEET FOR PRSHEETS                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (old.STATUS=0 and new.STATUS in(1,2)) then begin
    if (exists (select first(1) 1 from PRSHMAT SM
        join PRSHEETS S on (S.REF = SM.SUBSHEET) where SM.SHEET = new.REF and (s.STATUS<1 or s.STATUS>=3))
    ) then begin
      exception PRSHEETS_EXPT 'Karta zawiera jako surowiec inne niezatwierdzone karty technologiczne';
    end
  end
  if (old.STATUS in(1,2) and new.STATUS=0) then begin
    if (exists (select first(1) 1 from PRSHMAT SM join PRSHEETS S on (S.REF = SM.SHEET)
        where SM.SUBSHEET = new.ref and S.STATUS in (1,2))
    ) then begin
      exception PRSHEETS_EXPT 'Karta jest surowcem w innej karcie technologicznej';
    end
  end
end^
SET TERM ; ^
