--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CENNIKIZEWN_BI_REF FOR CENNIKIZEWN                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CENNIKIZEWN')
      returning_values new.REF;
end^
SET TERM ; ^
