--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKI_BU_REPLICAT FOR BANKI                          
  ACTIVE BEFORE UPDATE POSITION 1 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if((new.symbol <> old.symbol)
   or(new.nazwa <> old.nazwa ) or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null)
   or(new.ulica <> old.ulica ) or (new.ulica is null and old.ulica is not null) or (new.ulica is not null and old.ulica is null)
   or(new.miasto <> old.miasto ) or (new.miasto is null and old.miasto is not null) or (new.miasto is not null and old.miasto is null)
   or(new.kodp <> old.kodp ) or (new.kodp is null and old.kodp is not null) or (new.kodp is not null and old.kodp is null)
   or(new.konto <> old.konto ) or (new.konto is null and old.konto is not null) or (new.konto is not null and old.konto is null)
  )then
   waschange = 1;

  execute procedure rp_trigger_bu('BANKI',old.symbol, null, null, null, null, old.token, old.state,
        new.symbol, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
