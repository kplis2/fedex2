--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMSER_AD0 FOR DOKUMSER                       
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable dokumserack smallint;
declare variable ref integer;
declare variable wydania smallint;
declare variable ktm varchar(40);
declare variable akcept integer;
declare variable gen varchar(40);
declare variable typ varchar(1);
begin
  dokumserack = 0;
  wydania = 0;
  ktm='';
  if(old.typ = 'M') then begin
    select KTM from DOKUMPOZ where REF=old.refpoz into :ktm;
    if(:ktm is null or :ktm='') then exception TOWARY_BEZKTM 'Niezdefiniowany towar.';
    select DEFMAGAZ.NOTSERIAL, DEFDOKUM.wydania, DOKUMNAG.akcept
      from DOKUMPOZ
      left join DOKUMNAG on (DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
      left join DEFMAGAZ on (DEFMAGAZ.SYMBOL = DOKUMNAG.MAGAZYN)
      left join DEFDOKUM on (DEFDOKUM.SYMBOL = DOKUMNAG.TYP)
      where DOKUMPOZ.REF = old.refpoz
      into :dokumserack, :wydania, :akcept;
-- MS: nie mozna redagowac nr seryjnych na zaakceptowanych dok w trybie serializacji na magazynie
    if(:dokumserack = 0 and :akcept = 1) then
      exception dokumnag_akcept;
    if(dokumserack = 1 and :wydania = 1) then begin
      ref = 0;
      select max(dokumser.ref)
        from dokumser
        left join dokumpoz on (dokumser.refpoz = dokumpoz.ref)
        where dokumser.odserial = old.odserial
          and dokumser.ref <> old.ref
          and dokumser.iloscroz>0
          and dokumpoz.ktm = :ktm
        into :ref;
      if(:ref>0) then begin
        update dokumser set dokumser.iloscroz = 0 where dokumser.ref = :ref;
      end else begin
--        exception DOKUMSER_WRONGNR 'Nie mona zwolni starego numeru.';
      end
    end
  end

  if (coalesce(old.numbergen,0)<> '') then
    execute procedure free_number(old.numbergen, old.genrej, old.gendok, current_date, old.gennumber, 0)
      returning_values :ref;
  if (old.orgdokumser is not null) then
  begin
    select s.typ
      from dokumser s
      where s.ref = old.orgdokumser
      into :typ;
    if (:typ = 'R') then
    begin
      --relizacja dokumsera z raportu produkcyjnego na dokument magazynowy
      update dokumser s set s.iloscroz = s.iloscroz - old.ilosc
        where s.ref = old.orgdokumser;
    end
  end

  -- aktualizacja ilości skopiowanej w źródlowym rekordzie dokumser
  if(old.orgmmdokumser is not null) then
    update dokumser set ilosccopy = ilosccopy - old.ilosc where ref = old.orgmmdokumser;

end^
SET TERM ; ^
