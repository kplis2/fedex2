--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_AU_ALTPOZ FOR POZZAM                         
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable docsetsautoadd smallint;
declare variable kplnagref integer;
declare variable dokpoz integer;
declare variable wersjaref integer;
begin
  execute procedure getconfig('DOCSETSAUTOADD')
    returning_values :docsetsautoadd;
  if (:docsetsautoadd is null) then docsetsautoadd = 0;

  if (coalesce(new.fake,0) > 0 and coalesce(old.fake,0) = 0) then
  begin
    update pozzam
      set havefake = 1
      where ref = new.alttopoz;
  end
  else if (coalesce(new.fake,0) = 0 and coalesce(old.fake,0) > 0) then
  begin
    if (not exists(select first 1 1 from pozzam where alttopoz = old.alttopoz)) then
      update pozzam
        set havefake = 0
        where ref = old.alttopoz;
  end

  if (new.havefake = 1 and :docsetsautoadd > 0 and
      (new.ilosc <> old.ilosc or new.ilreal <> old.ilreal or new.magazyn is distinct from old.magazyn or new.mag2 is distinct from old.mag2) and
      exists(select first 1 1 from towary where ktm = new.ktm and usluga <> 1 and coalesce(altposmode,0) > 0)) then
  begin
    select first 1 ref from kplnag
      where ktm = new.ktm and wersjaref = new.wersjaref and akt = 1 and glowna = 1
    into :kplnagref;

    for
      select p.ref, p.wersjaref from pozzam p
        where p.zamowienie = new.zamowienie
          and p.alttopoz = new.ref
      into :dokpoz, :wersjaref
    do begin
      update pozzam p
          set p.ilosc = new.ilosc * (select k.ilosc from kplpoz k where k.nagkpl = :kplnagref and k.wersjaref = :wersjaref),
              p.ilreal = new.ilreal * (select k.ilosc from kplpoz k where k.nagkpl = :kplnagref and k.wersjaref = :wersjaref),
              p.magazyn = new.magazyn,
              p.mag2 = new.mag2
        where p.ref = :dokpoz;
    end
  end
end^
SET TERM ; ^
