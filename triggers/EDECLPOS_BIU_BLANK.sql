--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDECLPOS_BIU_BLANK FOR EDECLPOS                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
  declare variable datatype smallint;
  declare variable issqlgenerated type of column edeclarations.sqlgenerated;
begin

  select coalesce(e.sqlgenerated,0) from edeclarations e
    where e.ref = new.edeclaration
    into :issqlgenerated;

  if (:issqlgenerated = 0) then
  begin
    select dpd.datatype, dpd.field
      from edeclarations d
        join edeclposdefs dpd on dpd.edecldef = d.edecldef
      where d.ref = new.edeclaration
        and dpd.ref = new.edeclposdef
      into :datatype, new.field;
    --ftCurrency
    if (datatype = 1) then
      new.pvalue = replace(new.pvalue,',','.');
  end else if (coalesce(new.field,0) = 0) then  --BS100979, BS108145
  begin
    if (not exists(select first 1 1 from edeclpos e where e.edeclaration = new.edeclaration and e.field = 1)) then
    begin
      new.field = 1;
    end else
    begin
      select first 1 e.field + 1 from edeclpos e
        left join edeclpos e1 on (e1.field = e.field + 1
          and e.edeclaration = e1.edeclaration)
        where e.edeclaration = new.edeclaration and e1.field is null
        into new.field;
    end
  end
end^
SET TERM ; ^
