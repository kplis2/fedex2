--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRMETHODPARVALS_AD FOR PRMETHODPARVALS                
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable pvalue numeric(14,2);
begin
  if (old.fromdate < current_date) then
  begin
    select first 1 p.pvalue
      from prmethodparvals p
      where p.method = old.method
        and p.prmethodpar = old.prmethodpar
        and p.fromdate <= current_date
      order by p.fromdate desc
      into :pvalue;

    update prcalcpars p set p.pvalue = :pvalue
      where p.symbol = old.prmethodpar and p.partype = 'G'
        and exists (select first 1 1 from  prshcalcs c
          where c.ref = p.calc and c.method = old.method);
  end
end^
SET TERM ; ^
