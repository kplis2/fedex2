--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPEDYTWYS_AU_REPLICAT FOR SPEDYTWYS                      
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if((new.sposdost <> old.sposdost) or (new.spedytor <> old.spedytor)) then
    update spedytorzy set STATE = -1 where SYMBOL = new.spedytor;
  if(new.spedytor <> old.spedytor) then
    update spedytorzy set STATE = -1 where SYMBOL = old.spedytor;

end^
SET TERM ; ^
