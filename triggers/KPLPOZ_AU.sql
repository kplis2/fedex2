--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KPLPOZ_AU FOR KPLPOZ                         
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable towtypekpl varchar(100);
declare variable kplwersjaref integer;
begin
  if((new.wartmag <>old.wartmag) or (new.wartspr <> old.wartspr))then
    execute procedure NAGKPL_OBL(new.nagkpl);
  execute procedure get_config('TOWTYPEKOMPLET',2) returning_values :towtypekpl;
  if (:towtypekpl is null or :towtypekpl = '') then
    towtypekpl = '2';
  -- jezeli modyfikujemy glowna recepture to aktualizacja na stanach ilosciowych ilosci potencjalnych mozliwych do wykonania
  if (new.ilosc <> old.ilosc or (new.ilosc is not null and old.ilosc is null) or (new.ilosc is null and old.ilosc is not null)
      or (new.wersjaref <> old.wersjaref) or (new.wersjaref is null and old.wersjaref is not null) or (new.wersjaref is not null and old.wersjaref is null)
     ) then
  begin
    select kplnag.wersjaref
      from kplnag
      where kplnag.ref = new.nagkpl and kplnag.glowna = 1
      into :kplwersjaref;
    if (:kplwersjaref is not null) then
      execute procedure CALCULATE_KOMPLETY_POT(:kplwersjaref,null,cast(:towtypekpl as integer));
  end
end^
SET TERM ; ^
