--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_AU_DATAOTRZKOR FOR NAGFAK                         
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable bkdoctype integer;
declare variable payday timestamp;
declare variable docdate timestamp;
declare variable company integer;
declare variable vperiodprompt integer;
declare variable VPERIOD varchar(6);
declare variable vatdate timestamp;
declare variable vatokres varchar (10);

begin
  if ((old.dataotrzkor <> new.dataotrzkor
    or (new.dataotrzkor is not null and old.dataotrzkor is null)
    or (new.dataodbioru is not null and old.dataodbioru is null)
    or (new.dataodbioru <> old.dataodbioru)  )
    and new.akceptacja > 0 and new.blokada >=3) then
  begin
    select vatperiod, doctype, docdate, payday, company from bkdocs where bkdocs.otable = 'NAGFAK' and
      bkdocs.oref = new.ref
    into :vatokres, :bkdoctype, :docdate, :payday, :company;
    select vperiodprompt from bkdoctypes where ref=:bkdoctype
    into :vperiodprompt;
    
    if (coalesce(vperiodprompt,0) > 0) then
    begin
      select vatperiod, vatdate from BKDOC_VATDATE_OBL('NAGFAK',new.ref,:docdate,:payday,:company,:vperiodprompt)
      into :vperiod, :vatdate;
  
      if (coalesce(:vatokres,'') <> ''  and :vatokres <> :vperiod) then exception VATPERIODWYPELNIONY;
      else
      update bkdocs set vatperiod = :vperiod, vatdate = :vatdate  where bkdocs.otable = 'NAGFAK'
        and bkdocs.oref = new.ref;
    end
  end
end^
SET TERM ; ^
