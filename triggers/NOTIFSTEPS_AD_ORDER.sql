--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTIFSTEPS_AD_ORDER FOR NOTIFSTEPS                     
  ACTIVE AFTER DELETE POSITION 20 
as
begin
  update notifsteps n set n.number = n.number - 1
    where n.number > old.number and n.notification = old.notification;
end^
SET TERM ; ^
