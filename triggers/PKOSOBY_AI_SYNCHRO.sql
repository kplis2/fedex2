--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PKOSOBY_AI_SYNCHRO FOR PKOSOBY                        
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable synchro smallint;
declare variable refout integer;
declare variable kli integer;
declare variable slodefkli integer;
declare variable slotyp varchar(40);
begin
  execute procedure getconfig('SYNCHROUZYKLI') returning_values :synchro;
  if (synchro = 1) then
  begin
   if (new.synchro is null) then
   begin
     select cp.slodef, cp.slopoz from cpodmioty cp where cp.ref = new.cpodmiot
       into :slodefkli,:kli;
     select typ from slodef where ref=:slodefkli into :slotyp;
     if(:slotyp='KLIENCI') then begin
       insert into UZYKLI(PKOSOBA, KLIENT, NAZWA, IMIE, NAZWISKO, DMIASTO, DKODP,
         DULICA, DNRDOMU, DNRLOKALU, DPOCZTA, TELKOM,
         TELEFON, FAX, EMAIL, SYNCHRO, PESEL, AKTYWNY, WINDYKACJA)
       values (new.REF,:kli, new.NAZWA, new.IMIE, new.NAZWISKO, new.MIASTO, new.KODP,
         new.ULICA, new.NRDOMU, new.NRLOKALU, new.POCZTA, new.KOMORKA,
         new.TELEFON, new.FAX, new.EMAIL, 1, new.pesel, new.aktywny, new.windykacja);
     end
   end
   if(new.synchro=1) then update pkosoby set synchro=null where REF=new.ref;
  end
end^
SET TERM ; ^
