--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOCTOBKDOCS_BU FOR DOCTOBKDOCS                    
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable nieksiegowy integer;
begin
  if (new.param2 is null) then new.param2 = '';
  if (new.bkreg='') then new.bkreg = null;
  if (new.vatreg='') then new.vatreg = null;
  if (new.taxgr='') then new.taxgr = null;
  if (new.taxgr is null and new.typ = 0) then
    exception BKDOCS_ZAKUP_MA_GRUPE;
  if(new.param2<>old.param2 and new.typ=1) then begin
    select nieksiegowy from defmagaz where symbol = new.param2 into :nieksiegowy;
    if(:nieksiegowy=1) then exception doctobkdocs_nieksiegowy;
  end
end^
SET TERM ; ^
