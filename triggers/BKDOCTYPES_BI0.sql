--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCTYPES_BI0 FOR BKDOCTYPES                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if ((new.multicurr is NULL) or (new.kind=0)) then new.multicurr = 0;
  if(new.kind=3) then new.multicurr = 1;
  new.isvatcorrection = coalesce(new.isvatcorrection,0);
end^
SET TERM ; ^
