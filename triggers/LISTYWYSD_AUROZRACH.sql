--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSD_AUROZRACH FOR LISTYWYSD                      
  ACTIVE AFTER UPDATE POSITION 1 
as
declare variable slodefr integer;
declare variable slopozr integer;
declare variable kontofkr KONTO_ID;
declare variable symbfakr varchar(40);
declare variable winien numeric(14,2);
declare variable ma numeric(14,2);
declare variable winiendek numeric(14,2);
declare variable madek numeric(14,2);
declare variable winienroz numeric(14,2);
declare variable maroz numeric(14,2);
declare variable waluta varchar(3);
declare variable slodef integer;
declare variable slopoz integer;
declare variable kontofkkl KONTO_ID;
declare variable SYMBROZRACH varchar(30);
declare variable sposzapplat integer;
declare variable klient integer;
declare variable kontofk KONTO_ID;
declare variable spedkonto KONTO_ID;
declare variable pobranie numeric(14,2);
declare variable fsclracchref integer;
declare variable dataplat timestamp;
declare variable tmp integer;
declare variable konfigw varchar(20);
declare variable fsakcept smallint;
declare variable company companies_id;
declare variable symbfak symbol_id;
begin
  if(new.akcept = 2 and old.akcept <> 2) then begin
    select sposdost.platnik, sposdost.kontofk from SPOSDOST where REF=new.sposdost into :klient, :spedkonto;
    select ref, prefixfk from SLODEF where TYP='KLIENCI' into :slodef, :kontofk;
    slopoz = :klient;
    klient = :klient;
    fsclracchref = null;
    winienroz = 0;
    maroz = 0;
    winiendek = 0;
    madek = 0;

    if (trim(replace(spedkonto,' ','')) = '') then spedkonto = null; --wykluczenie pustych

    if(:sposzapplat is null or :sposzapplat = 0) then sposzapplat = :slopoz;
    select KONTOFK from KLIENCI where REF=:klient into :kontofkkl;

    if (trim(replace(kontofkkl,' ','')) = '') then kontofkkl = null; --wykluczenie pustych

    symbrozrach = substring (new.symbolsped from 1 for 20);
    if(:symbrozrach is null or (:symbrozrach = '')) then symbrozrach = substring(new.symbol from 1 for 20);
    if (exists(select ref from rozrachroz
          where ROZRACHROZ.doktyp = 'D' and ROZRACHROZ.dokref = new.ref)) then
    begin
      tmp = 0;
      for
        select rr.SLODEF, rr.SLOPOZ, rr.KONTOFK, rr.SYMBFAK, rr.WALUTA, rr.WINIEN, rr.MA, rr.PAYDAY, r.company
        from rozrachroz rr
          left join rozrach r on r.symbfak = rr.symbfak
        where rr.doktyp = 'D' and rr.dokref = new.ref
        into :slodefr, :slopozr, :kontofkr, :symbfakr, :waluta, :winien, :ma, :dataplat, :company
      do begin
        tmp = tmp + 1;
        if (:winien is not null) then
          winiendek = winiendek + winien;
        if (:ma is not null) then
          madek = madek + ma;
        if(:waluta is null or (:waluta = '')) then
          execute procedure getconfig('WALPLN') returning_values :waluta;
        fsclracchref = null;
        select fsclracch.ref
          from fsclracch
          where fsclracch.stable = 'LISTYWYSD' and fsclracch.sref = new.ref
          into :fsclracchref;
        if (fsclracchref is null) then
        begin
          execute procedure GEN_REF('FSCLRACCH') returning_values :fsclracchref;
          execute procedure get_config('SIDFK_ROZLKOMPEN',2) returning_values :konfigw;
          fsakcept = cast(konfigw as smallint);
          fsakcept = fsakcept + 1; -- zgodnie z zalozeniami konfig zwiekszany o 1
          insert into fsclracch(REF, DICTDEF, DICTPOS, STATUS, STABLE, SREF, currency, FSTYPEAKC, company)
            values(:fsclracchref, :slodefr, :slopozr, 0, 'LISTYWYSD', new.ref, :waluta, :fsakcept,  :company);
        end

        insert into fsclraccp(fsclracch, dictdef, dictpos, description, debit, credit, currdebit,
            currcredit, rate, side, cleared, autopos, autochange, settlement, account)
          values(:fsclracchref, :slodefr, :slopozr, 'Roz. dekl. na sped', :ma, :winien,
              :ma, :winien, 1, 1, 0, 1, 0, :symbfakr, :kontofkr);
      end
    end
    /*przesuniecie z faktur*/
    winien = 0;
    ma = 0;
    select sum(WINIEN), sum(MA) from rozrachroz
    where ROZRACHROZ.doktyp = 'D' and ROZRACHROZ.dokref = new.ref
    into :winien, :ma;
    if(winien is null) then winien = 0;
    if(ma is null) then ma = 0;
    pobranie = new.pobranie;
    if(pobranie is null) then pobranie = 0;
    pobranie = winien - ma + pobranie;
    winien = 0;
    if (exists(select rozrach.slodef from rozrach join NAGFAK on (NAGFAK.ref=rozrach.faktura)
          left join PLATNOSCI on (PLATNOSCI.ref = NAGFAK.sposzap)
          where nagfak.listywysd = new.ref and platnosci.naspedytora = 1)
        and (spedkonto is not null or kontofkkl is not null) --musi byc jakies konto
        ) then
    begin
      for
        select ROZRACH.slodef, ROZRACH.slopoz, ROZRACH.symbfak, ROZRACH.kontofk, ROZRACH.waluta,
            NAGFAK.dozaplaty, rozrach.dataplat, NAGFAK.company, NAGFAK.symbol
          from ROZRACH join NAGFAK on (NAGFAK.ref = rozrach.faktura)
          left join PLATNOSCI on (PLATNOSCI.ref = NAGFAK.sposzap)
          where nagfak.listywysd = new.ref and platnosci.naspedytora = 1
          into :slodefr, :slopozr, :symbfakr, :kontofkr, :waluta, :ma, :dataplat, :company,
            :symbfak
      do begin
        if(ma > pobranie) then ma = pobranie;
        if(ma > 0) then begin
          if (:winien is not null) then
            winienroz = winienroz + winien;
          if (ma is not null) then
            maroz = maroz + ma;
          if (fsclracchref is null) then
          begin
            execute procedure GEN_REF('FSCLRACCH') returning_values :fsclracchref;
            execute procedure get_config('SIDFK_ROZLKOMPEN',2) returning_values :konfigw;
            fsakcept = cast(konfigw as smallint);
            fsakcept = fsakcept + 1; -- zgodnie z zalozeniami konfig zwiekszany o 1
            insert into fsclracch(REF, DICTDEF, DICTPOS, STATUS, STABLE, SREF, currency, FSTYPEAKC, company)
                           values(:fsclracchref, :slodefr, :slopozr, 0, 'LISTYWYSD', new.ref, :waluta, :fsakcept, :company);
          end
          insert into fsclraccp(fsclracch, dictdef, dictpos, description, debit, credit, currdebit,
              currcredit, rate, side, cleared, autopos, autochange, settlement, account)
            values(:fsclracchref, :slodefr, :slopozr, 'Rozrachunek na spedytora', :ma, :winien,
                :ma,:winien, 1, 1, 0, 1, 0, :symbfakr, :kontofkr);
        end
        pobranie = pobranie - ma;
      end
    end
    winien = null;
    ma = null;
    select  sum(winien), sum(ma) from listywysd_rozrach_all ('D',new.ref)
      into :winien, :ma;
    if(winien is null) then winien = 0;
    if(ma is null) then ma = 0;
    ma = ma - winien;
    winien = 0;
    if(spedkonto is not null and fsclracchref is not null) then
      insert into fsclraccp(fsclracch, dictdef, dictpos, description, debit, credit, currdebit,
          currcredit, rate, side, cleared, autopos, autochange, settlement, account, payday)
        values(:fsclracchref, :slodef, :slopoz, 'Rozrachunek na spedytora', :winien, :ma,
           :winien, :ma, 1, 0, 0, 1, 0,
           iif(coalesce(new.symbolsped,'') = '', :symbfak, substring (new.symbolsped from 1 for 20)), --wrzucenie symbolu faktury na kompensate jesli brak nr spedycyjnego
           :spedkonto, :dataplat);
    else if (fsclracchref is not null) then
      insert into fsclraccp(fsclracch, dictdef, dictpos, description, debit, credit, currdebit,
          currcredit, rate, side, cleared, autopos, autochange, settlement, account, payday)
        values(:fsclracchref, :slodefr, :slopozr, 'Rozrachunek na spedytora', :winien, :ma,
            :winien, :ma, 1, 0, 0, 1, 0, substring (new.symbolsped from 1 for 20), :kontofkkl, :dataplat);
   --update fsclracch set status = 1 where status = 0 and sref = new.ref and stable = 'LISTYWYSD'; --[PM] XXX !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  end else if (new.akcept <> 2 and old.akcept = 2) then begin
    update fsclracch set status = 0 where STABLE = 'LISTYWYSD' and SREF = new.ref;
    delete  from fsclracch where STABLE = 'LISTYWYSD' and SREF = new.ref;
  end
end^
SET TERM ; ^
