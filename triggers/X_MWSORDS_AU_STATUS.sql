--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWSORDS_AU_STATUS FOR MWSORDS                        
  ACTIVE AFTER UPDATE POSITION 0 
as
    declare variable grupasped dokumnag_id;
    declare variable x_imp_ref integer_id;
begin
    if (new.status = 2 and old.status = 1) then
    begin
        select d.grupasped from dokumnag d where d.ref = new.docid
            into :grupasped;
        for
            select d.x_imp_ref from dokumnag d
                where d.grupasped = :grupasped
            into: x_imp_ref do
        begin
            update x_imp_dokumnag xd set xd.status = 12, xd.mwsstatus = 2
                where xd.nagid = :x_imp_ref;
        end
    end
end^
SET TERM ; ^
