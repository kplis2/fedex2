--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPOSDOST_EDERULESAFTER_BI_REF FOR SPOSDOST_EDERULESAFTER         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SPOSDOST_EDERULESAFTER')
      returning_values new.REF;
end^
SET TERM ; ^
