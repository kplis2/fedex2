--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPEDYTORZY_BI1 FOR SPEDYTORZY                     
  ACTIVE BEFORE INSERT POSITION 1 
as
begin
  if(coalesce(new.oddzial,'')='') then
    execute procedure GETCONFIG('AKTUODDZIAL') returning_values new.oddzial;
end^
SET TERM ; ^
