--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPOSITIONS_BI_RIGHTS FOR PMPOSITIONS                    
  ACTIVE BEFORE INSERT POSITION 1 
AS
begin
  select READRIGHTS,WRITERIGHTS,READRIGHTSGROUP,WRITERIGHTSGROUP
  from PMPLANS where REF=new.pmplan
  into new.readrights,new.writerights,new.readrightsgroup,new.writerightsgroup;
end^
SET TERM ; ^
