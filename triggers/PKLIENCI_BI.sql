--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PKLIENCI_BI FOR PKLIENCI                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable contactcontrol varchar(100);
BEGIN
  if(new.crm is null) then new.crm = 0;

      -- sprawdzenie poprawnosci telefonu
  execute procedure get_config('CONTACTCONTROL',2) returning_values :contactcontrol;
  if (:contactcontrol is null) then contactcontrol = '0';
  if ((:contactcontrol = '1') and (new.telefon is not null)) then
    execute procedure contact_control('PHONE',new.telefon,0,'PKLIENCI','TELEFON',new.ref,new.skrot);

END^
SET TERM ; ^
