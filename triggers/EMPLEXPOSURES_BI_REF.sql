--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLEXPOSURES_BI_REF FOR EMPLEXPOSURES                  
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('EMPLEXPOSURES') returning_values new.ref;
end^
SET TERM ; ^
