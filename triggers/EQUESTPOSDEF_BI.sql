--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EQUESTPOSDEF_BI FOR EQUESTPOSDEF                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_equestposdef,1);
end^
SET TERM ; ^
