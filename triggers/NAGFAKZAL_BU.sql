--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAKZAL_BU FOR NAGFAKZAL                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable stawka numeric(14,2);
declare variable kurs numeric(14,4);
declare variable akcept integer;
begin
  if((new.wartbru<>old.wartbru) or (new.wartnet<>old.wartnet)) then begin
    select akceptacja from nagfak where ref=new.faktura into :akcept;
    if(:akcept=1 or :akcept=8) then exception NAGFAKZAL_AKCEPTACJA;
    select stawka from VAT where grupa=new.vat into :stawka;
    select kurs from NAGFAK where REF=new.fakturazal into :kurs;
    if(new.wartbru<>old.wartbru) then begin
      new.wartbruzl = new.wartbru * :kurs;
      new.wartnet = cast((new.wartbru / (1+:stawka / 100)) as DECIMAL(14,2));
      new.wartnetzl = cast((new.wartbruzl / (1+:stawka / 100)) as DECIMAL(14,2));
    end else begin
      new.wartnetzl = new.wartnet * :kurs;
      new.wartbru = new.wartnet + cast((new.wartnet * (:stawka / 100)) as DECIMAL(14,2));
      new.wartbruzl = new.wartnetzl + cast((new.wartnetzl * (:stawka / 100)) as DECIMAL(14,2));
    end
  end
end^
SET TERM ; ^
