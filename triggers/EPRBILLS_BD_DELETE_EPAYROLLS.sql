--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRBILLS_BD_DELETE_EPAYROLLS FOR EPRBILLS                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  delete from epayrolls where ref = old.ref;
end^
SET TERM ; ^
