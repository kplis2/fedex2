--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKACCOUNTS_AI_WPK FOR BKACCOUNTS                     
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  --Badamy czy konto zostało wkopiowane z wzorca
  if (new.pattern_ref is distinct from null) then
  begin
    -- Czyli konto dodane ze wzorca ergo nie ma jeszcze analityk, więc trzeba je skopiować ze wzorca
    -- no i oczywicie kopnąć analityki, na triggerach, żeby dodać slodefy

    -- debug+
    --exception universal' '||new.pattern_ref||' '||new.yearid||' '|| new.symbol;
    -- debug-

    insert into accstructure ( bkaccount, nlevel, separator, dictdef, dictfilter,
      len, ord, doopisu, pattern_ref)
    select new.ref , a.nlevel, a.separator, a.dictdef, a.dictfilter,
      a.len, a.ord, a.doopisu, a.ref
      from accstructure a
      where a.bkaccount = new.pattern_ref;

    -- Pamiętać należy o wyróżnikach dopiętych do konta parenta, więc trzeba by zainsertować
    -- rekordy wiążące dany ślownik wyróżnikowy z danym kontem - ale nowym kontem, a nie kontem parentem
    insert into accstructuredists(number,distdef,distfilter,otable,oref,ord, pattern_ref)
      select number,distdef,distfilter,otable, new.ref, ord, ref
        from accstructuredists a
          where oref = new.pattern_ref and otable = 'BKACCOUNTS';
  end
end^
SET TERM ; ^
