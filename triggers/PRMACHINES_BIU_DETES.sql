--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRMACHINES_BIU_DETES FOR PRMACHINES                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.PRODDATE<>old.PRODDATE or old.PRODDATE is null or new.BUYDATE<>old.BUYDATE or old.BUYDATE is null) then begin
    if (new.PRODDATE>new.BUYDATE) then exception PRMACHINES_EXPT 'Data produkcji nie może być późniejsza od daty zakupu.';
  end
end^
SET TERM ; ^
