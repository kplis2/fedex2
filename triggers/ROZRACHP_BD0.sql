--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACHP_BD0 FOR ROZRACHP                       
  ACTIVE BEFORE DELETE POSITION 0 
as
  declare variable symbol varchar(255);
begin
  if (exists(select ref from decrees where dtratediff = old.ref)) then
  begin
    select first 1 B.symbol from decrees D
        join bkdocs B on (B.ref = D.bkdoc)
      where D.dtratediff = old.ref
      into :symbol;
    exception CANT_DELETE_ROZRACHP 'Nie można usunąć zapisu rozrachunku. Różnice kursowe: ' || symbol;
  end
  if (exists(select ref from decrees where ctratediff = old.ref)) then
  begin
    select first 1 B.symbol from decrees D
        join bkdocs B on (B.ref = D.bkdoc)
      where D.ctratediff = old.ref
      into :symbol;
    exception CANT_DELETE_ROZRACHP 'Nie można usunąć zapisu rozrachunku. Różnice kursowe: ' || symbol;
  end
  if (old.notadokum is not null) then
    exception CANT_DELETE_ROZRACHP_NOTA substring('Do rozrachunku '||old.symbfak||' jest już wystawiona nota '||(select n.symbol||' w okresie '||n.period from notynag n where n.ref = old.notadokum) from 1 for 255);
  if (exists(select ref from fsclraccp where rozrachp = old.ref)) then
  begin
    select max(iif(f.fstype = 0,'rozliczenie ','kompensata ')|| 'w okresie '||f.okres||' o '|| iif(coalesce(f.symbol,'') = '', 'nr '||f.ref, coalesce('symbolu '||F.symbol,'nr '||F.ref)))
      from fsclracch F
        join fsclraccp P on (P.fsclracch = F.ref)
      where P.rozrachp = old.ref
      into :symbol;

    exception cant_delete_rozrachp substring('Nie można usunąć zapisu rozrachunku. Istnieje ' || coalesce(symbol,'') from 1 for 255);
  end
end^
SET TERM ; ^
