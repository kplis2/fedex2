--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRACTS_BUD_CONTR FOR EMPLCONTRACTS                  
  ACTIVE BEFORE UPDATE OR DELETE POSITION 4 
AS
begin
  if (deleting or old.empltype = 1 and new.empltype <> 1) then
  begin
    if (exists(select ec.ref
        from emplcontracts ec
        where  ec.employee = old.employee
          and ec.empltype = 1
          and ec.fromdate = old.enddate + 1)
      and exists(select ec.ref
        from emplcontracts ec
        where  ec.employee = old.employee
          and ec.empltype = 1
          and ec.enddate = old.fromdate - 1)) then
      exception contr_emplcontr;
  end
end^
SET TERM ; ^
