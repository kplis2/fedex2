--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_BU_PRSHMAT FOR POZZAM                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable cnt integer;
declare variable sheet integer;
begin
  if ((new.wersjaref <> old.wersjaref or (old.wersjaref is null and new.wersjaref is not null))
    and exists(select ref from nagzam where ref = new.zamowienie and prsheet is not null)) then
  begin
    select prsheet
      from nagzam
      where ref = new.zamowienie
      into :sheet;

    if (:sheet is null) then
      exit;

    select count(pm.ref)
      from prshmat pm
        join prsheets ps on (ps.ref = pm.sheet)
      where pm.wersjaref = new.wersjaref
        and pm.sheet = :sheet
      into :cnt;

    if (cnt = 1) then
      select pm.ref
        from prshmat pm
          join prsheets ps on (ps.ref = pm.sheet)
        where pm.wersjaref = new.wersjaref
          and pm.sheet = :sheet
        into new.prshmat;
    else
      exception PRSCHEDGUIDES_ERROR 'POZZAM_BI_PRSHMAT: Brak wskazania surowca z k.tech';
  end 
end^
SET TERM ; ^
