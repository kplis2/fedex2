--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BHPTIMEUNITS_BI_DEFAULTUNITS FOR BHPTIMEUNITS                   
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
--MWr Domyslna jednostka moze byc tylko jedna

  if (new.def4period = 1 and (new.def4period <> old.def4period or (new.def4period is null and old.def4period is not null) or (new.def4period is not null and old.def4period is null)))
  then begin
    update bhptimeunits
      set def4period = 0
      where ref <> new.ref;
  end

  if (new.def4wash = 1 and (new.def4wash <> old.def4wash or (new.def4wash is null and old.def4wash is not null) or (new.def4wash is not null and old.def4wash is null)))
  then begin
    update bhptimeunits
      set def4wash = 0
      where ref <> new.ref;
  end
end^
SET TERM ; ^
