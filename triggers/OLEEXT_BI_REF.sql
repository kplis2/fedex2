--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OLEEXT_BI_REF FOR OLEEXT                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('OLEEXT')
      returning_values new.REF;
end^
SET TERM ; ^
