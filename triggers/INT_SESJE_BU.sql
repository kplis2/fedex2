--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INT_SESJE_BU FOR INT_SESJE                      
  ACTIVE BEFORE UPDATE POSITION 5 
as
begin

  if (old.liczbaobiektow is distinct from new.liczbaobiektow
      and coalesce(old.liczbaobiektow,0) = 0
      and coalesce(new.liczbaobiektow,0) > 0
      ) then
    begin
        new.datainskoniec = current_timestamp(0);
    end

  if (old.licznikprzetw is distinct from new.licznikprzetw
      and old.dataostprzetw is not distinct from new.dataostprzetw
      ) then
    begin
      new.dataostprzetw = current_timestamp(0);
    end

   if (old.dataostprzetw is distinct from new.dataostprzetw
       and old.licznikprzetw is not distinct from new.licznikprzetw
       ) then
    begin
      new.licznikprzetw = coalesce(new.licznikprzetw,0) + 1;
    end

  if (new.dataprzetw is null) then
    new.dataprzetw = new.dataostprzetw;

  if (new.neosdataprzetw is null
      and new.neosdataostprzetw is not null) then
    new.neosdataprzetw = new.neosdataostprzetw;
    
end^
SET TERM ; ^
