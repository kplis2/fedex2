--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYS_BI_REF FOR LISTYWYS                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('LISTYWYS')
      returning_values new.REF;
end^
SET TERM ; ^
