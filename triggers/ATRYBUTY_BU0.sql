--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ATRYBUTY_BU0 FOR ATRYBUTY                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable kod varchar(20);
declare variable iddefcechw integer_id;
begin
  if ((new.wersjaref<>old.wersjaref or new.ktm is null or new.nrwersji is null)
      and new.wersjaref is not null and new.wersjaref <> 0) then
  begin
    select nrwersji, ktm from WERSJE
      where ref=new.wersjaref
      into new.nrwersji, new.ktm;
  end else
  begin
    if (new.ktm<>old.ktm or new.nrwersji<>old.nrwersji
        or new.wersjaref<>old.wersjaref or (new.wersjaref is null)) then
      select ref from WERSJE
        where ktm=new.ktm and nrwersji=new.nrwersji
          into new.wersjaref;
  end
  if (new.cecha <> old.cecha or new.wartosc <> old.wartosc) then begin
    if (new.cecha is not null and new.wartosc is not null and new.wartosc<>'') then begin
        execute procedure getcecha(new.ktm, new.cecha, new.wartosc) returning_values :iddefcechw;
        select kod from defcechw where id_defcechw=:iddefcechw into :kod;
        if (kod = '') then new.kod=null;
        else new.kod = :kod;
    end
  end
end^
SET TERM ; ^
