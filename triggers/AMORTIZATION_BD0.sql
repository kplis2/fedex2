--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER AMORTIZATION_BD0 FOR AMORTIZATION                   
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if (old.tamort is not null) then
    update fxdassets set tredemptiongross = coalesce(tredemptiongross,0) - old.tamort where ref = old.fxdasset;

  if (old.famort is not null) then
    update fxdassets set fredemptiongross = coalesce(fredemptiongross,0) - old.famort where ref = old.fxdasset;
end^
SET TERM ; ^
