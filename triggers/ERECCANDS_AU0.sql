--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ERECCANDS_AU0 FOR ERECCANDS                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.status <> old.status or new.notes <> old.notes or coalesce(new.refuscause,0) <> coalesce(old.refuscause,0)) then
    insert into ereccandhist (ereccand, status, mark, notes, refuscause)
      values (new.ref, new.status, new.mark, new.notes, new.refuscause);
end^
SET TERM ; ^
