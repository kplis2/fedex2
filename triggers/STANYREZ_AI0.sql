--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYREZ_AI0 FOR STANYREZ                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable cnt integer;
declare variable typ char;
begin
  select d.typ
    from defmagaz d
    where d.symbol = new.magazyn
    into :typ;
  if(((new.status = 'R') or (new.status = 'B') or (new.status = 'Z') or (new.status = 'D')) and new.zreal <> 1) then begin
    cnt = null;
    if(new.onstcen = 1) then begin
      cnt = 0;
      if(exists(select first 1 1 from STANYCEN where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja
              and ((new.cena = cena) or (new.cena is null) or (new.cena = 0 and :typ = 'E') )
              and ((new.dostawa = dostawa) or (new.dostawa is null)))) then cnt = 1;
      if(:cnt > 0) then begin
        if(new.status = 'R' or (new.status = 'Z')) then
           update STANYCEN set ZAREZERW = ZAREZERW+new.ilosc where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja
              and ((new.cena = cena) or (new.cena is null) or (new.cena = 0 and :typ = 'E'))
              and ((new.dostawa = dostawa) or (new.dostawa is null));
        else if(new.status = 'B') then
           update STANYCEN set ZABLOKOW = ZABLOKOW + new.ilosc where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja
              and ((new.cena = cena) or (new.cena is null) or (new.cena = 0 and :typ = 'E') )
              and ((new.dostawa = dostawa) or (new.dostawa is null));
        else if(new.status = 'D') then
           update STANYCEN set ZAMOWIONO = ZAMOWIONO + new.ilosc where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja
              and ((new.cena = cena) or (new.cena is null) or (new.cena = 0 and :typ = 'E') )
              and ((new.dostawa = dostawa) or (new.dostawa is null));
      end else begin
        if(new.status = 'R' or (new.status = 'Z')) then
           insert into  STANYCEN(MAGAZYN, KTM, WERSJA, CENA, DOSTAWA,ILOSC, WARTOSC,ZAREZERW, ZABLOKOW, ZAMOWIONO)
             values(new.magazyn, new.ktm, new.wersja, new.cena, new.dostawa,0,0,new.ilosc,0,0);
        else if(new.status = 'B') then
           insert into  STANYCEN(MAGAZYN, KTM, WERSJA, CENA, DOSTAWA,ILOSC, WARTOSC,ZAREZERW, ZABLOKOW, ZAMOWIONO)
             values(new.magazyn, new.ktm, new.wersja, new.cena, new.dostawa,0,0,0,new.ilosc,0);
        else if(new.status = 'D') then
           insert into  STANYCEN(MAGAZYN, KTM, WERSJA, CENA, DOSTAWA,ILOSC, WARTOSC,ZAREZERW, ZABLOKOW, ZAMOWIONO)
             values(new.magazyn, new.ktm, new.wersja, new.cena, new.dostawa,0,0,0,0,new.ilosc);
      end
    end else begin
      /*rozliczenie na stanach ilosciowych*/
      cnt = 0;
      if(exists(select first 1 1 from STANYIL where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja)) then cnt = 1;
      if(:cnt > 0) then begin
        if(new.status = 'R' or (new.status = 'Z')) then
           update STANYIL set ZAREZERW = ZAREZERW+new.ilosc where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja;
        else if (new.status = 'B') then
           update STANYIL set ZABLOKOW = ZABLOKOW + new.ilosc where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja;
        else if (new.status = 'D') then
           update STANYIL set ZAMOWIONO = ZAMOWIONO + new.ilosc where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja;
      end else begin
        if(new.status = 'R' or (new.status = 'Z')) then
           insert into  STANYIL(MAGAZYN, KTM, WERSJA, ILOSC, WARTOSC,ZAREZERW, ZABLOKOW)
             values(new.magazyn, new.ktm, new.wersja, 0,0,new.ilosc,0);
        else if(new.status = 'B') then
           insert into  STANYIL(MAGAZYN, KTM, WERSJA, ILOSC, WARTOSC,ZAREZERW, ZABLOKOW)
             values(new.magazyn, new.ktm, new.wersja, 0,0,0,new.ilosc);
        else if(new.status = 'D') then
           insert into  STANYIL(MAGAZYN, KTM, WERSJA, ILOSC, WARTOSC,ZAREZERW, ZABLOKOW, ZAMOWIONO)
             values(new.magazyn, new.ktm, new.wersja, 0,0,0,0,new.ilosc);
      end
    end
  end
end^
SET TERM ; ^
