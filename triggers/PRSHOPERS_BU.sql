--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHOPERS_BU FOR PRSHOPERS                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable status integer;
declare variable konfigs varchar(255);
declare variable konfig numeric(14,4);
declare variable quantity numeric(14,4);
declare variable opertime numeric(14,4);
begin
  if(new.oper='') then new.oper = NULL;
  if(new.workplace='') then new.workplace = NULL;
  if(new.scheduled is null) then new.scheduled = 0;
  if(new.calculated is null) then new.calculated = 0;
  if(new.reported is null) then new.reported = 0;
  if(new.amountresultcalctype is null) then new.amountresultcalctype = 0;
  if ((new.calculated = 1 or new.scheduled = 1) and new.oper is null) then exception prsheets_data 'Nie uzupeniony typ operacji';
  if(new.complex is not null and new.complex = 0 and (old.complex is null or new.complex <> old.complex) and
    exists(select ref from prshopers po where po.opermaster = new.ref)) then begin
    exception prsheets_opercom_change 'Operacja ma zdefioniowaną operacje podrzedną.';
  end
  if(old.complex is not null and old.complex = 0 and (new.complex is null or new.complex <> old.complex) and
    (exists(select ref from prshtools pt where pt.opermaster = new.ref) or
     exists(select ref from prshmat pm where pm.opermaster = new.ref))) then begin
    exception prsheets_opercom_change;
  end
  if(new.complex = 1 and (new.opertype is null or new.opertype < 0)) then exception PRSHEETS_DATA 'Rodzaj operacji musi być wskazany';
  if(new.scheduled = 1 and old.scheduled <> 1) then begin
    if(new.opermaster is not null) then execute procedure prsheets_no_operschedloop_up(new.opermaster) returning_values status;
    execute procedure prsheets_no_operschedloop_down(new.ref) returning_values status;
  end
  new.eoperfactor = replace(new.eoperfactor,',','.');
  new.eopertime = replace(new.eopertime,',','.');
  new.eplacetime = replace(new.eplacetime,',','.');
  new.econdition = replace(new.econdition,',','.');
  if(new.reportedfactor is null or new.reportedfactor = 0) then new.reportedfactor = 1;
  if (new.amountinfrom is null) then new.amountinfrom = 0;
  if (new.reported = 0 and old.reported > 0) then
  begin
    if (exists (select first 1 1 from prshmat m where m.opermaster = new.ref and m.autodocout = 1)) then
      exception prsheets_data 'Do operacji zdefiniowano surowce rozchodowane automatycznie przy raportowaniu';
  end
  if (new.complex = 1 and new.serialnumbergen <> '') then new.serialnumbergen = null;
end^
SET TERM ; ^
