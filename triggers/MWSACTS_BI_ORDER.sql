--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_BI_ORDER FOR MWSACTS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable maxnum integer;
begin
  new.ord = 1;
  select max(number) from mwsacts where mwsord = new.mwsord
    into :maxnum;
  if (new.number is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update mwsacts set ord = 0, number = number + 1
      where number >= new.number and mwsord = new.mwsord;
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
