--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRPSNS_BU_SUMORD FOR FRPSNS                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (new.algorithm = 2 and old.algorithm <> 2) then
    new.countord = 0;
  if (new.countord = 0) then begin
    select max(frpsns.countord) + 1
      from frsumpsns
        join frpsns on (frpsns.ref = frsumpsns.sumpsn)
      where frsumpsns.frpsn = new.ref and frsumpsns.frversion = 0
    into new.countord;
    if (new.countord is null or new.countord = 0) then new.countord = 1;
  end
end^
SET TERM ; ^
