--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDCELLS_BI0 FOR FRDCELLS                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable symbol varchar(40);
declare variable perspect integer;
declare variable pedsdimhier integer;
declare variable persdimval integer;
begin
  if (new.frdpersdimhier is null and new.frdpersdimval is not null) then
    select frdpersdimvals.frdpersdimshier
      from frdpersdimvals
      where frdpersdimvals.ref = new.frdpersdimval
      into new.frdpersdimhier;
  if (new.frdperspects is null and new.frdpersdimhier is not null) then
    select frdpersdimhier.frdperspect
      from frdpersdimhier
      where frdpersdimhier.ref = new.frdpersdimhier
      into new.frdperspects;
  select frdhdrs.frhdr
    from frdhdrsvers
      join frdhdrs on (frdhdrs.ref = frdhdrsvers.frdhdr)
    where frdhdrsvers.ref = new.frdhdrver
    into :symbol;
  select frdpersdimvals.ref,frdpersdimhier.ref, frdperspects.ref
    from frdpersdimvals
      join frdpersdimhier on (frdpersdimhier.ref = frdpersdimvals.frdpersdimshier)
      join frdperspects on (frdperspects.ref = frdpersdimhier.frdperspect)
    where frdpersdimvals.ref = new.frdpersdimval
    into :persdimval, :pedsdimhier, :perspect;
  if (:symbol is not null and :symbol <> '' and :persdimval is not null
      and :pedsdimhier is not null and :perspect is not null
      and new.dimelem is not null and new.timelem is not null) then
    new.adress = '[PLAN:'||:symbol||'][VERS:'||new.frdhdrver||'][PERSPECT:'||
      :perspect||'][PERSHIER:'||:pedsdimhier||'][PERSDIMVAL:'||:persdimval||
      '][ELEMENT:'||new.dimelem||'][TIMELEM:'||new.timelem||']';
  else
    exception FRD_CELL_ADRESS_NOT_COMPLETE;
end^
SET TERM ; ^
