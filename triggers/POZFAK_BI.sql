--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAK_BI FOR POZFAK                         
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable korekta integer;
declare variable aktywny smallint;
declare variable towakt varchar(255);
declare variable typ varchar(3);
declare variable zakup smallint;
declare variable opak smallint;
declare variable dniwaznbufc varchar(255);
declare variable dniwaznbufi integer;
declare variable dniwazn integer;
declare variable skad smallint;
declare variable akceptacja smallint;
declare variable aktywna_wersja smallint;
begin
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji, ktm, usluga, akt from wersje where ref=new.wersjaref into new.wersja, new.ktm, new.usluga, :aktywna_wersja;
  else if(new.ktm is not null and new.wersja is not null) then
    select ref, usluga,akt from wersje where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref, new.usluga, :aktywna_wersja;
  if(new.ilosc is null) then new.ilosc = 0;
  if(new.ilosco is null) then new.ilosco = 0;
  if(new.iloscm is null) then new.iloscm = 0;
  if(new.pilosc is null) then new.pilosc = 0;
  if(new.piloscm is null) then new.piloscm = 0;
  if(new.pcenacen is null) then new.pcenacen = 0;
  if(new.pcenanet is null) then new.pcenanet = 0;
  if(new.pcenabru is null) then new.pcenabru = 0;
  if(new.rabat is null) then new.rabat = 0;
  if(new.rabattab is null) then new.rabattab = 0;
  if(new.magazyn = '') then new.magazyn = null;
  if(new.kosztzak is null) then new.kosztzak = 0;
  if(new.kosztkat is null) then new.kosztkat = 0;
  if(new.pkosztzak is null) then new.pkosztzak = 0;
  if(new.obliczcenzak is null) then new.obliczcenzak = 0;
  if(new.obliczcenkoszt is null) then new.obliczcenkoszt = 0;
  if(new.obliczcenfab is null) then new.obliczcenfab = 0;
  if(new.zaplacono is null) then new.zaplacono = 0;
  if(new.wersja is null) then new.wersja = 0;
  if(new.opk is null) then new.opk = 0;
  if(new.pgr_vat is null) then new.pgr_vat = new.gr_vat;
  if(new.foc is null) then new.foc = 0;
  new.fake = coalesce(new.fake,0);
  if (new.alttopoz is not null) then
  begin
    select skad
      from nagfak
      where ref = new.dokument
      into :skad;
    if( skad > 0 ) then
      if (new.frompozzam is null and
          new.fromdokumpoz is null
          and not exists (select first 1 1
                          from pozfak
                          where ref = new.alttopoz
                            and ( frompozzam is not null
                            or fromdokumpoz is not null))) then
        exception poz_fake;
  end
  if(new.magazyn is null) then
    select stansprzed.magazyn
    from nagfak
    left join stansprzed on (nagfak.stanowisko = stansprzed.stanowisko)
    where nagfak.ref=new.dokument into new.magazyn;
  if(new.magazyn = '') then new.magazyn = null;
  select akt, dniwazn from towary where ktm = new.ktm into :aktywny, :dniwazn;
  skad = 0;
  akceptacja = 0;
  select typ, skad, akceptacja from nagfak where ref = new.dokument into :typ, :skad, :akceptacja;
  if(:akceptacja=1) then exception nagfak_akcept;
  select korekta, zakup, opak from typfak where symbol = :typ into :korekta, :zakup,  :opak;
  new.zakup = :zakup;
  execute procedure getconfig('TOWARAKTYWNY') returning_values :towakt;
  if(:towakt='1' and (:aktywny<>1 or :aktywna_wersja = 0) and :korekta=0) then
    if (:aktywny = 0) then
      exception towar_nieaktywny 'Wybrany towar '||new.ktm||' jest nieaktywny.';
    else
      exception towar_nieaktywny 'Wybrano nieaktywną wersję towaru '||new.ktm;
  if(new.frompozzam > 0 and new.dostawa is null) then begin
    select dostawamag from pozzam where ref=new.frompozzam into new.dostawa;
  end
  if(:skad = 0 and new.dostawa is null and :dniwazn>0 and :korekta = 0 and :zakup = 1 and :opak = 0) then begin
    execute procedure getconfig('DNIWAZNBUF') returning_values :dniwaznbufc;
    if(:dniwaznbufc<>'' and :dniwaznbufc<>'-1') then begin
      if(new.datawazn is null) then exception dokumpoz_bad_expirydate 'Nie podano daty ważnożci dla: ' || new.ktm || '..';
        dniwaznbufi = cast(:dniwaznbufc as integer);
      if(new.datawazn < current_date) then
        exception dokumpoz_bad_expirydate 'Data ważnożci mniejsza niż dzisiejsza. ';
      if(new.datawazn > current_date + :dniwazn + :dniwaznbufi) then
        exception dokumpoz_bad_expirydate   'Błędna data ważnosći .Asortyment ma ' || :dniwazn || ' dni przydatności. ';
    end
  end
  if(new.krajpochodzenia = '') then new.krajpochodzenia = null;
  if(new.krajpochodzenia <> '') then
    update towary set towary.krajpochodzenia = new.krajpochodzenia where towary.ktm = new.ktm;
  if(coalesce(new.nazwat,'') = '') then select nazwa from towary where ktm = new.ktm into new.nazwat;
  if(coalesce(new.pkwiu,'') = '') then select pkwiu from wersje where ref = new.wersjaref into new.pkwiu;
  if(coalesce(new.pkwiu,'') = '') then select pkwiu from towary where ktm = new.ktm into new.pkwiu;
end^
SET TERM ; ^
