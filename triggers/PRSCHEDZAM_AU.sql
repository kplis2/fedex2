--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDZAM_AU FOR PRSCHEDZAM                     
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.number <> old.number or (new.number is not null and old.number is null) ) then
    update PRSCHEDOPERS set SCHEDZAMNUM = new.number where SCHEDZAM = new.ref;
end^
SET TERM ; ^
