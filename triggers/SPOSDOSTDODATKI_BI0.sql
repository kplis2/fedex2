--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPOSDOSTDODATKI_BI0 FOR SPOSDOSTDODATKI                
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.procent is null) then new.procent = 0;
  if(new.kwota is null) then new.kwota = 0;
end^
SET TERM ; ^
