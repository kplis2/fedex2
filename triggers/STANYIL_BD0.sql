--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYIL_BD0 FOR STANYIL                        
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable typ_mag char(1);
begin
  select TYP from DEFMAGAZ where SYMBOL=old.magazyn into :typ_mag;
  if(:typ_mag='S' and old.wartosc<>0) then
    exception STANYIL_DELETE_NOT_ALLOW;
  if((old.ilosc <> 0) or (old.zablokow<>0) or (old.zamowiono<>0) or (old.zarezerw<>0)) then begin
    exception STANYIL_DELETE_NOT_ALLOW;
  end else begin
    delete from STANYCEN where (STANYCEN.MAGAZYN=old.magazyn) and (STANYCEN.KTM=old.ktm) and (STANYCEN.WERSJA=old.wersja);
  end
end^
SET TERM ; ^
