--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHMEASUREACTS_BI_REF FOR PRSHMEASUREACTS                
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
   if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('PRSHMEASUREACTS')
      returning_values new.REF;
end^
SET TERM ; ^
