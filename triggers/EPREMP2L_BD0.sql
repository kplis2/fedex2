--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPREMP2L_BD0 FOR EPREMPL                        
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable status integer;
begin
  select status from epayrolls where ref = old.epayroll
    into :status;
  if (status > 0) then exception payroll_is_closed;
  if (old.status > 0) then exception EPREMPL_ACCEPTED;
end^
SET TERM ; ^
