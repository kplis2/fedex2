--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDSYM_AU0 FOR LISTYWYSDSYM                   
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.symbol <> old.symbol) then
    execute procedure listywysd_oblsymprzew(new.listywysddok);
end^
SET TERM ; ^
