--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDTYPEDEST4OP_AI0 FOR MWSORDTYPEDEST4OP              
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  if (not exists(select first 1 1 from opermagmwshhfuncs o where o.operator = new.operator and o.magazyn = new.wh)) then
    insert into opermagmwshhfuncs (operator, magazyn, mwshhfunc)
      select new.operator, new.wh, f.ref
        from mwshhfuncdefs f
        where f.disabled = 0 and f.alwaysenabled = 0;
end^
SET TERM ; ^
