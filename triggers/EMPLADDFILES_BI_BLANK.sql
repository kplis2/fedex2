--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLADDFILES_BI_BLANK FOR EMPLADDFILES                   
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable po integer;
declare variable eo integer;
begin
 select persorder, emplorder
    from empladdfiletypes
    where ref = new.filetype
    into po,eo;
/*Gdy kartoteka widoczna w osobowej lub wspoldzielona*/
  if((coalesce(new.employee,0) = 0 or eo = -1) or (po > -1 and eo > -1) ) then
    new.employee = null;
--Gdy kartoteka widoczna w pracowniczej
  if(coalesce(new.person,0) = 0 or po = -1) then
    new.person = null;
  if(new.person is null and new.employee is null) then
    exception universal'Brak przypisania do pracownika i osoby';
end^
SET TERM ; ^
