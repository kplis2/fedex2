--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERWIA_BD_ORDER FOR DEFOPERWIA                     
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then
    exit;
  select max(numer) from defoperwia where operacja = old.operacja
     into :maxnum;
  if (old.numer = :maxnum) then
    exit;
  update defoperwia set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and operacja = old.operacja;
end^
SET TERM ; ^
