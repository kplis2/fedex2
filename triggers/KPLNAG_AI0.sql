--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KPLNAG_AI0 FOR KPLNAG                         
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable stat integer;
begin
  if(new.glowna=1) then begin
    update KPLNAG set glowna=0 where wersjaref=new.wersjaref and ref<>new.ref and glowna=1;
    /* aktualizacja cen kompletow, na worku bedzie trzeba obudowac konfigiem */
    execute procedure CENNIK_AKTUKOMPLETY(new.ktm,NULL) returning_values :stat;
  end
end^
SET TERM ; ^
