--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ERECADV_AU0 FOR ERECADV                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.cost <> old.cost) then
    update erecruits set costs = costs - old.cost + new.cost where ref = new.recruit;
end^
SET TERM ; ^
