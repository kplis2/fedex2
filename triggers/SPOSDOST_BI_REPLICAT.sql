--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPOSDOST_BI_REPLICAT FOR SPOSDOST                       
  ACTIVE BEFORE INSERT POSITION 1 
AS
begin
 execute procedure rp_trigger_bi('SPOSDOST',new.ref, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
