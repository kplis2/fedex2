--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROMOCJE_BU_REPLICAT FOR PROMOCJE                       
  ACTIVE BEFORE UPDATE POSITION 3 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if(((new.ref <> old.ref)
   or (new.replicat <> old.replicat)
   or (new.rodzaj <> old.rodzaj) or (new.rodzaj is not null and old.rodzaj is null) or (new.rodzaj is null and old.rodzaj is not null)
   or (new.nazwa <> old.nazwa) or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null)
   or (new.opis <> old.opis) or (new.opis is not null and old.opis is null) or (new.opis is null and old.opis is not null)
   or (new.wyklucz <> old.wyklucz) or (new.wyklucz is not null and old.wyklucz is null) or (new.wyklucz is null and old.wyklucz is not null)
   or (new.dataod <> old.dataod) or (new.dataod is not null and old.dataod is null) or (new.dataod is null and old.dataod is not null)
   or (new.datado <> old.datado) or (new.datado is not null and old.datado is null) or (new.datado is null and old.datado is not null)
   or (new.typ <> old.typ) or (new.typ is not null and old.typ is null) or (new.typ is null and old.typ is not null)
   or (new.wartosc <> old.wartosc) or (new.wartosc is not null and old.wartosc is null) or (new.wartosc is null and old.wartosc is not null)
   or (new.liczba <> old.liczba) or (new.liczba is not null and old.liczba is null) or (new.liczba is null and old.liczba is not null)
   or (new.cecha <> old.cecha) or (new.cecha is not null and old.cecha is null) or (new.cecha is null and old.cecha is not null)
   or (new.wartcechy <> old.wartcechy) or (new.wartcechy is not null and old.wartcechy is null) or (new.wartcechy is null and old.wartcechy is not null)
   or (new.minilosc <> old.minilosc) or (new.minilosc is not null and old.minilosc is null) or (new.minilosc is null and old.minilosc is not null)
   or (new.grupakli <> old.grupakli) or (new.grupakli is not null and old.grupakli is null) or (new.grupakli is null and old.grupakli is not null)
   or (new.wynik <> old.wynik) or (new.wynik is not null and old.wynik is null) or (new.wynik is null and old.wynik is not null)
   or (new.upust <> old.upust) or (new.upust is not null and old.upust is null) or (new.upust is null and old.upust is not null)
   or (new.procent <> old.procent) or (new.procent is not null and old.procent is null) or (new.procent is null and old.procent is not null)
   or (new.ktm <> old.ktm) or (new.ktm is not null and old.ktm is null) or (new.ktm is null and old.ktm is not null)
   or (new.wersja <> old.wersja) or (new.wersja is not null and old.wersja is null) or (new.wersja is null and old.wersja is not null)
   or (new.ilosc <> old.ilosc) or (new.ilosc is not null and old.ilosc is null) or (new.ilosc is null and old.ilosc is not null)
   or (new.szablon <> old.szablon) or (new.szablon is not null and old.szablon is null) or (new.szablon is null and old.szablon is not null)
   or (new.oddzial <> old.oddzial) or (new.oddzial is not null and old.oddzial is null) or (new.oddzial is null and old.oddzial is not null)
   )
   and (new.replicat = 1)
  ) then
   waschange = 1;

  execute procedure rp_trigger_bu('PROMOCJE',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;

end^
SET TERM ; ^
