--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FXDASSETCA_BD_ACCOUNT FOR FXDASSETCA                     
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if (coalesce(old.status,0) = 1) then begin
    exception universal'Pozycja zaakceptowana - usuniecie niemożliwe';
  end else if (coalesce(old.status,0) = 2) then begin
    exception universal'Pozycja przeniesiona do archiwum - usuniecie niemożliwe';
  end
end^
SET TERM ; ^
