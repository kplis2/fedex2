--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_LISTYWYSDPOZ_BU_KOLOR FOR LISTYWYSDPOZ                   
  ACTIVE BEFORE UPDATE POSITION 20 
AS
begin
  --[PM] XXX kolor w gridzie do pakowania
  if (old.ilosc is distinct from new.ilosc
      or old.iloscspk is distinct from new.iloscspk
     ) then
    begin
      if (coalesce(old.ilosc,0) <> coalesce(new.ilosc,0)) then
        begin
          if (coalesce(new.ilosc,0) = coalesce(new.iloscspk,0) ) then
            new.x_kolor = 1;
          else
            new.x_kolor = 0;
        end
      if (coalesce(old.iloscspk,0) <> coalesce(new.iloscspk,0)) then
        begin
          if (coalesce(new.ilosc,0) = coalesce(new.iloscspk,0)) then
            begin
              if (not exists(select first 1 1 from listywysdpoz lp
                  where lp.dokument = new.dokument
                  and coalesce(lp.ilosc,0) <> coalesce(lp.iloscspk,0))
                ) then
                begin
                  new.x_kolor = 1;
                  --update listywysdpoz set x_kolor = 1 where dokument = new.dokument
                    --and x_kolor <> 1 and ref <> new.ref;
                end
              else
                begin
                  new.x_kolor = 2;
                  --update listywysdpoz set x_kolor = 0 where dokument = new.dokument
                  --  and x_kolor <> 0 and coalesce(ilosc,0) <> coalesce(iloscspk,0)
                  --  and ref <> new.ref;
                  --update listywysdpoz set x_kolor = 1 where dokument = new.dokument
                  --  and x_kolor <> 1 and coalesce(ilosc,0) = coalesce(iloscspk,0)
                  --  and ref <> new.ref;
                end
            end
          else
            begin
              new.x_kolor = 2;
              --update listywysdpoz set x_kolor = 0 where dokument = new.dokument
              --  and x_kolor <> 0 and coalesce(ilosc,0) <> coalesce(iloscspk,0)
              --  and ref <> new.ref;
              --update listywysdpoz set x_kolor = 1 where dokument = new.dokument
              --  and x_kolor <> 1 and coalesce(ilosc,0) = coalesce(iloscspk,0)
              --  and ref <> new.ref;
            end
        end
    end
end^
SET TERM ; ^
