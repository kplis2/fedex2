--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLUCZEST_AI0 FOR CPLUCZEST                      
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable autokarta integer;
begin
  if(((new.nrkarty = '') or (new.nrkarty  is null)) and new.token <> 7) then begin
     select CPL.autokarta from CPL where CPL.ref=new.cpl into :autokarta;
     if(:autokarta = 1) then begin
       insert into CPLUCZESTKART(SYMBOL, DATA, CPLUCZESTID)
       values (new.ref,current_date,new.ref);
     end
  end else if(new.nrkarty <> '' and new.token <> 7) then begin
       insert into CPLUCZESTKART(SYMBOL, DATA, CPLUCZESTID)
       values (new.nrkarty,current_date,new.ref);
  end
end^
SET TERM ; ^
