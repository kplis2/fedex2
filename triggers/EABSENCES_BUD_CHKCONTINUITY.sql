--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_BUD_CHKCONTINUITY FOR EABSENCES                      
  ACTIVE BEFORE UPDATE OR DELETE POSITION 0 
as
declare variable eabsence integer;
begin
/*MWr: Nie mozna usunac/aktualizowac rekordu, ktory stanowi ciaglosc innych
  nieobecnosci. Zmiana ecolumn moze sie odbywac jedynie w swoim zakresie(V/S)*/

  if ((deleting and coalesce(old.mflag,0) = 0) or new.employee <> old.employee or
      (new.ecolumn <> old.ecolumn and not exists (select first 1 1 from ecolparams cp1
        join ecolparams cp2 on (cp2.pval = cp1.pval and cp2.param = cp1.param and cp1.param = 'BASETYPE')
          where cp1.ecolumn = new.ecolumn and cp2.ecolumn = old.ecolumn)))
  then begin
    if (deleting) then
      eabsence = old.ref;
    else
      eabsence = new.ref;

    if (exists(select first 1 1
                 from eabsences
                 where correction in (0,2)
                   and (firstabsence = :eabsence or baseabsence = :eabsence)
                   and ref <> :eabsence))
    then begin
      if (deleting) then
         exception ABSENCE_CONTINUITY_DELETE;
      else
         exception ABSENCE_CONTINUITY_UPDATE;
    end
  end
end^
SET TERM ; ^
