--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACLIMITS_BIU_FILL_D FOR EVACLIMITS                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 9 
as
  declare variable workminutes numeric(14,2);
  declare variable lastdate date;
begin

  if (new.actlimit is distinct from old.actlimit
    or new.restlimit is distinct from old.restlimit
    or new.prevlimit is distinct from old.prevlimit
    or new.limitcons is distinct from old.limitcons
    or new.addlimit is distinct from old.addlimit
    or new.otherlimit is distinct from old.otherlimit
    or new.disablelimit is distinct from old.disablelimit
    or new.traininglimit is distinct from old.traininglimit
    or new.supplementallimit is distinct from old.supplementallimit
  ) then begin
    if (new.vyear >= 2012) then
    begin
      lastdate = cast(new.vyear||'-12-31' as date);
      if (new.vyear = extract(year from current_date)) then  --BS55404
      begin
        select first 1 dayworkdim/60
          from e_get_employmentchanges(new.employee,current_date,:lastdate,';DAYWORKDIM;')
          order by fromdate asc
          into :workminutes;
      end else
      begin
        if (new.vyear > extract(year from current_date)) then
          lastdate = cast(new.vyear||'-01-01' as date);
    
        select first 1 dayworkdim/60
          from e_get_employmentchanges(new.employee,null,:lastdate,';DAYWORKDIM;')
          order by fromdate desc
          into :workminutes;
      end
    end

    if (coalesce(workminutes,0) = 0 or new.vyear < 2012) then
      workminutes = 60 * 8;
  
    new.actlimitd = new.actlimit / workminutes;
    new.restlimitd = new.restlimit / workminutes;
    new.prevlimitd = new.prevlimit / workminutes;
    new.limitconsd = new.limitcons / workminutes;
    new.addlimitd = new.addlimit / workminutes;
    new.otherlimitd = new.otherlimit / workminutes;
    new.disablelimitd = new.disablelimit / workminutes;
    new.traininglimitd = new.traininglimit / workminutes;
    new.supplementallimitd = new.supplementallimit / workminutes;
  end
end^
SET TERM ; ^
