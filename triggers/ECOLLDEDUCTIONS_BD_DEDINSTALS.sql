--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECOLLDEDUCTIONS_BD_DEDINSTALS FOR ECOLLDEDUCTIONS                
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  delete from ecolldedinstals where insvalue = 0 and colldeduction = old.ref;
end^
SET TERM ; ^
