--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_BD_ORDER FOR POZZAM                         
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 5 or coalesce(old.fake, 0) > 0) then exit;
  update pozzam set ord = 1, numer = numer - 1
    where ref <> old.ref and numer > old.numer and zamowienie = old.zamowienie;
end^
SET TERM ; ^
