--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_AD0 FOR MWSACTS                        
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  if (old.status <> 6 and old.cortomwsact is not null) then
    execute procedure MWS_MWSACT_CALC_QUANTITYL(old.cortomwsact);
end^
SET TERM ; ^
