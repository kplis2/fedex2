--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKVATPOS_BI_FUEL FOR BKVATPOS                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (exists (select first 1 1 from grvat g where g.fuelregistry=1 and g.symbol=new.vatgr)) then
    if (coalesce(new.fuelkind,0)=0 or coalesce(new.fuelquantity,0)=0) then
      exception bkvatpos_fuelregistry;
end^
SET TERM ; ^
