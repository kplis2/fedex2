--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKPERIODS_BIU0 FOR BKPERIODS                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if(new.sidblocked is null) then new.sidblocked = 0;
  new.dname = new.yearid || ' ' || new.name;
  if (new.ptype = 1 and new.sidblocked = 1 and old.sidblocked = 0) then
  begin
    if (exists(select d.ref from dokumnag d join defmagaz m on (d.magazyn = m.symbol) where d.data >= old.sdate and d.data <= old.fdate and d.akcept not in (1,8) and coalesce(m.nieksiegowy,0)<1 and d.company=old.company))
      then exception BKPERIODS_SIDBLOCKED_AKCEPT;
    if (exists(select d.ref from dokumnot d join defmagaz m on (d.magazyn = m.symbol) where d.data >= old.sdate and d.data <= old.fdate and d.akcept not in (1,8) and coalesce(m.nieksiegowy,0)<1 and d.company=old.company))
      then exception BKPERIODS_SIDBLOCKED_AKCEPT;
    if (exists(select ref from nagfak where data >= old.sdate and data <= old.fdate and akceptacja not in (1,8) and nieobrot <> 3 and company=old.company))
      then exception BKPERIODS_SIDBLOCKED_AKCEPT;
    if (exists(select ref from rkrapkas where datado >= old.sdate and datado <= old.fdate and status =0 and company=old.company))
      then exception BKPERIODS_SIDBLOCKED_AKCEPT;
    if (exists(select ref from inwenta i where i.data >= old.sdate and i.data <= old.fdate and i.zamk <> 2))
      then exception BKPERIODS_SIDBLOCKED_AKCEPT;
  end
end^
SET TERM ; ^
