--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWS_TOWJEDN_BI FOR TOWJEDN                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable jedn varchar(5);
declare variable przelicz numeric(14,4);
declare variable newprzelicz varchar(20);
begin
  select t.miara
    from towary t
    where t.ktm = new.ktm
  into :jedn;
  przelicz = new.przelicz;
  if (przelicz = cast(przelicz as numeric(14,0))) then
    newprzelicz = cast(przelicz as numeric(14,0));
  else if (przelicz = cast(przelicz as numeric(14,1))) then
    newprzelicz = cast(przelicz as numeric(14,1));
  else if (przelicz = cast(przelicz as numeric(14,2))) then
    newprzelicz = cast(przelicz as numeric(14,2));
  else if (przelicz = cast(przelicz as numeric(14,3))) then
    newprzelicz = cast(przelicz as numeric(14,3));
  else
    newprzelicz = przelicz;
  new.mwsnazwa = new.jedn || '(' || newprzelicz || ' ' || :jedn || ')';
end^
SET TERM ; ^
