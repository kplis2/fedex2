--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CFAZY_DOCUMENT_BI FOR CFAZY                          
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable document smallint;
begin
  if (new.document = 1) then
  begin
    select contrtypes.document from contrtypes where contrtypes.ref = new.contrtype
      into :document;
    if(new.document <> :document) then
      exception CFAZY_CONTRTYPES_DOKUMENT;
  end
end^
SET TERM ; ^
