--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPREMPL_BD_DELETE_ALL_EPRPOS FOR EPREMPL                        
  ACTIVE BEFORE DELETE POSITION 1 
as
begin
  execute procedure delete_all_eprpos(old.epayroll,old.employee);
end^
SET TERM ; ^
