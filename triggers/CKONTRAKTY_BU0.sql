--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKONTRAKTY_BU0 FOR CKONTRAKTY                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(coalesce(new.nazwa,'')<>coalesce(old.nazwa,'')) then begin
    if(new.nazwa is null or new.nazwa = '') then exception CKONTRAKTY_NAZWA;
  end
  if(new.FAZA is not NULL) then
    select GRUPA from CFAZY where CFAZY.REF = new.FAZA into new.GRUPA;
  else new.GRUPA = NULL;
  if(new.cpodmiot=0) then new.cpodmiot = NULL;
  if(new.osoba=0) then new.osoba = NULL;
end^
SET TERM ; ^
