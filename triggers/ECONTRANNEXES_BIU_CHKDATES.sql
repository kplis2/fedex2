--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRANNEXES_BIU_CHKDATES FOR ECONTRANNEXES                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
as
  declare variable cfromdate timestamp;
  declare variable ctodate timestamp;
begin
/*MWr Kontrola dat na aneksie. Trigger zastepuje wczesniej istniejace triggery:
      ECONTRANNEXES_BIU_FROMDATE, ..._BIU_TODATE, ..._BU0 (PR24342)*/

  if (inserting or new.fromdate <> old.fromdate or new.atype <> old.atype
    or new.emplcontract <> old.emplcontract or new.employee <> old.employee
    or new.todate <> old.todate or (new.todate is null and old.todate is not null)
    or (new.todate is not null and old.todate is null)
  ) then begin

  --okreslenie zakresu umowy o prace i kontrola dat aneksu pod jego katem
    select fromdate, coalesce(todate, enddate) from emplcontracts
      where ref = new.emplcontract
      into :cfromdate, :ctodate;

    if (new.fromdate > :ctodate or (new.atype = 1 and new.fromdate <= :cfromdate)
      or (new.atype = 2 and (new.todate < :cfromdate or new.todate > :ctodate or new.fromdate < :cfromdate))
    ) then
      if (new.atype = 1 and new.fromdate = :cfromdate) then
        exception econtrannexes_dateoutcontrrange 'Aneks nie może rozpoczynać się w pierwszym dniu trwania umowy.';
      else
        exception econtrannexes_dateoutcontrrange;

  --dla oddelegowania musi byc data jego zakonczenia
    if (new.atype = 2) then
    begin
      if (new.todate is null) then
        exception daty_od_do_musza_byc_wypelnione;
      else if (new.todate < new.fromdate) then
        exception data_do_musi_byc_wieksza;
    end

  --aneks nie moze wchodzic w ramy czasowe innego aneksu
    if (exists(select first 1 1 from econtrannexes
      where emplcontract = new.emplcontract and ref <> new.ref
       and ((new.atype = 2 and new.fromdate <= coalesce(todate,fromdate) and new.todate >= fromdate)
         or (new.atype = 1 and new.fromdate <= coalesce(todate,fromdate) and new.fromdate >= fromdate)))
    ) then
      exception econtrannexes_dateinannexrange;
  end
end^
SET TERM ; ^
