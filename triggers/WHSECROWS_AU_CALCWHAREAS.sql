--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHSECROWS_AU_CALCWHAREAS FOR WHSECROWS                      
  ACTIVE AFTER UPDATE POSITION 1 
AS
declare variable whsecrowsref integer;
begin
  -- po dostawieniu nowego rzedu trzeba przeliczyc wszystkie obszary w rzedach
  if (new.w <> old.w or new.number <> old.number) then
  begin
    for
      select ref from whsecrows where whsecrows.whsec = new.whsec
        into :whsecrowsref
    do begin
      execute procedure MWS_ROWDIST_CALCULATE(whsecrowsref);
    end
  end
end^
SET TERM ; ^
