--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLKONTA_BI_REPLICAT FOR CPLKONTA                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
 execute procedure rp_trigger_bi('CPLKONTA',new.cpluczest, new.typpkt, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
