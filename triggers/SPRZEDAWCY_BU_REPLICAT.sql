--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDAWCY_BU_REPLICAT FOR SPRZEDAWCY                     
  ACTIVE BEFORE UPDATE POSITION 1 
AS
  declare variable waschange integer;
begin
  waschange = 0;
     if(new.ref <> old.ref or
      new.skrot <> old.skrot or (new.skrot is not null and old.skrot is null) or (new.skrot is null and old.skrot is not null) or
      new.zewn <> old.zewn or (new.zewn is not null and old.zewn is null) or (new.zewn is null and old.zewn is not null) or
      new.firma <> old.firma or (new.firma is not null and old.firma is null) or (new.firma is null and old.firma is not null) or
      new.NAZWA <> old.NAZWA or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null) or
      new.imie <> old.imie or (new.imie is not null and old.imie is null) or (new.imie is null and old.imie is not null) or
      new.nazwisko <> old.nazwisko or (new.nazwisko is not null and old.nazwisko is null) or (new.nazwisko is null and old.nazwisko is not null) or
      new.adres <> old.adres or (new.adres is not null and old.adres is null) or (new.adres is null and old.adres is not null) or
      new.kodpoczt  <> old.kodpoczt or (new.kodpoczt is not null and old.kodpoczt is null) or (new.kodpoczt is null and old.kodpoczt is not null) or
      new.poczta <> old.poczta or (new.poczta is not null and old.poczta is null) or (new.poczta is null and old.poczta is not null) or
      new.miasto <> old.miasto or (new.miasto is not null and old.miasto is null) or (new.miasto is null and old.miasto is not null) or
      new.telefon <> old.telefon or (new.telefon is not null and old.telefon is null) or (new.telefon is null and old.telefon is not null) or
      new.email <> old.email or (new.email is not null and old.email is null) or (new.email is null and old.email is not null) or
      new.nip <> old.nip or (new.nip is not null and old.nip is null) or (new.nip is null and old.nip is not null) or
      new.akt <> old.akt or (new.akt is not null and old.akt is null) or (new.akt is null and old.akt is not null) or
      new.bank <> old.bank or (new.bank is not null and old.bank is null) or (new.bank is null and old.bank is not null) or
      new.rachunek <> old.rachunek or (new.rachunek is not null and old.rachunek is null) or (new.rachunek is null and old.rachunek is not null) or
      new.kontofk <> old.kontofk or (new.kontofk is not null and old.kontofk is null) or (new.kontofk is null and old.kontofk is not null) or
      new.klient <> old.klient or (new.klient is not null and old.klient is null) or (new.klient is null and old.klient is not null)
     )
  then
   waschange = 1;

  execute procedure rp_trigger_bu('SPRZEDAWCY',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
