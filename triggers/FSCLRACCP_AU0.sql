--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FSCLRACCP_AU0 FOR FSCLRACCP                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.autochange<>1 and
     (new.currdebit2c<>old.currdebit2c or new.currcredit2c<>old.currcredit2c)) then
    execute procedure fs_make_balance(new.fsclracch);
end^
SET TERM ; ^
