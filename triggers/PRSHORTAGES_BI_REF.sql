--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHORTAGES_BI_REF FOR PRSHORTAGES                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('PRSHORTAGES') returning_values new.ref;
end^
SET TERM ; ^
