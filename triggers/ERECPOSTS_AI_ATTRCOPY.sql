--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ERECPOSTS_AI_ATTRCOPY FOR ERECPOSTS                      
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable defweight float;
declare variable attribute int;
begin
  -- przepisanie cech domylnych z EWORKPOSTATTR
  for
    select DEFWEIGHT, ATTRIBUTE
      from EWORKPOSTATTR where WORKPOST = new.WORKPOST
      into :defweight, :attribute
  do
    insert into ERECATTR (RECPOST, ATTRIBUTE, WEIGHT)
      values (new.REF, :attribute, :defweight);

end^
SET TERM ; ^
