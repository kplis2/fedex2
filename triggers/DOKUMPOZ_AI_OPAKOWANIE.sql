--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_AI_OPAKOWANIE FOR DOKUMPOZ                       
  ACTIVE AFTER INSERT POSITION 0 
AS
  declare variable towkaucja smallint;
  declare variable klientopk smallint;
  declare variable status integer;
  declare variable klient integer;
  declare variable typ varchar(3);
  declare variable opakowanie integer;
  declare variable zrodlo smallint;
begin
  select typ, klient, zrodlo from dokumnag where ref=new.dokument into :typ, :klient, :zrodlo;
  select opak from defdokum where symbol=:typ into :opakowanie;
  select kaucja from towary where ktm = new.ktm into :towkaucja;
 -- exception test_break :opakowanie ||' '|| :towkaucja;
  if (:towkaucja = 1 and :opakowanie = 1 and (:zrodlo = 0 or :zrodlo=1)) then
  begin
    select kaucja from klienci where ref = :klient into :klientopk;
    if (:klientopk >0) then
      execute procedure DOKUMPOZ_KAUCJA_OPAKOWANIE(new.ref,NULL, :klientopk) returning_values :status;
  end
end^
SET TERM ; ^
