--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRACTSDEF_AU1 FOR ECONTRACTSDEF                  
  ACTIVE AFTER UPDATE POSITION 1 
AS
begin
 if (new.name <> old.name or (new.name is not null and old.name is null)) then
 begin
   update PRSHOPERSECONTRDEFS set PRSHOPERSECONTRDEFS.econtractsdefname = new.name
     where PRSHOPERSECONTRDEFS.econtractsdef = new.ref;
 end
end^
SET TERM ; ^
