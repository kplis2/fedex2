--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHY_BU_ORDER FOR DEFCECHY                       
  ACTIVE BEFORE UPDATE POSITION 1 
as
declare variable maxnum integer;
begin
  if (NEW.ord is null) then new.ord = 1;
  if (new.lp is null) then new.lp = old.lp;
  if (new.ord = 0 or new.lp = old.lp) then
  begin
    new.ord = 1;
    exit;
  end
  --lpowanie obszarow w rzedzie gdy nie podamy lpu
  if (new.lp <> old.lp) then
    select max(lp) from defcechy where grupa = new.grupa
      into :maxnum;
  else
    maxnum = 0;
  if (new.lp < old.lp) then
  begin
    update defcechy set ord = 0, lp = lp + 1
      where symbol <> new.symbol and lp >= new.lp
        and lp < old.lp and grupa = new.grupa;
  end else if (new.lp > old.lp and new.lp <= maxnum) then
  begin
    update defcechy set ord = 0, lp = lp - 1
      where symbol <> new.symbol and lp <= new.lp
        and lp > old.lp and grupa = new.grupa;
  end else if (new.lp > old.lp and new.lp > maxnum) then
    new.lp = old.lp;
end^
SET TERM ; ^
