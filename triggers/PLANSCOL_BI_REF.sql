--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PLANSCOL_BI_REF FOR PLANSCOL                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('PLANSCOL')
      returning_values new.REF;
end^
SET TERM ; ^
