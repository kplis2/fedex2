--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCEBASES_BU_GROSS FOR EABSENCEBASES                  
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable company smallint;
  declare variable zus_wsk numeric(14,4);
begin
  if (new.sbase <> old.sbase) then
  begin
    select company from employees where ref = new.employee into :company;
    execute procedure e_calculate_zus(new.period, :company) returning_values :zus_wsk;
    new.sbasegross = new.sbase / :zus_wsk;
  end
end^
SET TERM ; ^
