--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSVERSQ_BIU_REF FOR MWSVERSQ                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSVERSQ') returning_values new.ref;
end^
SET TERM ; ^
