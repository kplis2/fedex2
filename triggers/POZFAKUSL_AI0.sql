--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAKUSL_AI0 FOR POZFAKUSL                      
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable suma numeric(14,2);
declare variable wartosc numeric(14,2);
begin
  select sum(KWOTA) from POZFAKUSL where REFPOZFAK=new.refpozfak into :suma;
  select WARTNETZL - PWARTNETZL from POZFAK where REF=new.refpozfak into :wartosc;
  if ((:wartosc < 0 and :suma < :wartosc) or (:wartosc > 0 and :suma > :wartosc)) then
    exception POZFAKUSL_ZADUZO;
end^
SET TERM ; ^
