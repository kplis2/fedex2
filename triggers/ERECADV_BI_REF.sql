--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ERECADV_BI_REF FOR ERECADV                        
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_erecadv,1);
end^
SET TERM ; ^
