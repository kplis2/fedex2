--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYREZ_AU0 FOR STANYREZ                       
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable zarezerw numeric(14,4);
declare variable zablokow numeric(14,4);
declare variable zamow numeric(14,4);
declare variable ilosc numeric(14,4);
declare variable cnt integer;
declare variable typ char;
begin
  zarezerw = 0;
  zablokow = 0;
  zamow = 0;
   select d.typ
    from defmagaz d
    where d.symbol = new.magazyn
    into :typ;
  /*zmiana stanu*/
  if((new.status <> old.status) and (new.zreal <> 1)) then begin
    if(old.status = 'R' or (old.status = 'Z')) then
      zarezerw = - old.ilosc ;
    if(old.status = 'B') then
      zablokow = - old.ilosc;
    if(old.status = 'D') then
      zamow = - old.ilosc;
    if(new.status = 'R' or (new.status = 'Z')) then
      zarezerw = :zarezerw + new.ilosc;
    if(new.status = 'B') then
      zablokow = :zablokow + new.ilosc;
    if(new.status = 'D') then
      zamow = :zamow + new.ilosc;
    cnt = null;
    if(new.onstcen = 1) then begin
      cnt = 0;
      if(exists(select first 1 1 from STANYCEN where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja
              and ((new.cena = cena) or (new.cena is null) or (new.cena = 0 and :typ = 'E'))
              and ((new.dostawa = dostawa) or (new.dostawa is null)))) then cnt = 1;
      if(:cnt > 0) then
         update STANYCEN set ZAREZERW = ZAREZERW + :zarezerw, ZABLOKOW = ZABLOKOW + :zablokow,
              ZAMOWIONO = ZAMOWIONO + :zamow
              where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja
              and ((new.cena = cena) or (new.cena is null) or (new.cena = 0 and :typ = 'E'))
              and ((new.dostawa = dostawa) or (new.dostawa is null));
      else
        insert into STANYCEN(MAGAZYN, KTM, WERSJA, CENA, DOSTAWA,ILOSC, WARTOSC, ZAREZERW, ZABLOKOW, ZAMOWIONO)
             values(new.magazyn, new.ktm, new.wersja, new.cena, new.dostawa,0,0,:zarezerw,:zablokow,:zamow);
    end else begin
      cnt = 0;
      if(exists(select first 1 1 from STANYIL where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja)) then cnt = 1;
      if(:cnt > 0) then
         update STANYIL set ZAREZERW = ZAREZERW + :zarezerw, ZABLOKOW = ZABLOKOW + :zablokow, ZAMOWIONO = ZAMOWIONO + :zamow
           where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja;
      else
        insert into  STANYIL(MAGAZYN, KTM, WERSJA, ILOSC, WARTOSC,ZAREZERW, ZABLOKOW, ZAMOWIONO)
             values(new.magazyn, new.ktm, new.wersja,0,0,:zarezerw,:zablokow, :zamow);
    end
  end else if ((new.ilosc <> old.ilosc) and (new.zreal <> 1)) then
  if((new.status = 'D') or (new.status = 'R') or (new.status = 'B') or (new.status = 'Z')) then begin
    ilosc = new.ilosc - old.ilosc;
    cnt = null;
    if(new.onstcen = 1) then begin
      cnt = 0;
      if(exists(select first 1 1 from STANYCEN where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja
              and ((new.cena = cena) or (new.cena is null) or (new.cena = 0 and :typ = 'E'))
              and ((new.dostawa = dostawa) or (new.dostawa is null)))) then cnt = 1;
      if(:cnt > 0) then begin
        if(new.status = 'R' or (new.status = 'Z')) then
           update STANYCEN set ZAREZERW = ZAREZERW+:ilosc where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja
              and ((new.cena = cena) or (new.cena is null) or (new.cena = 0 and :typ = 'E'))
              and ((new.dostawa = dostawa) or (new.dostawa is null));
        else if(new.status='B') then
           update STANYCEN set ZABLOKOW = ZABLOKOW + :ilosc where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja
              and ((new.cena = cena) or (new.cena is null) or (new.cena = 0 and :typ = 'E'))
              and ((new.dostawa = dostawa) or (new.dostawa is null));
        else if(new.status='D') then
           update STANYCEN set ZAMOWIONO = ZAMOWIONO + :ilosc where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja
              and ((new.cena = cena) or (new.cena is null) or (new.cena = 0 and :typ = 'E'))
              and ((new.dostawa = dostawa) or (new.dostawa is null));
      end else begin
        if(new.status = 'R' or (new.status = 'Z')) then
           insert into  STANYCEN(MAGAZYN, KTM, WERSJA, CENA, DOSTAWA,ILOSC, WARTOSC,ZAREZERW, ZABLOKOW, ZAMOWIONO)
             values(new.magazyn, new.ktm, new.wersja, new.cena, new.dostawa,0,0,:ilosc,0,0);
        else if(new.status='B') then
           insert into  STANYCEN(MAGAZYN, KTM, WERSJA, CENA, DOSTAWA,ILOSC, WARTOSC,ZAREZERW, ZABLOKOW, ZAMOWIONO)
             values(new.magazyn, new.ktm, new.wersja, new.cena, new.dostawa,0,0,0,:ilosc,0);
        else if(new.status='D') then
           insert into  STANYCEN(MAGAZYN, KTM, WERSJA, CENA, DOSTAWA,ILOSC, WARTOSC,ZAREZERW, ZABLOKOW, ZAMOWIONO)
             values(new.magazyn, new.ktm, new.wersja, new.cena, new.dostawa,0,0,0,0,:ilosc);
      end
    end else begin
      cnt = 0;
      if(exists(select first 1 1 from STANYIL where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja)) then cnt = 1;
      if(:cnt > 0) then begin
        if(new.status = 'R' or (new.status = 'Z')) then
           update STANYIL set ZAREZERW = ZAREZERW+:ilosc where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja;
        else if(new.status = 'B') then
           update STANYIL set ZABLOKOW = ZABLOKOW + :ilosc where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja;
        else if(new.status = 'D') then
           update STANYIL set ZAMOWIONO = ZAMOWIONO + :ilosc where MAGAZYN=new.magazyn AND KTM = new.ktm AND WERSJA = new.wersja;
      end else begin
        if(new.status = 'R' or (new.status = 'Z')) then
           insert into  STANYIL(MAGAZYN, KTM, WERSJA, ILOSC, WARTOSC,ZAREZERW, ZABLOKOW)
             values(new.magazyn, new.ktm, new.wersja, 0,0,:ilosc,0);
        else if (new.status = 'B') then
           insert into  STANYIL(MAGAZYN, KTM, WERSJA, ILOSC, WARTOSC,ZAREZERW, ZABLOKOW)
             values(new.magazyn, new.ktm, new.wersja, 0,0,0,:ilosc);
        else if (new.status = 'D') then
           insert into  STANYIL(MAGAZYN, KTM, WERSJA, ILOSC, WARTOSC,ZAREZERW, ZABLOKOW, ZAMOWIONO)
             values(new.magazyn, new.ktm, new.wersja, 0,0,0,0,:ilosc);
      end
    end
  end
-- BS39909
  if (coalesce(old.cena,0) <> coalesce(new.cena,0) and new.zreal = 0 and new.pozzam > 0) then
    update pozzam set cenamag = new.cena
      where ref = new.pozzam;
  if (coalesce(old.cena,0) <> coalesce(new.cena,0) and new.zreal = 2 and new.pozzam > 0) then
    update pozfak set cenamag = new.cena
      where ref = new.pozzam;
  if (coalesce(old.cena,0) <> coalesce(new.cena,0) and new.zreal = 3 and new.pozzam > 0) then
    update dokumpoz set cena = new.cena
      where ref = new.pozzam;
end^
SET TERM ; ^
