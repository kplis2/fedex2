--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTDETS_BI_REF FOR MWSACTDETS                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('MWSACTDETS')
      returning_values new.REF;
end^
SET TERM ; ^
