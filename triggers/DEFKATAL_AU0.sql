--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFKATAL_AU0 FOR DEFKATAL                       
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable numer integer;
declare variable typ smallint;
declare variable wartosc varchar(100);
declare variable nazwa varchar(255);
declare variable cnt integer;
begin
  if(new.nazwa <> old.nazwa) then
   update defmenu set NAZWA=new.nazwa where KATALOG=new.ref and (elem is null or (elem = 0));
  if(new.automat = 1 and old.automat = 0 and new.typ = 'C' and new.cecha is not null) then begin
    /* naliczenie domylne cech */
    select typ from DEFCECHY where SYMBOL=new.cecha into :typ;
    if(:typ = 0 or :typ =1) then begin
      for select opis, wartnum from DEFCECHW where cecha = new.cecha order by wartnum into :nazwa, :wartosc do begin
         cnt = null;
         select count(*) from DEFELEM where KATALOG = new.ref and WARTOSC = :wartosc into :cnt;
         if(:cnt is null) then cnt = 0;
         if(:cnt = 0) then begin
           numer = null;
           select max(numer) from DEFELEM where katalog = new.ref into :numer;
           if(:numer is null) then numer = 0;
           numer = numer + 1;
           nazwa = substring(:nazwa from 1 for 100);
           insert into DEFELEM(KATALOG, NUMER, NAZWA, WARTOSC)
             values(new.ref, :numer, :nazwa, :wartosc);
         end
      end
    end
    if(:typ = 0 or :typ =1) then begin
      for select opis, wartnum from DEFCECHW where cecha = new.cecha order by wartnum into :nazwa, :wartosc do begin
         cnt = null;
         select count(*) from DEFELEM where KATALOG = new.ref and WARTOSC = :wartosc into :cnt;
         if(:cnt is null) then cnt = 0;
         if(:cnt = 0) then begin
           numer = null;
           select max(numer) from DEFELEM where katalog = new.ref into :numer;
           if(:numer is null) then numer = 0;
           numer = numer + 1;
           nazwa = substring(:nazwa from 1 for 100);
           insert into DEFELEM(KATALOG, NUMER, NAZWA, WARTOSC)
             values(new.ref, :numer, :nazwa, :wartosc);
         end
      end
    end
    if(:typ = 2 ) then begin
      for select opis, wartstr from DEFCECHW where cecha = new.cecha order by wartstr into :nazwa, :wartosc do begin
         cnt = null;
         select count(*) from DEFELEM where KATALOG = new.ref and WARTOSC = :wartosc into :cnt;
         if(:cnt is null) then cnt = 0;
         if(:cnt = 0) then begin
           numer = null;
           select max(numer) from DEFELEM where katalog = new.ref into :numer;
           if(:numer is null) then numer = 0;
           numer = numer + 1;
           nazwa = substring(:nazwa from 1 for 100);
           insert into DEFELEM(KATALOG, NUMER, NAZWA, WARTOSC)
             values(new.ref, :numer, :nazwa, :wartosc);
         end
      end
    end
    if(:typ = 3 or :typ =4) then begin
      for select opis, wartdate from DEFCECHW where cecha = new.cecha order by wartdate into :nazwa, :wartosc do begin
         cnt = null;
         select count(*) from DEFELEM where KATALOG = new.ref and WARTOSC = :wartosc into :cnt;
         if(:cnt is null) then cnt = 0;
         if(:cnt = 0) then begin
           numer = null;
           select max(numer) from DEFELEM where katalog = new.ref into :numer;
           if(:numer is null) then numer = 0;
           numer = numer + 1;
           nazwa = substring(:nazwa from 1 for 100);
           insert into DEFELEM(KATALOG, NUMER, NAZWA, WARTOSC)
             values(new.ref, :numer, :nazwa, :wartosc);
         end
      end
    end
  end
end^
SET TERM ; ^
