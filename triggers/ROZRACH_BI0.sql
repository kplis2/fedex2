--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACH_BI0 FOR ROZRACH                        
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable slotyp varchar(15);
  declare variable addkonto ANALYTIC_ID;
  declare variable slokli integer;
  declare variable kod varchar(100);
  declare variable nazwa varchar(255);
  declare variable kontofk KONTO_ID;
begin
  if (new.dataotw is not null and new.dataplat is not null and new.dataotw > new.dataplat) then
    exception ROZRACH_DATA_OTW;

  if (new.walutowy is null) then new.walutowy = 0;
  if (new.rights is null) then new.rights = '';
  if (new.rightsgroup is null) then new.rightsgroup = '';
  if (new.walutowy = 0 or new.waluta is null) then
    execute procedure GETCONFIG('WALPLN') returning_values new.waluta;
  if (new.waluta is null) then new.waluta = 'PLN';
  if (new.skad is null) then new.skad = 0;
  if (new.odsetki = 0) then new.odsetki = null;
  if (new.btranamount is null) then new.btranamount = 0;
  if (new.btranamountakc is null) then new.btranamountakc = 0;
  if (new.btranamountbank is null) then new.btranamountbank = 0;
  if (new.dataplat is null) then new.dataplat = new.dataotw;
  if (new.planpayday is null) then new.planpayday = new.dataplat;
  if (new.rkdokpozamount is null) then new.rkdokpozamount = 0;

  new.datazamk = null;
  new.datazamklastoper = null;
  if (new.datazamk is null) then
    new.dni = current_date - cast(new.dataplat as date);

  if (new.klient > 0 and (new.slodef is null or new.slodef = -1)) then
  begin
    select ref from SLODEF where typ = 'KLIENCI' into new.slodef;
    new.slopoz = new.klient;
  end

  if (new.slopoz is null) then new.slopoz = new.klient;

  if (new.klient is null) then
  begin
    select REF from SLODEF where TYP = 'KLIENCI'
      into :slokli;
    if (slokli = new.slodef) then
      new.klient = new.slopoz;
  end

  if (new.klient > 0) then
    select ODSETKI from KLIENCI where REF = new.klient and ODSETKI > 0
      into new.odsetki;

  if (new.skad > 0 and (new.kontofk is null or new.kontofk = '')) then
  begin
     -- skonstruowanie symbolu ksiegowego, jesli rozrachunek jest tworzony bezposrednio z dok. SID
     select SLODEF.PREFIXFK from SLODEF where REF=new.slodef
       into new.kontofk;
     select TYP from slodef where REF=new.slodef into :slotyp;
     if (slotyp='KLIENCI') then
       select KONTOFK from KLIENCI where REF=new.slopoz
         into :addkonto;
     if (addkonto is null) then
       addkonto = '';
     new.kontofk = new.kontofk || addkonto;
  end

  if (new.kontofk is null) then
    new.kontofk = '';

  if (new.oddzial is null or new.oddzial = '') then
  begin
    if (new.faktura > 0) then
      select ODDZIAL, sposzap from NAGFAK where ref = new.faktura
        into new.oddzial, new.sposzap;
  end else if (new.faktura>0) then
  begin
    select sposzap from NAGFAK where ref = new.faktura
      into new.sposzap;
  end


  if (new.oddzial is null or new.oddzial = '') then
  begin
    execute procedure GETCONFIG('AKTUODDZIAL') returning_values new.oddzial;
  end

  if ((new.slokod is null or new.slokod = '') and new.slodef > 0 and new.slopoz > 0) then
  begin
    execute procedure SLO_DANE(new.slodef, new.slopoz)
      returning_values kod, nazwa, kontofk;
    new.slokod = substring(kod from 1 for 80);
    new.slonazwa = substring(nazwa from 1 for 80);
  end

  new.saldo = new.winien - new.ma;
  new.saldowm = new.winien - new.ma;
  new.saldowmzl = new.winienzl - new.mazl;
  if (new.saldo < 0) then new.saldo = - new.saldo;
--  new.saldodoprzelewu = new.ma - new.winien - new.btranamountbank;
--  if (new.saldodoprzelewu < 0) then new.saldodoprzelewu = 0;
  new.zamk = 0;

end^
SET TERM ; ^
