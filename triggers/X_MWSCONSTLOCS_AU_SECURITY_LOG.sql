--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWSCONSTLOCS_AU_SECURITY_LOG FOR MWSCONSTLOCS                   
  ACTIVE AFTER UPDATE POSITION 99 
as
begin
  if (new.act <> old.act) then
    insert into security_log (logfil, tablename, item, key1, oldvalue, newvalue)
      values ('MWSCONSTLOCS', 'MWSCONSTLOCS', 'ACT', new.ref, old.act, new.act);
end^
SET TERM ; ^
