--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_LISTYWYSD_BU_ANULOWANIE FOR LISTYWYSD                      
  ACTIVE BEFORE UPDATE POSITION 100 
as
begin
  if(new.akcept is distinct from old.akcept
    and old.akcept is not distinct from 2
    and (coalesce(new.statussped,0) = 1 or coalesce(new.symbolsped,'') != '') --zalozono przesylke u spedytora
    and coalesce(new.x_anulowany,0) != 1 --przesylka nie jest anulowana
  )then
    exception universal 'Nie można otworzyć powtórnie dokumentu spedycyjnego wysłanego do spedytora. Najpierw anuluj zlecenie przesyłki.';
end^
SET TERM ; ^
