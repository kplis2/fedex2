--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFFOLD_BI0 FOR DEFFOLD                        
  ACTIVE BEFORE INSERT POSITION 5 
AS
declare variable wiadomosci integer;
declare variable opread varchar(255);
declare variable opedit varchar(255);
declare variable opsend varchar(255);
declare variable publicbox smallint;
begin

--PL: propagacja praw na podfoldery
select operread, operedit, opersend from deffold where deffold.ref = new.rodzic
    into :opread, :opedit, :opsend;
  if(new.mailbox is not null) then begin
    select publicbox from mailboxes where ref=new.mailbox into :publicbox;
    if(publicbox=1) then new.typ = 'P';
    else new.typ = 'U';
  end

  if(coalesce(new.operread,'') = '') then
    new.operread = :opread;
  if(coalesce(new.operedit,'') = '') then
    if(new.rola = 2 or new.rola = 4 or new.rola = 1) then
      new.operedit = :opedit;
  if(coalesce(new.opersend,'') = '') then
    if(new.rola = 3 or new.rola = 1) then
      new.opersend = :opsend;

    wiadomosci = coalesce(new.nowewiadomosci,0) + coalesce(new.nieprzeczytane,0);
    new.FORMATFIELD = '';

    --ustawienie pogrubienia nazwy gdy s a nieprzeczytane bądź nowe wiadomosci
    if (wiadomosci = 0) then begin
        new.NAZWAWYS = new.NAZWA;
    end else begin
        new.NAZWAWYS = new.NAZWA||' ('||(wiadomosci)||')';
        new.FORMATFIELD = '*=*=%B';
    end

    --Ustawienie wyszarzenia nazwy dla niesynchronizownych folderów
    if(new.synchrobyserver = 2) then begin
        if(new.FORMATFIELD = '') then begin
            new.FORMATFIELD = '*=*=%TclDkGray';
        end else begin
            new.FORMATFIELD = new.FORMATFIELD || '%TclDkGray';
        end
    end

    --Ustawaimy na nazwie folderu kursywe gdy jest on publiczny
    --tylko dla folderu reprezentującego skrzynke
    if(new.rodzic is null) then begin
        if(new.TYP = 'P') then begin
            if(new.FORMATFIELD = '') then begin
                new.FORMATFIELD = '*=*=%I';
            end else begin
                new.FORMATFIELD = new.FORMATFIELD || '%I';
            end
        end
    end

    --Jesli dodalismy jakies formatowanie to string zamykamy '*'
    if(new.FORMATFIELD <> '') then begin
        new.FORMATFIELD = new.FORMATFIELD || '*';
    end

    if (new.synchrobyserver is null) then
      new.synchrobyserver=1;
end^
SET TERM ; ^
