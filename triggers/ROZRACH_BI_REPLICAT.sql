--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACH_BI_REPLICAT FOR ROZRACH                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
 execute procedure rp_trigger_bi('ROZRACH',new.slodef, new.slopoz, new.symbfak, new.kontofk, new.company, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
