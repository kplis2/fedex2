--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKYEARS_BD_WPK FOR BKYEARS                        
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable ispattern smallint_id;
begin
  -- usuwamy bkyears, sprawdzamy, czy nie jest jest wzorcem - jak tak to usuwaj
  -- wszystkie zależne bkyears.
  if (coalesce(old.patternref,0)=0) then
  begin
    select c.pattern
      from companies c
      where c.ref = old.company
      into :ispattern;
    if (coalesce(ispattern,0)=1) then
      delete from bkyears b
        where b.patternref = old.ref;
  end
end^
SET TERM ; ^
