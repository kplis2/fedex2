--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGZAM_BD0 FOR NAGZAM                         
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable ilosc numeric(14,4);
declare variable magazyn char(3);
begin
  /* jeśli kasowane zamowienie bylo realizacja innego zamowienia */
  /*naglowej i pozycje powinny jeszcze istniec */
  if(old.org_ref > 0 AND old.org_ref <> old.ref) then
     execute procedure ZAM_DEL_REAL(old.org_ref, old.ref);
/*  if(old.stan = 'N') then begin
    for select magazyn, ktm, wersja, ilosc  from POZZAM where ZAMOWIENIE=old.ref into :magazyn, :ktm, :wersja, :ilosc do
      execute procedure REZ_DEL_ZAM(:magazyn, :ktm, :wersja, :ilosc, old.ref);
  end*/
  if(old.refzrodl > 0) then   begin
      execute procedure ZAM_DEL_DYSP(old.refzrodl, old.ref);
  end
  /*ponizsze po to, by pozycje sie skasowaly przy istniejacym naglowku, a to w celu istnienia jego zemiennych*/
  update POZZAM set ord = 5 where ZAMOWIENIE=old.ref;
  delete from POZZAM where ZAMOWIENIE=old.ref;
  if(exists(select ref from DOKUMNAG where zamowienie = old.ref and zrodlo=1)) then
    exception NAGZAM_EXPT;
  if(old.kktm is not null and (old.kilosc > 0 or old.kilzreal > 0)
    and old.prschedule is not null and old.planscol is not null
  ) then
    execute procedure plans_knagzam_minus(old.ref);
  --kasowanie przewodników automatycznie wygeneowanych
  if (old.autoscheduled = 1) then
    delete from prschedzam where zamowienie = old.ref;
end^
SET TERM ; ^
