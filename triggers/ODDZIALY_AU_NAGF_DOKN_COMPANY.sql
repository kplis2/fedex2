--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ODDZIALY_AU_NAGF_DOKN_COMPANY FOR ODDZIALY                       
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.company <> old.company) then
    begin
      update nagfak set nagfak.company = new.company where nagfak.oddzial = new.oddzial;
      update dokumnag set dokumnag.company = new.company where dokumnag.oddzial = new.oddzial;
    end
end^
SET TERM ; ^
