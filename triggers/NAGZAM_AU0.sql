--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGZAM_AU0 FOR NAGZAM                         
  ACTIVE AFTER UPDATE POSITION 0 
as
  declare variable pozref integer;
  declare variable bn char(1);
  declare variable cenabrutto decimal(14,4);
  declare variable cenanetto decimal(14,4);
  declare variable pozzamref integer;
  declare variable kompletilpelne smallint;
  declare variable przelicznik numeric(14,4);
  declare variable konfigs varchar(255);
  declare variable konfig numeric(14,4);
  declare variable termdostdecldown timestamp;
  declare variable WORKTIME DOUBLE PRECISION;
  declare variable techjednquantity numeric(14,4);
  declare variable typhar smallint;
  declare variable ilprod numeric(14,4);
  declare variable typzamowienia smallint;
begin
  /* synchronizacja danych o magazynie z pozycjami */
  select kompletilpelne, wydania
    from typzam
    where symbol = new.typzam
    into :kompletilpelne, :typzamowienia;
  if((new.magazyn <> old.magazyn or (old.magazyn is null))and new.magazyn <> '') then begin
    if(:typzamowienia in (0,1)) then
        update pozzam set magazyn = new.magazyn where zamowienie  = new.ref;
    if(new.prdepart is not null) then update prschedguides set warehouse = new.magazyn where zamowienie = new.ref;
  end
  if((new.mag2 <> old.mag2 or (old.mag2 is null))and new.mag2 <> '') then
    update pozzam set mag2 = new.mag2 where zamowienie  = new.ref;
  if(new.rabat <> old.rabat or( new.kurs <> old.kurs) or (new.bn <> old.bn) or (new.bn is null and old.bn is not null) or (new.bn is not null and old.bn is null)) then
    update pozzam set rabat = -101 where zamowienie = new.ref;
  if(new.typ <> old.typ) then
  begin
    if((new.typ > 1 and old.typ < 2) or (new.typ < 2 and old.typ > 2)) then
    begin
      for
        select ref from POZZAM where ZAMOWIENIE = new.ref
        into :pozref
      do begin
        execute procedure POZZAM_OBL(:pozref);
      end
    end
  end
  if(coalesce(new.kilosc,0) <> coalesce(old.kilosc,0) and typzamowienia = 2) then
  begin
    if(kompletilpelne is not null and kompletilpelne = 1 and old.kilosc is not null and old.kilosc <> 0) then
      update POZZAM set ILOSC = cast((ILOSC/old.kilosc * new.kilosc) as numeric(14,4)) where ZAMOWIENIE = new.ref;
    else
      update POZZAM set ILOSC = new.kilosc * KILOSC where ZAMOWIENIE = new.ref;
    if(exists(select ref from prschedguides where zamowienie = new.ref)) then
      execute procedure prschedguidesgroup_amountcalc(new.ref, null, null);
  end
  if(new.kilreal <> old.kilreal) then begin
    if(kompletilpelne is not null and kompletilpelne = 1 and old.kilreal is not null and old.kilreal <> 0)
      then update POZZAM set ILREAL = new.kilREAL * ILREAL / old.kilreal  where ZAMOWIENIE=new.ref;
    else update POZZAM set ILREAL = new.kilREAL * KILOSC where ZAMOWIENIE=new.ref;
  end
  if(new.kcennik <> old.kcennik or (new.kcennik is not null and old.kcennik is null))then begin
    bn = new.bn;
    if (bn is null) then
      execute procedure GETCONFIG('WGCEN') returning_values :bn;
    if((:bn <> 'B') and (:bn <> 'N') ) then bn = 'N';
    for select CENNIK.cenabru, cennik.cenanet, pozzam.ref
      from POZZAM
        join CENNIK on (POZZAM.WERSJAREF = CENNIK.WERSJAREF and CENNIK.cennik = new.kcennik)
      where POZZAM.ZAMOWIENIE = new.ref
      into :cenabrutto, :cenanetto, :pozzamref
    do begin
      if(:bn = 'N') then cenabrutto = :cenanetto;
      update POZZAM set CENACEN = :cenabrutto where REF = :pozzamref;
    end
  end
  if(new.kkomplet = 1 and (new.org_ref <> old.org_ref)) then
    execute procedure NAGZAM_OBL(new.org_ref);
  if((new.kpopref > 0 and (new.kilosc <> old.kilosc))
    or (new.kpopref > 0 and (new.kilzreal <> old.kilzreal))
    or (new.kilosc > 0 and (new.kpopref <> old.kpopref) and (new.kisreal <> old.kisreal)))
  then begin
    if(new.kpopref <> old.kpopref) then
      execute procedure POZZAM_OBL(old.kpopref);
    execute procedure POZZAM_OBL(new.kpopref);
  end
  if(new.faktura <> old.faktura or (new.faktura is null and old.faktura is not null))then
    execute procedure NAGFAK_SYMBOLZAM(old.faktura);
  if(new.faktura <> old.faktura or (new.faktura is not null and old.faktura is null))then
    execute procedure NAGFAK_SYMBOLZAM(new.faktura);
  /* zanzaczenie zamówienia jako realizacja - naliczenie realizacji na oryginalnym zamówieniu */
/*  if(new.org_ref <> old.org_ref and new.org_ref > 0) then
    execute procedure ZAM_OBL_REAL(new.org_ref, new.ref);*/
  /* wycofanie realizacji zamówienia - realizacja staje się zamowieniem wolny - dzialanie hipotetyczne */
/*  if(new.org_ref <> old.org_ref and new.org_ref = 0 and old.org_ref>0) then
    execute procedure ZAM_DEL_REAL(old.org_ref, new.ref);*/
  /* jeśli dane zamowienie przestalo byc swoja realizacja - wycofanie ilosci zrealizowanych */
/*  if((new.org_ref = 0 or (new.org_ref is null)) and old.org_ref > 0 and old.org_ref = old.ref) then
    update POZZAM set ILZREAL = ILZREAL - ILREAL where ZAMOWIENIE=old.ref;*/
  if( new.prschedule is not null and new.planscol is not null and
    ((new.kktm <> old.kktm ) or (new.kktm is not null and old.kktm is null) or (new.kktm is null and old.kktm is not null)
    or (new.kilosc <> old.kilosc) or (new.kilzreal <> old.kilzreal))) then
      execute procedure plans_knagzam_plus(new.ref);
-- odswierzenie kaskadowe deklarowanych terminów realizacji zleceń produkcyjnych
  if ((((new.termdostdecl is null and old.termdostdecl is not null)
      or (new.termdostdecl is not null and (old.termdostdecl is null or
      new.termdostdecl <> old.termdostdecl))) and new.prdepart is not null)
  ) then begin
    select typhar from prdeparts where symbol = new.prdepart into :typhar;
    if(typhar = 0) then select techjednquantity from prsheets where ref = new.prsheet into :techjednquantity;
    else techjednquantity = new.techjednquantity;
    worktime = null;
    if(:techjednquantity is not null and :techjednquantity > 0) then begin
      execute procedure getconfig('TECHJEDNQUANTITY') returning_values :konfigs;
      konfig = cast(konfigs as numeric(14,2));
      if(konfig = 0) then konfig = 1;
      worktime =  :techjednquantity / konfig;
    end
    if(worktime is null) then worktime = 0;
    execute procedure pr_calendar_checkstarttime(new.prdepart, :termdostdecldown, :worktime)
    returning_values :termdostdecldown;
    update nagzam n set termdostdecl = new.termdostdecl - :worktime where n.kpopprzam = new.ref;
  end
-- propagacja deklarowanego terminu realizacji zamówienia na zlecenia produkcyjneg
  if(new.kpopzam is null and new.termdost is not null and (old.termdost is null
    or new.termdost <> old.termdost)) then begin
     update nagzam set termdostdecl = new.termdost where kpopzam = new.ref and kpopprzam is null;
  end
  --przepisanie priorytetów na zlecenia i przewodniki
  if (coalesce(old.prpriority,0) <> coalesce(new.prpriority,0)) then
  begin
    update prschedzam p set p.prpriority = new.prpriority where p.zamowienie = new.ref;
    update prschedguides g set g.prpriority = new.prpriority where g.zamowienie = new.ref;
  end
  -- powiazanie z produkcja - anulowanie zlecenia produkcyjnego
  if (typzamowienia = 3) then
  begin
    if (new.anulowano = 1 and old.anulowano = 0) then
    begin
      -- anulujemy pozycje zamówienia przychodowe, bo tam jest obsluzona reszta
      update pozzam set anulowano = 1 where zamowienie = new.ref and out = 0 and anulowano = 0;
    end
  end
  /* uzupelnainie tabeli splat po zmianie klienta/dostawcy */
  if(new.klient<>old.klient or new.dostawca<>old.dostawca) then begin
    delete from NAGFAKTERMPLAT where doktyp = 'Z' and dokref = new.ref;
    if (new.dostawca is not null) then
      INSERT INTO NAGFAKTERMPLAT (DOKTYP, DOKREF, TERMZAP, PROCENTPLAT, SPOSPLAT)
        select 'Z', new.ref, new.datawe +k.dniplat, k.procentplat, k.sposplat from KONTRAHTERMPLAT k where k.slodef = 6 and k.slopoz = new.dostawca;
    else if(new.klient is not null) then
      INSERT INTO NAGFAKTERMPLAT (DOKTYP, DOKREF, TERMZAP, PROCENTPLAT, SPOSPLAT)
        select 'Z', new.ref, new.datawe +k.dniplat, k.procentplat, k.sposplat from KONTRAHTERMPLAT k where k.slodef = 1 and k.slopoz = new.klient;
  end
end^
SET TERM ; ^
