--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWS_DOKUMNAG_AU0 FOR DOKUMNAG                       
  ACTIVE AFTER UPDATE POSITION 20 
AS
declare variable procsql varchar(255);
declare variable procsqls varchar(255);
declare variable refmwsord integer;
declare variable mwsord integer;
declare variable docblockreal smallint;
declare variable delafterackchange smallint;
declare variable mwsordstatus integer;
declare variable cnt integer;
declare variable wydania smallint;
declare variable docposid integer;
declare variable refmwsordnew integer;
declare variable mwsordswaiting smallint;
declare variable grupasped integer;
declare variable frompozzam integer;
begin
--  if (new.mwsdoc is null or new.mwsdoc = 0) then
  --  exit;
  select d.afterackproc, f.wydania, d.mwsordwaiting
    from defdokummag d
      join defdokum f on (f.symbol = d.typ)
    where d.magazyn = new.magazyn and d.typ = new.typ
    into procsql, wydania, mwsordswaiting;
  if (mwsordswaiting is null) then mwsordswaiting = 0;
    if (new.mwsdisposition = 1 and (:procsql is null or :procsql ='' )) then exception MWSORDS_NO_CALCPROC;
  if (mwsordswaiting = 0) then
  begin
    -- generowanie zleceń magazynowych
    if (((new.akcept = 1 and old.akcept <> 1 and old.mwsdisposition = 0)
          or (new.mwsdisposition = 1 and old.mwsdisposition <> 1 and new.akcept = 0))
        and procsql is not null and procsql <> '' and procsql <> ' '
        and (new.mwsblockmwsords is null or new.mwsblockmwsords = 0)
    ) then
    begin
      -- wywolanie procedury na akceptacje dokumentu magazynowego
      procsql = 'select refmwsord from '||procsql||'('||new.ref||','||new.grupasped||',null,null,null,null,1,null)';
      execute statement procsql into refmwsord;
    end
  end
if (new.ref not in (186, 188)) then begin
  -- sprzatanie zlecen magazynowych przy wydaniu
  if (new.typ <> 'BO' and ((new.akcept <> 1 and old.akcept = 1 and new.wydania = 1)
      or (new.grupasped <> old.grupasped and new.grupasped = new.ref and new.grupasped is not null))
  ) then
  begin
    for
      select distinct o.ref, o.status, mt.docblockreal, mt.delaftackchange
        from mwsacts a
          join mwsords o on (a.mwsord = o.ref)
          left join mwsordtypes mt on (mt.ref = o.mwsordtype)
        where a.doctype = 'M'
          and (o.docgroup = old.grupasped or a.docid = new.ref) --- xxx KBI ZG124059 bo multipiking
        order by o.ref desc
        into mwsord, mwsordstatus, delafterackchange, delafterackchange
    do begin
      if (mwsordstatus > 1 and mwsordstatus < 7
         or exists(select ref from mwsacts where status > 1 and status < 6 and mwsord = :mwsord)
      ) then
        exception MWSORDS_IN_REAL 'Zlecenie jest w realizacji';
      else
      begin
        if (mwsord is null) then exception MWSORDS_IN_REAL 'MWS_DOKUMNAG_AU0: Brak zlecenia magazynowego';
        if (procsql is null) then exception MWSORDS_IN_REAL 'MWS_DOKUMNAG_AU0: brak procedury generującej zlecenia';
        -- sprzatanie operacji zlecenia i zlecenia magazynowego
        -- najpierw dokonujemy rekalkulacji calego zlecenia - bo moze sa tam pozycje z innego dokumentu
        if (old.grupasped is null) then
          grupasped = 0;
        else
          grupasped = old.grupasped;

        procsqls = 'select refmwsord from '||procsql||'('||cast(new.ref as string)||','||cast(coalesce( grupasped,'') as string)||',null,null,'||mwsord||',null,1,null)';
        if (procsql <> '') then
          execute statement procsqls into refmwsordnew;
        -- jak nie istnieja juz pozycje to mozna usunac zlecenie
        -- zaczynamy od pozycji
        -- exception test_break new.ref;

        for
          select ref
            from dokumpoz
            where dokument = new.ref
            into docposid
        do begin
          update mwsacts set status = 0 where docposid = :docposid and mwsord = :mwsord and doctype = 'M';
          delete from mwsacts where docposid = :docposid and mwsord = :mwsord and doctype = 'M';
        end
        select count(ref) from mwsacts where mwsord = :mwsord into cnt;
        if (cnt = 0 and new.mwsmanacts = 0) then
        begin
          update mwsords set status = 0 where ref = :mwsord and status <> 0;
          delete from mwsords where ref = :mwsord;
        end
        select count(ref) from mwsacts where mwsord = :refmwsordnew into cnt;
        if (cnt = 0 and new.mwsmanacts = 0) then
        begin
          update mwsords set status = 0 where ref = :refmwsordnew and status <> 0;
          delete from mwsords where ref = :refmwsordnew;
        end
      end
    end
  end
  end
  -- sprzatanie zlecen magazynowych przy przyjeciu
  if (new.akcept <> 1 and old.akcept = 1 and new.wydania = 0 and coalesce(new.mwsblockmwsords,0) = 0) then
  begin
    if (exists(select first 1 1 from mwsords o where o.docid = new.ref and o.status = 5)) then
      exception universal 'Zlecenie rozładunku zostało już zamknięte.';
    for
      select distinct o.ref, o.status, mt.docblockreal, mt.delaftackchange
        from mwsacts a
          join mwsords o on (a.mwsord = o.ref)
          left join mwsordtypes mt on (mt.ref = o.mwsordtype)
        where a.doctype = 'M' and a.docid = new.ref
        order by o.ref desc
        into mwsord, mwsordstatus, delafterackchange, delafterackchange
    do begin
      if (new.akcept = 9) then
      begin
        for
          select ref
            from dokumpoz
            where dokument = new.ref
            into docposid
        do begin
          delete from mwsacts where docposid = :docposid and mwsord = :mwsord and doctype = 'M' and status = 0;
        end
      end
      if (new.akcept = 0) then
      begin
        if (mwsordstatus = 5 and exists(select ref from mwsacts where status > 1 and status < 6 and mwsord = :mwsord)
        ) then
          exception MWSORDS_IN_REAL 'Zlecenie jest zrealizowane';
        else if (mwsordstatus > 1 and mwsordstatus < 5 or exists(select ref from mwsacts where status > 1 and status < 6 and mwsord = :mwsord)
        ) then
          exception MWSORDS_IN_REAL 'Zlecenie jest w realizacji';
        for
          select ref
            from dokumpoz
            where dokument = new.ref
            into docposid
        do begin
          update mwsacts set status = 0 where docposid = :docposid and mwsord = :mwsord and doctype = 'M';
          delete from mwsacts where docposid = :docposid and mwsord = :mwsord and doctype = 'M';
        end
        select count(ref) from mwsacts where mwsord = :mwsord into cnt;
        if (cnt = 0 and new.mwsmanacts = 0) then
        begin
          update mwsords set status = 0 where ref = :mwsord and status <> 0;
          delete from mwsords where ref = :mwsord;
        end
        select count(ref) from mwsacts where mwsord = :refmwsordnew into cnt;
        if (cnt = 0 and new.mwsmanacts = 0) then
        begin
          update mwsords set status = 0 where ref = :refmwsordnew and status <> 0;
          delete from mwsords where ref = :refmwsordnew;
        end
      end
    end
  end
    /* Uzgodnienie ilosci na zamówieniu */
  if(new.mwsdisposition <> old.mwsdisposition) then
  begin
    for
      select frompozzam
        from dokumpoz
        where dokument = new.ref and frompozzam is not null
      into :frompozzam
    do begin
      execute procedure pozzam_obl(:frompozzam);
    end
  end
end^
SET TERM ; ^
