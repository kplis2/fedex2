--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER UZYKLI_AD_SYNCHRO FOR UZYKLI                         
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable refpkos integer ;
declare variable synchro smallint;
begin
  execute procedure getconfig('SYNCHROUZYKLI') returning_values :synchro;
  if (synchro = 1) then
  begin
    refpkos = null;
    select pkosoby.ref from pkosoby where pkosoby.ref = old.pkosoba
    into :refpkos;
    if (refpkos is not null ) then
    delete from pkosoby where pkosoby.ref = :refpkos;
  end
end^
SET TERM ; ^
