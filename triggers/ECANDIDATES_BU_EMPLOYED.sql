--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECANDIDATES_BU_EMPLOYED FOR ECANDIDATES                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.employee is null and old.employee is not null) then
    new.employed = 0;
end^
SET TERM ; ^
