--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_BI_REALTIME FOR MWSACTS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  -- deklarowany czas wykonania zlecenia
  if (new.timestartdecl is not null and new.timestopdecl is not null) then
    new.realtimedecl = new.timestopdecl - new.timestartdecl;
  -- rzeczywisty czas wykonania zlecenia
  if (new.status = 2 and new.timestart is null) then
    new.timestart = current_timestamp(0);
  if (new.status = 5 and new.timestart is null) then
    new.timestart = current_timestamp(0);
  if (new.status = 5 and new.timestop is null) then
  begin
    new.timestop = current_timestamp(0);
    new.datestop = current_date;
  end
  if (new.timestart is not null and new.timestop is not null) then
    new.realtime = new.timestop - new.timestart;
  if (new.status = 6) then
  begin
    new.timestart = current_timestamp(0);
    new.timestop = current_timestamp(0);
  end
end^
SET TERM ; ^
