--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ATRYBUTY_AD_TOWARY FOR ATRYBUTY                       
  ACTIVE AFTER DELETE POSITION 2 
as
declare variable atrakr varchar(20);
declare variable sql varchar(255);
begin
  if (old.nrwersji = 0) then
  begin
    select DEFCECHY.redundantakr from DEFCECHY
      where defcechy.symbol=old.cecha
      into :atrakr;

    if (atrakr<>'') then
    begin
      sql = 'update TOWARY set '||atrakr||'='''' where KTM='''||old.ktm||'''';
    end

  end
end^
SET TERM ; ^
