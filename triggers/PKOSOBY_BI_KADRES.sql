--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PKOSOBY_BI_KADRES FOR PKOSOBY                        
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable nastim timestamp;
begin
 if(new.korespond = 0) then begin
   /* adres prywatny lub siedziby firmy*/
   new.kulica = new.pulica;
   new.knrdomu = new.pnrdomu;
   new.knrlokalu = new.pnrlokalu;
   new.kmiasto = new.pmiasto;
   new.kkodp = new.pkodp;
   new.kpoczta = new.ppoczta;
   new.kkraj = new.pkraj;
 end else if(new.korespond = 2) then begin
   new.kulica = new.ulica;
   new.knrdomu = new.nrdomu;
   new.knrlokalu = new.nrlokalu;
   new.kmiasto = new.miasto;
   new.kkodp = new.kodp;
   new.kpoczta = new.poczta;
   new.kkraj = new.kraj;
 end
 if(new.imie is null) then new.imie = '';
 if(new.nazwisko is null) then new.nazwisko = '';
 if(new.nazwafirmy is null) then new.nazwafirmy = '';
 if(new.imie<>'' or new.nazwisko<>'') then
   new.nazwa = new.imie || ' ' || new.nazwisko;
 else
   new.nazwa = new.nazwafirmy;
 execute procedure PKOSOBY_IMIENINY(new.dzienim,new.miesim,new.przypomn) returning_values new.dataprzypim;
end^
SET TERM ; ^
