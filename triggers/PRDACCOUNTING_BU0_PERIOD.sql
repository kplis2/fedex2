--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRDACCOUNTING_BU0_PERIOD FOR PRDACCOUNTING                  
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable okres VARCHAR(6);
declare variable datazm TIMESTAMP;
declare variable fday TIMESTAMP;
declare variable lday TIMESTAMP;
begin
  if ((coalesce(new.fromsdate,0) <> coalesce(old.fromsdate,0)) or (new.sdate <> old.sdate))  then begin
    if (new.fromsdate = 1 and (extract(day from new.sdate) <> 1)) then begin
      execute procedure datatookres(new.sdate,0,0,0,1)
        returning_values :okres,:datazm,:fday,:lday;
      new.firstperiod = :okres;
    end else begin
      new.fromsdate = 0;
      new.sdate = null;
    end
  end
end^
SET TERM ; ^
