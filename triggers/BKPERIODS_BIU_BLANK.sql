--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKPERIODS_BIU_BLANK FOR BKPERIODS                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.sidblocked is null) then new.sidblocked = 0;
  if (new.vatblocked is null) then new.vatblocked = 0;
end^
SET TERM ; ^
