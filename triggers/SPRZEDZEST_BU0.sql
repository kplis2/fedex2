--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDZEST_BU0 FOR SPRZEDZEST                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable okres varchar(6);
begin
  if(new.status is null) then new.status = 'O';
  if(new.dowyplaty is null) then new.dowyplaty = 0;
  if(new.wyplacono is null) then new.wyplacono = 0;
  if(new.prowizjastala is null) then new.prowizjastala = 0;
  if((new.prowizjastala <> old.prowizjastala) or (new.sumprowizji <> old.sumprowizji)) then
    new.dowyplaty = new.sumprowizji + new.prowizjastala;
  if(((old.status = 'Z') or (old.status = 'W')) and new.status <>'O') then begin
    if((new.wyplacono <> new.dowyplaty) and (new.status <> 'Z')) then new.status = 'Z';
    if((new.wyplacono = new.dowyplaty) and (new.status <> 'W')) then new.status = 'W';
  end
  if(new.status = 'Z' and old.status ='O') then
   new.datazam = current_date;
  if(new.data is null) then new.data = current_date;
  if(new.data <> old.data) then begin
    okres = cast( extract( year from new.data) as char(4));
    if(extract(month from new.data) < 10) then
      okres = :okres ||'0' ||cast(extract(month from new.data) as char(1));
    else
      okres = :okres ||cast(extract(month from new.data) as char(2));
    if(new.okres = old.okres) then
      new.okres = :okres;
    else begin
      if(new.okres <> :okres) then exception SPRZEDZEST_DATA_NIE_W_OKRESIE;
    end
  end
end^
SET TERM ; ^
