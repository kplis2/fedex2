--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ERECATTR_AIU0 FOR ERECATTR                       
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
AS
declare RecCand_ref int;
begin
  -- przeliczam wszystkie oceny !
  for
    select REF from ereccands
      where RECPOST=new.RECPOST
      into :RecCand_ref
  do
    execute procedure eRecCandCompRating(RecCand_ref);
end^
SET TERM ; ^
