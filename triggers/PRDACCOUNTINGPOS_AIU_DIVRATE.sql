--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRDACCOUNTINGPOS_AIU_DIVRATE FOR PRDACCOUNTINGPOS               
  ACTIVE AFTER INSERT OR UPDATE POSITION 1 
as
declare variable sumdivrate numeric(14,6);
declare variable sumdedeductamount numeric(14,6);
declare variable sumfirstdeductamount numeric(14,6);
declare variable deductamount numeric(14,6);
declare variable firstdeductamount numeric(14,6);
begin
  select sum(coalesce(divrate,0))
  from prdaccountingpos where prdversion = new.prdversion
  into :sumdivrate;
  if (:sumdivrate > 100) then
    exception universal 'Suma procentów dla danego rozliczenia przekracza 100%';
end^
SET TERM ; ^
