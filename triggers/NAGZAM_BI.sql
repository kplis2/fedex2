--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGZAM_BI FOR NAGZAM                         
  ACTIVE BEFORE INSERT POSITION 1 
AS
declare variable NUMBERG varchar(40);
declare variable KOSZTDOST DECIMAL(14,2);
declare variable okres varchar(8);
declare variable walpln varchar(3);
declare variable popzam integer;
declare variable local smallint;
declare variable autokilosc numeric(14,4);
declare variable klient integer;
declare variable prog numeric(14,2);
declare variable konfig varchar(255);
declare variable cnt integer;
declare variable dataokres timestamp;
declare variable lokres varchar(6);
declare variable moddata smallint;
declare variable slodeftyp varchar(20);
declare variable dnirealizacji integer;
declare variable sposobzaplaty integer;
declare variable czasdostawy integer;
declare variable dnizaplaty integer;
BEGIN
  execute procedure CHECK_LOCAL('NAGZAM',new.ref) returning_values :local;
  if(new.bn is null or new.bn='' or new.bn=' ') then
    execute procedure GETCONFIG('WGCEN') returning_values new.bn;
  if(new.status is null) then new.status = 0;
  if(new.operator = 0) then new.operator = NULL;
  if(new.sprzedawca = 0) then new.sprzedawca = NULL;
  if(new.TABKURS = 0) then new.TABKURS = null;
  if(new.sprzedrozl is null) then new.sprzedrozl = 0;
  if(new.dokmagakc is null) then new.dokmagakc = 0;
  if(new.replicat is NULL) then new.replicat = 0;
  if(new.rejestr is not null) then
    select STANMAG from DEFREJZAM where SYMBOL = new.rejestr into new.stan;
  if(new.stan is null or (new.stan = '') or (new.stan = ' ')) then new.stan = 'N';
  if(new.kstan is null or (new.kstan = '') or (new.kstan = ' ')) then new.kstan = 'N';
  if(new.kilosc is null) then new.kilosc = 0;
  if(new.kinne is null) then new.kinne = 0;
  if(new.kilreal is null) then new.kilreal = 0;
  if(new.kilzreal is null) then new.kilzreal = 0;
  if(new.onmainschedule is null) then new.onmainschedule = 0;
  if(new.termdost = '1899-12-30') then new.termdost = null;
  if(new.sumazaliczekzl is null) then new.sumazaliczekzl = 0;
  if(new.kwotafakzalnieroz is null) then new.kwotafakzalnieroz = 0;
  if(new.autoscheduled is null) then new.autoscheduled = 0;
  if(new.prpreprod is null) then new.prpreprod = 0;
  if(new.anulowano is null) then new.anulowano = 0;
  select defrejzam.moddata from defrejzam where defrejzam.symbol=new.rejestr into :moddata;
  if(:moddata=1) then begin
    dataokres=new.datawe;
    lokres = cast( extract( year from :dataokres) as char(4));
    if(extract(month from :dataokres) < 10) then
      lokres = :lokres ||'0' ||cast(extract(month from :dataokres) as char(1));
    else
      lokres = :lokres ||cast(extract(month from :dataokres) as char(2));
    new.okres=:lokres;
  end
  if(new.waluta is null or (new.waluta = '')) then begin
    execute procedure GETCONFIG('WALPLN') returning_values new.waluta;
    if(new.waluta is null) then new.waluta = 'PLN';
  end
  select WYDANIA, outonpozzamenabled, eksport
     from TYPZAM where SYMBOL = new.typZAM
     into new.kkomplet, new.outonpozzamenabled, new.eksport;
  if (new.outonpozzamenabled is null) then new.outonpozzamenabled = 0;
  if(new.kkomplet <> 3) then new.prdepart = null;
  if(new.kkomplet = 2 or (new.kkomplet = 3) ) then new.kkomplet = 1;
  else new.kkomplet = 0;
  if(new.kkomplet = 1 and new.typ > 1) then begin
    new.kilosc = new.kilreal;
    new.kilzreal = new.kilreal;
  end
  execute procedure GETCONFIG('WALPLN') returning_values :walpln;
  if(:walpln is null or (new.waluta = '')) then walpln = 'PLN';
  if(new.waluta <> :walpln ) then new.walutowe = 1;
  else new.walutowe = 0;
  if(new.hasdokmagbezfak is null) then new.hasdokmagbezfak = 0;
  if( (new.id is null or new.id = '') and
      (new.numer is null or (new.numer = 0)) /*and
      (new.org_ref is null or (new.org_ref = 0))*/
  ) then begin
     /*nadanie numeru zamówienia*/
     select number_gen from DEFREJZAM where new.rejestr = defrejzam.symbol into :numberg;
     if(new.datawe is null) then new.datawe = current_date;

     new.datazm = new.datawe;
     if(new.prdepart is not null) then
       execute procedure get_number(:numberg,new.prdepart,new.typzam,new.datawe, 0, new.ref) returning_values new.numer, new.id;
     else
       execute procedure get_number(:numberg,new.rejestr,new.typzam,new.datawe, 0, new.ref) returning_values new.numer, new.id;
     /* nadanie symbolu okresu */
     okres = cast( extract( year from new.datawe) as char(4));
     if(extract(month from new.datawe)<10)then okres = okres ||'0';
     okres = okres ||cast(extract(month from new.datawe) as char(2));
     new.okres = okres;
  end
  if(new.datazm is null) then new.datazm = current_date;
  if(new.typ is null) then new.typ = 0;
/*  if((new.odbiorca is null or (new.odbiorca = '')) and new.KLIENT is not null)then begin
    select NAZWA from KLIENCI where REF=new.KLIENT into new.odbiorca;
  end*/
  if((new.oddzial is null) or (new.oddzial = '')) then  begin
    execute procedure GETCONFIG('AKTUODDZIAL') returning_values new.oddzial;
  end
  if(new.kosztdost is null) then new.kosztdost = 0;
  if(new.kosztdost is null) then kosztdost = 0;
  else kosztdost = new.kosztdost;
  if(new.rabat is not null) then
       kosztdost = kosztdost - cast((kosztdost * new.rabat/100) as DECIMAL(14,2));
  new.dozaplaty = kosztdost + new.sumwartbru;
  if(new.sumwartnet is null) then new.sumwartnet = 0;
  if(new.sumwartbru is null) then new.sumwartbru = 0;
  if(new.sumwartrnet is null) then new.sumwartrnet = 0;
  if(new.sumwartrbru is null) then new.sumwartrbru = 0;
  if((new.kurs is null) or (new.kurs = 0)) then new.kurs = 1;
  if(new.kosztdost is null) then new.kosztdost = 0;
  if(new.dozaplaty is null) then new.dozaplaty = 0;
  if(coalesce(new.platnik,0)=0)then
    select platnik from platnicy where klient = new.klient and domyslny = 1 into new.platnik;
  if(coalesce(new.platnik,0)=0)then new.platnik = new.klient;
  if(new.sposdost is null and new.klient is not null) then begin
    select sposdost from klienci where ref=new.klient into new.sposdost;
  end
  if(new.sposdost is null and new.dostawca is not null) then begin
    select sposdost from DOSTAWCY where ref=new.dostawca into new.sposdost;
  end
  if(new.sposdost is null) then
  begin
    execute procedure GETCONFIG('SPOSDOST')
      returning_values :konfig;
    if (konfig is not null and konfig<>'') then
      new.sposdost = cast(konfig as integer);
  end

  /* obliczenie kosztu dostawy */
  prog = null;
  select max(prog)
    from sdostkoszt
    where platnosc=new.sposzap and sposdost=new.sposdost and prog<=new.sumwartbru
    into :prog;
  if (prog is not null) then
  begin
    select koszt
      from sdostkoszt
      where platnosc=new.sposzap and sposdost=new.sposdost and prog=:prog
      into new.kosztdost;
  end else
  begin
    select prog, koszt from sposdost where ref = new.sposdost
      into :prog, new.kosztdost;
    if (new.sumwartbru>=prog and prog<>0) then
      new.kosztdost = 0;
  end

  /* obliczanie terminu dostawy */
  if (new.termdost is null and new.dostawca is not null) then
    begin
      select coalesce(dostawcy.dnireal,0)
        from dostawcy
        where dostawcy.ref = new.dostawca
        into :dnirealizacji;
      if (dnirealizacji > 0) then
        new.termdost = :dnirealizacji + new.datawe;
    end
  else if (new.termdost is null and new.sposdost is not null) then
    begin
      select coalesce(sposdost.czas,0)
        from sposdost
        where sposdost.ref = new.sposdost
        into :czasdostawy;
      new.termdost = :czasdostawy + new.datawe;
    end

  /* obliczenie sposobu platnosci */
  if (new.sposzap is null and new.dostawca is not null) then
    begin
      select dostawcy.sposplat
        from dostawcy
        where dostawcy.ref = new.dostawca
        into :sposobzaplaty;
      if (sposobzaplaty is not null) then
        new.sposzap = :sposobzaplaty;
    end
  if (new.sposzap is null) then
    begin
      execute procedure GETCONFIG('SPOSZAP')
        returning_values  :konfig;
      if (konfig is not null and konfig<>'') then
        new.sposzap = cast(konfig as integer);
    end

  /* obliczenie terminu platnosci */
  if(new.termzap is null and new.sposzap is not null) then
    begin
      select platnosci.termin
        from platnosci
        where platnosci.ref = new.sposzap
        into :dnizaplaty;
      new.termzap = :dnizaplaty;
    end

  if(new.org_ref > 0 and :local = 1) then
    /*nastapila realizacja zamowienia*/
     new.datareal = current_date;
  if(new.kpopref > 0 and :local = 1) then begin
    if(new.kinne = 0) then
      select ZAMOWIENIE, KTM, WERSJA, WERSJAREF, ILOSC - ILZREAL-ILZDYSP
      from POZZAM where REF=new.kpopref into
       :popzam, new.kktm, new.kwersja, new.kwersjaref, :autokilosc;
    else begin
      select ZAMOWIENIE
      from POZZAM where REF=new.kpopref into
       :popzam;
    end
    if(new.kilosc = 0 and :autokilosc > 0) then
      new.kilosc = :autokilosc;
    select GRUPYKLI.CENNIK,NAGZAM.REF,NAGZAM.ID, NAGZAM.TERMDOST, nagzam.termdostdecl, NAGZAM.KLIENT
      from NAGZAM
      left join KLIENCI on (KLIENCI.REF = NAGZAM.KLIENT)
      left join GRUPYKLI on (KLIENCI.GRUPA = GRUPYKLI.ref)
      where NAGZAM.REF =:popzam
      into new.kcennik,new.refzrodl, new.symbzrodl, new.termdost, new.termstartdecl, :klient;
    if(new.klient is null) then new.klient = :klient;
    if(new.magazyn is null or (new.magazyn = '')) then begin
      select MAGAZYN from NAGZAM where ref=:popzam into new.magazyn;
    end
  end
--okreslenie magazynu na zleceniu produkcyjnym jesli nie ustawiony
  if(new.magazyn is null and new.prsheet is not null) then
    select warehouse from prsheets where ref = new.prsheet into new.magazyn;
  if(new.kwersjaref is not null) then
    select WERSJE.nrwersji, WERSJE.ktm from WERSJE where ref = new.kwersjaref into new.kwersja, new.kktm;
  else if(new.kktm is not null) then
    select WERSJE.nrwersji, WERSJE.ref from WERSJE where KTM = new.kktm and nrwersji = coalesce(new.kwersja,0) into new.kwersja, new.kwersjaref;
  if(new.termdostdecl is not null) then
    new.termdost = new.termdostdecl;
  if(new.magazyn is not null) then
    select ODDZIAL from DEFMAGAZ where SYMBOL = new.magazyn into new.oddzial;
  if(new.mag2 is not null) then
    select oddzial from DEFMAGAZ where SYMBOL = new.mag2 into new.oddzial2;
  if(new.sprzedawca is not null) then
    select ODDZIAL from SPRZEDAWCY where ref = new.sprzedawca into new.oddzialspr;
  if(new.dostawca is not null and new.fakturant is null) then begin
    select FAKTURANT from DOSTAWCY where ref=new.dostawca into new.fakturant;
    if(new.fakturant is null) then
      new.fakturant = new.dostawca;
  end
  if(new.sposdostauto is null) then begin
    if(new.sposdost > 0) then select AUTOZMIANA from SPOSDOST where ref=new.sposdost into new.sposdostauto;
  end
  if(new.sposdostauto is null) then new.sposdostauto = 0;
  new.wysylkadod = 0;
  if(new.reddokumref > 0 and new.reddokumtyp<>'')then begin
    cnt = null;
    select count(*) from NAGZAM where REDDOKUMTYP = new.reddokumtyp and REDDOKUMREF = new.reddokumref into :cnt;
    if(:cnt is null) then cnt = 0;
    if(:cnt > 0) then
      new.reddokumgl = 0;
    else
      new.reddokumgl = 1;
  end
  if(new.klient > 0) then begin
    if(new.slodef is NULL or (new.slodef=0)) then
      select REF from SLODEF where TYP='KLIENCI' into new.slodef;
    select TYP from SLODEF where REF=new.slodef into :slodeftyp;
    if(:slodeftyp='KLIENCI') then new.slopoz = new.klient;
    if (new.editype is null or new.editype = '') then
      select first 1 k.editype
        from klienciedi k
        join urzsprtyp u on u.symbol=k.editype
        where k.klient = new.klient and new.datawe >= k.fromdate and new.datawe <= k.todate and
          u.expzam=1 into new.editype;
  end else if(new.dostawca > 0) then begin
    if(new.slodef is NULL or (new.slodef=0)) then select REF from SLODEF where  TYP='DOSTAWCY' into new.slodef;
    select TYP from SLODEF where REF=new.slodef into :slodeftyp;
    if(:slodeftyp='DOSTAWCY') then new.slopoz = new.dostawca;
  end
END^
SET TERM ; ^
