--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECOSTSPOS_BI_REF FOR ECOSTSPOS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ECOSTSPOS')
      returning_values new.REF;
end^
SET TERM ; ^
