--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAMUPVIEW_BU FOR POZZAMUPVIEW                   
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (new.ilosc <> old.ilosc) then
    begin
      update pozzamup set
        pozzamup.ilosc = new.ilosc
      where pozzamup.nagzamup = new.ref
        and pozzamup.refpoz = new.refpoz;
    end
end^
SET TERM ; ^
