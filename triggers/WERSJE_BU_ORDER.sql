--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WERSJE_BU_ORDER FOR WERSJE                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 1;
  if (new.nrwersji is null) then new.nrwersji = old.nrwersji;
  if (new.ord = 0 or new.nrwersji = old.nrwersji) then
  begin
    new.ord = 1;
    exit;
  end
  --nrwersjiowanie obszarow w rzedzie gdy nie podamy nrwersjiu
  if (new.nrwersji <> old.nrwersji) then
    select max(nrwersji) from wersje where ktm = new.ktm
      into :maxnum;
  else
    maxnum = 0;
  /*if (new.nrwersji < old.nrwersji) then
  begin
    update wersje set ord = 0, nrwersji = nrwersji + 1
      where ref <> new.ref and nrwersji >= new.nrwersji
        and nrwersji < old.nrwersji and ktm = new.ktm;
  end else if (new.nrwersji > old.nrwersji and new.nrwersji <= maxnum) then
  begin
    update wersje set ord = 0, nrwersji = nrwersji - 1
      where ref <> new.ref and nrwersji <= new.nrwersji
        and nrwersji > old.nrwersji and ktm = new.ktm;
  end
  else */
  if (new.nrwersji > old.nrwersji and new.nrwersji > maxnum) then
    new.nrwersji = old.nrwersji;
end^
SET TERM ; ^
