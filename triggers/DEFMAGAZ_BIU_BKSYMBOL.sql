--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFMAGAZ_BIU_BKSYMBOL FOR DEFMAGAZ                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
  declare variable kodln smallint;
  declare variable analityka smallint;
begin
  if (new.bksymbol is null) then new.bksymbol = new.symbol;
  select kodln, analityka from slodef where typ = 'DEFMAGAZ' into :kodln, analityka;
  if (analityka = 1 and kodln is not null) then
    if (coalesce(char_length(new.bksymbol),0) <> 0 and coalesce(char_length(new.bksymbol),0) <> kodln) then  -- [DG] XXX ZG119346
      exception BAD_LENGTH_FKKOD;
end^
SET TERM ; ^
