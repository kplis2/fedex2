--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDES_BD FOR PRSCHEDGUIDES                  
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable prschedguidelast integer;
begin
  if (exists (select first 1 1 from prschedguidedets d where d.prschedguide = old.ref)) then
    exception prschedguides_error 'Przewodnik '||old.symbol||' posiada rozpiski. Usunięcie zlecenia niemożliwe';
  update prschedguidespos set pggamountcalc = 1 where prschedguide = old.ref;
  delete from prschedguidespos where prschedguide = old.ref;
  if(old.status > 0) then
    exception prschedguides_error 'Przewodnik '||old.symbol||' został pobrany. Usunięcie zlecenia niemożliwe';
  if(old.lastone = 1) then begin
    select first 1 ref from prschedguides where prschedzam = old.prschedzam and ref <> old.ref and status = 0
      order by starttime desc nulls first, numer desc into :prschedguidelast;
    if(prschedguidelast is null) then select max(ref) from prschedguides
      where prschedzam = old.prschedzam and ref <> old.ref into :prschedguidelast;
    if(prschedguidelast is not null) then update prschedguides set lastone = 1 where ref = :prschedguidelast;
  end
end^
SET TERM ; ^
