--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAK_AD0 FOR POZFAK                         
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable dokumkoryg integer;
begin
  delete from DOKUMSER where REFPOZ = old.ref and TYP = 'F';
  --SN
  if(old.refk is not null and coalesce(old.recurdeleted,0) = 0) then begin
    select dokument from pozfak where ref= old.refk into :dokumkoryg;
    update pozfak set recurdeleted = 1 where refk in (select ref from pozfak where dokument = :dokumkoryg) and dokument = old.dokument;
    delete from pozfak where refk in (select ref from pozfak where dokument = :dokumkoryg) and dokument = old.dokument;
    update nagfak set korekta = null where ref = (select dokument from pozfak where ref= old.refk);
    execute procedure nagfak_obl_psum(old.dokument);
  end
  execute procedure FAK_POZ_CHANGED(old.dokument, 0,old.gr_vat, old.pgr_vat);
  update DOKUMPOZ set FROMPOZFAK = null where FROMPOZFAK = old.ref;
 if(old.waga > 0) then
   update NAGFAK set WAGA = WAGA  - old.waga where ref =old.dokument;
 if(old.objetosc > 0) then
   update NAGFAK set objetosc = objetosc  - old.objetosc where ref =old.dokument;
end^
SET TERM ; ^
