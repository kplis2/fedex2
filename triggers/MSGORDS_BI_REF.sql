--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MSGORDS_BI_REF FOR MSGORDS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('MSGORDS')
      returning_values new.REF;
end^
SET TERM ; ^
