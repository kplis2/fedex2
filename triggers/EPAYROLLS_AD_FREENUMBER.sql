--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPAYROLLS_AD_FREENUMBER FOR EPAYROLLS                      
  ACTIVE AFTER DELETE POSITION 0 
AS
  declare variable BLOCKREF integer;
  declare variable numberg varchar(255);
begin
--MWr: Zwolnienie numeru dla numeratora

  if (old.prtype <> 'UCP') then
    execute procedure GETCONFIG('EPAYROLLSNUM') returning_values :numberg;
  else
    execute procedure GETCONFIG('EPRBILLSNUM') returning_values :numberg;

  --spr. czy numerator przypisany do list plac/rachunku UCP i zwolnienie numeru:
  if (coalesce(numberg,'') <> '') then
    execute procedure FREE_NUMBER(:numberg, old.company, old.prtype, substring(old.cper from 1 for 4) || '/' || substring(old.cper from 5 for 6) || '/1', old.number,0)
      returning_values :blockref ;
end^
SET TERM ; ^
