--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FXDASSETS_BU_CHECKACCOUNTING FOR FXDASSETS                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable dictdef integer;
begin
  if (new.symbol <> old.symbol) then
  begin
    select ref from slodef where typ = 'FXDASSETS'
      into :dictdef;
    execute procedure check_dict_using_in_accounts(dictdef, old.symbol, old.company);
  end
end^
SET TERM ; ^
