--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRCONDIT_BU_TIME FOR EMPLCONTRCONDIT                
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  new.workstartstr = substring(cast(new.workstart as string)from 1 for 5);
  new.workendstr = substring(cast(new.workend as string)from 1 for 5);
end^
SET TERM ; ^
