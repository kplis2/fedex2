--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CENNIK_AI FOR CENNIK                         
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable symbol varchar(20);
declare variable oddzial varchar(10);
begin
  symbol = NULL;
  select SYMBOL, ODDZIAL from DEFCENNIK where REF=new.cennik into :symbol, :oddzial;

  if(:symbol='A') then
    if (coalesce(:oddzial,'') <> '') then
      update STANYIL set CENANETA=new.cenanet, CENABRUA=new.cenabru where WERSJAREF=new.wersjaref and ODDZIAL = :oddzial;
    else
      update STANYIL set CENANETA=new.cenanet, CENABRUA=new.cenabru where WERSJAREF=new.wersjaref;
  else if(:symbol='B') then
    if (coalesce(:oddzial,'') <> '') then
      update STANYIL set CENANETB=new.cenanet, CENABRUB=new.cenabru where WERSJAREF=new.wersjaref and ODDZIAL = :oddzial;
    else
      update STANYIL set CENANETB=new.cenanet, CENABRUB=new.cenabru where WERSJAREF=new.wersjaref;
end^
SET TERM ; ^
