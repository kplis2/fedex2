--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWJEDN_BU0 FOR TOWJEDN                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable cnt integer;
declare variable miara varchar(5);
begin
 if(new.s = 1) then begin
   cnt = null;
   select count(*) from TOWJEDN where KTM =new.ktm and S = 2 and JEDN <> new.jedn into :cnt;
   if(:cnt is null) then cnt = 0;
   if(:cnt = 0) then new.s = 2;
 end
 if(new.z = 1) then begin
   cnt = null;
   select count(*) from TOWJEDN where KTM =new.ktm and Z = 2 and JEDN <> new.jedn into :cnt;
   if(:cnt is null) then cnt = 0;
   if(:cnt = 0) then new.z = 2;
 end
 if(new.c = 1) then begin
   cnt = null;
   select count(*) from TOWJEDN where KTM =new.ktm and C = 2 and JEDN <> new.jedn into :cnt;
   if(:cnt is null) then cnt = 0;
   if(:cnt = 0) then new.c = 2;
 end
 /*to znaczy ze przylal to replikator*/
 if (new.glowna is null) then
 begin
   select miara from TOWARY where KTM =new.ktm into :miara;
   if(:miara = new.jedn) then
   begin
     new.glowna = 1;
     new.s = 1;
     new.z = 1;
   end
   else
   begin
     new.glowna = 0;
     new.s = 0;
     new.z = 0;
   end
 end
 if(new.s is null and old.s is not null) then new.s = old.s;
 if(new.z is null and old.z is not null) then new.z = old.z;
 if(new.przelicz is null or (new.przelicz <=0))
  then exception TOWJEDN_WRONPRZELICZ;
 if (new.intrastjedn = 1 and coalesce(old.intrastjedn,0) = 0) then begin
   if (exists(select first 1 1
        from towjedn
        where towjedn.intrastjedn = 1 and towjedn.ktm = new.ktm and towjedn.ref <> new.ref)) then
     exception TOWJEDN_EXPT;
 end
 --obliczenie kubatury na podstawie wymiarow
 if (coalesce(new.szer,0)<>coalesce(old.szer,0) or coalesce(new.dlug,0)<>coalesce(old.dlug,0) or coalesce(new.wys,0)<>coalesce(old.wys,0)) then begin
   if(new.szer+new.wys+new.dlug<>0) then
   begin
     --XXX JO: modyfikacja dotyczaca liczenia objetosci i wazna specyfika mnozenia
     new.vol = cast(coalesce(new.szer,0) * coalesce(new.wys,0) as numeric(15,4));
     new.vol = cast(new.vol * coalesce(new.dlug,0) as numeric(15,4));
     new.vol = new.vol/1000;
    --XXX JO: koniec
   end
 end
end^
SET TERM ; ^
