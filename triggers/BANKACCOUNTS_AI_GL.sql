--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKACCOUNTS_AI_GL FOR BANKACCOUNTS                   
  ACTIVE AFTER INSERT POSITION 0 
as
begin
  if(new.gl = 1) then
  begin
    update bankaccounts set gl = 0 where gl = 1 and dictdef = new.dictdef and dictpos = new.dictpos and ref <> new.ref;
    if(new.dictdef = 1) then
      update klienci set bank = new.bank, rachunek = new.account where ref = new.dictpos;
    else if (new.dictdef = 6) then
      update dostawcy set bank = new.bank, rachunek = new.account where ref = new.dictpos;
  end
  if(new.dictdef = 6) then
    update dostawcy set state = -1 where ref = new.dictpos;
  else if(new.dictdef = 1) then
    update klienci set state = -1 where ref = new.dictpos;
end^
SET TERM ; ^
