--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_AI_REZ FOR DOKUMPOZ                       
  ACTIVE AFTER INSERT POSITION 1 
as
declare variable ack smallint;
declare variable blokada smallint;
declare variable nieobrot smallint;
declare variable wydania smallint;
declare variable local smallint;
declare variable zrodlo smallint;
declare variable mws smallint;
declare variable magazyn varchar(3);
declare variable ildost numeric(14,4);
declare variable usluga smallint;
declare variable frommwsord integer;
declare variable inwent smallint_id;
begin
  select d.AKCEPT, t.blokada, t.NIEOBROT, t.WYDANIA, d.ZRODLO, m.mws, d.magazyn, d.frommwsord, coalesce(t.inwent,0)
    from DOKUMNAG d
      left join DEFDOKUM t on (d.typ = t.SYMBOL)
      left join defmagaz m on (m.symbol = d.magazyn)
    where d.REF=new.dokument
    into :ack, :blokada, :nieobrot, :wydania, :zrodlo, mws, magazyn, frommwsord, inwent;
  if(ack <> 1 and blokada = 1 and nieobrot <> 1 and wydania = 1) then
  begin
    execute procedure CHECK_LOCAL('DOKUMPOZ',new.ref) returning_values local;
    if(local = 1 and zrodlo = 0) then
      execute procedure REZ_SET_KTM_FAK(new.ref,3);
    select coalesce(usluga,0) from towary where ktm = new.ktm
      into usluga;
    if(local = 1 and mws = 1 and usluga <> 1 and frommwsord is null and inwent = 0) then
    begin
      execute procedure mag_checkilwol(new.ref,:magazyn,new.wersjaref,'M',new.dostawa)
        returning_values ildost;
      if (ildost < new.ilosc) then
        exception STCEN_MINUS 'Dostępna ilość na magazynie dla:'||new.ktm||' to: '||:ildost;
    end
  end
end^
SET TERM ; ^
