--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCENNIK_BD0 FOR DEFCENNIK                      
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable il integer;
begin
  select count(*) from GRUPYKLI where cennik = old.ref into :il;
  if(:il > 0 AND old.akt = 0) then exception CENNIK_CON_GRUPYKLI;
end^
SET TERM ; ^
