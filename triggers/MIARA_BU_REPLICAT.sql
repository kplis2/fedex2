--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MIARA_BU_REPLICAT FOR MIARA                          
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if((new.MIARA <> old.miara)
   or(new.opis <> old.opis) or (new.opis is null and old.opis is not null) or (new.opis is not null and old.opis is null)
   or(new.dokladnosc <> old.dokladnosc ) or (new.dokladnosc is null and old.dokladnosc is not null) or (new.dokladnosc is not null and old.dokladnosc is null)

  )then
   waschange = 1;

  execute procedure rp_trigger_bu('MIARA',old.miara, null, null, null, null, old.token, old.state,
        new.miara, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
