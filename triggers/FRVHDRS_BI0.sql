--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRVHDRS_BI0 FOR FRVHDRS                        
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable frversion integer;
  declare variable frversion1 integer;
begin
  execute procedure get_global_param('AKTUOPERATOR') returning_values new.regoper;
  new.regdate = current_timestamp(0);
  select count(*) from frcols where frhdr=new.frhdr into new.amountcols;
  if (new.frhdr='') then
    new.frhdr = null;

  select max(frversion)
    from frsumpsns S join frpsns P on (S.frpsn = P.ref and P.frhdr = new.frhdr)
    into :frversion;

  select max(frversion)
    from frpsnsalg A join frpsns P on (A.frpsn = P.ref and P.frhdr = new.frhdr)
    into :frversion1;

  if (coalesce(frversion1, 0) > coalesce(frversion, 0)) then
    new.frversion = frversion1;
  else
    new.frversion = frversion;

  if (new.frvtype is null) then new.frvtype = 0;
end^
SET TERM ; ^
