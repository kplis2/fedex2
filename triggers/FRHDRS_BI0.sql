--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRHDRS_BI0 FOR FRHDRS                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (exists (select 1 from frhdrs d where d.symbol = new.symbol)) then
    exception universal 'Sprawozdanie o takim symbolu już istnieje!';
end^
SET TERM ; ^
