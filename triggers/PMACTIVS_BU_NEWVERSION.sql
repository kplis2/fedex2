--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMACTIVS_BU_NEWVERSION FOR PMACTIVS                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable pmactivnew integer;
declare variable licznik integer;
begin
  select count(ref) from pmplans where mainref=new.pmplan into :licznik;
  if(licznik>1) then begin
    execute procedure GEN_REF('PMACTIVS') returning_values :pmactivnew;
    insert into pmactivs ( REF, PMPOSITION, PMPLAN, SYMBOL, NAME, QUANTITY,
      STARTDATE, ENDDATE, READRIGHTS, WRITERIGHTS, READRIGHTSGROUP, WRITERIGHTSGROUP,
      PMELEMENT, PARENT, MAINREF, VERSION, VERSIONNUM)
      values(:pmactivnew, old.PMPOSITION, old.PMPLAN, old.SYMBOL, old.NAME, old.QUANTITY,
      old.STARTDATE, old.ENDDATE, old.READRIGHTS, old.WRITERIGHTS, old.READRIGHTSGROUP,
      old.WRITERIGHTSGROUP, old.PMELEMENT, old.PARENT, old.REF, old.VERSION, old.VERSIONNUM);
    select version, versionnum from pmplans where ref=new.pmplan into new.version, new.versionnum;
  end
end^
SET TERM ; ^
