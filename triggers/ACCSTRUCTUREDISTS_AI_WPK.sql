--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTUREDISTS_AI_WPK FOR ACCSTRUCTUREDISTS              
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable current_company companies_id;
declare variable is_pattern  smallint_id;
declare variable bkaccount_dest bkaccounts_id;
declare variable slopoz_dest slopoz_id;
begin
  -- Jeżeli wyróżnik nie jest synchronizowany to trzebva sprawdzić, czy nie jest wzorcem
  if (coalesce(new.pattern_ref,0)=0) then
  begin
    execute procedure get_global_param('CURRENTCOMPANY')
      returning_values :current_company;
    select c.pattern
      from companies c
      where c.ref = :current_company
      into :is_pattern;
    if (coalesce(is_pattern,0)>0) then
    begin
      if (new.otable = 'BKACCOUNTS') then
      begin
      -- Zakład jest wzorcem, wiec musimy wkopiowac wyróżnik nowy do zależnych
        for
          select b.ref
            from bkaccounts b
            where b.pattern_ref = new.oref
            into :bkaccount_dest
        do begin
          insert into accstructuredists(number,distdef,distfilter,otable,oref,ord, pattern_ref)
            values(new.number, new.distdef, new.distfilter, new.otable, :bkaccount_dest, new.ord, new.ref);
        end
      end
      else if( new.otable = 'SLOPOZ')then
      begin
        for
          select s.ref
            from slopoz s
            where s.pattern_ref = new.oref
            into :slopoz_dest
        do begin
           insert into accstructuredists(number,distdef,distfilter,otable,oref,ord, pattern_ref)
             values(new.number, new.distdef, new.distfilter, new.otable, :slopoz_dest, new.ord, new.ref);
        end
      end
    end
  end
end^
SET TERM ; ^
