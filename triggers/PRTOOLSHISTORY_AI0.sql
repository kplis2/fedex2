--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSHISTORY_AI0 FOR PRTOOLSHISTORY                 
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable plamount numeric(14,2);
declare variable miamount numeric(14,2);
BEGIN
  if (exists (select first 1 1 from prtoolsdocs pd join prtoolsdefdocs d on (pd.prtoolsdefdoc = d.symbol and d.outside = 0)
    where pd.ref = new.prtoolsdoc)
  ) then
  begin
    select sum(p.amount)
      from prtoolshistory p
        join prtoolsdocs d on (p.prtoolsdoc = d.ref)
        join prtoolsdefdocs df on (d.prtoolsdefdoc = df.symbol)
      where p.prtools = new.prtools
        and p.tooltype = new.tooltype
        and coalesce(p.invno,'') = coalesce(new.invno,'')
        and coalesce(p.price,0) = coalesce(new.price,0)
        and coalesce(p.prdsdef,'') = coalesce(new.prdsdef,'')
        and coalesce(p.employees,0) = coalesce(new.employees,0)
        and coalesce(p.prschedzam,0) = coalesce(new.prschedzam,0)
        and coalesce(p.prschedguide,0) = coalesce(new.prschedguide,0)
        and coalesce(p.prmachine,0) = coalesce(new.prmachine,0)
        and coalesce(p.slodef,0) = coalesce(new.slodef,0)
        and coalesce(p.slopoz,0) = coalesce(new.slopoz,0)
        and p.hsaction = 0
        and df.outside = 0
      into :plamount;

    select sum(p.amount)
      from prtoolshistory p
        join prtoolsdocs d on (p.prtoolsdoc = d.ref)
        join prtoolsdefdocs df on (d.prtoolsdefdoc = df.symbol)
      where p.prtools = new.prtools
        and p.tooltype = new.tooltype
        and coalesce(p.invno,'') = coalesce(new.invno,'')
        and coalesce(p.price,0) = coalesce(new.price,0)
        and coalesce(p.prdsdef,'') = coalesce(new.prdsdef,'')
        and coalesce(p.employees,0) = coalesce(new.employees,0)
        and coalesce(p.prschedzam,0) = coalesce(new.prschedzam,0)
        and coalesce(p.prschedguide,0) = coalesce(new.prschedguide,0)
        and coalesce(p.prmachine,0) = coalesce(new.prmachine,0)
        and coalesce(p.slodef,0) = coalesce(new.slodef,0)
        and coalesce(p.slopoz,0) = coalesce(new.slopoz,0)
        and p.hsaction = 1
        and df.outside = 0
      into :miamount;

    if (coalesce(:plamount,0) > coalesce(:miamount,0)) then
      exception prtools_error 'Ilosć wypożyczona < ilosci zwróconej ';
  end
END^
SET TERM ; ^
