--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWRKCRTLIMITS_AIUD_EVAC FOR EWRKCRTLIMITS                  
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 
as
declare variable cyear integer;
declare variable vc2 smallint;
declare variable vmc smallint;
declare variable empl integer;
begin
  if(updating and old.ecolumn is not distinct from new.ecolumn
              and old.daylimit is not distinct from new.daylimit) then
    exit;
  execute procedure get_config('VACREQCOLUMN', 2) returning_values :vc2;
  execute procedure get_config('VACMDCOLUMN', 2) returning_values :vmc;
  select extract(year from todate), employee
    from eworkcertifs
    where ref = coalesce(new.workcertif,old.workcertif)
    into cyear, empl;
  if(inserting or (updating and new.ecolumn in(vc2,vmc))) then
  begin
    if(new.ecolumn = vc2) then
      update evaclimits set vacreqused = vacreqused + new.daylimit
        where employee = :empl and vyear = :cyear;
    if(new.ecolumn = vmc) then
      update evaclimits set vacmdused = vacmdused + new.daylimit
        where employee = :empl and vyear = :cyear;
  end
  if(deleting or (updating and old.ecolumn in (vc2,vmc))) then
  begin
    if(old.ecolumn = vc2) then
      update evaclimits set vacreqused = vacreqused - old.daylimit
        where employee = :empl and vyear = :cyear;
    if(old.ecolumn = vmc) then
      update evaclimits set vacmdused = vacmdused - old.daylimit
        where employee = :empl and vyear = :cyear;
  end
end^
SET TERM ; ^
