--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSD_BU_EDI FOR LISTYWYSD                      
  ACTIVE BEFORE UPDATE POSITION 10 
as
declare variable local smallint;
declare variable gln type of gln_id;
declare variable ederulesym varchar(60);
declare variable edi smallint;
declare variable editype varchar(40);
declare variable edocref integer;
declare variable ederule integer;
declare variable isedocnew integer;
begin
  execute procedure CHECK_LOCAL('LISTYWYSD',new.ref) returning_values :local;
  --akceptacja
  if(new.akcept = 2 and old.akcept<>2 and :local = 1) then begin
    edi = 0;
    if(new.adrtyp='T' and new.slodef=1 and new.slopoz is not null) then begin
      if(:gln is not null) then edi = 1;
    end
    --okreslenie typu edi
    editype = null;
    if(edi = 1) then begin
      for select eg.ederule, ederules.symbol
        from edegen eg
           join ederules on (eg.ederule = ederules.ref)
        where eg.slopoz = new.slodef and eg.slodef = new.slopoz
            and new.dataakc >= eg.fromdate and (new.dataakc <= eg.todate or eg.todate is null)
            and eg.dokumtype = 'LISTYWYSD'
            into :ederule, :ederulesym
      do begin
        --proba eksportu naglowka dokumentu - ewentualnie procedura weryfikujaca odrzuca dany dokument
          execute procedure EDEDOC_INIT(:ederule,'LISTYWYSD',new.ref,'','','') returning_values :edocref, :isedocnew;
          if(:edocref > 0) then begin
            edi = 1;
            editype = :ederulesym;
          end
            else edi = 0;
         end
    end

  end
  --wycofanie akceptacji
  if(new.akcept<>2 and old.akcept=2 and :local = 1) then begin
    delete from EDEDOCSH where otable='LISTYWYSD' and oref=new.ref;
    edi = 0;
  end
end^
SET TERM ; ^
