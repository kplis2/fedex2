--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRORDANALMAT_BI0 FOR PRORDANALMAT                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  new.connectionid = current_connection;
  if (new.quantityav is null) then new.quantityav = 0;
  if (new.status is null) then new.status = 10;
  if (new.qbefore is null) then new.qbefore = 0;
  if (new.qafter is null) then new.qafter = 0;
end^
SET TERM ; ^
