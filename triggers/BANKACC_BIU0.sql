--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKACC_BIU0 FOR BANKACC                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
declare variable correctaccount integer;
declare variable msg4badaccount varchar(60);
begin
  if (new.stanbank='') then
    new.stanbank = null;
  -- uzupelnianie banku stanowiska kasowego/bankowego na podstawie banku rachunku
  if (new.stanbank is not null and new.stanbank <> '') then
    update rkstnkas set bank = new.bank where kod = new.stanbank and company = new.company;
  if (old.stanbank is not null and (new.stanbank is null or new.stanbank = '')) then
    update rkstnkas set bank = null where kod = old.stanbank and company = new.company;
  if (new.trantype='') then
    new.trantype = null;
  if (new.account is not null and (new.account <> old.account)) then
  begin -- teraz ,ozna robic updeta na innych polach Pawel
    -- jako 2 param mozna podac dwuliterowy kod kraju (domyslnie PL)
    execute procedure control_bankaccount_number(new.account,'PL')
      returning_values (correctaccount, msg4badaccount);
    if (correctaccount = 0) then
      exception BANKI_BAD_ACCOUNT :msg4badaccount;
  end
end^
SET TERM ; ^
