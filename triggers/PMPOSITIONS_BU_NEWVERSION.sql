--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPOSITIONS_BU_NEWVERSION FOR PMPOSITIONS                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable pmpositionnew integer;
declare variable licznik integer;
begin
  select count(ref) from pmplans where mainref=new.pmplan into :licznik;
  if(:licznik>1) then begin
    execute procedure GEN_REF('PMPOSITIONS') returning_values :pmpositionnew;
    insert into pmpositions( REF, PMELEMENT, PMPLAN, NAME, SYMBOL, KTM, DESCRIPT,
      QUANTITY, POSITIONTYPE, CALCPRICE, CALCVAL, BUDPRICE, BUDVAL, BUDVALR, INTQ,
      OFEQ, EXTQ, EXTVAL, REALQ, REALVAL, USEDQ, USEDPRICE, USEDVAL, READRIGHTS,
      WRITERIGHTS, READRIGHTSGROUP, WRITERIGHTSGROUP, UNIT, VERSION, VERSIONNUM, MAINREF)
      values(:pmpositionnew, old.PMELEMENT, old.PMPLAN, old.NAME, old.SYMBOL, old.KTM, old.DESCRIPT,
      old.QUANTITY, old.POSITIONTYPE, old.CALCPRICE, old.CALCVAL, old.BUDPRICE, old.BUDVAL, old.BUDVALR, old.INTQ,
      old.OFEQ, old.EXTQ, old.EXTVAL, old.REALQ, old.REALVAL, old.USEDQ, old.USEDPRICE, old.USEDVAL, old.READRIGHTS,
      old.WRITERIGHTS, old.READRIGHTSGROUP, old.WRITERIGHTSGROUP, old.UNIT, old.VERSION, old.VERSIONNUM, old.REF);
    select version, versionnum from pmplans where ref=new.pmplan into new.version, new.versionnum;
  end
end^
SET TERM ; ^
