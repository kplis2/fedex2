--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPERSCOMPANY_BI_REF FOR EPERSCOMPANY                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EPERSCOMPANY')
      returning_values new.REF;
end^
SET TERM ; ^
