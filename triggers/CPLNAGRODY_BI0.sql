--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLNAGRODY_BI0 FOR CPLNAGRODY                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  else if(new.ktm is not null and new.wersja is not null) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
end^
SET TERM ; ^
