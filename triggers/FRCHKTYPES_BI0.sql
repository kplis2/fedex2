--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRCHKTYPES_BI0 FOR FRCHKTYPES                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.balacc is null) then new.balacc = 0;
  if (new.resacc is null) then new.resacc = 0;
  if (new.nullonly is null) then new.nullonly = 0;
end^
SET TERM ; ^
