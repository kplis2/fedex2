--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CTYPYKON_BI_REF FOR CTYPYKON                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CTYPYKON')
      returning_values new.REF;
end^
SET TERM ; ^
