--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHTOOLS_BI_REF FOR PRSHTOOLS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('PRSHTOOLS')
      returning_values new.REF;
  if (new.opermaster is null or new.opermaster = 0)
    then exception prsheets_data 'Definiowanie narzedzia dozwolone wyłącznie dla operacji.';
end^
SET TERM ; ^
