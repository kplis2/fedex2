--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROMOCT_AU_REPLICAT FOR PROMOCT                        
  ACTIVE AFTER UPDATE POSITION 1 
as
begin
  if((new.constcena <> old.constcena) or (new.constcena is not null and old.constcena is null) or (new.constcena is null and old.constcena is not null) or
     (new.ilosc <> old.ilosc ) or (new.ilosc is not null and old.ilosc is null) or (new.ilosc is null and old.ilosc is not null) or
     (new.wersja <> old.wersja ) or (new.wersja is not null and old.wersja is null) or (new.wersja is null and old.wersja is not null) or
     (new.procent <> old.procent ) or (new.procent is not null and old.procent is null) or (new.procent is null and old.procent is not null)
   ) then
     update PROMOCJE set STATE = -1 where ref=new.promocja;
end^
SET TERM ; ^
