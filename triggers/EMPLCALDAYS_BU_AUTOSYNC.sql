--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCALDAYS_BU_AUTOSYNC FOR EMPLCALDAYS                    
  ACTIVE BEFORE UPDATE POSITION 2 
AS
begin
  --DS: przywracanie domyslnych wartosci dla danego dnia
  if (new.autosync=1 and old.autosync=0) then
  begin
    select daykind, workstart, workend, workstart2, workend2, worktime
      from ecaldays
      where ref = new.ecalday
      into new.daykind, new.workstart, new.workend, new.workstart2, new.workend2, new.nomworktime;
    new.overtime0 = 0;
    new.overtime50 = 0;
    new.overtime100 = 0;
    new.compensatory_time = 0;
    new.compensated_time = 0;
    new.overtime0str = '';
    new.overtime50str = '';
    new.overtime100str = '';
    new.compensatory_timestr = '';
    new.compensated_timestr = '';
    new.overtime_type = 0;
    new.customized = 0;
    execute procedure count_nighthours(new.ecalendar, new.workstart, new.workend, new.workstart2, new.workend2)
      returning_values new.nighthours;
    execute procedure durationstr2(new.nighthours)
      returning_values new.nighthoursstr;
  end
end^
SET TERM ; ^
