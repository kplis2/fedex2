--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSTYPESPOS_BI0 FOR PRTOOLSTYPESPOS                
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.cquantity is null) then new.cquantity = 1;
  if (new.cquantity <= 0) then
    EXCEPTION PRTOOLSTYPES_AMOUNT 'Ilosc narzedzia w kompelcie musi być > 0';
end^
SET TERM ; ^
