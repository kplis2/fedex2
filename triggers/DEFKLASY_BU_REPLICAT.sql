--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFKLASY_BU_REPLICAT FOR DEFKLASY                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (
     new.nazwa <> old.nazwa or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null) or
     new.tresc <> old.tresc or (new.tresc is null and old.tresc is not null) or (new.tresc is not null and old.tresc is null) or
     new.state = -2
   ) then
     execute procedure REPLICAT_STATE('DEFKLASY',new.state, new.ref, NULL, NULL, NULL) returning_values new.state;
end^
SET TERM ; ^
