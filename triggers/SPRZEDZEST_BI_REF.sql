--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDZEST_BI_REF FOR SPRZEDZEST                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SPRZEDZEST')
      returning_values new.REF;
end^
SET TERM ; ^
