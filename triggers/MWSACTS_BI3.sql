--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_BI3 FOR MWSACTS                        
  ACTIVE BEFORE INSERT POSITION 3 
AS
declare variable whp varchar(3);
declare variable whl varchar(3);
begin
  if (new.mwsconstlocp is not null and new.mwsconstlocl is not null) then
  begin
    select wh from mwsconstlocs where ref = new.mwsconstlocp
      into whp;
    select wh from mwsconstlocs where ref = new.mwsconstlocl
      into whl;
    if (whp is not null and whp <> '' and whl is not null and whl <> '' and whl <> whp) then
      exception universal 'Próba przesunięcia towarów między różnymi magazynami';
  end
end^
SET TERM ; ^
