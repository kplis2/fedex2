--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMSER_AI0 FOR DOKUMSER                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable typ varchar(1);
begin
  if (new.orgdokumser is not null) then
  begin
    select s.typ
      from dokumser s
      where s.ref = new.orgdokumser
      into typ;

    if (:typ = 'R') then
    begin
      --relizacja dokumsera z raportu produkcyjnego na dokument magazynowy
      update dokumser s set s.iloscroz = s.iloscroz + new.ilosc
        where s.ref = new.orgdokumser;
    end 
  end
end^
SET TERM ; ^
