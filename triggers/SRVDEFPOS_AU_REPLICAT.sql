--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVDEFPOS_AU_REPLICAT FOR SRVDEFPOS                      
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if((new.descript <> old.descript ) or (new.descript is not null and old.descript is null) or (new.descript is null and old.descript is not null)
    or (new.usluga <> old.usluga ) or (new.usluga is not null and old.usluga is null) or (new.usluga is null and old.usluga is not null)
    or (new.printable <> old.printable ) or (new.printable is not null and old.printable is null) or (new.printable is null and old.printable is not null)
    or (new.contrtype <> old.contrtype ) or (new.contrtype is not null and old.contrtype is null) or (new.contrtype is null and old.contrtype is not null)
    or (new.createfak <> old.createfak ) or (new.createfak is not null and old.createfak is null) or (new.createfak is null and old.createfak is not null)
 ) then
     update CONTRTYPES set STATE = -1 where ref=new.contrtype;
end^
SET TERM ; ^
