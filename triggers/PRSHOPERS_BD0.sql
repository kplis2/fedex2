--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHOPERS_BD0 FOR PRSHOPERS                      
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  delete from PRSHMAT where OPERMASTER = old.ref;
  delete from prshopersecontrdefs where prshoper = old.ref;
end^
SET TERM ; ^
