--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHORTAGES_AI0 FOR PRSHORTAGES                    
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable amount numeric(14,4);
declare variable amount1 numeric(14,4);
declare variable kamountnorm numeric(14,4);
declare variable amountshortages numeric(14,4);
declare variable prschedguidespos integer;
begin
  if (coalesce(new.ktm,'') = '') then
    exception PRSHORTAGES_KTM;
  if(new.amount <> 0 and new.prschedoper is not null) then
  begin
    select sum(s.headergroupamount)
      from prshortages s
        join prshortagestypes t on (t.symbol = s.prshortagestype)
      where s.prschedoper = new.prschedoper
        and coalesce(t.noamountinfluence,0) = 0
        and coalesce(s.headergroup,s.ref) = s.ref
      into :amountshortages;

    if (amountshortages is null) then amountshortages = 0;
    update prschedopers pso set pso.amountshortages = :amountshortages
      where pso.ref = new.prschedoper;
  end

  if(new.prschedguidespos is not null) then
  begin
    update prschedguides g set g.pggamountcalc = 1 where g.ref = new.prschedguide;
    for
      select p.ref, coalesce(p.kamountnorm,1)
        from prschedguidespos p
        where p.prschedguide = new.prschedguide
          and p.prschedopernumber <= (select o.number from prschedopers o where o.ref = new.prschedoper)
        into :prschedguidespos, :kamountnorm
    do begin
      amount = 0;
      amount1 = 0;
      select sum((s.amount / coalesce(o.reportedfactor,1)) * :kamountnorm)
        from prshortages s
          join prshortagestypes t on (t.symbol = s.prshortagestype)
          join prschedopers o on (o.ref = s.prschedoper)
        where s.prschedguide = new.prschedguide
          and coalesce(t.noamountinfluence,0) = 0
          and coalesce(t.backasresourse,0) = 0
        into :amount;

      select coalesce(sum(s.amount),0)
        from prshortages s
          join prshortagestypes t on (t.symbol = s.prshortagestype)
        where s.prschedguide = new.prschedguide
          and s.prschedguidespos = :prschedguidespos
          and coalesce(t.noamountinfluence,0) = 0
          and coalesce(t.backasresourse,0) = 1
        into :amount1;

      amount = coalesce(:amount,0) + coalesce(:amount1,0);

      select :amount * (select first 1 oo.reportedfactor from prschedopers oo
          where oo.guide = new.prschedguide and oo.activ = 1 and oo.reported > 0
          order by oo.number desc)
        from rdb$database
        into :amount;
      update prschedguidespos p set p.amountshortage = :amount where p.ref = :prschedguidespos;

      kamountnorm = null;
    end
    update prschedguides g set g.pggamountcalc = 0 where g.ref = new.prschedguide;
  end
  if(new.prschedoper is not null and not exists(select ref from propersraps where prschedoper = new.prschedoper and ref <> new.ref))
    then execute procedure PRSCHEDOPERS_STATUSCHECK(null, null, new.prschedoper);

  if (exists(select first 1 1 from prshortagestypes t where t.symbol = new.prshortagestype and t.header = 1)
    and coalesce(new.headergroup,0) = 0
  ) then
    exception PRSHORTAGES_ERROR 'Błąd grupy braków';

  if (exists (select first 1 1 from prshortagestypes t where t.symbol = new.prshortagestype and t.backasresourse = 0 and t.header = 1 and t.noamountinfluence = 0)
    and new.headergroupamount <> new.amount
  ) then
    exception PRSHORTAGES_ERROR 'Brak wymairowy, błąd naliczenia ilosci surowca';
end^
SET TERM ; ^
