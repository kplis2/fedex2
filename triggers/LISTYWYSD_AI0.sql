--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSD_AI0 FOR LISTYWYSD                      
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable ildok integer;
declare variable ilspr integer;
declare variable waga numeric(15,4);
declare variable objetosc numeric(15,4);
declare variable koszt numeric(14,2);
begin
  if(new.typ = 'M') then
     update DOKUMNAG set WYSYLKA = new.listawys where ((DOKUMNAG.WYSYLKA is null) or (DOKUMNAG.WYSYLKA <> new.listawys)) and DOKUMNAG.REF = new.refdok;
  else if(new.typ = 'Z') then
     update NAGZAM set WYSYLKA = new.listawys where ref =new.refzam and ((NAGZAM.WYSYLKA is null) or (NAGZAM.WYSYLKA <> new.listawys));
  else if(new.typ = 'R') then
     update SRVREQUESTS set SRVREQUESTS.WYSYLKA = new.ref where SRVREQUESTS.REF =new.refsrv;
  if(new.listawys is not null) then begin
    ildok = 0;
    select count(*) from listywysd where listawys = new.listawys  into :ildok;
    if(:ildok is null) then ildok = 0;
    waga = 0;
    objetosc = 0;
    koszt = 0;
    select sum(waga),sum(kosztdost), sum(objetosc) from listywysd where listawys=new.listawys into :waga,:koszt,:objetosc;
    if(:waga is null) then waga = 0;
    if(:koszt is null) then koszt = 0;
    if(:objetosc is null) then objetosc = 0;
    ilspr = 0;
    select count(*) from listywysd where listawys=new.listawys and wyschecked=1 into :ilspr;
    if(:ilspr is null) then ilspr = 0;
    update listywys set ildok = :ildok, waga = :waga, kosztdost = :koszt, ilspr = :ilspr, objetosc = :objetosc
      where ref=new.listawys;
  end
  if (new.acceptoper is not null or new.acceptmistake is not null) then
    execute procedure LISTYWYSD_MISTAKES_PROGRES(new.ref, 'A',new.acceptoper,new.acceptmistake);
  if (new.selloper is not null or new.sellmistake is not null) then
    execute procedure LISTYWYSD_MISTAKES_PROGRES(new.ref, 'S',new.selloper,new.sellmistake);
  if (new.checkoper is not null or new.checkmistake is not null) then
    execute procedure LISTYWYSD_MISTAKES_PROGRES(new.ref, 'S',new.checkoper,new.checkmistake);
end^
SET TERM ; ^
