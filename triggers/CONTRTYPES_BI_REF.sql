--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CONTRTYPES_BI_REF FOR CONTRTYPES                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CONTRTYPES')
      returning_values new.REF;
  if(new.typfak = '') then new.typfak = null;
  if(new.numbergen = '') then new.numbergen = null;
end^
SET TERM ; ^
