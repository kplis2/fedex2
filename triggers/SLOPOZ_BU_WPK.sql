--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLOPOZ_BU_WPK FOR SLOPOZ                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
   -- jeżeli jest to pozycja slownika synchronizowana to pozwalaj tylko na update ze wzorca
  if (new.pattern_ref is not null) then
  begin
    if (coalesce(new.internalupdate,0)=0) then
    begin
      --sytucja gdzie chcemy zmienic krotke slownika ktora podlega synchronizacji
      --nie mozemy tego zrobic i sciagamy wartosci ze wzorca
      select kod, kontoks, crm, token, state, akt, cpodmiot,
        masterref, masterident, opis, dflags, rights, rightsgroup
      from slopoz
      where ref = new.pattern_ref
      into new.kod, new.kontoks, new.crm, new.token, new.state, new.akt, new.cpodmiot,
        new.masterref, new.masterident, new.opis, new.dflags, new.rights, new.rightsgroup;
    end else begin
      --update ze wzorca wiec zakladam, ze dane ok i zeruje internal
      new.internalupdate = 0;
    end
  end
end^
SET TERM ; ^
