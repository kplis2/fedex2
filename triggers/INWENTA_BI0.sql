--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INWENTA_BI0 FOR INWENTA                        
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable chronologia integer;
declare variable lokres varchar(6);
declare variable cnt integer;
begin
  if(new.zamk is null) then new.zamk = 0;
  if((new.magazyn is null) or (new.magazyn = '   ')) then exception INWENTA_BEZ_MAGAZYNU;
  if(new.calkowity is null) then new.calkowity = 0;
  if(new.maroznice is null) then new.maroznice = 0;
  if(new.data is null) then new.data = current_date;
  if(new.symbol is null) then new.symbol = '';
  if(new.symbol = '') then exception INWENTAP_EXPT 'Brak podanego symbolu arkusza inwentaryzacyjnego.';
  if(new.rozliczeniowy is null) then new.rozliczeniowy = 0;
  if(new.zbiorczy is null) then new.zbiorczy = 0;
  if(new.zpartiami is null) then new.zpartiami = 0;
  select CHRONOLOGIA from DEFMAGAZ where SYMBOL = new.magazyn into :chronologia;
  if(:chronologia is null) then chronologia = 0;
  /*kontrola okresu*/
  lokres = cast( extract( year from new.data) as char(4));
  if(extract(month from new.data) < 10) then
       lokres = :lokres ||'0' ||cast(extract(month from new.data) as char(1));
  else
       lokres = :lokres ||cast(extract(month from new.data) as char(2));
  if((new.okres is null) or (new.okres = '      ')) then
    new.okres = :lokres;
  else
    if(new.okres <> :lokres) then exception INWENTA_DATA_POZA_OKRES;
  /* kontrola chronologii */
  if(:chronologia >= 1) then begin
    select count(*) from DOKUMNAG where MAGAZYN=new.magazyn and DATA >= (new.data+:chronologia) and AKCEPT = 1 into :cnt;
    if(:cnt > 0) then exception INWENTA_SA_AKCEPT_DOK_POZ;
  end
  if(new.inwentaparent is not null ) then
    select SYMBOL from INWENTA where REF=new.inwentaparent into new.inwentaparents;
end^
SET TERM ; ^
