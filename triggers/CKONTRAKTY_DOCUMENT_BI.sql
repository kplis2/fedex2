--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKONTRAKTY_DOCUMENT_BI FOR CKONTRAKTY                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.faza is not null) then
  begin
    select cfazy.document from cfazy where cfazy.ref = new.faza into new.document;
  end
end^
SET TERM ; ^
