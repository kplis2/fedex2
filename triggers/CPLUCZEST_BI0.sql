--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLUCZEST_BI0 FOR CPLUCZEST                      
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable cplstatus integer;
declare variable nazwaucz varchar(255);
declare variable nazwacpl varchar(255);
begin
   select STATUS, NAZWA from CPL where REF=new.cpl into :cplstatus, :nazwacpl;
   if(:cplstatus > 1) then exception CPL_ZAMKNIETY;/* mozna dodawać, jesli jest w trakcie przygowoania */
   if(new.pkosoba is not NULL and new.cpodmiot is not NULL) then begin
     select NAZWA from PKOSOBY where PKOSOBY.REF = new.pkosoba into nazwaucz;
     select SKROT,NAZWA,CSTATUS from CPODMIOTY where CPODMIOTY.REF = new.cpodmiot into new.skrot, new.nazwafirmy, new.cstatus;
   end else if(new.cpodmiot is not NULL) then begin
     select SKROT,SKROT,NAZWAFIRMY,CSTATUS from CPODMIOTY where CPODMIOTY.REF = new.cpodmiot into new.skrot, nazwaucz, new.nazwafirmy,new.cstatus;
   end
   if(nazwaucz is null) then nazwaucz = '';
   if(nazwacpl is null) then nazwacpl = '';
   if(new.nrkarty is not null and new.nrkarty <> '') then new.nazwa = substring(nazwaucz||' - '||nazwacpl||' - '||new.nrkarty from 1 for 80);
   else new.nazwa = substring(nazwaucz||' - '||nazwacpl from 1 for 80);
   if(new.mnoznik is null) then new.mnoznik = 1;
end^
SET TERM ; ^
