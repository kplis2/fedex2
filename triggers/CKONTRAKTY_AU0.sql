--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKONTRAKTY_AU0 FOR CKONTRAKTY                     
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable akt smallint;

begin
  if(new.cpodmiot<>old.cpodmiot) then
    execute procedure CKONTRAKTY_UPDATE_CPODMIOTYKON(new.ref);
  if (new.faza <> old.faza or (new.faza is not null and old.faza is null) or (new.faza is null and old.faza is not null)
    or new.nazwa <> old.nazwa or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null)
    or new.cpodmiot <> old.cpodmiot or (new.cpodmiot is not null and old.cpodmiot is null) or (new.cpodmiot is null and old.cpodmiot is not null)
     ) then
  begin
    if (exists(
         select slopoz.ref
           from slopoz
             join contrtypes on (slopoz.slownik = contrtypes.projectdef)
           where slopoz.masterref = new.ref)
       ) then
    begin
      if (new.faza is null) then
        update slopoz set slopoz.akt = 0, nazwa = new.nazwa, opis=new.cpodmskrot where slopoz.masterref = new.ref;
      else begin
        select cfazy.document
          from cfazy
          where cfazy.aktywne = 1 and cfazy.ref = new.faza
          into :akt;
        if(:akt is null) then akt = 0;
        update slopoz set slopoz.akt = :akt, nazwa = new.nazwa, opis=new.cpodmskrot where slopoz.masterref = new.ref;
      end
    end
  end
end^
SET TERM ; ^
