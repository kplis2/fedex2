--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFMENU_BI_ORDER FOR DEFMENU                        
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable maxnum integer;
begin
  new.ord = 1;
  select max(numer) from defmenu where poziom = new.poziom
    into :maxnum;
  if (new.numer is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.numer = maxnum + 1;
  end else if (new.numer <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update defmenu set ord = 0, numer = numer + 1
      where numer >= new.numer and poziom = new.poziom;
  end else if (new.numer > maxnum) then
    new.numer = maxnum + 1;
end^
SET TERM ; ^
