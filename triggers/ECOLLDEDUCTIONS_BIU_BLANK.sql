--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECOLLDEDUCTIONS_BIU_BLANK FOR ECOLLDEDUCTIONS                
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if (new.initdeduction = 0) then new.initdeduction = null;
  if (new.deductionrate = 0) then new.deductionrate = null;
  if (new.deducted is null) then new.deducted = 0;
  if (new.priority is null) then new.priority = 0;
  if (new.sequence is null) then new.sequence = 0;
  if (new.limitkp4ucp is null) then new.limitkp4ucp = 0;

--BS45944, znacznik ART140U1P3 nie jest istotny dla alimentow
  if (new.dkind = 7700) then new.art140u1p3 = 0;
  else new.art140u1p3 = coalesce(new.art140u1p3,1);
end^
SET TERM ; ^
