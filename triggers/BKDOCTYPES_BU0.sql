--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCTYPES_BU0 FOR BKDOCTYPES                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if ((new.multicurr is NULL) or (new.kind=0)) then new.multicurr = 0;
  if (new.kind=3) then new.multicurr = 1;
  if (new.kind is distinct from old.kind) then
    if (exists
        (select first 1 1 from bkdocs b
         join bkdoctypes bd on (b.doctype = bd.ref)
          where bd.kind = old.kind)) then
     exception universal 'Zmiana typu dokumentu ksiegowego niemozliwa.';
end^
SET TERM ; ^
