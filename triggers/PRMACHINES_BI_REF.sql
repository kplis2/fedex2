--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRMACHINES_BI_REF FOR PRMACHINES                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('PRMACHINES')
      returning_values new.REF;
end^
SET TERM ; ^
