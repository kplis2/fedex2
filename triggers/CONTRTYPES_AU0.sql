--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CONTRTYPES_AU0 FOR CONTRTYPES                     
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if ((new.projectdef is not null and old.projectdef is null)
      or (new.projectdef <> old.projectdef)
      or (new.projectdef is null and old.projectdef is not null)) then
  begin
    update SLODEF set SLODEF.mastertable = 'CKONTRAKTY' where slodef.ref = new.projectdef;
  end
end^
SET TERM ; ^
