--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FAEQUIPMENT_BI_REF FOR FAEQUIPMENT                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FAEQUIPMENT')
      returning_values new.REF;
end^
SET TERM ; ^
