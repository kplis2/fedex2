--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKPLIKSEGREGATORY_BD0 FOR DOKPLIKSEGREGATORY             
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if(old.segdefault = 1)then exception universal 'Usunięcie powiązania domyślnego zablokowane. Aby usunąć powiązanie zmień segregator na inny na fiszce dokumentu.';
end^
SET TERM ; ^
