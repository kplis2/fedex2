--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MENU_BI_ORDER FOR MENU                           
  ACTIVE BEFORE INSERT POSITION 1 
as
begin
  if (new.numer is null) then
  begin
    select case when max(numer) is null then 1 else max(numer)+1 end
      from MENU where rodzic = new.rodzic and symbol = new.symbol
      into new.numer;
    new.ORD = 1;
  end

  if (new.ord is null) then
    new.ord = 0;
end^
SET TERM ; ^
