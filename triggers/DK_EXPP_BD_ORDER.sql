--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DK_EXPP_BD_ORDER FOR DK_EXPP                        
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then exit;
  update dk_expp set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and dkexp = old.dkexp and dkdok = old.dkdok;
end^
SET TERM ; ^
