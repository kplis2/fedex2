--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDECLPOS_BUD_OPERATOR FOR EDECLPOS                       
  ACTIVE BEFORE UPDATE OR DELETE POSITION 0 
AS
declare variable chgoperator integer;
begin
  execute procedure get_global_param ('AKTUOPERATOR')
    returning_values :chgoperator;
  update edeclarations d set d.chgoperator = :chgoperator, d.chgtimestamp = current_timestamp(0)
    where d.ref = new.edeclaration;
end^
SET TERM ; ^
