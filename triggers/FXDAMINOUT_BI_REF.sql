--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FXDAMINOUT_BI_REF FOR FXDAMINOUT                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FXDAMINOUT')
      returning_values new.REF;
end^
SET TERM ; ^
