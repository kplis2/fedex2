--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYIL_BI0_JEDN_ZM FOR STANYIL                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable przelicznik1 numeric(14,4);
declare variable przelicznik2 numeric(14,4);
declare variable miara1 JEDN_MIARY;
declare variable miara2 JEDN_MIARY;
begin

  select first 1 t.przelicz,t.jedn
    from towjedn t
    where t.ktm = new.ktm and t.dodatkowa = 1
  into : przelicznik1, :miara1;
  new.miara1 = :miara1;
  if (przelicznik1 is not null) then
    new.iloscjm1 = new.ilosc / :przelicznik1;

  select first 1 t.przelicz,t.jedn
    from towjedn t
    where t.ktm = new.ktm and t.dodatkowa = 2
  into : przelicznik2, :miara2;
  new.miara2 = :miara2;
  if (przelicznik2 is not null) then
    new.iloscjm2 = new.ilosc / :przelicznik2;

end^
SET TERM ; ^
