--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_BU_ORDER FOR DOKUMPOZ                       
  ACTIVE BEFORE UPDATE POSITION 1 
as
  declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 0;
  if (new.ord = 5) then exit;
  if (new.numer is null) then new.numer = old.numer;
  if (new.ord = 1 or new.numer = old.numer) then
  begin
    new.ord = 0;
    exit;
  end
  --numerowanie obszarow w rzedzie gdy nie podamy numeru
  if (new.numer <> old.numer) then
    select max(numer) from dokumpoz
      where dokument = new.dokument
        and coalesce(fake,0) = 0
      into :maxnum;
  else
    maxnum = 0;
  if (new.numer < old.numer) then
  begin
    update dokumpoz set ord = 1, numer = numer + 1
      where ref <> new.ref and numer >= new.numer
        and numer < old.numer and dokument = new.dokument
        and coalesce(fake,0) = 0;
  end
  else if (new.numer > old.numer and new.numer <= maxnum) then
  begin
    update dokumpoz set ord = 1, numer = numer - 1
      where ref <> new.ref and numer <= new.numer
        and numer > old.numer and dokument = new.dokument
        and coalesce(fake,0) = 0;
  end else if (new.numer > old.numer and new.numer > maxnum) then
    new.numer = old.numer;

  -- rozwiazanie dotyczace fake-ow ustalone z Marcinem Smereka
  -- pozycje fake dostaja numer pozycji nadrzednej - alttopoz
  -- MSt: BS71202
  -- Przesuwam update na fake'ach  na koniec, bo przeciez najpierw ustalam numer nadrzednej pozycji
  -- a potem jak ustale to dopiero moge wg tego aktualizowac numer podrzednych pozycji
  if (new.numer <> old.numer and coalesce(new.havefake,0) > 0) then
  begin
    update dokumpoz set ord = 1, numer = new.numer
      where alttopoz = new.ref
        and dokument = new.dokument
        and coalesce(fake,0) > 0;
  end
end^
SET TERM ; ^
