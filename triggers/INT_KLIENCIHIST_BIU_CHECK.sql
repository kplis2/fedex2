--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INT_KLIENCIHIST_BIU_CHECK FOR INT_KLIENCIHIST                
  ACTIVE BEFORE INSERT OR UPDATE POSITION 5 
as
declare variable komunikat string1024;
declare variable czy_firma smallint_id;
begin
  komunikat = '';
  if(coalesce(new.fskrot, '') = '')then
    new.fskrot = substring(new.nazwa from 1 for 40);
  if(old.krajid is distinct from new.krajid)then
  begin
    if(coalesce(new.krajid, 'PL') = 'PL') then
      new.zagraniczny = 0;
    else
      new.zagraniczny = 1;
  end
  if(coalesce(new.nazwa,'') = '') then
    komunikat = komunikat || 'nazwy ';
  if(coalesce(new.fskrot,'') = '') then
    komunikat = komunikat || 'skrotu ';
  if(coalesce(new.krajid,'') = '') then
    komunikat = komunikat || 'kraju ';
  --if(coalesce(new.miasto,'') = '') then
    --komunikat = komunikat || 'miasta ';
  --if(coalesce(new.kodp,'') = '' and coalesce(new.krajid,'') = 'PL') then  -- XXX KBI bo w ses brak jest identyfiaktora karaju dlatego wszystko idzie jako PL i wywala sie n azagranicznych
    --komunikat = komunikat || 'kodu pocztowego ';                          -- XXX KBI bo w ses brak jest identyfiaktora karaju dlatego wszystko idzie jako PL i wywala sie n azagranicznych
  if(coalesce(new.firma,0) = 1
    and coalesce(new.nip,'') = ''
  ) then
    komunikat = komunikat || 'NIP dla firmy';

  if(komunikat != '')then
    exception universal 'Brakuje: ' || komunikat;

--  if(coalesce(new.zagraniczny,0) = 0
--    and not exists(select first 1 1 from cpmiasta m where lower(m.opis) = lower(coalesce(new.miasto,'')))
--  )then
--    exception universal 'Miasto nie zgadza się z kodem pocztowym.';
end^
SET TERM ; ^
