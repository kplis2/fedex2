--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMELEMENTS_BU0 FOR PMELEMENTS                     
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if(new.status is null) then new.status = 0;
  if(new.calcmval is null) then new.calcmval = 0;
  if(new.calcsval is null) then new.calcsval = 0;
  if(new.calcw is null) then new.calcw = 0;
  if(new.calcwval is null) then new.calcwval = 0;
  if(new.calce is null) then new.calce = 0;
  if(new.calceval is null) then new.calceval = 0;
  if(new.calcsumval is null) then new.calcsumval = 0;
  if(new.calcval is null) then new.calcval = 0;
  if(new.budmval is null) then new.budmval = 0;
  if(new.budsval is null) then new.budsval = 0;
  if(new.budwval is null) then new.budwval = 0;
  if(new.budeval is null) then new.budeval = 0;
  if(new.budsumval is null) then new.budsumval = 0;
  if(new.budval is null) then new.budval = 0;
  if(new.budw is null) then new.budw =0;
  if(new.bude is null) then new.bude =0;
  if(new.realmval is null) then new.realmval = 0;
  if(new.realsval is null) then new.realsval = 0;
  if(new.realw is null) then new.realw = 0;
  if(new.reale is null) then new.reale = 0;
  if(new.realval is null) then new.realval = 0;
  if(new.realsumval is null) then new.realsumval = 0;
  if(new.realwval is null) then new.realwval =0;
  if(new.realeval is null) then new.realeval =0;
  if(new.realsumval is null) then new.realsumval =0;
  if(new.erealmval is null) then new.erealmval = 0;
  if(new.erealsval is null) then new.erealsval = 0;
  if(new.erealw is null) then new.erealw = 0;
  if(new.ereale is null) then new.ereale = 0;
  if(new.erealval is null) then new.erealval = 0;
  if(new.erealsumval is null) then new.erealsumval = 0;
  if(new.erealwval is null) then new.erealwval =0;
  if(new.erealeval is null) then new.erealeval =0;
  if(new.erealsumval is null) then new.erealsumval =0;
end^
SET TERM ; ^
