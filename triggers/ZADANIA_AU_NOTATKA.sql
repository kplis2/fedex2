--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZADANIA_AU_NOTATKA FOR ZADANIA                        
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable notref integer;
declare variable zadnazwa varchar(255);
begin
  if(not ((new.notatka is not NULL and old.notatka is NULL) or (new.notatka is NULL and old.notatka is not NULL))) then begin
    if(new.tworznotatke=1 and new.notatka is null) then begin
      notref = gen_id(GEN_CNOTATKI,1);
      select OPIS from DEFZADAN where REF=new.zadanie into :zadnazwa;
      insert into CNOTATKI(REF,NAZWA,DATA,TRESC,CPODMIOT,PKOSOBA,ZADANIE,OPERATOR)
      values (:notref,:zadnazwa,new.datawyk,new.opiswyk,new.cpodmiot,new.osoba,new.ref,new.operwyk);
      update ZADANIA set notatka = :notref where REF=new.ref;
    end else if(new.tworznotatke=1 and new.notatka is not null) then begin
      select OPIS from DEFZADAN where REF=new.zadanie into :zadnazwa;
      update CNOTATKI set NAZWA = :zadnazwa, DATA = new.datawyk, TRESC = new.opiswyk,
                          CPODMIOT = new.cpodmiot, PKOSOBA = new.osoba, OPERATOR = new.operwyk
      where REF = new.notatka;
    end else if(new.tworznotatke=0 and new.notatka is not null) then begin
      delete from CNOTATKI where ref = new.notatka;
    end
  end
end^
SET TERM ; ^
