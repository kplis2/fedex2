--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFDZIALATR_AD_REPLICAT FOR DEFDZIALATR                    
  ACTIVE AFTER DELETE POSITION 2 
as
begin
  if (old.dzial is not null) then
    update DZIALY set STATE=-2 where ref=old.dzial;
  if (old.klasa is not null) then
    update DEFKLASY set STATE=-2 where ref=old.klasa;
end^
SET TERM ; ^
