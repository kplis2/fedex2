--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERMAG_BU0 FOR DEFOPERMAG                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable aa integer;
begin
  if(new.MAG2 <> old.MAG2) then begin
    select count(*) from DEFMAGAZ where symbol=new.mag2 into :aa;
    if(:aa is null) then aa = 0;
    if(:aa = 0) then
      EXCEPTION DEFOPERMAG_INVALID_MAG2;
  end

end^
SET TERM ; ^
