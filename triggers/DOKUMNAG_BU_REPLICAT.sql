--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_BU_REPLICAT FOR DOKUMNAG                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.REF<>old.REF
      or (new.magazyn <> old.magazyn)
      or (new.okres <> old.okres )
      or (new.typ <> old.typ)
      or (new.symbol <> old.symbol ) or (new.symbol is not null and old.symbol is null) or (new.symbol is null and old.symbol is not null)
      or (new.data <> old.data ) or (new.data is not null and old.data is null) or (new.data is null and old.data is not null)
      or (new.dostawa <> old.dostawa ) or (new.dostawa is not null and old.dostawa is null) or (new.dostawa is null and old.dostawa is not null)
      or (new.dostawca <> old.dostawca ) or (new.dostawca is not null and old.dostawca is null) or (new.dostawca is null and old.dostawca is not null)
      or (new.klient <> old.klient ) or (new.klient is not null and old.klient is null) or (new.klient is null and old.klient is not null)
      or (new.refk <> old.refk ) or (new.refk is not null and old.refk is null) or (new.refk is null and old.refk is not null)
      or (new.mag2 <> old.mag2 ) or (new.mag2 is not null and old.mag2 is null) or (new.mag2 is null and old.mag2 is not null)
      or (new.symbolk <> old.symbolk ) or (new.symbolk is not null and old.symbolk is null) or (new.symbolk is null and old.symbolk is not null)
      or (new.wartosc <> old.wartosc ) or (new.wartosc is not null and old.wartosc is null) or (new.wartosc is null and old.wartosc is not null)
      or (new.wartkaucja <> old.wartkaucja ) or (new.wartkaucja is not null and old.wartkaucja is null) or (new.wartkaucja is null and old.wartkaucja is not null)
      or (new.wartsnetto <> old.wartsnetto ) or (new.wartsnetto is not null and old.wartsnetto is null) or (new.wartsnetto is null and old.wartsnetto is not null)
      or (new.wartsbrutto <> old.wartsbrutto ) or (new.wartsbrutto is not null and old.wartsbrutto is null) or (new.wartsbrutto is null and old.wartsbrutto is not null)
      or (new.akcept <> old.akcept ) or (new.akcept is not null and old.akcept is null) or (new.akcept is null and old.akcept is not null)
      or (new.dataakc <> old.dataakc ) or (new.dataakc is not null and old.dataakc is null) or (new.dataakc is null and old.dataakc is not null)
      or (new.operator <> old.operator ) or (new.operator is not null and old.operator is null) or (new.operator is null and old.operator is not null)
      or (new.oddzial <> old.oddzial ) or (new.oddzial is not null and old.oddzial is null) or (new.oddzial is null and old.oddzial is not null)
      or (new.wysylka <> old.wysylka ) or (new.wysylka is not null and old.wysylka is null) or (new.wysylka is null and old.wysylka is not null)
      or (new.slodef <> old.slodef ) or (new.slodef is not null and old.slodef is null) or (new.slodef is null and old.slodef is not null)
      or (new.slopoz <> old.slopoz ) or (new.slopoz is not null and old.slopoz is null) or (new.slopoz is null and old.slopoz is not null)
      or (new.kosztdost <> old.kosztdost ) or (new.kosztdost is not null and old.kosztdost is null) or (new.kosztdost is null and old.kosztdost is not null)
      or (new.blokadafak <> old.blokadafak ) or (new.blokadafak is not null and old.blokadafak is null) or (new.blokadafak is null and old.blokadafak is not null)
      or (new.symbfak <> old.symbfak ) or (new.symbfak is not null and old.symbfak is null) or (new.symbfak is null and old.symbfak is not null)
      ) then
   waschange = 1;

  execute procedure rp_trigger_bu('DOKUMNAG',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
