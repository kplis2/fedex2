--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMACTIVS_BI_RIGHTS FOR PMACTIVS                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  select READRIGHTS,WRITERIGHTS,READRIGHTSGROUP,WRITERIGHTSGROUP
  from PMPLANS where REF=new.pmplan
  into new.readrights,new.writerights,new.readrightsgroup,new.writerightsgroup;
end^
SET TERM ; ^
