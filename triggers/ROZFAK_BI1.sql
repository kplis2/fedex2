--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZFAK_BI1 FOR ROZFAK                         
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable bn char(1);
declare variable vat_val numeric(14,4);
declare variable kurs numeric(14,4);
declare variable vkurs numeric(14,4);
declare variable zakup integer;
declare variable zaliczkowy integer;
declare variable typ varchar(10);
declare variable korekta smallint;
declare variable korsamo smallint;
begin
  select bn, KURS, VKURS, zakup, zaliczkowy, typ
  from nagfak where nagfak.ref = new.dokument
  into :bn, :kurs, :vkurs, :zakup, :zaliczkowy, :typ;
  select korekta, korsamo from typfak where symbol=:typ into :korekta, :korsamo;
  /*kursy potrzebne tylko dla faktur zaliczkowych*/
  if(:kurs is null) then kurs = 1;
  if(:vkurs is null) then vkurs = 1;
  if(:bn is null) then select bn from typfak where symbol = :typ into :bn;
  select stawka from vat where grupa = new.vat into :vat_val;
  if(:vat_val is null) then vat_val = 0;
  /* z zewnatrz ustawiane jest zarowno brutto jak i netto */
  /* tutaj zaleznie od polityki jedna z tych wartosci jest przeliczana od nowa */
  /* oczywiscie liczymy tez vat */
  if(:bn = 'N') then begin
     if(:zakup=0 and :korsamo=1) then begin
       new.psumwartvat = cast((new.psumwartnet * (:vat_val/100)) as numeric(14,2));
       new.psumwartbru = new.psumwartnet + new.psumwartvat;
       new.psumwartvatzl = cast((new.psumwartnetzl * (:vat_val/100)) as numeric(14,2));
       new.psumwartbruzl = new.psumwartnetzl + new.psumwartvatzl;
       new.pesumwartvat = cast((new.pesumwartnet * (:vat_val/100)) as numeric(14,2));
       new.pesumwartbru = new.pesumwartnet + new.pesumwartvat;
       new.pesumwartvatzl = cast((new.pesumwartnetzl * (:vat_val/100)) as numeric(14,2));
       new.pesumwartbruzl = new.pesumwartnetzl + new.pesumwartvatzl;
     end else begin
       /*kopiowane z dokumentu korygowanego, wic nie ma co przeliczać*/
       new.psumwartvat = new.psumwartbru - new.psumwartnet;
       new.psumwartvatzl = new.psumwartbruzl - new.psumwartnetzl;
       new.pesumwartvat = new.pesumwartbru - new.pesumwartnet;
       new.pesumwartvatzl = new.pesumwartbruzl - new.pesumwartnetzl;
     end
     -- nowa metoda liczenia VATu, dla zwyklej faktury jest to samo, ale dla korekty liczy sie vat od wartosci korygowanej a nie po korekcie
     new.sumwartvat = cast(((new.sumwartnet-new.psumwartnet) * (:vat_val/100)) as numeric(14,2)) + new.psumwartvat;
     new.sumwartbru = new.sumwartnet + new.sumwartvat;
     new.sumwartvatzl = cast(((new.sumwartnetzl-new.psumwartnetzl) * (:vat_val/100)) as numeric(14,2)) + new.psumwartvatzl;
     new.sumwartbruzl = new.sumwartnetzl + new.sumwartvatzl;
  end else if(:bn = 'B') then begin
     if(:zakup=0 and :korsamo=1) then begin
       new.psumwartvat = cast((NEW.pSUMWARTBRU - new.psumwartbru /(1+ (:vat_val/100))) as numeric(14,2));
       new.psumwartnet = new.psumwartbru - new.psumwartvat;
       new.psumwartvatzl = cast((NEW.pSUMWARTBRUZL - new.psumwartbruZL /(1+ (:vat_val/100))) as numeric(14,2));
       new.psumwartnetzl = new.psumwartbruzl - new.psumwartvatzl;
       new.pesumwartvat = cast((NEW.peSUMWARTBRU - new.pesumwartbru /(1+ (:vat_val/100))) as numeric(14,2));
       new.pesumwartnet = new.pesumwartbru - new.pesumwartvat;
       new.pesumwartvatzl = cast((NEW.peSUMWARTBRUZL - new.pesumwartbruZL /(1+ (:vat_val/100))) as numeric(14,2));
       new.pesumwartnetzl = new.pesumwartbruzl - new.pesumwartvatzl;
     end else begin
       /*kopiowane z dokumentu korygowanego, wic nie ma co przeliczać*/
       new.psumwartvat = new.psumwartbru - new.psumwartnet;
       new.psumwartvatzl = new.psumwartbruzl - new.psumwartnetzl;
       new.pesumwartvat = new.pesumwartbru - new.pesumwartnet;
       new.pesumwartvatzl = new.pesumwartbruzl - new.pesumwartnetzl;
     end
     -- nowa metoda liczenia VATu, dla zwyklej faktury jest to samo, ale dla korekty liczy sie vat od wartosci korygowanej a nie po korekcie
     new.sumwartvat = cast(((new.sumwartbru-new.psumwartbru) - (new.sumwartbru-new.psumwartbru) /(1+ (:vat_val/100))) as numeric(14,2)) + new.psumwartvat;
     new.sumwartnet = new.sumwartbru - new.sumwartvat;
     new.sumwartvatzl = cast(((new.sumwartbruzl-new.psumwartbruzl) - (new.sumwartbruzl-new.psumwartbruzl) /(1+ (:vat_val/100))) as numeric(14,2)) + new.psumwartvatzl;
     new.sumwartnetzl = new.sumwartbruzl - new.sumwartvatzl;
  end
  if(new.esumwartnet is null) then new.esumwartnet = 0;
  if(new.esumwartbru is null) then new.esumwartbru = 0;
  if(new.esumwartnetzl is null) then new.esumwartnetzl = 0;
  if(new.esumwartbruzl is null) then new.esumwartbruzl = 0;
  if(new.vesumwartnetzl is null) then new.vesumwartnetzl = 0;
  if(new.vesumwartbruzl is null) then new.vesumwartbruzl = 0;
  if(:zaliczkowy>0) then begin
    new.esumwartvat = cast((NEW.ESUMWARTBRU - new.esumwartbru /(1+ (:vat_val/100))) as numeric(14,2));
    new.esumwartnet = new.esumwartbru - new.esumwartvat;
    new.esumwartbruzl = new.esumwartbru*:kurs;
    new.esumwartvatzl = new.esumwartvat*:kurs;
    new.esumwartnetzl = new.esumwartbruzl - new.esumwartvatzl;
    new.vesumwartbruzl = new.esumwartbru*:vkurs;
    new.vesumwartvatzl = new.esumwartvat*:vkurs;
    new.vesumwartnetzl = new.vesumwartbruzl - new.vesumwartvatzl;
  end else begin
    new.esumwartvat = new.esumwartbru - new.esumwartnet;
    new.esumwartvatzl = new.esumwartbruzl - new.esumwartnetzl;
    -- nowa metoda liczenia VATu, dla rozliczenia zaliczki jest tak jak bylo, ale dla korekty liczy sie vat od wartosci korygowanej a nie po korekcie
    if(:korekta=1) then begin
      if (:bn = 'N') then
        new.vesumwartvatzl = cast(((new.vesumwartnetzl-new.pvesumwartnetzl) * (:vat_val/100)) as NUMERIC(14,2)) + new.pvesumwartvatzl;
      else if (:bn ='B') then
        new.vesumwartvatzl = cast(((new.vesumwartbruzl-new.pvesumwartbruzl) - (new.vesumwartbruzl-new.pvesumwartbruzl)/(1+(:vat_val/100)))as numeric(14,2)) + new.pvesumwartvatzl;
    end else begin
      if (:bn = 'N') then
        new.vesumwartvatzl = cast((new.VESUMWARTNETZL * (:vat_val/100)) as NUMERIC(14,2));
      else if (:bn ='B') then
        new.vesumwartvatzl = cast((new.vesumwartbruzl - new.vesumwartbruzl/(1+(:vat_val/100)))as numeric(14,2));
    end
  end
  if(new.pesumwartnet is null) then new.pesumwartnet = 0;
  if(new.pesumwartbru is null) then new.pesumwartbru = 0;
  if(new.pesumwartnetzl is null) then new.pesumwartnetzl = 0;
  if(new.pesumwartbruzl is null) then new.pesumwartbruzl = 0;
  if(new.pvesumwartnetzl is null) then new.pvesumwartnetzl = 0;
  if(new.pvesumwartbruzl is null) then new.pvesumwartbruzl = 0;
  new.pesumwartvat = new.pesumwartbru - new.pesumwartnet;
  new.pesumwartvatzl = new.pesumwartbruzl - new.pesumwartnetzl;
  new.pvesumwartvatzl = cast((new.PVESUMWARTNETZL * (:vat_val/100)) as NUMERIC(14,2)) ;
end^
SET TERM ; ^
