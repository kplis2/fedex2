--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAK_BD FOR POZFAK                         
  ACTIVE BEFORE DELETE POSITION 0 
as
  declare variable akceptacja smallint;
  declare variable intrastatst smallint;
begin
  select akceptacja from nagfak where ref = old.dokument into :akceptacja;
  if(:akceptacja=1) then exception NAGFAK_AKCEPT;

  /* weryfikacja czy pozycja nie znajduje sie na nieotwartej deklaracji Intrastat */
  intrastatst = null;
  select coalesce(intrastath.statusflag,0)
    from intrastatp left join intrastath
      on intrastatp.intrastath = intrastath.ref
      where intrastatp.otable = 'POZFAK'
        and intrastatp.oref = old.dokument
        and intrastatp.sref = old.ref
    into :intrastatst;
  if (:intrastatst is not null and :intrastatst <> 0) then
    exception INTRASTAT_STATUS_CHECK;
end^
SET TERM ; ^
