--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKACCOUNTS_BD_CHECKRKDOKNAG FOR BANKACCOUNTS                   
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  -- sprawdzenie, czy rachunek nie zostal przypisany do jakiegos wyciagu
  if ( exists (
        select first 1 1 from rkdoknag r
          where old.eaccount = r.ticaccount
            and  old.dictdef = r.slodef
            and  old.dictpos = r.slopoz
         ) ) then
  begin
    exception universal 'Usunięcie niemożliwe. Rachunek przypisany do co najmniej jednego wyciągu bankowego.';
  end
end^
SET TERM ; ^
