--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYCEN_BI0 FOR STANYCEN                       
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable stminus integer;
declare variable typ_mag char(1);
declare variable dostdata timestamp;
declare variable dostdatawazn timestamp;
begin
  if(new.ref is null) then new.ref = gen_id(GEN_STANYCEN,1);
  if(new.wersjaref is null) then new.wersjaref = 0;
  if(new.wersjaref <> 0) then
    select nrwersji from WERSJE where ref=new.wersjaref into new.wersja;
  else if(new.ktm is not null and new.wersja is not null) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if(new.ilosc is null) then new.ilosc = 0;
  select ODDZIAL,STANYCEN,TYP from DEFMAGAZ where SYMBOL = new.magazyn
    into new.oddzial,:stminus,:typ_mag;
  if(:stminus is null) then stminus = 0;
  if(:stminus = 0 and new.ilosc<0) then
    exception STCEN_MINUS;
  if(new.cena is null) then new.cena = 0;
  if(typ_mag<>'S') then begin
    new.wartosc = new.ilosc * new.cena;
  end else if(new.ilosc<>0) then begin
    new.cena = new.wartosc/new.ilosc;
  end
  select data,datawazn from DOSTAWY where REF=new.dostawa into :dostdata,:dostdatawazn;
  if(new.data is null) then new.data = :dostdata;
  if(new.data is null) then new.data = current_date;
  if(new.dostawa is null) then new.dostawa = 0;
  else new.datawazn = :dostdatawazn;
  if(new.datawazn is not null) then new.datafifo = new.datawazn;
  else new.datafifo = new.data;
  if(new.zablokow is null) then new.zablokow = 0;
  if(new.zarezerw is null) then new.zarezerw = 0;
  if(new.zamowiono is null) then new.zamowiono = 0;
end^
SET TERM ; ^
