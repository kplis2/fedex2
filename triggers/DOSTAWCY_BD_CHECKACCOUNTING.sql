--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTAWCY_BD_CHECKACCOUNTING FOR DOSTAWCY                       
  ACTIVE BEFORE DELETE POSITION 0 
as
  declare variable dictdef integer;
begin
  select ref from slodef where typ = 'DOSTAWCY'
    into :dictdef;
  execute procedure check_dict_using_in_accounts(dictdef, old.kontofk, old.company);
end^
SET TERM ; ^
