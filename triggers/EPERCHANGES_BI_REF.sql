--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPERCHANGES_BI_REF FOR EPERCHANGES                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EPERCHANGES')
      returning_values new.REF;
end^
SET TERM ; ^
