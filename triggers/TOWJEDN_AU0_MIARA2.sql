--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWJEDN_AU0_MIARA2 FOR TOWJEDN                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable ilosc numeric(14,4);
begin
  if (new.jedn <> old.jedn or (new.przelicz <> old.przelicz) or (old.dodatkowa<>new.dodatkowa)) then
    begin
      if(new.dodatkowa<>old.dodatkowa and old.dodatkowa = 1) then
        update stanyil set iloscjm1 = null, miara1 = null
        where ktm = new.ktm;
      if(new.dodatkowa<>old.dodatkowa and old.dodatkowa = 2) then
        update stanyil set iloscjm2 = null, miara2 = null
        where ktm = new.ktm;
        if (new.dodatkowa = 1 and new.przelicz is not null and new.przelicz<>0) then
       begin
         update stanyil s set
           s.miara1 = new.jedn,
           s.iloscjm1 = s.ilosc / new.przelicz
         where s.ktm = new.ktm;
       end
      if (new.dodatkowa = 2 and new.przelicz is not null and new.przelicz<>0) then
        begin
          update stanyil s set
            s.miara2 = new.jedn,
            s.iloscjm2 = s.ilosc / new.przelicz
          where s.ktm = new.ktm;
        end
    end
end^
SET TERM ; ^
