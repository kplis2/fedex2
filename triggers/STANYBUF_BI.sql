--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYBUF_BI FOR STANYBUF                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.ref is null) then
    execute procedure GEN_REF('STANYZAM') returning_values new.ref;
  if(new.status is null) then new.status = 0;
end^
SET TERM ; ^
