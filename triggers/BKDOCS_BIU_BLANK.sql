--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_BIU_BLANK FOR BKDOCS                         
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.sumdebit is null) then new.sumdebit = 0;
  if (new.sumcredit is null) then new.sumcredit = 0;
  if (new.baldebit is null) then new.baldebit = 0;
  if (new.balcredit is null) then new.balcredit = 0;
  if (new.sumnetv is null) then new.sumnetv = 0;
  if (new.sumvatv is null) then new.sumvatv = 0;
  if (new.sumgrossv is null) then new.sumgrossv = 0;
  if (new.curnetval is null) then new.curnetval = 0;
  if (new.sumnet is null) then new.sumnet = 0;
  if (new.curvatrate is null) then new.curvatrate = new.currate;
  if (new.checked is null) then new.checked = 0;
  if (new.status is null) then new.status =0;
  if (new.sumvate is null) then new.sumvate = 0;
end^
SET TERM ; ^
