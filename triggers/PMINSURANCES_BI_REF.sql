--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMINSURANCES_BI_REF FOR PMINSURANCES                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF = 0) then
    execute procedure GEN_REF('PMINSURANCES') returning_values new.REF;
end^
SET TERM ; ^
