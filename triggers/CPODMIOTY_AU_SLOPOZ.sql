--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_AU_SLOPOZ FOR CPODMIOTY                      
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable slotyp varchar(40);
declare variable crm smallint;
begin
  select TYP from SLODEF where ref = new.slodef into :slotyp;
  if(:slotyp = 'ESYSTEM' and
     (new.skrot <> old.skrot or (new.skrot is not null and old.skrot is null) or (new.skrot is null and old.skrot is not null) or
      new.NAZWA <> old.NAZWA or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null) or
      new.kontofk <> old.kontofk or (new.kontofk is not null and old.kontofk is null) or (new.kontofk is null and old.kontofk is not null)
     )
   ) then begin
        update SLOPOZ set KOD = new.skrot, NAZWA = new.nazwa, KONTOKS = new.kontofk
        where REF=new.slopoz;
  end
  if (:slotyp='ESYSTEM') then begin
    select CRM from SLOPOZ where REF=new.slopoz into :crm;
    if(:crm='0') then begin
      update SLOPOZ set CRM=1
      where REF=new.slopoz;
    end
  end
end^
SET TERM ; ^
