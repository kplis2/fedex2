--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ETRAINNEEDS_BI_COST FOR ETRAINNEEDS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.costtype = 0) then  new.totalcost = 0;
  if (new.costtype = 1) then  new.personcost = 0;
end^
SET TERM ; ^
