--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECREDITDELAYS_BI0 FOR ECREDITDELAYS                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ECREDITDELAYS')
      returning_values new.REF;
end^
SET TERM ; ^
