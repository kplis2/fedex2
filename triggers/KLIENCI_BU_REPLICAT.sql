--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_BU_REPLICAT FOR KLIENCI                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.REF<>old.REF
   or (new.fskrot <> old.fskrot)
   or(new.id <> old.id ) or (new.id is null and old.id is not null) or (new.id is not null and old.id is null)
   or(new.firma <> old.firma ) or (new.firma is null and old.firma is not null) or (new.firma is not null and old.firma is null)
   or(new.nazwa <> old.nazwa ) or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null)
   or(new.imie <> old.imie ) or (new.imie is null and old.imie is not null) or (new.imie is not null and old.imie is null)
   or(new.nazwisko <> old.nazwisko ) or (new.nazwisko is null and old.nazwisko is not null) or (new.nazwisko is not null and old.nazwisko is null)
   or(new.grupa <> old.grupa ) or (new.grupa is null and old.grupa is not null) or (new.grupa is not null and old.grupa is null)
   or(new.nip <> old.nip ) or (new.nip is null and old.nip is not null) or (new.nip is not null and old.nip is null)
   or(new.regon <> old.regon ) or (new.regon is null and old.regon is not null) or (new.regon is not null and old.regon is null)
   or(new.ulica <> old.ulica ) or (new.ulica is null and old.ulica is not null) or (new.ulica is not null and old.ulica is null)
   or(new.nrdomu <> old.nrdomu ) or (new.nrdomu is null and old.nrdomu is not null) or (new.nrdomu is not null and old.nrdomu is null)
   or(new.nrlokalu <> old.nrlokalu ) or (new.nrlokalu is null and old.nrlokalu is not null) or (new.nrlokalu is not null and old.nrlokalu is null)
   or(new.miasto <> old.miasto ) or (new.miasto is null and old.miasto is not null) or (new.miasto is not null and old.miasto is null)
   or(new.kodp <> old.kodp ) or (new.kodp is null and old.kodp is not null) or (new.kodp is not null and old.kodp is null)
   or(new.poczta <> old.poczta ) or (new.poczta is null and old.poczta is not null) or (new.poczta is not null and old.poczta is null)
   or(new.kraj <> old.kraj ) or (new.kraj is null and old.kraj is not null) or (new.kraj is not null and old.kraj is null)
   or(new.telefon <> old.telefon ) or (new.telefon is null and old.telefon is not null) or (new.telefon is not null and old.telefon is null)
   or(new.fax <> old.fax ) or (new.fax is null and old.fax is not null) or (new.fax is not null and old.fax is null)
   or(new.email <> old.email ) or (new.email is null and old.email is not null) or (new.email is not null and old.email is null)
   or(new.www <> old.www ) or (new.www is null and old.www is not null) or (new.www is not null and old.www is null)
   or(new.uwagi <> old.uwagi ) or (new.uwagi is null and old.uwagi is not null) or (new.uwagi is not null and old.uwagi is null)
   or(new.dulica <> old.dulica ) or (new.dulica is null and old.dulica is not null) or (new.dulica is not null and old.dulica is null)
   or(new.dmiasto <> old.dmiasto ) or (new.dmiasto is null and old.dmiasto is not null) or (new.dmiasto is not null and old.dmiasto is null)
   or(new.dkodp <> old.dkodp ) or (new.dkodp is null and old.dkodp is not null) or (new.dkodp is not null and old.dkodp is null)
   or(new.dpoczta <> old.dpoczta ) or (new.dpoczta is null and old.dpoczta is not null) or (new.dpoczta is not null and old.dpoczta is null)
   or(new.dkraj <> old.dkraj ) or (new.dkraj is null and old.dkraj is not null) or (new.dkraj is not null and old.dkraj is null)
   or(new.limitkr <> old.limitkr ) or (new.limitkr is null and old.limitkr is not null) or (new.limitkr is not null and old.limitkr is null)
   or(new.sposplat <> old.sposplat ) or (new.sposplat is null and old.sposplat is not null) or (new.sposplat is not null and old.sposplat is null)
   or(new.upust <> old.upust ) or (new.upust is null and old.upust is not null) or (new.upust is not null and old.upust is null)
   or(new.waluta <> old.waluta ) or (new.waluta is null and old.waluta is not null) or (new.waluta is not null and old.waluta is null)
   or(new.kontofk <> old.kontofk ) or (new.kontofk is null and old.kontofk is not null) or (new.kontofk is not null and old.kontofk is null)
   or(new.dostawca <> old.dostawca ) or (new.dostawca is null and old.dostawca is not null) or (new.dostawca is not null and old.dostawca is null)
   or(new.bank <> old.bank ) or (new.bank is null and old.bank is not null) or (new.bank is not null and old.bank is null)
   or(new.rachunek <> old.rachunek ) or (new.rachunek is null and old.rachunek is not null) or (new.rachunek is not null and old.rachunek is null)
   or(new.sprzedawca <> old.sprzedawca ) or (new.sprzedawca is null and old.sprzedawca is not null) or (new.sprzedawca is not null and old.sprzedawca is null)
   or(new.przedstaw <> old.przedstaw ) or (new.przedstaw is null and old.przedstaw is not null) or (new.przedstaw is not null and old.przedstaw is null)
   or(new.faktoring <> old.faktoring ) or (new.faktoring is null and old.faktoring is not null) or (new.faktoring is not null and old.faktoring is null)
   or(new.aktywny <> old.aktywny ) or (new.aktywny is null and old.aktywny is not null) or (new.aktywny is not null and old.aktywny is null)
   or(new.kodzewn <> old.kodzewn ) or (new.kodzewn is null and old.kodzewn is not null) or (new.kodzewn is not null and old.kodzewn is null)
   or(new.flagi <> old.flagi ) or (new.flagi is null and old.flagi is not null) or (new.flagi is not null and old.flagi is null)
   or(new.vat <> old.vat ) or (new.vat is null and old.vat is not null) or (new.vat is not null and old.vat is null)
   or(new.niewyplac <> old.niewyplac ) or (new.niewyplac is null and old.niewyplac is not null) or (new.niewyplac is not null and old.niewyplac is null)
   or new.typ <> old.typ  or (new.typ is null and old.typ is not null) or (new.typ is not null and old.typ is null)
   or  new.komorka <> old.komorka  or (new.komorka is null and old.komorka is not null) or (new.komorka is not null and old.komorka is null)
 ) then
   waschange = 1;

  execute procedure rp_trigger_bu('KLIENCI',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
