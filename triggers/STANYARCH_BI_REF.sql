--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYARCH_BI_REF FOR STANYARCH                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('STANYARCH')
      returning_values new.REF;
end^
SET TERM ; ^
