--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPOSITIONS_BI_REF FOR PMPOSITIONS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF = 0) then
    execute procedure GEN_REF('PMPOSITIONS') returning_values new.REF;
end^
SET TERM ; ^
