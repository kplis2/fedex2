--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_AU_ROZLRACH FOR NAGFAK                         
  ACTIVE AFTER UPDATE POSITION 5 
as
declare variable slodefr integer;
declare variable slopozr integer;
declare variable kontofkr KONTO_ID;
declare variable symbfakr varchar(40);
declare variable winien numeric(14,2);
declare variable ma numeric(14,2);
declare variable waluta varchar(3);
declare variable cnt integer;
declare variable slodef integer;
declare variable slopoz integer;
declare variable kontofkkl KONTO_ID;
declare variable SYMBROZRACH varchar(30);
declare variable sposzapplat integer;
declare variable klient integer;
declare variable kontofk KONTO_ID;
declare variable tmp integer;
declare variable fsclracchref integer;
declare variable dataplat timestamp;
declare variable winiendek numeric(14,2);
declare variable madek numeric(14,2);
declare variable winienroz numeric(14,2);
declare variable maroz numeric(14,2);
declare variable konfigw varchar(20);
declare variable fsakcept smallint;
declare variable fsakcept2 smallint;
declare variable blokadarozliczen smallint;
declare variable numer smallint;
declare variable termzap timestamp;
declare variable fsclpos integer;
declare variable rozrachauto varchar(255);
declare variable settlement varchar(20);
begin
 select blokadarozliczen from typfak where typfak.symbol = new.typ
    into :blokadarozliczen;
  if(new.zakup = 0 and new.nieobrot = 0) then begin
    execute procedure GETCONFIG('ROZRACHAUTO') returning_values :rozrachauto;
    if(:rozrachauto is null or (:rozrachauto = '')) then rozrachauto = '0';
  end
  if(:blokadarozliczen is null) then blokadarozliczen = 0;
  if(:blokadarozliczen <> 1)  then  begin
  if(new.akceptacja = 1 and old.akceptacja <> 1) then begin
    if(new.zakup = 0) then begin
         select ref, prefixfk from SLODEF where TYP='KLIENCI' into :slodef, :kontofk;
         if(:slodef is null or (:slodef = 0)) then exception NAGFAK_BEZSLOKL;
         /*zamina klienta i slopoz, jesli przesila sposob platnosci*/
         if(new.sposzap > 0) then
           select POSREDNIKSTALY from PLATNOSCI where ref = new.sposzap into :sposzapplat;
         slopoz = new.platnik;
         klient = new.platnik;
         if(:sposzapplat is null or :sposzapplat = 0) then sposzapplat = :slopoz;
         select KONTOFK from KLIENCI where REF=:klient into :kontofkkl;
         symbrozrach = new.symbol;
    end else if(new.sad<>1) then begin
         select ref, prefixfk from SLODEF where TYP='DOSTAWCY' into :slodef, :kontofk;
         if(:slodef is null or (:slodef = 0)) then exception NAGFAK_BEZSLODOST;
         select KONTOFK from dostawcy where REF=new.dostawca into :kontofkkl;
         if(new.symbfak is null or (new.symbfak = '')) then exception NAGFA_BEZSYMBFAK;
         slopoz = new.dostawca;
         klient = NULL;
         sposzapplat = :slopoz;
         symbrozrach = new.symbfak;
    end else begin
         if(new.symbfak is null or (new.symbfak = '')) then exception NAGFA_BEZSYMBFAK;
         sposzapplat = :slopoz;
    end
    tmp = 0;
    fsclracchref = null;
    winienroz = 0;
    maroz = 0;
    winiendek = 0;
    madek = 0;
    kontofk = :kontofk||:kontofkkl;
    fsakcept = 0;
    fsakcept2 = 0;
    execute procedure get_config('SIDFK_ROZLKOMPEN',2) returning_values :konfigw;
    if(:konfigw is not null and :konfigw<>'') then begin
      fsakcept = cast(konfigw as smallint);
      fsakcept = fsakcept + 1; -- zgodnie z zalozeniami konfig zwiekszany o 1
      if(:fsakcept=4) then begin
        fsakcept = 1;
        fsakcept2 = 1; -- automatycznie akceptuj kompensate
      end
      fsakcept2 = 1; -- automatycznie akceptuj kompensate
    end
    for
      select SLODEF, SLOPOZ, KONTOFK, SYMBFAK, WALUTA, WINIEN, MA , PAYDAY
      from rozrachroz
      where ROZRACHROZ.doktyp = 'F' and ROZRACHROZ.dokref = new.ref
      into :slodefr, :slopozr, :kontofkr, :symbfakr, :waluta, :winien, :ma, :dataplat
    do begin
        tmp = tmp+1;
        if (:winien is not null) then
          winiendek = :winiendek + :winien;
        if (:ma is not null) then
          madek = :madek + :ma;
        if(:waluta is null or (:waluta = '')) then
          execute procedure getconfig('WALPLN') returning_values :waluta;
        fsclracchref = null;
        select fsclracch.ref
          from fsclracch
          where fsclracch.stable = 'NAGFAK' and fsclracch.sref = new.ref
          into :fsclracchref;
        if (:fsclracchref is null) then
        begin
          execute procedure GEN_REF('FSCLRACCH') returning_values :fsclracchref;
          insert into fsclracch(REF, DICTDEF, DICTPOS, STATUS, STABLE, SREF, currency, FSTYPEAKC, COMPANY)
                         values(:fsclracchref, :slodefr, :slopozr, 0, 'NAGFAK', new.ref, :waluta, :fsakcept, new.company);
        end
        execute procedure add_settlement_to_clracc(:fsclracchref, :kontofkr, :symbfakr, :slodefr,  :slopozr, :ma,  :winien);
    end
    if(:fsclracchref is not null) then begin
      select sum(currdebit2c), sum(currcredit2c)
        from fsclraccp
        where fsclraccp.fsclracch = :fsclracchref
      into  :madek, :winiendek;
      execute procedure add_settlement_to_clracc(:fsclracchref, :kontofk, :symbrozrach, :slodef,  :slopoz, :winiendek, :madek);
--SB akceptowanie kompensat
      select count(*)
        from fsclraccp P
        where P.fsclracch = :fsclracchref
        into :fsclpos;
      if(:fsakcept2=1 and :fsclpos > 1) then update fsclracch set status = 1 where ref = :fsclracchref;
    end
--SN platnosc na rozrachunkach wg pozycji
    if(:rozrachauto is not null and :rozrachauto <> '' and :rozrachauto <> '0'
       and not exists(select ref from rozrachroz where doktyp = 'F' and dokref = new.ref)
    ) then begin
      numer = 1;
      select kontofk from rozrach where faktura = new.ref into :kontofk;
      for select sum(p.wartbru - p.pwartbru), p.termzap
        from pozfak p
        where p.dokument = new.ref and p.termzap is not null
        group by p.termzap
        into :winien, :termzap
      do begin
        ma = 0;
        if(winien < 0) then begin
          ma = winien;
          winien = 0;
        end
        select settlementout from reg_settlement_name_create(new.symbol, :numer) into :settlement;
        insert into ROZRACH(company, SLODEF, SLOPOZ,KONTOFK, KLIENT,SYMBFAK,FAKTURA, DATAOTW,DATAPLAT,WALUTOWY,WALUTA, SKAD)
          values(new.company, new.slodef,new.slopoz, :kontofk, new.klient, :settlement, new.ref, new.data, :termzap, new.walutowa, new.waluta,new.skad);
        insert into ROZRACHP(company, SLODEF, SLOPOZ,kontofk,KLIENT,SYMBFAK,OPERACJA,FAKTURA,DATA,WINIEN,MA,skad)
          values(new.company, new.slodef, new.slopoz, :kontofk,new.klient,: settlement, new.symbol, new.ref,new.data, :winien, :ma,1);
        insert into ROZRACHP(company, SLODEF, SLOPOZ,kontofk,KLIENT,SYMBFAK,OPERACJA,FAKTURA,DATA,WINIEN,MA,skad)
          values(new.company, new.slodef, new.slopoz, :kontofk,new.klient,new.symbol, :settlement, new.ref,new.data, -:winien, -:ma,1);
        insert into rozrachroz(slodef, slopoz, kontofk, symbfak, doktyp, dokref,waluta, winien, ma, payday, stable)
          values (:slodef, new.klient, :kontofk, :settlement ,'F',new.ref,'',:ma, :winien, :termzap, 'POZFAK');
        numer = numer + 1;
      end
    end
  end else if(old.akceptacja =1 and new.akceptacja = 0)then begin
    --SN
    delete from rozrachroz r where r.doktyp = 'F' and r.dokref = new.ref and r.stable = 'POZFAK';
  end
  end
end^
SET TERM ; ^
