--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRACTSPOSDEF_REF FOR ECONTRACTSPOSDEF               
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ECONTRACTSPOSDEF')
      returning_values new.REF;
end^
SET TERM ; ^
