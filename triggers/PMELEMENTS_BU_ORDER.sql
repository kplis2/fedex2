--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMELEMENTS_BU_ORDER FOR PMELEMENTS                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 0;
  if (new.ord = 5) then exit;
  if (new.number is null) then new.number = old.number;
  if (new.ord = 1 or new.number = old.number) then
  begin
    new.ord = 0;
    exit;
  end
  --numerowanie obszarow w rzedzie gdy nie podamy numeru
  if (new.number <> old.number) then
    select max(number) from pmelements where pmplan = new.pmplan
      into :maxnum;
  else
    maxnum = 0;
  if (new.number < old.number) then
  begin
    update pmelements set ord = 1, number = number + 1
      where ref <> new.ref and number >= new.number
        and number < old.number and pmplan = new.pmplan;
  end else if (new.number > old.number and new.number <= maxnum) then
  begin
    update pmelements set ord = 1, number = number - 1
      where ref <> new.ref and number <= new.number
        and number > old.number and pmplan = new.pmplan;
  end else if (new.number > old.number and new.number > maxnum) then
    new.number = old.number;

end^
SET TERM ; ^
