--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCTYPES_AIU0 FOR BKDOCTYPES                     
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
as
begin
  if (new.isdefault = 1) then
  begin
    update bkdoctypes set isdefault = 0
      where company = new.company and bkreg = new.bkreg and isdefault = 1 and ref <> new.ref;
  end
end^
SET TERM ; ^
