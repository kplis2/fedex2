--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPL_BI_REF FOR CPL                            
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CPL')
      returning_values new.REF;
end^
SET TERM ; ^
