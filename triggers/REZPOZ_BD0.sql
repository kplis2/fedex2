--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER REZPOZ_BD0 FOR REZPOZ                         
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable zasob integer;
begin
  select REZZASOB from REZNAG where REF=old.reznag into :zasob;
  if(old.rezzasob=:zasob) then exception REZZASOBY_GLOWNY;
end^
SET TERM ; ^
