--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDES_BU_ORDER FOR PRSCHEDGUIDES                  
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.number<1) then
    new.number = 1;
  if (new.ord=1 and old.ord<>2) then
  begin
    if (new.number>old.number) then
      new.ord = 3;
    else if (new.number<old.number) then
      new.ord = 0;
  end
  if (new.ord=-1) then
    new.ord = 1;
end^
SET TERM ; ^
