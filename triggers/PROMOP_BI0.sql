--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROMOP_BI0 FOR PROMOP                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.gratis is null) then new.gratis = 1;
  if (new.iloscza < 0 or new.iloscza is null) then exception universal 'Niepoprawna ilość!';
  if (new.gratis=1 and new.cennik is not null) then exception universal 'Nie można wskazać cennika dla gratisu!';
  if (new.gratis=1 and new.rabat<>0) then exception universal 'Nie można podać rabatu dla gratisu!';
end^
SET TERM ; ^
