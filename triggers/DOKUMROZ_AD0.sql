--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMROZ_AD0 FOR DOKUMROZ                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable mag char(3);
declare variable wydania integer;
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable bktm varchar(40);
declare variable bwersja integer;
declare variable data timestamp;
declare variable dokument integer;
declare variable blokada smallint;
declare variable koropkrozid integer;
declare variable dokumnotref integer;
declare variable notilosc numeric(14,4);
declare variable fromdokumnotp integer;
declare variable notserial integer;
declare variable ilosc numeric(14,4);
declare variable curroperator integer;
declare variable ujemne smallint;
declare variable datadok timestamp;
declare variable stanyujemne smallint;
begin
 if(old.ack = 1 or old.ack = 9) then begin

   select defdokum.wydania, dokumnag.magazyn, DOKUMPOZ.KTM,DOKUMPOZ.wersja,DOKUMNAG.DATA,
     DOKUMNAG.REF,DOKUMNAG.BLOKADA, DOKUMPOZ.BKTM, DOKUMPOZ.BWERSJA, DEFDOKUM.STANYUJEMNE
   from DOKUMPOZ
   left join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.DOKUMENT)
   left join DEFDOKUM on(DOKUMNAG.TYP = DEFDOKUM.symbol)
    where DOKUMPOZ.ref = old.pozycja
    into :wydania,:mag,:ktm,:wersja,:data,
         :dokument, :blokada, :bktm, :bwersja, :stanyujemne;
   datadok = data;
   if(old.ack = 9) then begin
     ktm = :bktm;
     wersja = :bwersja;
   end
   if(:wydania = 0) then begin
      execute procedure REZ_BLOK_PRZESUN(:mag,:ktm,:wersja,old.cena, old.dostawa,old.ilosc);
      execute procedure REZ_BLOK_CHECK_ex(:mag,:ktm,:wersja,old.cena,old.dostawa,old.ilosc);
      execute procedure MINUS_SC(:mag,:ktm,:wersja,old.ilosc,old.cena,old.dostawa,old.serialnr,NULL,old.cena_koszt,old.orgdokumser,:datadok);
      -- kontrola mozliwosci zejscia na stany ujemne dla operatora
      select ILOSC from STANYIL where MAGAZYN=:mag and KTM=:ktm and WERSJA=:wersja into :ilosc;
      if(:ilosc<0) then begin
        execute procedure get_global_param('AKTUOPERATOR') returning_values :curroperator;
        select UJEMNE from OPERMAG where MAGAZYN=:mag and OPERATOR = :curroperator into :ujemne;
        if(:ujemne is null) then ujemne = 0;
        if(:ujemne=0) then exception STIL_MINUS 'Brak uprawnień operatora do zejścia na stan ujemny dla: '||:ktm;
        if(:stanyujemne is null) then stanyujemne = 0;
        if(:stanyujemne=0) then exception STIL_MINUS 'Brak uprawnień typu dok. do zejścia na stan ujemny dla: '||:ktm;
      end
      execute procedure REZ_DOST_DEREALIZE(:dokument,:ktm,:wersja,old.cena,old.dostawa,0);
   end else begin
      execute procedure PLUS_SC(:mag,:ktm,:wersja,old.ilosc,old.cena,old.dostawa,old.serialnr,NULL,old.cena_koszt,old.orgdokumser,:datadok);
      execute procedure REZ_BLOK_DEREALIZE(:dokument,:ktm,:wersja,old.cena,old.dostawa,0);
      if(:blokada <> 2) then begin
        execute procedure rez_blok_rozpisz(:mag, :ktm, :wersja);
      end
   end
   /* usun noty rozchodowe ale po odkreceniu stanow, aby wrocic w cenie po korekcie */
   if(exists(select ref from DOKUMNOTP where DOKUMROZKOR = old.ref)) then begin
     if(:wydania = 1 and old.cenatoroz is null) then begin
       for select DOKUMNOTP.dokument
         from DOKUMNOTP where DOKUMNOTP.dokumrozkor = old.ref
         into :dokumnotref
       do begin
         update dokumnot set akcept = 0 where ref = :dokumnotref;
         select ilosc,fromdokumnotp from dokumnotp where dokumnotp.dokumrozkor = old.ref and dokumnotp.dokument = :dokumnotref
         into :notilosc,:fromdokumnotp;
         if(:fromdokumnotp is not null) then
           update dokumnotp set stanmagchanged = stanmagchanged+:notilosc
             where dokumnotp.ref=:fromdokumnotp;
         delete from DOKUMNOTP where dokumnotp.dokumrozkor = old.ref and dokumnotp.dokument = :dokumnotref;
         if(exists(select ref from DOKUMNOTP where DOKUMENT = :dokumnotref)) then
           update dokumnot set akcept = 1 where ref = :dokumnotref;
         else
           delete from DOKUMNOT where ref=:dokumnotref;
       end
     end else
       exception DOKUMROZ_HASNOTKOR;
   end
   if(old.kortoroz > 0) then begin
     update DOKUMROZ set ILOSCL = ILOSCL +  old.ilosc where REF=old.kortoroz;
     /*korekta ilosciowa rozpiski rozliczajacej*/
     select opkrozid from DOKUMROZ where ref=old.kortoroz into :koropkrozid;
     if(:koropkrozid > 0) then
       execute procedure OPK_STANYAKTUREAL(:koropkrozid);
   end
   if(old.opkrozid is not null) then
     execute procedure OPK_STANYAKTUREAL(old.opkrozid);
 end
 if(old.ack <> 8 and old.ack <> 7) then
   if(old.serialnr <> '') then begin
       execute procedure SERNR_DEL_ROZ(old.pozycja, old.ilosc, old.serialnr);
   end
 update DOKUMPOZ set ILOSCL = ILOSCL - old.iloscl, ILOSCROZ = ILOSCROZ - old.ilosc  where ref=old.pozycja;
 if(old.blokoblnag = 0) then begin
   select dokument from DOKUMPOZ where ref=old.pozycja into :dokument;
   if(:dokument is not null) then
     execute procedure DOKMAG_OBL_WAR(:dokument,0);
 end
 update DOKUMROZ set CENATOROZ = null where CENATOROZ = old.ref;
end^
SET TERM ; ^
