--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKACCOUNTS_BD_CHKEMPLCONTRS FOR BANKACCOUNTS                   
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
--MWr: sprawdzenie, czy rachunek nie zostal przypisany do jakiejs umowy (np.UCP)

  if (exists(select first 1 1 from emplcontracts where bankaccount = old.ref)) then
    exception universal 'Usunięcie niemożliwe. Rachunek przypisany do co najmniej jednej umowy.';
end^
SET TERM ; ^
