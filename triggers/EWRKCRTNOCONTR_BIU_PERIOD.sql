--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWRKCRTNOCONTR_BIU_PERIOD FOR EWRKCRTNOCONTR                 
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
  declare variable fromdate date;
  declare variable todate date;
begin
  select fromdate, todate
    from eworkcertifs
    where ref = new.workcertif
    into :fromdate, :todate;
  if (:fromdate > new.fromdate or :todate < new.todate ) then
    exception okres_nieskladkowy;

  if (new.fromdate > new.todate) then
    exception data_do_musi_byc_wieksza;

  for
    select fromdate, todate
      from ewrkcrtnocontr
      where ref <> new.ref
        and workcertif = new.workcertif
      into :fromdate, :todate
  do begin
    if ((new.fromdate <= :todate and new.todate >= :todate)
        or (new.fromdate <= :fromdate and new.todate >= :fromdate) ) then
      exception data_oddo_na_zewnatrz;
  end
end^
SET TERM ; ^
