--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVREQUESTS_BI_REF FOR SRVREQUESTS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SRVREQUESTS')
      returning_values new.REF;
end^
SET TERM ; ^
