--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDOPERS_BU FOR PRSCHEDOPERS                   
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable eopertime varchar(255);
declare variable opertime numeric(14,4);
declare variable prsheet integer;
declare variable prcalc integer;
declare variable ret numeric(16,6);
declare variable amountrate float;
declare variable prdepart varchar(10);
declare variable amount numeric(14,4);
declare variable amountcur numeric(14,4);
declare variable tguide smallint;
declare variable reportedfactor numeric(14,4);
declare variable prschdate timestamp;
declare variable sql varchar(60);
declare variable amountin numeric(14,4);
declare variable amountshortages numeric(14,4);
declare variable tmpamount numeric(14,4);
begin
  if (new.realprecent is null) then new.realprecent = 0;
  if (new.calcrealprecent is null) then new.calcrealprecent = 0;
  if(new.starttime > new.endtime) then exception prschedopers_start_date;
  if(new.guide is null and new.machine is null) then exception prschedopers_nomachine;
  if(new.status <> old.status) then begin
    select pg.blocked from prschedguides pg
    where pg.ref = new.guide
    into :tguide;
  if(tguide>0) then exception prschedguidesblocks_expt;
  end
  if(new.guide = 0) then new.guide = null;
  if(new.amountin is null) then new.amountin = 0;
  if(new.amountresult is null) then new.amountresult = 0;
  if(new.amountshortages is null) then new.amountshortages = 0;
  if(new.unreported is null) then new.unreported = 0;
  if (new.prqcontrolmeth is null) then new.prqcontrolmeth = 0;
  if(new.verified  = 0 and (old.verified is null or old.verified > 0 )) then begin
  execute procedure prschedopers_calc_worktime(new.ref)
    returning_values new.worktime, new.oper;
  end
  if(coalesce(new.worktime,0) <> coalesce(old.worktime,0)) then
    execute procedure timediff2str(new.worktime) returning_values new.wortimestr;
  if (coalesce(new.amountresult,0) <> coalesce(old.amountresult,0)
    or coalesce(new.amountin,0) <> coalesce(old.amountin,0)
    or coalesce(new.amountshortages,0) <> coalesce(old.amountshortages,0)
    or coalesce(new.reportedfactor,0) <> coalesce(old.reportedfactor,0)
    or coalesce(new.calcrealprecent,0) <> coalesce(old.calcrealprecent,0)
  ) then
  begin
    if (coalesce(new.amountresult,0) <> coalesce(old.amountresult,0) and coalesce(new.checkprschedguidedets,1) = 1) then
    begin
      tmpamount = null;
      select sum(d.quantity)
        from prschedguidedets d
        where d.prschedguide = new.guide and d.prschedoper = new.ref and d.out = 0
          and d.byproduct = 0 and d.shortage = 0
        into tmpamount;
      if (tmpamount is null) then tmpamount = 0;
      if (coalesce(new.amountresult,0) < tmpamount) then
        exception prschedopers_error 'Próba zaraportowania mniejszej ilości niż zostało już rozpisane';
    end
    if (coalesce(new.amountshortages,0) + coalesce(new.amountresult,0) > coalesce(new.amountin,0)) then
      exception prschedopers_error 'Próba zaraportowania większej ilości niż jest na operacji '||new.amountin;
    new.realprecent = (new.amountresult + new.amountshortages) * 100;
    if (new.realprecent > 0) then
    begin
      select first 1 (p.amount  / p.kamountnorm /
          coalesce((select first 1 oo.reportedfactor from prschedopers oo
            where oo.guide = p.prschedguide order by oo.number desc),1))
        from prschedguidespos p
        where p.prschedguide = new.guide
        order by p.amount desc
        into :amountin;
      amountshortages = 0;
      tmpamount = null;
      for
        select min(o.amountshortages)
          from prsched_calc_next_oper(new.ref,null,1) p
            join prschedopers o on (o.ref = p.rprschedoper)
          where o.amountshortages <> 0
          group by o.ref
          into :tmpamount
      do begin
        if (tmpamount is null) then tmpamount = 0;
        amountshortages = :amountshortages + coalesce(:tmpamount,0);
        tmpamount = null;
      end
      amountin = coalesce(:amountin,0) - coalesce(:amountshortages,0);
      if (coalesce(:amountin,0) = 0) then
        new.realprecent = 100;
      else
        new.realprecent = new.realprecent / (:amountin * coalesce(new.reportedfactor,1));
    end else
      new.realprecent = 0;
  end
  if (new.calcrealprecent = 1) then new.calcrealprecent = 0;
--ustawianie znacznika reported w zaleznosci od activ
  if(new.activ = 0 and old.activ = 1) then begin
    new.machine = null;
    new.starttime = null;
    new.endtime = null;
    if (new.shoper is not null) then
      new.reported = 0;
  end else if (new.activ = 1 and old.activ = 0 and new.unreported = 0) then begin
    select ps.reported from prshopers ps where ps.ref = new.shoper into new.reported;
  end
--naliczanie amountresult i weryfikacja zgodnosci ilosci
  if(new.reported = 0 and (old.reported > 0 or new.amountin <> old.amountin)) then begin
    new.amountresult = new.amountin;
  end else if (new.reported > 0 and old.reported = 0) then begin
    if(new.activ = 0) then exception prschedopers_error 'Tylko operacja aktywna może być raportowana';
    execute procedure PRSCHEDOPER_AMOUNTIN_CALC(new.ref)
      returning_values(new.amountin);
    new.amountresult = 0;
  end
--zmiany statusów operacji
  if(new.status = 0 and old.status <> 0) then begin
/*    if(exists(select ref from prshortages where prschedoper = new.ref)
      or exists(select ref from propersraps where prschedoper = new.ref))
    then exception prschedopers_error substring('Do operacji '''||new.oper||''' wystawiono raport. Wycofanie niemożliwe' from 1 for 55);
*/  end else if(new.status = 3 and old.status <> 3) then
  begin
    select reportedfactor from prshopers where ref = new.shoper into :reportedfactor;
    if(reportedfactor is null or reportedfactor = 0) then reportedfactor = 1;
    if (reportedfactor <> 1 and new.activ = 1 ) then
      if (new.amountresult + new.amountshortages <> cast(new.amountin * :reportedfactor as integer)) then
        exception prschedopers_error substring('Il.zreal.('||new.amountresult||') różna od il. na wejściu('||new.amountin||'*'||:reportedfactor||') i braków('||new.amountshortages||')' from 1 for 75);
    else if (new.activ = 1) then
      if (new.amountresult + new.amountshortages <> new.amountin) then
        exception prschedopers_error substring('Il.zreal.('||new.amountresult||') różna od il. na wejściu('||new.amountin||'*'||:reportedfactor||') i braków('||new.amountshortages||')' from 1 for 75);
    if (new.serial = 1 and new.activ = 1) then
    begin
      select sum(s.ilosc)
        from propersraps r
          join dokumser s on (r.ref = s.refpoz and s.typ = 'R')
        where r.prschedoper = new.ref
        into :amount;

      if (new.amountresult <> coalesce(:amount,0)) then
        exception prschedopers_error 'Il.zreal.('||new.amountresult||') różna od il. nadanych num.ser('||coalesce(:amount,0)||')';
    end
    if (new.prqcontrolmeth = 3) then
    begin
      -- operacja wymaga kontroli po operacji, sprawdzam czy sa zarejestrowane parametry QJ
      if (exists (select first 1 1 from propersraps r
          join prschedopers op on (r.prschedoper = op.ref)
          join prshopers o on (o.ref = op.shoper)
          join prshmeasureacts a on (a.prshoper = o.ref)
          left join prschedopermeasureacts ra on (ra.propersrap = r.ref and ra.prshmeasureasct = a.ref)
        where op.ref = new.ref
          and a.ref is not null
          and ra.ref is null)
      ) then
        exception prschedopers_error 'Brak lub niepełny wpis z pomiarów kontrloli jakości';
    end
  end
--naliczanie czasow zakonczenia, jesli zminil sie czas rozpoczecia
  if((new.calcdeptimes in (1,3) and(
        (((new.starttime <> old.starttime) or (new.starttime is not null and old.starttime is null))and new.calcdeptimes = 1)
        or(((new.endtime <> old.endtime) or (new.endtime is not null and old.endtime is null))and new.calcdeptimes = 3)
        or (new.worktime <> old.worktime)))
    or (new.calcdeptimes = 2)
  )then begin
    select prschedules.prdepart from PRSCHEDULES where ref=new.schedule into :prdepart;
    --okreslenie czasu zakonczenia lub rozpoczecia operacji

    if(new.calcdeptimes = 3) then begin
      execute procedure pr_calendar_checkendtime_tr(:prdepart, new.endtime, new.worktime) returning_values new.starttime;
    end

    if(new.calcdeptimes = 1) then begin
      execute procedure pr_calendar_checkendtime(:prdepart, new.starttime, new.worktime) returning_values new.endtime;
      if(new.status >= 2 and new.endtime < current_timestamp(0)) then
       new.endtime = current_timestamp(0);
    end
    if (new.calcdeptimes = 2) then
      new.calcdeptimes = 1;
    --okreslenie, czy dana oepracja nie ma konfliktów maszyn lub poprzedników
    execute procedure PRSCHEDOPER_CHECKTIMECONFLICT(new.ref, new.schedule, new.machine, new.starttime, new.endtime) returning_values new.timeconflict;
    new.lastchangetime = current_time;
  end
  if(new.opermanadded = -1 and old.opermanadded > 0) then
    exception prschedopers_error substring('Operacja "'||new.oper||'" ma już zdefiniowaną oper.ręczną' from 1 for 75);
  if(new.shoper is null and new.opermanadded = -1) then
    exception prschedopers_error substring('Niedozwolone rejestrowanie operacji ręcznych do operacji ręcznych' from 1 for 75);
end^
SET TERM ; ^
