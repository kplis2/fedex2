--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPLANS_AU0 FOR PMPLANS                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if(new.displayname<>old.displayname) then begin
    update PMPLANS set PMPLANMASTERNAME = new.displayname where PMPLANMASTER=new.ref;
  end
  if(new.versionnum>1 and old.version<>new.version) then begin
    update pmplans set mainversion=new.version where mainref=new.ref;
  end
end^
SET TERM ; ^
