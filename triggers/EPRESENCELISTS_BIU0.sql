--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRESENCELISTS_BIU0 FOR EPRESENCELISTS                 
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.status is null) then new.status = 0;
  if ((new.description is null or new.description = '') and new.listsdate is not null) then
    new.description = 'Lista obecności na: '||cast(new.listsdate as date);
  if (new.listsdate is not null) then
    new.dayweek = CAST( extract( weekday from new.listsdate) as smallint);
end^
SET TERM ; ^
