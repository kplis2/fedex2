--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHOPERSWORKPLACES_AU0 FOR PRSHOPERSWORKPLACES            
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable prshopersworkplace integer;
begin
  if(new.main = 1 and old.main = 0 or
     new.main = 1 and (new.prworkplace <> old.prworkplace or new.opertime <> old.opertime or new.opertimepf <> old.opertimepf )) then begin

    update prshopersworkplaces p
      set p.main = 0
    where p.prshoper = new.prshoper and p.ref <> new.ref;
    -- przeciwdzialanie zapetlaniu triggerow
    if(coalesce(new.opertimeupdate,0) = 0) then
    begin
      update prshopers p
        set p.eopertime = case when old.prshoper_clone = 1 then p.eopertime else cast(new.opertime as varchar(20)) end,
          p.workplace = new.prworkplace,
          p.opertimepf = coalesce(new.opertimepf, 0),
          p.prmachine = new.prmachine,
          p.opertimeupdate = 1
      where p.ref = new.prshoper;

      update prshopers p
        set p.opertimeupdate = 1
      where p.ref = new.prshoper;
    end
  end
  else if(old.main = 1 and new.main = 0 and not exists (select p.ref from prshopersworkplaces p where p.main = 1 and p.prshoper = new.prshoper)) then begin
    select min(p.ref) from prshopersworkplaces p where p.prshoper = new.prshoper into :prshopersworkplace;
    if(coalesce(prshopersworkplace,0)>0) then

      update prshopersworkplaces p
        set p.main = 1
      where p.ref = :prshopersworkplace;

    else
    begin
     -- przeciwdziaania zaptlaniu triggerów
      if(coalesce(new.opertimeupdate,0) = 0) then
      begin

        update prshopers p
          set p.workplace = null,
              p.opertimepf = null ,
              p.eopertime = null ,
              p.prmachine = null,
              p.opertimeupdate = 1
        where p.ref = new.prshoper;
    
        update prshopers p
          set p.opertimeupdate = 0
        where p.ref = new.prshoper;
      end
    end
  end

end^
SET TERM ; ^
