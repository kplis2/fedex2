--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDS_AI0 FOR MWSSTANDS                      
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  if (new.mwsstandtype is not null) then
  begin
    -- trzeba zalozyc segmenty regalów
    insert into mwsstandsegs (mwsstandsegtype, mwsconstlocsq, l, w, wh, whsec,
        whsecrow, whsecdict, whsecrowdict, mwsstandnumber, mwsstand, mwsstandsegtypedict, whareatype)
      select ref, mwsconstlocsq, l, w, new.wh, new.whsec,
          new.whsecrow, new.whsecdict, new.whsecrowdict, new.number, new.ref, symbol, whareatype
        from mwsstandsegtypes
        where mwsstandtype = new.mwsstandtype
        order by symbol;
    -- trzeba zalozyc poziomy regalow
    insert into mwsstandlevels (mwsstand, loadcapacity, volume, l, h, w, levelheight, mwsstandleveltype, number,
        wh, whsec, whsecdict, whsecrow, whsecrowdict, mwsconstloctype, act, x_default_goodsav)   -- XXX KBI domylny goodsav
      select new.ref, loadcapacity, volume, l, h, w, levelheight, ref, number,
          new.wh, new.whsec, new.whsecdict, new.whsecrow, new.whsecrowdict, mwsconstloctype, act, x_default_goodsav   -- XXX KBI domylny goodsav
        from mwsstandleveltypes
        where mwsstandtype = new.mwsstandtype
        order by number;
  end
end^
SET TERM ; ^
