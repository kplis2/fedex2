--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EXPIMPFUNC_BD_ORDER FOR EXPIMPFUNC                     
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update expimpfunc set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and eigroup = old.eigroup;
end^
SET TERM ; ^
