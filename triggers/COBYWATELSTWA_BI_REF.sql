--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER COBYWATELSTWA_BI_REF FOR COBYWATELSTWA                  
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
   if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('COBYWATELSTWA')
      returning_values new.REF;
end^
SET TERM ; ^
