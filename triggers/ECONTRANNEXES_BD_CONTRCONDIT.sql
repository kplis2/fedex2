--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRANNEXES_BD_CONTRCONDIT FOR ECONTRANNEXES                  
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
/*
  Trigger usuwa warunki z aneksu, gdyz pomimo iz jest foreign key to musza one
  zostac usuniete wczesniej. Zmienna kontekstowa jest ustawiana tutaj i odczytywana
  na triggerze EMPLCONTRCONDIT_BD_LASTANNEXE aby zablokowac jego dzialanie
*/
  rdb$set_context('USER_TRANSACTION', 'DELETEFROMANNEXE', 1);
  delete from emplcontrcondit where annexe = old.ref;
  rdb$set_context('USER_TRANSACTION', 'DELETEFROMANNEXE', 0);
end^
SET TERM ; ^
