--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAKTERMPLAT_BI_REF FOR NAGFAKTERMPLAT                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('NAGFAKTERMPLAT')
      returning_values new.REF;
  if(new.doktyp = 'F') then new.nagfak = new.dokref;
end^
SET TERM ; ^
