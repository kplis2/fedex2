--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPAYROLLS_AI_EPREMPL FOR EPAYROLLS                      
  ACTIVE AFTER INSERT POSITION 0 
AS
  declare variable cond varchar(1024);
  declare variable fromdate date;
  declare variable todate date;
  declare variable eref integer;
  declare variable statement_text varchar(1300);
begin

  execute procedure efunc_prdates(new.ref, 0)
    returning_values fromdate, todate;

  select cond
    from eprollstypes
    where prtype = new.prtype
    into :cond;

  if (new.prtype <> 'UCP') then
  begin
    statement_text = 'select distinct ref from employees where company = ' || new.company;
    if (cond is not null and cond <> '') then
      statement_text = statement_text || ' and ' || :cond;

    for
      execute statement :statement_text
      into :eref
    do begin
      if(exists (select first 1 1 from emplcontracts                   --PR22136
           where employee = :eref
             and ((empltype = 1 and new.prtype <> 'PRN')
               or (empltype = 3 and new.prtype = 'PRN'))
             and fromdate <= :todate
             and ((enddate is not null and enddate >= :fromdate)
               or (enddate is null and (todate is null or todate >= :fromdate))))) then
        insert into eprempl (epayroll, employee)
          values (new.ref, :eref);
    end
  end else
  begin
    select employee
      from emplcontracts
      where ref = new.emplcontract
      into :eref;

    insert into eprempl (epayroll, employee)
      values (new.ref, :eref);
  end
end^
SET TERM ; ^
