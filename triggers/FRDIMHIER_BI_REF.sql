--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDIMHIER_BI_REF FOR FRDIMHIER                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF = 0) then
    execute procedure GEN_REF('FRDIMHIER') returning_values new.REF;
end^
SET TERM ; ^
