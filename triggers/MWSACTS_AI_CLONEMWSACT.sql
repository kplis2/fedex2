--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_AI_CLONEMWSACT FOR MWSACTS                        
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable clonoper smallint;
begin
  if (new.quantityc > 0 and new.quantityc > 0 and (new.status = 2 or new.status = 1)) then
  begin
    select mwsordtypes.clonoper
      from mwsordtypes
        join mwsords on (mwsords.mwsordtype = mwsordtypes.ref)
      where mwsords.ref = new.mwsord
      into clonoper;
    if (clonoper is null) then clonoper = 0;
    if (clonoper > 0 and new.quantity > new.quantityc) then
      -- klonowanie operacji - zmienamy status na 5 a status_change zalozy nowa operacje na ilosc niepotwierdzona
      update mwsacts set status = 5 where ref = new.ref;
    else if (new.quantity <= new.quantityc) then
      -- obsuga bez klonowania czyli zmieniamy status na 5 dopiero jak bedzie wiecej lub tyle samo potwierdzone
      update mwsacts set status = 5 where ref = new.ref;
  end
end^
SET TERM ; ^
