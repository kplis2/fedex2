--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VALDOCS_BI0 FOR VALDOCS                        
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable company integer;
begin
  new.regdate = CURRENT_TIMESTAMP(0);

  select company
    from fxdassets f
    where f.ref = new.fxdasset
  into :company;

  select ref from amperiods
    where ammonth = extract(month from new.docdate)
      and amyear = extract(year from new.docdate)
      and company = :company
    into new.amperiod;
  if (new.tvallim is null) then
    new.tvallim=0;
end^
SET TERM ; ^
