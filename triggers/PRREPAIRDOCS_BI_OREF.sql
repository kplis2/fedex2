--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRREPAIRDOCS_BI_OREF FOR PRREPAIRDOCS                   
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if(old.OREF<>new.OREF or old.OREF is null) then begin
    if(new.OREF='') then exception PRREPAIRDOCS_EXPT 'Należy wskazać dokument';
  end
end^
SET TERM ; ^
