--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHEETS_BU FOR PRSHEETS                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable opermastererrdesc varchar(255);
declare variable prsheetuseonnagzam varchar(30);
declare variable usluga smallint;
begin
  if (new.autodocin is null) then new.autodocin = 0;
  if (new.amountinfrom is null) then new.amountinfrom = 0;
  if(new.batchquantity is null) then new.batchquantity = 0;
  if((new.wersjaref <> old.wersjaref or (new.ktm is null) or (new.wersja is null)) and new.wersjaref is not null and new.wersjaref <> 0) then begin
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  end else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)or (new.wersjaref is null)) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if(new.wersjaref = 0) then new.wersjaref = null;
  if(new.shortage is null) then new.shortage = 0;
  --KS--
  if (new.ktm <> old.ktm) then
  begin
    select t.usluga from towary t where t.ktm = new.ktm into :usluga;
    if (usluga = 1) then
      new.amountinfrom = 2;
  end
  --koniec KS ---

-- weryfikacja czy można zatwierdzic karte
  if(new.status > 0 and old.status = 0) then begin
    opermastererrdesc = null;
    if(new.batchquantity = 0) then exception prsheets_error substring('Brak wskazania ilości partii. Zmiana statusu niemożliwa' from 1 for 70);
    select max(po.descript) from prshopers po where po.complex = 1 and po.sheet = new.ref and
      (not exists(select po2.ref from prshopers po2 where po2.opermaster = po.ref))
    into :opermastererrdesc;
    if(opermastererrdesc is not null and opermastererrdesc <> '') then
      exception prsheets_error substring('Błąd - operacja "'||substring(opermastererrdesc from 1 for 17)||'..." nie ma zdefinowanych oper.podrzędnych' from 1 for 70);
    select max(po.descript) from prshopers po where po.complex = 1 and po.opertype = 1 and po.sheet = new.ref and po.operdefault is null
    into :opermastererrdesc;
    if(opermastererrdesc is not null and opermastererrdesc <> '') then
      exception prsheets_error substring('Błąd - operacja "'||substring(opermastererrdesc from 1 for 17)||'..." nie ma zdef. dom. oper.podrz.' from 1 for 70);
  end
-- weryfikacja czy można odtwierdzic karte
  if(new.status = 0 and old.status > 0) then begin
    prsheetuseonnagzam = null;
    select max(n.id)
      from pozzam p
        left join nagzam n on (n.ref = p.zamowienie)
      where p.prsheet = new.ref
      into :prsheetuseonnagzam;
    if(:prsheetuseonnagzam is not null and :prsheetuseonnagzam <>'') then
      exception prsheets_error substring('Karta użyta na zleceniu '||:prsheetuseonnagzam||' .Odzatwierdzenie niemożliwe' from 1 for 70);
  end
end^
SET TERM ; ^
