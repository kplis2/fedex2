--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKREGS_AU0 FOR BKREGS                         
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if ((new.rights is not null and old.rights is null) or new.rights<>old.rights
    or (new.rightsgroup is not null and old.rightsgroup is null)
    or new.rightsgroup<>old.rightsgroup) then
  update bkdocs set rights=new.rights, rightsgroup=new.rightsgroup
    where bkreg=new.symbol;
end^
SET TERM ; ^
