--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONTAKTY_AI_DATANASTKONT FOR KONTAKTY                       
  ACTIVE AFTER INSERT POSITION 0 
AS
  declare variable zadaniaref integer;
  declare variable defzadanieref integer;
  declare variable eol varchar(10);
begin
  eol='
';
  if(new.datanastkont is not null) then begin
    select ref from defzadan where defzadan.domyslne = 1 into :defzadanieref;
    if (defzadanieref is null) then
      exception ZADANIE_DOMYSLNE_BRAK;
    else begin
      execute procedure GEN_REF('ZADANIA') returning_values :zadaniaref;
      insert into zadania  (ref, kontakt, zadanie, operzal, operwyk,
                            data, datado, stan, cpodmiot, osoba,
                            datazal, opis, prawa, prawagrup)
      values (:zadaniaref, new.ref, :defzadanieref, new.operator, new.operator,
              new.datanastkont, new.datanastkont, 0, new.cpodmiot, new.pkosoba,
              current_date, 'Zadanie założone automatycznie dla kontaktu z dnia '||new.nazwa ||:eol||'Treść kontaktu: '||new.opis,
              new.prawa,  new.prawagrup);
      update kontakty set refwygzad = :zadaniaref where ref = new.ref;
    end
  end
end^
SET TERM ; ^
