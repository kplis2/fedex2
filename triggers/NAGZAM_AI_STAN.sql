--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGZAM_AI_STAN FOR NAGZAM                         
  ACTIVE AFTER INSERT POSITION 1 
as
declare variable termdost date;
declare variable ilosc numeric(14,4);
declare variable status integer;
begin
  termdost = new.termdost;
  if(:termdost is null) then termdost = current_date;
  if(new.typ >=2) then ilosc = new.kilreal;
  else begin
       ilosc = new.kilosc; -- - new.ilonklonm;
  end
  /*
     if(new.blokadarez = 1) then
       ilosc = 0;*/
  if(new.kktm<>'' and ((new.KSTAN ='B') or (new.kstan = 'R') or (new.kstan = 'D'))) then begin
      execute procedure REZ_SET_KTM(new.ref, 0, new.kktm, new.kwersja, :ilosc, 0, 0,new.kstan,:termdost) returning_values :status;
       if(:status <> 1) then exception REZ_WRONG;
  end
end^
SET TERM ; ^
