--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTCEN_AI0 FOR DOSTCEN                        
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable stawka numeric(14,4);
declare variable przelicz numeric(14,4);
begin
  if(new.gl = 1) then --skasuj znacznik GL w pozostalych rekordach tego towaru
    update DOSTCEN set GL = 0 where GL = 1 and WERSJAREF = new.wersjaref and (DOSTAWCA <> new.dostawca or WALUTA <> new.waluta or ODDZIAL <> new.oddzial);
  if(new.cenazakgl = 1) then --skasuj znacznik CENAZAKGL w pozostalych rekordach tego towaru
    update DOSTCEN set CENAZAKGL = 0 where CENAZAKGL = 1 and WERSJAREF = new.wersjaref and (DOSTAWCA <> new.dostawca or WALUTA <> new.waluta or ODDZIAL <> new.oddzial);
  if(new.akt = 1) then --skasuj znacznik AKT w pozostalych rekordach tego towaru i dostawcy
    update DOSTCEN set AKT = 0 where AKT = 1 and WERSJAREF = new.wersjaref and DOSTAWCA = new.dostawca and (WALUTA <> new.waluta or ODDZIAL <> new.oddzial);
  if(new.cenazakgl = 1 ) then begin
    select STAWKA from TOWARY join VAT on (TOWARY.VAT = VAT.GRUPA) where TOWARY.KTM = new.ktm into :stawka;
    stawka = 1 + :stawka/100;
    if(new.jedn > 0) then
      select PRZELICZ from TOWJEDN where REF=new.jedn into :przelicz;
    else
      przelicz = 1;
    update WERSJE set CENA_ZAKN = new.cenanet/ :przelicz, CENA_ZAKB = new.cenanet * :stawka / :przelicz,
                      CENA_FABN = new.cena_fab / :przelicz, cena_FABB = new.cena_FAB * :stawka / :przelicz,
                      CENA_KOSZTN = new.cena_koszt / :przelicz, cena_KOSZTB = new.cena_koszt * :stawka / :przelicz
       where REF=new.wersjaref;
  end
  if(new.cennikzewn > 0) then
    update CENNIKIZEWN set KTM = new.ktm where ref = new.cennikzewn;
  if(new.gl = 1) then
    update WERSJE set wersje.symbol_dost = new.symbol_dost where REF=new.wersjaref;
end^
SET TERM ; ^
