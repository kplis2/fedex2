--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLOPER_AI0 FOR CPLOPER                        
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable proced varchar(255);
begin
  execute procedure CPL_OBLUCZEST(new.cpluczest,new.typpkt);
  if(new.typdok<> ''and new.refdok <> 0) then begin
    execute procedure CPL_OBLICZPROGLOJDONE(new.typdok, new.refdok);
  end
end^
SET TERM ; ^
