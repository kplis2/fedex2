--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDES_AU0 FOR PRSCHEDGUIDES                  
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable schgroupmain smallint;
declare variable prschedguidelastdecl integer;
declare variable prschedguidelastcur integer;
declare variable prschedoper integer;
declare variable prschedoperfirst integer;
declare variable amountreal numeric(14,4);
begin
  if(new.status > 0 and old.status = 0) then begin
    execute procedure prschedopers_statuscheck(new.prschedzam,new.ref, null);
  end
  else if(new.status = 0 and old.status > 0) then begin
    update prschedopers set status = 0 where guide = new.ref;
  end
  if(new.numer <> old.numer or (new.status <> old.status) ) then
    update PRSCHEDOPERS set prschedopers.guidenum = new.numer, guidestatus = new.status
      where prschedopers.guide = new.ref;
  -- po zmianie ilosci oczekiwanej dogenerowanie przewodników na reszte
  if (new.amount <> old.amount and new.pggamountcalc = 0) then
  begin
    update prschedopers o set o.worktime = (select worktime from prschedopers_calc_worktime(o.ref))
      where o.guide = new.ref;
    -- generowanie i kasowanie przewodników
    execute procedure prschedguidesgroup_amountcalc(new.zamowienie, new.pozzam, new.prschedzam);
  end
--ustawianie lastone
  if(new.lastone = 0
    and (new.starttime is null and old.starttime is not null or new.starttime is null and old.starttime is not null or new.starttime <> old.starttime
    or new.numer <> old.numer)
    or new.lastone = 1 and old.status = 0 and new.status > 0) then begin
    select first 1 ref from prschedguides where prschedzam = new.prschedzam and status = 0
      order by starttime desc nulls first, numer desc into :prschedguidelastdecl;
    select ref from prschedguides where prschedzam = new.prschedzam and lastone = 1 into :prschedguidelastcur;
    if(prschedguidelastdecl is null or prschedguidelastcur is null or :prschedguidelastdecl <> :prschedguidelastcur) then begin
      if(prschedguidelastdecl is null) then prschedguidelastdecl = new.ref;
      update prschedguides set lastone = 1 where ref = :prschedguidelastdecl;
      if(prschedguidelastcur is not null) then update prschedguides set lastone = 0 where ref = :prschedguidelastcur;
    end
  end
  if(new.status < 2
    and ((new.amount <> old.amount or new.amountdecl <> old.amountdecl))
  ) then begin
    update prschedguidespos set pggamountcalc = 1 where prschedguide = new.ref;
    update prschedguidespos set amount = (new.amount * kamountnorm) + amountshortage
      where prschedguide = new.ref;
    update prschedguidespos set pggamountcalc = 0 where prschedguide = new.ref;
    -- zmiana ilosci na operacji
    for
      select p.ref, p.amountresult + p.amountshortages
        from prschedopers p
          left join prschedoperdeps d on (p.ref = d.depto)
        where p.guide = new.ref and p.activ = 1 and d.ref is null and p.status < 2
        into prschedoperfirst, amountreal
    do begin
      if (amountreal > 0) then exception prschedopers_error 'Operacje są juz w trakcie realizacji';
      execute PROCEDURE PRSCHEDOPER_AMOUNTIN (prschedoperfirst);
    end
  end
  if(new.status <> old.status) then
    execute procedure prschedzam_obl(new.prschedzam);
  --ustawienie ilosci na 1 operacji gdy RW sa generowane z raportow
  if (old.status <> new.status and old.status = 0 and new.docfromoperrap > 0) then
  begin
    prschedoper = null;
    select first 1 o.ref
      from prschedopers o
      where o.guide = new.ref and o.reported >= 1 and o.activ = 1
      into prschedoper;
    if (prschedoper is not null) then
      execute procedure prschedoper_amountin(:prschedoper);
  end
  if (coalesce(new.amountzreal,0) <> coalesce(old.amountzreal,0)) then
    execute procedure POZZAM_OBL(new.pozzam);

  if (old.amount <> new.amount
    or old.amountzreal <> new.amountzreal
    or old.amountshortage <> new.amountshortage
    or old.status <> new.status
  ) then begin
    if( new.amount = new.amountzreal + new.amountshortage and new.status = 2) then
      execute procedure prschedguides_statuschange(new.ref, 3, null);
    else if (new.amount <> new.amountzreal + new.amountshortage and new.status = 3) then
      execute procedure prschedguides_statuschange(new.ref, 2, null);
  end
end^
SET TERM ; ^
