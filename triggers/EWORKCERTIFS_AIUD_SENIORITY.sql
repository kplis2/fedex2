--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWORKCERTIFS_AIUD_SENIORITY FOR EWORKCERTIFS                   
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 
AS
  declare variable ref integer;
  declare variable fromdate date;
  declare variable todate date;
  declare variable sy smallint;
  declare variable sm smallint;
  declare variable sd smallint;
  declare variable counter smallint;
begin
  --DS: przelicza staz dla nastepnego swiadectwa pracy nawet jezeli na biezacym nic sie nie zmienilo
  --sa sytuacje ze zmiany zachodza na nastepnym, mimo iz na biezacym nic sie nie zmienilo

  --znajduje nastepne swiadectwo pracy
  select first 1 w.ref, w.fromdate, w.todate
    from eworkcertifs w
    where w.employee = coalesce(new.employee, old.employee)
      and w.ref <> coalesce(new.ref, old.ref)
    --w kolejnosci wiekszy fromdate, todate, ref
      and   (w.fromdate > coalesce(new.fromdate, old.fromdate)
        or (w.fromdate = coalesce(new.fromdate, old.fromdate) and
           (w.todate > coalesce(new.todate, old.todate)
           or (w.todate = coalesce(new.todate, old.todate) and w.ref > coalesce(new.ref, old.ref)))))
    order by w.fromdate, w.todate, w.ref
    into ref, fromdate, todate;

  if (ref is not null) then
  begin
    execute procedure count_seniority(:ref, coalesce(new.employee, old.employee), :fromdate, :todate,1) returning_values sy, sm, sd;
    update eworkcertifs w set w.syears_zewn = :sy, w.smonths_zewn = :sm, w.sdays_zewn = :sd where w.ref = :ref;
  end
  else
  begin
    --zeby nie liczyc dla kazdego swiadectwa policzy tylko dla ostatniego
    --(bo zawsze przelicza wszystkie swiadectwa)
    sy=0; sm=0; sd=0;
    select sum(syears_zewn), sum(smonths_zewn), sum(sdays_zewn), count(ref)
      from eworkcertifs
      where employee = coalesce(new.employee, old.employee)
      into sy, sm, sd, counter;
    sy = coalesce(:sy,0);
    sm = coalesce(:sm,0);
    sd = coalesce(:sd,0);
    --jesli jest tylko jedno swiadectwo, nie uruchamiaj normalizacji
    --bo moze sie z 30 dni stazu zrobic jeden miesiac
    if (counter > 1) then
      execute procedure normalize_ymd(:sy, :sm, :sd) returning_values sy, sm, sd;
    update employees
      set syears = :sy, smonths = :sm, sdays = :sd
      where ref = coalesce(new.employee, old.employee);
  end
end^
SET TERM ; ^
