--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMSER_BI0 FOR DOKUMSER                       
  ACTIVE BEFORE INSERT POSITION 2 
as
declare variable ktm varchar(40);
declare variable ktm1 varchar(40);
declare variable serjedn smallint;
declare variable dokumserack smallint;
declare variable ref integer;
declare variable wydania smallint;
declare variable czyserialwydanie smallint;
declare variable status smallint;
declare variable cnt integer;
declare variable dokument integer;
declare variable akcept integer;
begin
  if(new.typ = 'M') then
    select KTM from DOKUMPOZ where REF=new.refpoz into :ktm;
  else if(new.typ = 'F') then
    select KTM from pozfak where REF=new.refpoz into :ktm;
  else if(new.typ = 'Z') then
    select KTM from pozzam where REF=new.refpoz into :ktm;
  else if (new.typ = 'R') then
    select g.ktm
      from propersraps r
        join prschedguides g on (g.ref = r.prschedguide)
      where r.ref = new.refpoz
      into :ktm;

  if (coalesce(new.numbergen,'') <> '') then
    execute procedure get_number(new.numbergen, new.genrej, new.gendok, current_date, null, new.gendokref)
      returning_values new.gennumber, new.odserial;

  select TOWARY.SERJEDN from TOWARY where KTM = :ktm into :serjedn;
  if(new.ilosc is null) then new.ilosc = 0;
  if(new.iloscroz is null) then new.iloscroz = 0;
  if(new.ord is null) then new.ord = 0;
  if(new.odserial is not null and new.odserial <> '' and (new.doserial is null or (new.doserial = '')) and new.ilosc = 0)then
  begin
    new.ilosc = 1;
  end
  if(new.odserialnr is not null) then new.odserial = new.odserialnr;
  if(new.ilosc > 0 and new.odserialnr is not null and :serjedn = 1) then
    new.doserialnr = new.odserialnr + new.ilosc-1;
  if(new.doserialnr is null and new.odserialnr is not null and :serjedn = 1) then new.doserialnr = new.odserialnr;
  if(new.odserialnr is null and new.doserialnr is not null) then new.odserialnr = new.doserialnr;
  if(new.doserialnr is not null) then new.doserial = new.doserialnr;
  if(new.odserialnr is not null and new.doserialnr is not null) then begin
    if(new.odserialnr > new.doserialnr) then exception DOKUMSER_WRONGNR;
    new.ilosc = new.doserialnr - new.odserialnr + 1;
  end
  dokumserack = 0;
  wydania = 0;
  if(new.typ = 'M') then begin
    select DEFMAGAZ.NOTSERIAL, DEFDOKUM.wydania, DOKUMNAG.akcept
      from DOKUMPOZ
      left join DOKUMNAG on (DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
      left join DEFMAGAZ on (DEFMAGAZ.SYMBOL = DOKUMNAG.MAGAZYN)
      left join DEFDOKUM on (DEFDOKUM.SYMBOL = DOKUMNAG.TYP)
      where DOKUMPOZ.REF = new.refpoz
      into :dokumserack, :wydania, :akcept;
-- MS: nie mozna redagowac nr seryjnych na zaakceptowanych dok w trybie serializacji na magazynie
    if(:dokumserack = 0 and :akcept = 1 and new.ord=0) then
      exception dokumnag_akcept;
    if(:dokumserack = 1 and :wydania = 1) then begin
      new.iloscroz = new.ilosc;
      ref = 0;
      select max(dokumser.ref)
        from dokumser
        join dokumpoz on (dokumser.refpoz = dokumpoz.ref and dokumpoz.ktm = :ktm)
        join DOKUMNAG on (DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
        join DEFDOKUM on (DEFDOKUM.SYMBOL = DOKUMNAG.TYP and DEFDOKUM.WYDANIA = 0)
        where dokumser.odserial = new.odserial and dokumser.ref <> new.ref and dokumser.iloscroz = 0
        into :ref;
      if(:ref>0) then begin
        update dokumser set dokumser.iloscroz = dokumser.ilosc where dokumser.ref = :ref;
      end else begin
        select serwydanie from towary where ktm=:ktm into :czyserialwydanie;
        if (czyserialwydanie = 0) then begin
          exception DOKUMSER_WRONGNR 'Brak wolnego podanego numeru seryjnego w bazie.';
        end
      end
    end
  end
  if (new.doserialnr is not null and new.doserialnr >= new.odserialnr) then begin
    execute procedure dokumser_rozbij(new.ref,new.refpoz,new.typ,new.numer,new.ord,
        new.odserial,new.doserial,new.odserialnr,new.doserialnr,new.ilosc,new.iloscroz,new.orgdokumser)
      returning_values :status;
    if (status = 1) then begin
      new.doserial = '';
      new.odserialnr = null;
      new.doserialnr = null;
      new.ilosc = 1;
    end
  end
  if (:dokumserack = 1 and new.iloscroz = 0) then begin
    select dokumpoz.ktm,dokumpoz.dokument from dokumpoz where dokumpoz.ref = new.refpoz into :ktm1,:dokument;
    cnt = 0;
    select count(dokumser.ref) from dokumser left join dokumpoz on dokumpoz.ref = dokumser.refpoz
           where dokumpoz.dokument=:dokument and dokumpoz.ktm = :ktm1 and dokumser.odserial = new.odserial into :cnt;
    if (cnt > 0) then
      exception DOKUMSER_WRONGNR 'Numer seryjny '||new.odserial||' powtórzony na tym dokumencie.';
  end
  if(new.ilosccopy is null) then new.ilosccopy = 0;
end^
SET TERM ; ^
