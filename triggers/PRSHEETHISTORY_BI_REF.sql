--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHEETHISTORY_BI_REF FOR PRSHEETHISTORY                 
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('PRSHEETHISTORY')
      returning_values new.REF;
end^
SET TERM ; ^
