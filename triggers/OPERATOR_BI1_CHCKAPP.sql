--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OPERATOR_BI1_CHCKAPP FOR OPERATOR                       
  ACTIVE BEFORE INSERT POSITION 1 
AS
begin
  if(coalesce(new.applications,'') = '')
   then exception universal 'Brak wskazania aplikacji do których operator ma dostep';
end^
SET TERM ; ^
