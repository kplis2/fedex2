--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDLEVELS_BI_REF FOR MWSSTANDLEVELS                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSSTANDLEVELS') returning_values new.ref;
end^
SET TERM ; ^
