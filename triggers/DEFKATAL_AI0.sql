--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFKATAL_AI0 FOR DEFKATAL                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable typ integer;
declare variable wartosc varchar(100);
declare variable nazwa varchar(255);
declare variable numer integer;
begin
  if(new.cecha is not null and new.typ = 'C' and new.automat = 1) then begin
    /* naliczenie domylne cech */
    select typ from DEFCECHY where SYMBOL=new.cecha into :typ;
    if(:typ = 0 or :typ =1) then begin
      numer = 1;
      for select opis, wartnum from DEFCECHW where cecha = new.cecha order by wartnum into :nazwa, :wartosc do begin
         nazwa = substring(:nazwa from 1 for 100);
         insert into DEFELEM(KATALOG, NUMER, NAZWA, WARTOSC)
           values(new.ref, :numer, :nazwa, :wartosc);
         numer = :numer + 1;
      end
    end
    if(:typ = 2) then begin
      numer = 1;
      for select opis, wartnum from DEFCECHW where cecha = new.cecha order by wartnum into :nazwa, :wartosc do begin
         nazwa = substring(:nazwa from 1 for 100);
         insert into DEFELEM(KATALOG, NUMER, NAZWA, WARTOSC)
           values(new.ref, :numer, :nazwa, :wartosc);
         numer = :numer + 1;
      end
    end
    if(:typ = 3 or (:typ = 4)) then begin
      numer = 1;
      for select opis, wartdate from DEFCECHW where cecha = new.cecha order by wartdate into :nazwa, :wartosc do begin
         nazwa = substring(:nazwa from 1 for 100);
         insert into DEFELEM(KATALOG, NUMER, NAZWA, WARTOSC)
           values(new.ref, :numer, :nazwa, :wartosc);
         numer = :numer + 1;
      end
    end
  end
end^
SET TERM ; ^
