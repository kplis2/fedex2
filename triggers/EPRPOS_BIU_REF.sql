--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRPOS_BIU_REF FOR EPRPOS                         
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EPRPOS')
      returning_values new.REF;
end^
SET TERM ; ^
