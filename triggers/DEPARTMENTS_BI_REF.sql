--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEPARTMENTS_BI_REF FOR DEPARTMENTS                    
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DEPARTMENTS')
      returning_values new.REF;
end^
SET TERM ; ^
