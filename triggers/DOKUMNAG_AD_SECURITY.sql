--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_AD_SECURITY FOR DOKUMNAG                       
  ACTIVE AFTER DELETE POSITION 5 
AS
declare variable opis varchar(1024);
declare variable skrot varchar(255);
declare variable wydania smallint;
begin
  if (old.akcept = 0 and old.wasdeakcept = 1 and old.numer = -1) then begin
    select wydania from defdokum where symbol = old.typ into :wydania;
    if (:wydania is null) then wydania = 0;
    if (old.klient is not null and :wydania = 1) then
      select fskrot from klienci where ref = old.klient into :skrot;
    else if (old.dostawca is not null and :wydania = 0) then
      select id from dostawcy where ref = old.dostawca into :skrot;
    else
      skrot = '';
    opis = old.oldsymbol || '; ' || old.magazyn || '; ' ||:skrot || '; ' || old.data;
    insert into security_log(logfil, tablename, item, opis)
      values('DOKUMNAG', 'DOKUMNAG', 'DELETE', :opis);
  end
end^
SET TERM ; ^
