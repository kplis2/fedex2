--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMCONTRACTS_BI_REF FOR PMCONTRACTS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF = 0) then
    execute procedure GEN_REF('PMCONTRACTS') returning_values new.REF;

end^
SET TERM ; ^
