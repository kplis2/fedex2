--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRVMANUALPOS_BI_REF FOR FRVMANUALPOS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
   if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FRVMANUALPOS')
      returning_values new.REF;
END^
SET TERM ; ^
