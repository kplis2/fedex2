--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANREJ_BU0 FOR STANREJ                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable zak smallint;
begin
  if(new.stansprzed is not null) then new.stanfaktyczny = new.stansprzed;
  else new.stanfaktyczny = '';
  select ZAKUP from REJFAK where SYMBOL = new.REJFAK into :zak;
  select ZAKUPU from STANSPRZED where  STANOWISKO = new.stansprzed into new.zakupu;
  if(:zak <> new.zakupu) then
    exception STANTYPDAFAK_ZAKNIESG;
end^
SET TERM ; ^
