--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_TOWARY_AU_DANEDOD2WERS FOR TOWARY                         
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if (new.x_mws_typ_lokacji is distinct from old.x_mws_typ_lokacji) then
    update wersje
      set x_mws_typ_lokacji=new.x_mws_typ_lokacji
      where ktm=new.ktm
        and coalesce(x_mws_typ_lokacji,0)=coalesce(old.x_mws_typ_lokacji,0);

  if (new.x_mws_partie is distinct from old.x_mws_partie) then
    update wersje
      set x_mws_partie=new.x_mws_partie
      where ktm=new.ktm
        and coalesce(x_mws_partie,0)=coalesce(old.x_mws_partie,0);

  if (new.x_mws_serie is distinct from old.x_mws_serie) then
    update wersje
      set x_mws_serie=new.x_mws_serie
      where ktm=new.ktm
        and coalesce(x_mws_serie,0)=coalesce(old.x_mws_serie,0);  

  if (new.x_mws_slownik_ehrle is distinct from old.x_mws_slownik_ehrle) then
    update wersje
      set x_mws_slownik_ehrle=new.x_mws_slownik_ehrle
      where ktm=new.ktm
        and coalesce(x_mws_slownik_ehrle,0)=coalesce(old.x_mws_slownik_ehrle,0);
end^
SET TERM ; ^
