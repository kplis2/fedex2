--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DZIALY_BI FOR DZIALY                         
  ACTIVE BEFORE INSERT POSITION 1 
AS
BEGIN
  execute procedure REPLICAT_STATE('DZIALY',new.state, new.ref, NULL,NULL,NULL) returning_values new.state;
  if (new.grupa = '') then new.grupa = null;
END^
SET TERM ; ^
