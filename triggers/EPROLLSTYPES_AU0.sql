--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPROLLSTYPES_AU0 FOR EPROLLSTYPES                   
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (coalesce(new.isaddit,0) <> coalesce(old.isaddit,0)) then
    update epayrolls set isaddit = new.isaddit where prtype = new.prtype;
end^
SET TERM ; ^
