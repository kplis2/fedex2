--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOCTOBKDOCS_BI0 FOR DOCTOBKDOCS                    
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable nieksiegowy integer;
begin
  if (new.param2 is null) then new.param2 = '';
  if (new.bkreg='') then new.bkreg = null;
  if (new.vatreg='') then new.vatreg = null;
  if (new.taxgr='') then new.taxgr = null;
  if (new.taxgr is null and new.typ = 0) then
    exception BKDOCS_ZAKUP_MA_GRUPE;
  if(new.typ=1) then begin
    select nieksiegowy from defmagaz where symbol = new.param2 into :nieksiegowy;
    if(:nieksiegowy=1) then exception doctobkdocs_nieksiegowy;
  end
end^
SET TERM ; ^
