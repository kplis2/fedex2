--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CRMCLAIMBILLS_BU FOR CRMCLAIMBILLS                  
  ACTIVE BEFORE UPDATE POSITION 0 
as

declare variable prow numeric(14,2);
declare variable profit numeric(14,2);
declare variable clientamount numeric(14,2);
begin
  Select CK.prowizja  from crmclaimbills CB
    left join crmclaims CR on (CB.crmclaim = CR.ref)
    left join ckontrakty CK on (CK.ref = CR.ckontrakt)
    where CB.ref = new.ref
    into :prow ;

  profit =   new.amount * (prow / 100);
  clientamount = new.amount * ((100 - prow) / 100);

  new.profit = :profit ;
  new.clientamount = :clientamount ;

end^
SET TERM ; ^
