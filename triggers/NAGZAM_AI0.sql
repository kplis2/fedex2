--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGZAM_AI0 FOR NAGZAM                         
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable ildoprod numeric(14,4);
begin
  if(new.kkomplet =1 and new.org_ref >0) then
    execute procedure NAGZAM_OBL(new.org_ref);
  if(new.kkomplet = 1 and new.kpopref > 0) then
    execute procedure POZZAM_OBL(new.kpopref);
  if(new.faktura > 0) then
    execute procedure NAGFAK_SYMBOLZAM(new.faktura);
  if(new.kktm is not null and new.kktm <>'' and
   new.prschedule is not null and new.planscol is not null) then
     execute procedure plans_knagzam_plus(new.ref);
  execute procedure NAGZAM_TERMSTARTDOST(new.ref);
  if (new.kpopref is not null and new.kpopprzam is null) then
  begin
    select sum(n.kilosc)
      from nagzam n
      where n.kpopref = new.kpopref
        and n.kpopprzam is null
      into :ildoprod;
    update pozzam p set p.ildoprod = :ildoprod
      where p.ref = new.kpopref;
  end
  if (new.dostawca is not null) then
    INSERT INTO NAGFAKTERMPLAT (DOKTYP, DOKREF, TERMZAP, PROCENTPLAT, SPOSPLAT)
      select 'Z', new.ref, new.datawe +k.dniplat, k.procentplat, k.sposplat from KONTRAHTERMPLAT k where k.slodef = 6 and k.slopoz = new.dostawca;
  else if(new.klient is not null) then
    INSERT INTO NAGFAKTERMPLAT (DOKTYP, DOKREF, TERMZAP, PROCENTPLAT, SPOSPLAT)
      select 'Z', new.ref, new.datawe +k.dniplat, k.procentplat, k.sposplat from KONTRAHTERMPLAT k where k.slodef = 1 and k.slopoz = new.klient;
end^
SET TERM ; ^
