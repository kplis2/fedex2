--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCENREG_BI_REF FOR DEFCENREG                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DEFCENREG')
      returning_values new.REF;
end^
SET TERM ; ^
