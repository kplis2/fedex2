--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTAWY_BU0 FOR DOSTAWY                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable skrot varchar(40);
declare variable symbfak varchar(80);
declare variable okres varchar(6);
declare variable procent numeric(14,7);
declare variable ildni integer;
declare variable dostsymbfak varchar(20);
declare variable symbkonc varchar(80);
begin
  if(new.indywidualdost is null) then new.indywidualdost = 0;
  if(new.data <> old.data or (new.okres is null or (new.okres = ''))) then begin
    okres = cast( extract( year from new.data) as char(4));
    if(extract(month from new.data) < 10) then
      okres = :okres ||'0' ||cast(extract(month from new.data) as char(1));
    else
      okres = :okres ||cast(extract(month from new.data) as char(2));
    new.okres = :okres;
  end

  if((new.magazyn <> old.magazyn) or(new.magazyn is null and old.magazyn is not null) or(new.magazyn is not null and old.magazyn is null))then
   if(not(new.status ='O' ))then new.magazyn=old.magazyn;  --exception DOSTAWA_ZMIANA_MAG_ZLA;
end^
SET TERM ; ^
