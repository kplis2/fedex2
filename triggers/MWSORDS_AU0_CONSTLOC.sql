--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDS_AU0_CONSTLOC FOR MWSORDS                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable symbol varchar(20);
declare variable locref integer;
begin
  if (new.mwsfirstconstlocpref is null and new.status = 1) then
  begin
    select first 1 mc.symbol, mc.ref, mc.distfromstartarea
      from mwsacts ma
        left join mwsconstlocs mc on(ma.mwsconstlocp = mc.ref)
      where ma.mwsord = new.ref
      order by ma.number
      into new.mwsfirstconstlocp, new.mwsfirstconstlocpref, new.distfromstartarea;
  end
end^
SET TERM ; ^
