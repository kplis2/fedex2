--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DZIALATR_BI_REF FOR DZIALATR                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DZIALATR')
      returning_values new.REF;
end^
SET TERM ; ^
