--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKSZABL_BI_REF FOR DOKSZABL                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DOKSZABL')
      returning_values new.REF;
end^
SET TERM ; ^
