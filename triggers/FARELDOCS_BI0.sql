--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FARELDOCS_BI0 FOR FARELDOCS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable purchasedate timestamp;
begin
  select F.purchasedt
    from fxdassets F
    where F.ref = new.fxdasset
    into :purchasedate;
  if (new.docdate < purchasedate or new.operdate < purchasedate) then
    exception FARELDOCS_DATE;

  if (exists(
    select ref
      from fareldocs
      where fxdasset = new.fxdasset and docdate > new.docdate
    )) then
    exception FARELDOCS_DATE 'Nie można wystawić dokumentu z datą wcześniejszą niż już istniejące.';

  if (exists(
    select ref
      from fareldocs
      where fxdasset = new.fxdasset and operdate > new.operdate
    )) then
    exception FARELDOCS_DATE 'Nie można wystawić dokumentu z datą operacji wcześniejszą niż już istniejące.';

  if (new.operiod is null) then
  begin
    select ref from amperiods
      where ammonth = extract(month from new.operdate) and amyear = extract(year from new.operdate)
      into new.operiod;
  end

  if ( -- sprawdzanie czy wprowadzono jakiekolwiek zmiany
    ((new.branch is null and new.obranch is null) or (new.branch is not null and new.obranch is not null and new.obranch = new.branch))
      and ((new.department is null and new.odepartment is null) or (new.department is not null and new.odepartment is not null and new.department = new.odepartment))
      and ((new.compartment is null and new.ocompartment is null) or (new.compartment is not null and new.ocompartment is not null and new.compartment = new.ocompartment))
      and ((new.person is null and new.operson is null) or (new.person is not null and new.operson is not null and new.person = new.operson))
      and ((new.tbksymbol is null and new.otbksymbol is null) or (new.tbksymbol is not null and new.otbksymbol is not null and new.tbksymbol = new.otbksymbol))
      and ((new.fbksymbol is null and new.ofbksymbol is null) or (new.fbksymbol is not null and new.ofbksymbol is not null and new.fbksymbol = new.ofbksymbol))
  ) then
    exception FARELDOCS_NOCHANGE;

  execute procedure get_global_param('AKTUOPERATOR') returning_values new.regoper;
  new.regdate = current_timestamp(0);
end^
SET TERM ; ^
