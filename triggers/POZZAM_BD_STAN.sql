--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_BD_STAN FOR POZZAM                         
  ACTIVE BEFORE DELETE POSITION 1 
as
declare variable stan char(1);
declare variable termdost date;
declare variable status integer;
declare variable org_ref integer;
declare variable typ integer;
declare variable reff integer;
begin
     select TYP, cast(TERMDOST as date), REF, ORG_REF  from NAGZAM where ref=old.zamowienie into :TYP,:termdost, :reff, :org_ref;
     if(:termdost is null) then termdost = current_date;
     if(:org_ref is null) then org_ref = 0;
     if(:reff is not null and old.blokadarez = 0)then begin
       if((old.STAN ='B') or (old.stan = 'R') or (old.stan = 'D')) then begin
          execute procedure REZ_SET_KTM(old.zamowienie, old.ref, old.ktm, old.wersja, 0,old.cenamag, old.dostawamag, 'N',:termdost) returning_values :status;
          if(:status <> 1) then begin
            exception REZ_WRONG;
          end
       end
     end
   delete from STANYREZ where ZAMOWIENIE = old.zamowienie and  POZZAM=old.ref;
end^
SET TERM ; ^
