--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CFAZY_BU FOR CFAZY                          
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.aktywne <> old.aktywne) then
    update srvrequests set aktywne = new.aktywne
      where srvrequests.status = new.ref;
end^
SET TERM ; ^
