--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_BU_ORDER FOR RKDOKPOZ                       
  ACTIVE BEFORE UPDATE POSITION 2 
as
  declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 1;
  if (new.lp is null) then new.lp = old.lp;
  if (new.ord = 0 or new.lp = old.lp) then
  begin
    new.ord = 1;
    exit;
  end
  --numerowanie obszarow w rzedzie gdy nie podamy lp
  if (new.lp <> old.lp) then
    select max(lp) from rkdokpoz where dokument = new.dokument
      into :maxnum;
  else
    maxnum = 0;
  if (new.lp < old.lp) then
  begin
    update rkdokpoz set ord = 0, lp = lp + 1
      where ref <> new.ref and lp >= new.lp
        and lp < old.lp and dokument = new.dokument;
  end else if (new.lp > old.lp and new.lp <= maxnum) then
  begin
    update rkdokpoz set ord = 0, lp = lp - 1
      where ref <> new.ref and lp <= new.lp
        and lp > old.lp and dokument = new.dokument;
  end else if (new.lp > old.lp and new.lp > maxnum) then
    new.lp = old.lp;
end^
SET TERM ; ^
