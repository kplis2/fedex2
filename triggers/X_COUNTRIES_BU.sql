--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_COUNTRIES_BU FOR COUNTRIES                      
  INACTIVE BEFORE UPDATE POSITION 10 
as
begin
  --[PM] XXX nie ma zmiany symbolu ani nazwy kraju!!!
  if (old.symbol is distinct from new.symbol
      or old.name is distinct from new.name
     ) then
     exception universal 'Nie wolno zmieniac skrotu ani nazwy kraju! Skontaktuj sie z Sente.';

end^
SET TERM ; ^
