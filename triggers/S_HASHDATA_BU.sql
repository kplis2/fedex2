--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_HASHDATA_BU FOR S_HASHDATA                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable nr integer;
begin
  if(new.datatype<>old.datatype) then begin
    -- numer w nowym
    select max(number)+1 from s_hashdata where datatype=new.datatype into :nr;
    if(:nr is null) then nr = 1;
    new.number = :nr;
    -- numer w starym
    nr = NULL;
    select max(number) from s_hashdata where datatype=old.datatype into :nr;
    if(:nr is not null and :nr<>old.number) then
      update s_hashdata set number = old.number where datatype=old.datatype and number=:nr;
  end
end^
SET TERM ; ^
