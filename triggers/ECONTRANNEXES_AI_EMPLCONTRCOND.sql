--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRANNEXES_AI_EMPLCONTRCOND FOR ECONTRANNEXES                  
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable econtranex econtrannexes_id;
begin
  select first 1 a.ref
    from econtrannexes a
    where a.emplcontract = new.emplcontract and a.ref <> new.ref
    order by a.emplcontract, a.fromdate desc
    into :econtranex;
  if (:econtranex is not null) then
    insert into emplcontrcondit (contrcondit, emplcontract, annexe, val_text,
        workstart, workend, val_date, val_numeric, dictdef, dictpos, workstartstr, workendstr)
      select c.contrcondit, c.emplcontract, new.ref, c.val_text,
        c.workstart, c.workend, c.val_date, c.val_numeric, c.dictdef, c.dictpos, c.workstartstr, c.workendstr
        from emplcontrcondit c
        where c.annexe = :econtranex;
  else
    insert into emplcontrcondit (contrcondit, emplcontract, annexe, val_text,
        workstart, workend, val_date, val_numeric, dictdef, dictpos, workstartstr, workendstr)
      select c.contrcondit, c.emplcontract, new.ref, c.val_text,
        c.workstart, c.workend, c.val_date, c.val_numeric, c.dictdef, c.dictpos, c.workstartstr, c.workendstr
        from emplcontrcondit c
        where c.emplcontract = new.emplcontract;
end^
SET TERM ; ^
