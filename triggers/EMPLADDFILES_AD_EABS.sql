--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLADDFILES_AD_EABS FOR EMPLADDFILES                   
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable mdc integer;
begin

  execute procedure get_config('VACMDCOLUMN', 2) returning_values :mdc;
  if(exists(select first 1 1
              from eabsences
              where ecolumn = :mdc
              and correction in (0,2)
              and employee = old.employee
              and fromdate >= old.fromdate
              and todate <= coalesce(old.todate,current_timestamp(0)))) then
      exception universal'Usunięcie niemożliwe. W danym okresie są zarejestrowane nieobecności';
end^
SET TERM ; ^
