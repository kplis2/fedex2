--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRORDANALMAT_BI_REF FOR PRORDANALMAT                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null) then
    execute procedure gen_ref('PRORDANALMAT') returning_values new.ref;
end^
SET TERM ; ^
