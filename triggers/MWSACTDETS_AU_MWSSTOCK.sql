--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTDETS_AU_MWSSTOCK FOR MWSACTDETS                     
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  -- status 0 -> niezzakceptowano +++++ brak obslugi bo niepotrzebna
  -- status 1 -> do realizacji
  -- status 2 -> w realizacji
  -- status 3 -> zawieszono
  -- status 4 -> zablokowano  +++++ brak obslugi bo niepotrzebna
  -- status 5 -> potwierdzono (zrealizowano)
  -- status 6 -> anulowano +++++ brak obslugi bo niepotrzebna
  if (old.status > 0 and old.status <> new.status) then
  begin
    -- ZMIANY NA LOKACJACH WYNIKAJACE Z ODKRECANIA STATUSU OPERACJI
    if (old.status = 1 or old.status = 2) then
    begin
      --obsluga lokacji końcowej
      if (new.mwspallocl is not null) then
        execute procedure MINUS_ORDERED_MWSSTOCK(old.wh,old.whsec,old.wharea,old.good,
          old.vers,old.mwsconstlocl,old.mwspallocl,old.quantity,old.lot,old.stancen,
          old.x_partia, old.x_serial_no, old.x_slownik); -- XXX KBI
      --obsluga lokacji poczatkowej
      if (old.mwspallocp is not null) then
        execute procedure MINUS_BLOCKED_MWSSTOCK(old.wh,old.whsec,old.wharea,old.good,
          old.vers,old.mwsconstlocp,old.mwspallocp,old.quantity,old.lot,old.stancen,
          old.x_partia, old.x_serial_no, old.x_slownik); -- XXX KBI
    end
    else if (old.status = 3) then
    begin
      --obsluga lokacji końcowej
      if (old.mwspallocl is not null) then
        execute procedure MINUS_ORDSUSPENDED_MWSSTOCK(old.wh,old.whsec,old.wharea,old.good,
          old.vers,old.mwsconstlocl,old.mwspallocl,old.quantity,old.lot,old.stancen,
          old.x_partia, old.x_serial_no, old.x_slownik); -- XXX KBI
      --obsluga lokacji pocyatkowej
      if (new.mwspallocp is not null) then
        execute procedure MINUS_SUSPENDED_MWSSTOCK(old.wh,old.whsec,old.wharea,old.good,
          old.vers,old.mwsconstlocp,old.mwspallocp,old.quantity,old.lot,old.stancen,
          old.x_partia, old.x_serial_no, old.x_slownik); -- XXX KBI
    end
    else if (old.status = 5) then
    begin
      --obsluga lokacji końcowej
      if (old.mwspallocl is not null) then
        execute procedure MINUS_QUANTITY_MWSSTOCK(old.wh,old.whsec,old.wharea,old.good,
          old.vers,old.mwsconstlocl,old.mwspallocl,old.quantityc,old.lot,old.stancen,
          old.x_partia, old.x_serial_no, old.x_slownik); -- XXX KBI
      --obsluga lokacji pocyatkowej
      if (old.mwspallocp is not null) then
        execute procedure PLUS_QUANTITY_MWSSTOCK(old.wh,old.whsec,old.wharea,old.good,
          old.vers,old.mwsconstlocp,old.mwspallocp,old.quantityc,old.lot,old.stancen,
          old.x_partia, old.x_serial_no, old.x_slownik); -- XXX KBI
    end
  end
  if (new.status > 0 and old.status <> new.status) then
  begin
    -- ZMIANY NA LOKACJACH WYNIKAJACE ZE STATUSU OPERACJI
    if (new.status = 1 or new.status = 2) then
    begin
      --obsluga lokacji końcowej
      if (new.mwspallocl is not null) then
        execute procedure PLUS_ORDERED_MWSSTOCK(new.wh,new.whsec,new.wharea,new.good,
          new.vers,new.mwsconstlocl,new.mwspallocl,new.quantity,new.lot,new.stancen,
          new.x_partia, new.x_serial_no, new.x_slownik); -- XXX KBI
      --obsluga lokacji pocyatkowej
      if (new.mwspallocp is not null) then
        execute procedure PLUS_BLOCKED_MWSSTOCK(new.wh,new.whsec,new.wharea,new.good,
          new.vers,new.mwsconstlocp,new.mwspallocp,new.quantity,new.lot,new.stancen,
          new.x_partia, new.x_serial_no, new.x_slownik); -- XXX KBI
    end
    else if (new.status = 3) then
    begin
      --obsluga lokacji końcowej
      if (new.mwspallocl is not null) then
        execute procedure PLUS_ORDSUSPENDED_MWSSTOCK(new.wh,new.whsec,new.wharea,new.good,
          new.vers,new.mwsconstlocl,new.mwspallocl,new.quantity,new.lot,new.stancen,
          new.x_partia, new.x_serial_no, new.x_slownik); -- XXX KBI
      --obsluga lokacji pocyatkowej
      if (new.mwspallocp is not null) then
        execute procedure PLUS_SUSPENDED_MWSSTOCK(new.wh,new.whsec,new.wharea,new.good,
          new.vers,new.mwsconstlocp,new.mwspallocp,new.quantity,new.lot,new.stancen,
          new.x_partia, new.x_serial_no, new.x_slownik); -- XXX KBI
    end
    else if (new.status = 5) then
    begin
      --obsluga lokacji końcowej
      if (new.mwspallocl is not null) then
        execute procedure PLUS_QUANTITY_MWSSTOCK(new.wh,new.whsec,new.wharea,new.good,
          new.vers,new.mwsconstlocl,new.mwspallocl,new.quantityc,new.lot,new.stancen,
          new.x_partia, new.x_serial_no, new.x_slownik); -- XXX KBI
      --obsluga lokacji pocyatkowej
      if (new.mwspallocp is not null) then
        execute procedure MINUS_QUANTITY_MWSSTOCK(new.wh,new.whsec,new.wharea,new.good,
          new.vers,new.mwsconstlocp,new.mwspallocp,new.quantityc,new.lot,new.stancen,
          new.x_partia, new.x_serial_no, new.x_slownik); -- XXX KBI
    end
  end
end^
SET TERM ; ^
