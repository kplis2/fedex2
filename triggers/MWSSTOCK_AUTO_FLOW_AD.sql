--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTOCK_AUTO_FLOW_AD FOR MWSSTOCK                       
  INACTIVE AFTER DELETE POSITION 0 
as
declare variable X_ROW_TYPE smallint_id;
declare variable X_FLOW_ORDER smallint_id;
declare variable STATUS SMALLINT_ID;
declare variable MSG STRING1024;
declare variable actuoperator smallint_id;
begin
  select mc.x_row_type, mc.x_flow_order
    from mwsconstlocs mc
    where mc.ref = old.mwsconstloc
  into :x_row_type, :x_flow_order;
  if (x_row_type = 1 and x_flow_order = 5) then begin
    if (not exists(select first 1 1 from mwsstock mo
    where mo.mwsconstloc = old.mwsconstloc
      and mo.quantity > 0
      and mo.ref <> old.ref)) then begin
        select g.pvalue
          from globalparams g
          where g.psymbol = 'AKTUOPERATOR'
            and g.connectionid = current_connection
        into :actuoperator;
        execute procedure x_mws_auto_flow(old.mwsconstloc, actuoperator)
          returning_values status, msg;
    end
  end
end^
SET TERM ; ^
