--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INWENTA_BI_REF FOR INWENTA                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('INWENTA')
      returning_values new.REF;
end^
SET TERM ; ^
