--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RELATIONS_BIU0 FOR RELATIONS                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.quantityfrom is null) then new.quantityfrom = 0;
  if (new.quantityto is null) then new.quantityto = 0;
  if (new.quantitytoreal is null) then new.quantitytoreal = 0;
  if (new.act is null) then new.act = 1;
  if (new.realpriority is null) then new.realpriority = 100;
end^
SET TERM ; ^
