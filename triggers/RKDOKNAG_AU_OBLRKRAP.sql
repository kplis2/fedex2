--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_AU_OBLRKRAP FOR RKDOKNAG                       
  ACTIVE AFTER UPDATE POSITION 2 
AS
begin
  if ((old.kwota <> new.kwota) or (new.kwotazl <> old.kwotazl) or (new.pm <> old.pm) or
    (new.anulowany <> old.anulowany) or (new.numer = 0 and old.numer > 0 )
    or (new.numer is null and old.numer > 0) or (coalesce(old.status,0) <> coalesce(new.status,0)) ) then
    execute procedure RKRAP_OBL_WAR(new.raport);
end^
SET TERM ; ^
