--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRDVERSIONS_BI_DECREE FOR PRDVERSIONS                    
  ACTIVE BEFORE INSERT POSITION 2 
AS
declare variable bkperiod varchar(6);
declare variable bkdocsymbol varchar(20);
begin
 select first 1 b.symbol, b.period from decrees d join bkdocs b on (b.ref = d.bkdoc)
   join prdaccounting p on (p.ref = new.prdaccounting)
    where d.period>=new.fromperiod and d.prdaccounting = new.prdaccounting and d.ref<>p.decree
 into :bkdocsymbol, :bkperiod;
 if(coalesce(:bkdocsymbol,'') <> '') then
   exception prd_deducted 'Istnieje powiązany dokument RMK ' || :bkdocsymbol || ' (' || :bkperiod || '). Nie można dodać!';
end^
SET TERM ; ^
