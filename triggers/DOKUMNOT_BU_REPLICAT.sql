--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNOT_BU_REPLICAT FOR DOKUMNOT                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.REF<>old.REF
    or (new.oddzial <> old.oddzial ) or (new.oddzial is not null and old.oddzial is null) or (new.oddzial is null and old.oddzial is not null)
    or (new.typ <> old.typ)
    or (new.okres <> old.okres )
    or (new.magazyn <> old.magazyn)
    or (new.mag2 <> old.mag2 ) or (new.mag2 is not null and old.mag2 is null) or (new.mag2 is null and old.mag2 is not null)
    or (new.symbol <> old.symbol ) or (new.symbol is not null and old.symbol is null) or (new.symbol is null and old.symbol is not null)
    or (new.data <> old.data ) or (new.data is not null and old.data is null) or (new.data is null and old.data is not null)
    or (new.dokumnagkor <> old.dokumnagkor ) or (new.dokumnagkor is not null and old.dokumnagkor is null) or (new.dokumnagkor is null and old.dokumnagkor is not null)
    or (new.wartosc <> old.wartosc ) or (new.wartosc is not null and old.wartosc is null) or (new.wartosc is null and old.wartosc is not null)
    or (new.uwagi <> old.uwagi ) or (new.uwagi is not null and old.uwagi is null) or (new.uwagi is null and old.uwagi is not null)
    or (new.faktura <> old.faktura ) or (new.faktura is not null and old.faktura is null) or (new.faktura is null and old.faktura is not null)
    or (new.akcept <> old.akcept ) or (new.akcept is not null and old.akcept is null) or (new.akcept is null and old.akcept is not null)
    or (new.dataakc <> old.dataakc ) or (new.dataakc is not null and old.dataakc is null) or (new.dataakc is null and old.dataakc is not null)
    or (new.wasdeakcept <> old.wasdeakcept ) or (new.wasdeakcept is not null and old.wasdeakcept is null) or (new.wasdeakcept is null and old.wasdeakcept is not null)
    or (new.blokada <> old.blokada ) or (new.blokada is not null and old.blokada is null) or (new.blokada is null and old.blokada is not null)
    or (new.grupa <> old.grupa ) or (new.grupa is not null and old.grupa is null) or (new.grupa is null and old.grupa is not null)
    or (new.operator <> old.operator ) or (new.operator is not null and old.operator is null) or (new.operator is null and old.operator is not null)
    or (new.oldbkdocnum <> old.oldbkdocnum ) or (new.oldbkdocnum is not null and old.oldbkdocnum is null) or (new.oldbkdocnum is null and old.oldbkdocnum is not null)
    ) then
   waschange = 1;

  execute procedure rp_trigger_bu('DOKUMNOT',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
