--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TRASY_BU0 FOR TRASY                          
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(coalesce(new.oddzial,'')<>coalesce(old.oddzial,'') and coalesce(new.oddzial,'')='') then
    execute procedure GETCONFIG('AKTUODDZIAL') returning_values new.oddzial;
end^
SET TERM ; ^
