--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWORKCERTIFS_BIU_STAZ_WSP FOR EWORKCERTIFS                   
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  execute procedure count_seniority(new.ref, new.employee, new.fromdate, new.todate, 0)
    returning_values new.syears, new.smonths, new.sdays;
end^
SET TERM ; ^
