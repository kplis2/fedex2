--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WEEKS_BI_REF FOR WEEKS                          
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('WEEKS')
      returning_values new.REF;
end^
SET TERM ; ^
