--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OFERTY_BU0 FOR OFERTY                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable numerator varchar(40);
declare variable BLOCKREF integer;
declare variable slodeftyp VARCHAR(15) ;
begin
  if (new.wersja is null) then new.wersja = 1;
  if(new.kosztdost is null) then new.kosztdost = 0;
  if (coalesce(new.slodef,0) > 0 and coalesce(new.slopos,0) > 0 and coalesce(new.slonazwa ,'') = '' ) then
  begin
    select typ
      from slodef
      where ref = new.slodef
      into :slodeftyp;
    if (:slodeftyp = 'KLIENCI') then
      select nazwa
        from klienci
        where ref = new.slopos
        into new.slonazwa;
    else if (:slodeftyp = 'DOSTAWCY') then
      select nazwa
        from dostawcy
        where ref = new.slopos
        into new.slonazwa;
    else
      select nazwa
        from slopoz
        where ref  = new.slopos
        into new.slonazwa;
  end 
  if ((coalesce(new.slodef, 0)<>coalesce(old.slodef,0))
    or (coalesce(new.slopos,0)<>coalesce(old.slopos,0))) then
    select ref from cpodmioty
      where cpodmioty.slodef = new.slodef
        and cpodmioty.slopoz = new.slopos
      into new.cpodmiot;
end^
SET TERM ; ^
