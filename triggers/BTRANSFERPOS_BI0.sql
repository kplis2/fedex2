--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BTRANSFERPOS_BI0 FOR BTRANSFERPOS                   
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable slodef integer;
declare variable slopoz integer;
declare variable onother numeric(14,2);
declare variable kod varchar(100);
declare variable nazwa varchar(200);
begin

  if(new.company is null) then
    select company from BTRANSFERS where ref=new.btransfer into new.company;
   if(new.settlement is null) then new.settlement = '';
   if(new.account is null) then new.account = '';
   if(new.maxamount is null or (new.maxamount = 0)) then begin
     select SLODEF, SLOPOZ
       from BTRANSFERS
       where ref = new.btransfer
       into :slodef, :slopoz;
     select MA - WINIEN
       from ROZRACH
       where SLODEF = :slodef
         and slopoz = :slopoz
         and KONTOFK = new.account
         and company = new.company
         and SYMBFAK = new.settlement
       into new.maxamount;
     if (coalesce(new.maxamount,0) = 0 ) then
       exception universal :slodef ||' '||:slopoz||' '||new.account||' '||new.company||' '||new.settlement;
     if(new.maxamount is null) then new.maxamount = 0;
     select sum(BTRANSFERPOS.AMOUNT)
       from BTRANSFERPOS
       join BTRANSFERS on (BTRANSFERS.slodef = :slodef and BTRANSFERS.slopoz = :slopoz and BTRANSFERPOS.BTRANSFER = BTRANSFERS.REF)
       join BTRANTYPE on (BTRANTYPE.symbol = BTRANSFERS.btype)
       where BTRANSFERPOS.ACCOUNT = new.account
         and BTRANSFERS.company = new.company
         and BTRANSFERPOS.settlement = new.settlement
         and ((BTRANSFERS.status < 3 and ((BTRANTYPE.genrozrach = 0) or (BTRANTYPE.genrozrach is null)))
           or (BTRANSFERS.status < 1 and BTRANTYPE.genrozrach > 0)
             )
       into :onother;
     if(onother is null) then onother = 0;
     new.maxamount = new.maxamount - :onother;
     if( new.maxamount = 0) then exception universal new.settlement ||' '||:onother;
   end
   if(new.amount is null or (new.amount = 0)) then new.amount = new.maxamount;
   if(new.account is null or (new.account = ''))then begin
     select SLODEF, SLOPOZ from BTRANSFERS where ref = new.btransfer into :slodef, :slopoz;
     execute procedure SLO_DANE(:slodef, :slopoz)
            returning_values :kod, :nazwa, new.account;
   end

end^
SET TERM ; ^
