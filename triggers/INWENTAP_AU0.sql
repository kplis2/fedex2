--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INWENTAP_AU0 FOR INWENTAP                       
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable inwentaparent integer;
begin
  if(old.blokadanalicz = 0 and new.blokadanalicz = 0) then begin
    if(new.cena<>old.cena or new.ilstan<>old.ilstan or new.ilinw<>old.ilinw or old.status<>new.status) then
      execute procedure INWENTA_OBLNAG(new.inwenta);
  end
  select inwenta.inwentaparent from INWENTA where ref=new.inwenta into :inwentaparent;
  if(new.ilinw <> old.ilinw) then begin
    if(:inwentaparent > 0) then
      execute procedure INWENTAP_OBLICZMASTER(:inwentaparent, new.wersjaref, new.cena, new.dostawa, new.ilinw);
  end
  if((new.status <> old.status) or (new.status is not null and old.status is null)
   or(new.ilroznica <> old.ilroznica) or (new.ilroznica is not null and old.ilroznica is null) )then
    update INWENTAP set STATUSMASTER = new.status, ilroznicamaster = new.ilroznica, status = -1 where POZMASTER = new.ref;
end^
SET TERM ; ^
