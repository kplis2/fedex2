--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DISTPOS_BI_REF FOR DISTPOS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DISTPOS')
      returning_values new.REF;
end^
SET TERM ; ^
