--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFFORMY_BU_REPLICAT FOR DEFFORMY                       
  ACTIVE BEFORE UPDATE POSITION 1 
as
begin
  if(new.akt <> old.akt )then begin
    execute procedure REPLICAT_STATE('DEFFORMY',new.state, NULL, new.akt, old.akt, NULL) returning_values new.state;
  end
  if(new.akt = 1 AND
       ( new.TYTUL <> old.tytul or (new.tytul is null and old.tytul is not null) or (new.tytul is not null and old.tytul is null) or
         new.metoda <> old.metoda or (new.metoda is null and old.metoda is not null) or (new.metoda is not null and old.metoda is null) or
         new.akcja <> old.akcja or (new.akcja is null and old.akcja is not null) or (new.akcja is not null and old.akcja is null) or
         new.symbol <> old.symbol or (new.symbol is null and old.symbol is not null) or (new.symbol is not null and old.symbol is null) or
         new.state = -2
       )
    )then
       execute procedure REPLICAT_STATE('DEFFORMY',new.state, NULL, new.akt, old.akt, NULL) returning_values new.state;
  if(new.symbol <> old.symbol and old.akt = 1) then begin
         execute procedure DESYNC_DEFFORMY(old.symbol);
  end
  if(new.state = -1 and old.state <> -1) then
         execute procedure DESYNC_DEFFORMY(old.symbol);
end^
SET TERM ; ^
