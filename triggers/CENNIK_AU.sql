--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CENNIK_AU FOR CENNIK                         
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable symbol varchar(20);
declare variable oddzial varchar(10);
begin
  symbol = NULL;
  oddzial = null;
  if(new.wersjaref<>old.wersjaref) then begin
    select SYMBOL, ODDZIAL from DEFCENNIK where REF=new.cennik into :symbol, :oddzial;
    if(:symbol='A') then
      if (coalesce(:oddzial,'') <> '') then
        update STANYIL set CENANETA=NULL, CENABRUA=NULL where WERSJAREF=old.wersjaref and ODDZIAL = :oddzial;
      else
        update STANYIL set CENANETA=NULL, CENABRUA=NULL where WERSJAREF=old.wersjaref;
    if(:symbol='B') then
      if (coalesce(:oddzial,'') <> '') then
        update STANYIL set CENANETB=NULL, CENABRUB=NULL where WERSJAREF=old.wersjaref and ODDZIAL = :oddzial;
          else
        update STANYIL set CENANETB=NULL, CENABRUB=NULL where WERSJAREF=old.wersjaref;
  end

  if((new.cenanet<>old.cenanet) or (new.cenabru<>old.cenabru) or (new.wersjaref<>old.wersjaref)) then begin
    select SYMBOL, ODDZIAL from DEFCENNIK where REF=new.cennik into :symbol, :oddzial;
    if(:symbol='A') then
      if (coalesce(:oddzial,'') <> '') then
        update STANYIL set CENANETA=new.cenanet, CENABRUA=new.cenabru where WERSJAREF=new.wersjaref and ODDZIAL = :oddzial;
      else
        update STANYIL set CENANETA=new.cenanet, CENABRUA=new.cenabru where WERSJAREF=new.wersjaref;
    if(:symbol='B') then
      if (coalesce(:oddzial,'') <> '') then
        update STANYIL set CENANETB=new.cenanet, CENABRUB=new.cenabru where WERSJAREF=new.wersjaref and ODDZIAL = :oddzial;
      else
        update STANYIL set CENANETB=new.cenanet, CENABRUB=new.cenabru where WERSJAREF=new.wersjaref;
  end
end^
SET TERM ; ^
