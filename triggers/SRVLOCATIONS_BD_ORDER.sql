--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVLOCATIONS_BD_ORDER FOR SRVLOCATIONS                   
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then
    exit;
  select max(number) from srvlocations where srvrequest = old.srvrequest
     into :maxnum;
  if (old.number = :maxnum) then
    exit;
  update srvlocations set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and srvrequest = old.srvrequest;
end^
SET TERM ; ^
