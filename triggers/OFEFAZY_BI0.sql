--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OFEFAZY_BI0 FOR OFEFAZY                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('OFEFAZY')
      returning_values new.REF;
end^
SET TERM ; ^
