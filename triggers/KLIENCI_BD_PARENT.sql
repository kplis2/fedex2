--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_BD_PARENT FOR KLIENCI                        
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if (exists(select ref from KLIENCI where parent = old.ref)) then
    exception universal 'Wybrany klient posiada kontrahentów podrzędne. Usunięcie niemożliwe.' ;
end^
SET TERM ; ^
