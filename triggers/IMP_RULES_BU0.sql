--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER IMP_RULES_BU0 FOR IMP_RULES                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.symbol = '') then exception IMPRULES_EMPTYSYMBOL;
  if(new.typ <> 'OLE_IMP' and new.typ <> 'FILE_IMP' and new.typ <> 'FILE_EXP')
    then exception IMPRULES_ZLYTYP;

end^
SET TERM ; ^
