--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYARCH_BIU0 FOR STANYARCH                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable typmag char;
declare variable chronologiadostaw smallint;
begin
  if (new.wersjaref is null) then
    select ref from wersje where ktm = new.ktm and nrwersji = new.wersja
      into new.wersjaref;
  if ((new.stan < 0 and inserting) or (updating and new.stan < 0 and new.stan <> old.stan)) then
  begin
    select coalesce(chronologiadostaw,0), typ
      from defmagaz
      where symbol = new.magazyn
      into chronologiadostaw, typmag;
    if (typmag = 'P' and chronologiadostaw = 1) then
      exception STANYARCH_ERROR substring('Ujemny stan dla: '||new.ktm||' na dzień: '||cast(new.data as date)||' na dostawie: '||new.dostawa from 1 for 78);
  end
end^
SET TERM ; ^
