--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ETRAININGS_BIU_CHECKDATES FOR ETRAININGS                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.fromdate is not null and new.todate is not null) then
    if (new.fromdate > new.todate) then
      exception DATA_DO_MUSI_BYC_WIEKSZA;
end^
SET TERM ; ^
