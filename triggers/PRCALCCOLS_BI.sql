--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRCALCCOLS_BI FOR PRCALCCOLS                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.ref is null) then begin
    execute procedure GEN_REF('PRCALCCOLS') returning_values new.ref;
  end
  if(new.calc is null and new.parent is not null) then select calc from prcalccols where ref=new.parent into new.calc;
  if(new.modified is null) then new.modified = 0;
  if(new.calctype is null) then new.calctype = 0;
  if(new.isalt is null) then new.isalt = 0;
  if(new.excluded is null) then new.excluded = 0;
  if (new.calctoparent is null) then new.calctoparent = 0;
end^
SET TERM ; ^
