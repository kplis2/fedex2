--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDELEGATIONS_AD0 FOR EDELEGATIONS                   
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable numerator varchar(255);
declare variable blockref integer;
begin
  execute procedure GETCONFIG('EDELEGATIONSNUM') returning_values numerator;
  if(numerator is not null and numerator <> '' and old.number is not null) then begin
    execute procedure free_number(:numerator, null, null, current_date, old.number, null) returning_values blockref;
  end
end^
SET TERM ; ^
