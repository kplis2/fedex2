--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLPOZZAM_AI0 FOR CPLPOZZAM                      
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable ret integer;
declare variable sumpkt integer;
begin
  execute procedure CPLOPER_WYSTAWUSUN(new.ref) returning_values :ret;
  select sum(ILPKT) from CPLPOZZAM where CPLNAGZAM = new.CPLNAGZAM into :sumpkt;
  update CPLNAGZAM set SUMILPKT = :sumpkt where REF = new.CPLNAGZAM;
end^
SET TERM ; ^
