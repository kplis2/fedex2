--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INWENTAP_BI_REF FOR INWENTAP                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('INWENTAP')
      returning_values new.REF;
end^
SET TERM ; ^
