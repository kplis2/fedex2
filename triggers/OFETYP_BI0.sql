--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OFETYP_BI0 FOR OFETYP                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('OFETYP')
      returning_values new.REF;
end^
SET TERM ; ^
