--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MEDICALBOARDS_AD_USZCZ FOR MEDICALBOARDS                  
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable data timestamp;
declare variable datausz timestamp;
declare variable valusz numeric(14,2);
begin
  data = null;
  datausz = null;
  valusz = 0;

  select max(mb.meddate) from medicalboards mb where mb.ckontrakt = old.ckontrakt into :data;
         select ms.meddate, ms.medvalue  from medicalboards ms where ms.meddate = :data and ms.ckontrakt = old.ckontrakt into :datausz, :valusz;
         update ckontrakty ck set ck.uszczerbek = :valusz, ck.datauszczerb = :datausz where ck.ref=old.ckontrakt;
  end^
SET TERM ; ^
