--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDEFPCARDS_BD_DELSUBGROUPS FOR EDEFPCARDS                     
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
--MWr: Nalezy zabezpieczyc sie przed wywolaniem trrigera EDEFPCARDSGROUP_BD_CHKGRPOS

  delete from edefpcardsgrpos
    where subgroup is not null
      and defpcardgr in (select ref from edefpcardsgroup where defpcard = old.ref);
end^
SET TERM ; ^
