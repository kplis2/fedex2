--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EQUESTPOSANS_BI FOR EQUESTPOSANS                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_equestposans,1);
end^
SET TERM ; ^
