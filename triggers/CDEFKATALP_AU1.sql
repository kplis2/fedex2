--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CDEFKATALP_AU1 FOR CDEFKATALP                     
  ACTIVE AFTER UPDATE POSITION 1 
as
declare variable ckatal integer;
begin
 if(new.akronim <> old.akronim or new.numer<>old.numer or new.wartdom<>old.wartdom)then begin
   for select REF from CKATAL where CDEFKATAL = new.CDEFKATAL into :ckatal do begin
     update CKATALP set SYMBOL = new.akronim, numer = new.numer, wartosc = new.wartdom
     where SYMBOL = old.akronim and CKATAL = :ckatal;
   end
 end
end^
SET TERM ; ^
