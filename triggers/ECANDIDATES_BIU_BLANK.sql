--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECANDIDATES_BIU_BLANK FOR ECANDIDATES                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  new.birthyear = extract(year from new.birthdate);
  if (new.employed is null) then
    new.employed = 0;
end^
SET TERM ; ^
