--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPAYRULES_AIUD_CHECKDEFAULT FOR EPAYRULES                      
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 1 
AS
declare variable defaults integer;
declare variable payrules integer;
begin

  if (inserting or deleting or new.isdefault is distinct from old.isdefault) then
  begin
    select sum(isdefault), count(ref) from epayrules
      where company = coalesce(new.company, old.company)
      into :defaults, :payrules;
  
    if (payrules > 0 and defaults <> 1) then
    begin
      if (not updating) then
        exception universal 'Musi istnieć jeden domyślny regulamin wynagrodzeń!';
      else
        exception universal 'Nie można zmienić regulaminu domyślnego.';
    end
  end
end^
SET TERM ; ^
