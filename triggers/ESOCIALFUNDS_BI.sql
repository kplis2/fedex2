--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ESOCIALFUNDS_BI FOR ESOCIALFUNDS                   
  ACTIVE BEFORE INSERT POSITION 2 
as
declare variable temp_c integer;
begin
  temp_c = 0;

  SELECT Count(ref) FROM esocialfunds ef WHERE ef.employee = new.employee and ef.fundtype = new.fundtype and ef.funddate = new.funddate
    into :temp_c;

  if (temp_c <> 0) then
    exception esocfunds_duplicate;
end^
SET TERM ; ^
