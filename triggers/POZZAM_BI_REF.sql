--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_BI_REF FOR POZZAM                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('POZZAM')
      returning_values new.REF;
end^
SET TERM ; ^
