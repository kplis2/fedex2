--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EASRESULT_AI0 FOR EASRESULT                      
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  insert into easresultpos (easresult, number,  eascompetence)
    select new.ref, number, eascompetence
      from eassetpos where eassetofcomp = new.eassetofcomp;
end^
SET TERM ; ^
