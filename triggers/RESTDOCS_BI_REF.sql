--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RESTDOCS_BI_REF FOR RESTDOCS                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('RESTDOCS')
      returning_values new.REF;
end^
SET TERM ; ^
