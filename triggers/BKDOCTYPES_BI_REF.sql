--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCTYPES_BI_REF FOR BKDOCTYPES                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('BKDOCTYPES')
      returning_values new.REF;
end^
SET TERM ; ^
