--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHEETHISTORY_BI0 FOR PRSHEETHISTORY                 
  ACTIVE BEFORE INSERT POSITION 1 
as
begin
  if (new.regdate is null) then new.regdate = current_timestamp(0);
  if(new.operator <=0) then new.operator = null;
  if (new.operator is null) then
    execute procedure get_global_param ('AKTUOPERATOR') returning_values new.operator;
end^
SET TERM ; ^
