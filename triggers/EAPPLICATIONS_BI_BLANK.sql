--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EAPPLICATIONS_BI_BLANK FOR EAPPLICATIONS                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.company is null) then
  begin
    select company
      from employees
      where ref = new.employee
      into new.company;
  end
end^
SET TERM ; ^
