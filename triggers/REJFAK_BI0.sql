--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER REJFAK_BI0 FOR REJFAK                         
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.waluta = '') then new.waluta = null;
  if(new.usluginamag is null) then new.usluginamag = 0;
  if(new.uslugiwrej = '') then new.uslugiwrej = null;
  if(new.editnumer is null) then new.editnumer = 0;
end^
SET TERM ; ^
