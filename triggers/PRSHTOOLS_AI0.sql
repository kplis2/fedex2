--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHTOOLS_AI0 FOR PRSHTOOLS                      
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  if((new.econdition is not null and new.econdition <>'')
    or (new.ehourfactor is not null and new.ehourfactor <>'')
    or (new.esheetrate is not null and new.esheetrate <>'')
  ) then begin
     execute procedure prsheets_calculate(new.sheet,'','S');
  end

end^
SET TERM ; ^
