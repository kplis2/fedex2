--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROMOCJE_BI FOR PROMOCJE                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  else if(new.ktm is not null and new.wersja is not null) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;

  if(new.cecha = '') then new.cecha = null;
  if(new.replicat is null) then new.replicat = 0;
  if(new.promtyp is null) then new.promtyp = 0;
end^
SET TERM ; ^
