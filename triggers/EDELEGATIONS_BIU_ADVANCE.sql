--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDELEGATIONS_BIU_ADVANCE FOR EDELEGATIONS                   
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if ((new.currency<>old.currency) and (new.currency=1)
        and (not exists(select a.ref from edeladvances a where (a.delegation = new.ref and a.currency = 'PLN')))) then
    insert into edeladvances(delegation,currency, exchangerate,allowincurr)
      values (new.ref,'PLN',1,new.advanceget);
end^
SET TERM ; ^
