--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLOYMENT_AD_EMPLOYEES FOR EMPLOYMENT                     
  ACTIVE AFTER DELETE POSITION 0 
AS
  declare variable minfromdate timestamp;
  declare variable maxfromdate timestamp;
  declare variable todate timestamp;
begin
  select first 1 fromdate
    from employment
    where employee = old.employee
    order by fromdate
    into :minfromdate;

  select first 1 fromdate
    from employment
    where employee = old.employee
    order by fromdate desc
    into :maxfromdate;

  select todate
    from employment
    where employee = old.employee and fromdate = :maxfromdate
    into :todate;

  if (minfromdate <= current_date and (todate >= current_date or todate is null)) then
    update employees set fromdate = :minfromdate, todate = :todate, emplstatus = 1
      where ref = old.employee;
  else
    update employees set fromdate = :minfromdate, todate = :todate, emplstatus = 0
      where ref = old.employee;
end^
SET TERM ; ^
