--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRACTS_AIU_SETCURRENTS FOR EMPLCONTRACTS                  
  ACTIVE AFTER INSERT OR UPDATE POSITION 4 
as
begin
/*MWr: Aktualizacja wartosci biezacych na umowie. Trigger zastepuje wczesniej
       istniejace triggery: EMPLCONTRACTS_BI oraz BU_CURRENT (PR24342)*/

  if ( new.branch is distinct from old.branch or new.department is distinct from old.department
    or new.workpost is distinct from old.workpost or new.workdim is distinct from old.workdim
    or new.salary is distinct from old.salary or new.caddsalary is distinct from old.caddsalary
    or new.funcsalary is distinct from old.funcsalary or new.worktype is distinct from old.worktype
    or new.emplgroup is distinct from old.emplgroup or new.worksystem is distinct from old.worksystem
    or new.epayrule is distinct from old.epayrule
  ) then
    execute procedure emplcontracts_currents_update(new.ref);

end^
SET TERM ; ^
