--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_BIU4 FOR DECREES                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 4 
AS
begin
  if (new.currdebit <> old.currdebit or new.debit <> old.debit or
     new.currcredit <> old.currcredit or new.credit <> old.credit or inserting)  then
  begin
    new.saldowm = new.currdebit - new.currcredit;
    new.saldowmzl = new.debit - new.credit;
  end
end^
SET TERM ; ^
