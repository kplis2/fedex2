--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_BU_KONTO FOR RKDOKPOZ                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (new.konto is null or new.konto='') then
    execute procedure xk_rk_gen_acc(new.dokument, new.pozoper)
      returning_values new.konto;
  if(new.konto is null or (new.konto = '')) then new.konto = old.konto;

  if (new.pozoper <> old.pozoper) then begin -- jezeli zmiana tranzakcji przelicz konto
    execute procedure xk_rk_gen_acc(new.dokument, new.pozoper)
      returning_values new.konto;
    if(new.konto is null or (new.konto = '')) then new.konto = old.konto;
  end
  if (new.konto5 is null) then new.konto5 = '';
  if (new.konto5 <> '' and new.konto5 not like '5%') then exception rkdokpoz_bad5acc;
end^
SET TERM ; ^
