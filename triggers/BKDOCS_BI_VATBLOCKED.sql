--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_BI_VATBLOCKED FOR BKDOCS                         
  ACTIVE BEFORE INSERT POSITION 10 
AS
begin
  if (coalesce(new.vatperiod,'') != '') then
    if (exists(select p.id from bkperiods p where p.id = new.vatperiod and p.company = new.company and  p.vatblocked = 1)) then
      exception bkperiods_vatblocked;
end^
SET TERM ; ^
