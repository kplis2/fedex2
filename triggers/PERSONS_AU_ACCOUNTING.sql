--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PERSONS_AU_ACCOUNTING FOR PERSONS                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable company integer;
begin
/*Aktualizacja danych osobowych na EMPLOYEES i opisu kont ksiegowych dla danego roku
  Scalenie wczesniejszych triggerow PERSONS_AU oraz PERSONS_AU_EMPLOYEE (PR59074) */

  if (new.person <> old.person) then
  begin
    execute procedure get_global_param('CURRENTCOMPANY')
      returning_values :company;

    execute procedure person_set_accounting(new.ref, :company, null);
  end
end^
SET TERM ; ^
