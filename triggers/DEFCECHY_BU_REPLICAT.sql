--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHY_BU_REPLICAT FOR DEFCECHY                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.SYMBOL<>old.SYMBOL
    or new.rodzaj <> old.rodzaj or (new.rodzaj is null and old.rodzaj is not null) or (new.rodzaj is not null and old.rodzaj is null)
    or new.tabela <> old.tabela or (new.tabela is null and old.tabela is not null) or (new.tabela is not null and old.tabela is null)
    or new.pole <> old.pole or (new.pole is null and old.pole is not null) or (new.pole is not null and old.pole is null)
    or new.wersje <> old.wersje or (new.wersje is null and old.wersje is not null) or (new.wersje is not null and old.wersje is null)
    or new.typ <> old.typ or (new.typ is null and old.typ is not null) or (new.typ is not null and old.typ is null)
    or new.grupa <> old.grupa or (new.grupa is null and old.grupa is not null) or (new.grupa is not null and old.grupa is null)
    or new.wyszuk <> old.wyszuk or (new.wyszuk is null and old.wyszuk is not null) or (new.wyszuk is not null and old.wyszuk is null)
    or new.nazwa <> old.nazwa or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null)
    or new.symbol <> old.symbol or (new.symbol is null and old.symbol is not null) or (new.symbol is not null and old.symbol is null)
    or new.parsymbol <> old.parsymbol or (new.parsymbol is null and old.parsymbol is not null) or (new.parsymbol is not null and old.parsymbol is null)
    or new.parskrot <> old.parskrot or (new.parskrot is null and old.parskrot is not null) or (new.parskrot is not null and old.parskrot is null)
    or new.parnazwa <> old.parnazwa or (new.parnazwa is null and old.parnazwa is not null) or (new.parnazwa is not null and old.parnazwa is null)
    or new.partyp <> old.partyp or (new.partyp is null and old.partyp is not null) or (new.partyp is not null and old.partyp is null)
    or new.parredzamspr <> old.parredzamspr or (new.parredzamspr is null and old.parredzamspr is not null) or (new.parredzamspr is not null and old.parredzamspr is null)
    or new.parredzamzak <> old.parredzamzak or (new.parredzamzak is null and old.parredzamzak is not null) or (new.parredzamzak is not null and old.parredzamzak is null)
    or new.parredfakspr <> old.parredfakspr or (new.parredfakspr is null and old.parredfakspr is not null) or (new.parredfakspr is not null and old.parredfakspr is null)
    or new.parredfakzak <> old.parredfakzak or (new.parredfakzak is null and old.parredfakzak is not null) or (new.parredfakzak is not null and old.parredfakzak is null)
    or new.parreddokwyd <> old.parreddokwyd or (new.parreddokwyd is null and old.parreddokwyd is not null) or (new.parreddokwyd is not null and old.parreddokwyd is null)
    or new.parreddokprz <> old.parreddokprz or (new.parreddokprz is null and old.parreddokprz is not null) or (new.parreddokprz is not null and old.parreddokprz is null)
    or new.parwartdom <> old.parwartdom or (new.parwartdom is null and old.parwartdom is not null) or (new.parwartdom is not null and old.parwartdom is null)
    ) then
   waschange = 1;

  execute procedure rp_trigger_bu('DEFCECHY',old.symbol, null, null, null, null, old.token, old.state,
        new.symbol, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
