--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKRAPKAS_BI0 FOR RKRAPKAS                       
  ACTIVE BEFORE INSERT POSITION 1 
as
  declare variable lokres varchar(6);
  declare variable skbo  decimal(14,2);
  declare variable skbow  decimal(14,2);
  declare variable smkw  decimal(14,2);
  declare variable smkww  decimal(14,2);
  declare variable refrk integer;
  declare variable numerrk integer;
  declare variable datark timestamp;
  declare variable refprev integer;
  declare variable rokod integer;
  declare variable mscod integer;
  declare variable rokdo integer;
  declare variable mscdo integer;
  declare variable typstan char(1);
  declare variable dzienny integer;
  declare variable kilkadzien integer;
  declare variable otwkilka integer;
  declare variable cnt integer;
  declare variable gennumrap varchar(40);
  declare variable symbol varchar(30);
begin
  if (new.status is null) then new.status = 0;
  /*okres numerowania*/
  select DZIENNY, KILKADZIENNIE, OTWKILKA, RKSTNKAS.NUMRAP, RKSTNKAS.GENNUMRAP
    from RKSTNKAS
    where KOD = new.stanowisko
    into :dzienny,:kilkadzien, :otwkilka,:typstan,:gennumrap;

  if (typstan is null) then typstan = 'R';
  lokres = cast(extract(year from new.dataod) as char(4));
  if (typstan = 'R') then new.okresnum = lokres; -- okres roczny
  if (extract(month from new.dataod) < 10) then
     lokres = lokres || '0' ||cast(extract(month from new.dataod) as char(1));
  else begin
     lokres = lokres || cast(extract(month from new.dataod) as char(2));
  end
  if(typstan <> 'R') then new.okresnum = lokres; -- okres miesiczny
  if (new.datado is null) then
    new.datado = new.dataod;
  if (dzienny = 1 ) then
    new.datado = new.dataod;
  /* sprawdzanie dat i parametrow stanowiska*/
  if (OTWKILKA = 0) then begin
    if (exists(
      select ref from RKRAPKAS
        where STANOWISKO = new.stanowisko and STATUS = 0 and ref <> new.ref))
    then exception RPRAPKAS_SA_OTWARTE;
  end
  /* sprawdzenie, czy jaki raport nie nachodzi od dolu */
  cnt = null;
  if (kilkadzien = 1) then
  begin
    if (exists(
      select ref from RKRAPKAS
        where DATADO > new.DATAOD and DATADO < new.datado and stanowisko = new.stanowisko))
    then cnt = 1;
  end
  else begin
    if (exists(
      select ref from RKRAPKAS
        where DATADO >= new.DATAOD and DATADO <= new.datado and stanowisko = new.stanowisko))
    then cnt = 1;
  end
  if (cnt > 0) then exception RKRAPKAS_DATAZDOLU;
  /* sprawdzenie, czy jaki raport nie nachodzi od gory */
  cnt = null;
  if (kilkadzien = 1) then
  begin
    if (exists(
      select ref from RKRAPKAS
        where DATAOD > new.DATAOD and DATAOD < new.datado and stanowisko = new.stanowisko))
    then cnt = 1;
  end
  else begin
    if (exists(
      select ref from RKRAPKAS
        where DATAOD >= new.DATAOD and DATAOD <= new.datado and stanowisko = new.stanowisko))
    then cnt =1;
  end
  if (cnt > 0) then exception RKRAPKAS_DATAZGORY;

  /* sprawdzenie, czy inny raport nie polyka danego */
  cnt = null;
  if (exists(select ref from RKRAPKAS
    where DATAOD <= new.dataod and DATADO >= new.datado
      and ( (:kilkadzien = 0)
        or (new.dataod <> new.datado or ( new.dataod <> DATAOD and new.datado <> DATADO)) )
      and stanowisko = new.stanowisko))
  then exception RKRAPKAS_DATAW;

  if (new.okres is null) then new.okres = lokres;
  rokod = extract(year from new.dataod);
  mscod = extract(month from new.dataod);
  rokdo = extract(year from new.datado);
  mscdo = extract(month from new.datado);
  if ((rokod <> rokdo) or (mscod <> mscdo)) then exception RKRAPKAS_DATYSPOZAMSC;
  if ((new.okres <> lokres) or (new.dataod is null) or
      (new.datado is null) or (new.dataod > new.datado))
      then exception RK_NIEPRAWIDLOWE_DATY;

  /*nadajemy numer raportu w wasnym okresie*/
  if (new.numer is null) then
  begin
    execute procedure GET_NUMBER(:gennumrap, new.stanowisko,'',new.dataod,0, new.ref) returning_values new.numer, symbol;
  end

  /*sprawdzenie, czy istnieją raporty z mniejszym numerem poxniejsze */
/*
  DU: bąd na przelomie roku, oraz przy numeracji miesiecznej
  cnt =  null;
  select count(*) from RKRAPKAS where STANOWISKO = new.stanowisko and NUMER < new.numer and DATAOD > new.dataod
    into :cnt;
  if(:cnt > 0) then exception RKRAPKAS_DATARAPTOSMALL;
*/

  /*ustawiamy stan otwarcia kasy*/
  for
    select ref, dataod, numer
      from RKRAPKAS
      where STANOWISKO=new.stanowisko and DATAOD <= new.dataod
      order by dataod, numer
      into :refrk, :datark, :numerrk
  do begin
    if ((datark < new.dataod) or (numerrk < new.numer))
        then refprev = :refrk;
  end
  if (refprev > 0) then begin
    select SKWOTA, SKWOTAZL from RKRAPKAS
      where ref=:refprev
      into new.kwotabo, new.kwotabozl;
  end
  if (new.kwotabo is null) then new.kwotabo=0;
  if (new.kwotabozl is null) then new.kwotabozl=0;
  new.skwota = new.kwotabo + new.przychod - new.rozchod;
  new.skwotazl = new.kwotabozl + new.przychodzl - new.rozchodzl;
  new.wasdeakcept = 0;
  if (new.electronic is null) then new.electronic = 0;
end^
SET TERM ; ^
