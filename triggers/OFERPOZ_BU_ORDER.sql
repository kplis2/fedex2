--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OFERPOZ_BU_ORDER FOR OFERPOZ                        
  ACTIVE BEFORE UPDATE POSITION 1 
as
  declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 0;
  if (new.ord = 5) then exit;
  if (new.numer is null) then new.numer = old.numer;
  if (new.ord = 1 or new.numer = old.numer) then
  begin
    new.ord = 0;
    exit;
  end
  --numerowanie obszarow w rzedzie gdy nie podamy numeru
  if (new.numer <> old.numer) then
    select max(numer) from oferpoz where oferta = new.oferta
      into :maxnum;
  else
    maxnum = 0;
  if (new.numer < old.numer) then
  begin
    update oferpoz set ord = 1, numer = numer + 1
      where ref <> new.ref and numer >= new.numer
        and numer < old.numer and oferta = new.oferta;
  end else if (new.numer > old.numer and new.numer <= maxnum) then
  begin
    update oferpoz set ord = 1, numer = numer - 1
      where ref <> new.ref and numer <= new.numer
        and numer > old.numer and oferta = new.oferta;
  end else if (new.numer > old.numer and new.numer > maxnum) then
    new.numer = old.numer;
end^
SET TERM ; ^
