--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_BU_PROSTYSYMBOL FOR NAGFAK                         
  ACTIVE BEFORE UPDATE POSITION 2 
AS
begin
  if (new.zakup = 1) then
  begin
    if ((new.akceptacja = 1 or new.akceptacja = 8 ) and (new.symbolfak <> old.symbolfak or old.prostysymbol is null)) then
    begin
      new.prostysymbol = new.symbolfak;
      new.prostysymbol = replace(new.prostysymbol,  ' ',  ''); -- regula nr 1 wycinamy spacje z symbolu
      new.prostysymbol = replace(new.prostysymbol,  ';',  '');
      new.prostysymbol = replace(new.prostysymbol, ',', '');
      new.prostysymbol = replace(new.prostysymbol,  '.',  '');
      new.prostysymbol = replace(new.prostysymbol,  '_',  '');
      new.prostysymbol = replace(new.prostysymbol,  '-',  '');
      new.prostysymbol = replace(new.prostysymbol,  '=',  '');
      new.prostysymbol = replace(new.prostysymbol,  '+',  '');
      new.prostysymbol = replace(new.prostysymbol,  '?',  '');
      new.prostysymbol = replace(new.prostysymbol,  '/',  '');
      new.prostysymbol = replace(new.prostysymbol,  '\',  '');
      new.prostysymbol = replace(new.prostysymbol,  '|',  '');
      new.prostysymbol = replace(new.prostysymbol,  '!',  '');
      -- regula musi byc spujna/zgodna z trigerem na bkdoksie
    end
  end
  else if (new.zakup = 0) then
  begin
    if ((new.akceptacja = 1 or new.akceptacja = 8) and (new.symbol <> old.symbol or old.prostysymbol is null)) then
    begin
      new.prostysymbol = new.symbol;
      new.prostysymbol = replace(new.prostysymbol,  ' ',  '');-- regula nr 1 wycinamy spacje z symbolu
      new.prostysymbol = replace(new.prostysymbol,  ';',  '');
      new.prostysymbol = replace(new.prostysymbol, ',', '');
      new.prostysymbol = replace(new.prostysymbol,  '_',  '');
      new.prostysymbol = replace(new.prostysymbol,  '-',  '');
      new.prostysymbol = replace(new.prostysymbol,  '+',  '');
      new.prostysymbol = replace(new.prostysymbol,  '=',  '');
      new.prostysymbol = replace(new.prostysymbol,  '/',  '');
      new.prostysymbol = replace(new.prostysymbol,  '\',  '');
      new.prostysymbol = replace(new.prostysymbol,  '?',  '');
      new.prostysymbol = replace(new.prostysymbol,  '!',  '');
      new.prostysymbol = replace(new.prostysymbol,  '.',  '');
      new.prostysymbol = replace(new.prostysymbol,  '|',  '');
      -- regula musi byc spujna/godna z trigerem na bkdoksie
    end
  end
  if (new.prostysymbol = '') then
    new.prostysymbol = null;
end^
SET TERM ; ^
