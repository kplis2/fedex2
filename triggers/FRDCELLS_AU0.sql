--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDCELLS_AU0 FOR FRDCELLS                       
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  -- zmiana wartosci komórek zaleznych dla wartosci planowanych
  if (new.planamount <> old.planamount
      or (new.planamount is null and old.planamount is not null)
      or (new.planamount is not null and old.planamount is null)) then
    execute procedure FRD_BOTTOMUP_CALCULATE (new.ref,'PLANAMOUNT');
  if (new.realamount <> old.realamount
      or (new.realamount is null and old.realamount is not null)
      or (new.realamount is not null and old.realamount is null)) then
    execute procedure FRD_BOTTOMUP_CALCULATE (new.ref,'REALAMOUNT');
  if (new.val1 <> old.val1
      or (new.val1 is null and old.val1 is not null)
      or (new.val1 is not null and old.val1 is null)) then
    execute procedure FRD_BOTTOMUP_CALCULATE (new.ref,'VAL1');
  if (new.val2 <> old.val2
      or (new.val2 is null and old.val2 is not null)
      or (new.val2 is not null and old.val2 is null)) then
    execute procedure FRD_BOTTOMUP_CALCULATE (new.ref,'VAL2');
  if (new.val3 <> old.val3
      or (new.val3 is null and old.val3 is not null)
      or (new.val3 is not null and old.val3 is null)) then
    execute procedure FRD_BOTTOMUP_CALCULATE (new.ref,'VAL3');
  if (new.val4 <> old.val4
      or (new.val4 is null and old.val4 is not null)
      or (new.val4 is not null and old.val4 is null)) then
    execute procedure FRD_BOTTOMUP_CALCULATE (new.ref,'VAL4');
  if (new.val5 <> old.val5
      or (new.val5 is null and old.val5 is not null)
      or (new.val5 is not null and old.val5 is null)) then
    execute procedure FRD_BOTTOMUP_CALCULATE (new.ref,'VAL5');
end^
SET TERM ; ^
