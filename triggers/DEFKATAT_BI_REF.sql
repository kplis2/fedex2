--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFKATAT_BI_REF FOR DEFKATAT                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DEFKATAT')
      returning_values new.REF;
end^
SET TERM ; ^
