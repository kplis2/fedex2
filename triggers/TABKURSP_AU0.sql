--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TABKURSP_AU0 FOR TABKURSP                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if((new.waluta <> old.waluta)
   or(new.sredni <> old.sredni) or (new.sredni is not null and old.sredni is null) or (new.sredni is null and old.sredni is not null)
   or(new.zakupu <> old.zakupu) or (new.zakupu is not null and old.zakupu is null) or (new.zakupu is null and old.zakupu is not null)
   or(new.sprzedazy <> old.sprzedazy ) or (new.sprzedazy is not null and old.sprzedazy is null) or (new.sprzedazy is null and old.sprzedazy is not null)
  ) then
    update TABKURS set STATE = -1 where REF=new.tabkurs;
end^
SET TERM ; ^
