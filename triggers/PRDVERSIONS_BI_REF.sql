--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRDVERSIONS_BI_REF FOR PRDVERSIONS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('PRDVERSIONS')
      returning_values new.REF;
end^
SET TERM ; ^
