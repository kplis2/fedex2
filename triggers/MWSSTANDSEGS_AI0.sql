--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDSEGS_AI0 FOR MWSSTANDSEGS                   
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable cnt smallint;
declare variable number integer;
declare variable w numeric(14,2);
declare variable x_row_type smallint_id; -- XXX KBI
begin
  -- zalozenie obszarów
  if (new.mwsconstlocsq is null or new.mwsconstlocsq = 0) then
    exception MWSSTANDSEG_CONSTLOCSQ_NOT_SET;
  cnt = 0;
  -- okreslam szerokosc obszaru
  select w,  x_type from whsecrows where ref = new.whsecrow into :w, :x_row_type; -- XXX KBI x_row_type
  if (x_row_type is null) then x_row_type = 0; -- XXX KBI
  if (w is null) then w = 0;
  -- szukam segmentow z danego regalu wstawionych wczesniej
  select max(number)
    from whareas
    where mwsstand = new.mwsstand
    into :number;
  -- szukam obszarow z regalów ustawionych przed zakladanym i biore max. numer
  if (number is null) then
    number = 0;
  if (number = 0) then
  begin
    select max(number)
      from whareas
      where whsecrow = new.whsecrow
        and mwsstandnumber < new.mwsstandnumber
      into :number;
    if (number is null) then
      number = 0;
  end
  -- trzeba sprawdzic czy mozna zalozyc nowy obszar czy jest tu na przyklad obszar nie zwiazany z regalami,
  -- np logistyczny
  while (exists (select REF from whareas where number = :number + 1 and whsecrow = new.whsecrow
      and (mwsstandseg is null or areatype = 1 or areatype = 2)))
  do begin
    number = number + 1;
  end
  while (cnt < new.mwsconstlocsq)
  do begin
    cnt = cnt + 1;
    insert into whareas (wh, areatype, whsec, whsecrow, number, whsecdict, mwsstandseg,
        whsecrowdict, mwsstandsegtype, mwsstandsegtypedict, mwsstand,
        mwsstandnumber, w, l, locnumber, x_row_type)                              -- XXX KBI x_row_type
      values (new.wh, new.whareatype, new.whsec, new.whsecrow, :number + :cnt,new.whsecdict, new.ref,
          new.whsecrowdict, new.mwsstandsegtype, new.mwsstandsegtypedict, new.mwsstand,
          new.mwsstandnumber, :w, new.w / new.mwsconstlocsq, :cnt, :x_row_type);  -- XXX KBI x_row_type
  end
end^
SET TERM ; ^
