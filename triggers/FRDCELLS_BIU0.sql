--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDCELLS_BIU0 FOR FRDCELLS                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.planamount is null) then new.planamount = 0;
  if (new.realamount is null) then new.realamount = 0;
  if (new.val1 is null) then new.val1 = 0;
  if (new.val2 is null) then new.val2 = 0;
  if (new.val3 is null) then new.val3 = 0;
  if (new.val4 is null) then new.val4 = 0;
  if (new.val5 is null) then new.val5 = 0;
  if (new.pplanamount is null) then new.pplanamount = 0;
  if (new.deviation is null) then new.deviation = 0;
  if (new.percplanreal is null) then new.percplanreal = 0;
  if (new.lastsessplanamount is null) then new.lastsessplanamount = 0;
  if (new.wasmodinsess is null) then new.wasmodinsess = 0;
  if (new.wasmod is null) then new.wasmod = 0;
  if (new.timeofchange is null) then new.timeofchange = current_timestamp(0);
  if (new.readrights is null or new.readrights = ';') then
    new.readrights = '';
  if (new.writerights is null or new.writerights = ';') then
    new.writerights = '';
end^
SET TERM ; ^
