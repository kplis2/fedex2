--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CFORMY_BI_REF FOR CFORMY                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CFORMY')
      returning_values new.REF;
end^
SET TERM ; ^
