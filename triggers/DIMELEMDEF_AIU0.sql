--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DIMELEMDEF_AIU0 FOR DIMELEMDEF                     
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
AS
declare variable cell integer;
declare variable frdreadrights varchar(80);
declare variable frdwriterights varchar(80);
declare variable cellreadrights varchar(80);
declare variable cellwriterights varchar(80);
begin
  for
    select frdcells.ref, frdpersdimvals.readrights, frdpersdimvals.writerights,
        frdcells.readrights, frdcells.writerights
      from frdcells
        join frdpersdimvals on (frdcells.frdpersdimval = frdpersdimvals.ref)
      where frdcells.dimelem = new.ref
      into :cell, :frdreadrights, :frdwriterights,
        :cellreadrights, :cellwriterights
  do begin
    execute procedure FRDCELLS_SETRIGHTS(:cell,:frdreadrights,:frdwriterights,new.readrights,
      new.writerights, :cellreadrights, :cellwriterights);
  end
end^
SET TERM ; ^
