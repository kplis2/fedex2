--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DZIALY_BU_REPLICAT FOR DZIALY                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.nazwa <> old.nazwa or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null) or
    new.id <> old.id or (new.id is not null and old.id is null) or (new.id is null and old.id is not null) or
    new.klasan <> old.klasan or (new.klasan is not null and old.klasan is null) or (new.klasan is null and old.klasan is not null) or
    new.klasas <> old.klasas or  (new.klasas is not null and old.klasas is null) or (new.klasas is null and old.klasas is not null) or
    new.klasaa <> old.klasaa or (new.klasaa is not null and old.klasaa is null) or (new.klasaa is null and old.klasaa is not null) or
    new.state = -2
  ) then execute procedure REPLICAT_STATE('DZIALY',new.state, new.ref, NULL,NULL,NULL) returning_values new.state;
end^
SET TERM ; ^
