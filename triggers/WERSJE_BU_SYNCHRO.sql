--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WERSJE_BU_SYNCHRO FOR WERSJE                         
  ACTIVE BEFORE UPDATE POSITION 5 
as
declare variable cnt integer;
begin
   if(new.nrwersji <> old.nrwersji) then begin
      select count(*) from ANALDOSTP where WERSJAREF = new.ref into :cnt;
      if(:cnt > 0) then exception WERSJAS_ANALDOSTP;
      select count(*) from DOKUMPOZ where WERSJAREF = new.ref into :cnt;
      if(:cnt > 0) then exception WERSJAS_DOKUMPOZ;
      select count(*) from INWENTAP where WERSJAREF = new.ref into :cnt;
      if(:cnt > 0) then exception WERSJAS_INWENTAP;
      select count(*) from POZFAK where WERSJAREF = new.ref into :cnt;
      if(:cnt > 0) then exception WERSJAS_POZFAK;
      select count(*) from POZZAM where WERSJAREF = new.ref into :cnt;
      if(:cnt > 0) then exception WERSJAS_POZZAM;
      select count(*) from STANYIL where WERSJAREF = new.ref into :cnt;
      if(:cnt > 0) then exception WERSJAS_STANYIL;
      select count(*) from STANYREZ where WERSJAREF = new.ref into :cnt;
      if(:cnt > 0) then exception WERSJAS_STANYREZ;
   end
end^
SET TERM ; ^
