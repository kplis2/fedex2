--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDEDETS_AI0 FOR PRSCHEDGUIDEDETS               
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  if (new.quantity > 0 and new.out in (1,3)) then
    EXECUTE PROCEDURE PRSCHEDGUIDEPOS_AMOUNTZREAL(new.prschedguidepos);
  else if (new.quantity > 0 and new.out = 0 and new.prschedoper is not null) then
  begin
    -- sprawdzenie czy nie probujemy przyjac wiecej niz na operacji
    execute procedure PRSCHEDOPER_CHECK_AMOUNTZREAL(new.prschedoper);
    if (new.accept > 0) then
    begin
      -- wyliczenie ilosci zrealizowanej
      EXECUTE PROCEDURE PRSCHEDGUIDE_AMOUNTZREAL(new.prschedguide);
      -- wyliczenie ilosci braków
      if (new.prshortage is not null) then
        EXECUTE PROCEDURE PRSHORTAGE_AMOUNTZREAL(new.prshortage);
    end
  end
  if(new.out = 1 and new.accept > 0 and new.quantity > 0) then
    execute procedure prschedguides_statuschange(new.prschedguide,1,null);
end^
SET TERM ; ^
