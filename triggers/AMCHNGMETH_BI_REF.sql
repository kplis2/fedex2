--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER AMCHNGMETH_BI_REF FOR AMCHNGMETH                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('AMCHNGMETH')
      returning_values new.REF;
end^
SET TERM ; ^
