--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAMUP_BU_CHECK FOR POZZAMUP                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
   if(new.ilosc is null) then
     exception POZZAMUP_EXPT 'Ilość towaru do odebrania nie może być zerowa.';
   if(old.ilosc <> new.ilosc) then begin
     if(new.ilosc = 0) then
       exception POZZAMUP_EXPT 'Ilość towaru do odebrania nie może być zerowa.';
     if(new.ilosc < 0) then
       exception POZZAMUP_EXPT 'Ilość towaru do odebrania nie może być ujemna.';
   end
end^
SET TERM ; ^
