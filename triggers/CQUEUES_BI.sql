--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CQUEUES_BI FOR CQUEUES                        
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_cqueues,1);
end^
SET TERM ; ^
