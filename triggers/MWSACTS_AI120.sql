--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_AI120 FOR MWSACTS                        
  ACTIVE AFTER INSERT POSITION 120 
AS
declare variable type mwsordtypes_id;
begin
  select mwsordtype from mwsords where ref = new.mwsord into :type;
  if( type=8 and new.x_slownik is not null) then
  begin
     update x_slowniki_ehrle s set s.mwsacts=new.ref where s.ref=new.x_slownik;
  end
end^
SET TERM ; ^
