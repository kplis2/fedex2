--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDPOZ_BI0 FOR LISTYWYSDPOZ                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.waga is null) then new.waga = 0;
  if (new.waga = 0) then begin
    select WAGA from TOWJEDN where ref=new.jedno into new.waga;
    new.waga = new.waga * new.ilosco;
  end
  if (new.objetosc is null) then new.objetosc = 0;
  if (new.objetosc = 0) then begin
    select VOL from TOWJEDN where ref=new.jedno into new.objetosc;
    new.objetosc = new.objetosc * new.ilosco;
  end
  if(new.waga is null) then new.waga = 0;
  if (new.objetosc is null) then new.objetosc = 0;
  if(new.jedno is not null and new.jedno>0) then
    select JEDN from TOWJEDN where ref=new.jedno into new.miarao;
  new.iloscorg = new.ilosc;
  if(new.spedjedn='') then new.spedjedn = null;
  if(new.zlecjedn='') then new.zlecjedn = null;
  --XXX JO:
  if (new.x_spakowano is null) then new.x_spakowano = 0;
end^
SET TERM ; ^
