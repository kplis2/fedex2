--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWARY_AFTER_INSERT FOR TOWARY                         
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable newtoken smallint;
declare variable wersjaref integer;
declare variable mwsnazwa string80;
begin
  if(new.token <>8 and new.token <> 7) then begin   /*jeśli to nie jest replikator*/
    insert into towjedn(KTM,GLOWNA,S,Z,C,M,JEDN,CONST,PRZELICZ,KODKRESK)
      values (new.ktm, 1,2,2,2,2,new.miara,1,1,new.kodkresk);

  insert into WERSJE(KTM,NRWERSJI,NAZWA,OPIS,AKT,NAZWAT,
      CENA_ZAKN,CENA_ZAKB,CENA_FABN, CENA_FABB, CENA_KOSZTN, CENA_KOSZTB, CENA_ZAKNL,
      DNIWAZN,SYMBOL_DOST,KODKRESK,MARZAMIN,USLUGA,BKSYMBOL,KODPROD)
   values(new.KTM,0,'Podstawowa','Wersja podstawowa',new.AKT,new.nazwa,
      new.cena_zakn,new.cena_zakb,new.cena_fabn, new.cena_fabb, new.cena_kosztn, new.cena_kosztb, new.CENA_ZAKNL,
      new.dniwazn, new.symbol_dost,new.kodkresk,new.marzamin,new.usluga,new.bksymbol, new.kodprod);
  end
  if(new.cennikzewn > 0) then begin
     select REF from WERSJE where KTM = new.ktm and NRWERSJI = 0 into :wersjaref;
    insert into DOSTCEN(DOSTAWCA, WERSJAREF, CENNIKZEWN, SYMBOL_DOST, NAZWA_DOST,
      cenanet, cena_fab, cena_koszt)
    select DOSTAWCA, :wersjaref, REF, new.symbol_dost, NAZWA, new.cena_zakn, new.cena_fabn, new.cena_kosztn
    from CENNIKIZEWN where REF=new.cennikzewn;
  end

  execute procedure TOWAR_MWSNAZWA(new.KTM) returning_values :mwsnazwa;
  update towary set mwsnazwa = :mwsnazwa where KTM = new.KTM;
end^
SET TERM ; ^
