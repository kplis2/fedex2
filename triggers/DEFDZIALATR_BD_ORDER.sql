--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFDZIALATR_BD_ORDER FOR DEFDZIALATR                    
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update defdzialatr set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and klasa = old.klasa;
end^
SET TERM ; ^
