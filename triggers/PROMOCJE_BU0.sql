--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROMOCJE_BU0 FOR PROMOCJE                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable val DECIMAL(14,2);
begin
  if((new.wersjaref <> old.wersjaref or (new.ktm is null) or (new.wersja is null)) and new.wersjaref is not null and new.wersjaref <> 0) then begin
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  end else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)or (new.wersjaref is null)) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if(new.cecha = '') then new.cecha = null;
  if(new.wartosc <> old.wartosc or (new.ilosc <> old.ilosc)) then begin
    select min(wartosc) from promocr where promocja = new.ref into :val;
    if(:val is not null) then new.wartosc = val;
    select min(ilosc) from promocr where promocja = new.ref into :val;
    if(:val is not null) then new.minilosc = val;
  end
  if(new.promtyp is null) then new.promtyp = 0;
end^
SET TERM ; ^
