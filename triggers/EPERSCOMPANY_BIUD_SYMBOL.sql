--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPERSCOMPANY_BIUD_SYMBOL FOR EPERSCOMPANY                   
  ACTIVE BEFORE INSERT OR UPDATE OR DELETE POSITION 1 
AS
  declare variable slength smallint;
  declare variable dictdef integer;
  declare variable number integer = 0;
begin
  if (not deleting) then
  begin
    if (new.symbol = '') then
      new.symbol = null;
  
    if (new.symbol is null) then
    begin
      select max(kodln) from slodef
        where typ = 'PERSONS'
        into :slength;
  
      select coalesce(max(cast(symbol as integer)),0) from eperscompany
        where company = new.company
        into number;
  
      execute procedure get_symbol_from_number(number + 1, coalesce(slength,3))
        returning_values new.symbol;
    end
  end

  if (deleting or (updating and new.symbol <> old.symbol or new.company <> old.company)) then
  begin
  --Realizacja zadania dawnych: PERSONS_BUCHECKACCOUNTING oraz PERSONS_BD_CHECKACCOUNTING (PR59074)
    select ref from slodef
      where typ = 'PERSONS'
      into :dictdef;

    execute procedure check_dict_using_in_accounts(dictdef, old.symbol, old.company);
  end
end^
SET TERM ; ^
