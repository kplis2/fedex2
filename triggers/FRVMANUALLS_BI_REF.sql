--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRVMANUALLS_BI_REF FOR FRVMANUALLS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FRVMANUALLS')
      returning_values new.REF;
END^
SET TERM ; ^
