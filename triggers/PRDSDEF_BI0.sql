--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRDSDEF_BI0 FOR PRDSDEF                        
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.autoamountin is null) then new.autoamountin = 0;
  if (new.autoamountin = 1 and coalesce(new.wh,'') = '') then
    exception PRDSDEF_ERROR 'Brak wskazania magazynu przy automatycznym naliczaniu iloci';
  if (new.wh = '') then new.wh = null;
  if (new.allprtoolstypes is null) then new.allprtoolstypes = 0;
end^
SET TERM ; ^
