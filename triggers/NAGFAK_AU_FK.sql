--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_AU_FK FOR NAGFAK                         
  ACTIVE AFTER UPDATE POSITION 3 
as
declare variable konf varchar(255);
declare variable bkref integer;
declare variable sblokadadelay varchar(255);
declare variable blokadadelay integer;
declare variable blokadasid smallint;
declare variable zerowanie varchar(255);
begin
  if(coalesce(new.bkblock,0) = 0 and ((new.akceptacja = 1) or (new.akceptacja = 8)) and old.akceptacja <> 1 and old.akceptacja <> 8
     and ((new.nieobrot =0) or (new.nieobrot = 2))) then begin
    -- sprawdzenie czy okres ksiegowy dla company jest juz zablokowany
    if(new.zakup = 0) then
      select min(sidblocked) from bkperiods
        where ptype=1 and old.data>=sdate and old.data<=fdate and company = old.company
        into :blokadasid;
    else
      select min(sidblocked) from bkperiods
        where ptype=1 and old.dataotrz>=sdate and old.dataotrz<=fdate and company = old.company
        into :blokadasid;
    if (blokadasid = 1) then exception SID_FK_BLOKADAKSIEGOWA;
    --akceptacja dokumentu - sprawdzenie, czy dekretowac
    if(new.zakup = 0) then
      execute procedure GETCONFIG('SIDFK_SPRAUTO') returning_values :konf;
    else
      execute procedure GETCONFIG('SIDFK_ZAKAUTO') returning_values :konf;
    if(:konf = '1') then begin
      execute procedure DOCS_TO_BKDOCS(0,new.ref,new.operakcept,0,1);
    end
  end else
  if((old.anulowanie = 0 and new.anulowanie<>0)
     or (new.akceptacja <> 1 and new.akceptacja <> 8
         and ((old.akceptacja = 1) or (old.akceptacja = 8))
        )
  )then begin
    /*deakceptacja - sprawdzenie, czy wycofac dekretacje*/
    if(new.zakup = 0) then
      execute procedure GETCONFIG('SIDFK_SPRBLOK') returning_values :konf;
    else
      execute procedure GETCONFIG('SIDFK_ZAKBLOK') returning_values :konf;
    if(:konf = '1' or (:konf = '2')) then begin
      select max(ref) from BKDOCS where OTABLE='NAGFAK' and OREF = old.ref into :bkref;
      if(:bkref > 0 or new.blokada = 3) then begin
        if(:bkref is null) then begin
          -- blokada bez dokumentu - np. znacznik zaksiegowania po replikacji z oddzialu - sprawdzenie wg kryteriow z akceptacji dokumentu
          execute procedure GETCONFIG('SIDFK_BLOKADA')
            returning_values :sblokadadelay;
          if(:sblokadadelay <> '') then
            blokadadelay = cast(:sblokadadelay as integer);
          else blokadadelay = 0;
          if(current_date > new.data + :blokadadelay) then
            exception NAGFAK_BLOKADAKS;
        end
        if(:konf = '1') then exception NAGFAK_ZAKSIEGOWANY;
        else begin
          if(new.zakup = 0) then
            select sidblocked from bkperiods
              where ptype = 1 and old.data>=sdate and old.data<=fdate and company = old.company
            into :blokadasid;
          else
            select sidblocked from bkperiods
              where ptype = 1 and old.dataotrz>=sdate and old.dataotrz<=fdate and company = old.company
            into :blokadasid;
          if (:blokadasid = 1) then exception SID_FK_BLOKADAKSIEGOWA;
          --PR10957
          --execute procedure DOCUMENTS_DELFROM_BKDOCS(0,old.ref);
          execute procedure GET_CONFIG('SIDFK_ANULOWANIE', -1) returning_values :zerowanie;
          if (zerowanie = '') then zerowanie = '0';
          if (zerowanie = '1' and new.anulowanie <> 0 and old.anulowanie = 0) then
            execute procedure DOCUMENTS_DELFROM_decrees(0,old.ref);
          else if (zerowanie = '0' and new.anulowanie <> 0 and old.anulowanie = 0) then
            execute procedure DOCUMENTS_DELFROM_BKDOCS(0,old.ref);
          else
            execute procedure DOCUMENTS_DELFROM_BKDOCS(0,old.ref);
        end
        update NAGFAK set BLOKADA = 0 where BLOKADA = 3 and REF=old.ref;
      end
    end
  end
end^
SET TERM ; ^
