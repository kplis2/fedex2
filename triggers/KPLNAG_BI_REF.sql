--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KPLNAG_BI_REF FOR KPLNAG                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('KPLNAG')
      returning_values new.REF;
end^
SET TERM ; ^
