--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRPAYROLLSPOS_AD0 FOR ECONTRPAYROLLSPOS              
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  if (old.countnag = 0 and  old.points <> 0) then
    execute procedure ECONTRPAYROLLS_CALCULATE(old.econtrpayrollnag);
  if (old.accord = 2 and old.points <> 0 and old.econtractsdef is not null) then
    execute procedure ECONTRPAYROLLS_CALC_ADDINGS(old.econtrpayrollnag);
end^
SET TERM ; ^
