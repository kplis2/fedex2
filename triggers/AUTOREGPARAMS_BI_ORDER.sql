--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER AUTOREGPARAMS_BI_ORDER FOR AUTOREGPARAMS                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable maxnum integer;
begin
  new.ord = 1;
  select max(numer) from autoregparams where autoregschem = new.autoregschem
    into :maxnum;
  if (new.numer is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.numer = maxnum + 1;
  end else if (new.numer <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update autoregparams set ord = 0, numer = numer + 1
      where numer >= new.numer and autoregschem = new.autoregschem;
  end else if (new.numer > maxnum) then
    new.numer = maxnum + 1;
end^
SET TERM ; ^
