--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRPSNSALG_AIU0 FOR FRPSNSALG                      
  INACTIVE AFTER INSERT OR UPDATE POSITION 0 
AS
  declare variable proc varchar(8191);
  declare variable ret  numeric(14,4);
begin
  execute procedure STATEMENT_EXECUTE(new.decl, new.body,null,null, 0)
    returning_values :ret;
  when any
    do exception universal 'Błąd w definicji algorytmu  '||new.body;
end^
SET TERM ; ^
