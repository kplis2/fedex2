--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLADDFILES_BI_REF FOR EMPLADDFILES                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EMPLADDFILES')
      returning_values new.REF;
end^
SET TERM ; ^
