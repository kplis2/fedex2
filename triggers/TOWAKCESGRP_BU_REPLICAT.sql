--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWAKCESGRP_BU_REPLICAT FOR TOWAKCESGRP                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.ref<>old.ref
      or new.name<>old.name or (new.name is null and old.name is not null) or (new.name is not null and old.name is null)
      ) then
   waschange = 1;

  execute procedure rp_trigger_bu('TOWAKCESGRP',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
