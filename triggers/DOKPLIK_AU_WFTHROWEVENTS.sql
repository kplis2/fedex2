--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKPLIK_AU_WFTHROWEVENTS FOR DOKPLIK                        
  ACTIVE AFTER UPDATE POSITION 1 
AS
begin
  if(old.blokada is distinct from 0 and new.blokada is not distinct from 0)then
  begin
    execute procedure wf_throwevent('DOKPLIK_CHANGE','SENTE.DOKPLIK',new.ref,
      coalesce(new.symbol, new.plik, current_timestamp(0)));
  end
end^
SET TERM ; ^
