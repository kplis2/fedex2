--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_BI_ORDER FOR RKDOKPOZ                       
  ACTIVE BEFORE INSERT POSITION 1 
as
  declare variable maxnum integer;
begin
  new.ord = 1;
  select max(lp) from rkdokpoz where dokument = new.dokument
    into :maxnum;
  if (new.lp is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.lp = maxnum + 1;
  end else if (new.lp <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update rkdokpoz set ord = 0, lp = lp + 1
      where lp >= new.lp and dokument = new.dokument;
  end else if (new.lp > maxnum) then
    new.lp = maxnum + 1;
end^
SET TERM ; ^
