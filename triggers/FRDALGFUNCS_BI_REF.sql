--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDALGFUNCS_BI_REF FOR FRDALGFUNCS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FRDALGFUNCS')
      returning_values new.REF;
end^
SET TERM ; ^
