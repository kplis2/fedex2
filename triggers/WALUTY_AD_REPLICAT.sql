--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WALUTY_AD_REPLICAT FOR WALUTY                         
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  execute procedure RP_TRIGGER_AD('WALUTY', old.symbol, NULL, NULL, NULL, NULL, old.token, old.STATE);
end^
SET TERM ; ^
