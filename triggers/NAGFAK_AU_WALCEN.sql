--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_AU_WALCEN FOR NAGFAK                         
  ACTIVE AFTER UPDATE POSITION 1 
AS
declare variable konfigwalcen varchar(255);
begin
  if (coalesce(new.waluta,'') != coalesce(old.waluta,'')
    or coalesce(new.kurs,0) != coalesce(old.kurs,0)) then
  begin
    execute procedure get_config('WALCEN',2) returning_values :konfigwalcen;
    if (:konfigwalcen = '1') then begin
      update pozfak p set p.walcen = new.waluta, p.kurscen = new.kurs
        where p.dokument = new.ref and p.walcen = old.waluta;
    end
  end
end^
SET TERM ; ^
