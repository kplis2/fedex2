--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDES_AD0 FOR PRSCHEDGUIDES                  
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  if(old.amount > 0 and old.pggamountcalc = 0) then execute procedure prschedguidesgroup_amountcalc(old.zamowienie, old.pozzam, old.prschedzam);
  execute procedure prschedzam_obl(old.prschedzam);

  if (old.ref <> old.prschedguideorg) then
  begin
    -- uzupelanianie ilosci na oryginalnym przewodniku
    update prschedguides p set p.amount = p.amount + old.amount
      where ref = old.prschedguideorg;
    update prschedguidespos set amountzreal = amount  * kamountshortage * kamountnorm
      where prschedguide = old.prschedguideorg;
  end
end^
SET TERM ; ^
