--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDAWCY_BD_CHECKACCOUNTING FOR SPRZEDAWCY                     
  ACTIVE BEFORE DELETE POSITION 0 
as
  declare variable dictdef int;
begin
  select ref from slodef where typ = 'SPRZEDAWCY'
    into :dictdef;
  execute procedure check_dict_using_in_accounts(dictdef, old.kontofk, old.company);
end^
SET TERM ; ^
