--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FXDASSETCA_BU0 FOR FXDASSETCA                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable status smallint;
begin
  if (coalesce(old.amperiod,0) <> coalesce(new.amperiod,0)
    or (coalesce(old.share,0) <> coalesce(new.share,0))
    or (coalesce(old.account,'') <> coalesce(new.account,''))
  ) then
  begin
    select a.status
      from amperiods a
      where a.ref = new.amperiod
      into :status;
    if (status = 2) then
      exception fxdassetca_error 'Okres jest już zamkniety.';
    else if (status is null) then
      exception fxdassetca_error 'Nieprawidlowy okres';
    else
    begin
      if (new.share is null) then new.share = 0;
      if (new.share < 0) then
        exception fxdassetca_error 'Wartosc udzialu < 0';
      if (new.account is null) then
        exception fxdassetca_error 'Brak konta';
      /*if (not exists(select * from BKSYMBOLS b where b.symbol = new.account and b.sgroup = 'EFK')) then
        exception fxdassetca_error 'Nieprawidowy symbol konta';  */
    end 
  end 

end^
SET TERM ; ^
