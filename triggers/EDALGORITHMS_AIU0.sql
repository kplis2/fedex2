--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDALGORITHMS_AIU0 FOR EDALGORITHMS                   
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
AS
  declare variable proc1 varchar(1024);
  declare variable proc2 varchar(1024);
  declare variable proc3 varchar(1024);
begin
    proc1 = 'create or alter procedure XX_ED_ALGORITHM_' || new.ref || '_A
(key1 integer, key2 integer)
returns (ret varchar(40))
as
' || new.decl1 || '
begin
' || new.body1 || '
  suspend;
end;';
    execute statement proc1;
    proc2 = 'create or alter procedure XX_ED_ALGORITHM_' || new.ref || '_R' ||
'(key1 integer, key2 integer)
returns (ret varchar(40))
as
' || new.decl2 || '
begin
' || new.body2 || '
  suspend;
end;';
    execute statement proc2;

    proc3 = 'create or alter procedure XX_ED_ALGORITHM_' || new.ref || '_D' ||
'(key1 integer, key2 integer)
returns (ret varchar(1024))
as
' || new.decl3 || '
begin
' || new.body3 || '
  suspend;
end;';
    execute statement proc3;

end^
SET TERM ; ^
