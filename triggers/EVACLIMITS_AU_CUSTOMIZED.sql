--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACLIMITS_AU_CUSTOMIZED FOR EVACLIMITS                     
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
--DS: jezeli odznaczylismy ktorys znacznik to przelicz karte

  if (new.vyear <> old.vyear or new.employee <> old.employee
    or (old.actlimit_cust = 1 and new.actlimit_cust = 0)
    or (old.addlimit_cust = 1 and new.addlimit_cust = 0)
    or (old.addvdate_cust = 1 and new.addvdate_cust = 0)
    or (new.addvdate_cust = 1 and old.addvdate is distinct from new.addvdate)
    or (old.disablelimit_cust = 1 and new.disablelimit_cust = 0)
    or (old.traininglimit_cust = 1 and new.traininglimit_cust = 0)
  ) then
    execute procedure e_vac_card(new.vyear || '/1/1', new.employee);
end^
SET TERM ; ^
