--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRCALCCOLS_BI_ALT FOR PRCALCCOLS                     
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable opermaster integer;
begin
  -- jesli nie wybieramy zadnej alternatywy, to wybierz domyslna
  if(new.prshoperalt is null and new.isalt=1) then begin
    select operdefault from prshopers where ref=new.fromprshoper into new.prshoperalt;
  end
end^
SET TERM ; ^
