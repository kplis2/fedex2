--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPEDYTWYS_BU0 FOR SPEDYTWYS                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable rodzajwys smallint;
declare variable cnt integer;
begin
  if (new.spedytor <> old.spedytor or new.sposdost <> old.sposdost) then begin
    select s.rodzajwys from sposdost s where s.ref = new.sposdost into :rodzajwys;
    select count(s.sposdost) from spedytwys s where s.sposdost = new.sposdost into :cnt;

    if (coalesce(:rodzajwys,0) > 0 and coalesce(:cnt,0) > 0) then
      exception universal;
  end
end^
SET TERM ; ^
