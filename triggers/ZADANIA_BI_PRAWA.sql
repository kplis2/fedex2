--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZADANIA_BI_PRAWA FOR ZADANIA                        
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable rodzaj varchar(40);
declare variable kierunek varchar(10);
declare variable nazwa varchar(255);
declare variable opernazwa varchar(255);
begin
 if(new.cpodmiot=0) then new.cpodmiot = NULL;
 if(new.osoba=0) then new.osoba = NULL;
 if(new.prawadef is null) then new.prawadef = 0;
 if(new.prawadef=0) then begin
   if(new.cpodmiot is not null) then
     select PRAWA,PRAWAGRUP from CPODMIOTY where ref = new.cpodmiot
     into new.prawa, new.prawagrup;
   else begin
     new.prawa = '';
     new.prawagrup = '';
   end
 end
 execute procedure GET_SUPERIORS(new.prawa) returning_values new.prawa;
 if(new.cpodmiot is not NULL) then
   select SKROT,SLODEF,OPEROPIEK,TELEFON,COMPANY from CPODMIOTY where REF=new.CPODMIOT
   into new.cpodmskrot, new.cpodmslodef, new.cpodmoperopiek, new.cpodmtelefon, new.company;
 if(new.OSOBA is not NULL) then
   select NAZWA,TELEFON from PKOSOBY where REF=new.OSOBA
   into new.PKOSOBYNAZWA, new.PKOSOBYTELEFON;
 else begin
   new.PKOSOBYNAZWA = NULL;
   new.PKOSOBYTELEFON = NULL;
 end
 select substring(OPIS from 1 for 20) from DEFZADAN where REF=new.zadanie into :rodzaj;

 kierunek = ' do ';
 if(new.data is not null) then
   nazwa = cast(new.data as date) || ' ' || :rodzaj;
 else
   nazwa = :rodzaj;
 if(new.cpodmiot is not null) then nazwa = :nazwa || :kierunek || substring(new.cpodmskrot from 1 for 20);
 select NAZWA from OPERATOR where REF=new.operwyk into :opernazwa;
 nazwa = :nazwa || ' (' || :opernazwa || ')';
 new.nazwa = substring(:nazwa from 1 for 80);

 if(new.cpodmiot is not null) then
   select ancestors from CPODMIOTY where ref = new.cpodmiot into new.cpodmancestors;
end^
SET TERM ; ^
