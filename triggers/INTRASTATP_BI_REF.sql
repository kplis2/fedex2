--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INTRASTATP_BI_REF FOR INTRASTATP                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('INTRASTATP')
      returning_values new.REF;
end^
SET TERM ; ^
