--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_BU_PRD FOR DECREES                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable deducted numeric(14,2);
  declare variable cnt smallint;
begin
  -- BS80359 niech to sie wykonuje tylko dla dekretow dotyczacych RMK
  if (old.prdsymbol <> '' and (new.account <> old.account or new.side <> old.side or new.debit <> old.debit
    or new.credit <> old.credit or (new.status < 2 and old.status > 1))) then
    begin
      -- BS80359 czesciej jest tak ze deductedamount = 0 wiec niech sprawdzi ten 1 rekord
      select deductedamount
        from prdaccounting
        where decree = new.ref
        into :deducted;
      if (deducted > 0) then begin
        -- BS80359 a jak jest tam wartosc, to wtedy sprawdz czy odpisy sa z innych dokumentow
        select count(ref) from decrees
          where prdsymbol = old.prdsymbol
            and account = old.account
            and company = old.company
            and bkdoc <> old.bkdoc
          into :cnt;
        if (cnt>0) then exception prd_deducted;
      end
    end
end^
SET TERM ; ^
