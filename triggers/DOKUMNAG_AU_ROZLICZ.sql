--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_AU_ROZLICZ FOR DOKUMNAG                       
  ACTIVE AFTER UPDATE POSITION 4 
AS
declare variable czy_rozr smallint;
declare variable operacja varchar(20);
declare variable rozr varchar(20);
declare variable il integer;
declare variable kontofk ACCOUNT_ID;
declare variable winien numeric(14,2);
declare variable winienzl numeric(14,2);
declare variable kod varchar(20);
declare variable nazwa varchar(255);
declare variable btranref integer;
declare variable wydania smallint;
declare variable ma numeric(14,2);
declare variable mazl numeric(14,2);
declare variable termplat timestamp;
declare variable rozrachauto smallint;
begin
  if(new.akcept = 1 and old.AKCEPT <> 1) then begin
     /*akceptacja DOKUMENTY MAGAZYNOWEGO - moze trzeba stowrzyc rozrachunek*/
    select defdokum.rozliczeniowy, wydania from defdokum where symbol = new.TYP into :czy_rozr, :wydania;
    execute procedure GETCONFIG('ROZRACHAUTO') returning_values :rozrachauto;
    if(:czy_rozr > 0 and new.slodef > 0 and new.slopoz > 0 and :rozrachauto<>0) then begin
      if(new.termzap is not null) then
        termplat = new.termzap;
      else
        termplat = new.data;
        if(new.symbfak <> '') then
          rozr = new.symbfak;
        else
          rozr = new.symbol;
        if (:rozr is not null and :rozr <>'' and new.wartsbrutto > 0) then
        begin
          if (:kontofk is null or :kontofk = '') then
            execute procedure SLO_DANE(new.slodef, new.slopoz)
              returning_values :kod, :nazwa, :kontofk;
          winienzl = 0; mazl = 0;
          winien = 0; ma = 0;
          if(:wydania = 1) then begin
            winien = new.wartsbrutto;
            winienzl = winien;
          end else begin
            mazl = new.wartsbrutto;
            ma = mazl;
          end
          il = null;
          select count(*)
            from ROZRACH
            where SLOPOZ=new.slopoz
              and SLODEF = new.slodef
              and SYMBFAK=:rozr
              and KONTOFK=:kontofk
              and company = new.company
            into :il;
--          if ((il is null or il=0) and :czy_rozr = 1) then exception RK_BRAK_ROZRACHUNKU;
          if ((il is null or il=0) and :czy_rozr = 2) then
            insert into ROZRACH (company, SLODEF, SLOPOZ, SYMBFAK, DATAOTW, DATAPLAT, WALUTOWY, WALUTA, KONTOFK, SKAD)
              values(new.company, new.slodef, new.slopoz, :rozr, new.data, :termplat, 0, 'PLN', :kontofk, 8);
          operacja = new.symbol;
          insert into ROZRACHP(SLODEF, SLOPOZ, SYMBFAK, kontofk, OPERACJA, DATA, TRESC, WINIEN, MA, SKAD, STABLE, SREF, kurs, winienzl, mazl)
           values (new.slodef, new.slopoz, :rozr, :kontofk, :operacja, new.data, 'Akceptajca dok. magazynowego', :winien, :ma, 8, 'DOKUMNAG', new.ref, 1,:winienzl , :mazl);
        end
    end
  end
  else if(new.akcept <> 1 and old.akcept = 1) then begin
    /*kasujemy pozycje. Jeli sa to jedyne na rozrachunku, to skasują rónież rozrachunek*/
    delete from ROZRACHP where company = old.company and SLODEF = old.slodef and slopoz = old.slopoz and stable = 'DOKUMNAG' and SREF = new.ref;
  end
end^
SET TERM ; ^
