--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FSCLRACCH_AU_ACCEPTANCE FOR FSCLRACCH                      
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable local integer;
begin
  execute procedure CHECK_LOCAL('FSCLRACCH', old.ref) returning_values local;
  if (old.status < 1 and new.status = 1 and (new.fstype = 0 or new.fstypeakc = 1) and :local = 1) then begin
      execute procedure FS_REG_SETTLEMENTS_FROM_CRLACC(new.ref);
  end
end^
SET TERM ; ^
