--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWSSTOCK_BU_X_BLOCKED FOR MWSSTOCK                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
    --XXX Ldz ZG 126909
    if (new.x_blocked is distinct from old.x_blocked and new.x_blocked is null) then
      new.x_blocked = 0;
end^
SET TERM ; ^
