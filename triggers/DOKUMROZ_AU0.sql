--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMROZ_AU0 FOR DOKUMROZ                       
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable mag char(3);
declare variable wydania integer;
declare variable typ_mag char(1);
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable bktm varchar(40);
declare variable bwersja integer;
declare variable il numeric(15,4);
declare variable data timestamp;
declare variable dokument integer;
declare variable ilmin numeric(15,4);
declare variable ilplu numeric(15,4);
declare variable dt timestamp;
declare variable zamowienie integer;
declare variable refdokum integer;
declare variable cen numeric(14,4);
declare variable zdejblok numeric(14,4);
declare variable korekta smallint;
declare variable slodef integer;
declare variable slopoz integer;
declare variable bn varchar(1);
declare variable dokpozref integer;
declare variable opk smallint;
declare variable opktryb smallint;
declare variable back0 integer;
declare variable back1 integer;
declare variable pozycja integer;
declare variable numer integer;
declare variable koropkrozid integer;
declare variable stanyopkref integer;
declare variable oblblok smallint;
declare variable ilosc numeric(14,4);
declare variable operakcept integer;
declare variable ujemne smallint;
declare variable datadok timestamp;
declare variable stanyujemne smallint;
begin
   select DEFDOKUM.wydania, DEFDOKUM.KORYG,dokumnag.magazyn, defmagaz.typ,DOKUMPOZ.KTM,DOKUMPOZ.wersja,
     DOKUMNAG.REF, DOKUMPOZ.FROMZAM,DOKUMNAG.SLODEF, DOKUMNAG.SLOPOZ,DEFDOKUM.KAUCJABN, DOKUMNAG.data,
     DEFDOKUM.OPAK,DOKUMPOZ.OPK, DOKUMPOZ.REF, DOKUMPOZ.BKTM, DOKUMPOZ.BWERSJA, DOKUMNAG.OPERAKCEPT, DEFDOKUM.STANYUJEMNE
   from DOKUMPOZ
   left join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.DOKUMENT)
   left join DEFDOKUM on(DOKUMNAG.TYP = DEFDOKUM.symbol)
   left join DEFMAGAZ on ( DOKUMNAG.magazyn = DEFMAGAZ.symbol)
    where DOKUMPOZ.ref = new.pozycja
    into :wydania,:korekta,:mag,:typ_mag,:ktm,:wersja,
         :refdokum, :zamowienie,:slodef,:slopoz,:bn, :datadok,
         :opktryb,:opk,:dokpozref, :bktm, :bwersja, :operakcept, :stanyujemne;
 if(new.ack = 9) then begin
   ktm = :bktm;
   wersja = :bwersja;
 end
 if(:zamowienie is null) then zamowienie = 0;
 if(old.dostawa <> new.dostawa) then begin
   if(old.ack = 1 or old.ack = 9) then begin
     /*zmiana parti - dotyczy dokumentow przychodowych*/
     if(:wydania = 0) then begin
        /* najpierw dodaje nowe stany, potem usuwam stare */
        select DATA from STANYCEN where MAGAZYN = :mag and KTM = :ktm and WERSJA = :wersja
          and CENA = old.cena and dostawa = old.dostawa into :data;
        execute procedure PLUS_SC(:mag,:ktm,:wersja,old.ilosc,old.cena,new.dostawa,new.serialnr,:data,new.cena_koszt,new.orgdokumser,:datadok);
        execute procedure MINUS_SC(:mag,:ktm,:wersja,old.ilosc,old.cena,old.dostawa,old.serialnr,NULL,NULL,old.orgdokumser,:datadok);
     end
   end
 end/*koniec zmian zwiazanych ze zmiana parametrow rozpiski*/
 /*pozostala kwestia ilosci */
 if(((new.ack = 9 or old.ack = 7) and old.ack = 1) or (new.ack = 1 and (old.ack = 9 or old.ack = 7) )) then oblblok = 1;
 else oblblok = 0;
 if(((old.ilosc <> new.ilosc) or (new.cena <> old.cena) or (new.ack <> old.ack)) and (new.ack <> 8) and (new.ack <> 7)
  and :oblblok = 0) then begin
     ilmin = 0; /*ilosc zmniejszenia rozpiski*/
     ilplu = 0; /*ilosc zwiekszenia rozpiski*/
     dt = current_date;
     if(new.ack = 1 and old.ack = 1) then begin
      /* byl zaakcepotowany */
       /* jesli tylko zmiana ilosci_ */
       if(((new.ilosc <> old.ilosc) and (new.cena = old.cena)) or (new.notaupdate = 1)) then
          ilplu = new.ilosc- old.ilosc;
       else begin
           ilmin = old.ilosc;
          ilplu = new.ilosc;
       end
     end else if(new.ack = 1 and old.ack = 0) then
       ilplu = new.ilosc;
     else if(new.ack = 0 and old.ack = 1) then
       ilmin = old.ilosc;
     if(:ilplu < 0) then begin ilmin = ilmin -ilplu; ilplu = 0; end
     if(:ilmin < 0) then begin ilplu = ilplu -ilmin;ilmin = 0;end
     if(:ilmin <> 0 or ilplu <> 0)then begin
        if((:wydania = 0 and ilmin > 0) or (:wydania = 1 and ilplu > 0)) then begin
          /*zmniejszenie stanu magazynowego*/
          zdejblok = 0;
          if(:wydania = 0) then begin
             il = :ilmin;
             cen = old.cena;
          end else begin
             il = :ilplu;
             cen = new.cena;
          end
          if(:wydania = 0 or (:wydania = 1 and :korekta = 1)) then begin
            /* zmniejszenie przyjecia - przed zmniejszeniem stanow moze byc konieczne przesuniecie z blokad na do zablokowania */
            /* lub zwiekszenie wydania korygującego - tak samo udostpnienie stanów przez przesunicie */
            execute procedure REZ_BLOK_PRZESUN(:mag,:ktm,:wersja,:cen, new.dostawa,:il);
          end else begin
            /*zwiekszenie wydania - przed zmniejszeniem stanow zwolnienie blokad i sprawdzenie mozliwosci realizowania */
            execute procedure REZ_BLOK_REALIZE(:zamowienie, :refdokum, :mag,:ktm,:wersja,:cen,new.dostawa, :il) returning_values :zdejblok;
           wydania = :wydania;
          end
          execute procedure REZ_BLOK_CHECK(new.pozycja, new.numer,:il,:zdejblok);
          execute procedure MINUS_SC(:mag,:ktm,:wersja,:il,old.cena,new.dostawa,new.serialnr, dt,new.cena_koszt,new.orgdokumser,:datadok);
          -- kontrola mozliwosci zejscia na stany ujemne dla operatora
          select ILOSC from STANYIL where MAGAZYN=:mag and KTM=:ktm and WERSJA=:wersja into :ilosc;
          if(:ilosc<0) then begin
            select UJEMNE from OPERMAG where MAGAZYN=:mag and OPERATOR = :operakcept into :ujemne;
            if(:ujemne is null) then ujemne = 0;
            if(:ujemne=0) then exception STIL_MINUS 'Brak uprawnień operatora do zejścia na stan ujemny dla: '||:ktm;
            if(:stanyujemne is null) then stanyujemne = 0;
            if(:stanyujemne=0) then exception STIL_MINUS 'Brak uprawnień typu dok. do zejścia na stan ujemny dla: '||:ktm;
          end
          /*zmniejszenie wydania - przywrocenie stanow zamowien dostaw*/
          if(:wydania = 0) then
            execute procedure REZ_DOST_DEREALIZE(:refdokum,:ktm,:wersja,:cen,new.dostawa,new.ilosc);
       end
       if((:wydania = 0 and ilplu > 0) or (:wydania = 1 and ilmin > 0)) then begin
         /*zwikszenie stanu magazynowego*/
         if(:wydania = 0) then begin
            il = :ilplu;
            cen = new.cena;
         end else begin
            il = :ilmin;
            cen = old.cena;
         end
         execute procedure PLUS_SC(:mag,:ktm,:wersja,:il,:cen,new.dostawa,new.serialnr,dt,new.cena_koszt,new.orgdokumser,:datadok);
         if(:wydania = 0 or (:wydania = 1 and :korekta = 1)) then  begin
           execute procedure REZ_DOST_REALIZE(:zamowienie, :refdokum, :mag,:ktm,:wersja,:cen,new.dostawa, :il) returning_values :zdejblok;
           execute procedure REZ_BLOK_ROZPISZ(:mag,:ktm,:wersja);
           /*procedura zbadania ilosci mozliwych do przepisania po zwiekszeniu dostawy z do zablokowania na blokady*/
         end else begin
           /*zmniejszenie rozchodu - cześc z zrealizowanych musi wrocic na zablokowano */
           execute procedure REZ_BLOK_DEREALIZE(:refdokum,:ktm,:wersja,:cen,new.dostawa,new.ilosc);
               /*dlatego new.ilosc, bo ma byc przekazana nowa ilosc rozchodowana przez pozycje rozpiski, by stwierdzic, ile powinno byc cofniete z realizacji blokad.
                 mechanizm taki, bo pozycja mogla miec wiecej niz realizowane blokady, najpierw wiec "oddaje" to, co wziela wiecej*/
         end
       end
       back0 = 0;
       back1 = 1;
       pozycja = new.pozycja;
       numer = new.numer;
       if(new.kortoroz > 0) then begin
         select DEFDOKUM.OPAK, DEFDOKUM.kaucjabn, dokumpoz.ref, dokumroz.numer, dokumpoz.opk
         from DOKUMROZ
         left join dokumpoz on (dokumroz.pozycja = dokumpoz.ref)
         left join DOKUMNAG on (DOKUMNAG.ref = dokumpoz.dokument)
         left join defdokum on (defdokum.symbol = dokumnag.typ)
         where dokumroz.ref = new.kortoroz
         into :opktryb, :bn, :dokpozref, :numer, :opk;
         back0 = 1;
         back1 = 0;
         pozycja = dokpozref;
       end
       if(:opktryb = 1 and :opk > 0) then begin
          /*naliczanie stanów opakowan*/
          if(new.notaupdate = 1 and new.cena <> old.cena)then begin
            select max(ref) from STANYOPK where SLODEF = :slodef and SLOPOZ = :slopoz
              and POZDOKUM = :DOKPOZREF AND KTM = :ktm and WERSJA = :wersja and CENAMAG = old.cena
            into :stanyopkref;
            if(:stanyopkref > 0) then
              update STANYOPK set  CENAMAG = new.cena, WARTMAG = ILOSC * new.cena  where REF =:stanyopkref;
          end else if(new.cena <> old.cena or (new.cenasnetto <> old.cenasnetto) or (new.ack <> old.ack)) then begin
            /*wycofanie i naliczenie na nowo*/
            if(:bn = 'B') then begin
              if(old.ack = 1) then
                execute procedure OPK_ADD(:slodef,:slopoz,:ktm,:wersja,old.ilosc,old.cena,old.cenasbrutto,:dokpozref,:back1);
              if(new.ack = 1) then
                execute procedure OPK_ADD(:slodef,:slopoz,:ktm,:wersja,new.ilosc,new.cena,new.cenasbrutto,:dokpozref,:back0);
            end else begin
              if(old.ack = 1) then begin
                execute procedure OPK_ADD(:slodef,:slopoz,:ktm,:wersja,old.ilosc,old.cena,old.cenasnetto,:dokpozref,:back1);
              end
              if(new.ack = 1) then begin
                execute procedure OPK_ADD(:slodef,:slopoz,:ktm,:wersja,new.ilosc,new.cena,new.cenasnetto,:dokpozref,:back0);
              end
            end
          end else if(new.ilosc > old.ilosc) then begin
            if(:bn = 'B') then
              execute procedure OPK_ADD(:slodef,:slopoz,:ktm,:wersja,:ilplu,new.cena,new.cenasbrutto,:dokpozref,:back0);
            else
              execute procedure OPK_ADD(:slodef,:slopoz,:ktm,:wersja,:ilplu,new.cena,new.cenasnetto,:dokpozref,:back0);
          end else if(new.ilosc < old.ilosc) then begin
            if(:bn = 'B') then
              execute procedure OPK_ADD(:slodef,:slopoz,:ktm,:wersja,:ilmin,new.cena,new.cenasbrutto,:dokpozref,:back1);
            else
              execute procedure OPK_ADD(:slodef,:slopoz,:ktm,:wersja,:ilmin,new.cena,new.cenasnetto,:dokpozref,:back1);
          end
       end
     end
 end
 if(new.ilosc <> old.ilosc) then
   update DOKUMPOZ set ILOSCROZ = ILOSCROZ +(new.ilosc - old.ilosc)  where ref=new.pozycja;
 if(new.iloscl <> old.iloscl) then
   update DOKUMPOZ set ILOSCL = ILOSCL + (new.iloscl - old.iloscl) where ref = new.pozycja;
 if((new.wartosc <> old.wartosc) and new.blokoblnag =0) then begin
   select dokument from DOKUMPOZ where ref=new.pozycja into :dokument;
   execute procedure DOKMAG_OBL_WAR(:dokument,0);
 end
 if(new.cena <> old.cena and new.notaupdate = 1 and :wydania = 0) then begin
   if (not exists(select pozycja from DOKUMROZ where POZYCJA = new.pozycja and ref <> new.ref and CENA <> new.cena)) then
     update DOKUMPOZ set CENA = new.cena where ref=new.pozycja and CENA = old.cena;
 end
 if(new.ack <> 8 and new.ack <> 7) then begin
   /*jesli to korekta ilosciowa. to uaktualnienie ilosci na dokumencie oryginalnym*/
   if(new.kortoroz> 0) then begin
     if(new.ack > 0 and old.ack = 0) then
       update DOKUMROZ set ILOSCL = ILOSCL - new.ilosc  where REF=new.kortoroz;
     else if (new.ack = 0 and old.ack > 0) then
       update DOKUMROZ set ILOSCL = ILOSCL + old.ilosc  where REF=new.kortoroz;
     else if(new.ilosc <> old.ilosc) then
       update DOKUMROZ set ILOSCL = ILOSCL - (new.ilosc - old.ilosc) where REF=new.kortoroz;
     /*korekta ilosciowa rozpiski rozliczajacej*/
     if((new.ilosc <> old.ilosc) or (new.ack <> old.ack))then begin
       select opkrozid from DOKUMROZ where ref=new.kortoroz into :koropkrozid;
       if(:koropkrozid > 0) then
         execute procedure OPK_STANYAKTUREAL(:koropkrozid);
     end
   end
   if((new.opkrozid <> old.opkrozid) or (new.opkrozid is null and old.opkrozid is not null) or (new.opkrozid is not null and old.opkrozid is null))then begin
     if(old.opkrozid is not null) then
       execute procedure OPK_STANYAKTUREAL(old.opkrozid);
     if(new.opkrozid is not null) then begin
       execute procedure OPK_STANYAKTUREAL(new.opkrozid);
     end
   end else if(new.opkrozid = old.opkrozid and (new.ilosc <> old.ilosc or (new.cena <> old.cena) or (new.cenasbrutto <> old.cenasbrutto) or (new.cenasnetto <> old.cenasnetto) or (new.ack <> old.ack))) then
     execute procedure OPK_STANYAKTUREAL(new.opkrozid);

   if(new.serialnr <> old.serialnr) then begin
     execute procedure SERNR_DEL_ROZ(new.pozycja, old.ilosc, old.serialnr);
     execute procedure sernr_add_roz(new.pozycja, new.ilosc, new.serialnr);
   end else if(new.ilosc < old.ilosc and new.serialnr <> '') then begin
     il = old.ilosc - new.ilosc;
     execute procedure sernr_del_roz(new.pozycja, :il, new.serialnr);
   end else if(new.ilosc > old.ilosc and new.serialnr <> '') then begin
     il = new.ilosc - old.ilosc;
     execute procedure sernr_add_roz(new.pozycja, :il, new.serialnr);
   end
   if(new.notaupdate = 1) then
     execute procedure DOKPOZ_OBL_FROMROZ(new.pozycja);
 end
end^
SET TERM ; ^
