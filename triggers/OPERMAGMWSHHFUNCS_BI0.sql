--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OPERMAGMWSHHFUNCS_BI0 FOR OPERMAGMWSHHFUNCS              
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null) then
    execute procedure gen_ref('OPERMAGMWSHHFUNCS') returning_values new.ref;
end^
SET TERM ; ^
