--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ODDZIALY_AI0 FOR ODDZIALY                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable odd varchar(255);
declare variable cnt smallint;
begin
  execute procedure GETCONFIG('AKTUODDZIAL') returning_values :odd;
  if(:odd = new.symbol) then begin
    select count(*) from KONFIG where AKRONIM = 'AKTULOCAT' into :cnt;
    if(:cnt > 0) then
      update KONFIG set WARTOSC = new.location where AKRONIM = 'AKTULOCAT';
    else
      insert into KONFIG(AKRONIM,WARTOSC,ORD) values('AKTULOCAT',new.location,1);
  end

end^
SET TERM ; ^
