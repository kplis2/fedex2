--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_AD_AMPERIODS FOR BKDOCS                         
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  --okres amortyzacji zaksigowany -> zamknity
  if (old.otable = 'AMPERIODS' and coalesce(old.oref,0) > 0) then
    update amperiods set status = 1 where ref = old.oref;
end^
SET TERM ; ^
