--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHORTAGES_BU0 FOR PRSHORTAGES                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable prschedopernumber integer;
declare variable prschedoperdeclnumber integer;
begin
  if (new.shortageratio is null) then new.shortageratio = 1;
  if (new.amount is null) then new.amount = 0;
  if (new.amountreal is null ) then new.amountreal = 0;
  if (new.amountzreal is null) then new.amountzreal = 0;
  if((new.prnagzam is null or new.prpozzam is null) and new.prschedguidespos is not null) then begin
    select pg.zamowienie, pp.pozzamref
      from prschedguidespos pp
      left join prschedguides pg on (pp.prschedguide = pg.ref)
      where pp.ref = new.prschedguidespos
    into new.prnagzam, new.prpozzam;
  end
  if((new.prschedoperdecl <> old.prschedoperdecl) or (new.prschedoperdecl is not null and old.prschedoperdecl is null)) then begin
    select number from prschedopers where ref = new.prschedoperdecl into :prschedoperdeclnumber;
    select number from prschedopers where ref = new.prschedoper into :prschedopernumber;
    if(:prschedoperdeclnumber > :prschedopernumber) then
      exception prshortages_error 'Numer operacji deklarowanej nie może być wyższy od numeru operacji bieżącej';
  end
  if(new.prschedoper is not null and new.prschedguide is null) then
    select guide from prschedopers where ref = new.prschedoper into new.prschedguide;
  if(new.dokumnag is null and old.dokumnag is not null) then new.amountzreal = 0;
  if(new.amount <> old.amount and new.dokumnag is not null) then
    exception prshortages_error 'Brak zaewidencjonowany na dok.mag. Zmiana ilości niemożliwa';

  if (old.prshortagestype <> new.prshortagestype) then
    select p.noamountinfluence
      from prshortagestypes p
      where p.symbol = new.prshortagestype
      into new.noamountinfluence;

  if ((old.amount <> new.amount or old.amountzreal <> new.amountzreal)
    and new.amount < coalesce(new.amountzreal,0)
  ) then
    exception prshortages_error 'Ilosc zrealizowanych braków jest wieksza od ilosci na raporcie';
end^
SET TERM ; ^
