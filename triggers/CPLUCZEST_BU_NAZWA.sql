--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLUCZEST_BU_NAZWA FOR CPLUCZEST                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable tryb integer;
declare variable nazwa varchar(255);
begin
  select SL.trybred, CP.nazwa from cpodmioty CP left join slodef SL on (CP.slodef = SL.Ref) where CP.ref = new.cpodmiot
  into :tryb, :nazwa;
  if (:tryb = 2) then  new.nazwa = :nazwa ;
end^
SET TERM ; ^
