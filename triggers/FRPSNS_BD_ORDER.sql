--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRPSNS_BD_ORDER FOR FRPSNS                         
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update frpsns set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and frhdr = old.frhdr;
end^
SET TERM ; ^
