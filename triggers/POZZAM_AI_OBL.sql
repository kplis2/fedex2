--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_AI_OBL FOR POZZAM                         
  ACTIVE AFTER INSERT POSITION 3 
as
declare variable local smallint;
begin
  if(new.genpozref > 0) then  /*jesli pozycja pochodzna */
    execute procedure POZZAM_OBL(new.genpozref);
  if(new.popref > 0) then /*jesli pozycja - klon*/
    execute procedure POZZAM_OBL(new.popref);
  execute procedure check_local('POZZAM',new.ref) returning_values :local;
  if(:local = 1) then begin
    execute procedure OBLICZ_NAGZAM(new.zamowienie);
  end
  if (new.prschedguidepos is not null) then
    execute procedure prschedguidedet_change(0,'POZZAM',new.ref, new.prschedguidepos);
end^
SET TERM ; ^
