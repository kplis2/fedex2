--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_BU_SERIAL FOR DOKUMPOZ                       
  ACTIVE BEFORE UPDATE POSITION 2 
as
declare variable nserial integer;
begin
  if(new.ktm is not null and (new.wersjaref <> old.wersjaref)) then begin
    new.serialized = 0;
    select SERIAL from TOWARY where KTM = new.ktm into new.serialized;
    if(new.serialized = 1) then begin
      select NOTSERIALIZED from DOKUMNAG where REF=new.dokument into nserial;
      if(:nserial = 1) then new.serialized = 0;
    end
  end
  if(new.serialized <> old.serialized) then
    delete from DOKUMSER where TYP = 'M' and REFPOZ=new.ref;
end^
SET TERM ; ^
