--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRPSNS_BU_POSITION FOR FRPSNS                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable symbol varchar(15);
begin
  if (new.symbol <> old.symbol) then
    if (exists(select fr.symbol from frpsns fr
                 where fr.frhdr = new.frhdr
                   and fr.symbol = new.symbol)) then
      exception FRPSNS_EXPT 'Pozycja o symbolu ' || new.symbol || ' już istnieje!';
end^
SET TERM ; ^
