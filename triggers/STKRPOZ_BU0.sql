--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STKRPOZ_BU0 FOR STKRPOZ                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.ismaska=0) then begin
    if(new.wersjaref <> old.wersjaref and new.wersjaref is not null and new.wersjaref <> 0) then
      select nrwersji from WERSJE where ref=new.wersjaref into new.wersja;
    else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)) then
      select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
    if(new.ktm <> old.ktm and new.ktm <> '')then
      select NAZWA, MIARA from TOWARY where KTM = new.ktm into new.nazwat, new.miara;
    else if (new.ktm = '') then begin
      new.nazwat = ''; new.miara = '';
    end
  end else begin
    new.wersjaref = null;
    new.wersja = 0;
    new.nazwat = '';
    new.miara = '';
  end
  if(new.ilmax <> old.ilmin or new.ilmax <> old.ilmax) then
    new.lastdatachange = current_time;
end^
SET TERM ; ^
