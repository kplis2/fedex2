--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDES_BU_BLOCK FOR PRSCHEDGUIDES                  
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable gref integer;
begin
  if(new.schedblocked < 2) then
  begin
    if(new.schedblocked = 0) then update prschedzam pz set pz.schedblocked = 2 where pz.ref = new.prschedzam;
    if(new.schedblocked = 1) then update prschedzam pz set pz.schedblocked = 3 where pz.ref = new.prschedzam;
    for select ref from prschedopers po where po.guide = new.ref into :gref
    do begin
      if(new.schedblocked = 0) then
      update prschedopers po set po.schedblocked = 2 where po.ref = :gref;
      if(new.schedblocked = 1) then
      update prschedopers po set po.schedblocked = 3 where po.ref = :gref;
    end
  end
  else if(new.schedblocked = 2) then
  begin
    new.schedblocked = 0;
    update prschedzam pz set pz.schedblocked = 2 where pz.ref = new.prschedzam;
  end
  else if(new.schedblocked = 3) then
  begin
    new.schedblocked = 1;
    update prschedzam pz set pz.schedblocked = 3 where pz.ref = new.prschedzam;
  end
end^
SET TERM ; ^
