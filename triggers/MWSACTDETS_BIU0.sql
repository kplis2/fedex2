--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTDETS_BIU0 FOR MWSACTDETS                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.quantity is null) then new.quantity = 0;
  if (new.quantityc is null) then new.quantityc = 0;
  if (new.quantityl is null) then new.quantityl = 0;
  if (new.docid is null) then
    exception DOCID_NOT_DEFINED;
end^
SET TERM ; ^
