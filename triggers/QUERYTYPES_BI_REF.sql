--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER QUERYTYPES_BI_REF FOR QUERYTYPES                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('QUERYTYPES')
      returning_values new.REF;
end^
SET TERM ; ^
