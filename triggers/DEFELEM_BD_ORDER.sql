--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFELEM_BD_ORDER FOR DEFELEM                        
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then
    exit;
  select max(numer) from defelem where katalog = old.katalog
     into :maxnum;
  if (old.numer = :maxnum) then
    exit;
  update defelem set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and katalog = old.katalog;
end^
SET TERM ; ^
