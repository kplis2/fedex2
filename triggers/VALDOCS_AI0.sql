--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VALDOCS_AI0 FOR VALDOCS                        
  ACTIVE AFTER INSERT POSITION 0 
as
begin
  if (new.liquidation = 1) then
  begin
    update fxdassets
      set activ = 0, liquidationdate = new.operdate, liquidationperiod = new.amperiod
      where ref = new.fxdasset;
  end

  if (new.tvalue is not null) then
    update fxdassets set tvaluegross = coalesce(tvaluegross,0) + new.tvalue where ref = new.fxdasset;
  if (new.fvalue is not null) then
    update fxdassets set fvaluegross = coalesce(fvaluegross,0) + new.fvalue where ref = new.fxdasset;
  if (new.tredemption is not null) then
    update fxdassets set tredemptiongross = coalesce(tredemptiongross,0) + new.tredemption where ref = new.fxdasset;
  if (new.fredemption is not null) then
    update fxdassets set fredemptiongross = coalesce(fredemptiongross,0) + new.fredemption where ref = new.fxdasset;

  if (exists(select first 1 1 from vdoctype
               where amcorrection = 1 and symbol = new.doctype)
  ) then
    insert into amortization (fxdasset, amperiod, tamort, ttype, famort, ftype,correction, cor_amperiod)
      values (new.fxdasset, new.operiod, new.tredemption, 2, new.fredemption, 2, new.ref, new.corperiod);
end^
SET TERM ; ^
