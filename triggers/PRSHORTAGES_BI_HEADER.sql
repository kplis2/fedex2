--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHORTAGES_BI_HEADER FOR PRSHORTAGES                    
  ACTIVE BEFORE INSERT POSITION 3 
as
declare variable ktm VARCHAR(40);
declare variable one smallint;
declare variable amount numeric(14,4);
declare variable prschedguidespos INTEGER;
declare variable shortageratio numeric(14,4);
declare variable backasresourse integer;
declare variable wersjaref integer;
declare variable header smallint;
declare variable procsql varchar(100);
declare variable stmt varchar(1000);
begin
  select coalesce(t.header,0), procsql, t.backasresourse
    from prshortagestypes t
    where t.symbol =  new.prshortagestype
    into header, procsql, backasresourse;

  if (header = 1 and new.headergroup is null) then
    new.headergroup = new.ref;
  if (new.headergroup is null) then
    new.headergroup = new.ref;

  if (new.headergroup is not null and new.headergroup = new.ref and header = 1) then
  begin
    one = 0;
    for
      select p.ktm, p.ref, p.wersjaref
        from prschedguidespos p
        where p.prschedguide = new.prschedguide
          and p.prschedopernumber <= (select s.number from prschedopers s where s.ref = new.prschedoper)
        into ktm, prschedguidespos, wersjaref
    do begin
      if (:one = 0) then
      begin
        one = 1;
        if (backasresourse = 0) then
        begin
          select g.ktm, w.ref
            from prschedguides g
              left join wersje w on (w.ktm = g.ktm and w.nrwersji = g.wersja)
            where g.ref = new.prschedguide
            into new.ktm, new.wersjaref;
        end else begin
          new.ktm = :ktm;
          new.wersjaref = :wersjaref;
          if (procsql is not null and procsql <> '') then
          begin
            stmt = 'select ktm, wersjaref, ratio from '||procsql||'('||new.ref||','''||new.ktm||''','||new.wersjaref||')';
            execute statement stmt into new.ktm, new.wersjaref, new.shortageratio;
          end
        end
        new.prschedguidespos = :prschedguidespos;
        execute procedure prshortage_amount_calc(new.headergroupamount, new.prshortagestype, new.prschedguidespos, new.prschedoper)
          returning_values new.amount;
      end else begin
        if (:backasresourse = 1) then
        begin
          select m.reqktmtag
            from prshmat m
              join prschedguidespos g on (g.prshmat = m.ref)
            where g.ref = new.prschedguidespos
            into new.reqktmtag;
          if (procsql is not null and procsql <> '') then
          begin
            stmt = 'select ktm, wersjaref, ratio from '||procsql||'('||new.ref||','''||:ktm||''','||:wersjaref||')';
            execute statement stmt into ktm, wersjaref, shortageratio;
          end
          execute procedure prshortage_amount_calc(new.headergroupamount, new.prshortagestype, new.prschedguidespos, new.prschedoper)
            returning_values :amount;
          insert into prshortages (ktm, prshortagestype, person, employee, amount,
            prschedguidespos, prschedguide, prschedoper, prdepart,
            prschedzam, prschedoperdecl, operator, regdate, shortageratio,
            headergroup, headergroupwersjaref, headergroupamount,wersjaref)
          values(:ktm, new.prshortagestype, new.person, new.employee, :amount,
              :prschedguidespos, new.prschedguide, new.prschedoper, new.prdepart,
              new.prschedzam, new.prschedoperdecl, new.operator, new.regdate, :shortageratio,
              new.ref, new.headergroupwersjaref, new.headergroupamount,:wersjaref);
        end
      end
    end 
  end
  else
  begin
    if (procsql is not null and procsql <> '') then
    begin
      stmt = 'select ktm, wersjaref, ratio from '||procsql||'('||new.ref||','''||new.ktm||''','||new.wersjaref||')';
      execute statement stmt into new.ktm, new.wersjaref, new.shortageratio;
    end
    new.amount = new.headergroupamount;
  end
end^
SET TERM ; ^
