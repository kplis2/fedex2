--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ARTYKULY_BI FOR ARTYKULY                       
  ACTIVE BEFORE INSERT POSITION 1 
AS
BEGIN
   execute procedure REPLICAT_STATE('ARTYKULY',new.state,new.ref, NULL, NULL, NULL) returning_values new.state;
END^
SET TERM ; ^
