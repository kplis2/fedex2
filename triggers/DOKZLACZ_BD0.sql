--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKZLACZ_BD0 FOR DOKZLACZ                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if(old.ztable = 'CKONTRAKTY' and exists (select ref from dokplik where ref = old.dokument and ckontrakt = old.zdokum)) then
    exception universal 'Dokument utworzony z bieżącej sprawy. Odłączenie niedozwolone';
end^
SET TERM ; ^
