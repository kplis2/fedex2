--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_AD_FK FOR NAGFAK                         
  ACTIVE AFTER DELETE POSITION 3 
as
declare variable konf varchar(255);
declare variable bkref integer;
declare variable blokadasid smallint;
begin
  if((old.akceptacja = 1) or (old.akceptacja = 8)) then begin
    --sprawdzenie czy okres ksiegowy dla company jest juz zablokowany
    if (old.zakup = 0) then
      select min(sidblocked) from bkperiods
        where ptype=1 and old.data>=sdate and old.data<=fdate and company = old.company
        into :blokadasid;
    else
      select min(sidblocked) from bkperiods
        where ptype=1 and old.dataotrz>=sdate and old.dataotrz<=fdate and company = old.company
        into :blokadasid;
    if (:blokadasid = 1) then exception SID_FK_BLOKADAKSIEGOWA;
    if(old.zakup = 0) then
      execute procedure GETCONFIG('SIDFK_SPRBLOK') returning_values :konf;
    else
      execute procedure GETCONFIG('SIDFK_ZAKBLOK') returning_values :konf;
    if(:konf = '1' or (:konf = '2')) then begin
      select max(ref) from BKDOCS where OTABLE='NAGFAK' and OREF = old.ref into :bkref;
      if(:bkref > 0) then begin
        if(:konf = '1') then exception NAGFAK_ZAKSIEGOWANY;
        else begin
          if (old.zakup = 0) then
            select sidblocked from bkperiods
              where ptype = 1 and old.data>=sdate and old.data<=fdate and company = old.company
            into :blokadasid;
          else
            select sidblocked from bkperiods
              where ptype = 1 and old.dataotrz>=sdate and old.dataotrz<=fdate and company = old.company
            into :blokadasid;
          if (:blokadasid = 1) then exception SID_FK_BLOKADAKSIEGOWA;
          execute procedure DOCUMENTS_DELFROM_BKDOCS(0,old.ref);
        end
      end
    end
  end
end^
SET TERM ; ^
