--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHMAT_BD_ORDER FOR PRSHMAT                        
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update prshmat set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and sheet = old.sheet;
end^
SET TERM ; ^
