--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_BD0 FOR DOKUMNAG                       
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable chronologia smallint;
declare variable cnt integer;
declare variable sblokadadelay varchar(255);
declare variable blokadadelay integer;
declare variable numberg varchar(20);
declare variable local smallint;
declare variable numblock integer;
begin
  /* procedura odakceptowania tutaj, bo w trigerze AFTER już nie ma pozycji - usuniete przez foregin key'a */
  if(old.AKCEPT = 8) then update DOKUMNAG set AKCEPT=7 where REF=old.ref;

  if(old.akcept = 9 and exists(select first 1 1 from dokumpoz where dokumpoz.dokument = old.ref)) then
    exception dopoprawy_pozycje;

  if(old.AKCEPT = 1 or old.akcept = 9) then begin
   select chronologia from DEFMAGAZ where symbol=old.magazyn into :chronologia;
   if(:chronologia is null) then chronologia = 0;
   /*kontrola chronologii*/
   if(:chronologia > 0) then begin
      cnt = null;
      select count(*) from DOKUMNAG where MAGAZYN=old.magazyn and DATA>=(old.data+:chronologia) and akcept = 1 into :cnt;
      if(:cnt is null) then cnt = 0;
      if(:cnt > 0) then
        exception DOKUMNG_AKCEP_POZNIEJ;
   end
   execute procedure GETCONFIG('SIDFK_BLOKADA') returning_values :sblokadadelay;
   if(old.blokada > 0 and old.token <> 7) then begin
     if(:sblokadadelay <> '') then
       blokadadelay = cast(:sblokadadelay as integer);
     else blokadadelay = 0;
     if(bin_and(old.blokada,1)>0) then exception DOKUMNAG_USUBLOK;
     if(bin_and(old.blokada,4)>0 and current_date >= old.data + :blokadadelay) then exception DOKUMNAG_ZAKSIEGOWANY;
   end
   execute procedure DOKUMNAG_ACK(old.ref,0);
  end else
    if(old.blokada > 0 and old.token <> 7) then
      exception DOKUMNAG_USUBLOK;
  /*sprawdzenie, czy nie szostaną osamotnione faktury*/
  if(old.faktura > 0 and exists(select ref from NAGFAK where SKAD = 2 and NAGFAK.REF = old.faktura)) then
    exception DOKUMNAG_EXPT 'Dokument magazynowy zostal wyfakturowany. Usuniecie niemozliwe.';
  delete from DOKUMPOZ where dokument = old.ref;

end^
SET TERM ; ^
