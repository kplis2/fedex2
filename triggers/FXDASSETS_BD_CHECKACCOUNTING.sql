--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FXDASSETS_BD_CHECKACCOUNTING FOR FXDASSETS                      
  ACTIVE BEFORE DELETE POSITION 0 
AS
  declare variable dictdef integer;
begin
  select ref from slodef where typ = 'FXDASSETS'
    into :dictdef;
  execute procedure check_dict_using_in_accounts(dictdef, old.symbol, old.company);
end^
SET TERM ; ^
