--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SEGREGATOR_BI_ORDER FOR SEGREGATOR                     
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable maxnum integer;
begin
  new.ord = 1;
  select max(numer) from segregator where rodzic = new.rodzic
    into :maxnum;
  if (new.numer is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.numer = maxnum + 1;
  end else if (new.numer <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update segregator set ord = 0, numer = numer + 1
      where numer >= new.numer and rodzic = new.rodzic;
  end else if (new.numer > maxnum) then
    new.numer = maxnum + 1;
end^
SET TERM ; ^
