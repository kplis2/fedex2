--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EXECUTION_TMP_BIU_PVALUE FOR EXECUTION_TMP                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (inserting or new.dvalue <> old.dvalue or new.tval <> old.tval) then
  begin
    if ((sign(new.dvalue) = sign(new.tval) and abs(new.dvalue) > abs(new.tval))
      or (sign(new.dvalue) <> sign(new.tval) and new.dvalue > new.tval)
    ) then
      new.pvalue = new.tval;
    else
      new.pvalue = new.dvalue;
  end
end^
SET TERM ; ^
