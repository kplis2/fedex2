--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ETRAININGS_BI_REF FOR ETRAININGS                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ETRAININGS')
      returning_values new.REF;
end^
SET TERM ; ^
