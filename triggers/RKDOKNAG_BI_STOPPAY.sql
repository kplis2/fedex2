--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_BI_STOPPAY FOR RKDOKNAG                       
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable stop integer;
declare variable stanowisko varchar(2);
declare variable typ varchar(1);
begin
  if (new.pm = '-') then -- tylko wyplaty
  begin
    select stanowisko
      from rkrapkas
      where ref = new.raport
    into :stanowisko;

    select kasabank
      from rkstnkas
      where kod = :stanowisko
    into :typ;

    if (new.slodef = 6 and typ = 'K') then
    begin
      select stoppay
        from dostawcy
        where ref = new.slopoz
      into :stop;
      if (stop = 1) then
        exception stoppay;
    end
  end
end^
SET TERM ; ^
