--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_BU_REPLICAT FOR CPODMIOTY                      
  ACTIVE BEFORE UPDATE POSITION 3 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if((new.REF <> old.ref)
   or (new.skrot <> old.skrot)
   or(new.firma <> old.firma ) or (new.firma is null and old.firma is not null) or (new.firma is not null and old.firma is null)
   or(new.nazwa <> old.nazwa ) or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null)
   or(new.imie <> old.imie ) or (new.imie is null and old.imie is not null) or (new.imie is not null and old.imie is null)
   or(new.nazwisko <> old.nazwisko ) or (new.nazwisko is null and old.nazwisko is not null) or (new.nazwisko is not null and old.nazwisko is null)
   or(new.nip <> old.nip ) or (new.nip is null and old.nip is not null) or (new.nip is not null and old.nip is null)
   or(new.regon <> old.regon ) or (new.regon is null and old.regon is not null) or (new.regon is not null and old.regon is null)
   or(new.ulica <> old.ulica ) or (new.ulica is null and old.ulica is not null) or (new.ulica is not null and old.ulica is null)
   or(new.miasto <> old.miasto ) or (new.miasto is null and old.miasto is not null) or (new.miasto is not null and old.miasto is null)
   or(new.kodp <> old.kodp ) or (new.kodp is null and old.kodp is not null) or (new.kodp is not null and old.kodp is null)
   or(new.poczta <> old.poczta ) or (new.poczta is null and old.poczta is not null) or (new.poczta is not null and old.poczta is null)
   or(new.kraj <> old.kraj ) or (new.kraj is null and old.kraj is not null) or (new.kraj is not null and old.kraj is null)
   or(new.telefon <> old.telefon ) or (new.telefon is null and old.telefon is not null) or (new.telefon is not null and old.telefon is null)
   or(new.fax <> old.fax ) or (new.fax is null and old.fax is not null) or (new.fax is not null and old.fax is null)
   or(new.email <> old.email ) or (new.email is null and old.email is not null) or (new.email is not null and old.email is null)
   or(new.www <> old.www ) or (new.www is null and old.www is not null) or (new.www is not null and old.www is null)
   or(new.uwagi <> old.uwagi ) or (new.uwagi is null and old.uwagi is not null) or (new.uwagi is not null and old.uwagi is null)
   or(new.aktywny <> old.aktywny ) or (new.aktywny is null and old.aktywny is not null) or (new.aktywny is not null and old.aktywny is null)
   or(new.kodzewn <> old.kodzewn ) or (new.kodzewn is null and old.kodzewn is not null) or (new.kodzewn is not null and old.kodzewn is null)
   or(new.niewyplac <> old.niewyplac ) or (new.niewyplac is null and old.niewyplac is not null) or (new.niewyplac is not null and old.niewyplac is null)
   or(new.typ <> old.typ ) or (new.typ is null and old.typ is not null) or (new.typ is not null and old.typ is null)
   or(new.komorka <> old.komorka ) or (new.komorka is null and old.komorka is not null) or (new.komorka is not null and old.komorka is null)
   or(new.flagi <> old.flagi ) or (new.flagi is null and old.flagi is not null) or (new.flagi is not null and old.flagi is null)

  )then
   waschange = 1;

  execute procedure rp_trigger_bu('CPODMIOTY',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
