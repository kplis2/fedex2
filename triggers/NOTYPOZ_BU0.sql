--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTYPOZ_BU0 FOR NOTYPOZ                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable docstatus integer;
begin
  select status from notynag where ref = old.dokument into :docstatus;
  if (docstatus > 0) then exception NOTYNAG_ACCEPTED;
  if(new.bdebit is null) then new.bdebit = 0;
  if(new.bcredit is null) then new.bcredit = 0;
  if(new.ammount is null) then new.ammount = 0;
  if(new.ammountpaid is null) then new.ammountpaid = 0;
  if(new.interest is null) then new.interest = 0;
  if(new.interestzl is null) then new.interestzl = 0;
  if(new.daysof is null) then new.daysof = 0;
  if(new.bdebitzl is null) then new.bdebitzl = 0;
  if(new.bcreditzl is null) then new.bcreditzl = 0;

end^
SET TERM ; ^
