--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVDEFPOSTRANS_AU_REPLICAT FOR SRVDEFPOSTRANS                 
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable contrtype integer;
begin
  if((new.srvdefpos <> old.srvdefpos ) or (new.srvdefpos is not null and old.srvdefpos is null) or (new.srvdefpos is null and old.srvdefpos is not null)
    or (new.number <> old.number ) or (new.number is not null and old.number is null) or (new.number is null and old.number is not null)
    or (new.createdokmag <> old.createdokmag ) or (new.createdokmag is not null and old.createdokmag is null) or (new.createdokmag is null and old.createdokmag is not null)
    or (new.magazyn <> old.magazyn ) or (new.magazyn is not null and old.magazyn is null) or (new.magazyn is null and old.magazyn is not null)
    or (new.defdokum <> old.defdokum ) or (new.defdokum is not null and old.defdokum is null) or (new.defdokum is null and old.defdokum is not null)
    or (new.mag2 <> old.mag2 ) or (new.mag2 is not null and old.mag2 is null) or (new.mag2 is null and old.mag2 is not null)
    or (new.klient <> old.klient ) or (new.klient is not null and old.klient is null) or (new.klient is null and old.klient is not null)
    or (new.dostawca <> old.dostawca ) or (new.dostawca is not null and old.dostawca is null) or (new.dostawca is null and old.dostawca is not null)
    or (new.createfak <> old.createfak ) or (new.createfak is not null and old.createfak is null) or (new.createfak is null and old.createfak is not null)
    or (new.rejfak <> old.rejfak ) or (new.rejfak is not null and old.rejfak is null) or (new.rejfak is null and old.rejfak is not null)
    or (new.typfak <> old.typfak ) or (new.typfak is not null and old.typfak is null) or (new.typfak is null and old.typfak is not null)
    or (new.guaranty <> old.guaranty ) or (new.guaranty is not null and old.guaranty is null) or (new.guaranty is null and old.guaranty is not null)
    or (new.ord <> old.ord ) or (new.ord is not null and old.ord is null) or (new.ord is null and old.ord is not null)
  ) then begin
    select contrtype from srvdefpos where srvdefpos.ref = new.srvdefpos into :contrtype;
    update CONTRTYPES set STATE = -1 where ref = :contrtype;
  end
end^
SET TERM ; ^
