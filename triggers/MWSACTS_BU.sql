--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_BU FOR MWSACTS                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (old.status > 0
    and (coalesce(new.lot,0) <> coalesce(old.lot,0) or new.vers <> old.vers )
  ) then
    exception mwsacts_error 'Zmiana na zaakceptowanej operacji niemożliwa';
end^
SET TERM ; ^
