--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDCELLSBOTTOMUP_BI_REF FOR FRDCELLSBOTTOMUP               
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FRDCELLSBOTTOMUP')
      returning_values new.REF;
end^
SET TERM ; ^
