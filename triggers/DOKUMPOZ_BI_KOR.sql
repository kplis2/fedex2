--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_BI_KOR FOR DOKUMPOZ                       
  ACTIVE BEFORE INSERT POSITION 5 
as
begin
  if (new.kortopoz is not null) then
    select pmplan, pmelement, pmposition from dokumpoz where ref = new.kortopoz into new.pmplan, new.pmelement, new.pmposition;
end^
SET TERM ; ^
