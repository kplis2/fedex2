--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREEMATCHINGS_BI_REF FOR DECREEMATCHINGS                
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null) then
    execute procedure gen_ref('DECREEMATCHINGS') returning_values new.ref;
end^
SET TERM ; ^
