--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER UZYKLI_AI_SYNCHRO FOR UZYKLI                         
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable synchro smallint;
declare variable refout integer;
declare variable i integer;
declare variable cpodm integer;
declare variable slodefkli integer;
begin
  execute procedure getconfig('SYNCHROUZYKLI') returning_values :synchro;
  if (synchro = 1) then
  begin
    if (new.synchro is null and new.pkosoba is null) then begin
      cpodm = null;
      select ref from slodef where typ='KLIENCI' into :slodefkli;
      select cp.ref from cpodmioty cp where cp.slopoz=new.klient and cp.slodef=:slodefkli
      into :cpodm;
      if(:cpodm is not null) then begin
        execute procedure GEN_REF('PKOSOBY') returning_values :refout;
        insert into pkosoby (REF, CPODMIOT, IMIE, NAZWISKO, NAZWA, ULICA, NRDOMU, NRLOKALU,
           MIASTO, POCZTA, KODP, KOMORKA, TELEFON,
           FAX, EMAIL, OPIS, SYNCHRO, PESEL, AKTYWNY, WINDYKACJA)
        values (:refout, :cpodm, new.imie, new.nazwisko, new.nazwa, new.dulica, new.dnrdomu, new.dnrlokalu,
           new.dmiasto, new.dpoczta, new.dkodp, new.telkom,  new.telefon,
           new.fax, new.email, new.uwagi, 1, new.pesel, new.aktywny, new.windykacja);
        update uzykli set uzykli.pkosoba = :refout, uzykli.synchro = null
          where uzykli.ref = new.ref;
      end
    end
  end
end^
SET TERM ; ^
