--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDELPOS_BU0 FOR EDELPOS                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable kmcost numeric(14,2);
  declare variable dataod date;     /* Data poczatkowa na naglowku delegacji */
  declare variable datado date;     /* Data koncowa na naglowku delgacji */
begin
  if(new.vehicle<>old.vehicle or new.distance <> old.distance) then begin
    if(new.distance = 0) then new.costs = 0;
    else begin
      select coalesce(e.costs,0) from evehicletypes e where e.ref = new.vehicle
      into :kmcost;
      new.costs = :kmcost * new.distance;
    end
  end

  if((new.fromdate<>old.fromdate) or (new.todate<>old.todate)) then begin
    select fromdate, todate
      from edelegations
      where ref = new.delegation
    into :dataod, :datado;
    if((cast(new.fromdate as date)<:dataod) or (cast(new.todate as date)>:datado)) then
      exception EDELPOS_DATY;
  end
end^
SET TERM ; ^
