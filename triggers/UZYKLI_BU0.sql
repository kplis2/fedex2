--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER UZYKLI_BU0 FOR UZYKLI                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.nazwisko is null or (new.nazwisko = ''))then exception UZYKLI_NONAME;
  if(new.id is null) then new.id = '';
  if(new.imie is null) then new.imie = '';
  new.nazwa = new.imie||' '||new.nazwisko;
  if (old.id <> new.id and new.id<>'') then begin
    if(exists (select ID from uzykli where uzykli.id = new.id))
      then exception UZYKLI_ID_EXISTS 'Uzytkownik o podanym ID ('||new.id||') juz istnieje! Podaj Inny ID.';
  end
  if (new.pesel <> old.pesel and new.pesel='') then new.pesel = null;
  if ((new.haslo is not null or new.haslo = '') and (old.haslo is null or new.haslo <> old.haslo)
    and coalesce(char_length(new.haslo),0)<32) -- [DG] XXX ZG119346
  then begin
    new.haslo1 = md5sum('SENTE_' || new.haslo || '_NewPass');
    new.haslo = md5sum(new.haslo);
  end
end^
SET TERM ; ^
