--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHOPERS_BI FOR PRSHOPERS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable status integer;
BEGIN
  if(new.oper='') then new.oper = NULL;
  if(new.workplace='') then new.workplace = NULL;
  if(new.scheduled is null) then new.scheduled = 0;
  if(new.calculated is null) then new.calculated = 0;
  if(new.reported is null) then new.reported = 0;
  if(new.amountresultcalctype is null) then new.amountresultcalctype = 0;
  if ((new.calculated = 1 or new.scheduled = 1) and new.oper is null) then exception prsheets_data 'Nie uzupeniony typ operacji';
  if(new.complex = 1 and (new.opertype is null or new.opertype < 0)) then exception PRSHEETS_DATA 'Rodzaj operacji musi być wskazany';
  if(new.scheduled = 1 and new.opermaster is not null) then begin
    if(new.opermaster is not null) then execute procedure prsheets_no_operschedloop_up(new.opermaster) returning_values status;
    execute procedure prsheets_no_operschedloop_down(new.ref) returning_values status;
  end
  new.eoperfactor = replace(new.eoperfactor,',','.');
  new.eopertime = replace(new.eopertime,',','.');
  new.eplacetime = replace(new.eplacetime,',','.');
  new.econdition = replace(new.econdition,',','.');
  if(new.complex = 1 and (new.opertype is null or new.opertype < 0)) then exception PRSHEETS_DATA 'Rodzaj operacji musi być wskazany';
  if(new.reportedfactor is null or new.reportedfactor = 0) then new.reportedfactor = 1;
  if (new.amountinfrom is null) then new.amountinfrom = 0;
  if (new.complex = 1 and new.serialnumbergen <> '') then new.serialnumbergen = null;
END^
SET TERM ; ^
