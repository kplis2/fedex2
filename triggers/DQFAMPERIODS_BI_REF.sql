--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DQFAMPERIODS_BI_REF FOR DQFAMPERIODS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DQFAMPERIODS')
      returning_values new.REF;
end^
SET TERM ; ^
