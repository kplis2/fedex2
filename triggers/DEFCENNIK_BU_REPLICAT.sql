--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCENNIK_BU_REPLICAT FOR DEFCENNIK                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if((new.ref <> old.ref)
   or(new.nazwa <> old.nazwa ) or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null)
   or(new.datado <> old.datado ) or (new.datado is null and old.datado is not null) or (new.datado is not null and old.datado is null)
   or(new.dataod <> old.dataod ) or (new.dataod is null and old.dataod is not null) or (new.dataod is not null and old.dataod is null)
   or(new.marza  <> old.marza ) or (new.marza is null and old.marza is not null) or (new.marza is not null and old.marza is null)
   or(new.tryb <> old.tryb ) or (new.tryb is null and old.tryb is not null) or (new.tryb is not null and old.tryb is null)
   or(new.waluta <> old.waluta ) or (new.waluta is null and old.waluta is not null) or (new.waluta is not null and old.waluta is null)
   or(new.zrodlo <> old.zrodlo ) or (new.zrodlo is null and old.zrodlo is not null) or (new.zrodlo is not null and old.zrodlo is null)
   or(new.oddzial <> old.oddzial ) or (new.oddzial is null and old.oddzial is not null) or (new.oddzial is not null and old.oddzial is null)
   or(new.replicat <> old.replicat ) or (new.replicat is null and old.replicat is not null) or (new.replicat is not null and old.replicat is null)

  )then
   waschange = 1;

  execute procedure rp_trigger_bu('DEFCENNIK',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
