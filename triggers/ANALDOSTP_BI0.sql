--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ANALDOSTP_BI0 FOR ANALDOSTP                      
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  else if(new.ktm is not null and new.wersja is not null) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if(new.sprzedaz is null) then new.sprzedaz = 0;
  if(new.srednia is null) then new.srednia = 0;
  if(new.sredniaw is null) then new.sredniaw = 0;
  if(new.potrzeba is null) then new.potrzeba = 0;
  if(new.reczny is null) then new.reczny = 0;

end^
SET TERM ; ^
