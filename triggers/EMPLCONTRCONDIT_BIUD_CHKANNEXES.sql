--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRCONDIT_BIUD_CHKANNEXES FOR EMPLCONTRCONDIT                
  ACTIVE BEFORE INSERT OR UPDATE OR DELETE POSITION 0 
AS
declare variable emplcontract integer ;
declare variable afromdate date;
begin
  if (deleting or inserting or (updating
      and (new.annexe is distinct from old.annexe
        or (new.emplcontract is distinct from old.emplcontract)
        or (new.contrcondit is distinct from old.contrcondit)
        or (new.val_text is distinct from old.val_text)
        or (new.workstart is distinct from old.workstart)
        or (new.workend is distinct from old.workend)
        or (new.val_date is distinct from old.val_date)
        or (new.val_numeric is distinct from old.val_numeric)
        or (new.dictdef is distinct from old.dictdef)
        or (new.dictpos is distinct from old.dictpos)
        or (new.workstartstr is distinct from old.workstartstr)
        or (new.workendstr is distinct from old.workendstr)))
  ) then begin

    if ((new.annexe is null and new.emplcontract is not null)
        or (deleting and old.annexe is null and old.emplcontract is not null)
    ) then begin
      select first 1 emplcontract
        from econtrannexes ec
        where emplcontract = iif(deleting, old.emplcontract, new.emplcontract)
        into :emplcontract;

      if (emplcontract is not null) then
        exception universal 'Umowa posiada aneks - modyfikacja warunku umowy nie jest możliwa!'  ;
    end else

    if (new.annexe is not null
       or (deleting and old.annexe is not null)
    ) then begin
      select fromdate
        from econtrannexes
        where ref = iif(deleting, old.annexe, new.annexe)
        into :afromdate;

      select first 1 emplcontract
        from econtrannexes
        where emplcontract = iif(deleting, old.emplcontract, new.emplcontract) and fromdate > :afromdate
        into :emplcontract;

      if (emplcontract is not null) then
        exception universal 'Istnieje późniejszy aneks - modyfikacja warunku bieżącego aneksu nie jest możliwa!'  ;
    end
  end
end^
SET TERM ; ^
