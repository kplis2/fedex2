--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EALGORITHMS_AIUD_GEN_XXPROCED FOR EALGORITHMS                    
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 
AS
begin
/*-- Before PR53533:
  execute statement 'create or alter procedure XX_E_ALGORITHM' ||
    '(key1 integer, key2 integer) returns (ret numeric(16,2)) as
    ' || new.decl || ' begin ' || new.body || ' suspend;  end;' */

  if (not inserting and (deleting
     or new.ecolumn is distinct from old.ecolumn
     or new.ealgtype is distinct from old.ealgtype)
  ) then
    execute procedure ealgorithm_modify (old.ealgtype, old.ecolumn, 1);

  if (inserting or updating
    and (new.ecolumn is distinct from old.ecolumn
       or new.ealgtype is distinct from old.ealgtype
       or new.body is distinct from old.body
       or new.decl is distinct from old.decl)
  ) then
    execute procedure ealgorithm_modify (new.ealgtype, new.ecolumn);
end^
SET TERM ; ^
