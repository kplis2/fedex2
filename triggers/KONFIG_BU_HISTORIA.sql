--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONFIG_BU_HISTORIA FOR KONFIG                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.multi = 0 and new.historia = 1) then
    exception konfig_historia;
end^
SET TERM ; ^
