--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKPLIK_AD_WFTHROWEVENT FOR DOKPLIK                        
  ACTIVE AFTER DELETE POSITION 10 
AS
begin
  execute procedure wf_throwevent('DOKPLIK_DEL','SENTE.DOKPLIK',old.ref,
   coalesce(old.symbol, current_timestamp(0)),1);
end^
SET TERM ; ^
