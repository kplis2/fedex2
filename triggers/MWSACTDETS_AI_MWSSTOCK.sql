--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTDETS_AI_MWSSTOCK FOR MWSACTDETS                     
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  -- status 0 -> niezzakceptowano +++++ brak obslugi bo niepotrzebna
  -- status 1 -> do realizacji
  -- status 2 -> w realizacji
  -- status 3 -> zawieszono
  -- status 4 -> zablokowano  +++++ brak obslugi bo niepotrzebna
  -- status 5 -> potwierdzono (zrealizowano)
  -- status 6 -> anulowano +++++ brak obslugi bo niepotrzebna
  if (new.status > 0) then
  begin
    -- ZMIANY NA LOKACJACH WYNIKAJACE ZE STATUSU OPERACJI
    if (new.status = 1 or new.status = 2) then
    begin
      --obsluga lokacji końcowej
      if (new.mwspallocl is not null) then
        execute procedure PLUS_ORDERED_MWSSTOCK(new.wh,new.whsec,new.wharea,new.good,
          new.vers,new.mwsconstlocl,new.mwspallocl,new.quantity,new.lot,new.stancen,
          new.x_partia, new.x_serial_no, new.x_slownik); -- XXX KBI
      --obsluga lokacji pocyatkowej
      if (new.mwspallocp is not null) then
        execute procedure PLUS_BLOCKED_MWSSTOCK(new.wh,new.whsec,new.wharea,new.good,
          new.vers,new.mwsconstlocp,new.mwspallocp,new.quantity,new.lot,new.stancen,
          new.x_partia, new.x_serial_no, new.x_slownik); -- XXX KBI
    end
    else if (new.status = 3) then
    begin
      --obsluga lokacji końcowej
      if (new.mwspallocl is not null) then
        execute procedure PLUS_ORDSUSPENDED_MWSSTOCK(new.wh,new.whsec,new.wharea,new.good,
          new.vers,new.mwsconstlocl,new.mwspallocl,new.quantity,new.lot,new.stancen,
          new.x_partia, new.x_serial_no, new.x_slownik); -- XXX KBI
      --obsluga lokacji pocyatkowej
      if (new.mwspallocp is not null) then
        execute procedure PLUS_SUSPENDED_MWSSTOCK(new.wh,new.whsec,new.wharea,new.good,
          new.vers,new.mwsconstlocp,new.mwspallocp,new.quantity,new.lot,new.stancen,
          new.x_partia, new.x_serial_no, new.x_slownik); -- XXX KBI
    end
    else if (new.status = 5) then
    begin
      -- obsluga lokacji końcowej
      if (new.mwspallocl is not null) then
        execute procedure PLUS_QUANTITY_MWSSTOCK(new.wh,new.whsec,new.wharea,new.good,
          new.vers,new.mwsconstlocl,new.mwspallocl,new.quantity,new.lot,new.stancen,
          new.x_partia, new.x_serial_no, new.x_slownik); -- XXX KBI
      --obsluga lokacji pocyatkowej
      if (new.mwspallocp is not null) then
        execute procedure MINUS_QUANTITY_MWSSTOCK(new.wh,new.whsec,new.wharea,new.good,
          new.vers,new.mwsconstlocp,new.mwspallocp,new.quantity,new.lot,new.stancen,
          new.x_partia, new.x_serial_no, new.x_slownik); -- XXX KBI
    end
  end
end^
SET TERM ; ^
