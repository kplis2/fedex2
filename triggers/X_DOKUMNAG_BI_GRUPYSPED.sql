--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_DOKUMNAG_BI_GRUPYSPED FOR DOKUMNAG                       
  ACTIVE BEFORE INSERT POSITION 101 
as
    declare variable GRUPASPED integer_id;
begin
-- triger sluzacy do automatycznego polaczenia wz w grupe spedycyjna
 --  exception universal 'mkd grupa sped'||coalesce(new.grupasped,0) ;
    if (new.typ = 'WZ') then
    begin
        select first 1 dg.grupasped
            from dokumnag dg
                left join listywysd ld on (ld.refdok = dg.grupasped)
            where   dg.odbiorcaid = new.odbiorcaid
                and coalesce(dg.sposdost,0) = coalesce(new.sposdost,0)
                and coalesce(dg.sposzap,0) = coalesce(new.sposzap,0)
                and dg.datawydania = new.datawydania
                and ld.ref is null
            into:grupasped;
        if (grupasped is not null) then
            new.grupasped = :grupasped;
    end
end^
SET TERM ; ^
