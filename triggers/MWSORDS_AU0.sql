--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDS_AU0 FOR MWSORDS                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable grupasped integer;
begin
  if (new.status <> old.status and new.docid is not null) then
  begin
    select coalesce(g.grupasped, g.ref) from dokumnag g where g.ref = new.docid
      into grupasped;
    execute procedure dokumnag_mwsrealstatus(grupasped,1);
  end
  -- wycofanie dokumentów PZ+/-
  if (new.mwsordtypedest = 2 and old.status = 5 and new.status < 5) then
  begin
    update dokumnag d set d.akcept = 0 where d.frommwsord = new.ref;
    delete from dokumnag d where d.frommwsord = new.ref;
--XXX
    update MWSORDS  set docid = null where ref = new.ref;
--XXX
  end
end^
SET TERM ; ^
