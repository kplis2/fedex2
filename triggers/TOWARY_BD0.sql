--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWARY_BD0 FOR TOWARY                         
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable numer integer;
declare variable ilosc integer;
begin
  if(old.token = 7) then
    update WERSJE set TOKEN = 7 where KTM = old.ktm;
  if (old.kodwkasie<>'') then begin
    ilosc=0;
    numer = cast(old.kodwkasie as integer);
    select count(*) from numberfree where nazwa='Kod w kasie' and typ <>0 and number=:numer into :ilosc;
    if(ilosc=0) then
      execute procedure free_number('Kod w kasie', null, null, null, :numer, null) returning_values :numer;
  end
end^
SET TERM ; ^
