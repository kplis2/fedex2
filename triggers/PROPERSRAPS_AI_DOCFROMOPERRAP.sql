--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROPERSRAPS_AI_DOCFROMOPERRAP FOR PROPERSRAPS                    
  ACTIVE AFTER INSERT POSITION 2 
as
declare variable autodoc smallint;
declare variable depto smallint;
begin
  if (coalesce(new.amount,0) > 0) then
  begin
    if (exists (select first 1 1 from prschedopers p left join prshmat m on (p.shoper = m.opermaster)
        where p.ref = new.prschedoper and m.autodocout > 0)) then
    begin
      --generowanie dokumentow RW do raportow produkcyjnych
      execute procedure prschedguide_real(new.ref, null, 0, 1);
    end
    if (exists (select  first 1 1
          from prschedopers p
           left join prschedguides g on (p.guide = g.ref)
           left join prsheets s on (s.ref = g.prsheet)
         where s.autodocin = 1 and p.ref = new.prschedoper)
    ) then
      execute procedure pr_proper_check_last_reported(new.prschedoper)
        returning_values depto;
      if (depto = 0) then
        execute procedure prschedguide_real(new.ref, null, 1, 0);
  end
end^
SET TERM ; ^
