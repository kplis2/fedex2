--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYS_AU_ROZLICZONO FOR LISTYWYS                       
  ACTIVE AFTER UPDATE POSITION 1 
as
begin
  if(new.rozliczono<>old.rozliczono) then
    update LISTYWYSD set ROZLICZONO = new.rozliczono where LISTAWYS=new.ref and ROZLICZONO<>new.rozliczono;
end^
SET TERM ; ^
