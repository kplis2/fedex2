--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYCEN_AI0 FOR STANYCEN                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable isstan smallint;
declare variable sumcen decimal(14,2);
declare variable sumil decimal(14,4);
begin
  select count(*) from STANYIL where MAGAZYN=new.magazyn and ktm = new.ktm and wersja = new.wersja into :isstan;
  if(:isstan is null) then isstan = 0;
  if(:isstan = 0) then
    insert into STANYIL(MAGAZYN,KTM,WERSJA,ILOSC,CENA, WARTOSC) values(new.magazyn, new.ktm, new.wersja, new.ilosc, new.cena, new.wartosc);
  else begin
     /* obliczenie średniej ceny */
     select sum(ilosc), sum(wartosc) from STANYCEN where MAGAZYN=new.MAGAZYN AND KTM = new.ktm AND WERSJA=new.wersja into :sumil, :sumcen;
    if(:sumil is null or (:sumil = 0)) then begin
       sumil = 1;
       sumcen = new.cena;
    end

     update STANYIL set cena=(:sumcen / :sumil), ilosc = ilosc + new.ilosc, wartosc = wartosc + new.wartosc where
     magazyn = new.magazyn and ktm = new.ktm and wersja = new.wersja;
  end
end^
SET TERM ; ^
