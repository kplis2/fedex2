--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACLIMITS_BIU_CUSTOMIZED FOR EVACLIMITS                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 5 
AS
begin
  --DS: jezeli updatujemy karte i jest zaznaczona modyfikacja to wez stara liczbe minut (zeby nie nadpisac modyfikacji)
  if (updating) then
  begin
    if (new.actlimit_cust = 1 and old.actlimit_cust = 1 and new.actlimith = old.actlimith) then new.actlimit = old.actlimit;
    if (new.addlimit_cust = 1 and old.addlimit_cust = 1 and new.addlimith = old.addlimith) then new.addlimit = old.addlimit;
    if (new.disablelimit_cust = 1 and old.disablelimit_cust = 1 and new.disablelimith = old.disablelimith) then new.disablelimit = old.disablelimit;
    if (new.traininglimit_cust = 1 and old.traininglimit_cust = 1 and new.traininglimith = old.traininglimith) then new.traininglimit = old.traininglimit;
    --w przypadku daty problem jest inny, nie ma podzialu na pole minuty w bazie i godziny dla uzytkownika
    --dlatego stosujemy nastepujacy myk:
    --jezeli uzytkownik zaznacza modyfikacje bo bazy zapisywana jest jedynka, jezeli system - dwojka
    --jezeli zmiana jest robiona przez system a wczesniej byla modyf. wtedy nie zmieniaj wartosci
    if (new.addvdate_cust = 2 and old.addvdate_cust = 1) then new.addvdate = old.addvdate;
    --i nie przechowuj w bazie dwojki
    if (new.addvdate_cust = 2) then new.addvdate_cust = old.addvdate_cust;
  end
  --DS: oznaczenie czy na danej karcie jest jakas zmiana uzytkownika
  if (new.addlimit_cust = 1 or new.actlimit_cust = 1 or new.traininglimit_cust = 1
        or new.addvdate_cust = 1 or new.disablelimit_cust = 1) then
    new.customized = 1;
  else new.customized = 0;
end^
SET TERM ; ^
