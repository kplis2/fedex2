--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_APPINI_AD FOR S_APPINI                       
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  if(position(':Customized' in  old.ident) > 0) then
  begin
    execute procedure SET_CUSTOMIZED(old.section, old.ident, 0);
  end
end^
SET TERM ; ^
