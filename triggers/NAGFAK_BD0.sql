--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_BD0 FOR NAGFAK                         
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable cnt integer;
declare variable rkanulowano integer;
declare variable local smallint;
declare variable rkusun varchar(20);
declare variable rknumer integer;
declare variable rkstanowisko varchar(3);
declare variable rktyp varchar(3);
begin
  execute procedure CHECK_LOCAL('NAGFAK',old.ref) returning_values :local;
  if(:local = 1) then begin
    cnt = null;
--    select count(*) from NAGFAK where REFK = old.ref into :cnt;
--SN
    if(/*:cnt > 0 or*/ old.korekta is not null) then
      exception NAGFAK_KOREKTY_SA;
    if(old.REFFAK > 0) then
      exception NAGFA_FAKTURY_SA;
    if(old.zafisk = 1) then
        exception NAGFAK_DOK_ZAFISKALIZOWANY;
    if(old.blokada > 0 and old.token <> 7) then
      exception NAGFAK_USUNBLOK;
    if(old.akceptacja = 9 and exists(select first 1 1 from pozfak where pozfak.dokument = old.ref)) then
      exception dopoprawy_pozycje;
    if(old.akceptacja = 10) then
      exception DOPOPRAWY;
    if(old.skad = 1 and old.akceptacja = 0) then
      exception DOZAMOWIENIA;
  end

  if(old.akceptacja = 1 or (old.akceptacja >= 9) and (:local = 1) and (old.nieobrot = 1)) then begin
    /* sprawdzenei, czy są anulowane dok. kasowe związane z danym */
    select ANULOWANY, NUMER, STANOWISKO,TYP from RKDOKNAG where REF=old.rkdoknag into :rkanulowano, :rknumer, :rkstanowisko, :rktyp;
    if(:rkanulowano = 0) then begin
        execute procedure GETCONFIG('KASSPRZUSU') returning_values :rkusun;
        if(:rkusun = '1' ) then begin
          if(:rknumer > 0) then begin
            cnt = null;
            select count(*) from RKDOKNAG where STANOWISKO = :rkstanowisko and TYP=:rktyp and NUMER > :rknumer into :cnt;
            if(:cnt > 0) then begin
              rkusun = '';
            end
          end
        end else if(:rkusun = '2') then
          rkusun = '1';
        else begin
          rkusun = '';
        end
        if(:rkusun <>'')then begin
          execute procedure GETCONFIG('KASUSUNLAST') returning_values :rkusun;
          if(:rkusun = '1' and :rknumer > 0) then begin
            /*sprawdzenie, czy usuwac, czy anulowac jednak*/
            cnt = null;
            select count(*) from RKDOKNAG where STANOWISKO = :rkstanowisko and TYP=:rktyp and NUMER > :rknumer into :cnt;
            if(:cnt > 0) then rkusun = '';
          end
          if(:rkusun ='1') then begin
            update RKDOKNAG set NUMER = 0 where REF=old.rkdoknag;
            delete from  RKDOKNAG where REF=old.rkdoknag;
          end else
            update RKDOKNAG set ANULOWANY=1, ANULOPIS='Wycofanie akceptacji dok. sprzedazy' where REF=old.rkdoknag;
        end else
          exception NAGFA_ACK_SARKDOKNAG;
    end
    cnt = null;
    select count(*) from DOKUMNAG where FAKTURA = old.ref and ZRODLO = 2 into :cnt;
    if(:cnt > 0) then exception NAGFA_AKC_SADOKMAGRECZ;
    /* usuniecie dokumentów magazynowych wystawionych automatycznie do dokumentu sprzedazy */
    if(old.akceptacja >= 9) then
      update DOKUMNAG set BLOKADA = 0 where FAKTURA = old.ref and zrodlo = 3;
    update DOKUMNAG set AKCEPT = 0 where FAKTURA = old.ref and zrodlo = 3;
    delete from DOKUMNAG where FAKTURA = old.ref and zrodlo = 3;
    delete from ROZRACHP where FAKTURA = old.ref and SKAD = 1;
    select count(*) from ROZRACHP  where ROZRACHP.FAKTURA = old.ref and ROZRACHP.skad < 6 into :cnt;

    if(:cnt > 0) then
      exception NAGFAF_USUN_BYLY_ZAPLATY;
    delete from ROZRACH where FAKTURA=old.ref and SKAD = 1;
  end

end^
SET TERM ; ^
