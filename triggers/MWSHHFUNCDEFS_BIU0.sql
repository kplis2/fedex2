--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSHHFUNCDEFS_BIU0 FOR MWSHHFUNCDEFS                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.alwaysenabled is null) then new.alwaysenabled = 0;
  if (new.disabled is null) then new.disabled = 1;
end^
SET TERM ; ^
