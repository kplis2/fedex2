--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACHP_AU0 FOR ROZRACHP                       
  ACTIVE AFTER UPDATE POSITION 0 
as
  declare variable zaprej varchar(255);
  declare variable sprzedzestid integer;
begin
  if (new.winien <> old.winien or new.ma <> old.ma or new.kurs <> old.kurs or new.winienzl <> old.winienzl or new.mazl <> old.mazl) then
    execute procedure ROZRACH_OBL_NAG(new.kontofk, new.slodef, new.slopoz, new.symbfak, new.company);
  if (new.kontofk <> old.kontofk or new.symbfak <> old.symbfak
      or new.slodef <> old.slodef or new.slopoz <> old.slopoz
      or new.company <> old.company) then
  begin
    execute procedure ROZRACH_OBL_NAG(old.kontofk, old.slodef, old.slopoz, old.symbfak, old.company);
    execute procedure ROZRACH_OBL_NAG(new.kontofk, new.slodef, new.slopoz, new.symbfak, new.company);
  end
  if (new.ma <> old.ma or (new.ma is null and old.ma is not null) or (new.ma is not null and old.ma is null) or
     new.winien <> old.winien or (new.winien is null and old.winien is not null) or (new.winien is not null and old.winien is null)
  ) then begin
    execute procedure GETCONFIG('ZAPREJ') returning_values :zaprej;
    if(:zaprej = '1' and new.faktura is not null) then
      execute statement 'execute procedure NAGFAK_OBL_ZAP(' || new.faktura || ')';
    select SPRZEDZESTID from ROZRACH
      where company = new.company and kontofk = new.kontofk and symbfak = new.symbfak
      into :sprzedzestid;
    if (sprzedzestid > 0) then
      execute procedure SPRZEDZEST_OBL_ROZRACH(sprzedzestid);
  end
end^
SET TERM ; ^
