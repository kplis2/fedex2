--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWSCONSTLOCS_BU_TAKEFROMMIX FOR MWSCONSTLOCS                   
  ACTIVE BEFORE UPDATE POSITION 99 
as
begin
  if (new.mwsconstloctype is distinct from old.mwsconstloctype and new.mwsconstloctype is not null
        and new.takefrommix is null) then
    new.takefrommix = 1;
end^
SET TERM ; ^
