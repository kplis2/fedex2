--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDTYPES_BIU0 FOR MWSORDTYPES                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.docblockreal is null) then new.docblockreal = 0;
  if (new.delaftackchange is null) then new.delaftackchange = 0;
  if (new.recdoc is null) then new.recdoc = 0;
  if (new.closemode is null) then new.closemode = 0;
  if (new.docrequired is null) then new.docrequired = 0;
  if (new.stocktaking is null) then new.stocktaking = 0;
  if (new.opersettlmode is null) then new.opersettlmode = 0;
  if (new.stocktakingsettlmode is null) then new.stocktakingsettlmode = 0;
  if (new.operautoreal is null) then new.operautoreal = 0;
  if (new.cor is null) then new.cor = 0;
  if (new.clonoper is null) then new.clonoper = 0;
  if (new.autoloccreate is null) then new.autoloccreate = 0;
  if (new.actudocpos is null) then new.actudocpos = 0;
  if (new.blockgoodsnotacc is null) then new.blockgoodsnotacc = 0;
  if (new.packgoods is null) then new.packgoods = 0;
  if (new.packrequired is null) then new.packrequired = 0;
  if (new.goodsrequired is null) then new.goodsrequired = 0;
  if (new.locsrequired is null) then new.locsrequired = 0;
  if (new.gridownacts is null) then new.gridownacts = 0;
  if (new.mwsordwaitingcalc is null) then new.mwsordwaitingcalc = 0;
  if (new.verifymwsconstloc is null) then new.verifymwsconstloc = 0;
  if (new.lastposquestion is null) then new.lastposquestion = 1;
end^
SET TERM ; ^
