--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVREQUESTS_AU0 FOR SRVREQUESTS                    
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable crm smallint;
declare variable srvrequest smallint;
declare variable contract integer;
declare variable ref integer;
declare variable srvposref smallint;
declare variable nazwa varchar(255);
begin
 ref=null;
 select crm,srvrequest from contrtypes where ref=new.contrtype into :crm,:srvrequest;
 if(new.wysylka is null and old.wysylka is not null) then
    delete from LISTYWYSD where REFSRV = old.ref;
 if(:crm=1 and :srvrequest=1) then
   if (new.contract is null) then begin
      nazwa = new.symbol;
      if(nazwa is null or nazwa = '') then nazwa = 'zgłoszenie bez symbolu';
      contract = GEN_ID(GEN_CKONTRAKTY,1);
      select cpodmioty.REF from CPodmioty
      where (new.repdictdef=CPODMIOTY.slodef and new.repdictpos=CPODMIOTY.slopoz) into :ref;
      insert into CKONTRAKTY(REF,FAZA,CONTRTYPE,NAZWA,DATAZAL,OPERZAL,CPODMIOT)
      values(:contract,new.status,new.contrtype,:nazwa,new.reqdate,new.regoper,:ref);
      update SRVREQUESTS set contract=:contract where REF=new.ref;
      end
   else
     if(new.status<>old.status or (new.status is null and old.status is not null) or (new.status is not null and old.status is null) or
        new.contrtype<>old.contrtype or (new.contrtype is null and old.contrtype is not null) or (new.contrtype is not null and old.contrtype is null) or
        new.symbol<>old.symbol or (new.symbol is null and old.symbol is not null) or (new.symbol is not null and old.symbol is null) or
        new.reqdate<>old.reqdate or (new.reqdate is null and old.reqdate is not null) or (new.reqdate is not null and old.reqdate is null) or
        new.regoper<>old.regoper or (new.regoper is null and old.regoper is not null) or (new.regoper is not null and old.regoper is null)
     )
     then begin
      nazwa = new.symbol;
      if(nazwa is null or nazwa = '') then nazwa = 'zgłoszenie bez symbolu';
       update CKONTRAKTY set FAZA=new.status,
         CONTRTYPE=new.contrtype,
         NAZWA=:nazwa,
         DATAZAL=new.reqdate,
         OPERZAL=new.regoper
       where REF=new.contract;
     end
/*  if(new.repnetprice is not null and new.repnetprice<>0 and new.repcurr is null ) then exception test_break 'Nie zdefiniowano waluty platnosci UE zglaszającego.';
  if(new.repnetprice2 is not null  and new.repcurr2 is null ) then exception test_break 'Nie zdefiniowano waluty platnosci poza UE zglaszającego.';
  if(new.repnetprice<>old.repnetprice or (new.repcurr<>old.repcurr) ) then begin
    srvposref = 0;
    select ref from srvpositions where srvrequest = new.ref and ktm='USLUGA0001' into :srvposref;
    if(srvposref>0) then
      update SRVPOSITIONS set netsaleprice = new.repnetprice, currency = new.repcurr where ref = :srvposref;
    else
      insert into SRVPOSITIONS(SRVREQUEST,SRVDEFPOS,NUMBER,AMOUNT,NETSALEPRICE,KTM, CURRENCY)
       values  (new.ref,1,1,1,new.repnetprice,'USLUGA0001', new.repcurr);
    execute procedure srv_buduj_trase(new.ref);
  end
  if(new.repnetprice2<>old.repnetprice2 or (new.repcurr2<>old.repcurr2)
      or (new.repnetprice2 is null and old.repnetprice2 is not null)
      or (new.repnetprice2 is not null and old.repnetprice2 is null)) then begin
    srvposref = 0;
    select ref from srvpositions where srvrequest = new.ref  and ktm='USLUGA0002' into :srvposref;
    if(new.repnetprice2 is not null and new.repnetprice2<>0) then
      if(:srvposref>0) then
        update SRVPOSITIONS set netsaleprice = new.repnetprice2, currency = new.repcurr2 where ref = :srvposref;
      else
        insert into SRVPOSITIONS(SRVREQUEST,SRVDEFPOS,NUMBER,AMOUNT,NETSALEPRICE,KTM, CURRENCY)
         values  (new.ref,1,2,1,new.repnetprice2,'USLUGA0002', new.repcurr2);
    else if (:srvposref>0) then delete from srvpositions where  ref = :srvposref;
    execute procedure srv_buduj_trase(new.ref);
  end
*/
end^
SET TERM ; ^
