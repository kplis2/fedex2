--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKRAPKAS_BU0 FOR RKRAPKAS                       
  ACTIVE BEFORE UPDATE POSITION 1 
as
  declare variable blokfk smallint;
  declare variable rk_ref integer;
  declare variable status smallint;
  declare variable kasabank char;
  declare variable local integer;
begin
   execute procedure CHECK_LOCAL('RKRAPKAS',old.ref) returning_values :local;
   if (new.przychod is null) then
     new.przychod = 0;
   if (new.rozchod is null) then
     new.rozchod = 0;
   if (new.przychodzl is null) then
     new.przychodzl = 0;
   if (new.rozchodzl is null) then
     new.rozchodzl = 0;
   if (new.skwota is null) then
     new.skwota = 0;
   if (new.skwotazl is null) then
     new.skwotazl = 0;
   if (new.symbol is null or new.symbol='') then
     new.symbol = cast(new.numer as varchar(20));


  new.skwota = new.kwotabo + new.przychod - new.rozchod;
  /* BS34032 usuniety fragment i przeniesiony do RKRAP_OBL_WAR */

  if (local = 1) then begin -- zezwala na automatyczne zmiany kwot bilansu otwarcia i zamkniecia dla zamknietych raportow
    if ((new.przychod <> old.przychod or new.przychodzl <> old.przychodzl or
      new.rozchod <> old.rozchod or new.rozchodzl <> new.rozchodzl) and new.status > 0) then
    begin
      exception RKRAPZAS_ZAMKKWOATA;
    end

    select S.kasabank
      from rkrapkas R
        join rkstnkas S on (S.kod = R.stanowisko)
      where ref=new.ref
      into :kasabank;
    if ((new.skwota <> old.skwota or new.skwotazl <> old.skwotazl) and new.status > 0 and kasabank = 'K') then
      exception RKRAPZAS_ZAMKKWOATA 'Raport kasowy nr '||new.symbol||'('||new.ref||') z okresu '||new.okres||' jest zamknięty. Zmiana kwot niemożliwa.';

  end

  if (new.status = 0 and old.status > 0 and local = 1) then begin
    for
      select ref
        from rkrapkas
        where stanowisko = new.stanowisko
          and ((rkrapkas.dataod > new.dataod and ref <> old.ref)
          or (rkrapkas.dataod = new.dataod and numer > old.numer)) -- jezeli z tego samego dnia no to szukam z wyzszych numerem
--      and rkrapkas.dataod >= new.dataod and ref <> old.ref
        into :rk_ref
    do begin
      if (rk_ref is not null) then
      begin
        select R.status, S.kasabank
          from rkrapkas R
            join rkstnkas S on (R.stanowisko = S.kod)
          where ref=:rk_ref
          into :status, :kasabank;
        if (status > 0 and kasabank = 'K') then -- kontrola chronologii tylko dla raportów kasowych
        begin
          exception ERROR_OPENING_RKRAPKAS;
        end
      end
    end
    new.wasdeakcept = 1;
    if (old.status > 1) then
    begin
      execute procedure GET_CONFIG('SIDFK_KASBLOK', 2)
        returning_values :blokfk;
      if (blokfk <> '2') then
        exception NAGFAK_BLOKADAKS;
    end
  end
end^
SET TERM ; ^
