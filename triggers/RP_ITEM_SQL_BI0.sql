--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RP_ITEM_SQL_BI0 FOR RP_ITEM_SQL                    
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.ref is null or (new.ref = 0)) then
    execute procedure gen_ref('rp_item_sql') returning_values new.ref;
end^
SET TERM ; ^
