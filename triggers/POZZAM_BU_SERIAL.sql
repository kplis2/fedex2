--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_BU_SERIAL FOR POZZAM                         
  ACTIVE BEFORE UPDATE POSITION 2 
as
declare variable nserial integer;
begin
  if(new.ktm is not null and (new.wersjaref <> old.wersjaref)) then begin
    new.serialized = 0;
    select SERIAL from TOWARY where KTM = new.ktm into new.serialized;
    if(new.serialized = 1) then begin
      select NOTSERIAL from defmagaz where SYMBOL=new.magazyn into :nserial;
      if(:nserial = 1) then begin
        new.serialized = 0;
      end
    end
  end
  if (new.ktm <> old.ktm or new.magazyn<> old.magazyn or new.wersja <> old.wersja) then begin
    update dokumser set stan = 'N' where ref =
      (select orgdokumser from dokumser where TYP = 'Z' and REFPOZ=old.ref);
  end
  if(new.serialized <> old.serialized) then
    delete from DOKUMSER where TYP = 'Z' and REFPOZ=new.ref;
end^
SET TERM ; ^
