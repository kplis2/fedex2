--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_BU_CHECKCOSTSRING FOR BKDOCS                         
  ACTIVE BEFORE UPDATE POSITION 1 
as
  declare variable koszty varchar(255);
  declare variable sum4 numeric(15,2);
  declare variable sum490 numeric(15,2);
  declare variable sum5 numeric(15,2);
begin
  if (new.status > 0 and old.status = 0) then
  begin
    execute procedure GET_CONFIG('CHECKCOSTSRING', 1) returning_values koszty;
    if (koszty = '1') then
    begin
      select sum(case when D.account like '5%%' then D.debit else 0 end) as sum5,
          sum(case when D.account like '4%%' and D.account not like '490%' then D.debit else 0 end) as sum4,
          sum(case when D.account like '490%' then D.credit else 0 end) as sum490
        from decrees D
        where D.bkdoc = old.ref
        into :sum5, :sum4, :sum490;
      if (sum5 <> sum4 or sum5 <> sum490 or sum4 <> sum490) then
      begin
        if ((sum4=sum5) and (sum4<>sum490)) then
        begin
          sum490 = sum490 - sum4;
          if (new.autodoc = 1) then
            new.status = old.status;
          else
            exception BKDOC_EXPT 'Niezgodność konta 490 o kwotę = '||sum490;
        end
        sum4 = sum4 - sum5;
        if (sum4<>0) then
          if (new.autodoc = 1) then
            new.status = old.status;
          else
            exception BKDOC_EXPT 'Niezgodność kregu kosztowego o kwotę
4-5= ' || sum4 || '
490= ' || sum490;
      end
    end
  end
end^
SET TERM ; ^
