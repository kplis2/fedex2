--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CRMCLAIMPOS_AI_AMOUNT FOR CRMCLAIMPOS                    
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable amount numeric(14,2);
declare variable suma numeric(14,2);
begin
suma = 0;
  for
    select amount from crmclaimpos CA where CA.crmclaim = new.crmclaim
    into :amount
  do begin
    suma = suma + amount;
  end
  update crmclaims CS set CS.amount = :suma where CS.ref = new.crmclaim;
end^
SET TERM ; ^
