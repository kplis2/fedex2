--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ARTYKULY_BD_ORDER FOR ARTYKULY                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then exit;
  update artykuly set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and dzial = old.dzial;
end^
SET TERM ; ^
