--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAMUP_BI_CHECK FOR POZZAMUP                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
   if(new.ilosc = 0 or new.ilosc is null) then
     exception POZZAMUP_EXPT 'Ilość towaru do odebrania nie może być zerowa.';
   if(new.ilosc < 0) then
     exception POZZAMUP_EXPT 'Ilość towaru do odebrania nie może być ujemna.';
end^
SET TERM ; ^
