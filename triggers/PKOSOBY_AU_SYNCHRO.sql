--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PKOSOBY_AU_SYNCHRO FOR PKOSOBY                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable procedura varchar(1024);
declare variable synchro smallint;
declare variable d integer;
declare variable d2 integer;
declare variable klientref integer;
begin
  execute procedure getconfig('SYNCHROUZYKLI') returning_values :synchro;
  if (synchro = 1) then
  begin
    if (new.synchro is null and old.synchro is null) then
    begin
      procedura = 'update uzykli set';
      d = coalesce(char_length(:procedura),0);  -- [DG] XXX ZG119346
      if (new.nazwa <> old.nazwa)
        then procedura = :procedura ||' NAZWA = '''||new.nazwa||''',';
      if (new.imie <> old.imie)
        then procedura = :procedura ||' IMIE = '''||new.imie||''',';
      if (new.nazwisko <> old.nazwisko)
        then procedura = :procedura ||' NAZWISKO = '''||new.nazwisko||''',';
      if (new.miasto <> old.miasto)
        then procedura = :procedura ||' DMIASTO = '''||new.miasto||''',';
      if (new.kodp <> old.kodp)
        then procedura = :procedura ||' DKODP = '''||new.kodp||''',';
      if (new.ulica <> old.ulica)
        then procedura = :procedura ||' DULICA = '''||new.ulica||''',';
      if (new.nrdomu <> old.nrdomu)
        then procedura = :procedura ||' NRDOMU = '''||new.nrdomu||''',';
      if (new.nrlokalu <> old.nrlokalu)
        then procedura = :procedura ||' NRLOKALU = '''||new.nrlokalu||''',';
      if (new.komorka <> old.komorka)
        then procedura = :procedura ||' TELKOM = '''||new.komorka||''',';
      if (new.telefon <> old.telefon)
        then procedura = :procedura ||' TELEFON = '''||new.telefon||''',';
      if (new.fax <> old.fax)
        then procedura = :procedura ||' FAX = '''||new.fax||''',';
      if (new.email <> old.email)
        then procedura = :procedura ||' EMAIL = '''||new.email||''',';
      if (new.poczta <> old.poczta)
        then procedura = :procedura ||' DPOCZTA = '''||new.poczta||''',';
      if (new.opis<>old.opis)
        then procedura = :procedura ||' UWAGI = '''||new.opis||''',';
      if (new.pesel<>old.pesel)
        then procedura = :procedura ||' PESEL = '''||new.pesel||''',';
      if (new.aktywny<>old.aktywny)
        then procedura = :procedura ||' AKTYWNY = '''||new.aktywny||''',';
      if (new.windykacja<>old.windykacja)
        then procedura = :procedura ||' WINDYKACJA = '''||new.windykacja||''',';
      d2 = coalesce(char_length(:procedura),0);  -- [DG] XXX ZG119346
      if (d2 > d) then
      begin
        klientref = null;
        select uzykli.ref
          from uzykli where uzykli.pkosoba = new.ref
          into :klientref;
        if(:klientref is not null) then begin
          procedura = :procedura ||' SYNCHRO = 1 where uzykli.ref = '||cast(:klientref as varchar (60));
          execute statement procedura;
        end
      end
    end
    if (new.synchro = 1) then update pkosoby set pkosoby.synchro = NULL where pkosoby.ref = new.ref;
  end
end^
SET TERM ; ^
