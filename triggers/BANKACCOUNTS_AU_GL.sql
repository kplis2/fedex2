--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKACCOUNTS_AU_GL FOR BANKACCOUNTS                   
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.gl <> old.gl and new.gl = 1) then
    update bankaccounts set gl = 0 where ref <> new.ref and gl = 1 and dictdef = new.dictdef and dictpos = new.dictpos;
  if (new.gl <> old.gl and new.gl = 0) then
  begin
    if(new.DICTDEF = 1) then
      update KLIENCI set BANK = null, RACHUNEK = '' where REF=new.DICTPOS;
    else if(new.DICTDEF = 6) then
      update DOSTAWCY set BANK = null, RACHUNEK = '' where REF=new.DICTPOS;
  end
  /* aktualizacja banku i rachunku */
  if((new.gl = 1 and old.gl = 0) or
     (new.gl = 1 and ((new.account <> old.account) or (coalesce(new.bank,'') <> coalesce(old.bank,''))))) then
  begin
    if(new.dictdef = 1) then
    begin
      update klienci
        set bank = new.bank,
          rachunek = new.account
        where ref = new.dictpos;
    end
    else if(new.dictdef = 6) then
    begin
      update dostawcy
        set bank = new.bank,
          rachunek = new.account
        where ref = new.dictpos;
    end
  end

  if(new.account <> old.account) then
  begin
    if(new.dictdef = 1) then
      update klienci set state = -1 where ref = new.dictpos;
    else if(new.dictdef = 6) then
      update dostawcy set state = -1 where ref = new.dictpos;
  end

end^
SET TERM ; ^
