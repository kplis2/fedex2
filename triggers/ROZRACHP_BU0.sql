--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACHP_BU0 FOR ROZRACHP                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable walutowy smallint;
begin
  if (new.winien is null) then new.winien = 0;
  if (new.ma is null) then new.ma = 0;
  if (new.winienzl is null) then new.winienzl = 0;
  if (new.mazl is null) then new.mazl = 0;
  select walutowy from rozrach
    where company = new.company and slodef = new.slodef and slopoz = new.slopoz
      and symbfak = new.symbfak and kontofk = new.kontofk
    into :walutowy;

  if (new.kontofk is null) then
  begin
    select kontofk from rozrach
      where company = new.company and slodef = new.slodef and slopoz = new.slopoz
        and symbfak = new.symbfak
      into new.kontofk;
  end
  if (new.kontofk is null) then new.kontofk = '';

  --if (new.interestdate is null) then new.interestdate = new.data;
  --if (new.data <> old.data and old.interestdate = old.data) then new.interestdate = new.data;

  if (walutowy <> 1) then
  begin
    new.kurs = 1;
    new.winienzl = new.winien;
    new.mazl = new.ma;
  end
end^
SET TERM ; ^
