--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_USERS_BI0 FOR S_USERS                        
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.userid is null or (new.userid = 0))then
    new.userid = gen_id(GEN_USERS,1);
  if(new.userpass <> '' and new.userpass is not null) then
    new.last_change  = current_timestamp(0);
end^
SET TERM ; ^
