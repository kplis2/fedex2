--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLADDFILETYPES_AIUD_ORDER FOR EMPLADDFILETYPES               
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 
AS
declare variable fromtrig integer;
declare variable maxnum integer;
declare variable ref integer;
begin

  fromtrig = rdb$get_context('USER_TRANSACTION', 'EMPLADDFILETYPE_FROMTRIGGER');
  rdb$set_context('USER_TRANSACTION', 'EMPLADDFILETYPE_FROMTRIGGER', 1);
--Przeliczanie na zmiane emplorder
  if (coalesce(fromtrig,0) = 0) then
  begin
    if(new.emplorder is distinct from old.emplorder
      and (coalesce(new.emplorder,-1) >= -1 or coalesce(old.emplorder,-1) >= -1)
    ) then begin
      if (deleting ) then
        ref = old.ref;
      else begin
        ref = new.ref;
  
        select max(emplorder) from empladdfiletypes
          where ref <> :ref
          into :maxnum;
      end
  
      if(new.emplorder > maxnum) then
      begin
        update empladdfiletypes
          set emplorder = :maxnum + 1
          where ref = :ref;
        update empladdfiletypes
          set emplorder = emplorder - 1
          where emplorder <= new.emplorder
            and emplorder > old.emplorder
            and old.emplorder <> -1;
      end
      else if (((new.emplorder < old.emplorder or inserting) and new.emplorder <> -1)
                 or (old.emplorder = -1 and not deleting) )  then
      begin
        update empladdfiletypes
          set emplorder = emplorder + 1
          where emplorder >= new.emplorder
            and (emplorder < old.emplorder or inserting or old.emplorder = -1 )
            and ref <> :ref;
      end
      else if ((new.emplorder > old.emplorder and new.emplorder <= maxnum)
                or deleting
                or new.emplorder = -1) then
      begin
         update empladdfiletypes
           set emplorder = emplorder - 1
           where ref <> :ref
            and (emplorder <= new.emplorder or (deleting and old.emplorder <> -1) or new.emplorder = -1 )
            and emplorder > old.emplorder;
      end
    end
  --przeliczanie na zmiane persorder
  
   if (new.persorder is distinct from old.persorder
      and (coalesce(new.persorder,-1) >= -1 or coalesce(old.persorder,-1) >= -1)
    ) then begin
      if (deleting ) then
        ref = old.ref;
      else begin
        ref = new.ref;
  
        select max(persorder) from empladdfiletypes
          where ref <> :ref
          into :maxnum;
      end
  
      if(new.persorder > maxnum) then
      begin
        update empladdfiletypes
          set persorder = :maxnum + 1
          where ref = :ref;
        update empladdfiletypes
          set persorder = persorder - 1
          where persorder <= new.persorder
            and persorder > old.persorder
            and old.persorder <> -1;
      end
      else if (((new.persorder < old.persorder or inserting) and new.persorder <> -1)
                 or (old.persorder = -1 and not deleting) )  then
      begin
        update empladdfiletypes
          set persorder = persorder + 1
          where persorder >= new.persorder
            and (persorder < old.persorder or inserting or old.persorder = -1 )
            and ref <> :ref;
      end
      else if ((new.persorder > old.persorder and new.persorder <= maxnum)
                or deleting
                or new.persorder = -1) then
      begin
         update empladdfiletypes
           set persorder = persorder - 1
           where ref <> :ref
            and (persorder <= new.persorder or (deleting and old.persorder <> -1) or new.persorder = -1 )
            and persorder > old.persorder;
      end
    end
  rdb$set_context('USER_TRANSACTION', 'EMPLADDFILETYPE_FROMTRIGGER', 0);
  end
end^
SET TERM ; ^
