--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_AI_CALC_DOKUMPOZ FOR MWSACTS                        
  ACTIVE AFTER INSERT POSITION 10 
AS
declare variable quantitychange smallint;
declare variable quantitycchange smallint;
begin
  if (new.quantity > 0) then
    quantitychange = 1;
  else
    quantitychange = 0;
  if (new.quantityc > 0) then
    quantitycchange = 1;
  else
    quantitycchange = 0;
  -- obliczenie ilosci na pozcycji dokumentu magazynowego
  if (new.quantity > 0 and new.docposid > 0 and new.doctype = 'M') then
    execute procedure DOKUMPOZ_OBL_FROM_MWSACTS(new.doctype, new.docid,new.docposid,new.stocktaking,new.recdoc,quantitychange,quantitycchange);
  if (new.status > 0 or new.docposid is not null and new.doctype = 'M') then
    execute procedure xk_mws_recalc_sellav_for_stock (new.vers,new.wh);
end^
SET TERM ; ^
