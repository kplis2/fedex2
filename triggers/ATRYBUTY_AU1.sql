--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ATRYBUTY_AU1 FOR ATRYBUTY                       
  ACTIVE AFTER UPDATE POSITION 1 
as
declare variable werref integer;
declare variable wart_old varchar(255);
declare variable cnt integer;
begin
  if (new.zmien=1) then
  begin
    for
      select REF from WERSJE
        where ktm=new.ktm and ref<>new.wersjaref
        into :werref
    do begin
      select wartosc from ATRYBUTY
        where WERSJAREF=:werref and CECHA=new.cecha
        into :wart_old;

      select count(*) from ATRYBUTY
        where WERSJAREF=:werref and CECHA=new.cecha
        into :cnt;

      if (cnt>0) then
      begin
        if (wart_old is null or wart_old='' or wart_old=old.wartosc) then
          update ATRYBUTY set WARTOSC=new.wartosc
            where WERSJAREF=:werref and CECHA = new.cecha;
      end else
        insert into ATRYBUTY(KTM, WERSJAREF, CECHA, WARTOSC)
          values (new.ktm, :werref, new.cecha, new.wartosc);
    end

    update ATRYBUTY set ZMIEN=0
      where KTM=new.ktm and cecha=new.cecha and nrwersji=new.nrwersji;

  end
end^
SET TERM ; ^
