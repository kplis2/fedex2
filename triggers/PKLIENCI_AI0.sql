--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PKLIENCI_AI0 FOR PKLIENCI                       
  ACTIVE AFTER INSERT POSITION 0 
as
 declare variable slodef integer;
 declare variable cnt integer;
 declare variable company integer;
begin
  execute procedure get_global_param('CURRENTCOMPANY') returning_values company;
  if(new.crm = 1) then begin
     select ref from SLODEF where TYP='PKLIENCI' into :slodef;
     if(:slodef is null) then exception SLODEF_BEZPKLIENCI;
     select count(*) from CPODMIOTY where SLODEF = :slodef and SLOPOZ = new.ref into :cnt;
     if(:cnt > 0) then
        update CPODMIOTY set SKROT = substring(new.skrot from 1 for 40), nazwa = new.nazwa, --XXX ZG133796 MKD
                            TELEFON = new.telefon, fax = new.fax,
                            email = new.email, www = new.www,
                            ulica = new.ulica, nrdomu = new.nrdomu, nrlokalu = new.nrlokalu,
                            miasto = new.miasto, kodp = new.kodp,
                            zrodlo = new.zrodlo, forma = new.forma, branza = new.branza,
                            cstatus = new.status, uwagi = new.uwagi
        where SLODEF = :slodef and SLOPOZ = new.ref ;
     else
       insert into CPODMIOTY(FIRMA, AKTYWNY, SKROT, NAZWA,
                               TELEFON, FAX, EMAIL, WWW,SLODEF, SLOPOZ,
                               ULICA, NRDOMU, NRLOKALU,
                               MIASTO, KODP, ZRODLO, FORMA, BRANZA,
                               CSTATUS, UWAGI, COMPANY)
       values(1,1, substring(new.skrot from 1 for 40), new.nazwa,--XXX ZG133796 MKD
              new.telefon, new.fax, new.email, new.www, :slodef, new.ref,
              new.ulica, new.nrdomu, new.nrlokalu,
              new.miasto, new.kodp, new.zrodlo, new.forma, new.branza,
              new.status, new.uwagi, :company);
  end
end^
SET TERM ; ^
