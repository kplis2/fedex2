--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROPERSRAPS_BI0 FOR PROPERSRAPS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable guide smallint;
declare variable calc smallint;
declare variable properrapcalcamountr smallint;
begin
  select pg.blocked, pg.ref
    from prschedopers po
      left join prschedguides pg on po.guide = pg.ref
    where po.ref = new.prschedoper
    into :guide, new.prschedguide;

  if(guide>0) then exception prschedguidesblocks_expt;
  if(new.amount is null) then new.amount = 0;
  if(new.setuptime is null) then new.setuptime = 0;
  if (new.regtime is null) then new.regtime = current_timestamp(0);
  if (new.maketimefrom > new.maketimeto) then
    exception propersraps_error 'Data rozpoczęcia nie może być późniejsza od daty zakończenia';
  if (new.operator is null) then
    execute procedure get_global_param('AKTUOPERATOR') returning_values new.operator;

  execute procedure GET_CONFIG('PROPERRAPCALCAMOUNTR', 2) returning_values :properrapcalcamountr;
  if(coalesce(properrapcalcamountr,0) = 0) then begin
    if(new.econtractsposdef is not null) then
      select E.calcamountresult
        from econtractsposdef E
        where E.ref = new.econtractsposdef
      into new.calcamountresult;
    else new.calcamountresult = 1;
  end
end^
SET TERM ; ^
