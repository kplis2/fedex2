--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLPOZZAM_BU0 FOR CPLPOZZAM                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if((new.NAGRODA<>0) and (new.NAGRODA is not NULL)) then begin
    if((new.NAGRODA<>old.NAGRODA) or (old.NAGRODA is NULL)) then
      select ILPKT from CPLNAGRODY where REF=new.NAGRODA into new.CENAPKT;
  end else
    new.CENAPKT = 0;
  new.ILPKT = new.ILOSC*new.CENAPKT;
end^
SET TERM ; ^
