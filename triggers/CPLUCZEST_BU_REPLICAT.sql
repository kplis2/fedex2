--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLUCZEST_BU_REPLICAT FOR CPLUCZEST                      
  ACTIVE BEFORE UPDATE POSITION 4 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if((new.REF <> old.ref)
   or (new.cpl <> old.cpl)
   or(new.cpodmiot <> old.cpodmiot ) or (new.cpodmiot is null and old.cpodmiot is not null) or (new.cpodmiot is not null and old.cpodmiot is null)
   or(new.pkosoba <> old.pkosoba ) or (new.pkosoba is null and old.pkosoba is not null) or (new.pkosoba is not null and old.pkosoba is null)
   or(new.dataotw <> old.dataotw ) or (new.dataotw is null and old.dataotw is not null) or (new.dataotw is not null and old.dataotw is null)
   or(new.datadeaktyw <> old.datadeaktyw ) or (new.datadeaktyw is null and old.datadeaktyw is not null) or (new.datadeaktyw is not null and old.datadeaktyw is null)
   or(new.akt <> old.akt ) or (new.akt is null and old.akt is not null) or (new.akt is not null and old.akt is null)
   or(new.datalast <> old.datalast ) or (new.datalast is null and old.datalast is not null) or (new.datalast is not null and old.datalast is null)
   or(new.mnoznik <> old.mnoznik ) or (new.mnoznik is null and old.mnoznik is not null) or (new.mnoznik is not null and old.mnoznik is null)
   or(new.nrkarty <> old.nrkarty ) or (new.nrkarty is null and old.nrkarty is not null) or (new.nrkarty is not null and old.nrkarty is null)  -- zmiana dla H&B

  )then
   waschange = 1;

  execute procedure rp_trigger_bu('CPLUCZEST',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
