--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_AI0 FOR RKDOKPOZ                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable slodef integer;
declare variable slopoz integer;
declare variable company integer;
begin
  execute procedure rkdok_obl_war(new.dokument);
  if (new.konto is not null and coalesce(new.kwota,0) <> 0 and coalesce(new.rozrachunek,'') <> '') then
  begin
    select n.slodef, n.slopoz, r.company
      from rkdoknag n
        join rkrapkas r on (n.raport = r.ref)
      where n.ref = new.dokument
      into :slodef, :slopoz, :company;
    execute procedure rozrach_rkdokpoz_count(:slodef, :slopoz, new.konto, new.rozrachunek, :company);
  end
end^
SET TERM ; ^
