--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_BU_VDEBITED FOR BKDOCS                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable i smallint;
begin
  if (new.vat = 1) then
  begin
    new.vdebited = 0;
    select count(*) from bkvatpos where bkdoc = new.ref and debited = 0
      into :i;
    if (i > 0) then
    begin
      new.vdebited = 1;
      select count(*) from bkvatpos where bkdoc = new.ref and debited = 1
        into :i;
      if (i > 0) then
        new.vdebited = 2;
    end
  end
end^
SET TERM ; ^
