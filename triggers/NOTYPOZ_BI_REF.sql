--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTYPOZ_BI_REF FOR NOTYPOZ                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('NOTYPOZ')
      returning_values new.REF;
end^
SET TERM ; ^
