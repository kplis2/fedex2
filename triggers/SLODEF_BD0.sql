--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLODEF_BD0 FOR SLODEF                         
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable isdist smallint;
declare variable sql varchar(255);
begin
  if(old.predef = 1) then exception SLODEF_PREDEF;
  if (old.isdist > 0) then begin
    isdist = 0;
    if(old.company > 0) then begin
      if(old.isdist = 1) then sql =' select first 1 ref from distpos where COMPANY = '||old.company||' AND coalesce(symbol,'''') <> ''''';
      else sql =' select first 1 ref from distpos where '||old.company||' ANDcoalesce(symbol'||old.isdist||','''') <> ''''';
    end else begin
      if(old.isdist = 1) then sql =' select first 1 ref from distpos where coalesce(symbol,'''') <> ''''';
    else sql =' select first 1 ref from distpos where coalesce(symbol'||old.isdist||','''') <> ''''';
    end
    execute statement :sql into :isdist;
    if(isdist > 0) then exception isdist_existsturnovers;
  end
end^
SET TERM ; ^
