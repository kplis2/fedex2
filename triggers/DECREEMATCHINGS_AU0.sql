--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREEMATCHINGS_AU0 FOR DECREEMATCHINGS                
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  -- jak przepinamy rekordy z grupy do grupy to przeliczamy, jak dodajemy lub usuwamy z grupy to tez
  if (coalesce(new.decreematchinggroup,0) <> coalesce(old.decreematchinggroup,0) and new.blockcalc = 0) then
  begin
    if (new.decreematchinggroup > 0) then
      execute procedure decreematchinggroup_calc(new.decreematchinggroup);
    if (old.decreematchinggroup > 0) then
      execute procedure decreematchinggroup_calc(old.decreematchinggroup);
  end
  -- jak zmieniamy credit albo debit to trzeba przeliczyc grupe
  if ((new.credit <> old.credit or new.debit <> old.debit) and new.decreematchinggroup > 0 and new.blockcalc = 0) then
    execute procedure decreematchinggroup_calc(new.decreematchinggroup);
end^
SET TERM ; ^
