--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYCEN_AD0 FOR STANYCEN                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable sumcen decimal(14,2);
declare variable sumil decimal(14,4);
declare variable isstan smallint;
begin
  select count(*) from STANYIL where MAGAZYN=old.magazyn and ktm = old.ktm and wersja = old.wersja into :isstan;
  if(:isstan is null) then isstan = 0;
  if(:isstan > 0) then begin
    select sum(ilosc), sum(wartosc) from STANYCEN where MAGAZYN=old.MAGAZYN AND KTM = old.ktm AND WERSJA=old.wersja into :sumil, :sumcen;

    if(:sumil is null or (:sumil = 0)) then begin
       sumil = 1;
       sumcen = old.cena;
    end
    update STANYIL set  cena = (:sumcen / :sumil),ilosc = ilosc - old.ilosc, wartosc = wartosc - old.wartosc where
     magazyn = old.magazyn and ktm = old.ktm and wersja = old.wersja;
  end
end^
SET TERM ; ^
