--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTCEN_BI0 FOR DOSTCEN                        
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable cnt integer;
declare variable walpln varchar(10);
begin
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  else if(new.ktm is not null and new.wersja is not null) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  execute procedure GETCONFIG('WALPLN') returning_values :walpln;
  if(:walpln is null or :walpln='') then walpln = 'PLN';
  if(new.waluta is null or new.waluta='') then begin
    new.waluta = :walpln;
  end
  if(new.oddzial is null or new.oddzial='') then begin
    execute procedure GETCONFIG('AKTUODDZIAL') returning_values new.oddzial;
  end
  if(new.cena_detal is null) then new.cena_detal = 0;
  if(new.cenanet is null ) then new.cenanet = 0;
  if(new.cena_fab is null) then new.cena_fab = 0;
  if(new.symbol_dost is null) then new.symbol_dost = '';
  if(new.cenawal is null) then new.cenawal = new.cenanet;
  -- ustaw glownego dostawce jesli jest to pierwszy rekord dla tego towaru
  if(new.gl is null) then new.gl = 0;
  if (exists (select first 1 1 from DOSTCEN where WERSJAREF = new.wersjaref)) then cnt = 1;
  else cnt = 0;
  if(:cnt = 0) then new.gl = 1;
  -- ustaw glowna cene jesli jest to pierwszy rekord dla towaru w walucie PLN
  if(new.cenazakgl is null) then new.cenazakgl = 0;
  if(:cnt = 0 and new.waluta=:walpln) then new.cenazakgl = 1;
  -- ustaw akt jesli jest to pierwszy dostawca dla danego towaru
  if (exists (select first 1 1 from DOSTCEN where WERSJAREF = new.wersjaref and DOSTAWCA = new.dostawca)) then cnt = 1;
  else cnt = 0;
  if(:cnt = 0) then new.akt = 1;
  else new.akt = 0;
end^
SET TERM ; ^
