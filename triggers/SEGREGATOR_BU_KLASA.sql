--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SEGREGATOR_BU_KLASA FOR SEGREGATOR                     
  ACTIVE BEFORE UPDATE POSITION 1 
as
declare variable klasapar varchar(20);
begin
  if(new.rodzic <> old.rodzic or (new.rodzic is null and old.rodzic is not null) or (new.rodzic is not null and old.rodzic is null) or (new.lista is null)) then begin
    if(new.rodzic is not null and new.rodzic > 0) then begin
      select lista,slodef,wlasciciel from SEGREGATOR where ref = new.rodzic into new.lista,new.slodef,new.wlasciciel;
      if(new.lista is null) then new.lista = ';';
      new.lista = new.lista || new.rodzic||';';
    end else begin
      new.lista = ';';
      if(new.wlasciciel=0) then new.wlasciciel=NULL;
      if(new.slodef=0) then new.slodef=NULL;
    end
  end
/*  if(new.klasa<>old.klasa and old.klasa is not NULL) then
    exception SEGREGATOR_NOCHANGE;
  if((new.rodzic<>0) and (new.rodzic is not NULL)) then
    select KLASA from SEGREGATOR where ref = new.rodzic into :klasapar;
  else klasapar = new.klasa;
  if(:klasapar<>new.klasa) then
    exception SEGREGATOR_NOCHANGE;
*/
end^
SET TERM ; ^
