--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDECLDEFS_BI_REF FOR EDECLDEFS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('EDECLDEFS') returning_values new.ref;
end^
SET TERM ; ^
