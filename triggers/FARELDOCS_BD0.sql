--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FARELDOCS_BD0 FOR FARELDOCS                      
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (exists(
    select ref
      from fareldocs F
      where (F.fxdasset = old.fxdasset and F.docdate > old.docdate)
      or (F.fxdasset = old.fxdasset and F.docdate = old.docdate and F.regdate > old.regdate)
    )) then
    exception FARELDOCS_NOTLAST;
end^
SET TERM ; ^
