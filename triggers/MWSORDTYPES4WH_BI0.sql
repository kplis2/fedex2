--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDTYPES4WH_BI0 FOR MWSORDTYPES4WH                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.mwsordtype is not null) then
    select mwsordtypes.symbol
      from mwsordtypes where mwsordtypes.ref = new.mwsordtype
      into new.mwsordsymb;
end^
SET TERM ; ^
