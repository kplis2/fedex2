--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDTYPEDETS_BIU0 FOR MWSORDTYPEDETS                 
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
begin
  if (new.allowordadd is null) then new.allowordadd = 0;
  if (new.allowordedit is null) then new.allowordedit = 0;
  if (new.alloworddel is null) then new.alloworddel = 0;
  if (new.allowactadd is null) then new.allowactadd = 0;
  if (new.allowactadd is null) then new.allowactadd = 0;
  if (new.allowactedit is null) then new.allowactedit = 0;
  if (new.allowactdel is null) then new.allowactdel = 0;
  if (new.ext is null) then new.ext = 0;
  if (new.docout is null) then new.docout = 0;
  if (new.closeoncommit is null) then new.closeoncommit = 1;
  if (new.realmode is null) then new.realmode = 0;
end^
SET TERM ; ^
