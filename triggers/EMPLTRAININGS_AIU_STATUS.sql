--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLTRAININGS_AIU_STATUS FOR EMPLTRAININGS                  
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
AS
declare variable status int;
begin
  select STATUS from ETRAININGS where REF=new.TRAINING
    into status;
  if (status is not null) then
  begin
    if ((new.STATUS>0 and status=0) or (new.STATUS=0 and status>0)) then
      EXCEPTION NIEDOZWOLONY_STATUS;
  end
end^
SET TERM ; ^
