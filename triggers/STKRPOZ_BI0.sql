--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STKRPOZ_BI0 FOR STKRPOZ                        
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.ismaska is null) then new.ismaska = 0;
  if(new.ismaska = 0) then begin
    if(new.wersjaref is not null and new.wersjaref <> 0) then
      select nrwersji from WERSJE where ref=new.wersjaref into new.wersja;
    else if(new.ktm is not null and new.wersja is not null) then
      select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
    if(new.ktm <> '')then
      select NAZWA, MIARA from TOWARY where KTM = new.ktm into new.nazwat, new.miara;
  end else begin
    new.wersjaref = null;
    new.wersja = 0;
    new.nazwat = '';
    new.miara = '';
  end
  if(new.status is null) then new.status = 0;
  new.lastdatachange = current_time;
end^
SET TERM ; ^
