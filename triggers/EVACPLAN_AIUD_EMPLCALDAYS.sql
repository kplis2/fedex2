--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACPLAN_AIUD_EMPLCALDAYS FOR EVACPLAN                       
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 
AS
begin
/*Oznaczenie na kalendarzu pracownika, czy w danym okresie istnieje
  zarejestrowany, nieodrzucony wniosek*/

  if (inserting or deleting or old.ref <> new.ref
    or old.employee <> new.employee or old.state <> new.state
    or old.fromdate <> new.fromdate or old.todate <> new.todate
  ) then begin

    if (not inserting) then
      update emplcaldays
        set vacplan = null
        where employee = old.employee
          and cdate between old.fromdate and old.todate;

    if (not deleting and new.state <> 3) then
      update emplcaldays
        set vacplan = new.ref
        where employee = new.employee
          and cdate between new.fromdate and new.todate;
  end
end^
SET TERM ; ^
