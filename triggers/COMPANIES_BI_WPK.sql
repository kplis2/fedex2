--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER COMPANIES_BI_WPK FOR COMPANIES                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  --Zakad nie może być naraz wzorcowym i podlegac pod wzorzec
  if (new.pattern = 1 and new.pattern_dependent = 1) then
    exception company_pattern_error;
end^
SET TERM ; ^
