--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_AD0 FOR DOKUMNAG                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable NUMBERG varchar(40);
declare variable numpoakcept smallint;
declare variable local smallint;
declare variable rkdoknag integer;
declare variable rknumer integer;
declare variable rkstanowisko varchar(3);
declare variable rktyp varchar(3);
declare variable rkusun varchar(20);
declare variable rkanulowano integer;
declare variable autocreaterkrap smallint;
declare variable cnt integer;
declare variable BLOCKREF integer;
declare variable sumwaga numeric(14,4);
declare variable dokumpoz integer;
declare variable frompozzam integer;
declare variable status integer;
declare variable msg varchar(4096);
begin
  if(old.numer <> 0) then begin
    select NUMBER_GEN, NUMPOAKCEPT from DEFMAGAZ where  symbol = old.magazyn into :numberg, :numpoakcept;
    if(:numpoakcept is null) then numpoakcept = 0;
    execute procedure CHECK_LOCAL('DOKUMNAG',old.ref) returning_values :local;
    if(:numberg is not null and :local = 1) then begin
      if(old.numer > -1) then begin
        execute procedure free_number(:numberg, old.magazyn, old.typ, old.data, old.numer, old.numblockget) returning_values :blockref;
        if(:blockref > 0) then
          update NUMBERBLOCK set OPERATOR = old.operator where ref=:blockref;
      end
    end
  end
  if(old.akcept = 1 and old.zamowienie >0) then
    execute procedure MAG_CHECK_ZAM_ACK(old.zamowienie);
  if(old.zamowienie>0 and old.historia>0 and old.zrodlo=0) then
    execute procedure OPER_BACK_OPER(old.zamowienie,old.historia,0,0,0) returning_values :status,:msg;
  if((old.akcept = 1) and (:local = 1))then begin
    if(old.rkdoknag > 0) then begin
      rkdoknag = old.rkdoknag;
      execute procedure GETCONFIG('KASSPRZUSU') returning_values :rkusun;
      select ANULOWANY, NUMER, STANOWISKO,TYP from RKDOKNAG where REF=old.rkdoknag into :rkanulowano, :rknumer, :rkstanowisko, :rktyp;
      if(:rkusun = '1') then begin
        if(:rknumer > 0) then begin
           cnt = null;
           select count(*) from RKDOKNAG where STANOWISKO = :rkstanowisko and TYP=:rktyp and NUMER > :rknumer into :cnt;
           if(:cnt > 0) then begin
             rkusun = '';
           end
        end
      end else if(:rkusun = '2') then
        rkusun = '1';
      else rkusun = '';
      if(:rkusun <>'')then begin
         execute procedure GETCONFIG('KASUSUNLAST') returning_values :rkusun;
         if(:rkusun = '1') then begin
           /*sprawdzenie, czy usuwac, czy anulowac jednak*/
           cnt = null;
           select count(*) from RKDOKNAG where STANOWISKO = :rkstanowisko and TYP=:rktyp and NUMER > :rknumer into :cnt;
           if(:cnt > 0) then rkusun = '';
         end
         if(:rkusun ='1' and :rknumer > 0) then begin
           update RKDOKNAG set NUMER = 0 where REF=:rkdoknag;
           delete from  RKDOKNAG where REF=:rkdoknag;
           rkdoknag = null;
         end else
           update RKDOKNAG set ANULOWANY=1, ANULOPIS='Anulowanie dok. magazynowego' where REF=:rkdoknag;
      end else
         exception NAGFA_ACK_SARKDOKNAG;
    end
    delete from LISTYWYSD where REFDOK = old.ref;
  end
  if(old.faktura > 0) then begin
    execute procedure NAGFAK_UPDATE_MAGCONNECT(old.faktura);
  end
  update DOKUMNAG set REF2=NULL, SYMBOL2 = '' where REF2 = old.ref;
  if(old.zamowienie > 0 and old.faktura is null) then
     execute procedure nagzam_hasdokmagbezfak(old.zamowienie);
  if(old.numblock > 0) then
    update NUMBERBLOCK set DOKDOKUMNAG = null where ref=old.numblock;
  if(old.grupasped = old.ref) then
    update DOKUMNAG set GRUPASPED = REF where grupasped = old.ref;
  else
    execute procedure dokumnag_grupased_koszt(old.grupasped);
  delete from cploper
    where cploper.typdok='DOKUMNAG' and cploper.refdok=old.ref;
 /* naliczenie wagi z dokumentow na rejestrze wjazdow i wyjazdow */
  if(((old.akcept=1) or (old.akcept=8)) and old.cinout is not null) then begin
    sumwaga = 0;
    select sum(WAGA) from DOKUMNAG where CINOUT=old.cinout and ((AKCEPT=1) or (AKCEPT=8))  into :sumwaga;
    update CINOUTS set DOCWEIGHT=:sumwaga where REF=old.cinout;
  end
  delete from NAGFAKTERMPLAT where doktyp='M' and dokref = old.ref;
end^
SET TERM ; ^
