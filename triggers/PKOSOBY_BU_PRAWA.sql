--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PKOSOBY_BU_PRAWA FOR PKOSOBY                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
 if((new.cpodmiot<>old.cpodmiot) or (new.cpodmiot is not null and old.cpodmiot is null)) then
   select PRAWA,PRAWAGRUP,SKROT,AKTYWNY,SLODEF,OPEROPIEK,CSTATUS,TYP,COMPANY,ANCESTORS from CPODMIOTY where REF=new.CPODMIOT
   into new.prawa, new.prawagrup, new.cpodmskrot, new.cpodmaktywny, new.cpodmslodef, new.cpodmoperopiek, new.cpodmcstatus, new.cpodmtyp, new.company, new.cpodmancestors;
 else if(new.cpodmiot is null) then begin
   new.prawa = '';
   new.prawagrup = '';
   new.cpodmskrot = NULL;
   new.cpodmaktywny = NULL;
   new.cpodmoperopiek = NULL;
   new.cpodmcstatus = NULL;
   new.cpodmtyp = NULL;
   new.cpodmancestors = NULL;
 end
end^
SET TERM ; ^
