--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_BU3 FOR MWSACTS                        
  ACTIVE BEFORE UPDATE POSITION 3 
AS
declare variable whp varchar(3);
declare variable whl varchar(3);
begin
  if (new.mwsconstlocp is not null and new.mwsconstlocl is not null
    and (new.mwsconstlocp <> old.mwsconstlocp or new.mwsconstlocl <> old.mwsconstlocl
      or (new.mwsconstlocp is not null and old.mwsconstlocp is null)
      or (new.mwsconstlocl is not null and old.mwsconstlocl is null))
  ) then
  begin
    select wh from mwsconstlocs where ref = new.mwsconstlocp
      into whp;
    select wh from mwsconstlocs where ref = new.mwsconstlocl
      into whl;
    if (whp is not null and whp <> '' and whl is not null and whl <> '' and whl <> whp) then
      exception universal 'Próba przesunięcia towarów między różnymi magazynami';
  end
  if ((new.mwsconstlocl is not null and old.mwsconstlocl is null) or new.mwsconstlocl <> old.mwsconstlocl) then
  begin
    select wh from mwsconstlocs where ref = new.mwsconstlocl into whl;
    select wh from mwsords where ref = new.mwsord into whp;
    if (whp <> whl) then
      exception universal 'Lokacja należy do magazynu: '||whl;
  end
  if ((new.mwsconstlocp is not null and old.mwsconstlocp is null) or new.mwsconstlocp <> old.mwsconstlocp) then
  begin
    select wh from mwsconstlocs where ref = new.mwsconstlocp into whp;
    select wh from mwsords where ref = new.mwsord into whl;
    if (whp <> whl) then
      exception universal 'Lokacja należy do magazynu: '||whp;
  end
end^
SET TERM ; ^
