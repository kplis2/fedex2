--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRHDRS_AU_FRVERSION FOR FRHDRS                         
  ACTIVE AFTER UPDATE POSITION 0 
AS
  declare variable fpref integer;
  declare variable fpalg smallint;
  declare variable ssumpsn integer;
  declare variable sratio smallint;
  declare variable maxfrver integer;
  declare variable maxfrver1 integer;
  declare variable sfrcol integer;
  declare variable salgtype smallint;
  declare variable sproced varchar(4096);
  declare variable sbody varchar(4096);
  declare variable sdecl varchar(4096);

begin
  if (new.status = 1 and old.status = 0) then
  begin
    select coalesce(max(frversion), 0)
      from frsumpsns FS
        join frpsns FP on (FP.ref = FS.frpsn)
      where FP.frhdr = new.symbol
      into :maxfrver1;

    select coalesce(max(frversion), 0)
      from frpsnsalg FA
        join frpsns FP on (FP.ref = FA.frpsn)
      where FP.frhdr = new.symbol
      into :maxfrver;

    if (maxfrver < maxfrver1) then
      maxfrver = maxfrver1;

    for
      select FP.ref, FP.algorithm
        from frpsns FP
        where FP.frhdr = new.symbol
        into :fpref, :fpalg
    do begin
      if (fpalg = 2) then
      begin
        for
          select FS.sumpsn, FS.ratio
            from frsumpsns FS
            where FS.frpsn = :fpref and FS.frversion = 0
            into :ssumpsn, :sratio
        do begin
          insert into frsumpsns (frpsn, sumpsn, ratio, frversion)
            values (:fpref, :ssumpsn, :sratio, :maxfrver + 1);
        end
      end
      else if (fpalg = 3) then
      begin
        for
          select FA.frcol, FA.algtype, FA.proced, FA.body, FA.decl
            from frpsnsalg FA
            where FA.frpsn = :fpref and FA.frversion = 0
            into :sfrcol, :salgtype, :sproced, :sbody, :sdecl
        do begin
          insert into frpsnsalg (frpsn, frcol, algtype, proced, body, decl, frversion)
            values (:fpref, :sfrcol, :salgtype, :sproced, :sbody, :sdecl, :maxfrver +1);
        end
      end
    end
  end
end^
SET TERM ; ^
