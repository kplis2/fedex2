--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER REZPOZ_BI_PMG FOR REZPOZ                         
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable pmelem integer;
begin
  select rn.pmelement from reznag rn where rn.ref = new.reznag into pmelem;
  if (pmelem is not null and pmelem <> 0) then
  begin
    if (exists(select rezzasob from rezpoz where reznag = new.reznag)) then
      exception REZNAG_WRONGROZLICZ;
  end
end^
SET TERM ; ^
