--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DISTPOS_BI_ORDER FOR DISTPOS                        
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable maxnum integer;
begin
  new.ord = 1;
    select first 1 number from distpos where decrees = new.decrees
      order by decrees desc, number desc
    into :maxnum;
  if (new.number is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update distpos set ord = 0, number = number + 1
      where number >= new.number and decrees = new.decrees
      order by decrees, number;
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
