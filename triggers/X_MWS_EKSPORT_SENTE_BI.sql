--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWS_EKSPORT_SENTE_BI FOR X_MWS_EKSPORT_SENTE            
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_x_mws_eksport_sente,1);
  if (new.DATA is null) then
    new.DATA = current_timestamp(0);
end^
SET TERM ; ^
