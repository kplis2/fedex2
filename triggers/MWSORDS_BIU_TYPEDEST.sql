--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDS_BIU_TYPEDEST FOR MWSORDS                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.mwsordtypedest is null) then
  begin
    select mo.mwsortypedets
      from mwsordtypes mo
      where mo.ref = new.mwsordtype
      into new.mwsordtypedest;
  end 
  if (new.mwsordtypedest = 15 and coalesce(new.docsymbs,'') <> '' and coalesce(new.whsecgroups,'') = '') then
  begin
    select first 1 g.whsecgroups
      from mwsconstlocs c
        left join whsecgroups g on (g.whsecgroups like '%;'||c.whsec||';%')
      where c.symbol = new.docsymbs
      into new.whsecgroups;
  end
  if (new.status > 1 and new.status < 5 and old.status = 1 and new.mwsordtypedest = 11 and new.palgroup is not null) then
  begin
    update mwspallocs p set p.status = 1 where p.ref = new.palgroup;
  end
end^
SET TERM ; ^
