--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_BU_QUANTITYSTRING FOR MWSACTS                        
  ACTIVE BEFORE UPDATE POSITION 1 
AS
declare variable procsql varchar(1024);
declare variable quantitystring varchar(80);
declare variable quantity numeric(14,4);
begin
  if (new.good <> old.good or ((new.quantity - new.quantityc) <> (old.quantity - old.quantityc))) then
  begin
    quantity = new.quantity - new.quantityc;
    if (quantity < 0) then quantity = 0;
    execute procedure get_config('MWS_QYANTITYS_PROC',2) returning_values(procsql);
    procsql = 'select QUANTITYSTRING from '||procsql||'('''||new.good||''','||new.vers||','||quantity||',1)';
    execute statement procsql into quantitystring;
    new.quantitystring = quantitystring;
  end
end^
SET TERM ; ^
