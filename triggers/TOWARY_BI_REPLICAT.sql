--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWARY_BI_REPLICAT FOR TOWARY                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
 execute procedure rp_trigger_bi('TOWARY',new.ktm, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
