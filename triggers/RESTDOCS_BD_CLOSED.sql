--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RESTDOCS_BD_CLOSED FOR RESTDOCS                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
  declare variable period_status integer;
begin
    select status from amperiods
    where ammonth = extract(month from old.docdate) and amyear = extract(year from old.docdate)
    into :period_status;
    if (period_status > 0) then exception AMPERIOD_CLOSED 'Nie można usunąć dokumentu dla zamkniętego okresu!';
end^
SET TERM ; ^
