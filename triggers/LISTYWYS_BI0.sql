--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYS_BI0 FOR LISTYWYS                       
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable cnt integer;
begin
  if(new.stan is null or (new.stan = ' ')) then new.stan = 'O';
  if(new.trasa = 0) then new.trasa = null;
  if(new.kierowca = 0) then new.kierowca = null;
  if(new.typ is null) then exception LISTYWYS_BRAK_TYPU;
  if((new.symbol is null) or (new.symbol = ''))then
    new.symbol = 'TYM'||new.ref;
  if(new.spedytor is not null and new.trasa > 0) then begin
    select count(*) from SPEDYTTRAS where SPEDYTOR = new.spedytor and trasa = new.trasa into :cnt;
    if(:cnt is null) then cnt = 0;
    if(:cnt = 0) then exception LISTYWYS_SPEDYTORBEZTRASY;
  end
  new.waga = 0;
  new.objetosc = 0;
  if(new.typ is null or new.typ = '') then new.typ = '1';
  if(new.spedytor='') then new.spedytor = null;
  if(new.datawys is not null) then
    select okres from datatookres(cast(new.datawys as date),null,null,null,0) into new.okres;
  else if (new.dataplwys is not null) then
    select okres from datatookres(cast(new.dataplwys as date),null,null,null,0) into new.okres;
  if(new.rozliczono is null) then new.rozliczono = 0;
end^
SET TERM ; ^
