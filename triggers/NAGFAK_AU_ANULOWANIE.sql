--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_AU_ANULOWANIE FOR NAGFAK                         
  ACTIVE AFTER UPDATE POSITION 1 
AS
declare variable local smallint;
declare variable dokmagref integer;
declare variable cnt integer;
declare variable dokmag varchar(3);
declare variable dokmagk varchar(3);
declare variable dokmago varchar(3);
declare variable dokmagko varchar(3);
declare variable magazyn char(3);
declare variable datadokm timestamp;
declare variable aktulocat varchar(255);
declare variable hasdokmag smallint;
declare variable dokmagonakcept smallint;
declare variable stat integer;
declare variable DOKSYM varchar(20);
declare variable minref integer;
declare variable maxref integer;
declare variable minsymbol varchar(100);
declare variable maxsymbol varchar(100);
declare variable minwartosc numeric(14,2);
declare variable maxwartosc numeric(14,2);
declare variable wystawdok integer;
declare variable reffakgrupa integer;
begin
  execute procedure CHECK_LOCAL('NAGFAK',new.ref) returning_values :local;
  if(new.anulowanie>0 and old.anulowanie = 0 and (:local = 1)) then begin
    wystawdok = 1;
    if(new.anulowanie=2) then begin
      wystawdok = 0;
    end
    /*anulowanie dokumentu VAT*/
    /*usunicie not magazynowych */
    for select ref from DOKUMNOT where FAKTURA = new.ref
    into :dokmagref
    do begin
      update DOKUMNOT set AKCEPT = 0 where REF=:dokmagref;
      delete from DOKUMNOT where ref=:dokmagref;
    end
    cnt = null;
    /*usuniecie wpisów w rozrachunkach*/
    delete from ROZRACHP where FAKTURA = new.ref and SKAD = 1;
    select count(*) from ROZRACHP  where ROZRACHP.FAKTURA = new.ref and skad <> 6 and SKAD <> 3 into :cnt;
    if(:cnt > 0) then
         exception NAGFAF_ODAKCPET_BYLY_ZAPLATY;
    delete from ROZRACH where FAKTURA=new.ref and SKAD = 1;
    /*wypene korekty dokumentów magazynowyc powiązanych z faktura*/
    /*TODO: Kuba*/
    /*sprawdzenie, czy wygeneroane dok. magazynowe nie są w jakim dziwnym stanie*/
    cnt = null;
    select count(*) from DOKUMNAG where FAKTURA = new.ref and (AKCEPT <> 1 and AKCEPT <> 8) into :cnt;
    if(:cnt > 0) then exception NAGFAK_ANUL_MADOKMAGNIEAKCEPT;


    /*sprawdzenie, czy dany dokument generowal dokumenty magazynowe przy akceptacji*/
    cnt = null;
/*    select count(*) from DOKUMNAG where FAKTURA = new.ref and (ZRODLO = 2 or ZRODLO = 3) into :cnt;*/
    cnt = 1;
    if(:wystawdok > 0) then begin
      select DOKMAGONAKCEPT from TYPFAK where SYMBOL = new.TYP into :dokmagonakcept;
      if(:dokmagonakcept > 0) then dokmagonakcept = 3;
      select TYPDOK, TYPDOKK,TYPDOKO,TYPDOKKO
      from STANTYPFAK where STANFAKTYCZNY = new.stanowisko and TYPFAK = new.typ
       into :dokmag,:dokmagk,:dokmago,:dokmagko;
      datadokm = current_date;
      execute procedure GETCONFIG('AKTULOCAT') returning_values :aktulocat;
      hasdokmag = 0;
      for select distinct POZFAK.MAGAZYN from POZFAK
      left join DEFMAGAZ on (DEFMAGAZ.symbol = POZFAK.MAGAZYN)
      left join ODDZIALY on (ODDZIALY.oddzial = DEFMAGAZ.oddzial)
      where POZFAK.DOKUMENT = new.ref and ODDZIALY.LOCATION=:aktulocat
      into :magazyn
      do begin
         execute procedure NAGFAK_CREATE_DOKMAG(new.ref, :magazyn, :dokmag, :dokmagk, :dokmago, :dokmagko, :datadokm, :dokmagonakcept, :hasdokmag, 0) returning_values :stat,:doksym;
         select count(*) from DOKUMNAG where FAKTURA = new.ref and MAGAZYN=:magazyn and (ZRODLO = 2 or ZRODLO = 3) into :cnt;
         if(:cnt=2) then begin
           /* jesli sa dwa dokumenty na danym magazynie to je skojarz */
           select min(REF) from DOKUMNAG where FAKTURA = new.ref and MAGAZYN=:magazyn and (ZRODLO = 2 or ZRODLO = 3) into :minref;
           select max(REF) from DOKUMNAG where FAKTURA = new.ref and MAGAZYN=:magazyn and (ZRODLO = 2 or ZRODLO = 3) into :maxref;
           if(:minref<>:maxref) then begin
             select SYMBOL,WARTOSC from DOKUMNAG where REF=:minref into :minsymbol,:minwartosc;
             select SYMBOL,WARTOSC from DOKUMNAG where REF=:maxref into :maxsymbol,:maxwartosc;
             update DOKUMNAG set REF2=:maxref, SYMBOL2=:maxsymbol,WARTOSC2=:maxwartosc where REF=:minref;
             update DOKUMNAG set REF2=:minref, SYMBOL2=:minsymbol,WARTOSC2=:minwartosc where REF=:maxref;
           end
         end
      end
    end
  end else if(new.anulowanie=0 and old.anulowanie<>0 and (:local = 1)) then begin
    for select DOKUMNAG.REF from DOKUMNAG join defdokum on (DOKUMNAG.TYP = DEFDOKUM.SYMBOL)
      where DOKUMNAG.FAKTURA = new.ref and DOKUMNAG.ZANULOWANIA = 1
      order by DEFDOKUM.wydania desc
      into :dokmagref
      do begin
        update DOKUMNAG set FAKTURA=NULL, NUMBLOCKGET = -new.numblock where ref=:dokmagref;
        update DOKUMNAG set AKCEPT=0 where REF = :dokmagref;
        delete from DOKUMNAG where REF = :dokmagref;
      end
    execute procedure DOCUMENTS_DELFROM_BKDOCS(0,new.ref);
    execute procedure NAGFAK_UPDATE_MAGCONNECT(new.ref);
    update NAGFAK set AKCEPTACJA=10 where REF=new.ref;
    update NAGFAK set AKCEPTACJA=1 where REF=new.ref;
  end
end^
SET TERM ; ^
