--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ERECRUITS_BIU_BLANK FOR ERECRUITS                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
  declare variable i integer;
begin
  if (coalesce(new.symbol,'') = '') then
  begin
    new.symbol = extract(year from new.startdate);
    if (extract(month from new.startdate)<10) then
      new.symbol = new.symbol || '0';
    new.symbol = new.symbol || extract(month from new.startdate);

    select cast(substring(max(symbol) from 7 for 3) as integer) from erecruits where symbol starting with new.symbol
      into :i;
    if (i is null) then i = 0;
    i = i + 1;

    new.symbol = new.symbol || '00' || i ;
  end
  if (new.www is null) then new.www = 0;
  if (new.costs is null) then new.costs = 0;
  if (new.candsqnt is null) then new.candsqnt = 0;

--<XXX MWr_20160607 Zg90589  
  if (new.x_rights is null) then new.x_rights = '';
  if (new.x_rightsgroup is null) then new.x_rightsgroup = '';
--XXX>
end^
SET TERM ; ^
