--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECOLLDEDUCTIONS_BI_REF FOR ECOLLDEDUCTIONS                
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure GEN_REF('ECOLLDEDUCTIONS') returning_values new.ref;
end^
SET TERM ; ^
