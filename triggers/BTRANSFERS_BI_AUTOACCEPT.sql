--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BTRANSFERS_BI_AUTOACCEPT FOR BTRANSFERS                     
  ACTIVE BEFORE INSERT POSITION 2 
as
  declare variable autoaccept smallint;
begin
  select autoakceptaftergen from btrantype
    where symbol = new.btype
    into :autoaccept;
  if (autoaccept = 1 and new.autobtransfer = 1) then
  begin
    if (new.amount <= 0 or new.toacc = '' or new.towho = '') then
      new.status = 0;
    else begin
      new.dataakc = current_date;
      new.status = 1;
    end
  end
end^
SET TERM ; ^
