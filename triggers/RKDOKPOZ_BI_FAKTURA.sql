--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_BI_FAKTURA FOR RKDOKPOZ                       
  ACTIVE BEFORE INSERT POSITION 1 
as
  declare variable slopoz integer;
  declare variable typoper integer;
  declare variable slodef integer;
  declare variable slotyp varchar(255);
  declare variable company integer;
begin
  select TYP from RKPOZOPER where REF = new.POZOPER
    into :typoper;

  if (new.faktura is null and (typoper=2 or typoper=3)) then
  begin
    select n.SLODEF, n.SLOPOZ, r.company
      from RKDOKNAG n
        join rkrapkas r on (r.ref = n.raport)
      where n.REF=new.dokument
      into :slodef,:slopoz, :company;

    select TYP from SLODEF where REF=:slodef into :slotyp;
    if (slotyp = 'KLIENCI') then
    begin
      select first 1 ref from NAGFAK where PLATNIK=:slopoz and SYMBOL = new.rozrachunek
          and company = :company and DOZAPLATY > 0 and NIEOBROT in (0,2)
        into new.faktura;
      if(new.faktura is null) then
        select first 1 ref from NAGFAK where KLIENT=:slopoz and SYMBOL = new.rozrachunek
            and company = :company and DOZAPLATY > 0 and NIEOBROT in (0,2)
          into new.faktura;
    end
    if (slotyp = 'DOSTAWCY') then
    begin
      select first 1 ref from NAGFAK where PLATNIK=:slopoz and SYMBFAK = new.rozrachunek
          and company = :company and DOZAPLATY > 0 and NIEOBROT in (0,2)
        into new.faktura;
      if(new.faktura is null) then
        select first 1 ref from NAGFAK where DOSTAWCA=:slopoz and SYMBFAK = new.rozrachunek
            and company = :company and DOZAPLATY > 0 and NIEOBROT in (0,2)
          into new.faktura;
    end
  end

  if (new.faktura is null and typoper=1 and new.rozrachunek<>'') then
  begin
    select SLODEF,SLOPOZ from RKDOKNAG where REF=new.dokument into :slodef,:slopoz;
    select min(FAKTURA) from ROZRACH where SLODEF=:slodef and SLOPOZ=:slopoz and SYMBFAK=new.rozrachunek
      into new.faktura;
  end

  if (new.zamowienie is null and typoper=4) then
  begin
    select SLOPOZ from RKDOKNAG where REF=new.dokument
      into :slopoz;
    select ref from NAGZAM where ((PLATNIK=:slopoz) or (DOSTAWCA=:slopoz)) and ID = new.rozrachunek
      into new.zamowienie;
  end
end^
SET TERM ; ^
