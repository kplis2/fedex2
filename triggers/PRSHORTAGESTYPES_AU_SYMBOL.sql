--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHORTAGESTYPES_AU_SYMBOL FOR PRSHORTAGESTYPES               
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if (new.symbol <> old.symbol) then
    update PRSHORTAGESPRDEPARTS p set p.prshortagetype = new.symbol
      where p.prshortagetype = old.symbol;
end^
SET TERM ; ^
