--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVLOCATIONS_BI_REF FOR SRVLOCATIONS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SRVLOCATIONS')
      returning_values new.REF;
end^
SET TERM ; ^
