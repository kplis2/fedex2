--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_DOKUMNAG_AU_INVENTA FOR DOKUMNAG                       
  INACTIVE AFTER UPDATE POSITION 19 
as
declare variable refmwsord integer_id;
declare variable refmwsact integer_id;
declare variable stocktaking smallint_id;
begin
  -- Ten trigger ma byc przed MWS_DOKUMNAG_AU0, zeby wyczyscic ladnie wszystko!
  -- Akceptacja dokumentu PZ-, wystawia zlecenie wydania WT z lokacji INWENTA (ZG126337) i automatyczne jego potwierdzenie
  if (new.typ in ('IN-','IN+') and new.akcept <> old.akcept and new.akcept = 1 and exists (
        select first 1 1
          from defmagaz m
          where m.symbol = new.magazyn
            and m.mws = 1)) then begin
    execute procedure x_mws_gen_mwsordout_inwenta('M',new.ref)
      returning_values :refmwsord;
  -- odakceptowanie/"wycofanie do poprawy" dokumentu PZ-, usuniecie zlecenie wydania WT z lokacji INWENTA (ZG126337)
  end else    if (new.typ in ('IN-','IN+') and new.akcept <> old.akcept and new.akcept <> 1  and exists (
        select first 1 1
          from defmagaz m
          where m.symbol = new.magazyn
            and m.mws = 1)) then begin
    for select o.ref
      from mwsords o
      where o.docid = new.ref
        and o.doctype = 'M'
        and o.mwsordtypedest = 1
      into :refmwsord
    do begin
      update mwsords o set o.status = 1 where o.ref = :refmwsord and o.status > 1;
      for select a.ref, c.stocktaking
        from mwsacts a
          left join mwsconstlocs c on (c.ref = a.mwsconstlocp)
        where a.mwsord = :refmwsord
        into :refmwsact, :stocktaking
      do begin
        if (coalesce(:stocktaking,0) = 0) then
          exception universal 'Błąd wycofania zlecenia magazynowego z lokacji inwentaryzacyjnej';
        else
        begin
          update mwsacts a set a.status = 1 where a.ref = :refmwsact and a.status > 1;
          update mwsacts a set a.status = 0 where a.ref = :refmwsact;
          delete from mwsacts a where a.ref = :refmwsact;
        end
      end
      update mwsords o set o.status = 0 where o.ref = :refmwsord;
      delete from mwsords o where o.ref = :refmwsord;
    end

  end
end^
SET TERM ; ^
