--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECANDIDATTR_BIU_RATING FOR ECANDIDATTR                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
  declare variable AttrType SMALLINT;
begin
  -- wypeLnienie pola STRRATING zgodnie z typem cechy
  select ATTRTYPE from EDICTRECATTR
    where REF = new.ATTRIBUTE
    into :AttrType;
  if (AttrType=0) then -- Liczba
    new.STRRATING = CAST(new.RATING as VARCHAR(20)) ;
  if (AttrType=1) then -- Pytanie
    if (new.RATING=0) then
      new.STRRATING = 'Nie';
    else
      new.STRRATING = 'Tak';
  if (AttrType=2) then -- Ocena
    if (new.RATING<0.175) then
      new.STRRATING = 'Brak';
    else if (new.RATING<0.4) then
      new.STRRATING = 'Słabo';
    else if (new.RATING<0.6) then
      new.STRRATING = 'Średnio';
    else if (new.RATING<0.825) then
      new.STRRATING = 'Dobrze';
    else 
      new.STRRATING = 'Świetnie!';
end^
SET TERM ; ^
