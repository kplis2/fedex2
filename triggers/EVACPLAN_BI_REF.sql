--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACPLAN_BI_REF FOR EVACPLAN                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EVACPLAN')
      returning_values new.REF;
end^
SET TERM ; ^
