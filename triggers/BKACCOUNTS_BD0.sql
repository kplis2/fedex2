--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKACCOUNTS_BD0 FOR BKACCOUNTS                     
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if (exists(select ref from accounting
      where company = old.company and yearid = old.yearid and account starting with old.symbol))
  then
    exception fk_cant_delete_bkaccount;
  
  if (exists(select ref from ACCSTRUCTUREDISTS where otable = 'BKACCOUNTS' and oref = old.ref))
  then
    exception fk_cant_delete_bkaccount 'Nie można usunąć konta, które posiada wyróżniki!';
end^
SET TERM ; ^
