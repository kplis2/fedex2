--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMAILZAL_BIU_REF FOR EMAILZAL                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EMAILZAL')
      returning_values new.REF;
  --BS41102
  if(new.hidden is null) then
    new.hidden = 0;
end^
SET TERM ; ^
