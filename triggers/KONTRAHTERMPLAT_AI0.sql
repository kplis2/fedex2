--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONTRAHTERMPLAT_AI0 FOR KONTRAHTERMPLAT                
  ACTIVE AFTER INSERT POSITION 0 
AS
  declare variable procentsp numeric(3,2);
begin
  select sum(k.procentplat)
    from KONTRAHTERMPLAT k
    where k.slodef = new.slodef and k.slopoz = new.slopoz into :procentsp;
  if (procentsp > 100) then
    exception TAB_TERMPLAT_PROCENT;
end^
SET TERM ; ^
