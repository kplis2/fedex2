--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHORTAGES_BU_HEADER FOR PRSHORTAGES                    
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (new.headergroupamount <> old.headergroupamount) then
  begin
    execute procedure prshortage_amount_calc(new.headergroupamount, new.prshortagestype, new.prschedguidespos, new.prschedoper)
      returning_values new.amount;

    if (coalesce(new.headergroup, new.ref) = new.ref) then
      update prshortages s set s.headergroupamount = new.headergroupamount
        where s.headergroup = new.headergroup and s.ref <> new.ref;
  end
end^
SET TERM ; ^
