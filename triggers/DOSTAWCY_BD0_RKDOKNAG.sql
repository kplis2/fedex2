--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTAWCY_BD0_RKDOKNAG FOR DOSTAWCY                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (exists (select ref from rkdoknag R where R.slodef = 6 and R.slopoz = old.ref)) then
    exception DOSTAWCY_EXPT 'Dla podmiotu jest wystawiony dokument kasowy!';
end^
SET TERM ; ^
