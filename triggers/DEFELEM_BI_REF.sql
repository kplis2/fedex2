--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFELEM_BI_REF FOR DEFELEM                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DEFELEM')
      returning_values new.REF;
end^
SET TERM ; ^
