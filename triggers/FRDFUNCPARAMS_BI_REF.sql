--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDFUNCPARAMS_BI_REF FOR FRDFUNCPARAMS                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FRDFUNCPARAMS')
      returning_values new.REF;
end^
SET TERM ; ^
