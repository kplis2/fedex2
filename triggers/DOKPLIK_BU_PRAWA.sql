--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKPLIK_BU_PRAWA FOR DOKPLIK                        
  ACTIVE BEFORE UPDATE POSITION 1 
as
begin
 if(new.prawadef=0) then begin
   if((new.cpodmiot<>old.cpodmiot) or ((new.cpodmiot is not null) and (old.cpodmiot is null))) then
     select PRAWA,PRAWAGRUP from CPODMIOTY where ref = new.cpodmiot
     into new.prawa, new.prawagrup;
   else begin
     new.prawa = '';
     new.prawagrup = '';
   end
 end
 if(new.prawa<>old.prawa) then
   execute procedure GET_SUPERIORS(new.prawa) returning_values new.prawa;
 if((new.cpodmiot<>old.cpodmiot) or (new.CPODMIOT is not NULL and old.cpodmiot is null)) then begin
   select SKROT,OPEROPIEK from CPODMIOTY where REF=new.CPODMIOT
   into new.cpodmskrot, new.cpodmoperopiek;
 end else if(new.cpodmiot is null) then begin
   new.cpodmskrot = NULL;
   new.cpodmoperopiek = NULL;
 end
end^
SET TERM ; ^
