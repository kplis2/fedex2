--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FK_CHECK_ACCOUNT_DISTDEF_BI0 FOR FK_CHECK_ACCOUNT_DISTDEF       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  select count(*)
    from FK_CHECK_ACCOUNT_DISTDEF
    --BS69913 indeks i wymuszenie planu zapytania
    where CONTEXT_ID = new.CONTEXT_ID
    plan(FK_CHECK_ACCOUNT_DISTDEF index(FK_CHECK_ACCOUNT_DISTDEF_IDX2))
    --/BS69913
  into new.NUMBER;

  new.NUMBER = new.NUMBER + 1;
end^
SET TERM ; ^
