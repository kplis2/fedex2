--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FXDTYPES_BI_REF FOR FXDTYPES                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FXDTYPES')
      returning_values new.REF;
end^
SET TERM ; ^
