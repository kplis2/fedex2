--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_BD_PARENT FOR CPODMIOTY                      
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if (exists(select ref from cpodmioty where parent = old.ref)) then
    exception universal 'Wybrany kontrahent posiada podmioty podrzędne. Usunięcie niemożliwe.' ;
end^
SET TERM ; ^
