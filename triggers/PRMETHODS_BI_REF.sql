--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRMETHODS_BI_REF FOR PRMETHODS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('PRMETHODS')
      returning_values new.REF;
end^
SET TERM ; ^
