--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTUREDISTS_BI_REF FOR ACCSTRUCTUREDISTS              
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if(new.ref is null) then
    execute procedure gen_ref('ACCSTRUCTUREDISTS') returning_values new.ref;
end^
SET TERM ; ^
