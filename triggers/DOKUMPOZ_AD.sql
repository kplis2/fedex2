--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_AD FOR DOKUMPOZ                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable ack integer;
begin
  select AKCEPT from DOKUMNAG where ref=old.dokument into :ack;
  if(:ack = 0 or :ack = 9) then
    execute procedure DOKMAG_OBL_WAR(old.dokument,0);
end^
SET TERM ; ^
