--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSD_BD0 FOR LISTYWYSD                      
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable stan char(1);
begin
  select stan from LISTYWYS where ref = old.listawys into :stan;
  if(:stan = 'A') then
    exception LISTYWYS_USUND;
end^
SET TERM ; ^
