--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EAPPLTYPES_BD FOR EAPPLTYPES                     
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if (old."TYPE" = 0) then
    exception universal 'Nie można usunąć tej pozycji slownika - jest wymagana przez WebSerwis w eHRM.';
  else if (old."TYPE" = 1) then
    if (not exists(select first 1 1 from EAPPLTYPES where type = 1 and ref <> old.ref)) then
      exception universal 'Musi zostac co najmniej jedna pozycja slownika o typie = 1.';
end^
SET TERM ; ^
