--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER COMPANIES_BI_REF FOR COMPANIES                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('COMPANIES')
      returning_values new.REF;
end^
SET TERM ; ^
