--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWORKCERTIFS_AIUD_E_VAC_CARD FOR EWORKCERTIFS                   
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 2 
AS
begin
  if (updating and new.fromdate = old.fromdate and new.todate = old.todate
    and coalesce(new.syears,0) = coalesce(old.syears,0)
    and coalesce(new.smonths,0) = coalesce(old.smonths,0)
    and coalesce(new.sdays,0) = coalesce(old.sdays,0)
    and coalesce(new.employee,0) = coalesce(old.employee,0)
    and coalesce(new.seniorityflags,'') = coalesce(old.seniorityflags,'')
  ) then
    exit;

  if (';'||new.seniorityflags||';' containing ';STU;' or ';'||old.seniorityflags||';' containing ';STU;') then
    execute procedure e_vac_card (current_date, -coalesce(new.employee, old.employee));
end^
SET TERM ; ^
