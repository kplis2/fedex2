--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERREJ_BI0 FOR DEFOPERREJ                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if((new.ref is null) or (new.ref = 0)) then
    new.ref = gen_id(GEN_DEFOPERREJ,1);
  if(new.operacja = '') then new.operacja = NULL;
end^
SET TERM ; ^
