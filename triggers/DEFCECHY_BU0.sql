--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHY_BU0 FOR DEFCECHY                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable nazwa varchar(80);
begin
  if(((old.grupa is null) or (new.grupa<>old.grupa)) and new.grupa is not null) then begin
    select nazwa from DEFCECHG where REF = new.grupa into :nazwa;
  end
  if(nazwa is not null) then new.nazwagrupy = :nazwa;
  if(new.grupa is null) then new.nazwagrupy = null;
  if(coalesce(new.tabela,'')<>coalesce(old.tabela,'')) then begin
    if(coalesce(new.tabela,'') = '') then exception universal 'Nie wypełnione pole Tabela.';
  end
  if (new.wersje is null ) then new.wersje = 1;
  if (coalesce(new.partyp,0) <> coalesce(old.partyp,0) or coalesce(new.parskrot,'') <> coalesce(old.parskrot,'')) then
  begin
    if (new.partyp = 3 and coalesce(new.parskrot,'') = '') then
      exception DEFCECHY_EXCEPTION 'Brak podanego skrótu tworzącego dostawę.';
  end
end^
SET TERM ; ^
