--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_AU_OBL FOR POZZAM                         
  ACTIVE AFTER UPDATE POSITION 3 
as
declare variable local smallint;
begin
  if((new.ilosc <> old.ilosc) or (new.ilzreal <> old.ilzreal)) then
      if(new.genpozref > 0) then
        execute procedure POZZAM_OBL(new.genpozref);
  if(new.popref > 0) then
    if((new.ilzreal <> old.ilzreal) or (new.ilzdysp <> old.ilzdysp) or (new.ilzadysp <> old.ilzadysp)) then
        execute procedure POZZAM_OBL(new.popref);
  if (new.ilzreal <> old.ilzreal) then
    execute procedure pr_recalc_relations('POZZAM',new.ref,new.ilzreal - old.ilzreal,null,1);
  if(new.genpozref is null and old.genpozref is not null) then
    execute procedure POZZAM_OBL(old.genpozref);
  if(new.genpozref is not null and old.genpozref is null) then
    execute procedure POZZAM_OBL(new.genpozref);
  if (new.genpozref is not null and old.genpozref is not null and new.genpozref <> old.genpozref) then
  begin
    execute procedure POZZAM_OBL(old.genpozref);
    execute procedure POZZAM_OBL(new.genpozref);
  end
  execute procedure check_local('POZZAM',new.ref) returning_values :local;
  if(:local = 1) then begin
    if((new.WARTMAG<>old.WARTMAG) or (new.WARTRMAG<>old.WARTRMAG) or (new.KWARTMAG<>old.KWARTMAG) or
       (new.KWARTNET<>old.KWARTNET) or (new.KWARTBRU<>old.KWARTBRU) or (new.WAGA<>old.WAGA) or
       (new.wartnet<>old.wartnet) or (new.WARTBRU<>old.WARTBRU) or
       (new.WARTRNET<>old.WARTRNET) or (new.WARTRBRU<>old.WARTRBRU) or
       (new.wartnetzl<>old.wartnetzl) or (new.WARTBRUZL<>old.WARTBRUZL) or
       (new.WARTRNETZL<>old.WARTRNETZL) or (new.WARTRBRUZL<>old.WARTRBRUZL))
    then begin
      execute procedure OBLICZ_NAGZAM(new.zamowienie);
    end
  end
  if (new.prschedguidepos is not null) then
    execute procedure prschedguidedet_change(2,'POZZAM',new.ref,new.prschedguidepos);
end^
SET TERM ; ^
