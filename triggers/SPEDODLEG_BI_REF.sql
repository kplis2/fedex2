--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPEDODLEG_BI_REF FOR SPEDODLEG                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SPEDODLEG')
      returning_values new.REF;
end^
SET TERM ; ^
