--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_BIU_KWOTAZL FOR RKDOKPOZ                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 7 
AS
  declare variable techoper smallint_id;
begin
  select o.techoper
    from rkdoknag d
    join rkdefoper o  on (d.operacja = o.symbol)
    where d.ref = new.dokument
  into :techoper;
  if(techoper <> 1) then
    new.kwotazl = new.kwota * new.kurs;
end^
SET TERM ; ^
