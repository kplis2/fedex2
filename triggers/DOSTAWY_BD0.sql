--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTAWY_BD0 FOR DOSTAWY                        
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable cnt integer;
begin
  select count(*) from stanycen where dostawa=old.ref and ((ilosc<>0) or (zarezerw<>0) or (zablokow<>0) or (zamowiono<>0)) into :cnt;
  if(:cnt>0) then exception DOSTAWY_OBROTY;
end^
SET TERM ; ^
