--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER AMORTIZATION_BD_CLOSED FOR AMORTIZATION                   
  ACTIVE BEFORE DELETE POSITION 0 
AS
  declare variable period_status integer;
begin
  select status from amperiods where ref = old.amperiod into :period_status;
  if (period_status > 0) then exception AMPERIOD_CLOSED 'Nie można usunąć naliczonej amortyzacji dla zamkniętego okresu';
end^
SET TERM ; ^
