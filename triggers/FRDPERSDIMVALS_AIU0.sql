--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDPERSDIMVALS_AIU0 FOR FRDPERSDIMVALS                 
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
AS
  declare variable proc varchar(4098);
begin
 -- zakomentowane na rzecz PR42505
 /*$$IBEC$$  if (new.body is not null and new.body <> '' and new.decl is not null  $$IBEC$$*//*and new.decl <> ''*//*$$IBEC$$ ) then
  begin
    proc = 'create or alter procedure XX_FRD_ALGORITHM_PL_' || new.ref ||
'(key1 integer, key2 integer)
returns (ret numeric(16,2))
as
' || new.decl || '
begin
' || new.body || '
  suspend;
end;';
    execute statement proc;
  end
  if (new.bodyreal is not null and new.bodyreal <> '' and new.declreal is not null  $$IBEC$$*//*and new.declreal <> ''*//*$$IBEC$$ ) then
  begin
    proc = 'create or alter procedure XX_FRD_ALGORITHM_RE_' || new.ref ||
'(key1 integer, key2 integer)
returns (ret numeric(16,2))
as
' || new.declreal || '
begin
' || new.bodyreal || '
  suspend;
end;';
    execute statement proc;
  end
  if (new.bodyval1 is not null and new.bodyval1 <> '' and new.declval1 is not null  $$IBEC$$*//*and new.declval1 <> ''*//*$$IBEC$$ ) then
  begin
    proc = 'create or alter procedure XX_FRD_ALGORITHM_V1_' || new.ref ||
'(key1 integer, key2 integer)
returns (ret numeric(16,2))
as
' || new.declval1 || '
begin
' || new.bodyval1 || '
  suspend;
end;';
    execute statement proc;
  end
  if (new.bodyval2 is not null and new.bodyval2 <> '' and new.declval2 is not null  $$IBEC$$*//*and new.declval2 <> ''*//*$$IBEC$$ ) then
  begin
    proc = 'create or alter procedure XX_FRD_ALGORITHM_V2_' || new.ref ||
'(key1 integer, key2 integer)
returns (ret numeric(16,2))
as
' || new.declval2 || '
begin
' || new.bodyval2 || '
  suspend;
end;';
    execute statement proc;
  end
  if (new.bodyval3 is not null and new.bodyval3 <> '' and new.declval3 is not null  $$IBEC$$*//*and new.declval3 <> ''*//*$$IBEC$$ ) then
  begin
    proc = 'create or alter procedure XX_FRD_ALGORITHM_V3_' || new.ref ||
'(key1 integer, key2 integer)
returns (ret numeric(16,2))
as
' || new.declval3 || '
begin
' || new.bodyval3 || '
  suspend;
end;';
    execute statement proc;
  end
  if (new.bodyval4 is not null and new.bodyval4 <> '' and new.declval4 is not null  $$IBEC$$*//*and new.declval4 <> ''*//*$$IBEC$$ ) then
   begin
    proc = 'create or alter procedure XX_FRD_ALGORITHM_V4_' || new.ref ||
'(key1 integer, key2 integer)
returns (ret numeric(16,2))
as
' || new.declval4 || '
begin
' || new.bodyval4 || '
  suspend;
end;';
    execute statement proc;
  end
  if (new.bodyval5 is not null and new.bodyval5 <> '' and new.declval5 is not null  $$IBEC$$*//*and new.declval5 <> ''*//*$$IBEC$$ ) then
  begin
    proc = 'create or alter procedure XX_FRD_ALGORITHM_V5_' || new.ref ||
'(key1 integer, key2 integer)
returns (ret numeric(16,2))
as
' || new.declval5 || '
begin
' || new.bodyval5 || '
  suspend;
end;';
    execute statement proc;
  end $$IBEC$$*/
end^
SET TERM ; ^
