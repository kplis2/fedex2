--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFDZIALATR_AU_REPLICAT FOR DEFDZIALATR                    
  ACTIVE AFTER UPDATE POSITION 3 
as
begin
  if(new.NAGART <> old.NAGART or (new.nagart is null and old.nagart is not null) or (new.nagart is not null and old.nagart is null) or
    (new.NAZWA <> old.NAZWA) or (new.NAZWA is null and old.NAZWA is not null) or (new.NAZWA is not null and old.NAZWA is null) or
    (new.numer <> old.numer and new.ord = 1) or (new.numer is null and old.numer is not null) or (new.numer is not null and old.numer is null) or
    (new.wartosc <> old.wartosc) or (new.wartosc is null and old.wartosc is not null) or (new.wartosc is not null and old.wartosc is null) or
    (new.typ <> old.typ) or (new.typ is null and old.typ is not null) or (new.typ is not null and old.typ is null) or
    (new.nltobr <> old.nltobr) or (new.nltobr is null and old.nltobr is not null) or (new.nltobr is not null and old.nltobr is null) or
    (new.dictfilter <> old.dictfilter) or (new.dictfilter is null and old.dictfilter is not null) or (new.dictfilter is not null and old.dictfilter is null)
  ) then
  begin
    if (new.dzial is not null) then
      update DZIALY set STATE=-2 where ref=new.dzial;
    if (new.klasa is not null) then
      update DEFKLASY set STATE=-2 where ref=new.klasa;
  end
end^
SET TERM ; ^
