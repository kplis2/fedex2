--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRMACHINES_BI_SYMBOL FOR PRMACHINES                     
  ACTIVE BEFORE INSERT POSITION 1 
as
begin
  if(new.symbol is null or (new.symbol = '')) then
    exception PRMACHINES_EXPT 'Symbol nie może być pusty.';
end^
SET TERM ; ^
