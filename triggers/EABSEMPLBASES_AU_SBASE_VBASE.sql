--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSEMPLBASES_AU_SBASE_VBASE FOR EABSEMPLBASES                  
  ACTIVE AFTER UPDATE POSITION 1 
as
declare variable absence integer;
declare variable payroll integer;
declare variable status smallint;
begin
--MWr Aktualizacja zakresu podstaw do rozliczenia nieoebecnosci

  if (new.period <> old.period or
      new.sbase <> old.sbase or (new.sbase is null and old.sbase is not null) or (new.sbase is not null and old.sbase is null) or
      new.vbase <> old.vbase or (new.vbase is null and old.vbase is not null) or (new.vbase is not null and old.vbase is null) or
      new.vworkeddays <> old.vworkeddays or (new.vworkeddays is null and old.vworkeddays is not null) or (new.vworkeddays is not null and old.vworkeddays is null) or
      new.sworkeddays <> old.sworkeddays or (new.sworkeddays is null and old.sworkeddays is not null) or (new.sworkeddays is not null and old.sworkeddays is null) or
      new.takeit <> old.takeit or (new.takeit is null and old.takeit is not null) or (new.takeit is not null and old.takeit is null) or
      new.workdays <> old.workdays or (new.workdays is null and old.workdays is not null) or (new.workdays is not null and old.workdays is null) or
      new.vworkedhours <> old.vworkedhours or (new.vworkedhours is null and old.vworkedhours is not null) or (new.vworkedhours is not null and old.vworkedhours is null)
  ) then begin

    for
      select distinct eabsence
        from eabsencebases b
        where b.employee = new.employee
          and b.period = new.period
      into :absence
    do begin

      status = 0;
      select epayroll,
        case when a.baseabsence = a.ref then 0 else 1 end  -->zmieniaj tylko dla nieciaglych
        from eabsences a
        where ref = :absence
        into :payroll, :status;

      if (payroll is not null and status = 0) then begin
        select status from epayrolls
          where ref = :payroll
          into :status;
      end

      if (status = 0) then begin
        update eabsencebases set
          sbase = new.sbase,
          vbase = new.vbase,
          vworkeddays = new.vworkeddays,
          sworkeddays = new.sworkeddays,
          takeit = new.takeit,
          workdays = new.workdays,
          vworkedhours = new.vworkedhours
        where 
          eabsence = :absence and
          period = new.period and
          (new.sbase <> old.sbase or (new.sbase is null and old.sbase is not null) or (new.sbase is not null and old.sbase is null) or
           new.vbase <> old.vbase or (new.vbase is null and old.vbase is not null) or (new.vbase is not null and old.vbase is null) or
           new.vworkeddays <> old.vworkeddays or (new.vworkeddays is null and old.vworkeddays is not null) or (new.vworkeddays is not null and old.vworkeddays is null) or
           new.sworkeddays <> old.sworkeddays or (new.sworkeddays is null and old.sworkeddays is not null) or (new.sworkeddays is not null and old.sworkeddays is null) or
           new.takeit <> old.takeit or (new.takeit is null and old.takeit is not null) or (new.takeit is not null and old.takeit is null) or
           new.workdays <> old.workdays or (new.workdays is null and old.workdays is not null) or (new.workdays is not null and old.workdays is null) or
           new.vworkedhours <> old.vworkedhours or (new.vworkedhours is null and old.vworkedhours is not null) or (new.vworkedhours is not null and old.vworkedhours is null));
      end
    end

    --przypisz zaktualizowana podstawe do nieobecnosci jezeli jej brakuje
    for
      select e.ref, coalesce(r.status,0)
        from eabsences e
          left join epayrolls r on (r.ref = e.epayroll)
        where e.employee = new.employee
          and e.fbase <= new.period
          and e.lbase >= new.period
          and e.baseabsence = e.ref   -->zmieniaj tylko dla nieciaglych
          and not exists (select first 1 1 from eabsencebases b
                            where b.eabsence = e.baseabsence
                              and b.period = new.period)

        into :absence, :status
    do begin
      if (status = 0) then
        insert into eabsencebases (eabsence, employee, period, sbase, takeit, vbase, workdays, vworkeddays, sworkeddays, sbasegross, vworkedhours)
        values (:absence, new.employee, new.period, new.sbase, new.takeit, new.vbase, new.workdays, new.vworkeddays, new.sworkeddays, new.sbasegross, new.vworkedhours);
    end

    --usun z rozliczenia nieobecnosci podstawe z nieaktualnym okresem
    if (new.period <> old.period) then
    begin
      for
        select e.ref, coalesce(r.status,0)
          from eabsences e
            left join epayrolls r on (r.ref = e.epayroll)
          where e.employee = new.employee
            and e.fbase <= old.period
            and e.lbase >= old.period
            and e.baseabsence = e.ref   -->zmieniaj tylko dla nieciaglych
        into :absence, :status
      do begin
        if (status = 0) then
          delete from eabsencebases
          where period = old.period
            and eabsence = :absence;
      end
    end
  end
end^
SET TERM ; ^
