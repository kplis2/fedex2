--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCPARAMS_BIU_REF FOR BKDOCPARAMS                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if (coalesce(new.ref,0) = 0) then
    execute procedure gen_ref('BKDOCPARAMS') returning_values new.ref;
end^
SET TERM ; ^
