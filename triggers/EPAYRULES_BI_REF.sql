--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPAYRULES_BI_REF FOR EPAYRULES                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('EPAYRULES') returning_values new.ref;
end^
SET TERM ; ^
