--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ERECCANDS_BI_PLUS FOR ERECCANDS                      
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  -- uzupeniam informacje o ostatnim użyciu kandydata
  update ecandidates set LASTRECRUIT =
     (select RECRUIT from erecposts where REF=new.recpost)
    where REF = new.candidate;
end^
SET TERM ; ^
