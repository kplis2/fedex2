--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CENNIKIZEWN_BU_REPLICAT FOR CENNIKIZEWN                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.REF<>old.REF
     or(new.ktm <> old.ktm ) or (new.ktm is null and old.ktm is not null) or (new.ktm is not null and old.ktm is null)
     or(new.dostawca <> old.dostawca) or (new.dostawca is null and old.dostawca is not null) or (new.dostawca is not null and old.dostawca is null)
     or(new.symbol <> old.symbol ) or (new.symbol is null and old.symbol is not null) or (new.symbol is not null and old.symbol is null)
     or(new.nazwa <> old.nazwa ) or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null)
     or(new.pkwiu <> old.pkwiu) or (new.pkwiu is null and old.pkwiu is not null) or (new.pkwiu is not null and old.pkwiu is null)
     or(new.vat <> old.vat ) or (new.vat is null and old.vat is not null) or (new.vat is not null and old.vat is null)
     or(new.miara <> old.miara ) or (new.miara is null and old.miara is not null) or (new.miara is not null and old.miara is null)
     or(new.kodkresk <> old.kodkresk ) or (new.kodkresk is null and old.kodkresk is not null) or (new.kodkresk is not null and old.kodkresk is null)
     or(new.waga <> old.waga ) or (new.waga is null and old.waga is not null) or (new.waga is not null and old.waga is null)
     or(new.objetosc <> old.objetosc ) or (new.objetosc is null and old.objetosc is not null) or (new.objetosc is not null and old.objetosc is null)
     or(new.cena_fabn <> old.cena_fabn ) or (new.cena_fabn is null and old.cena_fabn is not null) or (new.cena_fabn is not null and old.cena_fabn is null)
     or(new.rabat_fabn <> old.rabat_fabn ) or (new.rabat_fabn is null and old.rabat_fabn is not null) or (new.rabat_fabn is not null and old.rabat_fabn is null)
     or(new.cena_zakn <> old.cena_zakn ) or (new.cena_zakn is null and old.cena_zakn is not null) or (new.cena_zakn is not null and old.cena_zakn is null)
     or(new.opis <> old.opis ) or (new.opis is null and old.opis is not null) or (new.opis is not null and old.opis is null)
     ) then
   waschange = 1;

  execute procedure rp_trigger_bu('CENNIKIZEWN',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
