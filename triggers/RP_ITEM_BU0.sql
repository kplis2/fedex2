--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RP_ITEM_BU0 FOR RP_ITEM                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
    if(new.TYP = 0) then  new.NAME ='Definicja obiektu';
    else if(new.TYP = 1) then new.NAME = new.STABLE;
    else if(new.TYP = 2 or (new.TYP = 3)) then new.NAME = new.FIELD;
    if(new.DEPENDS <> old.DEPENDS) then
      if(new.DEPENDS > 0 and new.DEPENDS is not null) then
        select STABLE || '.' ||FIELD from RP_ITEM where REF_ID = new.DEPENDS into new.dependsname;
      else
         new.dependsname = '';
 if(new.ord = 1 and old.ord <> 2) then
  begin
    if(new.numer > old.numer) then
      new.ord = 3;
    else if (new.numer < old.numer) then
      new.ord = 0;
  end


end^
SET TERM ; ^
