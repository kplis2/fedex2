--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTURE_BI_REF FOR ACCSTRUCTURE                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ACCSTRUCTURE')
      returning_values new.REF;
end^
SET TERM ; ^
