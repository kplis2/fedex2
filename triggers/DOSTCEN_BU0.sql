--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTCEN_BU0 FOR DOSTCEN                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable cnt integer;
declare variable walpln varchar(10);
begin
  if((new.wersjaref <> old.wersjaref or (new.ktm is null) or (new.wersja is null)) and new.wersjaref is not null and new.wersjaref <> 0) then begin
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  end else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)or (new.wersjaref is null)) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if(new.oddzial is null or new.oddzial='') then begin
    execute procedure GETCONFIG('AKTUODDZIAL') returning_values new.oddzial;
  end
  if(new.cena_detal is null) then new.cena_detal = 0;
  if(new.cenanet is null ) then new.cenanet = 0;
  if(new.cena_fab is null) then new.cena_fab = 0;
  if(new.symbol_dost is null) then new.symbol_dost = '';
  if(new.cenawal is null) then new.cenawal = new.cenanet;
end^
SET TERM ; ^
