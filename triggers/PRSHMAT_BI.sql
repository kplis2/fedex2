--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHMAT_BI FOR PRSHMAT                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable reported integer;
BEGIN
  if (new.stopcascade < 0 or new.stopcascade is null) then new.stopcascade = 0;
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  else if(new.ktm is not null and new.wersja is not null) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if (new.opermaster is null or new.opermaster = 0)
    then exception prsheets_data 'Definiowanie surowca dozwolone wyłącznie dla operacji.';
  new.egrossquantity = replace(new.egrossquantity,',','.');
  new.enetquantity = replace(new.enetquantity,',','.');
  new.econdition = replace(new.econdition,',','.');
  if (new.autodocout is null) then new.autodocout = 0;
  if (new.autodocout > 0) then
  begin
    select o.reported
      from prshopers o
      where o.ref = new.opermaster
      into reported;
    if (reported is null) then reported = 0;
    if (reported = 0) then
      exception prsheets_data 'Autowydawanie surowców tylko dla operacji raportowanych';
  end
END^
SET TERM ; ^
