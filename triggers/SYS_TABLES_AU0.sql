--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SYS_TABLES_AU0 FOR SYS_TABLES                     
  ACTIVE AFTER UPDATE POSITION 0 
AS
  declare variable table_name varchar(31);
begin
  if ((new.dorder is not null and old.dorder is null) or new.dorder <> old.dorder) then
  begin
    for
      select t.table_name
        from sys_tablesfk fk
          join sys_tables t on (fk.ontable = t.table_name)
        where fk.table_name = new.table_name and fk.null_flag = 1 and (t.dorder is null or t.dorder <= new.dorder)
        into :table_name
    do
      update sys_tables set dorder = new.dorder + 1 where table_name = :table_name;
  end
end^
SET TERM ; ^
