--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHCALCS_BI0 FOR PRSHCALCS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if(new.calcdate is null) then new.calcdate = current_date;
  if(new.logchanges is null) then new.logchanges = 0;
  if(new.regoper is null) then execute procedure GET_GLOBAL_PARAM('AKTUOPERATOR') returning_values new.regoper;
end^
SET TERM ; ^
