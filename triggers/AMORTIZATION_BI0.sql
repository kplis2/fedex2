--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER AMORTIZATION_BI0 FOR AMORTIZATION                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  new.correction = coalesce(new.correction,0);

  select amyear, ammonth from amperiods
    where ref = new.amperiod
    into new.amyear, new.ammonth;

  if (new.tamort is not null) then
    update fxdassets set tredemptiongross = coalesce(tredemptiongross,0) + new.tamort where ref = new.fxdasset;

  if (new.famort is not null) then
    update fxdassets set fredemptiongross = coalesce(fredemptiongross,0) + new.famort where ref = new.fxdasset;
end^
SET TERM ; ^
