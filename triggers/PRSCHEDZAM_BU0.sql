--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDZAM_BU0 FOR PRSCHEDZAM                     
  ACTIVE BEFORE UPDATE POSITION 1 
AS
begin
  if(new.nazwat is null) then
    select wersje.nazwat
      from wersje join nagzam on (nagzam.kktm = wersje.ktm and nagzam.kwersja = wersje.nrwersji)
      where nagzam.ref = new.zamowienie
    into new.nazwat;
end^
SET TERM ; ^
