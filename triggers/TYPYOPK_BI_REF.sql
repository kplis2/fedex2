--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TYPYOPK_BI_REF FOR TYPYOPK                        
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('TYPYOPK')
      returning_values new.REF;
end^
SET TERM ; ^
