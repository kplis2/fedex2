--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TEST_PR88459_BI FOR TEST_PR88459                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_test_pr88459,1);
end^
SET TERM ; ^
