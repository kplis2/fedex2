--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDS_BU0 FOR MWSORDS                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable chronologia smallint;
declare variable numpoakcept smallint;
declare variable local smallint;
declare variable numberg varchar(40);
declare variable mwsordtypedest smallint;
declare variable mwsordtypes varchar(4);
begin
  -- data pierwszej akceptacji zlecenia
  if ((new.status = 1 or new.status = 2) and new.accepttime is null
      and old.status <> 1 and old.status <> 2) then
    new.accepttime = current_timestamp(0);
  execute procedure CHECK_LOCAL('MWSORDS',new.ref) returning_values :local;
  select chronologia, numpoakcept, number_mwsordgen
    from DEFMAGAZ where symbol=new.wh into :chronologia, :numpoakcept, :numberg;
  if (:chronologia is null) then chronologia = 0;
  if (:numpoakcept is null) then numpoakcept = 0;
  if (new.mwsordtype <> old.mwsordtype) then
  begin
    select symbol, mwsortypedets from mwsordtypes where ref = new.mwsordtype
      into mwsordtypes, mwsordtypedest;
    new.mwsordtypes = mwsordtypes;
    new.mwsordtypedest = mwsordtypedest;
  end
  -- kontrola chronologii
  if (:chronologia > 0 and (new.status = 1 and old.status <> 1)) then begin
     if (exists(select * from MWSORDS where WH=new.wh and REGTIME>=(new.regtime + :chronologia) and status > 0)) then
       exception MWSORDS_ACC_LATER;
  end
  if(:numberg is not null and :local = 1) then
  begin
    if(:numpoakcept = 1 and :numberg is not null) then begin
      if(new.status = 1 and old.status in (0,7)) then begin  -- XXX KBI Nadawanie sysmblu zleceniom po akceptacji
         execute procedure Get_NUMBER(:numberg, new.wh, new.mwsordtypes, new.regtime, new.numblock, null)
           returning_values new.number, new.symbol;
         new.numblock = null;
      end else if (new.status = 0 and old.status = 1) then begin
       -- zwolnienie numeratora
        if(old.number <> 0) then begin
            execute procedure free_number(:numberg, old.wh, old.mwsordtypes, old.regtime, old.number, new.numblockget) returning_values new.numblock;
        end
        if(new.numblock > 0) then
          update NUMBERBLOCK set OPERATOR = new.operator where ref=new.numblock;
       --nadanie numeratora tymczasowego
       new.number = -1;
       new.symbol = 'TYM/'||cast(new.ref as varchar(20));
      end
    end
  end
  --blokada na dokumnag - ustawianie statusu na 3
  if (new.doctype = 'M') then
  begin
    if (exists(select d.ref from dokumnag d where d.grupasped = new.docgroup and d.blokada in (1,5))
      and new.status = 1 and old.status = 0
    ) then begin
      new.status = 3;
      new.accepttime = null;
    end
  end
end^
SET TERM ; ^
