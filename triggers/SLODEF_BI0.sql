--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLODEF_BI0 FOR SLODEF                         
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable nazwawyr varchar(20);
declare variable isdistnew smallint;
declare variable distoldref integer;
begin
  if (new.typ is null or (new.typ = '')) then new.typ = 'ESYSTEM';
  if (new.mag is null) then new.mag = 0;
  if (new.kartoteka is null) then new.kartoteka = 1;
  if (new.analityka is null) then new.analityka = 0;
  if (new.isdist is null) then new.isdist = 0;
  if (new.multidist is null) then new.multidist = 0;
  if (new.iscompany is null) then new.iscompany = 0;
  if(new.analityka + new.kartoteka + new.isdist + new.produkcja = 0) then
    exception SLODEF_RODZAJNIEOKRESLONY;
  if(new.kartoteka = 0) then begin
    new.crm = 0;
    new.firma = 0;
    new.mag = 0;
    new.opak = 0;
  end
  if(new.mag > 0) then begin
    if(new.opak is null) then new.opak = 0;
  end else
    new.opak = 0;
  nazwawyr = '';
  select REF, coalesce(nazwa,'') from slodef
    where isdist = new.isdist
       and coalesce(new.isdist,0)<>0
       and ((slodef.company is null) or (slodef.company = new.company))
       and ref <> new.ref
       into :distoldref, :nazwawyr;
  if(:distoldref > 0) then begin
    select max(isdist) from slodef
       where ((slodef.company is null) or (slodef.company = new.company))
       into :isdistnew;
       isdistnew = coalesce(isdistnew,0) + 1;
       exception tomany_dists 'Numer wyróżnika wykorzystywany w słowniku ' || :nazwawyr||'.Kolejny wolny nr '||:isdistnew;
  end
  if (coalesce(new.iscompany,0) = 1 and new.typ <> 'PERSONS') then begin
    if (not exists(select first 1 1 from rdb$relation_fields
                  where rdb$field_name = 'COMPANY' and rdb$relation_name = new.typ) or (new.typ = 'ESYSTEM') ) then
      exception NO_COMPANY_FIELD;
  end
end^
SET TERM ; ^
