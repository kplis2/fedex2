--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDOPERS_AU0 FOR PRSCHEDOPERS                   
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable prschedoper integer;
declare variable amountin numeric(14,4);
declare variable shoperdesc varchar(255);
declare variable opertype smallint;
declare variable shortages numeric(14,4) ;
begin
--zmiana statusów operacji zależnych
  if((new.status <> old.status or old.activ <> new.activ) and new.calcdeptimes = 1) then begin
    execute procedure prschedopers_statuscheck(new.schedzam, new.guide, new.ref);
  end
--zmiana statusu przewodnika
  if(new.status = 3 and new.status <> old.status and
    not exists (select ref from prschedopers where guide = new.guide and status < 3)
  ) then
    execute procedure prschedguides_statuschange(new.guide,2,null);
  else if(new.status < 3 and old.status = 3 and new.reported > 0 and
    exists(select ref from prschedguides where ref = new.guide and status = 2))
  then execute procedure prschedguides_statuschange(new.guide,1,null);
--wyliczenie ilosci na operacjach zależnych
  if(new.amountresult <> old.amountresult) then begin
    for
      select psod.depto
        from prschedoperdeps psod
          left join prschedopers pso on (pso.ref = psod.depto)
        where psod.depfrom = new.ref
        into :prschedoper
    do begin
      execute procedure prschedoper_amountin(:prschedoper);
    end
  end
--przy ustawieniu operacji alternatywnej jako aktywnej
  if(new.activ = 1 and old.activ = 0 and ( new.altverified is null or new.altverified = 0)) then begin
    execute procedure PRSCHEDOPER_ALT_SET(new.guide, new.shoper, -1, null);
    update prschedopers set altverified = 0 where guide = new.guide and altverified = 1;
  end
--badanie czy można zmienić raportowanie
  if(old.reported > 0 and new.reported = 0 and
    (exists(select ref from propersraps where prschedoper = new.ref and amount > 0)
     or exists(select ref from prshortages where prschedoper = new.ref and amount > 0))
  ) then begin
    select descript from prshopers where ref = new.shoper into :shoperdesc;
    exception prschedopers_error substring('Operacja '''||shoperdesc||''' ma zaewidencjonowany raport' from 1 for 75);
  end
  if (new.activ <> old.activ) then
    update prschedguidespos set activ = new.activ where prschedguide = new.guide and prschedoper = new.ref;
  -- sprawdzenie czy ilosc braków jest zgodna z raporami braków
  if (old.status <> new.status and new.status = 3 and new.reported > 0) then
  begin
    select coalesce(sum (amount),0)
      from prshortages ps
        left join prshortagestypes st on(st.symbol = ps.prshortagestype)
      where ps.prschedoper = new.ref
        and coalescE(ps.noamountinfluence,st.noamountinfluence) = 0
      into :shortages;

    if (coalesce(new.amountshortages,0) <> :shortages) then
      exception prschedopers_error 'PRSCHEDOPERS_AU0: Ilosć braków niezgodna z raportami' ;
  end 
end^
SET TERM ; ^
