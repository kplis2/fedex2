--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OFERTY_BU_EKSPORT FOR OFERTY                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable vat numeric(14,2);
declare variable ref integer;
begin
  if (new.eksport <> old.eksport) then
  begin
    for select ref, stawka from oferpoz op
        left join vat on op.gr_vat = vat.grupa
        where op.oferta= new.ref
        into :ref , :vat
    do begin
      if (new.eksport = 0) then
        update oferpoz set cenabru = cenanet + cast((cenanet * (:vat / 100)) as DECIMAL(14,2)),
                           wartbru = wartnet + cast((wartnet * (:vat / 100)) as DECIMAL(14,2)),
                           vat = cast((wartnet * (:vat / 100)) as DECIMAL(14,2))
        where ref = :ref;
      else
        update oferpoz set cenabru = cenanet, wartbru = wartnet, vat = 0
        where ref = :ref;
    end
  end
end^
SET TERM ; ^
