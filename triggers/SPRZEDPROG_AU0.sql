--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDPROG_AU0 FOR SPRZEDPROG                     
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if((new.sprzedawca <> old.sprzedawca)
   or (new.wart_min <> old.wart_min)
   or (new.procent <> old.procent) or (new.procent is null and old.procent is not null) or (new.procent is not null and old.procent is null)
   or (new.prowizja <> old.prowizja ) or (new.prowizja is null and old.prowizja  is not null) or (new.prowizja is not null and old.prowizja is null)
  )then begin
    update SPRZEDAWCY set STATE=-1 where REF=new.sprzedawca;
    if(new.sprzedawca <> old.sprzedawca) then
      update SPRZEDAWCY set STATE= -1 where ref = old.sprzedawca;
  end
end^
SET TERM ; ^
