--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRACTSASSIGN_BI_REF FOR ECONTRACTSASSIGN               
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ECONTRACTSASSIGN')
      returning_values new.REF;
end^
SET TERM ; ^
