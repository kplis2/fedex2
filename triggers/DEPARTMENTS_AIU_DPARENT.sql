--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEPARTMENTS_AIU_DPARENT FOR DEPARTMENTS                    
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
as
declare variable tmpsymbol varchar(40);
declare variable newdepartmentlist varchar(255);
declare variable olddepartmentlist varchar(255);
begin
  if(inserting or (updating and coalesce(new.dparent,0)<>coalesce(old.dparent,0))) then begin
    for select symbol,departmentlist from departments
    into :tmpsymbol,:olddepartmentlist
    do begin
      if(:olddepartmentlist is null) then olddepartmentlist = '';
      execute procedure GET_DEPARTMENTS(:tmpsymbol) returning_values :newdepartmentlist;
      if(:newdepartmentlist<>:olddepartmentlist) then begin
        update departments set departmentlist = :newdepartmentlist where symbol = :tmpsymbol;
        update employees set departmentlist = :newdepartmentlist where department = :tmpsymbol;
        update emplcontracts set departmentlist = :newdepartmentlist where department = :tmpsymbol;
      end
    end
  end
end^
SET TERM ; ^
