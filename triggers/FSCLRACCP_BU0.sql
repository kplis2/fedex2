--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FSCLRACCP_BU0 FOR FSCLRACCP                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if ((new.currdebit2c<>old.currdebit2c or new.currcredit2c<>old.currcredit2c)
      and new.autochange=0) then
    new.autopos = 0;
end^
SET TERM ; ^
