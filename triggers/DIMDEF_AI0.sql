--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DIMDEF_AI0 FOR DIMDEF                         
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  insert into dimhierdef (dim, name, descript)
    values (new.ref, new.shortname, new.name);
end^
SET TERM ; ^
