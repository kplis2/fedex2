--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WERSJE_BI_ORDER FOR WERSJE                         
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable maxnum integer;
begin
  new.ord = 1;
  select max(nrwersji) from wersje where ktm = new.ktm
    into :maxnum;
  if (new.nrwersji is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.nrwersji = maxnum + 1;
/*  end else if (new.nrwersji <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update wersje set ord = 0, nrwersji = nrwersji + 1
      where nrwersji >= new.nrwersji and ktm = new.ktm;     */
  end else if (new.nrwersji > maxnum) then
    new.nrwersji = maxnum + 1;
end^
SET TERM ; ^
