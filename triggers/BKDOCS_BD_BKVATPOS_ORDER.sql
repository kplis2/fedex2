--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_BD_BKVATPOS_ORDER FOR BKDOCS                         
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  update bkvatpos set ord = 0 where bkdoc = old.ref;
end^
SET TERM ; ^
