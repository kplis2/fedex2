--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKPLIK_BD0 FOR DOKPLIK                        
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if(exists(select first 1 ref from doklink where dokplik = old.ref and coalesce(nowy,0)=0)) then
    exception dokplik_error 'Do fiszki podłączony jest dokument. Usunięcie zablokowane';
  else
    delete from doklink where dokplik=old.ref;
  update dokpliksegregatory d set d.segdefault = 0 where d.dokplik = old.ref and d.segdefault = 1;
end^
SET TERM ; ^
