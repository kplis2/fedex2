--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER GRUPYKLI_AU0 FOR GRUPYKLI                       
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable il integer;
declare variable aktuoddzial varchar(100);
begin
  if(old.cennik <> new.cennik ) then begin
    if(old.cennik is not null) then begin
      select count(*) from GRUPYKLI where cennik = old.cennik into :il;
      if(:il is null) then il = 0;
      if(:il = 0) THEN begin
        execute procedure GETCONFIG('AKTUODDZIAL') returning_values :aktuoddzial;
        UPDATE defcennik set akt = 0 where akt <> 0 and ref = old.cennik and ((ODDZIAL=:aktuoddzial) or (ODDZIAL='') or (ODDZIAL is NULL));
      end
    end
    if(new.cennik is not null) then begin
       execute procedure GETCONFIG('AKTUODDZIAL') returning_values :aktuoddzial;
       update defcennik set akt = 1 where akt <> 1 and ref  = new.cennik and ((ODDZIAL=:aktuoddzial) or (ODDZIAL='') or (ODDZIAL is NULL));
    end
  end
  if(new.opis<>old.opis) then update klienci set grupanazwa=new.opis where grupa=new.ref;
end^
SET TERM ; ^
