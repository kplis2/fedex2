--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRCALCCOLS_BI_COLOR FOR PRCALCCOLS                     
  ACTIVE BEFORE INSERT POSITION 2 
as
begin
  if (new.color is not null) then
    exit; -- kolor jest ustawiony z definicji kalkulacji
  if(new.parent is null and new.calctype=0) then new.color = 3;
  else if(new.excluded=1) then new.color = 2;
  else new.color = new.calctype;
  if(new.fromprshmat is not null) then new.icon = 'MI_SUROWIEC';
  else if(new.fromprshoper is not null) then new.icon = 'MI_OPERACJA';
  else if(new.fromprshtool is not null) then new.icon = 'MI_NARZEDZIA';
  else if(new.fromprcalccol is not null) then new.icon = 'MI_TOOLKALK';
  else if(new.color=3) then new.icon = 'MI_TOOLKALK';
  else if(new.calctype=0) then new.icon = 'MI_CENNIKI';
end^
SET TERM ; ^
