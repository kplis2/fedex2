--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFKATAL_BU_REPLICAT FOR DEFKATAL                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.REF<>old.REF
      or new.nazwa <> old.nazwa or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null)
      or new.cecha <> old.cecha or (new.cecha is null and old.cecha is not null) or (new.cecha is not null and old.cecha is null)
      or new.typ <> old.typ or (new.typ is null and old.typ is not null) or (new.typ is not null and old.typ is null)
      or new.ukrywanie <> old.ukrywanie or (new.ukrywanie is null and old.ukrywanie is not null) or (new.ukrywanie is not null and old.ukrywanie is null)
      or new.automat <> old.automat or (new.automat is null and old.automat is not null) or (new.automat is not null and old.automat is null)
      or new.wyszuk <> old.wyszuk or (new.wyszuk is null and old.wyszuk is not null) or (new.wyszuk is not null and old.wyszuk is null)
      ) then
   waschange = 1;

  execute procedure rp_trigger_bu('DEFKATAL',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
