--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CDVERSIONS_BI_REF FOR CDVERSIONS                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
    if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CDVERSIONS')
      returning_values new.REF;
end^
SET TERM ; ^
