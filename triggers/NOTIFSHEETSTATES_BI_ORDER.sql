--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTIFSHEETSTATES_BI_ORDER FOR NOTIFSHEETSTATES               
  ACTIVE BEFORE INSERT POSITION 20 
as
declare variable maxnr integer;
begin
  select max(n.number)
    from notifsheetstates n
    into :maxnr;
  if (:maxnr is null) then maxnr = 0;

  if (coalesce(new.number,0) <= 0 or new.number > :maxnr + 1) then
    new.number = :maxnr + 1;
end^
SET TERM ; ^
