--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHOPERSWORKPLACES_AI0 FOR PRSHOPERSWORKPLACES            
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable multiworkplaces smallint;
begin
    if(new.main = 1 ) then begin
      update prshopersworkplaces p
        set p.main = 0
      where p.prshoper = new.prshoper
      and p.ref <> new.ref;

    if(coalesce(new.opertimeupdate,0) <> 0)then
    begin
      update prshopers p set p.eopertime = case when new.prshoper_clone = 1 then p.eopertime else cast(new.opertime as varchar(20)) end ,
        p.opertimepf = coalesce(new.opertimepf, 0),
        p.workplace = new.prworkplace,
        p.prmachine = new.prmachine,
        p.opertimeupdate = 1
      where p.ref = new.prshoper;

      update prshopers p set p.eopertime = case when new.prshoper_clone = 1 then p.eopertime else cast(new.opertime as varchar(20)) end  ,
        p.opertimeupdate = 0
      where p.ref = new.prshoper;
    end
  end
end^
SET TERM ; ^
