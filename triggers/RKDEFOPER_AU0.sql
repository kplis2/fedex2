--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDEFOPER_AU0 FOR RKDEFOPER                      
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable slotyp varchar(15);
begin
  if(new.slodef is null and new.slowymog = 0) then begin
   update RKPOZOPER set TYP = 0 where TYP > 0 and operacja = new.symbol;
  end else if(new.slodef is null and new.slowymog = 1) then begin
   update RKPOZOPER set TYp = 1 where TYP > 1 and operacja = new.symbol;
  end
  else if (new.slodef <> old.slodef or (new.slodef is not null and old.slodef is null)) then begin
    select TYP from SLODEF where REF=new.slodef into :slotyp;
    if(:slotyp <> 'KLIENCI') then
      update RKPOZOPER set TYP = 1 where TYP > 1 and OPERACJA = new.symbol;
  end
end^
SET TERM ; ^
