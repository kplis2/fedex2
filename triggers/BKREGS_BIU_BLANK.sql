--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKREGS_BIU_BLANK FOR BKREGS                         
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.rights is null) then new.rights = '';
  if (new.rightsgroup is null) then new.rightsgroup = '';
  if (new.symbol = '') then new.symbol = null;
  if (new.numgapfilling is null) then new.numgapfilling = 0;
end^
SET TERM ; ^
