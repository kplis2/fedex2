--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACHSTAN_BU0 FOR ROZRACHSTAN                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if(new.winien is null) then new.winien = 0;
  if(new.ma is null) then new.ma = 0;
  if(new.winienzl is null) then new.winienzl = 0;
  if(new.mazl is null) then new.mazl = 0;
  if(new.winienprz is null) then new.winienprz = 0;
  if(new.maprz is null) then new.maprz = 0;
  if(new.winienprzzl is null) then new.winienprzzl = 0;
  if(new.maprzzl is null) then new.maprzzl = 0;

  new.saldo = new.winien - new.ma;
  new.saldozl = new.winienzl - new.mazl;
  new.saldoprz = new.winienprz - new.maprz;
  new.saldoprzzl = new.winienprzzl - new.maprzzl;
end^
SET TERM ; ^
