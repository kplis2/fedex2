--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWJEDN_AU_REPLICAT FOR TOWJEDN                        
  ACTIVE AFTER UPDATE POSITION 1 
as
begin
  if((new.glowna <> old.glowna)
   or (new.ktm <> old.ktm)
   or (new.s <> old.s)
   or (new.z <> old.z)
   or (new.m <> old.m)
   or (new.c <> old.c)
   or (new.jedn <> old.jedn)
   or (new.przelicz <> old.przelicz)
  ) then begin
    update TOWARY set STATE = -1 where KTM = new.ktm;
    if(new.ktm <> old.ktm) then
      update TOWARY set STATE = -1 where ktm = old.ktm;
  end

end^
SET TERM ; ^
