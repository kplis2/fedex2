--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZAPYTPARAM_BI_ORDER FOR ZAPYTPARAM                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable maxnum integer;
begin
  new.ord = 1;
  select max(numer) from zapytparam where zapytanie = new.zapytanie
    into :maxnum;
  if (new.numer is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.numer = maxnum + 1;
  end else if (new.numer <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update zapytparam set ord = 0, numer = numer + 1
      where numer >= new.numer and zapytanie = new.zapytanie;
  end else if (new.numer > maxnum) then
    new.numer = maxnum + 1;
end^
SET TERM ; ^
