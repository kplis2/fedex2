--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFPRZED_AI0 FOR DEFPRZED                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable refi integer;
declare variable i integer;
declare variable num integer;
declare variable wart varchar(70);
declare variable katalog integer;
begin
  i = 0;
  num = 0;
  for select wart_min from DEFPRZED where cecha = new.cecha into :wart do begin
     i = i+1;
     if(wart = new.wart_min)then num = i;
  end
  for select ref from DEFKATAL where cecha = new.cecha into :katalog do begin
     for select ref from DEFMENU where KATALOG = :katalog and ((ELEM is NULL) or (elem = 0) ) and ((przed is null) or (przed = 0))
       into :refi
     do begin
        insert into DEFMENU(POZIOM,NUMER,KATALOG,PRZED,NAZWA,PIKTOGRAM)
            values(:refi,:num,:katalog,new.ref,new.opis,1);
     end
  end
end^
SET TERM ; ^
