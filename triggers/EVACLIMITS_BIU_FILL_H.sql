--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACLIMITS_BIU_FILL_H FOR EVACLIMITS                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 10 
AS
begin
--Uzupelninie infromacji o liczbie godzin poszczegolnych rodzajow urlopu

  if (new.actlimit <> coalesce(old.actlimit,0) and new.actlimit_cust = 0) then
    execute procedure EminsToHoursStr(new.actlimit) returning_values new.actlimith;

  if (new.addlimit <> coalesce(old.addlimit,0) and new.addlimit_cust = 0) then
    execute procedure EminsToHoursStr(new.addlimit) returning_values new.addlimith;

  if (new.disablelimit <> coalesce(old.disablelimit,0) and new.disablelimit_cust = 0) then
    execute procedure EminsToHoursStr(new.disablelimit) returning_values new.disablelimith;

  if (new.traininglimit <> coalesce(old.traininglimit,0) and new.traininglimit_cust = 0) then
    execute procedure EminsToHoursStr(new.traininglimit) returning_values new.traininglimith;

  if (new.restlimit <> coalesce(old.restlimit,0)) then
    execute procedure EminsToHoursStr(new.restlimit) returning_values new.restlimith;

  if (new.prevlimit <> coalesce(old.prevlimit,0)) then
    execute procedure EminsToHoursStr(new.prevlimit) returning_values new.prevlimith;

  if (new.limitcons <> coalesce(old.limitcons,0)) then
    execute procedure EminsToHoursStr(new.limitcons) returning_values new.limitconsh;

  if (new.otherlimit <> coalesce(old.otherlimit,0)) then
    execute procedure EminsToHoursStr(new.otherlimit) returning_values new.otherlimith;

  if (new.supplementallimit <> coalesce(old.supplementallimit,0)) then
    execute procedure EminsToHoursStr(new.supplementallimit) returning_values new.supplementallimith;

end^
SET TERM ; ^
