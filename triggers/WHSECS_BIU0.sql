--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHSECS_BIU0 FOR WHSECS                         
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.deliveryarea is null) then new.deliveryarea = 0;
  if (new.maxemptypalq is null) then new.maxemptypalq = 0;
  if (new.minemptypalq is null) then new.minemptypalq = 0;
  if (new.autogoodsrefill is null) then new.autogoodsrefill = 0;
  if (new.automove is null) then new.automove = 0;
end^
SET TERM ; ^
