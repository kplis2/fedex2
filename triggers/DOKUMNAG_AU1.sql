--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_AU1 FOR DOKUMNAG                       
  ACTIVE AFTER UPDATE POSITION 1 
as
declare variable local smallint;
declare variable stankas char(2);
declare variable operkas char(8) ;
declare variable pozkas integer;
declare variable rapkas integer;
declare variable rkdla varchar(255) ;
declare variable rkdlaskrot shortname_id; --XXX ZG133796 MKD
declare variable rkdoknag integer;
declare variable rkdokpoz integer;
declare variable rknumer integer;
declare variable rkstanowisko varchar(3);
declare variable rktyp varchar(3);
declare variable rkusun varchar(20);
declare variable rkanulowano integer;
declare variable autocreaterkrap smallint;
declare variable slodef integer;
declare variable slopoz integer;
declare variable status integer;
declare variable cnt integer;
declare variable pozlistawys integer;
declare variable transport integer;
declare variable dostawa integer;
declare variable ref integer;
declare variable refdokumnag integer;
declare variable dokumsped integer;
declare variable pozref integer;
declare variable prschedguidedet integer;
declare variable akc smallint;
declare variable skad smallint;
begin
 execute procedure CHECK_LOCAL('DOKUMNAG',new.ref) returning_values :local;
 if((new.akcept = 1) and (old.akcept <> 1) and (:local = 1)) then begin
   if(new.kwotazal > 0) then begin
     stankas = new.rkstnkas;
     select DEFDOKUMMAG.rkdefoper, defdokummag.rkpozoper
     from DEFDOKUMMAG where MAGAZYN = new.magazyn and TYP = new.typ
     into :operkas, :pozkas;
     if((:stankas <> '') and (:operkas <> '') and (:pozkas > 0)) then begin
        select min(REF) from RKRAPKAS where  STANOWISKO = :stankas and DATAOD <= new.data and DATADO >= new.data
          and status = 0 into :rapkas;
        if(:rapkas is null or (:rapkas = 0))then begin
          select AUTORAPKASCREATE from STANSPRZED where STANSPRZED.stanowisko = new.stansprzed into :autocreaterkrap;
          if(:autocreaterkrap > 0) then begin
            insert into RKRAPKAS(STANOWISKO,DATAOD,DATADO) values(:stankas, new.data, new.data);
              select min(REF) from RKRAPKAS where  STANOWISKO = :stankas and DATAOD <= new.data and DATADO >= new.data
              and status = 0 into :rapkas;
          end
        end
        if(:rapkas > 0) then begin
            rkdoknag = null;
            if(new.klient  > 0) then begin
               select ref from SLODEF where TYP = 'KLIENCI' into :slodef;
                 slopoz = new.klient;
                 select NAZWA,FSKROT from KLIENCI where REF=:slopoz into :rkdla, :rkdlaskrot;
                 if(:rkdla ='' or (:rkdla is null)) then exception NAGFAK_KLIBEZNAZWY;
            end else if (new.dostawca > 0) then begin
                 select ref from SLODEF where TYP = 'DOSTAWCY' into :slodef;
                 slopoz = new.dostawca;
                 select NAZWA,id from DOSTAWCY where REF=:slopoz into :rkdla,:rkdlaskrot;
                 if(:rkdla ='' or (:rkdla is null)) then exception NAGFAK_DOSTBEZNAZWY;
            end else if(new.slopoz > 0 and new.slodef > 0) then begin
                 slodef = new.slodef;
                 slopoz = new.slopoz;
                 select SLOPOZ.nazwa, SLOPOZ.kod from SLOPOZ where slopoz.slownik = :slodef and ref = :slopoz into :rkdla, :rkdlaskrot;
            end
            if(:rkdoknag is null) then begin
              execute procedure GEN_REF('RKDOKNAG') returning_values :rkdoknag;
              /*naglowej dokumentu kasowego*/
              insert into RKDOKNAG(REF, RAPORT, DATA, OPERACJA, SLODEF, SLOPOZ,
                OPIS,KURS, WALUTOWY,DLA,DLASKROT)
              values(:rkdoknag, :rapkas, new.data, :operkas, :slodef, :slopoz,
                'Zaliczka z systemu magazynowego',1,0,:rkdla, :rkdlaskrot);
            end
            /*pozycja dokumentu kasowego*/
            execute procedure GEN_REF('RKDOKPOZ') returning_values :rkdokpoz;
            insert into RKDOKPOZ(REF,DOKUMENT,ROZRACHUNEK,FAKTURA,DOKUMNAG,KWOTA, KWOTAZL,POZOPER)
               values(:rkdokpoz, :rkdoknag, new.symbol, NULL,new.ref,new.kwotazal, new.kwotazal, :pozkas);
            execute procedure RK_WYSTAW_DOKUMENT(:rkdoknag) returning_values :status;
            update DOKUMNAG set RKDOKNAG = :rkdoknag where REF=new.ref;
        end
            else exception RKRAPKAS_NORAPORT;
     end
   end
   /*dopisanie do listy wysylkowej*/
   if(new.wysylka > 0) then begin
     select TRANSPORTAUTO from DEFDOKUM where SYMBOL = new.typ into :transport;
     if(:transport = 1) then
       execute procedure listywys_addpoz(NULL, new.wysylka, new.ref, 'M') returning_values :pozlistawys;
   end
   if(old.akcept >= 9 and new.akcept = 1 and (new.wysylka is null or (new.wysylka = 0))) then begin
     select TRANSPORT from DEFDOKUM where SYMBOL = new.typ into :transport;
     if(:transport = 1) then begin
       select max(LISTYWYSD.ref) from LISTYWYSDPOZ
         join DOKUMPOZ on (LISTYWYSDPOZ.dokpoz = DOKUMPOZ.REF)
         join LISTYWYSD on (LISTYWYSDPOZ.DOKUMENT = LISTYWYSD.REF)
         where LISTYWYSD.akcept = 0 and DOKUMPOZ.DOKUMENT = new.ref
         into :dokumsped;
       if(:dokumsped > 0) then begin
         execute procedure listywys_addpoz(:dokumsped, new.wysylka, new.ref, 'M') returning_values :pozlistawys;
       end
     end
   end
 end
 else
 if((new.akcept <> 1) and (old.akcept = 1) and (:local = 1))then begin
     /* wycofanie dostaw zalozonych automatycznie */
     if(new.akcept=0 and new.wydania = 0 and new.koryg = 0 and new.zewn = 0) then --zmiany dotycza zgloszenia BS60020
     begin
       if(new.dostawa is not null) then begin
         update dokumnag set dostawa = null where ref=new.ref;
       end
       for select ref,dostawa from dokumpoz where dokument=new.ref into :ref,:dostawa
       do begin
         if(:dostawa is not null) then
         begin
           update dokumpoz set dostawa = null where ref=:ref;
         end
       end
       /* Usuwanie rekordów z DOSTAWY jeżeli nie istnieją rozpiski ani żadne wskazania na dostawe */
       if ((new.dostawa is not null)
            and (not exists(select first 1 1 from dokumroz where dostawa = new.dostawa))
            and (not exists(select first 1 1 from dokumpoz where dostawa = new.dostawa))
            and (not exists(select first 1 1 from dokumnag where dostawa = new.dostawa))
            and (not exists(select first 1 1 from pozzam where dostawamag = new.dostawa))) then
         delete from dostawy where ref = new.dostawa;
     end
   /* wycofanie dokumentu kasowego */
   if(new.rkdoknag > 0) then begin
     rkdoknag = new.rkdoknag;
     execute procedure GETCONFIG('KASSPRZUSU') returning_values :rkusun;
     select ANULOWANY, NUMER, STANOWISKO,TYP from RKDOKNAG where REF=new.rkdoknag into :rkanulowano, :rknumer, :rkstanowisko, :rktyp;
     if(:rkanulowano = 0) then begin
       if(:rkusun = '1') then begin
         if(:rknumer > 0) then begin
            cnt = null;
            select count(*) from RKDOKNAG where STANOWISKO = :rkstanowisko and TYP=:rktyp and NUMER > :rknumer into :cnt;
            if(:cnt > 0) then begin
              rkusun = '';
            end
         end
       end else if(:rkusun = '2') then
         rkusun = '1';
       else rkusun = '';
       if(:rkusun <>'')then begin
          execute procedure GETCONFIG('KASUSUNLAST') returning_values :rkusun;
          if(:rkusun = '1') then begin
             /*sprawdzenie, czy usuwac, czy anulowac jednak*/
             cnt = null;
             select count(*) from RKDOKNAG where STANOWISKO = :rkstanowisko and TYP=:rktyp and NUMER > :rknumer into :cnt;
             if(:cnt > 0) then rkusun = '';
          end
          if(:rkusun ='1' and :rknumer > 0) then begin
            update RKDOKNAG set NUMER = 0 where REF=new.rkdoknag;
            delete from  RKDOKNAG where REF=new.rkdoknag;
            rkdoknag = null;
          end else
            update RKDOKNAG set ANULOWANY=1, ANULOPIS='Anulowanie dok. magazynowego' where REF=new.rkdoknag;
       end else
            exception NAGFA_ACK_SARKDOKNAG;
     end
     if(:rkdoknag is null or :rkanulowano = 1) then
          update DOKUMNAG set RKDOKNAG = null, KWOTAZAL = 0 where RKDOKNAG is not null and REF=new.ref;
   end
   /*wycofanie z listy wysylkowej*/
   cnt = null;
   select count(*) from LISTYWYSD
     join LISTYWYSDPOZ on (LISTYWYSDPOZ.dokument = listywysd.ref)
     join DOKUMPOZ on (DOKUMPOZ.REF = LISTYWYSDPOZ.dokpoz)
     where DOKUMPOZ.DOKUMENT = new.ref and LISTYWYSD.akcept > 0
     into :cnt;
   if(:cnt > 0) then exception DOKUMNAG_MADOKSPEDACK;
   if(new.akcept = 0) then begin
     delete from LISTYWYSDPOZ where DOKTYP = 'M' and DOKREF = new.ref;
     delete from LISTYWYSD where REFDOK = new.ref;
   end
   /* wycofanie znacznika rozliczenia POZFAKUSL */
   update POZFAKUSL set ROZLICZONO=0 where ROZLICZONO=2 and REFDOKUMNAG=new.ref and REFNAGFAK is NULL;
 end
 if(new.blokadafak<> old.blokadafak and new.zamowienie > 0) then
   execute procedure nagzam_hasdokmagbezfak(new.zamowienie);
--naliczenie realizacji przewodnikow produkcyjnych
 if(new.skad >= 3 and new.akcept <> old.akcept) then
 begin
   for
     select p.ref, p.prschedguidedet
       from dokumpoz p
       where p.dokument = new.ref
       into pozref, prschedguidedet
   do begin
     execute procedure PRSCHEDGUIDEDET_OBL('DOKUMNAG',new.skad,:pozref,new.akcept);
   end
 end
 if (new.koryg = 1 and new.akcept <> old.akcept) then
 begin
   for
     select k.ref, n.skad, n.akcept
       from dokumpoz p
         left join dokumpoz k on (k.ref = p.kortopoz)
         left join dokumnag n on (n.ref = k.dokument)
       where p.dokument = new.ref and n.skad in (3,4)
       into pozref, skad, akc
   do begin
     execute procedure PRSCHEDGUIDEDET_OBL('DOKUMNAG',:skad,:pozref,:akc);
   end
 end
 if (new.akcept <> old.akcept and local = 1 and new.mwsdoc = 1) then
   execute procedure xk_mws_recalc_sellav_for_doc(new.ref,new.magazyn);
end^
SET TERM ; ^
