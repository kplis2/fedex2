--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSUPCLR_BIU0 FOR MWSSUPCLR                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.supfrom is null and new.docidfrom is not null and new.docidfrom > 0 and new.quantityfrom > 0) then
    select d.id, d.ref
      from dokumnag dn
        left join dostawcy d on (dn.dostawca = d.ref)
      where dn.ref = new.docidfrom
      into new.dictsupfrom, new.supfrom;
  if (new.supfrom is null and new.docidfrom is not null and new.docidfrom > 0 and new.quantityfrom = 0 and new.quantityto > 0) then
    select d.id, d.ref
      from dokumnag dn
        left join dostawcy d on (dn.dostawca = d.ref)
      where dn.ref = new.docidfrom
      into new.dictsupfrom, new.supfrom;
  if (new.supto is null and new.docidto is not null and new.docidto > 0 and new.quantityto > 0) then
    select d.id, d.ref
      from dokumnag dn
        left join dostawcy d on (dn.dostawca = d.ref)
      where dn.ref = new.docidto
      into new.dictsupto, new.supto;
end^
SET TERM ; ^
