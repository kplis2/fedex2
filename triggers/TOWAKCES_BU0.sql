--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWAKCES_BU0 FOR TOWAKCES                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if((new.awersjaref <> old.awersjaref or (new.aktm is null) or (new.awersja is null)) and new.awersjaref is not null and new.awersjaref <> 0) then begin
    select nrwersji, ktm from WERSJE where ref=new.awersjaref into new.awersja, new.aktm;
  end else if((new.aktm <> old.aktm) or (new.awersja <> old.awersja) or (new.awersjaref <> old.awersjaref)or (new.awersjaref is null)) then
    select ref from WERSJE where ktm = new.aktm and nrwersji = new.awersja into new.awersjaref;
  if (new.typ = '') then
    new.typ = null;
end^
SET TERM ; ^
