--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWRKCRTLIMITS_BIU_HOUR FOR EWRKCRTLIMITS                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.hourlimit is null and new.daylimit is not null
      and exists(select number from ecolumns where number = new.ecolumn and typ containing ';URL;')) then
    new.hourlimit = new.daylimit * 8;
end^
SET TERM ; ^
