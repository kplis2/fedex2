--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRPOS_BU_CHANGES FOR EPRPOS                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
/*MWr: Uzupelnienie informacji o czasie modyfikacji. Informacja, kto modyfikowal,
  jest dostarczana poprzez interfejs (akcja na 'Popraw' w skladnikach placowych).
  Rozwiazanie wspiera mechaznim niezmiennosci dla modyfikowanych wartosci
  skladnikow placowych - PR31748*/

  if (new.cauto = 0) then
  begin
    if (old.chgoperator is not null and new.chgtimestamp is null and new.ecolumn = old.ecolumn and new.pvalue = old.pvalue) then
      new.chgoperator = null;

    if (new.chgoperator is null) then
      new.chgtimestamp = null;

    if (new.chgoperator is not null and new.pvalue <> old.pvalue) then
      new.chgtimestamp = current_time;
  end
end^
SET TERM ; ^
