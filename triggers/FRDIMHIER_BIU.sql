--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDIMHIER_BIU FOR FRDIMHIER                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable status smallint;
begin
  select frhdrs.status
    from frhdrs
    where frhdrs.symbol = new.frhdr
    into :status;
  if (:status <> 0) then
    exception FRHDRS_NOT_ACCESIBLE;
end^
SET TERM ; ^
