--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYIL_AU0 FOR STANYIL                        
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable magmaster varchar(3);
declare variable stmin smallint;
declare variable oddzialtowary varchar(10);
declare variable ilospotkplall numeric(14,4);
begin
  select STanyIL from DEFMAGAZ where SYMBOL=new.magazyn into :stmin;

  if(:stmin is null) then stmin = 0;
  if(:stmin = 0 and new.ilosc<0) then exception STIL_MINUS 'Stany ilosciowe ponizej zera: '||new.ktm;
  if((new.ilosc + new.iloscpod)<> (old.ilosc + old.iloscpod)) then begin
    select MAGMASTER from DEFMAGAZ where symbol = new.magazyn into :magmaster;
    if(:magmaster <> '') then
      update STANYIL set ILOSCPOD = ILOSCPOD - (old.ilosc +old.iloscpod)+ (new.ilosc +new.iloscpod) where WERSJAREF = new.wersjaref and MAGAZYN = :magmaster;
  end
  if(new.ilosc <> old.ilosc) then begin
    execute procedure getconfig('STANYMAGONTOWARY') returning_values :oddzialtowary;
    if(new.oddzial = :oddzialtowary) then begin
      update TOWARY set TOWARY.iloscwoddziale = ILOSCWODDZIALE + new.ilosc - old.ilosc where KTM = new.ktm;
    end
  end
  if (new.ilosc - new.zablokow <> old.ilosc - old.zablokow) then
    execute procedure CALCULATE_KOMPLETY_POT(new.wersjaref,new.magazyn,new.usluga);
  if (new.usluga = 2 and new.iloscpot <> old.iloscpot) then
  begin
    select sum(stanyil.iloscpot) from stanyil where stanyil.wersjaref = new.wersjaref
      into :ilospotkplall;
    if (:ilospotkplall is null) then
      ilospotkplall = 0;
    update wersje set wersje.ilospotkplall = :ilospotkplall where wersje.ref = new.wersjaref;
  end
end^
SET TERM ; ^
