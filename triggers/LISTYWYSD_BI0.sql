--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSD_BI0 FOR LISTYWYSD                      
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable lokres varchar(6);
declare variable dataokres timestamp;
begin

  if(new.refdok is not null) then new.typ = 'M';
  else if(new.refzam is not null) then new.typ = 'Z';
  else if (new.refsrv is not null) then new.typ = 'R';
  if(new.waga is null) then new.waga = 0;
  if(new.objetosc is null) then new.objetosc = 0;
  if(new.akcept is null) then new.akcept = 0;
  if(new.kosztdostbez is null) then new.kosztdostbez = 0;
  if(new.kosztdostroz is null) then new.kosztdostroz = 0;
  if(new.uwagisped is null) then new.uwagisped = '';
  if(new.flagisped is null) then new.flagisped = '';
  if(new.uwagi is null) then new.uwagi = '';
  if(new.symbolsped is null) then new.symbolsped = '';
  if(new.sellmistake is null) then new.sellmistake = 1;
  if(new.acceptmistake is null) then new.acceptmistake = 1;
  if(new.checkmistake is null) then new.checkmistake = 1;
  if(new.kontrolapak is null) then new.kontrolapak = 0;
  if(new.symbol is not null) then
    new.symbol = substring(new.symbol from 1 for 60);
  if((new.termdost = '1899-12-30') ) then new.termdost = null;
  if(new.okres is null or (new.okres = '') or (new.okres='      ')) then begin
    if(:dataokres is null) then dataokres = current_date;
    new.okres = cast( extract( year from :dataokres) as char(4));
    if(extract(month from :dataokres) < 10) then
       lokres = :lokres ||'0' ||cast(extract(month from :dataokres) as char(1));
    else
       lokres = :lokres ||cast(extract(month from :dataokres) as char(2));
    new.okres = :lokres;
  end
  if(new.kontrahent<>'') then begin
    if(new.adrtyp='F') then new.fromplace = substring(new.kontrahent from 1 for 20);
    if(new.adrtyp='T') then new.toplace = substring(new.kontrahent from 1 for 20);
  end
  if(new.rozliczono is null) then new.rozliczono = 0;
  if (new.oddzial is null) then begin
    execute procedure getconfig('AKTUODDZIAL') returning_values new.oddzial;
  end
end^
SET TERM ; ^
