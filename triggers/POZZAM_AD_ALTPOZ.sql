--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_AD_ALTPOZ FOR POZZAM                         
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  if (coalesce(old.fake,0) > 0) then
  begin
    if (not exists(select first 1 1 from pozzam where zamowienie = old.zamowienie and alttopoz = old.alttopoz)) then
      update pozzam
        set havefake = 0
        where ref = old.alttopoz;
  end
  if (coalesce(old.havefake,0) > 0) then
  begin
    delete from pozzam
      where zamowienie = old.zamowienie
        and alttopoz = old.ref
        and coalesce(fake,0) > 0;
  end
end^
SET TERM ; ^
