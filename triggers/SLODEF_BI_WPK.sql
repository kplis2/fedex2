--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLODEF_BI_WPK FOR SLODEF                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  -- Jeżeli wstawiamy sownik analityczny to kopiujemy pattern_Ref. Nie ważne, czy jest synchro czy nie
  -- bo i tak musimy zrobić select wic mamy za darmo, a nie musimy dokonywać komparacji, gdyż jak nie ma synchro
  -- to wpiszemy null
  if (new.analityka = 1 and new.isdist = 0 and new.kartoteka = 0 and new.typ = 'ESYSTEM') then
  select a.pattern_ref
    from accstructure a
    where a.dictdef = new.ref
    into new.patternref;
end^
SET TERM ; ^
