--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_BU_DATAOTRZKOR FOR NAGFAK                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable blokadaksiegowas varchar (15);
declare variable blokadaksiegowa timestamp;
declare variable zakup smallint;
declare variable nieobrot smallint;
declare variable korekta smallint;
begin
  if (new.dataotrzkor = '1899-12-30') then new.dataotrzkor =null;
  else
  begin
    if (old.dataotrzkor is not null and new.dataotrzkor is null) then
      exception nagfak_data_otrz_kor 'Niepoprawny format nowej daty';
    select t.korekta, t.nieobrot, t.zakup
      from typfak t
      where t.symbol = new.typ
      into :korekta, :nieobrot, :zakup;
    if (:korekta is null) then korekta = 0;
    if (:nieobrot is null) then nieobrot = 0;
    if (:zakup is null) then zakup = 0;
    if (((old.dataotrzkor is null and new.dataotrzkor is not null)or (old.dataotrzkor <> new.dataotrzkor))
      and new.blokada >= 3 and :korekta = 1 and :zakup = 0 and :nieobrot in (0,2)) then
    begin
      execute procedure get_config ('SIDFK_BLOKADADAT',2)
        returning_values :blokadaksiegowas;
      if (:blokadaksiegowas is not null and :blokadaksiegowas<>'' ) then begin
        blokadaksiegowa = cast(:blokadaksiegowas as timestamp);
        if (new.dataotrzkor < :blokadaksiegowa
          or (old.dataotrzkor < :blokadaksiegowa
            and new.dataotrzkor > :blokadaksiegowa ))
        then exception nagfak_data_otrz_kor;
      end
    end
  end
end^
SET TERM ; ^
