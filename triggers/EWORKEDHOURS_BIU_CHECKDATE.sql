--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWORKEDHOURS_BIU_CHECKDATE FOR EWORKEDHOURS                   
  ACTIVE BEFORE INSERT OR UPDATE POSITION 2 
as
declare variable fromdate date;
declare variable todate date;
declare variable hdate date;
declare variable personnames varchar(120);
begin

  if (new.hdate is not null and (inserting
    or new.period <> old.period
    or new.hdate is distinct from old.hdate
    or new.sgroup is distinct from old.sgroup)
  ) then begin
    hdate = new.hdate;

    execute procedure period2dates(new.period)
      returning_values :fromdate, :todate;

    if (hdate < :fromdate or hdate > :todate) then
      exception invalid_date;

    if (new.sgroup = 'EP') then
    begin
      select fromdate, todate, personnames from employees
        where ref = new.employee
        into :fromdate, todate, personnames;

      if (not(fromdate <= hdate and (todate >= :hdate or todate is null))) then
        exception universal personnames || ' w dniu ' || hdate || ' nie posiada aktywnej umowy o pracę. '
          || 'Godziny dla danego pracownika można rejestrować w okresie od ' || fromdate || coalesce(' do ' || todate, '') || '.';
    end
  end
end^
SET TERM ; ^
