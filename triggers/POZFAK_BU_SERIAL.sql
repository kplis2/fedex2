--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAK_BU_SERIAL FOR POZFAK                         
  ACTIVE BEFORE UPDATE POSITION 2 
as
declare variable nserial integer;
begin
  if(new.ktm is not null and (new.wersjaref <> old.wersjaref)) then begin
    new.serialized = 0;
    select SERIAL from TOWARY where KTM = new.ktm into new.serialized;
    if(new.serialized = 1) then begin
/* wyciete - wąsciwa kontrola ma byc na poziomie dok. magazynowych - Kuba 05.07.10
      select NOTSERIAL from defmagaz where SYMBOL=new.magazyn into :nserial;
      if(:nserial = 1) then begin
        new.serialized = 0;
      end*/
    end
  end
  if(new.serialized <> old.serialized) then
    delete from DOKUMSER where TYP = 'F' and REFPOZ=new.ref;
end^
SET TERM ; ^
