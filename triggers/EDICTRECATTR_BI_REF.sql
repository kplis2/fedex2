--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDICTRECATTR_BI_REF FOR EDICTRECATTR                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EDICTRECATTR')
      returning_values new.REF;
end^
SET TERM ; ^
