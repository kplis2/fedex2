--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAKTERMPLAT_AI0 FOR NAGFAKTERMPLAT                 
  ACTIVE AFTER INSERT POSITION 0 
AS
  declare variable procentsp numeric(3,2);
  declare variable akc smallint;
begin
  if(new.doktyp='F') then begin
    select n.akceptacja from nagfak n where n.ref = new.dokref into :akc;
    if (:akc = 1) then exception NAGFAK_AKCEPT 'Modyfikacja tabeli spłat dla zaakceptowanej faktury niemożliwa';
  end else if(new.doktyp='M') then begin
    select n.akcept from dokumnag n where n.ref = new.dokref into :akc;
    if (:akc = 1) then exception NAGFAK_AKCEPT 'Modyfikacja tabeli spłat dla zaakceptowanego dokumentu niemożliwa';
  end
  select sum(n.procentplat)
    from NAGFAKTERMPLAT n
    where n.dokref = new.dokref and n.doktyp=new.doktyp into :procentsp;
  if (procentsp > 100) then
    exception TAB_TERMPLAT_PROCENT;
end^
SET TERM ; ^
