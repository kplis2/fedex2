--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PKOSOBY_AD_SYNCHRO FOR PKOSOBY                        
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable refukli integer ;
declare variable synchro smallint;
begin
  execute procedure getconfig('SYNCHROUZYKLI') returning_values :synchro;
  if (synchro = 1) then
  begin
    select uzykli.ref from uzykli where uzykli.pkosoba = old.ref
    into :refukli;
    if (refukli is not null) then
    delete from uzykli where uzykli.ref = :refukli;
  end
end^
SET TERM ; ^
