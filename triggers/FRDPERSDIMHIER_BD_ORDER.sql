--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDPERSDIMHIER_BD_ORDER FOR FRDPERSDIMHIER                 
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update frdpersdimhier set ord = 0, hierorder = hierorder - 1
    where ref <> old.ref and hierorder > old.hierorder and frdperspect = old.frdperspect;
end^
SET TERM ; ^
