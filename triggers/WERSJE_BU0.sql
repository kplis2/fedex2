--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WERSJE_BU0 FOR WERSJE                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable cnt integer;
declare variable free_no integer;
declare variable curr_no integer;
declare variable vaktywnytow integer;
begin
  if(new.usluga<>old.usluga) then begin
    select coalesce(tow.rights,''), coalesce(tow.rightsgroup,'')
      from towtypes tow where tow.numer = new.usluga
      into new.prawa, new.prawagrup;
  end
  if(new.mwscalcmix is null) then new.mwscalcmix = 0;
  if(new.cena_zakn is null) then new.cena_zakn = 0;
  if(new.cena_zakb is null) then new.cena_zakb = 0;
  if(new.cena_fabn is null) then new.cena_fabn = 0;
  if(new.cena_fabb is null) then new.cena_fabb = 0;
  if(new.ilosc is null) then new.ilosc = 0;
  if(new.zamowiono is null) then new.zamowiono = 0;
  if(new.zarezerw is null) then new.zarezerw = 0;
  if(new.zablokow is null) then new.zablokow = 0;
  if(new.cena_kosztn is null) then new.cena_kosztn = 0;
  if(new.cena_kosztb is null) then new.cena_kosztb = 0;
  if(new.cena_zaknl is null) then new.cena_zaknl = 0;
  if(new.dniwazn is null) then new.dniwazn = 0;
  if((new.CENA_ZAKB <> old.CENA_ZAKB) or (new.CENA_ZAKN <> old.CENA_ZAKN)
      or (new.CENA_FABB <> old.CENA_FABB) or (new.CENA_FABN <> old.CENA_FABN)
      or (new.CENA_KOSZTB <> old.CENA_KOSZTB) or (new.CENA_KOSZTN <> old.CENA_KOSZTN)
  ) then
        new.lastcenzmiana = current_time;
  if (old.vat <> new.vat) then
  begin
    if (new.vat = '') then new.vat = null;
    else if (exists(select t.ktm from towary t where t.ktm = new.ktm and t.vat = new.vat))then
      new.vat = null;
  end
  if (old.pkwiu <> new.pkwiu and new.pkwiu = '') then new.pkwiu = null;
  if(new.ktm<>old.ktm or new.nazwa<>old.nazwa) then begin
    if(exists(select ref from wersje where ktm=new.ktm and nazwa=new.nazwa and ref<>new.ref)) then
      exception WERSJE_NAZWA;
  end
  if (old.akt <> new.akt and new.akt = 1) then
  begin
    select akt from TOWARY where KTM = new.ktm into :vaktywnytow;
    if (:vaktywnytow = 0) then
      exception TOWAR_NIEAKTYWNY 'Towar nieaktywny. Nie można aktywować jego wersji.';
  end
end^
SET TERM ; ^
