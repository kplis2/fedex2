--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFELEM_BEFORE_INSERT FOR DEFELEM                        
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  /*ustawieni loka do przenumerowania*/
  if(new.ukrywanie is null) then
    select ukrywanie from DEFKATAL where REF=new.katalog into new.ukrywanie;
end^
SET TERM ; ^
