--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDDOCDIFF_BIU1 FOR MWSORDDOCDIFF                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
begin
  if (new.rozl is null) then new.rozl = 0;
  if (new.grupa is null or new.grupa = '') then
    select substring(grupa from 1 for 40) from towary where ktm = new.ktm
      into new.grupa;
end^
SET TERM ; ^
