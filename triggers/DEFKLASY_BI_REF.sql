--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFKLASY_BI_REF FOR DEFKLASY                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DEFKLASY')
      returning_values new.REF;
end^
SET TERM ; ^
