--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDS_BI_REF FOR MWSSTANDS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSSTANDS') returning_values new.ref;
end^
SET TERM ; ^
