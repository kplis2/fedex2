--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FXDASSETS_BI_REF FOR FXDASSETS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FXDASSETS')
      returning_values new.REF;
end^
SET TERM ; ^
