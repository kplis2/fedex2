--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_AD_EMPLCALDAYS FOR EABSENCES                      
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  update emplcaldays set absence = null, absencediff = null
    where employee = old.employee
    and cdate between old.fromdate and old.todate;
end^
SET TERM ; ^
