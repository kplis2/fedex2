--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACH_AD_FSBALANCES FOR ROZRACH                        
  ACTIVE AFTER DELETE POSITION 0 
AS
  declare variable debit numeric(14,2);
  declare variable credit numeric(14,2);
  declare variable fsbalances integer;
begin
  debit = 0;
  credit = 0;
  if (old.winien > old.ma) then debit = old.winien - old.ma;
  if (old.ma > old.winien) then credit = old.ma - old.winien;
  select ref from fsbalances
    where account = old.kontofk and dictdef = old.slodef and dictpos = old.slopoz
      and company = old.company and currency = old.waluta
    into :fsbalances;
  if (fsbalances is not null) then
  begin
    update fsbalances set debit = debit - :debit, credit = credit-:credit,
      btranamountbank = btranamountbank - old.btranamountbank
      where ref = :fsbalances;
  end
  if (old.dictgrp is not null) then
    update fsbalances set grpdebit = grpdebit - :debit, grpcredit = grpcredit - :credit,
      grpbtranamountbank = grpbtranamountbank - old.btranamountbank
      where dictgrp = old.dictgrp and company = old.company and currency = old.waluta;
end^
SET TERM ; ^
