--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EAPPLTYPES_BU FOR EAPPLTYPES                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (old.ref between 1 and 9 and (old.ref <> new.ref or new."TYPE" is distinct from old."TYPE")) then
    exception universal 'Nie można edytować typu ani numeru ref, obsluga wymagana przez WebSerwis eHRM';
end^
SET TERM ; ^
