--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPERSONS_BI_ADDPERSON FOR CPERSONS                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable ref integer;
begin

  rdb$set_context('USER_TRANSACTION', 'PERSON_ADD_COMPANY_EXIT', '1');

  insert into persons (ref, fname, middlename, sname, mname, fathername,
      mothername, mothmname, sex,  birthdate, nationality,
      citizenship, foreigner, steadystaycard,  evidenceno, evidissuedby,
      passportno, passissuedby, pesel, nip, profession, education,
      email, cellphone, activ, oemail, omobile, ophone, phone,
      evidencedate, passportdate, marstatus, convicted, birthplace,
      expevidencedate, exppassportdate, ehrm, login
  ) values (new.ref, new.fname, new.middlename, new.sname, new.mname, new.fathername,
      new.mothername, new.mothmname, new.sex, new.birthdate, new.nationality,
      new.citizenship, new.foreigner, new.steadystaycard, new.evidenceno, new.evidissuedby,
      new.passportno, new.passissuedby, new.pesel, new.nip, new.profession, new.education,
      new.email, new.cellphone, new.activ, new.oemail, new.omobile, new.ophone, new.phone,
      new.evidencedate, new.passportdate, new.marstatus, new.convicted, new.birthplace,
      new.expevidencedate, new.exppassportdate, new.ehrm, new.login
  ) returning ref into ref;

  rdb$set_context('USER_TRANSACTION', 'PERSON_ADD_COMPANY_EXIT', '0');

  execute procedure person_add_company(:ref, new.company, new.symbol);

end^
SET TERM ; ^
