--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDS_BD_ORDER FOR MWSSTANDS                      
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0 or old.ord = 5) then
    exit;
  select max(number) from mwsstands where whsecrow = old.whsecrow
    into :maxnum;
  if (old.number = :maxnum) then
    exit;
  update mwsstands set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and whsecrow = old.whsecrow;
end^
SET TERM ; ^
