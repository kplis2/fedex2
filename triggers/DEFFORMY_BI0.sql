--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFFORMY_BI0 FOR DEFFORMY                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.akt is null) then new.akt = 0;
  execute procedure REPLICAT_STATE('DEFFORMY',new.state, NULL,new.akt,'0',NULL) returning_values new.state;
end^
SET TERM ; ^
