--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_BU_KONTROLA FOR NAGFAK                         
  ACTIVE BEFORE UPDATE POSITION 8 
as
begin
  if(new.akceptacja = 1
   and old.akceptacja <> 0 and old.akceptacja<9
     and((new.sumwartnet<>old.sumwartnet)
       or(new.esumwartnet<>old.esumwartnet)
       or(new.esumwartbru<>old.esumwartbru)
       or(new.sumwartbru<>old.sumwartbru)
       or(new.esumwartnetzl<>old.esumwartnetzl)
       or(new.esumwartbruzl<>old.esumwartbruzl)
       or(new.sumwartnetzl<>old.sumwartnetzl)
       or(new.sumwartbruzl<>old.sumwartbruzl)
      ))
        then
  begin
    -- naliczanie pol ESUM...
    if(new.zaliczkowy>0) then begin
      new.esumwartbru = new.pesumwartbru + new.kwotazal;
      new.esumwartbruzl = new.esumwartbru*new.kurs;
      if(new.sumwartbru<>0) then begin
        update ROZFAK set ESUMWARTBRU=new.esumwartbru*SUMWARTBRU/new.sumwartbru
        where DOKUMENT=new.ref;
        select sum(ESUMWARTNET), sum(ESUMWARTNETZL) from ROZFAK where DOKUMENT=new.ref
        into new.esumwartnet, new.esumwartnetzl;
      end else begin
        new.esumwartnet = 0;
        new.esumwartnetzl = 0;
      end
    end else begin
      new.esumwartbru = new.sumwartbru;
      new.esumwartnet = new.sumwartnet;
      new.esumwartbruzl = new.sumwartbruzl;
      new.esumwartnetzl = new.sumwartnetzl;
      if(new.sad=0) then begin
        update ROZFAK set ESUMWARTNET=SUMWARTNET, ESUMWARTBRU=SUMWARTBRU,
                          ESUMWARTNETZL=SUMWARTNETZL, ESUMWARTBRUZL=SUMWARTBRUZL,
                          vesumwartbruZL= case when new.bn = 'B' then
                                            cast(SUMWARTBRU*new.vkurs as numeric(14,2))
                                          else
                                            cast(SUMWARTNET*new.vkurs as numeric(14,2)) + --netto
                                              cast(((cast(SUMWARTNET*new.vkurs as numeric(14,2))- psumwartnetzl) * ((select v.stawka from vat v where v.grupa = vat)/100)) as numeric(14,2)) + psumwartvatzl -- vat
                                          end,
                           VESUMWARTNETZL= case when new.bn = 'B' then
                                            VESUMWARTNETZL
                                          else
                                            SUMWARTNET*new.vkurs
                                          end

                        ,
                          BLOKADAPRZELICZ=1
        where DOKUMENT=new.ref;
      end else begin
        update ROZFAK set ESUMWARTNET=SUMWARTNET, ESUMWARTBRU=SUMWARTBRU,
                          ESUMWARTNETZL=SUMWARTNETZL, ESUMWARTBRUZL=SUMWARTBRUZL,
                          VESUMWARTNETZL=(select sum(POZSAD.WARTNET-POZSAD.PWARTNET) from POZSAD where POZSAD.FAKTURA=new.ref and POZSAD.GR_VAT=ROZFAK.VAT),
                          VESUMWARTBRUZL=(select sum(POZSAD.WARTNET-POZSAD.PWARTNET+POZSAD.VAT-POZSAD.PVAT) from POZSAD where POZSAD.FAKTURA=new.ref and POZSAD.GR_VAT=ROZFAK.VAT),
                          BLOKADAPRZELICZ=1
        where DOKUMENT=new.ref;
      end
    end
    delete from rozfak r where r.dokument=new.ref
     and coalesce(r.sumwartnet,0)=0 and coalesce(r.sumwartvat,0)=0 and coalesce(r.sumwartbru,0)=0
     and coalesce(r.esumwartnet,0)=0 and coalesce(r.esumwartvat,0)=0 and coalesce(r.esumwartbru,0)=0
     and coalesce(r.psumwartnet,0)=0 and coalesce(r.psumwartvat,0)=0 and coalesce(r.psumwartbru,0)=0
     and coalesce(r.pesumwartnet,0)=0 and coalesce(r.pesumwartvat,0)=0 and coalesce(r.pesumwartbru,0)=0;
  end
end^
SET TERM ; ^
