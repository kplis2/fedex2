--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVGOODDEF_BD_ORDER FOR SRVGOODDEF                     
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update srvgooddef set ord = 0, number = number - 1
    where symbol <> old.symbol and number > old.number and contrtype = old.contrtype;
end^
SET TERM ; ^
