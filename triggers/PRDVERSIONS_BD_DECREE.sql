--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRDVERSIONS_BD_DECREE FOR PRDVERSIONS                    
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable bkperiod varchar(6);
declare variable bkdocsymbol varchar(20);
begin
 select first 1 b.symbol, b.period from decrees d join bkdocs b on (b.ref = d.bkdoc)
   join prdaccounting p on (p.ref = old.prdaccounting)
     where d.period>=old.fromperiod and d.prdaccounting = old.prdaccounting and d.ref<>p.decree
 into :bkdocsymbol, :bkperiod;
 if(coalesce(:bkdocsymbol,'') <> '') then
   exception prd_deducted 'Istnieje powiązany dokument RMK ' || :bkdocsymbol || ' (' || :bkperiod || '). Nie można skasować!';
 else
 begin
  delete from prdaccountingpos where prdversion = old.ref;
 end
end^
SET TERM ; ^
