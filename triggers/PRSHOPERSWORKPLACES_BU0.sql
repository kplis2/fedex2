--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHOPERSWORKPLACES_BU0 FOR PRSHOPERSWORKPLACES            
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  new.main = coalesce(new.main,0);
  if(new.main = 0 and not exists (select p.ref from prshopersworkplaces p where p.main = 1 and p.prshoper = new.prshoper)) then begin
    new.main = 1;
  end
end^
SET TERM ; ^
