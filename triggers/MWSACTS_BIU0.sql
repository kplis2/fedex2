--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_BIU0 FOR MWSACTS                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
declare variable mwsordstatus smallint;
declare variable mwsordtype integer;
declare variable locsrequired smallint;
declare variable autoloccreate smallint;
declare variable mwspallocref integer;
declare variable mwspalloctype integer;
declare variable local smallint;
declare variable recdoctype smallint;
declare variable verscnt integer;
declare variable wh varchar(3);
declare variable mwsordtypedets integer;
begin
  if (new.stopnextmwsacts is null) then new.stopnextmwsacts = 0;
  if (new.refilltry is null) then new.refilltry = 0;
  if (new.multiversgood is null) then
  begin
    select count(ref) from wersje where ktm = new.good into verscnt;
    if (verscnt > 1) then
      verscnt = 1;
    new.multiversgood = verscnt;
  end
  if (new.l is null or new.w is null or new.h is null and new.mwspallocsize is not null) then
    select s.l, s.w, s.h, s.segtype
      from mwspallocsizes s
      where s.ref = new.mwspallocsize
      into new.l, new.w, new.h, new.segtype;
  if (new.l is null) then new.l = 80;
  if (new.w is null) then new.w = 120;
  if (new.h is null) then new.h = 180;
  if (new.packquantity is null) then new.packquantity = 1;
  if (inserting) then execute procedure CHECK_LOCAL('DOKUMPOZ',new.ref) returning_values :local;
  if (new.docid is null) then new.docid = 0;
  if (new.docposid is null) then new.docposid = 0;
  if (new.doctype is null) then new.doctype = '';
  if (new.status is null) then new.status = 0;
  if (new.status = 0) then new.quantityc = 0;
  if (new.stocktaking is null) then
    select stocktaking from mwsords where ref = new.mwsord into new.stocktaking;
  if (new.settlmode is null) then new.settlmode = 0;
  if (new.closepalloc is null) then new.closepalloc = 0;
  if (new.quantity is null) then new.quantity = 0;
  if (new.quantityl is null) then new.quantityl = 0;
  if (new.quantityc is null) then new.quantityc = 0;
  if (new.priority is null) then new.priority = 0;
  if (new.stancen = 0) then new.stancen = null;
  if (new.packmeth is null) then new.packmeth = '';
  if (new.statusblock is null) then new.statusblock = 0;
  if (new.disttogo is null) then new.disttogo = 0;
  if (new.mistakescnt is null) then new.mistakescnt = 0;
  if (new.mistakescntq is null) then new.mistakescntq = 0;
  if (new.datestopdecl is null and new.timestopdecl is not null) then
    new.datestopdecl = cast(new.timestopdecl as date);
  if (new.deepl is null and new.mwsconstlocl is not null) then
    select deep from mwsconstlocs where ref = new.mwsconstlocl into new.deepl;
  if (new.deepp is null and new.mwsconstlocl is not null) then
    select deep from mwsconstlocs where ref = new.mwsconstlocp into new.deepp;
  -- nie wolno podmieniac lokacji stalych na operacjach potwierdzonych
  if (new.mwsordtype is null) then
    select mwsordtype from mwsords where ref = new.mwsord into new.mwsordtype;
  if (new.mwsordtypedest is null) then
  begin
    select mt.mwsortypedets
      from mwsords o
        join mwsordtypes mt on (mt.ref = o.mwsordtype)
      where o.ref = new.mwsord
      into new.mwsordtypedest;
  end 
  if (new.good is null or new.good = '') then
    exception MWSACT_WITH_NO_GOOD 'operacja zlecenia bez towaru na poz. '||new.number;
  if (old.status = 5 and new.status <> 5
      and (new.mwsconstlocp <> old.mwsconstlocp or (new.mwsconstlocp is not null and old.mwsconstlocp is null)
        or (new.mwsconstlocp is null and old.mwsconstlocp is not null)
        or new.mwsconstlocl <> old.mwsconstlocl or (new.mwsconstlocl is not null and old.mwsconstlocl is null)
        or (new.mwsconstlocl is null and old.mwsconstlocl is not null))
  ) then
    exception MWSCONSTLOC_CANT_BE_CHANGED;
  if (new.mixedpallgroup is null) then new.mixedpallgroup = 0; --MF
  -- weryfikacja ilosci potwierdzonej - ilosc 0 tylko mozliwa dla operacji inwentaryzacji
  if (new.status = 5 and new.quantityc = 0 and new.stocktaking = 0) then
    exception QUANTITYC_ZERO;
 -- if (new.plus is null) then
 --   exception MWSACTDET_PLUS_NOT_DEFINED;
  if (new.recdoc is null) then
    select rec from mwsords where ref = new.mwsord into new.recdoc;
  select status, mwsordtype, wh
    from mwsords
    where ref = new.mwsord
    into :mwsordstatus, :mwsordtype, :wh;
  if (new.wh is null) then new.wh = wh;
  select t.locsrequired, t.autoloccreate, t.mwspalloctype, t.recdoc, t.mwsortypedets
    from mwsordtypes t
    where t.ref = :mwsordtype
    into locsrequired, autoloccreate, mwspalloctype, recdoctype, :mwsordtypedets;
  -- przychodowe pozycje musza miec dostawe
  if (new.recdoc = 1 and new.lot is null) then
  begin
    select first 1 lot from mwsacts where mwsord = new.mwsord and lot is not null
      into new.lot;
    if (new.lot is null) then
      execute procedure DOKUMNAG_CREATE_DOKMAG_DOSTAWA(null,new.slopoz,current_date,:wh,null,null,null,null)
        returning_values new.lot;
  end
  if ((new.status = 1 or new.status = 2) and old.status = 0) then
  begin
    if (new.mwspallocl is null) then
    begin
      -- wybranie lokacji paletowej z lokacji stalej
--      select first 1 ref from mwspallocs where mwsconstloc = new.mwsconstlocl
  --      into new.mwspallocl;
      -- wybranie innej lokacji z tego samego zlecenia
      if (new.recdoc = 0 and new.stocktaking = 1) then
        select first 1 mwspallocl from mwsacts where mwsord = new.mwsord
          into new.mwspallocl;
      if (new.recdoc = 1) then
        select first 1 mwspallocp from mwsacts where mwsord = new.mwsord
          into new.mwspallocp;
      -- dla dokumentów przychodowych jak w grupie paletowej jest juz lokacja paletowa to trzeba ja utrzymac
      if (:recdoctype = 1 and new.mwspallocl is null and new.palgroup is not null) then
        select first 1 mwspallocl from mwsacts where palgroup = new.palgroup and mwspallocl is not null
          into new.mwspallocl;
      if (new.mwspallocl is null and new.mwsconstlocl is not null --and autoloccreate > 0
      ) then
      begin
        execute procedure gen_ref('MWSPALLOCS') returning_values mwspallocref;
        insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status,
            fromdocid, fromdocposid, fromdoctype)
          select :mwspallocref, symbol||'/'||ref, ref, :mwspalloctype, 0,
              new.docid, new.docposid, new.doctype
            from mwsconstlocs 
            where ref = new.mwsconstlocl;
        new.mwspallocl = mwspallocref;
      end
      else if (new.mwspallocl is null and new.recdoc = 1 -- and autoloccreate > 0
      ) then
      begin
        execute procedure gen_ref('MWSPALLOCS') returning_values mwspallocref;
        insert into mwspallocs (ref, symbol, mwsconstloc, mwspalloctype, status,
            fromdocid, fromdocposid, fromdoctype)
          values (:mwspallocref, :mwspallocref, null, :mwspalloctype, 0,
              new.docid, new.docposid, new.doctype);
        new.mwspallocl = mwspallocref;
      end
    end
  end
  if (locsrequired = 0 and new.status = 5 and (new.mwsconstlocp is null or new.mwspallocp is null)) then
    exception MWSLOCSP_REQUIRED;
  if (locsrequired = 1 and new.status = 5 and (new.mwsconstlocl is null or new.mwspallocl is null)) then
    exception MWSLOCSE_REQUIRED;
  if (locsrequired = 2 and new.status = 5 and (new.mwsconstlocp is null or new.mwspallocp is null
      or new.mwsconstlocl is null or new.mwspallocl is null)) then
    exception MWSLOCS_REQUIRED;
  -- uzupelnienie sektora na operacji - najpierw z lokacji koncowej a potem z poczatkowej
  if (new.whsec is null and new.mwsconstlocl is not null) then
    select whsec from mwsconstlocs where ref = new.mwsconstlocl into new.whsec;
  if (new.whsec is null and new.mwsconstlocp is not null) then
    select whsec from mwsconstlocs where ref = new.mwsconstlocp into new.whsec;
  -- obsluga grupy potwierdzania operacji dla czasów realizacji operacji grupowej
  if (new.status = 2 and new.groupcommit is null) then
    new.groupcommit = new.ref;
  if (inserting) then  --Marcin F.
  begin
    select first 1 case when towjedn.waga is null then 0 else towjedn.waga end*new.quantity  -- xxx matj
      from towjedn
      where towjedn.ktm = new.good
       -- and towjedn.glowna = 1
    into new.weight;
    select VOLUME, LOADPERCENT, RATIO from XK_mws_good_vol(new.good, new.vers,new.quantity,0,0)
    into new.volume, new.loadpercent, new.packquantity;
  end
  else if (updating and (new.quantity <> old.quantity or new.good <> old.good)) then    --Marcin F.
  begin
    select case when towjedn.waga is null then 0 else towjedn.waga end*new.quantity
      from towjedn
      where towjedn.ktm = new.good
        and towjedn.glowna = 1
    into new.weight;
    select VOLUME, LOADPERCENT, RATIO from XK_mws_good_vol(new.good, new.vers,new.quantity,0,0)
      into new.volume, new.loadpercent, new.packquantity;
  end
  if (new.mwsordtypedest is not null and old.mwsordtypedest is null and new.mwsordtypedest <> 1) then
    select paleta from towary where ktm = new.good into new.ispal;

  if (updating and :mwsordtypedets = 15 and new.status <> old.status and old.status = 5 and
      exists(select first 1 1 from mwsords where mwsordtypedest = 7 and frommwsord = new.mwsord) and new.mwsord <> 4664) then
  begin
    exception universal 'Nie wolno odakceptowywać pozycji zlecenia rozliczonego zlecenia inwentaryzacji!';
  end
end^
SET TERM ; ^
