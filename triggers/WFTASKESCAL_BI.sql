--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WFTASKESCAL_BI FOR WFTASKESCAL                    
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_wftaskescal,1);
end^
SET TERM ; ^
