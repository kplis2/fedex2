--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPWOJ16M_BI_REF FOR CPWOJ16M                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CPWOJ16M')
      returning_values new.REF;
end^
SET TERM ; ^
