--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NUMBERGEN_BI0 FOR NUMBERGEN                      
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.typ is null) then new.typ = 0;
  if(new.s_order is null) then new.s_order = 0;
  if(new.oddzial is null) then new.oddzial=0;
  if(new.company is null) then new.company = 0;
end^
SET TERM ; ^
