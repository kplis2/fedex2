--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECANDIDATES_BI_REF FOR ECANDIDATES                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ECANDIDATES')
      returning_values new.REF;
end^
SET TERM ; ^
