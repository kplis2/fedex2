--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_BD0 FOR DECREES                        
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.status>0) then
    exception DECREES_BD;
  if (old.settlement is not null and old.settlement <> '') then
  begin
    execute procedure unreg_settlement(old.ref);
--    delete from rozrachp where rozrachp.stable = old.stable and rozrachp.sref = old.sref;
  end
  if (old.sssettlement is not null and old.sssettlement <> '' and old.ssallowdelete =0) then
    exception universal 'Dekrety rozrachunkowe powiązane z SiD można tylko modyfikować.';
end^
SET TERM ; ^
