--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_CHKDATES FOR EABSENCES                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 4 
as
begin
--Kontrola dat

  if (inserting or (updating and (new.fromdate <> old.fromdate or new.todate <> old.todate
      or new.employee <> old.employee or new.correction <> old.correction))) then
  begin
    if (new.fromdate > new.todate) then
      exception data_do_musi_byc_wieksza;

    if (new.correction <> 1) then
    begin
      if (exists(select first 1 1 from eabsences
               where fromdate <= new.todate
                 and todate >= new.todate
                 and employee = new.employee
                 and ref <> new.ref
                 and correction <> 1
                 and (corrected is null or corrected <> new.ref)
                 and (coalesce(emplcontract, new.emplcontract) = new.emplcontract
                      or new.emplcontract is null))
      /*bez ostatniego warunku nie usuniemy korekty po dzialaniu triggera _BD_CORRECTION,
      chyba za biezacy trigger przejdzie na after (BS20435)*/
      ) then
        exception data_do_w_srodku_innego;
    
      if (exists(select first 1 1 from eabsences
               where fromdate <= new.fromdate
                 and todate >= new.fromdate
                 and employee = new.employee
                 and ref <> new.ref
                 and correction <> 1
                 and (corrected is null or corrected <> new.ref)
                 and (coalesce(emplcontract, new.emplcontract) = new.emplcontract
                      or new.emplcontract is null))
      ) then
        exception data_od_w_srodku_innego;
    
      --sprawdzenie czy okres nie zawiera innego
      if (exists(select first 1 1 from eabsences
               where fromdate >= new.fromdate
                 and todate <= new.todate
                 and employee = new.employee
                 and ref <> new.ref
                 and correction <> 1
                 and (corrected is null or corrected <> new.ref)
                 and (coalesce(emplcontract, new.emplcontract) = new.emplcontract
                      or new.emplcontract is null))
      ) then
        exception data_oddo_na_zewnatrz;
    end
  end
end^
SET TERM ; ^
