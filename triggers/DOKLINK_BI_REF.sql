--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKLINK_BI_REF FOR DOKLINK                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('DOKLINK') returning_values new.ref;
end^
SET TERM ; ^
