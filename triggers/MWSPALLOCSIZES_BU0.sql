--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSPALLOCSIZES_BU0 FOR MWSPALLOCSIZES                 
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.l is null) then new.l = 0;
  if (new.w is null) then new.w = 0;
  if (new.h is null) then new.h = 0;
  if (new.l <> old.l or new.w <> old.w or new.h <> old.h) then
  begin
    new.shortname = cast(new.l as integer)||'x'||cast(new.h as integer); --szer x wys
    new.longname = cast(new.l as integer)||'x'||cast(new.w as integer)||'x'||cast(new.h as integer); -- --szer x wys x h
  end
  if (new.l = 0 or new.l is null or new.w is null or new.w = 0 or new.h is null or new.h = 0) then
  begin
    new.shortname = '';
    new.longname = '';
  end
end^
SET TERM ; ^
