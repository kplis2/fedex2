--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_MESSAGES_BIU_BLANK FOR S_MESSAGES                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.msgsubject is null) then new.msgsubject = '';
  if (new.cnterrorsend is null) then new.cnterrorsend = 0;
end^
SET TERM ; ^
