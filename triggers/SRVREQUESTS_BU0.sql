--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVREQUESTS_BU0 FOR SRVREQUESTS                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if(new.status<>old.status and new.status is not null) then select nazwa from CFAZY where ref=new.status into new.statusnazwa;
  if(new.regoper<>old.regoper and new.regoper is not null) then select nazwa from OPERATOR where ref=new.regoper into new.regopernazwa;
  if(new.SRECDATE is not null and new.CRECDATE is null) then begin
    new.CRECDATE = new.SRECDATE;
  end
  if(new.CPASSDATE is not null and new.SPASSDATE is null) then begin
    new.SPASSDATE = new.CPASSDATE;
  end
  if (new.status <> old.status) then
    select AKTYWNE from CFAZY where REF = new.status into new.aktywne;
end^
SET TERM ; ^
