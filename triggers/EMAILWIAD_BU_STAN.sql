--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMAILWIAD_BU_STAN FOR EMAILWIAD                      
  ACTIVE BEFORE UPDATE POSITION 1 
AS
begin
  if(new.stan is distinct from old.stan) then
  begin
    if(old.stan=2 and new.stan<2) then
      begin
        if((select rola from deffold d join emailwiad e on d.ref=e.folder
          where e.ref=new.ref)=2) then --nie pozwalaj w folderze draft
            new.stan=old.stan;--zmienia? stanu z edited na seen
      end
  end
end^
SET TERM ; ^
