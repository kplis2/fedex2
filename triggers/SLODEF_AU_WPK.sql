--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLODEF_AU_WPK FOR SLODEF                         
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable ispattern smallint_id;
declare variable slaveref slo_id;
begin
  -- musimy wykryc czy obecny slodef moze byc wzorcem, czyli na pewno musi byc indywidualny i
  -- nie podlegc pod inne company
  if (new.company is not null and new.patternref is null) then
  begin
    -- dobra sa przeslanki za wzorcem, wiec sprawdzamy a zakladzie
    select c.pattern
      from companies c
      where c.ref = new.company
      into :ispattern;
    if (ispattern is distinct from null) then
    begin
      --wiec skoro wzorzec to propaguj na zalezne
      for
        select s.ref
          from slodef s
          where s.patternref = new.ref
          into :slaveref
      do begin
       execute procedure synchronize_slodef(new.ref, slaveref, 1);
      end
    end
  end
  -- Jak odpinamy synchronizacje to odpinamy na pozycjach też
  if (coalesce(old.patternref,0) > 0 and coalesce(new.patternref,0)= 0) then
  begin
    update slopoz sp
      set sp.pattern_ref = null, sp.internalupdate = 1
      where sp.slownik = new.ref;
  end
end^
SET TERM ; ^
