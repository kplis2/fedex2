--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPOSITIONS_BI0 FOR PMPOSITIONS                    
  ACTIVE BEFORE INSERT POSITION 1 
AS
declare variable licznik integer;
declare variable ktm varchar(40);
begin
  select PMPLAN from PMELEMENTS where REF=new.pmelement into new.pmplan;
  if(new.ktm = '') then new.ktm = null;
-- jesli znajdzie w towarach unikalny rekord o takiej nazwie to przepisuje KTM
-- jesli znajdzie ich wiecej, szuka unikalnego po nazwie i symbolu
-- jesli nie znajdzie, to nie przepisuje ktm-u
  select count(*) from towary where nazwa=new.name into :licznik;
  if(licznik=1) then
  begin
    select ktm from towary where nazwa=new.name into :ktm;
    new.ktm=:ktm;
  end
  else if(licznik>1) then
  begin
    select count(*) from towary where nazwa=new.name and symbolnorma=new.symbol
      into :licznik;
    if(licznik=1) then
    begin
      select ktm from towary where nazwa=new.name and symbolnorma=new.symbol
        into :ktm;
      new.ktm=:ktm;
    end
  end
  if(new.version is null or new.version='') then begin
    select version, versionnum from pmplans where ref=new.pmplan into new.version, new.versionnum;
  end
  if(new.mainref is null) then new.mainref=new.ref;

  if(new.quantity is null) then new.quantity = 0;
  if(new.calcprice is null) then new.calcprice = 0;
  if(new.calcval is null) then new.calcval = 0;
  if(new.budprice is null) then new.budprice = 0;
  if(new.budval is null) then new.budval =0;
  if(new.budvalr is null) then new.budvalr = 0;
  if(new.usedq is null) then new.usedq = 0;
  if(new.usedprice is null) then new.usedprice = 0;
  if(new.usedval is null) then new.usedval = 0;
  if(new.intq is null) then new.intq = 0;
  if(new.ofeq is null) then new.ofeq = 0;
  if(new.extval is null) then new.extval = 0;
  if(new.realq is null) then new.realq = 0;
  if(new.realval is null) then new.realval = 0;
  if(new.erealq is null) then new.erealq = 0;
  if(new.erealprice is null) then new.erealprice = 0;
  if(new.erealval is null) then new.erealval = 0;
end^
SET TERM ; ^
