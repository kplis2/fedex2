--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_AU0 FOR DECREES                        
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable bktype smallint;
declare variable balsum numeric(14,2);
declare variable oldbktype smallint;
declare variable matchable smallint;
declare variable company companies_id;
declare variable yearid years_id;
begin
  if (new.account <> old.account
     or (new.account is not null and old.account is null)
     or (new.account is null and old.account is not null)
     or new.credit <> old.credit or new.debit <> old.debit
     or (old.credit is null and new.credit is not null)
     or (old.debit is null and new.debit is not null)
     or (old.credit is not null and new.credit is null)
     or (old.debit is not null and new.debit is null)
     )
  then begin
    select b.bktype from bkaccounts b where b.ref = new.accref into :bktype;
    select b.bktype from bkaccounts b where b.ref = old.accref into :oldbktype;
    if(bktype < 2 and oldbktype < 2) then begin
      select baldebit - balcredit
        from bkdocs b where b.ref = new.bkdoc into :balsum;
      balsum = balsum + new.debit - old.debit
        - new.credit + old.credit;
      update bkdocs b
        set sumdebit = sumdebit + new.debit - old.debit,
          sumcredit = sumcredit + new.credit - old.credit,
          baldebit = case when :balsum > 0 then :balsum else 0 end,
          balcredit = case when :balsum < 0 then -:balsum else 0 end
        where b.ref = new.bkdoc;
    end else if (bktype < 2 and oldbktype > 1) then begin  -- nastapila zmiana z pozabilansowego
      select baldebit - balcredit
        from bkdocs b where b.ref = new.bkdoc into :balsum;
      balsum = balsum + new.debit - new.credit;
      update bkdocs b
        set sumdebit = sumdebit + new.debit,
          sumcredit = sumcredit + new.credit,
          baldebit = case when :balsum > 0 then :balsum else 0 end,
          balcredit = case when :balsum < 0 then -:balsum else 0 end
        where b.ref = new.bkdoc;
    end else if (bktype > 1 and oldbktype < 2) then begin -- nastapila zmiana na pozabilansowe
      select baldebit - balcredit
        from bkdocs b where b.ref = new.bkdoc into :balsum;
      balsum = balsum - old.debit + old.credit;
      update bkdocs b
        set sumdebit = sumdebit - old.debit,
          sumcredit = sumcredit - old.credit,
          baldebit = case when :balsum > 0 then :balsum else 0 end,
          balcredit = case when :balsum < 0 then -:balsum else 0 end
        where b.ref = new.bkdoc;
    end
  end

  if ((new.settlement <> old.settlement
      or (old.settlement is not null and new.settlement is null)
      or (new.account <> old.account and old.settlement is not null) or (new.stransdate <> old.stransdate)
      ) and new.status < 1
      ) then
  begin
    execute procedure unreg_settlement(old.ref);
    delete from rozrachp where stable = old.stable and rozrachp.sref = old.sref;
  end

  if (new.status <> old.status) then
  begin
    if (new.status > 1 and old.status < 2) then
    begin
      execute procedure compute_turnovers(new.ref);
      if (new.settlement is not null and new.settlement <> '' and new.autobo = 0) then
        execute procedure reg_settlement(new.ref);
    end
    if (new.status < 2 and old.status > 1) then
      execute procedure uncompute_turnovers(new.ref);
    if (new.status = 3 and old.status < 3) then
      execute procedure compute_ebturnovers(new.ref);
    else if (new.status < 3 and old.status = 3) then
      execute procedure uncompute_ebturnovers(new.ref);
  end
  -- obsluga kont rozliczeniowych - kasowanie tabeli parowania
  if ((new.account <> old.account or coalesce(new.matchingsymbol,'') <> coalesce(old.matchingsymbol,''))
       and new.status < 1
  ) then
  begin
    select b.company, p.yearid
      from bkdocs b
        left join bkperiods p on (p.id = b.period and p.company = b.company)
      where b.ref = new.bkdoc
      into :company, :yearid;
    -- sprawdzamy tylko dla starego konta, bo dla nowego sie zalozy przy akceptacji dekretu
    execute procedure ACCOUNT_MATCHING_CHECK(old.account,:company,:yearid,old.currency) returning_values (:matchable);
    if (:matchable > 0) then
      execute procedure unreg_decreematching(new.ref);
  end
  -- obsluga kont rozliczeniowych - insert/update tabeli parowania
  if (new.status >= 2 and old.status < 2) then
  begin
    execute procedure decree_matching_check(new.ref) returning_values (:matchable);
    if (:matchable > 0) then
      execute procedure reg_decreematching(new.ref);
  end
end^
SET TERM ; ^
