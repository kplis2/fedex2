--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FSCLRACCP_BI_ORDER FOR FSCLRACCP                      
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable maxnum integer;
begin
  new.ord = 1;
  select max(number) from fsclraccp where fsclracch = new.fsclracch
    into :maxnum;
  if (new.number is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update fsclraccp set ord = 0, number = number + 1
      where number >= new.number and fsclracch = new.fsclracch;
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
