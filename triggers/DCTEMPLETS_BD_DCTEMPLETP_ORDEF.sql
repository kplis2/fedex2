--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DCTEMPLETS_BD_DCTEMPLETP_ORDEF FOR DCTEMPLETS                     
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  update dctempletpos set ord = 0 where dctemplet = old.ref;
end^
SET TERM ; ^
