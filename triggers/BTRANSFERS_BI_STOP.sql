--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BTRANSFERS_BI_STOP FOR BTRANSFERS                     
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable stop integer;
begin
  if (new.slodef = 6) then
  begin
    select stoppay
      from dostawcy
      where ref = new.slopoz
    into :stop;
    if (stop = 1) then
      exception stoppay;
  end
end^
SET TERM ; ^
