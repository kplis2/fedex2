--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWORKEDHOURS_BIU_BLANK FOR EWORKEDHOURS                   
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
begin
  if (new.hdate is null) then
    new.hdate = current_date;
  else if (new.hdate is distinct from old.hdate) then
    new.hdate = cast(new.hdate as date);  --BS81781
  if (new.dictpos = 0)
    then new.dictpos = null;
end^
SET TERM ; ^
