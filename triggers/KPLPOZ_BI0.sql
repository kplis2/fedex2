--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KPLPOZ_BI0 FOR KPLPOZ                         
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable typ smallint;
declare variable cennik integer;
begin
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  else if(new.ktm is not null and new.wersja is not null) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if(new.ilosc is null) then new.ilosc = 0;
  if(new.cenamag is null) then begin
    select KPLNAG.typmag from KPLNAG where REF=new.nagkpl into :typ;
    execute procedure KOMPLET_GET_CENAMAG(new.ktm, new.wersjaref,:typ) returning_values new.cenamag;
  end
  if(new.cenaspr is null) then begin
    select CENNIK from KPLNAG where REF=new.nagkpl into :cennik;
    select CENNIK.cenanet from CENNIK where CENNIK=:cennik and KTM = new.ktm and WERSJA = new.wersja into new.cenaspr;
    if(new.cenaspr is null) then
      select CENNIK.cenanet from CENNIK where CENNIK=:cennik and KTM = new.ktm and WERSJA = 0 into new.cenaspr;
  end
  if(new.cenamag is null) then new.cenamag = 0;
  if(New.cenaspr is null) then new.cenaspr = 0;
  new.wartmag = new.cenamag * new.ilosc;
  new.wartspr = new.cenaspr * new.ilosc;

end^
SET TERM ; ^
