--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER REZZASOBY_BI_REF FOR REZZASOBY                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('REZZASOBY')
      returning_values new.REF;
end^
SET TERM ; ^
