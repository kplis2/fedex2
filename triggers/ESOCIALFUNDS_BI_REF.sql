--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ESOCIALFUNDS_BI_REF FOR ESOCIALFUNDS                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (coalesce(new.ref,0) = 0) then
    execute procedure GEN_REF('ESOCIALFUNDS')
      returning_values new.REF;
end^
SET TERM ; ^
