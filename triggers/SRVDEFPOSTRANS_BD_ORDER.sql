--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVDEFPOSTRANS_BD_ORDER FOR SRVDEFPOSTRANS                 
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then exit;
  update srvdefpostrans set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and srvdefpos = old.srvdefpos;
end^
SET TERM ; ^
