--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTIFSHEETSTATES_AU_ORDER FOR NOTIFSHEETSTATES               
  ACTIVE AFTER UPDATE POSITION 20 
as
begin
  if (old.number <> new.number) then
  begin
    update notifsheetstates n set n.number = n.number + 1
      where n.number >= new.number and n.ref <> new.ref;
    update notifsheetstates n set n.number = n.number - 1
      where n.number > old.number and n.ref <> new.ref;
  end
end^
SET TERM ; ^
