--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWS_DOKUMNAG_AU_DOCWAITING FOR DOKUMNAG                       
  ACTIVE AFTER UPDATE POSITION 16 
AS
declare variable docposid integer;
declare variable vers integer;
declare variable quantity numeric(14,4);
declare variable mwsordwaiting smallint;
declare variable rozonakc smallint;
declare variable rozonakcs varchar(100);
declare variable genmwsordsafter smallint;
declare variable ilosc ilosci_mag;
declare variable iloscl ilosci_mag;
declare variable ilosconmwsacts ilosci_mag;
begin
  -- jezeli sie zmieni akceptacja albo dyspozycja
  if((new.akcept <> old.akcept or new.mwsdisposition <> old.mwsdisposition) and (new.mwsdoc = 1 or new.mwsmag is not null)) then
  begin
    select mwsordwaiting from defdokummag where typ = new.typ and magazyn = new.magazyn
      into mwsordwaiting;
    if (mwsordwaiting is null) then mwsordwaiting = 0;
    if (new.mwsdocreal = 2 or new.mwsdocreal = 4 or mwsordwaiting = 1 or new.mwsdisposition = 1) then
    begin
      -- wyliczyc dla kazdej pozycji pole genmwsordsafter
      for
        select p.ref, p.wersjaref,
            case when (new.mwsdisposition = 1 or coalesce(t.altposmode,0) = 1) then p.ilosc else p.iloscl end - p.ilosconmwsacts,
            p.ilosc, p.iloscl, p.ilosconmwsacts
          from dokumpoz p
            join towary t on (p.ktm = t.ktm)
          where p.dokument = new.ref
          into docposid, vers, quantity, :ilosc, :iloscl, :ilosconmwsacts
      do begin
        execute procedure MWS_SET_GENMWSORDSAFTER(new.ref, :docposid,
            case when new.mwsdoc = 1 or new.mwsmag is not null then 1 else 0 end, new.mwsdisposition, new.akcept,
            :ilosc, :iloscl, :ilosconmwsacts)
          returning_values :genmwsordsafter;

        update dokumpoz set genmwsordsafter = :genmwsordsafter
          where ref = :docposid;
      end
    end
  end
end^
SET TERM ; ^
