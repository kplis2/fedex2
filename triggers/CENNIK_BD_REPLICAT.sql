--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CENNIK_BD_REPLICAT FOR CENNIK                         
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable aktuoddzial varchar(255);
declare variable oddzial varchar(255);
declare variable cnt integer;
begin
  if(old.token = 0) then begin
    select count(*) from WERSJE where REF=old.wersjaref into :cnt;
    if(:cnt > 0) then begin
      select oddzial from DEFCENNIk where  REF=old.cennik into :oddzial;
      execute procedure GETCONFIG('AKTUODDZIAL') returning_values :aktuoddzial;
      if(:aktuoddzial <> :oddzial) then
        exception RP_CHANGERECORDTOKENERR;
    end
  end
end^
SET TERM ; ^
