--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_BU_GROUPBYSYMBS FOR MWSACTS                        
  ACTIVE BEFORE UPDATE POSITION 1 
AS
begin
  if (new.vers <> old.vers or new.vers is not null and old.vers is null) then
    execute procedure mws_mwsacts_groupby(new.vers)
      returning_values(new.keya, new.keyb, new.keyc, new.keyd);
  if (new.keya is null or (new.vers is null and old.vers is not null)) then new.keya = '';
  if (new.keyb is null or (new.vers is null and old.vers is not null)) then new.keyb = '';
  if (new.keyc is null or (new.vers is null and old.vers is not null)) then new.keyc = '';
  if (new.keyd is null or (new.vers is null and old.vers is not null)) then new.keyd = '';
end^
SET TERM ; ^
