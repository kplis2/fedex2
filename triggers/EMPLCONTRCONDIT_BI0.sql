--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRCONDIT_BI0 FOR EMPLCONTRCONDIT                
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
   if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('EMPLCONTRCONDIT')
      returning_values new.ref;
end^
SET TERM ; ^
