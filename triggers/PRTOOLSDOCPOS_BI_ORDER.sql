--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSDOCPOS_BI_ORDER FOR PRTOOLSDOCPOS                  
  ACTIVE BEFORE INSERT POSITION 1 
as
  declare variable maxnum integer;
begin
  new.ord = 1;
  select max(number)
    from PRTOOLSDOCPOS
    where PRTOOLSDOC = new.PRTOOLSDOC
    into :maxnum;
  if (new.number is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update PRTOOLSDOCPOS set ord = 0, number = number + 1
      where number >= new.number and PRTOOLSDOC = new.PRTOOLSDOC;
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
