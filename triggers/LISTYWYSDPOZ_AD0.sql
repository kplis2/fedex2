--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDPOZ_AD0 FOR LISTYWYSDPOZ                   
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable kontrolapak smallint;
begin
  if(old.doktyp='M') then
    update DOKUMPOZ set ILOSCLWYS = ILOSCLWYS - old.ilosc where ref = old.dokpoz;
  if(old.doktyp='Z') then
    execute procedure NAGZAM_CHECKWYS(old.dokref);

  select kontrolapak from listywysd where ref = old.dokument
  into :kontrolapak;
  if (:kontrolapak = 0) then
  begin
    --if(old.waga > 0) then
      --update LISTYWYSD set WAGA = WAGA - old.waga where ref=old.dokument;  KPI XXX138061
    if(old.objetosc > 0) then
      update LISTYWYSD set OBJETOSC = OBJETOSC - old.objetosc where ref=old.dokument;
  end
  execute procedure LISTYWYSD_OBLICZSYMBOL(old.dokument);
end^
SET TERM ; ^
