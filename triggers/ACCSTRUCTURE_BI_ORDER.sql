--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTURE_BI_ORDER FOR ACCSTRUCTURE                   
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable maxnum integer;
begin
  new.ord = 1;
  select max(nlevel) from accstructure where bkaccount = new.bkaccount
    into :maxnum;
  if (new.nlevel is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.nlevel = maxnum + 1;
  end else if (new.nlevel <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update accstructure set ord = 0, nlevel = nlevel + 1
      where nlevel >= new.nlevel and bkaccount = new.bkaccount;
  end else if (new.nlevel > maxnum) then
    new.nlevel = maxnum + 1;
end^
SET TERM ; ^
