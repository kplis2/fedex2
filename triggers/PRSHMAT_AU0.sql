--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHMAT_AU0 FOR PRSHMAT                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable test integer;
declare variable rodzaj char(1);
begin
  if((new.subsheet <> old.subsheet) or (new.subsheet is not null and old.subsheet is null)) then begin
    execute procedure prsheets_no_matloop_up(new.subsheet,new.sheet,50)
    returning_values :test;
  end
  if((new.econdition <> old.econdition) or (new.econdition is not null and old.econdition is null) or (new.econdition is null and old.econdition is not null)
    or (new.enetquantity <> old.enetquantity) or (new.enetquantity is not null and old.enetquantity is null) or (new.enetquantity is null and old.enetquantity is not null)
    or (new.egrossquantity <> old.egrossquantity) or (new.egrossquantity is not null and old.egrossquantity is null) or (new.egrossquantity is null and old.egrossquantity is not null)
  ) then begin
    rodzaj = '';
    if(new.mattype = 0) then rodzaj = 'S';
    if(new.mattype = 1) then rodzaj = 'P';
    if(new.mattype = 2) then rodzaj = 'O';
    execute procedure prsheets_calculate(new.sheet,'',:rodzaj);
  end
end^
SET TERM ; ^
