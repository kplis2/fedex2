--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STKRTAB_BI_REF FOR STKRTAB                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('STKRTAB')
      returning_values new.REF;
end^
SET TERM ; ^
