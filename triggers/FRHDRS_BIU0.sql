--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRHDRS_BIU0 FOR FRHDRS                         
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
  declare variable frpsns_count integer;
  declare variable ref integer;
  declare variable symb varchar(15);
begin
  if (new.autorefilled is null) then new.autorefilled = 0;
  if (coalesce(old.status, 0) <> coalesce(new.status, 0) and coalesce(new.status, 0) = 1) then
  begin
    for select ref, symbol from frpsns where frhdr = new.symbol and algorithm = 3 into :ref, :symb
    do begin
      select count(*) from frpsnsalg where frpsn = :ref into :frpsns_count;
      if(:frpsns_count = 0) then exception FRHDRS_COPY_EXPT 'Brak zdefiniowanego wyrażenia algorytmu dla wiersza '||:symb;
    end
    ref = null;
    for select ref, symbol from frpsns where frhdr = new.symbol and algorithm = 2 into :ref, :symb
    do begin
      select count(*) from frsumpsns where frpsn = :ref and frversion = 0 into :frpsns_count;
      if(:frpsns_count = 0) then exception FRHDRS_COPY_EXPT 'Brak zdefiniowanych składników sumy dla wiersza '||:symb;
    end
  end
end^
SET TERM ; ^
