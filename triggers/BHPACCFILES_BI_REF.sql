--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BHPACCFILES_BI_REF FOR BHPACCFILES                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
    if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('BHPACCFILES')
      returning_values new.REF;
end^
SET TERM ; ^
