--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_BI FOR DOKUMPOZ                       
  ACTIVE BEFORE INSERT POSITION 1 
AS
declare variable usluga smallint;
declare variable aktywny smallint;
declare variable towakt varchar(255);
declare variable mag char(3);
declare variable mag_typ char(1);
declare variable wydania integer;
declare variable koryg smallint;
declare variable zewn smallint;
declare variable typ_dok char(3);
declare variable dostawa integer;
declare variable cenamag numeric(14,4);
declare variable fifo char(1);
declare variable przelicz numeric(14,4);
declare variable przconst smallint;
declare variable zamowienie integer;
declare variable opktryb smallint;
declare variable local smallint;
declare variable walutowy smallint;
declare variable kurs numeric(14,4);
declare variable pozzamref integer;
declare variable dniwaznbufc varchar(255);
declare variable dniwaznbufi integer;
declare variable dniwazn integer;
declare variable zrodlo smallint;
declare variable jedn integer;
declare variable bn char(1);
declare variable vat numeric(14,4);
declare variable klient integer;
declare variable status smallint;
declare variable rabatnag numeric(14,2);
declare variable sprec varchar(20);
BEGIN
         -- exception test_break  new.stanopkroz;
  execute procedure CHECK_LOCAL('DOKUMPOZ',new.ref) returning_values :local;
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  else if(new.ktm is not null and new.wersja is not null) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  if(new.dostawa is NULL)then new.dostawa = 0;
  if(new.cena is NULL) then new.cena = 0;
  if(new.cenawal is NULL) then new.cenawal = 0;
  if(new.cena_koszt is null) then new.cena_koszt = 0;
  if(new.cenasr_koszt is null) then new.cenasr_koszt = 0;
  if(new.wart_koszt is null) then new.wart_koszt = 0;
  if(new.foc is NULL) then new.foc = 0;
  if(new.roz_blok is null) then new.roz_blok = 0;
  if(new.iloscroz is null) then new.iloscroz = 0;
  if(new.iloscl is null) then new.iloscl = 0;
  if(new.ilosclwys is null) then new.ilosclwys = 0;
  if(new.pwartosc is null) then new.pwartosc = 0;
  if(new.flagi is null) then new.flagi = '';
  if (coalesce(new.fake,0)=1) then
  begin
    new.opk = 1;
    update dokumnag dn
      set dn.altpoz = 1
      where dn.ref = new.dokument;
  end
  select AKT, USLUGA, DNIWAZN  from TOWARY where KTM = new.KTM into :aktywny, :usluga, :dniwazn;
  execute procedure GETCONFIG('TOWARAKTYWNY') returning_values :towakt;
  if(:local = 1) then begin
     /* sprawdzenie, czy dla magazynu partiowego dokument przychodowy dostawa zgodna z naglowkiem */
    select MAGAZYN, typ, dostawa, zamowienie, klient, BN, RABAT from DOKUMNAG where ref = new.dokument into :mag, :typ_dok, :dostawa, :zamowienie, :klient, :bn, :rabatnag;
    if(:rabatnag is null) then rabatnag = 0;
    select WYDANIA, OPAK, ZEWN, KORYG from DEFDOKUM where SYMBOL=:typ_dok
      into :wydania, opktryb, :zewn, :koryg;
    new.wydania = :wydania;
    if(:bn is null or :bn=' ') then bn = 'N';
    if(new.prec is null or new.prec = 0) then begin
      if(new.wydania=0) then execute procedure GETCONFIG('ZAKPREC') returning_values :sprec;
      else execute procedure GETCONFIG('SPRPREC') returning_values :sprec;
      if(:sprec is not null and :sprec<>'') then new.prec = cast(:sprec as integer);
    end
    if(:towakt='1' and :aktywny<>1 and :koryg=0) then
      exception TOWAR_NIEAKTYWNY 'Wybrany towar '||new.ktm||' jest nieaktywny.';
    if(:zamowienie is not null) then
      new.fromzam = zamowienie;
    select TYP, FIFO from DEFMAGAZ where SYMBOL=:mag into :mag_typ, :fifo;
    if(:wydania = 0 and :mag_typ = 'P' and((new.dostawa is null)or (new.dostawa = 0))) then begin
      new.dostawa = :dostawa;
    end
    /*jesli powiązany z pozycją zamówienia, to podczytanie symbolu dostawy do pozycji*/
    dostawa = 0;
    cenamag = 0;
    if(new.frompozzam > 0) then begin
      select dostawamag,cenamag from POZZAM where ref=new.frompozzam into :dostawa,:cenamag;
      if(:dostawa is null) then dostawa = 0;
      if(:cenamag is null) then cenamag = 0;
      if(:dostawa>0 and new.cena=0) then new.cena = :cenamag;
    end
    if(:dostawa = 0 and new.frompozfak > 0) then begin
      pozzamref = NULL;
      cenamag = 0;
      select FROMPOZZAM from POZFAK where ref=new.frompozfak into :pozzamref;
      if(:pozzamref is not null) then select dostawamag,cenamag from POZZAM where ref=:pozzamref into :dostawa,:cenamag;
      if(:dostawa is null) then dostawa = 0;
      if(:cenamag is null) then cenamag = 0;
      if(:dostawa>0 and new.cena=0) then new.cena = :cenamag;
    end
    if(:dostawa = 0 and new.frompozfak > 0) then begin
      cenamag = 0;
      select dostawa,cenamag from POZFAK where ref=new.frompozfak into :dostawa,:cenamag;
      if(:dostawa is null) then dostawa = 0;
      if(:cenamag is null) then cenamag = 0;
      if(:dostawa>0 and new.cena=0) then new.cena = :cenamag;
    end
    if(:dostawa > 0) then
      new.dostawa = :dostawa;
    /*nadanie numeru*/
    if(new.numer is null) then begin
      select max(numer) from DOKUMPOZ where DOKUMENT = new.dokument into new.numer;
      if(new.numer is null) then new.numer = 1;
      else new.numer = new.numer + 1;
    end
  end
  select ref from TOWJEDN where KTM = new.KTM and GLOWNA = 1 into :jedn;
  if(new.ilosc is null) then new.ilosc = 0;
  if(new.ilosco is null) then new.ilosco = 0;
  if(new.jedno is null) then new.jedno = :jedn;
  select PRZELICZ, CONST from TOWJEDN where REF=new.jedno into :przelicz, :przconst;
  if(:local = 1) then begin
    if(:przconst = 1 or (new.ilosco = 0)) then begin
      if(:przelicz > 0 ) then
        new.ilosco = new.ilosc / :przelicz;
    end
    select WALUTOWY, KURS from DOKUMNAG where ref = new.dokument into :walutowy, :kurs;
    if(new.prec<>4) then new.cenawal = cast(new.cenawal as numeric(14,2));
    if(:walutowy=1 and :wydania = 0) then begin  -- DD
      new.cena = new.cenawal*:kurs;
    end
    new.cenasr = new.cena;
    if(:usluga = 1) then begin
      new.cena = 0;
      new.dostawa = null;
      new.cenasr = 0;
    end
    new.wartosc = new.ilosc * new.cenasr;
    new.wartoscwal = new.ilosc * new.cenawal;
    if(:opktryb > 0) then begin
      if(new.opk is null and :usluga = 3) then new.opk = 1;
      if(new.opk is null and :usluga <> 3) then new.opk = 0;
      if(new.opk = 1 and new.cenacen = 0) then new.opk = 2;
    end else if (new.opk <> 1 or new.opk is null) then  -- PR 9337 - mozliwosc dodania opakowan do dokumentu, ktory nie robi obrotu opakowaniami do celów promocji
      new.opk = 0;
    if (new.opk is null) then exception DOKUMPOZ_OPK_NOT_SET;
  end
--  if(:usluga = 1 and new.opk = 0) then
--    exception DOKUMPOZ_USLUGA;
  if(:local = 1) then begin
    select TOWJEDN.waga*new.ilosco, towjedn.vol*new.ilosco from TOWJEDN where ref=new.jedno into new.waga, new.objetosc;
  end
  -- pobierz stawke VAT
  if(new.gr_vat is null and new.frompozfak is not null) then
    select pozfak.gr_vat from pozfak where pozfak.ref = new.frompozfak into new.gr_vat;
  if(new.gr_vat is null and new.frompozzam is not null) then
    select pozzam.gr_vat from pozzam where pozzam.ref = new.frompozzam into new.gr_vat;
  if(new.gr_vat is null) then
    select wersje.vat from wersje where wersje.ref = new.wersjaref into new.gr_vat;
  if(new.gr_vat is null) then
    select towary.vat from towary where towary.ktm = new.ktm into new.gr_vat;
  select coalesce(vat.stawka,0.0) from vat where vat.grupa = new.gr_vat into :vat;

  -- MSt: BS70028: Ceny sprzedazy zerowe dla fejkow
  if (new.fake = 1) then
  begin
    new.cenacen = 0;
    new.cenanet = 0;
    new.cenabru = 0;
    new.cenawal = 0;
    new.wartsnetto = 0;
    new.wartsbrutto = 0;
  end
  -- MSt: Koniec

  -- obliczenie ceny sprzedazy, ale tylko gdy wskazano refcennik
  if(new.refcennik is not null and new.cenacen is null) then begin
    -- pobierz cene z cennika
    execute procedure POBIERZ_CENE(new.refcennik,new.wersjaref, :jedn, :bn, new.cenacen, new.walcen, new.prec, new.cena, new.dostawa, :klient, 'M', new.dokument)
      returning_values new.cenacen, new.walcen, :jedn, :status, new.prec;
  end
  if(new.cenacen is null) then new.cenacen = 0;
  if(new.rabat is null) then new.rabat = 0;
  execute procedure OBLICZ_CENE(:bn, new.ilosc, new.cenacen, new.walcen, new.rabat + :rabatnag, :vat, new.prec)
    returning_values new.cenanet, new.cenabru, new.wartsnetto, new.wartsbrutto;
  if(new.obliczcenzak is null) then new.obliczcenzak = 0;
  if(new.obliczcenkoszt is null) then new.obliczcenkoszt = 0;
  if(new.obliczcenfab is null) then new.obliczcenfab = 0;
  if(new.dostawa is null and :dniwazn>0 and :wydania = 0 and
    :opktryb = 0 and :zewn = 1 and koryg = 0) then begin
      execute procedure GETCONFIG('DNIWAZNBUF') returning_values :dniwaznbufc;
      select zrodlo from DOKUMNAG where ref = new.dokument into :zrodlo;
      if((zrodlo = 1 or zrodlo = 0) and :dniwaznbufc<>'' and :dniwaznbufc<>'-1') then begin
          if(new.datawazn is null) then exception DOKUMPOZ_BAD_EXPIRYDATE 'Nie podano daty ważności dla: '|| new.ktm;
          dniwaznbufi = cast(:dniwaznbufc as integer);
          if(new.datawazn < current_date) then
            exception DOKUMPOZ_BAD_EXPIRYDATE 'Błędna data ważności dla: ' || new.ktm || '. Data wcześniejsza od dzisiejszej.';
          if(new.datawazn > current_date + :dniwazn + :dniwaznbufi) then
            exception DOKUMPOZ_BAD_EXPIRYDATE   'Błędna data ważności dla: '|| new.ktm || '. Towar ma ' || :dniwazn || ' dni przydatności. ';
    end
  end
    if (new.iloscdorozb is null) then new.iloscdorozb = 0;
  select defdokum.koryg,defdokum.zewn
    from dokumnag
    left join defdokum on (defdokum.symbol = dokumnag.typ)
    where dokumnag.ref = new.dokument
    into :koryg,:zewn;
  if(:koryg = 1 and :zewn = 1) then begin
    select dokumpoz.cenacen, dokumpoz.cenanet, dokumpoz.cenabru, dokumpoz.rabat, dokumpoz.rabattab
      from dokumpoz where dokumpoz.ref = new.kortopoz
      into new.cenacen, new.cenanet,  new.cenabru, new.rabat,  new.rabattab;
    new.wartsnetto = new.ilosc * new.cenanet;
    new.wartsbrutto = new.ilosc * new.cenabru;
  end
  if(new.krajpochodzenia = '') then new.krajpochodzenia = null;
  if(new.ilosc < 0)then
    exception DOKUMPOZ_EXPT 'Ilość towaru na pozycji dok. mag. nie może być ujemna!';
  if(new.ilosc = 0)then
    exception DOKUMPOZ_EXPT 'Ilość towaru na pozycji dok. mag. nie może być zerowa!';

END^
SET TERM ; ^
