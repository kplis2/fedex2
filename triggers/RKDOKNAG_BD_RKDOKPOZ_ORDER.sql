--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_BD_RKDOKPOZ_ORDER FOR RKDOKNAG                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  update rkdokpoz set ord = 0 where dokument = old.ref;
  delete from RKDOKPOZ where dokument = old.ref;
end^
SET TERM ; ^
