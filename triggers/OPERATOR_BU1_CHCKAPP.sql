--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OPERATOR_BU1_CHCKAPP FOR OPERATOR                       
  ACTIVE BEFORE UPDATE POSITION 1 
AS
begin
  if(coalesce(new.applications,'') = '' and new.applications <> old.applications)
   then exception universal 'Brak wskazania aplikacji do których operator ma dostep';
end^
SET TERM ; ^
