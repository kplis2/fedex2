--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNOT_BU0 FOR DOKUMNOT                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable chronologia smallint;
declare variable numpoakcept smallint;
declare variable numberg varchar(20);
declare variable cnt integer;
declare variable wydania smallint;
declare variable autosymbol smallint;
declare variable typmag char(1);
declare variable sblokadadelay varchar(255);
declare variable blokadadelay integer;
declare variable local smallint;
declare variable wysstan char(1);
declare variable miedzyzaw smallint;
declare variable aktulocat integer;
declare variable location integer;
declare variable BLOCKREF integer;
begin
 if(new.mag2 is null) then new.mag2 = '';
 if(new.akcept is null) then new.akcept = 0;
 if(new.akcept = 1 and old.akcept = 0) then
   new.dataakc = current_time;
 else if(new.akcept = 0 and old.akcept = 1) then begin
   new.dataakc = null;
   new.wasdeakcept = 1;
 end else if(new.akcept = 1 and old.akcept = 1 and (new.wartosc <> old.wartosc)) then begin
   new.wasdeakcept = 1;
 end

 if(new.blokada is null) then new.blokada = 0;
 if(new.operator = 0) then new.operator = NULL;
 if(new.magazyn <> old.magazyn) then
   select ODDZIAL from DEFMAGAZ where SYMBOL = new.magazyn into new.oddzial;
 if(new.akcept = 1 and old.akcept <> 1) then begin
   execute procedure getconfig('AKTULOCAT') returning_values :aktulocat;
   select LOCATION from oddzialy where ODDZIAL=new.oddzial into :location;
   if(:aktulocat <> :location) then
     new.akcept = old.akcept;
 end
 if(new.akcept <> old.akcept and new.akcept <> 8 and new.akcept <> 7)then begin
  /*zmiana stanu akceptacji walasciwa*/
  execute procedure GETCONFIG('SIDFK_BLOKADA') returning_values :sblokadadelay;
  if(new.blokada > 0) then begin
     if(:sblokadadelay <> '') then
       blokadadelay = cast(:sblokadadelay as integer);
     else blokadadelay = 0;
     if(bin_and(new.blokada,1)>0 and bin_and(old.blokada,1)>0) then exception DOKUMNAG_BLOKADAHAND;/*akceptacja przy istniejącej blokadzie*/
     if(bin_and(new.blokada,4)>0 and current_date >= new.data + :blokadadelay) then exception DOKUMNAG_ZAKSIEGOWANY;
     if(bin_and(new.blokada,4)>0) then new.blokada = bin_and(new.blokada,3);/*zniesienie blokady ksiegowej, jesli mozna*/
   end
   select chronologia, numpoakcept from DEFMAGAZ where symbol=new.magazyn into :chronologia, :numpoakcept;
   if(:chronologia is null) then chronologia = 0;
   if(:numpoakcept is null) then numpoakcept = 0;
   /*kontrola chronologii - poza wycofaniem do poprawy*/
   if(:chronologia > 0 and (new.blokada <> 2 and old.blokada <> 2)) then begin
      cnt = null;
      select count(*) from DOKUMNOT where MAGAZYN=new.magazyn and DATA>=(new.data+:chronologia) and akcept = 1 into :cnt;
      if(:cnt is null) then cnt = 0;
      if(:cnt > 0) then
        exception DOKUMNG_AKCEP_POZNIEJ;
   end
   select NUMBER_NOTGEN from DEFMAGAZ where  symbol = new.magazyn into :numberg;
   execute procedure CHECK_LOCAL('DOKUMNOT',new.ref) returning_values :local;
   if(:numberg is not null and :local = 1) then
     if(:numpoakcept = 1 and :numberg is not null) then begin
       if(new.akcept = 1) then begin
        /*nadanie numeratora nowego zwolnienie tymczasowego*/
          execute procedure Get_NUMBER(:numberg, new.magazyn, new.typ, new.data, 0, new.ref) returning_values new.numer, new.symbol;
       end else begin
        /*zwolnienie numeratora*/
         if(old.numer <> 0) then begin
             execute procedure free_number(:numberg, old.magazyn, old.typ, old.data, old.numer, 0) returning_values :blockref;
         end
        /*nadanie numeratora tymczasowego*/
         new.numer = -1;
         new.symbol = 'TYM/'||new.ref;
       end
     end
 end
end^
SET TERM ; ^
