--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMELEMENTS_BD_ORDER FOR PMELEMENTS                     
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 5) then exit;
  update pmelements set ord = 1, number = number - 1
    where ref <> old.ref and number > old.number and pmplan = old.pmplan;
end^
SET TERM ; ^
