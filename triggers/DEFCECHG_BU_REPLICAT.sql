--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHG_BU_REPLICAT FOR DEFCECHG                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.REF<>old.REF
      or new.NAZWA<>old.NAZWA or (new.NAZWA is NULL and old.nazwa is not null) 
      or (new.NAZWA is not NULL and old.nazwa is null)) then
   waschange = 1;

  execute procedure rp_trigger_bu('DEFCECHG',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
