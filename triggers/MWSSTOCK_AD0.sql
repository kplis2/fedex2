--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTOCK_AD0 FOR MWSSTOCK                       
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  if (not exists(select ref from mwsstock where stancen = old.stancen)) then
    if (not exists(select ref from mwsactdets where stancen = old.stancen)) then
      if (not exists(select ref from mwsacts where stancen = old.stancen and status < 5 and status > 0)) then
    -- mozemy skasowac stan na lokacji tylko gdy nic na nim nie ma i nie ma operacji na ten stan
        delete from stanycen where ref = old.stancen;
  if (old.mwsconstloc is not null) then
    update mwsconstlocs set act = -1 where ref = old.mwsconstloc and act = 100;
  if (old.mwsconstloc is not null) then
  begin
    execute procedure MWS_CALC_FREELOCS (old.mwsconstloc,1);
    if (exists (select ref from mwspallocs where ref = old.mwspalloc and segtype = 2)) then
      execute procedure mws_check_mwsconstlocsizes(old.mwsconstloc, 0);
  end
end^
SET TERM ; ^
