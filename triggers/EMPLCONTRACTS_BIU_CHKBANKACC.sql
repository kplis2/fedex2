--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRACTS_BIU_CHKBANKACC FOR EMPLCONTRACTS                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 2 
as
declare variable empl_person type of column employees.person;
declare variable bacc_person type of column bankaccounts.dictpos;
begin
--Kontrola przypisan konta bankowego (BS106300)

  if (new.bankaccount is not null and (inserting
     or new.bankaccount is distinct from old.bankaccount
     or new.employee is distinct from old.employee)
  ) then begin

    select person from employees
      where ref = new.employee
      into :empl_person;

    select dictpos from bankaccounts
      where ref = new.bankaccount
      into :bacc_person;

    if (bacc_person <> empl_person) then
      exception universal 'Wykryto próbę przypisania rachunku bankowego należącego do innej osoby!';
  end
end^
SET TERM ; ^
