--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZSAD_BD_ORDER FOR POZSAD                         
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update pozsad set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and faktura = old.faktura;
end^
SET TERM ; ^
