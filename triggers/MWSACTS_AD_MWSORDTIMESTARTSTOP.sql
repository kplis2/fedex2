--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_AD_MWSORDTIMESTARTSTOP FOR MWSACTS                        
  ACTIVE AFTER DELETE POSITION 2 
AS
declare variable number integer;
declare variable timestart timestamp;
declare variable timestop timestamp;
begin
  -- okreslenie deklarowanego czasu rozpoczecia zlecenia po skasowaniu pierwszej operacji
  if (not exists(select ref from mwsacts where mwsord = old.mwsord
      and number < old.number and status <> 6)) then
  begin
    select min(number)
      from mwsacts
      where mwsord = old.mwsord and number < old.number and status <> 6
      into number;
    select timestartdecl from mwsacts where mwsord = old.mwsord and number = :number and status <> 6
      into :timestart;
    update mwsords set timestartdecl = :timestart where ref = old.mwsord;
  end
  -- okreslecenie deklarowanego czasu zakonczenia zlecenia po skasowaniu ostatniej operacji
  if (not exists(select ref from mwsacts where mwsord = old.mwsord
      and number >= old.number and status <> 6)) then
  begin
    select max(number)
      from mwsacts
      where mwsord = old.mwsord and number >= old.number and status <> 6
      into number;
    select timestopdecl from mwsacts where mwsord = old.mwsord and number = :number and status <> 6
      into :timestop;
    update mwsords set timestopdecl = :timestop where ref = old.mwsord;
  end
  -- NIE LICZYMY RZECZYWISTEGO NA DELETE BO SIE NIE DA SKASOWAC OPERACJI POTWIERDZONEJ !!!!
end^
SET TERM ; ^
