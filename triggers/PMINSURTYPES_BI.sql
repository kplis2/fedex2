--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMINSURTYPES_BI FOR PMINSURTYPES                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF = 0) then
    execute procedure GEN_REF('PMINSURTYPES') returning_values new.REF;
end^
SET TERM ; ^
