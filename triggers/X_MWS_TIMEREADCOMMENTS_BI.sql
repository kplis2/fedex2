--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWS_TIMEREADCOMMENTS_BI FOR X_MWS_TIMEREADCOMMENTS         
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_x_mws_timereadcomments,1);
end^
SET TERM ; ^
