--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHOPERSECONTRDEFS_BI0 FOR PRSHOPERSECONTRDEFS            
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.sheet is null and new.prshoper is not null) then
    select sheet from prshopers where ref = new.prshoper into new.sheet;
end^
SET TERM ; ^
