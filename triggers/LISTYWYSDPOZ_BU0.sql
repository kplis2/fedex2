--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDPOZ_BU0 FOR LISTYWYSDPOZ                   
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable jedn integer;
declare variable przelicz numeric(14,4);
declare variable const integer;
begin
  if(new.ilosco <> old.ilosco) then begin
    select TOWJEDN.REF
      from DOKUMPOZ
      join TOWJEDN on (DOKUMPOZ.KTM=TOWJEDN.KTM and TOWJEDN.glowna=1)
      where DOKUMPOZ.REF=new.dokpoz
      into :jedn;
    if(:jedn <> new.jedno) then begin
      select przelicz, const from TOWJEDN where ref=new.jedno into :przelicz, :const;
      if(:przelicz is null) then przelicz = 0;
      if(:przelicz = 0) then przelicz = 1;
      if(:const = 1) then
        new.ilosc = new.ilosco/ :przelicz;
      /*else zalozenie, ze interfejs to juz przeliczyl poprawnie*/
    end else
      new.ilosc = new.ilosco;
  end
  if(new.ilosco <> old.ilosco) then begin
    select WAGA, VOL from TOWJEDN where ref=new.jedno into new.waga, new.objetosc;
    new.waga = new.waga * new.ilosco;
    new.objetosc = new.objetosc * new.ilosco;
  end
  if(new.waga is null) then new.waga = 0;
  if(new.objetosc is null) then new.objetosc = 0;
  if(new.jedno<>old.jedno or (old.jedno is null and new.jedno is not null and new.jedno>0)) then
    select JEDN from TOWJEDN where ref=new.jedno into new.miarao;
  if(new.spedjedn='') then new.spedjedn = null;
  if(new.zlecjedn='') then new.zlecjedn = null;
  --XXX JO:
  if (new.ilosc = new.iloscspk) then
    new.x_spakowano = 1;
end^
SET TERM ; ^
