--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDOPERS_BI_NORM FOR PRSCHEDOPERS                   
  ACTIVE BEFORE INSERT POSITION 2 
as
begin
  if (new.shoper is not null) then
    select norm
      from xk_prshopernorm(new.shoper)
      into new.propernorm;
end^
SET TERM ; ^
