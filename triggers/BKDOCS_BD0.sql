--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_BD0 FOR BKDOCS                         
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable ref integer;
declare variable account varchar(20);
begin
  if (old.status <> 0) then begin
    if (old.status = 3) then
      exception BKDOCS_ZAKSIEGOWANY;
    else
      exception BKDOCS_BEFORE_DELETE;
  end

  if(exists(select first 1 1 from bkdocs where otable = 'BKDOCS' and oref = old.ref)) then
    exception BKDOCS_STORNO_EXISTS;

  update decrees set ssallowdelete = 1 where bkdoc = old.ref;
  for select ref from decrees
    where decrees.bkdoc = old.ref
    order by decrees.number desc -- to odwrucenie kolejnoci powoduje że  nie zachodzi przenumerowywanie bo jest usuwany ostatni jest zdecydowanie szybciej
    into :ref
  do begin
    delete from decrees where ref = :ref;
  end
end^
SET TERM ; ^
