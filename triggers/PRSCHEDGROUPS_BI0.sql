--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGROUPS_BI0 FOR PRSCHEDGROUPS                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable prschedgroup varchar(20);
begin
  if(new.main = 1) then begin
    select symbol from prschedgroups where main = 1 and symbol <> new.symbol into :prschedgroup;
    if(prschedgroup is not null and prschedgroup <> '')
      then exception prschedules_error 'Zdefiniowano już główną grupę harmonogramów: '||:prschedgroup;
  end
end^
SET TERM ; ^
