--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLTRAININGS_BI_REF FOR EMPLTRAININGS                  
  ACTIVE BEFORE INSERT POSITION 1 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EMPLTRAININGS')
      returning_values new.REF;
end^
SET TERM ; ^
