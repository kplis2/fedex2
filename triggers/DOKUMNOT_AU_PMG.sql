--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNOT_AU_PMG FOR DOKUMNOT                       
  ACTIVE AFTER UPDATE POSITION 10 
as
declare variable rozliczpo smallint;
declare variable pmposition integer;
declare variable dokumnotp_ref integer;
declare variable dokumnotp_wartosc numeric(14,2);
begin
  if (new.akcept = 1 and old.akcept <> new.akcept) then
  begin
    select dd.rozliczpo from defdokum dd where dd.symbol = new.typ into :rozliczpo;
    if (rozliczpo = 1) then
    begin
      for select dokumpoz.pmposition, dokumnotp.ref, dokumnotp.wartosc
        from dokumnotp 
        join dokumroz on (dokumroz.ref = dokumnotp.dokumrozkor) 
        join dokumpoz on (dokumpoz.ref = dokumroz.pozycja) 
        where dokumnotp.dokument = new.ref
      into :pmposition, :dokumnotp_ref, :dokumnotp_wartosc
      do begin
        --exception universal dokumnotp_wartosc;
        update dokumnotp set pmposition = :pmposition where ref = :dokumnotp_ref;
        update pmpositions set USEDVAL = USEDVAL + :dokumnotp_wartosc where ref = :pmposition;
      end
    end
  end
  else
  if (old.akcept = 1 and old.akcept <> new.akcept) then
  begin
    select dd.rozliczpo from defdokum dd where dd.symbol = new.typ into :rozliczpo;
    if (rozliczpo = 1) then
    for select dokumnotp.pmposition, dokumnotp.ref, dokumnotp.wartosc
        from dokumnotp 
        where dokumnotp.dokument = new.ref
     into :pmposition, :dokumnotp_ref, :dokumnotp_wartosc
     do begin
         --  exception universal dokumnotp_wartosc;
        update dokumnotp set pmposition = null where ref = :dokumnotp_ref;
        update pmpositions set USEDVAL = USEDVAL - :dokumnotp_wartosc where ref = :pmposition;
     end
  end
end^
SET TERM ; ^
