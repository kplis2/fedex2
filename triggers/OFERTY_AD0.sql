--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OFERTY_AD0 FOR OFERTY                         
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable numerator varchar(40);
declare variable BLOCKREF integer;
begin
  if(old.numer is not null) then begin
    execute procedure GETCONFIG('OFERTYNUMERATOR') returning_values :numerator;
    if(:numerator is not null and :numerator<>'') then
      execute procedure FREE_NUMBER(:numerator,'','',old.data,old.numer, 0) returning_values :blockref ;
  end
end^
SET TERM ; ^
