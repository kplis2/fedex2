--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SYS_SCHEDULE_A_R_BIU_DESC FOR SYS_SCHEDULE_ACTIVITY_RULES    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if(new.type=0) then begin
    new.descript = 'codziennie';
  end
  if(new.type=1) then begin
    if(new.active_point=0) then new.descript = 'w każdą niedzielę';
    if(new.active_point=1) then new.descript = 'w każdy poniedziałek';
    if(new.active_point=2) then new.descript = 'w każdy wtorek';
    if(new.active_point=3) then new.descript = 'w każdą środę';
    if(new.active_point=4) then new.descript = 'w każdy czwartek';
    if(new.active_point=5) then new.descript = 'w każdy piątek';
    if(new.active_point=6) then new.descript = 'w każdą sobotę';
  end
  if(new.type=2) then begin
    new.descript = 'każdego '||cast(new.active_point as varchar(10))||'-go dnia miesiąca';
  end
  if(new.wholeday=1) then
    new.descript = new.descript||' przez całą dobę';
  else
    new.descript = new.descript||' od '||substring(new.start_point from 1 for 5)||' do '||substring(new.stop_point from 1 for 5);
end^
SET TERM ; ^
