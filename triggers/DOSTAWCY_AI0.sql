--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTAWCY_AI0 FOR DOSTAWCY                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable slodef integer;
declare variable cnt integer;
declare variable prawagrup varchar(80);
declare variable kontofk ANALYTIC_ID;
declare variable dl integer;
begin
 if(new.crm = 1) then begin
   select ref from SLODEF where TYP = 'DOSTAWCY' into :slodef;
   if(:slodef is null) then exception SLODEF_BEZDOSTAWCY;
   execute procedure GETCONFIG('DOMPRAWAGRUP') returning_values :prawagrup;
   select count(*) from CPODMIOTY where SLODEF = :slodef AND SLOPOZ = new.ref into :cnt;
   if(:cnt > 0) then
     update CPODMIOTY set SKROT = substring(new.ID from 1 for 40), NAZWA = new.nazwa,  --XXX ZG133796 MKD
                          TELEFON = new.telefon, FAX = new.fax, EMAIL = new.email,
                           www = new.www, NIP = new.nip, REGON = new.regon, UWAGI = new.uwagi,
                           ULICA = new.ulica, NRDOMU = new.nrdomu, NRLOKALU = new.nrlokalu,
                           miasto = new.miasto, kodp = new.kodp, poczta = new.poczta,
                           TYP = new.typ, PRAWAGRUP = :prawagrup, KONTOFK = new.kontofk, COMPANY = new.company,
                           cpowiaty = new.cpowiat
        where SLODEF = :slodef AND SLOPOZ = new.ref ;
   else
     insert into CPODMIOTY(AKTYWNY, SKROT, NAZWA, TELEFON, EMAIL, FAX, WWW,
           NIP, REGON, UWAGI, SLODEF, SLOPOZ, ULICA, NRDOMU, NRLOKALU,
           MIASTO, KODP, POCZTA, TYP, PRAWAGRUP, KONTOFK, COMPANY, cpowiaty)
     values(1, substring(new.ID from 1 for 40), new.nazwa, new.telefon, new.email, new.fax, new.www, --XXX ZG133796 MKD
               new.nip, new.regon, new.uwagi, :slodef, new.ref, new.ulica, new.nrdomu, new.nrlokalu,
               new.miasto, new.kodp, new.poczta, new.typ, :prawagrup, new.kontofk, new.company, new.cpowiat);

 end
end^
SET TERM ; ^
