--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERPARAM_BI_ORDER FOR DEFOPERPARAM                   
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable maxnum integer;
begin
  new.ord = 1;
  select max(numer) from defoperparam 
    into :maxnum;
  if (new.numer is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.numer = maxnum + 1;
  end else if (new.numer <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update defoperparam set ord = 0, numer = numer + 1
      where numer >= new.numer;
  end else if (new.numer > maxnum) then
    new.numer = maxnum + 1;
end^
SET TERM ; ^
