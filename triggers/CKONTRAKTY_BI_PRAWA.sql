--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKONTRAKTY_BI_PRAWA FOR CKONTRAKTY                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
 if(new.prawadef is null) then new.prawadef = 0;
 if(new.prawadef=0) then begin
   if(new.cpodmiot is not null) then
     select PRAWA,PRAWAGRUP from CPODMIOTY where ref = new.cpodmiot
     into new.prawa, new.prawagrup;
   else begin
     new.prawa = '';
     new.prawagrup = '';
   end
 end
 execute procedure GET_SUPERIORS(new.prawa) returning_values new.prawa;
 if(new.cpodmiot is not null) then
   select SKROT,SLODEF,OPEROPIEK,COMPANY from CPODMIOTY where REF=new.CPODMIOT
   into new.cpodmskrot, new.cpodmslodef, new.cpodmoperopiek,new.company;
 if(new.OSOBA is not NULL) then
   select NAZWA from PKOSOBY where REF=new.OSOBA
   into new.PKOSOBYNAZWA;
 else begin
   new.PKOSOBYNAZWA = NULL;
 end
 if(new.cpodmiot is not null) then
   select ancestors from CPODMIOTY where ref = new.cpodmiot into new.cpodmancestors;
end^
SET TERM ; ^
