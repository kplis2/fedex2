--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDECLDEFS_BIU_DECLGROUP FOR EDECLDEFS                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
as
declare variable scode varchar(20);
declare variable isgroup smallint;
begin

  new.isgroup = coalesce(new.isgroup,0);

  if (new.isgroup = 1 and (inserting
    or new.isgroup is distinct from old.isgroup
    or new.declgroup is distinct from old.declgroup
    or new.systemcode is distinct from old.systemcode
    or new.ref is distinct from old.ref)
  ) then begin
    if (exists(select first 1 1 from edecldefs
          where systemcode = new.systemcode and isgroup = 1 and ref <> new.ref)
    ) then
       exception universal 'Dla jednego kodu systemowego może być tylko jedna definicja zbiorcza';

    if (new.declgroup is not null) then
      exception universal 'Deklaracja z przypisaną deklaracją grupującą nie może być jednocześnie ustawiona jako zbiorcza';
  end

  if (updating and new.isgroup is distinct from old.isgroup) then
  begin
    if (exists(select first 1 1 from edecldefs where declgroup = new.ref))then
      exception universal 'Deklaracja jest przypisana jako grupująca do innej deklaracji';
  end

  if (new.declgroup is not null and (inserting
    or new.declgroup is distinct from old.declgroup
    or new.systemcode is distinct from old.systemcode)
  ) then begin
    select systemcode, isgroup from edecldefs
      where ref = new.declgroup
      into scode, isgroup;

    if (scode is distinct from new.systemcode) then
      exception universal 'Deklaracją grupującą może być tylko deklaracja o takim samym kodzie systemowym';
    if (isgroup <> 1) then
      exception universal 'Wskazana deklaracja grupująca nie jest zdefiniowana jako zbiorcza';
  end


end^
SET TERM ; ^
