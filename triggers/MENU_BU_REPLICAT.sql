--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MENU_BU_REPLICAT FOR MENU                           
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.symbol <> old.symbol or (new.symbol is null and old.symbol is not null) or (new.symbol is not null and old.symbol is null) or
     new.numer <> old.numer or (new.numer is null and old.numer is not null) or (new.numer is not null and old.numer is null) or
/*     new.poziom <> old.poziom or*/
     new.nazwa <> old.nazwa or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null) or
     new.link <> old.link or (new.link is null and old.link is not null) or (new.link is not null and old.link is null) or
     new.prefix <> old.prefix or (new.prefix is null and old.prefix is not null) or (new.prefix is not null and old.prefix is null) or
     new.typ <> old.typ or (new.typ is null and old.typ is not null) or (new.typ is not null and old.typ is null) or
     new.wartosci <> old.wartosci or (new.wartosci is null and old.wartosci is not null) or (new.wartosci is not null and old.wartosci is null)
  )then
    execute procedure REPLICAT_STATE('MENU',new.state, new.ref, NULL, NULL, NULL) returning_values new.state;
end^
SET TERM ; ^
