--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_AU_ORDER FOR RKDOKPOZ                       
  ACTIVE AFTER UPDATE POSITION 1 
as
begin
  if (new.ord = 0 or new.ord = 3) then
    execute procedure order_RKDOKPOZ(new.dokument, old.lp, new.lp);
end^
SET TERM ; ^
