--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFMAGAZ_BD0 FOR DEFMAGAZ                       
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable dokumcount integer;
begin
  select count(*) from DOKUMNAG where MAGAZYN = old.symbol into :dokumcount;
  if(:dokumcount > 0) then exception DEFMAGAZ_CHANGE_NOT_ALLOWED;

  select count(*) from POZZAM where MAGAZYN = old.symbol into :dokumcount;
  if(:dokumcount > 0) then exception DEFMAGAZ_CHANGE_NOT_ALLOWED;

  if (exists(select first 1 1 from defmagaz where magmaster = old.symbol)) then
    exception DEFMAGAZ_CHANGE_NOT_ALLOWED 'Magazyn jest magazynem nadrzędnym. Wyczyść powiązania.';

  delete from  stanycen where MAGAZYN=old.symbol;
  delete from  stanyil where MAGAZYN=old.symbol;

end^
SET TERM ; ^
