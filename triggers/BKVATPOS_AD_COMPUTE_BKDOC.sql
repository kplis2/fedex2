--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKVATPOS_AD_COMPUTE_BKDOC FOR BKVATPOS                       
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  update bkdocs set
    sumnetv = sumnetv - old.netv,
    sumvatv = sumvatv - old.vatv,
    sumgrossv = sumgrossv - old.grossv,
    curnetval = curnetval - old.currnetv,
    sumnet = sumnet - old.net,
    sumvate = sumvate - old.vate
  where ref = old.bkdoc;
end^
SET TERM ; ^
