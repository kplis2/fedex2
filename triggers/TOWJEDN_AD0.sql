--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWJEDN_AD0 FOR TOWJEDN                        
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable reff integer ;
begin
  if(old.s = 2) then begin
    select max(ref) from TOWJEDN where KTM = old.ktm and S = 1 into :reff;
    update TOWJEDN set S = 2 where REF=:reff;
  end
  if(old.z = 2) then begin
    select max(ref) from TOWJEDN where KTM = old.ktm and z = 1 into :reff;
    update TOWJEDN set z = 2 where REF=:reff;
  end
  if(old.c = 2) then begin
    select max(ref) from TOWJEDN where KTM = old.ktm and c = 1 into :reff;
    update TOWJEDN set c = 2 where REF=:reff;
  end
  if(old.rola=1) then update TOWARY set ILOSCWOPAK=1 where KTM = old.ktm;
  execute procedure TOWARY_OBLJEDN(old.ktm);
end^
SET TERM ; ^
