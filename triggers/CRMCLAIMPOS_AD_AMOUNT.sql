--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CRMCLAIMPOS_AD_AMOUNT FOR CRMCLAIMPOS                    
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable amount numeric(14,2);
declare variable suma numeric(14,2);
begin
suma = 0;
  for
    select amount from crmclaimpos CA where CA.crmclaim = old.crmclaim
    into :amount
  do begin
    suma = suma + amount;
  end
  update crmclaims CS set CS.amount = :suma where CS.ref = old.crmclaim;
end^
SET TERM ; ^
