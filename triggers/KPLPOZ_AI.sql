--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KPLPOZ_AI FOR KPLPOZ                         
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable towtypekpl varchar(100);
declare variable kplwersjaref integer;
begin
  execute procedure NAGKPL_OBL(new.nagkpl);
  execute procedure get_config('TOWTYPEKOMPLET',2) returning_values :towtypekpl;
  if (:towtypekpl is null or :towtypekpl = '') then
    towtypekpl = '2';
  -- jezeli modyfikujemy glowna recepture to aktualizacja na stanach ilosciowych ilosci potencjalnych mozliwych do wykonania
  select kplnag.wersjaref
    from kplnag
    where kplnag.ref = new.nagkpl and kplnag.glowna = 1
    into :kplwersjaref;
  if (:kplwersjaref is not null) then
    execute procedure CALCULATE_KOMPLETY_POT(:kplwersjaref,null,cast(:towtypekpl as integer));
end^
SET TERM ; ^
