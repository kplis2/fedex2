--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FSCLRACCH_BU0 FOR FSCLRACCH                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable symbol varchar(100);
  declare variable name varchar(255);
  declare variable account varchar(30);
begin
  if (old.dictpos is null and new.dictpos is not null) then
  begin
    execute procedure SLO_DANE(new.dictdef, new.dictpos)
      returning_values :symbol, :name, :account;
    new.dictsymbol = substring(:symbol from 1 for 80);
  end
  if (new.regdate <> old.regdate) then
  begin
    select OKRES from datatookres(cast(new.regdate as date),0,0,0,1)
      into new.PERIOD;
  end
end^
SET TERM ; ^
