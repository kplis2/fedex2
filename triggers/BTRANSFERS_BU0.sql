--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BTRANSFERS_BU0 FOR BTRANSFERS                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable cnt integer;
  declare variable aamount numeric(14,2);
begin
  if (new.towho is null) then new.towho = '';
  if (new.toacc is null) then new.toacc = '';
  if (new.amount is null) then new.amount = 0;
  if (new.data is null) then new.data = current_date;
  if (new.dataopen is null) then new.dataopen = current_date;
  if (new.btype <> old.btype) then
    select RIGHTS, RIGHTSGRUP
      from BTRANTYPE
      where SYMBOL = new.btype
      into new.rights,  new.rightsgrup;
  if (new.rights is null) then new.rights = '';
  if (new.rightsgrup is null) then new.rightsgrup = '';
  if (new.status > 0 and old.status = 0) then
  begin --akceptacja - sprawdzenie przelewu
    if (new.amount <= 0) then exception BTRANSFER_AMOUNTZERO;
    if (new.toacc = '') then exception BTRANSFER_NOTACCOUNT;
    if (new.towho = '') then exception BTRANSFER_NOTTOWHO;
    if ( exists (
      select first 1 1 from BANKACCOUNTS
        where EACCOUNT=replace(replace(new.TOACC,'-',''),  ' ',  '')
          and  DICTDEF=new.SLODEF
          and  DICTPOS=new.SLOPOZ
          and OFFDATE<=current_date
       ) ) then
    begin
      exception BTRANSFER_NOTTOWHO 'Konto na które ma trafić przelew jest nieaktualne';
    end
  end
  if (new.status = 1 and old.status < 1) then new.dataakc = current_date;
  if (new.status = 2 and old.status < 2) then new.datasend = current_date;
  if (new.status <= 1 and old.status > 1) then
  begin
    new.datasend = null;
    new.opersend = null;
  end
  if (new.status = 0 and old.status > 0) then
  begin
    new.dataakc = null;
    new.operack = null;
  end
  if (old.status = 3 and new.status < 3) then
  begin
    select count(*) from RKDOKNAG where BTRANSFER = old.ref into :cnt;
    if (cnt > 0) then exception BTRANSFER_HASRKDOKNAG;
  end
  if ((new.slodef <> old.slodef or (new.slodef is not null and old.slodef is null))
    and (new.slopoz <> old.slopoz or (new.slopoz is not null and old.slopoz is null)))
  then
    select kodks from slo_daneks(new.slopoz, new.slodef) into new.slokodks;
  else if (new.slodef is null or new.slopoz is null) then new.slokodks = null;
  if (new.status = 0 and old.status > 0) then
  begin --doliczenie do wkoty przelewu tych ze starych, które są niezaakceptowane
    select sum(amount)
      from BTRANSFERS
      where BTRANGROUP = new.btrangroup and status = 0 and ref <> new.ref
      into :aamount;
    if (aamount > 0) then new.amount = new.amount + :aamount;
  end
end^
SET TERM ; ^
