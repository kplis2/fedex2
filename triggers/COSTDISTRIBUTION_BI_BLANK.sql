--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER COSTDISTRIBUTION_BI_BLANK FOR COSTDISTRIBUTION               
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.cdtype is null) then new.cdtype = 0;
  if (new.costsource is null) then new.costsource = 0;
  if (new.status is null) then new.status = 0;
  if (new.reverse is null) then new.reverse = 0;
  if (new.destaccchecksum is null) then new.destaccchecksum = 0;
  if (new.descripttype is null) then new.descripttype = 0;
end^
SET TERM ; ^
