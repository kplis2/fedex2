--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RP_SYNCDEF_BI_REPLICAT FOR RP_SYNCDEF                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
 execute procedure rp_trigger_bi('RP_SYNCDEF',new.tabela, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
