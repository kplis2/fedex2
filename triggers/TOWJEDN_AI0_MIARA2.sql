--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWJEDN_AI0_MIARA2 FOR TOWJEDN                        
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable ilosc numeric(14,4);
begin
  if (new.dodatkowa = 1 and new.przelicz is not null and new.przelicz<>0) then
    begin
      update stanyil s set
        s.miara1 = new.jedn,
        s.iloscjm1 = s.ilosc / new.przelicz
      where s.ktm = new.ktm;
    end
  if (new.dodatkowa = 2 and new.przelicz is not null and new.przelicz<>0) then
    begin
      update stanyil s set
        s.miara2 = new.jedn,
        s.iloscjm2 = s.ilosc / new.przelicz
      where s.ktm = new.ktm;
    end
end^
SET TERM ; ^
