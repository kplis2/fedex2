--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHW_BI0 FOR DEFCECHW                       
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable typ smallint;
declare variable ilosc integer;
declare variable tworzyktm integer;
declare variable dlugosc integer;
declare variable len integer;
begin

    if(new.ID_DEFCECHW is NULL or (new.ID_DEFCECHW =0)) then
        new.ID_DEFCECHW = gen_id(gen_defcechw,1);
    select TYP,TWORZYKTM,DLUGOSC from DEFCECHY where DEFCECHY.symbol = new.cecha into :typ,:tworzyktm,:dlugosc;
    if(:typ < 2) then begin
      new.wartdate = null;
      select str from change_numeric_to_str(new.wartnum) into new.wartstr;
    end else if(:typ = 2) then begin
      new.wartnum = null;
      new.wartdate = null;
    end else begin
      new.wartnum = null;
      new.wartstr = coalesce(cast(new.wartdate as date),'');
    end
    if(new.wartstr is null and new.wartnum is null and new.wartdate is null) then
      exception DEFCECHW_NOVALUE;
    len = coalesce(char_length(new.wartstr),0); -- [DG] XXX ZG119346
    if(:tworzyktm>0 and :dlugosc>0 and :dlugosc<>:len) then
      exception DEFCECHW_NOVALUE 'Długość wartości cechy powinna wynosić '||:dlugosc||' zn.';
    ilosc = 0;
    new.zmien = 0;
    if(new.cecha is not null and new.kod is not null and new.kod<>'') then begin
      select count(*) from DEFCECHW where CECHA=new.cecha and KOD=new.kod and id_defcechw<>new.id_defcechw into :ilosc;
      if(ilosc>0) then exception DEFCECHW_KOD;
    end
 end^
SET TERM ; ^
