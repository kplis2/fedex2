--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDHDRS_BI_REF FOR FRDHDRS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FRDHDRS')
      returning_values new.REF;
end^
SET TERM ; ^
