--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLNAGZAM_AUCPLOPER FOR CPLNAGZAM                      
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable refpozzam integer;
declare variable ret integer;
begin
  if((new.CPL <> old.CPL) or
     (new.CPLUCZEST <> old.CPLUCZEST) or
     (new.DATA <> old.DATA) or
     (new.STATUS <> old.STATUS) or
     (new.DATAREAL <> old.DATAREAL) or
     (new.TYPPKT <> old.TYPPKT) or
     (new.OPIS <> old.OPIS)) then begin
       for select REF from CPLPOZZAM where CPLNAGZAM=new.REF into :refpozzam do begin
         execute procedure CPLOPER_WYSTAWUSUN(:refpozzam) returning_values :ret;
       end
  end
end^
SET TERM ; ^
