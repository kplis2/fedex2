--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DCTEMPLETPOS_BI_ORDER FOR DCTEMPLETPOS                   
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable maxnum integer;
begin
  new.ord = 1;
  select max(number) from dctempletpos where dctemplet = new.dctemplet
    into :maxnum;
  if (new.number is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update dctempletpos set ord = 0, number = number + 1
      where number >= new.number and dctemplet = new.dctemplet;
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
