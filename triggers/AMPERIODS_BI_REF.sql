--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER AMPERIODS_BI_REF FOR AMPERIODS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('AMPERIODS')
      returning_values new.REF;
end^
SET TERM ; ^
