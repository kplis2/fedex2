--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECREDINSTALS_AIUD_ECREDITS FOR ECREDINSTALS                   
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 
AS
begin
  -- W zależnoci od operacji trigger zwieksza lub zmniejsza wartosc kredytu
  if (inserting or deleting or old.insvalue <> new.insvalue) then
  begin
    update ecredits
      set actvalue = actvalue + iif(not inserting, old.insvalue, 0)
                              - iif(not deleting, new.insvalue, 0)
       where ref = coalesce(new.credit,old.credit);
  end
end^
SET TERM ; ^
