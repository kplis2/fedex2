--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDTYPES4WH_BU0 FOR MWSORDTYPES4WH                 
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.mwsordtype <> old.mwsordtype) then
    select mwsordtypes.symbol
      from mwsordtypes where mwsordtypes.ref = new.mwsordtype
      into new.mwsordsymb;
end^
SET TERM ; ^
