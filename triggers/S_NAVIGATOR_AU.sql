--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_NAVIGATOR_AU FOR S_NAVIGATOR                    
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable prefix varchar(255);
declare variable section varchar(255);
declare variable ref varchar(255);
declare variable prev varchar(255);
declare variable number integer;
declare variable changedfields varchar(1024);
begin
    if(new.initializing=1 or new.initializing<>old.initializing) then exit;
    -- umiesc dane w repozytorium
    prefix = 'Navigator:';
    section = :prefix || new.ref;

    if(new.ref<>old.ref) then execute procedure SYS_SET_APPINI(new.namespace,:section,'REF',new.ref);
    if(coalesce(new.parent,'')<>coalesce(old.parent,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'PARENT',new.parent);
    if(coalesce(new.signature,'')<>coalesce(old.signature,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'Signature',new.signature);
    if(coalesce(new.name,'')<>coalesce(old.name,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'NAME',new.name);
    if(coalesce(new.hint,'')<>coalesce(old.hint,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'HINT',new.hint);
    if(coalesce(new.kindstr,'')<>coalesce(old.kindstr,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'KINDSTR',new.kindstr);
    if(coalesce(new.actionid,'')<>coalesce(old.actionid,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'ACTIONID',new.actionid);
    if(coalesce(new.applications,'')<>coalesce(old.applications,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'APPLICATIONS',new.applications);
    if(coalesce(new.formodules,'')<>coalesce(old.formodules,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'FORMODULES',new.formodules);
    if(coalesce(new.rights,'')<>coalesce(old.rights,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'RIGHTS',new.rights);
    if(coalesce(new.rightsgroup,'')<>coalesce(old.rightsgroup,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'RIGHTSGROUP',new.rightsgroup);
    if(coalesce(new.isnavbar,0)<>coalesce(old.isnavbar,0)) then execute procedure SYS_SET_APPINI(new.namespace,:section,'ISNAVBAR',new.isnavbar);
    if(coalesce(new.istoolbar,0)<>coalesce(old.istoolbar,0)) then execute procedure SYS_SET_APPINI(new.namespace,:section,'ISTOOLBAR',new.istoolbar);
    if(coalesce(new.iswindow,0)<>coalesce(old.iswindow,0)) then execute procedure SYS_SET_APPINI(new.namespace,:section,'ISWINDOW',new.iswindow);
    if(coalesce(new.itemtype,0)<>coalesce(old.itemtype,0)) then execute procedure SYS_SET_APPINI(new.namespace,:section,'ITEMTYPE',new.itemtype);
    if(coalesce(new.groupprocedure,'')<>coalesce(old.groupprocedure,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'GROUPPROCEDURE',new.groupprocedure);
    if(coalesce(new.ribbontab,'')<>coalesce(old.ribbontab,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'RIBBONTAB',new.ribbontab);
    if(coalesce(new.ribbongroup,'')<>coalesce(old.ribbongroup,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'RIBBONGROUP',new.ribbongroup);
    if(coalesce(new.globalvalue,'')<>coalesce(old.globalvalue,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'GLOBALVALUE',new.globalvalue);
    if(coalesce(new.popupmenu,'')<>coalesce(old.popupmenu,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'POPUPMENU',new.popupmenu);
    if(coalesce(new.foradmin,0)<>coalesce(old.foradmin,0)) then execute procedure SYS_SET_APPINI(new.namespace,:section,'FORADMIN',new.foradmin);
    if(coalesce(new.shortcut,'')<>coalesce(old.shortcut,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'SHORTCUT',new.shortcut);
    if(coalesce(new.globalshortcut,'')<>coalesce(old.globalshortcut,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'GLOBALSHORTCUT',new.globalshortcut);
    if(coalesce(new.globalcontext,'')<>coalesce(old.globalcontext,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'GLOBALCONTEXT',new.globalcontext);
    if(coalesce(new.begingroup,0)<>coalesce(old.begingroup,0)) then execute procedure SYS_SET_APPINI(new.namespace,:section,'BEGINGROUP',new.begingroup);
    if(coalesce(new.globalsuffix,'')<>coalesce(old.globalsuffix,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'GLOBALSUFFIX',new.globalsuffix);
    if(coalesce(new.isehrm,0)<>coalesce(old.isehrm,0)) then execute procedure SYS_SET_APPINI(new.namespace,:section,'ISEHRM',new.isehrm);
    if(coalesce(new.ehrmlink,'')<>coalesce(old.ehrmlink,'')) then execute procedure SYS_SET_APPINI(new.namespace,:section,'EHRMLINK',new.ehrmlink);

    if(coalesce(old.parent,'')<>coalesce(new.parent,'')) then begin
      -- jesli zmieniono PARENT to przesortuj elementy w starym i nowym parencie
      execute procedure SYS_ORDER_APPINI(new.namespace,old.parent,:prefix,new.ref,old.prev,NULL);
      -- pobierz zmienione numery TODO
      update s_navigator set INITIALIZING=1 where coalesce(PARENT,'')=coalesce(old.parent,'');
      for select REF,PREV,NUMBER,CHANGEDFIELDS from sys_get_navigator(new.namespace, NULL, 'REF,PREV,PARENT,NUMBER,CHANGEDFIELDS')
        where coalesce(PARENT,'')=coalesce(old.parent,'')
        into :ref,:prev,:number,:changedfields
      do begin
        update s_navigator set NUMBER=:number, PREV=:prev, CHANGEDFIELDS=:changedfields where ref=:ref and (NUMBER<>:number or PREV<>:prev or CHANGEDFIELDS<>:changedfields);
      end
      -- ustaw PREV i kolejnosc
      execute procedure SYS_SET_APPINI(new.namespace,:section,'PREV',new.prev);
      execute procedure SYS_ORDER_APPINI(new.namespace,new.parent,:prefix,new.ref,NULL,new.prev);
      -- pobierz zmienione numery
      update s_navigator set INITIALIZING=1 where coalesce(PARENT,'')=coalesce(new.parent,'');
      for select REF,PREV,NUMBER,CHANGEDFIELDS from sys_get_navigator(new.namespace, NULL, 'REF,PREV,PARENT,NUMBER,CHANGEDFIELDS')
        where coalesce(PARENT,'')=coalesce(new.parent,'')
        into :ref,:prev,:number,:changedfields
      do begin
        update s_navigator set NUMBER=:number, PREV=:prev, CHANGEDFIELDS=:changedfields where ref=:ref and (NUMBER<>:number or PREV<>:prev or CHANGEDFIELDS<>:changedfields);
      end
      update s_navigator set INITIALIZING=0 where INITIALIZING=1;
    end else begin
      if(coalesce(old.prev,'')<>coalesce(new.prev,'')) then begin
        execute procedure SYS_SET_APPINI(new.namespace,:section,'PREV',new.prev);
        -- jesli zmieniono PREV to przesortuj elementy
        execute procedure SYS_ORDER_APPINI(new.namespace,new.parent,:prefix,new.ref,old.prev,new.prev);
        -- pobierz zmienione numery
        update s_navigator set INITIALIZING=1 where coalesce(PARENT,'')=coalesce(new.parent,'');
        for select REF,PREV,NUMBER,CHANGEDFIELDS from sys_get_navigator(new.namespace, NULL, 'REF,PREV,PARENT,NUMBER,CHANGEDFIELDS')
          where coalesce(PARENT,'')=coalesce(new.parent,'')
          into :ref,:prev,:number,:changedfields
        do begin
          update s_navigator set NUMBER=:number, PREV=:prev, CHANGEDFIELDS=:changedfields where ref=:ref and (NUMBER<>:number or PREV<>:prev or CHANGEDFIELDS<>:changedfields);
        end
        update s_navigator set INITIALIZING=0 where INITIALIZING=1;
      end
    end
end^
SET TERM ; ^
