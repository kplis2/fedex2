--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDLEVELTYPES_BI_REF FOR MWSSTANDLEVELTYPES             
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSSTANDLEVELTYPES') returning_values new.ref;
end^
SET TERM ; ^
