--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECOLUMNS_BU_REGOPERATOR FOR ECOLUMNS                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  execute procedure get_global_param ('AKTUOPERATOR')
    returning_values new.regoperator;
  new.regtimestamp = current_timestamp(0);
end^
SET TERM ; ^
