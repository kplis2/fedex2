--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRDVERSIONS_BU_DECREE FOR PRDVERSIONS                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable bkperiod varchar(6);
declare variable bkdocsymbol varchar(20);
begin
 if(new.fromperiod <> old.fromperiod) then begin
   select first 1 b.symbol, b.period from decrees d join bkdocs b on (b.ref = d.bkdoc)
   join prdaccounting p on (p.ref = new.prdaccounting)
     where (d.period>=new.fromperiod or d.period >= old.fromperiod)
        and d.prdaccounting = new.prdaccounting and d.ref <> p.decree
   into :bkdocsymbol, :bkperiod;
   if(coalesce(:bkdocsymbol,'') <> '') then
     exception prd_deducted 'Istnieje powiązany dokument RMK ' || :bkdocsymbol || ' (' || :bkperiod || '). Nie można poprawić!';
  end
end^
SET TERM ; ^
