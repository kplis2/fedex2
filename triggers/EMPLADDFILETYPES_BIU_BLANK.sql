--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLADDFILETYPES_BIU_BLANK FOR EMPLADDFILETYPES               
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
as
begin
  if (new.emplorder < -1 or new.emplorder is null) then
    new.emplorder = -1;

  if (new.persorder < -1 or new.persorder is null) then
    new.persorder = -1;

  if (updating and new.emplorder is distinct from old.emplorder and new.emplorder = -1) then
    if (exists(select first 1 1 from empladdfiles where filetype = new.ref and employee is not null)) then
      exception universal 'Kartoteka zawiera wpisy powiązane z pracownikiem - ukrycie niemożliwe.';

  if (updating and new.persorder is distinct from old.persorder and new.persorder = -1) then
    if (exists(select first 1 1 from empladdfiles where filetype = new.ref and person is not null)) then
      exception universal 'Kartoteka zawiera wpisy powiązane z osobą - ukrycie niemożliwe.';

  if(new.symbol is distinct from old.symbol and exists(select first 1 1 from empladdfiletypes where symbol = new.symbol)) then
    exception UNQSYMBOL;
end^
SET TERM ; ^
