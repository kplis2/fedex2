--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PLATNICY_BU0 FOR PLATNICY                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.klient = new.platnik) then exception PLATNICY_TENSAM;
  if(new.platnik <> old.platnik) then
    select FSKROT from KLIENCI where ref=new.platnik into new.fskrot;

end^
SET TERM ; ^
