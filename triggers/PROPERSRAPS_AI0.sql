--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROPERSRAPS_AI0 FOR PROPERSRAPS                    
  ACTIVE AFTER INSERT POSITION 1 
AS
declare variable amount numeric(14,4);
declare variable amountcur numeric(14,4);
declare variable amountresultcalctype smallint;
begin
  if(new.amount <> 0) then begin
    amount = 0;

    select prrs.amountresultcalctype
      from  prschedopers prps
        left join prshopers prrs on (prps.shoper = prrs.ref)
      where prps.ref = new.prschedoper
      into :amountresultcalctype;

    for select sum(por.amount)
      from propersraps por
      where por.prschedoper = new.prschedoper and por.calcamountresult = 1
      into :amountcur
    do begin
      --dla MAX
      if (:amountresultcalctype = 0) then
      begin
        if(amountcur > amount) then amount = :amountcur;
      --dla MIN
      end else if (:amountresultcalctype = 1) then
      begin
        if(amountcur < amount) then amount = :amountcur;
      --dla SUMA
      end else if (:amountresultcalctype = 2) then
        amount = :amount + :amountcur;
    end

    update prschedopers pso set pso.amountresult = :amount where pso.ref = new.prschedoper;
  end
  if(not exists(select ref from propersraps where prschedoper = new.prschedoper and ref <> new.ref))
    then execute procedure PRSCHEDOPERS_STATUSCHECK(null, null, new.prschedoper);
end^
SET TERM ; ^
