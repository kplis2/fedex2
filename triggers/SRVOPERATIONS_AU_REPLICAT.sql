--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVOPERATIONS_AU_REPLICAT FOR SRVOPERATIONS                  
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if((new.contrtype <> old.contrtype ) or (new.contrtype is not null and old.contrtype is null) or (new.contrtype is null and old.contrtype is not null)
  or (new.name <> old.name ) or (new.name is not null and old.name is null) or (new.name is null and old.name is not null)
  or (new.fromstatus <> old.fromstatus ) or (new.fromstatus is not null and old.fromstatus is null) or (new.fromstatus is null and old.fromstatus is not null)
  or (new.tostatus <> old.tostatus ) or (new.tostatus is not null and old.tostatus is null) or (new.tostatus is null and old.tostatus is not null)
  or (new.realize <> old.realize ) or (new.realize is not null and old.realize is null) or (new.realize is null and old.realize is not null)
  or (new.preaction <> old.preaction ) or (new.preaction is not null and old.preaction is null) or (new.preaction is null and old.preaction is not null)
  or (new.postaction <> old.postaction ) or (new.postaction is not null and old.postaction is null) or (new.postaction is null and old.postaction is not null)
  or (new.getnumber <> old.getnumber ) or (new.getnumber is not null and old.getnumber is null) or (new.getnumber is null and old.getnumber is not null)
  or (new.getnumber2 <> old.getnumber2 ) or (new.getnumber2 is not null and old.getnumber2 is null) or (new.getnumber2 is null and old.getnumber2 is not null)
  or (new.operright <> old.operright ) or (new.operright is not null and old.operright is null) or (new.operright is null and old.operright is not null)
  or (new.modfield <> old.modfield ) or (new.modfield is not null and old.modfield is null) or (new.modfield is null and old.modfield is not null)
  or (new.modvalue <> old.modvalue ) or (new.modvalue is not null and old.modvalue is null) or (new.modvalue is null and old.modvalue is not null)
  ) then
     update CONTRTYPES set STATE = -1 where ref=new.contrtype;
end^
SET TERM ; ^
