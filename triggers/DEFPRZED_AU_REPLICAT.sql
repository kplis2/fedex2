--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFPRZED_AU_REPLICAT FOR DEFPRZED                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.cecha <> old.cecha or (new.cecha is null and old.cecha is not null) or (new.cecha is not null and old.cecha is null) or
     new.wart_min <> old.wart_min or (new.wart_min is null and old.wart_min is not null) or (new.wart_min is not null and old.wart_min is null) or
     new.opis <> old.opis or (new.opis is null and old.opis is not null) or (new.opis is not null and old.opis is null) or
     new.ukrywanie <> old.ukrywanie or (new.ukrywanie is null and old.ukrywanie is not null) or (new.ukrywanie is not null and old.ukrywanie is null)
  ) then
     update DEFCECHY set STATE=-2 where SYMBOL=old.cecha or SYMBOL=new.cecha;

end^
SET TERM ; ^
