--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWKODKRESK_BI_REF FOR TOWKODKRESK                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('TOWKODKRESK')
      returning_values new.REF;
end^
SET TERM ; ^
