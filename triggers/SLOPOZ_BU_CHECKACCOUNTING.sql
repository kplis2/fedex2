--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLOPOZ_BU_CHECKACCOUNTING FOR SLOPOZ                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable ka smallint;
  declare variable an smallint;
  declare variable kodln smallint;
  declare variable isdist smallint;
begin
  if (new.akt is null) then new.akt = 1;
  select KARTOTEKA, ANALITYKA, KODLN, isdist
    from SLODEF where REF = new.slownik
    into :ka, :an, :kodln, :isdist;

  if (ka = 0 and (an = 1 or isdist = 1)) then
    new.kod = new.kontoks;

  if (an=1 and coalesce(char_length(new.kontoks),0) <> kodln) then -- [DG] XXX ZG119346
    exception fk_error_synlen;

  if (new.kontoks <> old.kontoks)then
  begin
    execute procedure check_dict_using_in_accounts(old.slownik, old.kontoks,null);
  end

  -- blokada poprawiania wyróżników jeżeli są obroty na koncie
  if(new.kontoks<>old.kontoks and :isdist>0
    and exists(select first 1 1 from decrees d where
      (d.dist1ddef=old.slownik and d.dist1symbol=old.kontoks)
      or (d.dist1ddef=old.slownik and d.dist1symbol=old.kontoks)
      or (d.dist2ddef=old.slownik and d.dist2symbol=old.kontoks)
      or (d.dist3ddef=old.slownik and d.dist3symbol=old.kontoks)
      or (d.dist4ddef=old.slownik and d.dist4symbol=old.kontoks)
      or (d.dist5ddef=old.slownik and d.dist5symbol=old.kontoks)
      )) then
        exception SLOPOZ_USED_IN_DECREES;

end^
SET TERM ; ^
