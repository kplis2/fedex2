--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WERSJE_BD0 FOR WERSJE                         
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable cnt integer;
declare variable cnt2 integer;
begin
  select count(*) from TOWARY where KTM = old.ktm into :cnt;
  if(:cnt > 0) then begin
    select count(*) from WERSJE where KTM = old.ktm into :cnt2;
    if(:cnt2 < 2) then
       exception WERSJE_USUWANIE_WERSJI_ZERO;
  end
  if(old.token = 7) then
    delete from TOWPLIKI where WERSJAREF = old.ref;
end^
SET TERM ; ^
