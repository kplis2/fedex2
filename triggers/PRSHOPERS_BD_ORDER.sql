--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHOPERS_BD_ORDER FOR PRSHOPERS                      
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  update prshopers set ord = 1, number = number - 1
    where ref <> old.ref and number > old.number and sheet = old.sheet
      and coalesce(opermaster, null, 0) = coalesce(old.opermaster, null, 0);
end^
SET TERM ; ^
