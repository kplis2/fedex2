--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLUCZEST_BD0 FOR CPLUCZEST                      
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable cplstatus integer;
begin
   select STATUS from CPL where REF=old.cpl into :cplstatus;
   if(:cplstatus > 1) then exception CPL_ZAMKNIETY;/* mozna odejmowac, jesli jest w trakcie przygowoania */
end^
SET TERM ; ^
