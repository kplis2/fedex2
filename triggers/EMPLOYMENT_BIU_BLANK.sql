--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLOYMENT_BIU_BLANK FOR EMPLOYMENT                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if (new.workdim is null) then new.workdim = 1.0000 * new.dimnum / new.dimden;
end^
SET TERM ; ^
