--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_AU_OPAKOWANIE FOR DOKUMPOZ                       
  ACTIVE AFTER UPDATE POSITION 4 
as
  declare variable towkaucja smallint;
  declare variable klientopk smallint;
  declare variable status integer;
  declare variable klient integer;
  declare variable typ varchar(3);
  declare variable opakowanie integer;
  declare variable zrodlo smallint;
begin
  if((coalesce(new.ktm,'')<>coalesce(old.ktm,'')) or
     (coalesce(new.wersja,0)<>coalesce(old.wersja,0)) or
     (coalesce(new.ilosc,0)<>coalesce(old.ilosc,0)) or
     (coalesce(new.jedno,0)<>coalesce(old.jedno,0)))
  then begin
    delete from dokumpoz where dokumpozopk = old.ref;
    select typ, klient, zrodlo from dokumnag where ref=new.dokument into :typ, :klient, :zrodlo;
    select opak from defdokum where symbol=:typ into :opakowanie;
    select kaucja from towary where ktm = new.ktm into :towkaucja;
    if (:towkaucja = 1 and :opakowanie = 1 and (:zrodlo = 0 or :zrodlo=1)) then
    begin
      select kaucja from klienci where ref = :klient into :klientopk;
      if (:klientopk >0) then
        execute procedure DOKUMPOZ_KAUCJA_OPAKOWANIE(new.ref,NULL, :klientopk) returning_values :status;
    end
  end
end^
SET TERM ; ^
