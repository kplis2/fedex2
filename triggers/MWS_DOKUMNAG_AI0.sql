--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWS_DOKUMNAG_AI0 FOR DOKUMNAG                       
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable procsql varchar(255);
declare variable procsqls varchar(255);
declare variable refmwsord integer;
declare variable mwsord integer;
declare variable docblockreal smallint;
declare variable delafterackchange smallint;
declare variable mwsordstatus integer;
declare variable cnt integer;
begin
  if (new.mwsdoc is null or new.mwsdoc = 0) then
    exit;
  select afterackproc from defdokummag where magazyn = new.magazyn and typ = new.typ
    into :procsql;
  -- generowanie zleceń magazynowych
  if (new.akcept = 1 and procsql is not null and procsql <> '' and procsql <> ' ') then
  begin
    -- wywolanie procedury na akceptacje dokumentu magazynowego
    procsql = 'select refmwsord from '||procsql||'('||new.ref||','||new.grupasped||',null,null,null,null,1,null)';
    execute statement procsql into refmwsord;
  end
end^
SET TERM ; ^
