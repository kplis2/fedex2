--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_BD_ORDER FOR DOKUMPOZ                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 5 or coalesce(old.fake, 0) > 0) then exit;
  update dokumpoz set ord = 1, numer = numer - 1
    where ref <> old.ref and numer > old.numer and dokument = old.dokument;
end^
SET TERM ; ^
