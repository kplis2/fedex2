--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRDVERSIONS_BU FOR PRDVERSIONS                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.fromperiod <> old.fromperiod and exists(select ref from prdversions where fromperiod = new.fromperiod and ref<> new.ref
       and prdaccounting=new.prdaccounting)) then
    exception universal 'Wersja rozliczenia już istnieje dla danego okresu.';
end^
SET TERM ; ^
