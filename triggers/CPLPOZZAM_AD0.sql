--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLPOZZAM_AD0 FOR CPLPOZZAM                      
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable sumpkt integer;
begin
  if(old.cploper is not NULL) then begin
    /* usun CPLOPER */
    update CPLOPER set CPLNAGZAM=NULL where REF=old.cploper;
    delete from CPLOPER where REF=old.cploper;
  end
  select sum(ILPKT) from CPLPOZZAM where CPLNAGZAM = old.CPLNAGZAM into :sumpkt;
  update CPLNAGZAM set SUMILPKT = :sumpkt where REF = old.CPLNAGZAM;
end^
SET TERM ; ^
