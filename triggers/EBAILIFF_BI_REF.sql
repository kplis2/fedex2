--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EBAILIFF_BI_REF FOR EBAILIFF                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EBAILIFF')
      returning_values new.REF;
end^
SET TERM ; ^
