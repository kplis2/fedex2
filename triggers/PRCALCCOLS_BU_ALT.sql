--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRCALCCOLS_BU_ALT FOR PRCALCCOLS                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  -- jesli nie wybieramy zadnej alternatywy, to wybierz domyslna
  if(new.prshoperalt is null and old.prshoperalt is not null and new.isalt=1) then begin
    select operdefault from prshopers where ref=new.fromprshoper into new.prshoperalt;
  end
end^
SET TERM ; ^
