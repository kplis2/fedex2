--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAMDYSP_BU0 FOR POZZAMDYSP                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable przelicz numeric(14,4);
begin
  if(new.jedn is null or (new.jedn = 0))then
    select jedn from POZZAM where ref=new.pozzam into new.jedn;
  if(new.jedno is null or (new.jedno = 0)) then
    select jedno from POZZAM where ref=new.pozzam into new.jedno;
  if(new.ilosc is null or (new.ilosc < 0)) then
    new.ilosc = 0;
  if(new.jedno = new.jedn) then
    new.ilosco = new.ilosc;
  if(new.ilosc <> old.ilosc) then begin
    if(new.iloscm is null or (new.iloscm <=0)) then begin
      select przelicz from TOWJEDN where ref=new.jedn into :przelicz;
      if(:przelicz is null) then przelicz = 1;
      new.iloscm = new.ilosc * :przelicz;
    end
    if(new.ilosco is null or (new.ilosc is null)) then begin
      select przelicz from TOWJEDN where ref=new.jedno into :przelicz;
      if(:przelicz is null) then przelicz = 1;
      new.ilosco = new.iloscm / :przelicz;
    end
  end
end^
SET TERM ; ^
