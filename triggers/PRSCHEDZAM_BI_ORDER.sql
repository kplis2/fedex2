--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDZAM_BI_ORDER FOR PRSCHEDZAM                     
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable maxnum integer;
begin
  new.ord = 1;
  select max(number) from prschedzam where prschedule = new.prschedule
    into :maxnum;
  if(new.status <2) then
  begin
    if (new.number is null) then
    begin
      if (maxnum is null) then maxnum = 0;
      new.number = maxnum + 1;
    end else if (new.number <= :maxnum) then
    begin
      update prschedzam set ord = 0, number = number + 1
        where number >= new.number and prschedule = new.prschedule;
    end else if (new.number > maxnum) then
      new.number = maxnum + 1;
  end
  else new.number = null;
end^
SET TERM ; ^
