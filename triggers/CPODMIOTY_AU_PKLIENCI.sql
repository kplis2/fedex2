--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_AU_PKLIENCI FOR CPODMIOTY                      
  ACTIVE AFTER UPDATE POSITION 2 
as
declare variable slodef integer;
declare variable crm smallint;
begin
  select ref from SLODEF where TYP = 'PKLIENCI' into :slodef;
  if(:slodef = new.slodef and
     (new.skrot <> old.skrot or (new.skrot is not null and old.skrot is null) or (new.skrot is null and old.skrot is not null) or
      new.NAZWA <> old.NAZWA or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null) or
      new.ulica <> old.ulica or (new.ulica is not null and old.ulica is null) or (new.ulica is null and old.ulica is not null) or
      new.nrdomu <> old.nrdomu or (new.nrdomu is not null and old.nrdomu is null) or (new.nrdomu is null and old.nrdomu is not null) or
      new.nrlokalu <> old.nrlokalu or (new.nrlokalu is not null and old.nrlokalu is null) or (new.nrlokalu is null and old.nrlokalu is not null) or
      new.miasto <> old.miasto or (new.miasto is not null and old.miasto is null) or (new.miasto is null and old.miasto is not null) or
      new.kodp <> old.kodp or (new.kodp is not null and old.kodp is null) or (new.kodp is null and old.kodp is not null) or
      new.telefon <> old.telefon or (new.telefon is not null and old.telefon is null) or (new.telefon is null and old.telefon is not null) or
      new.fax <> old.fax or (new.fax is not null and old.fax is null) or (new.fax is null and old.fax is not null) or
      new.www <> old.www or (new.www is not null and old.www is null) or (new.www is null and old.www is not null) or
      new.email <> old.email or (new.email is not null and old.email is null) or (new.email is null and old.email is not null) or
      new.zrodlo <> old.zrodlo or (new.zrodlo is not null and old.zrodlo is null) or (new.zrodlo is null and old.zrodlo is not null) or
      new.forma <> old.forma or (new.forma is not null and old.forma is null) or (new.forma is null and old.forma is not null) or
      new.branza <> old.branza or (new.branza is not null and old.branza is null) or (new.branza is null and old.branza is not null) or
      new.cstatus <> old.cstatus or (new.cstatus is not null and old.cstatus is null) or (new.cstatus is null and old.cstatus is not null) or
      new.uwagi <> old.uwagi or (new.uwagi is not null and old.uwagi is null) or (new.uwagi is null and old.uwagi is not null)
     )
   ) then begin
        update PKLIENCI set SKROT = new.skrot, NAZWA = new.nazwa,
                      ULICA = new.ulica, NRDOMU = new.nrdomu, NRLOKALU = new.nrlokalu,
                      MIASTO = new.miasto, KODP = new.kodp,
                      TELEFON = new.telefon, EMAIL = new.email, WWW = new.www, FAX = new.fax,
                      ZRODLO = new.zrodlo, FORMA = new.forma, BRANZA = new.branza, STATUS = new.cstatus, UWAGI = new.uwagi
          where REF=new.slopoz;
  end
  if (new.slodef=:slodef) then begin
    select CRM from PKLIENCI where REF=new.slopoz into :crm;
    if(:crm='0') then begin
      update PKLIENCI set CRM=1
      where REF=new.slopoz;
    end
  end
end^
SET TERM ; ^
