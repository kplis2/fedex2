--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGZAM_BD_STAN FOR NAGZAM                         
  ACTIVE BEFORE DELETE POSITION 1 
as
declare variable stan char(1);
declare variable termdost date;
declare variable status integer;
declare variable org_ref integer;
declare variable typ integer;
declare variable reff integer;
begin
  termdost = old.termdost;
  org_ref = old.org_ref;
  if(:termdost is null) then termdost = current_date;
  if(:org_ref is null) then org_ref = 0;
--  if(old.blokadarez = 0)then begin
     if(old.kktm<>'' and ((old.KSTAN ='B') or (old.Kstan = 'R') or (old.Kstan = 'D'))) then begin
        execute procedure REZ_SET_KTM(old.ref, 0, old.kktm, old.kwersja, 0,0, 0, 'N',:termdost) returning_values :status;
        if(:status <> 1) then begin
          exception REZ_WRONG;
        end
     end
--  end
  delete from STANYREZ where ZAMOWIENIE = old.ref and POZZAM=0;
end^
SET TERM ; ^
