--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ETAXALLOWANCES_BI_CHECK FOR ETAXALLOWANCES                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (exists(select ref from etaxallowances where ayear = new.ayear and person = new.person)) then
    exception universal 'Nie można zdefiniować dwóch ulg dla pracownika w jednym roku';
  if (new.incallowance > 0 and new.taxallowance > 0) then
    exception universal 'Nie można otrzymać jednocześnie zwolnienia od dochodu i ulgi podatkowej';
end^
SET TERM ; ^
