--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWPLIKI_BI_REF FOR TOWPLIKI                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('TOWPLIKI')
      returning_values new.REF;

end^
SET TERM ; ^
