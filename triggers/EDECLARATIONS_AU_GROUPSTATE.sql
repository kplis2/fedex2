--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDECLARATIONS_AU_GROUPSTATE FOR EDECLARATIONS                  
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if (new.state is distinct from old.state and new.state >= 1 and new.declgroup is null and new.person is null) then
  begin
    if (exists(select first 1 1 from edecldefs
                 where ref = new.edecldef and isgroup = 1)
    ) then
      update edeclarations set state = new.state
        where declgroup = new.ref and state is distinct from new.state;
  end

  if(new.status is distinct from old.status and new.status = 200) then
  begin
    if (exists(select first 1 1 from edecldefs
                 where ref = new.edecldef and isgroup = 1)
    ) then
      update edeclarations set status = new.status, refid = new.refid  --BS70284
        where declgroup = new.ref and status is distinct from new.status;
  end
end^
SET TERM ; ^
