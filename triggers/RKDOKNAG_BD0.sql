--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_BD0 FOR RKDOKNAG                       
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable token integer;
begin
  select token
    from rkrapkas
  where ref = old.raport
  into :token;
  if(:token is null) then token = 0;
  if (token<>7) then begin
    if (old.numer>0) then
      exception RK_ZOSTAL_WYSTAWIONY_DOKUMENT;
  end
end^
SET TERM ; ^
