--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHOPERS_AI0 FOR PRSHOPERS                      
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  if(coalesce(new.opertimeupdate,0) = 0 and
    coalesce(new.workplace,'')<> '' and coalesce(new.PRSHOPER_CLONE,0) <> 1) then
  BEGIN
    INSERT INTO PRSHOPERSWORKPLACES(PRSHOPER, PRWORKPLACE, PRMACHINE, OPERTIMEPF, OPERTIME, MAIN, opertimeupdate, prshoper_clone)
         VALUES (new.ref, new.workplace, new.prmachine,coalesce(new.opertimepf,0), coalesce(new.opertime,0), 1,1, new.prshoper_clone );

    update PRSHOPERSWORKPLACES p
      set p.opertimeupdate = 0
    where p.prshoper = new.REF
      and P.main = 1;
  END

  if((new.econdition is not null and new.econdition <>'')
    or (new.eoperfactor is not null and new.eoperfactor <>'')
    or (new.eopertime is not null and new.eopertime <>'')
    or (new.eplacetime is not null and new.eplacetime <>'')
  ) then begin
     execute procedure prsheets_calculate(new.sheet,'','S');
  end
  if(new.opertime > 0) then execute procedure PRSHEETS_TECHJEDNQUANTITY_CALC(new.sheet);
end^
SET TERM ; ^
