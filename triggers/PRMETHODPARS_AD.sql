--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRMETHODPARS_AD FOR PRMETHODPARS                   
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  if (old.partype = 'G') then
    delete from prmethodparvals p where p.method = old.method and p.prmethodpar = old.symbol;
end^
SET TERM ; ^
