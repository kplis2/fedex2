--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACCESSORIES_BI0 FOR MWSACCESSORIES                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSACCESSORIES') returning_values new.ref;
end^
SET TERM ; ^
