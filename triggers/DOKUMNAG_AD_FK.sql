--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_AD_FK FOR DOKUMNAG                       
  ACTIVE AFTER DELETE POSITION 3 
as
declare variable konf varchar(255);
declare variable bkref integer;
declare variable blokadasid smallint;
declare variable nieks smallint;
begin
  if((old.akcept = 1) or (old.akcept = 8))then begin
    --sprawdzenie czy okres ksiegowy dla company jest juz zablokowany
    select min(sidblocked) from bkperiods
      where ptype = 1 and old.data>=sdate and old.data<=fdate and company = old.company
      into :blokadasid;
    select D.nieksiegowy from defmagaz D
      where D.symbol = old.magazyn
      into :nieks;
    if(:nieks is null) then nieks = 0;
    if (:blokadasid = 1 and nieks = 0) then exception SID_FK_BLOKADAKSIEGOWA;
    --deakceptacja - sprawdzenie, czy wycofac dekretacje
    execute procedure GETCONFIG('SIDFK_DOKBLOK') returning_values :konf;
    execute procedure GETCONFIG('SIDFK_DOKBLOK') returning_values :konf;
    if(:konf = '1' or (:konf = '2')) then begin
      select max(ref) from BKDOCS where OTABLE='DOKUMNAG' and OREF = old.ref into :bkref;
      if(:bkref > 0) then begin
        if(:konf = '1') then exception DOKUMNAGFK_ZAKSIEGOWANY;
        else begin
          select sidblocked from bkperiods
            where ptype = 1 and old.data>=sdate and old.data<=fdate and company = old.company
          into :blokadasid;
          if (:blokadasid = 1) then exception SID_FK_BLOKADAKSIEGOWA;
          execute procedure DOCUMENTS_DELFROM_BKDOCS(1,old.ref);
        end
        update DOKUMNAG set BLOKADA = bin_and(BLOKADA,3) where bin_and(BLOKADA,4)>0 and REF=old.ref;
      end
    end
  end
end^
SET TERM ; ^
