--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_BI_COUNTBASE FOR EABSENCES                      
  ACTIVE BEFORE INSERT POSITION 6 
AS
begin
--Rozliczenie nieobecnosci, ustalenie podstawy miesiecznej, stawki dziennej i godzinowej

  execute procedure e_calculate_countbase (new.ref, 1, new.mflag, new.employee,
     new.ecolumn, new.fromdate, new.todate, new.dokdate, new.lbase, new.fbase,
     new.baseabsence, new.calcpaybase, new.correction, new.worksecs, new.certserial,
     new.certnumber, new.dimnum, new.dimden, new.npdays, new.company)
  returning_values new.monthpaybase, new.daypayamount, new.hourpayamount, new.payamount;

end^
SET TERM ; ^
