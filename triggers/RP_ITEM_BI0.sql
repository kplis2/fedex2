--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RP_ITEM_BI0 FOR RP_ITEM                        
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.ref_id is null or (new.ref_id = 0)) then
    execute procedure GEN_REF('RP_ITEM') returning_values new.ref_id;
    if(new.TYP = 0) then  new.NAME ='Definicja obiektu';
    else if(new.TYP = 1) then new.NAME = new.STABLE;
    else if(new.TYP = 2 or (new.TYP = 3)) then new.NAME = new.FIELD;

end^
SET TERM ; ^
