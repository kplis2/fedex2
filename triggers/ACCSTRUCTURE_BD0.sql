--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTURE_BD0 FOR ACCSTRUCTURE                   
  ACTIVE BEFORE DELETE POSITION 0 
as
  declare variable tmp BKSYMBOL_ID;
  declare variable yearid int;
  declare variable company int;
begin
  select symbol, yearid, company from bkaccounts where ref = old.bkaccount
    into :tmp, :yearid, :company;
  if (exists(select ref from accounting where yearid = :yearid and account starting with :tmp and company = :company)) then
    exception fk_cant_delete_bkaccount;
end^
SET TERM ; ^
