--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTIFSHEETSTATES_AI_ORDER FOR NOTIFSHEETSTATES               
  ACTIVE AFTER INSERT POSITION 20 
as
declare variable maxnr integer;
begin
  select max(n.number)
    from notifsheetstates n
    where n.ref <> new.ref
    into :maxnr;

  if (maxnr is null) then maxnr = 1;

  if (new.number < :maxnr) then
    update notifsheetstates n set n.number = n.number + 1
      where n.number >= new.number and n.ref <> new.ref;
end^
SET TERM ; ^
