--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYCEN_AU1 FOR STANYCEN                       
  ACTIVE AFTER UPDATE POSITION 1 
as
declare variable cnt integer;
declare variable zablokow numeric(14,4);
declare variable zarezerw numeric(14,4);
declare variable zamowiono numeric(14,4);
begin
   zablokow = 0;
   zarezerw = 0;
   zamowiono = 0;
   if(new.zablokow <> old.zablokow or (new.zarezerw <> old.zarezerw) or (new.zamowiono <> old.zamowiono) )then begin
     select count(*) from STANYIL where MAGAZYN=new.magazyn and KTM=new.ktm and WERSJA=new.wersja into :cnt;
     if(:cnt is null) then cnt = 0;
     zablokow = new.zablokow - old.zablokow;
     zarezerw = new.zarezerw - old.zarezerw;
     zamowiono = new.zamowiono - old.zamowiono;
     if(:cnt > 0) then
       update STANYIL set
         ZAREZERW = ZAREZERW + :zarezerw,
         ZABLOKOW = ZABLOKOW + :ZABLOKOW,
         ZAMOWIONO = ZAMOWIONO + :zamowiono
         where MAGAZYN=new.magazyn and KTM=new.ktm and WERSJA=new.wersja;
     else
       insert into STANYIL(MAGAZYN, KTM, WERSJA, ILOSC, CENA, WARTOSC, ZAREZERW, ZABLOKOW, ZAMOWIONO)
       values(new.magazyn, new.ktm, new.wersja, 0, 0, 0, :zarezerw, :zablokow, :zamowiono);
  end
end^
SET TERM ; ^
