--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDESPOS_AU0 FOR PRSCHEDGUIDESPOS               
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable prschedoper integer;
declare variable prpreprod integer;
begin
  select g.prpreprod
    from prschedguides g
    where g.ref = new.prschedguide
    into prpreprod;
  if (prpreprod is null) then prpreprod = 0;
-- wyliczanie amountin na pierwszej operacji do przewodnika
  if(new.amountzreal <> old.amountzreal or new.amountoverlimit <> old.amountoverlimit) then
  begin
    prschedoper = null;
    for
      select po.ref
        from prschedopers po
        where po.guide = new.prschedguide and po.ref = new.prschedoper
          and not exists (select first 1 1 from prschedoperdeps pd where pd.depto = po.ref)
          and not exists (select first 1 1 from prschedguides g where g.ref = new.prschedguide and g.docfromoperrap > 0)
        group by po.ref
        into prschedoper
    do begin
      if (prschedoper is not null) then
        execute procedure prschedoper_amountin(:prschedoper);
    end
    if (new.pozzamref is not null) then
      execute procedure prpozzam_amountzreal(new.pozzamref);
  end
  if(new.amount <> old.amount and new.pggamountcalc = 0 and prpreprod = 0) then
    execute procedure PRSCHEDGUIDESPOS_AMOUNTcalc(null,new.ref);
end^
SET TERM ; ^
