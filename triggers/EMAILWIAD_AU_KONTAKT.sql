--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMAILWIAD_AU_KONTAKT FOR EMAILWIAD                      
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable newtypkon integer;
declare variable oldtypkon integer;
declare variable emadrref integer;
declare variable emcpodmiot integer;
declare variable empkosoba integer;
begin
  select TYPKONTAKTU from DEFFOLD where REF=old.folder into :oldtypkon;
  select TYPKONTAKTU from DEFFOLD where REF=new.folder into :newtypkon;
  if(
    ((:newtypkon<>:oldtypkon) or (:newtypkon is NULL and :oldtypkon is not NULL) or (:newtypkon is not NULL and :oldtypkon is NULL)) or
    (new.stan<>old.stan) or
    ((new.tytul<>old.tytul) or (new.tytul is NULL and old.tytul is not NULL) or (new.tytul is not NULL and old.tytul is NULL)) or
    ((new.data<>old.data) or (new.data is NULL and old.data is not NULL) or (new.data is not NULL and old.data is NULL))
  ) then begin
    for /* Iterate for   table */
        select REF
        from EMAILADR
        where (EMAILADR.emailwiad=old.ref)
        into :emadrref
    do begin
      execute procedure WYREJESTRUJ_KONTAKT(:emadrref);
    end /* for   table */
    for /* Iterate for   table */
        select REF,CPODMIOT,PKOSOBA
        from EMAILADR
        where (EMAILADR.emailwiad=new.ref)
        into :emadrref,:emcpodmiot,:empkosoba
    do begin
      execute procedure REJESTRUJ_KONTAKT(:emadrref,new.ref,:emcpodmiot,:empkosoba);
    end /* for   table */
  end
end^
SET TERM ; ^
