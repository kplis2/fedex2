--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ETRAININGS_BIU_STATUS FOR ETRAININGS                     
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.status=0) then
    update EMPLTRAININGS set STATUS=0
      where TRAINING = new.REF;
  else
    if (new.TODATE is null) then
      exception BRAK_DATY_ZAKONCZENIA;
  else
    update EMPLTRAININGS
      set STATUS=1, COMPLDATE=new.TODATE
      where TRAINING=new.REF and STATUS=0;
end^
SET TERM ; ^
