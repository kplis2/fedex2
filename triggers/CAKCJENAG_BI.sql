--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CAKCJENAG_BI FOR CAKCJENAG                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable tworzykontakt smallint;
BEGIN
  if(new.typpkt=0) then new.typpkt = NULL;
  if(new.cykliczne is null) then new.cykliczne = 0;

  if(new.nazwa='') then
    exception AKCJEMAR_NAZWA;

  select tworzkon from defzadan where ref = new.zadanie into :tworzykontakt;
  if(:tworzykontakt=1 and new.rodzaj is null) then
    exception AKCJEMAR_TYPKONTAKTU;
  if(new.iloscpoz is null) then new.iloscpoz = 0;
  if(new.iloscpozwyk is null) then new.iloscpozwyk = 0;
  if(new.procentreal is null) then new.procentreal = 0.00;
END^
SET TERM ; ^
