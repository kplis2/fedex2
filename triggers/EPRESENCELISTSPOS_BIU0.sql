--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRESENCELISTSPOS_BIU0 FOR EPRESENCELISTSPOS              
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable minutes varchar(10);
declare variable hours varchar(10);
declare variable cut smallint;
declare variable timestampstr varchar(20);
declare variable listdate date;
declare variable shift smallint;
begin
  if (new.docref = 0) then new.docref = null;
  if (new.econtractposdef = 0) then new.econtractposdef = null;
  if (new.doctype <> 'LISTYWYSD' and new.doctype <> 'DOKUMNAG' and new.doctype <> 'PROPERSRAPS') then
    new.doctype = null;
  if (new.pointssource is null) then new.pointssource = 1;
  if (new.timestartstr is not null and new.timestartstr <> '') then
  begin
    select listsdate
      from epresencelists
      where ref = new.epresencelists
      into listdate;
    select shift from epresencelistnag where ref = new.epresencelistnag
      into shift;
    new.timestartstr = replace(new.timestartstr,',',':');
    new.timestartstr = replace(new.timestartstr, '.', ':');
    new.timestartstr = replace(new.timestartstr, ';', ':');
    cut = position(':' in new.timestartstr);
    if (:cut = 0) then
      exception TIME_EXPT;
    else
    begin
      hours = substring(new.timestartstr from 1 for :cut-1);
      minutes = substring(new.timestartstr from :cut+1);  -- [DG] XXX ZG119346
      if (coalesce(char_length(:hours),0)=1) then  -- [DG] XXX ZG119346
        hours = '0'||:hours;
      if (coalesce(char_length(:minutes),0)=1) then  -- [DG] XXX ZG119346
        minutes = '0'||:minutes;
      if (coalesce(char_length(:hours),0)<> 2 or coalesce(char_length(:minutes),0)<>2) then  -- [DG] XXX ZG119346
        exception TIME_EXPT 'Niewłaściwy format godzin lub minut';
      if (cast(:hours as integer) < 0 or cast(:hours as integer) > 23) then
        exception TIME_EXPT 'Niedopuszczalna godzina';
      if (cast(:minutes as integer) < 0 or cast(:minutes as integer) > 59) then
        exception TIME_EXPT 'Niedopuszczalny wymiar minut';
      timestampstr = :listdate||' '||:hours||':'||:minutes||':'||'00';
      new.timestart = cast(:timestampstr as timestamp);
      new.timestartstr = :hours||':'||:minutes;
    end
  end
  if (new.timestopstr is not null and new.timestopstr <> '') then
  begin
    select epresencelists.listsdate
      from epresencelists
      where epresencelists.ref = new.epresencelists
      into :listdate;
    new.timestopstr = replace(new.timestopstr,',',':');
    new.timestopstr = replace(new.timestopstr, '.', ':');
    new.timestopstr = replace(new.timestopstr, ';', ':');
    cut = position(':' in new.timestopstr);
    if (:cut = 0) then
      exception TIME_EXPT;
    else
    begin
      hours = substring(new.timestopstr from 1 for :cut-1);
      minutes = substring(new.timestopstr from :cut+1 for coalesce(char_length(new.timestopstr),0));  -- [DG] XXX ZG119346
      if (coalesce(char_length(:hours),0)=1) then  -- [DG] XXX ZG119346
        hours = '0'||:hours;
      if (coalesce(char_length(:minutes),0)=1) then  -- [DG] XXX ZG119346
        minutes = '0'||:minutes;
      if (coalesce(char_length(:hours),0)<> 2 or coalesce(char_length(:minutes),0)<>2) then  -- [DG] XXX ZG119346
        exception TIME_EXPT 'Niewłaściwy format godzin lub minut';
      if (cast(:hours as integer) < 0 or cast(:hours as integer) > 23) then
        exception TIME_EXPT 'Niedopuszczalna godzina';
      if (cast(:minutes as integer) < 0 or cast(:minutes as integer) > 59) then
        exception TIME_EXPT 'Niedopuszczalny wymiar minut';
      timestampstr = :listdate||' '||:hours||':'||:minutes||':'||'00';
      new.timestop = cast(:timestampstr as timestamp);
      new.timestopstr = :hours||':'||:minutes;
    end
  end
  if (shift = 2 and new.timestop < new.timestart) then
    new.timestop = new.timestop + 1;
  if (new.timestart > new.timestop) then
    exception TIME_EXPT 'Godzina rozpoczęcia musi być wcześniejsza niz zakończenia';
  new.jobtimeh = cast((new.timestop - new.timestart)*24 as integer);
  new.jobtimeq = cast(cast(cast((new.timestop - new.timestart)*96 as integer) as numeric(14,2))/4 as numeric(14,2));

  new.jobtime = (new.timestop - new.timestart) * 24 * 60 * 60;

  execute procedure durationstr2(new.jobtime) returning_values new.jobtimestr;
end^
SET TERM ; ^
