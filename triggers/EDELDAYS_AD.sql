--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDELDAYS_AD FOR EDELDAYS                       
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  if(exists(select first 1 1 from edelegations where ref = old.delegation)) then
    execute procedure edel_count_all(old.delegation);
end^
SET TERM ; ^
