--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRPOS_AIDU_AUTOCALCULATE FOR EPRPOS                         
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 1 
AS
begin
--Przeliczenie pozycji 'auto-przeliczanych'; Aktualizacja mechanizmu - PR31748

  if (rdb$get_context('USER_TRANSACTION', 'EPRPOS_DONOTCALC') = 1) then exit;  --PR53533

  if (deleting) then
  begin
    if (old.cauto <> 2) then
      execute procedure e_calculate_eprpos(old.employee, old.payroll, old.ecolumn);
  end else begin
    if (new.cauto = 0 and (new.ecolumn is distinct from old.ecolumn
                         or new.pvalue is distinct from old.pvalue)
    ) then
      execute procedure e_calculate_eprpos(new.employee, new.payroll, new.ecolumn);
  end
end^
SET TERM ; ^
