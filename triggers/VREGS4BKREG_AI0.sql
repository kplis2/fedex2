--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VREGS4BKREG_AI0 FOR VREGS4BKREG                    
  ACTIVE AFTER INSERT POSITION 0 
as
begin
  if (new.isdefault = 1) then
    update VREGS4BKREG set isdefault = 0
      where bkreg = new.bkreg and company = new.company and isdefault = 1 and vatreg <> new.vatreg;
end^
SET TERM ; ^
