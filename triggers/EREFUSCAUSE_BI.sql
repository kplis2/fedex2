--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EREFUSCAUSE_BI FOR EREFUSCAUSE                    
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_erefuscause,1);
end^
SET TERM ; ^
