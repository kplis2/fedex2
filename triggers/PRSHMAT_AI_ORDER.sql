--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHMAT_AI_ORDER FOR PRSHMAT                        
  ACTIVE AFTER INSERT POSITION 0 
as
begin
  if (new.ord <> 1) then
    execute procedure ORDER_PRSHMAT(new.sheet, new.opermaster, new.number, new.number);
end^
SET TERM ; ^
