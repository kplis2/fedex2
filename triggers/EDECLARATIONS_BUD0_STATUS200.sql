--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDECLARATIONS_BUD0_STATUS200 FOR EDECLARATIONS                  
  ACTIVE BEFORE UPDATE OR DELETE POSITION 0 
as
declare variable responding varchar(10);
begin
  if (deleting or (updating and (new.edecldef <> old.edecldef
      or new.status is distinct from old.status
      or new.refid is distinct from old.refid
      or new.fromdate is distinct from old.fromdate
      or new.todate is distinct from old.todate
      or new.regtimestamp is distinct from old.regtimestamp
      or new.person is distinct from old.person
      or new.emonth is distinct from old.emonth
      or new.eyear is distinct from old.eyear
      or new.period is distinct from old.period
      or new.state is distinct from old.state
      or new.filename is distinct from old.filename
      or new.correction is distinct from old.correction
      or new.enclosureowner is distinct from old.enclosureowner
      or new.company is distinct from old.company
      or new.declgroup is distinct from old.declgroup))
  ) then begin
    if (old.status = 200) then
      exception edecl_status200;
    else begin
      responding = coalesce(rdb$get_context('USER_TRANSACTION', 'EDE_PROCESS_RESPOND_Run'),'');

      if (old.state > 0 and responding <> '1' and (deleting
        or (old.state is not distinct from new.state  --wpp podpisanie czy wyslanie niemozliwe
          and new.declgroup is not distinct from old.declgroup))  --wpp grupownie niemozliwe
      ) then
        exception edecl_state;
    end
  end
end^
SET TERM ; ^
