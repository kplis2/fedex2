--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRESENCELISTSPOS_AU_DOCTYPE FOR EPRESENCELISTSPOS              
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.doctype <> old.doctype
     or new.doctype is not null and old.doctype is null
     or new.doctype is null and old.doctype is not null
     or new.docref is not null and old.docref is null
     or new.docref <> old.docref
     or new.docref is null and old.docref is not null ) then
  begin
    if (old.doctype = 'LISTYWYSD' and old.docref > 0) then
      update listywysd set listywysd.eprlistposused = 0 where listywysd.ref = old.docref;
    if (old.doctype = 'DOKUMNAG' and old.docref > 0) then
      update dokumnag set dokumnag.eprlistposused = 0 where dokumnag.ref = old.docref;
    if (new.doctype = 'LISTYWYSD' and new.docref > 0) then
      update listywysd set listywysd.eprlistposused = 1 where listywysd.ref = new.docref;
    if (new.doctype = 'DOKUMNAG' and new.docref > 0) then
      update dokumnag set dokumnag.eprlistposused = 1 where dokumnag.ref = new.docref;
  end
end^
SET TERM ; ^
