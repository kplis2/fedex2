--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTAWY_BI FOR DOSTAWY                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable skrot varchar(40);
declare variable symbfak varchar(80);
declare variable okres varchar(6);
declare variable symrejestr varchar(20);
declare variable ildni integer;
declare variable dostsymbfak varchar(20);
declare variable symbkonc varchar(80);
BEGIN
  if((new.status is null) or (new.status = ' ')) then new.status = 'O';
  if(new.data is null) then new.data = current_date;
  if(new.dataotw is null) then new.dataotw = current_date;
  if(new.isparam is null) then new.isparam = 0;
  okres = cast( extract( year from new.data) as char(4));
  if(new.indywidualdost is null) then new.indywidualdost = 0;

  if(extract(month from new.data) < 10) then
      okres = :okres ||'0' ||cast(extract(month from new.data) as char(1));
  else
      okres = :okres ||cast(extract(month from new.data) as char(2));
  if(new.okres is null or (new.okres = '      ')) then
   new.okres = :okres;
  else begin
    if(new.okres <> :okres) then exception DOSTAWY_DATA_NIE_W_OKRESIE;
  end
  new.aktualizuj = 0;

END^
SET TERM ; ^
