--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPEDYTTRAS_AU_REPLICAT FOR SPEDYTTRAS                     
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
 if((new.trasa <> old.trasa) or (new.spedytor <> old.spedytor)) then
   update SPEDYTORZY set STATE = -1 where SYMBOL = new.spedytor;
 if(new.spedytor <> old.spedytor) then
   update SPEDYTORZY set STATE = -1 where SYMBOL = old.spedytor;
end^
SET TERM ; ^
