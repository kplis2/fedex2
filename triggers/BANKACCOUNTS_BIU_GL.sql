--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKACCOUNTS_BIU_GL FOR BANKACCOUNTS                   
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
as
declare variable cnt integer;
begin
  if(new.gl is null) then new.gl = 0;
  if(new.gl = 0 and new.act = 1 and new.hbstatus = 0) then
  begin
    select count(ref)
      from bankaccounts
      where DICTDEF = new.DICTDEF and DICTPOS = new.DICTPOS and ref <> new.ref
      into :cnt;
    if(coalesce(cnt,0) = 0) then
      new.gl = 1;
  end

  if(new.bank = '') then new.bank = null;

  if(new.account = '' and new.hbstatus = 0) then
    exception slobanacc_emptyacc;

  -- dopisanie do konta lancuch znakow "glowny"
  if(new.gl = 1) then
    new.id = new.account || ' (GŁÓWNY)';
  else
    new.id = new.account;
  -- sprawdzenie czy nie jest dodawany/zmieniany rekord nie jest czasem glowny i nieaktywny
  if(new.gl = 1 and new.act = 0) then
    exception KONTO_GLOWNE;

  -- sprawdzenie czy nie jest dodawany/zmieniany rekord nie jest czasem glowny i do wplat
  if(new.gl = 1 and new.hbstatus = 1) then
    exception KONTO_DO_WPLAT;

end^
SET TERM ; ^
