--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYSER_BU_REZ FOR STANYSER                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if(new.ilosc<>old.ilosc or new.zablokow<>old.zablokow) then begin
    if(new.ilosc<new.zablokow) then
        exception STANYSER_MINUS 'Brak ilosci niezablokowanej dla serii: '||new.serialnr;
  end
end^
SET TERM ; ^
