--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHTOOLS_BU FOR PRSHTOOLS                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (new.opermaster is null or new.opermaster = 0)
    then exception prsheets_data 'Definiowanie narzedzia dozwolone wyłącznie dla operacji.';
  new.ehourfactor = replace(new.ehourfactor,',','.');
  new.esheetrate = replace(new.esheetrate,',','.');
  new.econdition = replace(new.econdition,',','.');
  if(new.symbol <> old.symbol or (new.symbol is not null and old.symbol is null)) then
    select prtools.tooltype from prtools where symbol = new.symbol into new.tooltype;
end^
SET TERM ; ^
