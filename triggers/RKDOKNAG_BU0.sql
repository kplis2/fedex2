--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_BU0 FOR RKDOKNAG                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable data1 timestamp;
  declare variable data2 timestamp;
  declare variable slownik integer;
  declare variable slodef integer;
  declare variable slowymog integer;
  declare variable typ_k varchar(3);
  declare variable typ_b varchar(3);
  declare variable kasabank char;
  declare variable waluta varchar(3);
  declare variable operacja_walutowa smallint;
  declare variable stanowisko_walutowe smallint;
  declare variable stanowisko_waluta varchar(3);
  declare variable country_currency varchar(3);
  declare variable local integer;
  declare variable rkdokpozkwota numeric(14,2);
  declare variable rkdokpozkwotazl numeric(14,2);
  declare variable status integer;
  declare variable electronic integer;
  declare variable BLOCKREF integer;
  declare variable getnumdok varchar(40);
begin
  if (new.blokada is null)
    then new.blokada = 0;
  execute procedure CHECK_LOCAL('RKDOKNAG',old.ref) returning_values :local;
  select PM, TYP_DOK, BTYPDOK, SLOWNIK, SLODEF, SLOWYMOG, WALUTOWA
    from RKDEFOPER
    where SYMBOL = new.OPERACJA
    into new.pm, :typ_k, :typ_b, :slownik, :slodef, :slowymog, :operacja_walutowa;

  select kasabank, walutowa, waluta from rkstnkas
    where kod = new.stanowisko
    into :kasabank, :stanowisko_walutowe, :stanowisko_waluta;

  if (kasabank='K') then
    new.typ = typ_k;
  else
    new.typ = typ_b;

  if (new.slodef<>old.slodef and new.slodef > 0 and :slownik = 0)
    then exception RK_DOKNAG_NOTSLO;

  if (new.slopoz<>old.slopoz and :slowymog = 1 and (new.slopoz = 0 or (new.slopoz is null)))
    then exception RK_DOKNAG_SLOWYMOG;

  if (new.dla<>old.dla and coalesce(new.dla,'')='' and :slowymog = 1)
    then exception RK_DOKNAG_BEZDLA;

  if (new.slodef<>old.slodef and :slodef is not null and :slodef <> new.slodef)
    then exception RK_DOKNAG_SLONOTDEFOPER;

  select DATAOD, DATADO from RKRAPKAS
    where ref=new.raport into data1, data2;
  if (new.electronic = 0 and (new.data<:data1 or new.data>:data2))
    then exception RK_NIEPRAWIDLOWA_DATA;

  select RKSTNKAS.WALUTA, RKSTNKAS.KOD from RKSTNKAS, RKRAPKAS
    where RKRAPKAS.REF = new.RAPORT and RKSTNKAS.KOD = RKRAPKAS.STANOWISKO
    into :waluta, new.STANOWISKO;

  if (new.waluta is null or new.waluta ='') then
    new.WALUTA = :waluta;

  execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
    returning_values :country_currency;
  if (new.waluta <>:country_currency) then
    new.walutowy=1;
  else
    new.walutowy=0;


  if(new.PM='+') then begin
    new.PRZYCHODZL = new.KWOTAZL;
    new.ROZCHODZL = 0;
    if (waluta <> country_currency) then
    begin
      if (old.kwota is null and new.kwota is not null) then
        new.stanwal = new.kwota;
      else
        new.stanwal = new.stanwal + (new.kwota - old.kwota);
    end
  end else
  begin
    new.PRZYCHODZL = 0;
    new.ROZCHODZL = new.KWOTAZL;
  end


  if (new.electronic = 1 and old.status < 2 and new.status = 2) then
  begin
    select sum(case
            when r.PM = '+'
            then r.kwota
            else  -r.kwota
        end )as sum1,
           sum(case
            when r.PM = '+'
            then r.kwotazl
           else  -r.kwotazl
    end)
      from rkdokpoz r
      where r.dokument = new.ref
        and coalesce(r.inneks,0) = 0
      into :rkdokpozkwota, :rkdokpozkwotazl;
      rkdokpozkwota = abs(rkdokpozkwota);
      rkdokpozkwotazl = abs(rkdokpozkwotazl);
    if (rkdokpozkwota <> new.kwota or rkdokpozkwotazl <> new.kwotazl) then
      exception universal 'Suma transakcji niezgodna z kwotą pozycji. Akceptacja niemożliwa.';
  end
  -- zwolnienie numeru przy zmianie kierunku operacji
  if(new.pm<>old.pm) then begin
    -- zwrot numeru zablokowanego
    -- BS45014 - wycito, bo teraz numer jest ZAWSZE zwracany przy wycofaniu akcetacji, a bez tego zmiana operacji nei jest możliwa.
  end
  if(new.numer = 0 and old.numer > 0) then
  begin
      -- zwrot numeru tylko, kiedy nie było blokady
      select GENNUMDOK from RKSTNKAS where KOD = old.stanowisko into :getnumdok;
      execute procedure FREE_NUMBER(:getnumdok,old.stanowisko, old.typ, old.data, old.numer, new.numblockget)
        returning_values new.numblockget;
  end

  -- kontrola homebankingowa
  select status, electronic from RKRAPKAS where REF = new.raport
    into :status, :electronic;
  if (electronic = 1 and status > 0
    and (old.operacja <> new.operacja or old.pm <> new.pm
      or old.slopoz <> new.slopoz or old.kwotazl <> new.kwotazl)
  ) then
    exception universal 'Wyciąg zamknięty. Edycja pozycji niemożliwa.';

  if (:electronic = 0 and coalesce(old.numer,0) <> coalesce(new.numer,0)) then
    if (coalesce(new.numer,0) > 0) then
      new.status = 2;
    else new.status = 0;

  --kontrola waluty, kursu
  if(stanowisko_walutowe = 0 and operacja_walutowa = 0 and (new.waluta <> :stanowisko_waluta or new.kurs <> 1)) then
    exception universal 'Wybrana nieprawidłowa waluta lub podany nieprawidłowy kurs.';
  if(stanowisko_walutowe = 1 and new.waluta <> :stanowisko_waluta) then
    exception universal 'Wybrana nieprawidłowa waluta.';
end^
SET TERM ; ^
