--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INTERESTTABLES_AU_ISDEFAULT FOR INTERESTTABLES                 
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if ((new.isdefault <> old.isdefault) and new.isdefault = 1) then
    update INTERESTTABLES set isdefault = 0 where ref <> new.ref and isdefault = 1;

end^
SET TERM ; ^
