--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDS_AI0 FOR MWSORDS                        
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable grupasped integer;
begin
  if (new.status not in (0,6) and new.docid is not null) then
  begin
    select coalesce(g.grupasped, g.ref) from dokumnag g where g.ref = new.docid
      into grupasped;
  --  execute procedure dokumnag_mwsrealstatus(grupasped,1);
  end
end^
SET TERM ; ^
