--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VATREGS_BIU_BLANK FOR VATREGS                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.dtaxgr = '') then new.dtaxgr = null;
  if (new.dvatgr = '') then new.dvatgr = null;
  if (new.symbol = '') then new.symbol = null;

end^
SET TERM ; ^
