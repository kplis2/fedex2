--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_BIU_ALTPOZ FOR DOKUMPOZ                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 11 
AS
declare variable zrodlo smallint;
declare variable zewn smallint;
declare variable wyd smallint;
declare variable kor smallint;
declare variable akcept smallint;
declare variable altposmode smallint;
begin
  if (coalesce(new.havefake,0) = 1 and coalesce(new.fake,0) = 1) then
    exception poz_fake 'Nie można dodawać pozycji alternatywne do pozycji będącej alternatywną';

  if (new.alttopoz is not null) then
  begin
    select zrodlo
      from dokumnag
      where ref = new.dokument
      into :zrodlo;
    if( zrodlo > 0 and zrodlo < 90) then
      if (new.frompozzam is null and new.frompozfak is null and
          not exists (select first 1 1 from dokumpoz
                          where dokument = new.dokument and ref = new.alttopoz
                          and ( frompozzam is not null or frompozfak is not null))) then
        exception poz_fake;
   end

  if (updating and coalesce(old.ktm,'') <> '' and new.ktm <> old.ktm and coalesce(old.havefake,0) = 1) then
  begin
    select coalesce(t.altposmode,0) from towary t where t.ktm = old.ktm
    into :altposmode;

    if (:altposmode > 0) then exception poz_fake 'Nie można modyfikować KTM dla pozycji definiującej pozycje alternatywne';
  end
   --do wyjasnienia
   /*$$IBEC$$ if (new.havefake = 1 and coalesce(new.dostawa,0) = 0) then
   begin
     select d.zewn, d.wydania, d.koryg, d.akcept
       from dokumnag d
       where d.ref = new.dokument
       into zewn, wyd, kor, akcept;
     if (zewn = 1 and wyd = 1 and kor = 0 and akcept = 0) then
       execute procedure DOKUMNAG_CREATE_DOKMAG_DOSTAWA(new.dokument,NULL,current_date,'M88',NULL,new.ref,'',NULL)
         returning_values new.dostawa;
   end $$IBEC$$*/
end^
SET TERM ; ^
