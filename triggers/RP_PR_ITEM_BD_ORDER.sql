--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RP_PR_ITEM_BD_ORDER FOR RP_PR_ITEM                     
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 5) then exit;
  update rp_pr_item set ord = 1, numer = numer - 1
    where ref_id <> old.ref_id and numer > old.numer and program = old.program;
end^
SET TERM ; ^
