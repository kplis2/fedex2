--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLOYMENT_BI0_REF FOR EMPLOYMENT                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EMPLOYMENT')
      returning_values new.REF;
end^
SET TERM ; ^
