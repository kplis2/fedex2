--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAK_AU_REZ FOR POZFAK                         
  ACTIVE AFTER UPDATE POSITION 1 
as
declare variable ack smallint;
declare variable blokada smallint;
declare variable nieobrot smallint;
declare variable local smallint;
declare variable skad smallint;
begin
  if(new.wersjaref <> old.wersjaref or (new.dostawa<>old.dostawa) or (new.dostawa is not null and old.dostawa is null) or (new.dostawa is null and old.dostawa is not null)
      or (new.magazyn <> old.magazyn)) then
    delete from STANYREZ where POZZAM = new.ref and ZREAL = 2;
  if(((new.ilosc - new.pilosc) <> (old.ilosc - old.pilosc)) or
      (new.wersjaref <> old.wersjaref) or
      (new.dostawa <> old.dostawa) or (new.dostawa is not null and old.dostawa is null) or (new.dostawa is null and old.dostawa is not null) or
      (new.magazyn <> old.magazyn)) then begin
    select NAGFAK.AKCEPTACJA,TYPFAK.blokada,NAGFAK.NIEOBROT,nagfak.skad
    from NAGFAK
    left join TYPFAK on (NAGFAK.typ = TYPFAK.SYMBOL)
    where NAGFAK.REF=new.dokument into :ack, :blokada, :nieobrot,:skad;
    execute procedure CHECK_LOCAL('POZFAK',new.ref) returning_values :local;
    if(:ack <> 1 and :blokada = 1 and :nieobrot <>1 and :local = 1 and :skad <> 2 and :skad <> 1) then begin
      execute procedure REZ_SET_KTM_FAK(new.ref,2);
    end
  end
end^
SET TERM ; ^
