--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRAFIELDS_BD_ORDER FOR FRAFIELDS                      
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then exit;
  update frafields set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and frcol = old.frcol and frpsn = old.frpsn;
end^
SET TERM ; ^
