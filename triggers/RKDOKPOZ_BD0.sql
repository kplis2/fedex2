--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_BD0 FOR RKDOKPOZ                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.accept = 1) then
    exception RKDOKNAG_EXPT 'Pozycja dokumentu zaakceptowana. Usuniecie niemożliwe.';
end^
SET TERM ; ^
