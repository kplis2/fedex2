--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLODEF_AD_REPLICAT FOR SLODEF                         
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  execute procedure RP_TRIGGER_AD('SLODEF', old.ref, NULL, NULL, NULL, NULL, old.token, old.STATE);
end^
SET TERM ; ^
