--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMRCP_AD FOR PMRCP                          
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  update pmpositions set USEDQ = USEDQ - old.amount, USEDVAL = USEDQ * BUDPRICE where ref = old.pmposition;
end^
SET TERM ; ^
