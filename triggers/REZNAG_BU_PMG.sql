--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER REZNAG_BU_PMG FOR REZNAG                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable posref integer;
declare variable nazwa varchar(255);
declare variable ktm varchar(40);
declare variable cnt integer;
begin
  if (new.status = 3 and (new.status<>old.status or old.status is null)) then --zmiana statusu na zamkniety
  begin
   if (new.pmelement is not null and new.pmelement <> 0) then  -- rozliczenie nastpuje tylko gdy wybrany jest element
    begin
      select ktm from rezzasoby where rezzasoby.ref = new.rezzasob into :ktm; --szukamy ktmu wypozyczonego sprzetu
      if (:ktm is not null and :ktm <> '') then
      begin
        select count(ref) from pmpositions where pmelement = new.pmelement and ktm = :ktm into :cnt;
        if (:cnt <= 1) then                          -- istnieje wiecej niz jedna pozycja z naszym ktmem
        begin
          if (:cnt < 1) then                   -- gdy nie ma w ogole takiej pozycji to ja zakladamy
          begin
            select nazwa from towary where ktm = :ktm into nazwa;
            execute procedure GEN_REF('PMPOSITIONS') returning_values :posref;
            insert into pmpositions (REF, PMELEMENT, PMPLAN, NAME, SYMBOL, KTM, POSITIONTYPE, USEDVAL)
             values (:posref, new.pmelement, new.pmplan, :nazwa, :ktm, :ktm, 'S', 0);
            new.pmposition = :posref;
          end
          else
          begin
            select ref from pmpositions where pmelement = new.pmelement and ktm = :ktm into :posref;
            new.pmposition = :posref;
          end
        end
        else
          exception universal 'Wybrany element nie posiadaj jednoznaczej pozycji na której można rozliczyć wypożyczenie.';
      end
      else
        exception  universal 'Wybrany zasób nie jest skojarzony z numerem KTM. Rozliczenie nie możliwe.';
    end
  end
  else
  if (old.status = 3 and new.status <> old.status) then   --gdy status sie zmienil a stary byl 'zamkniety' trzeba wycofac rozliczenie
  begin
    if (new.pmelement is not null and new.pmelement <> 0) then
    begin
      if (old.pmposition is not null and old.pmposition <> 0) then
        new.pmposition = null;
    end
  end
end^
SET TERM ; ^
