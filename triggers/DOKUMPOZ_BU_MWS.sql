--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_BU_MWS FOR DOKUMPOZ                       
  ACTIVE BEFORE UPDATE POSITION 10 
as
  declare variable mwsdoc smallint;
  declare variable mwsdisposition smallint;
  declare variable akcept smallint;
  declare variable altposmode smallint;
begin
  if (new.ilosconmwsactsc is null) then new.ilosconmwsactsc = 0;
  if (new.ilosconmwsacts is null) then new.ilosconmwsacts = 0;
  if (new.mwsactsdone is null) then new.mwsactsdone = 0;
  if (new.mwsordswaiting is null) then
  begin
    select coalesce(m.mwsordwaiting,0)
      from dokumnag d
        left join defdokummag m on (m.typ = d.typ and m.magazyn = d.magazyn)
      where d.ref = new.dokument
      into new.mwsordswaiting;
  end
  if(new.iloscl <> old.iloscl or new.ilosconmwsacts <> old.ilosconmwsacts or new.ilosc <> old.ilosc)then
  begin
    select case when d.mwsdoc = 1 or d.mwsmag is not null then 1 else 0 end ,
        d.mwsdisposition, d.akcept
      from dokumnag d
      where ref = new.dokument
    into :mwsdoc, :mwsdisposition, :akcept;

    execute procedure MWS_SET_GENMWSORDSAFTER(new.dokument, new.ref, :mwsdoc, :mwsdisposition, :akcept, new.ilosc, new.iloscl, new.ilosconmwsacts)
      returning_values new.genmwsordsafter;
  end

  if (new.braktowaru_ilosc is distinct from old.braktowaru_ilosc) then
  begin
    new.braktowaru_ilosc_prev = coalesce(new.braktowaru_ilosc, 0);
  end

end^
SET TERM ; ^
