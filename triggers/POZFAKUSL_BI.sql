--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAKUSL_BI FOR POZFAKUSL                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable usluga smallint;
declare variable zakup smallint;
BEGIN
  if(new.rozliczono is null) then new.rozliczono = 0;
  if(new.tryb is null) then new.tryb = 0;

  select p.usluga, n.zakup
      from pozfak p
        join nagfak n on (p.dokument = n.ref)
      where p.ref = new.refpozfak
  into :usluga, :zakup;

  if(:usluga<>1 and new.tryb<>2) then exception POZFAKUSL_NIEUSLUGA;
  if(new.refnagfak=0) then new.refnagfak = null;
  if(:zakup = 1 and (new.kwota=0 or new.kwota is null)) then
    exception POZFAKUSL_ZLAKWOTA;
END^
SET TERM ; ^
