--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PERSONS_AU_EPERSDATASECUR FOR PERSONS                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
  declare variable aktuoperator integer;
begin
  execute procedure get_global_param ('AKTUOPERATOR')
    returning_values aktuoperator;

  insert into epersdatasecur (person, kod, descript, operator)
    values (new.ref, 1, 'Modyfikacja danych osobowych', :aktuoperator);
end^
SET TERM ; ^
