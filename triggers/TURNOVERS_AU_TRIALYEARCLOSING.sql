--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TURNOVERS_AU_TRIALYEARCLOSING FOR TURNOVERS                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
 declare variable periodbo varchar(6);
 declare variable yearid integer;
 declare variable decree integer;
begin
  if (((new.debit <> old.debit) or (new.credit <> old.credit)) and new.bktype = 0) then
  begin
    select yearid
      from bkperiods
      where id = new.period and company = new.company
      into yearid;

    select id from bkperiods
      where yearid = :yearid + 1 and substring(id from 5 for 2) = '00'
      and company = new.company and status > 0
      into periodbo;

    if ((periodbo is not null) and (not exists(select ref from bkdocs where period = :periodbo
                and autobo = 1 and company = new.company))
      ) then
    begin
      execute procedure update_bo_turnovers(new.company, yearid, new.accounting);
      for
        select ref
          from decrees where period = :periodbo and status > 1 and company = new.company
            and account = new.account
          into :decree
      do
        execute procedure compute_turnovers(decree);
    end
  end
end^
SET TERM ; ^
