--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CALLS_BI_INIT FOR CALLS                          
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (coalesce(new.begincall,'') = '') then
    new.begincall = current_timestamp;
end^
SET TERM ; ^
