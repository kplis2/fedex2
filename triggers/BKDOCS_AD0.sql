--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_AD0 FOR BKDOCS                         
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable typ smallint;
begin
  if (old.otable is not null and old.oref > 0) then
  begin
    if (old.otable = 'NAGFAK')then typ = 0;
    else if (old.otable = 'DOKUMNAG') then typ = 1;
    else if (old.otable = 'RKRAPKAS') then typ = 2;
    else if (old.otable = 'DOKUMNOT') then typ = 3;
    else if (old.otable = 'FSCLRACCH') then typ = 4;
    else if (old.otable = 'EPAYROLLS') then typ = 5;
    else if (old.otable = 'EPRBILLS') then typ = 6;
    else if (old.otable = 'VALDOCS') then typ = 7;
    else typ = -1;
    if (typ = 0) then
    begin
      update NAGFAK
        set nagfak.oldbkdocnum = old.number, nagfak.oldbkvdocnum = old.vnumber
        where (nagfak.ref = old.oref);
    end
    if (typ >= 0) then
      execute procedure DOCS_DEL_BKDOCS(typ, old.oref, old.company);
    else if (old.otable = 'NOTYNAG') then
      update NOTYNAG set STATUS = 1 where BKDOC = old.ref;
    else if (old.otable = 'NOTYNAG_A') then
      update NOTYNAG set ANULOWANIE = 0 where REF = old.oref;
  end
end^
SET TERM ; ^
