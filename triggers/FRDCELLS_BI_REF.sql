--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDCELLS_BI_REF FOR FRDCELLS                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FRDCELLS')
      returning_values new.REF;
end^
SET TERM ; ^
