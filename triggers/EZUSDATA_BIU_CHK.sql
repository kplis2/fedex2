--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EZUSDATA_BIU_CHK FOR EZUSDATA                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
  declare variable invalidity varchar(20);
  declare variable code varchar(20);
begin
  select code
    from EDICTZUSCODES
    where ref=new.Invalidity
    into :invalidity;
  if (Invalidity<>'0' and (new.InvalidityFrom is null)) then
    exception EZUSDATA_EXPT;

  if (new.blockdate is not null) then
    new.baseincyear = extract(year from new.blockdate);
  else new.baseincyear = null;

  if (new.baseinc is null) then new.baseinc = 0;
  select z.code
    from edictzuscodes z
    where z.ref = new.blockade
    into :code;
  if (code = '3') then new.baseinc = 0; 

  if (new.baseincyear < extract(year from new.fromdate)) then
    exception blockade_date;
end^
SET TERM ; ^
