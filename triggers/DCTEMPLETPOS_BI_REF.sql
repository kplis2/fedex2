--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DCTEMPLETPOS_BI_REF FOR DCTEMPLETPOS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DCTEMPLETPOS')
      returning_values new.REF;
end^
SET TERM ; ^
