--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLOPOZ_BIU_WPK FOR SLOPOZ                         
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable patternslodef slo_id;
declare variable currentcompany companies_id;
begin
  -- Generalnie należy sprawdzić, czy dodawany slopoz nie ma odpowiednika we wzorcu lub
  -- jako niesynchronizowany w zależnych.
  -- tak wic należy sprawdzić, czy sownik tego slopoza jest wszorcem, czy może zależnym
  select s.patternref
    from slodef s
    where s.ref = new.slownik
    into :patternslodef;
  if (coalesce(patternslodef,0)>0 and new.pattern_ref is null) then
  begin
    -- w takiej sytuacji jestesmy nową pozycją slownika zaleznego oraz
    -- nie jestesmy krotką dodaną ze wzorca, zatem nalezy sprawdzic czy
    -- we wzorcu nie ma juz slopoza z takim symbolem
    if (exists (select first 1 1
                  from slopoz sp
                  where sp.slownik = :patternslodef and sp.kod = new.kod
                )) then
    begin
      --niestety jest taki symbol we wzorcu i nie pozwalam
      exception slopoz_exists_in_pattern;
    end
  end else
  begin
    --generalnie teraz nie jestem zalezny ale moge byc wzorcem wiec sprawdzam
    execute procedure get_global_param('CURRENTCOMPANY')
      returning_values :currentcompany;
    if (exists (select first 1 1
                  from companies c
                  where c.ref = :currentcompany and coalesce(c.pattern,0)>0
                )) then
    begin
      --no to na pewno jestemy wzorcem. W takim razie nie ma rady, trzeba sprawdzić
      --czy w jakims naszyn zaleznym nie ma slopoza o takim symbolu
      if (exists (select first 1 1
                    from slopoz sp join slodef sd on (sp.slownik = sd.ref)
                    where sd.patternref = new.slownik and sp.kod = new.kod and (sp.pattern_ref <> new.ref or  sp.pattern_ref is null)
                  )) then 
      begin
        --niestety istnieje slopoz w slowniku zaleznym z takim symbolem wiec dzwon
        exception slopoz_exists_in_slave;
      end
    end
  end
end^
SET TERM ; ^
