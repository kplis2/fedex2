--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDPERSPECTS_BIU FOR FRDPERSPECTS                   
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable status smallint;
begin
  if (new.mainperspect is null) then new.mainperspect = 0;
  if (new.calcmeth is null) then new.calcmeth = 0;
  if (new.calcmain is null) then new.calcmain = 0;
  if (new.shortname is null or new.shortname = '') then
    exception FRDPERSPECTS_NO_SHORTNAME;
  select frhdrs.status
    from frhdrs
    where frhdrs.symbol = new.frhdr
    into :status;
  if (new.calcmain = 1 and new.calcmain <> old.calcmain) then
    if (exists(select ref from frdperspects where frdperspects.frhdr = new.frhdr
        and frdperspects.calcmain = 1)) then
      exception FRDPERSPECT_CALCMAIN_EXISTS;
  if (:status <> 0) then
    exception FRHDRS_NOT_ACCESIBLE;
end^
SET TERM ; ^
