--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACH_BI_FSBALANCES FOR ROZRACH                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable fsbalances integer;
  declare variable typ varchar(31);
  declare variable debit numeric(14,2);
  declare variable credit numeric(14,2);
begin
  if (new.slodef is not null) then
  begin
    select typ from slodef where ref=new.slodef into :typ;
    if (typ = 'KLIENCI') then
      select dictgrp from klienci where ref = new.slopoz
        into new.dictgrp;
    else if (typ = 'DOSTAWCY') then
      select dictgrp from dostawcy where ref = new.slopoz
        into new.dictgrp;
  end
  debit = 0;
  credit = 0;

  if (new.winien > new.ma) then debit = new.winien - new.ma;
  if (new.ma > new.winien) then credit = new.ma - new.winien;

  select ref from fsbalances
    where account = new.kontofk and dictdef = new.slodef and dictpos = new.slopoz
      and company = new.company and currency = new.waluta
    into :fsbalances;

  if (fsbalances is null) then
  begin
    insert into fsbalances (account, dictdef, dictpos, company, dictgrp, debit, credit, currency, btranamountbank)
      values (new.kontofk, new.slodef, new.slopoz, new.company,  new.dictgrp, :debit, :credit, new.waluta, new.btranamountbank);
  end else
  begin
    update fsbalances set debit = debit + :debit, credit = credit + :credit, btranamountbank = btranamountbank + new.btranamountbank
      where ref = :fsbalances;
  end
  if (new.dictgrp is not null) then
    update fsbalances
      set grpdebit = grpdebit + :debit, grpcredit = grpcredit + :credit, grpbtranamountbank = grpbtranamountbank + new.btranamountbank
      where dictgrp = new.dictgrp and company = new.company and currency = new.waluta;
end^
SET TERM ; ^
