--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRANNEXES_BIU_BLANK FOR ECONTRANNEXES                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if (new.salary is null) then new.salary = 0;
  if (new.caddsalary is null) then new.caddsalary = 0;
  if (new.funcsalary is null) then new.funcsalary = 0;
  if (new.atype = 1) then new.todate = null;
  if (new.bksymbol = '') then new.bksymbol = null;
  if (new.dictdef = 0) then new.dictdef = null;
  if (new.dictpos = 0) then new.dictpos = null;
  if (new.workdim is null) then new.workdim = 1.0000 * new.dimnum / new.dimden;
  if (new.department = '') then new.department = null;
  if (inserting and new.regtimestamp is null) then new.regtimestamp = current_timestamp(0);     --PR61955
  if (inserting and new.regoperator is null) then
    execute procedure GET_GLOBAL_PARAM('AKTUOPERATOR') returning_values new.regoperator;
  if (new.employee is null) then
  begin
    select employee
      from emplcontracts
      where ref = new.emplcontract
    into new.employee;
  end
end^
SET TERM ; ^
