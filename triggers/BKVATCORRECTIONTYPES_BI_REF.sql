--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKVATCORRECTIONTYPES_BI_REF FOR BKVATCORRECTIONTYPES           
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('BKVATCORRECTIONTYPES')
      returning_values new.REF;
end^
SET TERM ; ^
