--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BTRANSFERPOS_AI0 FOR BTRANSFERPOS                   
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable slodef integer;
declare variable slopoz integer;
declare variable company companies_id;
begin
  if(new.amount <> 0) then
    update BTRANSFERS
      set AMOUNT = (select sum(AMOUNT)
                      from BTRANSFERPOS
                      where BTRANSFERPOS.btransfer = new.btransfer)
      where REF=new.btransfer;
  select slodef, slopoz, company
    from BTRANSFERs
    where ref = new.btransfer
  into :slodef, :slopoz, :company;
  execute procedure rozrach_btran_count(:slodef, slopoz, new.account, new.settlement,:company);
end^
SET TERM ; ^
