--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_BD_KOREKTA FOR BKDOCS                         
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  --  jeżeli dla dokumginentu istnieją korekty to nie pozwól usunąc

  --  ale czy czasami nie jestesmy koretą?
  if (old.bkdocsvatcorrectionref is null) then
  begin
    if (exists ( select first 1 1
                   from bkdocs b
                   where b.bkdocsvatcorrectionref = old.ref
                )) then
    begin
      exception bkdocs_exits_kor;
    end
  end
end^
SET TERM ; ^
