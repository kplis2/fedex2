--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYCEN_AI1 FOR STANYCEN                       
  ACTIVE AFTER INSERT POSITION 1 
as
declare variable cnt integer;
begin
   if(new.zablokow > 0 or (new.zarezerw > 0) or (new.zamowiono > 0) )then begin
     select count(*) from STANYIL where MAGAZYN=new.magazyn and KTM=new.ktm and WERSJA=new.wersja into :cnt;
     if(:cnt is null) then cnt = 0;
     if(:cnt > 0) then
       update STANYIL set
         ZAREZERW = ZAREZERW + new.ZAREZERW,
         ZABLOKOW = ZABLOKOW + new.ZABLOKOW,
         ZAMOWIONO = ZAMOWIONO + new.ZAMOWIONO
         where MAGAZYN=new.magazyn and KTM=new.ktm and WERSJA=new.wersja;
     else
       insert into STANYIL(MAGAZYN, KTM, WERSJA, ILOSC, CENA, WARTOSC, ZAREZERW, ZABLOKOW, ZAMOWIONO)
       values(new.magazyn, new.ktm, new.wersja, 0, 0, 0, new.zarezerw, new.zablokow, new.zamowiono);
  end
end^
SET TERM ; ^
