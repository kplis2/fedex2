--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRCONDIT_BIU_EXISTS FOR EMPLCONTRCONDIT                
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable dwpref integer;
declare variable dwnpref integer;
begin
  if (new.annexe is null) then
  begin
    if (exists (select first 1 1 from emplcontrcondit
                 where emplcontract = new.emplcontract
                   and contrcondit = new.contrcondit
                   and ref <> new.ref
                   and annexe is null
                   and dictpos is not distinct from new.dictpos
                   and workstart is not distinct from new.workstart
                   and workend is not distinct from new.workend
                  )
    ) then
      exception universal 'Dany warunek można dodać tylko raz dla danej umowy';
  end else begin
    if (exists (select first 1 1 from emplcontrcondit
                where emplcontract = new.emplcontract
                  and contrcondit = new.contrcondit
                  and ref <> new.ref
                  and annexe is not null
                  and annexe is not distinct from new.annexe
                  and dictpos is not distinct from new.dictpos
                  and workstart is not distinct from new.workstart
                  and workend is not distinct from new.workend)
    ) then
      exception universal'Dany warunek można dodać tylko raz dla danego aneksu';
  end

  select ref from edictcontrcondit where symbol = 'DNI_WOLNE_PLATNE' into :dwpref;
  select ref from edictcontrcondit where symbol = 'DNI_WOLNE_NIEPLATNE' into :dwnpref;
  if(new.contrcondit = dwpref or new.contrcondit = dwnpref) then
  begin
    new.val_numeric = floor(new.val_numeric);
  end
end^
SET TERM ; ^
