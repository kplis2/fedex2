--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWAKCES_BU_REPLICAT FOR TOWAKCES                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.ref<>old.ref
      or new.ktm<>old.ktm or (new.ktm is null and old.ktm is not null) or (new.ktm is not null and old.ktm is null)
      or new.aktm<>old.aktm or (new.aktm is null and old.aktm is not null) or (new.aktm is not null and old.aktm is null)
      or new.awersja<>old.awersja or (new.awersja is null and old.awersja is not null) or (new.awersja is not null and old.awersja is null)
      or new.awersjaref<>old.awersjaref or (new.awersjaref is null and old.awersjaref is not null) or (new.awersjaref is not null and old.awersjaref is null)
      or new.typ<>old.typ or (new.typ is null and old.typ is not null) or (new.typ is not null and old.typ is null)
      or new.opis<>old.opis or (new.opis is null and old.opis is not null) or (new.opis is not null and old.opis is null)
      or new.required<>old.required or (new.required is null and old.required is not null) or (new.required is not null and old.required is null)
      or new.akcesdegree<>old.akcesdegree or (new.akcesdegree is null and old.akcesdegree is not null) or (new.akcesdegree is not null and old.akcesdegree is null)
      or new.doubleakc<>old.doubleakc or (new.doubleakc is null and old.doubleakc is not null) or (new.doubleakc is not null and old.doubleakc is null)
      or new.towakcesgrp<>old.towakcesgrp or (new.towakcesgrp is null and old.towakcesgrp is not null) or (new.towakcesgrp is not null and old.towakcesgrp is null)
      ) then
   waschange = 1;

  execute procedure rp_trigger_bu('TOWAKCES',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
