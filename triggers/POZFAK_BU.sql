--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAK_BU FOR POZFAK                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable typ varchar(3);
declare variable korekta smallint;
declare variable zakup smallint;
declare variable opak smallint;
declare variable dniwaznbufc varchar(255);
declare variable dniwaznbufi integer;
declare variable dniwazn integer;
declare variable skad smallint;
declare variable akceptacja smallint;
declare variable aktywna_wersja smallint;
declare variable aktywny smallint;
declare variable towakt varchar(255);
begin
  if((new.wersjaref <> old.wersjaref or (new.ktm is null) or (new.wersja is null)) and new.wersjaref is not null and new.wersjaref <> 0) then begin
     select nrwersji, ktm, usluga, akt from WERSJE where ref=new.wersjaref into new.wersja, new.ktm, new.usluga, :aktywna_wersja;
  end else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)or (new.wersjaref is null)) then
    select ref, usluga, akt from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref, new.usluga, :aktywna_wersja;

  if(coalesce(new.datawazn,'1899-12-31')<>coalesce(old.datawazn,'1899-12-31')) then begin
    execute procedure GETCONFIG('DNIWAZNBUF') returning_values :dniwaznbufc;
    if(:dniwaznbufc<>'' and :dniwaznbufc<>'-1') then begin
      select dniwazn from towary where ktm = new.ktm into :dniwazn;
      select typ, skad, akceptacja from nagfak where ref = new.dokument into :typ, :skad, :akceptacja;
      select korekta, zakup, opak from typfak where symbol = :typ into :korekta, :zakup,  :opak;
      if(:skad = 0 and :akceptacja = 1 and :dniwazn>0 and :korekta = 0 and :zakup = 1 and :opak = 0) then begin
        if(new.datawazn is null) then exception DOKUMPOZ_BAD_EXPIRYDATE 'Nie podano daty ważności dla: '|| new.ktm;
        if(new.datawazn < current_date) then
          exception DOKUMPOZ_BAD_EXPIRYDATE 'Błędna data ważności dla: ' || new.ktm || '. Data wcześniejsza od dzisiejszej.';
        if(new.datawazn > current_date + :dniwazn + :dniwaznbufi) then
          exception DOKUMPOZ_BAD_EXPIRYDATE   'Błędna data ważności dla: '|| new.ktm || '. Towar ma ' || :dniwazn || ' dni przydatności. ';
      end
    end
  end
  if(new.krajpochodzenia = '') then new.krajpochodzenia = null;
  if(new.krajpochodzenia <> '' and new.krajpochodzenia <> old.krajpochodzenia) then
    update towary set krajpochodzenia = new.krajpochodzenia where ktm = new.ktm and coalesce(krajpochodzenia,'')='';

  select akceptacja from nagfak where ref = new.dokument into :akceptacja;
  if(:akceptacja=1) then begin
    if((new.ktm<>old.ktm) or
      (new.wersja<>old.wersja) or
      (new.wersjaref<>old.wersjaref) or
      (new.ilosc<>old.ilosc) or
      (new.cenanet<>old.cenanet) or
      (new.cenabru<>old.cenabru) or
      (new.cenanetzl<>old.cenanetzl) or
      (new.cenabruzl<>old.cenabruzl)) then  exception NAGFAK_AKCEPT;
  end

  if(new.ktm<>old.ktm) then begin
    select nazwa, akt from towary where ktm = new.ktm into new.nazwat, :aktywny;
    select pkwiu from wersje where ref = new.wersjaref into new.pkwiu;
    select typ from nagfak where ref = new.dokument into :typ;
    select korekta from typfak where symbol = :typ into :korekta;
    execute procedure GETCONFIG('TOWARAKTYWNY') returning_values :towakt;
    if(:towakt='1' and (:aktywny<>1 or :aktywna_wersja = 0) and :korekta=0) then
      if (:aktywny = 0) then
        exception TOWAR_NIEAKTYWNY 'Wybrany towar '||new.ktm||' jest nieaktywny.';
      else
        exception TOWAR_NIEAKTYWNY 'Wybrano nieaktywną wersję towaru '||new.ktm;
    if(coalesce(new.pkwiu,'') = '') then select pkwiu from towary where ktm = new.ktm into new.pkwiu;
  end
  else if (new.wersja <> old.wersja) then begin
    select akt from towary where ktm = new.ktm into :aktywny;
    select typ from nagfak where ref = new.dokument into :typ;
    select korekta from typfak where symbol = :typ into :korekta;
    execute procedure GETCONFIG('TOWARAKTYWNY') returning_values :towakt;
    if(:towakt='1' and (:aktywny<>1 or :aktywna_wersja = 0) and :korekta=0) then
      if (:aktywny = 0) then
        exception TOWAR_NIEAKTYWNY 'Wybrany towar '||new.ktm||' jest nieaktywny.';
      else
        exception TOWAR_NIEAKTYWNY 'Wybrano nieaktywną wersję towaru '||new.ktm;
  end
  if (new.alttopoz is not null) then
  begin
    select skad
      from nagfak
      where ref = new.dokument
      into :skad;
    if( skad > 0 ) then
      if (new.frompozzam is null and
          new.fromdokumpoz is null
          and not exists (select first 1 1
                          from pozfak
                          where ref = new.alttopoz
                            and ( frompozzam is not null
                            or fromdokumpoz is not null))) then
        exception poz_fake;
    new.fake = 1;
  end
  else
    new.fake = 0;
end^
SET TERM ; ^
