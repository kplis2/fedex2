--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZADANIA_AI_NOTATKA FOR ZADANIA                        
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable notref integer;
declare variable zadnazwa varchar(255);
begin
  if(new.tworznotatke=1 and new.notatka is null) then begin
    notref = gen_id(GEN_CNOTATKI,1);
    select OPIS from DEFZADAN where REF=new.zadanie into :zadnazwa;
    insert into CNOTATKI(REF,NAZWA,DATA,TRESC,CPODMIOT,PKOSOBA,ZADANIE,OPERATOR)
    values (:notref,:zadnazwa,new.datawyk,new.opiswyk,new.cpodmiot,new.osoba,new.ref,new.operwyk);
    update ZADANIA set notatka = :notref where REF=new.ref;
  end
end^
SET TERM ; ^
