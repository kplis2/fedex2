--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_BU_COUNTBASE FOR EABSENCES                      
  ACTIVE BEFORE UPDATE POSITION 3 
AS
declare variable status smallint;
begin
--Rozliczenie nieobecnosci, ustalenie podstawy miesiecznej, stawki dziennej i godzinowej

  if ((new.monthpaybase is null and new.hourpayamount is null) or new.ecolumn <> old.ecolumn
     or new.employee <> old.employee or new.days <> old.days or new.worksecs <> old.worksecs or new.npdays <> old.npdays
     or coalesce(new.lbase,'') <> coalesce(old.lbase,'') or coalesce(new.fbase,'') <> coalesce(old.fbase,'')
     or new.baseabsence <> old.baseabsence or coalesce(new.dokdate,'1899-12-30') <> coalesce(old.dokdate,'1899-12-30')
     or coalesce(new.certserial,'') <> coalesce(old.certserial,'') or coalesce(new.certnumber,'') <> coalesce(old.certnumber,'')
  ) then begin

    if (new.epayroll is not null) then
      select status from epayrolls where ref = new.epayroll into :status;

    if (coalesce(status,0) = 0) then -- BS45297
      execute procedure e_calculate_countbase (new.ref, 0, new.mflag, new.employee,
          new.ecolumn, new.fromdate, new.todate, new.dokdate, new.lbase, new.fbase,
          new.baseabsence, new.calcpaybase, new.correction, new.worksecs, new.certserial,
          new.certnumber, new.dimnum, new.dimden, new.npdays, new.company)
        returning_values new.monthpaybase, new.daypayamount, new.hourpayamount, new.payamount;

  end
end^
SET TERM ; ^
