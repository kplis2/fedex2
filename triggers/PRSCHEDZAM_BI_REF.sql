--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDZAM_BI_REF FOR PRSCHEDZAM                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.ref is null or (new.ref = 0)) then
    execute procedure gen_ref('PRSCHEDZAM') returning_values new.ref;

end^
SET TERM ; ^
