--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TABKURS_BU0 FOR TABKURS                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.bank <> old.bank or (new.data <> old.data)) then begin
    if(new.bank is not null) then
      new.symbol = substring(new.bank from 1 for 9) || '/' ||cast(new.data as date);
  end
end^
SET TERM ; ^
