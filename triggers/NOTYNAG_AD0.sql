--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTYNAG_AD0 FOR NOTYNAG                        
  ACTIVE AFTER DELETE POSITION 0 
as
  declare variable numberg varchar(40);
  declare variable blockref integer;
begin
  if (old.status = 1) then
  begin
    if (old.notakind = 0) then
      execute procedure GET_CONFIG('WEZWANIANUM', 2) returning_values numberg;
    else if (old.notakind = 1) then
      execute procedure GET_CONFIG('NOTYODSNUM', 2) returning_values numberg;
    else if (old.notakind = 2) then
      execute procedure GET_CONFIG('POTWIERDZENIANUM', 2) returning_values numberg;

    execute procedure free_number(numberg, 'NOTA', 'NOTA', old.data, old.number, 0)
      returning_values blockref;
  end

  update rozrachp set notadokum = NULL where notadokum = old.ref;
end^
SET TERM ; ^
