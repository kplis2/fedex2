--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKONTRAKTYE_BI_REF FOR CKONTRAKTYE                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CKONTRAKTYE')
      returning_values new.REF;
end^
SET TERM ; ^
