--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTAWY_AU FOR DOSTAWY                        
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable dokpozref integer;
begin
  if(new.aktualizuj=1) then begin
    if((new.datawazn<>old.datawazn or (new.datawazn is not null and old.datawazn is null) or (new.datawazn is null and old.datawazn is not null)) or
       (new.paramn1<>old.paramn1 or (new.paramn1 is not null and old.paramn1 is null) or (new.paramn1 is null and old.paramn1 is not null)) or
       (new.paramn2<>old.paramn2 or (new.paramn2 is not null and old.paramn2 is null) or (new.paramn2 is null and old.paramn2 is not null)) or
       (new.paramn3<>old.paramn3 or (new.paramn3 is not null and old.paramn3 is null) or (new.paramn3 is null and old.paramn3 is not null)) or
       (new.paramn4<>old.paramn4 or (new.paramn4 is not null and old.paramn4 is null) or (new.paramn4 is null and old.paramn4 is not null)) or
       (new.paramn5<>old.paramn5 or (new.paramn5 is not null and old.paramn5 is null) or (new.paramn5 is null and old.paramn5 is not null)) or
       (new.paramn6<>old.paramn6 or (new.paramn6 is not null and old.paramn6 is null) or (new.paramn6 is null and old.paramn6 is not null)) or
       (new.paramn7<>old.paramn7 or (new.paramn7 is not null and old.paramn7 is null) or (new.paramn7 is null and old.paramn7 is not null)) or
       (new.paramn8<>old.paramn8 or (new.paramn8 is not null and old.paramn8 is null) or (new.paramn8 is null and old.paramn8 is not null)) or
       (new.paramn9<>old.paramn9 or (new.paramn9 is not null and old.paramn9 is null) or (new.paramn9 is null and old.paramn9 is not null)) or
       (new.paramn10<>old.paramn10 or (new.paramn10 is not null and old.paramn10 is null) or (new.paramn10 is null and old.paramn10 is not null)) or
       (new.paramn11<>old.paramn11 or (new.paramn11 is not null and old.paramn11 is null) or (new.paramn11 is null and old.paramn11 is not null)) or
       (new.paramn12<>old.paramn12 or (new.paramn12 is not null and old.paramn12 is null) or (new.paramn12 is null and old.paramn12 is not null)) or
       (new.paramd1<>old.paramd1 or (new.paramd1 is not null and old.paramd1 is null) or (new.paramd1 is null and old.paramd1 is not null)) or
       (new.paramd2<>old.paramd2 or (new.paramd2 is not null and old.paramd2 is null) or (new.paramd2 is null and old.paramd2 is not null)) or
       (new.paramd3<>old.paramd3 or (new.paramd3 is not null and old.paramd3 is null) or (new.paramd3 is null and old.paramd3 is not null)) or
       (new.paramd4<>old.paramd4 or (new.paramd4 is not null and old.paramd4 is null) or (new.paramd4 is null and old.paramd4 is not null)) or
       (new.params1<>old.params1 or (new.params1 is not null and old.params1 is null) or (new.params1 is null and old.params1 is not null)) or
       (new.params2<>old.params2 or (new.params2 is not null and old.params2 is null) or (new.params2 is null and old.params2 is not null)) or
       (new.params3<>old.params3 or (new.params3 is not null and old.params3 is null) or (new.params3 is null and old.params3 is not null)) or
       (new.params4<>old.params4 or (new.params4 is not null and old.params4 is null) or (new.params4 is null and old.params4 is not null)) or
       (new.params5<>old.params5 or (new.params5 is not null and old.params5 is null) or (new.params5 is null and old.params5 is not null)) or
       (new.params6<>old.params6 or (new.params6 is not null and old.params6 is null) or (new.params6 is null and old.params6 is not null)) or
       (new.params7<>old.params7 or (new.params7 is not null and old.params7 is null) or (new.params7 is null and old.params7 is not null)) or
       (new.params8<>old.params8 or (new.params8 is not null and old.params8 is null) or (new.params8 is null and old.params8 is not null)) or
       (new.params9<>old.params9 or (new.params9 is not null and old.params9 is null) or (new.params9 is null and old.params9 is not null)) or
       (new.params10<>old.params10 or (new.params10 is not null and old.params10 is null) or (new.params10 is null and old.params10 is not null)) or
       (new.params11<>old.params11 or (new.params11 is not null and old.params11 is null) or (new.params11 is null and old.params11 is not null)) or
       (new.params12<>old.params12 or (new.params12 is not null and old.params12 is null) or (new.params12 is null and old.params12 is not null)))
    then begin
      for select DOKUMPOZ.REF
      from DOKUMPOZ
      left join DOKUMNAG on (DOKUMNAG.REF=DOKUMPOZ.DOKUMENT)
      left join DEFDOKUM on (DEFDOKUM.SYMBOL=DOKUMNAG.TYP)
      where DOKUMPOZ.DOSTAWA=new.ref and DEFDOKUM.WYDANIA=0
      into :dokpozref
      do begin
        update DOKUMPOZ set
          DATAWAZN = new.datawazn,
          PARAMN1 = new.paramn1,
          PARAMN2 = new.paramn2,
          PARAMN3 = new.paramn3,
          PARAMN4 = new.paramn4,
          PARAMN5 = new.paramn5,
          PARAMN6 = new.paramn6,
          PARAMN7 = new.paramn7,
          PARAMN8 = new.paramn8,
          PARAMN9 = new.paramn9,
          PARAMN10 = new.paramn10,
          PARAMN11 = new.paramn11,
          PARAMN12 = new.paramn12,
          PARAMD1 = new.paramd1,
          PARAMD2 = new.paramd2,
          PARAMD3 = new.paramd3,
          PARAMD4 = new.paramd4,
          PARAMS1 = new.params1,
          PARAMS2 = new.params2,
          PARAMS3 = new.params3,
          PARAMS4 = new.params4,
          PARAMS5 = new.params5,
          PARAMS6 = new.params6,
          PARAMS7 = new.params7,
          PARAMS8 = new.params8,
          PARAMS9 = new.params9,
          PARAMS10 = new.params10,
          PARAMS11 = new.params11,
          PARAMS12 = new.params12
        where DOKUMPOZ.REF=:dokpozref;
      end
    end
    update DOSTAWY set AKTUALIZUJ=0 where REF=new.ref;
  end
end^
SET TERM ; ^
