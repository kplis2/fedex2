--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ODDZIALY_AD1 FOR ODDZIALY                       
  ACTIVE AFTER DELETE POSITION 1 
as
declare variable oddzial varchar(100);
begin
  if(old.glowny = 1) then begin
    select min(ODDZIAL) from ODDZIALY where COMPANY = old.company into :oddzial;
    if(:oddzial is not null) then
      update ODDZIALY set GLOWNY  = 1 where COMPANY = old.company and ODDZIAL = :oddzial;
  end
end^
SET TERM ; ^
