--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKACCOUNTS_AU_WPK FOR BKACCOUNTS                     
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable is_pattern smallint_id;
declare variable slave_ref bkaccounts_id;
begin
  -- po update sprawdzamy, że jak nie jestesmy synchronizowani to mozemy byc wzorcowi
  if (coalesce(new.pattern_ref,0) = 0 ) then
  begin
    --zatem, nalezy zbadac znacznik na zakladzie
    select c.pattern
      from companies c
      where c.ref = new.company
      into :is_pattern;
    if (is_pattern is distinct from null) then
    begin
      --ok jestesmy wzorcem, wiec nalezy zupdatowac wszystkie nasze podlegle krotki
      --procedura symchronizujaca z parametrem internal = 1, żeby nie zapetlić sie i nie sciagac raz drugi wartosci
      for
        select b.ref
          from bkaccounts b
          where b.pattern_ref = new.ref
          into :slave_ref
        do begin
          execute procedure synchronize_bkaccounts(new.ref, slave_ref, 1);
        end
    end
  end
end^
SET TERM ; ^
