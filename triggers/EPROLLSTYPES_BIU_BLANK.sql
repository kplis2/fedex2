--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPROLLSTYPES_BIU_BLANK FOR EPROLLSTYPES                   
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.prtype = 'UCP') then begin
    new.isvisible = null;
    new.isaddit = null;
    new.dseparate = null;
    new.dtosbase = null;
    new.dtovbase = null;
    new.dtransfername = null;
  end else begin
    if (new.prtype = '') then new.prtype = null;
    new.isvisible = coalesce(new.isvisible,1);
    new.isaddit = coalesce(new.isaddit,0);
    new.dseparate = coalesce(new.dseparate,2);
    new.dtosbase = coalesce(new.dtosbase,3);
    new.dtovbase = coalesce(new.dtovbase,3);
    new.dtransfername = coalesce(new.dtransfername,'');
  end
  if (new.dtransferbacc = '') then new.dtransferbacc = null;
end^
SET TERM ; ^
