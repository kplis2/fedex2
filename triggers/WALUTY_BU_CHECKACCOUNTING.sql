--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WALUTY_BU_CHECKACCOUNTING FOR WALUTY                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable dictdef integer;
begin
  if (new.symbol <> old.symbol) then
  begin
    select ref from slodef where typ = 'WALUTY'
      into :dictdef;
    execute procedure check_dict_using_in_accounts(dictdef, old.symbol, null);
  end
end^
SET TERM ; ^
