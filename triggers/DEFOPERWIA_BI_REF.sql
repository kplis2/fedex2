--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERWIA_BI_REF FOR DEFOPERWIA                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DEFOPERWIA')
      returning_values new.REF;
end^
SET TERM ; ^
