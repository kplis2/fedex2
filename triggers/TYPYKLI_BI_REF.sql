--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TYPYKLI_BI_REF FOR TYPYKLI                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('TYPYKLI')
      returning_values new.REF;
end^
SET TERM ; ^
