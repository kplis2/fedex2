--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SECURITY_LOG_BI0_REF FOR SECURITY_LOG                   
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('SECURITY_LOG') returning_values new.ref;
end^
SET TERM ; ^
