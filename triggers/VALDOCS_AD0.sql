--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VALDOCS_AD0 FOR VALDOCS                        
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable blockref integer;
declare variable numberg varchar(40);
begin
  if (old.liquidation = 1) then
  begin
    if (exists(select first 1 1 from fxdassets
                 where ref = old.fxdasset and activ <> 1)
    ) then
      update fxdassets
        set activ = 1, liquidationdate = null, liquidationperiod = null
        where ref = old.fxdasset;
  end

  if (old.tvalue is not null) then
    update fxdassets set tvaluegross = coalesce(tvaluegross,0) - old.tvalue where ref = old.fxdasset;
  if (old.fvalue is not null) then
    update fxdassets set fvaluegross = coalesce(fvaluegross,0) - old.fvalue where ref = old.fxdasset;
  if (old.tredemption is not null) then
    update fxdassets set tredemptiongross = coalesce(tredemptiongross,0) - old.tredemption where ref = old.fxdasset;
  if (old.fredemption is not null) then
    update fxdassets set fredemptiongross = coalesce(fredemptiongross,0) - old.fredemption where ref = old.fxdasset;

  if (exists(select first 1 1 from vdoctype
               where amcorrection = 1 and symbol = old.doctype)
  ) then
    delete from amortization where correction = old.ref;

  execute procedure getconfig('NUMBERGEN_VALDOCS') returning_values :numberg;
  if (numberg <> '') then begin
    execute procedure free_number(:numberg,null, old.doctype, old.docdate,old.numer, 0) returning_values :blockref;
  end
end^
SET TERM ; ^
