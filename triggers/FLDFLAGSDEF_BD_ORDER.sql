--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FLDFLAGSDEF_BD_ORDER FOR FLDFLAGSDEF                    
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if (old.ord = 0) then exit;
  update fldflagsdef set ord = 0, number = number - 1
    where (symbol <> old.symbol or fldflagsgrp <> old.fldflagsgrp) and number > old.number and fldflagsgrp = old.fldflagsgrp;
end^
SET TERM ; ^
