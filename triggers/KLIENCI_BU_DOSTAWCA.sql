--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_BU_DOSTAWCA FOR KLIENCI                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable skrot shortname_id;  --XXX ZG133796 MKD
begin
  if ((new.dostawca is not null and old.dostawca is null) or (new.dostawca <> old.dostawca)) then begin
    select first 1 fskrot from klienci k where k.dostawca = new.dostawca and ref <> new.ref
    into :skrot;
    if (:skrot is not null) then
      exception KLIENCI_EXPT 'Klient '||:skrot||' jest już powiązany z tym dostawcą';
  end
end^
SET TERM ; ^
