--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER UZYKLI_BI_REPLICAT FOR UZYKLI                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
 execute procedure rp_trigger_bi('UZYKLI',new.ref, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
