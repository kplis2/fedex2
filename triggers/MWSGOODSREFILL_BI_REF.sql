--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSGOODSREFILL_BI_REF FOR MWSGOODSREFILL                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSGOODSREFILL') returning_values new.ref;
end^
SET TERM ; ^
