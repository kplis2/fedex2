--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SDOSTKOSZT_BI_REF FOR SDOSTKOSZT                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SDOSTKOSZT')
      returning_values new.REF;
end^
SET TERM ; ^
