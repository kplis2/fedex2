--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACLIMITSUCP_BIU_BLANK FOR EVACLIMITSUCP                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
--exception test_break''||new.emplcontract;
  if(coalesce(new.company,0) = 0) then
    select company
      from employees
      where ref = new.employee
      into new.company;

  new.paylimit = coalesce(new.paylimit,0);
  new.paylimitcons = coalesce(new.paylimitcons,0);
  new.payrestlimit = coalesce(new.payrestlimit,0);
  new.nopaylimit = coalesce(new.nopaylimit,0);
  new.nopaylimitcons = coalesce(new.nopaylimitcons,0);
  new.nopayrestlimit = coalesce(new.nopayrestlimit,0);

end^
SET TERM ; ^
