--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DCTEMPLETPOSDIST_BI_REF FOR DCTEMPLETPOSDIST               
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DCTEMPLETPOSDIST')
      returning_values new.REF;
end^
SET TERM ; ^
