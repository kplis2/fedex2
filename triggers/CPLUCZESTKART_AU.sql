--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLUCZESTKART_AU FOR CPLUCZESTKART                  
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.symbol <> old.symbol) then
    execute procedure cpl_uczest_nrkarty(new.cpluczestid);
end^
SET TERM ; ^
