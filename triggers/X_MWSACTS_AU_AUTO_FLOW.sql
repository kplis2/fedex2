--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWSACTS_AU_AUTO_FLOW FOR MWSACTS                        
  INACTIVE AFTER UPDATE POSITION 11 
as
  declare variable STATUS SMALLINT_ID;
  declare variable MSG STRING1024;
--  declare variable ref_mwsconstloc integer_id;
  declare variable ref_mwsord mwsords_id;
--  declare variable ref_mwsact mwsords_id;
  declare variable poz smallint_id;
  declare variable grupa smallint_id;

begin
 /* if ((old.status = 1 or old.status = 2) and new.status = 5 and (new.mwsordtype <> 18 or new.mwsordtype <> 15) ) then begin -- zmiana statusu na 5 i nie inwentaryzacja biezaca i nie rozliczenie inwentaryzacji
    if ((exists(select first 1 1 from mwsconstlocs mc where mc.ref = new.mwsconstlocp and mc.x_row_type = 1 and mc.x_flow_order = 1)) and
       (not exists(select first 1 1 from mwsstock mo where mo.mwsconstloc = new.mwsconstlocp and mo.quantity > 0 and mo.wh = new.wh))) then begin
      --regal przeplywowy i poz 1
      execute procedure x_mws_auto_flow(new.mwsconstlocp, new.operator)
        returning_values status, msg;

    end else if (exists(select first 1 1 from mwsconstlocs mc where mc.ref = new.mwsconstlocl and mc.x_row_type = 1 and mc.x_flow_order = 5)) then begin
      --regal przeplywowy i poz 5
      execute procedure x_mws_auto_flowin(new.mwsconstlocl, new.operator, new.mwsord, new.ref)
        returning_values status, msg;
    end
  end

  if (old.status = 5 and new.status <> 5 and (new.mwsordtype <> 18 or new.mwsordtype <> 15)) then
    if (exists(select first 1 1 from mwsconstlocs mc where mc.ref = new.mwsconstlocp and mc.x_row_type = 1)
      and (not exists(select first 1 1 from mwsacts ma where ma.ref <> new.ref and ma.status <> 5 and ma.mwsord = new.mwsord))
      and (exists(select first 1 1 from mwsords mo join mwsords mos on mo.frommwsord = mos.ref where mo.ref = new.mwsord))) then begin
      select mo.ref from mwsords mo where mo.frommwsord = new.mwsord into :ref_mwsord; --pobranie poprzedniego ref_mwsords
    --  exception universal 'test1';
    --update mwsord
      update mwsords mo set mo.autocommit = 0
        where mo.ref = :ref_mwsord
          and mo.status = 5;
      update mwsords mo set mo.status = 2
        where mo.ref = :ref_mwsord
          and mo.status = 5;
    --update i deltete mwsacts
      update mwsacts ma set ma.status = 2
        where ma.mwsord = :ref_mwsord
          and ma.status = 5;
      update mwsacts ma set ma.status = 1
        where ma.mwsord = :ref_mwsord
          and ma.status = 2;
      update mwsacts ma set ma.status = 0
        where ma.mwsord = :ref_mwsord
          and ma.status = 1;
      delete from mwsacts ma
        where ma.mwsord = :ref_mwsord
          and ma.status = 0;
    --update i delete mwsord
      update mwsords mo
        set mo.status = 0
        where mo.ref = :ref_mwsord
          and mo.status = 2;
      delete from mwsords mo
        where mo.ref = :ref_mwsord
          and mo.status = 0;
  end    */
end^
SET TERM ; ^
