--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSCONSTLOCS_AU0 FOR MWSCONSTLOCS                   
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.act <> old.act) then
    update mwsstock set actloc = new.act where mwsconstloc = new.ref and actloc <> new.act;
end^
SET TERM ; ^
