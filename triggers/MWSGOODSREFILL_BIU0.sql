--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSGOODSREFILL_BIU0 FOR MWSGOODSREFILL                 
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable blocked smallint;
declare variable mwssellav numeric(14,4);
declare variable realfaster smallint;
begin
  if (new.colindex is null) then new.colindex = 0;
  if (new.quantity is null) then new.quantity = 0;
  if (new.posquantity is null) then new.posquantity = 0;
  if (new.status is null) then new.status = 0;
  if (new.ordered is null) then new.ordered = 0;
  if (new.stocktaking is null) then new.stocktaking = 0;
  if (new.good is null or new.calcmix is null) then
    select ktm, mwscalcmix from wersje where ref = new.vers into new.good, new.calcmix;
  if ((new.dictcode is null or new.dictcode = '' or new.shippingtype is null
       or new.priority is null or new.docdate is null or new.docaccdate is null)
      and new.docid is not null
  ) then
  begin
    select k.fskrot, s.ref, substring(s.nazwa from 1 for 80), blokada,
        priorytet, data, dataakc, termdost, mwspreparegoods, coalesce(d.realfaster,0)
      from dokumnag d
        left join klienci k on (d.klient = k.ref)
        left join sposdost s on (s.ref = d.sposdost)
      where d.ref = new.docid
      into new.dictcode, new.shippingtype, new.shippingtypedict, blocked,
        new.priority, new.docdate, new.docaccdate, new.realdate, new.preparegoods, realfaster;
    if (new.realdate is null) then new.realdate = current_date;
    if (cast(new.realdate as date) > current_date and realfaster = 0) then
      new.reallater = 1;
    else
      new.reallater = 0;
    if (new.preparegoods is null) then new.preparegoods = 0;
    if (new.preparegoods <> 1 or new.preparegoods is null) then
    begin
      if (new.reallater = 1 or blocked = 1 or blocked = 5) then
        new.preparegoods = 0;
      else
        new.preparegoods = 1;
    end
  end
  if (new.ordered < new.posquantity) then
  begin
    select mwssellav from mws_get_goodquantity(null,new.wh,new.vers)
      into mwssellav;
    if (mwssellav is null) then
      mwssellav = 0;
    if (mwssellav = 0) then new.colindex = 1;
  end
  if (new.realdate <> old.realdate or (new.realdate is not null and old.realdate is null)) then
  begin
    if (cast(new.realdate as date) <= current_date) then
    begin
      select blokada from dokumnag where ref = new.docid into blocked;
      if (blocked = 0 or blocked = 4) then
      begin
        new.preparegoods = 1;
        new.reallater = 0;
      end
    end
  end
end^
SET TERM ; ^
