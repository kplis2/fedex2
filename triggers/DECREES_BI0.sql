--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_BI0 FOR DECREES                        
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable i_year int;
  declare variable country_currency varchar(3);
  declare variable currvalue numeric(14,2);
  declare variable multicurr smallint;
  declare variable status integer;
begin
  if (new.ssallowdelete is null) then
    new.ssallowdelete = 0;
  if ((new.OPENDATE is not null)and(new.PAYDAY is not null)) then
    if (new.OPENDATE>new.PAYDAY) then
      exception ROZRACH_DATA_OTW ;

  if (new.status is null) then new.status = 0;
  if (new.settlcreate is null) then new.settlcreate = 0;

  select period, bkreg, autodoc, transdate, autobo, company, status
    from bkdocs where ref = new.bkdoc
    into new.period, new.bkreg, new.autodoc, new.transdate, new.autobo, new.company, :status;

  if (status > 0) then
    exception DECREES_STATUS;
  if (new.settlement='') then
    new.settlement = null;
  if (new.settlement is not null and new.stransdate is null) then
    new.stransdate = new.transdate;

  if(new.credit is null) then new.credit = 0;
  if(new.debit is null) then new.debit = 0;
  if(new.currcredit is null) then new.currcredit = 0;
  if(new.currdebit is null) then new.currdebit = 0;
  if(new.sscredit is null) then new.sscredit = 0;
  if(new.ssdebit is null) then new.ssdebit = 0;
  if(new.sscurrcredit is null) then new.sscurrcredit = 0;
  if(new.sscurrdebit is null) then new.sscurrdebit = 0;
  if(new.prdcreate is null) then new.prdcreate = 0;

  execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
    returning_values country_currency;
  if (new.currency is null or new.currency='') then
    new.currency = country_currency;
  if (new.sscurrency is null or new.sscurrency='') then
    new.sscurrency = country_currency;
  select yearid from bkperiods where company = new.company and id = new.period into :i_year;
  select ref from bkaccounts
    where symbol = substring(new.account from 1 for 3) and yearid = :i_year
      and company = new.company
    into new.accref;
  if (new.accref is null) then
    select min(ref) from bkaccounts where yearid = :i_year
      into new.accref;
  else
  begin
    select multicurr
      from bkaccounts where ref=new.accref
      into :multicurr;
    if (multicurr=0) then
    begin
      new.currency = :country_currency;
      new.sscurrency = :country_currency;
      new.currcredit = 0;
      new.currdebit = 0;
      new.sscurrcredit = 0;
      new.sscurrdebit = 0;
      new.rate = 0;
      new.ssrate = 0;
    end
  end

  if (country_currency <> new.currency) then
  begin
    if (new.currdebit <> 0 ) then
      currvalue = new.currdebit;
    if (new.currcredit <> 0 ) then
      currvalue = new.currcredit;
    if (currvalue<>0) then
      new.descript = substring(cast(currvalue as varchar(255)) || ' ' || new.currency ||  ' x ' || cast(new.rate as varchar(255)) || ' - ' || new.descript from 1 for 80);
  end

end^
SET TERM ; ^
