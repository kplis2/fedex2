--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_AD0 FOR RKDOKPOZ                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable slodef integer;
declare variable slopoz integer;
declare variable company integer;
begin
  execute procedure rkdok_obl_war(old.dokument);

  if (old.konto is not null and coalesce(old.kwota,0) <> 0 and coalesce(old.rozrachunek,'') <> '') then
  begin
    select n.slodef, n.slopoz, r.company
      from rkdoknag n
        join rkrapkas r on (n.raport = r.ref)
      where n.ref = old.dokument
      into :slodef, :slopoz, :company;
    execute procedure rozrach_rkdokpoz_count(:slodef, :slopoz, old.konto, old.rozrachunek, :company);
  end
  delete from rozrachp where stable = 'RKDOKPOZ' and sref = old.ref;
end^
SET TERM ; ^
