--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PERSONS_BIU_EHRM FOR PERSONS                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 2 
as
begin
  if (inserting and new.ehrm is null) then new.ehrm = 0;
  if (new.pass_mustchange is null) then new.pass_mustchange = 0; --PR54326
  if (new.pass_daystochange is null) then new.pass_daystochange = 0; --PR54326
  if (inserting or new.passwd <> old.passwd and coalesce(new.passwd,'') <> '') then --PR54326
    new.pass_lastchange = current_timestamp(0);  --PR54326
  if ((new.passwd is not null and new.passwd <> '') and (inserting or old.passwd is null or new.passwd <> old.passwd)
    and coalesce(char_length(new.passwd),0)<>32)  -- [DG] XXX ZG119346
  then begin
    new.passwd = md5sum(new.passwd);
  end
  if ((new.authcookie is not null and new.authcookie <> '' ) and coalesce(char_length(new.authcookie),0) <> 32) then  -- [DG] XXX ZG119346
    new.authcookie = md5sum(new.authcookie);
end^
SET TERM ; ^
