--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDS_AU_EXPORT FOR MWSORDS                        
  ACTIVE AFTER UPDATE POSITION 120 
as
declare variable refdokumnag integer_id;
declare variable uwagi smallint_id;
declare variable reason string100;
declare variable korekta string1024;
declare variable symbol string20;
declare variable symbol_wa string20;
declare variable email string100;
declare variable dostawca string255;
declare variable temat string511;

begin
--Ldz - obsluga wysylania maili po realizacji PZ
reason = '';
-- szukamy zlecenia PT ze statusem zmienionym na 5, powiazanego z dokumentem PZ
  if(new.status = 5 and new.status <> old.status) then begin
    select d.ref, d.x_do_wyslania, d.symbol, d.int_symbol, dos.nazwa
      from dokumnag d
        join mwsords mo on (d.ref = mo.docid)
        join dostawcy dos on (d.dostawca = dos.ref)      --DTS 138206 dodanie dostawcy do tematu Email
      where d.typ in ('PZ')
        and mo.ref = new.ref
        and (d.wydrukowano = 0 or d.x_do_wyslania = 1)
    into :refdokumnag, :uwagi, :symbol, :symbol_wa, :dostawca;

    if (refdokumnag is null) then
      exit;
-- w x_do_wyslania zapisujemy info o zmianie uwag przez wozkowego na naglowku PZ lub pozycjach
    if (uwagi is null) then uwagi = 0;
-- symbol potrzebujemy do nazwy pdf, a reason do tresci maila
    reason = coalesce(symbol, '')||' '||coalesce(symbol_wa, '');

    if (uwagi = 1) then
      reason = reason||', '||'zmieniono uwagi';

-- szukamy symbolu korekty
    select list(d.symbol) --- xxxZG138651 KPI
      from dokumnag d
      where d.refk = :refdokumnag
    into :korekta;

    if (korekta is not null) then
      reason = reason||', '||'wystawiono korekte '||korekta;

    --szukamy adres na ktory wyslac maila   MKD
    
    select wartosc from getconfig('INT_MAIL')
      into :email;
    --domyslny email jezeli nie znajdzie w konfigu MKD
    if (coalesce(email, '') = '') then
     email = 'tomasz.posmyk@hermon.com.pl';


    temat = reason||', '||dostawca;

    if (not exists(select first 1 1 from x_export_dokum x where x.dokumnag = :refdokumnag and x.typ = 'PZ' and x.status = 1)) then
      insert into x_export_dokum (dokumnag, typ, status,  reason, symbol, email, temat)
        values(:refdokumnag, 'PZ', 1, :reason, :symbol, :email, :temat);
-- do testow
   /* insert into x_export_dokum (dokumnag, typ, status,  reason, symbol, email)
        values(:refdokumnag, 'PZ', 1, :reason, :symbol, 'zw2@sente.pl'); */

-- oznaczamy dokument jako wyslany i zerujemy do wyslania
    if (uwagi = 1) then
      update dokumnag d
        set d.x_do_wyslania = 0, d.wydrukowano = 1
        where d.ref = :refdokumnag;
  end
end^
SET TERM ; ^
