--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TURNOVERS_BI_BLANK FOR TURNOVERS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.debit is null) then new.debit = 0;
  if (new.credit is null) then new.credit = 0;
  select bktype, account, yearid from accounting where ref = new.accounting
    into new.bktype, new.account, new.yearid;
  if (new.ebdebit is null) then new.ebdebit = 0;
  if (new.ebcredit is null) then new.ebcredit = 0;
end^
SET TERM ; ^
