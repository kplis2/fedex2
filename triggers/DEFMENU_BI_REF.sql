--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFMENU_BI_REF FOR DEFMENU                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DEFMENU')
      returning_values new.REF;
end^
SET TERM ; ^
