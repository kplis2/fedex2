--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_AU_PROCAFTER FOR MWSACTS                        
  ACTIVE AFTER UPDATE POSITION 2 
AS
declare variable proc varchar(1024);
declare variable refmwsord integer;
begin
  if (new.status = 5 and old.status <> 5) then
  begin
    select mt.procaftactcommit
      from mwsords m
        join mwsordtypes mt on (m.mwsordtype = mt.ref)
      where m.ref = new.mwsord
      into :proc;
    if (proc <> '' and proc is not null) then
    begin
      proc = 'select refmwsord from '||proc||'('||new.docid||','||
          new.docid||','||new.docposid||','''||new.doctype||''','||
          new.mwsord||','||new.ref||',1,null)';
      execute statement proc
        into refmwsord;
    end
  end
  if (new.status <> 5 and old.status = 5) then
  begin
    proc = null;
    select mt.procafteractdecommit
      from mwsords m
        join mwsordtypes mt on (m.mwsordtype = mt.ref)
      where m.ref = new.mwsord
      into :proc;
    if (proc <> '' and proc is not null) then
    begin
      proc = 'select refmwsord from '||proc||'('||new.docid||','||
          new.docid||','||new.docposid||','''||new.doctype||''','||
          new.mwsord||','||new.ref||',1,null)';
      execute statement proc
        into refmwsord;
    end
  end
end^
SET TERM ; ^
