--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRZEKOD_BI_REF FOR PRZEKOD                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('PRZEKOD')
      returning_values new.REF;
end^
SET TERM ; ^
