--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EXPIMP_BI_REF FOR EXPIMP                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EXPIMP')
      returning_values new.REF;
end^
SET TERM ; ^
