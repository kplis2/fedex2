--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDICTCONTRCONDIT_BUD_CHECKLINKS FOR EDICTCONTRCONDIT               
  ACTIVE BEFORE UPDATE OR DELETE POSITION 0 
AS
begin
  if (deleting or new.symbol is distinct from old.symbol or new.unit is distinct from old.unit
    or new.quantity is distinct from old.quantity or new.etype is distinct from old.etype
    or new.ref <> old.ref or new.vat is distinct from old.vat) then

    if (exists (select first 1 1 from emplcontrcondit e where e.contrcondit = case when deleting then old.ref else new.ref end)) then
      exception universal 'Istnieją warunki tego typu dowiązane do umowy pracownika';
end^
SET TERM ; ^
