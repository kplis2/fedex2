--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CENNIK_BI0 FOR CENNIK                         
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable tktm varchar(40);
begin
  if(new.cenanet is null) then new.cenanet = 0;
  if(new.cenabru is null) then new.cenabru = 0;
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  else if(new.ktm is not null and new.wersja is not null) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  select AKT,WALUTA from DEFCENNIK where REF=new.CENNIK into new.AKT,new.WALUTA;
  select NAZWA from TOWARY where ktm = new.ktm into new.nazwat;
  if(new.MARZA is null) then new.marza = 0;
  if (new.stalacena is null) then new.stalacena = 0;
  if(new.narzut is null and new.marza <> 100) then
    new.narzut = new.marza / ( 100 - new.marza);
  if(new.jedn is null or (new.jedn = 0)) then
    select ref from TOWJEDN where KTM = new.ktm and GLOWNA = 1 into new.jedn;
  select KTM from TOWJEDN where REF=new.jedn into :tktm;
  if(:tktm <> new.ktm or (:tktm is null))then
    exception CENNIK_WRONGTOWJEDN;
  select REPLICAT from DEFCENNIK where ref=new.cennik into new.replicat;
  if(new.prec is null) then select PREC from DEFCENNIK where ref=new.cennik into new.prec;
end^
SET TERM ; ^
