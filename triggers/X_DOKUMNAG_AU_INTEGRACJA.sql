--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_DOKUMNAG_AU_INTEGRACJA FOR DOKUMNAG                       
  ACTIVE AFTER UPDATE POSITION 300 
as
declare variable idkoryg string120;
declare variable impnagid integer_id;
begin
    if ((new.typ in ('IN-','IN+','PZ-','PZ+')) and new.akcept = 1 and new.akcept <> old.akcept) then begin
        --exception universal 'new'||coalesce(new.akcept,-300) ||  'old'||coalesce(old.akcept,-300);
        select dn.int_id
            from dokumnag dn
            where dn.ref = new.refk
        into :idkoryg;

        insert into X_IMP_DOKUMNAG (  SYMBOL, NUMER, DATA, MAGAZYN, WYDANIA, ZEWNETRZNY,
                                    IDDOKORYGOWANEGO, SREF, TYPSENTE, STATUS, DATAPRZET)
        VALUES(   new.symbol,new.numer, new.data, new.magazyn, new.wydania, new.zewn,
                :idkoryg ,new.ref,  new.typ,10, current_timestamp)
            returning ref into :impnagid;

        insert into X_IMP_DOKUMPOZ (NUMER, IDARTYKULU, JEDNOSTKA, VAT, MAGAZYN, MAG2, ILOSC,
                                    UWAGI, IDPOZKORYGOWANEJ, SREF, SREFDOKUMNAG,
                                  NAGIMPREF, STATUS, DATAPRZET)
            select  DP.NUMER, t.int_id, DP.JEDNO, dp.gr_vat, new.magazyn, new.mag2, DP.ILOSC,
                    dp.uwagi, dpkor.int_id, dp.ref, dp.dokument,
                    :impnagid, 10, current_timestamp
            from DOKUMPOZ DP
                join towary t on (t.ktm = dp.ktm)
                left join dokumpoz dpkor on (dpkor.ref = dp.kortopoz)
            where dp.dokument = new.ref;
    end
end^
SET TERM ; ^
