--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVDEFPOSTRANS_AD_REPLICAT FOR SRVDEFPOSTRANS                 
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable contrtype integer;
begin
  select contrtype from srvdefpos where srvdefpos.ref = old.srvdefpos into :contrtype;
  update CONTRTYPES set STATE = -1 where ref = :contrtype;
end^
SET TERM ; ^
