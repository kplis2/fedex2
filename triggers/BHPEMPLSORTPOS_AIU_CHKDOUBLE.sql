--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BHPEMPLSORTPOS_AIU_CHKDOUBLE FOR BHPEMPLSORTPOS                 
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
declare variable fromdate date;
declare variable todate date;
declare variable employee integer;
begin
/*MWr: nie mozna pozwolic aby do danego sortu mozna bylo dopisac wiecej niz
      jeden artykul o takim samym numerze ktm i wersji. Generowanie dzialalo by
      wowczas blednie, z uwagi na fakt usuwania artykulow przed generow. planu.
      Z tego samego powodu nie mozna pozwolic, aby temu samemu pracownikowi,
      dany  art. nie byl przypisany drugi raz w ramach innego (waznego) sortu */

 if (exists(select first 1 1 from bhpemplsortpos where bhpemplsortdef = new.bhpemplsortdef
              and ktm = new.ktm and vers = new.vers and ref <> new.ref))
 then
   exception bhp_article_double;
 else begin
   select fromdate, todate, employee
     from bhpemplsortdef
     where ref = new.bhpemplsortdef
     into :fromdate, :todate, :employee;
    
   if (exists(select first 1 1 from bhpemplsortdef d
          join bhpemplsortpos p on (d.ref = p.bhpemplsortdef and d.ref <> new.bhpemplsortdef)
             where d.employee = :employee
               and (:fromdate <= d.todate or d.todate is null)
               and (:todate >= d.fromdate or :todate is null)
               and p.ktm = new.ktm and p.vers = new.vers))
   then
     exception bhp_emplsort_exists;
  end
end^
SET TERM ; ^
