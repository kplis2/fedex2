--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDICTCONTRCONDIT_BI_REF FOR EDICTCONTRCONDIT               
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EDICTCONTRCONDIT')
      returning_values new.REF;
end^
SET TERM ; ^
