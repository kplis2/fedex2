--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_AI0 FOR NAGFAK                         
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable local smallint;
begin
  execute procedure CHECK_LOCAL('NAGFAK',new.ref) returning_values :local;
  if(new.numblock > 0) then
    update NUMBERBLOCK set DOKNAGFAK = new.ref where ref=new.numblock;
  if(:local = 1) then begin
    execute procedure NAGFAK_SYMBOLZAM(new.ref);
  end
  if(new.skad = 0) then begin -- domyslne terminy platnosci z kontrahenta
    if (new.dostawca is not null) then
      INSERT INTO NAGFAKTERMPLAT (DOKTYP, DOKREF, TERMZAP, PROCENTPLAT, SPOSPLAT)
        select 'F', new.ref, new.data +k.dniplat, k.procentplat, k.sposplat from KONTRAHTERMPLAT k where k.slodef = 6 and k.slopoz = new.dostawca;
    else if (new.klient is not null) then
      INSERT INTO NAGFAKTERMPLAT (DOKTYP, DOKREF, TERMZAP, PROCENTPLAT, SPOSPLAT)
        select 'F', new.ref, new.data +k.dniplat, k.procentplat, k.sposplat from KONTRAHTERMPLAT k where k.slodef = 1 and k.slopoz = new.klient;
  end
end^
SET TERM ; ^
