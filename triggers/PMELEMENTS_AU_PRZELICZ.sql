--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMELEMENTS_AU_PRZELICZ FOR PMELEMENTS                     
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
    if(new.calcval <> old.calcval or new.budval <> old.budval or new.erealval <> old.erealval or
     new.realval <> old.realval or new.erealsval<>old.erealsval or new.erealsumval<>old.erealsumval) then
     execute procedure pmplans_calc(new.pmplan);
end^
SET TERM ; ^
