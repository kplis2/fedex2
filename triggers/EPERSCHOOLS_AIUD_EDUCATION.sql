--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPERSCHOOLS_AIUD_EDUCATION FOR EPERSCHOOLS                    
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 
as
  declare variable code varchar(20);
  declare variable vacbases integer; 
  declare variable perschools integer;
begin
  --DS: sprawdzanie czy prawidlowo oznaczone sa pola brane do urlopu oraz aktualizacja pola PERSONS.EDUCATION

  if (inserting or deleting or new.ref <> old.ref or new.person <> old.person
    or new.isvacbase is distinct from old.isvacbase
  ) then begin

    if (new.isvacbase = 1) then
      update eperschools p set p.isvacbase = 0
        where p.person = coalesce(new.person,old.person) and p.ref <> coalesce(new.ref,old.ref);
  
    select sum(s.isvacbase), count(s.ref)
      from eperschools s
      where s.person = coalesce(new.person,old.person)
        and s.ref <> coalesce(new.ref,old.ref)
      into :vacbases, :perschools;

    if (vacbases + coalesce(new.isvacbase,0) = 0 and perschools > 0) then
      exception school_vacseniority;
  
    select first 1 z.ref
      from eperschools s
        join edictzuscodes z on z.ref = s.education
      where s.person = coalesce(new.person,old.person)
      order by z.code desc, z.addinfo desc
      into :code;

    update persons set education = :code where ref = coalesce(new.person,old.person);
  end
end^
SET TERM ; ^
