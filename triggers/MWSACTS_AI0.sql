--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_AI0 FOR MWSACTS                        
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable mwsordtype integer;
declare variable grupasped integer;
begin
  -- zmiana statusu tylko dla nieinwentaryzacyjnych powoduje generowanie rozpiski
  -- do operacji zlecenia
  -- statusy powyzej 10 sa zablokowane do zadan specjalnych, na przyklad do obrobki rekordu
  if (new.status > 0) then
  begin
    select mwsordtype from mwsords where ref = new.mwsord into mwsordtype;
    execute procedure MWSACTS_STATUSCHANGE(:mwsordtype, new.mwsord, new.ref, 0);
  end
  if (new.cortomwsact is not null and new.status <> 6) then
    execute procedure MWS_MWSACT_CALC_QUANTITYL(new.cortomwsact);
  if (new.mwsordtypedest in (11,2) and new.status > 0 and new.status < 6
      and new.segtype = 2 and new.mwsconstlocl is not null
  ) then
  begin
    execute procedure mws_check_mwsconstlocsizes(new.mwsconstlocl, 1);
  end
  if (new.status not in (0,6) and new.docid is not null and new.doctype = 'M') then --[PM] XXX doctype !!!!!!
  begin
    select coalesce(g.grupasped, g.ref) from dokumnag g where g.ref = new.docid
      into grupasped;
    execute procedure dokumnag_mwsrealstatus(grupasped,0);
  end

  if (new.mwsordtypedest in (2,20) and new.frommwsact is not null and new.quantity > 0) then -- [DG] XXX przeniesione z BOWI in (20) #ZWROTY
  begin
    update mwsacts set quantity = quantity - new.quantity
      where ref = new.frommwsact
        and status = 0;
  end
end^
SET TERM ; ^
