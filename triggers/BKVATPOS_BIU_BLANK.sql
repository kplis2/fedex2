--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKVATPOS_BIU_BLANK FOR BKVATPOS                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.vatgr = '') then new.vatgr = null;
  if (new.netv is null) then new.netv = 0;
  if (new.vatv is null) then new.vatv = 0;
  if (new.vate is null) then new.vate = 0;
  if (new.grossv is null) then new.grossv = 0;
  if (new.currnetv is null) then new.currnetv = 0;
  if (new.net is null) then new.net = 0;
  if (new.debited is null) then new.debited = 1;
  if (new.taxgr = '' or new.taxgr is null ) then exception BKDOCS_ZAKUP_MA_GRUPE;
  if (new.debittype is null or new.debitprocent is null) then
  begin
    select g.defaultdebittype, g.debitedprocent
      from grvat g
      where g.symbol = new.taxgr
      into new.debittype, new.debitprocent;
  end
  select vatreg, company
    from bkdocs where ref = new.bkdoc
    into new.vatreg, new.company;
end^
SET TERM ; ^
