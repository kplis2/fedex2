--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPROLLBKDOC_BD0 FOR EPROLLBKDOC                    
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (exists(select ref from ebkdocpos where ebkdoc = old.ebkdoc)) then
    exception EBKDOCS_POSITION;
end^
SET TERM ; ^
