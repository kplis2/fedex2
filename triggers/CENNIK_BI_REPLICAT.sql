--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CENNIK_BI_REPLICAT FOR CENNIK                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
 execute procedure rp_trigger_bi('CENNIK',new.cennik, new.wersjaref, new.jedn, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
