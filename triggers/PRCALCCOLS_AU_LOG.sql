--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRCALCCOLS_AU_LOG FOR PRCALCCOLS                     
  ACTIVE AFTER UPDATE POSITION 2 
as
declare variable logchanges smallint;
begin
  if(new.cvalue<>old.cvalue) then begin
    select logchanges from prshcalcs where ref=new.calc into :logchanges;
    if(:logchanges=1) then
      insert into PRCALCCOLSLOG(PRCALCCOL,CVALUE) values (new.ref,old.cvalue);
  end
end^
SET TERM ; ^
