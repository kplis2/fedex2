--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRACTS_BIU_CONTR FOR EMPLCONTRACTS                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
declare variable previous_contracts integer;
declare variable fromdate date;
declare variable enddate date;
begin
  if (new.empltype <> 1) then exit;
  if (updating and old.fromdate = new.fromdate and old.employee = new.employee and old.enddate = new.enddate)
    then exit;

  select count(*)
    from emplcontracts ec
    where ec.employee = new.employee
      and ec.empltype = 1
    into :previous_contracts;

  if ((inserting and previous_contracts = 0) or (updating and previous_contracts <= 1)) then exit;

  select first 1 ec.enddate, ec.fromdate
    from emplcontracts ec
    where ec.employee = new.employee
        and ec.empltype = 1
        and ec.ref <> new.ref
        and ec.enddate < new.fromdate
    order by ec.fromdate desc
    into :enddate, :fromdate;

  if(coalesce(old.fromdate,new.fromdate +1) <> new.fromdate and enddate + 1 < new.fromdate) then
    exception CONTR_EMPLCONTR;

  select first 1 ec.enddate, ec.fromdate
    from emplcontracts ec
    where ec.employee = new.employee
        and ec.empltype = 1
        and ec.ref <> new.ref
        and ec.fromdate > new.fromdate
    order by ec.fromdate
    into :enddate, :fromdate;

  if((coalesce(old.enddate,new.fromdate +1) <> new.enddate and fromdate - 1 > new.enddate) or (new.fromdate < fromdate and new.enddate is null)) then
    exception CONTR_EMPLCONTR;
end^
SET TERM ; ^
