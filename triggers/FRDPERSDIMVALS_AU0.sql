--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDPERSDIMVALS_AU0 FOR FRDPERSDIMVALS                 
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable cell integer;
declare variable elemreadrights varchar(80);
declare variable elemwriterights varchar(80);
declare variable cellreadrights varchar(80);
declare variable cellwriterights varchar(80);
begin
  if ((new.readrights is null and new.readrights is not null)
     or (new.readrights is not null and old.readrights is null)
     or (new.readrights <> old.readrights) ) then
  begin
    for
      select frdcells.ref, dimelemdef.readrights, dimelemdef.writerights,
          frdcells.readrights, frdcells.writerights
        from frdcells
          join dimelemdef on (dimelemdef.ref = frdcells.dimelem)
        where frdcells.frdpersdimval = new.ref
        into :cell, :elemreadrights, :elemwriterights,
          :cellreadrights, :cellwriterights
    do begin
      execute procedure FRDCELLS_SETRIGHTS(:cell,new.readrights,new.writerights,:elemreadrights,
        :elemwriterights, :cellreadrights, :cellwriterights);
    end
  end
end^
SET TERM ; ^
