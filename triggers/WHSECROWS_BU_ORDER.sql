--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHSECROWS_BU_ORDER FOR WHSECROWS                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (new.ord = 5) then exit;
  if (NEW.ord is null) then new.ord = 1;
  if (new.number is null) then new.number = old.number;
  if (new.ord = 0 or new.number = old.number) then
  begin
    new.ord = 1;
    exit;
  end
  --numerowanie obszarow w rzedzie gdy nie podamy numeru
  if (new.number <> old.number) then
    select max(number) from whsecrows where whsec = new.whsec
      into :maxnum;
  else
    maxnum = 0;
  if (new.number < old.number) then
  begin
    update whsecrows set ord = 0, number = number + 1
      where ref <> new.ref and number >= new.number
        and number < old.number and whsec = new.whsec;
  end else if (new.number > old.number and new.number <= maxnum) then
  begin   
    update whsecrows set ord = 0, number = number - 1
      where ref <> new.ref and number <= new.number
        and number > old.number and whsec = new.whsec;
  end else if (new.number > old.number and new.number > maxnum) then
    new.number = old.number;
end^
SET TERM ; ^
