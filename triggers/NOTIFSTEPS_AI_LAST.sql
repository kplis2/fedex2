--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTIFSTEPS_AI_LAST FOR NOTIFSTEPS                     
  ACTIVE AFTER INSERT POSITION 30 
as
begin
  if (new.notifstep is not null) then
    if (exists (select first 1 1 from notifsteps n where n.notification = new.notification and n.number > new.number)) then
      exception universal 'Reguła zależna musi być wykonana wcześniej';
end^
SET TERM ; ^
