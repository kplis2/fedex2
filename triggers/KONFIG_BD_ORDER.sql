--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONFIG_BD_ORDER FOR KONFIG                         
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update konfig set ord = 0, numer = numer - 1
    where akronim <> old.akronim and numer > old.numer and grupa = old.grupa;
end^
SET TERM ; ^
