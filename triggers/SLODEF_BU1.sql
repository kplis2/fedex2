--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLODEF_BU1 FOR SLODEF                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.isdist is distinct from old.isdist and new.isdist = 0) then
  begin
    if (exists(
      select s.ref
        from slopoz s
        join ACCSTRUCTUREDISTS a on (a.otable = 'SLOPOZ' and a.oref = s.ref)
        where a.distdef = new.ref))
    then exception is_dist;
  end
end^
SET TERM ; ^
