--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_BI_ORDER FOR POZZAM                         
  ACTIVE BEFORE INSERT POSITION 5 
as
  declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 0;

  /*
  rozwiazanie dotyczace fake-ow ustalone z Marcinem Smereka
  pozycje fake dostaja numer pozycji nadrzednej - alttopoz
  */
  if (coalesce(new.alttopoz,0) > 0 and coalesce(new.fake,0) > 0) then
  begin
    select numer from pozzam where ref = new.alttopoz
    into new.numer;
    new.ord = 1;
  end

  if (new.ord = 1 and new.numer is not null) then begin
    new.ord = 0;
    exit;
  end

  select max(numer) from pozzam where zamowienie = new.zamowienie
    into :maxnum;
  if (new.numer is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.numer = maxnum + 1;
  end else if (new.numer <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update pozzam set ord = 1, numer = numer + 1
      where numer >= new.numer and zamowienie = new.zamowienie;
  end else if (new.numer > maxnum) then
    new.numer = maxnum + 1;
end^
SET TERM ; ^
