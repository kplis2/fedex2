--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDNAG_BI_REF FOR SPRZEDNAG                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SPRZEDNAG')
      returning_values new.REF;
end^
SET TERM ; ^
