--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTYNAG_BI_AKCEPT FOR NOTYNAG                        
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable numberg varchar(40);
begin
  if (new.status=1) then
  begin
    if (new.notakind=0) then
      execute procedure GETCONFIG('WEZWANIANUM')
        returning_values :numberg;
    else if (new.notakind=1) then
      execute procedure GETCONFIG('NOTYODSNUM')
        returning_values :numberg;
    else if (new.notakind = 2) then
      execute procedure GETCONFIG('POTWIERDZENIANUM')
        returning_values :numberg;

    if (numberg is null or numberg='') then
      exception NOTYNAGGEN_NOTDEFINED;
    execute procedure get_number(:numberg, 'NOTA', 'NOTA', new.data, 0, new.ref)
      returning_values new.number, new.symbol;
  end
end^
SET TERM ; ^
