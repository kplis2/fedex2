--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYCEN_BD0 FOR STANYCEN                       
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if((old.ilosc <> 0) or (old.zarezerw <> 0) or (old.zablokow<>0) or (old.zamowiono<>0) ) then
    exception STANYCEN_DELETE_NOT_ALLOW;
end^
SET TERM ; ^
