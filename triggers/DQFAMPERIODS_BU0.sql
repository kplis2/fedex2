--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DQFAMPERIODS_BU0 FOR DQFAMPERIODS                   
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable liczba integer;
begin

  select count(*) from amortization a
  where a.fxdasset = new.fxdasset and a.amperiod = new.amperiod
  into :liczba;

if ( liczba > 0 ) then
  exception DQFAMPERIODS_ERROR;
end^
SET TERM ; ^
