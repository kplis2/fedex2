--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FXDASSETCA_BU_ACCOUNT FOR FXDASSETCA                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable sum_share numeric(14,2);
begin
  if (coalesce(new.status,0) <> coalesce(old.status,0) and coalesce(new.status,0) = 1) then begin
    select sum(coalesce(fxc.share,0))
      from fxdassetca fxc
      where fxc.fxdasset = coalesce(new.fxdasset,0)
        and fxc.amperiod = coalesce(new.amperiod,0)
        and fxc.typeamortization = coalesce(new.typeamortization,0)
    into :sum_share;
    sum_share = coalesce(sum_share,0);
    if (sum_share <> 100 and coalesce(new.status,0) = 1) then
      exception universal 'Bląd - suma udzialów różna od 100%.'||:sum_share;
    new.typeamortization = coalesce(new.typeamortization,0);
  end
  if (coalesce(old.status,0) = 1 and coalesce(new.status, 0) = 1) then begin
    if (coalesce(new.typeamortization, 0) <> coalesce(old.typeamortization, 0)) then
      exception universal'Dokument zaakceptowany - operacja niemożliwa.';
    if (coalesce(new.amperiod, 0) <> coalesce(old.amperiod, 0)) then
      exception universal'Dokument zaakceptowany - operacja niemożliwa.';
    if (coalesce(new.account, 0) <> coalesce(old.account, 0)) then
      exception universal'Dokument zaakceptowany - operacja niemożliwa.';
    if (coalesce(new.share, 0) <> coalesce(old.share, 0)) then
      exception universal'Dokument zaakceptowany - operacja niemożliwa.';
  end
end^
SET TERM ; ^
