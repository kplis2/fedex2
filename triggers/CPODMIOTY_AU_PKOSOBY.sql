--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_AU_PKOSOBY FOR CPODMIOTY                      
  ACTIVE AFTER UPDATE POSITION 7 
as
declare variable cnt integer;
begin
  if(new.firma = 0 and old.firma = 1) then begin
    /*ustalenie PKOSOBy jednego rekordu*/
    select count(*) from PKOSOBY where CPODMIOT = new.REF into :cnt;
    if(:cnt > 1) then exception CPODMIOTY_PKWIECEJNIZ1;
    if(cnt = 1) then begin
      update PKOSOBY set IMIE = new.imie, nazwisko = new.nazwisko, nazwafirmy = new.nazwafirmy, plec = new.plec,
          dataur = new.dataur, wyksztal = new.wyksztal, zawod = new.zawod,
          telefon = new.telefon, fax = new.fax, email = new.email, komorka = new.komorka,
          ptelefon = new.ptelefon, pfax = new.pfax, pemail = new.pemail, pkomorka = new.pkomorka,
          aktywny = new.aktywny
          where CPODMIOT = new.ref;
    end else begin
      insert into PKOSOBY(CPODMIOT, NAZWISKO, IMIE, NAZWAFIRMY, DATAUR, WYKSZTAL, ZAWOD,
                          TELEFON, FAX, EMAIL, KOMORKA,PTELEFON, PFAX, PEMAIL, PKOMORKA, AKTYWNY)
        values(new.ref, new.nazwisko, new.imie, new.nazwafirmy, new.dataur, new.wyksztal, new.zawod,
               new.telefon, new.fax, new.email, new.komorka,new.ptelefon, new.pfax, new.pemail, new.pkomorka, new.aktywny);
    end
  end
  if(new.firma = 0) then begin
    /*synchronizacja danych podstawowych*/
    if(
       (new.imie <> old.imie) or (new.imie is not null and old.imie is null) or (new.imie is null and old.imie is not null) or
       (new.nazwisko <> old.nazwisko) or (new.nazwisko is not null and old.nazwisko is null) or (new.nazwisko is null and old.nazwisko is not null) or
       (new.nazwafirmy <> old.nazwafirmy) or (new.nazwafirmy is not null and old.nazwafirmy is null) or (new.nazwafirmy is null and old.nazwafirmy is not null) or
       (new.plec <> old.plec) or (new.plec is not null and old.plec is null) or (new.plec is null and old.plec is not null) or
       (new.wyksztal <> old.wyksztal ) or (new.wyksztal is not null and old.wyksztal is null) or (new.wyksztal is null and old.wyksztal is not null) or
       (new.zawod <> old.zawod ) or (new.zawod is not null and old.zawod is null) or (new.zawod is null and old.zawod is not null) or
       (new.nazwa <> old.nazwa) or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null) or
       (new.telefon <> old.telefon) or (new.telefon is not null and old.telefon is null) or (new.telefon is null and old.telefon is not null) or
       (new.fax <> old.fax) or (new.fax is not null and old.fax is null) or (new.fax is null and old.fax is not null) or
       (new.email <> old.email) or (new.email is not null and old.email is null) or (new.email is null and old.email is not null) or
       (new.komorka <> old.komorka) or (new.komorka is not null and old.komorka is null) or (new.komorka is null and old.komorka is not null) or
       (new.ptelefon <> old.ptelefon) or (new.ptelefon is not null and old.ptelefon is null) or (new.ptelefon is null and old.ptelefon is not null) or
       (new.pfax <> old.pfax) or (new.pfax is not null and old.pfax is null) or (new.pfax is null and old.pfax is not null) or
       (new.pemail <> old.pemail) or (new.pemail is not null and old.pemail is null) or (new.pemail is null and old.pemail is not null) or
       (new.pkomorka <> old.pkomorka) or (new.pkomorka is not null and old.pkomorka is null) or (new.pkomorka is null and old.pkomorka is not null) or
       (new.aktywny <> old.aktywny) or (new.aktywny is not null and old.aktywny is null) or (new.aktywny is null and old.aktywny is not null)
    ) then
      update PKOSOBY set IMIE = new.IMIE, NAZWISKO = new.NAZWISKO, NAZWAFIRMY = new.NAZWAFIRMY, PLEC= new.PLEC, NAZWA= new.NAZWA,
                           ZAWOD = new.ZAWOD, WYKSZTAL = new.WYKSZTAL,
                           TELEFON = new.telefon, FAX = new.fax, EMAIL = new.email, KOMORKA = new.KOMORKA,
                           PTELEFON = new.ptelefon, PFAX = new.pfax, PEMAIL = new.pemail, PKOMORKA = new.PKOMORKA,
                           AKTYWNY = new.aktywny
             where CPODMIOT = new.ref;

     /* synchronizacja adresu prywatnego i sluzbowego*/
    if(
       (new.pulica <> old.pulica) or (new.pulica is not null and old.pulica is null) or (new.pulica is null and old.pulica is not null) or
       (new.pnrdomu <> old.pnrdomu) or (new.pnrdomu is not null and old.pnrdomu is null) or (new.pnrdomu is null and old.pnrdomu is not null) or
       (new.pnrlokalu <> old.pnrlokalu) or (new.pnrlokalu is not null and old.pnrlokalu is null) or (new.pnrlokalu is null and old.pnrlokalu is not null) or
       (new.pmiasto <> old.pmiasto) or (new.pmiasto is not null and old.pmiasto is null) or (new.pmiasto is null and old.pmiasto is not null) or
       (new.pkodp <> old.pkodp) or (new.pkodp is not null and old.pkodp is null) or (new.pkodp is null and old.pkodp is not null) or
       (new.ppoczta <> old.ppoczta) or (new.ppoczta is not null and old.ppoczta is null) or (new.ppoczta is null and old.ppoczta is not null) or
       (new.pkraj <> old.pkraj) or (new.pkraj is not null and old.pkraj is null) or (new.pkraj is null and old.pkraj is not null) or
       (new.ulica <> old.ulica) or (new.ulica is not null and old.ulica is null) or (new.ulica is null and old.ulica is not null) or
       (new.nrdomu <> old.nrdomu) or (new.nrdomu is not null and old.nrdomu is null) or (new.nrdomu is null and old.nrdomu is not null) or
       (new.nrlokalu <> old.nrlokalu) or (new.nrlokalu is not null and old.nrlokalu is null) or (new.nrlokalu is null and old.nrlokalu is not null) or
       (new.miasto <> old.miasto) or (new.miasto is not null and old.miasto is null) or (new.miasto is null and old.miasto is not null) or
       (new.kodp <> old.kodp) or (new.kodp is not null and old.kodp is null) or (new.kodp is null and old.kodp is not null) or
       (new.poczta <> old.poczta) or (new.poczta is not null and old.poczta is null) or (new.poczta is null and old.poczta is not null) or
       (new.kraj <> old.kraj) or (new.kraj is not null and old.kraj is null) or (new.kraj is null and old.kraj is not null) or
       (new.firma <> old.firma)
    ) then begin
      update PKOSOBY set PULICA = new.PULICA, PNRDOMU = new.PNRDOMU, PNRLOKALU = new.PNRLOKALU,
                         PMIASTO = new.pmiasto, PKODP = new.pkodp, PPOCZTA = new.ppoczta, PKRAJ = new.pkraj,
                         ULICA = new.ULICA, NRDOMU = new.NRDOMU, NRLOKALU = new.NRLOKALU,
                         MIASTO = new.miasto,KODP = new.kodp, POCZTA = new.poczta, KRAJ = new.kraj
      where CPODMIOT = new.REF;
    end
    /* synchronizacja adresu korespondecynego*/
    if(new.korespond = 1) then
      if(
         (new.kulica <> old.kulica) or (new.kulica is not null and old.kulica is null) or (new.kulica is null and old.kulica is not null) or
         (new.knrdomu <> old.knrdomu) or (new.knrdomu is not null and old.knrdomu is null) or (new.knrdomu is null and old.knrdomu is not null) or
         (new.knrlokalu <> old.knrlokalu) or (new.knrlokalu is not null and old.knrlokalu is null) or (new.knrlokalu is null and old.knrlokalu is not null) or
         (new.kmiasto <> old.kmiasto) or (new.kmiasto is not null and old.kmiasto is null) or (new.kmiasto is null and old.kmiasto is not null) or
         (new.kkodp <> old.kkodp) or (new.kkodp is not null and old.kkodp is null) or (new.kkodp is null and old.kkodp is not null) or
         (new.kpoczta <> old.kpoczta) or (new.kpoczta is not null and old.kpoczta is null) or (new.kpoczta is null and old.kpoczta is not null) or
         (new.kkraj <> old.kkraj) or (new.kkraj is not null and old.kkraj is null) or (new.kkraj is null and old.kkraj is not null) or
         (new.firma <> old.firma)
      ) then
        update PKOSOBY set KULICA = new.KULICA, KNRDOMU = new.KNRDOMU, KNRLOKALU = new.KNRLOKALU,
            KMIASTO = new.Kmiasto, KKODP = new.Kkodp, KPOCZTA = new.Kpoczta, KKRAJ = new.Kkraj
          where CPODMIOT = new.REF;
    /* synchronizacja sposobu oznaczania adresu */
    if(new.korespond <> old.korespond or (new.firma <> old.firma))
      then update PKOSOBY set KORESPOND=new.korespond where CPODMIOT = new.ref;
  end else begin
    /* synchronizacja adresow sluzbowych i nazwy firmy*/
    if(
       (new.nazwa <> old.nazwa) or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null) or
       (new.ulica <> old.ulica) or (new.ulica is not null and old.ulica is null) or (new.ulica is null and old.ulica is not null) or
       (new.nrdomu <> old.nrdomu) or (new.nrdomu is not null and old.nrdomu is null) or (new.nrdomu is null and old.nrdomu is not null) or
       (new.nrlokalu <> old.nrlokalu) or (new.nrlokalu is not null and old.nrlokalu is null) or (new.nrlokalu is null and old.nrlokalu is not null) or
       (new.miasto <> old.miasto) or (new.miasto is not null and old.miasto is null) or (new.miasto is null and old.miasto is not null) or
       (new.kodp <> old.kodp) or (new.kodp is not null and old.kodp is null) or (new.kodp is null and old.kodp is not null) or
       (new.poczta <> old.poczta) or (new.poczta is not null and old.poczta is null) or (new.poczta is null and old.poczta is not null) or
       (new.kraj <> old.kraj) or (new.kraj is not null and old.kraj is null) or (new.kraj is null and old.kraj is not null) or
       (new.firma <> old.firma)
    ) then
      update PKOSOBY set NAZWAFIRMY = new.NAZWA,
          ULICA = new.ULICA, NRDOMU = new.NRDOMU, NRLOKALU = new.NRLOKALU,
          MIASTO = new.miasto,KODP = new.kodp, POCZTA = new.poczta, KRAJ = new.kraj
        where CPODMIOT = new.REF;
    if(new.aktywny=0 and old.aktywny=1) then
      update PKOSOBY set AKTYWNY = 0 where CPODMIOT = new.REF;
  end
end^
SET TERM ; ^
