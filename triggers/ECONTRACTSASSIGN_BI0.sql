--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRACTSASSIGN_BI0 FOR ECONTRACTSASSIGN               
  ACTIVE BEFORE INSERT POSITION 1 
AS
declare variable cnt int;
begin
  if (new.employees is not null) then
  begin
    select count(*) from operator where operator.employee = new.employees
      into :cnt;
/*    if (:cnt = 0) then
      exception OPERATOR_NOT_ASSIGNED;
    else */if (:cnt = 1) then
      select operator.ref from operator where operator.employee = new.employees
        into new.operator;
  end
end^
SET TERM ; ^
