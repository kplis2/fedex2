--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TRASY_BU_REPLICAT FOR TRASY                          
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if((new.symbol <> old.symbol)
   or(new.opis <> old.opis) or (new.opis is null and old.opis is not null) or (new.opis is not null and old.opis is null)

  )then
   waschange = 1;

  execute procedure rp_trigger_bu('TRASY',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
