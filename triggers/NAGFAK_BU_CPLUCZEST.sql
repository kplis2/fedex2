--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_BU_CPLUCZEST FOR NAGFAK                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if(new.cpluczestid <> old.cpluczestid
    or (new.cpluczestid is not null and old.cpluczestid is null)
  ) then begin
    if(exists(
       select ref
        from NAGFAK join TYPFAK on (NAGFAK.typ = TYPFAK.symbol)
        where NAGFAK.grupadok = new.grupadok and typfak.korekta = 0
          and NAGFAK.REF <> new.ref and nagfak.cpluczestid is not null
    )) then
      exception UNIVERSAL 'Inny dok. grupy ma wskazanego uczest. prog. loj.';
  end
end^
SET TERM ; ^
