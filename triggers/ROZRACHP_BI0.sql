--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACHP_BI0 FOR ROZRACHP                       
  ACTIVE BEFORE INSERT POSITION 1 
as
  declare variable walutowy smallint;
begin

  if (new.klient>0 and (new.slodef is null or new.slodef=-1)) then
  begin
    select min(REF) from SLODEF where TYP = 'KLIENCI'
      into new.slodef;
    new.slopoz = new.klient;
  end

  if (new.kontofk is null or new.kontofk = '') then
    select max(KONTOFK) from ROZRACH
      where company = new.company and slodef = new.slodef
        and slopoz = new.slopoz and symbfak = new.symbfak
      into new.kontofk;

  if (new.kontofk is null) then new.kontofk = '';

  select walutowy from rozrach
    where company = new.company and slodef = new.slodef and slopoz = new.slopoz
      and symbfak = new.symbfak and kontofk = new.kontofk
    into :walutowy;

  if (new.faktura is null) then
    select faktura from rozrach 
      where company = new.company and slodef = new.slodef and slopoz = new.slopoz
        and symbfak = new.symbfak and kontofk = new.kontofk
      into new.faktura;

  if (new.waluta is null) then
    select waluta from ROZRACH
      where company = new.company and kontofk = new.kontofk
        and symbfak = new.symbfak and slodef = new.slodef and slopoz = new.slopoz
      into new.waluta;

  if(new.klient is null) then
    select KLIENT from ROZRACH
      where company = new.company and kontofk = new.kontofk
        and symbfak = new.symbfak and slodef = new.slodef and slopoz = new.slopoz
      into new.klient;
  if (new.waluta is null) then new.waluta = 'PLN';
  if ((new.kurs is null or new.kurs = 0) and walutowy<>1) then
    new.kurs = 1;

  if (walutowy <> 1) then
  begin
    new.kurs = 1;
    new.winienzl = new.winien;
    new.mazl = new.ma;
  end

  if(new.winien is null) then new.winien = 0;
  if(new.ma is null) then new.ma = 0;

  if ((new.winienzl is null or new.winienzl=0) and new.kurs<>0) then
    new.winienzl = new.winien * new.kurs;
  if ((new.mazl is null or new.mazl=0) and new.kurs<>0) then
    new.mazl = new.ma * new.kurs;
  if (new.winienzl is null) then new.winienzl = 0;
  if (new.mazl is null) then new.mazl = 0;

  if (new.skad is null) then new.skad = 0;

  if (new.interestdate is null) then  new.interestdate = new.data;
  if (new.oddzial is null or (new.oddzial = '')) then
    execute procedure getconfig('AKTUODDZIAL') returning_values new.oddzial;

  if (new.stable is null) then new.stable = '';
  if (new.sref is null) then new.sref = 0;
end^
SET TERM ; ^
