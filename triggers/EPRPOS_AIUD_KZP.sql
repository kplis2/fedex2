--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRPOS_AIUD_KZP FOR EPRPOS                         
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 
AS
  declare variable fund integer;
  declare variable credit integer;
  declare variable premdate date;
  declare variable symbol varchar(20);
  declare variable coltype varchar(3);
begin
/*Synchronizacja rat pozyczek i skladek naliczonych na liscie plac ze splatami
  w module socjalnym. Przy delete skladnika sychro zapewniaja klucze obce. */

  if (deleting or (updating and new.pvalue = old.pvalue and new.ecolumn = old.ecolumn)) then
    exit;

  select coltype from ecolumns
    where number = coalesce(new.ecolumn,old.ecolumn)
      and number <> 7540   --nalezy wykluczyc wpisowe
    into :coltype;

  if (coltype = 'SKL') then
  begin
    if (updating and new.pvalue <> old.pvalue and new.ecolumn = old.ecolumn) then
      update efundprems set premvalue = new.pvalue where eprpos = new.ref;
    else begin
      if (not inserting) then
        delete from efundprems
           where ref in (select p.ref from efunds f
                           join efundprems p on (f.ref = p.fund)
                           where p.eprpos = old.ref and f.fundtype = old.ecolumn);

      select first 1 ref from efunds
        where employee = new.employee
          and fundtype = new.ecolumn
        into :fund;

      if (fund is not null) then
      begin
        select payday, symbol from epayrolls
          where ref = new.payroll
          into :premdate, :symbol;
        insert into efundprems values (null,:fund,:premdate,new.pvalue,:symbol,new.ref);
      end
    end
  end else

  if (coltype = 'KRE') then
  begin
    if (updating and new.pvalue <> old.pvalue and new.ecolumn = old.ecolumn) then
      update ecredinstals set insvalue = new.pvalue where eprpos = new.ref;
    else begin
      if (not inserting) then
        delete from ecredinstals
          where ref in (select i.ref from ecredits c
                          join ecredinstals i on (c.ref = i.credit)
                          where i.eprpos = old.ref and c.credtype = old.ecolumn);

      select first 1 ref from ecredits
        where employee = new.employee
          and credtype = new.ecolumn
          and actvalue > 0
        order by fromdate
        into :credit;

      if (credit is not null) then
      begin
        select payday, symbol from epayrolls
          where ref = new.payroll
          into :premdate, :symbol;
        insert into ecredinstals (credit, insvalue, document, instdate, eprpos)
          values (:credit,new.pvalue,:symbol,:premdate,new.ref);
      end
    end
  end
end^
SET TERM ; ^
