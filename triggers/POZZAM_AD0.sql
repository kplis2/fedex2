--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_AD0 FOR POZZAM                         
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  execute procedure OBLICZ_NAGZAM(old.zamowienie);
  execute procedure NAGZAM_CHECK_ZREAL(old.zamowienie);
  delete from DOKUMSER where REFPOZ = old.ref and TYP = 'Z';
  if(old.popref > 0) then
    update NAGZAM set KPOPREF = old.popref where KPOPREF = old.ref;
  delete from relations where stablefrom = 'POZZAM' and sreffrom = old.ref;
  delete from relations where stableto = 'POZZAM' and srefto = old.ref;
end^
SET TERM ; ^
