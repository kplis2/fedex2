--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDELEGATIONS_BU0 FOR EDELEGATIONS                   
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if ((new.rating<>old.rating) or (new.accomscurr<>old.accomscurr) or (new.drivelumpcurr<>old.drivelumpcurr) or (old.allowancescurr<>new.allowancescurr)) then begin
    new.accomscountryzl = new.accomscurr * new.rating;
    new.drivelumpcountryzl = new.drivelumpcurr * new.rating;
    new.allowancescountryzl = new.allowancescurr * new.rating;
  end
end^
SET TERM ; ^
