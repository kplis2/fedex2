--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CENNIK_BU_REPLICAT FOR CENNIK                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if ((((
    (new.CENNIK<>old.CENNIK) or (new.cennik is null and old.cennik is not null) or 
    (new.cennik is not null and old.cennik is null) or (new.jedn <> old.jedn) or (NEW.REPLICAT <> OLD.REPLICAT) OR
         (new.cenanet <> old.cenanet) or (new.cenanet is null and old.cenanet is not null) or 
         (new.cenanet is not null and old.cenanet is null) or (new.cenabru <> old.cenabru) or 
         (new.cenabru is null and old.cenabru is not null) or (new.cenabru is not null and old.cenabru is null) or
         (new.cenapnet <> old.cenapnet) or (new.cenapnet is null and old.cenapnet is not null) or 
         (new.cenapnet is not null and old.cenapnet is null) or (new.cenapbru <> old.cenapbru) or 
         (new.cenapbru is null and old.cenapbru is not null) or (new.cenapbru is not null and old.cenapbru is null)
           ) and (new.akt  = 1 or old.akt = 1)
       ) or (new.akt <> old.akt)
      ) and new.replicat = 1
     ) then 
   waschange = 1;

  execute procedure rp_trigger_bu('CENNIK',old.cennik, old.wersjaref, old.jedn, null, null, old.token, old.state,
        new.cennik, new.wersjaref, new.jedn, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
