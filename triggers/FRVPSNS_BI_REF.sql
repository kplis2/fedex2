--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRVPSNS_BI_REF FOR FRVPSNS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FRVPSNS')
      returning_values new.REF;
end^
SET TERM ; ^
