--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PKOSOBY_BU_KADRES FOR PKOSOBY                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable nastim timestamp;
begin
 if(new.korespond = 0) then begin
   /* adres prywatny lub siedziby firmy*/
   new.kulica = new.pulica;
   new.knrdomu = new.pnrdomu;
   new.knrlokalu = new.pnrlokalu;
   new.kmiasto = new.pmiasto;
   new.kkodp = new.pkodp;
   new.kpoczta = new.ppoczta;
   new.kkraj = new.pkraj;
 end else if(new.korespond = 2) then begin
   new.kulica = new.ulica;
   new.knrdomu = new.nrdomu;
   new.knrlokalu = new.nrlokalu;
   new.kmiasto = new.miasto;
   new.kkodp = new.kodp;
   new.kpoczta = new.poczta;
   new.kkraj = new.kraj;
 end
 if(new.imie is null) then new.imie = '';
 if(new.nazwisko is null) then new.nazwisko = '';
 if(new.imie<>'' or new.nazwisko<>'') then
   new.nazwa = new.imie || ' ' || new.nazwisko;
 else
   new.nazwa = new.nazwafirmy;
 if((new.miesim<>old.miesim) or (new.dzienim<>old.dzienim) or (new.przypomn<>old.przypomn)) then begin
   execute procedure PKOSOBY_IMIENINY(new.dzienim,new.miesim,new.przypomn) returning_values new.dataprzypim;
 end
 if (new.pesel <> old.pesel and new.pesel='') then new.pesel = null;
end^
SET TERM ; ^
