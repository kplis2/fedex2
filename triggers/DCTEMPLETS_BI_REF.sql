--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DCTEMPLETS_BI_REF FOR DCTEMPLETS                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DCTEMPLETS')
      returning_values new.REF;
end^
SET TERM ; ^
