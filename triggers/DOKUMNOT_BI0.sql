--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNOT_BI0 FOR DOKUMNOT                       
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable numpoakcept smallint;
declare variable numberg varchar(40);
declare variable lokres varchar(6);
declare variable chronologia smallint;
declare variable cnt integer;
declare variable przecena smallint;
begin
  if(new.blokada is null) then new.blokada = 0;
  if(new.mag2 is null) then new.mag2 = '';
  if(new.akcept is null) then new.akcept = 0;
  if(new.operator = 0) then new.operator = NULL;
  select PRZECENA from DEFDOKUM where SYMBOL=new.typ into :przecena;
  if(:przecena is null) then przecena = 0;
  /* dokument przeceny nie ma powiazania do dokumentu magazynowego */
  if((new.grupa is null) or (new.grupa = 0))
    then new.grupa = new.ref;
  if(new.grupa = new.ref and :przecena = 0 and ((new.dokumnagkor is null) or (new.dokumnagkor = 0))) then
    exception DOKUMNOT_BEZDOKUMKOR;
  if(new.wartosc is null) then new.wartosc = 0;
  if(new.dokumnagkor is not null) then begin
    select magazyn from dokumnag where ref=new.dokumnagkor into new.magazyn;
  end
  if((new.symbol is null or (new.symbol = '')) AND (new.numer is null or (new.numer = 0))) then begin
    /*numerator*/
    select DEFMAGAZ.numpoakcept from DEFMAGAZ where SYMBOL=new.magazyn into :numpoakcept;
    if(:numpoakcept is null) then numpoakcept = 0;
    select NUMBER_NOTGEN from DEFMAGAZ where  symbol = new.magazyn into :numberg;
    if(:numberg is not null) then begin
      if(:numpoakcept = 1 and new.akcept = 0) then begin
        new.numer = -1;
        new.symbol = 'TYM/'||new.ref;
      end else
        execute procedure Get_NUMBER(:numberg, new.magazyn, new.typ, new.data, 0, new.ref) returning_values new.numer, new.symbol;
    end
  end
  if((new.oddzial is null) or (new.oddzial = '')) then  begin
    select ODDZIAL from DEFMAGAZ where SYMBOL = new.magazyn into new.oddzial;
    if(new.oddzial is null or (new.oddzial = '')) then
      execute procedure GETCONFIG('AKTUODDZIAL') returning_values new.oddzial;
  end
  /*sprawdzenie okresu z data */
  lokres = new.okres;
    lokres = cast( extract( year from new.data) as char(4));
    if(extract(month from new.data) < 10) then
       lokres = :lokres ||'0' ||cast(extract(month from new.data) as char(1));
    else begin
       lokres = :lokres ||cast(extract(month from new.data) as char(2));
    end
  if((new.okres is null) or (new.okres = '      ')) then
    new.okres = :lokres;
  else if(new.okres <> :lokres) then exception DOKUMNAG_DATA_POZA_OKRES;
  /*kontrola akceptacji dokumentow pozniejszych */
  if(new.akcept = 1)  then begin
    select DEFMAGAZ.chronologia from DEFMAGAZ where SYMBOL=new.magazyn into :chronologia;
    if(:chronologia is null) then chronologia = 0;
    if(:chronologia > 0) then begin
      cnt = null;
      select count(*) from DOKUMNOT where MAGAZYN=new.magazyn and DATA>=(new.data+:chronologia) and akcept = 1 into :cnt;
      if(:cnt is null) then cnt = 0;
      if(:cnt > 0) then
        exception DOKUMNG_NEW_AKCEP_POZNIEJ;
    end
  end
  if(new.rodzaj is null) then begin
    if(new.grupa=new.ref) then new.rodzaj = 0;
    else begin
      select RODZAJ from DOKUMNOT where REF=new.grupa into new.rodzaj;
      if(new.rodzaj is null) then new.rodzaj = 0;
    end
  end
end^
SET TERM ; ^
