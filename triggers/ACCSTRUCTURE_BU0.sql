--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTURE_BU0 FOR ACCSTRUCTURE                   
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable i int;
  declare variable tmp BKSYMBOL_ID;
  declare variable yearid int;
  declare variable company int;
  declare variable sep char;
  declare variable len smallint;
begin
  select symbol, yearid, company from bkaccounts where ref = old.bkaccount
    into :tmp, :yearid, :company;

  if ((new.nlevel <> old.nlevel or new.separator <> old.separator or new.dictdef <> old.dictdef)
      and exists(select ref from accounting where yearid = :yearid and company = :company and account starting with :tmp))
  then
    exception FK_CANT_UPDATE_ACCSTRUCTURE;

  if (new.len <> old.len
      and exists(select ref from accounting where company = :company and account starting with :tmp))
  then
    exception FK_CANT_UPDATE_ACCSTRUCTURE 'Istnieją otwarte konta księgowe używające tego słownika';

  if (new.dictdef is null) then
    exception null_dictdef;
  select kodln from slodef where (slodef.ref = new.dictdef) into new.len;

  -- sprawdzenie czy konto nie jest zbyt długie
  select synlen from bkyears
    join bkaccounts on (bkaccounts.yearid = bkyears.yearid and bkaccounts.company = bkyears.company)
    where bkaccounts.ref = new.bkaccount
    into :i;

  for
    select separator, len
    from accstructure
    where bkaccount = new. bkaccount and ref<>new.ref
    into :sep, :len
  do
  begin
    if (:sep<>',') then
      i = :i + 1;
    i = :i + :len;
  end

  if (new.separator <>',') then
    i = :i + 1;
  i = :i + new.len;

  if (:i>120) then
    exception TOO_LONG_ACCOUNT;
end^
SET TERM ; ^
