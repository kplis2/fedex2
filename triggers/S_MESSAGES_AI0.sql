--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_MESSAGES_AI0 FOR S_MESSAGES                     
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable defmedium varchar(10);
begin
  if (new.status = 0) then
  begin
    defmedium = null;
    select s.symbol
      from s_defmediums s
      where s.ref = new.defmedium
      into :defmedium;

    if ((:defmedium = 'MESSAGE' or :defmedium = 'BALOONHINT' or :defmedium is null)
      and coalesce(new.deliverdate, current_timestamp(0)) <= current_timestamp(0)
    ) then
      POST_EVENT 'MESSAGE' || cast(new.tooper as varchar(10));
  end
end^
SET TERM ; ^
