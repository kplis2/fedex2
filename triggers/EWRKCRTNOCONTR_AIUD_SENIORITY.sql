--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWRKCRTNOCONTR_AIUD_SENIORITY FOR EWRKCRTNOCONTR                 
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 1 
AS
  declare variable fromdate date;
  declare variable todate date;
  declare variable employee integer;
  declare variable wfromdate date;
  declare variable wtodate date;
  declare variable wemployee integer;
  declare variable sy smallint;
  declare variable sm smallint;
  declare variable sd smallint;
  declare variable workcertif integer;
  declare variable wref integer;
begin

  workcertif = coalesce(new.workcertif, old.workcertif);
  select w.fromdate, w.todate, w.employee
    from eworkcertifs w
    where w.ref = :workcertif
    into :fromdate, :todate, :employee;

  execute procedure count_seniority(:workcertif, :employee, :fromdate, :todate,1)
    returning_values sy, sm, sd;
  update eworkcertifs set syears_zewn = :sy, smonths_zewn = :sm,  sdays_zewn = :sd
      where ref = :workcertif;

end^
SET TERM ; ^
