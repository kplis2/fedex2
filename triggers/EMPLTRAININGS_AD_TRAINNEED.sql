--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLTRAININGS_AD_TRAINNEED FOR EMPLTRAININGS                  
  ACTIVE AFTER DELETE POSITION 0 
AS
  declare variable trainneed integer;
begin
  select trainneed
    from etrainings
    where ref = old.training
    into :trainneed;

  update empltrainneeds N
    set N.etraining = null
    where N.etrainneed = :trainneed and N.etraining = old.training and N.employee = old.employee;
end^
SET TERM ; ^
