--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CENNIK_BU_ZMIANA FOR CENNIK                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if((new.cenanet<>old.cenanet) or
     (new.waluta<>old.waluta) or
     (new.jedn<>old.jedn)) then
  begin
    new.DATAZMIANY = current_timestamp(0);
  end
end^
SET TERM ; ^
