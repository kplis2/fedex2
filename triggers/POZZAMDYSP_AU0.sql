--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAMDYSP_AU0 FOR POZZAMDYSP                     
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable ilosc numeric(14,4);
begin
  /* naliczenie ilosci zadysponowanych */
  select sum(ILOSC) from POZZAMDYSP where POZZAM = new.POZZAM into :ilosc;
  if(:ilosc is null) then ilosc = 0;
  update POZZAM set ILDYSP = :ilosc where REF=new.POZZAM and ILDYSP <> :ilosc;

end^
SET TERM ; ^
