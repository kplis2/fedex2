--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKATAL_BI_REF FOR CKATAL                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CKATAL')
      returning_values new.REF;
end^
SET TERM ; ^
