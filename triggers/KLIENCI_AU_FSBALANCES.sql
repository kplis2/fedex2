--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_AU_FSBALANCES FOR KLIENCI                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable slodef INTEGER ;
begin
  if ( coalesce(old.dostawca,0) <> coalesce(new.dostawca,0)) then
  begin
    select max(s.ref)
      from slodef s
      where s.typ = 'KLIENCI'
      into :slodef;
    if (:slodef is not null) then
      execute procedure calculate_fsbalances(:slodef,new.ref);
  end
end^
SET TERM ; ^
