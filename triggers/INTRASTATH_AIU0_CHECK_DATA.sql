--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INTRASTATH_AIU0_CHECK_DATA FOR INTRASTATH                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
    -- sprawdzanie  numeru wlasnego
    if (coalesce( new.number,'' ) = '' ) then
      exception intrastat_number;

    else if(substring(new.number  from 1 for 2) <> substring(new.period  from  3 for 4 )) then
      exception intrastat_number_check;

    else if( coalesce(char_length(new.number),0) < 6 ) then  -- [DG] XXX ZG119346
      exception intrastat_number_len_check;

    -- sprawdzanie numeru UC
    if (coalesce( new.ucsymbol,'') = '' ) then
      exception intrastat_ucsymbol;

    else if( coalesce(char_length(new.ucsymbol),0) < 6 ) then  -- [DG] XXX ZG119346
      exception intrastat_ucsymbol_len_check;

    -- sprawdzanie okresu
    if ( coalesce( new.period, '') = '' ) then
      exception INTRASTAT_OKRES;

    -- sprawdzanie daty
    if ( coalesce( new.filldocdate,'') = '' ) then
      exception intrastat_date;

    -- sprawdzanie emaila
    if ( coalesce( new.email, '') = '' ) then
      exception intrastat_email;

    -- sprawdzanie miasta
    if ( coalesce( new.city, '') = '' ) then
      exception intrastat_city;

    -- sprawdzanie numeru dokumentu
    if ( coalesce( new.intrnumber,'' ) = '' ) then
      exception intrastat_intrnumber;

    -- sprawdzanie wersji dokumentu
    if ( coalesce( new.intrversion,'') = '' ) then
      exception intrastat_intrnumber 'Pole wersja dokumentu musi być wypełnione.';

    -- sprawdzenie wypelniającego
    if ( coalesce( new.operator,'') = '' ) then
      exception intrastat_operator;
end^
SET TERM ; ^
