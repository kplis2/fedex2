--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZADANIA_AU0 FOR ZADANIA                        
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.ckontrakt<>old.ckontrakt or (new.ckontrakt is not null and old.ckontrakt is null) or (new.ckontrakt is null and old.ckontrakt is not null)) then begin
    if(old.ckontrakt is not null) then execute procedure CKONTRAKTY_UPDATE_CPODMIOTYZAD(old.ckontrakt);
    if(new.ckontrakt is not null) then execute procedure CKONTRAKTY_UPDATE_CPODMIOTYZAD(new.ckontrakt);
  end
  if(new.stan <> old.stan and new.wfworkflow is not null) then execute procedure wfworkflow_status_set(new.wfworkflow);
end^
SET TERM ; ^
