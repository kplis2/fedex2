--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSD_BU1 FOR LISTYWYSD                      
  ACTIVE BEFORE UPDATE POSITION 1 
as
declare variable listawysref integer;
declare variable konfigctrl char(1);
declare variable konfigwaga char(1);
declare variable konfigauto char(1);
declare variable cnt integer;
declare variable okres varchar(6);
declare variable dummyts timestamp;
declare variable spedytor varchar(40);
begin
  execute procedure GETCONFIG('LISTYWYSDACKCTRL') returning_values :konfigctrl;
  execute procedure GETCONFIG('LISTYWYSDWAGAOPK') returning_values :konfigwaga;
  execute procedure GETCONFIG('LISTYWYSAUTONEW') returning_values :konfigauto;

  --akceptacja dokumentu spedycyjnego
  if (new.akcept = 1 and coalesce(old.akcept, 0) = 0) then begin

    if (:konfigctrl = '1') then begin
      --sprawdzenie czy zostaly jeszcze towary do spakowania
      cnt = 0;
      select count(lwp.ref)
        from listywysdpoz lwp
          left join listywysdroz lwr on (lwp.ref = lwr.listywysdpoz)
        where lwp.dokument = new.ref
          and lwp.ilosc > coalesce(lwp.iloscspk, 0)
      into :cnt;
      if (:cnt > 0) then
        exception LISTYWYSD_AKCEPT 'Zostały towary do spakowania. Akceptacja dokumentu spedycyjnego niemożliwa';

      --sprawdzenie czy zostaly jeszcze opakowania do zamkniecia
      cnt = 0;
      select count(lwo.ref) from listywysdroz_opk lwo
        where lwo.listwysd = new.ref and coalesce(lwo.stan,0) = 0 and lwo.rodzic is null
      into :cnt;
      if (:cnt > 0) then
        exception LISTYWYSD_AKCEPT 'Zostały opakowania do zamknięcia. Akceptacja dokumentu spedycyjnego niemożliwa';
    end

    if (:konfigwaga = '1') then begin
      execute procedure listywysd_oblnag(new.ref) returning_values new.waga, new.iloscpaczek, new.iloscpalet;
    end
  end
  --zamkniecie dokumentu spedycyjnego
  else if (new.akcept = 2 and coalesce(old.akcept, 0) = 1) then begin
    if (:konfigauto = '1') then begin
      select first 1 lw.ref
        from listywys lw
          join spedytwys spw on (lw.spedytor = spw.spedytor)
        where spw.sposdost = new.sposdost
          and lw.stan = 'O'
        order by lw.dataotw
      into new.listawys;

      if (new.listawys is not null) then begin
        update listywys set
        --waga = waga + new.waga,    KPI XXX138061
        iloscpaczek = iloscpaczek + new.iloscpaczek
          where ref = new.listawys;
      end
      else begin
        execute procedure GEN_REF('LISTYWYS') returning_values :listawysref;

        execute procedure datatookres(cast(current_date as varchar(15)),0,0,0,0)
          returning_values :okres, :dummyts, :dummyts, :dummyts;

        select spedytor from spedytwys where sposdost = new.sposdost into :spedytor;

        insert into listywys(REF, SYMBOL, NUMER, SPEDYTOR, KIEROWCA, TRASA, TRASANAZWA, WAGA, DATAOTW,
            DATAZAMK, DATAPLWYS, DATAWYS, STAN, BLOKADA, OPIS, WYDRUKOWANO, SYMBOLZEWN,
            ILOSCPACZEK, KOSZTDOST, TYP, ILDOK, ILSPR, NIEBEZPIECZNY, OPERATOR,
            OKRES, OBJETOSC)
          values(:listawysref, null,-1,:spedytor, null, null, null, new.waga,
                 current_timestamp(0), null, current_timestamp(0), null, 'O', 0, 'Utworzony automatycznie',
                 0, null, new.iloscpaczek, new.kosztdost,
                 1, 1, 1, 0, new.acceptoper, :okres, new.objetosc);
        new.listawys = :listawysref;
      end
    end
  end
  --wycofanie zamkniecia dokumentu spedycyjnego
  else if (new.akcept < 2 and coalesce(old.akcept, 0) = 2) then begin
    if (exists(select first 1 1 from listywys where ref = old.listawys and stan in ('A','Z'))) then
      exception LISTYWYSD_AKCEPT 'Nie można wycofać dokumentu spedycyjnego z zamknietego zlecenia transportowego';
  end
  else if (coalesce(new.akcept,0) = 0 and old.akcept = 1) then begin
    new.waga = 0;
    new.iloscpaczek = 0;
    new.iloscpalet = 0;
    new.termdost = null;
  end
end^
SET TERM ; ^
