--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERZAM_BU FOR DEFOPERZAM                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(coalesce(new.fakdepend,0)<>coalesce(old.fakdepend,0) or
     coalesce(new.faknojoin,0)<>coalesce(old.faknojoin,0)) then begin
    if(new.fakdepend=1 and new.faknojoin=1) then
      exception DEFOPERZAM_FAKDEPEND;
  end
end^
SET TERM ; ^
