--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_HASHDATA_AD FOR S_HASHDATA                     
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable nr integer;
begin
  select max(number) from s_hashdata where datatype=old.datatype into :nr;
  if(:nr is not null and :nr<>old.number) then
    update s_hashdata set number = old.number where datatype=old.datatype and number=:nr;
end^
SET TERM ; ^
