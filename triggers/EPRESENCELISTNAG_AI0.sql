--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRESENCELISTNAG_AI0 FOR EPRESENCELISTNAG               
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  insert into epresencelistspos (EPRESENCELISTNAG, epresencelists, econtractsdef,
      operator, accord, timestartstr, timestopstr, employee)
    select new.ref, new.epresencelist , e.econtractsdef,
        new.operator,e.accord,null,null,new.employee
      from econtractsassign e
      where e.employees = new.employee;
end^
SET TERM ; ^
