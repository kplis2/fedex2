--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZSAD_AI0 FOR POZSAD                         
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable sumacla  numeric(14,2);
declare variable sumavat  numeric(14,2);
declare variable psumacla  numeric(14,2);
declare variable psumavat  numeric(14,2);
begin
  select sum(clo),sum(vat), sum(pclo), sum(pvat) from POZSAD
  where POZSAD.faktura=new.faktura into :sumacla, :sumavat, :psumacla, :psumavat;
  update NAGFAK set SUMCLO=:sumacla,SUMVAT=:sumavat, PSUMCLO=:psumacla,PSUMVAT=:psumavat
  where NAGFAK.REF = new.faktura;
end^
SET TERM ; ^
