--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ATRYBUTY_BU_WERSJE FOR ATRYBUTY                       
  ACTIVE BEFORE UPDATE POSITION 1 
as
declare variable vwersje smallint;
begin
  if(new.cecha<>old.cecha or new.nrwersji<>old.nrwersji) then begin
    select wersje from defcechy where symbol = new.cecha into :vwersje;
    if (:vwersje = 0 and new.nrwersji>0) then exception ATRYBUT_CECHA;
  end
end^
SET TERM ; ^
