--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECALENDARS_AIU_ECALPATTERN FOR ECALENDARS                     
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
AS
  declare variable d timestamp;
begin
  --DS: bezposrednie wywolanie dodania dni kalendarza w triggerze (wczesniej bylo zaszyte w kodzie)
  --wzorzec zostanie dodany wewnatrz procedury
  --jezeli dodajemy kalendarz od kilku lat wstecz pouzupelniaja nam sie dni do roku biezacego
  if (inserting or new.startdate <> old.startdate) then
  begin
    d = new.startdate;
    while (extract(year from d) <= extract(year from current_date)) do
    begin
      if (inserting or not exists(select c.ref from ecaldays c where c.calendar = new.ref and c.cdate = :d)) then
        execute procedure ecal_setpattern(new.ref,extract(year from :d),:d) returning_values d;
      --jezeli nie dodajemy dni, rok nalezy dodac, zeby sie nie zapetlic
      else d = (cast(extract(year from d) as integer) + 1)||'-01-01';
    end
  end
end^
SET TERM ; ^
