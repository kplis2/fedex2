--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CRMCLAIMBILLS_AMOUNTBILL FOR CRMCLAIMBILLS                  
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.amount <> old.amount) then
  update crmclaims cs set  cs.amountbill = new.amount;
end^
SET TERM ; ^
