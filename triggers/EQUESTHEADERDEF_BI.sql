--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EQUESTHEADERDEF_BI FOR EQUESTHEADERDEF                
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_equestheaderdef,1);
end^
SET TERM ; ^
