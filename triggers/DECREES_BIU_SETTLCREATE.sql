--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_BIU_SETTLCREATE FOR DECREES                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
begin
  if (new.settlement is not null and new.settlement <> '' and new.settlcreate = 1
      and (inserting or (new.status = 0 and old.status = 0))) then
  begin
    if (exists(select ref from decrees
        where account = new.account and settlement = new.settlement
          and settlcreate = 1 and ref <> new.ref and company = new.company)) then
    begin
      if (new.autodoc = 1) then
        new.settlcreate = 0;
      else
        exception DECREES_EXPT 'Rozrachunek (' || new.settlement || ') jest już założony przez inny dokument!';
    end
    if (new.opendate is null) then
      exception DECREES_EXPT 'Proszę uzupełnić datę otwarcia rozrachunku!';
  end
end^
SET TERM ; ^
