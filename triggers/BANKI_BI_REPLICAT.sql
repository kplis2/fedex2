--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKI_BI_REPLICAT FOR BANKI                          
  ACTIVE BEFORE INSERT POSITION 1 
AS
begin
 execute procedure rp_trigger_bi('BANKI',new.symbol, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
