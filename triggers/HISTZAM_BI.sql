--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER HISTZAM_BI FOR HISTZAM                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  if(new.realout is NULL) then new.realout = 0;
  if(new.dpout is null) then new.dpout = 0;
  if(new.dbout is null) then new.dbout = 0;
  if(new.dkout is null) then new.dkout = 0;
  if(new.ddout is null) then new.ddout = 0;
  if(new.kisreal is null) then new.kisreal = 0;
  if(new.noback is null) then new.noback = 0;
  if(new.dokumentmain is null) then new.dokumentmain = 0;
END^
SET TERM ; ^
