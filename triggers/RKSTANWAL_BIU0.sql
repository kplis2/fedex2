--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKSTANWAL_BIU0 FOR RKSTANWAL                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if (new.stan < 0) then
  begin
    if (not exists (select first 1 1 from rkstnkas r where r.kod = new.rkstnkas and coalesce(r.saldom,0) = 1)) then
      exception rkstanwal_expt 'Niedozwolony stan ujemny';
  end
end^
SET TERM ; ^
