--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TRASY_AD_REPLICAT FOR TRASY                          
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  execute procedure RP_TRIGGER_AD('TRASY', old.ref, NULL, NULL, NULL, NULL, old.token, old.STATE);
end^
SET TERM ; ^
