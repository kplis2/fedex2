--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVPOSITIONS_BD_ORDER FOR SRVPOSITIONS                   
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update srvpositions set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and srvrequest = old.srvrequest;
end^
SET TERM ; ^
