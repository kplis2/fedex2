--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDESPOS_BD0 FOR PRSCHEDGUIDESPOS               
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (exists (select first 1 1 from prschedguidedets where prschedguidepos = old.ref and accept > 0)) then
    exception prschedguidedets_error 'Wydano już surowce do przewodnika';
  delete from prschedguidedets where prschedguidepos = old.ref and accept = 0;
end^
SET TERM ; ^
