--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VAT_AU0 FOR VAT                            
  ACTIVE AFTER UPDATE POSITION 0 
as
  declare variable slodef integer;
  declare variable currentyear integer;
begin
  if (new.stawka <> old.stawka) then
    update TOWARY set STATE=-1 where VAT=new.grupa;

  -- aktulizacja opisu kont ksiegowych dla bieżącego roku
  if(new.name<>old.name) then begin
    currentyear = extract(year from current_timestamp(0));
    for select ref from slodef where typ = 'VAT'
      into :slodef
    do begin
      execute procedure accounting_recalc_descript(:slodef, :currentyear, null, new.grupa);
    end
  end
end^
SET TERM ; ^
