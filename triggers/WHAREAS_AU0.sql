--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHAREAS_AU0 FOR WHAREAS                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.number <> old.number or new.l <> old.l) then
    execute procedure MWS_ROWDIST_CALCULATE(new.whsecrow);
end^
SET TERM ; ^
