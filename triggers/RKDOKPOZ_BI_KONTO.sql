--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_BI_KONTO FOR RKDOKPOZ                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable oldkonto ACCOUNT_ID;
  declare variable kontokas ACCOUNT_ID;
begin
  select stn.konto from rkdoknag rn
      left join rkstnkas stn on (rn.stanowisko = stn.kod)
    where rn.ref = new.dokument
    into :kontokas;

  oldkonto = new.konto;
  if (new.konto is null or new.konto='') then
    execute procedure xk_rk_gen_acc(new.dokument, new.pozoper) returning_values new.konto;
  if (new.konto2 is null ) then
    new.konto2 = kontokas;
  if(new.konto is null or (new.konto = '')) then
    new.konto = oldkonto;
  if (new.konto5 is null) then new.konto5 = '';
  if (new.konto5 <> '' and new.konto5 not like '5%') then exception rkdokpoz_bad5acc;
end^
SET TERM ; ^
