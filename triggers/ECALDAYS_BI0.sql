--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECALDAYS_BI0 FOR ECALDAYS                       
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable daylength double precision;
begin
  --automatyczne wypelnienie pól YEAR, MONTH, DAY
  new.CYEAR = CAST(extract(year from new.CDATE) as integer);
  new.CMONTH = CAST(extract(month from new.CDATE) as smallint);
  new.CDAY = CAST(extract(day from new.CDATE) as smallint);
  new.DAYOFWEEK = CAST(extract(weekday from new.CDATE) as smallint);
  new.WORKSTARTSTR = SUBSTRing(new.WORKSTART from 1 for 5);
  new.WORKENDSTR   = SUBSTRing(new.WORKEND from 1  for 5);
  if (new.WORKSTARTSTR CONTAINING '00:00' and new.daykind <> 1) then
    new.WORKSTARTSTR = '';
  if (new.WORKENDSTR CONTAINING '00:00' and new.daykind <> 1) then
    new.WORKENDSTR = '';
  --wyliczenie czasu pracy
  new.worktime = new.workend - new.workstart;
  if(new.worktime < 0) then begin
    new.worktime = 86400+new.worktime;
  end
  if(new.worktime = 0 and new.WORKSTARTSTR CONTAINING '00:00' and new.daykind = 1)then begin
    --dzien roboczy od polnocy dopolnocy
    new.worktime = 86400;
  end
  execute procedure durationstr2(new.worktime) returning_values new.worktimestr;
  daylength = 86400;
  new.prworktime = new.worktime/:daylength;
  new.workhours = cast(new.worktime as numeric(14,2)) / 3600.00;
end^
SET TERM ; ^
