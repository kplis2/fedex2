--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BTRANSFERPOS_BU0 FOR BTRANSFERPOS                   
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.company is null) then
    select company from BTRANSFERS where ref=new.btransfer into new.company;


   if(new.maxamount is null) then new.maxamount = 0;
   if(new.amount is null) then new.amount = 0;
   if(new.amount <> old.amount) then begin
     execute procedure btransfers_checkmaxamount(new.ref) returning_values new.maxamount;
/*     if(new.amount > new.maxamount) then exception BTRANSFERPOS_AMOUNTTOBIG;*/
/*     if(new.amount < 0) then exception BTRANSFERPOS_AMOUNTZERO;*/
   end
end^
SET TERM ; ^
