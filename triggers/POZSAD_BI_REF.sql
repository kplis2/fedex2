--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZSAD_BI_REF FOR POZSAD                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('POZSAD')
      returning_values new.REF;
end^
SET TERM ; ^
