--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_BI_RIGHTS FOR DECREES                        
  ACTIVE BEFORE INSERT POSITION 3 
as
declare variable aktuoper integer;
declare variable konf varchar(255);
declare variable rights varchar(255);
declare variable rightsgroup varchar(255);
declare variable opergroup varchar(1024);
declare variable allaclen integer;
declare variable aclen integer;
declare variable dictdef integer;
declare variable sdnazwa varchar(20);
declare variable spnazwa varchar(40);

begin
  execute procedure get_global_param ('AKTUOPERATOR') returning_values aktuoper;
  execute procedure get_config('DECREESRIGHTS',0) returning_values :konf;
  if (:konf = 1 and coalesce(new.autodoc,0) <> 1) then
  begin
    -- weryfikacja dostepu do kont syntetycznych
    select rights, rightsgroup from bkaccounts where ref = new.accref
           into :rights, :rightsgroup;
    select grupa from operator where ref = :aktuoper into :opergroup;
    if ((rights <> '' and rights is not null) or (rightsgroup <> '' and rightsgroup is not null)) then
    begin
      if (not((strmulticmp(';'||aktuoper||';',:rights)=1) or (strmulticmp(opergroup,rightsgroup)=1))) then
        exception rights 'Brak uprawnień do konta syntetycznego';
    end

    -- weryfikacja dostepu do kont analitycznych
    select rights, rightsgroup from accounting ac where ac.company = new.company and ac.yearid = substring(new.period from 1 for 4)
           and ac.account = new.account and ac.currency = new.currency
           into :rights, :rightsgroup;
    if ((rights <> '' and rights is not null) or (rightsgroup <> '' and rightsgroup is not null)) then
    begin
      if (not((strmulticmp(';'||aktuoper||';',:rights)=1) or (strmulticmp(opergroup,rightsgroup)=1))) then
        exception rights 'Brak uprawnień do konta analitycznego ' || new.account;
    end

    -- weryfikacja dostepu do slownikow analitycznych
    allaclen = 4; --dlugosc sytnetyki + separator
    for select acs.len, acs.dictdef from accstructure acs where acs.bkaccount = new.accref
        into :aclen, :dictdef
    do begin
       select rights, rightsgroup, slodef.nazwa, sp.kod from slopoz sp left join slodef on (slodef.ref = sp.slownik) where sp.slownik = :dictdef
             and sp.kod = substring(new.account from :allaclen+1 for  :allaclen + :aclen)
             into :rights, :rightsgroup, :sdnazwa, :spnazwa;
       if ((rights <> '' and rights is not null) or (rightsgroup <> '' and rightsgroup is not null)) then
       begin
         if (not((strmulticmp(';'||aktuoper||';',:rights)=1) or (strmulticmp(opergroup,rightsgroup)=1))) then
           exception rights 'Brak uprawnień do slownika analitycznego: "'||:sdnazwa||'", pozycja "'||:spnazwa||'"';
       end
       allaclen = allaclen + aclen + 1; --dodajemy dlugosc slownika + 1 na separator
    end
  end
end^
SET TERM ; ^
