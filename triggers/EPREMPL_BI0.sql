--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPREMPL_BI0 FOR EPREMPL                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable status integer;
begin

    select status
      from epayrolls
      where ref = new.epayroll
      into :status;

    if (status > 0) then exception payroll_is_closed;
end^
SET TERM ; ^
