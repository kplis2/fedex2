--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_BU_REPLICAT FOR POZZAM                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable mankalk smallint;
begin
  if (new.ktm<>old.ktm or new.ilosc<>old.ilosc or new.cenanet<>old.cenanet) then begin
    update nagzam set state=-1
      where ref=old.zamowienie;
  end
  select t.kiloscreczna
    from nagzam n
      join typzam t on (t.symbol = n.typzam)
    where n.ref = new.zamowienie
    into :mankalk;
  if (coalesce(mankalk, 0)<>0) then begin
    new.ilosc=new.kilosc;
  end
end^
SET TERM ; ^
