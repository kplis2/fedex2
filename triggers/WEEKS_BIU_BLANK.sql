--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WEEKS_BIU_BLANK FOR WEEKS                          
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.lastday is null) then new.lastday = new.firstday + 6;
  if (new.descript is null) then new.descript = new.firstday||' - '||new.lastday;
  new.weekyear = extract(year from (new.firstday + 4));
  new.weekno = (new.lastday - 4 - cast(new.weekyear||'-01-01' as date) + 1) / 7 + 1;
end^
SET TERM ; ^
