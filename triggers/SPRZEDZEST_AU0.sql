--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDZEST_AU0 FOR SPRZEDZEST                     
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable slodef integer;
declare variable cnt integer;
declare variable rozrach varchar(100);
declare variable skrot varchar(20);
declare variable kontofk KONTO_ID;
declare variable kontofkkl KONTO_ID;
begin
  if(new.status = 'Z' and old.status = 'O') then begin
    /* zamkniecie zestawieni - stowrzenie ewentualnie rozrachunku */
    execute procedure GETCONFIG('OBSSPRZEDROZRACH') returning_values :rozrach;
    if(:rozrach is null or (:rozrach = ''))then rozrach = '0';
    if(:rozrach = '1') then begin
      /*zalozenie rozrachunku*/
      select ref, prefixfk from SLODEF where TYP = 'SPRZEDAWCY' into :slodef, :kontofk;
      if(:slodef is null) then exception SLODEF_BEZSPRZEDAWCY;
      select KONTOFK from SPRZEDAWCY where REF=new.sprzedawca into :kontofkkl;
      if(:kontofk is null) then kontofk = '';
      if(:kontofkkl is null) then kontofkkl = '';
      kontofk = :kontofk || :kontofkkl;
      select substring(skrot from 1 for 20) from SPRZEDAWCY where REF=new.sprzedawca into :skrot; --XXX ZG133796 MKD
      if(skrot is null) then skrot = '';
      skrot = :skrot||'/'|| cast(new.data as date);
      insert into ROZRACH(SLODEF, SLOPOZ,KONTOFK, KLIENT,SYMBFAK,FAKTURA, DATAOTW,DATAPLAT,WALUTOWY,WALUTA, SKAD, SPRZEDZESTID)
        values(:slodef, new.sprzedawca, :kontofk,NULL,:skrot,NULL,new.datazam, new.datazam + 14,0,NULL,5,new.ref);
      insert into ROZRACHP(SLODEF, SLOPOZ,KLIENT,SYMBFAK,OPERACJA,FAKTURA,DATA,TRESC,WINIEN,MA,WALUTA,KURS, SKAD)
        values(:slodef,new.sprzedawca,NULL,:skrot,'Zamk. zestawienia',NULL,new.datazam,'Otwarcie rozrachunku',0,new.dowyplaty,NULL, 1,5);

    end
  end else if(new.status = '0' and old.status <> '0') then begin
    /* wycofanie ewentualne rozrachunkow */
      select ref, prefixfk from SLODEF where TYP = 'SPRZEDAWCY' into :slodef, :kontofk;
      if(:slodef is null) then exception SLODEF_BEZSPRZEDAWCY;
      select SYMBFAK from ROZRACH where SPRZEDZESTID = new.ref into :skrot;
      delete from ROZRACHP where SLODEF = :slodef and SLOPOZ = new.sprzedawca and  SYMBFAK=:skrot and SKAD = 5;
      select count(*) from ROZRACHP where SLODEF = :slodef and SLOPOZ = new.sprzedawca and SYMBFAK = :skrot into :cnt;
      if(:cnt > 0) then exception SPRZEDZEST_BYLY_WYPLATY;
      delete from ROZRACH where SLODEF = :slodef and SLOPOZ = new.sprzedawca and SYMBFAK = :skrot and SKAD = 5;
  end
end^
SET TERM ; ^
