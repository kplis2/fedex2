--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_BI110_KOLOR FOR DOKUMNAG                       
  ACTIVE BEFORE INSERT POSITION 110 
AS
begin
   --nadaje kolor poczatkowy dokumentom
   --0 czarny inne
   --1 nie wszystko wygenerowane czerwony
   --2 wszystko wygenerowane ale nie wszystko zlozone oliwkowy
  if (new.x_kolor is null) then
  begin
    if (new.typ in ('WZ','WZk')) then
        new.x_kolor = 1;
    else
        new.x_kolor = 0;
  end

end^
SET TERM ; ^
