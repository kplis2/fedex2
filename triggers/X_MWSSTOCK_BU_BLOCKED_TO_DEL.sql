--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWSSTOCK_BU_BLOCKED_TO_DEL FOR MWSSTOCK                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (new.blocked is distinct from old.blocked and new.x_blocked = 1) then
    exception universal 'Operacja zabroniona-dopisz do ZG 126909';
end^
SET TERM ; ^
