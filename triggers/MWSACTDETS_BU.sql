--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTDETS_BU FOR MWSACTDETS                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable lot integer;
declare variable mwsconstlocl integer;
declare variable mwsconstlocp integer;
declare variable vers integer;
begin
  if (new.vers <> old.vers or new.lot <> old.lot
    or coalesce(new.mwsconstlocp,0) <> coalesce(old.mwsconstlocp,0)
    or coalesce(new.mwsconstlocl,0) <> coalesce(old.mwsconstlocl,0)
  ) then
  begin
    select vers, coalesce(mwsconstlocl,0), coalesce(mwsconstlocp,0), coalesce(lot,0)
      from mwsacts a where a.ref = new.mwsact
      into vers, mwsconstlocl, mwsconstlocp, lot;
    if ((lot > 0 and lot <> new.lot) or (vers > 0 and vers <> new.vers)
      or (mwsconstlocl > 0 and mwsconstlocl <> coalesce(new.mwsconstlocl,0))
      or (mwsconstlocp > 0 and mwsconstlocp <> coalesce(new.mwsconstlocp,0))
    ) then
      exception mwsactdets_error 'Zmiana na rozpisce niemożliwa';
  end
end^
SET TERM ; ^
