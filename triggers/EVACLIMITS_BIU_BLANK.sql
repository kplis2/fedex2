--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACLIMITS_BIU_BLANK FOR EVACLIMITS                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable vc int;
declare variable vc2 integer;
begin
--Trigger blankujacy

  if (inserting or new.employee <> coalesce(old.employee,0) or new.vyear <> coalesce(old.vyear,0))
  then begin
    if (exists(select first 1 1 from evaclimits
                 where employee = new.employee and vyear = new.vyear and ref <> coalesce(new.ref,0))
    ) then
      exception universal 'Jeden pracownik może posiadać tylko jedną kartę na dany rok!';
    else begin
    --blank na company
      select company from employees
        where ref = new.employee
        into new.company;

    --wyliczenie urlopu wykorzystanego
      execute procedure get_config('VACCOLUMN', 2) returning_values :vc;
      execute procedure get_config('VACREQCOLUMN', 2) returning_values :vc2;
  
      select sum(worksecs)/60 from eabsences
        where employee = new.employee
          and ayear = new.vyear
          and ecolumn in (:vc,:vc2)
          and correction in (0,2)
        into new.limitcons;

    end
  end

  new.limitcons = coalesce( new.limitcons,0); --wykorzystany
  new.actlimit = coalesce(new.actlimit,0); --aktualny
  new.addlimit = coalesce(new.addlimit,0); --uzupelniajacy
  new.disablelimit = coalesce(new.disablelimit,0); --rehabilitacyjny
  new.traininglimit = coalesce(new.traininglimit,0); --szkoleniowy
  new.otherlimit = coalesce(new.otherlimit,0); --inny
  new.prevlimit = coalesce(new.prevlimit,0); --zalegly
--PR60316
  new.vacreqlimit = coalesce(new.vacreqlimit,0);
  new.vacreqused = coalesce(new.vacreqused,0);
  new.vacreqremain = coalesce(new.vacreqremain,0);
  new.vacmdlimit = coalesce(new.vacmdlimit,0);
  new.vacmdused = coalesce(new.vacmdused,0);
  new.vacmdremain = coalesce(new.vacmdremain,0);

  if (coalesce(new.limitconsh,'') = '') then new.limitconsh = '0';
  if (coalesce(new.actlimith,'') = '') then new.actlimith = '0';
  if (coalesce(new.addlimith,'')= '') then new.addlimith = '0';
  if (coalesce(new.disablelimith,'') = '') then new.disablelimith = '0';
  if (coalesce(new.traininglimith,'') = '') then new.traininglimith = '0';
  if (coalesce(new.otherlimith,'') = '') then new.otherlimith = '0';
  if (coalesce(new.prevlimith,'') = '') then new.prevlimith = '0';
  if (coalesce(new.supplementallimith,'') = '') then new.supplementallimith = '0';

  new.actlimit_cust = coalesce(new.actlimit_cust,0);
  new.addlimit_cust = coalesce(new.addlimit_cust,0);
  new.addvdate_cust = coalesce(new.addvdate_cust,0);
  new.disablelimit_cust = coalesce(new.disablelimit_cust,0);
  new.traininglimit_cust = coalesce(new.traininglimit_cust,0);
end^
SET TERM ; ^
