--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WERSJE_BI0 FOR WERSJE                         
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable vaktywnytow smallint;
begin
  select coalesce(tow.rights,''), coalesce(tow.rightsgroup,'')
    from towtypes tow where tow.numer = new.usluga
  into new.prawa, new.prawagrup;
  if(new.mwscalcmix is null) then new.mwscalcmix = 0;
  select NAZWA, MIARA, CHODLIWY, grupa, usluga, akt from TOWARY where KTM = new.ktm into new.nazwat, new.miara, new.chodliwy, new.grupa, new.usluga, :vaktywnytow;
  if(new.cena_zakn is null) then new.cena_zakn = 0;
  if(new.cena_zakb is null) then new.cena_zakb = 0;
  if(new.cena_fabn is null) then new.cena_fabn = 0;
  if(new.cena_fabb is null) then new.cena_fabb = 0;
  if(new.cena_kosztn is null) then new.cena_kosztn = 0;
  if(new.cena_kosztb is null) then new.cena_kosztb = 0;
  if(new.cena_zaknl is null) then new.cena_zaknl = 0;
  if(new.dniwazn is null) then new.dniwazn = 0;
  if(new.nieobrot is null) then new.nieobrot = 0;
  if(new.ilosc is null) then new.ilosc = 0;
  if(new.zamowiono is null) then new.zamowiono = 0;
  if(new.zarezerw is null) then new.zarezerw = 0;
  if(new.zablokow is null) then new.zablokow = 0;
  if(new.akt = 1 and :vaktywnytow = 0) then
    exception TOWAR_NIEAKTYWNY 'Towar nieaktywny. Nie można aktywować jego wersji.';
  new.lastcenzmiana = current_time;
  if (new.vat = '') then new.vat = null;
  if (new.vat is not null) then
  begin
    if (exists(select t.ktm from towary t where t.ktm = new.ktm and t.vat = new.vat)) then
      new.vat = null;
  end 
  if (new.pkwiu = '') then new.pkwiu = null;
  if(exists(select ref from wersje where ktm=new.ktm and nazwa=new.nazwa and ref<>new.ref)) then
    exception WERSJE_NAZWA;
end^
SET TERM ; ^
