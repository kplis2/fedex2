--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_LISTYWYSDROZ_OPK_AU_EXP FOR LISTYWYSDROZ_OPK               
  ACTIVE AFTER UPDATE POSITION 100 
as
  declare variable status status_id;
begin
-- insert do tabeli przesylajacej nr paczek do wapro, dopiero po uzupelnieniu wszystkich nr i po zamknieciu pakowania
  select ld.akcept from listywysd ld where ld.ref = new.listwysd
    into :status;
  if (coalesce(status,0) in (1,2)) then begin

      insert into X_IMP_PACZKI (NAGID, SYMBOL, NRLISTU, DATAGEN, STATUS, DATAPRZET, REF_SENTE)
        select dn.int_id, dn.int_symbol, opk.kodkresk, ld.dataakc, 10, current_timestamp, opk.ref
      from listywysdroz_opk opk
        join listywysd ld on ld.ref = opk.listwysd
        join dokumnag dn on dn.ref = ld.x_grupasped
        where opk.listwysd = new.listwysd;
  end
end^
SET TERM ; ^
