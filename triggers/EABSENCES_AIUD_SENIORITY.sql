--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_AIUD_SENIORITY FOR EABSENCES                      
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 
as
  declare variable wfromdate date;
  declare variable wtodate date;
  declare variable employee integer;
  declare variable sy smallint;
  declare variable sm smallint;
  declare variable sd smallint;
  declare variable wref integer;
begin
  --DS: jesli jest to urlop bezplatny
  if (coalesce(new.ecolumn, old.ecolumn) in (10,300,330) and (inserting or deleting
    or new.ecolumn <> old.ecolumn or new.fromdate <> old.fromdate)
  ) then begin
    employee = coalesce(new.employee,old.employee);
    --pobierz pierwsze swiadectwo pracy ktore pokrywa sie z okresem urlopu bezplatnego
    select first 1 w.ref, w.fromdate, w.todate
      from eworkcertifs w
      where w.employee = :employee
        and w.todate >= coalesce(new.fromdate, old.fromdate)
      order by w.fromdate
      into :wref, :wfromdate, :wtodate;

    if (wref is not null) then
    begin
      --i przelicz staz pracy dla danego swiadectwa, triggery przelicza dla nastepnych jezeli zajdzie potrzeba
      execute procedure count_seniority(:wref, :employee, :wfromdate, :wtodate,0) returning_values sy, sm, sd;
      execute statement 'update eworkcertifs w set w.syears = '||:sy||', w.smonths = '||:sm||', w.sdays = '||:sd||' where w.ref = '||:wref;
    end
  end
end^
SET TERM ; ^
