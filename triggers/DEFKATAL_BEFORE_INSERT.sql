--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFKATAL_BEFORE_INSERT FOR DEFKATAL                       
  ACTIVE BEFORE INSERT POSITION 1 
as
begin
  execute procedure REPLICAT_STATE('DEFKATAL',new.state, new.ref, NULL, NULL, NULL) returning_values new.state;
end^
SET TERM ; ^
