--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFREJZAM_BI_REPLICAT FOR DEFREJZAM                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
 execute procedure rp_trigger_bi('DEFREJZAM',new.symbol, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
