--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDULES_BU0 FOR PRSCHEDULES                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if(new.prschedgroup <> old.prschedgroup and exists(select ref from prschedules
    where prdepart = new.prdepart and prschedgroup = new.prschedgroup and ref <> new.ref)
  ) then exception prschedules_error substring('Istnieje już harmonogram grupy: '||new.prschedgroup||' na wydziale: '||new.prdepart from 1 for 70);
  if(new.main = 1) then
    update PRSCHEDULES set MAIN = 0 where PRDEPART = new.prdepart and main = 1;
end^
SET TERM ; ^
