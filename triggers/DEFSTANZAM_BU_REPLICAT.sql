--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFSTANZAM_BU_REPLICAT FOR DEFSTANZAM                     
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.REF<>old.REF
      or new.nazwa<>old.nazwa
      ) then
   waschange = 1;

  execute procedure rp_trigger_bu('DEFSTANZAM',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
