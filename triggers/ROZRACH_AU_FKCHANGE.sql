--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACH_AU_FKCHANGE FOR ROZRACH                        
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if (new.kontofk <> old.kontofk or new.symbfak <> old.symbfak
      or new.slopoz <> old.slopoz or new.slodef <> old.slodef
      or new.company <> old.company) then
  begin
    update rozrachp set slodef = new.slodef, slopoz = new.slopoz, kontofk = new.kontofk,
        symbfak = new.symbfak, company = new.company
      where slodef = old.slodef and slopoz = old.slopoz and kontofk = old.kontofk
        and symbfak = old.symbfak and company = old.company;
    update btransferpos set account = new.kontofk, settlement = new.symbfak
      where btransfer in (select ref from btransfers where slodef = new.slodef and slopoz = new.slopoz)
        and account = old.kontofk and settlement = old.symbfak;
  end

  if (coalesce(new.faktura,0) <> coalesce(old.faktura,0)) then
    update rozrachp p set faktura = new.faktura
      where p.symbfak = new.symbfak
      and p.slodef = new.slodef
      and p.slopoz = new.slopoz
      and p.kontofk = new.kontofk
      and p.company = new.company
      and coalesce(p.faktura,0) = coalesce(old.faktura,0);
end^
SET TERM ; ^
