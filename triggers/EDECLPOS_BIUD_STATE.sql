--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDECLPOS_BIUD_STATE FOR EDECLPOS                       
  ACTIVE BEFORE INSERT OR UPDATE OR DELETE POSITION 0 
AS
  declare variable state smallint;
begin
  select d.state
    from edeclarations d
    where d.ref = coalesce(new.edeclaration, old.edeclaration)
    into :state;
  if (state > 0) then
    exception edecl_state;
end^
SET TERM ; ^
