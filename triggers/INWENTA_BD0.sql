--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INWENTA_BD0 FOR INWENTA                        
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable wersjaref integer;
declare variable cena numeric(14,4);
declare variable dostawa integer;
declare variable inwentaparent integer;
declare variable inwentaparents varchar(20);
begin
  if(old.zamk = 1) then begin
    exception INWENTA_ZAAKCEPTOWANE;
  end
  update INWENTAP set ILSTAN = 0, BLOKADANALICZ = 1 where INWENTA = old.ref;/*by odblokwać trigera na usuwanie rekordu ze stanem > 0*/
  if(old.inwentaparent >0) then begin
    for select WERSJAREF, CENA,  DOSTAWA
    from INWENTAP where inwenta = old.ref
  into :wersjaref, :cena, :dostawa
    do begin
       execute procedure INWENTAP_OBLICZMASTER(old.inwentaparent, :wersjaref, :cena, :dostawa, 0);
    end
  end
 if(old.zbiorczy = 1) then begin
   if(exists(select inwenta.ref from inwenta where old.ref = inwenta.inwentaparent)) then
      exception INWENTA_SA_AKCEPT_DOK_POZ 'Usunięcie niemożliwe. Arkusz posiada arkusze podrzędne.';
 end


end^
SET TERM ; ^
