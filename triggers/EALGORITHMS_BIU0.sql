--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EALGORITHMS_BIU0 FOR EALGORITHMS                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
  declare variable lastvar integer;
  declare variable proced varchar(1024);
begin
  if (new.autocalculate is null) then new.autocalculate = 0;
  if (new.alwaysshow is null) then new.alwaysshow = 0;
  if (new.customized is null) then new.customized = 0;

  if (coalesce(new.customized,0) <> coalesce(old.customized,0)
    or coalesce(new.procedparam,'') <> coalesce(old.procedparam,'')
    or coalesce(new.proced,'') <> coalesce(old.proced,'')
  ) then begin
    proced = trim(iif(new.customized = 1, new.procedparam, new.proced));
    proced = trim(trailing '
' from proced);
    execute procedure STATEMENT_MULTIPARSE(:proced, 'EF_', '', '', 1, 1)
      returning_values new.decl, new.body, :lastvar;
  end
end^
SET TERM ; ^
