--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACLIMITS_BI_REF FOR EVACLIMITS                     
  ACTIVE BEFORE INSERT POSITION 20 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EVACLIMITS')
      returning_values new.REF;
end^
SET TERM ; ^
