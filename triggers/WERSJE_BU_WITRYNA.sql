--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WERSJE_BU_WITRYNA FOR WERSJE                         
  ACTIVE BEFORE UPDATE POSITION 9 
AS
declare variable twitryna smallint;
begin
  if (new.nrwersji = 0 and new.witryna <> old.witryna) then begin
    select witryna from towary
      where ktm = new.ktm
    into :twitryna;

    if (new.witryna <> :twitryna) then exception WERSJA_PODSTAWOWA;
  end
end^
SET TERM ; ^
