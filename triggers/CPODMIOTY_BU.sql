--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPODMIOTY_BU FOR CPODMIOTY                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if(new.nip<>old.nip or (old.nip is null and new.nip is not null) or (old.nip is not null and new.nip is null)) then
    new.prostynip = replace(new.nip, '-', '');
  if(new.cpowiaty = 0) then new.cpowiaty = NULL;
  if(new.obywatelstwo = 0) then new.obywatelstwo = NULL;
end^
SET TERM ; ^
