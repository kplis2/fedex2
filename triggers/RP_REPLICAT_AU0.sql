--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RP_REPLICAT_AU0 FOR RP_REPLICAT                    
  ACTIVE AFTER UPDATE POSITION 0 
as
begin

  if(new.replicat_type = 0 and old.replicat_type <> 0) then begin
    insert into RP_ITEM(ID_OBJ,PARENT_ID,STABLE,TYP)
      values(new.S_OBJ,0,new.SD_TABLE,1);
   insert into RP_ITEM(ID_OBJ,PARENT_ID,STABLE,TYP)
     values(new.D_OBJ,0,'',0);
  end else if((new.replicat_type = 0) and (new.SD_TABLE <> old.SD_TABLE) and(new.SOURCE_DBASE = old.SOURCE_DBASE)) then
    update RP_ITEM set STABLE=new.SD_TABLE where ID_OBJ = new.S_OBJ and PARENT_ID=0;

  if((new.SOURCE_DBASE <> old.SOURCE_DBASE) or (new.DEST_DBASE <> old.dest_dbase)) then begin
      if(new.SOURCE_DBASE <> old.source_dbase) then begin
         delete from RP_ITEM where ID_OBJ = old.S_OBJ and PARENT_ID = 0;
         insert into RP_ITEM(ID_OBJ,PARENT_ID,STABLE,TYP,FIELD)
           values(new.S_OBJ,0,new.SD_TABLE,1,'');
      end
      delete from RP_ITEM where ID_OBJ=old.D_OBJ and PARENT_ID = 0;
      insert into RP_ITEM(ID_OBJ,PARENT_ID,STABLE,TYP,FIELD)
        values(new.D_OBJ,0,'',0,'');
  end
end^
SET TERM ; ^
