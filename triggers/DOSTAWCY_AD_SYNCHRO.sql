--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTAWCY_AD_SYNCHRO FOR DOSTAWCY                       
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable refkli integer ;
declare variable synchro smallint;
begin
 select konfig.wartosc from konfig where konfig.akronim = 'SYNCHROKLIENT'
    into :synchro;
  if (synchro = 1) then
  begin
    select klienci.ref from klienci where klienci.dostawca = old.ref
    into :refkli;
    if (refkli is not null) then
    delete from klienci where klienci.ref = :refkli;
  end
end^
SET TERM ; ^
