--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RAPORTY_BU0 FOR RAPORTY                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable lokres varchar(6);
begin
  if (new.stawkapaliwa is null) then new.stawkapaliwa = 0;
  if (new.kosztenpoz is null) then new.kosztenpoz = 0;
  if (new.kosztrob is null) then new.kosztrob = 0;
  if(new.ktm = '') then exception universal 'Brak wartoci w polu KTM';
  if (new.numer is null) then exception universal 'Brak wartoci w polu Numer raportu';
  if(new.oddaty <> old.oddaty) then
  begin
    lokres = cast( extract( year from new.oddaty) as char(4));
    if(extract(month from new.oddaty) < 10) then
      lokres = :lokres ||'0' ||cast(extract(month from new.oddaty) as char(1));
    else begin
      lokres = :lokres ||cast(extract(month from new.oddaty) as char(2));
    end
    new.okres = lokres;
  end
  --Sprawdzenie nakdladania sie przedzialów
  if (new.ktm <> old.ktm or new.typ <> old.typ or new.oddaty <> old.oddaty
    or new.dodaty <> old.dodaty) then
  begin
     --Sprawdzenie poprawnoc przedzialu
    if (new.oddaty > new.dodaty)
      then exception universal 'Zle zdefiniowany przedział dat. Pole Od daty większe niż Do daty';
    if (exists (select * from raporty
      where raporty.ktm = new.ktm and raporty.typ = new.typ and raporty.rodzaj = new.rodzaj and raporty.magazyn=new.magazyn
      and new.oddaty <= raporty.dodaty and new.dodaty >= raporty.oddaty))
    then exception universal 'Wprowadzony przedział nakada się z innym przdziałem';
  end
end^
SET TERM ; ^
