--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRACTS_BIU_CHKDATES FOR EMPLCONTRACTS                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
as
begin
--Sprawdzenie czy daty sie nie przecinaja

  if (inserting or (updating and (old.fromdate <> new.fromdate or old.empltype <> new.empltype
      or new.enddate is distinct from old.enddate or new.employee <> old.employee
      or new.todate is distinct from old.todate or new.econtrtype <> old.econtrtype))
  ) then
    execute procedure emplcontracts_chkdates(new.ref, new.fromdate, coalesce(new.enddate,new.todate), new.employee, new.empltype, new.econtrtype);

end^
SET TERM ; ^
