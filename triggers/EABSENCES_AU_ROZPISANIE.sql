--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_AU_ROZPISANIE FOR EABSENCES                      
  ACTIVE AFTER UPDATE POSITION 0 
as
  declare variable lastday date;
  declare variable firstday date;
  declare variable afromdate date;
  declare variable atodate date;
  declare variable bfromdate date;
  declare variable btodate date;
  declare variable noinsert smallint;
  declare variable apayroll integer;
  declare variable bpayroll integer;
  declare variable bfirstabsence integer;
  declare variable bbaseabsence integer;
  declare variable aregtimestamp timestamp;
  declare variable aregoperator integer;
  declare variable achgtimestamp timestamp;
  declare variable achgoperator integer;
  declare variable bregtimestamp timestamp;
  declare variable bregoperator integer;
  declare variable bchgtimestamp timestamp;
  declare variable bchgoperator integer;
begin
/*trigger rozbija rekord na kilka innych, jezeli aktualizowana pozycja obejmuje
  swoim zasiegiem wiecej niz 1 miesiac                                       */

--trigger musi sie wywolac zawsze jezeli tylko zmiana dat (BS20435)
  if (new.fromdate <> old.fromdate or new.todate <> old.todate) then
  begin
  --skasowanie starego wiersza
    delete from eabsences where ref = new.ref;

    firstday = old.ayear || '/' || old.amonth || '/1';
    execute procedure MonthLastDay(firstday)
      returning_values :lastday;

    if (new.fromdate > :lastday or new.todate < :firstday
        or (extract(year from new.fromdate) || extract(month from new.fromdate) = old.ayear || old.amonth
          and extract(year from new.todate) || extract(month from new.todate) = old.ayear || old.amonth))
    then begin
    /*zakres poprawionej nieobecnosci wykroczyl poza pierwotny miesiac lub zmiana
      odbyla sie tylko w jego zakresie (moze wystapic podzial na chorobe-zasilek);
      uruchomienie dodawania - trigger do rozbicia zostanie uaktywniony*/
      insert into eabsences (ref, ecolumn, employee, fromdate, todate,
          dokdate, scode, bcode, monthpaybase, daypayamount,
          fbase, lbase, certserial, certnumber, company, epayroll,
          firstabsence, baseabsence, calcpaybase, regtimestamp,
          regoperator, chgtimestamp, chgoperator, dimnum, dimden,
          correction, corrected, corepayroll, mflag,emplcontract)
        values (new.ref, new.ecolumn, new.employee, new.fromdate, new.todate,
          new.dokdate, new.scode, new.bcode, new.monthpaybase, new.daypayamount,
          new.fbase, new.lbase, new.certserial, new.certnumber, new.company, new.epayroll,
          new.firstabsence, new.baseabsence, new.calcpaybase, new.regtimestamp,
          new.regoperator, new.chgtimestamp, new.chgoperator, new.dimnum, new.dimden,
          new.correction, new.corrected, new.corepayroll, new.mflag, new.emplcontract);
    end
    else begin
    --zakres nieobecnosci obejmuje przelom miesiaca - dwa inserty
      if (new.fromdate < :firstday) then
      begin
      --nieobecnosc zostla 'rozciagnieta w dol'
        afromdate = new.fromdate;          atodate = firstday - 1;
        bfromdate = firstday;              btodate = new.todate;
        apayroll = null;          --NULLuj payroll poza biezacym miesiacem
        bpayroll = new.epayroll;  --wstaw payroll tylko dla biezacego miesiaca
        bregtimestamp = new.regtimestamp;
        bregoperator = new.regoperator;
        bchgtimestamp = new.chgtimestamp;
        bchgoperator = new.chgoperator;
      end else
      if (new.todate > :lastday) then
      begin
      --nieobecnosc zostla 'rozciagnieta w gore'
        afromdate = new.fromdate;          atodate = :lastday;
        bfromdate = lastday + 1;           btodate = new.todate;
        apayroll = new.epayroll;           bpayroll = null;
        aregtimestamp = new.regtimestamp;
        aregoperator = new.regoperator;
        achgtimestamp = new.chgtimestamp;
        achgoperator = new.chgoperator;
      end else
        noinsert = 1;

      if (coalesce(noinsert,0) = 0) then
      begin
      /*(wazne, aby ponizszy insert (gdzie ustawiamy ref) rozpoczynal sie od
         nowgo fromdate. Reszte zalatwi trigger rozpisania na insert (BS20435)*/
        insert into eabsences (ref, ecolumn, employee, fromdate, todate, dokdate,
            scode, bcode, monthpaybase, daypayamount, fbase, lbase,
            certserial, certnumber, company, epayroll, firstabsence,
            baseabsence, calcpaybase, regtimestamp, regoperator,
            chgtimestamp, chgoperator, dimnum, dimden,
            correction, corrected, corepayroll, mflag, emplcontract)
          values (new.ref, new.ecolumn, new.employee, :afromdate, :atodate, new.dokdate,
            new.scode, new.bcode, new.monthpaybase, new.daypayamount, new.fbase, new.lbase,
            new.certserial, new.certnumber, new.company, :apayroll, new.firstabsence,
            new.baseabsence, new.calcpaybase, :aregtimestamp, :aregoperator,
            :achgtimestamp, :achgoperator, new.dimnum, new.dimden,
            new.correction, new.corrected, new.corepayroll, new.mflag, new.emplcontract);

        if (apayroll is null) then
        begin
        /*ponizsze zapytanie obowiazkowo musi sie wykonac dla przypadku, gdy
          rozszerzamy zakres dat w dol. W innym przypadku nie ma sensu wywolywac
          procedury, gdyz zwroci nam wiadoma wartosc                          */
          select firstabsence, baseabsence
            from e_get_absence_periods_bases(new.employee, new.ecolumn, :bfromdate, null, null)
            into :bfirstabsence, :bbaseabsence ;
        end else
        begin
          bfirstabsence = new.firstabsence;
          bbaseabsence = new.baseabsence;
        end

        insert into eabsences (ref, ecolumn, employee, fromdate, todate, dokdate,
            scode, bcode, monthpaybase, daypayamount, fbase, lbase,
            certserial, certnumber, company, epayroll, firstabsence,
            baseabsence, calcpaybase, regtimestamp, regoperator, chgtimestamp,
            chgoperator, dimnum, dimden, mflag, emplcontract)
          values (null, new.ecolumn, new.employee, :bfromdate, :btodate, new.dokdate,
            new.scode, new.bcode, new.monthpaybase, new.daypayamount, new.fbase, new.lbase,
            new.certserial, new.certnumber, new.company, :bpayroll, :bfirstabsence,
            :bbaseabsence, null, :bregtimestamp, :bregoperator, :bchgtimestamp,
            :bchgoperator, new.dimnum, new.dimden, new.mflag, new.emplcontract);
      end
    end
  end
end^
SET TERM ; ^
