--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAKZAL_AU FOR NAGFAKZAL                      
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable maxzaliczka numeric(14,2);
declare variable zaliczka numeric(14,2);
begin
  if(new.wartbru<>old.wartbru or new.wartbruzl<>old.wartbruzl or
     new.wartnet<>old.wartnet or new.wartnetzl<>old.wartnetzl) then
  begin
    select esumwartbruzl from nagfak where ref=new.fakturazal into :maxzaliczka;
    select sum(nagfakzal.wartbruzl) from nagfakzal
    left join nagfak on (nagfak.ref=nagfakzal.faktura)
    where nagfakzal.fakturazal=new.fakturazal and nagfak.anulowanie=0 into :zaliczka;
    if(:zaliczka>:maxzaliczka) then exception NAGFAKZAL_AKCEPTACJA 'Kwota rozliczana przekracza wartosc zaliczki o '||cast(:zaliczka-:maxzaliczka as varchar(14))||' PLN';
    if(new.vat<>old.vat or new.faktura<>old.faktura) then execute procedure NAGFAKZAL_OBL_ROZFAK(old.faktura,old.vat);
    execute procedure NAGFAKZAL_OBL_ROZFAK(new.faktura,new.vat);
  end
end^
SET TERM ; ^
