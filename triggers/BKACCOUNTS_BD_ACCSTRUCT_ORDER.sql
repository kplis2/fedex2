--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKACCOUNTS_BD_ACCSTRUCT_ORDER FOR BKACCOUNTS                     
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  update accstructure set ord = 0 where BKACCOUNT = old.ref;
end^
SET TERM ; ^
