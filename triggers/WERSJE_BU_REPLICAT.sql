--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WERSJE_BU_REPLICAT FOR WERSJE                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.ref<>old.ref
     or(new.ktm <> old.ktm)
     or (new.nrwersji <> old.nrwersji)
     or(new.nazwa <> old.nazwa ) or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null)
     or(new.opis <> old.opis ) or (new.opis is null and old.opis is not null) or (new.opis is not null and old.opis is null)
     or(new.kodkresk <> old.kodkresk ) or (new.kodkresk is null and old.kodkresk is not null) or (new.kodkresk is not null and old.kodkresk is null)
     or(new.marzamin <> old.marzamin ) or (new.marzamin is null and old.marzamin is not null) or (new.marzamin is not null and old.marzamin is null)
     or (new.cena_zakn<>old.cena_zakn)
     or (new.cena_kosztn<>old.cena_kosztn)
     or (new.cena_fabn<>old.cena_fabn)
     ) then
   waschange = 1;

  execute procedure rp_trigger_bu('WERSJE',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
