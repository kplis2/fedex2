--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDEFOPER_BI0 FOR RKDEFOPER                      
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
   if (new.pm is null or ((new.pm <> '+') and (new.pm <> '-'))) then
        exception RKDEFOPER_NOTPM;
   if (new.bank = 1) then new.btypdok = 'B';
   if (new.ob_dok is null) then new.ob_dok = 0;
   if (new.autodruk is null) then new.autodruk = 0;
   if (new.kasa = 1 and new.typ_dok is null) then new.typ_dok = 'INN';
   if (new.kasa = 0) then new.typ_dok = null;
   if (new.slownik is null) then new.slownik = 0;
   if (new.slownik <> 0) then new.nazwakontrah = null;
   if (new.ob_dok = 0) then
     new.autodruk = 0;
   if (new.slownik = 0) then begin
     new.slodef = null;
     new.slowymog = 0;
   end

end^
SET TERM ; ^
