--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMSER_BU0 FOR DOKUMSER                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable ktm varchar(40);
declare variable serjedn smallint;
declare variable dokumserack smallint;
declare variable ref integer;
declare variable wydania smallint;
declare variable akcept integer;
begin
  if(new.typ = 'M') then
    select KTM from DOKUMPOZ where REF=new.refpoz into :ktm;
  else if(new.typ = 'F') then
    select KTM from pozfak where REF=new.refpoz into :ktm;
  else if(new.typ = 'Z') then
    select KTM from pozzam where REF=new.refpoz into :ktm;
  else if (new.typ = 'R') then
    select g.ktm
      from propersraps r
        join prschedguides g on (g.ref = r.prschedguide)
      where r.ref = new.refpoz
      into :ktm;

  if (coalesce(old.numbergen,'') <> coalesce(new.numbergen,'')
    or coalesce(old.genrej,'') <> coalesce(new.genrej,'')
    or coalesce(old.gendok,'') <> coalesce(new.gendok,'')
    or coalesce(old.gendokref,0) <> coalesce(new.gendokref,0)
  ) then
  begin
    if (coalesce(old.numbergen,'') <> '') then
      execute procedure free_number(old.numbergen, old.genrej, old.gendok, current_date, old.gennumber, 0)
        returning_values :ref;
    if (coalesce(new.numbergen,'') <> '') then
      execute procedure get_number(new.numbergen, new.genrej, new.gendok, current_date, null, new.gendokref)
        returning_values new.gennumber, new.odserial;
  end

  select TOWARY.SERJEDN from TOWARY where KTM = :ktm into :serjedn;
  if(new.ilosc is null) then new.ilosc = 0;
  if(new.iloscroz is null) then new.iloscroz = 0;

  if(new.odserialnr is not null) then new.odserial = new.odserialnr;
  if(new.ilosc <> old.ilosc and new.doserialnr is not null and :serjedn = 1) then
    new.doserialnr = new.odserialnr + new.ilosc-1;
  if(new.doserialnr is null and new.odserialnr is not null and :serjedn = 1) then new.doserialnr = new.odserialnr;
  if(new.odserialnr is null and new.doserialnr is not null) then new.odserialnr = new.doserialnr;
  if(new.odserial is not null and new.doserial is null and new.ilosc = 0)
    then new.ilosc = 1;
  if(new.doserialnr is not null) then new.doserial = new.doserialnr;
  if(new.odserialnr is not null and new.doserialnr is not null) then begin
    if(new.odserialnr > new.doserialnr) then exception DOKUMSER_WRONGNR;
    new.ilosc = new.doserialnr - new.odserialnr + 1;
  end
  if(new.typ = 'M') then begin
    select DEFMAGAZ.NOTSERIAL, DEFDOKUM.wydania, DOKUMNAG.akcept
      from DOKUMPOZ
      left join DOKUMNAG on (DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
      left join DEFMAGAZ on (DEFMAGAZ.SYMBOL = DOKUMNAG.MAGAZYN)
      left join DEFDOKUM on (DEFDOKUM.SYMBOL = DOKUMNAG.TYP)
      where DOKUMPOZ.REF = new.refpoz
      into :dokumserack, :wydania, :akcept;
-- MS: nie mozna redagowac nr seryjnych na zaakceptowanych dok w trybie serializacji na magazynie
    if(:dokumserack = 0 and :akcept = 1 and
        (new.odserial <> old.odserial or new.ilosc <> old.ilosc
          or new.typ <> old.typ)) then
      exception dokumnag_akcept;
    if(:dokumserack = 1 and :wydania = 1) then begin
      /*zmiana numeru seryjnego*/
      if(old.odserial is null and new.odserial is not null or old.odserial is not null and new.odserial is null
         or old.odserial <> new.odserial) then begin
       /*zwolnienie starego numeru*/
        select max(dokumser.ref)
         from dokumser
         join dokumpoz on (dokumser.refpoz = dokumpoz.ref and dokumpoz.ktm = :ktm)
         join DOKUMNAG on (DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
         join DEFDOKUM on (DEFDOKUM.SYMBOL = DOKUMNAG.TYP and DEFDOKUM.WYDANIA = 0)
         where dokumser.odserial = old.odserial and dokumser.ref <> new.ref and dokumser.iloscroz = 1
        into :ref;
        if(:ref>0) then begin
          update dokumser set dokumser.iloscroz = 0 where dokumser.ref = :ref;
        end else begin
          /*exception DOKUMSER_WRONGNR 'Nie można zwolnić zmienianego numeru.';*/ref = 0;
        end
       /*blokada - jezeli jest - nowego numeru*/
        select max(dokumser.ref)
         from dokumser
         join dokumpoz on (dokumser.refpoz = dokumpoz.ref and dokumpoz.ktm = :ktm)
         join DOKUMNAG on (DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
         join DEFDOKUM on (DEFDOKUM.SYMBOL = DOKUMNAG.TYP and DEFDOKUM.WYDANIA = 0)
         where dokumser.odserial = new.odserial and dokumser.ref <> new.ref and dokumser.iloscroz = 0
        into :ref;
        if(:ref>0) then begin
          update dokumser set dokumser.iloscroz = 1 where dokumser.ref = :ref;
          new.iloscroz = 1;
        end else begin
           /* exception DOKUMSER_WRONGNR 'Nie można użyć wybranego numeru.';*/ref = 0;
        end
      end
    end
  end
end^
SET TERM ; ^
