--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_BD0 FOR POZZAM                         
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable cnt integer;
declare variable wydania smallint;
declare variable autoscheduled smallint;
declare variable pref integer;
begin
  select t.wydania, coalesce(n.autoscheduled,0)
    from nagzam n
      left join typzam t on (t.symbol = n.typzam)
    where n.ref = old.zamowienie
    into wydania, autoscheduled;
  select count(*) from POZZAM where POPREF = old.ref into :cnt;
  if(:cnt > 0) then
    exception POZZAM_HASPOPREF;
  select count(*) from POZZAM where GENPOZREF = old.ref into :cnt;
  if(:cnt > 0) then
    exception POZZAM_HASGENPOZREF;
  --kasowanie zleceń produkcyjnych
  if (wydania = 3) then
  begin
    -- sprawdzamy czy zlecenie jest juz zaharmonogramowane
    if (autoscheduled = 0) then
    begin
      if (exists (select first 1 1 from prschedguides g where g.pozzam = old.ref)) then
        exception prnagzam_error 'Pozycja zlecenia została dodana do harmonogramu';
    end
    update pozzam set ilosc = 0, prrecalcdepend = 1 where ref = old.ref;
    EXECUTE PROCEDURE PR_POZZAM_DEL_CASCADE(old.ref,0);
  end
  else if (wydania = 1) then
  begin
    -- sprawdzamy czy sa zalezne zlecenia produkcyjne
    if (exists(select first 1 1 from relations r where r.srefto = old.ref and r.stableto = 'POZZAM' and r.act > 0)) then
      exception prnagzam_error 'Istnieją zlecenia produkcyjne zależne';
  end
end^
SET TERM ; ^
