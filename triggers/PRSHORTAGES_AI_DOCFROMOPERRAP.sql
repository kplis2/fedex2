--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHORTAGES_AI_DOCFROMOPERRAP FOR PRSHORTAGES                    
  ACTIVE AFTER INSERT POSITION 2 
as
begin
  if (exists (select first 1 1 from prschedopers p left join prshmat m on (p.shoper = m.opermaster)
      where p.ref = new.prschedoper and m.autodocout > 0)) then
  begin 
    --generowanie dokumentow RW do raportow produkcyjnych
    execute procedure prschedguide_real(null, new.ref, 0, 1);
  end
end^
SET TERM ; ^
