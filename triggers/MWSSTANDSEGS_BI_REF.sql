--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDSEGS_BI_REF FOR MWSSTANDSEGS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSSTANDSEGS') returning_values new.ref;
end^
SET TERM ; ^
