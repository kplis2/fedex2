--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKRAPKAS_AD0 FOR RKRAPKAS                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable gennumrap varchar(40);
declare variable local smallint;
declare variable BLOCKREF integer;
begin
  execute procedure CHECK_LOCAL('RKRAPKAS',old.ref) returning_values :local;
  if(:local = 1) then begin
    select  RKSTNKAS.GENNUMRAP from RKSTNKAS where KOD=old.stanowisko into :gennumrap;
    execute procedure FREE_NUMBER(:gennumrap,old.stanowisko, '',old.dataod,old.numer,0) returning_values :blockref ;
  end
end^
SET TERM ; ^
