--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDROZ_BI_REF FOR LISTYWYSDROZ                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('LISTYWYSDROZ')
      returning_values new.REF;
end^
SET TERM ; ^
