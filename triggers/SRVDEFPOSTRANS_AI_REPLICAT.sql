--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVDEFPOSTRANS_AI_REPLICAT FOR SRVDEFPOSTRANS                 
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable contrtype integer;
begin
  select contrtype from srvdefpos where srvdefpos.ref = new.srvdefpos into :contrtype;
  update CONTRTYPES set STATE = -1  where ref = :contrtype;
end^
SET TERM ; ^
