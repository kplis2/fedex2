--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDAWCY_BI_REF FOR SPRZEDAWCY                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SPRZEDAWCY')
      returning_values new.REF;
end^
SET TERM ; ^
