--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TURNOVERSD_BD0 FOR TURNOVERSD                     
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if (old.credit <> 0 or old.debit<>0) then
    exception FK_CANT_DELETE_TURNOVERSD;
end^
SET TERM ; ^
