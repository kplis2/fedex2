--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ERECRUITS_BI_REF FOR ERECRUITS                      
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ERECRUITS')
      returning_values new.REF;
end^
SET TERM ; ^
