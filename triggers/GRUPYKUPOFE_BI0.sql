--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER GRUPYKUPOFE_BI0 FOR GRUPYKUPOFE                    
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  else if(new.ktm is not null and new.wersja is not null) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  select NAZWA from TOWARY where ktm = new.ktm into new.nazwat;
  if(new.ktm is not null and (not exists (select first 1 1 from towjedn where ktm = new.ktm and ref = new.jedn))) then
    select ref from TOWJEDN where ktm = new.ktm and glowna = 1 into new.jedn;
end^
SET TERM ; ^
