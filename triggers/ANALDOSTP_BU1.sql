--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ANALDOSTP_BU1 FOR ANALDOSTP                      
  ACTIVE BEFORE UPDATE POSITION 1 
as
declare variable wzrost numeric(14,2);
declare variable dnibad integer;
declare variable dzieldni integer;
declare variable dnisprz integer;
begin
 if((new.sprzedaz <> old.sprzedaz) or (new.reczny = 4)) then begin
   select WZROST,  DODATY - ODDATY, DATAKON - DATAPOCZ, STATYSTYKA  from ANALDOST where REF=new.ANALIZA
         into :wzrost, :dnisprz, :dnibad, :dzieldni;
   if(:wzrost is null) then wzrost = 0;
   if(:dzieldni is null) then dzieldni = 0;
   if(:dzieldni = 0) then dzieldni = 1;
   else if(:dzieldni = 1) then dzieldni = 7;
   else if(:dzieldni = 2) then dzieldni = 31;
   else dzieldni = 365;
   if(:dnibad is null or (:dnibad <=0)) then exception
            ANALDOSTP_CH_WR_DNIBAD;
   new.srednia = new.sprzedaz * :dzieldni  / :dnibad;
   if(new.reczny <> 3) then begin
    new.sredniaw = new.srednia *(100 + :wzrost)/100;
    new.potrzeba = new.sredniaw * :dnisprz/:dzieldni;
   end

 end else if (new.sredniaw <> old.sredniaw or (new.potrzeba <> old.potrzeba)) then
  if(new.sredniaw <> old.sredniaw and new.potrzeba = old.potrzeba) then begin
    select DODATY-ODDATY,DATAKON - DATAPOCZ  from ANALDOST where REF=new.ANALIZA into :dnisprz,:dzieldni;
    if(:dnisprz is null) then dnisprz = 0;
   if(:dzieldni is null) then dzieldni = 0;
   if(:dzieldni = 0) then dzieldni = 1;
   else if(:dzieldni = 1) then dzieldni = 7;
   else if(:dzieldni = 2) then dzieldni = 31;
   else dzieldni = 365;
    new.potrzeba = new.sredniaw * :dnisprz/:dzieldni;
  end
    new.reczny = 1;
end^
SET TERM ; ^
