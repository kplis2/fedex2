--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAM_AD_OBL FOR POZZAM                         
  ACTIVE AFTER DELETE POSITION 1 
as
begin
  if(old.genpozref > 0) then
    if((old.ilosc>0) or (old.ilzreal >0)) then
        execute procedure POZZAM_OBL(old.genpozref);
  if(old.popref > 0) then
    if((old.ilzreal > 0) or (old.ilzdysp > 0) or (old.ilzadysp > 0)) then
        execute procedure POZZAM_OBL(old.popref);
  if (old.prschedguidepos is not null) then
    execute procedure prschedguidedet_change(1,'POZZAM',old.ref,old.prschedguidepos);
end^
SET TERM ; ^
