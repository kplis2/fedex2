--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPHONES_BI0 FOR CPHONES                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CPHONES')
      returning_values new.REF;
end^
SET TERM ; ^
