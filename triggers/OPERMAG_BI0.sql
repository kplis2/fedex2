--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OPERMAG_BI0 FOR OPERMAG                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if(new.dokumtypesnewright in ('',';')) then new.dokumtypesnewright = null;
  if(new.dokumtypesacceptright in ('',';')) then new.dokumtypesacceptright = null;
  if(new.dokumtypeschangeright in ('',';')) then new.dokumtypeschangeright = null;
  if(new.dokumtypesacceptbackright in ('',';')) then new.dokumtypesacceptbackright = null;
  if (new.cantakemwsords is null) then new.cantakemwsords = 0;
  if (new.verifymwsconstloc is null) then new.verifymwsconstloc = 0;
  begin
    execute procedure mwsordtypedest4op_grants(coalesce(new.canchangemwsconstlocl,0), coalesce(new.supervisor,0))
      returning_values new.grants;
  end
  if (new.deldokmagfromzam is null) then
    new.deldokmagfromzam = 0;
end^
SET TERM ; ^
