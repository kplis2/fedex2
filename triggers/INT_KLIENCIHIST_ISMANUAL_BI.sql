--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INT_KLIENCIHIST_ISMANUAL_BI FOR INT_KLIENCIHIST                
  ACTIVE BEFORE INSERT POSITION 5 
as
begin
  if(new.is_manual is null) then
    new.is_manual = 0;

  if(new.is_manual not in(0,1,2)) then
    exception universal 'Niedozwolona wartosc IS_MANUAL';
end^
SET TERM ; ^
