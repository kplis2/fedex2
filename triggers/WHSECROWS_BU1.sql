--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHSECROWS_BU1 FOR WHSECROWS                      
  ACTIVE BEFORE UPDATE POSITION 1 
AS
begin
  if (new.w is null) then new.w = 0;
    execute procedure mws_whsecrowsdist_calculate(new.whsec,new.w,new.number)
      returning_values (new.coordxb, new.coordxe);
end^
SET TERM ; ^
