--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_AI_EMPLCALDAYS FOR EABSENCES                      
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  update emplcaldays set absence = new.ref, absencediff = null
    where employee = new.employee
    and cdate between new.fromdate and new.todate;
end^
SET TERM ; ^
