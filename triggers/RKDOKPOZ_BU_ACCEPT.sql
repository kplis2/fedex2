--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_BU_ACCEPT FOR RKDOKPOZ                       
  ACTIVE BEFORE UPDATE POSITION 9 
AS
declare variable walutowy int;
declare variable waluta_stan varchar(3);
declare variable country_currency varchar(3);
declare variable stanowisko char(2);
declare variable techoper smallint_id;
begin
  new.ACCEPT=COALESCE(new.ACCEPT, 0);
  if (coalesce(old.accept,0) <> new.accept or coalesce(old.anulowany,0) <> coalesce(new.anulowany,0)) then
  begin
    if (new.accept = 1 and new.anulowany = 0) then
    begin
      select r.waluta
        from RKDOKNAG d
        join RKSTNKAS r on (r.kod = d.stanowisko)
        where d.ref = new.dokument
        into :waluta_stan;
      execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
        returning_values country_currency;

      select n.walutowy
        from RKDOKNAG N
        where N.REF = new.DOKUMENT
        into :walutowy;
      if (walutowy=0 or :country_currency = :waluta_stan) then
      begin
        new.kwotazlfifo = new.kwotazl;
        exit;
      end
    end

    --<BS59416 - jezeli operacja techniczna, to nie generujemy rkdokroz
    select o.techoper
      from rkdoknag d
        join rkdefoper o  on (d.operacja = o.symbol)
      where d.ref = new.dokument
    into :techoper;
    if(techoper = 1) then
    begin
      new.kwotazlfifo = new.kwotazl;
      exit;
    end --BS59416>

    execute procedure rkdokpoz_ack(new.ref, old.accept, new.accept, old.anulowany, new.anulowany);

    select sum(cast(r.kwota * s.kurs as numeric(14,2)))
      from rkdokroz r
        join rkstanwal s on (r.rkstanwal = s.ref)
      where r.rkdokpoz = new.ref
      into new.kwotazlfifo;
    new.kwotazlfifo = iif (new.pm = '-', -1,1) * coalesce(new.kwotazlfifo,0);

  end
end^
SET TERM ; ^
