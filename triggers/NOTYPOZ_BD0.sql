--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTYPOZ_BD0 FOR NOTYPOZ                        
  ACTIVE BEFORE DELETE POSITION 0 
AS
  declare variable docstatus integer;
begin
  select status from notynag where ref = old.dokument into :docstatus;
  if (docstatus > 0) then exception NOTYNAG_ACCEPTED;
end^
SET TERM ; ^
