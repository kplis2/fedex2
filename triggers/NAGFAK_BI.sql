--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_BI FOR NAGFAK                         
  ACTIVE BEFORE INSERT POSITION 1 
AS
declare variable NUMBERG varchar(40);
declare variable lokres varchar(6);
declare variable symbolpoakcept smallint;
declare variable local smallint;
declare variable dataokres timestamp;
declare variable cnt integer;
declare variable kor integer;
declare variable autozmiana smallint;
declare variable bankdomyslny varchar(64);
declare variable tabkursuzywaj smallint;
declare variable typkurs smallint;
declare variable sredni numeric(14,4);
declare variable zakupu numeric(14,4);
declare variable magazyn varchar(5);
declare variable oddzial varchar(30);
declare variable aktuoddzial varchar(30);
declare variable sprzedazy numeric(14,4);
declare variable location integer;
declare variable aktulocation integer;
declare variable sposoblrozrach integer;
declare variable zak integer;
declare variable gln varchar(255);
declare variable dozapnetto smallint;
declare variable termdost timestamp;
declare variable vptransdate smallint;
declare variable va smallint;
declare variable oo smallint;
BEGIN
  execute procedure CHECK_LOCAL('NAGFAK',new.ref) returning_values :local;
  if(new.operator = 0) then new.operator = NULL;
  if(new.sprzedawca = 0) then new.sprzedawca = NULL;
  if(new.sprzedawcazew = 0) then new.sprzedawcazew = NULL;
  if(new.sprzedrozl is null) then new.sprzedrozl = 0;
  if(new.sumwartbru is null) then new.sumwartbru = 0;
  if(new.psumwartbru is null) then new.psumwartbru = 0;
  if(new.sumwartnet is null) then new.sumwartnet = 0;
  if(new.psumwartnet is null) then new.psumwartnet = 0;
  if(new.sumkaucja is null) then new.sumkaucja = 0;
  if(new.psumkaucja is null) then new.psumkaucja = 0;
  if(new.waga is null) then new.waga = 0;
  if(new.objetosc is null) then new.objetosc = 0;
  if(new.kosztdost is null) then new.kosztdost = 0;
  if(new.zaplacono is null) then new.zaplacono = 0;
  if(new.koszt is null) then new.koszt = 0;
  if(new.pkoszt is null) then new.pkoszt = 0;
  if(new.kosztkat is null) then new.kosztkat = 0;
  if(new.kosztanal is null) then new.kosztanal = 0;
  if(new.anulowanie is null) then new.anulowanie = 0;
  if(new.akceptacja is null) then new.akceptacja = 0;
  if(new.magrozlicz is null) then new.magrozlicz = 0;
  if(new.rozrachrozval is null) then new.rozrachrozval = 0;
  if(new.proglojdone is null) then new.proglojdone = 0;
  select zakup, sad, eksport, fakturarr, zaliczkowy, korekta, sposoblrozrach, dozaplatynetto, drukbezvat
    from TYPFAK where SYMBOL = new.typ
    into new.zakup, new.sad, new.eksport, new.fakturarr, new.zaliczkowy, :kor, :sposoblrozrach, :dozapnetto, new.drukbezvat;
  if(:dozapnetto is null) then dozapnetto = 0;
  if(new.zaliczkowy is null) then new.zaliczkowy = 0;
  if(new.rabat is null) then new.rabat = 0;
  if(new.magrozlicz > 0) then
    new.kosztanal = new.koszt;
  else
    new.kosztanal = new.kosztkat;
  select oo from typfak where symbol = new.typ into :oo;
  if(oo = 1) then
  begin
    if(coalesce(new.klient,0) <> 0) then
      select vatactive from klienci where ref = new.klient into :va;
    if(:va = 0) then
      exception UNIVERSAL'Kontrahent nie jest płatnikiem VAT. Wystawienie faktury OO jest niemożliwe';
  end
  if(:local = 1) then begin
    select nieobrot from TYPFAK where SYMBOL = new.typ into new.nieobrot;
    if(new.nieobrot=1 and new.zaliczkowy=0) then new.nieobrot = 3;
    if(new.nieobrot=1 and new.zaliczkowy>0) then new.nieobrot = 2;
    if(:kor = 1 and new.zakup = 0) then begin
      select ODDZIAL from DEFMAGAZ where SYMBOL=new.magazyn into :oddzial;
      select LOCATION from ODDZIALY where ODDZIAL=:Oddzial into :location;
      execute procedure GETCONFIG('AKTULOCAT') returning_values :aktulocation;
      if(:aktulocation <> :location) then begin
        execute procedure GETCONFIG('AKTUODDZIAL') returning_values :aktuoddzial;
        select oddzialy.magazyn from ODDZIALY where ODDZIAL=:aktuoddzial into :magazyn;
        if(:magazyn is not null and magazyn <> '' and :magazyn <> '   ') then
         new.magazyn = :magazyn;
      end
    end
  end
  if(new.zakup is null) then new.zakup = 0;
  if(new.nieobrot is null) then new.nieobrot = 0;
  if(new.data is null) then new.data = current_date;
  if(new.datasprz is null) then new.datasprz = new.data;
  if(new.grupadok is null or (new.grupadok = 0)) then new.grupadok = new.ref;
  if((new.kurs is null) or (new.kurs = 0)) then new.kurs = 1;
  new.vkurs = new.kurs;
  if((new.oddzial is null) or (new.oddzial = '')) then  begin
    execute procedure GETCONFIG('AKTUODDZIAL') returning_values new.oddzial;
  end
  if(new.company is null) then select company from oddzialy where oddzial = new.oddzial into new.company;
  if(new.skad is null) then new.skad = 0;
  if(new.symbfak is null) then new.symbfak = '';
  if(new.kwotazal is null) then new.kwotazal = 0;
  if(new.bn is null or (new.bn <>'N' and new.bn <> 'B'))then
   select BN from TYPFAK where SYMBOL = new.typ into new.bn;

  --naliczanie pol ESUM...

  if(new.zaliczkowy>0) then begin
    new.esumwartbru = new.pesumwartbru + new.kwotazal;
    new.esumwartbruzl = new.esumwartbru*new.kurs;
    new.esumwartnet = 0;
    new.esumwartnetzl = 0;
  end else begin
    new.esumwartbru = new.sumwartbru;
    new.esumwartnet = new.sumwartnet;
    new.esumwartbruzl = new.sumwartbruzl;
    new.esumwartnetzl = new.sumwartnetzl;
  end

  if(:sposoblrozrach <> 0) then begin
    if (:dozapnetto = 0) then
      new.dozaplaty = (new.esumwartbru - new.pesumwartbru) + (new.sumkaucja - new.psumkaucja) - new.zaplacono;
    else
      new.dozaplaty = (new.esumwartnet - new.pesumwartnet) + (new.sumkaucja - new.psumkaucja) - new.zaplacono;
  end else begin
    if (:dozapnetto = 0) then
      new.dozaplaty = (new.sumwartbru - new.psumwartbru) + (new.sumkaucja - new.psumkaucja) - new.zaplacono;
    else
      new.dozaplaty = (new.sumwartnet - new.psumwartnet) + (new.sumkaucja - new.psumkaucja) - new.zaplacono;
  end
  if((new.nieobrot = 1) or (new.nieobrot = 3)) then new.dozaplaty = 0;
  if(new.dataotrz is null) then new.dataotrz = new.datasprz;
  if(new.zakup = 0) then begin
    dataokres = new.data;
  end else begin
    vptransdate = null;
    select first 1 ptransdate --sprawdzam czy istnieje schemat dekretacji tego nagfaka
      from doctobkdocs
      where param1 = new.rejestr
        and param2 = new.typ
        and company = new.company
        and typ = 0
      into :vptransdate;
    if (vptransdate=0) then --jezeli istnieje sprawdzam wedlug jakiego okresu ma byc ksiegowane
      dataokres = new.data; --data sprzedazy
    else
      dataokres = new.dataotrz; -- data otrzymania
  end
  lokres = cast( extract( year from :dataokres) as char(4));
  if(extract(month from :dataokres) < 10) then
       lokres = :lokres ||'0' ||cast(extract(month from :dataokres) as char(1));
  else begin
       lokres = :lokres ||cast(extract(month from :dataokres) as char(2));
  end

  --kontrola uprawnień dokumentów do stanowisk
  if(new.stanowisko <> '') then begin
    if (not exists(select first 1 1
      from stantypfak
      where stantypfak.stanfaktyczny = new.stanowisko and stantypfak.typfak = new.typ)) then
        exception NAGFAK_TYP_STANSPRZED;
  end

  if(new.stanowisko<>'' and new.rejestr<>'') then begin
    cnt = null;
    select count(*) from stanrej where stansprzed=new.stanowisko and rejfak=new.rejestr into :cnt;
    if(:cnt=0) then exception nagfak_wrong_rejestr;
    select ZAKUP from REJFAK where SYMBOL = new.REJESTR into :zak;
    if(:zak <> new.zakup) then exception STANTYPDAFAK_ZAKNIESG;
  end
  if((new.okres is null) or (new.okres = '      ')) then
      new.okres = :lokres;
    else if(new.okres <> :lokres) then exception NAGFAK_DATA_POZA_OKRES;
  if((new.numer is null or (new.numer = 0)) and
     (new.symbol is null or (new.symbol = ''))
  ) then begin
    select SYMBOLPOAKCEPT from STANTYPFAK where STANTYPFAK.stanfaktyczny = new.stanowisko and STANTYPFAK.typfak = new.typ into :symbolpoakcept;
    if(:symbolpoakcept is null) then symbolpoakcept = 0;
    if(:symbolpoakcept = 0) then begin
     select number_gen from rejfak where new.rejestr = rejfak.symbol into :numberg;
     if(new.zakup = 1) then
       execute procedure get_number(:numberg,new.rejestr,new.typ,new.dataotrz, 0, new.ref) returning_values new.numer, new.symbol;
     else
       execute procedure get_number(:numberg,new.rejestr,new.typ,new.data, 0, new.ref) returning_values new.numer, new.symbol;
     new.oldsymbol = new.numer || ';' || new.symbol;
    end else begin
      new.numer = -1;
      new.symbol = 'TYM/'||cast(new.ref as varchar(20));
    end
    if(new.fakturarr=1) then new.symbfak = new.symbol;
  end
  if(new.waluta = '') then new.waluta = null;
  if(new.waluta is null) then begin
    execute procedure GETCONFIG('WALPLN') returning_values new.waluta;
    if(new.waluta is null) then new.waluta = 'PLN';
  end
  if(new.termin is null) then new.termin = 0;
  if(new.termzap is null) then new.termzap  = new.data + new.termin;
  if(new.psumwartbru is null) then new.psumwartbru = 0;
  if(new.psumwartbruzl is null) then new.psumwartbruzl = 0;
  if(new.psumwartnet is null) then new.psumwartnet = 0;
  if(new.psumwartnetzl is null) then new.psumwartnetzl = 0;
  if(new.sumwartbru is null) then new.sumwartbru = 0;
  if(new.sumwartbruzl is null) then new.sumwartbruzl = 0;
  if(new.sumwartnet is null) then new.sumwartnet = 0;
  if(new.sumwartnetzl is null) then new.sumwartnetzl = 0;

  if(new.pesumwartnet is null) then new.pesumwartnet = 0;
  if(new.pesumwartbru is null) then new.pesumwartbru = 0;
  if(new.pesumwartnetzl is null) then new.pesumwartnetzl = 0;
  if(new.pesumwartbruzl is null) then new.pesumwartbruzl = 0;

  if(new.ewartbru is null) then new.ewartbru = 0;
  if(new.ewartnet is null) then new.ewartnet = 0;
  if(new.ewartbruzl is null) then new.ewartbruzl = 0;
  if(new.ewartnetzl is null) then new.ewartnetzl = 0;
  new.ewartbru = new.esumwartbru - new.pesumwartbru;
  new.ewartnet = new.esumwartnet - new.pesumwartnet;
  new.ewartbruzl = new.esumwartbruzl - new.pesumwartbruzl;
  new.ewartnetzl = new.esumwartnetzl - new.pesumwartnetzl;

  if(new.wartbru is null) then new.wartbru = 0;
  if(new.wartnet is null) then new.wartnet = 0;
  if(new.wartbruzl is null) then new.wartbruzl = 0;
  if(new.wartnetzl is null) then new.wartnetzl = 0;
  new.wartbru = new.sumwartbru - new.psumwartbru;
  new.wartnet = new.sumwartnet - new.psumwartnet;
  new.wartbruzl = new.sumwartbruzl - new.psumwartbruzl;
  new.wartnetzl = new.sumwartnetzl - new.psumwartnetzl;

  if(new.kurs is null) then new.kurs = 1;
  if(new.pkurs is null) then new.pkurs = 1;
  if(new.zafisk is null) then new.zafisk = 0;
  if(new.platnik is null or (new.platnik = 0))then
  begin
    select p.platnik from platnicy p
      where p.klient = new.klient and p.domyslny = 1
      into new.platnik;
    if (new.platnik is null or new.platnik = 0) then
      new.platnik = new.klient;
  end
  if(new.tabkurs = 0) then new.tabkurs = null;
  if(new.sposzap is not null) then select FAKTORING from PLATNOSCI where REF=new.sposzap into new.faktoring;
  new.blokada = 0;
  new.wasdeakcept = 0;
  if(new.akceptacja <> 7 and (new.akceptacja <> 8)) then new.akceptacja = 0;
  if(new.wysylka = 0) then new.wysylka = null;
  if(new.trasadost = 0) then new.trasadost = null;
  if(new.wysylka > 0) then begin

    --tu tylko mogl operator wskazac wysylki niezamknite - lub replikacja

    select listywys.trasa, listywys.dataplwys from LISTYWYS where ref=new.wysylka and STAN = 'O' into new.trasadost, new.termdost;
  end
  if(new.dulica is null or (new.dulica is null)) then begin
    if(new.klient > 0) then
      select KLIENCI.dulica from KLIENCI where ref=new.klient into new.dulica;
    else if (new.dostawca > 0) then
      select dostawcy.ulica from DOSTAWCY where ref=new.dostawca into new.dulica;
  end
  if(new.dmiasto is null or (new.dmiasto is null)) then begin
    if(new.klient > 0) then
      select KLIENCI.dmiasto from KLIENCI where ref=new.klient into new.dmiasto;
    else if (new.dostawca > 0) then
      select dostawcy.miasto from DOSTAWCY where ref=new.dostawca into new.dmiasto;
  end
  if(new.slobankacc > 0) then begin
    cnt = null;
    select count(*) from bankaccounts where DICTDEF = 6 and DICTPOS = new.dostawca and ref=new.slobankacc and act=1 into :cnt;
    if(:cnt is null or (:cnt = 0))then
      new.slobankacc = null;
  end

    --obliczenie kosztu transportu dla dokumentu

  if ((new.sposdost is not null) and (new.akceptacja=0) and (new.zakup=0) and (:kor=0)) then begin
    if(new.termdost is null) then termdost = new.data; else termdost = new.termdost;
    select koszt, optyspos, kods, iloscpal, ilosckart from GET_KOSZTDOST(new.sposdost,new.sposzap,new.waga,new.objetosc,new.sumwartnetzl-new.psumwartnetzl,new.sumwartbruzl-new.psumwartbruzl,new.dkodp,new.flagisped,new.sposdostauto,NULL,:termdost,null, new.iloscpalet, new.iloscpaczek, '')
      into new.kosztdost, new.sposdost, new.kodsped, new.iloscpalet, new.iloscpaczek;
    if(new.sposdost = 0) then new.sposdost = null;
  end
  if (new.walutowa = 1) then begin
    tabkursuzywaj = 0; typkurs = -1;
    select bankdomyslny, tabkursuzywaj, typkurs from stansprzed s where s.stanowisko = new.stanowisko
     into: bankdomyslny, :tabkursuzywaj, :typkurs;
    if(tabkursuzywaj = 1) then begin
      if(:typkurs<0) then exception KURSTYP_BRAK;
      sredni = 0; zakupu = 0; sprzedazy = 0; cnt = null;
      select first 1 t.ref, tp.sredni, tp.zakupu, tp.sprzedazy from tabkurs t join tabkursp tp on (t.ref = tp.tabkurs)
      where t.akt=1 and t.bank = :bankdomyslny and tp.waluta = new.waluta
      order by t.data desc
      into :cnt, :sredni, :zakupu, :sprzedazy;
--      if(:typkurs = 0 and :sredni = 0) then exception KURS_BRAKWARTOSCI;
--      else if(:typkurs = 1 and :zakupu = 0) then exception KURS_BRAKWARTOSCI;
--      else if(:typkurs = 2 and :sprzedazy = 0) then exception KURS_BRAKWARTOSCI;
      if(:typkurs = 0)then new.kurs = :sredni;
      else if(:typkurs = 1)then new.kurs = :zakupu;
      else if(:typkurs = 2)then new.kurs = :sprzedazy;
      if(new.kurs=0) then new.kurs = 1;
      new.tabkurs = :cnt;
    end
  end
--domyslnie posiadanie oryginalu dokumentu
  new.oryginal = 0;
  if(new.krajwysylki = '') then new.krajwysylki = null;

END^
SET TERM ; ^
