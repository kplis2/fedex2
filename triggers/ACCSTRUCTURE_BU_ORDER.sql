--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTURE_BU_ORDER FOR ACCSTRUCTURE                   
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable maxnum integer;
begin
  if (new.ord is null) then new.ord = 1;
  if (new.nlevel is null) then new.nlevel = old.nlevel;
  if (new.ord = 0 or new.nlevel = old.nlevel) then
  begin
    new.ord = 1;
    exit;
  end
  --nlevelowanie obszarow w rzedzie gdy nie podamy nlevelu
  if (new.nlevel <> old.nlevel) then
    select max(nlevel) from accstructure where bkaccount = new.bkaccount
      into :maxnum;
  else
    maxnum = 0;
  if (new.nlevel < old.nlevel) then
  begin
    update accstructure set ord = 0, nlevel = nlevel + 1
      where ref <> new.ref and nlevel >= new.nlevel
        and nlevel < old.nlevel and bkaccount = new.bkaccount;
  end else if (new.nlevel > old.nlevel and new.nlevel <= maxnum) then
  begin
    update accstructure set ord = 0, nlevel = nlevel - 1
      where ref <> new.ref and nlevel <= new.nlevel
        and nlevel > old.nlevel and bkaccount = new.bkaccount;
  end else if (new.nlevel > old.nlevel and new.nlevel > maxnum) then
    new.nlevel = old.nlevel;
end^
SET TERM ; ^
