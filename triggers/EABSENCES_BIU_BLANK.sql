--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_BIU_BLANK FOR EABSENCES                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
begin
/*Uwaga: nie nalezy zmieniac pozycji trigerra, bez uzgodnienia dzialania
         z triggerami rozpisujacymi pod katem ustawienia ciaglosci*/

  new.npdays = coalesce(new.npdays, 0);
  new.correction = coalesce(new.correction, 0);
  if (new.corrected = 0) then new.corrected = null; --BS40975

  select company
    from employees
    where ref = new.employee
    into new.company;

--BS20435:
  new.calcpaybase = coalesce(new.calcpaybase, 0);
  if (new.calcpaybase = 1 or coalesce(new.baseabsence, 0) = 0) then
    new.baseabsence = new.ref;
  if (coalesce(new.firstabsence, 0) = 0) then
    new.firstabsence = new.ref;
  if (new.calcpaybase = 1 and new.firstabsence = new.baseabsence and new.firstabsence > 0) then
    new.calcpaybase = 0;
  if (updating and (new.fromdate <> old.fromdate or new.todate <> old.todate)) then
    new.mflag = 1;  --nalezy ustawic przy warunku takim samym jak na rozpisaniu

--ustawienie infomracji kto i kiedy rejestrowal nieobecnosc
  if (inserting and new.regtimestamp is null) then
  begin
    execute procedure get_global_param ('AKTUOPERATOR')
      returning_values new.regoperator;
    new.regtimestamp = current_timestamp(0);
  end

--PR26529:
  if (coalesce(new.dimnum, 0) = 0) then new.dimnum = 1;
  if (coalesce(new.dimden, 0) = 0) then new.dimden = 1;
  new.workdim = (1.0000 * new.dimnum) / new.dimden;
end^
SET TERM ; ^
