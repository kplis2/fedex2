--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_AI100 FOR DOKUMNAG                       
  ACTIVE AFTER INSERT POSITION 100 
AS
begin
    if (new.typ = 'PZ' and new.dostawca is not null) then
    begin
        update dostawcy d set d.x_popularity = (coalesce(d.x_popularity,0)+1)  where new.dostawca = d.ref;
    end
end^
SET TERM ; ^
