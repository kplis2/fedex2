--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWORKCATPOS_BI_REF FOR EWORKCATPOS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EWORKCATPOS')
      returning_values new.REF;
end^
SET TERM ; ^
