--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER AMCHNGMETH_BI_CLOSED FOR AMCHNGMETH                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable period_status integer;
  declare variable company integer;
begin
  select company
    from fxdassets f
    where f.ref = new.fxdasset
  into :company;

  select status from amperiods where
  ref = new.amperiod --BS97517
  and company = :company
  into :period_status;
  if (period_status > 0) then exception AMPERIOD_CLOSED 'Nie można zmienić metody dla zamkniętego okresu!';
end^
SET TERM ; ^
