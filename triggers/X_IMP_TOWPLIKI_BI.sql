--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_IMP_TOWPLIKI_BI FOR X_IMP_TOWPLIKI                 
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_x_imp_towpliki_id,1);
  if(new.ilprob is null) then
    new.ilprob = 0;
end^
SET TERM ; ^
