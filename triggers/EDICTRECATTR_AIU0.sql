--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDICTRECATTR_AIU0 FOR EDICTRECATTR                   
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
AS
declare variable RecCand_ref integer;
begin
  --przeliczam odpowiednie oceny
  for
    select CANDIDATE from ecandidattr
      where ATTRIBUTE = new.ref
      into :RecCand_ref
  do
    execute procedure eRecCandCompRating(RecCand_ref);
end^
SET TERM ; ^
