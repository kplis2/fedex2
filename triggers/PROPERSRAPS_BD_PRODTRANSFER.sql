--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROPERSRAPS_BD_PRODTRANSFER FOR PROPERSRAPS                    
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  -- wycofanie przekazania wyrobu gotowego jako surowiec na inny przewodnik
  if (exists(select first 1 1 from prschedguides where ref = old.prschedguide and prodtransfer = 1)) then
    execute procedure prod_transfer(old.ref,2);
end^
SET TERM ; ^
