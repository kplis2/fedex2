--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ODDZIALY_BI1 FOR ODDZIALY                       
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable cnt integer;
begin
  select count(*) from ODDZIALY where COMPANY = new.company into :cnt;
  if(:cnt is null) then cnt = 0;
  if(:cnt = 0) then new.glowny = 1;
end^
SET TERM ; ^
