--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CCHARAKTERKON_BI_REF FOR CCHARAKTERKON                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('CCHARAKTERKON') returning_values new.ref;
end^
SET TERM ; ^
