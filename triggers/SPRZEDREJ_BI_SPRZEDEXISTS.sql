--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDREJ_BI_SPRZEDEXISTS FOR SPRZEDREJ                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (exists (select S.sprzedawca from sprzedrej S where S.rejestr = new.rejestr and S.klient = new.klient)) then
    exception SPRZED_EXISTST 'W wybranym rejestrze jest już zdefiniowany sprzedawca.';
  if (exists (select S.sprzedawca from sprzedrej S where S.magazyn = new.magazyn and S.klient = new.klient)) then
    exception SPRZED_EXISTST 'W wybranym magazynie jest już zdefiniowany sprzedawca.';
end^
SET TERM ; ^
