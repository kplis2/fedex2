--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLKONTA_BI0 FOR CPLKONTA                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.mallow is null) then new.mallow = 0;
  if(new.sumpktp>=new.sumpktm) then begin
    new.salpktp = new.sumpktp-new.sumpktm;
    new.salpktm = 0;
  end else begin
    new.salpktm = new.sumpktm-new.sumpktp;
    new.salpktp = 0;
  end
  if(new.mallow = 0 and new.salpktm>0) then exception CPLKONTA_UJEMNE;
end^
SET TERM ; ^
