--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPLANCHANGES_BI_RIGHTS FOR PMPLANCHANGES                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  select READRIGHTS,WRITERIGHTS,READRIGHTSGROUP,WRITERIGHTSGROUP
  from PMPLANS where REF=new.pmplan
  into new.readrights,new.writerights,new.readrightsgroup,new.writerightsgroup;
end^
SET TERM ; ^
