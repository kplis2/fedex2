--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLOYEES_CHECKACCOUNTING FOR EMPLOYEES                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable dictdef integer;
begin
  if (new.symbol <> old.symbol) then
  begin
    select ref from slodef where typ = 'EMPLOYEES'
      into :dictdef;
    execute procedure check_dict_using_in_accounts(dictdef, old.symbol, old.company);
  end
end^
SET TERM ; ^
