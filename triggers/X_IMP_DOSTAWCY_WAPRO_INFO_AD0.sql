--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_IMP_DOSTAWCY_WAPRO_INFO_AD0 FOR DOSTAWCY                       
  ACTIVE AFTER DELETE POSITION 100 
AS

begin
if (coalesce(old.int_id,'')<>'') then   -- jeli dokument pochodzi z importu
  execute procedure x_imp_pozycjeusuwane_wapro(6,'DOSTAWCY', old.int_id,old.ref);
 
end^
SET TERM ; ^
