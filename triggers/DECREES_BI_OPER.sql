--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_BI_OPER FOR DECREES                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  execute procedure get_global_param('AKTUOPERATOR') returning_values new.regoper;
  new.regdate = current_timestamp(0);
  new.changeoper = new.regoper;
  new.changedate = new.regdate;
end^
SET TERM ; ^
