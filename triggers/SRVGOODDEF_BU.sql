--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVGOODDEF_BU FOR SRVGOODDEF                     
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if((new.fieldtype <> old.fieldtype or new.symbol <> old.symbol) and new.fieldtype in (0,1) and new.symbol not like 'N%') then exception universal 'Wybierz jedno z pól "N"';
  if((new.fieldtype <> old.fieldtype or new.symbol <> old.symbol) and  new.fieldtype in (2,5) and new.symbol not like 'S%') then exception universal 'Wybierz jedno z pól "S"';
  if((new.fieldtype <> old.fieldtype or new.symbol <> old.symbol) and  new.fieldtype in (3,4) and new.symbol not like 'D%') then exception universal 'Wybierz jedno z pól "D"';
end^
SET TERM ; ^
