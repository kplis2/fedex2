--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MIARA_BI_REPLICAT FOR MIARA                          
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
 execute procedure rp_trigger_bi('MIARA',new.miara, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
