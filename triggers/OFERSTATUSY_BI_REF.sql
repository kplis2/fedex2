--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OFERSTATUSY_BI_REF FOR OFERSTATUSY                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('OFERSTATUSY')
      returning_values new.REF;
end^
SET TERM ; ^
