--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DIMDEF_BUI FOR DIMDEF                         
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.act is null) then new.act = 0;
  if (new.istime is null) then new.istime = 0;
  if (new.shortname is null or new.shortname = '') then
    exception DIMDEF_SHORTNAME 'Należy wypełnić pole Nazwa skrócona';
  if (new.name is null or new.name = '') then
    exception DIMDEF_NAME 'Należy wypełnić pole Nazwa';
end^
SET TERM ; ^
