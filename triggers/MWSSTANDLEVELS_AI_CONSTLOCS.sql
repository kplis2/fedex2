--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDLEVELS_AI_CONSTLOCS FOR MWSSTANDLEVELS                 
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable act smallint;
begin
  select act from mwsstandleveltypes where ref = new.mwsstandleveltype into act;
  if (act is null) then act = 0;
  if (act > -1) then
  begin
    -- nalezy zalozyc lokacje stale na regalach (na kazdym poziomie regalow)
    insert into mwsconstlocs (wh, whsec, whsecdict, whsecrow, whsecrowdict, coordxb, coordxe,
        coordyb, coordye, mwsstandlevelnumber, mwsstandseg, mwsstandsegtypedict, mwsstandnumber,
        mwsstand, l, w, locnumber, volume, h, levelheight,
        coordzb, coordze, whareanumber, mwsconstloctype, maxpallocq,
        wharea, mwsstandlevel, mwsstandsegtype, act, loadcapacity, goodsav, x_row_type) --- XXX KBI domyslny goodsav i x_row_type
      select wh, whsec, whsecdict, whsecrow, whsecrowdict,coordxb, coordxe,
          coordyb, coordye, new.number, mwsstandseg, mwsstandsegtypedict, mwsstandnumber,
          mwsstand, l, w, locnumber, new.volume, new.h, new.levelheight,
          new.levelheight, new.levelheight + new.h, number, new.mwsconstloctype, new.maxpallocq,
          ref, new.ref, mwsstandsegtype, :act, new.loadcapacity, new.x_default_goodsav, x_row_type    -- XXX KBI domyslny goodsav  i x_row_type
        from whareas w
        where mwsstand = new.mwsstand and (areatype = 1 or areatype = 3) ;
  end
end^
SET TERM ; ^
