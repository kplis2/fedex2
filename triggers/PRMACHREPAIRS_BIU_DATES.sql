--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRMACHREPAIRS_BIU_DATES FOR PRMACHREPAIRS                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if(new.REPAIRFROM<>old.REPAIRFROM or new.REPAIRTO<>old.REPAIRTO or (old.REPAIRFROM is null and old.REPAIRTO is null)) then begin
    if(new.REPAIRFROM is not null and new.REPAIRTO is not null and new.REPAIRFROM>new.REPAIRTO) then begin
      exception PRMACHREPAIRS_EXPT 'Data zakończenia nie może być wcześniejsza od daty rozpoczcia.';
    end
  end
  if(new.INACTIONFROM<>old.INACTIONFROM or new.INACTIONTO<>old.INACTIONTO or (old.INACTIONFROM is null and old.INACTIONTO is null)) then begin
    if(new.INACTIONFROM is not null and new.INACTIONTO is not null and new.INACTIONFROM>new.INACTIONTO) then begin
      exception PRMACHREPAIRS_EXPT 'Data zakończenia przestoju nie może być wcześniejsza od daty jego rozpoczcia.';
    end
  end
end^
SET TERM ; ^
