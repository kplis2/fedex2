--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PLANS_AU0 FOR PLANS                          
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable bdata timestamp;
declare variable ldata timestamp;
declare variable colref integer;
declare variable plansref integer;
declare variable plansname varchar(40);
begin
  if(new.SQLROW is not null and new.SQLROW <> old.SQLROW)
  then begin
   execute procedure PLANSROW_NALICZ(new.ref, new.keyamount, new.SQLROW, new.statickeyamount,1);
  end
--badanie nachodzących czasowo na siebie aktywnych planów - wg typu i dzialu
  if(new.status is not null and new.status = 1 and (old.status is null or old.status <>1)) then begin
    for select ref, bdata, ldata from planscol pc where pc.plans = new.ref and pc.akt = 1
     into :colref, :bdata, :ldata
    do begin
      plansref = 0;
      select first 1 p.name, p.ref
       from plans p
       join planscol pc on (p.ref = pc.plans)
       join planstypes pt on (p.planstype = pt.symbol)
       where p.status = 1 and p.planstype = new.planstype
             and ((p.prdepart is null and new.prdepart is null) or
                  (new.prdepart is not null and p.prdepart = new.prdepart))
             and (pc.bdata >= :bdata and pc.bdata <= :ldata or
                  pc.ldata >= :bdata and pc.ldata <= :ldata or
                  pc.bdata <= :bdata and pc.ldata >= :ldata)
             and pc.ref <> :colref and pc.akt = 1
       into :plansname, :plansref;
       if (:plansref is not null and :plansref > 0) then begin
         exception PLANS_OKRES_NOT_ALLOW 'Daty kolumny planu pokrywają sie z datami kolumn planu: '||:plansname;
       end
    end
  end
end^
SET TERM ; ^
