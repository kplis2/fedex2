--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDS_BI_ORDER FOR MWSSTANDS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable maxnum integer;
begin
  new.ord = 1;
  select max(number) from mwsstands where whsecrow = new.whsecrow
    into :maxnum;
  if (new.number is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum
    and new.number > 0 --[PM] XXX
  ) then
  -- zmiana istniejacej kolejnosci
  begin
    update mwsstands set ord = 0, number = number + 1
      where number >= new.number and whsecrow = new.whsecrow;
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
