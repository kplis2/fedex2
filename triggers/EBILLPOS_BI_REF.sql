--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EBILLPOS_BI_REF FOR EBILLPOS                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EBILLPOS')
      returning_values new.REF;
end^
SET TERM ; ^
