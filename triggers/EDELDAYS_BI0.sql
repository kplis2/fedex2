--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDELDAYS_BI0 FOR EDELDAYS                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.breakfast is null) then new.breakfast = 0;
  if (new.dinner is null) then new.dinner = 0;
  if (new.supper is null) then new.supper = 0;
  if (new.citytransport is null) then new.citytransport = 0;
  if (new.toairport is null) then new.toairport = 0;
  if (new.meals is null) then new.meals = 0;
  if (new.accommodation is null) then new.accommodation = 0;
  if (new.nottocount is null) then new.nottocount = 0;
end^
SET TERM ; ^
