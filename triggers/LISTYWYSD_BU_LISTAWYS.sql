--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSD_BU_LISTAWYS FOR LISTYWYSD                      
  ACTIVE BEFORE UPDATE POSITION 3 
as
begin
  --zakladanie lub dopinanie do zlecenia transportowego
  if (new.listawys is null and new.akcept = 2 and old.akcept < 2
      and (exists (select first 1 1 from sposdost sd where sd.ref = new.sposdost and sd.listywys = 1))
  )then
  begin
    execute procedure listywysd_get_listawys(new.ref)
      returning_values new.listawys;
  end
  else if (new.listawys is not null and new.akcept < 2 and old.akcept = 2) then
    new.listawys = null;
end^
SET TERM ; ^
