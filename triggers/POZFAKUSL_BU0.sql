--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAKUSL_BU0 FOR POZFAKUSL                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable usluga smallint;
declare variable zakup smallint;
begin
  if(old.rozliczono<>0 and new.rozliczono<>0) then
    exception POZFAKUSL_ROZLICZONO;

  select p.usluga, n.zakup
    from pozfak p
      join nagfak n on (p.dokument = n.ref)
    where p.ref=new.refpozfak
  into :usluga, :zakup;

  if(:usluga<>1 and new.tryb<>2) then
    exception POZFAKUSL_NIEUSLUGA;
  if(:zakup = 1 and (new.kwota=0 or new.kwota is null)) then
    exception POZFAKUSL_ZLAKWOTA;
END^
SET TERM ; ^
