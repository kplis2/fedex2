--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKACCOUNTS_BU_CHECKRKDOKNAG FOR BANKACCOUNTS                   
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  -- sprawdzenie, czy rachunek nie zostal przypisany do jakiegos wyciagu
  if (coalesce(old.bank,'') <> coalesce(new.bank,'')
    or coalesce(old.account,'') <> coalesce(new.account,'')
    or coalesce(old.eaccount,'') <> coalesce(new.eaccount,'')
    or coalesce(old.bankacc,'') <> coalesce(new.bankacc,''))
  then
    if(exists(select first 1 1 from rkdoknag r
                where old.eaccount = r.ticaccount
                  and old.dictdef = r.slodef
                  and old.dictpos = r.slopoz))then
    begin
      exception universal 'Modyfikacja niemożliwa. Rachunek przypisany do co najmniej jednego wyciągu bankowego.';
    end
end^
SET TERM ; ^
