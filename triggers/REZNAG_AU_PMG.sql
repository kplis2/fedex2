--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER REZNAG_AU_PMG FOR REZNAG                         
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if (new.status = 3 and (new.status<>old.status or old.status is null)) then --zmiana statusu na zamkniety
  begin
    if (new.pmelement is not null and new.pmelement <> 0) then  -- rozliczenie nastpuje tylko gdy wybrany jest element
    begin
      if (new.pmposition is not null and new.pmposition <> 0 and old.pmposition is null) then
      begin
        update pmpositions set USEDQ = USEDQ + new.dniwypoz, USEDVAL = USEDVAL + new.stawka * new.dniwypoz where ref = new.pmposition;
      end
    end
  end
  else
  if (old.status = 3 and new.status <> old.status) then   --gdy status sie zmienil a stary byl 'zamkniety' trzeba wycofac rozliczenie
  begin
    if (new.pmelement is not null and new.pmelement <> 0) then
    begin
      if (old.pmposition is not null and old.pmposition <> 0) then
        update pmpositions set USEDQ = USEDQ - new.dniwypoz, USEDVAL = USEDVAL - new.stawka * new.dniwypoz where ref = old.pmposition;
    end
  end
end^
SET TERM ; ^
