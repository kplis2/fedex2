--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECONTRTYPECONDITS_BD_ALLOW FOR ECONTRTYPECONDITS              
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (exists (select first 1 1 from emplcontracts e where e.econtrtype = old.contrtype)) then
    exception ECONTRYPECONDIT;
end^
SET TERM ; ^
