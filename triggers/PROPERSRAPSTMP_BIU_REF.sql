--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROPERSRAPSTMP_BIU_REF FOR PROPERSRAPSTMP                 
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('PROPERSRAPSTMP') returning_values new.ref;
end^
SET TERM ; ^
