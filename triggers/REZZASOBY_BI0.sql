--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER REZZASOBY_BI0 FOR REZZASOBY                      
  ACTIVE BEFORE INSERT POSITION 1 
AS
begin
  if(new.prmachine > 0) then begin
    --kopiowanie nazw i opisow z maszyn
    if(not exists(select ref
               from rezgrup
               where rezgrup.ref = new.grupa and rezgrup.machinesgroup = 1
    )) then
      exception UNIVERSAL 'Grupa zasobów niepoziwązana z produkcją.';
    select prmachines.symbol, prmachines.descript
      from PRMACHINES where ref=new.prmachine
      into new.nazwa, new.opis;
     new.aktywny = 1;
   end
end^
SET TERM ; ^
