--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EAPPLICATIONS_BI_REF FOR EAPPLICATIONS                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EAPPLICATIONS')
      returning_values new.REF;
end^
SET TERM ; ^
