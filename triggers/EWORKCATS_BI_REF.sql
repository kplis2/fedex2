--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWORKCATS_BI_REF FOR EWORKCATS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EWORKCATS')
      returning_values new.REF;
end^
SET TERM ; ^
