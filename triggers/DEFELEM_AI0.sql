--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFELEM_AI0 FOR DEFELEM                        
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable refi integer;
begin
  for select distinct poziom from DEFMENU where KATALOG = new.KATALOG and ELEM is not NULL
    into :refi
  do begin
    if(not exists(select ref from defmenu where poziom=:refi and katalog=new.katalog and elem=new.ref)) then
        insert into DEFMENU(POZIOM,NUMER,KATALOG,ELEM,NAZWA,PIKTOGRAM)
            values(:refi,new.NUMER,new.KATALOG,new.REF,new.NAZWA,1);
  end

  for select ref from DEFMENU where KATALOG = new.KATALOG and ELEM is NULL
    into :refi
  do begin
    if(not exists(select ref from defmenu where poziom=:refi and katalog=new.katalog and elem=new.ref)) then
        insert into DEFMENU(POZIOM,NUMER,KATALOG,ELEM,NAZWA,PIKTOGRAM)
            values(:refi,new.NUMER,new.KATALOG,new.REF,new.NAZWA,1);
  end
end^
SET TERM ; ^
