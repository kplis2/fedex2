--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDESPOS_BU0 FOR PRSCHEDGUIDESPOS               
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable psgsymbol varchar(40);
declare variable psgstatusmat smallint;
begin
  select symbol, statusmat from prschedguides where ref = new.prschedguide into :psgsymbol, :psgstatusmat;
  if(new.amount is null) then new.amount = 0;
  if(new.amountzreal is null) then new.amountzreal = 0;
  if(new.amountzrealakc is null) then new.amountzrealakc = 0;
  if(new.number is null) then new.number = 1;
  if(new.activ is null) then new.activ = 0;
  if(new.amountshortage is null) then new.amountshortage = 0;
  if(new.pggamountcalc is null) then new.pggamountcalc = 0;
  if(new.amountoverlimit is null) then new.amountoverlimit = 0;
  if (new.autodocout is null) then new.autodocout = 0;
  if(new.stan is null or (new.stan = '') or (new.stan = ' ')) then new.stan = 'N';
  if(old.stan <> new.stan and new.stan <> 'N') then begin
    --s[prawdzenie, czy przypadkiem operacja nie jest juz zamknieta - wowczas brak blokad
    if(exists(select ref from  prschedguides where ref=new.prschedguide and prschedguides.status >= 3))then
      new.stan = 'N';
  end
--  if((new.amount <> old.amount or new.amountzreal <> old.amountzreal) and :psgstatusmat >=3) then
--    exception prschedguides_error substring('Wydanie materiałów pod przewodnik: '||:psgsymbol||' zakończone' from 1 for 75);
  if(new.amount < old.amount and new.amount < new.amountzreal) then
    exception prschedguides_error substring('Il.zreal. < il.zlec. - oz:'||new.number||' przew.:'||:psgsymbol from 1 for 55);
--  if(new.amountzreal > old.amountzreal and new.amountzreal > new.amount) then
  --  exception prschedguides_error 'Ilość wydana nie moż być większa od limitu';
  if(new.amount < 0)then
    exception prschedguides_error substring('Próba ustawienia ilości < 0 - przew.:'||:psgsymbol||' poz.:'||new.number from 1 for 55);
  if(new.kamountshortage is null or new.kamountshortage = 0) then
    exception prschedguides_error 'Brak określenia współczynnika braków na pozycji przewodnika';
  else if(new.kamountnorm is null or new.kamountnorm = 0) then
    exception prschedguides_error 'Brak określenia współczynnika normatywnego na pozycji przewodnika';
  if (coalesce(new.wersjaref,0) <> coalesce(old.wersjaref,0)) then
    select w.ktm, w.nrwersji
      from wersje w
      where w.ref = new.wersjaref
      into new.ktm, new.wersja;
  else if (coalesce(new.ktm,'') <> coalesce(old.ktm,'') or (coalesce(new.wersja,0) <> coalesce(old.wersja,0))) then
    select w.ref
      from wersje w
      where w.ktm = new.ktm
        and w.nrwersji = new.wersja
      into new.wersjaref;
end^
SET TERM ; ^
