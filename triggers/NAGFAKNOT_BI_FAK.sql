--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAKNOT_BI_FAK FOR NAGFAKNOT                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable fakakc smallint;
begin
  select AKCEPTACJA from nagfak where REF=new.DOKUMENT into :fakakc;
  if (:fakakc=0) then exception nagfaknot_expt 'Faktura nie jest zaakceptowana, popraw dane bezpośrednio na fakturze.';
end^
SET TERM ; ^
