--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERMAG_BI0 FOR DEFOPERMAG                     
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable aa integer;
begin
  if(new.MAG2 <> '' AND (new.MAG2 is not null)) then begin
    select count(*) from DEFMAGAZ where symbol=new.mag2 into :aa;
    if(:aa is null) then aa = 0;
    if(:aa = 0) then
      EXCEPTION DEFOPERMAG_INVALID_MAG2;
  end
  if(new.magazyn='') then new.magazyn = NULL;
  if(new.magnojoin is null) then new.magnojoin = 0;
end^
SET TERM ; ^
