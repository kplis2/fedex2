--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WFFIELD_BI FOR WFFIELD                        
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_wffield,1);
end^
SET TERM ; ^
