--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INT_SLOWNIK_BIU_SPRAWDZ FOR INT_SLOWNIK                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if(new.zrodlo is null)then
    exception universal 'Brak wartosci w polu zrodlo';
  if(new.otable is null)then
    exception universal 'Brak wartosci w polu otable';
   if(coalesce(new.wartoscid,'') = '' and coalesce(new.wartoscid,'') = '')then
  begin
    exception universal 'Wymagana wartość w polu WARTOSC lub WARTOSCID';
  end
   if(coalesce(new.oref,0) = 0 and coalesce(new.okey,'') = '')then
  begin
    exception universal 'Wymagana wartość w polu OREF lub OKEY';
  end
  if(coalesce(new.otable,'') not in ('PLATNOSCI', 'SPOSDOST','MIARA','ATRYBUTY','DEFMAGAZ')
  )then
  begin
    exception universal 'Niedozwolona wartość OTABLE:' || coalesce(new.otable,'<pusty>');
  end
end^
SET TERM ; ^
