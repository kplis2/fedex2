--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SEGREGATOR_BI_KLASA FOR SEGREGATOR                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.rodzic > 0) then begin
    select lista,slodef,wlasciciel from SEGREGATOR where ref = new.rodzic into new.lista,new.slodef,new.wlasciciel;
    if(new.lista is null) then new.lista = ';';
    new.lista = new.lista || new.rodzic||';';
  end else begin
    new.lista = ';';
    if(new.wlasciciel=0) then new.wlasciciel=NULL;
    if(new.slodef=0) then new.slodef=NULL;
  end
/*  if((new.SLODEF is NULL) and (new.WLASCICIEL is NULL)) then
    new.klasa='FIRM';
  else if((new.SLODEF is NULL) and (new.WLASCICIEL is not NULL)) then
    new.klasa='PRYW' || cast(new.WLASCICIEL as varchar(10)) || ';';
  else if((new.SLODEF is not NULL) and (new.WLASCICIEL is NULL)) then
    new.klasa='PODM' || cast(new.SLODEF as varchar(10)) || ';';
  else
    exception SEGREGATOR_NOTALLOWED;
*/
end^
SET TERM ; ^
