--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLS_BU0 FOR PRTOOLS                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (new.amount is null) then new.amount = 0;
  if (new.amountfree is null) then new.amountfree = 0;
  if (new.price is null) then new.price = 0;

  if (new.amount < 0 ) then
    exception PRTOOLS_ERROR 'Ilosć narzedzi '''||new.symbol||''' <0';
  if (new.amountfree < 0) then
    exception PRTOOLS_ERROR 'Ilosć wydanych narzedzi '''||new.symbol||''' <0';
  if (new.amount < new.amountfree ) then
    exception PRTOOLS_ERROR 'Ilosc narzedzi dost. > od il. narzedzi' ||new.amount||' '||new.amountfree ;
end^
SET TERM ; ^
