--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPERSCHOOLS_AIU_EDUCATION FOR EPERSCHOOLS                    
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
AS
  declare variable code varchar(20);
begin
  select first 1 z.ref
    from eperschools s
      join edictzuscodes z on z.ref = s.education
      where s.person = new.person
      order by z.code desc, z.addinfo desc
    into :code;
    update persons set education = :code where ref = new.person;
end^
SET TERM ; ^
