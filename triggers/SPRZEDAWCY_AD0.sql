--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDAWCY_AD0 FOR SPRZEDAWCY                     
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable slodef integer;
begin
  if(old.crm = 1) then begin
   select ref from SLODEF where TYP = 'SPRZEDAWCY' into :slodef;
   if(:slodef is null) then exception SLODEF_BEZSPRZEDAWCY;
   delete from CPODMIOTY where SLODEF = :slodef AND SLOPOZ = old.ref;
 end

end^
SET TERM ; ^
