--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYIL_BI0 FOR STANYIL                        
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable defcennik integer;
declare variable jedn integer;
declare variable oddzial varchar(10);
begin
  if (new.iloscpot is null) then new.iloscpot = 0;
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji, ktm, akt, nazwa from WERSJE where ref=new.wersjaref into new.wersja, new.ktm, new.akt, new.wnazwa;
  else if(new.ktm is not null and new.wersja is not null) then
    select ref, akt, nazwa from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref, new.akt, new.wnazwa;
  select NAZWA, symbol_dost, MIARA, CHODLIWY, grupa, usluga, DM, altposmode from TOWARY where KTM = new.ktm
    into new.nazwat,new.symbol_dost,  new.miara, new.chodliwy, new.grupa, new.usluga, :jedn, new.altposmode;
  if(new.zarezerw is null) then new.zarezerw =0;
  if(new.zablokow is null) then new.zablokow =0;
  if(new.zamowiono is null) then new.zamowiono = 0;
  if(new.ilosc is null) then new.ilosc = 0;
  if(new.wartosc is null) then new.wartosc = 0;
  if(new.wartosc <> 0) then begin
    if (new.ilosc = 0) then
      new.cena = new.wartosc;
    else
      new.cena = new.wartosc / new.ilosc;
  end else new.cena = 0;
  select ODDZIAL from DEFMAGAZ where SYMBOL = new.magazyn into :oddzial;
  new.oddzial = :oddzial;
  select DEFMAGAZ.glowny from DEFMAGAZ where SYMBOL = new.magazyn into new.glowny;
  if(new.stanmax is null or new.stanmin is null) then
    execute procedure STKRTAB_GETMINMAX(new.magazyn, new.ktm, new.wersja) returning_values new.stanmin, new.stanmax;
  if(coalesce(new.stanmax,0)<>0 or coalesce(new.stanmin,0)<>0) then begin
    insert into STANYMINMAX(MAGAZYN,WERSJAREF,DATA,STANMIN,STANMAX)
    values(new.magazyn, new.wersjaref, current_date, new.stanmin, new.stanmax);
  end
  /*okrelenie stosunku dla barier min-max*/
  new.minmaxstan = 0;
  if(new.ilosc < new.stanmin) then new.minmaxstan = 1;
  else if(new.ilosc > new.stanmax) then new.minmaxstan = 3;
  else if((new.stanmax is not null) or (new.stanmin is not null))then new.minmaxstan = 2;
  if(new.iloscpod is null) then new.iloscpod = 0;
  select sum(ilosc) from STANYIL join DEFMAGAZ on (STANYIL.MAGAZYN = DEFMAGAZ.SYMBOL)
    where STANYIL.WERSJAREF = new.wersjaref and DEFMAGAZ.magmaster = new.magazyn into new.iloscpod;
  if(new.iloscpod is null) then new.iloscpod = 0;
  new.iloscwolna = new.ilosc-new.zarezerw-new.zablokow;
  /*wypelnij ceny*/
  defcennik=NULL;

  select min(ref)
    from DEFCENNIK
    where SYMBOL='A'
      and (oddzial is not null and oddzial <> '' and oddzial = :oddzial)
      and akt=1
  into :defcennik;
  if (:defcennik is null) then
    select min(ref)
      from DEFCENNIK
      where SYMBOL='A'
       and coalesce(oddzial,'')=''
       and akt=1
    into :defcennik;
  if(:defcennik is not null) then
    select CENANET,CENABRU from CENNIK where CENNIK=:defcennik and WERSJAREF=new.wersjaref and JEDN=:jedn
    into new.CENANETA, new.CENABRUA;
  defcennik=NULL;
  select min(ref)
    from DEFCENNIK
    where SYMBOL='B'
      and (oddzial is not null and oddzial <> '' and oddzial = :oddzial)
      and akt=1
    into :defcennik;
  if (:defcennik is null) then
    select min(ref)
      from DEFCENNIK
      where SYMBOL='B'
       and coalesce(oddzial,'')=''
       and akt=1
    into :defcennik;
  if(:defcennik is not null) then
    select CENANET,CENABRU from CENNIK where CENNIK=:defcennik and WERSJAREF=new.wersjaref and JEDN=:jedn
    into new.CENANETB,new.CENABRUB;
  execute procedure GET_STANYIL_KOLOR(new.ilosc,new.stanmin,new.stanmax,new.kolor,new.minmaxstan,new.ktm) returning_values new.kolor;
end^
SET TERM ; ^
