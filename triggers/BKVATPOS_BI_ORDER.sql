--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKVATPOS_BI_ORDER FOR BKVATPOS                       
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable maxnum integer;
begin
  new.ord = 1;
  --select max(number) from bkvatpos where bkdoc = new.bkdoc
  --select curr_number_bkvatpos from bkdocs where ref  = new.bkdoc
  select first 1 number from bkvatpos where bkdoc = new.bkdoc
    order by bkdoc desc, number desc
    into :maxnum;
  if (new.number is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update bkvatpos set ord = 0, number = number + 1
      where number >= new.number and bkdoc = new.bkdoc
      order by bkdoc, number;
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
