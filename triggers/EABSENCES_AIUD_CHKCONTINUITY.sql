--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_AIUD_CHKCONTINUITY FOR EABSENCES                      
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 2 
as
declare variable nfromdate date;
declare variable necolumn integer;
declare variable nfirstabsence integer;
declare variable newfirstabsence integer;
declare variable newbaseabsence integer;
declare variable nbaseabsence integer;
declare variable nref integer;
declare variable ecolpval varchar(10);
declare variable newfbase varchar(6);
declare variable newlbase varchar(6);
declare variable nfbase varchar(6);
declare variable nlbase varchar(6);
declare variable employee integer;
declare variable ecolumn integer;
declare variable fromdate date;
declare variable epayroll integer;
declare variable ncalcpaybase smallint;
begin
/*MWr: Sprawdzenie, czy ciaglosc nastepnej (istniejacej) nieobecnosci danego
  typu V/S zalezy od nieobecnosci biezacej. Jezeli tak jest ustawia odpowiednio
  ciaglosc kolejnej, i tylko jednej niebecnosci. Ustawienie kolejnych rekordow
  odbywa sie juz kaskadowo.*/

  if ((deleting and old.correction < 2) or
      (inserting and new.baseabsence > 0 and new.correction < 2) or
      (updating and (new.fromdate <> old.fromdate or new.todate <> old.todate or
        (new.firstabsence <> old.firstabsence or new.fbase <> old.fbase or
         new.lbase <> old.lbase or new.baseabsence <> old.baseabsence or
         new.ecolumn <> old.ecolumn or new.employee <> old.employee or
         new.correction <> old.correction)))) then -->bez ostatniego warunku po usunieciu korkety
  begin                                            -- koljne nieobec. sie nie przelicza

  --ustawinie zmiennych
    if (deleting) then
    begin
      ecolumn = old.ecolumn;
      employee = old.employee;
      fromdate = old.fromdate;
    end else
    begin
      ecolumn = new.ecolumn;
      employee = new.employee;
      fromdate = new.fromdate;
    end

  --pobranie nastepnej nieobecnosci danego (jak biezaca nieobecnosc) typu
    select pval from ecolparams
      where param = 'BASETYPE' and ecolumn = :ecolumn
      into :ecolpval;

    select first 1 a.ref, a.fromdate, a.ecolumn, a.epayroll, a.calcpaybase,
            a.firstabsence, a.baseabsence, a.lbase, a.fbase
      from eabsences a
      join ecolparams p on (p.ecolumn = a.ecolumn and p.param = 'BASETYPE')
      where a.fromdate > :fromdate
        and a.employee = :employee
        and a.correction in (0,2)
        and p.pval = :ecolpval
      order by fromdate
      into :nref, :nfromdate, :necolumn, :epayroll, :ncalcpaybase,
           :nfirstabsence, :nbaseabsence, :nlbase, :nfbase;

    if (nfromdate is not null) then
    begin
    --nieobecnosc nastepna istnieje, sprawdzamy czy ciaglosc nie ulegla zmianie
    --jezli nieobecnosc jest rozliczona to nie zmieniamy jej ciaglosci i nie informujemy tym
      if (not(exists(select first 1 1 from epayrolls
                   where ref = :epayroll and status > 0))) then
      begin

        select firstabsence, baseabsence,  fbase, lbase
          from E_GET_ABSENCE_PERIODS_BASES(:employee, :necolumn, :nfromdate, null, null)
          into :newfirstabsence, :newbaseabsence, :newfbase, :newlbase;

        if (ncalcpaybase = 1) then
        begin
        --jezeli wymuszenie podstaw, to inne zmienne poza firstabsence nie moge sie zmienic
          newbaseabsence = nbaseabsence;
          newfbase = nfbase;
          newlbase = nlbase;
        end

        if (newfirstabsence <> nfirstabsence or newbaseabsence <> nbaseabsence
            or newfbase <> nfbase or newlbase <> nlbase) then
        begin
          update eabsences
             set firstabsence = :newfirstabsence,
                 baseabsence = :newbaseabsence,
                 fbase = :newfbase,
                 lbase = :newlbase
           where ref = :nref;
        end
      end
    end
  end
end^
SET TERM ; ^
