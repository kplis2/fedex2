--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLOYEES_AU_PERSONNAMES FOR EMPLOYEES                      
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable nowanazwa varchar(255);
declare variable slodef integer;
declare variable currentyear integer;
begin
  if (new.personnames <> old.personnames) then
  begin
    nowanazwa = substring (new.personnames from 1 for 80);
    update rozrach set slonazwa = :nowanazwa
      where slodef in (select ref from slodef where typ = 'EMPLOYEES')
        and slopoz = new.ref;

    -- aktulizacja opisu kont ksiegowych dla bieżącego roku
    currentyear = extract(year from current_timestamp(0));
    for select ref from slodef where typ = 'EMPLOYEES'
      into :slodef
    do begin
      execute procedure accounting_recalc_descript(:slodef, :currentyear, new.company, new.symbol);
    end
  end
end^
SET TERM ; ^
