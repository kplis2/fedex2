--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFREJZAM_BI0 FOR DEFREJZAM                      
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.typzam = '   ' or new.typzam = '') then new.typzam = null;
  execute procedure REPLICAT_STATE('DEFREJZAM', new.state, NULL, NULL, NULL, NULL) returning_values new.state;
  if(new.checkklientpr is null) then new.checkklientpr = 0;
  else if (new.checkklientpr > 100) then new.checkklientpr = 100;
  --Zabezpieczenie, żeby rejestr nie mial blednie prawa do wystawiania zamowien z pozycjami alternatywnymi.
  new.altallow = coalesce(new.altallow,0);
  new.rightaltallow =  coalesce(new.rightaltallow,'');
end^
SET TERM ; ^
