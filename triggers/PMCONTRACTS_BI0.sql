--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMCONTRACTS_BI0 FOR PMCONTRACTS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if(new.orgref is not NULL) then
     select SYMBOL from pmcontracts where REF = new.orgref into new.orgsymbol;
end^
SET TERM ; ^
