--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONTAKTY_AD0 FOR KONTAKTY                       
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  if(old.ckontrakt is not null) then execute procedure CKONTRAKTY_UPDATE_CPODMIOTYKON(old.ckontrakt);
  delete from dokzlacz d where d.ztable = 'KONTAKTY' and d.zdokum = old.ref;
end^
SET TERM ; ^
