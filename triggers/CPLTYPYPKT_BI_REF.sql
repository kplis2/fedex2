--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLTYPYPKT_BI_REF FOR CPLTYPYPKT                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CPLTYPYPKT')
      returning_values new.REF;
end^
SET TERM ; ^
