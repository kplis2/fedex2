--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRACTS_AIUD_EMPLOYEES FOR EMPLCONTRACTS                  
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 6 
as
begin
  -- uaktualnienie tabeli EMPLOYEES
  if (old.employee <> new.employee or deleting) then
    execute procedure employees_update(old.employee, null);
  if (not deleting
    and (old.employee is distinct from new.employee
      or old.cbranch is distinct from new.cbranch
      or old.cdepartment is distinct from new.cdepartment
      or old.econtrtype is distinct from new.econtrtype
      or old.cworkpost is distinct from new.cworkpost
      or old.cworktype is distinct from new.cworktype
      or old.autopresent is distinct from new.autopresent
      or old.cemplgroup is distinct from new.cemplgroup
      or old.cepayrule is distinct from new.cepayrule)
  ) then
    execute procedure employees_update(new.employee,new.fromdate);
end^
SET TERM ; ^
