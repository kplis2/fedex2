--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VIEW_ECONSTELEMS_BD FOR VIEW_ECONSTELEMS               
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  delete from econstelems
  where (employee = old.employee) and
        (ecolumn = old.ecolumn) and
        (fromdate = old.fromdate);
end^
SET TERM ; ^
