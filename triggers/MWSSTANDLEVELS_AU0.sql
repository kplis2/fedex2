--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDLEVELS_AU0 FOR MWSSTANDLEVELS                 
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.volume <> old.volume or new.h <> old.h or new.levelheight <> old.levelheight
      or new.number <> old.number) then
    update mwsconstlocs set volume = new.volume, h = new.h, levelheight = new.levelheight,
        coordzb = new.levelheight, coordze = new.levelheight + new.h,
        mwsstandlevelnumber = new.number
      where mwsstandlevel = new.ref;
end^
SET TERM ; ^
