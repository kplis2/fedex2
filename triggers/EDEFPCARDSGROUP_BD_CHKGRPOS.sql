--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDEFPCARDSGROUP_BD_CHKGRPOS FOR EDEFPCARDSGROUP                
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
--MWr: Kontrola, aby nie usunac grupy, ktora jest skladowa innej grupy

  if (exists(select first 1 1 from edefpcardsgrpos where subgroup = old.ref)) then
    exception edefpcardsgroup_delete;
end^
SET TERM ; ^
