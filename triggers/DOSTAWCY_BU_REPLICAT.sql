--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTAWCY_BU_REPLICAT FOR DOSTAWCY                       
  ACTIVE BEFORE UPDATE POSITION 1 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if((new.REF <> old.ref)
   or (new.id <> old.id)
   or(new.firma <> old.firma ) or (new.firma is null and old.firma is not null) or (new.firma is not null and old.firma is null)
   or(new.nazwa <> old.nazwa ) or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null)
   or(new.nip <> old.nip ) or (new.nip is null and old.nip is not null) or (new.nip is not null and old.nip is null)
   or(new.regon <> old.regon ) or (new.regon is null and old.regon is not null) or (new.regon is not null and old.regon is null)
   or(new.ulica <> old.ulica ) or (new.ulica is null and old.ulica is not null) or (new.ulica is not null and old.ulica is null)
   or(new.nrdomu <> old.nrdomu ) or (new.nrdomu is null and old.nrdomu is not null) or (new.nrdomu is not null and old.nrdomu is null)
   or(new.nrlokalu <> old.nrlokalu ) or (new.nrlokalu is null and old.nrlokalu is not null) or (new.nrlokalu is not null and old.nrlokalu is null)
   or(new.miasto <> old.miasto ) or (new.miasto is null and old.miasto is not null) or (new.miasto is not null and old.miasto is null)
   or(new.kodp <> old.kodp ) or (new.kodp is null and old.kodp is not null) or (new.kodp is not null and old.kodp is null)
   or(new.poczta <> old.poczta ) or (new.poczta is null and old.poczta is not null) or (new.poczta is not null and old.poczta is null)
   or(new.telefon <> old.telefon ) or (new.telefon is null and old.telefon is not null) or (new.telefon is not null and old.telefon is null)
   or(new.fax <> old.fax ) or (new.fax is null and old.fax is not null) or (new.fax is not null and old.fax is null)
   or(new.email <> old.email ) or (new.email is null and old.email is not null) or (new.email is not null and old.email is null)
   or(new.www <> old.www ) or (new.www is null and old.www is not null) or (new.www is not null and old.www is null)
   or(new.uwagi <> old.uwagi ) or (new.uwagi is null and old.uwagi is not null) or (new.uwagi is not null and old.uwagi is null)
   or(new.rabat <> old.rabat ) or (new.rabat is null and old.rabat is not null) or (new.rabat is not null and old.rabat is null)
   or(new.dnireal <> old.dnireal ) or (new.dnireal is null and old.dnireal is not null) or (new.dnireal is not null and old.dnireal is null)
   or(new.kontofk <> old.kontofk ) or (new.kontofk is null and old.kontofk is not null) or (new.kontofk is not null and old.kontofk is null)
   or(new.bank <> old.bank ) or (new.bank is null and old.bank is not null) or (new.bank is not null and old.bank is null)
   or(new.rachunek <> old.rachunek ) or (new.rachunek is null and old.rachunek is not null) or (new.rachunek is not null and old.rachunek is null)
   or(new.sposplat <> old.sposplat ) or (new.sposplat is null and old.sposplat is not null) or (new.sposplat is not null and old.sposplat is null)
   or(new.waluta <> old.waluta ) or (new.waluta is null and old.waluta is not null) or (new.waluta is not null and old.waluta is null)
   or(new.typ <> old.typ ) or (new.typ is null and old.typ is not null) or (new.typ is not null and old.typ is null)
   or(new.DOSTAWAILOSC <> old.DOSTAWAILOSC ) or (new.DOSTAWAILOSC is null and old.DOSTAWAILOSC is not null) or (new.DOSTAWAILOSC is not null and old.DOSTAWAILOSC is null)
   or(new.dostawamiara <> old.dostawamiara ) or (new.dostawamiara is null and old.dostawamiara is not null) or (new.dostawamiara is not null and old.dostawamiara is null)

  )then
   waschange = 1;

  execute procedure rp_trigger_bu('DOSTAWCY',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
