--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INWENTAP_BD0 FOR INWENTAP                       
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable calkowity smallint;
declare variable zbiorczy integer;
declare variable zamk integer;
declare variable cnt integer;
begin
  select calkowity, zbiorczy, zamk from INWENTA where ref=old.inwenta into :calkowity, :zbiorczy, :zamk;
  cnt = 0;
  if(exists(select first 1 1 from inwentap
    where inwenta = old.inwenta
      and wersjaref = old.wersjaref
      and ilstan = old.ilstan
      and ref<>old.ref)) then cnt = 1;
  if(old.ilstan > 0 and :calkowity > 0 and :cnt=0) then
     exception INWENTAP_USUN_STAN;
  if(:zbiorczy > 0 and old.ilinw > 0) then begin
     exception INWENTAP_EXPT 'Asortyment '||old.ktm||' istnieje na arkuszach podrzednych.';
  end
  if(:zamk>0)then
     exception INWENTA_ZAAKCEPTOWANE 'Nie można usunąć pozycji na zamkniętej inwentaryzacji!';
end^
SET TERM ; ^
