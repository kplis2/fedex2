--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFFORMP_BU_ORDER FOR DEFFORMP                       
  ACTIVE BEFORE UPDATE POSITION 1 
as
declare variable maxnum integer;
begin
  if (NEW.ord is null) then new.ord = 1;
  if (new.numer is null) then new.numer = old.numer;
  if (new.ord = 0 or new.numer = old.numer) then
  begin
    new.ord = 1;
    exit;
  end
  --numerowanie obszarow w rzedzie gdy nie podamy numeru
  if (new.numer <> old.numer) then
    select max(numer) from defformp where forma = new.forma
      into :maxnum;
  else
    maxnum = 0;
  if (new.numer < old.numer) then
  begin
    update defformp set ord = 0, numer = numer + 1
      where (forma <> old.forma or ident <> old.ident) and numer >= new.numer
        and numer < old.numer and forma = new.forma;
  end else if (new.numer > old.numer and new.numer <= maxnum) then
  begin
    update defformp set ord = 0, numer = numer - 1
      where (forma <> old.forma or ident <> old.ident) and numer <= new.numer
        and numer > old.numer and forma = new.forma;
  end else if (new.numer > old.numer and new.numer > maxnum) then
    new.numer = old.numer;
end^
SET TERM ; ^
