--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_BU0 FOR DOKUMNAG                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable chronologia smallint;
declare variable numpoakcept smallint;
declare variable numberg varchar(20);
declare variable wydania smallint;
declare variable autosymbol smallint;
declare variable typmag char(1);
declare variable sblokadadelay varchar(255);
declare variable blokadadelay integer;
declare variable local smallint;
declare variable wysstan char(1);
declare variable miedzyzaw smallint;
declare variable prostynipkon varchar(40);
declare variable prostynipdok varchar(40);
declare variable BLOCKREF integer;
declare variable datawazn timestamp;
declare variable cnt integer;
declare variable wagagr numeric(14,2);
declare variable waga numeric(14,2);
declare variable okoszt numeric(14,2);
declare variable nkoszt numeric(14,2);
declare variable spd integer;
declare variable symbolfak varchar(20);
declare variable koryg integer;
declare variable zewn integer;
declare variable aktulocat integer;
declare variable locat integer;
declare variable defslodef integer;
declare variable paramn1 numeric(14,4);
declare variable paramn2 numeric(14,4);
declare variable pozref integer;
declare variable skrotpar1 varchar(20);
declare variable skrotpar2 varchar(20);
declare variable aktuoperator integer;
declare variable slodefref integer;
declare variable kontrolazaw smallint;
declare variable symbol varchar(80);
declare variable objetoscgr numeric(14,4);
declare variable sumwartnetgr numeric(14,2);
declare variable sumwartbrugr numeric(14,2);
declare variable termdost timestamp;
declare variable dref integer;
declare variable havefake smallint;
declare variable fake smallint;
declare variable alttopoz integer;
declare variable pozdost integer;
declare variable autosellakcept smallint;
declare variable autosellsymbol symbol_id;
begin

 if(new.akcept = 1 and (old.akcept = 0 or old.akcept = 9))then begin
   new.dataakc = current_time;
   execute procedure get_global_param('AKTUOPERATOR') returning_values new.operakcept;
    

   --XXX Ldz aktoperator dla RW
     if (new.typ = 'RW' and new.operakcept is null) then
       new.operakcept = 74;
     if (new.typ = 'WZ' and new.operakcept is null) then
       new.operakcept = 74;
     if (new.typ = 'PW' and new.operakcept is null) then
       new.operakcept = 74;
     if (new.typ = 'ZZ' and new.operakcept is null) then
       new.operakcept = 74;
   --XXX Ldz aktoperator dla RW
   --if (new.operakcept is null) then new.operakcept = 74;
   if(new.operakcept is null) then exception DOKUMNAG_OPERATOR 'Brak wskazania akceptanta na '||new.symbol;
 end else if((new.akcept = 0 or new.akcept = 9) and old.akcept = 1) then begin
   new.dataakc = null;
   new.wasdeakcept = 1;
   new.operakcept = null;
   new.mwsdocreal = 0;
 end
 if(new.acceptmistake is null) then new.acceptmistake = 0;
 if(new.sellmistake is null) then new.sellmistake = 0;
 if(new.checkmistake is null) then new.checkmistake = 0;
 if(new.wydano is null) then new.wydano = 0;
 if(new.mag2 = '') then new.mag2 = null;
 if(new.dostawa=0) then new.dostawa = null;
 if(new.blokada is null) then new.blokada = 0;
 if(new.blokadafak is null) then new.blokadafak = 0;
 if(new.operator = 0) then new.operator = NULL;
 if(new.sumwartwal is null) then new.sumwartwal = 0;
 if(new.sumwartzl is null) then new.sumwartzl = 0;
 if(new.wartoscl is null) then new.wartoscl = 0;
 if(new.pwartosc is null) then new.pwartosc = 0;
 if(new.wartoscwal is null) then new.wartoscwal = 0;
 if(new.waluta='') then new.waluta = NULL;
 if(new.kurs is null) then new.kurs = 1;
 if(new.waga is null) then new.waga = 0;
 new.wartosczl = new.wartoscwal * new.kurs;
 new.odchylenie = new.sumwartzl - new.wartosczl;
 select DEFDOKUM.WYDANIA, DEFDOKUM.koryg, defdokum.zewn, defdokum.defslodef, defdokum.walutowy, defdokum.miedzyzaw
   from defdokum where DEFDOKUM.symbol = new.TYP
   into :wydania, :koryg, :zewn, :defslodef, new.walutowy, :miedzyzaw;
 select DEFMAGAZ.typ, DEFMAGAZ.autosymdost
   from DEFMAGAZ where DEFMAGAZ.symbol = new.MAGAZYN
   into :typmag, :autosymbol;
 if(new.akcept=7 and new.nip is not null and new.nip<>'') then begin
   if(new.klient is not null) then begin
     select PROSTYNIP from KLIENCI where REF=new.klient into :prostynipkon;
     prostynipdok = replace(new.nip, '-', '');
     if(prostynipdok<>prostynipkon) then begin
       new.klient = null;
       select min(REF) from KLIENCI where PROSTYNIP = :prostynipdok into new.klient;
       if(new.klient is null) then select min(REF) from KLIENCI where FSKROT = new.nip into new.klient;
     end
     if(new.klient is null) then exception DOKUMNAG_NOKONTRAH 'Podczas zakladania dok. mag. nie znaleziono klienta: '||new.nip;
   end
   if(new.dostawca is not null) then begin
     select PROSTYNIP from DOSTAWCY where REF=new.dostawca into :prostynipkon;
     prostynipdok = replace(new.nip, '-', '');
     if(prostynipdok <> :prostynipkon) then begin
       new.dostawca = null;
       select min(REF) from DOSTAWCY where PROSTYNIP = :prostynipdok into new.dostawca;
       if(new.dostawca is null) then select min(REF) from DOSTAWCY where ID=new.nip into new.dostawca;
     end
     if(new.dostawca is null) then exception DOKUMNAG_NOKONTRAH 'Podczas zakladania dok. mag. nie znaleziono dostawcy: '||new.nip;
   end
 end

 -- kontrola zmienianej daty przy włączonej chronologii BS42215
 if (new.data <> old.data) then
   begin
     select chronologia, numpoakcept from DEFMAGAZ where symbol=new.magazyn into :chronologia, :numpoakcept;
     if (:numpoakcept = 0 and :chronologia = 1) then exception dokumnag_expt 'Włączona kontrola chronologi. Nie można zmienić daty dokumentu.';
   end

 if(new.akcept <> old.akcept and new.akcept <> 8 and new.akcept <> 7)then begin
  /*zmiana stanu akceptacji walasciwa*/
   execute procedure GETCONFIG('SIDFK_BLOKADA') returning_values :sblokadadelay;
   if(new.blokada > 0) then begin
     if(:sblokadadelay <> '') then
       blokadadelay = cast(:sblokadadelay as integer);
     else blokadadelay = 0;
     if(bin_and(new.blokada,1)>0 and bin_and(old.blokada,1)>0) then exception DOKUMNAG_BLOKADAHAND;/*akceptacja przy istniejącej blokadzie*/
     if(bin_and(new.blokada,4)>0 and current_date >= new.data + :blokadadelay) then exception DOKUMNAG_ZAKSIEGOWANY;
     if(bin_and(new.blokada,4)>0) then new.blokada = bin_and(new.blokada,3);/*zniesienie blokady ksiegowej, jesli mozna*/
    end
    select chronologia, numpoakcept from DEFMAGAZ where symbol=new.magazyn into :chronologia, :numpoakcept;
    if(:chronologia is null) then chronologia = 0;
    if(:numpoakcept is null) then numpoakcept = 0;

   /*kontrola chronologii - poza wycofaniem do poprawy*/
   if(:chronologia > 0 and (new.blokada <> 2 and old.blokada <> 2 and new.akcept<> 9 and old.akcept <> 9)) then begin
      cnt = null;
      select count(*) from DOKUMNAG where MAGAZYN=new.magazyn and DATA>=(new.data+:chronologia) and akcept = 1 into :cnt;
      if(coalesce(cnt,0) > 0) then begin
        symbol = '';
        select first 1 symbol from DOKUMNAG where MAGAZYN=new.magazyn and DATA>=(new.data+:chronologia) and akcept = 1 into :symbol;
        if(cnt = 1) then exception DOKUMNG_AKCEP_POZNIEJ 'Naruszenie chronologii dokumentów - istnieje dokument późniejszy ' || :symbol;
        else if(cnt < 4) then exception DOKUMNG_AKCEP_POZNIEJ 'Naruszenie chronologii dokumentów - istnieją ' || :cnt || ' dokumenty późniejsze np. '||:symbol;
        else exception DOKUMNG_AKCEP_POZNIEJ 'Naruszenie chronologii dokumentów - istnieje '||:cnt || ' dokumentów późniejszych np. '||:symbol;
      end
    end
    select NUMBER_GEN from DEFMAGAZ where  symbol = new.magazyn into :numberg;
    execute procedure CHECK_LOCAL('DOKUMNAG',new.ref) returning_values :local;
    if(:numberg is not null and :local = 1) then
      if(:numpoakcept = 1 and :numberg is not null) then begin
       if(new.akcept = 1 and old.akcept <> 9 and new.blokada <> 2 and old.blokada <> 2) then begin

          execute procedure Get_NUMBER(:numberg, new.magazyn, new.typ, new.data, new.numblock, new.ref) returning_values new.numer, new.symbol;
          new.numblock = null;
          new.oldsymbol = new.numer || ';' || new.symbol;
        end else  if(old.akcept <> 9 and new.akcept <> 9 and new.blokada <> 2 and old.blokada <> 2) then begin

         /*zwolnienie numeratora*/
          if(old.numer <> 0) then begin
             execute procedure free_number(:numberg, old.magazyn, old.typ, old.data, old.numer, new.numblockget) returning_values new.numblock;
          end
          if(new.numblock > 0) then
            update NUMBERBLOCK set OPERATOR = new.operator where ref=new.numblock;
        /*nadanie numeratora tymczasowego*/
          new.numer = -1;
          new.symbol = 'TYM/'||cast(new.ref as varchar(20));
        end
      end
    if(((new.akcept = 0) or (new.akcept >= 9) or (new.akcept = 7))and (old.akcept = 1 or old.akcept = 8) ) then begin
      new.proglojdone = 0;
    end

  /* sprawdzenie czy dostawca aktywny */
    if(new.dostawca is not null and new.akcept=1) then begin
      if (not exists(select first 1 1 from DOSTAWCY where ref=new.dostawca and dostawcy.aktywny > 0))
      then exception DOKUMNAG_EXPT 'Wskazany na dokumencie dostawca jest zaznaczony jako nieaktywny !';
    end

  /* sprawdzenie uprawnien operatora*/

    if(new.akcept <> old.akcept) then begin
      aktuoperator = null;
      execute procedure get_global_param('AKTUOPERATOR') returning_values :aktuoperator;
      if(new.akcept = 1) then begin
        if(not exists(select * from opermag where magazyn=new.magazyn and operator=new.operakcept and
                   (dokumtypesacceptright is null or dokumtypesacceptright like '%;'||new.typ||';%'))) then
        exception DOKUMNAG_OPERATOR substring('Operator nie ma uprawnień do akceptacji '||new.typ||' na '||new.magazyn from 1 for 75);
      end else if(new.akcept = 9) then begin
        if(not exists(select * from opermag where magazyn=new.magazyn and operator=:aktuoperator and
                   (dokumtypeschangeright is null or dokumtypeschangeright like '%;'||new.typ||';%'))) then
        exception DOKUMNAG_OPERATOR substring('Operator nie ma uprawnień do wycofania do poprawy '||new.typ||' na '||new.magazyn from 1 for 75);
      end else if(new.akcept = 0) then begin
      if(not exists(select * from opermag where magazyn=new.magazyn and operator=:aktuoperator and
                   (dokumtypesacceptbackright is null or dokumtypesacceptbackright like '%;'||new.typ||';%'))) then
        exception DOKUMNAG_OPERATOR substring('Operator nie ma uprawnień do wycofania akceptacji '||new.typ||' na '||new.magazyn from 1 for 75);
      end
    end
    if(new.akcept = 1 and (old.akcept = 0 or old.akcept = 9)) then
    begin
      if(:wydania = 0 and :autosymbol = 1 and :typmag = 'P' and new.dostawa is null) then
      begin
        -- zalozenie tylu dostaw ile dat waznosci + dostawa dla nullowych dat
        -- BS41516 nie generuje dostawy dla pozycji korygowanych (KORTOPOZ is not null)
        for
          select DOKUMPOZ.REF,DOKUMPOZ.DATAWAZN, coalesce(DOKUMPOZ.FAKE,0),DOKUMPOZ.ALTTOPOZ
            from DOKUMPOZ
            where DOKUMENT = new.ref and (DOKUMPOZ.dostawa is null or DOKUMPOZ.dostawa=0) and DOKUMPOZ.KORTOPOZ is null
            order by coalesce(fake,0), DATAWAZN
            into :pozref,:datawazn,:fake,:alttopoz
        do begin
          if (:fake = 0) then
          begin
          execute procedure DOKUMNAG_CREATE_DOKMAG_DOSTAWA(new.ref,new.dostawca,new.data,new.magazyn,:datawazn,:pozref,NULL,NULL)
            returning_values new.dostawa;
            pozdost = new.dostawa;
          end
          else
            select p.dostawa from dokumpoz p
              where p.dokument = new.ref
                and p.ref = :alttopoz
            into :pozdost;

          if (:pozdost is not null) then
            update DOKUMPOZ set DOSTAWA = :pozdost where REF=:pozref;
          pozdost = null;
        end
      end
      select min(slodef.ref) from slodef where slodef.typ = 'DOSTAWCY' into :slodefref;
      if(:wydania = 0 and :koryg = 0 and :zewn = 1 and :defslodef = :slodefref and new.dostawca is null) then
        exception DOKUMNAG_BEZDOST;
    end
    if(new.akcept = 1 and (old.akcept = 0 or old.akcept = 9)) then
    begin
      if(:wydania = 0 and :autosymbol = 1 and :typmag = 'P') then
      begin
        /* poprawienie parametrów w juz istniejacych dostawach */
        for
          select DOKUMPOZ.REF,DOKUMPOZ.DATAWAZN
            from DOKUMPOZ
            where DOKUMENT = new.ref and (DOKUMPOZ.dostawa is not null and DOKUMPOZ.dostawa<>0)
            order by DATAWAZN
            into :pozref,:datawazn
        do begin
          execute procedure DOKUMNAG_UPDATE_DOKMAG_DOSTAWA(new.ref,new.dostawca,new.data,new.magazyn,:datawazn,:pozref);
        end
      end
    end
    if(new.akcept = 1 and new.mag2 is null) then begin
     /*sprawdzenie, czy musi byc podany magazyn 2*/
       if(:miedzyzaw > 0) then
         exception DOKUMNAG_BEZ_MAG2;
    end
    /*sprawdzenie, czy drugi podany magazyn inny niż żródowy*/
    if (new.akcept = 1 and new.mag2 = new.magazyn ) then begin
       if(:miedzyzaw = 2) then
         exception dokumnag_zly_mag2;
    end
    /*Sprawdzenie czy czy istnieją pozycje*/
    if(new.akcept = 1) then begin
      select count(*) from dokumpoz where dokumpoz.dokument = new.ref into :cnt;
      if(:cnt < 1) then
        exception DOKUMNAG_AKCEPT 'Nie można zaakceptować dokumentu magazynowego bez pozycji!';
    end

    if(new.akcept = 0 or new.akcept = 9)then begin
     /*sprawdzenie, czy są korekty ilociowe do danego dokumentu*/
      cnt = null;
      select count(*) from DOKUMNAG where REFK = new.ref into :cnt;
      if(:cnt > 0) then exception DOKUMNAG_MAKORIL;
      if(new.refk > 0) then begin
        /*jeli sam jest korygujący - powiązany sprawdzenie, czy są wystawione kolejne korekty*/
        cnt = null;
        select count(*) from DOKUMNAG where REFK = new.refk and korektanr > new.korektanr into :cnt;
        if(:cnt > 0) then exception DOKUMNAG_MAKORNEXT;
      end
      /* Sprawdzenie czy dokument powiązany ma ustawioną kontrol zawartosci (DEFDOKUM.MIEDZYZAWART)*/
      kontrolazaw = 0;
      if(new.ref2 is not null) then begin
        select dd.miedzyzawart
          from dokumnag dn
          left join defdokum dd on (dd.symbol=dn.typ)
        where dn.ref=new.ref2
        into :kontrolazaw;
        if (kontrolazaw=1) then exception DOKUMNAG_KONTROLAZAW;
      end
    end
    if(new.akcept = 1 and old.akcept <> 1) then begin
      execute procedure getconfig('AKTULOCAT') returning_values :aktulocat;
      select oddzialy.location from ODDZIALY where ODDZIAL=new.oddzial into :locat;
      if(:locat <> :aktulocat) then begin
        exception DOKUMNAG_EXPT 'Próba akceptacji dok. magazynowego z oddzialu: '||new.oddzial;
      end
    end
    new.wysylkadone = 0;
  end
  -- dyspozycja magazynowa przyjecia musi miec dostawe ustawiona dla zlecen magazynowych
  if (new.mwsdisposition = 1 and coalesce(old.mwsdisposition,0) = 0 and new.akcept = 0) then
  begin
    if(:wydania = 0 and :autosymbol = 1 and :typmag = 'P' and new.dostawa is null) then
    begin
      /* zalozenie tylu dostaw ile dat waznosci + dostawa dla nullowych dat */
      for
        select DOKUMPOZ.REF,DOKUMPOZ.DATAWAZN
          from DOKUMPOZ
          where DOKUMENT = new.ref and (DOKUMPOZ.dostawa is null or DOKUMPOZ.dostawa=0)
          order by DATAWAZN
          into :pozref,:datawazn
      do begin
        execute procedure DOKUMNAG_CREATE_DOKMAG_DOSTAWA(new.ref,new.dostawca,new.data,new.magazyn,:datawazn,:pozref,NULL,NULL)
          returning_values new.dostawa;
        update DOKUMPOZ set DOSTAWA = new.dostawa where REF=:pozref;
      end
    end
  end
 if(new.magazyn <> old.magazyn) then
   select ODDZIAL from DEFMAGAZ where SYMBOL = new.magazyn into new.oddzial;
 -- uzupenienie sprawy na dokumencie zgodnie z magazynem, jezeli nie byla wybrana wczesniej
 if(new.magazyn <> old.magazyn and new.ckontrakty is null) then
   select CKONTRAKTY from DEFMAGAZ where SYMBOL = new.magazyn into new.ckontrakty;
  if(((new.akcept = 1 and old.akcept = 0) or (new.akcept = 8 and old.akcept = 7)) and
      new.ckontrakty is null) then
    select ckontrakty from defmagaz where defmagaz.symbol = new.magazyn into new.ckontrakty;
 if((new.mag2<> old.mag2) or (new.mag2 is not null and old.mag2 is null)) then
    select ODDZIAL from DEFMAGAZ where SYMBOL = new.mag2 into new.oddzial2;
 else
   if(new.mag2 is null) then new.oddzial2 = null;
 if(new.akcept = 1 and old.akcept <> 1 and new.wysylka > 0)then begin
   select STAN from LISTYWYS where REF=new.wysylka into :wysstan;
   if(:wysstan <> 'O') then new.wysylka = null;
 end
 if(new.trasadost = 0) then new.trasadost = null;
 if(new.klient > 0 and ((new.klient <> old.klient) or (old.klient is null))) then begin
    select REF from SLODEF where  TYP='KLIENCI' into new.slodef;
    new.slopoz = new.klient;
    new.nip = null;
    select NIP from KLIENCI where REF=new.klient into new.NIP;
    if(new.NIP is null or (new.NIP='')) then select FSKROT from KLIENCI where REF=new.klient into new.NIP;
 end else if(new.dostawca > 0 and ((new.dostawca <> old.dostawca) or (old.dostawca is null))) then begin
    select REF from SLODEF where  TYP='DOSTAWCY' into new.slodef;
    new.slopoz = new.dostawca;
    new.nip = null;
    select NIP from DOSTAWCY where REF=new.dostawca into new.NIP;
    if(new.NIP is null or (new.NIP='')) then select ID from DOSTAWCY where REF=new.dostawca into new.NIP;
 end
 if(new.odbiorcaid <> old.odbiorcaid or (new.odbiorcaid is not null and old.odbiorcaid is null))then begin
   select ODBIORCY.odbiorcaspedyc from ODBIORCY where ref=new.odbiorcaid into new.odbiorcaidsped;
   if(new.odbiorcaidsped is null) then
     new.odbiorcaidsped = new.odbiorcaid;
 end else if(new.odbiorcaid is null and old.odbiorcaid is not null) then
   new.odbiorcaidsped = null;
-- przylacenie do grupy spedycyjnej
 if(new.grupasped <> old.grupasped and new.grupasped <> new.ref) then  begin
   select sposdost,kosztdostbez, kosztdostroz, flagisped, sposdostauto
   from DOKUMNAG where REF=new.grupasped
   into new.sposdost, new.kosztdostbez, new.kosztdostroz, new.flagisped, new.sposdostauto;
 end
--odlacenie od grupy spedycyjnej
 if(new.grupasped > 0 and (
    ((new.termdost <> old.termdost) or (new.termdost is not null and old.termdost is null) or (new.termdost is null and old.termdost is not null)) or
    ((new.odbiorcaidsped <> old.odbiorcaidsped) or (new.odbiorcaidsped is not null and old.odbiorcaidsped is null) or (new.odbiorcaidsped is null and old.odbiorcaidsped is not null))
 ))then begin
   cnt = null;
   select count(*) from DOKUMNAG where ref <> new.ref and (GRUPASPED = new.grupasped or (ref = new.grupasped))
     and TERMDOST = new.termdost and SPOSDOST = new.sposdost and ODDZIAL = new.oddzial and TYP = new.typ and KLIENT = new.klient
     and (new.odbiorcaid is null or (odbiorcaid = new.odbiorcaid))
   into :cnt;
   if(:cnt is null or (:cnt = 0)) then new.grupasped = null;
 end
 if(new.grupasped is null) then new.grupasped = new.ref;
 if(new.grupasped <> old.grupasped)THEN begin
   if(new.grupasped <> new.ref) then
    select SYMBOL from DOKUMNAG where ref = new.grupasped into new.grupaspeddesc;
   else
    new.grupaspeddesc = '';
 end
 /*obliczenie kosztu transportu jako przyrost kosztu transportu grupy dokumentów */
 if (new.waga is null) then new.waga=0;
 if (((new.waga<>old.waga) or
      (new.objetosc<>old.objetosc) or
      (new.sposdost<>old.sposdost) or
      (new.sposzap<>old.sposzap) or
      (new.dkodp<>old.dkodp) or
      (new.flagisped<>old.flagisped) or
      (new.wartsnetto<>old.wartsnetto) or
      (new.wartsbrutto<>old.wartsbrutto) or
      (new.sposdostauto<>old.sposdostauto) or
      (new.data<>old.data) or
      (new.termdost<>old.termdost)
     ) and (new.akcept=0 or new.akcept=9) and (:zewn=1) and ((:koryg=0 and :wydania=1) or (:koryg=1 and :wydania=0))
    ) then begin
   select sum(dokumnag.waga * (1-defdokum.koryg*2)),
          sum(dokumnag.objetosc * (1-defdokum.koryg*2)),
          sum(kosztdost),
          sum(dokumnag.wartsnetto * (1-defdokum.koryg*2)),
          sum(dokumnag.wartsbrutto * (1-defdokum.koryg*2))
     from dokumnag
     left join defdokum on (defdokum.symbol = dokumnag.typ)
     where dokumnag.grupasped=new.grupasped and dokumnag.ref <> new.ref
     into :wagagr, :objetoscgr, :nkoszt, :sumwartnetgr, :sumwartbrugr;
   if(:wagagr is null) then wagagr = 0;
   if(:objetoscgr is null) then objetoscgr = 0;
   if(:nkoszt is null) then nkoszt = 0;
   if(:sumwartnetgr is null) then sumwartnetgr = 0;
   if(:sumwartbrugr is null) then sumwartbrugr = 0;
   if(:koryg=1) then begin
     wagagr = :wagagr - new.waga;
     objetoscgr = :objetoscgr - new.objetosc;
     sumwartnetgr = :sumwartnetgr - new.wartsnetto;
     sumwartbrugr = :sumwartbrugr - new.wartsbrutto;
   end else begin
     wagagr = :wagagr + new.waga;
     objetoscgr = :objetoscgr + new.objetosc;
     sumwartnetgr = :sumwartnetgr + new.wartsnetto;
     sumwartbrugr = :sumwartbrugr + new.wartsbrutto;
   end
   if (new.termdost is null) then termdost = new.data; else termdost = new.termdost;
   select koszt, optyspos, kods, iloscpal, ilosckart
     from GET_KOSZTDOST(new.sposdost,new.sposzap,:wagagr,:objetoscgr, new.wartsnetto,new.wartsbrutto,new.dkodp,new.flagisped,new.sposdostauto,NULL,:termdost,null,new.iloscpalet,new.iloscpaczek,'')
     into new.kosztdost, new.sposdost, new.kodsped, new.iloscpalet, new.iloscpaczek;
   if(new.sposdost = 0) then new.sposdost = null;
   new.kosztdost = new.kosztdost - :nkoszt;
 end
 /*zaktualizowanie symbolu faktury na dokumencie magazynowym*/
 if (coalesce(new.faktura,0) <> coalesce(old.faktura,0)) then
 begin
   execute procedure DOKUMNAG_GET_SYMBFAK(new.ref,new.faktura) returning_values new.symbolfak;
 end

 if(new.akcept = 1 and old.akcept <> 1) then begin
   if (old.wydrukowano = 1) then new.wydrukowano = 2;
   else begin
     new.wydrukowano = 0;
     new.grupadruk = 0;
   end
 end
 if(new.krajwysylki = '') then new.krajwysylki = null;
 if (new.wydano = 1 and old.wydano = 0) then
   new.datawydania = current_timestamp(0);
 if (new.termdost is null) then new.termdost = new.data;
 if(new.historia is not null and (old.historia is null or new.historia <> old.historia)) then
   select skad, dokumentmain from histzam where ref = new.historia into new.skad, new.dokumentmain;
 if (new.akcept = 0 and old.akcept <> 0) then
 begin
   for
     select d.ref
       from dokumnag d
       where d.grupasped = new.grupasped and ref <> new.ref
       into :dref
   do begin
     execute PROCEDURE DOKUMNAG_MWSREALSTATUS(:dref,0);
   end
   new.mwsstatus = 0;
   new.mwsrealprogress = '0/0';
 end
end^
SET TERM ; ^
