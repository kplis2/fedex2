--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PLANSCOL_BU0 FOR PLANSCOL                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable okres varchar(6);
declare variable data timestamp;
declare variable akt smallint;
declare variable planstype varchar(20);
declare variable plansref integer;
declare variable plansname varchar(40);
declare variable plansdepart varchar (20);
declare variable period varchar(40);
begin
  if(new.bdata > new.ldata) then exception PLANS_OKRES_NOT_ALLOW 'Niespójna definicja dat.';
  if (new.period is not null and (old.period is null or new.period<>old.period)) then begin
    execute procedure datatookres(substring(new.period from 1 for 15), null, null, null, 1)
    returning_values(:okres, :data, new.bdata, new.ldata);
  end else if (new.LDATA <> old.LDATA or (new.BDATA <> old.BDATA)) then
    new.PERIOD = new.BDATA||'/'||new.LDATA;
--badanie nachodzących czasowo na siebie aktywnych planów - wg typu i dzialu
  if(new.akt is not null and new.akt = 1 and (old.akt is null or old.akt <> 1 or
     new.ldata <> old.ldata or new.bdata <> old.bdata)) then begin
    akt = 0;
    select status, planstype, prdepart
     from plans p where p.ref = new.plans into :akt, :planstype, :plansdepart;
    if(:akt is not null and :akt = 1) then begin
      plansref = 0;
      select first 1 p.name, p.ref
       from plans p
       join planscol pc on (p.ref = pc.plans)
       join planstypes pt on (p.planstype = pt.symbol)
       where p.status = 1 and p.planstype = :planstype
             and ((p.prdepart is null and :plansdepart is null) or
                  (:plansdepart is not null and p.prdepart = :plansdepart))
             and (pc.bdata >= new.bdata and pc.bdata < new.ldata or
                  pc.ldata > new.bdata and pc.ldata <= new.ldata or
                  pc.bdata <= new.bdata and pc.ldata >= new.ldata)
             and pc.ref <> new.ref
       into :plansname, :plansref;
       if (:plansref is not null and :plansref > 0) then
         exception PLANS_OKRES_NOT_ALLOW 'Daty kolumny pokrywają sie z datami planu: '||:plansname;
    end
  end
end^
SET TERM ; ^
