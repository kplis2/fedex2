--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRPOS_AD_PVALUE FOR EPRPOS                         
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  if (old.ecolumn = 9000) then
    update eprempl set pvalue = null where employee = old.employee and epayroll = old.payroll;
end^
SET TERM ; ^
