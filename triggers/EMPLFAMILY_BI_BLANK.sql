--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLFAMILY_BI_BLANK FOR EMPLFAMILY                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.familyaid is null) then
    new.familyaid = 0;

  if (new.careaid is null) then
    new.careaid = 0;

  if (new.insurance is null) then
    new.insurance = 0;

  if (new.inmanage is null) then
    new.inmanage = 1;

end^
SET TERM ; ^
