--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZSAD_BU0 FOR POZSAD                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable stawkavat numeric(14,2);
declare variable okres varchar(6);
BEGIN
  if(new.opis is null) then new.opis = '';
  if(new.podstawa is null) then new.podstawa = 0;
  if(new.grupacla is null) then new.grupacla = '';
  if(new.stawkacla is null) then new.stawkacla = 0;
  if(new.vat is null) then new.vat = 0;
  if(new.wartnet is null) then new.wartnet = 0;
  if (new.clo is null) then new.clo = 0;
  if (new.clo = 0) then
    new.clo = round(new.podstawa*(new.stawkacla/100.00));
  if (new.wartnet = 0) then new.wartnet = new.podstawa+new.clo;
  if (new.vat = 0) then
  begin
    select STAWKA from VAT where GRUPA = new.GR_VAT into :stawkavat;
    select NAGFAK.OKRES from NAGFAK where NAGFAK.REF = new.faktura into :okres;
    if (okres < '200601') then
      new.vat = cast( new.wartnet * (:stawkavat / 100) as numeric(14, 1));
    else
      new.vat = round(new.wartnet * (:stawkavat / 100));
  end
  new.clovat = new.clo + new.vat;

  if(new.ppodstawa is null) then new.ppodstawa = 0;
  if(new.pgrupacla is null) then new.grupacla = '';
  if(new.pstawkacla is null) then new.pstawkacla = 0;
  if(new.pclo is null) then new.pclo = 0;
  if(new.pwartnet is null) then new.pwartnet = 0;
  if(new.pgr_vat is null) then new.pgr_vat = '';
  if(new.pvat is null) then new.pvat = 0;
  new.pclovat = new.pclo + new.pvat;
END^
SET TERM ; ^
