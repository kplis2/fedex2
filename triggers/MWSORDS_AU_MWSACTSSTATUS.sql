--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSORDS_AU_MWSACTSSTATUS FOR MWSORDS                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable closemode smallint;
begin
  -- GDY zlecenie nei jest zamykane recznie to status sie nie propaguje na pozycje
  -- przeciwnie wynika z nich - dlatego jest exit z trigera !!!!!  e
  select mwsordtypes.closemode
    from mwsordtypes
    where ref = new.mwsordtype
    into closemode;
  if (closemode <> 0 and new.status <> old.status) then
  begin
    update mwsacts set mwsordstatus = new.status where mwsord = new.ref
      and mwsordstatus <> new.status;
    -- jezeli potwierdzamy zlecenie i jest tryb automatyczny to anulujemy wszystkie
    -- pozycje pozostale do realizacji na tym zleceniu
    if (new.status = 5 and old.status <> 5) then
      update mwsacts set status = 6 where mwsord = new.ref and status < 5;
    exit;
  end
  -- zmiana statusow na pozycjach zlecenia
  -- odakceptowanie
  if (new.status <> old.status and new.status = 0) then
    update mwsacts set status = new.status, mwsordstatus = new.status where mwsord = new.ref
      and (status = 1 or status = 3 or status = 4 or (status = 6 and statusblock = 0));
  -- akceptacja
  if (new.status <> old.status and new.status = 1) then
    update mwsacts set status = new.status, mwsordstatus = new.status where mwsord = new.ref
      and (status = 0 or status = 3 or status = 4) and mwsordtypedest <> 2; -- przyjecia maja miec pozycje niezaakceptowane.
  --- potwierdzenie
  --WYSTARCZY PRZY POTWIERDZENIU ZMIENIC ILOSC POTWIERDZONA I STATUS NA 2 - reszta na trigerach
  if (new.status <> old.status and new.status = 5) then begin
    update mwsacts set status = 2, quantityc = quantity, mwsordstatus = 2
      where mwsord = new.ref
      and (status = 1 or status = 2);
  end
  -- anulowanie
  if (new.status <> old.status and new.status = 6) then
    update mwsacts set status = new.status, mwsordstatus = new.status where mwsord = new.ref
      and (status = 0 or status = 1 or status = 3 or status = 4);
  if (new.mwsordtype <> old.mwsordtype or new.mwsordtypedest <> old.mwsordtypedest) then
    update mwsacts set mwsordtype = new.mwsordtype, mwsordtypedest = new.mwsordtypedest
      where mwsord = new.ref;
end^
SET TERM ; ^
