--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHORTAGES_AD_HEADER FOR PRSHORTAGES                    
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  if (coalesce(old.headergroup,0) > 0) then
    delete from prshortages where headergroup = old.headergroup;
end^
SET TERM ; ^
