--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFZADAN_BI FOR DEFZADAN                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  if(new.typpkt=0) then new.typpkt = NULL;
  if(new.domyslne is null) then new.domyslne = 0;
  if(new.domyslne = 1) then
    update defzadan set domyslne = 0 where domyslne = 1 and ref<>new.ref;
  if (new.redagopiswyk is null) then new.redagopiswyk = 0;
END^
SET TERM ; ^
