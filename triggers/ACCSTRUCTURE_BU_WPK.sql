--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTURE_BU_WPK FOR ACCSTRUCTURE                   
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable slodef_company companies_id;
declare variable bksymbol bksymbol_id;
declare variable patern_slodef slo_id;
declare variable dest_company companies_id;
begin
  -- Koleujno odpięto synchronizacje na bkyear, bkaccounts, accstructure, a teraz
  -- odpinamy na slodefie
  if (coalesce(old.pattern_ref,0) = 1 and coalesce(new.pattern_ref,0) = 0) then
  begin
    slodef_company = null;
    select s.company
      from slodef s
      where s.ref = new.dictdef
      into :slodef_company;
    -- Jeżeli slodef.company = null znaczy to, ze dany słownik jest wspolny dla
    -- wszystkich company. Co za tym idzie slownik ktory jest modyfikowany tylko we wzorcu
    -- nagle jak sie odepnie synchronizacje bedzie do edycji w fimrie zaleznej. Zeby uniknac
    -- tego karygodnego bledu nalezaloby skopiowac slownik i zrobic go zaleznym od obecnego company, ale
    -- trzeba by znalezc wszystkie zapisy, odksiegowac, pozmieniac slownik analityczny i zaksiegowac ( o ile sie uda)
    -- dlatego, nie jest to robione z automatu inalezy zrobic to recznie. NIE POLECAMY IMPLEMENTACJI TEGO L.G.
    if (slodef_company is distinct from null) then
    begin
      select b.symbol
        from bkaccounts b
        where b.ref = new.bkaccount
        into :bksymbol;
      exception slodef_sync_off' '||bksymbol;
    end else begin
      update slodef
        set patternref= null
        where ref = new.dictdef;
    end
  end else if (new.pattern_ref is distinct from null) then
  begin
    --jesteśmy w sytuacji, gdzie poziom analityki podlega pod synchronizacje ze wzorcem
    --zatem trzeba zbadac, czy jest to wewnetrzny update pochodzacy z wzorca czy nie
    if (coalesce(new.internalupdate,0)=0 ) then
    begin
      --przypadek jak nie jest to update wewntrzny wiec synchronizuj z wzorcem
      select nlevel, separator, dictdef, dictfilter, len, ord, doopisu
        from accstructure
        where ref = new.pattern_ref
        into new.nlevel, new.separator, new.dictdef, new.dictfilter, new.len, new.ord, new.doopisu;
    end
      -- sytuacja gdzie update był internal wiec zakladam ze wartosci ok, tylko sciagam flage internala,
      -- lub nie by internal ale wartoci sie podczytay ze wzorca, tylko trzeba jeszcze sprawdzić
      -- czy slownik nie jest indywidualny i ew. go skopiować lub odszukac.
      new.internalupdate = 0;
      -- trzeba by teraz skopiować slodefy jak są per company, lub dowiązania
      slodef_company = null;
      select s.company
        from slodef s
        where s.ref = new.dictdef
        into :slodef_company;
      if (slodef_company is distinct from null) then
      begin
        patern_slodef = new.dictdef;
        select b.company
          from bkaccounts b
          where b.ref = new.bkaccount
          into :dest_company;
        --okazuje sie, slownik jest dopiety do firmy wiec musimy zrobic kopie slownika
        execute procedure copy_slodef_slopoz(new.dictdef,null,:dest_company)
          returning_values new.dictdef;
        -- a na koniec dopiać synchronizacje do niego
        update slodef s
          set s.patternref = :patern_slodef
          where s.ref = new.dictdef;
     end
  end
end^
SET TERM ; ^
