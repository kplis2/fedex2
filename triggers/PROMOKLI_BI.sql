--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROMOKLI_BI FOR PROMOKLI                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
 if(new.grupakli > 0) then
    select grupykli.opis from GRUPYKLI where grupykli.ref = new.grupakli into new.nazwa;
  else if(new.klient > 0) then
    select klienci.nazwa from KLIENCI where ref=new.klient into new.nazwa;

-- BS34153
  if (coalesce(new.klient, 0) > 0 and exists(select first 1 1 from promokli where klient = new.klient and promocja = new.promocja)) then
    exception universal 'Ten klient jest już na liscie uprawnionych';

  if (coalesce(new.grupakli, 0) > 0 and exists(select first 1 1 from promokli where grupakli = new.grupakli and promocja = new.promocja)) then
    exception universal 'Ta grupa klientów jest już na liscie uprawnionych';
-- end
END^
SET TERM ; ^
