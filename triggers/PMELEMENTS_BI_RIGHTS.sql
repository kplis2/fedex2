--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMELEMENTS_BI_RIGHTS FOR PMELEMENTS                     
  ACTIVE BEFORE INSERT POSITION 1 
AS
begin
  select READRIGHTS,WRITERIGHTS,READRIGHTSGROUP,WRITERIGHTSGROUP
  from PMPLANS where REF=new.pmplan
  into new.readrights,new.writerights,new.readrightsgroup,new.writerightsgroup;
end^
SET TERM ; ^
