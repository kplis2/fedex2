--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRGANTTTIMES_BI_REF FOR PRGANTTTIMES                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null) then
    execute procedure gen_ref('PRGANTTTIMES') returning_values new.ref;
end^
SET TERM ; ^
