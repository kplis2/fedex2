--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAK_AI_KOSZTKAT FOR POZFAK                         
  ACTIVE AFTER INSERT POSITION 3 
AS
declare variable kosztkat numeric(14,2);
declare variable kosztzak numeric(14,2);
declare variable pkosztzak numeric(14,2);
declare variable wart_koszt numeric(14,2);
begin
  -- przenos koszty z pozycji faktury na naglowek
  if(new.kosztkat<>0 or new.kosztzak<>0 or new.pkosztzak<>0 or new.wart_koszt<>0) then begin
    select sum(KOSZTKAT), sum(KOSZTZAK), sum(PKOSZTZAK), sum(WART_KOSZT)
      from POZFAK where DOKUMENT = new.dokument
      into :kosztkat,:kosztzak,:pkosztzak,:wart_koszt;
    if(:kosztkat is null) then kosztkat = 0;
    if(:kosztzak is null) then kosztzak = 0;
    if(:pkosztzak is null) then pkosztzak = 0;
    if(:wart_koszt is null) then wart_koszt = 0;
    update NAGFAK set KOSZTKAT = :kosztkat, KOSZT = :kosztzak, PKOSZT = :pkosztzak, WART_KOSZT = :wart_koszt
      where NAGFAK.ref = new.dokument and
      ((coalesce(NAGFAK.KOSZTKAT,0) <> :kosztkat) or (coalesce(NAGFAK.KOSZT,0) <> :kosztzak) or (coalesce(NAGFAK.PKOSZT,0) <> :pkosztzak) or (coalesce(NAGFAK.WART_KOSZT,0) <> :wart_koszt));
  end
end^
SET TERM ; ^
