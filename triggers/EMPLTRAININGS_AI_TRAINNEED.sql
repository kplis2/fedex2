--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLTRAININGS_AI_TRAINNEED FOR EMPLTRAININGS                  
  ACTIVE AFTER INSERT POSITION 0 
AS
  declare variable trainneed integer;
begin
  select trainneed
    from etrainings
    where ref = new.training
    into :trainneed;

  if (trainneed is not null) then
    update empltrainneeds N
      set N.etraining = new.training
      where N.etrainneed = :trainneed and N.employee = new.employee;
end^
SET TERM ; ^
