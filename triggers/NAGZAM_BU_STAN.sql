--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGZAM_BU_STAN FOR NAGZAM                         
  ACTIVE BEFORE UPDATE POSITION 3 
as
declare variable status integer;
declare variable pozref integer;
declare variable KTM varchar(40);
declare variable WERSJA integer;
declare variable dostawa integer;
declare variable cenamag numeric(14,4);
begin
   --wlaczenie/wylaczenie zamowienia z glownego harmonogramu
   if(new.onmainschedule = 1 and old.onmainschedule = 0) then begin
     --zdjecie blokad z pozycji
     for select POZZAM.REF, POZZAM.KTM, POZZAM.wersja, POZZAM.cenamag, pozzam.dostawamag
     from POZZAM where ZAMOWIENIE = new.ref
     order by NUMER
     into :pozref, :ktm, :wersja, :cenamag, :dostawa
     do begin
       execute procedure REZ_SET_KTM(old.ref, :pozref, :ktm, :wersja, 0, :cenamag, :dostawa, 'W',old.termdost)
        returning_values :status;
       if(:status <> 1) then exception REZ_WRONG;
     end
     --zdjecie blokad naglowka
     execute procedure REZ_SET_KTM(old.ref, 0, old.kktm, old.kwersja, 0, 0, 0, 'W',old.termdost)
       returning_values :status;
     if(:status <> 1) then exception REZ_WRONG;
   end
end^
SET TERM ; ^
