--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACH_BIU_BALANCES FOR ROZRACH                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (abs(new.winien) > abs(new.ma)) then
    new.baldebit = new.winien - new.ma;
  else
    new.baldebit = 0;
  if (abs(new.ma) > abs(new.winien)) then
    new.balcredit = new.ma - new.winien;
  else
    new.balcredit = 0;
end^
SET TERM ; ^
