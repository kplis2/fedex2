--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWAKCES_AU FOR TOWAKCES                       
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable awersjaref integer;
begin
  -- aktualizacja powiazań pomiedzy towarami
  if ( (new.awersjaref <> old.awersjaref or new.typ <> old.typ
       or new.akcesdegree <> old.akcesdegree or new.doubleakc <> old.doubleakc)
       and new.doubleakc = 1 and new.token <> 7) then begin
    if ( exists(select ref from towakces where towakces.aktm = new.ktm
        and towakces.ktm = new.aktm) ) then
      update towakces set towakces.doubleakc = 2 where towakces.ref = old.ref;
    delete from towakces where towakces.ktm = old.aktm and towakces.aktm = old.ktm;
    if ( not exists(select ref from towakces where towakces.aktm = new.ktm
        and towakces.ktm = new.aktm) ) then begin
      select wersje.ref from wersje where wersje.ktm = new.ktm and wersje.nrwersji = 0
        into :awersjaref;
      insert into towakces (KTM, AKTM, AWERSJA, AWERSJAREF, TYP, OPIS, REQUIRED,
                            AKCESDEGREE, DOUBLEAKC, TOWAKCESGRP)
                     values(new.aktm, new.ktm, 0, :awersjaref, new.typ, new.opis, new.required,
                            new.akcesdegree, new.doubleakc, new.towakcesgrp);
    end
  end
end^
SET TERM ; ^
