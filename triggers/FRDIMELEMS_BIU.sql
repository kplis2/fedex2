--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDIMELEMS_BIU FOR FRDIMELEMS                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable status smallint;
begin
  select frhdrs.status
    from frdimhier
      join frhdrs on (frhdrs.symbol = frdimhier.frhdr)
    where frdimhier.ref = new.frdimhier
    into :status;
  if (new.shortname = '' or new.shortname is null) then
    select dimelemdef.shortname from dimelemdef
      where dimelemdef.ref = new.dimelem into new.shortname;

  if(coalesce(new.readrights,'')='') then new.readrights = ';';
  if(coalesce(new.readrightsgroup,'')='') then new.readrightsgroup = ';';
end^
SET TERM ; ^
