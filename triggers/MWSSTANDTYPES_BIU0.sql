--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDTYPES_BIU0 FOR MWSSTANDTYPES                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.levelsquant is null) then new.levelsquant = 0;
  if (new.mwsstandsegq is null) then new.mwsstandsegq = 0;
  if (new.w is null) then new.w = 0;
  if (new.l is null) then new.l = 0;
  if (new.segtype is null) then new.segtype = 1;
end^
SET TERM ; ^
