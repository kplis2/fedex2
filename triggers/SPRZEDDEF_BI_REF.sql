--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDDEF_BI_REF FOR SPRZEDDEF                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SPRZEDDEF')
      returning_values new.REF;
end^
SET TERM ; ^
