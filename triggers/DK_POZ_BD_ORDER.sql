--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DK_POZ_BD_ORDER FOR DK_POZ                         
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then exit;
  update dk_poz set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and dkdok = old.dkdok;
end^
SET TERM ; ^
