--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECALDAYS_AI_EMPLCALDAYS FOR ECALDAYS                       
  ACTIVE AFTER INSERT POSITION 0 
AS
  declare variable fromdate date;
  declare variable employee integer;
  declare variable calendar integer;
begin
--Insert dni do kalendarzy pracowniczych, przy dodaniu dni do kalend. podstawowego

  for
    select distinct employee
      from emplcalendar
      where calendar = new.calendar
        and fromdate <= new.cdate
        and (new.cdate <= todate or todate is null)
      into :employee
  do begin
    insert into emplcaldays (employee, cdate, daykind, descript, workstart,
      workend, nomworktime, ecalday,autosync, ecalendar)
    values (:employee, new.cdate, new.daykind, new.descript, new.workstart,
      new.workend, new.worktime, new.ref, 1, new.calendar);
  end
end^
SET TERM ; ^
