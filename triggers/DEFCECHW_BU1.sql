--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHW_BU1 FOR DEFCECHW                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable ilosc integer;
begin
ilosc = 0;
  if(
    (old.wartstr is not NULL and new.wartstr<>old.wartstr)
    or coalesce(old.kod,'') != coalesce(new.kod,'')
  ) then begin
    if(new.zmien is null or (new.zmien=0)) then begin
      select count(*) from ATRYBUTY where CECHA = old.cecha and WARTOSC = old.wartstr and kod = old.kod into :ilosc;  --dodano KOD
      if(ilosc>0) then exception DEFCECHW_ATR_WART2;
    end else begin
      update atrybuty set wartosc=new.wartstr, kod = new.kod where cecha=old.cecha and wartosc=old.wartstr and kod = old.kod;    --dodano KOD
      new.zmien = 0;
    end
  end
ilosc = 0;
  if(((new.kod is not null and old.kod is null) or (new.kod<>old.kod)) and new.kod<>'') then begin
    exception universal 'Pawel M.';
    select count(*) from DEFCECHW
    where CECHA=new.cecha and KOD=new.kod and id_defcechw<>new.id_defcechw
    into :ilosc;
    if(ilosc>0) then exception DEFCECHW_KOD;
  end
end^
SET TERM ; ^
