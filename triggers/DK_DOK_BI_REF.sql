--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DK_DOK_BI_REF FOR DK_DOK                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DK_DOK')
      returning_values new.REF;
end^
SET TERM ; ^
