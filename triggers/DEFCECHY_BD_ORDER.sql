--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHY_BD_ORDER FOR DEFCECHY                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update defcechy set ord = 0, lp = lp - 1
    where symbol <> old.symbol and lp > old.lp and grupa = old.grupa;
end^
SET TERM ; ^
