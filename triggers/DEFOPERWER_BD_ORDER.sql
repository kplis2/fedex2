--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERWER_BD_ORDER FOR DEFOPERWER                     
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update defoperwer set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and operacja = old.operacja;
end^
SET TERM ; ^
