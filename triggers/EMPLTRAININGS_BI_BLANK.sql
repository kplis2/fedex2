--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLTRAININGS_BI_BLANK FOR EMPLTRAININGS                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  select status
    from etrainings
    where ref = new.training
    into new.status;

  if (new.status = 1) then
    select todate
      from etrainings
      where ref = new.training
      into new.compldate;
end^
SET TERM ; ^
