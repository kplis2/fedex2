--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_BU_REPLICAT_FAKTURA FOR DOKUMNAG                       
  ACTIVE BEFORE UPDATE POSITION 1 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if(((new.ref <> old.ref)
      or (new.faktura <> old.faktura) or (new.faktura is not null and old.faktura is null) or (new.faktura is null and old.faktura is not null)
      or (new.symbolfak <> old.symbolfak) or (new.symbolfak is not null and old.symbolfak is null) or (new.symbolfak is null and old.symbolfak is not null)
      or (new.ref2 <> old.ref2) or (new.ref2 is not null and old.ref2 is null) or (new.ref2 is null and old.ref2 is not null)
     ) and ((new.akcept =1) or (new.akcept = 8))
  ) then
   waschange = 1;

  execute procedure rp_trigger_bu('DOKUMNAG',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
