--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INTRASTATP_BU FOR INTRASTATP                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable varsumvalue numeric(14,4);
  declare variable varsumvaluestat numeric(14,4);
  declare variable varposcount integer;
  declare variable intrastathstatus smallint;

  declare variable oldvarsumvalue numeric(14,4);
  declare variable oldvarsumvaluestat numeric(14,4);
  declare variable oldvarposcount integer;
  declare variable oldintrastathstatus smallint;
begin
  select coalesce(ith.sumval,0), coalesce(ith.sumvalstat,0), coalesce(ith.poscount,0), coalesce(ith.statusflag,0)
    from intrastath ith
    where new.intrastath = ith.ref
  into  :varsumvalue, :varsumvaluestat, :varposcount, :intrastathstatus;

  select coalesce(oldith.sumval,0), coalesce(oldith.sumvalstat,0), coalesce(oldith.poscount,0), coalesce(oldith.statusflag,0)
    from intrastath oldith
    where old.intrastath = oldith.ref
  into  :oldvarsumvalue, :oldvarsumvaluestat, :oldvarposcount, :oldintrastathstatus;

  if(:intrastathstatus <> 0 or :oldintrastathstatus <> 0) then
    exception intrastat_docum_notopened;
  else begin
    if (old.intrastath = new.intrastath) then begin     -- aktualizacja w ramach tego samego intrastatu
      if(new.statusflag is null) then new.statusflag = 0;
     if(old.val <> new.val or old.valstat <> new.valstat) then begin
        if(old.val <> new.val) then begin
          if (old.val > new.val) then
            varsumvalue = :varsumvalue - (old.val - new.val);
          else
            varsumvalue = :varsumvalue + (new.val - old.val);
        end
        if(old.valstat <> new.valstat) then begin
          if (old.valstat > new.valstat) then
            varsumvaluestat = :varsumvaluestat - (old.valstat - new.valstat);
          else
            varsumvaluestat = :varsumvaluestat + (new.valstat - old.valstat);
        end
        update intrastath ith set
          ith.sumval = :varsumvalue,
          ith.sumvalstat = :varsumvaluestat
        where ith.ref = new.intrastath;
      end
    end else begin                                      -- przeniesienie pozycji na inny intrastat
      if(new.statusflag is null) then new.statusflag = 0;
      varsumvalue = :varsumvalue + new.val;
      varsumvaluestat = :varsumvaluestat + new.valstat;
      varposcount = :varposcount + 1;

      update intrastath ith set
        ith.sumval = :varsumvalue,
        ith.sumvalstat = :varsumvaluestat,
        ith.poscount = :varposcount
      where ith.ref = new.intrastath;

      oldvarsumvalue = :oldvarsumvalue - old.val;
      oldvarsumvalue = :oldvarsumvalue - old.valstat;
      oldvarposcount = :oldvarposcount - 1;

      update intrastath oldith set
        oldith.sumval = :oldvarsumvalue,
        oldith.sumvalstat = :oldvarsumvaluestat,
        oldith.poscount = :oldvarposcount
      where oldith.ref = old.intrastath;
    end
  end
end^
SET TERM ; ^
