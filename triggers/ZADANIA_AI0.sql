--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZADANIA_AI0 FOR ZADANIA                        
  ACTIVE AFTER INSERT POSITION 0 
as
begin
  if(new.ckontrakt is not null) then execute procedure CKONTRAKTY_UPDATE_CPODMIOTYZAD(new.ckontrakt);
  if(new.wfworkflow is not null) then begin
    execute procedure wfworkflow_status_set(new.wfworkflow);
    insert into dokzlacz(ztable,zdokum,dokument)
      select 'ZADANIA',new.ref,d.dokument
        from wfworkflows w
        left join dokzlacz d on d.ztable = 'WFWORKFLOWS' and d.zdokum = w.ref
        left join dokzlacz dz on dz.ztable = 'ZADANIA' and dz.zdokum = new.ref
      where w.ref = new.wfworkflow and dz.ref is null;
  end
end^
SET TERM ; ^
