--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TYPFAK_AU0 FOR TYPFAK                         
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
 if(new.korekta <> old.korekta or (new.sad <> old.sad)) then update STANTYPFAK set KOREKTA = new.korekta, SAD = new.sad where TYPFAK = new.SYMBOL;
end^
SET TERM ; ^
