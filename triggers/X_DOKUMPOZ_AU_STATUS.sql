--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_DOKUMPOZ_AU_STATUS FOR DOKUMPOZ                       
  ACTIVE AFTER UPDATE POSITION 101 
as
declare variable grupasped dokumnag_id;
begin
--sluzy do zmiany statusow magazynowch w tabelach przejsciowych
  -- oraz do zmiany kolorowania dokumentow WZ
  --0 czarny inne
  --1 czerwony nie wszystko wygenerowane
  --2 oliwkowy wszystko wygenerowane ale nie wszystko zlozone

  --czy wszystkie pozycje z dokumentu zostaly wygenerowane
  if (new.ilosconmwsacts <> old.ilosconmwsacts) then
  begin
    if (not exists(
        select p.ref
            from dokumpoz p
            where (p.iloscl - p.ilosconmwsacts ) <> 0
                    and p.ilosc <>0
                    and p.dokument = new.dokument)
            ) then
     begin
        update dokumnag set x_kolor = 2 where ref= new.dokument;
     end
  end
  if( new.ilosconmwsactsc <> old.ilosconmwsactsc and
      new.ilosconmwsactsc = new.ilosc
      ) then
  begin
    --czy wszystko zostalo zlozone z danego dokumentu
    if (not exists(
        select p.ref
            from dokumpoz p
            where (p.ilosc - p.ilosconmwsactsc ) <> 0
                    and p.ilosc <>0
                    and p.dokument = new.dokument)
            ) then
     begin
        update dokumnag d set d.x_kolor = 0 where ref= new.dokument;
     end
    --czy wszystko z danej grupy spedycyjnej zostalo zlozone
    select d.grupasped from dokumnag d where d.ref = new.dokument
        into :grupasped;
    if(not EXISTS(
        select p.ref from mwsacts a
            join dokumpoz p on  (a.docposid = p.ref)
            join dokumnag d on (d.grupasped = :grupasped and d.ref = p.dokument)
            where (p.ilosc - p.ilosconmwsactsc ) <> 0 and p.ilosc <>0)) then
    begin
        update x_imp_dokumnag xd set xd.mwsstatus = 5, xd.status = 12
            where EXISTS(
                 SELECT ref
                   FROM dokumnag dd
                   WHERE dd.grupasped = :grupasped and xd.ref = dd.x_imp_ref);
    end
  end
end^
SET TERM ; ^
