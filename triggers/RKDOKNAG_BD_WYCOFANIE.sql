--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_BD_WYCOFANIE FOR RKDOKNAG                       
  ACTIVE BEFORE DELETE POSITION 0 
as
  declare variable getnumdok varchar(40);
  declare variable BLOCKREF integer;
  declare variable token integer;
begin
  select token
    from rkrapkas
  where ref = old.raport
  into :token;
  if (token<>7) then begin
    if(OLD.numer > 0 ) then begin
      /*zwrot numeru tylko, kiedy nie było blokady*/
      select GENNUMDOK from RKSTNKAS where KOD = old.stanowisko into :getnumdok;
      execute procedure FREE_NUMBER(:getnumdok,old.stanowisko, old.typ, old.data, old.numer, old.numblockget) returning_values :blockref;
    end
    else if(old.numblockget > 0) then
      delete from numberblock where ref=old.numblockget;
  end
end^
SET TERM ; ^
