--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPLANS_AU_RIGHTS FOR PMPLANS                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if(new.readrights<>old.readrights or new.writerights<>old.writerights or
     new.readrightsgroup<>old.readrightsgroup or new.writerightsgroup<>old.writerightsgroup) then begin
    update PMELEMENTS set READRIGHTS=new.readrights, WRITERIGHTS=new.writerights,
      READRIGHTSGROUP=new.readrightsgroup, WRITERIGHTSGROUP=new.writerightsgroup
      where PMPLAN=new.ref;
    update PMPOSITIONS set READRIGHTS=new.readrights, WRITERIGHTS=new.writerights,
      READRIGHTSGROUP=new.readrightsgroup, WRITERIGHTSGROUP=new.writerightsgroup
      where PMPLAN=new.ref;
    update PMACTIVS set READRIGHTS=new.readrights, WRITERIGHTS=new.writerights,
      READRIGHTSGROUP=new.readrightsgroup, WRITERIGHTSGROUP=new.writerightsgroup
      where PMPLAN=new.ref;
    update PMPLANCHANGES set READRIGHTS=new.readrights, WRITERIGHTS=new.writerights,
      READRIGHTSGROUP=new.readrightsgroup, WRITERIGHTSGROUP=new.writerightsgroup
      where PMPLAN=new.ref;
    update PMPLANPERSONS set READRIGHTS=new.readrights, WRITERIGHTS=new.writerights,
      READRIGHTSGROUP=new.readrightsgroup, WRITERIGHTSGROUP=new.writerightsgroup
      where PMPLAN=new.ref;
  end
end^
SET TERM ; ^
