--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWORKCATPOS_BIU_CHK FOR EWORKCATPOS                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin

 new.topay = coalesce(new.topay,0);
 new.frompay = coalesce(new.frompay,0);

 if (new.topay < new.frompay) then
   exception WORKCAT_WRONGRANGE;

 if (exists (select  first 1 1 from EWORKCATPOS
               where (((old.nrarab <> new.nrarab or old.nrarab is null) and nrarab = new.nrarab)
                    or((old.nrroma <> new.nrroma or old.nrroma is null) and nrroma = new.nrroma))
                 and workcat = new.workcat)) then
   exception WORKCAT_DOUBLE_NR;

 if (exists (select  first 1 1 from eworkcats where israngectrl = 1 and ref = new.workcat) and
     exists (select  first 1 1 from EWORKCATPOS
              where (((new.topay <= topay and new.topay >= frompay) or (new.frompay <= topay and new.frompay >= frompay))
                  or (new.topay > topay and frompay > new.frompay) or (new.frompay < frompay and topay < new.topay))
                and workcat = new.workcat
                and old.ref is null
                and new.isactive = 1
                and isactive = 1)) then
   exception WORKCAT_OVERLAP;
end^
SET TERM ; ^
