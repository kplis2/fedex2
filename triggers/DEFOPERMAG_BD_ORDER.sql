--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERMAG_BD_ORDER FOR DEFOPERMAG                     
  ACTIVE BEFORE DELETE POSITION 0 
AS
  declare variable maxnum integer;
begin
  if (old.ord = 0) then exit;
  update defopermag set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and operacja = old.operacja;
end^
SET TERM ; ^
