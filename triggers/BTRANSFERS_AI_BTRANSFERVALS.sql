--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BTRANSFERS_AI_BTRANSFERVALS FOR BTRANSFERS                     
  ACTIVE AFTER INSERT POSITION 0 
AS
  declare variable akronim varchar(20);
  declare variable wart varchar(255);
begin
  --DS: ustawienie odpowiednich wartosci w parametrach przelewu
  --takie same wartosci wykorzystywane sa w btransfers
  for
    select coalesce(b.akronim,''), b.wartdom
      from bankacc a
        left join bankacctyppar b on(a.trantype = b.bankacctype)
      where a.symbol = new.bankacc
    into :akronim, :wart
  do begin
    if (akronim = 'TYP') then
    begin
      if (new.btype = 'ZUS') then
        wart = 2;
      else if (new.btype = 'POD') then
        wart = 1;
    end
    insert into btransfervals (btransfer, akronim, wartosc)
      values (new.ref, :akronim, :wart);
  end
end^
SET TERM ; ^
