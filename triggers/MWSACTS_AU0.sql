--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_AU0 FOR MWSACTS                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable mwsordtype integer;
declare variable grupasped integer;
begin
  -- obsluga zmiany statusow operacji
  -- zmiana statusu tylko dla nieinwentaryzacyjnych powoduje generowanie rozpiski
  -- do operacji zlecenia
  if (new.status <> old.status) then
  begin
    select mwsordtype from mwsords where ref = new.mwsord into mwsordtype;
    --exception universal :mwsordtype||' 1 '||new.mwsord||' 1 '||new.ref||' 1 '||old.status;
    execute procedure MWSACTS_STATUSCHANGE(:mwsordtype, new.mwsord, new.ref ,old.status);
  end
  if (new.cortomwsact is not null and new.quantity <> old.quantity and new.status <> 6) then
    execute procedure MWS_MWSACT_CALC_QUANTITYL(new.cortomwsact);
  if (old.cortomwsact is not null and new.status = 6 and old.status <> 6) then
    execute procedure MWS_MWSACT_CALC_QUANTITYL(old.cortomwsact);
  if (new.cortomwsact <> old.cortomwsact and new.cortomwsact is not null and old.cortomwsact is not null) then
  begin
    if (old.cortomwsact is not null) then
      execute procedure MWS_MWSACT_CALC_QUANTITYL(old.cortomwsact);
    if (new.cortomwsact is not null) then
    execute procedure MWS_MWSACT_CALC_QUANTITYL(new.cortomwsact);
  end
  if (new.mwsordtypedest in (11,2) and new.status > 0 and new.status < 6
      and old.status in (0,6) and new.segtype = 2 and new.mwsconstlocl is not null
  ) then
  begin
    execute procedure mws_check_mwsconstlocsizes(new.mwsconstlocl, 1);
  end
  if (new.status <> old.status and new.docid is not null) then
  begin
    select coalesce(g.grupasped, g.ref) from dokumnag g where g.ref = new.docid
      into grupasped;
   -- execute procedure dokumnag_mwsrealstatus(grupasped,0);
  end

  if (new.mwsordtypedest = 2 and new.frommwsact is not null and new.quantity <> old.quantity) then
  begin
    update mwsacts set quantity = quantity - new.quantity + old.quantity
      where ref = new.frommwsact
        and status = 0;
  end
end^
SET TERM ; ^
