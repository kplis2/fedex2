--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INTERESTS_BU0 FOR INTERESTS                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.yeardays is null) then new.yeardays = 0;
  if(new.yeardays <=0) then exception INTERESTS_WRONGILDNI;
  if(new.ratey is null) then new.ratey = 0;
  if((new.ratey <> old.ratey) or (new.yeardays <> old.yeardays) )then begin
    new.ratem = new.ratey / 12;
    new.rated = new.ratey / new.yeardays;
  end

end^
SET TERM ; ^
