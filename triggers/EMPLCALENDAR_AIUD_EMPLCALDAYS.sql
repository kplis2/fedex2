--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCALENDAR_AIUD_EMPLCALDAYS FOR EMPLCALENDAR                   
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 5 
as
declare variable employee integer;
begin
/*MWr: Generowanie dni kalendarza praowniczego. Data '3000-01-01' sluzy do
      wywolania triggera podczas zmiany dat konca lub usuniecia umowy pracow.*/

   if (inserting or deleting or new.fromdate <> old.fromdate
     or new.calendar <> old.calendar or new.todate = '3000-01-01') then
   begin
     if (deleting) then employee = old.employee;
     else employee = new.employee;
     execute procedure e_gen_emplcaldays(:employee);
   end
end^
SET TERM ; ^
