--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDESPOS_BI_REF FOR PRSCHEDGUIDESPOS               
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('PRSCHEDGUIDESPOS') returning_values new.ref;
end^
SET TERM ; ^
