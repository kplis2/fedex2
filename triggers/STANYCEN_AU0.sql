--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYCEN_AU0 FOR STANYCEN                       
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable sumcen decimal(14,4);
declare variable sumil decimal(14,4);
declare variable isstan smallint;
begin
  select count(*) from STANYIL where MAGAZYN=new.magazyn and ktm = new.ktm and wersja = new.wersja into :isstan;
  if(:isstan is null) then isstan = 0;
  if(:isstan = 0) then
    insert into STANYIL(MAGAZYN,KTM,WERSJA,ILOSC,CENA, WARTOSC) values(new.magazyn, new.ktm, new.wersja, new.ilosc, new.cena, new.wartosc);
  else begin
     /* obliczenie średniej ceny */
     select sum(ilosc), sum(wartosc) from STANYCEN where MAGAZYN=new.MAGAZYN AND KTM = new.ktm AND WERSJA=new.wersja into :sumil, :sumcen;
    if(:sumil is null or (:sumil = 0)) then begin
       sumil = 1;
       sumcen = old.cena;
    end

     update STANYIL set cena=(:sumcen / :sumil ), ilosc = ilosc + new.ilosc - old.ilosc, wartosc = wartosc + new.wartosc - old.wartosc where
     magazyn = new.magazyn and ktm = new.ktm and wersja = new.wersja;
  end
-- BS39909
  if (coalesce(old.cena,0)<>coalesce(new.cena,0)) then begin
    update stanyrez r set r.cena = new.cena
      where r.zreal = 0
        and r.magazyn = new.magazyn
        and r.ktm = new.ktm
        and r.wersja = new.wersja
        and r.cena = old.cena
        and r.dostawa = new.dostawa;
  end
end^
SET TERM ; ^
