--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ETERMINATIONS_BI_REF FOR ETERMINATIONS                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF = 0) then
    execute procedure GEN_REF('ETERMINATIONS') returning_values new.REF;
end^
SET TERM ; ^
