--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFFORMP_AU_REPLICAT FOR DEFFORMP                       
  ACTIVE AFTER UPDATE POSITION 1 
as
begin
   if(
     new.numer <> old.numer or (new.numer is null and old.numer is not null) or (new.numer is not null and old.numer is null) or
     new.ident <> old.ident or (new.ident is null and old.ident is not null) or (new.ident is not null and old.ident is null) or
     new.typ <> old.typ or (new.typ is null and old.typ is not null) or (new.typ is not null and old.typ is null) or
     new.wartosc <> old.wartosc or (new.wartosc is null and old.wartosc is not null) or (new.wartosc is not null and old.wartosc is null) or
     new.wartdom <> old.wartdom or (new.wartdom is null and old.wartdom is not null) or (new.wartdom is not null and old.wartdom is null) or
     new.rozmiar <> old.rozmiar or (new.rozmiar is null and old.rozmiar is not null) or (new.rozmiar is not null and old.rozmiar is null) or
     new.tytul <> old.tytul or (new.tytul is null and old.tytul is not null) or (new.tytul is not null and old.tytul is null) or
     new.komentarz <> old.komentarz or (new.komentarz is null and old.komentarz is not null) or (new.komentarz is not null and old.komentarz is null) or
     new.blad <> old.blad or (new.blad is null and old.blad is not null) or (new.blad is not null and old.blad is null) or
     new.param <> old.param or (new.param is null and old.param is not null) or (new.param is not null and old.param is null) or
     new.paramtag <> old.paramtag or (new.paramtag is null and old.paramtag is not null) or (new.paramtag is not null and old.paramtag is null)
    )then
      update DEFFORMY set STATE=-2 where SYMBOL=old.forma;
    if(old.forma <> new.forma ) then
      update DEFFORMY set STATE=-2 where SYMBOL=old.forma or (SYMBOL = new.forma);
end^
SET TERM ; ^
