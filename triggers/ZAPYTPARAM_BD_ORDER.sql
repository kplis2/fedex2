--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZAPYTPARAM_BD_ORDER FOR ZAPYTPARAM                     
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update zapytparam set ord = 0, numer = numer - 1
    where (zapytanie <> old.zapytanie or typzap <> old.typzap or parametr <> old.parametr) and numer > old.numer and zapytanie = old.zapytanie;
end^
SET TERM ; ^
