--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_BI0 FOR MWSACTS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable quantityonlocation numeric(14,4);
begin
  new.regtime = current_timestamp(0);
  if (new.quantityl is null or new.quantityl = 0) then
    new.quantityl = new.quantity;
  if (new.status = 1) then
    new.accepttime = current_timestamp(0);
  if (new.timestartdecl is null) then
    new.timestartdecl = current_timestamp(0);
  if (new.mwsord is null) then exception MWSACT_WITH_NO_MWSORD;
  if (new.operator is null) then
    select operator from mwsords where ref = new.mwsord
      into new.operator;
  if (new.mwspallocsize is not null) then
    select s.l, s.w, s.h, s.segtype
      from mwspallocsizes s
      where s.ref = new.mwspallocsize
      into new.l, new.w, new.h, new.segtype;
  if (new.mwsordtypedest = 3 and new.mwsconstlocl is not null and new.vers is not null) then
  begin
    select sum(quantity)
      from mwsstock
      where mwsconstloc = new.mwsconstlocp and vers = new.vers
      into quantityonlocation;
    if (quantityonlocation is null) then quantityonlocation = 0;
    execute procedure xk_mws_loc_capacity(new.mwsconstlocl, new.vers, quantityonlocation, quantityonlocation)
      returning_values (new.quantityp);
  end
end^
SET TERM ; ^
