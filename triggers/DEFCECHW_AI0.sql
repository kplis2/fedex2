--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHW_AI0 FOR DEFCECHW                       
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable automat smallint;
declare variable katalog integer;
declare variable numer integer;
declare variable id_ref integer;
declare variable i integer;
declare variable typ smallint;
declare variable wartosc varchar(255);
declare variable cnt integer;
begin
   /* kontrola powtarzania wartości */
   select count(*) from DEFCECHW where CECHA = new.cecha and PREFIKS = new.prefiks and
     ( (WARTSTR = new.wartstr) or (WARTNUM = new.wartnum ) or (wartdate = new.wartdate)) and
     ID_DEFCECHW <> new.ID_DEFCECHW into :cnt;
   if(:cnt > 0) then
     exception DEFCECHW_VALUEPRESENT;
   for select AUTOMAT, REF from DEFKATAL where CECHA = new.cecha into :automat, :katalog
   do begin
     if(:automat is null ) then automat = 0;
     wartosc = null;
     if(:automat = 1) then begin
       numer = null;
       i = 1;
       /* okreslenie numeru wartosci */
       /*okreslenie typu wartosci */
       select  typ from DEFCECHY where symbol=new.cecha into :typ;
       if(:typ = 1 or (:typ = 0)) then begin
         for select id_defcechw from DEFCECHW  where CECHA = new.cecha order by WARTNUM into :id_ref do begin
           if(:id_ref = new.id_defcechw) then numer = i;
           i = i + 1;
         end
         wartosc = new.wartnum;
       end
       if(:typ = 2) then begin
         for select id_defcechw from DEFCECHW  where CECHA = new.cecha order by WARTSTR into :id_ref do begin
           if(:id_ref = new.id_defcechw) then numer = i;
           i = i + 1;
         end
         wartosc = new.wartstr;
       end
       if(:typ = 3 or (:typ = 4)) then begin
         for select id_defcechw from DEFCECHW  where CECHA = new.cecha order by WARTDATE into :id_ref do begin
           if(:id_ref = new.id_defcechw) then numer = i;
           i = i + 1;
         end
         wartosc = new.wartdate;
       end
       if(:numer is null)then numer = 1;
       /* numer wartosci okrelony */
        insert into DEFELEM(KATALOG, NUMER, NAZWA, WARTOSC, UKRYWANIE)
          values(:katalog, :numer, :wartosc, :wartosc, 0);
     end
   end
   if(coalesce(new.wartstr,'') = '' and (new.wartnum is not null or new.wartdate is not null)) then
   begin
     --new.wartstr = coalesce(new.wartnum,'') ||coalesce(new.wartdate,'');
   end
end^
SET TERM ; ^
