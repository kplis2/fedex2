--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHAREAS_AU_MWSCONSTLOCS FOR WHAREAS                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.coordxb <> old.coordxb or new.coordxe <> old.coordxe
      or new.coordyb <> old.coordyb or new.coordye <> old.coordye
      or new.number <> old.number or new.l <> old.l or new.w <> old.w
      or new.locnumber <> old.locnumber
  ) then
    update mwsconstlocs set coordxb = new.coordxb, coordxe = new.coordxe,
        coordyb = new.coordyb, coordye = new.coordye, whareanumber = new.number,
        l = new.l, w = new.w, locnumber = new.locnumber
      where wharea = new.ref;
end^
SET TERM ; ^
