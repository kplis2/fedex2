--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FARELDOCS_BI_CLOSED FOR FARELDOCS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable period_status integer;
  declare variable company integer;
begin
    select company
      from fxdassets f
      where f.ref = new.fxdasset
    into :company;

    select status from amperiods
    where ammonth = extract(month from new.docdate)
    and amyear = extract(year from new.docdate)
    and company =:company
    into :period_status;
    if (period_status > 0) then exception AMPERIOD_CLOSED 'Nie można wystawić dokumentu dla zamkniętego okresu!';
end^
SET TERM ; ^
