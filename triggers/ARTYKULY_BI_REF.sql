--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ARTYKULY_BI_REF FOR ARTYKULY                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ARTYKULY')
      returning_values new.REF;
end^
SET TERM ; ^
