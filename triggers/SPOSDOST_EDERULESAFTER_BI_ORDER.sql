--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPOSDOST_EDERULESAFTER_BI_ORDER FOR SPOSDOST_EDERULESAFTER         
  ACTIVE BEFORE INSERT POSITION 1 
as
  declare variable maxnum integer;
begin
  new.ord = 1;
  select max(number) from SPOSDOST_EDERULESAFTER where sposdost = new.sposdost
    into :maxnum;
  if (new.number is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.number = maxnum + 1;
  end else if (new.number <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update SPOSDOST_EDERULESAFTER set ord = 0, number = number + 1
      where number >= new.number and sposdost = new.sposdost;
  end else if (new.number > maxnum) then
    new.number = maxnum + 1;
end^
SET TERM ; ^
