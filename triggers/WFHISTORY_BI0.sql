--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WFHISTORY_BI0 FOR WFHISTORY                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  execute procedure get_global_param('AKTUOPERATOR') returning_values new.operator;
  if(new.operator is null) then exception universal 'Brak wskazania operatora na historii zmian obiegu dokumentów';
  new.operdate = current_timestamp(0);
end^
SET TERM ; ^
