--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_ATRYBUTY_BI_TOWARY FOR X_ATRYBUTY                     
  ACTIVE BEFORE INSERT POSITION 50 
AS
begin
    if (new.otable = 'WERSJE' and new.ktm <> '') then begin
        if (new.nazwacechy = 'X_MWS_TYP_LOKACJI') then
            update towary set x_mws_typ_lokacji = new.wartosc where ktm = new.ktm;
        if (new.nazwacechy = 'X_MWS_PARTIE') then
            update towary set X_MWS_PARTIE = new.wartosc where ktm = new.ktm;
        if (new.nazwacechy = 'X_MWS_SERIE') then
            update towary set X_MWS_SERIE = new.wartosc where ktm = new.ktm; 
        if (new.nazwacechy = 'X_MWS_SLOWNIK_EHRLE') then
            update towary set X_MWS_SLOWNIK_EHRLE = new.wartosc where ktm = new.ktm;
        if (upper( new.nazwacechy) = 'KOD_TOWARU_SES') then
            update towary set X_KOD_TOWARU_SES = new.wartosc where ktm = new.ktm;
    end
end^
SET TERM ; ^
