--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWS_DOKUMPOZ_AU_MWSDONE FOR DOKUMPOZ                       
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable mwsdoc smallint;
declare variable mwsdisposition smallint;
begin
  select mwsdoc, mwsdisposition
    from dokumnag where ref = new.dokument
    into :mwsdoc, :mwsdisposition;
  if (mwsdoc is null or mwsdoc = 0) then
    exit;
  if (new.mwsactsdone <> old.mwsactsdone) then
  begin
    if (not exists (
        select p.ref
          from dokumpoz p
            left join dokumnag d on (d.ref = p.dokument)
            left join towary t on (t.ktm = p.ktm)
          where p.dokument = new.dokument and t.usluga <> 1
            and case when d.mwsdisposition = 1 then p.ilosc else p.iloscl end - coalesce(p.ilosconmwsactsc,0) > 0 and p.mwsactsdone = 0)
    ) then
      update dokumnag set mwsdone = 1 where ref = new.dokument and mwsdone = 0;
    else
      update dokumnag set mwsdone = 0 where ref = new.dokument and mwsdone = 1;
  end

  /* Uzgodnienie ilosci na zamówieniu */
  if(new.ilosc <> old.ilosc and :mwsdisposition = 1) then
    execute procedure pozzam_obl(new.ref);
end^
SET TERM ; ^
