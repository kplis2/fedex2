--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRACTS_BI_SALARY FOR EMPLCONTRACTS                  
  ACTIVE BEFORE INSERT POSITION 2 
AS
begin
  if (new.salary = 0) then
  begin
     exception add_salary_to_emplcontracts;
  end
end^
SET TERM ; ^
