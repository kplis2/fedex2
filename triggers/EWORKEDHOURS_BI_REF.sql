--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWORKEDHOURS_BI_REF FOR EWORKEDHOURS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EWORKEDHOURS')
      returning_values new.REF;
end^
SET TERM ; ^
