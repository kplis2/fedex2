--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EXPOSURETYPES_BI_REF FOR EXPOSURETYPES                  
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
 if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('EXPOSURETYPES') returning_values new.ref;
end^
SET TERM ; ^
