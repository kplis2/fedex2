--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRCONDIT_AIUD_EVAC FOR EMPLCONTRCONDIT                
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 
as
declare variable ondate date;
begin
  if(new.val_numeric is distinct from old.val_numeric
      and exists(select first 1 1
                from edictcontrcondit
                where ref = coalesce(new.contrcondit,old.contrcondit)
                and (symbol = 'DNI_WOLNE_PLATNE'
                or symbol = 'DNI_WOLNE_NIEPLATNE') )) then
  begin
    if(coalesce(new.annexe,old.annexe) is null) then
      select fromdate from emplcontracts where ref = coalesce(new.emplcontract,old.emplcontract)
      into ondate;
    else
      select fromdate from econtrannexes where ref = coalesce(new.annexe,old.annexe)
      into ondate;
    /*select max(fromdate)
      from emplcontrhist
      where emplcontract = new.emplcontract
      into ondate;*/
    execute procedure refresh_evacucp(:ondate,coalesce(new.emplcontract,old.emplcontract));
   --Przeniesiono z triggera before, zeby uniknac podwajania kodu liczacego nieobecnosci
   --Jezeli po zmianie warunku i odswiezeniu kart jakims cudem mamy ujemne wyniki to rollback
    if(exists(select first 1 1
      from evaclimitsucp
      where emplcontract = coalesce(new.emplcontract,old.emplcontract)
      and (payrestlimit < 0 or nopayrestlimit < 0))) then
        exception universal'Wykorzystano urlop ponad zmienianą wartość. Zmiana niemożliwa.';

  end
end^
SET TERM ; ^
