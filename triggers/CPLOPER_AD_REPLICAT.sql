--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLOPER_AD_REPLICAT FOR CPLOPER                        
  ACTIVE AFTER DELETE POSITION 3 
AS
begin
  execute procedure RP_TRIGGER_AD('CPLOPER', old.ref, NULL, NULL, NULL, NULL, old.token, old.STATE);
end^
SET TERM ; ^
