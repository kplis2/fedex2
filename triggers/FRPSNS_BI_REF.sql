--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRPSNS_BI_REF FOR FRPSNS                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FRPSNS')
      returning_values new.REF;
  new.countord = 1;
end^
SET TERM ; ^
