--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDROZ_BI1 FOR LISTYWYSDROZ                   
  ACTIVE BEFORE INSERT POSITION 10 
AS
  declare variable status smallint_id;
  declare variable msg string100;
begin
  execute procedure x_mws_check_can_pack(new.listywysd, new.iloscmag, new.ktm)
    returning_values (status, msg);
  if (status = 0) then
    exception universal msg;
end^
SET TERM ; ^
