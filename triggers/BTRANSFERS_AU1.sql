--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BTRANSFERS_AU1 FOR BTRANSFERS                     
  ACTIVE AFTER UPDATE POSITION 1 
AS
declare variable czy_rozr smallint;
declare variable operacja varchar(20);
declare variable rozr varchar(20);
declare variable il integer;
declare variable kontofk ACCOUNT_ID;
declare variable kwota numeric(14,2);
declare variable kwotazl numeric(14,2);
declare variable kod varchar(20);
declare variable nazwa varchar(255);
declare variable btranref integer;
declare variable account ACCOUNT_ID;
declare variable settlement varchar(200);
begin
  /* aktualizacja pola BTRANAMOUNTBANK na ROZRACH
    przy wysaniu i wycofaniu przelewu obliczamy ROZRACH.BTRANAMOUNTBANK */
  if ( (new.status = 4 and old.status < 4)
    or (new.status < 4 and old.status = 4)
    or (new.status = 0 and old.status > 0) ) then
  begin
  for select distinct ACCOUNT, SETTLEMENT
    from BTRANSFERPOS where BTRANSFER = new.ref
    into :account, :settlement
    do begin
      execute procedure rozrach_btran_count(new.slodef, new.slopoz, :account, :settlement, new.company);
    end
  end

  if (new.status > 0 and old.status = 0) then
  begin  --akceptacja przelewu - moze trzeba stworzyc rozrachunek
    select BTRANTYPE.genrozrach
      from BTRANTYPE
      where btrantype.symbol = new.btype
      into :czy_rozr;
    if (czy_rozr > 0 and new.slodef > 0 and new.slopoz > 0) then
    begin
      for
        select ref, settlement, amount, amount, account
          from BTRANSFERPOS
          where BTRANSFER = new.ref
          order by ref
          into  :btranref, :rozr,  :kwota,  :kwotazl, :kontofk
      do begin
        if (rozr is not null and rozr <> '') then
        begin
          if (kontofk is null or kontofk = '') then
            execute procedure SLO_DANE(new.slodef, new.slopoz)
              returning_values kod, nazwa, kontofk;

          il = null;
          select count(*)
            from ROZRACH
            where SLOPOZ = new.slopoz
              and SLODEF = new.slodef
              and SYMBFAK = :rozr
              and KONTOFK = :kontofk
              and company = new.company
            into :il;
          if ((il is null or il = 0) and czy_rozr = 1) then exception RK_BRAK_ROZRACHUNKU;
          if ((il is null or il = 0) and czy_rozr = 2) then
            insert into ROZRACH (SLODEF, SLOPOZ, SYMBFAK, DATAOTW, DATAPLAT, WALUTOWY, WALUTA, KONTOFK, SKAD)
              values (new.slodef, new.slopoz, :rozr, new.data, new.data, 0, new.curr, :kontofk, 7);

          operacja = new.bankacc;
          insert into ROZRACHP (company, SLODEF, SLOPOZ, SYMBFAK, kontofk, OPERACJA, DATA, TRESC, WINIEN, MA, SKAD, STABLE, SREF, kurs, winienzl, mazl)
            values (new.company, new.slodef, new.slopoz, :rozr, :kontofk, :operacja, new.data, 'Akceptajca przelewu', :kwota, 0, 7, 'BTRANSFERPOS', :btranref, 1,:kwotazl , 0);
        end
      end
    end
    for
      select distinct ACCOUNT, SETTLEMENT
        from BTRANSFERPOS where BTRANSFER = new.ref
        into :account, :settlement
    do begin
      execute procedure rozrach_btran_count(new.slodef, new.slopoz, account, settlement, new.company);
    end
  end
  else if (new.status = 0 and old.status > 0) then
  begin --kasujemy pozycje. Jeli sa to jedyne na rozrachunku, to skasują rónież rozrachunek
    for
      select ref
        from BTRANSFERPOS
        where BTRANSFER = old.ref
        into :btranref
    do begin
      delete from ROZRACHP
        where company = old.company
          and SLODEF = old.slodef
          and slopoz = old.slopoz
          and stable = 'BTRANSFERPOS'
          and SREF = :btranref;
    end
    -- kasowanie przelewow z grupy, które zostaly dosumowane do tego
    delete from BTRANSFERS
      where BTRANGROUP = new.btrangroup
        and status = 0
        and ref <> new.ref;
    for
      select distinct ACCOUNT, SETTLEMENT
        from BTRANSFERPOS where BTRANSFER = new.ref
        into :account, :settlement
    do begin
      execute procedure rozrach_btran_count(new.slodef, new.slopoz, account, settlement, new.company);
    end
  end
end^
SET TERM ; ^
