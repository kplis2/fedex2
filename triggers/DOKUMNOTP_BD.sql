--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNOTP_BD FOR DOKUMNOTP                      
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable local integer;
begin
  execute procedure check_local('DOKUMNOTP',old.ref) returning_values :local;
  if(:local > 0) then begin
    if(exists(select ref from DOKUMNOTP where dokumnotp.fromdokumnotp = old.ref)) then
      exception UNIVERSAL 'Do pozycji noty mag. istniją pozycje wtórne na innych notach';
  end
end^
SET TERM ; ^
