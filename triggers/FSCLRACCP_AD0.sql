--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FSCLRACCP_AD0 FOR FSCLRACCP                      
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable fstype integer;
begin
  execute procedure fs_make_balance(old.fsclracch);
  execute procedure FSCLRACCH_FSTYP(old.fsclracch) returning_values :fstype;
end^
SET TERM ; ^
