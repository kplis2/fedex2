--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTOCK_BI0 FOR MWSSTOCK                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable goodsav smallint;
begin
  -- dostepnosc do zbiorki
  if (new.mwsconstloc is not null) then
  begin
    select goodsav, coalesce(locdest,0) from mwsconstlocs where ref = new.mwsconstloc
    into goodsav, new.locdest;

    if (goodsav is null) then goodsav = 0;
    if (goodsav < 3) then new.goodsav = goodsav;
    if (goodsav = 3) then
    begin
      if (exists (select 1 from mwsconstlocsymbs
        where mwsconstloc = new.mwsconstloc and const = 1
          and (symbol = new.good or vers = new.vers))
      ) then
        new.goodsav = goodsav;
      else
        new.goodsav = 0;
    end
    if (goodsav = 4) then new.goodsav = goodsav;
  end
  if (new.goodsav is null) then
    new.goodsav = 0;
end^
SET TERM ; ^
