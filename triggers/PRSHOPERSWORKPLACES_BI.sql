--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHOPERSWORKPLACES_BI FOR PRSHOPERSWORKPLACES            
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  new.main = coalesce(new.main,0);
  if(new.main = 0 and not exists (select p.ref from prshopersworkplaces p where p.main = 1 and p.prshoper = new.prshoper)) then begin
    new.main = 1;
  end
END^
SET TERM ; ^
