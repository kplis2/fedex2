--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTYNAG_BD_STATUS FOR NOTYNAG                        
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if (old.status > 1) then
  begin
    if (old.notakind = 1) then
      exception universal 'Najpierw oddekretuj note odsetkową';
  end
end^
SET TERM ; ^
