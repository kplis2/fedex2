--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_NAVIGATOR_AI FOR S_NAVIGATOR                    
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable prefix varchar(255);
declare variable section varchar(255);
declare variable ref varchar(255);
declare variable prev varchar(255);
declare variable number integer;
declare variable changedfields varchar(1024);
begin
  if(new.initializing=1) then exit;
    -- umiesc dane w repozytorium
    prefix = 'Navigator:';
    section = :prefix || new.ref;
    if(new.ref is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'REF',new.ref);
    if(new.parent is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'PARENT',new.parent);
    if(new.prev is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'PREV',new.prev);
    if(new.signature is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'Signature',new.signature);
    if(new.name is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'NAME',new.name);
    if(new.hint is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'HINT',new.hint);
    if(new.kindstr is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'KINDSTR',new.kindstr);
    if(new.actionid is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'ACTIONID',new.actionid);
    if(new.applications is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'APPLICATIONS',new.applications);
    if(new.formodules is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'FORMODULES',new.formodules);
    if(new.rights is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'RIGHTS',new.rights);
    if(new.rightsgroup is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'RIGHTSGROUP',new.rightsgroup);
    if(new.isnavbar is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'ISNAVBAR',new.isnavbar);
    if(new.istoolbar is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'ISTOOLBAR',new.istoolbar);
    if(new.iswindow is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'ISWINDOW',new.iswindow);
    if(new.itemtype is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'ITEMTYPE',new.itemtype);
    if(new.groupprocedure is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'GROUPPROCEDURE',new.groupprocedure);
    if(new.ribbontab is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'RIBBONTAB',new.ribbontab);
    if(new.ribbongroup is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'RIBBONGROUP',new.ribbongroup);
    if(new.globalvalue is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'GLOBALVALUE',new.globalvalue);
    if(new.popupmenu is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'POPUPMENU',new.popupmenu);
    if(new.foradmin is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'FORADMIN',new.foradmin);
    if(new.shortcut is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'SHORTCUT',new.shortcut);
    if(new.globalshortcut is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'GLOBALSHORTCUT',new.globalshortcut);
    if(new.globalcontext is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'GLOBALCONTEXT',new.globalcontext);
    if(new.begingroup is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'BEGINGROUP',new.begingroup);
    if(new.globalsuffix is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'GLOBALSUFFIX',new.globalsuffix);
    if(new.isehrm is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'ISEHRM',new.isehrm);
    if(new.ehrmlink is not null) then execute procedure SYS_SET_APPINI(new.namespace,:section,'EHRMLINK',new.ehrmlink);

    -- przesortuj elementy
    execute procedure SYS_ORDER_APPINI(new.namespace,new.parent,:prefix,new.ref,NULL,new.prev);
    -- zaktualizuj numery w biezacej tabeli
    update s_navigator set INITIALIZING=1 where coalesce(PARENT,'')=coalesce(new.parent,'');
    for select REF,PREV,NUMBER,CHANGEDFIELDS from sys_get_navigator(new.namespace, NULL, 'REF,PREV,PARENT,NUMBER,CHANGEDFIELDS')
      where coalesce(PARENT,'')=coalesce(new.parent,'')
      into :ref,:prev,:number,:changedfields
    do begin
      update s_navigator set NUMBER=:number, PREV=:prev, CHANGEDFIELDS=:changedfields where ref=:ref and (NUMBER<>:number or PREV<>:prev or CHANGEDFIELDS<>:changedfields);
    end
    update s_navigator set INITIALIZING=0 where INITIALIZING=1;
end^
SET TERM ; ^
