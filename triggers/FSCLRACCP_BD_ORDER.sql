--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FSCLRACCP_BD_ORDER FOR FSCLRACCP                      
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update FSCLRACCP set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and fsclracch = old.fsclracch;
end^
SET TERM ; ^
