--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMSER_BI_REF FOR DOKUMSER                       
  ACTIVE BEFORE INSERT POSITION 1 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DOKUMSER')
      returning_values new.REF;
end^
SET TERM ; ^
