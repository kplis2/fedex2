--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDEFPCARDSGROUP_BIU_BLANK FOR EDEFPCARDSGROUP                
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
as
begin
--MWr: Grupa jako 'Suma grup' wymaga wlaczonego podsumowanie dla grupy

  if (new.isgroupsum = 1 and new.issummary = 0) then
    new.issummary = 1;
end^
SET TERM ; ^
