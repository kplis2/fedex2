--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RELATIONS_AD0 FOR RELATIONS                      
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  if (old.relationdef = 1 and old.act > 0 and old.stableto = 'POZZAM'
      and (old.quantityto > 0 or old.quantitytoreal > 0)
  ) then
  begin
    execute procedure pozzam_obl(old.srefto);
  end
end^
SET TERM ; ^
