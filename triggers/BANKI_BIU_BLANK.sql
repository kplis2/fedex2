--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKI_BIU_BLANK FOR BANKI                          
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.symbol = '' or new.symbol is null) then
    exception BANKI_SYMBOL;
  if (new.nazwa = '' or new.nazwa is null) then
    exception BANKI_NAZWA;
end^
SET TERM ; ^
