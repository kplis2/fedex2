--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKATAL_BI_ORDER FOR CKATAL                         
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable maxnum integer;
begin
  new.ord = 1;
  select max(numer) from ckatal where cdefkatal = new.cdefkatal and CPODMIOT = new.CPODMIOT
    into :maxnum;
  if (new.numer is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.numer = maxnum + 1;
  end else if (new.numer <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update ckatal set ord = 0, numer = numer + 1
      where numer >= new.numer and cdefkatal = new.cdefkatal and CPODMIOT = new.CPODMIOT;
  end else if (new.numer > maxnum) then
    new.numer = maxnum + 1;
end^
SET TERM ; ^
