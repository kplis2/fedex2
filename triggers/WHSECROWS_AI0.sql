--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHSECROWS_AI0 FOR WHSECROWS                      
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable coordxb numeric(14,2);
declare variable coordxe numeric(14,2);
declare variable whsecrowref integer;
declare variable w numeric(14,2);
declare variable number integer;
begin
  if (new.number is not null) then
  begin
    for
      select ref, w, number from whsecrows
        where whsec = new.whsec and number >= new.number
        into whsecrowref, w, number
    do begin
      execute procedure mws_whsecrowsdist_calculate(new.whsec,w,number)
        returning_values (:coordxb, :coordxe);
      update whsecrows set coordxb = :coordxb, coordxe = :coordxe
        where ref = :whsecrowref;
    end
  end
end^
SET TERM ; ^
