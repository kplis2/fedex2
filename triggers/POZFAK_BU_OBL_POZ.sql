--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZFAK_BU_OBL_POZ FOR POZFAK                         
  ACTIVE BEFORE UPDATE POSITION 1 
as
declare variable bn char(1);
declare variable cennik integer;
declare variable cena DECIMAL(14,4);
declare variable rabat DECIMAL(14,2);
declare variable kurs numeric(14,4);
declare variable eksport smallint;
declare variable walnag char(3);
declare variable tabkurs integer;
declare variable sredni numeric(14,4);
declare variable zakupu numeric(14,4);
declare variable sprzedazy numeric(14,4);
declare variable typkurs integer;
declare variable walpln char(3);
declare variable cenywal integer;
declare variable zakup smallint;
declare variable jednprzelicz numeric(14,4);
declare variable jednconst smallint;
declare variable tabkursuzywaj smallint;
declare variable bankdomyslny varchar(64);
declare variable data TIMESTAMP;
declare variable fakturarr smallint;
declare variable dostawca integer;
declare variable rabatkask smallint;
declare variable ntabkurs integer;
declare variable sad integer;
declare variable cenacenbru numeric(14,4);
declare variable cenacennet numeric(14,4);
declare variable walcen varchar(3);
declare variable cenazak numeric(14,4);
declare variable typfak varchar(3);
declare variable klient integer;
declare variable stansprzed varchar(80);
declare variable rabatnag varchar(40);
declare variable scennik varchar(255);
declare variable korekta smallint;
declare variable fiskal smallint;
begin
  select nagfak.bn, nagfak.rabat, nagfak.kurs, nagfak.eksport, nagfak.fakturarr,
    nagfak.dostawca, nagfak.zakup, nagfak.sad, nagfak.typ, nagfak.klient,
    nagfak.waluta, nagfak.kurs, rejfak.cenywal, nagfak.tabkurs,
    nagfak.data, nagfak.stanowisko
  from NAGFAK
  left join REJFAK on ( nagfak.rejestr = REJFAK.SYMBOL)
  where nagfak.ref = new.dokument
  into :bn, :rabat, :kurs, :eksport, :fakturarr,
    :dostawca, :zakup, :sad, :typfak, :klient,
    :walnag, :kurs, :cenywal, :ntabkurs,
    :data,:stansprzed;
  if(new.ilosc is null) then new.ilosc = 0;
  if(new.rabat is null) then new.rabat = 0;
  if(new.rabattab is null) then new.rabattab = 0;
  if(new.kosztkat is null) then new.kosztkat = 0;
  if(new.kosztzak is null) then new.kosztzak = 0;
  if(new.pkosztzak is null) then new.pkosztzak = 0;
  if(new.pcenanet is null) then new.pcenanet = 0;
  if(new.pcenabru is null) then new.pcenabru = 0;
  /* PL: Wycofanie zmian MZ z PR46388
  select korekta, fiskalny
    from typfak
    where symbol = :typfak
  into :korekta, :fiskal;
  */
  select fiskalny
    from typfak
    where symbol = :typfak
  into :fiskal;
  if(new.gr_vat is null or (old.wersjaref <> new.wersjaref)) then
  begin
    select vat from wersje where ref = new.wersjaref into new.gr_vat;
    if (new.gr_vat is null) then
      select vat from towary where ktm = new.ktm into new.gr_vat;
    if(:fakturarr = 1) then begin
      execute procedure GETCONFIG('VATRR') returning_values new.gr_vat;
    end else if(:eksport = 1) then begin
      execute procedure GETCONFIG('VATEXPORT') returning_values new.gr_vat;
    end
  end
  if(new.gr_vat<>old.gr_vat) then begin
    if(new.gr_vat is not null) then
      select stawka from vat where grupa = new.gr_vat into new.vat;
    else
      new.vat  = 0;
  end
  if(new.pgr_vat<>old.pgr_vat) then begin
    if(new.pgr_vat is not null) then
      select stawka from vat where grupa = new.pgr_vat into new.pvat;
    else
      new.pvat  = 0;
  end
  if(new.opk > 0) then rabat = 0;--opakowania nie maią rabatu z nagłówka faktury
  if(:bn is null) then
    select TYPFAK.BN from TYPFAK where TYPFAK.SYMBOL=:typfak into :bn;
  if(:eksport is null) then eksport = 0;
  --ustalenie jednostki w zależnosci od rodzaju zamowienia
  if(new.jedn is null or (new.jedn = 0)) then begin
    if(:zakup = 0) then
      select REF from TOWJEDN where KTM = new.ktm and C = 2 into new.jedn;
    else
      select ref from TOWJEDN where KTM = new.ktm and Z  = 2 into new.jedn;
    if(new.jedn is null or (new.jedn = 0))then
      select ref from TOWJEDN where KTM = new.KTM and GLOWNA = 1 into new.jedn;
  end
  if(new.jedno is null or (new.jedno = 0))then
    if(:zakup = 0) then
      select REF from TOWJEDN where KTM = new.ktm and S = 2 into new.jedno;
    else
      select ref from TOWJEDN where KTM = new.ktm and Z  = 2 into new.jedno;
  if(new.jedno is null or (new.jedno = 0))then
    new.jedno = new.jedn;
  if(new.jedn = new.jedno) then begin
    new.ilosco = new.ilosc;
    new.pilosco = new.pilosc;
  end
  if(new.iloscm is null or (new.iloscm = 0) or (new.ilosc<>old.ilosc) or (new.jedn<>old.jedn)) then begin
    select PRZELICZ, CONST from TOWJEDN where REF=new.jedn into :jednprzelicz, :jednconst;
    new.iloscm = new.ilosc * :jednprzelicz;
  end
  if(new.piloscm is null or (new.piloscm = 0) or (new.pilosc<>old.pilosc) or (new.jedn<>old.jedn)) then begin
    select PRZELICZ, CONST from TOWJEDN where REF=new.jedn into :jednprzelicz, :jednconst;
    new.piloscm = new.pilosc * :jednprzelicz;
  end
  -- przeliczenie kosztu katalogowego
  if((coalesce(new.wersjaref,0) <> coalesce(old.wersjaref,0)) or
     (new.iloscm <> old.iloscm) or
     (new.piloscm <> old.piloscm)) then begin
    cenazak = null;
    select cena_zakn from wersje where ref=new.wersjaref into :cenazak;
    if(:cenazak is null) then cenazak = 0;
    if(:zakup=0) then
      new.kosztkat = :cenazak * (new.iloscm-new.piloscm);
    else
      new.kosztkat = -:cenazak * (new.iloscm-new.piloscm);
  end

  -- obliczenie ceny
  if(new.cenacen is null and :zakup = 0) then
  begin
    if(new.refcennik is not null) then cennik = new.refcennik;
    else begin
      select grupykli.cennik
        from klienci
        left join grupykli on (klienci.grupa = grupykli.ref)
        where klienci.ref = :klient
        into :cennik;
    end
    if(:cennik is null) then begin
      execute procedure GETCONFIG('CENNIK') returning_values :scennik;
      if (scennik <> '') then
        cennik = cast(scennik as integer);
    end
    select cenanet, cenabru, waluta
      from cennik
      where cennik = :cennik and ktm = new.ktm and wersja = new.wersja and jedn = new.jedn
      into :cenacennet, :cenacenbru, :walcen;
    if(:cenacennet is null) then
      select cenanet, cenabru, waluta
        from cennik
        where cennik = :cennik and ktm = new.ktm and wersja = 0 and jedn = new.jedn
        into :cenacennet, :cenacenbru, :walcen;
    new.orgcenanet = :cenacennet;
    if(new.orgcenanet is null) then new.orgcenanet = 0;
    if(:bn = 'B') then
    begin
      new.cenacen = cenacenbru;
      new.walcen = walcen;
    end
    else
    begin
      new.cenacen = cenacennet;
      new.walcen = walcen;
    end
  end
  --
  if(new.cenacen is null) then new.cenacen = 0;
  cena = new.cenacen;
  -- przeliczenie walut cennika na walutę dokumenut
  if(:cenywal=1 and new.walcen <> :walnag and new.walcen is not null and new.walcen <> '') then begin
    tabkursuzywaj = 0;
    typkurs = -1;
    select STANSPRZED.tabkurs, STANSPRZED.TYPKURS, STANSPRZED.TABKURSUZYWAJ, STANSPRZED.bankdomyslny
      from STANSPRZED
      where STANSPRZED.stanowisko = :stansprzed
      into :tabkurs, :typkurs, :tabkursuzywaj, :bankdomyslny;
    if(:ntabkurs is not null) then tabkurs = :ntabkurs;
    execute procedure GETCONFIG('WALPLN') returning_values :walpln;
    if(:walpln is null) then walpln = 'PLN';
    if(new.walcen <> :walpln ) then begin
      if(new.kurscen is null or new.kurscen=0 or new.kurscen=1) then begin
        if(new.tabkurscen is not null) then begin
          if(:typkurs<0) then exception KURSTYP_BRAK;
          sredni = 0; zakupu = 0; sprzedazy = 0;
          select tp.sredni, tp.zakupu, tp.sprzedazy from tabkurs t join tabkursp tp on (t.ref = tp.tabkurs)
          where t.ref = new.tabkurscen and tp.waluta = new.walcen
          into  :sredni, :zakupu, :sprzedazy;
--          if((:typkurs = 0 and :sredni = 0) or (:typkurs = 1 and :zakupu = 0) or (:typkurs = 2 and :sprzedazy = 0)) then begin
--            select data from tabkurs where ref = new.tabkurscen into :data;
--            exception KURS_BRAKWARTOSCI 'Niezdefiniowany kurs sredni. Bank: '||:bankdomyslny||' Data: '||substring(:data from 1 for 10)||' Waluta: '||new.walcen;
--          end
          if(:typkurs = 0)then new.kurscen = :sredni;
          else if(:typkurs = 1)then new.kurscen = :zakupu;
          else if(:typkurs = 2)then new.kurscen = :sprzedazy;
          if(new.kurscen=0) then new.kurscen = 1;
        end
        else if(tabkursuzywaj = 1) then begin
          if(:typkurs<0) then exception KURSTYP_BRAK;
          sredni = 0; zakupu = 0; sprzedazy = 0;
          select tp.sredni, tp.zakupu, tp.sprzedazy from tabkurs t join tabkursp tp on (t.ref = tp.tabkurs)
          where t.bank = :bankdomyslny and t.data = :data and tp.waluta = new.walcen
          into  :sredni, :zakupu, :sprzedazy;
--          if((:typkurs = 0 and :sredni = 0) or (:typkurs = 1 and :zakupu = 0) or (:typkurs = 2 and :sprzedazy = 0)) then
--            exception KURS_BRAKWARTOSCI 'Niezdefiniowany bieżący kurs. Bank:'||:bankdomyslny||' Data:'||substring(:data from 1 for 10)||' Waluta:'||new.walcen;
          if(:typkurs = 0)then new.kurscen = :sredni;
          else if(:typkurs = 1)then new.kurscen = :zakupu;
          else if(:typkurs = 2)then new.kurscen = :sprzedazy;
          if(new.kurscen=0) then new.kurscen = 1;
        end
        else begin
          select sredni, zakupu, sprzedazy from TABKURSP where TABKURS = :tabkurs and WALUTA = new.walcen into :sredni, :zakupu, :sprzedazy;
          if(:typkurs = 1) then sredni = :zakupu;
          else if(:typkurs = 2) then sredni = :sprzedazy;
          if(:sredni is null) then sredni = 1;
          new.kurscen = sredni;
        end
      end
    end else new.kurscen = 1;
    if(:kurs is null) then kurs = 1;
     if (new.prec = 4) then
      cena = cast(:cena * new.kurscen / :kurs as numeric(14,4));
    else
      cena = cast(:cena * new.kurscen / :kurs as numeric(14,2));
  end else
    new.walcen = :walnag;
  if(:kurs is null) then kurs = 1;
  if(coalesce(:fiskal,0)=1) then begin
    new.prec = 2;
    cena = cast(:cena as numeric(14,2));
    if(new.ilosc <> cast(new.ilosc as numeric(14,2))) then exception POZFAK_FISK 'Dokladność ilości nie zgodna z dopuszczoną dla paragonu! Dla '||coalesce(new.ktm,'');
  end
  --PL: Wycofanie zmian MZ w ramach PR46388
 -- if(:zakup = 0 or (new.rabat=-101) or korekta = 1) then begin
    if(new.rabat = -101) then new.rabat = old.rabat;
    --obliczenie rabatu
    execute procedure GETCONFIG('RABATNAG') returning_values :rabatnag;
    if(zakup = 0 and :rabatnag='1' and new.rabattab<>0) then begin
      if(new.prec=4) then cena = cast((:cena * (100- new.rabat) /100) as DECIMAL(14,4));
      else cena = cast((:cena * (100- new.rabat) /100) as DECIMAL(14,2));
    end else begin
      if(dostawca is not null and dostawca > 0) then begin
        select d.rabatkask from dostawcy d where d.ref = :dostawca into :rabatkask;
        if(rabatkask is not null and rabatkask = 1)then begin
          if(new.prec=4) then cena = cast(cena * (100- :rabat)*(100 - new.rabat)/10000 as DECIMAL(14,4));
          else cena = cast(cena * (100- :rabat)*(100 - new.rabat)/10000 as DECIMAL(14,2));
        end else begin
          if(new.rabat is not null) then rabat = rabat + new.rabat;
          if(:rabat <> 0)then begin
            if(new.prec=4) then cena = cast(cena * (100- :rabat)/100 as DECIMAL(14,4));
            else cena = cast(cena * (100- :rabat)/100 as DECIMAL(14,2));
          end
        end
      end else begin
        if(new.rabat is not null) then rabat = :rabat + new.rabat;
        if(:rabat <> 0)then begin
          if(new.prec=4) then cena = cast(:cena * (100- :rabat)/100 as DECIMAL(14,4));
          else cena = cast(:cena * (100- :rabat)/100 as DECIMAL(14,2));
        end
      end
    end
    -- obliczenie cen
    if(:bn = 'B') then begin
       new.cenabru = cena;
       new.wartbru = new.ilosc * new.cenabru;
       new.pwartbru = new.pilosc * new.pcenabru;
       new.pwartnet = cast((new.pwartbru / (1+new.pvat / 100)) as DECIMAL(14,2));
       if(:eksport = 0) then begin
         new.wartnet = cast((new.wartbru / (1+new.vat / 100)) as DECIMAL(14,2));
         new.cenanet = cast((new.cenabru / (1+(new.vat/100))) as DECIMAL(14,2));
       end else begin
         new.wartnet = new.wartbru;
         new.cenanet = new.cenabru;
       end
    end else begin
       new.cenanet = cena;
       new.wartnet = new.ilosc * new.cenanet;
       new.pwartnet = new.pilosc * new.pcenanet;
       new.pwartbru = new.pwartnet + cast((new.pwartnet * (new.pvat / 100)) as DECIMAL(14,2));
       if(:eksport = 0) then begin
         new.wartbru = new.wartnet + cast((new.wartnet * (new.vat / 100)) as DECIMAL(14,2));
         new.cenabru = new.cenanet + cast((new.cenanet * (new.vat / 100)) as DECIMAL(14,2));
       end else begin
         new.wartbru = new.wartnet;
         new.cenabru = new.cenanet;
       end
    end
   --PL: Wycofanie zmian MZ w ramach PR46388
  --end

  if(new.pilosco is null) then new.pilosco = 0;
  if(new.ilosco <> old.ilosco or (new.pilosco <> old.pilosco) or (new.jedno <> old.jedno)) then begin
    select TOWJEDN.waga * (new.ilosco-new.pilosco), TOWJEDN.vol * (new.ilosco-new.pilosco)
    from TOWJEDN where ref=new.jedno into new.waga, new.objetosc;
  end
  if(new.waga is null) then new.waga = 0;
  if(new.objetosc is null) then new.objetosc = 0;
  if(new.pwartnet is null) then new.pwartnet = 0;
  if(new.pwartbru is null) then new.pwartbru = 0;
  -- wartosci zlotowkowe
  /*MS: w przypadku SADu bierz kursy z PZI */
  if(:sad=1 and new.fromdokumpoz is not null) then begin
    select dokumnag.kurs
      from dokumpoz
      left join dokumnag on (dokumnag.ref=dokumpoz.dokument)
      where dokumpoz.ref=new.fromdokumpoz into :kurs;
    if(:kurs is null) then kurs = 1;
  end

  if(:bn = 'B') then begin
    new.cenanetzl = new.cenanet * :kurs;
    new.pcenanetzl = new.pcenanet * :kurs;
    new.wartbruzl = new.wartbru * :kurs;
    new.pwartbruzl = new.pwartbru * :kurs;
    new.cenabruzl = new.cenabru * :kurs;
    new.pcenabruzl = new.pcenabru * :kurs;
    if(:eksport = 0) then begin
      new.wartnetzl = cast((new.wartbruzl / (1+new.vat / 100)) as DECIMAL(14,2));
      new.pwartnetzl = cast((new.pwartbruzl / (1+new.pvat / 100)) as DECIMAL(14,2));
    end else begin
      new.wartnetzl = new.wartbruzl;
      new.pwartnetzl = new.pwartbruzl;
    end
    if(new.prec = 2) then
    begin
      new.cenanetzl = cast(new.cenanetzl as numeric(14,2));
      new.cenabruzl = cast(new.cenabruzl as numeric(14,2));
    end
  end else begin
    new.cenanetzl = new.cenanet * :kurs;
    new.pcenanetzl = new.pcenanet * :kurs;
    new.wartnetzl = new.wartnet * :kurs;
    new.pwartnetzl = new.pwartnet * :kurs;
    new.cenabruzl = new.cenabru * :kurs;
    new.pcenabruzl = new.pcenabru * :kurs;
    if(:eksport = 0) then begin
      new.wartbruzl = new.wartnetzl + cast((new.wartnetzl * (new.vat / 100)) as DECIMAL(14,2));
      new.pwartbruzl = new.pwartnetzl + cast((new.pwartnetzl * (new.pvat / 100)) as DECIMAL(14,2));
    end else begin
      new.wartbruzl = new.wartnetzl;
      new.pwartbruzl = new.pwartnetzl;
    end
    if(new.prec = 2) then
    begin
      new.cenanetzl = cast(new.cenanetzl as numeric(14,2));
      new.cenabruzl = cast(new.cenabruzl as numeric(14,2));
    end
  end
  new.wartvatzl = new.wartbruzl - new.wartnetzl;
end^
SET TERM ; ^
