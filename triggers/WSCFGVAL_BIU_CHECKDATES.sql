--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WSCFGVAL_BIU_CHECKDATES FOR WSCFGVAL                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
declare variable rfromdate timestamp;
declare variable rtodate timestamp;
declare variable symbol varchar(40);
begin
--Celowy brak warunkow wywolania, mechanizm wywolywany przez EPAYRULES_AU_CHECKDATES

  select fromdate, todate, symbol from epayrules
    where ref = new.epayrule and company = new.company
    into :rfromdate, :rtodate, :symbol;

  if (coalesce(rfromdate, new.fromdate) > new.fromdate
    or coalesce(rtodate, new.fromdate) < new.fromdate
  ) then
    exception universal 'Data parametru ' || new.wscfgdef || ' poza zakresem regulaminu: ' || symbol || '!';

end^
SET TERM ; ^
