--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_AU_PRD FOR DECREES                        
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable side smallint;
begin
  if (new.account<>old.account) then
  begin
    delete from prdaccounting P where P.decree = new.ref and P.account <> new.account;
  end
  if (new.status > 1 and old.status < 2) then
  begin

    --zmiana statusu rozliczenia zródlowego
    --exception test_break new.ref;
    update prdaccounting P set P.status = 1 where P.decree = new.ref;

    if (new.prdaccounting is not null) then
    begin
      -- aktualizacja wartosci odpisanej na rozliczeniu
      if (new.debit > 0) then
      begin
        update prdaccounting P set P.deductedamount = P.deductedamount + new.debit
        where p.ref = new.prdaccounting and p.account = new.account and P.decree <> new.ref;
      end
      else if (new.credit > 0) then  begin
        update prdaccounting P set P.deductedamount = P.deductedamount + new.credit
        where p.ref = new.prdaccounting and p.account = new.account and P.decree <> new.ref;
      end
      else begin
        select side from prdaccounting where ref = new.prdaccounting
          into :side;
        if (side = new.side) then
          update prdaccounting P set P.amount = P.amount - abs(new.debit-new.credit)
             where P.ref = new.prdaccounting and p.account = new.account and P.decree <> new.ref;
        else
          update prdaccounting P set P.deductedamount = P.deductedamount - abs(new.debit-new.credit)
            where P.ref = new.prdaccounting and p.account = new.account;
      end
    end
  end
  if (new.status < 2 and old.status > 1) then
  begin
    -- zmiana statusu rozliczenia zródlowego
    update prdaccounting P set P.status = 0 where P.decree = new.ref;

    if (new.prdsymbol is not null) then
    begin
       -- aktualizacja wartosci odpisanej na rozliczeniu
      if (new.debit > 0) then
      begin
        update prdaccounting P set P.deductedamount = P.deductedamount - new.debit
        where p.ref = new.prdaccounting and p.account = new.account and P.decree <> new.ref;
      end
      else if (new.credit > 0) then  begin
        update prdaccounting P set P.deductedamount = P.deductedamount - new.credit
        where p.ref = new.prdaccounting and p.account = new.account and P.decree <> new.ref;
      end
      else begin
        select side from prdaccounting where ref = new.prdaccounting
          into :side;
        if (side = new.side) then
          update prdaccounting P set P.amount = P.amount + abs(new.debit-new.credit)
             where P.ref = new.prdaccounting and p.account = new.account and P.decree <> new.ref;
        else
          update prdaccounting P set P.deductedamount = P.deductedamount + abs(new.debit-new.credit)
            where P.ref = new.prdaccounting and p.account = new.account;
      end
    end
  end
end^
SET TERM ; ^
