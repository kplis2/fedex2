--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONTAKTY_BI_PRAWA FOR KONTAKTY                       
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable rodzaj varchar(40);
declare variable kierunek varchar(10);
declare variable nazwa varchar(80);
declare variable opernazwa varchar(40);
begin
 if(new.prawadef is null) then new.prawadef = 0;
 if(new.prawadef=0) then begin
   if(new.cpodmiot is not null) then
     select PRAWA,PRAWAGRUP from CPODMIOTY where ref = new.cpodmiot
     into new.prawa, new.prawagrup;
   else begin
     new.prawa = ';';
     new.prawagrup = ';';
   end
 end
 execute procedure GET_SUPERIORS(new.prawa) returning_values new.prawa;
 if(new.cpodmiot is not null) then
   select SKROT,SLODEF,OPEROPIEK,TELEFON,COMPANY from CPODMIOTY where REF=new.CPODMIOT
   into new.cpodmskrot, new.cpodmslodef, new.cpodmoperopiek, new.cpodmtelefon, new.company;
 if(new.PKOSOBA is not NULL) then
   select NAZWA,TELEFON from PKOSOBY where REF=new.PKOSOBA
   into new.PKOSOBYNAZWA, new.PKOSOBYTELEFON;
 else begin
   new.PKOSOBYNAZWA = NULL;
   new.PKOSOBYTELEFON = NULL;
 end
 select NAZWA from CTYPYKON where REF=new.rodzaj into :rodzaj;
 rodzaj = substring(:rodzaj from 1 for 12);
 kierunek = '';
 if(new.inout=0) then kierunek = ' do ';
 else if(new.inout=1) then kierunek = ' od ';
 nazwa = cast(new.data as date) || ' ' || :rodzaj;
 if(new.cpodmiot is not null) then nazwa = :nazwa || :kierunek || substring(new.cpodmskrot from 1 for 20);
 select NAZWA from OPERATOR where REF=new.operator into :opernazwa;
 nazwa = :nazwa || ' (' || :opernazwa || ')';
 new.nazwa = substring(:nazwa from 1 for 80);
 if(new.cpodmiot is not null) then
   select ancestors from CPODMIOTY where ref = new.cpodmiot into new.cpodmancestors;
end^
SET TERM ; ^
