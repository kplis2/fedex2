--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER REZNAG_BU_CHANGE FOR REZNAG                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (old.status = 3 and new.status = 3 and
     (new.operzal <> old.operzal or
      new.datazal <> old.datazal or
      new.operwyk <> old.operwyk or
      new.status  <> old.status  or
      new.dataod  <> old.dataod  or
      new.datado  <> old.datado  or
      new.opis    <> old.opis    or
      new.pkosoba <> old.pkosoba or
      new.stawka  <> old.stawka  or
      new.pmplan  <> old.pmplan  or
      new.cpodmiot<> old.cpodmiot or
      new.rezzasob<> old.rezzasob or
      new.dniwypoz<> old.dniwypoz or
      new.pmelement <> old.pmelement or
      new.pmposition<> old.pmposition)) then
      exception reznag_change;
end^
SET TERM ; ^
