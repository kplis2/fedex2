--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLODEF_BD_WPK FOR SLODEF                         
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  --sprawdzamy, czy nie istnieją słowniki zalezne ktore maja modyfikacje indywidualne
  if (exists (select first 1 1
                from slodef d join slopoz p on (d.ref = p.slownik)
                where d.patternref = old.ref and p.pattern_ref is null
              ))then
    exception slodef_del_wpk_slave;
  -- jeżeli nie ma modyfikacji to usuwaj
  delete from slodef s
    where s.patternref = old.ref;
end^
SET TERM ; ^
