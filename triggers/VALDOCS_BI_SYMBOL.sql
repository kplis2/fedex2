--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VALDOCS_BI_SYMBOL FOR VALDOCS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable numer integer;
  declare variable numberg varchar(40);
begin
  execute procedure GETCONFIG('NUMBERGEN_VALDOCS') returning_values :numberg;
  if (numberg <> '') then begin
    execute procedure Get_NUMBER(:numberg, null, new.doctype, new.docdate, 0, null) returning_values new.numer, new.symbol;
  end
end^
SET TERM ; ^
