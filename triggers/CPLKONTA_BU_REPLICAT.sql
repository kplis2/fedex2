--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLKONTA_BU_REPLICAT FOR CPLKONTA                       
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.cpluczest <> old.cpluczest
      or new.typpkt <> old.typpkt
      or (new.sumpktp <> old.sumpktp ) or (new.sumpktp is not null and old.sumpktp is null) or (new.sumpktp is null and old.sumpktp is not null)
      or (new.sumpktm <> old.sumpktm ) or (new.sumpktm is not null and old.sumpktm is null) or (new.sumpktm is null and old.sumpktm is not null)
      ) then
   waschange = 1;

  execute procedure rp_trigger_bu('CPLKONTA',old.cpluczest, old.typpkt, null, null, null, old.token, old.state,
        new.cpluczest, new.typpkt, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
