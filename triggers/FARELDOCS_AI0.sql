--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FARELDOCS_AI0 FOR FARELDOCS                      
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  update fxdassets set oddzial = new.branch, department = new.department,
    comparment = new.compartment, person = new.person,
    tbksymbol = new.tbksymbol, fbksymbol = new.fbksymbol
    where ref = new.fxdasset;
end^
SET TERM ; ^
