--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDELCOUNTRYALLOW_BI0 FOR EDELCOUNTRYALLOW               
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EDELCOUNTRYALLOW')
      returning_values new.REF;
end^
SET TERM ; ^
