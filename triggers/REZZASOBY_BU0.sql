--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER REZZASOBY_BU0 FOR REZZASOBY                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin

  if(new.prmachine <> old.prmachine or (new.prmachine is not null and old.prmachine is null))then begin
    if(not exists(select ref
               from rezgrup
               where rezgrup.ref = new.grupa and rezgrup.machinesgroup = 1
    )) then
      exception UNIVERSAL 'Grupa zasobów niepoziwązana z produkcją.';
    select prmachines.symbol, prmachines.descript
      from PRMACHINES where ref=new.prmachine
      into new.nazwa, new.opis;
   end
end^
SET TERM ; ^
