--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROPERSRAPS_AI_PRODTRANSFER FOR PROPERSRAPS                    
  ACTIVE AFTER INSERT POSITION 2 
as
begin
  if (coalesce(new.amount,0) > 0) then
  begin
    -- przekazanie wyrobu gotowego jako surowiec na inny przewodnik
    if (exists(select first 1 1 from prschedguides where ref = new.prschedguide and prodtransfer = 1)) then
      execute procedure prod_transfer(new.ref,0);
  end
end^
SET TERM ; ^
