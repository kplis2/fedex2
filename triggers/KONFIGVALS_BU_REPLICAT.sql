--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONFIGVALS_BU_REPLICAT FOR KONFIGVALS                     
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.wartosc <> old.wartosc or (new.wartosc is null and old.wartosc is not null) or (new.wartosc is not null and old.wartosc is null)) then
   waschange = 1;

  execute procedure rp_trigger_bu('KONFIGVALS',old.akronim, old.oddzial, null, null, null, old.token, old.state,
        new.akronim, new.oddzial, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
