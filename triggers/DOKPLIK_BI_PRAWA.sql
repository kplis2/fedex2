--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKPLIK_BI_PRAWA FOR DOKPLIK                        
  ACTIVE BEFORE INSERT POSITION 1 
as
begin
 if(new.prawadef is null) then new.prawadef = 0;
 if(new.prawadef=0) then begin
   if(new.cpodmiot is not null) then
     select PRAWA,PRAWAGRUP from CPODMIOTY where ref = new.cpodmiot
     into new.prawa, new.prawagrup;
   else begin
     new.prawa = '';
     new.prawagrup = '';
   end
 end
 execute procedure GET_SUPERIORS(new.prawa) returning_values new.prawa;
 if(new.CPODMIOT is not NULL) then begin
   select SKROT,OPEROPIEK from CPODMIOTY where REF=new.CPODMIOT
   into new.cpodmskrot, new.cpodmoperopiek;
 end
end^
SET TERM ; ^
