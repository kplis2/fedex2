--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSPALLOCS_BI_REF FOR MWSPALLOCS                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSPALLOCS') returning_values new.ref;
end^
SET TERM ; ^
