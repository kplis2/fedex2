--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERWER_BI_ORDER FOR DEFOPERWER                     
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable maxnum integer;
begin
  new.ord = 1;
  select max(numer) from defoperwer where operacja = new.operacja
    into :maxnum;
  if (new.numer is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.numer = maxnum + 1;
  end else if (new.numer <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update defoperwer set ord = 0, numer = numer + 1
      where numer >= new.numer and operacja = new.operacja;
  end else if (new.numer > maxnum) then
    new.numer = maxnum + 1;
end^
SET TERM ; ^
