--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_BI_PARENT FOR KLIENCI                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
declare variable ancestors varchar(255);
begin
  if ((inserting and new.parent is not null) or (updating and (coalesce(new.parent,0) <> coalesce(old.parent,0))))  then
  begin
    select ancestors from KLIENCI where ref = new.parent into :ancestors;
    if (ancestors like '%;'||new.ref||';%') then
      exception universal 'Niedozwolone powiązanie klientów';
    if (:ancestors is null or :ancestors ='') then
      new.ancestors = ';'||new.parent||';';
    else
     new.ancestors = :ancestors || new.parent || ';' ;
  end
end^
SET TERM ; ^
