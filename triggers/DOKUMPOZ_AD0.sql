--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_AD0 FOR DOKUMPOZ                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable skad smallint;
declare variable dokumentmain integer;
declare variable usluga smallint;
declare variable ilosckor numeric(14,4);
declare variable mwsdisposition smallint;
begin
  select D.SKAD, D.DOKUMENTMAIN, d.mwsdisposition
    from DOKUMNAG D
    where D.ref = old.dokument
  into :skad, :dokumentmain, :mwsdisposition;
  if(old.wartrealpoz > 0) then begin
    update POZZAM set ILREAL = 0, ILREALO = 0, ILREALM = 0 where ref=old.wartrealpoz;
    execute procedure POZZAM_OBL(old.wartrealpoz);
  end
  if(old.wartrealzam > 0) then
    execute procedure NAGZAM_OBL(old.wartrealzam);
  if(old.frompozfak > 0 and ((old.wartosc <> 0) or (old.pwartosc <> 0)) ) then
     execute procedure POZFAK_OBL_KOSZT(old.frompozfak);
  update DOKUMROZ set CENATOPOZ = null where CENATOPOZ = old.ref;

  -- obliczanie ILOSCL dla pozycji uslugowych
  if(old.kortopoz is not null) then begin
    select USLUGA from TOWARY where KTM=old.ktm into :usluga;
    if(:usluga=1) then begin
      select sum(ILOSC) from DOKUMPOZ where KORTOPOZ=old.kortopoz into :ilosckor;
      if(:ilosckor is null) then ilosckor = 0;
      update DOKUMPOZ set ILOSCL = ILOSC - :ilosckor where REF=old.kortopoz;
    end
  end

  if(skad >= 3) then
    execute procedure PRSCHEDGUIDEDET_OBL('DOKUMNAG',:skad,old.ref,-1);

  /* Uzgodnienie ilosci na zamówieniu */
  if(:mwsdisposition = 1) then
    execute procedure pozzam_obl(old.ref);

end^
SET TERM ; ^
