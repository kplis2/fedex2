--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDEDETS_AD0 FOR PRSCHEDGUIDEDETS               
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  if (old.quantity > 0 and old.out in (1,3)) then
    EXECUTE PROCEDURE PRSCHEDGUIDEPOS_AMOUNTZREAL(old.prschedguidepos);
  else if (old.quantity > 0 and old.out = 0 and old.prschedguide is not null and old.prschedoper is not null) then
  begin
    EXECUTE PROCEDURE PRSCHEDGUIDE_AMOUNTZREAL(old.prschedguide);
    if (old.prshortage is not null) then
      EXECUTE PROCEDURE PRSHORTAGE_AMOUNTZREAL(old.prshortage);
  end
  if(old.out = 1 and old.accept > 0 and old.quantity > 0) then
  begin
    if (not exists(select first 1 1 from prschedguidedets d where d.out = 1 and d.accept > 0
        and d.quantity > 0 and d.prschedguide = old.prschedguide)
    ) then
      execute procedure prschedguides_statuschange(old.prschedguide,0,null);
  end
  if (old.ssource = 1 and exists(select first 1 1 from prschedguidedets d where d.stable = old.stable and d.ref = old.sref)) then
    execute procedure prschedguidedet_change(1,'PRSCHEDGUIDEDETS',old.ref,old.prschedguidepos);
end^
SET TERM ; ^
