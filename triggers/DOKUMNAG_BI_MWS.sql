--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_BI_MWS FOR DOKUMNAG                       
  ACTIVE BEFORE INSERT POSITION 30 
as
begin
  if (new.braktowaru is null) then
    new.braktowaru = 0;

  --[PM] do integracji  
  if (new.braktowaru_czaszm is null) then
    new.braktowaru_czaszm = current_timestamp(0);
end^
SET TERM ; ^
