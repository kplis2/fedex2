--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTAWCY_AU0 FOR DOSTAWCY                       
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable slodef integer;
declare variable cnt integer;
declare variable prawagrup varchar(80);
declare variable currentyear integer;
declare variable bankaccount integer;
begin
 if(new.crm = 1 and old.crm = 0)  then begin
   select ref from SLODEF where TYP = 'DOSTAWCY' into :slodef;
   if(:slodef is null) then exception SLODEF_BEZDOSTAWCY;
   execute procedure GETCONFIG('DOMPRAWAGRUP') returning_values :prawagrup;
   select count(*) from CPODMIOTY where SLODEF = :slodef AND SLOPOZ = new.ref into :cnt;
   if(:cnt > 0) then
     update CPODMIOTY set SKROT = substring(new.id from 1 for 40), NAZWA = new.nazwa,    --XXX ZG133796 MKD
                          TELEFON = new.telefon, FAX = new.fax, EMAIL = new.email,
                          WWW = new.www, NIP = new.nip, REGON = new.regon, UWAGI = new.uwagi,
                          ULICA = new.ulica, NRDOMU = new.nrdomu, NRLOKALU = new.nrlokalu,
                          MIASTO = new.miasto, KODP = new.kodp, POCZTA = new.poczta,
                          TYP = new.typ, PRAWAGRUP = :prawagrup, KONTOFK = new.kontofk, COMPANY = new.company,
                           cpowiaty = new.cpowiat, SYNCHRO=1
        where SLODEF = :slodef AND SLOPOZ = new.ref ;
   else
    insert into CPODMIOTY(AKTYWNY, FIRMA, SKROT, NAZWA, TELEFON, EMAIL, FAX, WWW,
         NIP, REGON, UWAGI, SLODEF, SLOPOZ, ULICA, NRDOMU, NRLOKALU,
         MIASTO, KODP, POCZTA, TYP, PRAWAGRUP, KONTOFK, COMPANY, cpowiaty)
      values(1, 1,substring(new.id from 1 for 40), new.nazwa, new.telefon, new.email, new.fax, new.www, --XXX ZG133796 MKD
         new.nip, new.regon, new.uwagi, :slodef, new.ref, new.ulica, new.nrdomu, new.nrlokalu,
         new.miasto, new.kodp, new.poczta, new.typ, :prawagrup, new.kontofk, new.company, new.cpowiat);

 end else if(new.crm = 0 and old.crm = 1) then begin
   select ref from SLODEF where TYP = 'DOSTAWCY' into :slodef;
   if(:slodef is null) then exception SLODEF_BEZDOSTAWCY;
   delete from CPODMIOTY where SLODEF = :slodef AND SLOPOZ = old.ref;
 end
 if(new.crm = 1 and old.crm = 1 and
     (new.id <> old.id or (new.id is not null and old.id is null) or (new.id is null and old.id is not null) or
      new.NAZWA <> old.NAZWA or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null) or
      new.ulica <> old.ulica or (new.ulica is not null and old.ulica is null) or (new.ulica is null and old.ulica is not null) or
      new.nrdomu <> old.nrdomu or (new.nrdomu is not null and old.nrdomu is null) or (new.nrdomu is null and old.nrdomu is not null) or
      new.nrlokalu <> old.nrlokalu or (new.nrlokalu is not null and old.nrlokalu is null) or (new.nrlokalu is null and old.nrlokalu is not null) or
      new.nip <> old.nip or (new.nip is not null and old.nip is null) or (new.nip is null and old.nip is not null) or
      new.miasto <> old.miasto or (new.miasto is not null and old.miasto is null) or (new.miasto is null and old.miasto is not null) or
      new.kodp <> old.kodp or (new.kodp is not null and old.kodp is null) or (new.kodp is null and old.kodp is not null) or
      new.poczta <> old.poczta or (new.poczta is not null and old.poczta is null) or (new.poczta is null and old.poczta is not null) or
      new.telefon <> old.telefon or (new.telefon is not null and old.telefon is null) or (new.telefon is null and old.telefon is not null) or
      new.fax <> old.fax or (new.fax is not null and old.fax is null) or (new.fax is null and old.fax is not null) or
      new.www <> old.www or (new.www is not null and old.www is null) or (new.www is null and old.www is not null) or
      new.email <> old.email or (new.email is not null and old.email is null) or (new.email is null and old.email is not null) or
      new.regon <> old.regon or (new.regon is not null and old.regon is null) or (new.regon is null and old.regon is not null) or
      new.typ <> old.typ or (new.typ is not null and old.typ is null) or (new.typ is null and old.typ is not null) or
      new.uwagi <> old.uwagi or (new.uwagi is not null and old.uwagi is null) or (new.uwagi is null and old.uwagi is not null) or
      new.kontofk <> old.kontofk or (new.kontofk is not null and old.kontofk is null) or (new.kontofk is null and old.kontofk is not null) or
      new.cpowiat <> old.cpowiat or (new.cpowiat is not null and old.cpowiat is null) or (new.cpowiat is null and old.cpowiat is not null)
     )
   ) then begin
        select ref from SLODEF where TYP = 'DOSTAWCY' into :slodef;
        if(:slodef is null) then exception SLODEF_BEZDOSTAWCY;
        update CPODMIOTY  set SKROT  = substring(new.id from 1 for 40), NAZWA = new.nazwa,--XXX ZG133796 MKD
                      ULICA = new.ulica, NRDOMU = new.nrdomu, NRLOKALU = new.nrlokalu,
                      NIP = new.nip, MIASTO = new.miasto,
                      KODP = new.kodp, POCZTA = new.poczta, UWAGI = new.uwagi,
                      REGON = new.regon, TELEFON = new.telefon, EMAIL = new.email, WWW = new.www, FAX = new.fax,
                      TYP = new.typ, KONTOFK = new.kontofk, cpowiaty = new.cpowiat, SYNCHRO = 1
        where SLODEF = :slodef AND SLOPOZ = new.ref ;

  end

  if(new.nip <> old.nip) then begin
    update dokumnag set nip=new.nip where dostawca = new.ref and (nip<>new.nip or nip is null);
  end
  if (new.nazwa <> old.nazwa) then
  begin
    update rozrach set slonazwa = new.nazwa
      where slodef in (select ref from slodef where typ = 'DOSTAWCY')
        and slopoz = new.ref;
  end

  -- aktulizacja opisu kont ksiegowych dla bieżącego roku
  if(new.nazwa<>old.nazwa) then begin
    currentyear = extract(year from current_timestamp(0));
    for select ref from slodef where typ = 'DOSTAWCY'
      into :slodef
    do begin
      execute procedure accounting_recalc_descript(:slodef,:currentyear, new.company, new.kontofk);
    end
  end

end^
SET TERM ; ^
