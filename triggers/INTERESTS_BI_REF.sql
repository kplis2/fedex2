--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INTERESTS_BI_REF FOR INTERESTS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('INTERESTS')
      returning_values new.REF;
end^
SET TERM ; ^
