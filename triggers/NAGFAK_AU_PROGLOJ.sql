--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_AU_PROGLOJ FOR NAGFAK                         
  ACTIVE AFTER UPDATE POSITION 7 
as
declare variable par varchar(255);
declare variable scpodmiot integer;
declare variable slodef_ref integer;
begin
  execute procedure GETCONFIG('WLACZPROGRAM') returning_values :par;
  -- wycofanie naliczenia punktow
  if ((new.akceptacja in (0,7,9,10) and old.akceptacja in (1,8))
    or (new.cpluczestid is null and old.cpluczestid is not null)
    or (new.cpluczestid <> old.cpluczestid)
    or (new.anulowanie<>0 and old.anulowanie=0)
  )then
    delete from cploper
    where cploper.typdok='NAGFAK' and cploper.refdok=new.ref;
  -- naliczanie punktow
  if (:par = '1') then begin
    if ((new.akceptacja in (1,8) and old.akceptacja in (0,7,9,10))
       or (new.cpluczestid <> old.cpluczestid)
       or (new.cpluczestid is not null and old.cpluczestid is null)
       or (new.anulowanie = 0 and old.anulowanie<>0)
       ) then begin
      if(new.cpluczestid is null) then begin
        -- szukamy podmiotu CRM dla klienta lub dostawcy
        if(new.zakup = 0 and new.klient > 0) then begin
          select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'KLIENCI' into :slodef_ref;
          select REF from CPODMIOTY
          where SLODEF = :slodef_ref
          and SLOPOZ = new.klient into :scpodmiot;
        end else if(new.zakup = 1 and new.dostawca > 0) then begin
          select min(SLODEF.REF) from SLODEF where SLODEF.TYP = 'DOSTAWCY' into :slodef_ref;
          select ref from CPODMIOTY
          where SLODEF = :slodef_ref
          and SLOPOZ = new.dostawca into :scpodmiot;
        end
        -- sprawdzamy unikalnosc uczestnikow programu
        if(:scpodmiot is not null and exists (select first 1 count(cpluczest.cpl) from cpluczest
               where cpluczest.cpodmiot = :scpodmiot
               and (cpluczest.datadeaktyw is null or cpluczest.datadeaktyw>=new.data)
               and (cpluczest.dataotw is null or cpluczest.dataotw<=new.data)
               group by cpluczest.cpl
               having (count(cpluczest.cpl) > 1)
              )
          ) then exception universal 'Niejednoznaczne wskazanie uczestnika programu lojalnościowego.';
      end
      execute procedure CRM_PROGRAMLOJ_OBLICZFORDOK('NAGFAK',new.ref,1,'A');
    end
  end
end^
SET TERM ; ^
