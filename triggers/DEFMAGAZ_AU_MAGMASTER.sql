--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFMAGAZ_AU_MAGMASTER FOR DEFMAGAZ                       
  ACTIVE AFTER UPDATE POSITION 1 
as
declare variable werref integer;
declare variable ilosc numeric(14,4);
begin
  if((new.magmaster <> old.magmaster) or (new.magmaster is null and old.magmaster is not null) or (new.magmaster is not null and old.magmaster is null))then begin
    if(old.magmaster <> '') then begin
      for select WERSJAREF, ILOSC from STANYIL where MAGAZYN = new.symbol
      into :werref, :ilosc
      do begin
        update STANYIL set ILOSCPOD = iloscpod - :ilosc where WERSJAREF = :werref and magazyn = old.magmaster;
      end
      if (not exists(select symbol from defmagaz where magmaster = old.magmaster)) then
        update defmagaz set ismagmaster = 0 where symbol = old.magmaster;
    end
    if(new.magmaster <> '') then begin
      for select WERSJAREF, ILOSC from STANYIL where MAGAZYN = new.symbol
      into :werref, :ilosc
      do begin
        update STANYIL set ILOSCpod = iloscpod + :ilosc where WERSJAREF = :werref and magazyn = new.magmaster;
      end
      update defmagaz set ismagmaster = 1 where symbol = new.magmaster;
    end
  end
end^
SET TERM ; ^
