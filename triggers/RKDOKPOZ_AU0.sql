--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_AU0 FOR RKDOKPOZ                       
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable slodef integer;
declare variable slopoz integer;
declare variable company integer;
begin
  if(coalesce(new.kwota,0)<>coalesce(old.kwota,0) or coalesce(new.kwotazl,0)<>coalesce(old.kwotazl,0)
        or coalesce(new.pm,'')<>coalesce(old.pm,'') or coalesce(new.inneks,0)<>coalesce(old.inneks,0)
        or coalesce(new.accept,0)<>coalesce(old.accept,0)) then
    execute procedure rkdok_obl_war(new.dokument);

  if (coalesce(new.konto,'') <> coalesce(old.konto,'') or coalesce(new.kwota,0) <> coalesce(old.kwota,0)
    or coalesce(new.rozrachunek,'') <> coalesce(old.rozrachunek,'') or coalesce(new.accept,0) <> coalesce(old.accept,0)
    or coalesce(new.anulowany,0) <> coalesce(old.anulowany,0)
  ) then
  begin
    select n.slodef, n.slopoz, r.company
      from rkdoknag n
        join rkrapkas r on (n.raport = r.ref)
      where n.ref = new.dokument
      into :slodef, :slopoz, :company;
    execute procedure rozrach_rkdokpoz_count(:slodef, :slopoz, new.konto, new.rozrachunek, :company);
    if (coalesce(old.rozrachunek,'') <> coalesce(new.rozrachunek,0)
      or coalesce(old.konto,'') <> coalesce(new.konto,'')
    ) then
      execute procedure rozrach_rkdokpoz_count(:slodef, :slopoz, old.konto, old.rozrachunek, :company);
  end
end^
SET TERM ; ^
