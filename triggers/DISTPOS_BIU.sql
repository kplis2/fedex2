--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DISTPOS_BIU FOR DISTPOS                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
declare variable pozycja smallint;
begin
  if (inserting) then begin
    if(new.status is null) then new.status = 0;
    if(coalesce(new.period,'') = '') then select period from decrees where ref = new.decrees into new.period;
    if(new.transdate is null) then select transdate from decrees where ref = new.decrees into new.transdate;
    if(new.company is null) then select company from decrees where ref = new.decrees into new.company;
  end
  if(inserting or
      (updating and
        (new.tempdictdef <> old.tempdictdef or new.tempdictdef2 <> old.tempdictdef2
         or new.tempdictdef3 <> old.tempdictdef3 or new.tempdictdef4 <> old.tempdictdef4
         or new.tempdictdef5 <> old.tempdictdef5 or new.tempdictdef6 <> old.tempdictdef6
         or new.tempsymbol <> old.tempsymbol or new.tempsymbol2 <> old.tempsymbol2
         or new.tempsymbol3 <> old.tempsymbol3 or new.tempsymbol4 <> old.tempsymbol4
         or new.tempsymbol5 <> old.tempsymbol5 or new.tempsymbol6 <> old.tempsymbol6

         or (new.tempdictdef is null and old.tempdictdef is not null)
         or (new.tempdictdef2 is null and old.tempdictdef2 is not null)
         or (new.tempdictdef3 is null and old.tempdictdef3 is not null)
         or (new.tempdictdef4 is null and old.tempdictdef4 is not null)
         or (new.tempdictdef5 is null and old.tempdictdef5 is not null)
         or (new.tempdictdef6 is null and old.tempdictdef6 is not null)
         or (new.tempsymbol is null and old.tempsymbol is not null)
         or (new.tempsymbol2 is null and old.tempsymbol2 is not null)
         or (new.tempsymbol3 is null and old.tempsymbol3 is not null)
         or (new.tempsymbol4 is null and old.tempsymbol4 is not null)
         or (new.tempsymbol5 is null and old.tempsymbol5 is not null)
         or (new.tempsymbol6 is null and old.tempsymbol6 is not null)

         or (new.tempdictdef is not null and old.tempdictdef is null)
         or (new.tempdictdef2 is not null and old.tempdictdef2 is null)
         or (new.tempdictdef3 is not null and old.tempdictdef3 is null)
         or (new.tempdictdef4 is not null and old.tempdictdef4 is null)
         or (new.tempdictdef5 is not null and old.tempdictdef5 is null)
         or (new.tempdictdef6 is not null and old.tempdictdef6 is null)
         or (new.tempsymbol is not null and old.tempsymbol is null)
         or (new.tempsymbol2 is not null and old.tempsymbol2 is null)
         or (new.tempsymbol3 is not null and old.tempsymbol3 is null)
         or (new.tempsymbol4 is not null and old.tempsymbol4 is null)
         or (new.tempsymbol5 is not null and old.tempsymbol5 is null)
         or (new.tempsymbol6 is not null and old.tempsymbol6 is null)
        )
      )
    ) then begin
      new.symbol = '';
      new.symbol2 = '';
      new.symbol3 = '';
      new.symbol4 = '';
      new.symbol5 = '';
      new.symbol6 = '';
      new.symbol7 = '';
      new.symbol8 = '';
      new.symbol9 = '';
      new.symbol10 = '';
      new.symbol11 = '';
      new.symbol12 = '';
      new.symbol13 = '';
      new.symbol14 = '';
      new.symbol15 = '';
      new.symbol16 = '';
      new.symbol17 = '';
      new.symbol18 = '';
      new.symbol19 = '';
      new.symbol20 = '';
      new.symbol21 = '';
      new.symbol22 = '';
      new.symbol23 = '';
      new.symbol24 = '';
      new.symbol25 = '';
      new.symbol26 = '';
      new.symbol27 = '';
      new.symbol28 = '';
      new.symbol29 = '';
      new.symbol30 = '';
      new.symbol31 = '';
      new.symbol32 = '';
      new.symbol33 = '';
      new.symbol34 = '';
      new.symbol35 = '';
      new.symbol36 = '';
      new.symbol37 = '';
      new.symbol38 = '';
      new.symbol39 = '';
      new.symbol40 = '';
      if(new.tempdictdef is not null and coalesce(new.tempsymbol,'') <> '') then begin
        select isdist from slodef where ref=new.tempdictdef into :pozycja;
        if(pozycja = 1) then new.symbol = new.tempsymbol;
        if(pozycja = 2) then new.symbol2 = new.tempsymbol;
        if(pozycja = 3) then new.symbol3 = new.tempsymbol;
        if(pozycja = 4) then new.symbol4 = new.tempsymbol;
        if(pozycja = 5) then new.symbol5 = new.tempsymbol;
        if(pozycja = 6) then new.symbol6 = new.tempsymbol;
        if(pozycja = 7) then new.symbol7 = new.tempsymbol;
        if(pozycja = 8) then new.symbol8 = new.tempsymbol;
        if(pozycja = 9) then new.symbol9 = new.tempsymbol;
        if(pozycja = 10) then new.symbol10 = new.tempsymbol;
        if(pozycja = 11) then new.symbol11 = new.tempsymbol;
        if(pozycja = 12) then new.symbol12 = new.tempsymbol;
        if(pozycja = 13) then new.symbol13 = new.tempsymbol;
        if(pozycja = 14) then new.symbol14 = new.tempsymbol;
        if(pozycja = 15) then new.symbol15 = new.tempsymbol;
        if(pozycja = 16) then new.symbol16 = new.tempsymbol;
        if(pozycja = 17) then new.symbol17 = new.tempsymbol;
        if(pozycja = 18) then new.symbol18 = new.tempsymbol;
        if(pozycja = 19) then new.symbol19 = new.tempsymbol;
        if(pozycja = 20) then new.symbol20 = new.tempsymbol;
        if(pozycja = 21) then new.symbol21 = new.tempsymbol;
        if(pozycja = 22) then new.symbol22 = new.tempsymbol;
        if(pozycja = 23) then new.symbol23 = new.tempsymbol;
        if(pozycja = 24) then new.symbol24 = new.tempsymbol;
        if(pozycja = 25) then new.symbol25 = new.tempsymbol;
        if(pozycja = 26) then new.symbol26 = new.tempsymbol;
        if(pozycja = 27) then new.symbol27 = new.tempsymbol;
        if(pozycja = 28) then new.symbol28 = new.tempsymbol;
        if(pozycja = 29) then new.symbol29 = new.tempsymbol;
        if(pozycja = 30) then new.symbol30 = new.tempsymbol;
        if(pozycja = 31) then new.symbol31 = new.tempsymbol;
        if(pozycja = 32) then new.symbol32 = new.tempsymbol;
        if(pozycja = 33) then new.symbol33 = new.tempsymbol;
        if(pozycja = 34) then new.symbol34 = new.tempsymbol;
        if(pozycja = 35) then new.symbol35 = new.tempsymbol;
        if(pozycja = 36) then new.symbol36 = new.tempsymbol;
        if(pozycja = 37) then new.symbol37 = new.tempsymbol;
        if(pozycja = 38) then new.symbol38 = new.tempsymbol;
        if(pozycja = 39) then new.symbol39 = new.tempsymbol;
        if(pozycja = 40) then new.symbol40 = new.tempsymbol;
      end
      if(new.tempdictdef2 is not null and coalesce(new.tempsymbol2,'') <> '') then begin
        select isdist from slodef where ref=new.tempdictdef2 into :pozycja;
        if(pozycja = 1) then new.symbol = new.tempsymbol2;
        if(pozycja = 2) then new.symbol2 = new.tempsymbol2;
        if(pozycja = 3) then new.symbol3 = new.tempsymbol2;
        if(pozycja = 4) then new.symbol4 = new.tempsymbol2;
        if(pozycja = 5) then new.symbol5 = new.tempsymbol2;
        if(pozycja = 6) then new.symbol6 = new.tempsymbol2;
        if(pozycja = 7) then new.symbol7 = new.tempsymbol2;
        if(pozycja = 8) then new.symbol8 = new.tempsymbol2;
        if(pozycja = 9) then new.symbol9 = new.tempsymbol2;
        if(pozycja = 10) then new.symbol10 = new.tempsymbol2;
        if(pozycja = 11) then new.symbol11 = new.tempsymbol2;
        if(pozycja = 12) then new.symbol12 = new.tempsymbol2;
        if(pozycja = 13) then new.symbol13 = new.tempsymbol2;
        if(pozycja = 14) then new.symbol14 = new.tempsymbol2;
        if(pozycja = 15) then new.symbol15 = new.tempsymbol2;
        if(pozycja = 16) then new.symbol16 = new.tempsymbol2;
        if(pozycja = 17) then new.symbol17 = new.tempsymbol2;
        if(pozycja = 18) then new.symbol18 = new.tempsymbol2;
        if(pozycja = 19) then new.symbol19 = new.tempsymbol2;
        if(pozycja = 20) then new.symbol20 = new.tempsymbol2;
        if(pozycja = 21) then new.symbol21 = new.tempsymbol2;
        if(pozycja = 22) then new.symbol22 = new.tempsymbol2;
        if(pozycja = 23) then new.symbol23 = new.tempsymbol2;
        if(pozycja = 24) then new.symbol24 = new.tempsymbol2;
        if(pozycja = 25) then new.symbol25 = new.tempsymbol2;
        if(pozycja = 26) then new.symbol26 = new.tempsymbol2;
        if(pozycja = 27) then new.symbol27 = new.tempsymbol2;
        if(pozycja = 28) then new.symbol28 = new.tempsymbol2;
        if(pozycja = 29) then new.symbol29 = new.tempsymbol2;
        if(pozycja = 30) then new.symbol30 = new.tempsymbol2;
        if(pozycja = 31) then new.symbol31 = new.tempsymbol3;
        if(pozycja = 32) then new.symbol32 = new.tempsymbol3;
        if(pozycja = 33) then new.symbol33 = new.tempsymbol3;
        if(pozycja = 34) then new.symbol34 = new.tempsymbol3;
        if(pozycja = 35) then new.symbol35 = new.tempsymbol3;
        if(pozycja = 36) then new.symbol36 = new.tempsymbol3;
        if(pozycja = 37) then new.symbol37 = new.tempsymbol3;
        if(pozycja = 38) then new.symbol38 = new.tempsymbol3;
        if(pozycja = 39) then new.symbol39 = new.tempsymbol3;
        if(pozycja = 40) then new.symbol40 = new.tempsymbol3;
      end
      if(new.tempdictdef3 is not null and coalesce(new.tempsymbol3,'') <> '') then begin
        select isdist from slodef where ref=new.tempdictdef3 into :pozycja;
        if(pozycja = 1) then new.symbol = new.tempsymbol3;
        if(pozycja = 2) then new.symbol2 = new.tempsymbol3;
        if(pozycja = 3) then new.symbol3 = new.tempsymbol3;
        if(pozycja = 4) then new.symbol4 = new.tempsymbol3;
        if(pozycja = 5) then new.symbol5 = new.tempsymbol3;
        if(pozycja = 6) then new.symbol6 = new.tempsymbol3;
        if(pozycja = 7) then new.symbol7 = new.tempsymbol3;
        if(pozycja = 8) then new.symbol8 = new.tempsymbol3;
        if(pozycja = 9) then new.symbol9 = new.tempsymbol3;
        if(pozycja = 10) then new.symbol10 = new.tempsymbol3;
        if(pozycja = 11) then new.symbol11 = new.tempsymbol3;
        if(pozycja = 12) then new.symbol12 = new.tempsymbol3;
        if(pozycja = 13) then new.symbol13 = new.tempsymbol3;
        if(pozycja = 14) then new.symbol14 = new.tempsymbol3;
        if(pozycja = 15) then new.symbol15 = new.tempsymbol3;
        if(pozycja = 16) then new.symbol16 = new.tempsymbol3;
        if(pozycja = 17) then new.symbol17 = new.tempsymbol3;
        if(pozycja = 18) then new.symbol18 = new.tempsymbol3;
        if(pozycja = 19) then new.symbol19 = new.tempsymbol3;
        if(pozycja = 20) then new.symbol20 = new.tempsymbol3;
        if(pozycja = 21) then new.symbol21 = new.tempsymbol3;
        if(pozycja = 22) then new.symbol22 = new.tempsymbol3;
        if(pozycja = 23) then new.symbol23 = new.tempsymbol3;
        if(pozycja = 24) then new.symbol24 = new.tempsymbol3;
        if(pozycja = 25) then new.symbol25 = new.tempsymbol3;
        if(pozycja = 26) then new.symbol26 = new.tempsymbol3;
        if(pozycja = 27) then new.symbol27 = new.tempsymbol3;
        if(pozycja = 28) then new.symbol28 = new.tempsymbol3;
        if(pozycja = 29) then new.symbol29 = new.tempsymbol3;
        if(pozycja = 30) then new.symbol30 = new.tempsymbol3;
        if(pozycja = 31) then new.symbol31 = new.tempsymbol3;
        if(pozycja = 32) then new.symbol32 = new.tempsymbol3;
        if(pozycja = 33) then new.symbol33 = new.tempsymbol3;
        if(pozycja = 34) then new.symbol34 = new.tempsymbol3;
        if(pozycja = 35) then new.symbol35 = new.tempsymbol3;
        if(pozycja = 36) then new.symbol36 = new.tempsymbol3;
        if(pozycja = 37) then new.symbol37 = new.tempsymbol3;
        if(pozycja = 38) then new.symbol38 = new.tempsymbol3;
        if(pozycja = 39) then new.symbol39 = new.tempsymbol3;
        if(pozycja = 40) then new.symbol40 = new.tempsymbol3;
      end
      if(new.tempdictdef4 is not null and coalesce(new.tempsymbol4,'') <> '') then begin
        select isdist from slodef where ref=new.tempdictdef4 into :pozycja;
        if(pozycja = 1) then new.symbol = new.tempsymbol4;
        if(pozycja = 2) then new.symbol2 = new.tempsymbol4;
        if(pozycja = 3) then new.symbol3 = new.tempsymbol4;
        if(pozycja = 4) then new.symbol4 = new.tempsymbol4;
        if(pozycja = 5) then new.symbol5 = new.tempsymbol4;
        if(pozycja = 6) then new.symbol6 = new.tempsymbol4;
        if(pozycja = 7) then new.symbol7 = new.tempsymbol4;
        if(pozycja = 8) then new.symbol8 = new.tempsymbol4;
        if(pozycja = 9) then new.symbol9 = new.tempsymbol4;
        if(pozycja = 10) then new.symbol10 = new.tempsymbol4;
        if(pozycja = 11) then new.symbol11 = new.tempsymbol4;
        if(pozycja = 12) then new.symbol12 = new.tempsymbol4;
        if(pozycja = 13) then new.symbol13 = new.tempsymbol4;
        if(pozycja = 14) then new.symbol14 = new.tempsymbol4;
        if(pozycja = 15) then new.symbol15 = new.tempsymbol4;
        if(pozycja = 16) then new.symbol16 = new.tempsymbol4;
        if(pozycja = 17) then new.symbol17 = new.tempsymbol4;
        if(pozycja = 18) then new.symbol18 = new.tempsymbol4;
        if(pozycja = 19) then new.symbol19 = new.tempsymbol4;
        if(pozycja = 20) then new.symbol20 = new.tempsymbol4;
        if(pozycja = 21) then new.symbol21 = new.tempsymbol4;
        if(pozycja = 22) then new.symbol22 = new.tempsymbol4;
        if(pozycja = 23) then new.symbol23 = new.tempsymbol4;
        if(pozycja = 24) then new.symbol24 = new.tempsymbol4;
        if(pozycja = 25) then new.symbol25 = new.tempsymbol4;
        if(pozycja = 26) then new.symbol26 = new.tempsymbol4;
        if(pozycja = 27) then new.symbol27 = new.tempsymbol4;
        if(pozycja = 28) then new.symbol28 = new.tempsymbol4;
        if(pozycja = 29) then new.symbol29 = new.tempsymbol4;
        if(pozycja = 30) then new.symbol30 = new.tempsymbol4;
        if(pozycja = 31) then new.symbol31 = new.tempsymbol4;
        if(pozycja = 32) then new.symbol32 = new.tempsymbol4;
        if(pozycja = 33) then new.symbol33 = new.tempsymbol4;
        if(pozycja = 34) then new.symbol34 = new.tempsymbol4;
        if(pozycja = 35) then new.symbol35 = new.tempsymbol4;
        if(pozycja = 36) then new.symbol36 = new.tempsymbol4;
        if(pozycja = 37) then new.symbol37 = new.tempsymbol4;
        if(pozycja = 38) then new.symbol38 = new.tempsymbol4;
        if(pozycja = 39) then new.symbol39 = new.tempsymbol4;
        if(pozycja = 40) then new.symbol40 = new.tempsymbol4;
      end
      if(new.tempdictdef5 is not null and coalesce(new.tempsymbol5,'') <> '') then begin
        select isdist from slodef where ref=new.tempdictdef5 into :pozycja;
        if(pozycja = 1) then new.symbol = new.tempsymbol5;
        if(pozycja = 2) then new.symbol2 = new.tempsymbol5;
        if(pozycja = 3) then new.symbol3 = new.tempsymbol5;
        if(pozycja = 4) then new.symbol4 = new.tempsymbol5;
        if(pozycja = 5) then new.symbol5 = new.tempsymbol5;
        if(pozycja = 6) then new.symbol6 = new.tempsymbol5;
        if(pozycja = 7) then new.symbol7 = new.tempsymbol5;
        if(pozycja = 8) then new.symbol8 = new.tempsymbol5;
        if(pozycja = 9) then new.symbol9 = new.tempsymbol5;
        if(pozycja = 10) then new.symbol10 = new.tempsymbol5;
        if(pozycja = 11) then new.symbol11 = new.tempsymbol5;
        if(pozycja = 12) then new.symbol12 = new.tempsymbol5;
        if(pozycja = 13) then new.symbol13 = new.tempsymbol5;
        if(pozycja = 14) then new.symbol14 = new.tempsymbol5;
        if(pozycja = 15) then new.symbol15 = new.tempsymbol5;
        if(pozycja = 16) then new.symbol16 = new.tempsymbol5;
        if(pozycja = 17) then new.symbol17 = new.tempsymbol5;
        if(pozycja = 18) then new.symbol18 = new.tempsymbol5;
        if(pozycja = 19) then new.symbol19 = new.tempsymbol5;
        if(pozycja = 20) then new.symbol20 = new.tempsymbol5;
        if(pozycja = 21) then new.symbol21 = new.tempsymbol5;
        if(pozycja = 22) then new.symbol22 = new.tempsymbol5;
        if(pozycja = 23) then new.symbol23 = new.tempsymbol5;
        if(pozycja = 24) then new.symbol24 = new.tempsymbol5;
        if(pozycja = 25) then new.symbol25 = new.tempsymbol5;
        if(pozycja = 26) then new.symbol26 = new.tempsymbol5;
        if(pozycja = 27) then new.symbol27 = new.tempsymbol5;
        if(pozycja = 28) then new.symbol28 = new.tempsymbol5;
        if(pozycja = 29) then new.symbol29 = new.tempsymbol5;
        if(pozycja = 30) then new.symbol30 = new.tempsymbol5;
        if(pozycja = 31) then new.symbol31 = new.tempsymbol5;
        if(pozycja = 32) then new.symbol32 = new.tempsymbol5;
        if(pozycja = 33) then new.symbol33 = new.tempsymbol5;
        if(pozycja = 34) then new.symbol34 = new.tempsymbol5;
        if(pozycja = 35) then new.symbol35 = new.tempsymbol5;
        if(pozycja = 36) then new.symbol36 = new.tempsymbol5;
        if(pozycja = 37) then new.symbol37 = new.tempsymbol5;
        if(pozycja = 38) then new.symbol38 = new.tempsymbol5;
        if(pozycja = 39) then new.symbol39 = new.tempsymbol5;
        if(pozycja = 40) then new.symbol40 = new.tempsymbol5;
      end
      if(new.tempdictdef6 is not null and coalesce(new.tempsymbol6,'') <> '') then begin
        select isdist from slodef where ref=new.tempdictdef6 into :pozycja;
        if(pozycja = 1) then new.symbol = new.tempsymbol6;
        if(pozycja = 2) then new.symbol2 = new.tempsymbol6;
        if(pozycja = 3) then new.symbol3 = new.tempsymbol6;
        if(pozycja = 4) then new.symbol4 = new.tempsymbol6;
        if(pozycja = 5) then new.symbol5 = new.tempsymbol6;
        if(pozycja = 6) then new.symbol6 = new.tempsymbol6;
        if(pozycja = 7) then new.symbol7 = new.tempsymbol6;
        if(pozycja = 8) then new.symbol8 = new.tempsymbol6;
        if(pozycja = 9) then new.symbol9 = new.tempsymbol6;
        if(pozycja = 10) then new.symbol10 = new.tempsymbol6;
        if(pozycja = 11) then new.symbol11 = new.tempsymbol6;
        if(pozycja = 12) then new.symbol12 = new.tempsymbol6;
        if(pozycja = 13) then new.symbol13 = new.tempsymbol6;
        if(pozycja = 14) then new.symbol14 = new.tempsymbol6;
        if(pozycja = 15) then new.symbol15 = new.tempsymbol6;
        if(pozycja = 16) then new.symbol16 = new.tempsymbol6;
        if(pozycja = 17) then new.symbol17 = new.tempsymbol6;
        if(pozycja = 18) then new.symbol18 = new.tempsymbol6;
        if(pozycja = 19) then new.symbol19 = new.tempsymbol6;
        if(pozycja = 20) then new.symbol20 = new.tempsymbol6;
        if(pozycja = 21) then new.symbol21 = new.tempsymbol6;
        if(pozycja = 22) then new.symbol22 = new.tempsymbol6;
        if(pozycja = 23) then new.symbol23 = new.tempsymbol6;
        if(pozycja = 24) then new.symbol24 = new.tempsymbol6;
        if(pozycja = 25) then new.symbol25 = new.tempsymbol6;
        if(pozycja = 26) then new.symbol26 = new.tempsymbol6;
        if(pozycja = 27) then new.symbol27 = new.tempsymbol6;
        if(pozycja = 28) then new.symbol28 = new.tempsymbol6;
        if(pozycja = 29) then new.symbol29 = new.tempsymbol6;
        if(pozycja = 30) then new.symbol30 = new.tempsymbol6;
        if(pozycja = 31) then new.symbol31 = new.tempsymbol6;
        if(pozycja = 32) then new.symbol32 = new.tempsymbol6;
        if(pozycja = 33) then new.symbol33 = new.tempsymbol6;
        if(pozycja = 34) then new.symbol34 = new.tempsymbol6;
        if(pozycja = 35) then new.symbol35 = new.tempsymbol6;
        if(pozycja = 36) then new.symbol36 = new.tempsymbol6;
        if(pozycja = 37) then new.symbol37 = new.tempsymbol6;
        if(pozycja = 38) then new.symbol38 = new.tempsymbol6;
        if(pozycja = 39) then new.symbol39 = new.tempsymbol6;
        if(pozycja = 40) then new.symbol40 = new.tempsymbol6;
      end
  end
end^
SET TERM ; ^
