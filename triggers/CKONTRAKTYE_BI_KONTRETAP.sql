--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKONTRAKTYE_BI_KONTRETAP FOR CKONTRAKTYE                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.nazwa is null) then new.nazwa = '';
  if (new.ckontrakt is not null) then
    select nazwa from ckontrakty where ref = new.ckontrakt
    into new.kontretap;
    if (new.nazwa <> '') then begin
      new.kontretap = new.kontretap||' - '||new.nazwa;
    end
end^
SET TERM ; ^
