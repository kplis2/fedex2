--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SLOPOZ_AI0 FOR SLOPOZ                         
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable cnt integer;
declare variable slodefcrm integer;
declare variable dimhier integer;
begin
  if(new.crm = 1) then begin
    select crm from slodef where ref=new.slownik into :slodefcrm;
    if(:slodefcrm = 1) then begin
      select count(*) from CPODMIOTY where SLODEF = new.slownik and SLOPOZ = new.ref into :cnt;
      if(:cnt > 0) then
        update CPODMIOTY set SKROT = new.kod, NAZWA = new.nazwa, KONTOFK = new.kontoks where SLODEF = new.slownik and SLOPOZ = new.ref ;
      else
        insert into CPODMIOTY(SLODEF, SLOPOZ,AKTYWNY, SKROT, NAZWA, KONTOFK) values(new.slownik, new.ref, 1, new.kod, new.nazwa, new.kontoks);
    end
  end
  -- synchronizacja elementów sownika z elementami wymiarów, w których sownik jest wskazany na definicji wymiaru
  -- a ma zaznaczoną autosynchronizacje ze slownikiem
  for
    select dimhierdef.ref
      from dimhierdef
      where dimhierdef.slodef = new.slownik and dimhierdef.elemsaddmeth = 2
      into :dimhier
  do begin
    insert into dimelemdef  (shortname, dimhier, slodef, slopoz, name, act)
       values (new.kod, :dimhier, new.slownik, new.ref, new.nazwa, new.akt);
  end
end^
SET TERM ; ^
