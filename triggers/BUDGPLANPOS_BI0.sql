--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BUDGPLANPOS_BI0 FOR BUDGPLANPOS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('BUDGPLANPOS')
      returning_values new.REF;
end^
SET TERM ; ^
