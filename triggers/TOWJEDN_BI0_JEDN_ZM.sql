--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWJEDN_BI0_JEDN_ZM FOR TOWJEDN                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable dodatkowa smallint;
begin
    if(new.dodatkowa>0) then
      if(exists(select first 1 1 from TOWJEDN where KTM=new.KTM and DODATKOWA=new.DODATKOWA and REF<>new.REF)) then
        exception universal 'Istnieje już jednostka dodatkowa '||new.dodatkowa;
end^
SET TERM ; ^
