--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMROZ_BD0 FOR DOKUMROZ                       
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable slodef integer;
declare variable slopoz integer;
declare variable bn varchar(1);
declare variable dokpozref integer;
declare variable opk smallint;
declare variable opktryb smallint;
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable bktm varchar(40);
declare variable bwersja integer;
declare variable back integer;
declare variable cnt integer;
declare variable wydania integer;
declare variable dokumnotref integer;
declare variable local integer;
declare variable dokmagref integer;
begin
  select DOKUMPOZ.KTM,DOKUMPOZ.wersja, DOKUMNAG.SLODEF, DOKUMNAG.SLOPOZ,DEFDOKUM.KAUCJABN,DEFDOKUM.OPAK,DOKUMPOZ.OPK,DOKUMPOZ.REF, DOKUMPOZ.BKTM, DOKUMPOZ.BWERSJA, DOKUMnag.ref
   from DOKUMPOZ
   left join DOKUMNAG on ( DOKUMNAG.ref = DOKUMPOZ.DOKUMENT)
   left join DEFDOKUM on(DOKUMNAG.TYP = DEFDOKUM.symbol)
   left join DEFMAGAZ on ( DOKUMNAG.magazyn = DEFMAGAZ.symbol)
    where DOKUMPOZ.ref = old.pozycja
    into :ktm,:wersja,  :slodef,:slopoz,:bn,:opktryb,:opk,:DOKPOZREF, :bktm, :bwersja, :dokmagref;
  if(old.ack = 1 or old.ack = 9) then begin
    back = 1;
    if(old.ack = 9) then begin
      ktm = :bktm;
      wersja = :bwersja;
    end
    if(old.kortoroz > 0) then begin
      select DEFDOKUM.OPAK, DEFDOKUM.kaucjabn, dokumpoz.ref, dokumpoz.opk
      from DOKUMROZ
      left join dokumpoz on (dokumroz.pozycja = dokumpoz.ref)
      left join DOKUMNAG on (DOKUMNAG.ref = dokumpoz.dokument)
      left join defdokum on (defdokum.symbol = dokumnag.typ)
      where dokumroz.ref = old.kortoroz
      into :opktryb, :bn, :dokpozref, :opk;
      back = 0;
    end
    execute procedure CHECK_LOCAL('DOKUMNAG',:dokmagref) returning_values :local;
    if(:local = 1) then begin
      if(:opktryb = 1 and :opk > 0) then begin
        if(:bn = 'B') then
          execute procedure OPK_ADD(:slodef,:slopoz,:ktm,:wersja,old.ilosc,old.cena,old.cenasbrutto,:dokpozref,:back);
        else
          execute procedure OPK_ADD(:slodef,:slopoz,:ktm,:wersja,old.ilosc,old.cena,old.cenasnetto,:dokpozref,:back);
      end
      if(exists(select ref from DOKUMNOTP where DOKUMROZKOR = old.ref)) then begin
        select DEFDOKUM.wydania
        from DOKUMPOZ
          left join DOKUMNAG on (DOKUMPOZ.dokument = DOKUMNAG.REF)
          left join DEFDOKUM on (DOKUMNAG.typ = DEFDOKUM.SYMBOL)
        where DOKUMPOZ.ref = old.pozycja
        into :wydania;
        if(:wydania = 1 and old.cenatoroz is null) then begin
/*          for select DOKUMNOTP.dokument
            from DOKUMNOTP where DOKUMNOTP.dokumrozkor = old.ref
            into :dokumnotref
          do begin
            execute procedure DOKUMNOT_ACK(:dokumnotref,0);
            delete from DOKUMNOTP where dokumnotp.dokumrozkor = old.ref and dokumnotp.dokument = :dokumnotref;
            if(exists(select * from DOKUMNOTP where DOKUMENT = :dokumnotref)) then
              execute procedure DOKUMNOT_ACK(:dokumnotref,1);
           else
              delete from DOKUMNOT where ref=:dokumnotref;
          end*/
        end else
          exception DOKUMROZ_HASNOTKOR;
      end
    end
  end
end^
SET TERM ; ^
