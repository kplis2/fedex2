--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCENKLI_BI_REF FOR DEFCENKLI                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DEFCENKLI')
      returning_values new.REF;
end^
SET TERM ; ^
