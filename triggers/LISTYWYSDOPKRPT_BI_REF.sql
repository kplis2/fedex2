--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDOPKRPT_BI_REF FOR LISTYWYSDOPKRPT                
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('LISTYWYSDOPKRPT')
      returning_values new.REF;
end^
SET TERM ; ^
