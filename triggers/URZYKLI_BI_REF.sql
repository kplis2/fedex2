--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER URZYKLI_BI_REF FOR URZYKLI                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('URZYKLI')
      returning_values new.REF;
end^
SET TERM ; ^
