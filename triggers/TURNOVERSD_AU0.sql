--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TURNOVERSD_AU0 FOR TURNOVERSD                     
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if ((new.debit = 0) and (new.credit = 0)) then
    delete from turnoversd where accounting = new.accounting and period = new.period and trndate = new.trndate;
end^
SET TERM ; ^
