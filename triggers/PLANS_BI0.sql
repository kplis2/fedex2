--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PLANS_BI0 FOR PLANS                          
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.status is null) then new.status = 0;
  if (new.keyamount is null or (new.keyamount <0 or (new.keyamount > 4))) then
    exception universal 'Min. ilosc kluczy to 0 max. 4';
--badanie nachodzących czasowo na siebie aktywnych planów - wg typu i dzialu
end^
SET TERM ; ^
