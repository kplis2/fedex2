--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKDOCS_AU_WFTHROWEVENT FOR BKDOCS                         
  ACTIVE AFTER UPDATE POSITION 10 
AS
begin
  if(old.status is null and new.status is not null or
     old.status=0 and new.status=1 or
     old.status=0 and new.status=2 or
     old.status=0 and new.status=3)then
  begin
     execute procedure wf_throwevent('BKDOC_CHANGE','SENTE.BKDOC',new.ref,
      coalesce(new.symbol, old.symbol, current_timestamp(0)));
  end
end^
SET TERM ; ^
