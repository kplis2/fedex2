--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCALDAYS_BIU_TIMESTR FOR EMPLCALDAYS                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 3 
as
declare variable daytype smallint_id;
begin
--Konwersja czasu na tekst

  if (inserting or new.daykind <> old.daykind
    or new.workstart <> old.workstart or new.workend <> old.workend
    or new.workstart2 is distinct from old.workstart2
    or new.workend2 is distinct from old.workend2
    or new.overtime_type is distinct from old.overtime_type
  ) then begin
    new.workstartstr = substring(new.workstart from 1 for 5);
    new.workendstr = substring(new.workend from 1 for 5);
    new.workstartstr2 = substring(new.workstart2 from 1 for 5);
    new.workendstr2 = substring(new.workend2 from 1 for 5);

    select daytype from edaykinds where ref = new.daykind into :daytype;
    --jezeli w dzien wolny ktos robi nadgodziny, to powinny sie wyswietlic godziny
    --jezeli jest 0:00-0:00 to zakladam ze to dzien wolny a nie nadgodziny 24h
    if (daytype <> 1) then
      if (new.workstart = '00:00' and new.workend = '00:00') then
      begin
        new.workstartstr = '';
        new.workendstr = '';
      end

    if (new.workstart2 = '00:00' and new.workend2 = '00:00') then
    begin
      new.workstartstr2 = '';
      new.workendstr2 = '';
    end

    --wyliczenie czasu pracy
    execute procedure count_worktime(new.workstart, new.workend, new.workstart2, new.workend2, :daytype)
      returning_values new.worktime;
    if (inserting or new.overtime_type = 3) then new.nomworktime = new.worktime;

    execute procedure durationstr2(new.nomworktime)
      returning_values new.nomworktimestr;
    execute procedure durationstr2(new.worktime)
      returning_values new.worktimestr;

    execute procedure count_nighthours(new.ecalendar, new.workstart, new.workend, new.workstart2, new.workend2)
      returning_values new.nighthours;
    execute procedure durationstr2(new.nighthours)
      returning_values new.nighthoursstr;
  end
end^
SET TERM ; ^
