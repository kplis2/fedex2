--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ETRAINNEEDS_BIU_BLANK FOR ETRAINNEEDS                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.subject = '') then new.subject = null;
  if (new.personcost is null) then new.personcost = 0;
  if (new.totalcost is null) then new.totalcost = 0;
  if (new.duration is null) then new.duration = 0;
  if (new.addpersoncost is null) then new.addpersoncost = 0;
end^
SET TERM ; ^
