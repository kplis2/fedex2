--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVDEVICES_BI_REF FOR SRVDEVICES                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SRVDEVICES')
      returning_values new.REF;
end^
SET TERM ; ^
