--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFMENU_BU_SCIEZKA FOR DEFMENU                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable symbol varchar(255);
begin
  if(coalesce(new.poziom,0)<>coalesce(old.poziom,0) or
     coalesce(new.elem,0)<>coalesce(old.elem,0) or
    new.sciezka='XXXRECALCULATE') then begin
    new.sciezka = '';
    if(new.poziom is not null and new.poziom<>0) then
      select sciezka from DEFMENU where ref=new.poziom into new.sciezka;
    if(new.sciezka is null) then new.sciezka = '';
    symbol = '';
    if(new.elem is not null) then
      select WARTOSC from DEFELEM where REF=new.elem into :symbol;
    new.sciezka = new.sciezka || :symbol;
  end
end^
SET TERM ; ^
