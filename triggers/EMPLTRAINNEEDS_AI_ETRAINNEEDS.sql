--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLTRAINNEEDS_AI_ETRAINNEEDS FOR EMPLTRAINNEEDS                 
  ACTIVE AFTER INSERT POSITION 0 
AS
  declare variable enumber numeric(14,2);
  declare variable costtype smallint;
  declare variable personcost numeric(14,2);
  declare variable totalcost numeric(14,2);
begin
  select count(ref)
    from empltrainneeds
    where etrainneed = new.etrainneed
    into :enumber;

  enumber = coalesce(:enumber, 0);

  select costtype, personcost, totalcost
    from etrainneeds
    where ref = new.etrainneed
    into :costtype, :personcost, :totalcost;

  if (costtype = 0) then
    totalcost = personcost * enumber;
  else if (costtype = 1 and enumber > 0) then
    personcost = totalcost / enumber;
  else if (costtype = 1 and enumber = 0) then
    personcost = 0;

  if (costtype is not null) then
    update etrainneeds set personcost = :personcost, totalcost = :totalcost where ref = new.etrainneed;
end^
SET TERM ; ^
