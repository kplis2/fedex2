--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRCALCPARS_BI FOR PRCALCPARS                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.partype = 'G') then
    select first 1 v.pvalue
      from prmethodpars p
        join prshcalcs c on (c.method = p.method)
        join prmethodparvals v on (v.method = p.method and v.prmethodpar = p.symbol)
      where c.ref = new.calc
        and p.symbol = new.symbol
        and v.fromdate <= current_date
      order by v.fromdate desc
      into new.pvalue;
end^
SET TERM ; ^
