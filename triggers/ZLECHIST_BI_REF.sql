--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZLECHIST_BI_REF FOR ZLECHIST                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ZLECHIST')
      returning_values new.REF;
end^
SET TERM ; ^
