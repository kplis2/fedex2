--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWJEDN_AU0 FOR TOWJEDN                        
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.z > 1) then
    update TOWJEDN set Z = 1 where KTM = new.ktm and Z = 2 and JEDN <> new.jedn;
  if(new.s > 1) then
    update TOWJEDN set S = 1 where KTM = new.ktm and S = 2 and JEDN <> new.jedn;
  if(new.c > 1) then
    update TOWJEDN set C = 1 where KTM = new.ktm and C = 2 and JEDN <> new.jedn;
  if(new.rola=1) then update TOWARY set ILOSCWOPAK=new.przelicz where KTM = new.ktm;
  if(new.rola=1 and old.rola<>1) then update TOWJEDN set rola=0 where KTM=new.ktm and ROLA=1 and REF<>new.REF;
  execute procedure TOWARY_OBLJEDN(new.ktm);
end^
SET TERM ; ^
