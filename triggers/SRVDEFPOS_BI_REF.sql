--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVDEFPOS_BI_REF FOR SRVDEFPOS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('SRVDEFPOS')
      returning_values new.REF;
end^
SET TERM ; ^
