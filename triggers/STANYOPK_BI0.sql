--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYOPK_BI0 FOR STANYOPK                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  new.zreal = 0;
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  else if(new.ktm is not null and new.wersja is not null) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  select akt from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.akt;
  select NAZWA, MIARA, CHODLIWY, grupa, usluga from TOWARY where KTM = new.ktm into new.nazwat, new.miara, new.chodliwy, new.grupa, new.usluga;
  if(new.ilzreal is null) then new.ilzreal =  0;
  if(new.wartmagzreal is null) then new.wartmagzreal = 0;
  if(new.kaucjazreal is null) then new.kaucjazreal = 0;
  if(new.kaucja is null) then new.kaucja  = 0;
  if(new.wartmag is null) then new.wartmag  = 0;
  if(new.cenaspr is null) then new.cenaspr = 0;
  if(new.data is null) then new.data = current_date;
  new.datazamk = null;
  new.stan  = new.ilosc - new.ilzreal;
  new.wartmagstan = new.wartmag - new.wartmagzreal;
  new.kaucjastan = new.kaucja - new.kaucjazreal;
end^
SET TERM ; ^
