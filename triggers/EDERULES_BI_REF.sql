--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDERULES_BI_REF FOR EDERULES                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EDERULES')
      returning_values new.REF;
  if(new.needsign is null) then new.needsign = 0;
end^
SET TERM ; ^
