--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DPCOSTUNITS_BU0 FOR DPCOSTUNITS                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.symbol = '') then exception bad_parameter 'Uzupełnij symbol!';
  if (new.name = '') then exception bad_parameter 'Uzupełnij nazwę!';
end^
SET TERM ; ^
