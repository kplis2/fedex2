--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWARY_BU0 FOR TOWARY                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable cnt integer;
declare variable stawka numeric(14,4);
declare variable aktuoddzial varchar(255);
declare variable haspkwiu smallint;
declare variable numer integer;
declare variable symbol varchar(40);
declare variable ilosc integer;
declare variable free integer;
begin

  if(new.usluga<>old.usluga) then begin
    select coalesce(tow.rights,''), coalesce(tow.rightsgroup,'')
      from towtypes tow where tow.numer = new.usluga
      into new.prawa, new.prawagrup;
  end
  if(new.ktm is null) then new.ktm = '';
  if(new.nazwa is null) then new.nazwa = '';
  if(new.ktm = '') then exception TOWARY_BEZKTM;
  if(new.nazwa = '') then exception TOWARY_BEZNAZWY;
  if(new.kodkresk is not null and new.kodkresk <> '' and
    (old.kodkresk is null or old.kodkresk = '')) then new.kodkreskadd = current_date;
  if(new.iloscwoddziale is null) then new.iloscwoddziale = 0;
  if(new.cena_zakn is null) then new.cena_zakn = 0;
  if(new.cena_zakb is null) then new.cena_zakb = 0;
  if(new.cena_fabn is null) then new.cena_fabn = 0;
  if(new.cena_fabb is null) then new.cena_fabb = 0;
  if(new.cena_kosztn is null) then new.cena_kosztn = 0;
  if(new.cena_kosztb is null) then new.cena_kosztb = 0;
  if(new.cena_zaknl is null) then new.cena_zaknl = 0;
  if(new.dniwazn is null) then new.dniwazn = 0;
  if(new.serial is null) then new.serial = 0;
  if(new.serjedn is null) then new.serjedn = 0;
  if(new.serpojedyn is null) then new.serpojedyn = 0;
  if(new.sercyfr is null) then new.sercyfr = 0;
  if(new.serautowyd is null) then new.serautowyd = 0;
  if(new.altposmode is null) then new.altposmode = 0;
  if(new.serial = 0) then begin
    new.serjedn = 0;
    new.serpojedyn = 0;
    new.sercyfr = 0;
    new.serautowyd = 0;
  end
  if((new.CENA_ZAKB <> old.CENA_ZAKB) or (new.CENA_ZAKN <> old.CENA_ZAKN)
      or (new.CENA_FABB <> old.CENA_FABB) or (new.CENA_FABN <> old.CENA_FABN)
      or (new.CENA_KOSZTB <> old.CENA_KOSZTB) or (new.CENA_KOSZTN <> old.CENA_KOSZTN)
  ) then
        new.lastcenzmiana = current_time;
  if(new.vat is not null and (new.vat <> old.vat or (old.vat is null))) then begin
    select count(*) from VAT where VAT.grupa = new.vat into :cnt;
    if(:cnt is null) then cnt = 0;
    if(:cnt = 0) then exception    TOWARY_BLEDNYVAT;
    select HASPKWIU from VAT where VAT.grupa = new.vat into :haspkwiu;
    if(:haspkwiu > 0) then begin
      if(new.pkwiu is null or new.pkwiu = '') then exception TOWARY_MUSTHAVEPKWIU;
    end
  end
  if (new.AKT = 1 and old.AKT=0) then begin
    numer=0;
    execute procedure Get_NUMBER('Kod w kasie', null, null, null, 0, null) returning_values :numer, :symbol;
    if (:numer=0) then exception GENERATOR_NOT_DEFINED 'Niepoprawnie wygenerowany kod zewnetrzny. Akceptacja niemożliwa.';
    else new.kodwkasie = cast(:numer as varchar(6));
  end
  else if(new.AKT=0 and old.AKT=1) then begin
    ilosc=0;
    numer = cast(old.kodwkasie as integer);
    select count(*) from numberfree where nazwa='Kod w kasie' and typ <>0 and number=:numer into :ilosc;
    if(ilosc=0) then
      execute procedure free_number('Kod w kasie', null, null, null, :numer, null) returning_values :numer;
    new.kodwkasie = '';
  end

  if (new.KTM<>old.KTM) then
    BEGIN
        execute procedure towary_ktm_check(old.ktm)
           returning_values free;

       if (free=0) then
        begin
            exception towary_ktm;
        end

    END

end^
SET TERM ; ^
