--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RAPORTY_BI1 FOR RAPORTY                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable lokres varchar(6);
begin
  if (new.stawkapaliwa is null) then new.stawkapaliwa = 0;
  if (new.kosztenpoz is null) then new.kosztenpoz = 0;
  if (new.kosztrob is null) then new.kosztrob = 0;
  if(new.ktm = '' or new.ktm is null)
    then exception universal 'Brak wartoci w polu KTM';
  if (new.numer is null)
    then exception universal 'Brak wartoci w polu Numer raportu';
  lokres = cast( extract( year from new.oddaty) as char(4));
  if(extract(month from new.oddaty) < 10) then
    lokres = :lokres ||'0' ||cast(extract(month from new.oddaty) as char(1));
  else begin
    lokres = :lokres ||cast(extract(month from new.oddaty) as char(2));
  end
  new.okres = lokres;
  --Sprawdzenie poprawnoc przedzialu
  if (new.oddaty > new.dodaty)
    then exception universal 'Zle zdefiniowany przedział dat. Pole Od daty większe niż Do daty';

  ---Sprawdzenie nachodzenia sie raportów
  if (exists (select ref from raporty
    where raporty.ktm = new.ktm and raporty.typ = new.typ and raporty.rodzaj = new.rodzaj and raporty.magazyn=new.magazyn
      and new.oddaty <= raporty.dodaty and new.dodaty >= raporty.oddaty))
  then exception universal 'Wprowadzony przedział nakada się z innym przdziałem';

end^
SET TERM ; ^
