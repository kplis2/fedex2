--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYIL_BI_REPLICAT FOR STANYIL                        
  ACTIVE BEFORE INSERT POSITION 1 
AS
begin
 execute procedure rp_trigger_bi('STANYIL',new.magazyn, new.wersjaref, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
