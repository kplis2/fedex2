--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLUCZEST_BI_NAZWA FOR CPLUCZEST                      
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable tryb integer;
declare variable nazwa varchar(255);
declare variable skrot varchar(60);
begin
  select SL.trybred, CP.nazwa, CP.skrot from cpodmioty CP left join slodef SL on (CP.slodef = SL.Ref) where CP.ref = new.cpodmiot
  into :tryb, :nazwa, :skrot;
  if (:tryb = 2) then  begin
  new.nazwa = :nazwa ;
  new.skrot = :skrot ;
  end
end^
SET TERM ; ^
