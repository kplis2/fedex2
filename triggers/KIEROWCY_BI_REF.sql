--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KIEROWCY_BI_REF FOR KIEROWCY                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('KIEROWCY')
      returning_values new.REF;
end^
SET TERM ; ^
