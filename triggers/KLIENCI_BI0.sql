--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_BI0 FOR KLIENCI                        
  ACTIVE BEFORE INSERT POSITION 1 
AS
  declare variable dl smallint;
  declare variable wymagajnip varchar(40);
  declare variable nazwafizycznych varchar(255);
  declare variable contactcontrol varchar(100);
  declare variable grupadom varchar(255);
  declare variable ccompany smallint;
begin
  if (new.powiazany is null) then
    new.powiazany = 0;
  if (new.fskrot is null) then
    new.fskrot = '';
  if (new.firma is null) then
    new.firma = 1;
  if (new.aktywny is null) then
    new.aktywny = 1;
  if (new.niewyplac is null) then
    new.niewyplac = 0;
  if (new.sposplatbezp is null) then
    new.sposplatbezp = 0;
  if (new.zagraniczny is null) then
    new.zagraniczny = 0;
  if (new.language is null) then
    new.language = 0;
  if (new.fskrot = '') then
    exception KLIENCI_FSKROTEMPTY;
  if (new.rachunek is null) then
    new.rachunek = '';
  if(new.sposdostauto is null) then
    new.sposdostauto = 1;
  if(new.krajid = '') then new.krajid = null;
  if ((new.kraj is null or new.kraj = '') and new.krajid is not null) then
    select name from countries where symbol = new.krajid into new.kraj;
  execute procedure get_config('KLIENCIHIST',1) returning_values new.historia;
  if (new.historia is null) then new.historia = 0;
  if (new.firma = 0) then
  begin
    if (new.imie is null) then
      new.imie = '';
    if (new.nazwisko is null)
      then new.nazwisko = '';
    execute procedure getconfig('NAZWAFIZYCZNYCH')
      returning_values :nazwafizycznych;
    if (nazwafizycznych = '1' and new.nazwa is null) then --XXX MKD dodanie and nazwa is null  inczej problemy przy imporcie HERMON
      new.nazwa = new.nazwisko || ' ' || new.imie ;
    else if (new.nazwa is null) then --XXX MKD dodanie warunku nazwa is null  inczej problemy przy imporcie HERMON
      new.nazwa = new.imie || ' ' || new.nazwisko;
  end
  if (new.nazwa is null or new.nazwa = '') then
    exception KLIENCI_PUSTANAZWA;
  if (new.crm is null) then
    new.crm = 0;
  if (new.bank = '') then
    new.bank = null;
  if (new.oddzial = '') then
    new.oddzial = null;

  grupadom = '';
  if (new.grupa is null) then
    execute procedure GETCONFIG('DGRUPA')
      returning_values :grupadom;
  if(:grupadom is not null and :grupadom<>'') then new.grupa = :grupadom;
  if (new.grupa is null) then
    select min(REF) from GRUPYKLI
      into new.grupa;
  if (new.grupa is not null) then
    select OPIS from GRUPYKLI where REF=new.grupa
      into new.grupanazwa;


  if (new.korespond is null) then
    new.korespond = 0;
  if (new.korespond = 0) then
  begin
    new.kulica = new.ulica;
    new.knrdomu = new.nrdomu;
    new.knrlokalu = new.nrlokalu;
    new.kmiasto = new.miasto;
    new.kkodp = new.kodp;
    new.kpoczta = new.poczta;
    new.kkraj = new.kraj;
  end

  if (new.firma=1 and new.zagraniczny=0 and new.nip='') then
  begin
    execute procedure GETCONFIG('WYMAGAJNIP')
      returning_values :wymagajnip;
    if (wymagajnip = '1') then
      exception BRAK_NIPU;
  end

  -- utworzenie prostego nipu
  new.prostynip = replace(new.nip, '-', '');
  new.regdate = current_date;
  -- sprawdzenie poprawnosci danych teleadresowych i obsluga ksiazki tel.
  execute procedure get_config('CONTACTCONTROL',2) returning_values :contactcontrol;
  if (:contactcontrol is null) then contactcontrol = '0';
  if (:contactcontrol = '1') then begin
    -- sprawdzenie poprawnosci email
    if (new.email is not null) then
      execute procedure contact_control('EMAIL',new.email,0,'KLIENCI','EMAIL',new.ref,new.fskrot);

    -- sprawdzenie poprawnosci telefonu
    if (new.telefon is not null) then
      execute procedure contact_control('PHONE',new.telefon,0,'KLIENCI','TELEFON',new.ref,new.fskrot);

    -- sprawdzenie poprawnosci komorki
    if (new.komorka is not null) then
      execute procedure contact_control('CELL',new.komorka,0,'KLIENCI','KOMORKA',new.ref,new.fskrot);

    -- sprawdzenie poprawnosci faxu
    if (new.fax is not null) then
      execute procedure contact_control('FAX',new.fax,0,'KLIENCI','FAX',new.ref,new.fskrot);
  end
  -- nadanie kodu ksiegowego
  if (new.kontofk is null or new.kontofk='') then begin
      execute procedure X_BLANK_KODFK(new.company, 'KLIENCI', new.ref, null) returning_values new.kontofk;
  end
  -- sprawdzenie dlugosci kodu ksiegowego
  select KODLN from slodef where typ='KLIENCI'
    into :dl;
  if (dl is not null) then
  if (coalesce(char_length(new.kontofk),0)<>0 and coalesce(char_length(new.kontofk),0)<>dl) then  -- [DG] XXX ZG119346
    exception BAD_LENGTH_FKKOD;

  -- Blankowanie adresu, jezeli adres jest nieuzupelniony to przepisujemy z glownego
  if(coalesce(new.dnazwa,'') = '' and coalesce(new.dulica,'') = '' and
     coalesce(new.dmiasto,'') = '' and coalesce(new.dkodp,'') = '' and
     coalesce(new.dkraj,'') = '' and coalesce(new.dpoczta,'') = '') then
  begin
    new.dnazwa = new.nazwa;
    new.dulica = new.ulica;
    new.dnrdomu = new.nrdomu;
    new.dnrlokalu = new.nrlokalu;
    new.dmiasto = new.miasto;
    new.dkodp = new.kodp;
    new.dkraj = new.kraj;
    new.dpoczta = new.poczta;
  end

  new.vatactive = coalesce(new.vatactive, 1);
  new.vatconnected = coalesce(new.vatconnected,0);
end^
SET TERM ; ^
