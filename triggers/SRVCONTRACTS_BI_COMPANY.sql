--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SRVCONTRACTS_BI_COMPANY FOR SRVCONTRACTS                   
  ACTIVE BEFORE INSERT POSITION 1 
AS
begin
  if (new.oddzial is null) then
    execute procedure GETCONFIG('AKTUODDZIAL') returning_values new.oddzial;
  select company from oddzialy where oddzial = new.oddzial into new.company;
end^
SET TERM ; ^
