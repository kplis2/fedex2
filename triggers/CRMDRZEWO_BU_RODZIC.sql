--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CRMDRZEWO_BU_RODZIC FOR CRMDRZEWO                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable rodzic integer;
begin
  if (coalesce(new.rodzic, 0) <> coalesce(old.rodzic, 0)) then
    execute procedure CRMDRZEWO_RODZIC(new.rodzic, new.ref);
end^
SET TERM ; ^
