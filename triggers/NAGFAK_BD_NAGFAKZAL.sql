--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_BD_NAGFAKZAL FOR NAGFAK                         
  ACTIVE BEFORE DELETE POSITION 0 
AS
  declare variable local smallint;
  declare variable localfak smallint;
  declare variable ref integer  ;
begin
  execute procedure CHECK_LOCAL('NAGFAK',old.ref) returning_values :local;
  if (:local = 0) then begin
    for select nagfakzal.faktura from nagfakzal
      where nagfakzal.fakturazal = old.ref
    into :ref
    do begin
      execute procedure check_local('NAGFAK',:ref) returning_values :localfak;
      if (:localfak = 0) then
      begin
        delete from nagfakzal where nagfakzal.faktura = :ref and nagfakzal.fakturazal = old.ref;
      end
    end
  end
end^
SET TERM ; ^
