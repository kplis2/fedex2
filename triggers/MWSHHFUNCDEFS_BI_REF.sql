--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSHHFUNCDEFS_BI_REF FOR MWSHHFUNCDEFS                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null) then
    execute procedure gen_ref('MWSHHFUNCDEFS') returning_values new.ref;
end^
SET TERM ; ^
