--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHORTAGES_BD0 FOR PRSHORTAGES                    
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if(old.dokumnag is not null or coalesce(old.amountzreal,0) > 0) then
    exception prshortages_error 'Do braku wystawiono dok.mag. Usunięcie braku niemożliwe';
end^
SET TERM ; ^
