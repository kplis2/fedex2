--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHMAT_AI0 FOR PRSHMAT                        
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable test integer;
declare variable rodzaj char(1);
begin
  if(new.subsheet is not null) then begin
    execute procedure prsheets_no_matloop_up(new.subsheet,new.sheet,50)
    returning_values :test;
  end
  if((new.econdition is not null and new.econdition <>'')
    or (new.enetquantity is not null and new.enetquantity <>'')
    or (new.egrossquantity is not null and new.egrossquantity <>'')
  ) then begin
     rodzaj = '';
     if(new.mattype = 0) then rodzaj = 'S';
     if(new.mattype = 1) then rodzaj = 'P';
     if(new.mattype = 2) then rodzaj = 'O';
     execute procedure prsheets_calculate(new.sheet,'',:rodzaj);
  end
end^
SET TERM ; ^
