--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKONTRAKTY_DOCUMENT_BU FOR CKONTRAKTY                     
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.faza <> old.faza) then
  begin
    select cfazy.document from cfazy where cfazy.ref = new.faza into new.document;
  end
end^
SET TERM ; ^
