--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAMUPVIEW_BD FOR POZZAMUPVIEW                   
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  delete from pozzamup
  where pozzamup.nagzamup = old.ref
    and pozzamup.refpoz = old.refpoz;
end^
SET TERM ; ^
