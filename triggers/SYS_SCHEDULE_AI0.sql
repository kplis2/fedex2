--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SYS_SCHEDULE_AI0 FOR SYS_SCHEDULE                   
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  insert into sys_schedule_activity_rules(sys_schedule,"TYPE",ACTIVE_POINT,START_POINT,STOP_POINT,WHOLEDAY)
  values(new.ref,0,0,'00:00','23:59',1);
end^
SET TERM ; ^
