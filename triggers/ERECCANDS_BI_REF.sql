--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ERECCANDS_BI_REF FOR ERECCANDS                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ERECCANDS')
      returning_values new.REF;
end^
SET TERM ; ^
