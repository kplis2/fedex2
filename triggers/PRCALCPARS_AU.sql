--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRCALCPARS_AU FOR PRCALCPARS                     
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable paramname varchar(255);
declare variable parent integer;
declare variable ret numeric(16,6);
begin
  -- jesli parametr ulegl zmianie to przelicz wyrazenia zalezne
  if(coalesce(new.pvalue,0)<>coalesce(old.pvalue,0)) then begin
    paramname = 'PAR('||new.symbol||')';
    for select parent from prcalccols where calc=new.calc and expr=:paramname and calctype=1 and modified=0
      into :parent
      do begin
        execute procedure PRSHCALCS_SUBEXECUTE(:parent) returning_values :ret;
        update prcalccols set cvalue=coalesce(:ret,0) where ref=:parent;
      end
  end
end^
SET TERM ; ^
