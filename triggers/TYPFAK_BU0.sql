--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TYPFAK_BU0 FOR TYPFAK                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.korekta = 0) then new.korwart = 0;
  if (new.korsamo is null) then new.korsamo = 0;
  if(new.dozaplatynetto is null) then new.dozaplatynetto = 0;
  if (new.drukbezvat is null) then new.drukbezvat = 0;
end^
SET TERM ; ^
