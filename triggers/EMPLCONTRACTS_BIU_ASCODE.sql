--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRACTS_BIU_ASCODE FOR EMPLCONTRACTS                  
  ACTIVE BEFORE INSERT OR UPDATE POSITION 3 
AS
  declare variable personnames varchar(120);
begin
  if (inserting or
      new.ascode <> old.ascode or (new.ascode is null and old.ascode is not null) or (new.ascode is not null and old.ascode is null) or
      new.iflags <> old.iflags or (new.iflags is null and old.iflags is not null) or (new.iflags is not null and old.iflags is null)
  ) then
    --iflags moze miec postac ';' a takie cos tez powinno traktowac jako puste
    if (new.ascode is null and coalesce(char_length(new.iflags),0) > 1) then  -- [DG] XXX ZG119346
    begin
      select personnames from employees where ref = new.employee into :personnames;
      exception universal 'Proszę uzupełnić tytuł ubezpieczenia dla pracownika '||personnames;
    end
end^
SET TERM ; ^
