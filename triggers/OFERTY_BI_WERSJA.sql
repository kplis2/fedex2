--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OFERTY_BI_WERSJA FOR OFERTY                         
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (coalesce(new.orgoferta,0) <> 0) then
  begin
    select coalesce(max(o.wersja),1)+1
      from oferty o
      where (o.orgoferta = new.orgoferta) or (o.ref = new.orgoferta)
      into new.wersja;
  end
end^
SET TERM ; ^
