--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DISTPOS_BD_ORDER FOR DISTPOS                        
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then exit;
  update distpos set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and decrees = old.decrees;
end^
SET TERM ; ^
