--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTOCK_AI0 FOR MWSSTOCK                       
  ACTIVE AFTER INSERT POSITION 0 
AS
declare variable autorefill smallint;
begin
  -- jak przychodzi nowy towar na lokacje to trzeba odznaczyc lokacje do dowiezienia towaru
  if (new.ordered + new.quantity > 0) then
  begin
    select w.autogoodsrefill
      from mwsconstlocs m
        join whsecs w on (w.ref = m.whsec)
      where m.ref = new.mwsconstloc
      into autorefill;
    if (autorefill = 1) then
      update mwsconstlocs set refill = 0 where ref = new.mwsconstloc and refill = 1;
  end
  if (new.mwsconstloc is not null) then
    execute procedure MWS_CALC_FREELOCS (new.mwsconstloc,1);
  -- zweryfikowanie stanyu arkusza inwentaryzacyjnego
  if (new.quantity <> 0 and new.locdest = 6) then
    execute procedure mws_check_inwenta_status(new.ref,new.good, new.vers, new.lot, new.wh);
  if (new.quantity > 0 or new.blocked > 0 or new.ordered > 0) then
    execute procedure xk_mws_recalc_sellav_for_stock (new.vers,new.wh);
end^
SET TERM ; ^
