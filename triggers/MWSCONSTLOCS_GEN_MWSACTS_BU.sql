--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSCONSTLOCS_GEN_MWSACTS_BU FOR MWSACTS                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable proc varchar(1024);
declare variable mwsordtype integer;
declare variable good varchar(40);
declare variable vers integer;
declare variable mwsord integer;
declare variable wh varchar(3);
declare variable whsec integer;
declare variable wharea integer;
declare variable maxlevel smallint;
declare variable priority smallint;
declare variable mixedpallgroup smallint;
declare variable quantity numeric(14,4);
declare variable mwsconstlocl integer;
declare variable wharealogl integer;
declare variable ispal smallint;
declare variable paltype varchar(40);
declare variable blockconstlocchange smallint;
declare variable rec smallint;
declare variable bwharea integer;
declare variable h numeric(14,2);
declare variable w numeric(14,2);
declare variable l numeric(14,2);
declare variable refilltry smallint;
declare variable mwsactref integer;
declare variable procwh varchar(1024);
begin
  blockconstlocchange = new.blockconstlocchange;
  if (blockconstlocchange is null) then blockconstlocchange = 0;
  if (new.manmwsacts > 0) then exit;
  if (new.status = 5 and old.status <> 5 and blockconstlocchange = 0 and new.mwsordtypedest = 2) then
  begin
    proc = null;
    procwh = null;
    select w.mwsconstlocprocsql
      from mwsordtypes4wh w
      where w.wh = new.wh and w.mwsordtype = new.mwsordtype
      into procwh;
    select mt.mwsconstlocprocsql, m.mwsordtype, m.rec, m.bwharea
      from mwsords m
        join mwsordtypes mt on (m.mwsordtype = mt.ref)
      where m.ref = new.mwsord
      into :proc, mwsordtype, rec, bwharea;
    if (procwh is not null and procwh <> '') then
      proc = procwh;
    if (new.palgroup is not null) then
    begin
      select first 1 a.mwsconstlocl, a.wharealogl
        from mwsacts a
          where palgroup = new.palgroup and status = 5 and mwsconstlocl is not null
            and mwsord = new.mwsord
        into mwsconstlocl, wharealogl;
      select good from mwsacts where ref = new.palgroup into paltype;
    end
    if (mwsconstlocl is not null) then
    begin
      new.mwsconstlocl = mwsconstlocl;
      new.wharealogl = wharealogl;
    end
    if (proc <> '' and proc is not null and mwsconstlocl is null) then
    begin
      if (new.good is null) then good = ''; else good = new.good;
      if (new.vers is null) then vers = 0; else vers = new.vers;
      if (new.mwsord is null) then mwsord = 0; else mwsord = new.mwsord;
      if (new.wh is null) then wh = ''; else wh = new.wh;
      if (new.whsec is null) then whsec = 0; else whsec = new.whsec;
      if (rec = 0) then
      begin
        if (wharea is null) then wharea = 0;
        else wharea = wharea;
      end else if (rec = 1) then
      begin
        if (bwharea is null) then wharea = 0;
        else wharea = bwharea;
      end
      if (new.maxlevel is null) then maxlevel = 0; else maxlevel = new.maxlevel;
      if (new.priority is null) then priority = 0; else priority = new.priority;
      if (new.mixedpallgroup is null) then mixedpallgroup = 0; else mixedpallgroup = new.mixedpallgroup;
      if (new.quantity is null) then quantity = 0; else quantity = new.quantity;
      if (new.ispal is null) then ispal = 0; else ispal = new.ispal;
      if (new.l is null) then l = 0; else l = new.l;
      if (new.w is null) then w = 0; else w = new.w;
      if (new.h is null) then h = 0; else h = new.h;
      if (new.refilltry is null) then refilltry = 0; else refilltry = new.refilltry;
      mwsactref = new.ref;
      if (paltype is null) then paltype = '';
      proc = 'select mwsconstlocl, wharealogl from '||proc||'('''||good||''','||
          vers||','||mwsactref||','||mwsord||','||mwsordtype||','''||
          wh||''','||whsec||','||wharea||','||maxlevel||','||
          priority||','||ispal||','||mixedpallgroup||','||quantity||','''||paltype||
          ''','||l||','||w||','||h||','||refilltry||')';
      execute statement proc
        into mwsconstlocl, wharealogl;
      new.mwsconstlocl = mwsconstlocl;
      new.wharealogl = wharealogl;
    end
  end
end^
SET TERM ; ^
