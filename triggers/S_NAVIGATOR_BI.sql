--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_NAVIGATOR_BI FOR S_NAVIGATOR                    
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.initializing is null) then new.initializing = 1;
  -- blankowanie namespace
  if(coalesce(new.namespace,'')='') then execute procedure GET_GLOBAL_PARAM('NAMESPACE') returning_values new.namespace;
  if(coalesce(new.namespace,'')='') then new.namespace = 'SENTE';
  -- blankowanie refa
  if(new.ref is null or new.ref='') then
    new.ref = new.namespace||cast(gen_id(GEN_S_APPINI,1) as varchar(20));
  -- blankowanie PREV
  if(new.prev is null or new.prev='') then new.prev = '<NONE>';
  if(position('=' in new.prev)>0) then begin
    new.prev = substring(new.prev from 1 for position('=' in new.prev)-1);
  end
  -- nulowanie PARENT
  if(new.parent='') then new.parent = NULL;
  -- obliczanie childrencount
  if(exists(select first 1 1  from s_navigator where parent=new.ref)) then new.childrencount = 1;
  else new.childrencount = 0;
  if(new.parent is not null) then
    update s_navigator set childrencount = 1 where ref = new.parent and childrencount=0;
end^
SET TERM ; ^
