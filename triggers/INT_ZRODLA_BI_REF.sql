--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INT_ZRODLA_BI_REF FOR INT_ZRODLA                     
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure GEN_REF('INT_ZRODLA')
      returning_values new.ref;
end^
SET TERM ; ^
