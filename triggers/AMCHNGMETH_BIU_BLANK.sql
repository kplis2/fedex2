--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER AMCHNGMETH_BIU_BLANK FOR AMCHNGMETH                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
declare variable company companies_id;
begin
  select company from fxdassets
    where ref = new.fxdasset
    into :company;  --BS97517
  select ref from amperiods
    where ammonth = extract(month from new.docdate)
      and amyear = extract(year from new.docdate)
      and company = :company
    into new.amperiod;
  if (new.tameth = '') then new.tameth = null;
  if (new.tarate is null) then new.tarate = 0;
  if (new.tacrate is null) then new.tacrate = 0;
  if (new.fameth = '') then new.fameth = null;
  if (new.farate is null) then new.farate = 0;
  if (new.facrate is null) then new.facrate = 0;
end^
SET TERM ; ^
