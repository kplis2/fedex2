--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_SLOWNIKI_EHRLE_BI FOR X_SLOWNIKI_EHRLE               
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_x_slowniki_ehrle,1);
end^
SET TERM ; ^
