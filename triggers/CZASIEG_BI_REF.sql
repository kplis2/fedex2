--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CZASIEG_BI_REF FOR CZASIEG                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CZASIEG')
      returning_values new.REF;
end^
SET TERM ; ^
