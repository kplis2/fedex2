--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOSTAWCY_AU_UPDATE_SRVCONTRACTS FOR DOSTAWCY                       
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if ((new.telefon<>old.telefon) or (new.id <> old.id) or (old.prostynip<>new.prostynip) or
      (new.ulica<>old.ulica) or (new.nrdomu <> old.nrdomu) or (new.nrlokalu <> old.nrlokalu) or
      (new.miasto<>old.miasto) or (new.kodp<>old.kodp) ) then
  update srvcontracts s
    set s.name = new.id,
        s.taxid = new.prostynip,
        s.street = new.ulica,
        s.houseno = new.nrdomu,
        s.localno = new.nrlokalu,
        s.city = new.miasto,
        s.postcode = new.kodp,
        s.phone = new.telefon
  where s.dictpos = new.ref and s.dictdef = 6;
end^
SET TERM ; ^
