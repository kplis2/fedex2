--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWAKCESGRP_BI_REF FOR TOWAKCESGRP                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('TOWAKCESGRP')
      returning_values new.REF;
end^
SET TERM ; ^
