--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDECLPOS_BI_REF FOR EDECLPOS                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
 if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EDECLPOS')
      returning_values new.REF;
end^
SET TERM ; ^
