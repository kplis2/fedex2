--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FLDFLAGSGRP_BD_FLDFLAGSDEF_ORDE FOR FLDFLAGSGRP                    
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  update fldflagsdef set ord = 0 where fldflagsgrp = old.symbol;
end^
SET TERM ; ^
