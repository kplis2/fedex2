--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDES_BU0 FOR PRSCHEDGUIDES                  
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable precis smallint;
declare variable ilosc numeric(14,4);
begin
  if (new.prschedguideorg is null) then new.prschedguideorg = new.ref;
  if(new.amount is null) then new.amount = 0;
  if(new.amountzreal is null) then new.amountzreal = 0;
  if(new.pggamountcalc is null) then new.pggamountcalc = 0;
  if(new.statusmat is null) then new.statusmat = 0;
  if (new.amountshortagedecl is null) then new.amountshortagedecl = 0;
  if (new.amountshortage is null) then new.amountshortage = 0;
  if (new.kamountshortage is null) then new.kamountshortage = 0;
  if (new.prpreprod is null) then new.prpreprod = 0;
  if (new.anulowano is null) then new.anulowano = 0;
--  if(new.amount = 0 and new.pggamountcalc = 0) then exception prschedguides_error substr('Ilość oczekiwana z przewodnika ('||new.symbol||') = 0. Zmiana ilości niemożliwa',1,75);
  if(new.amountdecl <> old.amountdecl) then begin
    new.amount = new.amountdecl;
  end
  if((new.status <> old.status or  new.stan <> old.stan) and new.stan <> 'N') then begin
    --sprawdzenie, czy nie jest juz zamkniety - wowczas brak blokad
    if(new.status >=3) then new.stan = 'N';
  end
  --ustawienie ilosci po realizacji przewodnika z ostatniej operacji produkcyjnej
  if(new.status = 2 and old.status < 2) then
    select p1.amountresult from prschedopers p1 where p1.guide = new.ref and p1.number =
      (select max(number) from prschedopers p2 where p2.guide = new.ref and reported > 0) into new.amount;
--wyrownywanie koncowek w ilosciach
  select m.dokladnosc
    from towary t
    left join miara m on (t.miara = m.miara)
    where ktm = new.ktm
  into :precis;
  if(new.amount <> floor(new.amount)) then
    new.amount = floor(new.amount * power(10,:precis))/power(10,:precis);
  if(new.amountdecl <> floor(new.amountdecl)) then
    new.amountdecl = floor(new.amountdecl * power(10,:precis))/power(10,:precis);
--zamkniecie gdy amount = 0
  if(new.status = 2 and old.status < 2 and new.amount = 0) then new.status = 3;
  if (coalesce(old.prpriority,0) <> coalesce(new.prpriority,0) and new.prguidepriority is null) then
    new.prguidepriority = new.prpriority;
  if (new.amount <> old.amount) then
    new.amountshortagedecl = new.amount * new.kamountshortage;
  if (coalesce(new.amountzreal,0) <> coalesce(old.amountzreal,0)
    or coalesce(new.amount,0) <> coalesce(old.amount,0)
    or coalesce(new.amountshortagedecl,0) <> coalesce(old.amountshortagedecl,0)
    or coalesce(new.amountshortage,0) <> coalesce(old.amountshortage,0)
  ) then
  begin
    if (coalesce(new.amountzreal,0) + coalesce(new.amountshortage,0) >
      coalesce(new.amount,0) + coalesce(new.amountshortagedecl,0)
    ) then
    begin
      ilosc = coalesce(new.amount,0) + coalesce(new.amountshortagedecl,0);
      exception prschedguides_error 'Realizacja zbyt dużej ilości. Przyjmij ponad limit: '||ilosc;
    end
  end
end^
SET TERM ; ^
