--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECALDAYS_BIU FOR ECALDAYS                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
as
  declare variable daylength double precision;
begin
  /* automatyczne wypelnienie pól YEAR, MONTH, DAY */
  new.cyear  = cast(extract(year from new.cdate)as integer);
  new.cmonth = cast(extract(month from new.cdate)as smallint);
  new.cday   = cast(extract(day from new.cdate)as smallint);
  new.dayofweek = cast(extract(weekday from new.cdate)as smallint);

  new.workstartstr = substring(new.workstart from 1 for 5);
  new.workendstr   = substring(new.workend from 1 for 5);
  if (new.workstartstr containing '00:00' and new.daykind <> 1) then
    new.workstartstr = '';
  if (new.workendstr containing '00:00' and new.daykind <> 1) then
    new.workendstr = '';

  new.workstartstr2 = substring(new.workstart2 from 1 for 5);
  new.workendstr2   = substring(new.workend2 from 1 for 5);
  if (new.workstartstr2 containing '00:00' and new.daykind <> 1) then
    new.workstartstr2 = '';
  if (new.workendstr2 containing '00:00' and new.daykind <> 1) then
    new.workendstr2 = '';
  --wyliczenie czasu pracy
  execute procedure count_worktime(new.workstart,new.workend,new.workstart2,new.workend2,new.daykind)
    returning_values new.worktime;
  execute procedure durationstr2(new.worktime) returning_values new.worktimestr;
  daylength = 86400;
  new.prworktime = new.worktime / daylength;
  new.workhours = cast(new.worktime as numeric(14,2)) / 3600.00;
end^
SET TERM ; ^
