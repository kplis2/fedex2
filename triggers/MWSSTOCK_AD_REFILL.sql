--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTOCK_AD_REFILL FOR MWSSTOCK                       
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable autogoodsrefill smallint;
declare variable quantity numeric(14,4);
begin
  select w.autogoodsrefill
    from mwsconstlocs m
      join whsecs w on (w.ref = m.whsec)
    where m.ref = old.mwsconstloc
    into autogoodsrefill;
  if (autogoodsrefill is null) then autogoodsrefill = 0;
  if (autogoodsrefill > 0) then
  begin
    -- sprawdzamy czy lokacja jest pusta - czyli nie ma nic poza paleta
    select sum(s.quantity + s.ordered)
      from mwsstock s
      where s.mwsconstloc = old.mwsconstloc and s.ispal <> 1
      into quantity;
    if (quantity is null) then quantity = 0;
    if (quantity = 0) then
      update mwsconstlocs set refill = 1 where ref = old.mwsconstloc;
  end
end^
SET TERM ; ^
