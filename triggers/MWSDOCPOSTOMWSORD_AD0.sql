--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSDOCPOSTOMWSORD_AD0 FOR MWSDOCPOSTOMWSORD              
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  delete from mwsconstlocsforbidden where docposid = old.docposid;
  delete from docpos2real where docposid = old.docposid;
end^
SET TERM ; ^
