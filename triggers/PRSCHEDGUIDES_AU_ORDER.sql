--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDES_AU_ORDER FOR PRSCHEDGUIDES                  
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.ord = 0 or new.ord = 3) then
    execute procedure order_prschedguides(new.prschedule, old.number, new.number);
end^
SET TERM ; ^
