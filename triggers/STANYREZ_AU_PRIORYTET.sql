--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYREZ_AU_PRIORYTET FOR STANYREZ                       
  ACTIVE AFTER UPDATE POSITION 1 
as
declare variable cnt integer;
begin
  if(new.priorytet<>old.priorytet) then begin
    if(new.status='B') then begin
      cnt = 0;
      if(exists(select first 1 1 from STANYREZ where ZAMOWIENIE = new.zamowienie and POZZAM=new.pozzam and STATUS='Z' and ZREAL=0 and PRIORYTET<>new.PRIORYTET)) then cnt = 1;
      if(:cnt > 0) then
        update STANYREZ set PRIORYTET=new.priorytet where ZAMOWIENIE = new.zamowienie and POZZAM=new.pozzam and STATUS='Z' and ZREAL=0;
      else
        execute procedure REZ_BLOK_PRZELICZ(new.magazyn, new.ktm, new.wersja);
    end else if(new.status='Z') then begin
      cnt = 0;
      if(exists(select first 1 1 from STANYREZ where ZAMOWIENIE = new.zamowienie and POZZAM=new.pozzam and STATUS='B' and ZREAL=0 and PRIORYTET<>new.PRIORYTET)) then cnt = 1;
      if(:cnt > 0) then
        update STANYREZ set PRIORYTET=new.priorytet where ZAMOWIENIE = new.zamowienie and POZZAM=new.pozzam and STATUS='B' and ZREAL=0;
      else
        execute procedure REZ_BLOK_PRZELICZ(new.magazyn, new.ktm, new.wersja);
    end
  end
end^
SET TERM ; ^
