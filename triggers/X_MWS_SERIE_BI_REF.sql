--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWS_SERIE_BI_REF FOR X_MWS_SERIE                    
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_X_MWS_SERIE,1);
end^
SET TERM ; ^
