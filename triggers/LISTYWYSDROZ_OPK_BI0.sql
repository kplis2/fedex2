--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDROZ_OPK_BI0 FOR LISTYWYSDROZ_OPK               
  ACTIVE BEFORE INSERT POSITION 1 
AS
declare variable nropk integer;
declare variable kontrolapak smallint;
begin
  select max(nropk) from listywysdroz_opk
    where listwysd = new.listwysd
  into :nropk;
  if (:nropk is null) then nropk = 0;
  new.nropk = :nropk + 1;

-- [DG] XXX typczasowo - problemy z pakowaniem
--  select kontrolapak from listywysd where ref = new.listwysd
--  into :kontrolapak;
--  if (:kontrolapak = 0) then new.stan = 1;
end^
SET TERM ; ^
