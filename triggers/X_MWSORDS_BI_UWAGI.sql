--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWSORDS_BI_UWAGI FOR MWSORDS                        
  ACTIVE BEFORE INSERT POSITION 100 
as
    declare variable uwagi memo2048;
begin
    --pobieranie uwag z dokumentu na zlecenie magazynowe
    select coalesce(d.uwagi,'') || coalesce(d.uwagisped,'') || coalesce(d.uwagiwewn,'')
        from dokumnag d
        where ref = new.docid
        into :uwagi;
    if (coalesce(uwagi,'') <> '') then
    begin
        new.description = coalesce(new.description, '') || coalesce(uwagi, '');
    end
end^
SET TERM ; ^
