--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTUREDISTS_BU0 FOR ACCSTRUCTUREDISTS              
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable vslownik integer ;
declare variable vkontoks ANALYTIC_ID;
declare variable vcompany integer;
declare variable vyearid integer;
declare variable vsymbol varchar(3);
begin
  if (new.distdef <> old.distdef or new.number <> old.number) then
  begin
    if (new.otable = 'SLOPOZ') then
    begin
      -- pobieram dany slownik i konto zeby sprawdzic czy wyrozniki sa gdzies zaksiegowane
      select slownik, kontoks from slopoz where ref = new.oref into :vslownik, :vkontoks;
      execute procedure check_dict_using_in_accounts(:vslownik, :vkontoks,null);
    end
    if (new.otable = 'BKACCOUNTS') then
    begin
      -- pobieram company, yearid oraz symbol konta zeby sprawdzic czy konto jest otwarte
      select company, yearid, symbol
        from bkaccounts
        where ref = new.oref
        into :vcompany, :vyearid, :vsymbol;

      if (exists(
        select ref
          from accounting
          where yearid = :vyearid and account starting with :vsymbol
            and (company = :vcompany or :vcompany is null)))
      then
        exception bkaccount_opened;
    end
  end
end^
SET TERM ; ^
