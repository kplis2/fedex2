--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CDEFKATALP_AI1 FOR CDEFKATALP                     
  ACTIVE AFTER INSERT POSITION 1 
as
declare variable ckatal INTEGER;
begin
  for select REF from CKATAL where CDEFKATAL = new.CDEFKATAL into :ckatal do begin
    insert into CKATALP(CKATAL, SYMBOL, WARTOSC, WARTSTR, NUMER) values(:ckatal, new.akronim, new.WARTDOM, new.WARTDOM, new.numer);
  end
end^
SET TERM ; ^
