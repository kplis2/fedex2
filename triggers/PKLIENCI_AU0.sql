--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PKLIENCI_AU0 FOR PKLIENCI                       
  ACTIVE AFTER UPDATE POSITION 0 
as
 declare variable slodef integer;
 declare variable cnt integer;
 declare variable company integer;
begin
  execute procedure get_global_param('CURRENTCOMPANY') returning_values company;
  if(new.crm = 1 and old.crm = 0) then begin
     select ref from SLODEF where TYP='PKLIENCI' into :slodef;
     if(:slodef is null) then exception SLODEF_BEZPKLIENCI;
     select count(*) from CPODMIOTY where SLODEF = :slodef and SLOPOZ = new.ref into :cnt;
     if(:cnt > 0) then
        update CPODMIOTY set SKROT = substring(new.skrot from 1 for 40), nazwa = new.nazwa,    --XXX ZG133796 MKD
                            TELEFON = new.telefon, fax = new.fax,
                            email = new.email, www = new.www,
                            ulica = new.ulica, nrdomu = new.nrdomu, nrlokalu = new.nrlokalu,
                            MIASTO = new.miasto, kodp = new.kodp,
                            zrodlo = new.zrodlo, forma = new.forma, branza = new.branza,
                            cstatus = new.status, uwagi = new.uwagi
        where SLODEF = :slodef and SLOPOZ = new.ref ;
     else
       insert into CPODMIOTY(FIRMA, AKTYWNY, SKROT, NAZWA,
                               TELEFON, FAX, EMAIL, WWW,SLODEF, SLOPOZ,
                               ULICA, NRDOMU, NRLOKALU,
                               MIASTO, KODP, ZRODLO, FORMA, BRANZA,
                               CSTATUS, UWAGI, COMPANY)
       values(1,1, substring(new.skrot from 1 for 40), new.nazwa,  --XXX ZG133796 MKD
              new.telefon, new.fax, new.email, new.www, :slodef, new.ref,
              new.ulica, new.nrdomu, new.nrlokalu,
              new.miasto, new.kodp, new.zrodlo, new.forma, new.branza,
              new.status, new.uwagi, :company);
  end else if(new.crm = 0 and old.crm = 1) then begin
     select ref from SLODEF where TYP='PKLIENCI' into :slodef;
     if(:slodef is null) then exception SLODEF_BEZPKLIENCI;
     delete from CPODMIOTY where SLODEF = :slodef and SLOPOZ = old.ref;
  end
  if(new.crm = 1 and old.crm = 1 and
     (new.skrot <> old.skrot or (new.skrot is not null and old.skrot is null) or (new.skrot is null and old.skrot is not null) or
      new.NAZWA <> old.NAZWA or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null) or
      new.ulica <> old.ulica or (new.ulica is not null and old.ulica is null) or (new.ulica is null and old.ulica is not null) or
      new.nrdomu <> old.nrdomu or (new.nrdomu is not null and old.nrdomu is null) or (new.nrdomu is null and old.nrdomu is not null) or
      new.nrlokalu <> old.nrlokalu or (new.nrlokalu is not null and old.nrlokalu is null) or (new.nrlokalu is null and old.nrlokalu is not null) or
      new.miasto <> old.miasto or (new.miasto is not null and old.miasto is null) or (new.miasto is null and old.miasto is not null) or
      new.kodp <> old.kodp or (new.kodp is not null and old.kodp is null) or (new.kodp is null and old.kodp is not null) or
      new.telefon <> old.telefon or (new.telefon is not null and old.telefon is null) or (new.telefon is null and old.telefon is not null) or
      new.fax <> old.fax or (new.fax is not null and old.fax is null) or (new.fax is null and old.fax is not null) or
      new.www <> old.www or (new.www is not null and old.www is null) or (new.www is null and old.www is not null) or
      new.email <> old.email or (new.email is not null and old.email is null) or (new.email is null and old.email is not null) or
      new.zrodlo <> old.zrodlo or (new.zrodlo is not null and old.zrodlo is null) or (new.zrodlo is null and old.zrodlo is not null) or
      new.forma <> old.forma or (new.forma is not null and old.forma is null) or (new.forma is null and old.forma is not null) or
      new.branza <> old.branza or (new.branza is not null and old.branza is null) or (new.branza is null and old.branza is not null) or
      new.status <> old.status or (new.status is not null and old.status is null) or (new.status is null and old.status is not null) or
      new.uwagi <> old.uwagi or (new.uwagi is not null and old.uwagi is null) or (new.uwagi is null and old.uwagi is not null)
     )
   ) then begin
     select ref from SLODEF where TYP='PKLIENCI' into :slodef;
     if(:slodef is null) then exception SLODEF_BEZPKLIENCI;
        update CPODMIOTY set SKROT = substring(new.skrot from 1 for 40), NAZWA = new.nazwa,  --XXX ZG133796 MKD
                      ULICA = new.ulica, NRDOMU = new.nrdomu, NRLOKALU = new.nrlokalu,
                      MIASTO = new.miasto, KODP = new.kodp,
                      TELEFON = new.telefon, EMAIL = new.email, WWW = new.www, FAX = new.fax,
                      zrodlo = new.zrodlo, forma = new.forma, branza = new.branza,
                      cstatus = new.status, uwagi = new.uwagi
        where SLODEF = :slodef and SLOPOZ = new.ref ;
  end
end^
SET TERM ; ^
