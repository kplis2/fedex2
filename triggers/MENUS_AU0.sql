--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MENUS_AU0 FOR MENUS                          
  ACTIVE AFTER UPDATE POSITION 0 
as
begin

  if(new.link <> old.link or (new.prefix <> old.prefix) or (new.nazwa <> old.nazwa)) then
     update MENU set nazwa = new.nazwa,link = new.link, prefix = new.prefix where
        symbol=new.symbol and rodzic = 0 and numer = 0;

end^
SET TERM ; ^
