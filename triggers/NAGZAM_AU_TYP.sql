--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGZAM_AU_TYP FOR NAGZAM                         
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable refpoz integer;
declare variable ktm varchar(40);
declare variable wersja integer;
declare variable cenamag numeric(14,2);
declare variable dostawamag integer;
declare variable oldilosc numeric(14,4);
declare variable newilosc numeric(14,4);
declare variable stan char(1);
declare variable stat integer;
begin
  if((new.typ=3 and old.typ=0)) then begin
      for select ref,ktm,wersja,cenamag,dostawamag,iloscm-ilonklonm,ilrealm,stan
        from pozzam where zamowienie=new.ref
        into :refpoz,:ktm,:wersja,:cenamag,:dostawamag,:oldilosc,:newilosc,:stan
      do begin
        /* blokuj ilosci realizowane */
        execute procedure REZ_SET_KTM(new.ref, :refpoz, :ktm, :wersja, :newilosc, :cenamag, :dostawamag, :stan, cast(new.termdost as date)) returning_values 

:stat;
        if(:stat <> 1) then exception REZ_WRONG;
      end
      if(new.kktm <> '') then begin
        newilosc = new.kilreal;
        execute procedure REZ_SET_KTM(new.ref, 0, new.kktm, new.kwersja, :newilosc, 0, 0, new.kstan, new.termdost ) returning_values :stat;
        if(:stat <> 1) then exception REZ_WRONG;
      end
end
  if((new.typ=0 and old.typ=3)) then begin
      for select ref,ktm,wersja,cenamag,dostawamag,iloscm-ilonklonm,ilrealm,stan
        from pozzam where zamowienie=new.ref
        into :refpoz,:ktm,:wersja,:cenamag,:dostawamag,:oldilosc,:newilosc,:stan
      do begin
        /* blokuj ilosci zamawiane */
        execute procedure REZ_SET_KTM(new.ref, :refpoz, :ktm, :wersja, :oldilosc, :cenamag, :dostawamag, :stan, cast(new.termdost as date)) returning_values 

:stat;
        if(:stat <> 1) then exception REZ_WRONG;
      end
      if(new.kktm <> '') then begin
        newilosc = new.kilosc;
        execute procedure REZ_SET_KTM(new.ref, 0, new.kktm, new.kwersja, :newilosc, 0, 0, new.kstan, new.termdost ) returning_values :stat;
      end
  end
  if(  (new.termdostdecl <> old.termdostdecl) or (new.termdostdecl is null and old.termdostdecl is not null) or (new.termdostdecl is not null and 

old.termdostdecl is null)
      or(new.termstartdecl <> old.termstartdecl) or (new.termstartdecl is null and old.termstartdecl is not null) or (new.termstartdecl is not null and 

old.termstartdecl is null)
  )then
    execute procedure nagzam_termstartdost(new.ref);
end^
SET TERM ; ^
