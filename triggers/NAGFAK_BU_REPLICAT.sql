--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_BU_REPLICAT FOR NAGFAK                         
  ACTIVE BEFORE UPDATE POSITION 1 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if((new.ref <> old.ref)
   or (new.oddzial <> old.oddzial)
   or (new.rejestr <> old.rejestr)
   or (new.typ <> old.typ)
   or (new.stanowisko <> old.stanowisko) or (new.stanowisko is not null and old.stanowisko is null) or (new.stanowisko is null and old.stanowisko is not null)
   or (new.okres  <> old.okres ) or (new.okres is not null and old.okres is null) or (new.okres is null and old.okres is not null)
   or (new.numer <> old.numer ) or (new.numer is not null and old.numer is null) or (new.numer is null and old.numer is not null)
   or (new.symbol <> old.symbol ) or (new.symbol is not null and old.symbol is null) or (new.symbol is null and old.symbol is not null)
   or (new.klient <> old.klient ) or (new.klient is not null and old.klient is null) or (new.klient is null and old.klient is not null)
   or (new.platnik <> old.platnik ) or (new.platnik is not null and old.platnik is null) or (new.platnik is null and old.platnik is not null)
   or (new.data <> old.data ) or (new.data is not null and old.data is null) or (new.data is null and old.data is not null)
   or (new.datasprz <> old.datasprz ) or (new.datasprz is not null and old.datasprz is null) or (new.datasprz is null and old.datasprz is not null)
   or (new.termin <> old.termin ) or (new.termin is not null and old.termin is null) or (new.termin is null and old.termin is not null)
   or (new.termzap <> old.termzap ) or (new.termzap is not null and old.termzap is null) or (new.termzap is null and old.termzap is not null)
   or (new.sposdost <> old.sposdost ) or (new.sposdost is not null and old.sposdost is null) or (new.sposdost is null and old.sposdost is not null)
   or (new.sumkaucja <> old.sumkaucja ) or (new.sumkaucja is not null and old.sumkaucja is null) or (new.sumkaucja is null and old.sumkaucja is not null)
   or (new.sumwartnet <> old.sumwartnet ) or (new.sumwartnet is not null and old.sumwartnet is null) or (new.sumwartnet is null and old.sumwartnet is not null)
   or (new.sumwartbru <> old.sumwartbru ) or (new.sumwartbru is not null and old.sumwartbru is null) or (new.sumwartbru is null and old.sumwartbru is not null)
   or (new.psumkaucja <> old.psumkaucja ) or (new.psumkaucja is not null and old.psumkaucja is null) or (new.psumkaucja is null and old.psumkaucja is not null)
   or (new.psumwartnet <> old.psumwartnet ) or (new.psumwartnet is not null and old.psumwartnet is null) or (new.psumwartnet is null and old.psumwartnet is not null)
   or (new.psumwartbru <> old.psumwartbru ) or (new.psumwartbru is not null and old.psumwartbru is null) or (new.psumwartbru is null and old.psumwartbru is not null)
   or (new.rabat <> old.rabat ) or (new.rabat is not null and old.rabat is null) or (new.rabat is null and old.rabat is not null)
   or (new.dozaplaty <> old.dozaplaty ) or (new.dozaplaty is not null and old.dozaplaty is null) or (new.dozaplaty is null and old.dozaplaty is not null)
   or (new.uwagi <> old.uwagi ) or (new.uwagi is not null and old.uwagi is null) or (new.uwagi is null and old.uwagi is not null)
   or (new.akceptacja <> old.akceptacja ) or (new.akceptacja is not null and old.akceptacja is null) or (new.akceptacja is null and old.akceptacja is not null)
   or (new.anulowanie <> old.anulowanie ) or (new.anulowanie is not null and old.anulowanie is null) or (new.anulowanie is null and old.anulowanie is not null)
   or (new.dataakc <> old.dataakc ) or (new.dataakc is not null and old.dataakc is null) or (new.dataakc is null and old.dataakc is not null)
   or (new.korekta <> old.korekta ) or (new.korekta is not null and old.korekta is null) or (new.korekta is null and old.korekta is not null)
   or (new.symbolk <> old.symbolk ) or (new.symbolk is not null and old.symbolk is null) or (new.symbolk is null and old.symbolk is not null)
   or (new.reffak <> old.reffak ) or (new.reffak is not null and old.reffak is null) or (new.reffak is null and old.reffak is not null)
   or (new.symbolfak <> old.symbolfak ) or (new.symbolfak is not null and old.symbolfak is null) or (new.symbolfak is null and old.symbolfak is not null)
   or (new.refdokm <> old.refdokm ) or (new.refdokm is not null and old.refdokm is null) or (new.refdokm is null and old.refdokm is not null)
   or (new.symbdokm <> old.symbdokm ) or (new.symbdokm is not null and old.symbdokm is null) or (new.symbdokm is null and old.symbdokm is not null)
   or (new.waluta <> old.waluta ) or (new.waluta is not null and old.waluta is null) or (new.waluta is null and old.waluta is not null)
   or (new.kurs <> old.kurs ) or (new.kurs is not null and old.kurs is null) or (new.kurs is null and old.kurs is not null)
   or (new.sumkaucjazl <> old.sumkaucjazl ) or (new.sumkaucjazl is not null and old.sumkaucjazl is null) or (new.sumkaucjazl is null and old.sumkaucjazl is not null)
   or (new.sumwartnetzl <> old.sumwartnetzl ) or (new.sumwartnetzl is not null and old.sumwartnetzl is null) or (new.sumwartnetzl is null and old.sumwartnetzl is not null)
   or (new.sumwartbruzl <> old.sumwartbruzl ) or (new.sumwartbruzl is not null and old.sumwartbruzl is null) or (new.sumwartbruzl is null and old.sumwartbruzl is not null)
   or (new.psumkaucjazl <> old.psumkaucjazl ) or (new.psumkaucjazl is not null and old.psumkaucjazl is null) or (new.psumkaucjazl is null and old.psumkaucjazl is not null)
   or (new.psumwartnetzl <> old.psumwartnetzl ) or (new.psumwartnetzl is not null and old.psumwartnetzl is null) or (new.psumwartnetzl is null and old.psumwartnetzl is not null)
   or (new.psumwartbruzl <> old.psumwartbruzl ) or (new.psumwartbruzl is not null and old.psumwartbruzl is null) or (new.psumwartbruzl is null and old.psumwartbruzl is not null)
   or (new.pkurs <> old.pkurs ) or (new.pkurs is not null and old.pkurs is null) or (new.pkurs is null and old.pkurs is not null)
   or (new.operator <> old.operator ) or (new.operator is not null and old.operator is null) or (new.operator is null and old.operator is not null)
   or (new.sprzedawca <> old.sprzedawca ) or (new.sprzedawca is not null and old.sprzedawca is null) or (new.sprzedawca is null and old.sprzedawca is not null)
   or (new.zafisk <> old.zafisk ) or (new.zafisk is not null and old.zafisk is null) or (new.zafisk is null and old.zafisk is not null)
   or (new.nieobrot <> old.nieobrot ) or (new.nieobrot is not null and old.nieobrot is null) or (new.nieobrot is null and old.nieobrot is not null)
   or (new.kwotazap <> old.kwotazap ) or (new.kwotazap is not null and old.kwotazap is null) or (new.kwotazap is null and old.sposzap is not null)
   or (new.kwotazal <> old.kwotazal ) or (new.kwotazal is not null and old.kwotazal is null) or (new.kwotazal is null and old.kwotazal is not null)
   or (new.faktoring <> old.faktoring ) or (new.faktoring is not null and old.faktoring is null) or (new.faktoring  is null and old.faktoring is not null)
   or (new.koszt <> old.koszt ) or (new.koszt is not null and old.koszt is null) or (new.koszt is null and old.koszt is not null)
   or (new.pkoszt <> old.pkoszt ) or (new.pkoszt is not null and old.pkoszt is null) or (new.pkoszt is null and old.pkoszt is not null)
   or (new.dostawca <> old.dostawca ) or (new.dostawca is not null and old.dostawca is null) or (new.dostawca is null and old.dostawca is not null)
   or (new.wartmag <> old.wartmag ) or (new.wartmag is not null and old.wartmag is null) or (new.wartmag is null and old.wartmag is not null)
   or (new.kosztzak <> old.kosztzak ) or (new.kosztzak is not null and old.kosztzak is null) or (new.kosztzak is null and old.kosztzak is not null)
   or (new.pkosztzak <> old.pkosztzak ) or (new.pkosztzak is not null and old.pkosztzak is null) or (new.pkosztzak is null and old.pkosztzak is not null)
   or (new.zakup <> old.zakup ) or (new.zakup is not null and old.zakup is null) or (new.zakup is null and old.zakup is not null)
   or (new.magrozlicz <> old.magrozlicz ) or (new.magrozlicz is not null and old.magrozlicz is null) or (new.magrozlicz is null and old.magrozlicz is not null)
   or (new.magrozliczdata <> old.magrozliczdata ) or (new.magrozliczdata is not null and old.magrozliczdata is null) or (new.magrozliczdata is null and old.magrozliczdata is not null)
   or (new.kosztkat <> old.kosztkat ) or (new.kosztkat is not null and old.kosztkat is null) or (new.kosztkat is null and old.kosztkat is not null)
   or (new.cpluczestid <> old.cpluczestid) or (new.cpluczestid is not null and old.cpluczestid is null) or (new.cpluczestid is null and old.cpluczestid is not null)
   or (new.cpluczestkartid <> old.cpluczestkartid) or (new.cpluczestkartid is not null and old.cpluczestkartid is null) or (new.cpluczestkartid is null and old.cpluczestkartid is not null)

   or (new.dataotrzkor <> old.dataotrzkor)
  ) then
   waschange = 1;

  execute procedure rp_trigger_bu('NAGFAK',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
