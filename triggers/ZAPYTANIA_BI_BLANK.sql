--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ZAPYTANIA_BI_BLANK FOR ZAPYTANIA                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.nazwa = '') then
    new.nazwa = null;
  if (new.sql = '') then
    new.sql = null;
  if (new.rights is null) then new.rights = '';
  if (new.rightsgroup is null) then new.rightsgroup = '';
  if (new.isrefreshable is null) then new.isrefreshable = 1;
  if (new.querytype is not null) then
    select symbol from querytypes where ref = new.querytype
      into new.typ;
end^
SET TERM ; ^
