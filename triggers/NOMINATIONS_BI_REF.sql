--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOMINATIONS_BI_REF FOR NOMINATIONS                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
    if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('NOMINATIONS')
      returning_values new.REF;
end^
SET TERM ; ^
