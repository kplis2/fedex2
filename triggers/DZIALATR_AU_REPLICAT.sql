--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DZIALATR_AU_REPLICAT FOR DZIALATR                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.nazwa <> old.nazwa or (new.nazwa is null and old.nazwa is not null) or (new.nazwa is not null and old.nazwa is null) or
    new.wartosc <> old.wartosc or (new.wartosc is null and old.wartosc is not null) or (new.wartosc is not null and old.wartosc is null) or
    new.numer <> old.numer or (new.numer is null and old.numer is not null) or (new.numer is not null and old.numer is null)
  ) then
    update ARTYKULY set state=-2 where ref=new.ARTYKUL;
end^
SET TERM ; ^
