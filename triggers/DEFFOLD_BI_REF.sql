--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFFOLD_BI_REF FOR DEFFOLD                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DEFFOLD')
      returning_values new.REF;
end^
SET TERM ; ^
