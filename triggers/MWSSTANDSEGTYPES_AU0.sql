--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDSEGTYPES_AU0 FOR MWSSTANDSEGTYPES               
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable mwsstandseg integer;
begin
   if (new.l <> old.l) then
    update mwsstandsegs set l = new.l where mwsstandsegtype = new.ref;
  if (new.w <> old.w) then
    update mwsstandsegs set w = new.w where mwsstandsegtype = new.ref;
  if (new.symbol <> old.symbol) then
  begin
    update mwsstandsegs set mwsstandsegtypedict = new.symbol where mwsstandsegtype = new.ref;
    update whareas set mwsstandsegtypedict = new.symbol where mwsstandsegtype = new.ref;
  end
  if (new.maxhordist <> old.maxhordist) then
    for
      select ref
        from mwsstandsegs
        where mwsstandsegtype = new.ref
        into mwsstandseg
    do begin
      update whareas set maxhordist = new.maxhordist, maxvertdist = new.maxvertdist where mwsstandseg = :mwsstandseg;
    end
  if (new.maxvertdist <> old.maxvertdist) then
    for
      select ref
        from mwsstandsegs
        where mwsstandsegtype = new.ref
        into mwsstandseg
    do begin
      update whareas set maxvertdist = new.maxvertdist, maxhordist = new.maxhordist where mwsstandseg = :mwsstandseg;
    end
end^
SET TERM ; ^
