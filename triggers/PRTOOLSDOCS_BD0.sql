--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSDOCS_BD0 FOR PRTOOLSDOCS                    
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if (old.akcept = 1) then
    exception prtoolsdocs_error 'Dokument zaakceptowany, usunięcie niemożliwe';
end^
SET TERM ; ^
