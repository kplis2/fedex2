--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PERSONS_BIU_CONTROL FOR PERSONS                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 3 
AS
declare variable sperson smallint;
declare variable peselexists smallint;
declare variable company integer;
begin
  execute procedure get_global_param('CURRENTCOMPANY')
    returning_values :company;

  if (new.pesel is not null and (inserting or coalesce(old.pesel,'') <> coalesce(new.pesel,''))) then
  begin
    execute procedure getconfig('SINGLEPERSON')
      returning_values sperson;

    select first 1 1 from cpersons
      where pesel = new.pesel
        and (coalesce(:sperson,0) = 1 or company = :company)
      into :peselexists;

    if (peselexists = 1) then
      exception persons_pesel_check;
  end

  if (new.evidencedate >= new.expevidencedate or new.passportdate >= new.exppassportdate) then
    exception wrong_evi_pass_exp_date;

--Kontrola unikalnosci paszportu w ramach company (PR60210)
  if (new.passportno is distinct from old.passportno) then
  begin
    if (new.passportno <> '') then
      if (exists(select first 1 1 from cpersons p
                   where upper(replace(p.passportno,  ' ',  '')) = upper(replace(new.passportno,  ' ',  ''))
                     and p.company = :company)
      ) then
        exception not_unq_passportno;
  end
end^
SET TERM ; ^
