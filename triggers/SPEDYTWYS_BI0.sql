--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPEDYTWYS_BI0 FOR SPEDYTWYS                      
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable rodzajwys smallint;
declare variable cnt integer;
begin
  select s.rodzajwys from sposdost s where s.ref = new.sposdost into :rodzajwys;
  select count(s.sposdost) from spedytwys s where s.sposdost = new.sposdost into :cnt;

  if (coalesce(:rodzajwys,0) > 0 and coalesce(:cnt,0) > 0) then
    exception universal;
end^
SET TERM ; ^
