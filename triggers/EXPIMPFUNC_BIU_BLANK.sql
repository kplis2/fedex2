--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EXPIMPFUNC_BIU_BLANK FOR EXPIMPFUNC                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.eigroup = '') then
    new.eigroup = null;
  if (new.name = '') then
    new.name = null;
end^
SET TERM ; ^
