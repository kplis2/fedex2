--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLFAMILY_BIU_DATES FOR EMPLFAMILY                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  --kontrola zbiezności znaczników i DAT
  if (New.CAREAID<>0) then
  begin
    if (new.CAIDFROM is null or new.CAIDTO is null) then
      exception EFAMILY_CAID;
    if (new.CAIDFROM>new.CAIDTO) then
      exception DATA_DO_MUSI_BYC_WIEKSZA;
  end else
  begin
    new.CAIDFROM = null;
    new.CAIDTO = null;
  end

  if (New.FAMILYAID<>0) then
  begin
    if (new.FAIDFROM is null or new.FAIDTO is null) then
      exception EFAMILY_FAID;
    if (new.FAIDFROM>new.FAIDTO) then
      exception DATA_DO_MUSI_BYC_WIEKSZA;
  end else
  begin
    new.FAIDFROM = null;
    new.FAIDTO = null;
  end
end^
SET TERM ; ^
