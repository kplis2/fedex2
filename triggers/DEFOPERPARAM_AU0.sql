--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERPARAM_AU0 FOR DEFOPERPARAM                   
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable aa integer;
begin
  if(new.AKRONIM <> old.AKRONIM or (new.numer <> old.numer)) then begin
    update OPERPARAM set numer = new.numer, nazwa = new.akronim where nazwa = old.akronim;
  end
end^
SET TERM ; ^
