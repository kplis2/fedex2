--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRPOS_AIUD_EXECUTION FOR EPRPOS                         
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 
AS
declare variable ename varchar(22);
declare variable maxpvalue numeric(14,2);
begin
--MWr: synchronizacja naliczonych zajec komorniczych ze splatami w module komorniczym

  if (deleting or (new.cauto = 0 and (inserting or (updating
          and new.chgtimestamp is not null and new.pvalue <> old.pvalue)))
  ) then begin

    select name from ecolumns
      where number = coalesce(new.ecolumn,old.ecolumn) and coltype = 'KOM'
      into :ename;
  
    if (ename <> '') then
    begin
      if (deleting) then
        execute procedure ef_execution(old.employee, old.payroll, 'DELEPRPOS', old.ecolumn, null)
          returning_values maxpvalue;
      else begin
      --sekcja aktywana w przypadku modyfikacji wartosci potracenia przez uzytkownika
        execute procedure ef_execution(new.employee, new.payroll, '', new.ecolumn, new.pvalue)
          returning_values maxpvalue;
  
        if (new.pvalue <> 0) then
        begin
          if (maxpvalue is null) then
            exception universal 'Nie można zarejestrować potracenia, ponieważ nie znaleziono
aktywnych definicji potrąceń komorniczych typu '|| upper(ename);
          if (new.pvalue > 0 and maxpvalue < 0) then
            exception universal 'Wartość skł. ' || upper(ename) || ' można modyfikować w zakresie od '||
:maxpvalue || ' do 0.00. Sprawdź definicje potrąceń komorniczych!';
          else if (new.pvalue < 0 and maxpvalue > 0) then
            exception universal 'Wartość skł. ' || upper(ename) || ' nie może być mniejsza od zera.
Sprawdź definicje potrąceń komorniczych!';
          else if (abs(maxpvalue) < abs(new.pvalue)) then
            exception universal 'Wartość skł. ' || upper(ename) || ' nie może przekroczyć ' || :maxpvalue || '
Sprawdź definicje potrąceń komorniczych!';
        end
      end
    end
  end
end^
SET TERM ; ^
