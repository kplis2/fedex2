--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CONTRTYPES_BU_REPLICAT FOR CONTRTYPES                     
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.REF<>old.REF
      or new.symbol <> old.symbol or (new.symbol is null and old.symbol is not null) or (new.symbol is not null and old.symbol is null)
      or new.repdictdef <> old.repdictdef or (new.repdictdef is null and old.repdictdef is not null) or (new.repdictdef is not null and old.repdictdef is null)
      or new.exedictdef <> old.exedictdef or (new.exedictdef is null and old.exedictdef is not null) or (new.exedictdef is not null and old.exedictdef is null)
      or new.srvrequest <> old.srvrequest or (new.srvrequest is null and old.srvrequest is not null) or (new.srvrequest is not null and old.srvrequest is null)
      or new.crm <> old.crm or (new.crm is null and old.crm is not null) or (new.crm is not null and old.crm is null)
      or new.phase <> old.phase or (new.phase is null and old.phase is not null) or (new.phase is not null and old.phase is null)
      or new.recway <> old.recway or (new.recway is null and old.recway is not null) or (new.recway is not null and old.recway is null)
      or new.numbergen <> old.numbergen or (new.numbergen is null and old.numbergen is not null) or (new.numbergen is not null and old.numbergen is null)
      or new.numbergen2 <> old.numbergen2 or (new.numbergen2 is null and old.numbergen2 is not null) or (new.numbergen2 is not null and old.numbergen2 is null)
      or new.contrcharacter <> old.contrcharacter or (new.contrcharacter is null and old.contrcharacter is not null) or (new.contrcharacter is not null and old.contrcharacter is null)
      or new.document <> old.document or (new.document is null and old.document is not null) or (new.document is not null and old.document is null)
      or new.typfak <> old.typfak or (new.typfak is null and old.typfak is not null) or (new.typfak is not null and old.typfak is null)
      or new.typfakzak <> old.typfakzak or (new.typfakzak is null and old.typfakzak is not null) or (new.typfakzak is not null and old.typfakzak is null)
      or new.projectdef <> old.projectdef or (new.projectdef is null and old.projectdef is not null) or (new.projectdef is not null and old.projectdef is null)

  ) then 
   waschange = 1;

  execute procedure rp_trigger_bu('CONTRTYPES',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
