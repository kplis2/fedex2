--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FSCLRACCH_BI_REPLICAT FOR FSCLRACCH                      
  ACTIVE BEFORE INSERT POSITION 2 
AS
begin
 execute procedure rp_trigger_bi('FSCLRACCH',new.ref, null, null, null, null, new.token, new.state)
    returning_values new.token, new.state;
end^
SET TERM ; ^
