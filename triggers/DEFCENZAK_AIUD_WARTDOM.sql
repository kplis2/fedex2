--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCENZAK_AIUD_WARTDOM FOR DEFCENZAK                      
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 
AS
begin
  if (new.odwart<>old.odwart or old.odwart is null or
      new.procent<>old.procent or old.procent is null ) then begin
    execute procedure DEFCENZAK_WARTDOM(
      case when new.defcennikzak is null then old.defcennikzak else new.defcennikzak end,
      case when new.PARSYMBOL is null then old.PARSYMBOL else new.PARSYMBOL end);
  end
end^
SET TERM ; ^
