--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECALDAYS_BI_REF FOR ECALDAYS                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ECALDAYS')
      returning_values new.REF;
end^
SET TERM ; ^
