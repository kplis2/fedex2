--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DZIALY_BI_REF FOR DZIALY                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DZIALY')
      returning_values new.REF;
end^
SET TERM ; ^
