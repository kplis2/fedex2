--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPRZEDZESTP_BD0 FOR SPRZEDZESTP                    
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable typ char(1);
begin
  select TYP from SPRZEDZEST where REF=old.zestawienie into :typ;
  if(:typ = 'Z')then begin
    if(old.zam > 0) then update NAGZAM set SPRZEDROZL = 0 where REF=old.zam;
    if(old.fak > 0) then update NAGFAK set SPRZEDROZL = 0 where REF=old.fak;
  end
end^
SET TERM ; ^
