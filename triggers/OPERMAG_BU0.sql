--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OPERMAG_BU0 FOR OPERMAG                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if(new.dokumtypesnewright in ('',';')) then new.dokumtypesnewright = null;
  if(new.dokumtypesacceptright in ('',';')) then new.dokumtypesacceptright = null;
  if(new.dokumtypeschangeright in ('',';')) then new.dokumtypeschangeright = null;
  if(new.dokumtypesacceptbackright in ('',';')) then new.dokumtypesacceptbackright = null;
  begin
    execute procedure mwsordtypedest4op_grants(coalesce(new.canchangemwsconstlocl,0), coalesce(new.supervisor,0))
      returning_values new.grants;
  end
end^
SET TERM ; ^
