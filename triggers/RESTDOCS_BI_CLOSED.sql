--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RESTDOCS_BI_CLOSED FOR RESTDOCS                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable period_status integer;
begin
    select status from amperiods
    where ammonth = extract(month from new.docdate) and amyear = extract(year from new.docdate)
    into :period_status;
    if (period_status > 0) then exception AMPERIOD_CLOSED 'Nie można wystawić dokumentu dla zamkniętego okresu!';
end^
SET TERM ; ^
