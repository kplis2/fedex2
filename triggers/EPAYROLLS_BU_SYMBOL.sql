--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPAYROLLS_BU_SYMBOL FOR EPAYROLLS                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable numberg varchar(255);
declare variable blockref integer;
declare variable is_ctrl_nr smallint;
begin
--MWr: Przypisanie symbolu na podstawie numeratora

  if(new.cper <> old.cper or new.prtype <> old.prtype or new.number <> old.number) then
  begin

    if (new.prtype <> 'UCP') then
      execute procedure GETCONFIG('EPAYROLLSNUM') returning_values :numberg;
    else
      execute procedure GETCONFIG('EPRBILLSNUM') returning_values :numberg;

    is_ctrl_nr = 1;                       --czy kontrola unikalnosci symbolu
    if (coalesce(numberg,'') = '') then   --spr. czy numerator przypisany do list plac/rachunku UCP:
      new.symbol = new.prtype || '/' || new.cper || '/' || coalesce(new.number,0);
    else begin
      execute procedure FREE_NUMBER(:numberg, old.company, old.prtype, substring(old.cper from 1 for 4) || '/' || substring(old.cper from 5 for 6) || '/1', old.number,0)
        returning_values :blockref ;
      while (is_ctrl_nr = 1) do begin
        execute procedure Get_NUMBER(:numberg, new.company, new.prtype, substring(new.cper from 1 for 4) || '/' || substring(new.cper from 5 for 6) || '/1', :blockref, null)
          returning_values new.number, new.symbol;
        if (exists(select first 1 1 from epayrolls where symbol = new.symbol and company = new.company and ref <> new.ref)) then
          blockref = 0;
        else
          is_ctrl_nr = 0;
      end
    end

    if (is_ctrl_nr = 1 and exists(select first 1 1 from epayrolls where symbol = new.symbol and company = new.company and ref <> new.ref)) then
    begin
      if (new.prtype <> 'UCP') then
        exception EPAYROLLS_SYMBOL;
      else
        exception EPAYROLLS_SYMBOL_UCP;
    end
  end
end^
SET TERM ; ^
