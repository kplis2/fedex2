--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_MESSAGEPARAMS_BIU_REF FOR S_MESSAGEPARAMS                
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
as
begin
  if (coalesce(new.ref,0) = 0) then
    execute procedure gen_ref('S_MESSAGEPARAMS')returning_values new.ref;
end^
SET TERM ; ^
