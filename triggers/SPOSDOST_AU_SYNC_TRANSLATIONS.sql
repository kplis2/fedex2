--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER SPOSDOST_AU_SYNC_TRANSLATIONS FOR SPOSDOST                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if (new.nazwa is distinct from old.nazwa) then
    execute procedure SYNC_SYS_TRANSLATIONS('SPOSDOST', new.ref, new.nazwa);
end^
SET TERM ; ^
