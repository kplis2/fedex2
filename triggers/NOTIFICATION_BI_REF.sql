--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NOTIFICATION_BI_REF FOR NOTIFICATION                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  IF (COALESCE(NEW.ref,0) = 0) THEN
    EXECUTE PROCEDURE gen_Ref('NOTIFICATION') RETURNING_VALUES NEW.ref;
end^
SET TERM ; ^
