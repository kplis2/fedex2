--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKVATPOS_BI_REF FOR BKVATPOS                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('BKVATPOS')
      returning_values new.REF;
end^
SET TERM ; ^
