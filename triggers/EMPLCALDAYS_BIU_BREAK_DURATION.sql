--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCALDAYS_BIU_BREAK_DURATION FOR EMPLCALDAYS                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
  declare variable interval integer;
  declare variable time_zero time;
begin
  --DS: dot. przerywanego systemu czasu pracy
  --sprawdzenie czy przerwa w pracy nie jest dluzsza niz 5 godzin
  time_zero = '00:00:00';
  if (new.workstart2 > time_zero) then
  begin
    --interval = new.workstart2 - new.workend;
    if (new.workstart2 - new.workend > 5 * 3600) then
      exception duration_of_break;

    if (new.workstart2 < '05:00:00') then
    begin
      interval = new.workstart2 - time_zero;
      if (new.workend - interval < '19:00') then
        exception duration_of_break;
    end
    else if (new.workstart2 - new.workend < 0) then
      exception duration_of_break;
  end
end^
SET TERM ; ^
