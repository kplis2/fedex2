--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DCTEMPLETPOS_BD_ORDER FOR DCTEMPLETPOS                   
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then exit;
  update dctempletpos set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and dctemplet = old.dctemplet;
end^
SET TERM ; ^
