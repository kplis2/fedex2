--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSCONSTLOCSYMBS_BI0 FOR MWSCONSTLOCSYMBS               
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable mws smallint;
declare variable cref integer;
begin
  if (new.mwsconstloc is null) then new.mwsconstloc = 0;
  if (new.mwsstandlevelnumber is null) then new.mwsstandlevelnumber = 0;
  if (new.const is null) then new.const = 0;
  mws = null;
  select d.mws
    from mwsconstlocs m
      left join defmagaz d on (d.symbol = m.wh)
    where m.ref = new.mwsconstloc
    into mws;
  if (mws is null and new.wh is not null and new.wh <> '' ) then
    select d.mws
      from defmagaz d
      where d.symbol = new.wh
      into mws;
  if (mws is null) then mws = 0;
  if (mws = 1) then
  begin
    if (new.mwsconstlocsymbol is null) then
    begin
      select symbol, goodsav from mwsconstlocs where ref = new.mwsconstloc
        into new.mwsconstlocsymbol, new.goodsav;
    end
  end
  if (mws = 0) then
  begin
    new.avmode = 1;
    new.const = 1;
    if (new.wh is null or new.wh = '') then
      exception mwsconstlocsymbs_wh;
    if (new.mwsconstlocsymbol is null or new.mwsconstlocsymbol = '') then
      exception mwsconstlocsymbs_wh 'Brak wskazanej lokacji magazynowej';
    select C.ref
      from mwsconstlocs C
      where C.wh = new.wh
        and C.symbol = new.mwsconstlocsymbol
      into :cref;
    if (cref is null) then
    begin
      execute procedure gen_ref('MWSCONSTLOCS') returning_values cref;
      insert into mwsconstlocs(ref, wh, symbol) values(:cref, new.wh, new.mwsconstlocsymbol);
    end
    new.mwsconstloc = cref;
  end
end^
SET TERM ; ^
