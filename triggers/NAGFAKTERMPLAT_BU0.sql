--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAKTERMPLAT_BU0 FOR NAGFAKTERMPLAT                 
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable akc smallint;
begin
  if(new.termzap<>old.termzap or new.procentplat<>old.procentplat or new.sposplat<>old.sposplat) then begin
    if(new.doktyp='F') then begin
      select n.akceptacja from nagfak n where n.ref = new.dokref into :akc;
      if (:akc = 1) then exception NAGFAK_AKCEPT 'Modyfikacja tabeli spłat dla zaakceptowanej faktury niemożliwa';
    end else if(new.doktyp='M') then begin
      select n.akcept from dokumnag n where n.ref = new.dokref into :akc;
      if (:akc = 1) then exception NAGFAK_AKCEPT 'Modyfikacja tabeli spłat dla zaakceptowanego dokumentu niemożliwa';
    end
  end
end^
SET TERM ; ^
