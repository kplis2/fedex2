--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CNOTATKI_BI_PRAWA FOR CNOTATKI                       
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
 if (new.pkosoba = 0) then
   new.pkosoba = null;
 if(new.prawadef is null) then new.prawadef = 0;
 if(new.prawadef=0) then begin
   if(new.cpodmiot is not null) then
     select PRAWA,PRAWAGRUP from CPODMIOTY where ref = new.cpodmiot
     into new.prawa, new.prawagrup;
   else begin
     new.prawa = '';
     new.prawagrup = '';
   end
 end
 execute procedure GET_SUPERIORS(new.prawa) returning_values new.prawa;
 if(new.cpodmiot is not null) then
   select SKROT,SLODEF,OPEROPIEK,TELEFON,COMPANY from CPODMIOTY where REF=new.CPODMIOT
   into new.cpodmskrot, new.cpodmslodef, new.cpodmoperopiek, new.cpodmtelefon, new.company;
 if(new.PKOSOBA is not NULL) then
   select NAZWA,TELEFON from PKOSOBY where REF=new.PKOSOBA
   into new.PKOSOBYNAZWA,new.PKOSOBYTELEFON;
 else begin
   new.PKOSOBYNAZWA = NULL;
   new.PKOSOBYTELEFON = NULL;
 end
 if(new.cpodmiot is not null) then
   select ancestors from CPODMIOTY where ref = new.cpodmiot into new.cpodmancestors;
end^
SET TERM ; ^
