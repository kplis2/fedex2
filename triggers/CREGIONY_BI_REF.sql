--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CREGIONY_BI_REF FOR CREGIONY                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CREGIONY')
      returning_values new.REF;
end^
SET TERM ; ^
