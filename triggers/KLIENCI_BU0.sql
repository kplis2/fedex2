--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_BU0 FOR KLIENCI                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
  declare variable dl smallint;
  declare variable wymagajnip VARCHAR(40);
  declare variable NAZWAFIZYCZNYCH varchar(255);
  declare variable contactcontrol varchar(100);
  declare variable cnt integer;
begin
   if(new.imie is null) then new.imie = '';
   if(new.nazwisko is null) then new.nazwisko = '';
   if(new.nazwa is null) then new.nazwa = '';
   if(new.bank = '')then new.bank = null;
   if(new.oddzial = '') then new.oddzial = null;
   if(new.fskrot is null) then new.fskrot = '';
   if(new.fskrot = '') then exception KLIENCI_FSKROTEMPTY;
   if(new.rachunek is null) then new.rachunek = '';
   if(new.powiazany is null) then new.powiazany = 0;
   if(new.fskrot <> old.fskrot and new.fskrot is not null) then new.fskrot = upper(new.fskrot);
   if(coalesce(new.krajid,'') <> coalesce(old.krajid,'') and new.krajid is not null) then
    select name from countries where symbol = new.krajid into new.kraj;
   if ((new.firma=0  or new.firma is null)
     and (new.imie<>old.imie or new.nazwisko<>old.nazwisko)
     or new.nazwa=''or new.nazwa is null) then
   begin
    execute procedure getconfig('NAZWAFIZYCZNYCH')
      returning_values :NAZWAFIZYCZNYCH;
    if (NAZWAFIZYCZNYCH = '1') then
      new.nazwa = new.nazwisko || ' ' || new.imie ;
    else
      new.nazwa = new.imie || ' ' || new.nazwisko;
   end
  if (new.nazwa is null or (new.nazwa = '')) then
     exception KLIENCI_PUSTANAZWA;
  if (new.korespond > 1) then new.korespond = 1;
  if (new.firma=1 and new.zagraniczny=0 and new.nip='') then
  begin
    execute procedure GETCONFIG('WYMAGAJNIP') returning_values :wymagajnip;
    if (:wymagajnip = '1') then exception BRAK_NIPU;
  end
  /* sprawdzenie dlugosci kodu ksiegowego */
  select KODLN from slodef where typ='KLIENCI' into :dl;
  if (:dl is not null) then
    if (coalesce(char_length(new.kontofk),0)<>0 and coalesce(char_length(new.kontofk),0) <> :dl) then  -- [DG] XXX ZG119346
      exception BAD_LENGTH_FKKOD;
  /* utworzenie prostego nipu */
  if(new.nip<>old.nip or (old.nip is null and new.nip is not null) or (old.nip is not null and new.nip is null)) then
    new.prostynip = replace(new.nip, '-', '');
  if(new.grupa<>old.grupa and new.grupa is not null) then select OPIS from GRUPYKLI where REF=new.grupa into new.grupanazwa;
  if(new.korespond is null) then new.korespond = 0;
  if(new.korespond = 0) then begin
    new.kulica = new.ulica;
    new.knrdomu = new.nrdomu;
    new.knrlokalu = new.nrlokalu;
    new.kmiasto = new.miasto;
    new.kkodp = new.kodp;
    new.kpoczta = new.poczta;
    new.kkraj = new.kraj;
  end
  -- sprawdzenie poprawnosci danych teleadresowych i obsluga ksiazki tel.
  execute procedure get_config('CONTACTCONTROL',2) returning_values :contactcontrol;
  if (:contactcontrol is null) then contactcontrol = '0';
  if (:contactcontrol = '1') then begin
    -- sprawdzenie poprawnosci email
    if( (new.email <> old.email and new.email <> '')
        or (new.email is not null and old.email is null) ) then
      execute procedure contact_control('EMAIL',new.email,0,'KLIENCI','EMAIL',new.ref,new.fskrot);
    -- sprawdzenie poprawnosci telefonu
    if( new.telefon <> old.telefon ) then
      execute procedure contact_control('PHONE',new.telefon,0,'KLIENCI','TELEFON',new.ref,new.fskrot);
    -- sprawdzenie poprawnosci komorki
    if( new.komorka <> old.komorka ) then
      execute procedure contact_control('CELL',new.komorka,0,'KLIENCI','KOMORKA',new.ref,new.fskrot);
    -- sprawdzenie poprawnosci telefonu
    if( new.fax <> old.fax ) then
      execute procedure contact_control('FAX',new.fax,0,'KLIENCI','FAX',new.ref,new.fskrot);
  end
  --wpis do historii
  if(new.historia = 1 and (new.nazwa <> old.nazwa or new.miasto <> old.miasto or new.kodp <> old.kodp or
      new.ulica <> old.ulica or new.poczta <> old.poczta)) then
  begin
    select count(*) from kliencihist where data = current_date and klient = new.ref into :cnt;
    if (cnt=0) then begin
      insert into kliencihist(klient, data, nazwa, kodp, miasto, ulica, nrdomu, nrlokalu, poczta)
      values (new.ref, current_date, old.nazwa, old.kodp, old.miasto, old.ulica, old.nrdomu, old.nrlokalu, old.poczta);
    end else exception universal 'System dokonał już dzisiaj zapisu w tabeli danych historycznych klienta, aby kontynuować zmianę odznacz znacznik "Czy zapisywać zmiany adresowe w historii klienta?"';
  end
  if(new.historia <> old.historia) then
    execute procedure get_config('KLIENCIHIST',1) returning_values new.historia;

  --Nieaktywny klient nie moze miec aktywnej umowy serwisowej
  --Zablokowanie mozliwosci deaktywacji klienta (proba zmiany KLIENCI.Aktywny na 0),
  --gdy istnieje przynajmniej jedna aktywna umowa serwisowa dla klienta (ktora posiada wartosc SRVCONTRACTS.Aktywny = 1)
  if(new.aktywny=0 and old.aktywny=1) then
    --Jezeli istnieje umowa serwisowa dla klienta (SRVCONTRACTS.dictdef = 1)
    --oraz umowa dotyczy naszego klienta (SRVCONTRACTS.dictpos = KLIENCI.ref)
    --oraz umowa jest aktywna
    --to zablokuj mozliwosc deaktywacji klienta
    if (exists (select aktywny from srvcontracts where srvcontracts.dictdef = 1 and new.ref=srvcontracts.dictpos and aktywny = 1)) then
      exception KLIENCI_AKTYWNA_UMOWA_SERWIS;
end^
SET TERM ; ^
