--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZSAD_AD0 FOR POZSAD                         
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable sumacla  numeric(14,2);
declare variable sumavat  numeric(14,2);
declare variable psumacla  numeric(14,2);
declare variable psumavat  numeric(14,2);
begin
  if(old.clo is not null or old.vat is  not null
      or old.pclo is not null or old.pvat is  not null
  ) then begin
    select sum(clo),sum(vat), sum(pclo), sum(pvat) from POZSAD
    where POZSAD.faktura=old.faktura into :sumacla, :sumavat, :psumacla, :psumavat;
    update NAGFAK set SUMCLO=:sumacla,SUMVAT=:sumavat, PSUMCLO=:psumacla,PSUMVAT=:psumavat
    where NAGFAK.REF = old.faktura;
  end
end^
SET TERM ; ^
