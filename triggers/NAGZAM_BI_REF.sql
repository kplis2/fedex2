--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGZAM_BI_REF FOR NAGZAM                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('NAGZAM')
      returning_values new.REF;
end^
SET TERM ; ^
