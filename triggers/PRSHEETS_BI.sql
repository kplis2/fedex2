--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHEETS_BI FOR PRSHEETS                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable usluga smallint;
BEGIN
  if(new.wersjaref is not null and new.wersjaref <> 0) then
    select nrwersji from WERSJE where ref=new.wersjaref into new.wersja;
  else if(new.wersja is not null and new.ktm is not null) then
    select ref from WERSJE where nrwersji = new.wersja and ktm = new.ktm into new.wersjaref;
  if(new.wersjaref=0) then new.wersjaref=null;
  if (new.wersjaref is null) then
    select wersje.ref, wersje.nrwersji from wersje where wersje.ktm = new.ktm and wersje.nrwersji = 0 into new.wersjaref, new.wersja;
  if(new.shortage is null) then new.shortage = 0;
  if (new.autodocin is null) then new.autodocin = 0;
  --KS--
  select t.usluga from towary t where t.ktm = new.ktm into :usluga;
  if (usluga = 1) then
    new.amountinfrom = 2;
  --koniec KS---
END^
SET TERM ; ^
