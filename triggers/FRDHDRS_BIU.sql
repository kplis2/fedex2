--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDHDRS_BIU FOR FRDHDRS                        
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable status smallint;
begin
  if (new.status is null) then new.status = 0;
  select frhdrs.status
    from frhdrs
    where frhdrs.symbol = new.frhdr
    into :status;
 /*if (:status <> 0) then
    exception FRHDRS_NOT_ACCESIBLE;  */
  if(coalesce(new.readrights,'')='') then new.readrights = ';';
  if(coalesce(new.readrightsgroup,'')='') then new.readrightsgroup = ';';
end^
SET TERM ; ^
