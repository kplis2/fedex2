--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFFORMY_BU0 FOR DEFFORMY                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable cnt integer;
begin
  if(new.akt = 0 and old.akt = 1) then begin
      select count(*) from DEFDOKWIT where forma = old.symbol into :cnt;
      if(cnt is null) then cnt = 0;
      /*proba deaktywacji formularza zwiazanego z dokumentem witryny */
      if(cnt > 0) then exception DEFFORMY_WITH_DEFDOKWIT;
  end

end^
SET TERM ; ^
