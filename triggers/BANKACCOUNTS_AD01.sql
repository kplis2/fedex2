--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKACCOUNTS_AD01 FOR BANKACCOUNTS                   
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable cnt integer;
declare variable slobanref integer;
begin
 if(old.gl = 1) then begin
   select min(ref) from bankaccounts where DICTDEF = old.DICTDEF and DICTPOS = old.DICTPOS
     and GL = 0 and hbstatus <> 1 into :slobanref;
   if(:slobanref > 0) then
     /*przelaczenie an inny reachunek bankwy*/
      update bankaccounts set GL = 1 where REF=:slobanref and act=1;
   else begin
     /*skasowanie ostatniego rachunku bankowego - wyczyszczenie pol w slownikach*/
     if(old.DICTDEF = 1) then
       update KLIENCI set BANK = null, RACHUNEK = '' where REF=old.DICTPOS;
     else if(old.DICTDEF = 6) then
       update DOSTAWCY set BANK = null, RACHUNEK = '' where REF=old.DICTPOS;
   end
 end
 if(old.DICTDEF = 6) then
    update DOSTAWCY set STATE = -1 where REF=old.DICTPOS;
  else if(old.DICTDEF = 1) then
    update KLIENCI set STATE = -1 where REF=old.DICTPOS;
end^
SET TERM ; ^
