--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRMACHREPAIRS_BI_SRVREQUESTS FOR PRMACHREPAIRS                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable new_REF int;
begin
  select PRREPAIRS, PHASE from CONTRTYPES where REF=new.CONTRTYPE into new.PRREPAIRS, new.STATUS;
  if (new.PRREPAIRS is null or new.PRREPAIRS<1 or new.PRREPAIRS>3) then exception PRMACHREPAIRS_EXPT 'Niedozwolony typ (CONTRTYPES).';
  if (new.SRVREQUEST is null) then begin
    execute procedure GEN_REF('SRVREQUESTS') returning_values new_REF;
    new.SRVREQUEST=new_REF;
    insert into SRVREQUESTS (REF,      SYMBOL    , CONTRTYPE    , STATUS    , REQDATE       , FAULTDESC      , SRVDESC         )
                     values (:new_REF, new.SYMBOL, new.CONTRTYPE, new.STATUS, new.REPAIRFROM, new.DAMAGECAUSE, new.REPAIRDESC  );
  end
end^
SET TERM ; ^
