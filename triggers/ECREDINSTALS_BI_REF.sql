--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECREDINSTALS_BI_REF FOR ECREDINSTALS                   
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ECREDINSTALS')
      returning_values new.REF;
end^
SET TERM ; ^
