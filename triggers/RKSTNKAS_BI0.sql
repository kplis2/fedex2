--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKSTNKAS_BI0 FOR RKSTNKAS                       
  ACTIVE BEFORE INSERT POSITION 0 
as
  declare variable country_currency varchar(255);
begin
  if (new.waluta is null or new.waluta = '') then
    exception RKSTNKAS_BRAKWALUTY;
  if (new.bank='') then
    new.bank = NULL;
  execute procedure GET_CONFIG('COUNTRY_CURRENCY', 2)
    returning_values :country_currency;
  if (country_currency<>new.waluta) then
    new.walutowa = 1;
  else
    new.walutowa = 0;
end^
SET TERM ; ^
