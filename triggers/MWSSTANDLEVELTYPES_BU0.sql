--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDLEVELTYPES_BU0 FOR MWSSTANDLEVELTYPES             
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if (new.l is null) then new.l = 0;
  if (new.w is null) then new.w = 0;
  if (new.h is null) then new.h = 0;
  if (new.levelheight is null) then new.levelheight = 0;
  if (new.loadcapacity is null) then new.loadcapacity = 0;
  if ((new.l <> old.l) or (new.h <> old.h) or (new.w <> old.w)) then
  begin
    new.volume = cast(new.l as numeric(14,4))/100 * cast(new.h as numeric(14,4))/100 * cast(new.w as numeric(14,4))/100;
  end
  if (new.mwsconstloctype <> old.mwsconstloctype or (new.mwsconstloctype is not null and old.mwsconstloctype is null)) then
    select maxpallocq from mwsconstloctypes where ref = new.mwsconstloctype
      into new.maxpallocq;
end^
SET TERM ; ^
