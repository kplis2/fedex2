--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPLANS_AU_CKONTRAKT FOR PMPLANS                        
  ACTIVE AFTER UPDATE POSITION 5 
AS
begin
  if((new.faza<>old.faza) or
     (new.contrtype<>old.contrtype) or
     (new.displayname<>old.displayname) or
     (coalesce(new.regdate,'')<>coalesce(old.regdate,'')) or
     (coalesce(new.regoper,0)<>coalesce(old.regoper,0)) or
     (coalesce(new.cpodmiot,0)<>coalesce(old.cpodmiot,0)) or
     (coalesce(new.descript,'')<>coalesce(old.descript,'')) or
     (coalesce(new.ovalue,0)<>coalesce(old.ovalue,0)) or
     (coalesce(new.plstartdate,'')<>coalesce(old.plstartdate,'')) or
     (coalesce(new.writerights,'')<>coalesce(old.writerights,''))) then begin
    update CKONTRAKTY set
      FAZA = new.FAZA,
      CONTRTYPE = new.CONTRTYPE,
      NAZWA = new.DISPLAYNAME,
      DATAZAL = new.REGDATE,
      OPERZAL = new.regoper, 
      CPODMIOT = new.CPODMIOT,
      OPIS = new.DESCRIPT,
      WARTPOT = new.OVALUE,
      DATAPLREAL = new.PLSTARTDATE,
      PRAWA = new.WRITERIGHTS
    where ref=new.contract;
  end


end^
SET TERM ; ^
