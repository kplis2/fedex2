--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDSEGTYPES_BIU0 FOR MWSSTANDSEGTYPES               
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.maxhordist is null) then new.maxhordist = 0;
  if (new.maxvertdist is null) then new.maxvertdist = 0;
end^
SET TERM ; ^
