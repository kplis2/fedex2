--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRVMANUALLS_BI FOR FRVMANUALLS                    
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  execute procedure get_global_param('AKTUOPERATOR') returning_values new.regoper;
  new.regdate = current_timestamp(0);
  if (exists(select ref from frvmanualls where period = new.period and frhdr = new.frhdr))
   then exception universal 'Istnieją już wartości dla sprawozdania '||new.frhdr||' w okresie '||new.period;
end^
SET TERM ; ^
