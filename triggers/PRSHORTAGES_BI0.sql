--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHORTAGES_BI0 FOR PRSHORTAGES                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable prschedopernumber integer;
declare variable prschedoperdeclnumber integer;
declare variable guide smallint;
begin
  if (new.shortageratio is null) then new.shortageratio = 1;
  if (new.regdate is null) then new.regdate = current_timestamp(0);
  if (new.amountzreal is null) then new.amountzreal = 0;
  if (new.operator is null) then
    execute procedure get_global_param('AKTUOPERATOR') returning_values new.operator;
  select pg.blocked from prschedopers po
      left join prschedguides pg on po.guide = pg.ref
    where po.ref = new.prschedoper
    into :guide;
  if(guide>0) then exception prschedguidesblocks_expt;
  if(new.amount is null) then new.amount = 0;
  if((new.prnagzam is null or new.prpozzam is null) and new.prschedguidespos is not null) then begin
    select pg.zamowienie, pp.pozzamref
      from prschedguidespos pp
      left join prschedguides pg on (pp.prschedguide = pg.ref)
      where pp.ref = new.prschedguidespos
    into new.prnagzam, new.prpozzam;
  end
  if(new.prschedoperdecl is not null) then begin
    select number from prschedopers where ref = new.prschedoperdecl into :prschedoperdeclnumber;
    select number from prschedopers where ref = new.prschedoper into :prschedopernumber;
    if(:prschedoperdeclnumber > :prschedopernumber) then
      exception prshortages_error 'Numer operacji deklarowanej nie może być wyższy od numeru operacji bieżącej';
  end
  if(new.prschedoper is not null and new.prschedguide is null) then
    select guide from prschedopers where ref = new.prschedoper into new.prschedguide;

  select p.noamountinfluence
    from prshortagestypes p
    where p.symbol = new.prshortagestype
    into new.noamountinfluence;
  if (new.amountreal is null ) then
    new.amountreal = 0;
  if (new.amountzreal is null) then
    new.amountzreal = 0;

  select m.reqktmtag
    from prshmat m
      left join prschedguidespos g on (g.prshmat = m.ref)
    where g.ref = new.prschedguidespos
  into new.reqktmtag;
end^
SET TERM ; ^
