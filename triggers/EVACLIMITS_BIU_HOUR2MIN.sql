--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACLIMITS_BIU_HOUR2MIN FOR EVACLIMITS                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 2 
AS
begin
/*DS: jezeli sa zaznaczone znaczniki modyf. indywidualna, na podstawie godzin przelicz
      minuty, a potem to juz mechanizm zrobi reszte*/

  if (new.addlimit_cust = 1) then
    execute procedure hours2minutes(new.addlimith) returning_values new.addlimit;

  if (new.actlimit_cust = 1) then
    execute procedure hours2minutes(new.actlimith) returning_values new.actlimit;

  if (new.disablelimit_cust = 1) then
    execute procedure hours2minutes(new.disablelimith) returning_values new.disablelimit;

  if (new.traininglimit_cust = 1) then
    execute procedure hours2minutes(new.traininglimith) returning_values new.traininglimit;

  /*jedyny przypadek kiedy ma nie przeliczac to gdy po updacie liczba godzin sie nie zmienila
    czyli pozwolmy systemowi obliczyc ulop dodatkowy*/
  if (inserting or new.otherlimith <> old.otherlimith) then
    execute procedure hours2minutes(new.otherlimith) returning_values new.otherlimit;
end^
SET TERM ; ^
