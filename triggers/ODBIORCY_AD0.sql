--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ODBIORCY_AD0 FOR ODBIORCY                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable odbiorcaid integer;
begin
    update KLIENCi set STATE = -1 where REF=old.klient;
 if(old.gl = 1) then begin
   select min(ref) from ODBIORCY where KLIENT = old.klient and GL = 0 into :odbiorcaid;
   if(:odbiorcaid > 0) then
     /*przelaczenie an inny reachunek bankwy*/
     update ODBIORCY set GL = 1 where REF=:odbiorcaid;
 end
 update ODBIORCY set ODBIORCASPEDYC = NULL where ODBIORCASPEDYC = old.ref;
end^
SET TERM ; ^
