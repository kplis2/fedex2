--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TABKURS_BI FOR TABKURS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  if(new.bank is not null AND (NEW.SYMBOL IS NULL or (NEW.SYMBOL = ''))) then
    new.symbol = substring(new.bank from 1 for 9) || '/' ||cast(new.data as date);
  if(new.symbol is null) then exception TABKURS_SYMBOLNULL;
  if(new.akt is null) then new.akt = 1;
END^
SET TERM ; ^
