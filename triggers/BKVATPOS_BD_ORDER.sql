--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BKVATPOS_BD_ORDER FOR BKVATPOS                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update bkvatpos set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and bkdoc = old.bkdoc;
end^
SET TERM ; ^
