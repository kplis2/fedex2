--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WFCHANGELOG_BI_GEN FOR WFCHANGELOG                    
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_WFCHANGELOG_id,1);
end^
SET TERM ; ^
