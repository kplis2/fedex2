--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHG_BI_ORDER FOR DEFCECHG                       
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable maxnum integer;
begin
  new.ord = 1;
  select max(lp) from defcechg
    into :maxnum;
  if (new.lp is null) then
  begin
    if (maxnum is null) then maxnum = 0;
    new.lp = maxnum + 1;
  end else if (new.lp <= :maxnum) then
  -- zmiana istniejacej kolejnosci
  begin
    update defcechg set ord = 0, lp = lp + 1
      where lp >= new.lp;
  end else if (new.lp > maxnum) then
    new.lp = maxnum + 1;
end^
SET TERM ; ^
