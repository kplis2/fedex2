--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDZAM_BI FOR PRSCHEDZAM                     
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable outonpozzamenabled smallint;
begin
  select n.outonpozzamenabled
    from nagzam n where n.ref = new.zamowienie
    into outonpozzamenabled;
  if (outonpozzamenabled is null) then outonpozzamenabled = 0;
  new.outonpozzamenabled = outonpozzamenabled;
  if(new.schedblocked is null) then new.schedblocked = 0;
  select max(NUMBER) from prschedzam where prschedule = new.prschedule into new.number;
  if(new.number is null) then new.number = 0;
  new.number = new.number + 1;
  new.status = 0;
  new.resstatus = 0;
  if(new.prsheet is null) then
    select nagzam.prsheet from NAGZAM where ref=new.zamowienie into new.prsheet;
  if (outonpozzamenabled = 0) then
  begin
    if(new.prsheet is null) then
      select PRSHEETS.ref from prsheets join nagzam on (nagzam.kktm = prsheets.ktm and prsheets.status = 2) into new.prsheet;
    if(new.prsheet is null) then
      exception PRSCHEDZAM_ERROR 'Brak wskazania karty technologicznej dla pozycji hamronogramu';
    if(new.prshcalc is null) then
      select prshcalcs.ref from prshcalcs where prshcalcs.sheet = new.prsheet and prshcalcs.status = 2 into new.prshcalc;
  end
--  if(new.prshcalc is null) then
  if (new.prdepart is null) then
    select prdepart from prschedules where ref = new.prschedule into new.prdepart;
  if (exists(select ref from prschedzam s where s.zamowienie = new.zamowienie and s.prschedule = new.prschedule and s.ref <> new.ref)) then
    exception prschedzam_error 'Wskazane zlecenie jest juz dodane do harmonogramu';
  select wersje.nazwat
    from wersje
    join nagzam on (nagzam.kktm = wersje.ktm and nagzam.kwersja = wersje.nrwersji)
    where nagzam.ref = new.zamowienie
  into new.nazwat;

  select n.prpriority
    from nagzam n
    where n.ref = new.zamowienie
    into new.prpriority;
end^
SET TERM ; ^
