--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_AD_FSBALANCES FOR KLIENCI                        
  ACTIVE AFTER DELETE POSITION 0 
AS
declare variable slodef INTEGER ;
begin
  if (old.dostawca is not null ) then
  begin
    select max(s.ref)
      from slodef s
      where s.typ = 'KLIENCI'
      into :slodef;
    if (:slodef is not null) then
      execute procedure calculate_fsbalances(:slodef,old.ref);
  end
end^
SET TERM ; ^
