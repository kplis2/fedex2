--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_BU0 FOR DOKUMPOZ                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable usluga smallint;
declare variable mag char(3);
declare variable mag_typ char(1);
declare variable wydania integer;
declare variable koryg smallint;
declare variable zewn smallint;
declare variable typ_dok char(3);
declare variable dostawa integer;
declare variable fifo char(1);
declare variable ii numeric(14,4);
declare variable ack integer;
declare variable przelicz numeric(14,4);
declare variable przconst smallint;
declare variable opktryb smallint;
declare variable walutowy smallint;
declare variable kurs numeric(14,4);
declare variable cnt integer;
declare variable dniwaznbufc varchar(255);
declare variable dniwaznbufi integer;
declare variable dniwazn integer;
declare variable zrodlo smallint;
declare variable jedn integer;
declare variable klient integer;
declare variable bn char(1);
declare variable status integer;
declare variable gr_vat varchar(5);
declare variable vat numeric(14,2);
declare variable rabatnag numeric(14,2);
begin
  if((new.wersjaref <> old.wersjaref or (new.ktm is null) or (new.wersja is null)) and new.wersjaref is not null and new.wersjaref <> 0) then begin
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  end else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)or (new.wersjaref is null)) then
    select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  select USLUGA, DNIWAZN from TOWARY where KTM = new.KTM into :usluga, :dniwazn;
  if(new.wartsnetto is null) then new.wartsnetto = 0;
  if(new.wartsbrutto is null) then new.wartsbrutto = 0;
  select MAGAZYN, typ, dostawa, klient, walutowy, kurs, AKCEPT, BN, RABAT from DOKUMNAG where ref = new.dokument into :mag, :typ_dok, :dostawa, :klient, :walutowy, :kurs, :ack, :bn, :rabatnag;
  if(:rabatnag is null) then rabatnag = 0;
  select WYDANIA, OPAK, ZEWN, KORYG from DEFDOKUM where SYMBOL=:typ_dok
    into :wydania, opktryb, :zewn, :koryg;
  if(:bn is null or :bn=' ') then bn = 'N';
     /* sprawdzenie, czy dla magazynu partiowego dokument przychodowy dostawa zgodna z naglowkiem */
  select TYP, FIFO from DEFMAGAZ where SYMBOL=:mag into :mag_typ, :fifo;
  if(:wydania = 0 and :mag_typ = 'P' and((new.dostawa is null)or (new.dostawa = 0))) then
    new.dostawa = :dostawa;
  if(new.ilosc is null) then new.ilosc = 0;
  if(new.ilosco is null) then new.ilosco = 0;
  if(new.cena is null) then new.cena = 0;
  if(new.cenawal is null) then new.cenawal = 0;
  if(new.cena_koszt is null) then new.cena_koszt = 0;
  if(new.cenasr_koszt is null) then new.cenasr_koszt = 0;
  if(new.wart_koszt is null) then new.wart_koszt = 0;
  if(new.foc is null) then new.foc = 0;
  if (coalesce(new.fake,0)=1) then
  begin
    new.opk = 1;
    update dokumnag dn
      set dn.altpoz = 1
      where dn.ref = new.dokument;
  end else
  begin
    new.opk = 0;
    if (not exists (select first 1 1 from dokumpoz dp where dp.dokument = new.dokument and dp.fake = 1)) then
    begin
      update dokumnag dn
      set dn.altpoz = 0
      where dn.ref = new.dokument;
    end
  end
  -- jesli nie uzupelniono jedno lub zmienil sie ktm i obecne jedno nie nalezy do new.ktm, to ustal jedno domyslne
  if(new.jedno is null
     or (coalesce(old.ktm,'')<>coalesce(new.ktm,'')
     and not exists(select t.ref from towjedn t where t.ktm = coalesce(new.ktm,'') and t.ref = new.jedno))) then
    select REF from TOWJEDN where KTM = new.ktm and GLOWNA = 1 into new.jedno;
  select PRZELICZ, CONST from TOWJEDN where REF=new.jedno into :przelicz, przconst;
  if(:przconst = 1 or (new.ilosco = 0)) then begin
    if(:przelicz > 0 ) then
      new.ilosco = new.ilosc / :przelicz;
  end else  if(new.ilosco <> old.ilosco and :przconst = 1) then
    new.ilosc = new.ilosco * :przelicz;
  if((new.cenawal <> old.cenawal) or (new.ilosc<>old.ilosc)) then begin
    if(new.prec<>4) then new.cenawal = cast(new.cenawal as numeric(14,2));
    new.wartoscwal = new.cenawal * new.ilosc;
  end
  if((new.cenawal<>old.cenawal) or (new.rabat=-101)) then begin
    if(new.rabat=-101) then new.rabat = old.rabat;
    if(:walutowy=1 and :wydania = 0) then begin     --DD
      new.cena = new.cenawal*:kurs;
    end
  end
  if(:usluga = 1) then begin
    new.cena = 0;
    new.dostawa = null;
    new.cenasr = 0;
  end
  if((new.cena <> old.cena) or (new.ilosc<>old.ilosc)) then begin
    new.cenasr = new.cena;
    new.wartosc = new.cena * new.ilosc;
  end
  -- obliczenie ceny sprzedazy, ale tylko gdy wskazano refcennik
  if(new.refcennik is not null and new.cenacen is null) then begin
    -- pobierz cene z cennika
    execute procedure POBIERZ_CENE(new.refcennik,new.wersjaref, :jedn, :bn, new.cenacen, new.walcen, new.prec, new.cena, new.dostawa, :klient, 'M', new.dokument)
      returning_values new.cenacen, new.walcen, :jedn, :status, new.prec;
  end
  if(new.cenacen is null) then new.cenacen = 0;
  if(new.rabat is null) then new.rabat = 0;
  -- MSt: BS70028: Ceny sprzedazy zerowe dla fejkow
  if (new.fake = 1 and
      (new.fake is distinct from old.fake or new.cenacen is distinct from old.cenacen or new.cenanet is distinct from old.cenanet)) then
  begin
    new.cenacen = 0;
    new.cenanet = 0;
    new.cenabru = 0;
    new.cenawal = 0;
    new.wartsnetto = 0;
    new.wartsbrutto = 0;
  end
  -- MSt: Koniec
  -- oblicz wartosci
  if(new.ilosc<>old.ilosc or new.cenacen<>old.cenacen or new.walcen<>old.walcen or new.rabat<>old.rabat or new.gr_vat <> old.gr_vat ) then begin
    select coalesce(vat.stawka,0.0) from vat where vat.grupa = new.gr_vat into :vat;
    execute procedure OBLICZ_CENE(:bn, new.ilosc, new.cenacen, new.walcen, new.rabat + :rabatnag, :vat, new.prec)
      returning_values new.cenanet, new.cenabru, new.wartsnetto, new.wartsbrutto;
  end
  if(new.ilosco <> old.ilosco or (new.jedno <> old.jedno)) then begin
    select TOWJEDN.waga*new.ilosco, towjedn.vol*new.ilosco from TOWJEDN where ref=new.jedno into new.waga, new.objetosc;
  end
  if((new.ktm <> old.ktm ) or(new.wersja <> old.wersja ) ) then begin
    /*wyfofanie tutaj, by rozpiski widzialy proawidlowa wartosc ktm i werjsa, jesli by to mialy  byc wycofania z magazynu */
    if(new.roz_blok=0 and old.roz_blok=0) then begin
      if(:ack = 1 or :ack=8) then begin
        exception DOKUMNAG_AKCEPT;
--      ii = - old.ilosc;
--      execute procedure DOKUMPOZ_CHANGE(old.ref, old.ktm, old.wersja, old.cena, old.dostawa, :ii,1,0);
      end
    end
  end
  if(:opktryb > 0) then begin
    if(new.opk is null and :usluga = 3) then new.opk = 1;
    if(new.opk is null and :usluga <> 3) then new.opk = 0;
    if(new.opk = 1 and new.cenacen = 0) then new.opk = 2;
  end else if (new.opk <> 1 or new.opk is null) then -- PR 9337 - mozliwosc dodania opakowan do dokumentu, ktory nie robi obrotu opakowaniami do celów promocji
    new.opk = 0;
  if (new.opk is null) then exception DOKUMPOZ_OPK_NOT_SET;
  --if(:usluga = 1 and new.opk = 0) then exception DOKUMPOZ_USLUGA;
  if(new.blokcena = 1 and old.blokcena = 0) then begin
    /*weryfikacja, czy na pewno cena ma być blokowana*/
    if(:wydania = 1) then new.blokcena = 0;
    else begin
      if(not exists(select dokumroz.ref
                      from DOKUMROZ
                      join DOKUMPOZ on (DOKUMPOZ.ref = DOKUMROZ.POZYCJA)
                      left join DOKUMNAG on (DOKUMPOZ.DOKUMENT = DOKUMNAG.REF)
                      left join DEFDOKUM on (DEFDOKUM.symbol = DOKUMNAG.TYP)
                      where DEFDOKUM.WYDANIA = 1 and DOKUMNAG.MAGAZYN = :mag
                      and DOKUMPOZ.WERSJAREF = new.wersjaref
                      and DOKUMROZ.cena = new.bcena and dokumroz.dostawa = new.bdostawa)) then
      begin
        if(not exists(select pozycja from dokumroz join dokumnotp on (DOKUMNOTP.dokumrozkor = dokumroz.ref)
                    where DOKUMROZ.pozycja = new.ref))
        then new.blokcena = 0;
      end
    end
  end
  if(:dniwazn>0 and :wydania = 0 and :opktryb = 0 and :zewn = 1 and koryg = 0) then begin
    if(new.datawazn<>old.datawazn or (new.datawazn is not null and old.datawazn is null) or (new.datawazn is null and old.datawazn is not null)) then
    begin
      execute procedure GETCONFIG('DNIWAZNBUF') returning_values :dniwaznbufc;
      select zrodlo from DOKUMNAG where ref = new.dokument into :zrodlo;
      if((:zrodlo = 0 or zrodlo = 1) and :dniwaznbufc<>'' and :dniwaznbufc<>'-1') then begin
        if(new.datawazn is null) then exception DOKUMPOZ_BAD_EXPIRYDATE 'Nie podano daty ważnożci dla: '|| new.ktm;
        dniwaznbufi = cast(:dniwaznbufc as integer);
        if(new.datawazn < current_date) then
          exception DOKUMPOZ_BAD_EXPIRYDATE 'Błędna data ważnosci dla: ' || new.ktm || '. Data mniejsza od dzisiejszej.';
        if(new.datawazn > current_date + :dniwazn + :dniwaznbufi) then
          exception DOKUMPOZ_BAD_EXPIRYDATE   'Błędna data ważnosći dla: '|| new.ktm || '. Towar ma ' || :dniwazn || ' dni przydatności. ';
      end
    end
  end
  if (new.iloscdorozb is null) then new.iloscdorozb = 0;
  if((new.iloscdorozb<>old.iloscdorozb)) then begin
    if((new.iloscdorozb < 0) or (new.iloscdorozb > new.ilosc)) then
      exception DOKUMPOZ_EXPT;
  end
  if (new.krajpochodzenia = '') then new.krajpochodzenia = null;
  if(new.ilosc<>old.ilosc) then begin
    if(new.ilosc < 0)then
      exception DOKUMPOZ_EXPT 'Ilość towaru na pozycji dok. mag. nie może być ujemna!';
    if(new.ilosc = 0)then
      exception DOKUMPOZ_EXPT 'Ilość towaru na pozycji dok. mag. nie może być zerowa!';
  end
end^
SET TERM ; ^
