--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKROZ_AD_STANYWAL FOR RKDOKROZ                       
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  update RKSTANWAL S set STAN = STAN - old.KWOTA,
      PRZYCHODY = PRZYCHODY - IIF(old.kwota > 0, old.KWOTA,0),
      ROZCHODY = ROZCHODY + IIF(old.kwota < 0, old.KWOTA,0)
      where S.REF = old.RKSTANWAL;

  delete from rozrachp r where r.stable = 'RKDOKROZ' and r.sref = old.ref;
end^
SET TERM ; ^
