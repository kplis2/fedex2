--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLSHISTORY_BI0 FOR PRTOOLSHISTORY                 
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.datereal is null) then new.datereal = current_timestamp(0);
  if (new.dateaction is null) then new.dateaction = current_timestamp(0);

end^
SET TERM ; ^
