--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLTAXINFO_BI_REF FOR EMPLTAXINFO                    
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EMPLTAXINFO')
      returning_values new.REF;
end^
SET TERM ; ^
