--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMAILWIAD_AU_IID FOR EMAILWIAD                      
  ACTIVE AFTER UPDATE POSITION 2 
AS
begin
  if(new.iid > 0 and coalesce(old.iid,0) = 0 and new.uid > 0) then begin
    --zaktualizowano numer iid - np. po przesunieciu miedzy folderami - "rozsuwam"
    if (exists (select ref from EMAILWIAD where folder = new.folder and iid = new.iid and ref <> new.ref))
    then begin
      update EMAILWIAD set IID = IID+1 where folder = new.folder and iid >= new.iid;
    end
  end
end^
SET TERM ; ^
