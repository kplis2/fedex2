--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPAYROLLS_BD_STATUSPAYROLLS FOR EPAYROLLS                      
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if (old.empltype = 2) then --BS54429
  begin
    if (old.status > 0) then
      exception epayrolls_closed trim('Rachunek ' || old.symbol) || ' '
       || trim(iif(old.status = 1, 'zaakceptowany', 'zaksięgowany'))
       || ' - usunięcie niemożliwe!';
  end else begin
    if (old.status = 1) then
      exception epayrolls_closed;
    else if (old.status = 2) then
      exception epayrolls_booked;
    if (exists (select first 1 1 from eprempl where epayroll = old.ref)) then
      exception epayrolls_positions;
  end
end^
SET TERM ; ^
