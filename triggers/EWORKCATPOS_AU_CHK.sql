--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EWORKCATPOS_AU_CHK FOR EWORKCATPOS                    
  ACTIVE AFTER UPDATE POSITION 1 
AS
begin
 if (exists (select first 1 1 from eworkcats where israngectrl = 1 and ref = new.workcat) and
     exists (select first 1 1 from EWORKCATPOS
               where (((new.topay <= topay and new.topay >= frompay) or (new.frompay <= topay and new.frompay >= frompay))
                   or (new.topay > topay and frompay > new.frompay) or (new.frompay < frompay and topay < new.topay))
                 and workcat = new.workcat
                 and ref <> new.ref
                 and new.isactive = 1
                 and isactive = 1  )) then
   exception WORKCAT_OVERLAP;
end^
SET TERM ; ^
