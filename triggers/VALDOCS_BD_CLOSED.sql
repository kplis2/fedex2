--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VALDOCS_BD_CLOSED FOR VALDOCS                        
  ACTIVE BEFORE DELETE POSITION 0 
AS
  declare variable period_status integer;
begin
  select status from amperiods where ref = old.amperiod into :period_status;
  if (period_status > 0) then exception AMPERIOD_CLOSED 'Nie można usunąć dokumentu dla zamkniętego okresu!';
  if(old.status = 1)then exception test_break 'Dokument zaksięgowany. Usunięcie zablokowane';
end^
SET TERM ; ^
