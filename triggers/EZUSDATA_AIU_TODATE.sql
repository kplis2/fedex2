--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EZUSDATA_AIU_TODATE FOR EZUSDATA                       
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
as
declare variable todate date;
declare variable fromdate date;
begin
/*MWr: Ustalenie czasu trwania rekordow przy edycji rekordu. Zmiana 'daty od'
       na rekordzie pociaga za soba odswiezenie wartosci TODATE na wszystkich
       wpisach w tabeli EZUSDATA danej osoby*/

  if (inserting or new.fromdate <> old.fromdate) then
  begin
  --ustalenie daty rozpoczyna sie od najmlodszego rekordu
    update ezusdata
      set todate = null
      where person = new.person
        and fromdate = (select max(fromdate)
                          from ezusdata
                          where person = new.person);

  --przejscie przez wszytskie wpisy
    for
      select fromdate
        from ezusdata
        where person = new.person
        order by fromdate desc
        into :fromdate
    do begin
    --aktualizacja TODATE dla rekordu wczesniejszego
      todate = fromdate - 1;
      update ezusdata
        set todate = :todate
        where ref in (select first 1 ref from ezusdata
                        where person = new.person
                          and fromdate < :fromdate
                        order by fromdate desc);
    end
  end
end^
SET TERM ; ^
