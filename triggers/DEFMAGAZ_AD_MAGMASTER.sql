--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFMAGAZ_AD_MAGMASTER FOR DEFMAGAZ                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable werref integer;
declare variable ilosc numeric(14,4);
begin
  if(old.magmaster is not null and old.magmaster <> '') then
  begin
    if (not exists(select symbol from defmagaz where magmaster = old.magmaster)) then
      update defmagaz set ismagmaster = 0 where symbol = old.magmaster;
  end
end^
SET TERM ; ^
