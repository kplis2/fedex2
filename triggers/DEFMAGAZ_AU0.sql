--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFMAGAZ_AU0 FOR DEFMAGAZ                       
  ACTIVE AFTER UPDATE POSITION 0 
as
  declare variable slodef integer;
  declare variable currentyear integer;
  declare variable company integer;
begin
  if(new.SYMBOL <> old.SYMBOL) then begin
    update DEFOPERMAG set MAG2 = new.SYMBOL where MAG2=old.symbol;
  end
  if(new.glowny <> old.glowny) then
    update STANYIL set GLOWNY = new.glowny where MAGAZYN = new.symbol;

  -- aktulizacja opisu kont ksiegowych dla bieżącego roku
  if(new.opis<>old.opis) then begin
    currentyear = extract(year from current_timestamp(0));
    select company from oddzialy where oddzial=new.oddzial into :company;
    for select ref from slodef where typ = 'DEFMAGAZ'
      into :slodef
    do begin
      execute procedure accounting_recalc_descript(:slodef, :currentyear, :company, new.bksymbol);
    end
  end
end^
SET TERM ; ^
