--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CKATALP_BI0 FOR CKATALP                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable cdefkatal integer;
begin
  select CDEFKATAL from CKATAL where REF=new.ckatal into :cdefkatal;
  select NUMER from CDEFKATALP where CDEFKATAL=:cdefkatal and AKRONIM=new.symbol into new.numer;
end^
SET TERM ; ^
