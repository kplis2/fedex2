--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTUREDISTS_BD_WPK FOR ACCSTRUCTUREDISTS              
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable ispattern smallint_id;
declare variable company companies_id;
declare variable pattern_delete string10;
begin
  -- Sprawdzamy, czy nie jest zalezny
  if (coalesce(old.pattern_ref,0)=0) then
  begin
    -- Jak nie to może jest wzorcowy
    if (old.otable = 'BKACCOUNTS') then
      select  b.company
        from  bkaccounts b
        where b.ref = old.oref
        into :company;
    else if (old.otable = 'SLOPOZ') then
      select sd.company
        from slopoz sp join slodef sd on (sp.slownik = sd.ref)
        where sp.ref = old.oref
        into :company;
   if (company is not null) then
     select c.pattern
       from companies c
       where c.ref = :company
       into :ispattern;
   if (coalesce(ispattern,0)=1) then
      rdb$set_context('USER_TRANSACTION', 'PATTERN_DELETE', 'TRUE');
     delete from accstructuredists
       where pattern_ref = old.ref;
  end else begin
    pattern_delete = coalesce(rdb$get_context('USER_TRANSACTION', 'PATTERN_DELETE'),'FALSE');
    if (pattern_delete <> 'TRUE') then
      exception wpk_delete_error_accstrdist;
  end
end^
SET TERM ; ^
