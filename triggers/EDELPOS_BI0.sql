--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDELPOS_BI0 FOR EDELPOS                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable typ smallint;    /* Typ delegacji: 0 - krajowa, 1 - zagraniczna*/
  declare variable dataod date;     /* Data poczatkowa na naglowku delegacji */
  declare variable datado date;     /* Data koncowa na naglowku delgacji */
begin
  select typ, fromdate, todate
    from edelegations
    where ref = new.delegation
  into :typ, :dataod, :datado;

  if(:typ = 0) then new.incountry = 1;
  if((cast(new.fromdate as date)<:dataod) or (cast(new.todate as date)>:datado)) then
    exception EDELPOS_DATY;
end^
SET TERM ; ^
