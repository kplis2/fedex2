--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNOTP_BI0 FOR DOKUMNOTP                      
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable pozycja integer;
declare variable akcept integer;
declare variable typ varchar(3);
declare variable przecena smallint;
begin
  akcept=0;
  select AKCEPT, typ from DOKUMNOT where DOKUMNOT.REF = new.dokument into :akcept, :typ;
  select PRZECENA from DEFDOKUM where SYMBOL=:typ into :przecena;
  if(new.akceptpoz is null) then new.akceptpoz = :akcept;
  if(:przecena is null) then przecena = 0;
  -- dokument przeceny nie ma powiazania do dokumentu magazynowego
  -- pozycja sklonowana takze nie jest niczym uzupelniana
  if(akcept < 7 and :przecena=0 and new.clonedfrom is null) then begin
    select pozycja, cena from DOKUMROZ where REF=new.dokumrozkor into :pozycja, new.cenaold;
    if(:pozycja is null) then exception DOKUMNOTP_BRAKDOKUMROZ;
    if(new.ilosc is null or (new.ilosc = 0)) then
      select ILOSC from DOKUMROZ where REF=new.dokumrozkor into new.ilosc;
    select KTM, WERSJAREF from DOKUMPOZ where ref=:pozycja into new.ktm, new.wersjaref;
  end
  if(new.cenaold is null) then new.cenaold = 0;
  if(new.cenanew is null) then new.cenanew = 0;
  if (new.dostawa is null) then
    select r.dostawa from dokumroz r where r.ref = new.dokumrozkor
      into new.dostawa;
  if (new.dokumnagdate is null) then
    select n.data
      from dokumroz r
        join dokumpoz p on (p.ref = r.pozycja)
        join dokumnag n on (n.ref = p.dokument)
      where r.ref = new.dokumrozkor
      into new.dokumnagdate;
end^
SET TERM ; ^
