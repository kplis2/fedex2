--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWSORDS_AU_RW_WOZKI FOR MWSORDS                        
  ACTIVE AFTER UPDATE POSITION 200 
as
  declare variable  refdoc dokumnag_id;
begin
  -- przy realizacji wt z dokumentu rw chemy zeby wozki automatycznie sie zwalnialy (nie potrzebne jest pakowanie)
  if (old.status <> new.status and new.status = 5) then --gdy konczymy dokument
  begin
    --dla wszystkich rw z grupy spedycyjnej
    for
      select dn.ref from dokumnag dn
        where new.docgroup = dn.grupasped and dn.typ = 'RW'
      into refdoc
    do begin
      --zwalniam wuzek
      execute procedure X_MWS_RELASE_CART(refdoc);
      refdoc = null;
    end
  end
end^
SET TERM ; ^
