--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROPERSRAPS_BU0 FOR PROPERSRAPS                    
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable properrapcalcamountr smallint;
begin
  if(new.amount is null) then new.amount = 0;
  if(new.setuptime is null) then new.setuptime = 0;
  if (new.maketimefrom > new.maketimeto) then
    exception propersraps_error 'Data rozpoczęcia nie może być późniejsza od daty zakończenia';

  execute procedure GET_CONFIG('PROPERRAPCALCAMOUNTR', 2) returning_values :properrapcalcamountr;
  if(coalesce(properrapcalcamountr,0) = 0) then begin
    if(new.econtractsposdef is not null) then
      select E.calcamountresult
        from econtractsposdef E
        where E.ref = new.econtractsposdef
      into new.calcamountresult;
    else new.calcamountresult = 1;
  end
end^
SET TERM ; ^
