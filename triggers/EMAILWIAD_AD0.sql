--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMAILWIAD_AD0 FOR EMAILWIAD                      
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  if(old.stan = 0 ) then
    update DEFFOLD set NIEPRZECZYTANE = NIEPRZECZYTANE - 1 where ref=old.folder;
  if(old.iid > 0) then
    update EMAILWIAD set IID = IID - 1 where FOLDER = old.folder and IID > old.iid;
  if(coalesce(old.uid,0) <> 0)then
    execute procedure emailwiad_calc_maxuid(old.folder);

  delete from dokzlacz d where d.ztable = 'EMAILWIAD' and d.zdokum = old.ref;

end^
SET TERM ; ^
