--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CLIENTRELATIONS_AD FOR CLIENTRELATIONS                
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  if (exists(select klient from CLIENTRELATIONS where klient=old.relationto
      and RELATIONTO=old.KLIENT)) then
    delete from CLIENTRELATIONS where klient=old.relationto and relationto=old.klient;
end^
SET TERM ; ^
