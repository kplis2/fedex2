--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER UZYKLI_BU_REPLICAT FOR UZYKLI                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.ref<>old.ref or
      new.klient <> old.klient or
      new.id <> old.id or
      new.haslo <> old.haslo or (new.haslo is null and old.haslo is not null) or (new.haslo is not null and old.haslo is null) or
      new.imie <> old.imie or (new.imie is null and old.imie is not null) or (new.imie is not null and old.imie is null) or
      new.nazwisko  <> old.nazwisko or (new.nazwisko is null and old.nazwisko is not null) or (new.nazwisko is not null and old.nazwisko is null) or
      new.rola <> old.rola or (new.rola is null and old.rola is not null) or (new.rola is not null and old.rola is null) or
      new.telefon <> old.telefon or (new.telefon is null and old.telefon is not null) or (new.telefon is not null and old.telefon is null) or
      new.fax <> old.fax or (new.fax is null and old.fax is not null) or (new.fax is not null and old.fax is null) or
      new.email <> old.email or (new.email is null and old.email is not null) or (new.email is not null and old.email is null) or
      new.uwagi <> old.uwagi or (new.uwagi is null and old.uwagi is not null) or (new.uwagi is not null and old.uwagi is null) or
      new.dulica <> old.dulica or (new.dulica is null and old.dulica is not null) or (new.dulica is not null and old.dulica is null) or
      new.dkodp <> old.dkodp or (new.dkodp is null and old.dkodp is not null) or (new.dkodp is not null and old.dkodp is null) or
      new.dpoczta <> old.dpoczta or (new.dpoczta is null and old.dpoczta is not null) or (new.dpoczta is not null and old.dpoczta is null) or
      new.witryna <> old.witryna or (new.witryna is null and old.witryna is not null) or (new.witryna is not null and old.witryna is null) or
      new.faktura <> old.faktura or (new.faktura is null and old.faktura is not null) or (new.faktura is not null and old.faktura is null) or
      new.pesel <> old.pesel or (new.pesel is null and old.pesel is not null) or (new.pesel is not null and old.pesel is null)) then
   waschange = 1;

  execute procedure rp_trigger_bu('UZYKLI',old.ref, null, null, null, null, old.token, old.state,
        new.ref, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
