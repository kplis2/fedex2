--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRDACCOUNTING_AI FOR PRDACCOUNTING                  
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable multi smallint;
declare variable bkacc integer;
declare variable descript varchar(255);
declare variable multiprd smallint;

begin
  multiprd = 0;
  if(new.firstprdversion > 0) then begin
      update or insert into PRDVERSIONS(REF, PRDACCOUNTING, FROMPERIOD)
      values (new.firstprdversion, new.ref, new.firstperiod)
      matching (REF);
  end
  select coalesce(b.multiprd,0) from bkaccounts b where (substring(new.account from 1 for 3) = b.symbol
  and substring(new.decreeperiod from 1 for 4) = b.yearid) and b.company = new.company into :multi;
  --jezeli konto nie ma wielu kont odpisu to sprawdzamy czy konto odpisu ma wyrozniki wielowartosciowe
  if(:multi = 0) then
  select first 1 MULTI
    from check_account(new.company, new.deductaccount, cast(substring(new.firstperiod from  1 for 4) as integer), 0)
    into :multiprd;
  if(:multi = 0 and :multiprd = 0) then begin
    execute procedure FK_GET_ACCOUNT_DESCRIPT(new.company, new.decreeperiod, new.deductaccount) returning_values :descript;
    insert into PRDACCOUNTINGPOS(PRDACCOUNTING, DEDUCTACCOUNT, DEDUCTAMOUNT, FIRSTDEDUCTAMOUNT,
                                 DIST1DDEF, DIST1SYMBOL, DIST2DDEF, DIST2SYMBOL, DIST3DDEF, DIST3SYMBOL, DIST4DDEF,
                                 DIST4SYMBOL, DIST5DDEF, DIST5SYMBOL, DIST6DDEF, DIST6SYMBOL, DECREE, DESCRIPT, DIVRATE, PRDVERSION)
    values(new.ref, new.deductaccount, new.deductamount, new.firstdeductamount,
           new.DIST1DDEF, new.DIST1SYMBOL, new.DIST2DDEF, new.DIST2SYMBOL, new.DIST3DDEF, new.DIST3SYMBOL, new.DIST4DDEF,
           new.DIST4SYMBOL, new.DIST5DDEF, new.DIST5SYMBOL, new.DIST6DDEF, new.DIST6SYMBOL, new.decree, :descript, 100, new.firstprdversion);
  end
  update decrees set prdaccounting = new.ref where ref = new.decree;
end^
SET TERM ; ^
