--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLKARIERA_BI_REF FOR CPLKARIERA                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
 if (new.REF is null or new.REF=0) then
   execute procedure GEN_REF('CPLKARIERA')
     returning_values new.REF;
end^
SET TERM ; ^
