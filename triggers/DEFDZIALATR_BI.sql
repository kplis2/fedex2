--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFDZIALATR_BI FOR DEFDZIALATR                    
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  new.ord = 0;
  if(new.klasa is not null and new.dzial is not null) then
    exception DEFDZIALATR_DZIALKLASA;
END^
SET TERM ; ^
