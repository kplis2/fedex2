--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OPERGRUPY_BD0 FOR OPERGRUPY                      
  ACTIVE BEFORE DELETE POSITION 0 
as

begin
  if (exists(select ref from OPERATOR
      where GRUPA like ('%;' || old.symbol || ';%'))) then
    exception OPERGRUPY_USED;
end^
SET TERM ; ^
