--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PLATNOSCI_AI_SYNC_TRANSLATIONS FOR PLATNOSCI                      
  ACTIVE AFTER INSERT POSITION 0 
as
begin
  execute procedure SYNC_SYS_TRANSLATIONS('PLATNOSCI', new.nazwadruk, new.opis);
end^
SET TERM ; ^
