--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWJEDN_BD0 FOR TOWJEDN                        
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable cnt integer;
begin
  if(old.glowna =1 ) then begin
    select count(*) from TOWARY where KTM = old.ktm into :cnt;
    if(:cnt > 0) then exception TOWJEDN_DELMAINFORBIDEN;
  end
end^
SET TERM ; ^
