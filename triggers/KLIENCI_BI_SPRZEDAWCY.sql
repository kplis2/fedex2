--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_BI_SPRZEDAWCY FOR KLIENCI                        
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable spref integer;
declare variable spnazwa varchar(255);
begin
  if(new.sprzedawcazew is not null)
  then begin
    select sprzedawcy.sprzedawcawewn from sprzedawcy where sprzedawcy.ref = new.sprzedawcazew into :spref;
    select sprzedawcy.nazwa from sprzedawcy where sprzedawcy.ref = :spref into :spnazwa;
    if (new.sprzedawca <> :spref) then
      exception universal 'Sprzedawca wew. powinien byc: '||:spnazwa;
  end
end^
SET TERM ; ^
