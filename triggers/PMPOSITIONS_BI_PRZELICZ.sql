--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PMPOSITIONS_BI_PRZELICZ FOR PMPOSITIONS                    
  ACTIVE BEFORE INSERT POSITION 1 
AS
begin
-- pozycja planu operacyjnego - dane kosztorysowe
   if (new.calcval > 0 and new.quantity > 0) then
     new.calcprice = new.calcval / new.quantity;
   else
     new.calcval = new.quantity * new.calcprice;

-- pozycja planu operacyjnego - dane budżetowe
   if (new.budval > 0 and new.quantity > 0) then
     new.budprice = new.budval / new.quantity;
   else
     new.budval = new.quantity * new.budprice;

-- wartość urealniona
  if(new.quantity <> 0) then
    new.budvalr = (1 - (new.realq/new.quantity)) * new.budval + new.realval;

-- pozycja planu operacyjnego - dane wykonawcze
  if (new.usedq>0) then
    new.usedprice = new.usedval / new.usedq;
  else
    new.usedprice = 0;

-- kolor
   if(new.usedval>new.budval) then new.kolor = 1; else new.kolor = 0;
end^
SET TERM ; ^
