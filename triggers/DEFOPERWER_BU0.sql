--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFOPERWER_BU0 FOR DEFOPERWER                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.TYP='ZAM' or new.przed = 0) then begin
    if (new.WYNIK =1) then  exception DEFOPERWER_TYP_ZAM;
    new.wynik = 0;
  end
  if(new.regula < 12 and (new.pole is null or new.pole = '')) then
    exception DEFOPERWER_TYP_ZAM 'Wskazanie pola dla reguły jest wymagane.';
end^
SET TERM ; ^
