--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTOCK_BIU0 FOR MWSSTOCK                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable GOODSAV smallint;
begin
  if (new.quantity is null) then new.quantity = 0;
  if (new.blocked is null) then new.blocked = 0;
  if (new.sellav is null) then new.sellav = 1;
  if (new.ordered is null) then new.ordered = 0;
  if (new.suspended is null) then new.suspended = 0;
  if (new.reserved is null) then new.reserved = 0;
  if (new.ordsuspended is null) then new.ordsuspended = 0;
  if (new.stockplan is null) then new.stockplan = 0;
  if (new.marketable is null) then new.marketable = 0;
  if (new.loadpercent is null) then new.loadpercent = 0;
  if (new.finishmwsconstloc is null) then new.finishmwsconstloc = 0;
  if (new.x_blocked is null) then new.x_blocked = 0; --XXX Ldz ZG 126909
  if (new.branch is null and new.wh is not null) then
    select oddzial
      from defmagaz where symbol = new.wh
      into new.branch;
  if (new.actloc is null and new.mwsconstloc is not null) then
    select act from mwsconstlocs where ref = new.mwsconstloc into new.actloc;
  else if (new.actloc is null) then
    new.actloc = 1;
  if (new.ispal is null) then
    select paleta from towary where ktm = new.good into new.ispal;
  if (new.ispal is null) then
    new.ispal = 0;
  if (new.mwsstandlevelnumber is null) then
    select mwsstandlevelnumber from mwsconstlocs where ref = new.mwsconstloc
      into new.mwsstandlevelnumber;
  if (new.deep is null and new.mwsconstloc is not null) then
    select deep from mwsconstlocs where ref = new.mwsconstloc into new.deep;
  if ((new.wharea is null and new.mwsconstloc is not null)
      or (new.sellav is null and new.mwsconstloc is not null)) then
    select wharea, goodssellav from mwsconstlocs where ref = new.mwsconstloc
      into new.wharea, new.sellav;
  if (new.act is null or new.marketable is null or new.versnumber is null
      or new.goodtype is null or new.goodgroup is null or
      new.goodname is null or new.goodname = ''
  ) then
    select akt, chodliwy, nrwersji, grupa,
        nazwat, usluga
      from wersje
      where ref = new.vers
      into new.act, new.marketable, new.versnumber, new.goodgroup,
        new.goodname, new.goodtype;
  if (inserting) then  --Marcin F.
  begin
    select x.volume from xk_mws_przelicz_jednostki (new.good, new.vers, new.quantity - new.blocked + new.blocked, 0) x
      into new.volume;
  end
  else if ((new.quantity + new.blocked <> old.quantity + old.blocked or new.good <> old.good) and updating) then
  begin
    select x.volume from xk_mws_przelicz_jednostki (new.good, new.vers, new.quantity - new.blocked + new.blocked, 0) x
      into new.volume;
  end
  if (new.loadpercent is null) then new.loadpercent = 0;
end^
SET TERM ; ^
