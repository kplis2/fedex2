--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KLIENCI_AI0 FOR KLIENCI                        
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable slodef integer;
declare variable cnt integer;
declare variable cnt1 integer;
declare variable operator integer;
declare variable prawagrup varchar(80);
declare variable klihasodb varchar(255);
declare variable kontofk ANALYTIC_ID;
declare variable dl integer;
declare variable newkontofk ANALYTIC_ID;
declare variable parentpod integer;
begin
  parentpod = null;
  if(new.crm = 1 and new.token <> 7) then begin
    select ref from SLODEF where TYP='KLIENCI' into :slodef;
    if(:slodef is null) then exception SLODEF_BEZKLIENTOW;
    select count(*),max(OPEROPIEK) from CPODMIOTY where SLODEF=:slodef and SLOPOZ = new.ref into :cnt,:operator;
    if(:operator is null and new.sprzedawca is not null) then begin
      select count(*) from OPERATOR where SPRZEDAWCA=new.sprzedawca into :cnt1;
      if(:cnt1=1) then
        select ref from OPERATOR where SPRZEDAWCA=new.sprzedawca into :operator;
    end
    execute procedure GETCONFIG('DOMPRAWAGRUP') returning_values :prawagrup;
    select kontofk from klienci where ref = new.ref into :newkontofk;
    if (new.parent is not null and new.parent <> 0) then
    begin
      select ref from cpodmioty where slodef = 1 and slopoz = new.parent into :parentpod;
      if (parentpod is null or parentpod = 0) then
          exception universal 'Błąd synchronizacji. Klient nadrzędny nie ma odpowiednika w kartotece podmiotów.';
    end
    if(:cnt > 0) then
       update CPODMIOTY set AKTYWNY = new.aktywny, SKROT = substring(new.fskrot from 1 for 40), NAZWA = new.nazwa, --XXX ZG133796 MKD
                            TELEFON = new.telefon, FAX = new.fax, KOMORKA = new.komorka,
                            EMAIL = new.email, EMAILWINDYK = new.emailwindyk, WWW = new.www,
                            MIASTO = new.miasto, POCZTA = new.poczta,
                            KODP = new.kodp, KRAJ = new.kraj, ULICA = new.ulica, NRDOMU = new.nrdomu, NRLOKALU = new.nrlokalu,
                            KMIASTO = new.kmiasto, KPOCZTA = new.kpoczta,
                            KKODP = new.kkodp, KKRAJ = new.kkraj, KULICA = new.kulica, KNRDOMU = new.knrdomu, KNRLOKALU = new.knrlokalu,
                            KORESPOND = new.korespond, REGION = new.region,
                            NIP = new.nip, REGON = new.regon,
                            FIRMA = new.firma, IMIE = new.imie, NAZWISKO = new.nazwisko,
                            UWAGI = new.uwagi, NIEWYPLAC = new.NIEWYPLAC, TYP = new.typ, KODZEWN = new.kodzewn,
                            OPEROPIEK = :operator, PRAWAGRUP = :prawagrup, KONTOFK = :newkontofk,
                            DATAZAL = new.datazal, CPWOJ16M = new.cpwoj16m, COMPANY = new.company,
                            cpowiaty = new.cpowiat, parent = :parentpod
       where SLODEF=:slodef and SLOPOZ = new.ref ;
    else
      insert into CPODMIOTY(AKTYWNY, SKROT, NAZWA, TELEFON, FAX, KOMORKA,
                            EMAIL, EMAILWINDYK, WWW, NIP, REGON, SLODEF, SLOPOZ,
                            ULICA, NRDOMU, NRLOKALU, MIASTO, POCZTA, KODP, KRAJ, FIRMA, IMIE, NAZWISKO,
                            UWAGI, NIEWYPLAC, TYP, KODZEWN, OPEROPIEK, PRAWAGRUP, KONTOFK,
                            DATAZAL,CPWOJ16M, COMPANY, REGION, cpowiaty, parent)
         values(new.aktywny, substring(new.fskrot from 1 for 40), new.nazwa, new. telefon, new.fax, new.komorka,--XXX ZG133796 MKD
                new.email, new.emailwindyk, new.www, new.nip, new.regon, :slodef, new.ref,
                new.ulica, new.nrdomu, new.nrlokalu, new.miasto, new.poczta, new.kodp, new.kraj, new.firma, new.imie, new.nazwisko,
                new.uwagi, new.niewyplac, new.typ, new.kodzewn, :operator, :prawagrup, :newkontofk,
                new.datazal,new.cpwoj16m, new.company, new.region, new.cpowiat, :parentpod);
  end
  execute procedure GETCONFIG('KLIHASODB') returning_values :klihasodb;
  if(:klihasodb = '1') then begin
    insert into ODBIORCY(KLIENT, NAZWA, NAZWAFIRMY, DULICA, DNRDOMU, DNRLOKALU, DMIASTO, DKODP, DPOCZTA, GRUPAKUP,
        krajid, dtelefon, email, www)                           -- XXX KBI wdrożenie
      values (new.ref, new.FSKROT, new.dnazwa, new.dulica, new.dnrdomu, new.dnrlokalu, new.dmiasto, new.dkodp, new.dpoczta, new.grupakup,
          new.krajid, new.telefon, new.email, new.www);         -- XXX KBI wdrożenie
  end

end^
SET TERM ; ^
