--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_LISTYWYSDROZ_OPK_BI_SYMBSPED FOR LISTYWYSDROZ_OPK               
  ACTIVE BEFORE INSERT POSITION 100 
as
begin
  -- sluzy do ujednolicenia symbolu dokumentu spedycyjnego przewoxnika dla wszystkich opakowan
  if (new.listwysd is not null) then
    select sd.symbolsped from listywysd sd
      where sd.ref = new.listwysd
      into new.symbolsped;
end^
SET TERM ; ^
