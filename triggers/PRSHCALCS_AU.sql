--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHCALCS_AU FOR PRSHCALCS                      
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(new.status=2 and old.status<>2) then begin
    update PRSHCALCS set STATUS=1 where STATUS=2 and SHEET=new.sheet and REF<>new.ref;
  end
end^
SET TERM ; ^
