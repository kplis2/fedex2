--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROMOP_BU0 FOR PROMOP                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.ktm <> old.ktm and new.wersja=old.wersja) then begin
    select ref from WERSJE where ktm = new.ktm and nrwersji = 0 into new.wersja;
  end else if(new.wersja <> old.wersja) then begin
    select ktm from WERSJE where ref=new.wersja into new.ktm;
  end
  if(new.gratis is null) then new.gratis = 1;
  if(new.iloscza<>old.iloscza) then begin
    if(new.iloscza < 0 or new.iloscza is null) then exception universal 'Niepoprawna ilość!';
  end
  if(new.gratis<>old.gratis or coalesce(new.cennik,0)<>coalesce(old.cennik,0) or new.rabat<>old.rabat) then begin
    if (new.gratis=1 and new.cennik is not null) then exception universal 'Nie można wskazać cennika dla gratisu!';
    if (new.gratis=1 and new.rabat<>0) then exception universal 'Nie można podać rabatu dla gratisu!';
  end
end^
SET TERM ; ^
