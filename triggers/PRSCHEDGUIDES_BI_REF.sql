--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDES_BI_REF FOR PRSCHEDGUIDES                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('PRSCHEDGUIDES')
      returning_values new.REF;
end^
SET TERM ; ^
