--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCENNIKZAK_BI FOR DEFCENNIKZAK                   
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable mag varchar(10);
declare variable ref integer;
declare variable magazyny varchar(255);
begin
  if(new.magazyny is null) then new.magazyny = '';
  if(new.automat is null) then new.automat = 0;
  if(new.automat=0) then begin
    for select SYMBOL from DEFMAGAZ into :mag
    do begin
      mag = :mag||';';
      if(new.magazyny like :mag) then begin
        for select REF,MAGAZYNY from DEFCENNIKZAK
          where REF<>new.ref and KTM=new.ktm and MAGAZYNY like :mag
          into :ref,:magazyny
          do begin
            magazyny = replace(:magazyny, :mag, '');
            update DEFCENNIKZAK set MAGAZYNY=:magazyny, AUTOMAT=1
            where REF=:ref;
          end
      end
    end
  end
  new.automat = 0;
end^
SET TERM ; ^
