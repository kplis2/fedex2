--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTUREDISTS_BU_WPK FOR ACCSTRUCTUREDISTS              
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable current_company companies_id;
declare variable is_pattern smallint_id;
begin
  -- Sprawdzamy czy dowiązanie wyróżnika podlega synchronizacji
  if (new.pattern_ref is not null) then
  begin
    -- jeżeli podlega synchronizacji to należy sprawdzić czy jest to update
    -- wewntrzny czy nie tzn. czy jest to update wynikający z synchronizacji,
    -- czy nie. Jeżeli nie to powinna zadziaac ochrona w postaci pobrania prawidowych
    -- wartości ze wzorca.
    execute procedure get_global_param('CURRENTCOMPANY')
      returning_values :current_company;
    select c.pattern
      from companies c
      where c.ref = :current_company
      into :is_pattern;
    if (coalesce(is_pattern,0)=0) then
    begin
      if (coalesce(new.internalupdate,0)=0) then
      begin
        -- nie można upodatować tego rekordu wic synchronizujemy ze wzorcem.
        select number, distdef, distfilter, otable, ord
          from accstructuredists
          where ref = new.pattern_ref
          into new.number, new.distdef, new.distfilter, new.otable, new.ord;
      end else begin
        --jeżeli jednak jest to update wewntrzny to zakładam, że dane są poprawne
        new.internalupdate = 0;
      end
    end
  end
end^
SET TERM ; ^
