--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWS_KONFIG_AU0 FOR KONFIG                         
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable uplevel smallint;
declare variable deep smallint;
declare variable wh varchar(3);
declare variable uplevels varchar(100);
declare variable deeps varchar(100);
declare variable alldeeps varchar(100);
declare variable alldeep smallint;
begin
  if ((new.akronim = 'MWSTAKEFROMDEEP' or new.akronim = 'MWSTAKEFROMUPLEVEL')
    and (new.wartosc <> old.wartosc)
  ) then
  begin
    execute procedure get_config('MWSTAKEFROMDEEP',2) returning_values deeps;
    if (deeps is null or deeps = '') then
      deep = 0;
    else
      deep = cast(deeps as integer);
    execute procedure get_config('MWSTAKEFROMALLDEEPS',2) returning_values alldeeps;
    if (alldeeps is null or alldeeps = '') then
      alldeep = 0;
    else
      alldeep = cast(alldeeps as smallint);
    if (deeps = 0) then
      alldeep = 0;
    execute procedure get_config('MWSTAKEFROMUPLEVEL',2) returning_values uplevels;
    if (uplevels is null or uplevels = '') then
      uplevel = 1;
    else
      uplevel = cast(uplevels as integer);
    for
      select symbol from defmagaz where mws = 1
        into wh
    do begin
      execute procedure MWSCONSTLOCS_SET_GOODSAV(:wh,:deep,:alldeep,:uplevel);
      delete from mwsgoodsrefill where wh = :wh;
    end
  end
end^
SET TERM ; ^
