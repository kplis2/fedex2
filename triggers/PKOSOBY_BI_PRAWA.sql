--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PKOSOBY_BI_PRAWA FOR PKOSOBY                        
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
 select PRAWA,PRAWAGRUP,SKROT,AKTYWNY,SLODEF,OPEROPIEK,CSTATUS,TYP,COMPANY,ANCESTORS from CPODMIOTY where REF=new.CPODMIOT
 into new.prawa, new.prawagrup, new.cpodmskrot, new.cpodmaktywny, new.cpodmslodef, new.cpodmoperopiek, new.cpodmcstatus, new.cpodmtyp, new.company, new.cpodmancestors;
end^
SET TERM ; ^
