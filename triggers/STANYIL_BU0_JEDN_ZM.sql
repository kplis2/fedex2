--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANYIL_BU0_JEDN_ZM FOR STANYIL                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable przelicznik1 numeric(14,4);
declare variable przelicznik2 numeric(14,4);
begin
 if(new.ilosc<>old.ilosc) then
   begin
    select first 1 t.przelicz
      from towjedn t
      where t.ktm = new.ktm and t.dodatkowa = 1
    into : przelicznik1;
    if (przelicznik1 is not null) then
      new.iloscjm1 = new.ilosc / :przelicznik1;

    select first 1 t.przelicz
      from towjedn t
      where t.ktm = new.ktm and t.dodatkowa = 2
    into : przelicznik2;
    if (przelicznik2 is not null) then
      new.iloscjm2 = new.ilosc / :przelicznik2;
   end
end^
SET TERM ; ^
