--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRPOS_AIU_PVALUE FOR EPRPOS                         
  ACTIVE AFTER INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.ecolumn = 9000) then
    update eprempl set pvalue = new.pvalue where employee = new.employee and epayroll = new.payroll;
end^
SET TERM ; ^
