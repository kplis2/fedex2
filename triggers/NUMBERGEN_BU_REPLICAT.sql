--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NUMBERGEN_BU_REPLICAT FOR NUMBERGEN                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable waschange integer;
begin
  waschange = 0;
  if (new.NAZWA<>old.NAZWA
      or new.typ<>old.typ or (new.typ is null and old.typ is not null) or (new.typ is not null and old.typ is null)
      or new.rejestr<>old.rejestr or (new.rejestr is null and old.rejestr is not null) or (new.rejestr is not null and old.rejestr is null)
      or new.dokument<>old.dokument or (new.dokument is null and old.dokument is not null) or (new.dokument is not null and old.dokument is null)
      or new.magazyn<>old.magazyn or (new.magazyn is null and old.magazyn is not null) or (new.magazyn is not null and old.magazyn is null)
      or new.okres<>old.okres or (new.okres is null and old.okres is not null) or (new.okres is not null and old.okres is null)
      or new.chrono<>old.chrono or (new.chrono is null and old.chrono is not null) or (new.chrono is not null and old.chrono is null)
      or new.s_rejestr<>old.s_rejestr or (new.s_rejestr is null and old.s_rejestr is not null) or (new.s_rejestr is not null and old.s_rejestr is null)
      or new.s_dokument<>old.s_dokument or (new.s_dokument is null and old.s_dokument is not null) or (new.s_dokument is not null and old.s_dokument is null)
      or new.s_okres<>old.s_okres or (new.s_okres is null and old.s_okres is not null) or (new.s_okres is not null and old.s_okres is null)
      or new.s_oddzial<>old.s_oddzial or (new.s_oddzial is null and old.s_oddzial is not null) or (new.s_oddzial is not null and old.s_oddzial is null)
      or new.delimiter<>old.delimiter or (new.delimiter is null and old.delimiter is not null) or (new.delimiter is not null and old.delimiter is null)
      or new.braki<>old.braki or (new.braki is null and old.braki is not null) or (new.braki is not null and old.braki is null)
      or new.suffix<>old.suffix or (new.suffix is null and old.suffix is not null) or (new.suffix is not null and old.suffix is null)
      ) then
   waschange = 1;

  execute procedure rp_trigger_bu('NUMBERGEN',old.nazwa, null, null, null, null, old.token, old.state,
        new.nazwa, null, null, null, null, new.token, new.state, waschange)
    returning_values new.token, new.state;
end^
SET TERM ; ^
