--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MENUWAR_BI_REF FOR MENUWAR                        
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('MENUWAR')
      returning_values new.REF;
end^
SET TERM ; ^
