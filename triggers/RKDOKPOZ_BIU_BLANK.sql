--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKPOZ_BIU_BLANK FOR RKDOKPOZ                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable status smallint;
begin
  select pm from rkpozoper where ref = new.pozoper into new.pm;
  if (new.kurs is null or new.kurs = 0) then
    select kurs from rkdoknag where ref = new.dokument
      into new.kurs;
  if (new.anulowany is null) then new.anulowany = 0;

  -- kontrola homebankingowa
  select status from RKRAPKAS where REF = (select first 1 raport from rkdoknag where ref=new.dokument)
    into :status;
  if (status > 0
     and (old.pm<>new.pm or old.kwotazl<>new.kwotazl or old.kwota<>new.kwota or old.kurs<>new.kurs
          or old.rozrachunek<>new.rozrachunek or new.konto<>old.konto)
     ) then
    exception universal 'Wyciąg zamknięty. Edycja pozycji niemożliwa.';
end^
SET TERM ; ^
