--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_BU100_X_SERIE FOR MWSACTS                        
  ACTIVE BEFORE UPDATE POSITION 100 
AS
declare variable typ smallint_id;
declare variable ref integer_id;
begin
    -- nie pozwala zatwierdzic gdy nie jest podany numer seryjny a towar tego wymaga
  if(new.status = 5 and new.x_serial_no is null) then
  begin
    select t.x_mws_serie from towary t where t.ktm = new.good into :typ;
    select xs.ref from x_mws_serie xs where xs.mwsordin =new.ref or xs.mwsordout = new.ref into :ref;
    if (coalesce(:typ, 0)<>0 and coalesce(:typ, 0) = 0) then
    begin
        exception universal 'Nie podano numeru seryjnego dla towaru: '||new.good || ' pozycja zamowienia: '||new.ref;
    end
  end
end^
SET TERM ; ^
