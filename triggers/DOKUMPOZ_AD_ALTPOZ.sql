--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_AD_ALTPOZ FOR DOKUMPOZ                       
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  if (coalesce(old.fake,0) > 0) then
  begin
    if (not exists(select first 1 1 from dokumpoz where alttopoz = old.alttopoz)) then
      update dokumpoz
        set havefake = 0
      where ref = old.alttopoz;
  end
  if (coalesce(old.havefake,0) > 0) then
  begin
    delete from dokumpoz
      where dokument = old.dokument
        and alttopoz = old.ref
        and coalesce(fake,0) > 0;
  end
end^
SET TERM ; ^
