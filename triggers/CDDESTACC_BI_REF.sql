--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CDDESTACC_BI_REF FOR CDDESTACC                      
  ACTIVE BEFORE INSERT POSITION 0 
AS
BEGIN
  IF (NEW.REF IS NULL) THEN
    execute procedure gen_ref('CDDESTACC') returning_values NEW.REF;
END^
SET TERM ; ^
