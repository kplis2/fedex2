--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCALDAYS_BU_WORKTIME FOR EMPLCALDAYS                    
  ACTIVE BEFORE UPDATE POSITION 4 
AS
begin
  --DS: przeliczenie sekund na godziny
  if (new.worktime <> old.worktime or new.overtime_type <> old.overtime_type
    or coalesce(new.overtime0,0) <> coalesce(old.overtime0,0)
    or coalesce(new.overtime50,0) <> coalesce(old.overtime50,0)
    or coalesce(new.overtime100,0) <> coalesce(old.overtime100,0)
    or coalesce(new.nighthours,0) <> coalesce(old.nighthours,0)
    or coalesce(new.compensatory_time,0) <> coalesce(old.compensatory_time,0)
    or coalesce(new.compensated_time,0) <> coalesce(old.compensated_time,0)
  ) then begin
    execute procedure durationstr2(new.overtime0) returning_values new.overtime0str;
    execute procedure durationstr2(new.overtime50) returning_values new.overtime50str;
    execute procedure durationstr2(new.overtime100) returning_values new.overtime100str;
    execute procedure durationstr2(new.nighthours) returning_values new.nighthoursstr;
    execute procedure durationstr2(new.compensatory_time) returning_values new.compensatory_timestr;
    execute procedure durationstr2(new.compensated_time) returning_values new.compensated_timestr;
  end
end^
SET TERM ; ^
