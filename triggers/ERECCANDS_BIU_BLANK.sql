--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ERECCANDS_BIU_BLANK FOR ERECCANDS                      
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (inserting) then
    select first 1 ref
      from erecsource where new.referrer starting with http and http <> ''
      into new.source;
  if (inserting or new.status <> old.status) then
    select todo, waiting from erecstatus where ref = new.status
      into new.todo, new.waiting;
end^
SET TERM ; ^
