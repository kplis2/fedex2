--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCENNIK_BI0 FOR DEFCENNIK                      
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable walpln varchar(5);
begin
   if (coalesce(new.symbol, '') in ('A','B') and
       exists(select first 1 1 from defcennik where
                coalesce(symbol, '') = coalesce(new.symbol, '') and
                coalesce(oddzial, '') = coalesce(new.oddzial, '') and
                ref <> new.ref and AKT = new.akt)) then exception cenniki_symbol;

   if (new.rabatstalacena is null) then new.rabatstalacena = 0;
   if(new.akt is null) then new.akt = 0;
   if(NEW.datado is not null and new.datado is not null) then
     if(new.datado < new.dataod) then
       exception DEFCENNIK_DATAODDO;
   execute procedure GETCONFIG('WALPLN') returning_values :walpln;
   if(:walpln is null) then new.waluta = 'PLN';
   if(new.waluta is null) then begin
     new.waluta = :walpln;
   end
   if(new.waluta = :walpln or (new.kurs is null)) then new.kurs = 1;
   if(new.marza is null) then new.marza = 0;
   if(new.oddzial='') then new.oddzial = NULL;
   if(new.kolejnosc is null or new.kolejnosc = 0) then new.kolejnosc = 1;
   execute procedure REPLICAT_STATE('DEFCENNIK',new.state, new.ref, new.akt, '0', NULL) returning_values new.state;
end^
SET TERM ; ^
