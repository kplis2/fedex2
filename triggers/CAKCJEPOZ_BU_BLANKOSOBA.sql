--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CAKCJEPOZ_BU_BLANKOSOBA FOR CAKCJEPOZ                      
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if(old.cpodmiot <> new.cpodmiot) then
  begin
    if(not exists(select first 1 1 from pkosoby where ref = new.pkosoba and cpodmiot = new.cpodmiot)) then
    begin
      new.pkosoba = null;
    end
  end
end^
SET TERM ; ^
