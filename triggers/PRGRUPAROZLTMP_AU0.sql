--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRGRUPAROZLTMP_AU0 FOR PRGRUPAROZLTMP                 
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.status = 1 and old.status = 0) then
  begin
    update prschedguidedets t set t.prgruparozl = new.prgruparozl
      where t.ref = new.ref;
  end
end^
SET TERM ; ^
