--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INTRASTATP_AU FOR INTRASTATP                     
  ACTIVE AFTER UPDATE POSITION 0 
as
begin

  /* aktualizacja kraju wysylki */
  if(coalesce(new.krajwysylki,'')<>coalesce(old.krajwysylki,'')) then begin
    if (new.otable = 'NAGFAK') then
      update nagfak set krajwysylki = new.krajwysylki where ref = new.oref;
    else
      update dokumnag set krajwysylki = new.krajwysylki where ref = new.oref;
  end

  /* aktualizacja rodzaju transkacji */
  if(coalesce(new.intrdostawy,'')<>coalesce(old.intrdostawy,'')) then begin
    if (new.otable = 'NAGFAK') then
      update pozfak set intrtransakcja = new.intrdostawy where ref = new.sref;
    else
      update dokumpoz set intrtransakcja = new.intrdostawy where ref = new.sref;
  end

  /* aktualizacja kraju pochodzenia */
  if(coalesce(new.krajpochodzenia,'')<>coalesce(old.krajpochodzenia,'')) then begin
    if(new.otable='NAGFAK') then
      update pozfak set krajpochodzenia = new.krajpochodzenia where ref = new.sref;
    else
      update dokumpoz set krajpochodzenia = new.krajpochodzenia where ref = new.sref;
  end

end^
SET TERM ; ^
