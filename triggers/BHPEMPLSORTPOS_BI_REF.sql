--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BHPEMPLSORTPOS_BI_REF FOR BHPEMPLSORTPOS                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('BHPEMPLSORTPOS')
      returning_values new.REF;
end^
SET TERM ; ^
