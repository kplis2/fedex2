--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TURNOVERS_BD0 FOR TURNOVERS                      
  ACTIVE BEFORE DELETE POSITION 0 
as
begin
  if(exists (
    select first 1 1
      from decrees d
      where d.account = old.account and d.company = old.company
        and cast(substring(d.period from 1 for 4) as integer) = old.yearid
        and d.status > 0))
  then
    exception FK_CANT_DELETE_TURNOVERSD;
end^
SET TERM ; ^
