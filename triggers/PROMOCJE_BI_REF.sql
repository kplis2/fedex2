--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROMOCJE_BI_REF FOR PROMOCJE                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('PROMOCJE')
      returning_values new.REF;
end^
SET TERM ; ^
