--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER S_PATCHES_BI_REF FOR S_PATCHES                      
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_s_patches,1);
end^
SET TERM ; ^
