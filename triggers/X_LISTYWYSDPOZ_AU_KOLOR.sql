--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_LISTYWYSDPOZ_AU_KOLOR FOR LISTYWYSDPOZ                   
  ACTIVE AFTER UPDATE POSITION 20 
AS
begin
  --[PM] XXX kolor w gridzie do pakowania
  if (coalesce(old.iloscspk,0) <> coalesce(new.iloscspk,0)) then
    begin
      if (coalesce(new.ilosc,0) = coalesce(new.iloscspk,0)) then
        begin
          if (not exists(select first 1 1 from listywysdpoz lp
              where lp.dokument = new.dokument
              and coalesce(lp.ilosc,0) <> coalesce(lp.iloscspk,0))
            ) then
            begin
              --new.x_kolor = 1;
              update listywysdpoz set x_kolor = 1 where dokument = new.dokument
                and x_kolor <> 1 and ref <> new.ref;
            end
          else
            begin
              --new.x_kolor = 2;
              update listywysdpoz set x_kolor = 0 where dokument = new.dokument
                and x_kolor <> 0 and coalesce(ilosc,0) <> coalesce(iloscspk,0)
                and ref <> new.ref;
              update listywysdpoz set x_kolor = 1 where dokument = new.dokument
                and x_kolor <> 1 and coalesce(ilosc,0) = coalesce(iloscspk,0)
                and ref <> new.ref;
            end
        end
      else
        begin
          --new.x_kolor = 2;
          update listywysdpoz set x_kolor = 0 where dokument = new.dokument
            and x_kolor <> 0 and coalesce(ilosc,0) <> coalesce(iloscspk,0)
            and ref <> new.ref;
          update listywysdpoz set x_kolor = 1 where dokument = new.dokument
            and x_kolor <> 1 and coalesce(ilosc,0) = coalesce(iloscspk,0)
            and ref <> new.ref;
        end
    end
end^
SET TERM ; ^
