--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER HISTZAM_AU0 FOR HISTZAM                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if(new.dokumentmain <> old.dokumentmain) then
    update dokumnag set skad = new.skad, dokumentmain = new.dokumentmain
      where historia = new.ref;
end^
SET TERM ; ^
