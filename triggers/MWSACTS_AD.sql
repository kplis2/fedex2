--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_AD FOR MWSACTS                        
  ACTIVE AFTER DELETE POSITION 9 
as
begin
  if (old.mwsordtypedest = 2 and old.frommwsact is not null) then
  begin
    update mwsacts set quantity = quantity + old.quantity
      where ref = old.frommwsact
        and status = 0;
  end
end^
SET TERM ; ^
