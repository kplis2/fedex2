--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMSER_BD_ORDER FOR DOKUMSER                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  if (old.ord = 0) then exit;
  update dokumser set ord = 0, numer = numer - 1
    where ref <> old.ref and numer > old.numer and refpoz = old.refpoz;
end^
SET TERM ; ^
