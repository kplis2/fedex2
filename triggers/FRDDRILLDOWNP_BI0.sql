--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDDRILLDOWNP_BI0 FOR FRDDRILLDOWNP                  
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('FRDDRILLDOWNP')
      returning_values new.REF;
end^
SET TERM ; ^
