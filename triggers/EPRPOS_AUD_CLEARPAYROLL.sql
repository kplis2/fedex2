--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRPOS_AUD_CLEARPAYROLL FOR EPRPOS                         
  ACTIVE AFTER UPDATE OR DELETE POSITION 0 
AS
  declare variable ctype varchar(3);
begin
  --DS: czyszczenie przypisanych list plac w momencie usuniecia danego skladnika

  if (coalesce(new.ecolumn,0) = old.ecolumn) then exit;

  --usuniecie kary porzadkowej
  if (old.ecolumn = 7950) then
    update empladdfiles a
      set a.epayroll = null
      where a.employee = old.employee
        and a.epayroll = old.payroll;

  --usuniecie swiadczenia socjalnego
  select coltype
    from ecolumns c
    where c.number = old.ecolumn
    into :ctype;

  if(ctype = 'SOC') then
    update esocialfunds a
      set a.epayroll = null
      where a.employee = old.employee
        and a.epayroll = old.payroll
        and a.fundtype = old.ecolumn;
end^
SET TERM ; ^
