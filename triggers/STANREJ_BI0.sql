--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER STANREJ_BI0 FOR STANREJ                        
  ACTIVE BEFORE INSERT POSITION 0 
as
declare variable zak smallint;
begin
  if(new.stansprzed is null) then new.stanfaktyczny = '';
  else new.stanfaktyczny = new.stansprzed;
  select ZAKUP from REJFAK where SYMBOL = new.REJFAK into :zak;
  select ZAKUPU from STANSPRZED where  STANOWISKO = new.stansprzed into new.zakupu;
  if(:zak <> new.zakupu) then
    exception STANTYPDAFAK_ZAKNIESG;

end^
SET TERM ; ^
