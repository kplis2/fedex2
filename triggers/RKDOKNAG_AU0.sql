--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_AU0 FOR RKDOKNAG                       
  ACTIVE AFTER UPDATE POSITION 0 
as
--  declare variable operacja varchar(20);
  declare variable faktura integer;
  declare variable rozr varchar(20);
  declare variable rkrapkasnr integer;
  declare variable stanowisko char(2);
  declare variable lokres char(6);
  declare variable getnumdok varchar(40);
  declare variable lp smallint;
  declare variable rkdokpoz_ref integer;
  declare variable zestanu integer;
  declare variable kwota numeric(14,2);
  declare variable BLOCKREF integer;
  declare variable local integer;
  declare variable poperacja integer;
  declare variable prozrachunek varchar(20);
  declare variable typ integer;
  declare variable COUNTRY_CURRENCY varchar(255);

begin
  execute procedure CHECK_LOCAL('RKDOKNAG',old.ref) returning_values :local;

  /* BS34032 fragment przeniesiony do RKDOKNAG_AU_OBLRKRAP */


  if (((new.anulowany = 1 and old.anulowany = 0)
       or (new.numer = 0 and old.numer > 0)
       or (new.numer is null and old.numer > 0)
      )
     and :local = 1) then
  begin
    /* wyfofanie pozycji rozrachunkĂłw i samych rozrachunkĂłw zaĹ‚oĹĽonych przez dokument */
    select RKRAPKAS.STANOWISKO, NUMER, RKRAPKAS.OKRES
      from RKRAPKAS where REF= old.raport
      into :stanowisko, :rkrapkasnr,:lokres;

    for
      select ROZRACHUNEK, FAKTURA, LP
        from RKDOKPOZ
        where DOKUMENT=new.ref
        into :rozr, :faktura, :lp
    do begin
      if (rozr is not null) then
      begin
        for select ref from rkdokpoz where dokument=new.ref
          into :rkdokpoz_ref
        do
          delete from rozrachp where stable='RKDOKPOZ' and sref= :rkdokpoz_ref;
      end

      rozr = null;
    end
    if (old.btransfer > 0) then
      update BTRANSFERS set STATUS = 3
      where REF = old.btransfer and STATUS <> 3;
    execute procedure RKRAP_OBL_WAR(old.raport);
  end

  if ((new.kurs <> old.kurs) or (new.kurs is not null and old.kurs is null)) then
  begin
    update RKDOKPOZ set kurs = new.kurs where dokument = new.ref;
  end

  if ((old.numer is null or old.numer = 0) and  new.numer > 0) then
  begin
    for select P.pozoper, P.rozrachunek
      from rkdokpoz P
      where P.dokument = new.ref
      into :poperacja, :prozrachunek
    do begin
      select PO.typ
        from rkpozoper PO
        where po.ref = :poperacja
      into :typ;
      if (typ > 0 and (prozrachunek is null or prozrachunek = '')) then
        exception RKDOKNAG_EXPT;
    end
  end
  execute procedure get_config('COUNTRY_CURRENCY',0) returning_values :country_currency;
  if (coalesce(:country_currency,'') = '') then
    COUNTRY_CURRENCY = 'PLN';
  if (coalesce(old.kurs,0) <> coalesce(new.kurs,0)
    and coalesce(new.kurs,0) = 0
    and (exists (select first 1 1 from rkdefoper r
      where r.symbol = new.symbol
      and r.redkurs = 1) or new.pm = '+')
    and new.waluta <> :country_currency
  ) then
    exception RKDOKNAG_EXPT 'Operacja wymaga podania kursu';
end^
SET TERM ; ^
