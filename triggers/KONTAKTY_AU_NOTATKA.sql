--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONTAKTY_AU_NOTATKA FOR KONTAKTY                       
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable notref integer;
declare variable konnazwa varchar(40);
begin
  if(not ((new.notatka is not NULL and old.notatka is NULL) or (new.notatka is NULL and old.notatka is not NULL))) then begin
    if(new.tworznotatke=1 and new.notatka is null) then begin
      notref = gen_id(GEN_CNOTATKI,1);
      select NAZWA from CTYPYKON where REF=new.rodzaj into :konnazwa;
      insert into CNOTATKI(REF,NAZWA,DATA,TRESC,CPODMIOT,PKOSOBA,KONTAKT,OPERATOR)
      values (:notref,:konnazwa,new.data,new.opis,new.cpodmiot,new.pkosoba,new.ref,new.operator);
      update KONTAKTY set notatka = :notref where REF=new.ref;
    end else if(new.tworznotatke=1 and new.notatka is not null) then begin
      select NAZWA from CTYPYKON where REF=new.rodzaj into :konnazwa;
      update CNOTATKI set NAZWA = :konnazwa, DATA = new.data, TRESC = new.opis,
                          CPODMIOT = new.cpodmiot, PKOSOBA = new.pkosoba, OPERATOR = new.operator
      where REF = new.notatka;
    end else if(new.tworznotatke=0 and new.notatka is not null) then begin
      delete from CNOTATKI where ref = new.notatka;
    end
  end
end^
SET TERM ; ^
