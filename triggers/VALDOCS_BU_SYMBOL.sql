--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VALDOCS_BU_SYMBOL FOR VALDOCS                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
  declare variable numer integer;
begin
  if (extract(year from new.docdate) <> extract(year from old.docdate)) then
  begin
    execute procedure Get_NUMBER('Dok. wartościowe dla środków trwałych', null, null, new.docdate, 0, null) returning_values :numer, new.symbol;
    new.symbol = new.doctype||'/'||new.symbol;
  end
end^
SET TERM ; ^
