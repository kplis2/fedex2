--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRVHDRS_AI_CALCULATEREPORT FOR FRVHDRS                        
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  if (new.frvtype = 1) then execute procedure FR_CHECK_REPORT(new.ref);
    else execute procedure FR_CALCULATE_REPORT(new.ref);
end^
SET TERM ; ^
