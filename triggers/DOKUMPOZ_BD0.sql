--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMPOZ_BD0 FOR DOKUMPOZ                       
  ACTIVE BEFORE DELETE POSITION 0 
as
  declare variable ack integer;
  declare variable ii numeric(15,5);
  declare variable usluga integer;
  declare variable intrastatst smallint;
begin
  select AKCEPT from DOKUMNAG where ref=old.dokument into :ack;
  select USLUGA from TOWARY where KTM=old.ktm into :usluga;

  /* weryfikacja czy pozycja nie znajduje sie na nieotwartej deklaracji Intrastat */
  intrastatst = null;
  select coalesce(intrastath.statusflag,0)
    from intrastatp left join intrastath
      on intrastatp.intrastath = intrastath.ref
      where intrastatp.otable = 'DOKUMPOZ'
        and intrastatp.oref = old.dokument
        and intrastatp.sref = old.ref
    into :intrastatst;
  if (:intrastatst is not null and :intrastatst <> 0) then
    exception INTRASTAT_STATUS_CHECK;

  if(:ack = 1 or :ack = 8) then begin
    -- XXX KBI
    if (rdb$get_context('USER_TRANSACTION', 'INT_IMP_ZAM_NAGLOWKI_DEL') <> 1) then
      exception DOKUMNAG_AKCEPT;
       -- XXX KBI
    --    ii = - old.ilosc;
    --    execute procedure DOKUMPOZ_CHANGE(old.ref,old.ktm,old.wersja,old.cena,old.dostawa,:ii,1,0);
  end else if(:ack = 9) then begin
    if(:usluga<>1) then begin
      ii = - old.bilosc;
      if(:ii <> 0) then begin
        update DOKUMROZ set ACK = 9 where DOKUMROZ.pozycja = old.ref and DOKUMROZ.ack = 1;
        execute procedure DOKUMPOZ_CHANGE(old.ref,old.bktm,old.bwersja,old.bcena,old.bdostawa,:ii,1,0);
        update DOKUMROZ set ACK = 1 where DOKUMROZ.pozycja = old.ref and DOKUMROZ.ack = 9;
      end
    end
  end
end^
SET TERM ; ^
