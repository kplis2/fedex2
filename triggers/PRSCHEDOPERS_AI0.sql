--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDOPERS_AI0 FOR PRSCHEDOPERS                   
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable worktime double precision;
declare variable worktimestr varchar(20) ;
declare variable oper varchar(20);
begin
  if(exists (select first 1 1 from PRSHOPERS where ref =new.shoper and coalesce(eopertime,'') <> '')) then begin
      execute procedure prschedopers_calc_worktime(new.ref)
        returning_values :worktime, :oper;
    execute procedure timediff2str(:worktime) returning_values :worktimestr;
    update prschedopers o set o.worktime = :worktime, o.wortimestr = :worktimestr where o.ref = new.ref;
  end
end^
SET TERM ; ^
