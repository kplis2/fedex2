--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDGUIDESBLOCKS_BI_BLOCK FOR PRSCHEDGUIDESBLOCKS            
  ACTIVE BEFORE INSERT POSITION 0 
as
 declare variable blocked smallint;
 declare variable isblocked smallint;
begin
  if(new.blocked is null) then new.blocked =0;
  select blocked from prschedguides pg where pg.ref = new.guide into :isblocked;
  if(isblocked is null) then isblocked = 0;
  if((new.descript = '' or new.descript is null) and (new.descriptret = '' or new.descriptret is null)) then
    exception PRSCHEDGUIDESBLOCK_NODESCRIPT;
  if(isblocked > 0 and new.descript <> '') then
    exception PRSCHEDGUIDESBLOCK_ISBLOCK;
  if(isblocked = 0 and new.descriptret <> '') then
    exception PRSCHEDGUIDESBLOCK_ISNOTBLOCK;
  select blocked from prschedguides pg where pg.ref = new.guide into :blocked;
  if(new.blocked <> blocked and (new.descript = '' or new.descript is null)) then
    new.descript = 'Zmiana statusu blokady';
  update prschedguides pg set pg.blocked = new.blocked where pg.ref = new.guide;
  new.rejdate = current_date;
end^
SET TERM ; ^
