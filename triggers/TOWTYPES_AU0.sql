--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER TOWTYPES_AU0 FOR TOWTYPES                       
  ACTIVE AFTER UPDATE POSITION 0 
as
begin
  if(coalesce(new.rights,'')<>coalesce(old.rights,'') or coalesce(new.rightsgroup,'')<>coalesce(old.rightsgroup,'')) then begin
    update towary set prawa=new.rights, prawagrup=new.rightsgroup where usluga = new.numer;
    update wersje set prawa=new.rights, prawagrup=new.rightsgroup where usluga = new.numer;
  end
end^
SET TERM ; ^
