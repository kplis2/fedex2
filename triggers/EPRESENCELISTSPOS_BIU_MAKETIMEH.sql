--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPRESENCELISTSPOS_BIU_MAKETIMEH FOR EPRESENCELISTSPOS              
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  -- jeli pola OD - DO sa wypelnione, a nie jest wypelnione pole MAKETIMEHOURS
  -- jest ono autonatycznie wypelniane
  if(new.TIMESTART is not null and new.TIMESTOP is not null and (new.MAKETIMEHOURS is null or new.MAKETIMEHOURS=0)) then begin
    new.MAKETIMEHOURS = new.TIMESTOP - new.TIMESTART;
  end
end^
SET TERM ; ^
