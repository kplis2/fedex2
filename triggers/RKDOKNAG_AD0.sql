--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKDOKNAG_AD0 FOR RKDOKNAG                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable token smallint;
begin
  select token
    from rkrapkas
  where ref = old.raport
  into :token;
 if(:token is null) then token = 0;
 if(:token <> 7) then
   execute procedure RKRAP_OBL_WAR(old.raport);
 update NAGFAK set RKDOKNAG = NULL where RKDOKNAG = old.ref;
 update RKDOKNAG set LP = LP - 1 where RAPORT = old.RAPORT and LP > old.lp;
 if(old.btransfer > 0) then
   update BTRANSFERS set STATUS = 2 where STATUS > 2 and REF=old.btransfer;
end^
SET TERM ; ^
