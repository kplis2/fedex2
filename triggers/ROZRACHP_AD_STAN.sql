--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ROZRACHP_AD_STAN FOR ROZRACHP                       
  ACTIVE AFTER DELETE POSITION 0 
AS
begin
  update rozrachstan
    set ma = ma - old.ma, winien = winien - old.winien,
    mazl = mazl - old.mazl, winienzl = winienzl - old.winienzl
    where slodef = old.slodef and slopoz = old.slopoz and company = old.company
    and kontofk = old.kontofk and waluta = old.waluta;
end^
SET TERM ; ^
