--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DECREES_BD_ORDER FOR DECREES                        
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then exit;
  update decrees set ord = 0, number = number - 1
    where ref <> old.ref and number > old.number and bkdoc = old.bkdoc;
end^
SET TERM ; ^
