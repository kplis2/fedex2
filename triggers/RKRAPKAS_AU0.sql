--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER RKRAPKAS_AU0 FOR RKRAPKAS                       
  ACTIVE AFTER UPDATE POSITION 0 
as
  declare variable raport integer;
  declare variable rapdat timestamp;
  declare variable rapnum integer;
  declare variable rapref integer;
  declare variable btrans integer;
begin
  if(new.skwota <> old.skwota or (new.skwotazl <> old.skwotazl))then begin
    /* okreslenie kolejnego raportu i przeniesienie salda */
    raport = 0;
    for select REF, DATAOD, numer from RKRAPKAS where
       STANOWISKO = new.stanowisko and DATAOD >= new.dataod
       order by DATAOD, NUMER
       into :rapref, :rapdat, :rapnum
    do begin
      if((:raport = 0) and ((:rapdat  > new.dataod ) or (:rapnum > new.numer)))then
          raport = :rapref;
    end
    if(:raport > 0) then
      update RKRAPKAS set KWOTABO = new.skwota, KWOTABOZL = new.skwotazl where REF=:raport;
      execute procedure rkrap_obl_war(:raport);
  end
  if(new.status = 2 and old.status < 2) then begin
    for select rkdoknag.btransfer from RKDOKNAG where RKDOKNAG.raport = new.ref and BTRANSFER > 0
    into :btrans
    do begin
      update BTRANSFERS set STATUS = 4 where BTRANSFERS.ref = :btrans;
    end
  end
  else if (new.status < 2 and old.status = 2) then
  begin
    for select rkdoknag.btransfer from RKDOKNAG where RKDOKNAG.raport = new.ref and BTRANSFER > 0
    into :btrans
    do begin
      update BTRANSFERS set STATUS = 3 where BTRANSFERS.ref = :btrans;
    end
  end
end^
SET TERM ; ^
