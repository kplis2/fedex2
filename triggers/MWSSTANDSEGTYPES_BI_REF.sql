--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTANDSEGTYPES_BI_REF FOR MWSSTANDSEGTYPES               
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSSTANDSEGTYPES') returning_values new.ref;
end^
SET TERM ; ^
