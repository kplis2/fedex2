--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSPALLPARTS_BI0 FOR MWSPALLPARTS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.ref is null or new.ref = 0) then
    execute procedure gen_ref('MWSPALLPARTS') returning_values new.ref;
end^
SET TERM ; ^
