--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHY_BI0 FOR DEFCECHY                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable nazwa varchar(80);
begin
  if(new.grupa is not null) then begin
    select nazwa from DEFCECHG where REF = new.grupa into :nazwa;
  end
  if(nazwa is not null) then new.nazwagrupy = :nazwa;
  if(coalesce(new.tabela,'') = '') then exception universal 'Nie wypełnione pole Tabela.';
  if(new.wersje is null) then new.wersje = 1;
  if (new.partyp = 3 and coalesce(new.parskrot,'') = '') then
    exception DEFCECHY_EXCEPTION 'Brak podanego skrótu tworzącego dostawę.';
end^
SET TERM ; ^
