--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER GRUPYKUPOFE_BU0 FOR GRUPYKUPOFE                    
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if((new.wersjaref <> old.wersjaref or (new.ktm is null) or (new.wersja is null)) and new.wersjaref is not null and new.wersjaref <> 0) then
  begin
    select nrwersji, ktm from WERSJE where ref=new.wersjaref into new.wersja, new.ktm;
  end else if((new.ktm <> old.ktm) or (new.wersja <> old.wersja) or (new.wersjaref <> old.wersjaref)or (new.wersjaref is null)) then
  begin
    if((new.ktm <> old.ktm) and (new.ktm is not null) and (new.wersjaref is not null) and (old.wersjaref = new.wersjaref)) then
      select first 1 ref, nrwersji from wersje where ktm = new.ktm and nrwersji = 0 into new.wersjaref, new.wersja;
    else
      select ref from WERSJE where ktm = new.ktm and nrwersji = new.wersja into new.wersjaref;
  end
  if(new.ktm <> old.ktm) then
    select NAZWA from TOWARY where ktm = new.ktm into new.nazwat;
  if(new.ktm is not null and (not exists (select first 1 1 from towjedn where ktm = new.ktm and ref = new.jedn))) then
    select ref from TOWJEDN where ktm = new.ktm and glowna = 1 into new.jedn;
end^
SET TERM ; ^
