--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFCECHW_AD0 FOR DEFCECHW                       
  ACTIVE AFTER DELETE POSITION 0 
as
declare variable automat smallint;
declare variable katalog integer;
declare variable numer integer;
declare variable id_ref integer;
declare variable i integer;
declare variable typ smallint;
declare variable wartosc varchar(255);
begin
   for select AUTOMAT, REF from DEFKATAL where CECHA = old.cecha into :automat, :katalog
   do begin
     if(:automat is null ) then automat = 0;
     if(:automat = 1) then begin
     numer = null;
     i = 1;
     /* okreslenie numeru wartosci */
     /*okreslenie typu wartosci */
     select  typ from DEFCECHY where symbol=old.cecha into :typ;
     if(:typ = 1 or (:typ = 0)) then begin
         for select id_defcechw from DEFCECHW  where CECHA = old.cecha order by WARTNUM into :id_ref do begin
           if(:id_ref = old.id_defcechw) then numer = i;
           i = i + 1;
         end
         wartosc = old.wartnum;
      end
      if(:typ = 2) then begin
         for select id_defcechw from DEFCECHW  where CECHA = old.cecha order by WARTSTR into :id_ref do begin
           if(:id_ref = old.id_defcechw) then numer = i;
           i = i + 1;
         end
         wartosc = old.wartstr;
      end
      if(:typ = 3 or (:typ = 4)) then begin
         for select id_defcechw from DEFCECHW  where CECHA = old.cecha order by WARTDATE into :id_ref do begin
           if(:id_ref = old.id_defcechw) then numer = i;
           i = i + 1;
         end
         wartosc = old.wartdate;
      end
      if(:numer is null)then numer = 1;
      /* numer wartosci okrelony */
      delete from DEFELEM where KATALOG = :katalog and WARTOSC = :wartosc;
    end
   end
end^
SET TERM ; ^
