--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRDVERSIONS_BI FOR PRDVERSIONS                    
  ACTIVE BEFORE INSERT POSITION 1 
as
declare variable period varchar(6);
begin
  if (exists(select ref from prdversions where fromperiod = new.fromperiod and ref<> new.ref
             and prdaccounting=new.prdaccounting)) then
    exception universal 'Wersja rozliczenia już istnieje dla danego okresu.';
end^
SET TERM ; ^
