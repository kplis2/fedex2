--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WHSECS_AU0 FOR WHSECS                         
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.symbol <> old.symbol) then
  begin
    update whsecrows set whsecdict = new.symbol where whsec = new.ref;
    update mwsstands set whsecdict = new.symbol where whsec = new.ref;
    update mwsstandsegs set whsecdict = new.symbol where whsec = new.ref;
    update whareas set whsecdict = new.symbol where whsec = new.ref;
    update mwsconstlocs set whsecdict = new.symbol where whsec = new.ref;
    update mwsstandlevels set whsecdict = new.symbol where whsec = new.ref;
    update mwsconstlocs set whsecdict = new.symbol where whsec = new.ref;
  end
end^
SET TERM ; ^
