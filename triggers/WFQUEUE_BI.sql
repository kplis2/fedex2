--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WFQUEUE_BI FOR WFQUEUE                        
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.ref is null) then
    new.ref = gen_id(gen_wfqueue_id,1);
  new.regdate = current_timestamp;
end^
SET TERM ; ^
