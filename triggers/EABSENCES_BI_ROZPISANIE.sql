--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EABSENCES_BI_ROZPISANIE FOR EABSENCES                      
  ACTIVE BEFORE INSERT POSITION 2 
AS
declare variable fday date;
declare variable lday date;
declare variable lastday date;
declare variable i integer;
declare variable RecalcWork integer;
declare variable dni integer;
declare variable year_of_birth integer;
declare variable current_year integer;
declare variable prog integer;
declare variable bcode integer;
declare variable isDivisionEabs smallint;
declare variable baseabsence integer;
declare variable firstabsence integer;
declare variable ref integer;
declare variable calcpaybase smallint;
declare variable regoperator integer;
declare variable npayroll integer;
declare variable sickwait timestamp;
begin
  if (new.ecolumn in (40,50,80,90,130,160,170)) then
  begin
   select todate from efunc_get_sickwait(new.employee, new.fromdate) into :sickwait;
    if(:sickwait is not null) then
    begin
      select Z.ref
        from ecolparams C
          join edictzuscodes Z on (Z.code = C.pval and Z.dicttype = 9)
        where C.ecolumn = 130 and C.param = 'BCODE'
        into :bcode;
      if(:sickwait < new.todate) then
      begin
        execute procedure get_global_param ('AKTUOPERATOR')
         returning_values :regoperator;
        insert into eabsences (ecolumn, employee, fromdate, todate,
            bcode, monthpaybase, daypayamount,
            fbase, lbase, epayroll, calcpaybase,
            regtimestamp, regoperator, chgtimestamp, chgoperator,firstabsence, baseabsence,
      certserial, certnumber )
          values (130, new.employee, new.fromdate, :sickwait,
            :bcode, null, null,
            null, null,  new.epayroll, null,
            current_timestamp(0), :regoperator, null,null,null,null, new.certserial, new.certnumber );
        new.fromdate = :sickwait + 1;
      end
      else if(:sickwait >= new.todate) then
      begin
        new.ecolumn = 130;
        new.bcode = :bcode;
      end
    end
  end
  if((:sickwait < new.fromdate or :sickwait is null) and new.ecolumn = 130) then
  begin
    new.ecolumn = 40;
    select Z.ref
      from ecolparams C
        join edictzuscodes Z on (Z.code = C.pval and Z.dicttype = 9)
      where C.ecolumn = new.ecolumn and C.param = 'BCODE'
      into new.bcode;
  end
  execute procedure MonthLastDay(new.fromdate) returning_values lday;
  new.days = new.todate - new.fromdate + 1;
  new.ayear = extract(year from new.fromdate);
  isDivisionEabs = 0;
  if (new.todate > lday) then
    IsDivisionEabs = 1;

  --od tego miejsca daty od-do na pewno obejmuja tylko jeden i ten sam miesiac:
  if (new.ecolumn in (40,50,60)) then
  begin
    prog = 33;

    if (new.fromdate >= '2009/2/1') then
    begin
      --sprawdza czy osoba nie ukonczyla w zeszlym roku 50 lat (PR19983)
      select extract (year from p.birthdate)
        from employees e
          join persons p on p.ref = e.person
        where e.ref = new.employee
        into :year_of_birth;

      current_year = extract (year from new.fromdate);
      if (current_year - 1 - year_of_birth >= 50) then
        prog = 14;
    end

    --sprawdza czy na przelomie roku osoba nie miala juz zasilku (BS20917)
    if (not exists(select first 1 1 from eabsences
          where ecolumn in (90,100,110,120) and correction in (0,2)     --BS36980
            and employee = new.employee and todate = new.fromdate - 1))
    then
      select sum(days) from eabsences
        where ecolumn in (40,50,60) and correction in (0,2)
          and employee = new.employee and ayear = new.ayear
        into i;
    else
      i = prog;

  --sprawdza ilosc dni chorobowego ze swiadectwa pracy
    select sum(ewr.daylimit)
      from eworkcertifs ew
        join ewrkcrtlimits ewr on ewr.workcertif = ew.ref
      where employee = new.employee
        and extract(year from todate) = new.ayear
        and ewr.ecolumn = 40
      into :dni;

    i = coalesce(i,0) + coalesce(dni,0);

    if (i >= prog) then
    begin
    --aktualizacja bcode
      new.ECOLUMN = new.ECOLUMN + 50;
      select Z.ref
        from ecolparams C
          join edictzuscodes Z on (Z.code = C.pval and Z.dicttype = 9)
        where C.ecolumn = new.ecolumn and C.param = 'BCODE'
        into new.bcode;
    end
    else if (i + new.days > prog) then
    begin

      select Z.ref
        from ecolparams C
          join edictzuscodes Z on (Z.code = C.pval and Z.dicttype = 9)
        where C.ecolumn = new.ecolumn + 50
          and C.param = 'BCODE'
        into :bcode;

      execute procedure GEN_REF('EABSENCES')
        returning_values ref;
      firstabsence = null; baseabsence = null;
      if (IsDivisionEabs = 0) then
      begin
      /*jezeli nie nastapil podzial nieobecnosci na miesiace (odbyla sie aktualizacja
        rekordu w ramach jednego miesiaca) trzeba ustawic tak ciaglosc aby nowo
        stworzona nieobecnosc ZUS nie byla poczatkiem dla choroby wynagrodzenie*/
        select baseabsence, firstabsence from E_GET_ABSENCE_PERIODS_BASES(new.employee, new.ecolumn, new.fromdate, null, null)
          into :baseabsence, :firstabsence;
        if (firstabsence = 0) then
        begin
          firstabsence = ref;
          baseabsence = ref;
        end
      end

    if ( firstabsence = ref ) then
      npayroll = new.epayroll;
    else
      npayroll = null;

      execute procedure get_global_param ('AKTUOPERATOR')
        returning_values :regoperator;
      --jezeli brak rozbicia na miesiace wylacz wymuszenie przeliczenia podstaw
      calcpaybase = new.calcpaybase;
      if (isDivisionEabs = 0 and calcpaybase = 1) then
        calcpaybase = 0;


      insert into eabsences (ref, ecolumn, employee, fromdate, todate,
          dokdate, scode, scodeb, bcode, monthpaybase, daypayamount,
          fbase, lbase, certserial, certnumber, epayroll, calcpaybase,
          regtimestamp, regoperator, chgtimestamp, chgoperator,firstabsence, baseabsence)
        values (:ref, new.ecolumn+50, new.employee, new.fromdate + :prog - :i, new.todate,
          new.dokdate, new.scode, new.scodeb, :bcode, new.monthpaybase, new.daypayamount,
          new.fbase, new.lbase, new.certserial, new.certnumber, :npayroll, :calcpaybase,
          current_timestamp(0), :regoperator, null,null,:firstabsence,:baseabsence);

      new.todate = new.FromDate + prog - i - 1;
      RecalcWork = 1;
    end
  end

  --new.days = new.todate - new.fromdate + 1;
  if (new.ecolumn = 360) then
  begin
    select sum(days) from eabsences
      where employee = new.employee and ayear = new.ayear
        and ecolumn = 360
        and correction in (0,2)
      into :i;
    i = coalesce(i,0);
    if (i >= 90) then
    begin
    --aktualizacja bcode
      new.ecolumn = 350;
      select Z.ref
        from ecolparams C
          join edictzuscodes Z on (Z.code = C.pval and Z.dicttype = 9)
        where C.ecolumn = new.ecolumn and C.param = 'BCODE'
        into new.bcode;
    end
    else if (i + new.days > 90) then
    begin
      select Z.ref
        from ecolparams C
          join edictzuscodes Z on (Z.code = C.pval and Z.dicttype = 9)
        where C.ecolumn = new.ecolumn - 10 and C.param = 'BCODE'
        into :bcode;
      ref = null;
      execute procedure GEN_REF('EABSENCES')
        returning_values ref;
      firstabsence = null; baseabsence = null;
      if (IsDivisionEabs = 0) then
      begin
      /*jezeli nie nastapil podzial nieobecnosci na miesiace (odbyla sie aktualizacja
        rekordu w ramach jednego miesiaca) trzeba ustawic tak ciaglosc aby nowo
        stworzona nieobecnosc ZUS nie byla poczatkiem dla choroby wynagrodzenie*/
        select baseabsence, firstabsence from E_GET_ABSENCE_PERIODS_BASES(new.employee, new.ecolumn, new.fromdate, null, null)
          into :baseabsence, :firstabsence;
        if (firstabsence = 0) then
        begin
          firstabsence = ref;
          baseabsence = ref;
        end
      end

    if ( firstabsence = ref ) then
      npayroll = new.epayroll;
    else
      npayroll = null;

      execute procedure get_global_param ('AKTUOPERATOR')
        returning_values :regoperator;
      --jezeli brak rozbicia na miesiace wylacz wymuszenie przeliczenia podstaw
      calcpaybase = new.calcpaybase;
      if (isDivisionEabs = 0 and calcpaybase = 1) then
        calcpaybase = 0;

      insert into eabsences (ref, ecolumn, employee, fromdate, todate,
          dokdate, scode, scodeb, bcode, monthpaybase, daypayamount,
          fbase, lbase, certserial, certnumber, epayroll, calcpaybase,
          regtimestamp, regoperator, chgtimestamp, chgoperator,firstabsence,baseabsence)
        values (:ref, 350, new.employee, new.fromdate + 89 - :i + 1, new.todate,
          new.dokdate, new.scode, new.scodeb, :bcode, new.monthpaybase, new.daypayamount,
          new.fbase, new.lbase, new.certserial, new.certnumber, :npayroll, :calcpaybase,
          current_timestamp(0), :regoperator, null,null,:firstabsence,:baseabsence);

      new.todate = new.fromdate + 89 - i;
      RecalcWork = 1;
      new.days = new.todate - new.fromdate + 1;
    end
  end

  --sprawdzenie, czy zmienieny rekord nie stanowi juz ciaglosci (BS20435)
  if (exists(select first 1 1
               from E_GET_ABSENCE_PERIODS_BASES(new.employee, new.ecolumn, new.fromdate, null, null)
               where firstabsence = 0))
  then begin
    new.firstabsence = new.ref;
    new.baseabsence = new.ref;
  end

  if ( firstabsence = ref ) then
    npayroll = new.epayroll;
  else
    npayroll = null;

  --podzielenie nieobecnosc na kolejne okresy/miesiace
  if (new.todate > lday) then
  begin
    lastday = new.todate;
    new.todate = lday;

    while (lastday > lday) do
    begin
      fday = lday + 1;
      execute procedure MonthLastDay(fday) returning_values :lday;
      if (lastday < lday) then
        lday = lastday;

      insert into eabsences (ecolumn, employee, fromdate, todate, dokdate,
          scode, scodeb, bcode, monthpaybase, daypayamount, fbase,lbase,
          certserial, certnumber, epayroll, calcpaybase,
          regtimestamp, regoperator, chgtimestamp, chgoperator, dimnum, dimden,
          emplcontract
          )
        values (new.ecolumn, new.employee, :fday, :lday, new.dokdate,
          new.scode, new.scodeb, new.bcode, new.monthpaybase, new.daypayamount, new.fbase, new.lbase,
          new.certserial, new.certnumber, :npayroll, new.calcpaybase,
          new.regtimestamp, new.regoperator, new.chgtimestamp, new.chgoperator, new.dimnum, new.dimden,
          new.emplcontract
          );
    end
    RecalcWork = 1;
  end

--jezeli rozpisujemy nieoebcnosc z calcpaybase = 1, z ustawiona lista plac
--i zmienia ona ecolumn to baseabsence potrafi byc nullowe, dlatego:
  if (new.calcpaybase = 1) then
    new.baseabsence = new.ref;

  if (RecalcWork = 1) then begin
    execute procedure ECAL_WORK(new.employee, new.fromdate, new.todate, new.emplcontract) --BS76214
      returning_values new.workdays, new.worksecs, new.vacsecs;
    new.worksecs = (new.worksecs * new.dimnum) / new.dimden;
  end
end^
SET TERM ; ^
