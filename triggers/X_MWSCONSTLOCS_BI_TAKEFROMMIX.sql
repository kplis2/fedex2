--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWSCONSTLOCS_BI_TAKEFROMMIX FOR MWSCONSTLOCS                   
  ACTIVE BEFORE INSERT POSITION 99 
as
begin
  if (new.takefrommix is null and new.mwsconstloctype is not null) then
    new.takefrommix = 1;
end^
SET TERM ; ^
