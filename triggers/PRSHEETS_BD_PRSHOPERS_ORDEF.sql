--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSHEETS_BD_PRSHOPERS_ORDEF FOR PRSHEETS                       
  ACTIVE BEFORE DELETE POSITION 0 
AS
begin
  update prshopers set ord = 0 where sheet = old.ref;
end^
SET TERM ; ^
