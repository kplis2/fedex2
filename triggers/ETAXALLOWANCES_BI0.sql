--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ETAXALLOWANCES_BI0 FOR ETAXALLOWANCES                 
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('ETAXALLOWANCES')
      returning_values new.REF;
end^
SET TERM ; ^
