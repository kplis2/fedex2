--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER VIEW_ECONSTELEMS_BI FOR VIEW_ECONSTELEMS               
  ACTIVE BEFORE INSERT POSITION 1 
as
begin
  insert into econstelems (
    employee,
    ecolumn,
    fromdate,
    todate,
    evalue,
    etype)
  values (
    new.employee,
    new.ecolumn,
    new.fromdate,
    new.todate,
    new.evalue,
    0);
end^
SET TERM ; ^
