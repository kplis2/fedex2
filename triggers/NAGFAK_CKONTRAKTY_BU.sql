--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_CKONTRAKTY_BU FOR NAGFAK                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable ckontrakty integer;
begin
  if(new.akceptacja = 1 and old.akceptacja <> 1 and new.skad = 2) then
  begin
    for
      select CKONTRAKTY from dokumnag where dokumnag.faktura = new.ref
        order by dokumnag.data into :ckontrakty
    do
      new.ckontrakty = :ckontrakty;
  end
  if(new.akceptacja = 1 and old.akceptacja <> 1 and new.skad = 1) then
  begin
    for
      select CKONTRAKTY from nagzam where nagzam.faktura = new.ref
        order by nagzam.datawe into :ckontrakty
    do
      new.ckontrakty = :ckontrakty;
  end
end^
SET TERM ; ^
