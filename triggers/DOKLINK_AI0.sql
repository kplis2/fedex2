--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKLINK_AI0 FOR DOKLINK                        
  ACTIVE AFTER INSERT POSITION 0 
AS
begin
  if(new.status = 2) then begin
    update doklink set status = 1 where dokplik = new.dokplik and ref <> new.ref and status = 2;
    update dokplik set plik = new.plik, dokszabl = new.dokszabl, format = new.format,
      rozmiar = new.rozmiar where ref = new.dokplik;
  end
end^
SET TERM ; ^
