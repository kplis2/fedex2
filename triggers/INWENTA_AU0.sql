--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER INWENTA_AU0 FOR INWENTA                        
  ACTIVE AFTER UPDATE POSITION 1 
as
declare variable wersjaref integer;
declare variable cena numeric(14,4);
declare variable dostawa integer;
begin
 if(new.ord = 1) then begin
    if((new.zamk  = 0) and (old.zamk = 1)) then begin
      if(old.inwm > 0) then begin
        update DOKUMNAG set AKCEPT = 0 where REF=old.inwm and AKCEPT<>0;
        delete from DOKUMNAG where REF=old.inwm;
      end
      if(old.inwp > 0) then begin
        update DOKUMNAG set AKCEPT = 0 where REF=old.inwp and AKCEPT<>0;
        delete from DOKUMNAG where REF=old.inwp;
      end
    end/*akceptacja dokument - wystawienie dokumentów w systemie na podstawie inwentaryzacji w osobnej procedurze */

 end
 if(old.inwentaparent > 0 and ((new.inwentaparent <> old.inwentaparent) or (new.inwentaparent is null))) then begin
   for select WERSJAREF, CENA,  DOSTAWA
   from INWENTAP where inwenta = new.ref
   into :wersjaref, :cena, :dostawa
   do begin
     execute procedure INWENTAP_OBLICZMASTER(old.inwentaparent, :wersjaref, :cena, :dostawa, 0);
   end
 end
 if(new.inwentaparent > 0 and ((new.inwentaparent <> old.inwentaparent) or (old.inwentaparent is null))) then begin
   for select WERSJAREF, CENA,  DOSTAWA
   from INWENTAP where inwenta = new.ref
   into :wersjaref, :cena, :dostawa
   do begin
     execute procedure INWENTAP_OBLICZMASTER(new.inwentaparent, :wersjaref, :cena, :dostawa, 0);
   end
 end
end^
SET TERM ; ^
