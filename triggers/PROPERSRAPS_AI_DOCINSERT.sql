--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PROPERSRAPS_AI_DOCINSERT FOR PROPERSRAPS                    
  ACTIVE AFTER INSERT POSITION 1 
AS
declare variable ordnumber integer;
declare variable prdocsmode smallint;
declare variable prdoctype varchar(3);
declare variable prdoctype2 varchar(3);
declare variable warehousein varchar(3);
declare variable warehouseout varchar(3);
declare variable outwarehousein varchar(3);
declare variable outwarehouseout varchar(3);
declare variable docwh varchar(3);
declare variable docwh2 varchar(3);
declare variable doubledoc smallint;
declare variable docid integer;
declare variable good varchar(20);
declare variable vers integer;
declare variable STATUS SMALLINT;
declare variable NEWDOC INTEGER;
declare variable NEWDOCSYM VARCHAR(30);
declare variable NEWDOCVAL NUMERIC(14,2);
declare variable LOT integer;
declare variable lotsymbol varchar(120);
begin
  if (new.econtractsdef is not null and new.econtractsposdef is not null
      and new.amount > 0
  ) then
  begin
    select ecp.ordnumber, ecp.prdocsmode, ecp.prdoctype, ecd.propers
      from econtractsposdef ecp
        join econtractsdef ecd on (ecp.econtractsdef = ecd.ref)
      where ecp.ref = new.econtractsposdef
      into ordnumber, prdocsmode, prdoctype, lotsymbol;
    if (ordnumber is null or prdocsmode is null or prdoctype is null or ordnumber = 0) then
      exit;
    if (ordnumber > 1) then
      if (not exists(select ecp.ref
          from econtractsposdef ecp
            join propersraps pr on (pr.econtractsposdef = ecp.ref)
          where ecp.ordnumber = :ordnumber - 1 and ecp.econtractsdef = new.econtractsdef
            and pr.prschedoper = new.prschedoper)
      ) then
        exception PREVIOUS_OPERS_RQUIRED;
    select prd.warehousein, prd.warehouseout, prd.outwarehousein, prd.outwarehouseout
      from prschedopers prso
        join prdeparts prd on (prso.prdepart = prd.symbol)
      where prso.ref = new.prschedoper
      into warehousein, warehouseout, outwarehousein, outwarehouseout;
    lot = null;
    if (prdocsmode = 1) then
    begin
      docwh = warehousein;
      if (docwh is null) then
        exception PREVIOUS_OPERS_RQUIRED 'Mag=null, prawdopodobnie brak wskazania typu operacji na karcie tech.';
      execute procedure DOKUMNAG_CREATE_DOKMAG_DOSTAWA(NULL,null,current_date,docwh,NULL,NULL,lotsymbol,null)
        returning_values :lot;
    end
    else if (prdocsmode = 2) then begin
      select max(ref) from dostawy where symbol = :lotsymbol into lot;
      docwh = warehouseout;
    end
    else if (prdocsmode = 3) then
    begin
      select max(ref) from dostawy where symbol = :lotsymbol into lot;
      docwh = outwarehousein;
    end
    else if (prdocsmode = 4) then
    begin
      select max(ref) from dostawy where symbol = :lotsymbol into lot;
      docwh = warehouseout;
    end
    select dd.miedzy, dd.przeciw from defdokum dd where dd.symbol = :prdoctype
      into doubledoc, prdoctype2;
    if (doubledoc = 1) then
    begin
      if (prdocsmode = 2) then
        docwh2 = outwarehousein;
      else if (prdocsmode = 3) then
        docwh2 = warehouseout;
    end
    if (doubledoc is null) then doubledoc = 0;
    if (new.amount <=0 ) then
        exception PREVIOUS_OPERS_RQUIRED 'Nie moża raportować w iloci 0';
    execute procedure gen_ref('DOKUMNAG') returning_values docid;
    insert into DOKUMNAG (REF,MAGAZYN, MAG2, TYP, DATA, AKCEPT, BLOKADA,ZRODLO, DOSTAWA)
      values(:docid,:docwh, :docwh2,:prdoctype, current_date, 0, 0, 0, :lot);
    select prg.ktm, prg.wersja
      from prschedguides prg
        join prschedopers prso on (prg.ref = prso.guide and prso.ref = new.prschedoper)
      into good, vers;
    insert into DOKUMPOZ(DOKUMENT, KTM, WERSJA, ILOSC,CENA)
        values (:docid, :good, :vers, new.amount, 0.01);
    update dokumnag set akcept = 1 where ref = :docid;
    if (:docid is null) then
      exception PREVIOUS_OPERS_RQUIRED 'Blad przy generowaniu dokumentu magazynowego';
    update propersraps set docid = :docid where ref = new.ref;
    -- dokument przeciwny
    if (doubledoc = 1 and prdoctype2 is not null and prdoctype2 <> '') then
    begin
      execute procedure MAG_CREATE_DOK_PRZECIW(docid, prdoctype2,current_date,null,1,1,docwh2,0,0,0)
        returning_values(STATUS, NEWDOC, NEWDOCSYM, NEWDOCVAL);
    end
  end
end^
SET TERM ; ^
