--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER KONFIG_BU0_ZAMK FOR KONFIG                         
  ACTIVE BEFORE UPDATE POSITION 0 
AS
declare variable sdate timestamp;
begin
  if (new.akronim = 'ROZRACHZAMK' and coalesce(new.wartosc,'0')<>coalesce(old.wartosc,'0') and coalesce(new.wartosc,'0') = '1') then
   begin
      select first 1 sdate from bkperiods where status = 1 order by sdate
      into :sdate;
      if (:sdate is not null) then begin
      update rozrach set zamk = 0 where datazamk > :sdate and zamk = 1 and saldo <> 0;
      update rozrach set zamk = 1 where saldo = 0 and datazamk > :sdate and zamk = 0;
      end
   end else if (new.akronim = 'ROZRACHZAMK' and coalesce(new.wartosc,'0')<>coalesce(old.wartosc,'0') and coalesce(new.wartosc,0) = 0) then
     begin
        select first 1 sdate from bkperiods where status = 1 order by sdate
        into :sdate;
        if (:sdate is not null) then begin
            update rozrach set zamk = 0 where datazamk > :sdate and zamk = 1;
        end

     end

end^
SET TERM ; ^
