--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER AMORTIZATION_BI_CLOSED FOR AMORTIZATION                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
  declare variable period_status integer;
begin
  select status from amperiods where ref = new.amperiod into :period_status;
  if (period_status > 0) then exception AMPERIOD_CLOSED 'Nie można naliczyć amortyzacji dla zamkniętego okresu!';
end^
SET TERM ; ^
