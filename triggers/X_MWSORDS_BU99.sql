--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_MWSORDS_BU99 FOR MWSORDS                        
  ACTIVE BEFORE UPDATE POSITION 99 
as
declare variable odbior string255;
begin
  if (new.status = 1 and old.status=0 and coalesce(lower(new.description),'') not like '%Waga: %Odbiorca: %' and new.mwsordtypedest =1 ) then
  begin

    select coalesce(o.nazwa,k.nazwa)
      from dokumnag d
        left join odbiorcy o on (d.odbiorcaid=o.ref)
        left join klienci k on (d.klient=k.ref)
      where d.ref = new.docid
      into :odbior;

      new.description = coalesce(new.description, '') || ' Waga: ' || coalesce(cast(cast(new.weight as numeric_14_2)  as string20), '') || ' Odbiorca: ' || coalesce(:odbior,'');
  end
end^
SET TERM ; ^
