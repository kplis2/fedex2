--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRDHDRSVERS_BIU FOR FRDHDRSVERS                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
declare variable number integer;
declare variable status smallint;
begin
  if (new.number is null) then
  if (new.main = 1) then
  begin
    if (exists (select ref from frdhdrsvers where frdhdrsvers.frdhdr = new.frdhdr
        and frdhdrsvers.main = 1 and frdhdrsvers.ref <> new.ref)) then
      exception TOO_MANY_MAIN_FRDHRSVERS;
  end
  select frhdrs.status
    from frdhdrs
      join frhdrs on (frhdrs.symbol = frdhdrs.frhdr)
    where frdhdrs.ref = new.frdhdr
    into :status;
  if (:status <> 0) then
    exception FRHDRS_NOT_ACCESIBLE;
end^
SET TERM ; ^
