--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DEFELEM_BU_REPLICAT FOR DEFELEM                        
  ACTIVE BEFORE UPDATE POSITION 0 
AS
begin
  if((new.numer <> old.numer ) or (new.numer is not null and old.numer is null) or (new.numer is null and old.numer is not null)
     or (new.nazwa <> old.nazwa ) or (new.nazwa is not null and old.nazwa is null) or (new.nazwa is null and old.nazwa is not null)
     or (new.wartosc <> old.wartosc ) or (new.wartosc is not null and old.wartosc is null) or (new.wartosc is null and old.wartosc is not null)
     or (new.ukrywanie <> old.ukrywanie ) or (new.ukrywanie is not null and old.ukrywanie is null) or (new.ukrywanie is null and old.ukrywanie is not null)
     or (new.bitmapa <> old.bitmapa ) or (new.bitmapa is not null and old.bitmapa is null) or (new.bitmapa is null and old.bitmapa is not null)
  ) then
  update defkatal set state = -1
    where defkatal.ref = old.katalog;
end^
SET TERM ; ^
