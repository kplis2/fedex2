--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EDEGEN_BI_REF FOR EDEGEN                         
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EDEGEN')
      returning_values new.REF;
end^
SET TERM ; ^
