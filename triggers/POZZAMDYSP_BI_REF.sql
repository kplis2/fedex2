--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER POZZAMDYSP_BI_REF FOR POZZAMDYSP                     
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('POZZAMDYSP')
      returning_values new.REF;
end^
SET TERM ; ^
