--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKACCOUNTS_BI_REF FOR BANKACCOUNTS                   
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable correctaccount integer;
declare variable msg4badaccount varchar(60);
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('BANKACCOUNTS')
      returning_values new.REF;
end^
SET TERM ; ^
