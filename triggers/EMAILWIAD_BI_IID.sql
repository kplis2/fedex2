--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMAILWIAD_BI_IID FOR EMAILWIAD                      
  ACTIVE BEFORE INSERT POSITION 5 
AS
declare variable tymiid integer;
declare variable sql varchar(255);
begin
  --BS44523
  if(new.id <> '' and exists (select ref from EMAILWIAD where folder=new.folder and ID = new.ID and UID is null)) then
    EXCEPTION UNIVERSAL 'PODANY ID JUZ ISTNIEJE';

  if((coalesce(new.iid,-1) < 0) and new.uid > 0) then begin
    --nie okreslono IID dla wiadomoci typu IMAP pobieranej - przyjmuje, że na początek
    select max(iid) from EMAILWIAD where folder = new.folder into :tymiid;
    new.iid = coalesce(tymiid,0) + 1;
  end else if (new.iid > 0) then begin
    --musze tutaj w trigerze BI, bo w AI bym nie znal oryginalnej wartosci iid
    if(exists (select ref from EMAILWIAD where FOLDER = new.folder and IID = new.iid + 1 and ref <>new.ref)) then begin
      sql = 'update EMAILWIAD set IID = IID + 1 where FOLDER = '||new.folder||' and IID >= '||new.iid||' and ref <> '||new.ref;
      execute statement :sql;
    end
  end
  if(coalesce(new.id,'')='') then
    select uuid_to_char(gen_uuid())||'@neos.sente' from rdb$database into new.id;

end^
SET TERM ; ^
