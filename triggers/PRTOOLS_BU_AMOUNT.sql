--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRTOOLS_BU_AMOUNT FOR PRTOOLS                        
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (new.amountfree <> old.amountfree and new.amountfree = 0 and
    exists (select symbol from prtoolstypes p WHERE p.symbol = new.tooltype and p.verificationnr = 1)
  ) then
    new.state = 3;
end^
SET TERM ; ^
