--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER FRSUMPSNS_AU0_FRPSNS FOR FRSUMPSNS                      
  ACTIVE AFTER UPDATE POSITION 0 
AS
declare variable maxsumpsn integer;
declare variable maxord smallint;
declare variable ord smallint;
begin
  maxord = 0;
  for select fr.ord from frsumpsns fs
        join frpsns fr on (fr.ref = fs.frpsn)
        where frpsn = new.frpsn and fs.frversion = new.frversion
        into :ord
  do begin
     if (:ord > : maxord) then
        maxord = :ord;
  end

  select max(fs.sumpsn) from frsumpsns fs
     where frpsn = new.frpsn and fs.frversion = new.frversion
     into :maxsumpsn;

  if (:maxsumpsn > new.frpsn) then
       update frpsns set frpsns.ord = :maxord + 1 where frpsns.ref = new.frpsn;

end^
SET TERM ; ^
