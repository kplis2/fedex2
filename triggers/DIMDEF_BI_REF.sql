--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DIMDEF_BI_REF FOR DIMDEF                         
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('DIMDEF')
      returning_values new.REF;
end^
SET TERM ; ^
