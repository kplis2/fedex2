--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLCONTRACTS_AD FOR EMPLCONTRACTS                  
  ACTIVE AFTER DELETE POSITION 0 
as
begin
  delete from employment where emplcontract = old.ref;  --BS53156
  delete from dokzlacz where ztable = 'EMPLCONTRACTS' and zdokum = old.ref;
end^
SET TERM ; ^
