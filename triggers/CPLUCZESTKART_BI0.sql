--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLUCZESTKART_BI0 FOR CPLUCZESTKART                  
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if(new.data is null) then new.data = current_date;
  if(new.symbol is null) then new.symbol = '';
  if(new.symbol = '') then exception UNIVERSAL ' Brak wypelnionego symbolu karty programu lojalnosciowego';
  if(new.zablokowana is null) then new.zablokowana = 0;
  select CPLUCZEST.cpodmiot from CPLUCZEST where ref=new.cpluczestid into new.cpodmiot;
end^
SET TERM ; ^
