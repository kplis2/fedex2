--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNAG_AU_PMG FOR DOKUMNAG                       
  ACTIVE AFTER UPDATE POSITION 0 
as
declare variable tw_nazwa varchar(255);
declare variable dp_ktm varchar(40);
declare variable dp_ref integer;
declare variable dp_wartosc numeric(14,2);
declare variable dp_ilosc numeric(14,4);
declare variable dp_pmelement integer;
declare variable dp_pmplan integer;
declare variable dp_pmposition integer;
--declare variable posref integer;
declare variable cnt integer;
declare variable dp_jedn varchar(10);
declare variable pos_jedn varchar(10);
declare variable przelicz_jedn numeric(14,4);
declare variable rozliczpo integer;
declare variable wydania integer;
declare variable koryg integer;
begin
select dd.rozliczpo, dd.wydania, dd.koryg from defdokum dd where dd.symbol = new.typ into :rozliczpo, :wydania, :koryg;
if (rozliczpo = 1) then
begin
-- naliczanie kosztów dokumentów magazynowych na plany operacyjne
  if (new.akcept = 1 and old.akcept <> new.akcept) then
    begin      --przy akceptacji
      for select dp.ref, dp.ktm, dp.wartosc, dp.ilosc, dp.pmelement, dp.pmplan, dp.pmposition, tw.nazwa, we.miara
        from dokumpoz dp
        left join towary tw on (dp.ktm = tw.ktm)
        left join wersje we on (dp.wersjaref = we.ref)
        where dp.dokument = new.ref and dp.pmelement is not null
        into :dp_ref, :dp_ktm, :dp_wartosc, :dp_ilosc, dp_pmelement, dp_pmplan, dp_pmposition, tw_nazwa, dp_jedn
      do begin      -- bierzemy kazda pozycje osobno - kazda moze byc rozliczona na innym planie i elemencie
        if (dp_pmposition is null or dp_pmposition = 0) then
        begin
          select count(ref) from pmpositions where pmelement = :dp_pmelement and ktm = :dp_ktm and mainref=ref into :cnt;
          if (:cnt > 1) then
            exception universal 'Wybrany element nie posiadaj jednoznaczej pozycji na której można rozliczyć pozycje ' || :dp_ktm|| ' '||:dp_pmelement||' '||pos_jedn;
          else
          begin
            dp_pmposition=null;
            select ref from pmpositions where pmelement = :dp_pmelement and ktm = :dp_ktm and mainref=ref into :dp_pmposition; --szukamy naszego ktmu na pozycjach planu
          end
        end
        /************************Przeliczanie jednostki************************/
        select pmpositions.unit from pmpositions where ref = :dp_pmposition into :pos_jedn;
        if (pos_jedn <> dp_jedn) then
          begin
            przelicz_jedn = null;
            select przelicz from towjedn where ktm = :dp_ktm and jedn = :pos_jedn into przelicz_jedn;
            if (przelicz_jedn is null or przelicz_jedn = 0) then
              exception universal 'Brak możliwości rozliczenia materiału. Nieznana jednostka! ' || :dp_ktm;--|| ' '||:dp_pmelement||' '||dp_pmposition;
            else
              dp_ilosc = :dp_ilosc / przelicz_jedn;

          end
        ------------------------------------------------------------------------
        if (dp_pmposition is null or dp_pmposition = 0) then
        begin
          execute procedure GEN_REF('PMPOSITIONS') returning_values :dp_pmposition;
          insert into pmpositions (REF, PMELEMENT, PMPLAN, NAME, SYMBOL, KTM, POSITIONTYPE, USEDVAL, UNIT, USEDQ)    -- gdy nie ma pozycji wspólnej to ja zakladamy
           values (:dp_pmposition, :dp_pmelement, :dp_pmplan, :tw_nazwa, :dp_ktm, :dp_ktm, 'M', :dp_wartosc, :dp_jedn, :dp_ilosc);
          update dokumpoz set pmposition = :dp_pmposition where ref = :dp_ref;
        end
        else
        begin
          -- exception test_break 'test ' || coalesce(:posref,'null') ||' '|| :dp_wartosc;
          if (koryg = 0) then
            update pmpositions set USEDVAL = USEDVAL + :dp_wartosc, USEDQ = USEDQ + :dp_ilosc where ref = :dp_pmposition;  --gdy znajdziemy dokladnie taki sam ktm, naliczamy wartosci i ilosc, cena srednia zostaje przeliczona na trig PMPOSITION_BU_PRZELICZ
          else
            update pmpositions set USEDVAL = USEDVAL - :dp_wartosc, USEDQ = USEDQ - :dp_ilosc where ref = :dp_pmposition;

          update dokumpoz set pmposition = :dp_pmposition where ref = :dp_ref;
        end
      end
    end
end
end^
SET TERM ; ^
