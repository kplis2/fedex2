--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOCPOS2REAL_BI_REF FOR DOCPOS2REAL                    
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  execute procedure gen_ref('DOCPOS2REAL') returning_values new.ref;
end^
SET TERM ; ^
