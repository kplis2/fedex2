--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSSTOCK_BI_MIX FOR MWSSTOCK                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
declare variable goodcnt integer;
declare variable deliveryarea smallint;
begin
  select deliveryarea from whsecs where ref = new.whsec
    into deliveryarea;
  if (deliveryarea <> 2 or deliveryarea is null) then
    exit;
  if (new.mixedpalgroup is null) then new.mixedpalgroup = 0;
  if (new.quantity > 0) then
  begin
    select count(distinct vers)
      from mwsstock
      where mwsconstloc = new.mwsconstloc and quantity > 0 and ispal = 0
        and vers <> new.vers and ispal = 0
      into goodcnt;
    if (new.mwsconstloc is null or new.ispal = 1) then
      exit;
    if (exists (select a.ref from mwsaccessories a where a.mwsconstloc = new.mwsconstloc)) then
      exit;
    if (goodcnt is null) then goodcnt = 0;
    if (goodcnt > 0 and new.quantity > 0) then
    begin
      new.mixedpalgroup = 1;
      update mwsstock set mixedpalgroup = 1 where mwsconstloc = new.mwsconstloc and quantity > 0
        and new.mwsconstloc is not null and mixedpalgroup <> 1 and mixedpalgroup <> 1;
    end
    else
    begin
      new.mixedpalgroup = 0;
      update mwsstock set mixedpalgroup = 0
        where mwsconstloc = new.mwsconstloc and quantity > 0
          and new.mwsconstloc is not null and mixedpalgroup <> 0;
    end
  end
end^
SET TERM ; ^
