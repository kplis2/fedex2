--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER COMPANIES_BU_WPK FOR COMPANIES                      
  ACTIVE BEFORE UPDATE POSITION 1 
AS
declare variable allow_pattern_off smallint_id;
declare variable allow_pattern_on  smallint_id;
begin
  /*$$IBEC$$ --Jeżeli zakład staje sie wzorcowym to nalezy sprawdzic czy reszta zakladow jest na to przygotowana
  if (coalesce(new.pattern,0) = 1 and coalesce(old.pattern,0) = 0) then
  begin
    execute procedure allow_company_pattern_on(new.ref)
      returning_values :allow_pattern_on;
  end else $$IBEC$$*/ if (coalesce(old.pattern,0) = 1 and coalesce(new.pattern,0) = 0) then
  begin
    -- jezeli zaklad przestaje byc wzorcowym nalezy sprawdzic czy moze
    -- tj. czy nie ma zakladow podleglych pod ten wzorzec
    execute procedure allow_company_pattern_off(new.ref)
      returning_values :allow_pattern_off;
    if (allow_pattern_off = 0) then
       exception company_pattern_off;
  end
  if (coalesce(new.pattern,0)>0) then
  begin
    if (exists( select first 1 1
                  from bkyears ba
                  where ba.company = new.ref and coalesce(ba.patternref,0)>0)) then
    begin
      exception universal'Zakład synchronizowany nie może być wzorcem';
    end
  end
end^
SET TERM ; ^
