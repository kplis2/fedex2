--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ECREDITS_BIU0_CHK_INSTALMENT FOR ECREDITS                       
  ACTIVE BEFORE INSERT OR UPDATE POSITION 0 
AS
begin
  if (new.finstalment > new.initvalue or new.instalment > new.initvalue) then
    exception ECREDITS_INCORRECT_INSTALMENTS;

  if (exists(select c.ref
    from ecredits c
    where c.employee = new.employee
      and c.credtype = new.credtype
      and c.ref <> new.ref
      and c.actvalue > 0)) then
  begin
    if (new.actvalue = 0) then
      exception uneditable_credit;
    else
      exception doubled_credit;
  end
end^
SET TERM ; ^
