--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CNOTATKI_BU_PRAWA FOR CNOTATKI                       
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
 if(new.prawadef=0) then begin
   if((new.cpodmiot<>old.cpodmiot) or ((new.cpodmiot is not null) and (old.cpodmiot is null))) then
     select PRAWA,PRAWAGRUP from CPODMIOTY where ref = new.cpodmiot
     into new.prawa, new.prawagrup;
 end
 if(new.prawa<>old.prawa) then
   execute procedure GET_SUPERIORS(new.prawa) returning_values new.prawa;
 if((new.cpodmiot<>old.cpodmiot) or ((new.cpodmiot is not null) and (old.cpodmiot is null))) then
   select SKROT,SLODEF,OPEROPIEK,TELEFON,COMPANY from CPODMIOTY where REF=new.CPODMIOT
   into new.cpodmskrot, new.cpodmslodef, new.cpodmoperopiek, new.cpodmtelefon, new.company;
 else if(new.cpodmiot is null) then begin
   new.cpodmskrot = NULL;
   new.cpodmslodef = NULL;
   new.cpodmoperopiek = NULL;
   new.cpodmtelefon = NULL;
 end
 if((new.pkosoba<>old.pkosoba) or ((new.PKOSOBA is not NULL) and (old.pkosoba is NULL))) then
   select NAZWA,TELEFON from PKOSOBY where REF=new.PKOSOBA
   into new.PKOSOBYNAZWA, new.PKOSOBYTELEFON;
 else if(new.pkosoba is NULL) then begin
   new.PKOSOBYNAZWA = NULL;
   new.PKOSOBYTELEFON = NULL;
 end
 if((new.cpodmiot<>old.cpodmiot) or ((new.cpodmiot is not null) and (old.cpodmiot is null))) then
   select ancestors from CPODMIOTY where ref = new.cpodmiot into new.cpodmancestors;
end^
SET TERM ; ^
