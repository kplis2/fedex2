--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSPALLOCSIZES_BI0 FOR MWSPALLOCSIZES                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.l is null) then new.l = 0;
  if (new.w is null) then new.w = 0;
  if (new.h is null) then new.h = 0;
  if (new.l > 0 and new.w > 0 and new.h > 0) then
  begin
    new.shortname = cast(new.l as integer)||'x'||cast(new.h as integer); --szer x wys
    new.longname = cast(new.l as integer)||'x'||cast(new.w as integer)||'x'||cast(new.h as integer); -- --szer x wys x h
  end
end^
SET TERM ; ^
