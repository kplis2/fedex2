--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER WERSJE_AU_SYNCHRO FOR WERSJE                         
  ACTIVE AFTER UPDATE POSITION 3 
as
begin
  if(new.nrwersji <> old.nrwersji or (new.ktm <> old.ktm)) then begin
    update PROMOCJE set KTM=NULL,WERSJA=NULL where WERSJAREF = new.ref;
    update ATRYBUTY set KTM=NULL,NRWERSJI=NULL where WERSJAREF = new.ref;
    update CENNIK set KTM=NULL,WERSJA=NULL where WERSJAREF = new.ref;
    update DOSTCEN set KTM=NULL,WERSJA=NULL where WERSJAREF = new.ref;
    update TOWPLIKI set KTM=NULL,WERSJA=NULL where WERSJAREF = new.ref;
  end
end^
SET TERM ; ^
