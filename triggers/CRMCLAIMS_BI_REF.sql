--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CRMCLAIMS_BI_REF FOR CRMCLAIMS                      
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
   if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('CRMCLAIMS')
      returning_values new.REF;
end^
SET TERM ; ^
