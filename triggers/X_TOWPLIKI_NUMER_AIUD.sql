--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_TOWPLIKI_NUMER_AIUD FOR TOWPLIKI                       
  ACTIVE AFTER INSERT OR UPDATE OR DELETE POSITION 0 
as
begin
  --numerowanie zdjec, glowne jako 1. reszta +1
  if (inserting) then
    execute procedure X_MWS_UPDATE_TOWPLIKI(new.ktm,1);

  if (deleting) then
    execute procedure X_MWS_UPDATE_TOWPLIKI(old.ktm,1);

  if (updating and (old.x_glowne is distinct from new.x_glowne or old.ktm is distinct from new.ktm or old.numer is distinct from new.numer)) then
    execute procedure X_MWS_UPDATE_TOWPLIKI(new.ktm,1);
end^
SET TERM ; ^
