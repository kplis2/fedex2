--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EMPLTAXINFO_BIU_BLANK FOR EMPLTAXINFO                    
  ACTIVE BEFORE INSERT OR UPDATE POSITION 1 
AS
begin
  new.costs = coalesce(new.costs,1);
  new.allowance = coalesce(new.allowance,1);
  new.leveldec = coalesce(new.leveldec,0);
  new.extamount = coalesce(new.extamount,0);
  new.businessactiv = coalesce(new.businessactiv,0);
end^
SET TERM ; ^
