--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NEOSSSERVER_STATES_BI FOR NEOSSSERVER_STATES             
  ACTIVE BEFORE INSERT POSITION 0 
as
begin
  if (new.id is null) then
    new.id = gen_id(gen_neossserver_states,1);
end^
SET TERM ; ^
