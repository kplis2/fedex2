--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER MWSACTS_BU0 FOR MWSACTS                        
  ACTIVE BEFORE UPDATE POSITION 1 
AS
begin
  if (new.status <> old.status and new.status <> 0 and new.status <> 6) then
    select operator from mwsords where ref = new.mwsord into new.operator;
  if (new.status = 0 and old.status <> 0 or new.quantity <> old.quantity) then
    new.quantityl = new.quantity;
  if (new.timestopdecl <> old.timestopdecl and new.timestopdecl is not null) then
    new.datestopdecl = cast(new.timestopdecl as date);
  if ((new.mwspallocsize is not null and old.mwspallocsize is null)
      or (new.mwspallocsize <> old.mwspallocsize)
  ) then
  begin
    select s.l, s.w, s.h, s.segtype
      from mwspallocsizes s
      where s.ref = new.mwspallocsize
      into new.l, new.w, new.h, new.segtype;
  end
end^
SET TERM ; ^
