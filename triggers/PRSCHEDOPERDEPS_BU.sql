--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PRSCHEDOPERDEPS_BU FOR PRSCHEDOPERDEPS                
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (old.depfrom <> new.depfrom) then
  begin
    select OPER, SHOPER from prschedopers
      where ref=new.depfrom
    into new.depformoper, new.depfromshoper;
  end
end^
SET TERM ; ^
