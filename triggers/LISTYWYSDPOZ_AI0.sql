--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER LISTYWYSDPOZ_AI0 FOR LISTYWYSDPOZ                   
  ACTIVE AFTER INSERT POSITION 0 
as
declare variable kontrolapak smallint;
begin
  if(new.doktyp='M') then
    update DOKUMPOZ set ILOSCLWYS = ILOSCLWYS + new.ilosc where ref=new.dokpoz;
  if(new.doktyp='Z') then
    execute procedure NAGZAM_CHECKWYS(new.dokref);

  select kontrolapak from listywysd where ref = new.dokument
  into :kontrolapak;
  if (:kontrolapak = 0) then
  begin
    --if (new.waga > 0) then
      --update LISTYWYSD set WAGA = WAGA + new.waga where ref=new.dokument;   KPI XXX138061
    if (new.objetosc > 0) then
      update LISTYWYSD set OBJETOSC = OBJETOSC + new.objetosc where ref=new.dokument;
  end
  execute procedure LISTYWYSD_OBLICZSYMBOL(new.dokument);
end^
SET TERM ; ^
