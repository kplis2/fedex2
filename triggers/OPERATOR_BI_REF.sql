--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER OPERATOR_BI_REF FOR OPERATOR                       
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('OPERATOR')
      returning_values new.REF;
end^
SET TERM ; ^
