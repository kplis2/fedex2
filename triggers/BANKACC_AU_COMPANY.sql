--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER BANKACC_AU_COMPANY FOR BANKACC                        
  ACTIVE AFTER UPDATE POSITION 0 
AS
begin
  if (new.company <> old.company) then
    update btransfers set btransfers.company = new.company
      where btransfers.bankacc = new.symbol;
end^
SET TERM ; ^
