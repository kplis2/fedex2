--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER DOKUMNOTP_BU0 FOR DOKUMNOTP                      
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if(new.cenaold is null) then new.cenaold = 0;
  if(new.cenanew is null) then new.cenanew = 0;
  if (coalesce(old.dokumrozkor,0) <> coalesce(new.dokumrozkor,0)) then
    select r.dostawa
      from dokumroz r
      where r.ref = new.dokumrozkor
      into new.dostawa;
end^
SET TERM ; ^
