--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER NAGFAK_DOKUMNOT_BD FOR NAGFAK                         
  ACTIVE BEFORE DELETE POSITION 0 
as
declare variable cnt integer;
declare variable local smallint;
begin
  execute procedure CHECK_LOCAL('NAGFAK',old.ref) returning_values :local;
  if (:local = 1) then begin
    select count(*) from dokumnot where dokumnot.faktura = old.ref into :cnt;
    if (:cnt is null) then cnt = 0;
    if (:cnt > 0) then exception NAGFAK_NOTY;
  end
end^
SET TERM ; ^
