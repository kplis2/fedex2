--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EPERSDATASECUR_BI_REF FOR EPERSDATASECUR                 
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('EPERSDATASECUR')
      returning_values new.REF;
end^
SET TERM ; ^
