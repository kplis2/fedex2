--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER CPLDEFOPER_BU0 FOR CPLDEFOPER                     
  ACTIVE BEFORE UPDATE POSITION 0 
as
declare variable cnt integer;
begin
  if(new.pm <> old.pm or
     (new.isnagroda <> old.isnagroda) or
     (new.nagroda <> old.nagroda) or (new.nagroda is not null and old.nagroda is null)or (new.nagroda is null and old.nagroda is not null) or
     (new.kontakttyp <> old.kontakttyp) or (new.kontakttyp is null and old.kontakttyp is not null) or (new.kontakttyp is not null and old.kontakttyp is null))
  then begin
    select count(*) from CPLOPER where OPERACJA = new.ref into :cnt;
    if(:cnt > 0) then exception CPLDEFOPER_WYKORZ;
  end
  if(old.symbol=old.nazwa) then new.symbol = new.nazwa;
/*  if((new.ilpkt <> old.ilpkt and new.ilpktconst = 1) or (new.ilpkt > 0 and new.ilpktconst <> old.ilpktconst and new.ilpktconst = 1))then
    update CPLOPER set ILPKT = new.ilpkt where OPERACJA = new.ref;
  if(new.autozreal = 1 and old.autozreal = 0) then
    update CPLOPER set ZREAL   = 1 where OPERACJA = new.ref;*/
end^
SET TERM ; ^
