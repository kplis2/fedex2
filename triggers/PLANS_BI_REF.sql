--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER PLANS_BI_REF FOR PLANS                          
  ACTIVE BEFORE INSERT POSITION 0 
AS
begin
  if (new.REF is null or new.REF=0) then
    execute procedure GEN_REF('PLANS')
      returning_values new.REF;
end^
SET TERM ; ^
