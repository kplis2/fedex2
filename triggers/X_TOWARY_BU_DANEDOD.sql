--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER X_TOWARY_BU_DANEDOD FOR TOWARY                         
  ACTIVE BEFORE UPDATE POSITION 0 
as
begin
  if (new.x_mws_typ_lokacji is distinct from old.x_mws_typ_lokacji and new.x_mws_typ_lokacji is null) then new.x_mws_typ_lokacji=0;
  if (new.x_mws_partie is distinct from old.x_mws_partie and new.x_mws_partie is null) then new.x_mws_partie=0;
  if (new.x_mws_serie is distinct from old.x_mws_serie and new.x_mws_serie is null) then new.x_mws_serie=0;      
  if (new.x_mws_slownik_ehrle is distinct from old.x_mws_slownik_ehrle and new.x_mws_slownik_ehrle is null) then new.x_mws_slownik_ehrle=0;
end^
SET TERM ; ^
