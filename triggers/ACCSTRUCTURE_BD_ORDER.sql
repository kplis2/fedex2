--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER ACCSTRUCTURE_BD_ORDER FOR ACCSTRUCTURE                   
  ACTIVE BEFORE DELETE POSITION 0 
AS
declare variable maxnum integer;
begin
  if (old.ord = 0) then exit;
  update accstructure set ord = 0, nlevel = nlevel - 1
    where ref <> old.ref and nlevel > old.nlevel and bkaccount = old.bkaccount;
end^
SET TERM ; ^
