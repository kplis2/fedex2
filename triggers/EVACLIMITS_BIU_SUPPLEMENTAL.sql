--@@@***SKEY***@@@--
SET TERM ^;
CREATE OR ALTER TRIGGER EVACLIMITS_BIU_SUPPLEMENTAL FOR EVACLIMITS                     
  ACTIVE BEFORE INSERT OR UPDATE POSITION 4 
AS
begin
--Urlop dodatkowy to na chwile obecna suma urlopu szkoleniowego i dodatkowego innego
  if (inserting or new.traininglimit <> old.traininglimit or new.otherlimit <> old.otherlimit) then
    new.supplementallimit = new.traininglimit + new.otherlimit;
end^
SET TERM ; ^
